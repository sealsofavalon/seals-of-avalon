// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MONSTER_110000123_ANIM_H__
#define MONSTER_110000123_ANIM_H__

namespace Monster_110000123_Anim
{
    enum
    {
        ATTACK_01               = 0,
        ATTACK_02               = 1,
        DEATH                   = 2,
        RUN                     = 4,
        SKILL01                 = 3,
        STAND                   = 5,
        WAIT                    = 6,
        WALK                    = 7
    };
}

#endif  // #ifndef MONSTER_110000123_ANIM_H__
