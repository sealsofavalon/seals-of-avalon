// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MONSTER_110000548_ANIM_H__
#define MONSTER_110000548_ANIM_H__

namespace Monster_110000548_Anim
{
    enum
    {
        ATTACK_01               = 1,
        ATTACK_02               = 2,
        ATTACK_03               = 3,
        DEATH                   = 4,
        RUN                     = 5,
        SKILL01                 = 6,
        SKILL02                 = 7,
        SKILL03                 = 11,
        SKILL04                 = 12,
        SKILL05                 = 10,
        SKILL06                 = 13,
        SKILL07                 = 14,
        SKILL08                 = 15,
        SKILL09                 = 16,
        SKILL11                 = 18,
        SKILL12                 = 17,
        SKILL13                 = 20,
        SKILL14                 = 19,
        STAND                   = 0,
        WAIT                    = 8,
        WALK                    = 9
    };
}

#endif  // #ifndef MONSTER_110000548_ANIM_H__
