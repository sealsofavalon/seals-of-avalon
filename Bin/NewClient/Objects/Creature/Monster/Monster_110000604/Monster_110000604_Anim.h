// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MONSTER_110000604_ANIM_H__
#define MONSTER_110000604_ANIM_H__

namespace Monster_110000604_Anim
{
    enum
    {
        BACKWARD01              = 21,
        RUN                     = 6,
        SKILL01                 = 5,
        STAND                   = 0,
        STRAFE_LEFT             = 23,
        STRAFE_RIGHT            = 22,
        WAIT                    = 3,
        WALK                    = 1
    };
}

#endif  // #ifndef MONSTER_110000604_ANIM_H__
