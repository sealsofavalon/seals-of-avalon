// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MONSTER_110000606_ANIM_H__
#define MONSTER_110000606_ANIM_H__

namespace Monster_110000606_Anim
{
    enum
    {
        ATTACK_01               = 1,
        ATTACK_02               = 2,
        DEATH                   = 3,
        DIALOG                  = 7,
        RUN                     = 4,
        SKILL01                 = 8,
        STAND                   = 0,
        WAIT                    = 5,
        WALK                    = 6
    };
}

#endif  // #ifndef MONSTER_110000606_ANIM_H__
