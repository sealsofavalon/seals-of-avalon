// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MONSTER_110006004_ANIM_H__
#define MONSTER_110006004_ANIM_H__

namespace Monster_110006004_Anim
{
    enum
    {
        ATTACK_01               = 7,
        ATTACK_02               = 8,
        BACKWARD01              = 21,
        JUMP_FALL               = 17,
        JUMP_FALL_RUN           = 18,
        JUMP_FLY                = 19,
        JUMP_START              = 20,
        RUN                     = 6,
        SKILL01                 = 5,
        SKILL02                 = 10,
        SKILL03                 = 11,
        SKILL04                 = 9,
        SKILL05                 = 12,
        SKILL06                 = 14,
        SKILL07                 = 16,
        SKILL08                 = 15,
        SKILL09                 = 13,
        STAND                   = 0,
        STRAFE_LEFT             = 22,
        STRAFE_RIGHT            = 23,
        WAIT                    = 3,
        WAIT1                   = 2,
        WAIT2                   = 4,
        WALK                    = 1
    };
}

#endif  // #ifndef MONSTER_110006004_ANIM_H__
