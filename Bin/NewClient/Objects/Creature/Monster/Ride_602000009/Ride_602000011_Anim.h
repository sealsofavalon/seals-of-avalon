// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RIDE_602000011_ANIM_H__
#define RIDE_602000011_ANIM_H__

namespace Ride_602000011_Anim
{
    enum
    {
        BACKWARD01              = 7,
        JUMP_FALL               = 1,
        JUMP_FALL_RUN           = 2,
        JUMP_FLY                = 3,
        JUMP_START              = 4,
        RUN                     = 5,
        STAND                   = 0,
        WAIT                    = 6
    };
}

#endif  // #ifndef RIDE_602000011_ANIM_H__
