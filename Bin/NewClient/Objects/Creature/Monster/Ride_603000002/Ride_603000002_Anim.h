// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RIDE_603000002_ANIM_H__
#define RIDE_603000002_ANIM_H__

namespace Ride_603000002_Anim
{
    enum
    {
        ATTACK_01               = 0,
        BACKWARD01              = 9,
        DEATH                   = 1,
        JUMP_FALL               = 2,
        JUMP_FALL_RUN           = 3,
        JUMP_FLY                = 4,
        JUMP_START              = 5,
        RUN                     = 6,
        STAND                   = 7,
        WAIT                    = 8
    };
}

#endif  // #ifndef RIDE_603000002_ANIM_H__
