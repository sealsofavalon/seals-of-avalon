// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RIDE_603000003_ANIM_H__
#define RIDE_603000003_ANIM_H__

namespace Ride_603000003_Anim
{
    enum
    {
        BACKWARD01              = 7,
        JUMP_FALL               = 0,
        JUMP_FALL_RUN           = 1,
        JUMP_FLY                = 2,
        JUMP_START              = 3,
        RUN                     = 4,
        STAND                   = 5,
        WAIT                    = 6
    };
}

#endif  // #ifndef RIDE_603000003_ANIM_H__
