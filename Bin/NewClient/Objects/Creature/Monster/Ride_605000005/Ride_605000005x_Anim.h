// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RIDE_605000005X_ANIM_H__
#define RIDE_605000005X_ANIM_H__

namespace Ride_605000005x_Anim
{
    enum
    {
        JUMP_FALL               = 1,
        JUMP_FALL_RUN           = 2,
        JUMP_FLY                = 3,
        JUMP_START              = 4,
        RUN                     = 5,
        STAND                   = 0,
        WAIT                    = 6
    };
}

#endif  // #ifndef RIDE_605000005X_ANIM_H__
