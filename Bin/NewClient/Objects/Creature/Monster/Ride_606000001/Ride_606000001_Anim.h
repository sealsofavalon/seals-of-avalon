// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RIDE_606000001_ANIM_H__
#define RIDE_606000001_ANIM_H__

namespace Ride_606000001_Anim
{
    enum
    {
        ACTION_300001212        = 13,
        ACTION_400004210        = 12,
        ATTACK_01               = 6,
        BACKWARD01              = 9,
        DEATH                   = 7,
        JUMP_FALL               = 5,
        JUMP_FALL_RUN           = 8,
        JUMP_FLY                = 4,
        JUMP_START              = 3,
        RUN                     = 2,
        STAND                   = 0,
        STRAFE_LEFT             = 11,
        STRAFE_RIGHT            = 10,
        WAIT                    = 1
    };
}

#endif  // #ifndef RIDE_606000001_ANIM_H__
