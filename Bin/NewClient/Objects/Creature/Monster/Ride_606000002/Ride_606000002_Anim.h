// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RIDE_606000002_ANIM_H__
#define RIDE_606000002_ANIM_H__

namespace Ride_606000002_Anim
{
    enum
    {
        ACTION_300001212        = 0,
        ACTION_400004210        = 2,
        ATTACK_01               = 3,
        BACKWARD01              = 4,
        DEATH                   = 5,
        JUMP_FALL               = 6,
        JUMP_FALL_RUN           = 7,
        JUMP_FLY                = 8,
        JUMP_START              = 9,
        RUN                     = 10,
        STAND                   = 11,
        STRAFE_LEFT             = 12,
        STRAFE_RIGHT            = 13,
        WAIT                    = 1
    };
}

#endif  // #ifndef RIDE_606000002_ANIM_H__
