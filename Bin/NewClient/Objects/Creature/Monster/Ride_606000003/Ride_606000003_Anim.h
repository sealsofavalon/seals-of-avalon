// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RIDE_606000003_ANIM_H__
#define RIDE_606000003_ANIM_H__

namespace Ride_606000003_Anim
{
    enum
    {
        ACTION_300001212        = 13,
        ACTION_400004210        = 12,
        ATTACK_01               = 1,
        BACKWARD01              = 11,
        DEATH                   = 2,
        JUMP_FALL               = 3,
        JUMP_FALL_RUN           = 4,
        JUMP_FLY                = 5,
        JUMP_START              = 6,
        RUN                     = 7,
        STAND                   = 0,
        STRAFE_LEFT             = 10,
        STRAFE_RIGHT            = 9,
        WAIT                    = 8
    };
}

#endif  // #ifndef RIDE_606000003_ANIM_H__
