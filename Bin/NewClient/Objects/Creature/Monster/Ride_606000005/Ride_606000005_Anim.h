// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RIDE_606000005_ANIM_H__
#define RIDE_606000005_ANIM_H__

namespace Ride_606000005_Anim
{
    enum
    {
        ACTION_300001212        = 13,
        ACTION_400004210        = 0,
        ATTACK_01               = 2,
        BACKWARD01              = 3,
        DEATH                   = 4,
        JUMP_FALL               = 5,
        JUMP_FALL_RUN           = 6,
        JUMP_FLY                = 7,
        JUMP_START              = 8,
        RUN                     = 9,
        STAND                   = 10,
        STRAFE_LEFT             = 11,
        STRAFE_RIGHT            = 12,
        WAIT                    = 1
    };
}

#endif  // #ifndef RIDE_606000005_ANIM_H__
