// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RIDE_606000104_ANIM_H__
#define RIDE_606000104_ANIM_H__

namespace Ride_606000104_Anim
{
    enum
    {
        ATTACK_01               = 1,
        BACKWARD01              = 12,
        DEATH                   = 2,
        JUMP_FALL               = 3,
        JUMP_FALL_RUN           = 4,
        JUMP_FLY                = 5,
        JUMP_START              = 6,
        RUN                     = 7,
        SKILL01                 = 8,
        SKILL02                 = 9,
        SKILL03                 = 10,
        STAND                   = 0,
        STRAFE_LEFT             = 14,
        STRAFE_RIGHT            = 13,
        WAIT                    = 11
    };
}

#endif  // #ifndef RIDE_606000104_ANIM_H__
