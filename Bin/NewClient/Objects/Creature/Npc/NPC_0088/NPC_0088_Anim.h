// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef NPC_0088_ANIM_H__
#define NPC_0088_ANIM_H__

namespace NPC_0088_Anim
{
    enum
    {
        ATTACK_01               = 2,
        ATTACK_02               = 3,
        DEATH                   = 4,
        DIALOG                  = 5,
        RUN                     = 6,
        STAND                   = 0,
        WAIT                    = 7,
        WALK                    = 1
    };
}

#endif  // #ifndef NPC_0088_ANIM_H__
