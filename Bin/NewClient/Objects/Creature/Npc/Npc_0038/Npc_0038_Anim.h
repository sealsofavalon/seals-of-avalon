// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef NPC_0038_ANIM_H__
#define NPC_0038_ANIM_H__

namespace Npc_0038_Anim
{
    enum
    {
        ATTACK_01               = 3,
        ATTACK_02               = 4,
        DEATH                   = 5,
        RUN                     = 6,
        STAND                   = 1,
        WAIT                    = 0,
        WALK                    = 2
    };
}

#endif  // #ifndef NPC_0038_ANIM_H__
