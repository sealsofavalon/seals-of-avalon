// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef NPC_0058_ANIM_H__
#define NPC_0058_ANIM_H__

namespace Npc_0058_Anim
{
    enum
    {
        DIALOG                  = 3,
        RUN                     = 4,
        STAND                   = 0,
        WAIT                    = 1,
        WALK                    = 2
    };
}

#endif  // #ifndef NPC_0058_ANIM_H__
