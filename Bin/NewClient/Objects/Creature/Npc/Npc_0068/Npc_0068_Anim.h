// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef NPC_0068_ANIM_H__
#define NPC_0068_ANIM_H__

namespace Npc_0068_Anim
{
    enum
    {
        DIALOG                  = 0,
        RUN                     = 1,
        STAND                   = 2,
        WAIT                    = 3,
        WALK                    = 4
    };
}

#endif  // #ifndef NPC_0068_ANIM_H__
