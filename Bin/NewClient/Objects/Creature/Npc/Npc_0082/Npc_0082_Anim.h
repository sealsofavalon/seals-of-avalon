// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef NPC_0082_ANIM_H__
#define NPC_0082_ANIM_H__

namespace Npc_0082_Anim
{
    enum
    {
        ATTACK_01               = 0,
        ATTACK_02               = 1,
        DEATH                   = 2,
        DIALOG                  = 3,
        RUN                     = 4,
        STAND                   = 5,
        WAIT                    = 6,
        WALK                    = 7
    };
}

#endif  // #ifndef NPC_0082_ANIM_H__
