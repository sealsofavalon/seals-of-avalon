// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef NPC_0147_ANIM_H__
#define NPC_0147_ANIM_H__

namespace Npc_0147_Anim
{
    enum
    {
        ATTACK_01               = 1,
        ATTACK_02               = 2,
        DEATH                   = 3,
        DIALOG                  = 4,
        RUN                     = 5,
        STAND                   = 0,
        WAIT                    = 6,
        WALK                    = 7
    };
}

#endif  // #ifndef NPC_0147_ANIM_H__
