static const float3 g_MaterialEmissive = { 0.2f, 0.2f, 0.2f };
static const float3 g_MaterialDiffuse = { 1.2f, 1.2f, 1.2f };
static const float3 g_MaterialAmbient = { 0.7f, 0.7f, 0.7f };

static const float3 g_SpecularColor = { 0.3f, 0.3f, 0.3f };
static const float  g_SpecularPower = 30.f;

static const int MAX_BONES = 30;

float4x4 g_SkinWorld : SkinWorld;
float4x4 g_SkinWorldViewProj : SkinWorldViewProj;
float4x3 g_BoneMatrix3[MAX_BONES] : BoneMatrix3;
float4 g_EyePos : EyePos;

float3 g_DirAmbient0 : Ambient
<
    string Object = "DirectionalLight";
    int ObjectIndex = 0;
>;

float3 g_DirDiffuse0 : Diffuse
<
    string Object = "DirectionalLight";
    int ObjectIndex = 0;
>;

float3 g_DirWorldDirection0 : Direction
<
    string Object = "DirectionalLight";
    int ObjectIndex = 0;
>;

float4x4 transformGlow : TexTransformGlow;

struct Input
{
    float4 pos : POSITION;
    float3 normal : NORMAL;
    float4 blendWeights : BLENDWEIGHT;
    float4 blendIndices : BLENDINDICES;
    float2 tex0 : TEXCOORD0;
};

struct Output
{
    float4 pos : POSITION;
    float4 diffuse : TEXCOORD0;
    float2 tex0 : TEXCOORD1;
    float2 tex1 : TEXCOORD2;
};

Output AlphaReflectSkinnedVS(Input In)
{
    Output Out;
        
    // Compensate for lack of UBYTE4 on Geforce3
    int4 indices = D3DCOLORtoUBYTE4(In.blendIndices);

    // Calculate normalized fourth bone weight
    float weight3 = 1.0f - In.blendWeights[0] - In.blendWeights[1] - In.blendWeights[2];
    
    float4x3 BoneTransform;
    BoneTransform  = g_BoneMatrix3[indices[0]] * In.blendWeights[0];
    BoneTransform += g_BoneMatrix3[indices[1]] * In.blendWeights[1];
    BoneTransform += g_BoneMatrix3[indices[2]] * In.blendWeights[2];
    BoneTransform += g_BoneMatrix3[indices[3]] * weight3;
    
    float3 pos = mul(In.pos, BoneTransform);
    float3 normal = mul(In.normal, (float3x3)BoneTransform);
    
    float3 worldPos = mul(pos, (float3x3)g_SkinWorld);
    Out.pos = mul(float4(pos, 1.f), g_SkinWorldViewProj);
    Out.tex0 = In.tex0;
    
    float3 worldNrm = mul(normal, (float3x3)g_SkinWorld);
    worldNrm = normalize(worldNrm);
    
    float3 viewVector = g_EyePos - worldPos;
    float3 worldReflect = reflect(-viewVector, worldNrm);
    Out.tex1 = normalize(worldReflect);
            
    //light0
    float intensity = max(0, dot(-g_DirWorldDirection0, worldNrm));
    float3 diffuseAccum = g_DirDiffuse0*intensity;
    float3 ambientAccum = g_DirAmbient0;
    
    //light1
    //intensity = max(0, dot(g_DirWorldDirection0, worldNrm));
    //diffuseAccum += g_DirDiffuse0*intensity;
    //ambientAccum += g_DirAmbient0;
    
    Out.diffuse.xyz = g_MaterialEmissive + g_MaterialDiffuse*diffuseAccum + g_MaterialAmbient*ambientAccum;
    Out.diffuse.w = 1.f;
    return Out;
}


// textures
texture BaseMap
<
    string NTM = "base";
>;

texture ReflectMap
<
    string NTM = "shader";
    int NTMIndex = 0;
>;

// Samplers
sampler BaseSampler = sampler_state
{
    Texture = (BaseMap);
    MINFILTER = LINEAR;
    MIPFILTER = LINEAR;
    MAGFILTER = LINEAR;
};

sampler ReflectSampler = sampler_state
{
    Texture = (ReflectMap);
    MINFILTER = LINEAR;
    MIPFILTER = LINEAR;
    MAGFILTER = LINEAR;
};

float4 AlphaReflectSkinnedPS(float4 diffuse : TEXCOORD0, float2 tex0: TEXCOORD1, float2 tex1: TEXCOORD2) : COLOR0
{
    float4 baseColor = tex2D(BaseSampler, tex0);
    float4 reflectColor = tex2D(ReflectSampler, tex1);
    return baseColor*diffuse + reflectColor*baseColor.a;
}

technique AlphaReflectSkinned
<
    string description = "Alpha Glow Skinned";
    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
    int BonesPerPartition = MAX_BONES;
>
{
    pass P0
    {
        VertexShader = compile vs_2_0 AlphaReflectSkinnedVS();
        PixelShader = compile ps_2_0 AlphaReflectSkinnedPS();
    }
}

