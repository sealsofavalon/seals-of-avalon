texture g_texSrcColor1;
sampler2D g_samSrcColor1 = sampler_state
{
    Texture = (g_texSrcColor1);
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Linear;
    MipFilter = Linear;
};

texture g_texSrcColor2;
sampler2D g_samSrcColor2 = sampler_state
{
    Texture = (g_texSrcColor2);
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Linear;
    MipFilter = Linear;
};

float g_bloomThreshold : GLOBAL;

#define GB_SAMPLE_COUNT 15

float2 g_gaussianBlurOffsets[GB_SAMPLE_COUNT] : GLOBAL;
float g_gaussianBlurWeights[GB_SAMPLE_COUNT] : GLOBAL;

float g_bloomIntensity : GLOBAL;
float g_baseIntensity : GLOBAL;
float g_bloomSaturation : GLOBAL;
float g_baseSaturation : GLOBAL;

float4x4 g_matWorldViewProj : WORLDVIEWPROJECTION;

float4 AdjustSaturation(float4 color, float saturation)
{
    float grey = dot(color, float3(0.3, 0.59, 0.11));
    return lerp(grey, color, saturation);
}

struct VS_OUTPUT
{
    float4 Pos      : POSITION;
    float2 Tex0     : TEXCOORD0;
};

VS_OUTPUT Default_VS(float4 inPos: POSITION, float2 inTex0: TEXCOORD0)
{
    VS_OUTPUT Out;
    Out.Pos = mul(inPos, g_matWorldViewProj);
    Out.Tex0 = inTex0;
    return Out;
}

float4 BloomThreshold_PS(float2 texCoord: TEXCOORD0) : COLOR0
{
   float4 t = tex2D(g_samSrcColor1, texCoord);
   return saturate((t - g_bloomThreshold) / (1.0f - g_bloomThreshold));
}

float4 GaussianBlurH_PS(float2 texCoord : TEXCOORD0) : COLOR0
{
    float4 t = 0;
    
    for (int i = 0; i < GB_SAMPLE_COUNT; i++)
    {
        //t += tex2D(g_samSrcColor1, texCoord + g_gaussianBlurOffsets[i]) * g_gaussianBlurWeights[i];
        
        float2 offset = { g_gaussianBlurOffsets[i].x, 0 };
        t += tex2D(g_samSrcColor1, texCoord + offset) * g_gaussianBlurWeights[i];
    }
    
    return t;
}

float4 GaussianBlurV_PS(float2 texCoord : TEXCOORD0) : COLOR0
{
    float4 t = 0;
    
    for (int i = 0; i < GB_SAMPLE_COUNT; i++)
    {
        //t += tex2D(g_samSrcColor1, texCoord + g_gaussianBlurOffsets[i]) * g_gaussianBlurWeights[i];
        
        float2 offset = { 0, g_gaussianBlurOffsets[i].y };
        t += tex2D(g_samSrcColor1, texCoord + offset) * g_gaussianBlurWeights[i];
    }
    
    return t;
}

float4 BloomCombine_PS(float2 texCoord : TEXCOORD0) : COLOR0
{
    float4 bloom = tex2D(g_samSrcColor1, texCoord);
    float4 base = tex2D(g_samSrcColor2, texCoord);
    
    bloom = AdjustSaturation(bloom, g_bloomSaturation) * g_bloomIntensity;
    base = AdjustSaturation(base, g_baseSaturation) * g_baseIntensity;
    
    base *= (1 - saturate(bloom));
    
    // Combine the two images.
    return base + bloom;
}

technique BloomThreshold
{
    pass p0
    {
        VertexShader = compile vs_2_0 Default_VS();
        PixelShader = compile ps_2_0 BloomThreshold_PS();
    }
}

technique GaussianBlurH
{
    pass p0
    {
        VertexShader = compile vs_2_0 Default_VS();
        PixelShader = compile ps_2_0 GaussianBlurH_PS();
    }
}

technique GaussianBlurV
{
    pass p0
    {
        VertexShader = compile vs_2_0 Default_VS();
        PixelShader = compile ps_2_0 GaussianBlurV_PS();
    }
}

technique BloomCombine
{
    pass p0
    {
        VertexShader = compile vs_2_0 Default_VS();
        PixelShader = compile ps_2_0 BloomCombine_PS();
    }
}