
static const int MAX_SAMPLES = 16;

float2 gakSampleOffsets[MAX_SAMPLES] : GLOBAL;

//-----------------------------------------------------------------------------
// **TEXTURES AND SAMPLERS**
//-----------------------------------------------------------------------------
texture BaseTex
< 
    string NTM = "base";
>;

texture SmoothTex
< 
    bool hidden = true;
    string NTM = "shader";
    int NTMIndex = 1;
>;

texture SmoothBigTex
< 
    bool hidden = true;
    string NTM = "shader";
    int NTMIndex = 2;
>;

sampler2D BaseSampler =
sampler_state
{
    Texture = <BaseTex>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler2D SmoothSampler =
sampler_state
{
    Texture = <SmoothTex>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler2D SmoothBigSampler =
sampler_state
{
    Texture = <SmoothBigTex>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    AddressU = Clamp;
    AddressV = Clamp;
};


float4x4 WorldViewProj : WORLDVIEWPROJECTION;

struct VS_OUTPUT
{
    float4 Pos      : POSITION;
    float2 Tex0     : TEXCOORD0;
};

VS_OUTPUT VSMain(float4 inPos: POSITION, float2 inTex0: TEXCOORD0)
{
    VS_OUTPUT Out;
    Out.Pos = mul(inPos, WorldViewProj);
    Out.Tex0 = inTex0;
    return Out;
}



 float4 BloomSmoothPS(
	   in float2 kScreenPosition : TEXCOORD0
	   ) : COLOR
{
    float4 kSample = 0.0f;

    for (int i = 0; i < 16; i++)
    {
        kSample += tex2D(BaseSampler, kScreenPosition + 
            gakSampleOffsets[i]);
    }
    
    return kSample / 16;
}



float4 BloomBigSmoothPS(
	   float2 kScreenPosition : TEXCOORD0
	   ) : COLOR
{
    float4 kSample = tex2D(SmoothSampler, kScreenPosition );

    return float4(kSample.rgb, 1.0f);
}



float4 BloomFinalPS(
			  float2 TexC0 : TEXCOORD0
			  ) : COLOR
{
	float4 BaseTx = tex2D(BaseSampler, TexC0);
	float4 SmoothTx = tex2D(SmoothBigSampler, TexC0);
	float4 BigSmoothTx = tex2D(SmoothSampler, TexC0);
     
        SmoothTx.rgb = SmoothTx.rgb * SmoothTx.rgb;

        float4 lerpColor;

	//lerpColor = lerp(BaseTx, SmoothTx, SmoothTx.b + 0.1); //b越大 结果越偏向t1 越亮越模糊
	
       float4 saveBase = BaseTx;
       BaseTx.rgb = BaseTx.rgb * BaseTx.rgb;
       BaseTx.rgb = BaseTx.rgb*0.3 + saveBase.rgb*0.7;
      
       lerpColor.rgb = BaseTx.rgb + SmoothTx.rgb*0.3;

       lerpColor.rgb = lerpColor.rgb*1.2;

       // lerpColor = saturate(lerpColor);

	return float4(lerpColor.rgb, 1.0);
}

float4 BloomDeadPS(
			  float2 TexC0 : TEXCOORD0
			  ) : COLOR
{
	float4 BaseTx = tex2D(BaseSampler, TexC0);
	float4 SmoothTx = tex2D(SmoothBigSampler, TexC0);
	float4 BigSmoothTx = tex2D(SmoothSampler, TexC0);
     
        SmoothTx.rgb = SmoothTx.rgb * SmoothTx.rgb;

        float4 lerpColor;

	//lerpColor = lerp(BaseTx, SmoothTx, SmoothTx.b + 0.1); //b越大 结果越偏向t1 越亮越模糊
	
       float4 saveBase = BaseTx;
       BaseTx.rgb = BaseTx.rgb * BaseTx.rgb;
       BaseTx.rgb = BaseTx.rgb*0.3 + saveBase.rgb*0.7;
      
       lerpColor.rgb = BaseTx.rgb + SmoothTx.rgb*0.3;

       lerpColor.rgb = lerpColor.rgb*1.2;

       lerpColor.rgb = (lerpColor.r + lerpColor.g + lerpColor.b) / 3.0;       

       return float4(lerpColor.rgb, 1.0);
}


technique Bloom_SmoothPS
<
    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
>
{
    pass P0
    {
	 VertexShader = compile vs_2_0 VSMain();
        PixelShader  = compile ps_2_0 BloomSmoothPS();
    }
}


technique Bloom_SmoothBigPS
<
    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
>
{
    pass P0
    {
	 VertexShader = compile vs_2_0 VSMain();
        PixelShader  = compile ps_2_0 BloomBigSmoothPS();
    }
}


technique Bloom_FinalPS
<
    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
>
{
    pass P0
    {
	 VertexShader = compile vs_2_0 VSMain();
        PixelShader  = compile ps_2_0 BloomFinalPS();
    }
}


technique Bloom_DeadPS
<
    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
>
{
    pass P0
    {
	 VertexShader = compile vs_2_0 VSMain();
        PixelShader  = compile ps_2_0 BloomDeadPS();
    }
}