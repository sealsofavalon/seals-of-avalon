static const float3 g_MaterialEmissive = { 0.2f, 0.2f, 0.2f };
static const float3 g_MaterialDiffuse = { 1.2f, 1.2f, 1.2f };
static const float3 g_MaterialAmbient = { 0.7f, 0.7f, 0.7f };

static const float3 g_SpecularColor = { 0.3f, 0.3f, 0.3f };
static const float  g_SpecularPower = 30.f;

static const int MAX_BONES = 30;

float4x4 g_World : World;
float4x4 g_WorldViewProj : WorldViewProj;
float4x4 g_ViewProj : ViewProj;
float4 g_EyePos : EyePos;

float4 g_MatDiffuse : MaterialDiffuse;

float g_ExtrusionScale = 0.024f;
float4 EdgeColor = {1.0f, 0.98f, 0.49f, 0.6f};

float3 g_DirAmbient0 : Ambient
<
    string Object = "DirectionalLight";
    int ObjectIndex = 0;
>;

float3 g_DirDiffuse0 : Diffuse
<
    string Object = "DirectionalLight";
    int ObjectIndex = 0;
>;

float3 g_DirWorldDirection0 : Direction
<
    string Object = "DirectionalLight";
    int ObjectIndex = 0;
>;

struct Input
{
    float4 pos : POSITION;
    float3 normal : NORMAL;
    float2 tex0 : TEXCOORD0;
};

struct Output
{
    float4 pos : POSITION;
    float4 diffuse : TEXCOORD0;
    float2 tex0 : TEXCOORD1;
};

struct EdgePassOutput
{
    float4 Pos : POSITION;
    float2 tex0 : TEXCOORD0;
};

EdgePassOutput EdgePassVS(Input In)
{
    EdgePassOutput Out;
 
    float4 worldPos = mul(In.pos, g_World); 
    float4 dir = worldPos - g_EyePos;
    float distance = length(dir);

    float ExtrusionScale = g_ExtrusionScale * (distance * 0.06f);

    float3 worldNrm = mul(In.normal, (float3x3)g_World);
    worldNrm = normalize(worldNrm);
    
    // Extrude the position a small amount before we transform into clip space.
    float4 ExtrudedPos = worldPos + float4((ExtrusionScale * worldNrm), 0.0f);

    Out.Pos = mul(ExtrudedPos, g_ViewProj);
    Out.tex0 = In.tex0;

    return Out;
}

Output VS(Input In, uniform bool hightLight)
{
    Output Out;
    Out.pos = mul(In.pos, g_WorldViewProj);
    Out.tex0 = In.tex0;
    
    float3 worldNrm = mul(In.normal, (float3x3)g_World);
    worldNrm = normalize(worldNrm);
    
    //light0
    float intensity = max(0, dot(-g_DirWorldDirection0, worldNrm));
        
    float3 diffuseAccum = g_DirDiffuse0*intensity;
    float3 ambientAccum = g_DirAmbient0;
        
    //light1
    //把第一个光照反向后当成第二个光照
    //intensity = max(0, dot(g_DirWorldDirection0, worldNrm));
    //diffuseAccum += g_DirDiffuse0*intensity;
    //ambientAccum += g_DirAmbient0;
    
    Out.diffuse.xyz = g_MaterialEmissive + g_MatDiffuse.rgb*diffuseAccum + g_MaterialAmbient*ambientAccum;
    Out.diffuse.w = g_MatDiffuse.a;
    if(hightLight)
    {
        Out.diffuse.xyz += float3(0.5f, 0.5f, 0.5f);
    }
    
    return Out;
}


// textures
texture BaseMap
<
    string NTM = "base";
>;

// Samplers
sampler BaseSampler = sampler_state
{
    Texture = (BaseMap);
    MINFILTER = LINEAR;
    MIPFILTER = LINEAR;
    MAGFILTER = LINEAR;
};

float4 EdgePassPS(float4 tex0 : TEXCOORD0) : COLOR0
{
    float4 baseColor = tex2D(BaseSampler, tex0);
    float4 FinalColor = EdgeColor;
    FinalColor.a = baseColor.a * FinalColor.a;
    return FinalColor;
}


float4 PS(float4 diffuse : TEXCOORD0, float2 tex0: TEXCOORD1) : COLOR0
{
    float4 baseColor = tex2D(BaseSampler, tex0);
    return baseColor * diffuse;
}

technique FXSY
<
    string description = "FXSY ";
    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
>
{
    pass P0
    {
        VertexShader = compile vs_2_0 VS(false);
        PixelShader = compile ps_2_0 PS();
    }
}



technique FXSY_HL
<
    string description = "FXSY ";
    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
>
{
    pass P0
    {
        VertexShader = compile vs_2_0 VS(true);
        PixelShader = compile ps_2_0 PS();
    }
}


technique FXSY_HL_EDG
<
    string description = "FXSY HL";
    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
>
{
    pass P0
    {
        VertexShader = compile vs_1_1 EdgePassVS();
        PixelShader = compile ps_2_0 EdgePassPS();
        CullMode = CCW;
    }

    pass P1
    {
        VertexShader = compile vs_2_0 VS(true);
        PixelShader = compile ps_2_0 PS();
        CullMode = NONE;
    }
}

technique FXSY_ALPHA
<
    string description = "FXSY ALPHA";
    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
>
{
    pass P0
    {
        VertexShader = compile vs_2_0 VS(false);
        PixelShader = compile ps_2_0 PS();
        CullMode = CW;
        AlphaBlendEnable = true;
        AlphaTestEnable = false;
        SrcBlend = SrcAlpha;
        DestBlend = InvSrcAlpha;
    }
}