
float4x4 g_World : World;
float4x4 g_ViewProj : ViewProj;
float3 g_EyePos : EyePos;

struct Input
{
    float4 pos : POSITION;
    float3 normal : NORMAL;
    float2 tex0 : TEXCOORD0;
    float3 binormal : BINORMAL;
    float3 tangent : TANGENT;
};

struct Output
{
    float4 hpos : POSITION;
    float4 worldPos : TEXCOORD0;
    float3 worldNormal : TEXCOORD1;
    float3 worldBinormal : TEXCOORD2;
    float3 worldTangent : TEXCOORD3;
    float3 tangentSpaceViewVec : TEXCOORD4;
    float2 tex0 : TEXCOORD5;    
};

Output VS(Input In)
{
    Output Out;
    Out.worldPos = mul(In.pos, g_World);
    Out.hpos = mul(Out.worldPos, g_ViewProj);
    Out.tex0 = In.tex0;
    
    Out.worldNormal = mul(In.normal, (float3x3)g_World);
    Out.worldBinormal = mul(In.binormal, (float3x3)g_World);
    Out.worldTangent = mul(In.tangent, (float3x3)g_World);
    
    float3 viewVec = g_EyePos - Out.worldPos;
    Out.tangentSpaceViewVec = mul(float3x3(Out.worldTangent, Out.worldBinormal, Out.worldNormal), viewVec);
    
    return Out;
}

float3 LightPos0 : POSITION
<
    string Object = "PointLight";
    int ObjectIndex = 0;
>;

float4 LightDiff0 : DIFFUSE
<
    string Object = "PointLight";
    int ObjectIndex = 0;
>;

float4 LightAmbi0 : AMBIENT
<
    string Object = "PointLight";
    int ObjectIndex = 0;
>;

float3 LightAtten0 : ATTENUATION
<
    string Object = "PointLight";
    int ObjectIndex = 0;
>;

float3 LightPos1 : POSITION
<
    string Object = "PointLight";
    int ObjectIndex = 1;
>;

float4 LightDiff1 : DIFFUSE
<
    string Object = "PointLight";
    int ObjectIndex = 1;
>;

float4 LightAmbi1 : AMBIENT
<
    string Object = "PointLight";
    int ObjectIndex = 1;
>;

float3 LightAtten1 : ATTENUATION
<
    string Object = "PointLight";
    int ObjectIndex = 1;
>;

float3 LightPos2 : POSITION
<
    string Object = "PointLight";
    int ObjectIndex = 2;
>;

float4 LightDiff2 : DIFFUSE
<
    string Object = "PointLight";
    int ObjectIndex = 2;
>;

float4 LightAmbi2 : AMBIENT
<
    string Object = "PointLight";
    int ObjectIndex = 2;
>;

float3 LightAtten2 : ATTENUATION
<
    string Object = "PointLight";
    int ObjectIndex = 2;
>;

float3 LightPos3 : POSITION
<
    string Object = "PointLight";
    int ObjectIndex = 3;
>;

float4 LightDiff3 : DIFFUSE
<
    string Object = "PointLight";
    int ObjectIndex = 3;
>;

float4 LightAmbi3 : AMBIENT
<
    string Object = "PointLight";
    int ObjectIndex = 3;
>;

float3 LightAtten3 : ATTENUATION
<
    string Object = "PointLight";
    int ObjectIndex = 3;
>;

float3 LightPos4 : POSITION
<
    string Object = "PointLight";
    int ObjectIndex = 4;
>;

float4 LightDiff4 : DIFFUSE
<
    string Object = "PointLight";
    int ObjectIndex = 4;
>;

float4 LightAmbi4 : AMBIENT
<
    string Object = "PointLight";
    int ObjectIndex = 4;
>;

float3 LightAtten4 : ATTENUATION
<
    string Object = "PointLight";
    int ObjectIndex = 4;
>;

float3 g_MaterailDiffuse : MaterialDiffuse;
float3 g_MaterialAmbient : MaterialAmbient;

texture BaseMap
<
    string NTM = "base";
>;

texture NormalMap
<
    string NTM = "normal";
>;

texture ParallaxMap
<
    string NTM = "parallax";
>;

texture GlowMap
<
    string NTM = "glow";
>;

sampler BaseSampler = sampler_state
{
    Texture = (BaseMap);
};

sampler NormalSampler = sampler_state
{
    Texture = (NormalMap);
};

sampler ParallaxSampler = sampler_state
{
    Texture = (ParallaxMap);
};

sampler GlowSampler = sampler_state
{
    Texture = (GlowMap);
};

float3 PointLight(float3 worldPos, float3 worldNrm, float3 lightPos, float3 lightDiff)
{
    float3 LightVector = normalize(lightPos - worldPos);
        
    // Take N dot L as intensity.
    float LightNDotL = dot(LightVector, worldNrm);
    float LightIntensity = max(0, LightNDotL);

    return lightDiff * LightIntensity;
}

float4 PS(Output In) : COLOR0
{
    float3 worldNormal = normalize(In.worldNormal);
    float3 worldBinormal = normalize(In.worldBinormal);
    float3 worldTangent = normalize(In.worldTangent);
    float3 tangentSpaceViewVec = normalize(In.tangentSpaceViewVec);
    
    float2 parallax = tex2D(ParallaxSampler, In.tex0);
    
    // Calculate offset scaling constant bias.
    float offsetScale = 0.05;
    float2 bias = float2(offsetScale, offsetScale) * -0.5;

    // Calculate offset
    float2 offset = parallax * offsetScale + bias;

    float2 tex = In.tex0 + offset * tangentSpaceViewVec.xy;
    float4 baseColor = tex2D(BaseSampler, tex);
    float4 glowColor = tex2D(GlowSampler, tex);
    
    float4 normalColor = tex2D(NormalSampler, tex);
    normalColor = normalColor * 2.0 - 1.0;
    
    float3x3 xForm = float3x3(worldTangent, worldBinormal, worldNormal);
    xForm = transpose(xForm);
    float3 normal = mul(xForm, normalColor);
    normal = normalize(normal);
    
    float3 color = PointLight(In.worldPos, normal, LightPos0, LightDiff0);
    color += PointLight(In.worldPos, normal, LightPos1, LightDiff1);
    color += PointLight(In.worldPos, normal, LightPos2, LightDiff2);
    color += PointLight(In.worldPos, normal, LightPos3, LightDiff3);
    color += PointLight(In.worldPos, normal, LightPos4, LightDiff4);
    
    color = color*baseColor + glowColor;
    return float4(color, baseColor.a);
}

technique GlowNormal
<
    string description = "Shader for Glow and Normal. ";
    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
    string NBTMethod = "NDL";
    int NBTSourceUV = 0;
>
{
    pass Single_Pass
    {
        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 PS();
    }
}

