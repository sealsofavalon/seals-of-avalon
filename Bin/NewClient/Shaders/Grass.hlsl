float4   g_ViewPos : register(C0);
float4x4 g_ViewProj : register(C1);
float4 g_LightDiffuse : register(C5);
float4 g_LightAmbient : register(C6);
float4 g_Params : register(C7);
float4 g_TexCoords[16*4] : register(C8);

static const float4 g_CornerOffset[8] =
{
    float4(-1.f,  0.f, -0.1f, 0.f),
    float4(-1.f,  0.f,  2.0f, 0.f),
    float4( 1.f,  0.f,  2.0f, 0.f),
    float4( 1.f,  0.f, -0.1f, 0.f),
    float4( 0.f, -1.f, -0.1f, 0.f),
    float4( 0.f, -1.f,  2.0f, 0.f),
    float4( 0.f,  1.f,  2.0f, 0.f),
    float4( 0.f,  1.f, -0.1f, 0.f),
};

static const float4 g_Offset[8] =
{
    float4( 0.333f,  0.333f, 0.f, 0.f),
    float4( 0.333f,  0.000f, 0.f, 0.f),
    float4( 0.333f, -0.333f, 0.f, 0.f),
    float4( 0.000f,  0.333f, 0.f, 0.f),
    float4( 0.000f, -0.333f, 0.f, 0.f),
    float4(-0.333f,  0.333f, 0.f, 0.f),
    float4(-0.333f,  0.000f, 0.f, 0.f),
    float4(-0.333f, -0.333f, 0.f, 0.f),
};

struct InputVS
{
    float4 pos : POSITION;    
    float4 grass : TEXCOORD0; //x:size, y:rot, z:corner, w:texcoords    
    float offset : TEXCOORD1;
};

struct OutputVS
{
    float4 hpos : POSITION;
    float4 diffuse : COLOR0;
    float2 tex0 : TEXCOORD0;
};

float2 RotateVector(float2 vec, float theta)
{
    float c = cos(theta);
    float s = sin(theta);
    return float2(vec.x*c - vec.y*s, vec.x*s + vec.y*c); 
}

OutputVS GrassVS(InputVS In)
{
    OutputVS Out;
    float size = In.grass.x;
    float rot = In.grass.y;
    float corner = In.grass.z;
    float tex = In.grass.w;
    float fogDensity = g_Params.x;
    float time = g_Params.y;
    
    float4 cornerOffset = g_CornerOffset[corner]*size;
    cornerOffset.xy = RotateVector(cornerOffset.xy, rot);
    cornerOffset.xy += (cornerOffset.z + 0.1f)*cos(time)*0.08f;
    
    float4 worldPos = In.pos - g_ViewPos + g_Offset[In.offset] + cornerOffset; //原点和摄像机重合
    
    Out.hpos = mul(worldPos, g_ViewProj);
    Out.tex0 = g_TexCoords[tex];
    Out.diffuse = (g_LightDiffuse + g_LightAmbient)*0.5f;
    
    return Out;
}

sampler g_Grass : register(S0);
float4 GrassPS(OutputVS In) : COLOR0
{
    float4 baseColor = tex2D(g_Grass, In.tex0);
    float4 outColor = In.diffuse*baseColor;
    
    return outColor;
}