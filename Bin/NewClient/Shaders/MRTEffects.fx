
//---------------------------------------------------------------------------
// Textures and Samplers
//---------------------------------------------------------------------------
texture g_texSrcColor0;
sampler2D g_samSrcColor0 = sampler_state
{
    Texture = (g_texSrcColor0);
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Linear;
    MipFilter = Linear;
};

texture g_texSrcColor1;
sampler2D g_samSrcColor1 = sampler_state
{
    Texture = (g_texSrcColor1);
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Linear;
    MipFilter = Linear;
};

//---------------------------------------------------------------------------
// Constants
//---------------------------------------------------------------------------

float4x4 g_matWorldViewProj : WORLDVIEWPROJECTION;

//---------------------------------------------------------------------------
// Vertex Shaders
//---------------------------------------------------------------------------
struct VS_OUTPUT
{
    float4 Pos      : POSITION;
    float2 Tex0     : TEXCOORD0;
};

VS_OUTPUT VSMain(float4 inPos: POSITION, float2 inTex0: TEXCOORD0)
{
    VS_OUTPUT Out;
    Out.Pos = mul(inPos, g_matWorldViewProj);
    Out.Tex0 = inTex0;
    return Out;
}

static float2 samples[12] = 
{
    -0.326212, -0.405805,
    -0.840144, -0.073580,
    -0.695914,  0.457137,
    -0.203345,  0.620716,
    0.962340, -0.194983,
    0.473434, -0.480026,
    0.519456,  0.767022,
    0.185461, -0.893124,
    0.507431,  0.064425,
    0.896420,  0.412458,
    -0.321940, -0.932615,
    -0.791559, -0.597705,
};

float4 blur_PS(float2 vTexCoord: TEXCOORD0) : COLOR0
{	
   float4 sum = tex2D(g_samSrcColor0, vTexCoord);
   for(int i = 0; i < 12; i++)
   {
      sum += tex2D(g_samSrcColor0, vTexCoord + 0.025 * samples[i]);
   }
   return sum / 13;
}

static float Luminance = 0.08f;
static const float fMiddleGray = 0.18f;
static const float fWhiteCutoff = 0.8f;

float4 Bright_PS(float2 vTexCoord: TEXCOORD0) : COLOR0
{
    float3 ColorOut = tex2D(g_samSrcColor0, vTexCoord);

    ColorOut *= fMiddleGray / ( Luminance + 0.001f );
    ColorOut *= ( 1.0f + ( ColorOut / ( fWhiteCutoff * fWhiteCutoff ) ) );
    ColorOut -= 5.0f;

    ColorOut = max( ColorOut, 0.0f );

    ColorOut /= ( 10.0f + ColorOut );

    return float4( ColorOut, 1.0f );
}

float4 Combine_PS(float2 vTexCoord : TEXCOORD0) : COLOR0
{
    //ʹ��Bright Passʱ
    return tex2D(g_samSrcColor0, vTexCoord)*0.4 +
           tex2D(g_samSrcColor1, vTexCoord);

    //return tex2D(g_samSrcColor0, vTexCoord)*0.4 +
    //       tex2D(g_samSrcColor1, vTexCoord)*0.6;
}


//---------------------------------------------------------------------------
// Techniques
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
technique MRT_Blur
{
    pass p0
    {
        VertexShader = compile vs_2_0 VSMain();
        PixelShader = compile ps_2_0 blur_PS();
        ZEnable = false;
        AlphaBlendEnable = false;
    }
}

//-----------------------------------------------------------------------------
technique MRT_Bright
{
    pass p0
    {
        VertexShader = compile vs_2_0 VSMain();
        PixelShader = compile ps_2_0 Bright_PS();
        ZEnable = false;
        AlphaBlendEnable = false;
    }
}

//-----------------------------------------------------------------------------
technique MRT_Combine
{
    pass p0
    {
        VertexShader = compile vs_2_0 VSMain();
        PixelShader = compile ps_2_0 Combine_PS();
        ZEnable = false;
        AlphaBlendEnable = false;
    }
}