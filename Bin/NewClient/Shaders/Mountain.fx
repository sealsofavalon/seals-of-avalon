
float4x4 g_WorldViewProj : WorldViewProj;
float3 g_MaterialEmissive : MaterialEmissive;

struct Input
{
    float4 pos : POSITION;
    float2 tex0 : TEXCOORD0;
};

struct Output
{
    float4 hpos : POSITION;
    float4 diffuse : COLOR0;
    float2 tex0 : TEXCOORD0;
};

Output MountainVS(Input In)
{
    Output Out;
    Out.hpos = mul(In.pos, g_WorldViewProj);
    Out.tex0 = In.tex0;
                        
    Out.diffuse.rgb = g_MaterialEmissive;
    Out.diffuse.a = 1.f;
    return Out;
}


// textures
texture BaseMap
<
    string NTM = "base";
>;

// Samplers
sampler BaseSampler = sampler_state
{
    Texture = (BaseMap);
    MINFILTER = LINEAR;
    MIPFILTER = LINEAR;
    MAGFILTER = LINEAR;
};

float4 MountainPS(float4 diffuse : COLOR0, float2 tex0: TEXCOORD0) : COLOR0
{
    float4 baseColor = tex2D(BaseSampler, tex0);
    return float4(diffuse.rgb, baseColor.a);
}

technique FogMountain
<
    string description = "FogMountain";
    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
>
{
    pass P0
    {
        VertexShader = compile vs_2_0 MountainVS();
        PixelShader = compile ps_2_0 MountainPS();
    }
}

