float4x4 g_WorldViewProj : WorldViewProj;
float3 g_MaterialDiffuse : MaterialDiffuse;

struct Input
{
    float4 pos : POSITION;
};

struct Output
{
    float4 hpos : POSITION;
    float4 color : COLOR0;
};

Output VS(Input In)
{
    Output Out;
    Out.hpos = mul(In.pos, g_WorldViewProj);
    Out.color = float4(g_MaterialDiffuse, 1.f);
        
    return Out;
}

float4 PS(float4 color : COLOR0) : COLOR0
{    
    return color;
}

technique Shadow
<
    string Description = "Shadow";

    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
>
{
    pass P0
    {
        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 PS();		
    }
}