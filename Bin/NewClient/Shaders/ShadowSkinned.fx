static const int MAX_BONES = 30;
float4x4 g_SkinWorldViewProj : SkinWorldViewProj;
float4x3 g_BoneMatrix3[MAX_BONES] : BoneMatrix3;
float3 g_MaterialDiffuse : MaterialDiffuse;

struct Input
{
    float4 pos : POSITION;
    float4 blendWeights : BLENDWEIGHT;
    float4 blendIndices : BLENDINDICES;
};

struct Output
{
    float4 hpos : POSITION;
    float4 color : COLOR0;
};

Output VS(Input In)
{
    Output Out;
        
    // Compensate for lack of UBYTE4 on Geforce3
    int4 indices = D3DCOLORtoUBYTE4(In.blendIndices);

    // Calculate normalized fourth bone weight
    float weight3 = 1.0f - In.blendWeights[0] - In.blendWeights[1] - In.blendWeights[2];
    
    float4x3 BoneTransform;
    BoneTransform  = g_BoneMatrix3[indices[0]] * In.blendWeights[0];
    BoneTransform += g_BoneMatrix3[indices[1]] * In.blendWeights[1];
    BoneTransform += g_BoneMatrix3[indices[2]] * In.blendWeights[2];
    BoneTransform += g_BoneMatrix3[indices[3]] * weight3;
    
    float3 pos = mul(In.pos, BoneTransform);
    Out.hpos = mul(float4(pos, 1.f), g_SkinWorldViewProj);
    Out.color = float4(g_MaterialDiffuse, 1.f);
        
    return Out;
}

float4 PS(float4 color : COLOR0) : COLOR0
{
    return color;
}

technique ShadowSkinned
<
    string Description = "ShadowSkinned";

    bool UsesNiRenderState = true;
    bool UsesNiLightState = false;
    int BonesPerPartition = MAX_BONES;
>
{
    pass P0
    {
        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 PS();		
    }
}