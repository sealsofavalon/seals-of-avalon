static const float4 g_ShadowColor = { 0.f, 0.f, 0.f, 1.f };
static const float g_MaterialPower = 30.f;
static const float3 LIGHT_DIR = { 0.57735f, 0.57735f, -0.57735f };

float4 g_World : register(C0);
float4x4 g_ViewProj : register(C1);
float4 g_LightDiffuse : register(C5);
float4 g_LightAmbient : register(C6);

struct InputVS
{
    float4 pos : POSITION;
    float3 normal : NORMAL;
};

struct OutputVS
{
    float4 hpos : POSITION;
    float4 diffuse : TEXCOORD2;
    float4 specular : COLOR1;
    float2 tex0 : TEXCOORD0;
    float2 tex1 : TEXCOORD1;
};

OutputVS TerrainVS(InputVS In)
{
    OutputVS Out;
    float4 worldPos = In.pos + g_World;
    Out.hpos = mul(worldPos, g_ViewProj);
    Out.tex0 = float2(In.pos.x/4.f, In.pos.y/4.f);
    Out.tex1 = float2(In.pos.x/32.f, In.pos.y/32.f);
                    
    //light
    //float intensity = max(0.f, dot(-LIGHT_DIR, In.normal));
    float intensity = dot(-LIGHT_DIR, In.normal);
    if( intensity < 0.0f )
	intensity = -intensity*0.5f;		
    Out.diffuse.xyz = (g_LightDiffuse*intensity*2.0f + g_LightAmbient*0.4f)*1.1f;
    Out.diffuse.w = 1.f;
    
    //specular
    float3 halfVector = -LIGHT_DIR - normalize(worldPos.xyz); //-LIGHT_DIR + normalize(-worldPos.xyz)
    halfVector = normalize(halfVector);
    float lightNDotH = max(0, dot(In.normal, halfVector));
    intensity = pow(lightNDotH, g_MaterialPower);
    float specularMultiplier = lightNDotH > 0.0 ? 1.0 : 0.0;
    intensity = intensity * specularMultiplier;
    
    Out.specular = intensity * 0.6f;
        
    return Out;
}

sampler layer0Sampler : register(S0);
sampler layer1Sampler : register(S1);
sampler layer2Sampler : register(S2);
sampler layer3Sampler : register(S3);
sampler layer4Sampler : register(S4);
sampler layer5Sampler : register(S5);
sampler blend0Sampler : register(S6);
sampler blend1Sampler : register(S7);

float2 g_UVScale0 : register(C0);
float2 g_UVScale1 : register(C1);
float2 g_UVScale2 : register(C2);
float2 g_UVScale3 : register(C3);
float2 g_UVScale4 : register(C4);
float2 g_UVScale5 : register(C5);

float Gray(float4 color)
{
    return (color.r*0.30 + color.g*0.59 + color.b*0.11)*color.a;
    //return (color.r*0.27 + color.g*0.67 + color.b*0.06)*color.a;
}

float4 TerrainPS0(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);
    
    float4 color = layer0;
    
    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}

float4 TerrainPS0_S(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);	
    float4 blend0 = tex2D(blend0Sampler, In.tex1); //Format A8
    
    float4 color = layer0;
                
    color = lerp(color, g_ShadowColor, min(blend0.a, 0.3f));
    
    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}

float4 TerrainPS1(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);
    float4 layer1 = tex2D(layer1Sampler, In.tex0*g_UVScale1);
    
    float4 blend0 = tex2D(blend0Sampler, In.tex1); //Format A8
    
    float4 color = layer0;
    color = lerp(color, layer1, blend0.a);
    
    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}

float4 TerrainPS1_S(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);
    float4 layer1 = tex2D(layer1Sampler, In.tex0*g_UVScale1);
    
    float4 blend0 = tex2D(blend0Sampler, In.tex1); //Format A8
    float4 blend1 = tex2D(blend1Sampler, In.tex1); //Format A8
    
    float4 color = layer0;
    color = lerp(color, layer1, blend0.a);
    color = lerp(color, g_ShadowColor, min(blend1.a, 0.3f));
    
    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}

float4 TerrainPS2(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);		
    float4 layer1 = tex2D(layer1Sampler, In.tex0*g_UVScale1);		
    float4 layer2 = tex2D(layer2Sampler, In.tex0*g_UVScale2);
    
    float4 blend0 = tex2D(blend0Sampler, In.tex1); //Format A8
    float4 blend1 = tex2D(blend1Sampler, In.tex1); //Format A8
    
    float4 color = layer0;
            
    color = lerp(color, layer1, blend0.a);
    color = lerp(color, layer2, blend1.a);
    
    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}

float4 TerrainPS2_S(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);		
    float4 layer1 = tex2D(layer1Sampler, In.tex0*g_UVScale1);		
    float4 layer2 = tex2D(layer2Sampler, In.tex0*g_UVScale2);
    
    float4 blend0 = tex2D(blend0Sampler, In.tex1); //Format R8G8B8
    
    float4 color = layer0;
    
    color = lerp(color, layer1, blend0.r);
    color = lerp(color, layer2, blend0.g);
    color = lerp(color, g_ShadowColor, min(blend0.b, 0.3f));		
    
    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}

float4 TerrainPS3(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);		
    float4 layer1 = tex2D(layer1Sampler, In.tex0*g_UVScale1);		
    float4 layer2 = tex2D(layer2Sampler, In.tex0*g_UVScale2);
    float4 layer3 = tex2D(layer3Sampler, In.tex0*g_UVScale3);
    
    float4 blend0 = tex2D(blend0Sampler, In.tex1); //Format R8G8B8
    
    float4 color = layer0;
            
    color = lerp(color, layer1, blend0.r);
    color = lerp(color, layer2, blend0.g);
    color = lerp(color, layer3, blend0.b);
    
    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}

float4 TerrainPS3_S(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);
    float4 layer1 = tex2D(layer1Sampler, In.tex0*g_UVScale1);
    float4 layer2 = tex2D(layer2Sampler, In.tex0*g_UVScale2);
    float4 layer3 = tex2D(layer3Sampler, In.tex0*g_UVScale3);
    
    float4 blend0 = tex2D(blend0Sampler, In.tex1); //Format R8G8B8A8
    
    float4 color = layer0;
    
    color = lerp(color, layer1, blend0.r);
    color = lerp(color, layer2, blend0.g);
    color = lerp(color, layer3, blend0.b);
    color = lerp(color, g_ShadowColor, min(blend0.a, 0.3f));
    
    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}

float4 TerrainPS4(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);
    float4 layer1 = tex2D(layer1Sampler, In.tex0*g_UVScale1);
    float4 layer2 = tex2D(layer2Sampler, In.tex0*g_UVScale2);
    float4 layer3 = tex2D(layer3Sampler, In.tex0*g_UVScale3);
    float4 layer4 = tex2D(layer4Sampler, In.tex0*g_UVScale4);
    
    float4 blend0 = tex2D(blend0Sampler, In.tex1); //Format R8G8B8A8
    
    float4 color = layer0;
            
    color = lerp(color, layer1, blend0.r);
    color = lerp(color, layer2, blend0.g);
    color = lerp(color, layer3, blend0.b);
    color = lerp(color, layer4, blend0.a);

    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}

float4 TerrainPS4_S(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);
    float4 layer1 = tex2D(layer1Sampler, In.tex0*g_UVScale1);
    float4 layer2 = tex2D(layer2Sampler, In.tex0*g_UVScale2);
    float4 layer3 = tex2D(layer3Sampler, In.tex0*g_UVScale3);
    float4 layer4 = tex2D(layer4Sampler, In.tex0*g_UVScale4);
    
    float4 blend0 = tex2D(blend0Sampler, In.tex1); //Format R8G8B8A8
    float4 blend1 = tex2D(blend1Sampler, In.tex1); //Format A8
    
    float4 color = layer0;
    
    color = lerp(color, layer1, blend0.r);
    color = lerp(color, layer2, blend0.g);
    color = lerp(color, layer3, blend0.b);
    color = lerp(color, layer4, blend0.a);
    color = lerp(color, g_ShadowColor, min(blend1.a, 0.3f));

    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}

float4 TerrainPS5(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);
    float4 layer1 = tex2D(layer1Sampler, In.tex0*g_UVScale1);
    float4 layer2 = tex2D(layer2Sampler, In.tex0*g_UVScale2);
    float4 layer3 = tex2D(layer3Sampler, In.tex0*g_UVScale3);
    float4 layer4 = tex2D(layer4Sampler, In.tex0*g_UVScale4);
    float4 layer5 = tex2D(layer5Sampler, In.tex0*g_UVScale5);
    
    float4 blend0 = tex2D(blend0Sampler, In.tex1); //Format R8G8B8A8
    float4 blend1 = tex2D(blend1Sampler, In.tex1); //Format A8
    
    float4 color = layer0;
            
    color = lerp(color, layer1, blend0.r);
    color = lerp(color, layer2, blend0.g);
    color = lerp(color, layer3, blend0.b);
    color = lerp(color, layer4, blend0.a);
    color = lerp(color, layer5, blend1.a);

    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}

float4 TerrainPS5_S(OutputVS In) : COLOR0
{
    float4 layer0 = tex2D(layer0Sampler, In.tex0*g_UVScale0);
    float4 layer1 = tex2D(layer1Sampler, In.tex0*g_UVScale1);
    float4 layer2 = tex2D(layer2Sampler, In.tex0*g_UVScale2);
    float4 layer3 = tex2D(layer3Sampler, In.tex0*g_UVScale3);
    float4 layer4 = tex2D(layer4Sampler, In.tex0*g_UVScale4);
    float4 layer5 = tex2D(layer5Sampler, In.tex0*g_UVScale5);
    
    float4 blend0 = tex2D(blend0Sampler, In.tex1); //Format R8G8B8
    float4 blend1 = tex2D(blend1Sampler, In.tex1); //Format R8G8B8
    
    float4 color = layer0;
    
    color = lerp(color, layer1, blend0.r);
    color = lerp(color, layer2, blend0.g);
    color = lerp(color, layer3, blend0.b);
    color = lerp(color, layer4, blend1.r);
    color = lerp(color, layer5, blend1.g);
    color = lerp(color, g_ShadowColor, min(blend1.b, 0.3f));

    color = In.diffuse*color + In.specular*Gray(color);
    color.a = 1.f;
    return color;
}