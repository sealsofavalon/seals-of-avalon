float4 g_World : register(C0);
float4x4 g_ViewProj : register(C1);
float4 g_LightDiffuse : register(C5);
float4 g_LightAmbient : register(C6);
float g_TextureScale : register(C7); 
float g_Time : register(C8);
float2 g_WaterBase : register(C9);
float3 g_EyeDir : register(C10);

static const float shine = 128.0f;
static const float distortion = 0.015;
static const float refraction = 0.009;
static const float3 LIGHT_DIR = { 0.57735f, 0.57735f, -0.57735f };

struct InputVS
{
    float4 pos : POSITION;
};

struct OutputVS
{
    float4 hpos : POSITION;
    float4 diffuse : COLOR0;
    float2 tex0 : TEXCOORD0;
    float2 tex1 : TEXCOORD1;
};

struct RROutputVS
{
    float4 hpos : POSITION;
    float4 diffuse : COLOR0;
    float2 normCoords : TEXCOORD0;
    float2 refrCoords : TEXCOORD1;
    float4 proj : TEXCOORD2;
    float3 viewTangetSpace : TEXCOORD3;
    float2 depthCoords : TEXCOORD4;
};

OutputVS WaterVS(InputVS In)
{
    OutputVS Out;
    Out.hpos = mul(In.pos + g_World, g_ViewProj);
    Out.tex0 = float2(In.pos.x/2.f, In.pos.y/2.f)*g_TextureScale;
    Out.tex1 = float2(In.pos.x/32.f, In.pos.y/32.f);
    Out.diffuse = g_LightDiffuse*0.57735f + g_LightAmbient;
    
    return Out;
}

RROutputVS RRWaterVS(InputVS In)
{
    RROutputVS Out;
    float4 worldPos = In.pos + g_World;
    float4 hpos = mul(worldPos, g_ViewProj);
    Out.hpos = hpos;
    Out.diffuse = g_LightDiffuse;

    float2 tex = (g_WaterBase + In.pos.xy)*(1.f/4.f);
        
    // Scale texture coordinates to get mix of low/high frequency details
    Out.normCoords = float2(tex.x, tex.y + fmod(g_Time, 1000)*0.25)*0.25f;
    Out.refrCoords = float2(tex.x, tex.y - fmod(g_Time, 1000)*0.25);
    Out.depthCoords = float2(In.pos.x/32.f, In.pos.y/32.f);
    Out.viewTangetSpace = -worldPos;
    
    Out.proj.xy = (hpos.w - hpos.xy)*0.5f;
    Out.proj.z = (hpos.w + hpos.x)*0.5f;
    Out.proj.w = hpos.w;
    
    return Out;
}

float4 RefractWaterVS(float4 pos : POSITION) : POSITION
{
    return mul(pos + g_World, g_ViewProj);
}

sampler baseMap : register(S0);
sampler depthMap0 : register(S1);

sampler refrMap : register(S0);
sampler reflMap : register(S1);
sampler refrAlphaMap : register(S2);
sampler normalMap : register(S3);
sampler dudvMap : register(S4);
sampler depthMap1 : register(S5);

float4 WaterPS(OutputVS In) : COLOR0
{
    float4 baseColor = tex2D(baseMap, In.tex0);   
    float4 depthColor = tex2D(depthMap0, In.tex1);
    float4 outColor = In.diffuse*baseColor;

    outColor.a = outColor.a * depthColor.a;
 
    return outColor;
}

float4 AdditiveWaterPS(OutputVS In) : COLOR0
{
    float4 baseColor = tex2D(baseMap, In.tex0);
    float4 depthColor = tex2D(depthMap0, In.tex1);
    float4 outColor = In.diffuse + baseColor;

    outColor.a = In.diffuse.a * baseColor.a;
    outColor.a = outColor.a * depthColor.a;

    return outColor;
}

float4 RRWaterPS(RROutputVS In) : COLOR0
{
    float2 distOffset = tex2D(dudvMap, In.normCoords) * distortion;
    float2 tex = In.refrCoords + distOffset;
    float4 dudvColor = tex2D(dudvMap, tex);
    dudvColor = normalize(dudvColor * 2.f - 1.f) * refraction;

    float4 depthColor = tex2D(depthMap1, In.depthCoords);
    
    float3 normalVector = tex2D(normalMap, tex) * 2.f - 1.f;
    float3 lightReflection = normalize( reflect(LIGHT_DIR, normalVector) );
    
    float invFresnel = dot(normalVector, lightReflection);

    float4 tex1 = In.proj / In.proj.w;
    float4 projCoord = tex1 + dudvColor;
    projCoord = clamp(projCoord, 0.0001f, 0.999f);
    
    float3 refl = tex2D(reflMap, projCoord.xy);
    float3 refrA = tex2D(refrMap, tex1.zy + dudvColor.zy*0.3f );
    float3 refrZ = tex2D(refrMap, tex1.zy - dudvColor.zy*0.3f );
    refrA = lerp(refrA, refrZ, 0.5);
    float3 refrB = tex2D(refrMap, tex1.zy);

    float refrAlpha = tex2D(refrAlphaMap, projCoord.zy);
    float3 refr = lerp(refrA, refrB, refrAlpha);
    
    float3 localView = normalize(In.viewTangetSpace);
    float lightNDotH = max(0, dot(lightReflection, localView));
    float specularMultiplier = lightNDotH > 0 ? 1.0 : 0.0;
    float3 specular = pow(lightNDotH, shine) * specularMultiplier;

    float4 outColor;	
    outColor.rgb = lerp(refl, refr*In.diffuse, invFresnel) + specular;

    outColor.a = In.diffuse.a * depthColor.a;
    
    return outColor;
}

float4 RefractWaterPS() : COLOR
{
    return 0.f;
}