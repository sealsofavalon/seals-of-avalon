local PlayerMapA = 0;
local PlayerMapB = 0;
local RaidInstance;
local SideARace = 0;
local SideBRace = 0;
local StartFightEvent = 0;
local BlacksmithBanner = 0;
local FarmBanner = 0;
local LumberMillBanner = 0;
local MineBanner = 0;
local StableBanner = 0;
local SideABannerCount = 0;
local SideBBannerCount = 0;
local BannerMap = 0;
local ResurrectEventMap = 0;
local ResourceIncreaseEvent = 0;
local DefaultResurrectPositionA = 0;
local DefaultResurrectPositionB = 0;
local FightStarted = 0;
local BannerIndexMap = 0;
local BannerCaptureEventMap = 0;
local ReversedBannerCaptureEventMap = 0;
local CapturedBannerMap = 0;
local ResourceIncreaseArrayMap = 0;

function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );
	PlayerMapA = RaidInstance:ContainerMapCreate();
	PlayerMapB = RaidInstance:ContainerMapCreate();
	StartFightEvent = RaidInstance:CreateEvent( 45 * 1000, 1 );
	BannerIndexMap = RaidInstance:ContainerMapCreate();
	BannerOwnerMap = RaidInstance:ContainerMapCreate();
	BannerCaptureEventMap = RaidInstance:ContainerMapCreate();
	ReversedBannerCaptureEventMap = RaidInstance:ContainerMapCreate();
	CapturedBannerMap = RaidInstance:ContainerMapCreate();
	ResourceIncreaseArrayMap = RaidInstance:ContainerMapCreate();

	BlacksmithBanner = RaidInstance:SpawnGameObject( 20, 916.0, 1101.0, 136.0, 5.3, 0, 5.0, 10, 1, 1 );
	FarmBanner = RaidInstance:SpawnGameObject( 20, 757.0, 829.0, 39.0, 4.1, 0, 5.0, 10, 1, 1 );
	LumberMillBanner = RaidInstance:SpawnGameObject( 20, 690.0, 1370.0, -15.0, 5.3, 0, 5.0, 10, 1, 1 );
	MineBanner = RaidInstance:SpawnGameObject( 20, 1401.0, 656.0, 81.0, 2.1, 0, 5.0, 10, 1, 1 );
	StableBanner = RaidInstance:SpawnGameObject( 20, 1175.0, 1263.0, 47.0, 4.0, 0, 5.0, 10, 1, 1 );

	RaidInstance:ContainerMapInsert( BannerIndexMap, BlacksmithBanner, 0 );
	RaidInstance:ContainerMapInsert( BannerIndexMap, FarmBanner, 1 );
	RaidInstance:ContainerMapInsert( BannerIndexMap, LumberMillBanner, 2 );
	RaidInstance:ContainerMapInsert( BannerIndexMap, MineBanner, 3 );
	RaidInstance:ContainerMapInsert( BannerIndexMap, StableBanner, 4 );

	RaidInstance:AddBattleGroundBanner( 0, BuildLanguageString( 84 ) );
	RaidInstance:AddBattleGroundBanner( 1, BuildLanguageString( 85 ) );
	RaidInstance:AddBattleGroundBanner( 2, BuildLanguageString( 86 ) );
	RaidInstance:AddBattleGroundBanner( 3, BuildLanguageString( 87 ) );
	RaidInstance:AddBattleGroundBanner( 4, BuildLanguageString( 88 ) );

	RaidInstance:ContainerMapInsert( CapturedBannerMap, BlacksmithBanner, 0 );
	RaidInstance:ContainerMapInsert( CapturedBannerMap, FarmBanner, 0 );
	RaidInstance:ContainerMapInsert( CapturedBannerMap, LumberMillBanner, 0 );
	RaidInstance:ContainerMapInsert( CapturedBannerMap, MineBanner, 0 );
	RaidInstance:ContainerMapInsert( CapturedBannerMap, StableBanner, 0 );

	RaidInstance:ContainerMapInsert( ResourceIncreaseArrayMap, 1, 1 );
	RaidInstance:ContainerMapInsert( ResourceIncreaseArrayMap, 2, 1 );
	RaidInstance:ContainerMapInsert( ResourceIncreaseArrayMap, 3, 2 );
	RaidInstance:ContainerMapInsert( ResourceIncreaseArrayMap, 4, 3 );
	RaidInstance:ContainerMapInsert( ResourceIncreaseArrayMap, 5, 5 );

	BannerMap = RaidInstance:ContainerMapCreate();

	local Position = RaidInstance:CreatePosition( 969.0, 1061.0, 137.0, 2.1 );
	RaidInstance:ContainerMapInsert( BannerMap, BlacksmithBanner, Position );

	Position = RaidInstance:CreatePosition( 763.3, 748.8, 42.9, 3.2 );
	RaidInstance:ContainerMapInsert( BannerMap, FarmBanner, Position );

	Position = RaidInstance:CreatePosition( 638.0, 1403.0, -15.0, 5.3 );
	RaidInstance:ContainerMapInsert( BannerMap, LumberMillBanner, Position );

	Position = RaidInstance:CreatePosition( 1328.0, 702.0, 80.0, 5.4 );
	RaidInstance:ContainerMapInsert( BannerMap, MineBanner, Position );

	Position = RaidInstance:CreatePosition( 1132.0, 1284.0, 50.0, 5.2 );
	RaidInstance:ContainerMapInsert( BannerMap, StableBanner, Position );

	ResurrectEventMap = RaidInstance:ContainerMapCreate();

	DefaultResurrectPositionA = RaidInstance:CreatePosition( 1385.0, 1307.0, 80.5, 2.6 );
	DefaultResurrectPositionB = RaidInstance:CreatePosition( 675.1, 658.7, 80.9, 2.8 );
end

function OnUpdate( TimeElapsed )
end

function OnEnd()
end

function OnUnitDie( ObjectID )
	local Race = RaidInstance:ContainerMapFind( PlayerMapA, ObjectID )
	if Race == 0
	then
		Race = RaidInstance:ContainerMapFind( PlayerMapB, ObjectID );
	end

	if Race > 0
	then
		local EventID = RaidInstance:CreateEvent( 20 * 1000, 1 );
		RaidInstance:ContainerMapInsert( ResurrectEventMap, EventID, ObjectID );
	end
end

function RemoveResurrectEventByPlayer( PlayerID )
	local Count = RaidInstance:ContainerMapBegin( ResurrectEventMap );
	for i = 1, Count, 1
	do
		local Key, Value;
		Key, Value = RaidInstance:ContainerMapNext( ResurrectEventMap );
		if PlayerID == Value
		then
			RaidInstance:KillEvent( Key );
		end
	end
end

function OnPlayerResurrect( PlayerID )
	RemoveResurrectEventByPlayer( PlayerID );
end

function OnPlayerEnterInstance( PlayerID )
	if FightStarted == 0
	then
		RaidInstance:ForceRootPlayer( PlayerID );
	end

	local Race = RaidInstance:GetPlayerRace( PlayerID );
	if SideARace == 0
	then
		SideARace = Race;
		RaidInstance:AddPlayerResurrectPosition( Race, DefaultResurrectPositionA );
	else
		if SideBRace == 0 and SideARace ~= Race
		then
			SideBRace = Race;
			RaidInstance:AddPlayerResurrectPosition( Race, DefaultResurrectPositionB );
		end
	end

	if SideARace == Race
	then
		RaidInstance:ContainerMapInsert( PlayerMapA, PlayerID, Race );
		RaidInstance:TeleportPlayer( PlayerID, 1385.0, 1307.0, 80.5, 2.6 );
	else
		RaidInstance:ContainerMapInsert( PlayerMapB, PlayerID, Race );
		RaidInstance:TeleportPlayer( PlayerID, 675.1, 658.7, 80.9, 2.8 );
	end
	RaidInstance:UpdateBattleResourceToPlayers();
end

function OnPlayerLeaveInstance( PlayerID )
	RemoveResurrectEventByPlayer( PlayerID );

	if SideARace == RaidInstance:GetPlayerRace( PlayerID )
	then
		RaidInstance:ContainerMapErase( PlayerMapA, PlayerID );
	else
		RaidInstance:ContainerMapErase( PlayerMapB, PlayerID );
	end

	RaidInstance:ForceUnrootPlayer( PlayerID );
	RaidInstance:UpdateBattleResourceToPlayers();
end

function OnStartFightEvent( void )
	local PlayerCount = RaidInstance:RandomShufflePlayers();
	for i = 1, PlayerCount, 1
	do
		local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
		if PlayerID > 0
		then
			RaidInstance:ForceUnrootPlayer( PlayerID );
		end
	end
	RaidInstance:SendSystemNotify( BuildLanguageString( 0 ) );
	ResourceIncreaseEvent = RaidInstance:CreateEvent( 10 * 1000, 10000 );
	FightStarted = 1;
end

local SideAReadyToWin = 0;
local SideBReadyToWin = 0;

function OnResourceIncreaseEvent( void )
	local ResourceA = 0;
	local ResourceB = 0;
	if SideABannerCount > 0
	then
		local arg = RaidInstance:ContainerMapFind( ResourceIncreaseArrayMap, SideABannerCount );
		ResourceA = RaidInstance:ModifyBattleGroundResource( SideARace, (2 ^ arg) * 10 + (SideABannerCount + 1) * 20 );
	end

	if SideBBannerCount > 0
	then
		local arg = RaidInstance:ContainerMapFind( ResourceIncreaseArrayMap, SideBBannerCount );
		ResourceB = RaidInstance:ModifyBattleGroundResource( SideBRace, (2 ^ arg) * 10 + (SideBBannerCount + 1) * 20 );
	end

	if SideABannerCount > 0 or SideBBannerCount > 0
	then
		RaidInstance:UpdateBattleResourceToPlayers();
	end

	local WinnerRace = 0;
	local LoserRace = 0;
	local WinnerPlayerMap = 0;
	local LoserPlayerMap = 0;

	if ResourceA >= 4000 and SideAReadyToWin == 0
	then
		SideAReadyToWin = 1;
		RaidInstance:PlaySound( 0, "BattleGround/ReadyToWin.wav" );
	end

	if ResourceB >= 4000 and SideBReadyToWin == 0
	then
		SideBReadyToWin = 1;
		RaidInstance:PlaySound( 0, "BattleGround/ReadyToWin.wav" );
	end

	if ResourceA >= ResourceB and ResourceA >= 5000
	then
		WinnerRace = SideARace;
		WinnerPlayerMap = PlayerMapA;
		LoserRace = SideBRace;
		LoserPlayerMap = PlayerMapB;
	else
		if ResourceB >= ResourceA and ResourceB >= 5000
		then
			WinnerRace = SideBRace;
			WinnerPlayerMap = PlayerMapB;
			LoserRace = SideARace;
			LoserPlayerMap = PlayerMapA;
		end
	end

	if WinnerRace > 0 and LoserRace > 0
	then
		local WinnerCount = RaidInstance:ContainerMapBegin( WinnerPlayerMap );
		if WinnerCount > 0
		then
			for i = 1, WinnerCount, 1
			do
				local PlayerID = RaidInstance:ContainerMapNext( WinnerPlayerMap );
				RaidInstance:ItemReward( PlayerID, 900001003, 3 );
				local cls, lv, hp, mp = RaidInstance:GetPlayerBasicInfo( PlayerID );
				if lv >= 20 and lv <= 39
				then
					RaidInstance:HonorReward( PlayerID, 892 );
				elseif lv >39 and lv <= 59
				then
					RaidInstance:HonorReward( PlayerID, 1248 );
				elseif lv >59 and lv <= 79
				then
					RaidInstance:HonorReward( PlayerID, 2246 );
				elseif lv == 80
				then
					RaidInstance:HonorReward( PlayerID, 4492 );
				end
			end
		end

		local LoserCount = RaidInstance:ContainerMapBegin( LoserPlayerMap );
		if LoserCount > 0
		then
			for i = 1, LoserCount, 1
			do
				local PlayerID = RaidInstance:ContainerMapNext( LoserPlayerMap );
				RaidInstance:ItemReward( PlayerID, 900001003, 1 );
				local cls, lv, hp, mp = RaidInstance:GetPlayerBasicInfo( PlayerID );

				if lv >= 20 and lv <= 39
				then
					RaidInstance:HonorReward( PlayerID, 223 );
				elseif lv >39 and lv <= 59
				then
					RaidInstance:HonorReward( PlayerID, 312 );
				elseif lv >59 and lv <= 79
				then
					RaidInstance:HonorReward( PlayerID, 561 );
				elseif lv == 80
				then
					RaidInstance:HonorReward( PlayerID, 1123 );
				end
			end
		end
		RaidInstance:BattleGroundEndByRace( WinnerRace, LoserRace );
		RaidInstance:KillEvent( ResourceIncreaseEvent );
		FightStarted = 2;
	end
end

function OnResurrectPlayerEvent( EventID )
	local PlayerID = RaidInstance:ContainerMapFind( ResurrectEventMap, EventID );
	if PlayerID > 0
	then
		local Position = RaidInstance:SearchNearestPlayerResurrectPosition( PlayerID );
		if Position > 0
		then
			local x, y, z, o;
			x, y, z, o = RaidInstance:GetPosition( Position );
			RaidInstance:ResurrectPlayer( PlayerID, x, y, z, o );
		end
		RaidInstance:ContainerMapErase( EventID );
	end
end

function OnBannerCaptureEvent( BannerObjectID, BannerIndex )
	RaidInstance:ContainerMapErase( ReversedBannerCaptureEventMap, BannerObjectID );
	local OwnerState = RaidInstance:GetGameObjectFlag( BannerObjectID );
	local Race = 0;
	if OwnerState < 10
	then
		return;
	end

	if OwnerState < 20
	then
		Race = OwnerState - 10;
	end

	if OwnerState > 20 and OwnerState < 30
	then
		Race = OwnerState - 20;
	end

	if OwnerState > 30 and OwnerState < 40
	then
		Race = OwnerState - 30;
	end

	if OwnerState > 40
	then
		Race = OwnerState - 40;
	end

	if Race == 0
	then
		RaidInstance:SendSystemNotify( "function OnBannerCaptureEvent - Fatal Error!" );
		return;
	end

	RaidInstance:ModifyGameObjectFlag( BannerObjectID, Race );
	RaidInstance:BattleGroundBannerCaptured( Race, BannerIndex );
	RaidInstance:ContainerMapModifyValue( CapturedBannerMap, BannerObjectID, Race );

	local Position = RaidInstance:ContainerMapFind( BannerMap, BannerObjectID );
	if Position > 0
	then
		RaidInstance:AddPlayerResurrectPosition( Race, Position );
	end

	if Race == SideARace
	then
		SideABannerCount = SideABannerCount + 1;
	else
		SideBBannerCount = SideBBannerCount + 1;
	end
	RaidInstance:PlaySound( 0, "BattleGround/FlagCaptured.wav" );
	RaidInstance:UpdateBattleResourceToPlayers();
end

function OnEvent( EventID )
	if EventID == StartFightEvent
	then
		OnStartFightEvent( 1 );
	end

	if FightStarted ~= 1
	then
		return;
	end

	if EventID == ResourceIncreaseEvent
	then
		OnResourceIncreaseEvent( 1 );
	end

	local BannerObjectID = RaidInstance:ContainerMapFind( BannerCaptureEventMap, EventID );
	if BannerObjectID > 0
	then
		local BannerIndex = RaidInstance:ContainerMapFind( BannerIndexMap, BannerObjectID );
		OnBannerCaptureEvent( BannerObjectID, BannerIndex );
	else
		if EventID ~= ResourceIncreaseEvent and EventID ~= StartFightEvent
		then
			OnResurrectPlayerEvent( EventID );
		end
	end
end

function OnGameObjectTrigger( GameObjectID, PlayerID )
	if FightStarted ~= 1
	then
		return;
	end

	local Race = RaidInstance:GetPlayerRace( PlayerID );
	local OwnerState = RaidInstance:GetGameObjectFlag( GameObjectID );
	local LastRace = 0;

	if OwnerState > 40
	then
		LastRace = 3;
	end

	if OwnerState > 30 and OwnerState < 40
	then
		LastRace = OwnerState - 30;
	end

	if OwnerState > 20 and OwnerState < 30
	then
		LastRace = OwnerState - 20;
	end

	if OwnerState > 10 and OwnerState < 20
	then
		LastRace = OwnerState - 10;
	end

	if OwnerState > 0 and OwnerState < 10
	then
		LastRace = OwnerState;
	end

	if Race == LastRace
	then
		return;
	end

	local BannerIndex = RaidInstance:ContainerMapFind( BannerIndexMap, GameObjectID );
	local Position = RaidInstance:ContainerMapFind( BannerMap, GameObjectID );

	local CapturedRace = RaidInstance:ContainerMapFind( CapturedBannerMap, GameObjectID );

	if CapturedRace == Race
	then
		RaidInstance:BattleGroundBannerDefended( Race, BannerIndex );
		RaidInstance:ModifyGameObjectFlag( GameObjectID, Race );

		if Position > 0
		then
			RaidInstance:AddPlayerResurrectPosition( Race, Position );
		end

		if Race == SideARace
		then
			SideABannerCount = SideABannerCount + 1;
		else
			SideBBannerCount = SideBBannerCount + 1;
		end
	else
		local NewOwnerState = RaidInstance:BattleGroundBannerSuddenStricken( PlayerID, BannerIndex );
		RaidInstance:ModifyGameObjectFlag( GameObjectID, NewOwnerState );

		local BannerCaptureEvent = RaidInstance:CreateEvent( 120 * 1000, 1 );
		local OldEvent = RaidInstance:ContainerMapFind( ReversedBannerCaptureEventMap, GameObjectID );
		RaidInstance:ContainerMapInsert( BannerCaptureEventMap, BannerCaptureEvent, GameObjectID );
		if OldEvent > 0
		then
			RaidInstance:KillEvent( OldEvent );
			RaidInstance:ContainerMapModifyValue( ReversedBannerCaptureEventMap, GameObjectID, BannerCaptureEvent );
		else
			RaidInstance:ContainerMapInsert( ReversedBannerCaptureEventMap, GameObjectID, BannerCaptureEvent );
		end

		if CapturedRace > 0 and Position > 0
		then
			RaidInstance:RemovePlayerResurrectPosition( CapturedRace, Position );
			if CapturedRace == SideARace
			then
				SideABannerCount = SideABannerCount - 1;
			else
				SideBBannerCount = SideBBannerCount - 1;
			end
		end
	end
	RaidInstance:PlaySound( 0, "BattleGround/FlagTaken.wav" );
	RaidInstance:UpdateBattleResourceToPlayers();
end
