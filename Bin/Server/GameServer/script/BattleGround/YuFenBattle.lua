--Author wf
--6/14/2011
local RaidInstance;
local L_InstanceEnd = 0;
local L_EventList = {};
local L_RemovePowerList = {};
local L_GroupName = {};
L_GroupName[1] = "玄乌队";
L_GroupName[2] = "苍云队";
L_GroupName[3] = "裂天队";

L_RemovePowerList[1] = 1; --玩家
L_RemovePowerList[2] = 10; --守卫
L_RemovePowerList[3] = 500; --将军

local CEvent = {
				New = function(self, DelayTime, count, Func, attach)
				    local Event = {};
				    setmetatable(Event, {__index = self});
				    Event.ID = RaidInstance:CreateEvent( DelayTime, count, attach );
					Event.Count = count;
					Event.CallBack = Func;
					Event.EndCall = nil;
				    L_EventList[Event.ID] = Event;
				    local Ret = L_EventList[Event.ID];
					return Ret;
				 end,

				DoEvent = function(self, Param0, Param1, Param2, Param3, Param4)
				    if self.CallBack ~= nil
				    then
				        self.CallBack(Param0, Param1, Param2, Param3, Param4);
				    end

				    self.Count = self.Count - 1;
				    if self.Count <= 0
				    then
				        if self.EndCall ~= nil
				        then
					        self.EndCall();
				        end
				        RaidInstance:KillEvent(self.ID);
					    L_EventList[self.ID] = nil;
				    end
				end,

				ID = 0,
				Count = 0,
				CallBack = nil,
				EndCall = nil,
				};
local BattleRwardIndex = 4;

local InStanceLevelData = {};
	InStanceLevelData[1]= {lv = 20, entryBoss = {110005101, 110005102, 110005103}, entryconvoy = {110005113, 110005114, 110005115}};
	InStanceLevelData[2]= {lv = 40, entryBoss = {110005104, 110005105, 110005106}, entryconvoy = {110005116, 110005117, 110005118}};
	InStanceLevelData[3]= {lv = 60, entryBoss = {110005107, 110005108, 110005109}, entryconvoy = {110005119, 110005120, 110005121}};
	InStanceLevelData[4]= {lv = 80, entryBoss = {110005110, 110005111, 110005112}, entryconvoy = {110005122, 110005123, 110005124}};


local Creature_Data = {};
	Creature_Data[1] 		   = {BOSS = {}, convoy = {}, ffa = 11};
	Creature_Data[1].BOSS	   = {X = 1441.7, Y = 1169.7, Z = 52.6, O = 2.4};
	Creature_Data[1].convoy[1] = {X = 1443.6, Y = 1179.0, Z = 51.4, O = 2.2};
	Creature_Data[1].convoy[2] = {X = 1428.3, Y = 1177.5, Z = 46.1, O = 2.2};
	Creature_Data[1].convoy[3] = {X = 1431.3, Y = 1163.2, Z = 50.6, O = 2.2};

	Creature_Data[2] 		   = {BOSS = {}, convoy = {}, ffa = 12};
	Creature_Data[2].BOSS 	   = {X = 1276.6, Y = 1448.3, Z = 50.4, O = 0.0};
	Creature_Data[2].convoy[1] = {X = 1265.1, Y = 1447.5, Z = 49.5, O = 0.0};
	Creature_Data[2].convoy[2] = {X = 1284.4, Y = 1446.9, Z = 50.8, O = 0.0};
	Creature_Data[2].convoy[3] = {X = 1277.3, Y = 1430.7, Z = 46.4, O = 0.0};

	Creature_Data[3] 		   = {BOSS = {}, convoy = {}, ffa = 13};
	Creature_Data[3].BOSS 	   = {X = 1113.5, Y = 1166.5, Z = 53.5, O = 4.2};
	Creature_Data[3].convoy[1] = {X = 1119.8, Y = 1160.7, Z = 53.0, O = 4.2};
	Creature_Data[3].convoy[2] = {X = 1128.8, Y = 1175.3, Z = 46.6, O = 4.2};
	Creature_Data[3].convoy[3] = {X = 1116.6, Y = 1181.9, Z = 47.5, O = 4.2};

local BattleReward_Data = {};
	BattleReward_Data[1] = {item = 900001002 ,wincount = 3, losercount = 1,winhonor = 2730, losterhonor =560};
	BattleReward_Data[2] = {item = 900001002 ,wincount = 3, losercount = 1,winhonor = 4200, losterhonor =840};
	BattleReward_Data[3] = {item = 900001002 ,wincount = 3, losercount = 1,winhonor = 5600, losterhonor =1120};
	BattleReward_Data[4] = {item = 900001002 ,wincount = 3, losercount = 1,winhonor = 11340, losterhonor =2268};


function F_GiveBattleReward( plrid, win)
	if BattleReward_Data[BattleRwardIndex] == nil
	then
		return ;
	end
	if win == 1
	then
		RaidInstance:HonorReward(plrid, BattleReward_Data[BattleRwardIndex].winhonor);
		RaidInstance:ItemReward( plrid, BattleReward_Data[BattleRwardIndex].item, BattleReward_Data[BattleRwardIndex].wincount);
	else
		RaidInstance:HonorReward(plrid, BattleReward_Data[BattleRwardIndex].losterhonor);
		RaidInstance:ItemReward( plrid, BattleReward_Data[BattleRwardIndex].item, BattleReward_Data[BattleRwardIndex].losercount);
	end
end

local L_CreatureMgr = {
	Creature_Table = {},
	Creature_Team_Count = 3,-- 当前有几组怪
	Creature_Team_Convoy_Count = 3, -- 一组怪中的小弟个数



	-- 添加怪物
	F_Add_Creature = function(self, index, ffa)
		if self.Creature_Table[index] == nil
		then
			local entry = InStanceLevelData[BattleRwardIndex].entryBoss[index];
			local X = Creature_Data[index].BOSS.X;
			local Y = Creature_Data[index].BOSS.Y;
			local Z = Creature_Data[index].BOSS.Z;
			local O = Creature_Data[index].BOSS.O;

			self.Creature_Table[index] = {BOSS = 0, convoy = {}};
			self.Creature_Table[index].BOSS = RaidInstance:SpawnCreature( entry,X,Y,Z,O,0);
			RaidInstance:ModifyUnitPVPFlag(self.Creature_Table[index].BOSS, ffa);

			for i = 1, self.Creature_Team_Convoy_Count,1
			do
				--RaidInstance:SendSystemNotify("创建怪物@！！");

				entry = InStanceLevelData[BattleRwardIndex].entryconvoy[index];
				X = Creature_Data[index].convoy[i].X;
				Y = Creature_Data[index].convoy[i].Y;
				Z = Creature_Data[index].convoy[i].Z;
				O = Creature_Data[index].convoy[i].O;
				self.Creature_Table[index].convoy[i] = 0;
				self.Creature_Table[index].convoy[i] = RaidInstance:SpawnCreature( entry,X,Y,Z,O,0);
				RaidInstance:ModifyUnitPVPFlag(self.Creature_Table[index].convoy[i], ffa);

			end
		end

		return true;
	end,

	--初始化数据
	F_InitMgr = function(self ,  Team_Count, Convoy_Count)

		self.Creature_Team_Count = Team_Count;
		self.Creature_Team_Convoy_Count = Convoy_Count;

		for i = 1, self.Creature_Team_Count, 1
		do
			self:F_Add_Creature(i, Creature_Data[i].ffa);
		end
	end,

	-- 怪物死亡
	F_Creature_Dead =  function(self, Creature)
		for i = 1, self.Creature_Team_Count, 1
		do
			if self.Creature_Table[i].BOSS == Creature
			then
				self.Creature_Table[i].BOSS = 0;
				return  RaidInstance:RemoveNewBattleGroundPower(i - 1,  L_RemovePowerList[3]), i;
			end
			for j = 1, self.Creature_Team_Convoy_Count,1
			do
				if self.Creature_Table[i].convoy[j] == Creature
				then
					self.Creature_Table[i].convoy[j] = 0;
					return  RaidInstance:RemoveNewBattleGroundPower(i - 1,  L_RemovePowerList[2]), i;
				end
			end
		end

		return 0, 0;
	end

	};

local L_TeamMgr = {
	Team_Table = {},
	Team_Count = 3, -- 最大队伍数目
	Team_Member_Count = 10, -- 队伍最大人数
	ResurrectPos = {}, -- 复活位置列表
	ResurrectPos_Count = 0,
	-- 初始化数据
	F_InitMgr = function(self, TeamCount, MemberCount)

		self.Team_Count = TeamCount;
		self.Team_Member_Count = MemberCount

		for i = 1, self.Team_Count, 1
		do
			self.Team_Table[i] = {Loser = 0, Plr = {}};
			for j = 1, self.Team_Member_Count ,1
			do
				self.Team_Table[i].Plr[j] = 0;
			end
			RaidInstance:AddNewBattleGroundPower(i - 1, 500);
		end
	end,
	--增加复活点
	F_AddResurrectPos = function(self, Teamindex, x, y, z ,o)
		self.ResurrectPos_Count = self.ResurrectPos_Count + 1;
		self.ResurrectPos[self.ResurrectPos_Count] = {px = x, py = y, pz = z, po = o, Team = Teamindex};
		return self.ResurrectPos_Count;
	end,
	-- 搜索玩家附近的复活点位置
	F_GetNearResurrectPos =  function(self, Player)
		local team = self:F_GetPlayerTeam(Player);
		local posx, posy, posz =  RaidInstance:GetObjectPosition(Player);

		local x = 0.0;
		local y = 0.0;
		local z = 0.0;
		local o = 0.0;

		local check = 0;
		local len = 0.0;

		for k, v in ipairs(self.ResurrectPos)
		do
			if v.Team == team
			then
				local newlen  = (v.px - posx) * (v.px - posx)  + (v.py - posy) * (v.py - posy) + (v.pz - posz) * (v.pz - posz);
				if check == 0
				then
					check = 1;
					x = v.px; y = v.py; z = v.pz; o = v.po;
					--RaidInstance:SendSystemNotify( RaidInstance:ConvertInteger2String(x).."//"..RaidInstance:ConvertInteger2String(y).."//"..RaidInstance:ConvertInteger2String(z).."//"..RaidInstance:ConvertInteger2String(o).."//");

					len = newlen;
				else
					if newlen < len
					then
						x = v.px; y = v.py; z = v.pz; o = v.po;
						len = newlen;
					end
				end
			end
		end
		--RaidInstance:SendSystemNotify( RaidInstance:ConvertInteger2String(x).."//"..RaidInstance:ConvertInteger2String(y).."//"..RaidInstance:ConvertInteger2String(z).."//"..RaidInstance:ConvertInteger2String(o).."//");

		return x,y,z,o;
	end,

	F_GetFirstTeleportPos = function(self, team)

		for k, v in ipairs(self.ResurrectPos)
		do
			if v.Team == team
			then
				return v.px, v.py, v.pz, v.po;
			end
		end
		return 0.0,0.0,0.0,0.0;
	end,
	-- 获得玩家所在Team
	F_GetPlayerTeam = function(self, player)
		for i = 1, self.Team_Count, 1
		do
			for j = 1, self.Team_Member_Count ,1
			do
				if self.Team_Table[i].Plr[j] == player
				then
					return i;
				end
			end
		end

		return 0;
	end,
	-- 添加玩家
	F_AddPlr = function(self, plrid, Teamindex)

		if self.Team_Table[Teamindex] ~= nil
		then
			for j = 1, self.Team_Member_Count ,1
			do
				if self.Team_Table[Teamindex].Plr[j] == 0
				then
					self.Team_Table[Teamindex].Plr[j] = plrid;
					local x, y,z,o = self:F_GetFirstTeleportPos(Teamindex);
					--RaidInstance:SendSystemNotify("传送玩家@！！");
					--RaidInstance:SendSystemNotify( RaidInstance:ConvertInteger2String(x).."//"..RaidInstance:ConvertInteger2String(y).."//"..RaidInstance:ConvertInteger2String(z).."//"..RaidInstance:ConvertInteger2String(o).."//");
					RaidInstance:TeleportPlayer( plrid, x, y, z, o);
					return ;
				end
			end
		else
			RaidInstance:SendSystemNotify( "没有找到"..RaidInstance:ConvertInteger2String(Teamindex));
		end
	end,
	-- 玩家死亡 减少该组的战力
	F_PlrDead = function(self, DeadPlr)

		for i = 1, self.Team_Count, 1
		do
			for j = 1, self.Team_Member_Count ,1
			do
				if  self.Team_Table[i].Plr[j] == DeadPlr
				then
					return RaidInstance:RemoveNewBattleGroundPower(i - 1, L_RemovePowerList[1]), i;
				end
			end
		end

		return  0,0;
	end,

	F_CheckIsLoser = function(self, team)
		if self.Team_Table[team] ~= nil
		then
			return self.Team_Table[team].Loser;
		else
			return 1;
		end
	end,

	-- 玩家复活
	F_ResurrectPlr = function(self, plrid)

		local x,y,z,o = self:F_GetNearResurrectPos(plrid);
		RaidInstance:ResurrectPlayer(plrid, x,y,z,o);
	end,

	F_SetTeamLoser = function(self, team, EjectPlayer)
		if self.Team_Table[team] ~= nil and self.Team_Table[team].Loser == 0
		then
			self.Team_Table[team].Loser = 1;

			for i = 1, self.Team_Member_Count, 1
			do
				-- EjectPlayer 是否主动退出。主动退出无法获得奖励
				if self.Team_Table[team].Plr[i] ~= 0 and EjectPlayer == 1
				then
					--发放失败奖励
					F_GiveBattleReward(self.Team_Table[team].Plr[i], 0);
					RaidInstance:EjectPlayerFromInstance(self.Team_Table[team].Plr[i]);
					self.Team_Table[team].Plr[i] = 0;
				end
			end
			RaidInstance:NewBattleGroundLoserTeam(team);
		end

		local losercount = 0;
		local winindex = 0;
		for i = 1, self.Team_Count, 1
		do
			if self.Team_Table[i].Loser == 1
			then
				losercount = losercount + 1;
			else
				winindex = i;
			end
		end

		-- 只剩下一组没被淘汰的话 该组获胜
		if self.Team_Count - losercount <= 1
		then

			if self.Team_Table[winindex] ~= nil
			then
				for j = 1, self.Team_Member_Count, 1
				do
					if self.Team_Table[winindex].Plr[j] ~= 0
					then
						--发放战场胜利奖励
						F_GiveBattleReward(self.Team_Table[winindex].Plr[j], 1);
					end
				end
				RaidInstance:NewBattleGroundEnd(winindex, L_GroupName[winindex].."获得了本场战斗的胜利！");
				L_InstanceEnd = 1;
			end

		end

	end,

	-- 玩家离开战场
	F_RemovePlr = function(self, plrid)
		local checkTeam = 0;
		local NeedCheck = 1;
		for i = 1, self.Team_Count, 1
		do
			for j = 1, self.Team_Member_Count ,1
			do
				if self.Team_Table[i].Plr[j] ~= 0 and self.Team_Table[i].Plr[j] == plrid
				then
					self.Team_Table[i].Plr[j] = 0;
					checkTeam = i;
					break;
				end
			end

			if checkTeam ~= 0
			then
				break;
			end
		end

		if checkTeam ~= 0
		then
			for j = 1, self.Team_Member_Count ,1
			do
				if self.Team_Table[checkTeam].Plr[j] ~= 0
				then
					if self.Team_Table[checkTeam].Plr[j] ~= plrid
					then
						NeedCheck = 0;
						break;
					end
				end
			end
		end

		--RaidInstance:SendSystemNotify( "NeedCheck:"..RaidInstance:ConvertInteger2String(NeedCheck));
		--RaidInstance:SendSystemNotify( "checkTeam:"..RaidInstance:ConvertInteger2String(checkTeam));
		--RaidInstance:SendSystemNotify( "checkTeamLoser:"..RaidInstance:ConvertInteger2String(self:F_CheckIsLoser(checkTeam)));
		if NeedCheck == 1 and checkTeam ~= 0 and self:F_CheckIsLoser(checkTeam) == 0
		then
			RaidInstance:SendSystemNotify( L_GroupName[checkTeam].."放弃了本战场！");
			RaidInstance:RemoveNewBattleGroundPower(checkTeam - 1, L_RemovePowerList[3]);
			RaidInstance:UpdateNewBattleGroundPower(L_GroupName[1], L_GroupName[2], L_GroupName[3]);
			self:F_SetTeamLoser(checkTeam, 0);
		end
	end,
};

function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );

	local MinLv = RaidInstance:GetInstanceMinLevel();
	for i =1, 4, 1
	do
		if InStanceLevelData[i] ~= nil and InStanceLevelData[i].lv == MinLv
		then
			BattleRwardIndex =  i ;
			break;
		end

	end

	L_CreatureMgr:F_InitMgr(3, 3);
	L_TeamMgr:F_InitMgr(3,1);

	L_TeamMgr:F_AddResurrectPos(1, 1417.4, 1185.8, 46.0, 1.9);
	L_TeamMgr:F_AddResurrectPos(2, 1271.5, 1417.7, 46.0, 5.8);
	L_TeamMgr:F_AddResurrectPos(3, 1141.6, 1179.3, 46.0, 4.0);

end

function OnPlayerEnterInstance( PlayerID )
	local gTeam = RaidInstance:NewBattleGetPlayerTeam(PlayerID);
	--RaidInstance:SendSystemNotify( L_GroupName[gTeam]);
	L_TeamMgr:F_AddPlr(PlayerID,  gTeam);
	RaidInstance:UpdateNewBattleGroundPower(L_GroupName[1], L_GroupName[2], L_GroupName[3]);
end

function OnPlayerLeaveInstance( PlayerID )
	-- 战斗结束后，不能调用这个接口。
	if L_InstanceEnd == 0
	then
		-- 这里检测玩家是不是主动退出的。
		L_TeamMgr:F_RemovePlr(PlayerID);
	end
end

function GF_ResurrectPlr( Param0, Param1, Param2, Param3, Param4)
	L_TeamMgr:F_ResurrectPlr(Param0);
end

function OnUnitDie( VictimID, AttackerID )

	local power,Team = L_CreatureMgr:F_Creature_Dead(VictimID);
	if Team ~= 0
	then
		if power <= 0 and L_TeamMgr:F_CheckIsLoser(Team) == 0
		then
			RaidInstance:SendSystemNotify( L_GroupName[Team].."已经没有战争资源，退出本轮战场角逐！");
			L_TeamMgr:F_SetTeamLoser(Team, 1);
		end
	else
		power, Team = L_TeamMgr:F_PlrDead(VictimID);
		if  Team ~= 0
		then
			if  power > 0
			then
				local ResurrectEvent = CEvent:New(5000, 1, GF_ResurrectPlr, VictimID );
			else
				if  L_TeamMgr:F_CheckIsLoser(Team) == 0
				then
					RaidInstance:SendSystemNotify( L_GroupName[Team].."已经没有战争资源，退出本轮战场角逐！");
					L_TeamMgr:F_SetTeamLoser(Team, 1);
				end
			end
		end
	end

	RaidInstance:UpdateNewBattleGroundPower(L_GroupName[1], L_GroupName[2], L_GroupName[3]);
end

function OnEvent( EventID, Param0, Param1, Param2, Param3, Param4 )
    if L_EventList[EventID] ~= nil
	then
		L_EventList[EventID]:DoEvent(Param0, Param1, Param2, Param3, Param4 );
	end
end
