
local PosRes = {995.7,619.9,2.2,1.5,
981.2,619.6,1.1,1.5,
959.6,619.1,-0.5,1.2,
953.3,618.1,-0.7,1.1,
915.6,599.9,1.2,1.0,
910.5,596.5,1.2,0.8,
900.8,587.7,0.9,0.5,
897.8,582.7,0.4,6.3,
900.8,587.7,0.9,3.7,
910.5,596.5,1.2,4.0,
915.6,599.9,1.2,4.1,
953.3,618.1,-0.7,4.2,
959.6,619.1,-0.5,4.3,
981.2,619.6,1.1,4.7,
995.7,619.9,2.2,4.7};

local SpawnPos = {0.00, 5, 200000011,
					0.524, 5.774, 200000011,
					5.74, 5.774, 200000011};


function OnCombatStart_110002043(Id, TargetId )

	OnCombatStart_PrimaryPatrol(Id, TargetId);

end

function OnCombatStop_110002043( Id, TargetId )

	OnCombatStop_PrimaryPatrol(Id, TargetId)
end

function OnReturnFromCombat_110002043(Id)
	OnReturnFromCombat_PrimaryPatrol(Id);


end

function OnDamageTaken_110002043( Id , TargetAttackerId)

	OnDamageTaken_PrimaryPatrol( Id , TargetAttackerId);

end

function OnCastSpell_110002043( IdSpell )
	OnCastSpell_PrimaryPatrol( IdSpell );

end

function OnTargetParried_110002043( Id , TargetId )
	OnTargetParried_PrimaryPatrol( Id , TargetId );

end

function OnTargetDodged_110002043( Id , TargetId)
	OnTargetDodged_PrimaryPatrol( Id , TargetId);

end

function OnTargetBlocked_110002043( Id , TargetId, iAoumt)
	OnTargetBlocked_PrimaryPatrol( Id , TargetId, iAoumt);

end

function OnTargetCritHit_110002043( Id , TargetId, fAoumt )
	OnTargetCritHit_PrimaryPatrol( Id , TargetId, fAoumt );

end

function OnTargetDied_110002043( Id , TargetId )
	OnTargetDied_PrimaryPatrol( Id , TargetId);

end

function OnParried_110002043( Id , TargetId )

	OnParried_PrimaryPatrol( Id , TargetId);
end

function OnDodged_110002043( Id , TargetId )
	OnDodged_PrimaryPatrol( Id , TargetId);

end

function OnBlocked_110002043( Id , TargetId, iAmount)

	OnBlocked_110002043( Id , TargetId, iAmount);
end

function OnCritHit_110002043( Id, TargetId, fAmount )

	OnCritHit_PrimaryPatrol( Id, TargetId, fAmount );
end

function OnHit_110002043( Id, TargetId, fAmount )
	OnHit_PrimaryPatrol( Id, TargetId, fAmount );

end

function OnDied_110002043( Id, KillerId )
	OnDied_PrimaryPatrol( Id, KillerId );
end

function OnAssistTargetDied_110002043( Id, AssistTarget )
	OnAssistTargetDied_PrimaryPatrol( Id, AssistTarget );

end

function OnFear_110002043( Id, FearedId )
	OnFear_PrimaryPatrol( Id, FearedId );

end

function OnCallForHelp_110002043( Id )
	OnCallForHelp_PrimaryPatrol( Id );


end


function OnLoad_110002043( Id )
	CreateNumber(Id, "MaxCount",  15);
	LoadScript("script/CreatureAI/PrimaryPatrol.lua");


	local parentId = GetNumber(Id, "parentId");

	for i = 0,  14,  1
	do
		local n = i * 4 + 1;
		CreateNumber(Id, "PosMoveToPosX"..i, PosRes[n]);
		CreateNumber(Id, "PosMoveToPosY"..i, PosRes[n + 1]);
		CreateNumber(Id, "PosMoveToPosZ"..i, PosRes[n + 2]);
		CreateNumber(Id, "PosMoveToPosO"..i, PosRes[n + 3]);

	end





	CreateNumber(Id, "NoReady", 4);
	CreateNumber(Id, "TotalCount", 4);

	for i = 0, 2, 1
	do
		n = i * 3 + 1;
		CreateNumber(Id, "SpawnO"..i,  SpawnPos[n]);
		CreateNumber(Id, "SpawnPos"..i,  SpawnPos[n + 1]);
		CreateNumber(Id, "SpawnEntry"..i, SpawnPos[n + 2]);
	end

	OnLoad_PrimaryPatrol( Id );

end


function AIUpdate_110002043( Id )
	AIUpdate_PrimaryPatrol(Id);


end

function OnFlee_110002043( Id )
	OnFlee_PrimaryPatrol( Id );

end

function OnDestroy_110002043( Id )
	OnDestroy_PrimaryPatrol( Id );

end


