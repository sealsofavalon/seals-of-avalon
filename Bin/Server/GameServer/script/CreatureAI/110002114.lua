


local PosA = {PosX = 20, PosY = 20, PosZ = 0};
local PosB = {PosX = -20, PosY = -20, PosZ = 0};
local PosMoveTo = {PosX = 0, PosY = 0, PosZ = 0};
local PosMoveToB = {PosX = 0, PosY = 0, PosZ = 0};
local PosScrc = {PosX = 0, PosY = 0, PosZ = 0};

function OnCombatStart_110002114(Id, TargetId )		-- on instance start.

	SetNumber(Id, "InCombat",1);

end

function OnCombatStop_110002114( Id, TargetId )		-- instance update every second.

end

function OnReturnFromCombat_110002114(Id)
	SetNumber(Id, "InCombat",0);
	PosCount = GetNumber(Id, "MoveCount");
	local PosMoveToPosX = GetNumber(Id, "PosMoveToPosX");
	local PosMoveToPosY = GetNumber(Id, "PosMoveToPosY");
	local PosMoveToPosZ = GetNumber(Id, "PosMoveToPosZ");
	local PosMoveToPosBX = GetNumber(Id, "PosMoveToPosBX");
	local PosMoveToPosBY = GetNumber(Id, "PosMoveToPosBY");
	local PosMoveToPosBZ = GetNumber(Id, "PosMoveToPosBZ");
	local O = GetOrientation(Id);
	if PosCount == 0
	then
		CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);
	elseif PosCount == 1
	then
		CreatureMoveTo2(Id, PosMoveToPosBX, PosMoveToPosBY, PosMoveToPosBZ, O);
	end

end

function OnDamageTaken_110002114( Id , TargetAttackerId)		-- on instance start.



end

function OnCastSpell_110002114( IdSpell )		-- on instance start.


end

function OnTargetParried_110002114( Id , TargetId )		-- on instance start.


end

function OnTargetDodged_110002114( Id , TargetId)		-- on instance start.


end

function OnTargetBlocked_110002114( Id , TargetId, iAoumt)		-- on instance start.


end

function OnTargetCritHit_110002114( Id , TargetId, fAoumt )		-- on instance start.


end

function OnTargetDied_110002114( Id , TargetId )		-- on instance start.


end

function OnParried_110002114( Id , TargetId )		-- on instance start.


end

function OnDodged_110002114( Id , TargetId )		-- on instance start.


end

function OnBlocked_110002114( Id , TargetId, iAmount)		-- on instance start.


end

function OnCritHit_110002114( Id, TargetId, fAmount )		-- on instance start.


end

function OnHit_110002114( Id, TargetId, fAmount )		-- on instance start.


end

function OnDied_110002114( Id, KillerId )		-- on instance start.


end

function OnAssistTargetDied_110002114( Id, AssistTarget )		-- on instance start.


end

function OnFear_110002114( Id, FearedId )		-- on instance start.


end

function OnCallForHelp_110002114( Id )		-- on instance start.



end


function OnLoad_110002114( Id )		-- on instance start.
	RegisterAIUpdateEvent(Id, 100);
	local PosX = GetPosX(Id);
	local PosY = GetPosY(Id);
	local PosZ = GetPosZ(Id);
	CreateNumber(Id, "PosMoveToPosX");
	CreateNumber(Id, "PosMoveToPosY");
	CreateNumber(Id, "PosMoveToPosZ");
	SetNumber(Id, "PosMoveToPosX",PosX + PosA.PosX);
	SetNumber(Id, "PosMoveToPosY",PosX + PosA.PosX);
	SetNumber(Id, "PosMoveToPosZ",PosX + PosA.PosX);

	CreateNumber(Id, "PosMoveToPosBX");
	CreateNumber(Id, "PosMoveToPosBY");
	CreateNumber(Id, "PosMoveToPosBZ");
	SetNumber(Id, "PosMoveToPosBX",PosX + PosB.PosX);
	SetNumber(Id, "PosMoveToPosBY",PosX + PosB.PosX);
	SetNumber(Id, "PosMoveToPosBZ",PosX + PosB.PosX);

	CreatureMoveTo2(Id, GetNumber(Id, "PosMoveToPosX"), GetNumber(Id, "PosMoveToPosY"), GetNumber(Id, "PosMoveToPosZ"), GetOrientation(Id));
	local PosCount = CreateNumber(Id, "MoveCount");
	SetNumber(Id, "MoveCount", 0);
	local IsInCombat = CreateNumber(Id, "InCombat");
	SetNumber(Id, "InCombat", 0);

	SpawnCreature(Id, 200000003, PosX + 5, PosY, PosZ, 0);
	SpawnCreature(Id, 200000004, PosX + 10, PosY, PosZ, 0);
end


function AIUpdate_110002114( Id )

	local IsInCombat = GetNumber(Id, "InCombat");
	local PosCount = GetNumber(Id, "MoveCount");

	if IsInCombat == 0
	then
		local PosMoveToPosX = GetNumber(Id, "PosMoveToPosX");
		local PosMoveToPosY = GetNumber(Id, "PosMoveToPosY");
		local PosMoveToPosZ = GetNumber(Id, "PosMoveToPosZ");
		local PosMoveToPosBX = GetNumber(Id, "PosMoveToPosBX");
		local PosMoveToPosBY = GetNumber(Id, "PosMoveToPosBY");
		local PosMoveToPosBZ = GetNumber(Id, "PosMoveToPosBZ");
		local PosX = GetPosX(Id);
		local PosY = GetPosY(Id);
		local PosZ = GetPosZ(Id);
		local O = GetOrientation(Id);

		if PosCount == 0
		then
			FollowUnitByEntry(PosCount, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ);
			if	PosMoveToPosX == PosX and PosMoveToPosY == PosY
			then
				SetNumber(Id, "MoveCount",1);
				CreatureMoveTo2(Id, PosMoveToPosBX, PosMoveToPosBY, PosMoveToPosBZ, O);
			end
		elseif PosCount == 1
		then
			FollowUnitByEntry(PosCount, PosMoveToPosBX, PosMoveToPosBY, PosMoveToPosBZ);
			if 	PosMoveToPosBX == PosX and PosMoveToPosBX == PosY
			then
				SetNumber(Id, "MoveCount", 0);
				CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);

			end

		end

	end

end

function OnFlee_110002114( Id )


end

function OnDestroy_110002114( Id )


end



