

local PosA = {PosX = 20, PosY = 20, PosZ = 0};
local PosB = {PosX = -20, PosY = -20, PosZ = 0};
local PosMoveTo = {PosX = 0, PosY = 0, PosZ = 0};
local PosMoveToB = {PosX = 0, PosY = 0, PosZ = 0};
local PosScrc = {PosX = 0, PosY = 0, PosZ = 0};

function OnCombatStart_200000002(Id, TargetId )		-- on instance start.
	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(Id, "InCombatCount");

	if InCombat == 0
	then
		SetNumber(Id, "InCombatCount", InCombatCount + 1);
	end
	SetNumber(Id, "InCombat",1);


end

function OnCombatStop_200000002( Id, TargetId )		-- instance update every second.

end

function OnReturnFromCombat_200000002(Id)

	SetNumber(Id, "InCombat",0);
	SetNumber(Id, "InCombatCount", GetNumber(Id, "InCombatCount") - 1);
	PosCount = GetNumber(Id, "MoveCount");
	local PosMoveToPosX = GetNumber(Id, "PosMoveToPosX");
	local PosMoveToPosY = GetNumber(Id, "PosMoveToPosY");
	local PosMoveToPosZ = GetNumber(Id, "PosMoveToPosZ");
	local PosMoveToPosBX = GetNumber(Id, "PosMoveToPosBX");
	local PosMoveToPosBY = GetNumber(Id, "PosMoveToPosBY");
	local PosMoveToPosBZ = GetNumber(Id, "PosMoveToPosBZ");
	local O = GetOrientation(Id);
	if PosCount == 0
	then
		CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);
	elseif PosCount == 1
	then
		CreatureMoveTo2(Id, PosMoveToPosBX, PosMoveToPosBY, PosMoveToPosBZ, O);
	end

end

function OnDamageTaken_200000002( Id , TargetAttackerId)		-- on instance start.



end

function OnCastSpell_200000002( IdSpell )		-- on instance start.


end

function OnTargetParried_200000002( Id , TargetId )		-- on instance start.


end

function OnTargetDodged_200000002( Id , TargetId)		-- on instance start.


end

function OnTargetBlocked_200000002( Id , TargetId, iAoumt)		-- on instance start.


end

function OnTargetCritHit_200000002( Id , TargetId, fAoumt )		-- on instance start.


end

function OnTargetDied_200000002( Id , TargetId )		-- on instance start.


end

function OnParried_200000002( Id , TargetId )		-- on instance start.


end

function OnDodged_200000002( Id , TargetId )		-- on instance start.


end

function OnBlocked_200000002( Id , TargetId, iAmount)		-- on instance start.


end

function OnCritHit_200000002( Id, TargetId, fAmount )		-- on instance start.


end

function OnHit_200000002( Id, TargetId, fAmount )		-- on instance start.


end

function OnDied_200000002( Id, KillerId )		-- on instance start.

	SetNumber(Id, "IsDead", 1);

	local NoReady = GetNumber(Id, "NoReady");

	local IsInNoReady = GetNumber(Id,"IsInNoReady");
	if IsInNoReady == 1
	then
		SetNumber(Id, "NoReady", NoReady - 1);
	end

	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(Id, "InCombatCount");

	if InCombat == 1
	then
		SetNumber(Id, "InCombatCount", InCombatCount - 1);
	end

	SetNumber(Id, "TotalCount", GetNumber(Id, "TotalCount") - 1);

	local TotalCount = GetNumber(Id, "TotalCount");
	if TotalCount == 0
	then
		DelNumber(parentId);
	else


		DelNumber(Id, "PosMoveToPosX");
		DelNumber(Id, "PosMoveToPosY");
		DelNumber(Id, "PosMoveToPosZ");

		DelNumber(Id, "PosMoveToPosBX");
		DelNumber(Id, "PosMoveToPosBY");
		DelNumber(Id, "PosMoveToPosBZ");
		DelNumber(Id, "MoveCount");

		DelNumber(Id, "InCombat");
		DelNumber(Id, "ReadyNext");
		DelNumber(Id, "IsInNoReady");
	end


end

function OnAssistTargetDied_200000002( Id, AssistTarget )		-- on instance start.


end

function OnFear_200000002( Id, FearedId )		-- on instance start.


end

function OnCallForHelp_200000002( Id )		-- on instance start.



end


function OnLoad_200000002( Id )		-- on instance start.
	RegisterAIUpdateEvent(Id, 100);
	local PosX = GetPosX(Id);
	local PosY = GetPosY(Id);
	local PosZ = GetPosZ(Id);
	CreateNumber(Id, "PosMoveToPosX");
	CreateNumber(Id, "PosMoveToPosY");
	CreateNumber(Id, "PosMoveToPosZ");
	SetNumber(Id, "PosMoveToPosX",PosX + PosA.PosX);
	SetNumber(Id, "PosMoveToPosY",PosY + PosA.PosY);
	SetNumber(Id, "PosMoveToPosZ",PosZ + PosA.PosZ);

	CreateNumber(Id, "PosMoveToPosBX");
	CreateNumber(Id, "PosMoveToPosBY");
	CreateNumber(Id, "PosMoveToPosBZ");
	SetNumber(Id, "PosMoveToPosBX",PosX + PosB.PosX);
	SetNumber(Id, "PosMoveToPosBY",PosY + PosB.PosY);
	SetNumber(Id, "PosMoveToPosBZ",PosZ + PosB.PosZ);

	CreatureMoveTo2(Id, GetNumber(Id, "PosMoveToPosX"), GetNumber(Id, "PosMoveToPosY"), GetNumber(Id, "PosMoveToPosZ"), GetOrientation(Id));
	local PosCount = CreateNumber(Id, "MoveCount");
	SetNumber(Id, "MoveCount", 0);
	local IsInCombat = CreateNumber(Id, "InCombat");
	SetNumber(Id, "InCombat", 0);
	CreateNumber(Id, "AllReady");
	SetNumber(Id, "AllReady", 0);
	CreateNumber(Id, "NoReady");
	SetNumber(Id, "NoReady", 5);
	CreateNumber(Id, "TotalCount");
	SetNumber(Id, "TotalCount", 5);
	CreateNumber(Id, "InCombatCount");
	SetNumber(Id, "InCombatCount", 0);

	local IdCreature1 = SpawnCreature(Id, 200000003, PosX + 5, PosY, PosZ, 0);
	local IdCreature2 = SpawnCreature(Id, 200000004, PosX , PosY + 10, PosZ, 0);


	local IdCreature3 = SpawnCreature(Id, 200000003, PosX + 10, PosY, PosZ, 0);
	local IdCreature4 = SpawnCreature(Id, 200000004, PosX , PosY + 5, PosZ, 0);

	CreateNumber(IdCreature2, "parentId");
	CreateNumber(IdCreature1, "parentId");
	CreateNumber(IdCreature3, "parentId");
	CreateNumber(IdCreature4, "parentId");
	SetNumber(IdCreature1, "parentId", Id);
	SetNumber(IdCreature2, "parentId", Id);
	SetNumber(IdCreature3, "parentId", Id);
	SetNumber(IdCreature4, "parentId", Id);
	CreateNumber(Id, "ReadyNext");
	SetNumber(Id, "ReadyNext", 0);

	CreateNumber(Id, "IsDead");
	SetNumber(Id, "IsDead", 0);
	CreateNumber(Id, "IsInNoReady");
	SetNumber(Id, "IsInNoReady", 1);
end


function AIUpdate_200000002( Id )

	local IsInCombat = GetNumber(Id, "InCombat");
	local PosCount = GetNumber(Id, "MoveCount");

	local Dead = GetNumber(Id, "IsDead");
	local InCombatCount = GetNumber(Id, "InCombatCount");
	if InCombatCount == 0 and Dead == 0
	then
		local PosMoveToPosX = GetNumber(Id, "PosMoveToPosX");
		local PosMoveToPosY = GetNumber(Id, "PosMoveToPosY");
		local PosMoveToPosZ = GetNumber(Id, "PosMoveToPosZ");
		local PosMoveToPosBX = GetNumber(Id, "PosMoveToPosBX");
		local PosMoveToPosBY = GetNumber(Id, "PosMoveToPosBY");
		local PosMoveToPosBZ = GetNumber(Id, "PosMoveToPosBZ");
		local PosX = GetPosX(Id);
		local PosY = GetPosY(Id);
		local PosZ = GetPosZ(Id);
		local O = GetOrientation(Id);
		local AllReadyCount = GetNumber(Id, "AllReady");
		local TotalCount= GetNumber(Id, "TotalCount");
		local MoveReady = GetNumber(Id, "ReadyNext");
		local NoReady = GetNumber(Id, "NoReady");
		if MoveReady == 0
		then

			if PosCount == 0
			then
				if	PosMoveToPosX == PosX and PosMoveToPosY == PosY
				then
					SetNumber(Id, "ReadyNext", 1);
					SetNumber(Id, "AllReady", AllReadyCount + 1);
					SetNumber(Id, "NoReady", NoReady - 1);
					SetNumber(Id, "IsInNoReady", 0);
				--CreatureMoveTo2(Id, PosMoveToPosBX, PosMoveToPosBY, PosMoveToPosBZ, O);
				end
			elseif PosCount == 1
			then
				if 	PosMoveToPosBX == PosX and PosMoveToPosBY == PosY
				then
					SetNumber(Id, "ReadyNext", 1);
					SetNumber(Id, "AllReady", AllReadyCount + 1);
					SetNumber(Id, "NoReady", NoReady - 1);
					SetNumber(Id, "IsInNoBeady", 0);
				--CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);

			end

		end

		elseif MoveReady == 1
		then
			if AllReadyCount == TotalCount
			then
				if PosCount == 0
				then
					CreatureMoveTo2(Id, PosMoveToPosBX, PosMoveToPosBY, PosMoveToPosBZ, O);
					SetNumber(Id, "ReadyNext", 3);
					SetNumber(Id, "MoveCount",1);
					SetNumber(Id, "NoReady", NoReady +1);
					SetNumber(Id, "IsInNoReady", 1);
				elseif PosCount == 1
				then
					CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);
					SetNumber(Id, "MoveCount", 0);
					SetNumber(Id, "NoReady", NoReady + 1);
					SetNumber(Id, "IsInNoReady", 1);
					SetNumber(Id, "ReadyNext", 3);
				end
				if NoReady == TotalCount
				then
					SetNumber(Id, "AllReady", 0);
					SetNumber(Id, "ReadyNext", 0);
				end
			end
		elseif MoveReady == 3
		then
			SetNextSpel(NoReady, TotalCount);
			if NoReady == TotalCount
			then
				SetNumber(Id, "AllReady", 0);
				SetNumber(Id, "ReadyNext", 0);
			end

		end
	end


end

function OnFlee_200000002( Id )


end

function OnDestroy_200000002( Id )


end


