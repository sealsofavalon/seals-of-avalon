local PosA = {PosX = 20, PosY = 20, PosZ = 0};
local PosB = {PosX = -20, PosY = -20, PosZ = 0};
local PosMoveTo = {PosX = 0, PosY = 0, PosZ = 0};
local PosMoveToB = {PosX = 0, PosY = 0, PosZ = 0};
local PosScrc = {PosX = 0, PosY = 0, PosZ = 0};

function OnCombatStart_200000003(Id, TargetId )		-- on instance start.
	local parentId = GetNumber(Id, "parentId");


	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(parentId, "InCombatCount");

	if InCombat == 0
	then
		SetNumber(parentId, "InCombatCount", InCombatCount + 1);
	end

	SetNumber(Id, "InCombat",1);


end

function OnCombatStop_200000003( Id, TargetId )		-- instance update every second.

end

function OnReturnFromCombat_200000003(Id)
	local parentId = GetNumber(Id, "parentId");


	local InCombatCount = GetNumber(parentId, "InCombatCount");

	SetNumber(parentId, "InCombatCount", InCombatCount - 1);

	SetNumber(Id, "InCombat",0);
	PosCount = GetNumber(Id, "MoveCount");
	local PosMoveToPosX = GetNumber(Id, "PosMoveToPosX");
	local PosMoveToPosY = GetNumber(Id, "PosMoveToPosY");
	local PosMoveToPosZ = GetNumber(Id, "PosMoveToPosZ");
	local PosMoveToPosBX = GetNumber(Id, "PosMoveToPosBX");
	local PosMoveToPosBY = GetNumber(Id, "PosMoveToPosBY");
	local PosMoveToPosBZ = GetNumber(Id, "PosMoveToPosBZ");
	local O = GetOrientation(Id);
	if PosCount == 0
	then
		CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);
	elseif PosCount == 1
	then
		CreatureMoveTo2(Id, PosMoveToPosBX, PosMoveToPosBY, PosMoveToPosBZ, O);
	end
end

function OnDamageTaken_200000003( Id , TargetAttackerId)		-- on instance start.



end

function OnCastSpell_200000003( IdSpell )		-- on instance start.


end

function OnTargetParried_200000003( Id , TargetId )		-- on instance start.


end

function OnTargetDodged_200000003( Id , TargetId)		-- on instance start.


end

function OnTargetBlocked_200000003( Id , TargetId, iAoumt)		-- on instance start.


end

function OnTargetCritHit_200000003( Id , TargetId, fAoumt )		-- on instance start.


end

function OnTargetDied_200000003( Id , TargetId )		-- on instance start.


end

function OnParried_200000003( Id , TargetId )		-- on instance start.


end

function OnDodged_200000003( Id , TargetId )		-- on instance start.


end

function OnBlocked_200000003( Id , TargetId, iAmount)		-- on instance start.


end

function OnCritHit_200000003( Id, TargetId, fAmount )		-- on instance start.


end

function OnHit_200000003( Id, TargetId, fAmount )		-- on instance start.


end

function OnDied_200000003( Id, KillerId )		-- on instance start.



	local parentId = GetNumber(Id, "parentId");
	SetNumber(parentId, "TotalCount", GetNumber(parentId, "TotalCount") - 1);

	SetNumber(Id, "IsDead", 1);
	local NoReady = GetNumber(parentId, "NoReady");

	local IsInNoReady = GetNumber(Id,"IsInNoReady");
	if IsInNoReady == 1
	then
		SetNumber(parentId, "NoReady", NoReady -1);
	end

	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(parentId, "InCombatCount");

	if InCombat == 1
	then
		SetNumber(parentId, "InCombatCount", InCombatCount - 1);
	end
	local TotalCount = GetNumber(parentId, "TotalCount");
	if TotalCount == 0
	then
		DelNumber(parentId);
	end


	DelNumber(Id);
end

function OnAssistTargetDied_200000003( Id, AssistTarget )		-- on instance start.


end

function OnFear_200000003( Id, FearedId )		-- on instance start.


end

function OnCallForHelp_200000003( Id )		-- on instance start.



end


function OnLoad_200000003( Id )		-- on instance start.
	RegisterAIUpdateEvent(Id, 100);
	local PosX = GetPosX(Id);
	local PosY = GetPosY(Id);
	local PosZ = GetPosZ(Id);
	CreateNumber(Id, "PosMoveToPosX");
	CreateNumber(Id, "PosMoveToPosY");
	CreateNumber(Id, "PosMoveToPosZ");
	SetNumber(Id, "PosMoveToPosX",PosX + PosA.PosX);
	SetNumber(Id, "PosMoveToPosY",PosY + PosA.PosY);
	SetNumber(Id, "PosMoveToPosZ",PosZ + PosA.PosZ);

	CreateNumber(Id, "PosMoveToPosBX");
	CreateNumber(Id, "PosMoveToPosBY");
	CreateNumber(Id, "PosMoveToPosBZ");
	SetNumber(Id, "PosMoveToPosBX",PosX + PosB.PosX);
	SetNumber(Id, "PosMoveToPosBY",PosY + PosB.PosY);
	SetNumber(Id, "PosMoveToPosBZ",PosZ + PosB.PosZ);

	CreatureMoveTo2(Id, GetNumber(Id, "PosMoveToPosX"), GetNumber(Id, "PosMoveToPosY"), GetNumber(Id, "PosMoveToPosZ"), GetOrientation(Id));
	local PosCount = CreateNumber(Id, "MoveCount");
	SetNumber(Id, "MoveCount", 0);
	local IsInCombat = CreateNumber(Id, "InCombat");
--	SetNumber(Id, "InCombat", 0);
--	CreateNumber(Id, "AllReady");
--	SetNumber(Id, "AllReady", 1);
--	CreateNumber(Id, "TotalCount");
--	SetNumber(Id, "TotalCount", 1);

--	local IdCreature1 = SpawnCreature(Id, 200000003, PosX + 5, PosY, PosZ, 0);
--	local IdCreature2 = SpawnCreature(Id, 200000004, PosX + 10, PosY, PosZ, 0);
--	CreateNumber(IdCreature2, "parentId");
--	CreateNumber(IdCreature1, "parentId");
--	SetNumber(IdCreature1, "parentId", Id);
--	SetNumber(IdCreature2, "parentId", Id);

	CreateNumber(Id, "IsDead");
	SetNumber(Id, "IsDead", 0);
	CreateNumber(Id, "ReadyNext");
	SetNumber(Id, "ReadyNext", 0);
	CreateNumber(Id, "IsInNoReady");
	SetNumber(Id, "IsInNoReady", 1);

end


function AIUpdate_200000003( Id )

	local IsInCombat = GetNumber(Id, "InCombat");
	local PosCount = GetNumber(Id, "MoveCount");
	local parentId = GetNumber(Id, "parentId");
	local InCombatCount = GetNumber(parentId, "InCombatCount");

	local Dead = GetNumber(Id, "IsDead");

	if InCombatCount == 0 and Dead == 0
	then
		local PosMoveToPosX = GetNumber(Id, "PosMoveToPosX");
		local PosMoveToPosY = GetNumber(Id, "PosMoveToPosY");
		local PosMoveToPosZ = GetNumber(Id, "PosMoveToPosZ");
		local PosMoveToPosBX = GetNumber(Id, "PosMoveToPosBX");
		local PosMoveToPosBY = GetNumber(Id, "PosMoveToPosBY");
		local PosMoveToPosBZ = GetNumber(Id, "PosMoveToPosBZ");
		local PosX = GetPosX(Id);
		local PosY = GetPosY(Id);
		local PosZ = GetPosZ(Id);
		local O = GetOrientation(Id);
		local parentId = GetNumber(Id, "parentId");
		local AllReadyCount = GetNumber(parentId, "AllReady");
		local TotalCount= GetNumber(parentId, "TotalCount");
		local MoveReady = GetNumber(Id, "ReadyNext");
		local NoReady = GetNumber(parentId, "NoReady");

		if PosCount == 0
		then
			if	PosMoveToPosX == PosX and PosMoveToPosY == PosY and MoveReady == 0
			then
				SetNumber(Id, "ReadyNext", 1);
				SetNumber(parentId, "AllReady", AllReadyCount + 1);
				SetNumber(parentId, "NoReady", NoReady - 1);
				SetNumber(Id, "IsInNoBeady", 0);
				--CreatureMoveTo2(Id, PosMoveToPosBX, PosMoveToPosBY, PosMoveToPosBZ, O);
			end
		elseif PosCount == 1
		then
			if 	PosMoveToPosBX == PosX and PosMoveToPosBY == PosY and MoveReady == 0
			then
				SetNumber(Id, "ReadyNext", 1);
				SetNumber(parentId, "AllReady", AllReadyCount + 1);
				SetNumber(parentId, "NoReady", NoReady - 1);
				SetNumber(Id, "IsInNoBeady", 0);
				--CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);

			end

		end

		if AllReadyCount == TotalCount and MoveReady == 1
		then
			if PosCount == 0
			then
				CreatureMoveTo2(Id, PosMoveToPosBX, PosMoveToPosBY, PosMoveToPosBZ, O);
				SetNumber(Id, "ReadyNext", 3);
				SetNumber(Id, "MoveCount",1);
				SetNumber(parentId, "NoReady", NoReady + 1);
				SetNumber(Id, "IsInNoBeady", 1);
			elseif PosCount == 1
			then
				CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);
				SetNumber(Id, "MoveCount", 0);
				SetNumber(Id, "ReadyNext", 3);
				SetNumber(parentId, "NoReady", NoReady + 1);
				SetNumber(Id, "IsInNoBeady", 1);
			end

		end

		if NoReady == TotalCount and MoveReady == 3
		then
			SetNumber(parentId, "AllReady", 0);
			SetNumber(Id, "ReadyNext", 0);

		end

	end

end

function OnFlee_200000003( Id )


end

function OnDestroy_200000003( Id )



end
