


function OnCombatStart_200000005(Id, TargetId )		-- on instance start.
	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(Id, "InCombatCount");

	if InCombat == 0
	then
		SetNumber(Id, "InCombatCount", InCombatCount + 1);
	end
	SetNumber(Id, "InCombat",1);


end

function OnCombatStop_200000005( Id, TargetId )		-- instance update every second.

end

function OnReturnFromCombat_200000005(Id)

	SetNumber(Id, "InCombat",0);
	SetNumber(Id, "InCombatCount", GetNumber(Id, "InCombatCount") - 1);
	PosCount = GetNumber(Id, "MoveCount");

	local PosMoveToPosX = GetNumber(Id, "PosMoveToPosX" ..PosCount );
	local PosMoveToPosY = GetNumber(Id, "PosMoveToPosY".. PosCount);
	local PosMoveToPosZ = GetNumber(Id, "PosMoveToPosZ"..PosCount);
	local O = GetOrientation(Id);

	CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);

end

function OnDamageTaken_200000005( Id , TargetAttackerId)		-- on instance start.



end

function OnCastSpell_200000005( IdSpell )		-- on instance start.


end

function OnTargetParried_200000005( Id , TargetId )		-- on instance start.


end

function OnTargetDodged_200000005( Id , TargetId)		-- on instance start.


end

function OnTargetBlocked_200000005( Id , TargetId, iAoumt)		-- on instance start.


end

function OnTargetCritHit_200000005( Id , TargetId, fAoumt )		-- on instance start.


end

function OnTargetDied_200000005( Id , TargetId )		-- on instance start.


end

function OnParried_200000005( Id , TargetId )		-- on instance start.


end

function OnDodged_200000005( Id , TargetId )		-- on instance start.


end

function OnBlocked_200000005( Id , TargetId, iAmount)		-- on instance start.


end

function OnCritHit_200000005( Id, TargetId, fAmount )		-- on instance start.


end

function OnHit_200000005( Id, TargetId, fAmount )		-- on instance start.


end

function OnDied_200000005( Id, KillerId )		-- on instance start.

	SetNumber(Id, "IsDead", 1);
	local ParentId = GetNumber(Id, "parentId");
	local NoReady = GetNumber(ParentId, "NoReady");

	local IsInNoReady = GetNumber(Id,"IsInNoReady");
	if IsInNoReady == 1
	then
		SetNumber(ParentId, "NoReady", NoReady - 1);
	end

	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(ParentId, "InCombatCount");

	if InCombat == 1
	then
		SetNumber(ParentId, "InCombatCount", InCombatCount - 1);
	end

	SetNumber(ParentId, "TotalCount", GetNumber(Id, "TotalCount") - 1);

	local TotalCount = GetNumber(ParentId, "TotalCount");
	if TotalCount == 0
	then
		DelNumber(ParentId);
	end


	DelNumber(Id);

end

function OnAssistTargetDied_200000005( Id, AssistTarget )		-- on instance start.


end

function OnFear_200000005( Id, FearedId )		-- on instance start.


end

function OnCallForHelp_200000005( Id )		-- on instance start.



end


function calcO(O1, O2)
	local O = O1 + O2;
	if( O > math.pi * 2 )
	then
		O = O - math.pi * 2;
	end
	return O;

end

function OnLoad_200000005( Id )		-- on instance start.

	RegisterAIUpdateEvent(Id, 100);
	local PosX = GetPosX(Id);
	local PosY = GetPosY(Id);
	local PosZ = GetPosZ(Id);

	local parentId = GetNumber(Id, "parentId");
	CreateNumber(Id, "firstUpdate");
	SetNumber(Id,"firstUpdate", 1);

	CreateNumber(Id, "MoveCount");
	SetNumber(Id, "MoveCount", 0);
	local IsInCombat = CreateNumber(Id, "InCombat");
	SetNumber(Id, "InCombat", 0);
	CreateNumber(Id, "InCombatCount");
	SetNumber(Id, "InCombatCount", 0);
		CreateNumber(Id, "ReadyNext");
	SetNumber(Id, "ReadyNext", 0);

	CreateNumber(Id, "IsDead");
	SetNumber(Id, "IsDead", 0);
	CreateNumber(Id, "IsInNoReady");
	SetNumber(Id, "IsInNoReady", 1);



end


function AIUpdate_200000005( Id )

	local firstUpdate = GetNumber(Id, "firstUpdate");
	if firstUpdate > 0
	then

		local ParentO = GetNumber(Id, "parentO");
		local parentDistance = GetNumber(Id, "parentDistance");
		local ParentId = GetNumber(Id, "parentId");
		local TempPosX;
		local TempPosY;
		local TempPosZ;
		local MoveX;
		local MoveY;
		local TempPosO;
		for i = 0,  8,  1
		do

			TempPosX = GetNumber(ParentId, "PosMoveToPosX"..i);
			TempPosY = GetNumber(ParentId, "PosMoveToPosY"..i);
			TempPosZ = GetNumber(ParentId, "PosMoveToPosZ"..i);
			TempPosO = GetNumber(ParentId, "PosMoveToPosO"..i);
			local CureentO = calcO( ParentO, TempPosO);
			MoveY = math.sin(CureentO) * parentDistance;
			MoveX = math.cos(CureentO) * parentDistance;

			local n = i * 4 + 1;
			CreateNumber(Id, "PosMoveToPosX"..i);
			CreateNumber(Id, "PosMoveToPosY"..i);
			CreateNumber(Id, "PosMoveToPosZ"..i);
			CreateNumber(Id, "PosMoveToPosO"..i);


			SetNumber(Id, "PosMoveToPosX"..i, TempPosX + MoveX);
			SetNumber(Id, "PosMoveToPosY"..i, TempPosY + MoveY);
			SetNumber(Id, "PosMoveToPosZ"..i, TempPosZ);
			SetNumber(Id, "PosMoveToPosO"..i, TempPosO);
		end

		CreatureMoveTo2(Id, GetNumber(Id, "PosMoveToPosX0"), GetNumber(Id, "PosMoveToPosY0"), GetNumber(Id, "PosMoveToPosZ0"), GetOrientation(Id));
		SetNumber(Id, "firstUpdate", -1);

	return;
	end

	local ParentId = GetNumber(Id, "parentId");
	local IsInCombat = GetNumber(Id, "InCombat");
	local PosCount = GetNumber(Id, "MoveCount");
	local NextPosCount = 0;
	local Dead = GetNumber(Id, "IsDead");
	local InCombatCount = GetNumber(ParentId, "InCombatCount");
	if InCombatCount == 0 and Dead == 0
	then
		if PosCount == 8
		then
			NextPosCount = 0
		else
			NextPosCount = PosCount + 1;
		end

		local NextPosX = GetNumber(Id, "PosMoveToPosX"..NextPosCount);
		local NextPosY = GetNumber(Id, "PosMoveToPosY"..NextPosCount);
		local NextPosZ = GetNumber(Id, "PosMoveToPosZ"..NextPosCount);

		local CurrentPosX = GetNumber(Id, "PosMoveToPosX"..PosCount);
		local CurrentPosY = GetNumber(Id, "PosMoveToPosY"..PosCount);
		local CurrentPosZ = GetNumber(Id, "PosMoveToPosZ"..PosCount);
		local PosX = GetPosX(Id);
		local PosY = GetPosY(Id);
		local PosZ = GetPosZ(Id);
		local O = GetOrientation(Id);
		local AllReadyCount = GetNumber(ParentId, "AllReady");
		local TotalCount= GetNumber(ParentId, "TotalCount");
		local MoveReady = GetNumber(Id, "ReadyNext");
		local NoReady = GetNumber(ParentId, "NoReady");

		if MoveReady == 0
		then
			if PosX == CurrentPosX and PosY == CurrentPosY
			then
				SetNumber(Id, "ReadyNext", 1);
				SetNumber(ParentId, "AllReady", AllReadyCount + 1);
				SetNumber(ParentId, "NoReady", NoReady - 1);
				SetNumber(Id, "IsInNoReady", 0);
			end
		elseif MoveReady == 1
		then
			if AllReadyCount == TotalCount
			then
				if PosCount == 8
				then
					PosCount = 0
				else
					PosCount = PosCount + 1;
				end

				CreatureMoveTo2(Id, NextPosX, NextPosY, NextPosZ, O);
				SetNumber(Id, "ReadyNext", 3);
				SetNumber(Id, "MoveCount",PosCount);
				SetNumber(ParentId, "NoReady", NoReady +1);
				SetNumber(Id, "IsInNoReady", 1);
			end
		elseif MoveReady == 3
		then
			if NoReady == TotalCount
			then
				SetNumber(ParentId, "AllReady", 0);
				SetNumber(Id, "ReadyNext", 0);
			end
		end
	end

end

function OnFlee_200000005( Id )


end

function OnDestroy_200000005( Id )


end


