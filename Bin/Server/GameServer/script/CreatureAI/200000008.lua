
local PosRes = {1655.7, 1788.3, 25.5, 5.6,
			  1711.2, 1728.8, 24.0, 5.6,
			  1674.3, 1707.1, 24.3, 1.5,
			  1668.0, 1590.7, 27.5, 0.0,
			  1965.3, 1513.6, 13.5, 4.9,
			  2025.3, 1573.5, 15.3, 2.5,
			  1952.1, 1708.7, 21.6, 2.9,
			  1870.9, 1778.6, 24.5,	2.1,
			  1756.8, 1784.8, 23.7, 1.5};

local SpawnPos = {0.00, 5,
					1.57, 5,
					3.14, 5,
					4.71, 5};
local PosA = {PosX = 20, PosY = 20, PosZ = 0};
local PosB = {PosX = -20, PosY = -20, PosZ = 0};
local PosMoveTo = {PosX = 0, PosY = 0, PosZ = 0};
local PosMoveToB = {PosX = 0, PosY = 0, PosZ = 0};
local PosScrc = {PosX = 0, PosY = 0, PosZ = 0};

function OnCombatStart_200000008(Id, TargetId )		-- on instance start.
	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(Id, "InCombatCount");

	if InCombat == 0
	then
		SetNumber(Id, "InCombatCount", InCombatCount + 1);
	end
	SetNumber(Id, "InCombat",1);


end

function OnCombatStop_200000008( Id, TargetId )		-- instance update every second.

end

function OnReturnFromCombat_200000008(Id)

	SetNumber(Id, "InCombat",0);
	SetNumber(Id, "InCombatCount", GetNumber(Id, "InCombatCount") - 1);
	PosCount = GetNumber(Id, "MoveCount");

	local PosMoveToPosX = GetNumber(Id, "PosMoveToPosX" ..PosCount );
	local PosMoveToPosY = GetNumber(Id, "PosMoveToPosY".. PosCount);
	local PosMoveToPosZ = GetNumber(Id, "PosMoveToPosZ"..PosCount);
	local O = GetOrientation(Id);

	CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);

end

function OnDamageTaken_200000008( Id , TargetAttackerId)		-- on instance start.



end

function OnCastSpell_200000008( IdSpell )		-- on instance start.


end

function OnTargetParried_200000008( Id , TargetId )		-- on instance start.


end

function OnTargetDodged_200000008( Id , TargetId)		-- on instance start.


end

function OnTargetBlocked_200000008( Id , TargetId, iAoumt)		-- on instance start.


end

function OnTargetCritHit_200000008( Id , TargetId, fAoumt )		-- on instance start.


end

function OnTargetDied_200000008( Id , TargetId )		-- on instance start.


end

function OnParried_200000008( Id , TargetId )		-- on instance start.


end

function OnDodged_200000008( Id , TargetId )		-- on instance start.


end

function OnBlocked_200000008( Id , TargetId, iAmount)		-- on instance start.


end

function OnCritHit_200000008( Id, TargetId, fAmount )		-- on instance start.


end

function OnHit_200000008( Id, TargetId, fAmount )		-- on instance start.


end

function OnDied_200000008( Id, KillerId )		-- on instance start.

	SetNumber(Id, "IsDead", 1);

	local NoReady = GetNumber(Id, "NoReady");

	local IsInNoReady = GetNumber(Id,"IsInNoReady");
	if IsInNoReady == 1
	then
		SetNumber(Id, "NoReady", NoReady - 1);
	end

	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(Id, "InCombatCount");

	if InCombat == 1
	then
		SetNumber(Id, "InCombatCount", InCombatCount - 1);
	end

	SetNumber(Id, "TotalCount", GetNumber(Id, "TotalCount") - 1);

	local TotalCount = GetNumber(Id, "TotalCount");
	if TotalCount == 0
	then
		DelNumber(Id);
	else
		DelNumber(Id, "PosMoveToPosX");
		DelNumber(Id, "PosMoveToPosY");
		DelNumber(Id, "PosMoveToPosZ");
		DelNumber(Id, "MoveCount");
		DelNumber(Id, "InCombat");
		DelNumber(Id, "ReadyNext");
		DelNumber(Id, "IsInNoReady");
	end


end

function OnAssistTargetDied_200000008( Id, AssistTarget )		-- on instance start.


end

function OnFear_200000008( Id, FearedId )		-- on instance start.


end

function OnCallForHelp_200000008( Id )		-- on instance start.



end


function calcO(O1, O2)
	local O = O1 + O2;
	if( O > math.pi * 2 )
	then
		O = O -M_PI * 2;
	end
	return O;

end

function OnLoad_200000008( Id )		-- on instance start.
	RegisterAIUpdateEvent(Id, 100);
	local PosX = GetPosX(Id);
	local PosY = GetPosY(Id);
	local PosZ = GetPosZ(Id);

	local parentId = GetNumber(Id, "parentId");

	for i = 0,  8,  1
	do
		local n = i * 4 + 1;
		CreateNumber(Id, "PosMoveToPosX"..i);
		CreateNumber(Id, "PosMoveToPosY"..i);
		CreateNumber(Id, "PosMoveToPosZ"..i);
		CreateNumber(Id, "PosMoveToPosO"..i);
		SetNumber(Id, "PosMoveToPosX"..i, PosRes[n]);
		SetNumber(Id, "PosMoveToPosY"..i, PosRes[n + 1]);
		SetNumber(Id, "PosMoveToPosZ"..i, PosRes[n + 2]);
		SetNumber(Id, "PosMoveToPosO"..i, PosRes[n +3]);
	end

	local PosCount = CreateNumber(Id, "MoveCount");
	SetNumber(Id, "MoveCount", 0);
	local IsInCombat = CreateNumber(Id, "InCombat");
	SetNumber(Id, "InCombat", 0);
	CreateNumber(Id, "AllReady");
	SetNumber(Id, "AllReady", 0);
	CreateNumber(Id, "NoReady");
	SetNumber(Id, "NoReady", 5);
	CreateNumber(Id, "TotalCount");
	SetNumber(Id, "TotalCount", 5);
	CreateNumber(Id, "InCombatCount");
	SetNumber(Id, "InCombatCount", 0);

	local O = GetNumber(Id, "PosMoveToPosO0");
	local n  = 0;
	local tempO = 0;
	local MovePosX = 0;
	local MovePosY = 0;
	local IdCreature1 = 0;

	for i = 0, 3, 1
	do
		n = i * 2 + 1;
		tempO = calcO(SpawnPos[n] , O);
		MovePosX = math.cos(tempO) * SpawnPos[n + 1];
		MovePosY = math.sin(tempO) * SpawnPos[n + 1];
		IdCreature1 = SpawnCreature(Id, 200000005, PosX +MovePosX, PosY +MovePosY, PosZ,tempO);
		CreateNumber(IdCreature1, "parentId");
		CreateNumber(IdCreature1, "parentO");
		CreateNumber(IdCreature1, "parentDistance");
		SetNumber(IdCreature1, "parentId", Id);
		SetNumber(IdCreature1, "parentO", SpawnPos[n]);
		SetNumber(IdCreature1, "parentDistance", SpawnPos[n + 1]);
	end



	CreatureMoveTo2(Id, GetNumber(Id, "PosMoveToPosX0"), GetNumber(Id, "PosMoveToPosY0"), GetNumber(Id, "PosMoveToPosZ0"), GetOrientation(Id));


--[[
	local IdCreature1 = SpawnCreature(Id, 200000007, PosX + 3, PosY + 3, PosZ, 0);
	local IdCreature2 = SpawnCreature(Id, 200000007, PosX , PosY + 3, PosZ, 0);


	local IdCreature3 = SpawnCreature(Id, 200000006, PosX + 3, PosY + 5, PosZ, 0);
	local IdCreature4 = SpawnCreature(Id, 200000006, PosX , PosY + 5, PosZ, 0);

	local IdCreature5 = SpawnCreature(Id, 200000008, PosX + 3 PosY, PosZ, 0);

	CreateNumber(IdCreature2, "parentId");
	CreateNumber(IdCreature1, "parentId");
	CreateNumber(IdCreature3, "parentId");
	CreateNumber(IdCreature4, "parentId");
	CreateNumber(IdCreature5, "parentId");
	SetNumber(IdCreature1, "parentId", Id);
	SetNumber(IdCreature2, "parentId", Id);
	SetNumber(IdCreature3, "parentId", Id);
	SetNumber(IdCreature4, "parentId", Id);
	SetNumber(IdCreature5, "parentId", Id);
--]]
	CreateNumber(Id, "ReadyNext");
	SetNumber(Id, "ReadyNext", 0);

	CreateNumber(Id, "IsDead");
	SetNumber(Id, "IsDead", 0);
	CreateNumber(Id, "IsInNoReady");
	SetNumber(Id, "IsInNoReady", 1);
end


function AIUpdate_200000008( Id )

	local IsInCombat = GetNumber(Id, "InCombat");
	local PosCount = GetNumber(Id, "MoveCount");
	local NextPosCount = 0;
	local Dead = GetNumber(Id, "IsDead");
	local InCombatCount = GetNumber(Id, "InCombatCount");
	if InCombatCount == 0 and Dead == 0
	then
		if PosCount == 8
		then
			NextPosCount = 0
		else
			NextPosCount = PosCount + 1;
		end

		local NextPosX = GetNumber(Id, "PosMoveToPosX"..NextPosCount);
		local NextPosY = GetNumber(Id, "PosMoveToPosY"..NextPosCount);
		local NextPosZ = GetNumber(Id, "PosMoveToPosZ"..NextPosCount);

		local CurrentPosX = GetNumber(Id, "PosMoveToPosX"..PosCount);
		local CurrentPosY = GetNumber(Id, "PosMoveToPosY"..PosCount);
		local CurrentPosZ = GetNumber(Id, "PosMoveToPosZ"..PosCount);
		local PosX = GetPosX(Id);
		local PosY = GetPosY(Id);
		local PosZ = GetPosZ(Id);
		local O = GetOrientation(Id);
		local AllReadyCount = GetNumber(Id, "AllReady");
		local TotalCount= GetNumber(Id, "TotalCount");
		local MoveReady = GetNumber(Id, "ReadyNext");
		local NoReady = GetNumber(Id, "NoReady");

		if MoveReady == 0
		then
			if PosX == CurrentPosX and PosY == CurrentPosY
			then
				SetNumber(Id, "ReadyNext", 1);
				SetNumber(Id, "AllReady", AllReadyCount + 1);
				SetNumber(Id, "NoReady", NoReady - 1);
				SetNumber(Id, "IsInNoReady", 0);
			end
		elseif MoveReady == 1
		then
			if AllReadyCount == TotalCount
			then
				if PosCount == 8
				then
					PosCount = 0
				else
					PosCount = PosCount + 1;
				end

				CreatureMoveTo2(Id, NextPosX, NextPosY, NextPosZ, O);
				SetNumber(Id, "ReadyNext", 3);
				SetNumber(Id, "MoveCount",PosCount);
				SetNumber(Id, "NoReady", NoReady +1);
				SetNumber(Id, "IsInNoReady", 1);
			end
		elseif MoveReady == 3
		then
			if NoReady == TotalCount
			then
				SetNumber(Id, "AllReady", 0);
				SetNumber(Id, "ReadyNext", 0);
			end
		end
	end

end

function OnFlee_200000008( Id )


end

function OnDestroy_200000008( Id )


end


