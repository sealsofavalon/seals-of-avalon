



function OnCombatStart_PrimaryPatrol(Id, TargetId )		-- on instance start.
	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(Id, "InCombatCount");

	if InCombat == 0
	then
		SetNumber(Id, "InCombatCount", InCombatCount + 1);
	end
	SetNumber(Id, "InCombat",1);


end

function OnCombatStop_PrimaryPatrol( Id, TargetId )		-- instance update every second.

end

function OnReturnFromCombat_PrimaryPatrol(Id)

	SetNumber(Id, "InCombat",0);
	SetNumber(Id, "InCombatCount", GetNumber(Id, "InCombatCount") - 1);
	PosCount = GetNumber(Id, "MoveCount");

	local PosMoveToPosX = GetNumber(Id, "PosMoveToPosX" ..PosCount );
	local PosMoveToPosY = GetNumber(Id, "PosMoveToPosY".. PosCount);
	local PosMoveToPosZ = GetNumber(Id, "PosMoveToPosZ"..PosCount);
	local O = GetOrientation(Id);

	CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);

end

function OnDamageTaken_PrimaryPatrol( Id , TargetAttackerId)		-- on instance start.



end

function OnCastSpell_PrimaryPatrol( IdSpell )		-- on instance start.


end

function OnTargetParried_PrimaryPatrol( Id , TargetId )		-- on instance start.


end

function OnTargetDodged_PrimaryPatrol( Id , TargetId)		-- on instance start.


end

function OnTargetBlocked_PrimaryPatrol( Id , TargetId, iAoumt)		-- on instance start.


end

function OnTargetCritHit_PrimaryPatrol( Id , TargetId, fAoumt )		-- on instance start.


end

function OnTargetDied_PrimaryPatrol( Id , TargetId )		-- on instance start.


end

function OnParried_PrimaryPatrol( Id , TargetId )		-- on instance start.


end

function OnDodged_PrimaryPatrol( Id , TargetId )		-- on instance start.


end

function OnBlocked_PrimaryPatrol( Id , TargetId, iAmount)		-- on instance start.


end

function OnCritHit_PrimaryPatrol( Id, TargetId, fAmount )		-- on instance start.


end

function OnHit_PrimaryPatrol( Id, TargetId, fAmount )		-- on instance start.


end

function OnDied_PrimaryPatrol( Id, KillerId )		-- on instance start.

	SetNumber(Id, "IsDead", 1);

	local NoReady = GetNumber(Id, "NoReady");

	local IsInNoReady = GetNumber(Id,"IsInNoReady");
	if IsInNoReady == 1
	then
		SetNumber(Id, "NoReady", NoReady - 1);
	end

	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(Id, "InCombatCount");

	if InCombat == 1
	then
		SetNumber(Id, "InCombatCount", InCombatCount - 1);
	end

	SetNumber(Id, "TotalCount", GetNumber(Id, "TotalCount") - 1);

	local TotalCount = GetNumber(Id, "TotalCount");
	if TotalCount == 0
	then
		DelNumber(Id);
	else
		DelNumber(Id, "PosMoveToPosX");
		DelNumber(Id, "PosMoveToPosY");
		DelNumber(Id, "PosMoveToPosZ");
		DelNumber(Id, "MoveCount");
		DelNumber(Id, "InCombat");
		DelNumber(Id, "ReadyNext");
		DelNumber(Id, "IsInNoReady");
	end


end

function OnAssistTargetDied_PrimaryPatrol( Id, AssistTarget )		-- on instance start.


end

function OnFear_PrimaryPatrol( Id, FearedId )		-- on instance start.


end

function OnCallForHelp_PrimaryPatrol( Id )		-- on instance start.



end


function calcO(O1, O2)
	local O = O1 + O2;
	if( O > math.pi * 2 )
	then
		O = O -(math.pi * 2);
	end
	return O;

end

function OnLoad_PrimaryPatrol( Id )		-- on instance start.
	RegisterAIUpdateEvent(Id, 100);
	local TotalCount = GetNumber(Id, "TotalCount");
	local OtherCount = TotalCount -1;
	local PosO;
	local SpawnPos;
	local SpawnEntry;
	local tempO;
	local Orientation = GetOrientation(Id);
	local IdCreature1;
	local PosX = GetPosX(Id);
	local PosY = GetPosY(Id);
	local PosZ = GetPosZ(Id);
	CreateNumber(Id, "MoveCount", 0);
	CreateNumber(Id, "InCombat", 0);
	CreateNumber(Id, "AllReady", 0);
	CreateNumber(Id, "InCombatCount", 0);
	CreateNumber(Id, "firstUpdate", 1);
	CreateNumber(Id, "ReadyNext", 0);
	CreateNumber(Id, "IsDead", 0);
	CreateNumber(Id, "IsInNoReady", 1);
	local loop = OtherCount - 1;

	for i = 0, loop, 1
	do
		PosO = GetNumber(Id, "SpawnO"..i);
		SpawnPos = GetNumber(Id, "SpawnPos"..i);
		SpawnEntry = GetNumber(Id, "SpawnEntry"..i);

		tempO = PosO + Orientation;
		if tempO > 6.28
		then
			tempO = tempO - 6.28;
		end

		tempO = 3.14* 3 / 2 - tempO;
		if tempO < 0
		then
			tempO = tempO + 6.28;
		end

--		tempO = calcO(PosO , Orientation);


		MovePosX = math.cos(tempO) * SpawnPos;
		MovePosY = math.sin(tempO) * SpawnPos;
		IdCreature1 = SpawnCreature(Id, SpawnEntry, PosX +MovePosX, PosY +MovePosY, PosZ , tempO);
		CreateNumber(IdCreature1, "parentO", PosO);
		CreateNumber(IdCreature1, "parentOFathar", Orientation);
		CreateNumber(IdCreature1, "parentId", Id);
		CreateNumber(IdCreature1, "parentDistance", SpawnPos);
	end
end


function AIUpdate_PrimaryPatrol( Id )
	local firstUpdate = GetNumber(Id, "firstUpdate");
	if firstUpdate > 0
	then
		CreatureMoveTo2(Id, GetNumber(Id, "PosMoveToPosX0"), GetNumber(Id, "PosMoveToPosY0"), GetNumber(Id, "PosMoveToPosZ0"), GetOrientation(Id));
		SetNumber(Id, "firstUpdate", -1);
		return;
	end;
	local IsInCombat = GetNumber(Id, "InCombat");
	local PosCount = GetNumber(Id, "MoveCount");
	local NextPosCount = 0;
	local Dead = GetNumber(Id, "IsDead");
	local InCombatCount = GetNumber(Id, "InCombatCount");
	local MaxCount = GetNumber(Id, "MaxCount");
	local loopCount = MaxCount -1;
	if InCombatCount == 0 and Dead == 0
	then
		if PosCount == loopCount
		then
			NextPosCount = 0
		else
			NextPosCount = PosCount + 1;
		end

		local NextPosX = GetNumber(Id, "PosMoveToPosX"..NextPosCount);
		local NextPosY = GetNumber(Id, "PosMoveToPosY"..NextPosCount);
		local NextPosZ = GetNumber(Id, "PosMoveToPosZ"..NextPosCount);

		local CurrentPosX = GetNumber(Id, "PosMoveToPosX"..PosCount);
		local CurrentPosY = GetNumber(Id, "PosMoveToPosY"..PosCount);
		local CurrentPosZ = GetNumber(Id, "PosMoveToPosZ"..PosCount);
		local PosX = GetPosX(Id);
		local PosY = GetPosY(Id);
		local PosZ = GetPosZ(Id);
		local O = GetOrientation(Id);
		local AllReadyCount = GetNumber(Id, "AllReady");
		local TotalCount= GetNumber(Id, "TotalCount");
		local MoveReady = GetNumber(Id, "ReadyNext");
		local NoReady = GetNumber(Id, "NoReady");

		local TempX = PosX - CurrentPosX;
		local TempY = PosY - CurrentPosY;
		if MoveReady == 0
		then
			if TempX < 1 and TempX > -1 and TempY < 1 and TempY > -1
			then
				SetNumber(Id, "ReadyNext", 1);
				SetNumber(Id, "AllReady", AllReadyCount + 1);
				SetNumber(Id, "NoReady", NoReady - 1);
				SetNumber(Id, "IsInNoReady", 0);
			end
		elseif MoveReady == 1
		then
			if AllReadyCount == TotalCount
			then
				if PosCount == loopCount
				then
					PosCount = 0
				else
					PosCount = PosCount + 1;
				end

				CreatureMoveTo2(Id, NextPosX, NextPosY, NextPosZ, O);
				SetNumber(Id, "ReadyNext", 3);
				SetNumber(Id, "MoveCount",PosCount);
				SetNumber(Id, "NoReady", NoReady +1);
				SetNumber(Id, "IsInNoReady", 1);
			end
		elseif MoveReady == 3
		then
			if NoReady == TotalCount
			then
				SetNumber(Id, "AllReady", 0);
				SetNumber(Id, "ReadyNext", 0);
			end
		end
	end

end

function OnFlee_PrimaryPatrol( Id )


end

function OnDestroy_PrimaryPatrol( Id )


end


