


function OnCombatStart_SecondaryPatrol(Id, TargetId )
	local ParentId = GetNumber(Id, "parentId");
	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(ParentId, "InCombatCount");

	if InCombat == 0
	then
		SetNumber(ParentId, "InCombatCount", InCombatCount + 1);
	end
	SetNumber(Id, "InCombat",1);


end

function OnCombatStop_SecondaryPatrol( Id, TargetId )		-- instance update every second.

end

function OnReturnFromCombat_SecondaryPatrol(Id)

	local ParentId = GetNumber(Id, "parentId");
	SetNumber(Id, "InCombat",0);
	SetNumber(ParentId, "InCombatCount", GetNumber(ParentId, "InCombatCount") - 1);
	PosCount = GetNumber(Id, "MoveCount");

	local PosMoveToPosX = GetNumber(Id, "PosMoveToPosX" ..PosCount );
	local PosMoveToPosY = GetNumber(Id, "PosMoveToPosY".. PosCount);
	local PosMoveToPosZ = GetNumber(Id, "PosMoveToPosZ"..PosCount);
	local O = GetOrientation(Id);

	CreatureMoveTo2(Id, PosMoveToPosX, PosMoveToPosY, PosMoveToPosZ, O);

end

function OnDamageTaken_SecondaryPatrol( Id , TargetAttackerId)		-- on instance start.



end

function OnCastSpell_SecondaryPatrol( IdSpell )		-- on instance start.


end

function OnTargetParried_SecondaryPatrol( Id , TargetId )		-- on instance start.


end

function OnTargetDodged_SecondaryPatrol( Id , TargetId)		-- on instance start.


end

function OnTargetBlocked_SecondaryPatrol( Id , TargetId, iAoumt)		-- on instance start.


end

function OnTargetCritHit_SecondaryPatrol( Id , TargetId, fAoumt )		-- on instance start.


end

function OnTargetDied_SecondaryPatrol( Id , TargetId )		-- on instance start.


end

function OnParried_SecondaryPatrol( Id , TargetId )		-- on instance start.


end

function OnDodged_SecondaryPatrol( Id , TargetId )		-- on instance start.


end

function OnBlocked_SecondaryPatrol( Id , TargetId, iAmount)		-- on instance start.


end

function OnCritHit_SecondaryPatrol( Id, TargetId, fAmount )		-- on instance start.


end

function OnHit_SecondaryPatrol( Id, TargetId, fAmount )		-- on instance start.


end

function OnDied_SecondaryPatrol( Id, KillerId )		-- on instance start.

	SetNumber(Id, "IsDead", 1);
	local ParentId = GetNumber(Id, "parentId");
	local NoReady = GetNumber(ParentId, "NoReady");

	local IsInNoReady = GetNumber(Id,"IsInNoReady");
	if IsInNoReady == 1
	then
		SetNumber(ParentId, "NoReady", NoReady - 1);
	end

	local InCombat = GetNumber(Id, "InCombat");
	local InCombatCount = GetNumber(ParentId, "InCombatCount");

	if InCombat == 1
	then
		SetNumber(ParentId, "InCombatCount", InCombatCount - 1);
	end

	SetNumber(ParentId, "TotalCount", GetNumber(ParentId, "TotalCount") - 1);

	local TotalCount = GetNumber(ParentId, "TotalCount");
	if TotalCount == 0
	then
		DelNumber(ParentId);
	end


	DelNumber(Id);

end

function OnAssistTargetDied_SecondaryPatrol( Id, AssistTarget )		-- on instance start.


end

function OnFear_SecondaryPatrol( Id, FearedId )		-- on instance start.


end

function OnCallForHelp_SecondaryPatrol( Id )		-- on instance start.



end


function FcalcO(O1, O2)
	local O = O1 + O2;
	if( O > math.pi * 2 )
	then
		O = O -(math.pi * 2);
	end
	return O;

end

function OnLoad_SecondaryPatrol( Id )		-- on instance start.

	RegisterAIUpdateEvent(Id, 100);

	CreateNumber(Id, "MoveCount", 0);
	CreateNumber(Id, "InCombat", 0);
	CreateNumber(Id, "AllReady", 0);
	CreateNumber(Id, "InCombatCount", 0);
	CreateNumber(Id, "firstUpdate", 1);
	CreateNumber(Id, "ReadyNext", 0);
	CreateNumber(Id, "IsDead", 0);
	CreateNumber(Id, "IsInNoReady", 1);

end


function AIUpdate_SecondaryPatrol( Id )

	local ParentId = GetNumber(Id, "parentId");
	local firstUpdate = GetNumber(Id, "firstUpdate");
	local MaxCount= GetNumber(ParentId, "MaxCount");
	if firstUpdate > 0
	then

		local ParentO = GetNumber(Id, "parentO");
		local parentDistance = GetNumber(Id, "parentDistance");
		local TempPosX;
		local TempPosY;
		local TempPosZ;
		local MoveX;
		local MoveY;
		local TempPosO;
		for i = 0,  MaxCount - 1,  1
		do

			TempPosX = GetNumber(ParentId, "PosMoveToPosX"..i);
			TempPosY = GetNumber(ParentId, "PosMoveToPosY"..i);
			TempPosZ = GetNumber(ParentId, "PosMoveToPosZ"..i);
			TempPosO = GetNumber(ParentId, "PosMoveToPosO"..i);
			local Temp = ParentO + TempPosO;
			local CureentO = Temp;
			if Temp > 6.283
			then
				CureentO = Temp - 6.283;
			end

			CureentO = 3.14* 3 / 2 - CureentO;
			if CureentO < 0
			then
				CureentO = CureentO + 6.28;
			end
			--local CureentO = FcalcO( ParentO, TempPosO);
			MoveY = math.sin(CureentO) * parentDistance;
			MoveX = math.cos(CureentO) * parentDistance;

			CreateNumber(Id, "PosMoveToPosX"..i, TempPosX + MoveX);
			CreateNumber(Id, "PosMoveToPosY"..i, TempPosY + MoveY);
			CreateNumber(Id, "PosMoveToPosZ"..i, TempPosZ);
			CreateNumber(Id, "PosMoveToPosO"..i, TempPosO);

			--CreatNumber(Id, "CureentO"..i, CureentO);
		end

		CreatureMoveTo2(Id, GetNumber(Id, "PosMoveToPosX0"), GetNumber(Id, "PosMoveToPosY0"), GetNumber(Id, "PosMoveToPosZ0"), GetOrientation(Id));
		SetNumber(Id, "firstUpdate", -1);

	return;
	end

	local IsInCombat = GetNumber(Id, "InCombat");
	local PosCount = GetNumber(Id, "MoveCount");
	local NextPosCount = 0;
	local Dead = GetNumber(Id, "IsDead");
	MaxCount = MaxCount - 1;
	local InCombatCount = GetNumber(ParentId, "InCombatCount");
	if InCombatCount == 0 and Dead == 0
	then
		if PosCount == MaxCount
		then
			NextPosCount = 0
		else
			NextPosCount = PosCount + 1;
		end

		local NextPosX = GetNumber(Id, "PosMoveToPosX"..NextPosCount);
		local NextPosY = GetNumber(Id, "PosMoveToPosY"..NextPosCount);
		local NextPosZ = GetNumber(Id, "PosMoveToPosZ"..NextPosCount);

		local CurrentPosX = GetNumber(Id, "PosMoveToPosX"..PosCount);
		local CurrentPosY = GetNumber(Id, "PosMoveToPosY"..PosCount);
		local CurrentPosZ = GetNumber(Id, "PosMoveToPosZ"..PosCount);
		local PosX = GetPosX(Id);
		local PosY = GetPosY(Id);
		local PosZ = GetPosZ(Id);
		local O = GetOrientation(Id);
		local AllReadyCount = GetNumber(ParentId, "AllReady");
		local TotalCount= GetNumber(ParentId, "TotalCount");
		local MoveReady = GetNumber(Id, "ReadyNext");
		local NoReady = GetNumber(ParentId, "NoReady");
		local TempX = PosX - CurrentPosX;
		local TempY = PosY - CurrentPosY;
		if MoveReady == 0
		then
			if TempX < 1 and TempX > -1 and TempY < 1 and TempY > -1
			then
				SetNumber(Id, "ReadyNext", 1);
				SetNumber(ParentId, "AllReady", AllReadyCount + 1);
				SetNumber(ParentId, "NoReady", NoReady - 1);
				SetNumber(Id, "IsInNoReady", 0);
			end
		elseif MoveReady == 1
		then
			if AllReadyCount == TotalCount
			then
				if PosCount == MaxCount
				then
					PosCount = 0
				else
					PosCount = PosCount + 1;
				end

				CreatureMoveTo2(Id, NextPosX, NextPosY, NextPosZ, O);
				SetNumber(Id, "ReadyNext", 3);
				SetNumber(Id, "MoveCount",PosCount);
				SetNumber(ParentId, "NoReady", NoReady +1);
				SetNumber(Id, "IsInNoReady", 1);
			end
		elseif MoveReady == 3
		then
			if NoReady == TotalCount
			then
				SetNumber(ParentId, "AllReady", 0);
				SetNumber(Id, "ReadyNext", 0);
			end
		end
	end

end

function OnFlee_SecondaryPatrol( Id )


end

function OnDestroy_SecondaryPatrol( Id )


end


