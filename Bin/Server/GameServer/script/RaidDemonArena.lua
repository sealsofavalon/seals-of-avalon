--  demon arena --
--  2011/03/01 wufan--

local RaidInstance;
local DemonObjInfo = {};
local ObjSpellCD = {};
local SystemNotice = 0;
local PartisanASpellTime
local NeedCreate = 0;
local IsUseHPSpell = 0;
local FirstJoin = 0;

local BattleRwardIndex = 4;
local InStanceLevelData = {};
	InStanceLevelData[1]= {lv = 20, entryBoss = {110002151, 110002152, 110002153, 110002154, 110002155}};
	InStanceLevelData[2]= {lv = 30, entryBoss = {110002156, 110002157, 110002158, 110002159, 110002160}};
	InStanceLevelData[3]= {lv = 50, entryBoss = {110002161, 110002162, 110002163, 110002164, 110002165}};
	InStanceLevelData[4]= {lv = 70, entryBoss = {110002146, 110002147, 110002148, 110002149, 110002150}};

function F_CreateCreature(index)
	if DemonObjInfo[index] ~= nil
	then
		DemonObjInfo[index].guid = RaidInstance:SpawnCreature(DemonObjInfo[index].entry, DemonObjInfo[index].x, DemonObjInfo[index].y, DemonObjInfo[index].z, DemonObjInfo[index].o, 0 );
		DemonObjInfo[index].state = 1;

	end
end

function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );

	-- GET RANDOM INDEX

	local T = {};
	T[1] = {A = 0.8, B = 0.6, C = 0.4};
	T[2] = {A = 0.8, B = 0.4, C = 0.6};
	T[3] = {A = 0.6, B = 0.8, C = 0.4};
	T[4] = {A = 0.6, B = 0.4, C = 0.8};
	T[5] = {A = 0.4, B = 0.8, C = 0.6};
	T[6] = {A = 0.4, B = 0.6, C = 0.8};

	local MinLv = RaidInstance:GetInstanceMinLevel();
	for i =1, 4, 1
	do
		if InStanceLevelData[i] ~= nil and InStanceLevelData[i].lv == MinLv
		then
			BattleRwardIndex =  i ;
			break;
		end

	end

	local Tindex = LuaRandom(1, 6);

	-- info , entry , Create pos (x, y, z, o), spell , chp(when boss has less than  hp.. create  obj)
	DemonObjInfo[1] = {entry = InStanceLevelData[BattleRwardIndex].entryBoss[1], x = 733.8, y = 813.7, z = 0.0, o = 3.3, guid = 0, spell = 0    , spell_2 = 0,     casting  = 0.0, spellCD = 0.0,  chp = 1.0, 	 	   state = 0, hay = "哈哈"}; -- boss
	DemonObjInfo[2] = {entry = InStanceLevelData[BattleRwardIndex].entryBoss[2], x = 707.4, y = 797.2, z = 0.1, o = 3.5, guid = 0, spell = 61030, spell_2 = 61089, casting  = 3.5, spellCD = 30.0, chp = T[Tindex].A, state = 0, hay = "感受到我武藏大人的恐惧了么！"}; -- a
	DemonObjInfo[3] = {entry = InStanceLevelData[BattleRwardIndex].entryBoss[3], x = 767.5, y = 799.2, z = 0.1, o = 1.2, guid = 0, spell = 61031, spell_2 = 61090, casting  = 3.5, spellCD = 30.0, chp = T[Tindex].B, state = 0, hay = "我丢，我丢，我丢死你！"}; -- b
	DemonObjInfo[4] = {entry = InStanceLevelData[BattleRwardIndex].entryBoss[4], x = 758.5, y = 758.5, z = 0.0, o = 2.5, guid = 0, spell = 61032, spell_2 = 61091, casting  = 3.5, spellCD = 30.0, chp = T[Tindex].C, state = 0, hay = "该死的奴仆们，你们还能感受到魔力的源泉么？"}; -- c
	DemonObjInfo[5] = {entry = InStanceLevelData[BattleRwardIndex].entryBoss[5], x = 702.1, y = 772.4, z = 0.0, o = 4.6, guid = 0, spell = 61033, spell_2 = 61092, casting  = 3.5, spellCD = 30.0, chp = 0.2,         state = 0, hay = "大人，我来为你治疗！"}; -- d

	ObjSpellCD[1] = {spelltime = 0.0, spell2time = 0.0};
	ObjSpellCD[2] = {spelltime = 0.0, spell2time = 0.0};
	ObjSpellCD[3] = {spelltime = 0.0, spell2time = 0.0};
	ObjSpellCD[4] = {spelltime = 0.0, spell2time = 0.0};
	ObjSpellCD[5] = {spelltime = 0.0, spell2time = 0.0};

	FirstJoin = RaidInstance:CreateEvent(10000, 3);

end
function OnUnitDie( VictimID, AttackerID )
	for i = 1, 5, 1
	do
		if VictimID == DemonObjInfo[i].guid
		then
			DemonObjInfo[i].state = 0 ;
			break ;
		end
	end
end

function F_CreatureUseSpell(index, Time)
	if DemonObjInfo[index] ~= nil and DemonObjInfo[index].guid ~= 0 and DemonObjInfo[index].state == 1
	then
		if Time - ObjSpellCD[index].spelltime >=  DemonObjInfo[index].spellCD and  Time - ObjSpellCD[index].spell2time >= DemonObjInfo[index].casting
		then
			ObjSpellCD[index].spelltime = Time;
			if index == 2
			then
				RaidInstance:CreatureCastSpell( DemonObjInfo[index].guid, DemonObjInfo[index].spell, 6, GetRacePlayer(4), 0);
			end

			if index == 3
			then
				RaidInstance:CreatureCastSpell( DemonObjInfo[index].guid, DemonObjInfo[index].spell, 6, GetRacePlayer(1), 0);
			end

			if index == 4
			then
				RaidInstance:CreatureCastSpell( DemonObjInfo[index].guid, DemonObjInfo[index].spell, 6, GetRacePlayer(3), 0);
			end

			if index == 5
			then
				if DemonObjInfo[1].state == 1
				then
					RaidInstance:CreatureCastSpell( DemonObjInfo[index].guid, DemonObjInfo[index].spell, 1, DemonObjInfo[1].guid, 0);
				else
					RaidInstance:CreatureCastSpell( DemonObjInfo[index].guid, DemonObjInfo[index].spell, 4, DemonObjInfo[index].guid, 0);
				end
			end
			RaidInstance:CreatureSay( DemonObjInfo[index].guid, DemonObjInfo[index].hay, 0);
		else
			if Time - ObjSpellCD[index].spelltime >= DemonObjInfo[index].casting and Time - ObjSpellCD[index].spell2time >= DemonObjInfo[index].casting
			then
				ObjSpellCD[index].spell2time = Time

				local Ply =  0 ;
				Ply = RaidInstance:GetUnitTarget(DemonObjInfo[index].guid);
				if Ply == 0
				then
					Ply = GetRacePlayer(LuaRandom(1, 4));
				end
				RaidInstance:CreatureCastSpell( DemonObjInfo[index].guid, DemonObjInfo[index].spell_2, 6, Ply, 0);

			end
		end
	end
end

function OnUpdate( TimeElapsed )

	-- creat obj
	if DemonObjInfo[1] ~= nil and DemonObjInfo[1].guid ~= 0 and NeedCreate == 0
	then
		local hp , mp = RaidInstance:GetCreatureBasicInfo( DemonObjInfo[1].guid);
		for i = 2, 5, 1
		do
			if hp <= DemonObjInfo[i].chp and DemonObjInfo[i].guid == 0
			then
				F_CreateCreature(i);
				if i == 5
				then
					NeedCreate = 1;
				end
			end
		end
	end

	if IsUseHPSpell == 0
	then
		if DemonObjInfo[5].guid ~= 0 and DemonObjInfo[5].state == 1 and TimeElapsed - ObjSpellCD[5].spelltime >= DemonObjInfo[5].casting and  TimeElapsed - ObjSpellCD[5].spell2time >= DemonObjInfo[5].casting
		then
			local dHP, dMP = RaidInstance:GetCreatureBasicInfo( DemonObjInfo[5].guid);
			if dHP <= 0.5
			then
				RaidInstance:CreatureCastSpell(DemonObjInfo[5].guid, 61034, 4,DemonObjInfo[5].guid, 0);
				IsUseHPSpell = 1;
			end
		end
	end

	-- DemonObjs use spell
	F_CreatureUseSpell(2, TimeElapsed);
	F_CreatureUseSpell(3, TimeElapsed);
	F_CreatureUseSpell(4, TimeElapsed);
	F_CreatureUseSpell(5, TimeElapsed);

	-- DemonObj[5] only casting once

end

function GetRacePlayer( Class )
	local PlayerID = RaidInstance:GetRandomPlayerByClass(  Class ) ;

	if PlayerID == 0
	then
			local Count = RaidInstance:RandomShufflePlayers();
			for i = 1, Count, 1
			do
				PlayerID = RaidInstance:GetNextRandomShufflePlayer();
				return PlayerID;
			end
	end
	return PlayerID;
end

function OnEvent(  EventID )
	if EventID == FirstJoin
	then
		StartNotice();
	end
end

function StartNotice()
		if SystemNotice == 0
		then
			RaidInstance:SendSystemNotify( "30秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 12;
		elseif SystemNotice == 12
		then
			RaidInstance:SendSystemNotify( "20秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 11;
		elseif SystemNotice == 11
		then
			RaidInstance:SendSystemNotify( "10秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 10;
			RaidInstance:KillEvent(FirstJoin);
			FirstJoin = RaidInstance:CreateEvent(1000, 10);
		elseif SystemNotice == 10
		then
			--RaidInstance:SendSystemNotify( "9秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 9;
		elseif SystemNotice == 9
		then
			--RaidInstance:SendSystemNotify( "8秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 8;
		elseif SystemNotice == 8
		then
			--RaidInstance:SendSystemNotify( "7秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 7;
		elseif SystemNotice == 7
		then
			--RaidInstance:SendSystemNotify( "6秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 6;
		elseif SystemNotice == 6
		then
			RaidInstance:SendSystemNotify( "5秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 5;
		elseif SystemNotice == 5
		then
			RaidInstance:SendSystemNotify( "4秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 4;
		elseif SystemNotice == 4
		then
			RaidInstance:SendSystemNotify( "3秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 3;
		elseif SystemNotice == 3
		then
			RaidInstance:SendSystemNotify( "2秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 2;
		elseif SystemNotice == 2
		then
			RaidInstance:SendSystemNotify( "1秒后，玉能神器将召唤BOSS!" );
			SystemNotice = 1;
		elseif SystemNotice == 1
		then
			if  DemonObjInfo[1].guid == 0
			then
				--RaidInstance:SendSystemNotify( "玉能神器已经召唤BOSS!" );
				F_CreateCreature(1);
			end
			RaidInstance:KillEvent(FirstJoin);
			FirstJoin =0;
			SystemNotice = 0;
		end
end
