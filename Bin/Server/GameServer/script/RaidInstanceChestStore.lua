--Author Jane Chen
--3/25/2011
--

local RaidInstance;
local Bourne = 0;
local GossipIndexStart = 0;
local SpawnChestEvent = 0;
local SpawnChestMap = 0;
local CurrentChestID = 1;
local ChestObjectMap = 0;
local WipeEvent = 0;
local player = 0;

function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );
	Bourne = RaidInstance:SpawnCreature( 3023, 1790.1, 1282.0, 40.1, 6.3, 0 );
	RaidInstance:SwitchCreatureAIMode( Bourne );
	RaidInstance:ModifyObjectFaction( Bourne, 10000 );
	GossipIndexStart = RaidInstance:InsertNPCGossip( Bourne, "let's start!" );
	SpawnChestMap = RaidInstance:ContainerMapCreate();
	ChestObjectMap = RaidInstance:ContainerMapCreate();
	
	local pos = 0;
	pos = RaidInstance:CreatePosition( 1790.3, 1236.0, 40.7, 4.32 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 1, pos );
	
	pos = RaidInstance:CreatePosition( 1809.5, 1236.2, 40.3, 3.7 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 2, pos );

	pos = RaidInstance:CreatePosition( 1807.5, 1224.1, 43.4, 4.7 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 3, pos );

	pos = RaidInstance:CreatePosition( 1808.1, 1205.5, 48.4, 2.1 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 4, pos );
	
	pos = RaidInstance:CreatePosition( 1765.3, 1239.7, 40.0, 3.39 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 5, pos );
	
	pos = RaidInstance:CreatePosition( 1765.0, 1288.9, 40.0, 2.7 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 6, pos );

	pos = RaidInstance:CreatePosition( 1788.6, 1294.4, 40.0, 5.4 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 7, pos );

	pos = RaidInstance:CreatePosition( 1796.4, 1305.9, 40.0, 4.4 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 8, pos );

	pos = RaidInstance:CreatePosition( 1793.4, 1328.0, 40.0, 0.8 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 9, pos );

	pos = RaidInstance:CreatePosition( 1777.3, 1338.3, 40.5, 0.6 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 10, pos );

	pos = RaidInstance:CreatePosition( 1764.2, 1331.4, 40.0, 1.2 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 11, pos );

	pos = RaidInstance:CreatePosition( 1776.9, 1303.1, 40.0, 5.2 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 12, pos );

	pos = RaidInstance:CreatePosition( 1777.9, 1282.0, 40.0, 0.1 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 13, pos );

	pos = RaidInstance:CreatePosition( 1804.4, 1281.3, 40.0, 6.2 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 14, pos );

	pos = RaidInstance:CreatePosition( 1822.5, 1279.5, 40.3, 0.4 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 15, pos );

	pos = RaidInstance:CreatePosition( 1818.5, 1265.2, 40.0, 0.1 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 16, pos );

	pos = RaidInstance:CreatePosition( 1801.0, 1259.3, 40.0, 2.6 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 17, pos );

	pos = RaidInstance:CreatePosition( 1789.8, 1248.8, 40.1, 1.0 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 18, pos );

	pos = RaidInstance:CreatePosition( 1777.5, 1212.6, 48.4, 2.3 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 19, pos );
	
	pos = RaidInstance:CreatePosition( 1771.9, 1250.8, 40.0, 3.0 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 20, pos );

	pos = RaidInstance:CreatePosition( 1782.3, 1263.0, 40.0, 5.2 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 21, pos );
	
	pos = RaidInstance:CreatePosition( 1818.8, 1294.3, 40.0, 2.1 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 22, pos );

	pos = RaidInstance:CreatePosition( 1739.3, 1293.6, 41.7, -1.2 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 23, pos );

	pos = RaidInstance:CreatePosition( 1758.3, 1314.1, 40.2, 4.4 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 24, pos );

	pos = RaidInstance:CreatePosition( 1763.6, 1208.8, 52.1, 2.2 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 25, pos );

	pos = RaidInstance:CreatePosition( 1735.2, 1232.9, 52.2, 2.7 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 26, pos );

	pos = RaidInstance:CreatePosition( 1724.9, 1261.5, 54.0, 2.4 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 27, pos );

	pos = RaidInstance:CreatePosition( 1708.8, 1291.4, 56.8, 2.8 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 28, pos );

	pos = RaidInstance:CreatePosition( 1701.2, 1322.5, 54.9, 3.1 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 29, pos );

	pos = RaidInstance:CreatePosition( 1718.0, 1327.6, 51.1, 5.5 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 30, pos );
	
	pos = RaidInstance:CreatePosition( 1730.5, 1316.3, 44.9, 5.2 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 31, pos );
	
	pos = RaidInstance:CreatePosition( 1754.6, 1304.7, 40.0, 5.1 );
	RaidInstance:ContainerMapInsert( SpawnChestMap, 32, pos );
end

function OnPlayerEnterInstance( PlayerID )
	RaidInstance:AddBuffToPlayer( PlayerID, 96 );
	player = PlayerID;
end

function OnPlayerLeaveInstance( PlayerID )
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 96 );
end

function OnUnitDie( ObjectID )
	WipeEvent = RaidInstance:CreateEvent( 10 * 1000, 1 );
end

function OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )
	if NPC_ID == Bourne and GossipIndex == GossipIndexStart
	then
		RaidInstance:ModifyObjectFaction( Bourne, 10 );
		RaidInstance:EnableNPCGossip( Bourne, GossipIndexStart, 0 );
		SpawnChestEvent = RaidInstance:CreateEvent( 10 * 1000, 32 );
	end
end

function OnGameObjectTrigger( GameObjectID, PlayerID )
	local r = LuaRandom( 1, 100 );
	if r <= 20
	then
		RaidInstance:CreatureCastSpell( Bourne, 94, 1, PlayerID, 1 );
	elseif r <= 40
	then
		RaidInstance:CreatureCastSpell( Bourne, 95, 1, PlayerID, 1 );
	elseif r <= 60
	then
		local x, y, z, o = RaidInstance:GetObjectPosition( GameObjectID );
		RaidInstance:SpawnCreature( 3069, x, y, z, o, 0 );
	end	
end

function OnUpdate( TimeElapsed )
end

function OnEvent( EventID, Attach, Attach2, Attach3, Attach4, Attach5 )
	if EventID == SpawnChestEvent
	then
		local key, pos = RaidInstance:ContainerMapRandom( SpawnChestMap );
		local x, y, z, o = RaidInstance:GetPosition( pos );
		local obj = RaidInstance:SpawnGameObject( 10114, x, y, z, o, 0, 0, 0, 1, 1 );
		RaidInstance:ContainerMapInsert( ChestObjectMap, obj, 1 );
		RaidInstance:ContainerMapErase( SpawnChestMap, key );
	elseif EventID == WipeEvent
	then
		RaidInstance:EjectPlayerFromInstance( player );
	end
end
