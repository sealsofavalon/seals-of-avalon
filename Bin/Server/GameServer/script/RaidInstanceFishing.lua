
local A_LEFT = 1;
local A_TOP = 2;
local A_RIGHT = 3;
local A_BOTTOM = 4;
local A_MAIN = 5;

local RaidInstance;	-- raid instance object
local quotiety = 1.0;
local L_SystemGold = 0;

local L_FishDataTable = {};
local L_FishList = {};
local L_PlayerList = {};
local L_SpellTable = {};
local L_GameStateTable = {};
local L_CurGameState = nil;
local L_CurGameIndex = 0;
local L_EventCallList = {};

local Map_PlayerOldShape;
local Map_FishRandomSeed;

local BullSellerID = 0;
local BullSellerGossip = {};

local AreaOffset = 2;
local L_rcAreaTable = {};
local rcMiddArea =   {left = 1211, top = 1722, right = 1326, bottom = 1789};
local L_AreaSize = {};

---------------------------------------Class------------------------------------------------------
	local CFish = {
	               New = function(self, type, x, y, z)
	                   z = RaidInstance:GetLandHeight( x, y, 0.0 );
					   local CreatureID = RaidInstance:SpawnCreature( L_FishDataTable[type].TableID, x, y, z, 3.3, 10 );
	                   RaidInstance:SwitchCreatureAIMode( CreatureID );
	                   RaidInstance:ModifyObjectScale( CreatureID, L_FishDataTable[type].Scale );

	                   L_CurGameState.CurFishCount = L_CurGameState.CurFishCount + 1;

                       local o = {};
				       setmetatable(o, {__index = self});
				       o.ID = CreatureID;
				       o.Type = type;
				       o.Data = L_FishDataTable[type];
					   L_FishList[ CreatureID ] = o;
                       return L_FishList[ CreatureID ];
                   end,

	               ID = 0,
	               Data = 0,
	               Type = 0,
				   MoveStep = 0,
	               DeadEvent = 0,
	               IsDead = 0,
	               NeedDelete = 0,
				   PathList = 0,

				   SetMoveStep = function(self, Step)
				       slef.MoveStep = Step;
				   end,
				   GetMoveStep = function(self)
				       return self.MoveStep;
				   end,
				   DecMoveStep = function(self)
				       self.MoveStep = slef.MoveStep - 1;
				   end,


				   IsHit = function(self, SpellEntry )
                       local SpellHitRat = L_SpellTable[SpellEntry].HitRate;
	                   local InputGold   = L_SpellTable[SpellEntry].NeedsGold;
	                   local OutputGold  = self.Data.Gold;
	                   local GoldQuotiety = InputGold / OutputGold;

					   GoldQuotiety = GoldQuotiety * quotiety;
					   GoldQuotiety = GoldQuotiety * SpellHitRat;

	                   local FinalRat = GoldQuotiety * 1000;  --转换成千分比

	                   if FinalRat >= 900
					   then
	                       FinalRat = 900;
	                   end

 	                   local ret = RandomRat1000( FinalRat );

	                   if ( ret > 0 )
	                   then
	                       return true;
					   end

	                   return false;
				   end,

				   OnFishHitWithSpell = function(self, SpellEntry, AttackerID)
                       local Gold = self.Data.Gold;
                       local NeedsGold = L_SpellTable[SpellEntry].NeedsGold;
	                   local FishQt = self.Data.FearQt;

	                   if ( self:IsHit(SpellEntry) == true )
	                   then
		                   RaidInstance:CreatureStopMove( self.ID );
					       local SpellID =  self.Data.GSID;
					       RaidInstance:CreatureCastSpell( self.ID, SpellID, 1, AttackerID, 1 );
 	                       RaidInstance:KillCreature( self.ID );
 	                       self.IsDead = 1;
 	                       RaidInstance:DisplayTextOnObjectHead( self.ID, "X"..RaidInstance:ConvertInteger2String( Gold ), AttackerID );

 	                       L_SystemGold = L_SystemGold - Gold;
	                   else
	                       local run = LuaRandom(0, 100);
	                       if run < FishQt
	                       then
		                       RaidInstance:AddBuffToUnit( self.ID, 13 );
						   end
	                   end
                   end,

                   RunAway = function(self)
                       local x,y,z = GetAreaRandomPos(A_LEFT, A_BOTTOM);
                       self:MoveTo(x, y, z, 1);
                   end,


				   Dead = function(self, SpellEntry, AttackerID)
				       if AttackerID ~= nil
					   then
					   self.Data.DeadEvent( SpellEntry, AttackerID, self.ID );
					   end

					   if L_CurGameState ~= nil
					   then
					       L_CurGameState.CurFishCount = L_CurGameState.CurFishCount - 1;
					       local fish = self;
					       L_CurGameState:OnFishDead(fish);
					       if L_CurGameState.CurFishCount == 0
					       then
					           RaidInstance:CreatureSay( BullSellerID, "开始下一个模式", 0);
					           CreateEvent( 3000, 1, 0, SwitchNextGameState, nil);--SystemMessage("L_CurGameState.CurFishCount == 0");
					       end

					   end

					   RaidInstance:CreatureStopMove( self.ID );
					   RaidInstance:DespawnCreature( self.ID );
					   L_FishList[self.ID] = nil;
				   end,

				   RandomMove = function ( self , bRun, StartArea, EndArea)
	                   local x,y,z = GetAreaRandomPos(StartArea, EndArea);
					   RaidInstance:CreatureMoveTo( self.ID, x, y, z, 1, bRun );
				   end,

				   MoveTo = function(self, x, y, z, bRun)
				       RaidInstance:CreatureMoveTo( self.ID, x, y, z, 1, bRun );
				   end
				};

--------------------------------------------------------------------------------------------------
function SystemMessage( message)
    RaidInstance:SendSystemNotify( message );
end

function RandomRat100( rat )
    local ret = LuaRandom(0, 100);

    if ( ret < rat )
    then
        return 1;
    end

    return 0;
end

function RandomRat1000( rat )
    local ret = LuaRandom(0, 1000);

    if ( ret < rat )
    then
        return 1;
    end

    return 0;
end

function GetRandomArea(startArea, EndArea)
    local AreaID = LuaRandom( startArea, EndArea );
	return L_rcAreaTable[AreaID];
end

function GetAreaRandomPos(startArea, EndArea)
    local AreaID = LuaRandom( startArea, EndArea );
	local Area = L_rcAreaTable[AreaID];
	local y = LuaRandom( Area.left, Area.right ); --x
	local x = LuaRandom( Area.top, Area.bottom ); --y
	local z = RaidInstance:GetLandHeight( x, y, 0.0 );

	return x,y,z;
end

function GetRandomAreaAndPos( startArea, EndArea )
	local x,y,z = GetAreaRandomPos(startArea, EndArea);
	return x,y,z;
end

function MapFind(Map, Key)
    return RaidInstance:ContainerMapFind( Map, Key );
end

function MapInsert(Map, Key, Data)
    RaidInstance:ContainerMapInsert( Map, Key, Data );
end

function MapErase(Map, Key)
	RaidInstance:ContainerMapErase(Map, Key);
end

function MapShuffle(Map)
    RaidInstance:ContainerMapShuffle(Map);
end

function MapRandom(Map)
    return RaidInstance:ContainerMapRandom(Map);
end

function CreateEvent(DelayTime, count, attach, Func, EndFun)
    local EventID = RaidInstance:CreateEvent( DelayTime, count, attach );
	L_EventCallList[EventID] = {
	                   ID = EventID,
					   Count = count,
	                   CallBack = Func,
					   EndCall = EndFun,

					   DoEvent = function(self, Param0, Param1, Param2, Param3, Param4)
					       if self.CallBack ~= nil
	                       then
					           self.CallBack(Param0, Param1, Param2, Param3, Param4);
					       end

		                   self.Count = self.Count - 1;
					       if self.Count <= 0
		                   then
		                       if self.EndCall ~= nil
							   then
							       self.EndCall();
							   end
							   RaidInstance:KillEvent(self.ID);
							   L_EventCallList[self.ID] = nil;
		                   end
					   end,
					  };
end
-------------------------------------------------------------------------------------------------------
function OnUnitEnterCombat( UnitID )

end

function CreateNewFish( type, x, y, z)
	local fish = CFish:New(type, x, y, z);
	return fish;
end

function BornFish(type)
	local x,y,z = GetAreaRandomPos(A_LEFT, A_BOTTOM);
	local fish = CreateNewFish(type, x, y, z);
	return fish;
end

function BornRandomFish()
	local x,y,z = GetAreaRandomPos(A_LEFT, A_BOTTOM);
	local key, type = MapRandom( Map_FishRandomSeed );
	local fish = CreateNewFish(type, x, y, z);
	return fish;
end

function CreateFishRandomSeed()
    local Index = 0;

    for i = 0, 50, 1
	do
	    MapInsert(Map_FishRandomSeed, Index, 0); --鱼1
	    Index = Index + 1;
	end

	for i = 0, 40, 1
	do
	    MapInsert(Map_FishRandomSeed, Index, 1);--鱼2
	    Index = Index + 1;
	end

	for i = 0, 30, 1
	do
	    MapInsert(Map_FishRandomSeed, Index, 2);--鱼3
	    Index = Index + 1;
	end

	for i = 0, 20, 1
	do
	    MapInsert(Map_FishRandomSeed, Index, 3);--鱼4
	    Index = Index + 1;
	end

	for i = 0, 15, 1
	do
	    MapInsert(Map_FishRandomSeed, Index, 4);--寄居蟹-歪歪
	    Index = Index + 1;
	end

	for i = 0, 10, 1
	do
	    MapInsert(Map_FishRandomSeed, Index, 5);--乌龟
	    Index = Index + 1;
	end


	for i = 0, 9, 1
	do
	    MapInsert(Map_FishRandomSeed, Index, 6);--螃蟹
	    Index = Index + 1;
	end

	for i = 0, 4, 1
	do
	    MapInsert(Map_FishRandomSeed, Index, 7);--海星
	    Index = Index + 1;
	end

	for i = 0, 1, 1
	do
	    MapInsert(Map_FishRandomSeed, Index, 8);--鲨鱼
	    Index = Index + 1;
	end


	MapShuffle( Map_FishRandomSeed );
	MapShuffle( Map_FishRandomSeed );
	MapShuffle( Map_FishRandomSeed );
	MapShuffle( Map_FishRandomSeed );

end

---------------------------------------------------EventFunction----------------------------------------------------
function OnDefaultDead(SpellEntry, AttackerID, VictimID)
end

function OnStarFishDead(SpellEntry, AttackerID, VictimID)
end

function OnSharkDead(SpellEntry, AttackerID, VictimID)
    RaidInstance:ItemReward( AttackerID, 73, 1 );
end
-------------------------------------------------Create Fish CallBack List----------------------------------------------
function OnClearFish(Param0, Param1, Param2, Param3, Param4)
    local GameState = L_GameStateTable[Param0];
	if GameState == nil
	then
	    return;
	end;

	GameState:ClearFish(1);

    for i,fish in pairs(L_FishList) do
        fish:RunAway();
	end
end

function OnCreateFish(Param0, Param1, Param2, Param3, Param4)
    local GameState = L_GameStateTable[Param0];
	if GameState == nil
	then
	    return;
	end;

	if GameState.OnCreateFish ~= nil
	then
	    GameState:OnCreateFish();
	end
end

function _CreateRandomFish(GameState)
		local fish = BornRandomFish();
		if GameState.IsClearFish == 1
		then
		    fish:RunAway();
		else
		    fish:RandomMove(0, A_MAIN, A_MAIN);
		end

end

function _CreateFish(GameState)
		local fish = BornFish(GameState.BornFishType);
		if GameState.IsClearFish == 1
		then
		    fish:RunAway();
		else
		    fish:RandomMove(0, A_MAIN, A_MAIN);
		end
end

local Dir1X = 1208+5;
local Dir1Y = 1740;

function CoverAxes(x, y)
    return y, x;
end

function _CreateDirFish_1(GameState)

    local bRandomFish = LuaRandom(0, 1);

    for i = 0, 5, 1
    do
        local sx, sy, sz = 0;
        sx = Dir1X;
        sy = Dir1Y + i * 8;

        local ex, ey, ez = 0;
        ex = sx + L_AreaSize.width - 10;
        ey = sy;

       --sx,zy = CoverAxes(sx, sy);
       --ex,ey = CoverAxes(ex, ey);
        ez = RaidInstance:GetLandHeight( ey, ex, 0.0 );
		local fish = nil;
		if bRandomFish == 1
		then
		    fish = CreateNewFish(GameState.BornFishType, sy, sx, sz);
		else
		    local key, type = MapRandom( Map_FishRandomSeed );
		    fish = CreateNewFish(type, sy, sx, sz);
		end

        fish:MoveTo(ey, ex, ez, 1);
    end

end

----------------------------------------------------------------------------------------------------------------------------------
function SwitchNextGameState()
    L_CurGameState:ReSet();

    L_CurGameIndex = L_CurGameIndex + 1;
    if L_CurGameIndex > 2
    then
        L_CurGameIndex = 0;
    end

    L_CurGameState = L_GameStateTable[L_CurGameIndex];
	L_CurGameState:OnStart();
end

function _OnFishMoveArrival(GameState, fish)
	 if GameState.IsClearFish == 1
	 then
         fish:Dead(0, 0);
	 else
	    if fish.NeedDelete == 1
	    then
	        fish:Dead(0, 0);
	        return;
	    end

		if fish:GetMoveStep() == 0
		then
			fish.NeedDelete = 1;
			local x,y,z = GetAreaRandomPos(A_LEFT, A_BOTTOM);
			fish:RandomMove(0, A_LEFT, A_BOTTOM);
		else
			local bRun = RandomRat100(30);
			fish:DecMoveStep();
			fish:RandomMove( bRun, A_MAIN, A_MAIN);
		end
	end
end


function InitGameState()

    L_GameStateTable[0] =
						 {
						 Name = "NormalMode-RandomFish",
						 IsClearFish = 0,
						 ID = 0,
						 CurFishCount = 0,
						 BornFishType = 0,
						 OnCreateFish = 0,

						 ClearFish = function(self, bClear)
						     self.IsClearFish = bClear;
						 end,

						 ReSet = function(self)
						     self.IsClearFish = 0;
						     self.CurFishCount = 0;
						     self.BornFishType = 0;
						 end,

						 OnStart = function(self)
						       self:ReSet();
						       self.OnCreateFish = _CreateRandomFish;
							   CreateEvent( 50, 60, self.ID, OnCreateFish, nil);
							   CreateEvent( 1000*90, 1, self.ID, OnClearFish, nil);
						 end,

						 OnFishDead = function(self, fish)
	                         if self.IsClearFish ~= 1
	                         then
		                         local fish = BornRandomFish();
								 fish:RandomMove(0, A_MAIN, A_MAIN);
	                         end
						 end,

						 OnFishMoveArrival = function(self, fish)
						     _OnFishMoveArrival(self, fish);
						 end,
						 };



	L_GameStateTable[1] =
					 {
					 Name = "NormalMode-SameFish",
					 IsClearFish = 0,
					 ID = 1,
					 CurFishCount = 0,
					 BornFishType = 0,
					 OnCreateFish = 0,

					 ClearFish = function(self, bClear)
						 self.IsClearFish = bClear;
					 end,

					 ReSet = function(self)
						  self.IsClearFish = 0;
						  self.CurFishCount = 0;
						  self.BornFishType = 0;
					 end,

					 OnStart = function(self)
					     local FishType = LuaRandom(5, 8);
					     self.BornFishType = FishType;
					     local FishCount = FishType + (8 - FishType)*5;

					     self.OnCreateFish = _CreateFish;
						 CreateEvent( 50, FishCount, self.ID, OnCreateFish, nil);
						 CreateEvent( 50*1000, 1, self.ID, OnClearFish, nil);
					 end,

					 OnFishDead = function(self, fish)
	                     if self.IsClearFish ~= 1
	                     then
		                     local fish = BornFish(self.BornFishType);
							 fish:RandomMove(0, A_MAIN, A_MAIN);
	                     end
					 end,

					 OnFishMoveArrival = function(self, fish)
						 _OnFishMoveArrival(self, fish);
					 end,
					 };

	L_GameStateTable[2] =
					 {
					 Name = "FixDirMode-SameFish",
					 IsClearFish = 0,
					 ID = 2,
					 CurFishCount = 0,
					 BornFishType = 0,
					 OnCreateFish = 0,

					 ClearFish = function(self, bClear)
						 self.IsClearFish = bClear;
					 end,

					 ReSet = function(self)
						  self.IsClearFish = 0;
						  self.CurFishCount = 0;
						  self.BornFishType = 0;
					 end,

					 OnStart = function(self)
					     self:ReSet();
					     self.OnCreateFish = _CreateDirFish_1;
					     self.BornFishType = LuaRandom(2, 8);
						 CreateEvent( 2500, 10, self.ID, OnCreateFish, nil);
					 end,

					 OnFishDead = function(self, fish)
					 end,

					 OnFishMoveArrival = function(self, fish)
						 fish:Dead(0, 0);
					 end,
					 };

end
----------------------------------------------------------------------------------------------------------------------

function OnStart( ID )
	RaidInstance = LuaGetRaidObject( ID );

	L_FishDataTable[0] = {Name = "剑鱼",   TableID = 607000101, Gold = 1,  GSID = 772, Scale = 5.0, FearQt = 30, DeadEvent = OnDefaultDead}
	L_FishDataTable[1] = {Name = "珊瑚鱼", TableID = 607000102, Gold = 2,  GSID = 773, Scale = 4.0, FearQt = 30, DeadEvent = OnDefaultDead}
	L_FishDataTable[2] = {Name = "刺豚",   TableID = 607000103, Gold = 4,  GSID = 774, Scale = 4.0, FearQt = 15, DeadEvent = OnDefaultDead}
	L_FishDataTable[3] = {Name = "燕尾鱼", TableID = 607000104, Gold = 7,  GSID = 775, Scale = 3.0, FearQt = 30, DeadEvent = OnDefaultDead}
	L_FishDataTable[4] = {Name = "居蟹",   TableID = 604000003, Gold = 10, GSID = 776, Scale = 1.0, FearQt = 10, DeadEvent = OnDefaultDead}
	L_FishDataTable[5] = {Name = "乌龟",   TableID = 606000002, Gold = 20, GSID = 777, Scale = 2.0, FearQt = 10, DeadEvent = OnDefaultDead}
	L_FishDataTable[6] = {Name = "螃蟹",   TableID = 606000305, Gold = 40, GSID = 778, Scale = 1.0, FearQt = 30, DeadEvent = OnDefaultDead}
	L_FishDataTable[7] = {Name = "海星",   TableID = 606000306, Gold = 60, GSID = 778, Scale = 1.5, FearQt = 10, DeadEvent = OnStarFishDead}
	L_FishDataTable[8] = {Name = "鲨鱼",   TableID = 607000105, Gold = 100,GSID = 779, Scale = 4.0, FearQt = 60, DeadEvent = OnSharkDead}

	L_SpellTable[328] = {Name = "小小炮", HitRate = 1, NeedsGold = 1};
	L_SpellTable[329] = {Name = "小炮",   HitRate = 1.2, NeedsGold = 2};
	L_SpellTable[330] = {Name = "中炮",   HitRate = 1.4, NeedsGold = 3};
	L_SpellTable[331] = {Name = "大炮",   HitRate = 1.6, NeedsGold = 4};
	L_SpellTable[332] = {Name = "大大炮", HitRate = 1.8, NeedsGold = 5};

	L_rcAreaTable[A_LEFT]   = {left = rcMiddArea.left, top = rcMiddArea.top, right = rcMiddArea.left + AreaOffset, bottom = rcMiddArea.bottom};
    L_rcAreaTable[A_TOP]    = {left = rcMiddArea.left, top = rcMiddArea.top, right = rcMiddArea.right, bottom = rcMiddArea.top + AreaOffset};
    L_rcAreaTable[A_RIGHT]  = {left = rcMiddArea.right - AreaOffset, top = rcMiddArea.top, right = rcMiddArea.right, bottom = rcMiddArea.bottom};
    L_rcAreaTable[A_BOTTOM] = {left = rcMiddArea.left, top = rcMiddArea.bottom - AreaOffset, right = rcMiddArea.right, bottom = rcMiddArea.bottom};
    L_rcAreaTable[A_MAIN]   = {left = rcMiddArea.left, top = rcMiddArea.top, right = rcMiddArea.right, bottom = rcMiddArea.bottom};

    L_AreaSize.width = (rcMiddArea.right+AreaOffset) - (rcMiddArea.left - AreaOffset);
    L_AreaSize.height = (rcMiddArea.bottom+AreaOffset) - (rcMiddArea.top - AreaOffset);


	Map_PlayerOldShape = RaidInstance:ContainerMapCreate();
	Map_FishRandomSeed = RaidInstance:ContainerMapCreate();
	CreateFishRandomSeed();

	InitGameState();
	L_CurGameState = L_GameStateTable[0];
	L_CurGameState:OnStart();

	--Bullet Seller
	BullSellerID = RaidInstance:SpawnCreature( 3083, 1808.6, 1268.9, 0.1, 4.7, 0 );
	RaidInstance:ModifyObjectScale( BullSellerID, 4.0 );

	local GossipID = RaidInstance:InsertNPCGossip( BullSellerID, "开始捕鱼" );
	BullSellerGossip[GossipID] = { CallBack = OnStartFishing };

	GossipID = RaidInstance:InsertNPCGossip( BullSellerID, "结束捕鱼" );
	BullSellerGossip[GossipID] = {CallBack = OnEndFishing};

	--GossipID = RaidInstance:InsertNPCGossip( BullSellerID, "显示系统开心币" );
	--BullSellerGossip[GossipID] = {CallBack = OnShowSystemGold};
end

function OnUpdate( TimeElapsed )		-- instance update every second.

end


function OnUnitHitWithSpell( SpellEntry, AttackerID, VictimID )

    if L_FishList[AttackerID] == nil and L_PlayerList[AttackerID] == nil
    then
        return;
    end

    if L_PlayerList[AttackerID] ~= nil
    then
        if L_FishList[VictimID].IsDead == 1
        then
            return;
        end

		local fish = L_FishList[VictimID];

        fish:OnFishHitWithSpell(SpellEntry, AttackerID);

    elseif L_FishList[AttackerID] ~= nil
    then
	    local player = L_PlayerList[VictimID];
        player:OnPlayerHitWithSpell(SpellEntry, AttackerID);
    end
end

function OnUnitDie( VictimID, AttackerID )
end

function OnCreatureMoveArrival( CreatureID )

    if L_FishList[CreatureID] == nil or L_FishList[CreatureID].IsDead == 1
    then
        return;
    end

	local fish = L_FishList[CreatureID];

	if L_CurGameState ~= nil
	then
	    L_CurGameState:OnFishMoveArrival(fish);
	end

end

function OnEnd()

end

function OnObjectDie( VictimID, AttackerID )
end

function OnGameObjectTrigger( GameObjectID, PlayerID )
end

function OnUnitEnterCombat( PlayerID )
end

function OnStartFishing(PlayerID)

    if L_PlayerList[PlayerID] ~= nil
    then
        return;
    end

	RaidInstance:ModifyPlayerPVPFlag( PlayerID, 2 );

	local OldShape = RaidInstance:GetPlayerShapeShiftSpellEntry( PlayerID );
	MapInsert( Map_PlayerOldShape, PlayerID, OldShape );
	RaidInstance:DismissMountAndShift( PlayerID );

	local shape = LuaRandom(326, 327);
	RaidInstance:AddBuffToPlayer( PlayerID, shape );

    local player = {
                   ID = PlayerID,
                   KillFishList = 0,
                   WinGold = 0,
				   Score = 0,

				   OnPlayerHitWithSpell = function(self, SpellEntry, AttackerID)
	                   local fish = L_FishList[AttackerID];
                       local GoldAmount = fish.Data.Gold;

                       RaidInstance:ItemReward( self.ID, 56, GoldAmount );
                       fish:Dead( SpellEntry, self.ID);
				   end
                   }

   L_PlayerList[PlayerID] = player;

end

function OnEndFishing(PlayerID)

	RaidInstance:ModifyPlayerPVPFlag( PlayerID, 1 );
	local value = RaidInstance:ContainerMapFind( Map_PlayerOldShape, PlayerID );
	if value > 0
	then
	    RaidInstance:AddBuffToPlayer( PlayerID, value );
	end

    RaidInstance:RemoveBuffFromPlayer( PlayerID, 326 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 327 );
	L_PlayerList[PlayerID] = nil;
end

function OnShowSystemGold()
   RaidInstance:SendSystemNotify( "当前系统金钱:"..RaidInstance:ConvertInteger2String( L_SystemGold ) );
end

function OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )

	if NPC_ID == BullSellerID
	then
        BullSellerGossip[GossipIndex].CallBack(PlayerID);
	end
end

function OnRaidWipe()
end

function OnPlayerEnterInstance( PlayerID )
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 328 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 329 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 330 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 331 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 332 );
end

function OnPlayerLeaveInstance( PlayerID )
	OnEndFishing(PlayerID);
end

function OnUnitCastSpell( SpellID, PlayerID )

	if L_PlayerList[PlayerID] == nil or L_SpellTable[SpellID] == nil
    then
        return;
    end

    local NeedsGold = L_SpellTable[SpellID].NeedsGold;

	L_SystemGold = L_SystemGold + NeedsGold;

end


function OnEvent( EventID, Param0, Param1, Param2, Param3, Param4 )

    if L_EventCallList[EventID] == nil
	then
	    return;
	end

	local Event = L_EventCallList[EventID];
	Event:DoEvent(Param0, Param1, Param2, Param3, Param4 );

end
