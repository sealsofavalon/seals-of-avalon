--Author Jane Chen
--12/29/2010
--

local RaidInstance;
local GridMapPosition = 0;
local GridMapSpell = 0;
local GridMapCaster = 0;
local GridMapCastType = 0;
local GridMapTarget = 0;
local GridMapBad = 0;
local GridMapAnimation = 0;
local GridMapAnimationTime = 0;
local GridMapAttach = 0;
local GridMapObject = 0;
local GiftMap = 0;
local DanceAnimationMap = 0;
local Interval = 4.0;
local Bear = 0;
local Robot = 0;
local RollAndMoveGossipIndex = 0;
local CurrentPositionIndex = 1;
local MoveEvent = 0;
local TerminalTargetPositionIndex = 2;
local NextTargetPositionIndex = 2;
local BearMoving = 0;
local Dice = 0;
local MaxIndex = 55;
local GiftCount = 1;
local FactionRestoreSpellID = 0;
local FactionRestore = 0;
local MoveDirection = 1;
local GiftSpawnEvent = 0;
local ChestPositionMap = 0;
local EventAcceptNextRoll = 0;
local EventTimeAOEPoison = 0;
local VoiceAfterRollEvent = 0;
local Roll = 0;
local DiceDropOnTheGroundEvent = 0;
local DancePlayerCount = 0;
local board = 0;

local TargetRandomPlayer = 9900001;	-- constant number
local CasterByIndex = 9900002;		-- constant number

local SpellForward3 = 9900003;		-- constant number
local SpellBack5 = 9900004;			-- constant number
local SpellGoHome = 9900005;		-- constant number
local SpellSummonMobs = 9900006;	-- constant number
local SpellChestReward = 9900007;	-- constant number
local SpellDispelAura = 9900008;	-- constant number

local CenterX = 1789.98;
local CenterY = 1282.32;
local CenterZ = 40.0;
local CenterO = 5.72;
local CenterPosition = 0;

local EventRoll = 0;
local CurrentBearAnimationSpell = 0;
local NeedDanceCount = 0;
local NeedDanceCountPerPlayer = 50;
local BearMoveEvent = 0;
local BearMoveTerminal = 0;
local BearMoveDirection = 0;
local BearDanceEvent = 0;
local DanceCount = 0;
local DanceRightCount = 0;

local GiftAlreadyGive = 0;
local GiftGossipIndex = 999;
local GoodJobGossipIndex = 999;
local DanceTryGossipIndex = 999;
local BearMovingToCopyPosition = 0;
local DestX = 1699.3;
local DestY = 1179.4;
local DestZ = 40.0;
local DestO = 4.7;

local Ready2DanceEvent = 0;
local Dancing = 0;
local CountDownEvent = 0;
local WelcomeEvent = 0;
local CountDownIndex = 5;
local GoldBonus = 0;

function SummonMobs( Entry, Index )
	local Position = RaidInstance:ContainerMapFind( GridMapPosition, Index );
	if Position > 0
	then
		local x, y, z, o = RaidInstance:GetPosition( Position );
		local Count = RaidInstance:RandomShufflePlayers();

		local Mob1 = RaidInstance:SpawnCreature( Entry, x + Interval, y, z, o, 0 );
		local Mob2 = RaidInstance:SpawnCreature( Entry, x, y + Interval, z, o, 0 );
		local Mob3 = RaidInstance:SpawnCreature( Entry, x - Interval, y, z, o, 0 );
		local Mob4 = RaidInstance:SpawnCreature( Entry, x, y - Interval, z, o, 0 );

		for i = 1, Count, 1
		do
			local Target = RaidInstance:GetNextRandomShufflePlayer();
			RaidInstance:ModifyThreat( Mob1, Target, 1 );
			RaidInstance:ModifyThreat( Mob2, Target, 1 );
			RaidInstance:ModifyThreat( Mob3, Target, 1 );
			RaidInstance:ModifyThreat( Mob4, Target, 1 );
		end
	end
end

function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );

	WelcomeEvent = RaidInstance:CreateEvent( 5000, 1 );
	Robot = RaidInstance:SpawnCreature( 3023, 1763.6, 1304.1, 40.0, 5.4, 0 );
	RaidInstance:SetUnitInvisible( Robot, 1 );
	RaidInstance:SwitchCreatureAIMode( Robot );
	RaidInstance:ModifyObjectScale( Robot, 2.0 );

	CenterPosition = RaidInstance:CreatePosition( CenterX, CenterY, CenterZ, CenterO );

	GridMapPosition = RaidInstance:ContainerMapCreate();
	GridMapCaster = RaidInstance:ContainerMapCreate();
	GridMapSpell = RaidInstance:ContainerMapCreate();
	GridMapCastType = RaidInstance:ContainerMapCreate();
	GridMapTarget = RaidInstance:ContainerMapCreate();
	GridMapBad = RaidInstance:ContainerMapCreate();
	GridMapAnimation = RaidInstance:ContainerMapCreate();
	GridMapAnimationTime = RaidInstance:ContainerMapCreate();
	GridMapAttach = RaidInstance:ContainerMapCreate();
	GridMapObject = RaidInstance:ContainerMapCreate();
	GiftMap = RaidInstance:ContainerMapCreate();
	DanceAnimationMap = RaidInstance:ContainerMapCreate();

	RaidInstance:ContainerMapInsert( DanceAnimationMap, 80, 14 );
	RaidInstance:ContainerMapInsert( DanceAnimationMap, 81, 16 );
	RaidInstance:ContainerMapInsert( DanceAnimationMap, 82, 15 );
	RaidInstance:ContainerMapInsert( DanceAnimationMap, 83, 13 );
	RaidInstance:ContainerMapInsert( DanceAnimationMap, 84, 5 );

	Bear = RaidInstance:SpawnCreature( 3009, CenterX, CenterY, CenterZ, CenterO, 0 );
	RaidInstance:SwitchCreatureAIMode( Bear );
	RaidInstance:ModifyObjectScale( Bear, 2.0 );

	Dice = RaidInstance:SpawnCreature( 3024, 1785.6, 1254.2, 40.0, 3.3, 0 );
	RaidInstance:ModifyObjectScale( Dice, 2.0 );

	board = RaidInstance:SpawnCreature( 607000141, 1793.6, 1252.4, 40.2, 0.5, 0 );

	RollAndMoveGossipIndex = RaidInstance:InsertNPCGossip( Dice, "请掷我!" );

	RaidInstance:GeneratePuzzlePositionIntoContainerMap( GridMapPosition, MaxIndex - 1, Interval, CenterX, CenterY, CenterZ, CenterO, 0 );

	local TempSpellShuffle = RaidInstance:ContainerMapCreate();
	local TempCastTypeShuffle = RaidInstance:ContainerMapCreate();
	local TempTargetShuffle = RaidInstance:ContainerMapCreate();
	local TempBadShuffle = RaidInstance:ContainerMapCreate();
	local TempCasterShuffle = RaidInstance:ContainerMapCreate();
	local TempAnimationShuffle = RaidInstance:ContainerMapCreate();
	local TempAnimationTimeShuffle = RaidInstance:ContainerMapCreate();
	local TempAttachShuffle = RaidInstance:ContainerMapCreate();
	local TempObjectShuffle = RaidInstance:ContainerMapCreate();

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 2, SpellChestReward );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 2, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 2, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 2, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 2, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 2, 10 );					-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 2, 3000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 2, 10077 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 2, 3039 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 3, SpellChestReward );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 3, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 3, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 3, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 3, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 3, 10 );					-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 3, 3000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 3, 10076 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 3, 3030 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 4, SpellChestReward );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 4, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 4, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 4, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 4, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 4, 10 );					-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 4, 3000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 4, 10078 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 4, 3049 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 5, SpellChestReward );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 5, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 5, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 5, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 5, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 5, 10 );					-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 5, 3000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 5, 10078 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 5, 3049 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 6, SpellChestReward );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 6, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 6, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 6, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 6, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 6, 10 );					-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 6, 3000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 6, 10084 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 6, 3050 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 7, SpellChestReward );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 7, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 7, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 7, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 7, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 7, 10 );					-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 7, 3000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 7, 10084 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 7, 3050 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 8, SpellChestReward );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 8, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 8, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 8, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 8, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 8, 10 );					-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 8, 3000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 8, 10076 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 8, 3030 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 9, SpellChestReward );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 9, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 9, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 9, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 9, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 9, 10 );					-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 9, 3000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 9, 10076 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 9, 3030 );					-- game object on the grid

	-----

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 10, 62 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 10, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 10, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 10, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 10, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 10, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 10, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 10, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 10, 3035 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 11, 62 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 11, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 11, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 11, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 11, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 11, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 11, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 11, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 11, 3035 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 12, 62 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 12, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 12, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 12, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 12, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 12, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 12, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 12, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 12, 3035 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 13, 64 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 13, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 13, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 13, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 13, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 13, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 13, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 13, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 13, 3037 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 14, 64 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 14, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 14, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 14, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 14, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 14, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 14, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 14, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 14, 3037 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 15, 64 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 15, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 15, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 15, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 15, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 15, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 15, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 15, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 15, 3037 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 16, 63 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 16, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 16, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 16, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 16, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 16, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 16, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 16, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 16, 3036 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 17, 63 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 17, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 17, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 17, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 17, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 17, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 17, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 17, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 17, 3036 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 18, 63 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 18, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 18, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 18, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 18, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 18, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 18, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 18, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 18, 3036 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 19, 65 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 19, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 19, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 19, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 19, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 19, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 19, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 19, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 19, 3038 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 20, 65 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 20, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 20, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 20, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 20, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 20, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 20, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 20, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 20, 3038 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 21, 65 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 21, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 21, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 21, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 21, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 21, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 21, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 21, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 21, 3038 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 22, 67 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 22, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 22, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 22, 1 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 22, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 22, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 22, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 22, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 22, 3051 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 23, 67 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 23, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 23, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 23, 1 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 23, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 23, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 23, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 23, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 23, 3051 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 24, 67 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 24, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 24, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 24, 1 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 24, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 24, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 24, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 24, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 24, 3051 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 25, 68 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 25, 2 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 25, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 25, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 25, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 25, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 25, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 25, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 25, 3044 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 26, 68 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 26, 2 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 26, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 26, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 26, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 26, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 26, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 26, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 26, 3044 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 27, 68 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 27, 2 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 27, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 27, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 27, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 27, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 27, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 27, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 27, 3044 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 28, 71 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 28, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 28, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 28, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 28, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 28, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 28, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 28, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 28, 3047 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 29, 71 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 29, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 29, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 29, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 29, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 29, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 29, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 29, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 29, 3047 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 30, 72 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 30, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 30, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 30, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 30, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 30, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 30, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 30, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 30, 3048 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 31, 72 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 31, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 31, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 31, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 31, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 31, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 31, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 31, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 31, 3048 );					-- game object on the grid

	----

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 32, 69 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 32, 2 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 32, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 32, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 32, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 32, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 32, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 32, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 32, 3045 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 33, 69 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 33, 2 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 33, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 33, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 33, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 33, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 33, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 33, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 33, 3045 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 34, 69 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 34, 2 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 34, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 34, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 34, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 34, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 34, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 34, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 34, 3045 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 35, 69 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 35, 2 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 35, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 35, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 35, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 35, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 35, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 35, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 35, 3045 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 36, 69 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 36, 2 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 36, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 36, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 36, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 36, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 36, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 36, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 36, 3045 );					-- game object on the grid

	----

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 37, 70 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 37, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 37, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 37, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 37, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 37, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 37, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 37, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 37, 3046 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 38, 70 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 38, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 38, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 38, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 38, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 38, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 38, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 38, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 38, 3046 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 39, 70 );					-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 39, 1 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 39, TargetRandomPlayer );	-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 39, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 39, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 39, -1 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 39, -1 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 39, 0 );							-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 39, 3046 );					-- game object on the grid

	---- summon mobs 3010

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 40, SpellSummonMobs );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 40, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 40, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 40, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 40, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 40, 11 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 40, 2000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 40, 3010 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 40, 3033 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 41, SpellSummonMobs );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 41, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 41, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 41, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 41, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 41, 11 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 41, 2000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 41, 3010 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 41, 3033 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 42, SpellSummonMobs );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 42, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 42, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 42, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 42, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 42, 11 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 42, 2000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 42, 3010 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 42, 3033 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 43, SpellSummonMobs );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 43, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 43, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 43, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 43, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 43, 11 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 43, 2000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 43, 3010 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 43, 3033 );					-- game object on the grid

	---- summon mobs 3011

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 44, SpellSummonMobs );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 44, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 44, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 44, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 44, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 44, 11 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 44, 2000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 44, 3011 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 44, 3034 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 45, SpellSummonMobs );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 45, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 45, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 45, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 45, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 45, 11 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 45, 2000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 45, 3011 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 45, 3034 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 46, SpellSummonMobs );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 46, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 46, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 46, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 46, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 46, 11 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 46, 2000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 46, 3011 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 46, 3034 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 47, SpellSummonMobs );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 47, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 47, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 47, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 47, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 47, 11 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 47, 2000 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 47, 3011 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 47, 3034 );					-- game object on the grid

	---- bear forward 3 steps

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 48, SpellForward3 );			-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 48, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 48, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 48, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 48, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 48, 12 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 48, 5500 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 48, 0 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 48, 3032 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 49, SpellForward3 );			-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 49, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 49, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 49, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 49, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 49, 12 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 49, 5500 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 49, 0 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 49, 3032 );					-- game object on the grid

	-- bear backward 5 steps

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 50, SpellBack5 );			-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 50, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 50, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 50, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 50, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 50, 12 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 50, 5500 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 50, 0 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 50, 3031 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 51, SpellBack5 );			-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 51, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 51, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 51, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 51, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 51, 12 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 51, 5500 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 51, 0 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 51, 3031 );					-- game object on the grid

	-- bear go home

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 52, SpellGoHome );			-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 52, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 52, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 52, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 52, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 52, 12 );				-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 52, 5500 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 52, 0 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 52, 3042 );					-- game object on the grid

	-- dispel aura

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 53, SpellDispelAura );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 53, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 53, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 53, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 53, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 53, 7 );					-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 53, 1200 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 53, 1 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 53, 3043 );					-- game object on the grid

	RaidInstance:ContainerMapInsert( TempSpellShuffle, 54, SpellDispelAura );		-- spell id
	RaidInstance:ContainerMapInsert( TempCastTypeShuffle, 54, 3 );					-- cast type 1(SingleTarget) 2(DestinationAOE) 3(SourceAOE) 4(Self)
	RaidInstance:ContainerMapInsert( TempTargetShuffle, 54, Bear );					-- target Bear or TargetRandomPlayer
	RaidInstance:ContainerMapInsert( TempBadShuffle, 54, 0 );						-- harmful to player == 1 or == 0
	RaidInstance:ContainerMapInsert( TempCasterShuffle, 54, Bear );					-- caster
	RaidInstance:ContainerMapInsert( TempAnimationShuffle, 54, 7 );					-- caster play animation id, do not play when -1
	RaidInstance:ContainerMapInsert( TempAnimationTimeShuffle, 54, 1200 );			-- caster play animation time, do not play when -1
	RaidInstance:ContainerMapInsert( TempAttachShuffle, 54, 1 );					-- attach value, chest entry or mob entry or dispel cuont
	RaidInstance:ContainerMapInsert( TempObjectShuffle, 54, 3043 );					-- game object on the grid


	local Count = RaidInstance:ContainerMapShuffle( TempSpellShuffle );
	for i = 1, Count, 1
	do
		local key, value = RaidInstance:ContainerMapNext( TempSpellShuffle );
		RaidInstance:ContainerMapInsert( GridMapSpell, i + 1, value );

		value = RaidInstance:ContainerMapFind( TempCastTypeShuffle, key );
		RaidInstance:ContainerMapInsert( GridMapCastType, i + 1, value );

		value = RaidInstance:ContainerMapFind( TempTargetShuffle, key );
		RaidInstance:ContainerMapInsert( GridMapTarget, i + 1, value );

		value = RaidInstance:ContainerMapFind( TempBadShuffle, key );
		RaidInstance:ContainerMapInsert( GridMapBad, i + 1, value );

		value = RaidInstance:ContainerMapFind( TempCasterShuffle, key );
		RaidInstance:ContainerMapInsert( GridMapCaster, i + 1, value );

		value = RaidInstance:ContainerMapFind( TempAnimationShuffle, key );
		RaidInstance:ContainerMapInsert( GridMapAnimation, i + 1, value );

		value = RaidInstance:ContainerMapFind( TempAnimationTimeShuffle, key );
		RaidInstance:ContainerMapInsert( GridMapAnimationTime, i + 1, value );

		value = RaidInstance:ContainerMapFind( TempAttachShuffle, key );
		RaidInstance:ContainerMapInsert( GridMapAttach, i + 1, value );

		value = RaidInstance:ContainerMapFind( TempObjectShuffle, key );

		local Position = RaidInstance:ContainerMapFind( GridMapPosition, i + 1 );
		local x, y, z, o = RaidInstance:GetPosition( Position );
		local ObjectID = RaidInstance:SpawnCreature( value, x, y, z, o, 0 );
		RaidInstance:ModifyObjectScale( ObjectID, 1.5 );
		RaidInstance:ContainerMapInsert( GridMapObject, i + 1, ObjectID );
	end

	DanceTryGossipIndex = RaidInstance:InsertNPCGossip( Bear, BuildLanguageString( 97 ) );
	GiftGossipIndex = RaidInstance:InsertNPCGossip( Bear, BuildLanguageString( 98 ) );
	RaidInstance:EnableNPCGossip( Bear, DanceTryGossipIndex, 0 );
	RaidInstance:EnableNPCGossip( Bear, GiftGossipIndex, 0 );

	RaidInstance:ContainerMapInsert( GiftMap, GiftCount, 10081 );
	GiftCount = GiftCount + 1;
end

function BearMove( Terminal, Dir )
	MoveDirection = Dir;
	TerminalTargetPositionIndex = Terminal;
	NextTargetPositionIndex = CurrentPositionIndex + Dir;

	local Position = RaidInstance:ContainerMapFind( GridMapPosition, NextTargetPositionIndex );
	CurrentPositionIndex = NextTargetPositionIndex;

	if Position > 0
	then
		local x, y, z, o = RaidInstance:GetPosition( Position );
		RaidInstance:CreatureMoveTo( Bear, x, y, z, o, 0 );
	end
	BearMoving = 1;
end

function TryDanceEvent( void )
	DanceCount = 0;
	DanceRightCount = 0;
	local Count = RaidInstance:RandomShufflePlayers();
	if Count == 0
	then
		return;
	end

	RaidInstance:DispelAura( Bear, 10 );
	RaidInstance:TeleportCreature( Bear, DestX, DestY, DestZ, DestO );

	RaidInstance:ModifyObjectScale( Bear, 8.0 );

	for i = 1, Count, 1
	do
		local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
		RaidInstance:RemoveBuffFromPlayer( PlayerID, 66 );

		local x = DestX + 16.0;
		x = x + 4.0 * ( (i - 1) / 5 ) ;

		local y = DestY + 4.0 * ( ( (i - 1) % 5 ) - 2 );
		local z = DestZ;

		local PlayerPosition = RaidInstance:CreatePosition( x, y, z, 0.0 );
		local BearPosition = RaidInstance:CreatePosition( DestX, DestY, DestZ, DestO );
		local Orientation = RaidInstance:CalculateAngle( PlayerPosition, BearPosition );

		RaidInstance:DismissMountAndShift( PlayerID );
		RaidInstance:ForceRootPlayer( PlayerID );

		RaidInstance:TeleportPlayer( PlayerID, x, y, z, Orientation );

		RaidInstance:RemoveBuffFromPlayer( PlayerID, 68 );
		RaidInstance:RemoveBuffFromPlayer( PlayerID, 69 );
		RaidInstance:RemoveBuffFromPlayer( PlayerID, 70 );
		RaidInstance:RemoveBuffFromPlayer( PlayerID, 71 );
		RaidInstance:RemoveBuffFromPlayer( PlayerID, 72 );
		RaidInstance:DispelAura( PlayerID, 20 );

		RaidInstance:AddBuffToPlayer( PlayerID, 76 );

		RaidInstance:AddPlayerSpellCastLimit( PlayerID, 80 );
		RaidInstance:AddPlayerSpellCastLimit( PlayerID, 81 );
		RaidInstance:AddPlayerSpellCastLimit( PlayerID, 82 );
		RaidInstance:AddPlayerSpellCastLimit( PlayerID, 83 );
		RaidInstance:AddPlayerSpellCastLimit( PlayerID, 84 );
	end

	Dancing = 1;
	RaidInstance:PlaySound( 0, "Music/Dance.ogg", 1, 1, 0 );
	RaidInstance:PlaySound( Bear, "HappyBear/Ready2Dance.ogg" );
	Ready2DanceEvent = RaidInstance:CreateEvent( 10000, 1 );
	CountDownIndex = 0;
	CountDownEvent = RaidInstance:CreateEvent( 5000, 1 );
end

function OnPlayerResurrect( PlayerID )
	if EventTimeAOEPoison == 0
	then
		RaidInstance:RemoveBuffFromPlayer( PlayerID, 66 );
	end
end

function OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )
	if NPC_ID == Dice and RollAndMoveGossipIndex == GossipIndex and CurrentPositionIndex < MaxIndex
	then
		if BearMoving == 0 and EventRoll == 0
		then
			Roll = LuaRandom( 1, 6 );
			--Roll = 55;
			VoiceAfterRollEvent = RaidInstance:CreateEvent( 3000, 1 );
			EventRoll = RaidInstance:CreateEvent( 4200, 1 );
			RaidInstance:CreatureForcePlayAnimation( Dice, Roll );
			RaidInstance:PlaySound( Dice, "HappyBear/DiceRoll.wav" );
			DiceDropOnTheGroundEvent = RaidInstance:CreateEvent( 2400, 1 );
		else
			--RaidInstance:SendSystemNotify( "The bear is busy, please wait for a while." );
			RaidInstance:PlaySound( Dice, "HappyBear/DiceWait.wav" );
		end
	end

	if NPC_ID == Bear and GossipIndex == GoodJobGossipIndex
	then
		RaidInstance:EnableNPCGossip( Bear, GossipIndex, 0 );
		RaidInstance:PlaySound( Bear, "HappyBear/GoodJobEveryBody.wav" );
		RaidInstance:CreatureMoveTo( Bear, 1696.0, 1179.4, 40.0, 0.9, 1 );
		BearMovingToCopyPosition = 1;

		if GoldBonus > 0
		then
			RaidInstance:ContainerMapInsert( GiftMap, GiftCount, 10084 );
			GiftCount = GiftCount + 1;
		end
	end

	if NPC_ID == Bear and GossipIndex == GiftGossipIndex
	then
		if GiftAlreadyGive == 0
		then
			RaidInstance:ModifyObjectScale( Bear, 4.5 );
			GiftAlreadyGive = 1;
			RaidInstance:CreatureMoveTo( Bear, 1739.2, 1199.1, 40.0, 4.0, 1 );
		else
			local r = LuaRandom( 1, 2 );
			if r == 1
			then
				RaidInstance:PlaySound( Bear, "HappyBear/DontMessWithMeMan.wav" );
			else
				RaidInstance:PlaySound( Bear, "HappyBear/WhatRULookingAt.wav" );
			end
		end
	end

	if NPC_ID == Bear and GossipIndex == DanceTryGossipIndex
	then
		RaidInstance:EnableNPCGossip( Bear, GossipIndex, 0 );
		TryDanceEvent( 0 );
	end
end

local BearGossipAlreadyInsert = 0;
local BearOriginalFaction = 0;

function OnUpdate( TimeElapsed )
	if Bear > 0 and BearGossipAlreadyInsert == 0
	then
		local hp, mp = RaidInstance:GetCreatureBasicInfo( Bear );
		if hp <= 0.15
		then
			RaidInstance:ModifyObjectFaction( Bear, BearOriginalFaction );
			RaidInstance:CreatureForceLeaveCombat( Bear );
			GoodJobGossipIndex = RaidInstance:InsertNPCGossip( Bear, BuildLanguageString( 99 ) );

			BearGossipAlreadyInsert = 1;
			RaidInstance:PlaySound( Bear, "HappyBear/Congratulations.wav" );
			if EventTimeAOEPoison > 0
			then
				RaidInstance:KillEvent( EventTimeAOEPoison );

				local Count = RaidInstance:RandomShufflePlayers();
				for i = 1, Count, 1
				do
					PlayerID = RaidInstance:GetNextRandomShufflePlayer();
					RaidInstance:RemoveBuffFromPlayer( PlayerID, 66 );
				end

				EventTimeAOEPoison = 0;
			end
		end
	end
end

function OnUnitEnterCombat( UnitID )
	if UnitID == Bear
	then
		RaidInstance:ModifyCreatureHPPct( Bear, 1.0 );
		RaidInstance:PlaySound( Bear, "HappyBear/BearAggro.wav" );

		RaidInstance:CreatureCastSpell( Bear, 7, 1, Bear, 1 );
	end
end

function OnUnitDie( VictimID, AttackerID )
	if AttackerID == Bear
	then
		RaidInstance:PlaySound( Bear, "HappyBear/Loser.wav" );
	end
end

function OnUnitLeaveCombat( UnitID )
	if UnitID == Bear
	then
		RaidInstance:RemoveBuffFromCreature( Bear, 7 );
	end
end

function OnPlayerLeaveInstance( PlayerID )
	--ID 68 69 70 71 72 73 74
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 66 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 68 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 69 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 70 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 71 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 72 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 73 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 74 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 76 );

	RaidInstance:ForceUnrootPlayer( PlayerID );
	RaidInstance:ClearPlayerSpellCastLimit( PlayerID );
end

local RaidWipeEvent = 0;

function OnRaidWipe()
	if EventTimeAOEPoison > 0
	then
		RaidInstance:KillEvent( EventTimeAOEPoison );
		EventTimeAOEPoison = 0;
	end

	RaidInstance:SendSystemNotify( BuildLanguageString( 100 ) );
	RaidWipeEvent = RaidInstance:CreateEvent( 5000, 1 );
end

function OnEvent( EventID, Attach, Attach2, Attach3, Attach4, Attach5 )
	if EventID == CountDownEvent
	then
		if CountDownIndex == 0
		then
			CountDownEvent = RaidInstance:CreateEvent( 1000, 4 );
			CountDownIndex = 4;
			RaidInstance:SendSystemNotify( "5" );
			RaidInstance:PlaySound( Bear, "HappyBear/Five.wav" );
		elseif CountDownIndex == 4
		then
			CountDownIndex = 3;
			RaidInstance:SendSystemNotify( "4" );
			RaidInstance:PlaySound( Bear, "HappyBear/Four.wav" );
		elseif CountDownIndex == 3
		then
			CountDownIndex = 2;
			RaidInstance:SendSystemNotify( "3" );
			RaidInstance:PlaySound( Bear, "HappyBear/Three.wav" );
		elseif CountDownIndex == 2
		then
			CountDownIndex = 1;
			RaidInstance:SendSystemNotify( "2" );
			RaidInstance:PlaySound( Bear, "HappyBear/Two.wav" );
		elseif CountDownIndex == 1
		then
			RaidInstance:SendSystemNotify( "1" );
			RaidInstance:PlaySound( Bear, "HappyBear/One.wav" );
			CountDownIndex = 0;
			RaidInstance:KillEvent( CountDownEvent );
		end
	end

	if EventID == WelcomeEvent
	then
		RaidInstance:CreatureSay( Bear, BuildLanguageString( 101 ), 0 );
	end

	if EventID == Ready2DanceEvent
	then
		local Count = RaidInstance:RandomShufflePlayers();
		BearDanceEvent = RaidInstance:CreateEvent( 3000, NeedDanceCountPerPlayer );
		DancePlayerCount = Count;
		NeedDanceCount = Count * 42;

		RaidInstance:PlaySound( Bear, "HappyBear/DanceStart.ogg" );
	end

	if EventID == DanceFailEvent
	then
		if DanceRightCount < NeedDanceCount
		then
			local Count = RaidInstance:RandomShufflePlayers();
			for i = 1, Count, 1
			do
				local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
				RaidInstance:ForceUnrootPlayer( PlayerID );
				RaidInstance:ClearPlayerSpellCastLimit( PlayerID );
			end
			RaidInstance:PlaySound( Bear, "HappyBear/DanceFail.ogg" );
			RaidInstance:CreatureSay( Bear, BuildLanguageString( 102 ), 0 );
			RaidInstance:EnableNPCGossip( Bear, DanceTryGossipIndex, 1 );
		end
	end

	if EventID == RaidWipeEvent
	then
		RaidInstance:ForceEjectAllPlayers();
		return;
	end

	if EventID == DiceDropOnTheGroundEvent
	then
		RaidInstance:PlaySound( Dice, "HappyBear/DropOnGround.wav" );
	end

	if EventID == VoiceAfterRollEvent
	then
		if Roll == 1
		then
			RaidInstance:PlaySound( Dice, "HappyBear/One.wav" );
		elseif Roll == 2
		then
			RaidInstance:PlaySound( Dice, "HappyBear/Two.wav" );
		elseif Roll == 3
		then
			RaidInstance:PlaySound( Dice, "HappyBear/Three.wav" );
		elseif Roll == 4
		then
			RaidInstance:PlaySound( Dice, "HappyBear/Four.wav" );
		elseif Roll == 5
		then
			RaidInstance:PlaySound( Dice, "HappyBear/Five.wav" );
		elseif Roll == 6
		then
			RaidInstance:PlaySound( Dice, "HappyBear/Six.wav" );
		end
	end

	if EventID == EventRoll
	then
		if Roll + CurrentPositionIndex > MaxIndex
		then
			Roll = MaxIndex - CurrentPositionIndex;
		end

		BearMove( Roll + CurrentPositionIndex, 1 );

		if EventTimeAOEPoison == 0
		then
			RaidInstance:CreatureCastSpell( Robot, 66, 3, Robot, 1 );
			RaidInstance:PlaySound( Bear, "HappyBear/TimePoison.wav" );
			EventTimeAOEPoison = RaidInstance:CreateEvent( 60 * 1000, 10000 );
			RaidInstance:ForceGameStart();
		end
		EventRoll = 0;
	end

	if EventID == MoveEvent
	then
		NextTargetPositionIndex = NextTargetPositionIndex + MoveDirection;

		local Position = RaidInstance:ContainerMapFind( GridMapPosition, NextTargetPositionIndex );

		if Position > 0
		then
			local x, y, z, o = RaidInstance:GetPosition( Position );
			RaidInstance:CreatureMoveTo( Bear, x, y, z, o, 0 );
		end
	end

	if EventID == GiftSpawnEvent
	then
		local key, value = RaidInstance:ContainerMapNext( GiftMap );
		local key2, value2 = RaidInstance:ContainerMapNext( ChestPositionMap );
		if value2 > 0
		then
			local x, y, z, o = RaidInstance:GetPosition( value2 );
			local chest = RaidInstance:SpawnGameObject( value, x, y, z, o, 0, 2.0, 10000, 1, 1 );
			if value == 10084 and GoldBonus > 0
			then
				RaidInstance:SetChestLootGold( chest, GoldBonus );
			end

			local r = LuaRandom( 1, 2 );
			if r == 1
			then
				RaidInstance:PlaySound( Bear, "HappyBear/Gift1.wav" );
			else
				RaidInstance:PlaySound( Bear, "HappyBear/Gift2.wav" );
			end
			RaidInstance:CreatureForcePlayAnimation( Bear, 9 );
		end
	end

	if EventID == EventAcceptNextRoll
	then
		BearMoving = 0;
	end

	if EventID == EventTimeAOEPoison
	then
		RaidInstance:PlaySound( Bear, "HappyBear/TimePoison.wav" );
		RaidInstance:CreatureCastSpell( Robot, 66, 3, Robot, 1 );
	end

	if EventID == BearMoveEvent
	then
		BearMove( BearMoveTerminal, BearMoveDirection );
	end

	if EventID == BearDanceEvent
	then
		local SpellID, AnimationID = RaidInstance:ContainerMapRandom( DanceAnimationMap );
		CurrentBearAnimationSpell = SpellID;
		RaidInstance:CreatureForcePlayAnimation( Bear, AnimationID );
		DanceCount = DanceCount + 1;

		if DanceCount >= NeedDanceCountPerPlayer and Dancing == 1
		then
			Dancing = 0;
			RaidInstance:PlaySound( 0, "Music/kaixin.ogg", 1, 1, 0 );
			DanceFailEvent = RaidInstance:CreateEvent( 3000, 1 );
			CurrentBearAnimationSpell = 0;
		end
	end
end

function OnBearStopAndTrigger( Index )
	if Index >= MaxIndex
	then
		RaidInstance:SwitchCreatureAIMode( Bear );
		BearOriginalFaction = RaidInstance:ModifyObjectFaction( Bear, 10 );
		local Count = RaidInstance:RandomShufflePlayers();
		for i = 1, Count, 1
		do
			local Target = RaidInstance:GetNextRandomShufflePlayer();
			RaidInstance:ModifyThreat( Bear, Target, 1 );
		end
		return;
	end

	if Index >= 1
	then
		local ObjectID = RaidInstance:ContainerMapFind( GridMapObject, Index );
		if ObjectID > 0
		then
			RaidInstance:CreatureForcePlayAnimation( ObjectID, 1 );
		end

		local spell = RaidInstance:ContainerMapFind( GridMapSpell, Index );
		local castType = RaidInstance:ContainerMapFind( GridMapCastType, Index );
		local target = RaidInstance:ContainerMapFind( GridMapTarget, Index );
		local bad = RaidInstance:ContainerMapFind( GridMapBad, Index );
		local caster = RaidInstance:ContainerMapFind( GridMapCaster, Index );
		local animation = RaidInstance:ContainerMapFind( GridMapAnimation, Index );
		local animationTime = RaidInstance:ContainerMapFind( GridMapAnimationTime, Index );
		local attach = RaidInstance:ContainerMapFind( GridMapAttach, Index );

		if animation >= 0 and animationTime >= 0
		then
			EventAcceptNextRoll = RaidInstance:CreateEvent( animationTime, 1 );
			RaidInstance:CreatureForcePlayAnimation( caster, animation );
		else
			EventAcceptNextRoll = RaidInstance:CreateEvent( 3000, 1 );
		end

		local giftEntry = attach;
		local mobEntry = giftEntry;
		local dispelCount = giftEntry;

		--local debugInfo = "spell:"..RaidInstance:ConvertInteger2String( spell ).."<br>castType:"..RaidInstance:ConvertInteger2String( castType ).."<br>target:"..RaidInstance:ConvertInteger2String( target ).."<br>caster:"..RaidInstance:ConvertInteger2String( caster ).."<br>animation:"..RaidInstance:ConvertInteger2String( animation ).."<br>animationTime:"..RaidInstance:ConvertInteger2String( animationTime ).."<br>attach:"..RaidInstance:ConvertInteger2String( attach ).."<br>bad:"..RaidInstance:ConvertInteger2String( bad );
		--RaidInstance:SendSystemNotify( debugInfo );

		if spell == SpellGoHome
		then
			--RaidInstance:SendSystemNotify( "Bear:GoHome" );
			RaidInstance:PlaySound( Bear, "HappyBear/GoHome.wav" );
			BearMoveEvent = RaidInstance:CreateEvent( animationTime, 1 );
			BearMoveTerminal = 1;
			BearMoveDirection = -1;
			return;
		end

		if spell == SpellForward3
		then
			local Terminal = CurrentPositionIndex + 3;
			if Terminal > MaxIndex
			then
				Terminal = MaxIndex;
			end
			--RaidInstance:SendSystemNotify( "Bear:Forward3" );
			RaidInstance:PlaySound( Bear, "HappyBear/Forward.wav" );
			BearMoveEvent = RaidInstance:CreateEvent( animationTime, 1 );
			BearMoveTerminal = Terminal;
			BearMoveDirection = 1;
			return;
		end

		if spell == SpellBack5
		then
			local Terminal = CurrentPositionIndex - 5;
			if Terminal < 1
			then
				Terminal = 1;
			end
			--RaidInstance:SendSystemNotify( "Bear:Back5" );
			RaidInstance:PlaySound( Bear, "HappyBear/Backward.wav" );
			BearMoveEvent = RaidInstance:CreateEvent( animationTime, 1 );
			BearMoveTerminal = Terminal;
			BearMoveDirection = -1;
			return;
		end

		if spell == SpellSummonMobs
		then
			--RaidInstance:SendSystemNotify( "Summon mobs!" );
			if mobEntry == 3010
			then
				RaidInstance:PlaySound( Bear, "HappyBear/Caution.wav" );
			else
				RaidInstance:PlaySound( Bear, "HappyBear/Fire.wav" );
			end
			SummonMobs( mobEntry, Index );
			return;
		end

		if spell == SpellChestReward
		then
			--RaidInstance:SendSystemNotify( "Get a gift!" );
			local r = LuaRandom( 1, 2 );
			if r == 1
			then
				RaidInstance:PlaySound( Bear, "HappyBear/Gift1.wav" );
			else
				RaidInstance:PlaySound( Bear, "HappyBear/Gift2.wav" );
			end

			if giftEntry == 10084
			then
				GoldBonus = GoldBonus + 10 * 10000;
			else
				RaidInstance:ContainerMapInsert( GiftMap, GiftCount, giftEntry );
				GiftCount = GiftCount + 1;
			end

			return;
		end

		if spell == SpellDispelAura
		then
			--RaidInstance:SendSystemNotify( "Dispel an aura!" );
			RaidInstance:PlaySound( Bear, "HappyBear/YaHoo.wav" );
			RaidInstance:DispelAura( target, dispelCount );
			return;
		end

		if target == TargetRandomPlayer
		then
			RaidInstance:RandomShufflePlayers();
			target = RaidInstance:GetNextRandomShufflePlayer();
		end

		if caster == CasterByIndex
		then
			caster = RaidInstance:ContainerMapFind( GridMapObject, Index );
		end

		if bad == 1
		then
			FactionRestore = RaidInstance:ModifyObjectFaction( Bear, 10 );
			if FactionRestore > 0
			then
				FactionRestoreSpellID = spell;
			end
		end
		local r = LuaRandom( 1, 4 );
		if r == 1
		then
			RaidInstance:PlaySound( Bear, "HappyBear/Spell1.wav" );
		elseif r == 2
		then
			RaidInstance:PlaySound( Bear, "HappyBear/Spell2.wav" );
		elseif r == 3
		then
			RaidInstance:PlaySound( Bear, "HappyBear/Spell3.wav" );
		elseif r == 4
		then
			RaidInstance:PlaySound( Bear, "HappyBear/Spell4.wav" );
		end

		RaidInstance:CreatureCastSpell( Bear, spell, castType, target, 1 );
	end
end

function OnUnitCastSpell( SpellID, UnitID )
	if SpellID == FactionRestoreSpellID
	then
		RaidInstance:ModifyObjectFaction( UnitID, FactionRestore );

		FactionRestoreSpellID = 0;
		FactionRestore = 0;
	end

	if CurrentBearAnimationSpell > 0
	then
		if SpellID == CurrentBearAnimationSpell
		then
			DanceRightCount = DanceRightCount + 1;
			RaidInstance:CreatureSay( Bear, RaidInstance:ConvertInteger2String( DanceRightCount ).."/"..RaidInstance:ConvertInteger2String( NeedDanceCount ), 0 );
			if DanceRightCount >= NeedDanceCount
			then
				Dancing = 0;
				CurrentBearAnimationSpell = 0;
				DanceRightCount = 9999;
				RaidInstance:KillEvent( BearDanceEvent );
				local Count = RaidInstance:RandomShufflePlayers();
				for i = 1, Count, 1
				do
					local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
					RaidInstance:ForceUnrootPlayer( PlayerID );
					RaidInstance:ClearPlayerSpellCastLimit( PlayerID );
				end

				RaidInstance:CreatureSay( Bear, BuildLanguageString( 103 ), 0 );
				RaidInstance:PlaySound( 0, "Music/kaixin.ogg", 1, 1, 0 );
				RaidInstance:EnableNPCGossip( Bear, GiftGossipIndex, 1 );
				RaidInstance:PlaySound( Bear, "HappyBear/DanceWin.ogg" );

				Count = RaidInstance:ContainerMapBegin( GridMapObject );
				for i = 1, Count, 1
				do
					local Key, Value = RaidInstance:ContainerMapNext( GridMapObject );
					RaidInstance:DespawnCreature( Value );
				end
			end
		else
			local r = LuaRandom( 1, 2 );
			if r == 1
			then
				RaidInstance:PlaySound( UnitID, "HappyBear/DanceWrong1.ogg" );
			else
				RaidInstance:PlaySound( UnitID, "HappyBear/DanceWrong2.ogg" );
			end
		end
	end
end

function OnCreatureMoveArrival( CreatureID )
	if CreatureID == Bear
	then
		if GiftAlreadyGive == 1
		then
			RaidInstance:CreatureMoveTo( Bear, 1789.8, 1277.5, 40.0, 3.2, 1 );
			GiftAlreadyGive = 2;
		elseif GiftAlreadyGive == 2
		then
			local Count = RaidInstance:ContainerMapBegin( GiftMap );
			if Count > 0
			then
				ChestPositionMap = RaidInstance:ContainerMapCreate();
				RaidInstance:GeneratePuzzlePositionIntoContainerMap( ChestPositionMap, Count, Interval, CenterX, CenterY, CenterZ, CenterO, 0 );
				RaidInstance:ContainerMapBegin( ChestPositionMap );
				GiftSpawnEvent = RaidInstance:CreateEvent( 4000, Count );
			end
			GiftAlreadyGive = 3;
		elseif BearMovingToCopyPosition == 0
		then
			CurrentPositionIndex = NextTargetPositionIndex;
			if NextTargetPositionIndex == TerminalTargetPositionIndex
			then
				OnBearStopAndTrigger( TerminalTargetPositionIndex );
				return;
			end

			MoveEvent = RaidInstance:CreateEvent( 500, 1 );
		elseif BearMovingToCopyPosition == 1
		then
			RaidInstance:CreatureMoveTo( Bear, DestX, DestY, DestZ, DestO, 1 );
			BearMovingToCopyPosition = 2;
		elseif BearMovingToCopyPosition == 2
		then
			TryDanceEvent( 0 );
		end
	end
end

function OnPlayerEnterInstance( PlayerID )
	if Dancing == 0
	then
		RaidInstance:PlaySound( 0, "Music/kaixin.ogg", 1, 1, PlayerID );
	else
		RaidInstance:PlaySound( 0, "Music/Dance.ogg", 1, 1, PlayerID );
	end
end
