--Author Jane Chen
--3/27/2011
--

local RaidInstance;
local Bourne = 0;
local SpawnChestMapA = 0;
local SpawnChestMapB = 0;
local SpawnChestMapC = 0;
local BearA = 0;
local BearB = 0;
local BearC = 0;
local BearABusy = 0;
local BearBBusy = 0;
local BearCBusy = 0;
local BearLastKeyA = 0;
local BearLastKeyB = 0;
local BearLastKeyC = 0;
local Vendor = 0;
local ExchangeGossip1 = 0;
local ExchangeGossip2 = 0;
local ExchangeGossip3 = 0;
local CurrentChestCount = 0;
local DiceA1 = 0;
local DiceA2 = 0;
local DiceA3 = 0;
local TriggerA = 0;
local DiceABusy = 0;
local board_entry = 0;
local board_dance = 0;
local board_dice1 = 0;
local board_dice2 = 0;
local board_dice3 = 0;
local board_fish = 0;
local board_pk1 = 0;
local board_pk2 = 0;
local board_snow = 0;
local board_gamble = 0;

local DiceB1 = 0;
local DiceB2 = 0;
local DiceB3 = 0;
local TriggerB = 0;
local DiceBBusy = 0;

local DiceC1 = 0;
local DiceC2 = 0;
local DiceC3 = 0;
local TriggerC = 0;
local DiceCBusy = 0;

local DiceD1 = 0;
local DiceD2 = 0;
local DiceD3 = 0;
local TriggerD = 0;
local DiceDBusy = 0;

local ShapeShift = 0;
local ShapeMap = 0;
local IncreaseScaleGossip = 0;

local SnowmanMap = 0;

function FindFreeBear()
	if BearABusy == 0
	then
		return BearA;
	elseif BearBBusy == 0
	then
		return BearB;
	elseif BearCBusy == 0
	then
		return BearC;
	end

	return 0;
end

function GetLastKey( Bear )
	if BearA == Bear
	then
		return BearLastKeyA;
	elseif BearB == Bear
	then
		return BearLastKeyB;
	else
		return BearLastKeyC;
	end
end

function SetLastKey( Bear, Key )
	if BearA == Bear
	then
		BearLastKeyA = Key;
	elseif BearB == Bear
	then
		BearLastKeyB = Key;
	else
		BearLastKeyC = Key;
	end
end

function GetSpawnMap( Bear )
	if BearA == Bear
	then
		return SpawnChestMapA;
	elseif BearB == Bear
	then
		return SpawnChestMapB;
	elseif BearC == Bear
	then
		return SpawnChestMapC;
	end

	return 0;
end

function ModifyBearState( Bear, Busy )
	if BearA == Bear
	then
		BearABusy = Busy;
	elseif BearB == Bear
	then
		BearBBusy = Busy;
	elseif BearC == Bear
	then
		BearCBusy = Busy;
	end
end

function GetBearState( Bear )
	if BearA == Bear
	then
		return BearABusy;
	elseif BearB == Bear
	then
		return BearBBusy;
	elseif BearC == Bear
	then
		return BearCBusy;
	end
	return 0;
end

function ModifyDiceBusy( Dice, Busy )
	if DiceA1 == Dice
	then
		DiceABusy = Busy;
	elseif DiceB1 == Dice
	then
		DiceBBusy = Busy;
	elseif DiceC1 == Dice
	then
		DiceCBusy = Busy;
	elseif DiceD1 == Dice
	then
		DiceDBusy = Busy;
	else
		return 0;
	end

	return Dice;
end

function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );
	Bourne = RaidInstance:SpawnCreature( 3023, 2061.3, 1264.6, 0.0, 4.6, 0 );
	RaidInstance:ModifyObjectFaction( Bourne, 10000 );
	RaidInstance:SetUnitInvisible( Bourne, 1 );
	RaidInstance:SwitchCreatureAIMode( Bourne );

	board_entry = RaidInstance:SpawnCreature( 607000133, 1909.8, 1230.4, 20.4, 3.1, 0 );
	board_dance = RaidInstance:SpawnCreature( 607000134, 2057.1, 1104.7, 0.0, 4.2, 0 );
	board_dice1 = RaidInstance:SpawnCreature( 607000135, 2032.1, 1091.8, -0.1, 3.6, 0 );
	board_dice2 = RaidInstance:SpawnCreature( 607000135, 2007.0, 1318.8, 0.2, 5.6, 0 );
	board_dice3 = RaidInstance:SpawnCreature( 607000135, 1808.6, 1329.8, 0.0, 5.5, 0 );
	board_fish = RaidInstance:SpawnCreature( 607000136, 1811.6, 1248.7, 0.1, 3.7, 0 );
	board_pk1 = RaidInstance:SpawnCreature( 607000137, 1892.0, 1109.6, 20.0, 4.0, 0 );
	board_pk2 = RaidInstance:SpawnCreature( 607000138, 1911.6, 1108.7, 20.0, 2.7, 0 );
	board_snow = RaidInstance:SpawnCreature( 607000139, 2033.3, 1219.9, -0.2, 1.7, 0 );
	board_gamble = RaidInstance:SpawnCreature( 607000140, 1953.0, 1368.3, 20.0, 0.4, 0 );

	RaidInstance:SpawnGameObject( 10082, 1918.7, 1235.5, 20.0, 1.8, 0, 0, 10000, 1, 1 ); -- mailbox
	Vendor = RaidInstance:SpawnCreature( 3070, 1920.8, 1241.4, 20.0, 1.9, 0 );
	RaidInstance:ModifyObjectScale( Vendor, 4.0 );
	ShapeShift = RaidInstance:InsertNPCGossip( Vendor, BuildLanguageString( 157 ) );
	IncreaseScaleGossip = RaidInstance:InsertNPCGossip( Vendor, "花20个开心币增大一次体型,最多2次哦!" );
	ExchangeGossip1 = RaidInstance:InsertNPCGossip( Vendor, BuildLanguageString( 158 ) );
	ExchangeGossip2 = RaidInstance:InsertNPCGossip( Vendor, BuildLanguageString( 159 ) );
	ExchangeGossip3 = RaidInstance:InsertNPCGossip( Vendor, BuildLanguageString( 160 ) );

	DiceA1 = RaidInstance:SpawnCreature( 3084, 2031.1, 1119.0, 0.0, 3.1, 0 );
	RaidInstance:ModifyObjectScale( DiceA1, 2.7 );
	DiceA2 = RaidInstance:SpawnCreature( 3084, 2031.3, 1113.2, 0.0, 3.1, 0 );
	RaidInstance:ModifyObjectScale( DiceA2, 2.7 );
	DiceA3 = RaidInstance:SpawnCreature( 3084, 2031.3, 1107.3, 0.0, 3.1, 0 );
	RaidInstance:ModifyObjectScale( DiceA3, 2.7 );
	TriggerA = RaidInstance:SpawnCreature( 3077, 2022.8, 1113.3, 0.0, 4.7, 0 );
	RaidInstance:InsertNPCGossip( TriggerA, "花20个开心币玩一次" );
	RaidInstance:ModifyObjectScale( TriggerA, 4.0 );

	DiceB1 = RaidInstance:SpawnCreature( 3084, 1820.9, 1318.7, 0.0, 4.7, 0 );
	RaidInstance:ModifyObjectScale( DiceB1, 2.7 );
	DiceB2 = RaidInstance:SpawnCreature( 3084, 1826.4, 1318.4, 0.0, 4.8, 0 );
	RaidInstance:ModifyObjectScale( DiceB2, 2.7 );
	DiceB3 = RaidInstance:SpawnCreature( 3084, 1832.4, 1318.2, 0.0, 4.9, 0 );
	RaidInstance:ModifyObjectScale( DiceB3, 2.7 );
	TriggerB = RaidInstance:SpawnCreature( 3077, 1827.5, 1326.7, 0.0, 0.0, 0 );
	RaidInstance:InsertNPCGossip( TriggerB, "花20个开心币玩一次" );
	RaidInstance:ModifyObjectScale( TriggerB, 4.0 );

	--DiceC1 = RaidInstance:SpawnCreature( 3084, 1804.2, 1334.8, 0.0, 0.0, 0 );
	--RaidInstance:ModifyObjectScale( DiceC1, 2.7 );
	--DiceC2 = RaidInstance:SpawnCreature( 3084, 1807.6, 1334.9, 0.0, 6.3, 0 );
	--RaidInstance:ModifyObjectScale( DiceC2, 2.7 );
	--DiceC3 = RaidInstance:SpawnCreature( 3084, 1811.3, 1335.0, 0.0, 6.3, 0 );
	--RaidInstance:ModifyObjectScale( DiceC3, 2.7 );
	--TriggerC = RaidInstance:SpawnCreature( 3077, 1802.1, 1343.8, 0.0, 0.0, 0 );
	--RaidInstance:InsertNPCGossip( TriggerC, "花5个开心币玩一次" );

	DiceD1 = RaidInstance:SpawnCreature( 3084, 2030.5, 1306.3, 0.0, 4.4, 0 );
	RaidInstance:ModifyObjectScale( DiceD1, 2.7 );
	DiceD2 = RaidInstance:SpawnCreature( 3084, 2024.7, 1304.6, 0.0, 4.4, 0 );
	RaidInstance:ModifyObjectScale( DiceD2, 2.7 );
	DiceD3 = RaidInstance:SpawnCreature( 3084, 2019.6, 1303.1, 0.1, 4.4, 0 );
	RaidInstance:ModifyObjectScale( DiceD3, 2.7 );
	TriggerD = RaidInstance:SpawnCreature( 3077, 2023.6, 1313.0, 0.0, 5.9, 0 );
	RaidInstance:InsertNPCGossip( TriggerD, "花20个开心币玩一次" );
	RaidInstance:ModifyObjectScale( TriggerD, 4.0 );

	BearA = RaidInstance:SpawnCreature( 3078, 2076.8, 1251.6, 0.0, 1.6, 0 );
	BearB = RaidInstance:SpawnCreature( 3078, 2077.0, 1264.6, 0.0, 1.6, 0 );
	BearC = RaidInstance:SpawnCreature( 3078, 2077.2, 1278.7, 0.0, 1.6, 0 );
	RaidInstance:SwitchCreatureAIMode( BearA );
	RaidInstance:SwitchCreatureAIMode( BearB );
	RaidInstance:SwitchCreatureAIMode( BearC );

	SpawnChestMapA = RaidInstance:ContainerMapCreate();
	SpawnChestMapB = RaidInstance:ContainerMapCreate();
	SpawnChestMapC = RaidInstance:ContainerMapCreate();
	ShapeMap = RaidInstance:ContainerMapCreate();
	SnowmanMap = RaidInstance:ContainerMapCreate();

	RaidInstance:ContainerMapInsert( ShapeMap, 98, 98 );
	RaidInstance:ContainerMapInsert( ShapeMap, 99, 99 );
	RaidInstance:ContainerMapInsert( ShapeMap, 115, 115 );
	RaidInstance:ContainerMapInsert( ShapeMap, 116, 116 );
	RaidInstance:ContainerMapInsert( ShapeMap, 117, 117 );
	RaidInstance:ContainerMapInsert( ShapeMap, 119, 119 );

	local pos = 0;
	for i = 1427,  1434, 1
	do
		pos = RaidInstance:CreatePositionByStorage( i );
		RaidInstance:ContainerMapInsert( SpawnChestMapA, i, pos );
	end

	for i = 1435,  1446, 1
	do
		pos = RaidInstance:CreatePositionByStorage( i );
		RaidInstance:ContainerMapInsert( SpawnChestMapB, i, pos );
	end

	for i = 1447,  1456, 1
	do
		pos = RaidInstance:CreatePositionByStorage( i );
		RaidInstance:ContainerMapInsert( SpawnChestMapC, i, pos );
	end
end

function ShapeShiftPlayer( PlayerID )
	local OldShape = RaidInstance:GetPlayerShapeShiftSpellEntry( PlayerID );
	local r = 0;
	if OldShape > 0
	then
		RaidInstance:DismissMountAndShift();
		RaidInstance:ContainerMapErase( ShapeMap, OldShape );
		r = RaidInstance:ContainerMapRandom( ShapeMap );
		RaidInstance:ContainerMapInsert( ShapeMap, OldShape, OldShape );
	else
		r = RaidInstance:ContainerMapRandom( ShapeMap );
	end
	RaidInstance:AddBuffToPlayer( PlayerID, r );
end

function OnPlayerEnterInstance( PlayerID )
	RaidInstance:RemoveBuffFromPlayer( 10801 );
	RaidInstance:RemoveBuffFromPlayer( 10802 );
	RaidInstance:RemoveBuffFromPlayer( 10803 );
	ShapeShiftPlayer( PlayerID );
	RaidInstance:ModifyPlayerPVPFlag( PlayerID, 1 );

	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 86 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 120 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 89 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 303 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 304 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 305 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 306 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 339 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 340 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 341 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 342 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 343 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 344 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 351 );

	RaidInstance:EnableMeleeAttack( PlayerID, 0 );
	RaidInstance:ModifyPlayerHPPct( PlayerID, 1.0 );
end

function OnPlayerLeaveInstance( PlayerID )
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 119 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 98 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 99 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 115 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 116 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 117 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 307 );
	RaidInstance:ModifyPlayerPVPFlag( PlayerID, 0 );

	RaidInstance:ClearPlayerSpellCastLimit( PlayerID );
	RaidInstance:EnableMeleeAttack( PlayerID, 1 );
end

function OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )
	local ret = 0;
	if NPC_ID == Vendor
	then
		if ShapeShift == GossipIndex
		then
			ret = RaidInstance:ConsumeCurrency( PlayerID, 0, 56, 5 );
			if ret == 1
			then
				ShapeShiftPlayer( PlayerID );
				ret = 0;
			else
				ret = 2;
			end
		elseif GossipIndex == IncreaseScaleGossip
		then
			local StackCount = RaidInstance:GetAuraStackCount( PlayerID, 307 );
			if StackCount < 2
			then
				ret = RaidInstance:ConsumeCurrency( PlayerID, 0, 56, 20 );
				if ret == 1
				then
					RaidInstance:AddBuffToPlayer( PlayerID, 307 );
					ret = 0;
				else
					ret = 2;
				end
			else
				RaidInstance:CreatureWhisper( NPC_ID, PlayerID, "不能再变大了!" );
			end
		elseif GossipIndex == ExchangeGossip1
		then
			ret = RaidInstance:ExchangeItem2Yuanbao( PlayerID, 56, 100, 80 );
		elseif GossipIndex == ExchangeGossip2
		then
			ret = RaidInstance:ExchangeItem2Yuanbao( PlayerID, 56, 1000, 800 );
		elseif GossipIndex == ExchangeGossip3
		then
			ret = RaidInstance:ExchangeItem2Yuanbao( PlayerID, 56, 5000, 4000 );
		end

		if ret == 1
		then
			RaidInstance:CreatureWhisper( NPC_ID, PlayerID, "兑换失败,请稍后再试试!" );
		elseif ret == 2
		then
			RaidInstance:CreatureWhisper( NPC_ID, PlayerID, "开心币不够哦!" );
		end
	elseif NPC_ID == TriggerA
	then
		if DiceABusy == 0
		then
			ret = RaidInstance:ConsumeCurrency( PlayerID, 0, 56, 20 );
			if ret == 0
			then
				RaidInstance:CreatureWhisper( NPC_ID, PlayerID, "开心币不够哦!" );
				return;
			end
			local r1 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceA1, r1 );
			local r2 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceA2, r2 );
			local r3 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceA3, r3 );

			RaidInstance:CreateEvent( 2400, 1, DiceA2, 5 );
			if r1 == r2 and r1 == r3
			then
				RaidInstance:CreateEvent( 3400, 1, PlayerID, 4 );
			end

			RaidInstance:PlaySound( DiceA2, "HappyBear/DiceRoll.wav" );
			DiceABusy = 1;
			RaidInstance:CreateEvent( 5000, 1, DiceA1, 3 );
		else
			RaidInstance:PlaySound( PlayerID, "HappyBear/DiceWait.wav" );
		end
	elseif NPC_ID == TriggerB
	then
		if DiceBBusy == 0
		then
			ret = RaidInstance:ConsumeCurrency( PlayerID, 0, 56, 20 );
			if ret == 0
			then
				RaidInstance:CreatureWhisper( NPC_ID, PlayerID, "开心币不够哦!" );
				return;
			end

			local r1 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceB1, r1 );
			local r2 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceB2, r2 );
			local r3 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceB3, r3 );

			RaidInstance:CreateEvent( 2400, 1, DiceB2, 5 );
			if r1 == r2 and r1 == r3
			then
				RaidInstance:CreateEvent( 3400, 1, PlayerID, 4 );
			end

			RaidInstance:PlaySound( DiceB2, "HappyBear/DiceRoll.wav" );
			DiceBBusy = 1;
			RaidInstance:CreateEvent( 5000, 1, DiceB1, 3 );
		else
			RaidInstance:PlaySound( PlayerID, "HappyBear/DiceWait.wav" );
		end
	elseif NPC_ID == TriggerC
	then
		if DiceCBusy == 0
		then
			ret = RaidInstance:ConsumeCurrency( PlayerID, 0, 56, 20 );
			if ret == 0
			then
				RaidInstance:CreatureWhisper( NPC_ID, PlayerID, "开心币不够哦!" );
				return;
			end

			local r1 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceC1, r1 );
			local r2 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceC2, r2 );
			local r3 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceC3, r3 );

			RaidInstance:CreateEvent( 2400, 1, DiceC2, 5 );
			if r1 == r2 and r1 == r3
			then
				RaidInstance:CreateEvent( 3400, 1, PlayerID, 4 );
			end

			RaidInstance:PlaySound( DiceC2, "HappyBear/DiceRoll.wav" );
			DiceCBusy = 1;
			RaidInstance:CreateEvent( 5000, 1, DiceC1, 3 );
		else
			RaidInstance:PlaySound( PlayerID, "HappyBear/DiceWait.wav" );
		end
	elseif NPC_ID == TriggerD
	then
		if DiceDBusy == 0
		then
			ret = RaidInstance:ConsumeCurrency( PlayerID, 0, 56, 20 );
			if ret == 0
			then
				RaidInstance:CreatureWhisper( NPC_ID, PlayerID, "开心币不够哦!" );
				return;
			end

			local r1 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceD1, r1 );
			local r2 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceD2, r2 );
			local r3 = LuaRandom( 1, 6 );
			RaidInstance:CreatureForcePlayAnimation( DiceD3, r3 );

			RaidInstance:CreateEvent( 2400, 1, DiceD2, 5 );
			if r1 == r2 and r1 == r3
			then
				RaidInstance:CreateEvent( 3400, 1, PlayerID, 4 );
			end

			RaidInstance:PlaySound( DiceD2, "HappyBear/DiceRoll.wav" );
			DiceDBusy = 1;
			RaidInstance:CreateEvent( 5000, 1, DiceD1, 3 );
		else
			RaidInstance:PlaySound( PlayerID, "HappyBear/DiceWait.wav" );
		end
	end
end

function OnGameObjectTrigger( GameObjectID, PlayerID )
	RaidInstance:CreatureCastSpell( Bourne, 95, 1, PlayerID, 1 );
	RaidInstance:CreateEvent( 8000, 1, GameObjectID, 1 );
end

function OnUpdate( TimeElapsed )
	if TimeElapsed > 5 and CurrentChestCount < 5
	then
		local FreeBear = FindFreeBear();
		if FreeBear > 0
		then
			local Map = GetSpawnMap( FreeBear );
			local LastKey = GetLastKey( FreeBear );
			local key, pos = RaidInstance:ContainerMapRandomExceptKey( Map, LastKey );
			local x, y, z, o = RaidInstance:GetPosition( pos );
			RaidInstance:CreatureMoveTo( FreeBear, x, y, z, o, 1 );
			ModifyBearState( FreeBear, 1 );
			SetLastKey( FreeBear, key );
		end
	end
end

function OnEvent( EventID, Attach, Attach2, Attach3, Attach4, Attach5 )
	if Attach2 == 5
	then
		RaidInstance:PlaySound( Attach, "HappyBear/DropOnGround.wav" );
	elseif Attach2 == 4
	then
		RaidInstance:ItemReward( Attach, 59, 1 );
	elseif Attach2 == 1
	then
		RaidInstance:DespawnGameObject( Attach );
	elseif Attach2 == 2
	then
		ModifyBearState( Attach, 0 );
		local x, y, z, o = RaidInstance:GetObjectPosition( Attach );
		local obj = 0;
		local r = LuaRandom( 1, 100 );
		if r <= 5
		then
			obj = RaidInstance:SpawnGameObject( 10117, x + 2, y + 2, z, o, 0, 0, 0, 1, 1 );
			RaidInstance:ModifyObjectScale( obj, 5.0 );
		else
			obj = RaidInstance:SpawnGameObject( 10116, x + 2, y + 2, z, o, 0, 0, 0, 1, 1 );
			RaidInstance:ModifyObjectScale( obj, 3.0 );
		end
		RaidInstance:ModifyGameObjectDynamicFlags( obj, 57 );
		RaidInstance:ContainerMapInsert( SnowmanMap, obj, obj );
		CurrentChestCount = CurrentChestCount + 1;
	elseif Attach2 == 3
	then
		ModifyDiceBusy( Attach, 0 );
	end
end

function OnGameObjectDespawn( GameObjectID )
	local value = RaidInstance:ContainerMapFind( SnowmanMap, GameObjectID );
	if value > 0
	then
		RaidInstance:ContainerMapErase( SnowmanMap, GameObjectID );
		CurrentChestCount = CurrentChestCount - 1;
	end
end

function OnCreatureMoveArrival( CreatureID )
	if CreatureID == BearA or CreatureID == BearB or CreatureID == BearC
	then
		RaidInstance:CreatureForcePlayAnimation( CreatureID, 9 );
		RaidInstance:CreateEvent( 4000, 1, CreatureID, 2 );
	end
end

function OnPlayerLootItem( PlayerID, Entry, Amount )
	local var = RaidInstance:GetObjectVariable( PlayerID, 1 );
	if var == 0
	then
		if Entry == 56 and Amount >= 100
		then
			RaidInstance:AddBuffToPlayer( PlayerID, 127 );
			RaidInstance:PlaySound( PlayerID, "HappyBear/Congratulations.wav" );
		end

		if Entry == 59 and Amount >= 1
		then
			RaidInstance:AddBuffToPlayer( PlayerID, 127 );
			RaidInstance:PlaySound( PlayerID, "HappyBear/Congratulations.wav" );
		end
	end
end
