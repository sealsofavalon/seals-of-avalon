-- 04/07/2011
-- Author gui

local RaidInstance;

local BearX = 2058.1;
local BearY = 1124.2;
local BearZ = 1.8;
local BearO = 4.7;

local Ready2DanceEvent = 0;
local CountDownEvent = 0;
local CountDownIndex = 0;
local Dancing = 0;
local DanceAnimationMap = 0;
local EnrollPlayerMap = 0;
local EnrollGossipIndex = 0;
local CancelEnrollGossipIndex = 0;
local AcceptEnroll = 0;
local OpenEnrollEvent = 0;
local CloseEnrollEvent = 0;
local BroadcastEvent = 0;
local RestTime = 0;
local CurrentBearAnimationSpell = 0;
local PlayerOldShapeMap = 0;
local DanceCount = 0;
local RequireDanceCount = 5;

function OpenEnroll()
	local Count = RaidInstance:ContainerMapBegin( EnrollPlayerMap );
	for i = 1, Count, 1
	do
		local PlayerID, RightCount = RaidInstance:ContainerMapNext( EnrollPlayerMap );
		if PlayerID > 0
		then
			RaidInstance:ModifyObjectVariable( PlayerID, 0, 0 );
			if RightCount >= RequireDanceCount
			then
				RaidInstance:ItemReward( PlayerID, 56, 130 );
				RaidInstance:ItemReward( PlayerID, 74, 1 );
				RaidInstance:PlaySound( Bear, "HappyBear/DanceWin.ogg", 1, 0, PlayerID );
			else
				local RewardCount = 0;
				if RightCount == RequireDanceCount - 3
				then
					RewardCount = 50;
				elseif RightCount == RequireDanceCount - 2
				then
					RewardCount = 70;
				elseif RightCount == RequireDanceCount - 1
				then
					RewardCount = 80;
				end

				if RewardCount > 0
				then
					RaidInstance:ItemReward( PlayerID, 56, RewardCount );
				end
				RaidInstance:PlaySound( Bear, "HappyBear/DanceFail.ogg", 1, 0, PlayerID );
			end

			local value = RaidInstance:ContainerMapFind( PlayerOldShapeMap, PlayerID );
			if value > 0
			then
				RaidInstance:AddBuffToPlayer( PlayerID, value );
				RaidInstance:ForceUnrootPlayer( PlayerID );
				RaidInstance:ModifyPlayerPVPFlag( PlayerID, 1 );
			else
				RaidInstance:SendSystemNotify( "Fatal Error! - Dance.lua:69" );
			end
		end
	end

	RaidInstance:ContainerMapClear( EnrollPlayerMap );
	RaidInstance:ContainerMapClear( PlayerOldShapeMap );
	RaidInstance:EnableNPCGossip( Bear, EnrollGossipIndex, 1 );
	RaidInstance:EnableNPCGossip( Bear, CancelEnrollGossipIndex, 1 );
	AcceptEnroll = 1;
	CloseEnrollEvent = RaidInstance:CreateEvent( 5 * 60 * 1000, 1 );
	BroadcastEvent = RaidInstance:CreateEvent( 30 * 1000, 10 );
	RestTime = 5 * 60;
end

function DanceStart()
	local Count = RaidInstance:ContainerMapBegin( EnrollPlayerMap );
	if Count == 0
	then
		RaidInstance:SendSystemNotify( "由于本轮模仿秀活动无人报名参加，即将开始接受新一轮的报名" );
		OpenEnroll();
		return;
	end

	RaidInstance:TeleportCreature( Bear, BearX, BearY, BearZ, BearO );

	for i = 1, Count, 1
	do
		local PlayerID = RaidInstance:ContainerMapNext( EnrollPlayerMap );
		RaidInstance:RemoveBuffFromPlayer( PlayerID, 337 );
		RaidInstance:ContainerMapModifyValue( EnrollPlayerMap, PlayerID, 0 );
		local x = BearX + 20.0;
		x = x + 3.5 * ( (i - 1) / 5 ) ;

		local y = BearY + 3.5 * ( ( (i - 1) % 5 ) - 2 );
		local z = BearZ;

		local PlayerPosition = RaidInstance:CreatePosition( x, y, z, 0.0 );
		local BearPosition = RaidInstance:CreatePosition( BearX, BearY, BearZ, BearO );
		local Orientation = RaidInstance:CalculateAngle( PlayerPosition, BearPosition );

		local OldShape = RaidInstance:GetPlayerShapeShiftSpellEntry( PlayerID );
		RaidInstance:ContainerMapInsert( PlayerOldShapeMap, PlayerID, OldShape );
		RaidInstance:DismissMountAndShift( PlayerID );
		RaidInstance:ForceRootPlayer( PlayerID );

		RaidInstance:TeleportPlayer( PlayerID, x, y, z, Orientation );

		RaidInstance:AddBuffToPlayer( PlayerID, 76 );
		RaidInstance:ModifyPlayerPVPFlag( PlayerID, 2 );
	end

	RaidInstance:PlaySound( Bear, "HappyBear/Ready2Dance.ogg" );
	Dancing = 1;
	DanceCount = 0;
	Ready2DanceEvent = RaidInstance:CreateEvent( 10000, 1 );
	CountDownIndex = 0;
	CountDownEvent = RaidInstance:CreateEvent( 5000, 1 );
end

function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );

	DanceAnimationMap = RaidInstance:ContainerMapCreate();
	RaidInstance:ContainerMapInsert( DanceAnimationMap, 80, 14 );
	RaidInstance:ContainerMapInsert( DanceAnimationMap, 81, 16 );
	RaidInstance:ContainerMapInsert( DanceAnimationMap, 82, 15 );
	RaidInstance:ContainerMapInsert( DanceAnimationMap, 83, 13 );
	RaidInstance:ContainerMapInsert( DanceAnimationMap, 84, 5 );

	EnrollPlayerMap = RaidInstance:ContainerMapCreate();

	PlayerOldShapeMap = RaidInstance:ContainerMapCreate();

	Bear = RaidInstance:SpawnCreature( 3082, BearX, BearY, BearZ, BearO, 0 );
	RaidInstance:ModifyObjectScale( Bear, 8.5 );
	EnrollGossipIndex = RaidInstance:InsertNPCGossip( Bear, "花100个开心币报名参加模仿秀活动!" );
	CancelEnrollGossipIndex = RaidInstance:InsertNPCGossip( Bear, "取消报名模仿秀活动!" );
	OpenEnroll();
end

function OnPlayerEnterInstance( PlayerID )
	-- happy bear dance
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 80 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 81 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 82 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 83 );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 84 );

	RaidInstance:ClearObjectVariable( PlayerID );
end

function OnPlayerLeaveInstance( PlayerID )
	RaidInstance:ContainerMapErase( EnrollPlayerMap, PlayerID );
	RaidInstance:ContainerMapErase( PlayerOldShapeMap, PlayerID );
	RaidInstance:ForceUnrootPlayer( PlayerID );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 76 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 337 );
end

function OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )
	if NPC_ID == Bear
	then
		if GossipIndex == EnrollGossipIndex
		then
			if AcceptEnroll == 0
			then
				RaidInstance:CreatureWhisper( Bear, PlayerID, "波波现在不接受报名预约哦!" );
				return;
			end

			local value = RaidInstance:ContainerMapFind( EnrollPlayerMap, PlayerID );
			if value == 0
			then
				local var = RaidInstance:GetObjectVariable( PlayerID, 0 );
				if var > 0
				then
					RaidInstance:CreatureWhisper( Bear, PlayerID, "你已报名参加其他项目,不能再参加模仿秀活动了!" );
					return;
				end

				local ret = RaidInstance:ConsumeCurrency( PlayerID, 0, 56, 100 );
				if ret == 1
				then
					RaidInstance:ContainerMapInsert( EnrollPlayerMap, PlayerID, PlayerID );
					RaidInstance:CreatureWhisper( Bear, PlayerID, "报名参加模仿秀活动成功!" );
					RaidInstance:AddBuffToPlayer( PlayerID, 337 );
					RaidInstance:ModifyObjectVariable( PlayerID, 0, 1 );
				else
					RaidInstance:CreatureWhisper( Bear, PlayerID, "开心币不够哦!" );
				end
			else
				RaidInstance:CreatureWhisper( Bear, PlayerID, "你已经报过名咯!" );
			end
		elseif GossipIndex == CancelEnrollGossipIndex
		then
			if AcceptEnroll == 0
			then
				RaidInstance:CreatureWhisper( Bear, PlayerID, "波波现在接受取消报名哦!" );
				return;
			end

			local value = RaidInstance:ContainerMapFind( EnrollPlayerMap, PlayerID );

			if value > 0
			then
				RaidInstance:RemoveBuffFromPlayer( PlayerID, 337 );
				RaidInstance:ModifyObjectVariable( PlayerID, 1, 1 );
				RaidInstance:ItemReward( PlayerID, 56, 100 );
				RaidInstance:ModifyObjectVariable( PlayerID, 1, 0 );
				RaidInstance:ContainerMapErase( EnrollPlayerMap, PlayerID );
				RaidInstance:CreatureWhisper( Bear, PlayerID, "你已成功取消模仿秀报名!" );
				RaidInstance:ModifyObjectVariable( PlayerID, 0, 0 );
			end
		end
	end
end

function OnEvent( EventID, Attach, Attach2, Attach3, Attach4, Attach5 )
	if EventID == BroadcastEvent
	then
		RestTime = RestTime - 30;
		if RestTime > 0
		then
			RaidInstance:SendSystemNotify( "模仿秀活动"..RaidInstance:ConvertInteger2String(RestTime).."秒后截止报名" );
		else
			RaidInstance:SendSystemNotify( "模仿秀活动即将开始!" );
		end
	end

	if EventID == CloseEnrollEvent
	then
		AcceptEnroll = 0;
		RaidInstance:EnableNPCGossip( Bear, EnrollGossipIndex, 0 );
		RaidInstance:EnableNPCGossip( Bear, CancelEnrollGossipIndex, 0 );
		DanceStart();
	end

	if EventID == CountDownEvent
	then
		if CountDownIndex == 0
		then
			CountDownEvent = RaidInstance:CreateEvent( 1000, 4 );
			CountDownIndex = 4;
			RaidInstance:CreatureSay( Bear, "5", 0 );
			RaidInstance:PlaySound( Bear, "HappyBear/Five.wav" );
		elseif CountDownIndex == 4
		then
			CountDownIndex = 3;
			RaidInstance:CreatureSay( Bear, "4", 0 );
			RaidInstance:PlaySound( Bear, "HappyBear/Four.wav" );
		elseif CountDownIndex == 3
		then
			CountDownIndex = 2;
			RaidInstance:CreatureSay( Bear, "3", 0 );
			RaidInstance:PlaySound( Bear, "HappyBear/Three.wav" );
		elseif CountDownIndex == 2
		then
			CountDownIndex = 1;
			RaidInstance:CreatureSay( Bear, "2", 0 );
			RaidInstance:PlaySound( Bear, "HappyBear/Two.wav" );
		elseif CountDownIndex == 1
		then
			RaidInstance:CreatureSay( Bear, "1", 0 );
			RaidInstance:PlaySound( Bear, "HappyBear/One.wav" );
			CountDownIndex = 0;
			RaidInstance:KillEvent( CountDownEvent );
		end
	end

	if EventID == BearDanceEvent
	then
		DanceCount = DanceCount + 1;

		if DanceCount <= RequireDanceCount and Dancing == 1
		then
			local SpellID, AnimationID = RaidInstance:ContainerMapRandom( DanceAnimationMap );
			CurrentBearAnimationSpell = SpellID;
			RaidInstance:CreatureForcePlayAnimation( Bear, AnimationID );
		end

		-- end
		if DanceCount > RequireDanceCount and Dancing == 1
		then
			Dancing = 0;
			CurrentBearAnimationSpell = 0;
			RaidInstance:KillEvent( EventID );
			OpenEnroll();
		end
	end

	if EventID == Ready2DanceEvent
	then
		local Count = RaidInstance:RandomShufflePlayers();
		BearDanceEvent = RaidInstance:CreateEvent( 3000, RequireDanceCount + 1 );
		RaidInstance:PlaySound( Bear, "HappyBear/DanceStart.ogg" );
		RaidInstance:PlaySound( Bear, "Music/Dance.ogg" );
	end
end

function OnUnitCastSpell( SpellID, UnitID )
	if Dancing and CurrentBearAnimationSpell > 0
	then
		local value = RaidInstance:ContainerMapFind( PlayerOldShapeMap, UnitID );
		if value == 0
		then
			return;
		end

		if SpellID == CurrentBearAnimationSpell
		then
			local value = RaidInstance:ContainerMapFind( EnrollPlayerMap, UnitID );
			value = value + 1;
			RaidInstance:ContainerMapModifyValue( EnrollPlayerMap, UnitID, value );
			RaidInstance:CreatureWhisper( Bear, UnitID, "你已做对了"..RaidInstance:ConvertInteger2String( value ).."/"..RaidInstance:ConvertInteger2String( RequireDanceCount ).."继续加油哦!" );
		else
			local r = LuaRandom( 1, 2 );
			if r == 1
			then
				RaidInstance:PlaySound( UnitID, "HappyBear/DanceWrong1.ogg" );
			else
				RaidInstance:PlaySound( UnitID, "HappyBear/DanceWrong2.ogg" );
			end
		end
	end
end
