--Author Jane Chen
--12/10/2010
--

local RaidInstance;	-- raid instance object
local deadCount1 = 3;
local deadCount2 = 4;
local deadCount3 = 9;
local xiaobinwang1_guid;
local xiaobinwang2_guid;
local xiaobinwang3_guid;
local nvshi1_guid;
local nvshi2_guid;
local nvshi3_guid;
local nvshi4_guid;
local nvshi5_guid;
local nvshi6_guid;
local nvshi7_guid;
local nvshi8_guid;
local nvshi9_guid;
local nvshi10_guid;
local dan1_guid;

local xiulishang_guid;
local boss1_guid = 0;
local boss2_guid = 0;
local boss1_died = 0;
local boss2_died = 0;
local bigboss_guid = 0;
local boss2_already_spawned = 0;
local fear_event = 0;
local icerain_event = 0;
local bianshen_1 = 0;
local bianshen_2 = 0;
local bianshen_3 = 0;
local bianshen_4 = 0;
local bianshen_5 = 0;
local bigboss_rampaged = 0;
local bigboss_spawn_event = 0;
local shake_event = 0;
local motal_strike_event = 0;

function SpawnEggs( void )
	local egg;
	egg = RaidInstance:SpawnGameObject( 10070, 782.0, 745.7, 20.3, 2.9, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 773.9, 750.5, 20.3, 6.3, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 768.1, 759.5, 20.3, 3.3, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 769.8, 774.0, 20.3, 3.3, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 778.8, 779.6, 20.3, 4.3, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 792.8, 775.2, 20.3, 5.2, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 797.9, 763.3, 20.3, 6.3, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 792.6, 750.3, 20.3, 0.7, 0, 2.0, 10, 1, 1 );	
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 798.5, 749.9, 20.3, 0.9, 0, 2.0, 10, 1, 1 );	
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 815.5, 759.8, 20.3, 2.0, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 814.6, 781.7, 20.3, 1.3, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 790.5, 786.5, 20.3, 0.7, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 762.4, 786.5, 20.3, 1.2, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 743.3, 772.4, 20.3, 1.6, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 738.8, 749.7, 20.3, 5.5, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 744.5, 726.4, 20.3, 5.6, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 771.2, 714.8, 20.3, 4.5, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 797.9, 720.5, 20.3, 4.3, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	egg = RaidInstance:SpawnGameObject( 10070, 817.4, 738.7, 20.3, 4.5, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );

	egg = RaidInstance:SpawnGameObject( 10070, 782.1, 731.2, 20.3, 3.1, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );

	egg = RaidInstance:SpawnGameObject( 10070, 764.0, 741.7, 20.3, 2.0, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );
	
	egg = RaidInstance:SpawnGameObject( 10070, 822.6, 772.2, 20.3, 4.7, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );	

	egg = RaidInstance:SpawnGameObject( 10070, 826.8, 752.5, 20.3, 6.3, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );	

	egg = RaidInstance:SpawnGameObject( 10070, 825.2, 723.4, 20.3, 0.3, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );	

	egg = RaidInstance:SpawnGameObject( 10070, 790.1, 709.7, 20.3, 1.3, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );	

	egg = RaidInstance:SpawnGameObject( 10070, 754.0, 709.8, 20.3, 0.7, 0, 2.0, 10, 1, 1 );
	RaidInstance:ModifyObjectScale( egg, 2.2 );	
end

function DespawnEggs( void )
	RaidInstance:DespawnGameObjectByEntry( 10070 );
	RaidInstance:DespawnCreatureByEntry( 3002 );
end

function OnStart( RaidInstanceID )		-- on instance start.
	-- catch the raid instance object. this is very important.
	RaidInstance = LuaGetRaidObject( RaidInstanceID );
	
	xiaobinwang1_guid = RaidInstance:SpawnCreature( 3001, 824.4, 762.8, 20.3, 0.6, 0);
	nvshi1_guid = RaidInstance:SpawnCreature( 3002, 828.1, 764.9, 20.3, 0.6, 0);
	nvshi2_guid = RaidInstance:SpawnCreature( 3002, 823.8, 766.2, 20.3, 0.6, 0);
	local ThreatChain1 = RaidInstance:CreateThreatChain();
	RaidInstance:AddCreatureToThreatChain( xiaobinwang1_guid, ThreatChain1 );
	RaidInstance:AddCreatureToThreatChain( nvshi1_guid, ThreatChain1 );
	RaidInstance:AddCreatureToThreatChain( nvshi2_guid, ThreatChain1 );
	
	xiaobinwang2_guid = RaidInstance:SpawnCreature( 3001, 751.8, 719.9, 20.3, 5.0, 0 );
	nvshi3_guid = RaidInstance:SpawnCreature( 3002, 750.1, 722.9, 20.3, 0.6, 0 );
	nvshi4_guid = RaidInstance:SpawnCreature( 3002, 748.9, 719.1, 20.3, 0.6, 0 );
	local ThreatChain2 = RaidInstance:CreateThreatChain();
	RaidInstance:AddCreatureToThreatChain( xiaobinwang2_guid, ThreatChain2 );
	RaidInstance:AddCreatureToThreatChain( nvshi3_guid, ThreatChain2 );
	RaidInstance:AddCreatureToThreatChain( nvshi4_guid, ThreatChain2 );
	
	xiaobinwang3_guid = RaidInstance:SpawnCreature( 3001, 780.4, 763.3, 20.3, 6.0, 0 );
	nvshi5_guid = RaidInstance:SpawnCreature( 3002, 783.2, 766.8, 20.3, 0.2, 0 );
	nvshi6_guid = RaidInstance:SpawnCreature( 3002, 779.1, 766.9, 20.3, 0.2, 0 );
	local ThreatChain3 = RaidInstance:CreateThreatChain();
	RaidInstance:AddCreatureToThreatChain( xiaobinwang3_guid, ThreatChain3 );
	RaidInstance:AddCreatureToThreatChain( nvshi5_guid, ThreatChain3 );
	RaidInstance:AddCreatureToThreatChain( nvshi6_guid, ThreatChain3 );
end

function OnUpdate( TimeElapsed )		-- instance update every second.		
	if boss1_guid > 0 and boss2_already_spawned == 0
	then
		local hp, mp;
		hp, mp = RaidInstance:GetCreatureBasicInfo( boss1_guid );
		if hp <= 0.9
		then
			boss2_guid = RaidInstance:SpawnCreature( 3004, 780.0, 767.9, 20.3, 6.2, 0 );
			RaidInstance:CreatureSay( boss2_guid, "兄弟，我来帮助你了!", 1 );			
			boss2_already_spawned = 1;
		end
	end
	
	if bigboss_guid > 0 and bianshen_1 == 0
	then
		local TargetID = RaidInstance:GetUnitTarget( bigboss_guid );
		local hp, mp;
		hp, mp = RaidInstance:GetCreatureBasicInfo( bigboss_guid );
		if hp < 0.9 and TargetID > 0
		then
			RaidInstance:DismissMountAndShift( TargetID );
			RaidInstance:CreatureCastSpell( bigboss_guid, 49, 1, TargetID, 1 );
			RaidInstance:ModifyThreat( bigboss_guid, TargetID, -60 );
			bianshen_1 = 1;
		end
	end
	
	if bigboss_guid > 0 and bianshen_2 == 0
	then
		local TargetID = RaidInstance:GetUnitTarget( bigboss_guid );
		local hp, mp;
		hp, mp = RaidInstance:GetCreatureBasicInfo( bigboss_guid );
		if hp < 0.5 and TargetID > 0
		then
			RaidInstance:DismissMountAndShift( TargetID );
			RaidInstance:CreatureCastSpell( bigboss_guid, 45, 1, TargetID, 1 );
			RaidInstance:ModifyThreat( bigboss_guid, TargetID, -60 );
			bianshen_2 = 1;
		end
	end
	
	if bigboss_guid > 0 and bianshen_3 == 0
	then
		local TargetID = RaidInstance:GetUnitTarget( bigboss_guid );
		local hp, mp;
		hp, mp = RaidInstance:GetCreatureBasicInfo( bigboss_guid );
		if hp < 0.7 and TargetID > 0
		then
			RaidInstance:DismissMountAndShift( TargetID );
			RaidInstance:CreatureCastSpell( bigboss_guid, 47, 1, TargetID, 1 );
			RaidInstance:ModifyThreat( bigboss_guid, TargetID, -60 );
			bianshen_3 = 1;
		end
	end
	
	if bigboss_guid > 0 and bianshen_4 == 0
	then
		local TargetID = RaidInstance:GetUnitTarget( bigboss_guid );
		local hp, mp;
		hp, mp = RaidInstance:GetCreatureBasicInfo( bigboss_guid );
		if hp < 0.8 and TargetID > 0
		then
			RaidInstance:DismissMountAndShift( TargetID );
			RaidInstance:CreatureCastSpell( bigboss_guid, 48, 1, TargetID, 1 );
			RaidInstance:ModifyThreat( bigboss_guid, TargetID, -60 );
			bianshen_4 = 1;
		end
	end
	
	if bigboss_guid > 0 and bianshen_5 == 0
	then
		local TargetID = RaidInstance:GetUnitTarget( bigboss_guid );
		local hp, mp;
		hp, mp = RaidInstance:GetCreatureBasicInfo( bigboss_guid );
		if hp < 0.6 and TargetID > 0
		then
			RaidInstance:DismissMountAndShift( TargetID );
			RaidInstance:CreatureCastSpell( bigboss_guid, 46, 1, TargetID, 1 );
			RaidInstance:ModifyThreat( bigboss_guid, TargetID, -60 );
			bianshen_5 = 1;
		end
	end
	
	if bigboss_guid > 0 and bigboss_rampaged == 0
	then
		local hp, mp;
		hp, mp = RaidInstance:GetCreatureBasicInfo( bigboss_guid );
		if hp < 0.2
		then
			bigboss_rampaged = 1;
			RaidInstance:CreatureSay( bigboss_guid, "你们够犀利！我狂暴了！", 1 );
			RaidInstance:ModifyObjectScale( bigboss_guid, 1.5 );
			RaidInstance:CreatureCastSpell( bigboss_guid, 30, 4, bigboss_guid, 1 );
			local PlayerID1 = RaidInstance:GetCreatureTargetByThreatIndex( bigboss_guid, 1 );
			local PlayerID2 = RaidInstance:GetCreatureTargetByThreatIndex( bigboss_guid, 2 );
			local PlayerID3 = RaidInstance:GetCreatureTargetByThreatIndex( bigboss_guid, 3 );
			local PlayerID4 = RaidInstance:GetCreatureTargetByThreatIndex( bigboss_guid, 4 );
			local PlayerID5 = RaidInstance:GetCreatureTargetByThreatIndex( bigboss_guid, 5 );
			local PlayerID6 = RaidInstance:GetCreatureTargetByThreatIndex( bigboss_guid, 6 );
			if PlayerID1 > 0
			then
				RaidInstance:DismissMountAndShift( PlayerID1 );
				RaidInstance:AddBuffToPlayer( PlayerID1, 42 );
			end
			if PlayerID2 > 0
			then
				RaidInstance:DismissMountAndShift( PlayerID2 );
				RaidInstance:AddBuffToPlayer( PlayerID2, 42 );
			end
			
			if PlayerID3 > 0
			then
				RaidInstance:DismissMountAndShift( PlayerID3 );
				RaidInstance:AddBuffToPlayer( PlayerID3, 43 );
			end
			
			if PlayerID4 > 0
			then
				RaidInstance:DismissMountAndShift( PlayerID4 );
				RaidInstance:AddBuffToPlayer( PlayerID4, 43 );
			end
			
			if PlayerID5 > 0
			then
				RaidInstance:DismissMountAndShift( PlayerID5 );
				RaidInstance:AddBuffToPlayer( PlayerID5, 44 );
			end
			
			if PlayerID6 > 0
			then
				RaidInstance:DismissMountAndShift( PlayerID6 );
				RaidInstance:AddBuffToPlayer( PlayerID6, 44 );
			end
		end
	end
end

function OnUnitDie( VictimID, AttackerID )
	if VictimID == boss1_guid
	then
		boss1_died = 1;
	end
	
	if VictimID == boss2_guid
	then
		boss2_died = 1;
	end
	
	if boss1_died == 1 and boss2_died == 1
	then
		RaidInstance:SendSystemNotify( "30秒后大王刷新，大家做好准备" );
		bigboss_spawn_event = RaidInstance:CreateEvent( 30000, 1 );
		shake_event = RaidInstance:CreateEvent( 20 * 1000, 1 );
		RaidInstance:DespawnCreature( boss1_guid );
		RaidInstance:DespawnCreature( boss2_guid );
		boss1_died = 0;
		boss2_died = 0;
	end
	
	if VictimID == xiaobinwang1_guid or VictimID == xiaobinwang2_guid or VictimID == xiaobinwang3_guid or VictimID == nvshi1_guid or VictimID == nvshi2_guid or VictimID == nvshi3_guid or VictimID == nvshi4_guid or VictimID == nvshi5_guid or VictimID == nvshi6_guid  
	then
		deadCount3 = deadCount3 -1;
	end
	
	if deadCount3 == 0
	then
		xiulishang_guid = RaidInstance:SpawnCreature( 612, 779.3, 776.1, 20.3, 6.2, 0 );
		RaidInstance:CreatureSay( xiulishang_guid, "欢迎来到XX副本，寻找不远处的龙蛋，将会触发副本开始", 1 );
		dan1_guid = RaidInstance:SpawnGameObject( 10070, 788.9, 730.4, 20.3, 6.1, 0, 2.0, 10, 1, 1 );
		RaidInstance:ModifyObjectScale( dan1_guid, 2.2 );
		deadCount3 = 9;
	end
	
	if VictimID == nvshi7_guid or VictimID == nvshi8_guid or VictimID == nvshi9_guid or VictimID == nvshi10_guid 
	then 
		deadCount2 = deadCount2 - 1;
	end
	
	if deadCount2 == 0
	then
    	boss1_guid = RaidInstance:SpawnCreature( 3003, 780.0, 767.9, 20.3, 6.2, 0 );
		RaidInstance:CreatureSay( boss1_guid, "谁把我叫醒了?!", 1 );
		deadCount2 = 4;
	end
	
	if VictimID == boss1_guid
	then
		RaidInstance:CreatureSay( boss1_guid, "纳尼！！！！！！！", 1 );
		boss1_guid = 0;
		if boss2_guid == 0
		then
			boss2_guid = RaidInstance:SpawnCreature( 3004, 780.0, 767.9, 20.3, 6.2, 0 );
			RaidInstance:CreatureSay( boss2_guid, "兄弟，我来帮助你了!", 1 );			
			boss2_already_spawned = 1;
		end
	end
	
	if VictimID == bigboss_guid
	then
		RaidInstance:CreatureSay( bigboss_guid, "亚灭跌！", 1 );
		RaidInstance:KillEvent( fear_event );
		RaidInstance:KillEvent( icerain_event );
		bigboss_guid = 0;
		DespawnEggs( 1 );
	end
end

function OnCreatureResurrect( CreatureID )
	if CreatureID == boss1_guid
	then
		boss1_died = 0;
	end
	
	if CreatureID == boss2_guid
	then
		boss2_died = 0;
	end	
end

function OnGameObjectTrigger( GameObjectID, PlayerID )
	if GameObjectID == dan1_guid
	then
		nvshi7_guid = RaidInstance:SpawnCreature( 3002, 791.9, 730.4, 20.3, 6.1, 0 );
		RaidInstance:CreatureSay( nvshi7_guid, "啊，新鲜的肉!", 1 );
		nvshi8_guid = RaidInstance:SpawnCreature( 3002, 785.9, 730.4, 20.3, 6.1, 0 );
		RaidInstance:CreatureSay( nvshi8_guid, "啊，新鲜的肉!", 1 );
		nvshi9_guid = RaidInstance:SpawnCreature( 3002, 788.9, 733.4, 20.3, 6.1, 0 );
		RaidInstance:CreatureSay( nvshi9_guid, "啊，新鲜的肉!", 1 );
		nvshi10_guid = RaidInstance:SpawnCreature( 3002, 788.9, 727.4, 20.3, 6.1, 0 );
		RaidInstance:CreatureSay( nvshi10_guid, "啊，新鲜的肉!", 1 );
	end

	local GOEntry = RaidInstance:GetGameObjectEntry( GameObjectID );
	if GOEntry == 10070 and bigboss_guid > 0
	then
		local x = 0.0;
		local y = 0.0;
		local z = 0.0;
		local o = 0.0;
		x, y, z, o = RaidInstance:GetObjectPosition( GameObjectID );
		local nvshi = RaidInstance:SpawnCreature( 3002, x, y, z, o, 0 );
		RaidInstance:CreatureSay( nvshi, "啊，新鲜的肉!", 1 );
		RaidInstance:ModifyThreat( nvshi, PlayerID, 1 );
	end
end

function OnUnitEnterCombat( UnitID )
	if UnitID == bigboss_guid or UnitID == boss1_guid or UnitID == boss2_guid
	then
		local PlayerCount = RaidInstance:RandomShufflePlayers();
		if PlayerCount > 0
		then
			for i = 1, PlayerCount, 1
			do
				local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
				if PlayerID > 0
				then
					RaidInstance:ModifyThreat( UnitID, PlayerID, 1 );
				end
			end
		end
	end
	
	if UnitID == bigboss_guid
	then
		SpawnEggs( 1 );
		fear_event = RaidInstance:CreateEvent( 25000, 10000 );
		icerain_event = RaidInstance:CreateEvent( 5000, 10000 );
	end
end

function OnUnitLeaveCombat( UnitID )
	if UnitID == bigboss_guid
	then
		RaidInstance:KillEvent( fear_event );
		RaidInstance:KillEvent( icerain_event );
		RaidInstance:KillEvent( motal_strike_event );
		DespawnEggs( 1 );
		bianshen_1 = 0;
		bianshen_2 = 0;
		bianshen_3 = 0;
		bianshen_4 = 0;
		bianshen_5 = 0;
		bigboss_rampaged = 0;
	end
end

function OnRaidWipe()	
	local PlayerCount = RaidInstance:RandomShufflePlayers();
	if PlayerCount > 0
	then
		for i = 1, PlayerCount, 1
		do
			local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
			if PlayerID > 0
			then
				RaidInstance:EjectPlayerFromInstance( PlayerID );
			end
		end
	end
	RaidInstance:CreatureSay( bigboss_guid, "不给力!", 1 );
end

function OnPlayerLeaveInstance( PlayerID )
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 42 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 43 );
	RaidInstance:RemoveBuffFromPlayer( PlayerID, 44 );
end

function OnEvent( EventID, Attach, Attach2, Attach3, Attach4, Attach5 )
	if EventID == fear_event and bigboss_guid > 0
	then
		RaidInstance:CreatureCastSpell( bigboss_guid, 38, 3, bigboss_guid, 0 );
	end
	
	if EventID == icerain_event and bigboss_guid > 0
	then
		RaidInstance:CreatureCastSpell( bigboss_guid, 61019, 3, bigboss_guid, 0 );
	end
	
	if EventID == bigboss_spawn_event
	then
		bigboss_guid = RaidInstance:SpawnCreature( 3005, 780.0, 767.9, 20.3, 6.2, 0 );
		RaidInstance:CreatureSay( bigboss_guid, "你们给力,居然杀掉我两个小弟!", 1 );
		RaidInstance:SetCreatureImmune( bigboss_guid, 5 );
		RaidInstance:SetCreatureImmune( bigboss_guid, 9 );
		RaidInstance:SetCreatureImmune( bigboss_guid, 12 );
		RaidInstance:ModifyObjectScale( bigboss_guid, 1.2 );
		motal_strike_event = RaidInstance:CreateEvent( 3000, 10000 );
	end
	
	if EventID == shake_event
	then
		RaidInstance:ScreenShake( 10 );
		shake_event = 0;
	end
	
	if EventID == motal_strike_event and bigboss_guid > 0
	then
		local TargetID = RaidInstance:GetUnitTarget( bigboss_guid );
		RaidInstance:CreatureCastSpell( bigboss_guid, 61015, 1, TargetID, 0 );
	end
end
