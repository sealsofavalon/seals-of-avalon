--Author Jane Chen
--12/10/2010
--

local RaidInstance;
local bigboss_guid = 0;
local MobsMap = 0;
local bosshanbinjian_event = 0;
local dot_event = 0;
local aoeAlert_event = 0;
local huoyuAlert_event = 0;
local xiaobinhanbinjian_event = 0;
local summoned_mobs_index = 0;
local ThreatChain = 0;
local FireRainMark = 0;
local FireRainMarkDestroySelfEvent = 0;
local kaichangbai_event = 0;
local chuansongren_guid = 0;
local TeleportGossipIndex = 0;

function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );

	bigboss_guid = RaidInstance:SpawnCreature( 3007, 1383.4, 1349.7, 43.9, 0.1, 0 );
	kaichangbai_event = RaidInstance:CreateEvent( 5 * 1000, 1 );
	RaidInstance:SetCreatureImmune( bigboss_guid, 5 );
	RaidInstance:SetCreatureImmune( bigboss_guid, 9 );
	RaidInstance:SetCreatureImmune( bigboss_guid, 12 );

	RaidInstance:ModifyObjectScale( bigboss_guid, 0.6 );
	MobsMap = RaidInstance:ContainerMapCreate();
	ThreatChain = RaidInstance:CreateThreatChain();
	RaidInstance:AddCreatureToThreatChain( bigboss_guid, ThreatChain );

	local mob_guid = 0;
	local ThreatChain = RaidInstance:CreateThreatChain();
	mob_guid = RaidInstance:SpawnCreature( 3025, 1106.2, 1403.5, 10.0, 3.5, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3027, 1100.5, 1402.7, 9.8, 3.6, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3026, 1100.6, 1394.6, 9.8, 3.6, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1108.4, 1397.6, 10.0, 2.0, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1103.9, 1393.4, 9.8, 2.0, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	local mob_guid = 0;
	local ThreatChain = RaidInstance:CreateThreatChain();
	mob_guid = RaidInstance:SpawnCreature( 3025, 1078.1, 1415.4, 10.1, 4.0, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3027, 1079.9, 1414.1, 10.0, 2.1, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1078.1, 1408.6, 11.2, 3.3, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	local mob_guid = 0;
	local ThreatChain = RaidInstance:CreateThreatChain();
	mob_guid = RaidInstance:SpawnCreature( 3025, 1135.1, 1397.8, 11.4, 2.5, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3027, 1132.5, 1398.8, 10.6, 5.3, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1135.4, 1401.1, 10.6, 0.6, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	local mob_guid = 0;
	local ThreatChain = RaidInstance:CreateThreatChain();
	mob_guid = RaidInstance:SpawnCreature( 3025, 1145.2, 1269.2, 32.4, 3.6, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3027, 1148.6, 1270.6, 31.7, 2.1, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3026, 1143.5, 1272.2, 32.7, 3.6, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1146.9, 1275.3, 31.0, 3.6, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	local mob_guid = 0;
	local ThreatChain = RaidInstance:CreateThreatChain();
	mob_guid = RaidInstance:SpawnCreature( 3025, 1203.3, 1285.1, 37.6, 1.3, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3027, 1203.4, 1282.5, 37.6, 0.6, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	local mob_guid = 0;
	local ThreatChain = RaidInstance:CreateThreatChain();
	mob_guid = RaidInstance:SpawnCreature( 3025, 1163.9, 1199.4, 30.2, 3.4, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3027, 1167.3, 1202.7, 31.1, 3.4, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3026, 1165.6, 1205.5, 30.7, 0.2, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1163.4, 1203.6, 30.3, 4.6, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );


	local mob_guid = 0;
	local ThreatChain = RaidInstance:CreateThreatChain();
	mob_guid = RaidInstance:SpawnCreature( 3025, 1162.2, 1111.5, 40.0, 2.6, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3027, 1165.8, 1114.5, 39.9, 2.6, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3026, 1157.4, 1110.7, 39.8, 2.5, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1158.7, 1119.0, 39.7, 2.5, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3026, 1156.4, 1117.2, 39.5, 2.5, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3026, 1153.3, 1116.8, 39.6, 2.5, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );


	local mob_guid = 0;
	local ThreatChain = RaidInstance:CreateThreatChain();
	mob_guid = RaidInstance:SpawnCreature( 3025, 1171.6, 1153.3, 40.2, 2.1, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3027, 1178.1, 1151.3, 40.2, 2.1, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3026, 1173.2, 1148.6, 40.2, 2.1, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1174.2, 1156.8, 40.3, 0.5, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );


	local mob_guid = 0;
	local ThreatChain = RaidInstance:CreateThreatChain();
	mob_guid = RaidInstance:SpawnCreature( 3025, 1240.2, 1076.3, 40.4, 2.7, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3027, 1242.3, 1081.4, 40.3, 2.5, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3026, 1237.5, 1073.9, 39.9, 2.5, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1237.0, 1082.8, 39.9, 2.3, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1235.7, 1078.9, 39.8, 2.3, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );


	local mob_guid = 0;
	local ThreatChain = RaidInstance:CreateThreatChain();
	mob_guid = RaidInstance:SpawnCreature( 3025, 1274.0, 1125.2, 39.9, 1.3, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3027, 1276.3, 1121.4, 39.7, 1.3, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3026, 1276.4, 1115.5, 39.6, 1.3, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1263.7, 1125.2, 40.2, 1.4, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3025, 1263.6, 1121.5, 40.1, 1.4, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3026, 1265.7, 1118.1, 40.0, 1.4, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3028, 1262.6, 1111.3, 40.2, 1.4, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	local mob_guid = 0;
	local ThreatChain = RaidInstance:CreateThreatChain();
	mob_guid = RaidInstance:SpawnCreature( 3025, 1333.8, 1109.5, 40.1, 1.0, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3027, 1333.4, 1105.8, 39.9, 2.0, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	mob_guid = RaidInstance:SpawnCreature( 3026, 1333.0, 1113.2, 40.2, 1.7, 0);
	RaidInstance:AddCreatureToThreatChain( mob_guid, ThreatChain );

	chuansongren_guid = RaidInstance:SpawnCreature( 3029, 1343.7, 1160.7, 39.7, 3.3, 0);
	TeleportGossipIndex = RaidInstance:InsertNPCGossip( chuansongren_guid, BuildLanguageString( 89 ) );
end

function SummonMobs( Index )
	RaidInstance:PlaySound( bigboss_guid, "Raid_71/shaerna_05.ogg" );
	local FirstThreatTarget = RaidInstance:GetCreatureTargetByThreatIndex( bigboss_guid, 1 );
	local MobID = 0;

	local MobsGossip = BuildLanguageString( 90 );

	MobID = RaidInstance:SpawnCreature( 3008, 1365.3, 1356.8, 45.0, 5.7, 0 );
	RaidInstance:CreatureSay( MobID, MobsGossip, 1 );
	RaidInstance:AddCreatureToThreatChain( MobID, ThreatChain );
	RaidInstance:ModifyThreat( MobID, FirstThreatTarget, 1 );
	RaidInstance:ContainerMapInsert( MobsMap, MobID, Index );

	MobID = RaidInstance:SpawnCreature( 3008, 1373.6, 1366.4, 45.0, 5.7, 0 );
	RaidInstance:CreatureSay( MobID, MobsGossip, 1 );
	RaidInstance:AddCreatureToThreatChain( MobID, ThreatChain );
	RaidInstance:ModifyThreat( MobID, FirstThreatTarget, 1 );
	RaidInstance:ContainerMapInsert( MobsMap, MobID, Index );

	MobID = RaidInstance:SpawnCreature( 3008, 1383.2, 1369.6, 45.1, 6.0, 0 );
	RaidInstance:CreatureSay( MobID, MobsGossip, 1 );
	RaidInstance:AddCreatureToThreatChain( MobID, ThreatChain );
	RaidInstance:ModifyThreat( MobID, FirstThreatTarget, 1 );
	RaidInstance:ContainerMapInsert( MobsMap, MobID, Index );

	MobID = RaidInstance:SpawnCreature( 3008, 1394.2, 1366.7, 45.2, 0.4, 0 );
	RaidInstance:CreatureSay( MobID, MobsGossip, 1 );
	RaidInstance:AddCreatureToThreatChain( MobID, ThreatChain );
	RaidInstance:ModifyThreat( MobID, FirstThreatTarget, 1 );
	RaidInstance:ContainerMapInsert( MobsMap, MobID, Index );
	summoned_mobs_index = Index;
end

function OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )
	if TeleportGossipIndex == GossipIndex and NPC_ID == chuansongren_guid
	then
		RaidInstance:TeleportPlayer( PlayerID, 1381.9, 1298.6, 44.7, 3.1 );
		RaidInstance:PlaySound( PlayerID, "Raid_71/Teleport2Center.wav" );
	end
end

function OnPlayerEnterInstance( PlayerID )
	--RaidInstance:PlaySound( 0, "Music/diyu.ogg", 1, 1, 0 );
end

function OnUpdate( TimeElapsed )
	if bigboss_guid > 0
	then
		local hp, mp;
		hp, mp = RaidInstance:GetCreatureBasicInfo( bigboss_guid );
		if hp <= 0.8
		then
			if summoned_mobs_index == 0
			then
				SummonMobs( 1 );
			end
		end

		if hp <= 0.6
		then
			if summoned_mobs_index == 1
			then
				SummonMobs( 2 );
			end
		end

		if hp <= 0.4
		then
			if summoned_mobs_index == 2
			then
				SummonMobs( 3 );
			end
		end

		if hp <= 0.2
		then
			if summoned_mobs_index == 3
			then
				SummonMobs( 4 );
			end
		end
	end
end

function OnUnitEnterCombat( UnitID )
	if UnitID == bigboss_guid
	then
		summoned_mobs_index = 0;
		bosshanbinjian_event = RaidInstance:CreateEvent(4000, 100000 );
		huoyuAlert_event = RaidInstance:CreateEvent( 30000, 1 );
		xiaobinhanbinjian_event = RaidInstance:CreateEvent(3000, 100000 );
		SummonMobs( 0 );
	end
end

function OnPlayerCastSpell( SpellID, PlayerID )
end

function OnUnitDie( VictimID, AttackerID )
	if VictimID == bigboss_guid
	then
		RaidInstance:CreatureSay( bigboss_guid, BuildLanguageString( 91 ), 1 );
		RaidInstance:PlaySound( bigboss_guid, "Raid_71/shaerna_06.ogg" );
		RaidInstance:PlaySound( bigboss_guid, "Raid_71/ShaernaDefeated.wav" );
		RaidInstance:KillEvent( bosshanbinjian_event );
		RaidInstance:KillEvent( huoyuAlert_event );
		RaidInstance:KillEvent( aoeAlert_event );
		RaidInstance:KillEvent( dot_event );
		RaidInstance:KillEvent( xiaobinhanbinjian_event );
		bigboss_guid = 0;
		RaidInstance:DespawnCreatureByEntry( 3008 );
		local chest = RaidInstance:SpawnGameObject( 10074, 1350.9, 1364.2, 46.4, 5.2, 0, 2.0, 10000, 1, 1 );
		RaidInstance:ModifyObjectScale( chest, 3.0 );

		local Count = RaidInstance:RandomShufflePlayers();

		for i = 1, Count, 1
		do
			local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
			RaidInstance:AddPlayerTitle( PlayerID, 115 );
		end
	end
	if AttackerID == bigboss_guid
	then
		RaidInstance:CreatureSay( bigboss_guid, BuildLanguageString( 92 ), 1 );
	end

	--if RaidInstance:GetCreatureEntry( VictimID ) == 3008
	--then
	--	RaidInstance:DespawnCreature( VictimID );
	--end
end

function OnUnitLeaveCombat( UnitID )
	if UnitID == bigboss_guid
	then
		RaidInstance:KillEvent( bosshanbinjian_event );
		RaidInstance:KillEvent( huoyuAlert_event );
		RaidInstance:KillEvent( aoeAlert_event );
		RaidInstance:KillEvent( dot_event );
		RaidInstance:KillEvent( xiaobinhanbinjian_event );
		RaidInstance:DespawnCreatureByEntry( 3008 );
		RaidInstance:ContainerMapClear( MobsMap );
		summoned_mobs_index = 0;
	end
end

function OnEvent( EventID, Attach, Attach2, Attach3, Attach4, Attach5 )
	if EventID == bosshanbinjian_event
	then
		RaidInstance:RandomShufflePlayers();
		local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
		if PlayerID > 0
		then
			RaidInstance:CreatureCastSpell( bigboss_guid, 51, 3, bigboss_guid, 0 );
		end
	end

	if EventID == dot_event
	then
		RaidInstance:CreatureCastSpell( bigboss_guid, 52, 3, bigboss_guid, 1 );
		RaidInstance:CreatureSay( bigboss_guid, BuildLanguageString( 93 ), 1 )
		RaidInstance:PlaySound( bigboss_guid, "Raid_71/shaerna_04.ogg" );
		huoyuAlert_event = RaidInstance:CreateEvent( 10000, 1 );
	end

	if EventID == aoeAlert_event
	then
		RaidInstance:PlaySound( bigboss_guid, "Raid_71/Teleport2Center.wav" );
		RaidInstance:CreatureCastSpell( bigboss_guid, 9, 3, bigboss_guid, 1 );
		RaidInstance:CreatureCastSpell( bigboss_guid, 53, 3, bigboss_guid, 1 );
		--local PlayerCount = RaidInstance:RandomShufflePlayers();
		--if PlayerCount > 0
		--then
		--	for i = 1, PlayerCount, 1
		--	do
		--		local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
		--		if PlayerID > 0
		--		then
		--			RaidInstance:TeleportPlayer( PlayerID, 1381.8, 1349.0, 43.9, 4.4 );
		--		end
		--	end
		--end
		RaidInstance:CreatureSay( bigboss_guid, BuildLanguageString( 94 ), 1 );
		RaidInstance:PlaySound( bigboss_guid, "Raid_71/shaerna_02.ogg" );

		dot_event = RaidInstance:CreateEvent( 25000, 1 );
	end

	if EventID == huoyuAlert_event
	then
		RaidInstance:CreatureSay( bigboss_guid, BuildLanguageString( 95 ), 1 );
		RaidInstance:PlaySound( bigboss_guid, "Raid_71/shaerna_03.ogg" );
		RaidInstance:RandomShufflePlayers();
		local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
		if PlayerID > 0
		then
			RaidInstance:CreatureCastSpell( bigboss_guid, 54, 2, PlayerID, 1 );
			local x, y, z, o;
			x, y, z, o = RaidInstance:GetObjectPosition( PlayerID );
			if x > 0.0 or y > 0.0 or z > 0.0 or o > 0.0
			then
				RaidInstance:SceneEffect( "PlayFromServer/Raid_71/bingbaoshu/caiji_00048.nif", x, y, z, 1, 1.0, "Raid_71/IceBlast.wav", 4700 );
				--FireRainMark = RaidInstance:SpawnGameObject( 10072, x, y, z, o, 0, 2.0, 10, 1, 1 );
				--if FireRainMark > 0
				--then
				--	FireRainMarkDestroySelfEvent = RaidInstance:CreateEvent( 10 * 1000, 1 );
				--end
			end
		end

		aoeAlert_event = RaidInstance:CreateEvent( 15000, 1 );
	end

	if EventID == xiaobinhanbinjian_event
	then
		local FirstThreatTarget = RaidInstance:GetCreatureTargetByThreatIndex( bigboss_guid, 1 );
		local Count = RaidInstance:ContainerMapBegin( MobsMap );
		if Count > 0
		then
			for i = 1, Count, 1
			do
				local MobID = RaidInstance:ContainerMapNext( MobsMap );
				RaidInstance:CreatureCastSpell( MobID, 55, 1, FirstThreatTarget, 1 );
			end
		end
	end

	if EventID == FireRainMarkDestroySelfEvent and FireRainMark > 0
	then
		RaidInstance:DespawnGameObject( FireRainMark );
		FireRainMark = 0;
	end

	if EventID == kaichangbai_event
	then
		RaidInstance:PlaySound( bigboss_guid, "Raid_71/shaerna_01.ogg" );
		RaidInstance:CreatureSay( bigboss_guid, BuildLanguageString( 96 ), 1 );
		kaichangbai_event = 0;
	end

end
