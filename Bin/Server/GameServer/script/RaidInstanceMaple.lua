-- Raid instance lua sample
-- float value must be written like this: 1200.0
-- integer value must be written like this: 10
-- RaidInstance must be catched


local RaidInstance;	-- raid instance object

-- object
local GoodBoss, BadBoss;
local Boss_Ghost = {};
local Grave = {};

-- Event
local Event_TombShow;
local Event_GhostResurrection;
local Event_BossChanges;
local Event_GhostFight;
local Event_BadBossFight1, Event_BadBossFight2, Event_BadBossDeathFire;

-- other variables
local StartTime, CurrentTime;
local Trigger_GhostDie = 0;
local x, y, z, o; --Boss_Ghost's coordinate
local Position; -- the Boss initialized position
local Boss_X, Boss_Y, Boss_Z, Boss_O; -- the Boss's coordinate
local GhostDieMap;


function OnStart( ID )		-- on instance start.

	-- catch the raid instance object. this is very important.
	RaidInstance = LuaGetRaidObject( ID );
 	
 	-- 初始化怪物
	Boss_Ghost[1] = RaidInstance:SpawnCreature( 3019, 1360.8, 1218.3, 0.0, 6.3, 1, 0 );
	Boss_Ghost[2] = RaidInstance:SpawnCreature( 3019, 1317.7, 1154.1, 0.0, 4.0, 1, 0 );
	Boss_Ghost[3] = RaidInstance:SpawnCreature( 3019, 1405.6, 1149.3, 0.0, 2.2, 1, 0 );	
	Position = RaidInstance:CreatePosition( 1365.8, 1175.7, 0.0, 2.9 );
	Boss_X, Boss_Y, Boss_Z, Boss_O = RaidInstance:GetPosition( Position );
    GoodBoss = RaidInstance:SpawnCreature( 3022, Boss_X, Boss_Y, Boss_Z, Boss_O, 0, 10000 );	
    StartTime = os.time;
    
    GhostDieMap = RaidInstance:ContainerMapCreate();
end

function OnUpdate( TimeElapsed )		-- instance update every second.
	
	-- At first 10min, the GoodBoss asks for help every 10sec.
	if  TimeElapsed < 600 
	then
		CurrentTime = GetCurrentTime();
		if CurrentTime % 10 == 0
		then
			RaidInstance:CreatureSay( GoodBoss, "Please help me!! Kill the wizard!", 1 )
		end
	end
	
	-- At first 10min, when a Boss_ghost was killed and a tomb show up, creat Event_GhostResurrection().
	if  Trigger_GhostDie > 0 and TimeElapsed < 600
	then
		Event_GhostResurrection = RaidInstance:CreatEvent( 1 * 1000, 1 );
	end	
	
	-- At 10min, creat Event_BossChanges().
	if TimeElapsed == 600
	then
		Event_BossChanges = RaidInstance:CreatEvent( 1 * 1000, 1 );
	end
	
	-- At BadBoss is halfhp, change Event_BadBossFight1 to Event_BadBossFight1, the fight goes to the second part
	if BadBoss ~= nil 
	then 
		local hp, mp;
		local CastCountDown = 5;
		hp, mp = RaidInstance:GetCreatureBasicInfo( BadBoss );
		if  hp < 0.5 and hp > 0 and CastCountDown > 0
		then
			RaidInstance:KillEvent( Event_BadBossFight1 );
			Event_BadBossFight2 = RaidInstance:CreatEvent( 1 * 1000, 1 );
		end
		if  CastCountDown == 0 and hp > 0 
		then
			Event_BadBossDeathFire = RaidInstance:CreatEvent( 1 * 1000, 1 );
		end
	end
end

function OnEnd()
	

end


function OnObjectDie( VictimID, AttackerID )

	-- At first 10min, when a Boss_ghost was killed, creat Event_TombShow(). 
	if  VictimID == Boss_Ghost[0] or VictimID == Boss_Ghost[1] or VictimID == Boss_Ghost[2]
	then
		Event_TombShow = RaidInstance:CreatEvent( 1 * 1000, 1 );
		RaidInstance:ContainerMapInsert( GhostDieMap, Event_TombShow, VictimID );
	end
	
	if  VictimID == BadBoss
	then 
		RaidInstance:CreatureSay( BadBoss, "Unbelivable, a human can kill me...", 1 );
		RaidInstance:PlaySound( BadBoss, "Raid_71/shaerna_03.ogg" ); -- 神秘的声音说：“谢谢你，我的灵魂终于得以安息了！”
	end
end

function OnGameObjectTrigger( GameObjectID, PlayerID )
end

function OnUnitEnterCombat( UnitID )

	-- When boss_ghost begin fights,creat Event_GhostFight().
	if UnitID == Boss_Ghost[i]
	then
		Event_GhostFight = RaidInstance:CreatEvent( 1 * 1000, 1 );
	end
end

function OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )
end

function OnRaidWipe()
	if  BadBoss ~= nil
	then
		RaidInstance:PlaySound( BadBoss, "Raid_71/shaerna_03.ogg" );-- 神秘的声音说：“哈哈哈哈哈哈哈哈哈哈哈！”
	else 
		RaidInstance:PlaySound( GoodBoss, "Raid_71/shaerna_03.ogg" );-- 神秘的声音说：“哈哈哈哈哈哈哈哈哈哈哈！”
	end
	local PlayerCount = RaidInstance:RandomShufflePlayers();
	if PlayerCount > 0
	then
		for i = 1, PlayerCount, 1
		do
			local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
			if PlayerID > 0
			then
				RaidInstance:EjectPlayerFromInstance( PlayerID );
			end
		end
	end
end

function OnPlayerEnterInstance( PlayerID )
end

function OnPlayerLeaveInstance( PlayerID )
end

function OnPlayerCastSpell( SpellID, PlayerID )
end




 
-- Event_TombShow: A tomb show nearby the body & told the system that a ghots was dead.
function Event_TombShow()
		local j;
		x, y, z, o = RaidInstance:GetObjectPosition( VictimID );
		j = i;
		Grave[j] = RaidInstance:SpawnGameObject( 10023, ( x + 2 ), ( y + 2 ), z, o, 1, 10, 10000, 1, 0 );
		Trigger_GhostDie = Trigger_GhostDie + 1;
end


-- Event_GhostResurrection: the ghost will back in 90sec.
function Event_GhostResurrection()
	if  TimeCountDown( 90 ) == 0
	then
		RaidInstance:DespawnGameObject( Grave[j] );
		--RaidInstance:ResurrectCreature( Boss_Ghost[i], x, y, z, o );
		Boss_Ghost[i]
		RaidInstance:CreatureSay( Boss_Ghost[i], "i won't perish as long as he is alive", 1 );
		Trigger_GhostDie = Trigger_GhostDie - 1;	
	end	
end

-- Event_BossChanges: the GoodBoss changes to the BadBoss,and he will resurrect the ghosts.
function Event_BossChanges()
	RaidInstance:InsertNPCGossip( GoodBoss, "Oh,NO!! The Darkness is surrounding me..." )
	RaidInstance:ModifyCreatureMPPct( GoodBoss, 0 );
	if 	TimeCountDown ( 59 ) == 0
	then 
		RaidInstance:SpawnGameObject( 10075, 1365.8, 1175.7, 0.0, 2.9, 0, 400, 10, 0, 10 );
		RaidInstance:DespawnCreature( GoodBoss );
		BadBoss = RaidInstance:SpawnCreature( 3021, Boss_X, Boss_Y, Boss_Z, Boss_O, 0, 10 );
		RaidInstance:CreatureSay( BadBoss, "I'm the DarkLord!!Everyone will die!", 1 );
		RaidInstance:CreatureCastSpell( BadBoss, 90009, 4, BadBoss );
		local hp1, mp1, hp2, mp2, hp3, mp3;
		hp1, mp1 = RaidInstance:GetCreatureBasicInfo( Boss_Ghost[1] );
	 	hp2, mp2 = RaidInstance:GetCreatureBasicInfo( Boss_Ghost[2] );
	 	hp3, mp3 = RaidInstance:GetCreatureBasicInfo( Boss_Ghost[3] );	 	
		if hp1 == 0
		then
			RaidInstance:ResurrectCreature( Boss_ghost1, 1360.8, 1218.3, 0.0, 6.3 )
		end
		if hp2 == 0
		then
			RaidInstance:ResurrectCreature( Boss_ghost2, 1317.7, 1154.1, 0.0, 4.0 )
		end
		if hp3 == 0
		then
			RaidInstance:ResurrectCreature( Boss_ghost3, 1405.6, 1149.3, 0.0, 2.2 )
		end		
		if hp1 > 0 and hp1 < 0.5 
		then
			RaidInstance:CreatureCastSpell( Boss, 90003, 1, Boss_Ghost[1] );
		end
		if hp2 > 0 and hp2 < 0.5
		then
			RaidInstance:CreatureCastSpell( Boss, 90003, 1, Boss_Ghost[2] );
		end
		if hp3 > 0 and hp3 < 0.5
		then
			RaidInstance:CreatureCastSpell( Boss, 90003, 1, Boss_Ghost[3] );
		end			
	end
end

-- Event_GhostFight: the Ghost fight AI
-- AI Description:
-- The Ghost has two skills: 1\bingfeng dadi gai & 2\yanbao shu gai;
-- Before the BadBoss appears, the ghost casts two skills alternately, After the BadBoss appears, the ghost casts skills 1 only
function Event_GhostFight()
	if  BadBoss == nil
	then
		local seed1 = math.randomseed( 2 );
		if  seed1 == 1
		then
			RaidInstance:CreatureCastSpell( Boss_Ghost[i], 90004, 2, Boss_Ghost[i] );
		else 
			RaidInstance:GetCreatureThreatListCount( Boss_Ghost[i] );
			TargetID = RaidInstance:GetCreatureTargetByThreatIndex( Boss_Ghost[i], 1 );
			RaidInstance:CreatureCastSpell( Boss_Ghost[i], 90005, 2, TargetID );	
		end
	else 
		RaidInstance:GetRandomPlayerByClass( 1 );
		RaidInstance:CreatureCastSpell( Boss_Ghost[i], 90004, 2, Boss_Ghost[i] );
	end
end

-- Event_GhostFight: the Ghost fight AI
-- AI Description:
-- the BadBoss has two stages: 1\Fratics Stage & 2\Protected Stage
-- 1\Fratics Stage: two skills, high hp, high damage, physicaldamage;
-- 1\Skills: buff siwang zhili; fengbao liren gai & emo zhisheng 
function Event_BadBossFight1()
	RaidInstance:SetCreatureImmune( BadBoss, 5 );
	RaidInstance:SetCreatureImmune( BadBoss, 9 );
	RaidInstance:SetCreatureImmune( BadBoss, 12 );
	local seed2 = math.randomseed( 2 );
	if  seed2 == 1
	then
		RaidInstance:CreatureSay( BadBoss, "I'll kill you!!!", 1 );
		RaidInstance:CreatureCastSpell( BadBoss, 90006, 4, BadBoss );
	else
		-- emo zhisheng: random choose a class,make all the players in the class fear.
		RaidInstance:CreatureSay( BadBoss, "Listen to the Death!!", 1 );
		RaidInstance:CreatureCastSpell( BadBoss, 90006, 4, BadBoss );
		local seed3 = math.randomseed( 4 );
		local Count = RaidInstance:RandomShufflePlayers();
		for k = 1, Count, 1 
		do
			local Player = RaidInstance:GetNextRandomShufflePlayer();	
			local PlayerClass, level, hp, mp = RaidInstance:GetPlayerBasicInfo( Player );
			if  PlayerClass == seed3
			then 
				RaidInstance:AddBuffToPlayer( Player, 30503 );
			end
		end	
	end	
end


-- 1\Protected Stage: two skills, castdamage, summon creatures.
-- 1\Skills: buff hanbing pingzhang; xianxue jisi & siwang huoyan
function Event_BadBossFight2()
	RaidInstance:CreatureSay( BadBoss, "Good! I'll feed my blood to the DarkCreatures! Quivering, human.", 1 );	
	RaidInstance:CreatureCastSpell( BadBoss, 90001, 4, BadBoss );
	RaidInstance:SpawnCreature( 3019, Boss_X, ( Boss_Y + 5 ), Boss_Z, Boss_O, 0, 10 );
	RaidInstance:SpawnCreature( 3019, ( Boss_X + 5 ), ( Boss_Y + 2 ), Boss_Z, Boss_O, 0, 10 );
	RaidInstance:SpawnCreature( 3019, ( Boss_X - 5 ), ( Boss_Y + 2 ), Boss_Z, Boss_O, 0, 10 );
	RaidInstance:SpawnCreature( 3019, ( Boss_X + 2 ), ( Boss_Y - 5 ), Boss_Z, Boss_O, 0, 10 );
	RaidInstance:SpawnCreature( 3019, ( Boss_X - 2 ), ( Boss_Y - 5 ), Boss_Z, Boss_O, 0, 10 );
	CastCountDown = CastCountDown - 1;
end
function Event_BadBossDeathFire()
	RaidInstance:CreatureCastSpell( BadBoss, 90001, 3, BadBoss );
end

function GetCurrentTime()
	local Time1 = StartTime;
	local Time2 = os.time - StartTime;
	return Time2;
end

function TimeCountDown ( time )
	local TimeCount = time;
	TimeCount = TimeCount -1;
	if TimeCount == 0
	then
		return 0;
	else 
		return TimeCount;
	end
end





function OnEvent( EventID, Attach, Attach2, Attach3, Attach4, Attach5 )
	if  EventID == Event_TombShow
	then
		Event_TombShow();
	end
	if  EventID == Event_GhostResurrection
	then
		Event_GhostResurrection();
	end
	if  EventID == Event_BossChanges
	then 
		Event_BossChanges();
	end
	if  EventID == Event_GhostFight
	then 
		Event_GhostFight();
	end
	if  EventID == Event_BadBossFight1
	then 
		Event_BadBossFight1();
	end
	if  EventID == Event_BadBossFight2
	then 
		Event_BadBossFight2();
	end 
	if  EventID == Event_BadBossDeathFire
	then
		Event_BadBossDeathFire();
	end
end