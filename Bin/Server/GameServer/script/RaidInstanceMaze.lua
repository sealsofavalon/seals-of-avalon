-- wufan
-- maze lua 
-- 2011/01/14

local RaidInstance;

-- maze gate info 
-- npc and condition
local ToMazeNPC = 0  ;  
local ToMazeIndex = 0 ; 
local ToMazeNPCX = 739.8;
local ToMazeNPCY = 774.7;
local ToMazeNPCZ = 28.5;
local ToMazeNPCO = 4.6;


-- maze Out Gate info
local OutMazeNPC = 0;
local OutMazeIndex = 0;
local OutMazeNPCX = 1264.3;
local OutMazeNPCY = 780.2;
local OutMazeNPCZ = 29.2;
local OutMazeNPCO = 4.6;


-- maze map
local GridStateMap = 0 ; -- state map
local GridPositionIndexMap = 0 ; -- Position index map
local GridObjMap = 0; -- obj map 
local ReliveCountMap = 0; -- relive count map

local L_MazeBuf = 90103; -- 进入迷宫后的玩家身上的BUF

local L_StartPositionIndexR = 0; -- Start condition  X  const
local L_StartPositionIndexC = 4; -- Start condition  Y  const

local L_MaxR_Count = 15;  -- 迷宫的列数 const
local L_MaxC_Count = 7;   -- 迷宫的行数 const

local L_DeadStateGameOBJ = 10079 ; --死亡节点的对象 
local L_MaxReliveCount = 10000; -- 单个玩家复活次数

local L_EndPositionIndex = 0; -- 出口
local L_EndGameOBJ = 0; --出口放置的对象
local L_EndGameEvt = 0; --副本失败 evt
local L_StartEvent = 0; --副本开始 evt

-- test for shun
local L_TestCreate = 0;
local L_TestPosStart = 0;
local L_TestPosEnd = 0;
local L_TestMoveEvt_F_Flag = 0;
local L_TestMoveEvt_B_Flag = 0;
local L_TestSpell = 71;

function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );
	
	
	ToMazeNPC = RaidInstance:SpawnCreature( 3059, ToMazeNPCX, ToMazeNPCY, ToMazeNPCZ, ToMazeNPCO, 1 );
	ToMazeIndex = RaidInstance:InsertNPCGossip( ToMazeNPC, "进入迷宫寻找卡洛！" );
	RaidInstance:EnableNPCGossip( ToMazeNPC, ToMazeIndex, 1 );
	
-- test for shun
	L_TestCreate = RaidInstance:SpawnCreature( 2006, 765.0, 776.8, 28.7, 4.7,1 );
	RaidInstance:ModifyObjectScale( L_TestCreate, 4.2 );
	L_TestPosStart = RaidInstance:CreatePosition(765.0, 776.8, 28.7, 4.7);
	L_TestPosEnd = RaidInstance:CreatePosition(726.0, 778.9, 28.3, 1.7);
	L_TestCreatePos = RaidInstance:CreatePosition()
	L_TestMoveEvt_B_Flag= 1;
	
	GridStateMap = RaidInstance:ContainerMapCreate(); -- 状态
	GridPositionIndexMap = RaidInstance:ContainerMapCreate();  --位置
	GridObjMap = RaidInstance:ContainerMapCreate();  --对象
	ReliveCountMap = RaidInstance:ContainerMapCreate(); 
	
	
	L_EndPositionIndex = RaidInstance:CreateMazeStateMap(GridStateMap); --获取迷宫地图
	
	--i = 0
	
	local PositionID = RaidInstance:CreatePosition(855.3, 718.8,24.3,4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  0 , PositionID);

	PositionID = RaidInstance:CreatePosition(855.3, 744.7,28.1,4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  1 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(858.5, 768.3, 28.1, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  2 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(857.8, 786.7, 28.1, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  3 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(863.5, 817.0, 24.1, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  4 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(862.8, 830.9, 24.2, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  5 , PositionID);	

	
	PositionID = RaidInstance:CreatePosition(861.3, 848.2, 24.2, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  6 , PositionID);	
	
-- i = 1
	PositionID = RaidInstance:CreatePosition(878.2, 716.7, 24.3, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  7 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(880.5, 739.9, 24.5, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  8 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(877.2, 767.7, 24.5, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  9 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(877.0, 784.5, 24.1, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  10 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(882.0, 807.0, 24.1, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  11 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(877.6, 829.6, 24.2, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  12 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(877.4, 847.9, 24.2, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  13 , PositionID);
	
-- i = 2	

	PositionID = RaidInstance:CreatePosition(903.6, 722.6, 24.3, 4.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  14 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(899.1, 745.2, 24.4, 3.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  15 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(905.1, 762.6, 24.5, 3.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  16 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(902.4, 784.5, 24.5, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  17 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(902.0, 807.6, 24.2, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  18 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(902.0, 830.9, 24.2, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  19 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(902.2, 850.5, 24.2, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  20 , PositionID);
	
	
-- i =3
	PositionID = RaidInstance:CreatePosition(928.2, 718.5, 26.3, 6.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  21 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(928.2, 739.5, 24.8, 6.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  22 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(931.5, 762.9, 25.4, 6.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  23 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(927.2, 784.2, 24.3, 6.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  24 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(927.4, 807.5, 24.3, 6.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  25 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(927.5, 831.2, 24.2, 6.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  26 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(927.3, 850.3, 24.2, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  27 , PositionID);
	
-- i = 4	
	PositionID = RaidInstance:CreatePosition(952.7, 718.6, 28.0, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  28 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(953.2, 738.7, 28.1, 3.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  29 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(953.2, 764.0, 28.1, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  30 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(953.1, 783.7, 28.1, 3.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  31 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(953.5, 807.7, 24.4, 2.9);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  32 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(950.9, 830.6, 28.1, 3.3);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  33 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(953.1, 853.1, 28.1, 3.3);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  34 , PositionID);	
	
	
	
-- i = 5

	PositionID = RaidInstance:CreatePosition(975.6, 716.0, 28.0, 0.3);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  35 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(977.7, 739.0, 28.1, 6.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  36 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(977.6, 763.8, 28.1, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  37 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(976.2, 783.9, 28.1, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  38 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(975.6, 810.7, 28.1, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  39 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(975.7, 829.5, 28.1, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  40 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(975.3, 852.8, 28.1, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  41 , PositionID);
	
-- i = 6
	PositionID = RaidInstance:CreatePosition(999.0, 718.9, 28.0, 3.3);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  42 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1001.1, 738.9, 28.0, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  43 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1001.0, 763.4, 28.1, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  44 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1003.2, 782.3, 28.1, 2.9);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  45 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1001.6, 807.9, 28.1, 3.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  46 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1000.2, 831.9, 28.1, 2.9);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  47 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(999.6, 852.2, 28.1, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  48 , PositionID);
	
--i = 7	
	PositionID = RaidInstance:CreatePosition(1025.9, 718.9, 28.0, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  49 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1026.2, 738.6, 28.0, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  50 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1027.3, 760.7, 27.2, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  51 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1026.1, 781.1, 25.5, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  52 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1026.1, 808.6, 28.1, 6.3);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  53 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1026.9, 831.7, 28.1, 0.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  54 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1026.9, 855.5, 28.1, 5.6);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  55 , PositionID);
	
--i = 8
	PositionID = RaidInstance:CreatePosition(1052.4, 716.0, 23.5, 4.5);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  56 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1050.7, 739.8, 23.5, 3.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  57 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1050.7, 762.7, 23.4, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  58 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1050.6, 783.8, 23.5, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  59 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1052.4, 808.8, 28.3, 3.3);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  60 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1053.8, 830.6, 28.1, 3.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  61 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1051.8, 855.5, 28.1, 3.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  62 , PositionID);

--i = 9
	PositionID = RaidInstance:CreatePosition(1070.8, 711.4, 23.5, 0.4);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  63 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1080.2, 736.3, 23.5, 0.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  64 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1082.9, 762.3, 23.5, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  65 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1082.5, 778.3, 23.5, 3.3);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  66 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1076.4, 810.6, 28.3, 6.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  67 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1076.5, 831.9, 28.1, 6.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  68 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1074.5, 856.1, 28.1, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  69 , PositionID);
	
--i = 10
	PositionID = RaidInstance:CreatePosition(1098.5, 714.4, 23.5, 3.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  70 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1098.5, 736.3, 23.5, 3.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  71 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1102.0, 762.1, 23.9, 3.3);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  72 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1104.6, 786.3, 23.5, 3.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  73 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1102.4, 807.4, 25.4, 2.7);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  74 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1101.9, 832.9, 27.8, 3.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  75 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1099.1, 854.3, 28.1, 3.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  76 , PositionID);

--i = 11
	PositionID = RaidInstance:CreatePosition(1128.2, 715.7, 24.9, 6.3);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  77 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1127.5, 737.3, 24.7, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  78 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1128.1, 758.9, 25.2, 6.3);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  79 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1125.1, 781.2, 24.8, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  80 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1127.7, 807.9, 24.7, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  81 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1127.3, 832.0, 24.5, 0.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  82 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1128.2, 851.7, 26.9, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  83 , PositionID);
	
--i = 12
	PositionID = RaidInstance:CreatePosition(1149.2, 718.4, 26.4, 2.5);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  84 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1153.2, 739.3, 28.1, 3.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  85 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1153.3, 763.0, 26.4, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  86 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1153.5, 785.9, 25.9, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  87 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1153.7, 807.8, 27.5, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  88 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1153.7, 831.3, 26.2, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  89 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1153.9, 854.5, 26.5, 1.8);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  90 , PositionID);
	
--i = 13
	PositionID = RaidInstance:CreatePosition(1172.3, 714.3, 28.1, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  91 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1173.9, 737.0, 28.2, 3.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  92 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1174.3, 761.0, 28.1, 0.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  93 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1179.8, 786.9, 28.1, 3.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  94 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1176.8, 811.3, 28.1, 6.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  95 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1176.5, 832.2, 28.4, 0.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  96 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1175.1, 852.5, 27.9, 6.25);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  97 , PositionID);
	
--i = 14
	PositionID = RaidInstance:CreatePosition(1201.1, 714.1, 28.1, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  98 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1201.7, 739.5, 28.1, 3.1);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  99 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1201.5, 762.9, 28.1, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  100 , PositionID);	
	
	
	PositionID = RaidInstance:CreatePosition(1201.5, 786.0, 28.1, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  101 , PositionID);	
	
	PositionID = RaidInstance:CreatePosition(1201.6, 810.6, 28.1, 0.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  102 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1201.5, 831.4, 28.1, 6.2);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  103 , PositionID);
	
	PositionID = RaidInstance:CreatePosition(1201.7, 850.6, 28.2, 1.0);
	RaidInstance:ContainerMapInsert(GridPositionIndexMap,  104 , PositionID);
	-- 初始化格里对应的数据
	for i = 1, L_MaxR_Count, 1
	do
		for j = 1, L_MaxC_Count, 1
		do 	
		
			local PosID =  RaidInstance:ContainerMapFind(GridPositionIndexMap, (i - 1) * L_MaxC_Count + j - 1);
			local State = GetState(i - 1, j - 1);
	
			local x, y,z, o = RaidInstance:GetPosition(PosID);
			local objID = 0;
	
			if State == 2
			then
				objID = RaidInstance:SpawnGameObject( L_DeadStateGameOBJ, x, y, z, o, 0, 25.0, 10, 0, 1 );  -- 为2 表示死亡陷阱
				RaidInstance:ModifyObjectScale( objID, 2.2 );
			elseif State == 3
			then
				 objID = RaidInstance:SpawnGameObject( 10081, x, y, z, o, 0, 2.0, 10000, 1, 1 );  -- 为3 表示宝箱
			elseif State == 1
			then
				if( (i - 1) * L_MaxC_Count + j - 1) == L_EndPositionIndex
				then
					--10070
					L_EndGameOBJ = RaidInstance:SpawnGameObject( 10085, x, y, z, o, 0.0, 4.0, 10.0, 10, 1, 1); --
					objID = L_EndGameOBJ;
					RaidInstance:ModifyObjectScale( objID, 2.2 );
				end	
			end
	
			RaidInstance:ContainerMapInsert(GridObjMap, (i - 1) * L_MaxC_Count + j - 1,  objID );
		end
	end	
	

	
	L_StartEvent = RaidInstance:CreateEvent( 30000, 10 );
	
end

function OnUpdate( TimeElapsed )

-- 当陷阱组件消失的时候， 在当前位置在刷新一个
	for i = 1, L_MaxR_Count, 1
	do
		for j = 1, L_MaxC_Count, 1
		do 	
			local State = GetState(i - 1, j - 1);
			if State == 2
			then
				local objID = RaidInstance:ContainerMapFind(GridObjMap, (i - 1) * L_MaxC_Count + j - 1);
				local NotHasObj = RaidInstance:HasObject(objID);
				if NotHasObj == 1
				then
					local PosID =  RaidInstance:ContainerMapFind(GridPositionIndexMap, (i - 1) * L_MaxC_Count + j - 1);
					local x, y,z,o = RaidInstance:GetPosition(PosID);
					objID = RaidInstance:SpawnGameObject( L_DeadStateGameOBJ, x, y, z, o, 0, 25.0, 10, 0, 1 );  -- 为2 表示死亡陷阱
					RaidInstance:ModifyObjectScale( objID, 2.2 );
					RaidInstance:ContainerMapInsert(GridObjMap, (i - 1) * L_MaxC_Count + j - 1,  objID );
				end
			end
		end
	end	
-- FOR TEST

	local tx,ty,tz,to = RaidInstance:GetObjectPosition(L_TestCreate);
	
	if L_TestMoveEvt_F_Flag == 1
	then
			local lene = RaidInstance:CalculatePositionDistanceD(L_TestPosEnd,tx,ty,tz,to);
			if lene <= 0.3
			then
				L_TestMoveEvt_F_Flag = 0;
				L_TestMoveEvt_B_Flag = 1;
		
				local x, y,z,o = RaidInstance:GetPosition(L_TestPosStart);
				RaidInstance:CreatureMoveTo(L_TestCreate, x, y,z ,o, 0);
			end
	end
	
	if L_TestMoveEvt_B_Flag == 1
	then	
		local lens = RaidInstance:CalculatePositionDistanceD(L_TestPosStart,tx,ty,tz,to);
		if lens <= 0.3
		then
			L_TestMoveEvt_F_Flag = 1;
			L_TestMoveEvt_B_Flag = 0;
		
			local x, y,z,o = RaidInstance:GetPosition(L_TestPosEnd);
			RaidInstance:CreatureMoveTo(L_TestCreate, x, y,z ,o, 0);
		end		
	end
	
	local Count = RaidInstance:RandomShufflePlayers();
	for i = 1, Count, 1
	do
		local PlayerID = RaidInstance:GetNextRandomShufflePlayer();
		--RaidInstance:SendSystemNotify( "a1" );
		local lentoTest = RaidInstance:CalculateObjDistance(PlayerID , L_TestCreate);
		
		if lentoTest <= 2.0
		then
			--RaidInstance:SendSystemNotify( "111111" );
			local hasbuf = RaidInstance:HasBuff(PlayerID, L_MazeBuf);
			if hasbuf == 0
			then
				--RaidInstance:SendSystemNotify( "2222" );
				RaidInstance:AddBuffToPlayer( PlayerID, L_MazeBuf );
			end
		end
	end
	

	

	
end
function OnUnitDie( VictimID, AttackerID )
	local bEndGame = 1;
	local Count = RaidInstance:ContainerMapBegin(ReliveCountMap);
	for i = 1, Count, 1
	do
		local PlayerID, ReliveCount = RaidInstance:ContainerMapNext(ReliveCountMap);
		if PlayerID == VictimID
		then
			if ReliveCount < L_MaxReliveCount
			then
				bEndGame = 0;
				return ;
			end
		else
			if ReliveCount <= L_MaxReliveCount
			then
				bEndGame = 0;
				return ;
			end
		end
	end
	
	if bEndGame == 1
	then	
		L_EndGameEvt = RaidInstance:CreateEvent(5000, 1);
		RaidInstance:SendSystemNotify( "任务失败！5秒后关闭关副本！" );
	end
end

-- 获取状态
function GetState(PositionIndexR, PositionIndexC)
	local PositionState = 0 ;
	PositionState = RaidInstance:ContainerMapFind(GridStateMap, PositionIndexR * L_MaxC_Count + PositionIndexC);
	return PositionState ;
end
function  OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )
	if NPC_ID == ToMazeNPC
	then
		if GossipIndex == ToMazeIndex
		then
			local Relivecount = RaidInstance:ContainerMapFind(ReliveCountMap, PlayerID);
			if Relivecount >= (L_MaxReliveCount + 1)
			then
				--失败了4次。。不给传送啦 
				RaidInstance:CreatureSay( NPC_ID, "少侠内伤严重，还是先休息休息吧!", 1 );
				return ;
			else
					--传送起点
					local PositionID = RaidInstance:ContainerMapFind(GridPositionIndexMap, L_StartPositionIndexR * L_MaxC_Count + L_StartPositionIndexC);	
					local x, y,z,o = RaidInstance:GetPosition(PositionID);
					-- 重置位
					RaidInstance:TeleportPlayer(PlayerID, x, y,z,3.0);  -- 传送过去
					RaidInstance:AddBuffToPlayer( PlayerID, L_MazeBuf );
					
					return ;
			end
		end
	end
	

	if NPC_ID == OutMazeNPC
	then
		if GossipIndex == OutMazeIndex
		then
			-- 传送到下一关
		end
	end
end
function OnPlayerEnterInstance( PlayerID )
	RaidInstance:ContainerMapInsert(ReliveCountMap, PlayerID, 0);	
end

function OnPlayerLeaveInstance( PlayerID )
	RaidInstance:ContainerMapErase(ReliveCountMap, PlayerID);
	RaidInstance:RemoveBuffFromPlayer( PlayerID, L_MazeBuf );
end


function OnGameObjectTrigger( GameObjectID, PlayerID )

	if GameObjectID == L_EndGameOBJ
	then
		--到达终点
			RaidInstance:DespawnGameObject( L_EndGameOBJ );
			OutMazeNPC = RaidInstance:SpawnCreature( 3060, OutMazeNPCX, OutMazeNPCY, OutMazeNPCZ, OutMazeNPCO, 1 );
			OutMazeIndex = RaidInstance:InsertNPCGossip( OutMazeNPC, "完成卡洛的遗愿！" );
				
			RaidInstance:SendSystemNotify( "任务成功！" );
			
			local Count = RaidInstance:RandomShufflePlayers();
			for i = 1, Count, 1
			do
				local Target = RaidInstance:GetNextRandomShufflePlayer();
				RaidInstance:TeleportPlayer(Target, OutMazeNPCX , OutMazeNPCY ,OutMazeNPCZ,OutMazeNPCO);  -- 传送过去;
			end
			
			RaidInstance:EnableNPCGossip( OutMazeNPC, OutMazeIndex, 1 );
			
			L_EndGameOBJ = 0;
			
			return;
	end
end
function OnEvent( EventID, Attach2, Attach3, Attach4, Attach5 )
	if  EventID  == L_StartEvent
	then
	 	RaidInstance:SendSystemNotify( "欢迎来到死亡迷宫，小心遇到陷阱哦!" );
	end 
	
	
	if EventID == L_EndGameEvt
	then
		RaidInstance:ForceEjectAllPlayers();
	end
end
function OnPlayerResurrect( PlayerID )
	local RiliveCount = RaidInstance:ContainerMapFind(ReliveCountMap, PlayerID);
	RiliveCount = RiliveCount + 1;
	RaidInstance:ContainerMapInsert(ReliveCountMap, PlayerID, RiliveCount);
end