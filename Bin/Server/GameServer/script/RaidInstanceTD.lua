local RaidInstance;
local WaveCur = 0;
local WaveMax = 10;
local WaveEvent = 0;
local SpawnEvent = 0;
local PlayerMapId = 0;
local WaveMapId = 0;
local PlayerLevelMapId = 0;
local TowerID = 0;
local TriggerObjID = 0;
local StartEvent = 0;
local SpawnEventCount = 0;

local PlayerGost = 90103;
local PlayerHome = 90102;
local PlayerWarr = 90105;
local PlayerMagi = 90106;
local PlayerArch = 0;
local PlayerWarrUp1 = 0;
local PlayerWarrUp2 = 0;


local NormalAtt = 90103;
local PowerAtt = 90107;
local BombAtt = 90108;
local FrozeAtt = 90109;

local BossId10 = 0;
local BossId20 = 0;
local BossId30 = 0;


local playerrace = 0;

function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );
	
	PlayerMapId = RaidInstance:ContainerMapCreate();
	
	WaveMapId = RaidInstance:ContainerMapCreate();
	
	TowerID = RaidInstance:SpawnCreature( 3053, 1400.0, 1400.0, 0.0, 0.0, 0 );
	
	TriggerObjID = RaidInstance:SpawnGameObject( 10070, 1450.0, 1400.0, 0.0, 0.0, 0, 10.0, 10, 1, 1 );
end


function OnCreatureMoveArrival( CreatureID )
		local towerDamage = 5.0;
		if CreatureID == BossId10
		then
			towerDamage = 25.0;
		end
		
		if CreatureID == BossId20
		then
			towerDamage = 50.0;
		end
		
		if CreatureID == BossId30
		then
			towerDamage = 100.0;
		end
		
		RaidInstance:ModifyCreatureHPPct( TowerID, towerDamage );
		RaidInstance:DespawnCreature( CreatureID );
		RaidInstance:SendSystemNotify( "tower is danger" );
		RaidInstance:CreatureForcePlayAnimation( TowerID, 1 );
end

function OnGameObjectTrigger( GameObjectID, PlayerID )
	if GameObjectID == TriggerObjID
	then
		StartEvent = RaidInstance:CreateEvent( 5000, 1 );
		RaidInstance:SendSystemNotify( "35s spawn" );
	end
end


function OnPlayerEnterInstance( PlayerID )
	RaidInstance:ContainerMapInsert( PlayerMapId, PlayerID, 90103 );
	RaidInstance:ModifyPlayerMPPct( PlayerID, -100.0 );
	RaidInstance:LockManaRegeneration( PlayerID );	
	RaidInstance:AddBuffToPlayer( PlayerID, PlayerGost );
	--RaidInstance:ClearPlayerSpellCastLimit( PlayerID );
	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 4 );
	if playerrace == 0
	then
		local race = RaidInstance:GetPlayerRace( PlayerID );
		RaidInstance:ModifyObjectFaction( TriggerObjLose, race );
	end
end


function OnPlayerLeaveInstance( PlayerID )
	RaidInstance:ContainerMapErase( PlayerMapId, PlayerID );
	RaidInstance:ModifyPlayerMPPct( PlayerID, 100.0 );
	RaidInstance:UnlockManaRegeneration( PlayerID );
	RaidInstance:ClearPlayerSpellCastLimit( PlayerID );
	--RaidInstance:RemoveBuffFromPlayer( PlayerID,  );
end


function OnUnitDie( VictimID, AttackerID )
	if(VictimID == TowerID ) 
	then
	   	RaidInstance:KillEvent(WaveEvent);
	   	RaidInstance:KillEvent(SpawnEvent);
	else
		local Guid = RaidInstance:ContainerMapFind( WaveMapId, VictimID );
		if Guid > 0
		then
			 RaidInstance:ContainerMapErase( WaveMapId,VictimID );
			 local Count = RaidInstance:ContainerMapBegin( PlayerMapId );
			 for i = 1, Count, 1
			 do
			 	local Guid, entry = RaidInstance:ContainerMapNext( PlayerMapId );
			 	RaidInstance:AddUnitMana( Guid, WaveCur*2 );
			 end
		end
	end
end


function OnEvent( EventID, Attach, Attach2, Attach3, Attach4, Attach5 )

	if EventID == StartEvent
	then
		RaidInstance:KillEvent(StartEvent);
		WaveEvent = RaidInstance:CreateEvent( 10000, 30 );
	end
	
	if EventID == WaveEvent
	then
		SpawnEvent = RaidInstance:CreateEvent( 3000, 3 );
		SpawnEventCount = 0;
		WaveCur = WaveCur + 1;
		local strWaveCount = RaidInstance:ConvertInteger2String( WaveCur );
		RaidInstance:SendSystemNotify( strWaveCount );
	end
	
	if EventID == SpawnEvent
	then
		if WaveCur == 10
		then
			local BossId10 = RaidInstance:SpawnCreature( 3056, 1450.0, 1400.0, 0.0, 0.0, 0 );
			RaidInstance:ModifyThreat( BossId10, TowerID, 10000000 );
			RaidInstance:CreatureMoveTo( BossId10, 1400.0, 1400.0, 0.0, 0.0, 0 );
			SpawnEventCount = 3;
		end
		
		if WaveCur == 20
		then
			local BossId20 = RaidInstance:SpawnCreature( 3056, 1450.0, 1400.0, 0.0, 0.0, 0 );
			RaidInstance:ModifyThreat( BossId20, TowerID, 10000000 );
			RaidInstance:CreatureMoveTo( BossId20, 1400.0, 1400.0, 0.0, 0.0, 0 );
			SpawnEventCount = 3;
		end
		
		if WaveCur == 30
		then
			local BossId30 = RaidInstance:SpawnCreature( 3056, 1450.0, 1400.0, 0.0, 0.0, 0 );
			RaidInstance:ModifyThreat( BossId30, TowerID, 10000000 );
			RaidInstance:CreatureMoveTo( BossId30, 1400.0, 1400.0, 0.0, 0.0, 0 );
			SpawnEventCount = 3;
		end
		
	    SpawnEventCount = SpawnEventCount + 1;

		local strSpawnCount = RaidInstance:ConvertInteger2String( SpawnEventCount );
		
		RaidInstance:SendSystemNotify( strSpawnCount );
		if SpawnEventCount < 3
		then 
			local LineMob = RaidInstance:SpawnCreature( 3057, 1450.0, 1400.0, 0.0, 0.0, 0 );
			RaidInstance:ModifyThreat( LineMob, TowerID, 10000000 );
			RaidInstance:CreatureMoveTo( LineMob, 1400.0, 1400.0, 0.0, 0.0, 0 );
			RaidInstance:ContainerMapInsert( WaveMapId, LineMob, LineMob );
		
			LineMob = RaidInstance:SpawnCreature( 3057, 1450.0, 1420.0, 0.0, 0.0, 0 );
			RaidInstance:ModifyThreat( LineMob, TowerID, 10000000 );
			--RaidInstance:SwitchCreatureAIMode(LineMob );
			RaidInstance:CreatureMoveTo( LineMob, 1400.0, 1400.0, 0.0, 0.0, 0 );
			RaidInstance:ContainerMapInsert( WaveMapId, LineMob, LineMob );
		
			LineMob = RaidInstance:SpawnCreature( 3057, 1450.0, 1440.0, 0.0, 0.0, 0 );
			RaidInstance:ModifyThreat( LineMob, TowerID, 10000000 );
			--RaidInstance:SwitchCreatureAIMode(LineMob );
			RaidInstance:CreatureMoveTo( LineMob, 1400.0, 1400.0, 0.0, 0.0, 0 );
			RaidInstance:ContainerMapInsert( WaveMapId, LineMob, LineMob );
		
			LineMob = RaidInstance:SpawnCreature( 3057, 1450.0, 1460.0, 0.0, 0.0, 0 );
			RaidInstance:ModifyThreat( LineMob, TowerID, 10000000 );
			--RaidInstance:SwitchCreatureAIMode(LineMob );
			RaidInstance:CreatureMoveTo( LineMob, 1400.0, 1400.0, 0.0, 0.0, 0 );
			RaidInstance:ContainerMapInsert( WaveMapId, LineMob, LineMob );
		end
		
		if SpawnEventCount == 3
		then
			RaidInstance:SendSystemNotify( "next spawn" );
		end
	end
end


function OnUnitCastSpell( SpellID, UnitID )
	if SpellID == PlayerGost
	then
		RaidInstance:ForceUnrootPlayer( UnitID );
		RaidInstance:AddPlayerSpellCastLimit( PlayerID, 4 );
		RaidInstance:AddPlayerSpellCastLimit( UnitID, PlayerHome );
	end
	
	if SpellID == PlayerHome
	then
		RaidInstance:AddPlayerSpellCastLimit( PlayerID, 4 );
		RaidInstance:AddPlayerSpellCastLimit( UnitID, PlayerGost );
		RaidInstance:AddPlayerSpellCastLimit( UnitID, PlayerWarr );
		RaidInstance:AddPlayerSpellCastLimit( UnitID, PlayerMagi );
		RaidInstance:AddPlayerSpellCastLimit( UnitID, NormalAtt );
		
		RaidInstance:ForceRootPlayer( UnitID );
	end
	
	if SpellID == PlayerWarr
	then
		RaidInstance:ForceRootPlayer( UnitID );
		RaidInstance:AddPlayerSpellCastLimit( PlayerID, 4 );
		RaidInstance:AddPlayerSpellCastLimit( UnitID, PlayerGost );
		RaidInstance:AddPlayerSpellCastLimit( UnitID, PowerAtt );
		RaidInstance:AddPlayerSpellCastLimit( UnitID, BombAtt );
	end
	
	if SpellID == PlayerMagi
	then
		RaidInstance:ForceRootPlayer( UnitID );
		RaidInstance:AddPlayerSpellCastLimit( PlayerID, 4 );
		RaidInstance:AddPlayerSpellCastLimit( UnitID, PlayerGost );
		RaidInstance:AddPlayerSpellCastLimit( UnitID, PowerAtt );
		RaidInstance:AddPlayerSpellCastLimit( UnitID, FrozeAtt );
	end
	
end