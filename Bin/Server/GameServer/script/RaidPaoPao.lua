--Author wf
--3/28/2011
--
local RaidInstance;
-- 个人赛和团队赛的状态
local G_Match_State = {}; -- 个人赛模式（1）... 组队赛模式（0）

--报名NPC信息
local G_ApplyNPC = 0;
local G_ApplyGossip_Table ={};
local G_ApplyBuf_Table = {};
local G_ApplyKaixinCount = 100;
-- 报名NPC位置
local G_OutX = 1899.9;
local G_OutY = 1103.5;
local G_OutZ = 23.0;
local G_OutO = 3.3;
--下注NPC信息
local G_BetObj = 0;
local G_AddKaiXinCount = 100;
local G_BetGossipIndex_Table = {};
local G_TeamBet_Table = {};
local G_BetScale = 0.9; -- 奖励返还系数
-- 团队赛NPC相关
local G_Team_Table_A = {}; -- A组NPC信息
local G_Team_Table_B = {}; -- B组PNC信息
local G_Team_Buf_Table = {}; --BUF信息
local G_ToPaoPao_Pos_Table = {}; -- 传送的位置信息

-- 奖励物品ID
local G_RewardItemID = 75;
local G_BetRewardItemID = 76;
-- 团队赛成员信息
local G_ApplyTeamMember_Table = {};
-- 惩罚BUF
local G_TeamMemberLeaveBuf = 333;

--团队赛控制事件
local G_AppearHomeEvt = 0;
local G_ChooseAnimalEndEvt = 0;
local G_GameContrl = {G_NoticeGameStartEvt = 0, G_NoticeIndex = 0, G_EndGameEvt = 0, G_GameReStart = 0, IsReadyToStart = 0,G_OutLeiTaiTime = 10.0};

-- 团队赛相关
local G_TeamMaxCount = 1;
local G_TeamAFull = 0;
local G_TeamBFull = 0;
local G_TeamWinner = 0;
local G_WinKaiXin = 0.9; --参赛队员奖励返还系数

-- 系统通知报名信息
local G_SysMsgTimeSave = {};
-- center postion
local G_CenterX = 1888.2;
local G_CenterY = 1027.4;
local G_CenterZ = 25;
local G_CenterO = 3.3;
local G_Radius = 35 ;

--擂台突发事件相关数据 { 突发事件信息， 当前突发事件ID， 释放技能事件， 结束突发事件， 是否已经发生突发事件 游戏开始事件， 等待多长时间发生突发事件}
local G_EmergencyMgr = { G_EmergencyList_Table = {} , G_CurEmergency = 0, G_EmergencySpell_Evt = 0, G_EndEmergency_Evt = 0, G_EmergencyIsUse = 0, G_EmergencyMatch_Time = 0 , G_EmergencyWTime = 0,
	-- 初始化
	F_CreateEmergencyMgr = function(self)
	--催眠 恐惧 冰冻 火爆
		self.G_EmergencyList_Table[1] = {entry = 3087, guid = 0, spell = 361, spellWaittime = 20000, SpellCount = 3, UseSpellCount = 0, HitNotice = "擂台赛进行的如火如荼的时候，突然出现了不明生物！"};
		self.G_EmergencyList_Table[2] = {entry = 3087, guid = 0, spell = 362, spellWaittime = 20000, SpellCount = 3, UseSpellCount = 0, HitNotice = "擂台赛进行的如火如荼的时候，突然出现了不明生物！"};
		self.G_EmergencyList_Table[3] = {entry = 3087, guid = 0, spell = 363, spellWaittime = 20000, SpellCount = 2, UseSpellCount = 0, HitNotice = "擂台赛进行的如火如荼的时候，突然出现了不明生物！"};
		self.G_EmergencyList_Table[4] = {entry = 3087, guid = 0, spell = 364, spellWaittime = 20000, SpellCount = 4, UseSpellCount = 0, HitNotice = "擂台赛进行的如火如荼的时候，突然出现了不明生物！"};
	end,
	-- 游戏开始事件设置
	F_BeginEmergency = function(self, Time)
		self.G_EmergencyMatch_Time =  Time + 10 ;
		self.G_EmergencyWTime = LuaRandom(60, 190);
	end,
	-- 增加突发事件
	F_AddEmergency = function(self, Time)
		if self.G_EmergencyIsUse == 0 and self.G_CurEmergency == 0 and Time - self.G_EmergencyMatch_Time >= self.G_EmergencyWTime
		then
			index = LuaRandom(1, 4);
			if self.G_CurEmergency == 0 and self.G_EmergencyList_Table[index] ~= nil
			then
				if self.G_EmergencyList_Table[index].guid == 0
				then
					self.G_EmergencyList_Table[index].guid = RaidInstance:SpawnCreature( self.G_EmergencyList_Table[index].entry, G_CenterX, G_CenterY, G_CenterZ, G_CenterO, 0 );
					RaidInstance:ModifyObjectScale( self.G_EmergencyList_Table[index].guid, 6.0 );
					RaidInstance:SwitchCreatureAIMode( self.G_EmergencyList_Table[index].guid );
					RaidInstance:ModifyObjectFaction(self.G_EmergencyList_Table[index].guid,1000);
					RaidInstance:SendSystemNotify( self.G_EmergencyList_Table[index].HitNotice );
				end

				self.G_EmergencySpell_Evt = RaidInstance:CreateEvent(self.G_EmergencyList_Table[index].spellWaittime,  self.G_EmergencyList_Table[index].SpellCount);
				self.G_CurEmergency = index ;
			end
			self.G_EmergencyIsUse = 1;
			self.G_EmergencyMatch_Time = 0;
		end
	end,
	-- 使用技能
	F_EmergencyUseSpell =  function(self, EventID)
		if EventID ~= self.G_EmergencySpell_Evt
		then
			return ;
		end
		if self.G_EmergencyList_Table[self.G_CurEmergency] ~= nil and self.G_EmergencyList_Table[self.G_CurEmergency].guid ~= 0
		then
			RaidInstance:CreatureCastSpell(self.G_EmergencyList_Table[self.G_CurEmergency].guid, self.G_EmergencyList_Table[self.G_CurEmergency].spell, 3, self.G_EmergencyList_Table[self.G_CurEmergency].guid, 0);
			RaidInstance:SendSystemNotify("不明生物对擂台的参赛队员进行了惨绝人寰的打击！");
			self.G_EmergencyList_Table[self.G_CurEmergency].UseSpellCount = self.G_EmergencyList_Table[self.G_CurEmergency].UseSpellCount + 1;

			if self.G_EmergencyList_Table[self.G_CurEmergency].UseSpellCount >= self.G_EmergencyList_Table[self.G_CurEmergency].SpellCount
			then
				self.G_EndEmergency_Evt = RaidInstance:CreateEvent(20000, 1);
				self.G_EmergencySpell_Evt = 0;
			end
		end
	end,

	--结束突发事件
	F_EndEmergency = function(self, EventID)
		if EventID == 0 or EventID ==  self.G_EndEmergency_Evt
		then
			if self.G_CurEmergency ~= 0 and self.G_EmergencyList_Table[self.G_CurEmergency] ~= nil
			then
				if self.G_EmergencyList_Table[self.G_CurEmergency].guid ~= 0
				then
					RaidInstance:DespawnCreature(self.G_EmergencyList_Table[self.G_CurEmergency].guid);
					RaidInstance:SendSystemNotify("不明生物消失啦！");
				end
				self.G_EmergencyList_Table[self.G_CurEmergency].guid = 0;
				self.G_EmergencyList_Table[self.G_CurEmergency].UseSpellCount = 0;
			end
			self.G_CurEmergency = 0;
			self.G_EndEmergency_Evt = 0;
		end
	end,

	--重置突发事件状态
	F_ResetEmergency = function(self)
		self.G_EmergencyIsUse = 0;
		self.G_EmergencySpell_Evt = 0;
		self:F_EndEmergency(0);
	end
};

-- 蛋管理 {蛋的信息相关信息， 蛋的guid和状态， 每次刷出蛋的数量列表， 当前刷出的Index, 蛋的最大数量, 刷蛋}
local G_Egg_Mgr = { EggEntry_Table = {}, Egg_Table = {}, Egg_Create_Count = {}, CreateIndex = 1, G_Egg_Max = 120,  G_AddEgg_Evt = 0,

	-- 初始化数据
		F_CreateEggMgr = function(self)

			-- the egg's entry . and be bomo radius
			self.EggEntry_Table[1] = {entry = 11101, r = 2.0};
			self.EggEntry_Table[2] = {entry = 11102, r = 2.0};
			self.EggEntry_Table[3] = {entry = 11103, r = 2.0};
			self.EggEntry_Table[4] = {entry = 11104, r = 2.0};
			self.EggEntry_Table[5] = {entry = 11101, r = 2.0};
			--self.EggEntry_Table[5] = {entry = 11105, r = 2.0};

			for i = 1, self.G_Egg_Max, 1
			do
				self.Egg_Table[i]  = {guid = 0, state = 0};
			end

			for i = 1, 16, 1
			do
				self.Egg_Create_Count[i] = i ;
			end

		end,

	--增加刷蛋的事件
		F_CreateAddEvt = function(self)
			self.G_AddEgg_Evt = RaidInstance:CreateEvent(20000,  16);
		end,
	-- 刷蛋到场景
		F_AddEgg = function(self, index)
			if self.Egg_Table[index] ~= nil and self.Egg_Table[index].guid == 0
			then

				local x = G_CenterX + LuaRandom(0,  2 * G_Radius) - G_Radius;
				local xLen = (G_CenterX - x ) * (G_CenterX - x ) ;
				local YL = math.sqrt(G_Radius * G_Radius - xLen);
				local y = LuaRandom(G_CenterY - YL + 1, G_CenterY + YL - 1);
				local entry_i = LuaRandom(1, 5);

				self.Egg_Table[index].guid = RaidInstance:SpawnGameObject( self.EggEntry_Table[entry_i].entry, x, y, 25.9, 0.0, 0, self.EggEntry_Table[entry_i].r, 10, 1, 1 );
				self.Egg_Table[index].state = 1;
				RaidInstance:ModifyObjectScale( self.Egg_Table[index].guid, 3.2 );
			end
		end,
	-- 结束蛋事件（重置）
		F_Deswap = function(self)
			for i = 1, self.G_Egg_Max, 1
			do
				if self.Egg_Table[i] ~= nil and self.Egg_Table[i].guid ~= 0
				then
					RaidInstance:DespawnGameObject(self.Egg_Table[i].guid );
					self.Egg_Table[i].guid = 0;
					self.Egg_Table[i].state = 0;
				end
			end

			self.CreateIndex = 1;
			RaidInstance:KillEvent(self.G_AddEgg_Evt);
			self.G_AddEgg_Evt = 0;
		end,

	-- 刷蛋事件
		F_SetCreateEgg = function(self, EventID)
			if self.Egg_Create_Count[self.CreateIndex] ~= nil and  self.G_AddEgg_Evt == EventID
			then
				RaidInstance:SendSystemNotify("擂台上出现了一堆堆奇怪的蛋蛋！~");
				local LastIndex = 0;
				for j = 1, self.CreateIndex, 1
				do
					LastIndex = LastIndex + self.Egg_Create_Count[j];
				end

				for i = 1, self.Egg_Create_Count[self.CreateIndex], 1
				do
					self:F_AddEgg( LastIndex + i);
				end
				self.CreateIndex = self.CreateIndex + 1;
			end
		end
	};

-- 个人擂台赛管理
local G_Single_Match = {
 G_Single_Match_Table = {}, --个人赛玩家列表
 G_Join_Single_Match_Kaixin = 100, -- 报名费
 G_Single_Match_Player_In_LeiTai = 0, --当前擂台剩余人数
 G_Single_Match_Winner_Count = 1, --赢家的数量
 G_Active_Single_Player_Count = 0, -- 当前报名数量
 G_Single_Match_Reward_F = 0.9,
 G_Single_Match_Buf = {}, --个人赛BUF

 G_Single_Match_WinTable = {},
 G_Single_Will_BeginEvt = 0;

 F_CreateSingleMatch = function(self)
	self.G_Single_Match_Buf[1] = 354; -- PK 变身
	self.G_Single_Match_Buf[2] = 360; -- 报名BUF

	self.G_Single_Match_WinTable[1] = {Max = 3,  Win = 1};
	self.G_Single_Match_WinTable[2] = {Max = 7,  Win = 2};
	self.G_Single_Match_WinTable[3] = {Max = 15, Win = 3};
	self.G_Single_Match_WinTable[4] = {Max = 18, Win = 4};
 end,
 F_SingleMatch_WillBegin = function(self, evt)

	if evt ~= 0 and evt == self.G_Single_Will_BeginEvt
	then
		G_Match_State[1] = 1;
		RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[4], 0 );
		self.G_Single_Will_BeginEvt = 0;

		for i = 1, 4, 1
		do
			if self.G_Single_Match_WinTable[i] ~= nil and self.G_Single_Match_WinTable[i].Max <= self.G_Active_Single_Player_Count
			then
				self.G_Single_Match_Winner_Count = self.G_Single_Match_WinTable[i].Win;
			end
		end

		self.G_Single_Match_Player_In_LeiTai = self.G_Active_Single_Player_Count
	end
 end,
 --报名
 F_PlayerApplySingleMatch = function(self, PlayerID)
	if self.G_Single_Match_Table[PlayerID] == nil
	then
		-- 判断是否有钱
		local HasKaixin =  RaidInstance:ConsumeCurrency( PlayerID, 0, 56, self.G_Join_Single_Match_Kaixin );
		if HasKaixin == 0
		then
			RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "你没有足够的开心币!" );
			return ;
		end
		self.G_Single_Match_Table[PlayerID] = {state = 0, oldbuf = 0, leavetime = 0.0, NumLeaveTime = 0.0};
		self.G_Active_Single_Player_Count = self.G_Active_Single_Player_Count + 1;
		RaidInstance:ModifyObjectVariable( PlayerID, 0, 1 );
		RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "报名擂台个人赛成功!" );
		RaidInstance:AddBuffToPlayer(PlayerID, self.G_Single_Match_Buf[2]);

		if self.G_Active_Single_Player_Count >= self.G_Single_Match_WinTable[1].Max and self.G_Single_Will_BeginEvt == 0
		then
			self.G_Single_Will_BeginEvt = RaidInstance:CreateEvent(60000, 1);
			RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)60秒后结束报名!需要参加的玩家请赶快报名！已报名的玩家请做好准备！" );
		end
	end
 end,
 -- 取消报名
  F_PlayerCancelSingleMatch = function(self, PlayerID)
	if self.G_Single_Match_Table[PlayerID] ~= nil
	then
		if self.G_Single_Will_BeginEvt == 0
		then
			RaidInstance:ModifyObjectVariable( PlayerID, 1, 1 );
			RaidInstance:ItemReward( PlayerID, 56, self.G_Join_Single_Match_Kaixin * 0.8 );
			RaidInstance:ModifyObjectVariable( PlayerID, 1, 0 );
			RaidInstance:RemoveBuffFromPlayer(PlayerID, self.G_Single_Match_Buf[2]);
			RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[4], 1 );
			RaidInstance:ClearObjectVariable(PlayerID);
			RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "您取消参加个人擂台赛！" );
			RaidInstance:SendSystemNotify( "由于有玩家取消参加个人擂台赛，现在接受报名!" );
			self.G_Active_Single_Player_Count = self.G_Active_Single_Player_Count - 1;
			G_Match_State[1] = 0;
		end
		self.G_Single_Match_Table[PlayerID] = nil;
	end
end,

--发放奖励
  F_GiveSingleMatchReward = function(self)
		for k, v in pairs(self.G_Single_Match_Table)
		do
			if v ~= nil
			then
				local t_PlayerName = RaidInstance:GetPlayerName(k);
				local t_RewardKaixin = self.G_Join_Single_Match_Kaixin  * self.G_Active_Single_Player_Count * self.G_Single_Match_Reward_F / self.G_Single_Match_Winner_Count ;
				RaidInstance:SendSystemNotify("玩家["..t_PlayerName.."]在擂台赛(个人赛)表现突出，获得"..RaidInstance:ConvertInteger2String(t_RewardKaixin).."开心币");
				RaidInstance:ItemReward(k, 56, t_RewardKaixin );
				RaidInstance:ItemReward(k, G_RewardItemID, 1);
			end
		end
  end,

  -- 重置个人赛
  F_ResetSingleMatch = function(self)
	for k, v in pairs( self.G_Single_Match_Table)
		do
			RaidInstance:RemoveBuffFromPlayer(k, self.G_Single_Match_Buf[1]);
			RaidInstance:DismissMountAndShift(k);
			if v.buf ~= 0
			then
				RaidInstance:AddBuffToPlayer(k, v.buf);
				if v.buf == 326 or v.buf == 327
				then
					RaidInstance:ModifyPlayerPVPFlag(k, 2);
				else
					RaidInstance:ModifyPlayerPVPFlag(k, 1);
				end
			end
			if v.state ~= 4
			then
				RaidInstance:TeleportPlayer(k, G_ToPaoPao_Pos_Table[3].homex, G_ToPaoPao_Pos_Table[3].homey,G_ToPaoPao_Pos_Table[3].homez, G_ToPaoPao_Pos_Table[3].homeo );
			end
			RaidInstance:ClearObjectVariable(k);
			self.G_Single_Match_Table[k] = nil;
		end

		self.G_Active_Single_Player_Count = 0;
		self.G_Single_Match_Player_In_LeiTai = 0;
		self.G_Single_Match_Winner_Count = 1;
		self.G_Single_Will_BeginEvt = 0;
  end,

  --检测个人赛状态
  F_CheckSingle = function(self, Time)
		for k, v in pairs(self.G_Single_Match_Table)
		do
			if v ~= nil and (v.state == 2 or v.state == 3)
			then
				self:F_CheckSingleState(k, Time);
			end
		end
  end,
  --检查玩家状态
  F_CheckSingleState = function(self, PlayerID, Time)
		if self.G_Single_Match_Table[PlayerID] == nil
		then
			return ;
		end
		local x,y,z,o = RaidInstance:GetObjectPosition( PlayerID)
		local t_Dis = (G_CenterX - x) *(G_CenterX - x) + (G_CenterY - y) * (G_CenterY - y) + (G_CenterZ - z) * (G_CenterZ - z);
		if t_Dis > (G_Radius * G_Radius)
		then
			if self.G_Single_Match_Table[PlayerID].state == 2
			then
				self.G_Single_Match_Table[PlayerID].state = 3;
				self.G_Single_Match_Table[PlayerID].leavetime = Time;
			else
			-- 单次离开擂台 G_OutLeiTaiTime 时间出局
				if Time - self.G_Single_Match_Table[PlayerID].leavetime >= G_GameContrl.G_OutLeiTaiTime
				then
					self:F_OutSinglePlayer(PlayerID, 0);
					return;
				end
			-- 离开擂台总时间超过 G_OutLeiTaiTime * 4.0 出局
				if self.G_Single_Match_Table[PlayerID].NumLeaveTime >= G_GameContrl.G_OutLeiTaiTime * 4.0
				then
					self:F_OutSinglePlayer(PlayerID, 1);
					return;
				end
			end
		else
			if self.G_Single_Match_Table[PlayerID].state == 3
			then
				self.G_Single_Match_Table[PlayerID].NumLeaveTime = self.G_Single_Match_Table[PlayerID].NumLeaveTime + Time  - self.G_Single_Match_Table[PlayerID].leavetime
			-- 发送离开擂台时间叠加消息
				RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "你离开擂台时间总计"..RaidInstance:ConvertInteger2String(self.G_Single_Match_Table[PlayerID].NumLeaveTime).."秒!" );
			end

			self.G_Single_Match_Table[PlayerID].state = 2;
			self.G_Single_Match_Table[PlayerID].leavetime = 0;
		end
  end,

  -- 玩家出局
  F_OutSinglePlayer = function(self, PlayerID, isNumTimeOut)
	if self.G_Single_Match_Table[PlayerID] == nil
	then
		return ;
	end
	RaidInstance:RemoveBuffFromPlayer(PlayerID, self.G_Single_Match_Buf[1]);
	RaidInstance:DismissMountAndShift(PlayerID);
	if self.G_Single_Match_Table[PlayerID].buf ~= 0
	then
		RaidInstance:AddBuffToPlayer(PlayerID, self.G_Single_Match_Table[PlayerID].buf);
		if self.G_Single_Match_Table[PlayerID].buf == 326 or self.G_Single_Match_Table[PlayerID].buf == 327
		then
			RaidInstance:ModifyPlayerPVPFlag(PlayerID, 2);
		else
			RaidInstance:ModifyPlayerPVPFlag(PlayerID, 1);
		end
	end
	RaidInstance:ClearObjectVariable(PlayerID);
	RaidInstance:TeleportPlayer(PlayerID, G_ToPaoPao_Pos_Table[3].homex,  G_ToPaoPao_Pos_Table[3].homey, G_ToPaoPao_Pos_Table[3].homez, G_ToPaoPao_Pos_Table[3].homeo );
	RaidInstance:ForceUnrootPlayer(PlayerID);
	if isNumTimeOut == 1
	then
		RaidInstance:SendSystemNotify(RaidInstance:GetPlayerName(PlayerID).."离开擂台时间总计超过40秒，被击退出角斗场！" );
		RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "你离开擂台时间总计超过40秒，被击退出角斗场!" );
	else
		RaidInstance:SendSystemNotify(RaidInstance:GetPlayerName(PlayerID).."被击退出角斗场！" );
		RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "你被击退出角斗场!" );
	end

	self.G_Single_Match_Table[PlayerID] = nil;
	self.G_Single_Match_Player_In_LeiTai = self.G_Single_Match_Player_In_LeiTai - 1;

	if self.G_Single_Match_Player_In_LeiTai <= self.G_Single_Match_Winner_Count
	then
		RaidInstance:SendSystemNotify("本轮个人擂台赛结束");
		G_Match_State[1] = 4;
		G_GameContrl.G_EndGameEvt = RaidInstance:CreateEvent(10, 1);
	end
  end,
-- 准备开启擂台混战模式
  F_BeginSingleMatch = function(self)

		G_Match_State[1] = 2;
		self.G_Single_Match_Player_In_LeiTai = self.G_Active_Single_Player_Count;

		for k, v in pairs(self.G_Single_Match_Table)
		do
			if v ~= nil
			then
				RaidInstance:ModifyPlayerPVPFlag(k, 2);
			end
		end
		RaidInstance:SendSystemNotify( "擂台(个人赛)即将开启！" );
		G_GameContrl.IsReadyToStart = 1;
		RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[5], 1 );
		RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[4], 1 );
  end,
  -- 传送至擂台
  F_TransToLeiTai = function(self)
		for k, v in pairs(self.G_Single_Match_Table)
		do
			if v ~= nil
			then
				self.G_Single_Match_Table[k].buf = RaidInstance:GetPlayerShapeShiftSpellEntry(k);
				self.G_Single_Match_Table[k].state = 1;
				RaidInstance:RemoveBuffFromPlayer(k, self.G_Single_Match_Buf[2]);
				RaidInstance:DismissMountAndShift(k);
				RaidInstance:AddBuffToPlayer(k, self.G_Single_Match_Buf[1]);
				RaidInstance:TeleportPlayer(k, G_CenterX + LuaRandom(1, 8) * 0.5, G_CenterY + LuaRandom(1, 8) * 0.5, G_CenterZ, G_CenterO );
			end
		end
  end
};

--增加小动物NPC
function F_AddAnimalNpc(Teamindex, info_id)
	if Teamindex == 0
	then
		G_Team_Table_A[info_id].guid =	RaidInstance:SpawnCreature( G_Team_Table_A[info_id].entry, G_Team_Table_A[info_id].x, G_Team_Table_A[info_id].y,  G_Team_Table_A[info_id].z,  G_Team_Table_A[info_id].o, 0 );
		RaidInstance:ModifyObjectFaction(G_Team_Table_A[info_id].guid ,10000);

		G_Team_Table_A[info_id].Gossip = RaidInstance:InsertNPCGossip( G_Team_Table_A[info_id].guid, "选择".. G_Team_Buf_Table[info_id].name.."!" );
		RaidInstance:EnableNPCGossip( G_Team_Table_A[info_id].guid, G_Team_Table_A[info_id].Gossip, 0 );
	else
		G_Team_Table_B[info_id].guid =	RaidInstance:SpawnCreature( G_Team_Table_B[info_id].entry, G_Team_Table_B[info_id].x, G_Team_Table_B[info_id].y,  G_Team_Table_B[info_id].z,  G_Team_Table_B[info_id].o, 0 );
		RaidInstance:ModifyObjectFaction(G_Team_Table_B[info_id].guid ,10000);

		G_Team_Table_B[info_id].Gossip = RaidInstance:InsertNPCGossip( G_Team_Table_B[info_id].guid, "选择"..G_Team_Buf_Table[info_id].name.."!" );
		RaidInstance:EnableNPCGossip( G_Team_Table_B[info_id].guid, G_Team_Table_B[info_id].Gossip, 0 );
	end
end
--初始化副本数据
function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );
	-- 报名buf
	G_Team_Buf_Table[1] = {buf = 325, name = "乌龟" };
	G_Team_Buf_Table[2] = {buf = 322, name = "小鸡" };
	G_Team_Buf_Table[3] = {buf = 321, name = "企鹅" };
	G_Team_Buf_Table[4] = {buf = 324, name = "熊猫" };
	G_Team_Buf_Table[5] = {buf = 323, name = "雪人" };

	-- 开始前传送擂台位置
	local leitai_Table_A = {};
	local leitai_Table_B = {};
	-- 小队成员擂台初始位置
	-- Team A
	leitai_Table_A[1] = {x = 1884.7, y = 1004.4, z = 25.0, o = 3.3};
	leitai_Table_A[2] = {x = 1885.6, y = 1010.2, z = 25.0, o = 3.3};
	leitai_Table_A[3] = {x = 1883.9, y = 999.3,  z = 25.0, o = 3.3};
	leitai_Table_A[4] = {x = 1890.2, y = 1003.9, z = 25.0, o = 3.3};
	leitai_Table_A[5] = {x = 1879.0, y = 1005.5, z = 25.0, o = 3.3};
	--Team B
	leitai_Table_B[1] = {x = 1891.7, y = 1050.7, z = 25.0, o = 0.1};
	leitai_Table_B[2] = {x = 1890.7, y = 1044.7, z = 25.0, o = 0.1};
	leitai_Table_B[3] = {x = 1892.4, y = 1055.6, z = 25.0, o = 0.1};
	leitai_Table_B[4] = {x = 1884.9, y = 1051.7, z = 25.0, o = 0.1};
	leitai_Table_B[5] = {x = 1898.3, y = 1049.7, z = 25.0, o = 0.1};

	--擂台传送相关
	G_ToPaoPao_Pos_Table[1] = {homex = 1881.3, homey = 1115.7, homez = 20.0, homeo = 4.5, leitai = leitai_Table_A}; -- A 休息室
	G_ToPaoPao_Pos_Table[2] = {homex = 1924.4, homey = 1108.1, homez = 20.0, homeo = 2.5, leitai = leitai_Table_B}; -- B 休息室
	G_ToPaoPao_Pos_Table[3] = {homex = 1879.1, homey = 959.9,  homez = 41.1, homeo = 3.2, leitai = {}}; -- 传出擂台位置


	--擂台战斗角色NPC信息
	G_Team_Table_A[1] = {entry = 607000112, x = 1884.9, y = 1116.0, z = 20.0, o = 4.6, Gossip = 0, guid = 0};
	G_Team_Table_A[2] = {entry = 607000111, x = 1884.8, y = 1120.2, z = 20.0, o = 4.6, Gossip = 0, guid = 0};
	G_Team_Table_A[3] = {entry = 607000113, x = 1885.0, y = 1111.2, z = 20.0, o = 4.6, Gossip = 0, guid = 0};
	G_Team_Table_A[4] = {entry = 607000114, x = 1880.7, y = 1120.7, z = 20.0, o = 3.2, Gossip = 0, guid = 0};
	G_Team_Table_A[5] = {entry = 607000115, x = 1875.4, y = 1120.0, z = 20.0, o = 3.1, Gossip = 0, guid = 0};

	G_Team_Table_B[1] = {entry = 607000117, x = 1920.2, y = 1109.0, z = 20.0, o = 1.7, Gossip = 0, guid = 0};
	G_Team_Table_B[2] = {entry = 607000116, x = 1921.4, y = 1112.6, z = 20.0, o = 2.5, Gossip = 0, guid = 0};
	G_Team_Table_B[3] = {entry = 607000118, x = 1920.0, y = 1105.0, z = 20.0, o = 1.7, Gossip = 0, guid = 0};
	G_Team_Table_B[4] = {entry = 607000119, x = 1925.2, y = 1112.7, z = 20.0, o = 3.2, Gossip = 0, guid = 0};
	G_Team_Table_B[5] = {entry = 607000120, x = 1928.8, y = 1111.2, z = 20.0, o = 3.4, Gossip = 0, guid = 0};

	--创建npc
	-- team a
	F_AddAnimalNpc(0, 1);
	F_AddAnimalNpc(0, 2);
	F_AddAnimalNpc(0, 3);
	F_AddAnimalNpc(0, 4);
	F_AddAnimalNpc(0, 5);

	--team b
	F_AddAnimalNpc(1, 1);
	F_AddAnimalNpc(1, 2);
	F_AddAnimalNpc(1, 3);
	F_AddAnimalNpc(1, 4);
	F_AddAnimalNpc(1, 5);

	-- state ={0 , 1, 2, 3, 4}  等待, 倒计时, 战斗, 被打出擂台, 出局
	-- 参赛队员信息
	local TeamA = {};
	TeamA[1] = {pid = 0, state = 0, oldbuf = 0, leavetime = 0.0};
	TeamA[2] = {pid = 0, state = 0, oldbuf = 0, leavetime = 0.0};
	TeamA[3] = {pid = 0, state = 0, oldbuf = 0, leavetime = 0.0};
	TeamA[4] = {pid = 0, state = 0, oldbuf = 0, leavetime = 0.0};
	TeamA[5] = {pid = 0, state = 0, oldbuf = 0, leavetime = 0.0};

	local TeamB = {};
	TeamB[1] = {pid = 0, state = 0, oldbuf = 0, leavetime = 0.0};
	TeamB[2] = {pid = 0, state = 0, oldbuf = 0, leavetime = 0.0};
	TeamB[3] = {pid = 0, state = 0, oldbuf = 0, leavetime = 0.0};
	TeamB[4] = {pid = 0, state = 0, oldbuf = 0, leavetime = 0.0};
	TeamB[5] = {pid = 0, state = 0, oldbuf = 0, leavetime = 0.0};

	G_ApplyTeamMember_Table[1] = {Team = TeamA};
	G_ApplyTeamMember_Table[2] = {Team = TeamB};

	G_ApplyBuf_Table[1] = 352;
	G_ApplyBuf_Table[2] = 353;


--  下注NPC
	G_BetObj = RaidInstance:SpawnCreature( 604000005 , 1904.8, 1108.1, 20.0, 3.3, 0 );
	RaidInstance:ModifyObjectScale( G_BetObj, 2.0 );
	RaidInstance:SwitchCreatureAIMode( G_BetObj );
	RaidInstance:ModifyObjectFaction(G_BetObj,10000);
	G_BetGossipIndex_Table[1] = RaidInstance:InsertNPCGossip( G_BetObj, "(团队赛)花费"..RaidInstance:ConvertInteger2String(G_AddKaiXinCount).."开心币，支持A队" );
	G_BetGossipIndex_Table[2] = RaidInstance:InsertNPCGossip( G_BetObj, "(团队赛)花费"..RaidInstance:ConvertInteger2String(G_AddKaiXinCount).."开心币，支持B队" );
	RaidInstance:EnableNPCGossip( G_BetObj, G_BetGossipIndex_Table[1], 0 );
	RaidInstance:EnableNPCGossip( G_BetObj, G_BetGossipIndex_Table[2], 0 );

--　外围下注信息
	G_TeamBet_Table[1] = {Bet_count = 0,buf = 334, Bet = {}};
	G_TeamBet_Table[2] = {Bet_count = 0,buf = 335, Bet = {}};

--  报名NPC
	G_ApplyNPC = RaidInstance:SpawnCreature( 3079, G_OutX, G_OutY, G_OutZ, G_OutO, 0 );
	RaidInstance:ModifyObjectScale( G_ApplyNPC, 6.0 );
	RaidInstance:SwitchCreatureAIMode( G_ApplyNPC );
	RaidInstance:ModifyObjectFaction(G_ApplyNPC,10000);
	G_ApplyGossip_Table[1] = RaidInstance:InsertNPCGossip( G_ApplyNPC, "(团队赛)花费"..RaidInstance:ConvertInteger2String(G_ApplyKaixinCount).."开心币，加入A队" );
	G_ApplyGossip_Table[2] = RaidInstance:InsertNPCGossip( G_ApplyNPC, "(团队赛)花费"..RaidInstance:ConvertInteger2String(G_ApplyKaixinCount).."开心币，加入B队" );
	G_ApplyGossip_Table[3] = RaidInstance:InsertNPCGossip( G_ApplyNPC, "(团队赛)取消报名组队赛" );

	RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[1], 1 );
	RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[2], 1 );
	RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[3], 1);

	-- 个人赛模式
	G_ApplyGossip_Table[4] = RaidInstance:InsertNPCGossip( G_ApplyNPC, "(个人赛)花费"..RaidInstance:ConvertInteger2String(G_Single_Match.G_Join_Single_Match_Kaixin).."参加擂台争霸赛");
	RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[4], 1);

	G_ApplyGossip_Table[5] = RaidInstance:InsertNPCGossip( G_ApplyNPC, "(个人赛)取消擂台个人争霸赛");
	RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[5], 1);



	G_Single_Match:F_CreateSingleMatch();

	--赛制模式状态
	-- 0接受报名  1等待开始  2准备阶段  3开始状态 4结束奖励  5重置
	G_Match_State[0] = 0; -- 团队赛
	G_Match_State[1] = 0; -- 混战赛
	-- 蛋蛋和突发事件
	G_Egg_Mgr:F_CreateEggMgr();
	G_EmergencyMgr:F_CreateEmergencyMgr();
	--系统信息
	G_SysMsgTimeSave[1] = 0.0 ;
	G_SysMsgTimeSave[2] = 0.0 ;

end
-- 增加系统报名通知
function F_SendJoinMsg(Time)
	if G_Match_State[0] == 0 and Time - G_SysMsgTimeSave[1] > 120.0
	then
		RaidInstance:SendSystemNotify("擂台赛(团队赛)开始接受报名啦！");
		G_SysMsgTimeSave[1] = Time ;
	end

	if G_Match_State[1] == 0 and Time - G_SysMsgTimeSave[2] > 150.0
	then
		RaidInstance:SendSystemNotify("擂台赛(个人赛)开始接受报名啦！");
		G_SysMsgTimeSave[2] = Time ;
	end
end
-- 出局的玩家(团队赛)
function F_OutPlayer(Team, index)
	G_ApplyTeamMember_Table[Team].Team[index].state = 4;
	RaidInstance:TeleportPlayer(G_ApplyTeamMember_Table[Team].Team[index].pid, G_ToPaoPao_Pos_Table[3].homex,  G_ToPaoPao_Pos_Table[3].homey, G_ToPaoPao_Pos_Table[3].homez, G_ToPaoPao_Pos_Table[3].homeo );
	RaidInstance:SendSystemNotify(RaidInstance:GetPlayerName(G_ApplyTeamMember_Table[Team].Team[index].pid).."被击退出角斗场！" );
	RaidInstance:CreatureWhisper( G_ApplyNPC, G_ApplyTeamMember_Table[Team].Team[index].pid, "你被击退出角斗场!" );

	if index == 1
	then
		if Team == 1
		then
			RaidInstance:SendSystemNotify("由于A组队长被击退出角斗场,本场擂台赛(团队赛)B组获胜结果！10秒后发放奖励");
			G_TeamWinner =  2;
		else
			RaidInstance:SendSystemNotify("由于B组队长被击退出角斗场,本场擂台赛(团队赛)A组获胜结果！10秒后发放奖励");
			G_TeamWinner =  1;
		end
		G_GameContrl.G_EndGameEvt = RaidInstance:CreateEvent(10, 1);
		G_Match_State[0] = 4;
	end

end
-- 检测玩家是否出局
function F_CheckFireState(team, index, TimeElapsed)
	local x,y,z,o = RaidInstance:GetObjectPosition( G_ApplyTeamMember_Table[team].Team[index].pid);
	local t_Dis = (G_CenterX - x) *(G_CenterX - x) + (G_CenterY - y) * (G_CenterY - y) + (G_CenterZ - z) * (G_CenterZ - z);
	if t_Dis > (G_Radius * G_Radius)
	then
			-- 被打出擂台
		if G_ApplyTeamMember_Table[team].Team[index].state == 2
		then
			G_ApplyTeamMember_Table[team].Team[index].state = 3;
			G_ApplyTeamMember_Table[team].Team[index].leavetime = TimeElapsed;
		else
			if TimeElapsed - G_ApplyTeamMember_Table[team].Team[index].leavetime >= G_GameContrl.G_OutLeiTaiTime
			then
				F_OutPlayer(team, index);
				return;
			end
		end
	else
		G_ApplyTeamMember_Table[team].Team[index].state = 2;
		G_ApplyTeamMember_Table[team].Team[index].leavetime = 0.0;
	end
end

-- 数据更新
function OnUpdate( TimeElapsed )
	if G_TeamWinner == 0 and G_Match_State[0] == 3
	then
		for i = 1, 5, 1
		do
			if G_ApplyTeamMember_Table[1].Team[i].pid ~= 0 and ( G_ApplyTeamMember_Table[1].Team[i].state == 2 or G_ApplyTeamMember_Table[1].Team[i].state == 3)
			then
				F_CheckFireState(1, i, TimeElapsed);
			end

			if G_ApplyTeamMember_Table[2].Team[i].pid ~= 0 and ( G_ApplyTeamMember_Table[2].Team[i].state == 2 or G_ApplyTeamMember_Table[2].Team[i].state == 3)
			then
				F_CheckFireState(2, i, TimeElapsed);
			end
		end
	end

	if G_Match_State[1] == 3
	then
		G_Single_Match:F_CheckSingle(TimeElapsed);
	end

	--开启团队赛模式
	if G_Match_State[0] == 1 and G_Match_State[1] < 2
	then
		G_AppearHomeEvt  =  RaidInstance:CreateEvent(1000, 1);
		RaidInstance:SendSystemNotify( "本轮报名结束!擂台即将开启" );
		G_Match_State[0] = 2;
		G_EmergencyMgr:F_BeginEmergency(TimeElapsed);
	end

	-- 开始混战模式
	if G_Match_State[1] == 1 and G_Match_State[0] < 2
	then
		G_GameContrl.G_NoticeGameStartEvt = RaidInstance:CreateEvent(10000, 3);
		G_GameContrl.G_NoticeIndex = 0;
		G_Single_Match:F_BeginSingleMatch();
		G_EmergencyMgr:F_BeginEmergency(TimeElapsed);
	end


	-- 突发事件， 增加擂台趣味
	if G_Match_State[0] == 3 or G_Match_State[1] == 3
	then
		G_EmergencyMgr:F_AddEmergency(TimeElapsed);
	end

	F_SendJoinMsg(TimeElapsed);

end

-- 给予奖励
function F_GiveWinnerReward()

	if G_Match_State[0] == 4
	then
		for i = 1, 5, 1
		do
		if G_ApplyTeamMember_Table[G_TeamWinner] ~= nil and G_ApplyTeamMember_Table[G_TeamWinner].Team[i].pid ~= 0
			then
				local t_PlayerName = RaidInstance:GetPlayerName(G_ApplyTeamMember_Table[G_TeamWinner].Team[i].pid);
				local t_RewardKaixin = G_ApplyKaixinCount * G_WinKaiXin + G_ApplyKaixinCount;
				RaidInstance:SendSystemNotify("玩家["..t_PlayerName.."]在擂台赛(团队赛)表现突出，获得"..RaidInstance:ConvertInteger2String(t_RewardKaixin).."开心币");
				RaidInstance:ItemReward(G_ApplyTeamMember_Table[G_TeamWinner].Team[i].pid, 56, t_RewardKaixin );
				RaidInstance:ItemReward(G_ApplyTeamMember_Table[G_TeamWinner].Team[i].pid, G_RewardItemID, 1);
			end
		end
		F_GiveBetWinner();
		G_Match_State[0] = 5;
		RaidInstance:SendSystemNotify( "本轮擂台团体赛结束!下一轮比赛报名开启" );
	end

	if G_Match_State[1] == 4
	then
		G_Single_Match:F_GiveSingleMatchReward();
		G_Match_State[1] = 5;
		RaidInstance:SendSystemNotify( "本轮擂台个人赛结束!下一轮比赛报名开启" );
	end

	G_GameContrl.G_GameReStart =  RaidInstance:CreateEvent(3000, 1);

end
-- 重置擂台
function F_ResetGameState()
 	G_GameContrl.G_NoticeGameStartEvt = 0;
 	G_GameContrl.G_NoticeIndex = 0 ;
    G_GameContrl.G_EndGameEvt = 0;

	G_EmergencyMgr:F_ResetEmergency();
	G_Egg_Mgr:F_Deswap();
	RaidInstance:PlaySound( 0, "Music/fairground.ogg", 1, 1, 0 );

	if G_Match_State[1] == 5
	then

		G_Single_Match:F_ResetSingleMatch();
		RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[4], 1 );
		RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[5], 1 );
		G_Match_State[1] = 0;
	end

	if G_Match_State[0] == 5
	then
		RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[1], 1 );
		RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[2], 1 );
		RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[3], 1 );

		for i = 1, 5, 1
		do
			for team = 1, 2, 1
			do
				if G_ApplyTeamMember_Table[team] ~= nil and G_ApplyTeamMember_Table[team].Team[i].pid ~= 0
				then
					RaidInstance:RemoveBuffFromPlayer(G_ApplyTeamMember_Table[team].Team[i].pid, G_Team_Buf_Table[1].buf);
					RaidInstance:RemoveBuffFromPlayer(G_ApplyTeamMember_Table[team].Team[i].pid, G_Team_Buf_Table[2].buf);
					RaidInstance:RemoveBuffFromPlayer(G_ApplyTeamMember_Table[team].Team[i].pid, G_Team_Buf_Table[3].buf);
					RaidInstance:RemoveBuffFromPlayer(G_ApplyTeamMember_Table[team].Team[i].pid, G_Team_Buf_Table[4].buf);
					RaidInstance:RemoveBuffFromPlayer(G_ApplyTeamMember_Table[team].Team[i].pid, G_Team_Buf_Table[5].buf);
					RaidInstance:DismissMountAndShift(G_ApplyTeamMember_Table[team].Team[i].pid);
					if G_ApplyTeamMember_Table[team].Team[i].oldbuf ~= 0
					then
						RaidInstance:AddBuffToPlayer(G_ApplyTeamMember_Table[team].Team[i].pid, G_ApplyTeamMember_Table[team].Team[i].oldbuf);
						if G_ApplyTeamMember_Table[team].Team[i].oldbuf == 326 or G_ApplyTeamMember_Table[team].Team[i].oldbuf == 327
						then
							RaidInstance:ModifyPlayerPVPFlag(G_ApplyTeamMember_Table[team].Team[i].pid, 2);
						else
							RaidInstance:ModifyPlayerPVPFlag(G_ApplyTeamMember_Table[team].Team[i].pid, 1);
						end
					end
					if G_ApplyTeamMember_Table[team].state ~= 4
					then
						RaidInstance:TeleportPlayer(G_ApplyTeamMember_Table[team].Team[i].pid, G_ToPaoPao_Pos_Table[3].homex, G_ToPaoPao_Pos_Table[3].homey,G_ToPaoPao_Pos_Table[3].homez, G_ToPaoPao_Pos_Table[3].homeo );
					end
					RaidInstance:ClearObjectVariable(G_ApplyTeamMember_Table[team].Team[i].pid);

					G_ApplyTeamMember_Table[team].Team[i].pid = 0;
					G_ApplyTeamMember_Table[team].Team[i].state = 0;
					G_ApplyTeamMember_Table[team].Team[i].oldbuf = 0;
					G_ApplyTeamMember_Table[team].Team[i].leavetime = 0.0;
				end
			end
		end

		G_TeamAFull = 0;
		G_TeamBFull = 0;
		G_TeamWinner = 0;
		G_AppearHomeEvt = 0;
		G_ChooseAnimalEndEvt = 0;
		G_Match_State[0] = 0;
	end
end
-- 添加小队成员
function F_AddPlayerToTable(Teamindex, PlayerID)
	for i = 1, 5, 1
	do
		if G_ApplyTeamMember_Table[Teamindex].Team[i].pid == 0
		then
			G_ApplyTeamMember_Table[Teamindex].Team[i].pid = PlayerID
			if Teamindex == 1
			then
				RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "报名A组成功!" );
				RaidInstance:AddBuffToPlayer(PlayerID, G_ApplyBuf_Table[1]);
				G_TeamAFull = G_TeamAFull + 1;
			else
				RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "报名B组成功!" );
				RaidInstance:AddBuffToPlayer(PlayerID, G_ApplyBuf_Table[2]);
				G_TeamBFull = G_TeamBFull + 1;
			end
			RaidInstance:ModifyObjectVariable( PlayerID, 0, 1 );
			return;
		end
	end
end
-- 玩家报名
function F_PlayerApply(GossipIndex, PlayerID)
	-- 已经报名
	for  i = 1, 5, 1
	do
		if G_ApplyTeamMember_Table[1].Team[i].pid == PlayerID
		then
			RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "您已经报名擂台赛！" );
			return;
		end

		if G_ApplyTeamMember_Table[2].Team[i].pid == PlayerID
		then
			RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "您已经报名擂台赛！" );
			return;
		end
	end

-- Team A
	if G_ApplyGossip_Table[1] == GossipIndex and G_TeamAFull < G_TeamMaxCount
	then
		-- add to team a
		local t_HasKaixin =  RaidInstance:ConsumeCurrency( PlayerID, 0, 56, G_ApplyKaixinCount );
		if t_HasKaixin == 0
		then
			RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "你没有足够的报名费!" );
			return;
		end
		F_AddPlayerToTable(1, PlayerID);
		if G_TeamAFull >= G_TeamMaxCount
		then
			RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[1], 0 );
			RaidInstance:SendSystemNotify( "A 组报名已满!" );
		end

	end
-- Team B
	if G_ApplyGossip_Table[2] == GossipIndex and G_TeamBFull < G_TeamMaxCount
	then
		-- add to team b
		local t_HasKaixin =  RaidInstance:ConsumeCurrency( PlayerID, 0, 56, G_ApplyKaixinCount );
		if t_HasKaixin == 0
		then
			RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "你没有足够的报名费!" );
			return;
		end
		F_AddPlayerToTable(2, PlayerID);

		if G_TeamBFull >= G_TeamMaxCount
		then
			RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[2], 0 );
			RaidInstance:SendSystemNotify( "B 组报名已满!" );
		end
	end

	if G_TeamAFull >= G_TeamMaxCount and G_TeamBFull >= G_TeamMaxCount
	then
		G_Match_State[0] = 1;
	end
end
-- 传送到休息室
function F_AppearToHome()

	for i = 1, 5, 1
	do
		if G_ApplyTeamMember_Table[1].Team[i].pid ~= 0
		then
			G_ApplyTeamMember_Table[1].Team[i].oldbuf = RaidInstance:GetPlayerShapeShiftSpellEntry( G_ApplyTeamMember_Table[1].Team[i].pid );
			RaidInstance:TeleportPlayer(G_ApplyTeamMember_Table[1].Team[i].pid, G_ToPaoPao_Pos_Table[1].homex, G_ToPaoPao_Pos_Table[1].homey, G_ToPaoPao_Pos_Table[1].homez, G_ToPaoPao_Pos_Table[1].homeo );
			RaidInstance:ModifyPlayerPVPFlag(G_ApplyTeamMember_Table[1].Team[i].pid, 22);

		end

		if G_ApplyTeamMember_Table[2].Team[i].pid ~= 0
		then
			G_ApplyTeamMember_Table[2].Team[i].oldbuf = RaidInstance:GetPlayerShapeShiftSpellEntry( G_ApplyTeamMember_Table[2].Team[i].pid );
			RaidInstance:TeleportPlayer(G_ApplyTeamMember_Table[2].Team[i].pid, G_ToPaoPao_Pos_Table[2].homex, G_ToPaoPao_Pos_Table[2].homey, G_ToPaoPao_Pos_Table[2].homez, G_ToPaoPao_Pos_Table[2].homeo );
			RaidInstance:ModifyPlayerPVPFlag(G_ApplyTeamMember_Table[2].Team[i].pid, 21);
		end
	end


	RaidInstance:EnableNPCGossip( G_BetObj, G_BetGossipIndex_Table[1], 1 );
	RaidInstance:EnableNPCGossip( G_BetObj, G_BetGossipIndex_Table[2], 1 );

	RaidInstance:SendSystemNotify( "30秒后系统自动分配,大家可以找找【泡泡】给自己喜爱的队伍投票啦！" );
	G_ChooseAnimalEndEvt = RaidInstance:CreateEvent(30000, 1);
	G_GameContrl.IsReadyToStart = 1;
end
-- 获得外围某队下注的总数
function F_GetBetKaixinCount( team)
	if G_TeamBet_Table[team] == nil
	then
		return  0 ;
	end
	local TeamBetCount =  G_TeamBet_Table[team].Bet_count ;
	local BetKaixinCount = 0;
	if TeamBetCount > 0
	then
		-- 计算总下注数目
		for i = 1, TeamBetCount, 1
		do
			if G_TeamBet_Table[team].Bet[i] ~= nil
			then
				RaidInstance:RemoveBuffFromPlayer(G_TeamBet_Table[team].Bet[i].pid, G_TeamBet_Table[1].buf);
				RaidInstance:RemoveBuffFromPlayer(G_TeamBet_Table[team].Bet[i].pid, G_TeamBet_Table[2].buf);
				BetKaixinCount = BetKaixinCount +  G_TeamBet_Table[team].Bet[i].kaixin;
			end
		end
	end

	return BetKaixinCount;
end
--发放外围下注奖励
function F_GiveBetWinner()
	if G_TeamBet_Table[G_TeamWinner] ~= nil
	then
		local LoseTeam = 0;
		if G_TeamWinner == 1
		then
			LoseTeam = 2;
		end
		if G_TeamWinner == 2
		then
			LoseTeam = 1;
		end
		local WinCount = F_GetBetKaixinCount(G_TeamWinner);
		local LoseCount = F_GetBetKaixinCount(LoseTeam);

		local TeamBetCount =  G_TeamBet_Table[G_TeamWinner].Bet_count ;
		if TeamBetCount > 0
		then
		-- 计算总下注数目
			for i = 1, TeamBetCount, 1
			do
				if G_TeamBet_Table[G_TeamWinner].Bet[i] ~= nil
				then
					local Win =  G_TeamBet_Table[G_TeamWinner].Bet[i].kaixin + G_TeamBet_Table[G_TeamWinner].Bet[i].kaixin * G_BetScale * LoseCount / WinCount ;
					if Win > 0
					then
						RaidInstance:ItemReward( G_TeamBet_Table[G_TeamWinner].Bet[i].pid, 56, Win );
						RaidInstance:ItemReward( G_TeamBet_Table[G_TeamWinner].Bet[i].pid, G_BetRewardItemID, 1 );

						RaidInstance:CreatureWhisper( G_BetObj, G_TeamBet_Table[G_TeamWinner].Bet[i].pid, "由于你支持的小组在擂台赛获得胜利! 您获得了"..RaidInstance:ConvertInteger2String(Win).."开心币的奖励！" );
					end
				end
			end
		end
	end

	G_TeamBet_Table[1] = {Bet_count = 0,buf = 334, Bet = {}};
	G_TeamBet_Table[2] = {Bet_count = 0,buf = 335, Bet = {}};
end
--玩家下注
function F_AddPlayerBet(GossipIndex, PlayerID)

	-- 判断是否有钱
	local HasKaixin =  RaidInstance:ConsumeCurrency( PlayerID, 0, 56, G_AddKaiXinCount );
	if HasKaixin == 0
	then
		RaidInstance:CreatureWhisper( G_BetObj, PlayerID, "你没有足够的开心币!" );
		return ;
	end

	-- 下注的列表
	local TeamBetCount = 0;
	local BetIndex = 0;
	if GossipIndex == G_BetGossipIndex_Table[1] -- a
	then
		RaidInstance:CreatureWhisper( G_BetObj, PlayerID, "你给A队投票啦!" );
		RaidInstance:AddBuffToPlayer(PlayerID, G_TeamBet_Table[1].buf);
		BetIndex = 1;
	end
	if GossipIndex == G_BetGossipIndex_Table[2] -- b
	then
		RaidInstance:CreatureWhisper( G_BetObj, PlayerID, "你给B队投票啦!" );
		RaidInstance:AddBuffToPlayer(PlayerID, G_TeamBet_Table[2].buf);
		BetIndex = 2;
	end

	-- 查找下注列表数据
	if G_TeamBet_Table[BetIndex] ~= nil
	then
		TeamBetCount =  G_TeamBet_Table[BetIndex].Bet_count ;
		if TeamBetCount > 0
		then
			-- 查询是否已经下注
			for i = 1, TeamBetCount, 1
			do
				if G_TeamBet_Table[BetIndex].Bet[i] ~= nil and G_TeamBet_Table[BetIndex].Bet[i].pid == PlayerID
				then
					G_TeamBet_Table[BetIndex].Bet[i].kaixin = G_TeamBet_Table[BetIndex].Bet[i].kaixin + G_AddKaiXinCount;

					return;
				end
			end
		end

		--第一次下注操作 ;
		G_TeamBet_Table[BetIndex].Bet_count = G_TeamBet_Table[BetIndex].Bet_count + 1;
		TeamBetCount = G_TeamBet_Table[BetIndex].Bet_count;

		G_TeamBet_Table[BetIndex].Bet[TeamBetCount] = {pid = PlayerID, kaixin = G_AddKaiXinCount};

	end

end
--NPC对话信息
function OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )

	-- 只有组队赛的准备开始阶段才能下注
	if G_BetObj == NPC_ID and G_Match_State[0] == 2
	then
		F_AddPlayerBet(GossipIndex, PlayerID);
		return;
	end
	-- 报名
	if 	NPC_ID == G_ApplyNPC
	then
		--惩罚buf
		if RaidInstance:HasBuff(PlayerID, G_TeamMemberLeaveBuf) == 1
		then
			RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "你暂时不能参加这类活动！!" );
			return ;
		end
		-- 报名
		if RaidInstance:GetObjectVariable(PlayerID, 0) == 0
		then
			if G_ApplyGossip_Table[1] == GossipIndex or G_ApplyGossip_Table[2] == GossipIndex and G_Match_State[0] == 0
			then
				F_PlayerApply(GossipIndex, PlayerID);
			end
			-- 测试 个人擂台赛
			if G_ApplyGossip_Table[4] == GossipIndex and G_Match_State[1] == 0
			then
				G_Single_Match:F_PlayerApplySingleMatch(PlayerID);
			end
		else
		--取消报名
			if 	GossipIndex == G_ApplyGossip_Table[3]
			then
				F_PlayerCancelApply(PlayerID , 1);
			end

			if G_ApplyGossip_Table[5] == GossipIndex and (G_Match_State[1] == 0 or G_Match_State[1] == 1)
			then
				if G_Single_Match.G_Single_Will_BeginEvt == 0
				then
					G_Single_Match:F_PlayerCancelSingleMatch(PlayerID);
				else
					RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "擂台个人赛即将开始，此时无法取消报名!" );
				end
			end
		end
	end
end
-- 取消报名
function F_PlayerCancelApply(PlayerID, Leave)

	if G_Match_State[0] == 0 or G_Match_State[0] == 1
	then
		for  i = 1, 5, 1
		do
			if G_ApplyTeamMember_Table[1].Team[i].pid == PlayerID
			then
				RaidInstance:ModifyObjectVariable( PlayerID, 1, 1 );
				RaidInstance:ItemReward( PlayerID, 56, G_ApplyKaixinCount * 0.8 );
				RaidInstance:ModifyObjectVariable( PlayerID, 1, 0 );
				RaidInstance:ClearObjectVariable(PlayerID);
				G_ApplyTeamMember_Table[1].Team[i].pid = 0;
				G_ApplyTeamMember_Table[1].Team[i].state = 0;
				RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "您取消参加擂台赛！" );
				RaidInstance:SendSystemNotify( "由于有玩家取消参加擂台赛，A组接受报名!" );
				RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[1], 1 );
				RaidInstance:RemoveBuffFromPlayer(PlayerID, G_ApplyBuf_Table[1]);
				G_TeamAFull = G_TeamAFull - 1;
				G_Match_State[0] = 0;
				return;
			end

			if G_ApplyTeamMember_Table[2].Team[i].pid == PlayerID
			then
				RaidInstance:ModifyObjectVariable( PlayerID, 1, 1 );
				RaidInstance:ItemReward( PlayerID, 56, G_ApplyKaixinCount * 0.8 );
				RaidInstance:ModifyObjectVariable( PlayerID, 1, 0 );
				RaidInstance:ClearObjectVariable(PlayerID);
				G_ApplyTeamMember_Table[2].Team[i].pid = 0;
				G_ApplyTeamMember_Table[2].Team[i].state = 0;
				RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "您取消参加擂台赛！" );
				RaidInstance:SendSystemNotify( "由于有玩家取消参加擂台赛，B组接受报名!" );
				RaidInstance:RemoveBuffFromPlayer(PlayerID, G_ApplyBuf_Table[2]);
				RaidInstance:EnableNPCGossip( G_ApplyNPC, G_ApplyGossip_Table[2], 1 );
				G_TeamBFull = G_TeamBFull - 1;
				G_Match_State[0] = 0;
				return;
			end
		end
		if Leave == 1
		then
			RaidInstance:CreatureWhisper( G_ApplyNPC, PlayerID, "您没有报名啊！" );
		end
	end

end

-- 系统随即分配队员角色
function F_EndChooseAnimal()
	for  i = 1, 5, 1
	do
		for team = 1, 2, 1
		do
			if G_ApplyTeamMember_Table[team].Team[i].pid ~= 0 and G_ApplyTeamMember_Table[team].Team[i].state == 0
			then
				RaidInstance:RemoveBuffFromPlayer(G_ApplyTeamMember_Table[team].Team[i].pid, G_ApplyBuf_Table[team]);
				RaidInstance:DismissMountAndShift(G_ApplyTeamMember_Table[team].Team[i].pid);
				RaidInstance:AddBuffToPlayer(G_ApplyTeamMember_Table[team].Team[i].pid, G_Team_Buf_Table[i].buf);
				RaidInstance:TeleportPlayer(G_ApplyTeamMember_Table[team].Team[i].pid, G_ToPaoPao_Pos_Table[team].leitai[i].x, G_ToPaoPao_Pos_Table[team].leitai[i].y,  G_ToPaoPao_Pos_Table[team].leitai[i].z,  G_ToPaoPao_Pos_Table[team].leitai[i].o );
				RaidInstance:ForceRootPlayer(G_ApplyTeamMember_Table[team].Team[i].pid);
				G_ApplyTeamMember_Table[team].Team[i].state = 1;
			end
		end
	end
end
--系统事件响应
function OnEvent( EventID, Attach, Attach2, Attach3, Attach4, Attach5 )
	if EventID == G_AppearHomeEvt
	then
		F_AppearToHome();
		G_AppearHomeEvt = 0;
	end

	if EventID == G_ChooseAnimalEndEvt
	then
		G_GameContrl.IsReadyToStart = 0;
		RaidInstance:PlaySound( 0, "Music/fairground.ogg", 1, 1, 0 );

		F_EndChooseAnimal();
		G_GameContrl.G_NoticeGameStartEvt = RaidInstance:CreateEvent(10000, 3);
		G_GameContrl.G_NoticeIndex = 0;
		G_ChooseAnimalEndEvt = 0;
	end

	if G_GameContrl.G_NoticeGameStartEvt == EventID
	then
		F_NoticeBeginGame();
	end
	if G_GameContrl.G_EndGameEvt == EventID
	then
		F_GiveWinnerReward();
		G_GameContrl.G_EndGameEvt = 0;
	end
	if G_GameContrl.G_GameReStart == EventID
	then
		F_ResetGameState();
		G_GameContrl.G_GameReStart = 0;
	end


-- 突发事件和蛋
	if EventID ~= 0
	then
		G_EmergencyMgr:F_EndEmergency(EventID);
		G_EmergencyMgr:F_EmergencyUseSpell(EventID);
		G_Egg_Mgr:F_SetCreateEgg(EventID);

		G_Single_Match:F_SingleMatch_WillBegin(EventID);
	end;


end
-- 战斗开始
function F_BeginGame()

	if G_Match_State[0] == 3
	then
		for  i = 1, 5, 1
		do
			if G_ApplyTeamMember_Table[1].Team[i].pid ~= 0
			then
				RaidInstance:ForceUnrootPlayer(G_ApplyTeamMember_Table[1].Team[i].pid);
				G_ApplyTeamMember_Table[1].Team[i].state = 2;
			end

			if G_ApplyTeamMember_Table[2].Team[i].pid ~= 0
			then
				RaidInstance:ForceUnrootPlayer(G_ApplyTeamMember_Table[2].Team[i].pid);
				G_ApplyTeamMember_Table[2].Team[i].state = 2;
			end
		end
	end

	if G_Match_State[1] == 3
	then
		for k, v in pairs(G_Single_Match.G_Single_Match_Table)
		do
			if v ~= nil
			then
				G_Single_Match.G_Single_Match_Table[k].state = 2;
				--RaidInstance:ForceUnrootPlayer(k);
				RaidInstance:ModifyPlayerPVPFlag(k, 1);
			end
		end
	end

	G_Egg_Mgr:F_CreateAddEvt();
end
--玩家进入副本
function OnPlayerEnterInstance(PlayerID)
	for i = 309, 320, 1
	do
		RaidInstance:AddPlayerSpellCastLimit( PlayerID, i );
	end

	for i = 355, 359, 1
	do
		RaidInstance:AddPlayerSpellCastLimit( PlayerID, i );
	end

	RaidInstance:AddPlayerSpellCastLimit( PlayerID, 365);

	RaidInstance:ClearObjectVariable(PlayerID);

	if G_GameContrl.IsReadyToStart == 1 then
		RaidInstance:PlaySound( 0, "Music/leitai.ogg", 1, 1, 0 );
	else
		RaidInstance:PlaySound( 0, "Music/fairground.ogg", 1, 1, 0 );
	end
end
--玩家离开副本处理
function OnPlayerLeaveInstance( PlayerID )
	if G_Single_Match.G_Single_Match_Table[PlayerID] ~= nil
	then
		if G_Match_State[1] == 0 or G_Match_State[1] == 1
		then
			G_Single_Match:F_PlayerCancelSingleMatch(PlayerID);
		else
			if G_Match_State[1] ~= 5 or G_Match_State[1] ~= 4
			then
				G_Single_Match.G_Single_Match_Table[PlayerID].buf = 0;
				G_Single_Match:F_OutSinglePlayer(PlayerID, 0);
			end
		end
	end

	if G_Match_State[0] == 0 or G_Match_State[0] == 1
	then
		F_PlayerCancelApply(PlayerID, 0);
	else
		local NeedReturnMoney = 0;

		for  i = 1, 5, 1
		do
			for team = 1, 2, 1
			do
				if G_ApplyTeamMember_Table[team].Team[i].pid == PlayerID
				then
					if G_Match_State[0] ==  2
					then
						G_GameContrl.G_GameReStart = RaidInstance:CreateEvent(10, 1);
						G_ChooseAnimalEndEvt = 0;
						NeedReturnMoney = 1;
						G_Match_State[0] = 5;
					else
						if G_Match_State[0] == 3
						then
							RaidInstance:SendSystemNotify(RaidInstance:GetPlayerName(PlayerID).."退出了本轮擂台赛" );
							F_OutPlayer(team, i);
						end
					end
					-- 出局了离开。没有惩罚BUF。
					if G_ApplyTeamMember_Table[team].Team[i].state ~= 4
					then
						RaidInstance:AddBuffToPlayer(PlayerID, G_TeamMemberLeaveBuf);
					end
					G_ApplyTeamMember_Table[team].Team[i].pid = 0;
					G_ApplyTeamMember_Table[team].Team[i].state = 0;
					G_ApplyTeamMember_Table[team].Team[i].oldbuf = 0;
					G_ApplyTeamMember_Table[team].Team[i].leavetime = 0.0;

				break;
				end
			end
		end

		if NeedReturnMoney == 1
		then
			for  i = 1, 5, 1
			do
				for team = 1, 2, 1
				do
					if G_ApplyTeamMember_Table[team].Team[i].pid ~= PlayerID and  G_ApplyTeamMember_Table[team].Team[i].pid ~= 0
					then
						RaidInstance:ModifyObjectVariable( G_ApplyTeamMember_Table[team].Team[i].pid, 1, 1 );
						RaidInstance:ItemReward( G_ApplyTeamMember_Table[team].Team[i].pid, 56, G_ApplyKaixinCount );
						RaidInstance:CreatureWhisper( G_ApplyNPC, G_ApplyTeamMember_Table[team].Team[i].pid, "由于玩家["..RaidInstance:GetPlayerName(PlayerID).."]提前退出比赛，本轮(团队赛)取消！" );
						RaidInstance:ModifyObjectVariable( G_ApplyTeamMember_Table[team].Team[i].pid, 1, 0 );
					end
				end
			end
		end


	end

	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Team_Buf_Table[1].buf);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Team_Buf_Table[2].buf);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Team_Buf_Table[3].buf);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Team_Buf_Table[4].buf);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Team_Buf_Table[5].buf);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_ApplyBuf_Table[1]);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_ApplyBuf_Table[2]);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Single_Match.G_Single_Match_Buf[1]);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Single_Match.G_Single_Match_Buf[2]);
	RaidInstance:ClearObjectVariable(PlayerID);
	RaidInstance:ClearPlayerSpellCastLimit(PlayerID);
end
--战斗开始倒计时
function F_NoticeBeginGame()
		if G_GameContrl.G_NoticeIndex == 0
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)30秒后开始，小可爱们可以来观战啦，狂热的粉丝们可以找【泡泡】给自己喜爱队伍投票！" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)30秒后开始，请参加的玩家做好准备" );
				G_Single_Match:F_TransToLeiTai();
			end

			G_GameContrl.G_NoticeIndex = 12;
		elseif G_GameContrl.G_NoticeIndex == 12
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)20秒后开始，小可爱们可以来观战啦，狂热的粉丝们可以找【泡泡】给自己喜爱队伍投票！" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)20秒后开始，请参加的玩家做好准备" );
			end
			G_GameContrl.G_NoticeIndex = 11;
		elseif G_GameContrl.G_NoticeIndex == 11
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)10秒后开始，小可爱们可以来观战啦，狂热的粉丝们可以找【泡泡】给自己喜爱队伍投票！" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)10秒后开始，请参加的玩家做好准备" );
			end
			G_GameContrl.G_NoticeIndex = 10;
			G_GameContrl.G_NoticeGameStartEvt = RaidInstance:CreateEvent(1000, 10);
		elseif G_GameContrl.G_NoticeIndex == 10
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)9秒后开始" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)9秒后开始" );
			end

			G_GameContrl.G_NoticeIndex = 9;
		elseif G_GameContrl.G_NoticeIndex == 9
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)8秒后开始" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)8秒后开始" );
			end

			G_GameContrl.G_NoticeIndex = 8;
		elseif G_GameContrl.G_NoticeIndex == 8
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)7秒后开始" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)7秒后开始" );
			end
			G_GameContrl.G_NoticeIndex = 7;
		elseif G_GameContrl.G_NoticeIndex == 7
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)6秒后开始" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)6秒后开始" );
			end
			G_GameContrl.G_NoticeIndex = 6;
		elseif G_GameContrl.G_NoticeIndex == 6
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)5秒后开始" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)5秒后开始" );
			end
			RaidInstance:PlaySound( G_ApplyNPC, "HappyBear/Five.wav" );
			G_GameContrl.G_NoticeIndex = 5;
		elseif G_GameContrl.G_NoticeIndex == 5
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)4秒后开始" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)4秒后开始" );
			end
			RaidInstance:PlaySound( G_ApplyNPC, "HappyBear/Four.wav" );
			G_GameContrl.G_NoticeIndex = 4;

		elseif G_GameContrl.G_NoticeIndex == 4
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)3秒后开始" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)3秒后开始" );
			end
			RaidInstance:PlaySound( G_ApplyNPC, "HappyBear/Three.wav" );
			G_GameContrl.G_NoticeIndex = 3;
		elseif G_GameContrl.G_NoticeIndex == 3
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)2秒后开始" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)2秒后开始" );
			end
			RaidInstance:PlaySound( G_ApplyNPC, "HappyBear/Two.wav" );
			G_GameContrl.G_NoticeIndex = 2;
		elseif G_GameContrl.G_NoticeIndex == 2
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)1秒后开始" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)1秒后开始" );
			end
			RaidInstance:PlaySound( G_ApplyNPC, "HappyBear/One.wav" );
			G_GameContrl.G_NoticeIndex = 1;
		elseif G_GameContrl.G_NoticeIndex == 1
		then
			if G_Match_State[0] == 2
			then
				RaidInstance:SendSystemNotify( "本轮擂台赛(团队赛)正式开始" );
			else
				RaidInstance:SendSystemNotify( "本轮擂台赛(个人赛)正式开始" );
			end

			RaidInstance:PlaySound( 0, "Music/leitai.ogg",1,0,0 );


			if G_Match_State[0] == 2
			then
				G_Match_State[0] = 3;
				RaidInstance:EnableNPCGossip( G_BetObj, G_BetGossipIndex_Table[1], 0 );
				RaidInstance:EnableNPCGossip( G_BetObj, G_BetGossipIndex_Table[2], 0 );
			end

			if G_Match_State[1] == 2
			then
				G_Match_State[1] = 3;
			end

			F_BeginGame();
		end
end
