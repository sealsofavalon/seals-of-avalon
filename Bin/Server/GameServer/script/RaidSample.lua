-- Raid instance lua sample
-- float value must be written like this: 1200.0
-- integer value must be written like this: 10
-- RaidInstance must be catched

local glz_guid;		-- guilizi's unique id
local mk_guid;		-- mokui's unique id
local RaidInstance;	-- raid instance object

function OnStart( ID )		-- on instance start.

	-- catch the raid instance object. this is very important.
	RaidInstance = LuaGetRaidObject( ID );

	-- spawn guilizi, x = 1200.0, y = 1200.0, z = 0.0, o = 0.0, can respawn, faction = 10
	glz_guid = RaidInstance:SpawnCreature( 110000369, 1200.0, 1200.0, 0.0, 0.0, 1, 10 );
	
	-- spawn mokui, x = 1200.0, y = 1200.0, z = 0.0, o = 0.0, can respawn, faction = 10
	mk_guid = RaidInstance:SpawnCreature( 110000316, 1220.0, 1220.0, 0.0, 0.0, 1, 10 );
end

function OnUpdate( TimeElapsed )		-- instance update every second.
	if TimeElapsed >= 50
	then
		-- guilizi yell
		RaidInstance:CreatureSay( glz_guid, "guilizi", 1 );
	else
		-- mokui say
		RaidInstance:CreatureSay( mk_guid, "mokui", 0 );
	end
end

function OnEnd()

end

function OnObjectDie( VictimID, AttackerID )
end

function OnGameObjectTrigger( GameObjectID, PlayerID )
end

function OnUnitEnterCombat( PlayerID )
end

function OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )
end

function OnRaidWipe()
end

function OnPlayerEnterInstance( PlayerID )
end

function OnPlayerLeaveInstance( PlayerID )
end

function OnPlayerCastSpell( SpellID, PlayerID )
end

function OnEvent( EventID, Attach, Attach2, Attach3, Attach4, Attach5 )
end