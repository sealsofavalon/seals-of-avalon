--Author wf
--3/28/2011
--

local RaidInstance;
local G_SysWin = 0;
local G_SysGet = 0;
local G_f_SysWin = 75000; -- 6000/ 10000
local G_f_SysPIN = 10000; -- 1000/ 10000
local G_f_Father = 100000;
local G_f_SysBack = 0.6;
local G_Sys_OutMoney_Table = {};


-- 转盘信息
local G_Gamble_Info_Table = {};
local G_NPC_Stake_GossipIndex_Table = {}; -- 下注对话index

local G_Reward_ItemID = 76;
--NPC信息
local G_Coin_NPC_Table = {}; -- 下注npc的id
local G_Coin_Npc_Name_Table = {};
local G_AddKaixinStake = {Stake1 = 100, Stake2 = 50, Stake3 = 10, Stake4 = 5}; --下注数量

local G_Kaixin_Buf_Table = {}; -- 下注BUF
--玩家下注信息
local G_Player_Stake_Table = {};


--指针
local G_Pointer = 0;
local G_Pointer_Anim_Table = {};

-- 转盘控制
local G_CanAddKaixin = 1;
local G_EventBegin = 0;
local G_EventBeginCount = 0;
local G_BeginRunEvent = 0;
local G_RunMachine = 0;
local G_RuningEvent = 0;
local G_EndMachine = 0;

local G_PublicAnimCount = 0;
--效果
local G_EWA = 0;
local G_YanHuaPos = {};
local G_CurYanHuaIndex = 0;
local G_YanHuaEvent = 0;

--本轮结果
local G_LackResult = 0;

function F_AddCoinNPC(Entry, x, y, z, o, lackyIndex)
	local CoinNpc = RaidInstance:SpawnCreature( Entry, x,y,z,o, 0);
	RaidInstance:ModifyObjectScale( CoinNpc, 3.0 );
	RaidInstance:ModifyObjectFaction(CoinNpc,10000);

	G_NPC_Stake_GossipIndex_Table[CoinNpc] = {Stake_Gossip1 = 0, Stake_Gossip2 = 0,Stake_Gossip3 = 0,Stake_Gossip4 = 0};

	local ObjectIndex = 0;
	ObjectIndex = RaidInstance:InsertNPCGossip( CoinNpc, "给我投票"..RaidInstance:ConvertInteger2String(G_AddKaixinStake.Stake1).."开心币!" );
	RaidInstance:EnableNPCGossip( CoinNpc, ObjectIndex, 1 );
	G_NPC_Stake_GossipIndex_Table[CoinNpc].Stake_Gossip1 = ObjectIndex;

	ObjectIndex = RaidInstance:InsertNPCGossip( CoinNpc, "给我投票"..RaidInstance:ConvertInteger2String(G_AddKaixinStake.Stake2).."开心币!" );
	RaidInstance:EnableNPCGossip( CoinNpc, ObjectIndex, 1 );
	G_NPC_Stake_GossipIndex_Table[CoinNpc].Stake_Gossip2 = ObjectIndex;

	ObjectIndex = RaidInstance:InsertNPCGossip( CoinNpc, "给我投票"..RaidInstance:ConvertInteger2String(G_AddKaixinStake.Stake3).."开心币!" );
	RaidInstance:EnableNPCGossip( CoinNpc, ObjectIndex, 1 );
	G_NPC_Stake_GossipIndex_Table[CoinNpc].Stake_Gossip3 = ObjectIndex;

	ObjectIndex = RaidInstance:InsertNPCGossip( CoinNpc, "给我投票"..RaidInstance:ConvertInteger2String(G_AddKaixinStake.Stake4).."开心币!" );
	RaidInstance:EnableNPCGossip( CoinNpc, ObjectIndex, 1 );
	G_NPC_Stake_GossipIndex_Table[CoinNpc].Stake_Gossip4 = ObjectIndex;



	G_Coin_NPC_Table[lackyIndex] = CoinNpc;
end
function OnStart( RaidInstanceID )
	RaidInstance = LuaGetRaidObject( RaidInstanceID );
	-- add coin npc
	F_AddCoinNPC(607000121, 1897.9, 1367.6, 19.3, 0.7, 1); -- chicken 小鸡小鸡
	F_AddCoinNPC(607000122, 1905.9, 1363.0, 19.3, 0.5, 2); -- tortoise 乌龟乌龟
	F_AddCoinNPC(607000123, 1914.8, 1360.4, 19.3, 0.2, 3); -- penguin 企鹅企鹅
	F_AddCoinNPC(607000126, 1924.3, 1360.6, 19.3, 6.2, 4); -- crocodile 鳄鱼鳄鱼
	F_AddCoinNPC(607000124, 1933.2, 1362.8, 19.3, 6.0, 5); -- panda 熊猫熊猫
	F_AddCoinNPC(607000125, 1941.2, 1367.5, 19.3, 5.8, 6); -- sprorit 雪人雪人

	G_YanHuaPos[1] = {x = 1919.6, y = 1383.5, z = 18.5, o = 3.2};
	G_YanHuaPos[2] = {x = 1909.4, y = 1389.4, z = 18.5, o = 2.8};
	G_YanHuaPos[3] = {x = 1909.2, y = 1400.9, z = 18.5, o = 3.4};
	G_YanHuaPos[4] = {x = 1919.9, y = 1407.2, z = 18.5, o = 3.2};
	G_YanHuaPos[5] = {x = 1929.9, y = 1401.4, z = 18.5, o = 3.0};
	G_YanHuaPos[6] = {x = 1929.8, y = 1389.5, z = 18.5, o = 3.2};

	G_Coin_Npc_Name_Table[1] = "小鸡";
	G_Coin_Npc_Name_Table[2] = "乌龟";
	G_Coin_Npc_Name_Table[3] = "企鹅";
	G_Coin_Npc_Name_Table[4] = "鳄鱼";
	G_Coin_Npc_Name_Table[5] = "熊猫";
	G_Coin_Npc_Name_Table[6] = "雪人";

	-- coin buf
	G_Kaixin_Buf_Table[G_Coin_NPC_Table[1]] = 121;
	G_Kaixin_Buf_Table[G_Coin_NPC_Table[2]] = 126;
	G_Kaixin_Buf_Table[G_Coin_NPC_Table[3]] = 122;
	G_Kaixin_Buf_Table[G_Coin_NPC_Table[4]] = 124;
	G_Kaixin_Buf_Table[G_Coin_NPC_Table[5]] = 125;
	G_Kaixin_Buf_Table[G_Coin_NPC_Table[6]] = 123;

	--create pointer
	G_Pointer = RaidInstance:SpawnCreature( 3085, 1919.2, 1395.3, 19.9, 2.96, 0 );
	RaidInstance:ModifyObjectScale( G_Pointer, 1.0 );
	RaidInstance:SwitchCreatureAIMode( G_Pointer );
	RaidInstance:ModifyObjectFaction(G_Pointer,10000);

	G_EWA = RaidInstance:SpawnCreature( 3086, 1919.2, 1395.3, 19.2, 6.28, 0 );
	RaidInstance:ModifyObjectScale( G_EWA, 3.0 );
	RaidInstance:SwitchCreatureAIMode( G_EWA );
	RaidInstance:ModifyObjectFaction(G_EWA,10000);

	--gamble info
	G_Gamble_Info_Table[1] = { x = 1919.3, y = 1374.2, z = 18.5, o = 3.2, lackyIndex = 4, multiple = 1, anim = "roll_01", time = 4.433};
	G_Gamble_Info_Table[2] = { x = 1915.6, y = 1374.8, z = 18.5, o = 3.4, lackyIndex = 1, multiple = 1, anim = "roll_02", time = 6.0};
	G_Gamble_Info_Table[3] = { x = 1912.1, y = 1375.6, z = 18.5, o = 3.3, lackyIndex = 3, multiple = 1, anim = "roll_03", time = 5.333};
	G_Gamble_Info_Table[4] = { x =1909.2, y = 1377.0, z = 18.5, o = 3.7, lackyIndex = 1, multiple = 5, anim = "roll_04", time = 6.0 };
	G_Gamble_Info_Table[5] = { x =1906.0, y = 1379.4, z = 18.5, o = 3.8, lackyIndex = 6, multiple = 20, anim = "roll_05", time = 5.333 };
	G_Gamble_Info_Table[6] = { x =1903.8, y = 1382.3, z = 18.5, o = 3.9, lackyIndex = 4, multiple = 2, anim = "roll_06", time = 5.667 };
	G_Gamble_Info_Table[7] = { x =1901.6, y = 1385.2, z = 18.5, o = 4.1, lackyIndex = 2, multiple = 10, anim = "roll_07", time = 7.833 };
	G_Gamble_Info_Table[8] = { x =1899.9, y = 1388.6, z = 18.5, o = 4.5, lackyIndex = 1, multiple = 2, anim = "roll_08", time = 6.467 };
	G_Gamble_Info_Table[9] = { x =1898.4, y = 1391.7, z = 18.5, o = 4.5, lackyIndex = 6, multiple = 10, anim = "roll_09", time = 5.933 };
	G_Gamble_Info_Table[10] = { x =1898.3, y = 1395.7, z = 18.5, o = 4.8, lackyIndex = 6, multiple = 5, anim = "roll_10", time = 5.933 };
	G_Gamble_Info_Table[11] = { x =1898.5, y = 1399.4, z = 18.5, o = 4.9, lackyIndex = 4, multiple = 5, anim = "roll_11", time = 6.0 };
	G_Gamble_Info_Table[12] = { x =1899.5, y = 1402.9, z = 18.5, o = 5.0, lackyIndex = 5, multiple = 10, anim = "roll_12", time = 6.667 };
	G_Gamble_Info_Table[13] = { x =1901.4, y = 1406.2, z = 18.5, o = 5.3, lackyIndex = 3, multiple = 2, anim = "roll_13", time = 6.667 };
	G_Gamble_Info_Table[14] = { x =1903.5, y = 1409.3, z = 18.5, o = 5.4, lackyIndex = 4, multiple = 10, anim = "roll_14", time = 6.667 };
	G_Gamble_Info_Table[15] = { x =1906.0, y = 1411.6, z = 18.5, o = 5.6, lackyIndex = 2, multiple = 5, anim = "roll_15", time = 6.667 };
	G_Gamble_Info_Table[16] = { x =1909.2, y = 1413.9, z = 18.5, o = 5.8, lackyIndex = 5, multiple = 5, anim = "roll_16", time = 7.0 };
	G_Gamble_Info_Table[17] = { x =1912.3, y = 1415.5, z = 18.5, o = 5.8, lackyIndex = 5, multiple = 2, anim = "roll_17", time = 7.0 };
	G_Gamble_Info_Table[18] = { x =1915.9, y = 1416.3, z = 18.5, o = 6.0, lackyIndex = 2, multiple = 2, anim = "roll_18", time = 6.333 };
	G_Gamble_Info_Table[19] = { x =1919.7, y = 1416.8, z = 18.5, o = 0.0, lackyIndex = 6, multiple = 2, anim = "roll_19", time = 6.333 };
	G_Gamble_Info_Table[20] = { x =1923.0, y = 1416.6, z = 18.5, o = 0.1, lackyIndex = 5, multiple = 50, anim = "roll_20", time = 6.667 };
	G_Gamble_Info_Table[21] = { x =1926.7, y = 1415.5, z = 18.5, o = 0.3, lackyIndex = 3, multiple = 50, anim = "roll_21", time = 7.333 };
	G_Gamble_Info_Table[22] = { x =1930.0, y = 1413.8, z = 18.5, o = 0.6, lackyIndex = 2, multiple = 50, anim = "roll_22", time = 7.333 };
	G_Gamble_Info_Table[23] = { x =1933.1, y = 1411.7, z = 18.5, o = 0.7, lackyIndex = 6, multiple = 50, anim = "roll_23", time = 7.333 };
	G_Gamble_Info_Table[24] = { x =1935.7, y = 1409.4, z = 18.5, o = 0.9, lackyIndex = 3, multiple = 20, anim = "roll_24", time = 7.333 };
	G_Gamble_Info_Table[25] = { x =1937.7, y = 1406.1, z = 18.5, o = 1.0, lackyIndex = 4, multiple = 20, anim = "roll_25", time = 7.667 };
	G_Gamble_Info_Table[26] = { x =1939.1, y = 1402.8, z = 18.5, o = 1.0, lackyIndex = 1, multiple = 50, anim = "roll_26", time = 7.333 };
	G_Gamble_Info_Table[27] = { x =1940.1, y = 1399.2, z = 18.5, o = 1.4, lackyIndex = 4, multiple = 50, anim = "roll_27", time = 7.667 };
	G_Gamble_Info_Table[28] = { x =1940.5, y = 1395.5, z = 18.5, o = 1.5, lackyIndex = 1, multiple = 20, anim = "roll_28", time = 8.0 };
	G_Gamble_Info_Table[29] = { x =1940.0, y = 1392.0, z = 18.5, o = 1.8, lackyIndex = 1, multiple = 10, anim = "roll_29", time = 8.0 };
	G_Gamble_Info_Table[30] = { x =1939.3, y = 1388.5, z = 18.5, o = 2.0, lackyIndex = 3, multiple = 10, anim = "roll_30", time = 8.333 };
	G_Gamble_Info_Table[31] = { x =1937.6, y = 1385.2, z = 18.5, o = 2.1, lackyIndex = 2, multiple = 20, anim = "roll_31", time = 9.667 };
	G_Gamble_Info_Table[32] = { x =1935.8, y = 1382.1, z = 18.5, o = 2.3, lackyIndex = 3, multiple = 5, anim = "roll_32", time = 10.0 };
	G_Gamble_Info_Table[33] = { x =1933.3, y = 1379.3, z = 18.5, o = 2.5, lackyIndex = 6, multiple = 1, anim = "roll_33", time = 9.667 };
	G_Gamble_Info_Table[34] = { x =1930.1, y = 1377.4, z = 18.5, o = 2.7, lackyIndex = 2, multiple = 1, anim = "roll_34", time = 10.0 };
	G_Gamble_Info_Table[35] = { x =1926.7, y = 1375.9, z = 18.5, o = 2.8, lackyIndex = 5, multiple = 1, anim = "roll_35", time = 10.0 };
	G_Gamble_Info_Table[36] = { x =1923.3, y = 1374.8, z = 18.5, o = 2.9, lackyIndex = 5, multiple = 20, anim = "roll_36", time = 10.667 };

	G_Pointer_Anim_Table[1] =  {anim = "rotate", time = 1.0};


end

function OnPlayerLeaveInstance( PlayerID )
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Kaixin_Buf_Table[G_Coin_NPC_Table[1]]);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Kaixin_Buf_Table[G_Coin_NPC_Table[2]]);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Kaixin_Buf_Table[G_Coin_NPC_Table[3]]);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Kaixin_Buf_Table[G_Coin_NPC_Table[4]]);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Kaixin_Buf_Table[G_Coin_NPC_Table[5]]);
	RaidInstance:RemoveBuffFromPlayer(PlayerID, G_Kaixin_Buf_Table[G_Coin_NPC_Table[6]]);
end
function F_AddStake(NPC_ID, GossipIndex, PlayerID)
	local StakeKaixinCount = 0;
	if G_NPC_Stake_GossipIndex_Table[NPC_ID] ~= nil
	then
		if  G_NPC_Stake_GossipIndex_Table[NPC_ID].Stake_Gossip1 == GossipIndex
		then
			StakeKaixinCount = G_AddKaixinStake.Stake1;
		end

		if  G_NPC_Stake_GossipIndex_Table[NPC_ID].Stake_Gossip2 == GossipIndex
		then
			StakeKaixinCount = G_AddKaixinStake.Stake2;
		end

		if  G_NPC_Stake_GossipIndex_Table[NPC_ID].Stake_Gossip3 == GossipIndex
		then
			StakeKaixinCount = G_AddKaixinStake.Stake3;
		end

		if  G_NPC_Stake_GossipIndex_Table[NPC_ID].Stake_Gossip4 == GossipIndex
		then
			StakeKaixinCount = G_AddKaixinStake.Stake4;
		end

		if StakeKaixinCount > 0 and  RaidInstance:ConsumeCurrency( PlayerID, 0, 56, StakeKaixinCount ) == 1
		then
			-- add player stake data
			if G_Player_Stake_Table[PlayerID] ~= nil
			then
				if G_Player_Stake_Table[PlayerID].Stake[NPC_ID] ~= nil
				then
					G_Player_Stake_Table[PlayerID].Stake[NPC_ID] = G_Player_Stake_Table[PlayerID].Stake[NPC_ID] + StakeKaixinCount;
				else
					G_Player_Stake_Table[PlayerID].Stake[NPC_ID] = StakeKaixinCount;
				end

			else
				G_Player_Stake_Table[PlayerID] = {Stake = {}}
				G_Player_Stake_Table[PlayerID].Stake[NPC_ID] = StakeKaixinCount;
			end


			-- add buf to player and talk to player
			RaidInstance:AddBuffToPlayer(PlayerID, G_Kaixin_Buf_Table[NPC_ID]);
			RaidInstance:CreatureWhisper( NPC_ID, PlayerID, "你太有眼光了,知道我会赢！谢谢你的"..RaidInstance:ConvertInteger2String(StakeKaixinCount).."开心币!" );


			-- 如果没有准备启动
			if G_EventBegin == 0
			then
				G_EventBegin = RaidInstance:CreateEvent(10000, 3);
			end

		else
			RaidInstance:CreatureWhisper( NPC_ID, PlayerID, "您的开心币不够啊！" );
		end
	end
end
function OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )

	if NPC_ID == G_Coin_NPC_Table[1] or NPC_ID == G_Coin_NPC_Table[2] or NPC_ID == G_Coin_NPC_Table[3] or NPC_ID == G_Coin_NPC_Table[4] or NPC_ID == G_Coin_NPC_Table[5] or NPC_ID == G_Coin_NPC_Table[6]
	then
		if G_CanAddKaixin == 1
		then
			F_AddStake(NPC_ID, GossipIndex, PlayerID);
		else
			RaidInstance:CreatureWhisper( NPC_ID, PlayerID, "大转盘已经启动!下次再投票给我吧！" );
		end
	end

end

function OnEvent( EventID, Attach, Attach2, Attach3, Attach4, Attach5 )

	if EventID == G_EventBegin
	then
		F_StartNotice();
	end

	if EventID == G_BeginRunEvent
	then
		-- 开始启动 计算结果
		G_BeginRunEvent = 0;
		G_RunMachine = 1;
		RaidInstance:PlaySound(G_Pointer, "HappyBear/YaHoo.wav" );
		F_Random_Result();
		G_RuningEvent = RaidInstance:CreateEvent( 100, 1 );
		G_PublicAnimCount =  LuaRandom(6,  20);

		G_CurYanHuaIndex = 1;
 		G_YanHuaEvent = RaidInstance:CreateEvent( 200, 1 );
	end
	if G_RuningEvent == EventID
	then
		--启动客户端表现方式
		F_RunMachine();
	end
	if EventID == G_EndMachine
	then
		F_EndMachine();
		G_EndMachine = 0;
	end

	if EventID == G_YanHuaEvent
	then
		if G_RunMachine
		then
			RaidInstance:SceneEffect( "PlayFromServer/Raid_72/EA0003.nif",G_YanHuaPos[G_CurYanHuaIndex].x,G_YanHuaPos[G_CurYanHuaIndex].y,G_YanHuaPos[G_CurYanHuaIndex].z, 1, 1.0, "", 2000 );
			RaidInstance:SceneEffect( "PlayFromServer/Raid_72/Effect_lihua.nif",G_YanHuaPos[G_CurYanHuaIndex].x,G_YanHuaPos[G_CurYanHuaIndex].y,G_YanHuaPos[G_CurYanHuaIndex].z, 1, 1.0, "", 2000 );
			G_CurYanHuaIndex = G_CurYanHuaIndex +1;
			G_YanHuaEvent = RaidInstance:CreateEvent( 400, 1 );
			if G_CurYanHuaIndex > 6
			then
				G_CurYanHuaIndex = 1;
			end
		else
			G_CurYanHuaIndex= 0 ;
			G_YanHuaEvent = 0;
		end

	end
end
function F_EndMachine()
	local WinIndex = G_Gamble_Info_Table[G_LackResult].lackyIndex;
	local WinMultiple = G_Gamble_Info_Table[G_LackResult].multiple;

	RaidInstance:SceneEffect( "PlayFromServer/Raid_72/win.nif",  G_Gamble_Info_Table[G_LackResult].x,  G_Gamble_Info_Table[G_LackResult].y,  G_Gamble_Info_Table[G_LackResult].z + 2.0, 4, 5.0, "", 2000 );
	RaidInstance:SceneEffect( "PlayFromServer/Raid_72/Effect_lihua.nif",  G_Gamble_Info_Table[G_LackResult].x,  G_Gamble_Info_Table[G_LackResult].y,  G_Gamble_Info_Table[G_LackResult].z, 4, 1.0, "", 2000 );
	--RaidInstance:SceneEffect( "PlayFromServer/Raid_72/EA0003.nif",  G_Gamble_Info_Table[G_LackResult].x,  G_Gamble_Info_Table[G_LackResult].y,  G_Gamble_Info_Table[G_LackResult].z, 4, 5, "", 2000 );

	RaidInstance:SendSystemNotify("本期幸运小可爱:"..G_Coin_Npc_Name_Table[WinIndex].."--倍数："..RaidInstance:ConvertInteger2String(WinMultiple));

	for k ,v in pairs( G_Player_Stake_Table)
	do
		if v ~= nil
			then
				if v.Stake ~= nil and v.Stake[G_Coin_NPC_Table[WinIndex]] ~= nil
				then

					local T_PlayerName = RaidInstance:GetPlayerName(k);
					local KaixinCount = v.Stake[G_Coin_NPC_Table[WinIndex]] * WinMultiple;
					RaidInstance:SendSystemNotify(T_PlayerName.."赢得了"..RaidInstance:ConvertInteger2String(KaixinCount).."开心币!" );
					RaidInstance:ItemReward( k, 56, KaixinCount);
					RaidInstance:ItemReward( k, G_Reward_ItemID, 1);
					G_SysWin = G_SysWin - KaixinCount;
					RaidInstance:PlaySound( G_Pointer, "HappyBear/Congratulations.wav", 0, 0, k );
					RaidInstance:AddBuffToPlayer( k, 127 );

				end

				G_Player_Stake_Table[k] = nil;
			end

			RaidInstance:RemoveBuffFromPlayer(k, G_Kaixin_Buf_Table[G_Coin_NPC_Table[1]]);
			RaidInstance:RemoveBuffFromPlayer(k, G_Kaixin_Buf_Table[G_Coin_NPC_Table[2]]);
			RaidInstance:RemoveBuffFromPlayer(k, G_Kaixin_Buf_Table[G_Coin_NPC_Table[3]]);
			RaidInstance:RemoveBuffFromPlayer(k, G_Kaixin_Buf_Table[G_Coin_NPC_Table[4]]);
			RaidInstance:RemoveBuffFromPlayer(k, G_Kaixin_Buf_Table[G_Coin_NPC_Table[5]]);
			RaidInstance:RemoveBuffFromPlayer(k, G_Kaixin_Buf_Table[G_Coin_NPC_Table[6]]);
	end


--	RaidInstance:SendSystemNotify( "..........................................." );
	RaidInstance:ModifyObjectScale( G_Coin_NPC_Table[WinIndex], 4.0 );
	RaidInstance:CreatureForcePlayAnimation( G_Coin_NPC_Table[WinIndex], 0 );


	G_CanAddKaixin = 1;
    G_EventBegin = 0;
    G_EventBeginCount = 0;
    G_BeginRunEvent = 0;
	G_RunMachine = 0;
	G_RuningEvent = 0;
 	G_EndMachine = 0;
	G_PublicAnimCount = 0;
	G_LackResult = 0;
	G_CurYanHuaIndex = 0;
 	G_YanHuaEvent = 0;

	RaidInstance:SendSystemNotify( "大转盘接受投票啦" );
end
function F_RunMachine()

	if G_PublicAnimCount > 1
	then
		RaidInstance:CreatureForcePlayAnimationByName( G_Pointer,  G_Pointer_Anim_Table[1].anim);
	else
		if G_PublicAnimCount == 1
		then
			RaidInstance:CreatureForcePlayAnimationByName( G_Pointer,  G_Gamble_Info_Table[G_LackResult].anim);
		end
	end

	G_PublicAnimCount = G_PublicAnimCount - 1;

	if G_PublicAnimCount > 0
	then
		G_RuningEvent = RaidInstance:CreateEvent( G_Pointer_Anim_Table[1].time * 1000.0 , 1 );
	else
		G_RuningEvent = 0;
		G_EndMachine = 	RaidInstance:CreateEvent( G_Gamble_Info_Table[G_LackResult].time * 1000.0 , 1 );

	end

end
-- random the result
function F_Random_Result()

	local Stake_Count_Table = {};
	Stake_Count_Table[1] = 0;
	Stake_Count_Table[2] = 0;
	Stake_Count_Table[3] = 0;
	Stake_Count_Table[4] = 0;
	Stake_Count_Table[5] = 0;
	Stake_Count_Table[6] = 0;
	local Moneny_Get_This = 0;

	-- 计算总下注

	for k ,v in pairs( G_Player_Stake_Table)
	do
		if v ~= nil and v.Stake ~= nil
		then
			for j = 1, 6,1
			do
				if v.Stake[G_Coin_NPC_Table[j]] ~= nil
				then
					Stake_Count_Table[j] = Stake_Count_Table[j] + v.Stake[G_Coin_NPC_Table[j]];
					Moneny_Get_This = Moneny_Get_This + v.Stake[G_Coin_NPC_Table[j]];
				end
			end
		end
	end


	-- 计算本轮每个动物不同倍数系统需要返还的开心币

	local TSysmoney1 = 0; --系统输钱
	local TSysmoney3 = 0; --系统赢钱

	local TSysCount1 = 0;
	local TSysCount2 = 0;

	for i = 1, 36, 1
	do
		G_Sys_OutMoney_Table[i] = G_Gamble_Info_Table[i].multiple * Stake_Count_Table[G_Gamble_Info_Table[i].lackyIndex] ;

		if G_Sys_OutMoney_Table[i] - Moneny_Get_This == 0
		then
			TSysCount2 = TSysCount2 + 1;
		else
			if G_Sys_OutMoney_Table[i] - Moneny_Get_This > 0
			then
				TSysmoney1 = TSysmoney1 + G_Sys_OutMoney_Table[i] - Moneny_Get_This;
				TSysCount1 = TSysCount1 + 1;
			else
				TSysmoney3 = TSysmoney3 + Moneny_Get_This - G_Sys_OutMoney_Table[i];
			end
		end
	end

	local SysWinPercentage =  G_SysWin / G_SysGet * 100.0; -- 系统当前赢钱的百分比
	if G_SysWin + 10000  <  0 -- 系统输了10000 必须赢钱
	then
		F_GetRandom_Result(0, Moneny_Get_This, TSysmoney1, TSysmoney3,TSysCount1, TSysCount2);	--本轮系统赢钱
	else
		if SysWinPercentage < -50 and G_SysGet > 10000
		then
			F_GetRandom_Result(0,Moneny_Get_This, TSysmoney1, TSysmoney3,TSysCount1, TSysCount2); --本轮系统赢钱
		else
			if SysWinPercentage > (G_f_SysBack * 100.0) and G_SysGet > 20000
			then
				F_GetRandom_Result(2, Moneny_Get_This, TSysmoney1, TSysmoney3,TSysCount1, TSysCount2); -- 本轮系统输钱
			else
				F_GetRandom_Result(1, Moneny_Get_This, TSysmoney1, TSysmoney3,TSysCount1, TSysCount2); -- 正常输赢
			end
		end
	end


	G_SysGet = G_SysGet + Moneny_Get_This;

end
-- 根据系统需要进行系统随即选择
function F_GetRandom_Result(win, MoneyGet, money_table1, money_table3, c_table1, c_table2)

	local MaxCount = 0;
	for i = 1, 36, 1
	do
		local PercentageNum = 0;

		-- 系统赢钱
		if win == 0
		then
			if G_Sys_OutMoney_Table[i] - MoneyGet == 0
			then
				if c_table2 ~= 0
				then
					PercentageNum = G_f_SysPIN / c_table2;
				end
			else
				if  G_Sys_OutMoney_Table[i] - MoneyGet < 0
				then

					if c_table2 ~= 0
					then
						PercentageNum =  (MoneyGet - G_Sys_OutMoney_Table[i] ) * (G_f_Father - G_f_SysPIN) / money_table3;
					else
						PercentageNum =  (MoneyGet - G_Sys_OutMoney_Table[i] ) * G_f_Father / money_table3;
					end

				end
			end
		end

		-- 系统输钱
		if win == 2
		then
			if G_Sys_OutMoney_Table[i] - MoneyGet == 0
			then
				if c_table2 ~= 0
				then
					PercentageNum = G_f_SysPIN / c_table2;
				end
			else
				if  G_Sys_OutMoney_Table[i] - MoneyGet > 0
				then

					local Tem = G_f_Father - G_f_SysPIN ;
					if c_table2 == 0
					then
						Tem = G_f_Father;
					end

					if c_table1 - 1 > 0
					then
						PercentageNum = (Tem - (G_Sys_OutMoney_Table[i] - MoneyGet) * Tem / money_table1)  / (c_table1 - 1) ;
					else
						PercentageNum =  Tem;
					end

				end
			end
		end

		--正常情况
		if win == 1
		then
			if G_Sys_OutMoney_Table[i] - MoneyGet == 0
			then
				if c_table2 ~= 0
				then
					PercentageNum = G_f_SysPIN / c_table2;
				end
			else

				if  G_Sys_OutMoney_Table[i] - MoneyGet > 0
				then

					local Tem = G_f_Father - G_f_SysPIN - G_f_SysWin;

					if c_table1 - 1 > 0
					then
						PercentageNum = (Tem - (G_Sys_OutMoney_Table[i] - MoneyGet) * Tem / money_table1)  / (c_table1 - 1) ;
					else
						PercentageNum = Tem ;
					end
				else
					if c_table2 ~= 0
					then
						PercentageNum = (MoneyGet - G_Sys_OutMoney_Table[i] ) * G_f_SysWin / money_table3;
					else
						PercentageNum = (MoneyGet - G_Sys_OutMoney_Table[i] ) * (G_f_SysWin + G_f_SysPIN)/ money_table3;
					end
				end

			end
		end
		MaxCount = MaxCount + PercentageNum;
		G_Sys_OutMoney_Table[i] = PercentageNum;
	end

	--RaidInstance:SendSystemNotify("MaxCount / win : "..RaidInstance:ConvertInteger2String( MaxCount) .." / " ..RaidInstance:ConvertInteger2String( win));

	local RandNum = LuaRandom(1, MaxCount );

	for i = 1, 36, 1
	do
		if G_Sys_OutMoney_Table[i] > 0
  		then
  			RandNum = RandNum -  G_Sys_OutMoney_Table[i] ;
  		end

		if RandNum < 0
  		then
  			G_LackResult = i;
			--RaidInstance:SendSystemNotify("G_LackResult: "..RaidInstance:ConvertInteger2String(G_LackResult));
  			return ;
  		end
	end
	RaidInstance:SendSystemNotify( "eeeeeeeeeerrrrrrrrrrrooooorrrrrrr" );
	G_LackResult = 1;
end
-- start notice
function F_StartNotice()
	if G_EventBeginCount == 0
		then
			RaidInstance:SendSystemNotify( "大转盘30秒后开始!赶快来投票啊" );
			G_EventBeginCount = 12;
		elseif G_EventBeginCount == 12
		then
			RaidInstance:SendSystemNotify("大转盘20秒后开始!赶快来投票啊" );
			G_EventBeginCount = 11;
		elseif G_EventBeginCount == 11
		then
			RaidInstance:SendSystemNotify( "大转盘10秒后开始!赶快来投票啊" );
			G_EventBeginCount = 10;
			G_EventBegin = RaidInstance:CreateEvent(1000, 10);
		elseif G_EventBeginCount == 10
		then
			RaidInstance:SendSystemNotify( "大转盘9秒后开始!赶快来投票啊" );
			G_EventBeginCount = 9;
		elseif 	G_EventBeginCount == 9
		then
			RaidInstance:SendSystemNotify( "大转盘8秒后开始!赶快来投票啊" );
			G_EventBeginCount = 8;
		elseif G_EventBeginCount == 8
		then
			RaidInstance:SendSystemNotify("大转盘7秒后开始!赶快来投票啊" );
			G_EventBeginCount = 7;
		elseif G_EventBeginCount == 7
		then
			RaidInstance:SendSystemNotify( "大转盘6秒后开始!赶快来投票啊" );
			G_EventBeginCount = 6;
		elseif G_EventBeginCount == 6
		then
			RaidInstance:SendSystemNotify( "大转盘5秒后开始,停止投票" );
			RaidInstance:PlaySound( G_Pointer, "HappyBear/Five.wav" );
			G_EventBeginCount = 5;
			G_CanAddKaixin = 0;
		elseif G_EventBeginCount == 5
		then
			RaidInstance:SendSystemNotify( "大转盘4秒后开始,停止投票" );
			RaidInstance:PlaySound( G_Pointer, "HappyBear/Four.wav" );
			G_EventBeginCount = 4;

		elseif G_EventBeginCount == 4
		then
			RaidInstance:SendSystemNotify( "大转盘3秒后开始,停止投票" );
			RaidInstance:PlaySound( G_Pointer, "HappyBear/Three.wav" );
			G_EventBeginCount = 3;

		elseif G_EventBeginCount == 3
		then
			RaidInstance:SendSystemNotify( "大转盘2秒后开始,停止投票" );
			RaidInstance:PlaySound( G_Pointer, "HappyBear/Two.wav" );
			G_EventBeginCount = 2;

		elseif G_EventBeginCount == 2
		then
			RaidInstance:SendSystemNotify( "大转盘1秒后开始,停止投票" );
			RaidInstance:PlaySound( G_Pointer, "HappyBear/One.wav" );
			G_EventBeginCount = 1;

		elseif G_EventBeginCount == 1
		then
			RaidInstance:SendSystemNotify( "大转盘启动啦" );
			RaidInstance:PlaySound( G_Pointer, "HappyBear/Ready2Dance.ogg" );
			RaidInstance:KillEvent( L_EventBegin );

			G_EventBegin = 0;
			G_CanAddKaixin = 0;
			G_BeginRunEvent = RaidInstance:CreateEvent( 1000, 1 );
			G_EventBeginCount = 0;

			RaidInstance:ModifyObjectScale( G_Coin_NPC_Table[1], 3.0 );
			RaidInstance:ModifyObjectScale( G_Coin_NPC_Table[2], 3.0 );
			RaidInstance:ModifyObjectScale( G_Coin_NPC_Table[3], 3.0 );
			RaidInstance:ModifyObjectScale( G_Coin_NPC_Table[4], 3.0 );
			RaidInstance:ModifyObjectScale( G_Coin_NPC_Table[5], 3.0 );
			RaidInstance:ModifyObjectScale( G_Coin_NPC_Table[6], 3.0 );
		end
end
