-- Raid instance lua sample
-- float value must be written like this: 1200.0
-- integer value must be written like this: 10
-- RaidInstance must be catched

local RaidInstance;	-- raid instance object

local L_ObjectList = {};
local L_EventList = {};
local L_CastleList = {};

local ObjectType = {Npc = 1, soldier = 2, Tower = 3, Player = 4};
local CampType = {A = 10000, B = 20000};

local CEvent = {
				New = function(self, DelayTime, count, attach, Func)
				    local Object = {};
				    setmetatable(Object, {__index = self});
				    RaidInstance:SendSystemNotify( "1" );
				    Object.ID = RaidInstance:CreateEvent( DelayTime, count, attach );
				    RaidInstance:SendSystemNotify( "2" );
					Object.Count = count;
					Object.CallBack = Func;
					Object.EndCall = nil;
				    L_EventList[Object.ID] = Object;
				    local Ret = L_EventList[Object.ID];
					return Ret;
				 end,

				DoEvent = function(self, Param0, Param1, Param2, Param3, Param4)
				    if self.CallBack ~= nil
				    then
				        self.CallBack(Param0, Param1, Param2, Param3, Param4);
				    end

				    self.Count = self.Count - 1;
				    if self.Count <= 0
				    then
				        if self.EndCall ~= nil
				        then
					        self.EndCall();
				        end
				        RaidInstance:KillEvent(self.ID);
					    L_EventList[self.ID] = nil;
				    end
				end,

				ID = 0,
				Count = 0,
				CallBack = nil,
				EndCall = nil,
				};


local CNpc = {
			  New = function(self, tableID)
				   local Object = {};
				   setmetatable(Object, {__index = self});
				   Object.ID = RaidInstance:SpawnCreature( tableID, 0.0, 0.0, 0.0, 3.0, 0);
				   L_ObjectList[Object.ID] = Object;
				   local Ret = L_ObjectList[Object.ID];
				   return Ret;
			  end,

			  OnGossip = function(player)
			  end,

			  SetPosition = function(self, x, y, z, angle)
			      local z = RaidInstance:GetLandHeight( x, y, z );
			      RaidInstance:TeleportCreature( self.ID, x, y, z, angle);
			  end,

			  AddGossip = function(self, str, Call)
			      local ID = RaidInstance:InsertNPCGossip( self.ID, str);
				  self.GossipList[ID] = {};
				  self.GossipList[ID].CallBack = Call;
			  end,

			  DoGossip = function(self, Index, player)
			      if self.GossipList[Index].CallBack ~= nil
				  then
			          self.GossipList[Index].CallBack(self, player);
				  end
			  end,

			  Type = function()
			      return ObjectType.Npc;
			  end,

			  ID = 0,
			  GossipList = {},
			  };


local CSoldiers = {
				New = function(self, tableID, x, y, z, angle)
				   local Object = {};
				   setmetatable(Object, {__index = self});
				   z = RaidInstance:GetLandHeight( x, y, z );
				   Object.ID = RaidInstance:SpawnCreature( tableID, x, y, z, angle, 0);
				   L_ObjectList[Object.ID] = Object;
				   local Ret = L_ObjectList[Object.ID];
				   return Ret;
				end,

				SetFaction = function( self, faction )
				    RaidInstance:ModifyObjectFaction(self.ID, faction);
				end,

				Type = function()
			        return ObjectType.soldier;
			    end,

				ID = 0,
				};

local CTower = {
                New = function(self, tableID)
				   local Object = {};
				   setmetatable(Object, {__index = self});
				   Object.ID = RaidInstance:SpawnCreature( tableID, 0, 0, 0, 0, 0);
				   L_ObjectList[Object.ID] = Object;
				   local Ret = L_ObjectList[Object.ID];
				   return Ret;
				end,

			    SetPosition = function(self, x, y, z, angle)
			        local z = RaidInstance:GetLandHeight( x, y, z );
			        RaidInstance:TeleportCreature( self.ID, x, y, z, angle);
			    end,

			    SetFaction = function( self, faction )
				    RaidInstance:ModifyObjectFaction(self.ID, faction);
			    end,

			    Type = function()
				    return ObjectType.Tower;
			    end,

			    ID = 0,
                };

local CPlayer = {
                 New = function(self, ID)
				   local Object = {};
				   setmetatable(Object, {__index = self});
				   Object.ID = ID;
				   L_ObjectList[Object.ID] = Object;
				   local Ret = L_ObjectList[Object.ID];
				   return Ret;
				 end,

			    SetPosition = function(self, x, y, z, angle)
			        local z = RaidInstance:GetLandHeight( x, y, z );
			        RaidInstance:TeleportPlayer( self.ID, x, y, z, angle);
			    end,

				SetCamp = function(self, CampID)
				    RaidInstance:ModifyObjectFaction(self.ID, CampID);
				end,

			    Type = function()
				    return ObjectType.Player;
			    end,

				Leave = function(self)
				    L_ObjectList[self.ID] = nil;
				end,

				ID = 0,
				Faction = 0,
                 };

local CCastle = {
                 New = function(self, id)
				   local Object = {};
				   setmetatable(Object, {__index = self});
				   Object.ID = id;
				   Object.CampID = id;
				   L_CastleList[id] = Object;
				   local Ret = L_CastleList[id];
				   return Ret;
				 end,

				 PunchSoldier = function(self, Call)
				     RaidInstance:SendSystemNotify( "0" );
				     CEvent:New(10000, 100000, self.ID, Call);
				 end,

				 BuidTower = function(self, tableID)
				     local Tower = CTower:New(tableID);
				     return Tower;
				 end,

				 ID = 0,
				 Name = "",
				 PlayerBornPos0 = {},
				 PlayerBornPos1 = {},
				 PlayerBornPos2 = {},
				 SoldierBorn = {},
				 CampID = 0,
                 };



-------------------------------------------------Event CallBack--------------------------------------------------

function OnPunchSoldier(Param0, Param1, Param2, Param3, Param4 )

    local Castle = L_CastleList[CampType.A];
    RaidInstance:SendSystemNotify("Name:"..Castle.Name);
	if Castle ~= nil
	then
	        local Born = Castle.SoldierBorn0;
	        local Soldier = CSoldiers:New(Born.ID, Born.x, Born.y, Born.z, Born.angle);

	        --Born = Castle.SoldierBorn1;
	        --local Soldier = CSoldiers:New(Born.ID, Born.x, Born.y, Born.z, Born.angle);

	        --Born = Castle.SoldierBorn2;
	        --local Soldier = CSoldiers:New(Born.ID, Born.x, Born.y, Born.z, Born.angle);
	end
end

-----------------------------------------NPC_Gossip------------------------------------------------

function OnSelectCampA(Npc, player)
    local Castle = L_CastleList[CampType.A];
	if Castle ~= nil
	then
        local BornPos = Castle.PlayerBornPos;
        player:SetPosition(BornPos.x, BornPos.y, BornPos.z, BornPos.angle);
	    --player:SetCamp(Castle.CampID);
	end
end

function OnSelectCampB(Npc, player)
    local Castle = L_CastleList[CampType.B];
    local BornPos = Castle.PlayerBornPos;
    player:SetPosition(BornPos.x, BornPos.y, BornPos.z, BornPos.angle);
	--player:SetCamp(Castle.CampID);
end

function OnDebug(playerID)

end

---------------------------------------------------------------------------------------------------


function OnStart( ID )		-- on instance start.
     RaidInstance = LuaGetRaidObject( ID );

	 local CastleA = CCastle:New(CampType.A);
	 CastleA.Name = "CastleA";
	 CastleA.PlayerBornPos = {x = 2329.7, y = 1804.4, z = 39.2, angle = 1.9};
	 CastleA.SoldierBorn0 = {x = 2123.8, y = 1872.6, z = 40.3, angle = 1.1, ID = 200000008};
	 CastleA.SoldierBorn1 = {x = 2125.5, y = 1808.2, z = 39.4, angle = 1.4, ID = 200000002};
	 CastleA.SoldierBorn2 = {x = 2131.4, y = 1729.0, z = 43.9, angle = 1.7, ID = 200000002};
	 CastleA:PunchSoldier(OnPunchSoldier);


	 local CastleB = CCastle:New(CampType.B);
	 CastleB.Name = "CastleB";
	 CastleB.PlayerBornPos = {x = 1654.7, y = 1788.9, z = 25.5, angle = 5.6};
	 CastleB.SoldierBorn0 = {x = 0.0, y = 0.0, z = 0.0, angle = 0.0, ID = 0};
	 CastleB.SoldierBorn1 = {x = 0.0, y = 0.0, z = 0.0, angle = 0.0, ID = 0};
	 CastleB.SoldierBorn2 = {x = 0.0, y = 0.0, z = 0.0, angle = 0.0, ID = 0};
	 local Tower = CastleB:BuidTower(200000001);
	 Tower:SetPosition(1710.1, 1670.0, 47.6, 0);

	 Tower = CastleB:BuidTower(200000001);
	 Tower:SetPosition(1684.1, 1681.2, 47.6, 0);

	 Tower = CastleB:BuidTower(200000001);
	 Tower:SetPosition(1672.2, 1681.2, 47.6, 0);

	 Tower = CastleB:BuidTower(200000001);
	 Tower:SetPosition(1660.1, 1681.2, 47.6, 0);

	 Tower = CastleB:BuidTower(200000001);
	 Tower:SetPosition(1782.8, 1744.7, 47.6, 0);

	 Tower = CastleB:BuidTower(200000001);
	 Tower:SetPosition(1769.4, 1767.3, 47.6, 0);

	 Tower = CastleB:BuidTower(200000001);
	 Tower:SetPosition(1769.4, 1782.3, 47.6, 0);

	 Tower = CastleB:BuidTower(200000001);
	 Tower:SetPosition(1769.4, 1797.3, 47.6, 0);

	 local Guider = CNpc:New(3083);
	 Guider:SetPosition(2447.5, 1194.9, -0.5, 2.7);
	 Guider:AddGossip("SelectCampA", OnSelectCampA);
	 Guider:AddGossip("SelectCampB", OnSelectCampB);
	 Guider:AddGossip("Debug", OnDebug);

end

function OnUpdate( TimeElapsed )		-- instance update every second.
end

function OnEnd()

end

function OnObjectDie( VictimID, AttackerID )

end

function OnPlayerResurrect( PlayerID )
    local Player = L_ObjectList[PlayerID];
	if Player ~= nil
	then
	    Player:SetPosition();
	end
end

function OnGameObjectTrigger( GameObjectID, PlayerID )
end

function OnUnitEnterCombat( PlayerID )
end

function OnNPCGossipTrigger( NPC_ID, GossipIndex, PlayerID )
    local NPC = L_ObjectList[NPC_ID];
	if NPC == nil
	then
	    return;
	end;

	local Player = L_ObjectList[PlayerID];
	NPC:DoGossip( GossipIndex, Player);
end

function OnRaidWipe()
end

function OnPlayerEnterInstance( PlayerID )
    local Player = CPlayer:New(PlayerID);
	Player:SetPosition(2449.4, 1202.2, -0.7, 0.6);
end

function OnPlayerLeaveInstance( PlayerID )
    local Player = L_ObjectList[PlayerID];
	Player:Leave();
end

function OnPlayerCastSpell( SpellID, PlayerID )
end

function OnEvent( EventID, Param0, Param1, Param2, Param3, Param4 )
    if L_EventList[EventID] == nil
	then
	    return;
	end

	local Event = L_EventList[EventID];
	Event:DoEvent(Param0, Param1, Param2, Param3, Param4 );
end
