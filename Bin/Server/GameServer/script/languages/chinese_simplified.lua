function build_string_0()
	return "战斗开始了!";
end

function build_string_1()
	return "对方无人,你将在30秒后退出竞技场,抓紧时间拾取宝箱里的奖励吧!";
end

function build_string_2()
	return "你在竞技场的战斗中胜利,将在30秒内被传送出副本,抓紧时间拾取宝箱里的奖励吧!";
end

function build_string_3()
	return "你在竞技场的战斗中被击败，将在30秒内被传送出副本";
end

function build_string_4( sideA, sideB )
	return sideA.."战胜了"..sideB;
end

function build_string_5( who, itemName, refineLevel )
	return "恭喜["..who.."]的"..itemName.."精炼到"..refineLevel.."级!"
end

function build_string_6()
	return "拍卖已到期";
end

function build_string_7()
	return "拍卖成功";
end

function build_string_8()
	return "义拍成功";
end

function build_string_9( itemName, amount )
	return "道具:"..itemName.."<br>感谢您对慈善事业做出的贡献! <br>金额:"..amount.."元宝";
end

function build_string_10( itemName, ownerName, highestBidderName, highestBid, buyoutPrice, taxCut )
	return "道具:"..itemName.."<br>拍卖者:"..ownerName..", 最高竞标者:"..highestBidderName.."<br>最高竞标价格:"..highestBid.."元宝<br>一口价:"..buyoutPrice.."元宝<br>已扣除所得税:"..taxCut.."元宝";
end

function build_string_11( itemName, ownerName, highestBidderName, highestBid, buyoutPrice, taxCut )
	return "道具:"..itemName.."<br>拍卖者:"..ownerName..", 最高竞标者:"..highestBidderName.."<br>最高竞标价格:"..highestBid.."<br>一口价:"..buyoutPrice.."<br>已扣除所得税:"..taxCut;
end

function build_string_12( itemName )
	return "恭喜您成功购买"..itemName..",请尽快查收!";
end

function build_string_13( itemName )
	return "恭喜，您的"..itemName.."在拍卖行被买家收购啦!";
end

function build_string_14()
	return "拍卖已取消";
end

function build_string_15( itemName, feeCut )
	return "道具:"..itemName.."<br>系统惩罚您:"..feeCut.."已扣除";
end

function build_string_16( itemName, amount )
	return "道具:"..itemName.."<br>系统退回您:"..amount;
end

function build_string_17( itemName, feeCut )
	return "道具:"..itemName.."<br>系统惩罚您:"..feeCut.."元宝已扣除";
end

function build_string_18( itemName, amount )
	return "道具:"..itemName.."<br>系统退回您:"..amount.."元宝";
end

function build_string_19()
	return "您的出价太低!"
end

function build_string_20()
	return "当前出价大于一口价,自动设置为一口价购买";
end

function build_string_21()
	return "您当前已经出最高价!";
end

function build_string_22()
	return "竞标失败";
end

function build_string_23( itemName, amount )
	return "道具:"..itemName.."<br>您的竞标价被超过<br>系统退回您:"..amount.."元宝";
end

function build_string_24( itemName, amount )
	return "道具:"..itemName.."<br>您的竞标价被超过<br>系统退回您:"..amount;
end

function build_string_25()
	return "一口价不可以小于竞标价";
end

function build_string_26()
	return "激活码奖励";
end

function build_string_27( serial )
	return "您使用了激活码:"..serial.."获得了以下奖励";
end

function build_string_28()
	return "慈善捐助奖励";
end

function build_string_29( amount )
	return "亲爱的玩家,感谢你的爱心,由于您本次捐助已经超过了"..amount.."元宝。系统奖励您如下道具。希望您再接再厉。";
end

function build_string_30()
	return "慈善捐助奖励";
end

function build_string_31( amount )
	return "亲爱的玩家，感谢你的爱心，由于您累计捐助已经超过了"..amount.."元宝。系统奖励您如下道具。希望您再接再厉。";
end

function build_string_32( playerName, currentDonation, totalDonation )
	return "爱心大使["..playerName.."]捐助了"..currentDonation.."元宝(累计:"..totalDonation..")，获得了系统的奖励!";
end

function build_string_33( playerName, totalDonation )
	return "爱心大使["..playerName.."]累计捐助了"..totalDonation.."元宝，获得了丰厚的系统奖励!";
end

function build_string_34()
	return "系统退回";
end

function build_string_35( drugLevel, drugTime )
	return "您的账号已经纳入防沉迷系统,请尽快去官方网站填写防沉迷资料!当前沉迷等级为"..drugLevel..",当前累积游戏时间为:"..drugTime.."分钟";
end

function build_string_36()
	return "系统礼物";
end

function build_string_37()
	return "恭喜您，得到了系统赠送的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱";
end

function build_string_38( exp )
	return "恭喜您，得到了系统赠送的经验"..exp.."点";
end

function build_string_39( gold )
	return "恭喜您，得到了系统赠送的"..gold;
end

function build_string_40()
	return "战场奖励道具";
end

function build_string_41()
	return "竞技场奖励道具";
end

function build_string_42()
	return "称号奖励道具";
end

function build_string_43()
	return "副本奖励道具";
end

function build_string_44()
	return "制造物品";
end

function build_string_45()
	return "恭喜您，得到了战场奖励的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱";
end

function build_string_46()
	return "恭喜您，得到了竞技场奖励的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱";
end

function build_string_47()
	return "恭喜您，得到了称号奖励的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱";
end

function build_string_48()
	return "恭喜您，得到了副本奖励的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱";
end

function build_string_49( itemName )
	return "您制造了:"..itemName..",由于背包已满，系统将通过邮件发送给您，请查收您的邮箱";
end

function build_string_50()
	return "恭喜您，得到了系统赠送的道具，请查看您的背包";
end

function build_string_51()
	return "恭喜您，得到了战场奖励的道具，请查看您的背包";
end

function build_string_52()
	return "恭喜您，得到了竞技场奖励的道具，请查看您的背包";
end

function build_string_53()
	return "恭喜您，得到了称号奖励的道具，请查看您的背包";
end

function build_string_54()
	return "恭喜您，得到了副本奖励的道具，请查看您的背包";
end

function build_string_55( itemName )
	return "你制造了:"..itemName;
end

function build_string_56()
	return "您的账号被纳入防沉迷系统，累计在线时间已经进入不健康游戏时间，不会获得收益。请及时到网站填写防沉迷信息认证。";
end

function build_string_57()
	return "您的账号被纳入防沉迷系统，累计在线时间已经进入疲劳游戏时间，获得收益将减半。请及时到网站填写防沉迷信息认证。";
end

function build_string_58( currentTime, totalTime, drugLevel )
	return "您本次上线时间:"..currentTime.."分钟,当前累积游戏时间:"..totalTime.."分钟,当前沉迷等级为"..drugLevel.."请尽快去官方网站填写防沉迷资料!";
end

function build_string_59()
	return "恭喜您到达了20级,非常幸运的获得了一个生肖玉佩!";
end

function build_string_60()
	return "短时间内不能连续收徒";
end

function build_string_61()
	return "徒弟等级超过师傅，已自动脱离师徒关系";
end

function build_string_62()
	return "你已被禁言";
end

function build_string_63()
	return "副本内禁用";
end

function build_string_64()
	return "你还没有召唤宠物，不能喂食";
end

function build_string_65()
	return "宠物蛋还未到孵化时间，不能破蛋而出";
end

function build_string_66()
	return "您的宠物数量已经达到上限，不能再孵化了";
end

function build_string_67( Race )
	if Race == 1
	then
		return "妖族";
	elseif Race == 2
	then
		return "人族";
	else
		return "巫族";
	end
end

function build_string_68( Winner, Loser )
	return Winner.."战胜了"..Loser..",您将在60秒后离开战场";
end

function build_string_69()
	return "恭喜您,获得战场的胜利,将在10秒内被传送出副本";
end

function build_string_70()
	return "您在战场中被击败,将在10秒内被传送出副本";
end

function build_string_71( seconds )
	return "服务器将在"..seconds.."秒内关闭";
end

function build_string_72( minutes )
	return "服务器将在"..minutes.."分钟内关闭";
end

function build_string_73()
	return "拍卖行";
end

function build_string_74()
	return "将这里设为你的家";
end

function build_string_75()
	return "银行";
end

function build_string_76()
	return "请输入序列号";
end

function build_string_77()
	return "创建部族";
end

function build_string_78()
	return "查询部族列表";
end

function build_string_79()
	return "升级部族";
end

function build_string_80()
	return "解除师徒关系";
end

function build_string_81()
	return "传送到人族地图";
end

function build_string_82()
	return "传送到巫族地图";
end

function build_string_83()
	return "传送到妖族地图";
end

function build_string_84()
	return "坠星峰";
end

function build_string_85()
	return "试剑台";
end

function build_string_86()
	return "洞穴";
end

function build_string_87()
	return "嘹望台";
end

function build_string_88()
	return "望峰石";
end

function build_string_89()
	return "点我就传送到莎尔娜面前";
end

function build_string_90()
	return "女王，我来帮助你了!";
end

function build_string_91()
	return "我需要更多的灵魂……";
end

function build_string_92()
	return "凡人，止步吧，神的领域不容许受到亵渎!";
end

function build_string_93()
	return "我喜欢灵魂的味道……";
end

function build_string_94()
	return "看哪，这就是幽冥地狱的强大力量，快跑吧凡人们!";
end

function build_string_95()
	return "感受到那寒冷的气息了吗……";
end

function build_string_96()
	return "我知道，这一天终将到来。曾经犯过的错，注定将受到惩罚。但是追求神的力量并没有错，弱小的凡人们，如果你们不能战胜我，迎接你们的将是无尽的痛苦和黑暗……";
end

function build_string_97()
	return "你们真笨~记住,要全神贯注!再来一次吗?";
end

function build_string_98()
	return "做的好!现在跟我来拿礼物吧!";
end

function build_string_99()
	return "跟我来吧,注意模仿我的动作哦~嘿嘿,你会发现我是MJ的忠实粉丝!";
end

function build_string_100()
	return "玩家被全部击败,将在5秒内传送出副本!";
end

function build_string_101()
	return "欢迎来到开心乐园,我是一只来自美丽国度的空军熊汤姆!我的理想是为世界各地的朋友们带来和平与快乐!看到你对面的小骰子了么?骰了它,我就会向前走哦~当我走到第最后一格时,就可以来挑战我了!";
end

function build_string_102()
	return "挑战失败!";
end

function build_string_103()
	return "挑战成功!";
end

function build_string_104( raceName, bannerName )
	return "["..raceName.."]夺取了["..bannerName.."]";
end

function build_string_105( raceName, bannerName )
	return "["..raceName.."]守住了["..bannerName.."]";
end

function build_string_106( raceName, playerName, bannerName )
	return "["..raceName.."]的["..playerName.."]突袭了["..bannerName.."]";
end

function build_string_107()
	return "部族族长";
end

function build_string_108()
	return "部族长老";
end

function build_string_109()
	return "部族队长";
end

function build_string_110()
	return "一般族员";
end

function build_string_111( playerName )
	return "你没有权限设置"..playerName.."的等级";
end

function build_string_112()
	return "没有找到该阶位";
end

function build_string_113()
	return "不能添加非本族成员入部族";
end

function build_string_114()
	return "你没有权利邀请别人加入部族";
end

function build_string_115()
	return "人数已满";
end

function build_string_116()
	return "你的权限不够";
end

function build_string_117()
	return "族长在部族有成员的情况下无法退会";
end

function build_string_118( point )
	return "部族积分必须达到"..point.."才可以升级";
end

function build_string_119()
	return "部族已经达到顶级";
end

function build_string_120( itemName )
	return "部族升级缺少物品:"..itemName;
end

function build_string_121( hostileGuildName )
	return "你的部族正在与"..hostileGuildName.."宣战中,请等待解除宣战后再次宣战,每次宣战将持续24小时";
end

function build_string_122()
	return "不能对自己的部族宣战";
end

function build_string_123()
	return "不能向同盟宣战";
end

function build_string_124()
	return "没有这个部族";
end

function build_string_125()
	return "正在请求结盟，请等待回馈";
end

function build_string_126()
	return "不能和自己的部族结盟";
end

function build_string_127()
	return "不能对与我方交战状态的部族提出结盟请求";
end

function build_string_128()
	return "已经是同盟了";
end

function build_string_129()
	return "部族等级未达到3级";
end

function build_string_130()
	return "你不是部族领袖";
end

function build_string_131()
	return "对方部族领袖不在线";
end

function build_string_132()
	return "邀请和你同盟的部族已经解散";
end

function build_string_133()
	return "邀请已经过期，请让对方重新邀请";
end

function build_string_134( itemName )
	return "道具"..itemName.."已过期";
end

function build_string_135( restMinutes )
	return "您的账号已被封停,剩余时间为"..restMinutes.."分钟!";
end

function build_string_136( expire )
	return "你已被禁言,截止日期"..expire;
end

function build_string_137()
	return "您想要点什么";
end

function build_string_138()
	return "部族人数已满，请升级部族";
end

function build_string_139( itemName, itemCount, OldPrice, NewPrice )
	return "恭喜您享受商城打折活动,"..itemName.."*"..itemCount.."原价:"..OldPrice.."现价:"..NewPrice;
end

function build_string_140( playerName )
	return "玩家:["..playerName.."]正在副本中,或者副本排队中";
end

function build_string_141( playerName, minLevel )
	return "玩家:["..playerName.."]级别不足"..minLevel;
end

function build_string_142( playerName, maxLevel )
	return "玩家:["..playerName.."]级别高于"..maxLevel;
end

function build_string_143( cost )
	return "进入这个副本需要:"..cost.."个元宝";
end

function build_string_144( itemCount, itemName )
	return "您需要:"..itemCount.."个"..itemName.."才能进入这个副本";
end

function build_string_145( gold )
	return "进入这个副本需要:"..gold;
end

function build_string_146( playerName, limit )
	return "玩家:["..playerName.."]今天进入副本已经超过"..limit.."次";
end

function build_string_147( playerName, money )
	return "玩家:["..playerName.."]元宝数不足"..money.."个";
end

function build_string_148( playerName, itemName, itemCount )
	return "玩家:["..playerName.."]的道具["..itemName.."]不足"..itemCount.."个";
end

function build_string_149( playerName, gold )
	return "玩家:["..playerName.."]的金币不足"..gold;
end

function build_string_150( minPlayer )
	return "此副本不可以少于"..minPlayer.."人";
end

function build_string_151( maxPlayer )
	return "此副本不可以超过"..maxPlayer.."人";
end

function build_string_152( yuanbao )
	return "恭喜您，得到了系统赠送的"..yuanbao.."个元宝";
end

function build_string_153( yuanbao )
	return "您得到了"..yuanbao.."个元宝";
end

function build_string_154( yuanbao )
	return "恭喜您,成功从信用卡账户转帐到基础账户"..yuanbao.."个元宝";
end

function build_string_155(guildname, killername, score)
	return "被敌对部族:["..guildname.."]的["..killername.."]击杀，被掠夺部族积分"..score.."分.";
end

function build_string_156(guildname, killname, score)
	return "成功击杀敌对部族:["..guildname.."]的["..killname.."],掠夺部族积分"..score.."分.";
end

function build_string_157()
	return "花5个开心币改变一个造型";
end

function build_string_158()
	return "将100开心币兑换成80元宝";
end

function build_string_159()
	return "将1000开心币兑换成800元宝";
end

function build_string_160()
	return "将5000开心币兑换成4000元宝";
end

function build_string_161()
	return "只有部族族长有权利升级部族";
end

function build_string_162(Level)
	return "部族已经成功升到"..Level.."级";
end

function build_string_163(name)
	return "接受训练师的训练";
end

function build_string_164(name)
	return "接受训练师的训练";
end


function build_string_165(name)
	return "接训练师的训练";
end


function build_string_166(name)
	return "接训练师的训练";
end

function build_string_167( score, point )
	return "您的部族获得"..score.."点积分,您个人获得"..point.."点贡献值";
end

function build_string_168()
	return "暂离";
end

function build_string_169()
	return "勿扰";
end

function build_string_170()
	return "部族奖励";
end

function build_string_171()
	return "恭喜您,得到了部族奖励的道具,由于您的背包已满,系统将通过邮件发送给您,请查收您的邮箱";
end

function build_string_172()
	return "恭喜您,得到了部族奖励的道具,请查看您的背包";
end

function build_string_173()
	return "该部族处于免战中";
end


function build_string_174()
	return "恭喜您,部族进入免战状态";
end

function build_string_175()

	return "部族处于免战中";
end
