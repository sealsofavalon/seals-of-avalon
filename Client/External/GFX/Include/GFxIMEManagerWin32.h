/**********************************************************************

Filename    :   GFxIMEManagerWin32.h
Content     :   Implementation of Input Method Support on Win32 platforms.
Created     :   OCt 4, 2007
Authors     :   Ankur Mohan

Notes       :
History     :

Copyright   :   (c) 1998-2007 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/


#ifndef INC_IMEWIN32_H
#define INC_IMEWIN32_H
// GFx includes
#include "GFile.h"
#include "GImage.h"
#include "GFxPlayer.h"
#include "GFxLoader.h"
#include "GFxLog.h"
#include "GFxRenderConfig.h"
#include "GFxIMEManager.h"
#pragma comment(lib, "GFxIME")

/*class GFxIME
{

};*/



class GFxIMEWin32Impl;

// Public interface for Input Method Editor.

class GFxIMEManagerWin32 : public GFxIMEManager
{
public:

    /* Constructor */
    GFxIMEManagerWin32(HWND hwnd);

    /* Destructor */
    ~GFxIMEManagerWin32();


    /*************************************************/
    // Implementations of GFxIMEManager's methods
    /************************************************/
    virtual void    SetActiveMovie(GFxMovieView*);

    // handles IME events
    virtual UInt    HandleIMEEvent(GFxMovieView* pmovie, const GFxIMEEvent& imeEvent);

    // handle "imecommand(cmd, param)"
    virtual void    IMECommand(GFxMovieView* pmovie, const char* pcommand, const char* parg);

    /* Finalizes IME composition text, closes any pop ups (candidate list, reading window) */
    virtual void    OnFinalize();

    virtual void    OnShutdown();

    /* Override from GFxIMEManaager. Parses path of the argument to detect if it's a candidatelist
    object or not. */
    virtual bool    IsCandidateList(const char* path);

    // Override from GFxIMEManaager. Invoked when candidate list loading is completed.
    virtual void OnCandidateListLoaded(const char* pcandidateListPath);

    /* Override from GFxIMEManaager. Invoked when SetCandidateListStyle is called
    from Action Script */
    virtual void OnCandidateListStyleChanged(const GFxIMECandidateListStyle& style);

    virtual void OnEnableIME(bool enable);

    /*************************************************/
    // Functions for setting IME state
    /************************************************/

    void    Init();

    void    SetWndHandle(HWND hWnd);

    /* Records path to the CandidateList AS object to send invokes */
    void    RecordCandidateListPath(const char* parg);

    /* Records path to the AS object that handles composition string displays, coloring the text,
    loading up the Candidate List swf file etc. */
    void    RecordIMEControllerPath(const char* parg);

    /* Sets properties for the candidate list.
       param1: Font size for cand list text param2: Font color for regular text
       param3: Font color for highlighted row text.  */
    void    SetCandidateListProperties(INT size, INT fontColor1, INT fontColor2);


    // Handles WM_IME_NOTIFY for candidate list related notifications
    LRESULT OnIMECandidateProcessing(UINT message, UInt key, UInt info, bool downFlag);

    // Sets Input Language
    void SetInputLanguage(const char* inputLanguage);

    // Sets Conversion Mode
    void SetConversionMode(const char* convMode);

    /* Handles some WM_NOTIFY messages that get sent when backspace, space, esc keys etc
    are pushed. */
    void    CustomProcessing(UINT message, UInt key, UInt info);


    /*************************************************/
    // Miscellaneous Functions
    /************************************************/
    /* Selects a particular row of the Candidate List (specified by index). Doesn't finalize */
    void    SelectAndClose(int index); // Selects the text in row # and closes Candidate List

    void    FsCallBack(GFxMovieView* pmovie, const char* pcommand, const char* parg);

     /* Records the last keystroke for reading window display */
    void    PreProcessWM_KEYDOWN(const GFxIMEWin32Event& winEvt);

     /* Takes care of interacting with AS to display reading (input) window */
    void    DisplayReadingWindow(wchar_t* pReadingStr);

    /* Displays composition string, keeps track of how many characters are on the screen, cursor
    position etc */
    void    DisplayCompositionString(UINT& numCharDisplay);

    /* Checks if we are in a textfield or not. If not, translate message is not called. This
       is to make TAB work correctly when IME is active and focus is on a non textfield object. */
    bool    CheckIfInTextField();

    // Variables
private:
    // This class implements Input Method Editor functionality.
    class GFxIMEWin32Impl* pIME;

    // This variables keeps track of if the candidate list is loaded or not. If not, 
    // it's used to print a message to GFx log. The message is printed only the first
    // time the list is called into use. The variable is reset every time SetActiveMovie 
    // is called.
    bool                   CandListLoaded;

    enum Version {WinXP, WinVista, Win2K, Unknown};
    // Detects Windows Version
    virtual void    GetWindowsVersion(Version&);


};

#endif //INC_IMEWIN32_H

