#ifndef _RIBBON_H_
#define _RIBBON_H_

#include <vector>

class Ribbon
{
public:
	Ribbon();
	~Ribbon();

	struct SamplePoint
	{
		float fT;
		float x, y, z;
	};

	void Add3DPoint(float fT, float x, float y, float z);
	inline unsigned int GetPointCount() {
		return m_vPoints.size();
	}

	bool Get3DPoints(float fT, float fLife, unsigned int uiNum, float* pfPoint);

	std::vector<SamplePoint> m_vPoints;
};



#endif //_RIBBON_H_