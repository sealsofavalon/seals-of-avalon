---------------------------------------------------------------------------------
-- 位置: $(3dsmax)/scripts/startup/ 启动插件UI 菜单
---------------------------------------------------------------------------------

include "Sunyou/SYMenu.ms"



---------------------------------------------------------------------------------
-- 入口
---------------------------------------------------------------------------------
fn SYPluginMain = 
(
   ver = maxversion()
   if ver[1] < 5100 then
     messagebox "3ds max version 5.1 or above is required to run this script" title:"Sunyou Terrain Plugin"
	
   -- 装载菜单
    SYLoadMenu()   
)

SYPluginMain()
