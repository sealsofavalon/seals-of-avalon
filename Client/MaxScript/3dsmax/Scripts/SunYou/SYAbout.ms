rollout syAboutDialog "关于地形插件" width:375 height:149
(
	bitmap bmpLogo "Logo" pos:[25,23] width:64 height:64 enabled:true fileName:"SunYou/SYLogo.bmp"
	label lbl1 "SunYou Terrain Plugin." pos:[131,35] width:365 height:19
	label lbl2 "Copyright (c) 2007 Sunyou." pos:[126,71] width:371 height:27
	button btnOK "确定" pos:[130,115] width:74 height:23 enabled:true
	on btnOK pressed  do
		DestroyDialog syAboutDialog
)