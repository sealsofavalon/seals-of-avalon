
fn SYExportTerrain = 
(
  	local Str = "Terrain :{\n";
		--WriteString out_Stream Str
		--WriteByte out_Stream 0;
    local terrainObject
    local temp
    
    -- 查找 "sy_terrain" .
    for i = 1 to rootnode.children.count do
        if ( rootnode.children[i].name == "sy_terrain" ) then 
            terrainObject =  rootnode.children[i]	
        else
            terrainObject = undefined
    if terrainObject ==undefined then
    (
      	-- messagebox("场景中不存在地形")
      	Str = "}\n"
      --	WriteString out_Stream Str
      	--WriteByte out_Stream 0
         
    )
   
   local XSize = getuserprop terrainObject "SY_XSize"
   local YSize = getuserprop terrainObject "SY_YSize"
   if(XSize != undefined AND YSize != undefined) then
   (
			 	WriteLong out_Stream Width 
			  
			  -- 遍历行
    		for Y = 0 to Width-1 do 
    		( 
        	-- 遍历列 Max 以下标1作为数组开始索引,看来我们只能用这种方式来导出了. 检查下数据是否能对上.
        	for X =1 to Width do 
        	( 
           	local H = terrainObject.verts[X + Y*Width].pos.z 
           	WriteFloat out_Stream H 
        	)
    		)	
    
			  
			  -- 数据文本信息, 
				local Note = "Height Map Info " + filename + " : Size " + (Width as string) 
				local maxFile_ = maxFileName
				Note += "Exported From: "
				Note += maxFile_
				--WriteString out_Stream Note 
				
	  
	   
   )else
   (
      messagebox(" 地形缺少用户描述.")
   )
 
)

fn SYExportScene = 
(
   local filename = getSaveFileName caption:"导出场景数据" types:"Text(*.scene)|*.scene|"
   if filename!=undefined then
	 (
	     local out =fopen filename	"wb"
	  		local Str
	     -- export terrain if exists.
	    	local terrainObject
    		
    		local TerrExist = false
   		 -- 查找 "sy_terrain" .
    		for i = 1 to rootnode.children.count do
    		(
        	if ( rootnode.children[i].name == "sy_terrain") then
        	( 
            	terrainObject =  rootnode.children[i]	
            	break
           )
        	
       )
        
       -- debug = newScript()
    		if terrainObject !=undefined then
    		(
    			--	format "%\n " "Terrexist" to:debug
    				local XSize = getuserprop terrainObject "SY_XSize"
   					local YSize = getuserprop terrainObject "SY_YSize"
   					local GirdSize = getuserprop terrainObject "SY_TerrGrid"
   					
   				--	format "XSize %\n " XSize to:debug
   				--	format "YSize %\n " YSize to:debug
   				--	format "GSize %\n " GirdSize to:debug
   					if(XSize != undefined AND YSize != undefined AND GirdSize != undefined) then
   					(
   							TerrExist = true
      					Str = "TERR[";
								WriteString out Str
								
								XSize = XSize as integer
								YSize = YSize as integer
								GirdSize = GirdSize as integer
								
								WriteLong out XSize
								WriteLong out YSize
								WriteLong out GirdSize
								for Y = 0 to YSize-1 do 
    						( 
        						-- 遍历列 Max 以下标1作为数组开始索引,看来我们只能用这种方式来导出了. 检查下数据是否能对上.
        						for X =1 to XSize do 
        						( 
           							local H = terrainObject.verts[X + Y*XSize].pos.z 
           							WriteFloat out H 
        						)
    						)
    						Str = "]TERR"
					 			WriteString out Str			
						)
    		)
    		
    		if(TerrExist == false) then
    		(
    		   Str = "TERR[";
					 WriteString out Str
					 WriteLong out 0;	-- xsize
					 WriteLong out 0;	-- ysize
					 WriteLong out 0;	-- gird
					 Str = "]TERR"
					 WriteString out Str
    		)
	     
	     -- serialize scene
	     	Str = "NODES["
	     	WriteString out Str
				local NumNodes = 0;
				for i = 1 to rootnode.children.count do
				(
				 		local child = rootnode.children[i];
				 		local value = getUserProp child "SY_SrcFile"
						if(value != undefined) then
      			(
 							 NumNodes+=1
      			)
       			
        )
        
        WriteLong out NumNodes
        
	     	for i = 1 to rootnode.children.count do
				(
				 		local child = rootnode.children[i];
				 		-- 我们只导出Ref 对象
				 		local value = getUserProp child "SY_SrcFile"
						if(value != undefined) then
      			(
      			    Str = child.name
      			    WriteString out Str
								
								Str = value as string
								WriteString out Str
								
								Str = "R"
								WriteString out Str
								WriteFloat out child.rotation.controller.X_Rotation
								WriteFloat out child.rotation.controller.Y_Rotation
								WriteFloat out child.rotation.controller.Z_Rotation
								--WriteFloat out child.rotation.w
								
								Str = "T"
								WriteString out Str
								WriteFloat out child.pos.x
								WriteFloat out child.pos.y
								WriteFloat out child.pos.z

								Str = "S"
								WriteString out Str
								WriteFloat out child.scale.x
								WriteFloat out child.scale.y
								WriteFloat out child.scale.z
      			)
       			
        )
	     	Str = "]NODES"
	     	WriteString out Str
	     fclose out
	 )
)

fn SYImportScene = 
(
   local filename = getOpenFileName caption:"导入场景数据" types:"Text(*.scene)|*.scene|"
   if filename!=undefined then
	 (
				debug = newScript() 
	     local inf =fopen filename "rb"
	  		local Str
	     -- import terrain if exists.
	    	local terrainObject
    		
 
   		 -- 查找 "sy_terrain" .
    		for i = 1 to rootnode.children.count do
        if ( rootnode.children[i].name == "sy_terrain" ) then 
            terrainObject =  rootnode.children[i]	
        else
            terrainObject = undefined
            
        -- 如果当前场景不存在地形,则载入地形
        Str = ReadString inf 
        local XSize = ReadLong inf
        local YSize = ReadLong inf
        local Gird = ReadLong inf
        if terrainObject ==undefined then
        (
             terrainObject = plane length:(YSize*Gird) \ 
                             width:(XSize*Gird) \ 
                             lengthsegs:(YSize - 1) \
                             widthsegs:(XSize - 1) \
                             wireColor: green 
            terrainObject.name = "sy_terrain"
            clearUndoBuffer()
						collapseStack terrainObject
    				clearUndoBuffer()
   					addModifier terrainObject (Uvwmap())
    				collapseStack terrainObject
    				clearUndoBuffer()                
        		for Y = 0 to YSize-1 do 
    				( 
        			-- 遍历列 Max 以下标1作为数组开始索引,看来我们只能用这种方式来导出了. 检查下数据是否能对上.
        				for X =1 to XSize do 
        				( 
           					terrainObject.verts[X + Y*XSize].pos.z = ReadFloat inf 
        				)
    				)
    				
					setUserProp terrainObject "SY_TerrGrid" Gird
					setUserProp terrainObject "SY_XSize" XSize
					setUserProp terrainObject "SY_YSize" YSize
        )else
        (
        		-- skip the height value.   
        		fseek inf XSize*YSize*4 SEEK_CUR
        )
        
       Str = ReadString inf
    --	format "%\n " Str to:debug
	     -- serialize scene
	      Str =	ReadString inf
	    --  format "%\n " Str to:debug
	      if(Str != "NODES[") then
	      (
	         messagebox("IO Error!")
	      )
				local NumNodes =  ReadLong inf
				
				progressstart "载入场景节点..."
	     	for i = 1 to NumNodes do
				(
						progressupdate(100.0*i/NumNodes)
						local ObjName = ReadString inf
						local FileName = ReadString inf
						
						local ObjectList = getmaxfileobjectnames FileName
						if ObjectList.count > 1 then
				    (
				        local errormsg = FileName;
				        errormsg += "包含多个节点, 不能导入."
				    		messagebox errormsg title:"Sunyou Plugin"
				    		
				    		ReadString inf
				    		ReadFloat inf
				    		ReadFloat inf
				    		ReadFloat inf
				    
				    	ReadString inf
				    		ReadFloat inf
				    		ReadFloat inf
				    		ReadFloat inf
				    		--ReadFloat inf
				    
				    	ReadString inf
				    		ReadFloat inf
				    		ReadFloat inf
				    		ReadFloat inf
				    )else
				    (
				        mergemaxfile FileName #select
								local Object = $
				    		if(Object == undefined) then
				    		(
				         		messagebox "unknown error! please contact P.K." title:"Sunyou Plugin"
				    		)
				    
				   		 Object.name = ObjName
				    		setUserProp Object "SY_SrcFile" FileName
				    		
				    		format "% %\n " ObjName FileName to:debug
				    		
				    		Str = ReadString inf
				    		format "%\n " Str to:debug
				    		Object.rotation.controller.X_Rotation = ReadFloat inf
				    		Object.rotation.controller.Y_Rotation = ReadFloat inf
				    		Object.rotation.controller.Z_Rotation = ReadFloat inf
				    		format "%\n " Object.rotation to:debug
				    		--Object.rotation.w = ReadFloat inf
				    		
				    		Str = ReadString inf
				    		format "%\n " Str to:debug
				    		Object.pos.x = ReadFloat inf
				    		Object.pos.y = ReadFloat inf
				    		Object.pos.z = ReadFloat inf
				    		format "%\n " Object.pos to:debug
			
				    
				    		Str = ReadString inf
				    		format "%\n " Str to:debug
				    		Object.scale.x = ReadFloat inf
				    		Object.scale.y = ReadFloat inf
				    		Object.scale.z = ReadFloat inf
				    		format "%\n " Object.scale to:debug
				    )
						
        )
        progressend();
	    Str = ReadString inf
	    if(Str == "]NODES") then
	    (
	     -- messagebox "成功导入场景" title:"Sunyou Plugin"
	    )else
	    (
	       messagebox "导入场景过程中发生IO错误." title:"Sunyou Plugin"
	    )
	     fclose inf 
	 )
)