rollout SYForestRollout "植被编辑" width:243 height:457
(
	pickButton btnSelPolygon "选择区域" pos:[6,14] width:231 height:23
	pickButton btnSelObject "选择对象" pos:[6,40] width:231 height:22
	button btnCreate "创建" pos:[51,422] width:121 height:27
	groupBox grpTrans "Translation:" pos:[6,73] width:231 height:83
	groupBox grp3 "Rotation" pos:[6,163] width:231 height:103
	groupBox grp4 "Scale" pos:[5,272] width:231 height:109
	spinner spnTXMin "x:" pos:[20,110] width:81 height:16 range:[0,100,0] type:#float scale:0.1
	spinner spnTXMax "x:" pos:[145,110] width:81 height:16 range:[0,100,0] type:#float scale:0.1
	spinner spnTYMin "y:" pos:[22,132] width:81 height:16 range:[0,100,0] type:#float scale:0.1
	label lbl1 "Min" pos:[41,92] width:57 height:13
	spinner spnTYMax "y:" pos:[145,132] width:81 height:16 range:[0,100,0] type:#float scale:0.1
	label lbl2 "Max" pos:[165,90] width:54 height:15
	spinner spnRXMin "x:" pos:[20,198] width:81 height:16 range:[0,360,0] type:#float scale:1
	spinner spnSYMin "y:" pos:[21,331] width:81 height:16 range:[0.0001,1000,1] type:#float scale:0.0001
	spinner spnRXMax "x:" pos:[143,197] width:81 height:16 range:[0,360,0] type:#float scale:1
	spinner spnRYMax "y:" pos:[145,219] width:81 height:16 range:[0,360,0] type:#float scale:1
	label lbl3 "Min" pos:[41,177] width:57 height:19
	label lbl4 "Max" pos:[165,180] width:54 height:15
	spinner spnRZMin "z:" pos:[18,241] width:81 height:16 range:[0,360,0] type:#float scale:1
	spinner spnRZMax "z:" pos:[145,241] width:81 height:16 range:[0,360,0] type:#float scale:1
	label lbl5 "Min" pos:[41,290] width:57 height:19
	label lbl6 "Max" pos:[165,289] width:54 height:15
	spinner spnSXMin "x:" pos:[22,309] width:81 height:16 range:[0,100,0] range:[0.0001,1000,1] type:#float scale:0.0001
	spinner spnRYMin "y:" pos:[21,219] width:81 height:16 range:[0,360,0] type:#float scale:1
	spinner spnSZMin "z:" pos:[21,355] width:81 height:16 range:[0,100,0] range:[0.0001,1000,1] type:#float scale:0.0001
	spinner spnSXMax "x:" pos:[145,309] width:81 height:16 range:[0,100,0] range:[0.0001,1000,1] type:#float scale:0.0001
	spinner spnSYMax "y:" pos:[145,332] width:81 height:16 range:[0,100,0] range:[0.0001,1000,1] type:#float scale:0.0001
	spinner spnSZMax "z:" pos:[145,355] width:81 height:16 range:[0,100,0] range:[0.0001,1000,1] type:#float scale:0.0001
	spinner spnCount "Count:" pos:[17,395] width:139 height:16 range:[1,200,1] type:#integer scale:1
	on SYForestRollout open  do
(
		SYCurTreeShape = undefined;
		SYCurBoundShape = undefined;
		spnTXMin.value = SYTransMin.x;
		spnTYMin.value = SYTransMin.y;
		spnTXMax.value = SYTransMax.x;
		spnTYMax.value = SYTransMax.y;
		spnRXMin.value = SYRotMin.x;
		spnRYMin.value = SYRotMin.y;
		spnRZMin.value = SYRotMin.z;
		spnRXMax.value = SYRotMax.x;
		spnRYMax.value = SYRotMax.y;
		spnRZMax.value = SYRotMax.z;
		spnSXMin.value = SYScaleMin.x;
		spnSYMin.value = SYScaleMin.y;
		spnSZMin.value = SYScaleMin.z;
		spnSXMax.value = SYScaleMax.x;
		spnSYMax.value = SYScaleMax.y;
		spnSZMax.value = SYScaleMax.z;
	)
	on SYForestRollout close  do
(
	
	)
	
	--fn g_filter o = superclassof o == Geometryclass 
	on btnSelPolygon picked obj do
(
 			--if( classof obj == Line ) then
 			if( superClassOf obj == Geometryclass ) then
 			(
 			
 					btnSelPolygon.text = obj.name;
 					SYCurBoundShape = obj;
 			)else
 			(
 				 SYCurBoundShape = undefined;
 			)
 		
	)
	on btnSelObject picked obj do
(
			if( superClassOf obj == Geometryclass ) then
 			(
 					btnSelObject.text = obj.name;
 					SYCurTreeShape = obj;
 			)
 			else
 			(
 			   SYCurTreeShape = undefined;
 			)
	)
	on btnCreate pressed  do
(
			if( SYCurTreeShape != undefined and SYCurBoundShape != undefined) then
			(
			    SYTreeCount = spnCount.value
			    include "SunYou/SYForest.ms"
			   	SYCreateForest()
			)
	)
	on spnTXMin changed val do
		SYTransMin.x = val
	on spnTXMax changed val do
		SYTransMax.x = val
	on spnTYMin changed val do
		SYTransMin.y = val
	on spnTYMax changed val do
		SYTransMax.y = val
	on spnRXMin changed val do
		SYRotMin.x = val
	on spnSYMin changed val do
		SYScaleMin.y = val
	on spnRXMax changed val do
		SYRotMax.x= val
	on spnRYMax changed val do
		SYRotMax.y = val
	on spnRZMin changed val do
		SYRotMin.z = val
	on spnRZMax changed val do
		SYRotMax.z = val
	on spnSXMin changed val do
		SYScaleMin.x = val
	on spnRYMin changed val do
		SYRotMin.y = val
	on spnSZMin changed val do
		SYScaleMin.z = val
	on spnSXMax changed val do
		SYScaleMax.x = val
	on spnSYMax changed val do
		SYScaleMax.y = val
	on spnSZMax changed val do
		SYScaleMax.z = val
)