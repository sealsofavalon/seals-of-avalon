---------------------------------------------------------------------------------
-- 导入场景.
---------------------------------------------------------------------------------
macroScript syMenu_ImportScene
category:"SunYou" 
buttonText:"导入场景..."
tooltip:"导入场景结构" 
( 	   
    on execute do
    (
        include "SunYou/SYExportTerrain.ms"
        
       SYImportScene()
    )
)

---------------------------------------------------------------------------------
-- 导出场景.
---------------------------------------------------------------------------------
macroScript syMenu_ExportScene
category:"SunYou" 
buttonText:"导出场景..."
tooltip:"导出场景结构" 
( 	   
    on execute do
    (
        include "SunYou/SYExportTerrain.ms"
        
       SYExportScene()
    )
)


---------------------------------------------------------------------------------
-- 创建编辑地形
---------------------------------------------------------------------------------
macroScript syMenu_EditTerrain
category:"SunYou" 
buttonText:"地形编辑..."
tooltip:"创建/编辑地形" 
( 
    on execute do 
    (
        include "SunYou/SYTerrEditorGui.ms"      
        createDialog syTerrEditorDialog
    )
)

---------------------------------------------------------------------------------
-- 导入编辑对象
---------------------------------------------------------------------------------
macroScript syMenu_EditObject
category:"SunYou" 
buttonText:"对象编辑..."
tooltip:"导入/编辑对象" 
( 
    on execute do 
    (
        include "SunYou/SYObjectImport.ms"      
        createDialog syObjectImportDialog
    )
)

macroScript syMenu_EditForest
category:"SunYou" 
buttonText:"植被编辑..."
tooltip:"编辑植被对象" 
( 
    on execute do 
    (
        include "SunYou/SYForestUI.ms"      
        createDialog SYForestRollout
    )
)

-- 设置工程目录
macroScript syMenu_SetWorkingDir
category:"SunYou" 
buttonText:"设置工作目录..."
tooltip:"设置工程目录" 
( 
    on execute do 
    (
       
    )
)

---------------------------------------------------------------------------------
-- About
---------------------------------------------------------------------------------
macroScript syMenu_About
category:"SunYou" 
buttonText:"About..."
tooltip:"about" 
( 
    on execute do 
    (
        include "SunYou/SYAbout.ms"
        
        CreateDialog syAboutDialog
    )
)

---------------------------------------------------------------------------------
-- 添加菜单
---------------------------------------------------------------------------------
fn AddMenu =
(
    local bCreateMenu = false
	
    if menuMan.registerMenuContext 0x53594D4E then 
        bCreateMenu = true
    else 
    (	
        if (menuMan.findMenu "SunYou插件") == undefined then 
            bCreateMenu = true
    )

    if bCreateMenu then
    ( 
        local mainMenu = menuMan.getMainMenuBar()
        local syMenu = menuMan.createMenu "SunYou插件"
				
				-- 创建菜单命令对象
        local ItemsTemp = #()
        ItemsTemp[1] = menuMan.createActionItem "syMenu_ImportScene" "SunYou"
      	append ItemsTemp (menuMan.createActionItem "syMenu_ExportScene" "SunYou")
        append ItemsTemp (menuMan.createSeparatorItem())					-- 分割符
		
        append ItemsTemp (menuMan.createActionItem "syMenu_EditTerrain" "Sunyou")
        append ItemsTemp (menuMan.createActionItem "syMenu_EditObject" "Sunyou")
	append ItemsTemp (menuMan.createActionItem "syMenu_EditForest" "Sunyou")
        append ItemsTemp (menuMan.createSeparatorItem())

        append ItemsTemp (menuMan.createActionItem "syMenu_SetWorkingDir" "Sunyou")
        append ItemsTemp (menuMan.createSeparatorItem())

        append ItemsTemp (menuMan.createActionItem "syMenu_About" "Sunyou")
				
				-- 加入到子菜单中	
        for i=1 to itemsTemp.count do 
            syMenu.addItem ItemsTemp[i] -1
			
				-- 加入到MAX菜单栏中
        local subMenuItem = menuMan.createSubMenuItem "SunYou插件" syMenu
        local subMenuIndex = mainMenu.numItems()
        mainMenu.addItem subMenuItem subMenuIndex
        menuMan.updateMenuBar()
    ) 
)

---------------------------------------------------------------------------------
-- 卸载所有菜单
---------------------------------------------------------------------------------
fn RemoveMenu = 
(
    local menu = menuMan.findMenu "SunYou插件";
    if menu != undefined then
    (
        menuMan.unRegisterMenu menu;
        menuMan.updateMenuBar();
    )
)

---------------------------------------------------------------------------------
-- 安装菜单
---------------------------------------------------------------------------------
fn SYLoadMenu =
(
    RemoveMenu()
    AddMenu()
)


