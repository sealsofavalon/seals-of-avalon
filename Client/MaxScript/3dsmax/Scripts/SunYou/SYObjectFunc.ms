

-------------------------------------------------------------------------------
--  导入MAX对象,并创建实例
-------------------------------------------------------------------------------
fn SYImportObjectMAX = 
(
   -- 我们要使用max 的XREF 机制. 这里我们仅仅只是获取一个文件名,以确保我们的引用机制.
   local file = getOpenFilename caption:"导入引用对象" types:"3D Studio MAX (*.max)|*.max|"
	  if (file != undefined) then (
				return file
		)
			
			return undefined
)

-------------------------------------------------------------------------------
--  将XRef 对象加入到列表中,确保唯一性. NewFile : 新对象的文件名, List : 列表框对象
-------------------------------------------------------------------------------
fn SYAddXRefObject NewFile = 
(
    -- debug = newScript() 
		
		if(SYImportedList == undefined) then
		(
		 SYImportedList = #()
		)
		
			for fName in SYImportedList do 
			(
			
		--	format "%\n " fName to:debug
				 if fName == NewFile then
				 (
				    local warning = fName as string
				    warning += " 已经存在."
				    messagebox(warning)
				    return undefined
				 )
				 
				 
			)
			
			-- 不在引用列表中.加入引用列表,注意这个时候我们并不创建引用.
			SYImportedList += #(NewFile)
			
			--for fName in SYImportedList do 
			--(
			--format "%\n " fName to:debug
			--)
			
			return NewFile
)


fn SYPO_startStroke =
(
)
			
fn SYPO_endStroke =
(
	thePainterInterface.undoAccept()
)
			
fn SYPO_cancelStroke =
(
		thePainterInterface.undoCancel()
)
			
fn SYPO_systemEnd =
(
		--AO_objectspaint_button.checked=false
)

fn SYPO_paintStroke =
(
	with redraw off
	(
 		hitCount = thePainterInterface.GetHitCount()
		local objectfilename= SYCurPainterObj 
		
		localHit = Point3 0 0 0
		localNormal = Point3 0 0 0
		worldHit = Point3 0 0 0
		worldNormal = Point3 0 0 0
		str = 0.0f
		radius = 0.0f
				
		thePainterInterface.getHitPointData &localHit &localNormal &worldHit &worldNormal &radius &str 0

		hit = thePainterInterface.getIsHit 0
		if (hit) then
		(
				-- 合并对象
				-- 先获得将要合并的文件包含对象列表. 这一步我们应该移动到导入的时刻去,必须确保MAX中只包含一个节点????
				    local ObjectList = getmaxfileobjectnames objectfilename
				    if ObjectList.count == 1 then
				    (
				    -- debug = newScript() 
				    --	for objName in ObjectList do
				    --	(
				    --    	format "%\n " objName to:debug
				   -- 	)
							mergemaxfile objectfilename #select
				      
				      -- 获得当前选择对象. 如果选择对象为空,则根据对象名获取
				      Object = $
				      if Object == undefined then
				      (
				         	Object = SYFindObjectByName(ObjectList[1])
				         	for i = 1 to rootnode.children.count do
									(
											child = rootnode.children[i];
											if child.name == Name then
											( 
			   								Object = child
			   								break;
			   							)
									)
				      )
				      if(Object == undefined) then
				      (
				         messagebox "unknown error! please contact P.K." title:"Sunyou Plugin"
				      )
				      local ObjName = getFilenameFile objectfilename
				      Object.name = uniquename ObjName
				      Object.pos = worldHit
				      setUserProp Object "SY_SrcFile" objectfilename
				    )else
				    (
				      messagebox "合并文件中包含多个对象." title:"Sunyou Plugin"
				    )
		)
	)
)

fn SYObjectTransPos Obj =
(
   local result = Obj.pos.x as string
   result += " "
   result += Obj.pos.y as string
   result += " "
   result += Obj.pos.z as string
   return result
)

fn SYExportScene = 
(
		debug = newScript()
   local filename = getSaveFileName caption:"导出场景数据" types:"Text(*.sce)|*.sce|"
   
  
   local out =fopen filename	"wt"	
   WriteString out "<?xml version=\"1.0\" encoding=\"Windows-1252\" ?> "
		WriteString out "<Scene>\n"
		local element;
    if filename!=undefined then
		(
				for i = 1 to rootnode.children.count do
				(
				 		local child = rootnode.children[i];
				 		-- 我们只导出Ref 对象
				 		local value = getUserProp child "SY_SrcFile"
						if(value != undefined) then
      			(
      		 			 format "%\n " child.name to:debug
       					element = "\t<Object Name = \""
       					element += child.name as string
       					element += "\" RefFile = \""
       					element += value as string
       					element += "\" Pos = \""
       					element += child.pos as string
       					element += "\"\n\t\t\t\t Rot = \""
       					element += child.rotation as string
       					element += "\" Scl = \""
       					element += child.scale as string
       					element += "\"/>\n"
       					WriteString out element
      			)
       			
        )
		)
		
		WriteString out "</Scene>"
		fclose out
)