-----------------------------------------------------------------------------------------------------
-- max 的界面编辑实在是稀烂. 我决定手动编辑. 不要再用VSM 导出,否则会覆盖相应文件.
-----------------------------------------------------------------------------------------------------
rollout syObjectImportDialog "导入对象" width:427 height:389
(
	button btnImport "导入MAX文件" pos:[6,287] width:119 height:23
	checkbox chkOverTerr "放置于地形之上" pos:[7,326] width:234 height:24 enabled:true checked:true
	checkbox chk2 "Checkbox" pos:[315,323] width:104 height:28
	checkbutton ckbPainter "放置对象" pos:[11,355] width:410 height:21 highlightcolor:(color 250 200 75) toolTip:"放置对象"
	listBox lbxRef "对象列表" pos:[8,5] width:414 height:18 selection:0
	bitmap bmp1 "Bitmap" pos:[7,317] width:416 height:1 enabled:false fileName:""
	button btnExport "导出场景" pos:[175,291] width:127 height:22 enabled:true toolTip:"导出场景.."
	on syObjectImportDialog open  do
(
		-- 填充列表
		SYUpdateState()
		for fName in SYImportedList do 
		(
		   lbxRef.items += #(fName)
		)
)
	on syObjectImportDialog close  do
(
	thePainterInterface.endPaintSession()
	SYSaveState()
)
	on btnImport pressed  do
(
	   include "SunYou/SYObjectFunc.ms"
	
	    local objFileName = SYImportObjectMax() 
	   if(objFileName != undefined) then
	   (
	      local newObj = SYAddXRefObject objFileName
	      if(newObj != undefined) then
	      (
	         lbxRef.items += #(newObj)
	       )
	   )
)

	on ckbPainter changed state do
(
		include "SunYou/SYObjectFunc.ms"
   if(state) then
   (
    -- 绘制
		if(ckbPainter.text != "放置对象") then
		(
		    if $!=undefined then
				(
				thePainterInterface.additiveMode = FALSE
						thePainterInterface.initializeNodes 0 $
						thePainterInterface.offMeshHitType = 2
						thePainterInterface.scriptfunctions SYPO_StartStroke SYPO_PaintStroke SYPO_EndStroke SYPO_CancelStroke SYPO_SystemEnd
						thePainterInterface.initializeNodes 0 $
						thePainterInterface.startPaintSession()
				)else
				(
				   thePainterInterface.endPaintSession()
				)
		)
		else
		(
	  		messagebox("请先为刷子选择相应模型") title:("绘制模型")
	  		thePainterInterface.endPaintSession()
		) 
	)
	else
	(
	 -- 取消绘制
	 		thePainterInterface.endPaintSession()
	)
)
	
	on lbxRef selected sel do
(
      local Items = lbxRef.items
	    if( sel >0 AND sel <= Items.count ) then
	    (
	        local filename = Items[sel] as string
	       ckbPainter.text = filename
	       SYCurPainterObj = filename
	    )
	)
	
	on btnExport pressed  do
(
	include "SunYou/SYObjectFunc.ms"
	SYExportScene()
	)
) -- end of file