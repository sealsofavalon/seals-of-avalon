rollout syTerrEditorDialog "Terrain Editor" width:314 height:478
(
	spinner spnTerrXSize "大小(X):" pos:[23,33] width:119 height:16 range:[64,1024,64] type:#integer scale:64
	spinner spnTerrYSize "大小(Y):" pos:[164,35] width:117 height:16 range:[64,1024,64] type:#integer scale:64
	groupBox grpCreate "创建" pos:[7,10] width:290 height:79
	button btnImport "导入地形" pos:[9,139] width:288 height:21 toolTip:"导入地形"
	button btnCreate "创建地形" pos:[166,61] width:118 height:22 toolTip:"创建地形"
	groupBox grpModify "编辑地形" pos:[11,169] width:286 height:297
	spinner spnGridSize "GirdSize" pos:[17,61] width:125 height:16 range:[1,100,1] type:#integer scale:1
	dropDownList ddlModifyType "编辑方式:" pos:[31,185] width:248 height:41 enabled:true
	spinner spnBrushSize "刷子半径:" pos:[28,241] width:127 height:16 range:[1,20000,0] type:#integer scale:1 
	spinner spnBrushHard "刷子强度:" pos:[28,275] width:127 height:16 enabled:true range:[0,20000,0] type:#integer scale:1
	checkButton ckbPainter "绘制地形高度" pos:[32,301] width:224 height:23 toolTip:"绘制地形高度" checked:false
	groupBox grpImport "导入地形" pos:[8,100] width:291 height:68
	spinner spnHeightScale "高度缩放" pos:[16,116] width:148 height:16 range:[0.1,10,0] scale:0.1 
	button btnBrushOption ">>" pos:[261,302] width:30 height:23
	button btnAdjust "调整平面坐标" pos:[33,339] width:105 height:24
	on syTerrEditorDialog open  do
(
		include "SunYou/SYTerrainFunc.ms"
		-- 填充DropList,在编辑器中直接填充,导致语法错误....
		local DropListText = #()
		append DropListText "增加高度"
		append DropListText "降低高度"
		append DropListText "平    滑"
		ddlModifyType.items = DropListText
		ddlModifyType.selection = SYTerrPaintMode
		if (thePainterInterface.inpaintmode()) then 
		ckbPainter.checked=false
		thePainterInterface.maxStr=spnBrushHard.value
		thePainterInterface.maxSize= spnBrushSize.value
		thePainterInterface.normalScale=1.0
	)
	on syTerrEditorDialog close  do
(
		if (thePainterInterface.InPaintMode()) then 
		thePainterInterface.EndPaintSession()
	)
	on btnImport pressed  do
(
		include "SunYou/SYTerrainFunc.ms"
		
		bmpFilename = SYImportTerrain spnHeightScale.value
		 if bmpFilename != undefined do
		   btnImport.text = filenameFromPath bmpFilename
	)
	on btnCreate pressed  do
(
		include "SunYou/SYTerrainFunc.ms"
		SYCreateTerrain spnTerrXSize.value spnTerrYSize.value spnGridSize.value
	)
	on ddlModifyType selected sel do
(
		SYTerrPaintMode = sel	 
		thePainterInterface.endPaintSession() 
		ckbPainter.checked = FALSE 
		--debug = newScript() 
		--format "sel Mode :% \n " SYTerrPaintMode to:debug
	)
	on spnBrushSize changed val do
		thePainterInterface.maxSize=val
	on spnBrushHard changed val do
		thePainterInterface.maxStr=val
	on ckbPainter changed state do
	(
	include "SunYou/SYTerrainFunc.ms"   -- include
	if($ == undefined)then
	  messagebox("请先选择地形对象")
	if state == true then
	(
	     thePainterInterface.pointGatherEnable = TRUE
		thePainterInterface.additiveMode = TRUE
	     SYTerrPaintButton = ckbPainter
	     case SYTerrPaintMode of
	     (
	        1:(
			thePainterInterface.scriptfunctions SYPT_StartStroke SYPT_Higher SYPT_EndStroke SYPT_CancelStroke SYPT_SystemEnd
			)
			2:(
		
			thePainterInterface.scriptfunctions SYPT_StartStroke SYPT_Lower SYPT_EndStroke SYPT_CancelStroke SYPT_SystemEnd
			)				
			3:(
			thePainterInterface.scriptfunctions SYPT_StartStroke SYPT_Smooth SYPT_EndStroke SYPT_CancelStroke SYPT_SystemEnd
			)
	     )
	     thePainterInterface.initializeNodes 0 $	
	     thePainterInterface.startPaintSession()
	)
	else
	(
	   thePainterInterface.endPaintSession()
	)
	)
	on spnHeightScale changed val do
(
	
	)
	on btnBrushOption pressed  do
(
		thePainterInterface.paintOptions()
		spnBrushSize.value =thePainterInterface.maxSize
		spnBrushHard.value =thePainterInterface.maxStr
	)
	
	on btnAdjust pressed  do
(
include "SunYou/SYTerrainFunc.ms"
		SYPT_AdjustXY();
	)
)