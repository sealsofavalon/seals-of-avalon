global SYTerrSize = 256					-- 地形大小
global SYTerrGrid = 1					-- 地形大小
global SYTerrPaintMode  = 1				-- 地形绘制模式
global SYTerrHeightTrack = #()				-- 高度
global SYTerrPaintButton = undefined

global SYImportedList = #()				-- 当前文档的XRef对象列表
global SYCurPainterObj = #() 				-- 当前绘制的XRef对象路径

global SYCurBoundShape = undefined;
global SYCurTreeShape = undefined;
global SYTreeCount = 1
global SYTransMin = Point3 0 0 0		-- 种树时候最小位移
global SYRotMin 	= Point3 0 0 0		-- 种树时候最小旋转
global SYScaleMin = Point3 1 1 1		-- 种树时候最小缩放
global SYTransMax = Point3 0 0 0
global SYRotMax 	= Point3 0 0 0
global SYScaleMax = Point3 1 1 1

-- 保存状态到rootnode的自定义属性中
fn SYSaveState = 
(
  /* max 不允许脚本设置rootnode 的自定义属性. 我们需要通过程序来扩展这个功能. TODO  
  setUserProp rootnode "SY_TerrSize" SYTerrSize as string
   
   -- 保存导入列表
   -- 删除原来的.
   local NumImport = getUserProp rootnode "SY_ImportNum" 
   if(NumImport != undefined) then
   (
      for up=1 to NumImport do 
   		( 
      		local keystr = "SY_Import_" + up as string 
      		setUserProp rootnode keystr undefined
  		)
   )
   local NumImport = SYImportedList.count;
   setUserProp rootnode "SY_ImportNum" NumImport
   for up=1 to NumImport do 
   ( 
     local keystr = "SY_Import_" + up as string 
      setUserProp rootnode keystr SYImportedList[up]
  	)
  	*/
)


fn SYUpdateState = 
(
  /* local Value = getUserProp rootnode "SY_TerrSize"
   SYTerrSize = Value as integer
   local NumImport = getUserProp rootnode "SY_ImportNum" 
   if(NumImport != undefined) then
   (
   		SYImportedList = #()
      for up=1 to NumImport do 
   		( 
      		local keystr = "SY_Import_" + up as string 
      		local value = getUserProp rootnode keystr
      		if(value != undefined) then
      		(
      		  SYImportedList += #(value)
      		)
  		)
   )*/
   SYImportedList = #()
   for i = 1 to rootnode.children.count do
	(
			local child = rootnode.children[i];
			local value = getUserProp child "SY_SrcFile"
			if(value != undefined) then
      (
      		  SYImportedList += #(value)
      )
	)
)