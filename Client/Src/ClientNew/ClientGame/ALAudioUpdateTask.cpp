#include "stdafx.h"
#include "ALAudioUpdateTask.h"

//#include "NiParticleSystem.h"

#include "audio/ALAudioSystem.h"

NiImplementRTTI(AudioSysUpdateTask, NiTask);

NiCriticalSection AudioSysUpdateTask::ms_kPoolCriticalSection;
NiTObjectPool<AudioSysUpdateTask>* AudioSysUpdateTask::ms_pkPool;
//----------------------------------------------------------------------------
AudioSysUpdateTask::AudioSysUpdateTask() :
    m_fTime(0.0f)
{
}
//----------------------------------------------------------------------------
AudioSysUpdateTask::~AudioSysUpdateTask()
{
    m_spSystem = NULL;
}
//---------------------------------------------------------------------------
void AudioSysUpdateTask::_SDMInit()
{
    ms_pkPool = NiNew NiTObjectPool<AudioSysUpdateTask>;
}
//---------------------------------------------------------------------------
void AudioSysUpdateTask::_SDMShutdown()
{
    ms_pkPool->PurgeAllObjects();
    NiDelete ms_pkPool;
}
//---------------------------------------------------------------------------
AudioSysUpdateTask* AudioSysUpdateTask::GetFreeObject()
{
    ms_kPoolCriticalSection.Lock();
    AudioSysUpdateTask* pkTask = ms_pkPool->GetFreeObject();
    ms_kPoolCriticalSection.Unlock();
    return pkTask;
}
//---------------------------------------------------------------------------
void AudioSysUpdateTask::ReleaseObject(AudioSysUpdateTask* pkTask)
{
    ms_kPoolCriticalSection.Lock();
    ms_pkPool->ReleaseObject(pkTask);
    ms_kPoolCriticalSection.Unlock();
}
//----------------------------------------------------------------------------
void AudioSysUpdateTask::Init(ALAudioSystem* pkSystem, float fTime)
{
    m_spSystem = pkSystem;
    m_fTime = fTime;
}
//----------------------------------------------------------------------------
void AudioSysUpdateTask::DoTask()
{
    if ( m_spSystem != NULL )
    {
        m_spSystem->Do_UpdateSystem(m_fTime, true);
        m_spSystem = NULL;
    }
    Clear();
}
//----------------------------------------------------------------------------
bool AudioSysUpdateTask::Clear()
{
    m_spSystem = NULL;
    ReleaseObject(this);
    return true;
}

