#pragma once


NiSmartPointer(ALAudioSystem);

class AudioSysUpdateTask : public NiTask
{
    NiDeclareRTTI;

	// Pool managing interface
public:
    static AudioSysUpdateTask* GetFreeObject();
    static void ReleaseObject(AudioSysUpdateTask* pkTask);

    void Init(ALAudioSystem* pkSystem, float fTime);

    virtual void DoTask();
    virtual bool Clear();

    // *** begin Emergent internal use only ***
    static void _SDMInit();
    static void _SDMShutdown();
    // *** end Emergent internal use only ***

protected:

    // These objects are pool allocated, must use GetFree and Release
    AudioSysUpdateTask();
    ~AudioSysUpdateTask();

    static NiCriticalSection ms_kPoolCriticalSection;
    static NiTObjectPool<AudioSysUpdateTask>* ms_pkPool;
    // allow this class to be constructed by pool:
    friend class NiTNewInterface<AudioSysUpdateTask>;

protected:
    ALAudioSystemPtr m_spSystem;
    float m_fTime;
};

