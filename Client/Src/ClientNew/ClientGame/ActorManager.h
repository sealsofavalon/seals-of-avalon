#ifndef ACTOR_MANAGER_H
#define ACTOR_MANAGER_H

#include "ModelFile.h"
#include "ResourceManager.h"
#include "NiFilename.h"

class CActorManager : public NiActorManager
{
protected:
    CActorManager(NiKFMTool* pkKFMTool)
        :NiActorManager(pkKFMTool, true)
    {
    }

public:
	static NiActorManager* Create(const char* pcKFMFilename,
		bool bCumulativeAnimations = false, bool bLoadFilesFromDisk = true,
		NiStream* pkStream = NULL)
	{
		NIASSERT(pcKFMFilename);

		NiKFMTool::KFM_RC eRC, eSuccess = NiKFMTool::KFM_SUCCESS;

		// Create KFM tool and load KFM file.
		NiKFMToolPtr spKFMTool = NiNew NiKFMTool;
		eRC = spKFMTool->LoadFile(pcKFMFilename);
		if (eRC != eSuccess)
		{
			return NULL;
		}

		return Create(spKFMTool, pcKFMFilename);
	}

    static NiActorManager* Create(NiKFMTool* pkKFMTool, const char* pcKFMFilePath)
    {
        NIASSERT(pkKFMTool && pcKFMFilePath);

        // Build the KFM path.
        NiFilename kFilename(pcKFMFilePath);
        char acKFMPath[NI_MAX_PATH];
        NiSprintf(acKFMPath, NI_MAX_PATH, "%s%s", kFilename.GetDrive(), 
        kFilename.GetDir());
        pkKFMTool->SetBaseKFMPath(acKFMPath);

        // Create actor manager.
        CActorManager* pkActorManager = NiNew CActorManager(pkKFMTool);
        if(!pkActorManager->Load())
        {
            NiDelete pkActorManager;
            return NULL;
        }

        return pkActorManager;
    }

    bool Load()
    {
        NiStream kStream;

        const char* pcNIFFilename = m_spKFMTool->GetFullModelPath();
        NIASSERT(pcNIFFilename);

        NiAVObjectPtr spNIFRoot;
        //if(IsMfFile(pcNIFFilename))
        //{
        //    CModelFile kModelFile;
        //    spNIFRoot = kModelFile.Load(pcNIFFilename);
        //}
        //else
        //{
        //    if(!kStream.Load(pcNIFFilename))
        //        return false;

        //    spNIFRoot = NiDynamicCast(NiAVObject, kStream.GetObjectAt(0));
        //}

		spNIFRoot = g_ResMgr->LoadNif(pcNIFFilename);

        if(!spNIFRoot)
            return false;

        ChangeNIFRoot(spNIFRoot);
        LoadSequences(&kStream);

        return true;
    }
};

NiSmartPointer(CActorManager);

#endif //ACTOR_MANAGER_H