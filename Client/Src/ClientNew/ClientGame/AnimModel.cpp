#include "StdAfx.h"
#include "AnimModel.h"
#include "ResourceManager.h"
#include "AnimObject.h"
#include "ObjectManager.h"
#include "ItemManager.h"

void CAnimModel::Begin()
{
}

//异步线程中执行
void CAnimModel::Run()
{
    m_spActorManager = g_ResMgr->LoadKfm(m_kKfmName);
}

void CAnimModel::End()
{
    CTask::End();

    CAnimObject* pkAnimObject = (CAnimObject*)GetObject();
    if(pkAnimObject != NULL)
    {
        pkAnimObject->OnActorLoaded(m_spActorManager);
    }
    else
    {
        //对象已经被删除
        g_ResMgr->FreeKfm(m_spActorManager);
    }
}

UpdateObj* CAnimModel::GetObject()
{
    if(m_bClientObject)
        return ObjectMgr->GetClientPlayer(m_kGuid);

    return ObjectMgr->GetObject(m_kGuid);
}

CAvatarModel::~CAvatarModel()
{
    if(m_spAVObject)
        g_ResMgr->FreeNif(m_spAVObject);

    if(m_spEffectObject)
        g_ResMgr->FreeNif(m_spEffectObject);
}

void CAvatarModel::Begin()
{
    CPlayer* pkPlayer = (CPlayer*)GetObject();
    if(pkPlayer)
    {
        ui32 itemeffect = PLAYER_VISIBLE_ITEM_1_0 + (m_uiSlotIndex * 12) + 9 ;
        m_uiEffectDisplayId = pkPlayer->GetUInt32Value(itemeffect);
        m_uiRace = pkPlayer->GetRace();
        m_uiGender = pkPlayer->GetGender();
    }
}

void CAvatarModel::Run()
{
    std::string strFilename;
    ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(m_uiItemId);
    if(ItemInfo == NULL)
    {
        if(m_uiItemId == 0)
        {
            unsigned int uiDefaultId = m_uiRace + (m_uiGender<<16);
            unsigned int uiModelId = 0;
            ItemMgr->GetDefaultDisplayInfo(uiDefaultId, uiModelId, m_uiSlotIndex);
            ItemMgr->GetDisplayInfo(uiModelId, strFilename, m_uiRace, m_uiGender, true);
        }
    }
    else
    {
        ui32 display = ItemInfo->DisplayInfoID[0][0];
        if( ItemInfo->Class != ITEM_CLASS_WEAPON )
        {
            if( m_uiRace )
                display = ItemInfo->DisplayInfoID[m_uiRace-1][m_uiGender];
        }

        ItemMgr->GetDisplayInfo(display, strFilename, m_uiRace, m_uiGender, true);
    }

    if(!strFilename.empty())
        m_spAVObject = g_ResMgr->LoadNif(strFilename.c_str(), false);
    else
        return;

    strFilename.clear();
    if(m_uiEffectDisplayId != 0)
    {
        if( ItemMgr->GetDisplayInfo(m_uiEffectDisplayId, strFilename, 0, 0, false) )
            m_spEffectObject = g_ResMgr->LoadNif(strFilename.c_str(), false);
    }

}

void CAvatarModel::End()
{
    CTask::End();

    CPlayer* pkPlayer = (CPlayer*)GetObject();
    if(pkPlayer != NULL)
        pkPlayer->OnEquipmentChanged();
    else
    {
        //在装载完成之前, 角色已经下线时会发生这种情况
        NiDelete this;
    }
}

UpdateObj* CAvatarModel::GetObject()
{
    if(m_bClientObject)
        return ObjectMgr->GetClientPlayer(m_kGuid);

    return ObjectMgr->GetObject(m_kGuid);
}

void CFormModel::Begin()
{
}

void CFormModel::Run()
{
    if( m_uiEntryId != 0 )
        m_spActorManager = g_ResMgr->LoadKfm(m_kKfmName);
}

void CFormModel::End()
{
    CTask::End();

    CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(m_kGuid);
    if(pkPlayer != NULL && pkPlayer->GetAsPlayer())
        pkPlayer->OnFormModelChanged();
    else
    {
        //在装载完成之前, 角色已经下线时会发生这种情况
        NiDelete this;
    }
}
