#ifndef ANIM_MODEL_H
#define ANIM_MODEL_H

#include "Map/Task.h"
class CAnimObject;
class UpdateObj;


//可以进行异步装载
class CAnimModel : public CTask
{
public:
    CAnimModel(const ui64 guid, const char* pcKfmName, bool bClientObject)
        :m_kGuid(guid)
        ,m_kKfmName(pcKfmName)
        ,m_bClientObject(bClientObject)
    {
    }

    virtual ~CAnimModel()
    {
    }

    virtual void Begin();
    virtual void Run();
    virtual void End();

    virtual bool NeedDelete() const { return true; }

    UpdateObj* GetObject();

protected:
    NiActorManagerPtr m_spActorManager;

    NiFixedString m_kKfmName;
    ui64 m_kGuid;

    bool m_bClientObject;
};

//异步加载Avater
class CAvatarModel : public CTask
{
public:
    CAvatarModel(ui64 guid, unsigned int uiItemId, unsigned int uiSlotIndex, int iAttachmentSlot, bool bClientObject)
        :m_kGuid(guid)
        ,m_uiItemId(uiItemId)
        ,m_uiSlotIndex(uiSlotIndex)
        ,m_iAttachmentSlot(iAttachmentSlot)
        ,m_bClientObject(bClientObject)
        ,m_uiEffectDisplayId(0)
        ,m_uiRace(1)
        ,m_uiGender(0)
    {}

    virtual ~CAvatarModel();

    void SetGUID(const ui64 guid) { m_kGuid = guid; }

    virtual void Begin();
    virtual void Run();
    virtual void End();

    unsigned int GetItemId() const { return m_uiItemId; }
    unsigned int GetSlotIndex() const { return m_uiSlotIndex; }
    int GetAttachmentSlot() const { return m_iAttachmentSlot; }

    UpdateObj* GetObject();

protected:
    NiAVObjectPtr m_spAVObject;
    NiAVObjectPtr m_spEffectObject;

    ui64 m_kGuid;
    unsigned int m_uiItemId;
    unsigned int m_uiSlotIndex;
    int m_iAttachmentSlot;
    unsigned int m_uiEffectDisplayId;

    unsigned int m_uiRace;
    unsigned int m_uiGender;

    bool m_bClientObject;
};

//变身
class CFormModel : public CTask
{
public:
    CFormModel(const ui64 guid, const char* pcKfmName, unsigned int uiEntryId)
        :m_kGuid(guid)
        ,m_kKfmName(pcKfmName)
        ,m_uiEntryId(uiEntryId)
    {
		m_bisShapShift = false;
    }

    virtual ~CFormModel()
    {
    }

    virtual void Begin();
    virtual void Run();
    virtual void End();

    NiActorManager* GetActorManager() { return m_spActorManager; }
    unsigned int GetEntryId() const { return m_uiEntryId; }
	void SetShapShift( bool isShift ){ m_bisShapShift = isShift;}

	bool isShapShift(){ return m_bisShapShift; }

protected:
    ui64 m_kGuid;
    NiFixedString m_kKfmName;
    NiActorManagerPtr m_spActorManager;
    unsigned int m_uiEntryId;
	bool m_bisShapShift;
};

#endif