#pragma once
#include <vector>
#include <hash_map>

template<class KeyType, class ValueType,  class MapType>
class CArrayEx : public std::vector<ValueType>
{
public:
	CArrayEx(void)
	{
	}
	~CArrayEx(void)
	{
	}
	
	int Add(const KeyType& Key ,const ValueType& Value)
	{
		int index = FindIndex(Key);
		if (index == -1)
		{
			if (m_FreeIndics.size() > 0)
			{
				size_t FreeIndex = m_FreeIndics[m_FreeIndics.size() - 1];
				assert(FreeIndex >= 0 && FreeIndex < size());
				at(FreeIndex) = Value;
				m_Map.insert(MapType::value_type(Key, FreeIndex));
				m_FreeIndics.pop_back();
			}else
			{
				size_t Index = size();
				push_back(Value);
				m_Map.insert(MapType::value_type(Key, Index));
			}
		}
		return -1;
	}
	int FindIndex(const KeyType& Key)
	{
		MapType::const_iterator it = m_Map.find(Key);
		if (it != m_Map.end())
		{
			return it->second;
		}
		return -1;
	}
	
	void Remove(const KeyType& Key, const ValueType& EmptyValue)
	{
		MapType::iterator it = m_Map.find(Key);
		if (it != m_Map.end())
		{
			int Index = it->second;
			at(Index) = EmptyValue;
			m_FreeIndics.push_back(Index);
			m_Map.erase(it);
		}
	}
	void Clear()
	{
		clear();
		m_FreeIndics.clear();
		m_Map.clear();
	}
	MapType& GetMap() { return m_Map; }
protected:
	vector<int> m_FreeIndics;
	MapType m_Map;
};
