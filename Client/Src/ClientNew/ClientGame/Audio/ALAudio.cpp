#include "stdafx.h"
#include "ALAudio.h"

#include "vorbis/codec.h"
#include "vorbisStream.h"

#include "PackFile.h"


/// WAV File-header
struct WAVFileHdr
{
  ALubyte  id[4];
  ALsizei  size;
  ALubyte  type[4];
};

//// WAV Fmt-header
struct WAVFmtHdr
{
  ALushort format;
  ALushort channels;
  ALuint   samplesPerSec;
  ALuint   bytesPerSec;
  ALushort blockAlign;
  ALushort bitsPerSample;
};

/// WAV FmtEx-header
struct WAVFmtExHdr
{
  ALushort size;
  ALushort samplesPerBlock;
};

/// WAV Smpl-header
struct WAVSmplHdr
{
  ALuint   manufacturer;
  ALuint   product;
  ALuint   samplePeriod;
  ALuint   note;
  ALuint   fineTune;
  ALuint   SMPTEFormat;
  ALuint   SMPTEOffest;
  ALuint   loops;
  ALuint   samplerData;
  struct
  {
    ALuint identifier;
    ALuint type;
    ALuint start;
    ALuint end;
    ALuint fraction;
    ALuint count;
  }      loop[1];
};

/// WAV Chunk-header
struct WAVChunkHdr
{
  ALubyte  id[4];
  ALuint   size;
};

#define CHUNKSIZE 4096

float DBToLinear(float value)
{
   if(value <= 0.f)
      return(0.f);
   if(value >= 1.f)
      return(1.f);

   int max = logmax;
   int min = 0;
   int mid;
   int last = -1;

   mid = (max - min) / 2;
   do {
      last = mid;

      if(logtab[mid] == value)
         break;

      if(logtab[mid] < value)
         min = mid;
      else
         max = mid;

      mid = min + ((max - min) / 2);
   } while(last != mid);

   return((float)mid / logmax);
}

float linearToDB(float value)
{
   if(value <= 0.f)
      return(0.f);
   if(value >= 1.f)
      return(1.f);
   return (float)(logtab[(unsigned int)(logmax * value)]);
}

void ALFWprintf( const char* x, ... )
{
    va_list args;
    va_start( args, x );
    vprintf( x, args ); 
    va_end( args );
}

AudioBuffer::AudioBuffer()
{
   m_pStream = 0;
   m_buffID = 0;
   m_pszFileName = 0;
}

AudioBuffer::~AudioBuffer()
{
	if ( m_pStream)
	{
		delete m_pStream;
		m_pStream = 0;
	}

	if (m_buffID != 0)
	{
		alDeleteBuffers(1, &m_buffID);
		ALenum err = alGetError();

		_asm nop
		m_buffID = 0;
	}

	if (m_pszFileName)
	{
		NiFree(m_pszFileName);
		m_pszFileName = 0;
	}
}

AudioBufferPtr AudioBuffer::find(const char* filename)
{
	AudioBuffer* newBuff = NiNew AudioBuffer;
	if (newBuff->open(filename))
	{
		return newBuff;
	}

	NiDelete newBuff;
	return 0;
}

bool AudioBuffer::open(const char* filename)
{
	if (!CPackFile::AccessAudioFile(filename, NiFile::READ_ONLY))
	{
		return false;
	}

	m_pStream = NiNew CPackFile(filename, CHUNKSIZE, true );
	int nlen = strlen(filename) + 1;
	m_pszFileName = NiAlloc(char, nlen);
	NiStrcpy(m_pszFileName, nlen, filename);

	return true;
}

bool AudioBuffer::readOgg(NiFile* stream)
{
	if (!stream)
		return false;

    OggVorbisFile vf;
    vorbis_info *vi;

    ALenum  format = AL_FORMAT_MONO16;
    char   *data   = NULL;
    ALsizei size   = 0;
    ALsizei freq   = 22050;
    ALboolean loop = AL_FALSE;
	int current_section = 0;
	int endian = 0;

	if (vf.ov_open(stream, NULL, 0) < 0)
		return false;

	vi = vf.ov_info(-1);
    freq = vi->rate;

    long samples = (long)vf.ov_pcm_total(-1);
    if(vi->channels == 1) {
       format = AL_FORMAT_MONO16;
       size = 2 * samples;
    } else {
       format = AL_FORMAT_STEREO16;
       size = 4 * samples;
    }

	data = new char[size];
	if (data)
	{
		long ret = oggRead(&vf, data, size, endian, &current_section);
	}

	vf.ov_clear();
	if (data)
	{
		alBufferData(m_buffID, format, data, size, freq);
		delete[] data;
		return (alGetError() == AL_NO_ERROR);
	}

	return false;
}

long AudioBuffer::oggRead(OggVorbisFile* vf, char* buffer, int length, int bigendianp, int* bitstream)
{
	long bytesRead = 0;
	long totalBytes = 0;
	long offset = 0;
	long bytesToRead = 0;

	while (offset<length)
	{
		if ((length - offset) < CHUNKSIZE)
			bytesToRead = length - offset;
		else
			bytesToRead = CHUNKSIZE;

		bytesRead = vf->ov_read(buffer, bytesToRead, bigendianp, bitstream);
		if (bytesRead <= 0)
			break;
		offset += bytesRead;
		buffer += bytesRead;
	}

	return offset;
}

bool AudioBuffer::readWAV(NiFile* stream)
{
   WAVChunkHdr chunkHdr;
   WAVFmtExHdr fmtExHdr;
   WAVFileHdr  fileHdr;
   WAVSmplHdr  smplHdr;
   WAVFmtHdr   fmtHdr;

   ALenum  format = AL_FORMAT_MONO16;
   char   *data   = NULL;
   ALsizei size   = 0;
   ALsizei freq   = 22050;
   ALboolean loop = AL_FALSE;

   if (!stream)
      return false;

   stream->Read(&fileHdr.id[0], 4);
   stream->Read(&fileHdr.size, sizeof(ALsizei));
   stream->Read(&fileHdr.type[0], 4);

   fileHdr.size=((fileHdr.size+1)&~1)-4;

   stream->Read(&chunkHdr.id[0], 4);
   stream->Read(&chunkHdr.size, sizeof(ALsizei));
   // unread chunk data rounded up to nearest WORD
   int chunkRemaining = chunkHdr.size + (chunkHdr.size&1);

   while ((fileHdr.size!=0) && (stream->GetPosition() < stream->GetFileSize()))
   {
      // WAV Format header
      if (!strncmp((const char*)chunkHdr.id,"fmt ",4))
      {
         stream->Read(&fmtHdr.format, sizeof(fmtHdr.format));
         stream->Read(&fmtHdr.channels, sizeof(fmtHdr.channels));
         stream->Read(&fmtHdr.samplesPerSec, sizeof(fmtHdr.samplesPerSec));
         stream->Read(&fmtHdr.bytesPerSec, sizeof(fmtHdr.bytesPerSec));
         stream->Read(&fmtHdr.blockAlign, sizeof(fmtHdr.blockAlign));
         stream->Read(&fmtHdr.bitsPerSample, sizeof(fmtHdr.bitsPerSample));

         if (fmtHdr.format==0x0001)
         {
            format=(fmtHdr.channels==1?
               (fmtHdr.bitsPerSample==8?AL_FORMAT_MONO8:AL_FORMAT_MONO16):
               (fmtHdr.bitsPerSample==8?AL_FORMAT_STEREO8:AL_FORMAT_STEREO16));
            freq=fmtHdr.samplesPerSec;
            chunkRemaining -= sizeof(WAVFmtHdr);
         }
         else
         {
            stream->Read(&fmtExHdr, sizeof(WAVFmtExHdr));
            chunkRemaining -= sizeof(WAVFmtExHdr);
         }
      }
      // WAV Format header
      else if (!strncmp((const char*)chunkHdr.id,"data",4))
      {
         if (fmtHdr.format==0x0001)
         {
            size=chunkHdr.size;
            data=new char[chunkHdr.size];
            if (data)
            {
               stream->Read(data, chunkHdr.size);
               chunkRemaining -= chunkHdr.size;
            }
            else
               break;
         }
         else if (fmtHdr.format==0x0011)
         {
            //IMA ADPCM
         }
         else if (fmtHdr.format==0x0055)
         {
            //MP3 WAVE
         }
      }
      // WAV Loop header
      else if (!strncmp((const char*)chunkHdr.id,"smpl",4))
      {
         // this struct read is NOT endian safe but it is ok because
         // we are only testing the loops field against ZERO
         stream->Read(&smplHdr, sizeof(WAVSmplHdr));
         loop = (smplHdr.loops ? AL_TRUE : AL_FALSE);
         chunkRemaining -= sizeof(WAVSmplHdr);
      }

      // either we have unread chunk data or we found an unknown chunk type
      // loop and read up to 1K bytes at a time until we have
      // read to the end of this chunk
      char buffer[1024];
      while (chunkRemaining > 0)
      {
         int readSize = NiMin(1024, chunkRemaining);
         stream->Read(buffer, readSize);
         chunkRemaining -= readSize;
      }

      fileHdr.size-=(((chunkHdr.size+1)&~1)+8);

      // read next chunk header...
      stream->Read(&chunkHdr.id[0], 4);
      stream->Read(&chunkHdr.size, sizeof(chunkHdr.size));
      // unread chunk data rounded up to nearest WORD
      chunkRemaining = chunkHdr.size + (chunkHdr.size&1);
   }

   if (data)
   {
      alBufferData(m_buffID, format, data, size, freq);
      delete [] data;
      return (alGetError() == AL_NO_ERROR);
   }
   return false;
}

ALuint AudioBuffer::getALBuffer()
{
	if ( !alcGetCurrentContext() || !m_pStream)
		return 0;

	alGetError();

	if (m_buffID && alIsBuffer(m_buffID))
		return m_buffID;

   alGenBuffers(1, &m_buffID);

   if (alGetError() != AL_NO_ERROR)
	   return 0;

   int length = strlen(m_pszFileName);

   if (length > 3)
   {

	   bool succ = false;
	   if (!stricmp(m_pszFileName + length - 4, ".wav"))
       {
	       succ = readWAV(m_pStream);
       }
	   else
	   if (!stricmp(m_pszFileName + length - 4, ".ogg"))
	   {
			succ = readOgg(m_pStream);
	   }

	   if (succ)
           return m_buffID;
	  
   }

   alDeleteBuffers(1, &m_buffID);
   m_buffID = 0;

   return 0;
}

ALboolean ALInitOpenAL(OPENALFNTABLE& ALFunction)
{
	ALDeviceList *pDeviceList = NULL;
	ALCcontext *pContext = NULL;
	ALCdevice *pDevice = NULL;
	ALint i;
	ALboolean bReturn = AL_FALSE;

	pDeviceList = new ALDeviceList(ALFunction);
	if ((pDeviceList) && (pDeviceList->GetNumDevices()))
	{
		ALFWprintf("\nSelect OpenAL Device:\n");

		for (i = 0; i < pDeviceList->GetNumDevices(); i++) 
		{
			ALFWprintf("%d. %s%s\n", i + 1, pDeviceList->GetDeviceName(i), i == pDeviceList->GetDefaultDevice() ? "(DEFAULT)" : "");
		}

		pDevice = alcOpenDevice(pDeviceList->GetDeviceName(pDeviceList->GetDefaultDevice()));
		if (pDevice)
		{
			pContext = alcCreateContext(pDevice, NULL);
			if (pContext)
			{
				ALFWprintf("\nOpened %s Device\n", alcGetString(pDevice, ALC_DEVICE_SPECIFIER));
				alcMakeContextCurrent(pContext);
				bReturn = AL_TRUE;
			}
			else
			{
				alcCloseDevice(pDevice);
			}
		}

		delete pDeviceList;
	}

	return bReturn;
}

ALboolean ALShutdownOpenAL()
{
	ALCcontext *pContext;
	ALCdevice *pDevice;

	pContext = alcGetCurrentContext();
	pDevice = alcGetContextsDevice(pContext);
	
	alcMakeContextCurrent(NULL);
	alcDestroyContext(pContext);
	alcCloseDevice(pDevice);

	return AL_TRUE;
}
