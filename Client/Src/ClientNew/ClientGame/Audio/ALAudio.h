#ifndef _AL_AUDIO_H_
#define _AL_AUDIO_H_



#include"al.h"
#include"alc.h"
#include"efx.h"
#include"efx-creative.h"
#include"xram.h"
#include "aldlist.h"


typedef unsigned int AUDIOHANDLE;
#define NULL_AUDIOHANDLE 0
#define MAX_AUDIOSOURCES      2048

#define INVALID_SOURCE        0xffffffff


#define AUDIOHANDLE_LOOPING_BIT  (0x80000000)
#define AUDIOHANDLE_STREAMING_BIT  (0x40000000)
#define AUDIOHANDLE_INACTIVE_BIT (0x20000000)
#define AUDIOHANDLE_LOADING_BIT  (0x10000000)

#define HANDLE_MASK             ~(AUDIOHANDLE_LOOPING_BIT | AUDIOHANDLE_INACTIVE_BIT | AUDIOHANDLE_LOADING_BIT)
#define RETURN_MASK             ~(AUDIOHANDLE_INACTIVE_BIT | AUDIOHANDLE_LOADING_BIT)

#define MIN_GAIN              0.05f             // anything with lower gain will not be started
#define MIN_UNCULL_PERIOD     500               // time before buffer is checked to be unculled
#define MIN_UNCULL_GAIN       0.1f              // min gain of source to be unculled

#define ALX_DEF_SAMPLE_RATE      44100          // default values for mixer
#define ALX_DEF_SAMPLE_BITS      16
#define ALX_DEF_CHANNELS         2

#define AL_GAIN_LINEAR                  0xFF01

#define FORCED_OUTER_FALLOFF  10000.f           // forced falloff distance
static bool mDisableOuterFalloffs = false;      // forced max falloff?
static float mInnerFalloffScale = 1.f;            // amount to scale inner falloffs

static const double logtab[] = {
   0.00,    0.001,   0.002,   0.003,   0.004,
   0.005,   0.01,    0.011,   0.012,   0.013,
   0.014,   0.015,   0.016,   0.02,    0.021,
   0.022,   0.023,   0.024,   0.025,   0.03,
   0.031,   0.032,   0.033,   0.034,   0.04,
   0.041,   0.042,   0.043,   0.044,   0.05,
   0.051,   0.052,   0.053,   0.054,   0.06,
   0.061,   0.062,   0.063,   0.064,   0.07,
   0.071,   0.072,   0.073,   0.08,    0.081,
   0.082,   0.083,   0.084,   0.09,    0.091,
   0.092,   0.093,   0.094,   0.10,    0.101,
   0.102,   0.103,   0.11,    0.111,   0.112,
   0.113,   0.12,    0.121,   0.122,   0.123,
   0.124,   0.13,    0.131,   0.132,   0.14,
   0.141,   0.142,   0.143,   0.15,    0.151,
   0.152,   0.16,    0.161,   0.162,   0.17,
   0.171,   0.172,   0.18,    0.181,   0.19,
   0.191,   0.192,   0.20,    0.201,   0.21,
   0.211,   0.22,    0.221,   0.23,    0.231,
   0.24,    0.25,    0.251,   0.26,    0.27,
   0.271,   0.28,    0.29,    0.30,    0.301,
   0.31,    0.32,    0.33,    0.34,    0.35,
   0.36,    0.37,    0.38,    0.39,    0.40,
   0.41,    0.43,    0.50,    0.60,    0.65,
   0.70,    0.75,    0.80,    0.85,    0.90,
   0.95,    0.97,    0.99 };
const int logmax = sizeof logtab / sizeof *logtab;

float DBToLinear(float value);
float linearToDB(float value);

inline bool areEqualHandles(AUDIOHANDLE a, AUDIOHANDLE b)
{
   return((a & HANDLE_MASK) == (b & HANDLE_MASK));
}


class OggVorbisFile;

NiSmartPointer(AudioBuffer);
class AudioBuffer : public NiRefObject
{
public:
	AudioBuffer();
	~AudioBuffer();

	static AudioBufferPtr find(const char* filename);

	bool open(const char* filename);
	ALuint getALBuffer();

protected:
	bool readOgg(NiFile* stream);
	long oggRead(OggVorbisFile* vf, char* buffer, int length, int bigendianp, int* bitstream);
	bool readWAV(NiFile* stream);

private:
	NiFile* m_pStream;
	ALuint m_buffID;
	char* m_pszFileName;
};

// OpenAL initialization and shutdown
ALboolean ALInitOpenAL(OPENALFNTABLE& ALFunction);
ALboolean ALShutdownOpenAL();


#endif //_AL_AUDIO_H_

