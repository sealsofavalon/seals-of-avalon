
#ifndef _AUDIO_SOURCE_
#define _AUDIO_SOURCE_

#include <audio/AudioSource.h>
#include <audio/ALAudio.h>

typedef unsigned int AUDIOHANDLE;

class audio_load_task;
NiSmartPointer(ALAudioSource);

class ALAudioSource : public AudioSource
{
public:
	ALAudioSource(unsigned int uiType = TYPE_DEFAULT);
	virtual ~ALAudioSource();

	void Release();
    virtual bool SetConeData(float fAngle1Deg, 
        float fAngle2Deg, float fGain);
    virtual void GetConeData(float& fAngle1Deg, 
        float& fAngle2Deg, float& fGain);

    virtual bool SetMinMaxDistance(float fMin, float fMax);
    virtual void GetMinMaxDistance(float& fMin, float& fMax);
    
    virtual bool SetGain(float fGain);
    virtual float GetGain();

    virtual bool SetPlaybackRate(long lRate);
    virtual long GetPlaybackRate();

	virtual void SetFilename(const char* pcFilename);

    virtual bool Load();
    virtual bool Unload();

    virtual bool Play();
    virtual bool Stop();
    virtual void Rewind();

	virtual AudioSource::Status GetStatus();
    
    virtual bool SetPlayTime(float fTime);
    virtual float GetPlayTime();
    virtual bool GetPlayLength(float& fTime);
    
    virtual bool SetPlayPosition(unsigned int uiPos);
    virtual unsigned int GetPlayPosition();   

    virtual bool SetRoomEffectLevel(float fLevel);
    virtual float GetRoomEffectLevel();

    virtual bool SetOcclusionFactor(float fLevel);
    virtual float GetOcclusionFactor();

    virtual bool SetObstructionFactor(float fLevel);
    virtual float GetObstructionFactor();

    virtual NiPoint3 GetPosition();
    virtual void GetOrientation(NiPoint3& kDir, NiPoint3& kUp);
    virtual NiPoint3 GetWorldVelocity() { return m_kLocalVelocity; };

	virtual void DelayStop( uint32 seconds );


	void Update(float fTime);

	bool RealPlay();
	virtual int  GetPlayState(){
		int flag = 0;
		flag = interlocked_read( &m_wantplay );
		return flag;
	}
	void ThreadLoad();
	virtual void PlayAtOnce();
	AUDIOHANDLE GetHandle() const {
		return m_Handle;
	}

	AudioBufferPtr GetBuffer() {
		return m_spBuffer;
	}


	


	static volatile boost::uint32_t s_maxthreadtask;
protected:
    void UpdateWorldTransfrom();

private:
	AUDIOHANDLE m_Handle;
	Status m_eDoneStatus;
	Status m_ePrevStatus;
	AudioBufferPtr m_spBuffer;
	int m_iType;

	volatile boost::uint32_t m_wantplay;
};



#endif //_AUDIO_SOURCE_