#ifndef _AUDIO_SYSTEM_
#define _AUDIO_SYSTEM_

#include "ALAudio.h"
#include "AudioSystem.h"

struct LoopingImage
{
	AUDIOHANDLE handle;
	ALuint source;
	ALuint buffer;
	ALint state;
	unsigned int maxloopcount;
	unsigned int currentcount;

	void Clear()
	{
		handle = NULL_AUDIOHANDLE;
		source = 0;
		buffer = 0;
		state = AL_INITIAL;
		maxloopcount = 0;
		currentcount = 0;
	}

	LoopingImage(AUDIOHANDLE h)
	{
		Clear();
		handle = h;
		handle &= HANDLE_MASK;
	}
};

class ALListener;
class ALAudioSource;

class ALAudioSystem : public AudioSystem
{
public:
	enum
	{
		NUM_AUDIOTYPES = 32,
	};

	static void Initialize();
	static void Release();

    virtual bool Startup(const char* pcDirectoryname);
    virtual void Shutdown();
	
	virtual char* GetLastError(){ return 0; }
	virtual bool GetReverbAvailable(){ return false; }
	virtual bool  SetCurrentRoomReverb(unsigned int uiPreset){ return false; }
	virtual unsigned int GetCurrentRoomReverb(){ return 0; }

	virtual ~ALAudioSystem();

    AudioSource* CreateSource(unsigned int uiType = 
        AudioSource::TYPE_DEFAULT);
	virtual void AddEffectSound( AudioSource* pSound )
	{
		m_listEffectAudio.push_back( pSound );
	}
	virtual void AddToContinueSound( AudioSource* pSound )
	{
		m_listPlayList.push_back( pSound );
	}

	virtual SpeakerType GetSpeakerType(){ return TYPE_3D_2_SPEAKER; }
	virtual bool SetSpeakerType(unsigned int uiType = TYPE_3D_2_SPEAKER){ return false; }
	virtual bool SetBestSpeakerTypeAvailable(){ return false; }

    virtual void Update(float fTime, bool bUpdateAll = false);
	void Do_UpdateSystem(float fTime, bool bUpdateAll);

	bool IsPlaying(AUDIOHANDLE handle) const;

	void SetAudioTypeVolume(int type, float volume);
	float GetAudioTypeVolume(int type) const;

	void SetMasterVolume(float volume);
	float GetMasterVolume() const;

	AudioBufferPtr LoadALSource(ALAudioSource* src);
	AUDIOHANDLE CreateALSourceHandle(ALAudioSource* src);

	void ReleaseALSourceHandle(ALAudioSource* src);

	bool GetSourceStatus(AUDIOHANDLE handle, AudioSource::Status& status) const;
	AudioSource::Status GetStreamStatus(AUDIOHANDLE handle) const;

	void ALSetSourceVolume(AUDIOHANDLE handle, float vol);

	// alfunc
	AUDIOHANDLE ALPlay(AUDIOHANDLE handle);
	bool ALStop(AUDIOHANDLE handle);
	void ALSourcef(AUDIOHANDLE handle, ALenum name, ALfloat value);
	void ALSourcevf(AUDIOHANDLE handle, ALenum name, ALfloat *values);
	void ALSource3f(AUDIOHANDLE handle, ALenum name, ALfloat val1, ALfloat val2, ALfloat val3);
	void ALSourcei(AUDIOHANDLE handle, ALenum name, ALint value);
	void ALSourcePD(AUDIOHANDLE handle, const NiPoint3& pos, const NiPoint3& dir);
	void ALGetSourcef(AUDIOHANDLE handle, ALenum name, ALfloat *value);
	void ALGetSourcefv(AUDIOHANDLE handle, ALenum name, ALfloat* values);
	void ALGetSource3f(AUDIOHANDLE handle, ALenum name, ALfloat* val1, ALfloat* val2, ALfloat* val3);
	void ALGetSourcei(AUDIOHANDLE handle, ALenum name, ALint *value) const;
	void ALListener3f(ALenum name, ALfloat val1, ALfloat val2, ALfloat val3);
	void ALListenerfv(ALenum name, ALfloat* val);
	void ALGetListener3f(ALenum name, ALfloat* val1, ALfloat* val2, ALfloat* val3);
	void ALGetListenerfv(ALenum name, ALfloat* values);

	void CheckError() const;

    ALListener* GetListener();
	void SetSourceLoopCount(AUDIOHANDLE handle, unsigned int loopcount);

protected:
	ALAudioSystem();

	bool CullSource(unsigned int* index, float volume) { return false; }

	void UpdateMaxDistance();
	void UpdateScores(bool sourceOnly);
	void LoopingUpdate();

	AUDIOHANDLE GetNewHandle();
	bool FindFreeSource(unsigned int& index) const;
	unsigned int FindIndex(AUDIOHANDLE handle) const;
	ALuint FindSource(AUDIOHANDLE handle) const;

	float approximate3DVolume(float maxdist, float refdist, const NiPoint3& p);

	OPENALFNTABLE ALFunction;
private:

	std::list< AudioSourcePtr >  m_listEffectAudio;
	std::list< AudioSourcePtr >  m_listPlayList;
	AudioSourcePtr m_pSoundListPlaying;
	unsigned int m_iRequestSources;
	unsigned int m_iNumSources;
	ALuint m_Sources[MAX_AUDIOSOURCES];
	AUDIOHANDLE m_Handle[MAX_AUDIOSOURCES];
	float m_fScore[MAX_AUDIOSOURCES];
	unsigned int m_iType[MAX_AUDIOSOURCES];
	float m_TypeVolume[MAX_AUDIOSOURCES];
	float m_SourceVolume[MAX_AUDIOSOURCES];

	float m_AudioTypeVolume[NUM_AUDIOTYPES];
	float m_fMasterVolume;

	AUDIOHANDLE m_hLastHandle;

	std::map<AUDIOHANDLE, LoopingImage> m_LoopingList;

	friend class ALAudioSystemSDM;
    friend class ALAudioSource;
	friend class ALListener;
};

typedef NiPointer<ALAudioSystem> ALAudioSystemPtr;


#endif //_AUDIO_SYSTEM_