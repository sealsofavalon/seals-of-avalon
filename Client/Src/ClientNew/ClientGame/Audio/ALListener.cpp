
#include "stdafx.h"
#include "ALListener.h"

#include "audio/ALAudio.h"
#include "audio/ALAudioSystem.h"


ALListener::ALListener()
{
   /** 
    * Specify the current location in three dimensional space.
    * OpenAL, like OpenGL, uses a right handed coordinate system,
    *  where in a frontal default view X (thumb) points right, 
    *  Y points up (index finger), and Z points towards the
    *  viewer/camera (middle finger). 
    * To switch from a left handed coordinate system, flip the
    *  sign on the Z coordinate.
    * Listener position is always in the world coordinate system.
    */ 
    m_kDirection = NiPoint3(1, 0, 0);
    m_kUp = NiPoint3(0, 1, 0);  

    m_kLocal.m_Translate = NiPoint3(0, 0, 0);

    NiMatrix3 diagMatrix;
    diagMatrix.MakeDiagonal (1.0, 1.0, 1.0);
    m_kLocal.m_Rotate = diagMatrix;
    m_kLocalVelocity = NiPoint3(0, 0, 0);
}

ALListener::~ALListener()
{
    Release();
}

void ALListener::Update()
{
	if (!GetParent())
		return;

	ALAudioSystem* pkAS = (ALAudioSystem*)AudioSystem::GetAudioSystem();

	NiPoint3 worldLoc = GetWorldTranslate();
    NiPoint3 worldDir = GetWorldRotate() * m_kDirection;
    NiPoint3 worldUp =  GetWorldRotate() * m_kUp;
    NiPoint3 worldVel = GetWorldVelocity();

	pkAS->ALListener3f(AL_POSITION, worldLoc.x, worldLoc.y, worldLoc.z);

	float val[6];
	val[0] = worldDir.x;
	val[1] = worldDir.y;
	val[2] = worldDir.z;
	val[3] = worldUp.x;
	val[4] = worldUp.y;
	val[5] = worldUp.z;
	pkAS->ALListenerfv(AL_ORIENTATION, val);
	pkAS->ALListenerfv(AL_VELOCITY, (float*)&worldVel[0]);
}

NiPoint3 ALListener::GetPosition()
{
	ALAudioSystem* pkAS = (ALAudioSystem*)AudioSystem::GetAudioSystem();

	NiPoint3 pos;
	pkAS->ALFunction.alGetListener3f(AL_POSITION, &pos[0], &pos[1], &pos[2]);

	return pos;
}

NiPoint3 ALListener::GetVelocity()
{
	ALAudioSystem* pkAS = (ALAudioSystem*)AudioSystem::GetAudioSystem();

	NiPoint3 vel;
	pkAS->ALFunction.alGetListener3f(AL_VELOCITY, &vel[0], &vel[1], &vel[2]);

	return vel;
}

void ALListener::GetOrientation(NiPoint3& kDir, NiPoint3& kUp)
{
	ALAudioSystem* pkAS = (ALAudioSystem*)AudioSystem::GetAudioSystem();

	float val[6];
	pkAS->ALFunction.alGetListenerfv(AL_ORIENTATION, val);

	kDir.x = val[0];
	kDir.y = val[1];
	kDir.z = val[2];

	kUp.x = val[3];
	kUp.y = val[4];
	kUp.z = val[5];
}

void ALListener::SetDirectionVector(const NiPoint3& kDir)
{
    m_kDirection = kDir;
}

const NiPoint3& ALListener::GetDirectionVector() const
{
    return m_kDirection;
}

void ALListener::SetUpVector(const NiPoint3& kUp)
{
    m_kUp = kUp;
}

const NiPoint3& ALListener::GetUpVector() const
{
    return m_kUp;
}

