#ifndef _LISTENER_H_
#define _LISTENER_H_

#include <audio/Listener.h>


class ALListener : public AudioListener
{
public:
	virtual void Startup(){};

	virtual void  SetDirectionVector(const NiPoint3& kDir);
	virtual const NiPoint3& GetDirectionVector() const;

	virtual void  SetUpVector(const NiPoint3& kUp);
	virtual const NiPoint3& GetUpVector() const;

	virtual void Update();

	virtual NiPoint3 GetPosition();
	virtual NiPoint3 GetVelocity();
	virtual void GetOrientation(NiPoint3& kDir, NiPoint3& kUp);

    virtual NiPoint3 GetWorldVelocity() { return m_kLocalVelocity; };

protected:
	ALListener();
	virtual ~ALListener();

	friend class ALAudioSystem;

};


#endif //_LISTENER_H_

