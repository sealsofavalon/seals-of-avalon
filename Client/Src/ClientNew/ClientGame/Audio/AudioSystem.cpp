// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "stdafx.h"

#include "Listener.h"
#include "AudioSystem.h"
#include "AudioSource.h"
#include <NiBool.h>
#include <NiMaterialProperty.h>
#include <NiNode.h>
#include <NiTriShape.h>

#include "audio/ALAudioSystem.h"
#include "audio/ALListener.h"


AudioSystem* AudioSystem::ms_pAudioSystem = NULL;

NiImplementRTTI(AudioSystem, NiObject);

//---------------------------------------------------------------------------
AudioSystem::AudioSystem()
{
    m_pSources = NiNew NiTPointerList<AudioSource*>;
    NIASSERT(m_pSources);

    m_fUnitsPerMeter = 1.0;     // set units to meters
    m_uFlags = 0;

    m_fMusicGainScale = 0.3f;
    m_f3DSoundGainScale = 0.3f;
    m_fSoundGainScale = 0.3f;

	m_fMusicGainScaleInMute = 0.3f;
	m_f3DSoundGainScaleInMute = 0.3f;
	m_fSoundGainScaleInMute = 0.3f;

}
//---------------------------------------------------------------------------
AudioSystem::~AudioSystem()
{
    Shutdown();

    // 
    // Decrement/destroy listener
    //
    m_spListener = 0;
    NiDelete m_pSources;
}
//---------------------------------------------------------------------------
AudioListener* AudioSystem::GetListener() 
{
    return m_spListener;
}
//---------------------------------------------------------------------------
void AudioSystem::Shutdown() 
{
    if (m_pSources)
    {
        //
        // Any sources that have already released themselves will have been
        // removed from this list.
        //
        NiTListIterator pos = m_pSources->GetHeadPos();
        while (pos)
        {
            AudioSource* pSource = m_pSources->GetNext(pos);
            pSource->Unload();
        }

        m_pSources->RemoveAll();
    }

    //
    // Release and decrement/destroy the listener
    //
    if (m_spListener)
        m_spListener->Release();

}
//---------------------------------------------------------------------------  
void AudioSystem::AddSource(AudioSource* pSource)
{
    m_pSources->AddTail(pSource);
}
//---------------------------------------------------------------------------  
void AudioSystem::RemoveSource(AudioSource* pSource)
{
    if (pSource->GetRefCount() == 0)
        m_pSources->Remove(pSource);
}
//---------------------------------------------------------------------------
void AudioSystem::Update(float fTime, bool bUpdateAll)
{
    if (!m_spListener)
        return;

    if (bUpdateAll)
    {
        //
        // Update Listener
        //
        m_spListener->Update();

        //
        // Update Sources 
        //
        UpdateAllSources(fTime);
    }

    NIMETRICS_AUDIO_RECORDMETRICS();
}
//---------------------------------------------------------------------------
void AudioSystem::PlayAllSources()
{
    NiTListIterator pos = m_pSources->GetHeadPos();
    while (pos)
    {
        AudioSource* pSource = m_pSources->GetNext(pos);
        pSource->Play();
    }
}
//---------------------------------------------------------------------------
void AudioSystem::StopAllSources()
{
    NiTListIterator pos = m_pSources->GetHeadPos();
    while (pos)
    {
        AudioSource* pSource = m_pSources->GetNext(pos);
        pSource->Stop();
    }
}
//---------------------------------------------------------------------------
void AudioSystem::UpdateAllSources(float fTime)
{
    NiTListIterator pos = m_pSources->GetHeadPos();
    while (pos)
    {
        AudioSource* pSource = m_pSources->GetNext(pos);

		if (pSource->GetRefCount() == 0 )
		{
			if( pSource->GetPlayState() == 3 )
				continue;

			m_pSources->Remove(pSource);
			continue;
		}
		
        pSource->Update(fTime);
    }
}
//---------------------------------------------------------------------------
AudioSource* AudioSystem::GetFirstSource(NiTListIterator& iter) 
{
    if (m_pSources)
    {
        iter = m_pSources->GetHeadPos();
        return (iter ? m_pSources->GetNext(iter) : 0);
    }
    else
    {
        return 0;
    }
}
//---------------------------------------------------------------------------
AudioSource* AudioSystem::GetNextSource(NiTListIterator& iter)
{
    if (m_pSources)
        return (iter ? m_pSources->GetNext(iter) : 0);
    else
        return 0;
}
//---------------------------------------------------------------------------  
AudioSource* AudioSystem::FindDuplicateSource(AudioSource* pkOriginal)
{
    // Check to see if there is already a source with the same filename.
    // If so, duplicate the source rather than create a new one.  
    NiTListIterator iter;
    AudioSource* pkSource = GetFirstSource(iter);

    while (pkSource)
    {
        if ((pkOriginal != pkSource) && pkSource->GetAllowSharing() &&
            pkSource->GetLocalName() && 
            (!strcmp(pkOriginal->GetLocalName(), pkSource->GetLocalName())))
        {
            return pkSource;
        }
        pkSource = GetNextSource(iter);
    }

    return NULL;
}
//---------------------------------------------------------------------------  
bool AudioSystem::SetUnitsPerMeter(float fUnits)
{
    if (fUnits <= 0.0)
        return false;

    m_fUnitsPerMeter = fUnits;
    return true;
}
//---------------------------------------------------------------------------
// Streaming
//---------------------------------------------------------------------------
NiObject* AudioSystem::CreateObject()
{
    return AudioSystem::GetAudioSystem();
}
//---------------------------------------------------------------------------
void AudioSystem::LoadBinary (NiStream& stream)
{
    NiObject::LoadBinary(stream);
    float fVal1;
    NiStreamLoadBinary(stream, fVal1);
    SetUnitsPerMeter(fVal1);
    NiStreamLoadBinary(stream, m_uFlags);
}
//---------------------------------------------------------------------------
void AudioSystem::LinkObject(NiStream& stream)
{
    NiObject::LinkObject(stream);
}
//---------------------------------------------------------------------------
bool AudioSystem::RegisterStreamables(NiStream& stream)
{
    if (! NiObject::RegisterStreamables(stream))
        return false;
    
    return true;
}
//---------------------------------------------------------------------------
void AudioSystem::SaveBinary(NiStream& stream)
{
    NiObject::SaveBinary(stream);
    NiStreamSaveBinary(stream, m_fUnitsPerMeter);
    NiStreamSaveBinary(stream, m_uFlags);
}
//---------------------------------------------------------------------------
bool AudioSystem::IsEqual(NiObject* pObject)
{
    if (! NiObject::IsEqual(pObject))
        return false;

    AudioSystem* pSS = (AudioSystem*)pObject;

    if (m_fUnitsPerMeter != pSS->GetUnitsPerMeter()) 
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
AudioSystem* AudioSystem::GetAudioSystem()
{
    return ms_pAudioSystem;
}
//---------------------------------------------------------------------------
float AudioSystem::GetUnitsPerMeter()
{
    return m_fUnitsPerMeter;
}
//---------------------------------------------------------------------------
void AudioSystem::SetMusicGainScale(float fGainScale)
{
    m_fMusicGainScale = fGainScale;

    NiTListIterator pos = m_pSources->GetHeadPos();
    while (pos)
    {
        AudioSource* pSource = m_pSources->GetNext(pos);
        if(pSource->GetStreamed())
            pSource->SetGain(pSource->GetGain());
    }
}

void AudioSystem::Set3DSoundGainScale(float fGainScale)
{
    m_f3DSoundGainScale = fGainScale;

    NiTListIterator pos = m_pSources->GetHeadPos();
    while (pos)
    {
        AudioSource* pSource = m_pSources->GetNext(pos);
        if(pSource->GetType() == AudioSource::TYPE_3D)
            pSource->SetGain(pSource->GetGain());
    }
}

void AudioSystem::SetSoundGainScale(float fGainScale)
{
    m_fSoundGainScale = fGainScale;

    NiTListIterator pos = m_pSources->GetHeadPos();
    while (pos)
    {
        AudioSource* pSource = m_pSources->GetNext(pos);
        if(pSource->GetType() != AudioSource::TYPE_3D && !pSource->GetStreamed())
            pSource->SetGain(pSource->GetGain());
    }
}


void AudioSystem::Mute(BOOL isMute)
{
	if ( m_isMute == isMute )
	{
		return;
	}

	m_isMute = isMute;
	
	if ( isMute )
	{
		m_fMusicGainScaleInMute = m_fMusicGainScale;
		m_f3DSoundGainScaleInMute = m_f3DSoundGainScale;
		m_fSoundGainScaleInMute = m_fSoundGainScale;
		
		SetSoundGainScale(0.f);
		SetMusicGainScale(0.f);
		Set3DSoundGainScale(0.f);
	}
	else
	{
		SetSoundGainScale(m_fSoundGainScaleInMute);
		SetMusicGainScale(m_fMusicGainScaleInMute);
		Set3DSoundGainScale(m_f3DSoundGainScaleInMute);
	}
}
