#ifndef _WAVSTREAMSOURCE_H_
#define _WAVSTREAMSOURCE_H_


class WavStreamSource
{
	public:
		WavStreamSource(const char *filename);
		virtual ~WavStreamSource();

		virtual bool initStream();
		virtual bool updateBuffers();
		virtual void freeStream();
      virtual float getElapsedTime();
      virtual F32 getTotalTime();

	private:
		ALuint				    mBufferList[NUMBUFFERS];
		int						mNumBuffers;
		int						mBufferSize;
		Stream				   *stream;

		bool					bReady;
		bool					bFinished;

		ALenum  format;
		ALsizei size;
		ALsizei freq;

		ALuint			DataSize;
		ALuint			DataLeft;
		ALuint			dataStart;
		ALuint			buffersinqueue;

		bool			bBuffersAllocated;

		void clear();
		void resetStream();
};

#endif // _AUDIOSTREAMSOURCE_H_
