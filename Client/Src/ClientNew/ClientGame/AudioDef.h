#pragma once

enum eAudioSource
{
	UI_start = 0,
	UI_system = UI_start,
	UI_error,
	UI_button,
	UI_openoanel,
	UI_closepanel,
	UI_weaponequip,
	UI_fabricequip,
	UI_skinequip,
	UI_ironequip,
	UI_neckequip,
	UI_stone,
	UI_usepotion,
	UI_destroyitem,
	UI_moveitem,
	UI_news,
	UI_mail,
	UI_danger,
	UI_map,
	UI_moneyclick,
	UI_ok,
	UI_fail,
	UI_questnew,
	UI_questcompleted,
	UI_questFailed,
	UI_warvictory,
	UI_warfail,
	UI_goodluck,
	UI_makesuccess,
	UI_makefail,
	UI_last,
	UI_title,
	UI_dragSkill,
	UI_dropSkill,
};

const std::string LOGIN_BGM = "sound/music/xifang.ogg";
const std::string SELECT_CREATER_BGM = "sound/selectcreature.wav";

