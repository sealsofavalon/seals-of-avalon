#include "StdAfx.h"
#include "AudioInterface.h"
#include "audio/AudioSource.h"
#include "Map/Map.h"
#include "ObjectManager.h"

CAudioInterface::CAudioInterface(void)
{
	m_CurrentMusic = NULL;
	memset(m_Sources, 0, sizeof(m_Sources));

	memset(m_UiSources, 0, sizeof(m_UiSources));

	m_bInit = false;
	m_fNextTime = FLT_MAX;

	m_curActZoneID = m_curZoneID = m_curActMapId = -1;
}

CAudioInterface::~CAudioInterface(void)
{

	for ( int index = 0;index < MAX_UISOURCE; index++ )
	{
		if ( m_UiSources[index] )
		{
			m_UiSources[index] = NULL;
		}
		
	}
}

//BOOL CAudioInterface::Initialize(HWND hWnd)
//{
//	m_AudioSystem = (NiMilesAudioSystem*)AudioSystem::GetAudioSystem();
//	if (m_AudioSystem == NULL)
//	{
//		return FALSE;
//	}
//	m_AudioSystem->SetHWnd(hWnd);
//	if (!m_AudioSystem->Startup("Sound\\Miles"))
//	{
//		return FALSE;
//	}
//	m_AudioSystem->SetSpeakerType(AudioSystem::TYPE_3D_2_SPEAKER);
//	m_AudioSystem->SetUnitsPerMeter(1.0f);	// Meter 
//	memset(m_Sources, 0, sizeof(void*)*MAX_SOURCES);
//	m_bInit = true;
//	return TRUE;
//}

BOOL CAudioInterface::Initialize(HWND hWnd)
{
	m_AudioSystem = (ALAudioSystem*)(AudioSystem::GetAudioSystem());
	if (!m_AudioSystem)
		return FALSE;

	m_AudioSystem->Startup("");
	m_bInit = true;

	LoadMedia();

	return TRUE;
}

void CAudioInterface::LoadMedia()
{

    RegisterZoneMusic(4	, "music/zhandou1.ogg");
    RegisterZoneMusic(5	, "music/anning.ogg");
    RegisterZoneMusic(6	, "music/anning.ogg");
    RegisterZoneMusic(7	, "music/zhandou1.ogg");
    RegisterZoneMusic(8	, "music/zhandou1.ogg");
    RegisterZoneMusic(9	, "music/zhandou1.ogg");
    RegisterZoneMusic(10	, "music/zhandou1.ogg");
    RegisterZoneMusic(11	, "music/zhandou1.ogg");
    RegisterZoneMusic(12	, "music/zhandou1.ogg");
    RegisterZoneMusic(13	, "music/zhandou1.ogg");
    RegisterZoneMusic(14	, "music/renzu.ogg");
    RegisterZoneMusic(15	, "music/renzu.ogg");
    RegisterZoneMusic(16	, "music/zhandou1.ogg");
    RegisterZoneMusic(17	, "music/zhandou1.ogg");
    RegisterZoneMusic(21	, "music/zhandou1.ogg");
    RegisterZoneMusic(22	, "music/shuanghan.ogg");
    RegisterZoneMusic(23	, "music/shuanghan.ogg");
    RegisterZoneMusic(24	, "music/zhandou1.ogg");
    RegisterZoneMusic(25	, "music/zhandou1.ogg");
    RegisterZoneMusic(26	, "music/wuzu.ogg");
    RegisterZoneMusic(27	, "music/zhandou1.ogg");
    RegisterZoneMusic(28	, "music/zhandou1.ogg");
    RegisterZoneMusic(29	, "music/zhandou1.ogg");
    RegisterZoneMusic(32	, "music/zhandou1.ogg");
    RegisterZoneMusic(33	, "music/zhandou1.ogg");
    RegisterZoneMusic(34	, "music/anning.ogg");
    RegisterZoneMusic(35	, "music/anning.ogg");
    RegisterZoneMusic(36	, "music/zhandou1.ogg");
    RegisterZoneMusic(37	, "music/anning.ogg");
    RegisterZoneMusic(38	, "music/anning.ogg");
    RegisterZoneMusic(39	, "music/zhandou1.ogg");
    RegisterZoneMusic(40	, "music/zhandou1.ogg");
    RegisterZoneMusic(41	, "music/zhandou1.ogg");
    RegisterZoneMusic(42	, "music/zhandou1.ogg");
    RegisterZoneMusic(43	, "music/annning.ogg");
    RegisterZoneMusic(44	, "music/zhandou1.ogg");
    RegisterZoneMusic(45	, "music/zhandou1.ogg");
    RegisterZoneMusic(46	, "music/zhandou1.ogg");
    RegisterZoneMusic(47	, "music/zhandou1.ogg");
    RegisterZoneMusic(48	, "music/shenmi.ogg");
    RegisterZoneMusic(49	, "music/zhandou1.ogg");
    RegisterZoneMusic(50	, "music/gulao.ogg");
    RegisterZoneMusic(51	, "music/zhandou1.ogg");
    RegisterZoneMusic(52	, "music/zhandou1.ogg");
    RegisterZoneMusic(53	, "music/gulao.ogg");
    RegisterZoneMusic(54	, "music/zhandou1.ogg");
    RegisterZoneMusic(55	, "music/zhandou1.ogg");
    RegisterZoneMusic(56	, "music/zhandou1.ogg");
    RegisterZoneMusic(57	, "music/zhandou1.ogg");
    RegisterZoneMusic(58	, "music/zhandou1.ogg");
    RegisterZoneMusic(59	, "music/zhandou1.ogg");
    RegisterZoneMusic(60	, "music/zhandou1.ogg");
    RegisterZoneMusic(61	, "music/zhandou1.ogg");
    RegisterZoneMusic(62	, "music/zhandou1.ogg");
    RegisterZoneMusic(63	, "music/zhandou1.ogg");
    RegisterZoneMusic(64	, "music/zhandou1.ogg");
    RegisterZoneMusic(65	, "music/diyu.ogg");
    RegisterZoneMusic(66	, "music/zhandou1.ogg");
    RegisterZoneMusic(67	, "music/zhandou1.ogg");
    RegisterZoneMusic(68	, "music/zhandou1.ogg");
    RegisterZoneMusic(69	, "music/zhandou1.ogg");
    RegisterZoneMusic(70	, "music/zhandou1.ogg");
    RegisterZoneMusic(71	, "music/zhandou1.ogg");
	RegisterZoneMusic(71    , "wind_mountain_1.ogg");
    RegisterZoneMusic(72	, "music/zhandou2.ogg");
    RegisterZoneMusic(73	, "music/zhandou3.ogg");
    RegisterZoneMusic(74	, "music/zhandou1.ogg");
    RegisterZoneMusic(75	, "music/anning.ogg");
    RegisterZoneMusic(76	, "music/zhandou1.ogg");
    RegisterZoneMusic(77	, "music/zhandou1.ogg");
    RegisterZoneMusic(78	, "music/anning.ogg");
    RegisterZoneMusic(79	, "music/zhandou1.ogg");
    RegisterZoneMusic(80	, "music/zhandou1.ogg");
    RegisterZoneMusic(81	, "music/zhandou1.ogg");
    RegisterZoneMusic(82	, "music/zhandou1.ogg");
    RegisterZoneMusic(83	, "music/zhandou1.ogg");
    RegisterZoneMusic(84	, "music/zhandou1.ogg");
    RegisterZoneMusic(85	, "music/zhandou1.ogg");
    RegisterZoneMusic(86	, "music/zhandou1.ogg");
    RegisterZoneMusic(87	, "music/zhandou1.ogg");
    RegisterZoneMusic(88	, "music/zhandou1.ogg");
    RegisterZoneMusic(89	, "music/zhandou1.ogg");
    RegisterZoneMusic(90	, "music/xifang.ogg");
    RegisterZoneMusic(91	, "music/zhandou1.ogg");
    RegisterZoneMusic(92	, "music/zhandou1.ogg");
    RegisterZoneMusic(93	, "music/zhandou1.ogg");
    RegisterZoneMusic(94	, "music/tina.ogg");
    RegisterZoneMusic(95	, "music/zhandou1.ogg");
    RegisterZoneMusic(96	, "music/zhandou1.ogg");
    RegisterZoneMusic(97	, "music/zhandou1.ogg");
    RegisterZoneMusic(98	, "music/zhandou1.ogg");
    RegisterZoneMusic(99	, "music/zhandou1.ogg");
    RegisterZoneMusic(100, "music/zhandou1.ogg");
    RegisterZoneMusic(101, "music/zhandou1.ogg");
    RegisterZoneMusic(102, "music/buluo.ogg");
    RegisterZoneMusic(103, "music/zhandou1.ogg");
    RegisterZoneMusic(104, "music/zhandou1.ogg");
    RegisterZoneMusic(105, "music/anning.ogg");
    RegisterZoneMusic(106, "music/gulao.ogg");
    RegisterZoneMusic(107, "music/zhandou1.ogg");
    RegisterZoneMusic(108, "music/zhandou1.ogg");
    RegisterZoneMusic(109, "music/shenmi.ogg");
    RegisterZoneMusic(110, "music/zhandou1.ogg");
    RegisterZoneMusic(111, "music/zhandou1.ogg");
    RegisterZoneMusic(112, "music/zhandou1.ogg");
    RegisterZoneMusic(113, "music/zhandou1.ogg");
    RegisterZoneMusic(114, "music/anning.ogg");
    RegisterZoneMusic(115, "music/shenmi.ogg");
    RegisterZoneMusic(116, "music/shenmi.ogg");
    RegisterZoneMusic(117, "music/zhandou1.ogg");
    RegisterZoneMusic(118, "music/tiankongzhicheng.ogg");
    RegisterZoneMusic(119, "music/zhandou1.ogg");
    RegisterZoneMusic(120, "music/zhandou1.ogg");
    RegisterZoneMusic(121, "music/zhandou1.ogg");
    RegisterZoneMusic(122, "music/zhandou1.ogg");
    RegisterZoneMusic(123, "music/zhandou1.ogg");
    RegisterZoneMusic(124, "music/zhandou1.ogg");
    RegisterZoneMusic(125, "music/zhandou1.ogg");
    RegisterZoneMusic(126, "music/lianmeng.ogg");
    RegisterZoneMusic(127, "music/zhandou1.ogg");
    RegisterZoneMusic(128, "music/zhandou1.ogg");
    RegisterZoneMusic(129, "music/zhandou1.ogg");
    RegisterZoneMusic(130, "music/zhandou1.ogg");
    RegisterZoneMusic(131, "music/diyu.ogg");
    RegisterZoneMusic(132, "music/diyu.ogg");
    RegisterZoneMusic(133, "music/shenmi.ogg");
    RegisterZoneMusic(134, "music/shenmi.ogg");
    RegisterZoneMusic(135, "music/shenmi.ogg");
    RegisterZoneMusic(136, "music/dixiacheng.ogg");
    RegisterZoneMusic(137, "music/gulao.ogg");
    RegisterZoneMusic(138, "music/zhandou2.ogg");
    RegisterZoneMusic(139, "music/zhandou3.ogg");
    RegisterZoneMusic(140, "music/zhandou3.ogg");
    RegisterZoneMusic(141, "music/zhandou3.ogg");
    RegisterZoneMusic(142, "music/zhandou3.ogg");
    RegisterZoneMusic(143, "music/zhandou3.ogg");
    RegisterZoneMusic(144, "music/zhandou3.ogg");
    RegisterZoneMusic(146, "music/renzu.ogg");
    RegisterZoneMusic(147, "music/yaozu.ogg");
    RegisterZoneMusic(148, "music/wuzu.ogg");
    RegisterZoneMusic(149, "music/zhandou1.ogg");
    RegisterZoneMusic(150, "music/zhandou1.ogg");
    RegisterZoneMusic(151, "music/zhandou1.ogg");
    RegisterZoneMusic(152, "music/zhandou1.ogg");
    RegisterZoneMusic(153, "music/zhandou2.ogg");
    RegisterZoneMusic(154, "music/zhandou1.ogg");
    RegisterZoneMusic(155, "music/zhandou1.ogg");
    RegisterZoneMusic(156, "music/zhandou1.ogg");
    RegisterZoneMusic(157, "music/zhandou1.ogg");
    RegisterZoneMusic(158, "music/zhandou1.ogg");
    RegisterZoneMusic(159, "music/zhandou3.ogg");
    RegisterZoneMusic(160, "music/zhandou1.ogg");
    RegisterZoneMusic(161, "music/zhandou1.ogg");
    RegisterZoneMusic(162, "music/zhandou1.ogg");
    RegisterZoneMusic(163, "music/shenmi.ogg");
    RegisterZoneMusic(164, "music/zhandou2.ogg");
    RegisterZoneMusic(165, "music/dixiacheng.ogg");
    RegisterZoneMusic(166, "music/dongfang.ogg");
    RegisterZoneMusic(167, "music/dixiacheng.ogg");
    RegisterZoneMusic(168, "music/diyu.ogg");
	RegisterZoneMusic(169, "music/diyu.ogg");
	RegisterZoneMusic(170, "music/diyu.ogg");
	


	RegisterMapMusic( 68, "music/dixiacheng.ogg" );
	RegisterMapMusic( 69, "music/zhandou1.ogg" );
	//RegisterMapMusic( 70, "music/happy.ogg" );
	//RegisterMapMusic( 71, "music/dixiacheng.ogg" );
	RegisterMapMusic( 58, "music/lianmeng.ogg" );
	RegisterMapMusic( 59, "music/lianmeng.ogg" );
	RegisterMapMusic( 60, "music/lianmeng.ogg" );
	RegisterMapMusic( 61, "music/lianmeng.ogg" );
	RegisterMapMusic( 62, "music/lianmeng.ogg" );
	RegisterMapMusic( 63, "music/lianmeng.ogg" );
	RegisterMapMusic( 64, "music/lianmeng.ogg" );
	RegisterMapMusic( 65, "music/lianmeng.ogg" );
	RegisterMapMusic( 66, "music/lianmeng.ogg" );
	RegisterMapMusic( 67, "music/lianmeng.ogg" );

	//UI_system = UI_start,
	//UI_error,
	//UI_button,
	//UI_openoanel,
	//UI_closepanel,
	//UI_weaponequip,
	//UI_fabricequip,
	//UI_skinequip,
	//UI_ironequip,
	//UI_neckequip,
	//UI_stone,
	//UI_usepotion,
	//UI_destroyitem,
	//UI_moveitem,
	//UI_news,
	//UI_mail,
	//UI_danger,
	//UI_map,
	//UI_moneyclick,
	//UI_ok,
	//UI_fail,
	//UI_questnew,
	//UI_questcompleted,
	//UI_questFailed,
	//UI_warvictory,
	//UI_warfail,
	//UI_goodluck,
	//UI_makesuccess,
	//UI_makefail,

	Register(UI_system, "UI/UI_system.wav");
	Register(UI_error, "UI/UI_error.wav");
	Register(UI_button, "UI/UI_button.wav");
	Register(UI_openoanel, "UI/UI_openoanel.wav");
	Register(UI_closepanel, "UI/UI_closepanel.wav");
	Register(UI_weaponequip, "UI/UI_weaponequip.wav");
	Register(UI_fabricequip, "UI/UI_fabricequip.wav");
	Register(UI_skinequip, "UI/UI_skinequip.wav");
	Register(UI_ironequip, "UI/UI_ironequip.wav");
	Register(UI_neckequip, "UI/UI_neckequip.wav");
	Register(UI_stone, "UI/UI_stone.wav");
	Register(UI_usepotion, "UI/UI_usepotion.wav");
	Register(UI_destroyitem, "UI/UI_destroyitem.wav");
	Register(UI_moveitem, "UI/UI_moveitem.wav");
	Register(UI_news, "UI/UI_news.wav");
	Register(UI_mail, "UI/UI_mail.wav");
	Register(UI_danger, "UI/UI_danger.wav");
	Register(UI_map, "UI/UI_map.wav");
	Register(UI_moneyclick, "UI/UI_moneyclick.wav");
	Register(UI_ok, "UI/UI_ok.wav");
	Register(UI_fail, "UI/UI_fail.wav");
	Register(UI_questnew, "UI/UI_questnew.wav");
	Register(UI_questcompleted, "UI/UI_questcompleted.wav");
	Register(UI_questFailed, "UI/UI_questFailed.wav");
	Register(UI_warvictory, "UI/UI_warvictory.wav");
	Register(UI_warfail, "UI/UI_warfail.wav");
	Register(UI_goodluck, "UI/UI_goodluck.wav");
	Register(UI_makesuccess, "UI/UI_makesuccess.wav");
	Register(UI_makefail, "UI/UI_makefail.wav");
	Register(UI_title, "UI/UI_title.wav");
	Register(UI_dragSkill, "UI/UI_SpellIconPickup.wav");
	Register(UI_dropSkill, "UI/UI_SpellIconDrop.wav");



	/*PreLoad( "music/anning.ogg");
	PreLoad( "music/dixiacheng.ogg");
	PreLoad( "music/diyu.ogg");
	PreLoad( "music/dongfang.ogg");
	PreLoad( "music/gulao.ogg");
	PreLoad( "music/kaixin.ogg");
	PreLoad( "music/lianmeng.ogg");
	PreLoad( "music/renzu.ogg");
	PreLoad( "music/shenmi.ogg");
	PreLoad( "music/shuanghan.ogg");
	PreLoad( "music/tiankongzhicheng.ogg");
	PreLoad( "music/tina.ogg");
	PreLoad( "music/wuzu.ogg");
	PreLoad( "music/xifang.ogg");
	PreLoad( "music/yaozu.ogg");
	PreLoad( "music/yisilan.ogg");
	PreLoad( "music/zhandou1.ogg");
	PreLoad( "music/zhandou2.ogg");
	PreLoad( "music/zhandou3.ogg");*/


}

void CAudioInterface::Destroy()
{
	ReleaseAllSource();
	if (m_AudioSystem)
	{
		m_AudioSystem->Shutdown();
		m_AudioSystem = NULL;
	}
}

void CAudioInterface::ReleaseAllSource()
{
	for (int i = 0; i < MAX_SOURCES; i++)
	{
		AudioSource* Source = m_Sources[i];
		/*if (Source)
		{
			NiDelete Source;
		}*/
		m_Sources[i] = NULL;
	}
	m_SourceMap.clear();
}

void CAudioInterface::ReleaseIdelSource()
{

}

void CAudioInterface::Update(DWORD dt)
{
	if(m_AudioSystem && m_bInit)
	{
		m_AudioSystem->Update(NiGetCurrentTimeInSec(),true);
		QueryMusicState();

		//if (m_CurrentMusic == NULL)
		//{
		//	RandomPlayMusic();
		//}
	}
}

void CAudioInterface::SetSoundStateChangedCBFn(int index, AudioSource::StateChangedCallBack fn, AudioSource::Status eStatus)
{
	if (index < 0 && index >= MAX_SOURCES)
		return;

	m_Sources[index]->SetStateChangedCallBackFn(fn, eStatus);
}

void CAudioInterface::OnArea(unsigned int uiAreaID)
{

	int mapid = CMap::Get()->GetMapId();
	if (m_curActZoneID != uiAreaID)
	{
		NiFixedString activeZoneNm = GetZoneMusic(m_curActZoneID);
		if (activeZoneNm.Exists())
		{
			int ind = FindSound(activeZoneNm);
			if (ind != -1)
			{
				AudioSource* pkAct = GetSource(ind);
				if (pkAct && pkAct->GetStatus() == AudioSource::PLAYING)
				{
					if ( m_curActMapId !=  mapid )
					{
						pkAct->FadeToGain(0.0f, NiGetCurrentTimeInSec(), 1.0f);
					}
					else
						pkAct->FadeToGain(0.0f, NiGetCurrentTimeInSec(), 3.0f);
				}
			}
		}
	}


	if ( m_curActMapId !=  mapid )
	{
		NiFixedString activeMapNm = GetMapMusic(m_curActMapId);
		if (activeMapNm.Exists())
		{
			int ind = FindSound(activeMapNm);
			if (ind != -1)
			{
				AudioSource* pkAct = GetSource(ind);
				if (pkAct && pkAct->GetStatus() == AudioSource::PLAYING)
				{
					pkAct->FadeToGain(0.0f, NiGetCurrentTimeInSec(), 1.0f);
				}
			}
		}
		m_curActMapId = mapid;
	}
	//m_curActZoneID = uiAreaID;

	NiFixedString zmn;
	if ( uiAreaID == 0 )
	{
		zmn = GetMapMusic( mapid );
	}
	else
		zmn = GetZoneMusic(uiAreaID);

	if(zmn.Exists())
	{
		int ind = FindSound(zmn);
		if (ind != -1)
		{
			AudioSource* pkMzk = GetSource(ind);
			if (pkMzk && pkMzk->GetStatus() == AudioSource::PLAYING)
			{
				pkMzk->FadeToGain(1.0f, NiGetCurrentTimeInSec(), 7.0f);
				m_CurrentMusic = pkMzk;
				m_curActZoneID = uiAreaID;
			}
			else
			if (pkMzk && pkMzk->GetStatus() != AudioSource::PLAYING)
			{
				pkMzk->SetGain(0.0f);
				if (pkMzk->Load())
					pkMzk->Play();

				pkMzk->FadeToGain(1.0f, NiGetCurrentTimeInSec(), 5.0f);
				m_CurrentMusic = pkMzk;
				m_curActZoneID = uiAreaID;

			}
		}
		else
		{
			if (PlayMusic(zmn) != -1)
			{
				m_curActZoneID = uiAreaID;
			}
		}
	}
	else
	{
		zmn = GetZoneMusic(uiAreaID);
	}
}

int CAudioInterface::PlaySound(AudioSource* voice, const char* szName)
{
	if (m_AudioSystem == NULL)
	{
		return -1;
	}

	//voice->Unload();

	if ( voice->GetPlayState() == 1 )
	{
		return -1;
	}

	voice->SetFilename(szName);
	if (!voice->Load())
	{
		return -1;
	}

	voice->Play();
	AudioSystem::GetAudioSystem()->AddEffectSound( voice );
	return 1;
}

int CAudioInterface::PlaySoundByList(AudioSource* voice, const char* szName)
{
	if (m_AudioSystem == NULL)
	{
		return -1;
	}


	voice->SetFilename(szName);
	if (!voice->Load())
	{
		return -1;
	}

	AudioSystem::GetAudioSystem()->AddToContinueSound( voice );
	return 1;
}

int CAudioInterface::PlaySound(GSID id, const NiPoint3& Pos /*= NiPoint3::ZERO*/, float Volumn /*= 1.0f*/, int LoopCount /*= 1*/)
{
	NiFixedString fn;
	if (!m_SMMap.GetAt(id, fn)) return -1;

	return PlaySound(fn, Pos, Volumn, LoopCount);
}

int CAudioInterface::PlaySound(const char* Name, const NiPoint3& Pos, float Volumn, int loopcount)
{
	//if (m_AudioSystem == NULL)
	//{
	//	return -1;
	//}
	//NiFixedString SoundName = Name;

	//int Index = FindSound(SoundName);
	//AudioSource* Source;
	//if (Index == -1)
	//{
	//	if (Pos != NiPoint3::ZERO)
	//	{

	//	}
	//	else
	//	{
	//		Source = m_AudioSystem->CreateSource(AudioSource::TYPE_AMBIENT);
	//		if (Source == NULL)
	//		{
	//			return -1;
	//		}
	//		Source->SetFilename(Name);
	//		if(!Source->Load())
	//		{
	//			return -1;
	//		}
	//		Index = GetFreeSourceIndex();
	//		m_SourceMap.insert(SourcePair(SoundName, Index));
	//		m_Sources[Index] = Source;
	//	}
	//}

	//if (Index != -1 /*&& !IsPlaying(Index)*/)
	//{
	//	_PlaySound(Index, Pos, Volumn);
	//	return Index;
	//}
	//return Index;
	PlayUiSound( Name, Pos,Volumn,loopcount );
	return 0;
}

void CAudioInterface::PlayUiSound( const char* szName, const NiPoint3& Pos /* = NiPoint3::ZERO */, float Volumn /* = 1.0f */, int loopcount /* = 1 */ )
{
	if (m_AudioSystem == NULL)
	{
		return;
	}

	AudioSource* pSource = NULL;

	for ( int index = 0;index < MAX_UISOURCE; index++ )
	{
		if ( m_UiSources[index] == NULL )
		{
			m_UiSources[index] =  m_AudioSystem->CreateSource(AudioSource::TYPE_AMBIENT);
		}
		pSource = m_UiSources[index];
		if ( pSource == NULL )
		{
			continue;
		}
		if ( pSource->GetStatus() == AudioSource::PLAYING || ( pSource->GetPlayState() == 1 ) || ( pSource->GetPlayState() == 3 ) )
		{
			continue;
		}

		break;
	}

	if (pSource == NULL)
	{
		pSource = m_UiSources[0];
	}


	
	pSource->SetFilename(szName);
	if(!pSource->Load())
	{
		return;
	}
	else
	{
		pSource->Stop();
		pSource->SetGain(Volumn);		// 需要根据声道音量进行缩放.
		pSource->SetLoopCount(1);
		pSource->SetWorldTranslate(Pos);
		pSource->SetIsUpdata(true);
		pSource->Play();
		return;
	}
	

	

}


void CAudioInterface::PlayUiSound( GSID id, const NiPoint3& Pos /* = NiPoint3::ZERO */, float Volumn /* = 1.0f */, int loopcount /* = 1 */ )
{
	NiFixedString fn;
	if (!m_SMMap.GetAt(id, fn)) 
		return;

	PlayUiSound(fn, Pos, Volumn, loopcount);
}
AudioSourcePtr CAudioInterface::Play3DSound(const char* szName, const NiTransform& kLocalTransform, float fMinDistance, float fMaxDistance, float Volumn, int loopcount)
{
	if (!m_AudioSystem)
		return 0;

	AudioSource* pkSource = m_AudioSystem->CreateSource(AudioSource::TYPE_3D);
	if (!pkSource) return pkSource;

	pkSource->SetFilename(szName);
	if (!pkSource->Load()) return 0;

	pkSource->SetLocalTransform(kLocalTransform);
	pkSource->SetGain(Volumn);
	pkSource->SetLoopCount(loopcount);
	pkSource->SetSelectiveUpdate(true);
	pkSource->SetSelectiveUpdateTransforms(true);
	pkSource->SetSelectiveUpdatePropertyControllers(false);
	pkSource->SetSelectiveUpdateRigid(true);
	pkSource->Update(0.0f);
    pkSource->SetMinMaxDistance(fMinDistance, fMaxDistance);

	pkSource->Play();

	return pkSource;
}

int CAudioInterface::PlayMusic(const char* Name, int LoopCnt)
{
	if (m_AudioSystem == NULL)
	{
		return -1;
	}
	
	/*PlayUiSound( Name );

	return 1;*/

	NiFixedString SoundName = Name;

	int Index = FindSound(SoundName);
	if (Index == -1)
	{
		AudioSource* Source = m_AudioSystem->CreateSource(AudioSource::TYPE_AMBIENT);
		if (Source == NULL)
		{
			return -1;
		}
		Source->SetFilename(Name);
		Source->SetStreamed(true);
		Source->SetGain(0.0f);
		Source->SetLoopCount(LoopCnt);
		if(!Source->Load())
		{
			return -1;
		}
		Index = GetFreeSourceIndex();
		m_SourceMap.insert(SourcePair(SoundName, Index));
		m_Sources[Index] = Source;
	}

	if (Index != -1 /*&& !IsPlaying(Index)*/)
	{
		AudioSource* Source = m_Sources[Index];
		Source->SetGain(0.0f);
		Source->SetLoopCount(LoopCnt);
		Source->SetIsUpdata(true);
		//Source->SetFilename(Name);
		_PlaySound(Index);
		Source->FadeToGain(1.f, NiGetCurrentTimeInSec(), 3.0f);
	}
	return Index;
}

BOOL CAudioInterface::_PlaySound(int Index, const NiPoint3& Pos, float Volumn)
{
	NIASSERT(Index >= 0 && Index < MAX_SOURCES);
	StopSound(Index);
	AudioSource* Source = m_Sources[Index];
	Source->SetGain(Volumn);		// 需要根据声道音量进行缩放.
	Source->SetLoopCount(1);
	Source->SetWorldTranslate(Pos);
	// Check Buffer were Loaded ??
	//Source->Update(0.0f);
	Source->Play();
	return TRUE;
}

BOOL CAudioInterface::_PlaySound(int Index)
{
	NIASSERT(Index >= 0 && Index < MAX_SOURCES);
	StopSound(Index);
	AudioSource* Source = m_Sources[Index];
	NIASSERT(Source);
	if(!Source)
	{
		return FALSE;
	}
	Source->SetLoopCount(1);
	if (Source->GetStreamed() )
	{
		m_CurrentMusic = Source;
	}
	// Check Buffer were Loaded ??
	//Source->Update(0.0f);
	Source->Play();
	return TRUE;
}

AudioSource* CAudioInterface::GetSource(int iIndex)
{
	return m_Sources[iIndex];
}

void CAudioInterface::StopSound(int Index)
{
	NIASSERT(Index >= 0 && Index < MAX_SOURCES);
	AudioSource* Source = m_Sources[Index];
//	NIASSERT(Source);
	if(Source)
		Source->Stop();
}
void CAudioInterface::OnlyPlaySound(int Index)
{
	NIASSERT(Index >= 0 && Index < MAX_SOURCES);
	
	for (int i =0; i < MAX_SOURCES; i++)
	{
		if (i != Index)
		{
			AudioSource* Source = m_Sources[i];
			if(Source)
				Source->Stop();
		}
	}
	
}

void CAudioInterface::StopAllSound()
{
	if (m_AudioSystem)
		m_AudioSystem->StopAllSources();
}

void CAudioInterface::QueryMusicState()
{
	if (m_fNextTime <= NiGetCurrentTimeInSec())
	{
		CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
		if (!pkLocalPlayer)
		{
			return ;
		}
		const NiPoint3& kPos = pkLocalPlayer->GetPosition();
		unsigned int uiAreaId = CMap::Get()->GetAreaId(kPos.x, kPos.y);

		OnArea(uiAreaId);
		m_fNextTime = FLT_MAX;
	}

	if (m_CurrentMusic)
	{
		AudioSource::Status State =	m_CurrentMusic->GetStatus();
		if (State == AudioSource::DONE)
		{
			m_CurrentMusic = 0;
			float NextTimeoffset = 20.0f;
			if ( !m_curActZoneID )
			{
				NextTimeoffset = 3.f;
			}

			m_fNextTime = NiGetCurrentTimeInSec() + NextTimeoffset;
		}
	}
}

void CAudioInterface::RandomPlayMusic()
{
#define MusicDir "Sound\\Music\\"
	int Index = NiRand();
	Index %= 10;
	char FileName[MAX_PATH];
	NiSprintf(FileName, MAX_PATH, "%sMusic%.3d.mp3",MusicDir, Index);
	PlayMusic(FileName,1);
}


BOOL CAudioInterface::IsPlaying(INT Index) const
{
	AudioSource* Source = m_Sources[Index];
	if (Source && Source->GetStatus() == AudioSource::PLAYING)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL CAudioInterface::ReleaseSource(INT Index)
{
	AudioSource* Source = m_Sources[Index];
	if(Source)
	{
		NiFixedString FileName = Source->GetFilename();
		if (!FileName)
		{
			return FALSE;
		}
		FixedStringHashMap<int>::iterator it = m_SourceMap.find(FileName);
		if (it != m_SourceMap.end())
		{
			m_SourceMap.erase(it);
			//NiDelete Source;
			m_Sources[Index] = NULL;
			return TRUE;
		}
		return FALSE;
	}
	return TRUE;
}

void CAudioInterface::SetListenerNode(NiNode* pkNode)
{
	ALListener* pkListener = (ALListener*)m_AudioSystem->GetListener();
	if (pkNode)
	{
		pkNode->AttachChild((NiAVObject*)pkListener);
		pkNode->Update(0.0f, false);
	}

	pkListener->SetUpVector(NiPoint3(0.0f, 0.0f, 1.0f));
    pkListener->SetDirectionVector(NiPoint3(0.0f, -1.0f, 0.0f));
	pkListener->Update();
}

void CAudioInterface::Register(GSID id, const NiString& fn)
{
	const NiString spath = "Sound/";
	NiFixedString fullname = spath + fn;

	m_SMMap.SetAt(id, fullname);
}

void CAudioInterface::RegisterZoneMusic(unsigned int zoneid, const NiString& fn)
{
	const NiString spath = "Sound/";
	NiFixedString fullname = spath + fn;

	m_ZoneM.SetAt(zoneid, fullname);
}

void CAudioInterface::RegisterMapMusic(unsigned int mapid, const NiString& fn)
{
	const NiString spath = "Sound/";
	NiFixedString fullname = spath + fn;

	m_MapM.SetAt(mapid, fullname);
}


void CAudioInterface::ChangeMapMusic( const NiString& fn )
{
	int mapid = CMap::Get()->GetMapId();
	NiFixedString fullname = fn;


	NiFixedString zmn = GetMapMusic( mapid );
	if ( zmn.Equals( fn ) )
	{
		return;
	}
	if ( zmn.Exists() )
	{
		int ind = FindSound(zmn);
		if (ind != -1)
		{
			AudioSource* pkMzk = GetSource(ind);
			if (pkMzk && pkMzk->GetStatus() == AudioSource::PLAYING)
			{
				pkMzk->FadeToGain(0.0f, NiGetCurrentTimeInSec(), 1.0f);
			}
		}
	}
	
	
	int CurId = PlayMusic( fullname );

	if ( CurId )
	{
		m_CurrentMusic = GetSource(CurId);
	}

	m_MapM.SetAt(mapid, fullname );

	m_curActMapId = mapid;

}


NiFixedString CAudioInterface::GetSoundFile(GSID sid) const
{
	NiFixedString str;
	m_SMMap.GetAt(sid, str);

	return str;
}

NiFixedString CAudioInterface::GetZoneMusic(unsigned int zoneid) const
{
	NiFixedString str;
	m_ZoneM.GetAt(zoneid, str);

	return str;
}

NiFixedString CAudioInterface::GetMapMusic(unsigned int mapid) const
{
	NiFixedString str;
	m_MapM.GetAt(mapid, str);

	return str;
}


void CAudioInterface::PreLoad( int areaId )
{

	//NiFixedString fn = GetZoneMusic(areaId);


	//const NiString spath = "";
	//NiFixedString fullname = spath + fn;

	//
	//int Index = PlayMusic( fullname );

	//if ( Index != -1)
	//{
	//	StopSound( Index );
	//}

	//FILE* file = NULL;

	//file = fopen("LoadAudio.txt", "a");
	//if ( file )
	//{
	//	fprintf( file, "preload: %s\n", fullname );
	//	fclose(file);
	//}

	
	
	

	/*AudioBufferPtr pbuffer = AudioBuffer::find( fullname );

	if ( pbuffer )
	{
		pbuffer->getALBuffer();
	}*/
}


void CAudioInterface::PreLoadAll()
{
	return;
	NiTMapIterator itMusic = m_ZoneM.GetFirstPos();

	
	std::list< NiFixedString >::iterator itlistMusic;

	for( int i = 0; i < m_ZoneM.GetCount(); ++i )
	{
		unsigned int Id;
		NiFixedString str;
		m_ZoneM.GetNext( itMusic, Id, str );


		bool bLoaded = false;
		for( itlistMusic = m_listMusic.begin(); itlistMusic != m_listMusic.end(); ++itlistMusic )
		{
			if ( *itlistMusic == str )
			{
				bLoaded = true;
				break;
			}
		}

		if ( bLoaded )
			continue;

		PreLoad( Id );
		m_listMusic.push_back( str );

		
	}
}