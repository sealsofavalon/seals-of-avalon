#include "StdAfx.h"
#include "Aura.h"

#include "Skill/SkillInfo.h"
#include "SpellEffect.h"
#include "Skill/SkillManager.h"
#include "Effect/EffectManager.h"


AuraEffect::AuraEffect(void) : m_spellid(0)
{
}

AuraEffect::~AuraEffect(void)
{
}

void AuraEffect::SetOwner(CGameObject* pkOwner)
{
	m_pkOwner = pkOwner;
}
void AuraEffect::SetVisibleEffect(bool visible)
{
	NiTMapIterator pos = m_activeEffMap.GetFirstPos();
	while (pos)
	{
		SpellEffectNode* d;
		CEffectBase* pkEffect;
		m_activeEffMap.GetNext(pos, d, pkEffect);
		if (pkEffect)
		{
			pkEffect->SetVisible(visible);
		}
	}
}
void AuraEffect::Start(int spellid)
{
	NIASSERT(m_pkOwner);
	if (!m_pkOwner)
		return;

	SpellTemplate* pkSpellT = SYState()->SkillMgr->GetSpellTemplate(spellid);
	if (!pkSpellT)
		return;

	if (m_spellid != 0)
		ReleaseActiveEffects();

	m_spellid = spellid;

	std::vector<SpellEffectNode*> activeEffs;
	SYState()->SkillMgr->GetAuraEffects(spellid, TE_AuraStart, TA_Active, activeEffs);

	SpellEffectEmit::EmitParam ep;

	ep.m_iSpellid = spellid;
	ep.m_OwnerID = m_pkOwner->GetGUID();
//	unsigned int ui = 0;
//	ep.m_kTargets = m_targetList;
	ep.m_vEffects = activeEffs;

	std::vector<SpellEffectEmit::EmitResult> results;
	SpellEffectEmit::Get()->Emit(ep, results, false);

	for (unsigned int ui = 0; ui < results.size(); ui++)
	{
		m_activeEffMap.SetAt(results[ui].node, results[ui].instance);
	}
//	unsigned int ui = 0;
//	for (ui; ui < activeEffs.size(); ui++)
//	{
//		CEffectBase* pkEffect = 
//			SYState()->SkillMgr->PopulateEffect(m_pkOwner, 0, activeEffs[ui], pkSpellT);
//		if (pkEffect)
//		{
//			m_activeEffMap.SetAt(activeEffs[ui], pkEffect);
//		}
//	}
}

void AuraEffect::Over()
{
	std::vector<SpellEffectNode*> activeEffs, deactiveEffs;
	SYState()->SkillMgr->GetAuraEffects(m_spellid, TE_AuraOver, TA_Active, activeEffs);
	SYState()->SkillMgr->GetAuraEffects(m_spellid, TE_AuraOver, TA_Deactive, deactiveEffs);

	SpellTemplate* pkSpellT = SYState()->SkillMgr->GetSpellTemplate(m_spellid);
	if (!pkSpellT)
		return;

	ReleaseActiveEffects();

	SpellEffectEmit::EmitParam ep;

	ep.m_iSpellid = m_spellid;
	ep.m_OwnerID = m_pkOwner->GetGUID();
//	unsigned int ui = 0;
//	ep.m_kTargets = m_targetList;
	ep.m_vEffects = activeEffs;

	std::vector<SpellEffectEmit::EmitResult> results;
	SpellEffectEmit::Get()->Emit(ep, results, false);

	for (unsigned int ui = 0; ui < results.size(); ui++)
	{
		m_activeEffMap.SetAt(results[ui].node, results[ui].instance);
	}

	m_spellid = 0;
	//unsigned int ui = 0;
	//for (ui; ui < activeEffs.size(); ui++)
	//{
	//	CEffectBase* pkEffect = 
	//		SYState()->SkillMgr->PopulateEffect(m_pkOwner, 0, activeEffs[ui], pkSpellT);
	//	if (pkEffect)
	//	{
	//		m_activeEffMap.SetAt(activeEffs[ui], pkEffect);
	//	}
	//}
}


void AuraEffect::ReleaseActiveEffects()
{
	NiTMapIterator pos = m_activeEffMap.GetFirstPos();
	while (pos)
	{
		SpellEffectNode* d;
		CEffectBase* pkEffect;
		m_activeEffMap.GetNext(pos, d, pkEffect);
		if (pkEffect)
			EffectMgr->ReleaseEffect(pkEffect, true);
	}
}


