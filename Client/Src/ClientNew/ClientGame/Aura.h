#ifndef _AURA_H_
#define _AURA_H_

#include <NiMemObject.h>

class CGameObject;
struct SpellEffectNode;
class CEffectBase;

class AuraEffect : public NiMemObject
{
public:
	AuraEffect(void);
	~AuraEffect(void);

	void SetOwner(CGameObject* pkOwner);
	void Start(int spellid);
	void SetVisibleEffect(bool visible);
	void Over();

protected:
	void ReleaseActiveEffects();

private:
	NiTPointerMap<SpellEffectNode*, CEffectBase*> m_activeEffMap;
	CGameObject* m_pkOwner;
	int m_spellid;
};


#endif //_AURA_H_