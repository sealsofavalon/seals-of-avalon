#ifndef AVATAR_H
#define AVATAR_H

#include "..\..\..\..\SDBase\Public\ItemDef.h"
#define  ITS_COUNT  20


enum AttachmentSlot
{
	AS_None = -1,
	AS_BacksideRight = 0,
	AS_BacksideLeft,

	MAX_ATTACHMENT_SLOT,
};


enum Avatar_Type
{
	Avatar_Model,
	Avatar_Textrue,
	Avatar_Invalid
};

bool IsMeshLoaded(NiNode* pkRootNode, int iPart, unsigned int uiId);
bool IsTextureLoaded(NiNode* pkRootNode, int iPart, NiFixedString& sTexture);
int GetAvatarPartId(char* sPart);

class CAvatar
{
public:
	struct SAvatarInfo
	{
		unsigned int uiPart;
		int uiAttachmentSlot;
		Avatar_Type eType;
		//NiFixedString sFileName;
		unsigned int uiId;
		float fScale;

		SAvatarInfo()
		{
			uiPart = 0;
			eType = Avatar_Model;
			uiId = 0;
			fScale = 1.0f;
		}
	};

	struct SEffectInfo
	{
		std::string effectname;
		std::string attachnodename;
	};

	bool ChangeAvatar(NiActorManager* pkActorManager, NiObject* pkEquipObject, SAvatarInfo& sAvatarInfo, int iAttachmentSlot = -1);
	bool ChangeAvatar(NiNode* pkRootNode, NiObject* pkEquipObject, SAvatarInfo& sAvatarInfo, int iAttachmentSlot = -1, NiBoneLODController* pkLODCtrl = 0);

	bool AddEffectToEquip(NiNode* pkRootNode, std::string pkeffectFile, SAvatarInfo& sAvatarInfo, int iAttachmentSlot = -1);
	//删除角色的时候释放相关的Avatar资源
	static void FreeAvatar(NiAVObject* pkAVObject);

	bool ChangeAvatarSZ(NiNode* pkRootNode, NiNode* pkEquipNode, const char* szNodeName, NiBoneLODController* pkLODCtrl = 0);

protected:
	//void Equip(NiNode* pkBaseNode, NiNode* pkEquipNode, SAvatarInfo& sAvatarInfo);
	bool SkinAvatar(NiNode* pkRootNode, NiNode* pkEquipNode, SAvatarInfo& sAvatarInfo, NiBoneLODController* pkLODCtrl = 0);
	bool AttachmentAvatar(NiNode* pkRootNode, NiNode* pkEquipNode, SAvatarInfo& sAvatarInfo, int iAttachmentSlot = AS_None );
	//bool ChangeTexture(NiNode* pkRootNode, NiProperty* pkNewProperty, SAvatarInfo& sAvatarInfo);

#ifdef _ComposeTexture_
	bool ComposeTexture(NiTexturingProperty* pDestProperty, NiTexturingProperty* pSrcProperty);
#endif

	// Local member
	//NiFixedString m_sEquipNode;
	//NiFixedString m_sEquipMesh;
};

#endif