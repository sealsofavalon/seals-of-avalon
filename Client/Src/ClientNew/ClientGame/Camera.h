#ifndef CAMERA_H
#define CAMERA_H

// 包装NiCamera , 在这里进行镜头的碰撞,过渡等计算.
#define  Eye_OffSet 2.f
class CCamera
{
public:
	CCamera(void);
	~CCamera(void);
	void SetDefault();
	void Reset();
	const NiCamera& GetNiCamera() const { return m_NiCamera; }
	void SetTransform(const NiTransform& xform);
	void UpdateBindingCharacterInput();
	void Update(float dt);
	void NewUpdate(float dt);
	void Shaking(float fTime, float amplitude = 0.8, float period = 0.4 );
	void Dither(float amplitude, float period);
	const NiPoint3& GetTranslate() const { return  m_kTranslate; }
	
	// 为了避免每帧更新，请在调用TestVisible函数前，确保UpdateFrustumPlanes()被调用更新一次
	void UpdateFrustumPlanes();
	bool TestVisible(const NiBound& kBound);
	void SetCameraZ(int iz);

	bool SetRotateXY(float fX, float fY);

	void LookAt(NiPoint3 kEyePos, NiPoint3 kLookAt);

	void LockView(BOOL bLock);
	void SetAutoRotCamera(BOOL bRot);

	bool IsInWater();

	void LookPlayer();

	void SetEyehight( float hight ){ m_eyeHight = hight;}

	void SetForceRotate( bool bforce ){ m_bForceRotate = bforce;}
	void SetMaxDistance( float dist ) { m_fMaxCameraDist = dist; }	// 22 ~ 28

protected:
	void HandleInput(float dt, int iX,int iY, int iZ, bool bRot = false);
	float GetMouseMoveSpeed(int delta);
	void UpdateDitherOffset(float dt);
	NiCamera m_NiCamera;
	float m_fCamPitch;
	float m_fCamYaw;
	float m_fCamDist;
	float m_fDesirdCamDist;
	float m_fZoomSpeed;
	float m_fAcc;
	BOOL m_bDithering;
	float m_fDitherTime;
	float m_fInitAmp;
	float m_fNoiseSeed;
	float m_fDitherPeriod;

	float m_fLastFacing;

	float m_fOffSetAngle;
	float m_fAngleValue;
	float m_fLastiX;
	bool  m_bLastLButtonDown;
	bool  m_bForceRotate;

	float m_fShakingTime;

	NiFrustumPlanes m_kPlanes;

	NiPoint3 m_kTranslate;
	int m_count;
	POINT m_pt;

	NiPoint3*	m_pkLocalPos;
	NiPoint3	m_kRButtonPos;
	NiPoint3	m_kLButtonPos;

	BOOL		m_bLButtonDown;
	BOOL		m_bRButtonDown;


	BOOL		m_bLockView; //锁定视角
	BOOL        m_bAutoRotCamera;
	float		m_fDeltaTime;
	float		m_fDeltaZoomIn;
	float		m_eyeHight;

	float m_fMaxCameraDist;
};

#endif