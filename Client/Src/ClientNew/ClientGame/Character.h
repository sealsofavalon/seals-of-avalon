#ifndef CHARACTER_H
#define CHARACTER_H

#include "AnimObject.h"
#include "Skill/SkillInfo.h"
#include "Avatar.h"
#include "State/SYStateBase.h"
#include "Map/Collision.h"

#include "Utils/ClientUtils.h"
#include "Utils/CyclicBuffer.h"

class AuraEffect;
NiSmartPointer(ALAudioSource);


#define EXTENT (NiPoint3(0.3f, 0.3f, 0.8f))

//#define USEDIR

enum ECHAR_ANIM_GROUP
{
	CAG_STAND = 0x01,
	CAG_IDLE,
	CAG_MOUT_IDLE,
	CAG_WAIT,
	CAG_EMOTION,
	CAG_RUN,
	CAG_MOUT_RUN,
	CAG_JUMP_UP,
	CAG_JUMP_ON,
	CAG_JUMP_DOWN,
	CAG_JUMP_RUN,
	CAG_DEAD,
	CAG_WALK,
	CAG_FLY,
	CAG_SWIM,
	CAG_GUARD,
	CAG_PULLOUT,
	CAG_PUSHIN,
	CAG_COMBATPREPARE,
	CAG_DRAW_WEAPON,
	CAG_STUNNED,
	CAG_ATTACK,
	CAG_ATTACK2,
	CAG_BLOCK,
	CAG_DODGE,
	CAG_BACKWARD,
	CAG_MOUNTRUN,
};

// 或许我们不会用到这么多....
enum ECHAR_MOVE_STATE
{
	CMS_WALK = 0,						// 走路
	CMS_RUN,						// 跑步
	CMS_MOUNTRUN,
	CMS_RUSH,
	CMS_STORM,
	CMS_SWIPE,						// 重击
	CMS_KNOCKBACK,
	CMS_KNOCKBACK_DOWN,
	CMS_SIDESTEP,					// 
	CMS_BACKSTEP,
	CMS_TUMBLING_FRONT,
	CMS_TUMBLING_LEFT,
	CMS_TUMBLING_RIGHT,
	CMS_SHOULDER_CHARGE,
	CMS_SLIDING,
	CMS_TELEPORT,
	CMS_TUMBLING_BACK, 
	CMS_FLYING,
	CMS_MAX,
};

// 跳跃子状态
enum ECHAR_JUMP_STATE
{
	EJS_NONE = -1,
	EJS_START = 0,
	EJS_FALLING,
	EJS_LANDING,
};

// 角色状态处理, 便于在任意状态间切换.

enum ECharacterState
{	
	CHARSTATE_NONE = -1,
	CHARSTATE_IDLE = 0,		//!< 空闲状态.
	CHARSTATE_SPAWN,		//!< 出生状态
	CHARSTATE_MOVE,			//!< 走动
	CHARSTATE_ATTACK,
	CHARSTATE_JUMP,
	CHARSTATE_DEATH,		//! 死亡状态, 但尸体可见.
	CHARSTATE_ANIMATION,
	CHARSTATE_DAMAGE,
	CHARSTATE_AIR,
	CHARSTATE_DOWN,
	CHARSTATE_SKILL,
	CHARSTATE_GETUP,
	CHARSTATE_EVENTJUMP,
	CHARSTATE_SIT,
	CHARSTATE_SPECIALMOVE,
	CHARSTATE_EMOTICON,
	CHARSTATE_PICK,			//! 拾取


	CHARSTATE_TRIGGER_STATE,
	CHARSTATE_ITEM_DROP_STATE,

	CHARSTATE_NPC_IDLE_STATE,
	CHARSTATE_NPC_SWING_STATE,
	CHARSTATE_NPC_RESPONSE_STATE,
	CHARSTATE_MAX
};

enum CHARACTERMOVEFLAG
{
	CMF_None			= 1 << 0,
	CMF_SelectWayPoint	= 1 << 1,
	CMF_AttackMove		= 1 << 2,
	CMF_RushMove       = 1 << 3,
};

enum EWaterLevel
{
	WL_NONE,
	WL_STANDING_WATER, //
	WL_UNDER_WATER
};

struct DamageResult
{
	SYObjID TargetID;
	SpellID	SpellId;
	float	Time;
	float   TotalTime;
	DWORD	WeaponSoundKind;
	UINT	Damage;
	UINT Heal:1;// 治疗
	UINT Miss:1;
	UINT DamageType;
	UINT AdditionalBuffer;
	NiPoint3 Offset;
};


struct stTeleport
{
	bool        bTelePort;
	NiPoint3    ptTelePos;
	float       fTeleAngle;
	void ReSet()
	{
		bTelePort = false;
		ptTelePos = NiPoint3::ZERO;
		fTeleAngle = 0.f;
	}
};
enum{
	BUBBLE_DEMAGE,
	BUBBLE_SPELLDEMAGE,
	BUBBLE_HEALHP,
	BUBBLE_HEALMP,
	BUBBLE_GAINHONOR,
	BUBBLE_COMBAT,
	BUBBLE_HEADTEXT,
	BUBBLE_MAX,
};
const ui32 gActionstring[RACE_MAX][GENDER_NONE] = 
{
	0,1,2,3,4,5
};

//class SpellInstance;
class CPathFinder;

//
class CCharacter : public CAnimObject, public SYStateActorBase
{
public:
	map<uint32, uint32> m_increasespellcdt;
	friend class CPlayerInputMgr;
	CCharacter(void);
	virtual ~CCharacter(void);

	virtual void OnCreate(ByteBuffer* data, ui8 update_flags);
	virtual void PostOnCreate();
	
	virtual void SetDirectionTo(const NiPoint3& Target);
	inline bool IsDead() const { return m_bIsDead;/*GetUInt32Value(UNIT_FIELD_HEALTH) == 0*/; }
	inline SYObjID	GetTargetObjectID() const {
		return m_TargetObjectID; 
	}
	CCharacter* GetTargetObject();
	void SetWorldTransform(const NiPoint3& kPos, const NiPoint3& kDir );

	virtual void Update(float dt);
	virtual void PendingUpdate(){}
	virtual void UpdatePhysics(float fDelta);


	void UpdateAnimObj(float dt);

	void UpdateChatText(float dt);
	void SetChatText(const std::string& strText);
	
	virtual NiAVObject* GetWeaponNode();

	// ================== State Proxy =====================
	virtual IStateProxy* GetStateProxy(State StateID);
	virtual IStateProxy* GetStateInstance(State eState);
	//virtual IStateProxy* GetCurStateProxy();
	virtual BOOL IsStateActorActive();
	inline float GetWaitAnimTime() const { return m_fWaitAnimTime; }
	inline void SetWaitAnimTime(float fTime) { m_fWaitAnimTime = fTime; }

	inline void SetWaitState(bool bWait) { m_bIsWait = bWait; }
	inline bool IsWait() const { return m_bIsWait; }

	// 骑宠
	CCharacter* m_pkMountCreature;
	CCharacter* m_pkMountFoot;
	CCharacter* m_pkMountHead;

	void UpdateMountMode();

	inline bool isMount(){ return IsMountPlayer() || IsMountByPlayer() || IsMountCreature(); }
	inline bool IsMountCreature() { return m_pkMountCreature != NULL; }
	inline bool IsMountPlayer() { return m_pkMountFoot != NULL; }
	inline bool IsMountByPlayer() { return m_pkMountHead != NULL; }
	inline CCharacter* GetMountCreature() { return m_pkMountCreature; }
	inline CCharacter* GetMountFoot() { return m_pkMountFoot; }
	inline CCharacter* GetMountHead() { return m_pkMountHead; }
	//	================== 动画相关 =====================

	inline BOOL IsAttackAnimationEnded() { return m_AttackAnimEnded ;}
	virtual AnimationID GetNormalAttackAnimation(UINT uSerial) = 0;
	// 获得单位动作序列名称
	// eAnimGroup : 动作类别
	// nIndex : 动作索引
	virtual const char* GetBaseAnimationName(ECHAR_ANIM_GROUP eAnimGroup, int nIndex = 0);
	virtual const char* GetClassAnimationName(ECHAR_ANIM_GROUP eAnimGroup, int nIndex = 0);
	//  ================== 移动相关 =====================
	// 移动到相关位置
	// SrcPos : 路径起点. 
	// Target : 路径
	virtual BOOL MoveTo(const NiPoint3& Target, UINT Flag, bool findpath = true);
	virtual void Teleport(NiPoint3& transPos, float fOrientation);
	void ToNewWorld();

	virtual void StopMove();
	inline float	GetMoveSpeed() const; 
	inline void		SetMoveState(ECHAR_MOVE_STATE eState) { m_MoveState = eState;}
	inline ECHAR_MOVE_STATE GetMoveState() const	{return m_MoveState; }

	void UpdateDirection(float fTimeDelta);

	void UpdateTransformbyChar(CCharacter* pkChar);
	NiNode* GetPetNode();

	virtual void BeginAttack(SYObjID dwTargetObjectID);
	virtual void AttackUpdate(const ui64& victim_guid);
	virtual void EndAttack();

	inline void SetAttackPrepare(bool value) {
		m_bAttackPrepare = value;
	}

	inline bool IsAttackPrepare() {
		return m_bAttackPrepare;
	}

	inline bool IsShouldStopAttack() {
		return m_iAttackUpdate <= 0 && m_bStopAttack;
	}

	bool IsSpellCast(SpellID spellID);
	void SpellCast(SpellID spellID, ui32 castTime, const SpellCastTargets& spellCastTarget);
	void SpellDelay(ui32 uiDelay);
	void TunnelDelay( ui32 uiDelay );
	void SpellGo(SpellID spellID, const vector<ui64>& vTargetHit, const SpellCastTargets& spellCastTargets);
	void SpellTunnel(SpellID spellID, float castTime );
	void EndTunnel();
	void SpellFailure(uint32 spellid, uint8 uiErrorCode);
	void SpellDamage();
	void AddTarget(SYObjID id);
	void AddTarget(const NiPoint3& kPos, float fRadius);

	inline const Target& GetTargetList() const {
		return m_targetList;
	}
	inline void ClearTargetList() {
		m_targetList.Clear();
	}

	// Physics method
	// {

	void BeginCharPhys();
	void EndCharPhys();

	float GetZInitVelocity() const;

	virtual bool CalcMoveVelocity(NiPoint3& vVelocity, float fOrient, bool bUpVel /*= false*/);
	virtual bool CalcMoveVelocity(NiPoint3& vVelocity, bool bUpVel = false, NiMatrix3* pkRotMat=0);

	void PerformPhysics(float fTimeDelta);
	void SetPhysics(EPhysics ePhys);
	int GetPhsyicState() const { 
		return m_ePhysics; 
	}

	void StartNewPhysics(float fTimeDelta, int iIterations);
	void StepUp(NiPoint3& start, const NiPoint3& kDesireDir, NiPoint3 delta, Result& result);
	void PhysWalking(float fTimeDelta, int iIterations);
	void PhysSwimming(float fTimeDelta, int iIterations);
	void PhysFlying(float fTimeDelta, int iIterations);
	EWaterLevel CheckInWater();

	NiPoint3 NewFallVelocity(const NiPoint3& kVelocity, const NiPoint3& kAcceleration, float fTimeTick) const;
	void PhysFalling(float fTimeDelta, int iIterations);

	NiPoint3 CheckMoveable(const NiPoint3& kFrom, const NiPoint3& kTo, bool bJump) const;
	void ResetPhsyics() {
	}
	// }

	inline uint32 GetClientTime() const {
		return float2int32(NiGetCurrentTimeInSec() * 1000.0f);
	}

	inline uint32 GetInterpolateTime() const {
		return GetClientTime() - 100;
	}

	inline uint32 GetSCTime(const uint32& time) {
		uint32 mst = time - (GetClientTime() - m_iDelay) + GetClientTime();
		return mst;
	}

	virtual bool UpdateMoveTransform(float fTime, float fTimeDelta, float& fT);
	void FinishingMovementHistory(uint32 uiT);

	float GetBuoyancy() const;

	virtual void StartForward(bool bAutoRun = false);
	virtual void StopForward();
	virtual void StartBackward();
	virtual void StopBackward();
	virtual void StopVertical();
	virtual void StartLeft();
	virtual void StopLeft();
	virtual void StartFlyUp();
	virtual void StopFlyUp();
	virtual void StartSwimUp();
	virtual void StopSwimUp();
	virtual void StartRight();
	virtual void StopRight();
	virtual void StopHorizontal();
	virtual void StartTurnLeft();
	virtual void StartTurnRight();
	virtual void StopTurn();
	virtual void OnJumpInputEvent();
	virtual void SetFacing(float rot);

	virtual void StartSwimming();
	virtual void EndSwimming();

	inline float GetFacing() const { 
		return GetRotate().z; 
	}
	inline void SetPhysInitVelocity(const NiPoint3& vInitVelocity) { 
		m_kInitVel = vInitVelocity; 
	}
	bool SetJumpPhysState(const NiPoint3& vInitVelocity);

	inline const int GetJumpState() const { 
		return m_iJumpState; 
	}
	inline void SetJumpState(int iState) { 
		m_iJumpState = iState;
	}

	bool StartJump(bool bPhys, bool bSyncPos = false, NiPoint3* pkVelocity = NULL);
	void EndJump();


	inline SpellInstance* GetAttackInst( int i ) {
		return m_pAttackInst[i]; 
	}

	inline BOOL IsMoving() const { 
		return BOOL((this->GetMovementFlags() & MOVEFLAG_MOTION_MASK || this->GetMovementFlags() & MOVEFLAG_PITCH_UP || this->GetMovementFlags() & MOVEFLAG_PITCH_DOWN ) || m_bPathMove  ); 
	}

	bool CanMove();

	inline BOOL IsJumping() const {
		return BOOL( (GetMovementFlags() & MOVEFLAG_JUMPING) || GetJumpState() != EJS_NONE);
	}

	virtual void PrepareMovementInfo();

	// 
	inline void SetMovementInfo(const MovementInfo& moveInfo) { m_stMoveInfo = moveInfo; }
	inline const MovementInfo& GetMovementInfo() const { return m_stMoveInfo; }

	inline void ResetMovementFlags() { 
		m_stMoveInfo.flags = 0; 
	}
	inline uint32 GetMovementFlags() const {  
		return m_stMoveInfo.flags; 
	}
	inline void SetMovementFlag( const uint32& flags ) {
		m_stMoveInfo.flags = flags;
	}

	inline void ResetFallingFlag() { m_stMoveInfo.flags &= ~MOVEFLAG_FALLING_MASK; }
	inline void ResetJumpingFlag() { 
		m_stMoveInfo.flags &= ~MOVEFLAG_JUMPING; 
	}

	inline const Move& GetMove() const { 
		return m_kMove; 
	}

	void SetAccSpeed(float fAccSpeed);

	// 攻击	===========================================================
	// 能否被攻击
	bool IsHostile(CCharacter* pChar);
	bool IsFriendly(CCharacter* pChar);
	bool IsTarget(CCharacter* pChar);

	void EnterArena(ui32 mapID);
	void LeaveArena();
	bool IsInArena();

	virtual BOOL CanBeenAttack();
	virtual void StopAttack();

	inline const NiPoint3&		GetAreaAttackCenter() const	{ 
		return m_AttackCenter; 
	}
	inline void SetAreaAtttackCenter(const NiPoint3& Center) 
	{ 
		m_AttackCenter = Center;
	}
	
	inline void Damage(const DamageResult& NewDamage) { m_DamageResults.push_back(NewDamage);}
	

	virtual bool Draw(CCullingProcess* pkCuller);
	virtual void DrawName(const NiCamera* Camera);
#ifdef _DEBUG
	void DrawStateDesc(const NiPoint3& kPos);
#endif //

	virtual void DrawChatText(const NiCamera* Camera);
	virtual void DrawWaterEffect();

	virtual void PathMoveStart(const NiPoint3& pos, UINT uiFlag);
	virtual void PathMoveEnd();
	virtual void PathMoveUpdate(float fTimeDelta);

	inline const NiPoint3& GetPathMoveDir() {
		return m_kPathMoveDir;
	}

	inline bool IsPathMove() const { return m_bPathMove; }

	//////////////////////////////////////////////////////////////////////////
	// 
	//	网络消息处理
	//
	//////////////////////////////////////////////////////////////////////////
	virtual bool IsDead() { return (m_bIsDead || bool(GetHP() <= 0)); }
	virtual void OnDeath(bool bDeadNow = true);
	virtual void OnAlive();
    virtual void OnActorLoaded(NiActorManager* pkActorManager);

	//
	//玩家和怪物的属性
	//
	inline ui32	GetGender(){ return GetByte(UNIT_FIELD_BYTES_0,2); }
	inline void	SetGender(ui8 gender){ SetByte(UNIT_FIELD_BYTES_0, 2, gender); }
	inline ui32	GetRace()  { return GetByte(UNIT_FIELD_BYTES_0,0); }
	inline void	SetRace(uint8 race) { SetByte(UNIT_FIELD_BYTES_0,0, race); }
	inline ui32	GetClass() { return GetByte(UNIT_FIELD_BYTES_0,1); }
	inline ui32	GetLevel() { return GetUInt32Value(UNIT_FIELD_LEVEL); }
	inline ui32	GetMoney() { return GetUInt32Value(PLAYER_FIELD_COINAGE); }
	inline ui32	GetExp()   { return GetUInt32Value(PLAYER_XP); }
	inline ui32	GetMaxExp(){ return GetUInt32Value(PLAYER_NEXT_LEVEL_XP); }
	inline ui32	GetMaxHP() { return GetUInt32Value(UNIT_FIELD_MAXHEALTH); }
	inline ui32	GetHP()    { return GetUInt32Value(UNIT_FIELD_HEALTH); }
	inline ui32	GetMP()   { return GetUInt32Value(UNIT_FIELD_POWER1); }
	inline ui32	GetMaxMP()   { return GetUInt32Value(UNIT_FIELD_MAXPOWER1); }
	inline float	GetAttackMax() { return GetFloatValue(UNIT_FIELD_MAXDAMAGE); }
	inline float	GetAttackMin() { return GetFloatValue(UNIT_FIELD_MINDAMAGE); }
	inline AnimationID GetCurAnimationID(){ return m_uiCurrAnimID; }
	//BOOL    GetInCombat() const { return (BOOL)GetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);}
	inline float GetCombatReach() const
	{
		return GetFloatValue(UNIT_FIELD_COMBATREACH);
	}

	inline float GetBoundingRadius() const 
	{
		return GetFloatValue(UNIT_FIELD_BOUNDINGRADIUS);
	}
	static char* GetHeadImageFile(CCharacter* pChar = NULL);

	const char* GetTitleStr(){return m_titleName.c_str();};

	inline void SetInWorld(bool value) {
		m_bIsInWorld = value;
	}
	inline bool IsInWorld() const {
		return m_bIsInWorld;
	}

	void SetCameraVec( NiPoint3& vCam ){
			m_kCamera.x = vCam.x;
			m_kCamera.y = vCam.y;
			m_kCamera.z = vCam.z;
	}
	float GetBoundingScale() const
	{
		return GetFloatValue(OBJECT_FIELD_SCALE_X);
	}

	inline float GetAttckRangeToTarget(CCharacter* pkTargetObject)
	{
		NIASSERT(pkTargetObject);

		float selfreach = GetCombatReach();
		float targetradius = pkTargetObject->GetBoundingRadius();
		float selfradius = GetBoundingRadius();
		float targetscale = pkTargetObject->GetBoundingScale();
		float selfscale = GetBoundingScale();

		float range = ((((targetradius/**targetradius*/)*targetscale) + selfreach) + ((selfradius*selfscale) /*+ rang*/));

		return range;
	}

	inline unsigned int GetStrength() const { return GetUInt32Value(UNIT_FIELD_STAT0); }							//力量
	inline void SetStrength(unsigned int usStrength) { SetUInt32Value(UNIT_FIELD_STAT0, usStrength); }

	inline unsigned int GetAgility() const { return GetUInt32Value(UNIT_FIELD_STAT1); }							//敏捷
	inline void SetAgility(unsigned int usConstitution) { SetUInt32Value(UNIT_FIELD_STAT1, usConstitution); }

	inline unsigned int GetStamina() const { return GetUInt32Value(UNIT_FIELD_STAT2); }							//耐力
	inline void SetStamina(unsigned int usConstitution) { SetUInt32Value(UNIT_FIELD_STAT2, usConstitution); }

	inline unsigned int GetIntelligence() const { return GetUInt32Value(UNIT_FIELD_STAT3); }						//智力
	inline void SetIntelligence(unsigned int usIntelligence) { SetUInt32Value(UNIT_FIELD_STAT3, usIntelligence); }

	inline unsigned int GetSpirite() const { return GetUInt32Value(UNIT_FIELD_STAT4); }							//精神
	inline void SetSpirite(unsigned int usIntelligence) { SetUInt32Value(UNIT_FIELD_STAT4, usIntelligence); }

	inline unsigned int GetAmor() const { return GetUInt32Value(UNIT_FIELD_RESISTANCES); }
	inline void SetAmor(unsigned short usAmor) { SetUInt32Value(UNIT_FIELD_RESISTANCES, usAmor); }

	inline unsigned int GetVitality() const {return GetUInt32Value(UNIT_FIELD_STAT5); }							//体力
	inline void SetVitality(unsigned int usVitality){SetUInt32Value(UNIT_FIELD_STAT5,usVitality); }

	inline BOOL isInCombatState() {
		return HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);
	}

	//void SetAvatarInfo(unsigned int* uipInfo);

	void PushAttackResult( uint32 uiDamage, uint32 hitStats, ui64 victimGuid );

	// 在人物身上冒出影响数值
	virtual void BubbleUpEffectNumber(int Number, int affectType = 0,ui32 SubType = 0, const WCHAR* extraOutput = NULL , ui32 SubType2 = 1);

	// 设置目标ID,(跟随,交易,对话,攻击等目标)

	virtual BOOL SetMoveAnimation(UINT Flags, BOOL bForceStart = TRUE);
	virtual BOOL SetJumpAnimation(int iJumpState, bool bAnimLoop=false);

	BOOL SetAttackPrepare();
	virtual BOOL SetCombatPrepareAnimation(int iType);
	virtual BOOL SetIdleAnimation(int iType);
	virtual BOOL IsSYMount(){ return m_bisSYMount;}
	void OnDisplayedWaitSound(ui32 entry);

	INT MakeAttackSerial() const;
	virtual BOOL SetAttackAnimation(bool bPrepare = false);

	void ProcessJumpState(bool bSyncPos = false, NiPoint3* pkInitVelocity = NULL);
	void SetTargetObjectID(SYObjID dwTargetObjectID);

	int RequestDamageID();

	virtual bool GetBaseMoveSpeed(float& value) const { return false; }

	inline NiControllerSequence* GetMoveSeq() {
		return m_pkMoveSeq;
	}

	inline int GetSpecialState() {
		return m_iSpecialState;
	}

	BOOL IsStuned();
	BOOL IsIceBlock();
	BOOL IsSleep();
	BOOL IsRoot();
	BOOL IsFrozen();
	BOOL IsSilenced();
	BOOL IsConfused();
	BOOL IsFleeing();


	bool IsFlying() const;

	bool IsNeedUpdateSwimAnim(){ return m_bneedUpdateSwimAni;}

	void SetNeedUpdateSwimAnim( bool need ){ m_bneedUpdateSwimAni = need;}

protected:
	virtual void EndOfSequence(NiActorManager* pkManager,
		NiActorManager::SequenceID eSequenceID, float fCurrentTime, float fEventTime);

	void UpdateSpecialState(float dt);
	//NiBoneLODController* FindBoneLODController(NiAVObject* pkObject);
	void UpdateEvents();
	void UpdateCharacterLOD();
	void UpdateDamages(float fTime);
	
	void ProcessDamage(const DamageResult& Result);

public:
	void ApplyAura(ui32 spellid, ui8 slot);
	virtual void OnValueChanged(class UpdateMask* mask);
	struct AuraInfo
	{
		ui32 SpellId;
		bool UpdateMsk;
		float AuraTime;
		float MaxAuraTime;
		int level;
		AuraInfo():SpellId(0),UpdateMsk(false),AuraTime(0.0f),MaxAuraTime(0.0f),level(1){}
	}m_AuraInfo[MAX_AURAS];
	AuraInfo * GetAura(int slot);
	void SetAuraDuringTime(int slot, float time, float maxtime);
	void SetAuraLevel(int slot, int level);
	bool GetMountSpell(ui32& spellId);
	bool GetShapeSpell(ui32& spellId);
	bool IsAuraInChar(ui32 spellId);

	void UpdataAura(float dt);
	int32 m_iAttackUpdate;

	void EnablePhys(bool bEnable);
	void UpdatePullMove( float dt );

	float GetDesiredAngle() const;
	float GetFacingAngle() const;
	void SetDesiredAngle(float fAngle, bool bForce = false);
	void SetServerAngle( float serverAngle );

	float GetNameOffset() const { 
		return m_kNamePosOffset; }

	bool mbIsTargetTarget;
	void SetTargetTarget(bool btemp);
	void SetFly( bool bflying ) {
		m_bFlying = bflying;
	}

	void SetPhyFly( bool phyFly )
	{
		m_bPhyFly = phyFly;
	}

	stTeleport& GetTelePortPara(){ return m_stTelePortPara;}
	
	bool isNeedUpdateAnim(){ return m_bNeedDoAnim;}
	void SetisNeedAnim( bool isNeed ){ m_bNeedDoAnim = isNeed;}

	void SetStrikeBackSpeed( float bsSpeed ){ m_fbackStrikeSpeed = bsSpeed; }

	void SetIsStrikeBack( bool b ){ 
		m_bisStrikeBack = b;
	}

	void AddStrickBackCount(){ m_iStrickBackCount++; }

public:
	void SetSelState(bool state){ m_SelState = state;}
	bool GetSelState(){return m_SelState;}
	void StartPull( const NiPoint3& ptTarget ){ 
		m_bisForcePull = true;
		m_ptPullTargrt = ptTarget;
	}

	bool m_SelState;

	ui32 m_UnitFlag;

	std::string m_Name;
	std::string& GetCurMountSitAction() { return m_strMoutSitAction; }

	ALAudioSource* GetSoundInst() {
		return m_spVoice[0];
	}

	ALAudioSource* GetEnvSoundInst() {
		return m_spVoice[1];
	}

	bool laststormstate;

	float GetModelScale(){ return m_fModleScale;}

protected:
	// 攻击/技能相关
	SpellInstance* m_pAttackInst[2];	// attack, spell
	//std::vector<SpellInstance*> m_vPlayingSpell;
	Target m_targetList;

	AuraEffect* m_pkAura[MAX_AURAS];

	NiPoint3 m_AttackCenter;
	float m_fWaitAnimTime;
	bool m_bIsWait;
	std::vector<DamageResult> m_DamageResults;
	BOOL m_AttackAnimEnded;

	UINT m_uiPathMoveFlag;
	bool m_bForceFly;

	// 寻路/移动相关
	CPathFinder*		m_PathFinder;			// 寻路
	SYObjID				m_TargetObjectID;
	ECHAR_MOVE_STATE	m_MoveState;			// 移动状态.
	NiPoint3			m_MoveDestPos;

	float				m_fChatTime;
	std::string			m_strChatText;//场景聊天
	std::string			m_titleName;

	std::string m_strMoutSitAction;
	bool m_bIsInWorld;
	float m_fbackStrikeSpeed;
	bool  m_bisStrikeBack;
	int   m_iStrickBackCount;
	bool  m_bisForcePull;
	NiPoint3 m_ptPullTargrt;
	float m_fUsedTime;
	float m_fLmtTime;

	unsigned int m_uiAvatarInfo[MAX_INVENTORY_TYPES+2];	//Avatar;

	bool m_bPathMove;
	NiPoint3 m_kPathMoveDir;

	Move m_kMove;
	float m_fAccSpeed;

	// physics data
	// {
	NiPoint3 m_kInternelVel;
	NiPoint3 m_kInitVel;
	EPhysics m_ePhysics;
	NiPoint3 m_kCamera;
	float    m_scaleTime;
	// }

	bool m_bEnablePhys;
	bool m_bNeedDoAnim;

	int m_iJumpState;
	float m_fJumpEndTime;

	bool m_bStopAttack;
	// bool m_bInArena;
	ui32 m_uiArenaMapID;

	bool		m_bIsDead;

private:
	static IStateProxy* sm_StateProxys[STATE_MAX];

private:
	bool m_bInWater;
	float m_fWaterHeight;
	float m_fCurHeight;
	float m_fWaterLine;
	EWaterLevel	m_eWaterLevel;
	unsigned int m_uiLastWaterEffect;
	bool  m_bFlying;
	bool  m_bPhyFly;
	bool  m_bneedUpdateSwimAni;
	float m_speedScale;
	float m_fGravityScale;
	NiPoint3 m_ptGravity;


	int m_iSpecialState;
protected:
	MovementInfo m_stMoveInfo;

	bool m_bRebuild;
	int32 m_iDelay;

	float m_kNamePosOffset;
	float m_fModleScale;
	std::string m_strRunSound;
	stTeleport  m_stTelePortPara;
public:
	void SetModleScale(float modle);
protected:
	float m_fDesirdAngle;
	float m_fFaceingAngle;
	NiPoint3 m_LastOrient;
	NiPoint3 m_LastPoint;
	NiPoint3 m_NextPoint;
	float m_fTurnAccSpeed;
	bool m_bisStuned;
	BOOL m_bisSYMount;

	NiControllerSequence* m_pkMoveSeq;
 	bool m_bAttackPrepare;

	ALAudioSourcePtr m_spVoice[2];

	class CComboAttackEffect* m_spAttackEffect;
	struct sNameRecode* m_spNameRef;
};

#endif
