#include "StdAfx.h"
#include "ClientApp.h"
#include "UI\UISystem.h"
#include "SceneManager.h"
#include "Player.h"
#include "ResourceManager.h"
#include "ShaderHelper.h"
#include "ObjectManager.h"
#include "ClientState.h"
#include "Network\NetworkManager.h"
#include "Network\PacketBuilder.h"
#include "Network\PacketParser.h"
#include "PlayerInputMgr.h"
#include "Effect/EffectManager.h"
#include "AudioInterface.h"
#include "Skill/SkillManager.h"
#include "ItemManager.h"
#include "AnimObject.h"
#include "NiDX9TextureData.h"
#include "ClientProfiler.h"
#include "SyGfxManager.h"
#include "audio/ALAudioSystem.h"
#include "DpsCount.h"

#include "Utils/QuestDB.h"
#include "Utils/NpcTextDB.h"
#include "Utils/SpellDB.h"
#include "Utils/QuestFinisherDB.h"
#include "Utils/QuestStarterDB.h"
#include "Utils/DefaultDisplayInfoDB.h"
#include "Utils/CreatureSpawnsDB.h"
#include "Utils/MapInfoDB.h"
#include "Utils/AreaTriggerDB.h"
#include "Utils/GameObjectDB.h"
#include "Utils/LockDB.h"
#include "Utils/AreaDB.h"
#include "Utils/ChatFileterDB.h"
#include "Utils/LevelTipsDB.h"
#include "Utils/NewBirdDB.h"
#include "Utils/InstanceMsg.h"
#include "Utils/MapToMapPathInfo.h"
#include "Utils/NewBirdEventDB.h"
#include "Utils/Soundid.h"
#include "Utils/CastleNpcListDB.h"
#include "Utils/ItemEnchant.h"
#include "Utils/NPCListFile.h"
#include "Utils/CPlayerTitleDB.h"
#include "Utils/CreatureLootDB.h"
#include "Langauge/LangaugeNotice.h"
#include "../../../../SDBase/Public/DBCStores.h"

#include "PackFile.h"
#include "Map/TaskManager.h"
#include "Map/WaterShader.h"

#include "Console.h"
#include "resource.h"

#include "RenderTargetToTexture.h"
#include "Screenshot.h"
#include "ui/SystemSetup.h"


SY_State __SY__;

IDirect3DTexture9* SYD3DTexture(NiTexture* NiTex)
{
	NiDX9TextureData* Texture = (NiDX9TextureData*)NiTex->GetRendererData();
	if (Texture)
	{
		return (IDirect3DTexture9*)Texture->GetD3DTexture();
	}
	return NULL;
}
NiApplication* NiApplication::Create()
{
	return NiNew CClientApp;
}

CClientApp::CClientApp(void):
NiApplication("Seals Of Avalon ")
{
	CPackFile::Init( true );
	CPackFile::Init( false );
	NiFile::SetFileCreateFunc(CPackFile::CreateFile);
	NiFile::SetFileAccessFunc(CPackFile::AccessFile);

	ClientState = NiNew CClientState;

	m_pkAppWindow->SetWidth(ClientState->GetWidth());
	m_pkAppWindow->SetHeight(ClientState->GetHeight());
	if (ClientState->GetFullScreen())
	{
		SetFullscreen(true);
	}else
	{
		SetFullscreen(false);
	}

	m_uiDesktopWidth = ::GetSystemMetrics(SM_CXSCREEN);
	m_uiDesktopHeight = ::GetSystemMetrics(SM_CYSCREEN);

	m_UISystem = NULL;
	m_CursorID = SYC_ARROW;
}

CClientApp::~CClientApp(void)
{
	NiDelete ClientState;
}

void CClientApp::GetVideoSize(unsigned int& uiWidth, unsigned int& uiHeight)
{
	uiWidth = ClientState->GetWidth();
	uiHeight = ClientState->GetHeight();

	if(ClientState->GetFullScreen())
	{
		uiWidth = GetSystemMetrics(SM_CXSCREEN);
		uiHeight = GetSystemMetrics(SM_CYSCREEN);
		//找一个最接近，且系统支持的分辨率
		/*const NiDX9SystemDesc* pSystemDesc = NiDX9Renderer::GetSystemDesc();
		const NiDX9AdapterDesc::ModeDesc* pBestModeDesc = NULL;
		int iBest = 1000000;
		if(pSystemDesc)
		{
			const NiDX9AdapterDesc* pAdapter = pSystemDesc->GetAdapter(D3DADAPTER_DEFAULT);
			if(pAdapter)
			{
				for(unsigned int i = 0; i < pAdapter->GetModeCount(); ++i)
				{
					const NiDX9AdapterDesc::ModeDesc* pModeDesc = pAdapter->GetMode(i);
					int iDiff = abs(int(pModeDesc->m_uiWidth - uiWidth)) + abs(int(pModeDesc->m_uiHeight - uiHeight));
					if(iBest > iDiff)
					{
						iBest = iDiff;
						pBestModeDesc = pModeDesc;
					}
				}
			}
		}

		if(pBestModeDesc == NULL)
		{
			uiWidth = 1024;
			uiHeight = 768;
		}
		else
		{
			uiWidth = pBestModeDesc->m_uiWidth;
			uiHeight = pBestModeDesc->m_uiHeight;
		}*/
	}
	else
	{
		if(uiWidth > m_uiDesktopWidth - 2)
			uiWidth = m_uiDesktopWidth - 2;

		if(uiHeight > m_uiDesktopHeight - 12)
			uiHeight = m_uiDesktopHeight - 12;
	}
	ULog("RenderSize width = %d height = %d ",uiWidth,uiHeight);
}

// 重写
bool CClientApp::CreateRenderer()
{
	unsigned int uiWidth;
	unsigned int uiHeight;
	GetVideoSize(uiWidth, uiHeight);

	unsigned int uiUseFlags = NiDX9Renderer::USE_MULTITHREADED/*|NiDX9Renderer::USE_FPU_PRESERVE*/;
	//uiUseFlags |= NiDX9Renderer::USE_FULLSCREEN;
	if( ClientState->GetFullScreen() )
	{
		unsigned int uiStyle;
		RECT kRect;
		m_bFullscreen = true;

		kRect.left = 0;
		kRect.top = 0;
		kRect.right = uiWidth;
		kRect.bottom = uiHeight;;
		uiStyle = WS_POPUP;
		SetWindowPos(GetWindowReference(), HWND_NOTOPMOST, kRect.left, kRect.top, kRect.right - kRect.left, kRect.bottom - kRect.top, SWP_NOZORDER|SWP_SHOWWINDOW);
	}

	if (m_UISystem)
	{
		m_UISystem->PostResize(uiWidth, uiHeight);
	}

#ifdef NIDEBUG
	// 处理nv pref 适配器.
	UINT AdapterIndex = D3DADAPTER_DEFAULT;
	NiDX9Renderer::DeviceDesc DeviceType = NiDX9Renderer::DEVDESC_HAL_HWVERTEX;
	IDirect3D9* D3d9 = Direct3DCreate9(D3D_SDK_VERSION);
	NIASSERT(D3d9);
	for (UINT Adapter=0; Adapter < D3d9->GetAdapterCount(); Adapter++)
	{
		D3DADAPTER_IDENTIFIER9 Identifier;
		HRESULT Res;
		Res = D3d9->GetAdapterIdentifier(Adapter,0,&Identifier);
		if (strstr(Identifier.Description,"PerfHUD") != 0)	// NV PREFHUD 5.0 
		{
			AdapterIndex = Adapter;
			DeviceType = NiDX9Renderer::DEVDESC_REF;
			break;
		}
	}

	D3d9->Release();
	m_spRenderer = NiDX9Renderer::Create(uiWidth, uiHeight,
		uiUseFlags, GetWindowReference(), 
		GetRenderWindowReference(),
		AdapterIndex, 
		DeviceType,
		NiDX9Renderer::FBFMT_X8R8G8B8,
		NiDX9Renderer::DSFMT_D24S8,
		NiDX9Renderer::PRESENT_INTERVAL_IMMEDIATE,
		NiDX9Renderer::SWAPEFFECT_DISCARD,
		NiDX9Renderer::FBMODE_LOCKABLE, 2,
		NiDX9Renderer::REFRESHRATE_DEFAULT
		);
#else
	/*
	UINT AdapterIndex = D3DADAPTER_DEFAULT;
	NiDX9Renderer::DeviceDesc DeviceType = NiDX9Renderer::DEVDESC_HAL_HWVERTEX;
	IDirect3D9* D3d9 = Direct3DCreate9(D3D_SDK_VERSION);
	NIASSERT(D3d9);
	for (UINT Adapter=0; Adapter < D3d9->GetAdapterCount(); Adapter++)
	{
		D3DADAPTER_IDENTIFIER9 Identifier;
		HRESULT Res;
		Res = D3d9->GetAdapterIdentifier(Adapter,0,&Identifier);
		if (strstr(Identifier.Description,"PerfHUD") != 0)	// NV PREFHUD 5.0 
		{
			AdapterIndex = Adapter;
			DeviceType = NiDX9Renderer::DEVDESC_REF;
			break;
		}
	}

	m_spRenderer = NiDX9Renderer::Create(uiWidth, uiHeight,
		uiUseFlags, GetWindowReference(), 
		GetWindowReference(),
		AdapterIndex, 
		DeviceType,
		//0,
		//NiDX9Renderer::DEVDESC_PURE,
		NiDX9Renderer::FBFMT_X8R8G8B8,
		NiDX9Renderer::DSFMT_D24S8,
		(NiDX9Renderer::PresentationInterval)ClientState->GetPresentationInterval(),
		NiDX9Renderer::SWAPEFFECT_DISCARD,
		NiDX9Renderer::FBMODE_DEFAULT, 1,
		NiDX9Renderer::REFRESHRATE_DEFAULT);
	*/

	// Check for NVPerfHUD
	NiDX9Renderer::DeviceDesc eDevType = NiDX9Renderer::DEVDESC_HAL_HWVERTEX;
	unsigned int uiAdapter = D3DADAPTER_DEFAULT;
	if (m_bNVPerfHUD)
	{
		// Look for 'NVIDIA NVPerfHUD' adapter
		// If it is present, override default settings
		const NiDX9SystemDesc* pkSystemDesc = 
			NiDX9Renderer::GetSystemDesc();
		NIASSERT(pkSystemDesc);
		unsigned int uiAdapterCount = pkSystemDesc->GetAdapterCount();

		for (unsigned int i = 0; i < uiAdapterCount; i++)
		{
			const NiDX9AdapterDesc* pkAdapter = 
				pkSystemDesc->GetAdapter(i);
			const char* pcAdapterName = 
				pkAdapter->GetStringDescription();

			if (strstr(pcAdapterName,"NVPerfHUD"))
			{
				uiAdapter = i;
				eDevType = NiDX9Renderer::DEVDESC_REF_HWVERTEX;
				break;
			}
		}
	}

	// Create a DX9 renderer 
	m_spRenderer = NiDX9Renderer::Create(uiWidth, uiHeight,
		uiUseFlags, 
		GetWindowReference(),
		GetWindowReference(),
		uiAdapter, eDevType,
		NiDX9Renderer::FBFMT_X8R8G8B8,
		NiDX9Renderer::DSFMT_D24S8,
		(NiDX9Renderer::PresentationInterval)ClientState->GetPresentationInterval(),
		NiDX9Renderer::SWAPEFFECT_DISCARD,
		NiDX9Renderer::FBMODE_MULTISAMPLES_4, 2,
		NiDX9Renderer::REFRESHRATE_DEFAULT);
#endif 
	//bool bMutiSampleFqailed = false;
	if (m_spRenderer == NULL)
	{
		//uiUseFlags = NiDX9Renderer::USE_MULTITHREADED;	
		//if( ClientState->GetFullScreen() )
		//{
		//	m_bFullscreen = true;
		//	uiUseFlags |= NiDX9Renderer::USE_FULLSCREEN;
		//}
		//m_spRenderer = NiDX9Renderer::Create(uiWidth, uiHeight,
		//	uiUseFlags, GetWindowReference(), 
		//	GetWindowReference(),
		//	AdapterIndex, 
		//	DeviceType,
		//	//0,
		//	//NiDX9Renderer::DEVDESC_PURE,
		//	NiDX9Renderer::FBFMT_X8R8G8B8,
		//	NiDX9Renderer::DSFMT_D24S8,
		//	(NiDX9Renderer::PresentationInterval)ClientState->GetPresentationInterval(),
		//	NiDX9Renderer::SWAPEFFECT_DISCARD,
		//	NiDX9Renderer::FBMODE_LOCKABLE, 1,
		//	NiDX9Renderer::REFRESHRATE_DEFAULT);
		//bMutiSampleFqailed = true;
		if ( m_spRenderer == NULL )
		{
			NiMessageBox("Unable to create a renderer!", "Renderer Failure!");
			QuitApplication();
			return false;
		}
		
	}

	////全屏模式下， 可以让输入法窗口显示出来
	NiD3DRenderer* pkRenderer = (NiD3DRenderer*)NiRenderer::GetRenderer();
	if(pkRenderer)
	{
		LPDIRECT3DDEVICE9 pD3DDevice = pkRenderer->GetD3DDevice();
		if(pD3DDevice)
			pD3DDevice->SetDialogBoxMode(true);
	}

	//pixel shader 小于3.0的显卡上面禁用水面效果
	//虽然水面效果可以在pixel shader 2.0的机器上面运行， 但是会比较卡
	DWORD ver = pkRenderer->GetDeviceCaps()->PixelShaderVersion;
	if(ver < D3DPS_VERSION(3, 0))
	{
		ClientState->SetWaterReflect(false);
	}

	CRT2Tex::SM_Init(m_spRenderer, 128, 128);
	
	return true;
}

void CClientApp::RecreateRender()
{
	DASSERT(m_spRenderer);

	unsigned int uiWidth;
	unsigned int uiHeight;
	GetVideoSize(uiWidth, uiHeight);

	RECT kRect;
	NiAppWindow* pkAppWindow = ms_pkApplication->GetAppWindow();
	unsigned int uiUseFlags = NiDX9Renderer::USE_MULTITHREADED;
	unsigned int uiStyle;
	unsigned int uiRenderStyle = WS_CHILD;
	if( ClientState->GetFullScreen() )
	{
		m_bFullscreen = true;
		//uiUseFlags |= NiDX9Renderer::USE_FULLSCREEN;

		kRect.left = 0;
		kRect.top = 0;
		kRect.right = uiWidth;
		kRect.bottom = uiHeight;
		uiStyle = WS_POPUP;
	}
	else
	{
		m_bFullscreen = false;
		uiRenderStyle |= WS_VISIBLE;
		uiStyle = pkAppWindow->GetWindowStyle();
		GetWindowRect(GetWindowReference(), &kRect);
			//uiRenderStyle |= WS_CAPTION;
		if ( kRect.left == 0 && kRect.top == 0 )
		{
			kRect.left = ( m_uiDesktopWidth - uiWidth )/2;
			kRect.top = ( m_uiDesktopHeight - uiHeight )/2;
		}
		kRect.left += GetSystemMetrics( SM_CXBORDER ) + 2;
		kRect.top += GetSystemMetrics( SM_CYCAPTION ) + 3;

		kRect.right = kRect.left + uiWidth;
		kRect.bottom = kRect.top + uiHeight;
		AdjustWindowRect(&kRect, uiStyle, 0);
		if(kRect.left < 0)
		{
			kRect.right += -kRect.left;
			kRect.left = 0;
		}
		if(kRect.top < 0)
		{
			kRect.bottom += -kRect.top;
			kRect.top = 0;
		}
		
	}

	SetWindowLong(GetWindowReference(), GWL_STYLE, uiStyle);
	SetWindowLong(GetRenderWindowReference(), GWL_STYLE, uiRenderStyle);
	SetWindowPos(GetWindowReference(), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	SetWindowPos(GetRenderWindowReference(), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);


	//unsigned int uiBFMode = NiDX9Renderer::FBMODE_LOCKABLE;
	/*if (ClientState->GetMultiSample())
	{
		uiBFMode = NiDX9Renderer::FBMODE_MULTISAMPLES_4;
	}*/

	((NiDX9Renderer*)NiRenderer::GetRenderer())->Recreate(uiWidth, uiHeight, uiUseFlags, 
		GetWindowReference(), 
		NiDX9Renderer::FBFMT_X8R8G8B8,
		NiDX9Renderer::DSFMT_D24S8,
		(NiDX9Renderer::PresentationInterval)ClientState->GetPresentationInterval(),
		NiDX9Renderer::SWAPEFFECT_DISCARD,
		NiDX9Renderer::FBMODE_MULTISAMPLES_4, 2,
		NiDX9Renderer::REFRESHRATE_DEFAULT);


	if(!m_bFullscreen)
		SetWindowPos(GetWindowReference(), HWND_NOTOPMOST, kRect.left, kRect.top, kRect.right - kRect.left, kRect.bottom - kRect.top, SWP_NOZORDER|SWP_SHOWWINDOW);
	else
		SetWindowPos(GetWindowReference(), HWND_NOTOPMOST, kRect.left, kRect.top, kRect.right - kRect.left, kRect.bottom - kRect.top, SWP_NOZORDER|SWP_SHOWWINDOW);

	RECT rect;
	GetClientRect(GetWindowReference(), &rect);

	//if( uiWidth != rect.right - rect.left
	//	|| uiHeight != rect.bottom - rect.top )
	//{
	//	uiWidth = rect.right - rect.left;
	//	uiHeight = rect.bottom - rect.top;

	//	//重来一次
	//	((NiDX9Renderer*)NiRenderer::GetRenderer())->Recreate(uiWidth, uiHeight, uiUseFlags, 
	//		GetWindowReference(), 
	//		NiDX9Renderer::FBFMT_X8R8G8B8,
	//		NiDX9Renderer::DSFMT_D24S8,
	//		(NiDX9Renderer::PresentationInterval)ClientState->GetPresentationInterval(),
	//		NiDX9Renderer::SWAPEFFECT_DISCARD,
	//		NiDX9Renderer::FBMODE_MULTISAMPLES_4, 2,
	//		NiDX9Renderer::REFRESHRATE_DEFAULT);
	//}

	m_pkAppWindow->SetWidth(uiWidth);
	m_pkAppWindow->SetHeight(uiHeight);

	CWaterShader::ReCreate();

	//分辨率改变了必须重新创建
	if(ClientState->IsBloomEnabled() && EffectMgr )
	{
		EffectMgr->CleanupPostEffect();
		EffectMgr->CreatePostEffect();
	}

	if ( ObjectMgr )
	{
		ObjectMgr->GetLocalPlayer()->GetCamera().Reset();
	}
	

	if (m_UISystem)
	{
		m_UISystem->PostResize(uiWidth, uiHeight);
	}

	((NiDX9Renderer*)NiRenderer::GetRenderer())->GetRenderState()->SetRenderState( D3DRS_MULTISAMPLEANTIALIAS, ClientState->GetMultiSample() );

	ShowWindow(GetWindowReference(), SW_SHOW);
	UpdateWindow(GetWindowReference());

	//////全屏模式下， 可以让输入法窗口显示出来
	NiD3DRenderer* pkRenderer = (NiD3DRenderer*)NiRenderer::GetRenderer();
	if( pkRenderer && ClientState->GetFullScreen() )
	{
		LPDIRECT3DDEVICE9 pD3DDevice = pkRenderer->GetD3DDevice();
		if(pD3DDevice)
			pD3DDevice->SetDialogBoxMode(true);
	}
}

bool CClientApp::CreateCamera()
{
	ObjectMgr->GetLocalPlayer()->InitCamera();
	return true;
}

float ui_update_delta_sum = 0.f;

void CClientApp::UpdateFrame()
{
	NiParallelUpdateTaskManager::BeginUpdate();

	CLIENT_METRIC_RECORDMETRICS();
	NiApplication::UpdateFrame();

	CLIENT_METRIC_TIMER(NiMetricsClockTimer, kTimer, NETWORK_UPDATETIME);
	CLIENT_METRIC_STARTTIMER(kTimer);
	CLIENT_METRIC_ENDTIMER(kTimer);

	SYState()->CurTime = (DWORD)(m_fAccumTime*1000.f);

	/*
	if (SYState()->LastPing && NetworkMgr->IsConnected())
	{
		int tempTime = float2int32(NiGetCurrentTimeInSec() * 1000) - SYState()->LastPing;
		if (tempTime > 900000  )
		{
			NetworkMgr->Disconnect();
		}
	}
	*/

	if (m_UISystem)
	{
		static int ui_update_count = 0;
		if( ++ui_update_count % 2 == 0 )
		{
			 ui_update_delta_sum += m_fFrameTime;

			m_UISystem->Update( ui_update_delta_sum );

			if (SYState()->LocalPlayerInput)
				SYState()->LocalPlayerInput->OnMouseOBJ(ui_update_delta_sum);

			if( NetworkMgr )
				NetworkMgr->Update( ui_update_delta_sum );

			DWORD DeltaTime = (DWORD)(ui_update_delta_sum*1000);

			if ( DamageMgr )
			{
				DamageMgr->UpdateDps();
			}

			if (SYState()->LocalPlayerInput)
			{
				SYState()->LocalPlayerInput->Update(DeltaTime);
			}

			if (SYState()->IAudio)
			{
				SYState()->IAudio->Update(DeltaTime);
			}
			ui_update_delta_sum = 0.f;
		}
		else
			ui_update_delta_sum += m_fFrameTime;

		m_UISystem->UpdateKeyMap( m_fFrameTime );
	}

	m_pkTaskManager->Tick();

	if (SceneMgr)
	{
		SceneMgr->Update(m_fFrameTime);
	}

	if( EffectMgr )
		EffectMgr->Update(m_fAccumTime, m_fFrameTime);

	NiParallelUpdateTaskManager::EndUpdate();
	NiParallelUpdateTaskManager::WaitRenderSema();
}

void CClientApp::RenderFrame()
{
	NiDX9Renderer* pkRenderer = (NiDX9Renderer*)(NiRenderer*)m_spRenderer;
	NiDX9RenderState* pkRenderState = pkRenderer->GetRenderState();
	if(!pkRenderer || pkRenderer->IsDeviceLost())
	{
		NiParallelUpdateTaskManager::SignalRenderSema();
		return;
	}

	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	m_spRenderer->BeginUsingRenderTargetGroup(EffectMgr->GetPostEffectRenderTarget(), NiRenderer::CLEAR_ALL);

	CLIENT_METRIC_TIMER(NiMetricsClockTimer, kTimer2, SCENE_RENDERTIME);
	CLIENT_METRIC_STARTTIMER(kTimer2);
	Begin3DRender();
	if (SceneMgr && SceneMgr->CanRender() && pkPlayer)
	{
		SceneMgr->RenderScene(pkPlayer->GetNiCamera());
	}

	EndRender();
	CLIENT_METRIC_ENDTIMER(kTimer2);

	if(pkPlayer)
		EffectMgr->RenderHudEffects(&pkPlayer->GetNiCamera());

	m_spRenderer->EndUsingRenderTargetGroup();
	m_spRenderer->BeginUsingDefaultRenderTargetGroup(NiRenderer::CLEAR_ZBUFFER | NiRenderer::CLEAR_STENCIL);

	Begin2DRender();
	if (m_UISystem)
	{
		
		m_UISystem->RenderEX();
		EffectMgr->Render2DElements();
		pkRenderState->SetRenderState( D3DRS_MULTISAMPLEANTIALIAS, ClientState->GetMultiSample() );
	}
#if NIMETRICS
	RenderVisualTrackers();
#endif 
	m_spRenderer->EndUsingRenderTargetGroup();

	NiParallelUpdateTaskManager::SignalRenderSema();

	TakeScreenshot();
}
static bool MsgBoxDBError(CDBFile* pDBFile, const char* txt)
{
	if(pDBFile && !pDBFile->Load(txt))
	{
		char buf[256];
		sprintf(buf, "load DBFiles:%s failed!",  txt);
		MessageBox(NULL, buf, "error", MB_OK);
		return false;
	}
	return true;
}
bool CClientApp::Initialize()
{
	::SetClassLong(NiApplication::GetWindowReference(), GCL_HICON,
		(LONG)LoadIcon(NiApplication::GetInstanceReference(),MAKEINTRESOURCE(IDI_ICON1)));
	::SetClassLong(NiApplication::GetWindowReference(),GCL_HICONSM,
		(LONG)LoadIcon(NiApplication::GetInstanceReference(),MAKEINTRESOURCE(IDI_ICON1)));

	NiParallelUpdateTaskManager::Initialize();

	__SY__.ClientApp = this;
	NiSrand(::GetTickCount());

	LangaugeMgr = new LangaugeNotice;
	LangaugeMgr->InitNoticeFile();

	CAnimObject::StaticInitAnimNames();

	m_pkTaskManager = NiNew CTaskManager;
	m_pkTaskManager->Init();

	m_pkResMgr = NiNew CResMgr;

	for (int i = 0; i < SYC_MAX; i++)
	{
		m_Cursors[i] = NULL;
	}
	LoadCursor(SYC_ARROW);
	m_CursorID = SYC_ARROW;

	if (!CreateRenderer())
	{
		MessageBox(NULL, "error", "create randerer failed!", MB_OK);
		return false;
	}

	SYState()->CurDelay = 0;
	SYState()->CurTime = 0;
	SYState()->LastPing = 0;


	// Shader
	CShaderSystem::Setup();

	__SY__.Render = m_spRenderer;
	SceneMgr = NiNew CSceneManager;

#if SUPPORT_GFX
	if(!SyGfxManager::Create(m_spRenderer))
	{
		return FALSE;
	}
#endif 
	__SY__.IAudio = new CAudioInterface;

	ALAudioSystem::Initialize();
	if (!__SY__.IAudio || !__SY__.IAudio->Initialize(m_pkAppWindow->GetRenderWindowReference()))
	{
		//MessageBox(NULL, "错误", "音频初始化错误!", MB_OK);
		//return FALSE;
	}
	
	// 初始化UI 系统 Effect Manager 依赖UI系统, 因此在EFFECTMGR初始化前初始化UI
	if (m_UISystem == NULL)
	{
		m_UISystem = new CUISystem;
		if(!m_UISystem->Initialize(m_spRenderer, m_pkAppWindow->GetRenderWindowReference()))
		{
			MessageBox(NULL, "error", "UI initialize  failed!", MB_OK);
			return FALSE;
		}
		m_UISystem->SetImeHwnd(m_pkAppWindow->GetWindowReference());
		//m_UISystem->PostResize(ClientState->GetScreenWidth(),ClientState->GetScreenHeight());
	}
	__SY__.UI = m_UISystem;


	EffectMgr = NiNew CEffectManager;
	if(!EffectMgr || !EffectMgr->Initialize())
	{
		MessageBox(NULL, "error", " EffectMgr Initialize failed!", MB_OK);
		return FALSE;
	}

	ObjectMgr = NiNew CObjectManager;
	NetworkMgr = NiNew CNetworkManager;
	PacketBuilder = NiNew CPacketBuilder;
	PacketParser = NiNew CPacketParser;
	DamageMgr = new CDpsCount;
	PacketParser->Init();

	__SY__.LocalPlayerInput = new CPlayerInputMgr;
	NIASSERT(__SY__.LocalPlayerInput && SceneMgr);

	if(!SceneMgr->Initialize())
	{
		MessageBox(NULL, "error", "SceneMgr initialize failed!", MB_OK);
		return FALSE;
	}
	if (!__SY__.LocalPlayerInput->Initialize())
	{
		MessageBox(NULL, "error", "local player input initialize failed!", MB_OK);
		return FALSE;
	}

	NiColor bkgColor(0.0f,0.0f,1.0f);
	m_spRenderer->SetBackgroundColor(bkgColor);

	if (!CreateInputSystem())
		return false;
	__SY__.Input = m_spInputSystem;

	
	if (!CreateVisualTrackers())
		return false;

#if NIMETRICS
	ClientProfiler::InitProfiler(GetVisualTrackers());
#endif

	// DB
	bool bLoadDBFile = true;

	// ItemManager
	ItemMgr = NiNew CItemManager;
	if(!(bLoadDBFile &= ItemMgr->Initialize()))
		return false;

	g_pkDefaultDisplayInfo = NiNew CDefaultDisplayInfoDB;
	NIASSERT(g_pkDefaultDisplayInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkDefaultDisplayInfo,  "DBFiles\\create_display.db")))
		return false;

	g_pkCreateDefaultDisplayInfo = NiNew CDefaultDisplayInfoDB;
	NIASSERT(g_pkCreateDefaultDisplayInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkCreateDefaultDisplayInfo, "DBFiles\\create_display_preview.db")))
		return false;


	g_pkQusetInfo = NiNew CQuestDB;
	NIASSERT(g_pkQusetInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkQusetInfo,"DBFiles\\quests.db")))
		return false;


	g_pkQuestStarterInfo = NiNew CQuest_StarterDB;
	NIASSERT(g_pkQuestStarterInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkQuestStarterInfo, "DBFiles\\quest_creature_starter.db")))
		return false;


	g_pkAreaTriggerDB = NiNew CAreaTriggerDB;
	NIASSERT(g_pkAreaTriggerDB);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkAreaTriggerDB, "DBFiles\\areatriggers.db")))
		return false;


	g_pkGameObjectDB = NiNew CGameObjectDB;
	NIASSERT(g_pkGameObjectDB);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkGameObjectDB, "DBFiles\\gameobject_names.DB")))
		return false;

	g_LockObjects = NiNew CLockDB;
	NIASSERT(g_LockObjects);
	if(!(bLoadDBFile &= MsgBoxDBError(g_LockObjects, "DBFiles\\lock_dbc.DB")))
		return false;

	g_pkNpcTex = NiNew CNpcTextDB;
	NIASSERT(g_pkNpcTex);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkNpcTex, "DBFiles\\NpcText.db")))
		return false;


	g_pkSpellInfo = NiNew CSpellDB;
	NIASSERT(g_pkSpellInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkSpellInfo, "DBFiles\\spell.db")))
		return false;


	g_pkSpellRangeInfo = NiNew CSpellRangeDB;
	NIASSERT(g_pkSpellRangeInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkSpellRangeInfo, "DBFiles\\spellrange_dbc.db")))
		return false;


	g_pkSpellDurationInfo = NiNew CSpellDurationDB;
	NIASSERT(g_pkSpellDurationInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkSpellDurationInfo, "DBFiles\\spellduration_dbc.db")))
		return false;

	g_pkSpellRadiusInfo = NiNew CSpellRadiusDB;
	NIASSERT(g_pkSpellRadiusInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkSpellRadiusInfo, "DBFiles\\spellradius_dbc.db")))
		return false ;

	g_pkQuerySpellInfo = NiNew CQuerySpellDB;
	NIASSERT(g_pkQuerySpellInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkQuerySpellInfo, "DBFiles\\spell_for_client.db")))
		return false;


	g_pkQuestStarterDB = NiNew CQuestStarterDB;
	NIASSERT(g_pkQuestStarterDB);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkQuestStarterDB, "DBFiles\\creature_quest_starter.db")))
		return false;


	g_pkQuestFinisherDB = NiNew CQuestFinisherDB;
	NIASSERT(g_pkQuestFinisherDB);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkQuestFinisherDB, "DBFiles\\creature_quest_finisher.db")))
		return false;


	// 	g_pkCreatureSpawns = NiNew CreatureSpawnsDB;
	// 	NIASSERT(g_pkCreatureSpawns);
	// 	g_pkCreatureSpawns->Load("DBFiles\\creature_spawns.db");

	g_pkMapInfoDB = NiNew CMapInfoDB;
	NIASSERT(g_pkMapInfoDB != NULL);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkMapInfoDB, "DBFiles\\worldmap_info.db")))
		return false;


	g_pkAreaDB = NiNew CAreaDB;
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkAreaDB, "DBFiles\\area.db")))
		return false;


	g_pkChatFilterDB = NiNew CChatFilterDB;
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkChatFilterDB, "DBFiles\\ChatFilter.db")))
		return false;


	g_pkLevelTipsDB = NiNew CLevelTipsDB;
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkLevelTipsDB, "DBFiles\\questtips.DB")))
		return false;

	g_pkGuildLevelUpDB = NiNew CGuildLevelUpDB;
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkGuildLevelUpDB, "DBFiles\\guild_upgrade_list.db")))
		return false;


	g_pkInstanceMsgDB = NiNew CInstanceMsg;
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkInstanceMsgDB, "DBFiles\\instancemsg.db")))
		return false;


	g_pkNewBirdCategroyDB = NiNew CNewBirdCategroyDB;
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkNewBirdCategroyDB, "DBFiles\\newbirdcategory.db")))
		return false;



	g_pkNewBirdEventDB = NiNew CNewBirdEventDB;
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkNewBirdEventDB, "DBFiles\\newbirdevent.db")))
		return false;


	g_pkSoundFiles = NiNew SoundDB;
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkSoundFiles, "DBFiles\\soundid.db")))
		return false;

	g_CreatureLootDB = NiNew CreatureLootDB;
	if(!(bLoadDBFile &= MsgBoxDBError(g_CreatureLootDB, "DBFiles\\creatureloot.db")))
		return false;

	g_pkMapToMapPathInfo = new CMapToMapPathInfo;
	bLoadDBFile &= g_pkMapToMapPathInfo->LoadMapFile();


	g_pkCastleNpcListDB = NiNew CCastleNpcListDB;
	NIASSERT(g_pkCastleNpcListDB);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkCastleNpcListDB, "DBFiles\\castle_npc_list.db")))
		return false;


	g_pkItemEnchantDB = NiNew CItemEnchantDB;
	NIASSERT(g_pkItemEnchantDB);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkItemEnchantDB, "DBFiles\\enchantentry_dbc.DB")))
		return false;


	g_pkRefineItemEffectDB = NiNew CRefineitemeffectDB;
	NIASSERT(g_pkRefineItemEffectDB);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkRefineItemEffectDB, "DBFiles\\refine_item_effect_list.db")))
		return false;


	g_pkPlayerTitle = NiNew TitleDB;
	NIASSERT(g_pkPlayerTitle);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkPlayerTitle, "DBFiles\\title.db")))
		return false;

	g_pkPlaeryTitleReward = NiNew TiTleRewardDB;
	NIASSERT(g_pkPlaeryTitleReward);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkPlaeryTitleReward, "DBFiles\\title_reward.DB")))
		return false;



	g_pkCCreatureSpawnsInfo =NiNew CCreatureSpawnsFromClientDB;
	NIASSERT(g_pkCCreatureSpawnsInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkCCreatureSpawnsInfo, "DBFiles\\creature_spawn_for_client.db")))
		return false;

	g_pkSpellGambleInfo =NiNew CSpellGambleDB;
	NIASSERT(g_pkSpellGambleInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkSpellGambleInfo, "DBFiles\\gamble_conf.db")))
		return false;


	g_pkSpellTrainerInfo =NiNew CSpellTrainerDB;
	NIASSERT(g_pkSpellTrainerInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_pkSpellTrainerInfo, "DBFiles\\trainer_spells.DB")))
		return false;

	g_SkillLineNameInfo =NiNew CSkillLineNameDB;
	NIASSERT(g_SkillLineNameInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_SkillLineNameInfo, "DBFiles\\skilllineentry_dbc.DB")))
		return false;

	g_SkillLineSpellInfo =NiNew CSkillLineSpellDB;
	NIASSERT(g_SkillLineSpellInfo);
	if(!(bLoadDBFile &= MsgBoxDBError(g_SkillLineSpellInfo, "DBFiles\\skilllinespell_dbc.DB")))
		return false;

	g_ItemSetDB = NiNew ItemSetDB;
	NIASSERT(g_ItemSetDB);
	if (!(bLoadDBFile &= MsgBoxDBError(g_ItemSetDB, "DBFiles\\itemsetentry_dbc.DB")))
		return false;

	if(!bLoadDBFile)
	{
		MessageBox(NULL, "load DBFile error", "error", MB_OK);
		return false;
	}


	__SY__.SkillMgr = NiNew CSkillManager;
	if (!__SY__.SkillMgr->Initialize())
	{
		MessageBox(NULL, "Skillmgr initialize failed!", "error", MB_OK);
		return FALSE;
	}

	NiParticleSystem::SetParallelUpdateEnabled(true);
	NiParticleSystem::SetParallelUpdatePriority(NiTaskManager::MEDIUM);

	// 创建登录界面
	CUISystem::ShowEX(DID_LOGIN, TRUE, TRUE);

	if (m_spCamera)
	{
		m_spCamera->Update(0.0f);
	}


	return true;
}

void CClientApp::Terminate()
{
	TerminateProcess( GetCurrentProcess(), 0 );
	return;

#if NIMETRICS
	ClientProfiler::ShutdownProfiler();
#endif
	if (__SY__.LocalPlayerInput)
	{
		__SY__.LocalPlayerInput->Shutdown();
		delete __SY__.LocalPlayerInput; 
		__SY__.LocalPlayerInput = NULL;
	}

	if( NetworkMgr )
	{
		NiDelete NetworkMgr;
		NetworkMgr = NULL;
	}
	NiDelete PacketBuilder;
	NiDelete PacketParser;

	if (EffectMgr)
	{
		EffectMgr->Destroy();
		NiDelete EffectMgr;
		EffectMgr = NULL;
	}



	if (m_UISystem)
	{
		delete m_UISystem; m_UISystem = NULL;
	}

	if(m_pkTaskManager)
	{
		m_pkTaskManager->Shutdown();
		NiDelete m_pkTaskManager;
		m_pkTaskManager = NULL;
	}

	if(ItemMgr)
	{
		ItemMgr->Destroy();
		NiDelete ItemMgr;
		ItemMgr = NULL;
	}

	NiDelete ObjectMgr;
	ObjectMgr = NULL;


	NiDelete __SY__.SkillMgr;
	__SY__.SkillMgr = NULL;

	if (SceneMgr)
	{
		SceneMgr->Destroy();
		NiDelete SceneMgr;
		SceneMgr = NULL;
	}

	if (__SY__.IAudio)
	{
		__SY__.IAudio->Destroy();
		delete __SY__.IAudio; __SY__.IAudio = NULL;
	}
	ALAudioSystem::Release();



	CRT2Tex::SM_Destory();

	CShaderSystem::Shutdown();
	CAnimObject::StaticDeInitAnimNames();

#if SUPPORT_GFX
	SyGfxManager::Destroy();
#endif 
	//SyScripting::Shutdown();

	NiDelete m_pkResMgr;

	// DB
	if(g_pkDefaultDisplayInfo)
	{
		NiDelete g_pkDefaultDisplayInfo;
		g_pkDefaultDisplayInfo = NULL;
	}
	if (g_pkCreateDefaultDisplayInfo)
	{
		NiDelete g_pkCreateDefaultDisplayInfo;
		g_pkCreateDefaultDisplayInfo = NULL;
	}

	if(g_pkQusetInfo)
	{
		NiDelete g_pkQusetInfo;
		g_pkQusetInfo = NULL;
	}

	if(g_pkNpcTex)
	{
		NiDelete g_pkNpcTex;
		g_pkNpcTex = NULL;
	}

	if(g_pkSpellInfo)
	{
		NiDelete g_pkSpellInfo;
		g_pkSpellInfo = NULL;
	}

	if (g_pkSpellRangeInfo)
	{
		NiDelete g_pkSpellRangeInfo;
		g_pkSpellRangeInfo = NULL;
	}

	if (g_pkSpellDurationInfo)
	{
		NiDelete g_pkSpellDurationInfo;
		g_pkSpellDurationInfo = NULL;
	}

	if (g_pkQuerySpellInfo)
	{
		NiDelete g_pkQuerySpellInfo;
		g_pkQuerySpellInfo = NULL;
	}

	if (g_pkSpellRadiusInfo)
	{
		NiDelete g_pkSpellRadiusInfo ;
		g_pkSpellRadiusInfo = NULL ;
	}

	if(g_pkLevelTipsDB)
	{
		NiDelete g_pkLevelTipsDB;
		g_pkLevelTipsDB = NULL;
	}

	if(g_pkQuestStarterDB)
	{
		NiDelete g_pkQuestStarterDB;
		g_pkQuestStarterDB = NULL;
	}

	if(g_pkQuestFinisherDB)
	{
		NiDelete g_pkQuestFinisherDB;
		g_pkQuestFinisherDB = NULL;
	}

	if(g_pkCreatureSpawns)
	{
		NiDelete g_pkCreatureSpawns;
		g_pkCreatureSpawns = NULL;
	}

	if (g_pkCCreatureSpawnsInfo)
	{
		NiDelete g_pkCCreatureSpawnsInfo;
		g_pkCCreatureSpawnsInfo = NULL;
	}

	if(g_pkMapInfoDB)
	{
		NiDelete g_pkMapInfoDB;
		g_pkMapInfoDB = NULL;
	}

	if (g_pkPlayerTitle)
	{
		NiDelete g_pkPlayerTitle;
		g_pkPlayerTitle = NULL; 
	}

	if (g_pkQuestStarterInfo)
	{
		NiDelete g_pkQuestStarterInfo;
		g_pkQuestStarterInfo = NULL;
	}

	if(g_pkAreaDB)
	{
		g_pkAreaDB->Unload();
		NiDelete g_pkAreaDB;
		g_pkAreaDB = NULL;
	}

	if (g_pkAreaTriggerDB)
	{
		NiDelete g_pkAreaTriggerDB;
		g_pkAreaTriggerDB = NULL;
	}

	if (g_pkGuildLevelUpDB)
	{
		NiDelete g_pkGuildLevelUpDB;
		g_pkGuildLevelUpDB = NULL;
	}

	if (g_pkGameObjectDB)
	{
		NiDelete g_pkGameObjectDB;
		g_pkGameObjectDB = NULL;
	}

	if(g_pkChatFilterDB)
	{
		NiDelete g_pkChatFilterDB;
		g_pkChatFilterDB = NULL;
	}

	if(g_pkLevelTipsDB)
	{
		NiDelete g_pkLevelTipsDB;
		g_pkLevelTipsDB = NULL;
	}

	if (g_pkInstanceMsgDB)
	{
		NiDelete g_pkInstanceMsgDB;
		g_pkInstanceMsgDB = NULL;
	}

	if(g_pkNewBirdCategroyDB)
	{
		NiDelete g_pkNewBirdCategroyDB;
	}

	if(g_pkMapToMapPathInfo)
	{
		delete g_pkMapToMapPathInfo;
	}

	if (g_pkNewBirdEventDB)
	{
		NiDelete g_pkNewBirdEventDB;
		g_pkNewBirdEventDB = NULL;
	}

	if (g_pkSoundFiles)
	{
		NiDelete g_pkSoundFiles;
		g_pkSoundFiles = NULL;
	}
	
	if (g_CreatureLootDB)
	{
		NiDelete g_CreatureLootDB;
		g_CreatureLootDB = NULL;
	}
	if(g_pkPlaeryTitleReward)
	{
		NiDelete g_pkPlaeryTitleReward;
		g_pkPlaeryTitleReward = NULL;
	}

	if (g_pkCastleNpcListDB)
	{
		NiDelete g_pkCastleNpcListDB;
		g_pkCastleNpcListDB = NULL;
	}
	if (g_pkItemEnchantDB)
	{
		NiDelete g_pkItemEnchantDB;
		g_pkItemEnchantDB = NULL;
	}
	if (g_pkRefineItemEffectDB)
	{
		NiDelete g_pkRefineItemEffectDB;
		g_pkRefineItemEffectDB = NULL;
	}
	if (g_pkSpellGambleInfo)
	{
		NiDelete g_pkSpellGambleInfo;
		g_pkSpellGambleInfo = NULL;
	}
	if (g_pkSpellTrainerInfo)
	{
		NiDelete g_pkSpellTrainerInfo;
		g_pkSpellTrainerInfo = NULL;
	}
	if (g_SkillLineNameInfo)
	{
		NiDelete g_SkillLineNameInfo;
		g_SkillLineNameInfo = NULL;
	}
	if (g_SkillLineSpellInfo)
	{
		NiDelete g_SkillLineSpellInfo;
		g_SkillLineSpellInfo = NULL;
	}
	if (g_ItemSetDB)
	{
		NiDelete g_ItemSetDB;
		g_ItemSetDB = NULL;
	}
	if (LangaugeMgr)
	{
		LangaugeMgr->ShutDown();
		delete LangaugeMgr ;
		LangaugeMgr = NULL ;
	}
	CPackFile::Shutdown( true );
	CPackFile::Shutdown( false );

	NiParallelUpdateTaskManager::Shutdown();

	NiApplication::Terminate();
}

bool CClientApp::OnWindowResize(int iWidth, int iHeight, 
								unsigned int uiSizeType, NiWindowRef pWnd)
{
	if(!NiApplication::OnWindowResize(iWidth, iHeight, uiSizeType, pWnd))
	{
		return false;
	}
	if (m_UISystem)
	{
		m_UISystem->PostResize(iWidth, iHeight);
	}
	return true;
}
void CClientApp::ProcessInput()
{
	ProcessVisualTrackerInput();
	// Minimally, we provide a way to quit if the NiApplication derived app 
	// does not override with it's own ProcessInput.
	NiInputKeyboard* pkKeyboard = GetInputSystem()->GetKeyboard();
	if (pkKeyboard)
	{
		/*if (pkKeyboard->KeyWasPressed(NiInputKeyboard::KEY_ESCAPE))
		{
		QuitApplication();
		}
		elseif (pkKeyboard->KeyWasPressed(NiInputKeyboard::KEY_F2))
		{
		// We have overriden Initialize so that we can configure our
		// input devices.
		ConfigureInputDevices();
		}*/ 
	}

	// Allow ANY gamepad to exit the app
	NiInputGamePad* pkGamePad;
	for (unsigned int uiPort = 0; uiPort < NiInputSystem::MAX_GAMEPADS; 
		uiPort++)
	{
		pkGamePad = m_spInputSystem->GetGamePad(uiPort);
		if (pkGamePad)
		{
			if (pkGamePad->ButtonIsDown(NiInputGamePad::NIGP_START) &&
				pkGamePad->ButtonIsDown(NiInputGamePad::NIGP_SELECT))
			{
				QuitApplication();
			}
		}
	}
}

void CClientApp::ResetMutiSample()
{
	NiDX9Renderer* pkRenderer = (NiDX9Renderer*)(NiRenderer*)m_spRenderer;
	NiDX9RenderState* pkRenderState = pkRenderer->GetRenderState();

	pkRenderState->SetRenderState( D3DRS_MULTISAMPLEANTIALIAS, ClientState->GetMultiSample() );
}

bool CClientApp::OnDefault(NiEventRef pEventRecord)
{
	switch(pEventRecord->uiMsg)
	{
	case WM_SETCURSOR:
		{
			::SetCursor(m_Cursors[m_CursorID]);
		}
	case WM_ACTIVATE:
		{
			DWORD fActive  = LOWORD(pEventRecord->wParam);   // activation flag
			//BOOL bMinimized = (BOOL)HIWORD(pEventRecord->wParam); // minimized flag
			switch( fActive )
			{
				case WA_INACTIVE:
					{
						if ( AudioSystem::GetAudioSystem() )
						{
							AudioSystem::GetAudioSystem()->Mute( true );
						}
					}
					break;
				case WA_ACTIVE:
					{
						if ( AudioSystem::GetAudioSystem() && !(::IsIconic( pEventRecord->hWnd )) )
						{
							AudioSystem::GetAudioSystem()->Mute( false );
						}
					}
					break;
				case WA_CLICKACTIVE:
					{
						if ( AudioSystem::GetAudioSystem() && !(::IsIconic( pEventRecord->hWnd )) )
						{
							AudioSystem::GetAudioSystem()->Mute( false );
						}
					}
					break;
				default:
					break;
			}
			//HWND hwndPrevious= (HWND)lParam;   // window handle
		}
		return true;
		// 	case WM_KILLFOCUS:
		// 	case WM_INITMENUPOPUP:
		// 		{
		// 			if(!ObjectMgr)
		// 				break;
		// 
		// 			CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
		// 			if(!pkLocalPlayer)
		// 				break;
		// 
		// 			pkLocalPlayer->ResetMovementFlags();
		// 			SYState()->LocalPlayerInput->ResetMoveKeys();
		// 			SYState()->LocalPlayerInput->UpdateMoveKeys();
		// 			pkLocalPlayer->StopMove();
		// 		}
		// 		break;
	default:
		break;
	}
	// return true : 消息交由默认窗口过程处理.
	BOOL bProceded = FALSE;
	if (m_UISystem )
	{
		bProceded = m_UISystem->ProcessMessage(pEventRecord);
	}


	return bProceded ? true : false;
}

void CClientApp::Begin3DRender()
{
}

void CClientApp::Begin2DRender()
{
	//
	//BeginRender();
}

BOOL CClientApp::LoadCursor(ESYCursorID eID)
{
	if (m_Cursors[eID])
	{
		return TRUE;
	}
	static const char* CursorFiles[SYC_MAX] = 
	{
		"UData\\Cursors\\Arrow1.ani",
		"UData\\Cursors\\SelectTarget.ani",
		"UData\\Cursors\\SelectTargetError.ani",
		"UData\\Cursors\\Buy.ani",
		"UData\\Cursors\\Sale.ani",
		"UData\\Cursors\\Repair.ani",
		"UData\\Cursors\\RepairAll.ani",
		"UData\\Cursors\\SizeNS.ani",
		"UData\\Cursors\\NPCTalk.ani",
		"UData\\Cursors\\NOTPick.ani",
		"UData\\Cursors\\Arrow.ani",
		"UData\\Cursors\\BannerOpen.ani",
		"UData\\Cursors\\Unbind2.ani",
		"UData\\Cursors\\Ench.ani",
		"UData\\Cursors\\shovel.ani",
		"UData\\Cursors\\Mine.ani",
		"UData\\Cursors\\Herb.ani",
		"UData\\Cursors\\Mail.ani",
	};

	HCURSOR hCur = LoadCursorFromFile(CursorFiles[eID]);
	m_Cursors[eID] = hCur;
	return TRUE;
}

void CClientApp::SetCursor(ESYCursorID eCursorID)
{

	if (eCursorID == m_CursorID)
	{
		return ;
	}

	if ( SYState()->LocalPlayerInput->IsSelectingItem() )
	{
		return;
	}

	LoadCursor(eCursorID);
	m_CursorID = eCursorID;
	::SetCursor(m_Cursors[m_CursorID]);

}


#if NIMETRICS
void CClientApp::ProcessVisualTrackerInput()
{
	ClientProfiler::UpdateInput();
}
#endif
