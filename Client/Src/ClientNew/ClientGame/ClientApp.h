#pragma once

#include "CullingProcess.h"
#include "Map/SoundEmitter.h"

class CUISystem;
class CResMgr;
class CEffectManager;
class CCullingProcess;
class CTaskManager;

enum ESYCursorID
{
	SYC_ARROW = 0,	
	SYC_PICK,//拾取
	SYC_SELECTTARGETERROR,
	SYC_BUY,              //NPC购买   鼠标
	SYC_SALE,             //NPC出售   鼠标
	SYC_REPAIR,           //修理      鼠标
	SYC_REPAIRALL,        //修理全部  鼠标
	SYC_SIZENS,    // 更改窗口大小
	SYC_SELECTTARGET,     
	SYC_NOTPICK,  //不能拾取
	SYC_ATTACK, //可攻击
	SYC_BANNEROPEN, // 旗帜开启
	SYC_UNBANGDING, // 解除绑定
	SYC_ENCHANT,//附魔
	SYC_SHOVEL,// 铲子
	SYC_MINE,//采矿
	SYC_HERB,//采药
	SYC_MAIL,//采药
	SYC_MAX,
};

class CClientApp : public NiApplication
{
public:
	CClientApp(void);
	virtual ~CClientApp(void);
	
	void SetCursor(ESYCursorID eCursorID);
	//
	NiRenderer* GetRender() { return m_spRenderer; }

    void RecreateRender();
    void GetVideoSize(unsigned int& uiWidth, unsigned int& uiHeight);
	void ResetMutiSample();

protected:
	// 重写
	virtual bool Initialize();
	virtual void Terminate();
	virtual bool CreateRenderer();
	virtual void UpdateFrame();
	virtual void RenderFrame();
	virtual bool CreateCamera();
	virtual bool OnWindowResize(int iWidth, int iHeight, unsigned int uiSizeType, NiWindowRef pWnd);
	virtual bool OnDefault(NiEventRef pEventRecord);
	virtual void ProcessInput();
#if NIMETRICS
	virtual void ProcessVisualTrackerInput();
#endif 
protected:
	BOOL LoadCursor(ESYCursorID eID);
	void Begin3DRender();
	void Begin2DRender();

protected:
	CUISystem* m_UISystem;				// 用户界面系统.
	HCURSOR m_Cursors[SYC_MAX];
	ESYCursorID m_CursorID;

	CCullingProcessPtr m_spCullingProcess;
    unsigned int m_uiDesktopWidth;
    unsigned int m_uiDesktopHeight;

	CResMgr* m_pkResMgr;
    CTaskManager* m_pkTaskManager;
};

