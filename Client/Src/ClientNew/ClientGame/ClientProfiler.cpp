#include "StdAfx.h"
#include "ClientProfiler.h"
#if NIMETRICS

ClientProfiler* client_prof = NULL;
BOOL ClientProfiler::InitProfiler(NiTPointerList<NiVisualTrackerPtr>& TrackerList)
{
	client_prof = NiNew ClientProfiler;
	return client_prof->Init(TrackerList);
}

void ClientProfiler::ShutdownProfiler()
{
	if (client_prof)
	{
		//NiDelete client_prof;
		client_prof = NULL;
	}
}

void ClientProfiler::UpdateInput()
{
	if (client_prof == NULL)
	{
		return;
	}
	/*NiInputKeyboard* pkKeyboard = NiApplication::ms_pkApplication->GetInputSystem()->GetKeyboard();
	if (pkKeyboard)
	{
		static BOOL bF5Down = 0; 
		bool bCurDown = pkKeyboard->KeyWasPressed(NiInputKeyboard::KEY_F5);
		if (bCurDown && !bF5Down)
		{
			bF5Down = TRUE;
			client_prof->ShowProfGroup(MAIN);
		}else 
		{
			bF5Down = FALSE;
		}


		static BOOL bF6Down = 0;
		bCurDown = pkKeyboard->KeyWasPressed(NiInputKeyboard::KEY_F6);
		if (bCurDown && !bF6Down)
		{
			bF6Down = TRUE;
			client_prof->ShowProfGroup(CLIENT);
		}else 
		{
			bF6Down = FALSE;
		}
	}*/
}
ClientProfiler::ClientProfiler():NiVisualTrackerOutput(100)
{
	for (int g = 0; g < MAX; g++)
	{
		for (int i = 0;  i < ENTRY_MAX; i++)
		{
			m_pMainTrackers[g][i] = NULL;
		}
	}
	for (int g = 0; g < MAX; g++)
	{
		m_bGroupShow[g] = FALSE;
	}
}

ClientProfiler::~ClientProfiler(void)
{
}

BOOL ClientProfiler::Init(NiTPointerList<NiVisualTrackerPtr>& TrackerList)
{
	const unsigned int uiFramePeriod = 100;
	NiMetricsLayer::AddOutputModule(this);

	CreateMainTrackers(TrackerList);
	CreateClientGroup(TrackerList);
	return TRUE;
}
void ClientProfiler::Shutdown()
{

}

void ClientProfiler::ShowProfGroup(Group eGroup)
{
	NiVisualTracker** pTrackerGroup = m_pMainTrackers[eGroup];
	BOOL bShow = !m_bGroupShow[eGroup];
	m_bGroupShow[eGroup] = !m_bGroupShow[eGroup];
	for (int i = 0 ; i < ENTRY_MAX; i++)
	{
		if (pTrackerGroup[i])
		{
			pTrackerGroup[i]->SetShow( bShow? true : false);
		}
	}

	for (int g = 0; g < MAX ; g++)
	{
		if (g != eGroup)
		{
			pTrackerGroup = m_pMainTrackers[g];
			for (int i = 0 ; i < ENTRY_MAX; i++)
			{
				if (pTrackerGroup[i])
				{
					pTrackerGroup[i]->SetShow(false);
				}
			}
			m_bGroupShow[g] = FALSE;
		}
	}
}
void ClientProfiler::CreateMainTrackers(NiTPointerList<NiVisualTrackerPtr>& TrackerList)
{
	const float fLeftBorder = 0.05f;
	const float fTopBorder = 0.025f;
	const float fRegionHeight = 0.25f;
	const float fRegionWidth = 0.90f;

	NiRect<float> kWindowRect1;
	kWindowRect1.m_left   = fLeftBorder;
	kWindowRect1.m_right  = kWindowRect1.m_left + fRegionWidth;
	kWindowRect1.m_top    = 0.15f;
	kWindowRect1.m_bottom = kWindowRect1.m_top + fRegionHeight;

	NiRect<float> kWindowRect2;
	kWindowRect2.m_left   = fLeftBorder;
	kWindowRect2.m_right  = kWindowRect2.m_left + fRegionWidth;
	kWindowRect2.m_top    = kWindowRect1.m_bottom + fTopBorder;
	kWindowRect2.m_bottom = kWindowRect2.m_top + fRegionHeight;

	NiRect<float> kWindowRect3;
	kWindowRect3.m_left   = fLeftBorder;
	kWindowRect3.m_right  = kWindowRect3.m_left + fRegionWidth;
	kWindowRect3.m_top    = kWindowRect2.m_bottom + fTopBorder;
	kWindowRect3.m_bottom = kWindowRect3.m_top + fRegionHeight;

	NiColor blue(0.0f, 0.0f, 1.0f);
	NiColor yellow(1.0f, 1.0f, 0.0f);
	NiColor red(1.0f, 0.0f, 0.0f);

	NiVisualTracker* pkTracker = NULL;
	NiTListIterator kIter = TrackerList.GetHeadPos();
	// First Tracker
	pkTracker = TrackerList.GetNext(kIter);
	pkTracker->RemoveAll();
	AddGraph(pkTracker, NiParticleMetrics::ms_acNames[NiParticleMetrics::UPDATED_PARTICLES],
		NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, NiColor(0.0f, 1.0f, 0.0f),
		100, 0.1f, true, 1.0f, false, "Particles");

	AddGraph(pkTracker, NiApplicationMetrics::ms_acNames[NiApplicationMetrics::FRAMERATE],
		NiVisualTrackerOutput::FUNC_MEAN, NiColor(1.0f, 1.0f, 0.0f),
		100, 0.1f, true, 1.0f, false, "FPS");

	// Number of draw primitive calls
	AddGraph(pkTracker, NiDx9RendererMetrics::ms_acNames[NiDx9RendererMetrics::DRAW_PRIMITIVE],
		NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, NiColor(1.0f, 0.0f, 0.0f),
		100, 0.1f, true, 2.0f, false, "DrawCalls/2");
	m_pMainTrackers[MAIN][0] = pkTracker;

	// Second Tracker (Timing)
	pkTracker = TrackerList.GetNext(kIter);
	m_pMainTrackers[MAIN][1] = pkTracker;
#ifdef NI_MEMORY_DEBUGGER
	// Third Tracker (memory)
	NiMemTracker *pkMemTracker = NiMemTracker::Get();
	if (pkMemTracker)
	{
		pkTracker = TrackerList.RemoveTail();
		

		pkTracker = NiNew NiVisualTracker(500.0, 0, kWindowRect3, "Memory (MB)", true, 3);
		pkTracker->SetShow(false);
		pkTracker->AddGraph(NiNew MemHighWaterMarkUpdate(
			1024.0f*1024.0f, pkMemTracker), "High Watermark",
			red, 100, 0.1f, true);
		pkTracker->AddGraph(NiNew MemCurrentUpdate(
			1024.0f*1024.0f, pkMemTracker), "Current",
			yellow, 100, 0.1f, true);
		pkTracker->AddGraph(NiNew MemCurrentAllocCountUpdate(
			1.0f, pkMemTracker), "Num Allocs",
			blue, 100, 0.1f, true);

		TrackerList.AddTail(pkTracker);
		m_pMainTrackers[MAIN][2] = pkTracker;
	}
#endif
}


void ClientProfiler::CreateClientGroup(NiTPointerList<NiVisualTrackerPtr>& TrackerList)
{
	const float fLeftBorder = 0.05f;
	const float fTopBorder = 0.025f;
	const float fRegionHeight = 0.8f;
	const float fRegionWidth = 0.90f;

	NiRect<float> kWindowRect1;
	kWindowRect1.m_left   = fLeftBorder;
	kWindowRect1.m_right  = kWindowRect1.m_left + fRegionWidth;
	kWindowRect1.m_top    = 0.15f;
	kWindowRect1.m_bottom = kWindowRect1.m_top + fRegionHeight;

	NiRect<float> kWindowRect2;
	kWindowRect2.m_left   = fLeftBorder;
	kWindowRect2.m_right  = kWindowRect2.m_left + fRegionWidth;
	kWindowRect2.m_top    = kWindowRect1.m_bottom + fTopBorder;
	kWindowRect2.m_bottom = kWindowRect2.m_top + fRegionHeight;

	NiRect<float> kWindowRect3;
	kWindowRect3.m_left   = fLeftBorder;
	kWindowRect3.m_right  = kWindowRect3.m_left + fRegionWidth;
	kWindowRect3.m_top    = kWindowRect2.m_bottom + fTopBorder;
	kWindowRect3.m_bottom = kWindowRect3.m_top + fRegionHeight;

	NiColor network(0.0f, 0.0f, 1.0f);
	NiColor scene_cull(0.0f, 1.0f, 0.0f);
	NiColor shaow_draw(1.0f, 0.0f, 0.0f);
	NiColor terr_update(0.0f, 1.0f, 1.0f);
	NiColor terr_cull(1.0f, 0.0f, 1.0f);
	NiColor terr_draw(1.0f, 1.0f, 0.0f);

	NiColor scene_update(1.0f, 0.6f, 0.5f);
	NiColor scene_draw(1.0f, 1.0f, 1.0f);
	
	

	NiVisualTracker* pkTracker = NULL;
	pkTracker = NiNew NiVisualTracker(75, 0, kWindowRect1, "Client Update(ms)", false, 7);

	AddGraph(pkTracker, SYClientMetrics::ms_acNames[SYClientMetrics::NETWORK_UPDATETIME],
		NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, network,
		100, 0.1f, true, 0.001f, false, "Network");

	AddGraph(pkTracker, SYClientMetrics::ms_acNames[SYClientMetrics::TERRAIN_UPDATETIME],
		NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, terr_update,
		100, 0.1f, true, 0.001f, false, "TerrUpdate");

	AddGraph(pkTracker, SYClientMetrics::ms_acNames[SYClientMetrics::TERRAIN_CULLTIME],
		NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, terr_cull,
		100, 0.1f, true, 0.001f, false, "TerrCull");

	AddGraph(pkTracker, SYClientMetrics::ms_acNames[SYClientMetrics::TERRAIN_RENDERTIME],
		NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, terr_draw,
		100, 0.1f, true, 0.001f, false, "TerrDraw");

	AddGraph(pkTracker, SYClientMetrics::ms_acNames[SYClientMetrics::SCENE_RENDERTIME],
		NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, scene_draw,
		100, 0.1f, true, 0.001f, false, "SceneDraw");

	AddGraph(pkTracker, SYClientMetrics::ms_acNames[SYClientMetrics::SCENE_UPDATETIME],
		NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, scene_update,
		100, 0.1f, true, 0.001f, false, "SceneUpdate");

	AddGraph(pkTracker, SYClientMetrics::ms_acNames[SYClientMetrics::SCENE_CULLTIME],
		NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, scene_cull,
		100, 0.1f, true, 0.001f, false, "SceneCull");

	//AddGraph(pkTracker, SYClientMetrics::ms_acNames[SYClientMetrics::SHADOW_RENDERTIME],
	//	NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, shaow_draw,
	//	100, 0.1f, true, 0.001f, false, "Shadow");
	//AddGraph(pkTracker, SYClientMetrics::ms_acNames[SYClientMetrics::MAP_LOAD_TIME],
	//	NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, NiColor(1.0f, 0.0f, 0.0f),
	//	100, 0.1f, true, 2.0f, false, "MapLoad");


	TrackerList.AddTail(pkTracker);
	m_pMainTrackers[CLIENT][0] = pkTracker;
}

#endif 