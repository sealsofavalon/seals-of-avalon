#pragma once
#if NIMETRICS
#include <NiMetricsOutput.h>
class ClientProfiler : public NiVisualTrackerOutput
{
	enum Group
	{
		MAIN = 0,
		CLIENT,
		NETWORK,
		SCENE,
		MAX,
	};
	enum 
	{
		ENTRY_MAX =  8,
	};
public:
	ClientProfiler();
	~ClientProfiler(void);
	static BOOL InitProfiler(NiTPointerList<NiVisualTrackerPtr>& TrackerList);
	static void ShutdownProfiler();
	static void UpdateInput();
private:
	BOOL Init(NiTPointerList<NiVisualTrackerPtr>& TrackerList);
	void Shutdown();
	void CreateMainTrackers(NiTPointerList<NiVisualTrackerPtr>& TrackerList);
	void CreateClientGroup(NiTPointerList<NiVisualTrackerPtr>& TrackerList);
	void ShowProfGroup(Group eGroup);
	
	NiVisualTracker* m_pMainTrackers[MAX][ENTRY_MAX];
	BOOL m_bGroupShow[MAX];
};

#endif 