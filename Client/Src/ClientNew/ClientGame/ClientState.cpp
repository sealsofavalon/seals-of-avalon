
#include "StdAfx.h"
#include "ClientState.h"
#include "ClientApp.h"
#include "Effect/EffectManager.h"

#include "../../../External/json/json.h"
#ifdef _DEBUG
#pragma comment( lib, "../../../External/json/lib/json_d.lib" )
#else
#pragma comment( lib, "../../../External/json/lib/json.lib" )
#endif

#define CONFIG_FILE ("config.json")
#define LOGIN_FILE ("login.json")
#define CLIENT_INFO_FILE ("Data\\client_info.json")

CClientState::CClientState()
{
	m_bEnableBloom = false;

	m_kLoginIP = "192.168.1.86";
	m_usLoginPort = 7000;

	m_uiWidth = 1024;
	m_uiHeight = 768;
	m_bFullScreen = false;

	m_fCameraNearDist = 1.0f;
	m_fCameraFarDist = m_fCameraNearDist * 1000.0f;

	m_fCameraFOV = 45.0f;

	m_bWaterReflect = true;
    m_bCheckFile = false;

	m_bMultiSample = false;

	uiPresentationInterval = NiDX9Renderer::PRESENT_INTERVAL_ONE;

	LoadConfig();
    LoadClientInfo();
    LoadLogin();

	ParseCommandLine();

	memset(&m_stEnterGameInfo, 0, sizeof(EnterGameInfo));
}

CClientState::~CClientState()
{
}

void CClientState::SetPresentationInterval(unsigned int uiValue)
{
	if ( uiPresentationInterval != uiValue )
	{
		//Recreate
		uiPresentationInterval = uiValue;
		SYState()->ClientApp->RecreateRender();
	}
}

void CClientState::LoadConfig()
{
    NiFile* pkFile = NiFile::GetFile(CONFIG_FILE, NiFile::READ_ONLY);
    if(!pkFile || !(*pkFile))
    {
        NiDelete pkFile;
        return;
    }

    char* acBuffer = (char*)malloc( 64*1024 );
    unsigned int uiSize = pkFile->Read(acBuffer, 64*1024);
    NIASSERT(uiSize < 64*1024 - 1);
    acBuffer[uiSize] = '\0';

    NiDelete pkFile;

    Json::Reader Reader;
    Json::Value Root;

    if( !Reader.parse(acBuffer, Root) )
    {
        NIASSERT(false && "parse config error");
		free( acBuffer );
        return;
    }
    
    const Json::Value& CameraNear = Root["CameraNear"];
    if(CameraNear.isNumeric())
        m_fCameraNearDist = (float)CameraNear.asDouble();

    const Json::Value& CameraFar = Root["CameraFar"];
    if(CameraFar.isNumeric())
        m_fCameraFarDist = (float)CameraFar.asDouble();

    const Json::Value& CameraFOV = Root["CameraFOV"];
    if(CameraFOV.isNumeric())
        m_fCameraFOV = (float)CameraFOV.asDouble();

    const Json::Value& LoginIP = Root["LoginIP"];
    if(LoginIP.isString())
        m_kLoginIP = LoginIP.asCString();

    const Json::Value& LoginPort = Root["LoginPort"];
    if(LoginPort.isString())
        m_usLoginPort = atoi(LoginPort.asCString());
    else if(LoginPort.isNumeric())
        m_usLoginPort = (unsigned short)LoginPort.asDouble();

    const Json::Value& Bloom = Root["Bloom"];
    if(Bloom.isArray())
    {
        if( Bloom[0u].isBool() )
            m_bEnableBloom = Bloom[0u].asBool();

        if( Bloom[1].isDouble() )
            m_kBloomParams.fThreshold = (float)Bloom[1].asDouble();

        if( Bloom[2].isDouble() )
            m_kBloomParams.fIntensity = (float)Bloom[2].asDouble();

        if( Bloom[3].isDouble() )
            m_kBloomParams.fBaseIntensity = (float)Bloom[3].asDouble();

        if( Bloom[4].isDouble() )
            m_kBloomParams.fSaturation = (float)Bloom[4].asDouble();

        if( Bloom[5].isDouble() )
            m_kBloomParams.fBaseSaturation = (float)Bloom[5].asDouble();

        if( Bloom[6].isDouble() )
            m_kBloomParams.fBlur = (float)Bloom[6].asDouble();
    }

    //const Json::Value& WindowSize = Root["WindowSize"];
    //if(WindowSize.isArray() && WindowSize[0u].isInt() && WindowSize[1].isInt())
    //{
    //    m_uiWidth = WindowSize[0u].asInt();
    //    m_uiHeight = WindowSize[1].asInt();
    //}

	m_uiWidth = ::GetSystemMetrics(SM_CXSCREEN);
	m_uiHeight = ::GetSystemMetrics(SM_CYSCREEN);

	Json::Value WindowSize;
	WindowSize[0u] = m_uiWidth;
	WindowSize[1] = m_uiHeight;
	Root["WindowSize"] = WindowSize;

    //const Json::Value& FullScreen = 
    //if(FullScreen.isBool())
    //    m_bFullScreen = FullScreen.asBool();

	m_bFullScreen = true;
	Root["FullScreen"] = m_bFullScreen;

    const Json::Value& WaterReflect = Root["WaterReflect"];
    if(WaterReflect.isBool())
        m_bWaterReflect = WaterReflect.asBool();

	const Json::Value& MultiSample = Root["MultiSample"];
	if(MultiSample.isBool())
		m_bMultiSample = MultiSample.asBool();

	const Json::Value& PresentationInterval = Root["PresentationInterval"];
	if ( PresentationInterval.isInt() )
		uiPresentationInterval = PresentationInterval.asInt();

	//----------------Save-------------------------
	Json::StyledWriter writer;
	std::string buffer = writer.write( Root );
	free( acBuffer );

	//pkFile = NiFile::GetFile(CONFIG_FILE, NiFile::WRITE_ONLY);
	//if(!pkFile || !(*pkFile))
	//{
	//	NiDelete pkFile;
	//	return;
	//}

	//pkFile->Write(buffer.c_str(), buffer.size());
	//NiDelete pkFile;
}

void CClientState::LoadClientInfo()
{
    NiFile* pkFile = NiFile::GetFile(CLIENT_INFO_FILE, NiFile::READ_ONLY);
    if(!pkFile || !(*pkFile))
    {
        NiDelete pkFile;
        return;
    }

    char* acBuffer = (char*)malloc( 64*1024 );
    unsigned int uiSize = pkFile->Read(acBuffer, 64*1024);
    NIASSERT(uiSize < 64*1024 - 1);
    acBuffer[uiSize] = '\0';

    NiDelete pkFile;

    Json::Reader Reader;
    Json::Value Root;

    if( !Reader.parse(acBuffer, Root) )
    {
        DASSERT(false && "parse client info error");
		free( acBuffer );
        return;
    }

    const Json::Value& VersionValue = Root["version"];
    if(VersionValue.isString())
        m_strClientVersion = VersionValue.asString();

	free( acBuffer );
}

void CClientState::LoadLogin()
{
    NiFile* pkFile = NiFile::GetFile(LOGIN_FILE, NiFile::READ_ONLY);
    if(!pkFile || !(*pkFile))
    {
        NiDelete pkFile;
        return;
    }

    char* acBuffer = (char*)malloc( 64*1024 );
    unsigned int uiSize = pkFile->Read(acBuffer, 64*1024);
    NIASSERT(uiSize < 64*1024 - 1);
    acBuffer[uiSize] = '\0';

    NiDelete pkFile;

    Json::Reader Reader;
    Json::Value Root;

    if( !Reader.parse(acBuffer, Root) )
    {
        NIASSERT(false && "parse client info error");
		free( acBuffer );
        return;
    }

    const Json::Value& LoginIP = Root["LoginIP"];
    if(LoginIP.isString())
        m_kLoginIP = LoginIP.asCString();

    const Json::Value& LoginPort = Root["LoginPort"];
    if(LoginPort.isString())
        m_usLoginPort = atoi(LoginPort.asCString());
    else if(LoginPort.isNumeric())
        m_usLoginPort = (unsigned short)LoginPort.asDouble();

    const Json::Value& WindowSize = Root["WindowSize"];
    if(WindowSize.isArray() && WindowSize[0u].isInt() && WindowSize[1].isInt())
    {
        m_uiWidth = WindowSize[0u].asInt();
        m_uiHeight = WindowSize[1].asInt();
    }

    const Json::Value& FullScreen = Root["FullScreen"];
    if(FullScreen.isBool())
        m_bFullScreen = FullScreen.asBool();

    const Json::Value& WaterReflect = Root["WaterReflect"];
    if(WaterReflect.isBool())
        m_bWaterReflect = WaterReflect.asBool();

	const Json::Value& MultiSample = Root["MultiSample"];
	if(MultiSample.isBool())
		m_bMultiSample = MultiSample.asBool();

    const Json::Value& BloomEffect = Root["BloomEffect"];
    if(BloomEffect.isBool())
        m_bEnableBloom = BloomEffect.asBool();

	const Json::Value& PresentationInterval = Root["PresentationInterval"];
	if ( PresentationInterval.isInt() )
		uiPresentationInterval = PresentationInterval.asInt();
	free( acBuffer );
}

void CClientState::SaveLogin()
{
    Json::Value Root;

    NiFile* pkFile = NiFile::GetFile(LOGIN_FILE, NiFile::READ_ONLY);
    if(pkFile && *pkFile)
    {
        char* acBuffer = (char*)malloc( 64 * 1024 );

        unsigned int uiSize = pkFile->Read(acBuffer, 64 * 1024);
        NIASSERT(uiSize < 64*1024 - 1);
        acBuffer[uiSize] = '\0';

        Json::Reader Reader;
        if( !Reader.parse(acBuffer, Root) )
        {
            DASSERT(false && "parse client info error");
            Root.clear();
        }
		free( acBuffer );
    }

    NiDelete pkFile;

    Json::Value WindowSize;
    WindowSize[0u] = m_uiWidth;
    WindowSize[1] = m_uiHeight;
    Root["WindowSize"] = WindowSize;

    Root["FullScreen"] = m_bFullScreen;
    Root["WaterReflect"] = m_bWaterReflect;
    Root["BloomEffect"] = m_bEnableBloom;
    Root["LoginIP"] = (const char*)m_kLoginIP;
	Root["MultiSample"] = m_bMultiSample;
	Root["PresentationInterval"] = uiPresentationInterval;

    char buf[32];
    NiSprintf(buf, sizeof(buf), "%u", m_usLoginPort);
    Root["LoginPort"] = buf;



    Json::StyledWriter writer;
    std::string buffer = writer.write( Root );

    pkFile = NiFile::GetFile(LOGIN_FILE, NiFile::WRITE_ONLY);
    if(!pkFile || !(*pkFile))
    {
        NiDelete pkFile;
        return;
    }

    pkFile->Write(buffer.c_str(), buffer.size());
    NiDelete pkFile;
}

void CClientState::ParseCommandLine()
{
	NiCommand* pkCommand =  SYState()->ClientApp->GetCommand();

	//从命令行获取登录服务器的地址
	//可以是域名, 也可以是xxx.xxx.xxx.xxx.xxx
	char acBuf[128];
	if( pkCommand->String("ip", acBuf, sizeof(acBuf)) != 0 )
	{
		m_kLoginIP = acBuf;
	}
	
	//从命令行获取登录服务器的端口
	int iPort;
	if( pkCommand->Integer("port", iPort) != 0 )
	{
		m_usLoginPort = (unsigned short)iPort;
	}

	if( pkCommand->Boolean("bloom") )
	{
		m_bEnableBloom = true;
	}

    if( pkCommand->Boolean("check_file") )
    {
        m_bCheckFile = true;
    }
}

void CClientState::ClearGroup()
{
	m_GroupList.clear();
}

void CClientState::AddGroup(uint32 id, const std::string& strGroupName, uint32 status)
{
	GroupInfo kGroup;
	kGroup.strGroupName = strGroupName;
	kGroup.id = id;
	kGroup.status = status;

	m_GroupList.push_back(kGroup);
}

unsigned int CClientState::GetGroupCount() const
{
	return m_GroupList.size();
}

unsigned int CClientState::GetGroupID(const std::string& groupname) const
{
	for (int i = 0; i < m_GroupList.size(); i++)
	{
		if (m_GroupList[i].strGroupName == groupname)
		{
			return m_GroupList[i].id;
		}
	}

	return 0;
}
const GroupInfo* CClientState::GetGroup(unsigned int id) const
{
	if (id < m_GroupList.size())
		return &m_GroupList[id];		
	return NULL;
}
const GroupInfo* CClientState::GetGroupByID(unsigned int id) const
{
	for (int i = 0 ; i < m_GroupList.size(); i++)
	{
		if (m_GroupList[i].id == id )
			return &m_GroupList[i];
	}
	return NULL;
}


void CClientState::ClearRole()
{
	m_RoleList.clear();
}

void CClientState::AddRole(const RoleInfo& kRole)
{
	m_RoleList.push_back(kRole);
}

unsigned int CClientState::GetRoleCount()
{
	return m_RoleList.size();
}

RoleInfo* CClientState::GetRole(unsigned int id)
{
	if( id < m_RoleList.size() )
		return &m_RoleList[id];

	return NULL;
}

void CClientState::SetBloom(bool bEnable)
{
	m_bEnableBloom = bEnable;
}