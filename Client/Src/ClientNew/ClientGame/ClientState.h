#pragma once

struct GroupInfo
{
	std::string strGroupName;//utf8编码
	uint32 id;
	uint32 status;
};

typedef MSG_S2C::RoleInfo RoleInfo;

struct BloomParams
{
	float fThreshold;
	float fIntensity;
	float fBaseIntensity;
	float fSaturation;
	float fBaseSaturation;

	float fBlur;

	BloomParams()
		:fThreshold(0.25f)
		,fIntensity(1.25f)
		,fBaseIntensity(1.f)
		,fSaturation(1.f)
		,fBaseSaturation(1.f)
		,fBlur(4.f)
	{
	}
};

struct EnterGameInfo
{
	ui64 uiRoleID;
	unsigned int uiMapID;

	EnterGameInfo() : uiRoleID(-1), uiMapID(0)
	{
	}
};

class CClientState : public NiMemObject
{
private:
	NiFixedString  m_kLoginIP; //登录服务器地址
	unsigned short m_usLoginPort; //登录服务器端口

	unsigned int m_uiGateIP; //Gate服务器IP地址
	unsigned short m_usGatePort; //Gate服务器端口

	std::string m_strUserName;//用户名
	std::string m_strUserPass;//用户密码

	std::vector<GroupInfo> m_GroupList;//当前的服务器组列表

	unsigned int m_uiSessionID;
	unsigned int m_nAccountID;

	std::vector<RoleInfo> m_RoleList; //此账号的角色列表

	EnterGameInfo m_stEnterGameInfo;

	bool m_bEnableBloom;
	BloomParams m_kBloomParams;

	unsigned int m_uiWidth;
	unsigned int m_uiHeight;
	bool m_bFullScreen;
	unsigned int uiPresentationInterval;


	float m_fCameraNearDist;	// camera near plane
	float m_fCameraFarDist;		// camera far plane
	float m_fCameraFOV;			// camera fov

	bool m_bWaterReflect;
	bool m_bMultiSample;

    std::string m_strClientVersion;

    bool m_bCheckFile;

public:
	CClientState();
	virtual ~CClientState();

	void LoadConfig();
    void LoadClientInfo();
    void LoadLogin();
    void SaveLogin();
	void ParseCommandLine();

	inline const NiFixedString& GetLoginIP() const;
	inline unsigned short GetLoginPort() const;

	inline unsigned int GetGateIP() const;
	inline void SetGateIP(unsigned int uiIP);

	inline unsigned short GetGatePort() const;
	inline void SetGatePort(unsigned short usPort);

	inline const std::string& GetUserName() const;
	inline void SetUserName(const std::string& kUserName);

	inline const std::string& GetUserPass() const;
	inline void SetUserPass(const std::string& kUserPass);
    inline void ClearUserPass();

	inline const std::string& GetDesKey() const;
	inline void SetDesKey(const std::string& kDesKey);

	inline unsigned int GetSessionID() const;
	inline void SetSessionID(unsigned int uiSessionID);
	

	inline unsigned int GetAccountID() const;
	inline void SetAccountID(unsigned int uiAccountID);

	inline float GetCamNearDist() const;
	inline float GetCamFarDist() const;

	inline float GetCamFOV() const;

	inline void  SetEnterGameInfo(const EnterGameInfo& info);
	inline const EnterGameInfo& GetEnterGameInfo() const;

	void ClearGroup();
	void AddGroup(uint32 id, const std::string& strGroupName, uint32 status);
	unsigned int GetGroupCount() const;
	const GroupInfo* GetGroup(unsigned int id) const;
	const GroupInfo* GetGroupByID(unsigned int id) const;
	unsigned int GetGroupID(const std::string& groupname) const;

	void ClearRole();
	void AddRole(const RoleInfo& kRole);
	unsigned int GetRoleCount();
	RoleInfo* GetRole(unsigned int id);

	inline bool IsBloomEnabled() const;
	void SetBloom(bool bEnable);
	const BloomParams& GetBloomParams() const { return m_kBloomParams; }

	unsigned int GetWidth() const { return m_uiWidth; }
    void SetWidth(unsigned int uiWidth) { m_uiWidth = uiWidth; }
	unsigned int GetHeight() const { return m_uiHeight; }
    void SetHeight(unsigned int uiHeight) { m_uiHeight = uiHeight; }
	unsigned int GetPresentationInterval() const { return uiPresentationInterval; }
	void SetPresentationInterval(unsigned int uiValue);

	bool GetFullScreen() const { return m_bFullScreen; }
    void SetFullScreen(bool bFullScreen) { m_bFullScreen = bFullScreen; }

	bool GetWaterReflect() const { return m_bWaterReflect; }
    void SetWaterReflect(bool bEnable) { m_bWaterReflect = bEnable; }

	bool GetMultiSample() const { return m_bMultiSample; }
	void SetMultiSample(bool bEnable) { m_bMultiSample = bEnable; }

    const char* GetClientVersion() const { return m_strClientVersion.c_str(); }

    bool GetCheckFile() const { return m_bCheckFile; }
};

inline const NiFixedString& CClientState::GetLoginIP() const
{
	return m_kLoginIP;
}

inline unsigned short CClientState::GetLoginPort() const
{
	return m_usLoginPort;
}

inline unsigned int CClientState::GetGateIP() const
{
	return m_uiGateIP;
}

inline void CClientState::SetGateIP(unsigned int uiIP)
{
	m_uiGateIP = uiIP;
}

inline unsigned short CClientState::GetGatePort() const
{
	return m_usGatePort;
}

inline void CClientState::SetGatePort(unsigned short usPort)
{
	m_usGatePort = usPort;
}

inline const std::string& CClientState::GetUserName() const
{
	return m_strUserName;
}

inline void CClientState::SetUserName(const std::string& strUserName)
{
	m_strUserName = strUserName;
}

inline const std::string& CClientState::GetUserPass() const
{
	return m_strUserPass;
}

inline void CClientState::SetUserPass(const std::string& strUserPass)
{
	m_strUserPass = strUserPass;
}

inline void CClientState::ClearUserPass()
{
    //先清除内存当中保存的Password
    for(unsigned int i = 0; i < m_strUserPass.size(); ++i)
    {
        m_strUserPass[i] = ' ';
    }

    m_strUserPass.clear();
}

inline unsigned int CClientState::GetSessionID() const
{
	return m_uiSessionID;
}
inline void CClientState::SetSessionID(unsigned int uiSessionID)
{
	m_uiSessionID = uiSessionID;
}

inline unsigned int CClientState::GetAccountID() const
{
	return m_nAccountID;
}

inline void CClientState::SetAccountID(unsigned int uiAccountID)
{
	m_nAccountID = uiAccountID;
}

inline bool CClientState::IsBloomEnabled() const
{
	return m_bEnableBloom;
}

inline float CClientState::GetCamNearDist() const
{
	return m_fCameraNearDist;
}

inline float CClientState::GetCamFarDist() const
{
	return m_fCameraFarDist;
}

inline float CClientState::GetCamFOV() const
{
	return m_fCameraFOV;
}

inline void CClientState::SetEnterGameInfo(const EnterGameInfo& info)
{
	m_stEnterGameInfo = info;
}

inline const EnterGameInfo& CClientState::GetEnterGameInfo() const
{
	return m_stEnterGameInfo;
}

