#ifndef CONSOLE_H
#define CONSOLE_H

#define MAX_CMDS 10

#define RegistConsoleFun(p)   static ConsoleFunc  _##p(#p, p)

typedef void (*ConsoleCallBack)(std::vector<std::string>&);

struct ConsoleFunc
{
	ConsoleFunc(const char* pName, ConsoleCallBack pCB);
	ConsoleCallBack pcb;
	const char* pszName;
	ConsoleFunc* pNext;
};

class Console
{
	bool m_bConsoleEnabled;

	HANDLE stdOut;
	HANDLE stdIn;
	HANDLE stdErr;

	char m_Inbuf[512];
	signed int m_InPos;
	//char rgCmds[MAX_CMDS][512];
	//signed int  m_siCmdIndex;

	std::map<const std::string, ConsoleCallBack> m_FunMap;

public:
	Console();
	~Console();

	void Printf(int id, const char*s, ...);
	void Printf(const char *s, ...);

	void Process();
	void Enable(bool);
	void ProcessConsoleLine(const char *consoleLine);
	void InsertFunc(ConsoleFunc* pFunc);
	static void Create();
	static void Destroy();
	static bool IsEnabled();

	static bool m_bPrint;
};

extern Console* g_pkConsole;

#endif
