#pragma once

#include "Character.h"
#include "Effect/SceneEffect.h"


class CCreature : public CCharacter
{
public:
	CCreature(void);
	virtual ~CCreature(void);
	virtual EGAMEOBJ_TYPE GetGameObjectType() const { return GOT_CREATURE; }

	virtual AnimationID			GetNormalAttackAnimation(UINT uSerial);
	// test
	BOOL Create(const char* KFMFile, const char* Name);

	virtual void OnCreate(class ByteBuffer* data, ui8 update_flags);
    virtual void OnActorLoaded(NiActorManager* pkActorManager);
	virtual void OnValueChanged(class UpdateMask* mask);
	virtual void Update(float dt);

	virtual void SetIdleAnimation();
	virtual void SetWalkAnimation();
	virtual void SetRunAnimation();
	virtual void SetDialogAnimation();

	enum eQuestFlag
	{
		eNone = 0,
		eStart,
		eStartN,
		eFinish,
		eFinishN
	};

	bool GetBaseMoveSpeed(float& value) const;
	BOOL SetMoveAnimation(UINT Flags, BOOL bForceStart = TRUE);
	void MoveToPosition(const NiPoint3& kLocation, const NiPoint3& kTarget, const uint32& between, ECHAR_MOVE_STATE eMoveState);

	NiPoint3 GetMoveTarget(){ return m_kMoveTarget; }

	void SetQuestFlag(eQuestFlag eFlag);
	void OnQuestStatus(uint32 status);
	void QueryQuestStatus();
	eQuestFlag GetQuestFlag(){return m_QuestFlag;}
	bool UpdateMoveTransform(float fTime, float fTimeDelta, float& fT);


	void SetRecordLastFacing();  //记录最后一次的朝向
	void SetFacingLast();        //返回记录的最后的朝向

	virtual bool GetPickBound(NiBound& kBound);
	virtual bool IsPick(NiPick& kPicker,const NiPoint3& kOrig,const NiPoint3& kDir);
	virtual void StopMove();

	virtual BOOL IsLootAble();
	inline bool IsPet() const{return m_bPet;}
	virtual IStateProxy* GetStateProxy(State StateID);


protected:
	void AddEffect(UINT flag);
	void DelEffect(UINT flag);
	//特效
	CSceneEffect* m_pkMonsterEffect[2];
	
	void GetNPCFlagOBJ(NiPoint3 pkPos);

protected:
	NiNodePtr m_spQuestStart;
	NiNodePtr m_spQuestStartN;
	NiNodePtr m_spQuestFinish;
	NiNodePtr m_spQuestFinishN;

	NiNodePtr m_NPCFlagOBJ ;
	//NiNodePtr m_spQuest;
	bool m_bQuestNpc;
	float m_Lastfaceing; //NPC朝向记录。 用来和NPC对话后，返回到原来的朝向
	//
	NiPoint3 m_kMoveTarget;
	float m_moveSpeed;

	NiAVObject* m_pkLastCollObject;
	float m_fLastSafeZ;

	eQuestFlag m_QuestFlag;
	int m_Queststate;
	bool m_bPet;

	//ALAudioSourcePtr m_spVoice;

};
