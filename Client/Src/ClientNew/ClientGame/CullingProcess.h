#ifndef CULLINGPROCESS_H
#define CULLINGPROCESS_H
#include "Map/Box.h"
#include <hash_map>

class CWater;
class CChunk;
class CShadowGeometry;

typedef stdext::hash_multimap<ui32, NiVisibleArray*> VisibleArryHash;
typedef VisibleArryHash::iterator	VisibleArryHash_It;



class CCullingProcess : public NiCullingProcess
{
public:
	enum
	{
		CullTerMesh = 0,
		CullTerObject,
		CullGameObj,
		CullSceneObj,
		CullOtherObj,
		CullMax
	};

private:
	NiVisibleArray	m_kVisible;
	NiVisibleArray	m_arrVisible[CullMax];

	NiVisibleArray*	m_pkNormalVisible;

    NiVisibleArray m_kWaterArray; //可见的水面

	ui32  m_uiCurCullLayer;
    bool m_bHasRRWater; //是否有需要反射的水面可见
    float m_fRRWaterHeight; //需要反射的水面高度
    NiPoint3 m_kRRWaterBase;

    NiVisibleArray m_kReflectChunks; //需要反射的地形
    NiVisibleArray m_kReflectModels; //需要反射的模型

    NiCameraPtr m_spReflectCamera; //反射Camera
    NiFrustumPlanes m_kReflectPlanes; //反射Camera组成的Frustum Planes

	NiTPrimitiveArray<CShadowGeometry*> m_kShadowGeometrys;

    //用来排序水面
    float* m_pfDepths;
    unsigned int m_uiAllocatedDepths;

	bool	m_bIsCulled;

protected:
	virtual void AppendVirtual(NiGeometry& kVisible);

public:
	unsigned int DrawCullObj(NiRenderer* pkRender, ui32 eLayer);

public:
	CCullingProcess();
	virtual ~CCullingProcess();

	void Reset(NiCamera* pkCamera);
	bool ProcessObject(NiAVObject* pkObject, ui32 uiLayer, NiVisibleArray* pVisible = NULL);
	void RemoveAll();
	void Remove(ui32 eLayer);
	unsigned int GetVisibleCount(ui32 uiLayer) const { return m_arrVisible[uiLayer].GetCount(); }
	NiVisibleArray* GetVisible(ui32 uiLayer) { return &m_arrVisible[uiLayer]; }

	void OnWaterVisible(CWater* pkWater);
    NiVisibleArray* GetWaterSet() { return &m_kWaterArray; }
    bool HasRRWater() const { return m_bHasRRWater; }
    float GetRRWaterHeight() const { return m_fRRWaterHeight; }
    const NiPoint3& GetRRWaterBase() const { return m_kRRWaterBase; }
	void SortWater();
	void SortObjectsByDepth(NiVisibleArray& kArrayToSort, int l, int r);
	float ChoosePivot(int l, int r) const;

	NiVisibleArray* GetReflectModelSet() { return &m_kReflectModels; }
	NiVisibleArray* GetReflectChunkSet() { return &m_kReflectChunks; }

	NiCamera* GetCamera() { return (NiCamera*)m_pkCamera; }
	NiCamera* GetReflectCamera() { return m_spReflectCamera; }
	void UpdateReflectCamera();
	const NiFrustumPlanes& GetReflectFrustumPlanes() const { return m_kReflectPlanes; }

	void OnReflectChunkVisible(CChunk* pkChunk);
	void CullReflectModel(NiAVObject* pkSceneRoot);

	void BuildReflectVisibleSet();

	void OnDynmicShadowVisible(CShadowGeometry* pkShadowGeometry);
	NiTPrimitiveArray<CShadowGeometry*>& GetShadowGeometrys() { return m_kShadowGeometrys; }
};

NiSmartPointer(CCullingProcess);

#endif