#include "StdAfx.h"
#include "DecalModel.h"

#include "Map/Map.h"

DecalModel::DecalModel() : m_usMaxVertCount(0)
{
	Initialize(512);
}

DecalModel::~DecalModel()
{
}

BOOL DecalModel::Initialize(unsigned short usMaxVertexCount)
{
	m_usMaxVertCount = usMaxVertexCount;

	NiPoint3* pkVertices = NiNew NiPoint3[usMaxVertexCount];
	NIASSERT(pkVertices);
    NiPoint2* pkTexC = NiNew NiPoint2[usMaxVertexCount];
    NIASSERT(pkTexC);

	unsigned short usStrips = 1;
	unsigned short usTriangles = usMaxVertexCount - 2;
	m_pusStripLengths = NiAlloc(unsigned short, usStrips);
	m_pusStripLengths[0] = usMaxVertexCount;
	NIASSERT(m_pusStripLengths);
	m_pusStripLists = NiAlloc(unsigned short, usMaxVertexCount);
	NIASSERT(m_pusStripLists);

	unsigned short usIndex = 0;
	for (unsigned short us = 0; us < usTriangles; us++)
	{
		// even : i, i+1, i+2
		// odd : i+1, i, i+2
		if (us&1) {
			m_pusStripLists[us] = us;
			m_pusStripLists[us+1] = us+1;
			m_pusStripLists[us+2] = us+2;
		}
		else {
			m_pusStripLists[us] = us+1;
			m_pusStripLists[us+1] = us;
			m_pusStripLists[us+2] = us+2;
		}
	}

	m_spStripsData = NiNew NiTriStripsData(usMaxVertexCount, pkVertices, 0, 0, pkTexC, 1,
		NiGeometryData::NBT_METHOD_NONE, usTriangles, 1, m_pusStripLengths, m_pusStripLists);
	NIASSERT(m_spStripsData);

	m_spGeometry = NiNew NiTriStrips(m_spStripsData);
	if (!m_spGeometry)
		return FALSE;

	m_spGeometry->SetActiveVertexCount(0);
	m_spGeometry->SetActiveTriangleCount(0);

    NiZBufferProperty* pkZ = NiNew NiZBufferProperty;
    NIASSERT(pkZ);
    pkZ->SetZBufferTest(true);
    pkZ->SetZBufferWrite(false);
    m_spGeometry->AttachProperty(pkZ);

	NiAlphaProperty* pAlpha = NiNew NiAlphaProperty;
	NIASSERT(pAlpha);
    pAlpha->SetAlphaBlending(true);
    pAlpha->SetSrcBlendMode(NiAlphaProperty::ALPHA_ZERO);
    pAlpha->SetDestBlendMode(NiAlphaProperty::ALPHA_SRCCOLOR);

	m_spGeometry->AttachProperty(pAlpha);

//	NiTexturingProperty* pkTexture = NiNew NiTexturingProperty;
//	pkTexture->SetBaseTexture(ResMgr::CacheTexture("Texture\\footstep.tga"));

	m_spGeometry->UpdateProperties();

	return TRUE;
}

void DecalModel::Update(float fTimeLine)
{
	_asm nop
}

void DecalModel::BuildGeom(unsigned short usVertexCount, NiPoint3* pkVertex)
{
	if (usVertexCount > m_usMaxVertCount)
		usVertexCount = m_usMaxVertCount;

	NiPoint3* pkVertices = m_spGeometry->GetVertices();
	NIASSERT(pkVertices);

	m_pusStripLengths[0] = usVertexCount;
	m_spStripsData->Replace(1, m_pusStripLengths, m_pusStripLists);

	for (unsigned short us = 0; us < usVertexCount; us++)
	{
		pkVertices[us] = pkVertex[us];
	}

	m_spGeometry->SetActiveVertexCount(usVertexCount);
	m_spGeometry->SetActiveTriangleCount(usVertexCount - 2);
}

void DecalModel::Render()
{
	NiRenderer* pkRenderer = NiRenderer::GetRenderer();
	m_spGeometry->RenderImmediate(pkRenderer);
}

