#ifndef _DECAL_MODEL_
#define _DECAL_MODEL_

#include "Effect/DecalManager.h"

class DecalModel : public DecalBase
{
public:
	DecalModel();
	~DecalModel();

	BOOL Initialize(unsigned short usMaxVertexCount);

	enum
	{
		MAX_VERTEX = 4096,
	};

	void Update(float fTimeLine);
	void Render();

	void BuildGeom(unsigned short usVertexCount, NiPoint3* pkVertex);

private:
	NiTriStripsPtr m_spGeometry;
	NiTriStripsDataPtr m_spStripsData;
	unsigned short* m_pusStripLengths;
	unsigned short* m_pusStripLists;

	unsigned short m_usMaxVertCount;
};

#endif //_DECAL_MODEL_