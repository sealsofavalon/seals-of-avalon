#pragma once
#ifndef _DECAL_MODEL_RENDERER_
#define _DECAL_MODEL_RENDERER_

#include "ModelAdditionRenderer.h"

class CEffectBase;
typedef NiTPointerMap<CEffectBase*, CEffectBase*> DECALMODELMAP;

class DecalModelRenderer : public ModelRendererInterface
{
public:
	DecalModelRenderer();
	~DecalModelRenderer();

	void Render(int iZoneX, int iZoneY, NiAVObject* pkModel);

	void AddDecal(CEffectBase* );
	bool RemoveDecal(CEffectBase*);

	void Clear();

protected:
	bool BuildDecalPolygon(NiAVObject* pkModel, CEffectBase* pkDecal, int iModelZoneX, int iModelZoneY);

private:
	DECALMODELMAP m_DecalMap;


};


#endif //_DECAL_MODEL_RENDERER_
