#pragma once
#ifndef _DECAL_MODEL_RENDERER_
#define _DECAL_MODEL_RENDERER_

#include "ModelAdditionRenderer.h"

class DecalModel;
typedef NiTPointerMap<DecalModel*, DecalModel*> DECALMODELMAP;

class DecalModel;
class DecalModelRenderer : public ModelRendererInterface
{
public:
	DecalModelRenderer();
	~DecalModelRenderer();

	void Render(int iZoneX, int iZoneY, NiAVObject* pkModel, unsigned int frameID);

	void AddDecal(DecalModel* );
	void RemoveDecal(DecalModel*);

	unsigned int ClipGeometry(unsigned int uiVertexCount, const NiPoint3* pkVert, 
												const NiTransform& kWorldTransform, 
											  const NiBound& kBound, 
											  NiPoint3* pkResult);

	void Clear();

protected:
	unsigned int DecalModelRenderer::ClipPolygonAgainstPlane(const NiPlane& kPlane, unsigned int uiVertexCount, 
		const NiPoint3* pkVert, const NiTransform& kWorldTransform, NiPoint3* pkResult,
		bool bTransform);

protected:
	bool BuildDecal(NiAVObject* pkModel, DecalModel* pkDecal, int iModelZoneX, int iModelZoneY);

private:
	DECALMODELMAP m_DecalMap;
	unsigned char* m_pucLocation;

};


#endif //_DECAL_MODEL_RENDERER_
