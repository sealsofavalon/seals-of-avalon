#include "StdAfx.h"
#include "DpsCount.h"
#include "Utils/ClientUtils.h"
#include "ObjectManager.h"
#include "ui/UFun.h"


CDpsCount::CDpsCount()
{
	m_iLastTickTime = 0;
}

CDpsCount::~CDpsCount()
{

}


void CDpsCount::GetClientTime( uint64& time )
{
	time = float2int32(NiGetCurrentTimeInSec() * 1000.0f);
}

void CDpsCount::AddDamegeData( uint64 playerGuid, uint64 SpellId, uint64 realDamage )
{
	if ( m_mapInfo.find( playerGuid ) == m_mapInfo.end() )
	{
		DamageInfo dpsInfo;
		dpsInfo.SpellDataAll[SpellId] += realDamage;
		dpsInfo.SpellDataThis[SpellId] += realDamage;
		m_mapInfo[playerGuid] = dpsInfo;
	}
	else
	{
		DamageInfo& dmgInfo = m_mapInfo[playerGuid];
		if ( dmgInfo.bInAnalyst )
		{
			dmgInfo.SpellDataThis[SpellId] += realDamage;
		}
		dmgInfo.SpellDataAll[SpellId] += realDamage;
	}
}


void CDpsCount::AddHealData( uint64 playerGuid, uint64 SpellId, uint64 realHeal )
{
	if ( m_mapInfo.find( playerGuid ) == m_mapInfo.end() )
	{
		DamageInfo dpsInfo;
		dpsInfo.HealDataAll[SpellId] += realHeal;
		dpsInfo.HealDataThis[SpellId] += realHeal;
		m_mapInfo[playerGuid] = dpsInfo;
	}
	else
	{
		DamageInfo& dmgInfo = m_mapInfo[playerGuid];
		if ( dmgInfo.bInAnalyst )
		{
			dmgInfo.HealDataThis[SpellId] += realHeal;
		}
		dmgInfo.HealDataAll[SpellId] += realHeal;
	}
}

void CDpsCount::OnStartAnalyst( uint64 guid )
{
	if ( m_mapInfo.find( guid ) != m_mapInfo.end() )
	{
		DamageInfo& dmgInfo = m_mapInfo[guid];
		dmgInfo.attackTimeAllThis = 0;
		dmgInfo.SpellDataThis.clear();
		dmgInfo.HealDataThis.clear();
		dmgInfo.bInAnalyst = true;
		GetClientTime( dmgInfo.startTime );
	}
	else
	{
		DamageInfo dmginfo;
		dmginfo.bInAnalyst = true;
		GetClientTime( dmginfo.startTime );
		m_mapInfo[guid] = dmginfo;
		
	}
	
}


void CDpsCount::OnEndAnalyst( uint64 guid )
{
	if ( m_mapInfo.find( guid ) != m_mapInfo.end() )
	{
		DamageInfo& dmgInfo = m_mapInfo[guid];
		dmgInfo.bInAnalyst = false;
	}
}

void CDpsCount::ResetAll()
{
	for ( MAPDPSINFO::iterator itInfo = m_mapInfo.begin(); itInfo != m_mapInfo.end(); ++itInfo )
	{

		DamageInfo& dmgInfo = itInfo->second;
		dmgInfo.HealDataAll.clear();
		dmgInfo.HealDataThis.clear();
		dmgInfo.SpellDataAll.clear();
		dmgInfo.SpellDataThis.clear();
		dmgInfo.attackTimeAll = 0;
		dmgInfo.attackTimeAllThis = 0;
		if ( dmgInfo.bInAnalyst )
		{
			GetClientTime( dmgInfo.startTime );
		}
		

	}			
}


void CDpsCount::UpdateDps()
{
	if ( m_iLastTickTime == 0 )
	{
		GetClientTime( m_iLastTickTime );
	}

	uint64 curTime;
	GetClientTime( curTime );

	uint64 tickTime = curTime - m_iLastTickTime;

	if ( tickTime < 200 )
	{
		return;
	}

	for ( MAPDPSINFO::iterator itInfo = m_mapInfo.begin(); itInfo != m_mapInfo.end(); ++itInfo )
	{
		
		DamageInfo& dmgInfo = itInfo->second;
		if ( dmgInfo.bInAnalyst )
		{
			dmgInfo.attackTimeAllThis = curTime - dmgInfo.startTime;
			dmgInfo.attackTimeAll += tickTime;
		}

		uint64 guid = itInfo->first;

		/*if ( ObjectMgr->GetLocalPlayer()->GetGUID() == guid  )
		{
			uint64 damage = 0;
			uint64 timeThis = 0;
			float dps = 0.0f;
			damage = GetDamageThis( guid );
			timeThis = GetAttackTimeThis( guid );
			float time = timeThis/1000;

			
			if ( time < 1.0f )
			{
				time = 1.0f;
			}
			dps = (float)damage / time;
			
			char sztest[64] = {0};
			sprintf( sztest, "dpsThis: %f " , dps );
				ClientSystemNotify(sztest);
		}*/
	}

	m_iLastTickTime = curTime;
}


float CDpsCount::GetDps( uint64 guid )
{
	uint64 damage = 0;
	uint64 timeThis = 0;
	float dps = 0.0f;
	damage = GetDamageThis( guid );
	timeThis = GetAttackTimeThis( guid );
	float time = timeThis/1000;


	if ( time < 1.0f )
	{
		time = 1.0f;
	}
	dps = (float)damage / time;

	return dps;
}


float CDpsCount::GetHps( uint64 guid )
{
	uint64 damage = 0;
	uint64 timeThis = 0;
	float dps = 0.0f;
	damage = GetHealThis( guid );
	timeThis = GetAttackTimeThis( guid );
	float time = timeThis/1000;


	if ( time < 1.0f )
	{
		time = 1.0f;
	}
	dps = (float)damage / time;

	return dps;
}



MAPSPELLDPS* CDpsCount::GetSpellDamageThis( uint64 guid )
{
	if ( m_mapInfo.find(guid) != m_mapInfo.end() )
	{
		return &m_mapInfo[guid].SpellDataThis;
	}

	return NULL;
}

MAPSPELLDPS* CDpsCount::GetSpellDamageAll( uint64 guid )
{
	if ( m_mapInfo.find(guid) != m_mapInfo.end() )
	{
		return &m_mapInfo[guid].SpellDataAll;
	}

	return NULL;
}


MAPSPELLDPS* CDpsCount::GetSpellHealThis( uint64 guid )
{
	if ( m_mapInfo.find(guid) != m_mapInfo.end() )
	{
		return &m_mapInfo[guid].HealDataThis;
	}

	return NULL;
}

MAPSPELLDPS* CDpsCount::GetSpellHealAll( uint64 guid )
{
	if ( m_mapInfo.find(guid) != m_mapInfo.end() )
	{
		return &m_mapInfo[guid].HealDataAll;
	}

	return NULL;
}


uint64 CDpsCount::GetDamageThis( uint64 guid )
{
	uint64 uDamageThis =0;
	if ( m_mapInfo.find( guid ) != m_mapInfo.end())
	{
		MAPSPELLDPS* pSpellInfo =  GetSpellDamageThis( guid );
		for ( MAPSPELLDPS::iterator itSpell = pSpellInfo->begin(); itSpell != pSpellInfo->end(); ++itSpell )
		{
			uDamageThis += itSpell->second;
		}

		return uDamageThis;
	}
	else
		return 0;
}




uint64 CDpsCount::GetDamageAll( uint64 guid )
{
	uint64 uDamageAll =0;
	if ( m_mapInfo.find( guid ) != m_mapInfo.end())
	{
		MAPSPELLDPS* pSpellInfo =  GetSpellDamageAll( guid );
		for ( MAPSPELLDPS::iterator itSpell = pSpellInfo->begin(); itSpell != pSpellInfo->end(); ++itSpell )
		{
			uDamageAll += itSpell->second;
		}

		return uDamageAll;
	}
	else
		return 0;
}


uint64 CDpsCount::GetHealThis( uint64 guid )
{
	uint64 uDamageThis =0;
	if ( m_mapInfo.find( guid ) != m_mapInfo.end())
	{
		MAPSPELLDPS* pSpellInfo =  GetSpellHealThis( guid );
		for ( MAPSPELLDPS::iterator itSpell = pSpellInfo->begin(); itSpell != pSpellInfo->end(); ++itSpell )
		{
			uDamageThis += itSpell->second;
		}

		return uDamageThis;
	}
	else
		return 0;
}


uint64 CDpsCount::GetHealeAll( uint64 guid )
{
	uint64 uDamageThis =0;
	if ( m_mapInfo.find( guid ) != m_mapInfo.end())
	{
		MAPSPELLDPS* pSpellInfo =  GetSpellHealAll( guid );
		for ( MAPSPELLDPS::iterator itSpell = pSpellInfo->begin(); itSpell != pSpellInfo->end(); ++itSpell )
		{
			uDamageThis += itSpell->second;
		}

		return uDamageThis;
	}
	else
		return 0;
}


uint64 CDpsCount::GetAttackTimeThis( uint64 guid )
{
	uint64 timeThis = 0;
	if ( m_mapInfo.find( guid ) != m_mapInfo.end())
	{
		timeThis = m_mapInfo[guid].attackTimeAllThis;
	}

	return timeThis;
}


uint64 CDpsCount::GetAttackTimeAll( uint64 guid )
{
	uint64 timeAll = 0;
	if ( m_mapInfo.find( guid ) != m_mapInfo.end())
	{
		timeAll = m_mapInfo[guid].attackTimeAll;
	}
	return timeAll;
}