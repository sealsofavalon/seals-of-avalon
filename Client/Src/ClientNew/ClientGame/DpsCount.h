#pragma once

typedef std::map< uint64, uint64 > MAPSPELLDPS;

struct DamageInfo
{
	MAPSPELLDPS SpellDataAll;
	MAPSPELLDPS SpellDataThis;

	MAPSPELLDPS HealDataAll;
	MAPSPELLDPS HealDataThis;

	uint64 attackTimeAll;
	uint64 attackTimeAllThis;

	bool	bInAnalyst;
	uint64  startTime;


	DamageInfo()
	{
		SpellDataAll.clear();
		SpellDataThis.clear();
		attackTimeAll = 0;
		attackTimeAllThis = 0;
		bInAnalyst = false;
		startTime = 0;
	}
};

typedef std::map< uint64, DamageInfo >  MAPDPSINFO;

class CDpsCount
{
public:
	CDpsCount();
	~CDpsCount(void);

	void AddDamegeData( uint64 playerGuid, uint64 SpellId, uint64 realDamage );

	void AddHealData( uint64 playerGuid, uint64 SpellId, uint64 realHeal );

	void OnStartAnalyst( uint64 guid );

	void OnEndAnalyst( uint64 guid );

	void ResetAll();

	void UpdateDps();

	void GetClientTime( uint64& time );

	float GetDps( uint64 guid );
	float GetHps( uint64 guid );

	uint64 GetDamageThis( uint64 guid );
	uint64 GetDamageAll( uint64 guid );

	uint64 GetHealThis( uint64 guid );
	uint64 GetHealeAll( uint64 guid );

	MAPSPELLDPS* GetSpellDamageThis( uint64 guid );
	MAPSPELLDPS* GetSpellDamageAll( uint64 guid );

	MAPSPELLDPS* GetSpellHealThis( uint64 guid );
	MAPSPELLDPS* GetSpellHealAll( uint64 guid );


	uint64 GetAttackTimeThis( uint64 guid );
	uint64 GetAttackTimeAll( uint64 guid );
	

private:
	MAPDPSINFO m_mapInfo;
	uint64     m_iLastTickTime;
};