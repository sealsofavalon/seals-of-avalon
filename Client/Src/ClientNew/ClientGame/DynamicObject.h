#ifndef __DYNAMICOBJECT_H__
#define __DYNAMICOBJECT_H__
#include "Character.h"

enum
{
	NORMALMove = 0,
	WalkMove = 1,
	RunMove = 2,
};
class CDynamicObj : public CAnimObject
{
public:
	CDynamicObj();
	virtual ~CDynamicObj();
	virtual EGAMEOBJ_TYPE GetGameObjectType() const {return GOT_DYNAMICOBJECT;}
	
	virtual void Update(float dt);
	virtual void OnCreate(class ByteBuffer* data, ui8 update_flags);
	virtual void OnValueChanged(class UpdateMask* mask);
	void MoveToPosition(const NiPoint3& kLocation, const NiPoint3& kTarget, const uint32& between);
	void SetMoveState(ui8 state){m_MoveState = state;}
	void StopMove();
	void SetDirectionTo(NiPoint3 kpos);
	void SetDesiredAngle(float fz);
protected:
	void UpdateMoveTransform(float fTimeDelta);
	virtual bool Draw(CCullingProcess* pkCuller);
protected:
	NiPoint3 m_kMoveTarget;
	ui8 m_MoveState;
	float m_fFaceingAngle;

};

#endif