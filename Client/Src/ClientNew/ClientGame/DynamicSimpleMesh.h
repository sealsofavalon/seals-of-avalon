#pragma once

#include "NiTriShapeData.h"

class DynamicSimpleMeshData : public NiTriShapeData
{
public:
	DynamicSimpleMeshData(bool bWantNormals, bool bWantColors,
        unsigned short usNumTextureSets, int iMaxPQuantity = 1,
        int iPGrowBy = 1, int iMaxVQuantity = 4, int iVGrowBy = 1,
        int iMaxTQuantity = 2, int iTGrowBy = 1);

	~DynamicSimpleMeshData();

	unsigned short* GetIndices(int iPolygon);
    const unsigned short* GetIndices(int iPolygon) const;

	int Insert(unsigned short usNumVertices, 
		unsigned short usNumTriangles=0, const unsigned short* ausTriList=0);

    bool Remove(int iPolygon);
    void RemoveAll();

	void UpdateBound();

	bool SetVertices(int iPolygon, const NiPoint3* akValue);
    bool GetVertices(int iPolygon, NiPoint3* akValue) const;

	bool SetVertex(int iPolygon, int iVert, const NiPoint3& kValue);
	bool GetVertex(int iPolygon, int iVert, NiPoint3& kValue) const;

	bool GetColors(int iPolygon, NiColorA* akValue) const;
	bool SetColors(int iPolygon, const NiColorA& kCommonValue);
	bool SetColors(int iPolygon, const NiColorA* akValue);
	bool GetColor(int iPolygon, int iVertex, NiColorA& kValue) const;
	bool SetColor(int iPolygon, int iVertex, const NiColorA& kValue);

	bool SetTextures(int iPolygon, unsigned short usSet,
		   const NiPoint2* akValue);
	bool SetTextures(int iPolygon, unsigned short usSet, int iVert, 
		const NiPoint2& kValue);

	bool IsValid(int iPolygon) const;

    int GetMaxPQuantity() const;
    void SetPGrowBy(int iPGrowBy);
    int GetPGrowBy() const;
    int GetMaxVQuantity() const;
    void SetVGrowBy(int iVGrowBy);
    int GetVGrowBy() const;
    int GetMaxTQuantity() const;
    void SetTGrowBy(int iTGrowBy);
    int GetTGrowBy() const;

    int GetNumTriangles(int iPolygon) const;
    int GetNumVertices(int iPolygon) const;

    int GetNumPolygons() const;
	bool IsBoundNeedsUpdate() const;
private:
    // Polygon array management.  The active number of polygons is
    // m_usPQuantity.
    class Polygon : public NiMemObject
    {
    public:
        unsigned short m_usNumVertices;
        unsigned short m_usVOffset;  // offset into the vertex arrays
        unsigned short m_usNumTriangles;
        unsigned short m_usIOffset;  // offset into the index array

		// add to scene graph, culling actor
    };

    // Get a polygon from a handle.  The input handle must be valid.
    Polygon& GetPolygon(int iPolygon);
    const Polygon& GetPolygon(int iPolygon) const;

    Polygon* m_akPolygon;
    unsigned short* m_ausPIndexer;
    unsigned short m_usMaxPQuantity, m_usPGrowBy, m_usPQuantity;

    // Vertex array management.  The active number of vertices is
    // m_usVertices, including those not used due to removed polygons.
    unsigned short m_usMaxVQuantity, m_usVGrowBy;

    // Index array management.  The active number of indices is
    // 3*m_usTriangles.
    unsigned short m_usMaxIQuantity, m_usIGrowBy;

	bool m_bBoundNeedsUpdate;

	static const unsigned short ms_usInvalid;
};

inline int DynamicSimpleMeshData::GetMaxPQuantity() const
{
    return (int)m_usMaxPQuantity;
}

inline void DynamicSimpleMeshData::SetPGrowBy(int iPGrowBy)
{
    m_usPGrowBy = (unsigned short)(iPGrowBy > 0 ? iPGrowBy : 1);
}

inline int DynamicSimpleMeshData::GetPGrowBy() const
{
    return (int)m_usPGrowBy;
}

inline int DynamicSimpleMeshData::GetMaxVQuantity() const
{
    return (int)m_usMaxVQuantity;
}

inline void DynamicSimpleMeshData::SetVGrowBy(int iVGrowBy)
{
    m_usVGrowBy = (unsigned short)(iVGrowBy > 0 ? iVGrowBy : 1);
}

inline int DynamicSimpleMeshData::GetVGrowBy() const
{
    return (int)m_usVGrowBy;
}

inline int DynamicSimpleMeshData::GetMaxTQuantity() const
{
    return (int)m_usMaxIQuantity/3;
}

inline void DynamicSimpleMeshData::SetTGrowBy(int iTGrowBy)
{
    m_usIGrowBy = (unsigned short)(iTGrowBy > 0 ? 3 * iTGrowBy : 3);
}

inline int DynamicSimpleMeshData::GetTGrowBy() const
{
    return (int)m_usIGrowBy/3;
}

inline int DynamicSimpleMeshData::GetNumPolygons() const
{
    return (int)m_usPQuantity;
}

inline bool DynamicSimpleMeshData::IsBoundNeedsUpdate() const
{
	return m_bBoundNeedsUpdate;
}

inline DynamicSimpleMeshData::Polygon& DynamicSimpleMeshData::GetPolygon(
    int iPolygon)
{
    return m_akPolygon[m_ausPIndexer[iPolygon]];
}

inline const DynamicSimpleMeshData::Polygon& DynamicSimpleMeshData::GetPolygon(
    int iPolygon) const
{
    return m_akPolygon[m_ausPIndexer[iPolygon]];
}


class DynamicSimpleMesh : public NiTriShape
{
public:
	DynamicSimpleMesh();
	DynamicSimpleMesh(DynamicSimpleMeshData* pkModelData);
	~DynamicSimpleMesh();

	int Insert(unsigned short usNumVertices, 
		unsigned short usNumTriangles=0, const unsigned short* ausTriList = NULL);

    bool Remove(int iPolygon);
    void RemoveAll();

	bool SetVertices(int iPolygon, const NiPoint3* akValue);
	bool GetVertices(int iPolygon, NiPoint3* akValue) const;

	bool SetVertex(int iPolygon, int iVert, const NiPoint3& kValue);
	bool GetVertex(int iPolygon, int iVert, NiPoint3& kValue) const;

	bool GetColors(int iPolygon, NiColorA* akValue) const;
	bool SetColors(int iPolygon, const NiColorA& kCommonValue);
	bool SetColors(int iPolygon, const NiColorA* akValue);
	bool GetColor(int iPolygon, int iVertex, NiColorA& kValue) const;
	bool SetColor(int iPolygon, int iVertex, const NiColorA& kValue);

	bool SetTextures(int iPolygon, unsigned short usSet,
		   const NiPoint2* akValue);
	bool SetTextures(int iPolygon, unsigned short usSet, int iVert, 
		const NiPoint2& kValue);

	void Draw(NiRenderer* pkRenderer);
	void RenderImmediate(NiRenderer* pkRenderer);

	void UpdateWorldBound();

	DynamicSimpleMeshData* GetData();
	const DynamicSimpleMeshData* GetData() const;

};

typedef NiPointer<DynamicSimpleMesh> DynamicSimpleMeshPtr;