#pragma once
#include "EffectBase.h"
#include "GameObject.h"
#include "AudioInterface.h"
// 处于场景中的, 参与遮挡, 排序 的特效, 如果有附加于一个指定对象, 则跟随对象运动.


class CChainEffect : public CEffectBase //, public CGameObject
{
	friend class CEffectManager;
public:
	CChainEffect();
	virtual ~CChainEffect(void);

	virtual BOOL SelfManage() { return FALSE;}
	virtual BOOL SelfRender() const { return TRUE; }

	virtual void UpdateEffect(float fTimeLine, float fDeltaTime);
	virtual void RenderEffect(const NiCamera* pkCamera);
	void Sample(float fTime);

	void BindObject(NiAVObject* pkSrcObj, NiAVObject* pkDestObj);
	void SetTexture(const char* szTexFile);
	void SetColor(NiColor& kColor);
	void SetWidth(float fWidth) { m_fWidth = fWidth; }

	void SetUpdateSpeed(float fTime) { m_fUpdateSpeed = fTime; }
	void SetActiveTime(float fTime) { m_fActiveTime = fTime; }
	void SetDelayTime(float fTime)	{ m_fDelayTime = fTime; }

protected:
	NiNodePtr m_spEffectNode;

	NiAVObjectPtr m_spBindObjSender;
	NiAVObjectPtr m_spBindObjReceive;

	NiTexturePtr  m_spTexture;
	float		  m_fWidth;

	float		  m_fUpdateSpeed;
	float		  m_fActiveTime;
	float		  m_fDelayTime;

	NiTriShapePtr m_pkTriShape;
};
