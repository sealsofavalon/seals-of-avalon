#include "StdAfx.h"
#include "CharacterName.h"
#include "NiDX9TextureData.h"
#include "Player.h"
#include "ClientApp.h"
#include "ObjectManager.h"
#include "UI\UISystem.h"

#define SMALL_FONT_SIZE 14
CNameDrawer::CNameDrawer()
{
	m_NameFont = NULL;

	m_CastleAttack = NULL;
	m_CastleDefense = NULL;
	m_HpProgressBar = NULL;
	m_GMFlagTexture = NULL;
	m_SmallFont = NULL;
	m_pURender = NULL;
	m_HpBarBkg = NULL;

	m_MaskSkin = NULL;

	for(int i = 0; i < 12 ; i++)
		m_zodiactexture[i] = NULL;	
}
CNameDrawer::~CNameDrawer()
{
	m_NameFont = NULL;
	m_pURender = NULL;
	for(int i = 0; i < 12 ; i++)
		m_zodiactexture[i] = NULL;	
	m_HpBarBkg = NULL;
	m_MaskSkin = NULL;
}
BOOL CNameDrawer::Create(const char* FontFace,INT FontSize)
{
	if (m_NameFont == NULL)
	{
		m_NameFont = UControl::sm_UiRender->CreateFont(FontFace,FontSize,134);
		if (!m_NameFont)
			return FALSE;
	}
	if(m_SmallFont == NULL)
	{
		m_SmallFont = UControl::sm_UiRender->CreateFont(FontFace, SMALL_FONT_SIZE, 134);
		if(!m_SmallFont)
			return FALSE;
	}
	if (m_HpBarBkg == NULL)
	{
		m_HpBarBkg = UControl::sm_UiRender->LoadTexture("DATA\\UI\\MouseTip\\HPBarBkg.png");
		if (!m_HpBarBkg)
			return FALSE;
	}
	if (m_HpProgressBar == NULL)
	{
		m_HpProgressBar = UControl::sm_UiRender->LoadTexture("DATA\\UI\\MouseTip\\HPBar1.png");
		if (!m_HpProgressBar)
			return FALSE;
	}
	if (m_GMFlagTexture == NULL)
	{
		m_GMFlagTexture = UControl::sm_UiRender->LoadTexture("DATA\\UI\\GM.png");
		if (!m_GMFlagTexture)
		{
			return FALSE;
		}
	}
	if(m_MaskSkin == NULL)
	{
		m_MaskSkin = UControl::sm_System->LoadSkin("UIPlayerShow\\CharMask.skin");
		if(!m_MaskSkin)
			return FALSE;
	}
	char buf[128];
	for (int i = 0 ; i < 12; i++)
	{
		if (m_zodiactexture[i]  == NULL){
			sprintf(buf, "DATA\\UI\\CharName\\zodiac_%d.png", i);
			m_zodiactexture[i] = UControl::sm_UiRender->LoadTexture(buf);
			if (!m_zodiactexture[i])
				return FALSE;
		}
	}
	if (m_CastleAttack == NULL)
	{
		m_CastleAttack = UControl::sm_UiRender->LoadTexture("Data\\UI\\CharName\\BlueTeam.png");
		if (!m_CastleAttack)
			return FALSE;
	}
	if (m_CastleDefense == NULL)
	{
		m_CastleDefense = UControl::sm_UiRender->LoadTexture("Data\\UI\\CharName\\RedTeam.png");
		if (!m_CastleDefense)
			return FALSE;
	}
	if(m_pURender == NULL)
	{
		USystem* pUSystem = SYState()->UI->GetUSystem();
		NIASSERT(pUSystem);
		m_pURender = pUSystem->GetPlugin();	
		if (!m_pURender)
			return FALSE;
	}
	return TRUE;
}

INT CNameDrawer::TextOut(INT X, INT Y, CharName &Entry)
{
	UFontPtr pfont;
	float scale;
	if(BeginText(Entry, pfont, scale))
	{
		int height = Y;

		//float scale = 1.0f;

		//从下向上绘制
		//绘制血条
		if(Entry.Maxhp && Entry.Scale <= 80.f)
			height -= DrawHPBar(Entry.ShowHp, Entry.ShowHpBarColorful, X, height, Entry.Position.z, float(Entry.hp) / float(Entry.Maxhp), Entry.Level, Entry.Class, m_SmallFont, Entry.bTarget);

		if(Entry.bShowName && Entry.Scale <= 60.f)
		{
			//绘制部族名称
			height -= DrawGuildName(X, height, Entry.Position.z, pfont, Entry.Color, scale, 
				Entry.GuildNameLen, Entry.wszguildName, Entry.emblemtexture, Entry.bTarget);

			//绘制NPC的Title
			height -= DrawTitleText(X, height, Entry.Position.z, pfont, Entry.Color, scale, 
				Entry.titleStrLen, Entry.wsztitleName, Entry.bTarget);

			//绘制名字行
			height -= DrawNameLine(X, height, Entry.Position.z, pfont, Entry.Color, scale, 
				Entry.StrLen, Entry.wszName, Entry.prefixalLen, Entry.wszprefixal, 
				Entry.zodiac, Entry.bTarget);

			//绘制攻城战时攻方守方标志
			height -= DrawCastleImage(X, height,Entry.ffa, Entry.Position.z, Entry.bTarget);

		}
		//团队标示
		if (Entry.bShowMask)
		{
			height -= DrawGroupMask(X, height, Entry.GroupMask, Entry.Position.z, Entry.bTarget);
		}
		if (Entry.bGM)
		{
			height -= DrawGMImage(X, height, Entry.Position.z, Entry.bTarget);
		}
		EndText();
		return X;
	}
	return 0;
}
bool CNameDrawer::BeginText(CharName &Entry, UFontPtr &pfont, float& scale)
{
	if ( Entry.wszName == NULL )
		return false;

	if (Entry.Scale < 0.0f)
		return false;

	scale = 100.f * float(1.f -   Entry.Position.z) * float(SMALL_FONT_SIZE / 30.0f);
	if(scale < SMALL_FONT_SIZE / 30.0f)
	{
		scale = 1.0f;
		pfont = m_SmallFont;
	}
	else
	{
		pfont = m_NameFont;
	}
	m_pURender->BeginRender();
    g_pRenderInterface->SetRenderState(D3DRS_ZENABLE, TRUE);

	return true;
}
void CNameDrawer::EndText()
{
	g_pRenderInterface->SetRenderState(D3DRS_ZENABLE, FALSE);
	m_pURender->EndRender();
}

INT CNameDrawer::DrawHPBar(bool IsShow, bool IsClassType,INT X, INT Y,float depth, float Progesspos, ui32 level, ui32 Class, UFontPtr pfont, bool btarget )
{
	if(!m_HpProgressBar || !m_HpBarBkg)
		return 0;
	if(!IsShow)
		return 0;

	if (Progesspos > 1.0f) Progesspos = 1.0f;

	int Width = (int)(m_HpProgressBar->GetWidth() * Progesspos);

	UPoint Pos(X - (m_HpBarBkg->GetWidth()>>1), Y - m_HpBarBkg->GetHeight());
	//drawbkg
	m_pURender->DrawImage(m_HpBarBkg, Pos, LCOLOR_WHITE, 0, depth);

	//drawLevel
	char buf[256];
	itoa(level, buf, 10);
	UPoint TextPos(Pos.x + 6, Pos.y + 6);
	UDrawText(m_pURender, pfont, TextPos, UPoint(22, 10), LCOLOR_RED, buf, UT_CENTER, depth);

	URect ScreenRect;
	ScreenRect.pt0.x = Pos.x + 32;
	ScreenRect.pt0.y = Pos.y + 6;
	ScreenRect.pt1.x = ScreenRect.pt0.x + Width;
	ScreenRect.pt1.y = ScreenRect.pt0.y + m_HpProgressBar->GetHeight();

	UColor HPBarColor(LCOLOR_RED);
	if(IsClassType)
	{
		static UColor pColor[] = {0xFF00FF00, 0xFF592212, 0xFF3B174A, 0xFF013561, 0xFF166014, 0, 0};
		HPBarColor = pColor[Class];
		if (!btarget)
			HPBarColor.a = 150;
	}
	m_pURender->DrawImage(m_HpProgressBar, ScreenRect, HPBarColor, 0, depth);

	return m_HpBarBkg->GetHeight() + 2;
}
INT CNameDrawer::DrawCastleImage(INT X, INT Y, uint32 ffa, float depth, bool btarget )
{
#define CASTLEIMAGE_SIZE 32
	if(!m_CastleAttack || !m_CastleDefense)
		return 0;
	UTexturePtr texture = NULL;

	if (ffa == 21){
		texture = m_CastleDefense;
	}else if (ffa == 22){
		texture = m_CastleAttack;
	}else if (ffa == 13){
		texture = NULL;
	}

	if (texture)
	{
		int texturesize = CASTLEIMAGE_SIZE;
		UPoint texturePos(X - (texturesize>>1), Y - texturesize - 5 );

		URect ScreenRect;
		ScreenRect.pt0.x = texturePos.x;
		ScreenRect.pt0.y = texturePos.y;
		ScreenRect.pt1.x = texturePos.x + texturesize;
		ScreenRect.pt1.y = texturePos.y + texturesize;

		UColor CastleImage(LCOLOR_WHITE);
		if (!btarget)
			CastleImage.a = 150;

		m_pURender->DrawImage(texture, ScreenRect, CastleImage, 0, depth);/*
		m_pURender->DrawSkin(m_MaskSkin, ffa%8 - 1, ScreenRect, LCOLOR_WHITE, 0, depth);*/
	}
	else 
		return 0;
	return CASTLEIMAGE_SIZE;
#undef CASTLEIMAGE_SIZE
}
INT CNameDrawer::DrawGMImage(INT X, INT Y, float depth, bool btarget )
{
#define GMMAGE_SIZE 40 
	if (!m_GMFlagTexture)
	{
		return 0;
	}
	
	int texturesize = GMMAGE_SIZE;
	UPoint texturePos(X - (texturesize>>1), Y - texturesize - 5 );

	URect ScreenRect;
	ScreenRect.pt0.x = texturePos.x;
	ScreenRect.pt0.y = texturePos.y;
	ScreenRect.pt1.x = texturePos.x + texturesize;
	ScreenRect.pt1.y = texturePos.y + texturesize;

	UColor GMImage(LCOLOR_WHITE);
	if (!btarget) GMImage.a = 150;

	m_pURender->DrawImage(m_GMFlagTexture, ScreenRect, GMImage, 0, depth);
	return GMMAGE_SIZE;

#undef GMMAGE_SIZE
}
INT CNameDrawer::DrawGroupMask(INT X, INT Y, uint32 mask, float depth, bool btarget )
{
#define MASKIMAGE_SIZE 40
	if(!m_MaskSkin || mask == 8)
		return 0;

	if (m_MaskSkin)
	{
		int texturesize = MASKIMAGE_SIZE;
		UPoint texturePos(X - (texturesize>>1), Y - texturesize - 5 );

		URect ScreenRect;
		ScreenRect.pt0.x = texturePos.x;
		ScreenRect.pt0.y = texturePos.y;
		ScreenRect.pt1.x = texturePos.x + texturesize;
		ScreenRect.pt1.y = texturePos.y + texturesize;

		m_pURender->DrawSkin(m_MaskSkin, mask%8, ScreenRect, LCOLOR_WHITE, 0, 0.f);
	}
	else 
		return 0;
	return MASKIMAGE_SIZE;
#undef MASKIMAGE_SIZE
}
INT CNameDrawer::DrawNameLine(INT X, INT Y, float depth, UFontPtr pfont, UColor &Color, float scale, INT NameLen, WCHAR* wszName, INT prefixalLen, WCHAR* wszprefixal, uint8 zodiac, bool btarget )
{
	std::wstring str;
	if(zodiac)
		str += L" ";
	if(prefixalLen)
	{
		str += wszprefixal;
		str += L" ";
	}
	if(NameLen)
		str += wszName;


	int Nametextwidth = (int)(pfont->GetStrWidth(str.c_str()) * scale);
	int Nametextheight = (int)(pfont->GetHeight() * scale);


	BYTE alpha = 255;
	UColor NameColor(Color);
	if (!btarget) 	alpha = 150;
	NameColor.a = alpha;
	if(zodiac)
	{
		URect zodiacScreenRect;
		UPoint zodiacPos(X - (Nametextwidth>>1) - Nametextheight, Y - (Nametextheight));

		zodiacScreenRect.pt0.x = zodiacPos.x;
		zodiacScreenRect.pt0.y = zodiacPos.y;
		zodiacScreenRect.pt1.x = zodiacPos.x + Nametextheight;
		zodiacScreenRect.pt1.y = zodiacPos.y + Nametextheight;

		m_pURender->DrawImage(m_zodiactexture[zodiac - 1], zodiacScreenRect, UColor(255, 255, 255, alpha), 0, depth);
	}

	UPoint NamePos(X - ((Nametextwidth)>>1), Y - (Nametextheight));
	m_pURender->DrawTextN(pfont, NamePos + UPoint(1, 1), str.c_str(), str.length(), NULL, UColor(0, 0, 0, alpha), depth, scale);
	m_pURender->DrawTextN(pfont, NamePos, str.c_str(), str.length(), NULL, NameColor, depth, scale);
	return Nametextheight;
}

INT CNameDrawer::DrawGuildName(INT X, INT Y, float depth, UFontPtr pfont, UColor &Color, float scale, INT GuildNameLen, WCHAR* wszguildName, UTexturePtr emblemtex, bool btarget )
{
	if (!GuildNameLen)
		return 0;

	float titleScale = scale * 0.98f;

	int width = pfont->GetStrWidth(wszguildName);
	int textwidth = (int)(width * titleScale);
	int textheight = (int)(pfont->GetHeight() * titleScale);

	BYTE alpha = 255;
	UColor GuildColor(Color);
	if (!btarget) 	alpha = 150;
	GuildColor.a = alpha;

	UPoint emblemSize(textheight, textheight);
	if (emblemtex)
	{
		URect emblemscreenRect;
		UPoint emblemPos(X - (textwidth>>1) - emblemSize.x, Y - (textheight));

		emblemscreenRect.pt0.x = emblemPos.x;
		emblemscreenRect.pt0.y = emblemPos.y;
		emblemscreenRect.pt1.x = emblemPos.x + emblemSize.x;
		emblemscreenRect.pt1.y = emblemPos.y + emblemSize.y;
		m_pURender->DrawImage(emblemtex , emblemscreenRect, UColor(255, 255, 255, alpha), 0, depth);
	}

	textwidth >>= 1;

	UPoint Pos(X - textwidth, Y - textheight);
	UPoint Size(width, pfont->GetHeight());
	m_pURender->DrawTextN(pfont, Pos + UPoint(1, 1), wszguildName, GuildNameLen, NULL, UColor(0, 0, 0, alpha), depth, scale);
	m_pURender->DrawTextN(pfont, Pos, wszguildName, GuildNameLen, NULL, GuildColor, depth, scale);
	return (textheight) + 4;
}
INT CNameDrawer::DrawTitleText(INT X, INT Y, float depth, UFontPtr pfont, UColor &Color, float scale, INT titleStrLen, WCHAR* wsztitleName, bool btarget )
{
	if (!titleStrLen)
		return 0;

	float titleScale = scale - 0.05f;

	int width = pfont->GetStrWidth(wsztitleName);

	int textwidth = (int)(width * titleScale);
	int textheight = (int)(pfont->GetHeight() * titleScale);

	textwidth >>= 1;

	UPoint Pos(X - textwidth, Y - textheight);
	UPoint Size(width, pfont->GetHeight());

	BYTE alpha = 255;
	UColor TitleColor(Color);
	if (!btarget)	alpha = 150;

	m_pURender->DrawTextN(pfont, Pos + UPoint(1, 1), wsztitleName, titleStrLen, NULL, UColor(0, 0, 0, alpha), depth, scale);
	TitleColor.a = alpha;
	m_pURender->DrawTextN(pfont, Pos, wsztitleName, titleStrLen, NULL, TitleColor, depth, scale);
	return (textheight) + 4;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
CCharacterName::CCharacterName()
{
}

CCharacterName::~CCharacterName()
{
	Destroy();
}

BOOL CCharacterName::Initialise()
{
	if (!m_NameDrawer.Create("Arial",30))
	{
		return FALSE;
	}
	return TRUE;
}

void CCharacterName::Destroy()
{
	
	for( std::set<CharNamePtr>::iterator it = m_CharName.begin(); it != m_CharName.end(); ++it )
	{
		CharName* Entry = (*it)._ptr;
		delete Entry;
	}
	m_CharName.clear();
}

void CCharacterName::UpdateEffect(float fTimeLine, float fDeltaTime)
{

}

void CCharacterName::RenderEffect(const NiCamera* pkCamera)
{
	for( std::set<CharNamePtr>::iterator it = m_CharName.begin(); it != m_CharName.end(); ++it )
	{
		CharName* Entry = (*it)._ptr;
		m_NameDrawer.TextOut( Entry->Position.x, Entry->Position.y, *Entry);
	}
	m_CharName.clear();
}
void CCharacterName::AddEntry(CharName* NewEntry)
{
	m_CharName.insert(CharNamePtr(NewEntry));
}