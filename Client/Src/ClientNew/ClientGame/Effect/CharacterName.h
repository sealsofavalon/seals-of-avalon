#pragma once

#include "EffectBase.h"

struct CharName
{
	bool ShowHp;
	bool bShowName;
	bool bShowMask;
	bool bGM;
	bool ShowHpBarColorful;
	bool bTarget;
	bool bAFK;
	bool bDND;
	NiPoint3 Position;
	float Scale;
	UColor Color;

	UTexturePtr emblemtexture;

	ui8 Class;
	ui8 zodiac;
	ui32 hp;
	ui32 Maxhp;
	ui32 Level;
	ui32 ffa;
	ui32 GroupMask;

	INT StrLen;
	WCHAR wszName[128];

	INT titleStrLen;
	WCHAR wsztitleName[128];

	INT GuildNameLen;
	WCHAR wszguildName[128];

	INT prefixalLen;
	WCHAR wszprefixal[128];  //��ǰ��ν
};

struct CharNamePtr
{
	CharNamePtr( CharName* p ) : _ptr( p ){}
	CharName* _ptr;
};

inline bool operator < ( const CharNamePtr& a, const CharNamePtr& b )
{
	return a._ptr->Position.z > b._ptr->Position.z;
}

class CNameDrawer
{
public:
	CNameDrawer();
	~CNameDrawer();
	BOOL Create(const char* FontFace,INT FontSize);
	inline bool BeginText(CharName &Entry, UFontPtr &pfont, float& scale);
	INT TextOut(INT X, INT Y, CharName &Entry);
	inline void EndText();
protected:
	INT DrawCastleImage(INT X, INT Y, uint32 ffa, float depth, bool btarget = false);
	INT DrawHPBar(bool IsShow,	bool IsClassType,INT X, INT Y,float depth, float Progesspos, ui32 level, ui32 Class,UFontPtr pfont, bool btarget = false);
	INT DrawGroupMask(INT X, INT Y, uint32 mask, float depth, bool btarget = false);
	INT DrawGMImage(INT X, INT Y, float depth, bool btarget = false);

	INT DrawNameLine(INT X, INT Y, float depth, UFontPtr pfont, UColor &Color, float scale,INT NameLen, WCHAR* wszName, INT prefixalLen, WCHAR* wszprefixal, uint8 zodiac, bool btarget = false);
	INT DrawGuildName(INT X, INT Y, float depth, UFontPtr pfont, UColor &Color, float scale, INT GuildNameLen, WCHAR* wszguildName, UTexturePtr emblemtex, bool btarget = false);
	INT DrawTitleText(INT X, INT Y, float depth, UFontPtr pfont, UColor &Color, float scale, INT titleStrLen, WCHAR* wsztitleName, bool btarget = false);
private:
	URender* m_pURender;
	UFontPtr m_NameFont;
	UFontPtr m_SmallFont;
	UTexturePtr m_HpBarBkg;
	UTexturePtr m_HpProgressBar;
	UTexturePtr m_CastleAttack;
	UTexturePtr m_CastleDefense;
	UTexturePtr m_GMFlagTexture;
	UTexturePtr m_zodiactexture[12];
	USkinPtr m_MaskSkin;
};
class CCharacterName : public CEffectBase
{
public:
	
public:
	CCharacterName();
	virtual ~CCharacterName();
public:
	BOOL Initialise();
	void Destroy();
	virtual void UpdateEffect(float fTimeLine, float fDeltaTime);
	void RenderEffect(const NiCamera* pkCamera);
	void AddEntry(CharName* NewEntry);
protected:
	std::set<CharNamePtr> m_CharName;
	CNameDrawer			m_NameDrawer;
};