#include "StdAfx.h"
#include "ChatEffect.h"
#include "NiDX9TextureData.h"
#include "Player.h"
#include "ClientApp.h"
#include "ObjectManager.h"
#include "UI\UISystem.h"
CChatPopoDrawer::CChatPopoDrawer()
{
	m_ChatFont = NULL;
	m_popoSkin = NULL;
	m_WhoChat  = NULL;
}
CChatPopoDrawer::~CChatPopoDrawer()
{
	m_ChatFont = NULL;
	m_popoSkin = NULL;
	m_WhoChat  = NULL;
}
BOOL CChatPopoDrawer::Create(const char* popoSkin, const char* FontFace, INT FontSize)
{
	if (m_ChatFont == NULL)
	{
		m_ChatFont = UControl::sm_UiRender->CreateFont(FontFace,FontSize,134);
		if (!m_ChatFont)
		{
			return FALSE;
		}
	}
	if(m_popoSkin == NULL)
	{
		m_popoSkin = UControl::sm_System->LoadSkin(popoSkin);
		if (!m_popoSkin)
		{
			return FALSE;
		}
	}
	if (m_WhoChat == NULL)
	{
		m_WhoChat = UControl::sm_UiRender->LoadTexture("Data\\UI\\Talkpopo\\SpeakPsn.png");
		if (!m_WhoChat)
		{
			return FALSE;
		}
	}
	return TRUE;
}
INT CChatPopoDrawer::TextOut(INT X, INT Y, INT TextLen, const WCHAR* Chat, float depth)
{
	USystem* pUSystem = SYState()->UI->GetUSystem();
	NIASSERT(pUSystem);
	URender* pURender = pUSystem->GetPlugin();	

	if (!pURender)
		return 0;
	pURender->BeginRender();
	//TODO Render
	UPoint pos(X, Y - 20);
	INT textWidth = m_ChatFont->GetStrNWidth(Chat, TextLen);
	INT textHeight = m_ChatFont->GetHeight();
	int lineNum = textWidth / SY_CHATTEXT_WIDTH + 1;
	textHeight = lineNum * textHeight + (lineNum - 1) * 4;
	textWidth = textWidth > SY_CHATTEXT_WIDTH ? SY_CHATTEXT_WIDTH : textWidth;
	pos.x -= textWidth>>1;
	pos.y -= textHeight>>1;
	
	UPoint size(textWidth /*+ 9*/, textHeight);

	UPoint pos1 = pos;
	pos1.x += (size.x>>1) - (m_WhoChat->GetWidth()>>1);
	pos1.y += size.y + 4;

	UDrawResizeBitmap(pURender, m_popoSkin, pos, size, depth);
	pURender->DrawImage(m_WhoChat, pos1, 0xFFFFFFFF, 0, depth);
	UDrawStaticText(pURender, m_ChatFont, pos, textWidth, 0xFFFFFFFF, Chat, UT_LEFT, 4, depth);
	pURender->EndRender();
	return X;
}

CChatEffect::CChatEffect()
{

}
CChatEffect::~CChatEffect()
{
	Destroy();
}

BOOL CChatEffect::Initialise()
{
	if (!m_ChatDrawer.Create("Talkpopo\\popo.skin", "Arial", 12))
	{
		return FALSE;
	}
	return TRUE;
}
void CChatEffect::Destroy()
{
	for( std::vector<ChatText*>::iterator it = m_ChatText.begin(); it != m_ChatText.end(); ++it )
	{
		ChatText* Entry = *it;
		delete Entry;
	}
	m_ChatText.clear();
}
void CChatEffect::UpdateEffect(float fTimeLine, float fDeltaTime)
{

}
void CChatEffect::RenderEffect(const NiCamera* pkCamera)
{
	for( std::vector<ChatText*>::iterator it = m_ChatText.begin(); it != m_ChatText.end(); ++it )
	{
		ChatText* Entry = *it;
		m_ChatDrawer.TextOut(Entry->Pos.x, Entry->Pos.y, Entry->StrLen, Entry->wszText, Entry->Depth);
		delete Entry;
	}
	m_ChatText.clear();

}
void CChatEffect::AddEntry(ChatText* NewEntry)
{
	m_ChatText.push_back(NewEntry);
}