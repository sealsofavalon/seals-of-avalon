#pragma once

#include "EffectBase.h"
#define SY_CHATTEXT_MAXLEN 128
#define SY_CHATTEXT_WIDTH 265
class CChatPopoDrawer
{
public:
	CChatPopoDrawer();
	~CChatPopoDrawer();
	BOOL Create(const char* popoSkin, const char* FontFace, INT FontSize);
	INT TextOut(INT X, INT Y, INT TextLen, const WCHAR* Chat, float depth);
private:
	UFontPtr m_ChatFont;
	UTexturePtr m_WhoChat;
	USkinPtr m_popoSkin;
};
class CChatEffect : public CEffectBase
{
public:
	struct ChatText
	{
		UPoint Pos;
		INT StrLen;
		float Depth;
		UColor color;
		WCHAR wszText[SY_CHATTEXT_MAXLEN];
	};
public:
	CChatEffect();
	virtual ~CChatEffect();
public:
	BOOL Initialise();
	void Destroy();
	virtual void UpdateEffect(float fTimeLine, float fDeltaTime);
	void RenderEffect(const NiCamera* pkCamera);
	void AddEntry(ChatText* NewEntry);
protected:
	std::vector<ChatText*> m_ChatText;
	CChatPopoDrawer		  m_ChatDrawer;
};