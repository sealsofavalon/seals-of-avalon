#ifndef DECAL_H
#define DECAL_H

//#include "GameObject.h"
#include "DecalManager.h"

#define MAX_DecalVertices 1024

class CClipGeometryProcess
{
	friend class CDecalManager;
public:
	struct Triangle
	{
		unsigned short	index[3];
	};

	static bool Clip(NiGeometry* pkGeometry,
			const NiPoint3& center, 
			const NiPoint3& normal, 
			const NiPoint3& tangent, 
			float width, 
			float height, 
			float depth
			);

protected:
	CClipGeometryProcess();
	~CClipGeometryProcess();

	static void ClipGeometry(NiGeometry* pkGeometry);

	static long ClipPolygon(
		long vertexCount, 
		const NiPoint3* vertex, 
		const NiPoint3* normal, 
		NiPoint3* newVertex, 
		NiPoint3* newNormal);

	static long ClipPolygonAgainstPlane(
		const NiPlane& plane, 
		long vertexCount, 
		const NiPoint3* vertex, 
		const NiPoint3* normal, 
		NiPoint3* newVertex, 
		NiPoint3* newNormal);

	static bool AddPolygon(
		long vertexCount, 
		const NiPoint3* vertex, 
		const NiPoint3* normal);

	static NiPoint3 m_kDecalCenter;
	static NiPoint3 m_kDecalNormal;

	static NiPlane m_kLeftPlane;
	static NiPlane m_kRightPlane;
	static NiPlane m_kBottomPlane;
	static NiPlane m_kTopPlane;
	static NiPlane m_kFrontPlane;
	static NiPlane m_kBackPlane;

	static unsigned int m_uiMaxVertexCount;
	static unsigned int m_uiMaxTriangleCount;


public:
	static int m_iDecalVertexCount;
	static int m_iDecalTriangleCount;

	static NiPoint3 m_kVertexArray[MAX_DecalVertices];
	static NiPoint2 m_kTexcoordArray[MAX_DecalVertices];
	static Triangle m_kTriangleArray[MAX_DecalVertices];
};

#endif