#include "stdafx.h"
#include "DecalEffect.h"

#include "Map/Map.h"
#include "ObjectManager.h"
#include "DynamicSimpleMesh.h"
#include "Camera.h"
#include "LocalPlayer.h"

#include "SceneManager.h"


float DecalEffect::m_fDepthBias = -0.00016f;

DecalEffect::DecalEffect()
{
	m_bBuildMesh = false;
	m_bUpdateUV = false;
	m_bUpdateColor = false;

	m_bNeedUpdateMesh = false;
	m_bNeedUpdateColor = false;
	m_bPostRender = false;

	m_usWidth = 0;
	m_usHeight = 0;

	m_fFadeIn = 0.0f;
	m_fFadeOut = 0.0f;

	m_iAttachObjectID = 0;		// invalid character id
	m_bFacingCam = false;

	m_fHeight = 0.05f;

	m_spTerrainMesh = NiNew DynamicSimpleMesh(NiNew DynamicSimpleMeshData(false, false, 1));
	m_spModelMesh = NiNew DynamicSimpleMesh(NiNew DynamicSimpleMeshData(false, false, 1));

	NiTexturingProperty* pkTexturingProp = NiNew NiTexturingProperty;
	pkTexturingProp->SetBaseClampMode(NiTexturingProperty::CLAMP_S_CLAMP_T);
	pkTexturingProp->SetBaseFilterMode(NiTexturingProperty::FILTER_BILERP);
	m_spTerrainMesh->AttachProperty(pkTexturingProp);
	m_spModelMesh->AttachProperty(pkTexturingProp);

	m_pkAlphaP = NiNew NiAlphaProperty;
	m_pkAlphaP->SetAlphaBlending(true);
	m_pkAlphaP->SetSrcBlendMode(NiAlphaProperty::ALPHA_SRCALPHA);
	m_pkAlphaP->SetDestBlendMode(NiAlphaProperty::ALPHA_ONE);

	m_pkMaterail = NiNew NiMaterialProperty;
	m_pkMaterail->SetAlpha(1.0f);
	m_pkMaterail->SetDiffuseColor(NiColor::BLACK);
	m_pkMaterail->SetEmittance(NiColor::WHITE);

	NiZBufferProperty* pkZPropery = NiNew NiZBufferProperty;
	pkZPropery->SetTestFunction( NiZBufferProperty::TEST_LESS );

	m_fDepthBias -= 0.00001f;
	if (m_fDepthBias <= -0.00021f)
		m_fDepthBias = -0.00016f;

	pkZPropery->SetZSlopeBias(0.0008f);
	pkZPropery->SetZBias(m_fDepthBias);

	//m_spTerrainMesh->AttachProperty(pkZPropery);
	m_spTerrainMesh->AttachProperty(m_pkAlphaP);
	m_spTerrainMesh->AttachProperty(m_pkMaterail);

	//m_spModelMesh->AttachProperty(pkZPropery);
	m_spModelMesh->AttachProperty(m_pkAlphaP);
	m_spModelMesh->AttachProperty(m_pkMaterail);

	m_spEffectNode = NiNew NiNode;
	m_spEffectNode->AttachChild(m_spTerrainMesh);
	m_spEffectNode->AttachChild(m_spModelMesh);

	m_spEffectNode->Update(0);
	m_spEffectNode->UpdateProperties();
	m_spEffectNode->UpdateEffects();
}

DecalEffect::~DecalEffect()
{
//	m_aiPolygons.RemoveAll();
}

void DecalEffect::UpdateEffect(float fTimeLine, float fDeltaTime)
{
	m_bBuildMesh = false;
	m_bUpdateUV = true;
	m_bUpdateColor = false;

	if (m_bNeedUpdateMesh)
	{
		m_bBuildMesh = true;
		m_bUpdateUV = true;
		m_bUpdateColor = true;
	}

	CCharacter* pkChar = NULL;
	UpdateObj* pObj = ObjectMgr->GetObject( m_iAttachObjectID );
	if ( pObj && pObj->isType( TYPE_UNIT ) )
	{
		pkChar = (CCharacter*)pObj;
	}

	if ( pkChar && pkChar->GetSceneNode())
	{
		NiBound kBound;// = pkChar->GetSceneNode()->GetWorldBound();
		bool b = pkChar->GetPickBound(kBound);
		NIASSERT(b);
		m_fRadius = kBound.GetRadius() * 0.5f;
	}

	if (m_bNeedUpdateColor)
	{
		m_bUpdateColor = true;
	}

	if (m_bBuildMesh /*&& m_aiPolygons.GetSize() > 0*/)
	{
		//int i = (int)m_aiPolygons.GetSize() - 1;
		//for (i; i >= 0; --i)
		//{
		//	m_spDecalMesh->Remove(m_aiPolygons.GetAt(i));
		//}
		//m_aiPolygons.RemoveAll();
		m_spModelMesh->RemoveAll();
	}

	ClipTerrain(m_kUpdatePos, m_fRadius, m_fDepth);
}

void DecalEffect::RenderEffect(const NiCamera* pkCamera)
{
	if (m_bUpdateUV)
	{
		CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
		NIASSERT(pkLocalPlayer);
		if (!pkLocalPlayer)
			return;

		CCamera& kCamera = pkLocalPlayer->GetCamera();//.GetTranslate();
		const NiCamera& kNiCamera = kCamera.GetNiCamera();

		NiPoint3 kCamWorldPos = kNiCamera.GetTranslate();
		UpdatePolygonTextures(m_spTerrainMesh, kCamWorldPos);
		UpdatePolygonTextures(m_spModelMesh, kCamWorldPos);
	}

	if (m_bUpdateColor)
	{
		UpdatePolygonColors();
	}

	float alpha = 1.0f;
	if (m_fFadeIn > 0.0f)
	{
		alpha = GetElapsed()/m_fFadeIn;
		alpha = NiClamp(alpha, 0.0f, 1.0f);
		m_pkMaterail->SetAlpha(alpha);
	}

	if (m_fFadeOut > 0.0f && GetCurTime() >= m_fEndTime - m_fFadeOut)
	{
		alpha = (GetCurTime() + m_fFadeOut - m_fEndTime)/m_fFadeOut;
		m_pkMaterail->SetAlpha(1.0f-alpha);
	}

	m_spEffectNode->Update(0, false);

	NiBound kWorldBound;
	kWorldBound = m_spModelMesh->GetWorldBound();
	kWorldBound.Merge(&m_spTerrainMesh->GetWorldBound());
	m_spEffectNode->SetWorldBound(kWorldBound);

	m_bNeedUpdateColor = false;
	m_bNeedUpdateMesh = false;

	SceneMgr->RenderObject(m_spEffectNode);
}

bool DecalEffect::ClipTerrain(const NiPoint3& kCenter, float fRadius, float fDepth)
{
	if (fRadius < 0.1f) return false;

	float fTerrHeight = -FLT_MAX;
	CMap::Get()->GetHeight(kCenter.x, kCenter.y, fTerrHeight);
	if (fTerrHeight < kCenter.z - fDepth ) return false;

	int X0,X1,Y0,Y1;
	if (!FindBoundOnTerrain(kCenter, fRadius, &X0, &X1, &Y0, &Y1))
	{
		return false;
	}

	unsigned short usNewWidth = (unsigned short)(X1-X0+1);
	unsigned short usNewHeight = (unsigned short)(Y1-Y0+1);

	if (m_usWidth == 0) {
		usNewWidth++;
	}

	if (m_usHeight == 0) {
		usNewHeight++;
	}

	if (usNewWidth > m_usWidth || usNewHeight > m_usHeight)
	{
		m_bBuildMesh = true;
		m_bUpdateUV = true;
		m_bUpdateColor = true;
	}

	if (m_bBuildMesh)
		UpdateTerrainMeshVertices(usNewWidth, usNewHeight, X0, Y0);

	m_usWidth = usNewWidth;
	m_usHeight = usNewHeight;

	return true;
}

void DecalEffect::UpdatePolygonTextures(DynamicSimpleMesh* pkMesh, const NiPoint3& kEyePos)
{
	if (!pkMesh)
		return;

	DynamicSimpleMeshData* pkData = (DynamicSimpleMeshData*)pkMesh->GetData();
	if (!pkData)
		return;

	float Rad = m_fRotation*NI_PI/180.0f;
	float Cos = NiCos(Rad);
	float Sin = NiSin(Rad);
	float u,v;

	int iPolygons = pkData->GetNumPolygons();
	for (int i=0; i<iPolygons; i++)
	{
		int iVertices = pkData->GetNumVertices(i);
		for (int j=0; j<iVertices; j++)
		{
			NiPoint3 ptVert;
			if (!pkData->GetVertex(i, j, ptVert))
			{
				NIASSERT(0);
			}

			// 变换到中心坐标
			NiPoint2 xyPos = NiPoint2(ptVert.x - m_kUpdatePos.x, ptVert.y - m_kUpdatePos.y);
			NiPoint3 camLoc = kEyePos - m_kUpdatePos;

			NiPoint2 xm, ym;
			float fLength = NiSqrt(camLoc.x*camLoc.x + camLoc.y*camLoc.y);
			if (fLength > 1e-12f)
			{
				fLength = 1.0f/fLength;
				camLoc.x *= fLength;
				camLoc.y *= fLength;

				// Rotation1
				xm = NiPoint2(camLoc.y, -camLoc.x);
				ym = NiPoint2(camLoc.x, camLoc.y);
			}
			else
			{
				xm = NiPoint2(Cos, -Sin);
				ym = NiPoint2(Sin, Cos);
			}

			float fInverseRadius = 1.0f/m_fRadius;


			if (m_bFacingCam)
			{
				u = xyPos.Dot(xm) * fInverseRadius;
				v = xyPos.Dot(ym) * fInverseRadius;
			}
			else
			{
				u = xyPos.x * fInverseRadius;
				v = xyPos.y * fInverseRadius;
			}

			float uu = (u + 1.0f)*0.5f;
			float vv = (v + 1.0f)*0.5f;

			pkMesh->SetTextures(i, 0, j, NiPoint2(uu, vv));
		}
	}

	m_bUpdateUV = false;
}

void DecalEffect::UpdatePolygonColors()
{
	if (!m_spTerrainMesh)
		return;

	NiMaterialProperty* pkMaterialProp = (NiMaterialProperty*)m_spTerrainMesh->GetProperty(NiProperty::MATERIAL);
	if (!pkMaterialProp)
		return;

	pkMaterialProp->SetEmittance(NiColor(m_kColor.r, m_kColor.g, m_kColor.b));
}

BOOL DecalEffect::FindBoundOnTerrain(const NiPoint3& Center, float Radius, INT* xMin, INT* xMax, INT* yMin, INT* yMax)
{
	CMap* Map = CMap::Get();
	if (!Map) return FALSE;

	if (!Map->IsLoaded()) return FALSE;

	float XMin = (Center.x - Radius) * 0.25f;
	float XMax = (Center.x + Radius) * 0.25f;
	float YMin = (Center.y - Radius) * 0.25f;
	float YMax = (Center.y + Radius) * 0.25f;

	INT X1 = (INT)ceilf(XMax);
	INT X0 = (INT)floorf(XMin);
	INT Y1 = (INT)ceilf(YMax);
	INT Y0 = (INT)floorf(YMin);

	if (X1 <= X0) return FALSE;
	if (Y1 <= Y0) return FALSE;

	if (xMin) *xMin = X0;
	if (yMin) *yMin = Y0;
	if (xMax) *xMax = X1;
	if (yMax) *yMax = Y1;

	return TRUE;
}

int DecalEffect::AddPolygon(unsigned short usNumVertices, unsigned short usNumTriangles, const unsigned short* ausTriList, NiPoint3* akVaule)
{
	NIASSERT(!IsDead());

	if (ausTriList == 0)
		usNumTriangles = 0;

	int iPolygon = Insert(usNumVertices, usNumTriangles, ausTriList);

	for (int i=0; i<usNumVertices; i++)
	{
		akVaule[i].z = akVaule[i].z + m_fHeight;
	}
	m_spModelMesh->SetVertices(iPolygon, akVaule);

	m_bUpdateUV = true;
	m_bUpdateColor = true;

	return iPolygon;
}

void DecalEffect::UpdateTerrainMeshVertices(unsigned short usNewWidth, unsigned short usNewHeight, int iStartX, int iStartY)
{
	if (!m_spTerrainMesh)	return;

	m_spTerrainMesh->RemoveAll();

	unsigned short usVertexCount = usNewWidth * usNewHeight;

	float fTerrHeight;
	int vi;
	float xpos, ypos;

	unsigned short* ausIndices = NiAlloc(unsigned short, 6 * (usNewHeight - 1) * (usNewWidth - 1));

	//unsigned short x, y;
	unsigned short usIndex=0;
    for(unsigned short y = 1; y < usNewHeight; y++ )
    {
        for(unsigned short x = 1; x < usNewWidth; x++ )
        {
            ausIndices[usIndex+0] = ( unsigned short )( ( y - 1 ) * usNewWidth + ( x - 0 ) );
            ausIndices[usIndex+1] = ( unsigned short )( ( y - 0 ) * usNewWidth + ( x - 1 ) );
            ausIndices[usIndex+2] = ( unsigned short )( ( y - 1 ) * usNewWidth + ( x - 1 ) );

            ausIndices[usIndex+3] = ( unsigned short )( ( y - 0 ) * usNewWidth + ( x - 0 ) );
            ausIndices[usIndex+4] = ( unsigned short )( ( y - 0 ) * usNewWidth + ( x - 1 ) );
            ausIndices[usIndex+5] = ( unsigned short )( ( y - 1 ) * usNewWidth + ( x - 0 ) );

			usIndex += 6;
        }
    }

	unsigned short usTrigCount = usIndex / 3;

	m_spTerrainMesh->Insert(usVertexCount, usTrigCount, ausIndices);

	for (INT y = 0; y < usNewHeight; y++)
	{
		ypos = (y + iStartY)*4.0f;
		for (INT x = 0; x < usNewWidth; x++)
		{
			xpos = (x + iStartX)*4.0f;
			CMap::Get()->GetHeight(xpos,ypos,fTerrHeight);
			vi = x + y* usNewWidth;

			NiPoint3 kPos = NiPoint3(xpos, ypos, fTerrHeight + m_fHeight);
			m_spTerrainMesh->SetVertex(0, y * usNewWidth + x, kPos);
		}
	}

	NiFree(ausIndices);

	m_spTerrainMesh->Update(0.0f);

	//char szBuffer[256];
	//_snprintf(szBuffer, 256, "Terrain mesh vertices count:%d\n", usVertexCount);
	//NiOutputDebugString(szBuffer);
}

int DecalEffect::Insert(unsigned short usNumVertices, 
		unsigned short usNumTriangles, const unsigned short* ausTriList /*= NULL*/)
{
	NIASSERT(m_spModelMesh);
	return m_spModelMesh->Insert(usNumVertices, usNumTriangles, ausTriList);
}

void DecalEffect::SetTexture(const char* szTexFile)
{
	if (!m_spTerrainMesh)
		return;

	NiTexture* pkTexture = NiSourceTexture::Create(szTexFile);
	if (!pkTexture)
		return;

	SetTexture(pkTexture);
}

void DecalEffect::SetTexture(NiTexture* pkTexture)
{
	NiTexturingProperty* pkTexturingProp = (NiTexturingProperty*)m_spTerrainMesh->GetProperty(NiProperty::TEXTURING);
	if (!pkTexturingProp)
	{
		pkTexturingProp = NiNew NiTexturingProperty;
		if (!pkTexturingProp) {
			NIASSERT(0);
			return;
		}
		m_spModelMesh->AttachProperty(pkTexturingProp);
	}

	pkTexturingProp->SetBaseTexture(pkTexture);
}

void DecalEffect::SetZSlopeBias(float fValue)
{
	NiZBufferProperty* pkZBuffer = (NiZBufferProperty*)m_spTerrainMesh->GetProperty(NiProperty::ZBUFFER);
	if (!pkZBuffer)
	{
		pkZBuffer = NiNew NiZBufferProperty;
		if (!pkZBuffer)
			return;
		m_spModelMesh->AttachProperty(pkZBuffer);
	}

	pkZBuffer->SetZSlopeBias(fValue);
}

void DecalEffect::SetZBias(float fValue)
{
	NiZBufferProperty* pkZBuffer = (NiZBufferProperty*)m_spTerrainMesh->GetProperty(NiProperty::ZBUFFER);
	if (!pkZBuffer)
	{
		pkZBuffer = NiNew NiZBufferProperty;
		if (!pkZBuffer)
			return;
		m_spModelMesh->AttachProperty(pkZBuffer);
	}

	pkZBuffer->SetZBias(fValue);
}

void DecalEffect::SetFadein(float fT)
{
	m_fFadeIn = fT;
}

void DecalEffect::SetFadeout(float fT)
{
	m_fFadeOut = fT;
}

void DecalEffect::SetSrcAlphaOP(int op)
{
	if (!m_pkAlphaP)
		return;

	m_pkAlphaP->SetSrcBlendMode((NiAlphaProperty::AlphaFunction)op);
}

void DecalEffect::SetDestAlphaOP(int op)
{
	if (!m_pkAlphaP)
		return;

	m_pkAlphaP->SetDestBlendMode((NiAlphaProperty::AlphaFunction)op);
}

