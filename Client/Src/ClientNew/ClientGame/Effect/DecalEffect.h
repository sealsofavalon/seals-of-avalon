#ifndef _DECALEFFECT_H_
#define _DECALEFFECT_H_

#include "EffectBase.h"
#include "GameObject.h"

NiSmartPointer(DynamicSimpleMesh);

class DecalEffect : public CEffectBase
{
public:
	DecalEffect();
	virtual ~DecalEffect();

	virtual BOOL SelfManage() { return FALSE;}
	virtual BOOL SelfRender() const { return FALSE; }

	bool ClipTerrain(const NiPoint3& kCenter, float fRadius, float fDepth);
	int AddPolygon(unsigned short usNumVertices, unsigned short usNumTriangles, 
		const unsigned short* ausTriList, NiPoint3* akVaule);

	void SetTexture(const char* szTexFile);
	void SetTexture(NiTexture* pkTexture);
	void SetZSlopeBias(float fValue);
	void SetZBias(float fValue);

	//bool SetVertices(int iPolygon, NiPoint3* akValue);
    //bool SetTextures(DynamicSimpleMesh* pkMesh, int iPolygon, unsigned short usSet,
    //    const NiPoint2* akValue);

	void UpdateEffect(float fTimeLine, float fDeltaTime);
	void RenderEffect(const NiCamera* pkCamera);

	inline void PreCalcTimeLine() {
		m_fEndTime = m_MaxLife + m_fStartTime + m_fFadeOut;
	}

	inline float GetRadius() const  { return m_fRadius; }
	inline float GetDepth() const { return m_fDepth; }
	inline const NiPoint3& GetPosition() const { return m_kUpdatePos; }

	inline void SetBrushHeight(float fHeight)
	{
		if ( fHeight != m_fHeight )
		{
			m_fHeight = fHeight;
			m_bNeedUpdateMesh = true;
		}
	}

	inline void SetRadius(float fRadius) { 
		if (fRadius != m_fRadius) {
			if ( fRadius > 30.f )
			{
				fRadius = 30.f;
			}
			m_fRadius = fRadius; 
			m_bNeedUpdateMesh = true;
		}
	}
	inline void SetDepth(float fDepth) { 
		if (fDepth != m_fDepth) {
			m_fDepth = fDepth; 
			m_bNeedUpdateMesh = true;
		}
	}
	inline void SetPosition(const NiPoint3& ptPos) { 
		if (ptPos != m_kUpdatePos) {
			m_kUpdatePos = ptPos; 
			m_bNeedUpdateMesh = true;
		}
	}
	inline void SetColor(const NiColorA& kColor) { 
		if (kColor != m_kColor) {
			m_kColor = kColor; 
			m_bNeedUpdateColor = true;
		}
	}

	inline void SetFacingCam(bool val) {
		m_bFacingCam = val;
	}

	inline const NiColorA& GetColor() const { return m_kColor; }

	inline void SetAttachObjectID(ui64 iObjectID) { 
		m_iAttachObjectID = iObjectID; 
		m_bNeedUpdateMesh = true;
	}
	inline ui64 GetAttachObjectID() const {
		return m_iAttachObjectID; 
	}

	inline void SetPostRender(bool value) {
		m_bPostRender = value;
	}

	inline bool IsPostRender() {
		return m_bPostRender;
	}

	inline bool IsNeedUpdateMesh() { return m_bNeedUpdateMesh; }

	void SetFadein(float fT);
	void SetFadeout(float fT);
	void SetSrcAlphaOP(int op);
	void SetDestAlphaOP(int op);

	inline NiNode* GetEffectNode() {
		return m_spEffectNode;
	}

protected:
	int Insert(unsigned short usNumVertices, 
		unsigned short usNumTriangles, const unsigned short* ausTriList = NULL);

	void UpdatePolygonTextures(DynamicSimpleMesh* pkMesh, const NiPoint3& kEyePos);
	void UpdatePolygonColors();

	void UpdateTerrainMeshVertices(unsigned short usNewWidth, unsigned short usNewHeight, int iStartX, int iStartY);

	BOOL FindBoundOnTerrain(const NiPoint3& Center, float Radius, INT* xMin, INT* xMax, INT* yMin, INT* yMax);

private:
	DynamicSimpleMeshPtr m_spTerrainMesh;
	DynamicSimpleMeshPtr m_spModelMesh;
	NiNodePtr m_spEffectNode;

	NiAlphaProperty* m_pkAlphaP;
	NiMaterialProperty* m_pkMaterail;

	float m_fHeight;
	float m_fRadius;
	float m_fDepth;
	float m_fRotation;
	NiColorA m_kColor;

	ui64 m_iAttachObjectID;

	NiPoint3 m_kUpdatePos;

	unsigned short m_usWidth;
	unsigned short m_usHeight;

	bool m_bBuildMesh;
	bool m_bUpdateUV;
	bool m_bUpdateColor;

	bool m_bNeedUpdateMesh;
	bool m_bNeedUpdateColor;

	bool m_bFacingCam;
	float m_fFadeIn;
	float m_fFadeOut;

	bool m_bPostRender;

	static float m_fDepthBias;
};

#endif //