#include "StdAfx.h"
#include "DecalManager.h"
#include "SceneManager.h"

#include "ClientState.h"
#include "ClientApp.h"

#include "Console.h"

#include "DecalEffect.h"
#include "DecalModelBuild.h"

#include "ObjectManager.h"


CDecalManager::CDecalManager()
{
}

CDecalManager::~CDecalManager()
{
	
	Destroy();
}

bool CDecalManager::Initialize()
{
	m_pkDecalRenderer = new DecalModelRenderer;
	if (!m_pkDecalRenderer)
		return false;

	ModelAdditionRenderer::AddRenderer(m_pkDecalRenderer);

	return true;
}

void CDecalManager::Update(float dt)
{
	unsigned int ui = 0;
	for (ui; ui < m_Decals.size(); ui++)
	{
		DecalEffect* pkDecal = m_Decals[ui];
		NIASSERT(pkDecal);
		if (!pkDecal) continue;
		if (!pkDecal->IsVisible() || pkDecal->IsDead()) continue;

		if (pkDecal->GetAttachObjectID() != 0)
		{
			ui64 id = pkDecal->GetAttachObjectID();
			CGameObject* pkObj = (CGameObject*)ObjectMgr->GetObject(id);
			if (pkObj)
			{
				const NiPoint3& ptUpdatePos = pkObj->GetPosition();
				pkDecal->SetPosition(ptUpdatePos);
			}
		}

		pkDecal->Update(SYState()->ClientApp->GetAccumTime(), dt);
	}

	for (ui=0; ui < m_PostDecals.size(); ui++)
	{
		DecalEffect* pkDecal = m_PostDecals[ui];
		NIASSERT(pkDecal);
		if (!pkDecal) continue;
		if (!pkDecal->IsVisible() || pkDecal->IsDead()) continue;

		if (pkDecal->GetAttachObjectID() != 0)
		{
			ui64 id = pkDecal->GetAttachObjectID();
			CGameObject* pkObj = (CGameObject*)ObjectMgr->GetObject(id);
			if (pkObj)
			{
				const NiPoint3& ptUpdatePos = pkObj->GetPosition();
				pkDecal->SetPosition(ptUpdatePos);
			}
			else
				pkDecal->SetVisible(false);
		}

		pkDecal->Update(SYState()->ClientApp->GetAccumTime(), dt);
	}
	
}

void CDecalManager::Remove(DecalEffect* pkDecal)
{
	pkDecal->Deactivate();

	if (pkDecal->IsPostRender())
	{
		std::vector<DecalEffect*>::iterator it = m_PostDecals.begin();
		for (it; it != m_PostDecals.end(); ++it)
		{
			if (*it == pkDecal)
			{
				m_PostDecals.erase(it);
				break;
			}
		}
	}
	else
	{
		std::vector<DecalEffect*>::iterator it = m_Decals.begin();
		for (it; it != m_Decals.end(); ++it)
		{
			if (*it == pkDecal)
			{
				m_Decals.erase(it);
				break;
			}
		}
	}


	m_pkDecalRenderer->RemoveDecal(pkDecal);
}

void CDecalManager::Destroy()
{
	for (unsigned int ui=0; ui < m_Decals.size(); ui++)
	{
		m_pkDecalRenderer->RemoveDecal(m_Decals[ui]);
		NiDelete m_Decals[ui];
	}

	m_Decals.clear();

	for (unsigned int ui=0; ui < m_PostDecals.size(); ui++)
	{
		m_pkDecalRenderer->RemoveDecal(m_PostDecals[ui]);
		NiDelete m_PostDecals[ui];
	}
	m_PostDecals.clear();

	if (m_pkDecalRenderer)
	{
		delete m_pkDecalRenderer;
		m_pkDecalRenderer = 0;
	}
}

void CDecalManager::RegisterEffect(CEffectBase* Effect)
{
	DecalEffect* effect = (DecalEffect*)Effect;
	if (effect->IsPostRender())
		m_PostDecals.push_back(effect);
	else
		m_Decals.push_back(effect);

	m_pkDecalRenderer->AddDecal(Effect);
}

void CDecalManager::ReleaseEffect(CEffectBase* Effect, bool bReleaseChild)
{
	Remove((DecalEffect*)Effect);
}

void  CDecalManager::Render(const NiCamera* pkCamera)
{
	unsigned int ui = 0;
	for (ui; ui < m_Decals.size(); ui++)
	{
		DecalEffect* pkDecal = m_Decals[ui];
		NIASSERT(pkDecal);
		if (!pkDecal) continue;
		if (!pkDecal->IsVisible() || pkDecal->IsDead()) continue;

		pkDecal->RenderEffect(pkCamera);
	}

}

void CDecalManager::PostRender(const NiCamera* pkCamera)
{
	unsigned int ui = 0;
	for (ui; ui < m_PostDecals.size(); ui++)
	{
		DecalEffect* pkDecal = m_PostDecals[ui];
		NIASSERT(pkDecal);
		if (!pkDecal) continue;
		if (!pkDecal->IsVisible() || pkDecal->IsDead()) continue;

		pkDecal->RenderEffect(pkCamera);
	}
}

