#ifndef CDECALMANAGER_H
#define CDECALMANAGER_H


#include "EffectManageBase.h"


typedef int DECALID;
const DECALID INVALID_DECAL_ID = -1;

class DecalModelRenderer;

class DecalEffect;
class CDecalManager : public CEffectManageBase
{
	// 主要用于范围技能的区域选择上. 
public:
	CDecalManager();
	~CDecalManager();

public:
	bool Initialize();
	void Update(float dt);
	void Destroy();

	void Remove(DecalEffect* pkDecal);

	void RegisterEffect(CEffectBase* Effect);
	void ReleaseEffect(CEffectBase* Effect, bool bReleaseChild = false);

	void Render(const NiCamera* pkCamera);
	void PostRender(const NiCamera* pkCamera);

protected:
	DecalModelRenderer* m_pkDecalRenderer;
	std::vector<DecalEffect*> m_Decals;
	std::vector<DecalEffect*> m_PostDecals;

};

#endif
