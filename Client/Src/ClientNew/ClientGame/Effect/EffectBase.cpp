#include "StdAfx.h"
#include "EffectBase.h"
#include "EffectManager.h"
CEffectBase::CEffectBase(void)
{
	m_bVisible = false;
	m_bRegesiter = false;
	m_bAutoDelete = false;
	m_bNeedRelease = true;
	m_pkManage = 0;
	m_pkNext = 0;
	m_fEffScale = 1.f;
	m_bNoAutoScal = false;
	m_bWithSender = false;
	ResetTime();
}

CEffectBase::~CEffectBase(void)
{
}

void CEffectBase::AddCallbackObject(EffectCallbackObject* spObj)
{
	m_kCallbacks.Add(spObj);
}

void CEffectBase::RemoveCallbackObject(EffectCallbackObject* spObj)
{
	m_kCallbacks.Remove(spObj);
	m_kCallbacks.Compact();
}

void CEffectBase::Register(CEffectManageBase* pkManage)
{
	NIASSERT(pkManage);

	m_pkManage = pkManage;
	if (!SelfManage())
	{
		m_pkManage->RegisterEffect(this);
	}
}

void CEffectBase::UnRegister()
{
	NIASSERT(m_pkManage);
	if (!SelfManage())
	{
		m_pkManage->ReleaseEffect(this);
	}
}

void CEffectBase::Activate(float fTime, bool bVisible /*= false*/)
{ 
	SetCurTime(fTime);

	m_fStartTime = fTime;
	PreCalcTimeLine();

	SetVisible(bVisible);

	if (!m_bRegesiter)
	{
		Register(EffectMgr);
		m_bRegesiter = true;
	}
	else
	{
		NIASSERT("alrealy register" && 0);
	}

	ActivateEffect(fTime);
}


void CEffectBase::Activate(CEffectManageBase* pkManage, float fTime, bool bVisible /*= false*/)
{
	NIASSERT(pkManage);
	if (!pkManage)
		return;

	SetCurTime(fTime);
	m_fStartTime = fTime;
	PreCalcTimeLine();

	SetVisible(bVisible);

	if (!m_bRegesiter)
	{
		Register(pkManage);
		m_bRegesiter = true;
	}
	else
	{
		NIASSERT("alrealy register" && 0);
	}

	ActivateEffect(fTime);

    unsigned int uiCount = m_kCallbacks.GetSize();
    for (unsigned int ui = 0; ui < uiCount; ui++)
        m_kCallbacks.GetAt(ui)->ActivationChanged(this, true);
}

void CEffectBase::Update(float fTimeLine, float fDeltaTime)
{
	SetCurTime(fTimeLine);

	if (IsDead())
	{
	    unsigned int uiCount = m_kCallbacks.GetSize();
		for (unsigned int ui = 0; ui < uiCount; ui++)
			m_kCallbacks.GetAt(ui)->ActivationChanged(this, false);

		Deactivate();
		return;
	}

	if (!IsNeedUpdate())
	{
		return;
	}

	UpdateEffect(fTimeLine, fDeltaTime);

}

bool CEffectBase::IsNeedUpdate()
{
	if (!IsVisible())
		return false;

	return true;
}

void CEffectBase::Render(const NiCamera* pkCamera) {
	if (!IsVisible())
		return;

	RenderEffect(pkCamera);
}

