#pragma once

class CEffectManageBase;
class CEffectBase : public NiMemObject
{
public:
	CEffectBase(void);
	virtual ~CEffectBase(void);

	class EffectCallbackObject : public NiRefObject
	{
	public:
		EffectCallbackObject() : m_pkData(0) {}
		virtual ~EffectCallbackObject(void) {};
		virtual void ActivationChanged(CEffectBase* effect, bool activated) = 0;

		void AddData(void* pkData) {
			m_pkData = pkData;
		}

		void* GetData() {
			return m_pkData;
		}

	private:
		void* m_pkData;
	};
	typedef NiPointer<EffectCallbackObject> EffectCallbackObjectPtr;

	void AddCallbackObject(EffectCallbackObject* spObj);
	void RemoveCallbackObject(EffectCallbackObject* spObj);

	virtual void Update(float fTimeLine, float fDeltaTime);
	virtual void Render(const NiCamera* pkCamera);

	virtual void UpdateEffect(float fTimeLine, float fDeltaTime) = 0;
	virtual void RenderEffect(const NiCamera* pkCamera){};

	bool IsNeedUpdate();

	virtual BOOL SelfManage() { return FALSE;}
	virtual BOOL SelfRender() const { return FALSE; }
	virtual void AttachToScene(const NiPoint3& Position, SYObjID IdSender = 0, bool bLiveWithSender = false ){}
	virtual void AttachToSceneObject(SYObjID ParentID, const NiFixedString* pParentName = NULL, bool bWorld = false, bool bDetachUnbind = true, float scale = 1.0f ){}
	virtual void DettachFromScene() {}

	virtual void ActivateEffect(float fTime) {}

	virtual void Detach(){}

	void ResetTime() {
		m_MaxLife = 86400.0f;
		m_fStartTime = m_fCurTime = -FLT_MAX;
		m_fEndTime = FLT_MAX;
	}
	float GetElapsed() { 
		return m_fCurTime - m_fStartTime;
	}
	virtual void PreCalcTimeLine() {
		m_fEndTime = m_MaxLife + m_fStartTime;

		NIASSERT(m_fEndTime < 86400.0f);
		NIVERIFY(m_fEndTime < 86400.0f);
	}
	virtual BOOL IsDead() const
	{
		return BOOL(m_fCurTime >= m_fEndTime);
	}
	void Activate(CEffectManageBase* pkManage, float fTime, bool bVisible = false);
	void Activate(float fTime, bool bVisible = false);

	void Deactivate() { 
		ResetTime();
		SetVisible(false);

		if ( m_bNeedRelease )
		{
			Destroy();
		}
	
	}
	virtual void SetTranslate(const NiPoint3& position){};

	void SetCurTime(float fTime) {
		m_fCurTime = fTime;
	}
	float GetCurTime() const {
		return m_fCurTime;
	}
	void SetMaxLife(float MaxLife) 	{ 
		m_MaxLife = MaxLife; 
		PreCalcTimeLine();
	}
	float GetMaxLife() const { 
		return m_MaxLife;
	}
	bool IsActivated() const {
		return bool(m_fCurTime != -FLT_MAX);
	}
	bool IsVisible() {
		return m_bVisible;
	}
	void SetVisible(bool bValue) {
		m_bVisible = bValue;
	}

	void SetNext(CEffectBase* pkNext) {
		m_pkNext = pkNext;
	}

	CEffectBase* GetNext() {
		return m_pkNext;
	}
	void SetAutoDelete(bool bDelete) {
		m_bAutoDelete = bDelete;
	}
	void SetAutoRelease(bool bRelease ) {
		m_bNeedRelease = bRelease;
	}
	bool IsAutoDelete() {
		return m_bAutoDelete;
	}

	float GetEffScale(){ return m_fEffScale; }
	void  SetEffScale( float fScale ){ m_fEffScale = fScale; }

	bool GetIsNoAutoScale(){ return m_bNoAutoScal; }
	void SetIsNoAutoScale( bool fScale ){  m_bNoAutoScal = fScale; }

protected:
	void Destroy() {
		Detach();
		if (m_bRegesiter)
		{
			m_bRegesiter = false;
			UnRegister();
			return;
		}
		else
		{
			NiDelete this;
		}
	}

	virtual void Register(CEffectManageBase* pkManage);
	virtual void UnRegister();

	float m_MaxLife;
	float m_fCurTime;
	float m_fEndTime;
	float m_fStartTime;

	bool m_bVisible;
	bool m_bRegesiter;
	bool m_bAutoDelete;
	bool m_bNeedRelease;
	float m_fEffScale;
	bool m_bWithSender;
	bool m_bNoAutoScal;

	NiTObjectArray<EffectCallbackObjectPtr> m_kCallbacks;

	CEffectBase* m_pkNext;
	CEffectManageBase* m_pkManage;
};

