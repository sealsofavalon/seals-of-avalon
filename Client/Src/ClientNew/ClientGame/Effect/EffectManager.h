#pragma once
#include "Effect/NumberFont.h"
#include "Effect/CharacterName.h"
#include "Effect/ChatEffect.h"
#include "Effect/SceneEffect.h"
#include "Effect/Projectile.h"
#include "Effect/PostProcessEffect.h"
#include "Effect/TrailFX.h"

#include "EffectManageBase.h"
#include <hash_set>

class CBladeBlot;

#define  EffervescentTextColor 0xFFFF0000
enum ProjDescID
{
	PROJDESC_FIREBALL = 0,
	PROJDESC_BLOWSHOT = 1,
	PROJDESC_MAX,
};
struct sNameRecode
{
	ui32 type;
	void SetEmblem(uint32 style, uint32 color);
	uint32 emblemStyle;
	uint32 emblemColor;


	CharName stName;
};

#define SY_HUDTEXT_MAXLEN 128

class CEffectManager : public CEffectManageBase
{
	struct HUDText
	{
		UPoint Pos;
		UColor Color;

		float Life;
		float Age ;

		int StrLen;
		WCHAR wszText[SY_HUDTEXT_MAXLEN];
	};

public:
public:
	CEffectManager();
	~CEffectManager(void);

	BOOL Initialize();
	void Destroy();
	void Update(float fAccumTime, float fFrameTime);
	void RenderSceneEffects(const NiCamera* pkCamera);
	void RenderHudEffects(const NiCamera* pkCamera);		// 其实很多效果的渲染都会在场景渲染中完成, 这里只是进行一些额外的渲染.

	void RegisterEffect(CEffectBase* Effect);
	void ReleaseEffect(CEffectBase* Effect, bool bReleaseChild = false);
	void ReleaseAllEffects();
	void UpdateEffects(float fTimeLine, float fDeltaTime);
	
	//2D Elements
	void Render2DElements();
	void AddSceneText(const NiPoint3& pos, const char*  pcText, unsigned int uiColor, float fLife = -FLT_MAX);
	void SetChatPopoShow(BOOL vis);
	void AddChatText(const NiPoint3& pos, const char*  pcText, unsigned int uiColor, float fLife = -FLT_MAX);
	
	void AddEffeText(const NiPoint3& pos, const char* pText, UColor txtcolor = EffervescentTextColor);  //冒泡的文字
	void AddEffectText(const char* pText, int ColorIndex = 0);

	void SetCharNameShow(ui32 type, BOOL visible);

	void SetHostHpBarShow(bool visible);
	void SetHostHpBarColorful(bool visible);

	void AddCharName(sNameRecode* pNameRecode, const NiPoint3& pos, const char* pName, const char* ptitleName, const char* pGuildName, const char* prefixalName, unsigned int uiColor,
		float Distent, bool Show = false, bool bTarget = false);
	// pos : screen coord. .z : depth.
	void AddCComboAttackEffect(ui64 guid,CComboAttackEffect* pComboAttackEffect);
	void DeleteCComboAttackEffect(ui64 guid);
	void BubbleUpEffectNumber(CComboAttackEffect* pEffect,int Type,int SubType, int Number, 
		const NiColorA& InitCol, float fLife = 2.0f, const WCHAR* extraOutput = NULL , ui32 victimstate = 1); 


	//Post Effect
	void DeadEffect(bool bIsDead);
	void CreatePostEffect();
	void CleanupPostEffect();
	NiRenderTargetGroup* GetPostEffectRenderTarget();

	//Scene Effect
	CEffectBase* CreateSceneEffect(const char* EffectName, bool bAvtive=true, bool bVisible = false, bool infinity = true, 
		unsigned int loopcount = 1, bool bAttachToScene = false, bool bAutoDestroy = true);
	NiAVObjectPtr NewSceneEffectObject(const char* EffectName);
	void ReleaseAllSceneEffectTemplates();
	NiAVObjectPtr LoadSceneEffectTemplate(const NiFixedString& Name);
	NiAVObject* FindSceneEffectTemplate(const NiFixedString& Name);

protected:
	void RenderEffects(const NiCamera* pkCamera);

private:
	BOOL InitBubupNumberEffect();
	void ShutdownBubupNumberEffect();
	void UpdateHudTextEntry(float fDeltaTime);
	void UpdateEffervescentText(float fDeltaTime);
	void RenderHudText();
	void RenderEffervescentText();
protected:
	BOOL m_Enable;//  是否有效
	BOOL m_Inited;

	ui32 m_ShowCharName;

	bool m_IsDrawHpBar;
	bool m_IsDrawHpBarColorful;

	CCharacterName * m_CharacterName;
	CChatEffect*     m_ChatEffect;
	BOOL m_EnableComboAtcEffect;
	BOOL m_EnableChatTextEffect;
	
	//字体, 玩家在场景聊天里面输入的表情符号等2D元素 
	//CEGUI::RenderCache m_2DElements;
	UFontPtr	m_spHudFont;
	UFontPtr	m_spNBFont;
	PostProcessEffectPtr m_spPostEffect;

	FixedStringHashMap<NiAVObject*> m_SceneEffectMap;

	stdext::hash_set<CEffectBase*> m_EffectInstances;
	stdext::hash_set<CEffectBase*> m_SelfRenderList;

	std::list<HUDText*>		m_HUDText;
	std::list<HUDText*>		m_EffervescentText;
	std::map<ui64, CComboAttackEffect*> m_vComboAttackEffect;

	//test 
	float m_testEffectTime ;
};