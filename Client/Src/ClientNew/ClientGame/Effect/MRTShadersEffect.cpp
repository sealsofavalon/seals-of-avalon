
#include "StdAfx.h"

#include "MRTShadersEffect.h"

//---------------------------------------------------------------------------
bool MRTShadersEffect::CreateEffect()
{
	NiDX9Renderer* pkD3DRenderer = (NiDX9Renderer*)NiRenderer::GetRenderer();
	NIASSERT(pkD3DRenderer);


	HRESULT hr = pkD3DRenderer->GetDirect3D()->CheckDeviceFormat(
		pkD3DRenderer->GetAdapter(), pkD3DRenderer->GetDevType(),
		pkD3DRenderer->GetPresentParams()->BackBufferFormat,
		D3DUSAGE_RENDERTARGET, D3DRTYPE_TEXTURE, D3DFMT_A16B16G16R16F);
	if (FAILED(hr))
	{
		return false;
	}

	unsigned int uiWidth = pkD3DRenderer->GetDefaultRenderTargetGroup()->GetWidth(0);
	unsigned int uiHeight = pkD3DRenderer->GetDefaultRenderTargetGroup()->GetHeight(0);

	CreateRenderedTextures(uiWidth, uiHeight);

	// Create screen-filling render view.
	m_spScreenQuad = NiNew NiScreenFillingRenderView;
	NiTexturingProperty* pkTexProp = NiNew NiTexturingProperty();
	m_spScreenQuad->GetScreenFillingQuad().AttachProperty(pkTexProp);
	m_spScreenQuad->GetScreenFillingQuad().UpdateProperties();

	m_spViewRenderClick = NiNew NiViewRenderClick;
	m_spViewRenderClick->AppendRenderView(m_spScreenQuad);

	// Gaussian blur in X.
	m_spGaussBlurX = NiSingleShaderMaterial::Create("MRT_GaussBlurX");
	NIASSERT(m_spGaussBlurX);

	m_spGaussBlurY = NiSingleShaderMaterial::Create("MRT_GaussBlurY");
	NIASSERT(m_spGaussBlurY);

	return true;
}

//---------------------------------------------------------------------------
void MRTShadersEffect::CleanupEffect()
{
	CleanupRenderedTextures();
}

//---------------------------------------------------------------------------
void MRTShadersEffect::Render()
{
	NiDX9Renderer* pkRenderer = (NiDX9Renderer*)NiRenderer::GetRenderer();

	NiRenderTargetGroup* pkDefaultRenderTarget = pkRenderer->GetDefaultRenderTargetGroup();

	pkRenderer->EndUsingRenderTargetGroup();

	const unsigned int uiFrameID = pkRenderer->GetFrameID();
	NiTexture* pkReadTexture = m_spMRTColorsTexture;

	PrepareScreenQuad(pkReadTexture, m_spGaussBlurX);
	m_spViewRenderClick->SetRenderTargetGroup(m_spMRTRenderTarget);
	m_spViewRenderClick->Render(uiFrameID);

	pkRenderer->EndUsingRenderTargetGroup();

	PrepareScreenQuad(pkReadTexture, m_spGaussBlurY);
	m_spViewRenderClick->SetRenderTargetGroup(pkDefaultRenderTarget);
	m_spViewRenderClick->Render(uiFrameID);
}

//---------------------------------------------------------------------------
void MRTShadersEffect::CreateRenderedTextures(unsigned int uiWidth, unsigned int uiHeight)
{
	NiDX9Renderer* pkRenderer = (NiDX9Renderer*)NiRenderer::GetRenderer();
	NIASSERT(pkRenderer);

	NIASSERT((uiWidth % 8) == 0);
	NIASSERT((uiHeight % 8) == 0);

	NiTexture::FormatPrefs kPrefs;

	kPrefs.m_ePixelLayout = NiTexture::FormatPrefs::FLOAT_COLOR_64;

	// Create the multiple render target (MRT) textures.
	m_spMRTColorsTexture = NiRenderedTexture::Create(uiWidth, uiHeight, pkRenderer, kPrefs);
	NIASSERT(m_spMRTColorsTexture);

	// Create the MRT render target.
	m_spMRTRenderTarget = NiRenderTargetGroup::Create(1, pkRenderer);
	m_spMRTRenderTarget->AttachBuffer(m_spMRTColorsTexture->GetBuffer(), 0);
}
//---------------------------------------------------------------------------
void MRTShadersEffect::CleanupRenderedTextures()
{
	// Release D3D textures.
	NiRenderer* pkRenderer = NiRenderer::GetRenderer();
	pkRenderer->PurgeTexture(m_spMRTColorsTexture);

	// Release render targets
	m_spMRTRenderTarget = 0;
}
//---------------------------------------------------------------------------
void MRTShadersEffect::PrepareScreenQuad(NiTexture* pkColorsTexture, NiMaterial* pkMaterial)
{
	NIASSERT(pkColorsTexture && pkMaterial);

	NiTexturingProperty* pkTP = NiDynamicCast(NiTexturingProperty, 
		m_spScreenQuad->GetProperty(NiProperty::TEXTURING));
	NIASSERT(pkTP);

	NiTexturingProperty::ShaderMap* pkShaderMap = pkTP->GetShaderMap(0);
	if (pkShaderMap)
	{
		pkShaderMap->SetTexture(pkColorsTexture);
	}
	else
	{
		pkShaderMap = NiNew NiTexturingProperty::ShaderMap(
			pkColorsTexture, 0, NiTexturingProperty::CLAMP_S_CLAMP_T,
			NiTexturingProperty::FILTER_NEAREST, 0);
		NIASSERT(pkShaderMap);
		pkTP->SetShaderMap(0, pkShaderMap);
	}

	m_spScreenQuad->GetScreenFillingQuad().ApplyAndSetActiveMaterial(pkMaterial);
}