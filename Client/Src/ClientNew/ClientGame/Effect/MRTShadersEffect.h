#pragma once

#include "PostProcessEffect.h"

class MRTShadersEffect : public PostProcessEffect
{
public:
	MRTShadersEffect()
	{}

    virtual bool CreateEffect();
    virtual void CleanupEffect();

    virtual void Render();

private:
    void CreateRenderedTextures(unsigned int uiWidth, unsigned int uiHeight);
    void CleanupRenderedTextures();
    void CreateShaderTextures();

    void PrepareScreenQuad(NiTexture* pkColorsTexture, NiMaterial* pkMaterial);

    // Rendered textures.
    NiRenderedTexturePtr m_spMRTColorsTexture;

    // Render targets.
    NiRenderTargetGroupPtr m_spMRTRenderTarget;

    // Render Click
	NiViewRenderClickPtr m_spViewRenderClick;

	// Screen space quad.
    NiScreenFillingRenderViewPtr m_spScreenQuad;

	NiMaterialPtr m_spGaussBlurX;
	NiMaterialPtr m_spGaussBlurY;
    
    // Pass-through material.
    NiMaterialPtr m_spPassThroughMaterial;
};