#pragma once

#include "EffectBase.h"

enum
{
	ISTATE_NONE,
	ISTATE_MISS,
	ISTATE_BLOCK,
	ISTATE_CRICTICAL,
	ISTATE_ABSORB,
	ISTATE_RESIST,
	ISTATE_IMMUNE,
	ISTATE_DODGE
};
enum eNum
{
	NUM_0,
	NUM_1,
	NUM_2,
	NUM_3,
	NUM_4,
	NUM_5,
	NUM_6,
	NUM_7,
	NUM_8,
	NUM_9,
	NUM_REDUCE = 10,
	NUM_ADD = 11,
	NUM_MISS = 12,
	NUM_BLOCK = 13,
	NUM_ABSORB = 14,
	NUM_CRICTIAL = 15,
	NUM_RESIST = 16,
	NUM_IMMUNE = 17,
	NUM_DODGE = 18,
	NUM_X = 19,
};

class CNumberFont : public NiRefObject
{
	struct Vertex
	{
		float X,Y,Z,W;
		D3DCOLOR Color; 
		float u,v;
	};
	enum{
		MAX_VERTEX = 128,	//最大支持 63 个字符.
	};
public:
	static CNumberFont* Instance();
	static void Destory();
	BOOL Create(const char* BitmapFile);
	INT TextOut(INT X, INT Y, INT TextLen, const WCHAR* Text, DWORD ColorValue, UINT State = ISTATE_NONE, float Scale = 1.0f);
	float GetCharWidth() const { return m_CharWidth; }
private:
	CNumberFont(void);
	~CNumberFont(void);

	BOOL GetTextureUV(INT Number, NiRect<float>& UvRect);
	INT SetupCharVertex(INT Index, INT X, INT Y,INT CharHeight,INT CharWidth,DWORD Color, NiRect<float>& UvRect, float Scale);
	void PushDrawStatic();
	void PopDrawStatic();

	NiSourceTexturePtr m_Texture;
	void* m_DTexture;
	float m_CharWidth;
	float m_CharHeight;
	float m_Width;
	static Vertex sm_Vertics[MAX_VERTEX];	
	static CNumberFont* sm_NumberFont;
	bool m_bCreate;
};
struct AttackEffectEntry
{
	int         Number;
	bool	bNeedUpdatePosition;
	float       Scale;
	int         Type;
	ui32        SubType;
	ui32		victimstate;
	UColor	    Color;

	float		Age;
	float		Life;

	NiPoint3  BubbleUpPos_Init;
	NiPoint3  BubbleUpPos_Cal;

	BYTE		StrLen;
	WCHAR		szNbStr[64];//以宽字符串形式输入
	UINT iState;
};
class CComboAttackEffect : public CEffectBase
{
public:
	CComboAttackEffect();
	virtual ~CComboAttackEffect();
public:
	inline void SetOwner(class CCharacter* pchar){m_pComboCharacter = pchar;}
	inline bool IsNeedUpdate(){return m_AttackEffectEntrys.size() != 0;}
	BOOL Initialise();
	void Destroy();
	virtual void UpdateEffect(float fTimeLine, float fDeltaTime);
	void RenderEffect(const NiCamera* pkCamera);
	void AddEntry(AttackEffectEntry* NewEntry);
	void CalculatePosition(AttackEffectEntry* temp);
protected:
	bool m_bLocalPlayer;
	std::list<AttackEffectEntry*> m_AttackEffectEntrys;
	class CCharacter* m_pComboCharacter;
	const NiCamera* m_ViewCamera;
};