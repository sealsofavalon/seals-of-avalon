#include "StdAfx.h"
#include "OverlapEffect.h"
#include "Player.h"
#include "ObjectManager.h"

COverlapEffect::COverlapEffect(void)
{
}

COverlapEffect::~COverlapEffect(void)
{
}

BOOL COverlapEffect::UnProject(const NiPoint3& WorldPoint, POINT& ScreenPt)
{
	CPlayer* LocPlayer = ObjectMgr->GetLocalPlayer();
	if (LocPlayer)
	{
		const NiCamera& Cam = LocPlayer->GetNiCamera();
		float X;
		float Y;
		if (Cam.WorldPtToScreenPt(WorldPoint, X, Y))
		{

			ScreenPt.x = (LONG)X;
			ScreenPt.y = (LONG)Y;
			return TRUE;
		}
	}else
	{

	}
	return FALSE;
}

void COverlapEffect::SwithToOrthoMode()
{
	NiDX9Renderer* Render = (NiDX9Renderer*)SYState()->Render;
	NIASSERT(Render);

}