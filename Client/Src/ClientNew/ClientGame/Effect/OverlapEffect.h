#pragma once

#include "EffectBase.h"

// 重叠于场景之上的特效. 不参与任何遮挡
class COverlapEffect : public CEffectBase
{
public:
	COverlapEffect(void);
	virtual ~COverlapEffect(void);
	// World Space unproject to screen space.
	virtual BOOL UnProject(const NiPoint3& WorldPoint, POINT& ScreenPt);
	virtual BOOL SelfManage() { return TRUE;}

	static void SwithToOrthoMode();
};
