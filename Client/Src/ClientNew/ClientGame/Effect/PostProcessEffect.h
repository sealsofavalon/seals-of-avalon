#pragma once

#define MAX_SAMPLES 16

#define WOW_BLOOM 

class PostProcessEffect : public NiRefObject
{
public:
    PostProcessEffect();
    virtual ~PostProcessEffect();

	void Render();

	NiRenderTargetGroup* GetRenderTarget() const;
	void DeadEffect( bool bDeadEffect );

#ifdef WOW_BLOOM
	class Point4
	{
	public:
		Point4(float X, float Y, float Z, float W) : 
		  x(X), y(Y), z(Z), w(W) { /* */ }
		  Point4() { /**/ }
		  float x, y, z, w;
	};
#endif

private:
	void Init();
	void Release();

	void UpdateShaderConstant();
	void PrepareRenderClick();

#ifdef WOW_BLOOM
	void GetSampleOffsets_DownScale4x4(unsigned int uiWidth, 
		unsigned int uiHeight, NiPoint2 akSampleOffsets[]);

	void CreateWowBloom();

	NiShaderPtr				m_spBloomSmoothShader;
	NiShaderPtr				m_spBloomBigSmoothShader;
	NiShaderPtr				m_spBloomFinalShader;

	NiScreenFillingRenderView* m_pFinalView;

	NiRenderedTexturePtr	m_pBBTexture;
	NiRenderTargetGroupPtr	m_pBBRTG;

	NiRenderedTexturePtr	m_spSmoothTexture;
	NiRenderTargetGroupPtr	m_spSmoothRenderTargetGroup;

	NiTexturingPropertyPtr	m_spTexProp;


	NiRenderedTexturePtr	m_spSmoothBigTexture;
	NiRenderTargetGroupPtr	m_spSmoothBigRenderTargetGroup;

	NiPoint2				m_akSampleOffsets[MAX_SAMPLES];
	Point4					m_akSampleWeights[MAX_SAMPLES];
#else
	void CreateBloomThresholdRC();
	void CreateGaussianBlurHRC();
	void CreateGaussianBlurVRC();
	void CreateBloomCombineRC();

	NiRenderedTexturePtr m_pTempTexture1;
	NiRenderTargetGroupPtr m_pTempRTG1;

	NiRenderedTexturePtr m_pTempTexture2;
	NiRenderTargetGroupPtr m_pTempRTG2;
#endif

	NiDefaultClickRenderStepPtr m_pRenderStep;
};

NiSmartPointer(PostProcessEffect);