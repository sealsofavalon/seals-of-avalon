// P.K.
#pragma once
#include "EffectBase.h"
#include "SpelLEffect.h"

// data-driven - desc
struct PROJECTILE_DESC
{
	char	ProjectileModel[MAX_PATH];		// 发射模型.
	char	ExplosionModel[MAX_PATH];
	char    ExpSound[MAX_PATH];
	float	Scale;							// 缩放
//	INT		MaxLife;						// 生命期
	BOOL	Billboard;						// 面向镜头
	BOOL	MakeTrail;
	bool	bSelfMissile;					// 自导
	int		iProjectileType;
	float	fFlySpeed;						// 飞行速度
	float	fSelfMissileTime;				// 自导开始时间 [0, 1]
	bool	bNeedTarget;
	float	ExpScale;
};

class CGameObject;
class TrailEffect;

// 发射物体基类, 不处理加速度. 角速度, 在碰撞时刻死亡和自然死亡.
// 由于我们的游戏设定,发射的物体都是锁定目标并在指定时刻爆炸,
// 吟唱取消碰撞,
class ProjectileEffect : public CEffectBase //: public NiNode
{
public:
	ProjectileEffect(void);
	virtual ~ProjectileEffect(void);
	virtual BOOL Create(const PROJECTILE_DESC* EntityDesc, const NiPoint3& StartPos = NiPoint3::ZERO);

	void ActivateEffect(float fTime);

	virtual void UpdateEffect(float fTimeLine, float fDeltaTime);
	virtual void RenderEffect(const NiCamera* pkCamera);

	void CalcProjectileInfo(const NiPoint3& initPos, const NiPoint3& targetPos);
	bool GetTargetHitPosition(CGameObject* pkObject, NiPoint3& hitPos, bool bMiss = false);
	bool GetTargetHitPosition(SYObjID objectID, NiPoint3& hitPos, bool bMiss = false);

	float ProcessCurTransform(NiTransform& kTransform, float fT);
	void ProcessSelfMissile(float fDeltaT, const NiPoint3& curPos, const NiMatrix3& kCurRotation, CGameObject* pkObject, NiTransform& kTransform);
	bool CheckCollision(const NiPoint3& oldPos, const NiPoint3& newPos, const NiPoint3& targetPos);

	void AddEP(const SpellEffectEmit::EmitParam& ep);

	virtual void UnRegister();

	virtual BOOL SelfManage() { return FALSE;}
	virtual BOOL SelfRender() const { return TRUE; }

	inline NiNode* GetNode() { return m_ProjectileModel;}
	bool TrackObject(SYObjID TargetID, bool bMiss = false);
	bool TrackTarget(NiPoint3& kTarget, bool bMiss = false);

protected:
	virtual void OnCollison(float fTime);

protected:	
	NiNodePtr	m_ProjectileModel;

	char		m_ExplorsionModel[MAX_PATH];
	char		m_ExpSound[MAX_PATH];
	float		m_MaxLife;

	SYObjID    m_TargetObject;

	UINT		m_MakeTrail:1;
	UINT		m_Billboard:1;
	UINT		m_bTrackObject:1;
	UINT		m_bExploded;

	NiPoint3	m_OldPos;		
	NiMatrix3	m_kOldRotation;
	NiPoint3	m_CurVelocity;

	NiPoint3	m_kPath;
	float		m_fVelocity;
	float		m_fExpScale;
	NiPoint3    m_InitPos;
	NiPoint3	m_TargetPos;
	bool		m_bSelfMissle;
	bool		m_bNeedTarget;
	int			m_iProjectileType;
	float		m_fProjectileT;
	bool		m_bIsSelfMissle;
	float		m_fSelfMisiileTime;
	TrailEffect* m_pkTrail;

	bool m_bTriggerEP;
	SpellEffectEmit::EmitParam m_triggerEP;

	NiPropertyState* m_pkPS;
};
