#include "StdAfx.h"
#include "SceneEffect.h"
#include "GameObject.h"
#include "Scenemanager.h"
#include "ObjectManager.h"
#include "ResourceManager.h"


#include "Console.h"

CSceneEffect::CSceneEffect(NiAVObject* EffectObj) : m_bDetachUnbind(true), m_fSoundDelayTime( 0.f ),m_pEffectSound( NULL )
{
	m_spEffect = EffectObj;
	NIASSERT(m_spEffect);

	m_bWorld = false;

	m_Parent = SY_INVALID_OBJID;
	m_AttachToScene = FALSE;

	m_kOffset.MakeIdentity();
	m_fEffScale = 1.f;
}

CSceneEffect::~CSceneEffect(void)
{

	DettachFromParent();

	g_ResMgr->FreeNif(m_spEffect);
	m_spEffect = NULL;
}

void CSceneEffect::AttachToSceneObject(SYObjID ParentID, const NiFixedString* pParentName /* = NULL */, bool bWorld /* = false */, bool bDetachUnbind /* = true */, float scale /* = 1.0f */ )
{
	NIASSERT(m_spEffect);
	Detach();
	CGameObject* Obj = (CGameObject*)ObjectMgr->GetObject(ParentID);
	if (Obj)
	{
		m_Parent = ParentID;
		NiNodePtr ParentNode = Obj->GetSceneNode();

		if (ParentNode)
		{
			if (pParentName == NULL)
			{
				m_spParentNode = ParentNode;
			}
			else
			{
				NiAVObject* pSlot = ParentNode->GetObjectByName(*pParentName);
				m_spParentNode = NiDynamicCast(NiNode, pSlot);
				if (!m_spParentNode)
				{
					NILOG("ERROR can't find the slot node %s ", *pParentName);
					m_spParentNode = ParentNode;
				}
			}
		}
	}

	if (m_spParentNode && bWorld)
	{
		const NiTransform& kTransform = m_spParentNode->GetWorldTransform();
		m_spEffect->SetLocalTransform(kTransform);
		m_spParentNode = 0;

		m_bWorld = bWorld;
		m_bDetachUnbind = false;
	}
	else
	{
		m_bDetachUnbind = bDetachUnbind;
	}

	m_spEffect->Update(0.0f, false);
	m_spEffect->UpdateProperties();
	m_spEffect->UpdateEffects();
	m_spEffect->UpdateNodeBound();
	if ( scale != 1.0f )
	{
		m_spEffect->SetScale( scale );
	}
}

void CSceneEffect::AttachToScene()
{
	if (!m_AttachToScene) {
		m_AttachToScene = TRUE;
	}
}

void CSceneEffect::AttachToScene( const NiPoint3& Position, SYObjID IdSender /* = 0 */, bool bLivewithSender /* = false */ )
{
	NIASSERT(m_spEffect);
	if (m_Parent != SY_INVALID_OBJID)
	{
		DettachFromParent();
	}

	m_idSender =  IdSender;
	m_bWithSender = bLivewithSender;
	SetVisible(true);

	m_spEffect->SetTranslate(Position);
	m_spEffect->Update(0.0f, false);
	m_spEffect->UpdateEffects();
	m_spEffect->UpdateNodeBound();
	m_AttachToScene = TRUE;
}

void CSceneEffect::DettachFromParent()
{
	if (m_Parent != SY_INVALID_OBJID && m_spEffect)
	{
		m_spParentNode = NULL;
		m_Parent = SY_INVALID_OBJID;
	}
}

void CSceneEffect::DettachFromScene()
{
	if (m_AttachToScene)
	{
		SetVisible(false);
		m_AttachToScene = FALSE;
	}
}

void CSceneEffect::Detach()
{
	if (m_AttachToScene)
	{
		DettachFromScene();
	}else if (m_Parent != SY_INVALID_OBJID)
	{
		DettachFromParent();
	}
}

void CSceneEffect::ActivateEffect(float fTime) {
	NIASSERT(m_spEffect);
	if (m_spEffect)
		NiTimeController::StartAnimations(m_spEffect, fTime);
}

void CSceneEffect::RenderEffect(const NiCamera* pkCamera)
{
	SceneMgr->RenderObject(m_spEffect);
}

void CSceneEffect::UpdateEffect(float fTimeLine, float fDeltaTime)
{
	if ( m_Parent != 0 )
	{
		CGameObject* pkObj = (CGameObject*)ObjectMgr->GetObject( m_Parent );
		if (!pkObj)
		{
			Deactivate();
			m_pEffectSound = NULL;
			return;
		}
		else
		{
			if (m_spParentNode)
			{	
				NiTransform kT;
				const NiTransform& kTransform = m_spParentNode->GetWorldTransform();
				kT = kTransform * m_kOffset;
				if ( m_spEffect)
				{
					float fScale = m_fEffScale*kT.m_fScale;
					if ( m_bNoAutoScal )
					{
						fScale = m_fEffScale;
					}
					m_spEffect->SetLocalTransform(kT);
					m_spEffect->SetScale( fScale );
				}
			}
			else
			{
				if ( m_spEffect)
				{
					m_spEffect->SetScale( m_fEffScale );
				}
			}
		}
	}
	else
	{
		if ( m_spEffect)
		{
			m_spEffect->SetScale( m_fEffScale );
		}
	}	

	if (g_pkConsole)
		g_pkConsole->Printf(75, "%f\n", GetElapsed());

	if (m_spEffect)
	{
		if ( m_bWithSender )
		{
			CGameObject* pkSender = (CGameObject*)ObjectMgr->GetObject( m_idSender );
			if ( !pkSender )
			{
				Deactivate();
				m_pEffectSound = NULL;
				return;
			}
		}

		if ( (m_fCurTime - m_fStartTime) >= m_fSoundDelayTime )
		{
			if ( !m_pEffectSound && m_strSoundFile.length() )
			{
				m_pEffectSound = (ALAudioSource*)AudioSystem::GetAudioSystem()->CreateSource(AudioSource::TYPE_3D);
				if (!m_pEffectSound)
					return;

				GetEffectNode()->AttachChild(m_pEffectSound);
				GetEffectNode()->Update(0.0f, false);

				m_pEffectSound->SetMinMaxDistance(5.0f, 80.0f);
				m_pEffectSound->SetGain(2.4f);
				m_pEffectSound->SetLoopCount( 1 );
				m_pEffectSound->SetSelectiveUpdate(true);
				m_pEffectSound->SetSelectiveUpdateTransforms(true);
				m_pEffectSound->SetSelectiveUpdatePropertyControllers(false);
				m_pEffectSound->SetSelectiveUpdateRigid(true);
				m_pEffectSound->Update(0.0f);

				SYState()->IAudio->PlaySound(m_pEffectSound, m_strSoundFile.c_str());
			}
		}
		
		m_spEffect->Update(GetElapsed());
	}
	else
		m_pEffectSound = NULL;

	if (m_bDetachUnbind && m_Parent != SY_INVALID_OBJID && !m_bWorld)
	{
		CGameObject* Obj = (CGameObject*)ObjectMgr->GetObject(m_Parent);
		if (Obj == NULL)
		{
			Deactivate();
			m_pEffectSound = NULL;
		}
	}
}

void CSceneEffect::SetCycleType_Loop()
{
	CGameObject::SetController(m_spEffect, NiTimeController::APP_TIME, NiTimeController::LOOP);
}

void CSceneEffect::SetCycleType_Clamp()
{
	CGameObject::SetController(m_spEffect, NiTimeController::APP_TIME, NiTimeController::CLAMP);
}

void CSceneEffect::SetMaxLifeByEffectLife(INT LoopCnt)
{
	if (m_spEffect)
	{
		float MaxTime = 0.0f;
		NiTimeController* MaxTimeCtrl = NULL;
		CGameObject::GetMaxAnimTime(m_spEffect, MaxTime, &MaxTimeCtrl);
		if (MaxTime > 0.0f)
			SetMaxLife(MaxTime*LoopCnt);
	}
}

void CSceneEffect::SetTranslate(const NiPoint3& position)
{
	if (m_spEffect) {
		m_spEffect->SetTranslate(position);
	}
}

const NiPoint3& CSceneEffect::GetTranslate()
{
	if ( m_spEffect )
	{
		return m_spEffect->GetTranslate();
	}

	return NiPoint3::UNIT_X;
}

void CSceneEffect::SetOffset(const NiTransform& kOffset)
{
	m_kOffset = kOffset;
}


void CSceneEffect::SetEffectSound( const char* szSoundFile, int DelayTime )
{
	m_pEffectSound = NULL;
	m_strSoundFile = szSoundFile;

	m_fSoundDelayTime = (float)DelayTime/ 1000;
}
