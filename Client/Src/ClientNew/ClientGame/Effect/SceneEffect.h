#pragma once
#include "EffectBase.h"
#include "GameObject.h"
#include "AudioInterface.h"
// 处于场景中的, 参与遮挡, 排序 的特效, 如果有附加于一个指定对象, 则跟随对象运动.


class CSceneEffect : public CEffectBase //, public CGameObject
{
	friend class CEffectManager;
public:
	CSceneEffect(NiAVObject* EffectObj);
	virtual ~CSceneEffect(void);

	virtual void UpdateEffect(float fTimeLine, float fDeltaTime);
	virtual void RenderEffect(const NiCamera* pkCamera);

	virtual BOOL SelfManage() { return FALSE;}
	virtual BOOL SelfRender() const { return TRUE; }

	// 根据特效动画区间设置生命最大值.
	void SetCycleType_Loop();
	void SetCycleType_Clamp();
	void SetMaxLifeByEffectLife(INT LoopCnt = 1);
	void AttachToScene();
	virtual void AttachToScene(const NiPoint3& Position, SYObjID IdSender = 0, bool bLivewithSender = false );
	void AttachToSceneObject(SYObjID ParentID,  const NiFixedString* pParentName = NULL, bool bWorld = false, bool bDetachUnbind = true, float scale = 1.0f );
	void Detach();

	void ActivateEffect(float fTime);

	NiAVObject* GetAVObject() { return m_spEffect;}
	void SetTranslate(const NiPoint3& position);
	const NiPoint3& GetTranslate();
	void SetOffset(const NiTransform& kOffset);

	NiNode* GetEffectNode() {
		return NiSmartPointerCast(NiNode, m_spEffect);
	}

	void SetEffectSound( const char* szSoundFile, int DelayTime );

protected:
	void DettachFromParent();
	void DettachFromScene();
	SYObjID m_Parent;		// 附加对象.
	SYObjID m_idSender;
	NiAVObjectPtr m_spEffect;
	NiNodePtr m_spParentNode;
	BOOL m_AttachToScene;
	bool m_bWorld;
	bool m_bDetachUnbind;

	std::string m_strSoundFile;
	ALAudioSourcePtr m_pEffectSound;
	float m_fSoundDelayTime;
	NiTransform m_kOffset;
};
