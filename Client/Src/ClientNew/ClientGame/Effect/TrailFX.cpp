#include "StdAfx.h"
#include "TrailFX.h"

namespace
{
#define NUM_SCANLINE 75
	class Swoosh : public NiMemObject
	{
	public:
		Swoosh(NiNode* pkScene, NiAVObject* pkTop, float fInitialTime, float Length, const char* TexName);
		void TakeSample(float fTime);
	protected:
		NiPoint3 m_kVertices[NUM_SCANLINE * 2];
		float m_afVelocityScales[NUM_SCANLINE * 2];
		NiPoint2 m_kTexCoords[NUM_SCANLINE * 2];
		float m_Length;
		int m_iStartingIndex;
		int m_NumScanLines;

		NiTriShape* m_pkTriShape;
		NiAVObject* m_pkTopObject;

		float m_fLastTime;
		NiPoint3 m_kLastPoint;
		float m_fVelocityMinimum;
		float m_fVelocityMaximum;
	};


	Swoosh::Swoosh(NiNode* pkScene, NiAVObject* pkTop, float fInitialTime, float Length, const char* TexName)
	{
		//Establish base values for the swoosh.  We make these protected
		//member variables so we can add methods later to change them 
		//dynamically.
		m_fVelocityMinimum = 25.0f;
		m_fVelocityMaximum = 800.0f;

		//Create the arrays we'll need for the NiTriShapeDynamicData object
		NiPoint3* pkVerts = NiNew NiPoint3[NUM_SCANLINE * 2];
		NiPoint3* pkNormals = NiNew NiPoint3[NUM_SCANLINE * 2];
		NiPoint2* pkTexCoords = NiNew NiPoint2[NUM_SCANLINE * 2];
		NiColorA* pkColors = NiNew NiColorA[NUM_SCANLINE * 2];
		unsigned short* pkConnect = NiAlloc(unsigned short, NUM_SCANLINE * 2 * 3);

		//Initialize everything.  We set the UVs to their actual values as they
		//will remain constant over time.  This gives the swoosh the sense of 
		//hanging in the air.  We set the velocity scale on everything to 0.0f
		//though so the swoosh starts out invisible.
		float fUStep = 1.0f / NUM_SCANLINE;
		int i = 0;
		for (; i < NUM_SCANLINE * 2; i++)
		{
			pkVerts[i] = pkTop->GetWorldTranslate();
			pkNormals[i] = NiPoint3(1.0f, 0.0f, 0.0f);

			float fU = fUStep * (i / 2);
			float fV = 1.0f * (i % 2);
			pkTexCoords[i] = NiPoint2(fU, fV);
			m_kTexCoords[i] = NiPoint2(fU, fV);

			m_afVelocityScales[i] = 0.0f;

			pkColors[i] = NiColorA(1.0f, 1.0f, 1.0f, 1.0f - (i/2) * fUStep);
		}

		//Esatblish the connectivity array.  This will not change.
		for (i = 0; i < NUM_SCANLINE; i++)
		{
			pkConnect[6 * i] = 2 * i;
			pkConnect[6 * i + 1] = 2 * i + 1;
			pkConnect[6 * i + 2] = 2 * i + 2;
			pkConnect[6 * i + 3] = 2 * i + 1;
			pkConnect[6 * i + 4] = 2 * i + 3;
			pkConnect[6 * i + 5] = 2 * i + 2;
		}

		//Seed the system with one quad.
		m_iStartingIndex = 0;
		m_NumScanLines = 1;

		m_kVertices[m_iStartingIndex] = pkTop->GetWorldTranslate();
		m_kLastPoint = pkTop->GetWorldTranslate();
		m_kVertices[m_iStartingIndex + 1] = (pkTop->GetWorldRotate() * 
			NiPoint3(0.0f, 0.0f, m_Length)) + pkTop->GetWorldTranslate();

		NiTriShapeDynamicData* pkData = NiNew NiTriShapeDynamicData(
			NUM_SCANLINE * 2, pkVerts, pkNormals, 
			pkColors, pkTexCoords, 1, NiGeometryData::NBT_METHOD_NONE, 
			NUM_SCANLINE * 2 - 2, pkConnect, 0, 0);
		pkData->SetConsistency(NiGeometryData::VOLATILE);

		//Set a fake bound that insures rendering to begin with.
		NiBound kBound;
		kBound.SetCenter(0.0f, 0.0f, 0.0f);
		kBound.SetRadius(10000.0f);
		pkData->SetBound(kBound);

		m_pkTriShape = NiNew NiTriShape(pkData);

		m_pkTriShape->SetName("The Swoosh");
		pkScene->AttachChild(m_pkTriShape);

		//The shader takes care of all the necessary properties like making it
		//double sided, etc...
		NiMaterial* pkMaterial = NiSingleShaderMaterial::Create("Swoosh");
		m_pkTriShape->ApplyAndSetActiveMaterial(pkMaterial);

		//The shader depends on a base texture being there.
		NiTexturingProperty* pkTex = NiNew NiTexturingProperty(TexName);
		m_pkTriShape->AttachProperty(pkTex);

		m_pkTopObject = pkTop;
		m_fLastTime = fInitialTime - 0.001f;

		//Take the rist sample to get the swoosh started.
		TakeSample(fInitialTime);

		m_pkTriShape->Update(0.0f);
		m_pkTriShape->UpdateProperties();
		m_pkTriShape->UpdateEffects();

	}
	//---------------------------------------------------------------------------
	void Swoosh::TakeSample(float fTime)
	{
		//Establish iWriteIndex so we can insert into a circular queue.  Thus,
		//as points age out of the swoosh they are overwritten.
		int iWriteIndex = 0;
		if (m_NumScanLines == NUM_SCANLINE)
		{
			m_iStartingIndex -= 2;
			if (m_iStartingIndex < 0)
				m_iStartingIndex += NUM_SCANLINE * 2;
			iWriteIndex = m_iStartingIndex;
		}
		else
		{
			iWriteIndex = m_iStartingIndex + (2 * m_NumScanLines);
			m_NumScanLines++;
		}

		//The swoosh points are the staff and the point -70 on the staff's
		//local y axis which we then rotate into world space.  All swoosh 
		//geometry is in world space.
		m_kVertices[iWriteIndex] = m_pkTopObject->GetWorldTranslate();
		m_kVertices[iWriteIndex + 1] = (m_pkTopObject->GetWorldRotate() * 
			NiPoint3(0.0f, 0.0f, m_Length)) + m_pkTopObject->GetWorldTranslate();

		NiPoint3 kPoint = m_pkTopObject->GetWorldTranslate();
		kPoint -= m_kLastPoint;
		float fDist = NiSqrt(
			kPoint.x * kPoint.x + kPoint.y * kPoint.y + kPoint.z * kPoint.z);

		float fDeltaTime = fTime - m_fLastTime;
		if (fDeltaTime < 0.001f)
		{
			fDist = 0.0f;
			fDeltaTime = 1.0f;
		}

		fDist /= fDeltaTime;

		if (fDist > m_fVelocityMaximum)
		{
			fDist = 1.0f;
		}
		else if (fDist < m_fVelocityMinimum)
		{
			fDist = 0.0f;
		}
		else
		{
			fDist = (fDist - m_fVelocityMinimum) / (m_fVelocityMaximum - 
				m_fVelocityMinimum);
		}

		//Calculate the velocity and lerp between 0 and 1 for the velocity
		//scale.  This means the swoosh only appears when the staff moves
		//quickly.
		m_afVelocityScales[iWriteIndex] = fDist;
		m_afVelocityScales[iWriteIndex + 1] = fDist;

		m_fLastTime = fTime;
		m_kLastPoint = m_pkTopObject->GetWorldTranslate();

		NiPoint3* pkVerts = m_pkTriShape->GetVertices();
		NiPoint2* pkTextures = m_pkTriShape->GetTextures();
		NiColorA* pkColors = m_pkTriShape->GetColors();

		//Rebuild the data for the renderable object from the values we have
		//stored in the class members.
		float fColorStep = 1.0f / NUM_SCANLINE;
		for (int i = 0; i < m_NumScanLines * 2; i++)
		{
			pkVerts[i] = m_kVertices[(i + m_iStartingIndex) % (NUM_SCANLINE * 2)];
			pkTextures[i] = m_kTexCoords[(i + m_iStartingIndex) % (NUM_SCANLINE * 2)];
			pkColors[i] = (NiColorA(1.0f, 1.0f, 1.0f, (1.0f - (i/2) * fColorStep))
				* m_afVelocityScales[(i + m_iStartingIndex) % (NUM_SCANLINE * 2)]);

			if ((i + m_iStartingIndex) == NUM_SCANLINE * 2 ||
				(i + m_iStartingIndex) == NUM_SCANLINE * 2 + 1)
			{
				pkVerts[i] = m_kVertices[i + m_iStartingIndex - 2];
			}
		}

		//Call MarkAsChanged to inform the renderer to repack.
		NiTriShapeDynamicData* pkData = (NiTriShapeDynamicData*)(m_pkTriShape->
			GetModelData());
		pkData->SetActiveTriangleCount(m_NumScanLines * 2 - 2);
		pkData->SetActiveVertexCount(m_NumScanLines * 2);
		pkData->MarkAsChanged(NiGeometryData::VERTEX_MASK |
			NiGeometryData::TEXTURE_MASK | NiGeometryData::COLOR_MASK);

		//Update the bound for proper sorting and culling.
		NiBound kBound;
		kBound.ComputeFromData(m_NumScanLines * 2, pkVerts);
		m_pkTriShape->SetModelBound(kBound);
		m_pkTriShape->Update(0.0f);
	}

}

TrailFX::TrailFX(void)
{
	m_Geom = NULL;
	m_Dead = FALSE;
}

TrailFX::~TrailFX(void)
{
	if (m_Geom)
	{
		Swoosh* Tail = (Swoosh*)m_Geom;
		NiDelete Tail;
	}
}

void TrailFX::Update(DWORD DeltaTime)
{

}

BOOL TrailFX::Create(NiNode* Root, NiAVObject* TraceTarget, float Length, const char* TexName)
{
	return TRUE;
}