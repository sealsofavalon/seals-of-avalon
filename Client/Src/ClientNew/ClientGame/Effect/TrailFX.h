#pragma once

#include "EffectBase.h"

class TrailFX : public CEffectBase
{
public:
	TrailFX(void);
	~TrailFX(void);
// overload
	virtual void Update(DWORD DeltaTime);
	virtual BOOL SelfManage() { return FALSE;}
	virtual BOOL IsDead() const { return m_Dead; }
	virtual void Destroy() { m_Dead = TRUE; }
// interface
	BOOL Create(NiNode* Root, NiAVObject* TraceTarget, float Length, const char* TexName);
protected:
	HANDLE m_Geom;
	BOOL m_Dead;
};
