
#include "StdAfx.h"
#include "WaterEffect.h"

#include "SceneManager.h"
#include "ClientState.h"
#include "ClientApp.h"

#include "DynamicSimpleMesh.h"


NiZBufferProperty* CWaterEffect::ms_pkZBufferProperty;
NiAlphaProperty* CWaterEffect::ms_pkAlpha;
NiTexturingProperty* CWaterEffect::ms_pkWakeTextringProp;
NiTexturingProperty* CWaterEffect::ms_pkSplashTextringProp;

void CWaterEffect::_SDMInit()
{
	ms_pkZBufferProperty = NiNew NiZBufferProperty;
	ms_pkZBufferProperty->IncRefCount();
	ms_pkZBufferProperty->SetZBufferWrite(false);
	ms_pkZBufferProperty->SetZBufferTest(true);

	ms_pkAlpha = NiNew NiAlphaProperty;
	ms_pkAlpha->IncRefCount();
	ms_pkAlpha->SetAlphaBlending(true);
	ms_pkAlpha->SetSrcBlendMode(NiAlphaProperty::ALPHA_SRCALPHA);
	ms_pkAlpha->SetDestBlendMode(NiAlphaProperty::ALPHA_ONE);

	ms_pkWakeTextringProp = NiNew NiTexturingProperty;
	ms_pkWakeTextringProp->IncRefCount();
	ms_pkWakeTextringProp->SetBaseTexture(NiSourceTexture::Create("Texture\\Effect\\wake.dds"));

	ms_pkSplashTextringProp = NiNew NiTexturingProperty;
	ms_pkSplashTextringProp->IncRefCount();
	ms_pkSplashTextringProp->SetBaseTexture(NiSourceTexture::Create("Texture\\Effect\\splash.dds"));
}

void CWaterEffect::_SDMShutdown()
{
	ms_pkZBufferProperty->DecRefCount();
	ms_pkAlpha->DecRefCount();
	ms_pkWakeTextringProp->DecRefCount();
	ms_pkSplashTextringProp->DecRefCount();
}

CWaterEffect::CWaterEffect(const NiPoint3& kPos, const NiMatrix3& kRotate, EffectType eType)
	:m_eType(eType)
{
	NiPoint3* pkVertex = NiNew NiPoint3[4];
    NiPoint2* pkTexC = NiNew NiPoint2[4];
    unsigned short* pusConnect = NiAlloc(unsigned short, 2 * 3);
    NiTriShapeDynamicData* pkTriData = NiNew NiTriShapeDynamicData(4, pkVertex, 0, 0, pkTexC, 1, 
        NiGeometryData::NBT_METHOD_NONE, 2, pusConnect);

    m_spGeometry = NiNew NiTriShape(pkTriData);
    m_spGeometry->SetActiveVertexCount(4);
    m_spGeometry->SetActiveTriangleCount(2);

	pkTexC[0] = NiPoint2(0.f, 1.f);
	pkTexC[1] = NiPoint2(1.f, 1.f);
	pkTexC[2] = NiPoint2(1.f, 0.f);
	pkTexC[3] = NiPoint2(0.f, 0.f);

	pusConnect[0] = 0;
	pusConnect[1] = 1;
	pusConnect[2] = 2;

	pusConnect[3] = 2;
	pusConnect[4] = 3;
	pusConnect[5] = 0;

	m_spGeometry->AttachProperty(ms_pkZBufferProperty);
	m_spGeometry->AttachProperty(ms_pkAlpha);
	if (eType == ET_WAKE)
		m_spGeometry->AttachProperty(ms_pkWakeTextringProp);
	else
		m_spGeometry->AttachProperty(ms_pkSplashTextringProp);

	NiMaterialProperty* pkMaterial = NiNew NiMaterialProperty;
	pkMaterial->SetAlpha(1.0f);
	pkMaterial->SetDiffuseColor(NiColor::BLACK);
	pkMaterial->SetEmittance(NiColor::WHITE);
	m_spGeometry->AttachProperty(pkMaterial);

	m_spGeometry->SetTranslate(kPos);
	m_spGeometry->SetRotate(kRotate);

	m_spGeometry->UpdateProperties();
	m_spGeometry->Update(0.f, false);
}

void CWaterEffect::AddEffect(const NiPoint3& kPos, const NiMatrix3& kRotate, EffectType eType)
{
	float fCurTime = SYState()->ClientApp->GetAccumTime();

	CWaterEffect* pkWaterEffect = NiNew CWaterEffect(kPos, kRotate, eType);

	if( eType == ET_WAKE )
		pkWaterEffect->SetMaxLife(MAX_WAKE_LIFE);
	else
		pkWaterEffect->SetMaxLife(MAX_SPLASH_LIFE);

	pkWaterEffect->Activate(fCurTime, true);
}

void CWaterEffect::RenderEffect(const NiCamera* pkCamera)
{
	float r = GetElapsed() / m_MaxLife;
	float fSize;
	NiPoint3* pkVertices = m_spGeometry->GetVertices();
	if( m_eType == ET_WAKE )
	{
		fSize = 0.5f + 1.3f * r;
		pkVertices[0] = NiPoint3(-fSize, 0.f, 0.f);
		pkVertices[1] = NiPoint3(fSize, 0.f, 0.f);
		pkVertices[2] = NiPoint3(fSize, fSize*2.0f, 0.f);
		pkVertices[3] = NiPoint3(-fSize, fSize*2.0f, 0.f);
	}
	else
	{
		fSize = 0.2f + 0.5f * r;
		pkVertices[0] = NiPoint3(-fSize, -fSize, 0.f);
		pkVertices[1] = NiPoint3( fSize, -fSize, 0.f);
		pkVertices[2] = NiPoint3( fSize,  fSize, 0.f);
		pkVertices[3] = NiPoint3(-fSize,  fSize, 0.f);
	}
	
	NiMaterialProperty* pkMaterialProp = (NiMaterialProperty*)m_spGeometry->GetProperty(NiProperty::MATERIAL);
	NIASSERT(pkMaterialProp);

	float fAlpha = 1.f - r;
	fAlpha *= 2.f;
	fAlpha = NiClamp(fAlpha, 0.f, 1.f);

	pkMaterialProp->SetAlpha(fAlpha);
	m_spGeometry->GetModelData()->MarkAsChanged(NiGeometryData::VERTEX_MASK);
	SceneMgr->RenderObject(m_spGeometry);
}