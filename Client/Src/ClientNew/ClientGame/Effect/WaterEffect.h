#pragma once

#include "EffectBase.h"

#define MAX_WAKE_LIFE (0.8f)
#define MAX_SPLASH_LIFE (2.f)

class CWaterEffect : public CEffectBase
{
public:
	enum EffectType
	{
		ET_WAKE, //ˮ���ƶ��켣
		ET_SPLASH, //ˮ��ͣ���켣
	};

	CWaterEffect(const NiPoint3& kPos, const NiMatrix3& kRotate, EffectType eType);

	virtual BOOL SelfRender() const { return TRUE; }
	virtual void RenderEffect(const NiCamera* pkCamera);

	void UpdateEffect(float fTimeLine, float fDeltaTime) {}

	static void _SDMInit();
	static void _SDMShutdown();
	static void AddEffect(const NiPoint3& kPos, const NiMatrix3& kRotate, EffectType eType);

private:
	EffectType m_eType;
	NiTriShapePtr m_spGeometry;

	static NiZBufferProperty* ms_pkZBufferProperty;
	static NiAlphaProperty* ms_pkAlpha;
	static NiTexturingProperty* ms_pkWakeTextringProp;
	static NiTexturingProperty* ms_pkSplashTextringProp;
};