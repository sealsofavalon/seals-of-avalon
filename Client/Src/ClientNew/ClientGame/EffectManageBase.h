#ifndef _EFFECT_MANAGE_BASE_
#define _EFFECT_MANAGE_BASE_


class CEffectBase;
class CEffectManageBase : public NiMemObject
{
public:
	CEffectManageBase();
	virtual ~CEffectManageBase();

	virtual void RegisterEffect(CEffectBase* Effect) = 0;
	virtual void ReleaseEffect(CEffectBase* Effect, bool bReleaseChild = false) = 0;
};


#endif //_EFFECT_MANAGE_BASE_