#include "StdAfx.h"
#include "EquipSlot.h"

CEquipSlot::CEquipSlot()
{
	m_pkItem = NULL;
}

CEquipSlot::~CEquipSlot()
{
}

CEquipSlot::CEquipSlot(SYItem* pkItem)
{
	m_pkItem = pkItem;
}

void CEquipSlot::Copy(CSlot& slot)
{
	CSlot::Copy(slot);
	m_pkItem = ((CEquipSlot*)&slot)->m_pkItem;
}

void CEquipSlot::Clear()
{
	CSlot::Clear();
	m_pkItem = NULL;
}

ItemPrototype_Client* CEquipSlot::GetItemProperty()
{
	if(m_pkItem)
		return m_pkItem->GetItemProperty();
	return NULL;
}