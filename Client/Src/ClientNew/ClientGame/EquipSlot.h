#ifndef EQUIPSLOT_H
#define EQUIPSLOT_H

#include "Slot.h"
#include "Utils/ItemDB.h"
#include "SYItem.h"

class CEquipSlot : public CSlot
{
public:
	CEquipSlot();
	virtual	~CEquipSlot();

	CEquipSlot(SYItem* pkItem);

	virtual void Clear();

	virtual void Copy(CSlot& slot);

	// common
	virtual eSlotType GetSlotType() const { return ST_EQUIP; }
	virtual DWORD GetCode() const { if(m_pkItem) return m_pkItem->GetCode(); return 0; }

	ui64 GetGUID() 
	{ 
		if(m_pkItem) 
			return m_pkItem->GetGUID(); 
		return 0;
	}

	virtual void SetSlotType(eSlotType type) { __UNUSED(type); }
	virtual void SetCode(DWORD code) { __UNUSED(code); }

	virtual UINT GetNum() const { return 1; }
	virtual void SetNum(UINT num) {}

	inline bool	IsLocked() { if(IsBlocked() || m_bLocked) return true; return false; }
	inline void	SetLock(bool val) { m_bLocked = val; }

	inline bool IsBlocked()	{ return (m_Set == 15);	}

	ItemPrototype_Client* GetItemProperty();

private:
	SYItem* m_pkItem;
	bool m_bLocked;
	BYTE m_Set;
};

#endif