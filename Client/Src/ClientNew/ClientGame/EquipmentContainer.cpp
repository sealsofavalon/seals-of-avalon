#include "StdAfx.h"
#include "EquipmentContainer.h"
#include "EquipSlot.h"

CEquipmentContainer::CEquipmentContainer()
{
}

CEquipmentContainer::~CEquipmentContainer()
{
	CItemSlotContainer::ClearAll();
}

void CEquipmentContainer::Init(BYTE MaxSlotSize, BYTE SlotIdx)
{
	m_nSlotNum = 0;
	m_SlotIdx = SlotIdx;
	m_nMaxSlotNum = MaxSlotSize;
	m_ppSlotArray = new CSlot * [MaxSlotSize];
	for(BYTE i = 0; i < MaxSlotSize; ++i)
	{
		m_ppSlotArray[i] = new CEquipSlot;
	}
	ClearAll();
}

//void CEquipmentContainer::InsertSlot(BYTE AtPos, CEquipSlot* pEquipSlot)
bool CEquipmentContainer::InsertSlot(BYTE AtPos, CSlot& kSlot)
{
	NIASSERT(AtPos < GetMaxSlotNum());
	NIASSERT(IsEmpty(AtPos));

	if(AtPos >= GetMaxSlotNum())		
		return false;

	if(!IsEmpty(AtPos))				
		return false;

	m_ppSlotArray[AtPos]->Copy(kSlot);
	m_ppSlotArray[AtPos]->SetPos(AtPos);
	m_ppSlotArray[AtPos]->SetSlotIdx(GetSlotIdx());
	++m_nSlotNum;
	NIASSERT(m_nSlotNum <= GetMaxSlotNum());

	return true;
}