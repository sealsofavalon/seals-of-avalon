#ifndef EQUIPMENTCONTAINER_H
#define EQUIPMENTCONTAINER_H

#include "ItemSlotContainer.h"

class CEquipmentContainer : public CItemSlotContainer
{
public:
	CEquipmentContainer();
	virtual ~CEquipmentContainer();

	virtual void Init(BYTE MaxSlotSize, BYTE SlotIdx);

	//void InsertSlot(BYTE AtPos, class CEquipSlot* pItemSlot);
	virtual bool InsertSlot(BYTE AtPos, CSlot& rSlot);

protected:
};

#endif