#include "StdAfx.h"
#include "GameObject.h"
#include "Creature.h"
#include "SceneGameObj.h"
#include "SceneManager.h"
#include "ClientApp.h"
#include "LightManager.h"
#include "SceneProperty.h"
#include "Player.h"
#include "ShaderHelper.h"
#include "LocalPlayer.h"
#include "ObjectManager.h"

#include "Console.h"

extern std::string UTF8ToAnis(const std::string& str);

void CGameObject::OnValueChanged(UpdateMask* mask)
{
	UpdateObj::OnValueChanged(mask);

	if(mask->GetBit(OBJECT_FIELD_SCALE_X))
	{
		float scale = GetFloatValue(OBJECT_FIELD_SCALE_X);
		GetSceneNode()->SetScale(scale);
		GetSceneNode()->Update(0);
		GetSceneNode()->UpdateProperties();
	}
	//if( mask->GetBit())
}
void CGameObject::OnCreate(class ByteBuffer* data, ui8 update_flags)
{
	SetPosition(NiPoint3(m_positionX, m_positionY, m_positionZ));
	
	m_pkIsHightLight = FALSE;
	if( isType(TYPE_PLAYER) )
	{
		((CPlayer*)this)->SetWorldTransform(NiPoint3(m_positionX, m_positionY, m_positionZ), NiPoint3(0.f, 0.f, m_positionO));
		string name;
		*data >> name;
		((CPlayer*)this)->SetName(/*UTF8ToAnis*/(name).c_str());
		*data >> name;
		((CPlayer*)this)->SetGuildName(name);
		/**data >> name;
		((CPlayer*)this)->SetPrefixal(name);*/
	}
	else if( isType(TYPE_UNIT) && ( m_uint32Values[UNIT_FIELD_CREATEDBY] != 0 && m_uint32Values[UNIT_FIELD_SUMMONEDBY] != 0 ) )
	{
		string name;
		*data >> name;
		SetName(/*UTF8ToAnis*/(name).c_str());
		((CCharacter*)this)->m_Name = name;
	}
	else if( isType(TYPE_UNIT))
	{
		((CCreature*)this)->SetWorldTransform(NiPoint3(m_positionX, m_positionY, m_positionZ), NiPoint3(0.f, 0.f, m_positionO));
	}else if (isType(TYPE_GAMEOBJECT))
	{
		//((CSceneGameObj*)this)->SetWorldTransform(NiPoint3(m_positionX, m_positionY, m_positionZ), NiPoint3(0.f, 0.f, m_positionO));
	}
}

void CGameObject::PostOnCreate()
{
	AttachToScene();
}

CGameObject::CGameObject(void)
{
	m_spSceneNode = NiNew NiNode;
	NIASSERT(m_spSceneNode);
	m_Flags = GOF_NONE;
    m_kDirection = NiPoint3(0.f, 0.f, 0.f);
	m_pkIsHightLight = FALSE;
	m_bIsEnterView = false;
	m_pkIsHightEdgLight = FALSE;
	m_bAUpdate = true;
	m_fAlpha = 1.0f;
	m_fFadeStep = 0.0f;
	m_bSetAlphaProp = false;

}

CGameObject::~CGameObject(void)
{
	//if (m_spSceneNode->GetArrayCount() > 0)
	//{
	//	SceneMgr->DetachSceneObject(this);
	//}
	m_pkIsHightLight = FALSE;
	m_spSceneNode = NULL;
	m_bAUpdate = true;
}

void CGameObject::PreObjectCreate()
{

}

BOOL CGameObject::PostObjectCreated()
{
	return TRUE;
}

void CGameObject::PrepackGeomtry(NiAVObject* pObject)
{
	if (pObject == NULL || pObject->GetAppCulled())
	{
		return;
	}

	NiRenderer* pkRenderer = NiRenderer::GetRenderer();
	NIASSERT(pkRenderer);

	bool bDynamic = false;
	if (!(pkRenderer->GetFlags() & NiRenderer::CAPS_HARDWARESKINNING))
	{
		bDynamic = true;
	}

	if (NiIsKindOf(NiGeometry, pObject))
	{
		NiGeometry* pkGeom = (NiGeometry*)pObject;

		// Search for morpher controllers
		NiTimeController* pkController;

		for (pkController = pObject->GetControllers(); 
			pkController != NULL; pkController = pkController->GetNext())
		{
			if (NiIsKindOf(NiGeomMorpherController, pkController))
				bDynamic = true;
		}

		NiGeometryData::Consistency eFlags = bDynamic ? 
			NiGeometryData::VOLATILE : NiGeometryData::STATIC;
		pkGeom->SetConsistency(eFlags);

		pkGeom->GetModelData()->SetCompressFlags(NiGeometryData::COMPRESS_ALL);

		pkRenderer->PrecacheGeometry(pkGeom, 0, 0);
	}

	if (NiIsKindOf(NiNode, pObject))
	{
		NiNode* pkNode = (NiNode*)pObject;
		for (unsigned int i = 0; i < pkNode->GetArrayCount(); i++)
		{
			NiAVObject* pkChild = pkNode->GetAt(i);
			if (pkChild)
				PrepackGeomtry(pkChild);
		}
	}
}


void CGameObject::Update(float dt)
{
	if (m_spSceneNode)
	{
		if ( m_fFadeStep != 0.0f && m_fFadeStep != 1.0f)
		{
			m_fAlpha += m_fFadeStep * dt;

			if ( m_fAlpha<= 0.0f)
			{
				m_fAlpha = 0.0f;
				m_fFadeStep = 0.0f;
			}

			if ( m_fAlpha >= 1.0f )
			{
				m_fAlpha = 1.0f;
				m_fFadeStep = 1.0f;
			}

			SetAlpha(m_fAlpha);
			m_spSceneNode->UpdateProperties();
		}

		//CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer(); 
		//if ( pkPlayer && pkPlayer != this )
		//{
		//	NiNode* pNodeL = pkPlayer->GetSceneNode();
		//	NiPoint3 kPosL = pNodeL->GetWorldTranslate();
		//	NiPoint3 kMyPos = m_spSceneNode->GetWorldTranslate();
		//	float fDistance = (kPosL - kMyPos).Length();
		//	if ( fDistance > 30 && m_bIsEnterView )
		//	{
		//		FadeIn();
		//		m_bIsEnterView = false;
		//	}
		//	else if ( fDistance <= 30 && !m_bIsEnterView )
		//	{
		//		m_bIsEnterView = true;
		//		FadeOut();
		//	}
		//}

		m_spSceneNode->Update(SYState()->ClientApp->GetAccumTime());
	}
}
 
NiMaterialProperty* pMatProp = NULL;
// 加入到场景中.
BOOL CGameObject::AttachToScene(bool bNetObject)
{
	NIASSERT(SceneMgr);
	NIASSERT(GetName() != NULL);
	// Light
	SceneMgr->GetLightMgr()->ApplyDirLight(m_spSceneNode);
	m_spSceneNode->UpdateEffects();

	pMatProp = NiNew NiMaterialProperty;
	pMatProp->SetDiffuseColor( NiColor(1.5020f, 0.5020f, 0.5020f) );
	pMatProp->SetAmbientColor( NiColor(1.5020f, 0.5020f, 0.5020f) );
	pMatProp->SetSpecularColor( NiColor(0.2510f, 0.2510f, 0.2510f) );
	pMatProp->SetEmittance( NiColor(0.5020f, 0.5020f, 0.5020f) );
	pMatProp->SetAlpha(1.0f);
	m_spSceneNode->AttachProperty(pMatProp);
	m_spSceneNode->UpdateProperties();

	return SceneMgr->AttachSceneObject(this, bNetObject);
}

// 从场景中移除
void CGameObject::DetachFromScene(bool bNetObject)
{
	NIASSERT(SceneMgr);
//	NIASSERT(GetName() != NULL);
	if (m_spSceneNode)
	{
		SceneMgr->GetLightMgr()->DetachDirLight(m_spSceneNode);
	}
	SceneMgr->DetachSceneObject(this, bNetObject);
}


void SetShader(NiAVObject* Object, const char* szShader)
{
	if( NiIsKindOf( NiGeometry, Object ) )
	{
		NiGeometry* pkGeom = NiDynamicCast(NiGeometry, Object);
		if (pkGeom)
		{
			const NiMaterial* pkActiveMaterial = pkGeom->GetActiveMaterial();
			if ( !pkActiveMaterial )
				return;

			std::string strShader;

			if( pkActiveMaterial->GetName().Contains("FXSY") )
			{
				strShader = "FXSY_";
				strShader += szShader;
				pkGeom->ApplyAndSetActiveMaterial( strShader.c_str() );
				pkGeom->SetFlag(100);
			}
			else if( pkActiveMaterial->GetName().Contains("FXSkinningSY") )
			{
				strShader = "FXSkinningSY_";
				strShader += szShader;
				pkGeom->ApplyAndSetActiveMaterial( strShader.c_str() );
				pkGeom->SetFlag(100);
			}
			else if( pkActiveMaterial->GetName().Contains("AlphaSpecular") )
			{
				strShader = "AlphaSpecular_";
				strShader += szShader;
				pkGeom->ApplyAndSetActiveMaterial( strShader.c_str() );
				pkGeom->SetFlag(100);
			}
			return;
		}
	}

	if( NiIsKindOf(NiNode, Object) )
	{
		NiNode* pkNode = NiDynamicCast(NiNode, Object);
		if (pkNode)
		{
			for( UINT i = 0; i < pkNode->GetArrayCount(); ++i )
			{
				SetShader(pkNode->GetAt( i ), szShader);
			}
		}
	}
}



void SetHilightShader(NiAVObject* Object)
{
	if( NiIsKindOf( NiGeometry, Object ) )
	{
		NiGeometry* pkGeom = NiDynamicCast(NiGeometry, Object);
		if (pkGeom)
		{
			const NiMaterial* pkActivMaterial = pkGeom->GetActiveMaterial();
			if (pkActivMaterial)
			{
				if( pkActivMaterial->GetName().Contains("FXSY") )
				{
					pkGeom->ApplyAndSetActiveMaterial("FXSY_HL");
					pkGeom->SetFlag(100);
				}
				else if( pkActivMaterial->GetName().Contains("FXSkinningSY") )
				{
					pkGeom->ApplyAndSetActiveMaterial("FXSkinningSY_HL");
					pkGeom->SetFlag(100);
				}
				else if ( pkActivMaterial->GetName().Contains("AlphaSpecular") )
				{
					pkGeom->ApplyAndSetActiveMaterial("AlphaSpecular_HL");
					pkGeom->SetFlag(100);
				}
				return;
			}
		}
	}

    if( NiIsKindOf(NiNode, Object) )
    {
       NiNode* pkNode = NiDynamicCast(NiNode, Object);
		if (pkNode)
		{
			for( UINT i = 0; i < pkNode->GetArrayCount(); ++i )
			{
				SetHilightShader(pkNode->GetAt( i ));
			}
		}
    }
}

void RestoreShader(NiAVObject* Object)
{
    NiGeometry* pkGeom = NiDynamicCast(NiGeometry, Object);
	if (pkGeom)
	{
		const NiMaterial* pkMaterial = pkGeom->GetActiveMaterial();
		if ( !pkMaterial )
			return;

		if( pkMaterial->GetName().Contains("FXSY") )
		{
			pkGeom->ApplyAndSetActiveMaterial("FXSY");
			pkGeom->SetFlag(0);
		}
		else if( pkMaterial->GetName().Contains("FXSkinningSY") )
		{
			pkGeom->ApplyAndSetActiveMaterial("FXSkinningSY");
			pkGeom->SetFlag(0);
		}
		else if ( pkMaterial->GetName().Contains("AlphaSpecular") )
		{
			pkGeom->ApplyAndSetActiveMaterial("AlphaSpecular");
			pkGeom->SetFlag(0);
		}
	}
	if( NiIsKindOf(NiNode, Object) )
	{
		NiNode* pkNode = NiDynamicCast(NiNode, Object);
		if (pkNode)
		{
			for( UINT i = 0; i < pkNode->GetArrayCount(); ++i )
			{
				RestoreShader(pkNode->GetAt( i ));
			}
		}
	}
}



void CGameObject::HilightObject(BOOL Hilight)
{
	if (m_pkIsHightLight != Hilight)
	{
		m_pkIsHightLight = Hilight;
		if(Hilight)
		{
			SetShader(m_spSceneNode, "HL");
			//SetAlpha(0.5f);
		}
		else
		{
			//SetAlpha(1.0f);
			RestoreShader(m_spSceneNode);
		}
	}
}

void CGameObject::HilightEdgObject(BOOL bHilightEdg)
{
	if (m_pkIsHightEdgLight != bHilightEdg)
	{
		m_pkIsHightEdgLight = bHilightEdg;
		if(bHilightEdg)
		{	
			SetShader(m_spSceneNode, "HL_EDG");
		}
		else
		{
			RestoreShader(m_spSceneNode);
		}
	}
}

void CGameObject::ReSetAlpha()
{
	if ( m_fAlpha < 1.0f )
	{
		SetMatAlpha(m_spSceneNode, m_fAlpha);
	}
}

void CGameObject::SetMatAlpha(NiAVObject* Object, float fAlpha)
{
	NiGeometry* pkGeom = NiDynamicCast(NiGeometry, Object);
	if (pkGeom)
	{
		NiMaterialProperty* pMatProp = (NiMaterialProperty*)pkGeom->GetProperty(NiProperty::MATERIAL);
		if ( pMatProp )
			pMatProp->SetAlpha(fAlpha);
	}
	if( NiIsKindOf(NiNode, Object) )
	{
		NiNode* pkNode = NiDynamicCast(NiNode, Object);
		if (pkNode)
		{
			for( UINT i = 0; i < pkNode->GetArrayCount(); ++i )
			{
				NiAVObject* pChild = pkNode->GetAt( i ); 
				if ( pChild )
				{
					SetMatAlpha( pChild , fAlpha);
				}
			}
		}
	}
}

void CGameObject::SetAlpha(float fAlpha)
{
	if ( fAlpha < 1.0f && !m_bSetAlphaProp)
	{
		SetShader(m_spSceneNode, "ALPHA");
		m_bSetAlphaProp = true;
	}
	else if ( fAlpha >= 1.0f && m_bSetAlphaProp )
	{
		RestoreShader(m_spSceneNode);
		m_bSetAlphaProp = false; 
	}

	SetMatAlpha(m_spSceneNode, fAlpha);
}

void CGameObject::FadeIn() //--
{
	m_fFadeStep = -0.8f;
}

void CGameObject::FadeOut()//++
{
	m_fFadeStep = 0.8f;
}

void CGameObject::GetMaxAnimTime( NiObjectNET* pkObject, float& fTime, NiTimeController** pkOutCtrl )
{
	NiTimeController* pkCtrl;
	for( pkCtrl = pkObject->GetControllers() ; pkCtrl != NULL ; pkCtrl = pkCtrl->GetNext() )
	{
		if( pkCtrl->GetEndKeyTime() > fTime )
		{
			fTime = pkCtrl->GetEndKeyTime();
			if (pkOutCtrl)
			{
				*pkOutCtrl = pkCtrl;
			}
			
		}
	}

	if( NiIsKindOf( NiNode, pkObject ) )
	{
		NiNode* pkNode = (NiNode*)pkObject;
		for( UINT i=0 ; i<pkNode->GetArrayCount() ; ++i )
		{
			GetMaxAnimTime( pkNode->GetAt(i), fTime, pkOutCtrl );
		}
	}

	if( NiIsKindOf( NiAVObject, pkObject ) )
	{
		NiAVObject* pkAVObject = (NiAVObject*) pkObject;
		NiPropertyList* pkPropList = &(pkAVObject->GetPropertyList());
		NiTListIterator kIter = pkPropList->GetHeadPos();
		while( pkPropList != NULL && !pkPropList->IsEmpty() && kIter )
		{
			NiProperty* pkProperty = pkPropList->GetNext(kIter);
			if( pkProperty )
				GetMaxAnimTime( pkProperty, fTime, pkOutCtrl );
		}
	}
}

bool CGameObject::Draw(CCullingProcess* pkCuller)
{
	if(m_spSceneNode)
		return pkCuller->ProcessObject(m_spSceneNode, CCullingProcess::CullGameObj);

	return false;
}

void CGameObject::SetController(NiObjectNET* pkObject, NiTimeController::AnimType eAnimType, NiTimeController::CycleType eCycleType )
{
	NiTimeController* pkCtrl;
	for( pkCtrl = pkObject->GetControllers() ; pkCtrl != NULL ; pkCtrl = pkCtrl->GetNext() )
	{
		if( eAnimType != -1 )
			pkCtrl->SetAnimType( eAnimType );

		if( eCycleType != -1 )
			pkCtrl->SetCycleType( eCycleType );
	}

	if( NiIsKindOf( NiNode, pkObject ) )
	{
		NiNode* pkNode = (NiNode*)pkObject;
		for( UINT i=0 ; i<pkNode->GetArrayCount() ; ++i )
		{
			SetController( pkNode->GetAt(i), eAnimType, eCycleType );
		}
	}

	if( NiIsKindOf( NiAVObject, pkObject ) )
	{
		NiAVObject* pkAVObject = (NiAVObject*)pkObject;
		NiPropertyList* pkPropList = &(pkAVObject->GetPropertyList());
		NiTListIterator kIter = pkPropList->GetHeadPos();
		while( pkPropList != NULL && !pkPropList->IsEmpty() && kIter )
		{
			NiProperty* pkProperty = pkPropList->GetNext( kIter );
			if( pkProperty )
				SetController( pkProperty, eAnimType, eCycleType );
		}
	}
}