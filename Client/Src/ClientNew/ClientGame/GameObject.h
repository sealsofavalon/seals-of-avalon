#pragma once
#include "UpdateObject.h"
class CPlayer;
class CSceneGameObj;
class CCullingProcess;

enum EGAMEOBJ_TYPE
{
	GOT_UNKNOWN =	0x0000,
	GOT_NORMAL	=	0x0001,		// 随场景生成之对象.
	GOT_PLAYER	=	0x0004,		// 玩家.
	GOT_CREATURE = 0x0008,
	GOT_ITEM	=	0X0020,		// 物品 
	GOT_SCENEOBJECT = 0x0010,	// 场景对象
	GOT_DYNAMICOBJECT = 0X0016, // 动态对象
	GOT_ALL = (GOT_NORMAL | GOT_PLAYER | GOT_CREATURE | GOT_ITEM | GOT_SCENEOBJECT | GOT_DYNAMICOBJECT),
};

enum EGameObjectFlags
{
	GOF_NONE			= 0x00000000,
	GOF_HILIGHT			= 0x00000001,		// 高亮显示对象
};

#define GEO_HILIGHT 0x0200


class CGameObject : public UpdateObj
{
	friend class CSceneManager;
public:
	CGameObject(void);
	virtual ~CGameObject(void);

	virtual EGAMEOBJ_TYPE GetGameObjectType() const { return GOT_UNKNOWN; }
	virtual void HilightObject(BOOL Hilight);
	virtual void HilightEdgObject(BOOL bHilightEdg);

	virtual  CPlayer* GetAsPlayer()  { return NULL;}
	virtual  CSceneGameObj* GetAsSceneObj() { return NULL; }

	virtual BOOL CanBeenAttack() { return FALSE; }
	virtual BOOL IsAnimObject() { return FALSE; }
	virtual bool IsMapItem() const { return false; }

	inline NiNode* GetSceneNode() { return m_spSceneNode; }
	inline const NiFixedString& GetName() const;
	inline void SetName(const NiFixedString& NewName);

	virtual void SetPosition(const NiPoint3& kPos);
	inline const NiPoint3& GetPosition() const;
	inline const NiPoint3& GetPhyPosition() const;
	inline const NiPoint3& GetRotate() const;
	inline const NiMatrix3& GetMatrixRotate() const;
	inline void SetRotate(const NiPoint3& kDir);
	
	static void PrepackGeomtry(NiAVObject* pObject);
	// 加入到场景中.
	virtual BOOL AttachToScene(bool bNetObject = true);
	// 从场景中移除
	virtual void DetachFromScene(bool bNetObject = true);
	virtual void Update(float dt);

	virtual bool GetPickBound(NiBound& kBound) { kBound.SetRadius(0.0f); return false; }
	virtual bool IsPick(NiPick& kPicker, const NiPoint3& kOrig,const NiPoint3& kDir) { return false; }
	virtual bool HasAnimBound() { return false; }

	static void SetController(NiObjectNET* pkObject, NiTimeController::AnimType eAnimType, NiTimeController::CycleType eCycleType );
	static void GetMaxAnimTime(NiObjectNET* pkObject, float& fTime, NiTimeController** pkOutCtrl);

	void CollectMeshInfo();

	void SetAlpha(float fAlpha);

	void FadeIn();
	void FadeOut();

protected:
	virtual void PreObjectCreate();
	virtual BOOL PostObjectCreated();
	virtual bool Draw(CCullingProcess* pkCuller);

	void ReSetAlpha();

	void SetMatAlpha(NiAVObject* Object, float fAlpha);

	NiNodePtr m_spSceneNode;
	UINT m_Flags;
	float m_fAlpha;
	float m_fFadeStep;
	bool m_bSetAlphaProp;

	bool m_bIsEnterView;

	NiPoint3 m_kPosition;	// 关于物理的请直接用m_kPosition;
	NiPoint3 m_kDirection;
	BOOL m_pkIsHightLight;
	BOOL m_pkIsHightEdgLight;

public:
	virtual void OnValueChanged(class UpdateMask* mask);
	virtual void OnCreate(class ByteBuffer* data, ui8 update_flags);
	virtual void PostOnCreate();

	// 是否为本地玩家
	virtual bool IsLocalPlayer() const 
	{ 
		return false; 
	}
	virtual BOOL IsLootAble()
	{
		return false;
	}

	bool m_bAUpdate;
	virtual inline void SetAUpdate(bool bUpdate){
		m_bAUpdate = bUpdate;
	}
};

//////////////////////////////////////////////////////////////////////////
//		inline implement.
//////////////////////////////////////////////////////////////////////////

inline const NiFixedString& CGameObject::GetName() const
{
	NIASSERT(m_spSceneNode);
	return m_spSceneNode->GetName();
}

inline void CGameObject::SetName(const NiFixedString& NewName)
{
	NIASSERT(m_spSceneNode && NewName);
	m_spSceneNode->SetName(NewName);
}

inline const NiPoint3& CGameObject::GetPosition() const
{
	NIASSERT(m_spSceneNode);
	return m_spSceneNode->GetTranslate();
}

inline const NiPoint3& CGameObject::GetPhyPosition() const
{
	return m_kPosition;
}

inline void CGameObject::SetPosition(const NiPoint3& kPos)
{
	m_kPosition = kPos;
	if (m_spSceneNode)
		m_spSceneNode->SetTranslate(kPos);
}

inline void CGameObject::SetRotate(const NiPoint3& kDir)
{
	m_kDirection = kDir;
	if( m_spSceneNode )
	{
		NiMatrix3 kRotate;
		kRotate.MakeZRotation(kDir.z);
		m_spSceneNode->SetRotate(kRotate);
	}
}

inline const NiPoint3& CGameObject::GetRotate() const
{
	return m_kDirection;
}

inline const NiMatrix3& CGameObject::GetMatrixRotate() const
{
	if (m_spSceneNode)
	{
		return m_spSceneNode->GetRotate();
	}

	return NiMatrix3::IDENTITY;
}