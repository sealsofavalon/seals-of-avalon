#pragma once
//////////////////////////////////////////////////////////////////////////
/////////                      挂机系统                          /////////
//////////////////////////////////////////////////////////////////////////
#include "HookState.h"

const size_t  AutoMaxSpell = 5;
const size_t  AutoYaoShui = 3;
#define HookMgr CHookMgr::sm_HookMgr 

class UHookSet ;

struct HookActor
{
	virtual void UpdateState(DWORD Time) = 0;
	virtual BOOL SetNextState(HookState NextState, ui64 ID) = 0;
	virtual HookStateProxy* ForceStateChange(HookState NextState, ui64 id) = 0;
};


struct HookData
{
	BOOL AutoAttack ;  //自动攻击
	BOOL AutoSupply ;  //自动补给
	BOOL AutoPick ;    //自动拾取
	BOOL AutoShunBoss; //自动回避BOSS

	BOOL DeadAutoPos;
	BOOL UseSpellFirst ;   // 优先使用技能回复
	
	ui32 Spell[AutoMaxSpell] ;  //可以使用技能列表
	ui32 HPSpell;
	ui32 MPSpell;

	ui32 HPEntry[AutoYaoShui];
	ui32 MPEntry[AutoYaoShui];
	float Hp, Mp ;              // 血 蓝 吃药的界限

	NiPoint3 HookPos; // 挂机位置
	
	void ResetData()
	{
		AutoAttack = FALSE;
		AutoSupply = FALSE;
		AutoPick = FALSE;
		AutoShunBoss = FALSE;
		DeadAutoPos = FALSE;
		UseSpellFirst = FALSE ;


		for (int i = 0; i < AutoMaxSpell; i++)
		{
			Spell[i] = 0 ;
		}

		for ( int i = 0; i < AutoYaoShui; i++)
		{
			HPEntry[i] = 0;
			MPEntry[i] = 0;
		}
		HPSpell = 0;  //恢复HP技能
		MPSpell = 0;  //恢复MP技能

		HookPos = NiPoint3::ZERO ;

		Hp = 0.5f;
		Mp = 0.5f;
	}

	HookData()
	{
		AutoAttack = FALSE;
		AutoSupply = FALSE;
		AutoPick = FALSE;
		AutoShunBoss = FALSE;
		DeadAutoPos = FALSE;
		UseSpellFirst = FALSE;

		for (int i = 0; i < AutoMaxSpell; i++)
		{
			Spell[i] = 0 ;
		}

		for ( int i = 0; i < AutoYaoShui; i++)
		{
			HPEntry[i] = 0;
			MPEntry[i] = 0;
		}
		HPSpell = 0;  //恢复HP技能
		MPSpell = 0;  //恢复MP技能

		HookPos = NiPoint3::ZERO ;

		Hp = 0.5f;
		Mp = 0.5f;
	}
	

	HookData & operator=(const HookData & item)
	{
		if (this != &item)
		{
			AutoAttack   = item.AutoAttack;
			AutoSupply   = item.AutoSupply;
			AutoPick     = item.AutoPick;
			AutoShunBoss = item.AutoShunBoss;
			DeadAutoPos = item.DeadAutoPos;

			for (int i = 0; i < AutoMaxSpell; i++)
			{
				Spell[i] = item.Spell[i] ;
			}

			for ( int i = 0; i < AutoYaoShui; i++)
			{
				HPEntry[i] = item.HPEntry[i];
				MPEntry[i] = item.MPEntry[i];
			}

			HPSpell = item.HPSpell;
			MPSpell = item.MPSpell;
			Hp = item.Hp;
			Mp = item.Mp;

			UseSpellFirst = item.UseSpellFirst ;

			HookPos = item.HookPos;
		}

		return *this;
	}
};

class CHookMgr : public HookActor
{
public:
	CHookMgr();
	~CHookMgr();

	static void InitHookMgr(); 
	static void ShutDownHookMgr();
	static CHookMgr* sm_HookMgr ;
	static void OutPutError(const char* text);
	static void OutPutMsg(const char* text);

	BOOL LoadHookFile();  // 读取配置文件
	BOOL SaveHookFile();				  //保存配置文件
public:
	void SetHook();

	
public:
	BOOL CreateUI(UControl* pkCtrl);
	void BeginHookActor(); // 
	void EndHookActor();

	void HookRelive();
	void ResetHook();
	//
	BOOL StopUseSpellState(ui32 spellid, ui8 error = SPELL_CANCAST_OK);
	BOOL HookUseSpellFailed(ui8 error);
	BOOL StopPickState(ui64 guid);

	//
	BOOL SetHookData(const HookData& pkData);
	BOOL SetLoadHookData(const HookData& pkData);
	BOOL InitUIHookData(HookData& pkData);

	inline bool IsActorRun()const {return m_bStartHookActor;}
	inline const HookData* GetLoadHookData(){return m_LoadData;}
	inline const HookData* GetHookData(){return m_HookData;}
public:
	//状态机
	virtual void UpdateState(DWORD Time);
	virtual BOOL SetNextState(HookState NextState, ui64 ID);
	virtual HookStateProxy* ForceStateChange(HookState NextState, ui64 id);
protected:
	HookStateProxy* GetStateInstance(HookState eState); // 获得状态
	BOOL ChangeToNextState();

public:	
	
	BOOL SetNextNormalState(ui64 guid);
	BOOL SetNextNormalState();
	BOOL SetNextAttackSkillState();
	BOOL SetNextHPSkillState();
	BOOL SetNextMPSkillState();
	BOOL SetNextPickState();

	BOOL SetNextWalkState(NiPoint3 Pos, ui64 guid);
	BOOL SetNextWalkState();
	BOOL SetNextUseHPItemState();
	BOOL SetNextUseMPItemState();

	BOOL ForceMPSkillStateChange();
	BOOL ForceMPItemStateChange();


protected:
	HookData* m_HookData ; //    数据保存
	HookData*  m_LoadData ;
	bool m_bStartHookActor ;
	HookState  m_HookStat ;
	HookState  m_NextHookStat ;

	SYObjID m_CurTargetObjectID ;
	UHookSet* m_HookCtr ;  //UI  设置相关数据

	BOOL m_SaveAutoAction ;
	ui8 m_LastError ;
private:
};