#ifndef __HOOKSTATEBASE_H__
#define __HOOKSTATEBASE_H__ 


enum EHookStateProcessResult
{
	SUSPEND_HOOK = 0,	//挂起
	HOLD_HOOK,			//保持
	END_HOOK,			//结束
};
enum  HookState
{
	Hook_UnKnow = 0,
	Hook_Normal ,     //  正常状态 (检测是否有怪物可以攻击(回避BOSS))  
	Hook_UseSpell,    //  使用技能 (包括技能读条等等)
	Hook_Supply,      //  吃药水
	Hook_Pick ,       //  拾取
	Hook_Walk,        //  回到原位 

};

struct HookStateProxy
{
	virtual HookState GetHookState() = 0;
	virtual BOOL CanTransit(HookState state) = 0;
	virtual BOOL Start() = 0;
	virtual HookState End() = 0;
	virtual EHookStateProcessResult Process(DWORD dwtime) = 0;
	virtual BOOL IsLock() = 0;
	virtual void SetID(ui64 id) = 0;  // 技能状态是技能ID .  使用补给是药水ID  NORMAL 是排除选择的对象ID
	virtual ui64 GetID()= 0;
	virtual char* GetStateDesc() = 0;
};

#define HOOK_INSTANCE(Cls)		\
	public:							\
	static HookStateProxy* Instance() { static Cls proxy; return &proxy;}	\
	private:	\


class StateHookUseSpell : public HookStateProxy
{
public:
	HOOK_INSTANCE(StateHookUseSpell);
	virtual HookState GetHookState();
	virtual BOOL CanTransit(HookState state);
	virtual BOOL Start();
	virtual HookState End();
	virtual EHookStateProcessResult Process(DWORD dwtime);
	virtual BOOL IsLock();
	virtual void SetID(ui64 id){Spellid = (ui32)id;}
	virtual ui64 GetID(){return Spellid;}
	virtual char* GetStateDesc(){return "StateHookUseSpell";}
protected:
	BOOL IsJumped;
	ui32 Spellid ;
	NiPoint3 StartPos;
	NiPoint3 LastPos ;
	DWORD DwWaitTime;
	DWORD DwNormalAttackTime;

};
class StateHookSupply : public HookStateProxy
{
public:
	HOOK_INSTANCE(StateHookSupply);
	virtual HookState GetHookState();
	virtual BOOL CanTransit(HookState state);
	virtual BOOL Start();
	virtual HookState End();
	virtual EHookStateProcessResult Process(DWORD dwtime);
	virtual BOOL IsLock();
	virtual void SetID(ui64 id){Itemid = id;}
	virtual ui64 GetID(){return Itemid;}
	virtual char* GetStateDesc(){return "StateHookSupply";}
protected:
	DWORD LockTime ;
	ui64 Itemid ;
};
class StateHookNormal : public HookStateProxy
{
public:
	HOOK_INSTANCE(StateHookNormal);
	StateHookNormal();
	virtual HookState GetHookState();
	virtual BOOL CanTransit(HookState state);
	virtual BOOL Start();
	virtual HookState End();
	virtual EHookStateProcessResult Process(DWORD dwtime);
	virtual BOOL IsLock();
	virtual void SetID(ui64 id){guid = id;}
	virtual ui64 GetID(){return guid;}
	virtual char* GetStateDesc(){return "StateHookNormal";}
public:
	void SetActiveWaitSpell(BOOL isActive){bWaitSpell = isActive; DwWaitSpellTime = 0 ;}
protected:
	BOOL bWait ;
	DWORD DwWaitTime;
	ui64 guid ;

	BOOL bWaitSpell;
	DWORD DwWaitSpellTime ;

	DWORD WaitHPMPTime[2];
};

class StateHookPick : public HookStateProxy
{
public:
	HOOK_INSTANCE(StateHookPick);
	virtual HookState GetHookState();
	virtual BOOL CanTransit(HookState state);
	virtual BOOL Start();
	virtual HookState End();
	virtual EHookStateProcessResult Process(DWORD dwtime);
	virtual BOOL IsLock();
	virtual void SetID(ui64 id){;}
public:
	virtual ui64 GetID(){return PickOBJGuid;}
	virtual char* GetStateDesc(){return "StateHookPick";}
protected:
	ui64 PickOBJGuid ;
	DWORD DwWaitTime;
	BOOL IsJumped;

};

class StateHookWalk : public HookStateProxy
{
public:
	HOOK_INSTANCE(StateHookWalk);
	virtual HookState GetHookState();
	virtual BOOL CanTransit(HookState state);
	virtual BOOL Start();
	virtual HookState End();
	virtual EHookStateProcessResult Process(DWORD dwtime);
	virtual BOOL IsLock();
	virtual void SetID(ui64 id){guid = id;}
	virtual ui64 GetID(){return guid;}
	virtual char* GetStateDesc(){return "StateHookWalk";}
public:
	void SetMovePos(NiPoint3 pos){ToPos = pos;}
protected:
	BOOL IsJumped;
	DWORD DwWaitTime;
	BOOL StartMove ;
	
	NiPoint3 LastPos ;

	NiPoint3 ToPos;  //处理卡机状态 如果卡机的话。跑回技能开始呼叫的地点
	ui64 guid; //卡机出现的对象.保存起来用来排除在下次选取的目标里
};
#endif