#include "stdafx.h"
#include "UHookSet.h"
#include "ui/UIItemSystem.h"
#include "ui/UIGamePlay.h"
#include "ui/UInGameBar.h"
#include "LocalPlayer.h"
#include "ItemSlot.h"
#include "ItemStorage.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "Utils/ItemDB.h"
#include "Skill/SkillManager.h"

//////////////////////////////////////////////////////////////////////////
//
//       HOOK  ACTION BTN (SKILL ACTION BTN AND ITEM ACTION BTN)
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UHookSet::ActionButton, UDynamicIconButtonEx);
void UHookSet::ActionButton::StaticInit()
{

}

UHookSet::ActionButton::ActionButton()
{
	m_GlintTexture = NULL ;
}
UHookSet::ActionButton::~ActionButton()
{

}
void UHookSet::ActionButton::FouceUse()
{
	if (m_ActionContener.IsCreate())
	{
		m_ActionContener.FouceUse();
	}
}
BOOL UHookSet::ActionButton::OnCreate()
{
	if (!UDynamicIconButtonEx::OnCreate())
	{
		return FALSE ;
	}

	if (!m_GlintTexture)
	{
		m_GlintTexture = sm_UiRender->LoadTexture("Data\\UI\\InGameBar\\GLINk.tga");

	}
	if (!m_GlintTexture)
	{
		return FALSE ;
	}


	return TRUE ;
}
void UHookSet::ActionButton::OnRender(const UPoint& offset, const URect &updateRect)
{
	UDynamicIconButtonEx::OnRender(offset,updateRect);
	OnRenderGlint(offset,updateRect);
}

#define UAGLINTSIZE_X 16
#define UAGLINTSIZE_Y 16
void UHookSet::ActionButton::OnRenderGlint(const UPoint& offset,const URect &updateRect)
{
	if (mBGlint)
	{
		URect OldClip = sm_UiRender->GetClipRect();

		URect ClipRec;
		ClipRec.pt0.x = updateRect.pt0.x - (UAGLINTSIZE_X>>1);
		ClipRec.pt0.y = updateRect.pt0.y - (UAGLINTSIZE_Y>>1);
		ClipRec.pt1.x = ClipRec.pt0.x + updateRect.GetWidth() + (UAGLINTSIZE_X>>1) ;
		ClipRec.pt1.y = ClipRec.pt0.y + updateRect.GetHeight() + (UAGLINTSIZE_Y>>1) ;

		sm_UiRender->SetClipRect(ClipRec);

		UColor Color(255,255,200);
		if (mGlintTime <= 1.0f)
		{
			Color.a = (BYTE)(100 + 155 * mGlintTime);
		}else
		{
			Color.a = (BYTE)(100 + 155 * (2.0f - mGlintTime));
		}
		//Color.g *= alpha ;
		sm_UiRender->DrawImage(m_GlintTexture,ClipRec,Color);
		sm_UiRender->SetClipRect(OldClip);
	}
}
void UHookSet::ActionButton::OnTimer(float fDeltaTime)
{
	UDynamicIconButtonEx::OnTimer(fDeltaTime);

	if (mBGlint)
	{
		mGlintTime += fDeltaTime ;
		if (mGlintTime >= 2.0f)
		{
			mGlintTime = 0.f;
			mBGlint = FALSE ;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//
//                     HOOK SKILL  ACTION BTN
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UHookSet::SkillActionBtn, UHookSet::ActionButton);
void UHookSet::SkillActionBtn::StaticInit()
{

}

UHookSet::SkillActionBtn::SkillActionBtn()
{
	m_TypeID = UItemSystem::ICT_HOOK_SKILL;
	m_ActionContener.SetActionItem(new UHookSet::ActionBtnItem);
	
}
UHookSet::SkillActionBtn::~SkillActionBtn()
{

}
void UHookSet::SkillActionBtn::SetItemData(ActionDataInfo &aiteminfo)
{
	UControl* pkCtr = GetParent();
	if (!pkCtr)
	{
		return ;
	}
	UHookSet* pkHookSet = UDynamicCast(UHookSet, pkCtr);
	if (!pkHookSet)
	{
		return ;
	}

	if (m_nCtrlID >= Hook_Spell_0 && m_nCtrlID <=  Hook_Spell_4)
	{
		if (pkHookSet->AddHookASkill(aiteminfo.entry, m_nCtrlID - Hook_Spell_0))
		{
			return UHookSet::ActionButton::SetItemData(aiteminfo);
		}
	}

	if (m_nCtrlID == Hook_HP_Spell)
	{
		if (pkHookSet->AddHookHPSkill(aiteminfo.entry, 0))
		{
			return UHookSet::ActionButton::SetItemData(aiteminfo);
		}
	}
	
	if (m_nCtrlID == Hook_MP_Spell)
	{
		if (pkHookSet->AddHookMPSkill(aiteminfo.entry, 0))
		{
			return UHookSet::ActionButton::SetItemData(aiteminfo);
		}
	}
}
void UHookSet::SkillActionBtn::SetItemData(UActionContener &contener)
{
	UControl* pkCtr = GetParent();
	if (!pkCtr)
	{
		return ;
	}
	UHookSet* pkHookSet = UDynamicCast(UHookSet, pkCtr);
	if (!pkHookSet)
	{
		return ;
	}

	if (m_nCtrlID >= Hook_Spell_0 && m_nCtrlID <=  Hook_Spell_4)
	{
		if (pkHookSet->AddHookASkill(contener.GetDataInfo()->entry, m_nCtrlID - Hook_Spell_0))
		{
			return UHookSet::ActionButton::SetItemData(contener);
		}
	}

	if (m_nCtrlID == Hook_HP_Spell)
	{
		if (pkHookSet->AddHookHPSkill(contener.GetDataInfo()->entry, 0))
		{
			return UHookSet::ActionButton::SetItemData(contener);
		}
	}

	if (m_nCtrlID == Hook_MP_Spell)
	{
		if (pkHookSet->AddHookMPSkill(contener.GetDataInfo()->entry, 0))
		{
			return UHookSet::ActionButton::SetItemData(contener);
		}
	}
}
BOOL UHookSet::SkillActionBtn::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (UDynamicIconButtonEx::OnMouseUpDragDrop(pDragFrom,point,pDragData,nDataFlag))
	{
		UDynamicIconButtonEx * pDib = UDynamicCast(UDynamicIconButtonEx,pDragFrom);
		switch (nDataFlag)
		{
		case UItemSystem::ICT_SKILL:
			{
				if (!pDib)
				{
					return FALSE;
				}

				ActionDataInfo datainfo;
				datainfo.entry = pDib->GetItemData()->entry;
				datainfo.pos = GetItemData()->pos;
				datainfo.type = GetItemData()->type;

				SetItemData(datainfo);
			}
			return TRUE;
		case UItemSystem::ICT_INGAME_SKILL:
			{
				if (!pDib)
				{
					return FALSE;
				}
				
				if (pDib->GetItemData())
				{
					if (pDib->GetItemData()->type != UItemSystem::SPELL_DATA_FLAG)
					{
						return FALSE ;
					}
					
				}
				ActionDataInfo datainfo;
				datainfo.entry = pDib->GetItemData()->entry;
				datainfo.pos = GetItemData()->pos;
				datainfo.type = GetItemData()->type;

				SetItemData(datainfo);
			}
			return TRUE;
		}
	}
	
	return FALSE;
	
}
//////////////////////////////////////////////////////////////////////////
//
//                     HOOK ITEM  ACTION BTN
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UHookSet::ItemActionBtn, UHookSet::ActionButton);
void UHookSet::ItemActionBtn::StaticInit()
{

}

UHookSet::ItemActionBtn::ItemActionBtn()
{
	m_TypeID = UItemSystem::ICT_HOOK_ITEM;
	m_ActionContener.SetActionItem(new UHookSet::ActionBtnItem);

}
UHookSet::ItemActionBtn::~ItemActionBtn()
{

}
void UHookSet::ItemActionBtn::SetItemData(ActionDataInfo &aiteminfo)
{
	UControl* pkCtr = GetParent();
	if (!pkCtr)
	{
		return ;
	}
	UHookSet* pkHookSet = UDynamicCast(UHookSet, pkCtr);
	if (!pkHookSet)
	{
		return ;
	}
	
	if (m_nCtrlID >= Hook_HP_Item_0 && m_nCtrlID <=  Hook_HP_Item_2)
	{
		if (pkHookSet->AddHookHPItem(aiteminfo.entry, m_nCtrlID - Hook_HP_Item_0))
		{
			return UHookSet::ActionButton::SetItemData(aiteminfo);
		}
	}

	if (m_nCtrlID >= Hook_MP_Item_0 && m_nCtrlID <= Hook_MP_Item_2)
	{
		if (pkHookSet->AddHookMPItem(aiteminfo.entry, m_nCtrlID - Hook_MP_Item_0))
		{
			return UHookSet::ActionButton::SetItemData(aiteminfo);
		}
	}
}
void UHookSet::ItemActionBtn::SetItemData(UActionContener &contener)
{
	UControl* pkCtr = GetParent();
	if (!pkCtr)
	{
		return ;
	}
	UHookSet* pkHookSet = UDynamicCast(UHookSet, pkCtr);
	if (!pkHookSet)
	{
		return ;
	}

	if (m_nCtrlID >= Hook_HP_Item_0 && m_nCtrlID <=  Hook_HP_Item_2)
	{
		if (pkHookSet->AddHookHPItem(contener.GetDataInfo()->entry, m_nCtrlID - Hook_HP_Item_0))
		{
			return UHookSet::ActionButton::SetItemData(contener);
		}
	}

	if (m_nCtrlID >= Hook_MP_Item_0 && m_nCtrlID <= Hook_MP_Item_2)
	{
		if (pkHookSet->AddHookMPItem(contener.GetDataInfo()->entry, m_nCtrlID - Hook_MP_Item_0))
		{
			return UHookSet::ActionButton::SetItemData(contener);
		}
	}
}
BOOL UHookSet::ItemActionBtn::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (UDynamicIconButtonEx::OnMouseUpDragDrop(pDragFrom,point,pDragData,nDataFlag))
	{
		UDynamicIconButtonEx * pDib = UDynamicCast(UDynamicIconButtonEx,pDragFrom);

		CPlayerLocal* localPlayer = ObjectMgr->GetLocalPlayer();
		if (!localPlayer)
		{
			return FALSE ;
		}

		CStorage* pContainer = localPlayer->GetItemContainer();

		if (!pContainer)
		{
			return FALSE ;
		}

		switch (nDataFlag)
		{
		case UItemSystem::ICT_INGAME_SKILL:
			{
				if (!pDib)
				{
					return FALSE;
				}

				if (pDib->GetItemData())
				{
					if (pDib->GetItemData()->type != UItemSystem::ITEM_DATA_FLAG)
					{
						return FALSE ;
					}

					ItemPrototype_Client* pkItemInfo = ItemMgr->GetItemPropertyFromDataBase(pDib->GetItemData()->entry);

					if (!pkItemInfo)
					{
						return FALSE ;
					}

					if ((pkItemInfo->Class != ITEM_CLASS_CONSUMABLE) || (pkItemInfo->SubClass !=  ITEM_SUBCLASS_CONSUMABLE_POTION))
					{
						return FALSE ;
					}

					if (pkItemInfo->RequiredLevel > localPlayer->GetLevel())
					{
						return FALSE ;
					}

				}



				ActionDataInfo datainfo;
				datainfo.entry = pDib->GetItemData()->entry;
				datainfo.pos = GetItemData()->pos;
				datainfo.type = GetItemData()->type;
				datainfo.num = pContainer->GetItemCnt(pDib->GetItemData()->entry);

				SetItemData(datainfo);
			}
			return TRUE;
		case UItemSystem::ICT_BAG:
			{
				ActionDataInfo datainfo;
				ui8* FromePos = (ui8*)pDragData;
				
				if (localPlayer)
				{
					
					CItemSlot* pSlot = (CItemSlot*)pContainer->GetSlot(*FromePos);	

					if (!pSlot)
					{
						return FALSE;
					}

					ItemPrototype_Client* pkItemInfo = ItemMgr->GetItemPropertyFromDataBase(pSlot->GetCode());

					if (!pkItemInfo)
					{
						return FALSE ;
					}

				
					if ((pkItemInfo->Class != ITEM_CLASS_CONSUMABLE) || (pkItemInfo->SubClass !=  ITEM_SUBCLASS_CONSUMABLE_POTION))
					{
						return FALSE ;
					}

					if (pkItemInfo->RequiredLevel > localPlayer->GetLevel())
					{
						return FALSE ;
					}


					datainfo.entry = pSlot->GetCode();
					datainfo.pos = GetItemData()->pos;
					datainfo.type = UItemSystem::ITEM_DATA_FLAG;
					datainfo.num = pContainer->GetItemCnt(pSlot->GetCode());

					SetItemData(datainfo);

				}		
				return TRUE;
			}
		}
	}
	
	return FALSE;
	
}
//////////////////////////////////////////////////////////////////////////
///                  data 
//////////////////////////////////////////////////////////////////////////

void UHookSet::ActionBtnItem::Use()
{
	// DO NOTHING
}
//////////////////////////////////////////////////////////////////////////
//
//				HOOK UI SETTING 
//////////////////////////////////////////////////////////////////////////



UIMP_CLASS(UHookSet,UDialog);
UBEGIN_MESSAGE_MAP(UHookSet,UDialog)
//
UON_BN_CLICKED(13, &UHookSet::CheckAutoAttack)
UON_BN_CLICKED(15, &UHookSet::CheckAutoSupply)
UON_BN_CLICKED(17, &UHookSet::CheckUseSpellFrist)
UON_BN_CLICKED(19, &UHookSet::CheckAutoPick)
UON_BN_CLICKED(21, &UHookSet::CheckAutoShunBoss)
UON_BN_CLICKED(34, &UHookSet::CheckDeadAutoPos)

//Slider
UON_SLIDER_CHANGE(23, &UHookSet::ChangHPSlider)
UON_SLIDER_CHANGE(24, &UHookSet::ChangMPSlider)

UON_BN_CLICKED(25, &UHookSet::StartHook)
UON_BN_CLICKED(26, &UHookSet::ReSetHookData)
UON_BN_CLICKED(36, &UHookSet::SaveHookData)
UEND_MESSAGE_MAP()

void UHookSet::StaticInit()
{

}
UHookSet::UHookSet()
{
	m_HookData = new HookData ;

	for ( int i = 0; i < AutoMaxSpell; i ++)
	{
		m_SpellActionBtn[i] = NULL ;
	}

	m_HPActionBtn = NULL;
	m_MPActionBtn = NULL;

	for (int i = 0; i< AutoYaoShui; i++)
	{
		m_HPItemActionBtn[i] = NULL;
		m_MPItemActionBtn[i] = NULL;
	}


	m_AutoAttack  = NULL;
	m_AutoSupply  = NULL;
	m_AutoPick = NULL;
	m_AutoShunBoss = NULL;
	m_DeadAuto = NULL;
	m_UseSpellFrist = NULL;

	m_HPSlider = NULL;
	m_MPSlider = NULL;

	m_HookStarte = NULL;
	m_HookCancelOrStop = NULL;

	m_LockData = TRUE;
}
UHookSet::~UHookSet()
{
	if (m_HookData)
	{
		delete m_HookData ;
		m_HookData = NULL ;
	}
}

BOOL UHookSet::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE ;
	}

	m_SpellActionBtn[0] = UDynamicCast(SkillActionBtn, GetChildByID(Hook_Spell_0));
	m_SpellActionBtn[1] = UDynamicCast(SkillActionBtn, GetChildByID(Hook_Spell_1));
	m_SpellActionBtn[2] = UDynamicCast(SkillActionBtn, GetChildByID(Hook_Spell_2));
	m_SpellActionBtn[3] = UDynamicCast(SkillActionBtn, GetChildByID(Hook_Spell_3));
	m_SpellActionBtn[4] = UDynamicCast(SkillActionBtn, GetChildByID(Hook_Spell_4));


	m_HPActionBtn = UDynamicCast(SkillActionBtn, GetChildByID(Hook_HP_Spell));
	m_MPActionBtn = UDynamicCast(SkillActionBtn, GetChildByID(Hook_MP_Spell));

	m_HPItemActionBtn[0] = UDynamicCast(ItemActionBtn, GetChildByID(Hook_HP_Item_0));
	m_HPItemActionBtn[1] = UDynamicCast(ItemActionBtn, GetChildByID(Hook_HP_Item_1));
	m_HPItemActionBtn[2] = UDynamicCast(ItemActionBtn, GetChildByID(Hook_HP_Item_2));
	
	m_MPItemActionBtn[0] = UDynamicCast(ItemActionBtn, GetChildByID(Hook_MP_Item_0));
	m_MPItemActionBtn[1] = UDynamicCast(ItemActionBtn, GetChildByID(Hook_MP_Item_1));
	m_MPItemActionBtn[2] = UDynamicCast(ItemActionBtn, GetChildByID(Hook_MP_Item_2));

	for ( int i = 0; i < AutoMaxSpell; i ++)
	{
		if (!m_SpellActionBtn[i])
		{
			return FALSE ;
		}else
		{
			ActionDataInfo datainfo;
			datainfo.entry = 0;
			datainfo.Guid = 0;
			datainfo.pos = i;
			datainfo.type = UItemSystem::SPELL_DATA_FLAG;

			m_SpellActionBtn[i]->SetItemData(datainfo);
		}
	}

	if (!m_HPActionBtn || !m_MPActionBtn)
	{
		return FALSE ;
	}else
	{
		ActionDataInfo datainfo;
		datainfo.entry = 0;
		datainfo.Guid = 0;
		datainfo.pos = 0;
		datainfo.type = UItemSystem::SPELL_DATA_FLAG;

		m_HPActionBtn->SetItemData(datainfo);
		m_MPActionBtn->SetItemData(datainfo);
	}

	for (int i = 0; i< AutoYaoShui; i++)
	{
		if (!m_HPItemActionBtn[i]  || !m_MPItemActionBtn[i])
		{
			return FALSE ;
		}else
		{
			ActionDataInfo datainfo;
			datainfo.entry = 0;
			datainfo.Guid = 0;
			datainfo.pos = i;
			datainfo.type = UItemSystem::ITEM_DATA_FLAG;

			m_HPItemActionBtn[i]->SetItemData(datainfo);
			m_MPItemActionBtn[i]->SetItemData(datainfo);
		}
	}

	m_HPSlider = UDynamicCast(USlider, GetChildByID(23));
	m_MPSlider = UDynamicCast(USlider, GetChildByID(24));

	if (!m_HPSlider || !m_MPSlider)
	{
		return FALSE ;
	}else
	{
		m_HPSlider->SetRange(0,100);
		m_HPSlider->SetValue(50);

		m_MPSlider->SetRange(0,100);
		m_MPSlider->SetValue(50);
	}

	m_HookStarte = UDynamicCast(UBitmapButton, GetChildByID(25));
	m_HookCancelOrStop = UDynamicCast(UBitmapButton, GetChildByID(26));

	if (!m_HookCancelOrStop || !m_HookStarte)
	{
		return FALSE;
	}

	//按钮开关设置
	m_AutoAttack  = UDynamicCast(UButton, GetChildByID(13));
	m_AutoSupply  = UDynamicCast(UButton, GetChildByID(15));
	m_AutoPick = UDynamicCast(UButton,GetChildByID(19));
	m_AutoShunBoss = UDynamicCast(UButton, GetChildByID(21));
	m_UseSpellFrist = UDynamicCast(UButton, GetChildByID(17));
	m_DeadAuto = UDynamicCast(UButton,GetChildByID(34));

	if (!m_AutoAttack || !m_AutoSupply || !m_AutoPick || !m_AutoShunBoss || !m_UseSpellFrist || !m_DeadAuto)
	{
		return FALSE ;
	}else
	{
		m_AutoAttack->SetCheck(FALSE);
		m_AutoSupply->SetCheck(FALSE);
		m_AutoPick->SetCheck(FALSE);
		m_AutoShunBoss->SetCheck(FALSE);
		m_UseSpellFrist->SetCheck(FALSE);
		m_DeadAuto->SetCheck(FALSE);
	}

	m_CloseBtn->SetPosition(UPoint(435,56));
	m_LockData = FALSE ;
	return TRUE;
}
BOOL UHookSet::AddHookHPItem(ui32 entry, ui32 pos)
{
	if (entry)
	{
		if (!ItemMgr->IsHealingItem(entry))
		{
			CHookMgr::OutPutError(_TRAN("We need to respond to the blood props"));
			return FALSE ;
		}
	}
	if (m_LockData)
	{
		return TRUE;
	}
	if (m_HookData)
	{
		for (int i = 0; i < AutoYaoShui; i++)
		{
			if (entry && m_HookData->HPEntry[i] == entry)
			{
				//已经存在列表
				OutputDebugString("Hook settings: This medicine already in the list");
				CHookMgr::OutPutError(_TRAN("The prop already in the list"));
				return FALSE ;
			}
		}

		if (pos >= AutoYaoShui)
		{
			OutputDebugString("挂机设置：该药水位置不存在");
			return FALSE ;
		}

		m_HookData->HPEntry[pos] = entry ;

		return TRUE ;
	}
	return FALSE ;
}
BOOL UHookSet::AddHookMPItem(ui32 entry, ui32 pos)
{
	if (entry)
	{
		if (!ItemMgr->IsMaNaItem(entry))
		{
			CHookMgr::OutPutError(_TRAN("需要回复魔法道具"));
			return FALSE ;
		}
	}
	if (m_LockData)
	{
		return TRUE;
	}

	if (m_HookData)
	{
		for (int i = 0; i < AutoYaoShui; i++)
		{
			if (entry && m_HookData->MPEntry[i] == entry)
			{
				//已经存在列表
				OutputDebugString("挂机设置：该药水已经在列表");
				CHookMgr::OutPutError(_TRAN("该道具已经在列表"));
				return FALSE ;
			}
		}

		if (pos >= AutoYaoShui)
		{
			OutputDebugString("挂机设置：该药水位置不存在");
			return FALSE ;
		}

		m_HookData->MPEntry[pos] = entry ;

		return TRUE ;
	}
	return FALSE ;
}
BOOL UHookSet::AddHookASkill(ui32 entry, ui32 pos)
{
	if (entry)
	{
		SpellTemplate* pkST = SYState()->SkillMgr->GetSpellTemplate(entry);
		if (!pkST)
		{
			OutputDebugString("挂机设置：没有找到该技能信息 \n");
			CHookMgr::OutPutError(_TRAN("没有这个技能"));
			return FALSE;
		}
		if (!(pkST->GetSelectMethod()&TARGET_FLAG_UNIT) || (pkST->GetSelectMethod()&TARGET_FLAG_SELF))
		{
			OutputDebugString("挂机设置无法使用该技能 \n");
			CHookMgr::OutPutError(_TRAN("不允许使用该技能"));
			return FALSE ;
		}

		if (entry == 88822401 || entry == 22401 || entry == 45001)
		{
			return FALSE ;
		}

		if (entry != NORMAL_ATTACK_SPELL_ID && entry != 5003 && !SpellTemplate::IsDamageSpell(entry))
		{
			CHookMgr::OutPutError(_TRAN("不允许使用该技能"));
			return FALSE;
		}
		if (SpellTemplate::IsChongZhuangSpell(entry))
		{
			CHookMgr::OutPutError(_TRAN("不允许使用该技能"));
			return FALSE;
		}
	}
	
	if (m_LockData)
	{
		return TRUE;
	}
	if (m_HookData)
	{
		for (int i = 0; i < AutoMaxSpell; i++)
		{
			if (entry && m_HookData->Spell[i] == entry)
			{
				//已经存在列表
				OutputDebugString("挂机设置：该技能已经在列表");
				CHookMgr::OutPutError(_TRAN("该技能已经在列表"));
				return FALSE ;
			}
		}

		if (pos >= AutoMaxSpell)
		{
			OutputDebugString("挂机设置：该技能位置不存在");
			return FALSE ;
		}

		m_HookData->Spell[pos] = entry ;

		return TRUE ;
	}
	return FALSE ;
}
BOOL UHookSet::AddHookHPSkill(ui32 entry, ui32 pos)
{

	if (entry)
	{
		SpellTemplate* pkST = SYState()->SkillMgr->GetSpellTemplate(entry);
		if (!pkST)
		{
			OutputDebugString("挂机设置：没有找到该技能信息 \n");
			CHookMgr::OutPutError(_TRAN("没有这个技能"));
			return FALSE;
		}
		if (!(pkST->GetSelectMethod()&TARGET_FLAG_SELF))
		{
			OutputDebugString("挂机设置无法使用该技能 \n");
			CHookMgr::OutPutError(_TRAN("需要回复气血技能"));
			return FALSE ;
		}

		if (entry == 88822401 || entry == 22401 || entry == 45001)
		{
			return FALSE ;
		}

		if (!SpellTemplate::IsHealingSpell(entry))
		{
			CHookMgr::OutPutError(_TRAN("需要回复气血技能"));
			return FALSE ;
		}
	}
	

	if (m_LockData)
	{
		return TRUE;
	}
	if (m_HookData)
	{
		m_HookData->HPSpell = entry ;
		return TRUE ;
	}
	return FALSE ;
}
BOOL UHookSet::AddHookMPSkill(ui32 entry, ui32 pos)
{
	if (entry)
	{
		SpellTemplate* pkST = SYState()->SkillMgr->GetSpellTemplate(entry);
		if (!pkST)
		{
			OutputDebugString("挂机设置:没有找到该技能信息 \n");
			CHookMgr::OutPutError(_TRAN("没有这个技能"));
			return FALSE;
		}
		if (!(pkST->GetSelectMethod()&TARGET_FLAG_SELF))
		{
			OutputDebugString("挂机设置:无法使用该技能 \n");
			CHookMgr::OutPutError(_TRAN("需要回复魔法技能"));
			return FALSE ;
		}

		if (entry == 88822401 || entry == 22401 || entry == 45001)
		{
			return FALSE ;
		}

		if (!SpellTemplate::IsMaNaSpell(entry))
		{
			CHookMgr::OutPutError(_TRAN("需要回复魔法技能"));
			return FALSE ;
		}
	}
	

	if (m_LockData)
	{
		return TRUE;
	}
	if (m_HookData)
	{
		m_HookData->MPSpell = entry ;
		return TRUE ;
	}
	return FALSE ;
}

void UHookSet::CheckAutoAttack()   //自动攻击
{
	BOOL bAuto = m_AutoAttack->IsChecked();
	if (m_HookData)
	{
		m_HookData->AutoAttack = bAuto ;

		for ( int i = 0; i < AutoMaxSpell; i ++)
		{
			m_SpellActionBtn[i]->SetActive(bAuto) ;
		}
	}
}
void UHookSet::CheckAutoSupply()   //自动补给
{
	BOOL bAuto = m_AutoSupply->IsChecked();
	if (m_HookData)
	{
		m_HookData->AutoSupply = bAuto;

		for (int i = 0; i < AutoYaoShui; i++)
		{
			m_MPItemActionBtn[i]->SetActive(bAuto);
			m_HPItemActionBtn[i]->SetActive(bAuto);
		}

		m_HPSlider->SetActive(bAuto);
		m_MPSlider->SetActive(bAuto);

	}
}
void UHookSet::CheckAutoPick()     //自动拾取
{
	BOOL bAuto = m_AutoPick->IsChecked();
	if (m_HookData)
	{
		m_HookData->AutoPick = bAuto ;
	}
}
void UHookSet::CheckDeadAutoPos()
{
	BOOL bAuto  = m_DeadAuto->IsChecked() ;
	if (m_HookData)
	{
		m_HookData->DeadAutoPos = bAuto ;
	}
}
void UHookSet::CheckAutoShunBoss() //自动回避BOSS
{
	BOOL bAuto = m_AutoShunBoss->IsChecked();
	if (m_HookData)
	{
		m_HookData->AutoShunBoss = bAuto ;
	}
}
void UHookSet::CheckUseSpellFrist()//使用技能首先
{
	BOOL bUseSpellFrist = m_UseSpellFrist->IsChecked();
	if (m_HookData)
	{
		m_HookData->UseSpellFirst = bUseSpellFrist ;
	}

}
void UHookSet::ChangHPSlider()  //HP
{
	float HP = m_HPSlider->GetFValue();
	if (m_HookData)
	{
		m_HookData->Hp = HP ;
	}
	UStaticText* pkHpText = UDynamicCast(UStaticText, GetChildByID(29));
	if (pkHpText)
	{
		//char buf[_MAX_PATH];
		//sprintf(buf, "HP低于%d%%补给", (int)(HP * 100));
		std::string strRet = _TRAN(N_NOTICE_Z8, _I2A((int)(HP * 100)).c_str() );
		pkHpText->SetText( strRet.c_str() );
	}
}
void UHookSet::ChangMPSlider()  //MP
{
	float MP = m_MPSlider->GetFValue();
	if (m_HookData)
	{
		m_HookData->Mp = MP;
	}

	UStaticText* pkHpText = UDynamicCast(UStaticText, GetChildByID(31));
	if (pkHpText)
	{
		//char buf[_MAX_PATH];
		//sprintf(buf, "MP低于%d%%补给", (int)(MP * 100));
		std::string strRet = _TRAN(N_NOTICE_Z9, _I2A((int)(MP * 100)).c_str() );
		pkHpText->SetText(strRet.c_str());
	}
}
void UHookSet::OnClose()
{
	UDialog::OnClose();
	SetVisible(FALSE);
}
BOOL UHookSet::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE ;
}

void UHookSet::SetVisible(BOOL value)
{
	UInGame* pInGame = UInGame::Get();
	if (pInGame)
	{
		UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
		if (pGameBar)
		{
			UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONSHOP);
			if (pBtn)
			{
				pBtn->SetCheckState(value);
			}
		}
	}
	UDialog::SetVisible(value);
}

void UHookSet::OnDestroy()
{
	m_HookData->ResetData();
	SetHookData(*m_HookData);
	UDialog::OnDestroy();
}
void UHookSet::HookActiveUI(BOOL bHook)
{
	// 如果状态机在运行 除了停止按钮 其他的都必须ACTIVE FALSE 
	for ( int i = 0; i < AutoMaxSpell; i ++)
	{
		if (bHook) //如果在运行
		{
			m_SpellActionBtn[i]->SetActive(FALSE);
		}else
		{
			BOOL bAuto = m_AutoAttack->IsChecked();
			m_SpellActionBtn[i]->SetActive(bAuto);
		}
		
	}
	for (int i = 0; i< AutoYaoShui; i++)
	{
		if (bHook)
		{
			m_HPItemActionBtn[i]->SetActive(FALSE);
			m_MPItemActionBtn[i]->SetActive(FALSE);
		}else
		{
			BOOL bAuto = m_AutoSupply->IsChecked();
			
			m_HPItemActionBtn[i]->SetActive(bAuto);
			m_MPItemActionBtn[i]->SetActive(bAuto);
		}
	}

	if (bHook)
	{
		m_HPActionBtn->SetActive(FALSE);
		m_MPActionBtn->SetActive(FALSE);
	}else
	{
		BOOL bAuto = m_UseSpellFrist->IsChecked();
		m_HPActionBtn->SetActive(bAuto);
		m_MPActionBtn->SetActive(bAuto);
	}


	m_AutoAttack->SetActive(!bHook) ;
	m_AutoSupply->SetActive(!bHook) ;
	m_AutoPick->SetActive(!bHook);
	m_AutoShunBoss->SetActive(!bHook);
	m_DeadAuto->SetActive(!bHook);
	m_UseSpellFrist->SetActive(!bHook) ;

	m_HPSlider->SetActive(!bHook);
	m_MPSlider->SetActive(!bHook);

	
	if (bHook)
	{
		//如果挂机
		//变成停止图标
		m_HookCancelOrStop->SetBitmap("Public\\end.skin") ; //取消挂机
		GetChildByID(36)->SetActive(FALSE);
	}else
	{
		//如果没有挂机
		//变成取消图标
		m_HookCancelOrStop->SetBitmap("Public\\Delete.skin") ; // 删除设置
		GetChildByID(36)->SetActive(TRUE);
	}
	m_HookStarte->SetActive(!bHook);
	
}
void UHookSet::StartHook()
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal->IsHook())
	{
		if (!pkLocal->IsDead())
		{
			m_HookData->HookPos = pkLocal->GetPosition();
			HookMgr->SetHookData(*m_HookData);
			HookMgr->BeginHookActor();
			SetVisible(FALSE);
		}else
		{
			CHookMgr::OutPutError(_TRAN("当前状态无法挂机"));
		}
	}
}
void UHookSet::SaveHookData()
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal->IsHook())
	{
		m_HookData->HookPos = pkLocal->GetPosition();
		HookMgr->SetLoadHookData(*m_HookData);
		HookMgr->SaveHookFile();
	}
}
void UHookSet::ReSetHookData()
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		if (pkLocal->IsHook())
		{
			HookMgr->EndHookActor();
		}else
		{
			m_HookData->ResetData();
			SetHookData(*m_HookData);
		}
	}
}
BOOL UHookSet::SetHookData(HookData& pkData)
{
	//先锁定数据
	m_LockData = TRUE;
	
	//在设置UI 

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		m_LockData = FALSE;
		return FALSE;
	}

	if (pkLocal->IsHook())
	{
		m_LockData = FALSE;
		return FALSE;
	}
	
	CStorage* pkBag = pkLocal->GetItemContainer();
	if (!pkBag)
	{
		m_LockData = FALSE;
		return FALSE ;
	}

	*m_HookData = pkData ;


	for ( int i = 0; i < AutoMaxSpell; i ++)
	{
		if (!m_SpellActionBtn[i])
		{
			m_LockData = FALSE;
			return FALSE ;
		}else
		{
			ActionDataInfo datainfo;
			datainfo.entry = m_HookData->Spell[i];
			datainfo.Guid = 0;
			datainfo.pos = i;
			datainfo.type = UItemSystem::SPELL_DATA_FLAG;

			m_SpellActionBtn[i]->SetItemData(datainfo);
		}
	}

	if (!m_HPActionBtn || !m_MPActionBtn)
	{
		m_LockData = FALSE;
		return FALSE ;
	}else
	{
		ActionDataInfo datainfo;
		datainfo.entry = m_HookData->HPSpell;
		datainfo.Guid = 0;
		datainfo.pos = 0;
		datainfo.type = UItemSystem::SPELL_DATA_FLAG;

		m_HPActionBtn->SetItemData(datainfo);
		datainfo.entry = m_HookData->MPSpell;
		m_MPActionBtn->SetItemData(datainfo);
	}

	
	for (int i = 0; i< AutoYaoShui; i++)
	{
		if (!m_HPItemActionBtn[i]  || !m_MPItemActionBtn[i])
		{
			m_LockData = FALSE;
			return FALSE ;
		}else
		{
			ActionDataInfo datainfo;
			datainfo.entry = m_HookData->HPEntry[i];
			datainfo.Guid = 0;
			datainfo.pos = i;
			datainfo.type = UItemSystem::ITEM_DATA_FLAG;

			if (datainfo.entry)
			{
				datainfo.num = pkBag->GetItemCnt(datainfo.entry);
			}else
			{
				datainfo.num = 0;
			}

			m_HPItemActionBtn[i]->SetItemData(datainfo);


			datainfo.entry = m_HookData->MPEntry[i];
			if (datainfo.entry)
			{
				datainfo.num = pkBag->GetItemCnt(datainfo.entry);
			}else
			{
				datainfo.num = 0;
			}
			m_MPItemActionBtn[i]->SetItemData(datainfo);
		}
	}

	if (!m_HPSlider || !m_MPSlider)
	{
		m_LockData = FALSE;
		return FALSE ;
	}else
	{
		m_HPSlider->SetRange(0,100);
		m_HPSlider->SetValue(int(m_HookData->Hp * 100));

		m_MPSlider->SetRange(0,100);
		m_MPSlider->SetValue(int(m_HookData->Mp * 100));
	}

	if (!m_AutoAttack || !m_AutoSupply || !m_AutoPick || !m_AutoShunBoss || !m_UseSpellFrist || !m_DeadAuto)
	{
		m_LockData = FALSE;
		return FALSE ;
	}else
	{
		m_AutoAttack->SetCheck(!m_HookData->AutoAttack);
		m_AutoSupply->SetCheck(!m_HookData->AutoSupply);
		m_AutoPick->SetCheck(!m_HookData->AutoPick);
		m_AutoShunBoss->SetCheck(!m_HookData->AutoShunBoss);
		m_UseSpellFrist->SetCheck(!m_HookData->UseSpellFirst);
		m_DeadAuto->SetCheck(!m_HookData->DeadAutoPos);
	}

	m_LockData = FALSE ;
	return TRUE ;

}