#ifndef __UHOOKSET_H__
#define __UHOOKSET_H__

#include "UInc.h"
#include "HookManager.h"
#include "ui/UDynamicIconButtonEx.h"
enum
{
	Hook_Spell_0 = 0,
	Hook_Spell_1 = 1,
	Hook_Spell_2 = 2,
	Hook_Spell_3 = 3,
	Hook_Spell_4 = 4,
	
	Hook_HP_Item_0 = 5,
	Hook_HP_Item_1 = 6,
	Hook_HP_Item_2 = 7,

	Hook_MP_Item_0 = 8,
	Hook_MP_Item_1 = 9,
	Hook_MP_Item_2 = 10,


	Hook_HP_Spell  = 11,
	Hook_MP_Spell  = 12,



};
class UHookSet : public UDialog
{
	UDEC_CLASS(UHookSet);
	UDEC_MESSAGEMAP();
public:
	UHookSet();
	~UHookSet();

	BOOL SetHookData(HookData& pkData);  
	BOOL IsLockData(){return m_LockData;}
	virtual void SetVisible(BOOL value);
	// Action BTN 
	class ActionButton : public UDynamicIconButtonEx
	{
		UDEC_CLASS(UHookSet::ActionButton);
	public:
		ActionButton();
		virtual ~ActionButton();
	public:
		virtual void FouceUse();
	protected:
		virtual BOOL OnCreate();
		virtual void OnRender(const UPoint& offset, const URect &updateRect);
		virtual void OnRenderGlint(const UPoint& offset,const URect &updateRect);
		virtual void OnTimer(float fDeltaTime);
		virtual void OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag){};
	protected:
		UTexturePtr m_GlintTexture ;
	};

protected:
	// skill
	class SkillActionBtn : public ActionButton
	{
		UDEC_CLASS(UHookSet::SkillActionBtn);
	public:
		SkillActionBtn();
		virtual ~SkillActionBtn();
		virtual void SetItemData(ActionDataInfo &aiteminfo);
		virtual void SetItemData(UActionContener &contener);
	protected:
		virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
	};
	
	// item
	class ItemActionBtn : public ActionButton
	{
		UDEC_CLASS(UHookSet::ItemActionBtn);
	public:
		ItemActionBtn();
		virtual ~ItemActionBtn();
		virtual void SetItemData(ActionDataInfo &aiteminfo);
		virtual void SetItemData(UActionContener &contener);
	protected:
		virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
	};

	//data 
	class ActionBtnItem : public UActionItem
	{
	public:
		virtual void Use();
	};

	BOOL virtual OnCreate();
	void virtual OnDestroy();
	void virtual OnClose();
	BOOL virtual OnEscape();


protected:
	void StartHook();
	void ReSetHookData();
	void SaveHookData();

	void CheckAutoAttack();   //自动攻击
	void CheckAutoSupply();   //自动补给
	void CheckAutoPick();     //自动拾取
	void CheckAutoShunBoss(); //自动回避BOSS
	void CheckDeadAutoPos();
	void CheckUseSpellFrist();//使用技能首先

	void ChangHPSlider();  //HP
	void ChangMPSlider();  //MP


public:
	BOOL AddHookHPItem(ui32 entry, ui32 pos);
	BOOL AddHookMPItem(ui32 entry, ui32 pos);

	BOOL AddHookASkill(ui32 entry, ui32 pos);
	BOOL AddHookHPSkill(ui32 entry, ui32 pos);
	BOOL AddHookMPSkill(ui32 entry, ui32 pos);


	void HookActiveUI(BOOL bHook);
private:
	SkillActionBtn* m_SpellActionBtn[AutoMaxSpell];
	SkillActionBtn* m_HPActionBtn;
	SkillActionBtn* m_MPActionBtn;


	ItemActionBtn* m_HPItemActionBtn[AutoYaoShui];
	ItemActionBtn* m_MPItemActionBtn[AutoYaoShui];

	UButton* m_AutoAttack ;
	UButton* m_AutoSupply ;
	UButton* m_AutoPick;
	UButton* m_AutoShunBoss;
	UButton* m_DeadAuto;
	UButton* m_UseSpellFrist ;

	USlider* m_HPSlider;
	USlider* m_MPSlider;

	UBitmapButton* m_HookStarte ;  //HOOK 控制按钮 包括保存 开始
	UBitmapButton* m_HookCancelOrStop;// HOOK 控制按钮 包括停止 取消

	HookData* m_HookData ;
	
	BOOL m_LockData ; 
};

#endif