#include "StdAfx.h"
#include "ItemManager.h"
#include "Network/PacketBuilder.h"
#include "ui/UISystem.h"
#include "ObjectManager.h"
#include "ItemSlot.h"
#include "EquipSlot.h"
#include "ui/UIItemSystem.h"
#include "Console.h"
#include "ui/UIGamePlay.h"
#include "SYItem.h"
#include "Utils/DefaultDisplayInfoDB.h"
#include "ResourceManager.h"
#include "ui/UMessageBox.h"
#include "UI/UIPlayerTrade.h"
#include "AudioInterface.h"
#include "ui/UIShowPlayer.h"
#include "ui/UIPlayerEquipment.h"
#include "ui/UFun.h"
#include "ui/UISkill.h"
#include "ui/UInstanceMgr.h"

//void EquipItem(std::vector<std::string>& args)
//{
//	NIASSERT(args.size() == 1);
//	PacketBuilder->SendEquipItemMsg(INVENTORY_SLOT_NOT_SET, INVENTORY_SLOT_ITEM_START + atoi(args[0].c_str()));
//}
//RegistConsoleFun(EquipItem);


CItemManager::CItemManager()
:m_pkItemDB(NULL)
,m_pkDisplayInfoDB(NULL)
,m_pkCreatureNameDB(NULL)
,m_pkItenExtendedConstry(NULL)
,m_IsShowShizhuang(true)
{
	m_pkTradeItem.clear();
}

CItemManager::~CItemManager()
{
}

static bool MsgBoxDBError(CDBFile* pDBFile, const char* txt)
{
	if(pDBFile && !pDBFile->Load(txt))
	{
		char buf[256];
		sprintf(buf, "load DBFilse:%s failed!",  txt);
		MessageBox(NULL, buf, "error", MB_OK);
		return false;
	}
	return true;
}
bool CItemManager::Initialize()
{
	bool bLoadDBFile = true;
	m_pkItemDB = NiNew CItemDB("");//("DBFiles\\Item_template.DB");
	NIASSERT(m_pkItemDB);
	if(!(bLoadDBFile &= MsgBoxDBError(m_pkItemDB ,"DBFiles\\Item_template.DB")))
		return false;

	m_pkDisplayInfoDB = NiNew CDisplayInfoDB;
	NIASSERT(m_pkDisplayInfoDB);
	if(!(bLoadDBFile &= MsgBoxDBError(m_pkDisplayInfoDB ,"DBFiles\\displayinfo.db")))
		return false;

	m_pkCreatureNameDB = NiNew CCreatureNameDB;
	NIASSERT(m_pkCreatureNameDB);
	if(!(bLoadDBFile &= MsgBoxDBError(m_pkCreatureNameDB ,"DBFiles\\creature_names.db")))
		return false;

	m_pkItenExtendedConstry = NiNew ItemExtendedCostentry;
	NIASSERT(m_pkItenExtendedConstry);
	if(!(bLoadDBFile &= MsgBoxDBError(m_pkItenExtendedConstry ,"DBFiles\\itemextendedcostentry_dbc.db")))
		return false;

	return bLoadDBFile;
}

void CItemManager::Destroy()
{
	if(m_pkItemDB)
	{
		NiDelete m_pkItemDB;
		m_pkItemDB = NULL;
	}

	if(m_pkDisplayInfoDB)
	{
		NiDelete m_pkDisplayInfoDB;
		m_pkDisplayInfoDB = NULL;
	}

	if(m_pkCreatureNameDB)
	{
		NiDelete m_pkCreatureNameDB;
		m_pkCreatureNameDB = NULL;
	}
	if (m_pkItenExtendedConstry)
	{
		NiDelete m_pkItenExtendedConstry;
		m_pkItenExtendedConstry = NULL;
	}
}

void EquipSound(ui8 srcSlot)
{
	switch(srcSlot)
	{
	case EQUIPMENT_SLOT_MAINHAND:
	case EQUIPMENT_SLOT_OFFHAND:
	case 21:
		{
			SYState()->IAudio->PlayUiSound(UI_weaponequip, NiPoint3::ZERO, 2.f, 1 );
			HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_FIGHT);
		}
		break;
	case EQUIPMENT_SLOT_NECK:
	case EQUIPMENT_SLOT_FINGER1:
	case EQUIPMENT_SLOT_TRINKET1:
	case EQUIPMENT_SLOT_FINGER2:
	case EQUIPMENT_SLOT_TRINKET2:
		{
			SYState()->IAudio->PlayUiSound(UI_neckequip, NiPoint3::ZERO, 2.f, 1 );
		}
		break;
	case EQUIPMENT_SLOT_HEAD:
	case EQUIPMENT_SLOT_GLOVES:
	case EQUIPMENT_SLOT_CHEST:
	case EQUIPMENT_SLOT_TROUSERS:
	case EQUIPMENT_SLOT_BOOTS:
	case EQUIPMENT_SLOT_SHOULDERS:
	case EQUIPMENT_SLOT_WAIST:
		{
			SYState()->IAudio->PlayUiSound(UI_fabricequip, NiPoint3::ZERO, 2.f, 1 );
		}
		break;
	case EQUIPMENT_HOUNTER_BEIBU:
	case EQUIPMENT_HOUNTER_TOUBU:
	case EQUIPMENT_HOUNTER_JIANBANG:
	case EQUIPMENT_HOUNTER_SHANGYI:
	case EQUIPMENT_HOUNTER_XIAYI:
	case EQUIPMENT_HOUNTER_XIEZI:
		{
			SYState()->IAudio->PlayUiSound(UI_skinequip, NiPoint3::ZERO, 2.f, 1 );
		}
		break;
	default:
			SYState()->IAudio->PlayUiSound(UI_neckequip, NiPoint3::ZERO, 2.f, 1 );
		break;
	}
}
void _EquipSound(ui8 srcIntove)
{
	switch(srcIntove)
	{
	case INVTYPE_HEAD:
	case INVTYPE_GLOVES:
	case INVTYPE_CHEST:
	case INVTYPE_TROUSERS:
	case INVTYPE_BOOTS:  
	case INVTYPE_SHOULDERS:
	case INVTYPE_WAIST:
		{
			SYState()->IAudio->PlayUiSound(UI_fabricequip);
		}
		break;
	case INVTYPE_NECK:
	case INVTYPE_FINGER:
	case INVTYPE_UNUSER1:
	case INVTYPE_TRINKET:
	case INVTYPE_UNUSER2:	
		{
			SYState()->IAudio->PlayUiSound(UI_neckequip);
		}
		break;
	case INVTYPE_HOUNTER_BEIBU:
	case INVTYPE_HOUNTER_TOUBU:
	case INVTYPE_HOUNTER_JIANBANG:
	case INVTYPE_HOUNTER_SHANGYI:
	case INVTYPE_HOUNTER_XIAYI:
	case INVTYPE_HOUNTER_XIEZI:
		{
			SYState()->IAudio->PlayUiSound(UI_skinequip);
		}
		break;
	case INVTYPE_WEAPONMAINHAND:
	case INVTYPE_WEAPONOFFHAND:
	case INVTYPE_SHIELD:
	case INVTYPE_2HWEAPON:
	case INVTYPE_BAOZHU:
		{
			SYState()->IAudio->PlayUiSound(UI_weaponequip);
		}
		break;
	case INVTYPE_BAG:
		{

		}
		break;
	}
}
// 物品移动消息
void CItemManager::ItemMoveReq(ui8 dwIctFrom, ui8 dwIctTo, ui8 wPosFrom, int wPosTo, ui8 wNum)
{
	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if (localPlayer)
	{
		CStorage* pItemSoltContainer = localPlayer->GetItemContainer();
        CStorage* pBankItemContainer = localPlayer->GetBankContainer();
		if (pItemSoltContainer == NULL || pBankItemContainer == NULL)
		{
			return;
		}

		ui8 sFromBag ;
		ui8 sToBag ;
		ui8 sFromPos ;
		ui8 sToPos ;

		if (dwIctFrom == dwIctTo && wPosFrom == wPosTo)
		{
			return ;
		}

		if(dwIctFrom== UItemSystem::ICT_BAG && dwIctTo == UItemSystem::ICT_EQUIP)
		{
			if(localPlayer->GetUInt32Value(UNIT_FIELD_FORMDISPLAYID))
			{
				ClientSystemNotify(_TRAN("变身状态无法装备道具"));
				return;
			}
			CItemSlot* pFromSlot = (CItemSlot*)pItemSoltContainer->GetSlot(wPosFrom);
			assert(pFromSlot);
			ItemPrototype_Client* ItemInfo = GetItemPropertyFromDataBase(pFromSlot->GetCode());

			if (ItemInfo && pItemSoltContainer->GetServerBag(wPosFrom) != -1 && pItemSoltContainer->GetServerPos(wPosFrom) != -1)
			{
				sFromBag = pItemSoltContainer->GetServerBag(wPosFrom) ;
				sFromPos = pItemSoltContainer->GetServerPos(wPosFrom);
				EquipSound(ItemInfo->InventoryType);
				PacketBuilder->SendEquipItemMsg(sFromBag, sFromPos);
			}
			return ;
		}
		else if(dwIctFrom == UItemSystem::ICT_EQUIP && dwIctTo == UItemSystem::ICT_BAG)
		{
			if(localPlayer->GetUInt32Value(UNIT_FIELD_FORMDISPLAYID))
			{
				ClientSystemNotify(_TRAN("变身状态无法取下道具"));
				return;
			}

			//if (pItemSoltContainer->GetServerBag(wPosTo) != -1 && pItemSoltContainer->GetServerPos(wPosTo) != -1 )
			//{
			//	sToBag = pItemSoltContainer->GetServerBag(wPosTo);
			//	sToPos = pItemSoltContainer->GetServerPos(wPosTo);

			EquipSound(wPosFrom + EQUIPMENT_SLOT_START);
				PacketBuilder->SendItemMovePosReqMsg(INVENTORY_SLOT_NOT_SET,INVENTORY_SLOT_NOT_SET,wPosFrom + EQUIPMENT_SLOT_START,-1, 1);
			//}
			return ;
		}
		else if(dwIctFrom == UItemSystem::ICT_BAG && dwIctTo == UItemSystem::ICT_BAG)
		{

			if (pItemSoltContainer->GetServerBag(wPosFrom) != -1 && pItemSoltContainer->GetServerBag(wPosTo) != -1
				&& pItemSoltContainer->GetServerPos(wPosFrom)!= -1 && pItemSoltContainer->GetServerPos(wPosTo) != -1)
			{
				sFromBag = pItemSoltContainer->GetServerBag(wPosFrom);
				sToBag =   pItemSoltContainer->GetServerBag(wPosTo) ;
				sFromPos = pItemSoltContainer->GetServerPos(wPosFrom);
				sToPos =   pItemSoltContainer->GetServerPos(wPosTo);

				PacketBuilder->SendItemMovePosReqMsg(sFromBag, sToBag,sFromPos,sToPos,wNum);

			}
			return ;
		}else if (dwIctFrom == UItemSystem::ICT_BAG && dwIctTo == UItemSystem::ICT_BAG_SOLT)
		{
			if (pItemSoltContainer->GetServerBag(wPosFrom) != -1 &&  pItemSoltContainer->GetServerPos(wPosFrom)!= -1)
			{
				
				CItemSlot* pFromSlot = (CItemSlot*)pItemSoltContainer->GetSlot(wPosFrom);
				assert(pFromSlot);
				ItemPrototype_Client* ItemInfo = GetItemPropertyFromDataBase(pFromSlot->GetCode());
				if (ItemInfo && ItemInfo->Class == ITEM_CLASS_CONTAINER)
				{
					sFromBag = pItemSoltContainer->GetServerBag(wPosFrom) ;
					sFromPos = pItemSoltContainer->GetServerPos(wPosFrom);
					PacketBuilder->SendItemMovePosReqMsg(sFromBag,INVENTORY_SLOT_NOT_SET,sFromPos,wPosTo,1);	
				}
			}
			return ;
		}else if (dwIctFrom == UItemSystem::ICT_BAG_SOLT && dwIctTo == UItemSystem::ICT_BAG)
		{
			if (pItemSoltContainer->GetServerBag(wPosTo) != -1 && pItemSoltContainer->GetServerPos(wPosTo) != -1)
			{
				sToBag =   pItemSoltContainer->GetServerBag(wPosTo) ;
				sToPos =   pItemSoltContainer->GetServerPos(wPosTo);
				
				//判断这个位置是否有物品，如有有物品且物品不是包的话，返回。
				CItemSlot* pToSlot = (CItemSlot*)pItemSoltContainer->GetSlot(wPosTo);
				assert(pToSlot);
				ItemPrototype_Client* ItemInfo = GetItemPropertyFromDataBase(pToSlot->GetCode());
				if (ItemInfo && ItemInfo->Class != ITEM_CLASS_CONTAINER)
				{
					return ;
				}

				if (sToBag == wPosFrom)
				{
					UMessageBox::MsgBox_s(_TRAN("错误的操作!"));
					return;
				}
				PacketBuilder->SendItemMovePosReqMsg(INVENTORY_SLOT_NOT_SET,sToBag,wPosFrom,sToPos,1);
			}
			return ;
		}else if (dwIctFrom == UItemSystem::ICT_BANK && dwIctTo == UItemSystem::ICT_BAG)
		{

			if (pBankItemContainer->GetServerBag(wPosFrom) != -1 && pItemSoltContainer->GetServerBag(wPosTo) != -1
				&& pBankItemContainer->GetServerPos(wPosFrom)!= -1 && pItemSoltContainer->GetServerPos(wPosTo) != -1)
			{
				sFromBag = pBankItemContainer->GetServerBag(wPosFrom) ;
				sToBag =   pItemSoltContainer->GetServerBag(wPosTo) ;
				sFromPos = pBankItemContainer->GetServerPos(wPosFrom);
				sToPos =   pItemSoltContainer->GetServerPos(wPosTo);

				PacketBuilder->SendItemMovePosReqMsg(sFromBag, sToBag,sFromPos,sToPos,wNum);	
			}
			return ;
		}else if (dwIctFrom == UItemSystem::ICT_BAG && dwIctTo == UItemSystem::ICT_BANK)
		{

			if (pItemSoltContainer->GetServerBag(wPosFrom) != -1 && pBankItemContainer->GetServerBag(wPosTo) != -1
				&& pItemSoltContainer->GetServerPos(wPosFrom)!= -1 && pBankItemContainer->GetServerPos(wPosTo) != -1)
			{
				sFromBag = pItemSoltContainer->GetServerBag(wPosFrom) ;
				sToBag =   pBankItemContainer->GetServerBag(wPosTo) ;
				sFromPos = pItemSoltContainer->GetServerPos(wPosFrom);
				sToPos =   pBankItemContainer->GetServerPos(wPosTo);

				PacketBuilder->SendItemMovePosReqMsg(sFromBag, sToBag,sFromPos,sToPos,wNum);
			}
			return ;
		}else if (dwIctFrom == UItemSystem::ICT_BANK && dwIctTo == UItemSystem::ICT_BANK)
		{

			if (pBankItemContainer->GetServerBag(wPosFrom) != -1 && pBankItemContainer->GetServerBag(wPosTo) != -1
				&& pBankItemContainer->GetServerPos(wPosFrom)!= -1 && pBankItemContainer->GetServerPos(wPosTo) != -1)
			{
				sFromBag = pBankItemContainer->GetServerBag(wPosFrom) ;
				sToBag =   pBankItemContainer->GetServerBag(wPosTo) ;
				sFromPos = pBankItemContainer->GetServerPos(wPosFrom);
				sToPos =   pBankItemContainer->GetServerPos(wPosTo);

				PacketBuilder->SendItemMovePosReqMsg(sFromBag, sToBag,sFromPos,sToPos,wNum);
			}
			return ;
		}else if (dwIctFrom == UItemSystem::ICT_BAG && dwIctTo == UItemSystem::ICT_BANK_SOLT)
		{

			if (pItemSoltContainer->GetServerBag(wPosFrom) != -1 && pItemSoltContainer->GetServerPos(wPosFrom)!= -1 )
			{
				CItemSlot* pFromSlot = (CItemSlot*)pItemSoltContainer->GetSlot(wPosFrom);
				assert(pFromSlot);
				ItemPrototype_Client* ItemInfo = GetItemPropertyFromDataBase(pFromSlot->GetCode());
				if (ItemInfo && ItemInfo->Class == ITEM_CLASS_CONTAINER)
				{
					sFromBag = pItemSoltContainer->GetServerBag(wPosFrom) ;
					sFromPos = pItemSoltContainer->GetServerPos(wPosFrom);

					PacketBuilder->SendItemMovePosReqMsg(sFromBag,INVENTORY_SLOT_NOT_SET,sFromPos,wPosTo,1);
				}
				
			}
			return ;
		}else if (dwIctFrom == UItemSystem::ICT_BANK && dwIctTo == UItemSystem::ICT_BANK_SOLT)
		{
			if (pBankItemContainer->GetServerBag(wPosFrom) != -1 && pBankItemContainer->GetServerPos(wPosFrom)!= -1 )
			{
				//判断这个位置是否有物品，如有有物品且物品不是包的话，返回。
				CItemSlot* pFromSlot = (CItemSlot*)pBankItemContainer->GetSlot(wPosFrom);
				assert(pFromSlot);
				ItemPrototype_Client* ItemInfo = GetItemPropertyFromDataBase(pFromSlot->GetCode());
				if (ItemInfo && ItemInfo->Class == ITEM_CLASS_CONTAINER)
				{
					sFromBag = pBankItemContainer->GetServerBag(wPosFrom);
					sFromPos = pBankItemContainer->GetServerPos(wPosFrom);

					PacketBuilder->SendItemMovePosReqMsg(sFromBag,INVENTORY_SLOT_NOT_SET,sFromPos,wPosTo,1);
				}
				
			}
			return;
		}else if (dwIctFrom == UItemSystem::ICT_BANK_SOLT && dwIctTo == UItemSystem::ICT_BAG)
		{
			if (pItemSoltContainer->GetServerBag(wPosTo) != -1 && pItemSoltContainer->GetServerPos(wPosTo) != -1)
			{
				//判断这个位置是否有物品，如有有物品且物品不是包的话，返回。
				CItemSlot* pToSlot = (CItemSlot*)pItemSoltContainer->GetSlot(wPosTo);
				assert(pToSlot);
				ItemPrototype_Client* ItemInfo = GetItemPropertyFromDataBase(pToSlot->GetCode());
				if (ItemInfo && ItemInfo->Class != ITEM_CLASS_CONTAINER)
				{
					return ;
				}
				sToBag = pItemSoltContainer->GetServerBag(wPosTo);
				sToPos = pItemSoltContainer->GetServerPos(wPosTo);

				PacketBuilder->SendItemMovePosReqMsg(INVENTORY_SLOT_NOT_SET,sToBag,wPosFrom,sToPos,1);
			}
			return;
		}else if (dwIctFrom == UItemSystem::ICT_BANK_SOLT && dwIctTo == UItemSystem::ICT_BANK)
		{
			if (pBankItemContainer->GetServerBag(wPosTo) != -1 && pBankItemContainer->GetServerPos(wPosTo) != -1)
			{

				CItemSlot* pToSlot = (CItemSlot*)pBankItemContainer->GetSlot(wPosTo);
				assert(pToSlot);
				ItemPrototype_Client* ItemInfo = GetItemPropertyFromDataBase(pToSlot->GetCode());
				if (ItemInfo && ItemInfo->Class != ITEM_CLASS_CONTAINER)
				{
					return ;
				}

				sToBag = pBankItemContainer->GetServerBag(wPosTo);
				sToPos = pBankItemContainer->GetServerPos(wPosTo);

				if (sToBag == wPosFrom)
				{
					UMessageBox::MsgBox_s(_TRAN("错误的操作"));
					return;
				}
				PacketBuilder->SendItemMovePosReqMsg(INVENTORY_SLOT_NOT_SET,sToBag,wPosFrom,sToPos,1);
			}
			return;
		}
	}

}

//取消交易
void CItemManager::TradeConToBagCon()
{
	//情况交易列表。并且对所有的交易列表的数据进行释放锁定
	CPlayer* pLocal = ObjectMgr->GetLocalPlayer();
	if (!pLocal)
	{
		return ;
	}

	CStorage* pPackageContainer = pLocal->GetItemContainer();
	CPlayerTradeContainer* pTradeContainer = (CPlayerTradeContainer*)pLocal->GetContainer(ICT_PLAYERTRADE);
	NIASSERT(pTradeContainer);

	for (int i= 0; i < pTradeContainer->GetMaxSlotNum(); i++)
	{
		CItemSlot* pSlot = (CItemSlot*)pTradeContainer->GetSlot(i);
		if (pSlot->GetCode() != 0)
		{
			//解除锁定。
			CItemSlot* pkBagSlot = (CItemSlot*)pPackageContainer->GetSlotByGuid(pSlot->GetGUID());
			assert(pkBagSlot);
			pkBagSlot->SetLock(FALSE);
			SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG,pPackageContainer->GetUIPos(pkBagSlot->GetGUID()),pkBagSlot->GetCode(),pkBagSlot->GetNum());
		}
	}

	pTradeContainer->ClearAll();
}
//清空交易列表里的信息
bool CItemManager::ClearPlayerTradeContainer()
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	if(!pkPlayer)
		return false;

	CSlotContainer* pkContainer = pkPlayer->GetContainer(ICT_PLAYERTRADE);
	NIASSERT(pkContainer);

	pkContainer->ClearAll();
	
	//清空Bag的锁定信息
	pkPlayer->ReleaseLock();
	return true;
}
void CItemManager::AddTradeSucesseMsg()
{
	ClientSystemNotify(_TRAN("交易成功"));
	for (size_t ui =0; ui < m_pkTradeItem.size(); ui++)
	{
		ItemPrototype_Client* pkItemInfo = GetItemPropertyFromDataBase(m_pkTradeItem[ui].entry);
		if (pkItemInfo && m_pkTradeItem[ui].entry)
		{
			std::string strRet = _TRAN(N_NOTICE_Z10, _I2A(m_pkTradeItem[ui].stackcount).c_str(), pkItemInfo->C_name.c_str() );
			ClientSystemNotify( strRet.c_str() );
		}
	}
	m_pkTradeItem.clear();
}
void CItemManager::OnBankItemChange(ui8 bag, ui8 slot, ui64 guid)
{	
	if (guid)
	{
		AddItemToBank(bag,slot, guid);
	}else
	{
		RemoveItemFromBank(bag,slot);
	}
}
void CItemManager::AddItemToBank(ui8 bag, ui8 slot, ui64 guid)
{
	CPlayer* pLoclPlayer = ObjectMgr->GetLocalPlayer();
	if (!pLoclPlayer)
	{
		return;
	}
	
	SYItem* pkSYItem = (SYItem*)ObjectMgr->GetObject(guid);
	
	if( !pkSYItem || ( !pkSYItem->isType(TYPE_ITEM) && !pkSYItem->isType(TYPE_CONTAINER) ) )
	{
		return;
	}

	int pos  = slot ;
	if (bag == INVENTORY_SLOT_NOT_SET)
	{
		pos = pos - BANK_SLOT_ITEM_START;
	}

	CStorage* pkBankContainer = pLoclPlayer->GetBankContainer();
	CItemSlot* pkItemSlot = (CItemSlot*)pkBankContainer->GetSlot(bag,slot);
	CItemSlot kItemSlot(pkSYItem);
	kItemSlot.SetPos(pos);

	assert(pkBankContainer->GetContainerByIndex(bag));

	if(pkItemSlot && pkItemSlot->GetCode() != 0)
	{
		pkBankContainer->GetContainerByIndex(bag)->UpdateSlot(pos, kItemSlot);
	}
	else
	{
		pkBankContainer->GetContainerByIndex(bag)->InsertSlot(pos, kItemSlot);
	}

	SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BANK,pkBankContainer->GetUIPos(bag,slot),pkItemSlot->GetCode(),pkItemSlot->GetNum());
}
void CItemManager::RemoveItemFromBank(ui8 bag, ui8 slot)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return;

	// GetItemContainer
	CStorage* pkBankContainer = pkPlayer->GetBankContainer();
	if (pkBankContainer == NULL)
		return;

	int pos  = slot ;
	if (bag == INVENTORY_SLOT_NOT_SET)
	{
		pos = pos - BANK_SLOT_ITEM_START;
	}

	assert(pkBankContainer->GetContainerByIndex(bag));
	pkBankContainer->GetContainerByIndex(bag)->DeleteSlot(pos, NULL);
	SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BANK,pkBankContainer->GetUIPos(bag,slot),0,0);
}
void CItemManager::OnItemChange(ui8 bag, ui8 slot, ui64 guid)
{
	if( guid )
	{
		AddItemToBackPack(bag, slot , guid);
	}
	else
	{
		RemoveItemFromBackPack(bag, slot);
	}
	
	UIManufacture* pManuFacture = INGAMEGETFRAME(UIManufacture, FRAME_IG_MANUFACTURE);
	if (pManuFacture)
	{
		pManuFacture->UpdateVisibleList();
		pManuFacture->UpdateManuFacture();
	}
	USpecialityDlg* pSpecialityDlg = INGAMEGETFRAME(USpecialityDlg, FRAME_IG_SPECIALITYDLG);
	if (pSpecialityDlg)
	{
		pSpecialityDlg->SetCurList();
	}
	InstanceSys->OnUpdateFairGroundIcon();
	//char pC[255];
	//NiSprintf(pC,255,"Additem %d__%d__%d",bag,slot,guid);
	//ULOG(pC);
}

// 添加道具
void CItemManager::AddItemToBackPack(ui8 bag, ui8 slot, ui64 guid)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return;

	SYItem* pkSYItem = (SYItem*)ObjectMgr->GetObject(guid);
	
	if( !pkSYItem || ( !pkSYItem->isType(TYPE_ITEM) && !pkSYItem->isType(TYPE_CONTAINER) ) )
	{
		return;
	}

	int pos  = slot;
	if (bag == INVENTORY_SLOT_NOT_SET)
	{
		pos = pos - INVENTORY_SLOT_ITEM_START;
	}

	CStorage* pkPackageContainer = pkPlayer->GetItemContainer();
	CItemSlot* pkItemSlot = (CItemSlot*)pkPackageContainer->GetSlot(bag,slot);
	CItemSlot kItemSlot(pkSYItem);
	kItemSlot.SetPos(pos);
	assert(pkPackageContainer->GetContainerByIndex(bag));



	if(pkItemSlot && pkItemSlot->GetCode() != 0)
	{
		pkPackageContainer->GetContainerByIndex(bag)->UpdateSlot(pos, kItemSlot);
	}
	else
	{
		pkPackageContainer->GetContainerByIndex(bag)->InsertSlot(pos, kItemSlot);
	}

	
	SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG, pkPackageContainer->GetUIPos(bag,slot), kItemSlot.GetCode(),kItemSlot.GetNum(),UItemSystem::ITEM_DATA_FLAG,kItemSlot.IsLocked());
}

// 删除道具
void CItemManager::RemoveItemFromBackPack(ui8 bag, ui8 slot)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return;

	// GetItemContainer
	CStorage* pkItemContainer = pkPlayer->GetItemContainer();
	if (pkItemContainer == NULL)
		return;

	int pos  = slot;
	if (bag == INVENTORY_SLOT_NOT_SET)
	{
		pos = pos - INVENTORY_SLOT_ITEM_START;
	}

	assert(pkItemContainer->GetContainerByIndex(bag));
	pkItemContainer->GetContainerByIndex(bag)->DeleteSlot(pos, NULL);
	
	SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG, pkItemContainer->GetUIPos(bag,slot),0,0);
}

// 填加道具到装备栏
void CItemManager::AddItemToEquipmentContainer(ui8 slot, ui32 itemid,ui64 guid)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	if(!pkPlayer)
		return;

	if( itemid == 0 )
	{
		RemoveItemFromEquip(slot);
		SYState()->UI->GetUItemSystem()->SetUIIcon(ICT_EQUIP, slot, 0,0,UItemSystem::ITEM_DATA_FLAG);
	}

	ui8 index = slot - EQUIPMENT_SLOT_START;

	SYItem* pkSYItem = (SYItem*)ObjectMgr->GetObject(guid);

	if( !pkSYItem || ( !pkSYItem->isType(TYPE_ITEM) && !pkSYItem->isType(TYPE_CONTAINER) ) )
	{
		return;
	}
	// GetContainer
	CEquipmentContainer* pkEquipContainer = (CEquipmentContainer*)pkPlayer->GetContainer(ICT_EQUIP);
	CEquipSlot* pkEquipSlot = (CEquipSlot*)pkEquipContainer->GetSlot(index);
	CEquipSlot kItemSlot(pkSYItem);
	kItemSlot.SetPos(index);
	if(pkEquipSlot && pkEquipSlot->GetCode() != 0)
	{
		pkEquipContainer->UpdateSlot(index, kItemSlot);
	}
	else
	{
		pkEquipContainer->InsertSlot(index, kItemSlot);
	}

	SYState()->UI->GetUItemSystem()->SetUIIcon(ICT_EQUIP, slot, kItemSlot.GetCode(),kItemSlot.GetNum(),UItemSystem::ITEM_DATA_FLAG);
}

void CItemManager::RemoveItemFromEquip(ui8 slot)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return;

	// GetItemContainer
	CEquipmentContainer* pContainer = (CEquipmentContainer*)pkPlayer->GetContainer(ICT_EQUIP);
	ui8 index = slot - EQUIPMENT_SLOT_START;
	pContainer->DeleteSlot(index, NULL);

//	SYState()->UI->GetItemSystem()->SetItemToMap(index);
}
// 丢弃背包物品
void CItemManager::DropBackPackItemReq(ui8 wPos)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return;

	
	CStorage* pkItemContainer = pkPlayer->GetItemContainer();
	
	if (pkItemContainer->GetServerPos(wPos) != -1 && pkItemContainer->GetServerBag(wPos) != -1)
	{
		PacketBuilder->SendMapItemDropReqMsg(pkItemContainer->GetServerBag(wPos),pkItemContainer->GetServerPos(wPos));
	}
	// 否则出错。
}

// 物品购买请求
void CItemManager::ItemBuyReq(ui64 vecdorguid, ui32 dwItemId, ui8 byNum, ui32 amt)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return;

	PacketBuilder->SendItemBuyReqMsg(vecdorguid, dwItemId, byNum, amt);
}

// 物品卖出请求
void CItemManager::ItemSellReq(ui64 vecdorguid, ui64 itemguid, ui8 byNum)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return;

	PacketBuilder->SendItemSellReqMsg(vecdorguid, itemguid, byNum);
}

// 物品使用请求
void CItemManager::ItemUseReq(ui8 bag, ui8 slot, ui64 item_guid, SpellCastTargets& targets)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return;

	PacketBuilder->SendItemUseReqMsg(bag, slot, item_guid, targets);
}

void CItemManager::UnBindItemReq( ui64 item_guid )
{
	/*CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return;

	PacketBuilder->SendUnBindReqMsg( m_uiUsingItem, item_guid );

	m_uiUsingItem = 0;*/
}


// 换装
bool CItemManager::OnAvatar(std::string& strFileNames, NiNode* pkNode, CAvatar::SAvatarInfo& sAvatarInfo, ui32 refine_effect_displayid,int iAttachmentSlot /*= -1*/, NiBoneLODController* pkLODCtrl /*= 0*/,
							CAvatar::SEffectInfo* pkEffect /*= 0*/ )
{
	CAvatar kAvatar;
	//////////////////////////////////////////////////////////////////////////
	std::string RefineEFileName = "";
	if (refine_effect_displayid)
	{
		m_pkDisplayInfoDB->FindDisplayInfo(refine_effect_displayid, RefineEFileName, false);
	}
	//////////////////////////////////////////////////////////////////////////
	if(strFileNames.size())
	{
		NiAVObjectPtr spObject = g_ResMgr->LoadNif(strFileNames.c_str());

		kAvatar.ChangeAvatar(pkNode, spObject, sAvatarInfo, iAttachmentSlot, pkLODCtrl);

		
		if (INVTYPE_HOUNTER_BEIBU == sAvatarInfo.uiPart)
		{
			NiAVObject* pkR = pkNode->GetObjectByName("BacksideRightWeapon_Node");
			NiAVObject* pkL = pkNode->GetObjectByName("BacksideLeftWeapon_Node");


			if (pkR)
				pkR->SetAppCulled(true);
			if (pkL)
				pkL->SetAppCulled(true);
		}

		


		//if (pkEffect)
		//{
		//	NiNode* pkEffectNode = (NiNode*)pkNode->GetObjectByName(pkEffect->attachnodename.c_str());
		//	NiAVObject* pkEffectEnt = g_ResMgr->LoadNif(pkEffect->effectname.c_str());
		//	if (pkEffectNode && pkEffectEnt)
		//	{
		//		pkEffectNode->AttachChild(pkEffectEnt);
		//	}
		//}

		//////////////////////////////////////////////////////////////////////////
		//refine eff
		kAvatar.AddEffectToEquip(pkNode, RefineEFileName, sAvatarInfo,iAttachmentSlot);
				
		//////////////////////////////////////////////////////////////////////////

		pkNode->UpdateProperties();
		pkNode->UpdateEffects();
		pkNode->Update(0.0f, false);

		return true;
	}
	else
	{
		kAvatar.ChangeAvatar(pkNode, NULL, sAvatarInfo, iAttachmentSlot);

		if (INVTYPE_HOUNTER_BEIBU == sAvatarInfo.uiPart)
		{
			NiAVObject* pkR = pkNode->GetObjectByName("BacksideRightWeapon_Node");
			NiAVObject* pkL = pkNode->GetObjectByName("BacksideLeftWeapon_Node");

			if (pkR)
				pkR->SetAppCulled(false);
			if (pkL)
				pkL->SetAppCulled(false);

		}
		//////////////////////////////////////////////////////////////////////////
		//refine eff
		kAvatar.AddEffectToEquip(pkNode, RefineEFileName, sAvatarInfo,iAttachmentSlot);
				
		//////////////////////////////////////////////////////////////////////////

		pkNode->UpdateProperties();
		pkNode->UpdateEffects();
		pkNode->Update(0.0f, false);
	}

	return false;
}

void CItemManager::SetIsShowShizhuang(CPlayer* player, bool value)
{
	m_IsShowShizhuang = value;
	CPlayer* pkPlayer = player;
	if(!pkPlayer)
		return;
	UEquipment* pEquipment = INGAMEGETFRAME(UEquipment,FRAME_IG_PLAYEREQUIPDLG);
	if (!pEquipment)
		return;
	if (pkPlayer->IsLocalPlayer())
		pEquipment->SetIsShowShiZhuang(value);

	bool bHunterEquip = false;
	ui32 SlotIndex = 0;
	if (m_IsShowShizhuang)
	{
		for (ui16 index = EQUIPMENT_HOUNTER_BEIBU ; index <= EQUIPMENT_HOUNTER_XIEZI ; index++)
		{
			
			SlotIndex = index;
			ui32 visiblebase = PLAYER_VISIBLE_ITEM_1_0 + (SlotIndex * 12);
			uint32 entry = pkPlayer->GetUInt32Value(visiblebase);

			if (!pkPlayer->isShowHelm() && index == EQUIPMENT_HOUNTER_TOUBU)
			{
				entry = 0;
			}
			if (entry)
			{
				OnEquipmentChange(pkPlayer->GetGUID(), entry, SlotIndex);
				TeamShow->UpdateObjHead(pkPlayer->GetGUID(), visiblebase,entry,SlotIndex);


				if (pkPlayer->IsLocalPlayer())
				{
					ui32 effect_displayid = 0;
					ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (SlotIndex * 12) + 9 ;
					effect_displayid = pkPlayer->GetUInt32Value(itemeffect);
					pEquipment->UpDateUNiavobject(visiblebase, entry, SlotIndex, effect_displayid);
				}
			}
		}

	}
	else
	{
		for( ui16 index = EQUIPMENT_SLOT_START; index < EQUIPMENT_SLOT_END; index++ )
		{
			if(index == INVTYPE_HOUNTER_TOUBU)
			{
				bHunterEquip = true;
				SlotIndex = INVTYPE_HEAD;
			}
			else if(index == INVTYPE_HOUNTER_JIANBANG)
			{
				bHunterEquip = true;
				SlotIndex = INVTYPE_SHOULDERS;
			}
			else if(index == INVTYPE_HOUNTER_SHANGYI)
			{
				bHunterEquip = true;
				SlotIndex = INVTYPE_CHEST;
			}
			else if(index == INVTYPE_HOUNTER_XIAYI)
			{
				bHunterEquip = true;
				SlotIndex = INVTYPE_TROUSERS;
			}
			else if(index == INVTYPE_HOUNTER_XIEZI)
			{
				bHunterEquip = true;
				SlotIndex = INVTYPE_BOOTS;
			}
			if(bHunterEquip)
			{
				ui32 visiblebase = PLAYER_VISIBLE_ITEM_1_0 + (SlotIndex * 12);
				uint32 entry = pkPlayer->GetUInt32Value(visiblebase);

				if (!pkPlayer->isShowHelm() && SlotIndex == INVTYPE_HEAD)
				{
					entry = 0;
				}
				OnEquipmentChange(pkPlayer->GetGUID(), entry, SlotIndex);

				if (pkPlayer->IsLocalPlayer())
				{
					ui32 effect_displayid = 0;
					ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (SlotIndex * 12) + 9 ;
					effect_displayid = pkPlayer->GetUInt32Value(itemeffect);
					pEquipment->UpDateUNiavobject(visiblebase, entry, SlotIndex, effect_displayid);
					pEquipment->UpDateUNiavobject(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_BEIBU * 12), 0, INVTYPE_HOUNTER_BEIBU);
				}
				TeamShow->UpdateObjHead(pkPlayer->GetGUID(), visiblebase,entry,SlotIndex);
			}
		}
		OnEquipmentChange(pkPlayer->GetGUID(), 0, INVTYPE_HOUNTER_BEIBU);
	}
}
void CItemManager::OnEquipmentChange(CPlayer* pkPlayer, ui32 itemid, ui32 SlotIndex, int iAttachmentSlot /*= -1*/)
{
	if(pkPlayer == NULL)
		return;

	CAvatarModel* pkAvatarModel = NiNew CAvatarModel(pkPlayer->GetGUID(), itemid, SlotIndex, iAttachmentSlot, pkPlayer->IsClientObject() );
	pkPlayer->AddAvatarModel(pkAvatarModel);
	pkPlayer->HilightObject(false);
	g_pkTaskManager->PushTask(pkAvatarModel);
}

void CItemManager::OnEquipmentChange(ui64 guid, ui32 itemid, ui32 SlotIndex, int iAttachmentSlot /*= -1*/)
{
	CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(guid);
	if(pkPlayer == NULL)
		return;

    OnEquipmentChange(pkPlayer, itemid, SlotIndex, iAttachmentSlot);
}

void CItemManager::OnEquipmentChangeReal(CPlayer* pkPlayer, ui32 itemid, ui32 SlotIndex, int iAttachmentSlot)
{
    if( !pkPlayer || !pkPlayer->IsActorLoaded() )
    {
        NIASSERT(false);
        return;
    }

	ui32 effect_displayid = 0;
	ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (SlotIndex * 12) + 8 ;
	effect_displayid = pkPlayer->GetUInt32Value(itemeffect);

	
	NiNode* pkNode = pkPlayer->GetSceneNode();
	if (pkNode == NULL)
	{
		return;
	}
	ItemPrototype_Client* ItemInfo = GetItemPropertyFromDataBase(itemid);
	CAvatar::SAvatarInfo sAvatarInfo;
	sAvatarInfo.eType = Avatar_Model;
	sAvatarInfo.uiPart = 0;
	//ULOG("服务器 %d ItemId %d", SlotIndex, itemid);
	if(ItemInfo == NULL)
	{	
		// 是否换上基础装备
		// 换默认装备
		if(itemid == 0)
		{
			std::string strFileNames = "";
			unsigned int uiDefaultId = pkPlayer->GetRace() + (pkPlayer->GetGender()<<16);//pkPlayer->GetRace() + (pkPlayer->GetClass()<<8) + (pkPlayer->GetGender()<<16);
			unsigned int uiModelId = 0;
			GetDefaultDisplayInfo(uiDefaultId, uiModelId, SlotIndex);
			GetDisplayInfo(uiModelId, strFileNames, pkPlayer->GetRace(), pkPlayer->GetGender(), true);

			if( SlotIndex == INVTYPE_WEAPONOFFHAND )//	副手可能是盾牌也可能是副手武器,这里不好判断,所以onvatar两次
			{
				sAvatarInfo.uiPart = INVTYPE_SHIELD;
				OnAvatar(strFileNames, (NiNode*)pkNode->GetAt(0), sAvatarInfo, effect_displayid);
				sAvatarInfo.uiPart = INVTYPE_WEAPONOFFHAND;
				OnAvatar(strFileNames, (NiNode*)pkNode->GetAt(0), sAvatarInfo, effect_displayid);
			}
			sAvatarInfo.uiPart = SlotIndex;
			sAvatarInfo.uiId = uiModelId;
			OnAvatar(strFileNames, (NiNode*)pkNode->GetAt(0), sAvatarInfo,effect_displayid, iAttachmentSlot, pkPlayer->GetLODController());
		}
	}
	else
	{
		std::string strFileNames;
		ui32 display = ItemInfo->DisplayInfoID[0][0];

		if( ItemInfo->Class != ITEM_CLASS_WEAPON )
		{
			if( pkPlayer->GetRace() )
				display = ItemInfo->DisplayInfoID[pkPlayer->GetRace()-1][pkPlayer->GetGender()];
		}

		GetDisplayInfo(display, strFileNames, pkPlayer->GetRace(), pkPlayer->GetGender(), true);
		sAvatarInfo.uiPart = ItemInfo->InventoryType;
		sAvatarInfo.uiId = itemid;

		if(ItemInfo->InventoryType == INVTYPE_WEAPONMAINHAND && SlotIndex == EQUIPMENT_SLOT_OFFHAND)
		{
			sAvatarInfo.uiPart = INVTYPE_WEAPONOFFHAND;
		}
		if(ItemInfo->InventoryType == INVTYPE_WEAPONOFFHAND && SlotIndex == EQUIPMENT_SLOT_MAINHAND)
		{
			sAvatarInfo.uiPart = INVTYPE_WEAPONMAINHAND;
		}
		if(!IsMeshLoaded((NiNode*)pkNode->GetAt(0), sAvatarInfo.uiPart, itemid))
		{
			float fScale = 1.0f;
			if(SlotIndex == INVTYPE_WEAPONMAINHAND || SlotIndex == INVTYPE_WEAPONOFFHAND)
			{
				unsigned int uiRace = pkPlayer->GetRace();
				unsigned int uiGender = pkPlayer->GetGender();
				if(uiRace == RACE_YAO)
				{
					if(uiGender == GENDER_MALE)
						fScale = 1.3f;
				}
				else if(uiRace == RACE_REN)
				{
					fScale = 1.4f;
				}
				else
				{
					if(uiGender == GENDER_MALE)
						fScale = 1.6f;
					else
						fScale = 1.4f;
				}
			}
			sAvatarInfo.fScale = fScale;

			if( SlotIndex == INVTYPE_WEAPONOFFHAND )//	副手可能是盾牌也可能是副手武器,这里不好判断,所以onvatar两次
			{
				//副手可能是盾牌也可能是副手武器。 这里先判断当前的武器还是盾牌，然后选择性的去除对应的节点。

				CAvatar::SAvatarInfo sTemAvatarInfo;
				sTemAvatarInfo.eType = Avatar_Model;
				std::string strTem = "";
				if (sAvatarInfo.uiPart == INVTYPE_SHIELD)
				{
					sTemAvatarInfo.uiPart = INVTYPE_WEAPONOFFHAND;				
				}else
				{
					sTemAvatarInfo.uiPart = INVTYPE_SHIELD;
				}
				OnAvatar(strTem, (NiNode*)pkNode->GetAt(0), sTemAvatarInfo, 0);

				//换装
				OnAvatar(strFileNames, (NiNode*)pkNode->GetAt(0), sAvatarInfo, effect_displayid,iAttachmentSlot);

				if ( ItemInfo->InventoryType == INVTYPE_BAOZHU )
				{
					pkPlayer->SetNeedBallAnim( true );
				}
				else
				{
					pkPlayer->SetNeedBallAnim( false );
				}
			}
			else
			{
				OnAvatar(strFileNames, (NiNode*)pkNode->GetAt(0), sAvatarInfo, effect_displayid,iAttachmentSlot, pkPlayer->GetLODController());
			}

			if (INVTYPE_HOUNTER_BEIBU == sAvatarInfo.uiPart)
			{

			}
		}
	}


	if (!pkPlayer->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM))
	{
		if (pkPlayer->GetCurPostureState() == STATE_IDLE)
		{
			if (pkPlayer->IsWait())
				pkPlayer->SetIdleAnimation(CAG_WAIT);
			else
			{
				bool bIsInCombatState = pkPlayer->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);
				if ( bIsInCombatState )
				{
					pkPlayer->SetAttackPrepare();
				}
				else
					pkPlayer->SetIdleAnimation(CAG_IDLE);
			}
		}
		if(pkPlayer->IsMoving() && !pkPlayer->IsJumping())
		{
			pkPlayer->SetMoveAnimation(pkPlayer->GetMoveState());
		}
	}

	pkPlayer->ReflashShadowCaster();
}

void CItemManager::SendTradeSetItemReq(ui8 ucTradeSlot, ui8 ucBag, ui8 ucSlot)
{
	PacketBuilder->SendTradeSetItemMsg(ucTradeSlot, ucBag, ucSlot);
}

void CItemManager::SendTradeClearItemReq(ui8 ucTradeSlot)
{
	PacketBuilder->SendTradeClearItemMsg(ucTradeSlot);
}

void CItemManager::OnItemTradeStateEx(MSG_S2C::stTradeStat_Ext& stTradeState)
{
	m_pkTradeItem.clear();
	UPlayerTrade * pPlayerTrade = INGAMEGETFRAME(UPlayerTrade,FRAME_IG_PLAYERTRADEDLG);
	if (pPlayerTrade)
	{
		for(unsigned int ui = 0; ui < TRADE_SLOT_COUNT; ui++)
		{
			pPlayerTrade->SetIconByPos(ui, stTradeState.tradeitem[ui].entry,UItemSystem::ITEM_DATA_FLAG, stTradeState.tradeitem[ui].stackcount, stTradeState.tradeitem[ui].extra,FALSE);
			m_pkTradeItem.push_back(stTradeState.tradeitem[ui]);
		}
		pPlayerTrade->ShowTennetMoney(stTradeState.gold);
	}
}

ItemPrototype_Client* CItemManager::GetItemPropertyFromContainer(WORD wContainer /* = 0 */, WORD wPos /* = 0 */)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return NULL;

	CSlotContainer* pkContainer = pkPlayer->GetContainer((BYTE)wContainer);
	if(!pkContainer)
		return NULL;

	CItemSlot* pkItemSlot = (CItemSlot*)pkContainer->GetSlot((BYTE)wPos);
	if(!pkItemSlot)
		return NULL;

	return pkItemSlot->GetItemProperty();
}

ItemPrototype_Client* CItemManager::GetItemPropertyFromDataBase(ui32 dwId)
{
	NIASSERT(m_pkItemDB);
	return m_pkItemDB->GetInfo(dwId);
}
BOOL CItemManager::IsHealingItem(ui32 dwId)
{
	ItemPrototype_Client* pkItem = GetItemPropertyFromDataBase( dwId);
	if (pkItem)
	{
		for ( int i = 0 ; i < 5 ; i++ )
		{
			if (pkItem->Spells[i].Id)
			{
				if (SpellTemplate::IsHealingSpell(pkItem->Spells[i].Id))
				{
					return TRUE;
				}
			}
		}
	}
	return FALSE ;
}
BOOL CItemManager::IsMaNaItem(ui32 dwId)
{
	ItemPrototype_Client* pkItem = GetItemPropertyFromDataBase( dwId);
	if (pkItem)
	{
		for ( int i = 0 ; i < 5 ; i++ )
		{
			if (pkItem->Spells[i].Id)
			{
				if (SpellTemplate::IsMaNaSpell(pkItem->Spells[i].Id))
				{
					return TRUE;
				}
			}
		}
	}
	return FALSE ;
}
UINT CItemManager::GetItemRefineRate(ui32 dwId, UINT stType)
{
	//stType 为0 是攻击力加成
	//stType 为1 是防御力加成
	//stType 为2 是额外属性加成
	ItemPrototype_Client* ItemInfo = GetItemPropertyFromDataBase(dwId);
	if (ItemInfo)
	{
		if (stType >=0 && stType < 3)
		{
			return jinglian_property[ItemInfo->Quality][stType];
		}
	}

	return 0 ;
}
ItemExtendedCostentry::ItemExtendeData* CItemManager::GetItemExtendeDataFromDataBase(ui32 Costentry)
{
	NIASSERT(m_pkItenExtendedConstry);
	return m_pkItenExtendedConstry->GetItemExtende(Costentry);
}
ItemPrototype_Client* CItemManager::GetItemPropertyFromDataBase(const char* ItemName)
{
	NIASSERT(m_pkItemDB);
	return m_pkItemDB->GetInfo(ItemName);
}
bool CItemManager::GetItemListFormDataBase(ui32 Class, std::vector<ItemPrototype_Client>& vInfo)
{
	NIASSERT(m_pkItemDB);
	return m_pkItemDB->GetInfo(Class, vInfo);
}

bool CItemManager::GetDisplayInfo(unsigned int uiModelId, std::string& strFileName, unsigned int uiRace, unsigned int uiGender, bool bModel, bool bReturnShort /* = false */)
{
	NIASSERT(m_pkDisplayInfoDB);
	
	if(!bModel)
	{
		if(uiGender == GENDER_MALE)
			uiModelId += 1;
	}

	return m_pkDisplayInfoDB->FindDisplayInfo(uiModelId, strFileName, bReturnShort);
}

bool CItemManager::GetDefaultDisplayInfo(unsigned int uiDefaultId, ui32& uiModelId, unsigned int uiDefaultPart, bool bReturnShort /* = false */)
{
	NIASSERT(g_pkDefaultDisplayInfo);

	if(uiDefaultPart > 5)
		return false;

	return g_pkDefaultDisplayInfo->FindDefaultDisplayInfo(uiDefaultId, uiModelId, uiDefaultPart, bReturnShort);
}

bool CItemManager::GetDefaultModelInfo(unsigned int uiDefaultId, ui32& uiModelId)
{
	NIASSERT(g_pkDefaultDisplayInfo);

	return g_pkDefaultDisplayInfo->FindDefaultModelInfo(uiDefaultId, uiModelId);
}

bool CItemManager::IsCreatureNPC(unsigned int uiCreatureId)
{
	NIASSERT(m_pkCreatureNameDB);
	return m_pkCreatureNameDB->IsNPC(uiCreatureId);
}

bool CItemManager::GetCreatureName(unsigned int uiCreatureId, std::string& strName)
{
	NIASSERT(m_pkCreatureNameDB);
	return m_pkCreatureNameDB->FindName(uiCreatureId, strName);
}
bool CItemManager::GetNormalNpcName(unsigned int uiCreatureId, std::string& strName)
{
	NIASSERT(m_pkCreatureNameDB);
	if (m_pkCreatureNameDB->IsNPC(uiCreatureId) && !m_pkCreatureNameDB->FindTitleName(uiCreatureId, strName))
	{
		return m_pkCreatureNameDB->FindAllName(uiCreatureId, strName);
	}
	return false ;
}
bool CItemManager::GetTitleNpcName(unsigned int uiCreatureId, std::string& strName)
{
	NIASSERT(m_pkCreatureNameDB);
	if (m_pkCreatureNameDB->IsNPC(uiCreatureId) && m_pkCreatureNameDB->FindTitleName(uiCreatureId, strName))
	{
		return m_pkCreatureNameDB->FindAllName(uiCreatureId, strName);
	}
	return false ;
}
bool CItemManager::GetGuaiWuName(unsigned int uiCreatureId, std::string& strName)
{
	if (!m_pkCreatureNameDB->IsNPC(uiCreatureId))
	{
		return m_pkCreatureNameDB->FindAllName(uiCreatureId, strName);
	}
	return false ;
}
bool CItemManager::GetCreatureTitleName(unsigned int uiCreatureId, std::string& strName)
{
	NIASSERT(m_pkCreatureNameDB);
	return m_pkCreatureNameDB->FindTitleName(uiCreatureId, strName);
}

bool CItemManager::GetCreatureModelId(unsigned int uiEntry, unsigned int& uiModelId)
{
	NIASSERT(m_pkCreatureNameDB);
	return m_pkCreatureNameDB->FindModelId(uiEntry, uiModelId);
}

bool CItemManager::GetCreatureWalkSpeed(unsigned int uiEntry, float& value)
{
	NIASSERT(m_pkCreatureNameDB);
	return m_pkCreatureNameDB->FindWalkSpeed(uiEntry, value);
}

bool CItemManager::GetCreatureRunSpeed(unsigned int uiEntry, float& value)
{
	NIASSERT(m_pkCreatureNameDB);
	return m_pkCreatureNameDB->FindRunSpeed(uiEntry, value);
}

bool CItemManager::GetCreatureRankId(unsigned int uiEntry, unsigned int& value)
{
	NIASSERT(m_pkCreatureNameDB);
	return m_pkCreatureNameDB->FindRankID(uiEntry, value);
}

bool CItemManager::GetCNDB(unsigned int uiEntry, CCreatureNameDB::stCNDB& cndb)
{
	NIASSERT(m_pkCreatureNameDB);
	return m_pkCreatureNameDB->FindCNDB(uiEntry, cndb);
}



CItemSlot* CItemManager::GetItemSlotByItemId(ui32 itemid)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return NULL;

	CStorage* pkContainer = pkPlayer->GetItemContainer();
	if(!pkContainer)
		return NULL;

	return (CItemSlot*)pkContainer->GetSlotByEntry(itemid);
}

CItemSlot* CItemManager::GetItemSlot(WORD wContainer /* = 0 */, WORD wPos /* = 0 */)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();

	if(!pkPlayer)
		return NULL;

	CStorage* pkContainer = pkPlayer->GetItemContainer();
	if(!pkContainer)
		return NULL;

	return (CItemSlot*)pkContainer->GetSlot(wPos);
}

bool CItemManager::HandleFreshManTip(ui32 entry)
{
	if (m_pkItemDB->IsMountItem(entry))
	{
		HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_RIDE);
	}
	if (m_pkItemDB->IsShapeItem(entry))
	{
		HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_SHAPESHIFT);
	}
	if (m_pkItemDB->IsSkillBook(entry))
	{
		HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_LEARN_SPELL);
		HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_USE_SPELL);
	}
	return true;
}