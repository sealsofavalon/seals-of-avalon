#ifndef CITEMMANAGER_H
#define CITEMMANAGER_H

#include "Utils/ItemDB.h"
#include "Utils/DisplayInfo.h"
#include "Utils/CreatureName.h"
#include "Utils/ItemExtendedCostentry.h"
#include "Avatar.h"

class CPlayer;
class CItemManager : public NiMemObject
{
public:
	CItemManager();
	~CItemManager();

	bool Initialize();
	void Destroy();

	// 物品移动消息
	void ItemMoveReq(ui8 dwIctFrom, ui8 dwIctTo, ui8 wPosFrom, int wPosTo, ui8 wNum);
	//// 物品移动消息返回
	//void OnItemMove(ui8 dwIctFrom, ui8 dwIctTo, ui8 wPosFrom, ui8 wPosTo);

	void OnItemChange(ui8 bag, ui8 slot, ui64 guid);
	// 添加道具
	void AddItemToBackPack(ui8 bag, ui8 slot, ui64 guid);
	// 删除道具
	void RemoveItemFromBackPack(ui8 bag, ui8 slot);

	//Bank 里的物品操作
	void OnBankItemChange(ui8 bag, ui8 slot, ui64 guid);
	void AddItemToBank(ui8 bag, ui8 slot, ui64 guid);
	void RemoveItemFromBank(ui8 bag, ui8 slot);


	// 填加道具到装备栏
	void AddItemToEquipmentContainer(ui8 slot, ui32 itemid,ui64 guid);
	void RemoveItemFromEquip(ui8 slot);

	// 丢弃背包的物品
	void DropBackPackItemReq(ui8 wPos);

	// 物品购买请求
	void ItemBuyReq(ui64 vecdorguid, ui32 dwItemId, ui8 byNum, ui32 amt);
	// 物品卖出请求
	void ItemSellReq(ui64 vecdorguid, ui64 itemguid, ui8 byNum);
	// 物品使用请求
	void ItemUseReq(ui8 bag, ui8 slot, ui64 item_guid, SpellCastTargets& targets);
	void UnBindItemReq( ui64 item_guid );

	void SetIsShowShizhuang(CPlayer* player, bool value);
	// 换装
	void OnEquipmentChange(ui64 guid, ui32 itemid, ui32 SlotIndex, int iAttachmentSlot = -1);
	void OnEquipmentChange(CPlayer* pkPlayer, ui32 itemid, ui32 SlotIndex, int iAttachmentSlot = -1); //
    void OnEquipmentChangeReal(CPlayer* pkPlayer, ui32 itemid, ui32 SlotIndex, int iAttachmentSlot);
	bool OnAvatar(std::string& strFileNames, NiNode* pkNode, CAvatar::SAvatarInfo& sAvatarInfo, ui32 refine_effect_displayid = 0,int iAttachmentSlot = -1, 
		NiBoneLODController* pkLODCtrl = 0, CAvatar::SEffectInfo* pkEffect = 0);
	

	// 玩家之间物品交易设置
	void SendTradeSetItemReq(ui8 ucTradeSlot, ui8 ucBag, ui8 ucSlot);
	void SendTradeClearItemReq(ui8 ucTradeSlot);
	void OnItemTradeStateEx(MSG_S2C::stTradeStat_Ext& stTradeState);

	void AddTradeSucesseMsg();
	bool ClearPlayerTradeContainer();  //清空玩家交易列表
	void TradeConToBagCon();

	//ItemPrototype_Client* GetItemProperty(WORD wContainer = 0, WORD wPos = 0);
	ItemPrototype_Client* GetItemPropertyFromContainer(WORD wContainer, WORD wPos);
	class CItemSlot* GetItemSlotByItemId(ui32 itemid);
	class CItemSlot* GetItemSlot(WORD wContainer = 0, WORD wPos = 0);

	ItemPrototype_Client* GetItemPropertyFromDataBase(ui32 dwId);
	BOOL IsHealingItem(ui32 dwId);
	BOOL IsMaNaItem(ui32 dwId);

	UINT GetItemRefineRate(ui32 dwId, UINT stType); //根据物件的品质，返回相对的精炼的加成参数，

	ItemPrototype_Client* GetItemPropertyFromDataBase(const char* ItemName);
	bool GetItemListFormDataBase(ui32 Class, std::vector<ItemPrototype_Client>& vInfo);
	ItemExtendedCostentry::ItemExtendeData* GetItemExtendeDataFromDataBase(ui32 Costentry);

	bool GetDisplayInfo(unsigned int uiModelId, std::string& strFileName, unsigned int uiRace, unsigned int uiGender, bool bRoleModel, bool bReturnShort = false);
	bool GetDefaultDisplayInfo(unsigned int uiDefaultId, ui32& uiModelId, unsigned int uiDefaultPart, bool bReturnShort = false);
	bool GetDefaultModelInfo(unsigned int uiDefaultId, ui32& uiModelId);

	bool IsCreatureNPC(unsigned int uiCreatureId);

	bool GetCreatureName(unsigned int uiCreatureId, std::string& strName);
	bool GetNormalNpcName(unsigned int uiCreatureId, std::string& strName);
	bool GetTitleNpcName(unsigned int uiCreatureId, std::string& strName);
	bool GetGuaiWuName(unsigned int uiCreatureId, std::string& strName);

	bool GetCreatureTitleName(unsigned int uiCreatureId, std::string& strName);
	bool GetCreatureModelId(unsigned int uiEntry, unsigned int& uiModelId);
	bool GetCreatureWalkSpeed(unsigned int uiEntry, float& value);
	bool GetCreatureRunSpeed(unsigned int uiEntry, float& value);
	bool GetCreatureRankId(unsigned int uiEntry, unsigned int& value);

	bool GetCNDB(unsigned int uiEntry, CCreatureNameDB::stCNDB& cndb);

	bool HandleFreshManTip(ui32 entry);
	//ui64 GetUsingItem(){ return m_uiUsingItem;}
	//void SetUsingItem( ui64 ItemGui ){ m_uiUsingItem = ItemGui;}
	//void SetItemUsed(){ m_uiUsingItem = 0; }
protected:
	CItemDB* m_pkItemDB;
	CDisplayInfoDB* m_pkDisplayInfoDB;
	CCreatureNameDB* m_pkCreatureNameDB;
	ItemExtendedCostentry* m_pkItenExtendedConstry;
	bool m_IsShowShizhuang;
	std::vector<TradeItem>m_pkTradeItem; 
	//uint64 m_uiUsingItem;
};

#endif
