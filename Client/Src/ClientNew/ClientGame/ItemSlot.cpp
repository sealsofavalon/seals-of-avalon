#include "stdafx.h"
#include "ItemSlot.h"
#include "ItemManager.h"

CItemSlot::CItemSlot()
{
	m_pkItem = NULL;
	m_bLocked = false;
}

CItemSlot::~CItemSlot()
{
}

CItemSlot::CItemSlot(SYItem* pkItem)
{
	m_pkItem = pkItem;
	m_bLocked = false;
}

void CItemSlot::Copy(CSlot& slot)
{
	CSlot::Copy(slot);
	m_pkItem = ((CItemSlot*)&slot)->m_pkItem;
}

void CItemSlot::Clear()
{
	CSlot::Clear();
	m_pkItem = NULL;
}

ItemPrototype_Client* CItemSlot::GetItemProperty()
{
	if (m_pkItem)
	{
		return m_pkItem->GetItemProperty();
	}

	return NULL;
}