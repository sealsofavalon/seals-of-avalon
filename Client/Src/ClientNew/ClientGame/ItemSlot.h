#ifndef ITEMSLOT_H
#define ITEMSLOT_H

#include "Slot.h"
#include "Utils/ItemDB.h"
#include "SYItem.h"

class CItemSlot : public CSlot
{
public:
	CItemSlot();
	virtual ~CItemSlot();

	CItemSlot(SYItem* pkItem);

	virtual void Clear();

	virtual void Copy(CSlot& slot);

	// common
	virtual eSlotType GetSlotType() const { return ST_ITEM; }
	virtual DWORD GetCode() const 
	{
		if(m_pkItem) 
			return m_pkItem->GetCode(); 
		return 0; 
	}

	ui64 GetGUID() 
	{ 
		if(m_pkItem) 
			return m_pkItem->GetGUID(); 
		return 0;
	}

	virtual void SetSlotType(eSlotType type) { __UNUSED(type); }
	virtual void SetCode(DWORD code) { __UNUSED(code); }

	virtual UINT GetNum() const 
	{ 
		if(m_pkItem) 
			return m_pkItem->GetNum(); 
		return 0;
	}

	virtual void SetNum(UINT num) { ; }

	inline bool	IsLocked() { if (m_bLocked) return true; return false ;}
	inline void	SetLock( bool val ) { m_bLocked = val; }

	inline bool IsBlocked()	{ return (m_Set == 15);	}

	ItemPrototype_Client* GetItemProperty();

	inline bool IsPetEgg() { NIASSERT(m_pkItem); return m_pkItem->GetUInt32Value(ITEM_FIELD_GENERATE_TIME) != 0; }
	inline ui32 PetEggGenTime() { NIASSERT(m_pkItem); return m_pkItem->GetUInt32Value(ITEM_FIELD_GENERATE_TIME); }
	inline ui32 PetEggTotalTime() { NIASSERT(m_pkItem); return m_pkItem->GetUInt32Value(ITEM_FIELD_VALID_AFTER_GENERATE_TIME); }
	inline bool IsBonding(){NIASSERT(m_pkItem); return m_pkItem->HasFlag( ITEM_FIELD_FLAGS, ITEM_FLAG_QUEST | ITEM_FLAG_SOULBOUND)?true:false;}
private:
	SYItem* m_pkItem;
	bool m_bLocked;
	BYTE m_Set;
};

#endif