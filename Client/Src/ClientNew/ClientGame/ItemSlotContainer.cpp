#include "StdAfx.h"
#include "ItemSlotContainer.h"
#include "ItemSlot.h"

CItemSlotContainer::CItemSlotContainer(void)
{
	m_bIsBag = false;
}

CItemSlotContainer::~CItemSlotContainer(void)
{
}

void CItemSlotContainer::ClearAll()
{
	CSlotContainer::ClearAll();
	m_bValidState = true;
}

void CItemSlotContainer::Init(BYTE MaxSlotSize, BYTE SlotIdx)
{
	m_nSlotNum = 0;
	m_SlotIdx = SlotIdx;
	m_nMaxSlotNum = MaxSlotSize;
	m_bValidState = true;
	m_ppSlotArray = new CSlot * [MaxSlotSize];
	for(BYTE i = 0; i < MaxSlotSize; ++i)
	{
		m_ppSlotArray[i] = new CItemSlot;
	}
	ClearAll();
}

void CItemSlotContainer::UpdateSlot(BYTE AtPos, CSlot &rSlot)
{
	NIASSERT(AtPos < GetMaxSlotNum());
	NIASSERT(!IsEmpty(AtPos));

	if(AtPos >= GetMaxSlotNum())	
		return;

	if(IsEmpty(AtPos))			
		return;

	m_ppSlotArray[AtPos]->Copy(rSlot);
}

bool CItemSlotContainer::InsertSlot(BYTE AtPos, CSlot& kSlot)
{
	NIASSERT(AtPos < GetMaxSlotNum());
	NIASSERT(IsEmpty(AtPos));

	if(AtPos >= GetMaxSlotNum())		
		return false;

	if(!IsEmpty(AtPos))				
		return false;

	m_ppSlotArray[AtPos]->Copy(kSlot);
	m_ppSlotArray[AtPos]->SetPos(AtPos);
	m_ppSlotArray[AtPos]->SetSlotIdx(GetSlotIdx());
	++m_nSlotNum;
	NIASSERT(m_nSlotNum <= GetMaxSlotNum());

	return true;
}

UINT CItemSlotContainer::GetItemCnt(ui32 entryid)
{
	UINT cnt = 0;
	for(int i = 0; i < GetMaxSlotNum(); i++)
	{
		if( m_ppSlotArray[i] && m_ppSlotArray[i]->GetCode() == entryid )
		{
			cnt += m_ppSlotArray[i]->GetNum();
		}
	}
	return cnt;
}

bool CItemSlotContainer::IsLocked(BYTE AtPos)
{
	NIASSERT(AtPos < GetMaxSlotNum());

	return m_ppSlotArray[AtPos] ? ((CItemSlot*)m_ppSlotArray[AtPos])->IsLocked() : NULL;
}

void CItemSlotContainer::SetLock(BYTE AtPos, bool val)
{
	NIASSERT(AtPos < GetMaxSlotNum());
	if( m_ppSlotArray[AtPos] )
		((CItemSlot*)m_ppSlotArray[AtPos])->SetLock(val);
}