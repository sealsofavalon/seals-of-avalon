#ifndef ITEMSLOTCONTAINER_H
#define ITEMSLOTCONTAINER_H

#include "SlotContainer.h"
#include "..\..\..\..\SDBase\Public\ItemDef.h"


class SYItemBag;

class CItemSlotContainer : public CSlotContainer
{
public:
	CItemSlotContainer();
	virtual ~CItemSlotContainer();

	virtual void ClearAll();
	virtual void Init(BYTE MaxSlotSize, BYTE SlotIdx);
//	virtual void ClearBagSolt();
	virtual void UpdateSlot(BYTE AtPos, CSlot& rSlot);

	//virtual void InsertSlot(BYTE AtPos, class CItemSlot* pItemSlot);
	virtual bool InsertSlot(BYTE AtPos, CSlot& rSlot);

	virtual bool IsLocked(BYTE AtPos);
	virtual void SetLock(BYTE AtPos, bool val);

	virtual bool ValidState() { return m_bValidState; }
	virtual void SetValidState(bool v) { m_bValidState = v; }

	UINT GetItemCnt(ui32 entryid);
private:	
	bool m_bValidState;
protected:
	bool m_bIsBag;  // 判断是否是一个背包的Container.
};

#endif