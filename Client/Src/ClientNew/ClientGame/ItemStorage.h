#ifndef __ITEMSTORAGE_H__
#define __ITEMSTORAGE_H__

#include "SYItem.h"
#include "ItemSlotContainer.h"

class CSYItemBagSlotContainer : public CItemSlotContainer
{
public:
	CSYItemBagSlotContainer();
	~CSYItemBagSlotContainer();
	SYItemBag* GetSYItemBagFromContainer();     // 如果为空，表示该位置不存在一个背包。
	void SetSYItemBag(SYItemBag* pBag);			//先需要释放资源。设定背包,根据背包的格子数重新分配Slot.
	void CopyBagData();							//拷贝背包栏里背包数据
protected:
	SYItemBag* m_pBag;  //对应的背包
};

struct SortMsg; 
class CStorage   //仓库管理、包括背包和银行。
{
public:
	enum  StorageType
	{
		EBAG = 0,   //包裹
		EBANK ,		//银行
	};

	CStorage(StorageType pType);
	~CStorage();
	
	void OnCreate();   //分配资源
	void OnDestroy();  //释放资源

	

	CSlot* GetSlot(ui8 bag ,BYTE AtPos);	//根据服务器的位置查询CSlot
	CSlot* GetSlot(BYTE cPos);				//根据UI的位置来查询CSlot
	CSlot* GetSlotByEntry(ui32 pEntry);		// 根据物品的类型来查询CSlot. 返回第一个该类型的CSlot.
	CSlot* GetSlotByGuid(ui64 guid);		//根据GUID ,返回对应的CSlot

	CItemSlotContainer* GetDefItemContainer();			 //返回系统默认的包 CItemSlotContainer
	CItemSlotContainer* GetContainerByPos(BYTE cPos);	 //根据UI的信息来获取CItemSlotContainer
	CItemSlotContainer* GetContainerByIndex(ui8 bag);	 //根据包的索引返回CItemSlotContainer.

	void ReleaseAllLock();
	bool HasLockSolt();
	UINT GetNumMaxSlotCount();
	UINT GetItemCnt(ui32 entryid);				// 获取一种道具的总数量.
	
	INT GetServerPos(int cPos);					//根据UI上的POS 查询在对应的列表的POS . 返回的POS 是服务器上对应的位置,查询失败返回-1
	INT GetServerBag(int cPos);					// 根据UI的POS .查询在哪个列表中. 查询失败返回-1
	UINT GetUIPos(ui8 bag, BYTE AtPos);			//根据在服务器位置, 返回UI上的位置.查询失败返回-1
	UINT GetUIPos(ui64 guid);

	void SetSYItemBagContainer(SYItemBag* pBag);  //设置对应的背包栏的背包. 不允许传空值. 
	void SetBagContainerNull(ui8 bag);			  //设定对应的背包栏为空. 
	void CopyBagDataToContainer();				  //拷贝所有非空背包栏的数据到列表.

	int SortBag(); //整理背包排序
	void PushBackSortMsg(SortMsg& sortMsg);

private:
	CItemSlotContainer* m_pDefContainer;        //默然的包.
	CSYItemBagSlotContainer* m_pDefBagSlot[8];  // 背包栏列表
	StorageType m_Type;							//类型  是银行还是包裹.


	vector<SortMsg> m_pkSortMsg ;
};
#endif