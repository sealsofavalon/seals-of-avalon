#include "stdafx.h"
#include "LangaugeNotice.h"
#include "../../../../new_common/Source/ssl/crc32.h"
#include "../ui/UFun.h"
#include "../Utils/DBFile.h"


const char* _TRAN(const char* str,bool ignore)
{
	if (LangaugeMgr)
	{
		std::string& s( LangaugeMgr->GetNoticeText(str, ignore) );
		if( s == "error!" )
			return str;
		else
			return s.c_str();
	}
	return NULL ;

}
string _TRAN(UINT index , const char* Flagstring0, const char* Flagstring1 , const char* Flagstring2 , const char* Flagstring3 ,const char* Flagstring4 )
{
	std::string strbuf;
	if (LangaugeMgr)
	{
		strbuf = LangaugeMgr->FormatText(index, Flagstring0, Flagstring1,Flagstring2,Flagstring3,Flagstring4);
	}
	return strbuf ;
}

string _I2A( int data )
{
	char buff[16];
	itoa( data, buff, 10 );
	string str = buff;
	
	return str ;
}

string _F2A( float data, long floatcount)
{
	char buff[32] = {0};
	/*
	if ( floatcount > 6 )
	{
		floatcount = 6;
	}
	itoa( (int)data, buff, 10 );

	size_t curlength = strlen( buff );
	if ( curlength >= 32 )
	{
		buff[32] = '\0';
	}

	float floatPoint = data - (int)data;
	if ( floatcount && (curlength < 32) )
	{
		buff[curlength] = '.';
	}
	while(floatcount)
	{
		curlength++;
		floatPoint *= 10;
		char cChar = (int)floatPoint + '0';
		if ( curlength < 32 )
		{
			buff[curlength] = cChar;
		}
		floatPoint -= (int)floatPoint;
		floatcount--;
	}

	curlength++;
	if ( curlength < 32 )
	{
		buff[curlength] = '\0';
	}
	else
		buff[32] = '\0';
		*/
	char sformat[16];
	sprintf( sformat, "%%.%uf", floatcount );
	sprintf( buff, sformat, data );

	string str = buff;

	return str;
}

LangaugeNotice::LangaugeNotice() : error( "error!" )
{
}

void LangaugeNotice::ShutDown()
{

}
BOOL LangaugeNotice::InitNoticeFile()
{
	// load normal notice map 
	NiFile* pkFile = NiFile::GetFile("Data\\international_db_text.txt", NiFile::READ_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return FALSE;
	}

	size_t pkSize = pkFile->GetFileSize();
	if( pkSize == 0 )
	{
		NiDelete pkFile;
		return FALSE;
	}

	char linebuf[4096];

	while(pkFile->GetLine(linebuf, 4096))
	{
		string str = linebuf;
		size_t pos = str.find_first_of("\t");
		if ( pos !=  string::npos)
		{
			ui32 crc = strtoul(str.substr(0, pos).c_str(), NULL, 10);
			m_crcNoticeMap.insert(make_pair(crc, str.substr(pos+1, str.length() - 1)));
		}
	}
	NiDelete pkFile;

	//load flag notice map 
	pkFile = NiFile::GetFile("Data\\international_db_flag_text.txt", NiFile::READ_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return FALSE;
	}

	pkSize = pkFile->GetFileSize();
	if( pkSize == 0 )
	{
		NiDelete pkFile;
		return FALSE;
	}

	while(pkFile->GetLine(linebuf, 4096))
	{
		string str = linebuf;
		size_t pos = str.find_first_of("\t");
		if ( pos !=  string::npos)
		{
			ui32 crc = strtoul(str.substr(0, pos).c_str(), NULL, 10);
			m_flagNoticeMap.insert(make_pair(crc, str.substr(pos+1, str.length() - 1)));
		}
	}
	NiDelete pkFile;

	return TRUE;
}

string& LangaugeNotice::GetNoticeText( const char* szText, bool ignore )
{
	std::string strbuff = szText;
	unsigned int uiLen = strbuff.length(); 
	unsigned int Crc = ssl::__crc32((const unsigned char*) strbuff.c_str(), uiLen );
	//m_ErroText = AnisToUTF8( m_ErroText );
	//std::string strBuff = ;

	
	NoticeTextMap::iterator itText = m_crcNoticeMap.find( Crc );
	if ( itText != m_crcNoticeMap.end() )
	{

		return itText->second;
	}
	else
	{
		if( !ignore )
		{
			strbuff = AnisToUTF8( strbuff );
			m_crcNoticeMap[Crc] = strbuff; 
#ifdef CLIENT_DEBUG
			FILE* flog =  fopen("language_log.txt", "a+"); 
			if (flog)
			{
				fprintf(flog, "%s\n", szText);
			}
			fclose(flog);
#endif	
			return m_crcNoticeMap[Crc];
		}
		else
		{
			return error;
		}
	}
}


string LangaugeNotice::FormatText( int indexenum, const char* Flagstring0, const char* Flagstring1 /* = 0 */, const char* Flagstring2 /* = 0 */, const char* Flagstring3 /* = 0 */,const char* Flagstring4 /* = 0 */ )
{
	NoticeTextMap::iterator it = m_flagNoticeMap.find(indexenum);
	if (it == m_flagNoticeMap.end())
	{
#ifdef CLIENT_DEBUG
		FILE* flog =  fopen("language_flag_log.txt", "a+"); 
		if (flog)
		{
			fprintf(flog, "id: %d is not found\n", indexenum);
		}
		fclose(flog);
#endif

		return "";
	}
	
	std::string strDb = m_flagNoticeMap[indexenum];
	std::string strUtf;
	size_t startpos = strDb.find( LANGUAGE_FLAG_0 );
	if ( Flagstring0 && ( startpos != string::npos	) )
	{
		strUtf = Flagstring0;
		
		strDb = strDb.substr( 0, startpos ) + strUtf + strDb.substr( startpos + 24, strDb.length());
	}
	startpos = strDb.find( LANGUAGE_FLAG_1 );
	if ( Flagstring1 && ( startpos != string::npos) )
	{
		strUtf = Flagstring1;
	
		strDb = strDb.substr( 0, startpos ) + strUtf + strDb.substr( startpos + 24, strDb.length());
	}
	startpos = strDb.find( LANGUAGE_FLAG_2 );
	if ( Flagstring2 && ( startpos != string::npos) )
	{
		strUtf = Flagstring2;
		
		strDb = strDb.substr( 0, startpos ) + strUtf + strDb.substr( startpos + 24, strDb.length());
	}

	startpos = strDb.find( LANGUAGE_FLAG_3 );
	if ( Flagstring3 && ( startpos != string::npos) )
	{
		strUtf = Flagstring3;
		
		strDb = strDb.substr( 0, startpos ) + strUtf + strDb.substr( startpos + 24, strDb.length());
	}

	startpos = strDb.find( LANGUAGE_FLAG_4 );
	if ( Flagstring4 && ( startpos != string::npos) )
	{
		strUtf = Flagstring4;
		strDb = strDb.substr( 0, startpos ) + strUtf + strDb.substr( startpos + 24, strDb.length());
	}

	return strDb;
}