#ifndef __LANGAUGE_CONFIG_H__
#define __LANGAUGE_CONFIG_H__

#define  CURRENTLANGAUGE 0
#include "NoticeIndex.h"

const char* _TRAN(const char* str, bool ignore = false);
string _TRAN(UINT index, const char* Flagstring0, const char* Flagstring1 = 0, const char* Flagstring2 = 0, const char* Flagstring3 = 0,const char* Flagstring4 = 0);
string _I2A( int data );

string _F2A( float data, long floatcount);

#define Hays_TRAN(param) _TRAN(param)

enum Langauge
{
	CHINESE = 0,
	ENGLISH = 1,
	MAX
};


#define LANGUAGE_FLAG_0 "<LANGUAGE_FLAG_STRING_0>"
#define LANGUAGE_FLAG_1 "<LANGUAGE_FLAG_STRING_1>"
#define LANGUAGE_FLAG_2 "<LANGUAGE_FLAG_STRING_2>"
#define LANGUAGE_FLAG_3 "<LANGUAGE_FLAG_STRING_3>"
#define LANGUAGE_FLAG_4 "<LANGUAGE_FLAG_STRING_4>"

class  LangaugeNotice
{
public:
	LangaugeNotice(); 

public:
	string& GetNoticeText( const char* szText, bool ignore = false );
	
	string FormatText( int indexenum, const char* Flagstring0, const char* Flagstring1 = 0, const char* Flagstring2 = 0, const char* Flagstring3 = 0,const char* Flagstring4 = 0 );
	BOOL InitNoticeFile();
	BOOL LoadNoticeFile();
	BOOL LoadFlagNoticeFile();
	void ShutDown();

protected:
private:
	typedef std::map<ui32,std::string> NoticeTextMap;
	std::string error;

	NoticeTextMap m_crcNoticeMap ; //DB FILE crc text (NoticeTextMap::iterator value crc32)
	NoticeTextMap m_flagNoticeMap; // flag text (NoticeTextMap::iterator value enum )
};

#endif

