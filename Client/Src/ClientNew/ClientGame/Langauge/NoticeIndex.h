#ifndef __NOTICEINDEX_H__
#define __NOTICEINDEX_H__

enum
{
	//提示
	N_NOTICE_Z8 = 8,
	N_NOTICE_Z9,
	N_NOTICE_Z10,
	N_NOTICE_Z11,
	N_NOTICE_Z12,
	N_NOTICE_Z14 = 14,
	N_NOTICE_Z15,
	N_NOTICE_Z16,
	N_NOTICE_Z17,
	N_NOTICE_Z18,
	N_NOTICE_Z19,
	N_NOTICE_Z20,
	N_NOTICE_Z21,
	N_NOTICE_Z22,
	N_NOTICE_Z23,
	N_NOTICE_Z24,
	N_NOTICE_Z25,
	N_NOTICE_Z26,
	N_NOTICE_Z27,
	N_NOTICE_Z28,
	N_NOTICE_Z29,
	N_NOTICE_Z33 = 33,
	N_NOTICE_Z34,
	N_NOTICE_Z35,
	N_NOTICE_Z36,
	N_NOTICE_Z37,
	N_NOTICE_Z38,
	N_NOTICE_Z39,
	N_NOTICE_Z40,
	N_NOTICE_Z41,
	N_NOTICE_Z42,
	N_NOTICE_Z43,
	N_NOTICE_Z44,
	N_NOTICE_Z45,
	N_NOTICE_Z46,
	N_NOTICE_Z47,
	N_NOTICE_Z48,
	N_NOTICE_Z49,
	N_NOTICE_Z50,
	N_NOTICE_Z51,
	N_NOTICE_Z52,
	N_NOTICE_Z53,
	N_NOTICE_Z54,
	N_NOTICE_Z55,
	N_NOTICE_Z56,
	N_NOTICE_Z57,
	N_NOTICE_Z58,
	N_NOTICE_Z59,
	N_NOTICE_Z60,
	N_NOTICE_Z61,
	N_NOTICE_Z62,
	N_NOTICE_Z63,
	N_NOTICE_Z64,
	N_NOTICE_Z65,
	N_NOTICE_Z66,
	N_NOTICE_Z67,
	N_NOTICE_Z68,

	N_NOTICE_Z69,
	N_NOTICE_Z70,
	N_NOTICE_Z71,
	N_NOTICE_Z72,
	N_NOTICE_Z73,
	N_NOTICE_Z74,
	N_NOTICE_Z75,
	N_NOTICE_Z76,
	N_NOTICE_Z77,
	N_NOTICE_Z78,
	N_NOTICE_Z79,
	N_NOTICE_Z80,
	N_NOTICE_Z81,
	N_NOTICE_Z82,
	N_NOTICE_Z83,
	N_NOTICE_Z84,
	N_NOTICE_Z85,
	N_NOTICE_Z86,
	N_NOTICE_Z87,
	N_NOTICE_Z88,
	N_NOTICE_Z89,
	N_NOTICE_Z90,
	N_NOTICE_Z91,
	N_NOTICE_Z92,
	N_NOTICE_Z93,
	N_NOTICE_Z94,
	N_NOTICE_Z95,
	N_NOTICE_Z96,
	N_NOTICE_Z97,
	N_NOTICE_Z98,
	N_NOTICE_Z99,
	N_NOTICE_Z100,
	N_NOTICE_Z101,
	N_NOTICE_Z102,
	N_NOTICE_Z103,
	N_NOTICE_Z104,
	N_NOTICE_Z105,
	N_NOTICE_Z106,
	N_NOTICE_Z107,
	N_NOTICE_Z108,
	N_NOTICE_Z109,
	N_NOTICE_Z110,
	N_NOTICE_Z111,
	N_NOTICE_Z112,
	N_NOTICE_Z113,
	N_NOTICE_Z114,
	N_NOTICE_Z115,
	N_NOTICE_Z116,
	N_NOTICE_Z117,

	N_NOTICE_F0 = 118, //小队%d
	N_NOTICE_F1 = 119, //玩家[%s]请求收你为徒，请确认或取消！
	N_NOTICE_F2 = 120, //"<color:FF0000>恭喜获得新的称号:%s<br>%s",
	N_NOTICE_F3 = 121, // "获得奖励：【%s】* %u",
	N_NOTICE_F4 = 122, // %d小时

	N_NOTICE_F5 = 134, //你确认要拍卖%s吗？
	N_NOTICE_F6 = 135, //[135]:%d时
	N_NOTICE_F7	= 136, //[136]:%d分
	N_NOTICE_F8	= 137, //[137]:%d秒
	N_NOTICE_F9 = 138, //透明度：%d%%
	N_NOTICE_F10 = 139, //%s，种族：%s，等级：%u，职业：%s，现在位置：%s<br>
	N_NOTICE_F11 = 140, //你对%s造成%u点伤害<br>
	N_NOTICE_F12 = 141, //<LANGUAGE_FLAG_STRING_0>的普通攻击未命中你<br>
	N_NOTICE_F13 = 142, //你对<LANGUAGE_FLAG_STRING_0>造成<LANGUAGE_FLAG_STRING_1>点致命一击伤害<br>
	N_NOTICE_F14 = 143, //<LANGUAGE_FLAG_STRING_0>躲闪了你的攻击<br>
	N_NOTICE_F15 = 144, //<LANGUAGE_FLAG_STRING_0>招架了你的攻击<br>
	N_NOTICE_F16 = 145, //<LANGUAGE_FLAG_STRING_0>打断了你的攻击<br>
	N_NOTICE_F17 = 146, //你对<LANGUAGE_FLAG_STRING_0>造成<LANGUAGE_FLAG_STRING_1>点伤害(格挡<LANGUAGE_FLAG_STRING_2>点)<br>
	N_NOTICE_F18 = 147, //<LANGUAGE_FLAG_STRING_0>避开了你的攻击<br>
	N_NOTICE_F19 = 148, //<LANGUAGE_FLAG_STRING_0>对你的攻击免疫<br>
	N_NOTICE_F20 = 149, //你对<LANGUAGE_FLAG_STRING_0>造成<LANGUAGE_FLAG_STRING_1>点伤害(偏斜)<br>

	N_NOTICE_F21 = 150, //%s对你造成%u点伤害<br>
	N_NOTICE_F22 = 151, //你的普通攻击未命中%s<br>
	N_NOTICE_F23 = 152, //%s对你造成了%u点致命一击伤害(吸收%u点)<br>
	N_NOTICE_F24 = 153, //%s对你造成%u点致命一击伤害<br>
	N_NOTICE_F25 = 154, //%s对你造成了%u点伤害(吸收%u点)<br>
	N_NOTICE_F26 = 155, //你躲闪了%s的攻击<br>
	N_NOTICE_F27 = 156, //你招架了%s的攻击<br>
	N_NOTICE_F28 = 157, //你打断了%s的攻击<br>
	N_NOTICE_F29 = 158, //%s对你造成%u点伤害(格挡%u点)<br>
	N_NOTICE_F30 = 159, //你避开了%s的攻击<br>
	N_NOTICE_F31 = 160, //你完全吸收了%s的攻击<br>
	N_NOTICE_F32 = 161, //你对%s的攻击免疫<br>
	N_NOTICE_F33 = 162, // %s对你造成%u点伤害(偏斜)<br>

	N_NOTICE_F34,		//[163]:你对%s使用%s造成%u点伤害%s<br>
	N_NOTICE_F35,		//[164]:你对%s使用%s造成%u点伤害%s(吸收%u点伤害)<br>
	N_NOTICE_F36,		//[165]:你对%s使用%s造成%u点伤害%s(抵抗%u点伤害)<br>
	N_NOTICE_F37,		//[166]:你对%s使用%s造成%u点伤害%s(%u点物理伤害)<br>
	N_NOTICE_F38,		//[167]:你对%s使用%s造成%u点伤害%s(格挡%u点伤害)<br>
	
	N_NOTICE_F39,		//[168]:%s对你使用%s造成%u点伤害%s<br>
	N_NOTICE_F40,		//[169]:%s对你使用%s造成%u点伤害%s(吸收%u点伤害)<br>
	N_NOTICE_F41,		//[170]:%s对你使用%s造成%u点伤害%s(抵抗%u点伤害)<br>
	N_NOTICE_F42,		//[171]:%s对你使用%s造成%u点伤害%s(%u点物理伤害)<br>
	N_NOTICE_F43,		//[172]:%s对你使用%s造成%u点伤害%s(格挡%u点伤害)<br>
	
	N_NOTICE_F44,		//[173]:你释放的%s未命中%s<br>
	N_NOTICE_F45,		//[174]:%s释放的%s未命中你<br>
	N_NOTICE_F46,		//[175]:你释放的%s被%s抵抗了<br>
	N_NOTICE_F47,		//[176]:%s释放的%s被你抵抗了<br>
	N_NOTICE_F48,		//[177]:%s躲闪了你释放的%s<br>
	N_NOTICE_F49,		//[178]:你躲闪了%s释放的%s<br>
	N_NOTICE_F50,		//[179]:%s招架了你释放的%s<br>
	N_NOTICE_F51,		//[180]:你招架了%s释放的%s<br>
	N_NOTICE_F52,		//[181]:%s格挡了你释放的%s<br>
	N_NOTICE_F53,		//[182]:你格挡了%s释放的%s<br>
	N_NOTICE_F54,		//[183]:%s免疫了你释放的%s<br>
	N_NOTICE_F55,		//[184]:你免疫了%s释放的%s<br>
	N_NOTICE_F56,		//[185]:%s免疫了你释放的%s<br>
	N_NOTICE_F57,		//[186]:你免疫了%s释放的%s<br>
	N_NOTICE_F58,		//[187]:%s对你释放%s(偏斜)<br>
	N_NOTICE_F59,		//[188]:你对%s释放%s(偏斜)<br>
	N_NOTICE_F60,		//[189]:%s吸收了你释放的%s所造成的伤害<br>
	N_NOTICE_F61,		//[190]:你吸收了%s释放的%s所造成的伤害<br>
	N_NOTICE_F62,		//[191]:你对自己释放%s恢复%u的生命值<br>
	N_NOTICE_F63,		//[192]:你对%s释放%s恢复%u的生命值<br>
	N_NOTICE_F64,		//[193]:%s对你了释放了%s恢复你%u的生命值<br>
	
	
	N_NOTICE_F65,		//[194]:你对自己释放%s给自己恢复%u的法力值<br>
	N_NOTICE_F66,		//[195]:你对%s释放%s给恢复其%u的法力值<br>
	N_NOTICE_F67,		//[196]:%s对你了释放了%s恢复你%u的法力值<br>
	N_NOTICE_F68,		//[197]:你的%s对%s造成%u的伤害<br>
	N_NOTICE_F69,		//[198]:%s的%s对你造成%u的伤害<br>
	N_NOTICE_F70,		//[199]:你的%s对%s恢复了%u的生命<br>
	N_NOTICE_F71,		//[200]:%s的%s对你恢复了%u的生命<br>
	N_NOTICE_F72,		//[201]:你的%s对%s恢复了%u的魔法<br>
	N_NOTICE_F73,		//[202]:%s的%s对你恢复了%u的魔法<br>
	
	N_NOTICE_F74,		//[203]:%s消灭了了%s<br>
	N_NOTICE_F75,		//[204]:%s已经被消灭了<br>
	N_NOTICE_F76,		//[205]:您获得了%u点经验<br>
	
	N_NOTICE_F77,		//[206]:获得<linkcolor:%s><a:#Item_%u>[%s]</a>*%u<br>
	N_NOTICE_F78,		//[207]:恭喜你升到%u级了<br>
	N_NOTICE_F79,		//[208]:获得%d的荣誉值<br>
	N_NOTICE_F80,		//[209]:失去%d的荣誉值<br>
	N_NOTICE_F81,		//[210]:您已学习了%s%u级<br>
	N_NOTICE_F82,		//[211]:%s%s你施放的%s造成的%s效果<br>
	N_NOTICE_F83,		//[212]:你%s%s施放的%s造成的%s效果<br>
	
	N_NOTICE_F84,		//[213]:确定要捐助%u的元宝?
	N_NOTICE_F85,		//[214]:%d元宝
	
	N_NOTICE_F86,		//[215]:好友<a:#Name_%s>[%s]</a>进入了游戏
	N_NOTICE_F87,		//[216]:好友[%s]退出了游戏
	N_NOTICE_F88,		//[217]:[%s]已经从好友列表中删除
	
	N_NOTICE_F89,		//[218]:您确定需要花费 %d 金购买包位吗？
	
	N_NOTICE_F90 = 219,		//[219]:（%u）战力：%u
	N_NOTICE_F91 = 222,		//[222]:（%u）资源(%d)：%u


	N_NOTICE_C0 = 225,  //[225]:等级%d 武修
	N_NOTICE_C1,		//[226]:等级%d 羽箭
	N_NOTICE_C2,		//[227]:等级%d 仙道
	N_NOTICE_C3,		//[228]:等级%d 真巫
	N_NOTICE_C4,		//[229]:摧毁物品：[%s]
	N_NOTICE_C5,		//[230]:修理需要%d金%d银%d铜
	N_NOTICE_C6,		//[231]:修理需要%d银%d铜
	N_NOTICE_C7,		//[232]:修理需要%d银
	N_NOTICE_C8,		//[233]:修理需要%d铜
	N_NOTICE_C9,		//[234]: %d天
	N_NOTICE_C10,		//[235]:当前帧数：%5.2f fps
	N_NOTICE_C11,		//[236]:当前延迟：%u ms
	N_NOTICE_C12,		//[237]:id为%d没有找到
	N_NOTICE_C13,		//[238]:%s战胜了%u个人
	N_NOTICE_C14,		//[239]:%s战胜了%u个人奖励%u经验值<br>
	N_NOTICE_C15,		//[240]:适合等级：%u - %u
	N_NOTICE_C16,		//[241]:费用：%u个元宝
	N_NOTICE_C17,		//[242]:费用：%u个%s
	N_NOTICE_C18,		//[243]:费用：%u个金币
	N_NOTICE_C19,		//[244]:您将于%d秒后被传送至%s
	N_NOTICE_C20,		//[245]:%d秒后将要传出副本

	N_NOTICE_C21 = 248,	//[248]:等级 %d级
	N_NOTICE_C22,		//[249]:%.2f秒
	N_NOTICE_C23,		//[250]:<br>获得金钱奖励:%d
	N_NOTICE_C24,		//[251]:<br><a:#R_%d>可选奖励物品:%s 奖励数量%d</a>
	N_NOTICE_C25,		//[252]:<br>获得奖励物品:%s 奖励数量%d
	N_NOTICE_C26,		//[253]:<br>需要金钱:%d
	N_NOTICE_C27,		//[254]:<br>需要物品:%s

	N_NOTICE_C28 = 257,	//[257]:%d级
	N_NOTICE_C29,		//[258]:提取：你需要支付%d金%d银%d铜

	 
	N_NOTICE_C30 = 277,	//[277]:<font:黑体:14><color:4d2f07>获得:%s<br>

	N_NOTICE_C31 = 283,		//[283]:分配制度：%s，%s品质!		
	N_NOTICE_C32,			//[284]:完成主线任务%d以及其支线任务!
							
	N_NOTICE_C33 = 287,		//[287]:<br><font:宋体:14><color:4d2f07>您可以找<linkcolor:137300><a:#NPC %d %d %d %d>%s</a>领取本任务<br>
	N_NOTICE_C34,			//[288]:<br><font:宋体:14><color:4d2f07>您可以找%s</a>领取本任务<br>
	N_NOTICE_C35 = 295,		//[295]:<br><font:宋体Bold:14><color:3f2404>获得荣誉 : <font:宋体:14><color:4d2f07>%d <br>
	N_NOTICE_C36,			//[296]:<br><font:宋体Bold:14><color:3f2404>获得金钱 : <font:宋体:14><color:4d2f07> %s <br>
	N_NOTICE_C37,			//[297]:<br><font:宋体Bold:14><color:3f2404>获得经验 : <font:宋体:14><color:4d2f07>%d <br>
	N_NOTICE_C38 = 299,		//[299]:需要荣誉 : %d / %d<br>				
	N_NOTICE_C39,			//[300]:任务%s ： 没有找到完成NPC的数据
	N_NOTICE_C40 = 308,		//[308]:恭喜！可以找<linkcolor:137300><a:#NPC %d %d %d %d>%s</a>提交本任务了 <br>
	N_NOTICE_C41,			//[309]:<color:137300>恭喜！可以找%s提交本任务了 <br>
	N_NOTICE_C42,			//[310]:精炼等级 %d<color:A73CE8> +↑%d <br>
	N_NOTICE_C43,			//[311]:精炼等级 + %d <br>
	N_NOTICE_C44,			//[312]:%s护甲 +%d <color:A73CE8>+ ↑%d<br>
	N_NOTICE_C45,			//[313]:%s护甲 +%d<br>
	N_NOTICE_C46,			//[314]:%s开孔 %d <br>未镶嵌 <br>未镶嵌 <br>未镶嵌 <br>
	N_NOTICE_C47,			//[315]:%s开孔 %d <color:A73CE8>+ ↑%d<br>%s未镶嵌 <br>未镶嵌 <br>
	N_NOTICE_C48,			//[316]:%s开孔 %d <br>未镶嵌 <br>未镶嵌 <br>
	N_NOTICE_C49,			//[317]:%s开孔 %d <color:A73CE8>+ ↑%d<br>%s未镶嵌 <br>
	N_NOTICE_C50,			//[318]:%s开孔 %d <br>未镶嵌 <br>
	N_NOTICE_C51,			//[319]:%s开孔 %d <color:A73CE8>+ ↑%d<br>
	N_NOTICE_C52,			//[320]:%s开孔 %d <br>
	N_NOTICE_C53,			//[321]:%s%s %5.0f - %5.0f <color:A73CE8>+ ↑%d<br>
	N_NOTICE_C54,			//[322]:%s%s  +%d <color:A73CE8>+ ↑%d<br>
	N_NOTICE_C55,			//[323]:%s需求等级  %d级<br>
	N_NOTICE_C56,			//[324]:%d分钟
	N_NOTICE_C57,			//[325]:%d秒
	N_NOTICE_C58,			//[326]:%s要复活你
	N_NOTICE_C59,			//[327]:小队%d
	N_NOTICE_C60,			//[328]:[%s]加入了团队
	N_NOTICE_C61,			//[329]:[%s]加入了队伍
	N_NOTICE_C62,			//[330]:[%s]离开了团队
	N_NOTICE_C63,			//[331]:[%s]离开了队伍
	N_NOTICE_C64,			//[332]:[%s]成为团长
	N_NOTICE_C65,			//[333]:[%s]成为队长
	N_NOTICE_C66,			//[334]:<font:黑体:14><color:415194><center>当前称号:%s<br><font:黑体:13><color:20C301><left>效果:%s<br>
	N_NOTICE_C67,			//[335]:<font:黑体:14><color:415194><center>当前称号:%s<br>
	N_NOTICE_C68,			//[336]:<font:黑体:14><color:415194><center>%s<br><br><color:DA9B01><left><font:黑体:13>描述:%s<br><br><color:20C301>效果:%s<br>
	N_NOTICE_C69,			//[337]:<font:黑体:14><color:415194><center>%s<br><br><color:DA9B01><left><font:黑体:13>描述:%s<br> 

	//add new
	N_NOTICE_C70,			//338 获得新任务[<LANGUAGE_FLAG_STRING_0>] <br>
	N_NOTICE_C71,			//339 恭喜您，可以去提交任务[<LANGUAGE_FLAG_STRING_0>] <br>
	N_NOTICE_C72,			//340 恭喜您完成了任务[<LANGUAGE_FLAG_STRING_0>] <br>
	N_NOTICE_C73,			//341 您已经放弃任务[<LANGUAGE_FLAG_STRING_0>] <br>
	N_NOTICE_C74,			//342 您无法完成任务[<LANGUAGE_FLAG_STRING_0>],任务失败 <br>

	N_NOTICE_C75,			//343 (每秒伤害<LANGUAGE_FLAG_STRING_0>)
	N_NOTICE_C76,			//344 速度<LANGUAGE_FLAG_STRING_0>
	N_NOTICE_C77,			//345 <LANGUAGE_FLAG_STRING_0> - <LANGUAGE_FLAG_STRING_1> 伤害
	N_NOTICE_C78,			//346 与<LANGUAGE_FLAG_STRING_0>交易成功

	N_NOTICE_C79,			//347 [<LANGUAGE_FLAG_STRING_0>]邀请你加入队伍!
	N_NOTICE_C80,			//348 [<LANGUAGE_FLAG_STRING_0>]拒绝了您的邀请!
	N_NOTICE_C81,			//349 [<LANGUAGE_FLAG_STRING_0>]邀请你进行骑乘!
	N_NOTICE_C82,			//350 [<LANGUAGE_FLAG_STRING_0>]同意了您的邀请!
	N_NOTICE_C83,			//351 [<LANGUAGE_FLAG_STRING_0>]拒绝了您的邀请!
	N_NOTICE_C84,			//352 [<LANGUAGE_FLAG_STRING_0>]邀请你加入部族<<LANGUAGE_FLAG_STRING_1>>!
	N_NOTICE_C85,			//353 有效时间:<LANGUAGE_FLAG_STRING_0> 至 <LANGUAGE_FLAG_STRING_1>

	N_NOTICE_C86,			//354[部族事件]:<LANGUAGE_FLAG_STRING_0>修改您的阶位为<LANGUAGE_FLAG_STRING_1>
	N_NOTICE_C87,			//355您修改<LANGUAGE_FLAG_STRING_0>的阶位为<LANGUAGE_FLAG_STRING_1>
	N_NOTICE_C88,			//356<LANGUAGE_FLAG_STRING_0>修改<LANGUAGE_FLAG_STRING_1>的阶位为<LANGUAGE_FLAG_STRING_2>
	N_NOTICE_C89,			//357 恭喜<LANGUAGE_FLAG_STRING_0>获得了<LANGUAGE_FLAG_STRING_1>称号
	N_NOTICE_C90,			//358 <LANGUAGE_FLAG_STRING_0>害怕的逃跑了<br>
	N_NOTICE_C91,			//359 <LANGUAGE_FLAG_STRING_0>小时<LANGUAGE_FLAG_STRING_1>分
	N_NOTICE_C92,			//360 [<LANGUAGE_FLAG_STRING_0>]已经成功的加入你的黑名单
	N_NOTICE_C93,			//361 <LANGUAGE_FLAG_STRING_0>分
	N_NOTICE_C94,			//362 <LANGUAGE_FLAG_STRING_0>正在召唤你到<LANGUAGE_FLAG_STRING_1>
	N_NOTICE_C95,			//363 第<LANGUAGE_FLAG_STRING_0>级部族升级

	N_NOTICE_X1,			//力量可以让手臂更加有力，对武修尤为重要，提高近战攻击强度。
	N_NOTICE_X2,			//
	N_NOTICE_X3,			//
	N_NOTICE_X4,			//
	N_NOTICE_X5,			//
	N_NOTICE_X6,			//
	N_NOTICE_X7,			//
	N_NOTICE_X8,			//
	N_NOTICE_X9,			//
	N_NOTICE_X10,			//
	N_NOTICE_X11,			//
	N_NOTICE_X12,			//
	N_NOTICE_X13,			//
	N_NOTICE_X14,			//
	N_NOTICE_X15,			//
	N_NOTICE_X16,			//
	N_NOTICE_X17,			//
	N_NOTICE_X18,			//
	N_MAX ,
};
#endif