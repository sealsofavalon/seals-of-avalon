#pragma once

enum LightColorNames 
{
	DIFFUSE_COLOR = 0,
	AMBIENT_COLOR,
	WATER_DIFFUSE_COLOR,
	WATER_AMBIENT_COLOR,
	FOG_COLOR,
	NUM_LIGHT_COLOR = 16, //保留一些方便以后扩充
};

#define DEFAULT_COLOR NiColor(1.0f, 1.0f, 1.0f)
#define LIGHT_DIR NiPoint3(1.f, 1.f, -1.f)

#define NUM_DAY_NIGHT_COLOR  24
class CDayNightColor : public NiMemObject
{
private:
	unsigned int m_uiFlag;
	NiColor m_kColors[NUM_DAY_NIGHT_COLOR];

public:
	NiColor GetColor(unsigned int uiHour, unsigned int uiMinute) const
	{
		NIASSERT( uiHour < NUM_DAY_NIGHT_COLOR );

		float r = uiMinute / 60.f;
		NiColor kColor1 = m_kColors[uiHour];
		NiColor kColor2 = m_kColors[uiHour == NUM_DAY_NIGHT_COLOR - 1 ? 0 : uiHour + 1];

		return kColor1 * (1.f - r) + kColor2 * r;
	}
};

class CTerrainLight : public NiMemObject
{
private:
	NiPoint3		m_kPosition;
	float			m_fInnerRadius;//内半径
	float			m_fOuterRadius;//内外半径
	bool			m_bGlobal;
	CDayNightColor 	m_akColors[NUM_LIGHT_COLOR];

	float			m_fWeight;

public:
	CTerrainLight()
		:m_fWeight(0.f)
	{
	}

	bool operator<(const CTerrainLight& l) const
	{
		//全局的最大
		if (m_bGlobal)
			return false;

		if (l.m_bGlobal)
			return true;

		if( NiAbs(m_fOuterRadius - l.m_fOuterRadius) > 0.1f )
			return m_fOuterRadius < l.m_fOuterRadius; 

		return m_kPosition.x < l.m_kPosition.x;
	}

	const NiPoint3& GetPosition() const { return m_kPosition; }

	float GetInnerRadius() const { return m_fInnerRadius; }
	float GetOuterRadius() const { return m_fOuterRadius; }
	
	float GetWeight() const { return m_fWeight; }
	void SetWeight( float fWeight ) { m_fWeight = fWeight; }


	bool IsGlobal() const { return m_bGlobal; }

	NiColor GetColor(unsigned int uiColor, unsigned int uiHour, unsigned int uiMinute) const;

	bool Load(NiFile* pkFile);
};

class CLightManager : public NiMemObject
{
public:
	CLightManager();
	virtual ~CLightManager();

public:
	void CalcLightWeights(const NiPoint3& pos);
	void Update(float dt);
	void ApplyDirLight(NiNode* pkObject);
	void DetachDirLight(NiNode* pkObject);

	bool Load(const NiFixedString& kMapName);
	void Unload(); 
	void ResetColors();

	const NiColor& GetAmbientColor() const { return m_akColors[AMBIENT_COLOR]; }
	const NiColor& GetDiffuseColor() const { return m_akColors[DIFFUSE_COLOR]; }

	const NiColor& GetWaterAmbientColor() const { return m_akColors[WATER_AMBIENT_COLOR]; }
	const NiColor& GetWaterDiffuseColor() const { return m_akColors[WATER_DIFFUSE_COLOR]; }
	const NiColor& GetFogColor() const { return m_akColors[FOG_COLOR]; }

	unsigned int GetVersion() const { return m_uiVersion; }


	static CLightManager* Get() { return ms_This; }

protected:
	NiDirectionalLightPtr m_DirLight;

	NiTPrimitiveArray<CTerrainLight*> m_akLightList;
	unsigned int m_uiCurrentLight;
	NiColor m_akColors[NUM_LIGHT_COLOR];
	unsigned int m_uiVersion;

private:
	static CLightManager* ms_This;
};
