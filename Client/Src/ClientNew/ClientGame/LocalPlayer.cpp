#include "StdAfx.h"
#include "LocalPlayer.h"
#include "ObjectManager.h"
#include "Player.h"
#include "SceneManager.h"
#include "ItemManager.h"
#include "LightManager.h"
#include "PlayerInputMgr.h"
#include "ObjectManager.h"
#include "AudioInterface.h"
#include "Network/PacketBuilder.h"
#include "Utils/ClientUtils.h"
#include "AudioInterface.h"
#include "Map/Map.h"
#include "Map/Chunk.h"
#include "Effect/DecalManager.h"
#include "Avatar.h"
#include "Effect/EffectManager.h"
#include "Effect/DecalManager.h"
#include "Skill/SkillManager.h"
#include "ui/UISystem.h"
#include "ui/UIItemSystem.h"
#include "State/StateAttack.h"
#include "State/StateIdle.h"
#include "State/StateMove.h"
#include "State/StateMoutMove.h"
#include "Console.h"
#include "ClientState.h"
#include "Network/NetworkManager.h"
#include "ui/UIGamePlay.h"
#include "Utils/QuestFinisherDB.h"
#include "Utils/CreatureSpawnsDB.h"
#include "UI/UIMiniMap.h"
#include "UI/UQuestLog.h"
#include "UI/UChat.h"
#include "ui/UISkill.h"
#include "Utils/QuestDB.h"
#include "SYMount.h"
#include "ResourceManager.h"
#include "ClientApp.h"
#include "ui/UInGameBar.h"
#include "ui/UIPlayerEquipment.h"
#include "ui/UNpcQuestDlg.h"
#include "ui/UReliveMsgBox.h"
#include "ui/SystemTips .h"
#include "ui/USuperSkill.h"
#include "ui/UBshowLevelTips.h"
#include "ui/UEquipmentWear.h"
#include "ui/UFriendsDlg.h"
#include "ui/UInstanceMgr.h"
#include "ui/UInGameBar.h"
#include "ui/UFun.h"
#include "Effect/SceneEffect.h"
#include "PathFinder.h"
#include "ui/UEquRefineManager.h"
#include "ItemSlot.h"
#include "AudioInterface.h"
#include "ui/TeacherSystem.h"
#include "ui/UInstanceMgr.h"
#include "ui/UIShowPlayer.h"
#include "ui/TitleMgr.h"
#include "ui/UIFadeText.h"
#include "Utils/CPlayerTitleDB.h"
#include "Utils/AreaDB.h"
#include "Hook/HookManager.h"
#include "DpsCount.h"
#include "ui/UINPCTrade.h"


#define TRANSPORT_DELAY_TIME (10.f)

ActiveLocalPlayerDesc CPlayerLocal::sm_ActiveLPDesc;
IStateProxy* CPlayerLocal::sm_StateProxys[STATE_MAX] = {NULL};


CPlayerLocal::CPlayerLocal(void)
{
	unsigned int i;
	for (i = 0; i < EFFECT_MAX; i++)
	{
		m_pkEffectList[i] = NULL;
	}

	m_uiFollowPlayerID = 0;
	m_fFollowRadius = 2.0f;

	CDecalManager* pkDecalManager = SceneMgr->GetDecalMgr();
	NIASSERT(pkDecalManager != NULL);

	// 角色数据创建在哪里?
	//	m_pkEffectList[MOVETARGETPOINT] = EffectMgr->CreateSceneEffect("Effect_000003000.nif", true);

	m_uiLastMovementFlags = 0;
	m_fLastSendMovementTime = FLT_MIN;
	m_fLastFacingTime =  FLT_MIN;
	m_fSendMovementDuration = 5.0f;


	TeacherSystem::InitMgr();
	m_CUState = cus_Normal;

	m_fLastDesiredAngle = 0.0f;
	m_bAutoRun = false;

	m_fLastTransportTime = -TRANSPORT_DELAY_TIME;

	m_eShadowType = CShadowGeometry::ST_DYNAMIC;

	m_pkAStarDir = NULL;

	m_uiLastHonor = 0;

	m_pkMapToMapInfo.clear();
	m_pkMapToMapNPC = 0;
	m_pkBChangeMap = FALSE;
	m_pkTraceGate = FALSE ;
	m_MoveToTarget = FALSE ;
	m_fRange = 0.0f;
	m_bAutoActionPos = false;

	m_bShowPositon = false;

	m_pEyeNod = NULL;

	//SM
	SM_CriticalChance=0;
	SM_FDur=0;//flat
	SM_PDur=0;//pct
	SM_FRadius=0;
	SM_FRange=0;
	SM_PCastTime=0;
	SM_FCastTime=0;
	SM_PCriticalDamage=0;
	SM_FDOT=0;
	SM_PDOT=0;
	SM_FEffectBonus=0;
	SM_PEffectBonus=0;
	SM_FDamageBonus=0;
	SM_PDamageBonus=0;
	SM_PSPELL_VALUE=0;
	SM_FSPELL_VALUE=0;
	SM_FHitchance=0;
	SM_PRange=0;//pct
	SM_PRadius=0;
	SM_PAPBonus=0;
	SM_PCost=0;
	SM_FCost=0;
	SM_FAdditionalTargets=0;
	SM_PJumpReduce=0;
	SM_FSpeedMod=0;
	SM_PNonInterrupt=0;
	SM_FPenalty=0;
	SM_PPenalty=0;
	SM_FCooldownTime = 0;
	SM_PCooldownTime = 0;
	SM_FChanceOfSuccess = 0;
	SM_FRezist_dispell = 0;
	SM_PRezist_dispell = 0;
	SM_FThreatReduce = 0;
	SM_PThreatReduce = 0;
	SM_FSPELL_VALUE0 = 0;
	SM_FSPELL_VALUE1 = 0;
	SM_FSPELL_VALUE2 = 0;
	SM_PSPELL_VALUE0 = 0;
	SM_PSPELL_VALUE1 = 0;
	SM_PSPELL_VALUE2 = 0;

	for (int i = 0 ; i < 12 ; ++i)
	{
		m_LocalBuyBack[i] = 0;
		m_LocalBuyBackPrice[i] = 0;
	}
	InitPVPInfo();
}

static bool bFirst = true;
CPlayerLocal::~CPlayerLocal(void)
{

	TeacherSystem::ShutDown();

	if (IsHook())
	{
		HookMgr->EndHookActor();
	}
	if(m_PathFinder)
	{
		delete m_PathFinder;
		m_PathFinder = NULL;
	}
	bFirst = true;

	m_pkMapToMapInfo.clear();
	m_pkMapToMapNPC = 0;
	m_pkBChangeMap = FALSE;
	m_pkTraceGate = FALSE;

	if (m_pkAStarDir)
	{
		EffectMgr->ReleaseEffect(m_pkAStarDir);
		m_pkAStarDir = NULL;
	}

	ULearnSkillBar *pSkillLearn = INGAMEGETFRAME(ULearnSkillBar,FRAME_IG_NEWLEARN);
	if ( pSkillLearn )
	{
		pSkillLearn->Clear();
	}
	

	if(SM_CriticalChance != 0) delete [] SM_CriticalChance ;
	if(SM_FDur != 0) delete [] SM_FDur ;//flat
	if(SM_PDur != 0) delete [] SM_PDur ;//pct
	if(SM_FRadius != 0) delete [] SM_FRadius ;
	if(SM_FRange != 0) delete [] SM_FRange ;
	if(SM_PCastTime != 0) delete [] SM_PCastTime ;
	if(SM_FCastTime != 0) delete [] SM_FCastTime ;
	if(SM_PCriticalDamage != 0) delete [] SM_PCriticalDamage ;
	if(SM_FDOT != 0) delete [] SM_FDOT ;
	if(SM_PDOT != 0) delete [] SM_PDOT ;
	if(SM_PEffectBonus != 0) delete [] SM_PEffectBonus ;
	if(SM_FEffectBonus != 0) delete [] SM_FEffectBonus ;
	if(SM_FDamageBonus != 0) delete [] SM_FDamageBonus ;
	if(SM_PDamageBonus != 0) delete [] SM_PDamageBonus ;
	if(SM_PSPELL_VALUE != 0) delete [] SM_PSPELL_VALUE ;
	if(SM_FSPELL_VALUE != 0) delete [] SM_FSPELL_VALUE ;
	if(SM_FHitchance != 0) delete [] SM_FHitchance ;
	if(SM_PRange != 0) delete [] SM_PRange ;//pct
	if(SM_PRadius != 0) delete [] SM_PRadius ;
	if(SM_PAPBonus != 0) delete [] SM_PAPBonus ;
	if(SM_PCost != 0) delete [] SM_PCost ;
	if(SM_FCost != 0) delete [] SM_FCost ;
	if(SM_FAdditionalTargets != 0) delete [] SM_FAdditionalTargets ;
	if(SM_PJumpReduce != 0) delete [] SM_PJumpReduce ;
	if(SM_FSpeedMod != 0) delete [] SM_FSpeedMod ;
	if(SM_PNonInterrupt != 0) delete [] SM_PNonInterrupt ;
	if(SM_FPenalty != 0) delete [] SM_FPenalty ;
	if(SM_PPenalty != 0) delete [] SM_PPenalty ;
	if(SM_FCooldownTime != 0) delete [] SM_FCooldownTime ;
	if(SM_PCooldownTime != 0) delete [] SM_PCooldownTime ;
	if(SM_FChanceOfSuccess != 0) delete [] SM_FChanceOfSuccess ;
	if(SM_FRezist_dispell != 0) delete [] SM_FRezist_dispell ;
	if(SM_PRezist_dispell != 0) delete [] SM_PRezist_dispell ;
	if(SM_FThreatReduce != 0) delete [] SM_FThreatReduce ;
	if(SM_PThreatReduce != 0) delete [] SM_PThreatReduce ;

	if(SM_FSPELL_VALUE0 != 0) delete [] SM_FSPELL_VALUE0 ;
	if(SM_FSPELL_VALUE1 != 0) delete [] SM_FSPELL_VALUE1 ;
	if(SM_FSPELL_VALUE2 != 0) delete [] SM_FSPELL_VALUE2 ;
	if(SM_PSPELL_VALUE0 != 0) delete [] SM_PSPELL_VALUE0 ;
	if(SM_PSPELL_VALUE1 != 0) delete [] SM_PSPELL_VALUE1 ;
	if(SM_PSPELL_VALUE2 != 0) delete [] SM_PSPELL_VALUE2 ;

	InitPVPInfo();
}
void CPlayerLocal::InitPVPInfo()
{
	m_uiAreaId = 0;
	m_kMapName = NULL;
	m_kLastAreaPos = NiPoint3::ZERO;
	m_uiAreaFlags = 0;

	m_IsChangMap = TRUE;

	m_ChangMapOK = FALSE ;
}
void CPlayerLocal::UpdataArea()
{
	//UMinimapFrame* pkMiniMap = INGAMEGETFRAME(UMinimapFrame,FRAME_IG_MINIMAP);
	//if (pkMiniMap)
	//{
	//	pkMiniMap->UpdataArea();
	//}

	//如果在加载地图的时候，不更新PVP信息等等
	if (m_IsChangMap)
	{
		return ;
	}
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	const NiPoint3& kPos = GetPosition();

	unsigned int uiAreaId = pkMap->GetAreaId(kPos.x, kPos.y);

	//BOOL bMapChang = FALSE ;
	if (m_kMapName != pkMap->GetMapName() || !m_kMapName.Exists())
	{
		OnMapChanged();
		OnAreaChanged(m_uiAreaId, TRUE);
		return ;
	}

	if(m_uiAreaId != uiAreaId)
	{
		bool bAreaChanged = false;
		if(m_uiAreaId == 0)
		{
			bAreaChanged = true;
		}
		else if((m_kLastAreaPos - kPos).SqrLength() > 9.f)
		{
			//缓冲
			//只有进入一个区3米后才算玩家已经到达这个区
			bAreaChanged = true;
		}
		if(bAreaChanged)
		{
			OnAreaChanged(uiAreaId);
		}
	}
	else
	{
		m_kLastAreaPos = kPos;
	}
}

void CPlayerLocal::OnAreaFlagsChanged(BOOL bChangMap)
{
	uint8 area = 0;
	if (m_uiAreaFlags & AreaInfo::SAFEZONE)
		area = 2;
	else
		if (m_uiAreaFlags & AreaInfo::PVP)
			area = 1;


	if(area != 2)
	{
		bool ShowSys = true ;
		if (area == 0 && bChangMap)
		{
			ShowSys = false ;
		}
		if (ShowSys)
		{
			ClientSystemNotify(_TRAN("该区域可能被强制PK，若反感此内容，请尽快离开本区域!"));
		}
	}

	if (area == 0)
		PacketBuilder->SendEnterLeavePVPZone(false);
	else
		PacketBuilder->SendEnterLeavePVPZone(true, area);
}

void CPlayerLocal::OnMapChanged()
{
	//通知小地图 地图信息改变

	CMap* pkMap = CMap::Get();
	if( !pkMap )
	{
		return ;
	}
	m_kMapName = pkMap->GetMapName();
	m_uiAreaId = 0;
	m_kLastAreaPos = NiPoint3::ZERO;
	m_uiAreaFlags = 0;

	m_ChangMapOK = TRUE;

	//UMinimapFrame* pkMiniMap = INGAMEGETFRAME(UMinimapFrame,FRAME_IG_MINIMAP);
	if (g_pkMiniMap)
	{
		g_pkMiniMap->OnMapChanged();
	}
}
void CPlayerLocal::OnAreaChanged(unsigned int uiAreaId, BOOL bChangMap)
{
	//更新地图的名称
	NiFixedString kAreaName;
	NiFixedString kAreaDesc;
	unsigned int uiAreaFlags = 0;
	const AreaInfo* pkAreaInfo = g_pkAreaDB->GetAreaInfo(uiAreaId);
	if(pkAreaInfo != NULL)
	{
		kAreaName = pkAreaInfo->kName;
		kAreaDesc = pkAreaInfo->kDesc;
		uiAreaFlags = pkAreaInfo->uiFlags;
	}

	//m_MapName->SetText(kAreaName); // 小地图区域名字
	if (g_pkMiniMap)
	{
		g_pkMiniMap->SetMapName(kAreaName);
	}

	uint8 area = 0;
	if (uiAreaFlags & AreaInfo::SAFEZONE)
		area = 2;
	else
		if (uiAreaFlags & AreaInfo::PVP)
			area = 1;
	UColor col = LCOLOR_GREEN;
	if(area != 2)
		col = LCOLOR_RED;

	static const char* color[] ={
		"ffff00",
		"ff0000",
		"00ff00",
	};

	UIFadeText* pAreaNameControl = INGAMEGETFRAME(UIFadeText, FRAME_IG_AREA_NAME);
	if(pAreaNameControl)
	{
		pAreaNameControl->SetTextColor(color[uiAreaFlags]);

		pAreaNameControl->SetText(kAreaName);
		if(m_uiAreaId != 0)
			pAreaNameControl->StartFade(1.f, -0.2f);
		else
			pAreaNameControl->StartFade(1.f, -0.1f);
	}

	UIFadeText* pAreaDescControl = INGAMEGETFRAME(UIFadeText, FRAME_IG_AREA_DESC);
	if(pAreaDescControl)
	{
		pAreaDescControl->SetTextColor(color[uiAreaFlags]);

		pAreaDescControl->SetText(kAreaDesc);
		if(m_uiAreaId != 0)
			pAreaDescControl->StartFade(1.f, -0.2f);
		else
			pAreaDescControl->StartFade(1.f, -0.1f);
	}

	PacketBuilder->SendZoneUpdate(uiAreaId);

	//Flags
	if(m_uiAreaFlags != uiAreaFlags || bChangMap || m_ChangMapOK)
	{
		m_uiAreaFlags = uiAreaFlags;
		OnAreaFlagsChanged(bChangMap);

		if (m_ChangMapOK)
		{
			m_ChangMapOK = FALSE;
		}

		if (bChangMap)
		{
			m_ChangMapOK = TRUE;
		}

	}

	//声音没有使用异步后台播放
	//切换背景音效的时候, 会在主线程中把整个文件先读
	//进内存, 这样会导致画面卡一下
	SYState()->IAudio->OnArea(uiAreaId);

	m_uiAreaId = uiAreaId;
}
void CPlayerLocal::Update(float dt)
{
	m_Camera.UpdateBindingCharacterInput();
	CPlayer::Update(dt);
	if(g_pkConsole)
		g_pkConsole->Process();
	m_Camera.Update(dt);

	UpdataArea();

#ifdef NIDEBUG
	if(SYState()->Input->GetKeyboard()->KeyIsDown(NiInputKeyboard::KEY_6))
	{
		if(!g_pkConsole)
		{
			Console::Create();
			g_pkConsole->Enable(true);
		}
	}

#endif


	if ( m_bShowPositon )
	{
		char Pos[256] = {0};
		NiPoint3 ptCur = GetPosition();
		float fangle = GetFacingAngle();

		sprintf(Pos, "当前位置：x:%0.1f, y:%0.1f,z:%0.1f, orient: %0.1f", ptCur.x, ptCur.y, ptCur.z, fangle );

		UISystemMsg* pSGN = INGAMEGETFRAME(UISystemMsg, FRAME_IG_SYSGIFTNOTIFY);

		pSGN->GetInstaceTime(Pos, 1.f);
	}


	/*if(SYState()->Input->GetKeyboard()->KeyIsDown(NiInputKeyboard::KEY_7))
	{
	SYState()->LocalPlayerInput->PickItem();
	}*/

}

void CPlayerLocal::UpdatePhysics(float fDelta)
{
	__super::UpdatePhysics(fDelta);
}

void CPlayerLocal::OnCreate(class ByteBuffer* data, ui8 update_flags)
{
	CPlayer::OnCreate(data, update_flags);

	NIASSERT( update_flags & UPDATETYPE_CREATE_YOURSELF );
	ObjectMgr->SetLocalPlayer(this);
	m_pkMapToMapInfo.clear() ;
	m_pkMapToMapNPC = 0;
	m_pkBChangeMap = FALSE;
	m_pkTraceGate = FALSE;
	SetYuanBao(0);
	SetMoney(0);

	//刷新任务日志
	UQuestDlg* QuestLog = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
	if(QuestLog)
	{
		QuestLog->GetQuestList()->OrderFinishQuestTem();
	}

	if (HookMgr)
	{
		HookMgr->LoadHookFile();
	}

	if ( !m_pEyeNod )
	{
		m_pEyeNod = GetSceneNode()->GetObjectByName( "Dummy02" );
	}

	CPlayerLocal::SetCUState(cus_Normal, TRUE);
}
#include"ui/UFittingRoom.h"
void CPlayerLocal::OnActorLoaded(NiActorManager* pkActorManager)
{
	CPlayer::OnActorLoaded(pkActorManager);

	SYState()->IAudio->SetListenerNode(GetSceneNode());

	UEquipment* pEquipment = INGAMEGETFRAME(UEquipment,FRAME_IG_PLAYEREQUIPDLG);
	if (pEquipment)
	{
		pEquipment->SetAddLocalData(this);
	}
	TeamShow->OnLocalPlayerCreate(this);

	UFittingRoom* pFittingRoom = INGAMEGETFRAME(UFittingRoom, FRAME_IG_FITTINGROOMDLG);
	if(!pFittingRoom)
		return;
	pFittingRoom->AddViewPlayer(this);
}


void CPlayerLocal::PostOnCreate()
{
	InitCamera(); //初始化摄像机
	CCharacter::PostOnCreate();
}

void CPlayerLocal::OnValueChanged(class UpdateMask* mask)
{
	CPlayer::OnValueChanged(mask);
	if( mask->GetBit(UNIT_FIELD_LEVEL) )
	{
		TeamShow->UpdateLocal();
	}

	if(mask->GetBit(UNIT_FIELD_HEALTH))
	{
		TeamShow->UpdateLocal();
	}
	if (mask->GetBit(UNIT_FIELD_MAXHEALTH))
	{
		TeamShow->UpdateLocal();
	}
	if (mask->GetBit(UNIT_FIELD_POWER1))
	{
		TeamShow->UpdateLocal();
	}
	if (mask->GetBit(UNIT_FIELD_MAXPOWER1))
	{
		TeamShow->UpdateLocal();
	}
	if(mask->GetBit(UNIT_FIELD_FLAGS))
	{
		//进入战斗状态
		if(HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT) && !(m_UnitFlag & UNIT_FLAG_COMBAT))
		{
			m_UnitFlag |= UNIT_FLAG_COMBAT;
			SetCUState(cus_Normal);
			ULOG("战斗状态(cus_Normal)");
			EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("进入战斗！"));
			DamageMgr->OnStartAnalyst( GetGUID() );
		}else if(!(HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT)) && (m_UnitFlag & UNIT_FLAG_COMBAT))
		{
			EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("脱离战斗！"));
			DamageMgr->OnEndAnalyst( GetGUID() );
			m_UnitFlag &= ~UNIT_FLAG_COMBAT ;
		}
	}
	if(mask->GetBit(PLAYER_FIELD_INSTANCE_BUSY))
	{
		InstanceSys->UpdateCurrentState();
	}

	if(mask->GetBit(UNIT_FIELD_ICE_BLOCK))
	{
		/*
		if(GetUInt32Value(UNIT_FIELD_ICE_BLOCK))
		{
		StopMove();
		if (g_pkConsole)
		g_pkConsole->Printf(162, "UNIT_FIELD_ICE_BLOCK = 1\n");

		CPlayerInputMgr * InputMgr = SYState()->LocalPlayerInput;
		InputMgr->LockMove();
		}
		else
		{
		if (g_pkConsole)
		g_pkConsole->Printf(162, "UNIT_FIELD_ICE_BLOCK = 0\n");

		CPlayerInputMgr * InputMgr = SYState()->LocalPlayerInput;
		InputMgr->UnlockMove();
		}
		*/
	}

	//	任务日志
	for(ui32 i = 0; i < 50; ++i)
	{
		ui32 questslot = PLAYER_QUEST_LOG_1_1 + i * MAX_QUEST_OFFSET + QUEST_ID_OFFSET;
		ui32 cntslot = PLAYER_QUEST_LOG_1_1 + i * MAX_QUEST_OFFSET + QUEST_COUNTS_OFFSET;
		ui32 timeslot = PLAYER_QUEST_LOG_1_1 + i * MAX_QUEST_OFFSET + QUEST_TIME_OFFSET;
		ui32 statslot = PLAYER_QUEST_LOG_1_1 + i * MAX_QUEST_OFFSET + QUEST_STATE_OFFSET;
		if( mask->GetBit( questslot ) || mask->GetBit(cntslot) || mask->GetBit(timeslot) || mask->GetBit(statslot) )
		{
			HELPEVENTTRIGGER(TUTORIAL_FLAG_QUEST_LOG);
			ui32 questid = GetUInt32Value(questslot+QUEST_ID_OFFSET);
			ui32 queststat = GetUInt32Value(questslot+QUEST_STATE_OFFSET);
			ui32 questcount = GetUInt32Value(questslot+QUEST_COUNTS_OFFSET);

			std::vector<ui32> vCreatureCount;
			vCreatureCount.clear();
			for( ui32 creature_idx = 0; creature_idx < 4; creature_idx++ )
			{
				ui32 creature_count =  0 ;
				creature_count = GetByteValue(cntslot,creature_idx);
				vCreatureCount.push_back(creature_count);
			}
			ui32 questtime = GetUInt32Value(timeslot);

			UInGame* pIngame = UInGame::Get();
			UQuestDlg * pUQuestLog = NULL;
			if (pIngame)
			{
				pUQuestLog = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
			}
			if (pUQuestLog)
			{
				pUQuestLog->GetQuestList()->OnUpdate(questid, queststat, vCreatureCount, i);

			}
			UpdateNpcState(questid, queststat);
			UpdateScenOBJState();
			// 任务完成特效
			if(queststat == QMGR_QUEST_FINISHED || queststat == QMGR_QUEST_REPEATABLE_FINISHED)
			{
				//任务可以交付的所有提示。
				ChatSystem->AppendQuestMsg(questid , CanFinishQuest);
			}

		}
	}
	BOOL bUpdateEQUI = FALSE;

	//	装备显示信息
	for( ui16 index = EQUIPMENT_SLOT_START; index < EQUIPMENT_SLOT_END; index++ )
	{
		int GuidBase = PLAYER_FIELD_INV_SLOT_HEAD + (index * 2);
		int visiblebase = PLAYER_VISIBLE_ITEM_1_0 + (index * 12);
		if( mask->GetBit(GuidBase) )
		{
			ui64 Guid = GetUInt64Value(GuidBase);
			//			ItemMgr->OnEuipmentChange(GetGUID(), GetUInt32Value(visiblebase), index);
			ItemMgr->AddItemToEquipmentContainer((ui8)index, GetUInt32Value(GuidBase),Guid);

			UEquipment* pEquipment = INGAMEGETFRAME(UEquipment,FRAME_IG_PLAYEREQUIPDLG);
			UEquipmentWear* pkEuipWear = INGAMEGETFRAME(UEquipmentWear,FRAME_IG_EQUIPWEAR);
			UFittingRoom* pkFittingRoom = INGAMEGETFRAME(UFittingRoom,FRAME_IG_FITTINGROOMDLG);

			if (pEquipment)
			{
				bool bChange = true;

				if(m_bShowShizhuang)
				{
					if(index == INVTYPE_HEAD)
					{
						uint32 Testentry = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_TOUBU * 12));
						if(Testentry)
							bChange = false;
					}
					else if(index == INVTYPE_SHOULDERS)
					{
						uint32 Testentry = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_JIANBANG * 12));
						if(Testentry)
							bChange = false;
					}
					else if(index == INVTYPE_CHEST)
					{
						uint32 Testentry = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_SHANGYI * 12));
						if(Testentry)
							bChange = false;
					}
					else if(index == INVTYPE_TROUSERS)
					{
						uint32 Testentry = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_XIAYI * 12));
						if(Testentry)
							bChange = false;
					}
					else if(index == INVTYPE_BOOTS)
					{
						uint32 Testentry = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_XIEZI * 12));
						if(Testentry)
							bChange = false;
					}
				}
				if(bChange)
				{
					ui32 effect_displayid = 0;
					ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (index * 12) + 9 ;
					effect_displayid = GetUInt32Value(itemeffect);

					GetItemEffect(GetUInt32Value(visiblebase), Guid, effect_displayid);

					pEquipment->UpDateUNiavobject(visiblebase, GetUInt32Value(visiblebase),index, effect_displayid);
					//pkFittingRoom->UpDateUNiavobject(visiblebase, GetUInt32Value(visiblebase),index, effect_displayid);
					//TeamShow->UpdateObjHead(GetGUID(), visiblebase, GetUInt32Value(visiblebase),index);
				}

				// 装备变化：如果怪物装位置为空，还原为普通装备
				if(GetUInt32Value(visiblebase) == 0 || m_bShowShizhuang == 0)
				{
					bool bHunterEquip = false;
					ui32 SlotIndex = 0;
					if(index == INVTYPE_HOUNTER_TOUBU)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_HEAD;
					}
					else if(index == INVTYPE_HOUNTER_JIANBANG)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_SHOULDERS;
					}
					else if(index == INVTYPE_HOUNTER_SHANGYI)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_CHEST;
					}
					else if(index == INVTYPE_HOUNTER_XIAYI)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_TROUSERS;
					}
					else if(index == INVTYPE_HOUNTER_XIEZI)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_BOOTS;
					}
					if(bHunterEquip)
					{
						ui32 Nvisiblebase = PLAYER_VISIBLE_ITEM_1_0 + (SlotIndex * 12);
						uint32 entry = GetUInt32Value(Nvisiblebase);
						ui32 effect_displayid = 0;

						GetItemEffect(GetUInt32Value(Nvisiblebase), Guid, effect_displayid);

						pEquipment->UpDateUNiavobject(Nvisiblebase, entry, SlotIndex,effect_displayid);
						//pkFittingRoom->UpDateUNiavobject(visiblebase, entry, SlotIndex,effect_displayid);
						//TeamShow->UpdateObjHead(GetGUID(), visiblebase,entry,SlotIndex);


						GuidBase = PLAYER_FIELD_INV_SLOT_HEAD + (SlotIndex * 2);
						Guid = GetUInt64Value(GuidBase);

						ItemMgr->AddItemToEquipmentContainer((ui8)SlotIndex, GetUInt32Value(GuidBase),Guid);
					}
				}
				if (!m_bShowShizhuang)
				{
					pEquipment->UpDateUNiavobject(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_BEIBU * 12), 0, INVTYPE_HOUNTER_BEIBU);
				}
			}

			if (pkEuipWear)
			{
				if (Guid)
				{
					pkEuipWear->UpdataEquipment(Guid);
				}else
				{
					pkEuipWear->RemoveEquipment(index); //
				}

			}

		}
	}


	UInGameBar * pInGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	if (pInGameBar)
	{
		if( mask->GetBit(PLAYER_XP) )
		{
			pInGameBar->SetCurExp(GetUInt32Value(PLAYER_XP));
		}
		if ( mask->GetBit(PLAYER_NEXT_LEVEL_XP) )
		{
			pInGameBar->SetNLExp(GetUInt32Value(PLAYER_NEXT_LEVEL_XP));
		}
	}
	UEquipment * pEquipment = INGAMEGETFRAME(UEquipment,FRAME_IG_PLAYEREQUIPDLG);

	if (pEquipment)
	{
		pEquipment->SetName(GetName());


		//头像
		if(mask->GetBit(UNIT_FIELD_BYTES_0))
		{
			pEquipment->SetHeadImage(GetByte(UNIT_FIELD_BYTES_0,0),GetByte(UNIT_FIELD_BYTES_0,2),GetByte(UNIT_FIELD_BYTES_0,1));
		}
	}
	if (pEquipment)
	{
		//属性抗性
		if (mask->GetBit(UNIT_FIELD_RESISTANCES_01))
		{
			pEquipment->UpdatePlayerRESISTANCES((ui32)(GetFloatValue(UNIT_FIELD_RESISTANCES_01) * 1000), UNIT_FIELD_RESISTANCES_01);
		}
		if (mask->GetBit(UNIT_FIELD_RESISTANCES_02))
		{
			pEquipment->UpdatePlayerRESISTANCES((ui32)(GetFloatValue(UNIT_FIELD_RESISTANCES_02) * 1000), UNIT_FIELD_RESISTANCES_02);
		}
		if (mask->GetBit(UNIT_FIELD_RESISTANCES_03))
		{
			pEquipment->UpdatePlayerRESISTANCES((ui32)(GetFloatValue(UNIT_FIELD_RESISTANCES_03) * 1000), UNIT_FIELD_RESISTANCES_03);
		}
		if (mask->GetBit(UNIT_FIELD_RESISTANCES_04))
		{
			pEquipment->UpdatePlayerRESISTANCES((ui32)(GetFloatValue(UNIT_FIELD_RESISTANCES_04) * 1000), UNIT_FIELD_RESISTANCES_04);
		}
		if (mask->GetBit(UNIT_FIELD_RESISTANCES_05))
		{
			pEquipment->UpdatePlayerRESISTANCES((ui32)(GetFloatValue(UNIT_FIELD_RESISTANCES_05) * 1000), UNIT_FIELD_RESISTANCES_05);
		}
		if (mask->GetBit(UNIT_FIELD_RESISTANCES_06))
		{
			pEquipment->UpdatePlayerRESISTANCES((ui32)(GetFloatValue(UNIT_FIELD_RESISTANCES_06) * 1000), UNIT_FIELD_RESISTANCES_06);
		}
		//基础属性
		if(mask->GetBit(UNIT_FIELD_STAT0))
		{
			pEquipment->UpdatePlayerPropety((int)(GetFloatValue(UNIT_FIELD_STAT0) * 1000),FIELD_ROLE_STRENGTH);
		}
		if(mask->GetBit(UNIT_FIELD_STAT1))
		{
			pEquipment->UpdatePlayerPropety((int)(GetFloatValue(UNIT_FIELD_STAT1) * 1000),FIELD_ROLE_AGILITY);
		}
		if(mask->GetBit(UNIT_FIELD_STAT2))
		{
			pEquipment->UpdatePlayerPropety((int)(GetFloatValue(UNIT_FIELD_STAT2) * 1000),FIELD_ROLE_STAMINA);
		}
		if(mask->GetBit(UNIT_FIELD_STAT3))
		{
			float tem = /*0.04999f +*/ GetFloatValue(UNIT_FIELD_SPELLDAMAGE);
			pEquipment->UpdatePlayerPropety((int)(GetFloatValue(UNIT_FIELD_STAT3) * 1000),FIELD_ROLE_INTELLIGENCE);
			pEquipment->UpdatePlayerPropety((int)(tem * 1000), FIELD_ROLE_SPELLDAMAGE);
		}
		if(mask->GetBit(UNIT_FIELD_STAT4))
		{
			pEquipment->UpdatePlayerPropety((int)(GetFloatValue(UNIT_FIELD_STAT4) * 1000),FIELD_ROLE_SPIRITE);
		}
		if(mask->GetBit(UNIT_FIELD_STAT5))
		{
			pEquipment->UpdatePlayerPropety((int)(GetFloatValue(UNIT_FIELD_STAT5) * 1000),FIELD_ROLE_VITALITY);
		}

		//防御属性
		if(mask->GetBit(UNIT_FIELD_RESISTANCES))
		{
			float amor = /*0.04999f+*/ GetFloatValue(UNIT_FIELD_RESISTANCES);
			pEquipment->UpdatePlayerPropety((ui32)(amor * 1000), FIELD_ROLE_RESISTANCES);
		}
		if(mask->GetBit(PLAYER_DODGE_PERCENTAGE))
		{
			pEquipment->UpdatePlayerPropety((int)(GetFloatValue(PLAYER_DODGE_PERCENTAGE) * 1000),FIELD_ROLE_DODGE_PERCENTAGE);
		}
		if (mask->GetBit(PLAYER_BLOCK_PERCENTAGE))
		{
			pEquipment->UpdatePlayerPropety((int)(GetFloatValue(PLAYER_BLOCK_PERCENTAGE) * 1000) , FIELD_ROLE_BLOCK_PERCENTAGE);
		}


		//近战属性
		if (mask->GetBit(UNIT_FIELD_ATTACK_POWER))
		{
			uint32 power = GetUInt32Value(UNIT_FIELD_ATTACK_POWER);
			pEquipment->UpdatePlayerPropety(power * 1000, FIELD_ROLE_ATTACK_POWER);
		}
		if (mask->GetBit(UNIT_FIELD_MINDAMAGE))
		{
			float damage = /*0.04999f+ */GetFloatValue(UNIT_FIELD_MINDAMAGE);
			pEquipment->UpdatePlayerPropety((ui32)(damage * 1000), FIELD_ROLE_MINDAMAGE);
		}
		if(mask->GetBit(UNIT_FIELD_BASEATTACKTIME))
		{
			uint32 tem = GetUInt32Value(UNIT_FIELD_BASEATTACKTIME);
			pEquipment->UpdatePlayerPropety(tem, FIELD_ROLE_BASEATTACKTIME);
		}
		if (mask->GetBit(PLAYER_CRIT_PERCENTAGE))
		{
			float crit =/* 0.04999f+*/ GetFloatValue(PLAYER_CRIT_PERCENTAGE);
			pEquipment->UpdatePlayerPropety((ui32)(crit * 1000), FIELD_ROLE_CRIT_PERCENTAGE);
		}


		//远程属性
		if(mask->GetBit(UNIT_FIELD_RANGED_ATTACK_POWER))
		{
			uint32 power = GetUInt32Value(UNIT_FIELD_RANGED_ATTACK_POWER);
			pEquipment->UpdatePlayerPropety(power* 1000, FIELD_ROLE_RANGED_ATTACK_POWER);
		}
		if(mask->GetBit(UNIT_FIELD_MINRANGEDDAMAGE))
		{
			float tem = /*0.04999f +*/ GetFloatValue(UNIT_FIELD_MINRANGEDDAMAGE);
			pEquipment->UpdatePlayerPropety((ui32)(tem* 1000),FIELD_ROLE_MINRANGEDDAMAGE);
		}
		if (mask->GetBit(PLAYER_RANGED_CRIT_PERCENTAGE))
		{
			float tem = /*0.04999f +*/ GetFloatValue(PLAYER_RANGED_CRIT_PERCENTAGE);
			pEquipment->UpdatePlayerPropety((ui32)(tem* 1000),FIELD_ROLE_RANGED_CRIT_PERCENTAGE);
		}
		if (mask->GetBit(UNIT_FIELD_RANGEDATTACKTIME))
		{
			uint32 tem = GetUInt32Value(UNIT_FIELD_RANGEDATTACKTIME);
			pEquipment->UpdatePlayerPropety(tem ,FIELD_ROLE_RANGEDATTACKTIME);
		}

		//施法属性
		if (mask->GetBit(UNIT_FIELD_SPELLDAMAGE))
		{
			float tem = /*0.04999f +*/ GetFloatValue(UNIT_FIELD_SPELLDAMAGE);
			pEquipment->UpdatePlayerPropety((ui32)(tem* 1000),FIELD_ROLE_SPELLDAMAGE);
		}
		if (mask->GetBit(PLAYER_SPELL_CRIT_PERCENTAGE1))
		{
			float tem = /*0.04999f +*/ GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1);
			pEquipment->UpdatePlayerPropety((ui32)(tem* 1000),FIELD_ROLE_SPELL_CRIT_PERCENTAGE);
		}

		// 社会属性
		if(mask->GetBit(PLAYER_FIELD_HONOR_CURRENCY))
		{
			ui32 Honor = GetUInt32Value(PLAYER_FIELD_HONOR_CURRENCY);
			if (bFirst)
			{
				pEquipment->UpdatePlayerPropety(Honor, FIELD_ROLE_HONORCURRENT);
				bFirst = false;
			}
			else
			{
				int GainHonor = Honor - m_uiLastHonor;
				if (GainHonor)
				{
					pEquipment->UpdatePlayerPropety(Honor, FIELD_ROLE_HONORCURRENT);
					ChatSystem->HonorGotMsg(GainHonor);
					//BubbleUpEffectNumber(GainHonor, BUBBLE_GAINHONOR);
				}
			}
			m_uiLastHonor = Honor;


			UQuestDlg* pNpcQuestDlg = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
			if(pNpcQuestDlg)
			{
				pNpcQuestDlg->GetQuestList()->UpdataHonor();
				//pNpcQuestDlg->SetTraceQuest(MsgRecv.questid);
			}

		}
		if (mask->GetBit(PLAYER_FIELD_TOTAL_HONOR))
		{
			pEquipment->UpdatePlayerPropety(GetUInt32Value(PLAYER_FIELD_TOTAL_HONOR), FIELD_ROLE_HONORTOTAL);
		}		
		if (mask->GetBit(PLAYER_FIELD_KILL_COUNT))
		{
			pEquipment->UpdatePlayerPropety(GetUInt32Value(PLAYER_FIELD_KILL_COUNT), FIELD_ROLE_KILLCOUNT);
		}		
		if (mask->GetBit(PLAYER_FIELD_BE_KILL_COUNT))
		{
			pEquipment->UpdatePlayerPropety(GetUInt32Value(PLAYER_FIELD_BE_KILL_COUNT), FIELD_ROLE_BEKILLCOUNT);
		}
	}
	if( mask->GetBit(UNIT_FIELD_LEVEL) )
	{
		ui32 uiNewLevel = GetUInt32Value(UNIT_FIELD_LEVEL);
		if (pEquipment)
		{
			pEquipment->SetLevel(uiNewLevel);
		}
		if (uiNewLevel == 3)
		{
			HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_CHAT);
		}
		if (uiNewLevel == 5)
		{
			HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_ENTER_INSTANCE);
		}
		if (uiNewLevel == 10)
		{
			HELPEVENTTRIGGER(TUTORIAL_FLAG_MONTER_CLOTH);
		}
	}

	//微波气功
	if (mask->GetBit(PLAYER_XP_TRIGGER))
	{
		USuperSkill* pkSuperSkill = INGAMEGETFRAME(USuperSkill, FRAME_IG_SUPERSKILL);
		if (pkSuperSkill)
		{
			pkSuperSkill->SetSuperSkillExp(GetUInt32Value(PLAYER_XP_TRIGGER));
		}
	}

	// 元宝
	if( mask->GetBit(PLAYER_FIELD_YUANBAO))
	{
		SetYuanBao(GetUInt32Value(PLAYER_FIELD_YUANBAO));
	}

	//精炼

	if (mask->GetBit(PLAYER_FIELD_JINGLIAN_SLOT_ITEM) && EquRefineMgr)
	{
		//主装备
		EquRefineMgr->UpDataMaterials(0, GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_ITEM), 0);
	}
	if (mask->GetBit(PLAYER_FIELD_JINGLIAN_SLOT_MAINGEM) && EquRefineMgr)
	{
		//精练或者几率宝石
		EquRefineMgr->UpDataMaterials(1, GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_MAINGEM), 0);
	}

	if (mask->GetBit(PLAYER_FIELD_JINGLIAN_SLOT_GEM1) && EquRefineMgr)
	{
		EquRefineMgr->UpDataMaterials(2, GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM1), 0);
	}
	if (mask->GetBit(PLAYER_FIELD_JINGLIAN_SLOT_GEM2) && EquRefineMgr)
	{
		EquRefineMgr->UpDataMaterials(3, GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM2), 0);
	}
	if (mask->GetBit(PLAYER_FIELD_JINGLIAN_SLOT_GEM3) && EquRefineMgr)
	{
		EquRefineMgr->UpDataMaterials(4, GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM3), 0);
	}
	if (mask->GetBit(PLAYER_FIELD_JINGLIAN_SLOT_GEM4)&& EquRefineMgr)
	{
		EquRefineMgr->UpDataMaterials(5, GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM4), 0);
	}

	if (mask->GetBit(PLAYER_CHOSEN_TITLE))
	{
		ui32 curTitleID = GetUInt32Value(PLAYER_CHOSEN_TITLE);
		TitleMgr->ShowCurTitleDis(curTitleID);
	}

	if (mask->GetBit(UNIT_FIELD_HEALTH))
	{
		if (GetMaxHP())
		{
			float hpper = float(GetHP()) / float(GetMaxHP());
			if (hpper <= 0.3f)
			{
				HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_USE_MEDICAL);
			}
		}
	}

	if(mask->GetBit(OBJECT_FIELD_SCALE_X) || mask->GetBit(UNIT_FIELD_MOUNTDISPLAYID) )
	{
		ui32 entry = GetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID);
		if( entry != 0 )
		{
			return;
		}

		NiAVObject* pkHelm = GetSceneNode()->GetObjectByName( "Dummy03" );
		float hight = Eye_OffSet;

		if ( pkHelm )
		{
			hight = (pkHelm->GetWorldTranslate().z - GetPosition().z - 0.2f);
		}

		m_Camera.SetEyehight( hight );

	}

	for (int i = 0 ; i < 12 ; ++i)
	{
		UNPCTrade* pTrade = INGAMEGETFRAME(UNPCTrade, FRAME_IG_NPCTRADEDLG);
		bool bChange = false;
		if (mask->GetBit(PLAYER_FIELD_VENDORBUYBACK_SLOT_1 + i*2))
		{
			m_LocalBuyBack[i] = GetUInt64Value(PLAYER_FIELD_VENDORBUYBACK_SLOT_1 + i*2);
			bChange = true;
		}
		if (mask->GetBit(PLAYER_FIELD_BUYBACK_PRICE_1 + i))
		{
			m_LocalBuyBackPrice[i] = GetUInt32Value(PLAYER_FIELD_BUYBACK_PRICE_1 + i);
			bChange = true;
		}
		if (pTrade && pTrade->GetBuyBackState())
		{
			pTrade->OnSetPageList();
		}
	}

	/*if ( pEquipment )
	{
	CPlayer* pPkPlayer = (CPlayer*)pEquipment->GetUICharacter();

	if ( pPkPlayer )
	{
	NiAVObject* pkHelmUI = pPkPlayer->GetSceneNode()->GetObjectByName( "Helmet_Node" );
	if ( pkHelmUI )
	{
	pkHelmUI->SetAppCulled( !m_bIsShowHelm );
	}
	}
	}*/

}
BOOL CPlayerLocal::IsHook()
{
	if (HookMgr)
	{
		return HookMgr->IsActorRun();
	}
	return FALSE ;
}
void CPlayerLocal::SetYuanBao(ui32 uiYuanBao)
{
	SYState()->UI->GetUItemSystem()->SetYuanBao(uiYuanBao);
}

void CPlayerLocal::OnLevelupStatus(ui32 newLevel)
{
	BOOL NeedShow = TRUE ;

	if (m_uiCurLevel > 1  && newLevel <= m_uiCurLevel)
	{
		NeedShow = FALSE;
	}

	CPlayer::OnLevelupStatus(newLevel);
	UpdateNpcState(0, 0);

	UBShowLevelTips* bShowLevelTips = INGAMEGETFRAME(UBShowLevelTips, FRAME_IG_BSHOWLEVELTIPS);
	UInGameBar* pBar = INGAMEGETFRAME(UInGameBar, FRAME_IG_ACTIONSKILLBAR);
	//if (bShowLevelTips)
	//{
	//	bShowLevelTips->SetCheck(!NeedShow);
	//	if (NeedShow)
	//	{
	//		ULOG("Test show");
	//	}else
	//	{
	//		ULOG("test colse");
	//	}	
	//}

	if (pBar)
	{
		if (NeedShow)
		{
			pBar->UpdateLevelUI(newLevel);
		}

		if (newLevel <= 10)
		{
			//pBar->GlintHelpBtn();
		}
	}
	InstanceSys->OnLevelUpdata(newLevel);
}

void CPlayerLocal::FollowPlayer(ui64 playerID)
{
	m_uiFollowPlayerID = playerID;
}

float CPlayerLocal::GetFollowResponeRange() const
{
	return m_fFollowRadius;
}

ui64 CPlayerLocal::GetFollowPlayerID() const
{
	return m_uiFollowPlayerID;
}

void CPlayerLocal::ActivateMoveTargetPointEffect(const NiPoint3& pos)
{
	CEffectBase* pkEffect = m_pkEffectList[MOVETARGETPOINT];
	if ( pkEffect != NULL ) {
		pkEffect->AttachToScene(pos);
	}
}

void CPlayerLocal::DeactivateMoveTargetPointEffect()
{
	CEffectBase* pkEffect = m_pkEffectList[MOVETARGETPOINT];
	if ( pkEffect != NULL ) {
		pkEffect->DettachFromScene();
	}
}

void CPlayerLocal::PathMoveStart(const NiPoint3& pos, UINT uiFlag)
{
	CCharacter::PathMoveStart(pos, uiFlag);

	if (uiFlag & CMF_SelectWayPoint)
	{
		ActivateMoveTargetPointEffect(pos);
	}
}
void CPlayerLocal::EndMoveToTarget()
{
	if (m_MoveToTarget)
	{
		ui64 guid = SYState()->LocalPlayerInput->GetTargetID();
		CCharacter* pkchar = (CCharacter*)ObjectMgr->GetObject(guid);
		if (pkchar)
		{
			SetDirectionTo(pkchar->GetPosition());
		}
		m_fRange = 0.0f;
		m_MoveToTarget = FALSE ;
	}
}
void CPlayerLocal::CheckMoveTarget(float fTimeDelta)
{
	static NiPoint3 LastDir  = NiPoint3::ZERO;
	static float LastTimeCheck = 0.0f;
	static UINT JumpCount = 0;

	if (m_MoveToTarget)
	{
		LastTimeCheck += fTimeDelta ;
		if (LastTimeCheck >= 1.0f)
		{
			if (LastDir == m_kPathMoveDir)
			{
				OnJumpInputEvent();
				JumpCount++;
			}
			LastTimeCheck = 0.0f;
			LastDir = m_kPathMoveDir;
		}
	}else
	{
		LastDir  = NiPoint3::ZERO;
		LastTimeCheck = 0.0f;
	}

}
void CPlayerLocal::MoveToTargetUpdate(float fTimeDelta)
{
	if (m_MoveToTarget)
	{
		ui64 Target = SYState()->LocalPlayerInput->GetTargetID();
		CCharacter* TargetObj = (CCharacter*)ObjectMgr->GetObject(Target);
		if (TargetObj)
		{
			NiPoint3 kTargetPos, kLocalPos;
			kTargetPos = TargetObj->GetPosition();
			if (TargetObj->isMount())
			{
				if(TargetObj->IsMountPlayer())
				{
					kTargetPos =  TargetObj->GetMountFoot()->GetPosition();
				}
				else if(TargetObj->IsMountCreature())
				{
					kTargetPos =  TargetObj->GetMountCreature()->GetPosition();
				}
			}

			kLocalPos = GetPosition() ;

			NiPoint3 kDir = kLocalPos - kTargetPos;
			float Len = kDir.Length();
			kDir.Unitize();
			if (Len > (m_fRange + 2.4f))
			{
				kTargetPos = kLocalPos - kDir * 2.4f;
			}else
			{
				kTargetPos = kTargetPos + kDir * m_fRange;
			}

			NiPoint3 kOldVec = - m_kPathMoveDir;
			if (kOldVec.Dot(kDir) <= 0.0f)
			{
				PathMoveEnd();
				return ;
			}
			m_MoveDestPos = kTargetPos;
			PathMoveStart(m_MoveDestPos, m_uiPathMoveFlag);
			SetDirectionTo(m_MoveDestPos);
		}else
		{
			PathMoveEnd();
			return ;
		}
	}
}

void CPlayerLocal::PathMoveUpdate(float fTimeDelta)
{
	if( m_bPathMove )
	{
		CheckMoveTarget(fTimeDelta);
		NiPoint3 kNewVec = GetPosition() - m_MoveDestPos;
		kNewVec.z = 0;
		kNewVec.Unitize();

		NiPoint3 kOldVec = -m_kPathMoveDir;
		if (kOldVec.Dot(kNewVec) <= 0.0f)
		{
			MoveToTargetUpdate(fTimeDelta);
			if ( m_PathFinder)
			{
				if ( m_PathFinder->IsPathPointEmpty())
				{

					if (m_pkMapToMapInfo.size() > 1)
					{	
						if (!m_pkBChangeMap )
						{
							PathTranceBegin();
						}
					}else
					{
						if (!m_MoveToTarget)
						{
							PathMoveEnd();
						}
					}
					return;
				}

				m_MoveDestPos = m_PathFinder->GetNextPathPoint();
				PathMoveStart(m_MoveDestPos, m_uiPathMoveFlag);
				SetDirectionTo(m_MoveDestPos);
			}
			else
			{
				NiPoint3 cp = this->GetPosition();
				NiPoint3 s, e;
				float tarz;
				s = NiPoint3(cp.x, cp.y, cp.z + 5.0f);
				e = NiPoint3(cp.x, cp.y, cp.z - 5.0f);
				NiPoint3 extent(0.1f, 0.1f, 0.1f);

				ResultList collList;
				if (CMap::Get()->MultiCollideBox(s, e, extent, collList))
					tarz = s.z + (e.z-s.z)*collList[0].fTime;
				//tarz = s.z * collList[0].fTime + e.z * (1 - collList[0].fTime);
				cp.z = tarz;
				SetPosition(cp);
				if (m_pkMapToMapInfo.size() > 1)
				{
					if (!m_pkBChangeMap )
					{
						PathTranceBegin();
					}
				}else
				{
					if (!m_MoveToTarget)
					{
						PathMoveEnd();
					}
				}
			}
		}
	}
}
void CPlayerLocal::PathMoveEnd()
{
	if (IsPathMove()) {
		DeactivateMoveTargetPointEffect();
		CCharacter::PathMoveEnd();

		StopMove();
	}

	EndMoveToTarget();
	m_pkMapToMapInfo.clear() ;
	m_pkMapToMapNPC = 0;
	m_pkBChangeMap = FALSE;
	m_pkTraceGate = FALSE;
	m_bAutoActionPos = false;
}

void CPlayerLocal::StartForward(bool bAutoRun)
{
	CPlayer::StartForward(bAutoRun);

	m_bAutoRun = bAutoRun;
}

void CPlayerLocal::StartBackward()
{
	CPlayer::StartBackward();
}

void CPlayerLocal::StopVertical()
{
	CCharacter::StopVertical();
}

void CPlayerLocal::StartLeft()
{
	CPlayer::StartLeft();
}

void CPlayerLocal::StartRight()
{
	CPlayer::StartRight();
}

void CPlayerLocal::StopHorizontal()
{
	CCharacter::StopHorizontal();
}

void CPlayerLocal::StartTurnLeft()
{
	CCharacter::StartTurnLeft();
}

void CPlayerLocal::StartTurnRight()
{
	CCharacter::StartTurnRight();
}

void CPlayerLocal::StopTurn()
{
	CCharacter::StopTurn();
}

void CPlayerLocal::SetFacing(float rot)
{
	if (IsSleep())
	{
		return;
	}
	CCharacter::SetFacing(rot);
}

void CPlayerLocal::OnJumpInputEvent()
{	
	if (IsJumping())
	{
		if (GetJumpState() == EJS_LANDING)
		{
			EndJump();
			SetLastMovementFlags(0);
		}
		else
			return;
	}

	SetPhyFly( true );

	if ( IsFlying()  )
	{
		StartFlyUp();

	}
	else if ( CheckInWater() == WL_UNDER_WATER )
	{
		StartSwimUp();
	}
	else
		m_stMoveInfo.flags |= MOVEFLAG_JUMPING;

	if ( !IsFlying() )
	{
		StopFlyUp();
	}
}

/*
bool CPlayerLocal::CanMove()
{
BOOL bIsJumping = IsJumping();

if (!bIsJumping)
return true;

return false;
}
*/

DWORD CPlayerLocal::GetMovementTimeStamp()
{
	return this->GetClientTime();
}



void CPlayerLocal::SendMovementPacket(MoveOP eOP)
{
	/*if ( m_stTelePortPara.bTelePort && SYState()->LocalPlayerInput->IsForceRoot() )
	{
	return;
	}*/

	if (GetUInt32Value(PLAYER_FOOT) != 0 || IsDead())
	{
		return ;
	}

	/// 这里用的是人物的位置 ， 骑宠状态下 传送的时候 骑宠的位置是没更新的。
	NiPoint3 ptCurPos = this->GetPosition();
	if ( IsMountCreature() /*&& IsFlying()*/ )
	{
		if ( this->GetMountCreature() )
		{
			ptCurPos = this->GetMountCreature()->GetPosition();
		}

	}

	//0对应Y轴负方向
	//按顺时针方向角度增大 



	MSG_C2S::stMove_OP msg;
	memset(&msg.move, 0, sizeof(msg.move));

	msg.moveOP = eOP;
	msg.move.flags = m_stMoveInfo.flags;

	// 当前移动数据
	if (m_stMoveInfo.flags & MOVEFLAG_JUMPING)
	{
		NiPoint3 vVel;
		CalcMoveVelocity(vVel);
		float fXYSpeed = vVel.Unitize();

		msg.move.j_cosAngle = vVel.x;
		msg.move.j_sinAngle = vVel.y;
		msg.move.j_xyspeed = fXYSpeed;
	}

	msg.move.x = ptCurPos.x;
	msg.move.y = ptCurPos.y;
	msg.move.z = ptCurPos.z;
	//msg.move.orientation = GetDesiredAngle();
	msg.move.orientation = GetFacingAngle();
	msg.move.mo = GetDesiredAngle();
	/*if ( m_stMoveInfo.flags & MOVEFLAG_MOVE_BACKWARD )
	{
	msg.move.orientation -= NI_PI;
	}*/

	/*if ( m_stMoveInfo.flags & MOVEFLAG_STRAFE_RIGHT )
	{
	msg.move.orientation -= (float)(NI_PI/2.f);
	}

	if ( m_stMoveInfo.flags & MOVEFLAG_STRAFE_LEFT )
	{
	msg.move.orientation += (float)(NI_PI/2.f);
	}*/
	msg.move.time = GetMovementTimeStamp();

	if (g_pkConsole)
		g_pkConsole->Printf(19, "send move flag: %d\n", m_stMoveInfo.flags);

	NetworkMgr->SendPacket(msg);

}

bool CPlayerLocal::HasQuest(uint32 uiQuestId)
{
	for(ui32 i = 0; i < 25; ++i)
	{
		ui32 questslot = PLAYER_QUEST_LOG_1_1+i*MAX_QUEST_OFFSET;

		ui32 questid = GetUInt32Value(questslot+QUEST_ID_OFFSET);
		if (questid == uiQuestId)
		{
			return true;
		}
	}
	return false;
}

bool CPlayerLocal::HasItem(ui32 ItemEntry)
{
	if (m_pkPackageContainer)
	{
		return ( m_pkPackageContainer->GetItemCnt(ItemEntry) > 0);
	}
	return false ;
}
bool CPlayerLocal::HasLearnSkill(ui32 Spellid)
{
	if (Spellid)
	{
		return FindSkill(Spellid);
	}
	return false;
}
void CPlayerLocal::StopAttack()
{
	PacketBuilder->SendAttackStopMsg();
}
BOOL CPlayerLocal::SetMoveToTarget(float fRange, UINT Flag )
{

	ui64 Target = SYState()->LocalPlayerInput->GetTargetID();
	CCharacter* TargetObj = (CCharacter*)ObjectMgr->GetObject(Target);
	if (TargetObj)
	{
		m_MoveToTarget = TRUE ;
		m_fRange = fRange;

		NiPoint3 kTargetPos, kLocalPos;
		kTargetPos = TargetObj->GetPosition();
		if (TargetObj->isMount())
		{
			if(TargetObj->IsMountPlayer())
			{
				kTargetPos =  TargetObj->GetMountFoot()->GetPosition();
			}
			else if(TargetObj->IsMountCreature())
			{
				kTargetPos =  TargetObj->GetMountCreature()->GetPosition();
			}
		}

		kLocalPos = GetPosition() ;

		NiPoint3 kDir = kLocalPos - kTargetPos;
		float Len = kDir.Length();
		kDir.Unitize();
		if (Len > (fRange + 2.4f))
		{
			kTargetPos = kLocalPos - kDir * 2.4f;
		}else
		{
			kTargetPos = kTargetPos + kDir * fRange;
		}
		MoveTo(kTargetPos, Flag);

		return TRUE ;
	}

	return FALSE ;
}
BOOL CPlayerLocal::MoveTo(const NiPoint3& Target, UINT Flag, bool findpath /*= true*/)
{
	return CPlayer::MoveTo(Target, Flag, findpath);
}
void CPlayerLocal::PathTraceGateBegin(ui32 Gateid)
{
	int size = m_pkMapToMapInfo.size();
	if (size)
	{
		/*char debugbuf[_MAX_PATH];
		sprintf(debugbuf, "<br>自动寻路碰到传送门：%u <br>" ,Gateid);
		ChatSystem->AppendChatMsg(CHAT_MSG_PICKITEM,debugbuf);*/
		if (!strcmp(m_pkMapToMapInfo[size - 1].NpcName.c_str(), "TransGate") && Gateid == m_pkMapToMapInfo[size - 1].uiNpcId)
		{
			m_pkTraceGate = TRUE ; //如果当前传送门和我们预计的传送是一样的 那么准备切换地图
			m_pkBChangeMap = TRUE;
			return ;
		}else
		{
			//如果当前传送门不是我们希望切换地图的传送门的话
			//如果切换到了新的地图, 切换后ContinuePathMove()会处理
			//如果还是本地图的话,传送完后调用ContinueTraceGatePathMove(),继续寻找本地图的目标
			m_pkBChangeMap = FALSE ; 
			m_pkTraceGate = TRUE;
		}
	}
}
void CPlayerLocal::SendTranceBegin(ui64 guid)
{
	int size = m_pkMapToMapInfo.size();
	if (size && m_pkMapToMapNPC == guid)
	{
		PacketBuilder->SendNpcGossipSelectOptionMsg(m_pkMapToMapNPC, m_pkMapToMapInfo[size - 1].MapIndex[0].TraceIndex);
		m_pkBChangeMap = TRUE;

	}else
	{
		PathMoveEnd() ;
	}

}
void CPlayerLocal::PathTranceBegin()
{
	int size = m_pkMapToMapInfo.size();
	if (size > 1)
	{

		if (!strcmp(m_pkMapToMapInfo[size - 1].NpcName.c_str(), "TransGate") && !m_pkTraceGate)
		{
			//如果走到了这里本地图都要结束的时候，还没有收到传送门碰撞的消息
			//停止本次寻路操作
			PathMoveEnd();
			return ;
		}

		if (m_pkMapToMapNPC == 0)
		{
			const stdext::hash_set<UpdateObj*>& temp( ObjectMgr->GetObjectByType(TYPEID_UNIT) );

			for( stdext::hash_set<UpdateObj*>::const_iterator it = temp.begin(); it != temp.end(); ++it )
			{
				UpdateObj* p = *it;
				CCreature* pkCreature = static_cast<CCreature*>(p);
				if (pkCreature && pkCreature->GetName().Equals(m_pkMapToMapInfo[size - 1].NpcName.c_str()))
				{
					m_pkMapToMapNPC = pkCreature->GetGUID() ;
					break ;
				}
			}
			if (!m_pkMapToMapNPC)
			{
				PathMoveEnd();
			}else
			{
				PacketBuilder->SendNpcGossipHelloMsg(m_pkMapToMapNPC);
			}
		}	
	}
}
BOOL CPlayerLocal::ContinueTraceGatePathMove()
{
	int size = m_pkMapToMapInfo.size() ;
	if (size )
	{
		if (m_pkTraceGate && !m_pkBChangeMap)
		{
			m_kPathMoveDir = NiPoint3::ZERO ;
			MoveToAStar(m_pkMapToMapInfo[size - 1].NpcPos, 0);
		}
	}
	m_pkTraceGate = FALSE ;
	m_pkBChangeMap = FALSE;
	return TRUE ;
}
BOOL CPlayerLocal::ContinuePathMove()
{
	int size = 0 ;
	size = m_pkMapToMapInfo.size() ;
	if (size > 1)
	{
		if (m_pkMapToMapInfo[size -1].MapIndex[0].MapID != CMap::Get()->GetMapId())
		{
			//如果传送过来发现不是预期目标，那么停止寻路
			PathMoveEnd();
			return FALSE ;
		}
		//传送后是预期地图，把刚刚进行的操作丢弃，进行下一个操作！~~SIZE -- ；
		m_pkMapToMapInfo.pop_back();
		size -= 1 ;
	}

	m_pkMapToMapNPC = 0;
	m_pkTraceGate = FALSE ;

	if (size)
	{
		m_kPathMoveDir = NiPoint3::ZERO ;
		MoveToAStar(m_pkMapToMapInfo[size - 1].NpcPos, 0);
	}else
	{
		PathMoveEnd();
	}

	return TRUE ;
}
BOOL CPlayerLocal::MoveToMapAStar(int MapId, const NiPoint3& Target, UINT Flag)
{
	/*SYSTEMTIME st;
	GetLocalTime( &st );
	char debugbuf[_MAX_PATH];
	sprintf(debugbuf ,"<br>开始寻路时间-----------<br>time:%d:%d:%d: <br>",st.wHour,st.wMinute,st.wSecond);
	ChatSystem->AppendChatMsg(CHAT_MSG_PICKITEM, debugbuf);*/

	if (IsHook())
	{
		ClientSystemNotify(_TRAN("挂机状态无法使用该操作!"));
		return FALSE ;
	}

	int curMapId = CMap::Get()->GetMapId();
	if (curMapId == MapId)
	{
		return MoveToAStar(Target , 0);
	}else
	{
		m_pkMapToMapInfo.clear() ;
		stMapToMapPath Last  ;
		Last.NpcPos = Target; 
		Last.NpcName = _TRAN("目标");

		std::vector<int> delMap;
		delMap.clear() ;
		if (g_pkMapToMapPathInfo->GetMapToMapInfo(curMapId, MapId, m_pkMapToMapInfo,GetRace(), delMap))
		{
			m_pkMapToMapInfo.insert(m_pkMapToMapInfo.begin(),Last);
			int size = m_pkMapToMapInfo.size();
			return MoveToAStar(m_pkMapToMapInfo[size - 1].NpcPos, 0);
		}else
		{
			ClientSystemNotify(_TRAN("无法到达该位置"));
			m_pkMapToMapInfo.clear() ;
		}
	}
	return TRUE ;
}
BOOL CPlayerLocal::MoveToAStar(const NiPoint3& Target, UINT Flag)
{
	if (IsDead())
	{
		return TRUE;
	}

	UMinimapFrame* pkMiniMap = INGAMEGETFRAME(UMinimapFrame,FRAME_IG_MINIMAP);

	m_pkBChangeMap = FALSE ;

	CPathNode* pkPath = CPathNodeManager::Get()->GetNearestPathNode(NiPoint3((float)Target.x, (float)Target.y, (float)Target.z));

	if (pkPath == NULL)
	{
		return TRUE;
	}

	if(!m_PathFinder)
		m_PathFinder = new CPathFinder;	

	bool bFindBest = m_PathFinder->GeneratePathWP((int)GetPosition().x, (int)GetPosition().y, (int)GetPosition().z, (int)pkPath->m_kTranslate.x, (int)pkPath->m_kTranslate.y, (int)pkPath->m_kTranslate.z);

	if (!bFindBest)
	{
		PathMoveEnd();
		return FALSE;
	}
	m_MoveDestPos = m_PathFinder->GetNextPathPoint();

	StartForward();
	PathMoveStart(m_MoveDestPos, Flag);

	if (GetMoveState() != CMS_RUSH)
		MoveTo();

	// Show Texture
	if(m_pkAStarDir == NULL)
	{
		m_pkAStarDir = (CSceneEffect*)EffectMgr->CreateSceneEffect("Effect_arrow.nif", true, true);
		m_pkAStarDir->AttachToSceneObject(GetGUID());
		//GetSceneNode()->AttachChild(m_pkAStarDir->GetAVObject());
	}
	else
	{
		m_pkAStarDir->SetVisible(true);
		//m_pkAStarDir->GetAVObject()->SetAppCulled(false);
	}

	if (pkMiniMap)
	{
		pkMiniMap->SetDrawFindOBJPos(TRUE,Target);
	}
	return TRUE;
}

void CPlayerLocal::MoveTo()
{
	CPlayer::MoveTo();
}

void CPlayerLocal::StopMove()
{
	if (!m_pkBChangeMap)
	{
		if (m_pkTraceGate)
		{
			return ;
		}
		CPlayer::StopMove();		
		// Hide Texture
		if(m_pkAStarDir)
		{
			m_pkAStarDir->SetVisible(false);
		}
	}
}

NiControllerSequence* CPlayerLocal::SetCurAnimation(AnimationID Anim, float Frequence /*= 1.0f*/, BOOL bLoop /*= FALSE*/, 
													BOOL bForceStart /*= FALSE*/, int iPriority /*= 0*/)
{
	return __super::SetCurAnimation(Anim, Frequence, bLoop, bForceStart, iPriority);
}

NiControllerSequence* CPlayerLocal::SetCurAnimation(const char* AnimName, float Frequence /*= 1.0f*/, BOOL bLoop /*= FALSE*/, 
													BOOL bForceStart /*= FALSE*/, int iPriority /*= 0*/)
{
	return __super::SetCurAnimation(AnimName, Frequence, bLoop, bForceStart, iPriority);
}
BOOL CPlayerLocal::IsNeedAddScenOBJ(ui32 ScenceOBJ_Entry, ui32 QuestID, ui32 QuestItemID)
{
	//	任务日志
	for(ui32 i = 0; i < 25; ++i)
	{
		ui32 questslot = PLAYER_QUEST_LOG_1_1 + i * MAX_QUEST_OFFSET + QUEST_ID_OFFSET;
		ui32 cntslot = PLAYER_QUEST_LOG_1_1 + i * MAX_QUEST_OFFSET + QUEST_COUNTS_OFFSET;

		ui32 questid = GetUInt32Value(questslot+QUEST_ID_OFFSET);
		CQuestDB::stQuest* stQuestinfo = g_pkQusetInfo->GetQuestInfo(questid);
		if (!stQuestinfo)
		{
			continue ;
		}
		if (questid == QuestID)
		{
			std::vector<ui32> vCreatureCount;
			vCreatureCount.clear();
			for( ui32 creature_idx = 0; creature_idx < 4; creature_idx++ )
			{
				ui32 creature_count =  0 ;
				creature_count = GetByteValue(cntslot,creature_idx);
				vCreatureCount.push_back(creature_count);	
			}

			for (int ui =0; ui < 4; ui++)
			{
				if (stQuestinfo->uiReqItemId[ui] && (QuestItemID == stQuestinfo->uiReqItemId[ui]))
				{
					if (vCreatureCount[ui] == stQuestinfo->uiReqItemCount[ui])
					{
						return FALSE ;
					}else
					{
						return TRUE ;
					}
				}
			}

			BOOL bKillItem = TRUE ;
			for (int j =0 ; j < 4; j++)
			{
				if (stQuestinfo->uiReqKillMobOrGOId[j])
				{
					bKillItem = FALSE ;
					if (ScenceOBJ_Entry == stQuestinfo->uiReqKillMobOrGOId[j])
					{
						if (vCreatureCount[j] == stQuestinfo->uiReqKillMobOrGOCount[j])
						{
							return FALSE ;
						}else
						{
							return TRUE ;
						}
					}
				}
			}
			return bKillItem ;
		}
	}

	return FALSE ;
}


//是否可以看见对方 
BOOL CPlayerLocal::CanLookTarget( CGameObject* pTargetObj )
{
	return TRUE;
	//NiNode* pNode = (NiNode*)pTargetObj->GetSceneNode()->GetObjectByName( "Effect_Chest" );
	//if ( !pNode )
	//	return TRUE;

	//NiPoint3 kTargetHeadPos = pNode->GetWorldTranslate();

	//NiNode* pMyHead = (NiNode*)GetSceneNode()->GetObjectByName( "Effect_Head01" );
	//if ( !pMyHead )
	//	return TRUE;

	//NiPoint3 kMyHeadPos = pMyHead->GetWorldTranslate();

	//NiPoint3 RayStart, RayDir;
	//RayStart = kMyHeadPos;
	//RayDir = kTargetHeadPos - kMyHeadPos;
	//RayDir.Unitize();

	//CGameObject* pkTargetObject = NULL;
	//NiPoint3 ptPickPos;
	//BOOL bPickedObject = FALSE;
	//pkTargetObject = ObjectMgr->PickObject(RayStart, RayDir, ptPickPos);

	//BOOL bRet = FALSE;
	//if(pkTargetObject && pkTargetObject == pTargetObj )
	//{
	//	NiPoint3 ptTarget;
	//	if( SceneMgr->FindMoveTarget(RayStart, RayDir, &ptTarget) )
	//	{
	//		if((ptTarget - RayStart).Length() >= (pkTargetObject->GetPosition() - RayStart).Length())
	//		{
	//			//有阻挡
	//			return TRUE;
	//		}
	//	}
	//}

	//return FALSE;
}

void CPlayerLocal::UpdateScenOBJState()
{	
	const stdext::hash_set<UpdateObj*>& temp( ObjectMgr->GetObjectByType(TYPEID_GAMEOBJECT) );

	for( stdext::hash_set<UpdateObj*>::const_iterator it = temp.begin(); it != temp.end(); ++it )
	{
		UpdateObj* p = *it;
		CSceneGameObj* pkSceneGameOBJ = static_cast<CSceneGameObj*>(p);

		if(pkSceneGameOBJ == NULL)
			continue;

		if (pkSceneGameOBJ->IsGatherObj())
		{
			if (pkSceneGameOBJ->CanSeeCollection())
			{
				pkSceneGameOBJ->AddEffect();
			}else
			{
				pkSceneGameOBJ->DelEffect();
			}	
		}
	}
}
void CPlayerLocal::UpdateNpcState(unsigned int uiQuestId, unsigned int uiQuestState)
{
	const stdext::hash_set<UpdateObj*>& temp( ObjectMgr->GetObjectByType(TYPEID_UNIT) );

	for( stdext::hash_set<UpdateObj*>::const_iterator it = temp.begin(); it != temp.end(); ++it )
	{
		UpdateObj* p = *it;
		CCreature* pkCreature = static_cast<CCreature*>(p);

		if(pkCreature == NULL)
			continue;

		if(pkCreature->HasFlag(UNIT_FIELD_FLAGS, UNIT_NPC_FLAG_QUESTGIVER))
			pkCreature->QueryQuestStatus();
	}	
}

void CPlayerLocal::ResetMovementInfo()
{
	memset(&m_stMoveInfo, 0, sizeof(m_stMoveInfo));
}

IStateProxy* CPlayerLocal::GetStateInstance(State eState)
{
	return GetStateProxy(eState);
}

IStateProxy* CPlayerLocal::GetStateProxy(State StateID)
{
	IStateProxy* pkStateProxy = NULL;
	if (StateID == STATE_MOVE)
		pkStateProxy = StateMoveLP::Instance();
	else if(StateID == STATE_MOUNT_MOVE)
		pkStateProxy = StateMountMoveLP::Instance();
	else
		pkStateProxy = CPlayer::GetStateProxy(StateID);

	return pkStateProxy;
}

//IStateProxy* CPlayerLocal::GetCurStateProxy()
//{
//	NIASSERT( GetCurState() < STATE_MAX);
//	static BOOL ClassStateInited = FALSE;
//	if (!ClassStateInited)
//	{
//		for (UINT i = STATE_UNKNOWN ; i < STATE_MAX; i++)
//		{
//			sm_StateProxys[i] =  GetStateProxy((State)i);
//		}
//
//		ClassStateInited = TRUE;
//	}
//	return sm_StateProxys[GetCurState()];
//}

void CPlayerLocal::OnDeath(bool bDeadNow /* = true */)
{
	CPlayer::OnDeath(bDeadNow);

	UDeathMsgBox* urmsgb = INGAMEGETFRAME(UDeathMsgBox, FRAME_IG_DEATHMSGBOX);
	if (urmsgb)
	{
		urmsgb->SetLocalDeath();
	}

	if (IsHook())
	{
		HookMgr->HookRelive();
	}

	if ( !ClientState->IsBloomEnabled() )
		EffectMgr->CreatePostEffect();

	EffectMgr->DeadEffect(bDeadNow);
	//MSG_C2S::stReliveReq Msg;
	//Msg.type = MSG_C2S::stReliveReq::Relive_Home;
	//NetworkMgr->SendPacket(Msg);
}

void CPlayerLocal::OnAlive()
{
	CPlayer::OnAlive();
	UDeathMsgBox* udmb = INGAMEGETFRAME(UDeathMsgBox, FRAME_IG_DEATHMSGBOX);
	if (udmb)
	{
		udmb->SetLocalRebrith();
	}

	if (IsHook())
	{
		HookMgr->ResetHook();

	}

	EffectMgr->DeadEffect(false);

	if ( !ClientState->IsBloomEnabled() )
		EffectMgr->CleanupPostEffect();
	
}

void CPlayerLocal::BubbleUpEffectNumber(int Number,  int affectType,ui32 subType, const WCHAR* extraOutput /*= NULL*/ , ui32 SubType2)
{
	static NiColorA sBubColors[BUBBLE_MAX] = {
		NiColorA(1.0f, 0.0f, 0.0f, 1.0f),
		NiColorA(1.0f, 0.0f, 0.0f, 1.0f),
		NiColorA(0.0f, 1.0f, 0.0f, 1.0f),
		NiColorA(0.0f, 0.0f, 1.0f, 1.0f),
		NiColorA(1.0f, 1.0f, 0.5f, 1.0f),
		NiColorA(0.0f, 1.0f, 0.0f, 1.0f),
		NiColorA(1.0f, 1.0f, 0.0f, 1.0f)
	};
	affectType = affectType % BUBBLE_MAX;

	EffectMgr->BubbleUpEffectNumber(m_spAttackEffect, affectType,subType, Number, 
		sBubColors[affectType], 1.5f, extraOutput,SubType2);
}
CUstate CPlayerLocal::GetCUState()
{
	CPlayerLocal* Playerlocal = ObjectMgr->GetLocalPlayer();
	if (Playerlocal)
	{
		return Playerlocal->m_CUState;
	}

	return cus_error;
}
bool CPlayerLocal::GetPathPoint(std::deque<NiPoint3>& PathPoint)
{
	if (m_PathFinder)
	{
		PathPoint.clear();
		PathPoint.push_back(m_MoveDestPos);
		return m_PathFinder->GetPathPoint(PathPoint);
	}
	return false;
}

bool CPlayerLocal::GetCanFinishQuestData(uint32 MapID, vector<NiPoint3>& PosVec)
{
	UQuestDlg * pUQuestLog = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
	if (pUQuestLog)
	{
		return pUQuestLog->GetQuestList()->GetCanFinishQuestData(MapID,PosVec);
	}

	return false;
}
BOOL CPlayerLocal::AddLearnNewSkillEffect()
{
	const INT LoopCount = 2;
	CSceneEffect* pkLevelEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect("EA0028.nif",true, true);
	if (pkLevelEffect)
	{	
		NIASSERT(pkLevelEffect);
		pkLevelEffect->AttachToSceneObject(GetGUID());
		pkLevelEffect->SetMaxLifeByEffectLife(LoopCount);
		SYState()->IAudio->PlaySound("Sound/items/learnskill.wav", NiPoint3::ZERO, 2.0f, 1 );

		return TRUE;
	}

	return FALSE;
}
BOOL CPlayerLocal::CanSendTradeRequest(ui64 guid)
{
	CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(guid);
	if (CPlayerLocal::GetCUState() == cus_Trade)
	{
		EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("你已经在交易，无法在发送请求！"));
		return FALSE ;
	}
	if (pkPlayer && !pkPlayer->IsLocalPlayer())
	{
		NiPoint3 pkLocalPos = this->GetPosition();
		NiPoint3 pkPlayerPos = pkPlayer->GetPosition();

		float Distance = (pkLocalPos - pkPlayerPos).Length();
		if (Distance < 5.0f)
		{
			return TRUE;
		}else
		{
			EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("距离太远，无法发送交易请求！"));
			return FALSE ;
		}
	}
	EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("没有找到该玩家！"));
	return FALSE;
}

void CPlayerLocal::SetCUState(CUstate pState , BOOL bChange)
{
	CPlayerLocal* Playerlocal = ObjectMgr->GetLocalPlayer();

	if (!Playerlocal)
	{
		return ;
	}

	CUstate OldState  = Playerlocal->m_CUState ;
	//如果是相同状态。那么返回。
	if (OldState == pState && !bChange)
	{
		return ;
	}

	UInGame::SetUIState(OldState,pState);
	/*if (OldState == cus_Pick)
	{
	Playerlocal->SetIdleAnimation(CAG_IDLE);
	}
	if (pState == cus_Pick)
	{ 
	Playerlocal->SetCurAnimation("Action_400004210", 1.0f,1 ,0,0);
	}*/

	if (Playerlocal)
	{
		Playerlocal->m_CUState = pState;
	}
}

void CPlayerLocal::SetPosition(const NiPoint3& kPos)
{
	CMap* pkMap = CMap::Get();
	if(kPos != m_kPosition && pkMap)
	{
		NiTPrimitiveArray<CModelInstance*> kModelList;
		NiPoint3 kExtent(EXTENT + NiPoint3(0.1f, 0.1f, 0.1f));
		CBox kBox;
		kBox.m_kMin = NiPoint3(kPos.x - kExtent.x, kPos.y - kExtent.y, kPos.z);
		kBox.m_kMax = NiPoint3(kPos.x + kExtent.x, kPos.y + kExtent.y, kPos.z + kExtent.z*2.f);
		pkMap->BoxCheck(kBox, kModelList);
		for(unsigned int ui = 0; ui < kModelList.GetSize(); ++ui)
		{
			OnContact(kModelList.GetAt(ui));
		}
	}

	CPlayer::SetPosition(kPos);
}

void CPlayerLocal::OnContact(CModelInstance* pkInstance)
{
	NIASSERT(pkInstance);
	switch(pkInstance->GetType())
	{
	case CModelInstance::ET_TRANSPORT:
		OnContactTransport(pkInstance);
		break;
	}
}

void CPlayerLocal::OnContactTransport(CModelInstance* pkInstance)
{
	NIASSERT(pkInstance && pkInstance->GetType() == CModelInstance::ET_TRANSPORT);
	CMap* pkMap = CMap::Get();
	if(pkMap && m_fLastTransportTime + 10.f < pkMap->GetTime()) //发送间隔至少为10秒
	{
		m_fLastTransportTime = pkMap->GetTime();
		if (!m_pkTraceGate)
		{
			PacketBuilder->SendAreaTrigger(pkInstance->GetTransportID());
			PathTraceGateBegin(pkInstance->GetTransportID());
		}
	}
}

void CPlayerLocal::OnMoveToNewWorld()
{
	m_fLastTransportTime = -TRANSPORT_DELAY_TIME;
}

NiPoint3 CPlayerLocal::GetLocalPlayerPos()
{
	if(IsMountPlayer())
	{
		return m_pkMountFoot->GetSceneNode()->GetTranslate();
	}
	else if(IsMountCreature())
	{
		return m_pkMountCreature->GetSceneNode()->GetTranslate();
	}

	return GetSceneNode()->GetTranslate();
}

void CPlayerLocal::OnEquipmentChanged()
{
	CPlayer::OnEquipmentChanged();
}


void CPlayerLocal::OnFormModelChanged()
{
	if(!IsActorLoaded())
		return;

	CPlayer::OnFormModelChanged();

	//NiAVObject* pkHelm = GetSceneNode()->GetObjectByName( "Effect_Chest" );
	//float hight = Eye_OffSet;

	//if ( pkHelm )
	//{
	//	hight = (pkHelm->GetWorldTranslate().z - GetPosition().z - 0.2f);
	//}

	//m_Camera.SetEyehight( hight );

}

bool CPlayerLocal::GetSkillList(vector<uint32>& vlist)
{
	vlist = m_SkillList;
	return true;
}

void CPlayerLocal::PushSkillList(vector<uint32>& vlist)
{
	m_SkillList = vlist;
}

void CPlayerLocal::PushSkill(uint32 spellID)
{
	m_SkillList.push_back(spellID);
}

void CPlayerLocal::PopSkill(uint32 spellID)
{
	vector<uint32>::iterator it = m_SkillList.begin();
	while(it != m_SkillList.end())
	{
		if (*it == spellID)
		{
			it = m_SkillList.erase(it);
			return;
		}
		++it;
	}
}

bool CPlayerLocal::FindSkill(uint32 spellID)
{
	vector<uint32>::iterator it = m_SkillList.begin();
	while(it != m_SkillList.end())
	{
		if (*it == spellID)
		{
			return true;
		}
		++it;
	}
	return false;
}

void CPlayerLocal::UpdataSpellModifier(int32 v,  uint8 x, uint8 type, bool pct)
{
	if (pct)
	{
		SpellAddPctModifier(v, x, type);
	}
	else
	{
		SpellAddFlatModifier(v, x, type);
	}
}

void CPlayerLocal::SpellAddFlatModifier(int32 v, uint8 x, uint8 type)
{
	switch(type)
	{
	case SMT_DAMAGE_DONE:
		SetSpellModifier(&SM_FDamageBonus, v, x, type);
		break;

	case SMT_DURATION:
		SetSpellModifier(&SM_FDur, v, x, type);
		break;

	case SMT_THREAT_REDUCED:
		SetSpellModifier(&SM_FThreatReduce, v, x, type);
		break;

	case SMT_RANGE:
		SetSpellModifier(&SM_FRange, v, x, type);
		break;

	case SMT_RADIUS:
		SetSpellModifier(&SM_FRadius, v, x, type);
		break;

	case SMT_CRITICAL:
		SetSpellModifier(&SM_CriticalChance, v, x, type);
		break;

	//case SMT_SPELL_VALUE:
	//	SetSpellModifier(&SM_FSPELL_VALUE, v, x, type);
	//	break;

	case SMT_CAST_TIME:
		SetSpellModifier(&SM_FCastTime, v, x, type);
		break;

	case SMT_COOLDOWN_DECREASE:
		SetSpellModifier(&SM_FCooldownTime, v, x, type);
		break;

	case SMT_COST:
		SetSpellModifier(&SM_FCost, v, x, type);
		break;

	case SMT_HITCHANCE:
		SetSpellModifier(&SM_FHitchance, v, x, type);
		break;

	case SMT_ADDITIONAL_TARGET:
		SetSpellModifier(&SM_FAdditionalTargets, v, x, type);
		break;

	case SMT_TRIGGER:
		SetSpellModifier(&SM_FChanceOfSuccess, v, x, type);
		break;

	case SMT_SPELL_VALUE_PCT:
		SetSpellModifier(&SM_FDOT, v, x, type);
		break;

	case SMT_PENALTY:
		SetSpellModifier(&SM_PPenalty, v, x, type);
		break;

	case SMT_EFFECT:
	case SMT_EFFECT_BONUS:
		SetSpellModifier(&SM_FEffectBonus, v, x, type);
		break;

	case SMT_RESIST_DISPEL:
		SetSpellModifier(&SM_FRezist_dispell, v, x, type);
		break;

	case SMT_SPELL_VALUE0:
		SetSpellModifier(&SM_FSPELL_VALUE0, v, x, type);
		break;

	case SMT_SPELL_VALUE1:
		SetSpellModifier(&SM_FSPELL_VALUE1, v, x, type);
		break;

	case SMT_SPELL_VALUE2:
		SetSpellModifier(&SM_FSPELL_VALUE2, v, x, type);
		break;

	default:
		break;
	}
}

void CPlayerLocal::SpellAddPctModifier(int32 v, uint8 x, uint8 type)
{
	switch(type)
	{
	case SMT_DAMAGE_DONE:
		SetSpellModifier(&SM_PDamageBonus, v, x, type);
		break;

	case SMT_DURATION:
		SetSpellModifier(&SM_PDur, v, x, type);
		break;

	case SMT_THREAT_REDUCED:
		SetSpellModifier(&SM_PThreatReduce, v, x, type);
		break;

	case SMT_ATTACK_POWER_AND_DMG_BONUS:
		{
			SetSpellModifier(&SM_PDamageBonus, v, x, type);
			SetSpellModifier(&SM_PAPBonus, v, x, type);
		}
		break;

	case SMT_RANGE:
		SetSpellModifier(&SM_PRange, v, x, type);
		break;

	case SMT_RADIUS:
		SetSpellModifier(&SM_PRadius, v, x, type);
		break;

	case SMT_CRITICAL:
		SetSpellModifier(&SM_CriticalChance, v, x, type);
		break;

	//case SMT_SPELL_VALUE:
	//	SetSpellModifier(&SM_PSPELL_VALUE, v, x, type);
	//	break;

	case SMT_NONINTERRUPT:
		SetSpellModifier(&SM_PNonInterrupt, v, x, type);
		break;

	case SMT_CAST_TIME:
		SetSpellModifier(&SM_PCastTime, v, x, type);
		break;

	case SMT_COOLDOWN_DECREASE:
		SetSpellModifier(&SM_PCooldownTime, v, x, type);
		break;

	case SMT_COST:
		SetSpellModifier(&SM_PCost, v, x, type);
		break;

	case SMT_CRITICAL_DAMAGE:
		SetSpellModifier(&SM_PCriticalDamage, v, x, type);
		break;

	case SMT_ADDITIONAL_TARGET:
		SetSpellModifier(&SM_FAdditionalTargets, v, x, type);
		break;

	case SMT_EFFECT:
	case SMT_EFFECT_BONUS:
		SetSpellModifier(&SM_PEffectBonus, v, x, type);
		break;

	case SMT_PENALTY:
		SetSpellModifier(&SM_PPenalty, v, x, type);
		break;

	case SMT_RESIST_DISPEL:
		SetSpellModifier(&SM_PRezist_dispell, v, x, type);
		break;

	case SMT_JUMP_REDUCE:
		SetSpellModifier(&SM_PJumpReduce, v, x, type);
		break;

	case SMT_SPELL_VALUE_PCT:
		SetSpellModifier(&SM_PDOT, v, x, type);
		break;

	case SMT_SPELL_VALUE0:
		SetSpellModifier(&SM_PSPELL_VALUE0, v, x, type);
		break;

	case SMT_SPELL_VALUE1:
		SetSpellModifier(&SM_PSPELL_VALUE1, v, x, type);
		break;

	case SMT_SPELL_VALUE2:
		SetSpellModifier(&SM_PSPELL_VALUE2, v, x, type);
		break;

	default:
		break;
	}
}

void CPlayerLocal::SetSpellModifier(int32** m, int32 v, uint8 x, uint8 type)
{
	if( *m == 0 )
	{
		*m = new int32[SPELL_GROUPS];
		memset(*m, 0, sizeof(int32) * SPELL_GROUPS);
	}
	(*m)[x] = v;
}
void CPlayerLocal::SM_FF_Value(uint8 type, float* v, uint64 group)
{
	switch(type)
	{
	case SMT_DAMAGE_DONE:					SM_FFValue(SM_FDamageBonus, v, group);break;
	case SMT_DURATION:							SM_FFValue(SM_FDur, v, group);break;
	case SMT_THREAT_REDUCED:				SM_FFValue(SM_FThreatReduce, v, group);break;
	case SMT_RANGE:									SM_FFValue(SM_FRange, v, group);break;
	case SMT_RADIUS:								SM_FFValue(SM_FRadius, v, group);	break;
	case SMT_CRITICAL:								SM_FFValue(SM_CriticalChance, v, group);break;
	//case SMT_SPELL_VALUE:						SM_FFValue(SM_FSPELL_VALUE, v, group);break;
	case SMT_CAST_TIME:							SM_FFValue(SM_FCastTime, v, group);	break;
	case SMT_COOLDOWN_DECREASE:	SM_FFValue(SM_FCooldownTime, v, group);break;
	case SMT_COST:									SM_FFValue(SM_FCost, v, group);break;
	case SMT_HITCHANCE:						SM_FFValue(SM_FHitchance, v, group);break;
	case SMT_ADDITIONAL_TARGET:		SM_FFValue(SM_FAdditionalTargets, v, group);break;
	case SMT_TRIGGER:								SM_FFValue(SM_FChanceOfSuccess, v, group);break;
	case SMT_SPELL_VALUE_PCT:				SM_FFValue(SM_FDOT, v, group);break;
	case SMT_PENALTY:								SM_FFValue(SM_PPenalty, v, group);	break;
	case SMT_EFFECT:
	case SMT_EFFECT_BONUS:					SM_FFValue(SM_FEffectBonus, v, group);break;
	case SMT_RESIST_DISPEL:					SM_FFValue(SM_FRezist_dispell, v, group);break;
	case SMT_SPELL_VALUE0:					SM_FFValue(SM_FSPELL_VALUE0, v, group);break;
	case SMT_SPELL_VALUE1:					SM_FFValue(SM_FSPELL_VALUE1, v, group);break;
	case SMT_SPELL_VALUE2:					SM_FFValue(SM_FSPELL_VALUE2, v, group);break;
	default:
		break;
	}
}

void CPlayerLocal::SM_FI_Value(uint8 type, int32* v, uint64 group)
{
	switch(type)
	{
	case SMT_DAMAGE_DONE:					SM_FIValue(SM_FDamageBonus, v, group);break;
	case SMT_DURATION:							SM_FIValue(SM_FDur, v, group);break;
	case SMT_THREAT_REDUCED:				SM_FIValue(SM_FThreatReduce, v, group);break;
	case SMT_RANGE:									SM_FIValue(SM_FRange, v, group);break;
	case SMT_RADIUS:								SM_FIValue(SM_FRadius, v, group);	break;
	case SMT_CRITICAL:								SM_FIValue(SM_CriticalChance, v, group);break;
	//case SMT_SPELL_VALUE:						SM_FIValue(SM_FSPELL_VALUE, v, group);break;
	case SMT_CAST_TIME:							SM_FIValue(SM_FCastTime, v, group);	break;
	case SMT_COOLDOWN_DECREASE:	SM_FIValue(SM_FCooldownTime, v, group);break;
	case SMT_COST:									SM_FIValue(SM_FCost, v, group);break;
	case SMT_HITCHANCE:						SM_FIValue(SM_FHitchance, v, group);break;
	case SMT_ADDITIONAL_TARGET:		SM_FIValue(SM_FAdditionalTargets, v, group);break;
	case SMT_TRIGGER:								SM_FIValue(SM_FChanceOfSuccess, v, group);break;
	case SMT_SPELL_VALUE_PCT:				SM_FIValue(SM_FDOT, v, group);break;
	case SMT_PENALTY:								SM_FIValue(SM_PPenalty, v, group);	break;
	case SMT_EFFECT:
	case SMT_EFFECT_BONUS:					SM_FIValue(SM_FEffectBonus, v, group);break;
	case SMT_RESIST_DISPEL:					SM_FIValue(SM_FRezist_dispell, v, group);break;
	case SMT_SPELL_VALUE0:					SM_FIValue(SM_FSPELL_VALUE0, v, group);break;
	case SMT_SPELL_VALUE1:					SM_FIValue(SM_FSPELL_VALUE1, v, group);break;
	case SMT_SPELL_VALUE2:					SM_FIValue(SM_FSPELL_VALUE2, v, group);break;
	default:
		break;
	}
}

void CPlayerLocal::SM_PF_Value(uint8 type, float* v, uint64 group)
{
	switch(type)
	{
	case SMT_DAMAGE_DONE:					SM_PFValue(SM_PDamageBonus, v, group);	break;
	case SMT_DURATION:							SM_PFValue(SM_PDur, v, group);break;
	case SMT_THREAT_REDUCED:				SM_PFValue(SM_PThreatReduce, v, group);	break;
	case SMT_RANGE:									SM_PFValue(SM_PRange, v, group);break;
	case SMT_RADIUS:								SM_PFValue(SM_PRadius, v, group);break;
	case SMT_CRITICAL:								SM_PFValue(SM_CriticalChance, v, group);break;
	//case SMT_SPELL_VALUE:						SM_PFValue(SM_PSPELL_VALUE, v, group);break;
	case SMT_NONINTERRUPT:					SM_PFValue(SM_PNonInterrupt, v, group);break;
	case SMT_CAST_TIME:							SM_PFValue(SM_PCastTime, v, group);break;
	case SMT_COOLDOWN_DECREASE:	SM_PFValue(SM_PCooldownTime, v, group);break;
	case SMT_COST:									SM_PFValue(SM_PCost, v, group);break;
	case SMT_CRITICAL_DAMAGE:			SM_PFValue(SM_PCriticalDamage, v, group);break;
	case SMT_ADDITIONAL_TARGET:		SM_PFValue(SM_FAdditionalTargets, v, group);break;
	case SMT_EFFECT:
	case SMT_EFFECT_BONUS:					SM_PFValue(SM_PEffectBonus, v, group);break;
	case SMT_PENALTY:								SM_PFValue(SM_PPenalty, v, group);break;
	case SMT_RESIST_DISPEL:					SM_PFValue(SM_PRezist_dispell, v, group);break;
	case SMT_JUMP_REDUCE:					SM_PFValue(SM_PJumpReduce, v, group);break;
	case SMT_SPELL_VALUE_PCT:				SM_PFValue(SM_PDOT, v, group);break;
	case SMT_SPELL_VALUE0:					SM_PFValue(SM_PSPELL_VALUE0, v, group);break;
	case SMT_SPELL_VALUE1:					SM_PFValue(SM_PSPELL_VALUE1, v, group);break;
	case SMT_SPELL_VALUE2:					SM_PFValue(SM_PSPELL_VALUE2, v, group);break;
	case SMT_ATTACK_POWER_AND_DMG_BONUS:
		{
			SM_PFValue(SM_PDamageBonus, v, group);
			SM_PFValue(SM_PAPBonus, v, group);
		}
		break;
	default:
		break;
	}
}

void CPlayerLocal::SM_PI_Value(uint8 type, int32* v, uint64 group)
{
	switch(type)
	{
	case SMT_DAMAGE_DONE:					SM_PIValue(SM_PDamageBonus, v, group);	break;
	case SMT_DURATION:							SM_PIValue(SM_PDur, v, group);break;
	case SMT_THREAT_REDUCED:				SM_PIValue(SM_PThreatReduce, v, group);	break;
	case SMT_RANGE:									SM_PIValue(SM_PRange, v, group);break;
	case SMT_RADIUS:								SM_PIValue(SM_PRadius, v, group);break;
	case SMT_CRITICAL:								SM_PIValue(SM_CriticalChance, v, group);break;
	//case SMT_SPELL_VALUE:						SM_PIValue(SM_PSPELL_VALUE, v, group);break;
	case SMT_NONINTERRUPT:					SM_PIValue(SM_PNonInterrupt, v, group);break;
	case SMT_CAST_TIME:							SM_PIValue(SM_PCastTime, v, group);break;
	case SMT_COOLDOWN_DECREASE:	SM_PIValue(SM_PCooldownTime, v, group);break;
	case SMT_COST:									SM_PIValue(SM_PCost, v, group);break;
	case SMT_CRITICAL_DAMAGE:			SM_PIValue(SM_PCriticalDamage, v, group);break;
	case SMT_ADDITIONAL_TARGET:		SM_PIValue(SM_FAdditionalTargets, v, group);break;
	case SMT_EFFECT:
	case SMT_EFFECT_BONUS:					SM_PIValue(SM_PEffectBonus, v, group);break;
	case SMT_PENALTY:								SM_PIValue(SM_PPenalty, v, group);break;
	case SMT_RESIST_DISPEL:					SM_PIValue(SM_PRezist_dispell, v, group);break;
	case SMT_JUMP_REDUCE:					SM_PIValue(SM_PJumpReduce, v, group);break;
	case SMT_SPELL_VALUE_PCT:				SM_PIValue(SM_PDOT, v, group);break;
	case SMT_SPELL_VALUE0:					SM_PIValue(SM_PSPELL_VALUE0, v, group);break;
	case SMT_SPELL_VALUE1:					SM_PIValue(SM_PSPELL_VALUE1, v, group);break;
	case SMT_SPELL_VALUE2:					SM_PIValue(SM_PSPELL_VALUE2, v, group);break;
	case SMT_ATTACK_POWER_AND_DMG_BONUS:
		{
			SM_PIValue(SM_PDamageBonus, v, group);
			SM_PIValue(SM_PAPBonus, v, group);
		}
		break;
	default:
		break;
	}
}