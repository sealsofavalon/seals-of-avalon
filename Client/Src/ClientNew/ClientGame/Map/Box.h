
#pragma once

#define FLOAT_MIN (1.175494351e-38F)
#define FLOAT_MAX (3.402823466e+38F)

class CBox
{
public:
   NiPoint3 m_kMin;
   NiPoint3 m_kMax;

public:
	/*
   void Reset();
   /// Perform an intersection operation with another box
   /// and store the results in this box.
   void Intersect(const CBox& kBox);
   void Intersect(const NiPoint3& kPoint);
   void Intersect( float x, float y, float z );
	*/

   inline void Reset()
   {
	   m_kMax.x = -FLOAT_MAX;
	   m_kMax.y = -FLOAT_MAX;
	   m_kMax.z = -FLOAT_MAX;

	   m_kMin.x = FLOAT_MAX;
	   m_kMin.y = FLOAT_MAX;
	   m_kMin.z = FLOAT_MAX;
   }

   inline void Intersect(const CBox& kBox)
   {
	   m_kMin.x = NiMin(m_kMin.x, kBox.m_kMin.x);
	   m_kMin.y = NiMin(m_kMin.y, kBox.m_kMin.y);
	   m_kMin.z = NiMin(m_kMin.z, kBox.m_kMin.z);

	   m_kMax.x = NiMax(m_kMax.x, kBox.m_kMax.x);
	   m_kMax.y = NiMax(m_kMax.y, kBox.m_kMax.y);
	   m_kMax.z = NiMax(m_kMax.z, kBox.m_kMax.z);
   }

   inline void Intersect(const NiPoint3& kPoint)
   {
	   m_kMin.x = NiMin(m_kMin.x, kPoint.x);
	   m_kMin.y = NiMin(m_kMin.y, kPoint.y);
	   m_kMin.z = NiMin(m_kMin.z, kPoint.z);

	   m_kMax.x = NiMax(m_kMax.x, kPoint.x);
	   m_kMax.y = NiMax(m_kMax.y, kPoint.y);
	   m_kMax.z = NiMax(m_kMax.z, kPoint.z);
   }

   inline void Intersect( float x, float y, float z )
   {
	   m_kMin.x = NiMin(m_kMin.x, x);
	   m_kMin.y = NiMin(m_kMin.y, y);
	   m_kMin.z = NiMin(m_kMin.z, z);

	   m_kMax.x = NiMax(m_kMax.x, x);
	   m_kMax.y = NiMax(m_kMax.y, y);
	   m_kMax.z = NiMax(m_kMax.z, z);
   }


   /// Collide a line against the box.
   ///
   /// Returns true on collision.
   bool CollideLine(const NiPoint3& start, const NiPoint3& end) const;
   bool CollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent) const;

   bool IsOverlapped(const CBox& box) const;
   bool IsContained(const NiPoint3& pos) const;

   int WhichSide(const NiPlane& kPlane) const;
   bool TestVisible(const NiFrustumPlanes& kPlanes);
   NiPoint3 GetCenter() const { return (m_kMin + m_kMax)/2.f; }
};