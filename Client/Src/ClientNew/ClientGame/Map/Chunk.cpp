
#include "StdAfx.h"

#include "Chunk.h"
#include "Tile.h"
#include "Collision.h"
#include "Water.h"
#include "TagStream.h"
#include "TerrainShader.h"
#include "LightManager.h"
#include "ClientState.h"
#include "Map.h"
#include "ModelInstance.h"
#include "CollisionData.h"
#include "ModelAdditionRenderer.h"
#include "SoundEmitter.h"


NiMaterial* CChunk::ms_pkTerrainMaterial;
unsigned short* CChunk::ms_pusTriList;

void CQuad::GetBoundBox(CBox& kBox) const
{
	kBox.m_kMin.x = x;
	kBox.m_kMin.y = y;
	kBox.m_kMin.z = FLOAT_MAX;
	kBox.m_kMin.z = z0 < kBox.m_kMin.z ? z0 : kBox.m_kMin.z;
	kBox.m_kMin.z = z1 < kBox.m_kMin.z ? z1 : kBox.m_kMin.z; 
	kBox.m_kMin.z = z2 < kBox.m_kMin.z ? z2 : kBox.m_kMin.z;
	kBox.m_kMin.z = z3 < kBox.m_kMin.z ? z3 : kBox.m_kMin.z;

	kBox.m_kMax.x = x + UNIT_SIZE;
	kBox.m_kMax.y = y + UNIT_SIZE;
	kBox.m_kMax.z = -FLOAT_MAX;
	kBox.m_kMax.z = z0 > kBox.m_kMax.z ? z0 : kBox.m_kMax.z;
	kBox.m_kMax.z = z1 > kBox.m_kMax.z ? z1 : kBox.m_kMax.z;
	kBox.m_kMax.z = z2 > kBox.m_kMax.z ? z2 : kBox.m_kMax.z;
	kBox.m_kMax.z = z3 > kBox.m_kMax.z ? z3 : kBox.m_kMax.z;
}

bool CQuad::CollideLine(const NiPoint3 &start, const NiPoint3 &end, float& fTime) const
{
	CBox BoundBox;
	GetBoundBox(BoundBox);

	if( !BoundBox.CollideLine(start, end) )
		return false;

	NiPoint3 v0(x,             y,		        z0);
	NiPoint3 v1(x + UNIT_SIZE, y,		        z1);
	NiPoint3 v2(x + UNIT_SIZE, y + UNIT_SIZE,	z2);
	NiPoint3 v3(x,             y + UNIT_SIZE,	z3);

	float t1 = LineCheckWithTriangle(start, end, v0, v1, v3);
	float t2 = LineCheckWithTriangle(start, end, v1, v2, v3);

	if( t1 > 1.0f && t2 > 1.0f )
		return false;

	fTime = NiMin( t1, t2 );

	return true;
}

bool CQuad::CollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, Result& result) const
{ 
	//先和Box碰撞
	CBox BoundBox;
	GetBoundBox(BoundBox);
	if( !BoundBox.CollideBox(start, end, extent) )
		return false;

	NiPoint3 v0(x,             y,		        z0);
	NiPoint3 v1(x + UNIT_SIZE, y,		        z1);
	NiPoint3 v2(x + UNIT_SIZE, y + UNIT_SIZE,	z2);
	NiPoint3 v3(x,             y + UNIT_SIZE,	z3);

	float t1 = FindSeparatingAxis(v0, v1, v3, start, end, extent, NiPoint3(1.f, 0.f, 0.f), NiPoint3(0.f, 1.f, 0.f), NiPoint3(0.f, 0.f, 1.f));
	float t2 = FindSeparatingAxis(v1, v2, v3, start, end, extent, NiPoint3(1.f, 0.f, 0.f), NiPoint3(0.f, 1.f, 0.f), NiPoint3(0.f, 0.f, 1.f));

	if( t1 > 1.0f && t2 > 1.0f )
		return false;

	NiPoint3 n;
	NiPoint3 dir = end - start;
	if( t1 < t2 )
	{
		n = NiPoint3(v0.z - v1.z, v0.z - v3.z, UNIT_SIZE);
		if( n.Dot(dir) < 0.f )
		{
			result.fTime = t1;
			result.kNormal = n;
			return true;
		}
		else if( t2 <= 1.f )
		{
			n = NiPoint3(v3.z - v2.z, v1.z - v2.z, UNIT_SIZE);
			if( n.Dot(dir) < 0.f )
			{
				result.fTime = t2;
				result.kNormal = n;
				return true;
			}
		}
	}
	else
	{
		n = NiPoint3(v3.z - v2.z, v1.z - v2.z, UNIT_SIZE);
		if( n.Dot(dir) < 0.f )
		{
			result.fTime = t2;
			result.kNormal = n;
			return true;
		}
		else if( t1 <= 1.f )
		{
			n = NiPoint3(v0.z - v1.z, v0.z - v3.z, UNIT_SIZE);
			if( n.Dot(dir) < 0.f )
			{
				result.fTime = t1;
				result.kNormal = n;
				return true;
			}
		}
	}

	return false;
}

void CQuad::MultiCollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList) const
{
	//先和Box碰撞
	CBox BoundBox;
	GetBoundBox(BoundBox);
	if( !BoundBox.CollideBox(start, end, extent) )
		return;

	NiPoint3 v0(x,             y,		        z0);
	NiPoint3 v1(x + UNIT_SIZE, y,		        z1);
	NiPoint3 v2(x + UNIT_SIZE, y + UNIT_SIZE,	z2);
	NiPoint3 v3(x,             y + UNIT_SIZE,	z3);

	float t1 = FindSeparatingAxis(v0, v1, v3, start, end, extent, NiPoint3(1.f, 0.f, 0.f), NiPoint3(0.f, 1.f, 0.f), NiPoint3(0.f, 0.f, 1.f));
	float t2 = FindSeparatingAxis(v1, v2, v3, start, end, extent, NiPoint3(1.f, 0.f, 0.f), NiPoint3(0.f, 1.f, 0.f), NiPoint3(0.f, 0.f, 1.f));

	NiPoint3 n;
	NiPoint3 dir = end - start;
	if( t1 < 1.0f )
	{
		//n = (v1 - v0).Cross(v3 - v1);
		n = NiPoint3(v0.z - v1.z, v0.z - v3.z, UNIT_SIZE);
		if( n.Dot(dir) < 0.f )
		{
			resultList.push_back(Result(t1, n));
		}
	}

	if( t2 < 1.0f )
	{
		//n = (v2 - v1).Cross(v3 - v2);
		n = NiPoint3(v3.z - v2.z, v1.z - v2.z, UNIT_SIZE);
		if( n.Dot(dir) < 0.f )
		{
			resultList.push_back(Result(t2, n));
		}
	}
}


CChunk::CChunk()
{
	m_pkPositionList = NiNew NiPoint3[VERTEX_COUNT*VERTEX_COUNT];
	memset(m_pkPositionList, 0, sizeof(NiPoint3)*VERTEX_COUNT*VERTEX_COUNT);

	m_pkNormalList = NiNew NiPoint3[VERTEX_COUNT*VERTEX_COUNT];
	memset(m_pkNormalList, 0, sizeof(NiPoint3)*VERTEX_COUNT*VERTEX_COUNT);

	m_pusTriList = NULL;

	m_usTriangles = (VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2;

	m_uiNumLayers = 0;
	memset(m_auiLayerTextures, 0, sizeof(m_auiLayerTextures));
    memset(m_auiGridInfo, 0xFF, sizeof(m_auiGridInfo));
	memset(m_auiFlyGridInfo, 0xFF, sizeof(m_auiFlyGridInfo));

	m_pkWater = NULL;
	m_bHasShadow = false;

	m_spkWaterEdgeMapDatas = NULL;
	m_spkWaterEdgeMapTextures = NULL;
}

CChunk::~CChunk()
{
	if( !m_spGeometry )
	{
		NiDelete[] m_pkPositionList;
		NiDelete[] m_pkNormalList;
	}

    for(unsigned int i = 0; i < m_kSoundEmitters.GetSize(); ++i)
    {
        NiDelete m_kSoundEmitters.GetAt(i);
    }

	NiFree(m_pusTriList);

	if( m_pkWater )
		NiDelete m_pkWater;
}

void CChunk::Set(int x, int y, CTile* pkTile)
{
	m_iX = x;
	m_iY = y;
	m_pkTile = pkTile;
}

//4Bit to Byte
unsigned char ToByte(unsigned char* pucPtr, bool bLowByte)
{
    float fValue;
    if(bLowByte)
        fValue = float(*pucPtr & 0x0f);
    else
        fValue = float((*pucPtr & 0xf0) >> 4);

    //变换到8bit
    fValue = fValue * (255.f / 15.f);
    return fValue >= 255.f ? 255 : unsigned char(fValue); 
}

bool CChunk::Load(NiBinaryStream* pkStream)
{
	m_kBoundBox.Reset();

	m_kBasePoint.x = float(m_pkTile->GetX() * TILE_SIZE + m_iX * CHUNK_SIZE);
	m_kBasePoint.y = float(m_pkTile->GetY() * TILE_SIZE + m_iY * CHUNK_SIZE);
	m_kBasePoint.z = 0.f;

	CTagStream kTagStream;
	while( kTagStream.ReadFromStream( pkStream ) )
	{
		switch( kTagStream.GetTag() )
		{
		case 'HEAD': //Header
			{
				if(m_pkTile->GetVersion() < MakeVersion(1, 0))
				{
					kTagStream.Read(&m_Header.m_uiTurnEdgeFlag1, sizeof(unsigned int));
					kTagStream.Read(&m_Header.m_uiTurnEdgeFlag2, sizeof(unsigned int));
					kTagStream.Read(&m_Header.m_uiHoleFlag1, sizeof(unsigned int));
					kTagStream.Read(&m_Header.m_uiHoleFlag2, sizeof(unsigned int));
					m_Header.m_uiAreaId = 0;
				}
				else
				{
					kTagStream.Read(&m_Header, sizeof(m_Header));
				}
			}
			break;

		case 'HGHT': //高度值
			{
				NIASSERT( kTagStream.GetSize() == VERTEX_COUNT*VERTEX_COUNT*sizeof(float) );

				float h;

				NiPoint3* pkPosition = m_pkPositionList;
				for(int  x = 0; x < VERTEX_COUNT; x++)
				{
					for(int y = 0; y < VERTEX_COUNT; y++)
					{
						kTagStream.Read(&h, sizeof(float));
						pkPosition->x = float(x*UNIT_SIZE);
						pkPosition->y = float(y*UNIT_SIZE);
						pkPosition->z = h;

						m_kBoundBox.Intersect(m_kBasePoint + *pkPosition);

						pkPosition++;
					}
				}
			}
			break;

		case 'NRML': //法线
			{
				NIASSERT( kTagStream.GetSize() == VERTEX_COUNT*VERTEX_COUNT*sizeof(char)*3 );

				char acNormal[3];
				NiPoint3* pkNormal = m_pkNormalList;
				for( int  x = 0; x < VERTEX_COUNT; x++ )
				{
					for( int y = 0; y < VERTEX_COUNT; y++ )
					{
						kTagStream.Read(acNormal, sizeof(acNormal));

						pkNormal->x = float(acNormal[0])/127.f;
						pkNormal->y = float(acNormal[1])/127.f;
						pkNormal->z = float(acNormal[2])/127.f;

						//*pkNormal *= 1.2f;

						pkNormal++;
					}
				}
			}
			break;

		case 'LMAP': //层贴图
			{
				m_uiNumLayers = kTagStream.GetSize() / sizeof(unsigned int);
				NIASSERT( m_uiNumLayers <= MAX_LAYERS );

				kTagStream.Read(m_auiLayerTextures, kTagStream.GetSize());
			}
			break;

		case 'ALPH':
			{
				NiBool bHasShadow;
				kTagStream.Read(&bHasShadow, sizeof(bHasShadow));
				m_bHasShadow = NIBOOL_IS_TRUE(bHasShadow);

				int iChannels; //每个Chanels占用4位
				if( m_uiNumLayers > 1 )
					iChannels = m_uiNumLayers - 1;
				else
					iChannels = 0;

				if( m_bHasShadow )
					iChannels++;

				NIASSERT( iChannels >= 1 && iChannels <= 6 );
				const NiPixelFormat* pkFormat1 = NULL;
				const NiPixelFormat* pkFormat2 = NULL;
				unsigned char* pucPtr1 = NULL;
				unsigned char* pucPtr2 = NULL;
				unsigned char* pucPtr = (unsigned char*)kTagStream.GetCurrentBuffer();
				switch( iChannels )
				{
				case 1:
					{
						pkFormat1 = &NiPixelFormat::BGRA4444;
						m_spAlphaMapData1 = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, *pkFormat1, 1, 1);
						pucPtr1 = m_spAlphaMapData1->GetPixels(0, 0);

						for( int i = 0; i < ALPHA_MAP_SIZE*ALPHA_MAP_SIZE/2; ++i )
						{
							pucPtr1[1] = ((pucPtr[0]&0x0f) << 4); //A
							pucPtr1[3] = (pucPtr[0]&0xf0); //A

							pucPtr++;
							pucPtr1 += 4;
						}
					}
					break;

				case 2:
					{
						pkFormat1 = &NiPixelFormat::BGRA4444;
						m_spAlphaMapData1 = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, *pkFormat1, 1, 1);
						pucPtr1 = m_spAlphaMapData1->GetPixels(0, 0);
						
						pkFormat2 = &NiPixelFormat::BGRA4444;
						m_spAlphaMapData2 = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, *pkFormat2, 1, 1);
						pucPtr2 = m_spAlphaMapData2->GetPixels(0, 0);

						for( int i = 0; i < ALPHA_MAP_SIZE*ALPHA_MAP_SIZE/2; ++i )
						{
							pucPtr1[1] = ((pucPtr[0]&0x0f) << 4); //A
							pucPtr1[3] = (pucPtr[0]&0xf0); //A

							pucPtr++;
							pucPtr1 += 4;
						}

						for( int i = 0; i < ALPHA_MAP_SIZE*ALPHA_MAP_SIZE/2; ++i )
						{
							pucPtr2[1] = ((pucPtr[0]&0x0f) << 4); //A
							pucPtr2[3] = (pucPtr[0]&0xf0); //A

							pucPtr++;
							pucPtr2 += 4;
						}
					}
					break;

				case 3:
					{
						pkFormat1 = &NiPixelFormat::BGRA4444;
						m_spAlphaMapData1 = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, *pkFormat1, 1, 1);
						pucPtr1 = m_spAlphaMapData1->GetPixels(0, 0);

						for( int i = 0; i < ALPHA_MAP_SIZE*ALPHA_MAP_SIZE/2; ++i )
						{
							//todo... 改变场景编辑器里面的通道的存储顺序
							pucPtr1[0] = (pucPtr[1] & 0x0f); //B
							pucPtr1[0] |= (pucPtr[0] & 0xf0);//G
							pucPtr1[1] = (pucPtr[0] & 0x0f);//A

							pucPtr1[2] =  (pucPtr[2] & 0xf0) >> 4; //B
							pucPtr1[2] |= (pucPtr[2] & 0x0f) << 4; //G
							pucPtr1[3] = (pucPtr[1] & 0xf0) >> 4; //A

							pucPtr1 += 4;
							pucPtr += 3;
						}
					}
					break;

				case 4:
					{
						pkFormat1 = &NiPixelFormat::BGRA4444;
						m_spAlphaMapData1 = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, *pkFormat1, 1, 1);
						pucPtr1 = m_spAlphaMapData1->GetPixels(0, 0);

						for( int i = 0; i < ALPHA_MAP_SIZE*ALPHA_MAP_SIZE; ++i )
						{
							pucPtr1[0] = (pucPtr[1]&0x0f); //B
							pucPtr1[0] |= (pucPtr[0]&0xf0); //G
							pucPtr1[1] = (pucPtr[0]&0x0f); //R
							pucPtr1[1] |= (pucPtr[1]&0xf0); //A

							pucPtr += 2;
							pucPtr1 += 2;
						}
					}
					break;

				case 5:
					{
						pkFormat1 = &NiPixelFormat::BGRA4444;
						m_spAlphaMapData1 = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, *pkFormat1, 1, 1);
						pucPtr1 = m_spAlphaMapData1->GetPixels(0, 0);

						pkFormat2 = &NiPixelFormat::BGRA4444;
						m_spAlphaMapData2 = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, *pkFormat2, 1, 1);
						pucPtr2 = m_spAlphaMapData2->GetPixels(0, 0);

						for( int i = 0; i < ALPHA_MAP_SIZE*ALPHA_MAP_SIZE; ++i )
						{
							pucPtr1[0] = (pucPtr[1]&0x0f); //B
							pucPtr1[0] |= (pucPtr[0]&0xf0); //G
							pucPtr1[1] = (pucPtr[0]&0x0f); //R
							pucPtr1[1] |= (pucPtr[1]&0xf0); //A

							pucPtr += 2;
							pucPtr1 += 2;
						}

						for( int i = 0; i < ALPHA_MAP_SIZE*ALPHA_MAP_SIZE/2; ++i )
						{
							pucPtr2[1] = ((pucPtr[0]&0x0f) << 4); //A
							pucPtr2[3] = (pucPtr[0]&0xf0); //A

							pucPtr++;
							pucPtr2 += 4;
						}
					}
					break;

				case 6:
					{
						pkFormat1 = &NiPixelFormat::BGRA4444;
						m_spAlphaMapData1 = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, *pkFormat1, 1, 1);
						pucPtr1 = m_spAlphaMapData1->GetPixels(0, 0);

						pkFormat2 = &NiPixelFormat::BGRA4444;
						m_spAlphaMapData2 = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, *pkFormat2, 1, 1);
						pucPtr2 = m_spAlphaMapData2->GetPixels(0, 0);

						for( int i = 0; i < ALPHA_MAP_SIZE*ALPHA_MAP_SIZE/2; ++i )
						{
							pucPtr1[0] = (pucPtr[1] & 0x0f); //B
							pucPtr1[0] |= (pucPtr[0] & 0xf0);//G
							pucPtr1[1] = (pucPtr[0] & 0x0f);//A

							pucPtr1[2] =  (pucPtr[2] & 0xf0) >> 4; //B
							pucPtr1[2] |= (pucPtr[2] & 0x0f) << 4; //G
							pucPtr1[3] = (pucPtr[1] & 0xf0) >> 4; //A

							pucPtr1 += 4;
							pucPtr += 3;
						}

						for( int i = 0; i < ALPHA_MAP_SIZE*ALPHA_MAP_SIZE/2; ++i )
						{
							pucPtr2[0] = (pucPtr[1] & 0x0f); //B
							pucPtr2[0] |= (pucPtr[0] & 0xf0);//G
							pucPtr2[1] = (pucPtr[0] & 0x0f);//A

							pucPtr2[2] =  (pucPtr[2] & 0xf0) >> 4; //B
							pucPtr2[2] |= (pucPtr[2] & 0x0f) << 4; //G
							pucPtr2[3] = (pucPtr[1] & 0xf0) >> 4; //A

							pucPtr2 += 4;
							pucPtr += 3;
						}
					}
					break;
				}
			}
			break;


		case 'WDMP': //水的深度图
			{
				m_spkWaterEdgeMapDatas = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, NiPixelFormat::ARGB8888, 1, 1);
				DWORD* pdwChannels = (DWORD*)m_spkWaterEdgeMapDatas->GetPixels(0, 0);

				int iBufferSize = sizeof(DWORD)*ALPHA_MAP_SIZE*ALPHA_MAP_SIZE; // 4 bits per pixel
				DWORD* pdwBuffer = NiAlloc(DWORD, iBufferSize);
				kTagStream.Read(pdwBuffer, iBufferSize);

				for( int i = 0; i < ALPHA_MAP_SIZE; i++ )
				{
					for( int j = 0; j < ALPHA_MAP_SIZE; j++ )
					{
						pdwChannels[i*ALPHA_MAP_SIZE+j] = pdwBuffer[i*ALPHA_MAP_SIZE+j];
					}
				}

				if ( !m_spkWaterEdgeMapTextures )
				{
					m_spkWaterEdgeMapTextures = CAlphaTexture::Create( m_spkWaterEdgeMapDatas );
				}
			}
			break;

		case 'LQUD': //水
			{
				m_pkWater = NiNew CWater(m_kBasePoint.x, m_kBasePoint.y, m_pkTile);
				m_pkWater->Load(&kTagStream);
			}
			break;

		case 'GRID'://GRID INFO
			{
				NiBool bUniformGrid;

				int iX; int iY;
				kTagStream.Read(&iX, sizeof(int));
				kTagStream.Read(&iY, sizeof(int));

				NIASSERT( iX == m_iX && iY == m_iY );

				kTagStream.Read(&bUniformGrid, sizeof(NiBool));
				if( bUniformGrid )
				{
					unsigned char ucUniformInfo;
					kTagStream.Read(&ucUniformInfo, sizeof(unsigned char));

					if( ucUniformInfo )
						memset(m_auiGridInfo, 0xFF, sizeof(m_auiGridInfo));
					else
						memset(m_auiGridInfo, 0, sizeof(m_auiGridInfo));
				}
				else
				{
					NIASSERT( GRID_COUNT == 32 );
					kTagStream.Read(m_auiGridInfo, sizeof(m_auiGridInfo));
				}
			}
			break;

		case 'FYGD':
			{
				NiBool bUniformGrid;

				int iX; int iY;
				kTagStream.Read(&iX, sizeof(int));
				kTagStream.Read(&iY, sizeof(int));

				NIASSERT( iX == m_iX && iY == m_iY );

				kTagStream.Read(&bUniformGrid, sizeof(NiBool));
				if( bUniformGrid )
				{
					unsigned char ucUniformInfo;
					kTagStream.Read(&ucUniformInfo, sizeof(unsigned char));

					if( ucUniformInfo )
						memset(m_auiFlyGridInfo, 0xFF, sizeof(m_auiFlyGridInfo));
					else
						memset(m_auiFlyGridInfo, 0, sizeof(m_auiFlyGridInfo));
				}
				else
				{
					NIASSERT( GRID_COUNT == 32 );
					kTagStream.Read(m_auiFlyGridInfo, sizeof(m_auiFlyGridInfo));
				}
			}
			break;

		//所引用的模型
		case 'MODL':
			{
				unsigned int uiCount;
				kTagStream.Read(&uiCount, sizeof(uiCount));
				m_kModelRefList.resize(uiCount);
				kTagStream.Read(&m_kModelRefList[0], uiCount*sizeof(ModelRef));
			}
			break;

        case 'GRSS':
            {
                m_kGrass.Load(&kTagStream, this);
            }
            break;

        case 'SNDE':
            {
                unsigned int uiSoundEmitterCount;
                kTagStream.Read(&uiSoundEmitterCount, sizeof(uiSoundEmitterCount));
                m_kSoundEmitters.SetSize(uiSoundEmitterCount);


                NiFixedString kName;
                NiFixedString kFilename;
                NiPoint3 kTranslate;
                unsigned int uiTimeInterval;
                unsigned int uiLoopCount;
                float fMinDistance;
                float fMaxDistance;
                CSoundEmitter* pkSoundEmitter;
                for(unsigned int i = 0; i < uiSoundEmitterCount; ++i)
                {
                    kTagStream.ReadFixedString(kName);
                    kTagStream.ReadFixedString(kFilename);
                    kTagStream.Read(&kTranslate, sizeof(NiPoint3));
                    kTagStream.Read(&uiTimeInterval, sizeof(unsigned int));
                    kTagStream.Read(&uiLoopCount, sizeof(unsigned int));
                    kTagStream.Read(&fMinDistance, sizeof(float));
                    kTagStream.Read(&fMaxDistance, sizeof(float));

                    kTranslate += m_kBasePoint;

                    pkSoundEmitter = NiNew CSoundEmitter;
                    pkSoundEmitter->SetFilename(kFilename);
                    pkSoundEmitter->SetTranslate(kTranslate);
                    pkSoundEmitter->SetTimeInterval((float)uiTimeInterval);
                    pkSoundEmitter->SetLoopCount(uiLoopCount);
                    pkSoundEmitter->SetMinDistance(fMinDistance);
                    pkSoundEmitter->SetMaxDistance(fMaxDistance);

                    m_kSoundEmitters.SetAt(i, pkSoundEmitter);
                }
            }
            break;

		default:
			{
				//NIASSERT( false );
			}
			break;
		}
	}

	CreateGeometry();

	return true;
}

bool CChunk::GetVertexIndex(float inX, float inY, int& iIndex) const
{
	float x = inX - m_kBoundBox.m_kMin.x;
	float y = inY - m_kBoundBox.m_kMin.y;

	unsigned int X = (unsigned int)(x/UNIT_SIZE);
	unsigned int Y = (unsigned int)(y/UNIT_SIZE);

	if( X >= VERTEX_COUNT || Y >= VERTEX_COUNT )
	{
		return false;
	}
	
	iIndex = X * VERTEX_COUNT + Y;
	return true;
}

float CChunk::GetHeight(float x, float y)
{
	float h;
	if( GetHeight(x, y, h) )
		return h;

	return 10.f;
}

bool CChunk::GetHeight(float x, float y, float& h)
{
	int iIndex;
	if( !GetVertexIndex( x, y, iIndex ) )
		return false;


	//  v3	          v2  
	//   *------------*
	//   |            |
	//   |            |
	//   |            |
	//   |            |
	//   |            |
	//   *------------*
	//  v0            v1
	
	float h0 = m_pkPositionList[iIndex].z;
	float h1 = m_pkPositionList[iIndex + VERTEX_COUNT].z;
	float h2 = m_pkPositionList[iIndex + VERTEX_COUNT + 1].z;
	float h3 = m_pkPositionList[iIndex + 1].z;

	float deltaX = NiFmod(x - m_kBoundBox.m_kMin.x, UNIT_SIZE)/float(UNIT_SIZE);
	float deltaY = NiFmod(y - m_kBoundBox.m_kMin.y, UNIT_SIZE)/float(UNIT_SIZE);

	if( deltaX + deltaY < 1.f )
	{
		//v3v0v1
		h = h0 + deltaX*(h1 - h0) + deltaY*(h3 - h0);
	}
	else
	{
		//v1v2v3
		h = h2 + (1 - deltaX)*(h3 - h2) + (1 - deltaY)*(h1 - h2);
	}

	return true;
}

bool CChunk::GetHeightAndNormal(float x, float y, float&h, NiPoint3& kNormal)
{
	int iIndex;
	if( !GetVertexIndex( x, y, iIndex ) )
		return false;


	//  v3	          v2  
	//   *------------*
	//   |            |
	//   |            |
	//   |            |
	//   |            |
	//   |            |
	//   *------------*
	//  v0            v1
	
	float h0 = m_pkPositionList[iIndex].z;
	float h1 = m_pkPositionList[iIndex + VERTEX_COUNT].z;
	float h2 = m_pkPositionList[iIndex + VERTEX_COUNT + 1].z;
	float h3 = m_pkPositionList[iIndex + 1].z;

	float deltaX = NiFmod(x - m_kBoundBox.m_kMin.x, UNIT_SIZE)/float(UNIT_SIZE);
	float deltaY = NiFmod(y - m_kBoundBox.m_kMin.y, UNIT_SIZE)/float(UNIT_SIZE);

	if( deltaX + deltaY < 1.f )
	{
		//v3v0v1
		h = h0 + deltaX*(h1 - h0) + deltaY*(h3 - h0);
		kNormal = (m_pkPositionList[iIndex + VERTEX_COUNT] - m_pkPositionList[iIndex]).UnitCross(m_pkPositionList[iIndex + 1] - m_pkPositionList[iIndex]);
	}
	else
	{
		//v1v2v3
		h = h2 + (1 - deltaX)*(h3 - h2) + (1 - deltaY)*(h1 - h2);
		kNormal = (m_pkPositionList[iIndex + 1] - m_pkPositionList[iIndex + VERTEX_COUNT + 1]).UnitCross(m_pkPositionList[iIndex + VERTEX_COUNT] - m_pkPositionList[iIndex + VERTEX_COUNT + 1]);
	}

	return true;
}


float CChunk::GetWaterHeight(float x, float y)
{
	float h;
	if( GetWaterHeight(x, y, h) )
		return h;

	return 10.f;
}

bool CChunk::GetWaterHeight(float x, float y, float& h)
{
	if( m_pkWater )
		return m_pkWater->GetHeight(x, y, h);

	return false;
}

bool CChunk::GetWaterFlag(float x, float y)
{
	if( m_pkWater )
		return m_pkWater->GetFlag(x, y);

	return false;
}

bool CChunk::GetFlyGridInfo(float x, float y)
{
	bool bGridInfo;

	if( GetFlyGridInfo(x, y, bGridInfo) )
		return bGridInfo;

	return false;
}

bool CChunk::GetFlyGridInfo(float x, float y, bool& flag)
{
	unsigned int X = unsigned int((x - m_kBoundBox.m_kMin.x)/GRID_SIZE);
	unsigned int Y = unsigned int((y - m_kBoundBox.m_kMin.y)/GRID_SIZE);

	if( X >= GRID_COUNT || Y >= GRID_COUNT )
		return false;

	//1可行走， 0不可行走
	flag = ( (m_auiFlyGridInfo[X] & (1 << Y)) != 0 );
	return true;
}

bool CChunk::GetGridInfo(float x, float y)
{
	bool bGridInfo;

	if( GetGridInfo(x, y, bGridInfo) )
		return bGridInfo;

	return false;
}

bool CChunk::GetGridInfo(float x, float y, bool& flag)
{
	unsigned int X = unsigned int((x - m_kBoundBox.m_kMin.x)/GRID_SIZE);
	unsigned int Y = unsigned int((y - m_kBoundBox.m_kMin.y)/GRID_SIZE);

	if( X >= GRID_COUNT || Y >= GRID_COUNT )
		return false;

	//1可行走， 0不可行走
	flag = ( (m_auiGridInfo[X] & (1 << Y)) != 0 );
	return true;
}


bool CChunk::GetQuad(float x, float y, CQuad& quad) const
{
	int iIndex;
	if( !GetVertexIndex(x, y, iIndex) )
		return false;


	//  v3	          v2  
	//   *------------*
	//   |            |
	//   |            |
	//   |            |
	//   |            |
	//   |            |
	//   *------------*
	//  v0            v1
	
	quad.z0 = m_pkPositionList[iIndex].z;
	quad.z1 = m_pkPositionList[iIndex + VERTEX_COUNT].z;
	quad.z2 = m_pkPositionList[iIndex + VERTEX_COUNT + 1].z;
	quad.z3 = m_pkPositionList[iIndex + 1].z;

	quad.x  = m_pkPositionList[iIndex].x + m_kBasePoint.x;
	quad.y  = m_pkPositionList[iIndex].y + m_kBasePoint.y;

	return true;
}

bool CChunk::CollideLine(const NiPoint3 &start, const NiPoint3 &end, float& fTime)
{
	if(CollideLineModels(start, end, fTime))
		return true;

	if(CollideLineTerrain(start, end, fTime))
		return true;

	return false;
}

bool CChunk::CollideLineTerrain(const NiPoint3 &start, const NiPoint3 &end, float& fTime)
{
	if( !m_kBoundBox.CollideLine(start, end) )
	   return false;

	float deltaX = end.x - start.x;
	float deltaY = end.y - start.y;
	int XDir = deltaX < 0.0f ? -1:1;
	int YDir = deltaY < 0.0f ? -1:1;

	int XStart, XEnd, YStart, YEnd;

	int XMin = (int)m_kBoundBox.m_kMin.x;
	int XMax = (int)m_kBoundBox.m_kMax.x;

	int YMin = (int)m_kBoundBox.m_kMin.y;
	int YMax = (int)m_kBoundBox.m_kMax.y;

	XStart = XDir > 0 ? ALIGN(start.x,           UNIT_SIZE) : ALIGN(start.x + UNIT_SIZE, UNIT_SIZE);
	XEnd   = XDir > 0 ? ALIGN(end.x + UNIT_SIZE, UNIT_SIZE) : ALIGN(end.x,               UNIT_SIZE);

	XStart = XDir > 0 ? NiMax(XStart, XMin) : NiMin(XStart, XMax);
	XEnd   = XDir > 0 ? NiMin(XEnd,   XMax) : NiMax(XEnd,   XMin);

	if( XStart*XDir >= XEnd*XDir )
		return false;

	int x, y;
	CQuad quad;
	if(XEnd == XStart + UNIT_SIZE*XDir)//此时DeltaX可能为0
	{
		YStart = YDir > 0 ? ALIGN(start.y,           UNIT_SIZE) : ALIGN(start.y + UNIT_SIZE, UNIT_SIZE);
		YEnd   = YDir > 0 ? ALIGN(end.y + UNIT_SIZE, UNIT_SIZE) : ALIGN(end.y,               UNIT_SIZE);

		YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
		YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);


		for( y = YStart; y*YDir < YEnd*YDir; y += UNIT_SIZE*YDir )
		{			
			if( GetQuad(float(XStart + XDir), float(y + YDir), quad) && 
				quad.CollideLine(start, end, fTime) )
			{
				return true;
			}
		}

		return false;
	}

	//Y = mX + c;
	float m = deltaY / deltaX;
	float c = end.y - end.x*m;
	float y1, y2;
	for( x = XStart; x*XDir < XEnd*XDir; x += UNIT_SIZE*XDir )
	{
		y1 = m*x + c;
		y2 = y1 + UNIT_SIZE*XDir*m;
		
		YStart = YDir > 0 ? ALIGN(y1,             UNIT_SIZE) : ALIGN(y1 + UNIT_SIZE, UNIT_SIZE);
		YEnd   = YDir > 0 ? ALIGN(y2 + UNIT_SIZE, UNIT_SIZE) : ALIGN(y2,             UNIT_SIZE);

		YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
		YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);

		for( y = YStart; y*YDir < YEnd*YDir; y += UNIT_SIZE*YDir )
		{
			if( GetQuad(float(x + XDir), float(y + YDir), quad) && 
				quad.CollideLine(start, end, fTime) )
			{
				return true;
			}
		}
   }

   return false;
}

bool CChunk::CollideLineModels(const NiPoint3 &start, const NiPoint3 &end, float& fTime)
{
	CMap* pkMap = CMap::Get();
	CModelInstance* pkModelInstance;
	unsigned int uiCollisionFrame = pkMap->GetCollisionFrame();
	for( unsigned int i = 0; i < m_kModelRefList.size(); ++i )
	{
		pkModelInstance = pkMap->GetModelInstance(m_kModelRefList[i]);
		if( !pkModelInstance 
		 || !pkModelInstance->GetCollision() 
		 || pkModelInstance->GetNumCollision() == uiCollisionFrame )
			continue;

		pkModelInstance->SetNumCollision(uiCollisionFrame);
		if(Collide(pkModelInstance->GetSceneRoot(), start, end, fTime))
			return true;
	}

	return false;
}

void CChunk::MultiCollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList)
{
	MultiCollideBoxModels(start, end, extent, resultList);
	MultiCollideBoxTerrain(start, end, extent, resultList);
}

void CChunk::MultiCollideBoxTerrain(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList)
{
	if( !m_kBoundBox.CollideBox(start, end, extent) )
		return;

	float deltaX = end.x - start.x;
	float deltaY = end.y - start.y;
	int XDir = deltaX < 0.0f ? -1:1;
	int YDir = deltaY < 0.0f ? -1:1;

	int XStart, XEnd, YStart, YEnd;

	int XMin = (int)m_kBoundBox.m_kMin.x;
	int XMax = (int)m_kBoundBox.m_kMax.x;

	int YMin = (int)m_kBoundBox.m_kMin.y;
	int YMax = (int)m_kBoundBox.m_kMax.y;

	XStart = XDir > 0 ? ALIGN(start.x - extent.x,             UNIT_SIZE) : ALIGN(start.x + extent.x + UNIT_SIZE, UNIT_SIZE);
	XEnd   = XDir > 0 ? ALIGN(end.x   + extent.x + UNIT_SIZE, UNIT_SIZE) : ALIGN(end.x   - extent.x,             UNIT_SIZE);

	XStart = XDir > 0 ? NiMax(XStart, XMin) : NiMin(XStart, XMax);
	XEnd   = XDir > 0 ? NiMin(XEnd,   XMax) : NiMax(XEnd,   XMin);

	if( XStart*XDir >= XEnd*XDir )
		return;

	int x, y;
	CQuad quad;
	if(NiAbs( deltaX ) < 0.01 || XEnd == XStart + UNIT_SIZE*XDir)//此时DeltaX可能为0
	{
		YStart = YDir > 0 ? ALIGN(start.y - extent.y,             UNIT_SIZE) : ALIGN(start.y + extent.y + UNIT_SIZE, UNIT_SIZE);
		YEnd   = YDir > 0 ? ALIGN(end.y   + extent.y + UNIT_SIZE, UNIT_SIZE) : ALIGN(end.y   - extent.y,             UNIT_SIZE);

		YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
		YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);

		for( x = XStart; x*XDir < XEnd*XDir; x += UNIT_SIZE*XDir )
		{
			for( y = YStart; y*YDir < YEnd*YDir; y += UNIT_SIZE*YDir)
			{			
				if( GetQuad(float(x + XDir), float(y + YDir), quad) )
					quad.MultiCollideBox(start, end, extent, resultList);
			}
		}
	}
	else
	{
		//Y = mX + c;
		float m = deltaY / deltaX;
		float c = end.y - end.x*m;
		float y1, y2;
		for( x = XStart; x*XDir < XEnd*XDir; x += UNIT_SIZE*XDir )
		{
			y1 = m*x + c;
			y2 = y1 + UNIT_SIZE*XDir*m;

			YStart = YDir > 0 ? ALIGN(y1 - extent.y,             UNIT_SIZE) : ALIGN(y1 + extent.y + UNIT_SIZE, UNIT_SIZE);
			YEnd   = YDir > 0 ? ALIGN(y2 + extent.y + UNIT_SIZE, UNIT_SIZE) : ALIGN(y2 - extent.y,             UNIT_SIZE);

			YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
			YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);

			for( y = YStart; y*YDir < YEnd*YDir; y += UNIT_SIZE*YDir )
			{
				if( GetQuad(float(x + XDir), float(y + YDir), quad) )
					quad.MultiCollideBox(start, end, extent, resultList);
			}
		}
	}
}

void CChunk::MultiCollideBoxModels(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList)
{
	CMap* pkMap = CMap::Get();
	CModelInstance* pkModelInstance;
	unsigned int uiCollisionFrame = pkMap->GetCollisionFrame();
	for( unsigned int i = 0; i < m_kModelRefList.size(); ++i )
	{
		pkModelInstance = pkMap->GetModelInstance(m_kModelRefList[i]);
		if( !pkModelInstance 
		 || !pkModelInstance->GetCollision() 
		 || pkModelInstance->GetNumCollision() == uiCollisionFrame )
			continue;

		pkModelInstance->SetNumCollision(uiCollisionFrame);
		MultiCollide(pkModelInstance->GetSceneRoot(), start, end, extent, resultList);
	}
}

void CChunk::BoxCheck(const CBox& kBox, NiTPrimitiveArray<CModelInstance*>& kModelList)
{
	CMap* pkMap = CMap::Get();
	CModelInstance* pkModelInstance;
	unsigned int uiCollisionFrame = pkMap->GetCollisionFrame();
	for( unsigned int i = 0; i < m_kModelRefList.size(); ++i )
	{
		pkModelInstance = pkMap->GetModelInstance(m_kModelRefList[i]);
		if( !pkModelInstance 
		 || !pkModelInstance->GetCollision() 
		 || pkModelInstance->GetNumCollision() == uiCollisionFrame )
			continue;

		pkModelInstance->SetNumCollision(uiCollisionFrame);
		if( pkModelInstance->GetType() != CModelInstance::ET_TRANSPORT ) //现在只和传送门碰撞
			continue;

		if(BoxCheckModelInstance(pkModelInstance->GetSceneRoot(), kBox))
			kModelList.Add(pkModelInstance);
	}
}

bool CChunk::IsTurnEdge(unsigned int uiX, unsigned int uiY) const
{
	NIASSERT( uiX < VERTEX_COUNT - 1 && uiY < VERTEX_COUNT - 1 )

	if( uiY < 4 )
	{
		//0, 1, 2, 3
		return (m_Header.m_uiTurnEdgeFlag1 & (1 << (uiY * 8 + uiX))) != 0;
	}
	else
	{
		//4, 5, 6, 7
		return (m_Header.m_uiTurnEdgeFlag2 & (1 << ((uiY - 4) * 8 + uiX))) != 0;
	}
}

bool CChunk::IsHole(unsigned int uiX, unsigned int uiY) const
{
	NIASSERT( uiX < VERTEX_COUNT - 1 && uiY < VERTEX_COUNT - 1 )

	if( uiY < 4 )
	{
		//0, 1, 2, 3
		return (m_Header.m_uiHoleFlag1 & (1 << (uiY * 8 + uiX))) != 0;
	}
	else
	{
		//4, 5, 6, 7
		return (m_Header.m_uiHoleFlag2 & (1 << ((uiY - 4) * 8 + uiX))) != 0;
	}
}


void CChunk::CreateTriList()
{
	if( m_Header.m_uiTurnEdgeFlag1 || m_Header.m_uiTurnEdgeFlag2 || m_Header.m_uiHoleFlag1 || m_Header.m_uiHoleFlag2 )
	{
		int idx = 0;
		m_pusTriList = NiAlloc(unsigned short, (VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2*3);
		for( unsigned short i = 0; i < VERTEX_COUNT - 1; i++ )
		{
			for( unsigned short j = 0; j < VERTEX_COUNT - 1; j++ )
			{
				// 1   3
				// *---*
				// |   |
				// *---*
				// 0   2

				if( IsHole(i, j) )
				{
					m_usTriangles -= 2;
					continue;
				}

				if( IsTurnEdge(i, j) )
				{
					//triangle 013
					m_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 0);
					m_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 0);
					m_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 1);


					//triangle 320
					m_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 1);
					m_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 1);
					m_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 0);
				}
				else
				{
					//triangle 012
					m_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 0);
					m_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 0);
					m_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 1);

					//triangle 321
					m_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 1);
					m_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 1);
					m_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 0);
				}
			}
		}
	}
	//else
	//{
	//	//使用共用的TriList
	//}
}

void CChunk::CreateGeometry()
{
	if( m_spGeometry )
		return;

	if( m_uiNumLayers == 0 )
		return;

	CreateTriList();

	NiTriShapeData* pkModelData = NiNew CTerrainGeomData(VERTEX_COUNT * VERTEX_COUNT, 
		m_pkPositionList, 
		m_pkNormalList, 
		NULL, 
		NULL,
		0,
		NiGeometryData::NBT_METHOD_NONE,
		m_usTriangles,
		m_pusTriList ? m_pusTriList : ms_pusTriList
		);
	
	m_spGeometry = NiNew NiTriShape(pkModelData);
	m_spGeometry->SetTranslate(m_kBasePoint);
	m_spGeometry->ApplyAndSetActiveMaterial(ms_pkTerrainMaterial);
	NiWireframeProperty* pkWireframe = NiNew NiWireframeProperty;
	pkWireframe->SetWireframe(true);
	m_spGeometry->AttachProperty(pkWireframe);
	m_spGeometry->UpdateProperties();
	m_spGeometry->Update(0.f);

	//Alpha texture
	if( m_spAlphaMapData1 )
		m_spAlphaTexture1 = CAlphaTexture::Create(m_spAlphaMapData1);

	if( m_spAlphaMapData2 )
		m_spAlphaTexture2 = CAlphaTexture::Create(m_spAlphaMapData2);

#ifdef NIDEBUG
	m_spGeometry->SetName("chunk");
#endif
}

void CChunk::BuildVisibleSet(CCullingProcess* pkCuller, bool bUpdate)
{
	pkCuller->ProcessObject(m_spGeometry, CCullingProcess::CullTerMesh);
	if( m_pkWater )
		m_pkWater->BuildVisibleSet(pkCuller);

	CMap* pkMap = CMap::Get();
	float fTime = pkMap->GetTime();

	unsigned int uiRenderFrame = pkMap->GetRenderFrame();
	CModelInstance* pkModelInstance;
	NiAVObject* pkAVObject;
    NiAlphaProperty* pkAlphaProperty;
	for( unsigned int i = 0; i < m_kModelRefList.size(); ++i )
	{
		pkModelInstance = pkMap->GetModelInstance(m_kModelRefList[i]);
		if( !pkModelInstance 
		 ||	pkModelInstance->GetNumRender() == uiRenderFrame
		 || pkModelInstance->GetHidden() )
			continue;

		pkModelInstance->SetNumRender(uiRenderFrame);

		pkAVObject = pkModelInstance->GetSceneRoot();
		if(bUpdate && pkModelInstance->GetShouldUpdate())
			pkAVObject->UpdateSelected(fTime);

		m_kModelRefArry.RemoveAll();
		
		pkCuller->ProcessObject(pkAVObject, CCullingProcess::CullTerObject, &m_kModelRefArry);

		//没有碰撞体， 不在上面显示Decal
		if(m_kModelRefArry.GetCount() == 0 || !pkModelInstance->GetCollision())
			continue;

		unsigned int ui = 0;
		for (ui=0; ui<m_kModelRefArry.GetCount(); ++ui)
		{
			NiGeometry& kGeometry = m_kModelRefArry.GetAt(ui);
			if(kGeometry.GetPropertyState())
			{
				pkAlphaProperty = kGeometry.GetPropertyState()->GetAlpha();
				if(pkAlphaProperty && pkAlphaProperty->GetAlphaBlending())
				{
					//在特殊的模型上面不显示Decals
					if( pkAlphaProperty->GetSrcBlendMode() <= NiAlphaProperty::ALPHA_INVDESTCOLOR 
						|| pkAlphaProperty->GetDestBlendMode() <= NiAlphaProperty::ALPHA_INVDESTCOLOR )
						continue;
				}
			}

			ModelAdditionRenderer::Render(m_iX, m_iY, &kGeometry);
		}

	}
}

void CChunk::BuildReflectVisibleSet(CCullingProcess* pkCuller)
{
	if(m_kBoundBox.TestVisible(pkCuller->GetReflectFrustumPlanes()))
		pkCuller->OnReflectChunkVisible(this);

	CMap* pkMap = CMap::Get();
	unsigned int uiRenderFrame = pkMap->GetRenderFrame();
	CModelInstance* pkModelInstance;
	NiAVObject* pkAVObject;
	for( unsigned int i = 0; i < m_kModelRefList.size(); ++i )
	{
		pkModelInstance = pkMap->GetModelInstance(m_kModelRefList[i]);
		if( !pkModelInstance
		 || !pkModelInstance->GetReflect() 
		 || pkModelInstance->GetHidden()
		 || pkModelInstance->GetNumRender() == uiRenderFrame)
			continue;

		pkAVObject = pkModelInstance->GetSceneRoot();
		pkModelInstance->SetNumRender(uiRenderFrame);
		pkCuller->CullReflectModel(pkAVObject);
	}
}

CTileTexture* CChunk::GetLayerTexture(unsigned int uiLayer)
{
	NIASSERT(uiLayer < m_uiNumLayers);

	return m_pkTile->GetTexture(m_auiLayerTextures[uiLayer]);
}

//静态初始化
void CChunk::_SDMInit()
{
	ms_pkTerrainMaterial = NiSingleShaderMaterial::Create(NiNew CTerrainShader);
	ms_pkTerrainMaterial->IncRefCount();

	ms_pusTriList = NiAlloc(unsigned short, (VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2*3);

	//generate triangle indices
	unsigned short idx = 0;
	for( unsigned short i = 0; i < VERTEX_COUNT - 1; i++ )
	{
		for( unsigned short j = 0; j < VERTEX_COUNT - 1; j++ )
		{
			// 1   3
			// *---*
			// |   |
			// *---*
			// 0   2

			//triangle 012
			ms_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 0);
			ms_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 0);
			ms_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 1);

			//triangle 321
			ms_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 1);
			ms_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 1);
			ms_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 0);
		}
	}
}

void CChunk::_SDMShutdown()
{
	ms_pkTerrainMaterial->DecRefCount();

	NiFree(ms_pusTriList);
}
