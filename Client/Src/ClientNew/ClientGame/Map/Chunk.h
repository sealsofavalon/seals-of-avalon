#pragma once

#include "Box.h"
#include "CullingProcess.h"
#include "Collision.h"
#include "GrassInfo.h"
#include "SoundEmitter.h"

#define MAX_LAYERS			(6)
#define ALPHA_MAP_SIZE		(64)

#define UNIT_SIZE			(4)
#define UNIT_COUNT			(8)
#define VERTEX_COUNT		(UNIT_COUNT + 1) //9

#define CHUNK_SIZE			(UNIT_COUNT * UNIT_SIZE) //32
#define CHUNK_COUNT			(16)

#define TILE_SIZE			(CHUNK_COUNT * CHUNK_SIZE) //512
#define TILE_COUNT			(64)

#define TILE_INDEX(x)		(int(x) / TILE_SIZE)			 

#define MAP_SIZE			(TILE_COUNT * TILE_SIZE) //32768

#define ALIGN( X, ALIGNMENT ) ( int(int(X) & ~(ALIGNMENT-1)) )

#define INVALID_LAYER		(0xFFFFFFFF)

#define MakeVersion(major, minor) ((unsigned int(major) << 8) + unsigned int(minor))

struct CChunkHeader
{
	unsigned int m_uiTurnEdgeFlag1;
	unsigned int m_uiTurnEdgeFlag2;
	unsigned int m_uiHoleFlag1;
	unsigned int m_uiHoleFlag2;
	unsigned int m_uiAreaId;
};

struct ModelRef
{
	int iTileX;
	int iTileY;
	unsigned int uiIndex;
};

typedef std::vector<ModelRef> ModelRefList;

//(x,y+4,z3)	(x+4,y+4,z2)  
//   *------------*
//   |            |
//   |            |
//   |            |
//   |            |
//   |            |
//   *------------*
//(x, y, z0)  (x+4, y, z1)
class CQuad
{
public:
	float x;
	float y;
	float z0;
	float z1;
	float z2;
	float z3;

	void GetBoundBox(CBox& box) const;
	bool CollideLine(const NiPoint3 &start, const NiPoint3 &end, float& fTime) const;
	bool CollideBox(const NiPoint3 &start, const NiPoint3 &end, const NiPoint3& extent, Result& result) const;
	void MultiCollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList) const;
};

#define GRID_SIZE		(1)
#define GRID_COUNT      (CHUNK_SIZE/GRID_SIZE)

struct CTileTexture;
class CTile;
class CWater;
class CModelInstance;
class CChunk 
{
public:
	CChunk();
	virtual ~CChunk();

	void Set(int x, int y, CTile* pkTile);

	bool Load(NiBinaryStream* pkStream);

	bool GetVertexIndex(float x, float y, int& iIndex) const;

	float GetHeight(float x, float y);
	bool GetHeight(float x, float y, float& h);
	bool GetHeightAndNormal(float x, float y, float&h, NiPoint3& kNormal);

	float GetWaterHeight(float x, float y);
	bool GetWaterHeight(float x, float y, float& h);
	bool GetWaterFlag(float x, float y);

	bool GetFlyGridInfo(float x, float y);
	bool GetFlyGridInfo(float x, float y, bool& flag);

	bool GetGridInfo(float x, float y);
	bool GetGridInfo(float x, float y, bool& flag);

	bool GetQuad(float inX, float inY, CQuad& quad) const;
	bool CollideLine(const NiPoint3 &start, const NiPoint3 &end, float& fTime);
	bool CollideLineModels(const NiPoint3 &start, const NiPoint3 &end, float& fTime);
	bool CollideLineTerrain(const NiPoint3 &start, const NiPoint3 &end, float& fTime);
	void MultiCollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList);
	void MultiCollideBoxTerrain(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList);
	void MultiCollideBoxModels(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList);
	void BoxCheck(const CBox& kBox, NiTPrimitiveArray<CModelInstance*>& kModelList);

	bool IsTurnEdge(unsigned int uiX, unsigned int uiY) const;
	bool IsHole(unsigned int uiX, unsigned int uiY) const;

	NiTexture* CreateAlphaTexture(NiPixelData* pkPixelData);
	void BuildTextureProperty();
	void CreateTriList();
	void CreateGeometry();
	NiTriBasedGeom* GetGeometry() { return m_spGeometry; }

	const CBox& GetBoundBox() const { return m_kBoundBox; }
	void BuildVisibleSet(CCullingProcess* pkCuller, bool bUpdate);
	void BuildReflectVisibleSet(CCullingProcess* pkCuller);

	unsigned int GetNumLayers() const { return m_uiNumLayers; }
	bool HasShadow() const { return m_bHasShadow; }
	CTileTexture* GetLayerTexture(unsigned int uiLayer);
	NiTexture* GetAlphaTexture1() const { return m_spAlphaTexture1; }
	NiTexture* GetAlphaTexture2() const { return m_spAlphaTexture2; }

	NiTexture*	GetWaterEdgeMapTexture() const { return m_spkWaterEdgeMapTextures; }

	unsigned int GetAreaId() const { return m_Header.m_uiAreaId; }
    const NiPoint3& GetBasePoint() const { return m_kBasePoint; }

    const CGrassInfo& GetGrassInfo() const { return m_kGrass; }

    unsigned int GetSoundEmitterCount() const { return m_kSoundEmitters.GetSize(); }
    CSoundEmitter* GetSoundEmitter(unsigned int uiIdx)
    {
        NIASSERT(uiIdx < m_kSoundEmitters.GetSize());
        if( uiIdx < m_kSoundEmitters.GetSize() )
            return m_kSoundEmitters.GetAt(uiIdx);
        return NULL;
    }

	static void _SDMInit();
	static void _SDMShutdown();

private:
	int			m_iX;
	int			m_iY;

	CChunkHeader m_Header;

	NiPoint3	m_kBasePoint;
	CBox		m_kBoundBox;
	CTile*		m_pkTile;


	//����ͷ���
	NiPoint3*		m_pkPositionList;
	NiPoint3*		m_pkNormalList;
	unsigned short* m_pusTriList;
	unsigned short  m_usTriangles;
	NiTriShapePtr	m_spGeometry;

	unsigned int	m_uiNumLayers;
	unsigned int	m_auiLayerTextures[MAX_LAYERS];
	NiPixelDataPtr	m_spAlphaMapData1;
	NiPixelDataPtr	m_spAlphaMapData2;
	NiTexturePtr	m_spAlphaTexture1;
	NiTexturePtr	m_spAlphaTexture2;

	NiPixelDataPtr m_spkWaterEdgeMapDatas;
	NiTexturePtr m_spkWaterEdgeMapTextures;

	bool			m_bHasShadow;
	CWater*			m_pkWater;

	NiVisibleArray	m_kModelRefArry;

	ModelRefList	m_kModelRefList;

	unsigned int	m_auiGridInfo[GRID_COUNT];
	unsigned int	m_auiFlyGridInfo[GRID_COUNT];

    CGrassInfo      m_kGrass;

    NiTPrimitiveArray<CSoundEmitter*> m_kSoundEmitters;

	static NiMaterial* ms_pkTerrainMaterial;
	static unsigned short*	ms_pusTriList;
};
