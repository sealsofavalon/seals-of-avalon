#pragma once

#include "Box.h"
#define DELTA (0.00001)

float LineCheckWithTriangle(const NiPoint3& start,const NiPoint3& end, const NiPoint3& v0,const NiPoint3& v1,const NiPoint3& v2);

struct Result
{
	float fTime;
	NiPoint3 kNormal;
	NiAVObject* pkCollObject;

	Result(float t, const NiPoint3& n, NiAVObject* pkCollObj = NULL)
		:fTime(t)
		,kNormal(n)
		,pkCollObject(pkCollObj)
	{
	}

	Result()
		:fTime(0.f)
		,kNormal(0.f, 0.f, 0.f)
		,pkCollObject(NULL)
	{
	}

	Result(const Result& other)
		:fTime(other.fTime)
		,kNormal(other.kNormal)
		,pkCollObject(other.pkCollObject)
	{
	}
};

struct ResultList : public std::vector<Result>
{
	bool Compare(const Result& r1, const Result& r2, bool bMinZ) const
	{
		if( r1.fTime < r2.fTime )
			return true;

		if( r1.fTime > r2.fTime )
			return false;

		if(bMinZ)
			return NiAbs(r1.kNormal.z) <= NiAbs(r2.kNormal.z);
		else
			return r1.kNormal.z >= r2.kNormal.z;
	}

	void Sort(bool bMinZ)
	{
		unsigned int uiSize = size();
		for( unsigned int i = 0; i < uiSize; ++i )
		{
			at(i).kNormal.Unitize();
		}

		Result t;
		for(unsigned int i = 0; i < uiSize; i++)
		{
			for(unsigned int j = i + 1; j < uiSize; j++)
			{
				if( Compare(at(j), at(i), bMinZ) )
				{
					t = at(i);
					at(i) = at(j);
					at(j) = t;
				}
			}
		}
	}
};

struct Move
{
	Move()
		:x(0.f), y(0.f), z(0.f)
		,yaw(0.f), pitch(0.f), roll(0.f)
	{}

	float x, y, z;          // float -1 to 1
	float yaw, pitch, roll; // 0-2PI
};

float FindSeparatingAxis(
	const NiPoint3& V0,
	const NiPoint3& V1,
	const NiPoint3& V2,
	const NiPoint3& Start,
	const NiPoint3& End,
	const NiPoint3& BoxExtent,
	const NiPoint3& BoxX,
	const NiPoint3& BoxY,
	const NiPoint3& BoxZ
	);


bool Collide(NiAVObject* pkAVObject, const NiPoint3& start, const NiPoint3& end, float& fTime);
void MultiCollide(NiAVObject* pkAVObject, const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList);
bool BoxCheckModelInstance(NiAVObject* pkAVObject, const CBox& kBox);

bool CreateCollisionData(NiAVObject* pkAVObject);

enum EPhysics
{
    PHYS_None               =0,
    PHYS_Walking            =1,
    PHYS_Falling            =2,
    PHYS_Swimming           =3,
    PHYS_Flying             =4,
    PHYS_Rotating           =5,
    PHYS_Projectile         =6,
    PHYS_Interpolating      =7,
    PHYS_MovingBrush        =8,
    PHYS_Spider             =9,
    PHYS_Trailer            =10,
    PHYS_Ladder             =11,
    PHYS_RootMotion         =12,
    PHYS_MAX                =13,
};