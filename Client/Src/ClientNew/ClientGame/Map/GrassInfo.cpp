#include "stdafx.h"
#include "GrassInfo.h"
#include "BitArray.h"
#include "Chunk.h"

void CGrassInfo::Load(NiBinaryStream* pkStream, CChunk* pkChunk)
{
    unsigned int uiGrassCount;
    pkStream->Read(&uiGrassCount, sizeof(m_uiGrassCount));
    if(uiGrassCount == 0)
        return;

    CBitArray<32*32> kGrassMask; //1024 bits
    unsigned int* puiBits = kGrassMask.GetBits();
    unsigned int uiCount = kGrassMask.GetBitCount() / 32;
    pkStream->Read(puiBits, uiCount * sizeof(unsigned int));

    m_pkGrassVertex = NiAlloc(GrassVertex, uiGrassCount*12); //每颗草4个面， 12个顶点

    unsigned int uiBits;
    unsigned short usGrass;
    NiPoint3 kCenter;
    const NiPoint3& kBasePoint = pkChunk->GetBasePoint();
    for(unsigned int i = 0; i < 32; ++i)
    {
        uiBits = *puiBits++;
        for(unsigned int j = 0; j < 32; ++j)
        {
            if(uiBits & (1 << j))
            {
                pkStream->Read(&usGrass, sizeof(unsigned short));

                kCenter = kBasePoint + NiPoint3(float(i), float(j), 0.f);
                kCenter.z = pkChunk->GetHeight(kCenter.x, kCenter.y);
                AddGrass(kCenter, usGrass);
            }
        }
    }

    NIASSERT(m_uiGrassCount == uiGrassCount);
}

void CGrassInfo::AddGrass(const NiPoint3& kCenter, unsigned short usGrass)
{
    //增加一个固定的小角度偏移, 防止相邻的两个面完全重叠在一起
    float fRot = GrassRot(usGrass) + NiFmod(10.f*kCenter.x + kCenter.y, 100.f)/5000.f;
    unsigned int idx = GrassIdx(usGrass)*4; //贴图坐标起始位置
    unsigned int offset = GrassOffset(usGrass);
    float fSize = GrassSize(usGrass);

    GrassVertex* pkGrassVertex = &m_pkGrassVertex[m_uiGrassCount*12];
    
    //triangle 0 ---------------------------------------------------------------------
    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 0, idx + 0, offset);  //0
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 1, idx + 1, offset);  //1
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 2, idx + 2, offset);  //2
    ++pkGrassVertex;


    //triangle 1 ---------------------------------------------------------------------
    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 2, idx + 2, offset);  //重复2
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 3, idx + 3, offset);  //3
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 0, idx + 0, offset); //重复0
    ++pkGrassVertex;

    //triangle 2 ---------------------------------------------------------------------
    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 4, idx + 0, offset);  //4
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 5, idx + 1, offset);  //5
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 6, idx + 2, offset);  //6
    ++pkGrassVertex;

    //triangle 3 ---------------------------------------------------------------------
    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 6, idx + 2, offset);  //重复6
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 7, idx + 3, offset);  //3
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kCenter, fSize, fRot, 4, idx + 0, offset); //重复4
    ++pkGrassVertex;

    ++m_uiGrassCount;
}