#include "stdafx.h"
#include "GrassMesh.h"
#include "Map.h"
#include "Utils/ClientUtils.h"
#include "LightManager.h"

CGrassMesh::CGrassMesh()
{
    m_spVertexShader = CreateVertexShader("Shaders\\Grass.hlsl", "GrassVS");
    m_spPixelShader = CreatePixelShader("Shaders\\Grass.hlsl", "GrassPS");

    Clear();
}

void CGrassMesh::SetXYCount(unsigned int uiXCount, unsigned int uiYCount) 
{
    float fX = 1.f / uiXCount;
    float fY = 1.f / uiYCount;
    for(unsigned int i = 0; i < uiXCount; ++i)
    {
        for(unsigned int j = 0; j < uiYCount; ++j)
        {
            m_kUV[i*uiYCount + j][0].x = i*fX;
            m_kUV[i*uiYCount + j][0].y = j*fY + fY;

            m_kUV[i*uiYCount + j][1].x = i*fX;
            m_kUV[i*uiYCount + j][1].y = j*fY;

            m_kUV[i*uiYCount + j][2].x = i*fX + fX;
            m_kUV[i*uiYCount + j][2].y = j*fY;

            m_kUV[i*uiYCount + j][3].x = i*fX + fX;
            m_kUV[i*uiYCount + j][3].y = j*fY + fY;
        }
    }
}

void CGrassMesh::SetTextureName(const NiFixedString& kTextureName)
{
    m_kTextureName = kTextureName;
    if(m_kTextureName)
		m_spTexture = NiSourceTexture::Create(m_kTextureName);
    else
        m_spTexture = NULL;
}

void CGrassMesh::Draw(const NiPoint3& kOrigin)
{
    if(m_uiTotalVertexCount == 0 || m_spTexture == NULL)
        return;

    NiDX9Renderer* pkRenderer = (NiDX9Renderer*)NiRenderer::GetRenderer();
    if(pkRenderer == NULL)
        return;

    LPDIRECT3DDEVICE9 pkD3DDevice = pkRenderer->GetD3DDevice();
    NiDX9RenderState* pkRenderState = pkRenderer->GetRenderState();

    bool bS0Mipmap;
    bool bNonPow2;
    bool bChanged;
    D3DBaseTexturePtr pkD3DTexture = pkRenderer->GetTextureManager()->PrepareTextureForRendering(m_spTexture, bChanged, bS0Mipmap, bNonPow2);
    pkRenderState->SetTexture(0, pkD3DTexture);

    pkRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
    //pkRenderState->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
    //pkRenderState->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
    pkRenderState->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
    pkRenderState->SetRenderState(D3DRS_ALPHAREF, 64);
    pkRenderState->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);
    pkRenderState->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
    pkRenderState->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
    pkRenderState->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);
    pkRenderState->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    pkRenderState->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    pkRenderState->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

    pkRenderState->SetVertexShader(m_spVertexShader->GetShaderHandle());
    pkRenderState->SetPixelShader(m_spPixelShader->GetShaderHandle());

    D3DXMATRIX kTempMat;
    D3DXMATRIX kD3DView = pkRenderer->GetD3DView();
    D3DXMATRIX kD3DProj = pkRenderer->GetD3DProj();

    float fValues[8 + 64][4];

    fValues[0][0] = kOrigin.x;
    fValues[0][1] = kOrigin.y;
    fValues[0][2] = kOrigin.z;
    fValues[0][3] = 0.f;

    kTempMat = kD3DView * kD3DProj;
    D3DXMatrixTranspose(&kTempMat, &kTempMat);
    memcpy(fValues[1], &kTempMat, sizeof(kTempMat));

	CLightManager* pkLightManager = CLightManager::Get();
	const NiColor& kDiffuseColor = pkLightManager->GetDiffuseColor();
	const NiColor& kAmbientColor = pkLightManager->GetAmbientColor();

    fValues[5][0] = kDiffuseColor.r;
    fValues[5][1] = kDiffuseColor.g;
    fValues[5][2] = kDiffuseColor.b;
    fValues[5][3] = 1.f;

    fValues[6][0] = kAmbientColor.r;
    fValues[6][1] = kAmbientColor.g;
    fValues[6][2] = kAmbientColor.b;
    fValues[6][3] = 1.f;

    fValues[7][0] = 0.f;
    fValues[7][1] = CMap::Get()->GetTime()*0.4f;//获取当前时间

    memcpy(fValues[8], m_kUV, sizeof(m_kUV));

    pkD3DDevice->SetVertexShaderConstantF(0, &fValues[0][0], sizeof(fValues)/(4*sizeof(float)));
    pkD3DDevice->SetFVF(D3DFVF_XYZ|D3DFVF_TEX2|D3DFVF_TEXCOORDSIZE4(0)|D3DFVF_TEXCOORDSIZE1(1));

    unsigned int uiVertexCount;
    for(unsigned int i = 0; i < MAX_MESH_COUNT; ++i)
    {
        uiVertexCount = m_uiVertexCount[i];
        if(uiVertexCount > 0)
        {
            pkD3DDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST,
                uiVertexCount/3,
                m_kGrassVertices[i],
                sizeof(GrassVertex));
        }
        else
            break;
    }

    Clear();
}

//清空
void CGrassMesh::Clear()
{
    m_uiTotalVertexCount = 0;
    m_uiCurrentMesh = 0;
    for(int i = 0; i < MAX_MESH_COUNT; ++i)
        m_uiVertexCount[i] = 0;
}

void CGrassMesh::AddGrassVertex(const GrassVertex* pkGrassVertex, unsigned int uiCount)
{
    if(m_uiCurrentMesh == MAX_MESH_COUNT)
        return;

    NIASSERT(pkGrassVertex && uiCount > 0);

    m_uiTotalVertexCount += uiCount;
    unsigned int uiCurrentCount = m_uiVertexCount[m_uiCurrentMesh];

    if(uiCurrentCount + uiCount > MAX_VERTEX_COUNT) //超出, 先Copy一部分
    {
        unsigned int uiToCopy = MAX_VERTEX_COUNT - uiCurrentCount;

        NiMemcpy(&m_kGrassVertices[m_uiCurrentMesh][uiCurrentCount], pkGrassVertex, uiToCopy*sizeof(GrassVertex));
        m_uiVertexCount[m_uiCurrentMesh] = MAX_VERTEX_COUNT;

        ++m_uiCurrentMesh;
        if(m_uiCurrentMesh == MAX_MESH_COUNT)
            return;

        uiCurrentCount = 0;
        pkGrassVertex += uiToCopy;
        uiCount -= uiToCopy;
    }

	// modified by Gui
	if( uiCurrentCount + uiCount <= MAX_VERTEX_COUNT )
	{
		NiMemcpy(&m_kGrassVertices[m_uiCurrentMesh][uiCurrentCount], pkGrassVertex, uiCount*sizeof(GrassVertex));
		m_uiVertexCount[m_uiCurrentMesh] += uiCount;
	}
	else
		assert( 0 );
}