#ifndef GRASS_MESH_H
#define GRASS_MESH_H

#include "GrassInfo.h"

#define MAX_MESH_COUNT (8)
#define MAX_GRASS_COUNT (5400)
#define MAX_VERTEX_COUNT (MAX_GRASS_COUNT*12) //每颗草4个面12个顶点
class CGrassMesh : public NiMemObject
{
public:
    CGrassMesh();

    void SetXYCount(unsigned int uiXCount, unsigned int uiYCount);
    void SetTextureName(const NiFixedString& kTextureName);

    void Draw(const NiPoint3& kOrigin);
    void AddGrassVertex(const GrassVertex* pkGrassVertex, unsigned int uiCount);
    void Clear();

private:
    unsigned int m_uiVertexCount[MAX_MESH_COUNT];
    GrassVertex m_kGrassVertices[MAX_MESH_COUNT][MAX_VERTEX_COUNT];
    NiTexturePtr m_spTexture;

    NiPoint4 m_kUV[16][4];
    NiFixedString m_kTextureName;

    NiD3DVertexShaderPtr m_spVertexShader;
    NiD3DPixelShaderPtr m_spPixelShader;

    unsigned int m_uiCurrentMesh;
    unsigned int m_uiTotalVertexCount;
};

#endif