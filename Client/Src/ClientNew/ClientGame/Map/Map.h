#pragma once

#include "Chunk.h"
#include "Tile.h"
#include "Collision.h"
#include "Water.h"
#include "WaterShader.h"
#include "Sky.h"
#include "PathNode.h"
#include "GrassMesh.h"
#include "Festival_Flag.h"

#define CURRENT_TILE_SIZE 25
#define INVALID_INDEX - 1

NiSmartPointer(FadeObjectAccum);

inline bool IsValidTileIndex(int x, int y)
{
	if( x < 0
		|| x >= TILE_COUNT
		|| y < 0
		|| y >= TILE_COUNT )
	{
		return false;
	}

	return true;
}


class CMap : public NiMemObject
{
public:
    // construction
    CMap(Festival_Flag Festival);
    virtual ~CMap();
	void Reset();

	int			GetTileFlag(int x, int y) const;
	CTile*		GetTile(float x, float y);

	inline CTile* GetTileByIndex(int x, int y)
	{
		//越界或者不存在
		if( !IsLoaded() || GetTileFlag(x, y) == 0 )
			return NULL;

		return m_pkTiles[x][y];
	}

	inline CChunk* GetChunk(float x, float y)
	{
		CTile* tile = GetTile(x, y);
		if( tile )
			return tile->GetChunk(x, y);

		return NULL;
	}

	inline float GetHeight(float x, float y)
	{
		float h;
		if( GetHeight(x, y, h) )
			return h;

		return 10.f;
	}
	
	inline bool GetHeight(float x, float y, float& h)
	{
		CChunk* pkChunk = GetChunk(x, y);
		if( pkChunk )
			return pkChunk->GetHeight(x, y, h);

		return false;
	}

	inline bool GetHeightAndNormal(float x, float y, float&h, NiPoint3& kNormal)
	{
		CChunk* pkChunk = GetChunk(x, y);
		if( pkChunk )
			return pkChunk->GetHeightAndNormal(x, y, h, kNormal);

		return false;
	}
	float		GetWaterHeight(float x, float y);
	bool		GetWaterHeight(float x, float y, float& h);
	bool		GetWaterFlag(float x, float y);
	void		UpdateWaterShaders(const NiColor& kAmbientColor, const NiColor& kDiffuseColor);
	CWaterShader* GetWaterShader(unsigned int uiIndex);

	bool		GetFlyGridInfo(float x, float y);
	bool		GetFlyGridInfo(float x, float y, bool& flag);
	bool		GetGridInfo(float x, float y, bool bIsFly = false);
	bool		GetGridInfo(float x, float y,bool bIsFly, bool& flag );

	//在本地图内传送
	bool		EnterTile(float x, float y);

	//从一个其他的地图传送过来
	void		Transport(const NiFixedString& kMapName);

	const		NiFixedString& GetMapName() const { return m_kMapName; }
	void		SetMapName(NiFixedString& kMapName) { m_kMapName = kMapName; }
	unsigned int GetMapId() const { return m_uiMapId; }
	void         SetMapId(unsigned int uiMapId) { m_uiMapId = uiMapId; }

	unsigned int GetAreaId(float x, float y);

	bool		Load();
	void		Unload();

	void		LoadTile(int x, int y);
	bool		HasTile() const;

	bool		CheckLoaded();
	bool		IsLoaded();

	bool		CollideLine(const NiPoint3 &start, const NiPoint3 &end, float& fTime);
	bool		MultiCollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList);
	bool		MultiCollideBox(const NiBound& kBound, ResultList& resultList);
	bool		MoveObject(NiPoint3& start, const NiPoint3& delta, const NiPoint3& extent, Result& result);

	void		BoxCheck(const CBox& kBox, NiTPrimitiveArray<CModelInstance*>& kModelList);

    void		Init();
    void		Shutdown();

	void		Update(float fAccumTime, float fFrameTime);
	void		BuildVisibleSet(CCullingProcess* pkCuller);
	void		BuildReflectVisibleSet(CCullingProcess* pkCuller);
	void		DrawSky(const NiCamera& kCamera, bool bRefectPass);

	CModelInstance*	GetModelInstance(const ModelRef& kModelRef);
	unsigned int GetRenderFrame() const { return m_uiRenderFrame; }
	unsigned int GetCollisionFrame() const { return m_uiCollisionFrame; }
	float GetTime() const { return m_fTime; }

	const CBox& GetBoundBox() const { return m_kBoundBox; }

    void DrawGrass(const NiPoint3& kOrigin);

	static CMap* Get() { return ms_pkMap; }

protected:
	NiFixedString m_kMapName;//地图名称
	unsigned int m_uiMapId;
	CBox m_kBoundBox;

	int		m_iTileFlags[TILE_COUNT][TILE_COUNT];
    CTile*  m_pkTiles[TILE_COUNT][TILE_COUNT];
	CTile*  m_pkCurrentTiles[CURRENT_TILE_SIZE];

	bool	m_bLoaded;

	unsigned int m_uiRenderFrame;
	unsigned int m_uiCollisionFrame;

	CWaterShader* m_pkWaterShaders[MAX_WATER_COUNT];

	float m_fTime;

	CSky m_kSky;

	CPathNodeManager m_kPathNodeManager;
    CGrassMesh m_kGrassMesh;

    CSoundEmitterManager m_kSoundEmitterManager;

	static CMap* ms_pkMap;

	const Festival_Flag m_Festival;
};