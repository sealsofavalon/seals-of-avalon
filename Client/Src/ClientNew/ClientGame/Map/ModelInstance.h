#pragma once

class CModelInstance : public NiMemObject
{
public:
	enum EntityFlag
	{
		EF_HIDDEN = 0x01, //是否隐藏
		EF_REFLECT = 0x02, //是否反射
		EF_NO_SHDOW = 0x04, //是否产生阴影
	};

	enum EntityType
	{
		ET_MODEL = 0, //场景模型
		ET_TRANSPORT, //传送门
		ET_TYPE_MAX,
	};

	CModelInstance(NiAVObject* pkSceneRoot)
		:m_pkSceneRoot(pkSceneRoot)
		,m_uiNumCollision(0)
		,m_uiNumRender(0)
		,m_bHidden(false)
		,m_bReflect(false)
		,m_bCollision(false)
		,m_bShouldUpdate(false)
		,m_uiType(ET_MODEL)
		,m_uiTransportID(0)
	{}

	NiAVObject* GetSceneRoot() const { return m_pkSceneRoot; }

	unsigned int GetNumCollision() const { return m_uiNumCollision; }
	void SetNumCollision(unsigned int uiNum) { m_uiNumCollision = uiNum; }

	unsigned int GetNumRender() const { return m_uiNumRender; }
	void SetNumRender(unsigned int uiNum) { m_uiNumRender = uiNum; }

	bool GetHidden() const { return m_bHidden; }
	void SetHidden(bool bHidden) { m_bHidden = bHidden; }

	bool GetReflect() const { return m_bReflect; }
	void SetReflect(bool bReflect) { m_bReflect = bReflect; }

	bool GetCollision() const { return m_bCollision; }
	void SetCollision(bool bCollision) { m_bCollision = bCollision; }

	bool GetShouldUpdate() const { return m_bShouldUpdate; } 
	void SetShouldUpdate(bool bShouldUpdate) { m_bShouldUpdate = bShouldUpdate; }

	unsigned int GetType() const { return m_uiType; }
	void SetType(unsigned int uiType) 
	{
		NIASSERT(uiType < ET_TYPE_MAX);
		m_uiType = uiType;
	}

	unsigned int GetTransportID() const { return m_uiTransportID; }
	void SetTransportID(unsigned int uiTransportID) { m_uiTransportID = uiTransportID; }

private:
	NiAVObject* m_pkSceneRoot;
	unsigned int m_uiNumCollision;
	unsigned int m_uiNumRender;
	bool m_bHidden; //是否隐藏
	bool m_bReflect; //是否反射
	bool m_bCollision; //是否有碰撞体
	bool m_bShouldUpdate; //是否需要更新

	unsigned int m_uiType; //类型
	unsigned int m_uiTransportID; //传送门ID
};