#pragma once

struct CPathNode
{
    NiPoint3 m_kTranslate;
	unsigned int m_uiLinkCount;
	unsigned int m_uiFirstLink;    
};

class CPathNodeManager : public NiMemObject
{
public:
    CPathNodeManager();
    virtual ~CPathNodeManager();

    void Load(const NiFixedString& kMapName);
	void Unload();

	//获取最近的Path Node, 如果场景没有Path Node 则返回NULL
	CPathNode* GetNearestPathNode(const NiPoint3& kPos) const;

	//通过ID 获取PathNode
	CPathNode* GetPathNode(unsigned int uiPathNodeId);

	//获取第idx个连接的PathNode
	CPathNode* GetLinkPathNode(CPathNode* pkPathNode, unsigned int idx, float& fLength);

	//计算每个连接的长度
	void CalcLinkLength();

	static CPathNodeManager* Get() { return ms_pkThis; }

private:
	unsigned int m_uiPathNodeCount;
	CPathNode* m_pkPathNodes;

	unsigned int m_uiTotalLinkCount;
	unsigned int* m_puiLinks;
	float* m_pfLinkLegnth;

    static CPathNodeManager* ms_pkThis;
};

#define g_pkPathNodeManager (CPathNodeManager::Get())