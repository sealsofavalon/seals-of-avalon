#include "StdAfx.h"
#include "ResourceManager.h"
#include "LightManager.h"
#include "Map/Map.h"
#include "Sky.h"

CSky::CSky()
	:m_kVisibleSet(16, 4)
	,m_kCuller(NULL)
{
	m_spZBuffer = NiNew NiZBufferProperty;
	m_spZBuffer->SetZBufferTest(false);
	m_spZBuffer->SetZBufferWrite(false);

	m_spMaterialProperty = NiNew NiMaterialProperty;
	m_spMaterial = NiSingleShaderMaterial::Create("FogMountain");
}

CSky::~CSky()
{
	g_ResMgr->FreeNif(m_spSceneRoot);
}

void CSky::Draw(const NiCamera& kCamera, bool bReflectPass)
{
	CMap* pkMap = CMap::Get();
	if(m_spSceneRoot && pkMap)
	{
		m_spSceneRoot->SetTranslate(kCamera.GetTranslate());
		if(!bReflectPass)
		{
			CLightManager* pkLightManager = CLightManager::Get();
			if(pkLightManager)
				m_spMaterialProperty->SetEmittance(pkLightManager->GetFogColor());

			if(m_spMountain)
				m_spMountain->SetAppCulled(false);
		}
		else
		{
			if(m_spMountain)
				m_spMountain->SetAppCulled(true);
		}

		m_spSceneRoot->Update(pkMap->GetTime()/30.f, !bReflectPass); //反射的时候不更新动画

		m_kVisibleSet.RemoveAll();
		NiDrawScene(const_cast<NiCamera*>(&kCamera), m_spSceneRoot, m_kCuller, &m_kVisibleSet); 
	}
}

void CSky::Load(const NiFixedString& kName)
{
	NiAVObject* pkSkydome = g_ResMgr->LoadNif(kName);
	g_ResMgr->FreeNif(m_spSceneRoot);
	m_spSceneRoot = pkSkydome;

	Process(m_spSceneRoot);

	m_spMountain = m_spSceneRoot->GetObjectByName("Mountain");
	if(m_spMountain == NULL)
		m_spMountain = m_spSceneRoot->GetObjectByName("Crater");
	
	ProcessMountain(m_spMountain);

	m_spSceneRoot->UpdateEffects();
	m_spSceneRoot->UpdateProperties();
	m_spSceneRoot->SetScale(0.1f);
	m_spSceneRoot->Update(0.f, false);
}

void CSky::Process(NiAVObject* pkAVObject)
{
	if(pkAVObject == NULL)
		return;

	if(NiIsKindOf(NiGeometry, pkAVObject))
	{
		NiZBufferProperty* pkZBuffer = (NiZBufferProperty*)pkAVObject->GetProperty(NiZBufferProperty::GetType());
		if(pkZBuffer)
		{
			pkZBuffer->SetZBufferTest(false);
			pkZBuffer->SetZBufferWrite(false);
		}
		else
		{
			pkAVObject->AttachProperty(m_spZBuffer);
		}
	}
	else if(NiIsKindOf(NiNode, pkAVObject))
	{
		NiNode* pkNode = (NiNode*)pkAVObject;
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ++ui)
			Process(pkNode->GetAt(ui));
	}
}

void CSky::ProcessMountain(NiAVObject* pkAVObject)
{
	if(pkAVObject == NULL)
		return;

	if(NiIsKindOf(NiGeometry, pkAVObject))
	{
		NiMaterialProperty* pkMaterialProperty = (NiMaterialProperty*)pkAVObject->GetProperty(NiMaterialProperty::GetType());
		if(pkMaterialProperty)
			pkAVObject->DetachProperty(pkMaterialProperty);

		pkAVObject->AttachProperty(m_spMaterialProperty);
		((NiGeometry*)pkAVObject)->ApplyAndSetActiveMaterial(m_spMaterial);
	}
	else if(NiIsKindOf(NiNode, pkAVObject))
	{
		NiNode* pkNode = (NiNode*)pkAVObject;
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ++ui)
			ProcessMountain(pkNode->GetAt(ui));
	}
}