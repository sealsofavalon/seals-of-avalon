#pragma once

class CSky : public NiMemObject
{
public:
	CSky();
	virtual ~CSky();

	void Draw(const NiCamera& kCamera, bool bReflectPass);
	void Load(const NiFixedString& kName);
	void Process(NiAVObject* pkAVObject);
	void ProcessMountain(NiAVObject* pkAVObject);

private:
	NiAVObjectPtr m_spSceneRoot;
	NiAVObjectPtr m_spMountain;
	NiCullingProcess m_kCuller;
	NiVisibleArray m_kVisibleSet;
	NiZBufferPropertyPtr m_spZBuffer;
	NiMaterialPropertyPtr m_spMaterialProperty;
	NiMaterialPtr m_spMaterial;
};