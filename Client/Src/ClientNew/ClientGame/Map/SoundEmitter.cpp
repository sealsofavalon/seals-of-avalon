#include "stdafx.h"
#include "SoundEmitter.h"
#include "ObjectManager.h"
#include "Map/Map.h"

void CSoundEmitter::Update(float fDeltaTime)
{
    if(m_spAudioSource == NULL)
        return;

    //间隔一定的时间再次播放
    if(m_spAudioSource->GetStatus() == AudioSource::DONE)
    {
        m_fTime += fDeltaTime;
        if( m_fTime > m_fTimeInterval 
         && m_uiCount < m_uiLoopCount )
        {
            m_fTime = 0.f;
            m_spAudioSource->Play();

            if(m_uiLoopCount < INFINITY_COUNT)
                ++m_uiCount;
        }
    }
}


CSoundEmitterManager* CSoundEmitterManager::ms_pkThis = NULL;
CSoundEmitterManager::CSoundEmitterManager()
    :m_kSoundEmitters(32, 4)
    ,m_uiFrame(0x12345678)
    ,m_pkLastChunk(NULL)
{
    NIASSERT(ms_pkThis == NULL);
    ms_pkThis = this;
}

CSoundEmitterManager::~CSoundEmitterManager()
{
    RemoveAll();
    ms_pkThis = NULL;
    m_pkLastChunk = NULL;
}

void CSoundEmitterManager::Update(float fDeltaTime)
{
    CMap* pkMap = CMap::Get();
    NIASSERT(pkMap != NULL && pkMap->IsLoaded());

    CheckChunkChanged();

    CSoundEmitter* pkSoundEmitter;
    for(unsigned int i = 0; i < m_kSoundEmitters.GetSize(); i++)
    {
        pkSoundEmitter = m_kSoundEmitters.GetAt(i);
        NIASSERT(pkSoundEmitter);

        pkSoundEmitter->Update(fDeltaTime);
    }
}

void CSoundEmitterManager::CheckChunkChanged()
{
	CMap* pkMap = CMap::Get();
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	if( !pkPlayer || !pkMap )
		return;

    CChunk* pkChunk;
    const NiPoint3& kPos = pkPlayer->GetPosition();
    pkChunk = pkMap->GetChunk(kPos.x, kPos.y);
    if(m_pkLastChunk == pkChunk)
        return;

    //更新音效
    m_pkLastChunk = pkChunk;

    ++m_uiFrame;
    NiPoint3 kCenter = kPos;
    if(pkChunk)
        kCenter = pkChunk->GetBasePoint() + NiPoint3(CHUNK_SIZE/2.f, CHUNK_SIZE/2.f, 0.f);

    for(int i = -4; i <= 4; ++i)
    {
        for(int j = -4; j <= 4; ++j)
        {
            pkChunk = pkMap->GetChunk(kCenter.x + i*CHUNK_SIZE, kCenter.y + j*CHUNK_SIZE);
            if(pkChunk == NULL)
                continue;

            for(unsigned int k = 0; k < pkChunk->GetSoundEmitterCount(); ++k)
            {
                AddSoundEmitter(pkChunk->GetSoundEmitter(k), kPos);
            }
        }
    }

    //清除多余的Sound
    CSoundEmitter* pkSoundEmitter;
    for(unsigned int i = 0; i < m_kSoundEmitters.GetSize(); i++)
    {
        pkSoundEmitter = m_kSoundEmitters.GetAt(i);
        if(m_uiFrame != pkSoundEmitter->GetFrame())
        {
            m_kSoundEmitters.SetAt(i, NULL);
            pkSoundEmitter->SetAudioSource(NULL);
        }
    }

    m_kSoundEmitters.Compact();

    //播放新加入的音效
    for(unsigned int i = 0; i < m_kSoundEmitters.GetSize(); i++)
    {
        pkSoundEmitter = m_kSoundEmitters.GetAt(i);
        if(pkSoundEmitter->GetAudioSource() == NULL)
        {
            NiTransform kTransform;
            kTransform.m_Rotate = NiMatrix3::IDENTITY;
            kTransform.m_fScale = 1.f;
            kTransform.m_Translate = pkSoundEmitter->GetTranslate();

            pkSoundEmitter->SetAudioSource(SYState()->IAudio->Play3DSound(pkSoundEmitter->GetFilename(), kTransform, pkSoundEmitter->GetMinDistance(), pkSoundEmitter->GetMaxDistance()));
        }
    }
}

void CSoundEmitterManager::AddSoundEmitter(CSoundEmitter* pkSoundEmitter, const NiPoint3& kCenter)
{
    pkSoundEmitter->SetFrame(m_uiFrame);

    //看是否已经存在
    for(unsigned int i = 0; i < m_kSoundEmitters.GetSize(); i++)
    {
        if(pkSoundEmitter == m_kSoundEmitters.GetAt(i))
            return;
    }

    //看是否文件名相同
    CSoundEmitter* pkOldSoundEmitter;
    for(unsigned int i = 0; i < m_kSoundEmitters.GetSize(); i++)
    {
        pkOldSoundEmitter = m_kSoundEmitters.GetAt(i);
        if(pkOldSoundEmitter->GetFilename() == pkSoundEmitter->GetFilename())
        {
            pkSoundEmitter->SetCount(pkOldSoundEmitter->GetCount());
            if( (pkOldSoundEmitter->GetTranslate() - kCenter).SqrLength() <= (pkSoundEmitter->GetTranslate() - kCenter).SqrLength() )
                continue;

            pkSoundEmitter->SetTime(pkOldSoundEmitter->GetTime());
            pkSoundEmitter->SetAudioSource(pkOldSoundEmitter->GetAudioSource());
            pkOldSoundEmitter->SetAudioSource(NULL);
            if(pkSoundEmitter->GetAudioSource())
            {
                NiTransform kTransform;
                kTransform.m_Rotate = NiMatrix3::IDENTITY;
                kTransform.m_fScale = 1.f;
                kTransform.m_Translate = pkSoundEmitter->GetTranslate();
                pkSoundEmitter->GetAudioSource()->SetLocalTransform(kTransform);
            }

            m_kSoundEmitters.SetAt(i, pkSoundEmitter);
            return;
        }
    }

    pkSoundEmitter->SetTime(0.f);
    m_kSoundEmitters.Add(pkSoundEmitter);
}

void CSoundEmitterManager::RemoveAll()
{
    for(unsigned int i = 0; i < m_kSoundEmitters.GetSize(); i++)
    {
        m_kSoundEmitters.GetAt(i)->SetAudioSource(NULL);
    }

    m_kSoundEmitters.RemoveAll();
}