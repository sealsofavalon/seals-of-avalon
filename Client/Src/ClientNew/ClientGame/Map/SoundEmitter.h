#ifndef SOUND_EMITTER_H
#define SOUND_EMITTER_H

#include "AudioInterface.h"
#define INFINITY_COUNT 10000
class CSoundEmitter : public NiMemObject
{
public:
    CSoundEmitter()
        :m_uiCount(0)
        ,m_fTime(0.f)
    {}

    void Update(float fDeltaTime);

    const NiFixedString& GetFilename() const { return m_kFilename; }
    void SetFilename(NiFixedString& kFilename) { m_kFilename = kFilename; }

    const NiPoint3& GetTranslate() const { return m_kTranslate; }
    void SetTranslate(const NiPoint3& kTranslate) { m_kTranslate = kTranslate; }

    float GetTimeInterval() const { return m_fTimeInterval; }
    void SetTimeInterval(float fTimeInterval) { m_fTimeInterval = fTimeInterval; }

    unsigned int GetLoopCount() const { return m_uiLoopCount; }
    void SetLoopCount(unsigned int uiLoopCount) { m_uiLoopCount = uiLoopCount; }

    float GetMinDistance() const { return m_fMinDistance; }
    void SetMinDistance(float fMinDistance) { m_fMinDistance = fMinDistance; }

    float GetMaxDistance() const { return m_fMaxDistance; }
    void SetMaxDistance(float fMaxDistance) { m_fMaxDistance = fMaxDistance; }

    unsigned int GetFrame() const { return m_uiFrame; }
    void SetFrame(unsigned int uiFrame) { m_uiFrame = uiFrame; }

    void SetAudioSource(AudioSourcePtr spAudioSource) { m_spAudioSource = spAudioSource; }
    AudioSourcePtr GetAudioSource() { return m_spAudioSource; }

    unsigned int GetCount() const { return m_uiCount; }
    void SetCount(unsigned int uiCount) { m_uiCount = uiCount; }

    float GetTime() const { return m_fTime; }
    void SetTime(float fTime) { m_fTime = fTime; }

private:
    NiFixedString m_kName;
    NiFixedString m_kFilename; //文件名
    NiPoint3 m_kTranslate; //位置
    float m_fTimeInterval; //时间间隔
    unsigned int m_uiLoopCount;
    float m_fMinDistance;
    float m_fMaxDistance;

    unsigned int m_uiCount; //已经播放的次数
    float m_fTime; //当前时间
    AudioSourcePtr m_spAudioSource;
    unsigned int m_uiFrame;
};

class CSoundEmitterManager : public NiMemObject
{
public:
    CSoundEmitterManager();
    ~CSoundEmitterManager();

    void Update(float fDeltaTime);

    void CheckChunkChanged();

    void AddSoundEmitter(CSoundEmitter* pkSoundEmitter, const NiPoint3& kCenter);
    void RemoveAll();

    static CSoundEmitterManager* Get() { return ms_pkThis; }

private:
    NiTPrimitiveArray<CSoundEmitter*> m_kSoundEmitters;
    unsigned int m_uiFrame;
    void* m_pkLastChunk;

    static CSoundEmitterManager* ms_pkThis;
};

#define g_pSoundEmitterManager (CSoundEmitterManager::Get())

#endif