
#include "StdAfx.h"

#include "TagStream.h"

#include "ResourceManager.h"
#include "ModelInstance.h"
#include "Chunk.h"

#ifdef NIDEBUG
//#define EMPTY_MAP_OBJ
#endif 


NiFixedString CTagStream::ms_SceneGraphComponent;
NiFixedString CTagStream::ms_TransformationComponent;

bool CTagStream::ReadFromStream(NiBinaryStream* pkStream)
{
	NIASSERT(pkStream);

	m_uiPos = 0;
	m_uiEnd = 0;
	
	if( pkStream->Read(&m_uiTag, sizeof(unsigned int)) == 0 )
		return false;

	pkStream->Read(&m_uiEnd, sizeof(unsigned int));


	if( m_uiEnd > m_uiAllocSize )
	{
		NiFree(m_pBuffer);

		m_uiAllocSize = m_uiEnd;
		m_pBuffer = NiAlloc(char, m_uiAllocSize);
	}

	unsigned int uiRead = pkStream->Read(m_pBuffer, m_uiEnd);
	NIASSERT( uiRead == m_uiEnd );

	return true;
}

bool CTagStream::ReadFixedString(NiFixedString& kString)
{
	kString = 0;

	if( m_uiPos >= m_uiEnd )
		return false;

	kString = &m_pBuffer[m_uiPos];
	m_uiPos += kString.GetLength() + 1;

	return true;
}

CModelInstance* CTagStream::ReadModelInstance()
{
	NiFixedString kName;
	if( !ReadFixedString(kName) )
		return NULL;

	unsigned int uiFlags = 0;
	unsigned int uiType = CModelInstance::ET_MODEL;
	if(GetVersion() < MakeVersion(1, 0))
	{
		unsigned char ucFlags;
		Read(&ucFlags, sizeof(ucFlags));
		uiFlags = ucFlags;
	}
	else
	{
		Read(&uiFlags, sizeof(uiFlags));
		Read(&uiType, sizeof(uiType));//物件类型
	}

	unsigned int uiTransportID = 0;
	if(uiType == CModelInstance::ET_TRANSPORT)
	{
		Read(&uiTransportID, sizeof(uiTransportID));
	}


	NiFixedString kNifFilePath;
	NiPoint3 kTranslate;
	NiMatrix3 kRotation;
	float fScale = 1.f;

	unsigned int uiComponentCount;
	Read(&uiComponentCount, sizeof(unsigned int));

	for( unsigned int i = 0; i < uiComponentCount; i++ )
	{
		NiFixedString kComponentName;
		ReadFixedString(kComponentName);

		if( kComponentName == ms_SceneGraphComponent )
		{
			ReadFixedString(kNifFilePath);
		}
		else if( kComponentName == ms_TransformationComponent )
		{
			Read(&kTranslate, sizeof(kTranslate));
			Read(&kRotation, sizeof(kRotation));
			Read(&fScale, sizeof(fScale));
		}
	}

#ifndef EMPTY_MAP_OBJ
	
	//装载Nif
	NiAVObject* pkAVObject = g_ResMgr->LoadNif((const char*)kNifFilePath);

	kTranslate.x += m_fBaseX;
	kTranslate.y += m_fBaseY;

	pkAVObject->SetTranslate(kTranslate);
	pkAVObject->SetRotate(kRotation);
	pkAVObject->SetScale(fScale);

	CModelInstance* pkModelInstance = NiNew CModelInstance(pkAVObject);
	pkModelInstance->SetHidden((uiFlags&CModelInstance::EF_HIDDEN) != 0);
	pkModelInstance->SetReflect((uiFlags&CModelInstance::EF_REFLECT) != 0);
	pkModelInstance->SetType(uiType);
	pkModelInstance->SetTransportID(uiTransportID);

	return pkModelInstance;
#else 
	return NULL;
#endif 
}

void CTagStream::_SDMInit()
{
	ms_SceneGraphComponent = "Scene Graph";
	ms_TransformationComponent = "Transformation";
}

void CTagStream::_SDMShutdown()
{
	ms_SceneGraphComponent = NULL;
	ms_TransformationComponent = NULL;
}

