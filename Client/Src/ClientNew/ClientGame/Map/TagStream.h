#pragma once

class CModelInstance;
class CTagStream : public NiMemStream
{
public:
	CTagStream()
		:m_uiVersion(0)
	{
	}

	bool ReadFromStream(NiBinaryStream* pkStream);

	bool ReadFixedString(NiFixedString& kString);
	CModelInstance* ReadModelInstance();
	unsigned int GetTag() const { return m_uiTag; }
	void SetBase(float x, float y) { m_fBaseX = x; m_fBaseY = y; }

	void* GetCurrentBuffer() { return &m_pBuffer[m_uiPos]; }

	bool IsEOF() const { return m_uiPos >= m_uiEnd; }

	unsigned int GetVersion() const { return m_uiVersion; }
	void SetVersion(unsigned int uiVersion) { m_uiVersion = uiVersion; }

	static void _SDMInit();
	static void _SDMShutdown();

private:
	int	m_uiTag;
	float m_fBaseX;
	float m_fBaseY;
	unsigned int m_uiVersion;

	static NiFixedString ms_SceneGraphComponent;
	static NiFixedString ms_TransformationComponent;
};