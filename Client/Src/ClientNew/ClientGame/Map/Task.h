#ifndef TASK_H
#define TASK_H

class CTask : public NiMemObject
{
public:
    CTask()
        :m_bFinished(false)
    {}

    virtual ~CTask() {};

    virtual void Begin() = 0;
    virtual void Run() = 0;
    virtual void End() { m_bFinished = true; }

    virtual bool NeedDelete() const { return false; }

    bool IsFinished() const { return m_bFinished; }

protected:
    bool m_bFinished;
};

#endif