#include "StdAfx.h"
#include "PackFile.h"
#include "Task.h"
#include "TaskManager.h"
//#include "../../PackTool/PackLib/PackInterface.h"
 
CTaskManager* CTaskManager::ms_pkThis = NULL;
void CTaskManager::Init()
{
    m_bStop = false;
    ms_pkThis = this;

    for(int i = 0; i < THIREAD_COUNT; ++i)
    {
        m_pkThreads[i] = NiThread::Create(&m_kTaskProcedure);
        //m_pkThreads[i]->SetPriority(NiThread::BELOW_NORMAL);
        m_pkThreads[i]->Resume();
    }

	m_pkAudioThreads = NiThread::Create(&m_kAudioTaskProcedure);
	m_pkAudioThreads->Resume();
}

void CTaskManager::Shutdown()
{
    m_bStop = true;

    for(int i = 0; i < THIREAD_COUNT; ++i)
    {
        if(m_pkThreads[i] && m_pkThreads[i]->GetStatus() == NiThread::RUNNING)
            m_pkThreads[i]->WaitForCompletion();

        NiDelete m_pkThreads[i];
        m_pkThreads[i] = NULL;
    }

    Tick();

    ms_pkThis = NULL;
}

void CTaskManager::Tick() //主线程中执行
{
    CTask* pkTask;
    bool bNeedDelete;
    while(pkTask = PopFinishTask())
    {
        bNeedDelete = pkTask->NeedDelete();
        pkTask->End(); //End()里面有可能会删除pkTask

        if(bNeedDelete)
            NiDelete pkTask;
    }
}

#define ENABLE_ASYNC_TASK
void CTaskManager::PushTask(CTask* pkTask) //主线程中执行
{
    pkTask->Begin();

#ifdef ENABLE_ASYNC_TASK
    m_kTasksCS.Lock();
    m_kTasks.push_back(pkTask);
    m_kTasksCS.Unlock();

#else
    //不使用多线程
    pkTask->Run();
    PushFinishTask(pkTask);
#endif
}

void CTaskManager::PushAudioTask(CTask* pkTask) //主线程中执行
{
	pkTask->Begin();
	m_kAudioTasksCS.Lock();
	m_kAudioTasks.push_back(pkTask);
	m_kAudioTasksCS.Unlock();

}



CTask* CTaskManager::PopTask() //任务线程中执行
{
    CTask* pTask = NULL;
    m_kTasksCS.Lock();
    if(!m_kTasks.empty())
    {
        pTask = m_kTasks.front();
        m_kTasks.pop_front();
    }
    m_kTasksCS.Unlock();

    return pTask;
}


CTask* CTaskManager::PopAudioTask() //任务线程中执行
{
	CTask* pTask = NULL;
	m_kAudioTasksCS.Lock();
	if(!m_kAudioTasks.empty())
	{
		pTask = m_kAudioTasks.front();
		m_kAudioTasks.pop_front();
	}
	m_kAudioTasksCS.Unlock();

	return pTask;
}


void CTaskManager::PushFinishTask(CTask* pkTask) //任务线程中执行
{
    m_kFinishTasksCS.Lock();
    m_kFinishTasks.push_back(pkTask);
    m_kFinishTasksCS.Unlock();
}

CTask* CTaskManager::PopFinishTask() //主线程中执行
{
    CTask* pTask = NULL;

    m_kFinishTasksCS.Lock();
    if(!m_kFinishTasks.empty())
    {
        pTask = m_kFinishTasks.front();
        m_kFinishTasks.pop_front();
    }
    m_kFinishTasksCS.Unlock();

    return pTask;
}

void CTaskManager::TaskThread(CTaskManager* pkThis)
{
    CPackFile::Init( false );

    CTask* pkTask;
	int index = 0;
    while(!pkThis->GetStop())
    {
        pkTask = pkThis->PopTask();
        if(pkTask)
        {
            pkTask->Run();
            pkThis->PushFinishTask(pkTask);
			if( ++index % 10 == 0 )
				NiSleep(10);
        }
        else
        {
            NiSleep(10);
        }
    }

    CPackFile::Shutdown( false );
}

void CTaskManager::TaskAudioThread(CTaskManager* pkThis)
{
	CPackFile::Init( true );

	CTask* pkTask;
	while(!pkThis->GetStop())
	{
		pkTask = pkThis->PopAudioTask();
		if(pkTask)
		{
			try
			{
				pkTask->Run();
			}
			catch(...)
			{
			}
			pkThis->PushFinishTask(pkTask);
			NiSleep(10);
		}
		else
		{
			NiSleep(10);
		}
	}

	CPackFile::Shutdown( true );
}