#ifndef TASK_MANAGER_H
#define TASK_MANAGER_H

#define g_pkTaskManager (CTaskManager::Get())
#define THIREAD_COUNT 1
class CTask;
class CTaskManager : public NiMemObject
{
    class CTaskProcedure : public NiThreadProcedure
    {
    public:
        virtual unsigned int ThreadProcedure(void* pvArg)
        {
            CTaskManager::TaskThread(g_pkTaskManager);
            return 0;
        }
    };

	class CAudioTaskProcedure : public NiThreadProcedure
	{
	public:
		virtual unsigned int ThreadProcedure(void* pvArg)
		{
			CTaskManager::TaskAudioThread(g_pkTaskManager);
			return 0;
		}
	};

public:
    void Init();
    void Shutdown();

    void Tick();

    void PushTask(CTask* pkTask);
    CTask* PopTask();

	void PushAudioTask(CTask* pkTask);
	CTask* PopAudioTask();

    void PushFinishTask(CTask* pkTask);
    CTask* PopFinishTask();

    bool GetStop() const { return m_bStop; }

    static CTaskManager* Get() { return ms_pkThis; }
    static void TaskThread(CTaskManager* pkThis);
	static void TaskAudioThread(CTaskManager* pkThis);

private:
    std::list<CTask*> m_kTasks; //未处理的队列
    NiCriticalSection m_kTasksCS;

	std::list<CTask*> m_kAudioTasks; //未处理的队列
	NiCriticalSection m_kAudioTasksCS;

    std::list<CTask*> m_kFinishTasks; //已经处理完毕的队列
    NiCriticalSection m_kFinishTasksCS;

    CTaskProcedure m_kTaskProcedure;
	CAudioTaskProcedure m_kAudioTaskProcedure;
    NiThread* m_pkThreads[THIREAD_COUNT];
	NiThread* m_pkAudioThreads;
    bool m_bStop;
    
    static CTaskManager* ms_pkThis;
};

#endif //TASK_MANAGER_H