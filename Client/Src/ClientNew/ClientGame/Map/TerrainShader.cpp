
#include "StdAfx.h"

#include "TerrainShader.h"
#include "Chunk.h"
#include "Map.h"
#include "LightManager.h"
#include "Utils\\ClientUtils.h"

CTerrainGeomData::CTerrainGeomData(unsigned short usVertices, NiPoint3* pkVertex,
        NiPoint3* pkNormal, NiColorA* pkColor, NiPoint2* pkTexture, 
        unsigned short usNumTextureSets, DataFlags eNBTMethod,
        unsigned short usTriangles, unsigned short* pusTriList)
		:NiTriShapeData(usVertices, pkVertex, pkNormal, pkColor, pkTexture, 
		usNumTextureSets, eNBTMethod, usTriangles, pusTriList)
{
}

CTerrainGeomData::~CTerrainGeomData()
{
	//地形共用以下数据, 防止被删除
	m_pusTriList = NULL;
}

NiSourceTexture* CAlphaTexture::Create(NiPixelData* pkPixelData)
{
	NIASSERT(pkPixelData);

	CAlphaTexture* pkTexture = NiNew CAlphaTexture;

	NiTexture::FormatPrefs::PixelLayout ePixelLayout;
	if( pkPixelData->GetPixelFormat().GetBitsPerPixel() == 8 )
		ePixelLayout = NiTexture::FormatPrefs::SINGLE_COLOR_8;
	else if( pkPixelData->GetPixelFormat().GetBitsPerPixel() == 16 )
		ePixelLayout = NiTexture::FormatPrefs::HIGH_COLOR_16;
	else
		ePixelLayout = NiTexture::FormatPrefs::TRUE_COLOR_32;

	pkTexture->m_kFormatPrefs.m_ePixelLayout = ePixelLayout;
	pkTexture->m_kFormatPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::SMOOTH;
	pkTexture->m_kFormatPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
	pkTexture->m_spSrcPixelData = pkPixelData;
	
	pkTexture->CreateRendererData();

	return pkTexture;
}

NiShaderDeclaration* CTerrainShader::ms_pkDecl;

CTerrainShader::CTerrainShader()
{
	SetName("TerrainShader");

	// This is the best (and only) implementation of this shader
	m_bIsBestImplementation = true;

    m_spVertexShader = CreateVertexShader("Shaders\\Terrain.hlsl", "TerrainVS");
    
    m_pkPixelShaders[0] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS0");
    m_pkPixelShaders[1] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS1");
    m_pkPixelShaders[2] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS2");
    m_pkPixelShaders[3] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS3");
    m_pkPixelShaders[4] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS4");
    m_pkPixelShaders[5] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS5");

	m_pkShadowPixelShaders[0] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS0_S");
	m_pkShadowPixelShaders[1] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS1_S");
	m_pkShadowPixelShaders[2] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS2_S");
	m_pkShadowPixelShaders[3] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS3_S");
	m_pkShadowPixelShaders[4] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS4_S");
	m_pkShadowPixelShaders[5] = CreatePixelShader("Shaders\\Terrain.hlsl", "TerrainPS5_S");
}

CTerrainShader::~CTerrainShader()
{
    NiDelete m_pkPixelShaders[0];
    NiDelete m_pkPixelShaders[1];
    NiDelete m_pkPixelShaders[2];
    NiDelete m_pkPixelShaders[3];
    NiDelete m_pkPixelShaders[4];
    NiDelete m_pkPixelShaders[5];

	NiDelete m_pkShadowPixelShaders[0];
	NiDelete m_pkShadowPixelShaders[1];
	NiDelete m_pkShadowPixelShaders[2];
	NiDelete m_pkShadowPixelShaders[3];
	NiDelete m_pkShadowPixelShaders[4];
	NiDelete m_pkShadowPixelShaders[5];
}

unsigned int CTerrainShader::PreProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    m_pkChunk = CMap::Get()->GetChunk(kWorld.m_Translate.x, kWorld.m_Translate.y);
	return 0;
}

unsigned int CTerrainShader::PostProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    m_pkChunk = NULL;

	pkGeometry->GetName();

    return 0;
}

unsigned int CTerrainShader::SetupTransformations(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        const NiSkinPartition::Partition* pkPartition, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects, 
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    m_pkD3DRenderState->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
	m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	m_pkD3DRenderState->SetRenderState(D3DRS_ZENABLE, TRUE);
	m_pkD3DRenderState->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
	m_pkD3DRenderState->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
    m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	m_pkD3DRenderState->SetRenderState(D3DRS_FOGENABLE, TRUE);

    m_pkD3DRenderer->SetModelTransform(kWorld);


    m_pkD3DRenderState->SetVertexShader(m_spVertexShader->GetShaderHandle());

    //Vertex Shader
    float fValues[7][4];
    D3DXMATRIX kD3DWorld = m_pkD3DRenderer->GetD3DMat();
    fValues[0][0] = kD3DWorld._41;
    fValues[0][1] = kD3DWorld._42;
    fValues[0][2] = kD3DWorld._43;
    fValues[0][3] = 0.f;

    D3DXMATRIX kD3DView = m_pkD3DRenderer->GetD3DView();
    D3DXMATRIX kD3DProj = m_pkD3DRenderer->GetD3DProj();
    D3DXMATRIX kTempMat = kD3DView * kD3DProj;
    D3DXMatrixTranspose(&kTempMat, &kTempMat);

    memcpy(fValues[1], &kTempMat, sizeof(kTempMat));

	CLightManager* pkLightManager = CLightManager::Get();
	const NiColor& kDiffuseColor = pkLightManager->GetDiffuseColor();
	const NiColor& kAmbientColor = pkLightManager->GetAmbientColor();

    fValues[5][0] = kDiffuseColor.r;
    fValues[5][1] = kDiffuseColor.g;
    fValues[5][2] = kDiffuseColor.b;
    fValues[5][3] = 1.f;

    fValues[6][0] = kAmbientColor.r;
    fValues[6][1] = kAmbientColor.g;
    fValues[6][2] = kAmbientColor.b;
    fValues[6][3] = 1.f;

    m_pkD3DRenderState->SetVertexShaderConstantF(0, &fValues[0][0], 7);

	//pixel shader
    unsigned int uiNumLayers = m_pkChunk->GetNumLayers();
    NIASSERT(uiNumLayers <= MAX_LAYERS && uiNumLayers > 0);
	if(m_pkChunk->HasShadow())
	{
		m_pkD3DRenderState->SetPixelShader(m_pkShadowPixelShaders[uiNumLayers - 1]->GetShaderHandle());
	}
	else
	{
		m_pkD3DRenderState->SetPixelShader(m_pkPixelShaders[uiNumLayers - 1]->GetShaderHandle());
	}

	NiDX9TextureManager* pkDX9TextureManager = m_pkD3DRenderer->GetTextureManager();
	bool bS0Mipmap;
	bool bNonPow2;
	bool bChanged; 
	D3DBaseTexturePtr pkD3DTexture;
	CTileTexture* pkTileTexture;
	for(unsigned int ui = 0; ui < uiNumLayers; ++ui)
	{
		pkTileTexture = m_pkChunk->GetLayerTexture(ui);
		pkD3DTexture = pkDX9TextureManager->PrepareTextureForRendering(pkTileTexture->spTexture, bChanged, bS0Mipmap, bNonPow2);
		m_pkD3DRenderState->SetTexture(ui, pkD3DTexture);
		m_pkD3DRenderState->SetSamplerState(ui, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
		m_pkD3DRenderState->SetSamplerState(ui, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
		m_pkD3DRenderState->SetSamplerState(ui, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		m_pkD3DRenderState->SetSamplerState(ui, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		m_pkD3DRenderState->SetSamplerState(ui, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

		fValues[ui][0] = pkTileTexture->fUScale;
		fValues[ui][1] = pkTileTexture->fVScale;
	}

	NiTexture* pkTexture = m_pkChunk->GetAlphaTexture1();
	if(pkTexture)
	{
		pkD3DTexture = pkDX9TextureManager->PrepareTextureForRendering(pkTexture, bChanged, bS0Mipmap, bNonPow2);
		m_pkD3DRenderState->SetTexture(6, pkD3DTexture);
		m_pkD3DRenderState->SetSamplerState(6, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
		m_pkD3DRenderState->SetSamplerState(6, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
		m_pkD3DRenderState->SetSamplerState(6, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		m_pkD3DRenderState->SetSamplerState(6, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		m_pkD3DRenderState->SetSamplerState(6, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
	}

	pkTexture = m_pkChunk->GetAlphaTexture2();
	if(pkTexture)
	{
		pkD3DTexture = pkDX9TextureManager->PrepareTextureForRendering(pkTexture, bChanged, bS0Mipmap, bNonPow2);
		m_pkD3DRenderState->SetTexture(7, pkD3DTexture);
		m_pkD3DRenderState->SetSamplerState(7, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
		m_pkD3DRenderState->SetSamplerState(7, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
		m_pkD3DRenderState->SetSamplerState(7, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		m_pkD3DRenderState->SetSamplerState(7, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		m_pkD3DRenderState->SetSamplerState(7, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
	}

    m_pkD3DRenderState->SetPixelShaderConstantF(0, &fValues[0][0], uiNumLayers);
    return 0;
}

NiGeometryData::RendererData* CTerrainShader::PrepareGeometryForRendering(
    NiGeometry* pkGeometry, const NiSkinPartition::Partition* pkPartition,
    NiGeometryData::RendererData* pkRendererData, 
    const NiPropertyState* pkState)
{
    NiGeometryData* pkData = pkGeometry->GetModelData();
    NiGeometryBufferData* pkBuffData = (NiGeometryBufferData*)pkData->GetRendererData();

	if(ms_pkDecl == NULL)
	{
		ms_pkDecl = m_pkD3DRenderer->CreateShaderDeclaration(1, 2);
		ms_pkDecl->IncRefCount();

		ms_pkDecl->SetEntry(0, NiShaderDeclaration::SHADERPARAM_NI_POSITION, NiShaderDeclaration::SPTYPE_FLOAT3, 0);
		ms_pkDecl->SetEntry(0, NiShaderDeclaration::SHADERPARAM_NI_NORMAL, NiShaderDeclaration::SPTYPE_FLOAT3, 1);
	}

	m_pkD3DRenderer->PackGeometryBuffer(pkBuffData, pkData, NULL, (NiD3DShaderDeclaration*)ms_pkDecl);

    NIASSERT(pkBuffData->GetStreamCount() == 2);

    m_pkD3DDevice->SetStreamSource(0, pkBuffData->GetVBChip(0)->GetVB(), 0, sizeof(NiPoint3));
    m_pkD3DDevice->SetStreamSource(1, pkBuffData->GetVBChip(1)->GetVB(), 0, sizeof(NiPoint3));

    m_pkD3DDevice->SetIndices(pkBuffData->GetIB());
    m_pkD3DRenderState->SetDeclaration(((NiD3DShaderDeclaration*)ms_pkDecl)->GetD3DDeclaration());

    return pkBuffData;
}

void CTerrainShader::_SDMInit()
{
    ms_pkDecl = NULL;
}

void CTerrainShader::_SDMShutdown()
{
    if(ms_pkDecl)
    {
        ms_pkDecl->DecRefCount();
        ms_pkDecl = NULL;
    }
}