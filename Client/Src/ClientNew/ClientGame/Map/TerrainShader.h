#pragma once

#include "Chunk.h"

class CTerrainGeomData : public NiTriShapeData
{
public:
	CTerrainGeomData(unsigned short usVertices, NiPoint3* pkVertex,
        NiPoint3* pkNormal, NiColorA* pkColor, NiPoint2* pkTexture, 
        unsigned short usNumTextureSets, DataFlags eNBTMethod,
        unsigned short usTriangles, unsigned short* pusTriList);

	virtual ~CTerrainGeomData();
};

class CAlphaTexture : public NiSourceTexture
{
public:
	static NiSourceTexture* Create(NiPixelData* pkPixelData);
};

class CTerrainShader : public NiD3DShaderInterface
{
public:
	CTerrainShader();
	virtual ~CTerrainShader();
	
	virtual unsigned int PreProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound);

	virtual unsigned int PostProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound);

    virtual unsigned int SetupTransformations(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        const NiSkinPartition::Partition* pkPartition, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects, 
        const NiTransform& kWorld, const NiBound& kWorldBound);

    virtual NiGeometryData::RendererData* PrepareGeometryForRendering(
        NiGeometry* pkGeometry, const NiSkinPartition::Partition* pkPartition,
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState);

    // Advance to the next pass.
	virtual unsigned int FirstPass() { return 1; }
	virtual unsigned int NextPass() { return 0; }

 	static void _SDMInit();
	static void _SDMShutdown();

private:
    static NiShaderDeclaration* ms_pkDecl;

    NiD3DVertexShaderPtr m_spVertexShader;
    NiD3DPixelShader* m_pkPixelShaders[MAX_LAYERS]; //地形上面没有阴影
	NiD3DPixelShader* m_pkShadowPixelShaders[MAX_LAYERS]; //地形上面有阴影

    CChunk* m_pkChunk; //所属的Chunk
};