#pragma once

#include "Chunk.h"
#include "TileLoader.h"
#include "Festival_Flag.h"

struct CTileTexture : public NiMemObject
{
	NiTexturePtr spTexture;
	float fUScale;
	float fVScale;
};

class CModelInstance;
class CTile : public NiNode
{
public:
	NiDeclareRTTI;

public:
	CTile(int x, int y, Festival_Flag Festival);
	virtual ~CTile();

	int GetX() const { return m_iX; }
	int GetY() const { return m_iY; }

	//异步资源线程里面执行
	bool Load();
	
	void BeginBackLoad();

	void OnLoaded();
    bool IsLoaded() const { return m_kTileLoader.IsFinished(); }

	CChunk* GetChunk(float x, float y);

	inline unsigned int GetTextureCount() const { return m_pkTextures.GetSize(); } 
	inline CTileTexture* GetTexture(unsigned int uiId)
	{
		if( uiId < m_pkTextures.GetSize() )
			return m_pkTextures.GetAt(uiId);

		return NULL;
	}

	inline CModelInstance* GetModelInstance(unsigned int uiIndex)
	{
		if(uiIndex < m_pkModels.GetSize())
			return m_pkModels.GetAt(uiIndex);

		return NULL;
	}

	inline unsigned int GetVersion() const { return m_uiVersion; }

    inline void SetNeedDelete(bool bNeedDelete) { m_bNeedDelete = bNeedDelete; }
    inline bool IsNeedDelete() const { return m_bNeedDelete; }
protected:
	NiFile* GetFile(const char* Filestr);
private:

	int	m_iX;
	int	m_iY;
	unsigned int m_uiVersion;
	
	NiNodePtr m_spModelNode; //所有模型组成的节点, 方便调用GameBryo 的相关功能
	NiTPrimitiveArray<CModelInstance*> m_pkModels; //模型的列表, 包含其他信息

	//地形贴图列表
	NiTPrimitiveArray<CTileTexture*> m_pkTextures;
	CChunk	m_kChunks[CHUNK_COUNT][CHUNK_COUNT];

	CTileLoader m_kTileLoader;

	NiTStringPointerMap<NiAVObject*> m_pObjectUpdateMap;
    bool m_bNeedDelete; //所属的地图已经删除, 异步装载完成后需要直接删除

	const Festival_Flag m_Festival ;
};

