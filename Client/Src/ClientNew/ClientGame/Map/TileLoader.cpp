#include "StdAfx.h"
#include "Tile.h"
#include "TileLoader.h"

void CTileLoader::Begin()
{
}

void CTileLoader::Run()
{
    m_pkTile->Load();
}

void CTileLoader::End()
{
    CTask::End();

    if(m_pkTile->IsNeedDelete())
        NiDelete m_pkTile; //��ͼ�Ѿ�ж��
    else
        m_pkTile->OnLoaded();
}