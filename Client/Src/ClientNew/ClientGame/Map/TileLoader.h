#pragma once

#include "Task.h"

class CTile;
class CTileLoader : public CTask
{
public:
	CTileLoader(CTile* pkTile)
        : m_pkTile(pkTile)
    {
    }

    virtual void Begin();
    virtual void Run();
    virtual void End();

protected:
	CTile* m_pkTile;
	bool m_bLoaded;
};