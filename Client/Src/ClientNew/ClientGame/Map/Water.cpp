#include "StdAfx.h"

#include "Tile.h"
#include "Water.h"
#include "LightManager.h"
#include "Map.h"

NiPoint3* CWater::ms_pkNormals;
NiPoint2* CWater::ms_pkTextureCoords;
CWaterShader* CWater::ms_pkDefaultWaterShader;

void CWater::_SDMInit()
{
	ms_pkNormals = NiNew NiPoint3[WATER_VERTEX_COUNT*WATER_VERTEX_COUNT];
	ms_pkTextureCoords = NiNew NiPoint2[WATER_VERTEX_COUNT*WATER_VERTEX_COUNT];
	for( int x = 0; x < WATER_VERTEX_COUNT; x++ )
	{
		for( int y = 0; y < WATER_VERTEX_COUNT; y++ )
		{
			ms_pkNormals[x*WATER_VERTEX_COUNT + y] = NiPoint3(0.f, 0.f, 1.f);
			ms_pkTextureCoords[x*WATER_VERTEX_COUNT + y] = NiPoint2(float(x)/2.f, float(y)/2.f);
		}
	}

	ms_pkDefaultWaterShader = NiNew CWaterShader;
	ms_pkDefaultWaterShader->IncRefCount();

	ms_pkDefaultWaterShader->SetTexturePathName("Data\\Textures\\Water\\river\\lake_a.");
	ms_pkDefaultWaterShader->Init();
}

void CWater::_SDMShutdown()
{
	NiDelete[] ms_pkNormals;
	NiDelete[] ms_pkTextureCoords;

	if( ms_pkDefaultWaterShader )
		ms_pkDefaultWaterShader->DecRefCount();
}

CWater::CWater(float x, float y, CTile* pkTile)
	:m_kBasePoint(x, y, 0.f)
	,m_pkTile(pkTile)
	,m_uiShaderIndex(0)
{
	m_pkPositionList = NiNew NiPoint3[WATER_VERTEX_COUNT*WATER_VERTEX_COUNT];
	m_pusTriList = NiAlloc(unsigned short, (WATER_VERTEX_COUNT - 1)*(WATER_VERTEX_COUNT-1)*6);
	
	memset(m_pusTriList, 0, (WATER_VERTEX_COUNT - 1)*(WATER_VERTEX_COUNT-1)*6*sizeof(unsigned short));
	memset(m_acFlags, 0, sizeof(m_acFlags));

	for( int x = 0; x < WATER_VERTEX_COUNT; x++ )
	{
		for( int y = 0; y < WATER_VERTEX_COUNT; y++ )
		{
			m_pkPositionList[x*WATER_VERTEX_COUNT + y] = NiPoint3(float(x * UNIT_SIZE), float(y * UNIT_SIZE), 0.f);
		}
	}
}

CWater::~CWater()
{
	if( !m_spGeometry )
	{
		NiDelete[] m_pkPositionList;
		NiFree(m_pusTriList);
	}
}

void CWater::Load(NiBinaryStream* pkStream)
{
	pkStream->Read(m_acFlags, sizeof(m_acFlags));

	float afHeights[WATER_VERTEX_COUNT][WATER_VERTEX_COUNT];

	pkStream->Read(afHeights, sizeof(afHeights));

	for( int x = 0; x < WATER_VERTEX_COUNT; x++ )
	{
		for( int y = 0; y < WATER_VERTEX_COUNT; y++ )
		{
			m_pkPositionList[x*WATER_VERTEX_COUNT + y].z = afHeights[x][y];
		}
	}

	//兼容之前版本的水面
	if( pkStream->Read(&m_uiShaderIndex, sizeof(m_uiShaderIndex)) != sizeof(m_uiShaderIndex) )
	{
		m_uiShaderIndex = 0;
	}

	BuildIndices();
	CreateGeometry();
}

float CWater::GetHeight(float x, float y)
{
	float h;
	if( GetHeight(x, y, h) )
		return h;

	return 10.f;
}

bool CWater::GetHeight(float x, float y, float& h)
{
	x -= m_kBasePoint.x;
	y -= m_kBasePoint.y;

	unsigned int X = unsigned int(x)/UNIT_SIZE;
	unsigned int Y = unsigned int(y)/UNIT_SIZE;

	if( X >= WATER_VERTEX_COUNT || Y >= WATER_VERTEX_COUNT )
		return false;

	if( !NIBOOL_IS_TRUE(m_acFlags[X][Y]) )
		return false;

	h = m_pkPositionList[X * VERTEX_COUNT + Y].z;

	return true;
}

bool CWater::GetFlag(float x, float y)
{
	x -= m_kBasePoint.x;
	y -= m_kBasePoint.y;

	unsigned int X = unsigned int(x)/UNIT_SIZE;
	unsigned int Y = unsigned int(y)/UNIT_SIZE;

	if( X >= WATER_VERTEX_COUNT - 1 || Y >= WATER_VERTEX_COUNT - 1 )
		return false;

	return NIBOOL_IS_TRUE(m_acFlags[X][Y]);
}

//重新建立 Indices
void CWater::BuildIndices()
{
	m_usTriangles = 0;
	m_fAVGHeight = 0.f;

	unsigned short usIdx;
	unsigned short* pusTriList = m_pusTriList;
	for( int x = 0; x < WATER_VERTEX_COUNT - 1; x++ )
	{
		for( int y = 0; y < WATER_VERTEX_COUNT - 1; y++ )
		{
			if( NIBOOL_IS_TRUE(m_acFlags[x][y]) )
			{
				m_usTriangles += 2;

				usIdx = unsigned short(x *  WATER_VERTEX_COUNT + y);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;

				usIdx = unsigned short((x + 1) *  WATER_VERTEX_COUNT + y);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;

				usIdx = unsigned short(x *  WATER_VERTEX_COUNT + y + 1);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;

				usIdx = unsigned short(x *  WATER_VERTEX_COUNT + y + 1);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;

				usIdx = unsigned short((x + 1) *  WATER_VERTEX_COUNT + y);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;

				usIdx = unsigned short((x + 1) *  WATER_VERTEX_COUNT + y + 1);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;
			}
		}
	}

	if(m_usTriangles > 0)
		m_fAVGHeight /= (m_usTriangles*3);
}


void CWater::CreateGeometry()
{
	CWaterTriData* pkWaterTriData = NiNew CWaterTriData(
		WATER_VERTEX_COUNT * WATER_VERTEX_COUNT,
		m_pkPositionList, 
		ms_pkNormals, 
		NULL,
		ms_pkTextureCoords,
		1,
		NiGeometryData::NBT_METHOD_NONE,
		m_usTriangles,
		m_pusTriList);

	m_spGeometry = NiNew NiTriShape(pkWaterTriData);
	m_spGeometry->SetTranslate(m_kBasePoint);

	CMap* pkMap = CMap::Get();
	NIASSERT(pkMap);

	CWaterShader* pkShader = GetShader();
	m_spGeometry->AttachProperty(pkShader->GetAlphaProperty());
	m_spGeometry->SetShader(pkShader);

	m_pkTile->AttachChild(m_spGeometry);

	m_spGeometry->UpdateProperties();
	m_spGeometry->UpdateEffects();
	m_spGeometry->Update(0.f, false);
}

CWaterShader* CWater::GetShader() const
{
    CMap* pkMap = CMap::Get();
    NIASSERT(pkMap);

    CWaterShader* pkWaterShader = pkMap->GetWaterShader(m_uiShaderIndex);
    if(pkWaterShader)
        return pkWaterShader;

    return ms_pkDefaultWaterShader;
}

void CWater::BuildVisibleSet(CCullingProcess* pkCuller)
{
	const NiFrustumPlanes& kPlanes = pkCuller->GetFrustumPlanes();
	unsigned int i;
	CBox kBox;
	m_spGeometry->GetWorldBound().GetAABB(kBox.m_kMin, kBox.m_kMax);
	for (i = 0; i < NiFrustumPlanes::MAX_PLANES; ++i)
	{
		int iSide = kBox.WhichSide(kPlanes.GetPlane(i));

		if (iSide == NiPlane::NEGATIVE_SIDE)
		{
			// The object is not visible since it is on the negative
			// side of the plane.
			break;
		}
	}

	if( i == NiFrustumPlanes::MAX_PLANES )
		pkCuller->OnWaterVisible(this);
}