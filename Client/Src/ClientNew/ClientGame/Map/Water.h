#pragma once

#define WATER_VERTEX_COUNT 9

class CWaterTriData : public NiTriShapeData
{
public:
    CWaterTriData(unsigned short usVertices, NiPoint3* pkVertex,
        NiPoint3* pkNormal, NiColorA* pkColor, NiPoint2* pkTexture,
        unsigned short usNumTextureSets, NiGeometryData::DataFlags eNBTMethod,
        unsigned short usTriangles, unsigned short* pusTriList)
		:NiTriShapeData(usVertices, pkVertex, pkNormal, pkColor, pkTexture,
		usNumTextureSets, eNBTMethod, usTriangles, pusTriList)
	{
	}

	virtual ~CWaterTriData()
	{
		m_pkTexture = NULL;
		m_pkNormal = NULL;
	}
};

class CTile;
class CWaterShader;
class CWater : public NiMemObject
{
private:
	NiPoint3 m_kBasePoint;
	CTile* m_pkTile;

	NiTriShapePtr m_spGeometry;
	
	//顶点
	NiPoint3* m_pkPositionList;

	//顶点索引
	unsigned short* m_pusTriList;

	//3角型的个数
	unsigned short	m_usTriangles;

	//是否有水
	NiBool m_acFlags[WATER_VERTEX_COUNT - 1][WATER_VERTEX_COUNT - 1];

	unsigned int m_uiShaderIndex; //使用哪个Shader来渲染
	float m_fAVGHeight; //水面的平均高度

	static NiPoint3* ms_pkNormals;
	static NiPoint2* ms_pkTextureCoords;
	static CWaterShader* ms_pkDefaultWaterShader;

private:
	void BuildIndices();
	void CreateGeometry();

public:
	CWater(float x, float y, CTile* pkTile);
	~CWater();

	void Load(NiBinaryStream* pkStream);

	float GetHeight(float x, float y);
	bool GetHeight(float x, float y, float& h);
	bool GetFlag(float x, float y);
	unsigned int GetShaderIndex() const { return m_uiShaderIndex; }

	NiGeometry* GetGeometry() { return m_spGeometry; }
	CWaterShader* CWater::GetShader() const;
	float GetAVGHeight() const { return m_fAVGHeight; }
	const NiPoint3& GetBasePoint() const { return m_kBasePoint; }
	void BuildVisibleSet(CCullingProcess* pkCuller);

	static void _SDMInit();
	static void _SDMShutdown();
	static CWaterShader* GetDefaultWaterShader() { return ms_pkDefaultWaterShader; }
};