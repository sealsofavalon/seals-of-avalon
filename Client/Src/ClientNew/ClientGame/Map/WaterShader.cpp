#include "stdafx.h"

#include "WaterShader.h"
#include "Water.h"
#include "Map.h"
#include "ResourceManager.h"
#include "Utils/ClientUtils.h"
#include "ClientState.h"
#include "TerrainShader.h"

NiShaderDeclaration* CWaterShader::ms_pkDecl;

NiD3DVertexShader* CWaterShader::ms_pkVertexShader;
NiD3DPixelShader* CWaterShader::ms_pkPixelShader;
NiD3DPixelShader* CWaterShader::ms_pkAdditivePixelShader;

NiD3DVertexShader* CWaterShader::ms_pkRefractVertexShader;
NiD3DPixelShader* CWaterShader::ms_pkRefractPixelShader;

NiD3DVertexShader* CWaterShader::ms_pkRRVertexShader;
NiD3DPixelShader* CWaterShader::ms_pkRRPixelShader;

bool CWaterShader::ms_bRefractPass;
bool CWaterShader::ms_bRRWaterEnable;

NiTexture* CWaterShader::ms_pkNormalTexture;
NiTexture* CWaterShader::ms_pkDuDvTexture;
NiRenderedTexture* CWaterShader::ms_pkRefractTexture;
NiRenderedTexture* CWaterShader::ms_pkRefractAlphaTexture;
NiRenderTargetGroup* CWaterShader::ms_pkRefractAlphaTarget;
NiRenderedTexture* CWaterShader::ms_pkReflectTexture;
NiRenderTargetGroup* CWaterShader::ms_pkReflectTarget;
NiPixelData* CWaterShader::ms_pkWaterEdgeMapDatas;
NiSourceTexture* CWaterShader::ms_pkWaterEdgeMapTextures;

#define DEFAULTMAP_SIZE 8

CWaterShader::CWaterShader()
	:m_uiTexureCount(0)
    ,m_pkCurrentTexture(NULL)
	,m_fAlpha(0.8f)
	,m_bAlphaBlend(true)
	,m_bAdditiveLight(true)
	,m_bRR(false)
	,m_fUVScale(1.f)
	,m_fSpeed(1.f)
	,m_pkChunk(NULL)
{
}

CWaterShader::~CWaterShader()
{
	Destroy();
}

unsigned int CWaterShader::PreProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
	m_pkChunk = CMap::Get()->GetChunk(kWorld.m_Translate.x, kWorld.m_Translate.y);

	return 0;
}

unsigned int CWaterShader::PostProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    if(!GetRR())
    {
        m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
        m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
    }

	m_pkChunk = NULL;
    return 0;
}

unsigned int CWaterShader::SetupTransformations(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        const NiSkinPartition::Partition* pkPartition, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects, 
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    m_pkD3DRenderer->SetModelTransform(kWorld);
	m_pkD3DRenderState->SetRenderState(D3DRS_FOGENABLE, TRUE);
    if(GetRR())
    {
        if(ms_bRefractPass)
        {
            SetupRefractRender();
        }
        else
        {
            SetupRRRender(kWorld.m_Translate);
        }
    }
    else
    {
        SetupRender();
    }

    m_pkD3DRenderState->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
    m_pkD3DRenderState->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);

    return 0;
}

NiGeometryData::RendererData* CWaterShader::PrepareGeometryForRendering(
    NiGeometry* pkGeometry, const NiSkinPartition::Partition* pkPartition,
    NiGeometryData::RendererData* pkRendererData, 
    const NiPropertyState* pkState)
{
   NiGeometryData* pkData = pkGeometry->GetModelData();
   NiGeometryBufferData* pkBuffData = (NiGeometryBufferData*)pkData->GetRendererData();

   if(ms_pkDecl == NULL)
   {
	   ms_pkDecl = m_pkD3DRenderer->CreateShaderDeclaration(1, 1);
	   ms_pkDecl->IncRefCount();

	   ms_pkDecl->SetEntry(0, NiShaderDeclaration::SHADERPARAM_NI_POSITION, NiShaderDeclaration::SPTYPE_FLOAT3, 0);
   }
   m_pkD3DRenderer->PackGeometryBuffer(pkBuffData, pkData, NULL, (NiD3DShaderDeclaration*)ms_pkDecl);


   // Set the streams
   m_pkD3DDevice->SetStreamSource(0, pkBuffData->GetVBChip(0)->GetVB(), 0, pkBuffData->GetVertexStride(0));
   m_pkD3DDevice->SetStreamSource(1, 0, 0, 0);
   m_pkD3DDevice->SetIndices(pkBuffData->GetIB());
   m_pkD3DRenderState->SetDeclaration(((NiD3DShaderDeclaration*)ms_pkDecl)->GetD3DDeclaration());

    return pkBuffData;
}

bool CWaterShader::Init()
{
	char acName[256];
	NiSprintf(acName, sizeof(acName), "water shader %x %d", this, NiRand());//防止重名
	SetName(acName);

	// This is the best (and only) implementation of this shader
	m_bIsBestImplementation = true;

	//textures
	for(int i = 0; i < MAX_TEXTURE_COUNT; i++)
	{
		m_pkTextures[i] = NULL;
	}

	//textures
	m_uiTexureCount = MAX_TEXTURE_COUNT;
	char acFilename[512];
	NiFile* pkFile;
	for(int i = 0; i < MAX_TEXTURE_COUNT; i++)
	{
		NiSprintf(acFilename, sizeof(acFilename), "%s%d.dds", (const char*)m_kTexturePathName, i + 1);
		pkFile = NiFile::GetFile(acFilename, NiFile::READ_ONLY);
		if (!pkFile || !(*pkFile))
		{
			NiDelete pkFile;
			m_uiTexureCount = i;
			break;
		}

		NiDelete pkFile;
	}

    m_spAlphaProperty = NiNew NiAlphaProperty;
	if(m_bAlphaBlend)
		m_spAlphaProperty->SetAlphaBlending(true);
	else
		m_spAlphaProperty->SetAlphaBlending(false);

	return true;
}

void CWaterShader::Destroy()
{
	for(int i = 0; i < MAX_TEXTURE_COUNT; i++)
	{
		if(m_pkTextures[i])
		{
			g_ResMgr->FreeTexture(m_pkTextures[i]);
			m_pkTextures[i] = NULL;;
		}
	}

	m_pkCurrentTexture = NULL;
}

bool CWaterShader::GetRR() const
{
    return m_bRR && ms_bRRWaterEnable;
}

void CWaterShader::SetCurrentTexture(unsigned int uiIndex)
{
	//NIASSERT(uiIndex < m_uiTexureCount);
	if(m_pkTextures[uiIndex] == NULL)
	{
		char acFilename[512];
		NiSprintf(acFilename, sizeof(acFilename), "%s%d.dds", (const char*)m_kTexturePathName, uiIndex + 1);
		m_pkTextures[uiIndex] = g_ResMgr->LoadTexture(acFilename);
	}

    m_pkCurrentTexture = m_pkTextures[uiIndex];
}

bool CWaterShader::Load(CTagStream& kStream)
{
	float fAlpha;
	kStream.Read(&fAlpha, sizeof(fAlpha));
	SetAlpha(fAlpha);

	NiBool bTemp;
	kStream.Read(&bTemp, sizeof(bTemp));
	SetAlphaBlend(NIBOOL_IS_TRUE(bTemp));

	kStream.Read(&bTemp, sizeof(bTemp));
	SetAdditiveLight(NIBOOL_IS_TRUE(bTemp));

	kStream.Read(&bTemp, sizeof(bTemp));
	SetRR(NIBOOL_IS_TRUE(bTemp));

	float fUVScale;
	kStream.Read(&fUVScale, sizeof(fUVScale));
	SetUVScale(fUVScale);

	float fSpeed;
	kStream.Read(&fSpeed, sizeof(fSpeed));
    fSpeed = 1.f;
	SetSpeed(fSpeed);

	NiFixedString kTexturePathName;
	kStream.ReadFixedString(kTexturePathName);
	SetTexturePathName(kTexturePathName);

	return true;
}

void CWaterShader::Update(const NiColor& kAmbientColor, const NiColor& kDiffuseColor)
{
	int iIndex;
	if(m_uiTexureCount > 1)
	{
		iIndex = int( NiGetCurrentTimeInSec() * ((1000.f * m_fSpeed) / m_uiTexureCount) );
		iIndex = iIndex % m_uiTexureCount;
	}
	else
		iIndex = 0;

	SetCurrentTexture(iIndex);

    m_kAmbientColor = kAmbientColor;
    m_kDiffuseColor = kDiffuseColor;
}

void CWaterShader::SetupRender()
{
    m_pkD3DRenderState->SetVertexShader(ms_pkVertexShader->GetShaderHandle());

    if(m_bAdditiveLight)
        m_pkD3DRenderState->SetPixelShader(ms_pkAdditivePixelShader->GetShaderHandle());
    else
        m_pkD3DRenderState->SetPixelShader(ms_pkPixelShader->GetShaderHandle());

    float fValues[9][4];
    D3DXMATRIX kD3DWorld = m_pkD3DRenderer->GetD3DMat();
    fValues[0][0] = kD3DWorld._41;
    fValues[0][1] = kD3DWorld._42;
    fValues[0][2] = kD3DWorld._43;
    fValues[0][3] = 0.f;

    D3DXMATRIX kD3DView = m_pkD3DRenderer->GetD3DView();
    D3DXMATRIX kD3DProj = m_pkD3DRenderer->GetD3DProj();
    D3DXMATRIX kTempMat = kD3DView * kD3DProj;
    D3DXMatrixTranspose(&kTempMat, &kTempMat);

    memcpy(fValues[1], &kTempMat, sizeof(kTempMat));

    fValues[5][0] = m_kDiffuseColor.r;
    fValues[5][1] = m_kDiffuseColor.g;
    fValues[5][2] = m_kDiffuseColor.b;
    fValues[5][3] = m_fAlpha;

    fValues[6][0] = m_kAmbientColor.r;
    fValues[6][1] = m_kAmbientColor.g;
    fValues[6][2] = m_kAmbientColor.b;
    fValues[6][3] = m_fAlpha;

    fValues[7][0] = m_fUVScale;

    m_pkD3DRenderState->SetVertexShaderConstantF(0, (float*)fValues, 9);

    bool bS0Mipmap;
    bool bNonPow2;
    bool bChanged; 

	NiTexture* pChunkDepthText = m_pkChunk->GetWaterEdgeMapTexture();

    D3DBaseTexturePtr pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(m_pkCurrentTexture, bChanged, bS0Mipmap, bNonPow2);

    m_pkD3DRenderState->SetTexture(0, pkD3DTexture);

	if ( pChunkDepthText )
	{
		pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(pChunkDepthText, bChanged, bS0Mipmap, bNonPow2);
		m_pkD3DRenderState->SetTexture(1, pkD3DTexture);
	}
	else
	{
		pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(ms_pkWaterEdgeMapTextures, bChanged, bS0Mipmap, bNonPow2);
		m_pkD3DRenderState->SetTexture(1, pkD3DTexture);
	}

    m_pkD3DRenderState->SetTexture(2, NULL);

    m_pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_ADDRESSU, D3DTADDRESS_WRAP);
    m_pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_ADDRESSV, D3DTADDRESS_WRAP);
    m_pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_MAGFILTER, D3DTEXF_LINEAR);
    m_pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_MINFILTER, D3DTEXF_LINEAR);
    m_pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_MIPFILTER, D3DTEXF_LINEAR);

	if ( pChunkDepthText )
	{
		m_pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_ADDRESSU, D3DTADDRESS_CLAMP);
		m_pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_ADDRESSV, D3DTADDRESS_CLAMP);
		m_pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_MAGFILTER, D3DTEXF_LINEAR);
		m_pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_MINFILTER, D3DTEXF_LINEAR);
		m_pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_MIPFILTER, D3DTEXF_LINEAR);
	}

    if(m_bAlphaBlend)
    {
        m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
        m_pkD3DRenderState->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
        m_pkD3DRenderState->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
        m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
    }
    else
    {
        m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
        m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
    }

}

void CWaterShader::SetupRefractRender()
{
    m_pkD3DRenderState->SetVertexShader(ms_pkRefractVertexShader->GetShaderHandle());
    m_pkD3DRenderState->SetPixelShader(ms_pkRefractPixelShader->GetShaderHandle());

    float fValues[5][4];
    D3DXMATRIX kD3DWorld = m_pkD3DRenderer->GetD3DMat();
    fValues[0][0] = kD3DWorld._41;
    fValues[0][1] = kD3DWorld._42;
    fValues[0][2] = kD3DWorld._43;
    fValues[0][3] = 0.f;

    D3DXMATRIX kD3DView = m_pkD3DRenderer->GetD3DView();
    D3DXMATRIX kD3DProj = m_pkD3DRenderer->GetD3DProj();
    D3DXMATRIX kTempMat = kD3DView * kD3DProj;
    D3DXMatrixTranspose(&kTempMat, &kTempMat);

    memcpy(fValues[1], &kTempMat, sizeof(kTempMat));
    m_pkD3DRenderState->SetVertexShaderConstantF(0, (float*)fValues, 5);
}

void CWaterShader::SetupRRRender(const NiPoint3& kWaterBase)
{
    m_pkD3DRenderState->SetVertexShader(ms_pkRRVertexShader->GetShaderHandle());
    m_pkD3DRenderState->SetPixelShader(ms_pkRRPixelShader->GetShaderHandle());

    float fValues[11][4];
    D3DXMATRIX kD3DWorld = m_pkD3DRenderer->GetD3DMat();
    fValues[0][0] = kD3DWorld._41;
    fValues[0][1] = kD3DWorld._42;
    fValues[0][2] = kD3DWorld._43;
    fValues[0][3] = 0.f;

    D3DXMATRIX kD3DView = m_pkD3DRenderer->GetD3DView();
    D3DXMATRIX kD3DProj = m_pkD3DRenderer->GetD3DProj();
    D3DXMATRIX kTempMat = kD3DView * kD3DProj;
    D3DXMatrixTranspose(&kTempMat, &kTempMat);

    memcpy(fValues[1], &kTempMat, sizeof(kTempMat));

    fValues[5][0] = m_kDiffuseColor.r;
    fValues[5][1] = m_kDiffuseColor.g;
    fValues[5][2] = m_kDiffuseColor.b;
    fValues[5][3] = m_fAlpha;

    fValues[6][0] = m_kAmbientColor.r;
    fValues[6][1] = m_kAmbientColor.g;
    fValues[6][2] = m_kAmbientColor.b;
    fValues[6][3] = m_fAlpha;

    fValues[7][0] = m_fUVScale;
	fValues[8][0] = CMap::Get()->GetTime();//获取当前时间

    fValues[9][0] = NiFmod(kWaterBase.x, 512.f);
    fValues[9][1] = NiFmod(kWaterBase.y, 512.f);

    D3DMATRIX kViewMat = m_pkD3DRenderer->GetInvView();
    fValues[10][0] = kViewMat._31;
    fValues[10][1] = kViewMat._32;
    fValues[10][2] = kViewMat._33;
    fValues[10][3] = kViewMat._34;

    m_pkD3DRenderState->SetVertexShaderConstantF(0, (float*)fValues, 11);

    bool bS0Mipmap;
    bool bNonPow2;
    bool bChanged;
    D3DBaseTexturePtr pkD3DTexture;
    pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(ms_pkRefractTexture, bChanged, bS0Mipmap, bNonPow2);
    m_pkD3DRenderState->SetTexture(0, pkD3DTexture);

    pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(ms_pkReflectTexture, bChanged, bS0Mipmap, bNonPow2);
    m_pkD3DRenderState->SetTexture(1, pkD3DTexture);

    pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(ms_pkRefractAlphaTexture, bChanged, bS0Mipmap, bNonPow2);
    m_pkD3DRenderState->SetTexture(2, pkD3DTexture);

    pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(ms_pkNormalTexture, bChanged, bS0Mipmap, bNonPow2);
    m_pkD3DRenderState->SetTexture(3, pkD3DTexture);

    pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(ms_pkDuDvTexture, bChanged, bS0Mipmap, bNonPow2);
    m_pkD3DRenderState->SetTexture(4, pkD3DTexture);

	NiTexture* pChunkDepthText = m_pkChunk->GetWaterEdgeMapTexture();

	if ( pChunkDepthText )
	{
		pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(pChunkDepthText, bChanged, bS0Mipmap, bNonPow2);
		m_pkD3DRenderState->SetTexture(5, pkD3DTexture);
	}
	else
	{
		pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(ms_pkWaterEdgeMapTextures, bChanged, bS0Mipmap, bNonPow2);
		m_pkD3DRenderState->SetTexture(5, pkD3DTexture);
	}

	if(m_bAlphaBlend)
	{
		m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		m_pkD3DRenderState->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		m_pkD3DRenderState->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	}
	else
	{
		m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	}
}

void CWaterShader::_SDMInit()
{
	ms_pkDecl = NULL;

	ms_bRefractPass = false;

    ms_pkVertexShader = CreateVertexShader("Shaders\\Water.hlsl", "WaterVS");
	ms_pkVertexShader->IncRefCount();

    ms_pkPixelShader = CreatePixelShader("Shaders\\Water.hlsl", "WaterPS");
	ms_pkPixelShader->IncRefCount();

    ms_pkAdditivePixelShader = CreatePixelShader("Shaders\\Water.hlsl", "AdditiveWaterPS");
	ms_pkAdditivePixelShader->IncRefCount();

	ms_pkNormalTexture = NULL;
    ms_pkDuDvTexture = NULL;
	ms_pkRefractTexture = NULL;
	ms_pkReflectTexture = NULL;
	ms_pkReflectTarget = NULL;

    ms_pkRefractAlphaTexture = NULL;
    ms_pkRefractAlphaTarget = NULL;

	ms_pkRefractVertexShader = NULL;
	ms_pkRefractPixelShader = NULL;
	ms_pkRRVertexShader = NULL;
	ms_pkRRPixelShader = NULL;

	if(ClientState->GetWaterReflect())
	{
        SetWaterRR(true);
	}


	ms_pkWaterEdgeMapDatas = NiNew NiPixelData(DEFAULTMAP_SIZE, DEFAULTMAP_SIZE, NiPixelFormat::ARGB8888, 1, 1);
	ms_pkWaterEdgeMapDatas->IncRefCount();
	DWORD* pdwChannels = (DWORD*)ms_pkWaterEdgeMapDatas->GetPixels(0, 0);

	int iBufferSize = sizeof(DWORD)*DEFAULTMAP_SIZE*DEFAULTMAP_SIZE; // 4 bits per pixel
	DWORD* pdwBuffer = NiAlloc(DWORD, iBufferSize);

	for( int i = 0; i < DEFAULTMAP_SIZE; i++ )
	{
		for( int j = 0; j < DEFAULTMAP_SIZE; j++ )
		{
			pdwChannels[i*DEFAULTMAP_SIZE+j] = 0xffffffff;
		}
	}

	if ( !ms_pkWaterEdgeMapTextures )
	{
		ms_pkWaterEdgeMapTextures = CAlphaTexture::Create( ms_pkWaterEdgeMapDatas );
		ms_pkWaterEdgeMapTextures->IncRefCount();
	}
}

void CWaterShader::_SDMShutdown()
{
	if(ms_pkDecl)
	{
		ms_pkDecl->DecRefCount();
		ms_pkDecl = NULL;
	}

	ms_pkVertexShader->DecRefCount();
	ms_pkVertexShader = NULL;

	ms_pkPixelShader->DecRefCount();
	ms_pkPixelShader = NULL;

	ms_pkAdditivePixelShader->DecRefCount();
	ms_pkAdditivePixelShader = NULL;

    SetWaterRR(false);
}

void CWaterShader::ReCreate()
{
	if(ms_pkRefractTexture)
	{
		ms_pkRefractTexture->DecRefCount();
		ms_pkRefractTexture = NULL;
	}

	if(ms_pkRefractAlphaTexture)
	{
		ms_pkRefractAlphaTexture->DecRefCount();
		ms_pkRefractAlphaTexture = NULL;
	}

	if(ms_pkRefractAlphaTarget)
	{
		ms_pkRefractAlphaTarget->DecRefCount();
		ms_pkRefractAlphaTarget = NULL;
	}

	if(ms_pkReflectTexture)
	{
		ms_pkReflectTexture->DecRefCount();
		ms_pkReflectTexture = NULL;
	}

	if(ms_pkReflectTarget)
	{
		ms_pkReflectTarget->DecRefCount();
		ms_pkReflectTarget = NULL;
	}

	if ( ms_pkWaterEdgeMapDatas )
	{
		ms_pkWaterEdgeMapDatas->DecRefCount();
		ms_pkWaterEdgeMapDatas = NULL;
	}

	if ( ms_pkWaterEdgeMapTextures )
	{
		ms_pkWaterEdgeMapTextures->DecRefCount();
		ms_pkWaterEdgeMapTextures = NULL;
	}


	NiRenderer* pkRenderer = NiRenderer::GetRenderer();
	NIASSERT(pkRenderer);

	NiRenderTargetGroup* pkDefaultRenderTarget = pkRenderer->GetDefaultRenderTargetGroup();
	NIASSERT(pkDefaultRenderTarget);

	unsigned int uiWidth = pkDefaultRenderTarget->GetWidth(0);
	unsigned int uiHeight = pkDefaultRenderTarget->GetHeight(0);


	NiTexture::FormatPrefs kPrefs;
	kPrefs.m_ePixelLayout = NiTexture::FormatPrefs::TRUE_COLOR_32;
	kPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
	kPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::NONE;

	//折射
	ms_pkRefractTexture = NiRenderedTexture::Create(uiWidth, uiHeight, pkRenderer, kPrefs);
	ms_pkRefractTexture->IncRefCount();

	ms_pkRefractAlphaTexture = NiRenderedTexture::Create(uiWidth, uiHeight, pkRenderer, kPrefs);
	ms_pkRefractAlphaTexture->IncRefCount();

	ms_pkRefractAlphaTarget = NiRenderTargetGroup::Create(ms_pkRefractAlphaTexture->GetBuffer(), pkRenderer, true, true);
	ms_pkRefractAlphaTarget->IncRefCount();

	//反射
	ms_pkReflectTexture = NiRenderedTexture::Create(uiWidth, uiHeight, pkRenderer, kPrefs);
	ms_pkReflectTexture->IncRefCount();

	NiDepthStencilBuffer* pkDSB = NiDepthStencilBuffer::Create(uiWidth, uiHeight, pkRenderer, NiPixelFormat::STENCILDEPTH824);
	ms_pkReflectTarget = NiRenderTargetGroup::Create(ms_pkReflectTexture->GetBuffer(), pkRenderer, pkDSB);
	ms_pkReflectTarget->IncRefCount();



	ms_pkWaterEdgeMapDatas = NiNew NiPixelData(DEFAULTMAP_SIZE, DEFAULTMAP_SIZE, NiPixelFormat::ARGB8888, 1, 1);
	ms_pkWaterEdgeMapDatas->IncRefCount();
	DWORD* pdwChannels = (DWORD*)ms_pkWaterEdgeMapDatas->GetPixels(0, 0);

	int iBufferSize = sizeof(DWORD)*DEFAULTMAP_SIZE*DEFAULTMAP_SIZE; // 4 bits per pixel
	DWORD* pdwBuffer = NiAlloc(DWORD, iBufferSize);

	for( int i = 0; i < DEFAULTMAP_SIZE; i++ )
	{
		for( int j = 0; j < DEFAULTMAP_SIZE; j++ )
		{
			pdwChannels[i*DEFAULTMAP_SIZE+j] = 0xffffffff;
		}
	}

	if ( !ms_pkWaterEdgeMapTextures )
	{
		ms_pkWaterEdgeMapTextures = CAlphaTexture::Create( ms_pkWaterEdgeMapDatas );
		ms_pkWaterEdgeMapTextures->IncRefCount();
	}
}

void CWaterShader::SetWaterRR(bool bEnable)
{
    if(bEnable)
    {
        if(ms_bRRWaterEnable)
            return;

        ms_bRRWaterEnable = true;

        ms_pkRefractVertexShader = CreateVertexShader("Shaders\\Water.hlsl", "RefractWaterVS");
        ms_pkRefractVertexShader->IncRefCount();

        ms_pkRefractPixelShader = CreatePixelShader("Shaders\\Water.hlsl", "RefractWaterPS");
        ms_pkRefractPixelShader->IncRefCount();

        ms_pkRRVertexShader = CreateVertexShader("Shaders\\Water.hlsl", "RRWaterVS");
        ms_pkRRVertexShader->IncRefCount();

        ms_pkRRPixelShader = CreatePixelShader("Shaders\\Water.hlsl", "RRWaterPS");
        ms_pkRRPixelShader->IncRefCount();

        ms_pkNormalTexture = g_ResMgr->LoadTexture("Data\\Textures\\Water\\normal.bmp");
        ms_pkDuDvTexture = g_ResMgr->LoadTexture("Data\\Textures\\Water\\dudv.bmp");

		ReCreate();
    }
    else
    {
        if(!ms_bRRWaterEnable)
            return;

        ms_bRRWaterEnable = false;

        if(ms_pkRefractVertexShader)
        {
            ms_pkRefractVertexShader->DecRefCount();
            ms_pkRefractVertexShader = NULL;
        }

        if(ms_pkRefractPixelShader)
        {
            ms_pkRefractPixelShader->DecRefCount();
            ms_pkRefractPixelShader = NULL;
        }

        if(ms_pkRRVertexShader)
        {
            ms_pkRRVertexShader->DecRefCount();
            ms_pkRRVertexShader = NULL;
        }

        if(ms_pkRRPixelShader)
        {
            ms_pkRRPixelShader->DecRefCount();
            ms_pkRRPixelShader = NULL;
        }

        if(ms_pkNormalTexture)
        {
            g_ResMgr->FreeTexture(ms_pkNormalTexture);
            ms_pkNormalTexture = NULL;
        }

        if(ms_pkDuDvTexture)
        {
            g_ResMgr->FreeTexture(ms_pkDuDvTexture);
            ms_pkDuDvTexture = NULL;
        }

        if(ms_pkRefractTexture)
        {
            ms_pkRefractTexture->DecRefCount();
            ms_pkRefractTexture = NULL;
        }

        if(ms_pkRefractAlphaTexture)
        {
            ms_pkRefractAlphaTexture->DecRefCount();
            ms_pkRefractAlphaTexture = NULL;
        }

        if(ms_pkRefractAlphaTarget)
        {
            ms_pkRefractAlphaTarget->DecRefCount();
            ms_pkRefractAlphaTarget = NULL;
        }

        if(ms_pkReflectTexture)
        {
            ms_pkReflectTexture->DecRefCount();
            ms_pkReflectTexture = NULL;
        }

        if(ms_pkReflectTarget)
        {
            ms_pkReflectTarget->DecRefCount();
            ms_pkReflectTarget = NULL;
        }

		if ( ms_pkWaterEdgeMapTextures )
		{
			ms_pkWaterEdgeMapTextures->DecRefCount();
			ms_pkWaterEdgeMapTextures = NULL;
		}
    }
}
