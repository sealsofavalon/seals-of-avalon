#ifndef WATER_SHADER_H
#define WATER_SHADER_H

#include "Chunk.h"
#include "TagStream.h"

#define MAX_TEXTURE_COUNT 30 //最多有多少张贴图
#define MAX_WATER_COUNT 4 //一张地图最多是4种水面

class CWaterShader : public NiD3DShaderInterface
{
public:
	CWaterShader();
	virtual ~CWaterShader();

	virtual unsigned int PreProcessPipeline(NiGeometry* pkGeometry, 
		const NiSkinInstance* pkSkin, 
		NiGeometryData::RendererData* pkRendererData, 
		const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
		const NiTransform& kWorld, const NiBound& kWorldBound);

	virtual unsigned int PostProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound);

    virtual unsigned int SetupTransformations(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        const NiSkinPartition::Partition* pkPartition, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects, 
        const NiTransform& kWorld, const NiBound& kWorldBound);

    virtual NiGeometryData::RendererData* PrepareGeometryForRendering(
        NiGeometry* pkGeometry, const NiSkinPartition::Partition* pkPartition,
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState);

    // Advance to the next pass.
    virtual unsigned int FirstPass() { return 1; }
    virtual unsigned int NextPass() { return 0; }

	void SetCurrentTexture(unsigned int uiIndex);

	float GetAlpha() const { return m_fAlpha; }
	void SetAlpha(float fAlpha) { m_fAlpha = fAlpha; }
	
	bool GetAlphaBlend() const { return m_bAlphaBlend; }
	void SetAlphaBlend(bool bAlphaBlend) { m_bAlphaBlend = bAlphaBlend; }

	bool GetAdditiveLight() const { return m_bAdditiveLight; }
	void SetAdditiveLight(bool bAdditive) { m_bAdditiveLight = bAdditive; }

    bool GetRR() const;
	void SetRR(bool bRR) { m_bRR = bRR; }

	float GetUVScale() const { return m_fUVScale; }
	void SetUVScale(float fUVScale) { m_fUVScale = fUVScale; }

	float GetSpeed() const { return m_fSpeed; }
	void SetSpeed(float fSpeed) { m_fSpeed = fSpeed; }

	const NiFixedString& GetTexturePathName() const { return m_kTexturePathName; }
	void SetTexturePathName(const NiFixedString& kTexturePathName) { m_kTexturePathName = kTexturePathName; }

	bool Load(CTagStream& kStream);
	bool Init();
	void Destroy();

	void Update(const NiColor& kAmbientColor, const NiColor& kDiffuseColor);
    
    void SetupRender();
    void SetupRefractRender();
    void SetupRRRender(const NiPoint3& kWaterBase);

    NiAlphaProperty* GetAlphaProperty() { return m_spAlphaProperty; }

	static NiRenderedTexture* GetRefractTexture() { return ms_pkRefractTexture; }
    static NiRenderedTexture* GetRefractAlphaTexture() { return ms_pkRefractAlphaTexture; }
    static NiRenderTargetGroup* GetRefractAlphaTarget() { return ms_pkRefractAlphaTarget; }

	static NiRenderedTexture* GetReflectTexture() { return ms_pkReflectTexture; }
	static NiRenderTargetGroup* GetReflectTarget() { return ms_pkReflectTarget; }

    static void SetRefractPass(bool bRefrPass) { ms_bRefractPass = bRefrPass; }

	static void _SDMInit();
	static void _SDMShutdown();

    static void SetWaterRR(bool bEnable);
	static void ReCreate();

private:
	float m_fAlpha; //透明度
	bool m_bAlphaBlend; //是否混合
	bool m_bAdditiveLight; //贴图和光照相加
	bool m_bRR; //ReflectRefract 反射折射
	float m_fUVScale; //贴图坐标缩放
	float m_fSpeed; //贴图变化速度, 如果为1， 则每秒变化m_uiTexureCount张贴图
	NiFixedString m_kTexturePathName; //相对路径 + 文件名， 没有扩展名

	unsigned int m_uiTexureCount;
	NiTexture* m_pkTextures[MAX_TEXTURE_COUNT];

	CChunk*	 m_pkChunk;

    NiColor m_kAmbientColor;
    NiColor m_kDiffuseColor;
    
    NiTexture* m_pkCurrentTexture;

    //排序用
    NiAlphaPropertyPtr m_spAlphaProperty;

	static NiShaderDeclaration* ms_pkDecl;

	static NiD3DVertexShader* ms_pkVertexShader;
	static NiD3DPixelShader* ms_pkPixelShader;
	static NiD3DPixelShader* ms_pkAdditivePixelShader;

	static NiD3DVertexShader* ms_pkRefractVertexShader;
	static NiD3DPixelShader* ms_pkRefractPixelShader;

	static NiD3DVertexShader* ms_pkRRVertexShader;
	static NiD3DPixelShader* ms_pkRRPixelShader;

	static NiTexture* ms_pkNormalTexture;
    static NiTexture* ms_pkDuDvTexture;
	static NiRenderedTexture* ms_pkRefractTexture;
	static NiRenderedTexture* ms_pkReflectTexture;
	static NiRenderTargetGroup* ms_pkReflectTarget;

	static NiPixelData* ms_pkWaterEdgeMapDatas;
	static NiSourceTexture* ms_pkWaterEdgeMapTextures;

    static NiRenderedTexture* ms_pkRefractAlphaTexture;
    static NiRenderTargetGroup* ms_pkRefractAlphaTarget;
	
    static bool	ms_bRefractPass; //当前是在折射Pass里面, 需要关闭深度写入， 颜色RGB写入， 只保留Alpha写入
	static bool ms_bRRWaterEnable; //控制是否启用
};

#endif WATER_SHADER_H