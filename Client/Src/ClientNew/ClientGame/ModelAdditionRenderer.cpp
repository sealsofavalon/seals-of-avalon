#include "StdAfx.h"
#include "ModelAdditionRenderer.h"

ModelRendererInterface::ModelRendererInterface()
{
}

ModelRendererInterface::~ModelRendererInterface()
{
}

void ModelRendererInterface::Render(int zoneX, int zoneY, NiAVObject* pkModel)
{
}

//-------------------------------------------------------------------------------------
std::vector<ModelRendererInterface*> ModelAdditionRenderer::m_MRIList;
ModelAdditionRenderer::ModelAdditionRenderer(void)
{
}

ModelAdditionRenderer::~ModelAdditionRenderer(void)
{
}

void ModelAdditionRenderer::AddRenderer(ModelRendererInterface* pkModelRenderer)
{
	if (!pkModelRenderer) return;

	m_MRIList.push_back(pkModelRenderer);
}

void ModelAdditionRenderer::Render(int zoneX, int zoneY, NiAVObject* pkModel)
{
	unsigned int ui = 0;
	for (ui; ui < m_MRIList.size(); ui++) {
		ModelRendererInterface* pkRenderer = m_MRIList[ui];

		NIASSERT(pkRenderer);
		if (!pkRenderer) continue;

		pkRenderer->Render(zoneX, zoneY, pkModel);
	}
}

