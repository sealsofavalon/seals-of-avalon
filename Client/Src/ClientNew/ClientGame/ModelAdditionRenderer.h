#pragma once

#ifndef _MODEL_ADDITION_RENDERER_
#define _MODEL_ADDITION_RENDERER_

#include <NiMemObject.h>

class ModelRendererInterface
{
public:
	ModelRendererInterface();
	virtual ~ModelRendererInterface();

	virtual void Render(int zoneX, int zoneY, NiAVObject* pkModel);
};

class ModelAdditionRenderer
{
public:
	static void AddRenderer(ModelRendererInterface* pkModelRenderer);
	static void Render(int zoneX, int zoneY, NiAVObject* pkModel);

protected:
	ModelAdditionRenderer(void);
	~ModelAdditionRenderer(void);

private:
	static std::vector<ModelRendererInterface*> m_MRIList;

};


#endif //_MODEL_ADDITION_RENDERER_