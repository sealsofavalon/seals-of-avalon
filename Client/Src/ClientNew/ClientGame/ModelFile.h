#ifndef MODEL_FILE_H
#define MODEL_FILE_H

#include "ResourceManager.h"
class CModelFile : public NiMemObject
{
public:
    void ProcessTexture(char* pcLine);
    void ProcessShader(char* pcLine);
    void ProcessLine(char* pcLine);
    MfResNode* Load(const NiFixedString& kName);

private:
    NiFixedString m_kFilePath;
    unsigned int m_uiLine;
    NiAVObject* m_pkRoot;
    MfResNode* m_pkResNode;
};

#endif
