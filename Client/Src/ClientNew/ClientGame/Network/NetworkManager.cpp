#include "stdafx.h"
#include "NetworkManager.h"
#include "PacketParser.h"
#include "PacketBuilder.h"
#include "ClientState.h"
#include "UI/UISystem.h"
#include "UI/UILogin.h"
#include "UI/UMessageBox.h"
#include "ui/UIGamePlay.h"
#include "ui/UIWaitList.h"
#include "ui/UChat.h"
#include "Utils/ClientUtils.h"
#include "../../../../../new_common/Source/compression/zlib/ZlibCompressionStrategy.h"
#include <nb30.h>
#pragma comment(lib,"netapi32.lib") 
#pragma comment(lib, "ws2_32.lib")

CNetworkManager::CNetworkManager()
{
	Init();
}

CNetworkManager::~CNetworkManager()
{
	delete gc;
	delete lc;
	net_global::free_net_service();
}

typedef struct _ASTAT_
{
	ADAPTER_STATUS adapt;
	NAME_BUFFER    NameBuff [30];
}ASTAT, * PASTAT;

ASTAT Adapter;


bool CNetworkManager::Init()
{
	static zip_compress_strategy cs;
	net_global::init_net_service( 1, 10, &cs, 1, 100 );

	m_bSendPing = false;
	gc = new gate_connection;
	lc = new login_connection;

	NCB Ncb;
	UCHAR uRetCode;
	char NetName[50];
	LANA_ENUM   lenum;
	int      i;

	memset( &Ncb, 0, sizeof(Ncb) );
	Ncb.ncb_command = NCBENUM;
	Ncb.ncb_buffer = (UCHAR *)&lenum;
	Ncb.ncb_length = sizeof(lenum);
	uRetCode = Netbios( &Ncb );
	//printf( "The NCBENUM return code is: 0x%x \n", uRetCode );

	for(i=0; i < lenum.length ;i++)
	{
		memset( &Ncb, 0, sizeof(Ncb) );
		Ncb.ncb_command = NCBRESET;
		Ncb.ncb_lana_num = lenum.lana[i];

		uRetCode = Netbios( &Ncb );
		//printf( "The NCBRESET on LANA %d return code is: 0x%x \n",
		//	lenum.lana, uRetCode );

		memset( &Ncb, 0, sizeof (Ncb) );
		Ncb.ncb_command = NCBASTAT;
		Ncb.ncb_lana_num = lenum.lana[i];

		strcpy( (char*)Ncb.ncb_callname, "*               " );
		Ncb.ncb_buffer = (PUCHAR) &Adapter;
		Ncb.ncb_length = sizeof(Adapter);

		uRetCode = Netbios( &Ncb );
		//printf( "The NCBASTAT on LANA %d return code is: 0x%x \n",
			//lenum.lana, uRetCode );
		if ( uRetCode == 0 )
		{
			//printf( "The Ethernet Number on LANA %d is:%02x%02x%02x%02x%02x%02x\n",
			//	lenum.lana,
			sprintf( mac_addr, "%02X-%02X-%02X-%02X-%02X-%02X",
				Adapter.adapt.adapter_address[0],
				Adapter.adapt.adapter_address[1],
				Adapter.adapt.adapter_address[2],
				Adapter.adapt.adapter_address[3],
				Adapter.adapt.adapter_address[4],
				Adapter.adapt.adapter_address[5] );
		}
	}
	return true;
}

void CNetworkManager::ConnectGate( const std::string& IP, unsigned short usPort)
{
	lc->close();
	lc->ignore_disconn = true;

	if( gc->is_connected() )
	{
		gc->close();
		gc->ignore_disconn = true;
		while( true )
		{
			Sleep( 100 );
			gc->run_no_wait();
			if( !gc->is_connected() )
				break;
		}
	}

    m_bSendPing = false;
	gc->connect( IP.c_str(), usPort );
}

void CNetworkManager::ConnectLogin(const char* pcAddress, unsigned short usPort)
{
	if( lc->is_connected() )
	{
		lc->close();
		lc->ignore_disconn = true;
		while( true )
		{
			Sleep( 100 );
			lc->run_no_wait();
			if( !lc->is_connected() )
				break;
		}
	}

	m_bSendPing = false;
	return lc->connect( pcAddress, usPort );
}

void CNetworkManager::Disconnect(DisConnectState pConnetState)
{
	BOOL bInLogin = SYState()->UI->IsCurFrame(DID_LOGIN);
	switch( pConnetState )
	{
	case CHOOSE_SERVER:
		{
			lc->delay_close( 1 );
			lc->ignore_disconn = true;
			
			gc->close();
			gc->ignore_disconn = true;
		}
		break;
	case CHOOSE_CHARACTER:
	case IN_GAME:
		{
			gc->close();
			gc->ignore_disconn = true;
		}
		break;
	default:
		return;
	}
	if (!bInLogin)
	{
		ULoginFrame::ReLogin();
	}

}

bool CNetworkManager::IsConnected()
{
	return gc->is_connected();
}

bool CNetworkManager::SendPacket(const PakHead& packet)
{
	CPacketSn cps;
	cps << packet;
	cps.SetProLen();
	unsigned int uiSize = cps.GetSize();

	if( uiSize == 0 )
		return true;

	if( gc->is_connected() )
		gc->send_message( cps.GetBuf(), cps.GetSize() );
	else if( lc->is_connected() )
		lc->send_message( cps.GetBuf(), cps.GetSize() );

	/*static UINT DebugMoveMsgCount = 0 ;
	DebugMoveMsgCount++ ;
	SYSTEMTIME st;
	GetLocalTime( &st );
	char debugbuf[_MAX_PATH];
	sprintf(debugbuf ,"<br>time:%d:%d:%d: 总计消息数量%u size:%u <br>",st.wHour,st.wMinute,st.wSecond,DebugMoveMsgCount,cps.GetSize());
	ChatSystem->AppendChatMsg(CHAT_MSG_PICKITEM, debugbuf);*/
	return true;
}

void CNetworkManager::Update(float fTime)
{
	uint32 mstime = float2int32(NiGetCurrentTimeInSec() * 1000);

	if ( gc->is_connected() && (SYState()->LastPing == 0 || (SYState()->LastPing + 15000) <= mstime) )
	{
		PacketBuilder->SendPing(mstime);
	}
	gc->run_no_wait();
	lc->run_no_wait();
}

///////////////////////////////////////////////////////////////////////////////////

gate_connection::gate_connection() : tcp_client( *net_global::get_io_service() ), ignore_disconn( false )
{
}

void gate_connection::on_connect()
{
	tcp_client::on_connect();
    PacketBuilder->SendGTLoginReqMsg();
    NetworkMgr->m_bSendPing = true;
    UMessageBox::PopMsgBox();
}

void gate_connection::on_connect_failed( boost::system::error_code error )
{
    UMessageBox::PopMsgBox();
	UMessageBox::MsgBox_s(_TRAN("连接服务器失败"),(BoxFunction*)ULoginFrame::ReLogin, 10.0f);
}

void gate_connection::on_close( const boost::system::error_code& error )
{
	tcp_client::on_close( error );

	if( ignore_disconn )
	{
		ignore_disconn = false;
		return;
	}
	BOOL bInLogin = SYState()->UI->IsCurFrame(DID_LOGIN);
	if (!bInLogin)
	{
		UMessageBox::MsgBox_s(_TRAN("与服务器断开连接"),(BoxFunction*)ULoginFrame::ReLogin, 10.0f);
	}
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pInGame)
	{
		pInGame->ResetBagAndBank();
	}
}

void gate_connection::proc_message( const message_t& msg )
{
	PacketParser->ProcessPacket( msg.data, msg.len );
}

/////////////////////////////////////////////////////////////////////////////////////

login_connection::login_connection() : tcp_client( *net_global::get_io_service() ), ignore_disconn( false )
{
}

void login_connection::on_connect()
{
	tcp_client::on_connect();
    UMessageBox::PopMsgBox();
    PacketBuilder->SendVersionMsg(0);
}

void login_connection::on_connect_failed( boost::system::error_code error )
{
    UMessageBox::PopMsgBox();
    UMessageBox::MsgBox_s(_TRAN("连接登录服务器失败"), NULL, 10);
}

void login_connection::on_close( const boost::system::error_code& error )
{
	tcp_client::on_close( error );

	if( ignore_disconn )
	{
		ignore_disconn = false;
		return;
	}

	BOOL bInLogin = SYState()->UI->IsCurFrame(DID_LOGIN);
	if (!bInLogin)
	{
		UMessageBox::MsgBox_s(_TRAN("与登录服务器断开连接"),(BoxFunction*)ULoginFrame::ReLogin, 10.0f);
	}
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pInGame)
	{
		pInGame->ResetBagAndBank();
	}
}

void login_connection::proc_message( const message_t& msg )
{
	PacketParser->ProcessPacket( msg.data, msg.len );
}

