#pragma once

#include "../../../../../new_common/Source/net/tcpclient.h"
#include "../../../../../new_common/Source/utilities/utilities.h"
#define BUFFER_SIZE 6553600


enum DisConnectState
{
	CHOOSE_SERVER,
	CHOOSE_CHARACTER,
	IN_GAME
};

class gate_connection : public tcp_client
{
public:
	gate_connection();
	virtual void on_connect();
	virtual void on_connect_failed( boost::system::error_code error );
	virtual void on_close( const boost::system::error_code& error );
	virtual void proc_message( const message_t& msg );

	bool ignore_disconn;
};

class login_connection : public tcp_client
{
public:
	login_connection();
	virtual void on_connect();
	virtual void on_connect_failed( boost::system::error_code error );
	virtual void on_close( const boost::system::error_code& error );
	virtual void proc_message( const message_t& msg );

	bool ignore_disconn;
};

class CNetworkManager : public NiMemObject
{
public:
	CNetworkManager();
	virtual ~CNetworkManager();

	bool Init();
	void Shutdown();

	void ConnectGate( const std::string& IP, unsigned short usPort);
	void ConnectLogin( const char* pcAddress, unsigned short usPort);
	void Disconnect(DisConnectState pConnetState = IN_GAME);
	bool IsConnected();
	bool SendPacket(const PakHead& packet);

	void Update(float fTime);

	bool m_bSendPing;

	gate_connection* gc;
	login_connection* lc;
	char mac_addr[18];
};
