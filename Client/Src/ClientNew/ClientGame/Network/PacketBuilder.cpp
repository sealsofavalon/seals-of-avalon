#include "stdafx.h"
#include "NetworkManager.h"
#include "PacketBuilder.h"
#include "ClientState.h"
#include "UI\UChat.h"
#include "UI\UFun.h"
#include "ObjectManager.h"
#include "../../../../SDBase/Protocol/C2LS.h"
#include "../../../../SDBase/Protocol/C2S_Loot.h"
#include "../../../../SDBase/Protocol/C2S_Spell.h"
#include "../Console.h"
#include "../../../../SDBase/Protocol/C2S_Guild.h"
#include "../../../../SDBase/Protocol/C2S_Auction.h"
#include "../../../../new_common/Source/ssl/MD5.h"

#include "../../../../new_common/Source/utilities/utilities.h"
#include "../../../../SDBase/Protocol/C2S_Contribution.h"
#include "../../../../SDBase/Protocol/C2S_Serial_for_gift.h"
#include "AudioInterface.h"
#include "Hook/HookManager.h"

bool g_just_leap = false;

CPacketBuilder::CPacketBuilder():
m_indexPing(0)
{
}

//发送当前客户端的版本号
bool CPacketBuilder::SendVersionMsg(int iVersion)
{
	MSG_C2LS::stVersionReq version;
    version.nVersion = atoi(ClientState->GetClientVersion());

	return NetworkMgr->SendPacket(version);
}

//发送加密后的账号和密码
bool CPacketBuilder::SendLoginReqMsg( unsigned int tablekey )
{
	MSG_C2LS::stLoginReq login;

	login.account = UTF8ToAnis(ClientState->GetUserName());
	std::string md5pwd = MD5Hash::_make_md5_pwd( UTF8ToAnis(ClientState->GetUserPass() ).c_str() );

    //清除掉内存当中保存的Password
    ClientState->ClearUserPass();
    
	encrypt_string( md5pwd, login.password, tablekey );
	login.mac_address = NetworkMgr->mac_addr;
	NetworkMgr->SendPacket(login);
    return true;
}

//发送选组消息
bool CPacketBuilder::SendGroupSelReqMsg(ui8 id)
{
	MSG_C2S::stSelectGroupReq group;
	group.groupid = id;

	return NetworkMgr->SendPacket(group);
}

bool CPacketBuilder::SendCanelGroupMsg()
{
	//MSG_C2LS::stReturnToLogin Msg;
	//return NetworkMgr->SendPacket(Msg);
	return true;
}
//连接GateServer
//发送登录游戏服务器消息
bool CPacketBuilder::SendGTLoginReqMsg()
{
	MSG_C2S::stLoginReq Msg;
	Msg.nTempPassword	= ClientState->GetSessionID();
	Msg.nAccountID	= ClientState->GetAccountID();//UTF8ToAnis(ClientState->GetUserName());
	
	NetworkMgr->SendPacket(Msg);

	SendPingIndex(0);

	return true;
}

//创建角色
bool CPacketBuilder::SendCreateRoleReqMsg(const std::string& strRoleName, ui8 ucSex, ui8 nRace, ui8 nClass)
{
	MSG_C2S::stCreateRoleReq Msg;
	Msg.nAccountID	= ClientState->GetAccountID();
	Msg.Name	= strRoleName;//角色名称
	Msg.nSex		= ucSex;
	Msg.nClass			= nClass;
	Msg.nRace	= nRace;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendCancelEnterGameLine()
{
	MSG_C2S::stCancelEnterLineReq Msg;
	
	return NetworkMgr->SendPacket(Msg);
}
//删除角色
bool CPacketBuilder::SendDeleteRoleReqMsg(ui64 guid)
{
	MSG_C2S::stDelRoleReq Msg;
	Msg.nAccountID	= ClientState->GetAccountID();
	Msg.guid	= guid;

	return NetworkMgr->SendPacket(Msg);
}

//请求进入游戏
bool CPacketBuilder::SendEnterGameReqMsg(std::string& strPlayerName, ui64 guid)
{
	MSG_C2S::stEnterGameReq Msg;
	Msg.AccountID	= ClientState->GetAccountID();
	Msg.guid		= guid;
	Msg.strRole		= UTF8ToAnis(strPlayerName);//角色名称
 	Msg.AccountName	= ClientState->GetUserNameA();
	
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendExitGameReqMsg()
{
	MSG_C2S::stExitReq Msg;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendLoadingOKMsg()
{
	MSG_C2S::stLoadingOK Msg;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendAttackReqMsg(CGameObject* pkTarget)
{
	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLocalPlayer);

	MSG_C2S::stAttack_Swing Msg;
	Msg.guid = pkTarget->GetGUID();

	return NetworkMgr->SendPacket( Msg );
}

bool CPacketBuilder::SendAttackStopMsg()
{
	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLocalPlayer);

	MSG_C2S::stAttack_Stop Msg;
	return NetworkMgr->SendPacket( Msg );
}

// 释放技能,使用SendAttackReqMsg释放技能函数有参数问题,所以用这个函数替换
bool CPacketBuilder::SendCastSpell(ui32 nSpellID, const SpellCastTargets& targets)
{
	MSG_C2S::stSpell_Cast Msg;
	Msg.spellId = nSpellID;
	Msg.targets = targets;
	return NetworkMgr->SendPacket( Msg );
}
bool CPacketBuilder::SendLeapPrepar(NiPoint3 pos, float fz)
{
	MSG_C2S::stLeapPrepare Msg;
	Msg.x = pos.x;
	Msg.y = pos.y;
	Msg.z = pos.z;

	Msg.o = fz;
	g_just_leap = true;

	return NetworkMgr->SendPacket(Msg);
}
// 中断当前技能释放
bool CPacketBuilder::SendCancelSpell()
{
	MSG_C2S::stSpell_Cancel_Cast Msg;
	return NetworkMgr->SendPacket( Msg );
}
bool CPacketBuilder::SendSetActionButton(ui8 pos, ui32 nSpellID, UINT nSpellType)
{
	MSG_C2S::stActionButton_Set Msg;

	Msg.action = nSpellID;
	Msg.misc = 0;
	Msg.button = pos;
	Msg.type = nSpellType;
	if (pos == 255)
	{
		return false;
	}

	return NetworkMgr->SendPacket(Msg);
}

// 停止buf
bool CPacketBuilder::SendCancelAura(ui32 nSpellID)
{
	MSG_C2S::stSpell_Cancel_Aura Msg;
	Msg.spellId = nSpellID;
	return NetworkMgr->SendPacket( Msg );
}

//请求复活消息
//bool CPacketBuilder::SendReliveReqMsg()
//{
//	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
//// 	NIASSERT(pkLocalPlayer);
//// 
//// 	MSG_C2S::stReliveReq Msg;
//// 	Msg.nReliveType	= 0; //复活类型
//	MSG_C2S::stSpell_Self_Resurrect Msg;
//
//	return NetworkMgr->SendPacket(Msg);
//}


//聊天消息
bool CPacketBuilder::SendChatReqMsg(ChatMsg msg,const char* pcMsg)
{
	if( !pcMsg[0] )
		return true;

	//CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	//NIASSERT(pkPlayer);

	//pkPlayer->SetChatText(pcMsg);

	MSG_C2S::stChat_Message Msg;
	Msg.txt	= pcMsg;

	Msg.type = msg;
	switch (msg)
	{
	case CHAT_MSG_AFK:
		{
			Msg.type = msg;
			Msg.reason = pcMsg;
			Msg.txt.clear();
		}
		break;
	case CHAT_MSG_DND:
		{
			Msg.type = msg;
			Msg.reason = pcMsg;
			Msg.txt.clear();
		}
		break;
	case CHAT_MSG_TRADE:
		{
			Msg.type = CHAT_MSG_CHANNEL;
			Msg.channel_name = "TRADE";
		}
		break;
	case CHAT_MSG_CITY:
		{
			Msg.type = CHAT_MSG_CHANNEL;
			std::string str;
			ChatSystem->GetAreaChannel(str);
			Msg.channel_name = str;
		}
		break;
	case CHAT_MSG_LFG:
		{
			Msg.type = CHAT_MSG_CHANNEL;
			Msg.channel_name = "LFG";
		}
		break;
	case CHAT_MSG_PRIVATE:
		{
			Msg.type = CHAT_MSG_CHANNEL;
			Msg.channel_name = ChatSystem->GetChannelData(CHAT_MSG_PRIVATE)->ChannelName;
		}
		break;
	}

	//todo...
	//优化, 玩家对玩家的消息数据可以直接发送UTF8 String

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendPrivateChatMsg(const char *pcMsg,ui64 guid,const char * Name)
{
	if( !pcMsg[0] )
		return true;

	MSG_C2S::stChat_Message Msg;
	Msg.txt	= pcMsg;

	Msg.type = CHAT_MSG_WHISPER;
	Msg.guid = guid;
	Msg.creature_name = Name;
	//todo...
	//优化, 玩家对玩家的消息数据可以直接发送UTF8 String

	return NetworkMgr->SendPacket(Msg);
}
//物品拾取消息
bool CPacketBuilder::SendMapItemPickUpReqMsg(ui64 mapitemguid)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkPlayer);

	MSG_C2S::stItem_PickUp Msg;
	Msg.mapitem_guid	= mapitemguid;

	return false;
	//return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendSplitItem(ui8 srcbag, ui8 srcslot, ui8 dstbag, ui8 dstslot, ui8 count)
{
	MSG_C2S::stItem_Split Msg;

	Msg.srcbag = srcbag;
	Msg.srcslot = srcslot;
	Msg.dstbag = dstbag;
	Msg.dstslot = dstslot;
	Msg.count = count;

	return NetworkMgr->SendPacket(Msg);

}

bool CPacketBuilder::SendRepairItem(ui64 NPCGuid, ui64 ItemGuid)
{
	//当ItemGuid = 0  为修理全部
	MSG_C2S::stItem_Repair Msg;

	Msg.npcguid = NPCGuid; 
	Msg.itemguid = ItemGuid;

	return NetworkMgr->SendPacket(Msg);
}


//丢弃背包道具到地图的请求
bool CPacketBuilder::SendMapItemDropReqMsg(ui8 bag, ui8 slot)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkPlayer);

	MSG_C2S::stItem_Destroy Msg;
	Msg.srcbag = bag;
	Msg.srcslot = slot;
	
	//Msg.count

	return NetworkMgr->SendPacket(Msg);
}

void AutoEquipItem(std::vector<std::string>& args)
{
	NIASSERT(args.size() == 2);
	
	PacketBuilder->SendEquipItemMsg(atoi(args[0].c_str()), atoi(args[1].c_str()) + INVENTORY_SLOT_ITEM_START);
}

RegistConsoleFun(AutoEquipItem);

bool CPacketBuilder::SendEquipItemMsg(ui8 bag, ui8 slot)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkPlayer);
	MSG_C2S::stItem_AutoEquip Msg;
	Msg.srcbag	= bag;
	Msg.srcslot	= slot;
	return NetworkMgr->SendPacket(Msg);
}

//移动背包道具位置的请求
bool CPacketBuilder::SendItemMovePosReqMsg(ui8 srcbag, ui8 dstbag ,ui8 srcslot, ui8 dstslot, WORD wNum)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkPlayer);

	MSG_C2S::stItem_Swap Msg;
	Msg.srcbag  = srcbag;
	Msg.dstbag  = dstbag;
	Msg.srcslot	= srcslot;
	Msg.dstslot	= dstslot;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendNpcClikedMsg(ui64 Npcguid)
{
	MSG_C2S::stItem_List_Inventory Msg;
	Msg.vendorguid = Npcguid;

	return NetworkMgr->SendPacket(Msg);
}

//物品购买请求
bool CPacketBuilder::SendItemBuyReqMsg(ui64 vecdorguid, ui32 dwItemId, ui8 byNum, ui32 amt)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkPlayer);

	MSG_C2S::stItem_Buy Msg;
	Msg.vendorguid = vecdorguid;
	Msg.item	= dwItemId;
	Msg.amt		= amt;
	Msg.count	= byNum;

	return NetworkMgr->SendPacket(Msg);
}

//物品卖出请求
bool CPacketBuilder::SendItemSellReqMsg(ui64 vecdorguid, ui64 itemguid, ui8 byNum)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkPlayer);

	MSG_C2S::stItem_Sell Msg;
	Msg.vendorguid = vecdorguid;
	Msg.itemguid	= itemguid;
	Msg._count	= byNum;

	return NetworkMgr->SendPacket(Msg);
}

//物品使用
bool CPacketBuilder::SendItemUseReqMsg(ui8 bag, ui8 slot, ui64 item_guid, SpellCastTargets targets)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkPlayer);

	MSG_C2S::stItem_Use Msg;
	Msg.bag		= bag;
	Msg.slot	= slot;
	Msg.item_guid = item_guid;
	Msg.targets = targets;

	return NetworkMgr->SendPacket(Msg);
}


bool CPacketBuilder::SendUnBindReqMsg( ui64 source, uint64 targetguid )
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkPlayer);

	MSG_C2S::stUnbindItemReq Msg;
	Msg.sourceGUID	= source;
	Msg.targetGUID	= targetguid;


	return NetworkMgr->SendPacket(Msg);
}

// 选中
bool CPacketBuilder::SendSetSelectionMsg(uint64 Selectguid, bool toattack)
{
	MSG_C2S::stSet_Selection Msg;
	Msg.guid = Selectguid;
	Msg.ToAttack = toattack;

	return NetworkMgr->SendPacket(Msg);
}

//
bool CPacketBuilder::SendSetTargetMsg(uint64 Targetguid)
{
	MSG_C2S::stSet_Target Msg;
	Msg.guid = Targetguid;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendPing(uint32 t)
{
	


	MSG_C2S::stPing Msg;
	SYState()->LastPing = t;
	Msg.timestamp = (t/1000);
	Msg.index = m_indexPing;
	m_indexPing ++;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendPingIndex(uint32 n)
{
	m_indexPing = n;
	return true;
}

bool CPacketBuilder::SendNPCTrainerBuySpell(ui64 guid, ui32 TeachSpellId)
{
	MSG_C2S::stNPC_Trainer_Buy_Spell Msg;
	Msg.guid = guid;
	Msg.TeachingSpellID = TeachSpellId;
	return NetworkMgr->SendPacket(Msg);
}
//////////////////////////////////////////////////////////////////////////
// Player Trade Msg
//////////////////////////////////////////////////////////////////////////
// 拿掉一个道具
bool CPacketBuilder::SendTradeClearItemMsg(ui8 ucTradeSlot)
{
	MSG_C2S::stTradeClearItem Msg;
	Msg.tradeslot = ucTradeSlot;

	return NetworkMgr->SendPacket(Msg);
}

// 增加一个道具
bool CPacketBuilder::SendTradeSetItemMsg(ui8 ucTradeSlot, ui8 ucBag, ui8 ucSlot)
{
	MSG_C2S::stTradeSetItem Msg;
	Msg.tradeslot = ucTradeSlot;
	Msg.bag = ucBag;
	Msg.slot = ucSlot;

	return NetworkMgr->SendPacket(Msg);
}

// 金钱
bool CPacketBuilder::SendTradeSetGoldMsg(ui32 uiGold)
{
	MSG_C2S::stTradeSetGold Msg;
	Msg.nGold = uiGold;

	return NetworkMgr->SendPacket(Msg);
}

void TradeReq(std::vector<std::string>& args)
{
	NIASSERT(args.size() == 1);

	PacketBuilder->SendTradeInitMsg(atoi(args[0].c_str()));
}

RegistConsoleFun(TradeReq);

// 交易请求
bool CPacketBuilder::SendTradeInitMsg(ui64 Targetguid)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal && pkLocal->CanSendTradeRequest(Targetguid))
	{
		MSG_C2S::stTradeInit Msg;
		Msg.target_guid = Targetguid;

		return NetworkMgr->SendPacket(Msg);
	}
	
	return false ;
}

// 交易同意
bool CPacketBuilder::SendTradeBeginMsg()
{
	MSG_C2S::stTrade_Begin Msg;

	return NetworkMgr->SendPacket(Msg);
}
//锁定交易
bool CPacketBuilder::SendTradeLockMsg()
{
	MSG_C2S::stTrade_Lock Msg;

	return NetworkMgr->SendPacket(Msg);
}
void TradeAcceptReq(std::vector<std::string>& args)
{
	PacketBuilder->SendTradeAcceptMsg();
}

RegistConsoleFun(TradeAcceptReq);

// 交易确认
bool CPacketBuilder::SendTradeAcceptMsg()
{
	MSG_C2S::stTradeAccept Msg;
	
	return NetworkMgr->SendPacket(Msg);
}

// 交易确认取消


//
bool CPacketBuilder::SendTradeBusyMsg()
{
	MSG_C2S::stTrade_Busy Msg;

	return NetworkMgr->SendPacket(Msg);
}

// 交易拒绝
bool CPacketBuilder::SendTradeIgnoreMsg()
{
	MSG_C2S::stTrade_Ignore Msg;

	return NetworkMgr->SendPacket(Msg);
}

// 交易取消
bool CPacketBuilder::SendTradeCancelMsg()
{
	MSG_C2S::stTrade_Cancel Msg;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendNpcGossipStatus(ui64 guid)
{
	/*
	MSG_C2S::stQuestGiver_Stats_Query Msg;
	Msg.guid = guid;
	return NetworkMgr->SendPacket(Msg);
	*/
	return true;
}
// 与任务npc对话
bool CPacketBuilder::SendNpcGossipHelloMsg(ui64 guid)
{
	MSG_C2S::stNPC_Gossip_Hello Msg;
	Msg.guid = guid;

	return NetworkMgr->SendPacket(Msg);
}

// 选择一个选项
bool CPacketBuilder::SendNpcGossipSelectOptionMsg(ui64 guid, ui32 option, string str /* =  */)
{
	MSG_C2S::stNPC_Gossip_Select_Option Msg;
	Msg.guid = guid;
	Msg.option = option;
	Msg.ExtraStr = str;

	return NetworkMgr->SendPacket(Msg);
}

// 与任务NPC交互
bool CPacketBuilder::SendQuestGiverHelloMsg(ui64 guid)
{
	MSG_C2S::stQuestGiver_Hello Msg;
	Msg.guid = guid;

	return NetworkMgr->SendPacket(Msg);
}

// 接受任务
bool CPacketBuilder::SendQuestGiverAcceptQuestMsg(ui64 guid, ui32 guestid)
{
	MSG_C2S::stQuestGiver_Accept_Quest Msg;
	Msg.guid = guid;
	Msg.quest_id = guestid;

	SYState()->IAudio->PlayUiSound(UI_questnew);
	NetworkMgr->SendPacket(Msg);
	return SendNpcGossipStatus(guid);
}

// 拒绝任务
bool CPacketBuilder::SendQuestGiverCancelMsg()
{
	MSG_C2S::stQuestGiver_Cancel Msg;
	
	return NetworkMgr->SendPacket(Msg);
}

// 完成任务
bool CPacketBuilder::SendQuestGiverCompleteQuestMsg(ui64 guid, ui32 questid)
{
	MSG_C2S::stQuestGiver_Complete_Quest Msg;
	Msg.guid = guid;
	Msg.quest_id = questid;

	NetworkMgr->SendPacket(Msg);
	return SendNpcGossipStatus(guid);
}

// 共享任务
bool CPacketBuilder::SendQuestPushToPartyMsg(ui32 questid)
{
	MSG_C2S::stQuest_PushTo_Party Msg;
	Msg.quest_id = questid;

	return NetworkMgr->SendPacket(Msg);
}

// 任务完成时选择奖励物品
bool CPacketBuilder::SendQuestGiverChooseRewardMsg(ui64 guid, ui32 questid, ui32 rewardslot)
{
	MSG_C2S::stQuestGiver_Choose_Reward Msg;
	Msg.guid = guid;
	Msg.quest_id = questid;
	Msg.reward_slot = rewardslot;

	return NetworkMgr->SendPacket(Msg);
}

// 任务日志 删除
bool CPacketBuilder::SendQuestLogRemoveQuestMsg(ui8 questslot)
{
	MSG_C2S::stQuestLog_Remove_Quest Msg;
	Msg.quest_slot = questslot;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendstQuestEscortReply(uint8 is_accept, uint32 quest_id, uint32 npc_id)
{
	MSG_C2S::stQuestEscortReply Msg;
	Msg.is_accept = true;
	Msg.quest_id = quest_id;
	Msg.npc_id = npc_id;
	return NetworkMgr->SendPacket(Msg);
}

//组队请求
bool CPacketBuilder::SendGroupInviteMsg(const char * Name)
{
	MSG_C2S::stGroupInvite Msg;
	Msg.membername = Name;

	return NetworkMgr->SendPacket(Msg);
}
//同意组队
bool CPacketBuilder::SendGroupAcceptMsg()
{
	MSG_C2S::stGroupAccept Msg;
	
	return NetworkMgr->SendPacket(Msg);
}
//拒绝组队
bool CPacketBuilder::SendRefuseGroupMsg()
{
	MSG_C2S::stGroupDecline Msg;

	return NetworkMgr->SendPacket(Msg);
}
//移除队员
bool CPacketBuilder::SendRemoveMemberMsg(ui64 PlayerGuid)
{
	MSG_C2S::stGroupUnviteGuid Msg;
	Msg.guid = PlayerGuid;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendRemoveMemberMsg(const char * membername)
{
	MSG_C2S::stGroupUnviteName Msg;
	Msg.membername = membername;

	return NetworkMgr->SendPacket(Msg);
}
//设置队长
//bool CPacketBuilder::SendSetGroupLeaderMsg(ui64 PlayerGuid)
//{
//	MSG_C2S::stGroupSetLeader Msg;
//	Msg.guid = PlayerGuid;
//
//	return NetworkMgr->SendPacket(Msg);
//}
//移交队长
bool CPacketBuilder::SendReSetGroupLeaderMsg(ui64 PlayerGuid)
{
	MSG_C2S::stGroupSetLeader Msg;
	Msg.guid = PlayerGuid;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendMemberReady(ui8 ready)
{
	MSG_C2S::stRaid_ReadyCheck Msg;
	Msg.ready = ready;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendChangeMemberSubGruopByGuid(ui64 desguid, ui64 srcguid)
{
	MSG_C2S::stGroupSwep Msg;
	Msg.desguid = desguid;
	Msg.srcguid = srcguid;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendChangeMemberSubGroup(const char* name, ui8 groupid)
{
	MSG_C2S::stGroup_Change_Sub_Group Msg;
	Msg.name = name;
	Msg.subGroup = groupid;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendChangeToTeam()
{
	MSG_C2S::stGroup_Raid_Convert Msg;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendChangeMemberIcon(ui64 guid, ui8 icon)
{
	MSG_C2S::stGroupSetPlayerIcon Msg;
	Msg.guid = guid;
	Msg.icon = icon;
	
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendTeamPickSetting(ui32 lootMethod, ui32 threshold)
{
	MSG_C2S::stGroupLootMethod Msg;

	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	if (!pLocal)
	{
		return false ;
	}

	Msg.lootMaster = pLocal->GetGUID();
	Msg.lootMethod = lootMethod;
	Msg.threshold = threshold;

	return NetworkMgr->SendPacket(Msg);
}
//加入队伍
//bool SendAddGroupMemMsg()
//{
//
//}
//离开队伍
//bool SendLeaveGroupMsg()
//{
//	
//}
//解散队伍
bool CPacketBuilder::SendGroupDisbandMsg()
{
	MSG_C2S::stGroupDisband Msg;
	/*ClientSystemNotify(_TRAN("您已经离开队伍!"));*/
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendStartLoot(ui64 guid)
{
	MSG_C2S::stMsg_Loot Msg;
	Msg.guid = guid;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendLootOneItem(ui32 slot)
{
	MSG_C2S::stMSG_AutoStore_Loot_Item Msg;
	Msg.lootSlot = slot;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendReleaseOneItem(ui64 guid)
{
	MSG_C2S::stLoot_Release Msg;
	Msg.guid = guid;

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		if (pkLocal->IsHook())
		{
			HookMgr->StopPickState(guid);
		}
	}

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendLootMoney()
{
	MSG_C2S::stLoot_Money Msg;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendStartRoll(uint64 creatureguid, uint32 slotid, uint8 choice, ui32 mapid, ui32	instanceid)
{
	MSG_C2S::stMsg_Loot_Roll Msg;
	Msg.creatureguid	= creatureguid;
	Msg.slotid			= slotid;
	Msg.choice			= choice;
	Msg.mapid			= mapid ;
	Msg.instanceid		= instanceid;
	return NetworkMgr->SendPacket(Msg);
}

//////////////////////////////////////////////////////////////////////////
// 好友消息
//////////////////////////////////////////////////////////////////////////
// 请求好友列表
bool CPacketBuilder::SendFriendListReqMsg(uint32 uiFlag)
{
	MSG_C2S::stFriend_List Msg;
	Msg.flag = uiFlag;

	return NetworkMgr->SendPacket(Msg);
}
// 添加好友
bool CPacketBuilder::SendFriendAddMsg(string& strName, string& strNote)
{
	MSG_C2S::stFriend_Add Msg;
	Msg.name = strName;
	Msg.note = strNote;

	return NetworkMgr->SendPacket(Msg);
}
// 好友删除
bool CPacketBuilder::SendFriendDelMsg(uint64 guid)
{
	MSG_C2S::stFriend_Del Msg;
	Msg.FriendGuid = guid;

	return NetworkMgr->SendPacket(Msg);
}
//
bool CPacketBuilder::SendFriendSetNote(uint64 guid, string& strNote)
{
	MSG_C2S::stFriend_Set_Note Msg;
	Msg.guid = guid;
	Msg.note = strNote;

	return NetworkMgr->SendPacket(Msg);
}
//
bool CPacketBuilder::SendFriendAddIgnore(string& strIgnoreName)
{
	MSG_C2S::stFriend_Add_Ignore Msg;
	Msg.ignore_name = strIgnoreName;

	return NetworkMgr->SendPacket(Msg);
}
//
bool CPacketBuilder::SendFriendDelIgnore(uint64 guid)
{
	MSG_C2S::stFriend_Del_Ignore Msg;
	Msg.IgnoreGuid = guid;

	return NetworkMgr->SendPacket(Msg);
}

//////////////////////////////////////////////////////////////////////////
bool CPacketBuilder::SendQueryNameMsg(uint64 guid)
{
	MSG_C2S::stQuery_Name Msg;
	Msg.guid = guid;

	return NetworkMgr->SendPacket(Msg);
}

//////////////////////////////////////////////////////////////////////////
bool CPacketBuilder::SendAuctionSellItemMsg(uint64 guid, uint64 item, uint32 bid, uint32 buyout, uint32 etime, bool bYP, bool bYuanBao)
{
	MSG_C2S::stAuction_Sell_Item Msg;
	Msg.guid = guid;
	Msg.item = item;
	Msg.bid = bid;
	Msg.buyout = buyout;
	Msg.etime = etime;
	Msg.yp = bYP;
	Msg.is_yuanbao = bYuanBao;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendAuctionRemoveItemMsg(uint64 guid, uint32 auctionid)
{
	MSG_C2S::stAuction_Remove_Item Msg;
	Msg.guid = guid;
	Msg.auction_id = auctionid;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendAuctionListItemsMsg(ui64 vendorguid, uint32 startIndex, std::string& auctionString, uint8 levelRange1, uint8 levelRange2, uint8 usableCheck, int32 inventoryType, int32 itemclass, int32 itemsubclass, int32 rarityCheck)
{
	MSG_C2S::stAuction_List_Items Msg;
	Msg.vendorguid = vendorguid;
	Msg.start_index = startIndex;
	Msg.auctionString = UTF8ToAnis(auctionString);
	Msg.levelRange1 = levelRange1;
	Msg.levelRange2 = levelRange2;
	Msg.usableCheck = usableCheck;
	Msg.inventory_type = inventoryType;
	Msg.itemclass = itemclass;
	Msg.itemsubclass = itemsubclass;
	Msg.rarityCheck = rarityCheck;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendAuctionListOwnerItems(ui64 vendorguid)
{
	MSG_C2S::stAuction_List_Owner_Items Msg;
	Msg.vendorguid = vendorguid;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendAuctionPlaceBidMsg(ui64 guid, uint32 aucitonId, uint32 price)
{
	MSG_C2S::stAuction_Place_Bid Msg;
	Msg.guid = guid;
	Msg.auction_id = aucitonId;
	Msg.price = price;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendAuctionListBidderItemsMsg(uint64 vendorguid)
{
	MSG_C2S::stAuction_List_Bidder_Items Msg;
	Msg.vendorguid = vendorguid;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendAuctionHelloMsg(uint64 vendorguid, ui32 auctionhouse_id)
{
	MSG_C2S::stAuction_Hello Msg;
	Msg.vendorguid = vendorguid;
	Msg.auctionhouse_id = auctionhouse_id;

	return NetworkMgr->SendPacket(Msg);
}
//////////////////////////////////////////////////////////////////////////
bool CPacketBuilder::SendRequestMailList()
{
	MSG_C2S::stMail_Get_List Msg;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendRequestNeedRead()
{
	MSG_C2S::stMail_Query_Next_Time Msg;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendRequestMailContent(uint32 messageId)
{
	MSG_C2S::stMail_Text_Query Msg;
	Msg.message_id = messageId ;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendMail(MSG_C2S::stMail_Send mail)
{
	return NetworkMgr->SendPacket(mail);
}

bool CPacketBuilder::SendReadMail(uint32 messageId)
{
	MSG_C2S::stMail_Mark_As_Read Msg;
	
	Msg.message_id = messageId;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendDeleteMail(uint32 messageId) // 请求删除邮件
{
	MSG_C2S::stMail_Delete Msg;

	Msg.message_id = messageId;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendGetMailItem(uint32 messageId, uint32 lowid)//请求提取邮件附件的道具
{
	MSG_C2S::stMail_Take_Item Msg;

	Msg.message_id = messageId;
	Msg.lowguid = lowid;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendGetMailMoney(uint32 messageId)//请求提取附件里的金钱
{
	MSG_C2S::stMail_Take_Money Msg;
	
	Msg.message_id = messageId;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendReturnToSender(uint32 messageId)//请求退回邮件
{
	MSG_C2S::stMail_Return_To_Sender Msg;

	Msg.message_id = messageId;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGetMailPaper(uint32 messageId)//获取邮件信纸
{
	MSG_C2S::stMail_Create_Text_Item Msg;

	Msg.message_id = messageId;
	
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendBuyBankBagPos()
{
	MSG_C2S::stItem_Buy_Bank Msg;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendChannelJoin(const char * channelname,uint32 dbc_id,const char * password)
{
	MSG_C2S::stChannel_Join Msg;
	Msg.dbc_id = dbc_id;
	if (dbc_id == CHAT_CHANNEL_TYPE_PRIVATE)
	{
		Msg.channelname = channelname;
	}
	if (dbc_id == CHAT_CHANNEL_TYPE_TRADE)
	{
		Msg.channelname = "TRADE";
	}
	if (dbc_id == CHAT_CHANNEL_TYPE_CITY)
	{
		Msg.channelname = channelname;
		//Msg.channelname = "CITY";
	}
	if (dbc_id == CHAT_CHANNEL_TYPE_LFG)
	{
		Msg.channelname = "LFG";
	}
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendchannelLeave(const char * channelname,uint32 code)
{
	MSG_C2S::stChannel_Leave Msg;
	if (channelname)
	{
		Msg.channelname = channelname;
	}
	return NetworkMgr->SendPacket(Msg);
}

//被骑的人邀请骑乘的人
bool CPacketBuilder::SendMountInvite(ui64 guid)
{
	MSG_C2S::stMount_Invite Msg;
	Msg.player_guid = guid;

	return NetworkMgr->SendPacket(Msg);
}

//解散队伍
bool CPacketBuilder::SendMountDisband()
{
	MSG_C2S::stMount_Disband Msg;

	return NetworkMgr->SendPacket(Msg);
}

//同意邀请
bool CPacketBuilder::SendMountAccept()
{
	MSG_C2S::stMount_Accept Msg;

	return NetworkMgr->SendPacket(Msg);
}

//拒绝邀请
bool CPacketBuilder::SendMountDecline()
{
	MSG_C2S::stMount_Decline Msg;

	return NetworkMgr->SendPacket(Msg);
}
////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////	
//查询工会信息
bool CPacketBuilder::SendGuildQuery(ui32 guild_id)
{
	MSG_C2S::stGuild_Query Msg;
	Msg.guild_id = guild_id;
	return NetworkMgr->SendPacket(Msg);
}
//创建工会
bool CPacketBuilder::SendCreateGuild(const char * GuildName)
{
	MSG_C2S::stGuild_Create Msg;
	if (GuildName)
	{	
		Msg.guild_name =  GuildName;
	}
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendGuildDisBand()
{
	MSG_C2S::stGuild_Disband Msg;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendLeaveGuild()
{
	MSG_C2S::stGuild_Leave Msg;
	return NetworkMgr->SendPacket(Msg);
}
//邀请加入工会
bool CPacketBuilder::SendGuildInvite(const char * Name)
{
	MSG_C2S::stGuild_Invite Msg;
	if (Name)
	{
		Msg.inviteeName = Name;
	}
	return NetworkMgr->SendPacket(Msg);
}
//同意
bool CPacketBuilder::SendAcceptGuildInvite()
{
	MSG_C2S::stGuild_Accept Msg;
	return NetworkMgr->SendPacket(Msg);
}
//拒绝
bool CPacketBuilder::SendDeclineGuildInvite()
{
	MSG_C2S::stGuild_Decline Msg;
	return NetworkMgr->SendPacket(Msg);
}
//发送工会公告信息
bool CPacketBuilder::SendGuildInfo(const std::string &str)
{
	MSG_C2S::stGuild_Set_Info Msg;
	Msg.NewGuildInfo = str;
	return NetworkMgr->SendPacket(Msg);
}
//提升成员
bool CPacketBuilder::SendPromoteMem(const char * Name)
{
	MSG_C2S::stGuild_Promote Msg;
	if (Name)
	{
		Msg.name = Name;
	}
	return NetworkMgr->SendPacket(Msg);
}
//降低成员
bool CPacketBuilder::SendDemoteMem(const char * Name)
{
	MSG_C2S::stGuild_Demote Msg;
	if (Name)
	{
		Msg.name = Name;
	}
	return NetworkMgr->SendPacket(Msg);
}
//删除成员
bool CPacketBuilder::SendRemoveMem(const char * Name)
{
	MSG_C2S::stGuild_Remove Msg;
	if (Name)
	{
		Msg.name = Name;
	}
	return NetworkMgr->SendPacket(Msg);
}
//设置会长
bool CPacketBuilder::SendSetLeader(const char * Name)
{
	MSG_C2S::stGuild_Set_Leader Msg;
	if (Name)
	{
		Msg.name = Name;
	}
	return NetworkMgr->SendPacket(Msg);
}
//修改职位信息
bool CPacketBuilder::SendSetRank(ui32 rankid,const char * rankName,ui32 IRight)
{
	MSG_C2S::stGuild_Set_Bank Msg;
	Msg.rankId = rankid;
	Msg.newName = rankName;
	Msg.iRights = IRight;
	//暂空
	return NetworkMgr->SendPacket(Msg);
}
//添加职位
bool CPacketBuilder::SendAddRank(const char * RankName)
{
	MSG_C2S::stGuild_Add_Rank Msg;
	if (RankName)
	{
		Msg.rankName = RankName;
	}
	return NetworkMgr->SendPacket(Msg);
}
//删除职位
bool CPacketBuilder::SendDelRank(const char * RankName)
{
	MSG_C2S::stGuild_Del_Rank Msg;
	if (RankName)
	{
		Msg.rankName = RankName;
	}
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGuildRoster()
{
	MSG_C2S::stGuild_Roster Msg;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGuildMemRank(const char* Name, uint8 Rankid)
{
	MSG_C2S::stGuildSetMemberRank Msg;
	Msg.member = Name;
	Msg.rank = Rankid;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGuildDeclareWar(ui32 GuildGuid)
{
	MSG_C2S::stGuildDeclareWar Msg;
	Msg.hostileguild = GuildGuid;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGuildAlien(ui32 GuildGuid)
{
	MSG_C2S::stGuildAllyReq Msg;
	Msg.allianceguild = GuildGuid;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGuildLevelUp()
{
	MSG_C2S::stGuild_Request_Guild_Level_Up Msg;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGuildEmblem(uint64 guid, ui32 EmblemStyle, ui32 EmblemColor)
{
	MSG_C2S::stGuild_Save_Emblem Msg;
	Msg.guid = guid;
	Msg.emblemStyle = EmblemStyle;
	Msg.emblemColor = EmblemColor;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendCastleBuyNpc(uint32 npc_id, uint32 map_id)
{
	MSG_C2S::stBuyCastleNPC Msg;
	Msg.npc_id = npc_id;
	Msg.map_id = map_id;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendCastleQueryState()
{
	MSG_C2S::stQueryCastleState Msg;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendFreshManHelpEnable(ui8 flag)
{
	MSG_C2S::stTutorial_Flag Msg;
	Msg.flag = flag;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendFreshManHelpClear()
{
	MSG_C2S::stTutorial_Clear Msg;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendLogOutMsg()
{
	MSG_C2S::stLogoutReq Msg;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendShopBuyMsg(uint32 id,uint8 buyType,uint8 cnt, const char * gifttoName)
{
	MSG_C2S::stShop_Buy Msg;
	Msg.id = id;
	Msg.buytype = buyType;
	Msg.cnt = cnt;
	if (gifttoName)
	{
		Msg.gift_to_name = gifttoName;
	}
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendShopAddtocategoryMsg(uint32 id,uint8 buyType)
{
	MSG_C2S::stShop_AddToCategory Msg;
	Msg.id = id;
	Msg.buytype = buyType;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendShopGetList()
{
	MSG_C2S::stShop_GetList Msg;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendAreaTrigger(uint32 id)
{
	MSG_C2S::stArea_Trigger Msg;
	Msg.id = id;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendResurrectResponse(uint8 state)
{
	MSG_C2S::stResurrect_Response Msg;
	Msg.status = state;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendResurrectByOther(bool accept)
{
	MSG_C2S::stResurrectByOther Msg;
	Msg.bAccept = accept;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendInstanceQueue(uint32 mapid)
{
	MSG_C2S::stQueueInstance Msg;
 	Msg.mapid = mapid;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendInstanceQuitQueue(uint32 mapid)
{
	MSG_C2S::stLeaveQueueInstance Msg;
	Msg.mapid = mapid;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendInstanceDeadExit(bool bExit)
{
	MSG_C2S::stChoiceInstanceDeadExit Msg;
	Msg.bExit = bExit;
	return NetworkMgr->SendPacket(Msg);	
}

//////////////////////////////////////////////////////////////////////////
// 捐助
//////////////////////////////////////////////////////////////////////////
bool CPacketBuilder::SendQueryContributionPlayerMsg(std::string strName)
{
	MSG_C2S::stQueryContributionPlayer Msg;
	Msg.name = strName;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendContributionConfirmMsg(uint32 coins, uint32 points)
{
	MSG_C2S::stContributionConfirm Msg;
	Msg.coins = coins;
	Msg.points = points;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendQuerySelContributionHistoryMsg()
{
	MSG_C2S::stQuerySelfContributionHistory Msg;
	
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendQueryContributionBillboardMsg(uint8 type)
{
	MSG_C2S::stQueryContributionBillboard Msg;
	Msg.type = type;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendEnterLeavePVPZone(bool bEnter, uint8 area/*=1*/)
{
	MSG_C2S::stEnterLeavePVPZone Msg;
	Msg.value = bEnter?area:0;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendZoneUpdate(unsigned int uiAreaId)
{
    MSG_C2S::stZone_Update Msg;
    Msg.newZone = uiAreaId;

    return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendAddRefineMaterial(ui8 BagSlot, ui8 Slot, ui8 refineSlot, BOOL to_inventory)  //添加材料
{
	MSG_C2S::stJingLianContainerMoveItemReq Msg;
	Msg.container_slot = BagSlot;
	Msg.slot = Slot;
	Msg.jinglian_container_slot = refineSlot;
	Msg.to_inventory = to_inventory;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendRefineEqu()  //发送消息
{
	MSG_C2S::stJingLianItemReq Msg;

	return NetworkMgr->SendPacket(Msg);
}	


bool CPacketBuilder::SendXiangQianMaterial()// 材料
{
	MSG_C2S::stXiangqianContainerMoveItemReq Msg;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendXiangQian() //发送镶嵌消息
{
	MSG_C2S::stXiangqianItemReq Msg ;

	return NetworkMgr->SendPacket(Msg);
}



bool CPacketBuilder::SendPetsListStabled()
{
	MSG_C2S::stPets_List_Stabled Msg;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendPetUnStable(ui32 pet_number)
{
	MSG_C2S::stPet_UnStable Msg;
	Msg.pet_number = pet_number;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendPetStable()
{
	MSG_C2S::stPet_Stable Msg;

	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendPetReName(ui64 guid, const char* Name)
{
	MSG_C2S::stPet_Rename Msg;
	Msg.guid = guid;
	Msg.name = Name;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendRandomRoll(uint32 min, uint32 max)
{
	MSG_C2S::stMsg_Random_Roll Msg;
	Msg.min = min > max ? max : min;
	Msg.max = max;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendQueryServerTime()
{
	MSG_C2S::stServerTime Msg;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendZhaoTuDi(string name) //发送请求
{
	MSG_C2S::stRecruitReq  Msg;
	Msg.student = name ;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendTuDiFanKui(bool accept)		//是否同意拜师 
{
	MSG_C2S::stRecruitReply  Msg;
	Msg.accept = accept;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendLadderReq()
{
	MSG_C2S::stLadderReq Msg;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendShowShiZhuang(uint8 uiShow)
{
	MSG_C2S::stItem_Show_Shizhuang Msg;
	Msg.bShow = uiShow;
	return NetworkMgr->SendPacket(Msg);
}

//称谓
bool CPacketBuilder::SendChangTitle(ui32 TitleIndex)
{
	MSG_C2S::stChooseTitle Msg;
	Msg.title_index = TitleIndex;
	
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendQueryPlayers(uint8 min_level, uint8 max_level, uint16 mapid, uint8 race, uint8 cls, std::string& str)
{
	MSG_C2S::stQueryPlayers Msg;
	Msg.min_level = min_level;
	Msg.max_level = max_level;
	Msg.mapid = mapid;
	Msg.race = race;
	Msg.cls = cls;
	Msg.str = str;

	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendQueryDonationLadder(uint32 event_id, bool is_total)
{
	MSG_C2S::stQueryDonationLadder Msg;
	Msg.event_id = event_id;
	Msg.is_total = is_total;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendDonateReq(uint32 event_id, uint32 yuanbao, const char* leave_word )
{
	MSG_C2S::stDonateReq Msg;
	Msg.event_id = event_id;
	Msg.yuanbao = yuanbao;
	//Msg.leave_words = leave_word;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendCreditApply()
{
	MSG_C2S::stCreditApplyReq Msg;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendCreditAction(uint32 yuanbao, uint8 is_restore)
{
	MSG_C2S::stCreditActionReq Msg;
	Msg.yuanbao = yuanbao;
	Msg.is_restore = is_restore;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendQueryCreditHistoryReq(uint32 from, uint32 to)
{
	MSG_C2S::stQueryCreditHistoryReq Msg;
	Msg.from = from;
	Msg.to = to;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendSerialForGift(string serial)
{
	MSG_C2S::stSerialCardForGift Msg;
	Msg.serial = serial;
	return NetworkMgr->SendPacket(Msg);
}


bool CPacketBuilder::SendAppearReq( const char* szName, ui64 sourceguid )
{
	MSG_C2S::stAppearItemReq Msg;
	Msg.sourceGUID =sourceguid;
	Msg.targetName = szName;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendSocketGems(ui64 itemguid, std::vector<ui64>& vGemsguid)
{
	MSG_C2S::stItem_Socket_Gems Msg;
	Msg.itemguid = itemguid;
	Msg.vGemsguid = vGemsguid;
	return NetworkMgr->SendPacket(Msg);
}
bool CPacketBuilder::SendSkillUnLearn(uint32 skillline)
{
	MSG_C2S::stSkill_UnLearn Msg;
	Msg.skill_line = skillline;
	return NetworkMgr->SendPacket(Msg);

}


bool CPacketBuilder::SendGroupApplyForJoining(ui64 guid)
{
	MSG_C2S::stGroupApplyForJoining Msg;
	Msg.guid = guid;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGroupApplyForJoiningAck(std::string& name, ui8 type)
{
	MSG_C2S::stGroupApplyForJoiningAck Msg;
	Msg.name = name;
	Msg.Type = type;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGroupListReq(uint64 mapid)
{
	MSG_C2S::stGroupListReq Msg;
	Msg.id = mapid;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGroupModifyTitleReq(std::string& Title)
{
	MSG_C2S::stGroupModifyTitleReq Msg;
	Msg.title = Title;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGroupModifyCanApplyForJoining(bool bOpen)
{
	MSG_C2S::stGroupModifyCanApplyForJoining Msg;
	Msg.bOpen = bOpen;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendGroupApplyForJoiningStateReq()
{
	MSG_C2S::stGroupApplyForJoiningStateReq Msg;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendAgreeSummon( )
{
	MSG_C2S::stSummon_Response Msg;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendFallSpeed( float speed )
{
	MSG_C2S::stFallSpeedOnLand Msg;
	Msg.speed = speed;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendSpellCancelAutoRepeat()
{
	MSG_C2S::stSpell_Cancel_Auto_Repeat Msg;
	return NetworkMgr->SendPacket(Msg);
}

bool CPacketBuilder::SendItemBuyBack(ui64 vendorguid,ui32 slot)
{
	//发这个消息可以回购物品。 slot 是0 ~ 11
	MSG_C2S::stItem_BuyBack Msg;
	Msg.vendorguid = vendorguid;
	Msg.slot = slot;
	return NetworkMgr->SendPacket(Msg);
}