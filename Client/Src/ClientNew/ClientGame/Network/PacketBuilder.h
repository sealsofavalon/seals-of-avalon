#pragma once

#include "Player.h"
#include "../../../../SDBase/Protocol/C2S_Mail.h"

class CPacketBuilder : public NiMemObject
{
public:
	CPacketBuilder();

	//连接登录服务器
	//发送当前客户端的版本号
	bool SendVersionMsg(int iVersion);

	//发送加密后的账号和密码
	bool SendLoginReqMsg( unsigned int tablekey );

	//发送选组消息
	bool SendGroupSelReqMsg(ui8 id);
	//取消选组的消息。
	bool SendCanelGroupMsg();

	//连接GateServer
	//发送登录游戏服务器消息
	bool SendGTLoginReqMsg();

	//创建角色
	bool SendCreateRoleReqMsg(const std::string& strRoleName, ui8 ucSex, ui8 nRace, ui8 nClass);

	//删除角色
	bool SendDeleteRoleReqMsg(ui64 guid);

	//取消排队
	bool SendCancelEnterGameLine();	
	
	//请求进入游戏
	bool SendEnterGameReqMsg(std::string& strPlayerName, ui64 guid);
	bool SendExitGameReqMsg();

	bool SendLoadingOKMsg();
	//攻击消息
	bool SendAttackReqMsg(CGameObject* pkTarget);
	bool SendAttackStopMsg();
	// 释放技能,使用SendAttackReqMsg释放技能函数有参数问题,所以用这个函数替换
	bool SendCastSpell(ui32 nSpellID, const SpellCastTargets& targets);

	bool SendLeapPrepar(NiPoint3 pos, float fz);

	//请求设置技能ID.
	bool SendSetActionButton(ui8 pos, ui32 nSpellID,UINT nSpellType);
	// 中断当前技能释放
	bool SendCancelSpell();

	// 停止buf
	bool SendCancelAura(ui32 nSpellID);

	//请求复活消息
	//bool SendReliveReqMsg();

	//聊天消息
	bool SendChatReqMsg(ChatMsg msg,const char* pcMsg);
	bool SendPrivateChatMsg(const char *pcMsg,ui64 guid,const char* Name);

	//拾取物品消息
	bool SendMapItemPickUpReqMsg(ui64 mapitemguid);

	//丢弃背包道具到地图的请求
	bool SendMapItemDropReqMsg(ui8 bag, ui8 slot);

	//拆分道具
	bool SendSplitItem(ui8 srcbag, ui8 srcslot, ui8 dstbag, ui8 dstslot, ui8 count);

	//物品修理消息 
	//当ItemGuid = 0  为修理全部
	bool SendRepairItem(ui64 NPCGuid, ui64 ItemGuid);

	//移动背包道具位置的请求
	bool SendItemMovePosReqMsg(ui8  srcbag, ui8 dstbag ,ui8 srcslot, ui8 dstslot, WORD wNum);

	//交易请求
	//bool SendMapTradeNpcRegMsg(unsigned int MapNpcId ,std::string& strkind,std::string& strAdd);
	bool SendNpcClikedMsg(ui64 Npcguid);

	//物品购买请求
	bool SendItemBuyReqMsg(ui64 vecdorguid, ui32 dwItemId, ui8 byNum, ui32 amt);

	//物品卖出请求
	bool SendItemSellReqMsg(ui64 vecdorguid, ui64 itemguid, ui8 byNum);

	//物品使用请求
	bool SendItemUseReqMsg(ui8 bag, ui8 slot, ui64 item_guid, SpellCastTargets targets);
	bool SendUnBindReqMsg( ui64 source, uint64 targetguid );

	bool SendEquipItemMsg(ui8 bag, ui8 slot);

	// 选中
	bool SendSetSelectionMsg(uint64 Selectguid, bool toattack);

	// 
	bool SendSetTargetMsg(uint64 Targetguid);

	bool SendPing(uint32 t);
	bool SendPingIndex(uint32 n);

	bool SendNPCTrainerBuySpell(ui64 guid, ui32 TeachSpellId);
//////////////////////////////////////////////////////////////////////////
//	Player Trade Msg
//////////////////////////////////////////////////////////////////////////
	// 拿掉一个道具
	bool SendTradeClearItemMsg(ui8 ucTradeSlot);
	// 增加一个道具
	bool SendTradeSetItemMsg(ui8 ucTradeSlot, ui8 ucBag, ui8 ucSlot);
	// 金钱
	bool SendTradeSetGoldMsg(ui32 uiGold);
	// 交易请求
	bool SendTradeInitMsg(ui64 Targetguid);
	// 交易同意
	bool SendTradeBeginMsg();
	//锁定交易
	bool SendTradeLockMsg();
	// 交易确认
	bool SendTradeAcceptMsg();
	
	// 交易取消
	bool SendTradeCancelMsg();
	// 交易拒绝
	bool SendTradeBusyMsg();
	// 交易拒绝
	bool SendTradeIgnoreMsg();
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//  任务相关
//////////////////////////////////////////////////////////////////////////
	// 请求任务状态
	bool SendNpcGossipStatus(ui64 guid);
	// 与任务npc对话
	bool SendNpcGossipHelloMsg(ui64 guid);
	// 选择一个选项
	bool SendNpcGossipSelectOptionMsg(ui64 guid, ui32 option, string str = "");
	// 与任务NPC交互
	bool SendQuestGiverHelloMsg(ui64 guid);
	// 接受任务
	bool SendQuestGiverAcceptQuestMsg(ui64 guid, ui32 guestid);
	// 拒绝任务
	bool SendQuestGiverCancelMsg();
	// 完成任务
	bool SendQuestGiverCompleteQuestMsg(ui64 guid, ui32 questid);
	// 共享任务
	bool SendQuestPushToPartyMsg(ui32 questid);
	// 任务完成时选择奖励物品
	bool SendQuestGiverChooseRewardMsg(ui64 guid, ui32 questid, ui32 rewardslot);
	// 任务日志 删除
	bool SendQuestLogRemoveQuestMsg(ui8 questslot);


	//组队护送任务
	bool SendstQuestEscortReply(uint8 is_accept, uint32 quest_id, uint32 npc_id);
///////////////////////////////////////////////////////////////////////
//  组队相关
///////////////////////////////////////////////////////////////////////
	//组队请求
	bool SendGroupInviteMsg(const char * Name);
	//加入队伍
	/*bool SendAddGroupMemMsg(){};*/
	//接受组队
	bool SendGroupAcceptMsg();
	//拒绝组队
	bool SendRefuseGroupMsg();
	//移除队员
	bool SendRemoveMemberMsg(ui64 PlayerGuid);
	bool SendRemoveMemberMsg(const char * membername);

	//bool SendSetGroupLeaderMsg(ui64 PlayerGuid);
	//移交队长	//设置队长
	bool SendReSetGroupLeaderMsg(ui64 PlayerGuid);
	bool SendChangeMemberIcon(ui64 guid, ui8 icon);
	bool SendChangeToTeam();
	bool SendChangeMemberSubGroup(const char* name, ui8 groupid);
	bool SendChangeMemberSubGruopByGuid(ui64 desguid, ui64 srcguid); 
	bool SendMemberReady(ui8 ready);
	//队伍拾取分配制度
	bool SendTeamPickSetting(ui32 lootMethod, ui32 threshold);
	//离开队伍
	//bool SendLeaveGroupMsg(){};
	//解散队伍
	bool SendGroupDisbandMsg();

////////////////////////////////////////////////////////////////
//	拾取相关
////////////////////////////////////////////////////////////////
	bool SendStartLoot(ui64 guid);				//	捡尸体
	bool SendLootOneItem(ui32 slot);			//	拾取尸体中的一个道具
	bool SendReleaseOneItem(ui64 guid);			//	捡开尸体后,放弃拾取
	bool SendLootMoney();						//	拾取金币
	bool SendStartRoll(uint64 creatureguid, uint32 slotid, uint8 choice, ui32 mapid ,ui32 instanceid);

////////////////////////////////////////////////////////////////
//	好友相关
////////////////////////////////////////////////////////////////
	// 请求好友列表
	bool SendFriendListReqMsg(uint32 uiFlag);
	// 添加好友
	bool SendFriendAddMsg(string& strName, string& strNote);
	// 删除好友
	bool SendFriendDelMsg(uint64 guid);
	// 
	bool SendFriendSetNote(uint64 guid, string& strNote);

	bool SendFriendAddIgnore(string& strIgnoreName);
	bool SendFriendDelIgnore(uint64 guid);
//////////////////////////////////////////////////////////////////////////
//	拍卖相关
//////////////////////////////////////////////////////////////////////////
	//出售物品，提交拍卖
	// bid是竞标价 buyout是一口价
	bool SendAuctionSellItemMsg(uint64 guid, uint64 item, uint32 bid, uint32 buyout, uint32 etime, bool bYP, bool bYuanBao);
	// 取消拍卖
	bool SendAuctionRemoveItemMsg(uint64 guid, uint32 auctionid);
	// 列出拍卖行道具
	bool SendAuctionListItemsMsg(
		ui64 vendorguid, 
		uint32 startIndex, 
		std::string& auctionString,
		uint8 levelRange1,  uint8 levelRange2, uint8 usableCheck,
		int32 inventoryType, int32 itemclass, int32 itemsubclass, int32 rarityCheck);
	// 列出自己拍卖的道具
	bool SendAuctionListOwnerItems(ui64 vendorguid);
	// 一口价买进
	bool SendAuctionPlaceBidMsg(ui64 guid, uint32 aucitonId, uint32 price);
	// 列出自己购买的道具
	bool SendAuctionListBidderItemsMsg(uint64 vendorguid);
	// npc交互，消息
	bool SendAuctionHelloMsg(uint64 vendorguid, ui32 auctionhouse_id);

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
	// 请求基本信息
	bool SendQueryNameMsg(ui64 guid);
//////////////////////////////////////////////////////////////////////////
///////////////  邮件相关                                   //////////////
//////////////////////////////////////////////////////////////////////////
	bool SendRequestMailList();
	bool SendRequestMailContent(uint32 messageId);   //请求查询邮件内容
	bool SendMail(MSG_C2S::stMail_Send mail);//发送邮件
	bool SendReadMail(uint32 messageId); //请求标记邮件已读
	bool SendDeleteMail(uint32 messageId); // 请求删除邮件
	bool SendGetMailItem(uint32 messageId, uint32 lowid);//请求提取邮件附件的道具
	bool SendGetMailMoney(uint32 messageId);//请求提取附件里的金钱
	bool SendReturnToSender(uint32 messageId);//请求退回邮件
	bool SendGetMailPaper(uint32 messageId);//获取邮件信纸
	bool SendRequestNeedRead();//发送请求获得未读列表信息

	//////////////////////////////////////////////////////////////////////////
	/////////////////////////     银行操作相关        ////////////////////////
	//////////////////////////////////////////////////////////////////////////

	bool SendBuyBankBagPos();
	//-----------------------------------------------------------------------
	//                          频道相关
	//-----------------------------------------------------------------------
	bool SendChannelJoin(const char * channelname,uint32 dbc_id = 0,const char * password = NULL);
	bool SendchannelLeave(const char * channelname,uint32 code = 0);

//////////////////////////////////////////////////////////////////////////
//	Mout
//////////////////////////////////////////////////////////////////////////
	//被骑的人邀请骑乘的人
	bool SendMountInvite(ui64 guid);
	//解散队伍
	bool SendMountDisband();
	//同意邀请
	bool SendMountAccept();
	//拒绝邀请
	bool SendMountDecline();
	//////////////////////////////////////////////////////////////////////////
	//	Guild
	//////////////////////////////////////////////////////////////////////////
	//查询工会信息
	bool SendGuildQuery(ui32 guild_id);
	//创建工会
	bool SendCreateGuild(const char * GuildName);
	//解散工会
	bool SendGuildDisBand();
	//退出工会
	bool SendLeaveGuild();
	//邀请加入工会
	bool SendGuildInvite(const char * Name);
	//同意
	bool SendAcceptGuildInvite();
	//拒绝
	bool SendDeclineGuildInvite();
	//发送工会公告信息
	bool SendGuildInfo(const std::string &str);
	//提升成员
	bool SendPromoteMem(const char * Name);
	//降低成员
	bool SendDemoteMem(const char * Name);
	//删除成员
	bool SendRemoveMem(const char * Name);
	//设置会长
	bool SendSetLeader(const char * Name);
	//修改职位信息
	bool SendSetRank(ui32 rankid,const char * rankName,ui32 IRight);
	//添加职位
	bool SendAddRank(const char * RankName);
	//删除职位
	bool SendDelRank(const char * RankName);
	//申请工会消息
	bool SendGuildRoster();
	//修改成员职位
	bool SendGuildMemRank(const char* Name, uint8 Rankid);
	//宣战
	bool SendGuildDeclareWar(ui32 GuildGuid);
	//同盟
	bool SendGuildAlien(ui32 GuildGuid);
	//工会升级
	bool SendGuildLevelUp();
	//工会徽章
	bool SendGuildEmblem(uint64 guid, ui32 EmblemStyle, ui32 EmblemColor);
	//工会攻城战部分
	bool SendCastleBuyNpc(uint32 npc_id, uint32 map_id);
	bool SendCastleQueryState();

	bool SendFreshManHelpEnable(ui8 flag);
	bool SendFreshManHelpClear();

	bool SendLogOutMsg();

	//////////////////////////////////////////////////////////////////////////
	//	Shop
	//////////////////////////////////////////////////////////////////////////
	bool SendShopBuyMsg(uint32 id,uint8 buyType,uint8 cnt = 1, const char * gifttoName = NULL);
	bool SendShopAddtocategoryMsg(uint32 id,uint8 buyType);
	bool SendShopGetList();

	bool SendAreaTrigger(uint32 id);
	/////////////////////////////////////////////////////////////////////////
	// 复活
	/////////////////////////////////////////////////////////////////////////
	bool SendResurrectResponse(uint8 state);
	bool SendResurrectByOther(bool accept);
	/////////////////////////////////////////////////////////////////////////
	// 副本
	/////////////////////////////////////////////////////////////////////////
	bool SendInstanceQueue(uint32 mapid);
	bool SendInstanceQuitQueue(uint32 mapid);
	bool SendInstanceDeadExit(bool bExit);
	//////////////////////////////////////////////////////////////////////////
	// 捐助
	//////////////////////////////////////////////////////////////////////////
	bool SendQueryContributionPlayerMsg(std::string strName);
	bool SendContributionConfirmMsg(uint32 coins, uint32 points);
	bool SendQuerySelContributionHistoryMsg();
	bool SendQueryContributionBillboardMsg(uint8 type);

	bool SendEnterLeavePVPZone(bool bEnter, uint8 area = 1);
    bool SendZoneUpdate(unsigned int uiAreaId);

	//////////////////////////////////////////////////////////////////////////
	// 打造和合成
	//////////////////////////////////////////////////////////////////////////
	bool SendAddRefineMaterial(ui8 BagSlot, ui8 Slot, ui8 refineSlot, BOOL to_inventory);  //添加材料
	bool SendRefineEqu();  //发送消息


	bool SendXiangQianMaterial();// 材料
	bool SendXiangQian(); //发送镶嵌消息

	//////////////////////////////////////////////////////////////////////////
	// 宠物相关
	//////////////////////////////////////////////////////////////////////////
	bool SendPetsListStabled();  //请求宠物列表
	bool SendPetUnStable(ui32 pet_number);
	bool SendPetStable();
	bool SendPetReName(ui64 guid, const char* Name);
	//////////////////////////////////////////////////////////////////////////
	bool SendRandomRoll(uint32 min, uint32 max);

	bool SendQueryServerTime();



	//////////////////////////////////////////////////////////////////////////
	//// 师徒相关														//////
	//////////////////////////////////////////////////////////////////////////

	bool SendZhaoTuDi(const string name); //发送请求
	bool SendTuDiFanKui(bool accept);		//是否同意拜师 

	//排行榜
	bool SendLadderReq();

	//时装
	bool SendShowShiZhuang(uint8 uiShow);

	//称谓
	bool SendChangTitle(ui32 TitleIndex);

	//在线查询
	bool SendQueryPlayers(uint8 min_level, uint8 max_level, uint16 mapid, uint8 race, uint8 cls, std::string& str);

	//////////////////////////////////////////////////////////////////////////
	// 捐助 new
	//////////////////////////////////////////////////////////////////////////
	bool SendQueryDonationLadder(uint32 event_id, bool is_total);

	bool SendDonateReq(uint32 event_id, uint32 yuanbao, const char* leave_word = NULL);

	bool SendCreditApply();
	bool SendCreditAction(uint32 yuanbao, uint8 is_restore);
	bool SendQueryCreditHistoryReq(uint32 from, uint32 to);

	//////////////////////////////////////////////////////////////////////////
	// 激活码
	//////////////////////////////////////////////////////////////////////////

	bool SendSerialForGift(string serial);

	bool SendAppearReq( const char* szName, ui64 sourceguid );

	bool SendSocketGems(ui64 itemguid, std::vector<ui64>& vGemsguid);

	bool SendSkillUnLearn(uint32 skillline);

	//////////////////////////////////////////////////////////////////////////
	//申请组队
	//////////////////////////////////////////////////////////////////////////

	bool SendGroupApplyForJoining(ui64 guid);
	bool SendGroupApplyForJoiningAck(std::string& name, ui8 type);
	bool SendGroupListReq(uint64 mapid);
	bool SendGroupModifyTitleReq(std::string& Title);
	bool SendGroupModifyCanApplyForJoining(bool bOpen);
	bool SendGroupApplyForJoiningStateReq();
	bool SendAgreeSummon();
	bool SendFallSpeed( float speed );

	bool SendSpellCancelAutoRepeat();
	bool SendItemBuyBack(ui64 vendorguid,ui32 slot);
protected:

	ui32 m_indexPing;
};

