#include "stdafx.h"
#include "ui/UIGamePlay.h"
#include "PacketParser.h"
#include "PacketBuilder.h"
#include "ClientState.h"
#include "UI/UISystem.h"
#include "UI/UILogin.h"
#include "UI/UIChoosePlayer.h"
#include "UI/UMessageBox.h"
#include "UI/UIGamePlay.h"
#include "UI/UIShowPlayer.h"
#include "ui/UPickItemSet.h"
#include "ui/SystemTips .h"
#include "Map/Map.h"
#include "../ItemManager.h"
#include "SYItem.h"
#include "ObjectManager.h"
#include "SceneManager.h"
#include "NetworkManager.h"
#include "Skill/SkillManager.h"
#include "Target.h"
#include "PlayerInputMgr.h"
#include "Effect\EffectManager.h"
#include "../ui/UNiAVControl.h"
#include "../../../../SDBase/Protocol/S2C_Loot.h"
#include "../../../../SDBase/Protocol/S2C_Auction.h"
#include "../../../../SDBase/Protocol/S2C.h"
#include "../../../../SDBase/Protocol/S2C_Mail.h"
#include "../../../../SDBase/Public/MailDef.h"
#include "../../../../SDBase/Protocol/S2C_Contribution.h"
#include "../../../../SDBase/Protocol/S2C_Serial_for_gift.h"
#include "Utils/SpellDB.h"
#include "Console.h"
#include "UI/UAuraSys.h"
#include "UI/UChat.h"
#include "UI/UINPCTrade.h"
#include "UI/UIBank.h"
#include "UI/UNpcQuestDlg.h"
#include "UI/UIPackage.h"
#include "UI/UIPlayerTrade.h"
#include "UI/UNpcDlg.h"
#include "UI/UISkill.h"
#include "UI/UIPickItemList.h"
#include "UI/UAuctionDlg.h"
#include "UI/UFriendsDlg.h"
#include "UI/UIMailSend.h"
#include "UI/UIMailReceiveList.h"
#include "UI/UIMailContent.h"
#include "UI/ShopSystem.h"
#include "UI/UInputAmount.h"
#include "ui/PlayerData.h"
#include "ui/UQuestLog.h"
#include "../ClientApp.h"
#include "Utils/ChatFileterDB.h"
#include "ui/UReliveMsgBox.h"
#include "ui/UEndowment.h"
#include "ui/SystemSetup.h"
#include "ui/UIChooseServer.h"
#include "ui/SystemSetup.h"
#include "ui/UIMiniMap.h"
#include "ui/UInGameBar.h"
#include "ui/UEquRefineManager.h"
#include "ui/UFun.h"
#include "ui/FreshManHelp.h"
#include "ui/SocialitySystem.h"
#include "ui/UITipSystem.h"
#include "AudioInterface.h"
#include "ui/TeacherSystem.h"
#include "SyServerTime.h"
#include "ui/UInstanceMgr.h"
#include "ui/UAuctionMgr.h"
#include "ui/UMailSystem.h"
#include "ui/TitleMgr.h"
#include "ui/RankMgr.h"
#include "Utils/AreaDB.h"
#include "Hook/HookManager.h"
#include "ui/SocialitySystem.h"
#include "UI/UICastBar.h"
#include "ui/UTeamSYS.h"
#include "ui/UIWaitList.h"
#include "DpsCount.h"
#include "Utils/MapInfoDB.h"

const char * szMonthNames[12] = {
	"1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"
};

void MakeIntString(char * buf, int num)
{
	if(num<10)
	{
		buf[0] = '0';
		//itoa(num, &buf[1], 10);
		sprintf(&buf[1], "%u", num);
	}
	else
	{
		//itoa(num,buf,10);
		sprintf(buf,"%u",num);
	}
}

void MakeIntStringNoZero(char * buf, int num)
{
	//itoa(num,buf,10);
	sprintf(buf,"%u",num);
}

string ConvertTimeStampToString(uint32 timestamp)
{
	int seconds = (int)timestamp;
	int mins=0;
	int hours=0;
	int days=0;
	int months=0;
	int years=0;
	if(seconds >= 60)
	{
		mins = seconds / 60;
		if(mins)
		{
			seconds -= mins*60;
			if(mins >= 60)
			{
				hours = mins / 60;
				if(hours)
				{
					mins -= hours*60;
					if(hours >= 24)
					{
						days = hours/24;
						if(days)
						{
							hours -= days*24;
							if(days >= 30)
							{
								months = days / 30;
								if(months)
								{
									days -= months*30;
									if(months >= 12)
									{
										years = months / 12;
										if(years)
										{
											months -= years*12;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	char szTempBuf[100];
	string szResult;

	if(years) {
		MakeIntStringNoZero(szTempBuf, years);
		szResult += szTempBuf;
		szResult += _TRAN(" 年, ");
	}

	if(months) {
		MakeIntStringNoZero(szTempBuf, months);
		szResult += szTempBuf;
		szResult += _TRAN(" 月, ");
	}

	if(days) {
		MakeIntStringNoZero(szTempBuf, days);
		szResult += szTempBuf;
		szResult += _TRAN(" 日, ");
	}

	if(hours) {
		MakeIntStringNoZero(szTempBuf, hours);
		szResult += szTempBuf;
		szResult += _TRAN(" 小时, ");
	}

	if(mins) {
		MakeIntStringNoZero(szTempBuf, mins);
		szResult += szTempBuf;
		szResult += _TRAN(" 分钟, ");
	}

	if(seconds) {
		MakeIntStringNoZero(szTempBuf, seconds);
		szResult += szTempBuf;
		szResult += _TRAN(" 秒");
	}

	return szResult;
}

string ConvertTimeStampToDataTime(uint32 timestamp)
{
	char szTempBuf[100];
	time_t t = (time_t)timestamp;
	tm * pTM = localtime(&t);

	string szResult;
	MakeIntString(szTempBuf, pTM->tm_year+1900);
	szResult += szTempBuf;
	szResult += _TRAN("年");

	szResult += _TRAN(szMonthNames[pTM->tm_mon]);
	MakeIntString(szTempBuf, pTM->tm_mday);
	szResult += szTempBuf;
	szResult += _TRAN("日");

	MakeIntString(szTempBuf, pTM->tm_hour);
	szResult += szTempBuf;
	szResult += _TRAN("时");
	MakeIntString(szTempBuf, pTM->tm_min);
	szResult += szTempBuf;
	szResult += _TRAN("分");

	//MakeIntString(szTempBuf, pTM->tm_sec);
	//szResult += szTempBuf;

	return szResult;
}
CPacketParser::CPacketParser(){}

void CPacketParser::Init()
{
	//
	m_opcodeTable[MSG_S2C::QUERY_RESPONSE]		= OpcodeHandler(&CPacketParser::ParseQueryNameResMsg);
	m_opcodeTable[MSG_S2C::MSG_ENTER_GAME_LINE_NUM] = OpcodeHandler(&CPacketParser::ParseEnterGameNum);
	m_opcodeTable[MSG_S2C::MSG_ENTER_GAME_WAIT_OK] = OpcodeHandler(&CPacketParser::ParseEnterGameWaitOK);
	
	m_opcodeTable[MSG_LS2C::VERSION_ACK]		= OpcodeHandler(&CPacketParser::ParseVersionMsg);
	m_opcodeTable[MSG_LS2C::LOGIN_ACK]			= OpcodeHandler(&CPacketParser::ParseLoginMsg);
	m_opcodeTable[MSG_S2C::MSG_SERVER_GROUP_LIST_ACK]		= OpcodeHandler(&CPacketParser::ParseGroupListMsg);

	//游戏服务器的消息(GateServer)
	m_opcodeTable[MSG_S2C::LOGIN_ACK]			= OpcodeHandler(&CPacketParser::ParseGTLoginMsg);
	m_opcodeTable[MSG_S2C::ROLELIST_ACK]		= OpcodeHandler(&CPacketParser::ParseRoleListMsg);
	m_opcodeTable[MSG_S2C::CREATEROLE_ACK]		= OpcodeHandler(&CPacketParser::ParseCreateRoleMsg);
	m_opcodeTable[MSG_S2C::DELROLE_ACK]			= OpcodeHandler(&CPacketParser::ParseDeleteRoleMsg);

	m_opcodeTable[MSG_S2C::ENTERGAME_ACK]		= OpcodeHandler(&CPacketParser::ParseEnterGameMsg);
	m_opcodeTable[MSG_S2C::MOVE_NEW_WORLD]      = OpcodeHandler(&CPacketParser::ParseMoveNewWorld);
	m_opcodeTable[MSG_S2C::TRANSFER_ABORTED]      = OpcodeHandler(&CPacketParser::ParseTransferAbouted);
	m_opcodeTable[MSG_S2C::MSG_MESSAGE_BOX]     = OpcodeHandler(&CPacketParser::ParseMsgBoxFormServer);

	m_opcodeTable[MSG_S2C::MOVE_OP]				= OpcodeHandler(&CPacketParser::ParsePlayerMoveMsg);
	m_opcodeTable[MSG_S2C::MONSTER_MOVE]		= OpcodeHandler(&CPacketParser::ParseMonsterMoveMsg);
	m_opcodeTable[MSG_S2C::MOVE_FORCE_ROOT]		= OpcodeHandler(&CPacketParser::ParseMoveForceRoot);
	m_opcodeTable[MSG_S2C::MOVE_FORCE_UNROOT]	= OpcodeHandler(&CPacketParser::ParseMoveForceUnRoot);
	m_opcodeTable[MSG_S2C::AI_SWITCH_ACTIVE_MOVER] = OpcodeHandler(&CPacketParser::ParseAISwitchMove);

	m_opcodeTable[MSG_S2C::AI_DIED]				= OpcodeHandler(&CPacketParser::ParseDeathMsg);
	m_opcodeTable[MSG_S2C::RELIVE_ACK]			= OpcodeHandler(&CPacketParser::ParseReliveAckMsg);
	m_opcodeTable[MSG_S2C::RELIVE_PKG]			= OpcodeHandler(&CPacketParser::ParseReliveMsg);
	m_opcodeTable[MSG_S2C::SPELL_RESURRECT_REQUEST] = OpcodeHandler(&CPacketParser::ParseSpellResurrectReq);

	m_opcodeTable[MSG_S2C::CHAT_MESSAGE]		= OpcodeHandler(&CPacketParser::ParseChatMsg);
	m_opcodeTable[MSG_S2C::CHAT_PLAYER_NOT_FOUND] = OpcodeHandler(&CPacketParser::ParseNotFoundPlayer);
	m_opcodeTable[MSG_S2C::MSG_SYSTEM_NOTIFY_ENUM] = OpcodeHandler(&CPacketParser::ParseSystemNotify);
	m_opcodeTable[MSG_S2C::MOVE_TELEPORT_ACK] = OpcodeHandler(&CPacketParser::ParseMoveTeleport);

	//m_opcodeTable[MSG_S2C::TRADELIST]			= OpcodeHandler(&CPacketParser::ParseNpcMsg); //message from the NPC

	//////////////////////////////////////////////////////////////////////////
	//	New Msg
	//////////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::UPDATE_OBJECT]		= OpcodeHandler(&CPacketParser::ParseObjectUpdate);
	m_opcodeTable[MSG_S2C::ACCOUNT_EXPIRE_IN_FEW_MINUTES] = OpcodeHandler(&CPacketParser::ParseAccountExpireInFewMinutes);

	m_opcodeTable[MSG_S2C::LEVELUP_INFO]					= OpcodeHandler(&CPacketParser::ParseLevelUpInfo);
	m_opcodeTable[MSG_S2C::MOVE_SET_SPEED]		= OpcodeHandler(&CPacketParser::ParseSpeedChange);
	

	//////////////////////////////////////////////////////////////////////////
	//	Item
	//////////////////////////////////////////////////////////////////////////

	// Item
	m_opcodeTable[MSG_S2C::LIST_INVENTORY]					= OpcodeHandler(&CPacketParser::ParseItemInventoryMsg);
	m_opcodeTable[MSG_S2C::ITEM_BUY]						= OpcodeHandler(&CPacketParser::ParseItemBuyMsg);
	m_opcodeTable[MSG_S2C::ITEM_BUY_FAILURE]				= OpcodeHandler(&CPacketParser::ParseItemBuyFailureMsg);
	m_opcodeTable[MSG_S2C::ITEM_SELL_FAILURE]				= OpcodeHandler(&CPacketParser::ParseItemSellFailureMsg);
	m_opcodeTable[MSG_S2C::ITEM_INVENTORY_CHANGE_FAILURE]	= OpcodeHandler(&CPacketParser::ParseItemInventoryChangeFailureMsg);
	m_opcodeTable[MSG_S2C::ITEM_SELL]						= OpcodeHandler(&CPacketParser::ParseItemSellMsg);
	m_opcodeTable[MSG_S2C::ITEM_READ_OK]					= OpcodeHandler(&CPacketParser::ParseItemReadOkMsg);
	m_opcodeTable[MSG_S2C::ITEM_QUERY_SINGLE_RESPONSE]		= OpcodeHandler(&CPacketParser::ParseItemQuerySingleResponseMsg);
	m_opcodeTable[MSG_S2C::ITEM_PUSH_RESULT]				= OpcodeHandler(&CPacketParser::ParseItemPushResultMsg);
	m_opcodeTable[MSG_S2C::ITEM_NAME_QUERY_RESPONSE]		= OpcodeHandler(&CPacketParser::ParseItemNameQueryResponseMsg);
	m_opcodeTable[MSG_S2C::ITEM_TIME_UPDATE]				= OpcodeHandler(&CPacketParser::ParseItemTimeUpdateMsg);
	m_opcodeTable[MSG_S2C::ITEM_ENCHANT_TIME_UPDATE]		= OpcodeHandler(&CPacketParser::ParseItemEnchantTimeUpdateMsg);
	m_opcodeTable[MSG_S2C::ITEM_ENCHANTMENT_LOG]			= OpcodeHandler(&CPacketParser::ParseItemEnchantmentLogMsg);
	m_opcodeTable[MSG_S2C::ITEM_INVENTORY_CHANGE_FAILURE]	= OpcodeHandler(&CPacketParser::ParseItemSwapFailure);

	// Player Trade
	m_opcodeTable[MSG_S2C::TRADE_STAT]				= OpcodeHandler(&CPacketParser::ParseTrade_StateMsg);
	m_opcodeTable[MSG_S2C::TRADE_STATUS_EXTENDED]	= OpcodeHandler(&CPacketParser::ParseTrade_State_ExtMsg);

	//
	m_opcodeTable[MSG_S2C::DESTROY_OBJECT] = OpcodeHandler(&CPacketParser::ParseDestroyObjMsg);


	//////////////////////////////////////////////////////////////////////////
	// Quest
	//////////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::NPC_GOSSIP_MESSAGE] = OpcodeHandler(&CPacketParser::ParseNpcGossipMessageMsg);
	m_opcodeTable[MSG_S2C::QUESTGIVER_QUEST_DETAILS] = OpcodeHandler(&CPacketParser::ParseQuestGiverQuestDetailsMsg);
	m_opcodeTable[MSG_S2C::QUESTGIVER_OFFER_REWARD] = OpcodeHandler(&CPacketParser::ParseQuestGiverOfferRewardMsg);
	m_opcodeTable[MSG_S2C::QUESTGIVER_REQUEST_ITEMS] = OpcodeHandler(&CPacketParser::ParseQuestGiverRequestItems);
	m_opcodeTable[MSG_S2C::QUESTGIVER_STATUS] = OpcodeHandler(&CPacketParser::ParseQuestGiverStatusQuery);
	m_opcodeTable[MSG_S2C::QUESTGIVER_INRANGE_STATUS_QUERY_RESPONSE] = OpcodeHandler(&CPacketParser::ParseQuestGiverInRangeQuery);
	m_opcodeTable[MSG_S2C::QUESTUPDATE_COMPLETE] = OpcodeHandler(&CPacketParser::ParseQuestUpdateComplete);
	m_opcodeTable[MSG_S2C::QUESTUPDATE_ADD_KILL] = OpcodeHandler(&CPacketParser::ParseQuestUpdateAddSkill);
	m_opcodeTable[MSG_S2C::QUESTUPDATE_ADD_ITEM] = OpcodeHandler(&CPacketParser::ParseQuestUpdateAddItem);
	m_opcodeTable[MSG_S2C::QUESTGIVER_QUEST_COMPLETE] = OpcodeHandler(&CPacketParser::ParseQuestGiverQuestComplete);
	m_opcodeTable[MSG_S2C::QUESTGIVER_QUEST_FAILED] = OpcodeHandler(&CPacketParser::ParseQuestGiverQuestFailed);
	m_opcodeTable[MSG_S2C::QUEST_CONFIRM_ACCEPT] = OpcodeHandler(&CPacketParser::ParseaAeccpetNewQuest);
	m_opcodeTable[MSG_S2C::QUESTLOG_FULL]			= OpcodeHandler(&CPacketParser::ParseQuestFull);
	m_opcodeTable[MSG_S2C::MSG_FINISHQUESTS]		= OpcodeHandler(&CPacketParser::ParseFinishedQuests);

	//////////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::ATTACK_START]				= OpcodeHandler(&CPacketParser::ParseAttackStart);
	m_opcodeTable[MSG_S2C::ATTACK_STOP]					= OpcodeHandler(&CPacketParser::ParseAttackStop);
	m_opcodeTable[MSG_S2C::ATTACKER_STATE_UPDATE]		= OpcodeHandler(&CPacketParser::ParseAttackUpdate);
	m_opcodeTable[MSG_S2C::ATTACKSWING_NOTINRANGE]		= OpcodeHandler(&CPacketParser::ParseAttackFail_NotInRange);
	m_opcodeTable[MSG_S2C::ATTACKSWING_BADFACING]		= OpcodeHandler(&CPacketParser::ParseAttackFail_BadFacing);
	m_opcodeTable[MSG_S2C::SPELL_HEAL_ON_PLAYER]		= OpcodeHandler(&CPacketParser::ParseHealHP);
	m_opcodeTable[MSG_S2C::SPELL_HEALMANAON_PLAYER] 	= OpcodeHandler(&CPacketParser::ParseHeadMP);
	m_opcodeTable[MSG_S2C::SPELL_NONMELEE_DAMAGE_LOG]	= OpcodeHandler(&CPacketParser::ParseNoMeleeDamage);
	m_opcodeTable[MSG_S2C::AURA_PERIODIC_LOG]			= OpcodeHandler(&CPacketParser::ParseAuraPeriodicDamage);
	m_opcodeTable[MSG_S2C::DAMAGE_ENVIRONMENTAL_LOG]	= OpcodeHandler(&CPacketParser::ParseEnvironmentalDamage);
	m_opcodeTable[MSG_S2C::SPELL_LOG_MISS]				= OpcodeHandler(&CPacketParser::ParseSpellMiss);
	m_opcodeTable[MSG_S2C::SPELL_CAST_RESULT]			= OpcodeHandler(&CPacketParser::ParseSpellCastResult);
	m_opcodeTable[MSG_S2C::SPELL_FAILURE]				= OpcodeHandler(&CPacketParser::ParseSpellFailure);
	m_opcodeTable[MSG_S2C::SPELL_START]					= OpcodeHandler(&CPacketParser::ParseSpellStart);
	m_opcodeTable[MSG_S2C::SPELL_GO]					= OpcodeHandler(&CPacketParser::ParseSpellGo);
	m_opcodeTable[MSG_S2C::SPELL_CLEAR_COOLDOWN]		= OpcodeHandler(&CPacketParser::ParseSpellClearCD);
	m_opcodeTable[MSG_S2C::SPELL_DELAYED]				= OpcodeHandler(&CPacketParser::ParseSpellDelay);
	m_opcodeTable[MSG_S2C::SPELL_TARGET_CAST_RESULT]	= OpcodeHandler(&CPacketParser::ParseSpellCastSuccess);
	m_opcodeTable[MSG_S2C::SPELL_CHANNEL_START]			= OpcodeHandler(&CPacketParser::ParseChannelSpellStart);
	m_opcodeTable[MSG_S2C::SPELL_CHANNEL_UPDATE]		= OpcodeHandler(&CPacketParser::ParseChannelSpellUpdate);
	m_opcodeTable[MSG_S2C::ACTION_BUTTONS]				= OpcodeHandler(&CPacketParser::ParseActionButtons);
	m_opcodeTable[MSG_S2C::SPELLS_INITIAL]				= OpcodeHandler(&CPacketParser::ParseSpellInit);
	m_opcodeTable[MSG_S2C::SPELL_LEARNED]				= OpcodeHandler(&CPacketParser::ParseSpellLearn);
	m_opcodeTable[MSG_S2C::SPELL_REMOVED]               = OpcodeHandler(&CPacketParser::ParseSpellRemove);
	///////////////////////////////////////////////////////////////////////
	//  组队相关
	///////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::GROUP_INVITE]              = OpcodeHandler(&CPacketParser::ParseGroupInviteMsg);
	m_opcodeTable[MSG_S2C::GROUP_UNINVITE]         = OpcodeHandler(&CPacketParser::ParseGroupUnInviteMsg);
	m_opcodeTable[MSG_S2C::GROUP_DECLINE]          =  OpcodeHandler(&CPacketParser::ParseGroupRefuseMsg);
	m_opcodeTable[MSG_S2C::GROUP_SET_LEADER]    = OpcodeHandler(&CPacketParser::ParseGroupLeaderChanged);
	m_opcodeTable[MSG_S2C::GROUP_SET_PLAYER_ICON]    = OpcodeHandler(&CPacketParser::ParseGroupMemberIcon);
	m_opcodeTable[MSG_S2C::GROUP_DESTROYED]     = OpcodeHandler(&CPacketParser::ParseGroupDestory);
	m_opcodeTable[MSG_S2C::GROUP_LIST]                = OpcodeHandler(&CPacketParser::ParseGroupList);
	m_opcodeTable[MSG_S2C::RAID_READYCHECK]            = OpcodeHandler(&CPacketParser::ParseGroupMemberReady);
	m_opcodeTable[MSG_S2C::MSG_GROUP_SWEP]            = OpcodeHandler(&CPacketParser::ParseGroupMemberSwep);
	m_opcodeTable[MSG_S2C::MSG_CHANGE_SUBGROUP]            = OpcodeHandler(&CPacketParser::ParseMemberSubGroupChange);
	m_opcodeTable[MSG_S2C::MSG_ADD_MEMBER]            = OpcodeHandler(&CPacketParser::ParseAddMember);
	m_opcodeTable[MSG_S2C::MSG_REMOVE_MEMBER]            = OpcodeHandler(&CPacketParser::ParseRemoveMember);
	m_opcodeTable[MSG_S2C::PARTY_MEMBER_STATS] = OpcodeHandler(&CPacketParser::ParseGroupMemberStatUpData);
	m_opcodeTable[MSG_S2C::PARTY_COMMAND_RESULT]  = OpcodeHandler(&CPacketParser::ParseGroupCommandResult);
	m_opcodeTable[MSG_S2C::MSG_SET_GROUP_LOOT_METHOD] = OpcodeHandler(&CPacketParser::ParseLootMethod);

	m_opcodeTable[MSG_S2C::LOOT_RESPONSE]				= OpcodeHandler(&CPacketParser::ParseLootList);
	m_opcodeTable[MSG_S2C::LOOT_REMOVED]				= OpcodeHandler(&CPacketParser::ParseLootRemove);
	m_opcodeTable[MSG_S2C::SMSG_LOOT_MONEY_NOTIFY]		= OpcodeHandler(&CPacketParser::ParseLootMoney);
	m_opcodeTable[MSG_S2C::SMSG_LOOT_CLEAR_MONEY]		= OpcodeHandler(&CPacketParser::ParseLootMoneyClear);
	m_opcodeTable[MSG_S2C::LOOT_START_ROLL]				= OpcodeHandler(&CPacketParser::ParseToRollItem);
	m_opcodeTable[MSG_S2C::MSG_RANDOM_ROLL]				= OpcodeHandler(&CPacketParser::ParseRandomRoll);
	m_opcodeTable[MSG_S2C::LOOT_ROLL]					= OpcodeHandler(&CPacketParser::ParseRolledItem);
	m_opcodeTable[MSG_S2C::LOOT_ROLL_WON]               = OpcodeHandler(&CPacketParser::ParsestRoolWon);          

	//////////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::FRIEND_STATUS]				= OpcodeHandler(&CPacketParser::ParseFriendStatMsg);
	m_opcodeTable[MSG_S2C::FRIEND_LIST]					= OpcodeHandler(&CPacketParser::ParseFriendListMsg);

	//////////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::AUCTION_COMMAND_RESULT]		= OpcodeHandler(&CPacketParser::ParseAuctionCommandResultMsg);
	m_opcodeTable[MSG_S2C::AUCTION_LIST_RESULT]			= OpcodeHandler(&CPacketParser::ParseAuctionListResultMsg);
	m_opcodeTable[MSG_S2C::AUCTION_BIDDER_NOTIFICATION] = OpcodeHandler(&CPacketParser::ParseAuctionBidderNotifyMsg);
	m_opcodeTable[MSG_S2C::AUCTION_OWNER_NOTIFICATION]	= OpcodeHandler(&CPacketParser::ParseAuctionOwnerNotifyMsg);
	m_opcodeTable[MSG_S2C::AUCTION_BIDDER_LIST_RESULT]	= OpcodeHandler(&CPacketParser::ParseAuctionBidderListResultMsg);
	m_opcodeTable[MSG_S2C::AUCTION_OWNER_LIST_RESULT]	= OpcodeHandler(&CPacketParser::ParseAuctionOwnerListResultMsg);
	m_opcodeTable[MSG_S2C::AUCTION_HELLO]				= OpcodeHandler(&CPacketParser::ParseAuctionHelloMsg);

	//////////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::MAIL_QUERY_NEXT_TIME]            = OpcodeHandler(&CPacketParser::ParseNeedReadMail);
	m_opcodeTable[MSG_S2C::MAIL_TEXT_QUERY_RESPONSE]	= OpcodeHandler(&CPacketParser::ParseMailContentMsg);
	m_opcodeTable[MSG_S2C::MAIL_RECEIVED]				= OpcodeHandler(&CPacketParser::ParseReceiveMail);
	m_opcodeTable[MSG_S2C::MAIL_LIST_RESULT]			= OpcodeHandler(&CPacketParser::ParseMailListMsg);
	m_opcodeTable[MSG_S2C::MAIL_SEND_RESULT]			= OpcodeHandler(&CPacketParser::ParseSendMailResult);
	//////////////////////////////////////////////////////////////////////////

	m_opcodeTable[MSG_S2C::AURA_SET_DURATION] = OpcodeHandler(&CPacketParser::ParseAuraDuration);
	m_opcodeTable[MSG_S2C::AURA_SET_SINGLE]  = OpcodeHandler(&CPacketParser::ParseAuraSingle);

	m_opcodeTable[MSG_S2C::LOG_XPGAIN]	= OpcodeHandler(&CPacketParser::ParseExpGain);
	//////////////////////////////////////////////////////////////////////////

	m_opcodeTable[MSG_S2C::NPC_SHOW_BANK] = OpcodeHandler(&CPacketParser::ParseShowBank);
	m_opcodeTable[MSG_S2C::CHANNEL_NOTIFY] = OpcodeHandler(&CPacketParser::ParseChannelNotify);

	//////////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::MOUNT_INVITE] = OpcodeHandler(&CPacketParser::ParseMountInviteMsg);
	m_opcodeTable[MSG_S2C::MOUNT_DISBAND] = OpcodeHandler(&CPacketParser::ParseMountDisbandMsg);
	m_opcodeTable[MSG_S2C::MOUNT_ACCEPT] = OpcodeHandler(&CPacketParser::ParseMountAcceptMsg);
	m_opcodeTable[MSG_S2C::MOUNT_DECLINE] = OpcodeHandler(&CPacketParser::ParseMountDeclineMsg);
	m_opcodeTable[MSG_S2C::MOUNT_RESULT] = OpcodeHandler(&CPacketParser::ParseMountResultMsg);
	m_opcodeTable[MSG_S2C::GUILD_INFO] = OpcodeHandler(&CPacketParser::ParseGuildInfoMsg);
	m_opcodeTable[MSG_S2C::GUILD_INVITE] = OpcodeHandler(&CPacketParser::ParseGuildInviteMsg);
	m_opcodeTable[MSG_S2C::GUILD_DECLINE] = OpcodeHandler(&CPacketParser::ParseGuildDeclineMsg);
	m_opcodeTable[MSG_S2C::GUILD_ROSTER] = OpcodeHandler(&CPacketParser::ParseGuildRosterMsg);
	m_opcodeTable[MSG_S2C::GUILD_QUERY_RESPONSE] = OpcodeHandler(&CPacketParser::ParseGuildQueryMsg);
	m_opcodeTable[MSG_S2C::GUILD_CREATE] = OpcodeHandler(&CPacketParser::ParseGuildCreateMsg);
	m_opcodeTable[MSG_S2C::GUILD_EVENT] = OpcodeHandler(&CPacketParser::ParseGuildEvent);
	m_opcodeTable[MSG_S2C::GUILD_COMMAND_RESULT] = OpcodeHandler(&CPacketParser::ParseGuildCommandResult);
	m_opcodeTable[MSG_S2C::TUTORIAL_FLAGS] = OpcodeHandler(&CPacketParser::ParseFreshManHelpFlagMsg);
	m_opcodeTable[MSG_S2C::SHOP_ITEMLIST] = OpcodeHandler(&CPacketParser::ParseShopItemListMsg);
	m_opcodeTable[MSG_S2C::SMSG_PONG] = OpcodeHandler(&CPacketParser::ParsePongMsg);
	m_opcodeTable[MSG_S2C::COPY_NAME] = OpcodeHandler(&CPacketParser::ParseCopyName);
	m_opcodeTable[MSG_S2C::HIDE_GM]	  = OpcodeHandler(&CPacketParser::ParseHideGM);
	m_opcodeTable[MSG_S2C::MSG_QUEUE_INSTANCE_UPDATE] = OpcodeHandler(&CPacketParser::ParseQueueInstanceUpdate);
	m_opcodeTable[MSG_S2C::MSG_ENTER_INSTANCE_COUNT_DOWN] = OpcodeHandler(&CPacketParser::ParseEnterInstanceCountDown);
	m_opcodeTable[MSG_S2C::MSG_ARENA_UPDATE_STATE] = OpcodeHandler(&CPacketParser::ParseArenaUpdateState);
	m_opcodeTable[MSG_S2C::MSG_INSTANCE_BILLBOARD] = OpcodeHandler(&CPacketParser::ParseInstanceBillBoard);
	m_opcodeTable[MSG_S2C::MSG_PLAYER_LEAVE_INSTANCE] = OpcodeHandler(&CPacketParser::ParsePlayerLeaveInstance);
	m_opcodeTable[MSG_S2C::MSG_SYSTEM_GIFT] = OpcodeHandler(&CPacketParser::ParseSystemGift);
	m_opcodeTable[MSG_S2C::MSG_SYSTEM_GIFT_NOTIFY] = OpcodeHandler(&CPacketParser::ParseSystemGiftMsg);
	m_opcodeTable[MSG_S2C::MSG_PLAYER_ENTER_INSTANCE] = OpcodeHandler(&CPacketParser::ParsePlayerEnterInstance);
	m_opcodeTable[MSG_S2C::MSG_SUNYOU_INSTANCE_LIST] = OpcodeHandler(&CPacketParser::ParseSunyouInstanceList);
	m_opcodeTable[MSG_S2C::MSG_SYSTEM_NOTIFY_TOP_MOVE] = OpcodeHandler(&CPacketParser::ParseSystemMoveMsg);
	m_opcodeTable[MSG_S2C::MSG_SYSTEM_NOTIFY_TOP_REMOVE] = OpcodeHandler(&CPacketParser::ParseSystemMoveRemoveMsg);
	m_opcodeTable[MSG_S2C::MSG_SYSTEM_NOTIFY_TOP_ADD] = OpcodeHandler(&CPacketParser::ParseSystemMoveAddMsg);
	

	//////////////////////////////////////////////////////////////////////////
	//	捐助
	//////////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::CONTRIBUTION_CONFIRM_RESULT] = OpcodeHandler(&CPacketParser::ParseContributionConfirmResult);
	m_opcodeTable[MSG_S2C::SELF_CONTRIBUTION_HISTORY_RESPONSE] = OpcodeHandler(&CPacketParser::ParseSelfContributionHistoryResponse);
	m_opcodeTable[MSG_S2C::CONTRIBUTION_BILLBOARD_RESPONSE] = OpcodeHandler(&CPacketParser::ParseContributionBillboardResponse);
	m_opcodeTable[MSG_S2C::CONTRIBUTION_QUERY_RESULT] = OpcodeHandler(&CPacketParser::ParseContributionQueryResult);
	m_opcodeTable[MSG_S2C::MSG_JINGLIAN_ITEM_ACK] = OpcodeHandler(&CPacketParser::ParseJingLianResult);
	m_opcodeTable[MSG_S2C::MSG_XIANGQIAN_ITEM_ACK] = OpcodeHandler(&CPacketParser::ParseXiangQianResult);
	m_opcodeTable[MSG_S2C::PARTY_KILL_LOG] = OpcodeHandler(&CPacketParser::ParseKillLog);

	m_opcodeTable[MSG_S2C::PET_STABLE_RESULT] = OpcodeHandler(&CPacketParser::ParsePetStableResult);
	m_opcodeTable[MSG_S2C::PET_SPELLS_MSG] = OpcodeHandler(&CPacketParser::ParsePetSpell);
	m_opcodeTable[MSG_S2C::PET_RENAME_ACK] = OpcodeHandler(&CPacketParser::ParsePetRenameAck);
	m_opcodeTable[MSG_S2C::PET_ACTION_SOUND] = OpcodeHandler(&CPacketParser::ParsePetActionSound);
	m_opcodeTable[MSG_S2C::QUERY_PET_NAME_RESPONSE] = OpcodeHandler(&CPacketParser::ParsePetNameQueryResponse);
	m_opcodeTable[MSG_S2C::PETS_LIST_STABLED] = OpcodeHandler(&CPacketParser::ParsePetListStabled);
	m_opcodeTable[MSG_S2C::PET_UNLEARN_CONFIRM] = OpcodeHandler(&CPacketParser::ParsePetUnLearnConfirm);
	m_opcodeTable[MSG_S2C::NOTIFY_MSG] = OpcodeHandler(&CPacketParser::ParseNotifyMsg);
	m_opcodeTable[MSG_S2C::GUILD_LIST_INFO_ACK] = OpcodeHandler(&CPacketParser::ParseQureyGuildsList);
	m_opcodeTable[MSG_S2C::GUILD_CONTRIBUTION_POINTS_NOTIFY] = OpcodeHandler(&CPacketParser::ParseGuildContributionPointsNotify);
	m_opcodeTable[MSG_S2C::SMSG_SERVERTIME] = OpcodeHandler(&CPacketParser::ParseServerTiem);
	m_opcodeTable[MSG_S2C::GUILD_DECLARE_WAR_NOTIFY] = OpcodeHandler(&CPacketParser::ParseGuildDeclareWarNotify);
	m_opcodeTable[MSG_S2C::MSG_CASTLE_NPC_STATE_NOTIFY] = OpcodeHandler(&CPacketParser::ParseCastleNPCStateNotify);
	m_opcodeTable[MSG_S2C::MSG_CASTLE_STATE_ACK] = OpcodeHandler(&CPacketParser::ParseCastleStateAck);


	m_opcodeTable[MSG_S2C::MSG_RECRUIT_NOTIFY] = OpcodeHandler(&CPacketParser::ParseShiFouBaiShi);
	m_opcodeTable[MSG_S2C::MSG_RECRUIT_ACK] = OpcodeHandler(&CPacketParser::ParseTuDiFankui);
    
	m_opcodeTable[MSG_S2C::MSG_CHOICE_WHILE_IN_INSTANCE] = OpcodeHandler(&CPacketParser::ParseChoiceDeadExit);
	m_opcodeTable[MSG_S2C::MSG_CASTLE_COUNT_DOWN] = OpcodeHandler(&CPacketParser::ParseCastleCountDown);
	m_opcodeTable[MSG_S2C::MSG_LADDER_ACK] = OpcodeHandler(&CPacketParser::ParseLadderAck);

	m_opcodeTable[MSG_S2C::MSG_TITLE_LIST] = OpcodeHandler(&CPacketParser::ParseTitleList);
	m_opcodeTable[MSG_S2C::MSG_TITLE_ADD] = OpcodeHandler(&CPacketParser::ParseAddTitle);
	m_opcodeTable[MSG_S2C::MSG_TITLE_REMOVE] = OpcodeHandler(&CPacketParser::ParseRemoveTitle);

	m_opcodeTable[MSG_S2C::MSG_BATTLE_GROUND_POWER_UPDATE] = OpcodeHandler(&CPacketParser::ParseBattleGroundPowerUpdate);
	m_opcodeTable[MSG_S2C::MSG_QUEST_ESCORT_NOTIFY] = OpcodeHandler(&CPacketParser::ParseQuestEscortNotify);
	m_opcodeTable[MSG_S2C::MSG_ON_ITEM_FIELD_RECORD_CHANGE] = OpcodeHandler(&CPacketParser::ParsestItemFieldRecordChange);
	m_opcodeTable[MSG_S2C::MSG_QUERY_PLAYERS_ACK] = OpcodeHandler(&CPacketParser::ParseQueryPlayerAck);
	m_opcodeTable[MSG_S2C::MSG_CC_RESULT] = OpcodeHandler(&CPacketParser::ParseCCResult);
	//////////////////////////////////////////////////////////////////////////战场////////////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::MSG_BATTLE_GROUND_SPECIAL_NOTIFY] = OpcodeHandler(&CPacketParser::ParseBattleKill);



	m_opcodeTable[MSG_S2C::MSG_DONATE_ACK] = OpcodeHandler(&CPacketParser::ParseDonateAck);
	m_opcodeTable[MSG_S2C::MSG_DONATION_LADDER_ACK] = OpcodeHandler(&CPacketParser::ParseDonationLadderAck);
	m_opcodeTable[MSG_S2C::MSG_DONATION_EVENT_LIST] = OpcodeHandler(&CPacketParser::ParseDonationEventList);
	m_opcodeTable[MSG_S2C::MSG_DONATE_ADD_HISTROY] = OpcodeHandler(&CPacketParser::ParsetAddDonateHistroy);
	m_opcodeTable[MSG_S2C::MSG_DONATION_HISTORY] = OpcodeHandler(&CPacketParser::ParseDonationHistory);

	m_opcodeTable[MSG_S2C::MSG_CREDIT_APPLY_ACK] = OpcodeHandler(&CPacketParser::ParseCreditApply);
	m_opcodeTable[MSG_S2C::MSG_CREDIT_ACTION_ACK] = OpcodeHandler(&CPacketParser::ParseCreditAction);
	m_opcodeTable[MSG_S2C::MSG_CREDIT_HISTORY_ACK] = OpcodeHandler(&CPacketParser::ParseCreditHistory);

	//////////////////////////////////////////////////////////////////////////声效////////////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::MSG_PLAY_SOUND_NOTIFY] = OpcodeHandler(&CPacketParser::ParsePalySound);
	m_opcodeTable[MSG_S2C::MSG_SCREEN_SHAKE] = OpcodeHandler(&CPacketParser::ParseScreenShaking);


	//////////////////////////////////////////////////////////////////////////
	m_opcodeTable[MSG_S2C::MSG_SERIALCARDFORGIFT_USE] = OpcodeHandler(&CPacketParser::ParseSerialForGift);
	m_opcodeTable[MSG_S2C::MSG_SERIALCARDFORGIFT_USE_RESULT] = OpcodeHandler(&CPacketParser::ParseSerialForGiftResult);


	m_opcodeTable[MSG_S2C::MSG_BATTLE_GROUND_RESOURCE_UPDATE] = OpcodeHandler(&CPacketParser::ParseBattleGroundResourceUpdate);
	m_opcodeTable[MSG_S2C::MSG_SCENE_EFFECT] = OpcodeHandler(&CPacketParser::ParseSceneEffectUpdate);


	m_opcodeTable[MSG_S2C::MSG_BATTLE_GROUND_DETAIL_INFORMATION] = OpcodeHandler(&CPacketParser::ParseBattleGroundDetailInformation);


	m_opcodeTable[MSG_S2C::MSG_CREATURE_FORCE_PLAY_ANIMATION] = OpcodeHandler(&CPacketParser::ParseActivateAnimation);
	m_opcodeTable[MSG_S2C::MOVE_KNOCK_BACK] = OpcodeHandler(&CPacketParser::ParseKnockBack );

	m_opcodeTable[MSG_S2C::MSG_UNBIND_ITEM_ACK] = OpcodeHandler(&CPacketParser::ParseUnBindItem );
	m_opcodeTable[MSG_S2C::MSG_APPEAR_ITEM_ACK] = OpcodeHandler(&CPacketParser::ParseAppearTo );
	m_opcodeTable[MSG_S2C::MSG_TITLE_BROADCAST] = OpcodeHandler(&CPacketParser::ParseTitleBroadcast );
	m_opcodeTable[MSG_S2C::MSG_GUILD_NPC_LEVEL_UP] = OpcodeHandler(&CPacketParser::ParseGuildLevelUp );
	m_opcodeTable[MSG_S2C::NPC_TRAINER_LIST] = OpcodeHandler(&CPacketParser::ParseSpellNPCTrainerList );
	m_opcodeTable[MSG_S2C::MSG_DISPLAY_TEXT_ON_OBJECT_HEAD] = OpcodeHandler(&CPacketParser::ParseDisplayTextOnObjectHead );
	m_opcodeTable[MSG_S2C::MSG_DISPLAY_TEXT_ON_OBJECT_HEAD] = OpcodeHandler(&CPacketParser::ParseDisplayTextOnObjectHead );
	m_opcodeTable[MSG_S2C::MSG_SKILL_NOTIFY_UNLEARN] = OpcodeHandler(&CPacketParser::ParseUnLearnProfession );
	m_opcodeTable[MSG_S2C::MSG_MONSTER_FLEE_NOTIFY] = OpcodeHandler(&CPacketParser::ParseMonsterRunaway );
	m_opcodeTable[MSG_S2C::MSG_INSERT_GEM_ACK] = OpcodeHandler(&CPacketParser::ParseInsertGemAck );

	m_opcodeTable[MSG_S2C::GROUP_APPLYFORJOINING_ACK] = OpcodeHandler(&CPacketParser::ParseGroupApplyForJoiningAck );
	m_opcodeTable[MSG_S2C::GROUP_MODIFY_TITLE_ACK] = OpcodeHandler(&CPacketParser::ParseGroupModifyTitleAck );
	m_opcodeTable[MSG_S2C::GROUP_MODIFY_CAN_APPLYFORJOINING_ACK] = OpcodeHandler(&CPacketParser::ParseGroupModifyCanApplyForJoiningAck );
	m_opcodeTable[MSG_S2C::GROUP_LIST_ACK] = OpcodeHandler(&CPacketParser::ParseGroupListAck );
	m_opcodeTable[MSG_S2C::GROUP_APPLYFORJOINING_RESULT] = OpcodeHandler(&CPacketParser::ParseGroupApplyForJoiningResult );
	m_opcodeTable[MSG_S2C::GROUP_APPLYFORJOINING] = OpcodeHandler(&CPacketParser::ParseGroupApplyForJoining );
	m_opcodeTable[MSG_S2C::GROUP_APPLYFORJOINING_STATE_ACK] = OpcodeHandler(&CPacketParser::ParsestGroupApplyForJoiningStateAck );
	m_opcodeTable[MSG_S2C::MSG_UPDATE_ACTION_BUTTONS] = OpcodeHandler(&CPacketParser::ParseUpdateActionButtons );
	m_opcodeTable[MSG_S2C::SMSG_SUMMON_REQUEST] = OpcodeHandler(&CPacketParser::ParseSummon );
	m_opcodeTable[MSG_S2C::CANCEL_AUTO_REPEAT] = OpcodeHandler(&CPacketParser::ParseSpellCancelAutoRepeat );
	m_opcodeTable[MSG_S2C::BEGIN_AUTO_REPEAT] = OpcodeHandler(&CPacketParser::ParseSpellBeginAutoRepeat );
	m_opcodeTable[MSG_S2C::SPELL_SET_FLAT_MODIFIER] = OpcodeHandler(&CPacketParser::ParseSpellSetFlatModifier );
	m_opcodeTable[MSG_S2C::SHOP_ACTIVITY_UPDATE] = OpcodeHandler(&CPacketParser::ParseShopActivityUpdate );
}

bool CPacketParser::ProcessPacket(char* pcBuffer, int iLen)
{
	CPacketUsn kPacket(pcBuffer, iLen);

	OpcodeHandler& oph = m_opcodeTable[kPacket.GetProNo()];
	if( oph.handler )
	{
		(this->*oph.handler)(kPacket);
	}
	else
		NILOG("Unknown MsgID %d\n", kPacket.GetProNo());
	
	return true;
}


//-------------------------------------------

void CPacketParser::ParseVersionMsg(CPacketUsn& pakTool)
{
	MSG_LS2C::stVersionAck msg;
	pakTool >> msg;

	PacketBuilder->SendLoginReqMsg( msg.encrypt_table );
}

void CPacketParser::ParseLoginMsg(CPacketUsn& pakTool)
{
	MSG_LS2C::stLoginAck msg;
	pakTool >> msg;
	ULoginFrame* pLoginFrame = UDynamicCast(ULoginFrame, SYState()->UI->GetDialogEX(DID_LOGIN));
	if (pLoginFrame == NULL)
	{
		return ;
	}
	pLoginFrame->ActiveLoginBtn();
	switch( msg.nStat )
	{
	case MSG_LS2C::stLoginAck::LOGIN_OK:
		{
			//pLoginFrame->BeginLoading();
		}
		break;

	case MSG_LS2C::stLoginAck::LOGIN_USER_ONLINE:
		{
			//UMessageBox::MsgBox_s("玩家已经在线了");
		}
		break;

	case MSG_LS2C::stLoginAck::LOGIN_PASSWORD_ERROR:
		{
			UMessageBox::MsgBox_s(_TRAN("密码错误!"));
		}
		return;

	case MSG_LS2C::stLoginAck::LOGIN_USER_NOT_FOUND:
		{
			UMessageBox::MsgBox_s(_TRAN("用户名不存在!"));
		}
		return;
	case MSG_LS2C::stLoginAck::LOGIN_USER_BANED:
		{
			UMessageBox::MsgBox_s(_TRAN("账号被封!"));
		}
		return ;
	case MSG_LS2C::stLoginAck::LOGIN_DATABASE_EXCEPTION:
		{
			UMessageBox::MsgBox_s(_TRAN("未知错误!"));
		}
		return;
	case MSG_LS2C::stLoginAck::LOGIN_NOT_ENOUGH_POINT_OR_EXPIRE:
		{
			UMessageBox::MsgBox_s(_TRAN("包月到期并且点卡耗尽!") );
		}
		return;
	case MSG_LS2C::stLoginAck::LOGIN_ACCOUNT_LOCKED:
	{
		string strText = _TRAN("账号已锁定,截止日期");
		strText += ConvertTimeStampToDataTime(msg.expire);
		UMessageBox::MsgBox_s(strText.c_str());
	}
	return;

    	case MSG_LS2C::stLoginAck::LOGIN_VERSION_NOT_MATCH:
        {
			std::string text = _TRAN("版本号不正确,请用自动更新工具更新!");
			text += "(Launcher.exe)";
            UMessageBox::MsgBox_s(text.c_str());
        }
        return;

    	case MSG_LS2C::stLoginAck::LOGIN_SERVER_CANNT_CONNECT:
        {
            UMessageBox::MsgBox_s(_TRAN("服务器无法连接!"));
        }
        return;

	default:
		{
		}
		return;
	}

	NetworkMgr->ConnectGate(msg.ip, msg.port);

	ClientState->SetSessionID(msg.tmp_password);
	ClientState->SetAccountID(msg.acct);

	pLoginFrame->SetKeepUse();
	

	//ClientState->ClearGroup();
// 	for(size_t i = 0; i < msg.vGroup.size(); ++i)
// 	{
//         ClientState->AddGroup(msg.vGroup[i].strGroupName, msg.vGroup[i].limit, msg.vGroup[i].online);
// 	}

	//显示家园系统等


	//显示组列表UI
	//todo... 这里用脚本来连接和UI的逻辑
	//把网络协议和界面分开
	UTRACE("Login accept.");
	SystemSetup->SetGameSystemUserFileRoot(PATH_ACCOUNTNAME, ClientState->GetUserName().c_str()); 
	//CUISystem::ShowEX(DID_CHOOSESERVER,TRUE,TRUE);
	
}

//选组后, 服务器返回GateServer的地址
void CPacketParser::ParseGroupListMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stServerGroupListAck msg;
	pakTool >> msg;

	ClientState->ClearGroup();
	for(size_t i = 0; i < msg.groups.size(); ++i)
	{
	    ClientState->AddGroup(msg.groups[i].id, msg.groups[i].name, msg.groups[i].status);
	}

//	SystemSetup->SetGameSystemUserFileRoot(PATH_ACCOUNTNAME, ClientState->GetUserName().c_str()); 
	CUISystem::ShowEX(DID_CHOOSESERVER,TRUE,TRUE);
}

//登录GateServer 失败后会返回这个消息
void CPacketParser::ParseGTLoginMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stLoginAck msg;
	pakTool >> msg;
// 	if(msg.nError == 5)
// 	{
// 		//被封停的账号
// 	}
}

//成功登录GateServer后, 返回角色列表
void CPacketParser::ParseRoleListMsg(CPacketUsn& pakTool)
{
	//NetworkMgr->m_bSendPing = false;
	MSG_S2C::stRoleListAck msg;
	pakTool >> msg;
	//如果重新选择人物 就先清理

    ObjectMgr->RemoveAll();

	CMap * pmap = CMap::Get();
	if (pmap)
	{
		if (ObjectMgr->GetLocalPlayer())
		{
			SYState()->IAudio->StopAllSound();
		}
		
		pmap->Unload();
		pmap->Reset();
	}
	if (SocialitySys && SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->DestroyTeamInfo();
	}
	UChooseServer* pchooseserver = UDynamicCast(UChooseServer, SYState()->UI->GetDialogEX(DID_CHOOSESERVER));
	if (pchooseserver)
	{
		pchooseserver->SetWaitServerMsg(FALSE);
	}
	UInGame* pInGame = UInGame::Get();
	if (pInGame)
	{
		pInGame->ResetBagAndBank();
	}
	ClientState->ClearRole();

	RoleInfo kRole;
	for( unsigned short i = 0; i < msg.vRoles.size(); i++ )
	{
		kRole			= msg.vRoles[i];
		kRole.name	= /*UTF8ToAnis*/(msg.vRoles[i].name);

		if( kRole.nStat == 2 )//已经删除了的角色
			continue;

		ClientState->AddRole(kRole);

		CPlayer* pkPlayer = ObjectMgr->CreatePlayer(kRole.guid, 0, false);
		pkPlayer->SetRace(kRole.nRace);
		pkPlayer->SetGender(kRole.nGender);
		pkPlayer->SetName(kRole.name.c_str());
		pkPlayer->SetPosition(NiPoint3(17.0f + i, -22.0f, 0.0f));
		pkPlayer->Update(0.0f);
	}

    //GM隐身状态消失
    CShadowGeometry::SetShadowColor(DEFAULT_SHADOW_COLOR);

	//显示选择人物界面
	CUISystem::ShowEX(DID_CHOOSEPLAYER, TRUE, TRUE);
	SYState()->ClientApp->SetCursor(SYC_ARROW);
}

void CPacketParser::ParseCreateRoleMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stCreateRoleAck msg;
	pakTool >> msg;
	if(msg.nError)
	{
		switch(msg.nError)
		{
		case MSG_S2C::stCreateRoleAck::NOT_LOGON:
			UMessageBox::MsgBox_s(_TRAN("请先登陆!"));
			break;

		case MSG_S2C::stCreateRoleAck::NAME_IS_USED:
			UMessageBox::MsgBox_s(_TRAN("这个名字已经被使用"));
			break;

		case MSG_S2C::stCreateRoleAck::ROLE_IS_FULL:
			UMessageBox::MsgBox(_TRAN("角色创建数量已满"));
			break;

		case MSG_S2C::stCreateRoleAck::NO_SUCH_RACECLASS:
			UMessageBox::MsgBox_s(_TRAN("种族职业错误"));
			break;

		case MSG_S2C::stCreateRoleAck::NAME_IS_TOO_LONG:
			UMessageBox::MsgBox_s(_TRAN("名字长度太长"));
			break;

		case MSG_S2C::stCreateRoleAck::BANNED_NAME:
			UMessageBox::MsgBox_s(_TRAN("名字被禁止使用"));
			break;

		default:
			{
				//char acMsg[128];
				//NiSprintf(acMsg, sizeof(acMsg), "未知错误 %d", msg.nError);
				std::string strRet = _TRAN(N_NOTICE_Z12, _I2A(msg.nError).c_str());
				UMessageBox::MsgBox_s(strRet.c_str() );
			}
			break;
		}
		
		UCreatePlayer* pCreateRole = UDynamicCast(UCreatePlayer, SYState()->UI->GetDialogEX(DID_CREATEPLAYER));
		pCreateRole->ActiveBtn();
		return;
	}

	RoleInfo kRole;
	kRole			= msg.role;
	kRole.name	= /*UTF8ToAnis*/(msg.role.name);

	ClientState->AddRole(kRole);

	CPlayer* pkPlayer = ObjectMgr->CreatePlayer(kRole.guid, 0, false);
	pkPlayer->SetName(kRole.name.c_str());
	pkPlayer->SetRace(kRole.nRace);
	pkPlayer->SetGender(kRole.nGender);
	pkPlayer->OnCreateFromUI(kRole.displayid, kRole.nRace, kRole.nGender);

	// 装备信息
	for(unsigned int ui = 0; ui < EQUIPMENT_SLOT_END - EQUIPMENT_SLOT_START; ui++)
	{
		if(kRole.equipdisplay[ui])
		{
			ItemMgr->OnEquipmentChange(kRole.guid, kRole.equipdisplay[ui], ui + EQUIPMENT_SLOT_START);
		}
	}
	pkPlayer->Update(0.0f);
	SYState()->IAudio->PlaySound( "Sound/UI/CreateRole.wav", NiPoint3::ZERO, 1.0f, 1 );
	//新创建的默认为最新登陆的

	SystemSetup->SaveLastRollName(kRole.name.c_str());
	

	//重新显示选人界面
	UChoosePlayer* pRchoose = UDynamicCast(UChoosePlayer, SYState()->UI->GetDialogEX(DID_CHOOSEPLAYER));
	CUISystem::ShowEX(DID_CHOOSEPLAYER,TRUE,TRUE);
	pRchoose->RefreshRoleList();
	
}

void CPacketParser::ParseEnterGameNum(CPacketUsn& pakTool)
{
	MSG_S2C::stEnterGameLine  msg;
	pakTool >> msg;

	ui32 waittime =  msg.estimateTime * msg.enter_game_number; //单位毫秒
	ui32 waitnumber =  msg.enter_game_number;

	UWaitList::WaitList(waitnumber, waittime / 1000);
	//显示排队信息
}
void CPacketParser::ParseEnterGameWaitOK(CPacketUsn& pakTool)
{
	MSG_S2C::stEnterGameWaitOK msg;
	pakTool >> msg;

	// pop 
	UWaitList::BeginEnter();
}

void CPacketParser::ParseDeleteRoleMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stDelRoleAck msg;
	pakTool >> msg;
	if( msg.nError != 0 )
		return;

	std::vector<RoleInfo> kRoleList;
	int iDeleteIndex = 0;
	for(unsigned int ui = 0; ui < ClientState->GetRoleCount();  ui++)
	{
		if( msg.guid == ClientState->GetRole(ui)->guid )
		{
			iDeleteIndex = ui;
			continue;
		}

		kRoleList.push_back(*ClientState->GetRole(ui));
	}

	ClientState->ClearRole();
	for( unsigned int ui = 0; ui < kRoleList.size(); ui++ )
	{
		ClientState->AddRole(kRoleList[ui]);
	}

	//重新显示选人界面
	UChoosePlayer* pRechoose = UDynamicCast(UChoosePlayer, SYState()->UI->GetDialogEX(DID_CHOOSEPLAYER));
	pRechoose->GetControlR()->DeletePlayer(iDeleteIndex);
	pRechoose->RefreshRoleList();
}
void CPacketParser::ParseMsgBoxFormServer(CPacketUsn& pakTool)
{
	MSG_S2C::stMessageBox MsgRecv;
	pakTool>>MsgRecv;

	if (MsgRecv.message.length())
	{
		UMessageBox::MsgBox_s(MsgRecv.message.c_str());
	}
}
void CPacketParser::ParseEnterGameMsg(CPacketUsn& pakTool)
{
	// Temp 释放掉，UI界面生成的角色，否则会有问题
	//NetworkMgr->m_bSendPing = true;

	MSG_S2C::stEnterGameAck MsgRecv;pakTool>>MsgRecv;
	UControl * loading = SYState()->UI->LoadDialogEX(DID_LOADING);
	if(loading)
	{
		ObjectMgr->RemoveAll();// for stack crash
		CMap * pmap = CMap::Get();
		if (pmap)
		{
			if (ObjectMgr->GetLocalPlayer())
			{
				SYState()->IAudio->StopAllSound();
			}

			pmap->Unload();
			pmap->Reset();
		}

		ULoadingFrame* pLoadingFrame = UDynamicCast(ULoadingFrame,loading);
		if (pLoadingFrame)
		{
			SystemSetup->CreateUserDirectory();
			if (!SystemSetup->LoadFile(FILE_USERKEYMAP))
			{
				ULOG("用户快捷键加载成功！！");
			}

			SYState()->IAudio->StopAllSound();

			CUISystem::ShowEX(DID_INGAME_MAIN,TRUE,TRUE);
			pLoadingFrame->InitLoading(LOGININLOADING, MsgRecv.nMapID);
			pLoadingFrame->BeginLoading();

			
			SceneMgr->EnterLevel(MsgRecv.nMapID, MsgRecv.x, MsgRecv.y);
			if (TeamShow)
			{
				TeamShow->SetTargetGUID(0);
			}

			if (SYState()->LocalPlayerInput)
			{
				SYState()->LocalPlayerInput->ClearInput();
			}
		}
	}

	PacketBuilder->SendQueryServerTime();
	
	UChoosePlayer* pkChoose = UDynamicCast(UChoosePlayer, SYState()->UI->GetDialogEX(DID_CHOOSEPLAYER));
	if (pkChoose)
	{
		pkChoose->ActiceEnterGameBtn();
	}
}
void CPacketParser::ParseTransferAbouted(CPacketUsn& pakTool)
{
	MSG_S2C::stTransfer_Abouted stTrandfer_Abouted;
	pakTool >> stTrandfer_Abouted;

	switch(stTrandfer_Abouted.stat)
	{
	case 2:
		ClientSystemNotify(_TRAN("副本未找到。"));
		return;
	case 1:
		ClientSystemNotify(_TRAN("副本已满。"));
		return;
	default:
		break;
	}

}
void CPacketParser::ParseMoveNewWorld(CPacketUsn& pakTool)
{
	MSG_S2C::stMove_New_World stNewWorld;
	pakTool >> stNewWorld;

	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer == NULL)
		return;

	pkLocalPlayer->StopMove();
	pkLocalPlayer->SetServerAngle( stNewWorld.orientation );
	stTeleport stTele = pkLocalPlayer->GetTelePortPara();

	if ( stTele.bTelePort  )
	{
		stTele.ReSet();
	}
	//
	if (SYState()->LocalPlayerInput)
	{
		SYState()->LocalPlayerInput->LockInput();
		SYState()->LocalPlayerInput->ResetMoveKeys();
	}

	UControl * loading = SYState()->UI->LoadDialogEX(DID_LOADING);
	if (loading)
	{
		//ULoginLoading * LoadingFrame = UDynamicCast(ULoginLoading,loading);
		ULoadingFrame* pLoadingFrame = UDynamicCast(ULoadingFrame, loading);
		pLoadingFrame->InitLoading(MAPLOADING, stNewWorld.map_id);
		pLoadingFrame->BeginLoading();
	}
	CPlayerLocal::SetCUState(cus_Normal, TRUE);
	ULOG("切换地图(cus_Normal)");
	ObjectMgr->RemoveGroupObjects(CObjectManager::IDT_CHAR);
	ObjectMgr->RemoveGroupObjects(CObjectManager::IDT_CORPSE);
	ObjectMgr->RemoveGroupObjects(CObjectManager::IDT_SCENEOBJECT);

#ifdef _DEBUG
		ObjectMgr->PrintObjectList();
#endif

	SceneMgr->EnterLevel(stNewWorld.map_id, stNewWorld.pos_x, stNewWorld.pos_y);

	pkLocalPlayer->SetPosition(NiPoint3(stNewWorld.pos_x, stNewWorld.pos_y, stNewWorld.pos_z));

	if ( pkLocalPlayer->GetTelePortPara().bTelePort )
	{
		pkLocalPlayer->GetTelePortPara().ReSet();
	}

	if (pkLocalPlayer->IsHook())
	{
		HookMgr->EndHookActor();
	}
	
	pkLocalPlayer->OnMoveToNewWorld();
	if (!pkLocalPlayer->IsInArena())
	{
		pkLocalPlayer->LeaveArena();
	}

	MSG_C2S::stMove_Teleport_Ack stTeleAck;
	stTeleAck.guid = pkLocalPlayer->GetGUID();
	NetworkMgr->SendPacket(stTeleAck);
}

void CPacketParser::ParsePlayerMoveMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stMove_OP msg;
	pakTool >> msg;
	ByteBuffer* data = &msg.buffer;

	uint8 length = 0;
	uint64 guid = 0;
	uint32 pos = 0;
	uint8 moveOP = 0;
	MovementInfo move;

	while( true )
	{
		if( pos >= data->size() )
			break;

		data->rpos( pos );
		*data >> length >> guid >> moveOP >> move;
		pos += length;

		CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(guid);
		if( !pkPlayer )
			return;

		if ( pkPlayer->IsDead() )
		{
			return;
		}
	// 
		if( pkPlayer->IsMountPlayer())
		{
			pkPlayer->SetPosition(pkPlayer->GetMountFoot()->GetPosition());
			return;
		}
		if( pkPlayer->IsMountByPlayer())
		{
			pkPlayer->GetMountHead()->SetPosition(pkPlayer->GetPosition());
		}
	// 	if(pkPlayer->IsMountCreature())
	// 	{
	// 		pkPlayer->GetMountCreature()->SetPosition((pkPlayer->GetPosition()));
	// 	}

		NIASSERT(pkPlayer != ObjectMgr->GetLocalPlayer());
		NIASSERT(pkPlayer->GetGameObjectType() == GOT_PLAYER );

		CPlayer::MoveInfo stMoveInfo;
		stMoveInfo.moveOP = moveOP;
		stMoveInfo.stMove = move;
		pkPlayer->AddRemotePlayerMovementInfo(stMoveInfo);
	}
}

void CPacketParser::ParseMoveForceRoot(CPacketUsn& pakTool)
{
	MSG_S2C::stMove_Force_Root MsgRecv;
	pakTool >> MsgRecv;
	CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(MsgRecv.guid);
	if( !pkPlayer )
		return;

	pkPlayer->StopMove();
	if (g_pkConsole)
		g_pkConsole->Printf(162, "CPacketParser::ParseMoveForceRoot\n");

	if(pkPlayer == ObjectMgr->GetLocalPlayer())
	{
		CPlayerInputMgr * InputMgr = SYState()->LocalPlayerInput;
		InputMgr->ForceRoot();
	}
}
void CPacketParser::ParseMoveForceUnRoot(CPacketUsn& pakTool)
{
	MSG_S2C::stMove_Force_UnRoot MsgRecv;
	pakTool >> MsgRecv;
	CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(MsgRecv.guid);
	if( !pkPlayer )
		return;

	if (g_pkConsole)
		g_pkConsole->Printf(162, "CPacketParser::ParseMoveForceUnRoot\n");

	if(pkPlayer == ObjectMgr->GetLocalPlayer())
	{
		CPlayerInputMgr * InputMgr = SYState()->LocalPlayerInput;
		InputMgr->ForceUnRoot();
	}
}
void CPacketParser::ParseMonsterMoveMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stMove_Monster msg;
	pakTool >> msg;
	ByteBuffer* data = &msg.buffer;

	uint32 pos = 0;
	MonsterMovementInfo info;
	uint8 length = 0;

	while( true )
	{
		if( pos >= data->size() )
			break;

		data->rpos( pos );
		*data >> length >> info;
		pos += length;
		
		NiPoint3 kLocation( info.x, info.y, info.z );
		NiPoint3 kNextPoint( kLocation );

		CCharacter* pkChar = NULL;
		UpdateObj* pkObject = ObjectMgr->GetObject(info.guid);
		if(!pkObject)
		{
			char szErr[256];
			_snprintf(szErr, 256, "CPacketParser::ParseMonsterMoveMsg, 未找到对象%llu\n", info.guid); 
			NiOutputDebugString(szErr);
			return;
		}
		if (pkObject->isType(TYPE_DYNAMICOBJECT))
		{
			CDynamicObj* phDyObj = static_cast<CDynamicObj*>(pkObject);
			NiPoint3 kWayPoints(info.wx, info.wy, info.wz);

			if(info.move_flag == 0x100)
				phDyObj->SetMoveState(RunMove);
			else if(info.move_flag == 0x0)
				phDyObj->SetMoveState(WalkMove);

			switch(info.move_type)
			{
			case MOVE_TYPE_NORMAL:
				phDyObj->MoveToPosition(kLocation, kWayPoints, info.time_between);
				break;
			case MOVE_TYPE_STOP:
				{
					phDyObj->SetPosition(NiPoint3(info.x, info.y, info.z));
					phDyObj->StopMove();
				}
				break;
			case  MOVE_TYPE_POLL:
				{
					phDyObj->SetPosition(kWayPoints);
					phDyObj->SetDesiredAngle( info.orientation );
					phDyObj->StopMove();

				}
				break;
			default:
				assert(0);
			}

			return ;

		}else
		{
			pkChar = static_cast<CCharacter*>(pkObject);
		}
		

		if (pkChar->GetHP() <= 0) {
			return;
		}

		uint32 timebetween = info.time_between;

		
		
		ECHAR_MOVE_STATE eMoveState = CMS_WALK;
		if(info.move_flag == 0x100)
			eMoveState = CMS_RUN;
		else if(info.move_flag == 0x0)
			eMoveState = CMS_WALK;


		if(pkChar->isType(TYPE_PLAYER))
		{
			if ( info.move_type == MOVE_TYPE_POLL )
			{
				NiPoint3 ptTarget( info.wx, info.wy, info.wz + 2.f );
				pkChar->SetMovementFlag( 0 );
				pkChar->StartPull( ptTarget );
			}


			if ( pkChar->IsLocalPlayer() )
			{
				CPlayer* plr = (CPlayer*)pkChar;
				switch(info.move_type)
				{
				case MOVE_TYPE_NORMAL:
					{
						//plr->SetPosition( kLocation );
						kNextPoint.x = info.wx;
						kNextPoint.y = info.wy;
						kNextPoint.z = info.wz;
						
						/*NiPoint3 offset = kLocation- plr->GetPosition();
						if ( offset.Length()< 0.001 )
						{
							return;
						}
						
							
						float angle = 0.f;
						float dx = (float)msg.x - plr->GetPosition().x;
						float dy = (float)msg.y - plr->GetPosition().y;
						if(dy != 0.0f)
						{
							angle = atan2(dx, dy);
							plr->SetDesiredAngle(angle, true);
						}

						CPlayer::MoveInfo stMoveInfo;
						stMoveInfo.moveOP = MOVE_START_FORWARD;
						stMoveInfo.stMove.x = msg.x;
						stMoveInfo.stMove.y = msg.y;
						stMoveInfo.stMove.z = msg.z;
						stMoveInfo.stMove.orientation = angle;

						plr->StartForward();

						stMoveInfo.stMove.flags = plr->GetMovementFlags();
						stMoveInfo.stMove.time = msg.timestamp;

						plr->AddRemotePlayerMovementInfo(stMoveInfo);*/
						
						//plr->SetPosition( kLocation );
						plr->MoveTo(kNextPoint, CMF_None, false);
			
					}
					break;
				case  MOVE_TYPE_POLL:
					{
						
					}
					
					
		// 			if(plr == ObjectMgr->GetLocalPlayer())
		// 			{
		// 				CPlayerInputMgr * InputMgr = SYState()->LocalPlayerInput;
		// 				InputMgr->LockInput();
		// 			}
					break;
				case MOVE_TYPE_STOP:
					{
						//plr->MoveTo(kLocation, CMF_None, false);
						//plr->SetPosition( kLocation );
						plr->StopMove();
						/*
						char szErr[256];
						_snprintf(szErr, 256, "stopmove\n", msg.guid); 
						NiOutputDebugString(szErr);
						*/
					}
					
		// 			if(plr == ObjectMgr->GetLocalPlayer())
		// 			{
		// 				CPlayerInputMgr * InputMgr = SYState()->LocalPlayerInput;
		// 				InputMgr->UnlockInput();
		// 			}
					break;
				}
			}
			
		}
		else
		{
			CCreature* pkMonster = (CCreature*)pkChar;
			//	NIASSERT(msg.vWayPoints.size() > 0);
			/*
			if (msg.vWayPoints.size() <= 0)
			{
				pkMonster->StopMove();
				//assert(0);
				return;
			}
			*/
			NiPoint3 kWayPoints(info.wx, info.wy, info.wz);

			pkMonster->SetMoveState(eMoveState);
			switch(info.move_type)
			{
			case MOVE_TYPE_NORMAL:
				pkMonster->MoveToPosition(kLocation, kWayPoints, info.time_between, eMoveState);
				break;
			case MOVE_TYPE_STOP:
				{
 					pkMonster->SetPosition(NiPoint3(info.x, info.y, info.z));
					pkMonster->StopMove();
				}
				break;
			case  MOVE_TYPE_POLL:
				{
					pkMonster->SetPosition(kWayPoints);
					pkMonster->SetDesiredAngle( info.orientation );
					pkMonster->StopMove();

				}
				break;
			default:
				assert(0);
			}
		}
	}
}

//死亡通知
void CPacketParser::ParseDeathMsg(CPacketUsn& pakTool)
{
	return ;

	MSG_S2C::stAI_Died msg;
	pakTool >> msg;
	CCharacter* pkChar = static_cast<CCharacter*>( ObjectMgr->GetObject(msg.died_guid) );
	if( !pkChar )
		return;

	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if ( pkLocalPlayer->GetTargetObject() == pkChar )
	{
		
		
		

		//CPlayerLocal::SetCUState(cus_Normal);
		//ULOG("死亡通知(cus_Normal)");
	}
}

//复活回应
void CPacketParser::ParseReliveAckMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stReliveAck msg;
	pakTool >> msg;
	CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(msg.guid);
	if( !pkChar )
		return;

	if( msg.nError != 0 )
	{
		//UMessageBox::MsgBox("Relive faild!!!");
		//复活失败
		//return;
	}

	pkChar->OnAlive();
	
	//pkChar->SetWorldTransform(NiPoint3(msg.x, msg.y, msg.z), NiPoint3(0.f, 0.f, msg.dir));
}

//复活通知(原地复活的时候, 发送给周边玩家)
void CPacketParser::ParseReliveMsg(CPacketUsn& pakTool)
{

	MSG_S2C::stRelivePkg msg;
	pakTool >> msg;
	CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(msg.guid);
	if( !pkChar )
		return;

	pkChar->OnAlive();
	pkChar->SetWorldTransform(NiPoint3(msg.x, msg.y, msg.z), NiPoint3(0.f, 0.f, msg.dir));
	if ( pkChar->GetGameObjectType() == GOT_CREATURE )
	{
		CSceneEffect* pEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect( "Effect_300002230.nif",true, true, false, 1 );
		pEffect->AttachToSceneObject( msg.guid );


		ALAudioSource* pkSound = (ALAudioSource*)AudioSystem::GetAudioSystem()->CreateSource(AudioSource::TYPE_3D);
		if (!pkSound)
			return;
		pEffect->GetEffectNode()->AttachChild(pkSound);
		pkSound->SetMinMaxDistance(5.0f, 80.0f);
		pkSound->SetGain(2.4f);
		SYState()->IAudio->PlaySound(pkSound, "Sound/spell/zwmz02.wav" );
	}
	
}

void OnAcceptResurrect()
{

}

void OnDeclineResurrect()
{

}
//复活邀请
void CPacketParser::ParseSpellResurrectReq(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Resurrect_Request MsgRecv;
	pakTool >> MsgRecv;
	ULOG("ParseSpellResurrectReq");
	UReliveMsgBox* URMB = INGAMEGETFRAME(UReliveMsgBox, FRAME_IG_RESURRECTMSGBOX);
	CPlayer* plr = (CPlayer*)ObjectMgr->GetObject(MsgRecv.caster_guid);
	if (URMB && plr)
	{
		URMB->ResurrectRespond(plr->GetName());
	}
	//if(plr)
	//{
	//	char szMsg[128];
	//	sprintf(szMsg, "%s想要复活你", plr->GetName());
	//	UMessageBox::MsgBox(szMsg,(BoxFunction*)OnAcceptResurrect, (BoxFunction*)OnDeclineResurrect, NULL);
	//}
}

//回答消息发送是否成功
void CPacketParser::ParseChatAckMsg(CPacketUsn& pakTool)
{
}

//别的玩家发送过来的消息
void CPacketParser::ParseChatMsg(CPacketUsn& pakTool)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap || !pkMap->IsLoaded() )
		return;

	MSG_S2C::stChat_Message msg;
	pakTool >> msg;
	//消息内容

	bool isPlayer = false;
	if( GUID_HIPART(msg.guid) == 0 )
		isPlayer = true;

	std::string creature_name;
	if( msg.creature_name.size() )
	{
		creature_name = msg.creature_name;
		if (!isPlayer)
		{
			creature_name = _TRAN( creature_name.c_str() );
		}
	}
	else
	{
		if( msg.guid )
		{
			UpdateObj* pObj = ObjectMgr->GetObject(msg.guid);
			if( pObj )
			{
				if( pObj->isType(TYPE_PLAYER) )
				{
					creature_name = ((CPlayer*)pObj)->GetName();
				}
				else if( pObj->isType(TYPE_UNIT) )
				{
					creature_name = ((CCreature*)pObj)->GetName();
				}
			}
		}
	}

	
	
	UInGame* pUiGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pUiGame == NULL)
	{
		return ;
	}
	bool bpopo = false;
	std::string chattext = msg.txt;
	if(ChatPopoFliter(chattext))
	{
		bpopo = true;
		if (msg.type != CHAT_MSG_SYSTEM &&
			msg.type != CHAT_MSG_MONSTER_SAY&&
			msg.type != CHAT_MSG_MONSTER_YELL&&
			msg.type != CHAT_MSG_MONSTER_WHISPER && 
			msg.type != CHAT_MSG_SYSTEM_BROADCAST)
		{
			GetFilterChatMsg(chattext);
		}
	}
		
	if (msg.type != CHAT_MSG_SYSTEM &&
		msg.type != CHAT_MSG_MONSTER_SAY&&
		msg.type != CHAT_MSG_MONSTER_YELL&&
		msg.type != CHAT_MSG_MONSTER_WHISPER && 
		msg.type != CHAT_MSG_SYSTEM_BROADCAST)
	{
		GetFilterChatMsg(msg.txt);
	}

	string text;
	if(msg.type != CHAT_MSG_MONSTER_SAY)
	{
		if (creature_name.size())
		{
			text = "<a:#Name_"+creature_name+">["+ creature_name +"]</a>:"+ msg.txt + "<br>";
		}
		else
		{
			text = ":" + msg.txt + "<br>";
		}
	}
	else
		text = msg.txt + "<br>";

	bool isShowChatPopo = false;
	switch(msg.type)
	{
	case CHAT_MSG_SAY:
		{
			if (msg.txt.length() != 0 && msg.guid != 0){
				ChatSystem->AppendChatMsg(CHAT_MSG_SAY,text.c_str());
				isShowChatPopo = true;
			}
		}
		break;
	case CHAT_MSG_PARTY:
		{
			ChatSystem->AppendChatMsg(CHAT_MSG_PARTY,text.c_str());
		}
		break;
	case CHAT_MSG_RAID:
		{
			if (SocialitySys->GetTeamSysPtr()->IsTeamLeader(creature_name.c_str()))
				ChatSystem->AppendChatMsg(CHAT_MSG_RAID_LEADER,text.c_str());
			else
				ChatSystem->AppendChatMsg(CHAT_MSG_RAID,text.c_str());
		}
		break;
	case CHAT_MSG_WHISPER:
		{
			ChatSystem->SetLastWhisperName(creature_name);
			ChatSystem->AppendChatMsg(CHAT_MSG_WHISPER, text.c_str());
			SYState()->IAudio->PlayUiSound(UI_news);
		}
		break;
	case CHAT_MSG_WHISPER_INFORM:
		{
			ChatSystem->SetLastWhisperName(creature_name);
			ChatSystem->AppendChatMsg(CHAT_MSG_WHISPER_INFORM, text.c_str());
		}
		break;
	case CHAT_MSG_GUILD:
		{
			ChatSystem->AppendChatMsg(CHAT_MSG_GUILD, text.c_str());
		}
		break;
	case CHAT_MSG_SYSTEM:
		{
			ChatMsgNotify(CHAT_MSG_SYSTEM, msg.txt.c_str());
			SYState()->IAudio->PlayUiSound(UI_system);
		}
		break;
	case CHAT_MSG_MONSTER_SAY:
		{
			if (msg.txt.length() != 0 && msg.guid != 0)
			{
				std::string strRet = _TRAN(N_NOTICE_Z14, creature_name.c_str(), msg.txt.c_str() );
				ChatMsgNotify(CHAT_MSG_MONSTER_SAY,strRet.c_str());
				isShowChatPopo = true;
			}
		}
		break;	
	case CHAT_MSG_MONSTER_YELL:
		{
			if (msg.txt.length() != 0 && msg.guid != 0)
			{
				std::string strRet = _TRAN(N_NOTICE_Z15, creature_name.c_str(), msg.txt.c_str() );

				ChatMsgNotify(CHAT_MSG_MONSTER_YELL,strRet.c_str(),creature_name.c_str(), msg.txt.c_str());
				isShowChatPopo = true;
			}
		}
		break;
	case CHAT_MSG_MONSTER_WHISPER:
		{
			if (msg.txt.length() != 0 && msg.guid != 0)
			{
				std::string strRet = _TRAN(N_NOTICE_Z16, creature_name.c_str(), msg.txt.c_str() );
				ChatMsgNotify(CHAT_MSG_MONSTER_SAY,strRet.c_str() );
				UISystemMsg* pSGN = INGAMEGETFRAME(UISystemMsg, FRAME_IG_SYSGIFTNOTIFY);
				if (pSGN)
				{
					pSGN->GetSystemMsg( strRet.c_str());
				}
			}
		}
		break;
	case CHAT_MSG_DALABA:  //大喇叭
		{
			ChatSystem->AppendChatMsg(CHAT_MSG_DALABA,text.c_str());
		}
		break;
	case CHAT_MSG_XIAOLABA:  //小喇叭
		ChatSystem->AppendChatMsg(CHAT_MSG_XIAOLABA,text.c_str());
		break;
	case CHAT_MSG_SYSTEM_BROADCAST:
		ChatSystem->AppendChatMsg(CHAT_MSG_SYSTEM_BROADCAST,text.c_str());
		break;
	case CHAT_MSG_CHANNEL:
		{
			if (msg.channel_id == 0)
			{
				ChatSystem->SetPChannelName(msg.channel_name);
				ChatSystem->AppendChatMsg(CHAT_MSG_PRIVATE, text.c_str());
			}
			else
			{
				switch (msg.channel_id)
				{
				case CHAT_CHANNEL_TYPE_CITY:
					{
						ChatSystem->AppendChatMsg(CHAT_MSG_CITY,text.c_str());
					}
					break;
				case CHAT_CHANNEL_TYPE_TRADE:
					{
						ChatSystem->AppendChatMsg(CHAT_MSG_TRADE,text.c_str());
					}
					break;
				case CHAT_CHANNEL_TYPE_LFG:
					{
						ChatSystem->AppendChatMsg(CHAT_MSG_LFG,text.c_str());
					}
					break;
				}
			}
		}
		break;
	case CHAT_MSG_AFK:
		{
			ChatSystem->AppendChatMsg(CHAT_MSG_AFK,text.c_str());
		}
		break;
	case CHAT_MSG_DND:
		{
			ChatSystem->AppendChatMsg(CHAT_MSG_DND,text.c_str());
		}
		break;
	default :
		{
			ChatMsgNotify(CHAT_MSG_SYSTEM,text.c_str());
		}
	}
	if( msg.guid && isShowChatPopo)
	{
		UpdateObj* pObj = ObjectMgr->GetObject(msg.guid);
		if(pObj &&  (pObj->isType(TYPE_PLAYER) || pObj->isType(TYPE_UNIT)) && bpopo)
		{
			((CCharacter*)pObj)->SetChatText(chattext);
		}
	}
}
void CPacketParser::ParseSystemNotify(CPacketUsn& pakTool)
{
	MSG_S2C::stSystemNotifyEnum msg;
	pakTool>>msg;

	string str ;
	switch (msg.e)
	{
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_0:
		{
			str = _TRAN("交易状态不能执行这项操作");
		}
		break; 
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_1:
		{
			str = _TRAN("部族名称太长");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_2:
		{
			str = _TRAN("部族名称含有非法字符");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_3:			// 你已经在部族里,请先脱离部族
		{
			str = _TRAN("你已经在部族里,请先脱离部族");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_4:			// 你24小时内加入过其他部族,请耐心等待
		{
			str = _TRAN("你24小时内加入过其他部族,请耐心等待");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_5:			// 此部族名字已经被使用
		{
			str = _TRAN("此部族名字已经被使用");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_6:			// 你必须拥有至少10金币
		{
			str = _TRAN("你必须拥有至少10金币");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_7:			// 你的等级必须大于等于10级
		{
			str = _TRAN("你的等级必须大于等于10级");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_8:			// 没有找到该玩家
		{
			str = _TRAN("没有找到该玩家");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_9:			// 你不属于任何一个部族
		{
			str = _TRAN("你不属于任何一个部族");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_10:			// 该玩家已经加入了一个部族
		{
			str = _TRAN("该玩家已经加入了一个部族");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_11:			// 该玩家已经被别人邀请加入部族
		{
			str = _TRAN("该玩家已经被别人邀请加入部族");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_12:			// 你没有权限邀请别人加入部族
		{
			str = _TRAN("你没有权限邀请别人加入部族");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_13:			// 该玩家和你不是同一种族
		{
			str = _TRAN("该玩家和你不是同一种族");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_14:			// 该玩家退出部族还没超过24小时
		{
			str = _TRAN("该玩家退出部族还没超过24小时");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_15:		// 您队伍中有人不在线
		{
			str = _TRAN("您队伍中有人不在线");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_16:		// 已加入战场的队列中
		{
			str = _TRAN("已加入战场的队列中");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_17:		// 战场的队列已取消
		{
			str = _TRAN("战场的队列已取消");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_18:		// 您的队伍中人数超过5人,不可以加入竞技场队列
		{
			str = _TRAN("您的队伍中人数超过5人,不可以加入竞技场队列");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_19:		// 您队伍中有玩家元宝不足,不能进入竞技场
		{
			str = _TRAN("您队伍中有玩家元宝不足,不能进入竞技场");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_20:		// 您队伍中有玩家未满足进入竞技场的等级要求
		{
			str = _TRAN("您队伍中有玩家未满足进入竞技场的等级要求");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_21:		// 您队伍中有玩家正在副本中或者正在副本排队中
		{
			str = _TRAN("您队伍中有玩家正在副本中或者正在副本排队中");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_22:		// 您队伍中有玩家下线了,不可以排队
		{
			str = _TRAN("您队伍中有玩家下线了,不可以排队");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_23:		// 已加入竞技场的队列中
		{
			str = _TRAN("已加入竞技场的队列中");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_24:		// 竞技场队列已取消
		{
			str = _TRAN("竞技场队列已取消");
		}
		break;
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_RELIVE_ITEM_IN_RAID:
		{
			str = _TRAN("团队副本内不可以使用复活石");
		}break;		// 团队副本内不可以使用复活石
	case MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_RELIVE_ITEM_IN_BG:		// 战场内不可以使用复活石
		{
			str = _TRAN("战场内不可以使用复活石");
		}
		break;
	}
	if (str.length())
	{
		EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), str.c_str());
		ClientSystemNotify(str.c_str());
	}

}
void CPacketParser::ParseNotFoundPlayer(CPacketUsn& pakTool)
{
	MSG_S2C::stChat_Not_Found_Player msg;
	pakTool >> msg;
	ChatMsgNotify(CHAT_MSG_SYSTEM, _TRAN("请求失败，没有发现该玩家或该玩家已下线。"));
}



void CPacketParser::ParseMoveTeleport(CPacketUsn& pakTool)
{
	MSG_S2C::stMove_Teleport_Ack stMoveTeleport;
	pakTool >> stMoveTeleport;

	UpdateObj* pkObj = ObjectMgr->GetObject(stMoveTeleport.guid);
	NIASSERT(pkObj && pkObj->isType(TYPE_PLAYER));
	if (pkObj == NULL || !pkObj->isType(TYPE_PLAYER))
		return;

	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer == NULL)
		return;

	CPlayer* pkPlayer = (CPlayer*)pkObj;
	NIASSERT(pkPlayer != NULL);
	if (pkPlayer == NULL)
		return;

	if ( pkPlayer->IsJumping() )
	{
		pkPlayer->EndJump();
	}

	bool bLocalPlayer = (stMoveTeleport.guid == pkLocalPlayer->guid)?true:false;
	if (bLocalPlayer) {
//		ObjectMgr->RemoveGroupObjects(CObjectManager::IDT_CHAR);
//		ObjectMgr->RemoveGroupObjects(CObjectManager::IDT_CORPSE);
//		ObjectMgr->RemoveGroupObjects(CObjectManager::IDT_SCENEOBJECT);

//#ifdef _DEBUG
//		ObjectMgr->PrintObjectList();
//#endif

		SYState()->LocalPlayerInput->LockInput();
		// 非本地玩家传送后位在显示列表会收到移除消息
	}

	pkPlayer->ResetState();
	pkPlayer->Teleport(NiPoint3(stMoveTeleport.x, stMoveTeleport.y, stMoveTeleport.z), stMoveTeleport.orientation);
	
// 	SceneMgr->EnterLevel(pkLocalPlayer->, stMoveTeleport.x, stMoveTeleport.y);
// 	ObjectMgr->GetLocalPlayer()->SetPosition(NiPoint3(stMoveTeleport.x, stMoveTeleport.y, stMoveTeleport.z));

	if (bLocalPlayer) {
		SYState()->LocalPlayerInput->UnlockInput();
		pkLocalPlayer->ContinueTraceGatePathMove();

	}
	

	/*if (bLocalPlayer)
	{
		MSG_C2S::stMove_Teleport_Ack stTeleAck;
		stTeleAck.guid = stMoveTeleport.guid;

		NetworkMgr->SendPacket(stTeleAck);
	}*/
}

//系统消息
void CPacketParser::ParseSystemMsg(CPacketUsn& pakTool)
{
	//MSG_S2C::CHAT_MESSAGE msg;
	//pakTool >> msg;
	//CUIChat::SystemMsg(AnisToUTF8(msg.strMessage));
}

// 交易列表
void CPacketParser::ParseItemInventoryMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItem_List_Inventroy MsgRecv;
	pakTool>>MsgRecv;

	UNPCTrade * npcTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);
	if (npcTrade)
	{
		npcTrade->OnItemList(MsgRecv);
		HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_BUY);
	}
}

void CPacketParser::ParseItemSwapFailure(CPacketUsn& pakTool)
{
	string error;
	MSG_S2C::stItem_Inv_Change_Failure MsgRecv;pakTool>>MsgRecv;
	switch(MsgRecv.error)
	{
	case IVN_ERR_BUILD_OK:
		error = _TRAN("修理成功");return;
	case INV_ERR_YOU_MUST_REACH_LEVEL_N:
		error = _TRAN("等级不够");break;
	case INV_ERR_ITEM_RANK_NOT_ENOUGH:
		error = _TRAN("使用等级不够");break;
	case INV_ERR_ITEM_DOESNT_GO_TO_SLOT:
		error = _TRAN("无法移动此物品");break;
	case INV_ERR_CANT_CARRY_MORE_OF_THIS:
		{

			error = _TRAN("无法携带更多此类物品");break;
			CPlayerLocal* pklocal = ObjectMgr->GetLocalPlayer();
			if (pklocal && pklocal->IsHook())
			{
				return  ;
			}
		}
	case INV_ERR_BAG_FULL:
	case INV_ERR_INVENTORY_FULL:
		{
			error = _TRAN("背包已满");
			CPlayerLocal* pklocal = ObjectMgr->GetLocalPlayer();
			UPickItemList * pPickItemList = INGAMEGETFRAME(UPickItemList,FRAME_IG_PICKLISTDLG);
			if (pklocal && pklocal->IsHook())
			{
				
				if (pPickItemList && pPickItemList->IsVisible())
				{
					pPickItemList->OnClose();
					pPickItemList->SetHookAutoPick(FALSE);
				}

				HookMgr->StopPickState(0);
				return ;
			}else
			{
				if (pPickItemList && pPickItemList->IsVisible())
				{
					pPickItemList->BagFull();
				}
			}
		}break;
	case INV_ERR_ITEM_CANT_BE_EQUIPPED:
		error = _TRAN("此物品无法装备");break;
	case INV_ERR_CANT_DO_IN_COMBAT:
		error = _TRAN("战斗状态无法装备");break;
    case INV_ERR_CANT_USE_ON_INSTACE:
        error = _TRAN("副本中无法使用这个道具");break;
	case INV_ERR_YOU_CAN_NEVER_USE_THAT_ITEM:
		error = _TRAN("您不能使用该物品：种族不匹配");break;
	case INV_ERR_YOU_CAN_NEVER_USE_THAT_ITEM2:
		error = _TRAN("您不能使用该物品：职业不匹配");break;
	case INV_ERR_NO_EQUIPMENT_SLOT_AVAILABLE2:
		error = _TRAN("您无法装备这个道具");break;
	case INV_ERR_CANT_DUAL_WIELD:
		error = _TRAN("无法双持");break;
	case INV_ERR_ALREADY_LOOTED:
		error = _TRAN("已经拾取这个道具");return;
	case INV_ERR_ITEM_IS_CURRENTLY_SOLD_OUT:
		error = _TRAN("道具已经被卖出");break;
	case INV_ERR_NOT_ENOUGH_MONEY:
		error = _TRAN("金钱不够!");break;
	case INV_ERR_SLOT_IS_EMPTY:
		error = "";break;
	case INV_ERR_NONEMPTY_BAG_OVER_OTHER_BAG:
		error = _TRAN("包裹里有物品, 移动错误."); break;
	case IVN_ERR_CANT_DESTORY:
		error = _TRAN("无法销毁这个道具");break;
	case IVN_ERR_CANT_SELL:
		error = _TRAN("无法出售这个道具");break;
	case IVN_ERR_CANT_TRADE:
		error = _TRAN("无法交易这个道具");break;
	case INV_ERR_CANT_USE_ON_MOUNT:
		error = _TRAN("骑乘时无法使用这个道具");break;
	case INV_ERR_NO_TITLE:
		error = _TRAN("当前称号不能使用此道具");break;
	default:
		//sprintf_s(szError, 128, "道具错误[%d],原因[%s]", MsgRecv.error, error);
		//error = szError;
		error = _TRAN("未知错误！");
		ULog("错误ID = %d",MsgRecv.error);
		return;
	}
	ClientSystemNotify( error.c_str() );
	EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f),  error.c_str(), 0XFFFF0000);
	//UMessageBox::MsgBox_s(error.c_str());
}
void CPacketParser::ParseItemBuyMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItem_Buy MsgRecv;
	pakTool>>MsgRecv;
	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(MsgRecv.itemid);
	if (ItemInfo)
	{
		std::string strRet = _TRAN(N_NOTICE_Z18, _I2A(MsgRecv.count).c_str(), ItemInfo->C_name.c_str() );
		ClientSystemNotify(strRet.c_str());

		strRet = _TRAN(19, ItemInfo->C_name.c_str() );
		EffectMgr->AddEffeText(NiPoint3(300.f,300.f,0.f), strRet.c_str() );
		//SYState()->IAudio->PlaySound(UI_moneyclick);
		//ChatSystem->ItemGotMsg(MsgRecv.itemid, MsgRecv.count);
	}
}

void CPacketParser::ParseItemBuyFailureMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItem_Buy_Failure MsgRecv;
	pakTool>>MsgRecv;
	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(MsgRecv.itementry);
	if (ItemInfo)
	{	
		string error;
		switch(MsgRecv.error)
		{
		case INV_ERR_YOU_MUST_REACH_LEVEL_N:
			error = _TRAN("等级不够");break;
		case INV_ERR_ITEM_DOESNT_GO_TO_SLOT:
			error =  _TRAN("无法移动此物品");break;
		case INV_ERR_CANT_CARRY_MORE_OF_THIS:
			error =  _TRAN("无法携带更多此类物品");break;
		case INV_ERR_BAG_FULL:
		case INV_ERR_INVENTORY_FULL:
			error =  _TRAN("背包已满");break;
		case INV_ERR_ITEM_CANT_BE_EQUIPPED:
			error =  _TRAN("此物品无法装备");break;
		case INV_ERR_CANT_DO_IN_COMBAT:
			error =  _TRAN("战斗状态无法装备");break;
        case INV_ERR_CANT_USE_ON_INSTACE:
            error =  _TRAN("副本中无法使用这个道具");break;
		case INV_ERR_YOU_CAN_NEVER_USE_THAT_ITEM:
			error =  _TRAN("您不能使用该物品：种族不匹配");break;
		case INV_ERR_YOU_CAN_NEVER_USE_THAT_ITEM2:
			error =  _TRAN("您不能使用该物品：职业不匹配");break;
		case INV_ERR_NO_EQUIPMENT_SLOT_AVAILABLE2:
			error =  _TRAN("您无法装备这个道具");break;
		case INV_ERR_CANT_DUAL_WIELD:
			error =  _TRAN("无法双持");break;
		case INV_ERR_ALREADY_LOOTED:
			error =  _TRAN("已经拾取这个道具");break;
		case INV_ERR_ITEM_IS_CURRENTLY_SOLD_OUT:
			error =  _TRAN("道具已经被卖出");break;
		case INV_ERR_NOT_ENOUGH_MONEY:
			error =  _TRAN("金钱或者道具不够");break;
		case INV_ERR_SLOT_IS_EMPTY:
			error = "";break;
		case INV_ERR_NONEMPTY_BAG_OVER_OTHER_BAG:
			error =  _TRAN("包裹里有物品, 移动错误."); break;
		default:
			//sprintf_s(szError, 128, "道具错误[%d],原因[%s]", MsgRecv.error, error);
			//error = szError;
			error =  _TRAN("未知错误！");
			ULog("错误ID = %d",MsgRecv.error);
			break;
		}

		std::string strRet = _TRAN(N_NOTICE_Z21, ItemInfo->C_name.c_str(), error.c_str() );
		ClientSystemNotify( strRet.c_str() );
	}
}

void CPacketParser::ParseItemSellFailureMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItem_Sell_Failure MsgRecv;
	pakTool>>MsgRecv;
	SYItem* pItem = (SYItem*)ObjectMgr->GetObject(MsgRecv.itemguid);
	if (pItem)
	{
		ItemPrototype_Client* ItemInfo = pItem->GetItemProperty();
		if ( ItemInfo)
		{
			std::string strRet = _TRAN(N_NOTICE_Z22, ItemInfo->C_name.c_str(), _I2A(MsgRecv.error).c_str() );
			ClientSystemNotify( strRet.c_str() );
		}
	}
}

void CPacketParser::ParseItemInventoryChangeFailureMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItem_Inv_Change_Failure MsgRecv;
	pakTool>>MsgRecv;
}

void CPacketParser::ParseItemSellMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItem_Sell MsgRecv;
	pakTool>>MsgRecv;
	SYItem* pItem = (SYItem*)ObjectMgr->GetObject(MsgRecv.itemguid);
	if (pItem)
	{
		ItemPrototype_Client* ItemInfo = pItem->GetItemProperty();
		if (ItemInfo)
		{	
			std::string strRet = _TRAN(N_NOTICE_Z23, ItemInfo->C_name.c_str() );
			ClientSystemNotify( strRet.c_str() );
		}
	}
}

void CPacketParser::ParseItemReadOkMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItem_Read_OK MsgRecv;
	pakTool>>MsgRecv;
}

void CPacketParser::ParseItemQuerySingleResponseMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItem_Query_Single_Respone MsgRecv;
	pakTool>>MsgRecv;
}

void CPacketParser::ParseItemPushResultMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItemPushResult MsgRecv;
	pakTool>>MsgRecv;
	CPlayer* pkLocal = ObjectMgr->GetLocalPlayer();
	if (MsgRecv.recivee_guid == pkLocal->GetGUID())
	{
		ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(MsgRecv.entry);
		if (ItemInfo)
		{
			ChatSystem->ItemGotMsg(MsgRecv.entry, MsgRecv.count);

			switch( ItemInfo->Class )
			{
			case ITEM_CLASS_CONSUMABLE:
				{
					if ( ItemInfo->SubClass == ITEM_SUBCLASS_CONSUMABLE_POTION )
					{
						SYState()->IAudio->PlayUiSound( "Sound/items/posion.wav", NiPoint3::ZERO, 2.f, 1 );
					}
					else
						SYState()->IAudio->PlayUiSound( "Sound/items/food.wav", NiPoint3::ZERO, 1.5f, 1 );
				}
				break;

			case ITEM_CLASS_WEAPON:
				{
					SYState()->IAudio->PlayUiSound( "Sound/items/weapon.wav", NiPoint3::ZERO, 1.5f, 1 );
				}
				break;

			case ITEM_CLASS_JEWELRY:
				{
					SYState()->IAudio->PlayUiSound( "Sound/items/gems.wav", NiPoint3::ZERO, 1.5f, 1 );
				}
				break;
			case ITEM_CLASS_ARMOR:
				{
					SYState()->IAudio->PlayUiSound( "Sound/items/Armor.wav", NiPoint3::ZERO, 1.5f, 1 );
				}
				break;
			case ITEM_CLASS_RECIPE:
				{
					SYState()->IAudio->PlayUiSound( "Sound/items/book.wav", NiPoint3::ZERO, 1.5f, 1 );
				}
				break;

			default:
					SYState()->IAudio->PlayUiSound( "Sound/items/bags.wav", NiPoint3::ZERO, 1.5f, 1 );
				break;
			}
		}
	}
}

void CPacketParser::ParseItemNameQueryResponseMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItem_Name_Query_Response MsgRecv;
	pakTool>>MsgRecv;
}

void CPacketParser::ParseItemTimeUpdateMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItemTimeUpdate MsgRecv;
	pakTool>>MsgRecv;
}

void CPacketParser::ParseItemEnchantTimeUpdateMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItem_Enchant_TimeUpdate MsgRecv;
	pakTool>>MsgRecv;
}

void CPacketParser::ParseItemEnchantmentLogMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stItem_Enchant_Log MsgRecv;
	pakTool>>MsgRecv;
}

void CPacketParser::ParseExpGain(CPacketUsn& pakTool)
{
	MSG_S2C::stLog_XP_Gain MsgRecv;pakTool>>MsgRecv;
	ChatSystem->ExpGotMsg(MsgRecv.xp);
}

void CPacketParser::ParseShowBank(CPacketUsn& pakTool)
{
	MSG_S2C::stNPC_Show_Bank MsgRecv;
	pakTool>>MsgRecv;
	UBank * puBank = INGAMEGETFRAME(UBank,FRAME_IG_BANKDLG);
	if (puBank)
	{
		puBank->SetBankerGuid(MsgRecv.guid);
		HELPEVENTTRIGGER(TUTORIAL_FLAG_BANK);
	}
}
// Trade
void CPacketParser::ParseTrade_StateMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stTradeStat MsgRecv;
	pakTool>>MsgRecv;

	UPlayerTrade * pPlayerTrade = INGAMEGETFRAME(UPlayerTrade,FRAME_IG_PLAYERTRADEDLG);
	if (pPlayerTrade == NULL)
	{
		return ;
	}
	switch(MsgRecv.nStat)
	{
	case TRADE_STATUS_PLAYER_BUSY:
		{
			
		}
		break;
	case TRADE_STATUS_PROPOSED:
		{
			CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(MsgRecv.guid);

			std::string name = _TRAN("玩家") ;
			if (pkPlayer)
			{
				name = pkPlayer->GetName();
			}
			std::string strRet = _TRAN(N_NOTICE_Z24, name.c_str());
			UMessageBox::MsgBox(strRet.c_str(),(BoxFunction*)UItemSystem::PlayerAgreeTrade,(BoxFunction*)UItemSystem::PlayerRefuseTrade, 10.f);
			
			CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
			NIASSERT(pkLocalPlayer);
			pkLocalPlayer->StopMove();
		}
		break;
	case TRADE_STATUS_INITIATED:
		{
			// 弹出交易对话框
			//UInGame* pkUinGame =(UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
			pPlayerTrade->SetTradePlayerName(MsgRecv.guid);
			//pPlayerTrade->SetHeadImage();

			CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
			NIASSERT(pkLocalPlayer);
			pkLocalPlayer->StopMove();
			
		}
		break;
	case TRADE_STATUS_CANCELLED:
		{
			CPlayerLocal::SetCUState(cus_Normal);
			ULOG("交易取消(cus_Normal)");
		}
		break;
	case TRADE_STATUS_ACCEPTED:
		{
			;
		}
		break;
	case TRADE_STATUS_ALREADY_TRADING:
		{
			CPlayerLocal::SetCUState(cus_Normal);		//
			ULOG("对方正在交易(cus_Normal)");
			EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("对方正在交易！"));
		}
		break;
	case TRADE_STATUS_LOCKED:
		{
			pPlayerTrade->SetTelnetItemContainerLock();
			EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("对方锁定交易！"));
		}
		break;
	case TRADE_STATUS_PLAYER_NOT_FOUND:
		{
			EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("没有找到对方！"));
		}
		break;
	case TRADE_STATUS_STATE_CHANGED:
		break;
	case TRADE_STATUS_COMPLETE:
		{
			//先清空UI上的信息,在重新设置状态
			UInGame* pUiGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);

			CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(MsgRecv.guid);


			if (pkPlayer)
			{
				string name = pkPlayer->GetName() ;
				ClientSystemNotify(_TRAN(N_NOTICE_C78, name.c_str()).c_str());
			}
			
			ItemMgr->AddTradeSucesseMsg();

			pPlayerTrade->SetVisible(FALSE);
			pPlayerTrade->ClearAll();
			ItemMgr->ClearPlayerTradeContainer();		//
			
			//交易完成
			CPlayerLocal::SetCUState(cus_Normal);
			ULOG("交易完成(cus_Normal)");
			EffectMgr->AddEffeText(NiPoint3(300.0f,300.f,0.0f),_TRAN("交易成功！"));
		}
		break;
	case TRADE_STATUS_UNACCEPTED:
		{
			EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("对方拒绝交易！"));
		}
		break;
	case TRADE_STATUS_TOO_FAR_AWAY:
		{
			EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("距离太远！"));
		}
		break;
	case TRADE_STATUS_WRONG_FACTION:
		{
			EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("不是一个阵营，无法交易！"));
		}
		break;
	case TRADE_STATUS_FAILED:
		{
			CPlayerLocal::SetCUState(cus_Normal);
			ULOG("交易失败(cus_Normal)");
			EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("交易失败！"));
		}
		break;
	case TRADE_STATUS_DEAD:
		{
			EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("当前目标无法交易！"));
		}
		break;
	case TRADE_STATUS_PETITION:
	case TRADE_STATUS_PLAYER_IGNORED:
		{
			CPlayerLocal::SetCUState(cus_Normal);
			ULOG("交易错误(cus_Normal)");
			//UMessageBox::MsgBox_s("error");
		}
		break;
	case TRADE_STATUS_DIFF_SRV:
		{
			CPlayerLocal::SetCUState(cus_Normal);
			EffectMgr->AddEffectText(_TRAN("交易失败：不同服务器间无法交易！"));
		}
		break;;
	}
}

void CPacketParser::ParseTrade_State_ExtMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stTradeStat_Ext MsgRecv;
	pakTool>>MsgRecv;

	ItemMgr->OnItemTradeStateEx(MsgRecv);
}

//
void CPacketParser::ParseDestroyObjMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stDestroyObj MsgRecv;
	pakTool>>MsgRecv;
	//if (UTeamSystem::pm_TeamSystem->FindPosInData(MsgRecv.guid) == -1)
	{
		ObjectMgr->RemoveObject(MsgRecv.guid);
	}
}

void CPacketParser::ParsePlayerRebirth(CPacketUsn& pakTool)
{
	int charid = 1000;
	CPlayer* pkPlayer = ObjectMgr->GetPlayer(charid);
	NIASSERT(pkPlayer != NULL);

	pkPlayer->Rebirth();

}

//////////////////////////////////////////////////////////////////////////
// Quest
//////////////////////////////////////////////////////////////////////////
// 服务器返回选项列表
void CPacketParser::ParseNpcGossipMessageMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stNPC_Gossip_Message MsgRecv;
	pakTool>>MsgRecv;

	UNpcDlg * pNpcDlg = INGAMEGETFRAME(UNpcDlg,FRAME_IG_NPCDLG);
	if (pNpcDlg)
	{
		pNpcDlg->OnNetmessage(MsgRecv);
		
	}

	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer)
	{
		pkLocalPlayer->SendTranceBegin(MsgRecv.CreatureGuid);
	}
}

// 收到任务说明
void CPacketParser::ParseQuestGiverQuestDetailsMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stQuestGiver_Quest_Details MsgRecv;
	pakTool>>MsgRecv;

	UNpcQuestDlg * pNpcQuestDlg = INGAMEGETFRAME(UNpcQuestDlg,FRAME_IG_NPCQUESTDLG);
	if (pNpcQuestDlg)
	{
		pNpcQuestDlg->SetVisible(TRUE);
		pNpcQuestDlg->OnNetMessage(MsgRecv);
	}
}

// 任务奖励列表
void CPacketParser::ParseQuestGiverOfferRewardMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stQuestGiver_Offer_Reward MsgRecv;
	pakTool>>MsgRecv;

	//UInGame* pIngame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	//pIngame->GetUQuestDlg()->SetVisible(TRUE);
	//pIngame->GetUQuestDlg()->OnNetMessage(MsgRecv);

	UNpcQuestDlg * pNpcQuestDlg = INGAMEGETFRAME(UNpcQuestDlg,FRAME_IG_NPCQUESTDLG);
	if (pNpcQuestDlg)
	{
		//pNpcQuestDlg->SetVisible(TRUE);
		pNpcQuestDlg->OnNetMessage(MsgRecv);
	}
}

// 任务完成所需条件
void CPacketParser::ParseQuestGiverRequestItems(CPacketUsn& pakTool)
{
	MSG_S2C::stQuestGiver_Request_Items MsgRecv;
	pakTool>>MsgRecv;

	//UInGame* pIngame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	//pIngame->GetUQuestDlg()->SetVisible(TRUE);
	//pIngame->GetUQuestDlg()->OnNetMessage(MsgRecv);

	UNpcQuestDlg * pNpcQuestDlg = INGAMEGETFRAME(UNpcQuestDlg,FRAME_IG_NPCQUESTDLG);
	if (pNpcQuestDlg)
	{
		//pNpcQuestDlg->SetVisible(TRUE);
		pNpcQuestDlg->OnNetMessage(MsgRecv);
	}
	//UInGame* pkInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	//pkInGame->GetNpcQuestDlg()->SetVisible(TRUE);
	//pkInGame->GetNpcQuestDlg()->OnNetMessage(MsgRecv);
}
void CPacketParser::ParseQuestGiverInRangeQuery(CPacketUsn& pakTool)
{
	MSG_S2C::stQuestGiver_Inrange_Status_Query_Response MsgRecv;pakTool>>MsgRecv;
	CGameObject* pkCreature = NULL ;
	for (int i = 0; i < MsgRecv.vQuestGivers.size(); i++)
	{
		pkCreature = (CGameObject*)(ObjectMgr->GetObject(MsgRecv.vQuestGivers[i].npc_guid));
		if (pkCreature && pkCreature->GetGameObjectType() == GOT_CREATURE)
		{
			((CCreature*)pkCreature)->OnQuestStatus(MsgRecv.vQuestGivers[i].stat);
		}
	}

}
void CPacketParser::ParseQuestGiverStatusQuery(CPacketUsn& pakTool)
{
	MSG_S2C::stQuestGiver_Stats MsgRecv;pakTool>>MsgRecv;

	CGameObject* pkCreature = (CGameObject*)(ObjectMgr->GetObject(MsgRecv.guid));
	if(pkCreature == NULL)
		return;
	if (pkCreature->GetGameObjectType() == GOT_CREATURE)
	{
		((CCreature*)pkCreature)->OnQuestStatus(MsgRecv.stats);
	}
	
}

// 任务日志完成刷新
void CPacketParser::ParseQuestUpdateComplete(CPacketUsn& pakTool)
{
	MSG_S2C::stQuest_Update_Complete MsgRecv;
	pakTool>>MsgRecv;

}

// 已杀死 name count/tcount
void CPacketParser::ParseQuestUpdateAddSkill(CPacketUsn& pakTool)
{
	MSG_S2C::stQuest_Update_Add_Skill MsgRecv;
	pakTool>>MsgRecv;

	std::string strName;
	if(ItemMgr->GetCreatureName(MsgRecv.entry, strName))	
	{
		//char buf[512];
		//sprintf(buf, "[任务信息]已清除 %s  %d/%d", (char*)strName.c_str(), MsgRecv.count, MsgRecv.tcount);

		std::string strRet = _TRAN(N_NOTICE_Z25, strName.c_str(), _I2A(MsgRecv.count).c_str(), _I2A(MsgRecv.tcount).c_str() );

		ClientSystemNotify(strRet.c_str());
		EffectMgr->AddEffectText(strRet.c_str(), 1);
	}
}

// 已拾取 name count/tcount
void CPacketParser::ParseQuestUpdateAddItem(CPacketUsn& pakTool)
{
	MSG_S2C::stQuest_Update_Add_Item MsgRecv;
	pakTool>>MsgRecv;

	CItemSlot* pkItemSlot = ItemMgr->GetItemSlotByItemId(MsgRecv.item_entry);
	ItemPrototype_Client* pkIPC = ItemMgr->GetItemPropertyFromDataBase(MsgRecv.item_entry);
	if(/*pkItemSlot && */pkIPC)
	{
		if(MsgRecv.count)
		{
			std::string strRet = _TRAN(N_NOTICE_Z26,  GetQualityColorStr(pkIPC->Quality).c_str(), _I2A(MsgRecv.item_entry).c_str(), (char*)pkIPC->C_name.c_str(), _I2A(MsgRecv.count).c_str(), _I2A(MsgRecv.tcount).c_str() );
			ClientSystemNotify(strRet.c_str());//"[任务信息]获得 <linkcolor:%s><a:#Item_%u>[%s]</a>  %d/%d", GetQualityColorStr(pkIPC->Quality).c_str(), MsgRecv.item_entry, (char*)pkIPC->C_name.c_str(), MsgRecv.count, MsgRecv.tcount);
		}
	}
}

// 任务完成奖励 金币,经验
void CPacketParser::ParseQuestGiverQuestComplete(CPacketUsn& pakTool)
{
	MSG_S2C::stQuestGiver_Quest_Complete MsgRecv;
	pakTool>>MsgRecv;

	//任务完成的所有提示...
	ChatSystem->AppendQuestMsg(MsgRecv.questid, FinishQuest);
	SYState()->IAudio->PlayUiSound(UI_questcompleted);

	std::string strRet = _TRAN(N_NOTICE_Z27, _I2A(MsgRecv.xp).c_str(), GetMoneyRichTextStr(MsgRecv.reward_money).c_str(), _I2A(MsgRecv.reward_honor).c_str() );

	ChatMsgNotify(CHAT_MSG_SYSTEM, strRet.c_str() );//"任务奖励：\n获得经验：%d\n获得金钱：%s\n获得荣誉：%d",MsgRecv.xp, GetMoneyRichTextStr(MsgRecv.reward_money).c_str(),MsgRecv.reward_honor);

	const NiFixedString strQuestFinishedEffectName = "EA0013.nif";
	CSceneEffect* pkQuestFinishedEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect(strQuestFinishedEffectName, true, true);

	const INT LoopCount = 1;
	if (pkQuestFinishedEffect)
	{
		CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
		pkQuestFinishedEffect->AttachToSceneObject(pkLocalPlayer->GetGUID());
		pkQuestFinishedEffect->SetMaxLifeByEffectLife(LoopCount);
	}

	for(unsigned int ui = 0; ui < MsgRecv.vItems.size(); ui++)
	{
		ChatSystem->ItemGotMsg(MsgRecv.vItems[ui].item_entry, MsgRecv.vItems[ui].count);
		ItemPrototype_Client* pkIPC = ItemMgr->GetItemPropertyFromDataBase(MsgRecv.vItems[ui].item_entry);
		if(pkIPC)
		{
			std::string strRet = _TRAN(N_NOTICE_Z28, (char*)pkIPC->C_name.c_str(), _I2A(MsgRecv.vItems[ui].count).c_str());
			ChatMsgNotify(CHAT_MSG_SYSTEM, strRet.c_str() );//"任务获得%s%d个", 
				//(char*)pkIPC->C_name.c_str(), MsgRecv.vItems[ui].count);
		}
	}

	if (!UInGame::Get())
	{
		NIASSERT(0);
		return ;
	}

	UQuestDlg* pNpcQuestDlg = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
	if(pNpcQuestDlg)
	{
		pNpcQuestDlg->GetQuestList()->OnQuestFinish(MsgRecv.questid);
		//pNpcQuestDlg->SetTraceQuest(MsgRecv.questid);
	}
}

void CPacketParser::ParseaAeccpetNewQuest(CPacketUsn& pakTool)
{
	MSG_S2C::stQuset_Confirm_Accpet MsgRecv;
	pakTool>>MsgRecv;

	//提示接到了新任务。。
	ChatSystem->AppendQuestMsg(MsgRecv.questid, GetNewQuest);
}
// 任务失败
void CPacketParser::ParseQuestGiverQuestFailed(CPacketUsn& pakTool)
{
	MSG_S2C::stQuestGiver_Quest_Failed MsgRecv;
	pakTool>>MsgRecv;

	ChatSystem->AppendQuestMsg(MsgRecv.questid, FaildQuest);
	string str ;
	
	//任务失败的所有提示
	if(MsgRecv.failed_reason == FAILED_REASON_FAILED)
	{
		SYState()->IAudio->PlayUiSound(UI_questFailed);
		if (MsgRecv.baccept)
		{

			str = _TRAN("无法获取任务！");
		}else
		{
			str = _TRAN("完成任务失败！");
		}
		
	}
	else if(MsgRecv.failed_reason == FAILED_REASON_INV_FULL)
	{
		SYState()->IAudio->PlayUiSound(UI_questFailed);
		
		if (MsgRecv.baccept)
		{
			str = _TRAN("背包已满！获取任务失败！");
		}else
		{
			str = _TRAN("背包已满！无法完成任务！");
		}
		
	}
	else if(MsgRecv.failed_reason == FAILED_REASON_DUPE_ITEM_FOUND)
	{
		SYState()->IAudio->PlayUiSound(UI_questFailed);
		if (MsgRecv.baccept)
		{
			str = _TRAN("由于无法携带更多的任务物品！获取任务失败！");
		}else
		{
			str = _TRAN("由于无法携带更多的任务物品！无法完成任务！");
		}
	}
	if (str.length())
	{
		EffectMgr->AddEffectText(str.c_str());
		ClientSystemNotify(str.c_str());
	}

}

void CPacketParser::ParseQuestFull(CPacketUsn& pakTool)
{
	MSG_S2C::stQuest_Log_Full MsgRecv;
	pakTool >> MsgRecv;
	ChatMsgNotify(CHAT_MSG_SYSTEM, _TRAN("任务日志已满,无法接受新任务"));
	SYState()->IAudio->PlayUiSound(UI_questFailed);
}

// 完成任务列表
void CPacketParser::ParseFinishedQuests(CPacketUsn& pakTool)
{
	MSG_S2C::stFinishQuests MsgRecv;
	pakTool>>MsgRecv;

	UQuestDlg* pNpcQuestDlg = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
	if(pNpcQuestDlg)
	{
		pNpcQuestDlg->GetQuestList()->PareseFinishQuest(MsgRecv);
	}
}


void CPacketParser::ParseQuestEscortNotify(CPacketUsn& pakTool)
{
	MSG_S2C::stQuestEscortNotify MsgRecv;
	pakTool>>MsgRecv;

	UQuestDlg* pNpcQuestDlg = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
	if(pNpcQuestDlg)
	{
		pNpcQuestDlg->GetQuestList()->ParseQuestEscortNotify(MsgRecv.who, MsgRecv.qst_id, MsgRecv.npc_id);
	}
}

void CPacketParser::ParseObjectUpdate(CPacketUsn& pakTool)
{

	MSG_S2C::stObjectUpdate msg;
	pakTool >> msg;
	ByteBuffer& data = msg.buffer;

	uint32 pos = 0;
	while( true )
	{
		if( data.size() <= pos )
			return;

		uint16 block_length = 0;
		ui8	updatetype;

		data.rpos( pos );

		data >> block_length >> updatetype;
		pos += block_length;

		ui32*	values = NULL;
		UpdateObj* pObj = NULL;
		ui64 guid;
		if( updatetype == UPDATETYPE_CREATE_OBJECT || updatetype == UPDATETYPE_CREATE_YOURSELF )
		{
			ui8 type;ui8 update_flags;
			
			data >> guid >>  type >> update_flags;

			switch(type)
			{
			case TYPEID_DYNAMICOBJECT:
				{
					pObj = ObjectMgr->GetObject(guid);
					if (!pObj)
						pObj = ObjectMgr->CreateDynamicObj(guid);
				}
				break;
			case TYPEID_ITEM:
				{
					if( update_flags & UPDATEFLAG_HASPOSITION )
					{
						pObj = ObjectMgr->GetObject(guid);

						if( !pObj )
							pObj = ObjectMgr->CreateSceneItem(guid, false);
					}
					else
					{
						pObj = ObjectMgr->GetObject(guid);
						if( !pObj )
							pObj = ObjectMgr->CreateSYItem(guid);
					}
				}
				break;
			case TYPEID_CONTAINER:
				{
					if( update_flags & UPDATEFLAG_HASPOSITION )
					{
						pObj = ObjectMgr->GetObject(guid);

						if( !pObj )
							pObj = ObjectMgr->CreateSceneItem(guid, true);
					}
					else
					{
						pObj = ObjectMgr->GetObject(guid);
						if( !pObj )
							pObj = ObjectMgr->CreateSYBag(guid);
					}
				}
				break;
			case TYPEID_PLAYER:
				{
					//玩家
					CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
					if(pkLocalPlayer && pkLocalPlayer->GetGUID() == guid)
						return;
					
					pObj = ObjectMgr->GetObject(guid);
					if( !pObj )
					{
						BOOL bLocPlayer = update_flags &UPDATETYPE_CREATE_YOURSELF;
						pObj = ObjectMgr->CreatePlayer(guid, bLocPlayer);
					}
				}
				break;
			case TYPEID_UNIT:
				{
					//NPC
					//1. 创建NPC 对象
					pObj =  ObjectMgr->CreateCreature(guid);
				}
				break;
			case TYPEID_GAMEOBJECT:
				{
					pObj = ObjectMgr->CreateSceneGameObj(guid);
				}
				break;
			default:
				return;
			}
			NIASSERT(pObj);
			pObj->OnCreateMsg(&data, update_flags);
		}
		else if(updatetype == UPDATETYPE_VALUES)
		{
			data >> guid;
			UpdateObj* pObj = ObjectMgr->GetObject(guid);
			if( pObj )
			{
				//values = pObj->m_uint32Values;
				pObj->OnUpdateMsg(&data);
			}
		}
		else if(updatetype == UPDATETYPE_OUT_OF_RANGE_OBJECTS )
		{
			ui64 guid;
			data >> guid;
			ObjectMgr->RemoveObject(guid);
		}
		else
			assert( 0 );
	}
 }

 void CPacketParser::ParseAccountExpireInFewMinutes(CPacketUsn& pakTool)
 {
	 MSG_S2C::stAccountExpireInFewMinutes msg;
	 pakTool >> msg;

	 std::string strRet = _TRAN(N_NOTICE_Z29, _I2A(msg.howlong).c_str() );
	 ChatMsgNotify(CHAT_MSG_SYSTEM, strRet.c_str() );//"您的帐号将在 %d 分钟内到期，请尽快充值!", msg.howlong);
 }

 void CPacketParser::ParseLevelUpInfo(CPacketUsn& pakTool)
{
	MSG_S2C::stLevel_Up_Info levelupInfo;
	pakTool >> levelupInfo;

	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLocalPlayer);
	ChatSystem->LevelUpMsg(levelupInfo.level);

}

void CPacketParser::ParseSpeedChange(CPacketUsn& pakTool)
{
	MSG_S2C::stMove_Set_Speed newSpeed;
	pakTool >> newSpeed;

	UpdateObj* pkObj = ObjectMgr->GetObject(newSpeed.guid);
	if (!pkObj)
	{
		NIASSERT(pkObj);
		return;
	}

	if (newSpeed.SpeedType == RUN)
		pkObj->m_runSpeed = newSpeed.speed;
	else
	if (newSpeed.SpeedType == WALK)
		pkObj->m_walkSpeed = newSpeed.speed;
	else if ( newSpeed.SpeedType == RUNBACK )
	{
		pkObj->m_backWalkSpeed = newSpeed.speed;
	}
//	else
//		NIASSERT(0 && "未处理的速度类型变更");
}

void CPacketParser::ParseAttackUpdate(CPacketUsn& pakTool)
 {
	MSG_S2C::stAttacker_State_Update MsgRecv;pakTool>>MsgRecv;

	DamageMgr->AddDamegeData( MsgRecv.attacker_guid, 0,MsgRecv.realdamage );

	CPlayer * pPlayerLocal = ObjectMgr->GetLocalPlayer();
	CCharacter* pAtkTarget = (CCharacter*)ObjectMgr->GetObject(MsgRecv.victim_guid);

	CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(MsgRecv.attacker_guid);

	bool needSound = (  (pkChar && pkChar->IsLocalPlayer()) || (pAtkTarget && pAtkTarget->IsLocalPlayer()) );

	bool bCrit = ( MsgRecv.hitStats == HitStatus::HITSTATUS_CRICTICAL );
	if (pAtkTarget && pkChar)
	{
		wchar_t szBuffer[256]={0};
		if (pPlayerLocal->GetGUID() == MsgRecv.attacker_guid || 
			pPlayerLocal->GetGUID() == MsgRecv.victim_guid  ||
			pkChar->GetUInt64Value(UNIT_FIELD_CREATEDBY) == pPlayerLocal->GetGUID())
		{
			if (MsgRecv.extra_damage > 0)
				_snwprintf(szBuffer, 255, L"+%d", MsgRecv.extra_damage);

			int damage = MsgRecv.realdamage - MsgRecv.extra_damage;

			if(damage < 0)
				pAtkTarget->BubbleUpEffectNumber(MsgRecv.realdamage, BUBBLE_DEMAGE,MsgRecv.hitStats, NULL, MsgRecv.victim_stat);
			else
				pAtkTarget->BubbleUpEffectNumber(damage, BUBBLE_DEMAGE,MsgRecv.hitStats, szBuffer, MsgRecv.victim_stat);
		}

		CPlayerInputMgr* pkInputMsg = SYState()->LocalPlayerInput;
		if (pPlayerLocal->GetGUID() == MsgRecv.victim_guid && pPlayerLocal->GetTargetObjectID() == INVALID_OBJECTID)
		{
			pkInputMsg->SetTargetID(MsgRecv.attacker_guid, true);
		}

		if ( pAtkTarget->GetGameObjectType() == GOT_PLAYER )
		{
			CPlayer* pPlayer = (CPlayer*)pAtkTarget;
			pPlayer->OnHitState( MsgRecv.victim_stat, bCrit, needSound );
			
		}
		NiFixedString strNodeName = "Effect_Chest";

		std::string strSoundHit;
		if ( bCrit )
		{
			CSceneEffect* pEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect( "Effect_100011230.nif",true, true, false );
			pEffect->AttachToSceneObject( MsgRecv.victim_guid, &strNodeName, false, true);
			pEffect->SetEffScale( 0.7f );
			if ( pkChar )
			{
				if ( pkChar->GetGameObjectType() == GOT_PLAYER )
				{
					strSoundHit = "Sound/HitCrit1.wav";
				}

				if ( pkChar->GetGameObjectType() == GOT_CREATURE )
				{
					strSoundHit = "Sound/HitCrit.wav";
				}
			}
			
		}
		else
		{
			CSceneEffect* pEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect( "Effect_100008230.nif",true, true, false );
			pEffect->AttachToSceneObject( MsgRecv.victim_guid, &strNodeName, false, true );
			pEffect->SetEffScale( 1.0f );
			strSoundHit = "";
		}

		if ( strSoundHit.length() > 0 && needSound )
		{
			/*AudioSource* pEffectSound  = AudioSystem::GetAudioSystem()->CreateSource(AudioSource::TYPE_3D);
			if (!pEffectSound)
				return;

			pAtkTarget->GetSceneNode()->AttachChild(pEffectSound);
			pAtkTarget->GetSceneNode()->Update(0.0f, false);

			pEffectSound->SetMinMaxDistance(1.0f, 60.0f);
			pEffectSound->SetGain(1.f);
			pEffectSound->SetLoopCount( 1 );
			pEffectSound->SetSelectiveUpdate(true);
			pEffectSound->SetSelectiveUpdateTransforms(true);
			pEffectSound->SetSelectiveUpdatePropertyControllers(false);
			pEffectSound->SetSelectiveUpdateRigid(true);
			pEffectSound->Update(0.0f);

			SYState()->IAudio->PlaySound( pEffectSound, strSoundHit.c_str());*/

			ALAudioSource* pSoundEnv =  pAtkTarget->GetEnvSoundInst();
			if ( pSoundEnv )
			{
				pAtkTarget->GetSceneNode()->AttachChild(pSoundEnv);
				pSoundEnv->SetMinMaxDistance(5.0f, 60.0f);
				pSoundEnv->SetGain(1.f);

				const char* pOldname =  pSoundEnv->GetFilename();

				if ( pOldname && !strcmp( pOldname, strSoundHit.c_str()))
				{
					pSoundEnv->Play();
				}
				else
					SYState()->IAudio->PlaySound(pSoundEnv, strSoundHit.c_str());
			}
			

		}

		

	}
	if (pkChar)
	{
		pkChar->AttackUpdate(MsgRecv.victim_guid);
	}
	ChatSystem->DamageMsg(MsgRecv.victim_guid, MsgRecv.attacker_guid, MsgRecv.realdamage, MsgRecv.victim_stat, MsgRecv.hitStats,
		MsgRecv.damage_abs, MsgRecv.resisted_damage, MsgRecv.blocked_damage);

	// UTRACE("########### ATTACK DAMAGE %d ", MsgRecv.realdamage);
 } 
 
void CPacketParser::ParseAttackFail_NotInRange(CPacketUsn& pakTool)
{
	//	停止这轮攻击动作的播放,下一轮timer中继续从头播放
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	if( !pkPlayer )
		return;


	CPlayerInputMgr* pkInputMsg = SYState()->LocalPlayerInput;
	pkInputMsg->SetSwingTargetID(pkPlayer->GetTargetObjectID());


	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal && pkLocal->IsHook())
	{
		HookMgr->HookUseSpellFailed(SPELL_FAILED_OUT_OF_RANGE);
	}

	//// HACK.
	//if (pkPlayer->GetCurActionState() == STATE_NORMAL_ATTACK)
	//{
	//	pkPlayer->StopMove();
	//	pkPlayer->SetNextState(STATE_IDLE, CUR_TIME);
	//	PacketBuilder->SendAttackStopMsg();
	//}
}

void CPacketParser::ParseAttackFail_BadFacing(CPacketUsn& pakTool)
{
	//	停止这轮攻击动作的播放,下一轮timer中继续从头播放

	EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("您面对了错误的方向"));
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal && pkLocal->IsHook())
	{
		HookMgr->HookUseSpellFailed(SPELL_FAILED_UNIT_NOT_INFRONT);
	}
}

void CPacketParser::ParseAttackStart(CPacketUsn& pakTool)
{
	MSG_S2C::stAttack_Start MsgRecv;pakTool>>MsgRecv;

	CCharacter* pAttacker = (CCharacter*)ObjectMgr->GetObject(MsgRecv.attacker_guid);
	if( !pAttacker  || pAttacker->GetHP() <= 0)
		return;

	if (pAttacker->GetMoveState() == CMS_WALK && pAttacker->GetCurPostureState() == STATE_MOVE)
	{
		pAttacker->StopMove();
		pAttacker->SetWorldTransform(NiPoint3(MsgRecv.x, MsgRecv.y, MsgRecv.z), 
			NiPoint3(0, 0, MsgRecv.orientation));
	}

	pAttacker->BeginAttack(MsgRecv.victim_guid);
}

void CPacketParser::ParseAttackStop(CPacketUsn& pakTool)
{
	MSG_S2C::stAttack_Stop MsgRecv;pakTool>>MsgRecv;

	CCharacter* pAttacker = (CCharacter*)ObjectMgr->GetObject(MsgRecv.attacker_guid);
	if( !pAttacker )
		return;

	pAttacker->EndAttack();
}

void CPacketParser::ParseKillLog(CPacketUsn& pakTool)
{
	MSG_S2C::stParty_Kill_Log MsgRecv;
	pakTool>>MsgRecv;

	CPlayer* player = ObjectMgr->GetLocalPlayer();
	if (player->GetGUID() == MsgRecv.playerguid)
	{
		HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_PICK);
	}

	ChatSystem->DeathMsg(MsgRecv.unitguid, MsgRecv.playerguid);

}

// HP治疗
void CPacketParser::ParseHealHP(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Heal_On_Player MsgRecv;pakTool>>MsgRecv;

	DamageMgr->AddHealData( MsgRecv.caster_guid, MsgRecv.spell_id, MsgRecv.dmg );
	CCharacter* pAtacker = (CCharacter*)ObjectMgr->GetObject(MsgRecv.caster_guid);
	CCharacter* pAtkTarget = (CCharacter*)ObjectMgr->GetObject(MsgRecv.target_guid);
	if (pAtkTarget == NULL)
	{
		return;
	}
	if (pAtacker == ObjectMgr->GetLocalPlayer() || pAtkTarget == ObjectMgr->GetLocalPlayer())
		pAtkTarget->BubbleUpEffectNumber(MsgRecv.dmg, BUBBLE_HEALHP);
	SpellTemplate* pkSpell = SYState()->SkillMgr->GetSpellTemplate(MsgRecv.spell_id);
	if (!pkSpell) return;

	SYState()->SkillMgr->HealHP(MsgRecv.caster_guid, MsgRecv.target_guid, MsgRecv.spell_id);
	ChatSystem->SpellHeathMsg(MsgRecv.target_guid, MsgRecv.caster_guid, MsgRecv.spell_id, MsgRecv.dmg);

}
// MP治疗
void CPacketParser::ParseHeadMP(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_HealMana_On_Player MsgRecv;pakTool>>MsgRecv;
	CCharacter* pAtacker = (CCharacter*)ObjectMgr->GetObject(MsgRecv.caster_guid);
	CCharacter* pAtkTarget = (CCharacter*)ObjectMgr->GetObject(MsgRecv.target_guid);
	if (pAtkTarget == NULL)
	{
		return;
	}

	if (pAtacker == ObjectMgr->GetLocalPlayer() || pAtkTarget == ObjectMgr->GetLocalPlayer())
		pAtkTarget->BubbleUpEffectNumber(MsgRecv.dmg, BUBBLE_HEALMP);

	SpellTemplate* pkSpell = SYState()->SkillMgr->GetSpellTemplate(MsgRecv.spell_id);
	if (!pkSpell) return;

	SYState()->SkillMgr->HealMP(MsgRecv.caster_guid, MsgRecv.target_guid, MsgRecv.spell_id);
	ChatSystem->SpellManaMsg(MsgRecv.target_guid, MsgRecv.caster_guid, MsgRecv.spell_id, MsgRecv.dmg);
}

// 法术伤害
void CPacketParser::ParseNoMeleeDamage(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_NoMelee_Damage_Log MsgRecv;pakTool>>MsgRecv;

	CCharacter* pAtacker = (CCharacter*)ObjectMgr->GetObject(MsgRecv.caster_guid);
	CCharacter* pAtkTarget = (CCharacter*)ObjectMgr->GetObject(MsgRecv.target_guid);

	DamageMgr->AddDamegeData( MsgRecv.caster_guid, MsgRecv.spellid, MsgRecv.Damage );

	if (!pAtkTarget || !pAtacker)
		return;

	const ui64& createbyguid = pAtacker->GetUInt64Value(OBJECT_FIELD_CREATED_BY);
	if (createbyguid == ObjectMgr->GetLocalPlayer()->GetGUID() || pAtacker == ObjectMgr->GetLocalPlayer() || pAtkTarget == ObjectMgr->GetLocalPlayer())
	{
		if(MsgRecv.Damage != 0)
		{
			pAtkTarget->BubbleUpEffectNumber(MsgRecv.Damage, BUBBLE_SPELLDEMAGE,  MsgRecv.bCri * (SPELL_LOG_REFLECT + 1));
		}else if (MsgRecv.AbsorbedDamage != 0)
		{
			pAtkTarget->BubbleUpEffectNumber(0, BUBBLE_SPELLDEMAGE, SPELL_LOG_ABSORB);
		}else if (MsgRecv.ResistedDamage != 0)
		{
			pAtkTarget->BubbleUpEffectNumber(0, BUBBLE_SPELLDEMAGE, SPELL_LOG_RESIST);
		}else if (MsgRecv.PhysicalDamage != 0)
		{
			pAtkTarget->BubbleUpEffectNumber(MsgRecv.PhysicalDamage, BUBBLE_DEMAGE);
		}else if (MsgRecv.BlockedDamage != 0)
		{
			pAtkTarget->BubbleUpEffectNumber(0, BUBBLE_SPELLDEMAGE, SPELL_LOG_BLOCK);
		}
	}
	SpellID eSkilID =  (SpellID)MsgRecv.spellid;
	//SpellTemplate* pSkill = SYState()->SkillMgr->GetSpellTemplate(eSkilID);
	//if (pSkill == NULL)
	//{
	//	return;
	//}

	SYState()->SkillMgr->Damage(MsgRecv.caster_guid, MsgRecv.target_guid, MsgRecv.spellid);
	ChatSystem->SpellDmgMsg(createbyguid, MsgRecv.target_guid, MsgRecv.caster_guid, MsgRecv.spellid, MsgRecv.Damage, 
		MsgRecv.AbsorbedDamage, MsgRecv.ResistedDamage, MsgRecv.PhysicalDamage, MsgRecv.BlockedDamage, MsgRecv.bCri);
}

// debuf 伤害
void CPacketParser::ParseAuraPeriodicDamage(CPacketUsn& pakTool)
{
	MSG_S2C::stAura_Periodic_Log MsgRecv;pakTool>>MsgRecv;


	CCharacter* pAtacker = (CCharacter*)ObjectMgr->GetObject(MsgRecv.caster_guid);
	CCharacter* pAtkTarget = (CCharacter*)ObjectMgr->GetObject(MsgRecv.target_guid);
	if (pAtkTarget == NULL)
	{
		return;
	}
	SpellID eSkilID =  (SpellID)MsgRecv.spell_entry;
	SpellTemplate* pSkill = SYState()->SkillMgr->GetSpellTemplate(eSkilID);
	if (pSkill == NULL)
	{
		return;
	}

	SYState()->SkillMgr->PeriodicDamage(MsgRecv.caster_guid, MsgRecv.target_guid, MsgRecv.spell_entry);
	if (pAtkTarget == ObjectMgr->GetLocalPlayer() || pAtacker == ObjectMgr->GetLocalPlayer())
	{
		if(MsgRecv.flag & FLAG_PERIODIC_DAMAGE)
		{
			DamageMgr->AddDamegeData( MsgRecv.caster_guid, MsgRecv.spell_entry, MsgRecv.amount );
			if(MsgRecv.amount != 0)
				pAtkTarget->BubbleUpEffectNumber(MsgRecv.amount, BUBBLE_SPELLDEMAGE);
			else if (MsgRecv.abs_dmg != 0)
				pAtkTarget->BubbleUpEffectNumber(MsgRecv.amount, BUBBLE_SPELLDEMAGE, SPELL_LOG_ABSORB);
			else if (MsgRecv.resisted_damage != 0)
				pAtkTarget->BubbleUpEffectNumber(MsgRecv.amount, BUBBLE_SPELLDEMAGE, SPELL_LOG_RESIST);

			ChatSystem->AuraDmgMsg(MsgRecv.target_guid, MsgRecv.caster_guid, MsgRecv.spell_entry, MsgRecv.amount, MsgRecv.abs_dmg, MsgRecv.resisted_damage);
		}
		if(MsgRecv.flag & FLAG_PERIODIC_HEAL)
		{
			DamageMgr->AddHealData( MsgRecv.caster_guid, MsgRecv.spell_entry, MsgRecv.amount );
			pAtkTarget->BubbleUpEffectNumber(MsgRecv.amount, BUBBLE_HEALHP);
			ChatSystem->AuraHealMsg(MsgRecv.target_guid, MsgRecv.caster_guid, MsgRecv.spell_entry, MsgRecv.amount, MsgRecv.abs_dmg, MsgRecv.resisted_damage);
		}
		if(MsgRecv.flag & FLAG_PERIODIC_ENERGIZE)
		{
			pAtkTarget->BubbleUpEffectNumber(MsgRecv.amount, BUBBLE_HEALMP);
			ChatSystem->AuraHealMpMsg(MsgRecv.target_guid, MsgRecv.caster_guid, MsgRecv.spell_entry, MsgRecv.amount, MsgRecv.abs_dmg, MsgRecv.resisted_damage);
		}
	}
}

// 环境伤害
void CPacketParser::ParseEnvironmentalDamage(CPacketUsn& pakTool)
{
	MSG_S2C::stDamage_Environmental_Log MsgRecv;pakTool>>MsgRecv;
	SYState()->SkillMgr->EnvironmentalDamage(MsgRecv.target_guid, MsgRecv.type);
}

// 技能miss
void CPacketParser::ParseSpellMiss(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Log_Miss MsgRecv;pakTool>>MsgRecv;

	CCharacter* pAtacker = (CCharacter*)ObjectMgr->GetObject(MsgRecv.caster_guid);
	CCharacter* pAtkTarget = (CCharacter*)ObjectMgr->GetObject(MsgRecv.target_guid);
	if (pAtkTarget == NULL)
	{
		return;
	}

	if (pAtkTarget == ObjectMgr->GetLocalPlayer() || pAtacker == ObjectMgr->GetLocalPlayer())
	{
		pAtkTarget->BubbleUpEffectNumber(0, BUBBLE_SPELLDEMAGE,MsgRecv.SpellLogType);

		ChatSystem->SpellMissMsg(MsgRecv.target_guid, MsgRecv.caster_guid, MsgRecv.spellid, MsgRecv.SpellLogType);
	}

}
void CPacketParser::ParseCCResult(CPacketUsn& pakTool)
{
	MSG_S2C::stCCResult MsgRecv;
	pakTool>>MsgRecv;
	//enum { _STUN, _ROOT, _FEAR, _SLOW, _DAMAGE };
	//enum { _IMMUNE, _RESIST };

	CCharacter* pAtacker = (CCharacter*)ObjectMgr->GetObject(MsgRecv.casterid);
	CCharacter* pAtkTarget = (CCharacter*)ObjectMgr->GetObject(MsgRecv.victimid);
	if (pAtkTarget == NULL)
		return;

	if (pAtkTarget == ObjectMgr->GetLocalPlayer() || pAtacker == ObjectMgr->GetLocalPlayer())
	{
		uint32 SpellLogType = SPELL_LOG_NONE;
		if(MsgRecv.result == MSG_S2C::stCCResult::_IMMUNE)
			SpellLogType = SPELL_LOG_IMMUNE;
		else if(MsgRecv.result == MSG_S2C::stCCResult::_RESIST)
			SpellLogType = SPELL_LOG_RESIST;

		pAtkTarget->BubbleUpEffectNumber(0, BUBBLE_SPELLDEMAGE, SpellLogType);

		ChatSystem->SpellCCResult(MsgRecv.victimid, MsgRecv.casterid, MsgRecv.spellid, MsgRecv.effect, MsgRecv.result);
	}
}

void CPacketParser::ParseSpellStart(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Start MsgRecv;pakTool>>MsgRecv;
	SYObjID AttckerID = MsgRecv.caster_unit_guid;
	SpellID spellid =  (SpellID)MsgRecv.spell_id;
//	SpellCastTargets targets;

	if( GET_TYPE_FROM_GUID(AttckerID) == HIGHGUID_TYPE_GAMEOBJECT )
	{
		return;
	}

	if (AttckerID == SY_INVALID_OBJID)
	{
		NILOG("SpellGo() invalid attacker id.");
		return;
	}

	CCharacter* pAttcker = (CCharacter*)ObjectMgr->GetObject(AttckerID);
	if (pAttcker == NULL)
	{
		NILOG("SpellGo() invalid attacker.");
		return;
	}

	NiPoint3 kSrcPos(MsgRecv.targets.m_srcX, MsgRecv.targets.m_srcY, MsgRecv.targets.m_srcZ);
	NiPoint3 kDestPos(MsgRecv.targets.m_destX, MsgRecv.targets.m_destY, MsgRecv.targets.m_destZ);
	pAttcker->ClearTargetList();
	if (MsgRecv.targets.m_targetMask & TARGET_FLAG_SOURCE_LOCATION)
	{
		pAttcker->AddTarget(kSrcPos, 3.0f);
	}

	if (MsgRecv.targets.m_targetMask & TARGET_FLAG_DEST_LOCATION)
	{
		pAttcker->AddTarget(kDestPos, 3.0f);
	}

	ui32 uiCastTime = MsgRecv.casttime;
	pAttcker->SpellCast(spellid, uiCastTime, MsgRecv.targets);
	//SpellTemplate* pkST = SYState()->SkillMgr->GetSpellTemplate(spellid);
	//SYState()->SkillMgr->AddGlobalCooldown(pkST->GetCategoryRecoverTime());
}
void CPacketParser::ParseSpellClearCD(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Clear_CD MsgRecv;pakTool>>MsgRecv;
	//
	if (SYState()->SkillMgr)
	{
		if (MsgRecv.spell_id)
		{
			SYState()->SkillMgr->CooldownCancel(MsgRecv.spell_id);
		}else
		{
			SYState()->SkillMgr->ClearCooldown();
		}
	}

}
void CPacketParser::ParseSpellGo(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Go MsgRecv;pakTool>>MsgRecv;
	SYObjID AttackerID = MsgRecv.caster_unit_guid;
	
	//if( GET_TYPE_FROM_GUID(AttckerID) == HIGHGUID_TYPE_GAMEOBJECT )
	//{
	//	return;
	//}
	if (AttackerID == SY_INVALID_OBJID)
	{
		NILOG("SpellGo() invalid attacker id.");
		return;
	}

	CGameObject* pkGameObject = (CGameObject*)ObjectMgr->GetObject(AttackerID);
	if ( !pkGameObject)
	{
		NILOG("SpellGo() invalid attacker.");
		return;
	}

	SpellID Spellid =  (SpellID)MsgRecv.spell_id;
	SpellTemplate* Template = SYState()->SkillMgr->GetSpellTemplate(Spellid);
	if (!Template)
		return;


	CCharacter* pAttcker = (CCharacter*)ObjectMgr->GetObject(AttackerID);
	if (pAttcker == NULL)
	{
		NILOG("SpellGo() invalid attacker.");
		return;
	}
	CPlayer* pkPlayer = pAttcker->GetAsPlayer();
	if (pkPlayer && pkPlayer->IsLocalPlayer())
	{
		SpellTemplate* pkTemplate = SYState()->SkillMgr->GetSpellTemplate(Spellid);
		if (pkTemplate ==  NULL) return;

		pkTemplate->AddCooldown();
		pkTemplate->AddStartCooldown();
		
		if (((CPlayerLocal*)pkPlayer)->IsHook())
		{
			char buf[_MAX_PATH];
			sprintf(buf,"服务器：技能：%d 使用成功\n\n\n\n\n", Spellid);
			OutputDebugString(buf);
			HookMgr->StopUseSpellState(Spellid);
		}


		if(Template->GetCategory() == SPELL_CATEGORY_PROFESSION_MANUFACTURE)
		{
			UIManufacture* pManuFacture = INGAMEGETFRAME(UIManufacture, FRAME_IG_MANUFACTURE);
			if (pManuFacture && pManuFacture->GetLockSpellEntry() == Spellid)
			{
				pManuFacture->UnLockBuild();
			}
			USpecialityDlg* pSpeciality = INGAMEGETFRAME(USpecialityDlg, FRAME_IG_SPECIALITYDLG);
			if (pSpeciality && pSpeciality->GetLockSpellEntry() == Spellid)
			{
				pSpeciality->UnLockBuild();
			}
		}
		//pkTemplate->AddStartCooldown();
	}

	if (pkGameObject->GetAsSceneObj() != 0)
	{
		CSceneGameObj* pkSceneObj = (CSceneGameObj*)pkGameObject->GetAsSceneObj();
		assert(pkSceneObj);
		if(pkSceneObj)
			pkSceneObj->SpellGo(Spellid, MsgRecv.vTargetHit, MsgRecv.targets);
	}
	else
	{
		// check attacker valid?
		CCharacter* pAttcker = (CCharacter*)ObjectMgr->GetObject(AttackerID);
		assert(pAttcker);
		if(pAttcker)
			pAttcker->SpellGo(Spellid, MsgRecv.vTargetHit, MsgRecv.targets);
	}
}

void CPacketParser::ParseSpellCastResult(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Cast_Result MsgRecv;pakTool>>MsgRecv;
	CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
	if( !pkPlayer )
		return;
	if( MsgRecv.ErrorMessage != SPELL_CANCAST_OK )
	{
		const char* pcError = SpellTemplate::GetSpellErrorString(MsgRecv.ErrorMessage);
		EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN(pcError));

		SYState()->SkillMgr->AddGlobalCooldown(0);

		pkPlayer->SpellFailure(MsgRecv.SpellId, MsgRecv.ErrorMessage);

		if (pkPlayer->IsHook())
		{
				char buf[_MAX_PATH];
				sprintf(buf,"服务器技能：%d 吟唱失败\n", MsgRecv.SpellId);
				OutputDebugString(buf);
				HookMgr->StopUseSpellState(MsgRecv.SpellId, MsgRecv.ErrorMessage);
			
		}	
	}
}

void CPacketParser::ParseSpellFailure(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Failure MsgRecv;pakTool>>MsgRecv;
	SYObjID AttckerID = MsgRecv.caster_guid;
	if (AttckerID == SY_INVALID_OBJID)
	{
		NILOG("SpellGo() invalid attacker id.");
		return;
	}
	CCharacter* pAttcker = (CCharacter*)ObjectMgr->GetObject(AttckerID);
	if (pAttcker == NULL)
	{
		return;
	}

	if (pAttcker == ObjectMgr->GetLocalPlayer())
	{
		const char* pcError = SpellTemplate::GetSpellErrorString(MsgRecv.error);
		EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN(pcError));
		//SYState()->SkillMgr->AddGlobalCooldown(0);

		if (ObjectMgr->GetLocalPlayer()->IsHook())
		{
			
			char buf[_MAX_PATH];
			sprintf(buf,"服务器：技能：%d 使用 失败\n", MsgRecv.spell_id);
			OutputDebugString(buf);
			HookMgr->StopUseSpellState(MsgRecv.spell_id, MsgRecv.error);
				
		}
	}

	pAttcker->SpellFailure(MsgRecv.spell_id, MsgRecv.error);
	//精炼
	if (MsgRecv.spell_id == JINGLIAN_SPELL_ID && pAttcker == ObjectMgr->GetLocalPlayer())
	{
		EquRefineMgr->ParseJingLianResult(JINGLIAN_SPELL_USE_FAILD);
	}
}

void CPacketParser::ParseChannelSpellStart(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Channel_Start MsgRecv;pakTool>>MsgRecv;
	SYObjID AttackerID = MsgRecv.caster_guid;

	if (AttackerID == SY_INVALID_OBJID)
	{
		NILOG("SpellGo() invalid attacker id.");
		return;
	}

	CGameObject* pkGameObject = (CGameObject*)ObjectMgr->GetObject(AttackerID);
	if ( !pkGameObject)
	{
		NILOG("SpellGo() invalid attacker.");
		return;
	}

	SpellID Spellid =  (SpellID)MsgRecv.SpellId;

	SpellTemplate* Template = SYState()->SkillMgr->GetSpellTemplate(Spellid);
	if (!Template)
		return;

	CCharacter* pAttcker = (CCharacter*)ObjectMgr->GetObject(AttackerID);
	if (pAttcker == NULL)
	{
		NILOG("SpellGo() invalid attacker.");
		return;
	}

	CPlayer* pkPlayer = pAttcker->GetAsPlayer();

	if (pkPlayer && pkPlayer->IsLocalPlayer())
	{
		SpellTemplate* pkTemplate = SYState()->SkillMgr->GetSpellTemplate(Spellid);
		if (pkTemplate ==  NULL) return;

		pkTemplate->AddCooldown();
		pkTemplate->AddStartCooldown();
	}

	if (pkGameObject->GetAsSceneObj() != 0)
	{
		CSceneGameObj* pkSceneObj = (CSceneGameObj*)pkGameObject->GetAsSceneObj();
		assert(pkSceneObj);
		//if(pkSceneObj)
			//pkSceneObj->SpellGo(Spellid, MsgRecv.vTargetHit, MsgRecv.targets);
	}
	else
	{
		// check attacker valid?
		CCharacter* pAttcker = (CCharacter*)ObjectMgr->GetObject(AttackerID);
		assert(pAttcker);
		if(pAttcker)
			pAttcker->SpellTunnel( Spellid, MsgRecv.duration );
	}
}

void CPacketParser::ParseChannelSpellUpdate(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Channel_Update MsgRecv;pakTool>>MsgRecv;
	SYObjID AttackerID = MsgRecv.caster_guid;

	CCharacter* pAttcker = (CCharacter*)ObjectMgr->GetObject(AttackerID);
	if (pAttcker == NULL)
	{
		NILOG("SpellGo() invalid attacker.");
		return;
	}

	CPlayer* pkPlayer = pAttcker->GetAsPlayer();

	if (pkPlayer && pkPlayer->IsLocalPlayer())
	{
		/*SpellTemplate* pkTemplate = SYState()->SkillMgr->GetSpellTemplate(Spellid);
		if (pkTemplate ==  NULL) return;

		pkTemplate->AddCooldown();
		pkTemplate->AddStartCooldown();*/
	}



	if( MsgRecv.time == 0 )
	{// 引导结束
		pkPlayer->EndTunnel();
	}
	else
	{// 引导加时延迟
		pkPlayer->TunnelDelay( MsgRecv.time );
	}
}

void CPacketParser::ParseSpellCastSuccess(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Target_Cast_Result MsgRecv;pakTool>>MsgRecv;
}

void CPacketParser::ParseActionButtons(CPacketUsn& pakTool)
{
	MSG_S2C::stAction_Buttons MsgRecv;pakTool>>MsgRecv;

	for (int i = 0; i < 132; i++)
	{
		SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_INGAME_SKILL, i ,MsgRecv.vActionButtons[i].Action,1,(UItemSystem::ItemType)MsgRecv.vActionButtons[i].Type);
	}	
}

void CPacketParser::ParseUpdateActionButtons(CPacketUsn& pakTool)
{
	MSG_S2C::stUpdateActionButtons MsgRecv; pakTool >> MsgRecv;
	for (size_t i = 0; i < MsgRecv.v.size(); ++i)
	{
		MSG_S2C::stUpdateActionButtons::stActionButton& ab( MsgRecv.v[i] );

		SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_INGAME_SKILL, ab.pos, ab.Action, 1, (UItemSystem::ItemType)ab.Type);
	}
}

void CPacketParser::ParseSpellDelay(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Delayed MsgREcv;pakTool>>MsgREcv;

	CCharacter* pAttcker = (CCharacter*)ObjectMgr->GetObject(MsgREcv.caster_guid);
	if (pAttcker == NULL)
	{
		NILOG("SpellGo() invalid attacker.");
		return;
	}

	pAttcker->SpellDelay(MsgREcv.delay);
}

void CPacketParser::ParseSpellInit(CPacketUsn& pakTool)
{
	MSG_S2C::stSpells_Initial MsgRecv;pakTool>>MsgRecv;
	SYState()->SkillMgr->ClearCooldown();


	uint16 ui = 0;
	for (ui=0; ui<MsgRecv.itemCount; ui++)
	{
		MSG_S2C::stSpells_Initial::stCoolDown& cd =  MsgRecv.vSpellCDs[ui];
		unsigned int pSpellGroup = 0;
		if (SYState()->SkillMgr->GetSpellTemplate(cd.spellid))
		{
			pSpellGroup = SYState()->SkillMgr->GetSpellTemplate(cd.spellid)->GetSpellGroup();
		}
		if (cd.spell_category_cd && MsgRecv.vSpellCDs[ui].spell_category && pSpellGroup > 0)
		{
			SYState()->SkillMgr->AddGroupCooldown(COOLDOWN_TYPE_CATEGORY,cd.spell_category_cd,pSpellGroup);
		}else
		{
			SYState()->SkillMgr->AddCooldown(COOLDOWN_TYPE_SPELL,cd.spellid,cd.spell_cd,pSpellGroup,0);
		}
	}

	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	if (pLocal)
	{
		pLocal->PushSkillList(MsgRecv.vSpells);
	}
	std::vector<ui32> NorSpells;
	std::vector<ui32> ManuSpells;
	for (unsigned int ui = 0 ; ui < MsgRecv.vSpells.size() ; ui++)
	{
		SpellTemplate* pkSpellTem = SYState()->SkillMgr->GetSpellTemplate(MsgRecv.vSpells[ui]);
		if (pkSpellTem && (pkSpellTem->GetCategory() == SPELL_CATEGORY_NORMAL || pkSpellTem->GetCategory() == SPELL_CATEGORY_GENDER || pkSpellTem->GetCategory() == SPELL_CATEGORY_RACE
			||pkSpellTem->GetCategory() == SPELL_CATEGORY_GUILD))
		{
			NorSpells.push_back(MsgRecv.vSpells[ui]);
		}else if (pkSpellTem && pkSpellTem->GetCategory() == SPELL_CATEGORY_PROFESSION_MANUFACTURE && pkSpellTem->GetLineNameEntry() == 0 && pkSpellTem->GetSubLineNameEntry() == 0)
		{
			ManuSpells.push_back(MsgRecv.vSpells[ui]);
		}
	}

	UISkill * pUSkillDlg = INGAMEGETFRAME(UISkill,FRAME_IG_SKILLDLG);
	if (pUSkillDlg)
	{
		pUSkillDlg->OnInitSkillList(NorSpells);
	}
	UIManufacture* pManuFacture = INGAMEGETFRAME(UIManufacture, FRAME_IG_MANUFACTURE);
	if (pManuFacture)
	{
		pManuFacture->SetSkillList(ManuSpells);
	}
	USkillTrainingDlg* pSkillTraining = INGAMEGETFRAME(USkillTrainingDlg, FRAME_IG_SPELLTRAININGDLG);
	if (pSkillTraining)
	{
		pSkillTraining->UpdateSpellList();
	}
	USpecialityDlg* pSpecialityDlg = INGAMEGETFRAME(USpecialityDlg, FRAME_IG_SPECIALITYDLG);
	if (pSpecialityDlg)
	{
		pSpecialityDlg->UpdateSpellList();
	}
}

void CPacketParser::ParseSpellLearn(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Learned MsgRecv;pakTool>>MsgRecv;
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	if (pLocal)
	{
		pLocal->PushSkill(MsgRecv.spell_id);
	}

	UISkill * pUSkillDlg = INGAMEGETFRAME(UISkill,FRAME_IG_SKILLDLG);
	UIManufacture* pManuFacture = INGAMEGETFRAME(UIManufacture, FRAME_IG_MANUFACTURE);
	USkillTrainingDlg* pSkillTraining = INGAMEGETFRAME(USkillTrainingDlg, FRAME_IG_SPELLTRAININGDLG);
	if (pUSkillDlg && pManuFacture && pSkillTraining && MsgRecv.spell_id)
	{
		SpellTemplate* pkSpellTem = SYState()->SkillMgr->GetSpellTemplate(MsgRecv.spell_id);
		if (pkSpellTem && pkSpellTem->GetCategory() == SPELL_CATEGORY_NORMAL || pkSpellTem->GetCategory() == SPELL_CATEGORY_GENDER || pkSpellTem->GetCategory() == SPELL_CATEGORY_RACE
			||pkSpellTem->GetCategory() == SPELL_CATEGORY_GUILD)
		{
			pUSkillDlg->OnUpdateSkillList(MsgRecv.spell_id);

			if (pLocal)
			{
				pLocal->AddLearnNewSkillEffect();
			}

			//添加
			UInGameBar* pBar = INGAMEGETFRAME(UInGameBar, FRAME_IG_ACTIONSKILLBAR);
			if (pBar)
			{
				pBar->GlintInGameBtn(UINGAMEBAR_BUTTONSKILL,TRUE);
			}

			if ( MsgRecv.disp_new_learn )
			{
				ULearnSkillBar *pSkillLearn = INGAMEGETFRAME(ULearnSkillBar,FRAME_IG_NEWLEARN);
				pSkillLearn->AddNewLearnSkill( MsgRecv.spell_id );
			}

		}else if (pkSpellTem && pkSpellTem->GetCategory() == SPELL_CATEGORY_PROFESSION_MANUFACTURE && pkSpellTem->GetLineNameEntry() == 0 && pkSpellTem->GetSubLineNameEntry() == 0)
		{
			pManuFacture->AddNewSkill(MsgRecv.spell_id);
			UInGameBar* pBar = INGAMEGETFRAME(UInGameBar, FRAME_IG_ACTIONSKILLBAR);
			if (pBar)
			{
				pBar->GlintInGameBtn(UINGAMEBAR_BUTTONSHOP,TRUE);
			}
			
			if ( pLocal )
			{
				pLocal->AddLearnNewSkillEffect();
			}
			
		}
		pSkillTraining->UpdateSpellList();
		
	}
}
void CPacketParser::ParseSpellRemove(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Removed MsgRecv;
	pakTool>>MsgRecv;

	//从技能面板移除技能
	UISkill* pkSkillDlg = INGAMEGETFRAME(UISkill, FRAME_IG_SKILLDLG);
	if (pkSkillDlg)
	{
		if (!pkSkillDlg->OnDelSkillByID(MsgRecv.spell_id))
		{
			ULOG("没找找到技能");
		}
	}
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	if (pLocal)
	{
		pLocal->PopSkill(MsgRecv.spell_id);
	}
	//从快捷栏移除技能
}

void CPacketParser::ParseSpellNPCTrainerList(CPacketUsn& pakTool)
{
	MSG_S2C::stNPC_Trainer_List MsgRecv;
	pakTool>>MsgRecv;

	USkillTrainingDlg* pDlg = INGAMEGETFRAME(USkillTrainingDlg, FRAME_IG_SPELLTRAININGDLG);
	if (pDlg)
	{
		pDlg->ParseTrainingAck(MsgRecv.guid, MsgRecv.TrainerType, MsgRecv.Id);
	}

}

void CPacketParser::ParseSpellSetFlatModifier(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Set_Flat_Modifier MsgRecv;
	pakTool>>MsgRecv;

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		pkLocal->UpdataSpellModifier(MsgRecv.v, MsgRecv.group, MsgRecv.type, MsgRecv.is_pct);
	}
}

///////////////////////////////////////////////////////////////////////
//  组队相关
///////////////////////////////////////////////////////////////////////
//对方的邀请
void CPacketParser::ParseGroupInviteMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupInvite MsgRecv; 
	pakTool>>MsgRecv;
	UMessageBox::MsgBox(_TRAN(N_NOTICE_C79, MsgRecv.name.c_str()).c_str(),
		(BoxFunction*)Teamsys::SendAcceptGroupInvite,
		(BoxFunction*)Teamsys::SendRefuseGroupInvite, 20.0f);
	SYState()->IAudio->PlayUiSound("Sound/UI/teamwork.wav", NiPoint3::ZERO, 1.0f, 1 );
}
//拒绝邀请
void CPacketParser::ParseGroupRefuseMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupDecline MsgRecv;
	pakTool>>MsgRecv;
	string str = _TRAN(N_NOTICE_C80, MsgRecv.name.c_str());
	ClientSystemNotify(str.c_str());
}
//被踢掉
void CPacketParser::ParseGroupUnInviteMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupUninvite MsgRecv;
	pakTool>>MsgRecv;
	ClientSystemNotify(_TRAN("您已经被移出队伍!"));
	SYState()->IAudio->PlayUiSound("Sound/UI/outteam.wav", NiPoint3::ZERO, 1.0f, 1 );
}
//队长更改
void CPacketParser::ParseGroupLeaderChanged(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupSetLeader MsgRecv;
	pakTool>>MsgRecv;
	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->LeaderChange(MsgRecv.name.c_str());
	}
}
void CPacketParser::ParseGroupMemberIcon(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupSetPlayerIcon MsgRecv;
	pakTool>>MsgRecv;

	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->MemberIconChange(MsgRecv);
	}

}
//添加成员
void CPacketParser::ParseAddMember(CPacketUsn & pakTool)
{
	MSG_S2C::stGroupAddMember MsgRecv;
	pakTool>>MsgRecv;
	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->AddMember(MsgRecv.NewMember, MsgRecv.SubTeam);
		SYState()->IAudio->PlaySound("Sound/UI/teamwork.wav", NiPoint3::ZERO, 1.0f, 1 );
	}
}
//踢出成员
void CPacketParser::ParseRemoveMember(CPacketUsn & pakTool)
{
	MSG_S2C::stGroupRemoveMember MsgRecv;
	pakTool>>MsgRecv;
	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->RemoveMember(MsgRecv.guid);
	}
	
}
void CPacketParser::ParseGroupMemberReady(CPacketUsn& pakTool)
{
	MSG_S2C::stRaid_ReadyCheck MsgRecv;
	pakTool>>MsgRecv;

	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->MemberReadyChange(MsgRecv.player_guid, MsgRecv.ready);
		//SYState()->IAudio->PlaySound("Sound/UI/teamwork.wav", NiPoint3::ZERO, 1.0f, 1 );
	}
}
//团队内成员位置互换
void CPacketParser::ParseGroupMemberSwep(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupSwep MsgRecv;
	pakTool>>MsgRecv; 

	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->MemberTeamSwep(MsgRecv.guiddes, MsgRecv.guidsrc);
	}
}
//团队内成员位置更改
void CPacketParser::ParseMemberSubGroupChange(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupChangeSubgroup MsgRecv;
	pakTool>>MsgRecv; 

	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->MemberTeamChange(MsgRecv.guid, MsgRecv.team);
	}
}
void CPacketParser::ParseGroupList(CPacketUsn & pakTool)
{
	MSG_S2C::stGroupList MsgRecv;
	pakTool>>MsgRecv;

	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->AddGroupList(MsgRecv.vSubGroups, MsgRecv.leaderguid, MsgRecv.subgroupcount);
		SYState()->IAudio->PlaySound("Sound/UI/teamwork.wav", NiPoint3::ZERO, 1.0f, 1 );
	}
	//组队拾取设定
	if (TEAMPICKITEMSET)
	{
		TEAMPICKITEMSET->SetTeamPick(MsgRecv.lootmethod, (ITEM_QUALITY)MsgRecv.lootthreshold);
	}
}
//队伍解散
void CPacketParser::ParseGroupDestory(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupDestroyed MsgRecv;
	pakTool>>MsgRecv;
	/*if (TeamShow)
	{
		TeamShow->SetTeamLeaderGuid(0);
		TeamShow->ReceiveDestoryGroup();
	}*/

	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->DestroyTeamInfo();
		if (SocialitySys->GetTeamSysPtr()->IsTuanDui())
		{
			ClientSystemNotify(_TRAN("你离开了团队！"));
		}else
		{
			ClientSystemNotify(_TRAN("你离开了队伍！"));
		}
	}
	SYState()->IAudio->PlayUiSound("Sound/UI/outteam.wav", NiPoint3::ZERO, 1.0f, 1 );
}
void CPacketParser::ParseGroupMemberStatUpData(CPacketUsn & pakTool)
{
	MSG_S2C::stPartyMemberStat MsgRecv;
	pakTool>>MsgRecv;
	/*if (TeamShow)
	{
		TeamShow->ReceiveGroupStat(MsgRecv.guid,MsgRecv.mask,MsgRecv.data);
	}*/
	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->UpdateGroupStat(MsgRecv.guid,MsgRecv.mask,MsgRecv.data);
	}
}
void CPacketParser::ParseGroupCommandResult(CPacketUsn & pakTool)
{
	MSG_S2C::stParty_Command_Result MsgRecv;
	pakTool>>MsgRecv;

	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->ErrorText(MsgRecv.member,MsgRecv.res);
	}
	
// 	if (TeamShow)
// 	{
// 		TeamShow->ReceiveGroupError(MsgRecv.member,MsgRecv.res);
// 	}
}
void CPacketParser::ParseLootMethod(CPacketUsn & pakTool)
{
	MSG_S2C::stGroupLootMethod MsgRecv;
	pakTool>>MsgRecv;

	if (TEAMPICKITEMSET)
	{
		TEAMPICKITEMSET->SetTeamPick(MsgRecv.lootmethod, (ITEM_QUALITY)MsgRecv.lootthreshold);
	}

}
void CPacketParser::ParseLootList(CPacketUsn& pakTool)
{
	MSG_S2C::stLoot_Response MsgRecv;pakTool>>MsgRecv;

	if(MsgRecv.gold)
	{
		//把金币当道具处理

		MSG_S2C::stLoot_Response::stLootItem golditem;
		golditem.entry = 1;
		golditem.count = MsgRecv.gold;
		MsgRecv.vLootItems.insert(MsgRecv.vLootItems.begin(), golditem);
	}
	//调用界面

	CGameObject* pGameOBJ = (CGameObject*)ObjectMgr->GetObject(MsgRecv.lootGuid);
	CPlayerLocal* pLocalPlayer = ObjectMgr->GetLocalPlayer();

	if (pLocalPlayer == NULL || pGameOBJ == NULL)
	{
		return ;
	}else
	{
		NiPoint3 ppGameOBJPos = pGameOBJ->GetPosition();
		NiPoint3 pLocalPlayerPos = pLocalPlayer->GetPosition();
		UPickItemList * pPickItemList = INGAMEGETFRAME(UPickItemList,FRAME_IG_PICKLISTDLG);
		if (!pPickItemList)
			return;
		if ((pLocalPlayerPos - ppGameOBJPos).Length() <= MaxLenToNPC  && MsgRecv.vLootItems.size() >0 )
		{
			pPickItemList->OnItemList(MsgRecv.vLootItems,MsgRecv.lootGuid,MsgRecv.gold,MsgRecv.loot_type);				
		}else
		{
			pPickItemList->OnClose();
		}
	}	
}

void CPacketParser::ParseLootRemove(CPacketUsn &pakTool)
{
	MSG_S2C::stMsg_Loot_Removed MsgRecv;pakTool>>MsgRecv;
	UPickItemList * pPickItemList = INGAMEGETFRAME(UPickItemList,FRAME_IG_PICKLISTDLG);
	if (pPickItemList && (pPickItemList->IsVisible() || pPickItemList->IsHookAutoPick()))
	{
		//pPickItemList->SetVisible(TRUE);
		ULOG("ParseLootRemove");
		if (pPickItemList->GetPickGuid())
		{
			pPickItemList->ReMoveItemByPos(MsgRecv.lootSlot);
		}
		
	}
}

void CPacketParser::ParseLootMoney(CPacketUsn& pakTool)
{
	MSG_S2C::stLoot_Money_Notify MsgRecv;pakTool>>MsgRecv;
	// 显示拾取到多少钱
	if(MsgRecv.money)	
	{
		std::string strRet = _TRAN(N_NOTICE_Z33, GetMoneyRichTextStr(MsgRecv.money).c_str());
		PickItemNotify( strRet.c_str() );//"获得金钱：%s",GetMoneyRichTextStr(MsgRecv.money).c_str());
	}
}

void CPacketParser::ParseLootMoneyClear(CPacketUsn& pakTool)
{
	MSG_S2C::stLoot_Clear_Money MsgRecv;pakTool>>MsgRecv;
	UPickItemList * pPickItemList = INGAMEGETFRAME(UPickItemList,FRAME_IG_PICKLISTDLG);
	if (pPickItemList && (pPickItemList->IsVisible() || pPickItemList->IsHookAutoPick()))
	{
		//pPickItemList->SetVisible(TRUE);
		ULOG("ParseLootMoneyClear");
		if (pPickItemList->GetPickGuid())
		{
			pPickItemList->ReMoveMoney();
		}		
	}
}

//需要ROLL的到户. 
void CPacketParser::ParseToRollItem(CPacketUsn& pakTool)
{
	MSG_S2C::stMsg_Loot_Start_Roll MsgRecv;pakTool>>MsgRecv;
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal && pkLocal->IsHook())
	{
		PacketBuilder->SendStartRoll(MsgRecv.loot_guid,MsgRecv.x,1,MsgRecv.mapid,MsgRecv.instanceid);
		return ;
	}

	TEAMROLLITEM->AddRollItem(MsgRecv);
	UPickItemList * pPickItemList = INGAMEGETFRAME(UPickItemList,FRAME_IG_PICKLISTDLG);
	if (pPickItemList && pPickItemList->IsVisible())
	{
		pPickItemList->SetNeedRoll();
	}
}

//暂时不用
void CPacketParser::ParseRandomRoll(CPacketUsn& pakTool)
{
	MSG_S2C::stMsg_Random_Roll MsgRecv;
	pakTool>>MsgRecv;
	CCharacter* pChar = (CCharacter*)ObjectMgr->GetObject(MsgRecv.player_guid);
	if (pChar)
	{
		std::string strRet = _TRAN(N_NOTICE_Z34, pChar->GetName(),_I2A(MsgRecv.rollnum).c_str(), _I2A(MsgRecv.min).c_str(), _I2A(MsgRecv.max).c_str());
		ClientSystemNotify( strRet.c_str() );//"%s掷出%d(%d-%d)", pChar->GetName(), MsgRecv.rollnum, MsgRecv.min, MsgRecv.max);
	}
	else
	{
		if (SocialitySys->GetTeamSysPtr())
		{
			TeamDate* pkData = SocialitySys->GetTeamSysPtr()->GetTeamDate(MsgRecv.player_guid);
			if (pkData)
			{
				std::string strRet = _TRAN(N_NOTICE_Z35, pkData->Name.c_str(), _I2A(MsgRecv.rollnum).c_str(), _I2A(MsgRecv.min).c_str(), _I2A(MsgRecv.max).c_str() );
				ClientSystemNotify( strRet.c_str() );//"%s掷出%d(%d-%d)", pkData->Name.c_str(), MsgRecv.rollnum, MsgRecv.min, MsgRecv.max);
			}
		}
	}
}

//别人ROLL的结果
void CPacketParser::ParseRolledItem(CPacketUsn& pakTool)
{
	MSG_S2C::stLoot_Roll MsgRecv;pakTool>>MsgRecv;

	CPlayerLocal* pLocalPlayer = ObjectMgr->GetLocalPlayer();
	assert(pLocalPlayer);

	ItemPrototype_Client* pItemInfo = ItemMgr->GetItemPropertyFromDataBase(MsgRecv.item_entry);

	if (pItemInfo)
	{
		std::string strBuff;
		if (pLocalPlayer->GetGUID() == MsgRecv.player_guid)
		{
			if (MsgRecv.need == 128)
			{
				strBuff = _TRAN(N_NOTICE_Z36, GetQualityColorStr(pItemInfo->Quality).c_str(), _I2A(MsgRecv.item_entry).c_str(), pItemInfo->C_name.c_str());
				//NiSprintf(buf, 255,"你放弃了<linkcolor:%s><a:#Item_%u>[%s]</a>", GetQualityColorStr(pItemInfo->Quality).c_str(), MsgRecv.item_entry, pItemInfo->C_name.c_str());
			}else
			{
				strBuff = _TRAN(N_NOTICE_Z37, GetQualityColorStr(pItemInfo->Quality).c_str(), _I2A(MsgRecv.item_entry).c_str(), pItemInfo->C_name.c_str(), _I2A((int)MsgRecv.roll).c_str() );	
				//NiSprintf(buf, 255,"你需求<linkcolor:%s><a:#Item_%u>[%s]</a>掷出%d(1-100)",GetQualityColorStr(pItemInfo->Quality).c_str(), MsgRecv.item_entry, pItemInfo->C_name.c_str(), (int)MsgRecv.roll);	
			}

		}else
		{
			if (SocialitySys->GetTeamSysPtr())
			{
				TeamDate* pkData = SocialitySys->GetTeamSysPtr()->GetTeamDate(MsgRecv.player_guid);
				if (pkData)
				{
					if (MsgRecv.need == 128)
					{
						strBuff = _TRAN(N_NOTICE_Z38, pkData->Name.c_str(), GetQualityColorStr(pItemInfo->Quality).c_str(), _I2A(MsgRecv.item_entry).c_str(), pItemInfo->C_name.c_str());
						//NiSprintf(buf, 255,"%s放弃了<linkcolor:%s><a:#Item_%u>[%s]</a>", pkData->Name.c_str(), GetQualityColorStr(pItemInfo->Quality).c_str(), MsgRecv.item_entry, pItemInfo->C_name.c_str());
					}else
					{
						strBuff = _TRAN(N_NOTICE_Z39, pkData->Name.c_str(), GetQualityColorStr(pItemInfo->Quality).c_str(), _I2A(MsgRecv.item_entry).c_str(), pItemInfo->C_name.c_str(), _I2A(MsgRecv.roll).c_str() );
						//NiSprintf(buf, 255,"%s需求<linkcolor:%s><a:#Item_%u>[%s]</a>掷出%d(1-100)",pkData->Name.c_str(), GetQualityColorStr(pItemInfo->Quality).c_str(), MsgRecv.item_entry, pItemInfo->C_name.c_str(), MsgRecv.roll);	
					}	
				}
			}
		}
		ClientSystemNotify(strBuff.c_str());
	}
}

void CPacketParser::ParsestRoolWon(CPacketUsn& pakTool)
{
	MSG_S2C::stMsg_Loot_Rool_Won MsgRecv;pakTool>>MsgRecv;
	CPlayerLocal* pLocalPlayer = ObjectMgr->GetLocalPlayer();
	assert(pLocalPlayer);

	ItemPrototype_Client* pItemInfo = ItemMgr->GetItemPropertyFromDataBase(MsgRecv.itemid);
	if (pItemInfo)
	{
		if (pLocalPlayer->GetGUID() == MsgRecv.player_guid)
		{
			std::string strRet = _TRAN(N_NOTICE_Z40, GetQualityColorStr(pItemInfo->Quality).c_str(), _I2A(MsgRecv.itemid).c_str(), pItemInfo->C_name.c_str());	
			PickItemNotify( strRet.c_str() );//"恭喜获得<linkcolor:%s><a:#Item_%u>[%s]</a>",GetQualityColorStr(pItemInfo->Quality).c_str(), MsgRecv.itemid, pItemInfo->C_name.c_str());	
		}else
		{
			if (SocialitySys->GetTeamSysPtr())
			{
				TeamDate* pkData = SocialitySys->GetTeamSysPtr()->GetTeamDate(MsgRecv.player_guid);
				if (pkData)
				{
					std::string strRet = _TRAN(N_NOTICE_Z41, pkData->Name.c_str() ,GetQualityColorStr(pItemInfo->Quality).c_str(), _I2A(MsgRecv.itemid).c_str(), pItemInfo->C_name.c_str());
					PickItemNotify( strRet.c_str() );//"%s获得了<linkcolor:%s><a:#Item_%u>[%s]</a>", pkData->Name.c_str() ,GetQualityColorStr(pItemInfo->Quality).c_str(), MsgRecv.itemid, pItemInfo->C_name.c_str());
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
void CPacketParser::ParseFriendStatMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stFriend_Stat MsgRecv;
	pakTool>>MsgRecv;
	if (SocialitySys)
	{
		SocialitySys->GetFriendSysPtr()->ParseNetMessage(MsgRecv);
	}
/*	UFriendsDlg * pFriendDlg = INGAMEGETFRAME(UFriendsDlg,FRAME_IG_FRIENDDLG);
	if (pFriendDlg)
	{
		pFriendDlg->OnNetMessage(MsgRecv);
	}*/	
}

void CPacketParser::ParseFriendListMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stFriend_List MsgRecv;
	pakTool>>MsgRecv;
	if (SocialitySys)
	{
		SocialitySys->GetFriendSysPtr()->ParseNetMessage(MsgRecv);
	}
/*	UFriendsDlg * pFriendDlg = INGAMEGETFRAME(UFriendsDlg,FRAME_IG_FRIENDDLG);
	if (pFriendDlg)
	{
		pFriendDlg->OnNetMessage(MsgRecv);
	}*/	
}

void CPacketParser::ParseQueryNameResMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stQuery_Name_Response MsgRecv;
	pakTool>>MsgRecv;

	sObjBaseInfo sInfo;
	sInfo.guid = MsgRecv.guid;
	sInfo.name = MsgRecv.name;
	sInfo.race = MsgRecv.race;
	sInfo.gender = MsgRecv.gender;
	sInfo.Class = MsgRecv.Class;

	ObjectMgr->AddObjBaseInfo(MsgRecv.guid, sInfo);

	if (SocialitySys)
	{
		SocialitySys->GetFriendSysPtr()->OnNameReflesh();
	}

	//更新下师徒称谓
	if (TeacherSystemMgr)
	{
		TeacherSystemMgr->ParseUpdate(sInfo);
	}
}

//////////////////////////////////////////////////////////////////////////
// 拍卖
//////////////////////////////////////////////////////////////////////////
// 拍卖操作结果
void CPacketParser::ParseAuctionCommandResultMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stAuction_Command_Result MsgRecv;
	pakTool>>MsgRecv;
	AUMgr->ParseCommandMsg(MsgRecv);
}

// 查询列表
void CPacketParser::ParseAuctionListResultMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stAuction_List_Result MsgRecv;
	pakTool>>MsgRecv;
	AUMgr->ParseAuctionListMsg(MsgRecv);
}

// 返回出价最高竞拍的玩家
void CPacketParser::ParseAuctionBidderNotifyMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stAuction_Bidder_Notifycation MsgRecv;
	pakTool>>MsgRecv;
}

//
void CPacketParser::ParseAuctionOwnerNotifyMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stAuction_Owner_Notifycation MsgRecv;
	pakTool>>MsgRecv;
}

// 自己购买道具列表
void CPacketParser::ParseAuctionBidderListResultMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stAuction_Bidder_List_Result MsgRecv;
	pakTool>>MsgRecv;
	AUMgr->ParseAuctionBidderMsg(MsgRecv);
}

//自己出售的道具
void CPacketParser::ParseAuctionOwnerListResultMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stAuction_Owner_List_Result MsgRecv;
	pakTool>>MsgRecv;

	AUMgr->ParseAuctionOwerList(MsgRecv);
}

//
void CPacketParser::ParseAuctionHelloMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stAuction_Hello MsgRecv;
	pakTool>>MsgRecv;

	AUMgr->ParseHelloMsg(MsgRecv);
}

void CPacketParser::ParseNeedReadMail(CPacketUsn& pakTool)
{
	MSG_S2C::stMail_Query_Next_Time MsgRecv ;
	pakTool>>MsgRecv;
	UMinimapFrame* pkMiniMap = INGAMEGETFRAME(UMinimapFrame,FRAME_IG_MINIMAP);
	if (MsgRecv.vMails.size())
	{
		//UI提示有未读邮件
		
		if (pkMiniMap)
		{
			pkMiniMap->OnShowMail(TRUE);
		}

	}else
	{
		if (pkMiniMap)
		{
			pkMiniMap->OnShowMail(FALSE);
		}	
	}

	if (MailSystemMgr)
	{
		MailSystemMgr->SetNeedReadConut(MsgRecv.vMails.size());
	}
}

//查询邮件文字内容回馈
void CPacketParser::ParseMailContentMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stMail_Text_Query_Response MsgRecv ; 
	pakTool>>MsgRecv;

	MailSystemMgr->SetCurReadMail(MsgRecv);

}
//收到新邮件
void CPacketParser::ParseReceiveMail(CPacketUsn& pakTool)
{
	MSG_S2C::stMail_Received MsgRecv ; 
	pakTool>>MsgRecv;

	ClientSystemNotify(_TRAN("你有新的邮件，请注意查收."));
	UMinimapFrame* pkMiniMap = INGAMEGETFRAME(UMinimapFrame,FRAME_IG_MINIMAP);
	if (pkMiniMap)
	{
		pkMiniMap->OnShowMail(TRUE);
	}
	MailSystemMgr->SetAddNeedReadCount();

}
//邮件列表信息
void CPacketParser::ParseMailListMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stMail_List_Result MsgRecv ; 
	pakTool>>MsgRecv;

	

	MSG_S2C::stMail_List_Result MailList;
	int Count = 0;
	MailList.npc_guid = MsgRecv.npc_guid;
	Count = MsgRecv.vMails.size();
	for (int i = 0; i < Count; i++)
	{
		MailList.vMails.push_back(MsgRecv.vMails[Count - i - 1]);
	}
		
	MailSystemMgr->InitMailList(MailList);

}
//发送邮件的结果返回
void CPacketParser::ParseSendMailResult(CPacketUsn& pakTool)
{
	MSG_S2C::stMail_Send_Result MsgRecv ; 
	pakTool>>MsgRecv;

	UInGame* pkInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pkInGame)
	{
		switch (MsgRecv.result)
		{
		case MAIL_OK:
			{
				switch (MsgRecv.res)
				{
				case MAIL_RES_MAIL_SENT :            //发送邮件的返回
					EffectMgr->AddEffectText(_TRAN("邮件发送成功！"), 1);
					break;
				case MAIL_RES_MONEY_TAKEN :          // 提取金钱的返回
					{
						//EffectMgr->AddEffeText("提取金钱成功！");
						MailSystemMgr->UpdateMailMoney(MsgRecv.message_id);
					}
					break;
				case MAIL_RES_ITEM_TAKEN :          //提取物品的返回
					{
						//EffectMgr->AddEffeText("提取物品成功！");
						MailSystemMgr->UpdateMailByMailID(MsgRecv.message_id,MsgRecv.item_guid,MsgRecv.stack_count);
					}
					break;
				case MAIL_RES_RETURNED_TO_SENDER :  //退回邮件的返回
					{
						//EffectMgr->AddEffeText("成功退回邮件！");
					}
					break;
				case MAIL_RES_DELETED :             //删除返回
					{
						//EffectMgr->AddEffeText("删除邮件成功！");
						MailSystemMgr->DeletaMailByID(MsgRecv.message_id);
					}
					break;
				case MAIL_RES_MADE_PERMANENT :      //拿取信纸
					{
						//EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), "拿取信纸成功！");
					}
					break;
				}
			}
			break;
		case MAIL_ERR_BAG_FULL:
			EffectMgr->AddEffectText(_TRAN("提取附件失败：背包已满。"));
			break;
		case MAIL_ERR_CANNOT_SEND_TO_SELF:
			EffectMgr->AddEffectText(_TRAN("发送失败：不能发送给自己。"));
			break;
		case MAIL_ERR_NOT_ENOUGH_MONEY:	
			MailSystemMgr->UpdateContent();
			EffectMgr->AddEffectText(_TRAN("发送失败：您没有足够金钱。"));
			break;
		case MAIL_ERR_RECIPIENT_NOT_FOUND:
			EffectMgr->AddEffectText(_TRAN("发送失败：没有找到该玩家。"));
			break;
		case MAIL_ERR_NOT_YOUR_ALLIANCE:
			EffectMgr->AddEffectText(_TRAN("发送失败：不是一个阵营。"));
			break;
		case MAIL_ERR_INTERNAL_ERROR:
			EffectMgr->AddEffectText(_TRAN("失败：您还不能这样做。"));
			break;
		case MAIL_MAILBOX_IS_FULL:
			EffectMgr->AddEffectText(_TRAN("邮箱已满，请及时清理！"));
			break;
		}
	}
}
//自己接受到的BUFF技能
void CPacketParser::ParseAuraDuration(CPacketUsn& pakTool)
{
	MSG_S2C::stAura_Set_Duration MsgRecv ; 
	pakTool>>MsgRecv;

	CPlayerLocal* pLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (!pLocalPlayer)
	{
		return;
	}
	pLocalPlayer->SetAuraDuringTime(MsgRecv.slot, MsgRecv.duration / 1000.0f, MsgRecv.duration / 1000.0f);
	pLocalPlayer->SetAuraLevel(MsgRecv.slot, MsgRecv.count);
	//if (AuraSystem)
	//{
	//	AuraSystem->SetTime(AURA_LOCALBIG,MsgRecv.slot, (float)MsgRecv.duration);
	//	AuraSystem->SetTime(AURA_LOCALSMALL,MsgRecv.slot, (float)MsgRecv.duration);
	//}
}

void CPacketParser::ParseAuraSingle(CPacketUsn& pakTool)
{
	MSG_S2C::stAura_Set_Single MsgRecv;
	pakTool>>MsgRecv;

	CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(MsgRecv.target_guid);
// 	NIASSERT(pkChar);
	if (!pkChar) return;

	pkChar->ApplyAura(MsgRecv.spellid, MsgRecv.slot);
	pkChar->SetAuraDuringTime(MsgRecv.slot, MsgRecv.duration1 / 1000.0f, MsgRecv.duration1 / 1000.0f);
	pkChar->SetAuraLevel(MsgRecv.slot, MsgRecv.count);

	//if (MsgRecv.target_guid == TeamShow->GetTargetID())
	//{
	//	if (AuraSystem)
	//	{
	//		AuraSystem->SetTime(AURA_TARGET,MsgRecv.slot, (float)MsgRecv.duration1);
	//	}

	//	//pkInGame->GetUIAura(2)->UpDataAura(MsgRecv.slot,MsgRecv.duration1,MsgRecv.target_guid);
	//}
}
void CPacketParser::ParseChannelNotify(CPacketUsn& pakTool)
{
	MSG_S2C::stChannel_Notify MsgRecv;
	pakTool>>MsgRecv;
	ChatSystem->ChannelNotify(MsgRecv.guid,MsgRecv.nofify_flag,MsgRecv.name,MsgRecv.old_member_flag,MsgRecv.member_flag);
}

void SendMountAccept()
{
	PacketBuilder->SendMountAccept();
}
void SendMountDecline()
{
	PacketBuilder->SendMountDecline();
}
//被骑的人邀请骑乘的人
void CPacketParser::ParseMountInviteMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stMount_Invite MsgRecv;
	pakTool>>MsgRecv;
	UMessageBox::MsgBox(_TRAN(N_NOTICE_C81, MsgRecv.player_name.c_str()).c_str(),
		(BoxFunction*)SendMountAccept,
		(BoxFunction*)SendMountDecline);

}
//解散队伍
void CPacketParser::ParseMountDisbandMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stMount_Disband MsgRecv;
	pakTool>>MsgRecv;

}
//同意邀请
void CPacketParser::ParseMountAcceptMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stMount_Accept MsgRecv;
	pakTool>>MsgRecv;
	ClientSystemNotify(_TRAN(N_NOTICE_C82, MsgRecv.player_name.c_str()).c_str());
}
//拒绝邀请
void CPacketParser::ParseMountDeclineMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stMount_Decline MsgRecv;
	pakTool>>MsgRecv;
	ClientSystemNotify(_TRAN(N_NOTICE_C83, MsgRecv.player_name.c_str()).c_str());

}
//回复
void CPacketParser::ParseMountResultMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stMount_Result MsgRecv;
	pakTool>>MsgRecv;

	switch (MsgRecv.result)
	{
	case MSG_S2C::MOUNT_RESULT_DISBAND:
		{
			ClientSystemNotify(_TRAN("骑乘解散"));
		}
		break;
	case MSG_S2C::MOUNT_RESULT_NOT_RIDE:
		{
			ClientSystemNotify(_TRAN("不在骑乘状态"));
		}
		break;
	case MSG_S2C::MOUNT_RESULT_BUSY:
		{
			ClientSystemNotify(_TRAN("目标状态忙"));
		}
		break;
	case MSG_S2C::MOUNT_RESULT_ALREADY_RIDE:
	case MSG_S2C::MOUNT_RESULT_NOT_IN_RIDE:	
		{
			ClientSystemNotify(_TRAN("目标已经骑乘"));
		}
		break;
	case MSG_S2C::MOUNT_RESULT_NOT_FOUND:
		{
			ClientSystemNotify(_TRAN("找不到目标"));
		}
		break;
	case MSG_S2C::MOUNT_RESULT_TARGET_SHAPESHIFT:
		{
			ClientSystemNotify(_TRAN("目标已经变身"));
		}
		break;
	case MSG_S2C::MOUNT_RESULT_TOO_FAR:
		{
			ClientSystemNotify(_TRAN("目标距离太远"));
		}break;
	case MSG_S2C::MOUNT_RESULT_CANNOTRIDE:
		{
			ClientSystemNotify(_TRAN("目标无法骑乘"));
		}break;
	}
}
//////////////////////////////////////////////////////////////////////////
// 工会
//////////////////////////////////////////////////////////////////////////
void SendGuildAccept()
{
	PacketBuilder->SendAcceptGuildInvite();
}
void SendGuildDecline()
{
	PacketBuilder->SendDeclineGuildInvite();
}
//被邀请加入工会
void CPacketParser::ParseGuildInviteMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stGuild_Invite MsgRecv ; 
	pakTool>>MsgRecv;
	UMessageBox::MsgBox(_TRAN(N_NOTICE_C84, MsgRecv.inviteeName.c_str(), MsgRecv.guildName.c_str()).c_str(),
		(BoxFunction*)SendGuildAccept,
		(BoxFunction*)SendGuildDecline);
}
//对方拒绝你的邀请
void CPacketParser::ParseGuildDeclineMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stGuild_Decline MsgRecv ; 
	pakTool>>MsgRecv;

	std::string strRet = _TRAN(N_NOTICE_Z42, MsgRecv.player_who_invite.c_str());
	ChatMsgNotify(CHAT_MSG_SYSTEM, strRet.c_str() );//"[%s] 拒绝加入部族!", MsgRecv.player_who_invite.c_str());
}
//工会简介
void CPacketParser::ParseGuildInfoMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stGuild_Info MsgRecv ; 
	pakTool>>MsgRecv;
}
//对应查询工会的回复
void CPacketParser::ParseGuildQueryMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stGuild_Query_Response MsgRecv ; 
	pakTool>>MsgRecv;
	if (SocialitySys)
	{
		SocialitySys->GetGuildSysPtr()->ParseMsgGuildQueryResponse(MsgRecv);
	}
}
//工会所有信息
void CPacketParser::ParseGuildRosterMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stGuild_Roster MsgRecv ; 
	pakTool>>MsgRecv;
	if (SocialitySys)
	{
		SocialitySys->GetGuildSysPtr()->ParseMsgGuildInfo(MsgRecv.guildInfo.c_str());
		SocialitySys->GetGuildSysPtr()->ParseMsgRankRight(MsgRecv.vBanks);
		SocialitySys->GetGuildSysPtr()->ParseMsgMember(MsgRecv.vMembers);
	}
}
//工会创建界面
void CPacketParser::ParseGuildCreateMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stGuild_Create MsgRecv;
	pakTool>>MsgRecv;
	if (SocialitySys)
	{
		SocialitySys->GetGuildSysPtr()->ParseMsgGuildCreate();
		HELPEVENTTRIGGER(TUTORIAL_FLAG_GUILD);
	}
}
void CPacketParser::ParseGuildEvent(CPacketUsn& pakTool)
{
	MSG_S2C::stGuild_Event MsgRecv;
	pakTool>>MsgRecv;
	if (SocialitySys)
	{
		SocialitySys->GetGuildSysPtr()->ParseMsgGuildEvent(MsgRecv.guild_event, MsgRecv.vStrs);
	}
}
void CPacketParser::ParseGuildCommandResult(CPacketUsn& pakTool)
{
	MSG_S2C::stGuild_Command_Result MsgRecv;
	pakTool>>MsgRecv;
	if (SocialitySys)
	{
		SocialitySys->GetGuildSysPtr()->ParseMsgGuildCommandResault(MsgRecv.iCmd, MsgRecv.szMsg, MsgRecv.iType);
	}
}
void CPacketParser::ParseNotifyMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stNotify_Msg MsgRecv;
	pakTool>>MsgRecv;
	ChatMsgNotify(CHAT_MSG_SYSTEM, MsgRecv.notify.c_str());
	UISystemMsg* pSGN = INGAMEGETFRAME(UISystemMsg, FRAME_IG_SYSGIFTNOTIFY);
	if (pSGN)
	{
		pSGN->GetSystemMsg( MsgRecv.notify.c_str(), 1.f);
	}
}

void CPacketParser::ParseQureyGuildsList(CPacketUsn& pakTool)
{
	MSG_S2C::stGuildListInfoAck MsgRecv;
	pakTool>>MsgRecv;
	if (SocialitySys)
	{
		SocialitySys->GetGuildSysPtr()->ParseMsgGuildList(MsgRecv.vGuilds);
	}
}
void CPacketParser::ParseGuildContributionPointsNotify(CPacketUsn& pakTool)
{
	MSG_S2C::stGuildContributionPointsNotify MsgRecv;
	pakTool>>MsgRecv;
	if (SocialitySys)
	{
		SocialitySys->GetGuildSysPtr()->ParseMsgGuildContributionPoints(MsgRecv.player_id, MsgRecv.points, MsgRecv.guild_score);
	}
}
void CPacketParser::ParseGuildDeclareWarNotify(CPacketUsn& pakTool)
{
	MSG_S2C::stGuildDeclareWarNotify MsgRecv;
	pakTool>>MsgRecv;
	if (SocialitySys)
	{
		SocialitySys->GetGuildSysPtr()->ParseGuildDeclareWarNotify(MsgRecv.active_guildname, MsgRecv.passive_guildname);
	}
}

void CPacketParser::ParseCastleNPCStateNotify(CPacketUsn& pakTool)
{
	MSG_S2C::stCastleNPCStateNotify MsgRecv;
	pakTool>>MsgRecv;

	SocialitySys->GetGuildSysPtr()->ParseGuildCastleNpcBuy(MsgRecv.buy_npcs, MsgRecv.map_id);
}

void CPacketParser::ParseCastleCountDown(CPacketUsn& pakTool)
{
	MSG_S2C::stCastleCountDown MsgRecv;
	pakTool>>MsgRecv;

	SocialitySys->GetGuildSysPtr()->ParseGuildCastleCountDown(MsgRecv.castle_name, MsgRecv.mapid, MsgRecv.next_state, MsgRecv.count_down_mins);
}

void CPacketParser::ParseGuildLevelUp(CPacketUsn& pakTool)
{
	MSG_S2C::stGuild_Npc_Level_Up MsgRecv;
	pakTool>>MsgRecv;

	SocialitySys->GetGuildSysPtr()->ParseMsgGuildLevelUp(MsgRecv.Level);
}

void CPacketParser::ParseBattleGroundPowerUpdate(CPacketUsn& pakTool)
{
	MSG_S2C::stBattleGroundPowerUpdate  MsgRecv;
	pakTool>>MsgRecv;

	InstanceSys->ParseBattleGroundPowerUpdate(MsgRecv);
}

void CPacketParser::ParseBattleGroundResourceUpdate(CPacketUsn& pakTool)
{
	MSG_S2C::stBattleGroundResourceUpdate  MsgRecv;
	pakTool>>MsgRecv;

	InstanceSys->ParseBattleGroundResourceUpdate(MsgRecv);
}

void CPacketParser::ParseBattleGroundDetailInformation(CPacketUsn& pakTool)
{
	MSG_S2C::stBattleGroundDetailInformation MsgRecv;
	pakTool>>MsgRecv;

	InstanceSys->ParseBattleGroundDetailInformation(MsgRecv);
}

void CPacketParser::ParseCastleStateAck(CPacketUsn& pakTool)
{
	MSG_S2C::stCastleStateAck MsgRecv;
	pakTool>>MsgRecv;
	SocialitySys->GetGuildSysPtr()->ParseGuildCastleState(MsgRecv.vstates, MsgRecv.server_time);
}

void CPacketParser::ParseFreshManHelpFlagMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stTutorial_Flags MsgRecv;
	pakTool>>MsgRecv;
	UFreshManTip* pFreshManTip = INGAMEGETFRAME(UFreshManTip, FRAME_IG_FRESHMANTIP);
	if (pFreshManTip)
	{
		unsigned int uisize = sizeof(MsgRecv.Tutorials);
		pFreshManTip->PraseTutorialFlag(MsgRecv.Tutorials, uisize);
	}

}
void CPacketParser::ParseShopItemListMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stShop_ItemList MsgRecv;	
	pakTool>>MsgRecv;
	ShopSystem * pShop = INGAMEGETFRAME(ShopSystem,FRAME_IG_SHOPDLG);
	if (pShop)
	{
		pShop->ParseItemList(MsgRecv.vItems, MsgRecv.vHotItems, MsgRecv.vShopActivity);
	}
}

void CPacketParser::ParseShopActivityUpdate(CPacketUsn& pakTool)
{
	MSG_S2C::stShopActivity_Update MsgRecv;	
	pakTool>>MsgRecv;

	ShopSystem * pShop = INGAMEGETFRAME(ShopSystem,FRAME_IG_SHOPDLG);
	if (pShop)
	{
		pShop->ParseActivityUpdate(MsgRecv.category, MsgRecv.discount);
	}
}

void CPacketParser::ParsePongMsg(CPacketUsn& pakTool)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	uint32 lstping = SYState()->LastPing;
	uint32 ct = float2int32(NiGetCurrentTimeInSec() * 1000);
	uint32 gap = ct - lstping;
	SYState()->CurDelay = gap / 2;
}

void CPacketParser::ParseCopyName(CPacketUsn& pakTool)
{
	MSG_S2C::stCopyName MsgRecv;pakTool >> MsgRecv;
	HGLOBAL   hGlobal=GlobalAlloc(GMEM_MOVEABLE,MsgRecv.name.size()+1);     //分配指定长度内存
	char *pszText = (char*)GlobalLock( hGlobal );
	strcpy( pszText, MsgRecv.name.c_str());
	OpenClipboard(NULL);     //打开剪贴板   
	EmptyClipboard();     //清空剪贴板   
	SetClipboardData(CF_TEXT, hGlobal);     //将ID放入剪贴板
	CloseClipboard();     //关闭剪贴板
	if( hGlobal )
		GlobalFree( hGlobal );
}

void CPacketParser::ParseHideGM(CPacketUsn& pakTool)
{
	MSG_S2C::stHideGM MsgRecv;pakTool >> MsgRecv;
	if(MsgRecv.bHide)
        CShadowGeometry::SetShadowColor(NiColor(1.f, 0.f, 0.f));
	else
        CShadowGeometry::SetShadowColor(DEFAULT_SHADOW_COLOR);
}


void CPacketParser::ParseQueueInstanceUpdate(CPacketUsn& pakTool)
{
	MSG_S2C::stQueueInstanceUpdate MsgRecv;pakTool>>MsgRecv;
	InstanceSys->ParseQueueUpdata(MsgRecv.mapid);
}
void CPacketParser::ParseEnterInstanceCountDown(CPacketUsn& pakTool)
{
	MSG_S2C::stEnterInstanceCountDown MsgRecv;pakTool>>MsgRecv;
	InstanceSys->ParseInstanceCountDown(MsgRecv.mapid, MsgRecv.seconds);
}
void CPacketParser::ParseArenaUpdateState(CPacketUsn& pakTool)
{
	MSG_S2C::stArenaUpdateState MsgRecv;pakTool>>MsgRecv;
	InstanceSys->ParseAreanUpdateState(MsgRecv.vinfo);
}
void CPacketParser::ParsePlayerEnterInstance(CPacketUsn& pakTool)
{
	MSG_S2C::stPlayerEnterInstance MsgRecv;pakTool>>MsgRecv;
	InstanceSys->ParseEnterInstance(MsgRecv.expire, MsgRecv.mapid, MsgRecv.category);
}
void CPacketParser::ParseSunyouInstanceList(CPacketUsn& pakTool)
{
	MSG_S2C::stSunyouInstanceList MsgRecv;pakTool>>MsgRecv;
	InstanceSys->ParseInstanceList(MsgRecv.v);
}

void OnInstanceDeadChoiceYes()
{
	InstanceSys->SendInstanceDeadExit(true);
	//PacketBuilder->SendInstanceDeadExit(true);
	//MSG_C2S::stChoiceInstanceDeadExit Msg;
	//Msg.bExit = true;
	//NetworkMgr->SendPacket(Msg);	
}
void OnInstanceDeadChoiceNo()
{
	InstanceSys->SendInstanceDeadExit(false);
	//MSG_C2S::stChoiceInstanceDeadExit Msg;
	//Msg.bExit = false;
	//NetworkMgr->SendPacket(Msg);	
}

void CPacketParser::ParseChoiceDeadExit(CPacketUsn& pakTool)
{
	UMessageBox::MsgBox(_TRAN("是否继续?"), (BoxFunction*)OnInstanceDeadChoiceNo, (BoxFunction*)OnInstanceDeadChoiceYes);
}

void CPacketParser::ParseInstanceBillBoard(CPacketUsn& pakTool)
{
	MSG_S2C::stInstanceBillboard MsgRecv;pakTool>>MsgRecv;
	InstanceSys->ParseAreanBillBoard(MsgRecv.vinfo);
	/*UBillBoard* pBillBoard = INGAMEGETFRAME(UBillBoard, FRAME_IG_BILLBOARD);
	if (pBillBoard)
	{
		pBillBoard->GetBillBoardInfo(MsgRecv.vinfo);
	}*/
}
void CPacketParser::ParsePlayerLeaveInstance(CPacketUsn& pakTool)
{
	MSG_S2C::stPlayerLeaveInstance MsgRecv;pakTool>>MsgRecv;
	InstanceSys->ParseLeaveInstance(MsgRecv.guid, MsgRecv.name);
	/*UInstanceTime* pInstanceTime = INGAMEGETFRAME(UInstanceTime, FRAME_IG_INSTANCETIME);
	if (pInstanceTime)
	{
		pInstanceTime->GetPlayerLeave(MsgRecv.guid, MsgRecv.name);
	}*/
}
void CPacketParser::ParseSystemGift(CPacketUsn& pakTool)
{
	MSG_S2C::stSystem_Gift MsgRecv;pakTool>>MsgRecv;
	UGiftGivenTips* pGiftTip = INGAMEGETFRAME(UGiftGivenTips, FRAME_IG_SYSGIFT);
	if (pGiftTip)
	{
		pGiftTip->GetSysGift(MsgRecv.timecounter);
	}
}
void CPacketParser::ParseSystemGiftMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stSystem_Gift_Notify MsgRecv;pakTool>>MsgRecv;
	BOOL bFine = FALSE;
	char buf[1024];
	std::string strBuff;

	switch(MsgRecv.ntype)
	{
	case 1:
		{
			ItemPrototype_Client* pItemInfo = ItemMgr->GetItemPropertyFromDataBase(MsgRecv.miscvalue1);
			if (pItemInfo)
			{
				//sprintf(buf, "恭喜你获得礼物：%d个%s", MsgRecv.miscvalue2, pItemInfo->C_name.c_str());
				strBuff = _TRAN(N_NOTICE_Z43, _I2A(MsgRecv.miscvalue2).c_str(), pItemInfo->C_name.c_str());
				bFine = TRUE;
			}
		}
		break;
	case 2:
		{
			std::string temp;
			UINT gold = MsgRecv.miscvalue1 / 10000;
			if (gold != 0)
			{
				temp += itoa(gold, buf, 10);
				temp += _TRAN("金");
			}
			UINT silver = (MsgRecv.miscvalue1 - gold*10000)/100;
			if (silver != 0)
			{
				temp += itoa(silver, buf, 10);
				temp += _TRAN("银");
			}
			UINT copper = MsgRecv.miscvalue1 % 100;			
			if (copper != 0)
			{
				temp += itoa(copper, buf, 10);
				temp += _TRAN("铜");
			}
			//sprintf(buf, "恭喜你获得礼物：金钱%s", temp.c_str());
			strBuff = _TRAN(N_NOTICE_Z44,temp.c_str());
			bFine = TRUE;
		}
		break;
	case 3:
		{
			//sprintf(buf, "恭喜你获得礼物：经验%u点", MsgRecv.miscvalue1);
			strBuff = _TRAN(N_NOTICE_Z45, _I2A(MsgRecv.miscvalue1).c_str() );
			bFine = TRUE;
		}
		break;
	case 4:
		{
			sprintf(buf, "%s", MsgRecv.Tips.c_str());
			bFine = TRUE;
		}
		break;
	}
	if (bFine)
	{
		UISystemMsg* pSGN = INGAMEGETFRAME(UISystemMsg, FRAME_IG_SYSGIFTNOTIFY);
		if (pSGN)
		{
			pSGN->GetSystemMsg(strBuff.c_str());
		}
	}
}

void CPacketParser::ParseAISwitchMove(CPacketUsn& pakTool)
{
	MSG_S2C::stAI_Swtich_Active_Mover MsgRecv;pakTool >> MsgRecv;
	if(!MsgRecv.flag)
		SYState()->LocalPlayerInput->LockInput();
	else
		SYState()->LocalPlayerInput->UnlockInput();
	return;
}


//////////////////////////////////////////////////////////////////////////
//	捐助
//////////////////////////////////////////////////////////////////////////
void CPacketParser::ParseContributionConfirmResult(CPacketUsn& pakTool)
{
	MSG_S2C::stContributionConfirmResult MsgRecv;
	pakTool >> MsgRecv;

	//UEndowment* pkEndowment = INGAMEGETFRAME(UEndowment, FRAME_IG_ENDOWMENTDLG);
	//if (pkEndowment)
	//{
	//	pkEndowment->OnNetMessage(MsgRecv);
	//}
}

void CPacketParser::ParseSelfContributionHistoryResponse(CPacketUsn& pakTool)
{
	MSG_S2C::stSelfContributionHistoryResponse MsgRecv;
	pakTool >> MsgRecv;

	//UEndowment* pkEndowment = INGAMEGETFRAME(UEndowment, FRAME_IG_ENDOWMENTDLG);
	//if (pkEndowment)
	//{
	//	pkEndowment->OnNetMessage(MsgRecv);
	//}
}

void CPacketParser::ParseContributionBillboardResponse(CPacketUsn& pakTool)
{
	MSG_S2C::stContributionBillboardResponse MsgRecv;
	pakTool >> MsgRecv;

	//UEndowment* pkEndowment = INGAMEGETFRAME(UEndowment, FRAME_IG_ENDOWMENTDLG);
	//if (pkEndowment)
	//{
	//	pkEndowment->OnNetMessage(MsgRecv);
	//}
}

void CPacketParser::ParseContributionQueryResult(CPacketUsn& pakTool)
{
	MSG_S2C::stContributionQueryResult MsgRecv;
	pakTool >> MsgRecv;

	//UEndowment* pkEndowment = INGAMEGETFRAME(UEndowment, FRAME_IG_ENDOWMENTDLG);
	//if (pkEndowment)
	//{
	//	pkEndowment->OnNetMessage(MsgRecv);
	//}
}

void CPacketParser::ParseSystemMoveMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stSystemNotifyTopMove MsgRecv;
	pakTool>>MsgRecv;

	USystemScrollMsg* pSystemMoveMsg = INGAMEGETFRAME(USystemScrollMsg, FRAME_IG_SYSTEMSCROLLTEXT);
	if (pSystemMoveMsg)
	{
		for (UINT i = 0 ; i < MsgRecv.vNotify.size() ; i++)
		{
			pSystemMoveMsg->AddMoveMsg(MsgRecv.vNotify[i].id, MsgRecv.vNotify[i].ntype, MsgRecv.vNotify[i].tips);
		}
	}
}

void CPacketParser::ParseSystemMoveAddMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stSystemNotifyAdd MsgRecv;
	pakTool>>MsgRecv;

	USystemScrollMsg* pSystemMoveMsg = INGAMEGETFRAME(USystemScrollMsg, FRAME_IG_SYSTEMSCROLLTEXT);
	if (pSystemMoveMsg)
	{
		pSystemMoveMsg->AddMoveMsg(MsgRecv.id, MsgRecv.ntype, MsgRecv.tips);
	}
}

void CPacketParser::ParseSystemMoveRemoveMsg(CPacketUsn& pakTool)
{
	MSG_S2C::stSystemNotifyRemove MsgRecv;
	pakTool>>MsgRecv;

	USystemScrollMsg* pSystemMoveMsg = INGAMEGETFRAME(USystemScrollMsg, FRAME_IG_SYSTEMSCROLLTEXT);
	if (pSystemMoveMsg)
	{
		pSystemMoveMsg->RemoveMoveMsg(MsgRecv.id);
	}
}


void CPacketParser::ParseJingLianResult(CPacketUsn& pakTool)
{
	MSG_S2C::stJingLianItemAck MsgRecv; 
	pakTool>>MsgRecv;

	if (EquRefineMgr)
	{
		EquRefineMgr->ParseJingLianResult(MsgRecv.result);
	}
}
void CPacketParser::ParseXiangQianResult(CPacketUsn& pakTool)
{
	MSG_S2C::stXiangqianItemAck MsgRecv; 
	pakTool>>MsgRecv;

	/*if (EquRefineMgr)
	{
		EquRefineMgr->ParseRefineResult(MsgRecv.result);
	}*/
}


void CPacketParser::ParsePetStableResult(CPacketUsn& pakTool)
{
	MSG_S2C::stPet_Stable_Result MsgRecv; 
	pakTool>>MsgRecv;

	UPetDlg* pPetDlg = INGAMEGETFRAME(UPetDlg, FRAME_IG_PLAYERPETDLG);
	if (pPetDlg && MsgRecv.result == 0x8)
	{
		pPetDlg->OnPetStableSuccess();
	}
}
void CPacketParser::ParsePetSpell(CPacketUsn& pakTool)
{
	MSG_S2C::stPet_Spells MsgRecv; 
	pakTool>>MsgRecv;
}
void CPacketParser::ParsePetActionSound(CPacketUsn& pakTool)
{
	MSG_S2C::stPet_Action_Sound MsgRecv; 
	pakTool>>MsgRecv;
}
void CPacketParser::ParsePetNameQueryResponse(CPacketUsn& pakTool)
{
	MSG_S2C::stPet_Name_Query_Response MsgRecv; 
	pakTool>>MsgRecv;
}
void CPacketParser::ParsePetListStabled(CPacketUsn& pakTool)
{
	MSG_S2C::stPets_List_Stabled MsgRecv; 
	pakTool>>MsgRecv;
	UPetDlg* pPetDlg = INGAMEGETFRAME(UPetDlg, FRAME_IG_PLAYERPETDLG);
	if (pPetDlg)
	{
		pPetDlg->ParsePetList(MsgRecv.StableSlotCount, MsgRecv.vPets);
	}
}
void CPacketParser::ParsePetUnLearnConfirm(CPacketUsn& pakTool)
{
	MSG_S2C::stPet_UnLearn_Confirm MsgRecv; 
	pakTool>>MsgRecv;
}

void CPacketParser::ParsePetRenameAck(CPacketUsn& pakTool)
{
	MSG_S2C::stPet_Rename_Ack MsgRecv;
	pakTool>>MsgRecv;

	UPetDlg* pPetDlg = INGAMEGETFRAME(UPetDlg, FRAME_IG_PLAYERPETDLG);
	if (pPetDlg)
	{
		pPetDlg->OnPetNameChange(MsgRecv.guid, MsgRecv.name);
	}
}
void CPacketParser::ParseServerTiem(CPacketUsn& pakTool)
{
	MSG_S2C::stServerTime MsgRecv;
	pakTool>>MsgRecv;
	if (TipSystem)
	{
		TipSystem->OnGetServerTime(MsgRecv.timeServer);
	}

	if(!gServerTime.IsInit())
	{
		gServerTime.SetServerTime(MsgRecv.timeServer);
	}
}

void CPacketParser::ParseBattleKill(CPacketUsn& pakTool)
{

	MSG_S2C::stBattleGroundSpecialNotify MsgRecv;
	pakTool>>MsgRecv;
	MsgRecv.who;
	MsgRecv.state;
	std::string strWhere;
	strWhere = "Sound\\DotASounds\\";
	std::string strSound;
	
	char txt[512];

	txt[0] = 0;

	std::string strText;

	if (MsgRecv.state&DOTA_2_KILL)
	{
		strSound = "Double_Kill.wav";
		//sprintf( txt, "[%s]双杀!", MsgRecv.who.c_str() );
		strText = _TRAN(N_NOTICE_Z46, MsgRecv.who.c_str() );
	}
	if (MsgRecv.state & DOTA_3_KILL)
	{
		strSound = "triple_kill.wav";
		//sprintf( txt, "[%s]三杀!", MsgRecv.who.c_str() );
		strText = _TRAN(N_NOTICE_Z47, MsgRecv.who.c_str() );

	}
	if (MsgRecv.state & DOTA_4_KILL)
	{
		strSound = "UltraKill.wav";
		//sprintf( txt, "[%s]四杀!", MsgRecv.who.c_str() );
		strText = _TRAN(N_NOTICE_Z48, MsgRecv.who.c_str() );
	}
	if (MsgRecv.state & DOTA_5_KILL)
	{
		strSound = "Rampage.wav";
		//sprintf( txt, "[%s]暴走!", MsgRecv.who.c_str() );
		strText = _TRAN(N_NOTICE_Z49, MsgRecv.who.c_str() );
	}

	std::string strPlaySound;
	if( !strSound.empty() )
		strPlaySound = strWhere + strSound;
	UISystemMsg* pSGN = INGAMEGETFRAME(UISystemMsg, FRAME_IG_SYSGIFTNOTIFY);
	if (txt[0])
	{
		ChatMsgNotify(CHAT_MSG_SYSTEM, strText.c_str() );
	
		if (pSGN)
		{
			pSGN->GetSystemMsg(strText.c_str());
		}
	}



	if( !strPlaySound.empty() )
	{
		ALAudioSource* pkSound = (ALAudioSource*)AudioSystem::GetAudioSystem()->CreateSource(AudioSource::TYPE_DEFAULT);
		if (!pkSound)
			return;

		pkSound->SetGain(1.0f);
		pkSound->SetLoopCount(1);
		pkSound->SetPlayTime(2.5f);
		SYState()->IAudio->PlaySoundByList(pkSound, strPlaySound.c_str());
	}
	strSound.clear();

	txt[0] = 0;
	strText.clear();

	if (MsgRecv.state & DOTA_FIRST_BLOOD)
	{
		strSound = "firstblood.wav";
		strText = _TRAN(N_NOTICE_Z50,  MsgRecv.who.c_str() );
		//sprintf( txt, "[%s]获得了第一滴血!", MsgRecv.who.c_str() );

	}
	if (MsgRecv.state & DOTA_KILLING_SPREE)
	{
	
		if(MsgRecv.is_break)
		{
			//sprintf( txt, "[%s]终结了[%s]的大杀特杀!",MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z51,  MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
		}
		else
		{
			strSound = "Killing_Spree.wav";
			//sprintf( txt, "[%s]正在大杀特杀!", MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z52,  MsgRecv.who.c_str() );
		}

	}
	if (MsgRecv.state & DOTA_DOMINATING)
	{

		if(MsgRecv.is_break)
		{
			//sprintf( txt, "[%s]终结了[%s]的主宰比赛",MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z53,  MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
		}
		else
		{
			strSound = "Dominating.wav";
			//sprintf( txt, "[%s]主宰了比赛!", MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z54,  MsgRecv.who.c_str() );
		}

	}
	if (MsgRecv.state & DOTA_MEGAKILL)
	{
		if(MsgRecv.is_break)
		{
			//sprintf( txt, "[%s]终结了[%s]的杀人如麻",MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z55,  MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
		}
		else
		{
			strSound = "MegaKill.wav";
			//sprintf( txt, "[%s]已经杀人如麻!", MsgRecv.who.c_str() );		
			strText = _TRAN(N_NOTICE_Z56,  MsgRecv.who.c_str() );
		}

	}

	if (MsgRecv.state & DOTA_UNSTOPPABLE)
	{
		if(MsgRecv.is_break)
		{
			//sprintf( txt, "[%s]终结了[%s]的无人能档!",MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z57,  MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
		}
		else
		{

			strSound = "Unstoppable.wav";
			//sprintf( txt, "[%s]已经无人能档!", MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z58,  MsgRecv.who.c_str() );
		}
	}

	if (MsgRecv.state & DOTA_WHICKEDSICK)
	{
		if(MsgRecv.is_break)
		{
			//sprintf( txt, "[%s]终结了[%s]的杀得变态!",MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z59,  MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
		}
		else
		{
			strSound = "WhickedSick.wav";
			//sprintf( txt, "[%s]杀得变态了!", MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z60,  MsgRecv.who.c_str() );
		}
	
	}

	if (MsgRecv.state & DOTA_MONSTERKILL)
	{
		if(MsgRecv.is_break)
		{
			//sprintf( txt, "[%s]终结了[%s]的妖怪般的杀戮!",MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z61,  MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
		}
		else
		{
			strSound = "monster_kill.wav";
			//sprintf( txt, "[%s]妖怪般的杀戮!", MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z62,  MsgRecv.who.c_str() );
		}

	}

	if (MsgRecv.state & DOTA_GODLIKE)
	{
		if(MsgRecv.is_break)
		{
			//sprintf( txt, "[%s]终结了[%s]的神一般的杀戮!",MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z63, MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
		}
		else
		{
			strSound = "GodLike.wav";
			//sprintf( txt, "[%s]如同神一般!", MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z64,  MsgRecv.who.c_str() );
		}
	}

	if (MsgRecv.state & DOTA_HOLYSHIT)
	{
		if(MsgRecv.is_break)
		{
			//sprintf( txt, "[%s]终结了[%s]的超越神的杀戮!",MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z65, MsgRecv.who_break.c_str(), MsgRecv.who.c_str() );
		}
		else
		{
			strSound = "HolyShit.wav";
			//sprintf( txt, "[%s]超越神了! 拜托谁去杀杀他吧!", MsgRecv.who.c_str() );
			strText = _TRAN(N_NOTICE_Z66,  MsgRecv.who.c_str() );
		}
	}
	strPlaySound.clear();
	if( !strSound.empty() )
		strPlaySound = strWhere + strSound;

	if (strText.length())
	{
		ChatMsgNotify(CHAT_MSG_SYSTEM, strText.c_str() );
	
		if (pSGN)
		{
			pSGN->GetSystemMsg( strText.c_str() );
		}
	}


	if( !strPlaySound.empty() )
	{
		ALAudioSource* pkSound = (ALAudioSource*)AudioSystem::GetAudioSystem()->CreateSource(AudioSource::TYPE_DEFAULT);
		if (!pkSound)
			return;

		pkSound->SetGain(1.0f);
		pkSound->SetLoopCount(1);
		pkSound->SetPlayTime(2.5f);
		SYState()->IAudio->PlaySoundByList(pkSound, strPlaySound.c_str());
	}
}

void CPacketParser::ParseShiFouBaiShi(CPacketUsn& pakTool)  //收到被老师招募的通知
{
	MSG_S2C::stRecruitNotify MsgRecv;
	pakTool>>MsgRecv;

	TeacherSystemMgr->ParseShiFouBaiShi(MsgRecv.who);

}
void CPacketParser::ParseTuDiFankui(CPacketUsn& pakTool)	   //回馈给老师的答复
{
	MSG_S2C::stRecruitAck  MsgRecv;
	pakTool>>MsgRecv;

	TeacherSystemMgr->ParseTuDiFankui(MsgRecv);

}

void CPacketParser::ParseLadderAck(CPacketUsn& pakTool)
{
	MSG_S2C::stLadderAck MsgRecv;
	pakTool>>MsgRecv;
	
	RankMgr->ParseRankData(MsgRecv.ladder_category ,MsgRecv.lv);
}


void CPacketParser::ParseTitleList(CPacketUsn& pakTool)
{
	MSG_S2C::stTitleList MsgRecv;
	pakTool>>MsgRecv;

	if (TitleMgr)
	{
		TitleMgr->ParseTitleList(MsgRecv);
	}
}
void CPacketParser::ParseAddTitle(CPacketUsn& pakTool)
{
	MSG_S2C::stTitleAdd MsgRecv;
	pakTool>>MsgRecv;

	if (TitleMgr)
	{
		TitleMgr->AddTitle(MsgRecv.title);
		
	}
}
void CPacketParser::ParseRemoveTitle(CPacketUsn& pakTool)
{
	MSG_S2C::stTitleRemove MsgRecv;
	pakTool>>MsgRecv;

	if (TitleMgr)
	{
		TitleMgr->RemoveTitle(MsgRecv.title.title);
	}
}

void CPacketParser::ParsestItemFieldRecordChange(CPacketUsn& pakTool)
{
	MSG_S2C::stOnItemFieldRecordChange MsgRecv;
	pakTool>>MsgRecv;

	unsigned int uiAreaId = MsgRecv.zoneid;
	const AreaInfo* pkAreaInfo = g_pkAreaDB->GetAreaInfo(uiAreaId);

	if(MsgRecv.entry == 5)//罗盘石
	{
		if(uiAreaId > 0 && pkAreaInfo)
		{
			std::string strRet = _TRAN(N_NOTICE_Z67, (const char*)pkAreaInfo->kName);
			ClientSystemNotify(strRet.c_str());//"已将 %s 记录为指定传送点。", (const char*)pkAreaInfo->kName);
		}
	}
	if(MsgRecv.entry == 11)//回城石
	{
		if(uiAreaId > 0 && pkAreaInfo)
		{
			std::string strRet = _TRAN(N_NOTICE_Z68, (const char*)pkAreaInfo->kName);
			ClientSystemNotify( strRet.c_str() );//"已将 %s 设置为你的家。", (const char*)pkAreaInfo->kName);
		}
	}
}

void CPacketParser::ParseQueryPlayerAck(CPacketUsn& pakTool)
{
	MSG_S2C::stQueryPlayersAck MsgRecv;
	pakTool>>MsgRecv;
	if(SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
		SocialitySys->GetOLPlayerQuerySysPtr()->ParseMsgQueryPlayersAck(MsgRecv.v);
	if (ChatSystem)
		ChatSystem->ParseQueryPlayer(MsgRecv.v);
}

void CPacketParser::ParseDonateAck(CPacketUsn& pakTool)
{
	MSG_S2C::stDonateAck MsgRecv;
	pakTool>>MsgRecv;

	EndowmentMgr->ParseDonationAck(MsgRecv.result);
}
void CPacketParser::ParseDonationLadderAck(CPacketUsn& pakTool)
{
	MSG_S2C::stDonationLadderAck MsgRecv;
	pakTool>>MsgRecv;

	EndowmentMgr->ParseDonationLadder(MsgRecv.total_query, MsgRecv.query_event_id, MsgRecv.local_query, MsgRecv.v);
}
void CPacketParser::ParseDonationEventList(CPacketUsn& pakTool)
{
	MSG_S2C::stDonationEventList MsgRecv;
	pakTool>>MsgRecv;

	EndowmentMgr->ParseDonationEventList(MsgRecv.v);
}
void CPacketParser::ParseDonationHistory(CPacketUsn& pakTool)
{
	MSG_S2C::stDonationHistory MsgRecv;
	pakTool>>MsgRecv;

	EndowmentMgr->ParseDonationHistory(MsgRecv.v);
}
void CPacketParser::ParsetAddDonateHistroy(CPacketUsn& pakTool)
{
	MSG_S2C::stAddDonateHistroy MsgRecv;
	pakTool>>MsgRecv;

	EndowmentMgr->ParseAddDonationHistory(MsgRecv.event_id, MsgRecv.datatime, MsgRecv.yuanbao);
}
//#include "ui/UCreditCard.h"
void CPacketParser::ParseCreditApply(CPacketUsn& pakTool)
{
	MSG_S2C::stCreditApplyAck MsgRecv;
	pakTool>>MsgRecv;

	//CreditMgr->ParseCreditApply(MsgRecv.result);
}
void CPacketParser::ParseCreditAction(CPacketUsn& pakTool)
{
	MSG_S2C::stCreditActionAck MsgRecv;
	pakTool>>MsgRecv;
	
	//CreditMgr->ParseCreditAction(MsgRecv.result, MsgRecv.is_restore);
}
void CPacketParser::ParseCreditHistory(CPacketUsn& pakTool)
{
	MSG_S2C::stCreditHistoryAck MsgRecv;
	pakTool>>MsgRecv;

	//CreditMgr->ParseCreditHistory(MsgRecv.v);
}


void CPacketParser::ParsePalySound(CPacketUsn& pakTool)
{
	MSG_S2C::stPlaySoundNotify MsgRecv;
	MsgRecv.LoopCount = 1;
	pakTool>>MsgRecv;
	char szFileName[256] = { 0 };
	sprintf( szFileName, "Sound\\PlayFromServer\\%s" ,MsgRecv.SourceFile.c_str()  );
	if ( MsgRecv.bMusic )
	{
		SYState()->IAudio->ChangeMapMusic( szFileName );
	}
	else
	{

		if ( MsgRecv.Sound3DBindObjectID )
		{
			CGameObject* pkObj = (CGameObject*)ObjectMgr->GetObject( MsgRecv.Sound3DBindObjectID );
			if ( pkObj )
			{
				ALAudioSource* pkSound = (ALAudioSource*)AudioSystem::GetAudioSystem()->CreateSource(AudioSource::TYPE_3D);
				if (!pkSound)
					return;
				pkObj->GetSceneNode()->AttachChild(pkSound);
				pkSound->SetMinMaxDistance(5.0f, 80.0f);
				pkSound->SetGain(2.4f);

				pkSound->SetLoopCount( MsgRecv.LoopCount );
				SYState()->IAudio->PlaySound(pkSound, szFileName );
				pkSound->SetPlayTime( 300.f );
			}
		}
		else
		{
			SYState()->IAudio->PlayUiSound( szFileName, NiPoint3::ZERO, 2.0f, MsgRecv.LoopCount );
		}
	}
	
}


void CPacketParser::ParseScreenShaking( CPacketUsn& pakTool )
{
	MSG_S2C::stScreenShake MsgRecv;
	pakTool >> MsgRecv;

	if ( MsgRecv.duration > 0 )
	{
		CPlayerLocal*  pkPlayer = ObjectMgr->GetLocalPlayer(); 
		if ( pkPlayer )
		{
			pkPlayer->GetCamera().Shaking(MsgRecv.duration);
		}
	}
}

void CPacketParser::ParseSerialForGift(CPacketUsn &pakTool)
{
	MSG_S2C::stSerial_for_gift_use MsgRecv;
	pakTool >> MsgRecv;

	UIAddMemberEdit* pEdit = INGAMEGETFRAME(UIAddMemberEdit, FRAME_IG_ADDMEMBERDLG);
	if (pEdit)
	{
		pEdit->OnAddMember(3);
	}
}
void CPacketParser::ParseSerialForGiftResult(CPacketUsn& pakTool)
{
	MSG_S2C::stSerial_for_gift_Result MsgRecv;
	pakTool >> MsgRecv;
	switch(MsgRecv.result)
	{
	case MSG_S2C::stSerial_for_gift_Result::RESULT_ACCEPT :
		return ;
	case MSG_S2C::stSerial_for_gift_Result::RESULT_NOT_FOUND : 
		{
			ChatSystem->AppendChatMsg(CHAT_MSG_SYSTEM, _TRAN("该序列号不存在!<br>"));
		}
		break ;
	case MSG_S2C::stSerial_for_gift_Result::RESULT_IS_USER: 
		{
			ChatSystem->AppendChatMsg(CHAT_MSG_SYSTEM, _TRAN("该序列号已经失效!<br>"));
		}
		break ; 
	case MSG_S2C::stSerial_for_gift_Result::RESULT_ERROR : 
		{
			//其他错误
			ChatSystem->AppendChatMsg(CHAT_MSG_SYSTEM, _TRAN("您使用过该功能，不能重复激活!<br>"));
		}
		break ; 
	}
}

void CPacketParser::ParseSceneEffectUpdate( CPacketUsn& pakTool )
{	
	MSG_S2C::stSceneEffect MsgRecv;
	pakTool >> MsgRecv;

	CSceneEffect* pEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect( MsgRecv.effect_file_name.c_str(),true, true, false, MsgRecv.loop_count );
	pEffect->SetEffScale(MsgRecv.scale);
	NiPoint3 ptEffect( MsgRecv.x, MsgRecv.y, MsgRecv.z);
	pEffect->AttachToScene( ptEffect );

	char szFileName[256] = { 0 };
	sprintf( szFileName, "Sound\\PlayFromServer\\%s" ,MsgRecv.sound_file_name.c_str() );

	pEffect->SetEffectSound( szFileName, MsgRecv.play_sound_delay );
	

}


void CPacketParser::ParseActivateAnimation( CPacketUsn& pakTool )
{
	MSG_S2C::stCreatureForcePlayAnimation MsgRecv;
	pakTool >> MsgRecv;

	CCharacter* pChar = (CCharacter*)ObjectMgr->GetObject( MsgRecv.CreatureID );
	if ( pChar )
	{
		if ( MsgRecv.AnimationName.length())
		{
			pChar->ActivateAnimation( MsgRecv.AnimationName.c_str(), 1, 2, 0.1f, NiKFMTool::SYNC_SEQUENCE_ID_NONE, false, true, true );
		}
		else
			pChar->ActivateAnimation( MsgRecv.AnimationID, 1, 2, 0.1f, NiKFMTool::SYNC_SEQUENCE_ID_NONE, false, true, true );
	}
}

void CPacketParser::ParseKnockBack( CPacketUsn& pakTool )
{
	MSG_S2C::stMove_Knock_Back MsgRecv;
	pakTool >> MsgRecv;

	CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject( MsgRecv.guid );

	if ( pkPlayer && !pkPlayer->IsDead() )
	{
		if( pkPlayer->IsLocalPlayer() )
		{
			SYState()->LocalPlayerInput->LockInput();
			((CPlayerLocal*)pkPlayer)->m_bAutoRun = true;

			if ( pkPlayer->IsJumping() )
			{
				pkPlayer->EndJump();
			}
			pkPlayer->SetMovementFlag( MOVEFLAG_JUMPING );
			pkPlayer->StartBackward();
			pkPlayer->SetNextState( STATE_MOVE, CUR_TIME );
			pkPlayer->SetDesiredAngle( MsgRecv.orientation );
			pkPlayer->AddStrickBackCount();
			pkPlayer->SetBackvVelocity();
		}
		else
		{
			if ( pkPlayer->IsJumping() )
			{
				//pkPlayer->SetPhysInitVelocity( vintVel );
				pkPlayer->SetBackvVelocity( MsgRecv.orientation );
			}
		}
		if ( MsgRecv.distance > 0.f )
		{
			pkPlayer->SetStrikeBackSpeed( MsgRecv.distance / 0.6 );
		}

		pkPlayer->SetIsStrikeBack( true );
		
		//pkPlayer->StartJump( true );
		/*if ( !pkPlayer->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM))*/
		pkPlayer->SetPhyFly( true );
		
	}			  
				 

}


void CPacketParser::ParseUnBindItem( CPacketUsn& pakTool )
{
	MSG_S2C::stUnbindItemAck MsgRecv;
	pakTool >> MsgRecv;

	switch ( MsgRecv.result )
	{
	case MSG_S2C::stUnbindItemAck::UNBIND_RESULT_SUCCEED:
		{
			EffectMgr->AddEffectText(_TRAN("您需要的物品已经成功解除绑定。"));
		}
		break;
	case MSG_S2C::stUnbindItemAck::UNBIND_RESULT_LEVEL:
		{
			EffectMgr->AddEffectText(_TRAN("解除绑定失败：物品等级不符合。"));
		}
		break;
	case MSG_S2C::stUnbindItemAck::UNBIND_RESULT_QUALITY:
		{
			EffectMgr->AddEffectText(_TRAN("解除绑定失败：物品品质不符合。"));
		}
		break;
	case MSG_S2C::stUnbindItemAck::UNBIND_RESULT_NEED_NOT:
		{
			EffectMgr->AddEffectText(_TRAN("物品不需要解除绑定。"));
		}
		break;
	case MSG_S2C::stUnbindItemAck::UNBIND_RESULT_NO_TARGET:
		{
			EffectMgr->AddEffectText(_TRAN("没有指定需要解除绑定的物品。"));
		}
		break;
	default:
		{
			EffectMgr->AddEffectText(_TRAN("解除绑定失败：未知错误。"));
		}
		break;
	}
}


void CPacketParser::ParseAppearTo( CPacketUsn& pakTool )
{
	MSG_S2C::stAppearItemAck MsgRecv;
	pakTool >> MsgRecv;

	switch ( MsgRecv.result )
	{
	case MSG_S2C::stAppearItemAck::APPEAR_RESULT_SUCCEED:
		{
			EffectMgr->AddEffectText(_TRAN("到达目的地。"));
		}
		break;
	case MSG_S2C::stAppearItemAck::APPEAR_RESULT_OFFLINE:
		{
			EffectMgr->AddEffectText(_TRAN("该玩家不在线。"));
		}
		break;
	case MSG_S2C::stAppearItemAck::APPEAR_RESULT_OTHER_RACE:
		{
			EffectMgr->AddEffectText(_TRAN("您不能传送到敌对玩家附近。"));
		}
		break;
	case MSG_S2C::stAppearItemAck::APPEAR_RESULT_INSTANCE:
		{
			EffectMgr->AddEffectText(_TRAN("该玩家正在副本中。"));
		}
		break;
	case MSG_S2C::stAppearItemAck::APPEAR_RESULT_WRONG_NAME:
		{
			EffectMgr->AddEffectText(_TRAN("没该玩家。"));
		}
		break;
	case MSG_S2C::stAppearItemAck::APPEAR_RESULT_WRONG_TELEPORTING:
		{
			EffectMgr->AddEffectText(_TRAN("错误的传送"));
		}
		break;
	default:
		{
			EffectMgr->AddEffectText(_TRAN("传送失败：未知错误。"));
		}
		break;
	}
}
#include "utils\CPlayerTitleDB.h"
void CPacketParser::ParseTitleBroadcast( CPacketUsn& pakTool)
{
	MSG_S2C::stTitleBroadcast MsgRecv;
	pakTool >> MsgRecv;

	//
	UISystemMsg* pSGN = INGAMEGETFRAME(UISystemMsg, FRAME_IG_SYSGIFTNOTIFY);
	if (pSGN)
	{
		TitleInfo* info = g_pkPlayerTitle ->GetTitleInfo(MsgRecv.TitleID);
		if(info)
		{
			//char buf[256];
			//sprintf(buf, "恭喜%s获得了%s称号", MsgRecv.Who.c_str(), info->stTitle.c_str());
			string str = _TRAN(N_NOTICE_C89, MsgRecv.Who.c_str(), info->stTitle.c_str() );
			pSGN->GetSystemMsg( str.c_str(), 4.0f);
			std::string who = "<a:#Name_"+MsgRecv.Who+">["+ MsgRecv.Who +"]</a>";
			str = _TRAN(N_NOTICE_C89, who.c_str(), info->stTitle.c_str() );
			ClientSystemNotify( str.c_str() );
		}
	}

}


void CPacketParser::ParseDisplayTextOnObjectHead(CPacketUsn& pakTool)
{
	MSG_S2C::stDisplayTextOnObjectHead MsgRecv;
	pakTool >> MsgRecv;

	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	CCharacter* pChar = (CCharacter*)ObjectMgr->GetObject(MsgRecv.guid);
	if (pChar && pLocal)
	{
		UStringW str;
		str.Set(MsgRecv.text.c_str());
		pChar->BubbleUpEffectNumber(0, BUBBLE_HEADTEXT, pLocal->GetGUID() == MsgRecv.self_id, str.GetBuffer());
	}
}


void CPacketParser::ParseUnLearnProfession(CPacketUsn& pakTool)
{
	MSG_S2C::stSkill_Notify_Unlearn MsgRecv;
	pakTool >> MsgRecv;
	CPlayerLocal* pPlayer = ObjectMgr->GetLocalPlayer();

	if ( pPlayer )
	{
		pPlayer->UnLearnProfession(MsgRecv.skill_id);
	}
	UISkill* pSkill = INGAMEGETFRAME(UISkill, FRAME_IG_SKILLDLG);
	if (pSkill)
	{
		pSkill->RefreshProf();
	}
}


void CPacketParser::ParseMonsterRunaway(CPacketUsn& pakTool)
{
	MSG_S2C::stMonsterFleeNotify MsgRecv;
	pakTool >> MsgRecv;

	CCreature* pkCreature = (CCreature*)ObjectMgr->GetObject(MsgRecv.monster_guid );

	std::string strbuf;

	if ( pkCreature )
	{
		strbuf = _TRAN(N_NOTICE_C90,pkCreature->GetName() );
		ClientSystemNotify( strbuf.c_str() );
	}
	//sprintf(MsgBuf, "%s已经被消灭了<br>", pkCreature->GetName() );

	
}

void CPacketParser::ParseInsertGemAck(CPacketUsn& pakTool)
{
	MSG_S2C::stInsertGemAck MsgRecv;
	pakTool >> MsgRecv;
	
	EquRefineMgr->ParseJingLianResult(JINGLIAN_RESULT_SUCCESS);
}


void CPacketParser::ParseGroupApplyForJoiningAck( CPacketUsn& pakTool)
{
	MSG_S2C::stGroupApplyForJoiningAck MsgRecv;
	pakTool >> MsgRecv;

	SocialitySys->GetApplyGroupSysPtr()->ParseGroupApplyForJoiningAck(MsgRecv.name, MsgRecv.type);
}
void CPacketParser::ParseGroupModifyTitleAck(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupModifyTitleAck MsgRecv;
	pakTool >> MsgRecv;

	SocialitySys->GetApplyGroupSysPtr()->ParseGroupModifyTitleAck(MsgRecv.Type);
}
void CPacketParser::ParseGroupModifyCanApplyForJoiningAck(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupModifyCanApplyForJoiningAck MsgRecv;
	pakTool >> MsgRecv;

	SocialitySys->GetApplyGroupSysPtr()->ParseGroupModifyCanApplyForJoiningAck(MsgRecv.Type);
}
void CPacketParser::ParseGroupListAck(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupListAck MsgRecv;
	pakTool >> MsgRecv;

	SocialitySys->GetApplyGroupSysPtr()->ParseGroupListAck(MsgRecv.vcGroup);
}

void CPacketParser::ParseGroupApplyForJoiningResult(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupApplyForJoiningResult MsgRecv;
	pakTool >> MsgRecv;

	SocialitySys->GetApplyGroupSysPtr()->ParseGroupApplyForJoiningResult(MsgRecv.name, MsgRecv.Type);
}

void CPacketParser::ParseGroupApplyForJoining(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupApplyForJoining MsgRecv;
	pakTool >> MsgRecv;

	SocialitySys->GetApplyGroupSysPtr()->ParseGroupApplyForJoining(MsgRecv.name, MsgRecv.classMask, MsgRecv.Level, MsgRecv.Sex);
}

void CPacketParser::ParsestGroupApplyForJoiningStateAck(CPacketUsn& pakTool)
{
	MSG_S2C::stGroupApplyForJoiningStateAck MsgRecv;
	pakTool >> MsgRecv;
	SocialitySys->GetApplyGroupSysPtr()->ParsestGroupApplyForJoiningStateAck(MsgRecv.bOpen);
}


void OnAgreeSummon()
{
	PacketBuilder->SendAgreeSummon();
}

void CPacketParser::ParseSummon(CPacketUsn& pakTool)
{
	MSG_S2C::stSummon_Request MsgRecv;
	pakTool >> MsgRecv;

	CMapInfoDB::MapInfoEntry stMapInfoEntry;
	if (!g_pkMapInfoDB->GetMapInfo(MsgRecv.mapid, stMapInfoEntry))
	{
		return;
	}

	float dutime = MsgRecv.duration/1000.f; 
	UMessageBox::MsgBox( _TRAN( N_NOTICE_C94, MsgRecv.Requestor.c_str(), stMapInfoEntry.name.c_str() ).c_str(),  (BoxFunction*)OnAgreeSummon, NULL, dutime );

}

void CPacketParser::ParseSpellCancelAutoRepeat(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Cancel_Auto_Repeat MsgRecv;
	pakTool>>MsgRecv;

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		pkLocal->StopAutoCastSpell(MsgRecv.spell_id);
	}
}

void CPacketParser::ParseSpellBeginAutoRepeat(CPacketUsn& pakTool)
{
	MSG_S2C::stSpell_Begin_AutoRepeat MsgRecv;
	pakTool>>MsgRecv;

 	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		pkLocal->AddAutoCastSpell(MsgRecv.spell_id);
	}
}