#pragma once

class CPacketParser : public NiMemObject
{
	struct OpcodeHandler
	{
		OpcodeHandler() : handler(NULL) {};
		OpcodeHandler( void (CPacketParser::*_handler)(CPacketUsn& pakTool) ) : handler(_handler) {};

		void (CPacketParser::*handler)(CPacketUsn& pakTool);
	};
private:
	OpcodeHandler m_opcodeTable[MSG_S2C::NUM_MSG_TYPES];
public:
	CPacketParser();

	void Init();
	bool ProcessPacket(char* pcBuffer, int iLen);

	// 人物基本属性
	void ParseQueryNameResMsg(CPacketUsn& pakTool);

	//登录服务器的消息
	void ParseVersionMsg(CPacketUsn& pakTool);
	void ParseLoginMsg(CPacketUsn& pakTool); 
	void ParseReturnToLogin(CPacketUsn& pakTool); //收到返回服务器消息
	void ParseGroupListMsg(CPacketUsn& pakTool);


	//游戏服务器的消息(GateServer)
	void ParseGTLoginMsg(CPacketUsn& pakTool);
	void ParseRoleListMsg(CPacketUsn& pakTool);
	void ParseCreateRoleMsg(CPacketUsn& pakTool);
	void ParseDeleteRoleMsg(CPacketUsn& pakTool);
	void ParseEnterGameNum(CPacketUsn& pakTool);
	void ParseEnterGameWaitOK(CPacketUsn& pakTool);

	void ParseEnterGameMsg(CPacketUsn& pakTool);
	void ParseMsgBoxFormServer(CPacketUsn& pakTool);
	void ParseMoveNewWorld(CPacketUsn& pakTool);
	void ParseTransferAbouted(CPacketUsn& pakTool);


	void ParsePlayerMoveMsg(CPacketUsn& pakTool);
	void ParseMonsterMoveMsg(CPacketUsn& pakTool);
	void ParseMoveForceRoot(CPacketUsn& pakTool);
	void ParseMoveForceUnRoot(CPacketUsn& pakTool);
	
	void ParseDeathMsg(CPacketUsn& pakTool);
	void ParseReliveAckMsg(CPacketUsn& pakTool);
	void ParseReliveMsg(CPacketUsn& pakTool);
	void ParseSpellResurrectReq(CPacketUsn& pakTool);

	void ParseItemAddToContainerMsg(CPacketUsn& pakTool);
	void ParseItemRemoveFromContainerMsg(CPacketUsn& pakTool);
	void ParseItemMovePosMsg(CPacketUsn& pakTool);

	void ParseItemContainerMsg(CPacketUsn& pakTool);

	void ParseAvatarMsg(CPacketUsn& pakTool);

	void ParseMoneyModifyMsg(CPacketUsn& pakTool);

	void ParsePlayerRebirth(CPacketUsn& pakTool);

	//void ParseNpcMsg(CPacketUsn& pakTool); //message from the NPC
	

	void ParseItemModifyNumContainerMsg(CPacketUsn& pakTool);


	//////////////////////////////////////////////////////////////////////////
	//	New Msg
	//////////////////////////////////////////////////////////////////////////
	void ParseObjectUpdate(CPacketUsn& pakTool);
	void ParseAccountExpireInFewMinutes(CPacketUsn& pakTool);

	void ParseExpGain(CPacketUsn& pakTool);
	/////////////////////////////////////////////////////////////////////////
	////////////////           NPC BANK            //////////////////////////
	//////////////////////////////////////////////////////////////////////////
	void ParseShowBank(CPacketUsn& pakTool);
//////////////////////////////////////////////////////////////////////////
//	Item
//////////////////////////////////////////////////////////////////////////
	void ParseLevelUpInfo(CPacketUsn& pakTool);
	void ParseSpeedChange(CPacketUsn& pakTool);

	void ParseItemInventoryMsg(CPacketUsn& pakTool);
	void ParseItemBuyMsg(CPacketUsn& pakTool);
	void ParseItemBuyFailureMsg(CPacketUsn& pakTool);
	void ParseItemSellFailureMsg(CPacketUsn& pakTool);
	void ParseItemInventoryChangeFailureMsg(CPacketUsn& pakTool);
	void ParseItemSellMsg(CPacketUsn& pakTool);
	void ParseItemReadOkMsg(CPacketUsn& pakTool);
	void ParseItemQuerySingleResponseMsg(CPacketUsn& pakTool);
	void ParseItemPushResultMsg(CPacketUsn& pakTool);
	void ParseItemNameQueryResponseMsg(CPacketUsn& pakTool);
	void ParseItemTimeUpdateMsg(CPacketUsn& pakTool);
	void ParseItemEnchantTimeUpdateMsg(CPacketUsn& pakTool);
	void ParseItemEnchantmentLogMsg(CPacketUsn& pakTool);

	//	错误提示
	void ParseItemSwapFailure(CPacketUsn& pakTool);
//////////////////////////////////////////////////////////////////////////

	// Player Trade
	void ParseTrade_StateMsg(CPacketUsn& pakTool);
	void ParseTrade_State_ExtMsg(CPacketUsn& pakTool);

	//
	void ParseDestroyObjMsg(CPacketUsn& pakTool);

	void ParseAttackUpdate(CPacketUsn& pakTool);
	void ParseAttackFail_NotInRange(CPacketUsn& pakTool);
	void ParseAttackFail_BadFacing(CPacketUsn& pakTool);
	void ParseAttackStart(CPacketUsn& pakTool);
	void ParseAttackStop(CPacketUsn& pakTool);

	void ParseKillLog(CPacketUsn& pakTool);

	void ParseHealHP(CPacketUsn& pakTool);// HP治疗
	void ParseHeadMP(CPacketUsn& pakTool);// MP治疗
	void ParseNoMeleeDamage(CPacketUsn& pakTool);// 法术伤害
	void ParseAuraPeriodicDamage(CPacketUsn& pakTool);// debuf 伤害
	void ParseEnvironmentalDamage(CPacketUsn& pakTool);// 环境伤害
	void ParseSpellMiss(CPacketUsn& pakTool);// 技能miss
	void ParseCCResult(CPacketUsn& pakTool);

	void ParseSpellCastResult(CPacketUsn& pakTool);// 释放结果,定义在SpellDef.h
	void ParseSpellFailure(CPacketUsn& pakTool);// 技能打断
	void ParseSpellStart(CPacketUsn& pakTool);// 服务器开始处理技能开始,之后有可能失败
	void ParseSpellGo(CPacketUsn& pakTool);// 技能生效
	void ParseSpellClearCD(CPacketUsn& pakTool);

	void ParseSpellDelay(CPacketUsn& pakTool);//技能延迟

	void ParseSpellCastSuccess(CPacketUsn& pakTool);//受害者收到的消息,对方释放技能成功

	void ParseChannelSpellStart(CPacketUsn& pakTool);// 技能开始吟唱
	void ParseChannelSpellUpdate(CPacketUsn& pakTool);// 技能吟唱更新
	
	void ParseActionButtons(CPacketUsn& pakTool);//	技能栏快捷按钮
	void ParseUpdateActionButtons(CPacketUsn& pakTool);
	void ParseSpellInit(CPacketUsn& pakTool);//技能初始化
	void ParseSpellLearn(CPacketUsn& pakTool);
	void ParseSpellRemove(CPacketUsn& pakTool);
	void ParseSpellNPCTrainerList(CPacketUsn& pakTool);
	void ParseSpellSetFlatModifier(CPacketUsn& pakTool);
//////////////////////////////////////////////////////////////////////////
// 任务相关
//////////////////////////////////////////////////////////////////////////
	// 服务器返回选项列表
	void ParseNpcGossipMessageMsg(CPacketUsn& pakTool);
	// 收到任务说明
	void ParseQuestGiverQuestDetailsMsg(CPacketUsn& pakTool);
	// 任务奖励列表
	void ParseQuestGiverOfferRewardMsg(CPacketUsn& pakTool);
	// 任务完成所需条件
	void ParseQuestGiverRequestItems(CPacketUsn& pakTool);
	void ParseQuestGiverStatusQuery(CPacketUsn& pakTool);
	void ParseQuestGiverInRangeQuery(CPacketUsn& pakTool);
	// 任务日志完成刷新
	void ParseQuestUpdateComplete(CPacketUsn& pakTool);
	// 已杀死 name count/tcount
	void ParseQuestUpdateAddSkill(CPacketUsn& pakTool);
	// 已拾取 name count/tcount
	void ParseQuestUpdateAddItem(CPacketUsn& pakTool);
	// 任务完成奖励 金币,经验,道具
	void ParseQuestGiverQuestComplete(CPacketUsn& pakTool);
	// 任务失败
	void ParseQuestGiverQuestFailed(CPacketUsn& pakTool);
	void ParseaAeccpetNewQuest(CPacketUsn& pakTool);
	void ParseQuestFull(CPacketUsn& pakTool);
	// 完成任务列表
	void ParseFinishedQuests(CPacketUsn& pakTool);

	//组队护送任务
	void ParseQuestEscortNotify(CPacketUsn& pakTool);

///////////////////////////////////////////////////////////////////////
//  组队相关
///////////////////////////////////////////////////////////////////////
	//对方的邀请
	void ParseGroupInviteMsg(CPacketUsn& pakTool);
	//拒绝邀请
	void ParseGroupRefuseMsg(CPacketUsn& pakTool);
	//被踢掉
	void ParseGroupUnInviteMsg(CPacketUsn& pakTool);
	//队长更改
	void ParseGroupLeaderChanged(CPacketUsn& pakTool);
	//队伍解散
	void ParseGroupDestory(CPacketUsn& pakTool);
	//成员列表
	void ParseGroupList(CPacketUsn & pakTool);
	//添加成员
	void ParseAddMember(CPacketUsn & pakTool);
	//踢出成员
	void ParseRemoveMember(CPacketUsn & pakTool);
	//团队成员就位更新
	void ParseGroupMemberReady(CPacketUsn& pakTool);
	//团队内成员位置互换
	void ParseGroupMemberSwep(CPacketUsn& pakTool);
	//团队内成员位置更改
	void ParseMemberSubGroupChange(CPacketUsn& pakTool);
	//ICON变化
	void ParseGroupMemberIcon(CPacketUsn& pakTool);
	////队伍成员状态更新
	void ParseGroupMemberStatUpData(CPacketUsn & pakTool);
	//
	void ParseGroupCommandResult(CPacketUsn & pakTool);

	void ParseLootMethod(CPacketUsn & pakTool);
////////////////////////////////////////////////////////////////
//	拾取相关
////////////////////////////////////////////////////////////////
	void ParseLootList(CPacketUsn& pakTool);		//	受到拾取道具列表
	void ParseLootRemove(CPacketUsn& pakTool);		//	去掉一个待拾取道具
	void ParseLootMoney(CPacketUsn& pakTool);		//	拾取的金币数量
	void ParseLootMoneyClear(CPacketUsn& pakTool);	//	列表中不显示钱了
	void ParseToRollItem(CPacketUsn& pakTool);		//	发送要lootrool的道具,客户端收到后显示骰子
	void ParseRandomRoll(CPacketUsn& pakTool);		//	随机rool的结果
	void ParseRolledItem(CPacketUsn& pakTool);		//	别人roll得结果,只需提示
	void ParsestRoolWon(CPacketUsn& pakTool); //谁ROLL得了物品
	////////////////////////////////////////////////////////////////
	//	聊天相关
	////////////////////////////////////////////////////////////////
	void ParseChatAckMsg(CPacketUsn& pakTool);
	void ParseChatMsg(CPacketUsn& pakTool);
	void ParseSystemMsg(CPacketUsn& pakTool);
	void ParseNotFoundPlayer(CPacketUsn& pakTool);
	void ParseSystemNotify(CPacketUsn& pakTool);

	void ParseMoveTeleport(CPacketUsn& pakTool);

//////////////////////////////////////////////////////////////////////////
// 好友相关
//////////////////////////////////////////////////////////////////////////
	// 好友上下线通知
	void ParseFriendStatMsg(CPacketUsn& pakTool);
	// 好友列表
	void ParseFriendListMsg(CPacketUsn& pakTool);
//////////////////////////////////////////////////////////////////////////
// 拍卖相关
//////////////////////////////////////////////////////////////////////////
	// 拍卖操作结果
	void ParseAuctionCommandResultMsg(CPacketUsn& pakTool);
	// 查询列表
	void ParseAuctionListResultMsg(CPacketUsn& pakTool);
	// 返回出价最高竞拍的玩家
	void ParseAuctionBidderNotifyMsg(CPacketUsn& pakTool);
	//
	void ParseAuctionOwnerNotifyMsg(CPacketUsn& pakTool);
	// 玩家购买道具列表
	void ParseAuctionBidderListResultMsg(CPacketUsn& pakTool);
	//自己出售
	void ParseAuctionOwnerListResultMsg(CPacketUsn& pakTool);
	//
	void ParseAuctionHelloMsg(CPacketUsn& pakTool);

//////////////////////////////////////////////////////////////////////////
//////////////////////////////   邮件相关  ///////////////////////////////
//////////////////////////////////////////////////////////////////////////

	//查询是否有未读邮件
	void ParseNeedReadMail(CPacketUsn& pakTool);
	//查询邮件文字内容回馈
	void ParseMailContentMsg(CPacketUsn& pakTool); 
	//收到新邮件
	void ParseReceiveMail(CPacketUsn& pakTool);
	//邮件列表信息
	void ParseMailListMsg(CPacketUsn& pakTool);
	//发送邮件的结果返回
	void ParseSendMailResult(CPacketUsn& pakTool);
//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------
	void ParseAuraDuration(CPacketUsn& pakTool);
	void ParseAuraSingle(CPacketUsn& pakTool);
	//--------------------------------------------------------------------
	//
	//--------------------------------------------------------------------
	void ParseChannelNotify(CPacketUsn& pakTool);

//////////////////////////////////////////////////////////////////////////
// 骑人
//////////////////////////////////////////////////////////////////////////
	//被骑的人邀请骑乘的人
	void ParseMountInviteMsg(CPacketUsn& pakTool);
	//解散队伍
	void ParseMountDisbandMsg(CPacketUsn& pakTool);
	//同意邀请
	void ParseMountAcceptMsg(CPacketUsn& pakTool);
	//拒绝邀请
	void ParseMountDeclineMsg(CPacketUsn& pakTool);
	//回复
	void ParseMountResultMsg(CPacketUsn& pakTool);
	//////////////////////////////////////////////////////////////////////////
	// 工会
	//////////////////////////////////////////////////////////////////////////
	//被邀请加入工会
	void ParseGuildInviteMsg(CPacketUsn& pakTool);
	//对方拒绝你的邀请
	void ParseGuildDeclineMsg(CPacketUsn& pakTool);
	//工会简介
	void ParseGuildInfoMsg(CPacketUsn& pakTool);
	//对应查询工会的回复
	void ParseGuildQueryMsg(CPacketUsn& pakTool);
	//工会所有信息
	void ParseGuildRosterMsg(CPacketUsn& pakTool);
	//创建工会界面
	void ParseGuildCreateMsg(CPacketUsn& pakTool);
	//工会事件消息
	void ParseGuildEvent(CPacketUsn& pakTool);
	//工会事件错误
	void ParseGuildCommandResult(CPacketUsn& pakTool);
	//
	void ParseNotifyMsg(CPacketUsn& pakTool);
	//
	void ParseQureyGuildsList(CPacketUsn& pakTool);
	//
	void ParseGuildContributionPointsNotify(CPacketUsn& pakTool);
	//
	void ParseGuildDeclareWarNotify(CPacketUsn& pakTool);
	//
	void ParseCastleNPCStateNotify(CPacketUsn& pakTool);
	//
	void ParseCastleStateAck(CPacketUsn& pakTool);
	//
	void ParseCastleCountDown(CPacketUsn& pakTool);
	//部族升级
	void ParseGuildLevelUp(CPacketUsn& pakTool);
	//战场
	//战场战斗力更新消息
	void ParseBattleGroundPowerUpdate(CPacketUsn& pakTool);

	void ParseBattleGroundResourceUpdate(CPacketUsn& pakTool);

	void ParseBattleGroundDetailInformation(CPacketUsn& pakTool);
	////////////////////////////////////////////////////////////////////////
	// 新手帮助
	////////////////////////////////////////////////////////////////////////
	void ParseFreshManHelpFlagMsg(CPacketUsn& pakTool);
	////////////////////////////////////////////////////////////////////////
	// 道具商城
	////////////////////////////////////////////////////////////////////////
	void ParseShopItemListMsg(CPacketUsn& pakTool);
	void ParseShopActivityUpdate(CPacketUsn& pakTool);
	void ParsePongMsg(CPacketUsn& pakTool);

	void ParseCopyName(CPacketUsn& pakTool);
	void ParseHideGM(CPacketUsn& pakTool);

	
	//////////////////////////////////////////////////////////////////////////
	//副本
	//////////////////////////////////////////////////////////////////////////
	void ParseQueueInstanceUpdate(CPacketUsn& pakTool);
	void ParseEnterInstanceCountDown(CPacketUsn& pakTool);
	void ParseArenaUpdateState(CPacketUsn& pakTool);
	void ParseInstanceBillBoard(CPacketUsn& pakTool);
	void ParsePlayerLeaveInstance(CPacketUsn& pakTool);
	void ParsePlayerEnterInstance(CPacketUsn& pakTool);
	void ParseSunyouInstanceList(CPacketUsn& pakTool);

    void ParseChoiceDeadExit(CPacketUsn& pakTool);

	void ParseSystemGift(CPacketUsn& pakTool);
	void ParseSystemGiftMsg(CPacketUsn& pakTool);

	void ParseAISwitchMove(CPacketUsn& pakTool);


	void ParseSystemMoveMsg(CPacketUsn& pakTool);
	void ParseSystemMoveAddMsg(CPacketUsn& pakTool);
	void ParseSystemMoveRemoveMsg(CPacketUsn& pakTool);


	//////////////////////////////////////////////////////////////////////////
	//	捐助 Old = =
	//////////////////////////////////////////////////////////////////////////
	void ParseContributionConfirmResult(CPacketUsn& pakTool);
	void ParseSelfContributionHistoryResponse(CPacketUsn& pakTool);
	void ParseContributionBillboardResponse(CPacketUsn& pakTool);
	void ParseContributionQueryResult(CPacketUsn& pakTool);


	//////////////////////////////////////////////////////////////////////////
	// 装备打造与合成
	//////////////////////////////////////////////////////////////////////////

	void ParseJingLianResult(CPacketUsn& pakTool);
	void ParseXiangQianResult(CPacketUsn& pakTool);
	//////////////////////////////////////////////////////////////////////////
	// 宠物相关
	//////////////////////////////////////////////////////////////////////////
	void ParsePetStableResult(CPacketUsn& pakTool);
	void ParsePetSpell(CPacketUsn& pakTool);
	void ParsePetActionSound(CPacketUsn& pakTool);
	void ParsePetNameQueryResponse(CPacketUsn& pakTool);
	void ParsePetListStabled(CPacketUsn& pakTool);
	void ParsePetUnLearnConfirm(CPacketUsn& pakTool);
	void ParseServerTiem(CPacketUsn& pakTool);
	void ParsePetRenameAck(CPacketUsn& pakTool);
//////////////////////////////////////////////////////////////////////////
	void ParseBattleKill(CPacketUsn& paktool);

	//////////////////////////////////////////////////////////////////////////
	////  拜师系统														//////
	//////////////////////////////////////////////////////////////////////////
	
	void ParseShiFouBaiShi(CPacketUsn& pakTool);  //收到被老师招募的通知
	void ParseTuDiFankui(CPacketUsn& pakTool);	   //回馈给老师的答复

	//////////////////////////////////////////////////////////////////////////
	//// 排行榜
	//////////////////////////////////////////////////////////////////////////

	void ParseLadderAck(CPacketUsn& pakTool);

	// 称谓
	void ParseTitleList(CPacketUsn& pakTool);
	void ParseAddTitle(CPacketUsn& pakTool);
	void ParseRemoveTitle(CPacketUsn& pakTool);
	//
	void ParsestItemFieldRecordChange(CPacketUsn& pakTool);

	//在线查询
	void ParseQueryPlayerAck(CPacketUsn& pakTool);

	//////////////////////////////////////////////////////////////////////////
	//	捐助 New = =
	//////////////////////////////////////////////////////////////////////////
	void ParseDonateAck(CPacketUsn& pakTool);
	void ParseDonationLadderAck(CPacketUsn& pakTool);
	void ParseDonationEventList(CPacketUsn& pakTool);
	void ParseDonationHistory(CPacketUsn& pakTool);
	void ParsetAddDonateHistroy(CPacketUsn& pakTool);

	//////////////////////////////////////////////////////////////////////////
	//	信用卡
	//////////////////////////////////////////////////////////////////////////
	void ParseCreditApply(CPacketUsn& pakTool);
	void ParseCreditAction(CPacketUsn& pakTool);
	void ParseCreditHistory(CPacketUsn& pakTool);

	/////////////////////////////////////////////////////////////////////////
	//声效
	/////////////////////////////////////////////////////////////////////////
	void ParsePalySound(CPacketUsn& pakTool);
	void ParseScreenShaking( CPacketUsn& pakTool );
	void ParseSceneEffectUpdate( CPacketUsn& pakTool );

	//////////////////////////////////////////////////////////////////////////
	//激活码奖励
	//////////////////////////////////////////////////////////////////////////
	
	void ParseSerialForGift(CPacketUsn& pakTool);
	void ParseSerialForGiftResult(CPacketUsn& pakTool);

	/////////////////////////////////////////////////////////////////////////
	//动画
	/////////////////////////////////////////////////////////////////////////
	void ParseActivateAnimation( CPacketUsn& pakTool );
	void ParseKnockBack( CPacketUsn& pakTool );

	/////////////////////////////////////////////////////////////////////////
	//解绑
	/////////////////////////////////////////////////////////////////////////
	void ParseUnBindItem( CPacketUsn& pakTool );

	void ParseAppearTo( CPacketUsn& pakTool );

	void ParseTitleBroadcast( CPacketUsn& pakTool);

	void ParseDisplayTextOnObjectHead(CPacketUsn& pakTool);

	void ParseUnLearnProfession(CPacketUsn& pakTool);

	void ParseMonsterRunaway(CPacketUsn& pakTool);

	void ParseInsertGemAck(CPacketUsn& pakTool);
	/////////////////////////////////////////////////////////////////////////
	//组队申请
	/////////////////////////////////////////////////////////////////////////
	void ParseGroupApplyForJoiningAck( CPacketUsn& pakTool);
	void ParseGroupModifyTitleAck(CPacketUsn& pakTool);
	void ParseGroupModifyCanApplyForJoiningAck(CPacketUsn& pakTool);
	void ParseGroupListAck(CPacketUsn& pakTool);
	void ParseGroupApplyForJoiningResult(CPacketUsn& pakTool);
	void ParseGroupApplyForJoining(CPacketUsn& pakTool);
	void ParsestGroupApplyForJoiningStateAck(CPacketUsn& pakTool);

	/////////////////////////////////////////////////////////////////////////
	void ParseSummon(CPacketUsn& pakTool);

	void ParseSpellCancelAutoRepeat(CPacketUsn& pakTool);
	void ParseSpellBeginAutoRepeat(CPacketUsn& pakTool);
};