#pragma once
#include "Player.h"
#include "LocalPlayer.h"
#include "Creature.h"
#include "SceneItem.h"
#include "SceneGameObj.h"
#include "DynamicObject.h"

#include "Utils/CyclicBuffer.h"
#include <hash_set>

#define MAX_OBJECT (1024*256)

#define INVALID_OBJECT_ID (0xFFFFFFFF)

class SYItem;
class SYItemBag;

typedef stdext::hash_map<ui64, UpdateObj*> ObjectMap;
typedef stdext::hash_map<string, UpdateObj*> ObjectMapName;
typedef std::map<ui64, ui64> IDMAP;

struct sObjBaseInfo
{
	ui64 guid;
	string name;
	ui32 race;
	ui32 gender;
	ui32 Class;
};

typedef stdext::hash_map<ui64, sObjBaseInfo> ObjBaseMap;

class CObjectManager : public NiRefObject
{
public:
	enum IDGroup
	{
		IDT_CHAR = 0,
		IDT_LOCALPLAYER,
		IDT_ITEM,
		IDT_CORPSE,
		IDT_SCENEOBJECT,

		IDT_NUM,
	};

private:
	ObjectMap m_ObjectMap;
	

	IDMAP m_IDMAP[IDT_NUM];

	ui64 m_uiLocalPlayerID;
	CPlayerLocal* m_pkLocalPlayer;

	ObjBaseMap m_ObjBaseMap;

    struct pickentry
    {
        ui64 id;
        int weight;
        CGameObject* obj;
        NiPoint3 penter;
        float enter;

        bool operator<(pickentry& entry) {
            return bool(enter < entry.enter);
        }
    };

    std::set<pickentry> m_leastPickSet;
    CyclicBuffer<pickentry, 5> m_leastPickList;

    ObjectMap m_ClientObjectMap;
	stdext::hash_set<UpdateObj*> m_setObjectByType[TYPEID_UNUSED];
    ui64 m_ClientObjectGuid;

	NiPick	m_kObjectPicker;
	NiPick  m_kBoundPicker;

public:
	CObjectManager();
	virtual ~CObjectManager();

	bool AddObject(UpdateObj* pkObject);
	void AddToIDList(UpdateObj* pkObject);
	int GetIDT(UpdateObj* pkObject);

	void RemoveGroupObjects(IDGroup eGroupType);
	const ObjectMap& GetObjectMap() {
		return m_ObjectMap;
	}

#ifdef _DEBUG
	void PrintObjectList();
#endif

	void RemoveAll();
	void RemoveObject(ui64 guid);
	UpdateObj* GetObject(ui64 guid);
	UpdateObj* GetObjectByName(const NiFixedString& kName);
	UpdateObj* GetObjectByEntry(ui8 type, ui32 entry);
	const stdext::hash_set<UpdateObj*>& GetObjectByType(ui8 type) const;

	//////////////////////////////////////////////////////////////////////////

	//创建一个玩家角色  
	// param : bLocSelf 是否创建本地玩家.
	CPlayer* CreatePlayer(ui64 guid, BOOL bLocSelf = FALSE, bool bInWorld = true);
	CPlayer* GetPlayer(ui64 guid);

	CPlayerLocal* GetLocalPlayer() { return m_pkLocalPlayer; }
	void SetLocalPlayer(CPlayerLocal* pkPlayer);

	CCreature* CreateCreature(ui64 guid);

	CSceneItem* CreateSceneItem(ui64 guid, bool isContainer);
	CSceneGameObj* CreateSceneGameObj(ui64 guid);
	SYItem* CreateSYItem(ui64 guid);
	CDynamicObj* CreateDynamicObj(ui64 guid);
	SYItemBag* CreateSYBag(ui64 guid);

	void AddObjBaseInfo(ui64 guid, sObjBaseInfo baseInfo);

	sObjBaseInfo* GetObjBaseInfo(ui64 guid);
	CGameObject* PickObject(const NiPoint3& ptOrig, const NiPoint3& vDir, NiPoint3& ptPickPos, bool first = false, bool bLoot = false,int iIncWeight = 0);
	CGameObject* FindNearByObj(CGameObject* pkLocalPlayer, float fRadius, UINT Groups, float& fDistance);

    CPlayer* CreateClientPlayer();
    CPlayer* GetClientPlayer(ui64 guid);
    void RemoveClientPlayer(ui64 guid);

    //生成客户端GUID
    ui64 GenClientGuid() { return ++m_ClientObjectGuid; }
};

