#pragma once

class CPackAudioManager;
class CPackFile : public NiFile
{
    NiDeclareDerivedBinaryStream();

public:
	CPackFile(const char* pcName, unsigned int uiBufferSize = 32768, bool bAudio = false );
	virtual ~CPackFile(void);

	virtual void Seek(int iOffset, int iWhence);

	virtual unsigned int GetFileSize() const;

    virtual void SetEndianSwap(bool bDoSwap);

    static NiFile* CreateFile(const char* pcName, NiFile::OpenMode eMode, unsigned int uiBufferSize);
    static bool AccessFile(const char* pcName, NiFile::OpenMode eMode );
	static bool AccessAudioFile(const char* pcName, NiFile::OpenMode eMode );

    static void Init( bool bAudio );
    static void Shutdown( bool bAudio );

protected:
	// Read or write a chunk of data from a file (no endian swapping)
	unsigned int FileRead(void* pvBuffer, unsigned int uiBytes);
	unsigned int FileWrite(const void* pvBuffer, unsigned int uiBytes);

    unsigned int DiskRead(void* pvBuffer, unsigned int uiBytes);

    bool Flush();

private:
    HANDLE m_hFile;
	bool m_bAudio;

};