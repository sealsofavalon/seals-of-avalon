#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "Map/PathNode.h"

struct IPathFinder
{

};

class AStarNode;

class CPathFinder
{
public:
	CPathFinder();
	~CPathFinder();

	enum ePathType
	{
		Normal = 0,
		Straight,
		Smooth,
	};

	bool GeneratePath(int iSX, int iSY,int iSZ, int iDX, int iDY, int iDZ);
	bool GeneratePathWP(int iSX, int iSY, int iSZ, int iDX, int iDY, int iDZ);

	NiPoint3 GetNextPathPoint();

	//
	bool GetPathPoint(std::deque<NiPoint3>& PathPoint);
	//


	void ClearPathPoint();	// For StopMove 

	bool IsPathPointEmpty();

	void CheckDestValid(int iSX, int iSY, int& iDX, int& iDY);
protected:
	int m_iSX, m_iSY, m_iDX, m_iDY, m_iSZ, m_iDZ;
	ePathType m_PathType;

	std::deque<NiPoint3> m_PathPoint;

	AStarNode* m_pkOpen;
	AStarNode* m_pkClosed;
	AStarNode* m_pkBest;

	int Step();
	int StepWP(CPathNode* pkPath);
	void StepInitialize(int iSX, int iSY, int iDX, int iDY);
	void StepInitializeWP(int iSX, int iSY, int iSZ, int iDX, int iDY, int iDZ);

	void AddToOpen(AStarNode* pkAddNode);
	void ClearNodes();
	void CreateChildren(AStarNode* pkNode);
	void CreateChildrenWP(AStarNode* pkNode, CPathNode* pkPathNode);
	void LinkChild(AStarNode* pkNode, AStarNode* pkTemp);
	void LinkChildWP(AStarNode* pkNode, AStarNode* pkTemp, float fLength, CPathNode* pkPath);

	AStarNode* CheckList(AStarNode* pkNode, int iX, int iY);
	AStarNode* GetBest();

	int GetDir(NiPoint3 k0, NiPoint3 k1);

	void ProcessPath();
	void ProcessPathNormal();
	void ProcessPathStraight();
	void ProcessPathSmooth();
};

class AStarNode
{
public:
	AStarNode(int iX = -1, int iY = -1, int iZ = -1)
		:m_iX(iX)
		,m_iY(iY)
		,m_iZ(iZ)
		,m_uiNumChildren(0)
	{
		m_pkParent = m_pkNext = NULL;
		memset(m_pkChildren, 0, sizeof(m_pkChildren));
	}

	AStarNode* GetChild(unsigned int uiChildIdx) const
	{
		return m_pkChildren[uiChildIdx];
	}

	void AddChild(AStarNode* pkChild)
	{
		m_pkChildren[m_uiNumChildren++] = pkChild;
		NIASSERT(m_uiNumChildren < 20);
	}

	void SetHValue(float fH) { m_fH = fH; }
	void SetGValue(float fG) { m_fG = fG; }
	float GetH() { return m_fH; }
	float GetG() { return m_fG; }

	void UpdateFValue() { m_fF = m_fH + m_fG; }
	float GetF() { return m_fF; }

	void SetNodeX(int iX) { m_iX = iX; }
	void SetNodeY(int iY) { m_iY = iY; }
	void SetNodeZ(int iZ) { m_iZ = iZ; }
	int GetNodeX() { return m_iX; }
	int GetNodeY() { return m_iY; }
	int GetNodeZ() { return m_iZ; }

	void SetParentNode(AStarNode* pkNode) { m_pkParent = pkNode; }
	AStarNode* GetParentNode() { return m_pkParent; }

	void SetNextNode(AStarNode* pkNode) { m_pkNext = pkNode; }
	AStarNode* GetNextNode() { return m_pkNext; }

	void SetPathNode(CPathNode* pkPath) { m_pkPathNode = pkPath; }
	CPathNode* GetPathNode() { return m_pkPathNode; }
protected:
	float m_fF;
	float m_fG;
	float m_fH;

	int m_iX;
	int m_iY;
	int m_iZ;

	unsigned int m_uiNumChildren;
	AStarNode* m_pkChildren[20];
	AStarNode* m_pkParent;
	AStarNode* m_pkNext;
	CPathNode* m_pkPathNode;
};

//class CAStar
//{
//public:
//	CAStar();
//	~CAStar();
//
//	bool GeneratePath(int, int, int, int);
//	int Step();
//	void StepInitialize(int, int, int, int);
//
//	int m_iSX, m_iSY, m_iDX, m_iDY;
//
//	//protected:
//	AStarNode* m_pkOpen;
//	AStarNode* m_pkClosed;
//	AStarNode* m_pkBest;
//
//	void AddToOpen(AStarNode*);
//	void ClearNodes();
//	void CreateChildren(AStarNode*);
//	void LinkChild(AStarNode*, AStarNode*);
//
//	AStarNode* CheckList(AStarNode*, int, int);
//	AStarNode* GetBest();
//};

#endif