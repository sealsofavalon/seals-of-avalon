#include "stdAfx.h"
#include "Player.h"
#include "SceneManager.h"
#include "ItemManager.h"
#include "LightManager.h"
#include "PlayerInputMgr.h"
#include "ObjectManager.h"
#include "AudioInterface.h"
#include "Network/PacketBuilder.h"
#include "Utils/ClientUtils.h"
#include "AudioInterface.h"
#include "Map/Map.h"
#include "Map/Chunk.h"
#include "Effect/DecalManager.h"
#include "Avatar.h"
#include "Effect/EffectManager.h"
#include "Effect/DecalManager.h"
#include "Skill/SkillManager.h"
#include "ui/UISystem.h"
#include "ui/UIItemSystem.h"
#include "State/StateAttack.h"
#include "State/StateIdle.h"
#include "State/StateMove.h"
#include "State/StateMoutMove.h"
#include "Console.h"
#include "ClientState.h"
#include "ClientApp.h"
#include "Network/NetworkManager.h"
#include "ui/UIGamePlay.h"
#include "ResourceManager.h"
#include "SYItem.h"
#include "EquipSlot.h"
#include "SYMount.h"
#include "PathFinder.h"
#include "ui/UIPackage.h"
#include "ui/UIBank.h"
#include "ui/UIPlayerEquipment.h"
#include "ui/UIShowPlayer.h"
#include "ui/SocialitySystem.h"
#include "ui/TeacherSystem.h"
#include "ui/UChat.h"
#include "ui/TitleMgr.h"
#include "Utils/CPlayerTitleDB.h"
#include "ui/UFun.h"
#include "ui/UInstanceMgr.h"
#include "DpsCount.h"
#include "ui/UIPlayerEquipment.h"
#include "ui/UISkill.h"
#include "Aura.h"


const static float NumAutoMoveTime = 0.05f;
IStateProxy* CPlayer::sm_StateProxys[STATE_MAX];
IStateProxy* CPlayer::GetStateInstance(State eState)
{
	return GetStateProxy(eState);
}
// 不同的类别相同状态的代理可能不一样.
IStateProxy* CPlayer::GetStateProxy(State StateID)
{
	switch(StateID)
	{
	case STATE_IDLE:
		return StateIdle::Instance();
	case STATE_ACTIONIDLE:
		return StateActionIdle::Instance();
	case STATE_MOUNT_IDLE:
		return StateMoutIdle::Instance();
	case STATE_DEATH:
		return StateDeath::Instance();
	case STATE_MOVE:
		return StateMove::Instance();
	case STATE_MOUNT_MOVE:
		return StateMountMove::Instance();
	case STATE_NORMAL_ATTACK:
		return StateAttack::Instance();
	case STATE_SKILL_ATTACK:
		return StateAttackSkill::Instance();
	case STATE_CONSTRAINT:
		return StateConstraint::Instance();
	default:
		break;
	}
	return NULL;
}

CPlayer::CPlayer(void)
    :m_kFormModels(2, 2)
{
	m_typeid = TYPEID_PLAYER;
	m_bRoot = false;
	m_LastUpdateTime = 0.0f;

	m_pkPackageContainer = NULL;
	m_pkBankContainer = NULL;
	m_pkEquipmentContainer = NULL;
	m_pkTradeContainer = NULL;

	m_spMountNode = NULL;

	m_spAMSave = NULL;

	m_fMountOffset = NiPoint3::ZERO;

	m_valuesCount = PLAYER_END;
	m_uiCurLevel = 0;

	m_bFirstSample = true;
	m_dCurAcc = m_dPrevAcc = 0.0;
	m_dPrevSRTT = m_dPrevSDEV = 0.0;

	m_uiLastTime = 0;
	m_uiLastRT = 0;
	m_uiLastFalg = 0;
	m_ft = 0;
	m_bBallAnim = false;

	m_bArmed = FALSE;
	m_bFirstStep = TRUE;
	m_iArmEvent = 0;
	m_bWeaponAttached = FALSE;
	m_bInitMoney = true;
	m_pSwimSound = NULL;
	m_iDodgeTimesLeft = 0;
	m_iCritTimesLeft = 0;
	m_iparryTimesLeft = 0;
	m_pCurBattleAniSeq = NULL;
	m_bIsShowHelm = true;
	m_bShowShizhuang = false;
	m_bisStuned = false;

	m_uiLocalMoveTime = 0;
	m_uiSrcMoveTime = 0;
	m_Movelevel = 0;
	m_ShowGroup = FALSE;

	_InitValues();

	for (int i = 0 ; i < EQUIPMENT_SLOT_END ; ++i)
	{
		m_Equiped[i].equipitementry = 0;
		m_Equiped[i].jinglianlevel = 0;
	}
}

CPlayer::~CPlayer(void)
{
	m_bRoot = false;
    if(!m_bClientObject)
	    DetachFromScene(true);

	m_pSwimSound = NULL;

	if(m_pkPackageContainer)
	{
		delete m_pkPackageContainer;
		m_pkPackageContainer = NULL;
	}
	
	if (m_pkBankContainer)
	{
		delete m_pkBankContainer;
		m_pkBankContainer = NULL;
	}

	if(m_pkEquipmentContainer)
	{
		delete m_pkEquipmentContainer;
		m_pkEquipmentContainer = NULL;
	}

	if(m_pkTradeContainer)
	{
		delete m_pkTradeContainer;
		m_pkTradeContainer = NULL;
	}

    if(m_spMountNode)
    {
	    g_ResMgr->FreeKfm(m_spMountNode);
	    m_spMountNode = NULL;
    }

    if(m_spAMSave)
    {
        g_ResMgr->FreeKfm(m_spAMSave);
        m_spAMSave = NULL;
    }

	CAvatar::FreeAvatar((NiNode*)m_spSceneNode->GetAt(0));

    for(unsigned int i = 0; i < m_kFormModels.GetSize(); ++i)
    {
        //模型已经装载完毕， 但是还没有变身
        if(m_kFormModels.GetAt(i)->IsFinished())
            NiDelete m_kFormModels.GetAt(i);
    }
    m_kFormModels.RemoveAll();

}

void CPlayer::HilightObject(BOOL Hilight)
{
	//if ( !IsLocalPlayer() )
	{
		CGameObject::HilightObject( Hilight );
		if ( m_pkMountCreature )
			m_pkMountCreature->HilightObject( Hilight );
	}
}

void CPlayer::HilightEdgObject(BOOL bHilightEdg)
{
	if ( !IsLocalPlayer() )
	{
		CGameObject::HilightEdgObject( bHilightEdg );
		if ( m_pkMountCreature )
			m_pkMountCreature->HilightEdgObject( bHilightEdg );
	}
}

void CPlayer::SetDirectionTo(const NiPoint3& Target)
{
	CCharacter::SetDirectionTo(Target);
}

bool CPlayer::InitCamera()
{
	m_Camera.SetDefault();

	NiTransform kTransform;
	kTransform.m_Translate = GetPosition();
	kTransform.m_Rotate = m_Camera.GetNiCamera().GetRotate();
	m_Camera.SetTransform(kTransform);

	return true;
}

void CPlayer::RegistTextKey()
{
	CAnimObject::RegistTextKey();

	unsigned int* puiSequenceIDs;
	unsigned int uiNumIDs;
	NiControllerSequence* pkSequence;
	NiTextKeyExtraData* pkExtraData;
	m_spActorManager->GetKFMTool()->GetSequenceIDs(puiSequenceIDs, uiNumIDs);
	for(unsigned int ui = 0; ui < uiNumIDs; ++ui)
	{
		pkSequence = m_spActorManager->GetSequence(puiSequenceIDs[ui]);
		if( pkSequence == NULL )
			continue;

		pkExtraData = m_spActorManager->GetSequence(puiSequenceIDs[ui])->GetTextKeys();
		if(pkExtraData)
		{
			unsigned int uiNumKeys;
			NiTextKey* pKeys = pkExtraData->GetKeys(uiNumKeys);
			for(unsigned int uiK = 0; uiK < uiNumKeys; ++uiK)
			{
				if(pKeys[uiK].GetText().Equals("attack_begin"))
				{
					m_spActorManager->RegisterCallback(NiActorManager::TEXT_KEY_EVENT, puiSequenceIDs[ui], "attack_begin");
				}
				if(pKeys[uiK].GetText().Equals("attack_end"))
				{
					m_spActorManager->RegisterCallback(NiActorManager::TEXT_KEY_EVENT, puiSequenceIDs[ui], "attack_end");
				}
			}
		}
	}
	NiFree(puiSequenceIDs);
}

BOOL CPlayer::CreatePlayer(ui32 uiDisplayId)
{
	std::string strFileName;
	ItemMgr->GetDisplayInfo(uiDisplayId, strFileName, GetRace(), GetGender(), true);

	NIASSERT(strFileName.size());
	if(strFileName.size() == 0)
		return FALSE;

	if(!CreateActor(strFileName.c_str(), true))
	{
		return FALSE;
	}

	{
		m_pkPackageContainer = new CStorage(CStorage::EBAG);

		m_pkBankContainer = new CStorage(CStorage::EBANK);
		
		m_pkEquipmentContainer = new CEquipmentContainer;
		m_pkEquipmentContainer->Init(EQUIPMENT_SLOT_END-EQUIPMENT_SLOT_START, 0);

		m_pkTradeContainer = new CPlayerTradeContainer;
		m_pkTradeContainer->Init(20, 0);
		m_pSwimSound = NiNew ALAudioSource(AudioSource::TYPE_3D);
	}

	return TRUE;
}

bool CPlayer::CanSeeObject( CGameObject* pObject )
{
	if ( !pObject )
		return false;

	NiBound kBound;
	pObject->GetPickBound(kBound);

	if ( kBound.GetRadius() < 0.1f )
		return false;

	return m_Camera.TestVisible(kBound);
}

void CPlayer::Update(float dt)
{
	/*UINT CURHP = GetUInt32Value(UNIT_FIELD_HEALTH);
	UINT CURState = GetUInt32Value(UNIT_FIELD_DEATH_STATE);
	
	if (CURHP  > 0 && CURState != ALIVE)
	{
		if (m_bIsDead)
		{
			OnAlive();
			SetUInt32Value(UNIT_FIELD_DEATH_STATE, ALIVE);
		}
	}

	if (CURHP <= 0 && CURState == ALIVE)
	{
		if (!m_bIsDead)
		{
			OnDeath();
			SetUInt32Value(UNIT_FIELD_DEATH_STATE, DEAD);
		}
	}*/

	ProcessMovement();
	CCharacter::Update(dt);

	float fCurrentTime = SYState()->ClientApp->GetAccumTime();
	if ( IsCurSecAnimEndInTime( fCurrentTime ) )
	{
		if ( m_iCritTimesLeft > 0 )
		{
			NiFixedString strAnim = "prepare_bombattack";
			AnimationID animID = GetAnimationIDByName(strAnim);

			NiControllerSequence* m_pCurBattleAniSeq = ActivateAnimation( animID, 2, 2, 0.1f, NiKFMTool::SYNC_SEQUENCE_ID_NONE, false, true, true );

			if ( m_pCurBattleAniSeq )
			{
				if ( m_iCritTimesLeft > 1 )
				{
					m_pCurBattleAniSeq->SetFrequency( 2.f );
				}
				else
					m_pCurBattleAniSeq->SetFrequency( 1.f );
				
				
			}
			m_iCritTimesLeft --;
			return;
		}

		if ( m_iparryTimesLeft > 0 )
		{
			NiFixedString strAnim = "single_hand_prepare_parry";
			AnimationID animID = GetAnimationIDByName(strAnim);

			NiControllerSequence* m_pCurBattleAniSeq = ActivateAnimation( animID, 2, 2, 0.1f, NiKFMTool::SYNC_SEQUENCE_ID_NONE, false, true, true );

			/*AudioSource* pEffectSound  = AudioSystem::GetAudioSystem()->CreateSource(AudioSource::TYPE_3D);
			if (!pEffectSound)
				return;

			GetSceneNode()->AttachChild(pEffectSound);
			GetSceneNode()->Update(0.0f, false);

			pEffectSound->SetMinMaxDistance(5.0f, 80.0f);
			pEffectSound->SetGain(1.f);
			pEffectSound->SetLoopCount( 1 );
			pEffectSound->SetSelectiveUpdate(true);
			pEffectSound->SetSelectiveUpdateTransforms(true);
			pEffectSound->SetSelectiveUpdatePropertyControllers(false);
			pEffectSound->SetSelectiveUpdateRigid(true);
			pEffectSound->Update(0.0f);*/
			if ( !m_spVoice[1] )
			{
				m_spVoice[1] = NiNew ALAudioSource(AudioSource::TYPE_3D);
			}
			GetSceneNode()->AttachChild(m_spVoice[1]);
			m_spVoice[1]->SetMinMaxDistance(5.0f, 60.0f);
			m_spVoice[1]->SetGain(1.f);

			std::string  filepath = "Sound/block.wav";
			const char* pOldname =  m_spVoice[1]->GetFilename();

			if ( pOldname && !strcmp( pOldname, filepath.c_str()))
			{
				m_spVoice[1]->Play();
			}
			else
				SYState()->IAudio->PlaySound(m_spVoice[1], filepath.c_str());

			//SYState()->IAudio->PlaySound( pEffectSound, "Sound/block.wav");


			if ( m_pCurBattleAniSeq )
			{
				if ( m_iparryTimesLeft > 1 )
				{
					m_pCurBattleAniSeq->SetFrequency( 2.f );
				}
				else
					m_pCurBattleAniSeq->SetFrequency( 1.f );


			}
			m_iparryTimesLeft --;
			return;
		}

		if ( m_iDodgeTimesLeft > 0 )
		{
			NiFixedString strAnim = "prepare_dodge";
			AnimationID animID = GetAnimationIDByName(strAnim);

			NiControllerSequence* m_pCurBattleAniSeq = ActivateAnimation( animID, 2, 2, 0.1f, NiKFMTool::SYNC_SEQUENCE_ID_NONE, false, true, true );

			if ( !m_spVoice[1] )
			{
				m_spVoice[1] = NiNew ALAudioSource(AudioSource::TYPE_3D);
			}
			GetSceneNode()->AttachChild(m_spVoice[1]);
			m_spVoice[1]->SetMinMaxDistance(5.0f, 60.0f);
			m_spVoice[1]->SetGain(1.f);

			std::string  filepath = "Sound/dodge.wav";
			const char* pOldname =  m_spVoice[1]->GetFilename();

			if ( pOldname && !strcmp( pOldname, filepath.c_str()))
			{
				m_spVoice[1]->Play();
			}
			else
				SYState()->IAudio->PlaySound(m_spVoice[1], filepath.c_str());


			/*AudioSource* pEffectSound  = AudioSystem::GetAudioSystem()->CreateSource(AudioSource::TYPE_3D);
			if (!pEffectSound)
				return;

			GetSceneNode()->AttachChild(pEffectSound);
			GetSceneNode()->Update(0.0f, false);

			pEffectSound->SetMinMaxDistance(5.0f, 80.0f);
			pEffectSound->SetGain(1.f);
			pEffectSound->SetLoopCount( 1 );
			pEffectSound->SetSelectiveUpdate(true);
			pEffectSound->SetSelectiveUpdateTransforms(true);
			pEffectSound->SetSelectiveUpdatePropertyControllers(false);
			pEffectSound->SetSelectiveUpdateRigid(true);
			pEffectSound->Update(0.0f);

			SYState()->IAudio->PlaySound( pEffectSound, "Sound/dodge.wav");*/

			if ( m_pCurBattleAniSeq )
			{
				if ( m_iDodgeTimesLeft > 1 )
				{
					m_pCurBattleAniSeq->SetFrequency( 2.f );
				}
				else
					m_pCurBattleAniSeq->SetFrequency( 1.f );
				
				
			}
			m_iDodgeTimesLeft --;
		}
	}
	else
	{
		if ( m_pCurBattleAniSeq )
		{
			if ( m_iDodgeTimesLeft + m_iCritTimesLeft + m_iparryTimesLeft> 0 )
			{
				m_pCurBattleAniSeq->SetFrequency( 2.f );
			}
		}
	}
	
}

void CPlayer::PendingUpdate()
{
	UpdatePendingProcess();
	if(IsMountCreature())
	{
		UpdateTransformbyChar( this );
		UpdateMountMode();
	}
	else
	if (IsMountPlayer() || IsMountByPlayer())
	{
		
		UpdateMountMode();
	}
	else
		UpdateTransformbyChar(this);
}

void CPlayer::UpdatePendingProcess()
{
	float fT = SYState()->ClientApp->GetAccumTime();
	if (m_iArmEvent == 1 && fT >= m_fArmEvtTime )
	{
		AttachWeapon();
		m_iArmEvent = 0;
	}
	else
	if (m_iArmEvent == 2 && fT >= m_fArmEvtTime )
	{
		DetachWeapon();
		m_iArmEvent = 0;
	}
}

void CPlayer::DetachWeapon()
{
	ui32 mainhandweapon_entryid = GetUInt32Value(PLAYER_VISIBLE_ITEM_6_0);
	if (mainhandweapon_entryid != 0)
	{
		ItemMgr->OnEquipmentChange(this, 0, EQUIPMENT_SLOT_MAINHAND);
		ItemMgr->OnEquipmentChange(this, mainhandweapon_entryid, EQUIPMENT_SLOT_MAINHAND, AS_BacksideRight);
	}

	ItemPrototype_Client* pkOffHandWeapon = GetOffHandWeaponPrototype();
	if (pkOffHandWeapon && pkOffHandWeapon->SubClass != ITEM_SUBCLASS_WEAPON_BAOZHU)
	{
		ui32 offhandweapon_entryid = GetUInt32Value(PLAYER_VISIBLE_ITEM_7_0);
		if (offhandweapon_entryid != 0)
		{
			ItemMgr->OnEquipmentChange(this, 0, EQUIPMENT_SLOT_OFFHAND);
			ItemMgr->OnEquipmentChange(this, offhandweapon_entryid, EQUIPMENT_SLOT_OFFHAND, AS_BacksideLeft);
		}
	}
	m_bWeaponAttached = FALSE;
}

void CPlayer::AttachWeapon(bool imme /*= true*/, bool always/*=false*/)
{
	if (m_bWeaponAttached && !always)
		return;

	ui32 mainhandweapon_entryid = GetUInt32Value(PLAYER_VISIBLE_ITEM_6_0);
	if (mainhandweapon_entryid != 0)
	{
		if (imme)
		{
			ItemMgr->OnEquipmentChangeReal(this, 0, EQUIPMENT_SLOT_MAINHAND, AS_BacksideRight);
			ItemMgr->OnEquipmentChangeReal(this, mainhandweapon_entryid, EQUIPMENT_SLOT_MAINHAND, -1);
		}
		else
		{
			ItemMgr->OnEquipmentChange(this, 0, EQUIPMENT_SLOT_MAINHAND, AS_BacksideRight);
			ItemMgr->OnEquipmentChange(this, mainhandweapon_entryid, EQUIPMENT_SLOT_MAINHAND);
		}
	}

	ItemPrototype_Client* pkOffHandWeapon = GetOffHandWeaponPrototype();
	if (pkOffHandWeapon && pkOffHandWeapon->SubClass != ITEM_SUBCLASS_WEAPON_BAOZHU)
	{
		ui32 offhandweapon_entryid = GetUInt32Value(PLAYER_VISIBLE_ITEM_7_0);
		if (offhandweapon_entryid != 0)
		{
			if (imme)
			{
				ItemMgr->OnEquipmentChangeReal(this, 0, EQUIPMENT_SLOT_OFFHAND, AS_BacksideLeft);
				ItemMgr->OnEquipmentChangeReal(this, offhandweapon_entryid, EQUIPMENT_SLOT_OFFHAND, -1);
			}
			else
			{
				ItemMgr->OnEquipmentChange(this, 0, EQUIPMENT_SLOT_OFFHAND, AS_BacksideLeft);
				ItemMgr->OnEquipmentChange(this, offhandweapon_entryid, EQUIPMENT_SLOT_OFFHAND);
			}
		}
	}
	m_bWeaponAttached = TRUE;
}

void CPlayer::AddRemotePlayerMovementInfo(MoveInfo& moveInfo/*MovementInfo& moveInfo*/)
{
	if ( m_stTelePortPara.bTelePort || ( GetMoveState() == CMS_RUSH ) )
	{
		return;
	}
	uint32 msTime = GetClientTime();
	//
	int iCurDelta = msTime - moveInfo.stMove.time;//两个客户端时间差

	/*do
	{*/
		if (m_bFirstSample )
		{
			m_iLastDelta = iCurDelta;//保留首次移动时间差
			//m_dPrevSRTT = double(iCurDelta);//首次移动的时间差
			//m_dPrevSDEV = 0.0;
			if ( moveInfo.moveOP != MOVE_SET_FACING )
			{
				m_bFirstSample = false;
			}
			
			//m_ft = msTime;//开始移动在本客户端时间
			//m_uiSrcMoveTime = 0;
			//m_uiLocalMoveTime = 0;
			/*NiPoint3 curPoint, l;
			curPoint = NiPoint3(moveInfo.stMove.x, moveInfo.stMove.y, moveInfo.stMove.z);
			l = GetPosition();


			if ( !IsFlying() || (CheckInWater() != WL_UNDER_WATER) )
			{
				curPoint.z = l.z = 0.0f;
			}

			if ( (curPoint - l ).Length() > 0.001f )
			{
				this->SetPosition(curPoint);
			}

			m_LastPoint = curPoint;*/
		}
		else
		{
			//m_uiSrcMoveTime += moveInfo.stMove.time - m_uiLastRT;
			
			/*double srtt = 0.875 * double(m_dPrevSRTT) + 0.125 * double(iCurDelta);
			double e  = double(iCurDelta) - double(m_dPrevSRTT);
			double sdev = 0.75 * m_dPrevSDEV + 0.25 * fabs(e);
			double rt0  = srtt + sdev;

			m_dPrevAcc = m_dPrevAcc>sdev?m_dPrevAcc:sdev;
			double bias = m_dPrevAcc - sdev;


			if (fabs(m_dPrevAcc - sdev) >= 300 || m_dPrevAcc > 1500) {
				m_dPrevAcc = 0;
			}

			m_iLastDelta = LONG(rt0);
			m_dPrevSRTT = srtt;
			m_dPrevSDEV = sdev*/;
	 	}
	/*} while (m_bFirstSample);*/

	//moveInfo.stMove.time = (ui32)((moveInfo.stMove.time - (msTime - m_iLastDelta)) + m_dPrevAcc + msTime);
	//moveInfo.stMove.time = moveInfo.stMove.time;
	
		if ( moveInfo.moveOP != MOVE_SET_FACING )
		{
			if ( moveInfo.stMove.flags )
			{
				if ( (moveInfo.stMove.flags == MOVEFLAG_PITCH_UP) || (moveInfo.stMove.flags == MOVEFLAG_JUMPING) )
				{
					m_Movelevel += 2;
				}
				else
					m_Movelevel += 1;
			}
			else
				m_Movelevel += 2;
		}

	m_MovementInfos.push(moveInfo);


}

void CPlayer::ProcessMovement()
{
	if (m_MovementInfos.size() > 0 && !GetTelePortPara().bTelePort )
	{
		bool bFaceing = false;
	
		MoveInfo& miFacing = m_MovementInfos.front();
		if ( miFacing.moveOP == MOVE_SET_FACING )
		{
			bFaceing = true;
		}
		


		if ( ((m_Movelevel > 1) || (IsJumping())) || bFaceing/*|| IsFlying()*/|| (GetMoveState() == CMS_RUSH) )
		{
			uint32 mst = GetClientTime();/*float2int32(NiGetCurrentTimeInSec() * 1000);*/
			SetAccSpeed( 0.f );
			bool bMovementProcess = false;

			MoveInfo mi = m_MovementInfos.front();
			MovementInfo& moveinfo = mi.stMove;
			uint32 t = moveinfo.time;

			if ( m_bFirstStep )
			{
				SetPosition(NiPoint3(moveinfo.x, moveinfo.y, moveinfo.z));
			}

			NiPoint3 curPoint, l;

			l = GetPosition();

			curPoint = m_NextPoint = NiPoint3(moveinfo.x, moveinfo.y, moveinfo.z);

			if ( !IsFlying() && (CheckInWater() != WL_UNDER_WATER) )
			{
				curPoint.z = l.z = m_NextPoint.z = m_LastPoint.z = 0.0f;
			}

			float ptoff = (l - m_LastPoint).Length();
			float ptG = ( curPoint - m_LastPoint ).Length();



			uint32 srcElapsed = 0;
			uint32 localElapsed = 0;

			if( !m_bFirstStep )
			{
				srcElapsed = moveinfo.time - m_uiLastRT;
				localElapsed = mst - m_uiLocalMoveTime;
			}

			
			if ( (fabs( ptoff - ptG ) < 0.1f) || ( ptoff > ptG ) || (localElapsed > srcElapsed)&&((localElapsed - srcElapsed) > 250)/*m_Movelevel > 7*/ || !moveinfo.flags && !GetMovementFlags() )
			{
				if (mi.moveOP == MOVE_SET_FACING)
				{
					//SetServerAngle(moveinfo.orientation);
					m_fDesirdAngle = moveinfo.mo;
					m_fFaceingAngle = moveinfo.orientation;
					m_MovementInfos.pop();
					curPoint = GetPosition();
					return;
					//curPoint = GetPosition();
					//m_MovementInfos.pop();
					//return;//continue;
				}
				else
				{

					if ( m_bFirstStep )
					{
						m_uiLocalMoveTime = GetClientTime();
						m_uiLastRT = moveinfo.time;
						m_bFirstStep = false;
					}
				/*NiPoint3 MoveDir = curPoint - GetPosition();
				MoveDir.Unitize();
				MoveDir.z = 0.0f;

				float fZ = NiATan2(-MoveDir.y, MoveDir.x) - NI_PI/2;
				while( fZ < 0.f )
					fZ += NI_TWO_PI;

				while( fZ > NI_TWO_PI )
					fZ -= NI_TWO_PI;

				if( GetDesiredAngle() != fZ )
				{
					SetDesiredAngle(fZ);
				}*/
					float err = (curPoint - l).Length();

					m_MovementInfos.pop();
					//if (moveinfo.flags != 0 && (moveinfo.flags == m_uiLastFalg) && moveinfo.mo == GetDesiredAngle() || 
					//	this->IsJumping())
					//{

					//	//MoveTo( curPoint,moveinfo.flags );
					//	/*int32 gap = mst - m_uiLastTime;
					//	int32 ert = moveinfo.time - m_uiLastRT;
					//	int32 err = gap - ert;

					//	if (ert != 0)
					//	{
					//		float acc = float(err / ert) * 0.001f * GetMoveSpeed() * 0.5f;
					//		SetAccSpeed(acc);
					//	}*/
					//}
					//else
					//if ( err > 0.2f )
					//{
					//	SetPosition(NiPoint3(moveinfo.x, moveinfo.y, moveinfo.z));
					//}
					

					if (moveinfo.flags&MOVEFLAG_JUMPING)
					{
						if ( !m_bisStrikeBack )
						{
							EndJump();
						}
						
						SetMovementInfo(moveinfo);
						//SetServerAngle(moveinfo.orientation);
						m_fDesirdAngle = moveinfo.mo;
						m_fFaceingAngle = moveinfo.orientation;

						if (GetCurPostureState() != STATE_MOVE)
						{
							if (isMount())
							{
								ForceStateChange(STATE_MOUNT_MOVE, 0);
								if (IsMountCreature())
								{
									GetMountCreature()->ForceStateChange(STATE_MOUNT_MOVE, 0);
								}
							}
							else
							{
								ForceStateChange(STATE_MOVE, 0);
							}
						}
						else
						{
							ProcessJumpState(true);
						}
					}
					else
					{
						//bool bneedsetAnim = ( ((m_stMoveInfo.flags & MOVEFLAG_MOVE_BACKWARD) != ( (moveinfo.flags & MOVEFLAG_MOVE_BACKWARD) && !(moveinfo.flags & MOVEFLAG_MOVE_FORWARD) )) || ((m_stMoveInfo.flags & MOVEFLAG_STRAFE_LEFT) != (moveinfo.flags & MOVEFLAG_STRAFE_LEFT)) || ((m_stMoveInfo.flags & MOVEFLAG_STRAFE_RIGHT) != (moveinfo.flags & MOVEFLAG_STRAFE_RIGHT)));

						bool bneedsetAnim = ( ( m_stMoveInfo.flags != moveinfo.flags ) && ( moveinfo.flags != 0 ) );
						if ( HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM) )
						{
							bneedsetAnim = false;
						}

						if ( fabs( GetDesiredAngle() - moveinfo.mo ) > 0.01f )
						{
							SetPosition(NiPoint3(moveinfo.x, moveinfo.y, moveinfo.z));
						}
						m_bNeedDoAnim = bneedsetAnim;
						//SetServerAngle(moveinfo.orientation);
						SetMovementInfo(moveinfo);
						m_fDesirdAngle = moveinfo.mo;
						m_fFaceingAngle = moveinfo.orientation;

					/*	if ( (fabs( GetDesiredAngle() - moveinfo.mo ) > 0.01f) )
						{
							if ( m_MovementInfos.size() )
							{
								MoveInfo miFront = m_MovementInfos.front();
								MovementInfo& moveinfoNext = miFront.stMove;

								NiPoint3 ptNext = NiPoint3( moveinfoNext.x, moveinfoNext.y, moveinfoNext.z );

								NiPoint3 ptoffset = ptNext - l;
								NiPoint3 ptLastOff = ptNext - m_LastPoint;


								ptoffset.Unitize();
								ptoffset.z = 0.0f;

								ptLastOff.Unitize();
								ptLastOff.z = 0.0f;

								float fZDes = NiATan2(-ptLastOff.y, ptLastOff.x) - NI_PI/2;
								while( fZDes < 0.f )
									fZDes += NI_TWO_PI;

								while( fZDes > NI_TWO_PI )
									fZDes -= NI_TWO_PI;


								if ( fabs(moveinfo.mo - fZDes ) < 0.01f )
								{
									float fZOff = NiATan2(-ptoffset.y, ptoffset.x) - NI_PI/2;
									while( fZOff < 0.f )
										fZOff += NI_TWO_PI;

									while( fZOff > NI_TWO_PI )
										fZOff -= NI_TWO_PI;

									SetDesiredAngle( fZOff );
								}
							}
							else
								SetPosition(NiPoint3(moveinfo.x, moveinfo.y, moveinfo.z));
							
							moveinfo.mo = GetDesiredAngle();
							moveinfo.orientation = GetFacingAngle();
						}
						else
						{
							m_fDesirdAngle = moveinfo.mo;
							m_fFaceingAngle = moveinfo.orientation;
						}*/
					}

					if (moveinfo.flags != 0)
						MoveTo();
					else
					{
						//m_dPrevSRTT = 0.0;
						//m_dPrevSDEV = 0.0;
						m_bFirstStep = true;
						SetPosition(NiPoint3(moveinfo.x, moveinfo.y, moveinfo.z));
						m_uiLocalMoveTime = 0;
					}
				}

				

				bMovementProcess = true;
				m_uiLastTime = mst;
				//m_uiLastRT = moveinfo.time;
				m_uiLastFalg = moveinfo.flags;

				m_LastPoint = curPoint;

				NiPoint3 ptDir = NiPoint3::ZERO;
				if ( m_MovementInfos.size() )
				{
					MoveInfo& miFront = m_MovementInfos.front();
					MovementInfo& moveinfoNext = miFront.stMove;
					NiPoint3 ptNext( moveinfoNext.x,moveinfoNext.y,moveinfoNext.z );
					ptDir = ptNext - m_LastPoint;
				}
					
				//m_LastOrient = curPoint - m_LastPoint;

				if ( ptDir.Length() > 0.001f )
				{
					m_LastOrient = ptDir;
					m_LastOrient.Unitize();
				}

				//m_MovementInfos.pop();

				if ( moveinfo.flags )
				{
					if ( moveinfo.flags == MOVEFLAG_PITCH_UP || (moveinfo.flags == MOVEFLAG_JUMPING) )
					{
						m_Movelevel -= 2;
					}
					else
						m_Movelevel -= 1;
					
				}
				else
					m_Movelevel -= 2;

			}
			

			/*if (!IsJumping() && !bMovementProcess && m_uiLastFalg != 0 && mst >= m_uiLastTime + 2700) 
			{
				uint32 t = mst - m_uiLastTime + 700;
				float acc = (float)t * 0.001f * GetMoveSpeed();
				acc = NiMin(acc, GetMoveSpeed());
				SetAccSpeed(-acc);
			}*/
		}
		else if( m_Movelevel == 1 && !m_bFirstStep)
		{
			bool bneedwait = false;
			
			MoveInfo& mifirst = m_MovementInfos.front();
			MovementInfo& firstinfo = mifirst.stMove;

			m_NextPoint = NiPoint3(firstinfo.x, firstinfo.y, firstinfo.z);
			float ptoff = (GetPosition() - m_LastPoint).Length();
			float ptG = ( m_NextPoint - m_LastPoint ).Length();

			if ( ( ptoff > ptG ) && (fabs( ptoff - ptG ) > 0.5f) && !IsFlying() && (GetMoveState() != CMS_RUSH) )
			{
				//SetPosition(NiPoint3(m_NextPoint.x, m_NextPoint.y, m_NextPoint.z));
				float accspeed = -GetMoveSpeed();
				SetAccSpeed( accspeed );
			}
			
		}
			
	}
	
}

#define MIN_WALK_NORMAL 0.7f
#define STEP_HEIGHT 0.6f
#define	OVERCLIP 1.001f
#define BUMP_COUNT 4

/*
==================
ClipVelocity

Slide off of the impacting surface
==================
*/
NiPoint3 ClipVelocity( const NiPoint3& velocity, const NiPoint3& normal) 
{
	float	backoff;
	
	backoff = velocity.Dot(normal);
	
	if ( backoff < 0 ) 
	{
		backoff *= OVERCLIP;
	} 
	else 
	{
		backoff /= OVERCLIP;
	}

	return velocity - normal*backoff;
}

void CPlayer::EndOfSequence(NiActorManager* pkManager, 
	NiActorManager::SequenceID eSequenceID, float fCurrentTime, float fEventTime)
{
	CCharacter::EndOfSequence(pkManager, eSequenceID, fCurrentTime, fEventTime);
}

bool CPlayer::GetBaseMoveSpeed(float& value) const
{
	if( GetMoveState() == CMS_RUN)
	{
		value = 7.0f; 
		if ( m_stMoveInfo.flags & MOVEFLAG_MOVE_BACKWARD )
		{
			value = 4.5f;
		}
		return true;
	}
	else
	if (GetMoveState() == CMS_WALK)
	{
		value = 2.5f;
		return true;
	}
	else
		return false;

	return false;
}

BOOL CPlayer::SetMoveAnimation(UINT Flags, BOOL bForceStart)
{
	AnimationID animID = GetMovementAnimation(Flags);

	if ( animID == m_uiCurrAnimID )
	{
		return TRUE;
	}

	if ( CheckInWater() == WL_UNDER_WATER )
	{
		bForceStart = FALSE;
	}
	m_pkMoveSeq = SetCurAnimation(animID, 1.0, TRUE, bForceStart, 1 );

	float fBaseMoveSpeed = 0.0f;
	bool ret = GetBaseMoveSpeed(fBaseMoveSpeed);
	if (m_pkMoveSeq != NULL && fBaseMoveSpeed > 0.0f)
	{
		float fModleScal = m_fModleScale;
		if ( fModleScal < 0.00001 )
		{
			fModleScal = 1.f;
		}
		m_pkMoveSeq->SetFrequency(GetMoveSpeed() / ( fBaseMoveSpeed * fModleScal ) );
	}

	return (m_pkMoveSeq != NULL);
}


BOOL CPlayer::SetAttackAnimation(bool bPrepare /*= false*/)
{
	UINT Serial = (UINT)MakeAttackSerial();
	BOOL bLoop = FALSE;
	bool bScale = false;

	AnimationID AtkAnim = NiActorManager::INVALID_SEQUENCE_ID;
	int p = 2;
	if (bPrepare)
	{
		NiFixedString strAnimName = GetBaseAnimationName(CAG_COMBATPREPARE);
		AtkAnim = GetAnimationIDByName(strAnimName);

		if (CheckInWater() != WL_UNDER_WATER)
		{
			SetCurAnimation(AtkAnim, 1.0f, TRUE, FALSE, -1);
		}else
		{
			const char* szIdleAnimName = GetBaseAnimationName(CAG_IDLE, 1);
			AtkAnim = GetAnimationIDByName(szIdleAnimName);
			SetCurAnimation(szIdleAnimName, 1.0f, TRUE, TRUE, 1);
		}
		return TRUE;
	}
	else
	{
		AtkAnim = GetNormalAttackAnimation(Serial);
		bScale = true;
	}

	if (AtkAnim == NiActorManager::INVALID_SEQUENCE_ID)
		return FALSE;

	const uint32& baseAttackTime = GetUInt32Value(UNIT_FIELD_BASEATTACKTIME);
	NiControllerSequence* Seq = ActivateAnimation(AtkAnim, p, 1, 0.1f, NiKFMTool::SYNC_SEQUENCE_ID_NONE, false, true, true);
	if (Seq)
	{
		Seq->SetFrequency(1.0f);
		Seq->SetCycleType(bLoop?NiTimeController::LOOP:NiTimeController::CLAMP);

		/*if (bScale)
		{
			float fLength = Seq->GetLength() + 0.2f;
			float fScale = fLength/(baseAttackTime*0.001f);

			if (fScale > 1.0f)
				Seq->SetFrequency(fScale);
		}*/
	}

	return TRUE;
}

const char* CPlayer::GetBaseAnimationName(ECHAR_ANIM_GROUP eAnimGroup, int nIndex)
{
	// return CCharacter::GetBaseAnimationName(eAnimGroup, nIndex);
	std::string strPrefix;
	static char szAnimName[2048];
	strPrefix = GetWeaponPrefix();

	if (!isArmed())
		strPrefix = "no_weapon";

	if ((eAnimGroup == CAG_IDLE || eAnimGroup == CAG_WAIT) && GetUInt32Value(UNIT_FIELD_FORMDISPLAYID) == 0 )
	{
		if( CheckInWater() != WL_UNDER_WATER )
		{
			ui32 mainhandweapon_entryid = GetUInt32Value(PLAYER_VISIBLE_ITEM_6_0);
			ItemPrototype_Client* pkOffHandWeapon = GetOffHandWeaponPrototype();
			if (pkOffHandWeapon && pkOffHandWeapon->SubClass == ITEM_SUBCLASS_WEAPON_BAOZHU)
			{
				if (mainhandweapon_entryid == 0 || !this->isArmed())
				{
					return "prepare_ball";
				}
				else
				{
					return "prepare_ballsingle";
				}
			}
			else
			{
				if (m_bBallAnim && !CUISystem::IsCurFrame(DID_INGAME_MAIN))
				{
					return "prepare_ballsingle";
				}
				/*NiNode* BaseNode = (NiNode*)GetSceneNode()->GetAt(0);
				NiNode* avaterNode = (NiNode*)BaseNode->GetObjectByName("Shield_Node");
				if ( avaterNode )
				{
					NiAVObject* pEquip = avaterNode->GetAt(0);

				}*/
			}
		}
	}
	else
	if (eAnimGroup == CAG_COMBATPREPARE && GetUInt32Value(UNIT_FIELD_FORMDISPLAYID) == 0)
	{
		ItemPrototype_Client* pkOffHandWeapon = GetOffHandWeaponPrototype();
		if (pkOffHandWeapon && pkOffHandWeapon->SubClass == ITEM_SUBCLASS_WEAPON_BAOZHU)
		{
			return "prepare_ball";
		}

		if(!strcmp(strPrefix.c_str(), "no_weapon"))
		{
			NiStrcpy(szAnimName, 128, "prepare");
		}
		else if( !strcmp(strPrefix.c_str(), "archer") )
		{
			NiStrcpy(szAnimName, 128, "archer_attack_prepare");
		}
		else if(!strcmp(strPrefix.c_str(), "single_hand"))
		{
			NiStrcpy(szAnimName, 128, "single_hand_prepare");
		}
		else if(!strcmp(strPrefix.c_str(), "double_hand"))
		{
			NiStrcpy(szAnimName, 128, "double_hand_prepare");
		}
		else if(!strcmp(strPrefix.c_str(), "bimanual"))
		{
			NiStrcpy(szAnimName, 128, "bimanual_prepare");
		}
		return szAnimName;
	}
	else
	if (eAnimGroup == CAG_DRAW_WEAPON)
	{
		std::string drawPrefix, weponPrefix;
		if (nIndex == 0) // in
		{
			drawPrefix = "drawin";
		}
		else
		if (nIndex == 1) // out
		{
			drawPrefix = "drawout";
		}
		else
		{
			NIASSERT(0);
			return "";
		}

		ItemPrototype_Client* pkMainHandWeapon = GetMainHandWeaponPrototype();
		ItemPrototype_Client* pkOffHandWeapon = GetOffHandWeaponPrototype();
		if( pkMainHandWeapon )
		{
			if( pkMainHandWeapon->SubClass == ITEM_SUBCLASS_WEAPON_BOW )
				weponPrefix = "archer";
			else
			if ( pkMainHandWeapon->SubClass == ITEM_SUBCLASS_WEAPON_TWOHAND_AXE ||
				pkMainHandWeapon->SubClass == ITEM_SUBCLASS_WEAPON_TWOHAND_SWORD ||
				pkMainHandWeapon->SubClass == ITEM_SUBCLASS_WEAPON_TWOHAND_STAFF ||
				pkMainHandWeapon->SubClass == ITEM_SUBCLASS_WEAPON_TWOHAND_HAMMER)
				weponPrefix = "doublehand";
			else
				weponPrefix = "righthand";
		}

		if( pkOffHandWeapon && pkOffHandWeapon->SubClass != ITEM_SUBCLASS_WEAPON_BAOZHU)
		{
			weponPrefix = (pkMainHandWeapon == 0)?"lefthand":"bimanual";
		}

		std::string name = drawPrefix + "_" + weponPrefix;
		NiStrcpy(szAnimName, 128, name.c_str());

		return szAnimName;
	}
	else
	if (CAG_DEAD == eAnimGroup )
	{
		if( CheckInWater() != WL_UNDER_WATER )
		{
			ItemPrototype_Client* pkOffHandWeapon = GetOffHandWeaponPrototype();
			if (pkOffHandWeapon && pkOffHandWeapon->SubClass == ITEM_SUBCLASS_WEAPON_BAOZHU)
			{
				return "death_ball";
			}
		}
		
	}

	return CCharacter::GetBaseAnimationName(eAnimGroup, nIndex);
}

AnimationID CPlayer::GetMovementAnimation(int iType)
{
	if (!m_spActorManager)
		return NiActorManager::INVALID_SEQUENCE_ID;


	AnimationID AnimID = 0;

	if (iType == CMS_RUSH)
	{
		AnimID = GetAnimationIDByName("Action_100003210");
		return AnimID;
	}

	if ( iType == CMS_RUN )
	{
		if ( CheckInWater() == WL_UNDER_WATER)
		{
			if ( ( GetMovementFlags() & MOVEFLAG_PITCH_DOWN ) && !( GetMovementFlags() & MOVEFLAG_MOVING_MASK )  && !( GetMovementFlags() & MOVEFLAG_STRAFING_MASK ) )
			{
  				AnimID = GetAnimationIDByName("swim_wait");
			}
			else
			{
				if ( ( GetMovementFlags() & MOVEFLAG_MOVE_BACKWARD ) )
				{
					AnimID = GetAnimationIDByName("swim_backward");
					return AnimID;
				}

				if ( ( GetMovementFlags() & MOVEFLAG_STRAFE_LEFT ))
				{
					AnimID = GetAnimationIDByName("strafe_swim_left");
					return AnimID;
				}

				if ( ( GetMovementFlags() & MOVEFLAG_STRAFE_RIGHT ) )
				{
					AnimID = GetAnimationIDByName("strafe_swim_right");
					return AnimID;
				}

				AnimID = GetAnimationIDByName("swim");
			}
			
			return AnimID;
		}

		if ( ( GetMovementFlags() & MOVEFLAG_MOVE_BACKWARD ) )
		{
			AnimID = GetAnimationIDByName("Backward01");

			return AnimID;
		}

		if ( ( GetMovementFlags() & MOVEFLAG_STRAFE_LEFT ))
		{
	
			AnimID = GetAnimationIDByName("strafe_left");

			return AnimID;
		}

		if ( ( GetMovementFlags() & MOVEFLAG_STRAFE_RIGHT ) )
		{
			
			AnimID = GetAnimationIDByName("strafe_right");

			return AnimID;
		}
		
	}

	std::string strAniName;
	ItemPrototype_Client* pkOffHandWeapon = GetOffHandWeaponPrototype();
	if (!GetUInt32Value(UNIT_FIELD_FORMDISPLAYID) && pkOffHandWeapon && pkOffHandWeapon->SubClass == ITEM_SUBCLASS_WEAPON_BAOZHU && iType != CMS_MOUNTRUN)
	{
		strAniName = "run_ball";
	}
	else
	{
		std::string strPrefix;
		strPrefix = GetWeaponPrefix();
		if (!isArmed())
			strPrefix = "no_weapon";

		if(GetUInt32Value(UNIT_FIELD_FORMDISPLAYID) || IsMountCreature() || IsMountByPlayer() || IsMountPlayer() || !strcmp(strPrefix.c_str(), "no_weapon"))
		{
			strPrefix="";
		}

		bool bPrefix = true;
		std::string strMoveString;
		if (iType == CMS_RUN) {
				strMoveString = "run";
		}
		else if(iType == CMS_WALK){
			strMoveString = "walk";
		}
		else if(iType == CMS_MOUNTRUN){
			bPrefix = false;
			//strMoveString = "sit01";
			strMoveString = m_strMoutSitAction;
		}

		const std::string sepString = "_";
		// CAG_RUN : CAG_WALK
		if (!strPrefix.empty() && bPrefix) {
			strAniName = strPrefix + sepString + strMoveString;
		}
		else {
			strAniName = strMoveString;
		}
	}

	AnimID = GetAnimationIDByName(strAniName.c_str());

	return AnimID;
}

AnimationID CPlayer::GetNormalAttackAnimation(UINT uSerial)
{
	if(!m_spActorManager)
		return NiActorManager::INVALID_SEQUENCE_ID;
	
	char szAnimName[2048];

	if(GetUInt32Value(UNIT_FIELD_FORMDISPLAYID))
	{
		NiSprintf(szAnimName, 128, "attack_01");
	}
	else
	{
		ItemPrototype_Client* pkOffHandWeapon = GetOffHandWeaponPrototype();
		if (pkOffHandWeapon && pkOffHandWeapon->SubClass == ITEM_SUBCLASS_WEAPON_BAOZHU)
		{
			std::string strPrefix = GetWeaponPrefix();
			if (strPrefix.empty())
			{
				return NiActorManager::INVALID_SEQUENCE_ID;
			}

			if(!strcmp(strPrefix.c_str(), "no_weapon"))
			{
				NiSprintf(szAnimName, 128, "attack_ball%02d", uSerial + 1);
			}
			else
			{
				NiSprintf(szAnimName, 128, "attack_ballsingle%02d", uSerial + 1);
			}
		}
		else
		{
			//if (IsMountFoot())
			//{
			//	NiStrcpy(szAnimName, 128, "ride_attack01");
			//}
			//else
			{
				std::string strPrefix = GetWeaponPrefix();
				if (!isArmed())
					strPrefix = "no_weapon";
				if( !strcmp(strPrefix.c_str(), "archer") )
				{
					NiStrcpy(szAnimName, 128, "archer_attack");
				}
				else if(!strcmp(strPrefix.c_str(), "no_weapon"))
				{
					NiSprintf(szAnimName, 128, "%s%02d", "attack", uSerial + 1);
				}
				else
				{
					if (strPrefix.empty())
					{
						return NiActorManager::INVALID_SEQUENCE_ID;
					}
					NiSprintf(szAnimName, 128, "%s_%02d", strPrefix.c_str(), uSerial + 1);
				}
			}
		}	
	}	
	
	AnimationID AnimID = GetAnimationIDByName(szAnimName);

	return AnimID;
}

const char* CPlayer::GetWeaponPrefix()
{
	ItemPrototype_Client* pkMainHandWeapon = GetMainHandWeaponPrototype();

	if( pkMainHandWeapon )
	{
		if( pkMainHandWeapon->SubClass == ITEM_SUBCLASS_WEAPON_BOW )
			return "archer";
		if( pkMainHandWeapon->InventoryType == INVTYPE_2HWEAPON )
			return "double_hand";

		ItemPrototype_Client* pkOffHandWeapon = GetOffHandWeaponPrototype();
		if( pkOffHandWeapon )
		{
			if( pkOffHandWeapon->InventoryType == INVTYPE_SHIELD )
				return "single_hand";
			else
				return "bimanual";
		}
		else
			return "single_hand";
	}
	return "no_weapon";
}

ItemPrototype_Client* CPlayer::GetMainHandWeaponPrototype()
{
	ui32 mainhandweapon_entryid = GetUInt32Value(PLAYER_VISIBLE_ITEM_6_0);
	return ItemMgr->GetItemPropertyFromDataBase(mainhandweapon_entryid);
}

ItemPrototype_Client* CPlayer::GetOffHandWeaponPrototype()
{
	ui32 offhandweapon_entryid = GetUInt32Value(PLAYER_VISIBLE_ITEM_7_0);
	return ItemMgr->GetItemPropertyFromDataBase(offhandweapon_entryid);
}

BOOL CPlayer::Action_PickItem(ui64 itemguid)
{
//	if(!SetCurState(CHARSTATE_PICK))
	{
		//return FALSE;
	}

	// Play Animation

	// 发送拾取物品的消息
	SetNextState(STATE_IDLE, CUR_TIME);

	return TRUE;
}

BOOL CPlayer::ActUpdate_PickItem(DWORD dwDeltaTime)
{
	return TRUE;
}

BOOL CPlayer::ActEnd_PickItem(ECharacterState NextAction)
{
	return TRUE;
}

void CPlayer::SetMoney(int nNum)
{
	//CCharacter::SetMoney(nNum);
	if(IsLocalPlayer())
	{
		SYState()->UI->GetUItemSystem()->SetItemMoney(nNum);
	}
}

void CPlayer::Rebirth()
{
	_asm nop
}

void CPlayer::ReleaseLock()
{
	m_pkPackageContainer->ReleaseAllLock();
}
CSlotContainer* CPlayer::GetContainer(BYTE ContainerIndex)
{
	CSlotContainer* pkContainer = NULL;

	switch(ContainerIndex)
	{
	/*case ICT_BAG:
		pkContainer = m_pkItemContainer;
		break;*/
	case ICT_EQUIP:
		pkContainer = m_pkEquipmentContainer;
		break;
	case ICT_PLAYERTRADE:
		pkContainer = m_pkTradeContainer;
		break;
	/*case ICT_BANK:
		pkContainer = m_pkBankContainer;
		break;*/
	}

	return pkContainer;
}

////请求NPC交易
//
//BOOL CPlayer::TradeWithNpcReq(unsigned int npcid, std::string &strKind, std::string &strAdd)
//{
//   if(IsLocalPlayer())
//   {
//	   PacketBuilder->SendMapTradeNpcRegMsg(npcid,strKind,strAdd);
//
//	   return TRUE;
//   }
//   else
//   {
//	   return FALSE;
//   }
//}

bool CPlayer::OnCreateFromUI(ui32 uiDisplayId, ui8 race, ui8 gender)
{
	std::string strFileName;
	if( race )
		ItemMgr->GetDisplayInfo(uiDisplayId, strFileName, race, gender, true);
	else
		ItemMgr->GetDisplayInfo(uiDisplayId, strFileName, GetRace(), GetGender(), true);

	NIASSERT(strFileName.size());
	if(strFileName.size() == 0)
		return FALSE;

	if(!CreateActor(strFileName.c_str(), true))
	{
		return false;
	}

	return true;
}

void CPlayer::OnCreate(class ByteBuffer* data, ui8 update_flags)
{
	CCharacter::OnCreate(data, update_flags);

	//装载模型
	CreatePlayer(GetUInt32Value(UNIT_FIELD_DISPLAYID));

	m_uiCurLevel = GetUInt32Value(UNIT_FIELD_LEVEL);

	if (GetUInt32Value(UNIT_FIELD_DEATH_STATE))
	{
		OnDeath(false);
	}else
	{
		ResetMovementFlags();
		// 重置状态
		ResetState();
		m_bIsDead = false;
	}

}

void CPlayer::OnActorLoaded(NiActorManager* pkActorManager)
{
    CCharacter::OnActorLoaded(pkActorManager);

    NiAVObject* pkBoneNode = 0;
    pkBoneNode = GetSceneNode()->GetObjectByName("Dummy01");
    if(pkBoneNode)
        RecursiveSetBones((NiNode*)pkBoneNode, 1);

    pkBoneNode = GetSceneNode()->GetObjectByName("Bip01 Spine1");
    if(pkBoneNode)
        RecursiveSetBones((NiNode*)pkBoneNode, 2);

    m_kNamePosOffset = 0.0f;
    NiAVObject* pkNameOffset = GetSceneNode()->GetObjectByName("Dummy02");
    if(pkNameOffset)
        m_kNamePosOffset = pkNameOffset->GetTranslate().z * m_fModleScale;

    //处理变身
  

	if (GetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID))
	{
		SetCurAnimation(m_strMoutSitAction.c_str(), 1.0f, TRUE,FALSE,1);
		ForceStateChange(STATE_MOUNT_IDLE,CUR_TIME);
	}

	

    GetSceneNode()->UpdateNodeBound();
    GetSceneNode()->UpdateProperties();
    GetSceneNode()->UpdateEffects();
    GetSceneNode()->Update(0.0f);

	OnFormModelChanged();
}

void CPlayer::OnFormModelChanged()
{
    if(!IsActorLoaded())
        return;

    //变身必须得按照服务器端发送下来的顺序
    CFormModel* pkFormModel;
    for(unsigned int i = 0; i < m_kFormModels.GetSize(); ++i)
    {
        pkFormModel = m_kFormModels.GetAt(i);
        if(!pkFormModel->IsFinished())
            break;

        unsigned int uiEntryId = pkFormModel->GetEntryId();
        if(uiEntryId == 0)
        {
            if(m_spAMSave)
            {
                m_spActorManager = m_spAMSave;
                NiNode* ActorRoot = NiDynamicCast(NiNode, m_spSceneNode);
                NIASSERT(ActorRoot);
                if(m_spMountNode)
                {
                    ActorRoot->DetachChild(m_spMountNode->GetNIFRoot());
                    g_ResMgr->FreeKfm(m_spMountNode);
					m_spMountNode=NULL;
                }

                //
                RemapSequenceID();
                RegistTextKey();

                m_uiCurrAnimID = NiActorManager::INVALID_SEQUENCE_ID;

                m_spAMSave->GetNIFRoot()->SetAppCulled(false);
                m_spMountNode=NULL;

                NiAVObject* pkNameOffset = m_spActorManager->GetNIFRoot()->GetObjectByName("Dummy02");
                if(pkNameOffset)
                {
					m_kNamePosOffset = pkNameOffset->GetTranslate().z * m_fModleScale;
                }

                SetMoveState(CMS_RUN);
                if(!IsDead())
                    SetNextState(STATE_IDLE, CUR_TIME);
                else
                    ForceStateChange(STATE_DEATH, CUR_TIME);

                if(IsMoving())
                {
                    SetMoveAnimation(GetMoveState());
                }

                m_fArmEvtTime = SYState()->ClientApp->GetAccumTime();

				bool in = false;
				if (!HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED) && !isWeaponAttached())
					in = true;

                if(DrawWeapon(in, m_fArmEvtTime))
                {
                    m_iArmEvent = HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED) ? 1 : 0;
                }

                m_spAMSave = NULL;
            }
        }
        else
        {
			if(!m_spMountNode || pkFormModel->isShapShift() )
            {
				pkFormModel->SetShapShift(false);
				NiNode* ActorRoot = NiDynamicCast(NiNode, m_spSceneNode);
				NIASSERT(ActorRoot);

				if(m_spMountNode)
				{
					ActorRoot->DetachChild(m_spMountNode->GetNIFRoot());
					g_ResMgr->FreeKfm(m_spMountNode);
					m_spMountNode = NULL;
				}
                m_spMountNode = pkFormModel->GetActorManager();

				if (!m_spAMSave)
				{
					m_spAMSave = m_spActorManager;
				}
                
                m_spActorManager->GetNIFRoot()->SetAppCulled(true);
                m_spActorManager = NULL;
                m_spActorManager = m_spMountNode;

                //
                RemapSequenceID();
                RegistTextKey();

                ActorRoot->AttachChild(m_spMountNode->GetNIFRoot());
                bool bSelectedUpdate = true;
                bool bRigid = false;
                ActorRoot->SetSelectiveUpdateFlags(bSelectedUpdate, true, bRigid);

                ActorRoot->UpdateNodeBound();
                ActorRoot->UpdateProperties();
                ActorRoot->UpdateEffects();
                ActorRoot->Update(0.0f);

                //m_spMountNode->Update(0.0f);

                m_uiCurrAnimID = NiActorManager::INVALID_SEQUENCE_ID;

                NiControllerSequence* Seq = SetCurAnimation("stand");
                if(Seq)
                {
                    NiTimeController::CycleType CT = Seq->GetCycleType();
                    if (CT == NiTimeController::CLAMP)
                    {
                        Seq->SetCycleType(NiTimeController::LOOP);
                    }
                }

                NiAVObject* pkNameOffset = m_spMountNode->GetNIFRoot()->GetObjectByName("Dummy02");
                if(pkNameOffset)
                {
                    m_kNamePosOffset = pkNameOffset->GetTranslate().z * m_fModleScale;
                }
            }

            SetMoveState(CMS_RUN);
            SetNextState(STATE_IDLE, CUR_TIME);
        }

        //更新阴影
		ReflashShadowCaster();

        NiDelete pkFormModel;
        m_kFormModels.SetAt(i, NULL);
    }

    m_kFormModels.Compact();

	for (int index = UNIT_FIELD_AURA ; index <= UNIT_FIELD_AURA_55 ; index++)
	{
		if(GetUInt32Value(index))
		{
			ui32 localid = m_AuraInfo[index - UNIT_FIELD_AURA].SpellId;
			ui32 spellid = GetUInt32Value(index);
			if (spellid != 0)
			{	
				m_pkAura[index - UNIT_FIELD_AURA]->SetVisibleEffect(m_spAMSave == NULL);
			}
		}
	}

	CaculateBoundBox();
	
}
void CPlayer::UpdateTeamGroupDate(ui64 guid, ui32 mask)
{
	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->UpdateTeamGroupDate(guid, mask);
	}
}
void CPlayer::OnValueChanged(UpdateMask* mask)
{
	CCharacter::OnValueChanged(mask);
	
// 检查是否使用了增加包裹的格子道具
// 如果使用了. 初始化新的包裹, 然后刷新包裹里的所有道具
//初始化包裹包括初始化ItemSoltContainer.里面的数据. 
//并且提供方法来通过ItemSoltContainer里面的solt来查找对应的格子道具里面格子的数据. 
//以及提供通过格子道具里面的位置来查找solt的方法. 

	if(IsLocalPlayer() && UInGame::Get())
	{
		UPackage * pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
		UBank   * pBank = INGAMEGETFRAME(UBank,FRAME_IG_BANKDLG);

		for( ui16 index = INVENTORY_SLOT_BAG_START; index < INVENTORY_SLOT_BAG_END; index++ )
		{
			int VisibleBase = PLAYER_FIELD_INV_SLOT_HEAD + (index * 2);
			if (mask->GetBit(VisibleBase))
			{
				ui64 bagguid = GetUInt64Value(VisibleBase);
				SYItemBag* pSYItem = (SYItemBag*)ObjectMgr->GetObject(bagguid);
				if (pSYItem)
				{
					//设置包裹栏背包对于的SYItemBag. 在对UI进行相关的格子的操作后在进行数据拷贝。
					pSYItem->SetBagSlot(index);
					m_pkPackageContainer->SetSYItemBagContainer(pSYItem);
					pPackage->ShowUseBag(index,pSYItem->GetCode());
				}else
				{
					//清除包裹栏背包的相关数据
					m_pkPackageContainer->SetBagContainerNull((ui8)index);
					pPackage->ShowUseBag(index,0);
				}
				//UI的设置。在拷贝数据进入Container. 否则UI报错。
				pPackage->SetCanUseSlort(m_pkPackageContainer->GetNumMaxSlotCount());
				//数据拷贝
				m_pkPackageContainer->CopyBagDataToContainer();

			}
		}
			
		//	道具包物品
		for( ui16 index = INVENTORY_SLOT_ITEM_START; index < INVENTORY_SLOT_ITEM_END; index++)
		{
			int VisibleBase = PLAYER_FIELD_INV_SLOT_HEAD + (index * 2);
			ui64 guid = GetUInt64Value(VisibleBase);

			if (mask->GetBit(VisibleBase))
			{
				SYItem* pItem = (SYItem*)ObjectMgr->GetObject(guid);
				ItemMgr->OnItemChange(INVENTORY_SLOT_NOT_SET, (ui8)index , guid);
				if(pItem)
					pItem->OnQuestQuery();
			}
		}
		ChatSystem->UpdataDALABA();
		
		uint32 value = GetUInt32Value(PLAYER_BYTES_2);
		uint8 pBankBagCount = (uint8)(value >> 16);

		if (pBankBagCount > 0)
		{
			for (ui16 index = BANK_SLOT_BAG_START; index < BANK_SLOT_BAG_START + pBankBagCount; index++)
			{
				//包位是否激活.
				if (index < BANK_SLOT_BAG_END)
				{
					int VisibleBase = PLAYER_FIELD_INV_SLOT_HEAD + (index * 2);
					ui64 guid =  GetUInt64Value(VisibleBase);
					SYItemBag* pSYBankBag = (SYItemBag*)ObjectMgr->GetObject(guid);
					if (!pSYBankBag) pBank->ShowUseBagIcon(index,0,TRUE);
					if (mask->GetBit(VisibleBase))
					{
						if (pSYBankBag)
						{
							//设置包裹栏背包对于的SYItemBag. 在对UI进行相关的格子的操作后在进行数据拷贝。
							pSYBankBag->SetBagSlot(index);
							m_pkBankContainer->SetSYItemBagContainer(pSYBankBag);
							pBank->ShowUseBagIcon(index,pSYBankBag->GetCode(),TRUE);
						}else
						{
							//清除包裹栏背包的相关数据
							m_pkBankContainer->SetBagContainerNull((ui8)index);
							pBank->ShowUseBagIcon(index,0,TRUE);
						}
						
						//UI的设置。
						pBank->SetMaxSlot(m_pkBankContainer->GetNumMaxSlotCount());
						//数据拷贝。
						m_pkBankContainer->CopyBagDataToContainer();
					}
				}
			}
		}

		// 显示银行默认的格子里的道具
		for (ui16 index = BANK_SLOT_ITEM_START; index < BANK_SLOT_ITEM_END; index++)
		{
			int VisibleBase = PLAYER_FIELD_INV_SLOT_HEAD + (index * 2);
			ui64 guid = GetUInt64Value(VisibleBase);
			if (mask->GetBit(VisibleBase))
			{
				SYItem* pItem = (SYItem*)ObjectMgr->GetObject(guid);
				ItemMgr->OnBankItemChange(INVENTORY_SLOT_NOT_SET, (ui8)index , guid);
			}	
		}
	}

	//BOOL bUpdateEQUI = FALSE;
	BOOL bArm = HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);

	BOOL bUpdateProfession = false;
	uint32 lastUpdate = 0;
	for ( uint32 Pos = PLAYER_SKILL_INFO_1_1; Pos <  PLAYER_CHARACTER_POINTS1; Pos++ )
	{
		if ( mask->GetBit(Pos) )
		{
			uint32 spellLock = PLAYER_SKILL_INFO_1_1 + ((Pos - PLAYER_SKILL_INFO_1_1)/3) * 3;

			if ( lastUpdate == spellLock )
			{
				continue;
			}

			lastUpdate = spellLock;

			uint32 spellLockId = GetUInt32Value(spellLock++);
			if ( !spellLockId )
			{
				continue;
			}

			if ( spellLockId & 0x10000 )
			{
				spellLockId &= ~0x10000;
				
				uint32 spellLockFocus = GetUInt32Value(spellLock++);
				uint32 spellLockCur = spellLockFocus;
				spellLockCur = spellLockCur << 16;
				spellLockCur = spellLockCur >> 16;
				uint32 spellLockMax = spellLockFocus;
				spellLockMax = spellLockMax>> 16;


				PlayerProfessionLevel plPro;
				plPro.current = spellLockCur;
				plPro.Max = spellLockMax;
				bool bUpdate = false;
				if ( IsLocalPlayer() && (m_mapProfession.find(spellLockId) != m_mapProfession.end()) )
				{
					if ( plPro.current > m_mapProfession[spellLockId].current )
					{
						bUpdate = true;
					}
				}
				m_mapProfession[spellLockId] = plPro;
				UISkill* pSkill = INGAMEGETFRAME(UISkill, FRAME_IG_SKILLDLG);
				if ( pSkill )
				{
					pSkill->RefreshProf();
					if (bUpdate) pSkill->OnProfLevelChange(spellLockId, plPro.current);
				}
			}
			
		}
		

	}
	
	/*if( mask->GetBit( PLAYER_SKILL_INFO_1_1 ) )
	{
		ui32 isflying = GetUInt32Value( PLAYER_SKILL_INFO_1_1 );
	}*/
	
	if( mask->GetBit( PLAYER_FIELD_IS_FLYING ) )
	{
		ui32 isflying = GetUInt32Value( PLAYER_FIELD_IS_FLYING );
		SetFly(isflying);
	}
	if ( mask->GetBit(PLAYER_GUILDID) )
	{
		ui32 guild_id = GetUInt32Value(PLAYER_GUILDID);
		if (IsLocalPlayer())
		{
			SocialitySys->GetGuildSysPtr()->SetLocalGuildId(guild_id);
		}
		if (guild_id)
		{
			if (IsLocalPlayer())
			{
				SocialitySys->GuildSetCreate(guild_id);
			}
			else
			{
				if (!m_guildname.size())
				{
					std::string Name;
					if (SocialitySys->GetGuildSysPtr()->GetGuildName(guild_id, Name))
					{
						SetGuildName(Name);
					}
					else
					{
						SocialitySys->GetGuildSysPtr()->SendMsgGuildQuery(guild_id);
					}
				}
			}
		}
		else
		{
			if (IsLocalPlayer())
			{
				SocialitySys->GuildSetEmpty();
			}
			ClearGuildName();
		}
	}
	if ( mask->GetBit(PLAYER_FIELD_GUILD_SCORE) )
	{
		ui32 guild_Score = GetUInt32Value(PLAYER_FIELD_GUILD_SCORE);
		if (IsLocalPlayer())
		{
			SocialitySys->GetGuildSysPtr()->SetGuildScore(guild_Score);
		}
	}
	//	装备显示信息

	
	
	

	for( ui16 index = EQUIPMENT_SLOT_START; index < EQUIPMENT_SLOT_END; index++ )
	{	
		ui32 GuidBase = PLAYER_FIELD_INV_SLOT_HEAD + (index * 2);
		ui32 visiblebase = PLAYER_VISIBLE_ITEM_1_0 + (index * 12);

		if( mask->GetBit(visiblebase)  )
		{
			ui64 Guid = GetUInt64Value(GuidBase);
			uint32 entry = GetUInt32Value(visiblebase);
			uint32 jinglian_level = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + index * 12 + 1);
			uint32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (index * 12) + 9 ;
			uint32 effect_displayid = 0;
			GetRefineItemEffect(GetUInt32Value(visiblebase), jinglian_level, effect_displayid);

			m_Equiped[index].equipitementry = entry;
			m_Equiped[index].jinglianlevel = jinglian_level;
			if (!m_bIsShowHelm && (index == EQUIPMENT_SLOT_HEAD || index == EQUIPMENT_HOUNTER_TOUBU) )
			{
				continue ;
			}
			


			SetUInt32Value(itemeffect,effect_displayid);

			UViewPlayerEquipment* pViewPlayerEqu = INGAMEGETFRAME(UViewPlayerEquipment, FRAME_IG_VIEWPLAYEREQUIPDLG);
			if (pViewPlayerEqu && pViewPlayerEqu->IsVisible())
				pViewPlayerEqu->UpDateUNiavobject(this, visiblebase, GetUInt32Value(visiblebase), index);


			if ((PLAYER_VISIBLE_ITEM_7_0 == visiblebase || PLAYER_VISIBLE_ITEM_6_0 == visiblebase) )
			{
				ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(entry);
				bool bCanAttach = (ItemInfo && ItemInfo->InventoryType != INVTYPE_BAOZHU);
				if (!bArm && bCanAttach)
				{
					if (PLAYER_VISIBLE_ITEM_6_0 == visiblebase)
						ItemMgr->OnEquipmentChange(GetGUID(), entry, index, AS_BacksideRight);
					else if (PLAYER_VISIBLE_ITEM_7_0 == visiblebase)
						ItemMgr->OnEquipmentChange(GetGUID(), entry, index, AS_BacksideLeft);
				}
				else
				{
					ItemMgr->OnEquipmentChange(GetGUID(), entry, index);
					if (entry == 0)
					{
						if (PLAYER_VISIBLE_ITEM_6_0 == visiblebase)
							ItemMgr->OnEquipmentChange(GetGUID(), entry, index, AS_BacksideRight);
						else if (PLAYER_VISIBLE_ITEM_7_0 == visiblebase)
							ItemMgr->OnEquipmentChange(GetGUID(), entry, index, AS_BacksideLeft);
					}
				}

				m_bArmed = bArm;
				m_bWeaponAttached = bArm?TRUE:FALSE;

			}
			else
			{
				bool bChange = true;
				if( m_bShowShizhuang )
				{
					if(index == INVTYPE_HEAD)
					{
						uint32 Testentry = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_TOUBU * 12));
						if(Testentry)
							bChange = false;
					}
					else if(index == INVTYPE_SHOULDERS)
					{
						uint32 Testentry = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_JIANBANG * 12));
						if(Testentry)
							bChange = false;
					}
					else if(index == INVTYPE_CHEST)
					{
						uint32 Testentry = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_SHANGYI * 12));
						if(Testentry)
							bChange = false;
					}
					else if(index == INVTYPE_TROUSERS)
					{
						uint32 Testentry = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_XIAYI * 12));
						if(Testentry)
							bChange = false;
					}
					else if(index == INVTYPE_BOOTS)
					{
						uint32 Testentry = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_XIEZI * 12));
						if(Testentry)
							bChange = false;
					}
				}

				if(bChange) 
				{
					ItemMgr->OnEquipmentChange(GetGUID(), entry, index);

					//更新相关头像
					TeamShow->UpdateObjHead(GetGUID(), visiblebase, GetUInt32Value(visiblebase),index);

				}


				// 如果是怪物装，还原
				if(entry == 0 || !m_bShowShizhuang)
				{
					bool bHunterEquip = false;
					ui32 SlotIndex = 0;
					if(index == INVTYPE_HOUNTER_TOUBU)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_HEAD;
					}
					else if(index == INVTYPE_HOUNTER_JIANBANG)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_SHOULDERS;
					}
					else if(index == INVTYPE_HOUNTER_SHANGYI)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_CHEST;
					}
					else if(index == INVTYPE_HOUNTER_XIAYI)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_TROUSERS;
					}
					else if(index == INVTYPE_HOUNTER_XIEZI)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_BOOTS;
					}
					if(bHunterEquip)
					{
						ui32 visiblebases = PLAYER_VISIBLE_ITEM_1_0 + (SlotIndex * 12);
						uint32 entrys = GetUInt32Value(visiblebases);
						ItemMgr->OnEquipmentChange(GetGUID(), entrys, SlotIndex);

						TeamShow->UpdateObjHead(GetGUID(), visiblebases,entrys,SlotIndex);

					}
				}
				if (!m_bShowShizhuang)
				{
					ItemMgr->OnEquipmentChange(GetGUID(), 0, INVTYPE_HOUNTER_BEIBU);
					TeamShow->UpdateObjHead(GetGUID(), PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_BEIBU * 12),0,INVTYPE_HOUNTER_BEIBU);
				}
			}

		}

	}
	//时装
	if (mask->GetBit(PLAYER_VISIBLE_ITEM_SHOW_SHIZHUANG))
	{
		bool LastHelmChange  = m_bIsShowHelm;
		m_bIsShowHelm = GetUInt32Value(PLAYER_VISIBLE_ITEM_SHOW_SHIZHUANG) & 2;

		bool bShowShizhuang = m_bShowShizhuang;
		ui32 value = GetUInt32Value(PLAYER_VISIBLE_ITEM_SHOW_SHIZHUANG);
		m_bShowShizhuang = (value & 1)?true:false; 
		
		if (bShowShizhuang != m_bShowShizhuang)
		{
			ItemMgr->SetIsShowShizhuang(this, m_bShowShizhuang);
		}
		if (LastHelmChange != m_bIsShowHelm)
		{
			if (m_bIsShowHelm)
			{
				ui32 visiblebase = PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_TOUBU * 12);
				uint32 entry = GetUInt32Value(visiblebase);

				if (m_bShowShizhuang &&  entry)
				{
					ItemMgr->OnEquipmentChange(this, entry, INVTYPE_HOUNTER_TOUBU);		
				}else
				{
					entry = GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HEAD * 12));
					ItemMgr->OnEquipmentChange(this, entry, INVTYPE_HEAD);	
				}


			}else
			{
				ItemMgr->OnEquipmentChange(this, 0 ,INVTYPE_HEAD);
				ItemMgr->OnEquipmentChange(this, 0 ,INVTYPE_HOUNTER_TOUBU);
			}
		}


		if (TeamShow)
		{
			TeamShow->SetHeadEQShow(m_bIsShowHelm, GetGUID());
		}

		UViewPlayerEquipment* pViewPlayer = INGAMEGETFRAME(UViewPlayerEquipment, FRAME_IG_VIEWPLAYEREQUIPDLG);
		if (pViewPlayer && pViewPlayer->IsVisible())
		{
			pViewPlayer->SetShowHeadEQ(m_bIsShowHelm, GetGUID());
		}

		if (IsLocalPlayer())
		{
			UEquipment* pEquipment = INGAMEGETFRAME(UEquipment,FRAME_IG_PLAYEREQUIPDLG);
			if (pEquipment)
			{
				pEquipment->SetShowEQHead(m_bIsShowHelm, GetGUID());
			}
		}

	}

	/*UViewPlayerEquipment* pViewPlayerEqu = INGAMEGETFRAME(UViewPlayerEquipment, FRAME_IG_VIEWPLAYEREQUIPDLG);
	CPlayer* pPkPlayer = (CPlayer*)pViewPlayerEqu->GetUICharacter();

	if ( pPkPlayer )
	{
		NiAVObject* pkHelmUI = pPkPlayer->GetSceneNode()->GetObjectByName( "Helmet_Node" );
		if ( pkHelmUI )
		{
			pkHelmUI->SetAppCulled( !m_bIsShowHelm );
		}
	}*/

	if (mask->GetBit(UNIT_FIELD_FFA))
	{
		uint32 ffaA = GetUInt32Value(UNIT_FIELD_FFA);
		if (ffaA == 11)
			m_ffa = _TRAN(" 防守方");
		else
		if (ffaA == 12)
			m_ffa = _TRAN(" 进攻方");
		else
		if (ffaA == 13)
			m_ffa = _TRAN(" 中立方");
		else
		{
			m_ffa = "";
		}
	}

	if (mask->GetBit(PLAYER_FLAGS))
	{
		m_PlayerState.clear();
		bool bafk= HasFlag(PLAYER_FLAGS, PLAYER_FLAG_AFK) ;
		bool bdnd = HasFlag(PLAYER_FLAGS, PLAYER_FLAG_DND);
		if (bafk)
		{
			m_PlayerState += _TRAN("(暂离)");
		}
		if (bdnd)
		{
			m_PlayerState += _TRAN("(勿扰)");
		}
	}

	//	金币
	if( mask->GetBit(PLAYER_FIELD_COINAGE) )
	{
		SetMoney(GetUInt32Value(PLAYER_FIELD_COINAGE));
		if ( !m_bInitMoney && IsLocalPlayer() )
		{
			SYState()->IAudio->PlaySound( "Sound/UI/GoldChange.wav", NiPoint3::ZERO, 1.f, 1 );
		}
		
		m_bInitMoney = false;
	}

	if (m_bArmed != bArm && !this->IsDead())
	{
		m_fArmEvtTime = SYState()->ClientApp->GetAccumTime();
		if (bArm)
		{
			if (DrawWeapon(false, m_fArmEvtTime))
			{
				m_iArmEvent = 1;
			}
		}
		else
		{
			if (DrawWeapon(true, m_fArmEvtTime))
			{
				m_iArmEvent = 2;
			}
		}

		m_bArmed = bArm;
	}

	if( mask->GetBit(UNIT_FIELD_HEALTH) )
	{
		UpdateTeamGroupDate(GetGUID(), GROUP_UPDATE_FLAG_HEALTH);
	}
	if( mask->GetBit(UNIT_FIELD_POWER1) )
	{
		UpdateTeamGroupDate(GetGUID(), GROUP_UPDATE_FLAG_POWER);
	}
	if( mask->GetBit(UNIT_FIELD_MAXHEALTH) )
	{
		UpdateTeamGroupDate(GetGUID(), GROUP_UPDATE_FLAG_MAXHEALTH);
	}
	if( mask->GetBit(UNIT_FIELD_MAXPOWER1) )
	{
		UpdateTeamGroupDate(GetGUID(), GROUP_UPDATE_FLAG_MAXPOWER);
	}

	if (mask->GetBit(PLAYER_FLAGS))
	{
		if(HasFlag(PLAYER_FLAGS, PLAYER_FLAG_GM))
		{
			//m_spNameRef->stName.bGM = true;
		}
	}
	if ( !IsLocalPlayer())
	{
		if(mask->GetBit(UNIT_FIELD_FLAGS))
		{
			//进入战斗状态
			if(HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT) && !(m_UnitFlag & UNIT_FLAG_COMBAT))
			{
				DamageMgr->OnStartAnalyst( GetGUID() );
				m_UnitFlag |= UNIT_FLAG_COMBAT;
			}else if(!(HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT)) && (m_UnitFlag & UNIT_FLAG_COMBAT))
			{
				DamageMgr->OnEndAnalyst( GetGUID() );
				m_UnitFlag &= ~UNIT_FLAG_COMBAT ;
			}
		}
	}
	
	

	if( mask->GetBit(UNIT_FIELD_LEVEL) )
	{
		ui32 uiNewLevel = GetUInt32Value(UNIT_FIELD_LEVEL);
		OnLevelupStatus(uiNewLevel);

		UpdateTeamGroupDate(GetGUID(), GROUP_UPDATE_FLAG_LEVEL);
	}

	if(mask->GetBit(UNIT_FIELD_MOUNTDISPLAYID))
	{
		

		// 骑宠
		ui32 entry = GetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID);
		if(entry == 0)
		{
			//((SYMount*)m_pkMountFoot)->SetVisible(!m_bMount);
			//ResetMovementFlags();
			//m_bPathMove = false;
			//StopMove();
			SetPosition(GetPosition());
			SetMoveState(CMS_RUN);
			m_fMountOffset = NiPoint3::ZERO;
			if(m_pkMountCreature)
			{
				NiDelete m_pkMountCreature;
				m_pkMountCreature = NULL;
				SYState()->IAudio->PlaySound("Sound/items/UnMount.wav", NiPoint3::ZERO, 1.0f, 1 );
			}

			m_strMoutSitAction = "sit01";

			if(IsMoving())
			{
				if (m_bPathMove)
				{
					PathMoveEnd();
				}else
				{
					StopMove();
				}
				//SetMoveAnimation(GetMoveState());
				//SetNextState(STATE_MOVE, CUR_TIME);
			}
			ForceStateChange(STATE_IDLE, CUR_TIME);

			//SetFly( false );

		}
		else
		{
			ui32 displayid;
			ItemMgr->GetCreatureModelId(entry, displayid);
			std::string strFileName;
			ItemMgr->GetDisplayInfo(displayid, strFileName, GetRace(), GetGender(), true);
			m_pkMountCreature = NiNew SYMount(GetGUID(), this );
			((SYMount*)m_pkMountCreature)->Load(strFileName.c_str());

			if(m_eShadowType == CShadowGeometry::ST_DYNAMIC)
				m_pkMountCreature->ChangeShadowType(CShadowGeometry::ST_DYNAMIC);
			else
				m_pkMountCreature->ChangeShadowType(CShadowGeometry::ST_NULL);

			
			if ( IsFlying() )
			{
				NiPoint3 ptPos = GetPosition();
				ptPos.z += 1.f;
				SetPosition( ptPos );
				if ( IsLocalPlayer() )
				{
					m_pkMountCreature->SetFly( true );
				}
			}
			
			m_pkMountCreature->SetPosition(GetPosition());
			SetPhyFly( true );
			SetMoveState(CMS_MOUNTRUN);
			ForceStateChange(STATE_MOUNT_IDLE,CUR_TIME);
			NiNode* pkPetNode = m_pkMountCreature->GetPetNode();
			if(pkPetNode )
			{
				m_fMountOffset = pkPetNode->GetWorldTranslate();
			}

			std::vector<NiGeometry*> stGeometry;
			RecursiveGetGeometry(m_pkMountCreature->GetSceneNode(), stGeometry);
			for(unsigned int ui = 0; ui < stGeometry.size(); ui++)
			{
				NiExtraData* pkExtraData = stGeometry[ui]->GetExtraData("SitAction");
				if(pkExtraData)
				{
					NiStringExtraData* pkStringData = (NiStringExtraData*)pkExtraData;
					m_strMoutSitAction = pkStringData->GetValue();
					break;
				}
			}
			//SetFly( true );
		}

		if(m_pkMountCreature)
			m_pkMountCreature->ReflashShadowCaster();
	}
	if(mask->GetBit(UNIT_FIELD_FORMDISPLAYID))
	{
		// 变身
		ui32 entry = GetUInt32Value(UNIT_FIELD_FORMDISPLAYID);
        std::string strFileName;
		if(entry != 0)
		{
            ui32 displayid;
            ItemMgr->GetCreatureModelId(entry, displayid);
            ItemMgr->GetDisplayInfo(displayid, strFileName, GetRace(), GetGender(), true);
		}

        CFormModel* pkFormModel = NiNew CFormModel(GetGUID(), strFileName.c_str(), entry);
		if ( entry != 0 )
		{
			pkFormModel->SetShapShift( true );
		}
		
        m_kFormModels.Add(pkFormModel);
        g_pkTaskManager->PushTask(pkFormModel);
		OnDisplayedWaitSound(entry);
	}
	if(mask->GetBit(PLAYER_FOOT))
	{
		ui64 guid = GetUInt64Value(PLAYER_FOOT);
		if(guid)
		{
			CPlayer* pkChar = (CPlayer*)ObjectMgr->GetObject(guid);
			if(pkChar)
			{
				pkChar->m_pkMountHead = this;
				m_pkMountFoot = pkChar;
				NiNode* pkPetNode = m_pkMountFoot->GetPetNode();
				if(pkPetNode )
				{
					m_fMountOffset = pkPetNode->GetWorldTranslate();
				}

				SetMoveState(CMS_MOUNTRUN);
				SetNextState(STATE_MOUNT_IDLE, CUR_TIME);


				std::vector<NiGeometry*> stGeometry;
				RecursiveGetGeometry(m_pkMountFoot->GetSceneNode(), stGeometry);
				for(unsigned int ui = 0; ui < stGeometry.size(); ui++)
				{
					NiExtraData* pkExtraData = stGeometry[ui]->GetExtraData("SitAction");
					if(pkExtraData)
					{
						NiStringExtraData* pkStringData = (NiStringExtraData*)pkExtraData;
						m_strMoutSitAction = pkStringData->GetValue();
						break;
					}
				}
			}
		}
		else
		{
			m_pkMountFoot = NULL;
			m_fMountOffset = NiPoint3::ZERO;
			SetMoveState(CMS_RUN);
			SetNextState(STATE_IDLE, CUR_TIME);
		}
	}
	if(mask->GetBit(PLAYER_HEAD))
	{
		ui64 guid = GetUInt64Value(PLAYER_HEAD);
		if(guid)
		{
			CPlayer* pkChar = (CPlayer*)ObjectMgr->GetObject(guid);
			if(pkChar)
			{
				pkChar->m_pkMountFoot = this;
				m_pkMountHead = pkChar;
				pkChar->SetNextState(STATE_MOUNT_IDLE, CUR_TIME);
				pkChar->SetMoveState(CMS_MOUNTRUN);
				NiNode* pkPetNode = GetPetNode();
				if(pkPetNode == NULL)
					return;
				m_fMountOffset = pkPetNode->GetWorldTranslate();
			}
		}
		else
		{
			m_pkMountHead = NULL;
			m_fMountOffset = NiPoint3::ZERO;
			SetMoveState(CMS_RUN);
			SetNextState(STATE_IDLE, CUR_TIME);
		}
	}

	//师徒称谓变化
	if (mask->GetBit(PLAYER_FIELD_TEACHER))
	{
		
		
	}
	if (mask->GetBit(PLAYER_FIELD_STUDENT))
	{
		
	}
	//
	if (mask->GetBit(UNIT_FIELD_DEATH_STATE))
	{
		ui32 state = GetUInt32Value(UNIT_FIELD_DEATH_STATE) ;
		if (state != ALIVE)
		{
			OnDeath();
		}else
		{
			OnAlive();
		}
	}

	if (mask->GetBit(PLAYER_CHOSEN_TITLE))
	{
		ui32 curTitleID = GetUInt32Value(PLAYER_CHOSEN_TITLE);
		if (curTitleID)
		{
			//查找对应的称谓
			//然后赋值
			TitleInfo* pkTitle = g_pkPlayerTitle->GetTitleInfo(curTitleID);
			if (pkTitle)
			{
				m_GuidTitle  = pkTitle->stTitle.c_str() ;
			}else
			{
				ULOG("do not find id %d title", curTitleID);
			}

		}else
		{
			m_GuidTitle = "";
		}
		
	}

	if(mask->GetBit(OBJECT_FIELD_SCALE_X))
	{
		float scale = GetFloatValue(OBJECT_FIELD_SCALE_X);
		if(m_spMountNode)
		{
			NiAVObject* pkNameOffset = m_spMountNode->GetNIFRoot()->GetObjectByName("Dummy02");
			if(pkNameOffset)
			{
				m_kNamePosOffset = pkNameOffset->GetTranslate().z * scale;
			}
		}
	}

	if( HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED) )
	{
		if( !m_bisStuned )
		{
			
			if ( !IsJumping()&& !IsDead())
			{
				SetIdleAnimation( CAG_STUNNED );
			}

			m_bisStuned = true;
		}
	}
	else
	{
		if ( m_bisStuned )
		{
			m_bisStuned = false;
			if ( !IsJumping() && !IsDead())
			{
				SetIdleAnimation( CAG_IDLE );
			}
			
		}
		
	}
}

void CPlayer::OnLevelupStatus(ui32 newLevel)
{
	if (newLevel > m_uiCurLevel)
	{
		const INT LoopCount = 2;
		CSceneEffect* pkLevelEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect("EA0001.nif",true, true);
		if (pkLevelEffect)
		{	
			NIASSERT(pkLevelEffect);
			pkLevelEffect->AttachToSceneObject(GetGUID());
			pkLevelEffect->SetMaxLifeByEffectLife(LoopCount);
			SYState()->IAudio->PlaySound("Sound/char/LevelUp.wav", NiPoint3::ZERO, 1.0f, 1 );
		}
	}

	m_uiCurLevel = newLevel;
}

char* CPlayer::GetHeadImageFile(CPlayer* pPlayer,uint8 mRace,uint8 mGender) //设置人物的头像
{
	static char* strfile = NULL ;

	if(pPlayer)
	{
		mRace = pPlayer->GetRace();
		mGender = pPlayer->GetGender();
	}

	if (mGender == GENDER_FEMALE)
	{
		if (mRace == RACE_YAO)
		{
			strfile = "Data\\UI\\Icon\\Player\\Y_Fmale.png";
		}else if (mRace == RACE_REN)
		{
			strfile = "Data\\UI\\Icon\\Player\\R_Fmale.png";
		}else if (mRace == RACE_WU)
		{
			strfile = "Data\\UI\\Icon\\Player\\W_Fmale.png";
		}
	}else if (mGender == GENDER_MALE)
	{
		if (mRace == RACE_YAO)
		{
			strfile = "Data\\UI\\Icon\\Player\\Y_Male.png";
		}else if (mRace == RACE_REN)
		{
			strfile = "Data\\UI\\Icon\\Player\\R_Male.png";
		}else if (mRace == RACE_WU)
		{
			strfile = "Data\\UI\\Icon\\Player\\W_Male.png";
		}
	}

	return strfile;
}

BOOL CPlayer::MoveTo(const NiPoint3& Target, UINT Flag, bool findpath /*= true*/)
{
	//// Test
	//if(!m_PathFinder)
	//	m_PathFinder = new CPathFinder;	

	//m_PathFinder->GeneratePathWP((int)GetPosition().x, (int)GetPosition().y, (int)GetPosition().z, (int)Target.x, (int)Target.y, (int)Target.z);
	//m_MoveDestPos = m_PathFinder->GetNextPathPoint();
	//m_MoveDestPos = Target;

	StartForward();
	PathMoveStart(Target, Flag);
	

	BOOL b = CCharacter::MoveTo(Target, Flag, findpath);

	

	return b;
}

void CPlayer::MoveTo()
{
	if (isMount() && GetCurPostureState() != STATE_MOUNT_MOVE)
	{
		SetNextState(STATE_MOUNT_MOVE, CUR_TIME);
		if(IsMountCreature())
			GetMountCreature()->SetNextState(STATE_MOUNT_MOVE, CUR_TIME);
	}
	else if( !isMount() && GetCurPostureState() != STATE_MOVE)
	{
		SetNextState(STATE_MOVE, CUR_TIME);
	}
}

void CPlayer::StopMove()
{
	if(IsMoving())
	{
		CCharacter::StopMove();

		StopMountMove();
	}
}

bool CPlayer::GetPickBound(NiBound& kBound) 
{
	if (!GetSceneNode())
		return false;

	kBound.SetRadius(0.0f);

	
	CAnimObject::GetPickBound(kBound);


	//if player has mout then run down code
	if ( m_pkMountCreature && m_pkMountCreature->GetSceneNode())
	{
		NiBound kMountBound;
		kMountBound.SetRadius(0.0f);
		
		m_pkMountCreature->GetPickBound(kMountBound);

		const NiBound kMBound = kMountBound;
		kBound.Merge(&kMBound);
	}

	return kBound.GetRadius() != 0.0f;
}

bool CPlayer::IsPick(NiPick& kPicker,const NiPoint3& kOrig,const NiPoint3& kDir)
{
	if (!GetSceneNode())
		return false;

	//const char* list[6] = { "Chest", "Gloves", "Pants", "Boot", "Face", "Hair" };
	//for (unsigned int ui = 0; ui < 6; ui++)
	//{
	//	NiAVObject* pkObj = GetSceneNode()->GetObjectByName(list[ui]);
	//	if (pkObj && pkObj->IsVisualObject())
	//	{
	//		kPicker.SetTarget( pkObj );
	//		if ( kPicker.PickObjects(kOrig, kDir) )
	//			return true;
	//	}
	//}
	if ( !CAnimObject::IsPick(kPicker, kOrig, kDir) )
	{
		if ( m_pkMountCreature )
			return m_pkMountCreature->IsPick(kPicker, kOrig, kDir);
	}
	else
		return true;


	////if player has mout then run down code
	//if ( m_pkMountCreature && m_pkMountCreature->GetSceneNode())
	//{
	//	//------------------------------------------------------------------------------------------------------
	//	NiNode* pkNode = NiDynamicCast(NiNode,m_pkMountCreature->GetSceneNode()->GetObjectByName("Scene Root"));
	//	if (!pkNode)
	//		return false;

	//	unsigned int ui = 0;
	//	for (ui; ui < pkNode->GetArrayCount(); ui++)
	//	{
	//		NiAVObject* pkObj = pkNode->GetAt(ui);

	//		if (GetUInt32Value(UNIT_FIELD_PETNUMBER) && pkObj->GetName().Equals("Dummy01"))
	//			continue;

	//		if (pkObj&& pkObj->IsVisualObject())
	//		{
	//			kPicker.SetTarget( pkObj );
	//			if ( kPicker.PickObjects(kOrig, kDir) )
	//				return true;
	//		}
	//	}

	//}

	return false;
}

BOOL CPlayer::GetCanTeacherOrP()
{
	if (GetUInt32Value(PLAYER_FIELD_TEACHER) ||GetUInt32Value(PLAYER_FIELD_STUDENT))
	{
		return FALSE;
	}
	return TRUE;
}

void CPlayer::OnDeath(bool bDeadNow /*= true*/)
{
	StopMove();
	CCharacter::OnDeath(bDeadNow);

	m_bFirstSample = true;
}

void CPlayer::DrawName(const NiCamera* Camera)
{
	if( /*m_fChatTime > 0.f ||*/ !m_spNameRef )
		return;
	float fNamePosOffset = m_kNamePosOffset;

	m_spNameRef->type = GOT_PLAYER;

	NiPoint3 kPos;
	std::string Name = GetName();
	if(IsMountCreature())
	{
		NiNode* pkPetNode = m_pkMountCreature->GetPetNode();
		if(pkPetNode == NULL)
			return;
		NiAVObject* pObject = GetSceneNode()->GetObjectByName("Bip01");
		if (GetSceneNode() && pObject)
		{
			NiPoint3 TempPos = pObject->GetTranslate();
			TempPos.z *= m_fModleScale;
			kPos = pkPetNode->GetWorldTranslate() - TempPos;
		}
		kPos.z += fNamePosOffset;
	}
	else if(IsMountPlayer())
	{
		NiNode* pkPetNode = m_pkMountFoot->GetPetNode();
		if(pkPetNode == NULL)
			return;

		NiAVObject* pObject = GetSceneNode()->GetObjectByName("Bip01");
		if (GetSceneNode() && pObject)
		{
			NiPoint3 TempPos = pObject->GetTranslate();
			TempPos.z *= m_fModleScale;
			kPos = pkPetNode->GetWorldTranslate() - TempPos;
		}
		kPos.z += fNamePosOffset;
		Name += " & " ;
		Name += m_pkMountFoot->GetName();
	}
	else if(IsMountByPlayer())
	{
		return;
	}
	else
	{
		kPos = GetPosition();
		kPos.z += fNamePosOffset;
	}

	WorldPtToUIPt(kPos, Camera);
	
	bool IsShowHPBar = false;
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)ObjectMgr->GetLocalPlayer();
	unsigned int uiColor;
	if(pkLocalPlayer==this)
		uiColor = D3DCOLOR_ARGB(255, 255, 255, 255);
	else if(IsFriendly(pkLocalPlayer))
	{
		uiColor = D3DCOLOR_ARGB(255, 51, 153, 255);
		if (SocialitySys->GetTeamSysPtr()->InTeam(GetGUID()))
		{
			uiColor = D3DCOLOR_ARGB(255, 102, 255, 255);
		}
	}
	else if( HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT) || GetUInt32Value( UNIT_FIELD_FFA) != 0)
	{
		uiColor = D3DCOLOR_ARGB(200, 255, 0, 0);
		IsShowHPBar = true;
	}
	else if(IsHostile(pkLocalPlayer))
	{
		uiColor = D3DCOLOR_ARGB(200, 255, 0, 0);
		IsShowHPBar = true;
	}
	else if(IsDead())
		uiColor = D3DCOLOR_ARGB(200, 255, 0, 0);
	else
		uiColor = D3DCOLOR_ARGB(200, 255, 255, 0);

	uint8 zodiac = (uint8)GetUInt32Value(PLAYER_FIELD_SHENGXIAO);

	CharName* NewName = &m_spNameRef->stName;

	NewName->ffa = GetUInt32Value(UNIT_FIELD_FFA);
	NewName->zodiac = zodiac;

	if(SocialitySys)
	{
		ui32 style = 0,  color = 0;
		if(SocialitySys->GetGuildSysPtr()->GetGuildEmblem(GetUInt32Value(PLAYER_GUILDID), style, color))
		{
			m_spNameRef->SetEmblem( style, color);
		}
		UINT mask = 8;
		if(!SocialitySys->GetTeamSysPtr()->GetTargetIcon(&mask, GetGUID()))
		{
			mask = 8;
		}
		NewName->GroupMask = mask;
	}

	float distance= 0.f;
	NiPoint3 dist(GetPosition());
	dist -= pkLocalPlayer->GetCamera().GetTranslate();
	distance = dist.Length();

	NewName->hp = GetUInt32Value(UNIT_FIELD_HEALTH);
	
	if (InstanceSys->IsPlayerInInstance() && ( InstanceSys->GetCurrentCategory() == INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND || InstanceSys->GetCurrentCategory() == INSTANCE_CATEGORY_TEAM_ARENA))
	{
		if (m_groupname.length() == 0)
		{
			ui32 id  = GetGroupIDByGUID(GetGUID());
			const GroupInfo* gs = ClientState->GetGroupByID(id);
			if (gs)
				m_groupname = gs->strGroupName;
		}
		Name = m_groupname +"-" + Name;
	}

	m_spNameRef->stName.bAFK = HasFlag(PLAYER_FLAGS, PLAYER_FLAG_AFK) ;
	m_spNameRef->stName.bDND = HasFlag(PLAYER_FLAGS, PLAYER_FLAG_DND);
	Name = m_PlayerState + Name;
	
	

	EffectMgr->AddCharName(m_spNameRef, kPos, Name.c_str(), NULL, m_guildname.c_str(), m_GuidTitle.c_str(), uiColor, distance, (pkLocalPlayer != this) & IsShowHPBar, true);
#ifdef _DEBUG
	DrawStateDesc(kPos);
#endif 
}

void CPlayer::UpdatePhysics(float fDelta)
{
	if ( m_bisForcePull )
	{
		UpdatePullMove(fDelta);
	}
	else
		PerformPhysics(fDelta);
}

BOOL CPlayer::DrawWeapon(bool in, float& t)
{
	ItemPrototype_Client* pkMain = GetMainHandWeaponPrototype();
	ItemPrototype_Client* pkOff = GetOffHandWeaponPrototype();
	if (!pkMain && pkOff && pkOff->SubClass == ITEM_SUBCLASS_WEAPON_BAOZHU)
	{
		return FALSE;
	}

	NiFixedString strAnim = GetBaseAnimationName(CAG_DRAW_WEAPON, in?0:1);

	AnimationID animID = GetAnimationIDByName(strAnim);
	if (animID == NiActorManager::INVALID_SEQUENCE_ID)
		return FALSE;

	NiControllerSequence* pkSeq =ActivateAnimation(animID, 2, -1, 0.1f);

	if (in)
		t = pkSeq->GetTimeAt("hide_arm", t);
	else
		t = pkSeq->GetTimeAt("draw_arm", t);

	return ( pkSeq != NULL);
}

void CPlayer::StopMountMove()
{
	if(IsMountPlayer())
	{
		NIASSERT(m_pkMountFoot);
		if (!m_pkMountFoot)
			return;
		m_pkMountFoot->StopMove();
	}
	else if(IsMountCreature())
	{
		NIASSERT(m_pkMountCreature);
		if (!m_pkMountCreature)
			return;
		m_pkMountCreature->StopMove();
	}
}


void CPlayer::OnHitState(  uint32 victimstat, bool bCrit, bool needSound )
{
	float fCurrentTime = SYState()->ClientApp->GetAccumTime();


	std::string strSoundFile;
	
	if ( GetUInt32Value(UNIT_CHANNEL_SPELL) )
	{
		return;
	}
	
	switch ( victimstat )//	vstate=1-wound,2-dodge,3-parry,4-interrupt,5-block,6-evade,7-immune,8-deflect
	{
	case 1:
		{
			if( bCrit )
			{
				NiFixedString strAnim = "prepare_bombattack";
				AnimationID animID = GetAnimationIDByName(strAnim);


				if ( m_pCurBattleAniSeq && !IsCurSecAnimEndInTime( fCurrentTime ) && ( m_pCurBattleAniSeq->GetName() == strAnim ) )
				{
					m_iCritTimesLeft++;
					if ( m_iCritTimesLeft > 2 )
					{
						m_iCritTimesLeft = 2;
					}

				}

				if ( m_iCritTimesLeft == 0 )
				{
					m_pCurBattleAniSeq = ActivateAnimation( animID, 2, 2, 0.1f, NiKFMTool::SYNC_SEQUENCE_ID_NONE, false, true, true );
					if ( m_pCurBattleAniSeq )
					{
						m_pCurBattleAniSeq->SetFrequency( 1.f );
					}
				}	
			}
		}
		break;
	case 2:
		{
			NiFixedString strAnim = "prepare_dodge";
			AnimationID animID = GetAnimationIDByName(strAnim);


			if (  m_pCurBattleAniSeq && !IsCurSecAnimEndInTime( fCurrentTime ) && ( m_pCurBattleAniSeq->GetName() == strAnim ) )
			{
				m_iDodgeTimesLeft++;
				if ( m_iDodgeTimesLeft > 2 )
				{
					m_iDodgeTimesLeft = 2;
				}
			}

			if ( m_iDodgeTimesLeft == 0 )
			{
				m_pCurBattleAniSeq = ActivateAnimation( animID, 2, 2, 0.1f, NiKFMTool::SYNC_SEQUENCE_ID_NONE, false, true, true );
				if ( m_pCurBattleAniSeq )
				{
					m_pCurBattleAniSeq->SetFrequency( 1.f );
				}
				strSoundFile = "Sound/dodge.wav";
			}	
		}
		break;

	case 5:
		{
			NiFixedString strAnim = "single_hand_prepare_parry";
			AnimationID animID = GetAnimationIDByName(strAnim);


			if ( m_pCurBattleAniSeq && !IsCurSecAnimEndInTime( fCurrentTime ) && ( m_pCurBattleAniSeq->GetName() == strAnim ) )
			{
				m_iDodgeTimesLeft++;
				if ( m_iDodgeTimesLeft > 2 )
				{
					m_iDodgeTimesLeft = 2;
				}

			}

			if ( m_iDodgeTimesLeft == 0 )
			{
				m_pCurBattleAniSeq = ActivateAnimation( animID, 2, 2, 0.1f, NiKFMTool::SYNC_SEQUENCE_ID_NONE, false, true, true );
				if ( m_pCurBattleAniSeq )
				{
					m_pCurBattleAniSeq->SetFrequency( 1.f );
				}
				strSoundFile = "Sound/block.wav";
			}	
		}
		break;
	}

	if ( strSoundFile.length() > 0 && needSound )
	{
		//AudioSource* pEffectSound  = AudioSystem::GetAudioSystem()->CreateSource(AudioSource::TYPE_3D);
		//if (!pEffectSound)
		//	return;

		//GetSceneNode()->AttachChild(pEffectSound);
		////GetSceneNode()->Update(0.0f, false);

		//pEffectSound->SetMinMaxDistance(1.0f, 60.0f);
		//pEffectSound->SetGain(1.f);
		///*pEffectSound->SetLoopCount( 1 );
		//pEffectSound->SetSelectiveUpdate(true);
		//pEffectSound->SetSelectiveUpdateTransforms(true);
		//pEffectSound->SetSelectiveUpdatePropertyControllers(false);
		//pEffectSound->SetSelectiveUpdateRigid(true);
		//pEffectSound->Update(0.0f);*/

		//SYState()->IAudio->PlaySound( pEffectSound, strSoundFile.c_str());

		if ( !m_spVoice[1] )
		{
			m_spVoice[1] = NiNew ALAudioSource(AudioSource::TYPE_3D);
		}
		GetSceneNode()->AttachChild(m_spVoice[1]);
		m_spVoice[1]->SetMinMaxDistance(5.0f, 60.0f);
		m_spVoice[1]->SetGain(1.f);

		const char* pOldname =  m_spVoice[1]->GetFilename();

		if ( pOldname && !strcmp( pOldname, strSoundFile.c_str()))
		{
			m_spVoice[1]->Play();
		}
		else
			SYState()->IAudio->PlaySound(m_spVoice[1], strSoundFile.c_str());

	}

	
}



void CPlayer::OnEquipmentChanged()
{
	CAnimObject::OnEquipmentChanged();
	/*NiAVObject* pkHelm = GetSceneNode()->GetObjectByName( "Helmet_Node" );
	if ( pkHelm )
	{
		pkHelm->SetAppCulled( !m_bIsShowHelm );
	}*/
}	


void CPlayer::Teleport(NiPoint3& transPos, float fOrientation)
{
	EmptyMovement();
	CCharacter::Teleport( transPos, fOrientation );
}

void CPlayer::StartForward( bool bAutoRun  )
{
	CCharacter::StartForward(bAutoRun);

	if ( (m_stMoveInfo.flags & MOVEFLAG_MOVE_BACKWARD) )
	{
		StopBackward();
	}

	if ( !HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM) && !IsJumping() )
	{
			m_bNeedDoAnim = true;
	}
}

void CPlayer::StopBackward()
{
	CCharacter::StopBackward();

	//SetMoveAnimation( GetMoveState() );
}


void CPlayer::StartBackward()
{
	if ( (m_stMoveInfo.flags & MOVEFLAG_MOVE_FORWARD) )
	{
		return;
	}

	CCharacter::StartBackward();
	if ( !HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM) )
	{
		m_bNeedDoAnim = true;
	}
}


void CPlayer::StartLeft()
{
	CCharacter::StartLeft();
	if ( !HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM) && !IsJumping() && !isMount() && !IsMountByPlayer() )
	{
		m_bNeedDoAnim = true;
	}
}


void CPlayer::StartRight()
{
	CCharacter::StartRight();
	if ( !HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM) && !IsJumping() && !isMount() && !IsMountByPlayer() )
	{
		m_bNeedDoAnim = true;
	}
}

void CPlayer::SetBackvVelocity()
{
	float angle = GetDesiredAngle();

	m_kInternelVel.x = -m_fbackStrikeSpeed*NiSin(angle);
	m_kInternelVel.y = -m_fbackStrikeSpeed*NiCos(angle);
}


void CPlayer::SetBackvVelocity(float orient )
{

	m_kInternelVel.x = -m_fbackStrikeSpeed*NiSin(orient);
	m_kInternelVel.y = -m_fbackStrikeSpeed*NiCos(orient);
}

void CPlayer::AddAutoCastSpell(uint32 spellID)
{
	m_AutoCastSpell.push_back(spellID);
}
void CPlayer::StopAutoCastSpell(uint32 spellID)
{
	std::vector<uint32>::iterator it = m_AutoCastSpell.begin();
	while(it != m_AutoCastSpell.end())
	{
		if (*it == spellID)
		{
			it = m_AutoCastSpell.erase(it);
			return;
		}
		++it;
	}
}
bool CPlayer::IsSpellAutoCast(uint32 spellID)
{
	for (unsigned int ui = 0 ; ui < m_AutoCastSpell.size() ; ++ui)
	{
		if (m_AutoCastSpell[ui] == spellID)
		{
			return true;
		}
	}
	return false;
}


NiAVObject* CPlayer::GetShapShiftNodeByName( const char* szName )
{
	NiAVObject* pNode = NULL;
	if ( m_spMountNode )
	{
		pNode = m_spMountNode->GetNIFRoot()->GetObjectByName(szName);
	}

	return pNode;
}
