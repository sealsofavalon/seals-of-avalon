#pragma once
#include "AnimObject.h"
#include "Camera.h"
#include "Character.h"

#include "ItemSlotContainer.h"
#include "EquipmentContainer.h"
#include "PlayerTradeContainer.h"
#include "ItemStorage.h"
#include "Effect/SceneEffect.h"


#define  MAXItemSlot 150
#define  MAXBankSlot 150
//===**玩家的身体部分信息**====//
struct PlayerPartInfo
{
	WORD HairID;
	WORD BodyID;
	WORD HandID;
	WORD HeadID;
	WORD ArmID;
	WORD FootID;
	WORD LegID;
};

struct PlayerProfessionLevel
{
	uint32 current;
	uint32 Max;
};

struct WeaponSlot
{
	char szName[128];
	int slot;
	int status;
};


class CPlayer : public CCharacter
{
public:
	CPlayer(void);
	virtual ~CPlayer(void);
 //========================================================================//	
	//返回游戏物体对象的类型
	virtual EGAMEOBJ_TYPE GetGameObjectType() const { return GOT_PLAYER; }

	virtual void HilightObject(BOOL Hilight);
	virtual void HilightEdgObject(BOOL bHilightEdg);

	//返回此对象类型
	virtual  CPlayer* GetAsPlayer() { return this;}
	//设置转向
	void SetDirectionTo(const NiPoint3& Target);
    // 得到状态代理结构信息
	virtual IStateProxy* GetStateProxy(State StateID);
	virtual IStateProxy* GetStateInstance(State eState);
	// 得到当前状态结构信息
	//virtual IStateProxy* GetCurStateProxy();
	// 初始镜头.
	bool InitCamera();
	//=== **得到摄像头**=============================================//
	// 对NiCamera 的操作必须由CCamera完成,通过这里只允许返回const 修饰.
    const NiCamera& GetNiCamera() const { return m_Camera.GetNiCamera(); } 
	 CCamera& GetCamera()  { return m_Camera; } 

	 bool CanSeeObject( CGameObject* pObject );
	// 更新
	virtual void Update(float dt);
	void PendingUpdate();
	// 注册动画关键点 信息
	virtual void RegistTextKey();
    // 创建玩家
	//BOOL CreatePlayer(const PlayerPartInfo* PartInfo);
	BOOL CreatePlayer(ui32 uiDisplayId);
    // 动画序列结束
	virtual void EndOfSequence(NiActorManager* pkManager,
		NiActorManager::SequenceID eSequenceID, float fCurrentTime, float fEventTime);

	bool GetBaseMoveSpeed(float& value) const;
	virtual BOOL SetMoveAnimation(UINT Flags, BOOL bForceStart = TRUE);
	virtual BOOL SetAttackAnimation(bool bPrepare = false);
	virtual const char* GetBaseAnimationName(ECHAR_ANIM_GROUP eAnimGroup, int nIndex = 0);
	virtual void OnEquipmentChanged();
	virtual void StartForward( bool bAutoRun = false );
	virtual void StartBackward();
	virtual void StopBackward();
	virtual void StartLeft();
	virtual void StartRight();


	// 移动
	AnimationID GetMovementAnimation(int iType);

	// 攻击
	virtual AnimationID	GetNormalAttackAnimation(UINT uSerial);

	const char* GetWeaponPrefix();
	ItemPrototype_Client* GetMainHandWeaponPrototype();
	ItemPrototype_Client* GetOffHandWeaponPrototype();

	BOOL isArmed() {
		return m_bArmed;
	}

	BOOL isWeaponAttached() {
		return m_bWeaponAttached;
	}

	// 拾取物体
	float GetSkillAttackRangeFactor() const { return 1.0f;}
	virtual BOOL Action_PickItem(ui64 itemguid);

	virtual BOOL MoveTo(const NiPoint3& Target, UINT Flag, bool findpath = true);
	virtual void MoveTo();
	virtual void StopMove();
	virtual void OnHitState( uint32 victimstat, bool bCrit, bool needSound );

	virtual bool GetPickBound(NiBound& kBound);
	virtual bool IsPick(NiPick& kPicker,const NiPoint3& kOrig,const NiPoint3& kDir);


	BOOL DrawWeapon(bool in, float& t);
	void StopMountMove();
	CSlotContainer* GetContainer(BYTE ContainerIndex);

	CStorage* GetItemContainer() { return m_pkPackageContainer; }
	CStorage* GetBankContainer() { return m_pkBankContainer; }
	void ReleaseLock();
    // 设置钱的数量
	void SetMoney(int nNum);
	AudioSource* GetSwimmingSound(){ return m_pSwimSound; }

	PlayerProfessionLevel* GetProfessionState( uint32 spellId )
	{
		if ( m_mapProfession.find(spellId) != m_mapProfession.end())
		{
			return &m_mapProfession[spellId];
		}

		return NULL;
	}

	bool GetProfessionEntryList(std::vector<uint32>& vSpellEntry)
	{
		std::map< uint32, PlayerProfessionLevel >::iterator it = m_mapProfession.begin();
		while( it != m_mapProfession.end() )
		{
			vSpellEntry.push_back(it->first);
			++it;
		}
		return vSpellEntry.size() > 0;
	}

	bool IsLearnProfession(uint32 spellId)
	{
		std::map< uint32, PlayerProfessionLevel >::iterator it = m_mapProfession.find( spellId );
		if( it != m_mapProfession.end() )
		{
			return true;
		}
		return false;
	}

	void UnLearnProfession( uint32 spellId )
	{
		std::map< uint32, PlayerProfessionLevel >::iterator it = m_mapProfession.find( spellId );
		if( it != m_mapProfession.end() )
		{
			m_mapProfession.erase( it );
		}
	}

 //   //请求NPC交易
	//BOOL TradeWithNpcReq(unsigned int npcid,std::string &strKind,std::string &strAdd);

	virtual void Rebirth();

	virtual void Teleport(NiPoint3& transPos, float fOrientation);
	virtual void SetBackvVelocity();
	virtual void SetBackvVelocity( float orient );


public:
	// DATE
	__forceinline uint8 GetRace() { return GetByte(UNIT_FIELD_BYTES_0,0); }
	__forceinline uint8 GetClass() { return GetByte(UNIT_FIELD_BYTES_0,1); }
	__forceinline uint8 GetGender() { return GetByte(UNIT_FIELD_BYTES_0,2); }
	__forceinline uint32 GetClassMask() { return 1 << ( GetClass()-1 ) ; }
	__forceinline uint32 GetRaceMask() { return 1 << ( GetRace()-1) ; }
	__forceinline void SetRace(uint8 race) { SetByte(UNIT_FIELD_BYTES_0,0, race); }
	__forceinline void SetGender(uint8 gender) { SetByte(UNIT_FIELD_BYTES_0,2, gender); }

	__forceinline uint8 GetAnimal()
	{
		return (uint8)((GetUInt32Value(PLAYER_BYTES) >> 24) & 0xFF);
	}
	__forceinline void SetAnimal(int newanimal)
	{
		SetUInt32Value(PLAYER_BYTES, ((GetUInt32Value(PLAYER_BYTES) & 0x00FFFFFF) | (uint8(newanimal) << 24)));
	}
	void AttachWeapon(bool imme = false, bool always = false);
	void DetachWeapon();
	bool isInShapShift(){
		if ( m_spMountNode )
		{
			return true;
		}
		else
			return false;
	}

	void SetNeedBallAnim( bool bneed ){ m_bBallAnim = bneed;}

	NiAVObject* GetShapShiftNodeByName( const char* szName );

	struct MoveInfo
	{
		ui8	moveOP;
		MovementInfo stMove;
	};

	void AddRemotePlayerMovementInfo(MoveInfo& moveInfo/*MovementInfo& moveInfo*/);
	void ProcessMovement();

	void EmptyMovement(){ 
		while ( m_MovementInfos.size() )
		{
			m_MovementInfos.pop();
		}
	}

	inline bool isShowHelm(){ return m_bIsShowHelm; }

	virtual void DrawName(const NiCamera* Camera);

	void SetFFAName(string name) {
		m_ffa = name;
	}
	void SetGuildName(string name){ if (name.size())m_guildname = '<'+name+'>';}
	void ClearGuildName(){m_guildname.clear();};
	BOOL GetCanTeacherOrP();  //是否可以收徒弟或者拜师

	inline std::string& GetGuildName(){return m_guildname;}
	inline void  SetShowGroup(bool show) {m_ShowGroup = show;}

	virtual void OnDeath(bool bDeadNow = true);

	
protected:
	void UpdatePhysics(float fDelta);
	void UpdatePendingProcess();


	// server status
	virtual void OnLevelupStatus(ui32 newLevel);

protected:
	virtual BOOL ActUpdate_PickItem(DWORD dwDeltaTime);
	virtual BOOL ActEnd_PickItem(ECharacterState NextAction);

protected:
	CCamera m_Camera;						// 之所以让镜头归于玩家所有,是考虑到有可能出现玩家之间的视野判断, 这部分将在客户端预计算? 
									        // 如果确定不会出现这种情况,那么镜头可以是唯一的. 写入到SceneManager 中也未尝不可.
	NiNodePtr m_spCharacterRoot;			// 玩家模型节点
	
	string m_guildname;
	string m_GuidTitle;	//当前称谓
	string m_ffa;
	string m_PlayerState;

	NiActorManagerPtr m_spMountNode;
	NiPoint3 m_fMountOffset;
	NiActorManagerPtr m_spAMSave;

	float m_LastUpdateTime;
	
	CStorage* m_pkPackageContainer;
	CStorage* m_pkBankContainer;
	CEquipmentContainer* m_pkEquipmentContainer;
	CPlayerTradeContainer* m_pkTradeContainer;
	bool      m_bBallAnim;
	
	std::map< uint32, PlayerProfessionLevel > m_mapProfession;
	uint32 m_uiCurLevel;
	bool   m_bIsShowHelm;
	bool   m_bShowShizhuang;

    NiTPrimitiveArray<CFormModel*> m_kFormModels; //延后的变身模型
protected:
private:
	static IStateProxy* sm_StateProxys[STATE_MAX];
	uint32 m_uiMoveStartTimeStamp;

	bool m_bFirstSample;
	bool m_bFirstStep;
	int m_iLastDelta;
	double m_dPrevSRTT;
	double m_dPrevSDEV;
	double m_dPrevAcc;
	double m_dCurAcc;
	int	   m_iDodgeTimesLeft;
	int	   m_iCritTimesLeft;
	int	   m_iparryTimesLeft;
	NiControllerSequence* m_pCurBattleAniSeq;

	uint32 m_ft;
	uint32 m_uiLocalMoveTime;
	uint32 m_uiSrcMoveTime;

	uint32 m_uiLastTime;
	uint32 m_uiLastRT;
	uint32 m_uiLastFalg;

private:
	string m_groupname;
	BOOL m_ShowGroup;
	std::queue<MoveInfo> m_MovementInfos;
	BOOL m_bArmed;
	int m_iArmEvent;
	float m_fArmEvtTime;
	BOOL m_bWeaponAttached;
	BOOL m_bInitMoney;
	int m_Movelevel;
	

	bool m_bRoot;
	AudioSourcePtr  m_pSwimSound;
public:
	virtual void OnValueChanged(class UpdateMask* mask);
	virtual void OnCreate(class ByteBuffer* data, ui8 update_flags);
	virtual bool OnCreateFromUI(ui32 uiDisplayId, ui8 race = 0, ui8 gender = 0);
    virtual void OnActorLoaded(NiActorManager* pkActorManager);
    virtual void OnFormModelChanged();
	void UpdateTeamGroupDate(ui64 guid, ui32 mask);

	static char* GetHeadImageFile(CPlayer* pPlayer=NULL, uint8 mRace=-1, uint8 mGender=-1);

	void MoveToPosition(const NiPoint3& kLocation, const NiPoint3& kTarget, const uint32& between, ECHAR_MOVE_STATE eMoveState);

protected:
	std::vector<uint32> m_AutoCastSpell;
public:
	void AddAutoCastSpell(uint32 spellID);
	void StopAutoCastSpell(uint32 spellID);
	bool IsSpellAutoCast(uint32 spellID);


public:
	struct stEqupinfo
	{
		uint32 equipitementry;
		uint32 jinglianlevel;
	};
	bool IsPlayerEquip(uint32 itementry)
	{
		for (int i = EQUIPMENT_SLOT_START ; i < EQUIPMENT_SLOT_END ; ++i)
		{
			if (m_Equiped[i].equipitementry && m_Equiped[i].equipitementry == itementry)
			{
				return true;
			}
		}
		return false;
	}
	uint32 GetJingLianLevel(uint32 itementry)
	{
		for (int i = EQUIPMENT_SLOT_START ; i < EQUIPMENT_SLOT_END ; ++i)
		{
			if (m_Equiped[i].equipitementry && m_Equiped[i].equipitementry == itementry)
			{
				return m_Equiped[i].jinglianlevel;
			}
		}
		return 0;
	}
protected:
	stEqupinfo m_Equiped[EQUIPMENT_SLOT_END];
};