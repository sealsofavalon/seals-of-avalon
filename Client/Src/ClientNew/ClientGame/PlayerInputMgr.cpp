#include "StdAfx.h"
#include "PlayerInputMgr.h"
#include "ObjectManager.h"
#include "Character.h"
#include "Player.h"
#include "Skill/SkillInfo.h"
#include "SceneManager.h"
#include "ClientApp.h"
#include "ClientState.h"
#include "Network/PacketBuilder.h"
#include "Skill/SkillManager.h"
#include "Effect/DecalManager.h"
#include "Effect/DecalEffect.h"
#include "Effect/EffectManager.h"
#include "ui/UISystem.h"
#include "ui/UIGamePlay.h"
#include "PathFinder.h"
#include "Creature.h"
#include "ui/UIShowPlayer.h"
#include "ui/UITipSystem.h"
#include "ui/UIRightMouseList.h"
#include "ui/UInGameBar.h"
#include "ui/UIPickItemList.h"
#include "ui/UChat.h"
#include "ui/UIPlayerEquipment.h"
#include "ui/UITipSystem.h"
#include "ui/UINPCTrade.h"
#include "AudioInterface.h"
#include "Console.h"
#include <list>
#include "Hook/HookManager.h"
#include "ui/SocialitySystem.h"
#include "../../../../SDBase/Public/CreatureDef.h"
#include "stdafx.h"
#include "./Effect/ChainEffect.h"
#include "ItemManager.h"
#include "ui/UFun.h"
#include "ui/UISkill.h"

using namespace std;

#include <audio/ALAudioSource.h>
#include "Screenshot.h"


CChainEffect* g_pTestChainEffect = NULL;

static list<AudioSource*> ml;

SYObjID MouseMoveTargetID = INVALID_OBJECTID;
SYObjID MouseBeginDragTargetID = INVALID_OBJECTID;

#define SPELL_LOG(x) EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN(x))
#define MOUSEUPDATATIME 0.5f
   
CPlayerInputMgr::CPlayerInputMgr(void)
{
	// -1为无效ID?
	m_TargetObjectID = INVALID_OBJECTID;
	m_SwingId = INVALID_OBJECTID;
	m_LastTargetID = INVALID_OBJECTID;

	m_LastUpdateTime = 0;
	m_MoveOBJUpdate = 0.0f;
	m_LastLBClick = FALSE;
	m_LBClickCnt = 0;
	m_CurLeftBtnDown = FALSE;
	m_LastMouseDownTime = 0;
	m_LastLeftBtnDown = FALSE;
	m_CurRightBtnDown = FALSE;	

	m_KeyboardMoveState = 0;

	m_pkSelectLocation = NULL;
	m_bSpellLocationSelect = false;
	m_bSpellItemSelect = false;

	m_fSpellMaxRadius = 0.0f;
	m_iLockMC = 0;
	m_iLockAC = 0;
	m_bUseMouseToMove = false;
	m_pMouseEffect = NULL;


	m_bLastFriendCheck = false;

	m_bAutoCancelProcess = false;
	m_bClearnQueueAction = TRUE;

	m_pkFlag = 0;
	m_force_root = false;
	m_bAutoAction = false;
	m_pkSelectLocationObj.clear();

	CHookMgr::InitHookMgr();
}

CPlayerInputMgr::~CPlayerInputMgr(void)
{
	list<AudioSource*>::iterator it = ml.begin();
	while (it != ml.end())
	{
		AudioSource* source = *it;
		NiDelete source;
		++it;
	}

	CHookMgr::ShutDownHookMgr();
}

// Note: NiInput 虽然使用了Buffer 方式, 但是还是不能很好的处理所有输入事件. 
// 一些输入会遗漏, 因此我觉得有必要用WIN32 MSG.
void CPlayerInputMgr::OnInputMessage(DWORD dwTime)
{
	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer == NULL)
	{
		return;
	}

	if( pkLocalPlayer->IsDead() )
		return;

	DWORD DeltaTime = dwTime - m_LastUpdateTime;
	m_LastUpdateTime = dwTime;
	
	m_LastLeftBtnDown = m_CurLeftBtnDown;
	m_LastRightBtnDown = m_CurRightBtnDown;

	// 判断规则.
	NiInputSystem* pkInput = SYState()->Input;
	NiInputKeyboard* Keyborad = pkInput->GetKeyboard();
	NiInputMouse* Mouse = pkInput->GetMouse();
	UINT CurModifier;
	m_CurLeftBtnDown = Mouse->ButtonIsDown(NiInputMouse::NIM_LEFT, CurModifier);
	m_CurRightBtnDown = Mouse->ButtonIsDown(NiInputMouse::NIM_RIGHT, CurModifier);
	//BOOL bCurLeftClick = /*m_CurLeftBtnDown ? FALSE :*/ Mouse->ButtonWasPressed(NiInputMouse::NIM_LEFT,CurModifier);
	//BOOL bCurRightClick = /*m_CurRightBtnDown ? FALSE :*/ Mouse->ButtonWasPressed(NiInputMouse::NIM_RIGHT,CurModifier);
	BOOL bCurLeftClick = FALSE;
	BOOL bCurRightClick = FALSE;
	if (!m_LastLeftBtnDown && m_CurLeftBtnDown)
	{
		bCurLeftClick = TRUE;
	}
	if (!m_LastRightBtnDown && m_CurRightBtnDown)
	{
		bCurRightClick = TRUE;
	}

	if (bCurLeftClick)
	{
		bCurLeftClick = bCurLeftClick;
	}

	if (m_LastLBClick && bCurLeftClick && dwTime < (DBL_CLICK_DELTA))
	{
		m_LBClickCnt++;
	}
	else
	{
		m_LastLBClick = bCurLeftClick;	
		if (bCurLeftClick)
		{
			m_LBClickCnt = 1;
		}
		else
		{
			m_LBClickCnt = 0;
		}
	}

	// 根据优先级来进行判断.

	BOOL Processed = FALSE;
	//#1 快捷键. 
	if(!Processed)
	{
		Processed = FALSE;
	}


	//#2 双击
	if(!Processed)
	{
		if (m_LBClickCnt >= 2)
		{
			Processed = LBDblClick();	
		}
	}

	//#3 单击
	if(!Processed && bCurLeftClick)
	{
		Processed = LBClick();
	}

	if(!Processed && bCurRightClick)
	{
		Processed = RBClick();
	}

	//#4 鼠标持续按下.
	if(!Processed)
	{
		if (bCurLeftClick)
		{
			/*	m_LastMouseDownTime = m_LastUpdateTime;
			m_LastLeftBtnUp = FALSE;*/
		}else if (!m_CurLeftBtnDown)
		{
			//m_LastLeftBtnUp = TRUE;
		}else if(m_CurLeftBtnDown)
		{
			if (m_LastUpdateTime - m_LastMouseDownTime > 3000)
			{
				Processed = LBContinueDown();
			}
		}
	}

	// 键盘
}

BOOL CPlayerInputMgr::Initialize()
{
	m_pkDecal = NiNew DecalEffect;
	if (!m_pkDecal)
		return FALSE;

	float fCurTime = SYState()->ClientApp->GetAccumTime();

	m_pkDecal->SetPostRender(true);
	m_pkDecal->Activate(SceneMgr->GetDecalMgr(), fCurTime);
	m_pkDecal->SetRadius(1.0f);
	m_pkDecal->SetDepth(2.0f);
	m_pkDecal->SetTexture("Texture\\Decal\\crescent.dds");
	m_pkDecal->SetVisible(false);
	m_pkDecal->SetFacingCam(true);
	m_pkDecal->SetZBias(-0.00022f);

	m_pkSelScenceObj = NiNew DecalEffect ;
	if (!m_pkSelScenceObj)
	{
		return FALSE ;
	}
	m_pkSelScenceObj->SetPostRender(true);
	m_pkSelScenceObj->Activate(SceneMgr->GetDecalMgr(), fCurTime);
	m_pkSelScenceObj->SetRadius(1.0f);
	m_pkSelScenceObj->SetDepth(2.0f);
	m_pkSelScenceObj->SetTexture("Texture\\Decal\\BLUE_FLOWER.dds");
	m_pkSelScenceObj->SetVisible(false);
	m_pkSelScenceObj->SetFacingCam(true);
	m_pkSelScenceObj->SetZBias(-0.00022f);



	m_pkSelectLocation = NiNew DecalEffect;
	if (!m_pkSelectLocation)
		return FALSE;

	m_pkSelectLocation->SetPostRender(true);
	m_pkSelectLocation->Activate(SceneMgr->GetDecalMgr(), fCurTime);

	m_pkSelectLocation->SetRadius(2.0f);
	m_pkSelectLocation->SetDepth(1.5f);
	m_pkSelectLocation->SetTexture("Texture\\Decal\\sign_runno_004.dds");
	m_pkSelectLocation->SetVisible(false);
	m_pkSelectLocation->SetColor(NiColorA(1, 1, 1, 1));
	m_pkSelectLocation->SetZBias(-0.00022f);

	//const NiFixedString strFlagEffectName = "Effect_jiantou.nif";
	//m_pkFlag = (CSceneEffect*)EffectMgr->CreateSceneEffect(strFlagEffectName,true, true);
//
//	Key moveKeys[] =	
//	{	
//		{LK_W, 0, 17},
//		{LK_S, 0, 31},
//		{LK_A, 0, 30},
//		{LK_D, 0, 32},
//		{LK_SPACE, 0, 57},
//		{LK_LEFT, 0, 0xCB},
//		{LK_RIGHT, 0, 0xCD}
////		{LK_G, 0}
//	};

	/*for (unsigned int ui = 0; ui < 7; ui++)
	{
		m_movekeys[ui] = moveKeys[ui];
	}*/

	m_movekeys[0].mkeycode = NiInputKeyboard::KEY_W;
	m_movekeys[1].mkeycode = NiInputKeyboard::KEY_S;
	m_movekeys[2].mkeycode = NiInputKeyboard::KEY_A;
	m_movekeys[3].mkeycode = NiInputKeyboard::KEY_D;
	m_movekeys[4].mkeycode = NiInputKeyboard::KEY_SPACE;
	m_movekeys[5].mkeycode = NiInputKeyboard::KEY_LEFT;
	m_movekeys[6].mkeycode = NiInputKeyboard::KEY_RIGHT;
	m_movekeys[7].mkeycode = NiInputKeyboard::KEY_Q;
	m_movekeys[8].mkeycode = NiInputKeyboard::KEY_E;


	return TRUE;
}

void CPlayerInputMgr::Shutdown()
{
	m_iLockMC = 0;
	m_iLockAC = 0;

}

void CPlayerInputMgr::Update(DWORD dwTime)
{
	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer == NULL)
	{
		return;
	}

	if (pkLocalPlayer->IsDead())
		return;


	DWORD DeltaTime = dwTime - m_LastUpdateTime;
	m_LastUpdateTime = dwTime;
	
	
	if (HookMgr)
	{
		HookMgr->UpdateState(dwTime);
	}

	
	UpdateTarget(dwTime);
	ProcessNextAction();
	PostUpdateTarget(dwTime);;
	
	UpdateDecal();
}

#ifdef WOW_CONTROL
bool bLastRBDownStep1 = false;
bool bLastRBDownStep2 = false;
#endif


bool CPlayerInputMgr::UpdateMoveKeys()
{
	CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
	if (!pkPlayer || m_bUseMouseToMove )
	{
		return false;
	}

	if (pkPlayer->IsHook())
	{
		return FALSE ;
	}
	bool bMountMove = true;
	if(pkPlayer->IsMountPlayer())
	{
		bMountMove = false;
	}

	bool bCharMove = pkPlayer->CanMove();
	if (!bMountMove || !bCharMove)
		return false;

	bool bOnlyCanFacing = pkPlayer->IsRoot();

	bool bCanMove = false;
	BOOL bJumping = pkPlayer->IsJumping();
	if (bCharMove && pkPlayer->CanMove() && !IsMoveLocked() && bMountMove)
	{
		bCanMove = true;
	}

	if(!bCanMove)
		return false;

	bool bCancelProcess = false;
	bool bProcessAD = false;
	bool bProcessRoteXY = false;
	bool bLockForward = (pkPlayer->m_bAutoRun||pkPlayer->IsPathMove())?true:false;
	bool bHasKeyDown = false;

	NiInputSystem* pkInput = SYState()->Input;
	NIASSERT(pkInput);
	NiInputMouse* pkMouse = pkInput->GetMouse();
	NIASSERT(pkMouse);
	bool bRD = pkMouse->ButtonIsDown(NiInputMouse::NIM_RIGHT);
	bool bLD = pkMouse->ButtonIsDown(NiInputMouse::NIM_LEFT);

	for (unsigned int ui = 0; ui < 9; ui++)
	{
		bool bKeyDown = false;
		if (m_movekeys[ui].GetState() == UNIInputKey::DOWN || m_movekeys[ui].GetState() == UNIInputKey::PRESS)
			bKeyDown = true;

		if (bKeyDown && ui >= 0 && ui < 5)
			bHasKeyDown = true;


		switch(m_movekeys[ui].GetCode())
		{
		case NiInputKeyboard::KEY_W:
			{
				if (bKeyDown)
				{
					if (bCanMove && !bOnlyCanFacing )
						pkPlayer->StartForward();
					bCancelProcess = true;
				}
				else if (!bLockForward)
				{
					if ( !( bRD && bLD ) )
					{
						pkPlayer->StopForward();
					}
					
				}
			}
			break;

			case NiInputKeyboard::KEY_S:
				{
					if (bKeyDown)
					{
						if (bCanMove && !bOnlyCanFacing )
							pkPlayer->StartBackward();
						bCancelProcess = true;
					}
					else if( !pkPlayer->m_bAutoRun )
					{
						pkPlayer->StopBackward();
					}
				}
				break;

			case NiInputKeyboard::KEY_D:
				{
					if (bKeyDown)
					{
						if (bCanMove && !bOnlyCanFacing )
						{
							pkPlayer->StartRight();
//#ifdef WOW_CONTROL
							bProcessAD = true;
//#endif
						}
						bCancelProcess = true;
					}
					else
					{
						pkPlayer->StopRight();
					}
				}
				break;

			case NiInputKeyboard::KEY_A:
				{
					if (bKeyDown)
					{
						if (bCanMove && !bOnlyCanFacing )
						{
							pkPlayer->StartLeft();
//#ifdef WOW_CONTROL
							bProcessAD = true;
//#endif
						}
						bCancelProcess = true;
					}
					else
					{
						pkPlayer->StopLeft();
					}
				}
				break;

			case NiInputKeyboard::KEY_SPACE:
				{
					if (bCanMove && bKeyDown && !pkPlayer->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM) && !bOnlyCanFacing )
  						pkPlayer->OnJumpInputEvent();
					
 					if ( !bKeyDown  )
					{
						pkPlayer->StopFlyUp();
						pkPlayer->StopSwimUp();
						
					}
					
				}
				break;

			case NiInputKeyboard::KEY_Q:
				{
					if (bKeyDown)
					{
//#ifdef WOW_CONTROL //RotateSpeeds
						//pkPlayer->GetCamera().Dither(1.0f, 1.0f);
						if ( bRD ) //鼠标右键按下后
						{
							bHasKeyDown = true;
							if (bCanMove)
							{
								pkPlayer->StartLeft();
								bCancelProcess = true;
							}
						}
						else //鼠标右键松开了
						{
							pkPlayer->GetCamera().SetRotateXY(-25.0f, 0.0f);
							bProcessRoteXY = true;
							if ( !bProcessAD )
							{
								pkPlayer->StopLeft();
							}
							
						}
					}
					else 
					{
						if ( !bProcessAD )
							pkPlayer->StopLeft();

						pkPlayer->GetCamera().SetRotateXY(0.0f, 5.0f);
					}
//#else
//					pkPlayer->GetCamera().SetRotateXY(-10.0f, 0.0f);
//					bProcessRoteXY = true;
//#endif
					//}
					//else
					//{
					//	pkPlayer->GetCamera().SetRotateXY(0.0f, 1.0f);
					//}
				}
				break;
			case NiInputKeyboard::KEY_E:
				{
					if (bKeyDown)
					{
//#ifdef WOW_CONTROL RotateSpeeds Kalisto
						if ( bRD )
						{
							bHasKeyDown = true;
							if ( bCanMove )
							{
								pkPlayer->StartRight();
								bCancelProcess = true;
							}
						}
						else
						{
							pkPlayer->GetCamera().SetRotateXY(25.0f, 0.0f);
							if ( !bProcessAD )
							{
								pkPlayer->StopRight();
							}
						}
					    
					}
					else
					{
						if ( !bProcessAD )
							pkPlayer->StopRight();

						if ( !bProcessRoteXY )
						{
							pkPlayer->GetCamera().SetRotateXY(0.0f, -5.0f);
						}

					}
//#else
//						pkPlayer->GetCamera().SetRotateXY(10.0f, 0.0f);
//#endif
//					}
//					else
//					{
//						if ( !bProcessRoteXY )
//						{
//							pkPlayer->GetCamera().SetRotateXY(0.0f, -1.0f);
//						}
		//			}
				}
				break ;
			default:
				break;
		}
	}

	/*if ((m_movekeys[5].GetState() == UNIInputKey::DOWN || m_movekeys[5].GetState() == UNIInputKey::PRESS)&&
		(m_movekeys[6].GetState() == UNIInputKey::DOWN || m_movekeys[6].GetState() == UNIInputKey::PRESS))*/
	if (bRD && bLD)
	{
		bHasKeyDown = true;
		if (bCanMove && !bOnlyCanFacing )
		{
			pkPlayer->GetCamera().SetForceRotate( true );
			pkPlayer->StartForward();
		}
	}

	if (bCancelProcess)
	{
		m_bAutoCancelProcess = true;
		AutoCancelProcess();
	}
	if( bHasKeyDown && bCanMove && !bOnlyCanFacing) {
		pkPlayer->MoveTo();
		return TRUE;
	}
	else
	if (!bJumping && !pkPlayer->IsPathMove() && !pkPlayer->m_bAutoRun)
	{
		pkPlayer->StopMove();
	}

	return bHasKeyDown;
}

BOOL CPlayerInputMgr::LBDblClick()
{
	NiInputSystem* pkInput = SYState()->Input;
	NIASSERT(pkInput);

	NiInputMouse* pkMouse = pkInput->GetMouse();
	NIASSERT(pkMouse);

	bool bRD = pkMouse->ButtonIsDown(NiInputMouse::NIM_RIGHT);
	if (bRD) return FALSE;

 	BOOL bRet = FALSE;
 	NiPoint3 ptPickPos;
 	POINT ptMouse = GetCurMousePoint();
 	CGameObject* pkTargetObject = (CGameObject*)ObjectMgr->GetObject( MouseMoveTargetID );//PickObject(ptMouse, ptPickPos, false, true);
 
 	if (pkTargetObject)
 	{
 		if (pkTargetObject->GetGUID() != GetTargetObjectID())
		{
 			AutoCancelProcess();
			return bRet;
		}
 
 		if ( pkTargetObject->GetGameObjectType() == GOT_CREATURE )
 		{
 			bRet = OnRequireCreature((CCreature*)pkTargetObject);
 		}
 
 	//	if ( pkTargetObject->GetGameObjectType() == GOT_PLAYER )
 	//	{
 	//		bRet = OnRequirePlayer((CPlayer*)pkTargetObject);
 	//	}
		//if (pkTargetObject->GetGameObjectType() == GOT_SCENEOBJECT)
		//{
		//	return FALSE ;
		//	//bRet = OnRequireSceneObject((CSceneGameObj*)pkTargetObject);
		//}
 	}
 	else
 	{
 		//bRet = MouseMoveTargetProcess();
 	}

	return bRet;
}

BOOL CPlayerInputMgr::RBDblClick()
{
	return RBClick();
}

bool CPlayerInputMgr::IsSelectingTarget() const
{
	return m_bSpellLocationSelect;
}


bool CPlayerInputMgr::IsSelectingItem() const
{
	return m_bSpellItemSelect;
}



BOOL CPlayerInputMgr::HilightSelectTarget(NiPoint3 dest, float radius)
{
	if (radius == 0.0f)
	{
		EndHilight();
		return TRUE ;
	}
	for (int i =0; i < m_pkSelectLocationObj.size(); i++)
	{
		CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(m_pkSelectLocationObj[i]);
		if (pkChar)
		{
			if ((pkChar->GetPosition() - dest).Length() > radius && pkChar->GetGUID() != m_TargetObjectID)
			{
				pkChar->HilightObject(FALSE);
			}
		}
	}

	m_pkSelectLocationObj.clear();

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	const stdext::hash_set<UpdateObj*>& temp( ObjectMgr->GetObjectByType(TYPEID_UNIT));
	const stdext::hash_set<UpdateObj*>& temp2( ObjectMgr->GetObjectByType(TYPEID_PLAYER));

	for( stdext::hash_set<UpdateObj*>::const_iterator it = temp.begin(); it != temp.end(); ++it )
	{
		UpdateObj* p = *it;
		CCreature* pkCreature = static_cast<CCreature*>(p);
		if (pkCreature && pkCreature->GetGameObjectType() == GOT_CREATURE && 
			!pkCreature->IsFriendly(pkLocal) && (pkCreature->GetPosition() - dest).Length() <= radius)
		{
			pkCreature->HilightObject(TRUE);
			m_pkSelectLocationObj.push_back(pkCreature->GetGUID());
		}
	}

	for( stdext::hash_set<UpdateObj*>::const_iterator it = temp2.begin(); it != temp2.end(); ++it )
	{
		UpdateObj* p = *it;
		CPlayer* pkPlayer = static_cast<CPlayer*>(p);
		if (pkPlayer && !pkPlayer->IsLocalPlayer() && pkPlayer->GetGameObjectType() == GOT_PLAYER && 
			!pkPlayer->IsFriendly(pkLocal) && (pkPlayer->GetPosition() - dest).Length() <= radius)
		{
			pkPlayer->HilightObject(TRUE);
			m_pkSelectLocationObj.push_back(pkPlayer->GetGUID());
		}
	}


	return TRUE ;
	
}
BOOL CPlayerInputMgr::EndHilight()
{
	for (int i =0; i < m_pkSelectLocationObj.size(); i++)
	{
		CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(m_pkSelectLocationObj[i]);
		if (pkChar)
		{
			if (pkChar->GetGUID() != m_TargetObjectID)
			{
				pkChar->HilightObject(FALSE);
			}
		}
	}

	m_pkSelectLocationObj.clear() ;

	return TRUE ;
}
void CPlayerInputMgr::EndSelectTarget()
{
	if (IsSelectingTarget() && m_pkSelectLocation)
	{
		//Hiden
		EndHilight();
		m_pkSelectLocation->SetVisible(false);
		m_bSpellLocationSelect = false;
	}

	if ( IsSelectingItem() )
	{
		EndSelectItem();
	}
}



void CPlayerInputMgr::EndSelectItem()
{
	m_bSpellItemSelect = false;
	SYState()->ClientApp->SetCursor(SYC_ARROW);
}


void CPlayerInputMgr::ProcessSpellLocationSelect(const NiPoint3& kRayStart, const NiPoint3& kRayDir)
{
	NiPoint3 kDestPos;
	BOOL bRet = SceneMgr->FindMoveTarget(kRayStart, kRayDir, &kDestPos);

	CPlayerLocal* pkLP = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLP);
	if (!pkLP)
		return;

	static const NiColorA kOutSide = NiColorA(1, 0, 0, 1);
	static const NiColorA kInSide = NiColorA(0, 1, 0, 1);

	if (bRet && m_pkSelectLocation && m_pkSelectLocation->IsVisible())
	{
		NiPoint3 kDist = kDestPos - pkLP->GetPosition();
		if (!pkLP->IsFlying())
		{
			kDist.z = 0.f ;
		}
		if (kDist.Length() > m_fSpellMaxRadius)
		{
			m_pkSelectLocation->SetColor(kOutSide);
			EndHilight();
		}
		else
		{
			HilightSelectTarget(kDestPos, m_pkSelectLocation->GetRadius());
			m_pkSelectLocation->SetColor(kInSide);
		}
		m_pkSelectLocation->SetPosition(kDestPos);
	}
	else if( m_pkSelectLocation )
		m_pkSelectLocation->SetPosition(kDestPos);
}

void CPlayerInputMgr::MouseMove()
{
	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer == NULL) {
		return;
	}

	const NiCamera& CurCam = pkLocalPlayer->GetNiCamera();
	POINT ptMouse = GetCurMousePoint();
	NiPoint3 RayStart, RayDir;
	if (!GetRayFormMousePoint(ptMouse, &CurCam, RayStart, RayDir))
	{
		return;
	}

	//if (IsSelectingTarget())
	{
		ProcessSpellLocationSelect(RayStart, RayDir);
		return;
	}
}

void CPlayerInputMgr::OnRightMouseDown(const POINT& position, UINT nRepCnt, UINT flags)
{
	NiPoint3 ptPickPos;

	POINT ptMouse = GetCurMousePoint();
	CGameObject* TargetObject = PickObject(ptMouse, ptPickPos, false);
	if ( TargetObject )
		MouseBeginDragTargetID = TargetObject->GetGUID();
	else
		MouseBeginDragTargetID = INVALID_OBJECTID;
}

void CPlayerInputMgr::OnMouseDown(const POINT& point, UINT nRepCnt, UINT uFlags)
{
	
}

void CPlayerInputMgr::OnMouseUp(const POINT& position, UINT flags)
{
}

DWORD dwNowTime = 0;
DWORD dwEndTime = 0;

void CPlayerInputMgr::OnMouseMove(const POINT& position, UINT flags)
{
	DWORD dwNowTime = SYState()->CurTime;

	DWORD dwDelayTime = dwNowTime - dwEndTime;

	NiInputSystem* pkInput = SYState()->Input;
	NIASSERT(pkInput);
	NiInputMouse* pkMouse = pkInput->GetMouse();
	NIASSERT(pkMouse);
	bool bRD = pkMouse->ButtonIsDown(NiInputMouse::NIM_RIGHT);
	if ( bRD )
	{
		NiPoint3 ptPickPos;

		POINT ptMouse = GetCurMousePoint();
		CGameObject* TargetObject = PickObject(ptMouse, ptPickPos, false);

		if ( TargetObject )
			MouseMoveTargetID = TargetObject->GetGUID();
		else
			MouseMoveTargetID = INVALID_OBJECTID;
		return;
	}


	//if ( dwDelayTime >= 100 )
	{
		//dwEndTime = dwNowTime;

		NiPoint3 ptPickPos;

		POINT ptMouse = GetCurMousePoint();

		UpdateObj* Last = ObjectMgr->GetObject(m_LastTargetID);
		CGameObject* LastTarget = NULL;
		if (Last)
		{
			if (!Last->isType(TYPE_ITEM) || Last->IsMapItem())
				LastTarget = (CGameObject*)Last ;//dynamic_cast<CGameObject*>(Last)
			else
			{
				m_LastTargetID = INVALID_OBJECTID;
				return ;
			}
		}
		CGameObject* TargetObject = PickObject(ptMouse, ptPickPos, false);
		CGameObject* DecalObject = (CGameObject*)ObjectMgr->GetObject(m_TargetObjectID);

		if ( TargetObject )
			MouseMoveTargetID = TargetObject->GetGUID();
		else
			MouseMoveTargetID = INVALID_OBJECTID;

		if (m_LastTargetID)
			TipSystem->HideTip();
		

		if ( TargetObject && TargetObject != DecalObject)
			TargetObject->HilightEdgObject(TRUE);

		if ( LastTarget && LastTarget != TargetObject && LastTarget != DecalObject )
			LastTarget->HilightEdgObject(FALSE);

		if(LastTarget && LastTarget != TargetObject)
		{
			if (LastTarget->GetGameObjectType() == GOT_CREATURE)
			{		}
			if (LastTarget->GetGameObjectType() == GOT_ITEM)
			{ }
			if (LastTarget->GetGameObjectType() == GOT_SCENEOBJECT)
			{
				CSceneGameObj* pkSceneOBJ = (CSceneGameObj*)LastTarget;
				if ( (pkSceneOBJ->IsGatherObj() && pkSceneOBJ->CanSeeCollection()) || pkSceneOBJ->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_FLAGSTAND || pkSceneOBJ->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_CHEST || pkSceneOBJ->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_MAILBOX )
				{
					TipSystem->HideTip();
					LastTarget->HilightObject(FALSE);
				}
			}

			m_LastTargetID = INVALID_OBJECTID;
		}
		
		CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
		if (TargetObject)
		{
			UPoint TipPos;
			if(UInGame::GetFrame(FRAME_IG_ACTIONSKILLBAREX1))
			{
				TipPos.x = UInGame::GetFrame(FRAME_IG_ACTIONSKILLBAREX1)->GetControlRect().right - 140;
				TipPos.y = UInGame::GetFrame(FRAME_IG_ACTIONSKILLBAREX1)->GetControlRect().top - 75;
			}
			if (TargetObject->GetGameObjectType() == GOT_CREATURE)
			{
				CCreature* pkObject = (CCreature*)TargetObject;
				if (pkObject->IsDead())
				{
					if (pkObject->HasFlag(UNIT_DYNAMIC_FLAGS, U_DYN_FLAG_LOOTABLE))
					{
						//显示可以拾取的光标
						SYState()->ClientApp->SetCursor(SYC_PICK);
					}else
					{
						//显示不能拾取光标
						SYState()->ClientApp->SetCursor(SYC_NOTPICK);
					}
				}else
				{	

					if (pLocal)
					{
						if (pLocal->IsHostile(pkObject))
						{
							//显示可以攻击的光标
							SYState()->ClientApp->SetCursor(SYC_ATTACK);
						}else 
						{
							if (pkObject->GetUInt32Value(UNIT_NPC_FLAGS))
							{
								if (pLocal->IsFriendly(pkObject))
								{
									SYState()->ClientApp->SetCursor(SYC_SELECTTARGET);
								}else
								{
									SYState()->ClientApp->SetCursor(SYC_ATTACK);
								}
							}else if (!pLocal->IsFriendly(pkObject))
							{
								SYState()->ClientApp->SetCursor(SYC_ATTACK);
							}
							else
							{
								SYState()->ClientApp->SetCursor(SYC_ARROW);
							}

						}
						//显示选择与NPC对话的光标 // 友好NPC
					}	

				}
				TipSystem->ShowTip(TIP_TYPE_MONSTER, TargetObject->GetUInt32Value(OBJECT_FIELD_ENTRY), TipPos, TargetObject->GetGUID());

			}

			if(TargetObject->GetGameObjectType() == GOT_SCENEOBJECT)
			{
				CSceneGameObj* pkSceneOBJ = (CSceneGameObj*)TargetObject;
				if( pkSceneOBJ->GetUInt32Value( GAMEOBJECT_DYN_FLAGS ) > 0 && pkSceneOBJ->GetUInt32Value( GAMEOBJECT_TYPE_ID ) == GAMEOBJECT_TYPE_PICK )
				{
					uint32 shapespell = 0;
					pLocal->GetShapeSpell( shapespell );
					if( shapespell != 326 && shapespell != 327 )
					{
						TipSystem->ShowTip(TIP_TYPE_SCENEGAMEOBJ, pkSceneOBJ->GetUInt32Value(OBJECT_FIELD_ENTRY), UPoint(ptMouse.x + 32, ptMouse.y + 32), pkSceneOBJ->GetGUID());
						SYState()->ClientApp->SetCursor(SYC_SHOVEL);
					}
					else
						SYState()->ClientApp->SetCursor(SYC_ARROW);
				}
				else if ( (pkSceneOBJ->IsGatherObj() && pkSceneOBJ->CanSeeCollection() ) )
				{
					TipSystem->ShowTip(TIP_TYPE_SCENEGAMEOBJ, pkSceneOBJ->GetUInt32Value(OBJECT_FIELD_ENTRY), UPoint(ptMouse.x + 32, ptMouse.y + 32), pkSceneOBJ->GetGUID());
					if ( pkSceneOBJ->GetReqSpell() == 430000010  )
					{
						SYState()->ClientApp->SetCursor(SYC_MINE);
					}
					else if ( pkSceneOBJ->GetReqSpell() == 430001010 )
					{
						SYState()->ClientApp->SetCursor(SYC_HERB);
					}
					else
						SYState()->ClientApp->SetCursor(SYC_PICK);
				}
				else if( pkSceneOBJ->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_FLAGSTAND )
				{
					uint32 flag = pkSceneOBJ->GetUInt32Value( GAMEOBJECT_FLAGS );
					uint32 race = pLocal->GetRace();
					if( (flag < 10 && race != flag)
						|| (flag > 10 && race != flag - flag/10*10) )
					{
						TipSystem->ShowTip(TIP_TYPE_SCENEGAMEOBJ, pkSceneOBJ->GetUInt32Value(OBJECT_FIELD_ENTRY), UPoint(ptMouse.x + 32, ptMouse.y + 32), pkSceneOBJ->GetGUID());
						SYState()->ClientApp->SetCursor(SYC_BANNEROPEN);
					}
				}
				else if( pkSceneOBJ->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_CHEST )
				{
					TipSystem->ShowTip(TIP_TYPE_SCENEGAMEOBJ, pkSceneOBJ->GetUInt32Value(OBJECT_FIELD_ENTRY), UPoint(ptMouse.x + 32, ptMouse.y + 32), pkSceneOBJ->GetGUID());
					SYState()->ClientApp->SetCursor(SYC_PICK);
				}
				else if( pkSceneOBJ->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_MAILBOX )
				{
					TipSystem->ShowTip(TIP_TYPE_SCENEGAMEOBJ, pkSceneOBJ->GetUInt32Value(OBJECT_FIELD_ENTRY), UPoint(ptMouse.x + 32, ptMouse.y + 32), pkSceneOBJ->GetGUID());
					SYState()->ClientApp->SetCursor(SYC_MAIL);
				}
				else
				{
					SYState()->ClientApp->SetCursor(SYC_ARROW);
				}
			}
			
			if (TargetObject->GetGameObjectType() == GOT_ITEM && TargetObject->IsMapItem())
			{
				CSceneItem* pkItem = static_cast<CSceneItem*> (TargetObject);
				if (pkItem)
					SYState()->ClientApp->SetCursor(SYC_PICK);
			}
			if (TargetObject->GetGameObjectType() == GOT_PLAYER)
			{
				if (pLocal)
				{
					if (!pLocal->IsFriendly((CPlayer*)TargetObject))
						SYState()->ClientApp->SetCursor(SYC_ATTACK);
					else
						SYState()->ClientApp->SetCursor(SYC_ARROW);
				}
				TipSystem->ShowTip(TIP_TYPE_MONSTER, TargetObject->GetUInt32Value(OBJECT_FIELD_ENTRY), TipPos, TargetObject->GetGUID());
			}

			//if (TargetObject->GetGameObjectType() != GOT_ITEM)
			{
				m_LastTargetID = TargetObject->GetGUID();
			}
			
		}
		else
			SYState()->ClientApp->SetCursor(SYC_ARROW);
		
	}

}
void CPlayerInputMgr::OnMouseOBJ(float ftime)
{
	m_MoveOBJUpdate += ftime ;
	if (m_MoveOBJUpdate < MOUSEUPDATATIME)
	{
		return ;
	}
	POINT ptMouse = GetCurMousePoint();
	if (UInGame::Get())
	{
		UNPCTrade* pkNpcTrade = INGAMEGETFRAME(UNPCTrade, FRAME_IG_NPCTRADEDLG);
		if (pkNpcTrade && pkNpcTrade->IsRepair())
		{
			return ;
		}
		BOOL bIsInGame= UInGame::Get()->HitTestIsInGame(UPoint(ptMouse.x, ptMouse.y));
		if (bIsInGame)
		{
			m_MoveOBJUpdate = 0.0f;
			OnMouseMove(ptMouse,0);
			MouseMove();
		}else if (UInGame::Get()->HitTestPackage(UPoint(ptMouse.x, ptMouse.y)))
		{
			if (SYState()->UI->GetUItemSystem()->IsUsedBangdItem() )
			{
				SYState()->ClientApp->SetCursor(SYC_UNBANGDING);
			}else if (SYState()->UI->GetUItemSystem()->IsUsedEnchantItem())
			{
				SYState()->ClientApp->SetCursor(SYC_ENCHANT);
			}
			else
			{
				SYState()->ClientApp->SetCursor(SYC_ARROW);
			}
		}
	}
}

POINT CPlayerInputMgr::GetCurMousePoint()
{
	HWND hWnd = SYState()->ClientApp->
		GetAppWindow()->GetWindowReference();

	POINT cursorPoint;
	::GetCursorPos(&cursorPoint);
	::ScreenToClient(hWnd, &cursorPoint);
	
	return cursorPoint;
}

BOOL CPlayerInputMgr::GetRayFormMousePoint(const POINT& pt, const NiCamera* pkCamera, NiPoint3& ptOrig, NiPoint3& vDir)
{
	if (!pkCamera)
		return FALSE;

	if (!pkCamera->WindowPointToRay(pt.x, pt.y, ptOrig, vDir))
		return FALSE;

	return TRUE;
}

CGameObject* CPlayerInputMgr::PickObject(const POINT& kScreenP, NiPoint3& ptPickPos, bool bFirst /*= false*/, bool bLoot/* = false*/, int weight /*= 1*/)
{
	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (!pkLocalPlayer) 
		return FALSE;

	const NiCamera& CurCam = pkLocalPlayer->GetNiCamera();
//	POINT ptMouse = GetCurMousePoint();

	NiPoint3 ptOrig, vDir;
	if (!GetRayFormMousePoint(kScreenP, &CurCam, ptOrig, vDir)) 
		return FALSE;

	return ObjectMgr->PickObject(ptOrig, vDir, ptPickPos, bFirst, bLoot, weight);
//	return PickObject(ptOrig, vDir, ptPickPos);
}

CGameObject* CPlayerInputMgr::PickObject(const NiPoint3& ptOrig, const NiPoint3& vDir, NiPoint3& ptPickPos)
{
	return ObjectMgr->PickObject(ptOrig, vDir, ptPickPos);
}

BOOL CPlayerInputMgr::PickGround(const NiPoint3& ptOrig, const NiPoint3& vDir, NiPoint3& ptTarget)
{
	if (!SceneMgr->FindMoveTarget(ptOrig, vDir, &ptTarget)) return FALSE;

	return TRUE;
}

CGameObject* CPlayerInputMgr::GetLastTargetObject()
{
	CGameObject* pObject = (CGameObject*)ObjectMgr->GetObject(m_LastTargetID);

	return pObject;
}

void CPlayerInputMgr::SetSelectItemInfo(float SpellRadius, ui8 bag, ui8 slot)
{
	m_fSpellMaxRadius = SpellRadius;
	m_ItemSelectInfo.bSelectByItem = true;
	m_ItemSelectInfo.bag = bag;
	m_ItemSelectInfo.slot = slot;
}

BOOL CPlayerInputMgr::LBClick()
{
	// 选取场景.
	/*if ( ItemMgr->GetUsingItem())
	{
		ItemMgr->SetItemUsed();
		SYState()->ClientApp->SetCursor(SYC_ARROW);
		return FALSE;
	}*/

	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer == NULL)
	{
		return FALSE;
	}

	if (pkLocalPlayer->IsHook())
	{
		return FALSE;
	}

	// 如果骑乘状态，并且是Head的话
	if(pkLocalPlayer->IsMountByPlayer())
		return FALSE;

	if ( IsSelectingTarget() )
	{
		const NiCamera& CurCam = pkLocalPlayer->GetNiCamera();

		POINT ptMouse = GetCurMousePoint();
		NiPoint3 ptOrig, vDir;
		if (!GetRayFormMousePoint(ptMouse, &CurCam, ptOrig, vDir)) return FALSE;

		NiPoint3 ptTarget;
		if (!PickGround(ptOrig, vDir, ptTarget)) 
		{
			return FALSE;
		}

		SpellCastTargets spllTarget;
		spllTarget.m_targetMask = TARGET_FLAG_DEST_LOCATION;
		spllTarget.m_destX = ptTarget.x;
		spllTarget.m_destY = ptTarget.y;
		spllTarget.m_destZ = ptTarget.z;

		if (m_ItemSelectInfo.bSelectByItem)
		{
			ItemMgr->ItemUseReq(m_ItemSelectInfo.bag, m_ItemSelectInfo.slot, 0, spllTarget);

			EndSelectTarget();
			return true;
		}
		else
		{
			if(!InputTestSpellUseItem(m_LocationSelSpellID))
			{
				EndSelectTarget();
				return false;
			}

			SpellTemplate* pkST = SYState()->SkillMgr->GetSpellTemplate(m_LocationSelSpellID);
			if (!pkST)
				return FALSE;

			if ( pkST->GetCastIndex() != 1 )
			{
				if (pkLocalPlayer->GetCurPostureState() == STATE_MOVE ||
					pkLocalPlayer->GetCurPostureState() == STATE_FALLING)
				{
					EndSelectTarget();
					SPELL_LOG("技能失败：不能在移动中执行该动作。");
					return FALSE;
				}
			}

			return OnLocationSkillAction(m_LocationSelSpellID, spllTarget);
		}
	}

	NiInputSystem* pkInput = SYState()->Input;
	NIASSERT(pkInput);

	NiInputMouse* pkMouse = pkInput->GetMouse();
	NIASSERT(pkMouse);

	bool bRD = pkMouse->ButtonIsDown(NiInputMouse::NIM_RIGHT);
	//if (bRD) return FALSE;

	const NiCamera& CurCam = pkLocalPlayer->GetNiCamera();

	NiPoint3 RayStart, RayDir;
	POINT ptMouse = GetCurMousePoint();
	NiPoint3 ptOrig, vDir;
	if (!GetRayFormMousePoint(ptMouse, &CurCam, RayStart, RayDir)) return FALSE;

	CGameObject* pkTargetObject = NULL;
	NiPoint3 ptPickPos;
	BOOL bPickedObject = FALSE;
	pkTargetObject = (CGameObject*)ObjectMgr->GetObject( MouseMoveTargetID );//ObjectMgr->PickObject(RayStart, RayDir, ptPickPos);

	BOOL bRet = FALSE;
	if(pkTargetObject)
	{
		// Todo 以后这里记得改掉
		// 现在即使有 TargetObject 我们还是需要在pick一次Terrain
//		NiPoint3 ptTarget;
//		if(PickGround(RayStart, RayDir, ptTarget))
//		{
//			if((ptTarget - RayStart).Length() >= (pkTargetObject->GetPosition() - RayStart).Length())
//			{
//				AutoCancelProcess();
//				bRet = Action_ClickTarget(pkTargetObject);
//			}
////			else
////			{
////				bRet = MouseMoveTargetProcess();
////			}
//		}
//		else
//		{
			AutoCancelProcess();
			bRet = Action_ClickTarget(pkTargetObject);
//		}
	}
	else if(m_bUseMouseToMove && !pkLocalPlayer->IsDead() && !IsForceRoot() )
	{
		NiPoint3 ptTarget;
		if(PickGround(RayStart, RayDir, ptTarget))
		{
			pkLocalPlayer->MoveTo(ptTarget, 0 );

			if ( !m_pMouseEffect )
			{
				m_pMouseEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect( "Effect_joy_100830.nif" ,true, true, false, 3, false, false );
			}
			else
			{
				float fCurTime = SYState()->ClientApp->GetAccumTime();
				m_pMouseEffect->Activate( fCurTime, true );
				m_pMouseEffect->SetMaxLifeByEffectLife(3);
			}
			
			//pEffect->SetEffScale(MsgRecv.scale);
			m_pMouseEffect->AttachToScene( ptTarget );
			/*if((ptTarget - RayStart).Length() >= (pkTargetObject->GetPosition() - RayStart).Length())
			{
				AutoCancelProcess();
				bRet = Action_ClickTarget(pkTargetObject);
			}
			else
			{
				bRet = MouseMoveTargetProcess();
			}*/
		}
		//bRet = MouseMoveTargetProcess();
	}

	return bRet;
}

BOOL CPlayerInputMgr::RBClick()
{
	BOOL bRet = FALSE;
	NiPoint3 ptPickPos;

	NiInputSystem* pkInput = SYState()->Input;
	NIASSERT(pkInput);

	NiInputMouse* pkMouse = pkInput->GetMouse();
	NIASSERT(pkMouse);

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return FALSE;
	}

	if (pkLocal->IsHook())
	{
		return FALSE ;
	}
	EndSelectTarget();
	bool bRD = pkMouse->ButtonIsDown(NiInputMouse::NIM_RIGHT);
	//if (bRD) return FALSE;

	POINT ptMouse = GetCurMousePoint();
	CGameObject* TargetObject = (CGameObject*)ObjectMgr->GetObject( MouseMoveTargetID );//PickObject(ptMouse, ptPickPos, false, true);
	if (TargetObject && MouseBeginDragTargetID == MouseMoveTargetID)
	{
		if (TargetObject->GetGameObjectType() == GOT_CREATURE)
		{
			AutoCancelProcess();
			bRet = OnRequireCreature((CCreature*)TargetObject);
		}

		if (TargetObject->GetGameObjectType() == GOT_PLAYER)
		{
			AutoCancelProcess();
			bRet = OnRequirePlayer((CPlayer*)TargetObject);
		}
	/*	if (TargetObject->GetGameObjectType() == GOT_ITEM)
		{
			bRet = OnRequireSceneObject((CSceneGameObj*)TargetObject);
		}*/
		if (TargetObject->GetGameObjectType() == GOT_SCENEOBJECT)
		{
			AutoCancelProcess();
			bRet = OnRequireSceneObject((CSceneGameObj*)TargetObject);
		}

		if (TargetObject->GetGameObjectType() == GOT_ITEM && TargetObject->IsMapItem())
		{
			CSceneItem* pkItem = static_cast<CSceneItem*> (TargetObject);
			NiPoint3 itempos = pkItem->GetPosition();
			if (pkItem && (pkLocal->GetPosition() - itempos).Length() < 5.0)
				PacketBuilder->SendStartLoot(pkItem->GetGUID());
		}
	}
	MouseBeginDragTargetID = 0;
	return bRet;
}

BOOL CPlayerInputMgr::LBContinueDown()
{
	return FALSE;
}

void CPlayerInputMgr::PostUpdateTarget(DWORD dwTime)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLocalPlayer != NULL);

	if ( m_SwingId != 0 && m_SwingId == GetTargetID() && pkLocalPlayer->IsShouldStopAttack())
	{
		SpellCastTargets spellCastTarget;
		spellCastTarget.m_targetMask |= TARGET_FLAG_UNIT;
		spellCastTarget.m_unitTarget = m_SwingId;
		spellCastTarget.m_itemTarget = 0;
		OnSkillAction(m_SwingId, 1, spellCastTarget, false);
	}
}

void CPlayerInputMgr::UpdateTarget(DWORD Time)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLocalPlayer != NULL);

	if(GetTargetID() != 0)
	{
		CCharacter* Char = (CCharacter*)ObjectMgr->GetObject(m_TargetObjectID);
		if(Char == NULL)
		{
			if (!SocialitySys->GetTeamSysPtr()->InTeam(GetTargetID()))
			{
				//TeamShow->SetTargetID(INVALID_OBJECTID);
				SetTargetID(INVALID_OBJECTID);

				m_pkSelScenceObj->SetVisible(false);
				m_pkSelectLocation->SetVisible(false);
				//m_pkFlag->SetVisible(false);
				SetDecal(INVALID_OBJECTID);
			
			}
		}
	}

	UIRightMouseList* pkRightMouseList = INGAMEGETFRAME(UIRightMouseList,FRAME_IG_RIGHTMOUSELIST);

	if ( pkRightMouseList && pkRightMouseList->IsVisible() )
	{
		ui64 uiTargetObjectID = pkRightMouseList->GetGUID();
		CPlayer* pPlayer = (CPlayer*)ObjectMgr->GetObject(uiTargetObjectID);
		if (pPlayer != NULL)
		{
			NiPoint3 p = pPlayer->GetPosition();
			NiPoint3 dist = p - pkLocalPlayer->GetPosition();

			if ( dist.Length() < 5.0f )
			{
				pkRightMouseList->ChangeItemState(_TRAN("跟随"), UIRightMouseList::ItemEnable);
			}
			else
			{
				pkRightMouseList->ChangeItemState(_TRAN("跟随"), UIRightMouseList::ItemDisable);
			}

			if (dist.Length() < 10.0f)
			{
				pkRightMouseList->ChangeItemState(_TRAN("交易"), UIRightMouseList::ItemEnable);
			}else
			{
				pkRightMouseList->ChangeItemState(_TRAN("交易"), UIRightMouseList::ItemDisable);
			}	
		}
		else
		{
			pkRightMouseList->ChangeItemState(_TRAN("跟随"), UIRightMouseList::ItemDisable);
			pkRightMouseList->ChangeItemState(_TRAN("交易"), UIRightMouseList::ItemDisable);
		}
	}

	ui64 uiFollowID = 0;
	if ( (uiFollowID = pkLocalPlayer->GetFollowPlayerID()) != 0 ) {
		CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(uiFollowID);
		if (pkPlayer != NULL) {
			NiPoint3 ptTarget = pkPlayer->GetPosition();
			NiPoint3 ptSrc = pkLocalPlayer->GetPosition();

			NiPoint3 vDir = ptTarget - ptSrc;
			float fLength = vDir.Unitize();

			if (fLength > 5.0f)
			{
				pkLocalPlayer->StopMove();
				pkLocalPlayer->FollowPlayer(0);
			}
			else
			if (fLength <= pkLocalPlayer->GetFollowResponeRange() - 0.5f && pkLocalPlayer->IsMoving())
			{
				pkLocalPlayer->StopMove();
			}
			else
			if (fLength > pkLocalPlayer->GetFollowResponeRange() + 0.5f ) {
				pkLocalPlayer->MoveTo(ptTarget, 0);
			}
		}
		else
		{
			pkLocalPlayer->StopMove();
			pkLocalPlayer->FollowPlayer(0);
		}
	}
	

	//#2
	
}

void CPlayerInputMgr::UpdatePick()
{
	
}


void CPlayerInputMgr::UpdateDecal()
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)ObjectMgr->GetLocalPlayer();

	CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(m_TargetObjectID);

	if (pkChar)  //这个对象已经成为敌对单位 那么就重新设置过
	{
		bool bIsFriend = pkLocalPlayer->IsFriendly(pkChar);

		if ( m_bLastFriendCheck != bIsFriend )
		{
			SetDecal( m_TargetObjectID );
			m_bLastFriendCheck = bIsFriend;
		}
		
	}
}

void CPlayerInputMgr::ProcessTarget()
{

}

void CPlayerInputMgr::ClearQueueAction()
{
	m_NextAction.EventID = ACTION_IDLE;
}

BOOL CPlayerInputMgr::Action_ClickTarget(CGameObject* pTargetObj)
{
	if (pTargetObj == NULL)
	{
		return FALSE;
	}
	EGAMEOBJ_TYPE eTargetType = pTargetObj->GetGameObjectType();
	if (eTargetType == GOT_CREATURE)
	{
		if(pTargetObj->GetGUID() != m_TargetObjectID)
			SYState()->IAudio->PlaySound(SELECT_CREATER_BGM.c_str());
		return  OnSelectCreature((CCreature*)pTargetObj);
	}
	else if (eTargetType == GOT_PLAYER)
	{
		if(pTargetObj->GetGUID() != m_TargetObjectID)
			SYState()->IAudio->PlaySound(SELECT_CREATER_BGM.c_str());
		return OnSelectPlayer((CPlayer*)pTargetObj);
	}
	else if(eTargetType == GOT_SCENEOBJECT)
	{
		return FALSE ;// OnSelectSceneObj((CSceneGameObj*)pTargetObj);
	}
	else
	{
		SetTargetID(INVALID_OBJECTID);
	}

	return FALSE;
}

BOOL CPlayerInputMgr::OnSelectPos(const NiPoint3& WorldPos)
{
	return FALSE;
}

BOOL CPlayerInputMgr::OnRequirePlayer(CPlayer* pkPlayer)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal && pkLocal->IsFriendly(pkPlayer))
	{
		SetTargetID(pkPlayer->GetGUID());
	}else
	{
		
		return OnAttackAction(pkPlayer);
	}
	return TRUE ;
}

BOOL CPlayerInputMgr::OnRequireSceneObject(CSceneGameObj* pkSceneObj)
{
	SetTargetID(pkSceneObj->GetGUID());
	return TRUE;
	
}
BOOL CPlayerInputMgr::OnRequireCreature(CCreature* pkCreature)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if(pkLocalPlayer == NULL)
	{
		return FALSE;
	}

	NiPoint3 PlayerPos = pkLocalPlayer->GetLocalPlayerPos();
	NiPoint3 TargetPos = pkCreature->GetPosition();
	float Distance = (TargetPos - PlayerPos).Length();

	//如果对象死亡
	if (pkCreature->IsDead())
	{
		UPickItemList* PickList = INGAMEGETFRAME(UPickItemList,FRAME_IG_PICKLISTDLG);
		if (!PickList)
			return TRUE;
		//这里还需要判断距离.才能发送请求?
		//发送获取尸体上的物品列表的请求.
		//当前是否在和NPC交互， 只有cus_Normal 状态下才可以拾取
		if(CPlayerLocal::GetCUState() == cus_Pick && !PickList->IsVisible())
				CPlayerLocal::SetCUState(cus_Normal);

		if(CPlayerLocal::GetCUState() != cus_Pick)
			SetTargetID(pkCreature->GetGUID());

		if (Distance <= 5.0f && CPlayerLocal::GetCUState() == cus_Normal)
		{
			if (PickList->GetPickGuid() != pkCreature->GetGUID() && !PickList->IsVisible() &&
				pkCreature->HasFlag(UNIT_DYNAMIC_FLAGS, U_DYN_FLAG_LOOTABLE))
			{
				CPlayerLocal::SetCUState(cus_Pick);
				PacketBuilder->SendStartLoot(pkCreature->GetGUID());
			}
			
		}
		return TRUE;
	}

	
    if(!pkLocalPlayer->IsHostile(pkCreature))
    {
		SetTargetID(pkCreature->GetGUID());
        if( (  pkCreature->HasFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_QUESTGIVER) 
            || pkCreature->HasFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_VENDOR)
            || pkCreature->HasFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP)
            || pkCreature->HasFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_TRANSPORTER)
            || pkCreature->HasFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_BAISHI)
            )
            && Distance < 5.f)
        {
            if (CPlayerLocal::GetCUState() == cus_Normal || CPlayerLocal::GetCUState() == cus_NPC)
            {
                PacketBuilder->SendNpcGossipHelloMsg(pkCreature->GetGUID());
            }
            return TRUE;
        }
    }

	bool bCanAttack = !pkCreature->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_2);
	
	if(pkLocalPlayer->IsHostile(pkCreature) || bCanAttack)
	{
		if(pkLocalPlayer->HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_SHAPE_CANTFIGHT))
		{
			SetTargetID(pkCreature->GetGUID(), true);
			return FALSE;
		}
		if(pkLocalPlayer->HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_MOUNT_CANTFIGHT))
		{
			SetTargetID(pkCreature->GetGUID(), true);
			return FALSE;
		}
		if (OnAttackAction(pkCreature))
		{
			SetTargetID(pkCreature->GetGUID(), true);
			return TRUE;
		}
	}


	return TRUE;
}

BOOL CPlayerInputMgr::OnSelectCreature(CCreature* pkCreature)
{
	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if(pkLocalPlayer == NULL)
	{
		return FALSE;
	}

	//if (pkCreature->IsDead())
	//	return FALSE;

	SYObjID TargetGUID = pkCreature->GetGUID();

	// Select targetObject
	SetTargetID(TargetGUID);

	return TRUE;
}

BOOL CPlayerInputMgr::OnLocationSkillAction(SpellID spellid, const SpellCastTargets& target)
{
	SYStateAction Action;
	memset(&Action, 0, sizeof(SYStateAction));

	Action.Time = CUR_TIME;

	Action.EventID = ACTION_SKILL_ATTACK;
	Action.SPELL.spellid = spellid;
	Action.SPELL.m_unitTarget = target.m_unitTarget;
	Action.SPELL.m_itemTarget = target.m_itemTarget;
	Action.SPELL.m_targetMask = target.m_targetMask;
	Action.SPELL.m_srcX = target.m_srcX;
	Action.SPELL.m_srcY = target.m_srcY;
	Action.SPELL.m_srcZ = target.m_srcZ;
	Action.SPELL.m_destX = target.m_destX;
	Action.SPELL.m_destY = target.m_destY;
	Action.SPELL.m_destZ = target.m_destZ;

	EndSelectTarget();

	m_NextAction = Action;

	CPlayerLocal* pkLocalPlayer =  ObjectMgr->GetLocalPlayer();
	uint32 idSpell = 0;
	if( pkLocalPlayer->isMount() && !pkLocalPlayer->IsFlying()
		&& ( pkLocalPlayer->HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_MOUNT_CANTFIGHT) || g_pkSpellInfo->IsMountSpell( spellid ) ) )
	{
		if( pkLocalPlayer->GetMountSpell( idSpell ))
		{
			PacketBuilder->SendCancelAura(idSpell);
		}
	}

	return TRUE;
}



BOOL CPlayerInputMgr::OnSkillToItemAction(const SpellCastTargets& target)
{
	SYStateAction Action;
	memset(&Action, 0, sizeof(SYStateAction));

	Action.Time = CUR_TIME;

	Action.EventID = ACTION_SKILL_ATTACK;
	Action.SPELL.spellid = m_ItemSelSpellID;
	Action.SPELL.m_unitTarget = target.m_unitTarget;
	Action.SPELL.m_itemTarget = target.m_itemTarget;
	Action.SPELL.m_targetMask = target.m_targetMask;
	Action.SPELL.m_srcX = target.m_srcX;
	Action.SPELL.m_srcY = target.m_srcY;
	Action.SPELL.m_srcZ = target.m_srcZ;
	Action.SPELL.m_destX = target.m_destX;
	Action.SPELL.m_destY = target.m_destY;
	Action.SPELL.m_destZ = target.m_destZ;

	EndSelectTarget();

	m_NextAction = Action;

	return TRUE;
}

bool CPlayerInputMgr::isInFront(CCharacter* attcker, CCharacter* target)
{
	// check if we facing something ( is the object within a 180 degree slice of our positive y axis )
	double x = target->GetPosition().x - attcker->GetPosition().x; //m_position.x;
	double y = target->GetPosition().y - attcker->GetPosition().y; //m_position.y;

    double angle = NiATan2(-y, x) - NI_HALF_PI;
    angle = ( angle >= 0.0 ) ? angle : NI_TWO_PI + angle;
	angle -= attcker->GetFacing();

    while( angle > NI_PI)
        angle -= NI_TWO_PI;

    while(angle < -NI_PI)
        angle += NI_TWO_PI;

	// replace NI_PI in the two lines below to reduce or increase angle

    double left = -1.0 * ( NI_HALF_PI );
    double right = ( NI_HALF_PI );

    return( ( angle >= left ) && ( angle <= right ) );
}

BOOL CPlayerInputMgr::OnSkillAction(SYObjID id, SpellID spellid, const SpellCastTargets &target, bool bMoveTo /*= true*/)
{
	CCharacter* pkChar = static_cast<CCharacter*>(ObjectMgr->GetObject(id));
	if (!pkChar)
		return FALSE;

	return OnSkillAction(pkChar, spellid, target, bMoveTo);
}

BOOL CPlayerInputMgr::OnSkillAction(CCharacter* pkChar, SpellID spellid, const SpellCastTargets& target, bool bMoveTo)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if(!pkLocalPlayer || !pkChar)
		return FALSE;

	if ( !InputTestSpellUseItem(spellid) )
		return FALSE;

	NiPoint3 TargetPos = pkChar->GetPosition();
	
	if (pkChar->isMount())
	{
		if(pkChar->IsMountPlayer())
		{
			TargetPos =  pkChar->GetMountFoot()->GetSceneNode()->GetTranslate();
		}
		else if(pkChar->IsMountCreature())
		{
			TargetPos =  pkChar->GetMountCreature()->GetSceneNode()->GetTranslate();
		}
	}
	NiPoint3 PlayerPos = pkLocalPlayer->GetLocalPlayerPos();

	float Distance = (TargetPos - PlayerPos).Length();
	SYObjID TargetGUID = pkChar->GetGUID();

	// 设置输入操作
	SYStateAction Action;
	memset(&Action, 0, sizeof(SYStateAction));

	Action.EventID  = ACTION_SKILL_ATTACK;
	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate(spellid);
	NIASSERT(pSpellTemplate);

	float fMinRange = 0.0f;
	float fMaxRange = 0.0f;
	SpellTemplate::GetRange(spellid, fMinRange, fMaxRange);

	if(spellid == 1)
		fMaxRange = pkLocalPlayer->GetAttckRangeToTarget(pkChar);

	if (!bMoveTo && Distance > fMaxRange)
		return FALSE;

	EndSelectTarget();

	uint32 idSpell = 0;
	if( pkLocalPlayer->isMount() && !pkLocalPlayer->IsFlying()
		&& ( pkLocalPlayer->HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_MOUNT_CANTFIGHT) || g_pkSpellInfo->IsMountSpell( spellid ) ) && ( Distance < fMaxRange ) )
	{
		if( pkLocalPlayer->GetMountSpell( idSpell ))
		{
			PacketBuilder->SendCancelAura(idSpell);
		}
	}
	//冲撞类技能取消骑乘
	if(SpellTemplate::IsChongZhuangSpell(spellid))
	{
		if (pkLocalPlayer->isMount() && !pkLocalPlayer->IsFlying() &&  Distance < fMaxRange )
		{
			if (pkLocalPlayer->GetMountFoot())
			{
				EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("当前状态无法施法"));
				return FALSE ;
			}
			if( pkLocalPlayer->GetMountSpell(idSpell) )
			{
				PacketBuilder->SendCancelAura(idSpell);
			}
		}
	}
	/*
	if( pkLocalPlayer->GetShapeSpell( idSpell ))
	{
		PacketBuilder->SendCancelAura(idSpell);
	}
	*/

	Action.Time = CUR_TIME;
	Action.SPELL.spellid = spellid;
	Action.SPELL.m_unitTarget = target.m_unitTarget;
	Action.SPELL.m_targetMask = target.m_targetMask;
	Action.SPELL.m_itemTarget = target.m_itemTarget;
	Action.SPELL.m_srcX = target.m_srcX;
	Action.SPELL.m_srcY = target.m_srcY;
	Action.SPELL.m_srcZ = target.m_srcZ;
	Action.SPELL.m_destX = target.m_destX;
	Action.SPELL.m_destY = target.m_destY;
	Action.SPELL.m_destZ = target.m_destZ;

	m_NextAction = Action;

	return TRUE;
}

BOOL CPlayerInputMgr::OnAttackAction(CCharacter* pkChar)
{
	// UseSkillKeyboard
	UInGame* pkGamePlayInterface = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (!pkGamePlayInterface || !pkChar)
		return FALSE;

	CCreature* pkCreature = static_cast<CCreature*>(pkChar);
	if (pkCreature && pkCreature->IsPet())
	{
		SetTargetID(pkChar->GetGUID());
		return TRUE;
	}

	SetTargetID(pkChar->GetGUID());

	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer->GetClass() == CLASS_BOW) // 羽箭
	{
		ItemPrototype_Client* pkItemPrototype = pkLocalPlayer->GetMainHandWeaponPrototype();
		if (pkItemPrototype && (pkItemPrototype->SubClass == ITEM_SUBCLASS_WEAPON_BOW ||
			pkItemPrototype->SubClass == ITEM_SUBCLASS_WEAPON_GUN ||
			pkItemPrototype->SubClass == ITEM_SUBCLASS_WEAPON_CROSSBOW))
		{
			BeginSkillAttack(5003);
		}
		else
		{
			ChatSystem->AppendChatMsg(CHAT_MSG_SYSTEM, _TRAN("没有装备弓箭,自动使用普通攻击\n"));
			BeginSkillAttack(NORMAL_ATTACK_SPELL_ID);
		}
	}
	else
	{
		BeginSkillAttack(NORMAL_ATTACK_SPELL_ID);
	}

	return TRUE;
}

BOOL CPlayerInputMgr::OnSelectPlayer(CPlayer* Char)
{
	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer == Char || pkLocalPlayer == NULL)
	{
		return FALSE;
	}

	SetTargetID((SYObjID)Char->GetGUID());
	EndSelectTarget();

	return TRUE;
}

BOOL CPlayerInputMgr::OnSelectSceneObj(CSceneGameObj* pkGameObj)
{
	OnRequireSceneObject(pkGameObj);
	EndSelectTarget();
	return TRUE;
}

// 移动到操作位置, 包含纯粹的移动操作.
BOOL CPlayerInputMgr::MoveToActionPos(const NiPoint3& ActionPos, UINT flag)
{

	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();

	if(pkLocalPlayer && !IsMoveLocked() && pkLocalPlayer->CanMove())
	{
		// 设置移动参数, 并在客户端计算是否可以移动到目的地(AI & COL)
		pkLocalPlayer->SetAutoActionPos( true );
		pkLocalPlayer->MoveTo(ActionPos, flag);
		return TRUE;
	}

	

	return FALSE;
}

BOOL CPlayerInputMgr::MoveToActionTarget(const SYObjID& ActionTarget, float fRange, UINT flag)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();

	if(pkLocalPlayer && !IsMoveLocked())
	{
		SetTargetID(ActionTarget);

		/*CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(ActionTarget);
		if (!pkChar)
			return FALSE;

		NiPoint3 kTargetPos, kLocalPos;
		kTargetPos = pkChar->GetPosition();
		if (pkChar->isMount())
		{
			if(pkChar->IsMountPlayer())
			{
				kTargetPos =  pkChar->GetMountFoot()->GetSceneNode()->GetTranslate();
			}
			else if(pkChar->IsMountCreature())
			{
				kTargetPos =  pkChar->GetMountCreature()->GetSceneNode()->GetTranslate();
			}
		}
		kLocalPos = pkLocalPlayer->GetLocalPlayerPos();

		NiPoint3 kDir = kLocalPos - kTargetPos;
		kDir.Unitize();
		kTargetPos = kTargetPos + kDir * fRange;*/

		//MoveToActionPos(kTargetPos, flag);

		if(pkLocalPlayer->CanMove())
		{
			// 设置移动参数, 并在客户端计算是否可以移动到目的地(AI & COL)
			pkLocalPlayer->SetAutoActionPos( true );
			pkLocalPlayer->SetMoveToTarget(fRange, flag);
			
			return TRUE;
		}
		
		return TRUE;
	}
	return FALSE;
}

// 这里的累加动作不能够随便压入到玩家的动作列表中, 因为玩家输入动作并不代表真实有效的,而是一种临时的.
// 比如一个 移动 --> 攻击 那么攻击会作为移动的累加动作保存, 但是此刻玩家可能又输入一个移动操作, 那么这个
// 累加的攻击操作就被清空了. 如果放入玩家动作列表, 那么将会被轮询,并最终执行.
BOOL CPlayerInputMgr::ProcessNextAction()
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)ObjectMgr->GetLocalPlayer();
	if( pkLocalPlayer->IsDead() )
	{
		return FALSE;
	}
	if (pkLocalPlayer->IsMoveToTarget() && !pkLocalPlayer->IsFleeing() )
		return FALSE;

	BOOL Ret = FALSE;
	DWORD ActionTime = m_NextAction.Time;
	switch(m_NextAction.EventID)
	{
	case ACTION_ATTACK:
		//Ret = ProcessNextAttack(ActionTime);
		NIASSERT(0);
		break;
	case ACTION_SKILL_ATTACK:
		Ret = ProcessNextSkillAttack(ActionTime);
		break;
	case ACTION_PICKITEM:
		Ret = ProcessPickItem(ActionTime);
		break;
	}
	
	if (Ret)
	{
		pkLocalPlayer->FollowPlayer(0);
	}
	
	if(m_bAutoCancelProcess)
	{
		ClearQueueAction();
		m_bAutoCancelProcess = false;

		return FALSE;
	}

	if(m_bClearnQueueAction)
		ClearQueueAction();

	m_bClearnQueueAction = TRUE;

	return Ret;
}

// 处理累加的技能攻击操作.
BOOL CPlayerInputMgr::ProcessNextSkillAttack(DWORD AtkTime)
{
	BOOL bRet = FALSE;
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer == NULL)
	{
		return FALSE;
	}

	if(pkLocalPlayer->GetMountHead()/* || pkLocalPlayer->GetUInt32Value(UNIT_FIELD_FORMDISPLAYID)*/)
	{
		return FALSE;
	}

	if(pkLocalPlayer->IsDead())
	{
		return FALSE;
	}

	UpdateObj* obj = NULL;
	uint64 targetID = m_NextAction.SPELL.m_unitTarget;
	if (targetID != INVALID_OBJECTID)
	{
		obj = (UpdateObj*)ObjectMgr->GetObject(targetID);
		if (!obj)
		{
			NIASSERT(0);
			return FALSE;
		}

		float fMinRange = 0.0f;
		float fMaxRange = 0.0f;
		NiPoint3 TargetPos = ((CGameObject*)obj)->GetPosition();
		CCharacter* pkCharOBJ = (CCharacter*)obj;		
		if (pkCharOBJ && pkCharOBJ->isMount() )
		{
			if (pkCharOBJ->IsMountPlayer())
			{
				TargetPos = pkCharOBJ->GetMountFoot()->GetPosition();
			}else if (pkCharOBJ->IsMountCreature())
			{
				TargetPos = pkCharOBJ->GetMountCreature()->GetPosition();
			}
		}
		
		NiPoint3 PlayerPos = pkLocalPlayer->GetLocalPlayerPos();
		float Distance = (TargetPos - PlayerPos).Length();

		SpellTemplate::GetRange(m_NextAction.SPELL.spellid, fMinRange, fMaxRange);
		if(m_NextAction.SPELL.spellid == NORMAL_ATTACK_SPELL_ID)
		{
			fMaxRange = pkLocalPlayer->GetAttckRangeToTarget(((CCharacter*)obj));
		}
		if ( Distance > fMaxRange)
		{
			if ( m_bAutoAction || m_bUseMouseToMove)
			{
				CCharacter* pChar = (CCharacter*)obj;
				if ( pChar->IsFlying() )
				{
					return TRUE;
				}
				if ( pkLocalPlayer->IsFlying() )
				{
					EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("您需要再靠近一点"));
					return TRUE;
				}
				
				if(pChar->isType(TYPE_UNIT) && pChar->IsMoving())
				{
					CCreature* pCreature = (CCreature*)pChar;
				}
				
				MoveToActionTarget(targetID, fMaxRange - 0.3f, CMF_AttackMove);
				
				m_NextAction.Time = CUR_TIME;
				m_bClearnQueueAction = FALSE;
				return TRUE;
			}
			else
			{
				EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("您需要再靠近一点"));
				return FALSE;
			}
		}
	}
	
	if (m_NextAction.SPELL.spellid == NORMAL_ATTACK_SPELL_ID)
	{
		obj = (UpdateObj*)ObjectMgr->GetObject(targetID);
		if (!obj)
		{
			NIASSERT(0);
			return FALSE;
		}

		if (m_SwingId != 0 && m_SwingId != pkLocalPlayer->GetTargetObjectID())
		{
			m_SwingId = INVALID_OBJECTID;
			return FALSE;
		}

		m_SwingId = INVALID_OBJECTID;

		static uint32 lastswing = 0;
		uint32 now = GetTickCount();
		if( lastswing == 0 || now - lastswing >= 900 )
		{
			bRet = PacketBuilder->SendAttackReqMsg((CGameObject*)obj);
			lastswing = now;
		}
		return bRet;
	}
	else
	{
		int spellid = m_NextAction.SPELL.spellid;
		SpellTemplate* pkST = SYState()->SkillMgr->GetSpellTemplate(spellid);
		if (!pkST)
			return false;

		bool rush = false;
		const stSpellInfo& si = pkST->GetSpellInfo();
		for(int i = 0; i < 3; i++)
		{
			if (si.uiEffect[i] == SPELL_EFFECT_CHONGZHUANG)
			{
				rush = true;
				break;
			}
		}
		
		if (rush)
		{
			CCharacter* pkTarget = (CCharacter*)obj;
			if (!pkLocalPlayer->IsMoving())
			{
				NiPoint3 kDir = pkTarget->GetPosition() - pkLocalPlayer->GetPosition();
				kDir.Unitize();

				float fZ = NiATan2(-kDir.y, kDir.x) - NI_PI/2;
				while( fZ < 0.f )
					fZ += NI_TWO_PI;

				while( fZ > NI_TWO_PI )
					fZ -= NI_TWO_PI;

				pkLocalPlayer->StopMove();
				pkLocalPlayer->StartForward();
				pkLocalPlayer->SetDesiredAngle(fZ);
				pkLocalPlayer->SendMovementPacket( MOVE_SET_FACING );
			}

			NiPoint3 target = pkTarget->GetPosition();
			float reach = pkLocalPlayer->GetAttckRangeToTarget(pkTarget);
			reach -= 0.3f;

			NiPoint3 dir = pkLocalPlayer->GetPosition() - target;
			dir.Unitize();
			target += dir * reach;

			NiPoint3 s, e;
			float t, tarz;
			s = NiPoint3(target.x, target.y, target.z + 5.0f);
			e = NiPoint3(target.x, target.y, target.z - 5.0f);
			if (CMap::Get()->CollideLine(s, e, t))
				tarz = s.z * (1 - t) + e.z * t;
			else
			{
				SPELL_LOG("移动目标失败");
				return false;
			}

			m_NextAction.SPELL.m_destX = target.x;
			m_NextAction.SPELL.m_destY = target.y;
			m_NextAction.SPELL.m_destZ = tarz;
		}
		else
		{
			if( pkLocalPlayer->GetDesiredAngle() != pkLocalPlayer->GetLastDesirdAngle() )
			{
				float fTime = SYState()->ClientApp->GetAccumTime();
				float fCurMoveAngle, fLastMoveAngle;
				uint32 uiMovementFlags = pkLocalPlayer->GetMovementFlags();
				fCurMoveAngle = pkLocalPlayer->GetDesiredAngle();

				pkLocalPlayer->SetSendMovementDuration(0.5f);
				pkLocalPlayer->SetLastSendMovementTime(fTime);
				pkLocalPlayer->SetLastMovementFlags(uiMovementFlags);
				pkLocalPlayer->SendMovementPacket( MOVE_SET_FACING );
				pkLocalPlayer->SetLastDesiredAngle(fCurMoveAngle);
			}
		}

		if (spellid == 22401 || spellid == 88822401)
		{
			/*static unsigned int limitcount = 10;
			NiPoint3 target;
			unsigned int icount = 0;
			bool nextsearch = true;
			bool succ = false;
			do
			{
				icount ++;

				float frand1, frand2;
				frand1 = (float)rand()/RAND_MAX;
				frand2 = (float)rand()/RAND_MAX;
				float randangle = frand1*NI_TWO_PI;
				float dist = 5.0f + frand2 * (12.0f - 5.0f);

				float sn, cs;
				NiSinCos(randangle, sn, cs);

				NiPoint3 curpos = pkLocalPlayer->GetPosition();

				NiPoint3 randvec, tar;
				randvec = NiPoint3(sn, cs, 0);
				tar = randvec * dist;
				tar += curpos;

				float startz, tarz;
				NiPoint3 s, e;

				startz = curpos.z;
				s = NiPoint3(tar.x, tar.y, startz + 10.0f);
				e = NiPoint3(tar.x, tar.y, startz - 10.0f);
				ResultList ret;
				bool col = CMap::Get()->MultiCollideBox(s, e, NiPoint3::ZERO, ret);

				if (col)
				{
					tarz = s.z + (e.z-s.z)*ret[0].fTime;
					if (fabs(tarz - startz) <= 2.0f)
					{
						nextsearch = false;
						target = tar;
						target.z = tarz;
						succ = true;
					}
				}

				if (icount >= limitcount)
				{
					nextsearch = false;
					succ = false;
				}
			}while(nextsearch);

			if (!succ)
			{
				SPELL_LOG("移动目标失败");
				return succ;
			}*/

			//m_NextAction.SPELL.m_destX = target.x;
			//m_NextAction.SPELL.m_destY = target.y;
			//m_NextAction.SPELL.m_destZ = target.z;
		}

		SYState()->SkillMgr->AddGlobalCooldown(pkST->GetStartRecoveryTime());

		SpellCastTargets spellCastTarget(m_NextAction.SPELL.m_targetMask, m_NextAction.SPELL.m_unitTarget,
			m_NextAction.SPELL.m_itemTarget, m_NextAction.SPELL.m_srcX, m_NextAction.SPELL.m_srcY, m_NextAction.SPELL.m_srcZ,
			m_NextAction.SPELL.m_destX, m_NextAction.SPELL.m_destY, m_NextAction.SPELL.m_destZ);

		bRet = PacketBuilder->SendCastSpell(spellid, spellCastTarget);
	}

	return bRet;
}

BOOL CPlayerInputMgr::ProcessPickItem(DWORD ActionTime)
{
	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if(pkLocalPlayer == NULL)
	{
		return TRUE;
	}

	UpdateObj* obj = (UpdateObj*)ObjectMgr->GetObject(m_NextAction.PICKITEM.TargetID);
	if( obj && !obj->isType(TYPE_ITEM) )
	{
		return TRUE;
	}
	CSceneItem* Target = (CSceneItem*)obj;
	if(Target == NULL)
	{
		return TRUE;
	}

	// 检查距离
	const NiPoint3& PlayerPos = pkLocalPlayer->GetPosition();
	NiPoint3 TargetPos = Target->GetPosition();
	float Distance = (TargetPos - PlayerPos).Length();

#define Item_Pick_Dis 1.3
	if(Distance < Item_Pick_Dis)
	{
		pkLocalPlayer->StopMove();
		// Temp
		pkLocalPlayer->SetNextState(STATE_IDLE, CUR_TIME);
		PacketBuilder->SendMapItemPickUpReqMsg(Target->GetGUID());
		return TRUE;
	}
	return FALSE;
}

void CPlayerInputMgr::LockInput()
{
	LockMove();
	LockAttack();
}

void CPlayerInputMgr::UnlockInput()
{
	UnlockMove();
	UnlockAttack();
}
void CPlayerInputMgr::ClearInput()
{
	//UnlockMove();
	m_iLockAC = 0;
	m_iLockMC = 0;
	if( m_iLockAC > 0 )
		m_iLockAC--;
}

BOOL CPlayerInputMgr::CanDoAttackAction(const SpellID& spellid) const
{
	CPlayer* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (!pkLocalPlayer)
	{
		return FALSE;
	}
	if (spellid != 13301 && 
		spellid != 13302 &&
		spellid != 13303 &&
		spellid != 22701 &&
		spellid != 771 &&
		(IsAttackLocked() || pkLocalPlayer->IsDead()))
	{
		return FALSE;
	}

	return TRUE;
}
BOOL CPlayerInputMgr::CheckSkillShanXian(float& radius, BOOL isShanXian01)
{
	//验证 本地玩家 正前方的20M 范围内的可以行走的区域
	// 查找到不可以行走区域的边界.
	//
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (!pkLocalPlayer)
		return FALSE;

	NiPoint3 kPos = pkLocalPlayer->GetLocalPlayerPos();
	NiPoint3 Rotate = pkLocalPlayer->GetRotate();

	if (!isShanXian01)
	{
		Rotate.z += NI_PI ; 
	}

	float randangle = NI_HALF_PI -  Rotate.z ;
	

	float newRadius = radius;

	bool CanShanxian = FALSE ;

	CMap* pkMap = CMap::Get();

	if (!pkMap)
	{
		return FALSE ;
	}

	for (int i = 0 ; i < 20; i++)
	{
		float rad =  i * (newRadius / 20.f) ;
		float radA = (i + 1) * (newRadius / 20.f);

		NiPoint3 NIRad;
		NiPoint3 NIRadA;

		NIRad.x= kPos.x - (rad*(cosf(randangle)));
		NIRad.y = kPos.y - (rad*(sinf(randangle)));

		NIRadA.x= kPos.x - (radA*(cosf(randangle)));
		NIRadA.y = kPos.y - (radA*(sinf(randangle)));


		bool flag = false ;
		bool flagA = false;
		pkMap->GetGridInfo(NIRad.x, NIRad.y,false, flag);
		pkMap->GetGridInfo(NIRadA.x, NIRadA.y,false, flagA);

		if ((flag && !flagA))
		{
		
			CanShanxian = true;
			newRadius = rad;
			break ;
		}

		if (i == 19 && flagA)
		{
			CanShanxian = true;
			newRadius = radius;
			break ;
		}

	}

	if (!CanShanxian)
	{
		return FALSE ;
	}

	radius = newRadius;
	return TRUE ;
}
#define  MIN_WALK_NORMAL 0.7f
BOOL CPlayerInputMgr::UseSkillShanXian(float radius, BOOL isShanXian01)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (!pkLocalPlayer)
		return FALSE;

	NiPoint3 kPos = pkLocalPlayer->GetLocalPlayerPos();
	NiPoint3 Rotate = pkLocalPlayer->GetRotate();


	if (radius < 0.5f)
	{
		PacketBuilder->SendLeapPrepar(kPos, Rotate.z);

		return FALSE;
	}

	CMap* pkMap = CMap::Get();
	if (!isShanXian01)
	{
		Rotate.z += NI_PI;
	}
	float randangle = NI_HALF_PI -  Rotate.z;

	kPos.z += EXTENT.z ;

	NiPoint3 NewPos;
	NewPos.x= kPos.x - (radius*(cosf(randangle)));
	NewPos.y = kPos.y - (radius*(sinf(randangle)));

	NewPos.z = kPos.z; 

	const float HeightJiaodu = NI_HALF_PI / 3 ;
	

	ResultList scollList;
	NiPoint3 s, e;

	s = NewPos + NiPoint3(0.0f, 0.0f, 30.0f) ;
	e = NewPos - NiPoint3(0.0f, 0.0f, 30.0f) ;

	if (pkMap->MultiCollideBox(s,e, EXTENT, scollList))
	{

		Result result = scollList[0];
		if (result.fTime < 1.0f)
		{
			NewPos.z = s.z + (e.z-s.z)*scollList[0].fTime;	
		}
	}


	if (NewPos.z < pkMap->GetHeight(NewPos.x , NewPos.y))
	{
		NewPos.z = pkMap->GetHeight(NewPos.x , NewPos.y);
	}

	ResultList TcollList;
	BOOL bCheck = FALSE;
	if (pkMap->MultiCollideBox(kPos  ,  NewPos, EXTENT, TcollList))
	{
		for (int i =0; i < TcollList.size() ;i++ )
		{
			if (TcollList[i].fTime < 1.0f)
			{
				if (TcollList[i].kNormal.z < MIN_WALK_NORMAL)
				{
					NewPos = kPos + (NewPos - kPos) * TcollList[i].fTime ;

					bCheck = TRUE;
					break ;
				}
				
			}
		}
	}


	if (bCheck)
	{
		ResultList CheckList;
		NiPoint3 start, end ;

		start = NewPos + NiPoint3(0.0f, 0.0f, 6.0f) ;
		end =   NewPos - NiPoint3(0.0f, 0.0f, 4.0f) ;

		if (pkMap->MultiCollideBox(start, end, EXTENT, CheckList))
		{
			if (CheckList.size())
			{
				if (CheckList[0].fTime < 1.0f)
				{
					NewPos.z = start.z + (end.z - start.z) * CheckList[0].fTime ;
				}
			}
		}

	}else
	{
		NewPos.z -= EXTENT.z ;
	}

	float h =  NewPos.z - kPos.z ;

	NiPoint3 lP = NewPos - kPos ;
	lP.z = 0;

	float MaxHeight =  tanf(HeightJiaodu) * lP.Length();

	if (MaxHeight < 2.0f)
	{
		MaxHeight = 2.0f;
	}

	if (MaxHeight > 5.f)
	{
		MaxHeight = 5.f;
	}
	
	if (fabs(h) >  MaxHeight)
	{
		return UseSkillShanXian(radius - 0.5f);
	}
	if (!isShanXian01)
	{
		Rotate.z -= NI_PI;
	}
	PacketBuilder->SendLeapPrepar(NewPos, Rotate.z);


	return TRUE;
}
BOOL CPlayerInputMgr::CanUseSkill(SpellID spellid)
{

	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (!pkLocalPlayer)
	{
		return FALSE ;
	}
	if (pkLocalPlayer->IsSleep())
	{
		SPELL_LOG("睡眠状态不能这样做");
		return FALSE;
	}

	if (!CanDoAttackAction(spellid))
	{
		return FALSE;
	}

	ui32 RecoverTime, RemainTime ;
	bool Find = SYState()->SkillMgr->GetCooldownTime(spellid, RecoverTime, RemainTime);
	bool bGlobal = SYState()->SkillMgr->GetGlobalCooldownTime(spellid,RecoverTime,RemainTime);

	if (Find)
	{
		return FALSE ;
	}

	if (bGlobal)
	{
		return FALSE ;
	}

	bool bCancast = SYState()->SkillMgr->CooldownCanCast(spellid);
	if (!bCancast)
	{
		SPELL_LOG(SpellTemplate::GetSpellErrorString(SPELL_FAILED_NOT_READY));
		return FALSE;
	}


	SpellTemplate* pkST = SYState()->SkillMgr->GetSpellTemplate(spellid);
	if (!pkST)
		return FALSE;

	if (!pkST->CheckOwnerRequest(pkLocalPlayer))
		return FALSE;


	if (pkST->GetCastIndex() != 1)
	{
		if (pkLocalPlayer->GetCurPostureState() == STATE_MOVE ||
			pkLocalPlayer->GetCurPostureState() == STATE_FALLING)
		{
			SPELL_LOG("不能在移动中执行该动作");
			return FALSE;
		}
	}

	return TRUE;


}
BOOL CPlayerInputMgr::BeginSkillAttack(SpellID spellid)
{
	USpecialityDlg* pSpeciality = INGAMEGETFRAME(USpecialityDlg, FRAME_IG_SPECIALITYDLG);
	if (pSpeciality)
	{
		if(pSpeciality->CallBuild(spellid))
			return TRUE;
	}

	if (!CanDoAttackAction(spellid))
	{
		return FALSE;
	}

	if (spellid == INVALID_SPELL_ID)
		return FALSE;

	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer && !pkLocalPlayer->IsHook())
	{
		if (pkLocalPlayer->GetAutoActionPos() )
		{
			return FALSE;
		}
	}
	

	AutoCancelProcess();

	bool bCancast = SYState()->SkillMgr->CooldownCanCast(spellid);
	if (!bCancast)
	{
		SPELL_LOG(SpellTemplate::GetSpellErrorString(SPELL_FAILED_NOT_READY));
		return FALSE;
	}

	
	if (!pkLocalPlayer)
		return FALSE;

	if (2101 == spellid)
	{
		uint32 enchantteacher = pkLocalPlayer->GetUInt32Value(PLAYER_FIELD_ENCHANT_TEACHER_CHARGES);
		if (!enchantteacher)
		{
			SPELL_LOG("技能失败：达到技能施法次数上限。");
			return FALSE;
		}
	}


	SpellTemplate* pkST = SYState()->SkillMgr->GetSpellTemplate(spellid);
	if (!pkST)
		return FALSE;

	if (!pkST->CheckOwnerRequest(pkLocalPlayer))
		return FALSE;


	float fMin = 0.0f, fMax = 0.0f;
	// pkST->GetRange(fMin, fMax);
	SpellTemplate::GetRange(spellid, fMin, fMax);
	m_fSpellMaxRadius = fMax;

	SpellCastTargets spellCastTarget;
	uint32 uiMask = pkST->GetSelectMethod();
	spellCastTarget.m_targetMask = uiMask;
	spellCastTarget.m_itemTarget = 0;

	if (pkST->GetCastIndex() != 1 && !(uiMask & TARGET_FLAG_DEST_LOCATION))
	{
		if (pkLocalPlayer->GetCurPostureState() == STATE_MOVE ||
			pkLocalPlayer->GetCurPostureState() == STATE_FALLING)
		{
			SPELL_LOG("技能失败：不能在移动中执行该动作。");
			return FALSE;
		}
	}

	//闪现技能
	if (spellid == 88822401 || spellid == 22401 || spellid == 45001 || spellid == 22402)
	{
		if (!CMap::Get())
		{
			return FALSE;
		}

		BOOL isShanXian01 = TRUE ;
		if (spellid == 45001)
		{
			isShanXian01 = FALSE ;
		}

		SpellCastTargets spllTarget;
		spellCastTarget.m_targetMask = TARGET_FLAG_UNK1;
	
		float SpellShanXianradius = 20.0f;
		if (spellid == 22402)
		{
			SpellShanXianradius = 28.0f;
		}
	
		if (!CheckSkillShanXian(SpellShanXianradius, isShanXian01))
		{
			NiPoint3 kPos = pkLocalPlayer->GetLocalPlayerPos();
			NiPoint3 Rotate = pkLocalPlayer->GetRotate();
		
			PacketBuilder->SendLeapPrepar(kPos, Rotate.z);
		}else
		{
			UseSkillShanXian(SpellShanXianradius, isShanXian01);
			
		}
			
		return OnSkillAction(pkLocalPlayer, spellid, spellCastTarget);

	}

	CCharacter* pkTarget = pkLocalPlayer->GetTargetObject();

	bool rush = false;
	const stSpellInfo& si = pkST->GetSpellInfo();
	for(int i = 0; i < 3; i++)
	{
		if (si.uiEffect[i] == SPELL_EFFECT_CHONGZHUANG)
		{
			rush = true;
			break;
		}
	}

	if ( rush )
	{
		if (!pkTarget)
		{
			SPELL_LOG("技能失败：请选择一个目标。");
			return FALSE;
		}

		SpellCastTargets spllTarget;
		spellCastTarget.m_targetMask = TARGET_FLAG_DEST_LOCATION;
		spellCastTarget.m_unitTarget = pkTarget->GetGUID();
		//spellCastTarget.m_destX = target.x;
		//spellCastTarget.m_destY = target.y;
		//spellCastTarget.m_destZ = tarz;

		SetTargetID(pkTarget->GetGUID(), true);
		return OnSkillAction(pkTarget, spellid, spellCastTarget);

	}

	if (((uiMask & TARGET_FLAG_SELF) && pkTarget == pkLocalPlayer) || 
		(uiMask == TARGET_FLAG_SELF) ||
		(uiMask & TARGET_FLAG_SOURCE_LOCATION))
	{
		spellCastTarget.m_unitTarget = pkLocalPlayer->GetGUID();
		if (uiMask & TARGET_FLAG_SOURCE_LOCATION)
		{
			spellCastTarget.m_srcX = pkLocalPlayer->GetPosition().x;
			spellCastTarget.m_srcY = pkLocalPlayer->GetPosition().y;
			spellCastTarget.m_srcZ = pkLocalPlayer->GetPosition().z;
		}

		return	OnSkillAction(pkLocalPlayer, spellid, spellCastTarget);
	}
	else
	if (uiMask & TARGET_FLAG_DEST_LOCATION)
	{
		pkST->SelectTarget(pkLocalPlayer);
	}
	else
	if (uiMask & TARGET_FLAG_ITEM)
	{
		SelectItem( spellid );
	}
	else
	if (uiMask & TARGET_FLAG_UNIT)
	{
		CCharacter* pkTarget = pkLocalPlayer->GetTargetObject();
		bool bAutoSelTarget = false;

		if(!pkTarget)
		{
			if(SpellTemplate::IsSelfOrFriendlySpell(pkST->GetSpellID()))
			{
				bAutoSelTarget = true;
				if(TeamShow->GetTargetID())
				{
					SPELL_LOG("技能失败：目标距离太远，无法施法。");
					return FALSE;
				}
				pkTarget = pkLocalPlayer;
			}
			else
			{
				SPELL_LOG("技能失败：请选择一个目标。");
				return FALSE;
			}
		}

		if(SpellTemplate::IsSelfSpell(pkST->GetSpellID()))
		{
			bAutoSelTarget = true;
		}
		else
		{
			if (pkTarget->IsFriendly(pkLocalPlayer))
			{
				if(SpellTemplate::IsEnemySpell(pkST->GetSpellID()))
				{
					//SPELL_LOG("技能失败：目标为友好状态。");
					return FALSE;
				}

				if(pkTarget->IsDead())
				{
					if(!SpellTemplate::IsResurrectSpell(pkST->GetSpellID()))
					{
						SPELL_LOG(SpellTemplate::GetSpellErrorString(SPELL_FAILED_BAD_TARGETS));
						return FALSE;
					}
				}
			}
			else
			{
				if(SpellTemplate::IsSelfOrFriendlySpell(pkST->GetSpellID()))
				{
					bAutoSelTarget = true;
				}
				else
				if(pkTarget->IsDead())
				{
					SPELL_LOG(SpellTemplate::GetSpellErrorString(SPELL_FAILED_TARGETS_DEAD));
					return FALSE;
				}
			}
		}
	
// 
// 		if(pkTarget->IsDead() && !pkTarget->IsFriendly(pkLocalPlayer))
// 		{
// 			if(SpellTemplate::IsSelfOrFriendlySpell(pkST->GetSpellID()))
// 			{
// 				bAutoSelTarget = true;
// 			}		
// 		}

		bool bCanAttack = bool(!pkTarget->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_2));
		if (!(pkST->GetSelectMethod() & TARGET_FLAG_SELF) && pkLocalPlayer == pkTarget)
		{
			SPELL_LOG(SpellTemplate::GetSpellErrorString(SPELL_FAILED_BAD_TARGETS));
			return FALSE;
		}

		if (!pkLocalPlayer->IsMoving())
		{
			NiPoint3 TargetPos = pkTarget->GetPosition();
			if (pkTarget->isMount())
			{
				if(pkTarget->IsMountPlayer())
				{
					TargetPos =  pkTarget->GetMountFoot()->GetSceneNode()->GetTranslate();
				}
				else if(pkTarget->IsMountCreature())
				{
					TargetPos =  pkTarget->GetMountCreature()->GetSceneNode()->GetTranslate();
				}

			}
			if ( ( TargetPos != pkLocalPlayer->GetLocalPlayerPos() ) && pkLocalPlayer->IsHook())
			{
				NiPoint3 kDir = TargetPos - pkLocalPlayer->GetLocalPlayerPos();

				kDir.Unitize();

				float fZ = NiATan2(-kDir.y, kDir.x) - NI_PI/2;
				while( fZ < 0.f )
					fZ += NI_TWO_PI;

				while( fZ > NI_TWO_PI )
					fZ -= NI_TWO_PI;

				pkLocalPlayer->SetDesiredAngle(fZ, true);
				pkLocalPlayer->SendMovementPacket(MOVE_SET_FACING);
			}
		}

		if(bAutoSelTarget)
		{
			spellCastTarget.m_unitTarget = pkLocalPlayer->GetGUID();
			return OnSkillAction(pkLocalPlayer, spellid, spellCastTarget, !pkLocalPlayer->IsMoving());
			//pkTarget = pkLocalPlayer;
		}
		else
		{
			if ( !pkLocalPlayer->CanLookTarget( pkTarget ) ) //如果看不见对方
			{
				EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("目标不在视野中"));
				return FALSE;
			}
		}
		SetTargetID(pkTarget->GetGUID(), true);
		spellCastTarget.m_unitTarget = pkTarget->GetGUID();
		return OnSkillAction(pkTarget, spellid, spellCastTarget, !pkLocalPlayer->IsMoving());
	}

	return TRUE ;
}

void CPlayerInputMgr::SelectTarget(INT SelFlag, INT SelAreaFlag, SpellID spellid, float fRadius, const char* Brush, float fBrushHeight)
{
	m_ItemSelectInfo.bSelectByItem = false;
	if (m_pkSelectLocation)
	{
		m_pkSelectLocation->SetVisible(true);
		m_pkSelectLocation->SetRadius(fRadius);
		m_pkSelectLocation->SetBrushHeight(fBrushHeight);
//		m_pkSelectLocation->Update( 0, 0 );
		if (Brush && Brush[0])
		{
			m_pkSelectLocation->SetTexture(Brush);
		}
		else
		{
			m_pkSelectLocation->SetTexture("Texture\\Decal\\sign_runno_004.dds");
		}
	}

	m_LocationSelSpellID = spellid;
	m_bSpellLocationSelect = true;
}


void CPlayerInputMgr::SelectItem( SpellID spellid )
{
	m_ItemSelSpellID = spellid;
	SYState()->ClientApp->SetCursor(SYC_ENCHANT);
	m_bSpellItemSelect = true;
}


int CPlayerInputMgr::GetFaction(SYObjID uiTargetID)
{
	return 0;
}

void CPlayerInputMgr::SetDecal(SYObjID uiTargetID)
{
	if (/*m_TargetObjectID == uiTargetID &&*/ m_pkDecal->IsVisible())
	{
		//return;
	}
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)ObjectMgr->GetLocalPlayer();

	float fRadius = 1.0f;
	unsigned int uiColor = 0xFFFFFFFF;
	NiColorA kColor;
	
	if (m_TargetObjectID)
	{
		CCharacter* TargetObject = (CCharacter*)ObjectMgr->GetObject(m_TargetObjectID);
		if (TargetObject)
		{
			TargetObject->HilightObject(FALSE);
		}
	}
	CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(uiTargetID);
	if (pkChar)
	{
		if (pkChar->GetSceneNode())
		{
			NiBound kBound;// = pkChar->GetSceneNode()->GetWorldBound();
			bool b = pkChar->GetPickBound(kBound);
			NIASSERT(b);
			fRadius = kBound.GetRadius() * 0.5f;
		}

		if(pkLocalPlayer->GetGUID()==uiTargetID)
		{
			kColor= NiColorA(1.0f, 1.0f, 1.0f, 1.0f);
		}
		else if(pkLocalPlayer->IsFriendly(pkChar))
		{
			kColor= NiColorA(0.0f, 1.0f, 0.0f, 1.0f);
		}
		else if( pkChar->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT) || 
			pkLocalPlayer->IsHostile(pkChar) || 
			(pkChar->isType( TYPE_UNIT ) && !pkLocalPlayer->IsFriendly(pkChar)))
		{
			kColor= NiColorA(1.0f, 0.0f, 0.0f, 1.0f);
		}
		else
		{
			kColor= NiColorA(1.0f, 1.0f, 1.0f, 1.0f);
		}
		
		pkChar->HilightEdgObject(FALSE);
		pkChar->HilightObject(TRUE);

		pkLocalPlayer->SetTargetObjectID(uiTargetID);

		if (m_pkDecal)
		{
			m_pkDecal->SetVisible(pkLocalPlayer->GetTargetObjectID()?true:false);
			m_pkDecal->SetAttachObjectID(pkLocalPlayer->GetTargetObjectID());
			m_pkDecal->SetRadius(fRadius);
			m_pkDecal->SetColor(kColor);
		}

		//m_pkFlag->SetVisible(false);

		//if (pkLocalPlayer->GetTargetObjectID() && 
		//	uiTargetID != pkLocalPlayer->GetGUID() && 
		//	m_pkFlag)
		//{
			//m_pkFlag->SetVisible(true);
			//m_pkFlag->AttachToSceneObject(uiTargetID, 0, false, false);

			//NiTransform kOffset;
			//kOffset.MakeIdentity();

			//kOffset.m_fScale = 2.0f;
			//kOffset.m_Translate.z = pkChar->GetNameOffset() + 1.3f;
			//m_pkFlag->SetOffset(kOffset);

			//NiNode* pkNode = m_pkFlag->GetEffectNode();
			//if (pkNode)
			//{
			//	NiAVObject* pkAVObj = pkNode->GetObjectByName("Editable Poly");
			//	if (pkAVObj)
			//	{
			//		NiMaterialProperty* pkMat = (NiMaterialProperty*)pkAVObj->GetProperty(NiProperty::MATERIAL);
			//		if (pkMat)
			//			pkMat->SetEmittance(NiColor(kColor.r, kColor.g, kColor.b));
			//	}
			//}
		//}
	}

	m_TargetObjectID = uiTargetID;

	if (!uiTargetID || !pkChar)
	{
		//if (m_pkFlag)
		//{
		//	m_pkFlag->SetVisible(false);
		//	m_pkFlag->Detach();
		//}

		if (m_pkDecal)
		{
			m_pkDecal->SetVisible(false);
		}
	}
}

BOOL CPlayerInputMgr::SetSelectScenceOBJ(SYObjID uiScenceOBjID)
{
	UpdateObj* pkGameObject =ObjectMgr->GetObject(uiScenceOBjID) ;

	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)ObjectMgr->GetLocalPlayer();
	
	CSceneGameObj* pkScObj =static_cast<CSceneGameObj*>(pkGameObject); 
	if (pkScObj && pkScObj->GetGameObjectType() == GOT_SCENEOBJECT)
	{
		if (pkLocalPlayer->IsDead())
		{
			return TRUE ;
		}

		if( pkScObj->GetUInt32Value( GAMEOBJECT_DYN_FLAGS ) > 0 && pkScObj->GetUInt32Value( GAMEOBJECT_TYPE_ID ) == GAMEOBJECT_TYPE_PICK )
		{
			//uint32 entry = pkScObj->GetUInt32Value(GAMEOBJECT_DYN_FLAGS);
			//if( pkLocalPlayer->GetItemContainer()->GetItemCnt( entry ) > 0 )
			uint32 shapespell = 0;
			pkLocalPlayer->GetShapeSpell( shapespell );
			if( shapespell != 326 && shapespell != 327 )
			{
				SpellCastTargets targets ;
				targets.m_unitTarget = uiScenceOBjID;
				targets.m_targetMask |= TARGET_FLAG_UNIT;
				targets.m_itemTarget = 0;

				OnSkillAction(pkScObj, 120, targets);
				return true;
			}
		}
		else if( pkScObj->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_FLAGSTAND )
		{
			uint32 flag = pkScObj->GetUInt32Value( GAMEOBJECT_FLAGS );
			uint32 race = pkLocalPlayer->GetRace();
			if( (flag < 10 && race != flag)
				|| (flag > 10 && race != flag - flag/10*10) )
			{
				float fRadius = 1.0f;
				NiColorA kColor= NiColorA(0.0f, 1.0f, 0.0f, 1.0f);
				NiBound kBound;// = pkChar->GetSceneNode()->GetWorldBound();
				bool b = pkScObj->GetPickBound(kBound);
				NIASSERT(b);
				fRadius = kBound.GetRadius() * 0.5f;


				if (m_pkSelScenceObj)
				{
					m_pkSelScenceObj->SetVisible(uiScenceOBjID?true:false);
					m_pkSelScenceObj->SetAttachObjectID(uiScenceOBjID);
					m_pkSelScenceObj->SetRadius(fRadius);
					m_pkSelScenceObj->SetColor(kColor);
				}

				//发送采集
				SpellCastTargets targets ;
				targets.m_unitTarget = uiScenceOBjID;
				targets.m_targetMask |= TARGET_FLAG_UNIT;
				targets.m_itemTarget = 0;

				OnSkillAction(pkScObj, 5,targets);
				return TRUE ;
			}
		}
		else if( pkScObj->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_CHEST || pkScObj->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_MAILBOX )
		{
			float fRadius = 1.0f;
			NiColorA kColor= NiColorA(0.0f, 1.0f, 0.0f, 1.0f);
			NiBound kBound;// = pkChar->GetSceneNode()->GetWorldBound();
			bool b = pkScObj->GetPickBound(kBound);
			NIASSERT(b);
			fRadius = kBound.GetRadius() * 0.5f;


			if (m_pkSelScenceObj)
			{
				m_pkSelScenceObj->SetVisible(uiScenceOBjID?true:false);
				m_pkSelScenceObj->SetAttachObjectID(uiScenceOBjID);
				m_pkSelScenceObj->SetRadius(fRadius);
				m_pkSelScenceObj->SetColor(kColor);
			}

			//发送采集
			SpellCastTargets targets ;
			targets.m_unitTarget = uiScenceOBjID;
			targets.m_targetMask |= TARGET_FLAG_UNIT;
			targets.m_itemTarget = 0;

			OnSkillAction(pkScObj, pkScObj->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_MAILBOX ? 86 : 6,targets);
			return TRUE ;
		}
		else if (pkScObj->IsGatherObj())
		{
			if (pkScObj->CanSeeCollection())
			{
				int cantcollect = pkScObj->CanbeCollected();
				switch ( cantcollect )
				{
				case 1:
					{
						EffectMgr->AddEffectText(_TRAN("采草技能等级不够"));
						return TRUE;
					}
				case 2:
					{
						EffectMgr->AddEffectText(_TRAN("需要采集工具"));
						return TRUE;
					}
				case 3:
					{
						EffectMgr->AddEffectText(_TRAN("采矿技能不够"));
						return TRUE;
					}
				default:
					break;
				}

				//通知服务器切换对象到0.
				PacketBuilder->SendSetSelectionMsg(INVALID_OBJECTID, FALSE);
				//if(pkLocalPlayer)
				//	pkLocalPlayer->SetTargetObjectID(0);
				//if (TeamShow)
				//{
				//	TeamShow->SetTargetGUID(0);
				//}

				//然后发送采集技能

				float fRadius = 1.0f;
				NiColorA kColor= NiColorA(0.0f, 1.0f, 0.0f, 1.0f);
				NiBound kBound;// = pkChar->GetSceneNode()->GetWorldBound();
				bool b = pkScObj->GetPickBound(kBound);
				NIASSERT(b);
				fRadius = kBound.GetRadius() * 0.5f;


				if (m_pkSelScenceObj)
				{
					m_pkSelScenceObj->SetVisible(uiScenceOBjID?true:false);
					m_pkSelScenceObj->SetAttachObjectID(uiScenceOBjID);
					m_pkSelScenceObj->SetRadius(fRadius);
					m_pkSelScenceObj->SetColor(kColor);
				}

				//发送采集
				SpellCastTargets targets ;
				targets.m_unitTarget = uiScenceOBjID;
				targets.m_targetMask |= TARGET_FLAG_UNIT;
				targets.m_itemTarget = 0;

				uint32 reqSpell = pkScObj->GetReqSpell();
				if ( !reqSpell )
				{
					reqSpell = 4;
				}
				OnSkillAction(pkScObj, reqSpell,targets);
			}
		}
		
		return TRUE;
	}else
	{
		m_pkSelScenceObj->SetVisible(false);
		return FALSE;
	}
}
// 通知服务器无效ID为0
void CPlayerInputMgr::SetTargetID(SYObjID uiTargetID, bool bAttack)
{
	if (SetSelectScenceOBJ(uiTargetID))
	{
		return ;
	}
	UpdateObj* pkGameObject =ObjectMgr->GetObject(uiTargetID) ;
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)ObjectMgr->GetLocalPlayer();
	if (m_TargetObjectID != uiTargetID)
	{
		CCharacter* pkObject = static_cast<CCharacter*>(pkGameObject);
		if (pkObject == NULL)
		{
			if (uiTargetID == INVALID_OBJECTID)
			{
				if (m_pkDecal)
				{
					m_pkDecal->SetVisible(false);
				}
				if (m_pkSelectLocation)
				{
					m_pkSelectLocation->SetVisible(false);
				}
				TeamShow->SetTargetGUID(uiTargetID);
				SetDecal(uiTargetID);
			}
			else
			{
				if(!pkObject && SocialitySys->GetTeamSysPtr()->InTeam(uiTargetID))
				{
					TeamShow->SetTargetGUID(uiTargetID);
					SetDecal(INVALID_OBJECTID);
				}
				m_TargetObjectID = uiTargetID;
				if(pkLocalPlayer)
					pkLocalPlayer->SetTargetObjectID(uiTargetID);
				return;
			}
		}
		if(uiTargetID != TeamShow->GetTargetID())
			TeamShow->SetTargetGUID(uiTargetID);
		SetDecal(uiTargetID);
		//m_TargetObjectID = uiTargetID;
	}

	if (m_SwingId != 0 && m_SwingId != uiTargetID)
	{
		m_SwingId = 0;
	}
	//没等消息的设置
	

	// 目标切换
	PacketBuilder->SendSetSelectionMsg(m_TargetObjectID, bAttack);
	if(pkLocalPlayer)
		pkLocalPlayer->SetTargetObjectID(uiTargetID);
}

void CPlayerInputMgr::SetSwingTargetID(SYObjID uiId)
{
	m_SwingId = uiId;
}

void CPlayerInputMgr::AutoCancelProcess()
{
	if (IsMoveLocked())
		return;

	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLocalPlayer != NULL);
	if (!pkLocalPlayer)
		return;

	if (pkLocalPlayer->IsPathMove())
	{
		pkLocalPlayer->PathMoveEnd();
	}

	pkLocalPlayer->FollowPlayer(0);

	if (pkLocalPlayer->m_bAutoRun)
	{
		pkLocalPlayer->StopMove();
		pkLocalPlayer->m_bAutoRun = false;
	}

	//EndSelectTarget();
}

static bool CanSelect(const NiPoint3& kEyePos, CCharacter* pkCreature)
{
    const NiBound& kBound = pkCreature->GetSceneNode()->GetWorldBound();
    CMap* pkMap = CMap::Get();
    if(pkMap)
    {
        //中间不能有阻挡
        float f;
        return !pkMap->CollideLine(kEyePos, kBound.GetCenter(), f);
    }

    return false;
}
SYObjID CPlayerInputMgr::HookAutoSelectCreature(ui64  guid, BOOL selDeadOBJ)
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)ObjectMgr->GetLocalPlayer();

	std::map<float,CCharacter*> vCanSelList;

	stdext::hash_set<UpdateObj*> temp( ObjectMgr->GetObjectByType(TYPEID_UNIT) );

	NiPoint3 kEyePos = pkLocalPlayer->GetLocalPlayerPos();
	kEyePos.z += Eye_OffSet / 2;

	for( stdext::hash_set<UpdateObj*>::iterator it = temp.begin(); it != temp.end(); ++it )
	{
		CCharacter* pkCreature = static_cast<CCharacter*>( *it );

		if(pkCreature == NULL)
			continue;
		if (m_TargetObjectID == pkCreature->GetGUID())
			continue;

		const NiBound& kBound = pkCreature->GetSceneNode()->GetWorldBound();
		NiPoint3 dist = kEyePos - kBound.GetCenter();
		float len = dist.SqrLength();
		if(len > 50.0f * 50.0f)
			continue;

		if (pkCreature->GetGUID() == guid)
		{
			continue ;
		}

		if (!pkCreature->IsFriendly(pkLocalPlayer) && pkCreature != pkLocalPlayer && pkCreature->GetGameObjectType() == GOT_CREATURE)
		{
			if (!selDeadOBJ && pkCreature->IsDead())
			{
			}else
			{
				if (pkCreature->IsDead() && !pkCreature->HasFlag(UNIT_DYNAMIC_FLAGS, U_DYN_FLAG_LOOTABLE))
				{
					
				}else
				{
					vCanSelList.insert(std::map<float,CCharacter*>::value_type(len, pkCreature));
				}
			}
		}
	}
	if (vCanSelList.size() == 0)
	{
		SetTargetID(INVALID_OBJECTID);
		return INVALID_OBJECTID;
	}
	std::map<float,CCharacter*>::iterator it = vCanSelList.begin();
	while (it != vCanSelList.end())
	{
		if (it->second && CanSelect(kEyePos, it->second))
		{
			Action_ClickTarget(it->second);
			it->second->SetSelState(TRUE);
			ULOG("HOOK 找到目标");
			return it->second->GetGUID();
			
		}
		++it;
	}
	SetTargetID(INVALID_OBJECTID);
	return INVALID_OBJECTID;
}

SYObjID CPlayerInputMgr::AutoSelectCreature()
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)ObjectMgr->GetLocalPlayer();

	std::map<float,CCharacter*> vCanSelList;

	stdext::hash_set<UpdateObj*> temp( ObjectMgr->GetObjectByType(TYPEID_UNIT) );
	const stdext::hash_set<UpdateObj*>& temp2( ObjectMgr->GetObjectByType(TYPEID_PLAYER) );
	for( stdext::hash_set<UpdateObj*>::const_iterator it = temp2.begin(); it != temp2.end(); ++it )
		temp.insert( *it );

    NiPoint3 kEyePos = pkLocalPlayer->GetLocalPlayerPos();
    kEyePos.z += Eye_OffSet;

	CCamera& ViewCam = pkLocalPlayer->GetCamera();
	ViewCam.UpdateFrustumPlanes();

	for( stdext::hash_set<UpdateObj*>::iterator it = temp.begin(); it != temp.end(); ++it )
	{
		CCharacter* pkCreature = static_cast<CCharacter*>( *it );

		if(pkCreature == NULL)
			continue;
		if (m_TargetObjectID == pkCreature->GetGUID())
			continue;

		if (pkCreature->IsDead())
			continue;

        const NiBound& kBound = pkCreature->GetSceneNode()->GetWorldBound();
        NiPoint3 dist = kEyePos - kBound.GetCenter();
        float len = dist.SqrLength();
        if(len > 40.0f * 40.0f)
            continue;
		if(!ViewCam.TestVisible(kBound))
			continue;

		if (!isInFront(pkLocalPlayer, pkCreature))
			continue;

		if (!pkCreature->IsFriendly(pkLocalPlayer) && pkCreature != pkLocalPlayer)
		{
			vCanSelList.insert(std::map<float,CCharacter*>::value_type(len, pkCreature));
		}
		else
		{
			pkCreature->SetSelState(FALSE);
		}
	}
	if (vCanSelList.size() == 0)
	{
		return 0;
	}
	std::map<float,CCharacter*>::iterator it2 = vCanSelList.begin();
	while (it2 != vCanSelList.end())
	{
		if (it2->second && !it2->second->GetSelState() && CanSelect(kEyePos, it2->second))
		{
			Action_ClickTarget(it2->second);
			it2->second->SetSelState(TRUE);
			return it2->second->GetGUID();
		}
		++it2;
	}

	for( stdext::hash_set<UpdateObj*>::const_iterator it3 = temp.begin(); it3 != temp.end(); ++it3 )
	{
		CCharacter* pkCreature = static_cast<CCharacter*>( *it3 );

		if(pkCreature == NULL)
			continue;
		pkCreature->SetSelState(FALSE);

	}
	std::map<float,CCharacter*>::iterator itTemp = vCanSelList.begin();
	if(itTemp != vCanSelList.end() && CanSelect(kEyePos, itTemp->second))
	{
		Action_ClickTarget(itTemp->second);
		itTemp->second->SetSelState(TRUE);
		return itTemp->second->GetGUID();
	}
	return INVALID_OBJECTID;
}

SYObjID CPlayerInputMgr::GetTargetID()
{
	return m_TargetObjectID;
}

bool g_bHiddenPlayer = false; //是否隐藏其他玩家

BOOL CPlayerInputMgr::KeyDown(UINT nKeyCode)
{
	CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
	if(pkPlayer == NULL)
		return FALSE;

	BOOL isHook = pkPlayer->IsHook() ;

	// reset key state
	for (unsigned int ui = 0; ui < 9; ui++)
	{
		if (m_movekeys[ui].Compare(nKeyCode))
		{
			m_movekeys[ui].SetState(UNIInputKey::DOWN);
		}
	}

	if (nKeyCode == UILK_2_NIINPUTKEY(LK_G) && !isHook && !IsForceRoot() )
	{
		if (!pkPlayer->m_bAutoRun)
		{
			pkPlayer->StopMove();
			pkPlayer->StartForward(true);
			pkPlayer->MoveTo();
		}
		else
		{
			AutoCancelProcess();
			pkPlayer->m_bAutoRun = false;
			pkPlayer->StopVertical();
		}
	}

    if(nKeyCode == UILK_2_NIINPUTKEY(LK_F12))
        g_bHiddenPlayer = !g_bHiddenPlayer;

    if(nKeyCode == UILK_2_NIINPUTKEY(LK_SYSRQ))
        EnableScreenshot();

	if (!isHook && nKeyCode == UILK_2_NIINPUTKEY(LK_TAB))
	{
		AutoSelectCreature();
	}

    if (nKeyCode == UILK_2_NIINPUTKEY(LK_RETURN))
    {
        ClientState->SetFullScreen(!ClientState->GetFullScreen());
        ClientState->SaveLogin();

        SYState()->ClientApp->RecreateRender();
    }

	if(nKeyCode == UILK_2_NIINPUTKEY(LK_F11))
	{
		HookMgr->SetHook();
	}
	return FALSE;
}

void CPlayerInputMgr::CommitMoveEvt()
{
}

BOOL CPlayerInputMgr::KeyUp(UINT nKeyCode)
{
	NiInputSystem* pkInput = SYState()->Input;
	NiInputKeyboard* pkKB = pkInput->GetKeyboard();

	// reset key state
	for (unsigned int ui = 0; ui < 9; ui++)
	{
		if (m_movekeys[ui].Compare(nKeyCode))
		{
			m_movekeys[ui].SetState(UNIInputKey::UP);
		}
	}

	return FALSE;
}

BOOL CPlayerInputMgr::OnEscape()
{
	if (IsSelectingTarget())
	{
		EndSelectTarget();
		return TRUE;
	}

	return FALSE;
}
