#include "StdAfx.h"
#include "PlayerTradeContainer.h"
#include "ItemSlot.h"

CPlayerTradeContainer::CPlayerTradeContainer(void)
{
}

CPlayerTradeContainer::~CPlayerTradeContainer(void)
{
}

//void CPlayerTradeContainer::InsertSlot(BYTE AtPos, class CItemSlot* pkItemSlot)
bool CPlayerTradeContainer::InsertSlot(BYTE AtPos, CSlot& kSlot)
{
	NIASSERT(AtPos < GetMaxSlotNum());
	NIASSERT(IsEmpty(AtPos));

	if(AtPos >= GetMaxSlotNum())		
		return false;

	if(!IsEmpty(AtPos))				
		return false;

	m_ppSlotArray[AtPos]->Copy(kSlot);
	++m_nSlotNum;
	NIASSERT(m_nSlotNum <= GetMaxSlotNum());

	return true;
}