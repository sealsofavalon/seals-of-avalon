#ifndef PLAYERTRADECONTAINER_H
#define PLAYERTRADECONTAINER_H

#include "ItemSlotContainer.h"

class CPlayerTradeContainer : public CItemSlotContainer
{
public:
	CPlayerTradeContainer(void);
	~CPlayerTradeContainer(void);

	//virtual void InsertSlot(BYTE AtPos, class CItemSlot* pkItemSlot);
	virtual bool InsertSlot(BYTE AtPos, CSlot& rSlot);
};


#endif