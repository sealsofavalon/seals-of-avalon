#ifndef _POS_STRAINT_EFFECT_H_
#define _POS_STRAINT_EFFECT_H_

#include "Effect/EffectBase.h"

class CCharacter;
class PosConstraintEffect : public CEffectBase
{
public :
	PosConstraintEffect(CCharacter* pkObject);
	virtual ~PosConstraintEffect();

	// constraint target
	void SetTarget(ui64 charid);
	bool Rush(NiPoint3& src, NiPoint3& dest, float speed);
	virtual void Update(float fTimeLine, float fDeltaTime);
	bool Teleport(float minrange, float maxrange);
	void UpdateEffect(float fTimeLine, float fDeltaTime);
	void EndUpdate();

private:
	NiPoint3 m_srcPos;
	NiPoint3 m_destPos;
	float m_fSpeed;
	CCharacter* m_pkObj;
	int m_iSaveState;
	float m_fTotalT;
	ui64 m_uiCharID;

	int m_lockcount;
	bool setidle;
	bool pathmove;
	bool setpos;
	bool constraintstate;
};


#endif //_POS_STRAINT_EFFECT_H_