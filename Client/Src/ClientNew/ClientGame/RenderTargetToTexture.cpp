#include "StdAfx.h"
#include "RenderTargetToTexture.h"

NiRenderedTexture* CRT2Tex::ms_pkTexture;
NiRenderTargetGroup* CRT2Tex::ms_pkTarget;
NiCamera CRT2Tex::ms_pkCamera;
NiRenderTargetGroup* CRT2Tex::m_pkCurRenderTarget;
NiVisibleArray* CRT2Tex::ms_pkVisibleArray;

CRT2Tex::CRT2Tex()
{
}

CRT2Tex::~CRT2Tex()
{
}

bool CRT2Tex::SM_Init(NiRenderer* pkRenderer, uint32 uiWidth, uint32 uiHeight)
{
	NiTexture::FormatPrefs kPrefs;
	kPrefs.m_ePixelLayout = NiTexture::FormatPrefs::TRUE_COLOR_32;
	kPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
	kPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::NONE;

	ms_pkTexture = NiRenderedTexture::Create(uiWidth, uiHeight, pkRenderer, kPrefs);
	ms_pkTexture->IncRefCount();

	ms_pkTarget = NiRenderTargetGroup::Create(ms_pkTexture->GetBuffer(), pkRenderer, true, true);
	ms_pkTarget->IncRefCount();

    ms_pkVisibleArray = NiNew NiVisibleArray(32, 16);

	return true;
}

bool CRT2Tex::SM_Destory()
{
	if(ms_pkTexture)
	{
		ms_pkTexture->DecRefCount();
		ms_pkTexture = NULL;
	}

	if(ms_pkTarget)
	{
		ms_pkTarget->DecRefCount();
		ms_pkTarget = NULL;
	}

    NiDelete ms_pkVisibleArray;

	return true;
}
// 
// void CRT2Tex::SetTextureSize(NiRenderer* pkRenderer, uint32 uiWidth, uint32 uiHeight)
// {
// 	NiTexture::FormatPrefs kPrefs;
// 	kPrefs.m_ePixelLayout = NiTexture::FormatPrefs::TRUE_COLOR_32;
// 	kPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
// 	kPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::SMOOTH;
// 
// 	if(ms_pkTexture)
// 		NiDelete ms_pkTexture;
// 	ms_pkTexture = NiRenderedTexture::Create(uiWidth, uiHeight, pkRenderer, kPrefs);
// 	ms_pkTexture->IncRefCount();
// }

bool CRT2Tex::BeginRT2Tex(NiAVObject* pObj)
{
	NiD3DRenderer* pkRenderer = (NiD3DRenderer*)NiRenderer::GetRenderer();
	NiD3DRenderState* pkRenderState = pkRenderer->GetRenderState();
	pkRenderer->BeginFrame();
	
	// Backup current render target group.
	m_pkCurRenderTarget = (NiRenderTargetGroup*)pkRenderer->GetCurrentRenderTargetGroup();
	if(!m_pkCurRenderTarget)
	{
		m_pkCurRenderTarget = (NiRenderTargetGroup*)pkRenderer->GetDefaultRenderTargetGroup();
		if(!m_pkCurRenderTarget)
			return false;
	}
	pkRenderer->EndUsingRenderTargetGroup();

	//-------------------------------------------------------------------------------------------------------------------------
	pkRenderer->BeginUsingRenderTargetGroup(ms_pkTarget, NiRenderer::CLEAR_ALL);
	pkRenderer->SetCameraData(&ms_pkCamera);

	//render
	ms_pkVisibleArray->RemoveAll();
	NiCullingProcess kCuller(ms_pkVisibleArray);
	NiDrawScene(&ms_pkCamera, pObj, kCuller);

	Ni2DBuffer* pkRefractBuffer = ms_pkTexture->GetBuffer();
	pkRefractBuffer->GetRendererData();
	pkRefractBuffer->GetPixelFormat();
	return true;
}

void CRT2Tex::EndRT2Tex(const NiCamera* oldcamera)
{
	NiD3DRenderer* pkRenderer = (NiD3DRenderer*)NiRenderer::GetRenderer();
	NiD3DRenderState* pkRenderState = pkRenderer->GetRenderState();

	pkRenderer->EndUsingRenderTargetGroup();

	pkRenderer->BeginUsingRenderTargetGroup((NiRenderTargetGroup *)m_pkCurRenderTarget, NiRenderer::CLEAR_NONE);

	// Set up the renderer's camera data.
	pkRenderer->SetCameraData(oldcamera);
	pkRenderer->EndFrame();
}