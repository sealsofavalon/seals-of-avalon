#ifndef CRT2TXT_H
#define CRT2TXT_H

class CRT2Tex : public NiMemObject
{
public:
	CRT2Tex();
	~CRT2Tex();

	static bool SM_Init(NiRenderer* pkRenderer, uint32 uiWidth, uint32 uiHeight);
	static bool SM_Destory();
	//static void SetTextureSize(NiRenderer* pkRenderer, uint32 uiWidth, uint32 uiHeight);

	static bool BeginRT2Tex(NiAVObject* pObj);
	static void EndRT2Tex(const NiCamera* oldcamera);

	static NiRenderedTexture* GetTexture() { return ms_pkTexture; }
	static NiRenderTargetGroup* GetTarget() { return ms_pkTarget; }
	static NiCamera& GetCamera() { return ms_pkCamera; }
protected:
	static NiRenderedTexture* ms_pkTexture;
	static NiRenderTargetGroup* ms_pkTarget;
	static NiCamera ms_pkCamera;

	static D3DBaseTexturePtr ms_pkD3DTexture;

	static NiRenderTargetGroup* m_pkCurRenderTarget;
    static NiVisibleArray* ms_pkVisibleArray;
};

#endif