#pragma once

struct ResNode : public NiMemObject
{
    int m_iRefCount;
    NiRefObject* m_pkResource;

    ResNode(NiRefObject* pkRes)
        :m_pkResource(pkRes)
        ,m_iRefCount(0)
    {
        m_pkResource->IncRefCount();
    }

    virtual ~ResNode()
    {
        m_pkResource->DecRefCount();
    }
};

struct MfResNode : public ResNode
{
    const char* m_aTextures[8];
    const char* m_aNifs[8];

    MfResNode(NiRefObject* pkRes)
        :ResNode(pkRes)
    {
        memset(m_aTextures, 0, sizeof(m_aTextures));
        memset(m_aNifs, 0, sizeof(m_aNifs));
    }
};

bool IsMfFile(const NiFixedString& kName);
NiGlobalStringTable::GlobalStringHandle MakeStandardName(const char* src);
void CoverMaterial(NiAVObject* pNode);



class CFixedString : public NiFixedString
{
public:
	CFixedString()
		:NiFixedString()
	{}

	CFixedString(const char* pcString)
		:NiFixedString(pcString)
	{}

	CFixedString(NiGlobalStringTable::GlobalStringHandle kHandle)
	{
		m_kHandle = kHandle;
	}

	operator int() const { return (int)NiGlobalStringTable::GetString(m_kHandle); }
};

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))


class CResMgr : public NiMemObject
{
	typedef stdext::hash_map<int,  ResNode*> ResourceMap;
	struct Loader
	{
		NiCriticalSection m_kCS;
		ResourceMap m_kResMap;
		NiStream m_kStream;
	};

public:
	CResMgr();
	virtual ~CResMgr();

	bool Initialize();
	void Destroy();

	void AddResData(NiAVObject* pkAVObject, const NiFixedString& kFilename);
	const NiFixedString* GetResData(NiAVObject* pkAVObject);

	//Nif
	NiAVObject* LoadNif(const NiFixedString& kFilename, bool bCopy = true);
	ResNode* LoadNifResNode(const CFixedString& kFilename);
    ResNode* LoadMfResNode(const CFixedString& kFilename);
	void FreeNif(NiAVObject* pkAVObject); 
	void FreeNif(const NiFixedString& kFilename);
    void FreeMf(const NiFixedString& kFilename);
	void DumpNif();
    void DumpMf();

	//��ɫģ��
	NiActorManager* LoadKfm(const NiFixedString& kFilename, bool bCopy = true);
	ResNode* LoadKfmResNode(const CFixedString& kFilename);
	void FreeKfm(NiActorManager* pkActorManager);
	void FreeKfm(const NiFixedString& kFilename);
	void DumpKfm();

	//��ͼ
	NiTexture* LoadTexture(const NiFixedString& kFilename);
	ResNode* LoadTextureResNode(const CFixedString& kFilename);
	void FreeTexture(NiTexture* pkTexture);
	void FreeTexture(const NiFixedString& kFilename);
	void DumpTexture();

	void ResetTexturePalette();

	static CResMgr* Get() { return ms_pkResMgr; }

protected:
	Loader m_kNifLoader;
    Loader m_kMfLoader;
	Loader m_kKfmLoader;
	Loader m_kTextureLoader;

	ResNode* m_pkNullNif;
	ResNode* m_pkNullKfm;
	ResNode* m_pkNullTexture;

	NiFixedString m_kExtraDataName;

	static CResMgr* ms_pkResMgr;
};

#define g_ResMgr (CResMgr::Get())