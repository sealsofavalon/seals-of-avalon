#include "StdAfx.h"
#include "SYItem.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "UI/UISystem.h"
#include "ui/UIGamePlay.h"
#include "ui/UIPackage.h"
#include "UI/UIBank.h"
#include "Utils/QuestFinisherDB.h"
#include "Utils/QuestDB.h"
#include "ui/UEquipmentWear.h"
#include "ui/UEquRefineManager.h"
#include "ui/UChat.h"
#include "Utils/AreaDB.h"
#include "ui/uiSkill.h"
#include "ui/UInstanceMgr.h"

SYItem::SYItem()
{
	m_valuesCount = ITEM_END;
	_InitValues();
	m_pkItemProperty = NULL;
}

SYItem::~SYItem()
{
	delete[] m_uint32Values;
	m_uint32Values = NULL;
}

void SYItem::OnValueChanged(class UpdateMask* mask)
{
	UpdateObj::OnValueChanged(mask);

	if( !ObjectMgr->GetLocalPlayer() )
	{
		return;
	}


	UInGame* pInGame =(UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	UPackage * pPackage = NULL;
	UBank* pBank = NULL;
	if (pInGame)
	{
		pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
		pBank = INGAMEGETFRAME(UBank,FRAME_IG_BANKDLG);
	}

	if( mask->GetBit(ITEM_FIELD_STACK_COUNT) )
	{//	叠加数量
		//SYState()->UI->GetItemSystem()->SetItemIcon(ICT_BAG, GetSlotIdx()+INVENTORY_SLOT_ITEM_START, this);
		//ui32 nCount = GetUInt32Value( ITEM_FIELD_STACK_COUNT );

		if (pPackage)
		{
			pPackage->RefurbishPag();
		}

		if (pBank)
		{
			pBank->RefurbishBank();
		}

		if (EquRefineMgr)
		{
			EquRefineMgr->ShowDefaultMaterials(GetGUID());
		}
		

		UIManufacture* pManuFacture = INGAMEGETFRAME(UIManufacture, FRAME_IG_MANUFACTURE);
		if (pManuFacture)
		{
			pManuFacture->UpdateVisibleList();
			pManuFacture->UpdateManuFacture();
		}
		USpecialityDlg* pSpecialityDlg = INGAMEGETFRAME(USpecialityDlg, FRAME_IG_SPECIALITYDLG);
		if (pSpecialityDlg)
		{
			pSpecialityDlg->SetCurList();
		}
		InstanceSys->OnUpdateFairGroundIcon();
	}

	if( mask->GetBit(ITEM_FIELD_CONTAINED) )
	{//道具所在的容器改变了
		if( GetUInt64Value(ITEM_FIELD_OWNER ) != GetUInt64Value(ITEM_FIELD_CONTAINED ))
		{

		}
	}
	bool bUpdateSocketGemList = false;
	if (mask->GetBit(ITEM_FIELD_ENCHANTMENT + 2 * 3))
	{
		bUpdateSocketGemList  = true;
	}
	if (mask->GetBit(ITEM_FIELD_ENCHANTMENT + 3 * 3))
	{
		bUpdateSocketGemList  = true;
	}
	if (mask->GetBit(ITEM_FIELD_ENCHANTMENT + 4 * 3))
	{
		bUpdateSocketGemList  = true;
	}
	if (bUpdateSocketGemList)
		EquRefineMgr->UpdateSocketGemList(GetGUID());
	
	if (mask->GetBit(ITEM_FIELD_JINGLIAN_LEVEL))
	{
		if (EquRefineMgr)
		{
			EquRefineMgr->ShowDefaultMaterials(GetGUID());
		}
	}
	if( mask->GetBit(ITEM_FIELD_DURATION) )
	{//持续时间

	}

	if( mask->GetBit(ITEM_FIELD_DURABILITY) )
	{//耐久度
		UEquipmentWear* pkEuipWear = INGAMEGETFRAME(UEquipmentWear,FRAME_IG_EQUIPWEAR);
		if (pkEuipWear)
		{
			pkEuipWear->UpdataEquipment(GetGUID());
		}
	}
	if( mask->GetBit(ITEM_FIELD_MAXDURABILITY) )
	{//最大耐久度

	}

	//宠物蛋
	if(mask->GetBit(ITEM_FIELD_GENERATE_TIME))
	{
		int i;
		ui32 Time = GetUInt32Value(ITEM_FIELD_GENERATE_TIME);
		i = 1;
	}
	if(mask->GetBit(ITEM_FIELD_RECORD_ZONEID))
	{

	}
	if(mask->GetBit(ITEM_FIELD_USE_CNT))
	{
		ui32 count = GetUInt32Value(ITEM_FIELD_USE_CNT);
	}
	if(mask->GetBit(ITEM_FIELD_CREATOR))
	{
		ui64 Creator = GetUInt64Value(ITEM_FIELD_USE_CNT);
	}
}

void SYItem::OnCreate(ByteBuffer* data, ui8 update_flags)
{
	m_pkItemProperty = ItemMgr->GetItemPropertyFromDataBase(GetUInt32Value(OBJECT_FIELD_ENTRY));
}

void SYItem::OnQuestQuery()
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if(!pkLocalPlayer)
		return;

	if(*(ui64*)&m_uint32Values[ITEM_FIELD_OWNER] != pkLocalPlayer->GetGUID())
	{
		return;
	}
	//	任务日志
	for(ui32 i = 0; i < 25; ++i)
	{
		ui32 questslot = PLAYER_QUEST_LOG_1_1 + i * MAX_QUEST_OFFSET + QUEST_ID_OFFSET;
		ui32 questid = pkLocalPlayer->GetUInt32Value(questslot+QUEST_ID_OFFSET);

		if(questid)
		{
			CQuestDB::stQuest* pQuest = g_pkQusetInfo->GetQuestInfo(questid);
			if(pQuest)
			{
				for(int j = 0; j < 4; j++)
				{
					if( pQuest->uiReqItemId[j] == GetUInt32Value(OBJECT_FIELD_ENTRY) )
					{
						if( pkLocalPlayer->GetItemContainer()->GetItemCnt(GetUInt32Value(OBJECT_FIELD_ENTRY)) == pQuest->uiReqItemCount[j] )
						{
							ui32 npcid;
							if(g_pkQuestFinisherDB->GetNpcId(questid, npcid))
							{
								CCreature* pkCreature = (CCreature*)ObjectMgr->GetObjectByEntry(TYPE_UNIT, npcid);
								if(pkCreature == NULL)
									return;
								pkCreature->QueryQuestStatus();
							}
						}
					}
				}

			}

		}
	}	
}
void SYItem::PostOnCreate()
{
}


//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

//void SYItem::SetSoltBag(UINT SlotBag)
//{
//	//if (SlotBag >= INVENTORY_SLOT_BAG_START && SlotBag < INVENTORY_SLOT_BAG_END )
//	{
//		m_Slot_Bag_index = SlotBag ;
//	}
//}
//void SYItem::SetSoltPos(UINT SlotPos)
//{
//	m_Slot_Bag_Pos = SlotPos;
//}
//
//UINT SYItem::GetSoltBag()
//{
//	return m_Slot_Bag_index;
//}
//UINT SYItem::GetSoltPos()
//{
//	return m_Slot_Bag_Pos;
//}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

SYItemBag::SYItemBag()
{
	m_valuesCount = CONTAINER_END;
	m_uint32Values = 0;
	_InitValues();

	m_SlotCount = 0 ;
	//m_ItemContainerStartPos = 0 ;
}
SYItemBag::~SYItemBag()
{
}
void SYItemBag::OnValueChanged(class UpdateMask* mask)
{
	SYItem::OnValueChanged(mask);

	//RefushSlortBag();

	for(unsigned int pos = 0; pos < GetUInt32Value(CONTAINER_FIELD_NUM_SLOTS); pos++)
	{
		ui64 guid = GetUInt64Value(CONTAINER_FIELD_SLOT_1 + pos*2);
		if (mask->GetBit(CONTAINER_FIELD_SLOT_1 + pos*2))
		{
			SYItem* pItem = (SYItem*)ObjectMgr->GetObject(guid);
			if (m_BagSlot >= INVENTORY_SLOT_BAG_START && m_BagSlot < INVENTORY_SLOT_BAG_END)
			{
				ItemMgr->OnItemChange(m_BagSlot, pos , guid);
			}

			if (m_BagSlot >= BANK_SLOT_BAG_START && m_BagSlot < BANK_SLOT_BAG_END)
			{
				ItemMgr->OnBankItemChange(m_BagSlot,pos,guid);
			}
		}	
	}
}

void SYItemBag::CopyToStorage()
{
	for(unsigned int pos = 0; pos < GetUInt32Value(CONTAINER_FIELD_NUM_SLOTS); pos++)
	{
		ui64 guid = GetUInt64Value(CONTAINER_FIELD_SLOT_1 + pos*2);
		
		SYItem* pItem = (SYItem*)ObjectMgr->GetObject(guid);
		if (m_BagSlot >= INVENTORY_SLOT_BAG_START && m_BagSlot < INVENTORY_SLOT_BAG_END)
		{
			ItemMgr->OnItemChange(m_BagSlot, pos , guid);
		}

		if (m_BagSlot >= BANK_SLOT_BAG_START && m_BagSlot < BANK_SLOT_BAG_END)
		{
			ItemMgr->OnBankItemChange(m_BagSlot,pos,guid);
		}
				
	}
}
//void SYItemBag::SetBagSlortAndPos(UINT index, UINT pos)
//{
//	m_ItemContainerStartPos = pos;
//	m_BagSlot = index;
//	RefushSlortBag();
//}
//UINT SYItemBag::GetStartPos()
//{
//	return m_ItemContainerStartPos;
//}
void SYItemBag::OnCreate(ByteBuffer *data, ui8 update_flags)
{
	SYItem::OnCreate(data,update_flags);

	if (m_pkItemProperty)
	{
		if (m_pkItemProperty->ContainerSlots != 0xff)
		{
			m_SlotCount = m_pkItemProperty->ContainerSlots;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
