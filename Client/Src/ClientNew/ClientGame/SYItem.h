#ifndef SYITEM_H
#define SYITEM_H

#include "UpdateObject.h"
#include "Utils/ItemDB.h"

class SYItem : public UpdateObj
{
public:
	SYItem();
	virtual ~SYItem();

	virtual void OnValueChanged(class UpdateMask* mask);
	virtual void OnCreate(class ByteBuffer* data, ui8 update_flags);
	virtual void PostOnCreate();
	void OnQuestQuery();

	virtual UINT GetNum() const { return GetUInt32Value(ITEM_FIELD_STACK_COUNT); }
	virtual void SetNum(UINT num) { SetUInt32Value(ITEM_FIELD_STACK_COUNT, num); }
	virtual UINT GetDurability()const{return GetUInt32Value(ITEM_FIELD_DURABILITY);}
	virtual UINT GetMaxDurability()const{return GetUInt32Value(ITEM_FIELD_MAXDURABILITY);}
	virtual bool IsMapItem() const { return false; }

	/*void SetSoltBag(UINT SlotBag);
	void SetSoltPos(UINT SlotPos);

	UINT GetSoltBag();
	UINT GetSoltPos();*/

	virtual ui32 GetCode() const { return GetUInt32Value(OBJECT_FIELD_ENTRY); }

	ItemPrototype_Client* GetItemProperty() 
	{ 
		if(m_pkItemProperty) 
			return m_pkItemProperty;
		else 
			return NULL;
	}
protected:
	ItemPrototype_Client* m_pkItemProperty;
	////标记哪个包裹,那个位置来对应ItemSoltContainer 里面的Solt的pos,来确定在UI界面的显示位置.
	//UINT m_Slot_Bag_index ; //道具所在的包裹的ID
	//UINT m_Slot_Bag_Pos;    //道具所在包裹的位置

};

//包裹信息,bag_0 bag_1 bag_2 bag_3.
class SYItemBag : public SYItem
{
public:
	SYItemBag();
	virtual ~SYItemBag();

	virtual void OnValueChanged(class UpdateMask* mask);
	virtual void OnCreate(class ByteBuffer* data, ui8 update_flags);

	//void SetBagSlortAndPos(UINT index, UINT pos);
	//UINT GetStartPos();
	UINT GetSlotCount(){return m_SlotCount;}
	UINT GetBagSlot(){return m_BagSlot;}
	void SetBagSlot(UINT index){m_BagSlot = index;}

	void CopyToStorage();
protected:
	UINT m_SlotCount;   //道具包裹里面的格子数目
	//UINT m_ItemContainerStartPos;   //道具包裹在客户端vector里开始的位置
	UINT m_BagSlot; //道具包裹的标示,用于包裹内的道具
};

#endif