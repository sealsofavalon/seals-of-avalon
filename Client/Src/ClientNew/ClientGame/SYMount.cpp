#include "StdAfx.h"
#include "SYMount.h"
#include "ResourceManager.h"
#include "ItemManager.h"
#include "ClientApp.h"

#include "AudioInterface.h"
#include "Utils/Soundid.h"
#include "ObjectManager.h"

#include "State/StateIdle.h"
#include "State/StateMove.h"
#include "State/StateMoutMove.h"

IStateProxy* SYMount::sm_StateProxys[STATE_MAX];
IStateProxy* SYMount::GetStateInstance(State eState)
{
	return GetStateProxy(eState);
}

// 不同的类别相同状态的代理可能不一样.
IStateProxy* SYMount::GetStateProxy(State StateID)
{
	switch(StateID)
	{
	case STATE_IDLE:
		return StateIdle::Instance();
	case STATE_ACTIONIDLE:
		return StateActionIdle::Instance();
	case STATE_MOUNT_MOVE:
		return StateMountMove::Instance();
	default:
		break;
	}
	return NULL;
}
SYMount::SYMount( ui64 guid, CCharacter* pFather )
{
	m_valuesCount = UNIT_END;
	_InitValues();
	SetGUID( guid << 32 );

	m_pfather = pFather;

	m_bisSYMount = true;
}

SYMount::~SYMount(void)
{
	DetachFromScene(false);
}

bool SYMount::Load(const char* pszFile)
{
	if(!CreateActor(pszFile, false))
	{	
		return false;
	}
	NiNode* ActorRoot = NiDynamicCast(NiNode, m_spSceneNode);
	NIASSERT(ActorRoot);
	ActorRoot->AttachChild(m_spActorManager->GetNIFRoot());
	SetName("");

	AttachToScene(false);
	m_spSceneNode->UpdateProperties();
	m_spSceneNode->UpdateEffects();
	m_spSceneNode->Update(0.0f);
	SetIdelAnimation();

	
	m_spVoice[0] = NiNew ALAudioSource(AudioSource::TYPE_3D);
	m_spVoice[1] = NiNew ALAudioSource(AudioSource::TYPE_3D);
	return true;
}

void SYMount::Update(float dt)
{
	if (dt < 0.000001f)
	{
		return;
	}
	float fTime = SYState()->ClientApp->GetAccumTime();
	DWORD dwTime = UINT(dt*1000);

	UpdateCharacterLOD();
	
	SYStateActorBase::UpdateState(fTime, dt, SL_Poseture);

	CAnimObject::Update(dt);
	CGameObject::Update(dt);
}
BOOL SYMount::SetIdleAnimation(int iType)
{
	AnimationID animID = NiActorManager::INVALID_SEQUENCE_ID;
	const char* szIdleAnimName = GetBaseAnimationName((ECHAR_ANIM_GROUP)iType, 0);
	if(iType == CAG_WAIT)
	{
		SetWaitAnimation();
		return true;
	}
	else
	{
		animID = GetAnimationIDByName(szIdleAnimName);
	}
	NiControllerSequence* Seq = SetCurAnimation(animID, 1.0, TRUE, FALSE );
	return (Seq != NULL);
}

void SYMount::PostOnCreate()
{

}

const char* SYMount::GetBaseAnimationName(ECHAR_ANIM_GROUP eAnimGroup, int nIndex)
{
 	switch (eAnimGroup)
	{
	case CAG_IDLE:			return "stand";
	case CAG_RUN:			return "run";
	case CAG_WAIT:			return "wait";
	case CAG_STAND:			return "wait";
	case CAG_JUMP_UP:		return "jump_start";
	case CAG_JUMP_ON:		return "jump_fly";
	case CAG_JUMP_DOWN:		return "jump_fall";
	case CAG_JUMP_RUN:		return "jump_fall_run";
	case CAG_BACKWARD:		return "Backward01";
	default:
		break;
	}

	return NULL;
}

void SYMount::SetIdelAnimation()
{
	NiControllerSequence* Seq = SetCurAnimation("stand");
	//NIASSERT(Seq);
	if(Seq == NULL)
		return;
	NiTimeController::CycleType CT = Seq->GetCycleType();
	if (CT == NiTimeController::CLAMP)
	{
		Seq->SetCycleType(NiTimeController::LOOP);
	}
}

void SYMount::SetWalkAnimation()
{
	SetCurAnimation("Walk");
}

void SYMount::SetRunAnimation()
{
	NiControllerSequence* Seq = SetCurAnimation("run");
	//NIASSERT(Seq);
	if(Seq == NULL)
		return;
	NiTimeController::CycleType CT = Seq->GetCycleType();
	if (CT == NiTimeController::CLAMP)
	{
		Seq->SetCycleType(NiTimeController::LOOP);
	}
}

void SYMount::SetWaitAnimation()
{
	AnimationID animID = NiActorManager::INVALID_SEQUENCE_ID;
	if(GetWaitSeqNum())
	{
		animID = GetAnimationIDByName("wait");
	}
	NiControllerSequence* Seq = SetCurAnimation(animID, 1.0, TRUE);
	if(Seq)
	{
		ALAudioSource* pkSound = GetSoundInst();
		if (pkSound){
			char soundfile[64];
			CCharacter* pkPlayer = (CCharacter*)ObjectMgr->GetObject(GetGUID()>>32);
			if(!pkPlayer)
				return;
			ui32 entryid = pkPlayer->GetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID);
			CCreatureNameDB::stCNDB cndb;
			if (!ItemMgr->GetCNDB(entryid, cndb))
				return;

			if (cndb.sound_stand != 0 )
			{
				SoundDB::soundfile sf;
				if ( g_pkSoundFiles->GetSoundFile(cndb.sound_stand, sf))
				{
					_snprintf(soundfile, 64, "sound/char/%s.wav", sf.filename.c_str());

					GetSceneNode()->AttachChild(pkSound);
					pkSound->SetMinMaxDistance(5.0f, 80.0f);
					pkSound->SetGain(1.0f);
					SYState()->IAudio->PlaySound(pkSound, soundfile);
				}
			}
		}
	}
}
void SYMount::MoveTo()
{
	SetRunAnimation();
}

void SYMount::StopMove()
{
	__super::StopMove();
//	SetIdelAnimation();
}

bool SYMount::GetPickBound(NiBound& kBound)
{
	if (!GetSceneNode())
		return false;

	kBound.SetRadius(0.f);
	
	return CAnimObject::GetPickBound(kBound);
}

bool SYMount::IsPick(NiPick& kPicker,const NiPoint3& kOrig,const NiPoint3& kDir)
{
	if (!GetSceneNode())
		return false;

	//NiNode* pkNode = NiDynamicCast(NiNode,GetSceneNode()->GetObjectByName("Scene Root"));
	//if (!pkNode)
	//	return false;

	//unsigned int ui = 0;
	//for (ui; ui < pkNode->GetArrayCount(); ui++)
	//{
	//	NiAVObject* pkObj = pkNode->GetAt(ui);

	//	if (GetUInt32Value(UNIT_FIELD_PETNUMBER) && pkObj->GetName().Equals("Dummy01"))
	//		continue;

	//	if (pkObj&& pkObj->IsVisualObject())
	//	{
	//		kPicker.SetTarget(pkObj);
	//		if ( kPicker.PickObjects(kOrig, kDir) )
	//			return true;
	//	}
	//}

	return CAnimObject::IsPick(kPicker, kOrig, kDir);
}

void SYMount::SetVisible(bool bVisible)
{
	m_spActorManager->GetNIFRoot()->SetAppCulled(bVisible);
}

float SYMount::GetHeightOffset()
{
	if(GetSceneNode())
	{
		if (GetSceneNode()->GetObjectByName("Pet_Node01"))
		{
			return GetSceneNode()->GetObjectByName("Pet_Node01")->GetTranslate().z;
		}
	}

	return 0.0f;
}