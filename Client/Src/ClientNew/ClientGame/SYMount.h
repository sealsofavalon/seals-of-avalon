#ifndef SYMOUNT_H
#define SYMOUNT_H

#include "Player.h"

class SYMount : public CCharacter
{
public:
	SYMount(ui64 guid, CCharacter* pFather );
	virtual ~SYMount();

	virtual IStateProxy* GetStateProxy(State StateID);
	virtual IStateProxy* GetStateInstance(State eState);

	bool Load(const char* pszKFMFile);
	virtual BOOL SetIdleAnimation(int iType);

	virtual void Update(float dt);
	virtual void PostOnCreate();

	virtual void MoveTo();
	virtual void StopMove();

	virtual bool GetPickBound(NiBound& kBound);
	virtual bool IsPick(NiPick& kPicker,const NiPoint3& kOrig,const NiPoint3& kDir);

	virtual void SetIdelAnimation();
	virtual void SetWalkAnimation();
	virtual void SetRunAnimation();
	virtual void SetWaitAnimation();

	const char* GetBaseAnimationName(ECHAR_ANIM_GROUP eAnimGroup, int nIndex = 0);
	virtual AnimationID GetNormalAttackAnimation(UINT uSerial) { return 0; };

	void SetVisible(bool bVisible);
	CCharacter* GetFather(){ return m_pfather; }

	float GetHeightOffset();
private:
	static IStateProxy* sm_StateProxys[STATE_MAX];
	CCharacter* m_pfather;
};

#endif