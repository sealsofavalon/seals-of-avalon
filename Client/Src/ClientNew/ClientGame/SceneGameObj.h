#ifndef SCENEGAMEOBJ_H
#define SCENEGAMEOBJ_H
//////////////////////////////////////////////////////////////////////////
//	地图动态物件,比如陷阱,触发
//////////////////////////////////////////////////////////////////////////
#include "Character.h"
#include "Skill/SkillInfo.h"
#include "Utils/GameObjectDB.h"
#include "Effect/SceneEffect.h"

class CSceneGameObj : public CCharacter
{
public:
	CSceneGameObj(void);
	virtual ~CSceneGameObj(void);

	virtual EGAMEOBJ_TYPE GetGameObjectType() const { return GOT_SCENEOBJECT; }

	bool CreateSceneGameObj(ui32 entry);

	CSceneGameObj* GetAsSceneObj() { return this; }
	void SpellGo(SpellID spellID, const vector<ui64>& vTargetHit, const SpellCastTargets& sepllCastTarget);

	virtual void Update(float dt);
	virtual void OnCreate(class ByteBuffer* data, ui8 update_flags);
	virtual bool GetPickBound(NiBound& kBound);
	virtual bool IsPick(NiPick& kPicker,const NiPoint3& kOrig,const NiPoint3& kDir);
	virtual void OnValueChanged(class UpdateMask* mask);
	AnimationID GetNormalAttackAnimation(UINT){return INVALID_ANIMID;}
	BOOL CanSeeCollection();  //是否能采集
	int CanbeCollected();
	BOOL HasRegItem();
	BOOL IsGatherObj();
	uint32 GetReqSpell();
	void SetShowName(BOOL bShow){m_bDrawName = bShow;}
public:
	void AddEffect();
	void DelEffect();
protected:
	CSceneEffect* m_pkScenceOBJEffect;
	bool m_bVisiblePickUpEffect ;

	CSceneEffect* m_pkFlagEffect[7];
	CSceneEffect* m_pkFlagEffectBurn;
	CSceneEffect* m_pkFlagEffectAppear;
	float         m_fShowTime;
protected:
	BOOL m_bDrawName;
	bool m_bFlag;
	float m_fColor;
	NiAmbientLight* m_pkAmbentLight;
	bool  m_bInBurn;
	bool  m_bInappear;
	int   m_FlagType;
	
	CGameObjectDB::stGameObject* m_GameObjInfo;

	virtual bool Draw(CCullingProcess* pkCuller);
	virtual void DrawName(const NiCamera* Camera);

	void UpdateEffect(float dt);
};

#endif