#include "StdAfx.h"
#include "SceneItem.h"
#include "SceneManager.h"
#include "Utils/ClientUtils.h"
#include "Effect/EffectManager.h"
#include "Avatar.h"
#include "ItemManager.h"
#include "ResourceManager.h"
#include "ui/UFun.h"
#include "LocalPlayer.h"
#include "ObjectManager.h"

CSceneItem::CSceneItem()
{
	m_pkItemProperty = NULL;
	m_Color = D3DCOLOR_ARGB(255, 255, 0, 0);
	m_kNamePosOffset = 1.f;
}

CSceneItem::~CSceneItem()
{
	NiAVObject* pkAVObject = m_spSceneNode->GetAt(0);
	if(pkAVObject)
		g_ResMgr->FreeNif(pkAVObject);
	DetachFromScene(true);
}

void CSceneItem::OnCreate(class ByteBuffer* data, ui8 update_flags)
{
	CGameObject::OnCreate(data, update_flags);
	CreateItem(GetUInt32Value(OBJECT_FIELD_ENTRY));

	NiAVObject* pkNameOffset = GetSceneNode()->GetObjectByName("Dummy02");
	if(pkNameOffset)
	{
		m_kNamePosOffset = pkNameOffset->GetTranslate().z * 1.f;
	}


	m_spNameRef = new sNameRecode;
}

bool CSceneItem::GetPickBound(NiBound& kBound)
{
	kBound.SetRadius(0.f);
	NiNode* pkNode = NiDynamicCast(NiNode,GetSceneNode()->GetObjectByName("Scene Root"));
	if (!pkNode)
		return false;

	unsigned int ui = 0;
	for (ui; ui < pkNode->GetArrayCount(); ui++)
	{
		NiAVObject* pkObj = pkNode->GetAt(ui);
		if ( pkObj )
		{
			if (pkObj&& pkObj->IsVisualObject())
			{
				if (kBound.GetRadius() == 0.0f)
					kBound = pkObj->GetWorldBound();
				else
					kBound.Merge(&pkObj->GetWorldBound());
			}
		}

	}

	return  kBound.GetRadius() != 0.0f;;
}

bool CSceneItem::IsPick(NiPick& kPicker,const NiPoint3& kOrig,const NiPoint3& kDir)
{
	NiNode* pkNode = NiDynamicCast(NiNode,GetSceneNode()->GetObjectByName("Scene Root"));
	if (!pkNode)
		return false;

	kPicker.SetTarget(pkNode);
	if ( kPicker.PickObjects(kOrig, kDir) )
		return true;
	

	return false;
}

void CSceneItem::Init( bool isContainer )
{
	if( isContainer )
	{
		m_valuesCount = CONTAINER_END;
		m_typeid = TYPEID_CONTAINER;
	}
	else
	{
		m_valuesCount = ITEM_END;
		m_typeid = TYPEID_ITEM;
	}
	_InitValues();
}

bool CSceneItem::CreateItem(ui32 itemid)
{
	m_pkItemProperty = ItemMgr->GetItemPropertyFromDataBase(itemid);
	if(m_pkItemProperty)
	{
		SetName(m_pkItemProperty->C_name.c_str());
		
		NiAVObject* pkAVObject = g_ResMgr->LoadNif("Objects\\Item\\Box.nif");
		NIASSERT(GetSceneNode());
		AttachToScene();
		m_spSceneNode->AttachChild(pkAVObject);
		m_spSceneNode->UpdateProperties();
		m_spSceneNode->UpdateEffects();
		m_spSceneNode->Update(0.0f);

		UColor c ;
		GetQualityColor(m_pkItemProperty->Quality, c);
		m_Color = c.ColorValue();
		
	}	

	return true;
}

void CSceneItem::Update(float dt)
{
	CGameObject::Update(dt);
}

bool CSceneItem::Draw(CCullingProcess* pkCuller)
{
	if(CGameObject::Draw(pkCuller))
	{
		NiCamera* pkCamera = pkCuller->GetCamera();
		DrawName(pkCamera);

		//NiPoint3 kPos;

		//kPos = GetPosition();
		//kPos.z += m_spSceneNode->GetWorldBound().GetRadius();
		//EffectMgr->AddSceneText(kPos, GetName(), m_Color);

		//WorldPtToUIPt(kPos, pkCuller->GetCamera());

		//EffectMgr->AddSceneText(kPos, GetName(), D3DCOLOR_ARGB(255, 255, 0, 0));

		return true;
	}

	return false;
}

void CSceneItem::DrawName(const NiCamera* Camera)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();

	NiPoint3 Point( GetPosition() );
	Point -= pkLocalPlayer->GetCamera().GetTranslate();

	float dist = Point.Length();

	NiPoint3 kPos( GetSceneNode()->GetTranslate() );
	kPos.z += m_kNamePosOffset;
	WorldPtToUIPt(kPos, Camera);

	CharName* NewName = &m_spNameRef->stName;
	NewName->bAFK = false;
	NewName->bDND = false;
	NewName->bShowMask = false;
	NewName->bShowName = true;
	NewName->Class = 0;
	NewName->ffa = 0;
	NewName->zodiac = 0;
	NewName->bGM = false;
	m_spNameRef->type = GOT_ITEM;

	EffectMgr->AddCharName(m_spNameRef, kPos, GetName(), NULL, NULL, NULL, m_Color, dist, false, true);
}