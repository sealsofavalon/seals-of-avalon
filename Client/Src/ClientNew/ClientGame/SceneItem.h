#ifndef SCENEITEM_H
#define SCENEITEM_H

#include "GameObject.h"
#include "Player.h"
#include "Utils/ItemDB.h"
class CSceneItem : public CGameObject
{
public:
	CSceneItem(void);
	virtual ~CSceneItem(void);

	void Init( bool isContainer );
	virtual EGAMEOBJ_TYPE GetGameObjectType() const { return GOT_ITEM; }
	bool CreateItem(ui32 itemid);
	bool GetPickBound(NiBound& kBound);
	bool IsPick(NiPick& kPicker,const NiPoint3& kOrig,const NiPoint3& kDir);
	virtual void Update(float dt);
	virtual void OnCreate(class ByteBuffer* data, ui8 update_flags);
	virtual bool IsMapItem() const { return true; }
protected:
	ItemPrototype_Client* m_pkItemProperty;
	virtual bool Draw(CCullingProcess* pkCuller);
	virtual void DrawName(const NiCamera* Camera);

	struct sNameRecode* m_spNameRef;
	DWORD m_Color ;
	float m_kNamePosOffset;
};

#endif