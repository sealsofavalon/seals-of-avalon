#pragma once

#include "Map/Map.h"
#include "ArrayEx.h"

#include "CullingProcess.h"

enum EScenePickGroup{
	SPG_PLAYER = 0X0001,
	SPG_ITEM = 0X0002, 
	SPG_CREATURE = 0X004,
	SPG_GAMEOBJ = 0X008,
	SPG_ALL = (SPG_PLAYER | SPG_ITEM | SPG_CREATURE | 0X008),

};

class CGameObject;
class CPlayer;
class CLightManager;
class CTerrainMgr;
class CSceneProperty;
class CDecalManager;
class CGameItem;
class CSceneEffect;
class CRandomMersenne;

#define  MAX_WATERBUBBLE_COUNT 10

class CCollisionManager
{
	friend class CSceneManager;
	typedef std::map<CGameObject*,int> PickObjMap;
	typedef std::vector<CGameObject*> PickObjList;
	typedef CArrayEx<CGameObject*, CGameObject*, std::map<CGameObject*, int> > ObjectList;
	struct ScenePickGroup
	{
		//PickObjMap ObjMap;			// 快速查找.
		//PickObjList ObjList;
		ObjectList Objects;
	};
	enum SPGType
	{
		GT_PLAYER = 0,
		GT_ITEM,
		GT_CREATURE,
		GT_MAX,
	};
public:
	CCollisionManager();
	virtual ~CCollisionManager();
	void InitPick();
	bool PickScene(const NiPoint3& kStart,const NiPoint3& kDir, UINT Groups = SPG_ALL, CGameObject**Result = NULL, NiPoint3* PickPos = NULL, bool bNormals = false, bool bFrontOnly = false, bool bFindFirst = false);

private:
	BOOL AttachToPickGroup(EScenePickGroup PickGroupFlags , CGameObject* Object);
	BOOL DetachFromPickGroup(EScenePickGroup PickGroupFlags, CGameObject* Object);
	BOOL AddToGroup(ScenePickGroup* Group, CGameObject* ObjectID);
	void RemoveFromGroup(ScenePickGroup* Group,CGameObject* ObjectID);
	bool PickScene(ScenePickGroup* Group, const NiPoint3& kStart,const NiPoint3& kDir, CGameObject**Result, NiPoint3* PickPos, bool bNormals, bool bFrontOnly, bool bFindFirst) ;
protected:
	ScenePickGroup m_SPG[GT_MAX];
	NiPick m_Picker;

	NiAVObject* m_pkLastPickObject;
};


class CSceneManager : public NiMemObject
{
	enum
	{
		OBJECT_MAX = 1024 << 5,
	};
public:
	CSceneManager(void);
	virtual ~CSceneManager(void);
	BOOL Initialize();

	BOOL EnterLevel(unsigned int uiMapID, float x, float y);
	void Destroy();

	void Update(float dt);
		
	bool RenderObject(NiAVObject* pkObject);
	void RenderScene(const NiCamera& Camera);
	
	//所有的动态阴影
	void RenderShadows();

	void RenderReflectTexture();
	void RenderRefractTexture();
	void RenderReflectWater();

	BOOL CanRender() const;
	
	// 角色,物品,怪物等需要动态创建的对象加入到场景
	BOOL AttachSceneObject(CGameObject* Object, bool bNetObject = true);
	// 角色,物品,怪物等需要动态创建的对象从场景分离.
	void DetachSceneObject(CGameObject* Object, bool bNetObject = true);
	
	INT FindSceneObjectByID(ui64 ObjectID);
	
	inline CCollisionManager* GetCollisionMgr() const { return m_CollisionMgr;}
	
	BOOL AttachToPickGroup(EScenePickGroup PickGroupFlags , CGameObject* Object);
	BOOL DetachFromPickGroup(EScenePickGroup PickGroupFlags, CGameObject* Object);
	
	BOOL FindMoveTarget(const NiPoint3& kStart,const NiPoint3& kDir, NiPoint3* Target);
		
	CLightManager* GetLightMgr() { return m_pkLightManager; }

	CSceneProperty* GetSceneProperty() { return m_SceneProperty; }

	CDecalManager* GetDecalMgr() { return m_pkDecalManager; }

	CGameObject* FindNearByObj(CGameObject* pkLocalPlayer, float fRadius, UINT Groups, float& fDistance);

	void ResetLevel();

	void SetBoundBoxRenderObject(CGameObject* pObject);
	void ShowBoundBoxRender(bool bShow) { m_bShowBoundBoxRender = bShow; }

	void UpdateWaterBubble();

protected:
	INT FindFreeIndex();
	void CreateBoundBoxRender();
	void UpdateBoundBoxRender();
	void ResetWaterBubble( CSceneEffect* pEffect, const NiPoint3& kPos );


protected:
	CGameObject* m_ObjectPool[OBJECT_MAX];
	typedef std::map<ui64, INT> SceneObjectMap;
	INT m_PoolMaxValidIndex;

	NiLinesPtr	m_spBoundBoxRender;
	bool		m_bShowBoundBoxRender;
	CGameObject*	m_pBoundBoxRenderObject;

	unsigned int	m_uiLastTime;

	SceneObjectMap m_ObjectMap;
	SceneObjectMap m_ClientObjectMap;
	std::vector<INT> m_vItems;		// Item索引

	CCollisionManager* m_CollisionMgr;
	NiAccumulatorPtr	m_spAlphaSorter;
	NiAccumulatorPtr	m_spEffectAS;
	CCullingProcessPtr	m_spCuller;
	//
	CLightManager*	m_pkLightManager;
	CSceneProperty*	m_SceneProperty;
	CDecalManager* m_pkDecalManager;
	ALAudioSourcePtr m_ptrWaterSound;
	CMap* m_pkMap;

	int				m_nWaterBubbleCount;

	CRandomMersenne* m_pRandomMize;
};

