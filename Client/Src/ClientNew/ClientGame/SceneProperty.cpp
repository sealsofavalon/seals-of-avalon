#include "StdAfx.h"
#include "SceneProperty.h"
#include "ClientApp.h"
#include "ClientState.h"
#include "LightManager.h"

CSceneProperty::CSceneProperty()
{
	InitFogParam();
}

void CSceneProperty::InitFogParam()
{
	m_spFog = NiNew NiFogProperty;
	m_spFog->SetFog(true);
	m_spFog->SetFogFunction(NiFogProperty::FOG_Z_LINEAR);
	m_spFog->SetDepth(0.8f); //ʹ��Depth ���� Density
	m_spFog->SetNear(35.0f);
	m_spFog->SetFar(300.0f);

	m_fSceneFogDepth = 0.8f;
	m_fSceneFogStart = 35.0f;
	m_fSceneFogEnd = 300.0f;

	m_fWaterFogDepth = 2.5f;
	m_fWaterFogStart = 3.0f;
	m_fWaterFogEnd = 60.0f;

	m_kWaterFogColor = NiColor(0.7f, 0.7f, 1.0f);
}

CSceneProperty::~CSceneProperty()
{
}

void CSceneProperty::ApplyFog(NiDX9RenderState* pkRenderState,const NiColor& kFogColor, float fDepth, float fStart /* = 0.0f */, float fEnd /* = 0.0f */)
{
	pkRenderState->SetRenderState(D3DRS_FOGENABLE, TRUE);
	float fFogNear = fStart;
	float fFogFar = fEnd;

	pkRenderState->SetRenderState(D3DRS_FOGDENSITY, F2DW(fDepth));
	pkRenderState->SetRenderState(D3DRS_FOGSTART, F2DW(fFogNear));
	pkRenderState->SetRenderState(D3DRS_FOGEND, F2DW(fFogFar));       
	pkRenderState->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_LINEAR);
	pkRenderState->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);
	pkRenderState->SetRenderState(D3DRS_RANGEFOGENABLE, FALSE);

	unsigned int uiFogColor = D3DCOLOR_XRGB((unsigned char)(255.0f*kFogColor.r), (unsigned char)(255.0f*kFogColor.g), (unsigned char)(255.0f*kFogColor.b));
	pkRenderState->SetRenderState(D3DRS_FOGCOLOR, uiFogColor);

	m_spFog->SetFogColor(kFogColor);
	m_spFog->SetDepth(fDepth);
	m_spFog->SetNear(fFogNear);
	m_spFog->SetFar(fFogFar);

}

void CSceneProperty::ApplySceneFog(NiDX9RenderState* pkRenderState, bool bEnable)
{
	if(bEnable)
	{
		float fDepth = m_spFog->GetDepth();
		CLightManager* pkLightManager = CLightManager::Get();

		if(pkLightManager)
		{
			const NiColor& kFogColor = pkLightManager->GetFogColor();
			ApplyFog(pkRenderState, kFogColor, m_fSceneFogDepth, m_fSceneFogStart, m_fSceneFogEnd);
		}
		else
		{
			pkRenderState->SetRenderState(D3DRS_FOGCOLOR, 0);
		}
	}
	else
	{
		pkRenderState->SetRenderState(D3DRS_FOGENABLE, FALSE);
		pkRenderState->SetRenderState(D3DRS_FOGCOLOR, 0);
	}
}

void CSceneProperty::ApplyWaterFog(NiDX9RenderState* pkRenderState)
{
	CLightManager* pkLightManager = CLightManager::Get();

	if(pkLightManager)
	{
		const NiColor& kFogColor = pkLightManager->GetFogColor();

		m_spFog->SetFogFunction(NiFogProperty::FOG_RANGE_SQ);
		ApplyFog(pkRenderState, kFogColor, m_fWaterFogDepth, m_fWaterFogStart, m_fWaterFogEnd);
	}
}