#ifndef CSCENEPROPERTY_H
#define CSCENEPROPERTY_H

class CSceneProperty : public NiMemObject
{
public:
	CSceneProperty();
	~CSceneProperty();

	void ApplySceneFog(NiDX9RenderState* pkRenderState, bool bEnable);
	void ApplyWaterFog(NiDX9RenderState* pkRenderState);

	NiFogPropertyPtr GetFogProperty() { return m_spFog; }
	void InitFogParam();

protected:
	void ApplyFog(NiDX9RenderState* pkRenderState,const NiColor& kFogColor, float fDepth, float fStart = 0.0f, float fEnd = 0.0f);

protected:
	float m_fWaterFogStart;
	float m_fWaterFogEnd;
	float m_fWaterFogDepth;

	float m_fSceneFogStart;
	float m_fSceneFogEnd;
	float m_fSceneFogDepth;

	NiColor	m_kWaterFogColor;


	NiFogPropertyPtr m_spFog;
};

#endif