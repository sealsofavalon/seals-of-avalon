#include "StdAfx.h"
#include "Screenshot.h"
#include "UI/UChat.h"

static bool s_bEnableScreen = false;
void EnableScreenshot()
{
    s_bEnableScreen = true;
}

void TakeScreenshot()
{
    if(!s_bEnableScreen)
        return;

    s_bEnableScreen = false;

    SYSTEMTIME st;
    GetLocalTime(&st);

    unsigned int uiYear = st.wYear % 100;
    unsigned int uiMonth = st.wMonth;
    unsigned int uiDay = st.wDay;
    unsigned int uiHour = st.wHour;
    unsigned int uiMinute = st.wMinute;
    unsigned int uiSecond = st.wSecond;

    char buffer[128];
    NiSprintf(buffer, sizeof(buffer), "Screenshots\\%02d%02d%02d_%02d%02d%02d.jpg", uiYear, uiMonth, uiDay, uiHour, uiMinute, uiSecond);

    NiRenderer* pkRenderer = NiRenderer::GetRenderer();
    if(pkRenderer)
    {
        if( pkRenderer->SaveScreenShot(buffer, NiRenderer::FORMAT_JPEG) )
        {
            //char msg[256];
            //NiSprintf(msg, sizeof(msg), "���� %s<br>", buffer);
			std::string strRet = _TRAN(N_NOTICE_Z70, buffer);
            ChatSystem->AppendChatMsg(CHAT_MSG_SYSTEM, strRet.c_str() );
        }
    }
}