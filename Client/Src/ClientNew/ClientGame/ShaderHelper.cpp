
#include "StdAfx.h"

#include <NiD3DXEffectShaderLibrary.h>
#include <NiD3DXEffectFactory.h>

#include "ShaderHelper.h"

class CD3DXEffectShaderLibrary : public NiD3DXEffectShaderLibrary
{
};

bool CShaderSystem::Setup()
{
    NiNew CShaderSystem();
    return true;
}

void CShaderSystem::Shutdown()
{
    NiDelete g_pkShaderSystem;
}

struct EffectParam
{
    const char* EffectName;
    const char* EffectFile;
    NiMaterial* Material;
};

static EffectParam s_EffectParams[] =
{
    {"AlphaGlow", "Shaders\\AlphaGlow.fx"},
    {"AlphaGlowSkinned", "Shaders\\AlphaGlowSkinned.fx"},
    {"AlphaReflect", "Shaders\\AlphaReflect.fx"},
    {"AlphaReflectSkinned", "Shaders\\AlphaReflectSkinned.fx"},
    {"AlphaSpecular", "Shaders\\AlphaSpecular.fx"},
	{"AlphaSpecular_HL", "Shaders\\AlphaSpecular.fx"},
	{"AlphaSpecular_HL_EDG", "Shaders\\AlphaSpecular.fx"},
	{"AlphaSpecular_ALPHA", "Shaders\\AlphaSpecular.fx"},
    {"AlphaSpecularSkinned", "Shaders\\AlphaSpecularSkinned.fx"},
    {"BloomThreshold", "Shaders\\Bloom.fx"},
    {"GaussianBlurH"},
    {"GaussianBlurV"},
    {"BloomCombine"},
    {"FXSkinningEquipSY", "Shaders\\FXSkingEquipSY.fxl"},
    {"FXSkinningSY", "Shaders\\FXSkingSY.fxl"},
    {"FXSkinningSY_HL", "Shaders\\FXSkingSY.fxl"},
	{"FXSkinningSY_HL_EDG", "Shaders\\FXSkingSY.fxl"},
	{"FXSkinningSY_ALPHA", "Shaders\\FXSkingSY.fxl"},
    {"FXSY", "Shaders\\FXSY.fx"},
    {"FXSY_HL", "Shaders\\FXSY.fx"},
	{"FXSY_HL_EDG", "Shaders\\FXSY.fx"},
	{"FXSY_ALPHA", "Shaders\\FXSY.fx"},
    {"FogMountain", "Shaders\\Mountain.fx"},
    {"Shadow", "Shaders\\Shadow.fx"},
    {"ShadowSkinned", "Shaders\\ShadowSkinned.fx"},
	{"HighLighting", "Shaders\\SY_Skinning.fx"},
    {"GlowNormal", "Shaders\\GlowNormal.fx"},
	{"Bloom_SmoothPS", "Shaders\\BloomEffect.fx"},
	{"Bloom_SmoothBigPS"},
	{"Bloom_FinalPS"},
    {NULL, NULL},
};

CShaderSystem* CShaderSystem::ms_pkThis = NULL;
CShaderSystem::CShaderSystem()
{
    NIASSERT(ms_pkThis == NULL);
    ms_pkThis = this;

    NiD3DRenderer* pkD3DRenderer = (NiD3DRenderer*)NiRenderer::GetRenderer();
    NiD3DXEffectShaderLibrary* pkLibrary = NiNew CD3DXEffectShaderLibrary;
    pkLibrary->SetRenderer(pkD3DRenderer);

    NiD3DXEffectFactory::GetInstance(true);


    char* acBuffer = (char*)malloc(1024*256); //希望不要有大于256K的Shader

    NiFile* pkFile;
    NiD3DXEffectFile* pkD3DXEffectFile;
    unsigned int uiBytes;
    for(int i = 0; s_EffectParams[i].EffectName; ++i)
    {
        if(s_EffectParams[i].EffectFile == NULL)
            continue;

        pkFile = NiFile::GetFile(s_EffectParams[i].EffectFile, NiFile::READ_ONLY);
        if( !pkFile || !(*pkFile) )
        {
            NiDelete pkFile;
            continue;
        }

        uiBytes = pkFile->Read(acBuffer, 1024*256);
        acBuffer[uiBytes] = '\0';

        pkD3DXEffectFile = NiD3DXEffectFactory::CreateD3DXEffectFromCode(s_EffectParams[i].EffectName, uiBytes, acBuffer);
        if(pkD3DXEffectFile)
            pkLibrary->InsertD3DXEffectFileIntoList(pkD3DXEffectFile);

        NiDelete pkFile;
    }

    NiShaderFactory::RegisterLibrary(pkLibrary);

    for(int i = 0; s_EffectParams[i].EffectName; ++i)
    {
        s_EffectParams[i].Material = NiMaterialLibrary::CreateMaterial(s_EffectParams[i].EffectName);
       // DASSERT(s_EffectParams[i].Material);
        if(s_EffectParams[i].Material)
        {
            s_EffectParams[i].Material->IncRefCount();
        }
    }

    m_pkMatFXSY = NiMaterial::GetMaterial("FXSY");
	m_pkMatFXSY_HL = NiMaterial::GetMaterial("FXSY_HL");
	m_pkMatFXSY_HL_EDG = NiMaterial::GetMaterial("FXSY_HL_EDG");

    m_pkMatFXSkinningSY = NiMaterial::GetMaterial("FXSkinningSY");
    m_pkMatFXSkinningSY_HL = NiMaterial::GetMaterial("FXSkinningSY_HL");
	m_pkMatFXSkinningSY_HL_EDG = NiMaterial::GetMaterial("FXSkinningSY_HL_EDG");

	free( acBuffer );
}

CShaderSystem::~CShaderSystem()
{
    for(int i = 0; s_EffectParams[i].EffectName; ++i)
    {
        if(s_EffectParams[i].Material)
        {
            s_EffectParams[i].Material->DecRefCount();
            s_EffectParams[i].Material = NULL;
        }
    }

    ms_pkThis = NULL;
}

