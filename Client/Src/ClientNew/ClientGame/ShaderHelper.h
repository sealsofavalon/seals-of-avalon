
#ifndef SHADERHELPER_H
#define SHADERHELPER_H

class CShaderSystem : public NiMemObject
{
public:
    static bool Setup();
    static void Shutdown();
    static CShaderSystem* Get() { return ms_pkThis; }

    CShaderSystem();
    ~CShaderSystem();

    NiMaterial* GetMatFXSY() { return m_pkMatFXSY; }
	NiMaterial* GetMatFXSY_HL() { return m_pkMatFXSY_HL; }
	NiMaterial* GetMatFXSY_HL_EDG() { return m_pkMatFXSY_HL_EDG; }

    NiMaterial* GetMatFXSkinningSY() { return m_pkMatFXSkinningSY; }
    NiMaterial* GetMatFXSkinningSY_HL() { return m_pkMatFXSkinningSY_HL; }
	NiMaterial* GetMatFXSkinningSY_HL_EDG() { return m_pkMatFXSkinningSY_HL_EDG; }

private:
    NiMaterial* m_pkMatFXSY;
	NiMaterial* m_pkMatFXSY_HL;
	NiMaterial* m_pkMatFXSY_HL_EDG;

    NiMaterial* m_pkMatFXSkinningSY;
    NiMaterial* m_pkMatFXSkinningSY_HL;
	NiMaterial* m_pkMatFXSkinningSY_HL_EDG;

    static CShaderSystem* ms_pkThis;
};

#define g_pkShaderSystem (CShaderSystem::Get())

#endif
