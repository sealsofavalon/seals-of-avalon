#include "StdAfx.h"
#include "Skill.h"
#include "Character.h"
#include "Player.h"
#include "ClientApp.h"
#include "PlayerInputMgr.h"
#include "Effect/EffectManager.h"
#include "SceneManager.h"
#include "Effect\DecalManager.h"
#include "ObjectManager.h"
#include "UI/UISYstem.h"
#include "Swoosh.h"
#include "ResourceManager.h"
#include "AudioInterface.h"
#include "SpellRush.h"
#include "ui/UIGamePlay.h"

SpellStageDescBase::~SpellStageDescBase()
{
	for (int i = 0; i < (int)Effects.size(); i++)
	{
		SpellEffectNode* pEntry = Effects[i];
		NiDelete pEntry;
	}
	Effects.clear();
}

SpellEffectNode::SpellEffectNode()
{
//	AttachNode = -1;				
	Slot = SES_BODY;	
	ForceClearAtStageEnd = FALSE;
	MaxLife = 0.f;
	Offset = 0.0f;
	Media[0] = 0;
}

SpellEffectNode::~SpellEffectNode()
{

}

SpellTemplate::SpellTemplate()
{
	m_SpellId = -1;
	m_Name[0] = '\0';		// 技能名称
	m_LongDesc = NULL;
	m_LevelLimit = 0;		// 技能级别限制	
	m_Profession = -1;		// 技能职业		-1: 表示所有职业
	m_SpellCls = SPELL_CLS_BASE;		// 技能类别
	m_AttackRange = 0.0f;	// 攻击距离 
	m_TargetRelative = STR_SELF;	// 目标社会关系
	m_TargetClass = TYPE_UNIT;		// 目标类别
	m_MPRequest = 0;
	m_HPRequest = 0;
	m_LevelRequest = 0;

	m_SelectionDesc.SelMethod = 0;
	m_SelectionDesc.Brush[0] = '\0';
	m_CastDesc = NULL;
	m_AttackDesc = NULL;
	m_DamageDesc = NULL;
}

SpellTemplate::~SpellTemplate()
{
	if (m_CastDesc)
	{
		NiDelete m_CastDesc;
	}
	if (m_AttackDesc)
	{
		NiDelete m_AttackDesc;
	}
	if (m_DamageDesc)
	{
		NiDelete m_DamageDesc;
	}
}

SpellInstance* SpellTemplate::CreateInstance()
{
	if (m_SpellCls == SPELL_CLS_BASE)
	{
		return NiNew SpellInstance(this);
	}else if (m_SpellCls == SPELL_CLS_RUSH)
	{
		return NiNew SpellRush(this);
	}
	return NULL;
}

BOOL SpellTemplate::CheckOwnerRequest(CCharacter* pOwner) const
{
	// 等级
	if ((int)pOwner->GetLevel() < m_LevelRequest)
	{
		EffectMgr->AddSceneText(NiPoint3(300.f, 300.f, 0.0f), "等级不够", 0xFFFFC87C, 3.f);
		return FALSE;
	}
	// HP
	if((int)pOwner->GetHP() < m_HPRequest)
	{
		EffectMgr->AddSceneText(NiPoint3(300.f, 300.f, 0.0f), "血量不足", 0xFFFFC87C, 3.f);
		return FALSE;
	}
	// MP
	if ((int)pOwner->GetMP() < m_MPRequest)
	{
		EffectMgr->AddSceneText(NiPoint3(300.f, 300.f, 0.0f), "魔法不足", 0xFFFFC87C, 3.f);
		// TODO Notify
		return FALSE;
	}
	return TRUE;
}

BOOL SpellTemplate::IsTargetValid(UpdateObj* pTarget) const
{
	if (pTarget == NULL)
	{
		// target is position
		if (m_TargetClass & STC_MAPPOSITION)
		{
			return TRUE;
		}
	}else
	{
		TYPE objType = pTarget->GetType();
		if (m_TargetClass & objType )
		{
			return TRUE;
		}	
	}
	return TRUE;			// 当前不明确策划的具体需求，故所有类型目标均可被攻击.
	return FALSE;
}

BOOL SpellTemplate::IsTargetRelativeValid(UpdateObj* pAttacker, UpdateObj* pAttackee) const
{
	NIASSERT(pAttacker);
	if (pAttackee == NULL)
	{
		return TRUE;
	}

	switch(m_TargetRelative)
	{
	case STR_SELF:
		return (pAttacker == pAttackee);
	case STR_FRIENDLY:
		{
			
		}
	default:
		return TRUE;
	}
	
	return FALSE;
}

BOOL SpellTemplate::IsNeedSelectTargetObject(CCharacter* pOwner) const
{
	if (GetSelectMethod() == STT_NONE)
		return FALSE;

	return TRUE;
}

int SpellTemplate::GetSelectMethod() const
{
	return m_SelectionDesc.SelMethod;
}

BOOL SpellTemplate::SelectTarget(CCharacter* Caller) const 
{
	// 对于非本地玩家, 不可能进行目标选择操作.
	CPlayer* LocalPlayer = Caller->GetAsPlayer();
	if (LocalPlayer == NULL || !LocalPlayer->IsLocalPlayer())
	{
		return FALSE;
	}
	int SelMethod = m_SelectionDesc.SelMethod;
	if (SelMethod == 0)
	{
		return FALSE;
	}
	
	// 本地玩家操作, 转发处理.
	SYState()->LocalPlayerInput->SelectTarget(SelMethod, 0, m_SelectionDesc.SelRadius, 0.0f, m_SelectionDesc.Brush);
	// 返回真, 让攻击操作保留.
	return TRUE;
}

CEffectBase* SpellTemplate::CreateEffect(SpellEffectNode* pEffectNode) const
{	
	NIASSERT(pEffectNode);
	int fxtype = pEffectNode->GetType();
	CEffectBase* pEffect = NULL;
	switch(fxtype)
	{
	case 0:
		pEffect = CreateNifEffect((SpellEffectNifNode*)pEffectNode);
		break;
	case 1:
		pEffect = CreateDecalEffect((SpellDecalEffectNode*)pEffectNode);
		break;
	case 2:
		pEffect = CreateTrailEffect((SpellTrailEffectNode*)pEffectNode);
		break;
	case 3:
		pEffect = CreateProjectileEffect((SpellProjectileEffectNode*)pEffectNode);
		break;
	default:
		break;
	}
	if (pEffect)
	{
		pEffect->SetLife(0.f);
		pEffect->SetMaxLife(pEffectNode->MaxLife);
		pEffect->Register();
	}
	return pEffect;
}

CEffectBase* SpellTemplate::CreateNifEffect(SpellEffectNifNode* pEffectNode) const
{
	NiAVObjectPtr pNif = ResMgr::InstanceNif(pEffectNode->Media);
	if (pNif == NULL)
	{
		return NULL;
	}
	pNif->Update(0.0f);
	pNif->UpdateEffects();
	pNif->UpdateProperties();
	//if (pEffectNode->AniLoopCnt < 0)
	//{
	//}
	CGameObject::SetController(pNif, NiTimeController::APP_INIT, NiTimeController::LOOP);
	NiTimeController::StartAnimations(pNif, -pEffectNode->Offset);
	return NiNew CSceneEffect(pNif);
}

CEffectBase* SpellTemplate::CreateDecalEffect(SpellDecalEffectNode* pEffectNode) const
{
	DecalTerrain* pDecalTerrain = NiNew DecalTerrain();
	NIASSERT(pDecalTerrain);
	pDecalTerrain->SetTexture(pEffectNode->Media);
	pDecalTerrain->SetRadius(pEffectNode->fRadius);
	return pDecalTerrain;
}

CEffectBase* SpellTemplate::CreateTrailEffect(SpellTrailEffectNode* pEffectNode) const
{
	return NULL;
}

CEffectBase* SpellTemplate::CreateProjectileEffect(SpellProjectileEffectNode* pEffectNode) const
{
	ProjectileEffect* pEffect = NiNew ProjectileEffect;

	return pEffect;
}

void SpellTemplate::Damage(CCharacter* Caller, CCharacter* Target)
{
	if (Target == NULL || m_DamageDesc == NULL)
	{
		return;
	}
	std::vector<SpellEffectNode*>& EffectNodes = m_DamageDesc->Effects;
	for (int i = 0 ; i < (int)EffectNodes.size(); i++)
	{
		CEffectBase* pEffect = EmitStageEffect(Target->GetGUID(), EffectNodes[i]);
		if (pEffect == NULL)
		{
			NILOG("create effect failed.");
			continue;
		}
	}
}

CEffectBase* SpellTemplate::EmitStageEffect(SYObjID TargetId, SpellEffectNode* EffectNode)
{
	CEffectBase* pEffect = CreateEffect(EffectNode);
	if (pEffect)
	{
		pEffect->SetMaxLife(EffectNode->MaxLife);
		switch(EffectNode->Slot)
		{
		case SES_HEAD01:
			{
				NiFixedString HeadNode("Effect_Head01");
				pEffect->AttachToSceneObject(TargetId, &HeadNode);
			}
			break;
		case SES_HEAD02:
			{
				NiFixedString HeadNode("Effect_Head02");
				pEffect->AttachToSceneObject(TargetId, &HeadNode);
			}
			break;
		case SES_FOOT:
		case SES_BODY:
			pEffect->AttachToSceneObject(TargetId);
			break;
		case SES_TERRAIN:
			break;
		case SES_WEAPON:
			{
				NiFixedString WeaponNode("AttackRightHand_Node");
				pEffect->AttachToSceneObject(TargetId, &WeaponNode);
			}
			break;

		default:
			break;
		}
	}

	return pEffect;
}

SpellInstance::SpellInstance(SpellTemplate* pTemplate):m_SpellTemplate(pTemplate)
{
	m_AttackTime = 0;
	m_AttackStep = LPAS_NONE;

	m_iLastStage = SS_Nothing;
	m_fCastFullTime = 0;
	m_fAttackStartTime = 0;
	m_fCastStartTime = 0;

	m_bHideWeapon[0] = m_bHideWeapon[1] = false;

}

SpellInstance::~SpellInstance()
{

}

void SpellInstance::Go(const Target& Targets)
{
	if (!m_pOwner)
		return;

	if (m_pOwner->GetAsPlayer() && m_pOwner->GetAsPlayer()->IsLocalPlayer())
	{
		CPlayerLocal* pLocPlayer = (CPlayerLocal*)m_pOwner;
		pLocPlayer->ResetSpellCD(m_SpellTemplate->m_SpellId);
	}
	
	BeginAttack(Targets);
//	CreateAttackEffect(Targets);
}

BOOL SpellInstance::Begin()
{
	int iSpellStage = m_pOwner->GetCurSpellStage();
	if (iSpellStage == SS_Cast)
	{
		float fCastTime = m_pOwner->GetCastTime();
		BeginCast(fCastTime);
	}
	else
	if (iSpellStage == SS_Release)
	{
		Go(m_pOwner->GetTargetList());
	}

	// direction
	CCharacter* pTarget = m_pOwner->GetTargetObject();
	if (pTarget && pTarget != m_pOwner )
	{
		m_pOwner->SetDirectionTo(pTarget->GetPosition());
	}

	ChangeApperance();

	return TRUE;
}

void SpellInstance::RestoreApperance()
{
	NiNodePtr spNode = m_pOwner->GetSceneNode();

	NiAVObject* pRightWeapon = spNode->GetObjectByName("RightWeapon");
	NiAVObject* pLeftWeapon = spNode->GetObjectByName("LeftWeapon");
	if (m_bHideWeapon[0] && pLeftWeapon)
	{
		pLeftWeapon->SetAppCulled(false);
	}

	if (m_bHideWeapon[1] && pRightWeapon)
	{
		pRightWeapon->SetAppCulled(false);
	}
}

BOOL SpellInstance::ChangeApperance()
{
	if (!m_SpellTemplate || !m_pOwner)
		return FALSE;

	if (m_SpellTemplate->m_ApperanceDesc.bHideWeapon)
	{
		NiNodePtr spNode = m_pOwner->GetSceneNode();

		if (!spNode)
			return FALSE;

		NiAVObject* pRightWeapon = spNode->GetObjectByName("RightWeapon");
		NiAVObject* pLeftWeapon = spNode->GetObjectByName("LeftWeapon");

		if (pRightWeapon && !pRightWeapon->GetAppCulled()) {
			pRightWeapon->SetAppCulled(true);
			m_bHideWeapon[1] = true;
		}

		if (pLeftWeapon) {
			pLeftWeapon->SetAppCulled(true);
			m_bHideWeapon[0] = true;
		}
	}

	return TRUE;
}

BOOL SpellInstance::Update(DWORD dwTime)
{
	float fCurrentTime = SYState()->ClientApp->GetAccumTime();
	m_AttackTime += dwTime;
	
	if (m_AttackStep == LPAS_CAST)
	{
		return UpdateCast(fCurrentTime);
	}else if (m_AttackStep == LPAS_ATTACK)
	{
		return UpdateAttack(fCurrentTime);
	}else 
	{
		
	}

	return TRUE;
}

void SpellInstance::End(BOOL bInterrupted)
{
	if (!m_pOwner)
		return;

	if (bInterrupted)
	{
		EndCast();
		EndAttack();

		m_pOwner->SetCurSpellStage(SS_Nothing);
		m_pOwner->SetLastSpellStage(SS_Nothing);
	}
	else
	{
		if (m_pOwner->GetLastSpellStage() == SS_Cast || m_pOwner->GetCurSpellStage() == SS_Cast)
		{
			EndCast();
			m_pOwner->SetLastSpellStage(SS_Nothing);
		}
		else
		if (m_pOwner->GetCurSpellStage() == SS_Release)
		{
			EndAttack();
			m_pOwner->SetCurSpellStage(SS_Nothing);
			m_pOwner->SetLastSpellStage(SS_Nothing);
		}
	}

	RestoreApperance();
}

void SpellInstance::BeginCast(float fCastTime)
{
	NIASSERT(m_pOwner);
	if (!m_pOwner)
		return;

	float fCurrentTime = SYState()->ClientApp->GetAccumTime();
	m_fCastStartTime = fCurrentTime;
	m_fCastFullTime = fCastTime;

	m_pOwner->SetCurAnimation(m_SpellTemplate->m_CastDesc->Animation, 0, 1);
	m_AttackStep = LPAS_CAST;
	m_AttackTime = 0;
	CreateCastEffects();
	
	if (m_SpellTemplate->m_CastDesc->Sound[0] != 0)
	{
		SYState()->IAudio->PlaySound(m_SpellTemplate->m_CastDesc->Sound, m_pOwner->GetPosition());
	}

	// 本地玩家显示吟唱对话框和锁定玩家移动输入
	CPlayer* Player = m_pOwner->GetAsPlayer();
	if (Player && Player->IsLocalPlayer())
	{
		//SYState()->LocalPlayerInput->LockMove();
		//			CUIPrepareMagic* CastingDialog = (CUIPrepareMagic*)SYState()->UI->GetDialog(DID_PREPAREMAGIC);
		//			CastingDialog->ShowWindow(1);
		//			CastingDialog->SetPos(0.0f);
		UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
		if (pInGame)
		{
			pInGame->GetUSkillCastBar()->SetVisible(TRUE);
			pInGame->GetUSkillCastBar()->SetPos(0.0f);
		}
	}
}

void SpellInstance::CastDelay(float fDelayTime)
{
	m_fCastFullTime += fDelayTime;
}

void SpellInstance::CastFailure()
{
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pInGame)
	{
		pInGame->GetUSkillCastBar()->SetPos(1.0f);
		pInGame->GetUSkillCastBar()->SetDisable(TRUE);
	}
	End(TRUE);
}

BOOL SpellInstance::UpdateCast(float fTime)
{
	if (m_pOwner == ObjectMgr->GetLocalPlayer())
	{
		UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
		if (pInGame)
		{
			pInGame->GetUSkillCastBar()->SetVisible(TRUE);
			pInGame->GetUSkillCastBar()->SetCurTime(fTime-m_fCastStartTime);
			pInGame->GetUSkillCastBar()->SetSkillText(m_SpellTemplate->m_Name,FALSE,m_fCastFullTime);
		}
	}

	return FALSE;
}

void SpellInstance::EndCast()
{
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pInGame)
	{
		pInGame->GetUSkillCastBar()->SetVisible(FALSE);

	}
	ClearCastEffects();
}

void SpellInstance::BeginAttack(const Target& Targets)
{
	float fCurrentTime = SYState()->ClientApp->GetAccumTime();

	m_fAttackStartTime = fCurrentTime;
	m_AttackStep = LPAS_ATTACK;
	if( m_SpellTemplate->m_AttackDesc )
	{
		if (m_SpellTemplate->m_AttackDesc->Sound[0] != 0)
		{
			SYState()->IAudio->PlaySound(m_SpellTemplate->m_AttackDesc->Sound, m_pOwner->GetPosition());
		}
		m_pOwner->SetCurAnimation(m_SpellTemplate->m_AttackDesc->Animation, 1.0f, FALSE, TRUE);
	}
	//EmitStageEffects(m_SpellTemplate->m_AttackDesc->Effects);
	CreateAttackEffect(Targets);
}

BOOL SpellInstance::UpdateAttack(float fTime)
{
	float nextEndTime = m_pOwner->GetNextEndTime();
	BOOL bIsEnd = m_pOwner->IsCurAnimEndInTime(fTime);

	if (bIsEnd)
	{
		float fT = fTime - nextEndTime;
		_asm nop
	}
	/*
	SpellAttackDesc* pAttackDesc = m_SpellTemplate->m_AttackDesc;
	NIASSERT(pAttackDesc);
	float Progress = m_AttackTime * 0.001f;
	if (Progress >= pAttackDesc->Time)
	{
		EndAttack();
		return FALSE;
	}
	*/
	// process effect.

	return bIsEnd;
}

void SpellInstance::EndAttack()
{
	m_pOwner->ClearTargetList();
/*	CPlayer* Player = m_pOwner->GetAsPlayer();
	if (Player && Player->IsLocalPlayer())
	{
		SYState()->LocalPlayerInput->UnlockMove();
	}	
*/
}

void SpellInstance::EmitStageEffects(const std::vector<SpellEffectNode*>& EffectNodes)
{
	for (int i = 0 ; i < (int)EffectNodes.size(); i++)
	{
		EmitStageEffect(EffectNodes[i]);
	}
}

CEffectBase* SpellInstance::EmitStageEffect(SpellEffectNode* EffectNode)
{
	CEffectBase* pEffect = m_SpellTemplate->CreateEffect(EffectNode);
	if (pEffect)
	{
		pEffect->SetMaxLife(EffectNode->MaxLife);
		switch(EffectNode->Slot)
		{
		case SES_HEAD01:
			{
				NiFixedString HeadNode("Effect_Head01");
				pEffect->AttachToSceneObject(m_pOwner->GetGUID(), &HeadNode);
			}
			break;
		case SES_HEAD02:
			{
				NiFixedString HeadNode("Effect_Head02");
				pEffect->AttachToSceneObject(m_pOwner->GetGUID(), &HeadNode);
			}
			break;
		case SES_FOOT:
		case SES_BODY:
			pEffect->AttachToSceneObject(m_pOwner->GetGUID());
			break;
		case SES_TERRAIN:
			break;
		case SES_WEAPON:
			{
				NiFixedString WeaponNode("AttackRightHand_Node");
				pEffect->AttachToSceneObject(m_pOwner->GetGUID(), &WeaponNode);
			}
			break;
		
		default:
			break;
		}
	}

	return pEffect;
}

void SpellInstance::CreateCastEffects()
{
	std::vector<SpellEffectNode*>& EffectNodes = m_SpellTemplate->m_CastDesc->Effects;
	for (int i = 0 ; i < (int)EffectNodes.size(); i++)
	{
		CEffectBase* pEffect = EmitStageEffect(EffectNodes[i]);
		if (pEffect == NULL)
		{
			NILOG("create effect failed.");
			continue;
		}
		if (EffectNodes[i]->GetType() != 3 && pEffect->GetMaxLife() <= 0.00001f)
		{
			m_CastEffects.push_back(pEffect);
		}
	}
}

void SpellInstance::ClearCastEffects()
{
	for (int i = 0; i < (int)m_CastEffects.size(); i++)
	{
		CEffectBase* pEffect = m_CastEffects[i];
		pEffect->UnRegister();
	}
	m_CastEffects.clear();
}

void SpellInstance::CreateAttackEffect(const Target& Targets)
{
	std::vector<SpellEffectNode*>& EffectNodes = m_SpellTemplate->m_AttackDesc->Effects;
	for (int t = 0; t < Targets.GetNumTargets(); t++)
	{
		const Target::TargetEntry& rTargetEntry = Targets.GetTargetEntry(t);
		for (int i = 0 ; i < (int)EffectNodes.size(); i++)
		{
			SpellEffectNode* EffectNode = EffectNodes[i];
			CEffectBase* pEffect = m_SpellTemplate->CreateEffect(EffectNode);
			if (!pEffect)
			{
				continue;
			}
			pEffect->SetMaxLife(EffectNode->MaxLife);
			if (EffectNode->GetType() == 3)
			{
				ProjectileEffect* pProjEffect = (ProjectileEffect*)pEffect;
				NiNode* pAttackerNode = m_pOwner->GetSceneNode();
				NiNode* RightHandleNode = (NiNode*)pAttackerNode->GetObjectByName("AttackRightHand_Node");
				NIASSERT(RightHandleNode);

				PROJECTILE_DESC ArrowDesc;
				ArrowDesc.Billboard = 0;
				ArrowDesc.MakeTrail = 1;
				ArrowDesc.MaxLife = 65536;		// 箭矢追上目标时间.
				ArrowDesc.Scale = 1.f;
				NiStrcpy(ArrowDesc.ProjectileModel, MAX_PATH, EffectNode->Media);
				NiStrcpy(ArrowDesc.ExplosionModel, MAX_PATH, ((SpellProjectileEffectNode*)EffectNode)->ExpMedia);
				if(pProjEffect->CreateProjectile(&ArrowDesc, RightHandleNode->GetWorldTranslate()))
				{
					Projectile* pProj = pProjEffect->GetProjectile();
					if (rTargetEntry.Object)
					{
						pProj->TrackObject(rTargetEntry.ObjectID);
					}
				}
			}else if (EffectNode->GetType() == 0)
			{
				switch(EffectNode->Slot)
				{
				
				case SES_TERRAIN:
					break;
				case SES_WEAPON:
					{
						NiFixedString WeaponNode("AttackRightHand_Node");
						pEffect->AttachToSceneObject(m_pOwner->GetGUID(), &WeaponNode);
					}
					break;
				case SES_HEAD01:
					{
						NiFixedString HeadNode("Effect_Head01");
						pEffect->AttachToSceneObject(m_pOwner->GetGUID(), &HeadNode);
					}
					
					break;
				case SES_HEAD02:
					{
						NiFixedString HeadNode("Effect_Head02");
						pEffect->AttachToSceneObject(m_pOwner->GetGUID(), &HeadNode);
					}
					break;
				case SES_FOOT:
				case SES_BODY:
					pEffect->AttachToSceneObject(m_pOwner->GetGUID());
					break;
				default:
					break;
				}
			}



		}
	}
	

}

void SpellInstance::DamageTargets(const Target& Targets)
{
	DamageResult NewDamage;
	NewDamage.Time = 0.0f;
	NewDamage.TotalTime = 0.0f;
	NewDamage.SpellId = m_SpellTemplate->m_SpellId;

	for (int t = 0; t < Targets.GetNumTargets(); t++)
	{
		const Target::TargetEntry& rTargetEntry = Targets.GetTargetEntry(t);
		if (rTargetEntry.Object)
		{
			CCharacter* TargetObject = (CCharacter*)ObjectMgr->GetObject(rTargetEntry.ObjectID);
			if (TargetObject == NULL)
			{
				continue;
			}
			TargetObject->Damage(NewDamage);
		}
	}
}
#if 0

CSkill::CSkill(SpellID TypeID)
{
	memset(&m_Info,0,sizeof(SKILL_INFO));

	// TEST
	if (TypeID == SKILL_TYPE_MGC_1)
	{
		m_Info.Identity = SKILL_TYPE_MGC_1;
		NiStrcpy(m_Info.SkillName, SKILL_NAME_MAX, "Magic 1");
		m_Info.TargetSelectionFlag = SKILL_TARGETSEL_AREA;
		m_Info.TargetAreaFlag = SKILL_TARGETAREA_ROUND;
		NiStrcpy(m_Info.TargetSelBrush, 128, "Texture\\Decal\\WOW_.dds");
		NiStrcpy(m_Info.PrepareAnim,128, "SkillCast");
		NiStrcpy(m_Info.PrepareEffect,128,"GreatCureTarget.nif");
		NiStrcpy(m_Info.ReleaseAnim,128, "SkillCast");
		NiStrcpy(m_Info.ReleaseEffect,128,"InfernoArea1.nif");
		m_fTargetAreaParam0 = 4.0f;
		m_fTargetAreaParam1 = 0.0f;
		m_PrepareTime = 1000;
		m_ReleaseTime = m_PrepareTime + 2500;
		m_bResetSkillWhenEnd = TRUE;
	}else if(TypeID == SKILL_TYPE_FIREBALL)
	{
		m_Info.Identity = SKILL_TYPE_FIREBALL;
		NiStrcpy(m_Info.SkillName, SKILL_NAME_MAX, "大火球术");
		m_Info.TargetSelectionFlag = SKILL_TARGETSEL_OBJECT;
		m_Info.TargetAreaFlag = 0;
		NiStrcpy(m_Info.PrepareAnim,128, "Jump");
		NiStrcpy(m_Info.PrepareEffect,128,"GreatCureTarget.nif");
		NiStrcpy(m_Info.ReleaseAnim,128, "AttackGlave_02");
		NiStrcpy(m_Info.ReleaseEffect,128,"PROJ#DrainMind.nif");
		m_Info.MaxRange = 14.0f;
		//m_Info.NumReleaseSerial = 3;
		m_fTargetAreaParam0 = 0.0f;
		m_fTargetAreaParam1 = 0.0f;
		m_PrepareTime = 1000;
		m_ReleaseTime = m_PrepareTime + 1500;
		m_bResetSkillWhenEnd = FALSE;
	}else if (TypeID == SKILL_TYPE_CUTOFHOST)
	{
		m_Info.Identity = SKILL_TYPE_CUTOFHOST;
		NiStrcpy(m_Info.SkillName, SKILL_NAME_MAX, "鬼斩");
		m_Info.TargetSelectionFlag = SKILL_TARGETSEL_OBJECT;

		NiStrcpy(m_Info.ReleaseAnim,128, "AttackGlave_03");
		NiStrcpy(m_Info.ReleaseEffect,128,"InfernoArea1.nif");
		m_Info.MaxRange = 3.0f;
		m_fTargetAreaParam0 = 0.0f;
		m_fTargetAreaParam1 = 0.0f;
		m_PrepareTime = 0;
		m_ReleaseTime = 0;
		m_bResetSkillWhenEnd = FALSE;
	}else if (TypeID == SKILL_TYPE_SUCKMP)
	{
		m_Info.Identity = SKILL_TYPE_SUCKMP;
		NiStrcpy(m_Info.SkillName, SKILL_NAME_MAX, "吸取魔力");
		m_Info.TargetSelectionFlag = SKILL_TARGETSEL_OBJECT;
		m_Info.TargetAreaFlag = 0;
		NiStrcpy(m_Info.PrepareAnim,128, "Jump");
		NiStrcpy(m_Info.PrepareEffect,128,"GreatCureTarget.nif");
		NiStrcpy(m_Info.ReleaseAnim,128, "AttackGlave_02");
		NiStrcpy(m_Info.ReleaseEffect,128,"PROJ#DrainMind.nif");
		m_Info.MaxRange = 14.0f;
		//m_Info.NumReleaseSerial = 3;
		m_fTargetAreaParam0 = 0.0f;
		m_fTargetAreaParam1 = 0.0f;
		m_PrepareTime = 1000;
		m_ReleaseTime = m_PrepareTime + 1500;
		m_bResetSkillWhenEnd = FALSE;
	}else if (TypeID == SKILL_TYPE_QUAKE_SHOCK)
	{
		m_Info.Identity = SKILL_TYPE_QUAKE_SHOCK;
		NiStrcpy(m_Info.SkillName, SKILL_NAME_MAX, "震地一击");

		m_Info.Profession = CLASS_WARRIOR;
		m_Info.ProfessionSpellIndex = 1;
		m_Info.TargetSelectionFlag = SKILL_TARGETSEL_SELF;
		m_Info.TargetAreaFlag = SKILL_TARGETAREA_ROUND;

		NiStrcpy(m_Info.ReleaseAnim,128, "Spell_001_002");
		NiStrcpy(m_Info.ReleaseEffect,128,"InfernoArea1.nif");
		m_Info.MaxRange = 0.0f;
		m_fTargetAreaParam0 = 0.0f;
		m_fTargetAreaParam1 = 0.0f;
		m_PrepareTime = 0;
		m_ReleaseTime = 2000;
		m_bResetSkillWhenEnd = FALSE;
	}else if (TypeID == SKILL_TYPE_HANDY_HEW)
	{
		m_Info.Identity = SKILL_TYPE_HANDY_HEW;
		NiStrcpy(m_Info.SkillName, SKILL_NAME_MAX, "顺势劈斩");
		m_Info.Profession = CLASS_WARRIOR;
		m_Info.ProfessionSpellIndex = 0;
		m_Info.TargetSelectionFlag = SKILL_TARGETSEL_OBJECT;
		
		NiStrcpy(m_Info.ReleaseAnim,128, "Spell_001_001");
		NiStrcpy(m_Info.ReleaseEffect,128,"Effect_100001000.nif");
		m_Info.MaxRange = 3.0f;
		m_fTargetAreaParam0 = 0.0f;
		m_fTargetAreaParam1 = 0.0f;
		m_PrepareTime = 0;
		m_ReleaseTime = 1500;
		m_bResetSkillWhenEnd = FALSE;
	}else if(TypeID == SKILL_TYPE_IMPALE_ACCURATELY)
	{
		m_Info.Identity = SKILL_TYPE_IMPALE_ACCURATELY;
		NiStrcpy(m_Info.SkillName, SKILL_NAME_MAX, "百步穿杨");
		m_Info.Profession = CLASS_BOW;
		m_Info.ProfessionSpellIndex = 0;
		m_Info.TargetSelectionFlag = SKILL_TARGETSEL_OBJECT;
		m_Info.TargetAreaFlag = 0;
		NiStrcpy(m_Info.ReleaseAnim,128, "Spell_004_001");
		NiStrcpy(m_Info.ReleaseEffect,128,"PROJ#shot_dmg_01.nif");
		m_Info.MaxRange = 60.0f;
		//m_Info.NumReleaseSerial = 3;
		m_fTargetAreaParam0 = 0.0f;
		m_fTargetAreaParam1 = 0.0f;
		m_PrepareTime = 0;
		m_ReleaseTime = 1500;
		m_bResetSkillWhenEnd = FALSE;
	}else if (TypeID == SKILL_TYPE_ICE_TRAP)
	{
		m_Info.Identity = SKILL_TYPE_ICE_TRAP;
		NiStrcpy(m_Info.SkillName, SKILL_NAME_MAX, "冰霜陷阱");
		m_Info.Profession = CLASS_BOW;
		m_Info.ProfessionSpellIndex = 1;
		m_Info.TargetSelectionFlag = SKILL_TARGETSEL_AREA;
		m_Info.TargetAreaFlag = SKILL_TARGETAREA_ROUND;
		NiStrcpy(m_Info.TargetSelBrush, 128, "Texture\\Decal\\WOW_.dds");
		
		NiStrcpy(m_Info.ReleaseAnim,128, "Spell_004_002");
		NiStrcpy(m_Info.ReleaseEffect,128,"BreakSw1.nif");
		m_fTargetAreaParam0 = 4.0f;
		m_fTargetAreaParam1 = 0.0f;
		m_PrepareTime = 0;
		m_ReleaseTime = 4500;
		m_bResetSkillWhenEnd = TRUE;
	}
	
	
	//
}

CSkill::~CSkill(void)
{

}

BOOL CSkill::CheckCondition(CCharacter* Caller) const
{
	if(Caller->GetHP() < m_Info.HPCost)
	{
		// TODO Notify
		return FALSE;
	}
	if (Caller->GetMP() < m_Info.MPCost)
	{
		// TODO Notify
		return FALSE;
	}
	return TRUE;
}

BOOL CSkill::IsNeedSelectTargetObject(CPlayer* LocalPlayer) const 
{ 
	if(m_Info.TargetSelectionFlag == (BYTE)SKILL_TARGETSEL_SELF) 
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CSkill::IsNeedPrepareAttack() const
{
	if (m_Info.PrepareAnim[0] != 0 && m_PrepareTime > 0)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL CSkill::CanMoveWhenPrepareing() const
{
	if (m_Info.PrepareBrokenCondition & SKILL_BROKEN_MOVE)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL CSkill::CanMoveWhenAttacking() const
{
	if (m_Info.ReleaseFreezeTime <0.000001f)
	{
		return TRUE;
	}
	return FALSE;
}
BOOL CSkill::LocalBeginAttack(CPlayer* LocalPlayer) 
{
	NIASSERT(LocalPlayer && LocalPlayer->IsLocalPlayer());
	if (!CheckCondition(LocalPlayer))
	{
		return FALSE;
	}
	if (IsNeedSelectTargetObject(LocalPlayer))
	{
		if (SelectTarget(LocalPlayer))
		{
			return TRUE;
		}
		return FALSE;
	}
	return TRUE;
}


BOOL CSkill::SelectTarget(CCharacter* Caller) 
{
	// 对于非本地玩家, 不可能进行目标选择操作.
	CPlayer* LocalPlayer = Caller->GetAsPlayer();
	if (LocalPlayer == NULL || !LocalPlayer->IsLocalPlayer())
	{
		return FALSE;
	}
	// 本地玩家操作, 转发处理.
	SYState()->LocalPlayerInput->SelectTarget(m_Info.TargetSelectionFlag, m_Info.TargetAreaFlag, 5.0f,0.0f, m_Info.TargetSelBrush);
	// 返回真, 让攻击操作保留.
	return TRUE;
}

CEffectBase* CSkill::CreatePrepareEffect(CCharacter* Caller)
{
	CSceneEffect* PrepareEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect(m_Info.PrepareEffect);
	if (PrepareEffect)
	{
		PrepareEffect->AttachToSceneObject(Caller->GetGUID());
		PrepareEffect->SetMaxLife(m_PrepareTime*0.001f);
	}
	return NULL;
}

CEffectBase* CSkill::CreateReleaseEffect(CCharacter* Caller)
{


	INT EffectLife = m_ReleaseTime - m_PrepareTime;

	//INT EffectLife = m_ReleaseTime - m_PrepareTime - TimeOffset;

	if (EffectLife > 0)
	{
		CSceneEffect* ReleaseEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect(m_Info.ReleaseEffect);
		if (ReleaseEffect)
		{
			if (Caller->GetTargetObjectID() != -1 && m_Info.TargetSelectionFlag != SKILL_TARGETSEL_AREA)
			{
				// 攻击对象
				ReleaseEffect->AttachToSceneObject(Caller->GetGUID());
			}else if (Caller->GetTargetObjectID() == -1 && m_Info.TargetSelectionFlag != SKILL_TARGETSEL_OBJECT)
			{
				// 攻击区域
				ReleaseEffect->AttachToScene(Caller->GetAreaAttackCenter());
			}
			ReleaseEffect->SetMaxLife(EffectLife*0.001f);
		}
	}else if (m_ReleaseTime == 0)
	{
		CSceneEffect* ReleaseEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect(m_Info.ReleaseEffect);
		if (ReleaseEffect)
		{
			if (Caller->GetTargetObjectID() != -1 && m_Info.TargetSelectionFlag != SKILL_TARGETSEL_AREA)
			{
				// 攻击对象
				ReleaseEffect->AttachToSceneObject(Caller->GetGUID());
			}else if (Caller->GetTargetObjectID() == -1 && m_Info.TargetSelectionFlag != SKILL_TARGETSEL_OBJECT)
			{
				// 攻击区域
				ReleaseEffect->AttachToScene(Caller->GetAreaAttackCenter());
			}
			ReleaseEffect->SetMaxLife(EffectLife*0.001f);
		}
	}
	
	return NULL;
}


CEffectBase* CSkill::CreateProjectileReleaseEffect(CCharacter* Caller)
{
	ProjectileEffect* Handle = EffectMgr->CreateProjectile(PROJDESC_FIREBALL, Caller->GetGUID());
	if (Handle)
	{
		if (Caller->GetTargetObjectID() != -1 && m_Info.TargetSelectionFlag != SKILL_TARGETSEL_AREA)
		{
			// 攻击对象
			Handle->SetTraceTarget(0,Caller->GetTargetObjectID());
		}
	}
	return NULL;
}

CEffectBase* CSkill::CreateDamageEffect(CCharacter* Caller, CCharacter* Target)
{
	return NULL;
}

//////////////////////////////////////////////////////////////////////////
CEffectBase* Skill_FireBall::CreateReleaseEffect(CCharacter* Caller)
{
	// 创建火球冲击目标
	CCharacter* TargetObject = Caller->GetTargetObject();
	if (TargetObject == NULL)
	{
		return NULL;
	}
	ProjectileEffect* Handle = EffectMgr->CreateProjectile(PROJDESC_FIREBALL, Caller->GetGUID());
	if (Handle)
	{
		Handle->SetNumEntity(1);
		Handle->SetTraceTarget(0,Caller->GetTargetObjectID());
		Handle->Project();
		return Handle;
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////
CEffectBase* Skill_CutOfHost::CreateReleaseEffect(CCharacter* Caller)
{
	// 跟踪武器, 创建Trail. 其实这样做的效果不好, 还不如在MAX 中预处理的动画来得好看.

	NIASSERT(Caller);
	Caller->CreateWeaponTrailFX(1.0f, "Texture\\Effect\\Swoosh.tga");

	return NULL;
}

void Skill_CutOfHost::ClearReleaseEffect(CCharacter* Caller)
{
	if (Caller->GetWeaponTrailFX())
	{
		Caller->GetWeaponTrailFX()->Enable(FALSE);
	}
}

void Skill_CutOfHost::UpdateReleaseEffect(CCharacter* Caller, DWORD Tick)
{
	if (Caller->GetWeaponTrailFX())
	{
		Caller->GetWeaponTrailFX()->TraceTrail2(Tick*0.001f);
	}
}

//////////////////////////////////////////////////////////////////////////
CEffectBase* Skill_SuckMP::CreateReleaseEffect(CCharacter* Caller)
{
	NiNode* Root = Caller->GetSceneNode();
	Root = (NiNode*)Root->GetAt(0);
	LightingBlot* Blot = EffectMgr->CreateLightingBlot();

	Blot->SetFrameAni(4,4,9,10.01f);
	Blot->Create(1.1f, "Lighting2.tga");
	Blot->SetParent(Root);
	Blot->SetTargetPair(Caller->GetGUID(), Caller->GetTargetObjectID());
	Blot->SetMaxLife(-1.0F);
	Caller->m_AttackEffect[0] = Blot;
	return NULL;
}

void Skill_SuckMP::ClearReleaseEffect(CCharacter* Caller)
{
	NIASSERT(Caller->m_AttackEffect[0]);
	//NiDelete Caller->m_AttackEffect[0];
	Caller->m_AttackEffect[0]->Destroy();
	Caller->m_AttackEffect[0] = NULL;
}

void Skill_SuckMP::UpdateReleaseEffect(CCharacter* Caller, DWORD Tick)
{
	// managed.
}

CEffectBase* Skill_Quake_Shock::CreateReleaseEffect(CCharacter* Caller)
{
	INT EffectLife = m_ReleaseTime - m_PrepareTime;

	if (EffectLife > 0)
	{
		CSceneEffect* ReleaseEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect("Effect_100002000.nif");
		if (ReleaseEffect)
		{
		
				// 攻击对象
				ReleaseEffect->AttachToSceneObject(Caller->GetGUID());
				//ReleaseEffect->AttachToScene(Caller->GetAreaAttackCenter());
			
			ReleaseEffect->SetMaxLife(EffectLife*0.001f);
		}
	}

	SKILL_DATA* pUserData = NiNew SKILL_DATA((IStateProxy*)this);
	pUserData->Decal = NULL; 
	pUserData->fTime = 0.0f;
	CPlayer* pPlayer = (CPlayer*)Caller;
	pPlayer->SetStateUserData(pUserData);
	return NULL;
}

void Skill_Quake_Shock::UpdateReleaseEffect(CCharacter* Caller, DWORD Tick)
{
	CPlayer* pPlayer = (CPlayer*)Caller;
	SKILL_DATA* pUserData = (SKILL_DATA*)pPlayer->GetStateUserData();
	NIASSERT(pUserData);
	float DeltaTime = Tick*0.001f;
	pUserData->fTime += DeltaTime;
	if (pUserData->fTime >= 0.9f && pUserData->Decal == NULL)
	{
		DecalTerrain* pDecal = SceneMgr->GetDecalMgr()->AddDecalOnTerrain();
		if (pDecal) 
		{
			pDecal->SetArea(Caller->GetPosition(), 6.0f);
			pDecal->SetTexture("Objects\\Effect\\Effect_100002000.dds");
			pDecal->SetAngularVelocity(NI_PI);
			//pDecal->EnableFrameTexture(4,8,64,64,2,30);

			
			pUserData->m_fFrameTime = -DeltaTime;
			pUserData->m_nCurFrame = -1;
			pUserData->m_nFrameRate = 30;
			
			char szBuf[MAX_PATH];
			for (int i = 0; i < 27; i++)
			{
				NiSprintf(szBuf, MAX_PATH, "Objects\\Effect\\Effect_100002000_Decal\\%d.dds", 27 + i);
				pUserData->spTextures[i] = NiSourceTexture::Create(szBuf);
			}
		}

		pUserData->Decal = pDecal;
	}
	
	if (pUserData->Decal)
	{
		// update decal frame.
		pUserData->m_fFrameTime += DeltaTime;
		int NextFrame;
		NextFrame = (int)(pUserData->m_fFrameTime*(float)pUserData->m_nFrameRate);
		NextFrame = NextFrame % 27;
		
		if (NextFrame != pUserData->m_nCurFrame)
		{
			pUserData->m_nCurFrame = NextFrame;
			pUserData->Decal->SetTexture(pUserData->spTextures[NextFrame]);
		}
	}

}

void Skill_Quake_Shock::EndSkill(CCharacter* Caller)
{
	CPlayer* pPlayer = (CPlayer*)Caller;

	SKILL_DATA* pUserData = (SKILL_DATA*)pPlayer->GetStateUserData();
	if (pUserData)
	{
		NIASSERT(pUserData->m_pState == (IStateProxy*)this);
		//SceneMgr->GetDecalMgr()->ReleaseTerrainDecal(pUserData->Decal);
		NiDelete pUserData->Decal;
		pPlayer->ClearStateUserData();
	}
}
///

CEffectBase* Skill_HANDY_HEW::CreateReleaseEffect(CCharacter* Caller)
{
	INT EffectLife = m_ReleaseTime - m_PrepareTime;
	if (EffectLife > 0)
	{
		CSceneEffect* ReleaseEffect = (CSceneEffect*)EffectMgr->CreateSceneEffect(m_Info.ReleaseEffect);
		if (ReleaseEffect)
		{
			// 攻击对象
			NiFixedString HandNodeName("RightHand_Node");
			ReleaseEffect->AttachToSceneObject(Caller->GetGUID(), &HandNodeName);
			ReleaseEffect->SetMaxLife(2.5f);
		}
	}
	return NULL;
}

void Skill_HANDY_HEW::UpdateReleaseEffect(CCharacter* Caller, DWORD Tick)
{

}

void Skill_HANDY_HEW::EndSkill(CCharacter* Caller)
{

}

//////////////////////////////////////////////////////////////////////////

CEffectBase* Skill_Range_Base::CreateReleaseEffect(CCharacter* Caller)
{
	// 创建火球冲击目标
	CCharacter* TargetObject = Caller->GetTargetObject();
	if (TargetObject == NULL)
	{
		return NULL;
	}
	ProjectileEffect* Handle = EffectMgr->CreateProjectile(/*PROJDESC_FIREBALL*/PROJDESC_BLOWSHOT, Caller->GetGUID());
	if (Handle)
	{
		Handle->SetNumEntity(1);
		Handle->SetTraceTarget(0,Caller->GetTargetObjectID());
		Handle->Project();
		return Handle;
	}
	return NULL;
}

BOOL Skill_IMPALE_ACCURATELY::ProxyStart(IStateActor* Owner, DWORD CurTime)
{

	return TRUE;
}

BOOL Skill_IMPALE_ACCURATELY::ProxyEnd(IStateActor* Owner, DWORD CurTime)
{
	
	return TRUE;
}

EStateProcessResult Skill_IMPALE_ACCURATELY::ProxyProcess(IStateActor* Owner, DWORD dwTick)
{
	CCharacter* Char = (CCharacter*)Owner;
	ESpellStep Step = Char->GetAttackStep();
	DWORD Time = Char->GetAttackTime();
	Char->SetAttackTime(Time + dwTick);
	if (Step == LPAS_NONE)
	{
		return SPR_END;
	}

	AnimationEventSet AtkEvent;
//	float Ret = Char->UpdateCurAnimation(dwTick, &AtkEvent);
	
	if (Step == LPAS_PREPARE)
	{
	
			// 进入释放状态.
		//	ReleaseAttack(Char,Skill, Time - Skill->GetPrepareTime());
		   Attack(Char, Time);
			Char->SetAttackStep(LPAS_RELEASE);
			
			return SPR_SUSPEND;
		
	}

	if (Step == LPAS_RELEASE)
	{
		//DWORD FreezeTime = Skill->GetPrepareTime() + Skill->GetAttackFreezeTime();
		//Skill->UpdateReleaseEffect(Char, dwTick);
		
		if (Ret >= 0.99f)
		{
			Char->SetAttackStep(LPAS_END);
			ClearReleaseEffect(Char);
			return SPR_END;
		}
	}

	if (Step == LPAS_END)
	{
		//End(Caller);
	}



	return SPR_SUSPEND;
}

void Skill_IMPALE_ACCURATELY::Attack(CCharacter* Attacker, INT TimeOffset)
{
	// 动画
	ui32 sex = Attacker->GetGender();
	WORD Prof = GetProfession();
	WORD ProfIdx = GetProfessionIndex();
	char szAnim[128];

	NiSprintf(szAnim, 128, "Spell_%02d_%03d_%03d", sex + 1, Prof, ProfIdx + 1);
	Attacker->SetCurAnimation(szAnim /*Skill->GetAttackAnim()*/, TimeOffset);

	// 攻击效果
	CreateReleaseEffect(Attacker);
}

CEffectBase* Skill_IMPALE_ACCURATELY::CreateReleaseEffect(CCharacter* Caller)
{
	// 创建火球冲击目标
	CCharacter* TargetObject = Caller->GetTargetObject();
	if (TargetObject == NULL)
	{
		return NULL;
	}
	ProjectileEffect* Handle = EffectMgr->CreateProjectile(/*PROJDESC_FIREBALL*/PROJDESC_BLOWSHOT, Caller->GetGUID());
	if (Handle)
	{
		Handle->SetNumEntity(1);
		Handle->SetTraceTarget(0,Caller->GetTargetObjectID());
		Handle->Project();
		return Handle;
	}
	return NULL;
}

#endif 
