#include "StdAfx.h"
#include "SkillInfo.h"

#include "Character.h"
#include "Player.h"
#include "ClientApp.h"
#include "PlayerInputMgr.h"
#include "SceneManager.h"
#include "Effect/EffectManager.h"
#include "Effect\DecalManager.h"
#include "ObjectManager.h"

#include "Trail.h"
#include "PosConstraintEffect.h"

#include "ResourceManager.h"
#include "SkillManager.h"

//#include "SpellRush.h"

#include "Utils/SpellDB.h"


static UINT CastTime[9] = {0,3000,1500,1000,2500,500,2000,1250,5000};
SpellTemplate::SpellTemplate(const stSpellInfo& info)
{
	m_spellInfo = info;
}

SpellTemplate::~SpellTemplate()
{
}

bool SpellTemplate::GetICON(std::string& icon) {
	const SpellVisualDesc* pSpellVisualKit = GetVisulalKit();
	if (!pSpellVisualKit) return false;

	icon = pSpellVisualKit->m_Icon;
	return true;
}

bool SpellTemplate::GetBrush(std::string& brush){
	const SpellVisualDesc* pSpellVisualKit = GetVisulalKit();
	if (!pSpellVisualKit) return false;

	brush = pSpellVisualKit->m_Brush;
	return true;
}

float SpellTemplate::GetBrushHeight(float& fHeight)
{
	const SpellVisualDesc* pSpellVisualKit = GetVisulalKit();
	if (!pSpellVisualKit) 
		return false;

	fHeight = pSpellVisualKit->m_fBrushHeight;
	return fHeight;
}

BOOL SpellTemplate::CheckOwnerRequest(CCharacter* pOwner) const
{
	NIASSERT(pOwner);
	if (!pOwner) return FALSE;

	if(pOwner->IsDead())
	{
		EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("当前状态无法施法"));
		return FALSE;
	}

	if (m_spellInfo.uiManaCost > pOwner->GetMP())
	{
		EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("魔法不足"));
		return FALSE;
	}

	return TRUE;
}

BOOL SpellTemplate::IsTargetValid(UpdateObj* pTarget) const
{
	return TRUE;
}

BOOL SpellTemplate::IsTargetRelativeValid(UpdateObj* pAttacker, UpdateObj* pAttackee) const
{
	return TRUE;
}

BOOL SpellTemplate::IsNeedSelectTargetObject(CCharacter* pOwner) const
{
	if (GetSelectMethod() & TARGET_FLAG_SELF)
		return FALSE;

	return TRUE;
}

int SpellTemplate::GetSelectMethod() const
{
	return m_spellInfo.uiTargets;
}

const SpellVisualDesc* SpellTemplate::GetVisulalKit() const {
	if (m_spellInfo.uiVisualKit != INVALID_SPELL_VISUAL_ID)
	{
		return SYState()->SkillMgr->GetSpellVisual(m_spellInfo.uiVisualKit);
	}
	
	return 0;
}

const AuraVisualDesc* SpellTemplate::GetAuraVisulalKit() const {
	if (m_spellInfo.uiVisualKit != INVALID_SPELL_VISUAL_ID)
	{
		return SYState()->SkillMgr->GetAuraVisual(GetVisulalKit()->auraeffid);
	}
	
	return 0;
}

SpellSoundDesc* SpellTemplate::FindSoundDesc(int id)
{
	const SpellVisualDesc* pVisDesc = GetVisulalKit();
	if (pVisDesc)
	{
		const SpellEffectDesc& effDesc = pVisDesc->m_kEffects;
		for (unsigned int ui = 0; ui < effDesc.vSounds.size(); ui++)
		{
			if (effDesc.vSounds[ui]->id == id)
			{
				return effDesc.vSounds[ui];
			}
		}
	}

	const AuraVisualDesc* pAuraVisDesc = GetAuraVisulalKit();
	if (pAuraVisDesc)
	{
		const SpellEffectDesc& effDesc = pAuraVisDesc->m_kEffects;
		for (unsigned int ui = 0; ui < effDesc.vSounds.size(); ui++)
		{
			if (effDesc.vSounds[ui]->id == id)
			{
				return effDesc.vSounds[ui];
			}
		}
	}

	return 0;
}

BOOL SpellTemplate::SelectTarget(CCharacter* Caller) const 
{
	if (!Caller) return FALSE;

	CPlayer* pkLocalPlayer = Caller->GetAsPlayer();

	if (!pkLocalPlayer || !pkLocalPlayer->IsLocalPlayer())
		return FALSE;

	CPlayerInputMgr* pkLocalInput = SYState()->LocalPlayerInput;

	int iSpellTargetType = GetSelectMethod();
	if (iSpellTargetType & TARGET_FLAG_DEST_LOCATION)
	{
		float fRadius = GetRadius();
		float fHeight = 0.5f ;
		if (fRadius == 0.f)
		{
			return FALSE ;
		}

		SpellTemplate* pkST = SYState()->SkillMgr->GetSpellTemplate(GetSpellID());
		std::string brush;
		pkST->GetBrush(brush);
		pkST->GetBrushHeight(fHeight);
		pkLocalInput->SelectTarget(iSpellTargetType, 0, GetSpellID(), fRadius, brush.c_str(), fHeight);
	}

	return TRUE;
}

BOOL SpellTemplate::CheckRangeInvalid(unsigned int spellid, float fDist)
{
	float fMinRange, fMaxRange;
	fMinRange = fMaxRange = 0.0f;
	GetRange(spellid, fMinRange, fMaxRange);

	if ( fDist >= fMinRange && fMaxRange)
		return TRUE;

	return FALSE;
}

void SpellTemplate::GetRange(unsigned int spellid, float& minRange, float& maxRange)
{
	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	if (!pSpellInfo) return;

	SpellTemplate::GetTRange(spellid, minRange, maxRange);
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		pkLocal->SM_FF_Value(SMT_RANGE, &maxRange, pSpellInfo->uiSpellGroupType);
		pkLocal->SM_PF_Value(SMT_RANGE, &maxRange, pSpellInfo->uiSpellGroupType);
	}
}

void SpellTemplate::GetTRange(unsigned int spellid, float& minRange, float& maxRange)
{
	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return;

	stSpellRangeInfo* pSpellRange = g_pkSpellRangeInfo->GetSpellRangeInfo(pSpellInfo->uiRangeIndex);
	//NIASSERT(pSpellRange);
	if (!pSpellRange) return;

	minRange = (float)pSpellRange->minrange;
	maxRange = (float)pSpellRange->maxrange;
}

bool SpellTemplate::GetTDescription(std::string& str)
{
	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(m_spellInfo.uiId);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;

	str = pSpellInfo->description;
	return true;
}

void GetTimeStr(uint32 time, std::string& str)
{
	float realtime = float(time * 0.001f);
	int32 min = int32(realtime / 60.f);
	int32 sec = time % 60000;
	char buf[256] = {0};
	if (min){
		if (sec)	{
			sprintf(buf, "%d%s%.1f%s", min , _TRAN("分"), float(sec * 0.001f),  _TRAN("秒"));
		}else{
			sprintf(buf, "%d%s", min, _TRAN("分钟"));
		}
	}else if (sec){
		sprintf(buf, "%.1f%s",  float(sec * 0.001f), _TRAN("秒"));
	}
	str = buf;
}

bool SpellTemplate::GetDescription(std::string& str, bool bLocal)
{
	std::string desc;
	GetTDescription(desc);
	str = desc;

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
		return false;
	int Strbegin = 0;
	int StrEnd = 0;
	while ( true )
	{
		int pos1 = str.find("<#");
		Strbegin = pos1;
		if (pos1 != -1)
		{
			std::string tstr = str.substr( pos1,  desc.length());
			int pos2 = tstr.find('>');
			StrEnd = Strbegin + pos2 + 1;
			std::string strrbuff = tstr.substr(1, pos2 - 1);
			int smt = atoi(strrbuff.substr(1, strrbuff.length()).c_str());
			char buf[256];
			switch(smt)
			{
			case SMT_DURATION:
				{
					uint32 tvalue = 0;GetTDuration(tvalue);
					uint32 newvalue = 0;GetDuration(newvalue);
					int32 idur = newvalue - tvalue;
					std::string timestr;
					if (!bLocal){
						idur = 0;
						GetTimeStr(tvalue, timestr);
					}else{
						GetTimeStr(newvalue, timestr);
					}
					if (idur > 0)sprintf(buf, "<a:#1>%s</a>", timestr.c_str());
					else if (idur < 0)sprintf(buf, "<a:#1>%s</a>", timestr.c_str());
					else	sprintf(buf, "%s", timestr.c_str());
				}
				break;
			case SMT_RANGE:
				{
					float minrange = 0.f, maxrange = 0.f;
					float newminrange = 0.f, newmaxrange = 0.f;
					SpellTemplate::GetTRange(m_spellInfo.uiId, minrange, maxrange);
					SpellTemplate::GetRange(m_spellInfo.uiId, newminrange, newmaxrange);
					float frange = newmaxrange - maxrange;
					if (!bLocal) frange = 0.f;
					if (frange > 0.f)sprintf(buf, "%d%s%d<a:#1>(+%d)</a>", (int)minrange, _TRAN("到"), (int)maxrange, (int)frange);
					else if (frange < 0.f)sprintf(buf, "%d%s%d<a:#1>(%d)</a>", (int)minrange, _TRAN("到"), (int)maxrange, (int)frange);
					else	sprintf(buf, "%d%s%d", (int)minrange, _TRAN("到"), (int)maxrange);
				}
				break;
			case SMT_RADIUS:
				{	
					float Radius = GetTRadius() ;
					float newRadius = GetRadius();
					float fradius = newRadius - Radius;
					if (!bLocal) fradius = 0.f;
					if (fradius > 0.f)sprintf(buf, "%d<a:#1>(+%d)</a>", (int)Radius, (int)fradius);
					else if (fradius < 0.f)sprintf(buf, "%d<a:#1>(%d)</a>", (int)Radius, (int)fradius);
					else	sprintf(buf, "%d", (int)Radius);
				}
				break;
			case SMT_CAST_TIME:
				{
					int32 casttime = GetTCastTime();
					int32 newcasttime = GetCastTime();
					int32 icasttime = newcasttime - casttime;
					if (!bLocal) icasttime = 0;
					if (icasttime > 0)sprintf(buf, "%.2f<a:#1>(+%.1f)</a>", float(casttime * 0.001f), float(icasttime * 0.001f));
					else if (icasttime < 0)sprintf(buf, "%.2f<a:#1>(%.1f)</a>", float(casttime * 0.001f), float(icasttime * 0.001f));
					else sprintf(buf, "%.2f", float(casttime * 0.001f));
				}
				break;
			case SMT_COOLDOWN_DECREASE:
				{
					int32 recoverTime = GetTRecoverTime();
					int32 newrecovertime  = GetRecoverTime();
					int32 irecovertime = newrecovertime - recoverTime;
					if (!bLocal) irecovertime = 0;
					if (irecovertime > 0)sprintf(buf, "%.2f<a:#1>(+%.2f)</a>", float(recoverTime * 0.001f), float(irecovertime * 0.001f));
					else if (irecovertime < 0)sprintf(buf, "%.2f<a:#1>(%.2f)</a>", float(recoverTime * 0.001f), float(irecovertime * 0.001f));
					else	sprintf(buf, "%.2f", float(recoverTime * 0.001f));
				}
				break;
			case SMT_COST:
				{
					uint32 tvalue = 0;GetTMPRequest(tvalue);
					uint32 newvalue = 0;GetMPRequest(newvalue);
					int32 imprequest = newvalue - tvalue;
					if (!bLocal) imprequest = 0;
					if (imprequest > 0)sprintf(buf, "%u<a:#1>(+%d)</a>", tvalue,  imprequest);
					else if (imprequest < 0)sprintf(buf, "%u<a:#1>(%d)</a>", tvalue, imprequest);
					else	sprintf(buf, "%u", tvalue );
				}
				break;
			case SMT_SPELL_VALUE0:
				{
					unsigned int basevalue = 0;
					unsigned int miscvalue = 0;
					GetTSpellPoint(0, basevalue, miscvalue);
					unsigned int  newbasevalue = 0;
					GetSpellPoint(0, newbasevalue, miscvalue);
					int32 ipoint = newbasevalue - basevalue;
					if (!bLocal) ipoint = 0;
					if (miscvalue)
					{
						if (ipoint > 0)sprintf(buf, "%u%s%u<a:#1>(+%d)</a>",basevalue, _TRAN("到"), basevalue + miscvalue, ipoint);
						else if (ipoint < 0)sprintf(buf, "%u%s%u<a:#1>(%d)</a>", basevalue, _TRAN("到"), basevalue + miscvalue, ipoint);
						else	sprintf(buf, "%u%s%u", basevalue, _TRAN("到"), basevalue + miscvalue);
					}else
					{
						if (ipoint > 0)sprintf(buf, "%u<a:#1>(+%d)</a>", basevalue, ipoint);
						else if (ipoint < 0)sprintf(buf, "%u<a:#1>(%d)</a>", basevalue, ipoint);
						else	sprintf(buf, "%u", basevalue);
					}
				}
				break;
			case SMT_SPELL_VALUE1:
				{
					unsigned int basevalue = 0;
					unsigned int miscvalue = 0;
					GetTSpellPoint(1, basevalue, miscvalue);
					unsigned int  newbasevalue = 0;
					GetSpellPoint(1, newbasevalue, miscvalue);
					int32 ipoint = newbasevalue - basevalue;
					if (!bLocal) ipoint = 0;
					if (miscvalue)
					{
						if (ipoint > 0)sprintf(buf, "%u%s%u<a:#1>(+%d)</a>",basevalue, _TRAN("到"), basevalue + miscvalue, ipoint);
						else if (ipoint < 0)sprintf(buf, "%u%s%u<a:#1>(%d)</a>", basevalue, _TRAN("到"), basevalue + miscvalue, ipoint);
						else	sprintf(buf, "%u%s%u", basevalue, _TRAN("到"), basevalue + miscvalue);
					}else
					{
						if (ipoint > 0)sprintf(buf, "%u<a:#1>(+%d)</a>", basevalue, ipoint);
						else if (ipoint < 0)sprintf(buf, "%u<a:#1>(%d)</a>", basevalue, ipoint);
						else	sprintf(buf, "%u", basevalue);
					}
				}
				break;
			case SMT_SPELL_VALUE2:
				{
					unsigned int basevalue = 0;
					unsigned int miscvalue = 0;
					GetTSpellPoint(2, basevalue, miscvalue);
					unsigned int  newbasevalue = 0;
					GetSpellPoint(2, newbasevalue, miscvalue);
					int32 ipoint = newbasevalue - basevalue;
					if (!bLocal) ipoint = 0;
					if (miscvalue)
					{
						if (ipoint > 0)sprintf(buf, "%u%s%u<a:#1>(+%d)</a>",basevalue, _TRAN("到"), basevalue + miscvalue, ipoint);
						else if (ipoint < 0)sprintf(buf, "%u%s%u<a:#1>(%d)</a>", basevalue, _TRAN("到"), basevalue + miscvalue, ipoint);
						else	sprintf(buf, "%u%s%u", basevalue, _TRAN("到"), basevalue + miscvalue);
					}else
					{
						if (ipoint > 0)sprintf(buf, "%u<a:#1>(+%d)</a>", basevalue, ipoint);
						else if (ipoint < 0)sprintf(buf, "%u<a:#1>(%d)</a>", basevalue, ipoint);
						else	sprintf(buf, "%u", basevalue);
					}
				}
				break;
			//case SMT_SPELL_VALUE_PCT:
			//	{
			//		unsigned int aurapoint = GetTAuraPoint();
			//		unsigned int newaurapoint = GetAuraPoint();
			//		int32 ipoint = newaurapoint - aurapoint;
			//		if (ipoint > 0)sprintf(buf, "%u(+%d)", aurapoint, ipoint);
			//		else if (ipoint < 0)sprintf(buf, "%u(%d)", aurapoint, ipoint);
			//		else	sprintf(buf, "%u", aurapoint);
			//	}
			//	break;
			case SMT_ADDITIONAL_TARGET:
				{
					uint32 tvalue = GetTTargetNum();
					uint32 newvalue = GetTargetNum();
					int32 imTarget = newvalue - tvalue;
					if (!bLocal) imTarget = 0;
					if (imTarget > 0)sprintf(buf, "%u<a:#1>(+%d)</a>", tvalue,  imTarget);
					else if (imTarget < 0)sprintf(buf, "%u<a:#1>(%d)</a>", tvalue, imTarget);
					else	sprintf(buf, "%u", tvalue );
				}
				break;
			case SMT_STACK_COUNT:
				{
					uint32 tvalue = GetTMaxStack();
					uint32 newvalue = GetMaxStack();
					int32 imstackcount = newvalue - tvalue;
					if (!bLocal) imstackcount = 0;
					if (imstackcount > 0)sprintf(buf, "%u<a:#1>(+%d)</a>", tvalue,  imstackcount);
					else if (imstackcount < 0)sprintf(buf, "%u<a:#1>(%d)</a>", tvalue, imstackcount);
					else	sprintf(buf, "%u", tvalue );
				}
				break;
			default:
				sprintf(buf, "");
				break;
			}
			str.replace(Strbegin, StrEnd - Strbegin, buf, strlen(buf));
		}else
			break;
	}
	str = "<linkcolor:00FF00>" + str;
	return true;
}

//uint32 SpellTemplate::GetAuraPoint()
//{
//	uint32 aurapoint = GetTAuraPoint();
//	return aurapoint;
//}
//
//uint32 SpellTemplate::GetTAuraPoint()
//{
//	uint32 aurapoint = 0;
//	for(int i = 0 ; i < 3 ; ++i)
//	{
//		if (m_spellInfo.uiEffect[i] == SPELL_EFFECT_APPLY_AURA)
//		{
//			if (m_spellInfo.uiEffectApplyAuraName[i] == SPELL_AURA_PERIODIC_DAMAGE)
//			{
//				aurapoint = m_spellInfo.uiEffectBasePoint[i];
//				break;
//			}
//		}
//	}
//	return aurapoint;
//}

bool SpellTemplate::GetSpellPoint(int TypePos, unsigned int& basepoint, unsigned int& miscpoint)
{
	if (GetTSpellPoint(TypePos, basepoint, miscpoint))
	{
		CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
		if (pkLocal)
		{
			switch(TypePos)
			{
			case 0:
				pkLocal->SM_FI_Value(SMT_SPELL_VALUE0, (int32*)&basepoint, GetSpellGroup());
				pkLocal->SM_PI_Value(SMT_SPELL_VALUE0, (int32*)&basepoint, GetSpellGroup());
				break;
			case 1:
				pkLocal->SM_FI_Value(SMT_SPELL_VALUE1, (int32*)&basepoint, GetSpellGroup());
				pkLocal->SM_PI_Value(SMT_SPELL_VALUE1, (int32*)&basepoint, GetSpellGroup());
				break;
			case 2:
				pkLocal->SM_FI_Value(SMT_SPELL_VALUE2, (int32*)&basepoint, GetSpellGroup());
				pkLocal->SM_PI_Value(SMT_SPELL_VALUE2, (int32*)&basepoint, GetSpellGroup());
				break;
			}
		}
		return true;
	}
	return false;
}

bool SpellTemplate::GetTSpellPoint(int TypePos, unsigned int& basepoint, unsigned int& miscpoint)
{
	//for(int i = 0 ; i < 3 ; ++i)
	//{
	//	if (m_spellInfo.uiEffect[i] == SPELL_EFFECT_SCHOOL_DAMAGE ||
	//		m_spellInfo.uiEffect[i] == SPELL_EFFECT_HEAL)
	//	{
	basepoint = m_spellInfo.uiEffectBasePoint[TypePos];
	miscpoint = m_spellInfo.uiEffectDieSides[TypePos];
	return true;
	//	}
	//}
	return false;
}

uint32 SpellTemplate::GetMaxStack()
{
	uint32 maxstack = GetTMaxStack();

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		pkLocal->SM_FI_Value(SMT_STACK_COUNT, (int32*)&maxstack, GetSpellGroup());
		pkLocal->SM_PI_Value(SMT_STACK_COUNT, (int32*)&maxstack, GetSpellGroup());
	}
	return maxstack;
}

uint32 SpellTemplate::GetTMaxStack()
{
	return m_spellInfo.MaxStack;
}

uint32 SpellTemplate::GetTargetNum()
{
	uint32 maxtarget = GetTTargetNum();

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		pkLocal->SM_FI_Value(SMT_ADDITIONAL_TARGET, (int32*)&maxtarget, GetSpellGroup());
		pkLocal->SM_PI_Value(SMT_ADDITIONAL_TARGET, (int32*)&maxtarget, GetSpellGroup());
	}
	return maxtarget;
}

uint32 SpellTemplate::GetTTargetNum()
{
	uint32 maxtarget = 0;
	for(int i = 0 ; i < 3 ; ++i)
	{
		maxtarget = max(m_spellInfo.uiEffectChainTarget[i], maxtarget);
	}
	return maxtarget;
}

bool SpellTemplate::GetMPRequest(unsigned int& uiValue) const
{
	if (GetTMPRequest(uiValue))
	{
		CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
		if (pkLocal)
		{
			pkLocal->SM_FI_Value(SMT_COST, (int32*)&uiValue, GetSpellGroup());
			pkLocal->SM_PI_Value(SMT_COST, (int32*)&uiValue, GetSpellGroup());
		}
	}

	if (uiValue < 0)
		uiValue = 0;

	return true;
}

bool SpellTemplate::GetTMPRequest(unsigned int& uiValue) const
{
	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(m_spellInfo.uiId);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;

	int32 T_value = pSpellInfo->uiManaCost;
	uiValue = T_value;

	unsigned int uiMPRequestPer = 0;
	CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
	if (!pkPlayer)
		return true;

	if(GetMpRequestPercentage(uiMPRequestPer))
	{
		T_value += unsigned int(float(pkPlayer->GetInt32Value(UNIT_FIELD_BASE_MANA) * uiMPRequestPer) / 100.f + 0.49f);
	}

	if (T_value < 0)
		T_value = 0;

	uiValue = T_value;
	return true;
}

bool SpellTemplate::GetMpRequestPercentage(unsigned int& uiValue) const
{
	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(m_spellInfo.uiId);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;

	uiValue = pSpellInfo->uiManaCostPercentage;

	return true;
}

bool SpellTemplate::GetDuration(unsigned int&durtime)
{
	if ( GetTDuration( durtime ) )
	{
		CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
		if (pkLocal)
		{
			pkLocal->SM_FI_Value(SMT_DURATION, (int32*)&durtime, GetSpellGroup());
			pkLocal->SM_PI_Value(SMT_DURATION, (int32*)&durtime, GetSpellGroup());
		}
		return true;
	}
	return false;
}

bool SpellTemplate::GetTDuration(unsigned int&durtime)
{
	stSpellDurationInfo* pSpellDI = g_pkSpellDurationInfo->GetSpellDurationInfo(m_spellInfo.uiDurationIndex);
	NIASSERT(pSpellDI);
	if(!pSpellDI) return false;

	durtime = pSpellDI->duration1;
	return true;
}

uint32 SpellTemplate::GetRecoverTime()
{
	uint32 recoverTime = GetTRecoverTime();
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		pkLocal->SM_FI_Value(SMT_COOLDOWN_DECREASE, (int32*)&recoverTime, GetSpellGroup());
		pkLocal->SM_PI_Value(SMT_COOLDOWN_DECREASE, (int32*)&recoverTime, GetSpellGroup());
	}
	return recoverTime;
}

uint32 SpellTemplate::GetTRecoverTime()
{
	return m_spellInfo.uiRecoverTime;
}

float SpellTemplate::GetTRadius() const
{
	float fRadius = 0.0f;
	//if (GetSelectMethod() & TARGET_FLAG_DEST_LOCATION ||
	//	GetSelectMethod() & TARGET_FLAG_SOURCE_LOCATION)
	{
		for (int i = 0; i < 3; i++)
		{
			int index = m_spellInfo.uiEffectRadius[i] ;
			float Radius = 0.0f ;
			if (g_pkSpellRadiusInfo->GetRadius(index, Radius))
			{
				if (Radius > fRadius)
				{
					fRadius = Radius ;
				}
			}
		}
	}
	return fRadius;
}

uint32 SpellTemplate::GetTCastTime()
{
	uint32 Index = GetCastIndex() - 1;
	int32 casttime = CastTime[Index];
	if (casttime < 0)
		casttime = 0;

	return (uint32)casttime;
}

uint32 SpellTemplate::GetCastTime()
{
	int32 casttime = GetTCastTime();

	CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
	if (pkPlayer)
	{
		pkPlayer->SM_FI_Value(SMT_CAST_TIME, &casttime, GetSpellGroup());
		pkPlayer->SM_PI_Value(SMT_CAST_TIME, &casttime, GetSpellGroup());
	}
	if (casttime < 0)
		casttime = 0;

	return (uint32)casttime;
}

float SpellTemplate::GetRadius() const 
{
	float fRadius = GetTRadius();
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if(pkLocal)
	{
		pkLocal->SM_FF_Value(SMT_RADIUS, &fRadius, GetSpellGroup());
		pkLocal->SM_PF_Value(SMT_RADIUS, &fRadius, GetSpellGroup());
	}
	return fRadius;
}

void SpellTemplate::AddCooldown()
{
	
	unsigned int pSpellGroup = m_spellInfo.uiSpellGroupType;
	if (/*m_spellInfo.uiCategory && m_spellInfo.uiCategoryRecoveryTime*/pSpellGroup > 0)
	{
		/*SYState()->SkillMgr->AddCooldown(COOLDOWN_TYPE_CATEGORY, m_spellInfo.uiCategory, m_spellInfo.uiCategoryRecoveryTime,
			m_spellInfo.uiId, 0);*/
		SYState()->SkillMgr->AddGroupCooldown(COOLDOWN_TYPE_CATEGORY,GetRecoverTime()/*m_spellInfo.uiCategoryRecoveryTime*/,pSpellGroup);
	}

	if (GetRecoverTime() > 0)
	{
		SYState()->SkillMgr->AddCooldown(COOLDOWN_TYPE_SPELL, m_spellInfo.uiId, GetRecoverTime(), m_spellInfo.uiId, 0);
		//SYState()->SkillMgr->AddGroupCooldown(COOLDOWN_TYPE_SPELL,m_spellInfo.uiRecoverTime,pSpellGroup);
	}

	//改技能对应的组
	

}

void SpellTemplate::AddStartCooldown()
{
	CPlayerLocal* pkLP = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLP);
	if (!pkLP)
		return;

	if (m_spellInfo.uiStartRecoveryTime == 0)
		return;

	int32 atime = float2int32((float)m_spellInfo.uiStartRecoveryTime * pkLP->GetFloatValue(UNIT_MOD_CAST_SPEED));
	if( atime <= 0 )
		return;
	if( atime >= 1500 )
		atime = 1500;

	atime += pkLP->m_increasespellcdt[m_spellInfo.uiId];
	//SYState()->SkillMgr->AddGlobalCooldown(atime);
}

const char* SpellTemplate::GetSpellErrorString(uint8 uiError)
{
	switch (uiError)
	{
	case SPELL_FAILED_AFFECTING_COMBAT:				return "SPELL_FAILED_AFFECTING_COMBAT";
	case SPELL_FAILED_ALREADY_AT_FULL_HEALTH:		return "技能失败：生命值已满";
	case SPELL_FAILED_ALREADY_AT_FULL_MANA:			return "技能失败：魔法值已满";
	case SPELL_FAILED_ALREADY_AT_FULL_POWER:		return "SPELL_FAILED_ALREADY_AT_FULL_POWER";
	case SPELL_FAILED_ALREADY_BEING_TAMED:			return "SPELL_FAILED_ALREADY_BEING_TAMED";
	case SPELL_FAILED_ALREADY_HAVE_CHARM:			return "SPELL_FAILED_ALREADY_HAVE_CHARM";
	case SPELL_FAILED_ALREADY_HAVE_SUMMON:			return "SPELL_FAILED_ALREADY_HAVE_SUMMON";
	case SPELL_FAILED_ALREADY_OPEN:					return "SPELL_FAILED_ALREADY_OPEN";
	case SPELL_FAILED_AURA_BOUNCED:					return "SPELL_FAILED_AURA_BOUNCED";
	case SPELL_FAILED_AUTOTRACK_INTERRUPTED:		return "SPELL_FAILED_AUTOTRACK_INTERRUPTED";
	case SPELL_FAILED_BAD_IMPLICIT_TARGETS:			return "SPELL_FAILED_BAD_IMPLICIT_TARGETS";
	case SPELL_FAILED_BAD_TARGETS:					return "技能失败：错误的目标";
	case SPELL_FAILED_CANT_BE_CHARMED:				return "SPELL_FAILED_CANT_BE_CHARMED";
	case SPELL_FAILED_CANT_BE_DISENCHANTED:			return "SPELL_FAILED_CANT_BE_DISENCHANTED";
	case SPELL_FAILED_CANT_BE_DISENCHANTED_SKILL:	return "SPELL_FAILED_CANT_BE_DISENCHANTED_SKILL";
	case SPELL_FAILED_CANT_BE_PROSPECTED:			return "SPELL_FAILED_CANT_BE_PROSPECTED";
	case SPELL_FAILED_CANT_CAST_ON_TAPPED:			return "SPELL_FAILED_CANT_CAST_ON_TAPPED";
	case SPELL_FAILED_CANT_DUEL_WHILE_INVISIBLE:	return "SPELL_FAILED_CANT_DUEL_WHILE_INVISIBLE";
	case SPELL_FAILED_CANT_DUEL_WHILE_STEALTHED:	return "SPELL_FAILED_CANT_DUEL_WHILE_STEALTHED";
	case SPELL_FAILED_CANT_STEALTH:					return "SPELL_FAILED_CANT_STEALTH";
	case SPELL_FAILED_CASTER_AURASTATE:				return "SPELL_FAILED_CASTER_AURASTATE";
	case SPELL_FAILED_CASTER_DEAD:					return "技能失败：你已经无法战斗";
	case SPELL_FAILED_CHARMED:						return "SPELL_FAILED_CHARMED";
	case SPELL_FAILED_CHEST_IN_USE:					return "SPELL_FAILED_CHEST_IN_USE";
	case SPELL_FAILED_CONFUSED:						return "SPELL_FAILED_CONFUSED";
	case SPELL_FAILED_DONT_REPORT:					return "SPELL_FAILED_DONT_REPORT";
	case SPELL_FAILED_EQUIPPED_ITEM:				return "SPELL_FAILED_EQUIPPED_ITEM";
	case SPELL_FAILED_EQUIPPED_ITEM_CLASS:			return "SPELL_FAILED_EQUIPPED_ITEM_CLASS";
	case SPELL_FAILED_EQUIPPED_ITEM_CLASS_MAINHAND:	return "SPELL_FAILED_EQUIPPED_ITEM_CLASS_MAINHAND";
	case SPELL_FAILED_EQUIPPED_ITEM_CLASS_OFFHAND:	return "SPELL_FAILED_EQUIPPED_ITEM_CLASS_OFFHAND";
	case SPELL_FAILED_ERROR:						return "SPELL_FAILED_ERROR";
	case SPELL_FAILED_FIZZLE:						return "SPELL_FAILED_FIZZLE";
	case SPELL_FAILED_FLEEING:						return "SPELL_FAILED_FLEEING";
	case SPELL_FAILED_FOOD_LOWLEVEL:				return "SPELL_FAILED_FOOD_LOWLEVEL";
	case SPELL_FAILED_HIGHLEVEL:					return "SPELL_FAILED_HIGHLEVEL";
	case SPELL_FAILED_HUNGER_SATIATED:				return "SPELL_FAILED_HUNGER_SATIATED";
	case SPELL_FAILED_IMMUNE:						return "";
	case SPELL_FAILED_INTERRUPTED:					return "技能失败：被打断";
	case SPELL_FAILED_INTERRUPTED_COMBAT:			return "技能失败：进入战斗被打断";
	case SPELL_FAILED_ITEM_ALREADY_ENCHANTED:		return "SPELL_FAILED_ITEM_ALREADY_ENCHANTED";
	case SPELL_FAILED_ITEM_GONE:					return "SPELL_FAILED_ITEM_GONE";
	case SPELL_FAILED_ITEM_NOT_FOUND:				return "合成道具缺失";
	case SPELL_FAILED_ITEM_NOT_READY:				return "SPELL_FAILED_ITEM_NOT_READY";
	case SPELL_FAILED_LEVEL_REQUIREMENT:			return "SPELL_FAILED_LEVEL_REQUIREMENT";
	case SPELL_FAILED_LINE_OF_SIGHT:				return "目标不在视野中";
	case SPELL_FAILED_LOWLEVEL:						return "目标等级太低了";
	case SPELL_FAILED_LOW_CASTLEVEL:				return "SPELL_FAILED_LOW_CASTLEVEL";
	case SPELL_FAILED_MAINHAND_EMPTY:				return "SPELL_FAILED_MAINHAND_EMPTY";
	case SPELL_FAILED_MOVING:						return "SPELL_FAILED_MOVING";
	case SPELL_FAILED_NEED_AMMO:					return "SPELL_FAILED_NEED_AMMO";
	case SPELL_FAILED_NEED_AMMO_POUCH:				return "SPELL_FAILED_NEED_AMMO_POUCH";
	case SPELL_FAILED_NEED_EXOTIC_AMMO:				return "SPELL_FAILED_NEED_EXOTIC_AMMO";
	case SPELL_FAILED_NOPATH:						return "SPELL_FAILED_NOPATH";
	case SPELL_FAILED_NOT_BEHIND:					return "SPELL_FAILED_NOT_BEHIND";
	case SPELL_FAILED_NOT_FISHABLE:					return "SPELL_FAILED_NOT_FISHABLE";
	case SPELL_FAILED_NOT_FLYING:					return "SPELL_FAILED_NOT_FLYING";
	case SPELL_FAILED_NOT_HERE:						return "技能失败：目标不在这里。";
	case SPELL_FAILED_NOT_INFRONT:					return "技能失败：目标不在面前。";
	case SPELL_FAILED_NOT_IN_CONTROL:				return "SPELL_FAILED_NOT_IN_CONTROL";
	case SPELL_FAILED_NOT_KNOWN:					return "SPELL_FAILED_NOT_KNOWN";
	case SPELL_FAILED_NOT_MOUNTED:					return "技能失败：不在骑乘状态。";
	case SPELL_FAILED_NOT_ON_TAXI:					return "SPELL_FAILED_NOT_ON_TAXI";
	case SPELL_FAILED_NOT_ON_TRANSPORT:				return "SPELL_FAILED_NOT_ON_TRANSPORT";
	case SPELL_FAILED_NOT_READY:					return "技能失败：您还不能这样做。";
	case SPELL_FAILED_NOT_SHAPESHIFT:				return "SPELL_FAILED_NOT_SHAPESHIFT";
	case SPELL_FAILED_NOT_STANDING:					return "SPELL_FAILED_NOT_STANDING";
	case SPELL_FAILED_NOT_TRADEABLE:				return "SPELL_FAILED_NOT_TRADEABLE";
	case SPELL_FAILED_NOT_TRADING:					return "SPELL_FAILED_NOT_TRADING";
	case SPELL_FAILED_NOT_UNSHEATHED:				return "SPELL_FAILED_NOT_UNSHEATHED";
	case SPELL_FAILED_NOT_WHILE_GHOST:				return "SPELL_FAILED_NOT_WHILE_GHOST";
	case SPELL_FAILED_NO_AMMO:						return "SPELL_FAILED_NO_AMMO";
	case SPELL_FAILED_NO_CHARGES_REMAIN:			return "SPELL_FAILED_NO_CHARGES_REMAIN";
	case SPELL_FAILED_NO_CHAMPION:					return "SPELL_FAILED_NO_CHAMPION";
	case SPELL_FAILED_NO_COMBO_POINTS:				return "SPELL_FAILED_NO_COMBO_POINTS";
	case SPELL_FAILED_NO_DUELING:					return "SPELL_FAILED_NO_DUELING";
	case SPELL_FAILED_NO_ENDURANCE:					return "SPELL_FAILED_NO_ENDURANCE";
	case SPELL_FAILED_NO_FISH:						return "SPELL_FAILED_NO_FISH";
	case SPELL_FAILED_NO_ITEMS_WHILE_SHAPESHIFTED:	return "SPELL_FAILED_NO_ITEMS_WHILE_SHAPESHIFTED";
	case SPELL_FAILED_NO_MOUNTS_ALLOWED:			return "技能失败：骑乘状态不允许使用。";
	case SPELL_FAILED_NO_PET:						return "SPELL_FAILED_NO_PET";
	case SPELL_FAILED_NO_POWER:						return "技能失败：魔法不足。";
	case SPELL_FAILED_NOTHING_TO_DISPEL:			return "SPELL_FAILED_NOTHING_TO_DISPEL";
	case SPELL_FAILED_NOTHING_TO_STEAL:				return "SPELL_FAILED_NOTHING_TO_STEAL";
	case SPELL_FAILED_ONLY_ABOVEWATER:				return "SPELL_FAILED_ONLY_ABOVEWATER";
	case SPELL_FAILED_ONLY_DAYTIME:					return "SPELL_FAILED_ONLY_DAYTIME";
	case SPELL_FAILED_ONLY_INDOORS:					return "SPELL_FAILED_ONLY_INDOORS";
	case SPELL_FAILED_ONLY_MOUNTED:					return "SPELL_FAILED_ONLY_MOUNTED";
	case SPELL_FAILED_ONLY_NIGHTTIME:				return "SPELL_FAILED_ONLY_NIGHTTIME";
	case SPELL_FAILED_ONLY_OUTDOORS:				return "SPELL_FAILED_ONLY_OUTDOORS";
	case SPELL_FAILED_ONLY_SHAPESHIFT:				return "SPELL_FAILED_ONLY_SHAPESHIFT";
	case SPELL_FAILED_ONLY_STEALTHED:				return "SPELL_FAILED_ONLY_STEALTHED";
	case SPELL_FAILED_ONLY_UNDERWATER:				return "SPELL_FAILED_ONLY_UNDERWATER";
	case SPELL_FAILED_OUT_OF_RANGE:					return "技能失败：目标距离太远。";
	case SPELL_FAILED_PACIFIED:						return "SPELL_FAILED_PACIFIED";
	case SPELL_FAILED_POSSESSED:					return "SPELL_FAILED_POSSESSED";
	case SPELL_FAILED_REAGENTS:						return "SPELL_FAILED_REAGENTS";
	case SPELL_FAILED_REQUIRES_AREA:				return "SPELL_FAILED_REQUIRES_AREA";
	case SPELL_FAILED_REQUIRES_SPELL_FOCUS:			return "SPELL_FAILED_REQUIRES_SPELL_FOCUS";
	case SPELL_FAILED_ROOTED:						return "SPELL_FAILED_ROOTED";
	case SPELL_FAILED_SILENCED:						return "技能失败：沉默状态无法施法。";
	case SPELL_FAILED_SPELL_IN_PROGRESS:			return "技能失败：另一个技能在使用中。";
	case SPELL_FAILED_SPELL_LEARNED:				return "SPELL_FAILED_SPELL_LEARNED";
	case SPELL_FAILED_SPELL_UNAVAILABLE:			return "技能失败：此时无法使用。";
	case SPELL_FAILED_STUNNED:						return "技能失败：眩晕状态无法施法。";
	case SPELL_FAILED_TARGETS_DEAD:					return "技能失败：目标已经无法战斗。";
	case SPELL_FAILED_TARGET_AFFECTING_COMBAT:		return "SPELL_FAILED_TARGET_AFFECTING_COMBAT";
	case SPELL_FAILED_TARGET_AURASTATE:				return "SPELL_FAILED_TARGET_AURASTATE";
	case SPELL_FAILED_TARGET_DUELING:				return "SPELL_FAILED_TARGET_DUELING";
	case SPELL_FAILED_TARGET_ENEMY:					return "技能失败：目标敌对状态。";
	case SPELL_FAILED_TARGET_ENRAGED:				return "SPELL_FAILED_TARGET_ENRAGED";
	case SPELL_FAILED_TARGET_FRIENDLY:				return "技能失败：目标友好状态。";
	case SPELL_FAILED_TARGET_IN_COMBAT:				return "SPELL_FAILED_TARGET_IN_COMBAT";
	case SPELL_FAILED_TARGET_IS_PLAYER:				return "SPELL_FAILED_TARGET_IS_PLAYER";
	case SPELL_FAILED_TARGET_IS_PLAYER_CONTROLLED:	return "SPELL_FAILED_TARGET_IS_PLAYER_CONTROLLED";
	case SPELL_FAILED_TARGET_NOT_DEAD:				return "技能失败：目标还能继续战斗。";
	case SPELL_FAILED_TARGET_NOT_IN_PARTY:			return "SPELL_FAILED_TARGET_NOT_IN_PARTY";
	case SPELL_FAILED_TARGET_NOT_LOOTED:			return "SPELL_FAILED_TARGET_NOT_LOOTED";
	case SPELL_FAILED_TARGET_NOT_PLAYER:			return "SPELL_FAILED_TARGET_NOT_PLAYER";
	case SPELL_FAILED_TARGET_NO_POCKETS:			return "SPELL_FAILED_TARGET_NO_POCKETS";
	case SPELL_FAILED_TARGET_NO_WEAPONS:			return "SPELL_FAILED_TARGET_NO_WEAPONS";
	case SPELL_FAILED_TARGET_UNSKINNABLE:			return "SPELL_FAILED_TARGET_UNSKINNABLE";
	case SPELL_FAILED_THIRST_SATIATED:				return "SPELL_FAILED_THIRST_SATIATED";
	case SPELL_FAILED_TOO_CLOSE:					return "SPELL_FAILED_TOO_CLOSE";
	case SPELL_FAILED_TOO_MANY_OF_ITEM:				return "SPELL_FAILED_TOO_MANY_OF_ITEM";
	case SPELL_FAILED_TOTEM_CATEGORY:				return "SPELL_FAILED_TOTEM_CATEGORY";
	case SPELL_FAILED_TOTEMS:						return "缺少所需要的道具";
	case SPELL_FAILED_TRAINING_POINTS:				return "SPELL_FAILED_TRAINING_POINTS";
	case SPELL_FAILED_TRY_AGAIN:					return "SPELL_FAILED_TRY_AGAIN";
	case SPELL_FAILED_UNIT_NOT_BEHIND:				return "SPELL_FAILED_UNIT_NOT_BEHIND";
	case SPELL_FAILED_UNIT_NOT_INFRONT:				return "技能失败：目标不在面前。";
	case SPELL_FAILED_WRONG_PET_FOOD:				return "SPELL_FAILED_WRONG_PET_FOOD";
	case SPELL_FAILED_NOT_WHILE_FATIGUED:			return "SPELL_FAILED_NOT_WHILE_FATIGUED";
	case SPELL_FAILED_TARGET_NOT_IN_INSTANCE:		return "SPELL_FAILED_TARGET_NOT_IN_INSTANCE";
	case SPELL_FAILED_NOT_WHILE_TRADING:			return "SPELL_FAILED_NOT_WHILE_TRADING";
	case SPELL_FAILED_TARGET_NOT_IN_RAID:			return "SPELL_FAILED_TARGET_NOT_IN_RAID";
	case SPELL_FAILED_DISENCHANT_WHILE_LOOTING:		return "SPELL_FAILED_DISENCHANT_WHILE_LOOTING";
	case SPELL_FAILED_PROSPECT_WHILE_LOOTING:		return "SPELL_FAILED_PROSPECT_WHILE_LOOTING";
	case SPELL_FAILED_PROSPECT_NEED_MORE:			return "SPELL_FAILED_PROSPECT_NEED_MORE";
	case SPELL_FAILED_TARGET_FREEFORALL:			return "SPELL_FAILED_TARGET_FREEFORALL";
	case SPELL_FAILED_NO_EDIBLE_CORPSES:			return "SPELL_FAILED_NO_EDIBLE_CORPSES";
	case SPELL_FAILED_ONLY_BATTLEGROUNDS:			return "SPELL_FAILED_ONLY_BATTLEGROUNDS";
	case SPELL_FAILED_TARGET_NOT_GHOST:				return "SPELL_FAILED_TARGET_NOT_GHOST";
	case SPELL_FAILED_TOO_MANY_SKILLS:				return "SPELL_FAILED_TOO_MANY_SKILLS";
	case SPELL_FAILED_TRANSFORM_UNUSABLE:			return "SPELL_FAILED_TRANSFORM_UNUSABLE";
	case SPELL_FAILED_WRONG_WEATHER:				return "PELL_FAILED_WRONG_WEATHER";
	case SPELL_FAILED_DAMAGE_IMMUNE:				return "SPELL_FAILED_DAMAGE_IMMUNE";
	case SPELL_FAILED_PREVENTED_BY_MECHANIC:		return "SPELL_FAILED_PREVENTED_BY_MECHANIC";
	case SPELL_FAILED_PLAY_TIME:					return "SPELL_FAILED_PLAY_TIME";
	case SPELL_FAILED_REPUTATION:					return "SPELL_FAILED_REPUTATION";
	case SPELL_FAILED_MIN_SKILL:					return "SPELL_FAILED_MIN_SKILL";
	case SPELL_FAILED_NOT_IN_ARENA:					return "技能失败：竞技场中无法使用。";
	case SPELL_FAILED_NOT_ON_SHAPESHIFT:			return "SPELL_FAILED_NOT_ON_SHAPESHIFT";
	case SPELL_FAILED_NOT_ON_STEALTHED:				return "SPELL_FAILED_NOT_ON_STEALTHED";
	case SPELL_FAILED_NOT_ON_DAMAGE_IMMUNE:			return "SPELL_FAILED_NOT_ON_DAMAGE_IMMUNE";
	case SPELL_FAILED_NOT_ON_MOUNTED:				return "SPELL_FAILED_NOT_ON_MOUNTED";
	case SPELL_FAILED_TOO_SHALLOW:					return "SPELL_FAILED_TOO_SHALLOW";
	case SPELL_FAILED_TARGET_NOT_IN_SANCTUARY:		return "SPELL_FAILED_TARGET_NOT_IN_SANCTUARY";
	case SPELL_FAILED_TARGET_IS_TRIVIAL:			return "SPELL_FAILED_TARGET_IS_TRIVIAL";
	case SPELL_FAILED_BM_OR_INVISGOD:				return "SPELL_FAILED_BM_OR_INVISGOD";
	case SPELL_FAILED_EXPERT_RIDING_REQUIREMENT:	return "SPELL_FAILED_EXPERT_RIDING_REQUIREMENT";
	case SPELL_FAILED_ARTISAN_RIDING_REQUIREMENT:	return "SPELL_FAILED_ARTISAN_RIDING_REQUIREMENT";
	case SPELL_FAILED_NOT_IDLE:						return "SPELL_FAILED_NOT_IDLE";
	case SPELL_FAILED_NOT_INACTIVE:					return "SPELL_FAILED_NOT_INACTIVE";
	case SPELL_FAILED_PARTIAL_PLAYTIME:				return "SPELL_FAILED_PARTIAL_PLAYTIME";
	case SPELL_FAILED_NO_PLAYTIME:					return "SPELL_FAILED_NO_PLAYTIME";
	case SPELL_FAILED_NOT_IN_BATTLEGROUND:			return "SPELL_FAILED_NOT_IN_BATTLEGROUND";
	case SPELL_FAILED_ONLY_IN_ARENA:				return "SPELL_FAILED_ONLY_IN_ARENA";
	case SPELL_FAILED_TARGET_LOCKED_TO_RAID_INSTANCE: return "SPELL_FAILED_TARGET_LOCKED_TO_RAID_INSTANCE";
	case SPELL_FAILED_UNKNOWN:						return "错误";
	case SPELL_FAILED_TOO_NEAR:						return "技能失败：距离太近,无法施法。";
	case SPELL_FAILED_CANTUSE_IN_SHAPESHIFT:		return "技能失败：变身状态无法使用这个技能。";
	case SPELL_FAILED_CANTUSE_IN_MOUNT:				return "技能失败：正在骑乘状态中。";
	case SPELL_FAILED_CANTUSE_IN_COMBAT:			return "技能失败：战斗状态无法使用这个技能。";
	case SPELL_FAILED_WRONG_WEAPON:					return "技能失败：没有装备合适的武器。";
	case SPELL_FAILED_NOT_ENOUGH_BAG_POS:			return "背包空间不足。";
	case SPELL_FAILED_INSTANCE_FULL:                             return "技能失败：副本已满。";
	case SPELL_FAILED_INSTANCE_NOT_FOUND:            return "技能失败：副本未找到。";
	case SPELL_FAILED_HAVE_PET:                                     return "技能失败：您已经拥有该种类型的宠物。";
	case SPELL_FAILED_YOU_ARE_NOT_GUILD_LEADER: return "技能失败：你不是会长。";
	case SPELL_FAILED_YOU_ARE_NOT_GROUP_LEADER: return "技能失败：你不是队长。";
	case SPELL_FAILED_TOO_MANY_GROUP_MEMBERS:		return "技能失败：你的队伍已满。";
	};
	
	return "SPELL_FAILED_UNKNOWN";

}

bool SpellTemplate::IsChongZhuangSpell(ui32 spellid)
{
	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;
	for(int i = 0; i < 3; i++)
	{
		if (pSpellInfo->uiEffect[i] == SPELL_EFFECT_CHONGZHUANG)
		{
			return true;
		}
	}
	return false ;
}
bool SpellTemplate::IsDamageSpell(ui32 spellid)
{
	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;
	
	for(int i = 0; i < 3; i++)
	{
		switch (pSpellInfo->uiEffect[i])
		{
		case SPELL_EFFECT_SCHOOL_DAMAGE:
		case SPELL_EFFECT_ENVIRONMENTAL_DAMAGE:
		case SPELL_EFFECT_HEALTH_LEECH:
		case SPELL_EFFECT_WEAPON_DAMAGE_NOSCHOOL:
		case SPELL_EFFECT_ADD_EXTRA_ATTACKS:
		case SPELL_EFFECT_WEAPON_PERCENT_DAMAGE:
		case SPELL_EFFECT_POWER_BURN:
		case SPELL_EFFECT_ATTACK:
		case SPELL_EFFECT_CHONGZHUANG:
			return true;
		}

		if( pSpellInfo->uiEffect[i]==SPELL_EFFECT_APPLY_AURA ||
			pSpellInfo->uiEffect[i]==SPELL_EFFECT_APPLY_AREA_AURA ||
			pSpellInfo->uiEffect[i]==SPELL_EFFECT_PERSISTENT_AREA_AURA)
		{
			switch (pSpellInfo->uiEffectApplyAuraName[i])
			{
			case SPELL_AURA_PERIODIC_DAMAGE:
			case SPELL_AURA_PROC_TRIGGER_DAMAGE:
			case SPELL_AURA_PERIODIC_DAMAGE_PERCENT:
			case SPELL_AURA_POWER_BURN:
			case SPELL_AURA_MOD_FEAR:
			case SPELL_AURA_MOD_STUN:
			case SPELL_AURA_MOD_ROOT:
			case SPELL_AURA_MOD_SILENCE:
			case SPELL_AURA_MOD_DAMAGE_TAKEN:
			case SPELL_AURA_MOD_TAUNT:
			case SPELL_AURA_MOD_THREAT:
				return true;
			}
		}
	}

	return false;
	
}
bool SpellTemplate::IsMaNaSpell(ui32 spellid)
{
	if (IsDamageSpell(spellid))
	{
		return false ;
	}

	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;

	for(int i = 0; i < 3; i++)
	{
		switch( pSpellInfo->uiEffect[i] )
		{
		case SPELL_EFFECT_ENERGIZE:
			return true;
		default: break;
		}


		if( pSpellInfo->uiEffect[i] == SPELL_EFFECT_APPLY_AURA ||
			pSpellInfo->uiEffect[i] == SPELL_EFFECT_APPLY_AREA_AURA ||
			pSpellInfo->uiEffect[i]==SPELL_EFFECT_PERSISTENT_AREA_AURA)
		{
			switch( pSpellInfo->uiEffectApplyAuraName[i] )
			{
			case SPELL_AURA_PERIODIC_ENERGIZE:
				return true;
			default: break;
			}
		}
	}

	

	return false ;
	
}
bool SpellTemplate::IsHealingSpell(ui32 spellid)
{
	if(IsDamageSpell(spellid))
		return false;

	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;
	for(int i = 0; i < 3; i++)
	{
		switch( pSpellInfo->uiEffect[i] )
		{
		case SPELL_EFFECT_HEALTH_LEECH:
		case SPELL_EFFECT_HEAL:
		case SPELL_EFFECT_HEALTH_FUNNEL:
		case SPELL_EFFECT_HEAL_MAX_HEALTH:
			return true;
		default: break;
		}

		if( pSpellInfo->uiEffect[i] == SPELL_EFFECT_APPLY_AURA ||
			pSpellInfo->uiEffect[i] == SPELL_EFFECT_APPLY_AREA_AURA ||
			pSpellInfo->uiEffect[i]==SPELL_EFFECT_PERSISTENT_AREA_AURA)
		{
			switch( pSpellInfo->uiEffectApplyAuraName[i] )
			{
			case SPELL_AURA_PERIODIC_HEAL:
			case SPELL_AURA_PERIODIC_HEALTH_FUNNEL:
			case SPELL_AURA_SCHOOL_ABSORB:
				return true;
			default: break;
			}
		}
	}

	return false;
}

bool SpellTemplate::IsResurrectSpell(ui32 spellid)
{
	if(IsDamageSpell(spellid))
		return false;

	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;

	for(int i = 0; i < 3; i++)
	{
		switch( pSpellInfo->uiEffect[i] )
		{
		case SPELL_EFFECT_RESURRECT:
		case SPELL_EFFECT_RESURRECT_FLAT:
		case SPELL_EFFECT_SELF_RESURRECT:
			return true;
		}
	}

	return false;
}

bool SpellTemplate::IsHelpFulSpell(ui32 spellid)
{
	if(IsDamageSpell(spellid))
		return false;

	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;

	bool bRet = IsHealingSpell(spellid);
	if(bRet)
		return true;

	bRet = IsResurrectSpell(spellid);
	if(bRet)
		return true;

	for(int i = 0; i < 3; i++)
	{
		switch( pSpellInfo->uiEffect[i] )
		{
		case SPELL_EFFECT_HEAL_MAX_HEALTH:
			return true;
		default: break;
		}

		if( pSpellInfo->uiEffect[i] == SPELL_EFFECT_APPLY_AURA ||
			pSpellInfo->uiEffect[i] == SPELL_EFFECT_APPLY_AREA_AURA ||
			pSpellInfo->uiEffect[i]==SPELL_EFFECT_PERSISTENT_AREA_AURA)
		{
			switch( pSpellInfo->uiEffectApplyAuraName[i] )
			{
			case SPELL_AURA_SCHOOL_ABSORB:
			case SPELL_AURA_MOD_INCREASE_SPEED:
			case SPELL_AURA_MOD_INCREASE_HEALTH:
			case SPELL_AURA_MOD_INCREASE_ENERGY:
			case SPELL_AURA_MOD_INCREASE_ENERGY_PERCENT:
			case SPELL_AURA_MOD_INCREASE_HEALTH_PERCENT:
				return true;
			case SPELL_AURA_MOD_STAT:
			case SPELL_AURA_MOD_RESISTANCE_PCT:
				{
					if(pSpellInfo->uiEffectMiscValue[i] > 0)
						return true;
					else
						return false;
				}
				break;
			default: break;
			}
		}
	}

	return false;
}


bool SpellTemplate::IsSelfSpell(ui32 spellid)
{
	if(IsDamageSpell(spellid))
		return false;

	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;

	for(int i = 0; i < 3; i++)
	{
		if(pSpellInfo->uiEffect[i])
		switch( pSpellInfo->uiImplicitTarget[i] )
		{
		case EFF_TARGET_SELF:
			return true;
		}
	}

	return false;
}

bool SpellTemplate::IsSelfOrFriendlySpell(ui32 spellid)
{
	if(IsDamageSpell(spellid))
		return false;

	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;

	for(int i = 0; i < 3; i++)
	{
		if(pSpellInfo->uiEffect[i])
		switch( pSpellInfo->uiImplicitTarget[i] )
		{
		case EFF_TARGET_SELF:
		case EFF_TARGET_FRIENDLY:
			return true;
		}
	}

	return false;
}

bool SpellTemplate::IsSingleEnemySpell(ui32 spellid)
{
	if(IsDamageSpell(spellid))
		return true;
	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;

	for(int i = 0; i < 3; i++)
	{
		if(pSpellInfo->uiEffect[i])
			switch( pSpellInfo->uiImplicitTarget[i] )
		{
			case EFF_TARGET_SINGLE_ENEMY:
				return true;
		}
	}

	return false;
}


bool SpellTemplate::IsEnemySpell(ui32 spellid)
{
	if(IsDamageSpell(spellid))
		return true;
	stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(spellid);
	NIASSERT(pSpellInfo);
	if (!pSpellInfo) return false;

	for(int i = 0; i < 3; i++)
	{
		switch (pSpellInfo->uiEffect[i])
		{
		case SPELL_EFFECT_SCHOOL_DAMAGE:
		case SPELL_EFFECT_ENVIRONMENTAL_DAMAGE:
		case SPELL_EFFECT_HEALTH_LEECH:
		case SPELL_EFFECT_WEAPON_DAMAGE_NOSCHOOL:
		case SPELL_EFFECT_ADD_EXTRA_ATTACKS:
		case SPELL_EFFECT_WEAPON_PERCENT_DAMAGE:
		case SPELL_EFFECT_POWER_BURN:
		case SPELL_EFFECT_ATTACK:
		case SPELL_EFFECT_CHONGZHUANG:
			return true;
		}

		if(pSpellInfo->uiEffect[i])
			switch( pSpellInfo->uiImplicitTarget[i] )
		{
			case EFF_TARGET_SINGLE_ENEMY:
			case EFF_TARGET_ALL_PARTY_AROUND_CASTER:
				return true;
		}
	}

	return false;
}