#include "StdAfx.h"
#include "SkillManager.h"
#include "utils\DBFile.h"
#include "../../../TinyXML/TinyXML.h"

#include "Utils/SpellDB.h"
#include "Utils/ClientUtils.h"

#include "Console.h"

#include "LocalPlayer.h"
#include "ObjectManager.h"

#include "SpellEffect.h"
#include "ObjectManager.h"
#include "ItemManager.h"


#define SKILL_VERIFY

inline const char* TrimSpace(char* Str, BOOL Head = TRUE, BOOL Tail = TRUE)
{
	if (Head)
	{
		while( *Str && (*Str == ' ' || *Str =='\r' || *Str =='\t'))
		{
			++Str;
		}
	}

	if (Tail)
	{
		int Len = (int)strlen(Str);
		char* pTail = Str + Len -1;
		while ( *pTail == 0 || *pTail == ' ' || *pTail =='\r' || *pTail == '\t' )
		{
			*pTail-- = '\0';
		}

	}
	return Str;
}


CSkillManager::CSkillManager(void)
{
	m_globalCooldown = 0;
}

CSkillManager::~CSkillManager(void)
{
	Destroy();
}

BOOL CSkillManager::Initialize()
{
    if (!LoadVisualTablesFromFile())
        return FALSE;

	if (!InitSpellTempalte())
		return FALSE;

	if (!SpellEffectEmit::Create())
		return FALSE;
	
	return TRUE;
}

void CSkillManager::Destroy()
{
	std::map<uint32, SpellVisualDesc*>::iterator itr = m_spellVisualTable.begin();
	for(; itr != m_spellVisualTable.end(); ++itr)
	{
		if(itr->second)
		{
			NiDelete itr->second;
			itr->second = 0;
		}
	}
	m_spellVisualTable.clear();

	std::map<uint32, AuraVisualDesc*>::iterator itAura = m_auraVisualTable.begin();
	for ( ; itAura != m_auraVisualTable.end(); ++itAura)
	{
		if (itAura->second)
		{
			NiDelete itAura->second;
			itAura->second = 0;
		}
	}
	m_auraVisualTable.clear();

	std::map<SpellID, SpellTemplate*>::iterator itSpell = m_SpellTemplates.begin();
	for (; itSpell != m_SpellTemplates.end(); ++itSpell)
	{
		if (itSpell->second)
		{
			NiDelete itSpell->second;
			itSpell->second = 0;
		}
	}
	m_SpellTemplates.clear();
	SpellEffectEmit::Destroy();
}

BOOL CSkillManager::InitSpellTempalte()
{
	NIASSERT(g_pkSpellInfo);
	if (!g_pkSpellInfo)
		return FALSE;

	std::map<unsigned int, stSpellInfo>& spellInfoMap = g_pkSpellInfo->GetSpellInfos();
	std::map<unsigned int, stSpellInfo>::iterator it = spellInfoMap.begin();
	for (it; it != spellInfoMap.end(); ++it) {
		stSpellInfo& spellinfo = it->second;
		NIASSERT(spellinfo.uiId != INVALID_SPELL_ID);

		SpellTemplate* pkSpellTemplate = NiNew SpellTemplate(spellinfo);

		m_SpellTemplates[spellinfo.uiId] = pkSpellTemplate;
	}

	return TRUE;
}

BOOL CSkillManager::LoadVisualTablesFromFile()
{
    NiFile* pkFile = NiFile::GetFile("Data\\Spell\\spell.lst", NiFile::READ_ONLY);
    if(!pkFile || !(*pkFile))
    {
        NiDelete pkFile;
        return FALSE;
    }

    char acPathName[1024];
    char acName[256];

    while( pkFile->GetLine(acName, sizeof(acName)) )
    {
        NiSprintf(acPathName, sizeof(acPathName), "Data\\Spell\\%s", acName);
        LoadVisualTableFromFile(acPathName);
    }

    NiDelete pkFile;

	return TRUE;
}

void CSkillManager::CooldownCancel_Group(uint32 SpellGroup)
{
	std::map<SpellID, SpellTemplate*>::const_iterator itrBegin = m_SpellTemplates.begin();
	std::map<SpellID, SpellTemplate*>::const_iterator itrEnd = m_SpellTemplates.end();
	std::map<SpellID, SpellTemplate*>::const_iterator itr;
	for (itr = itrBegin; itr != itrEnd; itr++)
	{
		SpellTemplate* pSpellTemplate = itr->second;
		if (pSpellTemplate && pSpellTemplate->GetSpellGroup() == SpellGroup)
		{
			//if (type == COOLDOWN_TYPE_CATEGORY)
			{
				CooldownCancel(pSpellTemplate->GetSpellID());
			}//else
			{
				//CooldownCancel(pSpellTemplate->GetSpellID());
			}
		}
	}
}

void CSkillManager::AddGroupCooldown(uint32 type, uint32 remainTime, uint32 SpellGroup)
{
	std::map<SpellID, SpellTemplate*>::const_iterator itrBegin = m_SpellTemplates.begin();
	std::map<SpellID, SpellTemplate*>::const_iterator itrEnd = m_SpellTemplates.end();
	std::map<SpellID, SpellTemplate*>::const_iterator itr;
	for (itr = itrBegin; itr != itrEnd; itr++)
	{
		SpellTemplate* pSpellTemplate = itr->second;
		if (pSpellTemplate && pSpellTemplate->GetSpellGroup() == SpellGroup && SpellGroup > 0)
		{
			if (type == COOLDOWN_TYPE_CATEGORY)
			{
				AddCooldown(COOLDOWN_TYPE_SPELL,pSpellTemplate->GetSpellID(),remainTime,pSpellTemplate->GetSpellID(),0);
			}else
			{
				AddCooldown(COOLDOWN_TYPE_SPELL,pSpellTemplate->GetSpellID(),remainTime,pSpellTemplate->GetSpellID(),0);
			}
		}
	}
}
UINT CSkillManager::TestSkillState(uint32 spell)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	SpellTemplate* pSpeeInfo = GetSpellTemplate(spell);

	if (!pkLocal || spell == 0 || !pSpeeInfo)
	{
		return TestError;
	}
	//耗蓝测试
	
	unsigned int uiMPRequest = 0;
	if (pSpeeInfo->GetMPRequest(uiMPRequest))
	{
		if (uiMPRequest > pkLocal->GetMP())
		{
			return NeedMoreMP ;
		}
	}
	//被动技能测试
	if(pSpeeInfo->GetAttribute() & ATTRIBUTES_PASSIVE)
	{
		return AttributePassive;
	}
	//主动技能测试
	//需要选择目标
	if (pSpeeInfo->GetSelectMethod() & TARGET_FLAG_UNIT)
	{
		CCharacter* pkTarget = pkLocal->GetTargetObject();
		if (!pkTarget || pkTarget == pkLocal)
		{
			return NeedSelObj;
		}

		NiPoint3 TargetPos = pkTarget->GetPosition();
		if (pkTarget->isMount())
		{
			if(pkTarget->IsMountPlayer())
				TargetPos =  pkTarget->GetMountFoot()->GetSceneNode()->GetTranslate();
			else if(pkTarget->IsMountCreature())
				TargetPos =  pkTarget->GetMountCreature()->GetSceneNode()->GetTranslate();
		}

		float MinRang = 0.0f; 
		float MaxRang = 0.0f;

		float len = (TargetPos - pkLocal->GetLocalPlayerPos()).Length();

		SpellTemplate::GetRange(spell, MinRang, MaxRang);

		if (len > MaxRang)
		{
			if(!SpellTemplate::IsEnemySpell(spell))
			{
				//return TestNormal;
			}
			return TooFar;
		}

		if (len < MinRang)
		{
			if(!SpellTemplate::IsEnemySpell(spell))
			{
				//return TestNormal;
			}
			return TooNear;
		}

		return TestError;
	}


	return TestNormal;
}
void CSkillManager::AddCooldown(uint32 type, uint32 misc, uint32 remainTime, uint32 spellid, uint32 itemid)
{
	uint32 msClientTime = float2int32(NiGetCurrentTimeInSec()*1000.0f);
	uint32 expireTime = msClientTime + remainTime;

	CPlayer* pPlr = ObjectMgr->GetLocalPlayer();
	remainTime += pPlr->m_increasespellcdt[spellid];

	PlayerCooldownMap::iterator it = m_cooldownMap[type].find(misc);
	if ( it != m_cooldownMap[type].end())
	{
		if (it->second.ExpireTime < msClientTime)
		{
			it->second.ExpireTime = expireTime;
			it->second.ItemId = itemid;
			it->second.SpellId = spellid;
		}
	}
	else
	{
		PlayerCooldown cd;
		cd.ExpireTime = expireTime;
		cd.SpellId = spellid;
		cd.ItemId = itemid;
		m_cooldownMap[type].insert(std::make_pair(misc, cd));
	}
}

void CSkillManager::AddGlobalCooldown(uint32 remainTime)
{
	uint32 msClientTime = float2int32(NiGetCurrentTimeInSec()*1000.0f);
	m_globalCooldown = msClientTime + remainTime;
}

void CSkillManager::CooldownCancel(uint32 spellid)
{
	//m_globalCooldown = 0;

	PlayerCooldownMap::iterator itr;
	itr = m_cooldownMap[COOLDOWN_TYPE_SPELL].find( spellid );
	if( itr != m_cooldownMap[COOLDOWN_TYPE_SPELL].end( ) )
	{
		m_cooldownMap[COOLDOWN_TYPE_SPELL].erase( itr );
	}
}

void CSkillManager::ClearCooldown()
{
	m_cooldownMap[COOLDOWN_TYPE_SPELL].clear();
	m_cooldownMap[COOLDOWN_TYPE_CATEGORY].clear();
}
bool CSkillManager::CooldownCanCast(uint32 spellid)
{
	PlayerCooldownMap::iterator itr;
	uint32 mstime = float2int32(NiGetCurrentTimeInSec()*1000.0f);

	itr = m_cooldownMap[COOLDOWN_TYPE_SPELL].find( spellid );
	if( itr != m_cooldownMap[COOLDOWN_TYPE_SPELL].end( ) )
	{
		if( mstime < itr->second.ExpireTime )
			return false;
		else
			m_cooldownMap[COOLDOWN_TYPE_SPELL].erase( itr );
	}

	SpellTemplate* pkSpell = m_SpellTemplates[spellid];
	NIASSERT(pkSpell);

	if( pkSpell->GetStartRecoveryTime() && m_globalCooldown )			/* gcd doesn't affect spells without a cooldown it seems */
	{
		if( mstime < m_globalCooldown )
			return false;
		else
			m_globalCooldown = 0;
	}

	return true;
}
bool CSkillManager::GetCategoryCooldownTime(uint32 spellid, uint32& uiRecoverTime, uint32& uiRemainTime)
{
	//Old CODE 
	return false;

	uint32 mstime = float2int32(NiGetCurrentTimeInSec() * 1000.0f);
	if (spellid == 0)
	{
		return false;
	}
	unsigned int  uiCategory = m_SpellTemplates[spellid]->GetCategory();
	if (uiCategory == 0)
	{
		return false;
	}

	PlayerCooldownMap::iterator itr;
	itr = m_cooldownMap[COOLDOWN_TYPE_CATEGORY].find(uiCategory);
	if (itr != m_cooldownMap[COOLDOWN_TYPE_CATEGORY].end())
	{
		if( mstime > itr->second.ExpireTime )
			return false;

		uiRemainTime = itr->second.ExpireTime - mstime;
		SpellTemplate* pkSpell = m_SpellTemplates[itr->second.SpellId];
		NIASSERT(pkSpell);
		uiRecoverTime = pkSpell->GetRecoverTime();

		return true;
	}

	return false;
}
bool CSkillManager::GetItemCooldownTime(uint32 itemid, uint32& uiRecoverTime, uint32& uiRemainTime)
{
	ItemPrototype_Client* pItem = ItemMgr->GetItemPropertyFromDataBase(itemid);

	if (pItem && (pItem->Class == ITEM_CLASS_CONSUMABLE || pItem->Class == ITEM_CLASS_USE || pItem->Class == ITEM_CLASS_RECIPE))
	{
		for (int i= 0; i < 5; i++)
		{
			if (pItem->Spells[i].Id != 0)
			{
				bool Find = SYState()->SkillMgr->GetCooldownTime(pItem->Spells[i].Id,uiRecoverTime,uiRemainTime);
				if (Find)
				{
					return true ;
				}
			}
		}
	}

	return false ;
}
bool CSkillManager::GetCooldownTime(uint32 spellid, uint32& uiRecoverTime, uint32& uiRemainTime)
{
	uint32 mstime = float2int32(NiGetCurrentTimeInSec() * 1000.0f);

	PlayerCooldownMap::iterator itr;
	itr = m_cooldownMap[COOLDOWN_TYPE_SPELL].find(spellid);
	if (itr != m_cooldownMap[COOLDOWN_TYPE_SPELL].end())
	{
		if( mstime > itr->second.ExpireTime )
			return false;

		uiRemainTime = itr->second.ExpireTime - mstime;
		SpellTemplate* pkSpell = m_SpellTemplates[spellid];
		NIASSERT(pkSpell);
		uiRecoverTime = pkSpell->GetRecoverTime();

		return true;
	}

	return false;
}

bool CSkillManager::GetGlobalCooldownTime(uint32 spellid, uint32& uiGlobalCooldown, uint32& uiRemainTime)
{
	uint32 mstime = float2int32(NiGetCurrentTimeInSec() * 1000.f);
	if (mstime > m_globalCooldown)
		return false;

	uiRemainTime = m_globalCooldown - mstime;

	CPlayerLocal* pkLP = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLP);
	if (!pkLP)
		return false;

	SpellTemplate* pkST = SYState()->SkillMgr->GetSpellTemplate(spellid);
	if (!pkST)
		return false;

	const stSpellInfo& si = pkST->GetSpellInfo();

	if (si.uiStartRecoveryTime == 0)
		return false;

	int32 atime = float2int32((float)si.uiStartRecoveryTime /** pkLP->GetFloatValue(UNIT_MOD_CAST_SPEED)*/);
	if( atime <= 0 )
		return false;
	if( atime >= pkST->GetStartRecoveryTime() )
		atime = pkST->GetStartRecoveryTime();
	//if( atime >= 1500 )
	//	atime = 1500;

	atime += pkLP->m_increasespellcdt[si.uiId];

	uiGlobalCooldown = atime;

	return true;
}

void CSkillManager::GetEffectsByTrigID(int spellid, int trigid, std::vector<SpellEffectNode*>& effects)
{
	SpellTemplate* pkSpell = GetSpellTemplate(spellid);
	if (!pkSpell) return;

	const SpellVisualDesc* pkVisualKit = pkSpell->GetVisulalKit();
	if (!pkVisualKit) return;

	const std::vector<SpellEffectNode*>& aEffectNodes = pkVisualKit->m_kEffects.vEffects;
	for ( unsigned int uj = 0; uj < aEffectNodes.size(); ++uj)
	{
		SpellEffectNode* pkEffectNode = aEffectNodes[uj];
		for (unsigned int uk = 0; uk < pkEffectNode->vRefTriggers.size(); ++uk)
		{
			if (pkEffectNode->vRefTriggers[uk] == trigid)
			{
				effects.push_back(pkEffectNode);
			}
		}
	}
}

void CSkillManager::GetSpellEffects(int spellid, int trigevt, int action, std::vector<SpellEffectNode*>& effects)
{
	SpellTemplate* pkSpell = GetSpellTemplate(spellid);
	if (!pkSpell) return;

	const SpellVisualDesc* pkVisualKit = pkSpell->GetVisulalKit();
	if (!pkVisualKit) return;

	std::vector<SpellTriggerBase*> aMatchTriggers;
	const std::vector<SpellTriggerBase*>& aTriggers = pkVisualKit->m_kEffects.vTriggers;

	unsigned int ui = 0;
	for ( ui = 0; ui < aTriggers.size(); ++ui)
	{
		SpellTriggerBase* pkTrigger = aTriggers[ui];
		NIASSERT(pkTrigger);

		if (pkTrigger->GetType() == SpellTriggerBase::Event)
		{
			SpellEventTrigger* pkEvtTrig = (SpellEventTrigger*)pkTrigger;
			if (pkEvtTrig->iEvent == trigevt ) {
				aMatchTriggers.push_back(pkEvtTrig);
			}
		}
	}

	const std::vector<SpellEffectNode*>& aEffectNodes = pkVisualKit->m_kEffects.vEffects;
	for (ui = 0; ui < aMatchTriggers.size(); ++ui)
	{
		int triggerid = aMatchTriggers[ui]->iTrigID;
		for ( unsigned int uj = 0; uj < aEffectNodes.size(); ++uj)
		{
			SpellEffectNode* pkEffectNode = aEffectNodes[uj];
			for (unsigned int uk = 0; uk < pkEffectNode->vRefTriggers.size(); ++uk)
			{
				if (pkEffectNode->vRefTriggers[uk] == triggerid &&
					aMatchTriggers[ui]->iAction == action)
				{
					effects.push_back(pkEffectNode);
				}
			}
		}
	}
}

void CSkillManager::GetAuraEffects(int spellid, int trigevt, int action, std::vector<SpellEffectNode*>& effects)
{
	SpellTemplate* pkSpell = GetSpellTemplate(spellid);
	if (!pkSpell) return;

	const SpellVisualDesc* pkVisualKit = pkSpell->GetVisulalKit();
	if (!pkVisualKit || pkVisualKit->auraeffid <= 0) return;

	std::map<uint32, AuraVisualDesc*>::iterator it = m_auraVisualTable.find(pkVisualKit->auraeffid);
	if (it != m_auraVisualTable.end())
	{
		AuraVisualDesc* v = it->second;

		std::vector<SpellTriggerBase*> aAuraTrigger;
		const std::vector<SpellTriggerBase*>& aTriggers = v->m_kEffects.vTriggers;
		unsigned int ui = 0;
		for ( ui = 0; ui < aTriggers.size(); ++ui)
		{
			SpellTriggerBase* pkTrigger = aTriggers[ui];
			NIASSERT(pkTrigger);

			if (pkTrigger->GetType() == SpellTriggerBase::Event)
			{
				SpellEventTrigger* pkEvtTrig = (SpellEventTrigger*)pkTrigger;
				if (pkEvtTrig->iEvent == trigevt ) {
					aAuraTrigger.push_back(pkEvtTrig);
				}
			}
		}

		const std::vector<SpellEffectNode*>& aEffectNodes = v->m_kEffects.vEffects;
		for (ui = 0; ui < aAuraTrigger.size(); ++ui)
		{
			int triggerid = aAuraTrigger[ui]->iTrigID;
			for ( unsigned int uj = 0; uj < aEffectNodes.size(); ++uj)
			{
				SpellEffectNode* pkEffectNode = aEffectNodes[uj];
				for (unsigned int uk = 0; uk < pkEffectNode->vRefTriggers.size(); ++uk)
				{
					if (pkEffectNode->vRefTriggers[uk] == triggerid)
					{
						if (aAuraTrigger[ui]->iAction == action)
						{
							effects.push_back(pkEffectNode);
						}
					}
				}
			}
		}
	}
}

void CSkillManager::Damage(ui64 caster, ui64 target, uint32 spellid)
{
	CCharacter* pkTarget = (CCharacter*)ObjectMgr->GetObject(target);
	if (!pkTarget) return;

	SpellTemplate* pkSpell = GetSpellTemplate(spellid);
	if (!pkSpell) return;

	const SpellVisualDesc* pkVisualKit = pkSpell->GetVisulalKit();
	if (!pkVisualKit) 
		return;

	if (!pkVisualKit->m_kEffects.HasProjectile())
	{
		const stSpellInfo& spellInfo = pkSpell->GetSpellInfo();
		std::vector<SpellEffectNode*> effects;
		GetSpellEffects(spellInfo.uiId, TE_Damage, TA_Active, effects);

		if (effects.size() > 0)
		{
			SpellEffectEmit::EmitParam ep;

			ep.m_iSpellid = spellInfo.uiId;
			ep.m_OwnerID = pkTarget->GetGUID();
			ep.m_vEffects = effects;

			std::vector<SpellEffectEmit::EmitResult> results;
			SpellEffectEmit::Get()->Emit(ep, results, false);
		}
	}

	//if (g_pkConsole)
	//	g_pkConsole->Printf(79, "CSkillManager::Damage caster:%llu, target:%llu, spellid:%d\n", caster, target, spellid);

	//SpellTemplate* pkSpell = GetSpellTemplate(spellid);
	//if (!pkSpell) return;

	//CCharacter* pkTarget = (CCharacter*)ObjectMgr->GetObject(target);
	//if (!pkTarget) return;

	//std::vector<SpellEffectNode*> effects;
	//GetSpellEffects(spellid, TE_Damage, TA_Active, effects);
	
	//for (unsigned int ui = 0; ui < effects.size(); ui++)
	//{
	//	PopulateEffect(pkTarget, 0, effects[ui], pkSpell);
	//}

//	SpellEffectEmit::EmitParam ep;
//
//	ep.m_iSpellid = spellid;
//	ep.m_OwnerID = pkTarget->GetGUID();
//	unsigned int ui = 0;
////	ep.m_kTargets = m_targetList;
//	ep.m_vEffects = effects;
//
//	std::vector<SpellEffectEmit::EmitResult> results;
//	SpellEffectEmit::Get()->Emit(ep, results, false);
}
//
//CEffectBase* CSkillManager::PopulateEffect(CCharacter* owner, const Target* const targets, 
//		SpellEffectNode* spelleffnode, SpellTemplate* pkSpellTemplate, bool active)
//{
//	SpellEffectEmit::EmitParam ep;
//
//	ep.m_iSpellid = spellID;
//	ep.m_pkOwner = this;
//	unsigned int ui = 0;
//	for ( ui = 0; ui < vTargetHit.size(); ui++)
//		ep.m_kTargets.AddEntry(vTargetHit[ui]);
//	ep.m_vEffects.push_back(spelleffnode);
//
//	std::vector<CEffectBase*> result;
//	SpellEffectEmit::Get()->Emit(ep, result, false);
//
////	CEffectBase* pEffect = SpellEffectEmit::Get()->EmitEffect(owner, targets, spelleffnode, pkSpellTemplate, active);
////	if (!pEffect) {
////		NILOG("create effect failed.");
////		return 0;
////	}
//
//	//if (pEffect && spelleffnode->iPostTrig != -1)
//	//{
//	//	PopulateNextEffect(owner, targets, spelleffnode, spellInfos, pEffect);
//	//}
//
//	return pEffect;
//}

//void CSkillManager::PopulateNextEffect(CCharacter* owner, const Target* const targets, SpellEffectNode* spelleffnode, 
//									   SpellTemplate* pkSpellTemplate, CEffectBase* pkEffect)
//{
//	if (!pkEffect) return;
//
//	const stSpellInfo& spellInfos = pkSpellTemplate->GetSpellInfo();
//
//	std::vector<SpellEffectNode*> effects;
//	GetEffectsByTrigID(spellInfos.uiId, spelleffnode->iPostTrig, effects);
//
//	for (unsigned int ui = 0; ui < effects.size(); ui++)
//	{
//		CEffectBase* pEffect = PopulateEffect(owner, targets, effects[ui], pkSpellTemplate, false);
//		if (!pEffect) {
//			NILOG("create effect failed.");
//			return;
//		}
//
//		CEffectBase::EffectCallbackObjectPtr spObj = NiNew EffectPostEvent;
//		if (spObj)
//		{
//			pkEffect->AddCallbackObject(spObj);
//			pkEffect->SetNext(pEffect);
//		}
//	}
//}

void CSkillManager::PeriodicDamage(uint64 caster, uint64 target, uint32 spellid)
{
	SpellTemplate* pkSpell = GetSpellTemplate(spellid);
	if (!pkSpell) return;

	CCharacter* pkTarget = (CCharacter*)ObjectMgr->GetObject(target);
	if (!pkTarget) return;

	std::vector<SpellEffectNode*> effects;
	GetSpellEffects(spellid, TE_PeriodicDamage, TA_Active, effects);
	
	//for (unsigned int ui = 0; ui < effects.size(); ui++)
	//{
	//	PopulateEffect(pkTarget, 0, effects[ui], pkSpell);
	//}

	SpellEffectEmit::EmitParam ep;

	ep.m_iSpellid = spellid;
	ep.m_OwnerID = pkTarget->GetGUID();
//	ep.m_kTargets = m_targetList;
	ep.m_vEffects = effects;

	std::vector<SpellEffectEmit::EmitResult> results;
	SpellEffectEmit::Get()->Emit(ep, results, false);
}

void CSkillManager::EnvironmentalDamage(uint64 target, uint8 type)
{
}

void CSkillManager::HealHP(uint64 caster, uint64 target, ui32 spellid)
{
	SpellTemplate* pkSpell = GetSpellTemplate(spellid);
	if (!pkSpell) return;

	CCharacter* pkTarget = (CCharacter*)ObjectMgr->GetObject(target);
	if (!pkTarget) return;

	std::vector<SpellEffectNode*> effects;
	GetSpellEffects(spellid, TE_HealHP, TA_Active, effects);

//	for (unsigned int ui = 0; ui < effects.size(); ui++)
//	{
//		PopulateEffect(pkTarget, 0, effects[ui], pkSpell);
//	}
	SpellEffectEmit::EmitParam ep;

	ep.m_iSpellid = spellid;
	ep.m_OwnerID = pkTarget->GetGUID();
	unsigned int ui = 0;
//	ep.m_kTargets = m_targetList;
	ep.m_vEffects = effects;

	std::vector<SpellEffectEmit::EmitResult> results;
	SpellEffectEmit::Get()->Emit(ep, results, false);
}

void CSkillManager::HealMP(uint64 caster, uint64 target, ui32 spellid)
{
	SpellTemplate* pkSpell = GetSpellTemplate(spellid);
	if (!pkSpell) return;

	CCharacter* pkTarget = (CCharacter*)ObjectMgr->GetObject(target);
	if (!pkTarget) return;

	std::vector<SpellEffectNode*> effects;
	GetSpellEffects(spellid, TE_HealMP, TA_Active, effects);

	//for (unsigned int ui = 0; ui < effects.size(); ui++)
	//{
	//	PopulateEffect(pkTarget, 0, effects[ui], pkSpell);
	//}
	SpellEffectEmit::EmitParam ep;

	ep.m_iSpellid = spellid;
	ep.m_OwnerID = pkTarget->GetGUID();
	unsigned int ui = 0;
//	ep.m_kTargets = m_targetList;
	ep.m_vEffects = effects;

	std::vector<SpellEffectEmit::EmitResult> results;
	SpellEffectEmit::Get()->Emit(ep, results, false);
}

BOOL CSkillManager::LoadVisualTableFromFile(const char* pszFileName)
{
	NILOG("Load spell script %s\n", pszFileName);

    NiFile* pkFile = NiFile::GetFile(pszFileName, NiFile::READ_ONLY);
    if(!pkFile || !(*pkFile))
    {
        NiDelete pkFile;
        return FALSE;
    }

    char* acBuffer = (char*)malloc(64*1024); //希望不要大于64K
    unsigned int uiSize = pkFile->Read(acBuffer, 64*1024);
    NiDelete pkFile;

    NIASSERT(uiSize < 64*1024 - 1);
    acBuffer[uiSize] = '\0';

	BOOL b = LoadVisualTableFromMemory(pszFileName, acBuffer);
	free( acBuffer );
	return b;
}

BOOL CSkillManager::LoadVisualTableFromMemory(const char* pszFileName, const char* pBuff)
{
	TiXmlDocument xdoc;
	 xdoc.Parse(pBuff);
	if (xdoc.Error() )
	{
#ifdef SKILL_VERIFY
		const char* Error = xdoc.ErrorDesc();
		//char szError[512];
		//_snprintf(szError, 255, "技能效果包脚本%s发生错误:%s", pszFileName, Error);
		std::string strRet = _TRAN(N_NOTICE_Z71, pszFileName, Error);
		NiMessageBox(strRet.c_str(), "error");
#endif

		NILOG("parse spell file failed, error : %s", Error);
		return FALSE;
	}
	TiXmlElement* pTable = xdoc.RootElement();
	if (pTable == NULL)
	{
		return FALSE;
	}
	if(!NiStricmp(pTable->Value(), "Spells"))
	{
		TiXmlElement* pChildElem = pTable->FirstChildElement("Spell");
		while (pChildElem)
		{
			LoadSpellVisualEntry(pChildElem);
			pChildElem = pChildElem->NextSiblingElement("Spell");
		}
	}
	else
	if (!NiStricmp(pTable->Value(), "Auras"))
	{
		TiXmlElement* pChildElem = pTable->FirstChildElement("Aura");
		while (pChildElem)
		{
			LoadAuraVisualEntry(pChildElem);
			pChildElem = pChildElem->NextSiblingElement("Aura");
		}
	}
	else
	{
		NILOG("unknown spell table");
#ifdef SKILL_VERIFY
		NiMessageBox("unknown spell table", "error");
#endif
		return FALSE;
	}

	// XML PARSE
	return TRUE;
}

BOOL CSkillManager::LoadSpellVisualEntry(void* pXInfo)
{
	TiXmlElement* pXElem = (TiXmlElement*)pXInfo;
	const char* pszValue = pXElem->Attribute("id");
	SpellTemplate* pSpellTemplate = NULL;
	int nSpellVisualID = -1;
	if (pszValue && pszValue[0])
	{
		nSpellVisualID = atoi(pszValue);
		if ( nSpellVisualID == 54 )
		{
			int a = 0;
		}
		
		if (nSpellVisualID <= 0)
		{
			return FALSE;
		}

		if (m_spellVisualTable.find(nSpellVisualID) != m_spellVisualTable.end())
		{
			char szError[128];
			_snprintf(szError, 127, "重复的技能效果包ID定义 id:%d", nSpellVisualID);
			NILOG(szError, nSpellVisualID);
			NILOG("\n");

#ifdef SKILL_VERIFY
	NiMessageBox(szError, "error");
#endif
			return FALSE;
		}
	}
	else
	{
		NILOG("技能必须定义ID");
#ifdef SKILL_VERIFY
		NiMessageBox("错误的技能数据定义", "error");
#endif
		return FALSE;
	}

	SpellVisualDesc* pSpellVisual = NiNew SpellVisualDesc;
	NIASSERT(pSpellVisual);
	if (!pSpellVisual)
		return FALSE;

	// child prop
	TiXmlElement* pChildElem = pXElem->FirstChildElement();
	while (pChildElem)
	{
		pszValue = pChildElem->Value();
		if (NiStricmp(pszValue, "def") == 0)
		{
			ParseSpellDef(pSpellVisual, pChildElem);
		}
		else if (NiStricmp(pszValue, "long_desc") == 0)
		{
			//LoadCharacterWeapon(pCharInfo, pChildElem);
		}
		else if (NiStricmp(pszValue, "Effects") == 0)
		{
			ParseEffects(pSpellVisual->m_kEffects, pChildElem);
		}
		else if (NiStricmp(pszValue, "Animation") == 0)
		{
			ParseAnimation(pSpellVisual, pChildElem);
		}
		else if (NiStricmp(pszValue, "Trigger") == 0)
		{
			ParseTrigger(pSpellVisual->m_kEffects, pChildElem);
		}
		else if (NiStricmp(pszValue, "Sound") == 0)
		{
			ParseSound(pSpellVisual->m_kEffects, pChildElem);
		}
		else
		if (NiStricmp(pszValue, "Apperance") == 0)
		{
			ParseApperanceAttr(pSpellVisual, pChildElem);
		}
		else
		if (NiStricmp(pszValue, "Aura") == 0)
		{
			ParseAuraAttr(pSpellVisual, pChildElem);
		}

		pChildElem = pChildElem->NextSiblingElement();
	}

	// insert
	m_spellVisualTable[nSpellVisualID] = pSpellVisual;

	return TRUE;
}

BOOL CSkillManager::LoadAuraVisualEntry(void* pXInfo)
{
	TiXmlElement* pXElem = (TiXmlElement*)pXInfo;
	const char* pszValue = pXElem->Attribute("id");
	int iAuraID = -1;
	if (pszValue && pszValue[0])
	{
		iAuraID = atoi(pszValue);
		if (iAuraID < 0)
		{
			return FALSE;
		}

		if (m_auraVisualTable.find(iAuraID) != m_auraVisualTable.end())
		{
			char szError[128];
			_snprintf(szError, 127, "重复的技能效果包ID定义 id:%d", iAuraID);
			NILOG(szError, iAuraID);
			NILOG("\n");

#ifdef SKILL_VERIFY
	NiMessageBox(szError, "error");
#endif
			return FALSE;
		}
	}
	else
	{
		NILOG("光环必须定义ID");
#ifdef SKILL_VERIFY
		NiMessageBox("错误的光环效果数据定义, 缺少ID", "error");
#endif
		return FALSE;
	}

	AuraVisualDesc* pkAuraVisual = NiNew AuraVisualDesc;
	NIASSERT(pkAuraVisual);
	if (!pkAuraVisual)
		return FALSE;

	// child prop
	TiXmlElement* pChildElem = pXElem->FirstChildElement();
	while (pChildElem)
	{
		pszValue = pChildElem->Value();
		if (NiStricmp(pszValue, "Effects") == 0)
		{
			ParseEffects(pkAuraVisual->m_kEffects, pChildElem, true);
		}
		else if (NiStricmp(pszValue, "Trigger") == 0)
		{
			ParseTrigger(pkAuraVisual->m_kEffects, pChildElem);
		}
		else if (NiStricmp(pszValue, "Sound") == 0)
		{
			ParseSound(pkAuraVisual->m_kEffects, pChildElem, true);
		}

		pChildElem = pChildElem->NextSiblingElement();
	}

	// insert
	m_auraVisualTable[iAuraID] = pkAuraVisual;

	return TRUE;
}

extern std::string UTF8ToAnis(const std::string& str);

BOOL CSkillManager::ParseSpellDef(SpellVisualDesc* pSpell, void* XDef)
{
	if (!pSpell)
		return FALSE;

	TiXmlElement* pXElem = (TiXmlElement*)XDef;

	const char* pszValue = pXElem->Attribute("icon");
	if (pszValue && pszValue[0])
	{
		NiStrcpy(pSpell->m_Icon, MAX_PATH, pszValue);
	}

	pszValue = pXElem->Attribute("brush");
	if ( pszValue && pszValue[0] )
	{
		NiStrcpy(pSpell->m_Brush, MAX_PATH, pszValue);
	}

	pszValue = pXElem->Attribute("height");
	if ( pszValue && pszValue[0] )
	{
		pSpell->m_fBrushHeight = atof( pszValue );
	}

	return TRUE;
}

BOOL CSkillManager::ParserTriggerEntry(SpellTriggerBase*& pTriggerEntry, void* XDef)
{
	NIASSERT(!pTriggerEntry);

	TiXmlElement* pXElem = (TiXmlElement*)XDef;
	const char* pszValue = pXElem->Attribute("type");

	int iType = -1;
	//SpellTriggerBase* pkNewTrigger;
	if (!NiStricmp(pszValue, "event"))
	{
		iType = SpellTriggerBase::Event;
		pTriggerEntry = NiNew SpellEventTrigger;
	}
	else
	if (!NiStricmp(pszValue, "time"))
	{
		iType = SpellTriggerBase::Timer;
		pTriggerEntry = NiNew SpellTimeTrigger;
	}

	if (!pTriggerEntry)
		return FALSE;

	pszValue = pXElem->Attribute("action");
	if (pszValue && pszValue[0] != '\0')
	{
		if (!NiStricmp(pszValue, "activate"))
		{
			pTriggerEntry->iAction = TA_Active;
		}
		else
		if (!NiStricmp(pszValue, "deactivate"))
		{
			pTriggerEntry->iAction = TA_Deactive;
		}
		//else
		//{
		//	NIASSERT(0 && "invalid effect trigger action!!!");
		//}
	}

	pszValue = pXElem->Attribute("id");
	pTriggerEntry->iTrigID = atoi(pszValue);
	if (iType == SpellTriggerBase::Event)
	{
		SpellEventTrigger* pkEventTrigger = (SpellEventTrigger*)pTriggerEntry;
		pszValue = pXElem->Attribute("event");
		if (pszValue && pszValue[0] != '\0')
		{
			if (!NiStricmp(pszValue, "caststart"))
			{
				pkEventTrigger->iEvent = TE_CastStart;
			}
			else
			if (!NiStricmp(pszValue, "castend"))
			{
				pkEventTrigger->iEvent = TE_CastEnd;
			}
			else
			if (!NiStricmp(pszValue, "attackstart"))
			{
				pkEventTrigger->iEvent = TE_AttackStart;
			}
			else
			if (!NiStricmp(pszValue, "attackend"))
			{
				pkEventTrigger->iEvent = TE_AttackEnd;
			}
			else
			if (!NiStricmp(pszValue, "animkey"))
			{
				pkEventTrigger->iEvent = TE_AnimKey;

				pszValue = pXElem->Attribute("key");
				if (pszValue && pszValue[0] != '0')
				{
					pkEventTrigger->iAnimKey = atoi(pszValue);
				}
			}
			else
			if (!NiStricmp(pszValue, "damage"))
			{
				pkEventTrigger->iEvent = TE_Damage;
			}
			else
			if (!NiStricmp(pszValue, "periodicdamage"))
			{
				pkEventTrigger->iEvent = TE_PeriodicDamage;
			}
			else
			if (!NiStricmp(pszValue, "aurastart"))
			{
				pkEventTrigger->iEvent = TE_AuraStart;
			}
			else
			if (!NiStricmp(pszValue, "auraover"))
			{
				pkEventTrigger->iEvent = TE_AuraOver;
			}
			else
			if (!NiStricmp(pszValue, "healhp"))
			{
				pkEventTrigger->iEvent = TE_HealHP;
			}
			else
			if (!NiStricmp(pszValue, "healmp"))
			{
				pkEventTrigger->iEvent = TE_HealMP;
			}
			else
			if (!NiStricmp(pszValue, "environmentaldamage"))
			{
				pkEventTrigger->iEvent = TE_EnvironmentalDamage;
			}
			else
			if (!NiStricmp(pszValue, "tunnelstart"))
			{
				pkEventTrigger->iEvent = TE_TunnelStart;
			}
			else
			if (!NiStricmp(pszValue, "tunnelend"))
			{
				pkEventTrigger->iEvent = TE_TunnelEnd;
			}
			//else
			//{
			//	NIASSERT(0 && "unkown effect trigger event type!!!");
			//}
		}
	}
	else
	if (iType == SpellTriggerBase::Timer)
	{
		SpellTimeTrigger* pkTimeTrigger = (SpellTimeTrigger*)pTriggerEntry;

		pszValue = pXElem->Attribute("iBegin");
		pkTimeTrigger->iBegin = atoi(pszValue);
		pszValue = pXElem->Attribute("fOffsetTime");
		pkTimeTrigger->fOffsetTime = (float)atof(pszValue);
	}
	else
	{
		// unkown
		return FALSE;
	}

	return TRUE;
}

BOOL CSkillManager::ParseEffectEntry(SpellEffectNode*& EffectEntry, void* XDef, bool aura)
{
	TiXmlElement* pXElem = (TiXmlElement*)XDef;
	const char* pszValue = pXElem->Attribute("type");

	EffectEntry = NULL;
	if (pszValue && pszValue[0] != '\0')
	{
		if (NiStricmp(pszValue , "nif") == 0)
		{
			EffectEntry = NiNew SpellEffectNifNode;
		}
		else
		if (NiStricmp(pszValue, "projectile") == 0)
		{
			EffectEntry =  NiNew SpellProjectileEffectNode;
		}
		else
		if (NiStricmp(pszValue, "trail") == 0)
		{
			EffectEntry = NiNew SpellTrailEffectNode;
		}
		else
		if (NiStricmp(pszValue, "posconstraint") == 0)
		{
			EffectEntry = NiNew SpellPosConstraintEffectNode;
		}
		else
		if (NiStricmp(pszValue, "decal") == 0)
		{
			EffectEntry = NiNew SpellDecalEffectNode;
		}
		else
		if (NiStricmp(pszValue, "chain") == 0)
		{
			EffectEntry = NiNew SpellChainEffectNode;
		}
	}

	if (EffectEntry == NULL)
	{
		return FALSE;
	}

	TiXmlElement* pkTrigger = pXElem->FirstChildElement("Trigger");
	while(pkTrigger)
	{
		int iRefID = INVALID_SPELL_TRIGGERID;

		pszValue = pkTrigger->Attribute("refid");
		if (pszValue && pszValue[0] != '\0')
		{
			iRefID = atoi(pszValue);
		}
		if (iRefID != INVALID_SPELL_TRIGGERID)
		{
			EffectEntry->vRefTriggers.push_back(iRefID);
		}

		pkTrigger = pkTrigger->NextSiblingElement("Trigger");
	}

	TiXmlElement* pkSound = pXElem->FirstChildElement("Sound");
	while (pkSound)
	{
		int iRefID = -1;
		pszValue = pkSound->Attribute("refid");
		if (pszValue && pszValue[0] != '\0')
		{
			iRefID = atoi(pszValue);
			if (aura)
				iRefID += 1000;
		}
		if (iRefID != -1)
			EffectEntry->vSounds.push_back(iRefID);

		pkSound = pkSound->NextSiblingElement("Sound");
	}

	pszValue = pXElem->Attribute("posttrigger");
	if (pszValue && pszValue[0] != '\0')
	{
		EffectEntry->iPostTrig = atoi(pszValue);
	}

	// common 
	pszValue = pXElem->Attribute("attach");
	if (pszValue && pszValue[0] != '\0')
	{
		//Effect_Head01     -- head01	-- 	SES_HEAD01
		//Effect_Head02     -- head02	-- 	SES_HEAD02
		//Effect_Chest      -- chest	-- 	SES_CHEST
		//Effect_Belt       -- belt		-- 	SES_BELT
		//Effect_RightHand  -- righthand-- 	SES_RIGHTHAND
		//Effect_LeftHand   -- lefthand	-- 	SES_LEFTHAND
		//Effect_RightFeet  -- rightfeet-- 	SES_RIGHTFEET
		//Effect_LeftFeet   -- leftfeet	-- 	SES_LEFTFEET
		//Effect_Foot01     -- foot01	-- 	SES_FOOT01
		//Effect_Foot02     -- foot02	-- 	SES_FOOT02

		if (!NiStricmp(pszValue, "head01"))
		{
			EffectEntry->Slot = SES_HEAD01;
		}
		else
		if (!NiStricmp(pszValue, "head02"))
		{
			EffectEntry->Slot = SES_HEAD02;
		}
		else
		if (!NiStricmp(pszValue, "chest"))
		{
			EffectEntry->Slot = SES_CHEST;
		}
		else
		if (!NiStricmp(pszValue, "belt"))
		{
			EffectEntry->Slot = SES_BELT;
		}
		else
		if (!NiStricmp(pszValue, "righthand"))
		{
			EffectEntry->Slot = SES_RIGHTHAND;
		}
		else
		if (!NiStricmp(pszValue, "lefthand"))
		{
			EffectEntry->Slot = SES_LEFTHAND;
		}
		else
		if (!NiStricmp(pszValue, "rightfeet"))
		{
			EffectEntry->Slot = SES_RIGHTFEET;
		}
		else
		if (!NiStricmp(pszValue, "leftfeet"))
		{
			EffectEntry->Slot = SES_LEFTFEET;
		}
		else
		if (!NiStricmp(pszValue, "foot01"))
		{
			EffectEntry->Slot = SES_FOOT01;
		}
		else
		if (!NiStricmp(pszValue, "foot02"))
		{
			EffectEntry->Slot = SES_FOOT02;
		}
		else
		if (!NiStricmp(pszValue, "original"))
		{
			EffectEntry->Slot = SES_ORIGINAL;
		}
		else
		if (!NiStricmp(pszValue, "backside"))
		{
			EffectEntry->Slot = SES_BACKSIDE;
		}
		else
		if (!NiStricmp(pszValue, "scene"))
		{
			EffectEntry->Slot = SES_SCENE;
		}
		else
		{
			EffectEntry->Slot = SES_CHEST;
		}
	}else
	{
		EffectEntry->Slot = SES_CHEST;
	}

	
	pszValue = pXElem->Attribute("maxtime");
	if (pszValue && pszValue[0] != '\0')
	{
		EffectEntry->MaxLife = (float)atof(pszValue);
	}

	pszValue = pXElem->Attribute("stay");
	if (pszValue && pszValue[0] != '\0')
	{
		EffectEntry->bWorld = !NiStricmp("true", pszValue);
	}

	pszValue = pXElem->Attribute("owner");
	if (pszValue && pszValue[0] != '\0')
	{
		EffectEntry->iOwner = atoi(pszValue);
	}

	pszValue = pXElem->Attribute("scale");
	if (pszValue && pszValue[0] != '\0')
	{
		EffectEntry->scale = atof(pszValue);
	}
	else
		EffectEntry->scale = 1.f;

	pszValue = pXElem->Attribute("noscale");
	if (pszValue && pszValue[0] != '\0')
	{
		EffectEntry->bNoScale = !NiStricmp("true", pszValue);
	}

	pszValue = pXElem->Attribute("targetprojetile");
	if (pszValue && pszValue[0] != '\0')
	{
		EffectEntry->bTargetProjetile = !NiStricmp("true", pszValue);
	}

	
	EffectEntry->bFailureBreak = EffectEntry->GetType() != ET_PROJECTILE?true:false;

	/*
	pszValue = pXElem->Attribute("offset");
	if (pszValue && pszValue[0])
	{
		EffectEntry->Offset = (float)atof(pszValue);
	}
	*/

	pszValue = pXElem->Attribute("media");
	if (pszValue && pszValue[0] != '\0')
	{
		NiStrcpy(EffectEntry->Media, MAX_PATH, pszValue);
	}

	// 

	if (EffectEntry->GetType() == ET_PROJECTILE)
	{
		SpellProjectileEffectNode* ProjEntry = (SpellProjectileEffectNode*)EffectEntry;
		pszValue = pXElem->Attribute("explosion");
		if (pszValue && pszValue[0] != '\0')
		{
			NiStrcpy(ProjEntry->ExpMedia, MAX_PATH, pszValue);
		}
		else
		{
			ProjEntry->ExpMedia[0] = 0;
		}

		pszValue = pXElem->Attribute("expsound");
		if (pszValue && pszValue[0] != '\0')
		{
			NiStrcpy(ProjEntry->ExpSound, MAX_PATH, pszValue);
		}
		else
		{
			ProjEntry->ExpSound[0] = 0;
		}

		pszValue = pXElem->Attribute("expscale");
		if (pszValue && pszValue[0] != '\0' )
		{
			ProjEntry->ExpScale = atof(pszValue);
		}

		pszValue = pXElem->Attribute("selfmissile");
		if (pszValue && pszValue[0] != '\0' )
		{
			ProjEntry->bSelfMissile = !NiStricmp("true", pszValue);
		}

		pszValue = pXElem->Attribute("center");
		if (pszValue && pszValue[0] != '\0' )
		{
			ProjEntry->bCenter = !NiStricmp("true", pszValue);
		}

		pszValue = pXElem->Attribute("needtarget");
		if (pszValue && pszValue[0] != '\0' )
		{
			ProjEntry->bNeedTarget = !NiStricmp("true", pszValue);
		}

		pszValue = pXElem->Attribute("projectiletype");
		if (pszValue && pszValue[0] != '\0' )
		{
			ProjEntry->iProjectileType = atoi(pszValue);
		}

		pszValue = pXElem->Attribute("projectileNum");
		if (pszValue && pszValue[0] != '\0' )
		{
			ProjEntry->projectileNum = atoi(pszValue);
		}

		pszValue = pXElem->Attribute("selfmissiletime");
		if (pszValue && pszValue[0] != '\0')
		{
			ProjEntry->fSelfMissileTime = (float)atof(pszValue);
			NIASSERT(ProjEntry->fSelfMissileTime >= 0.f && ProjEntry->fSelfMissileTime <= 1.0f);
		}
	}
	else
	if (EffectEntry->GetType() == ET_MODEL)
	{
		SpellEffectNifNode* pModelEffectNode = (SpellEffectNifNode*)EffectEntry;
		NIASSERT(pModelEffectNode);

		pszValue = pXElem->Attribute("clamp");
		if (pszValue && pszValue[0] != '\0')
		{
			pModelEffectNode->bClamp = !NiStricmp("true", pszValue);
		}

		pszValue = pXElem->Attribute("infinite");
		if (pszValue && pszValue[0] != '\0')
		{
			pModelEffectNode->bInfinite = !NiStricmp("true", pszValue);
		}

		pszValue = pXElem->Attribute("loop");
		if (pszValue && pszValue[0] != '\0')
		{
			pModelEffectNode->bLoop = !NiStricmp("true", pszValue);
		}

		pszValue = pXElem->Attribute("looplimit");
		if (pszValue && pszValue[0] != '\0')
		{
			pModelEffectNode->uiLoopLimit = atoi(pszValue);
		}

		pszValue = pXElem->Attribute("clamp");
		if (pszValue && pszValue[0] != '\0')
		{
			pModelEffectNode->bClamp = !NiStricmp("true", pszValue);
		}
	}
	else
	if (EffectEntry->GetType() == ET_TRAIL)
	{
		SpellTrailEffectNode* pkTrailEffectNode = (SpellTrailEffectNode*)EffectEntry;
		NIASSERT(pkTrailEffectNode);

		pszValue = pXElem->Attribute("traillifetime");
		if (pszValue && pszValue[0] != '\0')
		{
			pkTrailEffectNode->fLifeTime = (float)atof(pszValue);
		}

		pszValue = pXElem->Attribute("total");
		if (pszValue && pszValue[0] != '\0')
		{
			pkTrailEffectNode->fTotalTime = (float)atof(pszValue);
		}

		pszValue = pXElem->Attribute("texfile");
		if (pszValue && pszValue[0] != '\0')
		{
			NiStrcpy(pkTrailEffectNode->szTexture, MAX_PATH, pszValue);
		}
	}
	else
	if (EffectEntry->GetType() == ET_DECAL)
	{
		SpellDecalEffectNode* pkDecalEffectNode = (SpellDecalEffectNode*)EffectEntry;
		NIASSERT(pkDecalEffectNode);

		pszValue = pXElem->Attribute("radius");
		if (pszValue && pszValue[0] != '\0')
		{
			pkDecalEffectNode->fRadius = (float)atof(pszValue);
		}

		pszValue = pXElem->Attribute("fadein");
		if (pszValue && pszValue[0] != '\0')
		{
			pkDecalEffectNode->fFadein = (float)atof(pszValue);
		}

		pszValue = pXElem->Attribute("fadeout");
		if (pszValue && pszValue[0] != '\0')
		{
			pkDecalEffectNode->fFadeout = (float)atof(pszValue);
		}
	}
	else
	if (EffectEntry->GetType() == ET_POSCONSTRAINT)
	{
		SpellPosConstraintEffectNode* pkConstraint = (SpellPosConstraintEffectNode*)EffectEntry;
		NIASSERT(pkConstraint);

		pszValue = pXElem->Attribute("effecttype");
		if (pszValue && pszValue[0] != '\0')
		{
			pkConstraint->type = atoi(pszValue);
		}

		pszValue = pXElem->Attribute("anim");
		if (pszValue && pszValue[0] != '\0')
		{
			NiStrcpy(pkConstraint->szAnimName, 128, pszValue);
		}
	}else
	if ( EffectEntry->GetType() == ET_CHAIN )
	{
		SpellChainEffectNode* EffectChain = ( SpellChainEffectNode* )EffectEntry;
		pszValue = pXElem->Attribute("chainDes");
		if (pszValue && pszValue[0] != '\0')
		{	
			EffectChain->DesNodeName = pszValue;
		}

		pszValue = pXElem->Attribute("chainSrc");
		if (pszValue && pszValue[0] != '\0')
		{
			EffectChain->SrcNodeName = pszValue;
		}

		pszValue = pXElem->Attribute("chainTextureType");
		if (pszValue && pszValue[0] != '\0')
		{
			EffectChain->MediaType = pszValue;
		}

		pszValue = pXElem->Attribute("TextureCount");
		if (pszValue && pszValue[0] != '\0')
		{
			EffectChain->Count = (float)atoi(pszValue);
		}

		pszValue = pXElem->Attribute("lifetime");
		if (pszValue && pszValue[0] != '\0')
		{
			EffectChain->LifeTime = (float)atof(pszValue);
		}

		pszValue = pXElem->Attribute("updateSpeed");
		if (pszValue && pszValue[0] != '\0')
		{
			EffectChain->UpdateRate = (float)atof(pszValue);
		}

		pszValue = pXElem->Attribute("chaintype");
		if (pszValue && pszValue[0] != '\0')
		{
			EffectChain->chaintype = (float)atoi(pszValue);
		}

		pszValue = pXElem->Attribute("direction");
		if (pszValue && pszValue[0] != '\0')
		{
			EffectChain->dir = (float)atoi(pszValue);
		}

		pszValue = pXElem->Attribute("scale");
		if (pszValue && pszValue[0] != '\0')
		{
			EffectChain->scale = (float)atof(pszValue);
		}
	}

	//else
	//if (EffectEntry->GetType() == ET_SOUND)
	//{
	//	SpellSoundEffectNode* pSoundEffectNode = (SpellSoundEffectNode*)EffectEntry;
	//	NIASSERT(pSoundEffectNode);

	//	pszValue = pXElem->Attribute("loop");
	//	if (pszValue && pszValue[0] != '\0')
	//	{
	//		pSoundEffectNode->bLoop = !NiStricmp("true", pszValue);
	//	}

	//	pszValue = pXElem->Attribute("looplimit");
	//	if (pszValue && pszValue[0] != '\0')
	//	{
	//		pSoundEffectNode->uiLoopLimit = atoi(pszValue);
	//	}

	//	pszValue = pXElem->Attribute("source");
	//	if (pszValue && pszValue[0] != '\0')
	//	{
	//		NiStrcpy(pSoundEffectNode->szSource, MAX_PATH, pszValue);
	//	}
//	}
	else
	{
		NIASSERT(0 && "unkown effect type!!");
	}

	return TRUE;
}

BOOL CSkillManager::ParseAnimation(SpellVisualDesc* pSpell, void* XDef)
{
	if (!pSpell) return FALSE;

	TiXmlElement* pAnimElem = (TiXmlElement*)XDef;
	if (!pAnimElem)
		return FALSE;

	const char* pszValue = pAnimElem->Attribute("castanim");
	if (pszValue && pszValue[0] != '\0')
	{
		NiStrncpy(pSpell->szCastAnimName, 127, pszValue, 128);
	}

	pszValue = pAnimElem->Attribute("attackanim");
	if (pszValue && pszValue[0] != '\0')
	{
		NiStrncpy(pSpell->szAttackAnimName, 127, pszValue, 128);
	}

	pszValue = pAnimElem->Attribute("damageanim");
	if (pszValue && pszValue[0] != '\0')
	{
		NiStrncpy(pSpell->szDamageAnimName, 127, pszValue, 128);
	}

	pszValue = pAnimElem->Attribute("tunnelanim");
	if (pszValue && pszValue[0] != '\0')
	{
		NiStrncpy(pSpell->szTunnelAnimName, 127, pszValue, 128);
	}

	return TRUE;
}

BOOL CSkillManager::ParseSound(SpellEffectDesc& kEffects, void* XDef, bool aura)
{
	TiXmlElement* pSoundElem = (TiXmlElement*)XDef;
	if (!pSoundElem)
		return FALSE;

	SpellSoundDesc* pSoundDesc = NiNew SpellSoundDesc;

	TiXmlElement* pXElem = (TiXmlElement*)XDef;
	const char* pszValue = pXElem->Attribute("source");
	if (pszValue && pszValue[0] != '\0')
	{
		NiStrcpy(pSoundDesc->szSource, MAX_PATH, pszValue);
	}
	else
		return FALSE;
	
	unsigned int looplimit = 1;

	pszValue = pXElem->Attribute("looplimit");
	if (pszValue && pszValue[0] != '\0')
	{
		looplimit = (unsigned int)atoi(pszValue);
	}

	pszValue = pXElem->Attribute("id");
	if (pszValue && pszValue[0] != '\0')
	{
		pSoundDesc->id = atoi(pszValue);
		if (aura)
			pSoundDesc->id += 1000;
	}

	pszValue = pXElem->Attribute("volum");
	if (pszValue && pszValue[0] != '\0')
	{
		float gain = (float)atof(pszValue);
		pSoundDesc->Gain = gain;
	}

	pszValue = pXElem->Attribute("Min");
	if (pszValue && pszValue[0] != '\0')
	{
		float Min = (float)atof(pszValue);
		pSoundDesc->MinDist = Min;
	}

	pszValue = pXElem->Attribute("Max");
	if (pszValue && pszValue[0] != '\0')
	{
		float Max = (float)atof(pszValue);
		pSoundDesc->MaxDist = Max;
	}

	pSoundDesc->uiLoopLimit = looplimit;
	pSoundDesc->bLoop = looplimit > 1? true:false;

	kEffects.PushSound(pSoundDesc);

	return TRUE;
}

BOOL CSkillManager::ParseTrigger(SpellEffectDesc& kEffects, void* XDef)
{
	TiXmlElement* pTriggerElem = (TiXmlElement*)XDef;
	if (!pTriggerElem)
		return FALSE;

	SpellTriggerBase* pTrigger = NULL;
	if (!ParserTriggerEntry(pTrigger, pTriggerElem)) return FALSE;

	if (pTrigger)
	{
		kEffects.PushTrigger(pTrigger);
	}
	else
		return FALSE;

	return TRUE;
}

BOOL CSkillManager::ParseEffects(SpellEffectDesc& kEffects, void* XDef, bool aura)
{
	TiXmlElement* pXElem = (TiXmlElement*)XDef;
	TiXmlElement* pEffectElem = pXElem->FirstChildElement("Effect");
	while (pEffectElem)
	{
		SpellEffectNode* pEffectEntry = NULL;
		ParseEffectEntry(pEffectEntry, pEffectElem, aura);
		if (pEffectEntry)
		{
			kEffects.PushEffect(pEffectEntry);
		}

		pEffectElem = pEffectElem->NextSiblingElement("Effect");
	}

	return TRUE;
}

BOOL CSkillManager::ParseAuraAttr(SpellVisualDesc* pSpell, void* XDef)
{
	TiXmlElement* pXElem = (TiXmlElement*)XDef;
	const char* pszValue = pXElem->Attribute("refid");

	if (pszValue && pszValue[0] != '\0')
		pSpell->auraeffid = atoi(pszValue);

	return FALSE;
}

BOOL CSkillManager::ParseApperanceAttr(SpellVisualDesc* pSpell, void* XDef)
{
	TiXmlElement* pXElem = (TiXmlElement*)XDef;
	const char* pszValue = pXElem->Attribute("hideweapon");
	if (pszValue && pszValue[0])
	{
		pSpell->m_ApperanceDesc.bHideWeapon = !NiStricmp("true", pszValue);
		return TRUE;
	}

	return FALSE;
}

ESpellClass CSkillManager::GetSpellClass(const char* pszClsName)
{
	if ( NiStricmp(pszClsName,"rush") == 0)
	{
		return SPELL_CLS_RUSH;
	}
	return SPELL_CLS_BASE;
}

INT CSkillManager::GetProfession(const char* pszText)
{
	if (NiStricmp(pszText, "战士") == 0)
	{
		return CLASS_WARRIOR;
	}else if (NiStricmp(pszText,"箭师") == 0)
	{
		return CLASS_BOW;
	}else if (NiStricmp(pszText, "法师") == 0)
	{
		return CLASS_MAGE;
	}else if (NiStricmp(pszText, "道士") == 0)
	{
		return CLASS_PRIEST;
	}else if(NiStricmp(pszText, "ALL") == 0)
	{
		return -1;
	}else if (NiStricmp(pszText, "羽箭"))
	{
		return CLASS_BOW;
	}else if (NiStricmp(pszText, "箭灵者"))
	{
		return CLASS_BOW;
	}else if (NiStricmp(pszText, "控兽者"))
	{
		return CLASS_BOW;
	}else if (NiStricmp(pszText, "奥法箭使"))
	{
		return CLASS_BOW;
	}
	else
	{
		NILOG("unknown profession \"%s\"\n", pszText);
			return -1;
	}
}

SpellTargetRelative CSkillManager::GetRelative(const char* pszText)
{
	char Buf[MAX_PATH];
	NiStrcpy(Buf, MAX_PATH, pszText);
	pszText = TrimSpace(Buf);
	if (NiStricmp(pszText, "SELF") == 0)
	{
		return STR_SELF;
	}else if (NiStricmp(pszText, "FRIENDLY") == 0)
	{
		return STR_FRIENDLY;
	}else if (NiStricmp(pszText, "HOSTILE") == 0)
	{
		return STR_hostile;
	}else if (NiStricmp(pszText, "NEUTRALLY") == 0)
	{
		return STR_neutrally;
	}else
	{
		NILOG("Unknown relative \"%s\"\n", pszText);
	}

	return STR_UNKNOWN;
}

UINT CSkillManager::GetObjectClass(const char* pszText)
{
	if (NiStricmp(pszText, "item") == 0)
	{
		return TYPE_ITEM;
	}else if (NiStricmp(pszText, "unit") == 0)
	{
		return TYPE_UNIT;
	}else if (NiStricmp(pszText, "pos") == 0)
	{
		return STC_MAPPOSITION;
	}else
	{
		NILOG("unknown target class %s \n", pszText);
	}
	return 0;
}
