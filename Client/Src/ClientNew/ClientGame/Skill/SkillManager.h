#pragma once

#include "SkillInfo.h"

enum CooldownTypes
{
	COOLDOWN_TYPE_SPELL			= 0,
	COOLDOWN_TYPE_CATEGORY		= 1,
	NUM_COOLDOWN_TYPES,
};
enum 
{
	TestNormal = 0,		  //正常
	NeedMoreMP ,		//需要更多MP
	TooFar,           //对象距离太远
	TooNear,          //对象距离太近
	NeedSelObj ,      //需要选择对象
	AttributePassive,  //被动技能
	TestError,
};
struct PlayerCooldown
{
	uint32 ExpireTime;
	uint32 ItemId;
	uint32 SpellId;
};

class CSkillManager : public NiMemObject
{
public:
	CSkillManager(void);
	~CSkillManager(void);
	BOOL Initialize();
	void Destroy();
	inline SpellTemplate* GetSpellTemplate(SpellID id) const
	{
		std::map<SpellID, SpellTemplate*>::const_iterator itr = m_SpellTemplates.find(id);
		if(itr!=m_SpellTemplates.end())
			return itr->second;
		return NULL;
	}

	SpellVisualDesc* GetSpellVisual(int iSpellVisualID) const {
		std::map<uint32, SpellVisualDesc*>::const_iterator it = m_spellVisualTable.find(iSpellVisualID);
		if (it != m_spellVisualTable.end())
			return it->second;

		return NULL;
	}

	AuraVisualDesc* GetAuraVisual(int iAuraID) const {
		std::map<uint32, AuraVisualDesc*>::const_iterator it = m_auraVisualTable.find(iAuraID);
		if (it != m_auraVisualTable.end())
			return it->second;

		return NULL;
	}

	UINT TestSkillState(uint32 spell);

	void AddCooldown(uint32 type, uint32 misc, uint32 remainTime, uint32 spellid, uint32 itemid);
	void AddGroupCooldown(uint32 type, uint32 remainTime, uint32 SpellGroup);
	void AddGlobalCooldown(uint32 remainTime);
	void CooldownCancel(uint32 spellid);
	void CooldownCancel_Group(uint32 SpellGroup);
	bool CooldownCanCast(uint32 spellid);
	void ClearCooldown();

//	CEffectBase* PopulateEffect(CCharacter* owner, const Target* const targets, 
//		SpellEffectNode* spelleffnode, SpellTemplate* pkSpellTemplate, bool active = true);
//	void PopulateNextEffect(CCharacter* owner, const Target* const targets, 
//		SpellEffectNode* spelleffnode, SpellTemplate* pkSpellTemplate, CEffectBase* pkEffect);

	void GetEffectsByTrigID(int spellid, int trigid, std::vector<SpellEffectNode*>& effects);
	void GetSpellEffects(int spellid, int trigevt, int action, std::vector<SpellEffectNode*>& effects);
	void GetAuraEffects(int spellid, int trigevt, int action, std::vector<SpellEffectNode*>& effects);

	void Damage(ui64 caster, ui64 target, uint32 spellid);
	void PeriodicDamage(uint64 caster, uint64 target, uint32 spellid);
	void EnvironmentalDamage(uint64 target, uint8 type);
	void HealHP(uint64 caster, uint64 target, ui32 spellid);
	void HealMP(uint64 caster, uint64 target, ui32 spellid);

	bool GetCooldownTime(uint32 spellid, uint32& uiRecoverTime, uint32& uiRemainTime);
	bool GetItemCooldownTime(uint32 itemid, uint32& uiRecoverTime, uint32& uiRemainTime);
	bool GetCategoryCooldownTime(uint32 spellid, uint32& uiRecoverTime, uint32& uiRemainTime);
	bool GetGlobalCooldownTime(uint32 spellid, uint32& uiGlobalCooldown, uint32& uiRemainTime);

protected:
	BOOL LoadVisualTablesFromFile();
	BOOL LoadVisualTableFromFile(const char* pszFileName);
	BOOL LoadVisualTableFromMemory(const char* pszFileName, const char* pBuff);
	BOOL LoadSpellVisualEntry(void* pXInfo);
	BOOL LoadAuraVisualEntry(void* pXInfo);

	BOOL InitSpellTempalte();

	ESpellClass GetSpellClass(const char* pszClsName);
	INT GetProfession(const char* pszText);
	SpellTargetRelative GetRelative(const char* pszText);
	UINT GetObjectClass(const char* pszText);

	BOOL ParseSound(SpellEffectDesc& kEffects, void* XDef, bool aura=false);
	BOOL ParseTrigger(SpellEffectDesc& kEffects, void* XDef);
	BOOL ParseEffects(SpellEffectDesc& kEffects, void* XDef, bool aura = false);
	BOOL ParseSpellDef(SpellVisualDesc* pSpell, void* XDef);

	BOOL ParseAuraAttr(SpellVisualDesc* pSpell, void* XDef);
	BOOL ParseApperanceAttr(SpellVisualDesc* pSpell, void* XDef);
	BOOL ParseEffectEntry(SpellEffectNode*& EffectEntry, void* XDef, bool aura);
	BOOL ParseAnimation(SpellVisualDesc* pSpell, void* XDef);
	BOOL ParserTriggerEntry(SpellTriggerBase*& pTriggerEntry, void* XDef);

private:
	std::map<SpellID, SpellTemplate*> m_SpellTemplates;			// about 400 spells.
	std::map<uint32, SpellVisualDesc*> m_spellVisualTable;
	std::map<uint32, AuraVisualDesc*> m_auraVisualTable;

	typedef std::map<uint32, PlayerCooldown> PlayerCooldownMap;
	PlayerCooldownMap m_cooldownMap[NUM_COOLDOWN_TYPES];

	uint32 m_globalCooldown;
};
