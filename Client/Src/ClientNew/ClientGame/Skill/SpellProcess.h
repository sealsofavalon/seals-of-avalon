#ifndef _SPELL_PROCESS_
#define _SPELL_PROCESS_

#include "SkillInfo.h"

class CCharacter;
class SpellTemplate;

class SpellInstance : public NiMemObject
{
public:
	SpellInstance(CCharacter* pOwner);
	virtual ~SpellInstance();

	inline void SetOwner(CCharacter* Owner) { m_pOwner = Owner; }
	virtual BOOL IsSpellAttack() const { return TRUE; }

	void ResetTime();

	virtual BOOL Update(float fDelta);
	inline const SpellTemplate* GetTemplate() const { return m_SpellTemplate;}

	void ClearActiveEffectMap();

	class EffectTriggerManager
	{
	public:
		EffectTriggerManager(SpellInstance* pkSpellInst) : m_pkSpellInst(pkSpellInst) {
		}

		~EffectTriggerManager() {
			Reset();
		}

		typedef std::multimap<int, SpellEffectNode*> TRIGGERPAIR_MAP;
		typedef std::vector<SpellTriggerBase*> TRIGGERLIST;

		void Reset();

		void AddTrigger(SpellTriggerBase* pkTriggerInfo);
		void AddEffectTriggerPair(int iTriggerID, SpellEffectNode* pkEffectInfoNode);
		void Update(int iState, float fTime);

		void OnEvent(int iEvent, void* pvParam);
		void OnEvent(int iSpellID, int iSpellEvent, void* pvParam);

		void ExecuteTriggerByAction(const int& iAction);
		void ExecuteTriggerByEvent(const int& iEvent, bool bAll = true);

	protected:
		void ProcessTimeTriggers(float fTime);
		void ProcessEvtTriggers(const int& iEvent, void* pvParam, bool bIgnoreParam = false);
		void ProcessTrigger(const int& iTriggerID, const int& iAction);

		void ProcessEvtTriggers(const int& iSpellID, int iEvent, void* pvParam, bool bIgnoreParam =false );
		void ProcessTrigger(const int& iSpellID, const int& iTriggerID, const int& iAction);

	private:
		TRIGGERPAIR_MAP m_TriggerMap;
		TRIGGERLIST m_TimeTrigs;
		TRIGGERLIST m_EvtTrigs;
	

		SpellInstance* m_pkSpellInst;

	};

	void BeginCast(SpellID spellID, const uint32& castTime, const SpellCastTargets& spellCastTargets);
	BOOL UpdateCast(float fTime);
	bool IsSpellCast(SpellID spellID);

	void BeginTunnel(SpellID spellID, const uint32& castTime );
	BOOL UpdateTunnel( float fTime );
	void EndTunnel();

	void UpdateTriggers();
	void EndCast();

	void UpdateCastBar();

	void CastDelay(uint32 uiDelay);
	void TunnelDelay(uint32 uiDelay);
	void SpellFailure(uint32 spellid, uint8 uiErrorCode);

	virtual BOOL BeginMagic(const SpellID& spellID, const vector<ui64>& vTargetHit, const SpellCastTargets& spellCastTargets /*const Target& Targets*/ );
	virtual void EndMagic();

	virtual BOOL BeginAttack(const Target& Targets);
	virtual BOOL UpdateAttack(float fTime);
	virtual void EndAttack();

	void EndSpell();

	void ChangeSpellState(int iState);
	virtual void DamageTargets(const Target& Targets);

	bool EffectSetup(const SpellID& spellID);
	void ClearBreakEffects();

	float GetCastElapse();
	float GetAttackElapse();
	float GetSpellElapse();

	ESpellStep GetStage() const {
		return m_AttackStep;
	}

	void NeedRelease( bool isNeedRelease ){ m_bNeedRelease = isNeedRelease;}
	void Reset();

protected:

	bool InitSkillTemplate(const SpellID& spellID);
	void OnEvent(int iEvent, void* pvParam);

	void EmitEffect(const std::vector<SpellEffectNode*>& effectnodes);
	void EmitEffect(SpellEffectNode* pkEffectNodes);
	void EmitEffect(int iSpellID, int iTriggerID);

	bool ReleaseEffect(SpellEffectNode* pkEffectNode);
	bool ReleaseEffect(int iSpellID, int iTriggerID);

	void SetTargets(const vector<ui64>& vTargetHit, const SpellCastTargets& spellCastTargets);
	void ChangeLocation(const SpellCastTargets& spellCastTargets);
	BOOL ChangeApperance();
	void RestoreApperance();

	float m_fNextEndTime;

protected:
	CCharacter* m_pOwner;
	SpellTemplate* m_SpellTemplate;
	ESpellStep m_AttackStep;
	DWORD m_AttackTime;

	// float m_fCastFullTime;
	uint32 m_uiCastFullTime;
	uint32 m_uiCastOverTime;
	float m_fCastStartTime;
	float m_fAttackStartTime;

	float m_fTunnelStartTime;

	float m_fUpdateTime;
	bool  m_bWaitAnim;

	bool m_bHideWeapon;
	bool m_bIsEffectSetup;

	SpellID	m_SpellID;
	Target m_targetList;
	Target m_targetListdetail;


	EffectTriggerManager* m_pkTriggerMan;
	NiTMap<SpellEffectNode*, CEffectBase*> m_activeEffMap;

	int m_iCastAniCode;
	int m_iTunnelAniCode;
	int m_iAnimCode;

	bool		m_bNeedRelease;
};

class CRandomMersenne;

class NormalAttackInstance : public SpellInstance
{
	class ArcherData : public NiMemObject
	{
	public:
		ArcherData() 
		{
			m_bPreparedArrow = FALSE;
			m_bProjectiled = FALSE;
		}
		BOOL m_bPreparedArrow;
		BOOL m_bProjectiled;
		float m_fTime;
	};
	enum
	{
		ATK_SERIAL_MAX = 3,
	};
public:
	NormalAttackInstance(CCharacter* pOwner);
	virtual ~NormalAttackInstance();

	BOOL IsAvailibleAttackTarget(CCharacter* pkTarget);

	BOOL BeginAttack(const Target& Targets);
	BOOL UpdateAttack(float fTime);
	void EndAttack();

	void EmitAttackSoundEffect();
	virtual BOOL IsSpellAttack() const { 
		return FALSE; 
	}

	void SetBow(){ m_bBow = true; }

protected:
	// 攻击序列, 普通攻击由客户端随机,交由服务器同步.
	void	SetAttackSerial(UINT SerialNum)	{ m_AttackSerial = SerialNum; }
	UINT	GetAttackSerial() const			{ return m_AttackSerial; }

protected:
	// CCharacter* m_pTarget;
	SYObjID m_TargetObjectID;
	UINT  m_AttackSerial;
	bool m_bStopAttack;

	bool m_bBow;
	CRandomMersenne* voicegen;
};


#endif //_SPELL_PROCESS_