#include "StdAfx.h"
#include "SpellRush.h"
#include "Character.h"
#include "Player.h"
#include "ClientApp.h"
#include "PlayerInputMgr.h"
#include "Effect/EffectManager.h"
#include "ObjectManager.h"

SpellRush::SpellRush(CCharacter* pkOwner) : SpellInstance(pkOwner)
{
}

SpellRush::~SpellRush(void)
{
}

BOOL SpellRush::BeginMagic(SpellID spellID, const SpellCastTargets& spellCastTargets)
{
	if (!SpellInstance::BeginMagic(spellID, spellCastTargets))
		return FALSE;

	NiPoint3 kTragetPos;
	uint32 mask = spellCastTargets.m_targetMask;
	if ( mask & (TARGET_FLAG_OBJECT | TARGET_FLAG_UNIT | TARGET_FLAG_CORPSE | TARGET_FLAG_CORPSE2 ))
	{
		uint64 targetID = spellCastTargets.m_unitTarget;
		CGameObject* pkObject = (CGameObject*)ObjectMgr->GetObject(targetID);
		if (!pkObject)
			return FALSE;

		kTragetPos = pkObject->GetPosition();
	}
	else
	if ( mask & TARGET_FLAG_SOURCE_LOCATION )
	{
		kTragetPos = NiPoint3(spellCastTargets.m_srcX, spellCastTargets.m_srcY, spellCastTargets.m_srcZ);
	}
	else
	if ( mask & TARGET_FLAG_DEST_LOCATION )
	{
		kTragetPos = NiPoint3(spellCastTargets.m_destX, spellCastTargets.m_destY, spellCastTargets.m_destZ);
	}
	else
	{
		return FALSE;
	}

	m_pOwner->MoveTo(kTragetPos, CMF_RushMove);
	m_pOwner->SetMoveState(CMS_RUSH);

	return TRUE;
}

void SpellRush::EndMagic()
{
//	m_pOwner->SetMoveState(CMS_WALK);
	//m_pOwner->m_runSpeed /= 3;

	SpellInstance::EndMagic();
}