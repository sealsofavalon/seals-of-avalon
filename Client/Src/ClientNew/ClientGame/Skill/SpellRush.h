#pragma once
#include "SpellProcess.h"

class SpellRush : public SpellInstance
{
public:
	SpellRush(CCharacter* pkOwner);
	~SpellRush(void);
	virtual BOOL BeginMagic(SpellID spellID, const SpellCastTargets& spellCastTargets);
	virtual void EndMagic();
	//float m_fSavedSpeed;
};
