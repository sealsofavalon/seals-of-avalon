#include "StdAfx.h"
#include "Slot.h"

void CSlot::Clear()
{
	SetPos(255);
	SetSlotIdx(SI_MAX);
	SetNum(1);
}

void CSlot::Copy(CSlot& slot)
{
	SetPos(slot.GetPos());
	SetSlotIdx(slot.GetSlotIdx());
	SetNum(slot.GetNum());
}