#ifndef SLOT_H
#define SLOT_H

enum eSlotType
{
	ST_NONE,
	ST_ITEM,
	ST_EQUIP,
	ST_SKILL,
	ST_QUICK,
	ST_STYLE_QUICK,
	ST_DUMMY_ITEM,
};

enum eSlotContainerIndex 
{
	SI_TEMPINVENTORY = 0,
	SI_INVENTORY,
	SI_EQUIPMENT,
	SI_WAREHOUSE,
	SI_SKILL,
	SI_QUICK,
	SI_STYLE,
	SI_MAX,					
	SI_EVENT_INVENTORY,
	SI_TRADE,
	SI_NPCSTORE,			
	SI_ENCHANT,
	SI_RANKUP,
	SI_VENDOR_SELL,
	SI_VENDOR_BUY,
};

#define __UNUSED(variable) variable

	class CSlot
{
public:
	CSlot():m_Pos(0),m_nNum(1),m_SlotIdx(0){}
	virtual ~CSlot(){}

	virtual void Clear();
	virtual void Copy(CSlot& slot);

	// common
	virtual eSlotType GetSlotType() const { return ST_NONE; }
	virtual DWORD GetCode() const { return 0; }

	virtual void SetSlotType(eSlotType type) { __UNUSED(type); }
	virtual void SetCode(DWORD code) { __UNUSED(code); }

	// item	
	virtual DWORD GetSerial() const { return 0;	}
	virtual UINT GetNum() const { return m_nNum; }
	virtual void SetSerial(DWORD serial) { __UNUSED(serial); }
	virtual void SetNum(UINT num) { m_nNum = num; }

	// added
	void SetPos(BYTE pos) { m_Pos = pos; }
	BYTE GetPos() const { return m_Pos; }

	void SetSlotIdx(BYTE idx) { m_SlotIdx = idx; }
	BYTE GetSlotIdx() const	{ return m_SlotIdx; }

private:
	BYTE m_SlotIdx;
	BYTE m_Pos;
	UINT m_nNum;
};

#endif