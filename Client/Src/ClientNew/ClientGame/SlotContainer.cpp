#include "StdAfx.h"
#include "SlotContainer.h"
#include "ItemSlot.h"

CSlotContainer::CSlotContainer()
:m_nSlotNum(0)
,m_nMaxSlotNum(0)
,m_ppSlotArray(NULL)
,m_SlotIdx(SI_MAX)
{
}

CSlotContainer::~CSlotContainer()
{
	Release();
}

void CSlotContainer::Release()
{
	if(m_ppSlotArray)
	{
		for(BYTE i = 0; i < GetMaxSlotNum(); ++i)
		{
			if(m_ppSlotArray[i])
			{
				delete m_ppSlotArray[i];
				m_ppSlotArray[i] = NULL;
			}
		}
		delete[] m_ppSlotArray;
		m_ppSlotArray = NULL;
	}
}

void CSlotContainer::ClearAll()
{
	m_nSlotNum = 0;
	for(BYTE i = 0 ; i < GetMaxSlotNum() ; ++i)
	{
		m_ppSlotArray[i]->Clear();
		m_ppSlotArray[i]->SetPos(i);
		m_ppSlotArray[i]->SetSlotIdx(GetSlotIdx());
	}
}

void CSlotContainer::UpdateSlot(BYTE AtPos, CSlot& rSlot)
{
	NIASSERT(AtPos < GetMaxSlotNum());
	NIASSERT(!IsEmpty(AtPos));

	if(AtPos >= GetMaxSlotNum())	
		return;

	if(IsEmpty(AtPos))			
		return;

	m_ppSlotArray[AtPos]->Copy(rSlot);
	m_ppSlotArray[AtPos]->SetPos(AtPos);
	m_ppSlotArray[AtPos]->SetSlotIdx(GetSlotIdx());
}

bool CSlotContainer::InsertSlot(BYTE AtPos, CSlot& rSlot)
{
	NIASSERT(AtPos < GetMaxSlotNum());
	NIASSERT(IsEmpty(AtPos));

	if(AtPos >= GetMaxSlotNum())	
		return false;

	if(!IsEmpty(AtPos))			
		return false;

	m_ppSlotArray[AtPos]->Copy(rSlot);
	m_ppSlotArray[AtPos]->SetPos(AtPos);
	m_ppSlotArray[AtPos]->SetSlotIdx(GetSlotIdx());
	++m_nSlotNum;
	NIASSERT(m_nSlotNum <= GetMaxSlotNum());

	return true;
}

void CSlotContainer::DeleteSlot(BYTE AtPos, CSlot* pSlotOut)
{
	NIASSERT(AtPos < GetMaxSlotNum());

	if(AtPos >= GetMaxSlotNum())	
		return;

	if(pSlotOut)
	{
		pSlotOut->Copy(*m_ppSlotArray[AtPos]);
	}

	if(m_nSlotNum > 0)
		--m_nSlotNum;

	m_ppSlotArray[AtPos]->Clear();

	NIASSERT(m_nSlotNum <= GetMaxSlotNum());
}

CSlot* CSlotContainer::GetSlot(BYTE AtPos)
{
	NIASSERT(AtPos < GetMaxSlotNum());
	return m_ppSlotArray[AtPos];
}

bool CSlotContainer::GetEmptyPos(BYTE& emptyPosOut)
{
	BYTE pos = 0;
	while(pos < GetMaxSlotNum())
	{
		if(IsEmpty(pos))
		{
			emptyPosOut = pos;
			return true;
		}
		++pos;
	}

	emptyPosOut = pos;

	return false;
}

bool CSlotContainer::IsEmpty(BYTE AtPos)
{
	return (m_ppSlotArray[AtPos]->GetCode() == 0);
}