#ifndef SLOTCONTAINER_H
#define SLOTCONTAINER_H

#include "Slot.h"

class CSlotContainer
{
public:
	CSlotContainer();
	virtual ~CSlotContainer();

	virtual void Init(BYTE MaxSlotSize, BYTE SlotIdx) = 0;
	virtual void Release();
	virtual void ClearAll();

	//CSlot& GetSlot(BYTE AtPos);
	CSlot* GetSlot(BYTE AtPos);

	virtual void UpdateSlot(BYTE AtPos, CSlot& rSlot);
	virtual bool InsertSlot(BYTE AtPos, CSlot& rSlot);
	virtual void DeleteSlot(BYTE AtPos, CSlot* pSlotOut);

	virtual bool GetEmptyPos(BYTE& emptyPosOut);

	inline BYTE	GetSlotNum() { return m_nSlotNum; }
	inline BYTE	GetMaxSlotNum() { return m_nMaxSlotNum;	}

	virtual bool IsEmpty(BYTE AtPos);

	BYTE GetSlotIdx() { return m_SlotIdx; }

protected:
	BYTE m_nSlotNum;
	BYTE m_nMaxSlotNum;
	CSlot**	m_ppSlotArray;
	BYTE m_SlotIdx;
};

#endif
