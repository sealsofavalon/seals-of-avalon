#ifndef _SPELL_EFFECT_H_
#define _SPELL_EFFECT_H_


#include "Skill/SkillInfo.h"
#include <NiMemObject.h>

#include "Effect/EffectBase.h"
#include "ResourceManager.h"


class CGameObject;

class SpellEffectEmit : public NiMemObject
{
public:

	struct EmitParam
	{
		//CGameObject* m_pkOwner;
		SYObjID m_OwnerID;
		Target m_kTargets;
		Target m_kTargetsDetail;
		int m_iSpellid;
		std::vector<SpellEffectNode*> m_vEffects;
	};

	struct DelayEmiter
	{
		float m_fTime;
		bool m_bAll;
		EmitParam m_kParam;
	};

	struct EmitResult
	{
		SpellEffectNode* node;
		CEffectBase* instance;
	};

	static SpellEffectEmit* Get() {
		return m_pkSInst;
	}

	static bool Create() {
		m_pkSInst = NiNew SpellEffectEmit;
		return (m_pkSInst != 0);
	}

	static void Destroy() {
		NiDelete m_pkSInst;
		m_pkSInst = 0;
	}

	CEffectBase* CreateEffect(SpellEffectNode* pEffectNode );
	void Emit(SpellEffectEmit::EmitParam& param, std::vector<SpellEffectEmit::EmitResult>& result, 
		bool all=true, float delay=0.0f);

	bool ReleaseEffect(CEffectBase* pkEffect);
	void AddSound(SpellEffectNode* pkEffectNode, SpellTemplate* pkSpellTemplate, NiNode* pkNode);
	void EmitSound(NiNode* pkNode, const SpellSoundDesc& desc);

	void Update(float fTime);

	CEffectBase* CreateNifEffect( SpellEffectNifNode* pEffectNode,float scale = 1.0f );
	CEffectBase* CreateDecalEffect(SpellDecalEffectNode* pEffectNode);
	CEffectBase* CreateTrailEffect(SpellTrailEffectNode* pEffectNode);
	CEffectBase* CreateProjectileEffect(SpellProjectileEffectNode* pEffectNode);
	CEffectBase* CreatePosConstraintEffect(SpellPosConstraintEffectNode* pkEffectNode);
	CEffectBase* CreateChainEffect(SpellChainEffectNode* pkEffectNode);

protected:
	CEffectBase* EmitEffect(CGameObject* pkOwner, const Target* const pkTargetList, SpellEffectNode* pkEffectNode, 
		SpellTemplate* pkSpellTemplate, bool bActivate = true);

protected:
	SpellEffectEmit();
	~SpellEffectEmit();

private:
	static SpellEffectEmit* m_pkSInst;
	AudioSourcePtr m_pkTempSource;

	std::list<DelayEmiter> m_vDelayEmiters;

};

class EffectPostEvent : public CEffectBase::EffectCallbackObject
{
public:
	EffectPostEvent(void) {};
	~EffectPostEvent(void) {};

	void ActivationChanged(CEffectBase* effect, bool activated);
};


#endif //_SPELL_EFFECT_H_