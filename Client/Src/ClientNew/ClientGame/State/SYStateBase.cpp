#include "StdAfx.h"
#include "SYStateBase.h"

#include "Character.h"
#include "Console.h"


SYStateActorBase::SYStateActorBase(void)
{
	InitState();
}

SYStateActorBase::~SYStateActorBase(void)
{
}

void SYStateActorBase::InitState()
{
	m_eCurAtionState = STATE_ACTIONIDLE;
	m_eNextActionState = STATE_UNKNOWN;
	m_eCurPostureState = STATE_IDLE;
	m_eNextPosetureState = STATE_UNKNOWN;
	m_CurAction.EventID = ACTION_IDLE;

	for (unsigned int ui = 0; ui < SL_Max; ui++)
	{
		m_bBlocked[ui] = false;
	}
}

void SYStateActorBase::ResetState()
{
	ForceStateChange(STATE_ACTIONIDLE, CUR_TIME);
	ForceStateChange(STATE_IDLE, CUR_TIME);

	for (unsigned int ui = 0; ui < SL_Max; ui++)
	{
		m_bBlocked[ui] = false;
	}
}

BOOL SYStateActorBase::SetNextState(State eNextState, DWORD dwTime)
{
	IStateProxy* pkStateInst = GetStateInstance(eNextState);
	if (pkStateInst)
	{
		StateLayer eLayer = pkStateInst->GetLayer();
		if (!pkStateInst->CanTransite(this, dwTime, eNextState))
			return FALSE;

		if (eLayer == SL_Poseture)
		{
			SetNextPosetureState(eNextState);
		}
		else
		if (eLayer == SL_Action)
		{
			SetNextActionState(eNextState);
		}
		else
		{
			NIASSERT(0);
			return FALSE;
		}
	}

	return TRUE;
}

IStateProxy* SYStateActorBase::ForceStateChange(State eNextState, DWORD dwTime)
{
	IStateProxy* pkStateInst = GetStateInstance(eNextState);
	if (pkStateInst)
	{
		StateLayer eLayer = pkStateInst->GetLayer();
		
		if (IsLayerBlocked(eLayer))
			return NULL;
		
		

		if (!pkStateInst->CanTransite(this, dwTime, eNextState))
			return NULL;

		if (eLayer == SL_Poseture)
		{
			SetNextPosetureState(eNextState);
		}
		else
		if (eLayer == SL_Action)
		{
			SetNextActionState(eNextState);
		}
		else
		{
			NIASSERT(0);
			return NULL;
		}

		CommitStateChange(dwTime, eLayer);
		return pkStateInst;
	}

	return NULL;
}

INT SYStateActorBase::GetNumQueueAction()
{
	return m_ActionQueue.size();
}

void SYStateActorBase::ClearActionQueue()
{
	while(m_ActionQueue.size() > 0)
	{
		m_ActionQueue.pop();
	}
}

BOOL SYStateActorBase::ProcessAction(const StateAction* Action)
{
	NIASSERT(Action);
	m_CurAction = (const SYStateAction&)*Action;

	/*switch(m_CurAction.EventID)
	{
	case :

	}*/

	return TRUE;
}

void SYStateActorBase::PushAction(const StateAction* QueueAction)
{
	NIASSERT(QueueAction);
	const SYStateAction& Action = (const SYStateAction&)*QueueAction;
	m_ActionQueue.push(Action);
}

void SYStateActorBase::SetCurAction(const StateAction* QueueAction)
{
	m_CurAction = (const SYStateAction&)*QueueAction;
}

BOOL SYStateActorBase::ProcessNextAction()
{
	if (GetNumQueueAction() > 0)
	{
		SYStateAction action;

		while (m_ActionQueue.size() > 0)
		{
			action = m_ActionQueue.front();
			m_ActionQueue.pop();

			// �������.

			if (ProcessAction(&action))
			{
				return 1;	
			}			
		}

		SetNextState(STATE_IDLE, CUR_TIME);
	}
	
	return FALSE;
}

BOOL SYStateActorBase::CommitStateChange(DWORD dwTime, StateLayer eLayer)
{
	BOOL bResult = TRUE;

	if (eLayer == SL_Poseture)
	{
		State eSrcState, eDestState;

		eSrcState = GetCurPostureState();
		eDestState = GetNextPosetureState();

		IStateProxy* pkSrcStateInst = GetStateInstance(eSrcState);
		IStateProxy* pkDestStateInst = GetStateInstance(eDestState);

		if (!pkSrcStateInst || !pkDestStateInst)
			return FALSE;

		bResult = pkSrcStateInst->End(this, dwTime);
		bResult = pkDestStateInst->Start(this, dwTime);

		// commitchange succeed
		if (bResult)
		{
			m_eCurPostureState = m_eNextPosetureState;
			m_eNextPosetureState = STATE_UNKNOWN;
		}
	}
	else
	if (eLayer == SL_Action)
	{
		State eSrcState, eDestState;

		eSrcState = GetCurActionState();
		eDestState = GetNextActionState();

		IStateProxy* pkSrcStateInst = GetStateInstance(eSrcState);
		IStateProxy* pkDestStateInst = GetStateInstance(eDestState);

		if (!pkSrcStateInst || !pkDestStateInst)
			return FALSE;

		bResult = pkSrcStateInst->End(this, dwTime);
		bResult = pkDestStateInst->Start(this, dwTime);

		// commitchange succeed
		if (bResult)
		{
			m_eCurAtionState = m_eNextActionState;
			m_eNextActionState = STATE_UNKNOWN;
		}
	}
	else
	{
		NIASSERT(0);
	}

	return bResult;
}

void SYStateActorBase::UpdateState(float fTime, float fDelta, StateLayer eLayer)
{
	if (IsLayerBlocked(eLayer))
		return;

	EStateProcessResult eProcessResult = SPR_END;
	BOOL bCommitChangeState = FALSE;
	DWORD dwTime = float2int32(fTime * 1000.0f);

	State eCurState = STATE_UNKNOWN;
	if (eLayer == SL_Poseture)
	{
		eCurState = GetCurPostureState();
		IStateProxy* pkCurStateInst = GetStateInstance(eCurState);

		if (!pkCurStateInst)
			return;

		eProcessResult = pkCurStateInst->Process(this, fTime, fDelta);
		if (eProcessResult == SPR_HOLD)
			return;

		if (eProcessResult == SPR_END)
		{
			if (GetNextPosetureState() == STATE_UNKNOWN)
			{
				CCharacter* pkChar = (CCharacter*)this ;
				BOOL IsMount = FALSE;
				if (pkChar)
				{
					IsMount = pkChar->isMount();
				}
				if (IsMount)
				{
					m_eNextActionState = STATE_MOUNT_IDLE ;
				}else
				{
					m_eNextPosetureState = STATE_IDLE;
				}
			}

			bCommitChangeState = TRUE;
		}
		else
		if (eProcessResult == SPR_SUSPEND)
		{
			if (GetNextPosetureState() != STATE_UNKNOWN)
			{
				bCommitChangeState = TRUE;
			}
		}
		else
		{
			NIASSERT(0);
		}
	}
	else
	if (eLayer == SL_Action)
	{
		eCurState = GetCurActionState();
		IStateProxy* pkCurStateInst = GetStateInstance(eCurState);
		if (!pkCurStateInst)
			return;

		eProcessResult = pkCurStateInst->Process(this, fTime, fDelta);
		if (eProcessResult == SPR_HOLD)
			return;

		if (eProcessResult == SPR_END)
		{
			if (GetNextActionState() == STATE_UNKNOWN)
			{
				m_eNextActionState = STATE_ACTIONIDLE;
			}

			bCommitChangeState = TRUE;
		}
		else
		if (eProcessResult == SPR_SUSPEND)
		{
			if (GetNextActionState() != STATE_UNKNOWN)
			{
				bCommitChangeState = TRUE;
			}
		}
		else
		{
			NIASSERT(0);
		}
	}
	else
	{
		NIASSERT(0);
	}

	if (bCommitChangeState)
	{
		CommitStateChange(dwTime, eLayer);
	}
}

void SYStateActorBase::UpdateState(float fTime, float fDelta)
{
	UpdateState(fTime, fDelta, SL_Poseture);
	UpdateState(fTime, fDelta, SL_Action);
}

