#include "StdAfx.h"
#include "StateAttack.h"
#include "Character.h"

#include "ObjectManager.h"
//#include "UI/UISYstem.h"
#include "PlayerInputMgr.h"

#include "ClientApp.h"
#include "Skill/SpellProcess.h"

#include "Console.h"



BOOL StateAttack::CanTransite(IStateActor* Owner, DWORD CurTime, State ToState)
{
	return TRUE;
}

BOOL StateAttack::Start(IStateActor* Owner, DWORD CurTime)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	if (!pkChar)
		return FALSE;

	return TRUE;
}

EStateProcessResult StateAttack::Process(IStateActor* Owner, float fTime, float fDelta)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);
	if (!pkChar)
		return SPR_END;

	EStateProcessResult eResult = SPR_SUSPEND;
	NormalAttackInstance* pNAI = (NormalAttackInstance*)pkChar->GetAttackInst(0);
	if (pNAI && !pNAI->IsSpellAttack())
	{
		BOOL bEnd = pNAI->Update(fDelta);
		if (bEnd)
		{
			pNAI->EndAttack();
			//if(pkChar->GetGameObjectType() == GOT_CREATURE && pkChar->GetCurPostureState() == STATE_MOVE)
			//{
			//	pkChar->SetMoveAnimation(pkChar->GetMoveState());
			//}

			return SPR_END;
		}
		return eResult;
	}

	if(pkChar->isMount())
		return SPR_SUSPEND;
	return SPR_END;
}

State StateAttack::End(IStateActor* Owner, DWORD CurTime)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);

	return STATE_ACTIONIDLE;
}


//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////

BOOL StateAttackSkill::CanTransite(IStateActor* Owner, DWORD CurTime, State ToState)
{
	return TRUE;
}

BOOL StateAttackSkill::Start(IStateActor* Owner, DWORD CurTime)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);

	return TRUE;
}

EStateProcessResult StateAttackSkill::Process(IStateActor* Owner, float fTime, float fDelta)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);

	float fCurrentTime = SYState()->ClientApp->GetAccumTime();
	NormalAttackInstance* pNAI = (NormalAttackInstance*)pkChar->GetAttackInst(0);
	if ( pNAI )
	{
		BOOL bEnd = pNAI->Update(fDelta);
		if (bEnd)
		{
			pNAI->EndAttack();
		}
	}
	SpellInstance* pkSI = pkChar->GetAttackInst(1);
	if (pkSI != NULL)
	{
		BOOL bEnd = pkSI->Update(fDelta);
		if (bEnd)
			return SPR_END;
	}

	return SPR_SUSPEND;
}

State StateAttackSkill::End(IStateActor* Owner, DWORD CurTime)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);
	SpellInstance* pSI = (SpellInstance*)pkChar->GetAttackInst(1);
	if (pSI && pSI->IsSpellAttack())
	{
		pSI->EndSpell();
	}

	return STATE_ACTIONIDLE;
}
