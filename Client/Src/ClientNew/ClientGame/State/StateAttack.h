#pragma once
#include "StateBase.h"
#include "SYStateBase.h"

class CCharacter;

// ��ͨ����
class StateAttack : public IStateProxy
{
	PROXY_INSTANCE(StateAttack)

	virtual State GetState() const { return STATE_NORMAL_ATTACK;}
	virtual BOOL CanTransite(IStateActor* Owner, DWORD CurTime, State ToState);
	virtual BOOL Start(IStateActor* Owner, DWORD CurTime);
	virtual EStateProcessResult Process(IStateActor* Owner, float fTime, float fDelta);
	virtual State End(IStateActor* Owner, DWORD CurTime);

	virtual StateLayer GetLayer() { return SL_Action; }
	virtual char* GetStateDesc() { return "StateAttack"; }
};


// ���ܹ���
class StateAttackSkill : public StateAttack
{
	PROXY_INSTANCE(StateAttackSkill)
	virtual State GetState() const { return STATE_SKILL_ATTACK;}
	virtual BOOL CanTransite(IStateActor* Owner, DWORD CurTime, State ToState);
	virtual BOOL Start(IStateActor* Owner, DWORD CurTime);
	virtual EStateProcessResult Process(IStateActor* Owner, float fTime, float fDelta);
	virtual State End(IStateActor* Owner, DWORD CurTime);
	virtual char* GetStateDesc() { return "StateAttackSkill"; }
};