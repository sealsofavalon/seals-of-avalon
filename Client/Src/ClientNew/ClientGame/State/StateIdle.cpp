#include "StdAfx.h"
#include "StateIdle.h"
#include "Player.h"
#include "AudioInterface.h"

#include "ClientApp.h"
#include "Console.h"
#include "Map/Map.h"
#include "ObjectManager.h"
#include "../Network/NetworkManager.h"

#include "SYMount.h"



BOOL StateIdle::CanTransite(IStateActor* Owner, DWORD CurTime, State ToState)
{
	// 可切换到任意状态
	return TRUE;
}

BOOL StateIdle::Start(IStateActor* Owner, DWORD CurTime)
{
	CCharacter* Char = (CCharacter*)Owner;
	NIASSERT(Char);

	if ( (Char->GetMovementFlags() & MOVEFLAG_MOVE_BACKWARD) )
	{
		Char->StopBackward();
	}

	BOOL bStorm = Char->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM);
	Char->laststormstate = bStorm?true:false;
	if (bStorm)
		return TRUE;

	BOOL bIsInCombatState = Char->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);

	if ( !bIsInCombatState || Char->GetUInt32Value(UNIT_FIELD_FORMDISPLAYID) )
	{
		float fCurTime = SYState()->ClientApp->GetAccumTime();
		Char->SetWaitAnimTime(fCurTime);

		{	
			if (!Char->SetIdleAnimation(CAG_IDLE))
			{
				Char->SetIdleAnimation(CAG_WAIT);
			}
		}
	}
	else if ( Char->GetAsPlayer() != NULL )
	{
		CPlayer* pkPlayer = (CPlayer*)Char;
		NIASSERT(pkPlayer);
		if (!pkPlayer)
			return FALSE;

		pkPlayer->SetAttackPrepare();
	}
	else if ( Char->GetGameObjectType() == GOT_CREATURE)
	{
		Char->SetIdleAnimation(CAG_IDLE);
	}

	if ( Char->GetAsPlayer() != NULL )
	{
		CPlayer* pkPlayer = (CPlayer*)Char;
		NIASSERT(pkPlayer);
		if (!pkPlayer)
			return FALSE;

		AudioSource* pSouce = pkPlayer->GetSwimmingSound();

		if ( pSouce )
		{
			pSouce->Stop();
		}
	}
	return TRUE;
}

extern bool g_just_leap;

EStateProcessResult StateIdle::Process(IStateActor* Owner, float fTime, float fDelta)
{
	CCharacter* Char = (CCharacter*)Owner;

	bool bNeedReset = false;
	if ( Char->GetTelePortPara().bTelePort )
	{
		//if ( Char->IsLocalPlayer() )
		//{
		//	CPlayerLocal* pkLocal = (CPlayerLocal*)Char;
		//	MSG_C2S::stMove_Teleport_Ack stTeleAck;
		//	stTeleAck.guid = pkLocal->GetGUID();
		//	NetworkMgr->SendPacket(stTeleAck);
		//	//pkLocal->SendMovementPacket();
		//}

		NiPoint3 ptPos;
		ptPos.x = Char->GetTelePortPara().ptTelePos.x;
		ptPos.y = Char->GetTelePortPara().ptTelePos.y;
		ptPos.z = Char->GetTelePortPara().ptTelePos.z;

		float h = CMap::Get()->GetHeight(ptPos.x, ptPos.y);
		if( h > ptPos.z )
			ptPos.z = h;
		 Char->SetPosition( ptPos );
		 Char->SetRotate(NiPoint3(0, 0,  Char->GetTelePortPara().fTeleAngle ));
		 Char->SetServerAngle(  Char->GetTelePortPara().fTeleAngle );

		 Char->GetTelePortPara().ReSet();		
		if ( Char->IsLocalPlayer() )
		{
			CPlayerLocal* pkLocal = (CPlayerLocal*)Char;
			MSG_C2S::stMove_Teleport_Ack stTeleAck;
			stTeleAck.guid = pkLocal->GetGUID();
			NetworkMgr->SendPacket(stTeleAck);
			pkLocal->SendMovementPacket();

			if( !g_just_leap )
			{
				pkLocal->GetCamera().LookPlayer();
				if ( Char->IsJumping() )
				{
					Char->EndJump();
				}
			}
			else
				g_just_leap = false;
		}

		bool bAnimEnd = Char->IsCurAnimEndInTime(fTime);
		if ( !bAnimEnd )
		{
			bNeedReset = true;
		}
		
	}

	if ( Char->IsJumping() && Char->GetPhsyicState() == PHYS_Falling)
	{
		Char->ForceStateChange( STATE_MOVE, CUR_TIME );
		Char->SetJumpAnimation( EJS_FALLING, true );
		return SPR_SUSPEND;
	}
	
	if ( Char->GetPhsyicState() == PHYS_Falling && !Char->IsJumping() && !Char->laststormstate)
	{
		Char->ForceStateChange( STATE_MOVE, CUR_TIME );
		Char->SetJumpState( EJS_FALLING );
		Char->SetJumpAnimation( EJS_FALLING, true );
		return SPR_SUSPEND;
	}
	
	

	NIASSERT(Char);

	bool doanim = false;
	BOOL curstormstate = Char->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM);

	if (!curstormstate && Char->laststormstate)
		doanim = true;

	Char->laststormstate = curstormstate;

	if (Char->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM))
		return SPR_SUSPEND;
	else if (doanim)
		Start(Owner, 0);

	BOOL bIsInCombatState = false;
	if ( Char->GetGameObjectType() == GOT_PLAYER )
	{
		bIsInCombatState = Char->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);
		CPlayer* pPlayer = (CPlayer*)Char;
		if ( pPlayer->isInShapShift())
		{
			bIsInCombatState = false;
		}
	}

	if (!bIsInCombatState )
	{
		if (Char->IsAttackPrepare())
		{
			float fCurTime = SYState()->ClientApp->GetAccumTime();
			Char->SetWaitAnimTime(fCurTime);
			if (!Char->SetIdleAnimation(CAG_IDLE))
			{
				Char->SetIdleAnimation(CAG_WAIT);
			}

			// combate status
			Char->SetAttackPrepare(false);
		}
		else
		{
			float fCurrentTime = fTime;
			float s_fWaitTimer = 10.0f;
			if (Char->GetUInt32Value(UNIT_FIELD_PETNUMBER))
				s_fWaitTimer = 5.0f;

			if ( bNeedReset )
			{
				Char->SetCurAnimation( "stand", 1.f, 0, true, 2 );
				bNeedReset = false;
			}

			bool bAnimEnd = Char->IsCurAnimEndInTime(fCurrentTime);
			
			bool bChangeToWait = false;
			float fWaitTime = Char->GetWaitAnimTime();

			if (!Char->IsWait()) {
				if (fCurrentTime - fWaitTime >= s_fWaitTimer)
				{
					Char->SetWaitAnimTime(fCurrentTime);	// restart waitanim interval timer
					bChangeToWait = Char->GetUInt32Value(UNIT_FIELD_PETNUMBER)?true:bool(rand()&1);
				}
			}
			else
				if (bAnimEnd)
				{
					Char->SetWaitState(false);
					Char->SetWaitAnimTime(fCurrentTime);	// restart waitanim interval timer
					Char->SetIdleAnimation(CAG_IDLE);
				}

				if (!Char->IsWait() && bChangeToWait)
				{
					Char->SetWaitState(true);
					Char->SetIdleAnimation(CAG_WAIT);
				}
		}
	}
	else
		if ( Char->GetAsPlayer() != NULL && bIsInCombatState && !Char->IsAttackPrepare())
		{
			CPlayer* pkPlayer = (CPlayer*)Char;
			NIASSERT(pkPlayer);
			if (!pkPlayer)
				return SPR_END;

			pkPlayer->SetAttackPrepare();
		}

		// 检查输入事件.
		Char->ProcessNextAction();

		// 返回挂起尝试退出状态.
		return SPR_SUSPEND;
}

State StateIdle::End(IStateActor* Owner, DWORD CurTime)
{
	return STATE_IDLE;
}


//----------------------------------------------------------------------------
BOOL StateActionIdle::CanTransite(IStateActor* Owner, DWORD CurTime, State ToState)
{
	return TRUE;
}

BOOL StateActionIdle::Start(IStateActor* Owner, DWORD CurTime)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);

	//// 因为动作现在未处理分层,临时根据状态判定,只有姿态为站立时才恢复站立动作
	//if (pkChar->GetCurPostureState() == STATE_IDLE)
	//{
	//	BOOL bIsInCombatState = pkChar->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);
	//	if ( !bIsInCombatState )
	//	{
	//		float fCurTime = SYState()->ClientApp->GetAccumTime();
	//		pkChar->SetWaitAnimTime(fCurTime);
	//		if (!pkChar->SetIdleAnimation(CAG_IDLE))
	//		{
	//			pkChar->SetIdleAnimation(CAG_WAIT);
	//		}
	//	}
	//	else
	//	if ( pkChar->GetAsPlayer() != NULL )
	//	{
	//		CPlayer* pkPlayer = (CPlayer*)pkChar;
	//		NIASSERT(pkPlayer);
	//		if (!pkPlayer)
	//			return FALSE;

	//		pkPlayer->SetCombatPrepareAnimation(CAG_COMBATPREPARE );
	//		pkPlayer->SetAttackPrepare(true);
	//	}
	//}

	
	return TRUE;
}

EStateProcessResult StateActionIdle::Process(IStateActor* Owner, float fTime, float fDelta)
{
	// 由于会影响到状态的切换，所以这里暂时改会返回 HOLD
	// 更好的解决方案，重新设计状态。
	//return SPR_HOLD;
	return SPR_SUSPEND;
}

State StateActionIdle::End(IStateActor* Owner, DWORD CurTime)
{
	return STATE_ACTIONIDLE;
}


//----------------------------------------------------------------------------


//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////

BOOL StateDeath::CanTransite(IStateActor* Owner, DWORD CurTime, State ToState)
{
	// 可切换到任意状态
	return TRUE;
}

BOOL StateDeath::SetDeathAnimation(CCharacter* pActor)
{
	NIASSERT(pActor);

	int AniIndex = 0;
	if ( ( pActor->GetGameObjectType() == GOT_PLAYER ) && ( pActor->CheckInWater() == WL_UNDER_WATER ) )
	{
		AniIndex = 1;
	}
	const char* szIdleAnimName = pActor->GetBaseAnimationName(CAG_DEAD, AniIndex );
	NiControllerSequence* Seq = pActor->SetCurAnimation( szIdleAnimName, 1.0f, FALSE, TRUE );
	return Seq != NULL;
}

BOOL StateDeath::Start(IStateActor* Owner, DWORD CurTime)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);

	if (pkChar)
	{
		SetDeathAnimation(pkChar);

		pkChar->SetLayerBlock(SL_Action);
		pkChar->SetPhysics(PHYS_Falling);
		pkChar->StopMove();
	}

	return TRUE;
}

EStateProcessResult StateDeath::Process(IStateActor* Owner, float fTime, float fDelta)
{
	CCharacter* Char = (CCharacter*)Owner;
	NIASSERT(Char);

	Char->StopMove();

	if( Char->GetGameObjectType() == GOT_PLAYER )
	{
		if ( Char->CheckInWater() == WL_UNDER_WATER )
		{
			if ( Char->IsCurAnimEndInTime( fTime ) )
			{
				Char->SetCurAnimation( "swim_died", 1.0f, TRUE, TRUE );
			}
		}
	}

	return SPR_HOLD;
}

State StateDeath::End(IStateActor* Owner, DWORD CurTime)
{
	return STATE_IDLE;
}




//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
BOOL StateMoutIdle::CanTransite(IStateActor* Owner, DWORD CurTime, State ToState)
{
	// 可切换到任意状态
	return TRUE;
}

BOOL StateMoutIdle::SetMoutIdleAnimation(CPlayer* pkPlayer, int iType)
{
	NiActorManager* pkActorManager = pkPlayer->GetActorManager();
	NIASSERT(pkActorManager != NULL);
	if(pkActorManager == NULL)
		return FALSE;

	NIASSERT(pkPlayer);

	const char* szIdleAnimName = NULL;

	if(pkPlayer->IsMountPlayer())
	{
		AnimationID AnimID = pkPlayer->GetAnimationIDByName(pkPlayer->GetCurMountSitAction().c_str());

		if (pkActorManager->GetCurAnimation() != AnimID)
		{
			NiControllerSequence* Seq = pkPlayer->SetCurAnimation(AnimID, 1.0, 1,FALSE, 1);
			if (!Seq)
			{
				return FALSE;
			}
		}	
	}
	else if(pkPlayer->IsMountByPlayer())
	{
		NiControllerSequence* Seq = pkPlayer->SetCurAnimation("stand",1.0,1,FALSE, 1);
		if (!Seq)
		{
			return FALSE;
		}
	}
	else if(pkPlayer->IsMountCreature())
	{
		SYMount* pkMountObject = (SYMount*)pkPlayer->GetMountCreature();
		pkMountObject->SetIdleAnimation(CAG_IDLE);

		AnimationID AnimID = pkPlayer->GetAnimationIDByName(pkPlayer->GetCurMountSitAction().c_str());
		
		if (pkActorManager->GetCurAnimation() != AnimID)
		{
			NiControllerSequence* Seq = pkPlayer->SetCurAnimation(AnimID, 1.0, 1,FALSE,1);
			if (!Seq)
			{
				return FALSE;
			}
		}	
	}
	return TRUE;
}

BOOL StateMoutIdle::Start(IStateActor* Owner, DWORD CurTime)
{
	CPlayer* Char = (CPlayer*)Owner;
	NIASSERT(Char);

	float fCurTime = SYState()->ClientApp->GetAccumTime();
	

	if (Char->IsMountCreature())
	{
		SYMount* pkMountObject = (SYMount*)Char->GetMountCreature();
		pkMountObject->SetIdelAnimation();
	}

	Char->SetWaitAnimTime(fCurTime);
	SetMoutIdleAnimation(Char, STATE_MOUNT_IDLE);

	return TRUE;
}

EStateProcessResult StateMoutIdle::Process(IStateActor* Owner, float fTime, float fDelta)
{
	CPlayer* Player = (CPlayer*)Owner;
	NIASSERT(Player);
	float fCurrentTime = fTime;
	static float s_fWaitTimer = 5.0f;
	bool bAnimEnd = Player->IsCurAnimEndInTime(fCurrentTime);
	bool bChangeToWait = false;


	if (!Player->IsMountByPlayer())
	{
		bool doanim = false;
		BOOL curstormstate = Player->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM);
		if (curstormstate || Player->laststormstate)
		{
			Start(Owner, 0);
		}
	}
	
	float fWaitTime = Player->GetWaitAnimTime();
	if (!Player->IsWait()) {
		if (fCurrentTime - fWaitTime >= s_fWaitTimer)
		{
			Player->SetWaitAnimTime(fCurrentTime);	// restart waitanim interval timer
			bChangeToWait = bool(rand()&1);
		}
	}
	else
		if (bAnimEnd)
		{
			Player->SetWaitState(false);
			Player->SetWaitAnimTime(fCurrentTime);	// restart waitanim interval timer
			SetMoutIdleAnimation(Player, STATE_MOUNT_IDLE);
		}
		// 检查输入事件.
		Player->ProcessNextAction();

		// 返回挂起尝试退出状态.
		return SPR_SUSPEND;
}

State StateMoutIdle::End(IStateActor* Owner, DWORD CurTime)
{
	return STATE_IDLE;
}