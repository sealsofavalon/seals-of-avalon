#pragma once
#include "SYStateBase.h"

class CCharacter;
class CPlayer;

// stateidleʵ������stand״̬
class StateIdle : public IStateProxy
{
	PROXY_INSTANCE(StateIdle);
	virtual State GetState() const	{ return STATE_IDLE; }
	virtual BOOL CanTransite(IStateActor* Owner, DWORD CurTime, State ToState);
	virtual BOOL Start(IStateActor* Owner, DWORD CurTime);
	virtual EStateProcessResult Process(IStateActor* Owner, float fTime, float fDelta);
	virtual State End(IStateActor* Owner, DWORD CurTime);

	virtual StateLayer GetLayer() { return SL_Poseture; }
	virtual char* GetStateDesc() { return "StateIdle"; }

};

class StateActionIdle : public IStateProxy
{
	PROXY_INSTANCE(StateActionIdle);
	virtual State GetState() const	{ return STATE_ACTIONIDLE; }
	virtual BOOL CanTransite(IStateActor* Owner, DWORD CurTime, State ToState);
	virtual BOOL Start(IStateActor* Owner, DWORD CurTime);
	virtual EStateProcessResult Process(IStateActor* Owner, float fTime, float fDelta);
	virtual State End(IStateActor* Owner, DWORD CurTime);

	virtual StateLayer GetLayer() { return SL_Action; }
	virtual char* GetStateDesc() { return "StateActionIdle"; }
};

class StateDeath : public IStateProxy
{
	PROXY_INSTANCE(StateDeath);
	virtual State GetState() const	{ return STATE_DEATH; }
	virtual BOOL CanTransite(IStateActor* Owner, DWORD CurTime, State ToState);
	virtual BOOL Start(IStateActor* Owner, DWORD CurTime);
	virtual EStateProcessResult Process(IStateActor* Owner, float fTime, float fDelta);
	virtual State End(IStateActor* Owner, DWORD CurTime);

	BOOL SetDeathAnimation(CCharacter* pActor);

	virtual StateLayer GetLayer() { return SL_Poseture; }
	virtual char* GetStateDesc() { return "StateDeath"; }
};

class StateMoutIdle : public IStateProxy
{
	PROXY_INSTANCE(StateMoutIdle);
	virtual State GetState() const { return STATE_MOUNT_IDLE; }
	virtual BOOL CanTransite(IStateActor* Owner, DWORD CurTime, State ToState);
	virtual BOOL Start(IStateActor* Owner, DWORD CurTime);
	virtual EStateProcessResult Process(IStateActor* Owner, float fTime, float fDelta);
	virtual State End(IStateActor* Owner, DWORD CurTime);

	BOOL SetMoutIdleAnimation(CPlayer* pkPlayer, int iType);

	virtual StateLayer GetLayer() { return SL_Poseture; }
	virtual char* GetStateDesc() { return "StateMoutIdle"; }

};