#include "StdAfx.h"
#include "StateMoutMove.h"
#include "LocalPlayer.h"
#include "ClientApp.h"
#include "SYMount.h"
#include "ObjectManager.h"

BOOL StateMountMove::CanTransite(IStateActor* Owner, DWORD CurTime, State ToState)
{
	return TRUE;
}

BOOL StateMountMove::SetMoveAnimation(CPlayer* pActor, UINT Flags)
{
	NIASSERT(pActor);
	if (!pActor) return FALSE;

	NiControllerSequence* pkSeq = NULL;
	if ( pActor->GetGameObjectType() != GOT_PLAYER ) {
		int index = 0;
		if (Flags == CMS_RUN)
		{
			Flags = CAG_RUN;
			index = 1;
		}else if (Flags == CMS_WALK)
		{
			Flags = CAG_WALK;
			index = 2;
		}
		else if(Flags == CMS_MOUNTRUN)
		{
			Flags = CAG_MOUNTRUN;
		}
		
		if ( pActor->IsSYMount() )
		{
			SYMount* pkMount = (SYMount*)pActor;
			CCharacter* pkFather = pkMount->GetFather();
			if ( pkFather && ( pkFather->GetMovementFlags() & MOVEFLAG_MOVE_BACKWARD ) )
			{
				Flags = CAG_BACKWARD;
			}
		}

		const char* szIdleAnimName = pActor->GetBaseAnimationName((ECHAR_ANIM_GROUP)Flags, index);
		AnimationID animID = pActor->GetAnimationIDByName( szIdleAnimName );
		if ( animID != pActor->GetCurAnimationID() )
		{
			pkSeq = pActor->SetCurAnimation(szIdleAnimName, 1.0f, TRUE, FALSE, 1);

			if ( pkSeq && pActor->IsSYMount() )
			{
				SYMount* pkMount = (SYMount*)pActor;
				CPlayer* pkFather = (CPlayer*)pkMount->GetFather();
				float fBaseMoveSpeed = 0.0f;
				bool ret = pkFather->GetBaseMoveSpeed(fBaseMoveSpeed);
				if ( fBaseMoveSpeed < 0.0001f)
				{
					fBaseMoveSpeed = 7.f;
				}
				pkSeq->SetFrequency( pkFather->GetMoveSpeed() / ( fBaseMoveSpeed * pActor->GetSceneNode()->GetScale() ) );
			}
		}
	}
	else {
		CPlayer* pkPlayer = (CPlayer*)pActor;
		if (!pkPlayer) return FALSE;

		AnimationID animID = pkPlayer->GetMovementAnimation(Flags);
		//pkSeq = pkPlayer->SetCurAnimation(animID, 1.0, TRUE, TRUE, 1);

		std::string strAnim;
		if(pkPlayer->IsMountCreature())
		{
			strAnim = pkPlayer->GetCurMountSitAction();
		}
		else if(pkPlayer->IsMountPlayer())
		{
			//pkSeq = pkPlayer->SetCurAnimation(pkPlayer->GetCurMountSitAction().c_str(), 1.0f, TRUE, FALSE, 1);
			strAnim = pkPlayer->GetCurMountSitAction();
		}
		else if ( pkPlayer->IsMountByPlayer() )
		{
			//strAnim = "run";
			if ( ( pkPlayer->GetMovementFlags() & MOVEFLAG_MOVE_BACKWARD ) )
			{
				strAnim = "Backward01";
			}
			else
				strAnim = "run";
		}

		if ( strAnim.length() )
		{
			animID = pkPlayer->GetAnimationIDByName( strAnim.c_str() );
		}
		
		if ( animID != pkPlayer->GetCurAnimationID() )
		{
			pkSeq = pkPlayer->SetCurAnimation(animID, 1.0, TRUE, FALSE, 1);
		}
	}
	
	return pkSeq != NULL;
}

BOOL StateMountMove::SetJumpAnimation(CPlayer* pkChar, int iJumpState, bool bAnimLoop)
{
	NIASSERT(pkChar != NULL);

	NiActorManager* pkActorManager = pkChar->GetActorManager();
	NIASSERT(pkActorManager != NULL);
	if(pkActorManager == NULL)
		return FALSE;

	BOOL bForceStart = FALSE;

	//CAG_JUMP_UP, CAG_JUMP_ON, CAG_JUMP_DOWN, CAG_RUN
	ECHAR_ANIM_GROUP eJumpAnim;
	if (iJumpState == EJS_START) {
		eJumpAnim = CAG_JUMP_UP;
		bForceStart = TRUE;
	}
	else if (iJumpState == EJS_FALLING) 
	{
		eJumpAnim = CAG_JUMP_ON;
		bForceStart = TRUE;
	}
	else if (iJumpState == EJS_LANDING) 
	{
		if (pkChar->IsMoving())
			eJumpAnim = CAG_JUMP_RUN;
		else
		{
			eJumpAnim = CAG_JUMP_DOWN;
		}
		bForceStart = TRUE;
	}
	else
	{
		NIASSERT(0);
		return FALSE;
	}

	if(pkChar->IsMountPlayer())
	{
		NiFixedString strAnim = pkChar->GetBaseAnimationName(eJumpAnim, pkChar->GetMoveState());
		AnimationID animID = pkChar->GetAnimationIDByName(strAnim);
		if (pkActorManager->GetCurAnimation() != animID)
		{
			NiControllerSequence* sq = pkChar->SetCurAnimation(animID, 1, bAnimLoop);
			if (!sq)
			{
				return FALSE;
			}

		}
	}
	else if(pkChar->IsMountByPlayer())
	{
		NiFixedString strAnim = pkChar->GetBaseAnimationName(eJumpAnim, pkChar->GetMoveState());
		AnimationID animID = pkChar->GetAnimationIDByName(strAnim);
		if(pkChar->SetCurAnimation(animID, 1, bAnimLoop, bForceStart) == NULL)
			return FALSE;
		
	}
	else if(pkChar->IsMountCreature())
	{
		if(pkChar)
		{
 			NiFixedString strAnim = pkChar->GetBaseAnimationName(eJumpAnim, pkChar->GetMoveState());
 			AnimationID animID = pkChar->GetAnimationIDByName(strAnim);
			if (pkActorManager->GetCurAnimation() != animID)
			{
				NiControllerSequence* sq = pkChar->SetCurAnimation(animID, 1, bAnimLoop,bForceStart );
				if (!sq)
				{
					return FALSE ;
				}
			}
			
			strAnim = pkChar->GetMountCreature()->GetBaseAnimationName(eJumpAnim);
			animID = pkChar->GetMountCreature()->GetAnimationIDByName(strAnim);
			if(pkChar->GetMountCreature()->SetCurAnimation(animID, 1, bAnimLoop, bForceStart) == NULL)
				return FALSE;
		}
	}
	else 
	{
		NiFixedString strAnim = pkChar->GetBaseAnimationName(eJumpAnim, pkChar->GetMoveState());
		AnimationID animID = pkChar->GetAnimationIDByName(strAnim);
		if (pkActorManager->GetCurAnimation() != animID)
		{
			NiControllerSequence* sq = pkChar->SetCurAnimation(animID, 1, bAnimLoop);
			if (!sq)
			{
				return FALSE ;
			}
		}	
	}

	return TRUE;
}

BOOL StateMountMove::ProcessJumpState(IStateActor* Owner, bool bSyncPos /*= false*/, NiPoint3* pkInitVelocity /*= NULL*/)
{
	CPlayer* pkChar = (CPlayer*)Owner;

	NIASSERT(pkChar);

	bool bAnimEnd = false;

	float fCurTime = SYState()->ClientApp->GetAccumTime();
	if(pkChar->IsMountPlayer())
	{
		bAnimEnd = pkChar->GetMountFoot()->IsCurAnimEndInTime(fCurTime);
		
	}
	else if(pkChar->IsMountCreature())
	{
		bAnimEnd = pkChar->GetMountCreature()->IsCurAnimEndInTime(fCurTime);
		/*if (pkChar->GetMountCreature()->GetActorManager())
		{
			AnimationID anim = pkChar->GetMountCreature()->GetActorManager()->GetCurAnimation();
			
		}*/
	}
	else if(pkChar->IsMountByPlayer())
	{
		bAnimEnd = pkChar->IsCurSecAnimEndInTime(fCurTime);
	
	}
	else
	{
		bAnimEnd = pkChar->IsCurSecAnimEndInTime(fCurTime);
	
	}

	BOOL bRet = FALSE;



	int iJumpState = pkChar->GetJumpState();
	if ( iJumpState == EJS_NONE ) {
		bRet = SetJumpAnimation(pkChar, EJS_START);
		NIASSERT(bRet);

		if ( !pkChar->StartJump(true, bSyncPos, pkInitVelocity)) {
			pkChar->SetJumpState(EJS_NONE);
			return bRet;
		}

		pkChar->ResetJumpingFlag();
	}
	else
	{
		if ( bAnimEnd && iJumpState == EJS_START) {
			bRet = SetJumpAnimation(pkChar, EJS_FALLING, true);
			pkChar->SetJumpState(EJS_FALLING);
		}

		if ( pkChar->GetPhsyicState() != PHYS_Falling && iJumpState == EJS_FALLING) {
			bRet = SetJumpAnimation(pkChar, EJS_LANDING);
			pkChar->SetJumpState(EJS_LANDING);
		}

		if ( pkChar->GetJumpState() == EJS_LANDING)
		{
			if (pkChar->IsMoving() || bAnimEnd)
				pkChar->EndJump();

			if (pkChar->IsMoving()) {
				bRet = SetMoveAnimation(pkChar, pkChar->GetMoveState());
			}
		}
	}


	return bRet ;
}

BOOL StateMountMove::Start(IStateActor* Owner, DWORD CurTime)
{
	CPlayer* Char = (CPlayer*)Owner;
	NIASSERT(Char);
	// 设置行走,跑步动画.

	if ( !Char->IsFlying() )
	{
		Char->SetPhysics(PHYS_Walking);
	}
	

	if (Char->IsJumping())
	{
		ProcessJumpState(Char);
	}
	else
	{
		SetMoveAnimation(Char, Char->GetMoveState());
	}

	return TRUE;
}

EStateProcessResult StateMountMove::ProcessMovement(IStateActor* Owner, float fTime, float fDelta)
{
	CPlayer* pkPlayer = (CPlayer*)Owner;

	NIASSERT(pkPlayer != NULL);
	EStateProcessResult ret = SPR_SUSPEND;

	BOOL bJumping = pkPlayer->IsJumping();
	if (bJumping || pkPlayer->IsMoving())	
	{
		if (pkPlayer->IsJumping())
		{
			ProcessJumpState(Owner);
		}

	}
	if (!pkPlayer->IsMountByPlayer())
	{
		bool doanim = false;
		BOOL curstormstate = pkPlayer->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM);
		if (curstormstate || pkPlayer->laststormstate)
		{
			Start(Owner, 0);
		}
	}

	if ( pkPlayer->IsSYMount() )
	{
		SYMount* pMount = (SYMount*)Owner;
		if ( pMount->GetFather() && (!pMount->GetFather()->IsJumping() && pMount->GetFather()->IsMoving())  )
		{
			if ( pMount->GetFather()->isNeedUpdateAnim() )
			{
				SetMoveAnimation(pkPlayer, pkPlayer->GetMoveState());
			}
			
		}
	}

	if (bJumping || pkPlayer->IsMoving()) {
		ret = SPR_HOLD;
	}
	else {
		pkPlayer->SetNextState(STATE_MOUNT_IDLE, (int)fTime);
		if(pkPlayer->IsMountCreature())
			pkPlayer->GetMountCreature()->SetNextState(STATE_IDLE, (int)fTime);

		ret = SPR_SUSPEND;
	}
	return ret;
}

EStateProcessResult StateMountMove::Process(IStateActor* Owner, float fTime, float fDelta)
{
	CPlayer* pkChar = (CPlayer*)Owner;
	NIASSERT(pkChar);

	EStateProcessResult ret = SPR_SUSPEND;
	ret = ProcessMovement(Owner, fTime, fDelta);
	return ret;
}

State StateMountMove::End(IStateActor* Owner, DWORD CurTime)
{
	

	return STATE_MOUNT_IDLE;
}



//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
BOOL StateMountMoveLP::CanTransite(IStateActor* Owner, DWORD CurTime, State ToState)
{
	return TRUE;
}

BOOL StateMountMoveLP::Start(IStateActor* Owner, DWORD CurTime)
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)Owner;
	NIASSERT(pkLocalPlayer);

	if (!pkLocalPlayer)
		return FALSE;

	if ( !pkLocalPlayer->IsFlying() )
	{
		pkLocalPlayer->SetPhysics(PHYS_Walking);
	}
	
	// 设置行走,跑步动画.
	pkLocalPlayer->SetLastMovementFlags(0);

	float fCurMoveAngle, fLastMoveAngle;

	fCurMoveAngle = pkLocalPlayer->GetDesiredAngle();
	fLastMoveAngle = pkLocalPlayer->GetLastDesirdAngle();

	uint32 uiMoveFlag = pkLocalPlayer->GetMovementFlags();
	if (uiMoveFlag != pkLocalPlayer->GetLastMovementFlags() ||
		fCurMoveAngle != fLastMoveAngle) {
		pkLocalPlayer->SetSendMovementDuration(0.5f);
		pkLocalPlayer->SetLastSendMovementTime(CurTime * 0.001f);
		pkLocalPlayer->SetLastMovementFlags(uiMoveFlag);
		pkLocalPlayer->SetLastDesiredAngle(fCurMoveAngle);

		pkLocalPlayer->SendMovementPacket();

	}
	BOOL bRet ;
	if (pkLocalPlayer->IsJumping())
	{
		bRet = ProcessJumpState(pkLocalPlayer);
	}else
	{

		bRet = SetMoveAnimation(pkLocalPlayer, pkLocalPlayer->GetMoveState());
	}

	//NIASSERT(bRet);

	return TRUE;
}

EStateProcessResult StateMountMoveLP::Process(IStateActor* Owner, float fTime, float fDelta)
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)Owner;
	NIASSERT(pkLocalPlayer != NULL);

	pkLocalPlayer->PathMoveUpdate(fDelta);

	EStateProcessResult ret = SPR_SUSPEND;

	uint32 uiMovementFlags = pkLocalPlayer->GetMovementFlags();
	uint32 uiLastMovementFlags = pkLocalPlayer->GetLastMovementFlags();
	if (uiMovementFlags != uiLastMovementFlags)
	{
		pkLocalPlayer->SetSendMovementDuration(0.5f);
		pkLocalPlayer->SetLastSendMovementTime(fTime);
		pkLocalPlayer->SetLastMovementFlags(uiMovementFlags);
		pkLocalPlayer->SendMovementPacket();
		pkLocalPlayer->SetLastDesiredAngle(pkLocalPlayer->GetDesiredAngle());
	
	}
	else if( pkLocalPlayer->GetDesiredAngle() != pkLocalPlayer->GetLastDesirdAngle() )
	{
		if(fTime - pkLocalPlayer->GetLastSendMovementTime() > 0.35)
		{
			pkLocalPlayer->SetSendMovementDuration(0.5f);
			pkLocalPlayer->SetLastSendMovementTime(fTime);
			pkLocalPlayer->SetLastMovementFlags(uiMovementFlags);
			pkLocalPlayer->SendMovementPacket();
			pkLocalPlayer->SetLastDesiredAngle(pkLocalPlayer->GetDesiredAngle());
		
		}		
	}
	else
	if ( (uiMovementFlags & MOVEFLAG_MOTION_MASK) &&
		(pkLocalPlayer->GetLastSendMovementTime() + pkLocalPlayer->GetSendMovementDuration() <=	fTime) 
		&& !pkLocalPlayer->IsJumping()) {

		pkLocalPlayer->SendMovementPacket();
		pkLocalPlayer->SetLastSendMovementTime(fTime);
		pkLocalPlayer->SetLastMovementFlags(uiMovementFlags);
	
	}

	ret = ProcessMovement(Owner, fTime, fDelta);

	if ( pkLocalPlayer->ProcessNextAction() ) {
		pkLocalPlayer->SetNextState(STATE_MOUNT_IDLE, (int)fTime);
		return SPR_SUSPEND;
	}

	return ret;
}


State StateMountMoveLP::End(IStateActor* Owner, DWORD CurTime)
{
	CPlayer* pkChar = (CPlayer*)Owner;
	NIASSERT(pkChar);

	CPlayer* pkPlayer = (CPlayer*)pkChar;
	NIASSERT(pkPlayer);
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();

	if ( pkLocalPlayer == Owner && pkLocalPlayer->GetDesiredAngle() != pkLocalPlayer->GetLastDesirdAngle())
	{
		float fCurMoveAngle, fLastMoveAngle;
		float fTime = SYState()->ClientApp->GetAccumTime();
		uint32 uiMovementFlags = pkLocalPlayer->GetMovementFlags();

		fCurMoveAngle = pkLocalPlayer->GetDesiredAngle();
		fLastMoveAngle = pkLocalPlayer->GetLastDesirdAngle();
		pkLocalPlayer->SetSendMovementDuration(0.5f);
		pkLocalPlayer->SetLastSendMovementTime(fTime);
		pkLocalPlayer->SetLastMovementFlags(uiMovementFlags);
		pkLocalPlayer->SendMovementPacket();
		pkLocalPlayer->SetLastDesiredAngle(fCurMoveAngle);
	}

	return STATE_MOUNT_IDLE;
}
