#pragma once

#include "SYStateBase.h"

class CPlayer;

class StateMountMove : public IStateProxy
{
	PROXY_INSTANCE(StateMountMove);
	virtual State GetState() const { return STATE_MOUNT_MOVE; }
	virtual BOOL CanTransite(IStateActor* Owner, DWORD CurTime, State ToState);
	virtual BOOL Start(IStateActor* Owner, DWORD CurTime);
	virtual EStateProcessResult Process(IStateActor* Owner, float fTime, float fDelta);
	virtual State End(IStateActor* Owner, DWORD CurTime);

	virtual StateLayer GetLayer() { return SL_Poseture; }
	virtual char* GetStateDesc() { return "StateMountMove"; }

protected:
	EStateProcessResult ProcessMovement(IStateActor* Owner, float fTime, float fDelta);
	BOOL ProcessJumpState(IStateActor* Owner, bool bSyncPos = false, NiPoint3* pkInitVelocity = NULL);

	BOOL SetMoveAnimation(CPlayer* pActor, UINT Flags);
	BOOL SetJumpAnimation(CPlayer* pkChar, int iJumpState, bool bAnimLoop = false);
};

class StateMountMoveLP : public StateMountMove
{
	PROXY_INSTANCE(StateMountMoveLP);
	virtual State GetState() const	{ return STATE_MOUNT_MOVE; }
	virtual BOOL CanTransite(IStateActor* Owner, DWORD CurTime, State ToState);
	virtual BOOL Start(IStateActor* Owner, DWORD CurTime);
	virtual EStateProcessResult Process(IStateActor* Owner, float fTime, float fDelta);
	virtual State End(IStateActor* Owner, DWORD CurTime);

	virtual char* GetStateDesc() { return "StateMountMoveLP"; }
};	