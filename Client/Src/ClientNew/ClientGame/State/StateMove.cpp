#include "StdAfx.h"
#include "StateMove.h"
#include "LocalPlayer.h"
#include "Console.h"
#include "ClientApp.h"
#include "../Network/NetworkManager.h"

BOOL StateMove::CanTransite(IStateActor* Owner, DWORD CurTime, State ToState)
{
	return TRUE;
}

BOOL StateMove::Start(IStateActor* Owner, DWORD CurTime)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	if (!pkChar->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM))
	{
		pkChar->laststormstate = false;
		_doAnim(Owner);
	}
	else
		pkChar->laststormstate = true;

	return TRUE;
}

void StateMove::_doAnim(IStateActor* Owner)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);

	if (pkChar->IsJumping())
	{
		pkChar->ProcessJumpState(true);
	}
	else
	{
		pkChar->SetMoveAnimation(pkChar->GetMoveState());
	}
}

EStateProcessResult StateMove::Process(IStateActor* Owner, float fTime, float fDelta)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);

	EStateProcessResult eRet = SPR_SUSPEND;


	if (pkChar->GetGameObjectType() == GOT_PLAYER)
		eRet = ProcessMovement(Owner, fTime, fDelta);
	else
	{
		float fr = 1.0f;

		bool bUpdateRet = false;

		float fT = 0.0f;
		bUpdateRet = pkChar->UpdateMoveTransform(fTime, fDelta, fT);

		bool bStoped = false;
		if (!pkChar->IsMoving() && !pkChar->IsJumping())
			bStoped = true;

		if( bStoped || bUpdateRet || pkChar->ProcessNextAction()) {
			return SPR_END;
		}
	}

	if (pkChar->GetGameObjectType() == GOT_PLAYER)
	{
		if (!pkChar->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM))
		{
			float basespeed = 0.0f;
			bool ret = pkChar->GetBaseMoveSpeed(basespeed);
			NiControllerSequence* pkMoveSeq = pkChar->GetMoveSeq();
			if (pkMoveSeq != 0 && basespeed > 0.0f)
			{
				pkMoveSeq->SetFrequency(pkChar->GetMoveSpeed() *1.45f/ (basespeed*pkChar->GetSceneNode()->GetScale()));
			}
		}
	}
	

	return eRet;
}

EStateProcessResult StateMove::ProcessMovement(IStateActor* Owner, float fTime, float fDelta)
{
	CPlayer* pkPlayer = (CPlayer*)Owner;

	NIASSERT(pkPlayer != NULL);

	if ( pkPlayer->IsDead() )
	{
		pkPlayer->StopMove();
		pkPlayer->ForceStateChange( STATE_DEATH , fDelta );
		return SPR_END;
	}

	AudioSource* pkSound = pkPlayer->GetSwimmingSound();
	if ( pkSound->GetStatus() != AudioSource::PLAYING && pkPlayer->CheckInWater() == WL_UNDER_WATER && pkSound->GetPlayState() != 1 )
	{
		pkPlayer->GetSceneNode()->AttachChild( pkSound );
		pkSound->SetMinMaxDistance(5.0f, 50.0f);
		pkSound->SetGain(1.f);
		pkSound->SetLoopCount( 9999 );

		pkSound->SetSelectiveUpdate(true);
		pkSound->SetSelectiveUpdateTransforms(true);
		pkSound->SetSelectiveUpdatePropertyControllers(false);
		pkSound->SetSelectiveUpdateRigid(true);
		pkSound->Update(0.0f);
		const char* szFilename = pkSound->GetFilename();
		if ( !szFilename )
		{
			SYState()->IAudio->PlaySound(pkSound, "Sound/items/swimming.wav" );
		}
		pkSound->Play();
			
	}

	if ( pkSound && pkPlayer->CheckInWater() != WL_UNDER_WATER && ( pkSound->GetStatus() == AudioSource::PLAYING ) )
	{
		pkSound->Stop();
	}
	
	

	
	EStateProcessResult ret = SPR_SUSPEND;

	if ( pkPlayer->GetPhsyicState() == PHYS_Falling && !pkPlayer->IsJumping() &&  !pkPlayer->laststormstate)
	{
		pkPlayer->SetJumpState( EJS_FALLING );
		pkPlayer->SetJumpAnimation( EJS_FALLING, true );
	}

	bool doanim = false;
	BOOL curstormstate = pkPlayer->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM);

	if ( pkPlayer->IsNeedUpdateSwimAnim() && !pkPlayer->laststormstate)
	{
		if ( pkPlayer->CheckInWater() == WL_UNDER_WATER )
		{
			_doAnim( Owner );
		}
		

		pkPlayer->SetNeedUpdateSwimAnim(false);

	}

	if ( ( !curstormstate && pkPlayer->laststormstate ) || pkPlayer->isNeedUpdateAnim() )
		doanim = true;

	pkPlayer->laststormstate = curstormstate;

	if (curstormstate)
	{
		if (pkPlayer->IsJumping())
			pkPlayer->SetJumpState(EJS_NONE);
		return SPR_SUSPEND;
	}
	else if (doanim)
	{
		_doAnim(Owner);
		pkPlayer->SetisNeedAnim( false );
	}

	if (pkPlayer->IsJumping() || pkPlayer->IsMoving()) {
		if (pkPlayer->IsJumping())	{
				pkPlayer->ProcessJumpState();
		}

		if (!pkPlayer->IsJumping() && !pkPlayer->IsMoving())
		{
			ret = SPR_END;
		}
		else
			ret = SPR_HOLD;
	}
	else {
		ret = SPR_END;
	}

	return ret;
}

State StateMove::End(IStateActor* Owner, DWORD CurTime)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);
	return STATE_IDLE;
}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
BOOL StateConstraint::CanTransite(IStateActor* Owner, DWORD CurTime, State ToState)
{
	return TRUE;
}

BOOL StateConstraint::Start(IStateActor* Owner, DWORD CurTime)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);
	pkChar->SetLayerBlock(SL_Action, false);

	CPlayer* pkPlayer = pkChar->GetAsPlayer();
	if (pkPlayer)
	{
		SetConstrainAnimation(pkPlayer,pkPlayer->GetMoveState());
		
	}
	return TRUE;
}

EStateProcessResult StateConstraint::Process(IStateActor* Owner, float fTime, float fDelta)
{
	return SPR_HOLD;
}

State StateConstraint::End(IStateActor* Owner, DWORD CurTime)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);
	pkChar->SetLayerBlock(SL_Action, false);
	return STATE_UNKNOWN;
}
BOOL StateConstraint::SetConstrainAnimation(IStateActor* pActor, UINT Flags)
{
	NiControllerSequence* pkSeq = NULL;

	CPlayer* pkPlayer = (CPlayer*)pActor;
	if (!pkPlayer) return FALSE;

//	AnimationID animID = pkPlayer->GetMovementAnimation(Flags);
	//pkSeq = pkPlayer->SetCurAnimation(animID, 1.0, TRUE, TRUE, 1);

	float Frequence = pkPlayer->GetMoveSpeed() / pkPlayer->m_runSpeed ;
	if(pkPlayer->IsMountCreature())
	{
		pkPlayer->GetMountCreature()->SetCurAnimation("run", Frequence, TRUE, TRUE);
	}
	else if(pkPlayer->IsMountPlayer())
	{
		pkSeq = pkPlayer->SetCurAnimation(pkPlayer->GetCurMountSitAction().c_str(), 1.0f, TRUE, TRUE, 1);
	}
	else if(pkPlayer->IsMountByPlayer())
	{
		pkSeq = pkPlayer->SetCurAnimation("run", Frequence, TRUE, TRUE, 1);
	}else
	{
		//return pkPlayer->SetMoveAnimation(pkPlayer->GetMoveState());
	}

	return pkSeq != NULL ;
}


//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
BOOL StateMoveLP::CanTransite(IStateActor* Owner, DWORD CurTime, State ToState)
{
	return TRUE;
}

BOOL CheckFlag( uint32 flag, uint32 mask ) {
	return BOOL(flag & mask);
}

void StateMoveLP::_doAnim(IStateActor* Owner)
{
	CCharacter* pkChar = (CCharacter*)Owner;
	NIASSERT(pkChar);

	if (pkChar->IsJumping())
	{
		pkChar->ProcessJumpState();
	}
	else
	{
		pkChar->SetMoveAnimation(pkChar->GetMoveState() );
	}
}

BOOL StateMoveLP::Start(IStateActor* Owner, DWORD CurTime)
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)Owner;
	NIASSERT(pkLocalPlayer);

//	m_needsend = false;

	// 设置行走,跑步动画.
	pkLocalPlayer->SetLastMovementFlags(0);

	float fCurMoveAngle, fLastMoveAngle;

	fCurMoveAngle = pkLocalPlayer->GetDesiredAngle();
	fLastMoveAngle = pkLocalPlayer->GetLastDesirdAngle();
	uint32 uiMoveFlag = pkLocalPlayer->GetMovementFlags();
	if (uiMoveFlag != pkLocalPlayer->GetLastMovementFlags() ||
		fCurMoveAngle != fLastMoveAngle) {
		pkLocalPlayer->SetSendMovementDuration(0.5f);
		pkLocalPlayer->SetLastSendMovementTime(CurTime * 0.001f);
		pkLocalPlayer->SetLastMovementFlags(uiMoveFlag);
		pkLocalPlayer->SetLastDesiredAngle(fCurMoveAngle);

		pkLocalPlayer->SendMovementPacket();

	
	}
 
	if (!pkLocalPlayer->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM))
	{
		pkLocalPlayer->laststormstate = false;
		_doAnim(Owner);
	}
	else
		pkLocalPlayer->laststormstate = true;
	//if (!pkLocalPlayer->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM))
	//{
	//	BOOL bRet = TRUE;
	//	if (pkLocalPlayer->IsJumping())
	//	{
	//		pkLocalPlayer->ProcessJumpState();
	//	}
	//	else
	//	{
	//		bRet = pkLocalPlayer->SetMoveAnimation(pkLocalPlayer->GetMoveState());
	//	}
	//	NIASSERT(bRet);
	//}

	return TRUE;
}

EStateProcessResult StateMoveLP::Process(IStateActor* Owner, float fTime, float fDelta)
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)Owner;
// 
// 	if( m_needsend && fTime - pkLocalPlayer->GetLastSendMovementTime() > 0.3 )
// 	{
// 		pkLocalPlayer->SetSendMovementDuration(0.5f);
// 		pkLocalPlayer->SetLastSendMovementTime(fTime);
// 		pkLocalPlayer->SetLastMovementFlags(m_uiMovementFlags);
// 		pkLocalPlayer->SendMovementPacket();
// 		pkLocalPlayer->SetLastDesiredAngle(m_fCurMoveAngle);
// 		m_needsend = false;
// 	}

	pkLocalPlayer->PathMoveUpdate(fDelta);

	float fCurMoveAngle, fLastMoveAngle;

	fCurMoveAngle = pkLocalPlayer->GetDesiredAngle();
	fLastMoveAngle = pkLocalPlayer->GetLastDesirdAngle();
	
	uint32 uiMovementFlags = pkLocalPlayer->GetMovementFlags();
	uint32 uiLastMovementFlags = pkLocalPlayer->GetLastMovementFlags();
	if (uiMovementFlags != uiLastMovementFlags && uiMovementFlags )
	{
		pkLocalPlayer->SetSendMovementDuration(0.5f);
		pkLocalPlayer->SetLastSendMovementTime(fTime);
		pkLocalPlayer->SetLastMovementFlags(uiMovementFlags);
		pkLocalPlayer->SendMovementPacket();
		pkLocalPlayer->SetLastDesiredAngle(fCurMoveAngle);

		
	}
	else if( pkLocalPlayer->GetDesiredAngle() != pkLocalPlayer->GetLastDesirdAngle() )
	{
		if( fTime - pkLocalPlayer->GetLastFacingTime() > 0.2f )
		{
			pkLocalPlayer->SetSendMovementDuration(0.5f);
			pkLocalPlayer->SetLastFacingTime(fTime);
			pkLocalPlayer->SetLastMovementFlags(uiMovementFlags);
			pkLocalPlayer->SendMovementPacket();
			pkLocalPlayer->SetLastDesiredAngle(fCurMoveAngle);
			
//			m_needsend = false;
		}
// 		else
// 		{
// 			m_fCurMoveAngle = fCurMoveAngle;
// 			m_uiMovementFlags = uiMovementFlags;
// 			m_needsend = true;
// 		}
	}
	else
	if ( (uiMovementFlags & MOVEFLAG_MOTION_MASK) &&
		(pkLocalPlayer->GetLastSendMovementTime() + pkLocalPlayer->GetSendMovementDuration() <=	fTime) 
		&& !pkLocalPlayer->IsJumping()) {

		pkLocalPlayer->SendMovementPacket();
		pkLocalPlayer->SetLastSendMovementTime(fTime);
	}

	EStateProcessResult ret = StateMove::ProcessMovement(Owner, fTime, fDelta);

	if (!pkLocalPlayer->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM))
	{
		float basespeed = 0.0f;
		NiControllerSequence* pkMoveSeq = pkLocalPlayer->GetMoveSeq();
		if (pkMoveSeq != 0 && pkLocalPlayer->GetBaseMoveSpeed(basespeed))
		{
			pkMoveSeq->SetFrequency(pkLocalPlayer->GetMoveSpeed()*1.45f / (basespeed * pkLocalPlayer->GetSceneNode()->GetScale()));
		}

		if ( pkLocalPlayer->ProcessNextAction() ) {
			ret = SPR_END;
		}
	}
	return ret;
}


State StateMoveLP::End(IStateActor* Owner, DWORD CurTime)
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)Owner;
	NIASSERT(pkLocalPlayer);
	pkLocalPlayer->SetJumpState(EJS_NONE);
	pkLocalPlayer->SetLastMovementFlags(0);
	pkLocalPlayer->SendMovementPacket();

	return STATE_IDLE;
}

BOOL StateFalling::CanTransite(IStateActor* pkOwner, DWORD dwCurTime, State eToState)
{
	return TRUE;
}

BOOL StateFalling::Start(IStateActor* pkOwner, DWORD dwCurTime)
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)pkOwner;
	if (pkLocalPlayer->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM))
	{
		pkLocalPlayer->laststormstate = true ;
	}
	if ( pkLocalPlayer && !pkLocalPlayer->laststormstate)
	{
		pkLocalPlayer->SetJumpState( EJS_FALLING );
		pkLocalPlayer->SetJumpAnimation( EJS_FALLING, true );
	}
	return TRUE;
}

EStateProcessResult StateFalling::Process(IStateActor* pkOwner, float fTime, float fDelta)
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)pkOwner;
	if ( pkLocalPlayer )
	{
		pkLocalPlayer->ProcessJumpState( true );
	}

	if ( pkLocalPlayer->IsJumping() )
	{
		return SPR_HOLD;
	}
	else
		return SPR_END;
}

State StateFalling::End(IStateActor* pkOwner, DWORD dwCurTime)
{
	CPlayerLocal* pkLocalPlayer = (CPlayerLocal*)pkOwner;
	NIASSERT(pkLocalPlayer);
	pkLocalPlayer->SetJumpState(EJS_NONE);
	pkLocalPlayer->SetLastMovementFlags(0);
	return STATE_IDLE;
}
