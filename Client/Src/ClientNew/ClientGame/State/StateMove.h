#pragma once
#include "SYStateBase.h"
class CCharacter;

// 非本地玩家的移动状态.
class StateMove : public IStateProxy
{
	PROXY_INSTANCE(StateMove)

protected:
	virtual State GetState() const	{ return STATE_MOVE; }
	virtual BOOL CanTransite(IStateActor* Owner, DWORD CurTime, State ToState);
	virtual BOOL Start(IStateActor* Owner, DWORD CurTime);
	virtual EStateProcessResult Process(IStateActor* Owner, float fTime, float fDelta);
	virtual EStateProcessResult ProcessMovement(IStateActor* Owner, float fTime, float fDelta);
	virtual State End(IStateActor* Owner, DWORD CurTime);

	virtual StateLayer GetLayer() { return SL_Poseture; }
	virtual char* GetStateDesc() { return "StateMove"; }

protected:
	virtual void _doAnim(IStateActor* Owner);

};

class StateFalling : public IStateProxy
{
	PROXY_INSTANCE(StateFalling)
	virtual State GetState() const { return STATE_FALLING; }
	virtual BOOL CanTransite(IStateActor* pkOwner, DWORD dwCurTime, State eToState);
	virtual BOOL Start(IStateActor* pkOwner, DWORD dwCurTime);
	virtual EStateProcessResult Process(IStateActor* pkOwner, float fTime, float fDelta);
	virtual State End(IStateActor* pkOwner, DWORD dwCurTime);

	virtual StateLayer GetLayer() { return SL_Poseture; }
	virtual char* GetStateDesc() { return "StateFalling"; }
};




// Local player - LP
class StateMoveLP : public StateMove
{
	PROXY_INSTANCE(StateMoveLP)
	virtual State GetState() const	{ return STATE_MOVE; }
	virtual BOOL CanTransite(IStateActor* Owner, DWORD CurTime, State ToState);
	virtual BOOL Start(IStateActor* Owner, DWORD CurTime);
	virtual EStateProcessResult Process(IStateActor* Owner, float fTime, float fDelta);
	virtual State End(IStateActor* Owner, DWORD CurTime);

protected:
	void _doAnim(IStateActor* Owner);

	virtual char* GetStateDesc() { return "StateMoveLP"; }

// 	uint32 m_uiMovementFlags;
// 	float m_fCurMoveAngle;
// 	bool m_needsend;
};


class StateConstraint : public StateMove
{
	PROXY_INSTANCE(StateConstraint)
	virtual State GetState() const { return STATE_CONSTRAINT; }
	virtual BOOL CanTransite(IStateActor* Owner, DWORD CurTime, State ToState);
	virtual BOOL Start(IStateActor* Owner, DWORD CurTime);
	virtual EStateProcessResult Process(IStateActor* Owner, float fTime, float fDelta);
	virtual State End(IStateActor* Owner, DWORD CurTime);

	virtual StateLayer GetLayer() { return SL_Poseture; }
	virtual char* GetStateDesc() { return "StateConstraint"; }

protected:
	BOOL SetConstrainAnimation(IStateActor* pActor, UINT Flags);
};



