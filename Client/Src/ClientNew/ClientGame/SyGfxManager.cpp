// ***************************************************************
//  Copyright (C) Sunyou 2007-2008 - All Rights Reserved
//	
//  SyGfxManager.cpp  Created : P.K. 2008-08-27
//  
// ***************************************************************
#include "StdAfx.h"
#include "SyGfxManager.h"
#if SUPPORT_GFX
#include <GRendererD3D9.h>
#include <GfxLog.h>
namespace gfx
{
	class FileOpener : public GFxFileOpener
	{
	private:
		 virtual GFile*      OpenFile(const char *purl)
		 {
			 NIASSERT(purl);
			 UTRACE("gfx OpenFile(%s)", purl);
			 return new GSysFile(purl);
		 }
	};

	class Render : public NiMemObject
	{
		friend class SyGfxManager;
	public:
		Render(NiRenderer* pRender)
		{
			m_pNiRender = pRender;
			m_pD3DDev = NULL;
			m_GRender = NULL;
		}

		BOOL Initialize()
		{
			if (m_pNiRender == NULL)
			{
				return FALSE;
			}
			m_GRender = GRendererD3D9::CreateRenderer();
			if (m_GRender == NULL)
			{
				return FALSE;
			}
			
			NiDX9Renderer *pDX9Renderer = NiDynamicCast(NiDX9Renderer, m_pNiRender);
			NIASSERT(pDX9Renderer);

			unsigned int uiWidth;
			unsigned int uiHeight;
			unsigned int uiUseFlags;
			NiWindowRef kWndFocus;
			unsigned int uiAdapter;
			NiDX9Renderer::DeviceDesc eDesc;
			NiDX9Renderer::FrameBufferFormat eFBFormat;
			NiDX9Renderer::DepthStencilFormat eDSFormat; 
			NiDX9Renderer::PresentationInterval ePresentationInterval;
			NiDX9Renderer::SwapEffect eSwapEffect;
			unsigned int uiFBMode;
			unsigned int uiBackBufferCount;
			unsigned int uiRefreshRate;

			pDX9Renderer->GetCreationParameters(uiWidth, uiHeight, uiUseFlags,
				m_hWnd, kWndFocus, uiAdapter, eDesc, eFBFormat, eDSFormat,
				ePresentationInterval, eSwapEffect, uiFBMode, uiBackBufferCount,
				uiRefreshRate);
			
			// Obtain display settings     
			LPDIRECT3DDEVICE9 pkDevice = pDX9Renderer->GetD3DDevice();
			D3DPRESENT_PARAMETERS   kD3DParams;
			memcpy(&kD3DParams, pDX9Renderer->GetPresentParams(), sizeof(D3DPRESENT_PARAMETERS));

			if (FAILED(pkDevice->CreateStateBlock(D3DSBT_ALL, &m_pStateBlock)))
			{
				return FALSE;
			}

			BOOL bSuccess = m_GRender->SetDependentVideoMode(pkDevice,
				&kD3DParams, GRendererD3D9::VMConfig_NoSceneCalls, m_hWnd);
			return bSuccess;
		}
		
		void Shutdown()
		{
			if (m_pStateBlock)
			{
				m_pStateBlock->Release();
				m_pStateBlock = NULL;
			}
			if (m_GRender)
			{
				m_GRender->ResetVideoMode();
				m_GRender->Release();
				m_GRender = NULL;
			}
		}
		inline NiDX9Renderer* GetNiRender() { return (NiDX9Renderer*)m_pNiRender;}
		IDirect3DDevice9* m_pD3DDev;
		IDirect3DStateBlock9* m_pStateBlock;
		NiRenderer* m_pNiRender;
		GRendererD3D9* m_GRender;
		HWND m_hWnd;
	};

	class FSCommandHandler : public GFxFSCommandHandler
	{
	public:
		virtual void Callback(GFxMovieView* pmovie, const char* pcommand, const char* parg)
		{
			NIASSERT(pmovie);
			FlashMovie* pFlash = (FlashMovie*)pmovie->GetUserData();
			NIASSERT(pFlash);
			if (pFlash->m_pEventSink)
			{
				pFlash->m_pEventSink->OnCommand(pcommand, parg);
			}
		}
	};


	class UserEventHandler : public GFxUserEventHandler
	{
	public:

		virtual void HandleEvent(GFxMovieView* pmovie, const GFxEvent& event)
		{
			GUNUSED(pmovie);
			switch(event.Type)
			{
			case GFxEvent::DoShowMouse:
				//pApp->ShowCursor(true);
				break;
			case GFxEvent::DoHideMouse:
				//pApp->ShowCursor(false);
				break;
			case GFxEvent::DoSetMouseCursor:
				{
					/*const GFxMouseCursorEvent& mcEvent = static_cast<const GFxMouseCursorEvent&>(event);
					switch(mcEvent.CursorShape)
					{
					case GFxMouseCursorEvent::ARROW:
						pApp->SetCursor(::LoadCursor(NULL, IDC_ARROW));
						break;
					case GFxMouseCursorEvent::HAND:
						pApp->SetCursor(::LoadCursor(NULL, IDC_HAND));
						break;
					case GFxMouseCursorEvent::IBEAM:
						pApp->SetCursor(::LoadCursor(NULL, IDC_IBEAM));
						break;
					}*/
				}
				break;
			}
		}
	};


	class Log : public GFxLog
	{
	public:
		// We override this function in order to do custom logging.
		virtual void    LogMessageVarg(LogMessageType messageType, const char* pfmt, va_list argList)
		{
			char    formatBuff[2048];
			formatBuff[0] = '<';
			formatBuff[1] = 'G';
			formatBuff[2] = 'F';
			formatBuff[3] = 'X';
			formatBuff[4] = '>';

			NiVsprintf(&formatBuff[5], 2043, pfmt, argList);
			::OutputDebugStringA(formatBuff);
		}
	};

	class ImageLoader : public GFxImageLoader
	{
		virtual GImageInfoBase*     LoadImage(const char *purl)
		{
			UTRACE("LoadImage %s", purl);
			return NULL;
		}
	};
}
using namespace gfx;

SyGfxManager* SyGfxManager::sm_Gfx = NULL;


FlashMovie::FlashMovie()
{
	m_bEnabled = 1;
	m_bPaused = 0;
	m_LastAdvanceTime = 0.0f;
	m_NextAdvanceTime = 0.0f;
	m_bDirty = 1;
	m_fSpeedScale = 1.0f;
	m_fFrameFrequency = 1.0f;
	m_uiFrameCatchUp = 2;
	m_spMovieDef = NULL;
	m_spMovie = NULL;
	m_pEventSink = NULL;
}
FlashMovie::~FlashMovie()
{

}

BOOL FlashMovie::LoadMovie(const char* pszSWFFile)
{
	NIASSERT(SyGfxManager::sm_Gfx && pszSWFFile);
	GFxLoader& Loader = SyGfxManager::sm_Gfx->m_GfxLoader;
	if (!Loader.GetMovieInfo(pszSWFFile, &m_MovieInfo))
	{
		UTRACE("gfx getmovieiffo(%s) failed.", pszSWFFile);
		return FALSE;
	}
	m_spMovieDef = *Loader.CreateMovie(pszSWFFile);
	if (!m_spMovieDef)
	{
		UTRACE("gfx CreateMovie(%s) failed.", pszSWFFile);
		return FALSE;
	}
	
	//create instance.
	m_spMovie = *m_spMovieDef->CreateInstance();
	if (m_spMovie)
	{
		m_spMovie->SetUserData(this);
		GPtr<FSCommandHandler> spCmdHandler = *new FSCommandHandler;
		m_spMovie->SetFSCommandHandler(spCmdHandler);
		GPtr<UserEventHandler> spUserEventHandler = *new UserEventHandler;
		m_spMovie->SetUserEventHandler(spUserEventHandler);
		return TRUE;
	}
	m_spMovieDef = NULL;
	m_spMovie = NULL;
	return FALSE;
}

void FlashMovie::Advance(float fDeltaTime)
{
	if (!IsEnable())
	{
		return;
	}
	fDeltaTime = NiMin(fDeltaTime, 1.0f);
	m_LastAdvanceTime += fDeltaTime;
	if (m_LastAdvanceTime > m_NextAdvanceTime)	// while??
	{
		float fFrameStride = m_fFrameFrequency / m_MovieInfo.FPS;
		m_NextAdvanceTime += fFrameStride;
		if (!m_bPaused && m_spMovie->GetVisible())
		{
			// Advance the movie
			/*m_NextAdvanceTime =*/ m_spMovie->Advance(fDeltaTime * m_fSpeedScale, m_uiFrameCatchUp);
			m_bDirty = 1;
		}
	}
}

void FlashMovie::Render()
{
	if (!IsEnable())
	{
		return;
	}
	
	if (m_spMovie)
	{
		SyGfxManager::BeginRender();
		m_spMovie->Display();
		SyGfxManager::EndRender();
	}

	m_bDirty = 0;
}

void FlashMovie::SetViewport(int left, int top, int right, int bottom)
{
	if (m_spMovie && SyGfxManager::sm_Gfx->m_pRender)
	{
		
		NiRenderer* pNiRender = SyGfxManager::sm_Gfx->m_pRender->GetNiRender();

		UINT nBufWidth;
		UINT nBufHeight;
		pNiRender->ConvertFromNDCToPixels(1.0f, 1.0f, nBufWidth, nBufHeight);
		m_spMovie->SetViewport(nBufWidth, nBufHeight, left, top, right - left, bottom - top, 0);
	}
}

void FlashMovie::SetBackgroundAlpha(float fAlpha)
{
	if (m_spMovie)
	{
		m_spMovie->SetBackgroundAlpha(fAlpha);
	}
}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////

BYTE SyGfxManager::smLKey2GfxKey[0xFF];
void SyGfxManager::InitKeycodeRemapTable()
{
	memset(smLKey2GfxKey, 0, sizeof(smLKey2GfxKey));
#define MAP_KEYCODE(lkey, gfxkey)   smLKey2GfxKey[lkey] = gfxkey;
	MAP_KEYCODE(LK_NULL, GFxKey::VoidSymbol);
	MAP_KEYCODE(LK_ESCAPE, GFxKey::Escape);

	MAP_KEYCODE(LK_1, GFxKey::Num1);
	MAP_KEYCODE(LK_2, GFxKey::Num2);
	MAP_KEYCODE(LK_3, GFxKey::Num3);
	MAP_KEYCODE(LK_4, GFxKey::Num4);
	MAP_KEYCODE(LK_5, GFxKey::Num5);
	MAP_KEYCODE(LK_6, GFxKey::Num6);
	MAP_KEYCODE(LK_7, GFxKey::Num7);
	MAP_KEYCODE(LK_8, GFxKey::Num8);
	MAP_KEYCODE(LK_9, GFxKey::Num9);
	MAP_KEYCODE(LK_0, GFxKey::Num0);

	MAP_KEYCODE(LK_MINUS, GFxKey::Minus);
	MAP_KEYCODE(LK_EQUALS, GFxKey::Equal);
	MAP_KEYCODE(LK_BACK, GFxKey::Backspace);
	MAP_KEYCODE(LK_TAB, GFxKey::Tab);

	MAP_KEYCODE(LK_Q, GFxKey::Q);
	MAP_KEYCODE(LK_W, GFxKey::W);
	MAP_KEYCODE(LK_E, GFxKey::E);
	MAP_KEYCODE(LK_R, GFxKey::R);
	MAP_KEYCODE(LK_T, GFxKey::T);
	MAP_KEYCODE(LK_Y, GFxKey::Y);

	MAP_KEYCODE(LK_U, GFxKey::U);
	MAP_KEYCODE(LK_I, GFxKey::I);
	MAP_KEYCODE(LK_O, GFxKey::O);
	MAP_KEYCODE(LK_P, GFxKey::P);
	MAP_KEYCODE(LK_LBRACKET, GFxKey::BracketLeft);
	MAP_KEYCODE(LK_RBRACKET, GFxKey::BracketRight);

	MAP_KEYCODE(LK_RETURN, GFxKey::Return);
	MAP_KEYCODE(LK_LCONTROL, GFxKey::Control);
	MAP_KEYCODE(LK_A, GFxKey::A);
	MAP_KEYCODE(LK_S, GFxKey::S);
	MAP_KEYCODE(LK_D, GFxKey::D);
	MAP_KEYCODE(LK_F, GFxKey::F);

	MAP_KEYCODE(LK_G, GFxKey::G);
	MAP_KEYCODE(LK_H, GFxKey::H);
	MAP_KEYCODE(LK_J, GFxKey::J);
	MAP_KEYCODE(LK_K, GFxKey::K);
	MAP_KEYCODE(LK_L, GFxKey::L);
	MAP_KEYCODE(LK_SEMICOLON, GFxKey::Semicolon);

	//MAP_KEYCODE(LK_APOSTROPHE, GFxKey::?);
	//MAP_KEYCODE(LK_GRAVE, GFxKey::?);
	MAP_KEYCODE(LK_LSHIFT, GFxKey::Shift);
	MAP_KEYCODE(LK_BACKSLASH, GFxKey::Backslash);
	MAP_KEYCODE(LK_Z, GFxKey::Z);
	MAP_KEYCODE(LK_X, GFxKey::X);

	MAP_KEYCODE(LK_C, GFxKey::C);
	MAP_KEYCODE(LK_V, GFxKey::V);
	MAP_KEYCODE(LK_B, GFxKey::B);
	MAP_KEYCODE(LK_N, GFxKey::N);
	MAP_KEYCODE(LK_M, GFxKey::M);
	MAP_KEYCODE(LK_COMMA, GFxKey::Comma);

	MAP_KEYCODE(LK_PERIOD, GFxKey::Period);
	MAP_KEYCODE(LK_SLASH, GFxKey::Slash);
	MAP_KEYCODE(LK_RSHIFT, GFxKey::Shift);
	MAP_KEYCODE(LK_MULTIPLY, GFxKey::KP_Multiply);
	MAP_KEYCODE(LK_LMENU, GFxKey::Alt);
	MAP_KEYCODE(LK_SPACE, GFxKey::Space);
	MAP_KEYCODE(LK_CAPITAL, GFxKey::CapsLock);

	MAP_KEYCODE(LK_F1, GFxKey::F1);
	MAP_KEYCODE(LK_F2, GFxKey::F2);
	MAP_KEYCODE(LK_F3, GFxKey::F3);
	MAP_KEYCODE(LK_F4, GFxKey::F4);
	MAP_KEYCODE(LK_F5, GFxKey::F5);
	MAP_KEYCODE(LK_F6, GFxKey::F6);
	MAP_KEYCODE(LK_F7, GFxKey::F7);
	MAP_KEYCODE(LK_F8, GFxKey::F8);
	MAP_KEYCODE(LK_F9, GFxKey::F9);
	MAP_KEYCODE(LK_F10, GFxKey::F10);

	MAP_KEYCODE(LK_NUMLOCK, GFxKey::NumLock);

	MAP_KEYCODE(LK_SCROLL, GFxKey::ScrollLock);
	MAP_KEYCODE(LK_NUMPAD7, GFxKey::KP_7);
	MAP_KEYCODE(LK_NUMPAD8, GFxKey::KP_8);
	MAP_KEYCODE(LK_NUMPAD9, GFxKey::KP_9);
	MAP_KEYCODE(LK_SUBTRACT, GFxKey::KP_Subtract);
	MAP_KEYCODE(LK_NUMPAD4, GFxKey::KP_4);

	MAP_KEYCODE(LK_NUMPAD5, GFxKey::KP_5);
	MAP_KEYCODE(LK_NUMPAD6, GFxKey::KP_6);
	MAP_KEYCODE(LK_ADD, GFxKey::KP_Add);
	MAP_KEYCODE(LK_NUMPAD1, GFxKey::KP_1);
	MAP_KEYCODE(LK_NUMPAD2, GFxKey::KP_2);
	MAP_KEYCODE(LK_NUMPAD3, GFxKey::KP_3);

	MAP_KEYCODE(LK_NUMPAD0, GFxKey::KP_0);
	MAP_KEYCODE(LK_DECIMAL, GFxKey::KP_Decimal);
	//MAP_KEYCODE(LK_OEM_102, GFxKey::?);
	MAP_KEYCODE(LK_F11, GFxKey::F11);
	MAP_KEYCODE(LK_F12, GFxKey::F12);
	MAP_KEYCODE(LK_F13, GFxKey::F13);

	MAP_KEYCODE(LK_F14, GFxKey::F14);
	MAP_KEYCODE(LK_F15, GFxKey::F15);
	//MAP_KEYCODE(LK_KANA, GFxKey::?);
	//MAP_KEYCODE(LK_ABNT_C1, GFxKey::?);
	//MAP_KEYCODE(LK_CONVERT, GFxKey::?);
	//MAP_KEYCODE(LK_NOCONVERT, GFxKey::?);

	//MAP_KEYCODE(LK_YEN, GFxKey::?);
	//MAP_KEYCODE(LK_ABNT_C2, GFxKey::?);
	//MAP_KEYCODE(LK_NUMPADEQUALS, GFxKey::?);
	//MAP_KEYCODE(LK_PREVTRACK, GFxKey::?);
	//MAP_KEYCODE(LK_AT, GFxKey::?);
	//MAP_KEYCODE(LK_COLON, GFxKey::?);

	//MAP_KEYCODE(LK_UNDERLINE, GFxKey::?);
	//MAP_KEYCODE(LK_KANJI, GFxKey::?);
	//MAP_KEYCODE(LK_STOP, GFxKey::?);
	//MAP_KEYCODE(LK_AX, GFxKey::?);
	//MAP_KEYCODE(LK_UNLABELED, GFxKey::?);
	//MAP_KEYCODE(LK_NEXTTRACK, GFxKey::?);

	MAP_KEYCODE(LK_NUMPADENTER, GFxKey::KP_Enter);
	MAP_KEYCODE(LK_RCONTROL, GFxKey::Control);
	//MAP_KEYCODE(LK_MUTE, GFxKey::?);
	//MAP_KEYCODE(LK_CALCULATOR, GFxKey::?);
	//MAP_KEYCODE(LK_PLAYPAUSE, GFxKey::?);
	//MAP_KEYCODE(LK_MEDIASTOP, GFxKey::?);

	//MAP_KEYCODE(LK_VOLUMEDOWN, GFxKey::?);
	//MAP_KEYCODE(LK_VOLUMEUP, GFxKey::?);
	//MAP_KEYCODE(LK_WEBHOME, GFxKey::?);
	//MAP_KEYCODE(LK_NUMPADCOMMA, GFxKey::?);
	MAP_KEYCODE(LK_DIVIDE, GFxKey::KP_Divide);
	//MAP_KEYCODE(LK_SYSRQ, GFxKey::?);

	MAP_KEYCODE(LK_RMENU, GFxKey::Alt);
	//MAP_KEYCODE(LK_PAUSE, GFxKey::?);
	MAP_KEYCODE(LK_HOME, GFxKey::Home);
	MAP_KEYCODE(LK_UP, GFxKey::Up);
	MAP_KEYCODE(LK_PRIOR, GFxKey::PageUp);
	MAP_KEYCODE(LK_LEFT, GFxKey::Left);

	MAP_KEYCODE(LK_RIGHT, GFxKey::Right);
	//MAP_KEYCODE(LK_END, GFxKey::End);
	MAP_KEYCODE(LK_DOWN, GFxKey::Down);
	MAP_KEYCODE(LK_NEXT, GFxKey::PageDown);
	MAP_KEYCODE(LK_INSERT, GFxKey::Insert);
	MAP_KEYCODE(LK_DELETE, GFxKey::Delete);

	//MAP_KEYCODE(LK_LWIN, GFxKey::?);
	//MAP_KEYCODE(LK_RWIN, GFxKey::?);
	//MAP_KEYCODE(LK_APPS, GFxKey::?);
	//MAP_KEYCODE(LK_POWER, GFxKey::?);
	//MAP_KEYCODE(LK_SLEEP, GFxKey::?);
	//MAP_KEYCODE(LK_WAKE, GFxKey::?);

	//MAP_KEYCODE(LK_WEBSEARCH, GFxKey::?);
	//MAP_KEYCODE(LK_WEBFAVORITES, GFxKey::?);
	//MAP_KEYCODE(LK_WEBREFRESH, GFxKey::?);
	//MAP_KEYCODE(LK_WEBSTOP, GFxKey::?);
	//MAP_KEYCODE(LK_WEBFORWARD, GFxKey::?);
	//MAP_KEYCODE(LK_WEBBACK, GFxKey::?);

	//MAP_KEYCODE(LK_MYCOMPUTER, GFxKey::?);
	//MAP_KEYCODE(LK_MAIL, GFxKey::?);
	//MAP_KEYCODE(LK_MEDIASELECT, GFxKey::?);	
#undef MAP_KEYCODE

#define MAP_KEYCODE(lkey, gfxkey)   smGfxKey2LKey[gfxkey] = lkey;
#undef MAP_KEYCODE
}

SyGfxManager::SyGfxManager(void)
{
	
}

SyGfxManager::~SyGfxManager(void)
{
}

BOOL SyGfxManager::Create(NiRenderer* pRender)
{
	NIASSERT(sm_Gfx == NULL);
	sm_Gfx = NiNew SyGfxManager;
	if (!sm_Gfx->Initialize(pRender))
	{
		NILOG("Initialize gfx falied.\n");
		return FALSE;
	}
	InitKeycodeRemapTable();
	return TRUE;
}

void SyGfxManager::Destroy()
{
	if (sm_Gfx)
	{
		sm_Gfx->Shutdown();
		NiDelete sm_Gfx;
		sm_Gfx = NULL;
	}
}

BOOL SyGfxManager::Initialize( NiRenderer* pRender )
{
	NIASSERT(sm_Gfx);
#ifdef NIDEBUG
	m_GfxLoader.SetLog(GPtr<GFxLog>(*new Log()));
#endif 
	GPtr<GFxFileOpener> spFileOpener = *new FileOpener;
	m_GfxLoader.SetFileOpener(spFileOpener); 
	
	GPtr<GFxImageLoader> spImgLoader = *new ImageLoader;
	m_GfxLoader.SetImageLoader(spImgLoader);

	m_pRender = NiNew Render(pRender);
	NIASSERT(m_pRender);
	if (!m_pRender->Initialize())
	{
		return FALSE;
	}
	
	m_spGfxRenderCfg = new GFxRenderConfig(m_pRender->m_GRender);
	m_spGfxRenderCfg->SetRenderFlags(m_spGfxRenderCfg->GetRenderFlags() | GFxRenderConfig::RF_EdgeAA);
	m_GfxLoader.SetRenderConfig(m_spGfxRenderCfg);

	//m_pGfxRenderStats = new GFxRenderStats();
	//m_GfxLoader.SetRenderStats(m_pGfxRenderStats);

	NiDX9Renderer* DX9NiRender = m_pRender->GetNiRender();
	DX9NiRender->AddResetNotificationFunc(ResetNotifyCallback, this);
	return TRUE;
}

void SyGfxManager::Shutdown()
{
	if (m_pRender)
	{
		NiDX9Renderer* DX9NiRender = m_pRender->GetNiRender();
		DX9NiRender->RemoveResetNotificationFunc(ResetNotifyCallback);
		m_pRender->Shutdown();
		NiDelete m_pRender;
		m_pRender = NULL;
	}
	
	
	m_spGfxRenderCfg = NULL;
	m_GfxLoader.SetRenderConfig((GFxRenderConfig*)NULL);
}

bool SyGfxManager::ResetNotifyCallback(bool bBeforeReset, void* pvData)
{
	UTRACE("device lost.");
	return true;
}

void SyGfxManager::BeginRender()
{
	if (sm_Gfx->m_pRender && sm_Gfx->m_pRender->m_pStateBlock)
	{
		sm_Gfx->m_pRender->m_pStateBlock->Capture();
	}
}
void SyGfxManager::EndRender()
{
	if (sm_Gfx->m_pRender && sm_Gfx->m_pRender->m_pStateBlock)
	{
		sm_Gfx->m_pRender->m_pStateBlock->Apply();
	}
}

FlashMoviePtr SyGfxManager::CreateFlashMovie(const char* szSwfFileName)
{
	FlashMovie* pMovie = NiNew FlashMovie;
	if (pMovie )
	{
		if(pMovie->LoadMovie(szSwfFileName))
		{
			return pMovie;
		}else
		{
			NiDelete pMovie;
		}
	}
	return NULL;
}

#endif 