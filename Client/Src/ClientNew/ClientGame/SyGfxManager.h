// ***************************************************************
//  Copyright (C) Sunyou 2007-2008 - All Rights Reserved
//	
//  SyGfxManager.h  Created : P.K. 2008-08-26
//  Scaleform Gfx warpper
// ***************************************************************
#pragma once
#define SUPPORT_GFX 0
#pragma warning(disable : 4005)

#if SUPPORT_GFX
#include <GFxLoader.h>
#include <GFxPlayer.h>
#include <GFxImageResource.h>
#include <GFile.h>
namespace gfx
{
	class Render;
	class FSCommandHandler;
	class UserEventHandler;
}

struct IFlashMovieEventSink
{
	virtual void OnCommand(const char* pcommand, const char* parg) = 0;
	//virtual void OnEvent(const GFxEvent& event) = 0;
};

class FlashMovie : public NiRefObject
{
	friend class gfx::FSCommandHandler;
	friend class gfx::UserEventHandler;
public:
	FlashMovie();
	virtual ~FlashMovie();
	
	BOOL LoadMovie(const char* pszSWFFile);
	void Advance(float fDeltaTime);
	void Render();
	inline BOOL IsEnable() const { return m_bEnabled; }
	inline void Enable(BOOL bEnable) { m_bEnabled = bEnable;}
	
	void SetViewport(int left, int top, int right, int bottom);
	void SetBackgroundAlpha(float fAlpha);
	void SetEventSink(IFlashMovieEventSink* pNewSink) {m_pEventSink = pNewSink;}
	inline GFxMovieView* GetMovieView() { return m_spMovie;}
protected:
	UINT m_bEnabled:1;
	UINT m_bPaused:1;
	UINT m_bDirty:1;
	float m_fSpeedScale;
	float m_fFrameFrequency;
	UINT m_uiFrameCatchUp;
	float m_LastAdvanceTime;
	float m_NextAdvanceTime;
	IFlashMovieEventSink* m_pEventSink;
	GFxMovieInfo m_MovieInfo;
	GPtr<GFxMovieDef> m_spMovieDef;
	GPtr<GFxMovieView> m_spMovie;
};

typedef NiPointer<FlashMovie> FlashMoviePtr;

class SyGfxManager : public NiMemObject
{
	friend class FlashMovie;
public:
	static BOOL Create(NiRenderer* pRender);
	static void Destroy();

	static FlashMoviePtr CreateFlashMovie(const char* szSwfFileName);
	static void BeginRender();
	static void EndRender();
	static inline GFxKey::Code LKeyToGFxKey(KeyCodes elk) { return (GFxKey::Code)smLKey2GfxKey[elk];}
private:
	SyGfxManager(void);
	~SyGfxManager(void);
	
	BOOL Initialize( NiRenderer* pRender );
	void Shutdown();
	static void InitKeycodeRemapTable();
	static bool ResetNotifyCallback(bool bBeforeReset, void* pvData);
	GFxLoader               m_GfxLoader; 
	GPtr<GFxRenderConfig>			m_spGfxRenderCfg;
	//GFxRenderStats*				m_pGfxRenderStats;
	gfx::Render*						m_pRender;
	static SyGfxManager*			sm_Gfx;
	static BYTE				smLKey2GfxKey[0xFF];		//trans ui key code to flash key code.				   
};

#endif 