#ifndef _SYSERVERTIME_H
#define _SYSERVERTIME_H

#include "Network\PacketBuilder.h"

class SyServerTime
{
public:
	SyServerTime();

	BOOL IsInit();

	void SetServerTime(ui64 Time);

	i64 GetDelayTime();

protected:
	BOOL m_bInit;
	ui64 m_ServerTime;
	i64 m_ServerDelay;
};

extern SyServerTime gServerTime;

inline SyServerTime::SyServerTime()
:m_bInit(FALSE)
,m_ServerDelay(0)
{
	
}

inline BOOL SyServerTime::IsInit()
{
	return m_bInit;
}

inline void SyServerTime::SetServerTime(ui64 Time)
{
	m_ServerTime = Time;
	m_bInit = TRUE;
	m_ServerDelay = ::time(NULL) - m_ServerTime;
}

inline i64 SyServerTime::GetDelayTime()
{
	NIASSERT(m_bInit);
	return m_ServerDelay;
}

#endif