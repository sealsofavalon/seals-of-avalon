#pragma once

class Target
{
public:
	struct TargetEntry
	{
		BOOL Object;
		union
		{
			SYObjID ObjectID;
			float  Pos[4];
		};
	};

	Target(void);
	~Target(void);
	
	void AddEntry(SYObjID ObjectID)
	{
		TargetEntry NewEntry;
		memset(&NewEntry, 0, sizeof(TargetEntry));

		NewEntry.Object = TRUE;
		NewEntry.ObjectID = ObjectID;
		m_TargetEntries.push_back(NewEntry);
	}
	void AddEntry(const NiPoint3& Pos, float Radius)
	{
		TargetEntry NewEntry;
		memset(&NewEntry, 0, sizeof(TargetEntry));

		NewEntry.Object = FALSE;
		NewEntry.Pos[0] = Pos.x;
		NewEntry.Pos[1] = Pos.y;
		NewEntry.Pos[2] = Pos.z;
		NewEntry.Pos[3] = Radius;
		m_TargetEntries.push_back(NewEntry);
	}
	int GetNumTargets() const {
		return (int)m_TargetEntries.size();
	}
	const TargetEntry& GetTargetEntry(int index) const 
	{
		NIASSERT(index >= 0 && index < GetNumTargets());
		return m_TargetEntries[index];
	}
	inline const Target& operator =(const Target& Other)
	{
		m_TargetEntries = Other.m_TargetEntries;
		return *this;
	}
	void Clear() {
		m_TargetEntries.clear();
	}

private:
	std::vector<TargetEntry> m_TargetEntries;

};
