#ifndef _TRAIL_H_
#define _TRAIL_H_

#include "Effect/EffectBase.h"
#include "Ribbon.h"

NiSmartPointer(DynamicSimpleMesh);

class TrailEffect : public CEffectBase
{
public:
	TrailEffect();
	~TrailEffect();

	enum
	{
		NUM_SCANLINE = 50,
		NUM_DOUBLE_SCANLINE = NUM_SCANLINE * 2,
	};

	virtual BOOL SelfManage() { return FALSE;}
	virtual BOOL SelfRender() const { return TRUE; }

	void UpdateEffect(float fTimeLine, float fDeltaTime);
	void RenderEffect(const NiCamera* pkCamera);
	void Sample(float fTime);

	void BindObject(NiAVObject* pkObject, float fWidth, float fDelay, float fTotal = 2.0f);
	void SetTexture(const char* szTexFile);
	void SetTexture(NiTexture* pkTexture);
	void SetAxis(const NiPoint3& kAxis) {
		m_kAxis = kAxis;
	}

	NiNode* GetEffectNode() {
		return m_spEffectNode;
	}

	NiNode* GetSoundNode() {
		return m_spSoundNode;
	}

private:
	NiTriShape* m_pkTriShape;
	NiNodePtr m_spEffectNode;
	NiNodePtr m_spSoundNode;
	NiAVObjectPtr m_spBindObject;
	float m_fWidth;
	float m_fDelay;
	float m_fTotal;

	Ribbon m_kTopRibbon;
	Ribbon m_kBtmRibbon;

	NiPoint3 m_kAxis;
};


#endif //_TRAIL_H_

