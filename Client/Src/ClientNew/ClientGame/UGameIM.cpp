#include "StdAfx.h"
#include "UGameIM.h"

UIMP_CLASS(UGameIM,UControl);
UBEGIN_MESSAGE_MAP(UGameIM,UCmdTarget)
UEND_MESSAGE_MAP()
void UGameIM::StaticInit()
{

}

UGameIM::UGameIM(void)
{
}

UGameIM::~UGameIM(void)
{
}
BOOL UGameIM::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	return TRUE;
}
void UGameIM::OnDestroy()
{
	UControl::OnDestroy();
}