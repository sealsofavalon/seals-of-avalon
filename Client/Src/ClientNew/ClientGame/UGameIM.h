#pragma once
#include "UInc.h"

class UGameIM : public UControl
{
	UDEC_CLASS(UGameIM);
	UDEC_MESSAGEMAP();
public:
	UGameIM(void);
	virtual ~UGameIM(void);

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

};
