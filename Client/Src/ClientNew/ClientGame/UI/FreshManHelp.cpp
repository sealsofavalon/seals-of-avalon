#include "stdafx.h"
#include "FreshManHelp.h"
#include "../Network/PacketBuilder.h"
#include "Utils/NewBirdEventDB.h"
#include "../../../TinyXML/TinyXML.h"
#include "UISystem.h"
#include "UFun.h"
#include "UIGamePlay.h"
const char* cMoiveName[] = {
	"Begin",
	"Move",
	"Base",
};
GameTutorial::GameTutorial()
{
	sm_pTutorial = this;
	mPlayIndex = 0;
	mbJustSay = false;
}
GameTutorial::~GameTutorial()
{

}
GameTutorial* GameTutorial::sm_pTutorial = NULL;
GameTutorial* GameTutorial::Instance()
{
	if(sm_pTutorial == NULL)
	{
		return new GameTutorial;
	}
	return sm_pTutorial;
}

void GameTutorial::Clear()
{
	m_FrameList.clear();
	mPlayIndex = 0;
	m_FrameSayList.clear();
	mbJustSay = false;
}

void GameTutorial::Updata(float delta)
{
	static float dtime = 0.0f;
	if(dtime > 0.0f)
	{
		dtime -= delta;
	}
	if(mbJustSay && dtime <= 0.0f)
	{
		mbJustSay = false;
		PlayNext();
		dtime = 5.0f;
	}
}

void GameTutorial::ShowTutorial()
{
	UControl * turtoral = SYState()->UI->LoadDialogEX(DID_FRESHMANHELP);
	UControl::sm_System->PushDialog(turtoral);
	ReadMovieFileByName("Begin");
	BeginPlay();
}
void GameTutorial::CloseTutorial()
{
	UControl * turtoral = SYState()->UI->LoadDialogEX(DID_FRESHMANHELP);
	UControl::sm_System->PopDialog(turtoral);
	EndPlay();
}
void GameTutorial::DrawToolTip(URender* pRender, UFontPtr font, USkinPtr popo, const UPoint& pos, const WCHAR* pwChar)
{
	if(!pRender)
		return;
	if (!popo)
		return;
	if(!pwChar)
		return;
	static int LineWidth = 150;
	UPoint Size;
	Size.x = font->GetStrNWidth(pwChar, wcslen(pwChar));
	Size.y = font->GetHeight();

	int lineNum = Size.x / LineWidth + 1;

	Size.x  = min(Size.x , LineWidth);
	Size.y = lineNum * Size.y + (lineNum - 1) * 4;

	UPoint Position(pos.x + 20, pos.y - 20 - Size.y);

	UDrawResizeBitmap(pRender, popo, Position, Size);
	Position.x -= 5;
	UDrawStaticText(pRender, font, Position, LineWidth, UColor(76,143,255,255), pwChar, UT_LEFT, 4);
}
bool ParseBOOL(const char* cbool)
{
	if(!cbool)
		return false;
	if (NiStricmp(cbool,"TRUE") == 0)
		return true;
	else if (NiStricmp(cbool,"FALSE") == 0)
		return false;
	return false;
}
#define POSSIBLY_UNUSED
float ParseFloat(const char* cfloat)
{
	float ffData;
#if defined(_MSC_VER) && _MSC_VER >= 1400
	int POSSIBLY_UNUSED iFieldsAssigned = sscanf_s(cfloat, "%f", &ffData);
#else
	int POSSIBLY_UNUSED iFieldsAssigned = sscanf(cfloat, "%f", &ffData);
#endif
	return ffData;
}
mouseKey ParseMouseKey(const char* cMouse)
{
	if (NiStricmp(cMouse,"LEFTKEY") == 0)
		return MOUSE_KEY_LEFT;
	else if (NiStricmp(cMouse,"RIGHTKEY") == 0)
		return MOUSE_KEY_RIGHT;
	else if (NiStricmp(cMouse,"MIDDLEKEY") == 0)
		return MOUSE_KEY_MIDDLE;
	return MOUSE_KEY_NONE;
}
void GameTutorial::ReadMovieFileByName(const char* MovieName)
{
	char buf[256];
	sprintf(buf, "FreshMan_Movie_%s.xml", MovieName);
	ReadMovieFile(buf);
}
void GameTutorial::ReadMovieFile(const char* filename)
{
	//Open file
	char* acBuffer = (char*)malloc(256*1024);
	static const char* realpath = "DATA\\UI\\FreshManHelp\\Movie\\";
	sprintf(acBuffer, "%s%s", realpath, filename);
	NiFile* pkFile = NiFile::GetFile(acBuffer, NiFile::READ_ONLY);
	if(!pkFile || !(*pkFile))
	{
		NiDelete pkFile;
		free( acBuffer );
		return;
	}
	unsigned int uiSize = pkFile->Read(acBuffer, 256*1024);
	NIASSERT(uiSize < 256*1024 - 1);

	acBuffer[uiSize] = '\0';
	NiDelete pkFile;

	//parse
	TiXmlDocument xdoc;
	xdoc.Parse(acBuffer);

	if (xdoc.Error())
	{
		free( acBuffer );
		return;
	}

	TiXmlElement* pElement = xdoc.RootElement();
	if(!pElement)
	{
		free( acBuffer );
		return;
	}

	if(stricmp(pElement->Value(), "Movie") != 0)
	{
		free( acBuffer );
		return;
	}

	TiXmlElement* pChildElem = pElement->FirstChildElement();
	FrameListItem ReadBuf;
	BeginInput();
	while (pChildElem)
	{	
		if( pChildElem && NiStricmp("Frame", pChildElem->Value()) == 0 )
		{
			const char* pcBool = pChildElem->Attribute("IsKeyBoard");
			if(pcBool)
				ReadBuf.IsKeyBoard = ParseBOOL(pcBool);
			const char* pcIndex = pChildElem->Attribute("Index");
			if (pcIndex)
				ReadBuf.playIndex = atoi(pcIndex);

			TiXmlElement* pDefElement = pChildElem->FirstChildElement();
			if( pDefElement && NiStricmp("def", pDefElement->Value()) == 0 )
			{
				const char* cKeycode = pDefElement->Attribute("KeyCode");
				if(cKeycode)
				{
					if(ReadBuf.IsKeyBoard)
						ReadBuf.KeyCode = StringKey2LK(cKeycode);
					else 
						ReadBuf.MouseKey = ParseMouseKey(cKeycode);
				}
				const char* WinkTime = pDefElement->Attribute("WinkTime");
				if (WinkTime)
					ReadBuf.WinkTime = ParseFloat(WinkTime);
			}

			TiXmlElement* pToolTipElement = pDefElement->NextSiblingElement();
			if( pToolTipElement && NiStricmp("tooltip", pToolTipElement->Value()) == 0 )
			{
				const char* cToolTipDec = pToolTipElement->Attribute("Desc");
				if(cToolTipDec)
					ReadBuf.tooltip = AnisToUTF8(cToolTipDec);
			}
			if(ReadBuf.IsKeyBoard)
				InputList(ReadBuf.playIndex, ReadBuf.KeyCode, ReadBuf.WinkTime, ReadBuf.tooltip.c_str());
			else
				InputListMouse(ReadBuf.playIndex, ReadBuf.MouseKey, ReadBuf.WinkTime, ReadBuf.tooltip.c_str());
		}else if(pChildElem && NiStricmp("FrameSay", pChildElem->Value()) == 0 )
		{
			FrameSayItem sayitem;
			const char* pcIndex = pChildElem->Attribute("Index");
			if (pcIndex)
				sayitem.FrameIndex = atoi(pcIndex);

			TiXmlElement* pSayElement = pChildElem->FirstChildElement();
			if( pSayElement && NiStricmp("Saying", pSayElement->Value()) == 0 )
			{
				const char* pcSay = pSayElement->Attribute("des");
				if (pcSay)
					sayitem.Say = AnisToUTF8(pcSay);
			}
			InputSay(sayitem.FrameIndex, sayitem.Say.c_str());
		}
		pChildElem = pChildElem->NextSiblingElement();
	}
	EndInput();
	free( acBuffer );
}

#include "USystemSet.h"
void GameTutorial::ParseCommand(const char* command, const char* content)
{
	if(NiStricmp(command, "CLOSE") == 0)
	{
		CloseTutorial();
		return;
	}
	if(NiStricmp(command, "MOVIE") == 0)
	{
		ReadMovieFileByName(content);
		BeginPlay();
		return;
	}
	if(NiStricmp(command, "CHANGEKEY") == 0)
	{
		CloseTutorial();
		UKeySet* pKeySet = INGAMEGETFRAME(UKeySet, FRAME_IG_KEYSET);
		if (pKeySet)
			pKeySet->SetVisible(TRUE);
		return;
	}
}

bool KeyIndex(const FrameListItem& LI, const FrameListItem& RI)
{
	return (LI.playIndex<RI.playIndex);
}
bool KeyIndex1(const FrameSayItem& LI, const FrameSayItem& RI)
{
	return (LI.FrameIndex<RI.FrameIndex);
}
void GameTutorial::BeginInput()
{
	m_FrameList.clear();
	m_FrameSayList.clear();
}
void GameTutorial::InputSay(int playIndex, const char* say)
{
	FrameSayItem entry;
	entry.FrameIndex = playIndex;
	entry.Say = say;
	m_FrameSayList.push_back(entry);
}
void GameTutorial::InputList( int playIndex, UINT nKeyCode, float time, const char* tooltip )
{
	FrameListItem entry;
	entry.IsKeyBoard = true;
	entry.playIndex = playIndex;
	entry.KeyCode = nKeyCode;
	entry.WinkTime = time;
	entry.tooltip.resize(0);
	if(tooltip)
		entry.tooltip = tooltip;
	m_FrameList.push_back(entry);
}
void GameTutorial::InputListMouse( int playIndex, UINT nMoseCode, float time, const char* tooltip)
{
	FrameListItem entry;
	entry.IsKeyBoard = false;
	entry.playIndex = playIndex;
	entry.MouseKey = nMoseCode;
	entry.WinkTime = time;
	entry.tooltip.resize(0);
	if(tooltip)
		entry.tooltip = tooltip;
	m_FrameList.push_back(entry);
}
void GameTutorial::EndInput()
{
	std::sort(m_FrameList.begin(), m_FrameList.end(), &KeyIndex);
	std::sort(m_FrameSayList.begin(), m_FrameSayList.end(), &KeyIndex1);
}

void GameTutorial::BeginPlay()
{
	if(!m_FrameList.size() && !m_FrameSayList.size())
		return;
	if(!m_FrameList.size())
	{
		if(m_FrameSayList.size())
			mPlayIndex = m_FrameSayList[0].FrameIndex;
	}
	else 
		mPlayIndex = m_FrameList[0].playIndex;

	PlayNext();
}

void GameTutorial::PlayNext()
{
	UITutorial * turtoral = UDynamicCast(UITutorial, SYState()->UI->LoadDialogEX(DID_FRESHMANHELP));
	if(!turtoral)
		return;
	UKeyBoard* pKeyBoard = turtoral->GetKeyBoard();
	UAMouse* pMouse = turtoral->GetMouse();
	std::vector<FrameListItem>::iterator it = m_FrameList.begin();
	bool btemp = true;
	while(it != m_FrameList.end())
	{
		if(mPlayIndex == it->playIndex)
		{
			if(it->IsKeyBoard && pKeyBoard)
				pKeyBoard->WinkKey(it->KeyCode, it->WinkTime, it->tooltip.c_str());
			else if( !it->IsKeyBoard && pMouse )
				pMouse->WinkKey(it->MouseKey, it->WinkTime, it->tooltip.c_str());

			it = m_FrameList.erase(it);
			if(it == m_FrameList.end())
				btemp = false;
			continue;
		}
		++it;
	}
	std::vector<FrameSayItem>::iterator it1 = m_FrameSayList.begin();
	while(it1 != m_FrameSayList.end())
	{
		if(mPlayIndex == it1->FrameIndex)
		{
			turtoral->SetFrameSay(it1->Say.c_str());
			it1 = m_FrameSayList.erase(it1);
			continue;
		}
		++it1;
	}
	if(!m_FrameList.size())
	{
		if(m_FrameSayList.size() && btemp)
		{
			mbJustSay = true;
			return;
		}
		else
		{
			EndPlay();
			return;
		}
	}
	mPlayIndex = m_FrameList[0].playIndex;
}

void GameTutorial::EndPlay()
{
	m_FrameList.clear();
	mPlayIndex = 0;
}
void GameTutorial::OnPlayEnd()
{
	if(!m_FrameList.size())
	{
		if(m_FrameSayList.size())
		{
			mPlayIndex = m_FrameSayList[0].FrameIndex;
			PlayNext();
			return;
		}
		mPlayIndex = 0;
		return;
	}
	PlayNext();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int winkBase::WinkCount = 0;
void sKeyButton::SetField(const char* Filedname, const char* Value)
{
	if(strcmp(Filedname, "x") == 0) Pos.x = atoi(Value);
	if(strcmp(Filedname, "y") == 0) Pos.y = atoi(Value);
	if(strcmp(Filedname, "w") == 0) Size.x = atoi(Value);
	if(strcmp(Filedname, "h") == 0) Size.y = atoi(Value);
	if(strcmp(Filedname, "keycode") == 0) KeyButtonName = Filedname;
}
UIMP_CLASS(UKeyBoard,UControl);
void UKeyBoard::StaticInit()
{
	UREG_PROPERTY("KeyLayOutFile", UPT_STRING, UFIELD_OFFSET(UKeyBoard, m_strFileName));
}
UKeyBoard::UKeyBoard()
{
	memset(mWinkKey, NULL, sizeof(mWinkKey));
}
UKeyBoard::~UKeyBoard()
{
}
BOOL UKeyBoard::OnCreate()
{
	if(!UControl::OnCreate())
		return FALSE;

	if (m_strFileName.GetBuffer())
	{
		//TODO::Parse KeyBoard LayOut;.
		if(!ParseKeyBoardLayOut())
			return FALSE;
	}
	return TRUE;
}
BOOL UKeyBoard::ParseKeyBoardLayOut()
{
	//Open file
	char* acBuffer = (char*)malloc(256*1024);
	NiFile* pkFile = NiFile::GetFile(m_strFileName.GetBuffer(), NiFile::READ_ONLY);
	if(!pkFile || !(*pkFile))
	{
		NiDelete pkFile;
		free( acBuffer );
		return FALSE;
	}
	unsigned int uiSize = pkFile->Read(acBuffer, 256*1024);
	NIASSERT(uiSize < 256*1024 - 1);

	acBuffer[uiSize] = '\0';
	NiDelete pkFile;

	//parse
	TiXmlDocument xdoc;
	xdoc.Parse(acBuffer);

	if (xdoc.Error())
	{
		free( acBuffer );
		return FALSE;
	}

	TiXmlElement* pElement = xdoc.RootElement();
	if(!pElement)
	{
		free( acBuffer );
		return FALSE;
	}

	if(stricmp(pElement->Value(), "keyBoard") != 0)
	{
		free( acBuffer );
		return FALSE;
	}

	TiXmlElement* pChildElem = pElement->FirstChildElement();
	while (pChildElem)
	{
		static const char* Key_Button_Str = "keycode";
		const char* pKeyName = pChildElem->Attribute(Key_Button_Str);
		if (pKeyName == NULL)
		{
			pChildElem = pChildElem->NextSiblingElement();
			continue;
		}
		sKeyButton KeyButton;
		KeyButton.KeyButtonName = pKeyName;
		TiXmlAttribute* pAttr = pChildElem->FirstAttribute();
		const char* pField;
		const char* pValue;
		while (pAttr)
		{
			pField = pAttr->Name();
			pValue = pAttr->Value();
			KeyButton.SetField(pField,pValue);
			pAttr = pAttr->Next();
		}
		sprintf(acBuffer, "Data\\UI\\FreshManHelp\\KeyButton\\%s.png", pKeyName);
		KeyButton.KeyTexture = sm_UiRender->LoadTexture(acBuffer);
		KeyButton.KeyCode = StringKey2LK(pKeyName);
		if(!KeyButton.KeyTexture)
		{
			pChildElem = pChildElem->NextSiblingElement();
			continue;
		}
		if(m_KeyButtonMap.find(pKeyName) == m_KeyButtonMap.end())
		{
			m_KeyButtonMap.insert(std::map<std::string , sKeyButton>::value_type(pKeyName, KeyButton));
		}
		pChildElem = pChildElem->NextSiblingElement();
	}
	free( acBuffer );
	return TRUE;
}
void UKeyBoard::OnDestroy()
{
	m_KeyButtonMap.clear();
	memset(mWinkKey, NULL, sizeof(mWinkKey));
	UControl::OnDestroy();
}
void UKeyBoard::OnTimer(float fDeltaTime)
{
	for(int i = 0; i < 20 ; i++)
	{
		sKeyButton* pkeyButton = mWinkKey[i];
		if(!pkeyButton)
			continue;
		if(pkeyButton->WinkTime <= 0.f)
		{
			pkeyButton->IsWink = false;
			pkeyButton->color.a = 255;
			if(!--pkeyButton->WinkCount)
				GameTur->OnPlayEnd();
			mWinkKey[i] = NULL;
			continue;
		}
		pkeyButton->WinkTime -= fDeltaTime;
		pkeyButton->DeltaTime += fDeltaTime;
		if(pkeyButton->DeltaTime > pkeyButton->WinkDelta)
		{
			pkeyButton->color.a = 255 - pkeyButton->color.a;
			pkeyButton->DeltaTime = 0.f;
		}
	}
}
void UKeyBoard::ClearWink()
{
	for(int i = 0; i < 20 ; i++)
	{
		sKeyButton* pkeyButton = mWinkKey[i];
		if(!pkeyButton)
			continue;
		pkeyButton->IsWink = false;
		pkeyButton->color.a = 255;
		pkeyButton->WinkCount--;
		mWinkKey[i] = NULL;
	}

}
void UKeyBoard::OnRender(const UPoint& offset, const URect& updateRect)
{
	UControl::OnRender(offset, updateRect);

	std::map<std::string , sKeyButton>::iterator it = m_KeyButtonMap.begin();
	while(it != m_KeyButtonMap.end())
	{
		sKeyButton* pkeyButton = &it->second;
		if(pkeyButton->KeyIsDown || pkeyButton->IsWink)
		{
			if(pkeyButton->bToolTip)
				GameTur->DrawToolTip(sm_UiRender, m_Style->m_spFont, mPoPo[0],offset + pkeyButton->Pos, pkeyButton->ToolTip);
			sm_UiRender->DrawImage(pkeyButton->KeyTexture, URect(offset + pkeyButton->Pos, pkeyButton->Size), pkeyButton->color);
		}
		++it;
	}
}
void UKeyBoard::KeyDown(UINT nKeyCode,UINT nFlags)
{
	std::map<std::string , sKeyButton>::iterator it = m_KeyButtonMap.find(LK2StringKey(nKeyCode));
	if(it != m_KeyButtonMap.end())
	{
		sKeyButton* pkeyButton = &it->second;
		pkeyButton->KeyIsDown = true;
	}
}
void UKeyBoard::KeyUp(UINT nKeyCode,UINT nFlags)
{
	std::map<std::string , sKeyButton>::iterator it = m_KeyButtonMap.find(LK2StringKey(nKeyCode));
	if(it != m_KeyButtonMap.end())
	{
		sKeyButton* pkeyButton = &it->second;
		pkeyButton->KeyIsDown = false;
	}
}
void UKeyBoard::WinkKey(UINT nKeyCode, float time, const char* tooltip)
{
	std::map<std::string , sKeyButton>::iterator it = m_KeyButtonMap.find(LK2StringKey(nKeyCode));
	if(it != m_KeyButtonMap.end())
	{
		sKeyButton* pkeyButton = &it->second;
		pkeyButton->IsWink = true;
		pkeyButton->WinkTime = time;
		pkeyButton->DeltaTime = 0.f;
		if(tooltip)
		{
			int nRet = MultiByteToWideChar(CP_UTF8, 0, tooltip, strlen(tooltip), pkeyButton->ToolTip, 256 -1);
			pkeyButton->ToolTip[nRet] = 0;

			pkeyButton->bToolTip = (nRet != 0);
		}
		for(int i = 0 ; i < 20 ; i++)
		{
			if(mWinkKey[i] == NULL)
			{
				mWinkKey[i] = pkeyButton;
				pkeyButton->WinkCount++;
				break;
			}
		}
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UAMouse, UControl)
void UAMouse::StaticInit(){}

UAMouse::UAMouse()
{
	m_LeftButton = NULL;
	m_RightButton = NULL;
	m_MiddleButton = NULL;
	memset(mWinkKey, NULL, sizeof(mWinkKey));
}
UAMouse::~UAMouse()
{
}
BOOL UAMouse::OnCreate()
{
	if(!UControl::OnCreate())
		return FALSE;
	sMouseButton smbtn;
	if(m_LeftButton == NULL)
	{
		m_LeftButton = UDynamicCast(UBitmap, GetChildByID(1));
		if(!m_LeftButton)
			return FALSE;
		smbtn.m_MouseCtrl = m_LeftButton;
		m_MouseButton.push_back(smbtn);
	}
	if(m_RightButton == NULL)
	{
		m_RightButton = UDynamicCast(UBitmap, GetChildByID(3));
		if(!m_RightButton)
			return FALSE;
		smbtn.m_MouseCtrl = m_RightButton;
		m_MouseButton.push_back(smbtn);
	}
	if(m_MiddleButton == NULL)
	{
		m_MiddleButton = UDynamicCast(UBitmap, GetChildByID(2));
		if(!m_MiddleButton)
			return FALSE;
		smbtn.m_MouseCtrl = m_MiddleButton;
		m_MouseButton.push_back(smbtn);
	}
	return TRUE;
}
void UAMouse::OnDestroy()
{
	m_LeftButton = NULL;
	m_RightButton = NULL;
	m_MiddleButton = NULL;
	memset(mWinkKey, NULL, sizeof(mWinkKey));
	UControl::OnDestroy();
}
void UAMouse::OnRender(const UPoint& offset, const URect& updateRect)
{
	UControl::OnRender(offset, updateRect);
	for(int i = 0; i < 3 ; i++)
	{
		sMouseButton* pMouseButton = mWinkKey[i];
		if(pMouseButton && pMouseButton->bToolTip)
			GameTur->DrawToolTip(sm_UiRender, m_Style->m_spFont, mPoPo[0], offset + pMouseButton->m_MouseCtrl->GetWindowPos(), pMouseButton->ToolTip);
	}
}
void UAMouse::OnTimer(float fDeltaTime)
{
	for(int i = 0; i < MOUSE_KEY_NONE ; i++)
	{
		sMouseButton* pMouseButton = mWinkKey[i];
		if(!pMouseButton)
			continue;
		if(pMouseButton->WinkTime <= 0.f)
		{
			pMouseButton->IsWink = false;
			pMouseButton->m_MouseCtrl->SetVisible(FALSE);
			if(!--pMouseButton->WinkCount)
				GameTur->OnPlayEnd();
			mWinkKey[i] = NULL;
			continue;
		}
		pMouseButton->WinkTime -= fDeltaTime;
		pMouseButton->DeltaTime += fDeltaTime;
		if(pMouseButton->DeltaTime > pMouseButton->WinkDelta)
		{
			pMouseButton->m_MouseCtrl->SetVisible(!pMouseButton->m_MouseCtrl->IsVisible());
			pMouseButton->DeltaTime = 0.f;
		}
	}
}

void UAMouse::WinkKey(UINT nKeyCode, float time, const char* tooltip)
{
	if(nKeyCode >= MOUSE_KEY_NONE || nKeyCode < 0)
		return;

	sMouseButton* pMouseButton = &m_MouseButton[nKeyCode];
	pMouseButton->IsWink = true;
	pMouseButton->WinkTime = time;
	pMouseButton->DeltaTime = 0.f;
	if(tooltip)
	{
		int nRet = MultiByteToWideChar(CP_UTF8, 0, tooltip, strlen(tooltip), pMouseButton->ToolTip, 256 -1);
		pMouseButton->ToolTip[nRet] = 0;

		pMouseButton->bToolTip = (nRet != 0);
	}
	for(int i = 0 ; i < 3 ; i++)
	{
		if(mWinkKey[i] == NULL)
		{
			mWinkKey[i] = pMouseButton;
			pMouseButton->WinkCount++;
			break;
		}
	}
}
void UAMouse::LBDown()
{
	m_LeftButton->SetVisible(TRUE);
}
void UAMouse::LBUp()
{
	m_LeftButton->SetVisible(FALSE);
}
void UAMouse::RBDown()
{
	m_RightButton->SetVisible(TRUE);
}
void UAMouse::RBUp()
{
	m_RightButton->SetVisible(FALSE);
}
void UAMouse::MouseWheel()
{
	m_MiddleButton->SetVisible(TRUE);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIContentCtrl, UControl)
void UIContentCtrl::StaticInit(){}

UIContentCtrl::UIContentCtrl()
{
	m_Popo = NULL;
}
UIContentCtrl::~UIContentCtrl()
{}
BOOL UIContentCtrl::OnCreate()
{
	if(!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_Popo == NULL)
	{
		m_Popo = sm_System->LoadSkin("SystemTips\\bg.skin");
		if (!m_Popo)
		{
			return FALSE;
		}
	}
	return TRUE;
}
void UIContentCtrl::OnDestroy()
{
	UControl::OnDestroy();
}
void UIContentCtrl::OnRender(const UPoint& offset, const URect& updateRect)
{
	if (m_Popo)
		UDrawResizeImage(RESIZE_WANDH, sm_UiRender, m_Popo, updateRect.GetPosition(), updateRect.GetSize());
	RenderChildWindow(offset, updateRect);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UITutorial, UControl)
UBEGIN_MESSAGE_MAP(UITutorial, UControl)
UON_RED_URL_CLICKED(3,&UITutorial::OnClickURL)
UEND_MESSAGE_MAP()
void UITutorial::StaticInit(){}
UITutorial::UITutorial()
{
	mpKeyBoard = NULL;
	mpAMouse = NULL;
	mpNiAVctrlEff = NULL;
	mpRichTextCtrl = NULL;
}
UITutorial::~UITutorial()
{}
void UITutorial::SetFrameSay(const char* say)
{
	if(mpRichTextCtrl)
		mpRichTextCtrl->SetText(say, strlen(say));
}
BOOL UITutorial::OnCreate()
{
	if(!UControl::OnCreate())
	{
		return FALSE;
	}
	if(mpKeyBoard == NULL)
	{
		mpKeyBoard = UDynamicCast(UKeyBoard, GetChildByID(0)->GetChildByID(0));
		if(!mpKeyBoard)
			return FALSE;
	}
	if(mpAMouse == NULL)
	{
		mpAMouse = UDynamicCast(UAMouse, GetChildByID(0)->GetChildByID(1));
		if(!mpAMouse)
			return FALSE;
	}
	if(mpNiAVctrlEff == NULL)
	{
		mpNiAVctrlEff = UDynamicCast(UNiAVControlEff, GetChildByID(2));
		if(!mpNiAVctrlEff)
			return FALSE;
	}
	if(mpRichTextCtrl == NULL)
	{
		mpRichTextCtrl = UDynamicCast(URichTextCtrl, GetChildByID(3));
		if (!mpRichTextCtrl)	{
			return FALSE;
		}
	}
	return TRUE;
}
void UITutorial::OnDestroy()
{
	mpKeyBoard = NULL;
	mpAMouse = NULL;
	mpNiAVctrlEff = NULL;
	mpRichTextCtrl = NULL;
	UControl::OnDestroy();
}
void UITutorial::OnRender(const UPoint& offset, const URect& updateRect)
{
	UPoint pos(mpNiAVctrlEff->GetWindowPos());
	pos.x += mpNiAVctrlEff->GetWidth();
	if(mPoPo[0])
		UDrawResizeBitmap(sm_UiRender,mPoPo[1],mpRichTextCtrl->GetWindowPos(),mpRichTextCtrl->GetWindowSize());

	UControl::OnRender(offset, updateRect);
}

BOOL UITutorial::OnEscape()
{
	return TRUE;
}

void UITutorial::OnTimer(float fDeltaTime)
{
	GameTur->Updata(fDeltaTime);
	UControl::OnTimer(fDeltaTime);
}

void UITutorial::OnClickURL(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;
	int findpos = strURL.find('_');
	if(findpos == -1)
	{
		GameTur->ParseCommand(strURL.c_str(), NULL);
		return;
	}
	std::string command = strURL.substr(0,findpos);
	std::string content = strURL.substr(findpos + 1,strURL.size());
	GameTur->ParseCommand(command.c_str(), content.c_str());
}

void UITutorial::OnMouseUp(const UPoint& position, UINT flags)
{
}
void UITutorial::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
}
void UITutorial::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
}
void UITutorial::OnRightMouseUp(const UPoint& position, UINT flags)
{
}
void UITutorial::OnMiddleMouseDown(const UPoint& position, UINT flags)
{
}
void UITutorial::OnMiddleMouseUp(const UPoint& position, UINT flags)
{
}
BOOL UITutorial::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	return UControl::OnKeyDown(nKeyCode, nRepCnt, nFlags);
}
BOOL UITutorial::OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	return UControl::OnKeyUp(nKeyCode, nRepCnt, nFlags);
}
BOOL UITutorial::OnMouseWheel(const UPoint& position, int delta , UINT flags)
{
	return UControl::OnMouseWheel(position, delta, flags);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UFreshManTip, UDialog)
UBEGIN_MESSAGE_MAP(UFreshManTip,UControl)
UON_BN_CLICKED(3,&UFreshManTip::OnClickClose)
UON_BN_CLICKED(4,&UFreshManTip::OnClickNextTip)
UON_BN_CLICKED(6,&UFreshManTip::OnTipShowAgain)
UEND_MESSAGE_MAP()
void UFreshManTip::StaticInit()
{

}
UFreshManTip::UFreshManTip()
{
	m_RichEdit = NULL;
	m_CloseBtn = NULL;
	m_NextTipBtn = NULL;
	m_TitleText = NULL;
	m_IsTipShowAgain = TRUE;
	mEventIndex = 0;
	memset(m_Tutorialflag, -1, 8);
	m_bShowClose = FALSE;
	m_IsUpdateChild = FALSE;
}
UFreshManTip::~UFreshManTip()
{
	m_RichEdit = NULL;
	m_CloseBtn = NULL;
	m_NextTipBtn = NULL;
	m_TitleText = NULL;
}
BOOL UFreshManTip::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	if (m_RichEdit == NULL)
	{
		m_RichEdit = UDynamicCast(URichTextCtrl, GetChildByID(2));
		if (!m_RichEdit)
		{
			return FALSE;
		}
	}	
	if (m_CloseBtn == NULL)
	{
		m_CloseBtn = UDynamicCast(UButton, GetChildByID(3));
		if (!m_CloseBtn)
		{
			return FALSE;
		}
	}	
	if (m_NextTipBtn == NULL)
	{
		m_NextTipBtn = UDynamicCast(UButton, GetChildByID(4));
		if (!m_NextTipBtn)
		{
			return FALSE;
		}
		m_NextTipBtn->SetVisible(FALSE);
	}
	if (m_TitleText == NULL)
	{
		m_TitleText = UDynamicCast(UStaticText, GetChildByID(1));
		if (!m_TitleText)
		{
			return FALSE;
		}
	}
	return TRUE;
}
void UFreshManTip::OnDestroy()
{
	m_IsTipShowAgain = TRUE;
	m_TitleText->SetText(_TRAN("С������ʾ"));
	m_TipQueue.clear();
	mEventIndex = 0;
	memset(m_Tutorialflag, -1, 8);
	UDialog::OnDestroy();
}
void UFreshManTip::AddTipMessage(std::string& str)
{
	assert(m_RichEdit);
	m_RichEdit->ClearText();
	m_RichEdit->AppendText(str.c_str(), str.length(), true);
}
void UFreshManTip::PraseTutorialFlag(ui8 flag[], UINT uiSize)
{
	memcpy(m_Tutorialflag, flag, uiSize);

	//HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_MOVE);
	//HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_SCROLL);
	HelpEventTrigger(TUTORIAL_FLAG_HOW_TO_MOVE);
	HelpEventTrigger(TUTORIAL_FLAG_HOW_TO_SCROLL);
	HelpEventTrigger(TUTORIAL_FLAG_START_ANIMATION);
	//HelpEventTrigger(TURORIAL_FLAG_HOW_TO_OPENBAG);
}
void UFreshManTip::OnTipShowAgain()
{
	UButton* btn = UDynamicCast(UButton, GetChildByID(6));
	if (btn)
	{
		m_IsTipShowAgain = !btn->IsChecked();
	}
}
#include "UIIndex.h"
void UFreshManTip::OnClickClose()
{
	//todo sendMessage
	if (!m_IsTipShowAgain)
	{
		UButton* btn = UDynamicCast(UButton, GetChildByID(6));
		if (btn)
		{
			btn->SetCheckState(FALSE);
		}
		PacketBuilder->SendFreshManHelpClear();

		//UInGame::GetFrame(FRAME_IG_FRESHMANHELPBTN)->SetVisible(FALSE);
		//UInGame::GetFrame(FRAME_IG_FRESHMANHELPBTN)->SetGlint(FALSE);
	}
	m_TipQueue.clear();
	SetVisible(FALSE);
	if (mEventIndex == TUTORIAL_FLAG_HOW_TO_SCROLL)
	{
		HelpEventTrigger(TUTORIAL_FLAG_HOW_TO_OPENBAG);
	}
	if (mEventIndex == TUTORIAL_FLAG_HOW_TO_LEARN_SPELL)
	{
		HelpEventTrigger(TUTORIAL_FLAG_HOW_TO_USE_SPELL);
	}
	if (mEventIndex == TUTORIAL_FLAG_HOW_TO_EQUIP)
	{
		HelpEventTrigger(TUTORIAL_FLAG_QUEST_TIP);
	}
}
void UFreshManTip::OnClickNextTip()
{
	std::vector<std::string>::iterator it = m_TipQueue.begin();
	if (it != m_TipQueue.end())
	{
		it = m_TipQueue.erase(it);	
		std::string str = it->c_str();
		AddTipMessage(str);
		if (m_TipQueue.size() == 1)
			m_NextTipBtn->SetVisible(FALSE);
	}
}
void UFreshManTip::HelpEventTrigger(UINT EventIndex)
{
	return;
	if (!m_IsTipShowAgain)
		return;
	if (GetTurorialFlag(m_Tutorialflag, EventIndex))
		return;
	mEventIndex = EventIndex;
	if(mEventIndex == TUTORIAL_FLAG_START_ANIMATION)
	{
		//UInGame::Get()->TurtoralShow();

		//SetTurorialFlag(m_Tutorialflag, TUTORIAL_FLAG_START_ANIMATION);
		//PacketBuilder->SendFreshManHelpEnable(TUTORIAL_FLAG_START_ANIMATION);
		return;
	}
	if(!GetTurorialFlag(m_Tutorialflag, EventIndex))
	{
		std::string title;
		g_pkNewBirdEventDB->GetNewBirdEventStr(EventIndex, m_TipQueue);
		if(g_pkNewBirdEventDB->GetNewBirdEventTitle(EventIndex, title))
			m_TitleText;
		//m_TitleText->SetText(title.c_str());
	}
	SetTurorialFlag(m_Tutorialflag, EventIndex);
	PacketBuilder->SendFreshManHelpEnable(EventIndex);

	if (!m_TipQueue.size())
		return;
	if (m_TipQueue.size() > 1)
	{
		std::vector<std::string>::iterator it = m_TipQueue.begin();
		std::string str = it->c_str();
		AddTipMessage(str);
		m_NextTipBtn->SetVisible(TRUE);
	}
	else
	{
		AddTipMessage(m_TipQueue[0]);
		m_NextTipBtn->SetVisible(FALSE);
	}
	SetVisible(TRUE);
}