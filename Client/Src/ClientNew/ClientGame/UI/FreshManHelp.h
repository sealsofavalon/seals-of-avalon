#ifndef FRESHMANHELP_H
#define FRESHMANHELP_H
//--------------------------------------
//���ְ���
//--------------------------------------
#include "UInc.h"
#include "UNiAVControl.h"
#define GameTur GameTutorial::Instance()
enum mouseKey
{
	MOUSE_KEY_LEFT,
	MOUSE_KEY_RIGHT,
	MOUSE_KEY_MIDDLE,
	MOUSE_KEY_NONE,
};
struct FrameListItem
{
	bool IsKeyBoard;
	UINT KeyCode;
	int MouseKey;
	float WinkTime;
	std::string tooltip;
	int playIndex;
};
struct FrameSayItem
{
	int FrameIndex;
	std::string Say;
};
class GameTutorial
{
	GameTutorial();
	~GameTutorial();
public:
	enum eMovieName
	{
		FRESHMAN_MOVIE_BEGIN,
		FRESHMAN_MOVIE_MOVE,
		FRESHMAN_MOVIE_BASECONTROL,
	};
	static GameTutorial* sm_pTutorial;
	static GameTutorial* Instance();
	void Clear();
	void Updata(float delta);

	void ShowTutorial();
	void CloseTutorial();
	void DrawToolTip(URender* pRender, UFontPtr font,USkinPtr popo, const UPoint& pos, const WCHAR* pwChar);

	void ReadMovieFile(const char* filename);
	void ReadMovieFileByName(const char* MovieName);

	void ParseCommand(const char* command, const char* content);

	void BeginInput();
	void InputSay(int playIndex, const char* say);
	void InputList( int playIndex, UINT nKeyCode, float time = 5.0f, const char* tooltip = NULL);
	void InputListMouse( int playIndex, UINT nMoseCode, float time = 5.0f, const char* tooltip = NULL);
	void EndInput();

	void BeginPlay();
	void PlayNext();
	void EndPlay();
	void OnPlayEnd();
protected:
	int mPlayIndex;
	std::vector<FrameListItem> m_FrameList;
	std::vector<FrameSayItem> m_FrameSayList;
	bool mbJustSay;
};
struct winkBase
{
	winkBase():IsWink(false),WinkDelta(1.f),bToolTip(false){}

	bool IsWink;
	float WinkTime;
	float WinkDelta;
	float DeltaTime;

	bool bToolTip;
	WCHAR ToolTip[256];

	static int WinkCount;
};
struct sKeyButton : public winkBase
{
	sKeyButton():KeyIsDown(false),color(0xFFB28220),KeyTexture(NULL),KeyCode(LK_NULL){}
	void SetField(const char* Filedname, const char* Value);
	std::string KeyButtonName;
	UINT KeyCode;
	UTexturePtr KeyTexture;

	bool KeyIsDown;

	UPoint Pos;
	UPoint Size;
	UColor color;
};
class UKeyBoard : public UControl
{
	UDEC_CLASS(UKeyBoard);
public:
	UKeyBoard();
	~UKeyBoard();

	void KeyDown(UINT nKeyCode,UINT nFlags);
	void KeyUp(UINT nKeyCode,UINT nFlags);
	void WinkKey(UINT nKeyCode, float time = 5.0f, const char* tooltip = NULL);
	void ClearWink();

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect& updateRect);
	virtual void OnTimer(float fDeltaTime);

	BOOL ParseKeyBoardLayOut();
protected:
	UString m_strFileName;
	std::map<std::string , sKeyButton> m_KeyButtonMap;
	sKeyButton* mWinkKey[20];
};
struct sMouseButton : public winkBase
{
	sMouseButton():m_MouseCtrl(NULL){}
	UControl* m_MouseCtrl;
};
class UAMouse : public UControl
{
	UDEC_CLASS(UAMouse);
public:
	UAMouse();
	~UAMouse();

	void LBDown();
	void LBUp();
	void RBDown();
	void RBUp();
	void MouseWheel();
	void WinkKey(UINT nKeyCode, float time = 5.0f, const char* tooltip = NULL);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect& updateRect);
	virtual void OnTimer(float fDeltaTime);


	UBitmap* m_LeftButton;
	UBitmap* m_RightButton;
	UBitmap* m_MiddleButton;
	std::vector<sMouseButton> m_MouseButton;
	sMouseButton* mWinkKey[MOUSE_KEY_NONE];
};
class UIContentCtrl : public UControl
{
	UDEC_CLASS(UIContentCtrl);
public:
	UIContentCtrl();
	~UIContentCtrl();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect& updateRect);

	USkinPtr m_Popo;
};
class UITutorial : public UControl
{
	UDEC_CLASS(UITutorial);
	UDEC_MESSAGEMAP();
public:
	UITutorial();
	~UITutorial();

	void SetFrameSay(const char* say);
	inline UKeyBoard* GetKeyBoard(){return mpKeyBoard;};
	inline UAMouse* GetMouse(){return mpAMouse;};
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect& updateRect);
	virtual BOOL OnEscape();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnMiddleMouseDown(const UPoint& position, UINT flags);
	virtual void OnMiddleMouseUp(const UPoint& position, UINT flags);
	virtual BOOL OnMouseWheel(const UPoint& position, int delta , UINT flags);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
protected:
	void OnClickURL(UNM* pNM);

	UKeyBoard* mpKeyBoard;
	UAMouse* mpAMouse;
	UNiAVControlEff* mpNiAVctrlEff;
	URichTextCtrl* mpRichTextCtrl;
};
class UFreshManTip : public UDialog
{
	UDEC_CLASS(UFreshManTip);
	UDEC_MESSAGEMAP();
public:
	UFreshManTip();
	~UFreshManTip();
public:
	void AddTipMessage(std::string& str);
	void PraseTutorialFlag(ui8 flag[], UINT uiSize);
	void HelpEventTrigger(UINT EventIndex);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	void OnClickClose();
	void OnClickNextTip();
	void OnTipShowAgain();
private:
	UINT mEventIndex;
	UButton* m_CloseBtn;
	UButton* m_NextTipBtn;
	UStaticText* m_TitleText;
	BOOL m_IsTipShowAgain;
	URichTextCtrl* m_RichEdit;
	std::vector<std::string> m_TipQueue;
	ui8 m_Tutorialflag[8];
};
#endif