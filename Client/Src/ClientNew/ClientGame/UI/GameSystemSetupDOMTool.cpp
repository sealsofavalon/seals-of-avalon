#include "stdafx.h"
#include "GameSystemSetupDOMTool.h"

#if (_MSC_VER == 1500 ) //VC9.0
#ifdef _DEBUG
#pragma comment(lib, "TinyXml_vc90D.lib")
#else
#pragma comment(lib, "TinyXml_vc90.lib")
#endif
#elif (_MSC_VER == 1400) //VC8.0
#else
#error Unsupported version of Visual Studio
#endif

#define POSSIBLY_UNUSED
SystemSetDOMTool::SystemSetDOMTool()
{
	m_pkXMLDocument = NULL;
	m_pkCurrentElement = NULL;
}
SystemSetDOMTool::SystemSetDOMTool(const char* pcFileName)
{
	m_pkXMLDocument = NULL;
	m_pkCurrentElement = NULL;

	Init(pcFileName);
}
SystemSetDOMTool::~SystemSetDOMTool()
{
	Flush();
}
//
void SystemSetDOMTool::Init(const char* pcFileName)
{
	Flush();

	m_LoadFileName = pcFileName;
	m_pkXMLDocument = NiExternalNew TiXmlDocument(pcFileName);
}
void SystemSetDOMTool::Flush()
{
	if (m_pkXMLDocument)
		NiExternalDelete m_pkXMLDocument;

	m_pkXMLDocument = NULL;
	m_pkCurrentElement = NULL;
}
//
NiBool SystemSetDOMTool::LoadFile()
{
	NIASSERT(m_pkXMLDocument && m_LoadFileName.size());

    char* acBuffer = (char*)malloc(256*1024);
	ULOG(m_LoadFileName.c_str());
    NiFile* pkFile = NiFile::GetFile(m_LoadFileName.c_str(), NiFile::READ_ONLY);
    if(!pkFile || !(*pkFile))
    {
        NiDelete pkFile;
		free( acBuffer );
        return false;
    }

    unsigned int uiSize = pkFile->Read(acBuffer, 256*1024);
    NIASSERT(uiSize < 256*1024 - 1);

    acBuffer[uiSize] = '\0';
    NiDelete pkFile;

	m_pkXMLDocument->Parse(acBuffer);

	free( acBuffer );
	return true;
}

NiBool SystemSetDOMTool::SaveFile()
{
	NIASSERT(m_pkXMLDocument);
	return m_pkXMLDocument->SaveFile();
}

NiBool SystemSetDOMTool::SaveFile(const char* FileName)
{
	NIASSERT(m_pkXMLDocument);
	return m_pkXMLDocument->SaveFile(FileName);
}

//
TiXmlElement* SystemSetDOMTool::SetSectionToNextSibling()
{
	TiXmlElement* pkElement = GetCurrentSection();
	pkElement = pkElement->NextSiblingElement();
	m_pkCurrentElement = pkElement;
	return pkElement;
}
TiXmlElement* SystemSetDOMTool::SetSectionToFirstChild()
{
	TiXmlElement* pkElement = GetCurrentSection();
	pkElement = pkElement->FirstChildElement();
	m_pkCurrentElement = pkElement;
	return pkElement;
}
TiXmlElement* SystemSetDOMTool::SetSectionTo(const char* pcSection)
{
	if (!m_pkXMLDocument)
		return NULL;

	TiXmlElement* pkElement;

	if (!m_pkCurrentElement)
	{
		pkElement = m_pkXMLDocument->FirstChildElement(pcSection);  
		m_pkCurrentElement = pkElement;
	}
	else
	{
		pkElement = m_pkCurrentElement;
		pkElement = pkElement->FirstChildElement(pcSection);
		m_pkCurrentElement = pkElement;
	}
	return pkElement;
}
TiXmlElement* SystemSetDOMTool::ResetSectionTo(const char* pcSection)
{ 
	if (!m_pkXMLDocument)
		return NULL;

	m_pkCurrentElement = NULL;

	return SetSectionTo(pcSection);
}
TiXmlElement* SystemSetDOMTool::GetCurrentSection()
{
	//if (!m_pkCurrentElement)
	//	return m_pkXMLDocument->FirstChildElement();
	//else
	return m_pkCurrentElement;
}
NiBool SystemSetDOMTool::IsCurrentSectionValid()
{
	if (GetCurrentSection())
		return true;

	return false;
}
//
const char* SystemSetDOMTool::GetValueFromCurrent()
{
	TiXmlElement* pkElement = GetCurrentSection();
	NIASSERT(pkElement);
	return pkElement->Value();
}
//
TiXmlElement* SystemSetDOMTool::CreateRootSection(const char* SectionName)
{
	NIASSERT(m_pkXMLDocument);
	TiXmlElement * root = NiExternalNew TiXmlElement(SectionName);

	m_pkXMLDocument->LinkEndChild(root);
	return root;
}

TiXmlElement* SystemSetDOMTool::CreateSection(const char* SectionName)
{
	NIASSERT(m_pkXMLDocument);
	TiXmlElement* root = m_pkXMLDocument->RootElement();

	TiXmlElement * element = new TiXmlElement(SectionName);
	
	root->LinkEndChild(element);
	return element;
}

TiXmlElement* SystemSetDOMTool::PushElement(TiXmlElement* root, const char* Name, int iData)
{
	NIASSERT(root);
	TiXmlElement * element = new TiXmlElement(Name);
	root->LinkEndChild(element);

	char acBuf[256];
	NiSprintf(acBuf, 256, "%d", iData);
	TiXmlText* NameContent = NiExternalNew TiXmlText(acBuf);

	element->LinkEndChild(NameContent);
	return element;
}
TiXmlElement* SystemSetDOMTool::PushElement(TiXmlElement* root, const char* Name, bool iData)
{
	NIASSERT(root);
	TiXmlElement * element = new TiXmlElement(Name);
	root->LinkEndChild(element);
	TiXmlText *NameContent;
	if (iData)
		NameContent = NiExternalNew TiXmlText("TRUE");
	else
		NameContent = NiExternalNew TiXmlText("FALSE");

	element->LinkEndChild(NameContent);
	return element;
}
TiXmlElement* SystemSetDOMTool::PushElement(TiXmlElement* root, const char* Name, UPoint iData)
{
	NIASSERT(root);
	TiXmlElement * element = new TiXmlElement(Name);
	root->LinkEndChild(element);

	char acBuf[256];
	NiSprintf(acBuf, 256, "%d, %d", iData.x, iData.y);
	TiXmlText* NameContent = NiExternalNew TiXmlText(acBuf);

	element->LinkEndChild(NameContent);
	return element;
}

TiXmlElement* SystemSetDOMTool::PushElement(TiXmlElement* root, const char* Name, float iData)
{
	NIASSERT(root);
	TiXmlElement * element = new TiXmlElement(Name);
	root->LinkEndChild(element);

	char acBuf[256];
	NiSprintf(acBuf, 256, "%f", iData);
	TiXmlText* NameContent = NiExternalNew TiXmlText(acBuf);

	element->LinkEndChild(NameContent);
	return element;
}
//
NiBool SystemSetDOMTool::ReadPrimitive(bool& bData)
{
	TiXmlElement* pkElement = GetCurrentSection();

	const char* pcBool = pkElement->GetText();

	if (NiStricmp(pcBool,"TRUE") == 0)
	{
		bData = true;
	}
	else if (NiStricmp(pcBool,"FALSE") == 0)
	{
		bData = false;
	}
	else
	{
		return false;
	}

	return true;
}
NiBool SystemSetDOMTool::ReadPrimitive(int& iData)
{
	TiXmlElement* pkElement = GetCurrentSection();
	const char* pcInt = pkElement->GetText();
#if defined(_MSC_VER) && _MSC_VER >= 1400
	int POSSIBLY_UNUSED iFieldsAssigned = sscanf_s(pcInt, "%i", &iData);
#else //#if defined(_MSC_VER) && _MSC_VER >= 1400
	int POSSIBLY_UNUSED iFieldsAssigned = sscanf(pcInt, "%i", &iData);
#endif //#if defined(_MSC_VER) && _MSC_VER >= 1400
	if (iFieldsAssigned != 1)
	{
		return false;
	}
	return true;
}
NiBool SystemSetDOMTool::ReadPrimitive(float& fData)
{
	TiXmlElement* pkElement = GetCurrentSection();
	const char* pcInt = pkElement->GetText();
#if defined(_MSC_VER) && _MSC_VER >= 1400
	int POSSIBLY_UNUSED iFieldsAssigned = sscanf_s(pcInt, "%f", &fData);
#else //#if defined(_MSC_VER) && _MSC_VER >= 1400
	int POSSIBLY_UNUSED iFieldsAssigned = sscanf(pcInt, "%f", &fData);
#endif //#if defined(_MSC_VER) && _MSC_VER >= 1400
	if (iFieldsAssigned != 1)
	{
		return false;
	}
	return true;
}
NiBool SystemSetDOMTool::ReadPrimitive(UPoint& kData)
{
	TiXmlElement* pkElement = GetCurrentSection();
	const char* pcRow = pkElement->GetText();
#if defined(_MSC_VER) && _MSC_VER >= 1400
	int POSSIBLY_UNUSED iFieldsAssigned =
		sscanf_s(pcRow, "%d, %d", &kData.x, &kData.y);
#else //#if defined(_MSC_VER) && _MSC_VER >= 1400
	int POSSIBLY_UNUSED iFieldsAssigned =
		sscanf(pcRow, "%d, %d", &kData.x, &kData.y);
#endif //#if defined(_MSC_VER) && _MSC_VER >= 1400
	if (iFieldsAssigned != 2)
	{
		return false;
	}
	return true;
}