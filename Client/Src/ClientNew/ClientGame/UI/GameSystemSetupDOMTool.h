#ifndef __GAMESYSTEMSETUPDOMTOOL_H__
#define __GAMESYSTEMSETUPDOMTOOL_H__

#include "../../../TinyXML/TinyXML.h"

class SystemSetDOMTool
{
public:
	SystemSetDOMTool();
	SystemSetDOMTool(const char* pcFileName);
	~SystemSetDOMTool();
	//
	void Init(const char* pcFileName);
	void Flush();
	//
	NiBool LoadFile();
	NiBool SaveFile();
	NiBool SaveFile(const char* FileName);
	//
	TiXmlElement* SetSectionToNextSibling();
	TiXmlElement* SetSectionToFirstChild();
	TiXmlElement* SetSectionTo(const char* pcSection);
	TiXmlElement* ResetSectionTo(const char* pcSection);
	TiXmlElement* GetCurrentSection();
	NiBool IsCurrentSectionValid();
	//
	const char* GetValueFromCurrent();
	//
	TiXmlElement* CreateRootSection(const char* SectionName);
	TiXmlElement* CreateSection(const char* SectionName);
	TiXmlElement* PushElement(TiXmlElement* root, const char* Name, int iData);
	TiXmlElement* PushElement(TiXmlElement* root, const char* Name, bool iData);
	TiXmlElement* PushElement(TiXmlElement* root, const char* Name, UPoint iData);
	TiXmlElement* PushElement(TiXmlElement* root, const char* Name, float iData);
	//
	NiBool ReadPrimitive(bool& bData);
	NiBool ReadPrimitive(int& iData);
	NiBool ReadPrimitive(UPoint& kData);
	NiBool ReadPrimitive(float& fData);
protected:
	TiXmlDocument* m_pkXMLDocument;
	TiXmlElement* m_pkCurrentElement;
	std::string m_LoadFileName;
};
#endif