#include "stdafx.h"
#include "GameSystemSetupLoad.h"
#include "SystemSetup.h"
#include "GameSystemSetupDOMTool.h"
#include "UChat.h"
#include "Hook/HookManager.h"

GameSystemSetupLoad::GameSystemSetupLoad()
{
	bLoadingEnd = true;
}
GameSystemSetupLoad::~GameSystemSetupLoad()
{

}
BOOL LoadUserKeyMap(const char* FileName, BOOL bCover)
{
	if (!UControl::sm_System->LoadAcceleratorKey(FileName, bCover?true:false))
	{
		return FALSE;
	}
	return TRUE;
}
bool GameSystemSetupLoad::LoadUserSetupFile()
{
	bLoadingEnd = false;
	std::string FileDirectory;
	SystemSetup->GetGameSystemUserFileRoot(FileDirectory);

	//LoadUserSetting
	std::string UserSetFile = FileDirectory + "UserSetting.xml";
	SystemSetDOMTool kCDOM;
	kCDOM.Init(UserSetFile.c_str());
	if (kCDOM.LoadFile())
	{
		ULOG("用户设置文件打开成功");
		SystemSetup->SetSystemSettingFormDOM("System", kCDOM);
		SystemSetup->SetSystemSettingFormDOM("Audio", kCDOM);
		SystemSetup->SetSystemSettingFormDOM("Video", kCDOM);
		bLoadingEnd = true;
	}
	else
	{
		bLoadingEnd = true;
		SystemSetup->SaveFile();
		SystemSetup->SetSystemSettingFormSection("System");
		SystemSetup->SetSystemSettingFormSection("Audio");
		SystemSetup->SetSystemSettingFormSection("Video");
	}
	return true;
}

bool GameSystemSetupLoad::LoadUserKeyMapFile()
{
	std::string FileDirectory;
	SystemSetup->GetGameSystemUserFileRoot(FileDirectory);
	//LoadKeyMap
	std::string KeyMapFile = FileDirectory + "UserKeyMap.ackey";
	if (LoadUserKeyMap("DATA\\UI\\DefaultSetting\\DefaultKeyMap.ackey", FALSE))
	{
		ULOG("默认快捷键列表读取完毕！");
		if (!LoadUserKeyMap(KeyMapFile.c_str(), TRUE))
		{
			ULOG("用户自定义列表读取失败，开始创建");
			return UControl::sm_System->SaveAcceleratorKey(KeyMapFile.c_str())?true:false;
		}
		ULOG("读入用户自定义快捷键列表");
	}
	return true;
}

bool GameSystemSetupLoad::LoadLastRollInfo(std::string &name)
{
	std::string FileStr;
	if (SystemSetup->GetGameSystemAccountFile(FileStr))
	{
		FILE* file = fopen(FileStr.c_str(), "r");
		if (file)
		{
			char acBuffer[_MAX_PATH];
			int len;
			fread(&len, 1, sizeof(int), file);
			fread(acBuffer, 1, len, file);
			acBuffer[len] = '\0';
			fclose(file);

			name = acBuffer ;
			return true ;
		}
		return false;

	}
	return false;
}
bool GameSystemSetupLoad::LoadChatSetup()
{
	std::string FileDirectory;
	SystemSetup->GetGameSystemUserFileRoot(FileDirectory);
	std::string ChatSettingFile = FileDirectory + "UserChatSetting.xml";
	SystemSetDOMTool kCDOM;
	kCDOM.Init(ChatSettingFile.c_str());
	if (kCDOM.LoadFile())
	{
		ULOG("聊天设置打开成功");
		if (!kCDOM.ResetSectionTo("ChatSetting"))
			return false;
		if (!kCDOM.SetSectionTo("ChatChannelModify"))
			return false;
		kCDOM.SetSectionToFirstChild();
		while( kCDOM.IsCurrentSectionValid() )
		{
			std::string sValue = kCDOM.GetValueFromCurrent();


			int tempint;
			kCDOM.ReadPrimitive(tempint);

			int iPos = sValue.find_last_of('_');
			int istore = atoi(sValue.substr(iPos + 1, sValue.size()).c_str());

			UChat::MsgStore* pStore = ChatSystem->GetMsgStore(istore);
			pStore->VisModify = tempint;

			kCDOM.SetSectionToNextSibling();
		}
	}
	else
	{
		ChatSystem->SetChatSettingDefault();
		SystemSetup->SaveChatSetting();
	}
	return true;
}
bool GameSystemSetupLoad::LoadUserHookSetFile()// 读取HOOK 设定
{
	std::string FileDirectory;
	SystemSetup->GetGameSystemUserFileRoot(FileDirectory);
	std::string ChatSettingFile = FileDirectory + "UserHook.xml";
	SystemSetDOMTool kCDOM;
	HookData pkHookData;

	kCDOM.Init(ChatSettingFile.c_str());
	if (kCDOM.LoadFile())
	{
		if (!kCDOM.ResetSectionTo("HOOK"))
			return false;
		if (!kCDOM.SetSectionTo("HOOKSET"))
			return false;
		kCDOM.SetSectionToFirstChild();

		
		bool BData;
		float FData;
		int  IData;

		while(kCDOM.IsCurrentSectionValid())
		{
			const char* name  = kCDOM.GetValueFromCurrent();
			if (strcmp(name, "AutoAttack") == 0)
			{
				kCDOM.ReadPrimitive(BData);
				pkHookData.AutoAttack = BData;
			}
			if (strcmp(name, "AutoSupply") == 0)
			{
				kCDOM.ReadPrimitive(BData);
				pkHookData.AutoSupply = BData;
			}
			if (strcmp(name, "AutoPick") == 0)
			{
				kCDOM.ReadPrimitive(BData);
				pkHookData.AutoPick = BData;
			}
			if (strcmp(name, "AutoShunBoss") == 0)
			{
				kCDOM.ReadPrimitive(BData);
				pkHookData.AutoShunBoss = BData ;
			}
			if (strcmp(name, "DeadAutoPos") == 0)
			{
				kCDOM.ReadPrimitive(BData);
				pkHookData.DeadAutoPos = BData;
			}
			if (strcmp(name, "UseSpellFirst") == 0)
			{
				kCDOM.ReadPrimitive(BData);
				pkHookData.UseSpellFirst = BData;
			}
			if (strcmp(name, "Spell0") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.Spell[0] = IData ;
			}
			if (strcmp(name, "Spell1") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.Spell[1] = IData ;
			}
			if (strcmp(name, "Spell2") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.Spell[2] = IData ;
			}
			if (strcmp(name, "Spell3") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.Spell[3] = IData ;
			}
			if (strcmp(name, "Spell4") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.Spell[4] = IData ;
			}
			if (strcmp(name, "HPSpell") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.HPSpell = IData ;
			}
			if (strcmp(name, "MPSpell") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.MPSpell = IData;
			}
			if (strcmp(name, "HPItem0") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.HPEntry[0] = IData;
			}
			if (strcmp(name, "HPItem1") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.HPEntry[1] = IData;
			}
			if (strcmp(name, "HPItem2") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.HPEntry[2] = IData;
			}
			if (strcmp(name, "MPItem0") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.MPEntry[0] = IData;
			}
			if (strcmp(name, "MPItem1") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.MPEntry[1] = IData;
			}
			if (strcmp(name, "MPItem2") == 0)
			{
				kCDOM.ReadPrimitive(IData);
				pkHookData.MPEntry[2] = IData;
			}
			if (strcmp(name, "HP") == 0)
			{
				kCDOM.ReadPrimitive(FData);
				pkHookData.Hp = FData;
			}
			if (strcmp(name, "MP") == 0)
			{
				kCDOM.ReadPrimitive(FData);
				pkHookData.Mp = FData;
			}
			kCDOM.SetSectionToNextSibling();
		}

		if (HookMgr)
		{
			HookMgr->InitUIHookData(pkHookData);
		}
		return true;
	}
	return false;

}
bool GameSystemSetupLoad::LoadDefaultSetting(const char* Section)
{
	std::string KeyDefaultFilePath = "DATA\\UI\\DefaultSetting\\DefaultKeyMap.ackey";
	std::string SystemDefaultFilePath = "DATA\\UI\\DefaultSetting\\DefaultUserSetting.xml";
	if(strcmp(Section, "key") == 0)
		LoadUserKeyMap(KeyDefaultFilePath.c_str(), FALSE);
	SystemSetDOMTool kCDOM;
	kCDOM.Init(SystemDefaultFilePath.c_str());

	if (kCDOM.LoadFile())
	{
		if(strcmp(Section, "system") == 0)
			SystemSetup->SetSystemSettingFormDOM("System", kCDOM, false);
		if(strcmp(Section, "audio") == 0)
			SystemSetup->SetSystemSettingFormDOM("Audio", kCDOM, false);
		if(strcmp(Section, "video") == 0)
			SystemSetup->SetSystemSettingFormDOM("Video", kCDOM, false);
	}
	return true;
}