#ifndef __GAMESYSTEMSETUPLOAD_H__
#define __GAMESYSTEMSETUPLOAD_H__

#include "GameSystemSetupStruct.h"

class GameSystemSetupLoad
{
public:
	GameSystemSetupLoad();
	~GameSystemSetupLoad();
	
	bool bLoadingEnd;
public:
	bool LoadUserSetupFile();//读取用户配置脚本
	bool LoadUserKeyMapFile();//读取用户快捷配置脚本
	bool LoadDefaultSetting(const char* Section);//读取用户默认配置脚本
	bool LoadLastRollInfo(std::string &name); //读取最后退出角色的信息
	bool LoadChatSetup();//读取用户聊天设置
	bool LoadUserHookSetFile();// 读取HOOK 设定
};

#endif