#include "stdafx.h"
#include "GameSystemSetupMgr.h"
#include "SystemSetup.h"
#include "UFun.h"
#include "USystemSet.h"

#include "../../../../new_common/Source/ssl/crc32.h"
GameSystemSetupManage::GameSystemSetupManage()
{

}

GameSystemSetupManage::~GameSystemSetupManage()
{

}

void GameSystemSetupManage::SetGameSystemUserFileRoot(int Partypath, const char* path)
{
	char buf[256];
	ui32 crc  =  0;
	switch(Partypath)
	{
	case PATH_ACCOUNTNAME:
		mAccountName = path;
		break;
	case PATH_SERVERNAME: 
		crc = ssl::__crc32((const unsigned char *)path, strlen(path));
		sprintf(buf, "%u", crc);
		mServerName = buf;
		break;
	case PATH_ROLENAME:
		crc = ssl::__crc32((const unsigned char *)path, strlen(path));
		sprintf(buf, "%u", crc);
		mRoleName = buf;
		break;
	};
}

bool GameSystemSetupManage::GetGameSystemUserFileRoot(std::string &str)
{	
	if (mAccountName.size() && mServerName.size() && mRoleName.size())
	{
		str =  "Account\\" + mAccountName + "\\" + mServerName + "\\" + mRoleName + "\\";
		return true;
	}
	return false;
}
bool GameSystemSetupManage::GetGameSystemAccountFile(std::string &str)
{
	if (mAccountName.size() && mServerName.size())
	{
		str =  "Account\\" + mAccountName + "\\" + mServerName + "\\" + "LastRoll";
		ULOG(str.c_str());
		return true;
	}
	return false;
}
BOOL GameSystemSetupManage::CreateUserDirectory()
{
	BOOL bProcess = TRUE;
	std::string FileDirectory = "Account";
	bProcess &= ::CreateDirectory(FileDirectory.c_str(), NULL);
	FileDirectory += "\\" + mAccountName;
	bProcess &= ::CreateDirectory(FileDirectory.c_str(), NULL);
	FileDirectory += "\\" + mServerName;
	bProcess &= ::CreateDirectory(FileDirectory.c_str(), NULL);
	FileDirectory += "\\" + mRoleName;
	bProcess &= ::CreateDirectory(FileDirectory.c_str(), NULL);
	return bProcess;
}
BOOL GameSystemSetupManage::RemoveUserDirectory(const char* UserName)
{
	std::string FileDirectory = "Account";
	FileDirectory += "\\" + mAccountName + "\\" + mServerName + "\\";
	FileDirectory += UserName;
	::DeleteFile((FileDirectory + "\\UserSetting.xml").c_str());
	::DeleteFile((FileDirectory + "\\UserKeyMap.ackey").c_str());
	return ::RemoveDirectory(FileDirectory.c_str());
}

void GameSystemSetupManage::SetField(const char* OpCode, bool Choose)
{
	SetupMap::iterator it = m_SetupMap.find(OpCode);
	if (it != m_SetupMap.end())
	{
		Setup_structbool* pksetbool = (Setup_structbool*)it->second;
		if (pksetbool)
		{
			pksetbool->Date = Choose;
			pksetbool->bChange = TRUE;
		}
	}
}

void GameSystemSetupManage::SetField(const char* OpCode, int date)
{
	SetupMap::iterator it = m_SetupMap.find(OpCode);
	if (it != m_SetupMap.end())
	{
		Setup_structint* pksetint = (Setup_structint*)it->second;
		if (pksetint)
		{
			pksetint->Date = date;
			pksetint->bChange = TRUE;
		}
	}
}

void GameSystemSetupManage::SetField(const char* OpCode, UPoint date)
{
	SetupMap::iterator it = m_SetupMap.find(OpCode);
	if (it != m_SetupMap.end())
	{
		Setup_structPoint* pksetpoint = (Setup_structPoint*)it->second;
		if (pksetpoint)
		{
			pksetpoint->Date = date;
			pksetpoint->bChange = TRUE;
		}
	}
}
void GameSystemSetupManage::SetField(const char* OpCode, float date)
{
	SetupMap::iterator it = m_SetupMap.find(OpCode);
	if (it != m_SetupMap.end())
	{
		Setup_structfloat* pksetpoint = (Setup_structfloat*)it->second;
		if (pksetpoint)
		{
			pksetpoint->Date = date;
			pksetpoint->bChange = TRUE;
		}
	}
}

void GameSystemSetupManage::SetDefaultSetup()
{
	SetSetupCell("System", _TRAN("NPC Name"), "NPCName", true, &ShowNPCName);
	SetSetupCell("System", _TRAN("Player Name"), "PlayerName", true, &ShowPlayerName);
	SetSetupCell("System", _TRAN("Talk Popo"), "TalkPopo", true, &ChatPopoShow);
	SetSetupCell("System", _TRAN("Target Target"), "TargetTarget", true, &TargetTargetShow);
	SetSetupCell("System", _TRAN("Host Hp Bar"), "HostHpBar", false, &ShowHostHpBar);
	SetSetupCell("System", _TRAN("HPBarColorFul"), "HPBarColorFul", false, &ShowHpBarColorful);
	SetSetupCell("System", _TRAN("AddActionBar1"), "AddActionBar1", false, &ActionBar1);

	SetSetupCell("System", _TRAN("AddActionBar2"), "AddActionBar2", false, &ActionBar2);
	SetSetupCell("System", "ActionBar2TranType", "ActionBar2Type", 0, &ActionBar2Type);
	SetSetupCell("System", "ActionBar2Pos", "ActionBar2Pos", UPoint(400, 597), &ActionBar2Pos);
	SetSetupCell("System", "ActionBar2Lock", "AcitonBar2Lock", false, &ActionBar2Lock);

	SetSetupCell("System", "ActionBar3", "AddActionBar3", false, &ActionBar3);
	SetSetupCell("System", "ActionBar3TranType", "ActionBar3Type", 0, &ActionBar3Type);
	SetSetupCell("System", "ActionBar3Pos", "ActionBar3Pos", UPoint(400, 551), &ActionBar3Pos);
	SetSetupCell("System", "ActionBar3Lock", "AcitonBar3Lock", false, &ActionBar3Lock);

	SetSetupCell("System", "ActionBar4", "AddActionBar4", false, &ActionBar4);
	SetSetupCell("System", "ActionBar4TranType", "ActionBar4Type", 0, &ActionBar4Type);
	SetSetupCell("System", "ActionBar4Pos", "ActionBar4Pos", UPoint(400, 505), &ActionBar4Pos);
	SetSetupCell("System", "ActionBar4Lock", "AcitonBar4Lock", false, &ActionBar4Lock);

	SetSetupCell("System", "ActionBar5", "AddActionBar5", false, &ActionBar5);
	SetSetupCell("System", "ActionBar5TranType", "ActionBar5Type", 0, &ActionBar5Type);
	SetSetupCell("System", "ActionBar5Pos", "ActionBar5Pos", UPoint(400, 459), &ActionBar5Pos);
	SetSetupCell("System", "ActionBar5Lock", "AcitonBar5Lock", false, &ActionBar5Lock);

	SetSetupCell("System", "ActionBar6", "AddActionBar6", false, &ActionBar6);
	SetSetupCell("System", "ActionBar6TranType", "ActionBar6Type", 0, &ActionBar6Type);
	SetSetupCell("System", "ActionBar6Pos", "ActionBar6Pos", UPoint(400, 410), &ActionBar6Pos);
	SetSetupCell("System", "ActionBar6Lock", "AcitonBar6Lock", false, &ActionBar6Lock);

	SetSetupCell("System", "ActionBar7", "AddActionBar7", false, &ActionBar7);
	SetSetupCell("System", "ActionBar7TranType", "ActionBar7Type", 0, &ActionBar7Type);
	SetSetupCell("System", "ActionBar7Pos", "ActionBar7Pos", UPoint(400, 364), &ActionBar7Pos);
	SetSetupCell("System", "ActionBar7Lock", "AcitonBar7Lock", false, &ActionBar7Lock);

	SetSetupCell("System", _TRAN("LockCamera"), "LockCamera", false, &LockCamera);
	SetSetupCell("System", _TRAN("MiniMapNPC"), "MiniMapNPC", true, &MiniMapShowNPC);
	SetSetupCell("System", _TRAN("MiniMapMonster"), "MiniMapMonster", true, &MiniMapShowMonster);
	SetSetupCell("System", _TRAN("MiniMapPlayer"), "MiniMapPlayer", true, &MiniMapShowPlayer);
	SetSetupCell("System", _TRAN("ReCount"), "ReCount", false, &Plug_ReCount);
	//SetSetupCell("System", _TRAN("队友战斗信息", "TeamMemBattelMsg", true, NULL);
	//SetSetupCell("System", _TRAN("目标战斗信息", "TargetBattelMsg", true, NULL);
	SetSetupCell("System", _TRAN("MouseMove"), "MouseMove", false, &SetMouseMove);
	SetSetupCell("System", _TRAN("AutoPick"), "AutoPick", false, &AutoPickItem);
	SetSetupCell("System", _TRAN("ChatFileter"), "ChatFileter", true, &SetFilterChatMsg);
	SetSetupCell("System", _TRAN("Head3DShow"), "Head3DShow", false, &Show3DHead);
	//SetSetupCell("System", "自动调整施法距离", "CCastDist", false, &CCastDist);
	SetSetupCell("System", _TRAN("CameraFollow"), "CameraFollow", true, &CameraFollow);
	SetSetupCell("System", _TRAN("LockActionBar"), "LockActionBar", false, &LockActionBar);
	SetSetupCell("System", _TRAN("MouseSemsitivity"), "MouseSemsitivity",0.5f, &ChangeMouseSemsitivity);
	SetSetupCell("System", _TRAN("CameraFollowSpeed"), "CameraFollowSpeed",0.4f, &ChangeCamearFollowSpeed);
	SetSetupCell("System", _TRAN("CameraDis"), "CameraDis",0.0f, &ChangeCameardistance);
    SetSetupCell("Audio", _TRAN("EnableMusic"), "EnableMusic", true, &EnableMusic);
    SetSetupCell("Audio", _TRAN("EnableSound"), "EnableSound", true, &EnableSound);
	SetSetupCell("Audio", _TRAN("Enable3DSound"), "Enable3DSound", true, &Enable3DSound);
	SetSetupCell("Audio", _TRAN("MusicVolum"), "MusicVolum", 0.3f, &ChangeMusicVolum);
	SetSetupCell("Audio", _TRAN("SoundVolum"), "SoundVolum", 0.3f, &ChangeSoundVolum);
	SetSetupCell("Audio", _TRAN("EnvironSoundVolum"), "EnvironSoundVolum", 0.3f, &Change3DSoundVolum);
}

void GameSystemSetupManage::SetupSetting()
{
	SetupMap::iterator it;
	for (it = m_SetupMap.begin();it != m_SetupMap.end(); ++it)
	{
		if (it->second->bChange)
		{
			it->second->RUN();
			it->second->bChange = FALSE;
			SetupSettingToCtrl(it->second);
		}
	}
}

void GameSystemSetupManage::SetupSettingToCtrl(struct Setup_struct* pSetupstruct)
{
	SetupCtrl::iterator itCtrl = m_SetupCtrl.find(pSetupstruct->OpCode);
	switch(pSetupstruct->type)
	{
	case GOT_BOOLTYPE:
		{
			Setup_structbool * pkBoolType = (Setup_structbool* )pSetupstruct;
			if (itCtrl != m_SetupCtrl.end())
			{
				USetItem* pSetItem = UDynamicCast(USetItem, itCtrl->second);
				pSetItem->SetChoose(pkBoolType->Date);
			}
		}
		break;
	case GOT_INTTYPE:
		{
			Setup_structint * pkIntType = (Setup_structint* )pSetupstruct;
		}
		break;
	case GOT_POINTTYPE:
		{
			Setup_structPoint * pkPointType = (Setup_structPoint* )pSetupstruct;
		}
		break;
	case GOT_FLOATTYPE:
		{
			Setup_structfloat * pkFloatType = (Setup_structfloat* )pSetupstruct;
			if (itCtrl != m_SetupCtrl.end())
			{
				USetSlider* pSetItem = UDynamicCast(USetSlider, itCtrl->second);
				pSetItem->SetBarPosition(pkFloatType->Date);
			}
		}
	}
}

void GameSystemSetupManage::SetupSettingBySection(const char* Section)
{
	SetupMap::iterator it;
	for (it = m_SetupMap.begin();it != m_SetupMap.end(); ++it)
	{
		if ( it->second->RootSection.compare(Section)  != 0 )
		{
			continue;
		}
		it->second->RUN();
	}
}

void GameSystemSetupManage::SetupSettingByDOM(const char* OpCode, SystemSetDOMTool &Dom, bool bRun)
{
	SetupMap::iterator it = m_SetupMap.find(OpCode);
	SetupCtrl::iterator itCtrl = m_SetupCtrl.find(OpCode);
	if (it != m_SetupMap.end())
	{
		switch(it->second->type)
		{
		case GOT_BOOLTYPE:
			{
				bool tempbool;
				Dom.ReadPrimitive(tempbool);
				it->second->VALUE(&tempbool);
				if (itCtrl != m_SetupCtrl.end())
				{
					USetItem* pSetItem = UDynamicCast(USetItem, itCtrl->second);
					if (pSetItem)
					{
						pSetItem->SetChoose(tempbool);
					}
				}
			}
			break;
		case GOT_INTTYPE:
			{
				int tempint;
				Dom.ReadPrimitive(tempint);
				it->second->VALUE(&tempint);
			}
			break;
		case GOT_POINTTYPE:
			{
				UPoint tempPoint;
				Dom.ReadPrimitive(tempPoint);
				it->second->VALUE(&tempPoint);
			}
			break;
		case GOT_FLOATTYPE:
			{
				float tempfloat;
				Dom.ReadPrimitive(tempfloat);
				it->second->VALUE(&tempfloat);
				if (itCtrl != m_SetupCtrl.end())
				{
					USetSlider* pSetItem = UDynamicCast(USetSlider, itCtrl->second);
					if (pSetItem)
					{
						pSetItem->SetBarPosition(tempfloat);
					}
				}
			}
			break;
		};
		if (bRun)
			it->second->RUN();
	}
}

void GameSystemSetupManage::AbandonSetting()
{
	SetupMap::iterator it;
	for (it = m_SetupMap.begin();it != m_SetupMap.end(); ++it)
	{
		if (it->second->bChange)
		{
			it->second->UNDO();
			SetupCtrl::iterator it2 = m_SetupCtrl.find(it->first.c_str());
			switch(it->second->type)
			{
			case GOT_BOOLTYPE:
				{
					Setup_structbool * pkBoolType = (Setup_structbool* )it->second;
					if (it2 != m_SetupCtrl.end())
					{
						USetItem* pSetItem = UDynamicCast(USetItem, it2->second);
						pSetItem->SetChoose(pkBoolType->Date);
					}
				}
				break;
			case GOT_INTTYPE:
				{
					Setup_structint * pkIntType = (Setup_structint* )it->second;
				}
				break;
			case GOT_POINTTYPE:
				{
					Setup_structPoint * pkPointType = (Setup_structPoint* )it->second;
				}
				break;
			case GOT_FLOATTYPE:
				{
					Setup_structfloat * pkFloatType = (Setup_structfloat* )it->second;
					if (it2 != m_SetupCtrl.end())
					{
						USetSlider* pSetItem = UDynamicCast(USetSlider, it2->second);
						pSetItem->SetBarPosition(pkFloatType->Date);
					}
				}
			};
		}
		it->second->bChange = FALSE;
	}
}

void GameSystemSetupManage::SetSetupCell(const char* RootSection, const char* RealName, const char* OpCode, bool bChoose, ptrtypebool fun )
{
	SetupMap::iterator it = m_SetupMap.find(OpCode);
	if (it != m_SetupMap.end())
	{
		Setup_structbool* temp = (Setup_structbool*)it->second;
		if (temp)
		{
			temp->OpCode = OpCode;
			temp->RealName = RealName;
			temp->Date = bChoose;
			//temp->Undo = bChoose;
			temp->fun = fun;
			temp->RootSection = RootSection;
		}
	}
	else
	{
		Setup_structbool* temp = new Setup_structbool;
		temp->OpCode = OpCode;
		temp->RealName = RealName;
		temp->Date = bChoose;
		temp->Undo = bChoose;
		temp->fun = fun;
		temp->RootSection = RootSection;
		m_SetupMap.insert(SetupMap::value_type(OpCode, temp));
	}
}

void GameSystemSetupManage::SetSetupCell(const char* RootSection, const char* RealName, const char* OpCode, int date, ptrtypeint fun )
{
	SetupMap::iterator it = m_SetupMap.find(OpCode);
	if (it != m_SetupMap.end())
	{
		Setup_structint* temp = (Setup_structint*)it->second;
		if (temp)
		{
			temp->OpCode = OpCode;
			temp->RealName = RealName;
			temp->Date = date;
			//temp->Undo = date;
			temp->fun = fun;
			temp->RootSection = RootSection;
		}
	}
	else
	{
		Setup_structint* temp = new Setup_structint;
		temp->OpCode = OpCode;
		temp->RealName = RealName;
		temp->Date = date;
		temp->Undo = date;
		temp->fun = fun;
		temp->RootSection = RootSection;
		m_SetupMap.insert(SetupMap::value_type(OpCode, temp));
	}
}

void GameSystemSetupManage::SetSetupCell(const char* RootSection, const char* RealName, const char* OpCode, UPoint date, ptrtypePoint fun )
{
	SetupMap::iterator it = m_SetupMap.find(OpCode);
	if (it != m_SetupMap.end())
	{
		Setup_structPoint* temp = (Setup_structPoint*)it->second;
		if (temp)
		{
			temp->OpCode = OpCode;
			temp->RealName = RealName;
			temp->Date = date;
			//temp->Undo = date;
			temp->fun = fun;
			temp->RootSection = RootSection;
		}
	}
	else
	{
		Setup_structPoint* temp = new Setup_structPoint;
		temp->OpCode = OpCode;
		temp->RealName = RealName;
		temp->Date = date;
		temp->Undo = date;
		temp->fun = fun;
		temp->RootSection = RootSection;
		m_SetupMap.insert(SetupMap::value_type(OpCode, temp));
	}
}

void GameSystemSetupManage::SetSetupCell(const char* RootSection, const char* RealName, const char* OpCode, float date, ptrtypeFloat fun )
{
	SetupMap::iterator it = m_SetupMap.find(OpCode);
	if (it != m_SetupMap.end())
	{
		Setup_structfloat* temp = (Setup_structfloat*)it->second;
		if (temp)
		{
			temp->OpCode = OpCode;
			temp->RealName = RealName;
			temp->Date = date;
			//temp->Undo = date;
			temp->fun = fun;
			temp->RootSection = RootSection;
		}
	}
	else
	{
		Setup_structfloat* temp = new Setup_structfloat;
		temp->OpCode = OpCode;
		temp->RealName = RealName;
		temp->Date = date;
		temp->Undo = date;
		temp->fun = fun;
		temp->RootSection = RootSection;
		m_SetupMap.insert(SetupMap::value_type(OpCode, temp));
	}

}

BOOL GameSystemSetupManage::GetTextFormOpCode(const char* OpCode, std::string &str)
{
	SetupMap::iterator it = m_SetupMap.find(OpCode);
	if (it != m_SetupMap.end())
	{
		str = it->second->RealName;
		return TRUE;
	}
	return FALSE;
}

void GameSystemSetupManage::RegiestCtrl(const char* OpCode, UControl* ctrl)
{	
	SetupCtrl::iterator it = m_SetupCtrl.find(OpCode);
	if (it != m_SetupCtrl.end())
	{
		return;
	}
	m_SetupCtrl.insert(SetupCtrl::value_type(OpCode, ctrl));
}

void GameSystemSetupManage::OnLinkOpCode(const char* OpCode, bool LinkData)
{
	if(strcmp(OpCode, "HostHpBar") == 0)
	{
		USetItem* pSetItem = UDynamicCast(USetItem, GetControlWithOpCode("HPBarColorFul"));
		if (!pSetItem)
			return;
		pSetItem->SetCanUse(LinkData);
	}
	if (strcmp(OpCode, "LockCamera") == 0)
	{
	}
	if (strcmp(OpCode, "AddActionBar2") == 0)
	{
		Setup_structint* pint = (Setup_structint*)SystemSetup->GetStructWithOpCode("ActionBar2Type");
		if (pint)
		{
			SystemSetup->SetField("ActionBar2Type", pint->Date);
		}
		Setup_structPoint* pPoint = (Setup_structPoint*)SystemSetup->GetStructWithOpCode("ActionBar2Pos");
		if (pPoint)
		{
			SystemSetup->SetField("ActionBar2Pos", pPoint->Date);
		}
		Setup_structbool* PBool = (Setup_structbool*)SystemSetup->GetStructWithOpCode("AcitonBar2Lock");
		if (PBool)
		{
			SystemSetup->SetField("AcitonBar2Lock", PBool->Date);
		}
	}
	if (strcmp(OpCode, "AddActionBar3") == 0)
	{
		Setup_structint* pint = (Setup_structint*)SystemSetup->GetStructWithOpCode("ActionBar3Type");
		if (pint)
		{
			SystemSetup->SetField("ActionBar3Type", pint->Date);
		}
		Setup_structPoint* pPoint = (Setup_structPoint*)SystemSetup->GetStructWithOpCode("ActionBar3Pos");
		if (pPoint)
		{
			SystemSetup->SetField("ActionBar3Pos", pPoint->Date);
		}
		Setup_structbool* PBool = (Setup_structbool*)SystemSetup->GetStructWithOpCode("AcitonBar3Lock");
		if (PBool)
		{
			SystemSetup->SetField("AcitonBar3Lock", PBool->Date);
		}
	}
	if (strcmp(OpCode, "AddActionBar4") == 0)
	{
		Setup_structint* pint = (Setup_structint*)SystemSetup->GetStructWithOpCode("ActionBar4Type");
		if (pint)
		{
			SystemSetup->SetField("ActionBar4Type", pint->Date);
		}
		Setup_structPoint* pPoint = (Setup_structPoint*)SystemSetup->GetStructWithOpCode("ActionBar4Pos");
		if (pPoint)
		{
			SystemSetup->SetField("ActionBar4Pos", pPoint->Date);
		}
		Setup_structbool* PBool = (Setup_structbool*)SystemSetup->GetStructWithOpCode("AcitonBar4Lock");
		if (PBool)
		{
			SystemSetup->SetField("AcitonBar4Lock", PBool->Date);
		}
	}
	if (strcmp(OpCode, "AddActionBar5") == 0)
	{
		Setup_structint* pint = (Setup_structint*)SystemSetup->GetStructWithOpCode("ActionBar5Type");
		if (pint)
		{
			SystemSetup->SetField("ActionBar5Type", pint->Date);
		}
		Setup_structPoint* pPoint = (Setup_structPoint*)SystemSetup->GetStructWithOpCode("ActionBar5Pos");
		if (pPoint)
		{
			SystemSetup->SetField("ActionBar5Pos", pPoint->Date);
		}
		Setup_structbool* PBool = (Setup_structbool*)SystemSetup->GetStructWithOpCode("AcitonBar5Lock");
		if (PBool)
		{
			SystemSetup->SetField("AcitonBar5Lock", PBool->Date);
		}
	}
}

bool GameSystemSetupManage::GetStructWithSection(const char* section, vector<Setup_struct*>& vec)
{
	SetupMap::iterator it;
	for (it = m_SetupMap.begin();it != m_SetupMap.end(); ++it)
	{
		if ( it->second->RootSection.compare(section) != 0 )
		{
			continue;
		}
		vec.push_back(it->second);
	}
	return true;
}
Setup_struct* GameSystemSetupManage::GetStructWithOpCode(const char* OpCode)
{
	SetupMap::iterator it = m_SetupMap.find(OpCode);
	if (it != m_SetupMap.end())
	{
		return it->second;
	}
	return NULL;
}
UControl* GameSystemSetupManage::GetControlWithOpCode(const char* OpCode)
{
	SetupCtrl::iterator it = m_SetupCtrl.find(OpCode);
	if (it != m_SetupCtrl.end())
	{
		return it->second;
	}
	return NULL;
}