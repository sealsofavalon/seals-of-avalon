#ifndef __GAMESYSTEMSETUPMGR_H__
#define __GAMESYSTEMSETUPMGR_H__

#include "GameSystemSetupStruct.h"
#include "GameSystemSetupDOMTool.h"

class GameSystemSetupManage
{
public:
	GameSystemSetupManage();
	~GameSystemSetupManage();
public:
	void SetGameSystemUserFileRoot(int Partypath, const char* path);
	bool GetGameSystemUserFileRoot(std::string &str);
	bool GetGameSystemAccountFile(std::string &str);
	bool GetStructWithSection(const char* section, vector<Setup_struct*>& vec);
	Setup_struct* GetStructWithOpCode(const char* OpCode);
	UControl* GetControlWithOpCode(const char* OpCode);

	BOOL CreateUserDirectory();
	BOOL RemoveUserDirectory(const char* UserName);
public:
	BOOL GetTextFormOpCode(const char* OpCode, std::string &str);
	void RegiestCtrl(const char* OpCode, UControl* ctrl);
	void OnLinkOpCode(const char* OpCode, bool LinkData);
public:
	void SetField(const char* OpCode, bool Choose);
	void SetField(const char* OpCode, int date);
	void SetField(const char* OpCode, UPoint date);
	void SetField(const char* OpCode, float date);

	void SetDefaultSetup();
	void SetupSettingToCtrl(struct Setup_struct* pSetupstruct);
	void SetupSetting();
	void SetupSettingBySection(const char* Section);
	void SetupSettingByDOM(const char* OpCode, SystemSetDOMTool &Dom, bool bRun = true);
	void AbandonSetting();
public:
	void SetSetupCell(const char* RootSection, const char* RealName, const char* OpCode, bool bChoose, ptrtypebool fun = NULL );
	void SetSetupCell(const char* RootSection, const char* RealName, const char* OpCode, int date, ptrtypeint fun = NULL );
	void SetSetupCell(const char* RootSection, const char* RealName, const char* OpCode, UPoint date, ptrtypePoint fun = NULL );
	void SetSetupCell(const char* RootSection, const char* RealName, const char* OpCode, float date, ptrtypeFloat fun = NULL );

protected:
	typedef std::map<std::string,class UControl*> SetupCtrl;
	SetupCtrl m_SetupCtrl;

	typedef std::map<std::string, struct Setup_struct*> SetupMap;	
	SetupMap m_SetupMap;

	std::string mAccountName;
	std::string mServerName;
	std::string mRoleName;
};
#endif