#include "stdafx.h"
#include "GameSystemSetupSave.h"
#include "SystemSetup.h"
#include "UChat.h"
#include "Hook/HookManager.h"


GameSystemSetupSave::GameSystemSetupSave()
{

}

GameSystemSetupSave::~GameSystemSetupSave()
{

}

void GameSystemSetupSave::SetupSystemSetting()
{
	SystemSetup->SetupSetting();
	SaveUserSetupFile();
}
bool GameSystemSetupSave::SaveAccountFile(const char* name)
{
	std::string UserFilePath;
	if (SystemSetup->GetGameSystemAccountFile(UserFilePath))
	{		
		FILE* file = fopen(UserFilePath.c_str(), "w+");
		if (file)
		{
			int len = strlen(name);
			fwrite(&len, 1, sizeof(int), file);
			fwrite(name, 1, len, file);
			fclose(file);
			return true;
		}
		return false;
	}
	return false;
}
bool GameSystemSetupSave::SaveUserSetupFile()
{
	std::string UserFilePath;
	if (SystemSetup->GetGameSystemUserFileRoot(UserFilePath))
	{
		std::string SettingFilePath = UserFilePath + "UserSetting.xml";
		SystemSetDOMTool kCDOM;
		kCDOM.Init(SettingFilePath.c_str());
		SaveWithSection("System", kCDOM);
		SaveWithSection("Audio", kCDOM);	
		SaveWithSection("Video", kCDOM);
		kCDOM.SaveFile(SettingFilePath.c_str());
		std::string KeyMapFile = UserFilePath + "UserKeyMap.ackey";
		return UControl::sm_System->SaveAcceleratorKey(KeyMapFile.c_str())?true:false;
	}
	return false;
}
bool GameSystemSetupSave::SaveUserHookSetFile()// ����HOOK �趨
{
	std::string UserFilePath;
	if (SystemSetup->GetGameSystemUserFileRoot(UserFilePath))
	{
		std::string SettingFilePath = UserFilePath + "UserHook.xml";
		SystemSetDOMTool kCDOM;
		kCDOM.Init(SettingFilePath.c_str());

		TiXmlElement* footroot = kCDOM.CreateRootSection("HOOK");
		TiXmlElement* root1 = kCDOM.CreateSection("HOOKSET");
		const HookData* pkHookData = HookMgr->GetLoadHookData();
		if (root1)
		{
			kCDOM.PushElement(root1, "AutoAttack",     (bool) pkHookData->AutoAttack);
			kCDOM.PushElement(root1, "AutoSupply",     (bool) pkHookData->AutoSupply);
			kCDOM.PushElement(root1, "AutoPick",       (bool) pkHookData->AutoPick);
			kCDOM.PushElement(root1, "AutoShunBoss",   (bool) pkHookData->AutoShunBoss);
			kCDOM.PushElement(root1, "DeadAutoPos",	   (bool) pkHookData->DeadAutoPos);
			kCDOM.PushElement(root1, "UseSpellFirst",  (bool) pkHookData->UseSpellFirst);

			kCDOM.PushElement(root1, "Spell0", (int) pkHookData->Spell[0]);
			kCDOM.PushElement(root1, "Spell1", (int) pkHookData->Spell[1]);
			kCDOM.PushElement(root1, "Spell2", (int) pkHookData->Spell[2]);
			kCDOM.PushElement(root1, "Spell3", (int) pkHookData->Spell[3]);
			kCDOM.PushElement(root1, "Spell4", (int) pkHookData->Spell[4]);
		
			kCDOM.PushElement(root1, "HPSpell", (int) pkHookData->HPSpell);
			kCDOM.PushElement(root1, "MPSpell", (int) pkHookData->MPSpell);

			kCDOM.PushElement(root1, "HPItem0", (int) pkHookData->HPEntry[0]);
			kCDOM.PushElement(root1, "HPItem1", (int) pkHookData->HPEntry[1]);
			kCDOM.PushElement(root1, "HPItem2", (int) pkHookData->HPEntry[2]);


			kCDOM.PushElement(root1, "MPItem0", (int) pkHookData->MPEntry[0]);
			kCDOM.PushElement(root1, "MPItem1", (int) pkHookData->MPEntry[1]);
			kCDOM.PushElement(root1, "MPItem2", (int) pkHookData->MPEntry[2]);

			kCDOM.PushElement(root1, "HP", (float) pkHookData->Hp);
			kCDOM.PushElement(root1, "MP", (float) pkHookData->Mp);

		}
		bool bsuccess = kCDOM.SaveFile(SettingFilePath.c_str());
		return bsuccess;
	}
	return false;
}
bool GameSystemSetupSave::SaveUserChatSetFile()
{
	std::string UserFilePath;
	if (SystemSetup->GetGameSystemUserFileRoot(UserFilePath))
	{
		std::string SettingFilePath = UserFilePath + "UserChatSetting.xml";
		SystemSetDOMTool kCDOM;
		kCDOM.Init(SettingFilePath.c_str());

		TiXmlElement* footroot = kCDOM.CreateRootSection("ChatSetting");
		TiXmlElement* root1 = kCDOM.CreateSection("ChatChannelModify");
		if (root1)
		{
			for (int i = 0 ; i < CHAT_TABLE_NUM ; i++)
			{
				char buf[256];
				sprintf(buf, "CHAT_TABLE_%d", i);
				kCDOM.PushElement(root1, buf, ChatSystem->GetMsgStore(i)->VisModify);
			}
		}
		bool bsuccess = kCDOM.SaveFile(SettingFilePath.c_str());
		return bsuccess;
	}
	return false;
}

void GameSystemSetupSave::SaveWithSection(const char* section, SystemSetDOMTool &DOM)
{
	TiXmlElement *root = DOM.CreateRootSection(section);
	if (root)
	{
		std::vector<Setup_struct*> vec;
		SystemSetup->GetStructWithSection(section, vec);
		for (UINT i = 0; i < vec.size() ; ++i)
		{
			Setup_struct* pSetupStruct = vec[i];
			switch(pSetupStruct->type)
			{
				case GOT_BOOLTYPE:
					{
						Setup_structbool * pkBoolType = (Setup_structbool* )pSetupStruct;
						DOM.PushElement(root, pSetupStruct->OpCode.c_str(), pkBoolType->Date);
					}
					break;
				case GOT_INTTYPE:
					{
						Setup_structint * pkIntType = (Setup_structint* )pSetupStruct;
						DOM.PushElement(root, pSetupStruct->OpCode.c_str(), pkIntType->Date);
					}
					break;
				case GOT_POINTTYPE:
					{
						Setup_structPoint * pkPointType = (Setup_structPoint* )pSetupStruct;
						DOM.PushElement(root, pSetupStruct->OpCode.c_str(), pkPointType->Date);
					}
					break;
				case GOT_FLOATTYPE:
					{
						Setup_structfloat * pkFloatType = (Setup_structfloat* )pSetupStruct;
						DOM.PushElement(root, pSetupStruct->OpCode.c_str(), pkFloatType->Date);
					}
					break;
			};
		}
	}
}

void GameSystemSetupSave::SetSystemSettingFormDOM(const char* Section, SystemSetDOMTool &Dom, bool bRun)
{
	if (!Dom.ResetSectionTo(Section))
		return;
	Dom.SetSectionToFirstChild();
	while( Dom.IsCurrentSectionValid() )
	{
		const char* pcValue = Dom.GetValueFromCurrent();
		SystemSetup->SetupSettingByDOM(pcValue, Dom, bRun);
		Dom.SetSectionToNextSibling();
	}
}