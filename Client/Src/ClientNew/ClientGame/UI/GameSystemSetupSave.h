#ifndef __GAMESYSTEMSETUPSAVE_H__
#define __GAMESYSTEMSETUPSAVE_H__

#include "GameSystemSetupStruct.h"
#include "GameSystemSetupDOMTool.h"

class GameSystemSetupSave
{
public:
	GameSystemSetupSave();
	~GameSystemSetupSave();
public:
	void SetupSystemSetting();
	bool SaveUserSetupFile();
	bool SaveUserChatSetFile();
	bool SaveUserHookSetFile();// ����HOOK �趨
	bool SaveAccountFile(const char* name);
	void SaveWithSection(const char* section, SystemSetDOMTool &DOM);
	void SetSystemSettingFormDOM(const char* Section, SystemSetDOMTool &Dom, bool bRun = true);
};
#endif