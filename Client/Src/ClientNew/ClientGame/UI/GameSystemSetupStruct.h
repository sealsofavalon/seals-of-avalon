#ifndef __GAMESYSTEMSETUPSTRUCT_H__
#define __GAMESYSTEMSETUPSTRUCT_H__

enum SetupStructType
{
	GOT_BOOLTYPE,
	GOT_INTTYPE,
	GOT_POINTTYPE,
	GOT_FLOATTYPE,
};

typedef void(*ptrtypebool)(bool);
typedef void(*ptrtypeint)(int);
typedef void(*ptrtypePoint)(UPoint);
typedef void(*ptrtypeFloat)(float);

struct Setup_struct 
{
	std::string OpCode;
	std::string RealName;
	std::string RootSection;
	SetupStructType type;
	BOOL bChange;
	virtual void VALUE(void* date) = 0;
	virtual void UNDO() = 0;
	virtual void RUN() = 0;
};
struct Setup_structbool : public Setup_struct
{
	Setup_structbool():Date(true)
	{
		type = GOT_BOOLTYPE;
		bChange = FALSE;
	}
	bool Date;
	bool Undo;
	ptrtypebool fun;
	virtual void VALUE(void* date)
	{
		Date = *(bool*)date;
		bChange = TRUE;
	}
	virtual void UNDO()
	{
		Date = Undo;
		bChange = FALSE;
	}
	virtual void RUN()
	{
		if (fun)
		{
			(*fun)(Date);
			Undo = Date;
		}
	}
};
struct Setup_structint : public Setup_struct
{
	Setup_structint():Date(0)
	{
		type = GOT_INTTYPE; 
		bChange = FALSE;
	}
	int Date;
	int Undo;
	ptrtypeint fun;	
	virtual void VALUE(void* date)
	{
		Date = *(int*)date;
		bChange = TRUE;
	}
	virtual void UNDO()
	{
		Date = Undo;
		bChange = FALSE;
	}
	virtual void RUN()
	{		
		if (fun)
		{
			(*fun)(Date);
			Undo = Date;
		}
	}
};
struct Setup_structfloat : public Setup_struct
{
	Setup_structfloat():Date(0)
	{
		type = GOT_FLOATTYPE; 
		bChange = FALSE;
	}
	float Date;
	float Undo;
	ptrtypeFloat fun;	
	virtual void VALUE(void* date)
	{
		Date = *(float*)date;
		bChange = TRUE;
	}
	virtual void UNDO()
	{
		Date = Undo;
		bChange = FALSE;
	}
	virtual void RUN()
	{		
		if (fun)
		{
			(*fun)(Date);
			Undo = Date;
		}
	}
};
struct Setup_structPoint : public Setup_struct
{
	Setup_structPoint():Date(0,0)
	{
		type = GOT_POINTTYPE; 
		bChange = FALSE;
	}
	UPoint Date;
	UPoint Undo;
	ptrtypePoint fun;
	virtual void VALUE(void* date)
	{
		Date = *(UPoint*)date;
		bChange = TRUE;
	}
	virtual void UNDO()
	{
		Date = Undo;
		bChange = FALSE;
	}
	virtual void RUN()
	{
		if (fun)
		{
			(*fun)(Date);
			Undo = Date;
		}
	}
};
#endif