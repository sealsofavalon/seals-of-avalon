#include "stdafx.h"
#include "GuildSys.h"
#include "../Network/PacketBuilder.h"
#include "UIGamePlay.h"
#include "UIRightMouseList.h"
#include "SocialitySystem.h"
#include "ObjectManager.h"
#include "UIShowPlayer.h"
#include "UInGameBar.h"
#include "UFun.h"
#include "UMessageBox.h"
#include "Utils/CastleNpcListDB.h"
#include "utils/ChatFileterDB.h"
#include "SystemTips .h"
#include "UChat.h"
#include "Utils/LevelTipsDB.h"
#include "USkillButton.h"
#include "ItemManager.h"
class UGuildListItem : public UControl
{
	UDEC_CLASS(UGuildListItem);
public:
	UGuildListItem()
	{
		m_RankMaskBitMap = NULL;
		m_NameStateText = NULL;
		m_GenderStateText = NULL;
		m_LevelStateText = NULL;
		m_ClassStateText = NULL;
		m_AtribuiltText = NULL;
		m_GUID = 0;
		m_RightMouseDown = FALSE;
		m_bOnline = FALSE;
		m_bChoose = false;
	}
	~UGuildListItem()
	{

	}
public:
	void SetBeChoose(bool choose)
	{
		m_bChoose = choose;
		UpdataMemTextColor();
	}
	void SetMemberOnline(BOOL sta)
	{
		if ( !m_bOnline && sta )
		{
			SYState()->IAudio->PlayUiSound( "Sound/UI/friendOnline.wav" );
		}
		m_bOnline = sta;
		UpdataMemTextColor();
	}
	void UpdataMemTextColor()
	{
		UColor color = m_bChoose?m_Style->m_FontColorHL:m_Style->m_FontColor;
		if (!m_bOnline){
			color.Set(128, 128, 128, 255);
		}
		m_NameStateText->SetTextColor(color);
		m_GenderStateText->SetTextColor(color);
		m_LevelStateText->SetTextColor(color);
		m_ClassStateText->SetTextColor(color);
		m_AtribuiltText->SetTextColor(color);

		m_NameStateText->SetTextEdge(m_bChoose);
		m_GenderStateText->SetTextEdge(m_bChoose);
		m_LevelStateText->SetTextEdge(m_bChoose);
		m_ClassStateText->SetTextEdge(m_bChoose);
		m_AtribuiltText->SetTextEdge(m_bChoose);
	}
	void SetMemberRank(ui32 rank_id)
	{
		std::string filePath = "";
		std::string tooltips = SocialitySys->GetGuildSysPtr()->GetRankNameByRankId(rank_id);
		m_ToolTips.Set( tooltips.c_str() );
		switch(rank_id)
		{
		case 0:
			filePath = "Data\\ui\\SocialitySystem\\1.png";
			break;
		case 1:
			filePath = "Data\\ui\\SocialitySystem\\2.png";
			break;
		case 2:
			filePath = "Data\\ui\\SocialitySystem\\3.png";
			break;
		case 3:
			filePath = "Data\\ui\\SocialitySystem\\4.png";
			break;
		default:
			filePath = "Data\\ui\\SocialitySystem\\5.png";
			break;
		}
		if (filePath.size())
		{
			m_RankMaskBitMap->SetBitMapByStr(filePath.c_str());
		}
	}
	void SetMemberContributionPoints(uint32 contribution_points)
	{
		char buf[256];
		itoa(contribution_points, buf, 10);
		m_AtribuiltText->SetText(buf);
	}
	void SetMemberBase(const char* Name, ui32 rank_id, ui8 Gender, ui32 level, ui32 Class, ui64 guid, uint32 contribution_points)
	{
		if (Name)
		{
			m_NameStateText->SetText(Name);
			m_Name = Name;
		}

		m_GenderStateText->SetText(Gender?_TRAN("男"):_TRAN("女"));

		char buf[256];
		buf[0] = 0;
		itoa(level, buf, 10);
		m_LevelStateText->SetText(buf);

		itoa(contribution_points, buf, 10);
		m_AtribuiltText->SetText(buf);

		char szClass[8][20] = { "", "武修", "羽箭", "仙道", "真巫", "变身", "刺客", "" };
		m_ClassStateText->SetText(_TRAN(szClass[Class]));

		m_GUID = guid;

		SetMemberRank(rank_id);
	}
	bool GetName(std::string& str)
	{
		if (m_Name.size())
		{
			str = m_Name;
			return true;
		}
		return false;
	}
	ui64 GetGUID()
	{
		return m_GUID;
	}
	bool IsMemOnline()
	{
		return m_bOnline?true:false;
	}
protected:
	virtual BOOL OnCreate()
	{
		if (!UControl::OnCreate())
		{
			return FALSE;
		}
		if (m_RankMaskBitMap == NULL)
		{
			m_RankMaskBitMap = UDynamicCast(UBitmap, GetChildByID(0));
			if (!m_RankMaskBitMap)
			{
				return FALSE;
			}
		}
		if (m_NameStateText == NULL)
		{
			m_NameStateText = UDynamicCast(UStaticText, GetChildByID(1));
			if (!m_NameStateText)
			{
				return FALSE;
			}
			m_NameStateText->SetTextAlign(UT_LEFT);
		}
		if (m_GenderStateText == NULL)
		{
			m_GenderStateText = UDynamicCast(UStaticText, GetChildByID(2));
			if (!m_GenderStateText)
			{
				return FALSE;
			}
			m_GenderStateText->SetTextAlign(UT_CENTER);
		}
		if (m_LevelStateText == NULL)
		{
			m_LevelStateText = UDynamicCast(UStaticText, GetChildByID(3));
			if (!m_LevelStateText)
			{
				return FALSE;
			}
			m_LevelStateText->SetTextAlign(UT_CENTER);
		}
		if (m_ClassStateText == NULL)
		{
			m_ClassStateText = UDynamicCast(UStaticText, GetChildByID(4));
			if (!m_ClassStateText)
			{
				return FALSE;
			}
			m_ClassStateText->SetTextAlign(UT_CENTER);
		}
		if (m_AtribuiltText == NULL)
		{
			m_AtribuiltText = UDynamicCast(UStaticText, GetChildByID(5));
			if (!m_AtribuiltText)
			{
				return FALSE;
			}
			m_AtribuiltText->SetTextAlign(UT_CENTER);
		}
		mTipHoverTime = 300;
		return TRUE;
	}
	virtual void OnDestroy()
	{
		UControl::OnDestroy();
	}
	virtual void OnMouseEnter(const UPoint& position, UINT flags)
	{
		UControl::OnMouseEnter(position, flags);
	}
	virtual void OnMouseLeave(const UPoint& position, UINT flags)
	{
		UControl::OnMouseLeave(position, flags);
	}
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
	{
		//GuildSys SetSelectItem
		if (SocialitySys->GetGuildSysPtr())
		{
			SocialitySys->GetGuildSysPtr()->SetSelectItem(this);
		}
	}
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
	{
		m_RightMouseDown = TRUE;
		if (SocialitySys->GetGuildSysPtr())
		{
			SocialitySys->GetGuildSysPtr()->SetSelectItem(this);
		}
	}
	virtual void OnRightMouseUp(const UPoint& position, UINT flags)
	{
		if (m_RightMouseDown)
		{
			UIRightMouseList* pkRightMouseList = INGAMEGETFRAME(UIRightMouseList,FRAME_IG_RIGHTMOUSELIST);
			if(pkRightMouseList)
			{
				pkRightMouseList->Popup(position, BITMAPLIST_GUILD, m_Name.c_str(), m_GUID);
			}
			m_RightMouseDown = FALSE;
		}
	}
	virtual void OnRender(const UPoint& offset, const URect& updateRect)
	{
		if(m_bChoose && SocialitySys && SocialitySys->mBKGtexture)
			sm_UiRender->DrawImage(SocialitySys->mBKGtexture, updateRect);

		RenderChildWindow(offset, updateRect);
	}
protected:
	BOOL		 m_bOnline;
	bool		 m_bChoose;
	BOOL		 m_RightMouseDown;
	ui64		 m_GUID;
	std::string  m_Name;
	UBitmap*	 m_RankMaskBitMap;
	UStaticText* m_NameStateText;
	UStaticText* m_GenderStateText;
	UStaticText* m_LevelStateText;
	UStaticText* m_ClassStateText;
	UStaticText* m_AtribuiltText;
};
UIMP_CLASS(UGuildListItem, UControl)
void UGuildListItem::StaticInit(){}
//////////////////////////////////////////////////////////////////////////
class UIRankRadioButton : public UControl
{
	UDEC_CLASS(UIRankRadioButton);
public:
	UIRankRadioButton()
	{
		m_BkgSkin = NULL;
		mIsCheck = FALSE;
		m_bPress = 0;
	}
	~UIRankRadioButton()
	{
		m_BkgSkin = NULL;
	}
public:
	bool IsCheck()
	{
		return mIsCheck?true:false;
	}
	void SetCheck(BOOL vis)
	{
		mIsCheck = vis;
	}
protected:
	virtual BOOL OnCreate()
	{
		if (!UControl::OnCreate())
		{
			return FALSE;
		}
		if (m_strBitmapFile.GetBuffer())
		{
			m_BkgSkin = sm_System->LoadSkin(m_strBitmapFile.GetBuffer());
			if (!m_BkgSkin)
			{
				return FALSE;
			}
		}else
		{
			m_BkgSkin = sm_System->LoadSkin("Public\\SendCheck.skin");
		}
		return TRUE;
	}
	virtual void OnDestroy()
	{
		UControl::OnDestroy();
	}
	virtual void OnRender(const UPoint& offset,const URect &updateRect)
	{
		if (mIsCheck)
		{
			URect rect;
			rect.Set(offset.x, offset.y, updateRect.GetHeight(), updateRect.GetHeight());
			sm_UiRender->DrawSkin(m_BkgSkin, 2, rect);
		}
		else
		{
			URect rect;
			rect.Set(offset.x, offset.y, updateRect.GetHeight(), updateRect.GetHeight());
			sm_UiRender->DrawSkin(m_BkgSkin, 0, rect);
		}
		UDrawText(sm_UiRender, m_Style->m_spFont, offset, updateRect.GetSize(), m_Style->m_FontColor, m_strName.GetBuffer(), UT_RIGHT);
	}
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
	{
		if (!m_Active)
		{
			return;
		}

		if (!m_Style->mSoundButtOnDown.Empty())
		{
			sm_System->__PlaySound(m_Style->mSoundButtOnDown.GetBuffer());
		}
		CaptureControl();
		m_bPress = TRUE;
	}
	virtual void OnMouseUp(const UPoint& position, UINT flags)
	{
		if (!m_Active)
			return;
		ReleaseCapture();
		if (m_bPress)
		{
			PreCommand();
		}
		m_bPress = FALSE;
	}
	void PreCommand()
	{
		if(!m_Active)
			return;
		mIsCheck = !mIsCheck;
	}
protected:
	BOOL m_bPress;
	BOOL mIsCheck;
	USkinPtr m_BkgSkin;
	UString m_strBitmapFile;
	UString m_strName;
};
UIMP_CLASS(UIRankRadioButton, UControl)
void UIRankRadioButton::StaticInit()
{
	UREG_PROPERTY("Skin", UPT_STRING, UFIELD_OFFSET(UIRankRadioButton, m_strBitmapFile));
	UREG_PROPERTY("text", UPT_STRING, UFIELD_OFFSET(UIRankRadioButton, m_strName));
}
//////////////////////////////////////////////////////////////////////////
/////////////////////////////工会列表////////////////////////////////////
UIMP_CLASS(UIGuildList, UControl)
void UIGuildList::StaticInit(){}
UIGuildList::UIGuildList()
{
}
UIGuildList::~UIGuildList()
{

}
BOOL UIGuildList::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	return TRUE;
}
void UIGuildList::OnDestroy()
{
	ClearList();
	UControl::OnDestroy();
}	

void UIGuildList::OnRender(const UPoint& offset,const URect &updateRect)
{
	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	g_pRenderInterface->SetScissorRect(sm_UiRender->GetClipRect());
	RenderChildWindow(offset, updateRect);
	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
}
void UIGuildList::GetScrollLineSizes(UINT *rowHeight, UINT *columnWidth)
{
	//MemberMap::iterator it = m_MemberMap.begin();
	//if(it != m_MemberMap.end())
	//{
	//	*rowHeight = it->second->GetHeight();
	//}
	//else
	*rowHeight = 30;
	*columnWidth = 30;
}
void UIGuildList::SetMemberOnline(ui64 guid)
{
	for (int i = 0 ; i < 17 ; i++)
	{
		UGuildListItem* pItem = UDynamicCast(UGuildListItem, GetChildByID(i));
		if (pItem && pItem->GetGUID() == guid)
		{
			pItem->SetMemberOnline(true);
			return;
		}
	}
}

void UIGuildList::SetMemberOffline(ui64 guid)
{
	for (int i = 0 ; i < 17 ; i++)
	{
		UGuildListItem* pItem = UDynamicCast(UGuildListItem, GetChildByID(i));
		if (pItem && pItem->GetGUID() == guid)
		{
			pItem->SetMemberOnline(false);
			return;
		}
	}
}

void UIGuildList::SetMemberContributionPoints(ui64 guid, uint32 contribution_points)
{
	for (int i = 0 ; i < 17 ; i++)
	{
		UGuildListItem* pItem = UDynamicCast(UGuildListItem, GetChildByID(i));
		if (pItem && pItem->GetGUID() == guid)
		{
			pItem->SetMemberContributionPoints(contribution_points);
			return;
		}
	}
}

void UIGuildList::SetMemberRank(ui64 guid, uint8 rankid)
{
	for (int i = 0 ; i < 17 ; i++)
	{
		UGuildListItem* pItem = UDynamicCast(UGuildListItem, GetChildByID(i));
		if (pItem && pItem->GetGUID() == guid)
		{
			pItem->SetMemberRank(rankid);
			return;
		}
	}
}
void UIGuildList::UpdataList()
{
	std::vector<MSG_S2C::stGuild_Roster::stMember> temp;
	if(!SocialitySys->GetGuildSysPtr()->GetGuildMemberList(temp))
		return;
	int curpage, maxpage;
	SocialitySys->GetGuildSysPtr()->GetPage(curpage, maxpage);
	int OnlineMem = 0 ;
	for (unsigned int ui = 0 ; ui < 17 ; ui++)
	{
		UGuildListItem* pItem = UDynamicCast(UGuildListItem, GetChildByID(ui));
		if (pItem)
		{
			UINT pos = curpage * 17 + ui;
			bool bShow = pos < temp.size();
			pItem->SetVisible(bShow);
			if (bShow)
			{
				pItem->SetMemberOnline(temp[pos].bOnline);
				pItem->SetMemberBase(temp[pos].name.c_str(), temp[pos].rank_id, temp[pos].gender, temp[pos].last_level, temp[pos].Class, temp[pos].player_id, temp[pos].contribution_points);
			}
		}
	}
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->SetSelectItem(NULL);
	}
	ui32 level = SocialitySys->GetGuildSysPtr()->GetGuildLevel();
	ui32 membercount = g_pkGuildLevelUpDB->Getmembercount(level);

	UIGuildFrame* pFrame = UDynamicCast(UIGuildFrame, GetParent());
	if (pFrame)
	{
		for (unsigned int ui = 0 ; ui < temp.size() ; ++ui)
		{
			OnlineMem += temp[ui].bOnline;
		}
		pFrame->OnSetMemNumText(OnlineMem, temp.size(), membercount);
	}
}
void UIGuildList::ClearList()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->SetSelectItem(NULL);
	}
	sm_System->SetMouseControl(NULL);
	for (int i = 0 ; i < 17 ; i++)
	{
		UGuildListItem* pItem = UDynamicCast(UGuildListItem, GetChildByID(i));
		if (pItem)
		{
			pItem->SetVisible(FALSE);
			return;
		}
	}
}
//////////////////////////////////////////////////////////////////////////
///////////////////////////////工会界面/////////////////////////////////////
UIMP_CLASS(UIGuildFrame, UDialog)
UBEGIN_MESSAGE_MAP(UIGuildFrame, UDialog)
UON_BN_CLICKED(40,&UIGuildFrame::OnClickBtnQuitGuild)
UON_BN_CLICKED(41,&UIGuildFrame::OnClickBtnCityInfo)
UON_BN_CLICKED(42,&UIGuildFrame::OnClickBtnDelMember)
UON_BN_CLICKED(43,&UIGuildFrame::OnClickBtnInviteMem)
UON_BN_CLICKED(44,&UIGuildFrame::OnClickBtnChangeMark)
UON_BN_CLICKED(45,&UIGuildFrame::OnClickBtnSaveGuildInfo)
UON_BN_CLICKED(46,&UIGuildFrame::OnClickBtnSaveChange)
//UON_BN_CLICKED(47,&UIGuildFrame::OnClickBtnRankList)
//UON_BMLB_CLICKED(52,&UIGuildFrame::OnClickRankList)
UON_BN_CLICKED(11,&UIGuildFrame::OnClickBtnSortByRank)
UON_BN_CLICKED(12,&UIGuildFrame::OnClickBtnSortByName)
UON_BN_CLICKED(13,&UIGuildFrame::OnClickBtnSortByGender)
UON_BN_CLICKED(14,&UIGuildFrame::OnClickBtnSortByLevel)
UON_BN_CLICKED(15,&UIGuildFrame::OnClickBtnSortByClass)
UON_BN_CLICKED(16,&UIGuildFrame::OnClickBtnSortByAtrr)
UON_BN_CLICKED(17,&UIGuildFrame::OnClickBtnSortByOnline)
UON_BN_CLICKED(60,&UIGuildFrame::OnClickBtnFriendPage)
UON_BN_CLICKED(62,&UIGuildFrame::OnClickBtnOLPlayerPage)
UON_BN_CLICKED(63,&UIGuildFrame::OnClickBtnTeamPage)
UON_BN_CLICKED(64,&UIGuildFrame::OnClickBtnGroupApplyPage)
UON_BN_CLICKED(48,&UIGuildFrame::OnClickBtnGuildDisband)
UON_BN_CLICKED(70,&UIGuildFrame::OnPrevPage)
UON_BN_CLICKED(72,&UIGuildFrame::OnNextPage)
UON_CBOX_SELCHANGE(56, &UIGuildFrame::OnClickRankList)
UEND_MESSAGE_MAP()
void UIGuildFrame::StaticInit(){}
UIGuildFrame::UIGuildFrame()
{
	mGuildList = NULL;
	mRankComboBox = NULL;
}
UIGuildFrame::~UIGuildFrame()
{

}
UIGuildList* UIGuildFrame::GetGuildList()
{
	return mGuildList;
}
void UIGuildFrame::BeginRankList()
{
	if (mRankComboBox)
	{
		mRankComboBox->Clear();
	}
}
void UIGuildFrame::ValueRankList(const char * rankname)
{
	if (rankname && mRankComboBox)
	{
		mRankComboBox->AddItem(rankname);
	}
}
void UIGuildFrame::EndRankList()
{
	if (mRankComboBox && mRankComboBox->GetCount())
	{
		mRankComboBox->SetCurSel(0);
	}
}
void UIGuildFrame::SetGuildInfo(std::string &info)
{
	UStaticTextEdit* pStaticTextEdit = UDynamicCast(UStaticTextEdit, GetChildByID(9));
	if (pStaticTextEdit)
	{
		pStaticTextEdit->SetText(info.c_str());
	}
}

void UIGuildFrame::SetGuildName(const char* Name)
{
	UStaticText* pStaticText = UDynamicCast(UStaticText, GetChildByID(1));
	if (pStaticText)
	{
		std::string str = Name;
		str = _TRAN("部族：")+ str;
		pStaticText->SetText(str.c_str());
		pStaticText->SetTextAlign(UT_CENTER);
		pStaticText->SetTextEdge(true);
	}
}

void UIGuildFrame::SetGuildLevel(uint8 level)
{
	UStaticText* pStaticText = UDynamicCast(UStaticText, GetChildByID(3));
	if (pStaticText)
	{
		char buf[256];
		sprintf(buf, "%u", level);
		pStaticText->SetText(buf);
		pStaticText->SetTextAlign(UT_LEFT);
	}
}

void UIGuildFrame::SetGuildScore(ui32 Score)
{
	UStaticText* pStaticText = UDynamicCast(UStaticText, GetChildByID(5));
	if (pStaticText)
	{
		char buf[256];
		sprintf(buf, "%u", Score);
		pStaticText->SetText(buf);
		pStaticText->SetTextAlign(UT_LEFT);
	}
}

void UIGuildFrame::SetGuildWarScore(ui32 WarScore)
{
	UStaticText* pStaticText = UDynamicCast(UStaticText, GetChildByID(7));
	if (pStaticText)
	{
		char buf[256];
		sprintf(buf, "%u", WarScore);
		pStaticText->SetText(buf);
		pStaticText->SetTextAlign(UT_LEFT);
	}
}

void UIGuildFrame::SetGuildDisband()
{
	mGuildList->ClearList();
	if (IsVisible())
	{
		OnClose();
	}
}
void UIGuildFrame::SetGuildEmblemBitMap(char* buf)
{
	UBitmap* pBitMap = UDynamicCast(UBitmap, GetChildByID(55));
	if (pBitMap)
	{
		pBitMap->SetBitMapByStr(buf);
	}
}

void UIGuildFrame::Show()
{
	SetVisible(TRUE);
}
void UIGuildFrame::Hide()
{
	SetVisible(FALSE);
	UButton* pCheckButton = (UButton*)GetChildByID(61);
	if (pCheckButton)
	{
		pCheckButton->SetCheck(FALSE);
	}
}
void UIGuildFrame::OnPrevPage()
{
	UControl* pBtn = GetChildByID(70);
	if (pBtn)
	{
		pBtn->SetActive(SocialitySys->GetGuildSysPtr()->PrevPage());
	}
	int curpage, maxpage;
	SocialitySys->GetGuildSysPtr()->GetPage(curpage, maxpage);
	pBtn = GetChildByID(72);
	if (pBtn)
	{
		pBtn->SetActive(maxpage > 0 && curpage < maxpage);
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(71));
	if (pText)
	{
		char buf[256];
		pText->SetText(itoa(curpage, buf, 10));
	}
	mGuildList->UpdataList();

}

void UIGuildFrame::OnNextPage()
{
	UControl* pBtn = GetChildByID(72);
	if (pBtn)
	{
		pBtn->SetActive(SocialitySys->GetGuildSysPtr()->NextPage());
	}
	int curpage, maxpage;
	SocialitySys->GetGuildSysPtr()->GetPage(curpage, maxpage);
	pBtn = GetChildByID(70);
	if (pBtn)
	{
		pBtn->SetActive(maxpage > 0 && curpage > 0);
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(71));
	if (pText)
	{
		char buf[256];
		pText->SetText(itoa(curpage, buf, 10));
	}
	mGuildList->UpdataList();

}

void UIGuildFrame::OnSetMemNumText(int OnlineNum, int Count, int MaxCount)
{
	char buf[256];
	sprintf(buf, "%d/%d", Count, MaxCount );
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(75));
	if (pText)
	{
		pText->SetText(buf);
	}
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(12));
	if (pBtn)
	{
		sprintf(buf, "%s(%d)",_TRAN("成员"), OnlineNum);
		pBtn->SetText(buf);
	}
}

BOOL UIGuildFrame::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	if (mGuildList == NULL)
	{
		UControl* NewCtrl = GetChildByID(0);
		mGuildList = UDynamicCast(UIGuildList, NewCtrl);
		if (!mGuildList)
		{
			return FALSE;
		}
	}
	for (int i = 0 ; i < 6; i++)
	{
		mpRankChooseBtn[i] = UDynamicCast(UIRankRadioButton,GetChildByID(i+18));
		if (mpRankChooseBtn[i] == NULL)
		{
			return FALSE;
		}
	}
	UStaticTextEdit* pTextEdit = UDynamicCast(UStaticTextEdit ,GetChildByID(9));
	if (pTextEdit)
	{
		pTextEdit->SetTextMaxLen(100);
	}
	m_bCanDrag = FALSE;

	UButton* pCheckButton = (UButton*)GetChildByID(61);
	if (pCheckButton == NULL)
	{
		return FALSE;
	}
	pCheckButton->SetCheck(FALSE);

	if(mRankComboBox == NULL)
	{
		mRankComboBox = UDynamicCast(UComboBox, GetChildByID(56));
		if (!mRankComboBox)
		{
			return FALSE;
		}
	}
	UStaticText* pText = UDynamicCast(UStaticText ,GetChildByID(2));
	if (pText)
	{
		pText->SetTextAlign(UT_RIGHT);
		pText->SetTextColor("00d03e");
	}
	pText = UDynamicCast(UStaticText ,GetChildByID(4));
	if (pText)
	{
		pText->SetTextAlign(UT_RIGHT);
		pText->SetTextColor("00d03e");
	}
	pText = UDynamicCast(UStaticText ,GetChildByID(6));
	if (pText)
	{
		pText->SetTextAlign(UT_RIGHT);
		pText->SetTextColor("00d03e");
	}
	pText = UDynamicCast(UStaticText ,GetChildByID(10));
	if (pText)
	{
		pText->SetTextAlign(UT_RIGHT);
		pText->SetTextColor("df9302");
	}
	pText = UDynamicCast(UStaticText ,GetChildByID(74));
	if (pText)
	{
		pText->SetTextAlign(UT_RIGHT);
		pText->SetTextColor("00d03e");
	}

	pText = UDynamicCast(UStaticText ,GetChildByID(71));
	if (pText)
	{
		pText->SetTextAlign(UT_CENTER);
		pText->SetTextColor("018bd6");
	}


	//GetChildByID(46)->SetActive(FALSE);
	return TRUE;
}
void UIGuildFrame::OnDestroy()
{
	UDialog::OnDestroy();
}
void UIGuildFrame::OnClose()
{
	sm_System->GetCurFrame()->SetFocusControl();
	mRankComboBox->SetCurSel(0);
	if(SocialitySys) SocialitySys->HideAll();
	UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	if (pGameBar)
	{
		UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONFRIEND);
		if (pBtn)
		{
			pBtn->SetCheckState(FALSE);
		}
	}
}
BOOL UIGuildFrame::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void AcceptQuitGuild()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->SendMsgLeaveGuild();
	}
}

void DeclineQuitGuild()
{
}

void UIGuildFrame::OnClickBtnQuitGuild()
{
	UMessageBox::MsgBox(_TRAN("确定脱离部族么？"), (BoxFunction*)AcceptQuitGuild, (BoxFunction*)DeclineQuitGuild);
}
void UIGuildFrame::OnClickBtnCityInfo()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->OnGuildCastleInfo();
	}
}
void UIGuildFrame::OnClickBtnDelMember()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->RemoveSelect();
	}
}
void UIGuildFrame::OnClickBtnChangeMark()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->ParseUpLoadGuildEmblem();
	}
}
void UIGuildFrame::OnClickBtnInviteMem()
{
	if (TeamShow && TeamShow->GetTargetID())
	{
		CPlayer * pkLocalPlayer = ObjectMgr->GetLocalPlayer();
		CCharacter * pChar = (CCharacter*)ObjectMgr->GetObject(TeamShow->GetTargetID());
		if (pChar && pChar->isType(TYPE_PLAYER) && pChar->IsFriendly(pkLocalPlayer) && pChar != pkLocalPlayer)
		{
			SocialitySys->GetGuildSysPtr()->SendMsgInviteMember(pChar->GetName());			//PacketBuilder->SendGuildInvite(pChar->GetName());
		}
		else
		{
			UIAddMemberEdit* pEdit = NULL;
			pEdit = INGAMEGETFRAME(UIAddMemberEdit, FRAME_IG_ADDMEMBERDLG);
			pEdit->OnAddMember(1);
		}
	}
	else if (TeamShow->GetTargetID() == 0)
	{
		//TODO 输入对方姓名
		UIAddMemberEdit* pEdit = NULL;
		pEdit = INGAMEGETFRAME(UIAddMemberEdit, FRAME_IG_ADDMEMBERDLG);
		pEdit->OnAddMember(1);
	}
}
void UIGuildFrame::OnClickBtnSaveGuildInfo()
{
	UStaticTextEdit* pStaticTextEdit = UDynamicCast(UStaticTextEdit, GetChildByID(9));
	if (pStaticTextEdit)
	{
		char buf[256];
		pStaticTextEdit->GetText(buf, 256);
		SocialitySys->GetGuildSysPtr()->SendMsgSaveGuildInfo(buf);
	}
}
void UIGuildFrame::OnClickBtnSaveChange()
{
	GuildSys* pGuildSys = SocialitySys->GetGuildSysPtr();
	if(mRankComboBox && pGuildSys)
	{
		int rankid = mRankComboBox->GetCurSel();
		ui32 iRight;
		iRight = pGuildSys->SetRankRightVis(mpRankChooseBtn[0]->IsCheck(), rankid, GR_RIGHT_INVITE);
		iRight = pGuildSys->SetRankRightVis(mpRankChooseBtn[1]->IsCheck(), rankid, GR_RIGHT_REMOVE);
		iRight = pGuildSys->SetRankRightVis(mpRankChooseBtn[2]->IsCheck(), rankid, GR_RIGHT_EGUILDINFO);
		iRight = pGuildSys->SetRankRightVis(mpRankChooseBtn[3]->IsCheck(), rankid, GR_RIGHT_FENGYIN);
		iRight = pGuildSys->SetRankRightVis(mpRankChooseBtn[4]->IsCheck(), rankid, GR_RIGHT_DECLARE_WAR);
		//iRight = pGuildSys->SetRankRightVis(mpRankChooseBtn[5], GR_RIGHT_INVITE);

		char buf[256];
		if (buf)
		{
			pGuildSys->SendMsgSetGuildRank(rankid,buf,iRight);
		}
	}
}
void UIGuildFrame::OnClickBtnFriendPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(1);
	}
}
void UIGuildFrame::OnClickBtnGuildPage()
{

}
void UIGuildFrame::OnClickBtnOLPlayerPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(2);
	}
}
void UIGuildFrame::OnClickBtnTeamPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(3);
	}
}
void UIGuildFrame::OnClickBtnGroupApplyPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(4);
	}
}

void UIGuildFrame::OnClickBtnRankList()
{
	//if (mBitMapListBox && mBitMapListBox->GetItemCount() > 0)
	//{
	//	mBitMapListBox->SetPosition(UPoint(mBitMapListBox->GetWindowPos().x,401 - mBitMapListBox->GetHeight()));
	//	mBitMapListBox->SetVisible(TRUE);
	//	mBitMapListBox->CaptureControl();
	//}
}

void AcceptGuildDisband()
{
	PacketBuilder->SendGuildDisBand();
}

void DeclineGuildDisband()
{

}

void UIGuildFrame::OnClickBtnGuildDisband()
{
	ui32 rankid = SocialitySys->GetGuildSysPtr()->GetLocalRankId();
	if(rankid == 0)
		UMessageBox::MsgBox(_TRAN("确定解散部族么？"), (BoxFunction*)AcceptGuildDisband, (BoxFunction*)DeclineGuildDisband);
	else
		ClientSystemNotify(_TRAN("对不起，您没有权限解散部族。"));
}
void UIGuildFrame::OnClickRankList()
{
	if (mRankComboBox)
	{
		int rankid = mRankComboBox->GetCurSel();
		for (int i = 0 ; i < 6 ; i++)
		{
			mpRankChooseBtn[i]->SetCheck(FALSE);
		}
		GuildSys* pGuildSys = SocialitySys->GetGuildSysPtr();
		if (pGuildSys)
		{
			if (pGuildSys->GetRankRightVis(rankid, GR_RIGHT_INVITE))
			{
				mpRankChooseBtn[0]->SetCheck(TRUE);
			}
			if (pGuildSys->GetRankRightVis(rankid, GR_RIGHT_REMOVE))
			{
				mpRankChooseBtn[1]->SetCheck(TRUE);
			}
			if (pGuildSys->GetRankRightVis(rankid, GR_RIGHT_EGUILDINFO))
			{
				mpRankChooseBtn[2]->SetCheck(TRUE);
			}
			if (pGuildSys->GetRankRightVis(rankid, GR_RIGHT_DECLARE_WAR))
			{
				mpRankChooseBtn[4]->SetCheck(TRUE);
			}
			if (pGuildSys->GetRankRightVis(rankid, GR_RIGHT_FENGYIN))
			{
				mpRankChooseBtn[3]->SetCheck(TRUE);
			}
			//if (pGuildSys->GetRankRightVis(rankid, GR_RIGHT_INVITE))
			//{
			//}
		}
	}
}

void UIGuildFrame::OnClickBtnSortByRank()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->SortMemberByRank();
		mGuildList->UpdataList();
	}
}
void UIGuildFrame::OnClickBtnSortByName()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->SortMemberByName();
		mGuildList->UpdataList();
	}
}
void UIGuildFrame::OnClickBtnSortByGender()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->SortMemberByGender();
		mGuildList->UpdataList();
	}
}
void UIGuildFrame::OnClickBtnSortByLevel()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->SortMemberByLevel();
		mGuildList->UpdataList();
	}
}
void UIGuildFrame::OnClickBtnSortByClass()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->SortMemberByClass();
		mGuildList->UpdataList();
	}
}
void UIGuildFrame::OnClickBtnSortByAtrr()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->SortMemberByAtrr();
		mGuildList->UpdataList();
	}
}
void UIGuildFrame::OnClickBtnSortByOnline()
{
	if (SocialitySys->GetGuildSysPtr())
	{
		SocialitySys->GetGuildSysPtr()->SortMemberByOnline();
		mGuildList->UpdataList();
	}
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
class UIGuildQureyListitem : public UControl
{
	UDEC_CLASS(UIGuildQureyListitem);
public:
	UIGuildQureyListitem()
	{
		m_NameStateText = NULL;
		m_MemberNumStateText  = NULL;
		m_RaceStateText = NULL;
		m_LevelStateText = NULL;
		m_GuildBattlePoint = NULL;

		m_bChoose = false;
		m_Guildid = 0;
	}
	~UIGuildQureyListitem()
	{

	}
public:
	void SetBeChoose(bool choose)
	{
		m_bChoose = choose;
		UpdataMemTextColor();
	}
	void UpdataMemTextColor()
	{
		UColor color = m_bChoose?m_Style->m_FontColorHL:m_Style->m_FontColor;

		m_LevelStateText->SetTextColor(color);
		m_NameStateText->SetTextColor(color);
		m_MemberNumStateText->SetTextColor(color);
		m_GuildBattlePoint->SetTextColor(color);
		m_RaceStateText->SetTextColor(color);

		m_LevelStateText->SetTextEdge(m_bChoose);
		m_NameStateText->SetTextEdge(m_bChoose);
		m_MemberNumStateText->SetTextEdge(m_bChoose);
		m_GuildBattlePoint->SetTextEdge(m_bChoose);
		m_RaceStateText->SetTextEdge(m_bChoose);
	}
	void SetMemberBase(uint32 guildid, const char* Name, const char* guildLeader, uint8 level, uint32 Number, uint32 BattlePoint)
	{
		char buf[256];

		m_NameStateText->SetText(Name);

		itoa(level, buf, 10);
		m_LevelStateText->SetText(buf);

		itoa(Number, buf, 10);
		m_MemberNumStateText->SetText(buf);

		itoa(BattlePoint, buf, 10);
		m_GuildBattlePoint->SetText(buf);

		m_RaceStateText->SetText(guildLeader);

		m_Guildid = guildid;
	}
	bool GetGuildId(uint32 &Guildid)
	{
		if (m_Guildid)
		{
			Guildid = m_Guildid;
			return true;
		}
		return false;
	}
protected:
	virtual BOOL OnCreate()
	{
		if (!UControl::OnCreate())
		{
			return FALSE;
		}
		if (m_LevelStateText == NULL)
		{
			m_LevelStateText = UDynamicCast(UStaticText, GetChildByID(0));
			if (!m_LevelStateText)
			{
				return FALSE;
			}
			m_LevelStateText->SetTextAlign(UT_CENTER);
		}
		if (m_NameStateText == NULL)
		{
			m_NameStateText = UDynamicCast(UStaticText, GetChildByID(1));
			if (!m_NameStateText)
			{
				return FALSE;
			}
			m_NameStateText->SetTextAlign(UT_CENTER);
		}
		if (m_MemberNumStateText == NULL)
		{
			m_MemberNumStateText = UDynamicCast(UStaticText, GetChildByID(2));
			if (!m_MemberNumStateText)
			{
				return FALSE;
			}
			m_MemberNumStateText->SetTextAlign(UT_CENTER);
		}
		if (m_RaceStateText == NULL)
		{
			m_RaceStateText = UDynamicCast(UStaticText, GetChildByID(3));
			if (!m_RaceStateText)
			{
				return FALSE;
			}
			m_RaceStateText->SetTextAlign(UT_CENTER);
		}
		if (m_GuildBattlePoint == NULL)
		{
			m_GuildBattlePoint = UDynamicCast(UStaticText, GetChildByID(4));
			if (!m_GuildBattlePoint)
			{
				return FALSE;
			}
			m_GuildBattlePoint->SetTextAlign(UT_CENTER);
		}
		return TRUE;
	}
	virtual void OnDestroy()
	{
		UControl::OnDestroy();
	}
	virtual void OnMouseEnter(const UPoint& position, UINT flags)
	{

	}
	virtual void OnMouseLeave(const UPoint& position, UINT flags)
	{

	}
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
	{
		UIGuildQureyListDLg* pDlg = UDynamicCast(UIGuildQureyListDLg, GetParent());
		if (pDlg)
		{
			pDlg->SetChooseItem(this);
		}
	}
	virtual void OnRender(const UPoint& offset, const URect& updateRect)
	{
		if(m_bChoose && SocialitySys && SocialitySys->mBKGtexture)
			sm_UiRender->DrawImage(SocialitySys->mBKGtexture, updateRect);

		RenderChildWindow(offset, updateRect);
	}
protected:
	bool		 m_bChoose;
	uint32		 m_Guildid;

	UStaticText* m_LevelStateText;
	UStaticText* m_NameStateText;
	UStaticText* m_MemberNumStateText;
	UStaticText* m_RaceStateText;
	UStaticText* m_GuildBattlePoint;
};
UIMP_CLASS(UIGuildQureyListitem, UControl)
void UIGuildQureyListitem::StaticInit()
{

}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIGuildQureyListDLg, UDialog)
UBEGIN_MESSAGE_MAP(UIGuildQureyListDLg, UDialog)
//UON_BN_CLICKED(10,&UIGuildQureyListDLg::OnClickBtnOk)
UON_BN_CLICKED(8,&UIGuildQureyListDLg::OnClickBtnPrevPage)
UON_BN_CLICKED(9,&UIGuildQureyListDLg::OnClickBtnNextPage)
UON_BN_CLICKED(10,&UIGuildQureyListDLg::OnClickBtnSortByLevel)
UON_BN_CLICKED(11,&UIGuildQureyListDLg::OnClickBtnSortByName)
UON_BN_CLICKED(12,&UIGuildQureyListDLg::OnClickBtnSortByMemNum)
UON_BN_CLICKED(13,&UIGuildQureyListDLg::OnClickBtnSortByGuildLeaderName)
UON_BN_CLICKED(18,&UIGuildQureyListDLg::OnClickBtnDeclareWar)
UON_BN_CLICKED(19,&UIGuildQureyListDLg::OnClickBtnAllyReq)
UON_BN_CLICKED(20,&UIGuildQureyListDLg::OnClickBtnAllyQuit)
UEND_MESSAGE_MAP()
void UIGuildQureyListDLg::StaticInit()
{

}
UIGuildQureyListDLg::UIGuildQureyListDLg():PageCount(20),mbChooseItem(NULL)
{
	NowPage = 0;
	m_IsUpdateChild = FALSE;
}
UIGuildQureyListDLg::~UIGuildQureyListDLg()
{

}
BOOL UIGuildQureyListDLg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;

	UStaticText* pStaticText = UDynamicCast(UStaticText, GetChildByID(2));
	if (pStaticText)
	{
		pStaticText->SetTextColor("0293e2");
	}
	pStaticText = UDynamicCast(UStaticText, GetChildByID(1));
	if (pStaticText)
	{
		pStaticText->SetTextEdge(true);
	}

	return TRUE;
}
void UIGuildQureyListDLg::OnDestroy()
{
	mbChooseItem = NULL;
	UDialog::OnDestroy();
}
void UIGuildQureyListDLg::OnClose()
{
	SetVisible(FALSE);
}
BOOL UIGuildQureyListDLg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}	
void UIGuildQureyListDLg::OnClickBtnSortByName()
{
	SocialitySys->GetGuildSysPtr()->SortGuildByGuildName();
	UpdataNowList();
}
void UIGuildQureyListDLg::OnClickBtnSortByRace()
{
	SocialitySys->GetGuildSysPtr()->SortGuildByRace();
	UpdataNowList();
}
void UIGuildQureyListDLg::OnClickBtnSortByLevel()
{
	SocialitySys->GetGuildSysPtr()->SortGuildByLevel();
	UpdataNowList();
}
void UIGuildQureyListDLg::OnClickBtnSortByMemNum()
{
	SocialitySys->GetGuildSysPtr()->SortGuildByMemNum();
	UpdataNowList();
}
void UIGuildQureyListDLg::OnClickBtnSortByGuildLeaderName()
{
	SocialitySys->GetGuildSysPtr()->SortGuildByLeaderName();
	UpdataNowList();
}
void UIGuildQureyListDLg::OnClickBtnSortByCity()
{
	SocialitySys->GetGuildSysPtr()->SortGuildByCastleName();
	UpdataNowList();
}
void UIGuildQureyListDLg::OnClickBtnSortByAlien()
{
	SocialitySys->GetGuildSysPtr()->SortGuildByAlien();
	UpdataNowList();
}
void UIGuildQureyListDLg::OnClickBtnDeclareWar()
{
	uint32 guildid = 0;
	if (SocialitySys && mbChooseItem)
	{
		if (mbChooseItem->GetGuildId(guildid))
		{
			SocialitySys->GetGuildSysPtr()->SendMsgDeclareWar(guildid);
		}
	}
}
void UIGuildQureyListDLg::OnClickBtnAllyReq()
{
	uint32 guildid = 0;
	if (SocialitySys && mbChooseItem)
	{
		if (mbChooseItem->GetGuildId(guildid))
		{
			SocialitySys->GetGuildSysPtr()->SendMsgGuildAlien(guildid);
		}
	}
}
void UIGuildQureyListDLg::OnClickBtnAllyQuit()
{

}
void UIGuildQureyListDLg::OnClickBtnNextPage()
{
	if (NowPage < MaxPage)
	{
		++NowPage;
		UpdataNowList();
	}
}
void UIGuildQureyListDLg::OnClickBtnPrevPage()
{
	if (NowPage > 0)
	{
		--NowPage;
		UpdataNowList();
	}
}
void UIGuildQureyListDLg::OnGetGuildList(std::vector<MSG_S2C::stGuildListInfoAck::guild_info_t> &guildlist)
{
	MaxPage = guildlist.size() / PageCount;
	if (guildlist.size()%PageCount == 0 && guildlist.size() != 0)
	{
		--MaxPage;
	}
	NowPage = 0;
	UpdataNowList();
}

void UIGuildQureyListDLg::UpdataNowList()
{
	if (!SocialitySys)		return;
	std::vector<MSG_S2C::stGuildListInfoAck::guild_info_t> templist;
	if (SocialitySys->GetGuildSysPtr()->GetGuildList(templist))
	{
		for (unsigned int ui = 0 ; ui < PageCount ; ui++)
		{
			UIGuildQureyListitem* pItem = UDynamicCast(UIGuildQureyListitem, GetChildByID(40 + ui));
			if (pItem)
			{
				UINT pos = NowPage * PageCount + ui;
				bool bShow = pos < templist.size();
				pItem->SetVisible(bShow);
				if ( bShow )
				{
					std::string guildname = templist[pos].guildName;
					if (templist[pos].NoWar)
					{
						guildname = _TRAN("(免战)") + templist[pos].guildName;
					}
					pItem->SetMemberBase( templist[pos].id, guildname.c_str(), templist[pos].master_name.c_str(), templist[pos].level, templist[pos].member_count, templist[pos].war_score);
					//std::string castleName;
					//bool bGotCastle = SocialitySys->GetGuildSysPtr()->GetIsGuildGotCastle(templist[pos].id, castleName);
				}
			}
		}
		UStaticText* pStaticText = UDynamicCast(UStaticText, GetChildByID(2));
		if (pStaticText)
		{
			char buf[256];
			sprintf(buf, "%u/%u", NowPage + 1, MaxPage + 1);
			pStaticText->SetText(buf);
		}
		SetChooseItem(NULL);
	}else
	{
		for (unsigned int ui = 0 ; ui < PageCount ; ui++)
		{
			UIGuildQureyListitem* pItem = UDynamicCast(UIGuildQureyListitem, GetChildByID(40 + ui));
			if (pItem)
			{
				pItem->SetVisible(FALSE);
			}
		}

		SetChooseItem(NULL);
	}
}

void UIGuildQureyListDLg::SetChooseItem(class UIGuildQureyListitem* Item)
{
	if(mbChooseItem)
	{
		mbChooseItem->SetBeChoose(FALSE);
	}
	mbChooseItem = Item;
	if (mbChooseItem)
	{
		mbChooseItem->SetBeChoose(TRUE);
	}
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIGuildCreateDLg, UDialog)
UBEGIN_MESSAGE_MAP(UIGuildCreateDLg, UDialog)
UON_UEDIT_UPDATE(0,&UIGuildCreateDLg::OnClickBtnOk)
UON_BN_CLICKED(1,&UIGuildCreateDLg::OnClickBtnOk)
UON_BN_CLICKED(2,&UIGuildCreateDLg::OnClickBtnCancel)
UEND_MESSAGE_MAP(); 
void UIGuildCreateDLg::StaticInit()
{

}
UIGuildCreateDLg::UIGuildCreateDLg()
{
	m_pkEdit = NULL;
}
UIGuildCreateDLg::~UIGuildCreateDLg()
{

}

BOOL UIGuildCreateDLg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	if (m_pkEdit == NULL)
	{
		m_pkEdit = UDynamicCast(UEdit, GetChildByID(0));
		if (!m_pkEdit)
		{
			return FALSE;
		}
		m_pkEdit->SetMaxLength(20);
		m_pkEdit->Clear();
	}
	for (int i = 4 ; i < 7 ;i++)
	{
		UStaticText* pTempStaticText = UDynamicCast(UStaticText, GetChildByID(i));
		if (pTempStaticText)
		{
			pTempStaticText->SetTextColor(UColor(119,142,148));
			pTempStaticText->SetTextAlign(UT_LEFT);
		}
	}
	m_bCanDrag = FALSE;
	m_bShowClose = FALSE;
	return TRUE;
}
void UIGuildCreateDLg::OnDestroy()
{
	UDialog::OnDestroy();
}
void UIGuildCreateDLg::OnClose()
{
	sm_System->GetCurFrame()->SetFocusControl();
	m_pkEdit->Clear();
	this->SetVisible(FALSE);
}
BOOL UIGuildCreateDLg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UIGuildCreateDLg::SetVisible(BOOL value)
{
	UDialog::SetVisible(value);
	if (value)
	{
		m_pkEdit->SetFocusControl();
	}
}
void UIGuildCreateDLg::OnClickBtnOk()
{
	char szName[64];
	m_pkEdit->GetText(szName, 64);
	string strName = szName;
	if(strName.size() && SocialitySys)
	{
		SocialitySys->GetGuildSysPtr()->SendMsgCreateGuild(strName.c_str());
	}
	OnClose();
}
void UIGuildCreateDLg::OnClickBtnCancel()
{
	OnClose();
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIGuildEmblem, UDialog)
UBEGIN_MESSAGE_MAP(UIGuildEmblem, UDialog)
UON_BN_CLICKED(0,&UIGuildEmblem::OnClickPrevStyleEmblem)
UON_BN_CLICKED(1,&UIGuildEmblem::OnClickPrevColorEmblem)
UON_BN_CLICKED(2,&UIGuildEmblem::OnClickNextStyleEmblem)
UON_BN_CLICKED(3,&UIGuildEmblem::OnClickNextColorEmblem)
UON_BN_CLICKED(4,&UIGuildEmblem::OnClickOk)
UON_BN_CLICKED(5,&UIGuildEmblem::OnClickCancel)
UEND_MESSAGE_MAP();
void UIGuildEmblem::StaticInit()
{

}
UIGuildEmblem::UIGuildEmblem()
{
	mICurrentColor = 0;
	mICurrentEmblem = 1;
	m_bShowClose = FALSE;
	m_IsUpdateChild = FALSE;
}
UIGuildEmblem::~UIGuildEmblem()
{

}
BOOL UIGuildEmblem::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	if(!SetEmblemTexture())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;
	return TRUE;
}
void UIGuildEmblem::OnDestroy()
{
	UDialog::OnDestroy();
}
BOOL UIGuildEmblem::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UIGuildEmblem::OnClose()
{
	SetVisible(FALSE);
	mICurrentColor = 0;
	mICurrentEmblem = 1;
	SetEmblemTexture();
}
void UIGuildEmblem::OnClickNextStyleEmblem()
{
	mICurrentEmblem++;
	if (mICurrentEmblem > 36)
	{
		mICurrentEmblem = 36;
	}else
	{
		mICurrentColor = 0;
	}
	SetEmblemTexture();
}
void UIGuildEmblem::OnClickPrevStyleEmblem()
{
	mICurrentEmblem--;
	if (mICurrentEmblem < 1)
	{
		mICurrentEmblem = 1;
	}else
	{
		mICurrentColor = 0;
	}
	SetEmblemTexture();
}
void UIGuildEmblem::OnClickNextColorEmblem()
{
	mICurrentColor++;
	if (mICurrentColor > 5)
	{
		mICurrentColor = 5;
	}
	SetEmblemTexture();
}
void UIGuildEmblem::OnClickPrevColorEmblem()
{
	mICurrentColor--;
	if (mICurrentColor < 0)
	{
		mICurrentColor = 0;
	}
	SetEmblemTexture();
}
void UIGuildEmblem::OnClickOk()
{
	if (SocialitySys)
	{
		ui64 guid = ObjectMgr->GetLocalPlayer()->GetGUID();
		SocialitySys->GetGuildSysPtr()->SendMsgGuildEmblem(guid, mICurrentEmblem, mICurrentColor);
	}
	OnClose();
}
void UIGuildEmblem::OnClickCancel()
{
	OnClose();
}
BOOL UIGuildEmblem::SetEmblemTexture()
{
	char buf[256];
	itoa(mICurrentEmblem /*+ 1*/, buf, 10);
	std::string str = buf;
	if (str.length() == 1)
	{
		str.insert(0,"0");
	}
	str += ('a' + mICurrentColor);
	sprintf(buf, "Data\\UI\\SocialitySystem\\GuildEmblem\\%s.png", str.c_str());
	UBitmap* pBitMap = UDynamicCast(UBitmap, GetChildByID(9));
	if (!pBitMap || !pBitMap->SetBitMapByStr(buf))
	{
		return FALSE;
	}
	UStaticText* pStaticText = UDynamicCast(UStaticText, GetChildByID(7));
	if (pStaticText)
	{
		//sprintf(buf, "样式%d", mICurrentEmblem/* + 1*/);
		std::string strRet = _TRAN(N_NOTICE_Z74, _I2A(mICurrentEmblem).c_str() );
		pStaticText->SetText(strRet.c_str());
	}
	pStaticText = UDynamicCast(UStaticText, GetChildByID(8));
	if (pStaticText)
	{
		//sprintf(buf, "颜色%d", mICurrentColor + 1);
		std::string strRet = _TRAN(N_NOTICE_Z75, _I2A(mICurrentColor + 1).c_str() );
		pStaticText->SetText(strRet.c_str());
	}
	return TRUE;
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UICastleBuyNpcItem, UControl)
UBEGIN_MESSAGE_MAP(UICastleBuyNpcItem, UControl)
UON_BN_CLICKED(3,&UICastleBuyNpcItem::OnClickBtnBuy)
UEND_MESSAGE_MAP();
UINT UICastleBuyNpcItem::NpcItemId = 0;
void UICastleBuyNpcItem::StaticInit()
{

}
UICastleBuyNpcItem::UICastleBuyNpcItem()
{
	m_NpcName = NULL;
	m_NpcGoldCost = NULL;
	m_NpcPointCost = NULL;
	m_NpcHeadImage = NULL;
	m_BuyNpcButton = NULL;
}
UICastleBuyNpcItem::~UICastleBuyNpcItem()
{

}
BOOL UICastleBuyNpcItem::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_NpcName == NULL)
	{
		m_NpcName = UDynamicCast(UStaticText, GetChildByID(0));
		if (!m_NpcName)
		{
			return FALSE;
		}
	}
	if (m_NpcGoldCost == NULL)
	{
		m_NpcGoldCost = UDynamicCast(UStaticText, GetChildByID(1));
		if (!m_NpcGoldCost)
		{
			return FALSE;
		}
	}
	if (m_BuyNpcButton == NULL)
	{
		m_BuyNpcButton = UDynamicCast(UBitmapButton, GetChildByID(3));
		if (!m_BuyNpcButton)
		{
			return FALSE;
		}
	}
	if (m_NpcHeadImage == NULL)
	{
		m_NpcHeadImage = UDynamicCast(UBitmap, GetChildByID(4));
		if (!m_NpcHeadImage)
		{
			return FALSE;
		}
	}
	if (m_NpcPointCost == NULL)
	{
		m_NpcPointCost = UDynamicCast(UStaticText, GetChildByID(2));
		if (!m_NpcPointCost)
		{
			return FALSE;
		}
	}
	ItemId = NpcItemId;
	NpcItemId++;
	return TRUE;
}
void UICastleBuyNpcItem::OnDestroy()
{
	NpcItemId = 0;
	return UControl::OnDestroy();
}

void UICastleBuyNpcItem::OnClickBtnBuy()
{
	CastleNpcList NpcState;
	if(SocialitySys->GetGuildSysPtr()->GetCastleNpcContent(ItemId, NpcState))
	{
		SocialitySys->GetGuildSysPtr()->SendMsgGuildBuyNpc(NpcState.uNpcEntry);
	}
}

void UICastleBuyNpcItem::Update()
{
	CastleNpcList NpcState;
	if(SocialitySys->GetGuildSysPtr()->GetCastleNpcContent(ItemId, NpcState))
	{
		m_BuyNpcButton->SetVisible(TRUE);

		m_NpcName->SetText(NpcState.Comments.c_str());
		//char buf[256];
		//sprintf(buf, "花费资金：%u金", NpcState.GoldCost );
		std::string strRet = _TRAN(N_NOTICE_Z76, _I2A(NpcState.GoldCost).c_str() );
		m_NpcGoldCost->SetText(strRet.c_str());
		//sprintf(buf, "花费部族积分：%u", NpcState.GuildPointCost);
		strRet = _TRAN(N_NOTICE_Z77, _I2A(NpcState.GuildPointCost).c_str() );
		m_NpcPointCost->SetText(strRet.c_str());

		if (SocialitySys->GetGuildSysPtr()->GetCastleNpcState(NpcState.uNpcEntry))
		{
			m_BuyNpcButton->SetActive(FALSE);
		}
		else
		{
			m_BuyNpcButton->SetActive(TRUE);
		}
	}
	else
	{
		m_NpcName->Clear();
		m_NpcGoldCost->Clear();
		m_NpcPointCost->Clear();
		m_BuyNpcButton->SetVisible(FALSE);
	}
}

UIMP_CLASS(UIGuildCastleNpcBuyDlg, UDialog)
UBEGIN_MESSAGE_MAP(UIGuildCastleNpcBuyDlg, UDialog)
UON_BN_CLICKED(1,&UIGuildCastleNpcBuyDlg::OnClickBtnPrevPage)
UON_BN_CLICKED(2,&UIGuildCastleNpcBuyDlg::OnClickBtnNextPage)
UEND_MESSAGE_MAP();
void UIGuildCastleNpcBuyDlg::StaticInit()
{

}
UIGuildCastleNpcBuyDlg::UIGuildCastleNpcBuyDlg()
{
	m_IsUpdateChild = FALSE;
}
UIGuildCastleNpcBuyDlg::~UIGuildCastleNpcBuyDlg()
{

}
BOOL UIGuildCastleNpcBuyDlg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;
	return TRUE;
}
void UIGuildCastleNpcBuyDlg::OnDestroy()
{
	return UDialog::OnDestroy();
}
BOOL UIGuildCastleNpcBuyDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UIGuildCastleNpcBuyDlg::OnClose()
{
	SetVisible(FALSE);
}

void UIGuildCastleNpcBuyDlg::OnClickBtnPrevPage()
{
	SocialitySys->GetGuildSysPtr()->SetCNPrevPage();
}

void UIGuildCastleNpcBuyDlg::OnClickBtnNextPage()
{
	SocialitySys->GetGuildSysPtr()->SetCNNextPage();
}

void UIGuildCastleNpcBuyDlg::Update()
{
	for (int i = 0 ; i < 10 ; i++)
	{
		UICastleBuyNpcItem* pCastleBuyNpcItem = UDynamicCast(UICastleBuyNpcItem, GetChildByID(3 + i));
		if (pCastleBuyNpcItem)
		{
			pCastleBuyNpcItem->Update();
		}
	}
}
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UIGuildLevelUpDlg, UDialog)
UBEGIN_MESSAGE_MAP(UIGuildLevelUpDlg, UDialog)
UON_BN_CLICKED(1,&UIGuildLevelUpDlg::OnClickLevelUp)
UON_BN_CLICKED(2,&UIGuildLevelUpDlg::OnClickCancel)
UEND_MESSAGE_MAP();
void UIGuildLevelUpDlg::StaticInit()
{

}

UIGuildLevelUpDlg::UIGuildLevelUpDlg()
{

}

UIGuildLevelUpDlg::~UIGuildLevelUpDlg()
{

}

void UIGuildLevelUpDlg::Show(uint32 uilevel)
{
	SetVisible(TRUE);
	uint32 level = uilevel + 1;
	CGuildLevelUpDB::stGuildLevelUpInfo info;
	UControl* pItemContent = GetChildByID(7);
	UStaticText* pNoNeedItem = UDynamicCast(UStaticText, GetChildByID(8));
	if(!pItemContent || !pNoNeedItem)
		return;
	pNoNeedItem->SetTextColor("34190e");
	if(g_pkGuildLevelUpDB->GetGuildLevelUpInfo(level, info))
	{
		USkillButton::DataInfo Tempdata;
		if( info.require_item_entry )
		{
			pItemContent->SetVisible(TRUE);
			pNoNeedItem->SetVisible(FALSE);
			USkillButton* pSBtn = UDynamicCast(USkillButton, pItemContent->GetChildByID(8));
			if (pSBtn)
			{
				Tempdata.id = info.require_item_entry;
				Tempdata.pos = 0;
				Tempdata.type = UItemSystem::ITEM_DATA_FLAG;
				Tempdata.num = 1;
				pSBtn->SetStateData(&Tempdata);
			}
			UStaticText* pText = UDynamicCast(UStaticText, pItemContent->GetChildByID(0));
			ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(info.require_item_entry);
			if (pText && ItemInfo)
			{
				pText->SetText(ItemInfo->C_name.c_str());
				pText->SetTextAlign(UT_LEFT);
				pText->SetTextColor("FFFFFF");
				pText->SetTextShadow(true);
			}
		}else
		{
			pNoNeedItem->SetVisible(TRUE);
			pItemContent->SetVisible(FALSE);
		}
		char buf[256];
		UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(4));
		if (pText)
		{
			pText->SetText(itoa( info.require_score, buf, 10));
		}
		pText = UDynamicCast(UStaticText, GetChildByID(5));
		if (pText)
		{
			pText->SetText(itoa( info.require_gold, buf, 10));
		}
		pText = UDynamicCast(UStaticText, GetChildByID(6));
		if (pText)
		{
			pText->SetText(itoa( info.require_member_min, buf, 10));
		}

		pText = UDynamicCast(UStaticText, GetChildByID(3));
		if (pText)
		{
			pText->SetText(_TRAN(N_NOTICE_C95, _I2A(level).c_str()).c_str());
		}

		URichTextCtrl* pRichText = UDynamicCast(URichTextCtrl, GetChildByID(9));
		if (pRichText)
		{
			pRichText->SetText(info.How_Get.c_str(), info.How_Get.length());
		}
	}
}

BOOL UIGuildLevelUpDlg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	USkillButton::DataInfo Tempdata;
	USkillButton* pSBtn = UDynamicCast(USkillButton, GetChildByID(7)->GetChildByID(8));
	if (pSBtn)
	{
		Tempdata.id = 0;
		Tempdata.pos = 0;
		Tempdata.type = UItemSystem::ITEM_DATA_FLAG;
		Tempdata.num = 0;

		pSBtn->SetStateData(&Tempdata);
	}

	for(int i = 0 ; i < 5 ; ++i)
	{
		UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(50 + i));
		if (pText)
		{
			pText->SetTextColor("34190e");
			pText->SetTextAlign(UT_LEFT);
		}
	}
	UStaticText * pText = UDynamicCast(UStaticText, GetChildByID(4));
	if (pText)
	{
		pText->SetTextColor("FFFFFF");
		pText->SetTextAlign(UT_RIGHT);
	}
	pText = UDynamicCast(UStaticText, GetChildByID(5));
	if (pText)
	{
		pText->SetTextColor("FFFFFF");
		pText->SetTextAlign(UT_RIGHT);
	}
	pText = UDynamicCast(UStaticText, GetChildByID(6));
	if (pText)
	{
		pText->SetTextColor("FFFFFF");
		pText->SetTextAlign(UT_RIGHT);
	}
	return TRUE;
}

void UIGuildLevelUpDlg::OnDestroy()
{
	OnClose();
	UDialog::OnDestroy();
}

BOOL UIGuildLevelUpDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}

void UIGuildLevelUpDlg::OnClose()
{
	SetVisible(FALSE);
}

void UIGuildLevelUpDlg::OnClickLevelUp()
{
	SocialitySys->GetGuildSysPtr()->SendMsgGuildLevelUp();
	OnClose();
}

void UIGuildLevelUpDlg::OnClickCancel()
{
	OnClose();
}
//////////////////////////////////////////////////////////////////////////
GuildSys::GuildSys():mGuildFrame(NULL),m_bSelectItem(NULL),mGuildQureyListDlg(NULL),mGuildEmblem(NULL),mCastleBuyNpcDlg(NULL)
{
	mGuildCreateDlg = NULL;
	mGuildLevelUpDlg = NULL;
	mIsMotdEvent = FALSE;
	LocalRankId = 0;
	mCastleNpcPage = 0;
	mCastleNpcMaxPage =0;
	LocalGuildId = 0;

	m_CurrentPage = 0;
	m_MaxPage= 0; 
	mGuildLevel = 1;
}
GuildSys::~GuildSys()
{
	mGuildFrame = NULL;
	m_bSelectItem = NULL;
	mGuildLevelUpDlg = NULL;
	mCastleNpcPage = 0;
	mCastleNpcMaxPage =0;
	LocalGuildId = 0;

	m_CurrentPage = 0;
	m_MaxPage= 0; 
}

BOOL GuildSys::CreateFrame(UControl * Ctrl)
{
	UControl* NewCtrl = NULL;
	if (mGuildFrame == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\UGuild.udg");
		mGuildFrame = UDynamicCast(UIGuildFrame, NewCtrl);
		if (mGuildFrame)
		{
			Ctrl->AddChild(mGuildFrame);
			mGuildFrame->SetId(FRAME_IG_GUILDDLG);
		}
		else
		{
			return FALSE;
		}
	}
	NewCtrl = NULL;
	if (mGuildCreateDlg == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\GuildCreateDlg.udg");
		mGuildCreateDlg = UDynamicCast(UIGuildCreateDLg, NewCtrl);
		if (mGuildCreateDlg)
		{
			Ctrl->AddChild(mGuildCreateDlg);
			mGuildCreateDlg->SetId(FRAME_IG_CREATEGUILDDLG);
		}
		else
		{
			return FALSE;
		}
	}
	if (mGuildQureyListDlg == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\Guildslist.udg");
		mGuildQureyListDlg = UDynamicCast(UIGuildQureyListDLg, NewCtrl);
		if (mGuildQureyListDlg)
		{
			Ctrl->AddChild(mGuildQureyListDlg);
			mGuildQureyListDlg->SetId(FRAME_IG_GUILDSLISTDLG);
		}
		else
		{
			return FALSE;
		}
	}
	if (mGuildEmblem == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\UGuildEmblem.udg");
		mGuildEmblem = UDynamicCast(UIGuildEmblem, NewCtrl);
		if (mGuildEmblem)
		{
			Ctrl->AddChild(mGuildEmblem);
			mGuildEmblem->SetId(FRAME_IG_GUILDEMBLEM);
		}
		else
		{
			return FALSE;
		}
	}
	if (mCastleBuyNpcDlg == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\UCastleBuyNpcDlg.udg");
		mCastleBuyNpcDlg = UDynamicCast(UIGuildCastleNpcBuyDlg, NewCtrl);
		if (mCastleBuyNpcDlg)
		{
			Ctrl->AddChild(mCastleBuyNpcDlg);
			mCastleBuyNpcDlg->SetId(FRAME_IG_CASTLEBUYNPCDLG);
		}else
		{
			return FALSE;
		}
	}
	if(mGuildLevelUpDlg == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\GuildLevelUp.udg");
		mGuildLevelUpDlg = UDynamicCast(UIGuildLevelUpDlg, NewCtrl);
		if(mGuildLevelUpDlg)
		{
			Ctrl->AddChild(mGuildLevelUpDlg);
			mCastleBuyNpcDlg->SetId(FRAME_IG_GUILDLEVELUPDLG);
		}else
		{
			return FALSE;
		}
	}
	return TRUE;
}
void GuildSys::DestoryFrame()
{
	LocalGuildId = 0;
	mGuildMap.clear();
}
void GuildSys::ShowFrame()
{
	mGuildFrame->Show();
	SendMsgGuildQuery();
}
void GuildSys::HideFrame()
{
	mGuildFrame->Hide();
}
void GuildSys::ParseMsgMember(const std::vector<MSG_S2C::stGuild_Roster::stMember> & vMember)
{	
	mvMember = vMember;
	m_MaxPage = mvMember.size() / 17;
	//for (int i= 0 ; i < 200;  i++)
	//{
	//	MSG_S2C::stGuild_Roster::stMember NewMember;
	//	NewMember.bOnline = false;
	//	NewMember.Class = 1;
	//	NewMember.contribution_points = 0;
	//	NewMember.gender = 0;
	//	NewMember.high_guid = 0;
	//	NewMember.last_level = 200;
	//	NewMember.lastOnline = 0;
	//	char buf[256];
	//	sprintf(buf,"测试工会人%d", i);
	//	NewMember.name = buf;
	//	NewMember.rank_id = 1;
	//	NewMember.player_id = 1000000 + i;
	//	mvMember.push_back(NewMember);
	//}
	std::vector<MSG_S2C::stGuild_Roster::stMember>::const_iterator it = mvMember.begin();
	std::vector<ui64> sortMember;
	CPlayer* pkplayer = ObjectMgr->GetLocalPlayer();

	UIGuildList* pGuildList = mGuildFrame->GetGuildList();
	if (pGuildList)
	{	
		pGuildList->ClearList();
		while(it != mvMember.end())
		{
			if (pkplayer->GetGUID() == it->player_id)
			{
				SetLocalRankId(it->rank_id);
				break;
			}
			++it;
		}
		SortMemberByRank();
		pGuildList->UpdataList();
	}
}
void GuildSys::ParseMsgRankName(const std::vector<std::string> & vRank)
{
	m_RankName = vRank;
	mGuildFrame->BeginRankList();
	std::vector<std::string>::const_iterator it = vRank.begin();
	while (it != vRank.end())
	{
		mGuildFrame->ValueRankList(it->c_str());
		++it;
	}
	mGuildFrame->EndRankList();
}
void GuildSys::ParseMsgRankRight(const std::vector<MSG_S2C::stGuild_Roster::stBank> & vRankRight)
{
	mRankRight.clear();
	for (UINT i = 0 ; i < vRankRight.size() ; i++)
	{
		mRankRight.push_back(vRankRight[i].iRights);
	}
	mGuildFrame->EndRankList();
}
void GuildSys::ParseMsgGuildEvent(ui8 Event, std::vector<std::string> &vStr)
{
	switch(Event)
	{
	case GUILD_EVENT_PROMOTION://提升 提升人的名字,被提升人的名字,新等级
		{
		}
		break;
	case GUILD_EVENT_DEMOTION://降低 降低人的名字,被降低人的名字,新等级
		{
		}
		break;
	case GUILD_EVENT_MOTD://
		{
		}
		break;
	case GUILD_EVENT_JOINED://加入工会
		{
			if (vStr.size())
			{
				std::string strRet = _TRAN(N_NOTICE_Z78, vStr[0].c_str());
				ClientSystemNotify( strRet.c_str() );//"[部族事件]：[%s]加入了部族", vStr[0].c_str());
			}	
			SendMsgGuildQuery();
			SendMsgGuildRoster();
			SendMsgQureyCastleState();
		}
		break;
	case GUILD_EVENT_LEFT://离开公会
		{
			if (vStr.size())
			{
				ui64 playerGuid;
				if (GetGUIDFormName(vStr[0].c_str(), playerGuid))
				{
					std::vector<MSG_S2C::stGuild_Roster::stMember>::iterator it =  mvMember.begin();
					while(it != mvMember.end())
					{
						if (it->player_id == playerGuid)
						{
							mvMember.erase(it);
							mGuildFrame->GetGuildList()->UpdataList();
							break;
						}
						++it;
					}
				}
				std::string strRet = _TRAN(N_NOTICE_Z79, vStr[0].c_str());
				ClientSystemNotify(strRet.c_str());//"[部族事件]：部族成员[%s]离开了部族", vStr[0].c_str());
			}
		}
		break;
	case GUILD_EVENT_REMOVED://移除会员
		{
			if (vStr.size())
			{
				ui64 playerGuid;
				if (GetGUIDFormName(vStr[0].c_str(), playerGuid))
				{
					std::vector<MSG_S2C::stGuild_Roster::stMember>::iterator it =  mvMember.begin();
					while(it != mvMember.end())
					{
						if (it->player_id == playerGuid)
						{
							mvMember.erase(it);
							mGuildFrame->GetGuildList()->UpdataList();
							break;
						}
						++it;
					}
					//mGuildFrame->GetGuildList()->RemoveMember(playerGuid);
				}

				std::string strRet = _TRAN(N_NOTICE_Z80, vStr[0].c_str());
				ClientSystemNotify(strRet.c_str());//"[部族事件]：部族成员[%s]被移出部族", vStr[0].c_str());
			}
		}
		break;
	case GUILD_EVENT_LEADER_IS://会长是
		{
		}
		break;
	case GUILD_EVENT_LEADER_CHANGED: //会长更换
		{
		}
		break;
	case GUILD_EVENT_DISBANDED: //工会解散
		{
			ClientSystemNotify(_TRAN("[部族事件]：您所在的部族已经解散！"));
			ParseMsgGuildDisBand();
			m_GuildName.clear();
		}
		break;
	case GUILD_EVENT_TABARDCHANGE:
		break;
	case GUILD_EVENT_UNK1:
		break;
	case GUILD_EVENT_UNK2:
		break;
	case GUILD_EVENT_HASCOMEONLINE: 
		{
			if (vStr.size())
			{
				ui64 playerGuid;
				if (GetGUIDFormName(vStr[0].c_str(), playerGuid))
				{
					mGuildFrame->GetGuildList()->SetMemberOnline(playerGuid);
				}

				std::string strRet = _TRAN(N_NOTICE_Z81, vStr[0].c_str(), vStr[0].c_str());
				ClientSystemNotify( strRet.c_str() );//"[部族事件]：部族成员<a:#Name_%s>[%s]</a>进入了游戏", vStr[0].c_str(), vStr[0].c_str());
			}
		}
		break;
	case GUILD_EVENT_HASGONEOFFLINE:
		{
			if (vStr.size())
			{
				ui64 playerGuid;
				if (GetGUIDFormName(vStr[0].c_str(), playerGuid))
				{
					mGuildFrame->GetGuildList()->SetMemberOffline(playerGuid);
				}

				std::string strRet = _TRAN(N_NOTICE_Z82, vStr[0].c_str());
				ClientSystemNotify( strRet.c_str() );//"[部族事件]：部族成员[%s]离开了游戏", vStr[0].c_str());
			}
		}
		break;
	case GUILD_EVENT_BANKTABBOUGHT:
		break;
	case GUILD_EVENT_SETNEWBALANCE:
		break;
	case GUILD_EVENT_SETRANK://第一个是修改别人的人的名字，第二个是被修改的名字，第三个是被提升为XX职位的名称。
		{
			ui64 playerGuid;
			if (GetGUIDFormName(vStr[1].c_str(), playerGuid))
			{
				unsigned int ui = 0;
				for (ui = 0 ; ui < m_RankName.size() ; ui++)
				{
					if(m_RankName[ui].compare(vStr[2]) == 0)
					{
						break;
					}
					//if (strcmp(m_RankName[ui].c_str(), vStr[2].c_str()) == 0)
					//{
					//	break;
					//}
				}
				mGuildFrame->GetGuildList()->SetMemberRank(playerGuid, ui);
				std::string SetRank;
				CPlayer* pkplayer = ObjectMgr->GetLocalPlayer();
				if (pkplayer)
				{
					if (pkplayer->GetName().Equals(vStr[1].c_str()))
					{

						SetLocalRankId(ui);
						//LocalRankId = ui;
						SetRank = _TRAN(N_NOTICE_C86,vStr[0].c_str(),vStr[2].c_str());
						//SetRank ="[部族事件]：" + vStr[0] + "修改您的阶位为" + vStr[2];
					}
					else
					{
						if (pkplayer->GetName().Equals(vStr[0].c_str()))
						{
							SetRank = _TRAN(N_NOTICE_C87, vStr[1].c_str(),vStr[2].c_str());
							//SetRank = "您修改" + vStr[1] + "的阶位为" + vStr[2];
						}
						else
						{
							SetRank = _TRAN(N_NOTICE_C88,vStr[0].c_str(), vStr[1].c_str(),vStr[2].c_str());
							//SetRank = vStr[0] + "修改" + vStr[1] + "的阶位为" + vStr[2];
						}
					}
					ClientSystemNotify(SetRank.c_str());
				}
			}
		}
		break;
	case GUILD_EVENT_SET_INFORMATION:
		{
			if (vStr.size())
			{
				m_GuildMod = vStr[0];
				mGuildFrame->SetGuildInfo(m_GuildMod);
				ChatMsgNotify(CHAT_MSG_GUILDINFO, m_GuildMod.c_str());
			}
			else{
				mIsMotdEvent = TRUE;
			}
		}
		break;
	}
}
void GuildSys::ParseMsgGuildName(ui32 GuildID, const char * GuildName)
{
	if (mGuildFrame && GuildID == ObjectMgr->GetLocalPlayer()->GetUInt32Value(PLAYER_GUILDID))
	{
		mGuildFrame->SetGuildName(GuildName);
		m_GuildName = GuildName;
	}
	
	const stdext::hash_set<UpdateObj*>& temp( ObjectMgr->GetObjectByType(TYPEID_PLAYER) );

	for( stdext::hash_set<UpdateObj*>::const_iterator it = temp.begin(); it != temp.end(); ++it )
	{
		ui32 guild_id = (*it)->GetUInt32Value(PLAYER_GUILDID);
		if (guild_id == GuildID)
		{
			((CPlayer*)*it)->SetGuildName(GuildName);
		}
	}
}

void GuildSys::ParseMsgGuildQueryResponse(MSG_S2C::stGuild_Query_Response &msg)
{
	std::map<ui32, GuildRecode>::iterator it = mGuildMap.find(msg.guild_id);
	if (it != mGuildMap.end())
	{
		it->second.emblemColor = msg.emblemColor;
		it->second.emblemStyle = msg.emblemStyle;
		it->second.Name = msg.guild_name.c_str();
		it->second.guild_id = msg.guild_id;
	}
	else
	{
		GuildRecode kGuildRecode;
		kGuildRecode.emblemColor = msg.emblemColor;
		kGuildRecode.emblemStyle = msg.emblemStyle;
		kGuildRecode.Name = msg.guild_name.c_str();
		kGuildRecode.guild_id = msg.guild_id;
		mGuildMap.insert(std::map<ui32, GuildRecode>::value_type(msg.guild_id, kGuildRecode));
	}

	if (LocalGuildId && msg.guild_id == LocalGuildId)
	{
		ParseMsgRankName(msg.vBanks);
		ParseMsgGuildName(msg.guild_id, msg.guild_name.c_str());
		ParseMsgGuildQuery(msg.level, msg.score, msg.war_score, msg.alliance_guilds, msg.lead_guild_id);
		ParseGuildEmblem(msg.emblemStyle, msg.emblemColor);
	}
}

void GuildSys::ParseMsgGuildQuery(uint8 level, ui32 Score, ui32 War_Score,  std::map<uint32, std::string> AlienguildMap, uint32 lead_guild_id)
{
	if (mGuildFrame)
	{
		mGuildLevel = level;
		mGuildFrame->SetGuildLevel(level);
		mGuildFrame->SetGuildScore(Score);
		mGuildFrame->SetGuildWarScore(War_Score);
	}
}

void GuildSys::ParseMsgGuildList(std::vector<MSG_S2C::stGuildListInfoAck::guild_info_t> &guildlist)
{
	if (mGuildQureyListDlg)
	{
		mvGuild = guildlist;
		mGuildQureyListDlg->SetVisible(TRUE);
		mGuildQureyListDlg->OnGetGuildList(guildlist);
	}
}

void GuildSys::ParseMsgGuildContributionPoints(uint32 player_id, uint32 points, uint32 guild_score)
{
	if (mGuildFrame)
	{
		mGuildFrame->GetGuildList()->SetMemberContributionPoints(player_id, points);
		mGuildFrame->SetGuildScore(guild_score);
	}
}

void GuildSys::ParseUpLoadGuildEmblem()
{
	if (mGuildEmblem)
	{
		mGuildEmblem->SetVisible(TRUE);
	}
}

void GuildSys::ParseGuildEmblem(uint32 emblemStyle, uint32 emblemColor)
{
	char buf[256];
	if (emblemStyle)
	{
		itoa(emblemStyle /*+ 1*/, buf, 10);
		std::string str = buf;
		if (str.length() == 1)
		{
			str.insert(0,"0");
		}
		str += ('a' + emblemColor);
		sprintf(buf, "Data\\UI\\SocialitySystem\\GuildEmblem\\%s.png", str.c_str());

		if (mGuildFrame)
		{
			mGuildFrame->SetGuildEmblemBitMap(buf);
		}
	}
	else
	{

		if (mGuildFrame)
		{
			mGuildFrame->SetGuildEmblemBitMap("Data\\UI\\SocialitySystem\\DefaultguildMask.png");
		}
	}


}
void GuildSys::ParseGuildDeclareWarNotify(std::string &ActiveGuild, std::string &PassiveGuild)
{
	if (m_GuildName.compare(ActiveGuild) == 0)
	{
		std::string strRet = _TRAN(N_NOTICE_Z83, PassiveGuild.c_str());
		ChatMsgNotify(CHAT_MSG_SYSTEM, strRet.c_str() );//"部族向[%s]宣战了，大家做好战斗准备。", PassiveGuild.c_str());
	}
	else
	{
		std::string strRet = _TRAN(N_NOTICE_Z84, ActiveGuild.c_str());
		ChatMsgNotify(CHAT_MSG_SYSTEM, strRet.c_str());//"[%s]向我们部族宣战了，大家做好战斗准备。", ActiveGuild.c_str());
	}
}

void GuildSys::ParseGuildCastleNpcBuy(std::set<uint32> &buy_npcs, uint32 map_id)
{
	m_BuyNpc = buy_npcs;
	m_Castlemapid = map_id;

	mCastleBuyNpcDlg->Update();
	mCastleBuyNpcDlg->SetVisible(TRUE);
}

void GuildSys::ParseGuildCastleState(std::vector<MSG_S2C::stCastleStateAck::castle_state> &vCastlestate, uint32 server_time)
{
	m_vGuildCastleState = vCastlestate;
}

void GuildSys::ParseGuildCastleCountDown(std::string CastleName, uint32 mapid, uint8 next_state, uint8 count_down_mins)
{
	UISystemMsg* pSGN = INGAMEGETFRAME(UISystemMsg, FRAME_IG_SYSGIFTNOTIFY);
	if (pSGN)
	{
		std::string state= "";
		switch(next_state)// 2 ready 3 fight 0 end
		{
		case 0:
			state = _TRAN("[结束]");
			break;
		case 2:
			state = _TRAN("[准备]");
			break;
		case 3:
			state = _TRAN("[开战]");
			break;
		default:
			break;
		}
		//sprintf(buf, "还有%d分钟%s进入%s阶段", count_down_mins, CastleName.c_str(), state.c_str());
		std::string strRet = _TRAN(N_NOTICE_Z85, _I2A(count_down_mins).c_str(), CastleName.c_str(), state.c_str());
		pSGN->GetSystemMsg(strRet.c_str(), 10.0f);
		state = strRet;
		ClientSystemNotify( state.c_str() );
	}
}

void GuildSys::ParseMsgGuildInfo(const char * GuildInfo)
{
	if (mGuildFrame)
	{
		m_GuildMod = GuildInfo;
		//if (mIsMotdEvent)
		//{
		//	std::string strMotd = m_GuildMod + "<br>";
		//	ChatSystem->AppendChatMsg(CHAT_MSG_GUILDINFO, strMotd.c_str());
		//}
		mGuildFrame->SetGuildInfo(std::string(GuildInfo));
	}
}
void GuildSys::ParseMsgGuildCommandResault(uint32 iCmd, std::string &szMsg, uint32 iType)
{

}
void GuildSys::ParseMsgGuildCreate()
{
	if (mGuildCreateDlg)
	{
		mGuildCreateDlg->SetVisible(TRUE);
	}
}
void GuildSys::ParseMsgGuildLevelUp(uint32 level)
{
	if(mGuildLevelUpDlg)
	{
		mGuildLevelUpDlg->Show(level);
	}
}
void GuildSys::ParseMsgGuildDisBand()
{
	mGuildFrame->SetGuildDisband();
}
void GuildSys::SendMsgRemoveMember(const char* Name)
{
	PacketBuilder->SendRemoveMem(Name);
}
void GuildSys::SendMsgInviteMember(const char* Name)
{
	PacketBuilder->SendGuildInvite(Name);
}
void GuildSys::SendMsgGuildRoster()
{
	PacketBuilder->SendGuildRoster();
}
void GuildSys::SendMsgLeaveGuild()
{
	PacketBuilder->SendLeaveGuild();
}
void GuildSys::SendMsgSetGuildRank(ui32 rankid,const char * rankName,ui32 IRight)
{
	if (rankid < 0 || rankid >= (int)mRankRight.size())
	{
		return;
	}
	if(mRankRight[rankid] != IRight)
	{
		PacketBuilder->SendSetRank(rankid, m_RankName[rankid].c_str(), IRight);
	}
}
void GuildSys::SendMsgChangeMemRank(const char* Name, uint8 Rankid)
{
	PacketBuilder->SendGuildMemRank(Name, Rankid);
}
void GuildSys::SendMsgDeclareWar(ui32 GuildGuid)
{
	PacketBuilder->SendGuildDeclareWar(GuildGuid);
}
void GuildSys::SendMsgGuildAlien(ui32 GuildGuid)
{
	PacketBuilder->SendGuildAlien(GuildGuid);
}
void GuildSys::SendMsgGuildEmblem(uint64 guid, ui32 EmblemStyle, ui32 EmblemColor)
{
	PacketBuilder->SendGuildEmblem(guid, EmblemStyle, EmblemColor);
}
void GuildSys::SendMsgCreateGuild(const char* Name)
{
	if (!strlen(Name))
	{
		UMessageBox::MsgBox_s(_TRAN("请输入名字!"));
	}
	if(GetNameFliter(Name))
	{
		PacketBuilder->SendCreateGuild(Name);
	}
}
void GuildSys::SendMsgQureyCastleState()
{
	PacketBuilder->SendCastleQueryState();
}
void GuildSys::SendMsgGuildBuyNpc(uint32 npc_id)
{
	PacketBuilder->SendCastleBuyNpc(npc_id, m_Castlemapid);
}
void GuildSys::SendMsgSaveGuildInfo(const char* Info)
{
	std::string strInfo = Info;
	if (!g_pkChatFilterDB->GetFilterChatMsg(std::string(Info)))
	{
		PacketBuilder->SendGuildInfo(strInfo);
	}
	else
	{
		UMessageBox::MsgBox_s(_TRAN("您的输入中带有非法字符！"));
	}	
}

void GuildSys::SendMsgGuildLevelUp()
{
	PacketBuilder->SendGuildLevelUp();
}

void GuildSys::SendMsgGuildQuery()
{
	CPlayer * pkPlayer = ObjectMgr->GetLocalPlayer();
	if (pkPlayer)
	{
		ui32 guild_id = pkPlayer->GetUInt32Value(PLAYER_GUILDID);
		PacketBuilder->SendGuildQuery(guild_id);
	}
}

void GuildSys::SendMsgGuildQuery(ui32 guild_id)
{
	PacketBuilder->SendGuildQuery(guild_id);
}

void GuildSys::SetLocalGuildId(ui32 guild_Id)
{
	LocalGuildId = guild_Id;
}

ui32 GuildSys::SetRankRightVis(bool IsVis, int RankId, int RankVis)
{
	if (RankId < 0 || RankId >= (int)mRankRight.size())
	{
		return 0;
	}
	if (IsVis)
	{
		mRankRight[RankId] |= RankVis;
	}
	else
	{
		mRankRight[RankId] &= ~RankVis;
	}
	return mRankRight[RankId];
}

bool GuildSys::GetRankRightVis(int RankId, int RankVis)
{
	if (RankId < 0 || RankId >= (int)mRankRight.size())
	{
		return false;
	}
	if (mRankRight[RankId] & RankVis)
	{
		return true;
	}
	return false;
}

void GuildSys::SetLocalRankId(ui32 RankId)
{
	LocalRankId = RankId;
	UControl* pEdit = mGuildFrame->GetChildByID(53);
	if (!LocalRankId)
	{
		if (pEdit)
		{
			pEdit->SetActive(TRUE);
		}

		UControl*pButton = mGuildQureyListDlg->GetChildByID(19);
		if (pButton)
		{
			pButton->SetActive(TRUE);
		}

		pButton = mGuildQureyListDlg->GetChildByID(20);
		if (pButton)
		{
			pButton->SetActive(TRUE);
		}

		pButton = mGuildFrame->GetChildByID(46);
		if (pButton)
		{
			pButton->SetActive(TRUE);
		}
	}
	else
	{
		if (pEdit)
		{
			pEdit->SetActive(FALSE);
		}

		UControl*pButton = mGuildQureyListDlg->GetChildByID(19);
		if (pButton)
		{
			pButton->SetActive(FALSE);
		}

		pButton = mGuildQureyListDlg->GetChildByID(20);
		if (pButton)
		{
			pButton->SetActive(FALSE);
		}

		pButton = mGuildFrame->GetChildByID(46);
		if (pButton)
		{
			pButton->SetActive(FALSE);
		}
	}
	if (mRankRight.size())
	{
		if (GetRankRightVis(LocalRankId, GR_RIGHT_INVITE))
		{
			UControl*pButton = mGuildFrame->GetChildByID(43);
			if (pButton)
			{
				pButton->SetActive(TRUE);
			}
		}
		else
		{
			UControl*pButton = mGuildFrame->GetChildByID(43);
			if (pButton)
			{
				pButton->SetActive(FALSE);
			}
		}
		if (GetRankRightVis(LocalRankId, GR_RIGHT_REMOVE))
		{
			UControl*pButton = mGuildFrame->GetChildByID(42);
			if (pButton)
			{
				pButton->SetActive(TRUE);
			}
		}
		else
		{
			UControl*pButton = mGuildFrame->GetChildByID(42);
			if (pButton)
			{
				pButton->SetActive(FALSE);
			}
		}
		if (GetRankRightVis(LocalRankId, GR_RIGHT_EGUILDINFO))
		{
			UControl* pCtrl = mGuildFrame->GetChildByID(9);
			if (pCtrl)
			{
				pCtrl->SetActive(TRUE);
			}
		}
		else
		{
			UControl*pCtrl = mGuildFrame->GetChildByID(9);
			if (pCtrl)
			{
				pCtrl->SetActive(FALSE);
			}
		}
		if (GetRankRightVis(LocalRankId, GR_RIGHT_DECLARE_WAR))
		{
			UControl*pButton = mGuildQureyListDlg->GetChildByID(18);
			if (pButton)
			{
				pButton->SetActive(TRUE);
			}
		}
		else
		{
			UControl*pButton = mGuildQureyListDlg->GetChildByID(18);
			if (pButton)
			{
				pButton->SetActive(FALSE);
			}
		}
		if (GetRankRightVis(LocalRankId, GR_RIGHT_FENGYIN))
		{
		}
			//if (GetRankRightVis(LocalRankId, GR_RIGHT_INVITE))
			//{
			//}
	}
}

ui32 GuildSys::GetLocalRankId()
{
	return LocalRankId;
}

bool GuildSys::GetRankIdByGuid(ui64 guid, ui32 &RankId)
{
	for (unsigned int ui = 0 ; ui < mvMember.size() ; ui++)
	{
		if (mvMember[ui].player_id == guid)
		{
			RankId = mvMember[ui].rank_id;
			return true;
		}
	}
	return false;
}

bool GuildSys::GetGuildName(ui32 guild_id, std::string &Name)
{
	std::map<ui32, GuildRecode>::iterator it = mGuildMap.find(guild_id);
	if (it != mGuildMap.end())
	{
		Name = it->second.Name;
		return true;
	}
	return false;
}

bool GuildSys::GetGuildEmblem(ui32 guild_id, ui32& EmblemStyle, ui32& EmblemColor)
{
	std::map<ui32, GuildRecode>::iterator it = mGuildMap.find(guild_id);
	if (it != mGuildMap.end())
	{
		EmblemStyle = it->second.emblemStyle;
		EmblemColor = it->second.emblemColor;
		return true;
	}
	return false;
}

void GuildSys::OnGuildCastleInfo()
{
	std::string castleName;
	if(SocialitySys->GetGuildSysPtr()->GetIsGuildGotCastle(LocalGuildId, castleName))
	{
		std::string strRet = _TRAN(N_NOTICE_Z86, castleName.c_str() );
		ClientSystemNotify( strRet.c_str() );//"拥有城池[%s]", castleName.c_str() );
	}
}

bool GuildSys::GetIsGuildGotCastle(ui32 guild_id, std::string& CastleName)
{
	for (UINT ui = 0 ; ui < m_vGuildCastleState.size(); ui++)
	{
		if (guild_id == m_vGuildCastleState[ui].owner_guild)
		{
			CastleName = m_vGuildCastleState[ui].castle_name;
			return true;
		}
	}
	return false;
}

void GuildSys::SetSelectItem(class UGuildListItem* item)
{
	//注意 有可能控件已经被删除
	if (m_bSelectItem)
	{
		m_bSelectItem->SetBeChoose(FALSE);
	}
	if (item)
	{
		m_bSelectItem = item;
		m_bSelectItem->SetBeChoose(TRUE);
	}
	else
	{
		m_bSelectItem = NULL;
	}
}

void GuildSys::SetGuildScore(ui32 guildScore)
{
	if (mGuildFrame)
	{
		mGuildFrame->SetGuildScore(guildScore);
	}
}

void GuildSys::DelMember()
{
	std::string removeMemberName;
	if(m_bSelectItem && m_bSelectItem->GetName(removeMemberName))
	{
		SendMsgRemoveMember(removeMemberName.c_str());
	}
}
void GuildSys::AcceptDelMember()
{
	if (SocialitySys)
	{
		SocialitySys->GetGuildSysPtr()->DelMember();
	}
}
void GuildSys::DeclineDelMember()
{

}
void GuildSys::RemoveSelect()
{
	if (m_bSelectItem)
	{
		UMessageBox::MsgBox(_TRAN("确定踢出该会员么？"), 
			(BoxFunction*)GuildSys::AcceptDelMember,
			(BoxFunction*)GuildSys::DeclineDelMember);
	}
}
bool KeyRank(const MSG_S2C::stGuild_Roster::stMember& A, const MSG_S2C::stGuild_Roster::stMember& B)
{
	return (A.rank_id <= B.rank_id);
}
bool KeyGender(const MSG_S2C::stGuild_Roster::stMember& A, const MSG_S2C::stGuild_Roster::stMember& B)
{
	return (A.gender >= B.gender);
}
bool KeyLevel(const MSG_S2C::stGuild_Roster::stMember& A, const MSG_S2C::stGuild_Roster::stMember& B)
{
	return (A.last_level >= B.last_level);
}
bool KeyClass(const MSG_S2C::stGuild_Roster::stMember& A, const MSG_S2C::stGuild_Roster::stMember& B)
{
	return (A.Class <= B.Class);
}
bool KeyContribution(const MSG_S2C::stGuild_Roster::stMember& A, const MSG_S2C::stGuild_Roster::stMember& B)
{
	return (A.contribution_points >= B.contribution_points);
}
bool KeyOnline(const MSG_S2C::stGuild_Roster::stMember& A, const MSG_S2C::stGuild_Roster::stMember& B)
{
	return (A.bOnline > B.bOnline);
}
bool KeyName(const MSG_S2C::stGuild_Roster::stMember& A, const MSG_S2C::stGuild_Roster::stMember& B)
{
	return (A.name < B.name );
}
#define SORTGUILDMEM(FUNCPTR, vec) std::sort(mvMember.begin(), mvMember.end(), FUNCPTR);\
	for(size_t ui = 0; ui < mvMember.size() ; ui++)\
		vec.push_back(mvMember[ui].player_id);\
	mSortMemberVec = vec;
void GuildSys::SortMemberByRank()
{
	std::vector<ui64> tempVec;
	SORTGUILDMEM(&KeyRank, tempVec)
}
void GuildSys::SortMemberByName()
{
	std::vector<ui64> tempVec;
	SORTGUILDMEM(&KeyName, tempVec)
}
void GuildSys::SortMemberByGender()
{
	std::vector<ui64> tempVec;
	SORTGUILDMEM(&KeyGender, tempVec)
}
void GuildSys::SortMemberByLevel()
{
	std::vector<ui64> tempVec;
	SORTGUILDMEM(&KeyLevel, tempVec)
}
void GuildSys::SortMemberByClass()
{
	std::vector<ui64> tempVec;
	SORTGUILDMEM(&KeyClass, tempVec)
}
void GuildSys::SortMemberByAtrr()
{
	std::vector<ui64> tempVec;
	SORTGUILDMEM(&KeyContribution, tempVec)
}
void GuildSys::SortMemberByOnline()
{
	std::vector<ui64> tempVec;
	SORTGUILDMEM(&KeyOnline, tempVec)
}
#undef SORTGUILDMEM
bool KeyGuildName(const MSG_S2C::stGuildListInfoAck::guild_info_t& L_S, const MSG_S2C::stGuildListInfoAck::guild_info_t& R_S)
{
	return (L_S.guildName > R_S.guildName);
}
bool KeyGuildLevel(const MSG_S2C::stGuildListInfoAck::guild_info_t& L_S, const MSG_S2C::stGuildListInfoAck::guild_info_t& R_S)
{
	return (L_S.level >= R_S.level);
}
bool KeyGuildMemNum(const MSG_S2C::stGuildListInfoAck::guild_info_t& L_S, const MSG_S2C::stGuildListInfoAck::guild_info_t& R_S)
{
	return (L_S.member_count >= R_S.member_count);
}
bool KeyGuildLeaderName(const MSG_S2C::stGuildListInfoAck::guild_info_t& L_S, const MSG_S2C::stGuildListInfoAck::guild_info_t& R_S)
{
	return (L_S.master_name > R_S.master_name);
}
bool KeyGuildCastle(const MSG_S2C::stGuildListInfoAck::guild_info_t& L_S, const MSG_S2C::stGuildListInfoAck::guild_info_t& R_S)
{
	std::string LCastle;
	std::string RCastle;
	bool bLGot = SocialitySys->GetGuildSysPtr()->GetIsGuildGotCastle(L_S.id, LCastle);
	bool bRGot = SocialitySys->GetGuildSysPtr()->GetIsGuildGotCastle(R_S.id, RCastle); 
	if (bLGot == true && bRGot == true)
	{
		return (LCastle > RCastle);
	}
	return (bLGot > bRGot);
}
//bool KeyGuildAlien(const MSG_S2C::stGuildListInfoAck::guild_info_t& L_S, const MSG_S2C::stGuildListInfoAck::guild_info_t& R_S)
//{
//	return (L_S > R_S);
//}
//bool KeyGuildRace(const MSG_S2C::stGuildListInfoAck::guild_info_t& L_S, const MSG_S2C::stGuildListInfoAck::guild_info_t& R_S)
//{
//	return (L_S.Race > R_S.Race);
//}
#define SORTGUILD(FUNCPTR) std::sort(mvGuild.begin(), mvGuild.end(), FUNCPTR);
void GuildSys::SortGuildByGuildName()
{
	SORTGUILD(&KeyGuildName);
}
void GuildSys::SortGuildByRace()
{
	//SORTGUILD(&KeyGuildRace);
}
void GuildSys::SortGuildByLevel()
{
	SORTGUILD(&KeyGuildLevel);
}
void GuildSys::SortGuildByMemNum()
{
	SORTGUILD(&KeyGuildMemNum);
}
void GuildSys::SortGuildByLeaderName()
{
	SORTGUILD(&KeyGuildLeaderName);
}
void GuildSys::SortGuildByCastleName()
{
	SORTGUILD(&KeyGuildCastle);
}
void GuildSys::SortGuildByAlien()
{
	//SORTGUILD(&KeyGuildAlien);
}
bool GuildSys::GetSortList(std::vector<ui64>& vlist)
{
	vlist = mSortMemberVec;
	return true;
}
bool GuildSys::GetGuildMemberList(std::vector<MSG_S2C::stGuild_Roster::stMember>& vlist)
{
	vlist = mvMember;
	return true;
}
bool GuildSys::PrevPage()
{
	if (m_CurrentPage > 0)
	{
		--m_CurrentPage;
		return m_CurrentPage != 0;
	}
	return false;
}

bool GuildSys::NextPage()
{
	if (m_CurrentPage < m_MaxPage)
	{
		++m_CurrentPage;
		return m_CurrentPage != m_MaxPage;
	}
	return false;
}

bool GuildSys::GetPage(int& CurPage, int& MaxPage)
{
	CurPage = m_CurrentPage;
	MaxPage = m_MaxPage;
	return true;
}
#undef SORTGUILD
bool GuildSys::GetGUIDFormName(const char* Name, ui64 &guid)
{
	for (UINT i = 0 ; i < mvMember.size() ; i++)
	{
		if ( _stricmp(Name, mvMember[i].name.c_str()) == 0 )
		{
			guid = mvMember[i].player_id;
			return true;
		}
	}
	return false;
}
void GuildSys::SetRankNameToBitMapList(UBitMapListBox* Ctrl)
{
	for (unsigned int ui = 1 ; ui < m_RankName.size() ; ui++)
	{
		Ctrl->AddItem(m_RankName[ui].c_str(), UT_LEFT, 0xFFFFCC33);
	}
}
bool GuildSys::GetRankIdByRankName(const char* RankName, ui32& RankId)
{
	for (unsigned int ui = 0 ; ui < m_RankName.size() ; ui++)
	{
		if(_stricmp(RankName, m_RankName[ui].c_str()) == 0)
		{
			RankId = ui;
			return true;
		}
	}
	return false;
}

std::string GuildSys::GetRankNameByRankId(ui32 RankId)
{
	if (!m_RankName.size()  || RankId >= m_RankName.size())
	{
		return std::string("");
	}
	return m_RankName[RankId];
}

bool GuildSys::GetGuildList(std::vector<MSG_S2C::stGuildListInfoAck::guild_info_t>& vGuild)
{
	if (mvGuild.size())
	{
		vGuild = mvGuild;
		return true;
	}
	return false;
}

bool GuildSys::GetCastleNpcState(uint32 npc_id)
{
	std::set<uint32>::iterator it = m_BuyNpc.begin();
	while( it != m_BuyNpc.end() )
	{
		if (npc_id == *it)
		{
			return true;
		}
		++it;
	}
	return false;
}
bool GuildSys::GetCastleNpcContent(UINT id, struct CastleNpcList& CastleNpc)
{
	UINT realid = mCastleNpcPage * PERPAGE_NPCCOUNT + id;
	if (realid < m_CastleNpcList.size())
	{
		CastleNpc.uNpcEntry = m_CastleNpcList[realid].uNpcEntry;
		CastleNpc.GoldCost = m_CastleNpcList[realid].GoldCost;
		CastleNpc.GuildPointCost = m_CastleNpcList[realid].GuildPointCost;
		CastleNpc.Comments = m_CastleNpcList[realid].Comments;
		return true;
	}
	return false;
}	

void GuildSys::SetCNNextPage()
{
	mCastleNpcPage++;
	if (mCastleNpcPage >= mCastleNpcMaxPage)
	{
		mCastleNpcPage = mCastleNpcMaxPage;
	}
	mCastleBuyNpcDlg->Update();
}
bool GuildSys::GetDBNpcList()
{
	if (!g_pkCastleNpcListDB->GetNpcvMap(m_CastleNpcList))
	{
		return false;
	}
	mCastleNpcPage = 0;
	mCastleNpcMaxPage = m_CastleNpcList.size() / (PERPAGE_NPCCOUNT + 1);

	mCastleBuyNpcDlg->Update();
	return true;
}
void GuildSys::SetCNPrevPage()
{
	if (mCastleNpcPage > 0)
	{
		mCastleNpcPage--;
	}
	mCastleBuyNpcDlg->Update();
}

//////////////////////////////////////////////////////////////////////////