#ifndef GUILDSYS_H
#define GUILDSYS_H

#include "UInc.h"

class UIGuildList : public UControl
{
	UDEC_CLASS(UIGuildList);
public:
	UIGuildList();
	~UIGuildList();
public:
	void SetMemberOnline(ui64 guid);
	void SetMemberOffline(ui64 guid);
	void SetMemberRank(ui64 guid, uint8 rankid);
	void SetMemberContributionPoints(ui64 guid, uint32 contribution_points);
	virtual void GetScrollLineSizes(UINT *rowHeight, UINT *columnWidth);
public:
	void UpdataList();
	void ClearList();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
private:
	//memberlist member
	//typedef std::map<ui64, class UGuildListItem*> MemberMap;
	//MemberMap m_MemberMap;
};
class UIGuildFrame : public UDialog
{
	UDEC_CLASS(UIGuildFrame);
	UDEC_MESSAGEMAP();
public:
	UIGuildFrame();
	~UIGuildFrame();
public:
	UIGuildList* GetGuildList();
	void BeginRankList();
	void ValueRankList(const char * rankname);
	void EndRankList();
	void SetGuildInfo(std::string &info);
	void SetGuildName(const char* Name);
	void SetGuildLevel(uint8 level);
	void SetGuildScore(ui32 Score);
	void SetGuildWarScore(ui32 WarScore);
	void SetGuildDisband();
	void SetGuildEmblemBitMap(char* buf);

	void Show();
	void Hide();

	void OnPrevPage();
	void OnNextPage();
	void OnSetMemNumText(int OnlineNum, int Count, int MaxCount);
protected:
	void OnClickBtnQuitGuild();
	void OnClickBtnCityInfo();
	void OnClickBtnDelMember();
	void OnClickBtnInviteMem();
	void OnClickBtnChangeMark();
	void OnClickBtnSaveGuildInfo();
	void OnClickBtnSaveChange();
	void OnClickBtnFriendPage();
	void OnClickBtnGuildPage();
	void OnClickBtnOLPlayerPage();
	void OnClickBtnTeamPage();
	void OnClickBtnGroupApplyPage();
	void OnClickBtnRankList();
	void OnClickBtnGuildDisband();
	
	void OnClickRankList();

	void OnClickBtnSortByRank();
	void OnClickBtnSortByName();
	void OnClickBtnSortByGender();
	void OnClickBtnSortByLevel();
	void OnClickBtnSortByClass();
	void OnClickBtnSortByAtrr();
	void OnClickBtnSortByOnline();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
private:
	UIGuildList* mGuildList;
	//UBitMapListBox* mBitMapListBox;
	UComboBox* mRankComboBox;
	class UIRankRadioButton* mpRankChooseBtn[6];
};
class UIGuildCreateDLg : public UDialog
{
	UDEC_CLASS(UIGuildCreateDLg);
	UDEC_MESSAGEMAP();
public:
	UIGuildCreateDLg();
	~UIGuildCreateDLg();
	
	virtual void SetVisible(BOOL value);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
protected:
	void OnClickBtnOk();
	void OnClickBtnCancel();

	UEdit* m_pkEdit;
};
class UIGuildQureyListDLg : public UDialog
{
	UDEC_CLASS(UIGuildQureyListDLg);
	UDEC_MESSAGEMAP();
public:
	UIGuildQureyListDLg();
	~UIGuildQureyListDLg();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
public:
	void OnClickBtnSortByName();
	void OnClickBtnSortByRace();
	void OnClickBtnSortByLevel();
	void OnClickBtnSortByMemNum();
	void OnClickBtnSortByGuildLeaderName();
	void OnClickBtnSortByCity();
	void OnClickBtnSortByAlien();

	void OnClickBtnDeclareWar();
	void OnClickBtnAllyReq();
	void OnClickBtnAllyQuit();

	void OnClickBtnNextPage();
	void OnClickBtnPrevPage();
public:
	void OnGetGuildList(std::vector<MSG_S2C::stGuildListInfoAck::guild_info_t> &guildlist);
	void UpdataNowList();
	void SetChooseItem(class UIGuildQureyListitem* Item);
protected:
	class UIGuildQureyListitem* mbChooseItem;
	const UINT PageCount;
	UINT MaxPage;
	UINT NowPage;
};
//stQueryGuildListReq -> MSG_S2C::stGuildListInfoAck
//stGuildAllyReq stGuildDeclareWar stQueryGuildListReq stGuildListInfoAck
class UIGuildEmblem : public UDialog
{
	UDEC_CLASS(UIGuildEmblem);
	UDEC_MESSAGEMAP()
public:
	UIGuildEmblem();
	~UIGuildEmblem();
public:
	BOOL SetEmblemTexture();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
protected:
	void OnClickNextStyleEmblem();
	void OnClickPrevStyleEmblem();
	void OnClickNextColorEmblem();
	void OnClickPrevColorEmblem();
	void OnClickOk();
	void OnClickCancel();
protected:
	int mICurrentColor;
	int mICurrentEmblem;
	//BitMapId = 2 * mICurrentEmblem + mICurrentColor
};
class UICastleBuyNpcItem : public UControl
{
	UDEC_CLASS(UICastleBuyNpcItem);
	UDEC_MESSAGEMAP();
public:
	UICastleBuyNpcItem();
	~UICastleBuyNpcItem();
public:
	void Update();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
protected:
	void OnClickBtnBuy();
protected:
	UStaticText* m_NpcName;
	UStaticText* m_NpcGoldCost;
	UStaticText* m_NpcPointCost;
	UBitmap* m_NpcHeadImage;
	UBitmapButton* m_BuyNpcButton;
	static UINT NpcItemId;
	UINT ItemId;
};
class UIGuildCastleNpcBuyDlg : public UDialog
{
	UDEC_CLASS(UIGuildCastleNpcBuyDlg);
	UDEC_MESSAGEMAP();
public:
	UIGuildCastleNpcBuyDlg();
	~UIGuildCastleNpcBuyDlg();
public:
	void Update();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
protected:
	void OnClickBtnPrevPage();
	void OnClickBtnNextPage();
};

class UIGuildLevelUpDlg : public UDialog
{
	UDEC_CLASS(UIGuildLevelUpDlg);
	UDEC_MESSAGEMAP();
public:
	UIGuildLevelUpDlg();
	~UIGuildLevelUpDlg();

	void Show(uint32 uilevel);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();

	void OnClickLevelUp();
	void OnClickCancel();
};

class GuildSys
{
	typedef struct 
	{
		ui64 guild_id;
		const char* Name;
		uint32 emblemStyle;
		uint32 emblemColor;
	}GuildRecode, *GuildRef;
public:
	GuildSys();
	~GuildSys();
public:
	BOOL CreateFrame(UControl * Ctrl);
	void DestoryFrame();
	void ShowFrame();
	void HideFrame();
public:
	void ParseMsgMember(const std::vector<MSG_S2C::stGuild_Roster::stMember> & vMember);
	void ParseMsgRankName(const std::vector<std::string> & vRank);
	void ParseMsgRankRight(const std::vector<MSG_S2C::stGuild_Roster::stBank> & vRankRight);
	void ParseMsgGuildEvent(ui8 Event, std::vector<std::string> &vStr);
	void ParseMsgGuildName(ui32 GuildID, const char * GuildName);
	void ParseMsgGuildInfo(const char * GuildInfo);
	void ParseMsgGuildCommandResault(uint32 iCmd, std::string &szMsg, uint32 iType);
	void ParseMsgGuildCreate();
	void ParseMsgGuildDisBand();
	void ParseMsgGuildLevelUp(uint32 level);
	void ParseMsgGuildQueryResponse(MSG_S2C::stGuild_Query_Response &msg);
	void ParseMsgGuildQuery(uint8 level, ui32 Score, ui32 War_Score, std::map<uint32, std::string> AlienguildMap, uint32 lead_guild_id);
	void ParseMsgGuildList(std::vector<MSG_S2C::stGuildListInfoAck::guild_info_t> &guildlist);
	void ParseMsgGuildContributionPoints(uint32 player_id, uint32 points, uint32 guild_score);
	void ParseUpLoadGuildEmblem();
	void ParseGuildEmblem(uint32 emblemStyle, uint32 emblemColor);
	void ParseGuildDeclareWarNotify(std::string &ActiveGuild, std::string &PassiveGuild);
	void ParseGuildCastleNpcBuy(std::set<uint32> &buy_npcs, uint32 map_id);
	void ParseGuildCastleState(std::vector<MSG_S2C::stCastleStateAck::castle_state> &vCastlestate, uint32 server_time);
	void ParseGuildCastleCountDown(std::string CastleName, uint32 mapid, uint8 next_state, uint8 count_down_mins);

	void SendMsgRemoveMember(const char* Name);
	void SendMsgInviteMember(const char* Name);
	void SendMsgCreateGuild(const char* Name);
	void SendMsgSaveGuildInfo(const char* Info);
	void SendMsgGuildRoster();
	void SendMsgGuildQuery();
	void SendMsgGuildQuery(ui32 guild_id);
	void SendMsgLeaveGuild();
	void SendMsgSetGuildRank(ui32 rankid,const char * rankName,ui32 IRight);
	void SendMsgChangeMemRank(const char* Name, uint8 Rankid);
	void SendMsgDeclareWar(ui32 GuildGuid);
	void SendMsgGuildAlien(ui32 GuildGuid);
	void SendMsgGuildEmblem(uint64 guid, ui32 EmblemStyle, ui32 EmblemColor);
	void SendMsgQureyCastleState();
	void SendMsgGuildBuyNpc(uint32 npc_id);
	void SendMsgGuildLevelUp();
public:
	void SetLocalGuildId(ui32 guild_Id);
	ui32 SetRankRightVis(bool IsVis, int RankId, int RankVis);
	bool GetRankRightVis(int RankId, int RankVis);
	void SetLocalRankId(ui32 RankId);
	ui32 GetLocalRankId();
	bool GetRankIdByGuid(ui64 guid, ui32 &RankId);
	void SetSelectItem(class UGuildListItem* item);
	void SetGuildScore(ui32 guildScore);

	bool GetGuildName(ui32 guild_id, std::string &Name);
	bool GetGuildEmblem(ui32 guild_id, ui32& EmblemStyle, ui32& EmblemColor);


	void OnGuildCastleInfo();

	bool GetIsGuildGotCastle(ui32 guild_id, std::string& CastleName);

	void RemoveSelect();
	void DelMember();
	static void AcceptDelMember();
	static void DeclineDelMember();

public:
	//特殊
	void SetRankNameToBitMapList(UBitMapListBox* Ctrl);
	bool GetRankIdByRankName(const char* RankName, ui32& RankId);
	std::string GetRankNameByRankId(ui32 RankId);
public:
	void SortMemberByRank();
	void SortMemberByName();
	void SortMemberByGender();
	void SortMemberByLevel();
	void SortMemberByClass();
	void SortMemberByAtrr();
	void SortMemberByOnline();

	void SortGuildByGuildName();
	void SortGuildByRace();
	void SortGuildByLevel();
	void SortGuildByMemNum();
	void SortGuildByLeaderName();
	void SortGuildByCastleName();
	void SortGuildByAlien();
	
	bool GetSortList(std::vector<ui64>& vlist);
	bool GetGuildMemberList(std::vector<MSG_S2C::stGuild_Roster::stMember>& vlist);

	bool PrevPage();
	bool NextPage();
	bool GetPage(int& CurPage, int& MaxPage);
	uint8 GetLevel(){return m_LocalGuildLevel;};
private:
	int m_CurrentPage;
	int m_MaxPage;
	std::vector<ui64> mSortMemberVec;
	uint8 m_LocalGuildLevel;

private:
	bool GetGUIDFormName(const char* Name, ui64 &guid);

private:
	BOOL mIsMotdEvent;
	std::vector<MSG_S2C::stGuild_Roster::stMember> mvMember;
	std::vector<MSG_S2C::stGuildListInfoAck::guild_info_t> mvGuild;
	std::map<ui32, GuildRecode> mGuildMap;
	class UGuildListItem* m_bSelectItem;
	std::vector<ui32> mRankRight;
	std::vector<std::string> m_RankName;
	std::string m_GuildMod;
	std::string m_GuildName;

	ui32 LocalRankId;
	ui32 LocalGuildId;

	//攻城战
public:
	bool GetCastleNpcState(uint32 npc_id);
	bool GetCastleNpcContent(UINT id, struct CastleNpcList& CastleNpc);
	bool GetDBNpcList();
	void SetCNNextPage();
	void SetCNPrevPage();
protected:
	std::vector<MSG_S2C::stCastleStateAck::castle_state> m_vGuildCastleState;
	std::set<uint32> m_BuyNpc;
	uint32 m_Castlemapid;
	std::vector<struct CastleNpcList> m_CastleNpcList;
	UINT mCastleNpcPage;
	UINT mCastleNpcMaxPage;
	enum
	{
		PERPAGE_NPCCOUNT = 10,
	};


	ui32 mGuildLevel;

protected:
	//界面
	UIGuildFrame* mGuildFrame;
	UIGuildCreateDLg* mGuildCreateDlg;
	UIGuildQureyListDLg* mGuildQureyListDlg;
	UIGuildEmblem* mGuildEmblem;
	UIGuildCastleNpcBuyDlg* mCastleBuyNpcDlg;
	UIGuildLevelUpDlg* mGuildLevelUpDlg;
public:
	bool GetGuildList(std::vector<MSG_S2C::stGuildListInfoAck::guild_info_t>& vGuild);
	ui32 GetGuildLevel(){return mGuildLevel;};

};

#endif