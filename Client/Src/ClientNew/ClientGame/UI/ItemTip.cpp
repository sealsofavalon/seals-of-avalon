#include "StdAfx.h"
#include "ItemManager.h"
#include "ItemTip.h"
#include "ObjectManager.h"
#include "EquipSlot.h"
#include "SYItem.h"
#include "UITipSystem.h"
#include "UIGamePlay.h"
#include "LocalPlayer.h"
#include "UFun.h"
#include "UISkill.h"
#include "Utils/AreaDB.h"
#include "utils/ItemEnchant.h"
#include "TitleMgr.h"
#include "Skill/SkillManager.h"
#include "Utils/ItemExtendedCostentry.h"

ItemTip::ItemTip()
{
	m_IsComTip = FALSE;

	m_Count = 0;
	m_pkData = NULL;
	m_Texture[0] = NULL;
	m_Texture[1] = NULL;
	m_Texture[2] = NULL;
	m_Texture[3] = NULL;

	m_SocketSkin = NULL;

	m_PayDataItemSkin = NULL;
	m_PayDataItemIndex = 0;
	m_ItemSpecProperty.IsCreate = false;
	m_BoardWide = 8;

	PayDateName[0] = _TRAN("道具");
	PayDateName[1] = _TRAN("部族积分");
	PayDateName[2] = _TRAN("部族贡献");
	PayDateName[3] = _TRAN("荣誉");
	PayDateName[4] = _TRAN("竞技积分");

	m_Item_PlayerGuid = 0;
}
ItemTip::~ItemTip()
{
	m_pkData = NULL;
}

BOOL ItemTip::InitTexture(URender * pRender)
{
	if (m_Texture[0] == NULL)
	{
		m_Texture[0] = pRender->LoadTexture("Data\\UI\\NPCTrade\\honor.png");
	}

	if (m_Texture[1] == NULL)
	{
		m_Texture[1] = pRender->LoadTexture("Data\\UI\\NPCTrade\\sorce.png");
	}
	if (m_Texture[2] == NULL)
	{
		m_Texture[2] = pRender->LoadTexture("Data\\UI\\NPCTrade\\gongxian.png");
	}
	if (m_Texture[3] == NULL)
	{
		m_Texture[3] = pRender->LoadTexture("Data\\UI\\NPCTrade\\Arena.png");
	}
	for (int i = 0; i < 4; i++)
	{
		if (m_Texture[i] == NULL)
		{
			return FALSE;
		}
	}
	if (m_SocketSkin == NULL)
	{
		m_SocketSkin = UControl::sm_System->LoadSkin("Style\\Sockets.skin");
	}
	if (!m_SocketSkin)
	{
		return FALSE;
	}
	return TRUE;
}
void ItemTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos, PayData* pkData, USkinPtr PayItemSkin, UINT SkinIndex)
{
	mEntry = ID;
	mID = 0;
	m_PayDataItemSkin = NULL;
	m_PayDataItemIndex = 0;

	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(mEntry);
	if (ItemInfo)
	{
		m_Money.Create(UControl::sm_UiRender);
		m_RepairPrice.Create(UControl::sm_UiRender);
		Ctrl->SetPosition(pos);
		Ctrl->SetVisible(TRUE);
		m_KeepDraw = TRUE;
	}
	else
	{
		return;
	}
	if (m_Content.size() > 0)
	{
		m_Content.clear();
	}
	m_pkData = pkData ;
	

	if (m_pkData && m_pkData->mItemId)
	{
		m_PayDataItemSkin = PayItemSkin;
		m_PayDataItemIndex = SkinIndex;
	}
	m_IsComTip = TipSystem->IsComparaTip(this);
	m_Item_PlayerGuid = TipSystem->GetShowPlayerGuid();
	m_Count = 1;

	m_ItemSpecProperty.IsCreate = false;

	SeparateItem(ItemInfo->Class,ItemInfo->SubClass);
}
void ItemTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos)
{
	if (mEntry == ID)
	{
		//return;
	}

	mEntry = ID;
	mID = 0;
	m_pkData = NULL;
	m_PayDataItemSkin = NULL;
	m_PayDataItemIndex = 0;

	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(mEntry);
	if (ItemInfo)
	{
		m_Money.Create(UControl::sm_UiRender);
		m_RepairPrice.Create(UControl::sm_UiRender);
		Ctrl->SetPosition(pos);
		Ctrl->SetVisible(TRUE);
		m_KeepDraw = TRUE;
	}
	else
	{
		return;
	}
	if (m_Content.size() > 0)
	{
		m_Content.clear();
	}
	m_IsComTip = TipSystem->IsComparaTip(this);
	m_Item_PlayerGuid = TipSystem->GetShowPlayerGuid();

	m_Count = 1;

	SeparateItem(ItemInfo->Class,ItemInfo->SubClass);
}

void ItemTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid ,UINT count)
{
	if (mID == guid && mEntry == ID)
	{
		//return;
	}
	m_pkData = NULL;
	m_PayDataItemSkin = NULL;
	m_PayDataItemIndex = 0;

	SYItem * Item = (SYItem *)ObjectMgr->GetObject(guid);
	if (Item && Item->GetUInt32Value(ITEM_FIELD_LIFE_STYLE)/* && !TipSystem->mIsUpdateServerTime*/)
	{
		if (mID != guid || mEntry != ID)
		{
			TipSystem->QueryServerTime();
		}
	}
	mEntry = ID;
	mID = guid;
	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(mEntry);
	if (ItemInfo)
	{
		m_Money.Create(UControl::sm_UiRender);
		m_RepairPrice.Create(UControl::sm_UiRender);
		Ctrl->SetPosition(pos);
		Ctrl->SetVisible(TRUE);
		m_KeepDraw = TRUE;
	}
	else
	{
		return;
	}
	if (m_Content.size() > 0)
	{
		m_Content.clear();
	}
	m_IsComTip = TipSystem->IsComparaTip(this);
	m_Item_PlayerGuid = TipSystem->GetShowPlayerGuid();

	m_Count = count ;

	BulidSpecProperty();
	SeparateItem(ItemInfo->Class,ItemInfo->SubClass);
}
void ItemTip::ReCreateTip()
{
	m_pkData = NULL;
	m_PayDataItemSkin = NULL;
	m_PayDataItemIndex = 0;

	SYItem * Item = (SYItem *)ObjectMgr->GetObject(mID);
	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(mEntry);
	if (ItemInfo)
	{
		m_Money.Create(UControl::sm_UiRender);
		m_RepairPrice.Create(UControl::sm_UiRender);
		m_KeepDraw = TRUE;
	}
	else
	{
		return;
	}
	if (m_Content.size() > 0)
	{
		m_Content.clear();
	}
	m_IsComTip = TipSystem->IsComparaTip(this);
	m_Item_PlayerGuid = TipSystem->GetShowPlayerGuid();

	BulidSpecProperty();
	SeparateItem(ItemInfo->Class,ItemInfo->SubClass);
}
void ItemTip::BulidSpecProperty()
{
	SYItem * Item = (SYItem *)ObjectMgr->GetObject(mID);
	if (Item)
	{
		m_ItemSpecProperty.IsCreate = true;
		m_ItemSpecProperty.ItemEntry = mEntry;
		m_ItemSpecProperty.itemdata.jinglian_level = Item->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
		for( int i = 0; i < 6; ++i )
			m_ItemSpecProperty.itemdata.enchants[i] =  Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + i * 3 );
	}
	else
	{
		m_ItemSpecProperty.IsCreate = false;
	}
}

void ItemTip::DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect )
{
	if (m_KeepDraw)
	{
		//渲染边框以及内部填充区域
		UDrawResizeImage(RESIZE_WANDH, pRender, TipSystem->mBkgSkin, pos, updateRect.GetSize());
		//pRender->DrawRectFill(updateRect,pCtrlStl->mFillColor);
		//pRender->DrawRect(updateRect,pCtrlStl->mBorderColor);
		//渲染文字
		int height = DrawContent(pRender,pCtrlStl,pos,updateRect);
		//设置当前提示框的高度
		if (height != Ctrl->GetHeight())
		{
			Ctrl->SetHeight(height);
		}
	}
}
void ItemTip::DestoryTip()
{
	//mEntry = 0;
	//mID = 0;
	m_KeepDraw = FALSE;
	m_Content.clear();
	m_Money.Destory();
	m_RepairPrice.Destory();
	m_Count = 0;
	m_ItemSpecProperty.IsCreate = false;
}

//暴击率+5%			1
//闪避+3%			2
//生命最大值+2%		3
//魔法最大值+2%		4
//伤害减少+3%		5
//移动速度+5%		6
//杀怪经验+1%		7
//命中率+10%		8
//格挡几率+10%		9
//冰冻目标+2%		10
//击晕目标+2%		11
//迟缓目标+2%		12
//消魔+5%			13
//吸血+3%			14
//杀怪回血+5%		15
//杀怪回魔+5%		16
//恐惧几率+2%		17
//反射伤害+2%		18

void ItemTip::CreateContentItem(int Type)
{
	ContentItem TEMP_CI;
	BOOL BFine = FALSE;
	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(mEntry);
	SYItem * Item = (SYItem *)ObjectMgr->GetObject(mID);
	float RateArg = 0 ; // 强化后的属相加强参数
	if (Item)
	{
		//RateArg = (float)Item->GetUInt32Value(ITEM_FIELD_REFINE_UPGRADE_RATE);
	}
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (ItemInfo && pkLocal)
	{
		TEMP_CI.Type = Type;
		switch (Type)
		{
		case ITEM_TIP_CURRENTEQUIP:
			{
				TEMP_CI.FontColor = GetColor(C_COLOR_OTHER);
				TEMP_CI.str.Set( _TRAN( "[现在装备]"));
				TEMP_CI.FontFace = UControl::sm_UiRender->CreateFont("Arial", 14,134);
				TEMP_CI.LineHAdd = 1;
				BFine = TRUE;
			}
			break;
		case ITEM_TIP_MONEYCOUNT:
			{
				TEMP_CI.FontColor = GetColor(C_COLOR_ITEMJUNK);
				TEMP_CI.str.Set(_TRAN("金钱"));
				if (m_Count > 0)
				{
					m_Money.SetMoney(m_Count);
				}
				BFine = TRUE;
			}
			break;
		case ITEM_TIP_YUANBAOCOUNT:
			{
				TEMP_CI.FontColor = GetColor(C_COLOR_ITEMJUNK);
				TEMP_CI.str.Set(_TRAN("元宝"));
				if (m_Count > 0)
				{
					m_Money.SetMoney(m_Count ,TRUE);
				}
				BFine = TRUE;
			}
			break ;
		case ITEM_TIP_NAME:
			{
				if (ItemInfo->C_name.size())
				{
					string fname;
					fname = ItemInfo->C_name;

					TEMP_CI.str.Set(fname.c_str());
					TEMP_CI.FontFace = TipSystem->GetNameFontFace();

					GetQualityColor(ItemInfo->Quality, TEMP_CI.FontColor);
					
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_BONDING:
			{
				TEMP_CI.str.Set(_TRAN(BondingType[ItemInfo->Bonding]));
				TEMP_CI.FontColor = GetColor(C_COLOR_BANGDING);
				TEMP_CI.bChangeLine = false;
				BFine = TRUE;
			/*	SYItem * Item = (SYItem *)ObjectMgr->GetObject(mID);*/
				if(Item && Item->HasFlag( ITEM_FIELD_FLAGS, ITEM_FLAG_QUEST | ITEM_FLAG_SOULBOUND ))
				{
					TEMP_CI.str.Set(_TRAN("已绑定"));
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_REFINELEVEL:
			{
				if (m_ItemSpecProperty.IsCreate && m_ItemSpecProperty.itemdata.jinglian_level)
				{
					//string fname;

					//char buf[256];
					//sprintf_s(buf, 256, "精练：%d", m_ItemSpecProperty.itemdata.jinglian_level);

					std::string strRet = _TRAN(N_NOTICE_Z87, _I2A(m_ItemSpecProperty.itemdata.jinglian_level).c_str());

					//fname = strRet;

					TEMP_CI.str.Set(strRet.c_str());

					BFine = TRUE ;
				}else
				{
					TEMP_CI.str.Set("");

					BFine = TRUE ;
				}
				TEMP_CI.FontColor = GetColor(C_COLOR_XQ);
				TEMP_CI.LineHAdd = 0;
				TEMP_CI.ta = UT_RIGHT;
			}
			break;
		case ITEM_TIP_SELLALLOWABLE:
			{
				TEMP_CI.str.Set(ItemInfo->SellPrice?_TRAN("可出售"):_TRAN("不可出售"));
				TEMP_CI.FontColor = GetColor(C_COLOR_ITEMPROPERTY);
				TEMP_CI.ta = UT_RIGHT;
				TEMP_CI.LineHAdd = 0;
				BFine = TRUE;
			}
			break;
		case ITEM_TIP_SUBCLASS:
			{
				TEMP_CI.bChangeLine = false;
				switch (ItemInfo->Class)
				{
				case ITEM_CLASS_CONSUMABLE:
					{
						if (ItemInfo->SubClass >= 0 && ItemInfo->SubClass < ITEM_SUBCLASS_CONSUMABLE_MAX)
						{	
							TEMP_CI.str.Set(_TRAN(ConsumableType[ItemInfo->SubClass]));
							BFine = TRUE;
						}
					}
					break;
				case ITEM_CLASS_CONTAINER:
					{
						TEMP_CI.str.Set(_TRAN("包裹类"));
						BFine = TRUE;
					}
					break;
				case ITEM_CLASS_WEAPON:
					{
						if (ItemInfo->SubClass >= 0 && ItemInfo->SubClass < ITEM_SUBCLASS_WEAPON_MAX)
						{
							std::string strtip = _TRAN(WeaponType[ItemInfo->SubClass]);
							switch(ItemInfo->InventoryType)
							{
							case INVTYPE_WEAPONMAINHAND:
								strtip += _TRAN(",需要主手");
								break;
							case INVTYPE_WEAPONOFFHAND:
								strtip += _TRAN(",需要副手");
								break;
							case INVTYPE_2HWEAPON:
								strtip += _TRAN(",需要双手");
								break;
							}
							TEMP_CI.str.Set(strtip.c_str());
							BFine = TRUE;
						}
					}
					break;
				case ITEM_CLASS_JEWELRY:
					break;
				case ITEM_CLASS_ARMOR:

					{
						if(ItemInfo->SubClass  >= 0 && ItemInfo->SubClass < ITEM_SUBCLASS_ARMOR_MAX)
						{
							TEMP_CI.str.Set(_TRAN(ArmorType[ItemInfo->SubClass]));
							BFine = TRUE;
						}
					}
					break;
				case ITEM_CLASS_REAGENT:
					{

					}
					break;
				case ITEM_CLASS_RECIPE:
					{
						if (ItemInfo->SubClass  >= 0 && ItemInfo->SubClass <= ITEM_SUBCLASS_RECIPE_FISNING)
						{
							TEMP_CI.str.Set(_TRAN(RecipeType[ItemInfo->SubClass]));
							BFine = TRUE;
						}
					}	
					break;
				case ITEM_CLASS_MONEY:
					{
						TEMP_CI.str.Set(_TRAN("金币"));
						BFine = TRUE;
					}
					break;
				case ITEM_CLASS_QUEST:
					{
						TEMP_CI.str.Set(_TRAN("任务物品"));
						BFine = TRUE;
					}
					break;
				case ITEM_CLASS_MISCELLANEOUS:
					{

					}
					break;
				case ITEM_CLASS_USE:
					{
						TEMP_CI.str.Set(_TRAN("重复使用道具"));
						BFine = TRUE;
					}
					break;
				}
			}
			break;
		case ITEM_TIP_DAMAGE:
			{
				//char buf[256];

				UINT DamageAdd = ItemMgr->GetItemRefineRate(mEntry , 0) * m_ItemSpecProperty.itemdata.jinglian_level;

				if (!ItemInfo->Damage[0].Max == 0 && ItemInfo->Damage[0].Type < 5)
				{
					int min = int(ItemInfo->Damage[0].Min) + DamageAdd;
					int max = int(ItemInfo->Damage[0].Max) + DamageAdd;

					//sprintf_s(buf,256,"%d -%d 伤害", min, max);

					string strTem  = _TRAN(N_NOTICE_C77, _I2A(min).c_str(), _I2A(max).c_str());

					TEMP_CI.str.Set(strTem.c_str());
					TEMP_CI.FontColor = GetColor(C_COLOR_ITEMPROPERTY);
					TEMP_CI.bChangeLine = false;
					BFine = TRUE;
					if (BFine)
					{
						m_Content.push_back(TEMP_CI);
						BFine = FALSE;
					}
				}
			}
			break;
		case ITEM_TIP_SPEED:
			{
				//char buf[256];
				if (ItemInfo->Delay > 0 )
				{
					float delay = float(ItemInfo->Delay) / 1000.f;
					//sprintf_s(buf,256,"速度%.2f",delay);
					string strtem = _TRAN(N_NOTICE_C76, _F2A(delay, 2).c_str());
					TEMP_CI.str.Set(strtem.c_str());
					TEMP_CI.FontColor = GetColor(C_COLOR_ITEMPROPERTY);
					TEMP_CI.ta = UT_RIGHT;
					TEMP_CI.LineHAdd = 0;
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_DPS:
			{
				//char buf[256];
				if (ItemInfo->Delay > 0 )
				{
					float delay = float(ItemInfo->Delay) / 1000.f;
					UINT DamageAdd = ItemMgr->GetItemRefineRate(mEntry , 0) * m_ItemSpecProperty.itemdata.jinglian_level;
					float dps = ( ItemInfo->Damage[0].Min + ItemInfo->Damage[0].Max ) / 2.f;
					dps = dps + DamageAdd;
					dps = dps / delay;

					string str = _TRAN(N_NOTICE_C75, _F2A(dps, 2).c_str());
					TEMP_CI.str.Set(str.c_str());
					TEMP_CI.FontColor = GetColor(C_COLOR_ITEMPROPERTY);
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_STATS:
			{
				UINT RefineADD = 0 ;
				if (m_ItemSpecProperty.IsCreate)
				{
					RefineADD = ItemMgr->GetItemRefineRate(mEntry, 2) * m_ItemSpecProperty.itemdata.jinglian_level ;
				}

				 
				char buf[256];
				for (int i = 0 ;i < 10 ; i++)
				{
					if (!ItemInfo->Stats[i].Value == 0)
					{
						if (ItemInfo->Stats[i].Type > STAT_NONE && ItemInfo->Stats[i].Type <= SPELL_DAMAGE)
						{
							
							if (ItemInfo->Stats[i].Type > VITALITY)
							{
								if (ItemInfo->Stats[i].Type  > SHIELD_BLOCK_RATING && ItemInfo->Stats[i].Type <= SPELL_DAMAGE)
								{
									sprintf_s(buf,256,"%s  +%d",_TRAN(ItemStat[ItemInfo->Stats[i].Type]),ItemInfo->Stats[i].Value);
								}else
								{
									sprintf_s(buf,256,"%s  +%d%%",_TRAN(ItemStat[ItemInfo->Stats[i].Type]),ItemInfo->Stats[i].Value);
								}
								
							}else
							{
								sprintf_s(buf,256,"%s  +%d",_TRAN(ItemStat[ItemInfo->Stats[i].Type]),(ItemInfo->Stats[i].Value + RefineADD));
							}
							
							TEMP_CI.str.Set(buf);
							//TEMP_CI.FontColor = GetColor(C_COLOR_ITEMPROPERTY);
							BFine = TRUE;
						}
					}
					if (BFine)
					{
						m_Content.push_back(TEMP_CI);
						BFine = FALSE;
					}
				}
				if (Item)
				{
					for ( unsigned int ui = 0; ui < 2; ui++ )
					{
						TEMP_CI.str.Set("(");
						uint32 propid =  Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + ui * 3 );
						if (propid != 0)
						{
							ItemEnchantProto enchant_infos;
							g_pkItemEnchantDB->GetEnchantProperty(propid, enchant_infos);
							sprintf_s(buf,256,"%s ",enchant_infos.sName.c_str());
							TEMP_CI.str.Append(buf);
							BFine = TRUE;
						}
						if (BFine)
						{
							TEMP_CI.str.Append(")");
							TEMP_CI.FontColor = ui?GetColor(C_COLOR_ITEMRARE):GetColor(C_COLOR_ITEMPROPERTY);
							m_Content.push_back(TEMP_CI);
							BFine = FALSE;
						}
					}
				}
				else if(m_ItemSpecProperty.IsCreate)
				{
					for ( unsigned int ui = 0; ui < 2; ui++ )	{
						TEMP_CI.str.Set("(");
						uint32 propid = m_ItemSpecProperty.itemdata.enchants[ui];
						if (propid != 0)
						{
							ItemEnchantProto enchant_infos;
							g_pkItemEnchantDB->GetEnchantProperty(propid, enchant_infos);
							sprintf_s(buf,256,"%s ",enchant_infos.sName.c_str());
							TEMP_CI.str.Append(buf);
							BFine = TRUE;
						}
						if (BFine)
						{
							TEMP_CI.str.Append(")");
							TEMP_CI.FontColor = ui?GetColor(C_COLOR_ITEMRARE):GetColor(C_COLOR_ITEMPROPERTY);
							m_Content.push_back(TEMP_CI);
							BFine = FALSE;
						}
					}
				}
			}
			return;
		case ITEM_TIP_REQUIRELEVEL:
			{
				CPlayer * pkplayer = ObjectMgr->GetLocalPlayer();
				if (ItemInfo->RequiredLevel > pkplayer->GetLevel())
				{
					TEMP_CI.FontColor = GetColor(C_COLOR_WARNING);
				}
				//char buf[1024];
				//sprintf_s(buf,1024,"需求等级： %d级",ItemInfo->RequiredLevel);
				std::string strRet = _TRAN(N_NOTICE_Z88, _I2A(ItemInfo->RequiredLevel).c_str() );
				TEMP_CI.str.Set(strRet.c_str());
				BFine = TRUE;
			}
			break;
		case ITEM_TIP_HOLE_CNT:
			{
				//if (Item && Item->GetUInt32Value(ITEM_FIELD_HOLE_CNT))
				//{
				//	//char buf[1024];
				//	//sprintf_s(buf,1024,"开孔数  %d", Item->GetUInt32Value(ITEM_FIELD_HOLE_CNT));

				//	std::string strRet = _TRAN(N_NOTICE_Z89, _I2A(Item->GetUInt32Value(ITEM_FIELD_HOLE_CNT)).c_str() );

				//	TEMP_CI.str.Set(strRet.c_str());
				//	TEMP_CI.FontColor = GetColor(C_COLOR_XQ);
				//	BFine = TRUE;
				//}
				//else
				//{
				//	if (m_ItemSpecProperty.IsCreate && m_ItemSpecProperty.itemdata.hole_cnt)
				//	{
				//		//char buf[1024];
				//		//sprintf_s(buf,1024,"开孔数  %d", m_ItemSpecProperty.itemdata.hole_cnt);
				//		std::string strRet = _TRAN(N_NOTICE_Z90, _I2A(m_ItemSpecProperty.itemdata.hole_cnt).c_str());
				//		TEMP_CI.str.Set(strRet.c_str());
				//		TEMP_CI.FontColor = GetColor(C_COLOR_XQ);
				//		BFine = TRUE;
				//	}
				//}
			}
			break;
		case ITEM_TIP_HOLE_1:
			{
				if (ItemInfo->Sockets[0].SocketColor)
				{
					int colorindex = 0;
					if(ItemInfo->Sockets[0].SocketColor & 1){
						colorindex = 4;
					}else if(ItemInfo->Sockets[0].SocketColor & 2){
						colorindex = 1;
					}else if(ItemInfo->Sockets[0].SocketColor & 4){
						colorindex = 2;
					}else if(ItemInfo->Sockets[0].SocketColor & 8){
						colorindex = 3;
					}
					TEMP_CI.str.Set(_TRAN(cSocketsColor[colorindex]));
					TEMP_CI.FontColor = GetColor(C_COLOR_DEFAULT);
					TEMP_CI.Icon = m_SocketSkin;
					TEMP_CI.IconIndex = colorindex;
					ItemEnchantProto enchant_infos;
					std::string str;
					if (Item && Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + 2 * 3 ))
					{
						g_pkItemEnchantDB->GetEnchantProperty(Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + 2 * 3 ), enchant_infos);
						TEMP_CI.str.Set(enchant_infos.sName.c_str());
						TEMP_CI.FontColor = GetColor(C_COLOR_XQ);
						ItemInfo = ItemMgr->GetItemPropertyFromDataBase(enchant_infos.uiGemEntry);
						if (ItemInfo)
							str = ItemInfo->C_icon;
					}else if (m_ItemSpecProperty.IsCreate && m_ItemSpecProperty.itemdata.enchants[2])
					{
						g_pkItemEnchantDB->GetEnchantProperty(m_ItemSpecProperty.itemdata.enchants[2], enchant_infos);
						TEMP_CI.str.Set(enchant_infos.sName.c_str());
						TEMP_CI.FontColor = GetColor(C_COLOR_XQ);
						ItemInfo = ItemMgr->GetItemPropertyFromDataBase(enchant_infos.uiGemEntry);
						if (ItemInfo)
							str = ItemInfo->C_icon;
					}
					if (str.length())
					{	
						int pos = str.find(";");
						std::string skinstr = str.substr(0,pos);
						std::string strIndex = str.substr(pos+1,str.length() -1);
						TEMP_CI.Icon = UControl::sm_System->LoadSkin(skinstr.c_str());
						TEMP_CI.IconIndex = atoi(strIndex.c_str());
					}
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_HOLE_2:
			{
				if (ItemInfo->Sockets[1].SocketColor)
				{
					int colorindex = 0;
					if(ItemInfo->Sockets[1].SocketColor & 1){
						colorindex = 4;
					}else if(ItemInfo->Sockets[1].SocketColor & 2){
						colorindex = 1;
					}else if(ItemInfo->Sockets[1].SocketColor & 4){
						colorindex = 2;
					}else if(ItemInfo->Sockets[1].SocketColor & 8){
						colorindex = 3;
					}
					TEMP_CI.str.Set(_TRAN(cSocketsColor[colorindex]));
					TEMP_CI.FontColor = GetColor(C_COLOR_DEFAULT);
					TEMP_CI.Icon = m_SocketSkin;
					TEMP_CI.IconIndex = colorindex;
					ItemEnchantProto enchant_infos;
					std::string str;
					if (Item && Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + 3 * 3 ))
					{
						g_pkItemEnchantDB->GetEnchantProperty(Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + 3 * 3 ), enchant_infos);
						TEMP_CI.str.Set(enchant_infos.sName.c_str());
						TEMP_CI.FontColor = GetColor(C_COLOR_XQ);
						/*ItemPrototype_Client**/ ItemInfo = ItemMgr->GetItemPropertyFromDataBase(enchant_infos.uiGemEntry);
						if (ItemInfo)
							str = ItemInfo->C_icon;
					}else if (m_ItemSpecProperty.IsCreate && m_ItemSpecProperty.itemdata.enchants[3])
					{
						g_pkItemEnchantDB->GetEnchantProperty(m_ItemSpecProperty.itemdata.enchants[3], enchant_infos);
						TEMP_CI.str.Set(enchant_infos.sName.c_str());
						TEMP_CI.FontColor = GetColor(C_COLOR_XQ);
						/*ItemPrototype_Client**/ ItemInfo = ItemMgr->GetItemPropertyFromDataBase(enchant_infos.uiGemEntry);
						if (ItemInfo)
							str = ItemInfo->C_icon;
					}
					if (str.length())
					{	
						int pos = str.find(";");
						std::string skinstr = str.substr(0,pos);
						std::string strIndex = str.substr(pos+1,str.length() -1);
						TEMP_CI.Icon = UControl::sm_System->LoadSkin(skinstr.c_str());
						TEMP_CI.IconIndex = atoi(strIndex.c_str());
					}
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_HOLE_3:
			{
				if (ItemInfo->Sockets[2].SocketColor)
				{
					int colorindex = 0;
					if(ItemInfo->Sockets[2].SocketColor & 1){
						colorindex = 4;
					}else if(ItemInfo->Sockets[2].SocketColor & 2){
						colorindex = 1;
					}else if(ItemInfo->Sockets[2].SocketColor & 4){
						colorindex = 2;
					}else if(ItemInfo->Sockets[2].SocketColor & 8){
						colorindex = 3;
					}
					TEMP_CI.str.Set(_TRAN(cSocketsColor[colorindex]));
					TEMP_CI.FontColor = GetColor(C_COLOR_DEFAULT);
					TEMP_CI.Icon = m_SocketSkin;
					TEMP_CI.IconIndex = colorindex;
					ItemEnchantProto enchant_infos;
					std::string str;
					if (Item && Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + 4 * 3 ))
					{
						g_pkItemEnchantDB->GetEnchantProperty(Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + 4 * 3 ), enchant_infos);
						TEMP_CI.str.Set(enchant_infos.sName.c_str());
						TEMP_CI.FontColor = GetColor(C_COLOR_XQ);
						/*ItemPrototype_Client**/ ItemInfo = ItemMgr->GetItemPropertyFromDataBase(enchant_infos.uiGemEntry);
						if (ItemInfo)
							str = ItemInfo->C_icon;
					}else if (m_ItemSpecProperty.IsCreate && m_ItemSpecProperty.itemdata.enchants[4])
					{
						g_pkItemEnchantDB->GetEnchantProperty(m_ItemSpecProperty.itemdata.enchants[4], enchant_infos);
						TEMP_CI.str.Set(enchant_infos.sName.c_str());
						TEMP_CI.FontColor = GetColor(C_COLOR_XQ);
						/*ItemPrototype_Client**/ ItemInfo = ItemMgr->GetItemPropertyFromDataBase(enchant_infos.uiGemEntry);
						if (ItemInfo)
							str = ItemInfo->C_icon;
					}
					if (str.length())
					{	
						int pos = str.find(";");
						std::string skinstr = str.substr(0,pos);
						std::string strIndex = str.substr(pos+1,str.length() -1);
						TEMP_CI.Icon = UControl::sm_System->LoadSkin(skinstr.c_str());
						TEMP_CI.IconIndex = atoi(strIndex.c_str());
					}
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_ITEMSPELLDES:
			{
				for (int i = 0 ; i < 5 ; ++i)
				{
					if (ItemInfo->Spells[i].Id)
					{
						SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate(ItemInfo->Spells[i].Id);
						if (!pSpellTemplate)
						{
							continue;
						}
						switch(ItemInfo->Spells[i].Trigger)
						{
						case USE:
							{
								std::string desc;
								desc += pSpellTemplate->GetDescription(desc, m_Item_PlayerGuid == pkLocal->GetGUID());
								desc =  _TRAN("使用") + std::string(":") + desc;

								TEMP_CI.str.Set(desc.c_str());
								TEMP_CI.FontColor = GetColor(C_COLOR_ITEMUNCOMMON);
								m_Content.push_back(TEMP_CI);
								BFine = FALSE;
							}
							break;
						case ON_EQUIP:
							{
								std::string desc;
								desc += pSpellTemplate->GetDescription(desc, m_Item_PlayerGuid == pkLocal->GetGUID());
								desc =  _TRAN("装备") + std::string(":") + desc;

								TEMP_CI.str.Set(desc.c_str());
								TEMP_CI.FontColor = GetColor(C_COLOR_ITEMUNCOMMON);
								m_Content.push_back(TEMP_CI);
								BFine = FALSE;
							}
							break;
						case CHANCE_ON_HIT:
							{

							}
							break;
						case SOULSTONE:
							{

							}
							break;
						case LEARNING:
							{

							}
							break;
						case ON_UNEQUIP:
							{

							}
							break;
						}
					}
				}
			}
			break;
		case ITEM_TIP_SOCKETBONUS:
			{
				if (ItemInfo->SocketBonus)
				{
					ItemEnchantProto enchant_infos;
					g_pkItemEnchantDB->GetEnchantProperty(ItemInfo->SocketBonus, enchant_infos);
					std::string SocketBonus = _TRAN("插槽加成：") + enchant_infos.sName;
					TEMP_CI.str.Set(SocketBonus.c_str());
					TEMP_CI.FontColor = GetColor(C_COLOR_DEFAULT);

					if (Item && Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + 5 * 3 ))
					{
						TEMP_CI.FontColor = GetColor(C_COLOR_ITEMPROPERTY);
					}else if (m_ItemSpecProperty.IsCreate && m_ItemSpecProperty.itemdata.enchants[5])
					{
						TEMP_CI.FontColor = GetColor(C_COLOR_ITEMPROPERTY);
					}
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_GEMPROPERTY:
			{
				if (ItemInfo->GemProperties)
				{
					ItemEnchantProto enchant_infos;
					g_pkItemEnchantDB->GetEnchantProperty(ItemInfo->GemProperties, enchant_infos);
					std::string SocketBonus = enchant_infos.sName;
					TEMP_CI.str.Set(SocketBonus.c_str());
					TEMP_CI.FontColor = GetColor(C_COLOR_XQ);
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_ITEMUSECOUNT:
			{
				if (Item && ItemInfo->usecnt > 1)
				{
					ui32 usecount = Item->GetUInt32Value(ITEM_FIELD_USE_CNT);

					char buf[256];
					sprintf(buf, "%s:%u/%u", _TRAN("使用次数"), usecount, ItemInfo->usecnt);
					TEMP_CI.str.Set(buf);
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_DESC:
			{
				if (ItemInfo->C_desc.size())
				{
					if (strcmp(ItemInfo->C_desc.c_str(),"0") != 0)
					{
						TEMP_CI.str.Set(ItemInfo->C_desc.c_str());
						TEMP_CI.FontColor = GetColor(C_COLOR_DESC);
						BFine = TRUE;
					}
				}
			}
			break;
		case ITEM_TIP_SELLPRICE:
			{
				if (!m_pkData && ItemInfo->SellPrice)
				{
					char buf[1024];
					sprintf_s(buf,1024,_TRAN("出售单价： "));
					TEMP_CI.str.Set(buf);
					TEMP_CI.FontColor = GetColor(C_COLOR_PAY);
					float Price = ItemPrice(ItemInfo, false);
					
					m_Money.SetMoney(Price);

					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_REPAIR_PRICE:
			{
				float Price = ItemPrice(ItemInfo, true);
				if (Item && Item->GetDurability() < Item->GetMaxDurability())
				{
					ui32 NeedMoney = Price * ((float)Item->GetMaxDurability() - (float)Item->GetDurability()) / (float)Item->GetMaxDurability();
					NeedMoney = NeedMoney ? NeedMoney : 1;
					char buf[_MAX_PATH];
					sprintf_s(buf,_MAX_PATH,_TRAN("修理价格： "));
					TEMP_CI.str.Set(buf);
					TEMP_CI.FontColor = GetColor(C_COLOR_PAY);
					m_RepairPrice.SetMoney(NeedMoney);
					BFine = TRUE;
				}
			}
			break ;
		case ITEM_TIP_BUYPRICE:
			{
				if (!m_pkData)
				{
					char buf[1024];
					sprintf_s(buf,1024,_TRAN("购买价格： "));
					TEMP_CI.str.Set(buf);
					TEMP_CI.FontColor = GetColor(C_COLOR_PAY);

					float Price = ItemPrice(ItemInfo, true);
					m_Money.SetMoney(Price);

					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_REQUIREDPLAYERRANK:
			{
				bool isItemWork = TitleMgr->IsTitleInPlayer(ItemInfo->RequiredPlayerRank1);
				if (!isItemWork)
				{
					std::string temp;
					if(TitleMgr->GetTitleStr(ItemInfo->RequiredPlayerRank1, temp))
					{
						//sprintf(buf, "需要称号： %s",temp.c_str());

						std::string strRet = _TRAN(N_NOTICE_Z91, temp.c_str());

						TEMP_CI.str.Set(strRet.c_str());
						TEMP_CI.FontColor = LCOLOR_RED;
						BFine = TRUE;
					}
				}
			}
			break;
		case EQUIPSET_NAME:
			{
				stItemSetSingel tempSet;
				CPlayer* pPlayer = (CPlayer*)ObjectMgr->GetObject(m_Item_PlayerGuid);
				if( g_ItemSetDB->GetItemSetSingel(mEntry, tempSet) )
				{
					int SetCount = 0;
					int CurCount = 0;
					for (int i = 0 ; i < 8 ; ++i)
					{
						if (pPlayer)
							CurCount += pPlayer->IsPlayerEquip( tempSet.itemID[i] );
						SetCount += BOOL(tempSet.itemID[i] > 0);
					}
					char buf[256];
					sprintf(buf, "%s(%d/%d)", tempSet.name.c_str(), CurCount, SetCount);
					TEMP_CI.LineHAdd = 10;
					TEMP_CI.str.Set(buf);
					TEMP_CI.FontColor.Set(255, 202, 121, 255);
					BFine = TRUE;
				}
			}
			break;
		case EQUIPSET_EQUIP_NAME:
			{
				stItemSetSingel tempSet;
				CPlayer* pPlayer = (CPlayer*)ObjectMgr->GetObject(m_Item_PlayerGuid);
				if( g_ItemSetDB->GetItemSetSingel(mEntry, tempSet) )
				{
					for (int i = 0 ; i < 8 ; ++i)
					{
						ItemPrototype_Client* SetItemInfo = ItemMgr->GetItemPropertyFromDataBase(tempSet.itemID[i]);
						if (SetItemInfo)
						{
							char buf[256];
							if (pPlayer && pPlayer->IsPlayerEquip( tempSet.itemID[i] ))
							{
								sprintf(buf, "%s + %d", SetItemInfo->C_name.c_str(), pPlayer->GetJingLianLevel( tempSet.itemID[i] ));
								TEMP_CI.str.Set(buf);
								TEMP_CI.FontColor = GetColor(C_COLOR_XQ);
							}
							else
							{
								TEMP_CI.str.Set(SetItemInfo->C_name.c_str());
								TEMP_CI.FontColor = GetColor(C_COLOR_ITEMJUNK);
							}
							m_Content.push_back(TEMP_CI);
						}
					}
					BFine = FALSE;
				}
			}
			break;
		case EQUIPSET_PROPERTY:
			{
				stItemSetSingel tempSet;
				CPlayer* pPlayer = (CPlayer*)ObjectMgr->GetObject(m_Item_PlayerGuid);
				if( g_ItemSetDB->GetItemSetSingel(mEntry, tempSet) )
				{
					int CurCount = 0;
					bool CurEquipjlLevAllMax = true;
					for (int i = 0 ; i < 8 ; ++i)
					{
						if (pPlayer &&  pPlayer->IsPlayerEquip( tempSet.itemID[i] ))
						{
							CurCount++;
							CurEquipjlLevAllMax &= pPlayer->GetJingLianLevel(tempSet.itemID[i]) == 9;
						}
					}
					int index = 0 ;
					for (int i = 7 ; i >= 0; --i)
					{
						if(tempSet.spell[i].SpellID)
						{
							index = i;
							break;;
						}
					}
					for (int i = 0 ; i < 8 ; ++i)
					{
						if(tempSet.spell[i].itemscount)
						{
							SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)tempSet.spell[i].SpellID);
							if(pSpellTemplate)
							{
								std::string _tempbuf;
								if(pSpellTemplate->GetDescription(_tempbuf) && _tempbuf.length())
								{
									char buf[256];
									if (index == i)
									{
										if (CurCount >= tempSet.spell[i].itemscount && CurEquipjlLevAllMax)
										{
											sprintf(buf, "(%d)%s:",  tempSet.spell[i].itemscount, _TRAN("套装"));
											TEMP_CI.FontColor = GetColor(C_COLOR_ITEMUNCOMMON);
										}
										else
										{
											sprintf(buf, "(%d)%s(%s%d):", tempSet.spell[i].itemscount, _TRAN("套装"), _TRAN("需要套装精炼等级"), 9);
											TEMP_CI.FontColor = GetColor(C_COLOR_ITEMJUNK);
										}
									}else
									{
										if (CurCount >= tempSet.spell[i].itemscount)
										{
											sprintf(buf, "(%d)%s:",  tempSet.spell[i].itemscount, _TRAN("套装"));
											TEMP_CI.FontColor = GetColor(C_COLOR_ITEMUNCOMMON);
										}
										else
										{
											sprintf(buf, "(%d)%s:", tempSet.spell[i].itemscount, _TRAN("套装"));
											TEMP_CI.FontColor = GetColor(C_COLOR_ITEMJUNK);
										}
									}
									TEMP_CI.str.Set((buf + _tempbuf).c_str());
									m_Content.push_back(TEMP_CI);
								}
							}
						}
					}
					BFine = FALSE;
				}
			}
			break;
		case ITEM_TIP_DURABILITY:
			{
				if (Item)
				{
					//sprintf_s(buf,1024,"装备耐久： %d/%d",Item->GetDurability(),Item->GetMaxDurability());

					std::string strRet = _TRAN(N_NOTICE_Z92, _I2A(Item->GetDurability()).c_str(), _I2A(Item->GetMaxDurability()).c_str());

					if (Item->GetDurability() == 0)
					{
						TEMP_CI.FontColor = GetColor(C_COLOR_WARNING);
					}else if (Item->GetDurability() <= 10)
					{
						TEMP_CI.FontColor = UColor(255,255,0);
					}
					TEMP_CI.str.Set(strRet.c_str());
					BFine = TRUE;
				}
			}
			break;
		case ITME_TIP_REQUIRESKILL:
			{
				if (ItemInfo->RequiredSkill)
				{
					PlayerProfessionLevel* pProf = pkLocal->GetProfessionState(ItemInfo->RequiredSkill);

					if (!pkLocal->IsLearnProfession(ItemInfo->RequiredSkill) || pProf->current < ItemInfo->RequiredSkillRank)
					{
						std::string name;
						if(g_SkillLineNameInfo->GetSkillLineName(ItemInfo->RequiredSkill, name))
						{
							char buf[256];
							sprintf(buf, "%s(%u)", name.c_str(), ItemInfo->RequiredSkillRank);
							TEMP_CI.str.Set(buf);
							TEMP_CI.FontColor = GetColor(C_COLOR_WARNING);
							BFine = TRUE;
						}
					}
				}
			}
			break;
		case ITEM_TIP_ARMOR:
			{
				
				ui32 armor = ItemInfo->Armor;
				if (m_ItemSpecProperty.IsCreate)
				{
					ui32 RateArmor = ItemMgr->GetItemRefineRate(mEntry , 1) * m_ItemSpecProperty.itemdata.jinglian_level ;
					armor += RateArmor;
				}
				
				//sprintf_s(buf,1024,"%d点护甲",armor);

				std::string strRet = _TRAN(N_NOTICE_Z93, _I2A(armor).c_str());

				TEMP_CI.FontColor = GetColor(C_COLOR_ITEMPROPERTY);
				TEMP_CI.str.Set(strRet.c_str());
				BFine = TRUE;
			}
			break;
		case ITEM_TIP_STACKCOUNT:
			{
				std::string strRet;
				//SYItem * Item = (SYItem *)ObjectMgr->GetObject(mID);
				if(Item && ItemInfo->MaxCount > 1)
				{
					strRet = _TRAN(N_NOTICE_Z94, _I2A(Item->GetUInt32Value(ITEM_FIELD_STACK_COUNT)).c_str(), _I2A(ItemInfo->MaxCount).c_str() );
					TEMP_CI.str.Set(strRet.c_str());
					BFine = TRUE;
				}else if (m_Count > 0)
				{
					strRet = _TRAN(N_NOTICE_Z94, _I2A(m_Count).c_str(), _I2A(ItemInfo->MaxCount).c_str() );	
					TEMP_CI.str.Set(strRet.c_str());
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_ISSPELLLEARN:
			{
				char buf[1024];
				ui32 SpellId = ItemInfo->Spells[1].Id;
				if (SpellId && pkLocal->HasLearnSkill(SpellId))
				{
					sprintf_s(buf, 1024, _TRAN("此技能已经学会"));
					TEMP_CI.FontColor = GetColor(C_COLOR_WARNING);
					TEMP_CI.str.Set(buf);
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_ALLOWCLASS:
			{
				//适用职业：武修 羽箭 仙道 真巫
				CPlayer * pLocalPlayer = ObjectMgr->GetLocalPlayer();
				if (pLocalPlayer)
				{
					std::string classStr = _TRAN("适用职业： ");
					int ClassMask = 1 + 2 + 4 + 8;
					if(ItemInfo->AllowableClass & CLASS_WARRIOR)
					{
						classStr += _TRAN("武修 ");
						ClassMask -= 1;
					}
					if(ItemInfo->AllowableClass & 2)
					{
						classStr += _TRAN("羽箭 ");
						ClassMask -= 2;
					}
					if(ItemInfo->AllowableClass & 4)
					{
						classStr += _TRAN("仙道 ");
						ClassMask -= 4;
					}
					if(ItemInfo->AllowableClass & 8)
					{
						classStr += _TRAN("真巫 ");
						ClassMask -= 8;
					}
					if (!ClassMask)
					{
						classStr = _TRAN("适用职业： 所有职业");
						TEMP_CI.str.Set(classStr.c_str());
						BFine = TRUE;
					}
					else
					{
						if (!(ItemInfo->AllowableClass & pLocalPlayer->GetClassMask()))
						{
							TEMP_CI.FontColor = GetColor(C_COLOR_WARNING);
						}
						TEMP_CI.str.Set(classStr.c_str());
						BFine = TRUE;
					}
				}
			}
			break;
		case ITEM_TIP_ITEMLIFETIME:
			{
				//SYItem * Item = (SYItem *)ObjectMgr->GetObject(mID);
				if (Item && Item->GetUInt32Value(ITEM_FIELD_LIFE_STYLE) /*&& TipSystem->mIsUpdateServerTime*/)
				{
					
					std::string strBuff;
					uint32 lifetime = Item->GetUInt32Value(ITEM_FIELD_LIFE_VALUE1) - (TipSystem->mServerTime + time(NULL) - TipSystem->mLastUpdate);
					uint32 daytime = 3600 * 24;
					if (lifetime > daytime)
					{
						uint32 Iday = 0;
						Iday = lifetime / daytime;
						//sprintf_s(buf, 256, "剩余时间： %d天", 
						strBuff = _TRAN(N_NOTICE_Z96, _I2A(Iday).c_str() );
					}
					else
					{
						uint32 Hourtime = 3600;
						//if (lifetime > Hourtime)
						{
							uint32 Ihour = 0;
							Ihour = lifetime / Hourtime;
							//sprintf_s(buf, 256, "剩余时间： %d小时", Ihour);
							strBuff = _TRAN(N_NOTICE_Z97, _I2A(Ihour + 1).c_str() );
						}
					/*	else
						{
							uint32 ISecond = 0,  IMinite = 0;
							IMinite = lifetime / 60;
							strBuff = _TRAN( N_NOTICE_F7, _I2A(IMinite).c_str() );
						}*/
					}
					TEMP_CI.str.Set(strBuff.c_str());
					BFine = TRUE;
				}
			}
			break;
		case ITEM_TIP_WENXINTISHI:
			{
				TEMP_CI.FontColor = GetColor(C_COLOR_OTHER);
				TEMP_CI.str.Set(_TRAN("[CTRL+鼠标左击物品试穿！]"));
				BFine = TRUE;
			}
			break;
        case ITEM_TIP_LUOPANSHI:
            {
                //SYItem * Item = (SYItem *)ObjectMgr->GetObject(mID);
                if( Item )
                {
                    //罗盘石和回城石
                    if( Item->GetItemProperty()->ItemId == 5
                     || Item->GetItemProperty()->ItemId == 11)
                    {
							unsigned int uiAreaId = Item->GetUInt32Value(ITEM_FIELD_RECORD_ZONEID);
							const AreaInfo* pkAreaInfo = g_pkAreaDB->GetAreaInfo(uiAreaId);

                       /* char buf[256];*/
							std::string strBuff;
							if(uiAreaId > 0 && pkAreaInfo)
							{
								strBuff = _TRAN(N_NOTICE_Z98, (const char*)pkAreaInfo->kName);
							}
							else
							{
								strBuff = _TRAN("没有记录传送点");
							}
						   
							
						   TEMP_CI.FontColor = UColor(0, 200, 0);
                        TEMP_CI.str.Set(strBuff.c_str());
                        BFine = TRUE;
                    }
                }
            }
            break;

		case PAYDATA_START:
			{
				if (m_pkData)
				{
					TEMP_CI.FontColor = GetColor(C_COLOR_PAY);
					TEMP_CI.str.Set(_TRAN("需要支付"));
					BFine = TRUE;
				}
			}
			break;
		case PAYDATA_ITEM_NAME:
			{
				if (m_pkData && m_pkData->mItemId)
				{
					ItemPrototype_Client* PayItemInfo = ItemMgr->GetItemPropertyFromDataBase(m_pkData->mItemId);
					if (PayItemInfo)
					{
						char buf[256];
						sprintf_s(buf, sizeof(buf), "%d * %s",m_pkData->mItemCount, (const char*)PayItemInfo->C_name.c_str());

						TEMP_CI.FontColor = GetColor(C_COLOR_PAY);
						TEMP_CI.str.Set(buf);
						BFine = TRUE;
					}
				}
			}
			break;
		case PAYDATA_ITEM_GOLD:
			{
				if (m_pkData && m_pkData->mMoney)
				{
					TEMP_CI.FontColor = GetColor(C_COLOR_PAY);
					TEMP_CI.str.Set(_TRAN("金币"));
					if (m_Count > 0)
					{
						m_Money.SetMoney(m_pkData->mMoney);
					}
					BFine = TRUE;	
				}
				
			}break;
		case PAYDATA_GULD_SCORE:
			{
				if (m_pkData && m_pkData->mGuid_Jifen)
				{
					char buf[256];
					sprintf_s(buf, sizeof(buf), "%d", m_pkData->mGuid_Jifen);

					TEMP_CI.FontColor = GetColor(C_COLOR_PAY);
					TEMP_CI.str.Set(buf);
					BFine = TRUE;
				}
				
			}break;
		case PAYDATA_GULD_CONTRIBTTE:
			{
				if (m_pkData && m_pkData->mGuid_Gongxian)
				{
					char buf[256];
					sprintf_s(buf, sizeof(buf), "%d", m_pkData->mGuid_Gongxian);

					TEMP_CI.FontColor = GetColor(C_COLOR_PAY);
					TEMP_CI.str.Set(buf);
					BFine = TRUE;
				}
				

			}break;
		case PAYDATA_HONOR:
			{
				if ( m_pkData && m_pkData->mHonor)
				{
					char buf[256];
					sprintf_s(buf, sizeof(buf), "%d", m_pkData->mHonor);

					TEMP_CI.FontColor = GetColor(C_COLOR_PAY);
					TEMP_CI.str.Set(buf);
					BFine = TRUE;
				}
				
			}break;
		case PAYDATA_ARENA:
			{
				if ( m_pkData && m_pkData->mArena)
				{
					char buf[256];
					sprintf_s(buf, sizeof(buf), "%d", m_pkData->mArena);

					TEMP_CI.FontColor = GetColor(C_COLOR_PAY);
					TEMP_CI.str.Set(buf);
					BFine = TRUE;
				}
			}break;
		case PAYDATA_YUANBAO:
			{
				if (m_pkData && m_pkData->mYuanbao)
				{
					TEMP_CI.FontColor = GetColor(C_COLOR_PAY);
					TEMP_CI.str.Set(_TRAN("元宝"));
					if (m_Count > 0)
					{
						m_Money.SetMoney(m_pkData->mYuanbao, TRUE);
					}
					BFine = TRUE;	
				}
			}break;
		}

		if (BFine)
		{
			m_Content.push_back(TEMP_CI);
		}
	}
}
int ItemTip::DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect)
{
	BOOL bStartSTATS = FALSE;
	int height = m_BoardWide;
	IUFont* font = NULL;
	for (size_t i = 0 ;  i < m_Content.size() ; i++)
	{
		if(m_Content[i].FontFace)
			font = m_Content[i].FontFace;
		else
			font = pCtrlStl->m_spFont;

		switch (m_Content[i].Type)
		{
		default:
			{
				height += m_Content[i].LineHAdd;
				DrawToolTipText(Pos, updateRect, m_BoardWide, height, pRender, font, m_Content[i].str, m_Content[i].FontColor, m_Content[i].ta, m_Content[i].bChangeLine);
			}
			break;
		case ITEM_TIP_MONEYCOUNT:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
				size.y = font->GetHeight();
				UDrawText(pRender,font,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				m_Money.DrawMoney(pRender,font,pos,size,UT_RIGHT);
				height += size.y;
			}
			break;
		case ITEM_TIP_YUANBAOCOUNT:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
				size.y = font->GetHeight();
				UDrawText(pRender,font,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				m_Money.DrawMoney(pRender,font,pos,size,UT_RIGHT);
				height += size.y;
			}
			break;
		case ITEM_TIP_HOLE_1:
		case ITEM_TIP_HOLE_2:
		case ITEM_TIP_HOLE_3:
			{
				height += 5;
				UPoint pos,size; 
				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;
				int WideAdd = 0;
				if (m_Content[i].Icon)
				{
					pRender->DrawSkin(m_Content[i].Icon, m_Content[i].IconIndex, URect(pos, UPoint(16, 16)));
					WideAdd = 19;
				}
				int AddHeight = max(font->GetHeight(), 16);
				size.x = updateRect.GetWidth() - 2*m_BoardWide - WideAdd;
				size.y = font->GetHeight();
				pos.x += WideAdd;
				pos.y += (AddHeight - font->GetHeight()) / 2;
				UDrawText(pRender,font,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				height += AddHeight;
			}
			break;
		case ITEM_TIP_STATS:
			{
				if (!bStartSTATS)
				{
					height += 5;
					bStartSTATS = TRUE ;
				}
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
				size.y = UDrawStaticText(pRender, font,pos,size.x,m_Content[i].FontColor,m_Content[i].str, UT_LEFT, 5);
				height += size.y;
			}
			break;
		case ITEM_TIP_DESC:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
				size.y = UDrawStaticText(pRender,font,pos,size.x,m_Content[i].FontColor,m_Content[i].str, UT_LEFT, 5);
				height += size.y;
			}
			break;
		case ITEM_TIP_ITEMSPELLDES:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
				size.y = UDrawStaticText(pRender,font,pos,size.x,m_Content[i].FontColor,m_Content[i].str, UT_LEFT, 5);
				height += size.y;
			}
			break;
		case ITEM_TIP_SELLPRICE:
			{
				if (!m_pkData)
				{
					height += 5;
					UPoint pos,size; 

					pos.x = Pos.x + m_BoardWide;
					pos.y = Pos.y + height;

					size.x = updateRect.GetWidth() - 2*m_BoardWide;

					size.y = font->GetHeight();
					UDrawText(pRender,font,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
					m_Money.DrawMoney(pRender,font,pos,size,UT_RIGHT);
					height += size.y;
				}
			}
			break;
		case ITEM_TIP_BUYPRICE:
			{
				if (!m_pkData)
				{
					height += 5;
					UPoint pos,size; 

					pos.x = Pos.x + m_BoardWide;
					pos.y = Pos.y + height;

					size.x = updateRect.GetWidth() - 2*m_BoardWide;
					size.y = font->GetHeight();
					UDrawText(pRender,font,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
					m_Money.DrawMoney(pRender,font,pos,size,UT_RIGHT);
					height += size.y;	
				}
			}
			break;
		case PAYDATA_ITEM_NAME:
			{
				if (m_pkData && m_pkData->mItemId)
				{
					height += 5; 
					UPoint pos,size; 
					pos.x = Pos.x + m_BoardWide;
					pos.y = Pos.y + height;
					size.x = updateRect.GetWidth() - 2*m_BoardWide;
					size.y = font->GetHeight();

					UDrawText(pRender,font,pos,size,m_Content[i].FontColor,PayDateName[0].c_str(),UT_LEFT);
					UDrawText(pRender,font,pos,size - UPoint(19,0),m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
					
					if (m_PayDataItemSkin)
					{
						URect PayItemRec(UPoint(pos.x + size.x - 16,  pos.y - 1), UPoint(16,16));
						pRender->DrawSkin(m_PayDataItemSkin, m_PayDataItemIndex, PayItemRec);
						if (size.y <= 16)
							size.y = 16;
					}
					height += size.y;
				}
			}
			break;
		case ITEM_TIP_REPAIR_PRICE:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				size.y = font->GetHeight();
				UDrawText(pRender, font,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				m_RepairPrice.DrawMoney(pRender,font,pos,size,UT_RIGHT);
				height += size.y;
			}
			break;
		case PAYDATA_ITEM_GOLD:
			{
				if (m_pkData && m_pkData->mMoney)
				{
					height += 5;
					UPoint pos,size; 

					pos.x = Pos.x + m_BoardWide;
					pos.y = Pos.y + height;

					size.x = updateRect.GetWidth() - 2*m_BoardWide;
					size.y = font->GetHeight();
					UDrawText(pRender,font,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
					m_Money.DrawMoney(pRender,font,pos,size,UT_RIGHT);
					height += size.y;
				}
			}
			break;
		case PAYDATA_GULD_SCORE:
			{
				if (m_pkData && m_pkData->mGuid_Jifen)
				{
					height += 5;
					UPoint pos,size; 

					pos.x = Pos.x + m_BoardWide;
					pos.y = Pos.y + height;

					size.x = updateRect.GetWidth() - 2*m_BoardWide;

					size.y = font->GetHeight();
					UDrawText(pRender,font,pos,size,m_Content[i].FontColor,PayDateName[1].c_str(),UT_LEFT);
					UDrawText(pRender,font,pos,size - UPoint(19,0),GetColor(C_COLOR_DEFAULT),m_Content[i].str,UT_RIGHT);
					if (m_Texture[1])
					{
						URect PayItemRec(UPoint(pos.x + size.x - 16,  pos.y - 1), UPoint(16,16));
						URect ImageRec(0, 0,m_Texture[1]->GetWidth(), m_Texture[1]->GetHeight());
						pRender->DrawImage(m_Texture[1],PayItemRec, ImageRec);
						if (size.y <= 16)
							size.y = 16 ;
					}
					height += size.y;
				}
			}
			break;
		case PAYDATA_GULD_CONTRIBTTE:
			{
				if (m_pkData && m_pkData->mGuid_Gongxian)
				{
					height += 5;
					UPoint pos,size; 

					pos.x = Pos.x + m_BoardWide;
					pos.y = Pos.y + height;

					size.x = updateRect.GetWidth() - 2*m_BoardWide;
					size.y = font->GetHeight();
					UDrawText(pRender,font,pos,size,m_Content[i].FontColor,PayDateName[2].c_str(),UT_LEFT);
					UDrawText(pRender,font,pos,size - UPoint(19,0) ,GetColor(C_COLOR_DEFAULT),m_Content[i].str,UT_RIGHT);
					if (m_Texture[2])
					{
						URect PayItemRec(UPoint(pos.x + size.x - 16,  pos.y), UPoint(16,16));
						URect ImageRec(0, 0,m_Texture[2]->GetWidth(), m_Texture[2]->GetHeight()) ;
						pRender->DrawImage(m_Texture[2],PayItemRec, ImageRec);
						if (size.y <= 16)
							size.y = 16 ;
					}
					height += size.y;
				}
			}
			break;
		case PAYDATA_HONOR:
			{
				if ( m_pkData && m_pkData->mHonor)
				{
					height += 5;
					UPoint pos,size; 

					pos.x = Pos.x + m_BoardWide;
					pos.y = Pos.y + height;

					size.x = updateRect.GetWidth() - 2*m_BoardWide;
					size.y = font->GetHeight();
					UDrawText(pRender,font,pos,size,m_Content[i].FontColor,PayDateName[3].c_str(),UT_LEFT);
					UDrawText(pRender,font,pos,size - UPoint(19,0),GetColor(C_COLOR_DEFAULT),m_Content[i].str,UT_RIGHT);
					if (m_Texture[0])
					{
						URect PayItemRec(UPoint(pos.x + size.x - 16,  pos.y -1), UPoint(16,16));
						URect ImageRec(0, 0,m_Texture[0]->GetWidth(), m_Texture[0]->GetHeight()) ;
						pRender->DrawImage(m_Texture[0],PayItemRec, ImageRec);
						if (size.y <= 16)
							size.y = 16 ;
					}
					height += size.y;
				}

			}break;
		case PAYDATA_ARENA:
			{
				if ( m_pkData && m_pkData->mArena)
				{
					height += 5;
					UPoint pos,size; 

					pos.x = Pos.x + m_BoardWide;
					pos.y = Pos.y + height;

					size.x = updateRect.GetWidth() - 2*m_BoardWide;
					size.y = font->GetHeight();
					UDrawText(pRender,font,pos,size,m_Content[i].FontColor,PayDateName[4].c_str(),UT_LEFT);
					UDrawText(pRender,font,pos,size - UPoint(19,0),GetColor(C_COLOR_DEFAULT),m_Content[i].str,UT_RIGHT);
					if (m_Texture[3])
					{
						URect PayItemRec(UPoint(pos.x + size.x - 16,  pos.y - 1), UPoint(16,16));
						URect ImageRec(0, 0, m_Texture[3]->GetWidth(), m_Texture[3]->GetHeight());
						pRender->DrawImage(m_Texture[3],PayItemRec, ImageRec);
						if (size.y <= 16)
							size.y = 16 ;
					}
					height += size.y;
				}
			}break;
		case PAYDATA_YUANBAO:
			{
				if (m_pkData && m_pkData->mYuanbao)
				{
					height += 5;
					UPoint pos,size; 

					pos.x = Pos.x + m_BoardWide;
					pos.y = Pos.y + height;

					size.x = updateRect.GetWidth() - 2*m_BoardWide;
					size.y = font->GetHeight();
					UDrawText(pRender,font,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
					m_Money.DrawMoney(pRender,font,pos,size,UT_RIGHT);
					height += size.y;
				}
			}break;
		case ITEM_TIP_WENXINTISHI:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
				size.y = UDrawStaticText(pRender,font,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				height += size.y;
			}
			break;
		case EQUIPSET_EQUIP_NAME:
			{
				height += m_Content[i].LineHAdd;
				UPoint postion = Pos;
				postion.x += 10;
				DrawToolTipText(postion, updateRect, m_BoardWide, height, pRender, font, m_Content[i].str, m_Content[i].FontColor, m_Content[i].ta, m_Content[i].bChangeLine);
			}
			break;
		case EQUIPSET_PROPERTY:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
				size.y = UDrawStaticText(pRender,font,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				height += size.y;
			}
			break;
		}
	}
	return height + m_BoardWide;
}
void ItemTip::SeparateItem(int ItemClass,int SubClass)
{

	//if (m_pkData)
	{
		InitTexture(UControl::sm_UiRender);
	}

	if (m_IsComTip)
	{
		m_Item_PlayerGuid = ObjectMgr->GetLocalPlayer()->GetGUID();
		CreateContentItem(ITEM_TIP_CURRENTEQUIP);
	}

	switch (ItemClass)
	{
	case ITEM_CLASS_CONSUMABLE:
	case ITEM_CLASS_QUEST:
	case ITEM_CLASS_CONTAINER:
		{
			CreateContentItem(ITEM_TIP_NAME);
			CreateContentItem(ITEM_TIP_BONDING);
			CreateContentItem(ITEM_TIP_SELLALLOWABLE);
			
			CreateContentItem(ITEM_TIP_SUBCLASS);
			CreateContentItem(ITEM_TIP_REFINELEVEL);
			CreateContentItem(ITEM_TIP_DESC);
			CreateContentItem(ITEM_TIP_ITEMSPELLDES);
			CreateContentItem(ITEM_TIP_REQUIRELEVEL);
			
			CreateContentItem(ITEM_TIP_ALLOWCLASS);
			CreateContentItem(ITME_TIP_REQUIRESKILL);
			
			
			CreateContentItem(ITEM_TIP_ISSPELLLEARN);

			CreateContentItem(ITEM_TIP_ITEMLIFETIME);
			CreateContentItem(ITEM_TIP_STACKCOUNT);
			CreateContentItem(ITEM_TIP_ITEMUSECOUNT);
		
			CreateContentItem(ITEM_TIP_LUOPANSHI);
			CreateContentItem(ITEM_TIP_SELLPRICE);
			CreateContentItem(ITEM_TIP_REQUIREDPLAYERRANK);
		}
		break;
	case ITEM_CLASS_WEAPON:
		{
			CreateContentItem(ITEM_TIP_NAME);

			CreateContentItem(ITEM_TIP_BONDING);
			CreateContentItem(ITEM_TIP_SELLALLOWABLE);
			
			
			CreateContentItem(ITEM_TIP_SUBCLASS);
			CreateContentItem(ITEM_TIP_REFINELEVEL);


			CreateContentItem(ITEM_TIP_DAMAGE);
			CreateContentItem(ITEM_TIP_SPEED);
			CreateContentItem(ITEM_TIP_DPS);
			if (SubClass == ITEM_SUBCLASS_WEAPON_SHIELD)
			{
				CreateContentItem(ITEM_TIP_ARMOR);
			}
			CreateContentItem(ITEM_TIP_STATS);
			//开孔相关
			//CreateContentItem(ITEM_TIP_HOLE_CNT);
			CreateContentItem(ITEM_TIP_HOLE_1);
			CreateContentItem(ITEM_TIP_HOLE_2);
			CreateContentItem(ITEM_TIP_HOLE_3);
			CreateContentItem(ITEM_TIP_SOCKETBONUS);

			CreateContentItem(ITEM_TIP_ITEMSPELLDES);

			CreateContentItem(ITEM_TIP_REQUIRELEVEL);
			CreateContentItem(ITEM_TIP_ALLOWCLASS);
			CreateContentItem(ITME_TIP_REQUIRESKILL);
			//CreateContentItem(ITEM_TIP_DESC);
			CreateContentItem(ITEM_TIP_ITEMLIFETIME);
			CreateContentItem(ITEM_TIP_DURABILITY);
			CreateContentItem(ITEM_TIP_REQUIREDPLAYERRANK);

			CreateContentItem(EQUIPSET_NAME);
			CreateContentItem(EQUIPSET_EQUIP_NAME);
			CreateContentItem(EQUIPSET_PROPERTY);

			CreateContentItem(ITEM_TIP_SELLPRICE);
			CreateContentItem(ITEM_TIP_REPAIR_PRICE);
			
			if (!m_IsComTip)
			{
				CreateComparaTip();
			}
			if (m_pkData)
			{
				CreateContentItem(PAYDATA_START);
				CreateContentItem(PAYDATA_YUANBAO);
				CreateContentItem(PAYDATA_ITEM_GOLD);
				CreateContentItem(PAYDATA_HONOR);
				CreateContentItem(PAYDATA_GULD_SCORE);
				CreateContentItem(PAYDATA_GULD_CONTRIBTTE);
				CreateContentItem(PAYDATA_ARENA);
				CreateContentItem(PAYDATA_ITEM_NAME);
			}
			CreateContentItem(ITEM_TIP_WENXINTISHI);
			return ;
		}
		break;
	case ITEM_CLASS_ARMOR:
		{
			CreateContentItem(ITEM_TIP_NAME);

			CreateContentItem(ITEM_TIP_BONDING);
			CreateContentItem(ITEM_TIP_SELLALLOWABLE);
			

			CreateContentItem(ITEM_TIP_SUBCLASS);

			CreateContentItem(ITEM_TIP_REFINELEVEL);
			if (SubClass != ITEM_SUBCLASS_ARMOR_FINGER	
				&& SubClass !=ITEM_SUBCLASS_ARMOR_NECK 
				&& SubClass !=ITEM_SUBCLASS_ARMOR_TRINKET)
			{
				CreateContentItem(ITEM_TIP_ARMOR);
			}


			CreateContentItem(ITEM_TIP_STATS);
			//开孔相关
			//CreateContentItem(ITEM_TIP_HOLE_CNT);
			CreateContentItem(ITEM_TIP_HOLE_1);
			CreateContentItem(ITEM_TIP_HOLE_2);
			CreateContentItem(ITEM_TIP_HOLE_3);
			CreateContentItem(ITEM_TIP_SOCKETBONUS);

			CreateContentItem(ITEM_TIP_ITEMSPELLDES);

			CreateContentItem(ITEM_TIP_REQUIRELEVEL);
			CreateContentItem(ITEM_TIP_ALLOWCLASS);
			CreateContentItem(ITME_TIP_REQUIRESKILL);
			//CreateContentItem(ITEM_TIP_DESC);
			CreateContentItem(ITEM_TIP_ITEMLIFETIME);
			/*if (SubClass != ITEM_SUBCLASS_ARMOR_FINGER	
				&& SubClass !=ITEM_SUBCLASS_ARMOR_NECK 
				&& SubClass !=ITEM_SUBCLASS_ARMOR_TRINKET)
			*/{
				CreateContentItem(ITEM_TIP_DURABILITY);
			}
			
			CreateContentItem(ITEM_TIP_REQUIREDPLAYERRANK);
			
			CreateContentItem(EQUIPSET_NAME);
			CreateContentItem(EQUIPSET_EQUIP_NAME);
			CreateContentItem(EQUIPSET_PROPERTY);

			if (!m_IsComTip)
			{
				CreateComparaTip();
			}

			CreateContentItem(ITEM_TIP_SELLPRICE);
			CreateContentItem(ITEM_TIP_REPAIR_PRICE);
			if (m_pkData)
			{
				CreateContentItem(PAYDATA_START);
				CreateContentItem(PAYDATA_YUANBAO);
				CreateContentItem(PAYDATA_ITEM_GOLD);
				CreateContentItem(PAYDATA_HONOR);
				CreateContentItem(PAYDATA_GULD_SCORE);
				CreateContentItem(PAYDATA_GULD_CONTRIBTTE);
				CreateContentItem(PAYDATA_ARENA);
				CreateContentItem(PAYDATA_ITEM_NAME);

			}
			CreateContentItem(ITEM_TIP_WENXINTISHI);
			return ;

		}
		break;
	case ITEM_CLASS_JEWELRY:
		{
			CreateContentItem(ITEM_TIP_NAME);
			CreateContentItem(ITEM_TIP_GEMPROPERTY);
			CreateContentItem(ITEM_TIP_DESC);
		}
		break;
	case ITEM_CLASS_REAGENT:
		{
			CreateContentItem(ITEM_TIP_NAME);
			CreateContentItem(ITEM_TIP_DESC);
		}	
		break;
	case ITEM_CLASS_RECIPE:
		{
			CreateContentItem(ITEM_TIP_NAME);
			CreateContentItem(ITEM_TIP_BONDING);
			CreateContentItem(ITEM_TIP_SELLALLOWABLE);
			
			CreateContentItem(ITEM_TIP_SUBCLASS);
			CreateContentItem(ITEM_TIP_REFINELEVEL);
			CreateContentItem(ITEM_TIP_DESC);
			CreateContentItem(ITEM_TIP_REQUIRELEVEL);
			CreateContentItem(ITEM_TIP_ALLOWCLASS);
			CreateContentItem(ITME_TIP_REQUIRESKILL);

			CreateContentItem(ITEM_TIP_ISSPELLLEARN);
			

			CreateContentItem(ITEM_TIP_STACKCOUNT);
			CreateContentItem(ITEM_TIP_ITEMUSECOUNT);
			CreateContentItem(ITEM_TIP_SELLPRICE);
			CreateContentItem(ITEM_TIP_REQUIREDPLAYERRANK);
		}	
		break;
	case ITEM_CLASS_MONEY:
		{
			//CreateContentItem(ITEM_TIP_NAME);
			if (SubClass == ITEM_SUBCLASS_YUANBAO)
			{
				CreateContentItem(ITEM_TIP_YUANBAOCOUNT);
			}else if (SubClass == ITEM_SUBCLASS_GOLD)
			{
				CreateContentItem(ITEM_TIP_MONEYCOUNT);
			}
			
		}
		break;
	case ITEM_CLASS_MISCELLANEOUS:
		{
			CreateContentItem(ITEM_TIP_NAME);
			CreateContentItem(ITEM_TIP_DESC);
		}
		break;
	case ITEM_CLASS_USE:
		{
			CreateContentItem(ITEM_TIP_NAME);
			CreateContentItem(ITEM_TIP_BONDING);
			CreateContentItem(ITEM_TIP_SELLALLOWABLE);
			
			CreateContentItem(ITEM_TIP_SUBCLASS);
			CreateContentItem(ITEM_TIP_REFINELEVEL);
			CreateContentItem(ITEM_TIP_DESC);
			CreateContentItem(ITEM_TIP_REQUIRELEVEL);
			CreateContentItem(ITME_TIP_REQUIRESKILL);

			CreateContentItem(ITEM_TIP_ITEMLIFETIME);
			CreateContentItem(ITEM_TIP_ITEMUSECOUNT);
            CreateContentItem(ITEM_TIP_LUOPANSHI);
			CreateContentItem(ITEM_TIP_SELLPRICE);
			CreateContentItem(ITEM_TIP_REQUIREDPLAYERRANK);
		}
		break;
	}

	if (m_pkData)
	{
		CreateContentItem(PAYDATA_START);
		CreateContentItem(PAYDATA_YUANBAO);
		CreateContentItem(PAYDATA_ITEM_GOLD);
		CreateContentItem(PAYDATA_HONOR);
		CreateContentItem(PAYDATA_GULD_SCORE);
		CreateContentItem(PAYDATA_GULD_CONTRIBTTE);
		CreateContentItem(PAYDATA_ARENA);
		CreateContentItem(PAYDATA_ITEM_NAME);
		
	}
	return ;
}
void ItemTip::CreateComparaTip()
{
	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(mEntry);
	UInGame * pInGame = UDynamicCast(UInGame,TipSystem->GetRootCtrl());
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	if (ItemInfo && pInGame && pkPlayer)
	{
		if (UInGame::GetFrame(FRAME_IG_PLAYEREQUIPDLG)->CursorInWindow())
		{
			return;
		}
		CEquipmentContainer* pkEquipContainer = (CEquipmentContainer*)pkPlayer->GetContainer(ICT_EQUIP);
		if (pkEquipContainer)
		{
			CEquipSlot *pkEquipSlot = NULL;
			if (ItemInfo->InventoryType == INVTYPE_SHIELD || ItemInfo->InventoryType == INVTYPE_BAOZHU)
			{
				pkEquipSlot = (CEquipSlot*)pkEquipContainer->GetSlot(INVTYPE_WEAPONOFFHAND);
			}
			else	if (ItemInfo->InventoryType == INVTYPE_2HWEAPON)
			{
				pkEquipSlot = (CEquipSlot*)pkEquipContainer->GetSlot(INVTYPE_WEAPONMAINHAND);
			}
			else	if (ItemInfo->InventoryType < 20)
			{
				pkEquipSlot = (CEquipSlot*)pkEquipContainer->GetSlot(ItemInfo->InventoryType);
			}
			if (pkEquipSlot && pkEquipSlot->GetCode())
			{
				TipSystem->ShowTip(TIP_TYPE_ITEM_COMPARA1,pkEquipSlot->GetCode(),UPoint(0,0),pkEquipSlot->GetGUID());
			}
		}
	}
}
