#ifndef ITEMTIP_H
#define ITEMTIP_H
#include "MouseTip.h"
#include "UPayDataInc.h"
#include "UIMoneyText.h"

//物品鼠标提示应该是最为复杂的一个
//里面总共分为几种类型 一般物品的 装备 武器 还有一些任务道具等等 这些的渲染模式都不一样
//用现在这种模式能不能正确的将内容渲染出来呢？
//在装备和武器渲染过程中 会有一个装备对比框的构造这个对比框又该如何实现呢？

//*********************************  //*******************************
//* 装备名称                      *  //* 物品名称                    *
//* 是否绑定              可否出售*  //* 是否绑定           可否出售 *
//* 子类                          *  //* 使用等级                    *
//* 武器伤害               ?? - ??*  //* 物品说明                    *
//* 附加属性 +?                   *  //* 出售价格(买入价格)          *
//* 附加属性 +?                   *  //* 最大堆叠                    *
//* ...............               *  //*******************************
//* 装备等级                      *
//* 装备说明                      *
//* 出售价格（买入价格）          *
//* 装备耐久                      *
//*********************************

//这两个是装备和物品的鼠标提示,后面就按照这两个情况来进行渲染,在这个里面出现了物品说明这个需要渲染换行的说明
//用到一个静态文本的渲染，现在总共有3中 
//1.一般左对其或者右对齐的一般文本渲染
//2.需要换行的文本渲染
//3.钱币的渲染


enum
{
	ITEM_TIP_NAME                  = 0,
	ITEM_TIP_BONDING,
	
	ITEM_TIP_REFINELEVEL, //精炼等级
	
	ITEM_TIP_SELLALLOWABLE,
	ITEM_TIP_SUBCLASS,
	ITEM_TIP_DAMAGE,
	ITEM_TIP_SPEED,
	ITEM_TIP_DPS,
	ITEM_TIP_STATS,
	
	//开孔设置
	ITEM_TIP_HOLE_CNT,  //开孔数量
	ITEM_TIP_HOLE_1,	//镶嵌位置1 
	ITEM_TIP_HOLE_2,	//镶嵌位置2
	ITEM_TIP_HOLE_3,	//镶嵌位置3
	ITEM_TIP_SOCKETBONUS,

	ITEM_TIP_REQUIRELEVEL,
	ITEM_TIP_DESC,
	ITEM_TIP_SELLPRICE,
	ITEM_TIP_BUYPRICE,
	ITEM_TIP_DURABILITY,
	ITEM_TIP_CURRENTEQUIP,
	ITEM_TIP_ARMOR,
	ITEM_TIP_STACKCOUNT,
	ITEM_TIP_ALLOWCLASS,
	ITEM_TIP_MONEYCOUNT,
	ITEM_TIP_YUANBAOCOUNT,
	ITEM_TIP_ISSPELLLEARN,
	ITEM_TIP_ITEMLIFETIME,
    ITEM_TIP_LUOPANSHI,
	ITEM_TIP_REQUIREDPLAYERRANK,
	ITEM_TIP_REPAIR_PRICE, //修理价格

	ITEM_TIP_GEMPROPERTY,
	ITME_TIP_REQUIRESKILL,
	ITEM_TIP_ITEMSPELLDES,
	ITEM_TIP_ITEMUSECOUNT,
	//////////////////////////////////////////////////////////////////////////

	PAYDATA_START,

	PAYDATA_ITEM_NAME  ,  //需要支付物品名字
	//PAYDATA_ITEM_DESCRIPTION,			//需要支付物品描述
	PAYDATA_ITEM_GOLD,					//金币
	PAYDATA_GULD_SCORE,					//部族积分
	//PAYDATA_GULD_SCORE_DESCRIPTION,     //部族积分描述
	PAYDATA_GULD_CONTRIBTTE,			//部族贡献
	//PAYDATA_GULD_CONTRIBTTE_DESCRIPTION,//部族贡献描述
	PAYDATA_HONOR,						//荣誉值
	//PAYDATA_HONOR_DESCRIPTION,			//荣誉值描述
	PAYDATA_ARENA,						//PVP竞技积分
	//PAYDATA_ARENA_DESCRIPTION,			//PVP竞技积分描述
	PAYDATA_YUANBAO,					//元宝

	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////

	EQUIPSET_NAME,
	EQUIPSET_EQUIP_NAME,
	EQUIPSET_PROPERTY,


	//////////////////////////////////////////////////////////////////////////
	ITEM_TIP_WENXINTISHI,

	ITEM_TIP_MAX,
};
struct ItemSpecProperty
{
	bool IsCreate;
	ui32 ItemEntry;
	ItemExtraData itemdata;
	//
	//int PropertyNum;
	//std::vector<ui32> vEnchantId;
	//ui32 RateRefine;
	//ui32 RefineLevel;
};
class ItemTip : public MouseTip
{
	struct ContentItem  
	{
		ContentItem():Type(ITEM_TIP_MAX),FontFace(NULL),FontColor(UColor(255,255,255,255)),LineHAdd(5),ta(UT_LEFT),bChangeLine(true),Icon(NULL){}
		UStringW str;
		UColor FontColor;
		UFontPtr FontFace;
		int Type;
		int LineHAdd;
		EUTextAlignment ta;
		bool bChangeLine;
		USkinPtr Icon;
		int IconIndex;
	};
public:
	ItemSpecProperty m_ItemSpecProperty;
	ItemTip();
	~ItemTip();
public:
	void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos);
	void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos, PayData* pkData, USkinPtr PayItemSkin, UINT SkinIndex);
	void ReCreateTip();
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count);
	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl , const UPoint &pos,const URect &updateRect);
	virtual void DestoryTip();
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect);
	virtual void CreateContentItem(int Type);
private:
	void SeparateItem(int ItemClass,int SubClass);
	void CreateComparaTip();
	void BulidSpecProperty();
	BOOL InitTexture(URender * pRender);
	std::vector<ContentItem> m_Content;
	UMoney m_Money;
	UMoney m_RepairPrice;
	UINT m_Count;
	BOOL m_IsComTip;
	PayData* m_pkData;
	std::string PayDateName[5];
	UTexturePtr m_Texture[4];
	USkinPtr m_SocketSkin;
	USkinPtr m_PayDataItemSkin;
	UINT m_PayDataItemIndex;
	uint64 m_Item_PlayerGuid;
};
#endif