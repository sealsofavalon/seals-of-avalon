#include "StdAfx.h"
#include "KeySetControl.h"
#include "SystemSetup.h"
#include "UFun.h"

UIMP_CLASS(GetKeyCodeButton, UControl)
void GetKeyCodeButton::StaticInit()
{

}
GetKeyCodeButton::GetKeyCodeButton()
{
	m_bPress = FALSE;
	m_PressTexture = NULL;
	m_ReleaseTexture = NULL;
}
GetKeyCodeButton::~GetKeyCodeButton()
{
	m_bPress = FALSE;
	m_PressTexture = NULL;
	m_ReleaseTexture = NULL;
}

BOOL GetKeyCodeButton::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_PressTexture == NULL)
	{
		m_PressTexture = sm_UiRender->LoadTexture("DATA\\UI\\SystemSet\\Press.png");
		if (!m_PressTexture)
		{
			return FALSE;
		}
	}
	if (m_ReleaseTexture == NULL)
	{
		m_ReleaseTexture = sm_UiRender->LoadTexture("DATA\\UI\\SystemSet\\Release.png");
		if (!m_ReleaseTexture)
		{
			return FALSE;
		}
	}
	m_KeyBuffer.Set(_TRAN("Not Set"));
	return TRUE;
}
void GetKeyCodeButton::OnDestroy()
{
	m_bPress = FALSE;
	UControl::OnDestroy();
}
void GetKeyCodeButton::SetKeyCode( UINT nKeyCode, UINT flag )
{
	if (nKeyCode == LK_NULL)
	{
		m_KeyBuffer.Set(_TRAN("Not Set"));
	}
	else
	{
		std::string KeyStr;	
		if (flag & LKM_CTRL)
		{
			KeyStr += "Ctrl+";
		}
		if (flag & LKM_ALT)
		{
			KeyStr += "Alt+";
		}
		if (flag & LKM_SHIFT)
		{
			KeyStr += "Shift+";
		}
		KeyStr += LK2StringKey(nKeyCode);
		m_KeyBuffer.Set(KeyStr.c_str());
	}
	KeySetItem* pItem = UDynamicCast(KeySetItem, GetParent());
	if (pItem)
	{
		pItem->OnSetKeyStr(nKeyCode, flag);
	}
}
void GetKeyCodeButton::SetActive(BOOL value)
{
	if (!value)
	{
		m_KeyBuffer.Set(_TRAN("(不可用)"));
	}
	UControl::SetActive(value);
}
BOOL GetKeyCodeButton::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	if (nKeyCode == LK_ESCAPE)
	{
		return TRUE;
	}	
	if (nKeyCode != LK_RALT && nKeyCode != LK_LALT && 
		nKeyCode != LK_RCONTROL && nKeyCode != LK_LCONTROL && 
		nKeyCode != LK_RSHIFT && nKeyCode != LK_LSHIFT)
	{
		//SetKeyCode(nKeyCode, nFlags);
		KeySetItem* pItem = UDynamicCast(KeySetItem, GetParent());
		if (pItem)
		{
			pItem->OnKeyChange(nKeyCode, nFlags);
		}
		ClearFocus();
		m_bPress = FALSE;
		ReleaseCapture();
	}
	return TRUE;
}
void GetKeyCodeButton::OnRender(const UPoint& offset,const URect &updateRect)
{
	URect RenderRect(offset, m_Size);
	//if(updateRect.GetIntersection(RenderRect) != RenderRect)
	//	return;
	if (m_bPress)
	{
		sm_UiRender->DrawImage(m_PressTexture, RenderRect);
	}
	else
	{
		sm_UiRender->DrawImage(m_ReleaseTexture, RenderRect);
	}

	UDrawText(sm_UiRender, m_Style->m_spFont, offset, m_Size, 
		m_Style->m_FontColor, m_KeyBuffer.GetBuffer(), UT_CENTER);

	RenderChildWindow(offset, updateRect);
}
BOOL GetKeyCodeButton::OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	if (nKeyCode == LK_ESCAPE)
	{
		ReleaseCapture();
		ClearFocus();
		m_bPress = FALSE;
		return TRUE;
	}
	else
	{
		return UControl::OnKeyUp(nKeyCode, nRepCnt, nFlags);
	}
}

void GetKeyCodeButton::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	if (!IsCaptured())
	{
		m_bPress = TRUE;
		CaptureControl();
		SetFocusControl();
	}
	else
	{
		if (!GetControlRect().PointInRect(point))
		{
			ReleaseCapture();
			ClearFocus();
			m_bPress = FALSE;
		}
	}
}
void GetKeyCodeButton::OnMouseUp(const UPoint& position, UINT flags)
{
	//m_bPress = FALSE;
}
//////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(KeySetItem, UControl);
void KeySetItem::StaticInit()
{
	UREG_PROPERTY("RealName", UPT_STRING, UFIELD_OFFSET(KeySetItem, m_KeyNameStr));
	UREG_PROPERTY("OpCode", UPT_STRING, UFIELD_OFFSET(KeySetItem, m_KeyOpCode));
}
KeySetItem::KeySetItem()
{
	m_KeyName = NULL;
	m_AcKeyButton = NULL;
	m_BackAcKeyButton = NULL;
}
KeySetItem::~KeySetItem()
{

}
BOOL KeySetItem::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_KeyName == NULL)
	{
		m_KeyName = UDynamicCast(UStaticText, GetChildByID(0));
		if ( m_KeyName )
		{
			m_KeyName->SetTextAlign(UT_LEFT);
			if (m_KeyNameStr.GetBuffer())
			{
				m_KeyName->SetText(m_KeyNameStr.GetBuffer());
			}
			else
			{
				m_KeyName->SetText(_TRAN("未定义"));
			}
		}
		else
		{
			return FALSE;
		}
	}
	if (m_AcKeyButton == NULL)
	{
		m_AcKeyButton = UDynamicCast(GetKeyCodeButton, GetChildByID(1));
		if (!m_AcKeyButton)
		{
			return FALSE;
		}
		else
		{
			SetKeyStr();	
			m_KeyBuff.Save();
		}
	}
	if (m_BackAcKeyButton == NULL)
	{
		m_BackAcKeyButton = UDynamicCast(GetKeyCodeButton, GetChildByID(2));
		if (!m_BackAcKeyButton)
		{
			return FALSE;
		}
		else
		{
			m_BackAcKeyButton->SetActive(FALSE);
		}
	}
	return TRUE;
}
void KeySetItem::OnDestroy()
{
	UControl::OnDestroy();
}
void KeySetItem::OnSetKeyStr(UINT nKeyCode, UINT nFlag)
{
	if (!m_bCreated)
		return;
	m_KeyBuff.VALUE(nKeyCode, nFlag);
}
void KeySetItem::OnKeyChange(UINT nKeyCode, UINT nFlag)
{
	KeySetcontrol* pParent = UDynamicCast(KeySetcontrol, GetParent());
	if (pParent)
	{
		pParent->OnOneItemChange(m_KeyOpCode.GetBuffer(), nKeyCode, nFlag);
	}
}
BOOL KeySetItem::IsTheSameKey(UINT nKeyCode, UINT nFlag)
{
	return m_KeyBuff.KeyCode == nKeyCode && m_KeyBuff.Flags == nFlag;
}
BOOL KeySetItem::Save()
{
	if (sm_System)
	{
		m_KeyBuff.Save();
		return sm_System->SetAcceleratorKey(m_KeyOpCode.GetBuffer(), m_KeyBuff.KeyCode, m_KeyBuff.Flags);
	}
	return FALSE;
}
void KeySetItem::Quit()
{
	m_KeyBuff.UNDO();
}
void KeySetItem::SetKeyStr()
{
	if (m_KeyOpCode.GetBuffer())
	{
		UAccelarKey* pKey = sm_System->FindAcceleratorKey(m_KeyOpCode.GetBuffer());
		if (pKey)
		{
			std::string KeyCode;
			std::string Modify;
			UINT flag = 0;
			if (pKey->mAccelarKeyModify.GetBuffer())
			{
				Modify = pKey->mAccelarKeyModify.GetBuffer();
			}
			if (pKey->mAccelarKeyCode.GetBuffer())
			{
				KeyCode = pKey->mAccelarKeyCode.GetBuffer();
				if (Modify.size())
				{		
					if (Modify.find("LCONTROL") != -1)
					{
						flag |= LKM_CTRL;
					}
					if (Modify.find("LALT") != -1)
					{
						flag |= LKM_ALT;
					}
					if (Modify.find("LSHIFT") != -1)
					{
						flag |= LKM_SHIFT;
					}
				}
			}
			if (pKey->mAccelarKeyCode.GetBuffer())
			{
				UINT KeyCode = StringKey2LK(pKey->mAccelarKeyCode.GetBuffer());
				m_AcKeyButton->SetKeyCode(KeyCode, flag);
				m_KeyBuff.VALUE(KeyCode, flag);
				//m_KeyBuff.Save();
			}
		}
	}
}
void KeySetItem::ReSetKeyStr()
{
	SetKeyStr();
	m_KeyBuff.Save();
}
void KeySetItem::SetKeyStrByCurrentBuff()
{
	if (m_AcKeyButton)
	{
		m_AcKeyButton->SetKeyCode(m_KeyBuff.KeyCode, m_KeyBuff.Flags);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(KeySetcontrol, UControl);
void KeySetcontrol::StaticInit()
{

}
KeySetcontrol::KeySetcontrol()
{

}
KeySetcontrol::~KeySetcontrol()
{

}
BOOL KeySetcontrol::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	AddKey();
	return TRUE;
}
void KeySetcontrol::OnDestroy()
{
	DelKey();
	UControl::OnDestroy();
}

void KeySetcontrol::QuitWithoutSave()
{
	//SystemSetup->LoadFile(FILE_USERKEYMAP);
	KeySetItemMap::iterator it = m_KeySetItemMap.begin();
	while(it != m_KeySetItemMap.end())
	{
		it->second->Quit();
		it->second->Save();
		it->second->ReSetKeyStr();
		++it;
	}
}
void KeySetcontrol::SaveWithoutQuit()
{
	KeySetItemMap::iterator it = m_KeySetItemMap.begin();
	while(it != m_KeySetItemMap.end())
	{
		it->second->Save();
		++it;
	}
	SystemSetup->SaveFile();
}
void KeySetcontrol::QuitAndSave()
{
	KeySetItemMap::iterator it = m_KeySetItemMap.begin();
	while(it != m_KeySetItemMap.end())
	{
		it->second->Save();
		++it;
	}
	SystemSetup->SaveFile();
}
void KeySetcontrol::SetDefault()
{
	KeySetItemMap::iterator it = m_KeySetItemMap.begin();
	while(it != m_KeySetItemMap.end())
	{
		it->second->SetKeyStr();
		++it;
	}
}
void KeySetcontrol::AddKey()
{
	int height = 4;
	int size = sizeof(addkeyctrl)/sizeof(addkeyctrl[0]);
	for(int i = 0 ; i < size ; i++)
	{
		if(CreateTitle(_TRAN(addkeyctrl[i].Title), height))
		{
			int num = addkeyctrl[i].length;
			for(int j = 0 ; j < num ; j++)
			{
				CreateKeyItem(addkeyctrl[i].pstOpcode[j].OpCode, _TRAN(addkeyctrl[i].pstOpcode[j].ShowTxt), height);
			}
		}
	}
	m_Size.y = height;
}
void KeySetcontrol::DelKey()
{
	m_KeySetItemMap.clear();
	while(m_Childs.size())
	{
		UControl* ctrl = m_Childs[0];
		if (ctrl)
		{
			RemoveChild(ctrl);
			ctrl->DeleteThis();
		}
	}
}
BOOL KeySetcontrol::CreateTitle(const char* title, int &Height)
{
	UStaticText* Temp = (UStaticText*)UStaticText::CreateObject();
	if (!Temp)
		return FALSE;
	Temp->SetField("style", "SYSetTitle");
	Temp->SetField("text",  UTF8ToAnis(title).c_str());
	Temp->SetSize( UPoint(GetWidth(), 19) );
	Temp->SetPosition( UPoint(0, Height) );
	Temp->SetTextAlign(UT_LEFT);
	this->AddChild(Temp);
	Height += Temp->GetHeight() + 4;
	return TRUE;
}
BOOL KeySetcontrol::CreateKeyItem(const char* OpCode, const char* RealName, int& Height)
{
	KeySetItem* Temp = (KeySetItem*)sm_System->CreateDialogFromFile("SystemSet\\KeyItem.udg");
	if (!Temp)
		return FALSE;
	Temp->SetField("RealName", UTF8ToAnis(RealName).c_str());
	Temp->SetField("OpCode", OpCode);
	Temp->SetPosition( UPoint(0, Height) );
	this->AddChild(Temp);
	Height += Temp->GetHeight() + 5;
	m_KeySetItemMap.insert(KeySetItemMap::value_type(OpCode, Temp));
	return TRUE;
}
#include "UMessageBox.h"
KeySetItem* pChangeItem = NULL;
KeySetItem* pbChangeItem = NULL;
UINT gnKeyCode = 0;
UINT gnflag = 0;

void IsChangeKey()
{
	if(!pChangeItem || !pbChangeItem)
		return;
	pbChangeItem->OnSetKeyStr(0, 0);
	pbChangeItem->SetKeyStrByCurrentBuff();

	pChangeItem->OnSetKeyStr(gnKeyCode, gnflag);
	pChangeItem->SetKeyStrByCurrentBuff();
}
void DoNothing()
{

}
void KeySetcontrol::OnOneItemChange(const char* OpCode, UINT nKeyCode, UINT nFlag)
{
	KeySetItemMap::iterator it2 = m_KeySetItemMap.find(OpCode);
	if(it2 == m_KeySetItemMap.end())
		return;

	KeySetItemMap::iterator it = m_KeySetItemMap.begin();
	while (it != m_KeySetItemMap.end())
	{
		if (it == it2)
		{
			++it;
			continue;
		}
		if(it->second->IsTheSameKey(nKeyCode, nFlag))
		{
			pbChangeItem = it->second;
			pChangeItem = it2->second;
			gnKeyCode = nKeyCode;
			gnflag = nFlag;
			UMessageBox::MsgBox(_TRAN("已经存在的快捷键，是否修改"), (BoxFunction*)&IsChangeKey,  (BoxFunction*)&DoNothing);
			return;
		}
		++it;
	}
	 it2->second->OnSetKeyStr(nKeyCode, nFlag);
	 it2->second->SetKeyStrByCurrentBuff();
}

void KeySetcontrol::OnRender(const UPoint& offset, const URect &updateRect)
{
	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
    g_pRenderInterface->SetScissorRect(sm_UiRender->GetClipRect());

    UControl::OnRender(offset, updateRect);

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
}