#ifndef KEYSETCONTROL_H
#define KEYSETCONTROL_H
#include "UInc.h"
enum KeyType
{
	KEYTYPE_SYSTEM,
	KEYTYPE_MOVEMENT,
	KEYTYPE_MAINACTIONBAR,
	KEYTYPE_ADDACTIONBAR1,
	KEYTYPE_ADDACTIONBAR2,
	KEYTYPE_ADDACTIONBAR3,
	KEYTYPE_ADDACTIONBAR4,
	KEYTYPE_ADDACTIONBAR5,
};
class GetKeyCodeButton : public UControl
{
	UDEC_CLASS(GetKeyCodeButton);
public:
	GetKeyCodeButton();
	~GetKeyCodeButton();
public:
	void SetKeyCode(UINT nKeyCode, UINT flag );
	virtual void SetActive(BOOL value);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
protected:
	UTexturePtr m_PressTexture;
	UTexturePtr m_ReleaseTexture;
	UString m_KeyBuffer;
	BOOL m_bPress;
};
class KeySetItem : public UControl
{
	struct KeyBuff
	{
		KeyBuff():KeyCode(0),Flags(0),UNDOKeyCode(0),UNDOFlags(0){}
		UINT KeyCode;
		UINT Flags;
		UINT UNDOKeyCode;
		UINT UNDOFlags;
		void VALUE(UINT keycode, UINT flags)
		{
			KeyCode = keycode;
			Flags = flags;
		}
		void UNDO()
		{
			KeyCode = UNDOKeyCode;
			Flags = UNDOFlags;
		}
		void Save()
		{
			UNDOKeyCode = KeyCode;
			UNDOFlags = Flags;
		}
	};
	UDEC_CLASS(KeySetItem);
public:
	KeySetItem();
	~KeySetItem();
public:
	void SetKeyStr();
	void ReSetKeyStr();
	void SetKeyStrByCurrentBuff();

	void OnSetKeyStr(UINT nKeyCode, UINT nFlag);
	void OnKeyChange(UINT nKeyCode, UINT nFlag);

	BOOL IsTheSameKey(UINT nKeyCode, UINT nFlag);

	BOOL Save();
	void Quit();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
protected:
	UString m_KeyNameStr;
	UString m_KeyOpCode;
	UStaticText* m_KeyName;
	GetKeyCodeButton* m_AcKeyButton;
	GetKeyCodeButton* m_BackAcKeyButton;
	KeyBuff m_KeyBuff;
};
class KeySetcontrol : public UControl
{
	UDEC_CLASS(KeySetcontrol);
public:
	KeySetcontrol();
	~KeySetcontrol();
public:
	void OnOneItemChange(const char* OpCode, UINT nKeyCode, UINT nFlag);
public:
	void QuitWithoutSave();
	void SaveWithoutQuit();
	void QuitAndSave();
	void SetDefault();
    virtual void OnRender(const UPoint& offset, const URect &updateRect);

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	void AddKey();
	void DelKey();
protected:
	BOOL CreateTitle(const char* title, int &Height);
	BOOL CreateKeyItem(const char* OpCode, const char* RealName, int& Height);

	typedef std::map<std::string, KeySetItem*> KeySetItemMap;
	KeySetItemMap m_KeySetItemMap;
};
#endif