#include "StdAfx.h"
#include "MonsterTip.h"
#include "UITipSystem.h"
#include "Character.h"
#include "ObjectManager.h"
#include "UInc.h"
#include "UFun.h"
#include "ItemManager.h"
MonsterTip::MonsterTip()
{
}
MonsterTip::~MonsterTip()
{

}
void MonsterTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count)
{
	mEntry = ID;
	mID = guid;
	CCharacter * pkChar = (CCharacter*)ObjectMgr->GetObject(mID);
	if (pkChar)
	{
		m_Content.clear();
		m_KeepDraw = TRUE;
		Ctrl->SetVisible(TRUE);
		Ctrl->SetPosition(pos);
		if (pkChar->GetGameObjectType() == GOT_CREATURE)
		{
			CreateContentItem(MONSTER_TIP_NAME);
			CreateContentItem(MONSTER_TIP_STATE);
			CreateContentItem(MONSTER_TIP_GUILD);
			CreateContentItem(MONSTER_TIP_LEVLE);
			CreateContentItem(MONSTER_TIP_CLASS);
			CreateContentItem(MONSTER_TIP_HP);
			CreateContentItem(MONSTER_TIP_MP);
		}
		if(pkChar->GetGameObjectType() == GOT_PLAYER)
		{
			CreateContentItem(MONSTER_TIP_NAME);
			CreateContentItem(MONSTER_TIP_STATE);
			CreateContentItem(MONSTER_TIP_GUILD);
			CreateContentItem(MONSTER_TIP_RACE);
			CreateContentItem(MONSTER_TIP_LEVLE);
			CreateContentItem(MONSTER_TIP_CLASS);
		}
	}
}
void MonsterTip::DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl , const UPoint &pos,const URect &updateRect)
{
	if (m_KeepDraw)
	{
		UDrawResizeImage(RESIZE_WANDH, pRender, TipSystem->mBkgSkin, pos, updateRect.GetSize());
		int height = DrawContent(pRender,pCtrlStl,pos,updateRect);
		//设置当前提示框的高度
		if (height != Ctrl->GetHeight())
		{
			Ctrl->SetHeight(height);
		}
	}
}
void MonsterTip::DestoryTip()
{
	mEntry = 0;
	mID = 0;
	m_KeepDraw = FALSE;
	m_Content.clear();
}
void MonsterTip::CreateContentItem(int Type)
{
	CCharacter * pkChar = (CCharacter*)ObjectMgr->GetObject(mID);
	CCharacter* pLocal = ObjectMgr->GetLocalPlayer();
	BOOL BFine = FALSE;
	MonsterTip::ContentItem Temp_CI;
	if (pkChar)
	{
		switch (Type)
		{
		case MONSTER_TIP_NAME:
			{
				Temp_CI.Type = MONSTER_TIP_NAME;
				Temp_CI.str.Set(pkChar->GetName());
				if(pkChar->IsFriendly(pLocal))
					Temp_CI.FontColor = GetColor(C_COLOR_ITEMRARE);
				else if( pkChar->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT) )
					Temp_CI.FontColor = D3DCOLOR_ARGB(200, 253, 102, 0);
				else if(pkChar->IsHostile(pLocal))
					Temp_CI.FontColor = D3DCOLOR_ARGB(200, 253, 102, 0);
				else
					Temp_CI.FontColor = D3DCOLOR_ARGB(200, 255, 255, 0);
				if(pkChar->IsDead())
				{
					Temp_CI.FontColor.Set(128, 128, 128, 255);
				}
				Temp_CI.FontFace = TipSystem->GetNameFontFace();
				BFine = TRUE;
			}
			break;
		case MONSTER_TIP_RACE:
			{
				Temp_CI.Type = MONSTER_TIP_RACE;
				if(pkChar->GetGameObjectType() == GOT_PLAYER)
				{
					Temp_CI.str.Set(_TRAN(race_name[pkChar->GetRace()]));
					BFine = TRUE;
				}
			}
			break;
		case MONSTER_TIP_STATE:
			{
				Temp_CI.Type = MONSTER_TIP_STATE;
				if(pkChar->IsDead())
				{
					std::string strbuff = _TRAN("(无法战斗)");
					Temp_CI.str.Set(strbuff.c_str());
					Temp_CI.FontColor.Set(128, 128, 128, 255);
					BFine = TRUE;
				}
			}
			break;
		case MONSTER_TIP_GUILD:
			{
				Temp_CI.Type = MONSTER_TIP_GUILD;
				if (pkChar->GetGameObjectType() == GOT_CREATURE)
				{
					std::string title = pkChar->GetTitleStr();
					if(title.size())
					{
						Temp_CI.str.Set(('<' +title + '>').c_str());
						BFine = TRUE;
					}
				}
				if(pkChar->GetGameObjectType() == GOT_PLAYER)
				{
					CPlayer* CPlayer = pkChar->GetAsPlayer();
					if(CPlayer && CPlayer->GetGuildName().size())
					{
						Temp_CI.str.Set(CPlayer->GetGuildName().c_str());
						BFine = TRUE;
					}
				}
			}
			break;
		case MONSTER_TIP_LEVLE:
			{
				Temp_CI.Type = MONSTER_TIP_LEVLE;
				//char buf[256];
				//sprintf(buf, "等级  %u", pkChar->GetLevel());
				std::string strRet = _TRAN(N_NOTICE_Z99,  _I2A(pkChar->GetLevel()).c_str() );

				Temp_CI.FontColor.Set(0, 255, 0, 255);
				if(!pkChar->IsFriendly(pLocal))
				{
					if(pkChar->GetLevel()  > pLocal->GetLevel() + 10)
					{
						Temp_CI.FontColor.Set(255, 0, 0, 255);
					}
				}
				Temp_CI.str.Set(strRet.c_str());
				BFine = TRUE;
			}
			break;
		case MONSTER_TIP_CLASS:
			{
				Temp_CI.Type = MONSTER_TIP_CLASS;
				if (pkChar->GetGameObjectType() == GOT_CREATURE)
				{
					if(ItemMgr->IsCreatureNPC(pkChar->GetUInt32Value(OBJECT_FIELD_ENTRY))){
						Temp_CI.str.Set("NPC"); 
					}else
						Temp_CI.str.Set(_TRAN("怪物"));
					BFine = TRUE;
				}
				if(pkChar->GetGameObjectType() == GOT_PLAYER)
				{
					Temp_CI.str.Set(_TRAN(class_name[pkChar->GetClass()]));
					BFine = TRUE;
				}
			}
			break;
		case MONSTER_TIP_HP:
			{
				Temp_CI.Type = MONSTER_TIP_HP;
				if (pkChar->GetGameObjectType() == GOT_CREATURE && pkChar->GetMaxHP())
				{
					char buf[256];
					float fpos = float(pkChar->GetHP())/float(pkChar->GetMaxHP());
					sprintf(buf, "%5.2f%%", fpos * 100.f);

					if(fpos >= 0.f && fpos <= 0.f )
						Temp_CI.FontColor.Set(128, 128, 128, 255);
					else if(fpos >= 0.f && fpos <= 0.3f)
						Temp_CI.FontColor.Set(255, 0, 0, 255);
					else if(fpos >= 0.3f && fpos <= 0.6f)
						Temp_CI.FontColor.Set(255, 255, 0, 255);
					else if(fpos > 0.6f)
						Temp_CI.FontColor.Set(0, 255, 0, 255);
					Temp_CI.str.Set(buf);
					BFine = TRUE;
				}
			}
			break;
		case MONSTER_TIP_MP:
			{
				Temp_CI.Type = MONSTER_TIP_MP;
				if (pkChar->GetGameObjectType() == GOT_CREATURE && pkChar->GetMaxMP())
				{
					char buf[256];
					float fpos = float(pkChar->GetMP())/float(pkChar->GetMaxMP());
					sprintf(buf, "%5.2f%%", fpos * 100.f);
					Temp_CI.str.Set(buf);
					BFine = TRUE;
				}
			}
			break;
		}
		if (BFine)
		{
			m_Content.push_back(Temp_CI);
		}
	}
}
int MonsterTip::DrawContent(URender *pRender, UControlStyle * pCtrlStl,const UPoint &Pos, const URect &updateRect)
{
	int height = m_BoardWide;
	for (unsigned int i = 0 ; i < m_Content.size() ; i++)
	{
		switch (m_Content[i].Type)
		{
		case MONSTER_TIP_NAME:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				height += size.y;
			}
			break;		
		case MONSTER_TIP_STATE:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				height += size.y;
			}
			break;		
		case MONSTER_TIP_RACE:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				height += size.y;
			}
			break;
		case MONSTER_TIP_GUILD:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				height += size.y;
			}
			break;		
		case MONSTER_TIP_LEVLE:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				//height += size.y;
			}
			break;
		case MONSTER_TIP_CLASS:
			{
				//height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}

				height += size.y;
			}
			break;

		case MONSTER_TIP_HP:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;


				std::string strbuff = _TRAN("生命值：");
				wchar_t wcharbuff[64] = {0};
				::MultiByteToWideChar(CP_UTF8, 0, strbuff.c_str(), -1, wcharbuff, 64);

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawTextEx(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].FontColor,wcharbuff,m_Content[i].str);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawTextEx(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].FontColor,wcharbuff,m_Content[i].str);
				}

				height += size.y;
			}
			break;		

		case MONSTER_TIP_MP:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				std::string strbuff1 = _TRAN("魔法值：");
				wchar_t wcharbuff1[64] = {0};
				::MultiByteToWideChar(CP_UTF8, 0, strbuff1.c_str(), -1, wcharbuff1, 64);

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawTextEx(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].FontColor,wcharbuff1,m_Content[i].str);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawTextEx(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].FontColor,wcharbuff1,m_Content[i].str);
				}

				height += size.y;
			}
			break;		
		}
	}
	return height + m_BoardWide;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
InstanceTip::InstanceTip(){}
InstanceTip::~InstanceTip(){}
void InstanceTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count)
{
	mEntry = ID;

	char buf[256];
	sprintf(buf, "DATA\\UI\\Lobby\\InstanceToolTip\\ToolTip_%d.txt", mEntry);
	char* acBuffer = (char*)malloc( 256*1024 );
	NiFile* pkFile = NiFile::GetFile(buf, NiFile::READ_ONLY);
	if(!pkFile || !(*pkFile))
	{
		NiDelete pkFile;
		free( acBuffer );
		return;
	}
	unsigned int uiSize = pkFile->Read(acBuffer, 256*1024);
	NIASSERT(uiSize < 256*1024 - 1);

	acBuffer[uiSize] = '\0';
	NiDelete pkFile;

	URichTextCtrl* pRichText = UDynamicCast(URichTextCtrl, Ctrl->GetChildByID(0));
	if(pRichText)
	{
		string str = AnisToUTF8(acBuffer);
		pRichText->SetText(str.c_str(), str.length());
		pRichText->FormatContent();
		pRichText->SetHeight(pRichText->GetContentHeight());
		Ctrl->SetHeight(pRichText->GetContentHeight() + 3);
		pRichText->SetPosition(UPoint(3,3));
	}
	m_KeepDraw = TRUE;
	Ctrl->SetVisible(TRUE);
	Ctrl->SetPosition(pos);
	free( acBuffer );
}
void InstanceTip::DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect )
{
	if (m_KeepDraw)
	{
		UDrawResizeImage(RESIZE_WANDH, pRender, TipSystem->mBkgSkin, pos, updateRect.GetSize());
		DrawContent(pRender,pCtrlStl,pos,updateRect);
	}
}
void InstanceTip::DestoryTip()
{
	mEntry = 0;
	mID = 0;
	m_KeepDraw = FALSE;
}
int InstanceTip::DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect)
{
	return 0;
}
void InstanceTip::CreateContentItem(int Type){}