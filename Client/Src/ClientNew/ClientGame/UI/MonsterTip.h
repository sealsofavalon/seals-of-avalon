#ifndef MOUSTERTIP_H
#define MOUSTERTIP_H
#include "MouseTip.h"

//怪物鼠标提示。
enum
{
	MONSTER_TIP_NAME,
	MONSTER_TIP_STATE,
	MONSTER_TIP_RACE,
	MONSTER_TIP_GUILD,
	MONSTER_TIP_LEVLE,
	MONSTER_TIP_CLASS,
	MONSTER_TIP_HP,
	MONSTER_TIP_MP,
	MONSTER_TIP_MAX
};
class MonsterTip : public MouseTip
{
	struct ContentItem  
	{
		ContentItem():Type(MONSTER_TIP_MAX),FontFace(NULL),FontColor(UColor(255,255,255,255)){}
		UStringW str;
		UColor FontColor;
		UFontPtr FontFace;
		int Type;
	};
public:
	MonsterTip();
	~MonsterTip();
public:
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count);
	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect );
	virtual void DestoryTip();
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect);
	virtual void CreateContentItem(int Type);
private:
	vector<ContentItem> m_Content;
};

//副本鼠标提示。
//这个的格式由RichEdit来提供，策划写好文档即可

class InstanceTip : public MouseTip
{
public:
	InstanceTip();
	~InstanceTip();
public:
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count);
	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect );
	virtual void DestoryTip();
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect);
	virtual void CreateContentItem(int Type);
};
#endif