#ifndef MOUSETIP_H
#define MOUSETIP_H
#include "UInc.h"
enum
{
	TIP_TYPE_SPELL  = 0,
	TIP_TYPE_BUFF,
	TIP_TYPE_PLAYER,
	TIP_TYPE_MONSTER,
	TIP_TYPE_ITEM,
	TIP_TYPE_SCENEGAMEOBJ,
	TIP_TYPE_TITLE,
	TIP_TYPE_QUEST,
	TIP_TYPE_ITEM_COMPARA1,
	TIP_TYPE_ITEM_COMPARA2,
	TIP_TYPE_INSTANCE,
	TIP_TYPE_RECOUNT,
	TIP_TYPE_STRINGLIST,
	TIP_TYPE_PLAYERPROPERTY,
	TIP_TYPE_MAX
};

class MouseTip
{
public:
	MouseTip()
	{
		mEntry = 0;
		mID = 0;
		m_BoardWide = 5;
		m_KeepDraw = FALSE;
	}
	//创建提示框
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid ,UINT count) = 0;
	//销毁（也就是恢复默认数据
	virtual void DestoryTip() = 0;
	//绘制
	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl ,const UPoint &pos,const URect &updateRect) = 0;
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect) = 0;
	//创建单元栏中的数据
	virtual void CreateContentItem(int Type) = 0;
	virtual inline void SetType(int type){m_Type = type;}
	virtual int GetType()
	{
		if (m_Type < 0 || m_Type > TIP_TYPE_MAX){
			return TIP_TYPE_MAX;
		}
		return m_Type;
	}
protected:
	ui32 mEntry;
	ui64 mID;//物品ID 怪物ID 人物ID 技能ID
	int m_BoardWide;//提示框边界宽度
	BOOL m_KeepDraw;
	int m_Type;
};
#endif