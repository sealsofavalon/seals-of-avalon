#include "StdAfx.h"
#include "PlayerTip.h"
#include "Player.h"
#include "ObjectManager.h"
#include "UITipSystem.h"
#include "UIShowPlayer.h"
#include "UFun.h"
#include "UIIndex.h"
#include "UTeamDate.h"
#include "SocialitySystem.h"

PlayerTip::PlayerTip()
{
}
PlayerTip::~PlayerTip()
{

}
void PlayerTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count)
{
	mEntry = ID;
	mID = guid;
	CPlayer * pPlayer = ObjectMgr->GetPlayer(mID);
	if (pPlayer)
	{
		m_KeepDraw = TRUE;
		Ctrl->SetPosition(pos);
		Ctrl->SetVisible(TRUE);
	}
	else
	{
		TeamDate * di = SocialitySys->GetTeamSysPtr()->GetTeamDate(mID);
		if (di)
		{
			m_KeepDraw = TRUE;
			Ctrl->SetPosition(pos);
			Ctrl->SetVisible(TRUE);
		}
		else
		{
			return;
		}
	}
	if (m_Content.size() > 0)
	{
		m_Content.clear();
	}
	for (int i = 0 ; i < PLAYER_TIP_MAX ; i++)
	{
		CreateContentItem(i);
	}
}
void PlayerTip::DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl , const UPoint &pos,const URect &updateRect)
{
	if (m_KeepDraw)
	{
		//渲染边框以及内部填充区域
		UDrawResizeImage(RESIZE_WANDH, pRender, TipSystem->mBkgSkin, pos, updateRect.GetSize());
		//pRender->DrawRectFill(updateRect,pCtrlStl->mFillColor);
		//pRender->DrawRect(updateRect,pCtrlStl->mBorderColor);
		//渲染文字
		int height = DrawContent(pRender,pCtrlStl,pos,updateRect);
		//设置当前提示框的高度
		if (height != Ctrl->GetHeight())
		{
			Ctrl->SetHeight(height);
		}
	}
}
void PlayerTip::DestoryTip()
{
	mEntry = 0;
	mID = 0;
	m_KeepDraw = FALSE;
	m_Content.clear();
}
void PlayerTip::CreateContentItem(int Type)
{
	ContentItem Temp_CI;
	int mark = 0;
	CPlayer * pPlayer = ObjectMgr->GetPlayer(mID);
	TeamDate * di = SocialitySys->GetTeamSysPtr()->GetTeamDate(mID);
	if (pPlayer)
	{
		mark = 1;
	}
	else
	{
		if (di)
		{
			mark = 2;
		}
	}
	if (mark)
	{
		Temp_CI.Type = Type;
		switch (Type)
		{
		case PLAYER_TIP_NAME:
			{
				Temp_CI.FontFace = TipSystem->GetNameFontFace();
				if (mark == 1)
				{
					Temp_CI.str.Set(pPlayer->GetName());
					m_Content.push_back(Temp_CI);
				}
				else if (mark ==2)
				{
					Temp_CI.str.Set(di->Name.c_str());
					m_Content.push_back(Temp_CI);
				}
			}
			break;
		case PLAYER_TIP_CLASS:
			{
				if (mark == 1)
				{
					int Class = pPlayer->GetClass();
					Temp_CI.str.Set(_TRAN(class_name[Class]));
					m_Content.push_back(Temp_CI);
				}
				else if (mark ==2)
				{
					int Class = di->Class;
					
					Temp_CI.str.Set(_TRAN(class_name[Class]));
					m_Content.push_back(Temp_CI);
				}
			}
			break;
		case PLAYER_TIP_RACE:
			{
				if (mark == 1)
				{
					int race = pPlayer->GetRace();
					Temp_CI.str.Set(_TRAN(race_name[race]));
					m_Content.push_back(Temp_CI);
				}
				else if (mark ==2)
				{
					int race = di->Race;
					Temp_CI.str.Set(_TRAN(race_name[race]));
					m_Content.push_back(Temp_CI);
				}

			}
			break;
		case PlAYER_TIP_LEVEL:
			{
				if (mark == 1)
				{
					
					std::string strBuff;
					int lvl = pPlayer->GetLevel();
					if (lvl)
					{
						//sprintf_s(buf,1024,"等级%d",lvl);
						strBuff = _TRAN(N_NOTICE_Z100, _I2A(lvl).c_str() );
					}
					else
					{
						//sprintf_s(buf,1024,_TRAN("等级未知"));
						strBuff =_TRAN("等级未知");
					}
					
					Temp_CI.str.Set(strBuff.c_str() );
					m_Content.push_back(Temp_CI);
				}
				else if (mark ==2)
				{
					
					std::string strBuff;
					int lvl = di->lv;
					if (lvl)
					{
						//sprintf_s(buf,1024,"等级%d",lvl);
						strBuff = _TRAN(N_NOTICE_Z101, _I2A(lvl).c_str() );

					}
					else
					{
						//sprintf_s(buf,1024,_TRAN("等级未知"));

						strBuff = _TRAN("等级未知");
					}
					Temp_CI.str.Set(strBuff.c_str());
					m_Content.push_back(Temp_CI);
				}
			}
			break;
		case PLAYER_TIP_ISLOGIN:
			{
				Temp_CI.FontFace = TipSystem->GetNameFontFace();
				if (mark == 1)
				{
					Temp_CI.str.Set(_TRAN("在线"));
					Temp_CI.FontColor = LCOLOR_GREEN;
				}
				if (mark == 2)
				{
					if (!di->islogin)
					{
						Temp_CI.str.Set(_TRAN("离线"));
						Temp_CI.FontColor = GetColor(C_COLOR_WARNING);
					}
					else
					{
						Temp_CI.str.Set(_TRAN("在线"));
						Temp_CI.FontColor = LCOLOR_GREEN;
					}
				}
				m_Content.push_back(Temp_CI);
			}
			break;
		}
	}
}
int PlayerTip::DrawContent(URender *pRender,UControlStyle * pCtrlStl, const UPoint &Pos, const URect &updateRect)
{
	int height = m_BoardWide;
	for (unsigned int i = 0 ; i < m_Content.size() ; i++)
	{
		switch (m_Content[i].Type)
		{
		case PLAYER_TIP_NAME:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				//height += size.y;
			}
			break;
		case PLAYER_TIP_ISLOGIN:
			{
				//height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}

				height += size.y;
			}
			break;
		case PlAYER_TIP_LEVEL:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				//height += size.y;
			}
			break;
		case PLAYER_TIP_CLASS:
			{
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}

				height += size.y;
			}
			break;
		case PLAYER_TIP_RACE:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				height += size.y;
			}
			break;
		}
	}
	return height + m_BoardWide;
}