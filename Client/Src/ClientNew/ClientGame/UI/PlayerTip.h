#ifndef PLAYERTIP_H
#define PLAYERTIP_H
#include "MouseTip.h"
//--------------------------------------------
//玩家的鼠标提示（主要使用于组队的）
//--------------------------------------------

//*****************************
//*玩家姓名                   *
//*种族                       *
//*等级                职业   *
//*****************************
//后面渲染就按照这个来
enum
{
	PLAYER_TIP_NAME,
	PLAYER_TIP_ISLOGIN,
	PLAYER_TIP_RACE,
	PlAYER_TIP_LEVEL,
	PLAYER_TIP_CLASS,
	PLAYER_TIP_MAX
};
class PlayerTip : public MouseTip
{
	struct ContentItem 
	{
		ContentItem():Type(PLAYER_TIP_MAX),FontFace(NULL),FontColor(UColor(255,255,255,255)){}
		UStringW str;
		UColor FontColor;
		UFontPtr FontFace;
		int Type;
	};
public:
	PlayerTip();
	~PlayerTip();
public:
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count);
	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect );
	virtual void DestoryTip();
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect);
	virtual void CreateContentItem(int Type);
private:
	vector<ContentItem> m_Content;
};
#endif