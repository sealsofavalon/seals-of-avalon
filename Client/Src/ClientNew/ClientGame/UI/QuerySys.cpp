#include "StdAfx.h"
#include "QuerySys.h"
#include "UIIndex.h"
#include "SYItem.h"
#include "ItemManager.h"

#include "Utils/QuestDB.h"
#include "Utils/AreaDB.h"
#include "Utils/MapInfoDB.h"
#include "Utils/SpellDB.h"
#include "Utils/CreatureSpawnsDB.h"
#include "Utils/CPlayerTitleDB.h"


#include "UBigMap.h"
#include "UIGamePlay.h"
#include "UFun.h"
#include "UFittingRoom.h"

UIMP_CLASS(UQueryList, UControl)
void UQueryList::StaticInit()
{

}

void UQueryList::ListItem::ShowToolTip()
{
	TipSystem->ShowTip(EntryClass, entryID, Pos + Size);
}
void UQueryList::ListItem::HideToolTip()
{
	TipSystem->HideTip();
}
UQueryList::UQueryList()
{
	mColumHeight = 0;
	mItemHeight = 15;
	mItemHoverBkg = NULL;
	m_LastSelect = NULL;
	m_LastContent = NULL;
}

UQueryList::~UQueryList()
{

}

BOOL UQueryList::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (mItemHoverBkg == NULL)
	{
		mItemHoverBkg = sm_UiRender->LoadTexture("DATA\\UI\\QuerySystem\\ContentBkg.png");
		if (!mItemHoverBkg)
			return FALSE;
	}
	return TRUE;
}

void UQueryList::OnDestroy()
{
	UControl::OnDestroy();
}

void UQueryList::OnRender(const UPoint& offset,const URect &updateRect)
{
	DrawColumHead(offset);
	DrawGirdItem(offset);
}

void UQueryList::OnMouseEnter(const UPoint& position, UINT flags)
{

}
void UQueryList::OnMouseLeave(const UPoint& position, UINT flags)
{
	SetSelect(NULL);
}

void UQueryList::OnMouseMove(const UPoint& position, UINT flags)
{
	std::vector<ListItem>::iterator it = m_ListItem.begin();
	while(it != m_ListItem.end())
	{
		if (URect(it->Pos, it->Size).PointInRect(position))
		{
			SetSelect(&*it);
			return;
		}
		else
		{
			SetSelect(NULL);	
		}
		++it;
	}
}

void UQueryList::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	if (m_LastSelect)
	{
		for (unsigned int ui = 0 ; ui < m_LastSelect->vItemContent.size() ; ui++)
		{
			ItemContent* pItemConten = &m_LastSelect->vItemContent[ui];
			if (URect(pItemConten->Pos, pItemConten->Size).PointInRect(point))
			{
				if (pItemConten->UserData)
				{
					m_LastContent = pItemConten;
					m_LastContent->bKeyDown = true;
					CaptureControl();
					stQueryItemContent* pData = (stQueryItemContent*)pItemConten->UserData;
					switch (pData->type)
					{
					case QueryItemContentType_FindPath:
						{
							QueryMgr->FindPathMethod(pData->mapid, pData->kPos.x, pData->kPos.y, pData->kPos.z);
						}
						break;
					case QueryItemContentType_BigMap:
						{
							UBigMapFrame* pBigMap = INGAMEGETFRAME(UBigMapFrame, FRAME_IG_BIGMAP);
							if (pBigMap)
								pBigMap->ShowBigMap(pData->mapid);
						}
					case QueryItemContentType_FittingEquip:
						{
							if(uFlags == LKM_CTRL)
							{
								UFittingRoom* pFittingRoom = INGAMEGETFRAME(UFittingRoom, FRAME_IG_FITTINGROOMDLG);
								if(!pFittingRoom)
									return;
								pFittingRoom->FittingEquipment(pData->Itementry, (ItemExtraData*)NULL);
							}
						}
						break;
					}
				}
				return;
			}
		}
	}
}

void UQueryList::OnMouseUp(const UPoint& position, UINT flags)
{
	ReleaseCapture();
	if (m_LastContent)
	{
		m_LastContent->bKeyDown = false;
	}
}
void UQueryList::ClearColum()
{
	SetSelect(NULL);	
	m_Colum.clear();
	mColumHeight = 0;
	m_LastContent = NULL;
}

void UQueryList::AddColum(const char* Name, UPoint &Size, int TextAlign)
{
	ColumProperty newColum;
	newColum.Name = Name;
	newColum.Size = Size;
	newColum.TextAlign = TextAlign;
	m_Colum.push_back(newColum);

	if (mColumHeight < Size.y)
	{
		mColumHeight = Size.y;
	}
}

void UQueryList::ClearItem()
{
	m_LastContent = NULL;
	SetSelect(NULL);	
	for (unsigned int ui = 0 ; ui <m_ListItem.size() ; ui++)
	{
		for (unsigned int uj = 0 ; uj <m_ListItem[ui].vItemContent.size() ; uj++)
		{
			if(m_ListItem[ui].vItemContent[uj].UserData)
			{
				delete m_ListItem[ui].vItemContent[uj].UserData;
				m_ListItem[ui].vItemContent[uj].UserData = NULL;
			}
		}
	}
	m_ListItem.clear();
}

void UQueryList::AddItem(std::vector<ItemContent>& Item,int EntryId, int TipType)
{
	ListItem newEntry;
	newEntry.vItemContent = Item;
	newEntry.entryID = EntryId;
	newEntry.EntryClass = TipType;
	m_ListItem.push_back(newEntry);

	UPoint pos = UPoint(0, 0);
	pos.y += mColumHeight;
	std::vector<ListItem>::iterator it = m_ListItem.begin();
	while(it != m_ListItem.end())
	{
		it->bMouseHover = false;
		it->Pos = WindowToScreen(pos);
		it->Size.x = GetWindowSize().x;
		it->Size.y = mItemHeight;

		for (unsigned int ui = 0 ; ui < m_Colum.size() ; ui++)
		{
			ItemContent* pItem = &it->vItemContent[ui];
			ColumProperty* pColPro = &m_Colum[ui];

			pItem->Size = pColPro->Size;
			pItem->Size.y = mItemHeight;

			pItem->Pos = WindowToScreen(pos);
			//add
			pos.x += pColPro->Size.x;
		}
		pos.x = 0;
		pos.y += mItemHeight /*+ 2*/;
		++it;
	}
}

void UQueryList::SetItemHeight(int height)
{
	mItemHeight = height;
}

void UQueryList::DrawColumHead(const UPoint& offset)
{
	UFontPtr pFont = m_Style->m_spFont;
	UPoint pos = offset;
	for (unsigned int ui = 0 ; ui < m_Colum.size() ; ui++)
	{
		ColumProperty* pColPro = &m_Colum[ui];
		//draw
		UDrawText(sm_UiRender, pFont, pos, pColPro->Size, 
			/*m_Style->m_FontColor*/UColor(255,169,2), pColPro->Name.c_str(), 
			(EUTextAlignment)pColPro->TextAlign);
		//add
		pos.x += pColPro->Size.x;
	}
}

void UQueryList::DrawGirdItem(const UPoint& offset)
{
	UFontPtr pFont = m_Style->m_spFont;

	std::vector<ListItem>::iterator it = m_ListItem.begin();
	while(it != m_ListItem.end())
	{
		if (it->bMouseHover){
			sm_UiRender->DrawImage(mItemHoverBkg, it->Pos);
		}
		for (unsigned int ui = 0 ; ui < m_Colum.size() ; ui++)
		{
			ItemContent* pItem = &it->vItemContent[ui];
			ColumProperty* pColPro = &m_Colum[ui];
			UColor  color = pItem->color;
			if (pItem->bKeyDown){
				color = LCOLOR_RED;
			}
			//draw
			UDrawText(sm_UiRender, pFont, pItem->Pos, pItem->Size, 
				color, pItem->Text.c_str(), 
				(EUTextAlignment)pColPro->TextAlign);
		}
		++it;
	}
}


//////////////////////////////////////////////////////////////////////////////

QuestQuery::QuestQuery()
{
	memset(mAcceptKey, 0, sizeof(mAcceptKey));
	mAcceptKey[QUERYKEY_RACE] = 1;
	mAcceptKey[QUERYKEY_LEVEL] = 1;
	mAcceptKey[QUERYKEY_AERA] = 1;
}

void QuestQuery::InitiMemberVector()
{
	mvRegistKey.clear();
	mvQuestQueryList.clear();
}
bool KeySortQuest(QuestQuery::QuestQueryItem& quest1, QuestQuery::QuestQueryItem& quest2)
{
	bool bkey = false;

	ui32 mapid1,mapid2;
	float x,y,z;
	g_pkCCreatureSpawnsInfo->GetCreateInfomation(quest1.NPCID, mapid1, x, y, z);
	g_pkCCreatureSpawnsInfo->GetCreateInfomation(quest2.NPCID, mapid2, x, y, z);

	bkey = mapid1 * 10000 + quest1.Allowablelevel <= mapid2 * 10000 + quest2.Allowablelevel;
	return bkey;
}
int QuestQuery::FilterFromKey()
{
	std::vector<CQuestDB::stQuest> vList;
	if (g_pkQusetInfo->GetQuestList(vList))
	{
		for (unsigned int ui = 0 ; ui < vList.size() ; ui++)
		{
			QuestQueryItem NewEntry;
			NewEntry.AllowableRace = vList[ui].uiReqRaces;
			NewEntry.Allowablelevel = vList[ui].uiQuestLevel;
			g_pkQuestStarterInfo->GetStarterFormQuest(vList[ui].id, NewEntry.NPCID);
			NewEntry.questName = vList[ui].strTitle;
			AddMember(NewEntry);
		}
	}
	std::sort(mvQuestQueryList.begin(), mvQuestQueryList.end(), &KeySortQuest);
	return mvQuestQueryList.size();
}
void QuestQuery::InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount)
{
	pQueryList->ClearItem();
	if (Current >= mvQuestQueryList.size() )
		return;
	int MaxSize = min(PageCount, mvQuestQueryList.size() - Current);
	for(int i = 0 ; i < MaxSize ; i++)
	{
		std::vector<UQueryList::ItemContent> Content;
		QuestQueryItem* pQuestQueryItem = &mvQuestQueryList[Current + i];
		UQueryList::ItemContent newEntry;


		//任务名称
		newEntry.Text = pQuestQueryItem->questName;
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);
		//种族
		int raceMask = 1 + 2 + 4;
		if (pQuestQueryItem->AllowableRace & 1){
			newEntry.Text = _TRAN("贪狼");
			raceMask -= 1;
		}
		if (pQuestQueryItem->AllowableRace & 2){
			newEntry.Text = _TRAN("文昌");
			raceMask -= 2;
		}
		if (pQuestQueryItem->AllowableRace & 4){
			newEntry.Text = _TRAN("天相");
			raceMask -= 4;
		}
		if (!raceMask){
			newEntry.Text  = _TRAN("中立");
		}
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);
		////等级
		//char buf[256];
		//itoa(pQuestQueryItem->Allowablelevel, buf, 10);
		//newEntry.Text = buf;
		//newEntry.UserData = NULL;
		//newEntry.color = LCOLOR_WHITE;
		//Content.push_back(newEntry);
		//任务NPC名称
		if(!ItemMgr->GetCreatureName(pQuestQueryItem->NPCID, newEntry.Text ))
		{
			newEntry.Text = _TRAN("未知");
		}
		stQueryItemContent* pData = new stQueryItemContent;
		pData->type = QueryItemContentType_FindPath;
		if(g_pkCCreatureSpawnsInfo->GetCreateInfomation(pQuestQueryItem->NPCID, pData->mapid, pData->kPos.x, pData->kPos.y, pData->kPos.z))
		{
			pData->npcId = pQuestQueryItem->NPCID;
			newEntry.UserData = pData;
			newEntry.color = LCOLOR_GREEN;
		}
		else
		{
			newEntry.UserData = NULL;
			newEntry.color = LCOLOR_WHITE;
		}

		Content.push_back(newEntry);

		pQueryList->AddItem(Content);
	}
}
void QuestQuery::AddMember(QuestQueryItem& QueryItem)
{
	bool bSuccess = true;
	int raceMask[] = {1, 2, 4, 8};
	for (unsigned int ui = 0 ; ui < mvRegistKey.size() ; ui++)
	{
		switch(mvRegistKey[ui].key)
		{
		case QUERYKEY_RACE:
			{
				if (QueryItem.AllowableRace & 8)
					bSuccess &= (raceMask[mvRegistKey[ui].Value] == 8);
				else
					bSuccess &= (QueryItem.AllowableRace & raceMask[mvRegistKey[ui].Value])?true:false;
			}
			break;
		case QUERYKEY_LEVEL:
			{
				if (mvRegistKey[ui].Value)
					bSuccess &= (QueryItem.Allowablelevel == mvRegistKey[ui].Value);
			}
			break;
		//case QUERYKEY_AERA:
		//	{
		//		if (mvRegistKey[ui].Value)
		//		{
		//			ui32 mapid, areaId;
		//			g_pkCCreatureSpawnsInfo->GetCreateInfomation(QueryItem.NPCID, mapid, areaId);
		//			bSuccess &= (areaId == mvRegistKey[ui].Value);
		//		}
		//	}
		//	break;
		}
	}
	if (bSuccess)
		mvQuestQueryList.push_back(QueryItem);
}
//////////////////////////////////////////////////////////////////////////
NPCQuery::NPCQuery()
{
	memset(mAcceptKey, 0, sizeof(mAcceptKey));
	mAcceptKey[QUERYKEY_RACE] = 1;
	mAcceptKey[QUERYKEY_MAP] = 1;
}

void NPCQuery::InitiMemberVector()
{
	mvRegistKey.clear();
	mvNPCQueryList.clear();
}

int NPCQuery::FilterFromKey()
{
	std::vector<CCreatureSpawnsFromClientDB::stCreateSpawnsFromClientInfo> vList;
	g_pkCCreatureSpawnsInfo->GetCreateSpawnsList(vList);
	for(unsigned int ui = 0 ; ui < vList.size() ; ui++)
	{
		if (ItemMgr->IsCreatureNPC(vList[ui].Entry))
		{
			NPCQueryItem newEntry;
			newEntry.AreaId = vList[ui].AreaId;
			newEntry.Entry = vList[ui].Entry;
			newEntry.MapId = vList[ui].MapId;
			newEntry.Race = vList[ui].race;
			newEntry.kPos = vList[ui].kPos;
			AddMember(newEntry);
		}
	}
	return mvNPCQueryList.size();
}
void NPCQuery::InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount)
{	
	pQueryList->ClearItem();
	if (Current >= mvNPCQueryList.size() )
		return;
	int MaxSize = min(PageCount, mvNPCQueryList.size() - Current);
	for(int i = 0 ; i < MaxSize ; i++)
	{
		std::vector<UQueryList::ItemContent> Content;
		NPCQueryItem* pNPCQueryItem = &mvNPCQueryList[Current + i];
		UQueryList::ItemContent newEntry;

		//种族
		//char* Race[] = {"武修","羽箭","仙道","真巫"};
		char* Race[] = {"","贪狼","天相","文昌","中立"};
		newEntry.Text = _TRAN(Race[pNPCQueryItem->Race]);
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);

		//区域
		NiFixedString temp;
		if(g_pkAreaDB->GetAreaName(pNPCQueryItem->AreaId,temp))
			newEntry.Text = temp;
		else
			newEntry.Text = _TRAN("未知");
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);

		//NPC名称
		if(!ItemMgr->GetCreatureName(pNPCQueryItem->Entry, newEntry.Text ))
		{
			newEntry.Text = _TRAN("未知");
		}
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);

		//所在地图
		CMapInfoDB::MapInfoEntry entry;
		if (g_pkMapInfoDB)
			g_pkMapInfoDB->GetMapInfo(pNPCQueryItem->MapId, entry);
		newEntry.Text = entry.desc;

		stQueryItemContent* pData = new stQueryItemContent;
		pData->type = QueryItemContentType_BigMap;
		pData->mapid = pNPCQueryItem->MapId;
		pData->npcId = pNPCQueryItem->Entry;
		newEntry.UserData = pData;

		newEntry.color = LCOLOR_GREEN;
		Content.push_back(newEntry);

		pQueryList->AddItem(Content);
	}

}
void NPCQuery::AddMember(NPCQueryItem& QueryItem)
{
	bool bSuccess = true;
	for (unsigned int ui = 0 ; ui < mvRegistKey.size() ; ui++)
	{
		switch(mvRegistKey[ui].key)
		{
		case QUERYKEY_RACE:
			{
				bSuccess &= ((QueryItem.Race - 1) == mvRegistKey[ui].Value);
			}
			break;
		case QUERYKEY_MAP:
			{
				if (mvRegistKey[ui].Value)
					bSuccess &= (QueryItem.MapId == mvRegistKey[ui].Value);
			}
			break;
		}
	}
	if (bSuccess)
		mvNPCQueryList.push_back(QueryItem);
}
//////////////////////////////////////////////////////////////////////////
MonsterQuery::MonsterQuery()
{
	memset(mAcceptKey, 0, sizeof(mAcceptKey));
	mAcceptKey[QUERYKEY_LEVEL] = 1;
	mAcceptKey[QUERYKEY_MAP] = 1;
}

void MonsterQuery::InitiMemberVector()
{
	mvRegistKey.clear();
	mvMonsterQueryList.clear();
}

int MonsterQuery::FilterFromKey()
{
	std::vector<CCreatureSpawnsFromClientDB::stCreateSpawnsFromClientInfo> vList;
	g_pkCCreatureSpawnsInfo->GetCreateSpawnsList(vList);
	for(unsigned int ui = 0 ; ui < vList.size() ; ui++)
	{
		if (!ItemMgr->IsCreatureNPC(vList[ui].Entry))
		{
			MonsterQueryItem newEntry;
			newEntry.AreaId = vList[ui].AreaId;
			newEntry.Entry = vList[ui].Entry;
			newEntry.MapId = vList[ui].MapId;
			newEntry.kPos = vList[ui].kPos;
			newEntry.Level = vList[ui].uiLevel;
			AddMember(newEntry);
		}
	}
	return mvMonsterQueryList.size();
}

void MonsterQuery::InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount)
{
	pQueryList->ClearItem();
	if (Current >= mvMonsterQueryList.size() )
		return;
	int MaxSize = min(PageCount, mvMonsterQueryList.size() - Current);
	for(int i = 0 ; i < MaxSize ; i++)
	{
		std::vector<UQueryList::ItemContent> Content;
		MonsterQueryItem* pMonsterQueryItem = &mvMonsterQueryList[Current + i];
		UQueryList::ItemContent newEntry;

		//怪物名称
		if(!ItemMgr->GetCreatureName(pMonsterQueryItem->Entry, newEntry.Text ))
		{
			newEntry.Text = _TRAN("未知");
		}
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);
		//区域
		NiFixedString temp;
		if(g_pkAreaDB->GetAreaName(pMonsterQueryItem->AreaId, temp))
			newEntry.Text = temp;
		else
			newEntry.Text = _TRAN("未知");
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);
		//怪物等级
		char buf[256];
		itoa(pMonsterQueryItem->Level, buf, 10);
		newEntry.Text = buf;
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);
		//所在地图
		CMapInfoDB::MapInfoEntry entry;
		if (g_pkMapInfoDB)
			g_pkMapInfoDB->GetMapInfo(pMonsterQueryItem->MapId, entry);
		newEntry.Text = entry.desc;

		stQueryItemContent* pData = new stQueryItemContent;
		pData->type = QueryItemContentType_BigMap;
		pData->mapid = pMonsterQueryItem->MapId;
		pData->npcId = pMonsterQueryItem->Entry;
		newEntry.UserData = pData;

		newEntry.color = LCOLOR_GREEN;
		Content.push_back(newEntry);

		pQueryList->AddItem(Content);
	}
}

void MonsterQuery::AddMember(MonsterQueryItem& QueryItem)
{
	bool bSuccess = true;
	for (unsigned int ui = 0 ; ui < mvRegistKey.size() ; ui++)
	{
		switch(mvRegistKey[ui].key)
		{
		case QUERYKEY_LEVEL:
			{
				if(mvRegistKey[ui].Value)
					bSuccess &= (QueryItem.Level == mvRegistKey[ui].Value);
			}
			break;
		case QUERYKEY_MAP:
			{
				if (mvRegistKey[ui].Value)
					bSuccess &= (QueryItem.MapId == mvRegistKey[ui].Value);
			}
			break;
		}
	}
	if(bSuccess)
		mvMonsterQueryList.push_back(QueryItem);
}
//////////////////////////////////////////////////////////////////////////
EquipQuery::EquipQuery()
{
	memset(mAcceptKey, 0, sizeof(mAcceptKey));
	mAcceptKey[QUERYKEY_LEVEL] = 1;
	mAcceptKey[QUERYKEY_CLASS] = 1;
	mAcceptKey[QUERYKEY_EQUIPCLASS] = 1;
	mAcceptKey[QUERYKEY_QUILITY] = 1;
}

void EquipQuery::InitiMemberVector()
{
	mvRegistKey.clear();
	mvEquipQueryList.clear();
}
bool KeyLevel(const EquipQuery::EquipQueryItem& LI, const EquipQuery::EquipQueryItem& RI)
{
	return LI.level < RI.level;
}
int EquipQuery::FilterFromKey()
{
	std::vector<ItemPrototype_Client> vInfo;
	ItemMgr->GetItemListFormDataBase(ITEM_CLASS_WEAPON, vInfo);
	ItemMgr->GetItemListFormDataBase(ITEM_CLASS_ARMOR, vInfo);
	for (unsigned int ui = 0 ; ui < vInfo.size() ; ui++)
	{
		EquipQueryItem NewItem;
		ItemPrototype_Client* pItemInfo = &vInfo[ui];
		NewItem.entry = pItemInfo->ItemId;
		NewItem.AllowableClass = pItemInfo->AllowableClass;
		NewItem.EquipClass = pItemInfo->Class;
		NewItem.EquipSubClass = pItemInfo->SubClass;
		NewItem.level = pItemInfo->RequiredLevel;
		NewItem.Quility = pItemInfo->Quality;
		NewItem.Name = pItemInfo->C_name;
		AddMember(NewItem);
	}
	std::sort(mvEquipQueryList.begin(), mvEquipQueryList.end(), &KeyLevel);
	return mvEquipQueryList.size();
}

//暂时排除80级以上装备
#define MAX_GAME_LEVEL 80

void EquipQuery::AddMember(EquipQueryItem& QueryItem)
{
	bool bSuccess = true;
	static int classMask[] = {1, 2, 4, 8};

	if(QueryItem.level > MAX_GAME_LEVEL)
		return;

	for (unsigned int ui = 0 ; ui < mvRegistKey.size() ; ui++)
	{
		switch(mvRegistKey[ui].key)
		{
		case QUERYKEY_LEVEL:
			if(mvRegistKey[ui].Value)
				bSuccess &= (QueryItem.level == mvRegistKey[ui].Value);
			break;
		case QUERYKEY_CLASS:
			bSuccess &= (QueryItem.AllowableClass & classMask[mvRegistKey[ui].Value])?true:false;
			break;
		case QUERYKEY_EQUIPCLASS:
			{
				int TranValue = mvRegistKey[ui].Value;
				bool IsWeapon = true;
				if (mvRegistKey[ui].Value >= ITEM_SUBCLASS_WEAPON_MAX)
				{
					TranValue = mvRegistKey[ui].Value - ITEM_SUBCLASS_WEAPON_MAX;
					IsWeapon = false;
				}
				bSuccess &= (QueryItem.EquipClass == (IsWeapon?ITEM_CLASS_WEAPON:ITEM_CLASS_ARMOR) && 
					QueryItem.EquipSubClass == TranValue);
			}
			break;
		case QUERYKEY_QUILITY:
			bSuccess &= (QueryItem.Quility == mvRegistKey[ui].Value);
			break;
		}
	}
	if (bSuccess)
		mvEquipQueryList.push_back(QueryItem);
}
#undef MAX_GAME_LEVEL

void EquipQuery::InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount)
{
	pQueryList->ClearItem();
	if (Current >= mvEquipQueryList.size() )
		return;
	int MaxSize = min(PageCount, mvEquipQueryList.size() - Current);
	for(int i = 0 ; i < MaxSize ; i++)
	{
		std::vector<UQueryList::ItemContent> Content;
		EquipQueryItem* pEquipQueryItem = &mvEquipQueryList[Current + i];
		//名称
		UQueryList::ItemContent newEntry;
		newEntry.Text = pEquipQueryItem->Name;
		stQueryItemContent* pData = new stQueryItemContent;
		pData->type = QueryItemContentType_FittingEquip;
		pData->Itementry = pEquipQueryItem->entry;
		newEntry.UserData = pData;
		GetQualityColor(pEquipQueryItem->Quility ,newEntry.color);
		Content.push_back(newEntry);
		//等级
		char buf[256];
		itoa(pEquipQueryItem->level, buf, 10);
		newEntry.Text = buf;
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);
		//职业
		std::string classStr;
		int ClassMask = 1 + 2 + 4 + 8;
		if(pEquipQueryItem->AllowableClass & CLASS_WARRIOR)
		{
			classStr += _TRAN("武修 ");
			ClassMask -= 1;
		}
		if(pEquipQueryItem->AllowableClass & 2)
		{
			classStr += _TRAN("羽箭 ");
			ClassMask -= 2;
		}
		if(pEquipQueryItem->AllowableClass & 4)
		{
			classStr += _TRAN("仙道 ");
			ClassMask -= 4;
		}
		if(pEquipQueryItem->AllowableClass & 8)
		{
			classStr += _TRAN("真巫 ");
			ClassMask -= 8;
		}
		if (!ClassMask)
		{
			classStr = _TRAN("所有职业");
		}
		newEntry.Text = classStr;
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);
		//类型
		if(pEquipQueryItem->EquipClass == ITEM_CLASS_WEAPON)
			newEntry.Text = _TRAN(WeaponType[pEquipQueryItem->EquipSubClass]);
		else if( pEquipQueryItem->EquipClass  == ITEM_CLASS_ARMOR)
			newEntry.Text = _TRAN(ArmorType[pEquipQueryItem->EquipSubClass]);
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);
		//品质
		newEntry.Text = _TRAN(QuilityClass[pEquipQueryItem->Quility]);
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);

		pQueryList->AddItem(Content, pEquipQueryItem->entry, TIP_TYPE_ITEM);
	}
}
MapQuery::MapQuery()
{
	memset(mAcceptKey, 0, sizeof(mAcceptKey));
	mAcceptKey[QUERYKEY_RACE] = 1;
	mAcceptKey[QUERYKEY_LEVEL] = 1;
}

void MapQuery::InitiMemberVector()
{
	mvRegistKey.clear();
	mvMapQueryList.clear();
}

int MapQuery::FilterFromKey()
{
	std::vector<AreaInfo*> vInfo;
	g_pkAreaDB->GetAreaInfoList(vInfo);
	for(unsigned int ui = 0 ; ui < vInfo.size() ; ui++)
	{
		MapQueryItem NewEntry;
		NewEntry.AllowableRace = vInfo[ui]->AllowableRace;
		NewEntry.AreaName = vInfo[ui]->kName;
		NewEntry.Max_Level = vInfo[ui]->maxLevel;
		NewEntry.min_Level = vInfo[ui]->minLevel;
		NewEntry.MapId = vInfo[ui]->mapId;
		AddMember(NewEntry);
	}
	return mvMapQueryList.size();
}
void MapQuery::InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount)
{
	pQueryList->ClearItem();
	if (Current >= mvMapQueryList.size() )
		return;
	int MaxSize = min(PageCount, mvMapQueryList.size() - Current);
	for(int i = 0 ; i < MaxSize ; i++)
	{
		std::vector<UQueryList::ItemContent> Content;
		MapQueryItem* pMapQueryItem = &mvMapQueryList[Current + i];
		UQueryList::ItemContent newEntry;
		//种族
		int raceMask = 1 + 2 + 4;
		if (pMapQueryItem->AllowableRace & 1){
			newEntry.Text = _TRAN("贪狼");
			raceMask -= 1;
		}
		if (pMapQueryItem->AllowableRace & 2){
			newEntry.Text = _TRAN("文昌");
			raceMask -= 2;
		}
		if (pMapQueryItem->AllowableRace & 4){
			newEntry.Text = _TRAN("天相");
			raceMask -= 4;
		}
		if (!raceMask){
			newEntry.Text  = _TRAN("中立");
		}
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);
		//区域
		newEntry.Text = pMapQueryItem->AreaName;
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);
		//等级
		char buf[256];
		sprintf(buf, "%d - %d", pMapQueryItem->min_Level, pMapQueryItem->Max_Level);
		newEntry.Text = buf;
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);
		//地图
		CMapInfoDB::MapInfoEntry entry;
		if (g_pkMapInfoDB)
			g_pkMapInfoDB->GetMapInfo(pMapQueryItem->MapId, entry);
		newEntry.Text = entry.desc;
		stQueryItemContent* pData = new stQueryItemContent;
		pData->type = QueryItemContentType_BigMap;
		pData->mapid = pMapQueryItem->MapId;
		newEntry.UserData = pData;
		newEntry.color = LCOLOR_GREEN;
		Content.push_back(newEntry);

		pQueryList->AddItem(Content);
	}
}
void MapQuery::AddMember(MapQueryItem& MapItem)
{
	bool bSuccess = true;
	int raceMask[] = {1, 2, 4, 8};
	for (unsigned int ui = 0 ; ui < mvRegistKey.size() ; ui++)
	{
		switch(mvRegistKey[ui].key)
		{
		case QUERYKEY_RACE:
			{
				if (MapItem.AllowableRace & 8)
					bSuccess &= (raceMask[mvRegistKey[ui].Value] == 8);
				else
					bSuccess &= (MapItem.AllowableRace & raceMask[mvRegistKey[ui].Value])?true:false;
			}
			break;
		case QUERYKEY_LEVEL:
			{
				if (mvRegistKey[ui].Value)
					bSuccess &= (MapItem.min_Level <= mvRegistKey[ui].Value && MapItem.Max_Level >= mvRegistKey[ui].Value);
				//后期要去掉。100 - 160级没有开放 暂时不显示
				if (MapItem.min_Level >= 100)	{
					bSuccess &= false;
				}
			}
			break;
		}
	}
	if (bSuccess)
		mvMapQueryList.push_back(MapItem);
}

SpellQuery::SpellQuery()
{
	memset(mAcceptKey, 0, sizeof(mAcceptKey));
	mAcceptKey[QUERYKEY_CLASS] = 1;
	mAcceptKey[QUERYKEY_LEVEL] = 1;
}

void SpellQuery::InitiMemberVector()
{
	mvRegistKey.clear();
	mvSpellQueryList.clear();
}

int SpellQuery::FilterFromKey()
{
	std::vector<stQuerySpellInfo> vInfo;
	g_pkQuerySpellInfo->GetQuerySpellList(vInfo);
	for (unsigned int ui = 0 ; ui < vInfo.size() ; ui++)
	{
		SpellQueryItem NewEntry;
		stSpellInfo* pSpellInfo = g_pkSpellInfo->GetSpellInfo(vInfo[ui].SpellId);
		if (pSpellInfo)
		{
			NewEntry.SpellId = vInfo[ui].SpellId;
			NewEntry.SpellName = pSpellInfo->name;
			NewEntry.AllowableLevel = vInfo[ui].AllowableLevel;
			NewEntry.AllowableClass = vInfo[ui].AllowableClass;
			AddMember(NewEntry);
		}
	}
	return mvSpellQueryList.size();
}
void SpellQuery::InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount)
{
	pQueryList->ClearItem();
	if (Current >= mvSpellQueryList.size() )
		return;
	int MaxSize = min(PageCount, mvSpellQueryList.size() - Current);
	for(int i = 0 ; i < MaxSize ; i++)
	{
		std::vector<UQueryList::ItemContent> Content;
		SpellQueryItem* pSpellQueryItem = &mvSpellQueryList[Current + i];
		UQueryList::ItemContent newEntry;

		//技能名称
		newEntry.Text = pSpellQueryItem->SpellName;
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);

		//等级
		char buf[256];
		itoa(pSpellQueryItem->AllowableLevel, buf, 10);
		newEntry.Text = buf;
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);

		//职业
		int ClassMask = 1 + 2 + 4 + 8;
		newEntry.Text.clear();
		if(pSpellQueryItem->AllowableClass & CLASS_WARRIOR){
			newEntry.Text  += _TRAN("武修 ");
			ClassMask -= 1;
		}
		if(pSpellQueryItem->AllowableClass & 2){
			newEntry.Text  += _TRAN("羽箭 ");
			ClassMask -= 2;
		}
		if(pSpellQueryItem->AllowableClass & 4){
			newEntry.Text  += _TRAN("仙道 ");
			ClassMask -= 4;
		}
		if(pSpellQueryItem->AllowableClass & 8){
			newEntry.Text  += _TRAN("真巫 ");
			ClassMask -= 8;
		}
		if (!ClassMask){
			newEntry.Text  = _TRAN("所有职业");
		}
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);

		pQueryList->AddItem(Content, pSpellQueryItem->SpellId, TIP_TYPE_SPELL);
	}
}

void SpellQuery::AddMember(SpellQueryItem& SpellItem)
{
	bool bSuccess = true;
	int ClassMask[] = {1, 2, 4, 8};
	for (unsigned int ui = 0 ; ui < mvRegistKey.size() ; ui++)
	{
		switch(mvRegistKey[ui].key)
		{
		case QUERYKEY_CLASS:
			{
				bSuccess &= (SpellItem.AllowableClass & ClassMask[mvRegistKey[ui].Value])?true:false;
			}
			break;
		case QUERYKEY_LEVEL:
			{
				if (mvRegistKey[ui].Value)
					bSuccess &= (SpellItem.AllowableLevel == mvRegistKey[ui].Value);
			}
			break;
		}
	}
	if (bSuccess)
		mvSpellQueryList.push_back(SpellItem);
}
//////////////////////////////////////////////////////////////////////////
TitleQuery::TitleQuery()
{
	memset(mAcceptKey, 0, sizeof(mAcceptKey));
	mAcceptKey[QUERYKEY_TITLECLASS] = 1;
}
void TitleQuery::InitiMemberVector()
{
	mvRegistKey.clear();
	mvTitleQueryList.clear();
}
int TitleQuery::FilterFromKey()
{
	std::vector<TitleInfo> vInfo;
	g_pkPlayerTitle->GetTitleInfoList(vInfo);
	for (unsigned int ui = 0 ; ui < vInfo.size() ; ui++)
	{
		TitleQueryItem NewEntry;
		NewEntry.TitleClass = vInfo[ui].Class;
		NewEntry.TitleId = vInfo[ui].id;
		NewEntry.TitleName = vInfo[ui].stTitle;
		AddMember(NewEntry);
	}
	return mvTitleQueryList.size();
}
void TitleQuery::InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount)
{
	pQueryList->ClearItem();
	if (Current >= mvTitleQueryList.size() )
		return;
	int MaxSize = min(PageCount, mvTitleQueryList.size() - Current);
	for(int i = 0 ; i < MaxSize ; i++)
	{
		std::vector<UQueryList::ItemContent> Content;
		TitleQueryItem* pTitleQueryItem = &mvTitleQueryList[Current + i];
		UQueryList::ItemContent newEntry;

		//称号类型
		newEntry.Text = _TRAN(TitleClass[pTitleQueryItem->TitleClass]);
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);

		//称号名称
		newEntry.Text = pTitleQueryItem->TitleName;
		newEntry.UserData = NULL;
		newEntry.color = LCOLOR_WHITE;
		Content.push_back(newEntry);


		pQueryList->AddItem(Content, pTitleQueryItem->TitleId, TIP_TYPE_TITLE);
	}
}
void TitleQuery::AddMember(TitleQueryItem& TitleItem)
{
	bool bSuccess = true;
	for (unsigned int ui = 0 ; ui < mvRegistKey.size() ; ui++)
	{
		switch(mvRegistKey[ui].key)
		{
		case QUERYKEY_TITLECLASS:
			{
				bSuccess &= (TitleItem.TitleClass == mvRegistKey[ui].Value);
			}
			break;
		}
	}
	if (bSuccess)
		mvTitleQueryList.push_back(TitleItem);
}
///////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UQueryFrame, UControl)
UBEGIN_MESSAGE_MAP(UQueryFrame, UControl)
UON_BN_CLICKED(0, &UQueryFrame::OnChangeQueryMethod)
UON_BN_CLICKED(1, &UQueryFrame::OnChangeQueryMethod)
UON_BN_CLICKED(2, &UQueryFrame::OnChangeQueryMethod)
UON_BN_CLICKED(3, &UQueryFrame::OnChangeQueryMethod)
UON_BN_CLICKED(4, &UQueryFrame::OnChangeQueryMethod)
UON_BN_CLICKED(5, &UQueryFrame::OnChangeQueryMethod)
UON_BN_CLICKED(6, &UQueryFrame::OnChangeQueryMethod)
UON_BN_CLICKED(10, &UQueryFrame::OnBeginQuery)
UON_BN_CLICKED(11, &UQueryFrame::OnPrevPage)
UON_BN_CLICKED(12, &UQueryFrame::OnNextPage)
UON_BN_CLICKED(255, &UQueryFrame::OnClose)
UON_CBOX_SELCHANGE(35, &UQueryFrame::OnComboBox1)
UON_CBOX_SELCHANGE(36, &UQueryFrame::OnComboBox2)
UON_CBOX_SELCHANGE(37, &UQueryFrame::OnComboBox3)
UON_CBOX_SELCHANGE(38, &UQueryFrame::OnComboBox4)
UEND_MESSAGE_MAP()

void UQueryFrame::StaticInit()
{

}

UQueryFrame::UQueryFrame()
{
	for (int i = 0 ; i < 10 ; i++)
	{
		m_QueryKey[i].key = QUERYKEY_MAX;
		m_QueryKey[i].value = -1;//错误值
	}
	m_IsUpdateChild = FALSE;
}
UQueryFrame::~UQueryFrame()
{
}
BOOL UQueryFrame::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (m_Popo == NULL)
	{
		m_Popo = sm_System->LoadSkin("SystemTips\\bg_1.skin");
		if (!m_Popo)
		{
			return FALSE;
		}
	}
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(0));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	pBtn = UDynamicCast(UButton, GetChildByID(3));
	if (pBtn)
	{
		pBtn->SetActive(FALSE);
	}
	UStaticText* pStaticText = UDynamicCast(UStaticText, GetChildByID(18));
	pStaticText->SetTextColor(UColor(255,169,2));
	for (int i = 0 ; i < 4; i++)
	{
		UEdit* pedit = (UEdit*)GetChildByID(40 + i );
		if (pedit)
		{
			pedit->SetNumberOnly(TRUE);
		}
		pStaticText = UDynamicCast(UStaticText, GetChildByID(30 + i));
		pStaticText->SetTextColor(UColor(255,169,2));
	}

	return TRUE;
}
void UQueryFrame::OnDestroy()
{
	for (int i = 0 ; i < 10 ; i++)
	{
		m_QueryKey[i].key = QUERYKEY_MAX;
		m_QueryKey[i].value = -1;//错误值
	}
	UControl::OnDestroy();
}
void UQueryFrame::OnRender(const UPoint& offset,const URect &updateRect)
{
	//draw Background
	if (m_Popo)
	{
		//UDrawResizeImage(RESIZE_WANDH, sm_UiRender, m_Popo, offset, updateRect.GetSize());
	}

	//draw child
	RenderChildWindow(offset, updateRect);
}
BOOL UQueryFrame::OnEscape()
{
	if (!UControl::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UQueryFrame::OnChangeQueryMethod()
{
	for (int i = 0 ; i < QUERY_METHOD_MAX ; i ++)
	{
		UButton* pBtn = UDynamicCast(UButton, GetChildByID(i));
		if (pBtn->IsChecked())
		{
			QueryMgr->SetQueryMethod(i);
			return;
		}
	}
}
void UQueryFrame::OnBeginQuery()
{
	QueryMgr->BeginQuery();
	for(int i = 0 ; i < 5 ; i++)
	{
		if (m_QueryKey[i].key == QUERYKEY_LEVEL)
		{
			UEdit* pEdit = (UEdit*)GetChildByID(40 + i );
			if (pEdit)
			{
				char buf[256];
				int iRs =pEdit->GetText(buf, 256);
				buf[iRs] = 0;
				m_QueryKey[i].value = atoi(buf);
			}
		}
		if (m_QueryKey[i].value != -1)
		{
			QueryMgr->Culling(m_QueryKey[i].key, m_QueryKey[i].value);
		}
	}
	QueryMgr->EndQuery();
}
void UQueryFrame::OnPrevPage()
{
	QueryMgr->PrevPage();
}
void UQueryFrame::OnNextPage()
{
	QueryMgr->NextPage();
}
void UQueryFrame::OnClose()
{
	UButton* pBsHOW = INGAMEGETFRAME(UButton, FRAME_IG_BSHOWLEVELTIPS);
	if(pBsHOW)
	{
		pBsHOW->SetCheck(TRUE);
	}
	//SetVisible(FALSE);
}
void TransferredValue(QueryKey key, int SrcValue, void* pData, int& DstValue)
{
	int TValue = SrcValue;
	if (pData)
	{
		TValue = *((unsigned int*)(pData));
	}
	switch(key)
	{
	case QUERYKEY_RACE:
		DstValue = TValue;
		break;
	case QUERYKEY_MAP:
		DstValue = TValue;
		break;
	case QUERYKEY_AERA:
		DstValue = TValue;
		break;
	case QUERYKEY_LEVEL:
		DstValue = TValue;
		break;
	case QUERYKEY_CLASS:
		DstValue = TValue;
		break;
	case QUERYKEY_EQUIPCLASS:
		DstValue = TValue;
		break;
	case QUERYKEY_QUILITY:
		DstValue = TValue;
		break;
	case QUERYKEY_TITLECLASS:
		DstValue = TValue;
		break;
	}
}
void UQueryFrame::OnComboBox1()
{
	UComboBox* pComboBox = UDynamicCast(UComboBox, GetChildByID(35));
	TransferredValue(m_QueryKey[0].key, pComboBox->GetCurSel(),pComboBox->GetItemData(pComboBox->GetCurSel()), m_QueryKey[0].value);
	//m_QueryKey[0].value = pComboBox->GetCurSel();
}
void UQueryFrame::OnComboBox2()
{
	UComboBox* pComboBox = UDynamicCast(UComboBox, GetChildByID(36));
	TransferredValue(m_QueryKey[1].key, pComboBox->GetCurSel(),pComboBox->GetItemData(pComboBox->GetCurSel()),  m_QueryKey[1].value);
	//m_QueryKey[1].value = pComboBox->GetCurSel();
}
void UQueryFrame::OnComboBox3()
{
	UComboBox* pComboBox = UDynamicCast(UComboBox, GetChildByID(37));
	TransferredValue(m_QueryKey[2].key, pComboBox->GetCurSel(),pComboBox->GetItemData(pComboBox->GetCurSel()),  m_QueryKey[2].value);
	//m_QueryKey[2].value = pComboBox->GetCurSel();
}
void UQueryFrame::OnComboBox4()
{
	UComboBox* pComboBox = UDynamicCast(UComboBox, GetChildByID(38));
	TransferredValue(m_QueryKey[3].key, pComboBox->GetCurSel(),pComboBox->GetItemData(pComboBox->GetCurSel()),  m_QueryKey[3].value);
	//m_QueryKey[3].value = pComboBox->GetCurSel();
}
void UQueryFrame::SetFrameItemShow(int index, bool IsShow, QueryKey key)
{
	if (IsShow)
	{
		GetChildByID(30 + index)->SetVisible(IsShow);
		bool bEdit = QueryMgr->InitiUserInterface((UStaticText*)GetChildByID(30 + index),
			(UComboBox*)GetChildByID(35 + index),
			key);
		UEdit* pedit = (UEdit*)GetChildByID(40 + index );
		if (pedit)
		{
			pedit->SetVisible(bEdit);
			pedit->Clear();
		}
		GetChildByID(35 + index)->SetVisible(!bEdit);
		m_QueryKey[index].key = key;
		m_QueryKey[index].value = -1;

		GetChildByID(135 + index)->SetVisible(true);
	}
	else
	{
		m_QueryKey[index].key = QUERYKEY_MAX;
		m_QueryKey[index].value = -1;
		GetChildByID(30 + index)->SetVisible(false);
		GetChildByID(35 + index)->SetVisible(false);
		GetChildByID(40 + index)->SetVisible(false);
		GetChildByID(135 + index)->SetVisible(false);
	}
}

void UQueryFrame::SetPageText(char* buf)
{
	UStaticText* pStaticText = UDynamicCast(UStaticText, GetChildByID(60));
	if (pStaticText)
		pStaticText->SetText(buf);
}

void UQueryFrame::AddEquipCol()
{
	UQueryList* pQueryList = UDynamicCast(UQueryList, GetChildByID(50));
	if (pQueryList)
	{
		pQueryList->ClearColum();
		pQueryList->AddColum(_TRAN("名称"), UPoint(120, 20));
		pQueryList->AddColum(_TRAN("等级"), UPoint(60, 20));
		pQueryList->AddColum(_TRAN("职业"), UPoint(60, 20));
		pQueryList->AddColum(_TRAN("类型"), UPoint(60, 20));
		pQueryList->AddColum(_TRAN("品质"), UPoint(60, 20));
	}
}
void UQueryFrame::AddQuestCol()
{
	UQueryList* pQueryList = UDynamicCast(UQueryList, GetChildByID(50));
	if (pQueryList)
	{
		pQueryList->ClearColum();
		pQueryList->AddColum(_TRAN("任务名称"), UPoint(160, 20));
		pQueryList->AddColum(_TRAN("种族"), UPoint(100, 20));
		pQueryList->AddColum(_TRAN("接任务NPC"), UPoint(100, 20));
	}

}
void UQueryFrame::AddNpcCol()
{
	UQueryList* pQueryList = UDynamicCast(UQueryList, GetChildByID(50));
	if (pQueryList)
	{
		pQueryList->ClearColum();
		pQueryList->AddColum(_TRAN("种族"), UPoint(90, 20));
		pQueryList->AddColum(_TRAN("区域"), UPoint(90, 20));
		pQueryList->AddColum(_TRAN("NPC名"), UPoint(90, 20));
		pQueryList->AddColum(_TRAN("所在区域"), UPoint(90, 20));
	}

}
void UQueryFrame::AddMonsterCol()
{
	UQueryList* pQueryList = UDynamicCast(UQueryList, GetChildByID(50));
	if (pQueryList)
	{
		pQueryList->ClearColum();
		pQueryList->AddColum(_TRAN("怪物名"), UPoint(120, 20));
		pQueryList->AddColum(_TRAN("区域"), UPoint(80, 20));
		pQueryList->AddColum(_TRAN("等级"), UPoint(80, 20));
		pQueryList->AddColum(_TRAN("所在区域"), UPoint(80, 20));
	}

}
void UQueryFrame::AddMapCol()
{
	UQueryList* pQueryList = UDynamicCast(UQueryList, GetChildByID(50));
	if (pQueryList)
	{
		pQueryList->ClearColum();
		pQueryList->AddColum(_TRAN("种族"), UPoint(90, 20));
		pQueryList->AddColum(_TRAN("区域"), UPoint(90, 20));
		pQueryList->AddColum(_TRAN("等级"), UPoint(90, 20));
		pQueryList->AddColum(_TRAN("所在地图"), UPoint(90, 20));
	}

}
void UQueryFrame::AddSpellCol()
{
	UQueryList* pQueryList = UDynamicCast(UQueryList, GetChildByID(50));
	if (pQueryList)
	{
		pQueryList->ClearColum();
		pQueryList->AddColum(_TRAN("技能名称"), UPoint(140, 20));
		pQueryList->AddColum(_TRAN("等级"), UPoint(80, 20));
		pQueryList->AddColum(_TRAN("职业"), UPoint(140, 20));
	}
}
void UQueryFrame::AddTitleCol()
{
	UQueryList* pQueryList = UDynamicCast(UQueryList, GetChildByID(50));
	if (pQueryList)
	{
		pQueryList->ClearColum();
		pQueryList->AddColum(_TRAN("称号类型"), UPoint(80, 20));
		pQueryList->AddColum(_TRAN("称号名称"), UPoint(280, 20));
	}
}

UQueryList* UQueryFrame::GetQueryList()
{
	UQueryList* pQueryList = UDynamicCast(UQueryList, GetChildByID(50));
	return pQueryList;
}
void UQueryFrame::SetVisible(BOOL value)
{
	if (value)
	{
		//UButton* pBtn = UDynamicCast(UButton, GetChildByID(0));
		//if (pBtn)
		//{
		//	pBtn->SetCheck(FALSE);
		//}
	}
	UControl::SetVisible(value);
}
//////////////////////////////////////////////////////////////////////////

QueryManager::QueryManager()
{
	m_QueryFrame = NULL;
	memset(m_pQueryMethod, 0, sizeof(m_pQueryMethod));

	mCurrentPage = 0;
	mMaxPage = 0;
}

QueryManager::~QueryManager()
{
	for (int i = 0 ; i < QUERY_METHOD_MAX ; i++)
	{
		delete m_pQueryMethod[i];
	}
	memset(m_pQueryMethod, 0, sizeof(m_pQueryMethod));
}

bool QueryManager::Create(UControl* root)
{
	if(m_pQueryMethod[QUERY_METHOD_QUEST] == NULL)
	{
		m_pQueryMethod[QUERY_METHOD_QUEST] = new QuestQuery;
		if (!m_pQueryMethod[QUERY_METHOD_QUEST])
		{
			return false;
		}
	}
	if(m_pQueryMethod[QUERY_METHOD_NPC] == NULL)
	{
		m_pQueryMethod[QUERY_METHOD_NPC] = new NPCQuery;
		if (!m_pQueryMethod[QUERY_METHOD_NPC])
		{
			return false;
		}
	}
	if(m_pQueryMethod[QUERY_METHOD_MONSTER] == NULL)
	{
		m_pQueryMethod[QUERY_METHOD_MONSTER] = new MonsterQuery;
		if (!m_pQueryMethod[QUERY_METHOD_MONSTER])
		{
			return false;
		}
	}
	if(m_pQueryMethod[QUERY_METHOD_EQUIP] == NULL)
	{
		m_pQueryMethod[QUERY_METHOD_EQUIP] = new EquipQuery;
		if (!m_pQueryMethod[QUERY_METHOD_EQUIP])
		{
			return false;
		}
	}
	if(m_pQueryMethod[QUERY_METHOD_MAP] == NULL)
	{
		m_pQueryMethod[QUERY_METHOD_MAP] = new MapQuery;
		if (!m_pQueryMethod[QUERY_METHOD_MAP])
		{
			return false;
		}
	}
	if(m_pQueryMethod[QUERY_METHOD_SPELL] == NULL)
	{
		m_pQueryMethod[QUERY_METHOD_SPELL] = new SpellQuery;
		if (!m_pQueryMethod[QUERY_METHOD_SPELL])
		{
			return false;
		}
	}
	if(m_pQueryMethod[QUERY_METHOD_TITLE] == NULL)
	{
		m_pQueryMethod[QUERY_METHOD_TITLE] = new TitleQuery;
		if (!m_pQueryMethod[QUERY_METHOD_TITLE])
		{
			return false;
		}
	}


	{
		mvQueryString[QUERYKEY_RACE].keyOpCode  = QUERYKEY_RACE;
		mvQueryString[QUERYKEY_RACE].KeyName =  _TRAN("种族");
		mvQueryString[QUERYKEY_RACE].KeyContent.clear();
		mvQueryString[QUERYKEY_RACE].KeyContent.push_back(_TRAN("贪狼（妖）"));
		mvQueryString[QUERYKEY_RACE].KeyContent.push_back(_TRAN("文昌（人）"));
		mvQueryString[QUERYKEY_RACE].KeyContent.push_back(_TRAN("天相（巫）"));
		mvQueryString[QUERYKEY_RACE].KeyContent.push_back(_TRAN("中立"));
	}
	{
		mvQueryString[QUERYKEY_MAP].keyOpCode  = QUERYKEY_MAP;
		mvQueryString[QUERYKEY_MAP].KeyName =  _TRAN("地图");
		mvQueryString[QUERYKEY_MAP].KeyContent.clear();
		mvQueryString[QUERYKEY_MAP].KeyContent.push_back(_TRAN("随意"));
	}
	{
		mvQueryString[QUERYKEY_AERA].keyOpCode  = QUERYKEY_AERA;
		mvQueryString[QUERYKEY_AERA].KeyName =  _TRAN("地区");
		mvQueryString[QUERYKEY_AERA].KeyContent.clear();
		mvQueryString[QUERYKEY_AERA].KeyContent.push_back(_TRAN("随意"));
	}
	{
		mvQueryString[QUERYKEY_LEVEL].keyOpCode  = QUERYKEY_LEVEL;
		mvQueryString[QUERYKEY_LEVEL].KeyName =  _TRAN("等级");
		mvQueryString[QUERYKEY_LEVEL].KeyContent.clear();
	}
	{
		mvQueryString[QUERYKEY_CLASS].keyOpCode  = QUERYKEY_CLASS;
		mvQueryString[QUERYKEY_CLASS].KeyName =  _TRAN("职业");
		mvQueryString[QUERYKEY_CLASS].KeyContent.clear();
		mvQueryString[QUERYKEY_CLASS].KeyContent.push_back(_TRAN("武修"));
		mvQueryString[QUERYKEY_CLASS].KeyContent.push_back(_TRAN("羽箭"));
		mvQueryString[QUERYKEY_CLASS].KeyContent.push_back(_TRAN("仙道"));
		mvQueryString[QUERYKEY_CLASS].KeyContent.push_back(_TRAN("真巫"));
	}
	{
		mvQueryString[QUERYKEY_EQUIPCLASS].keyOpCode  = QUERYKEY_EQUIPCLASS;
		mvQueryString[QUERYKEY_EQUIPCLASS].KeyName =  _TRAN("类型");
		mvQueryString[QUERYKEY_EQUIPCLASS].KeyContent.clear();
		int size = sizeof(WeaponType)/sizeof(WeaponType[0]);
		for (int i = 0 ; i < size ; i++)
		{
			mvQueryString[QUERYKEY_EQUIPCLASS].KeyContent.push_back(_TRAN(WeaponType[i]));
		}
		size = sizeof(ArmorType)/sizeof(ArmorType[0]);
		for (int i = 0 ; i < size ; i++)
		{
			mvQueryString[QUERYKEY_EQUIPCLASS].KeyContent.push_back(_TRAN(ArmorType[i]));
		}
	}
	{
		mvQueryString[QUERYKEY_QUILITY].keyOpCode  = QUERYKEY_QUILITY;
		mvQueryString[QUERYKEY_QUILITY].KeyName =  _TRAN("品质");
		mvQueryString[QUERYKEY_QUILITY].KeyContent.clear();
		int size = sizeof(QuilityClass)/sizeof(QuilityClass[0]);
		for (int i = 0 ; i < size ; i++)
		{
			mvQueryString[QUERYKEY_QUILITY].KeyContent.push_back(_TRAN(QuilityClass[i]));
		}
	}
	{
		mvQueryString[QUERYKEY_TITLECLASS].keyOpCode = QUERYKEY_TITLECLASS;
		mvQueryString[QUERYKEY_TITLECLASS].KeyName = _TRAN("类型");
		mvQueryString[QUERYKEY_TITLECLASS].KeyContent.clear();
		int size = sizeof(TitleClass)/sizeof(TitleClass[0]);
		for (int i = 0 ; i < size ; i++)
		{
			mvQueryString[QUERYKEY_TITLECLASS].KeyContent.push_back(_TRAN(TitleClass[i]));
		}
	}
	if (m_QueryFrame == NULL)
	{
		m_QueryFrame = (UQueryFrame*)UControl::sm_System->CreateDialogFromFile("QuerySystem\\UQueryFrame.udg");
		if (!m_QueryFrame)
			return false;
		m_QueryFrame->SetId(FRAME_IG_QUERYFRAME);
		root->AddChild(m_QueryFrame);
	}
	return true;
}

#include "UIMiniMap.h"
#include "ObjectManager.h"
#include "Map/PathNode.h"
#include "Map/Map.h"
#include "Utils/MapToMapPathInfo.h"
void QueryManager::FindPathMethod(ui32 mapid, float x, float y, float z)
{
	NiPoint3 kPos = NiPoint3::ZERO ;

	// 检查当前地图ID，如果相同则寻路
	if(CMap::Get()->GetMapId() == mapid)
	{
		ClientSystemNotify(_TRAN("开始寻路，若需要取消，请按移动键。"));
		CPlayerLocal* pkLcoalPlayer = ObjectMgr->GetLocalPlayer();

		kPos = NiPoint3(x, y, z);
		pkLcoalPlayer->MoveToMapAStar(mapid, kPos, 0);

	}

}

void QueryManager::SetQueryMethod(int iQueryMethod)
{
	mCurrentPage = 0;
	mMaxPage = 0;
	miCurrentQueryMethod = iQueryMethod;
	m_QueryFrame->GetQueryList()->ClearItem();

	switch(iQueryMethod)
	{
	case QUERY_METHOD_QUEST:
		m_QueryFrame->SetFrameItemShow(0, true, QUERYKEY_LEVEL);
		m_QueryFrame->SetFrameItemShow(1, true, QUERYKEY_RACE);
		m_QueryFrame->SetFrameItemShow(2, false);
		m_QueryFrame->SetFrameItemShow(3, false);
		m_QueryFrame->AddQuestCol();
		break;
	case QUERY_METHOD_NPC:
		m_QueryFrame->SetFrameItemShow(0, true, QUERYKEY_RACE);
		m_QueryFrame->SetFrameItemShow(1, true, QUERYKEY_MAP);
		m_QueryFrame->SetFrameItemShow(2, false);
		m_QueryFrame->SetFrameItemShow(3, false);
		m_QueryFrame->AddNpcCol();
		break;
	case QUERY_METHOD_MONSTER:
		m_QueryFrame->SetFrameItemShow(0, true, QUERYKEY_LEVEL);
		m_QueryFrame->SetFrameItemShow(1, true, QUERYKEY_MAP);
		m_QueryFrame->SetFrameItemShow(2, false);
		m_QueryFrame->SetFrameItemShow(3, false);
		m_QueryFrame->AddMonsterCol();
		break;
	case QUERY_METHOD_EQUIP:
		m_QueryFrame->SetFrameItemShow(0, true, QUERYKEY_LEVEL);
		m_QueryFrame->SetFrameItemShow(1, true, QUERYKEY_CLASS);
		m_QueryFrame->SetFrameItemShow(2, true, QUERYKEY_EQUIPCLASS);
		m_QueryFrame->SetFrameItemShow(3, true, QUERYKEY_QUILITY);
		m_QueryFrame->AddEquipCol();
		break;
	case QUERY_METHOD_MAP:
		m_QueryFrame->SetFrameItemShow(0, true, QUERYKEY_LEVEL);
		m_QueryFrame->SetFrameItemShow(1, true, QUERYKEY_RACE);
		m_QueryFrame->SetFrameItemShow(2, false);
		m_QueryFrame->SetFrameItemShow(3, false);
		m_QueryFrame->AddMapCol();
		break;
	case QUERY_METHOD_SPELL:
		m_QueryFrame->SetFrameItemShow(0, true, QUERYKEY_LEVEL);
		m_QueryFrame->SetFrameItemShow(1, true, QUERYKEY_CLASS);
		m_QueryFrame->SetFrameItemShow(2, false);
		m_QueryFrame->SetFrameItemShow(3, false);
		m_QueryFrame->AddSpellCol();
		break;
	case QUERY_METHOD_TITLE:
		m_QueryFrame->SetFrameItemShow(0, true, QUERYKEY_TITLECLASS);
		m_QueryFrame->SetFrameItemShow(1, false);
		m_QueryFrame->SetFrameItemShow(2, false);
		m_QueryFrame->SetFrameItemShow(3, false);
		m_QueryFrame->AddTitleCol();
		break;
	}
	char buf[256];
	sprintf(buf, "%d / %d", mCurrentPage + 1, mCurrentPage + 1);
	m_QueryFrame->SetPageText(buf);
}

void QueryManager::BeginQuery()
{
	m_pQueryMethod[miCurrentQueryMethod]->InitiMemberVector();
}

void QueryManager::Culling(QueryKey key, uint16 Value)
{
	m_pQueryMethod[miCurrentQueryMethod]->RegisterKey(key, Value);
}

void QueryManager::EndQuery()
{
	mCurrentPage = 0;
	mMaxPage = m_pQueryMethod[miCurrentQueryMethod]->FilterFromKey() / (QUERYLIST_PAGE_COUNT + 1);
	m_pQueryMethod[miCurrentQueryMethod]->InitUIFromCurrent(m_QueryFrame->GetQueryList(), mCurrentPage * QUERYLIST_PAGE_COUNT, QUERYLIST_PAGE_COUNT);

	char buf[256];
	sprintf(buf, "%d / %d", mCurrentPage + 1, mMaxPage + 1);
	m_QueryFrame->SetPageText(buf);
}

bool QueryManager::InitiUserInterface(UStaticText* pStaticText, UComboBox* pComboBox, QueryKey key)
{
	pStaticText->SetText(mvQueryString[key].KeyName.c_str());
	bool bEdit = true;
	pComboBox->Clear();
	for (unsigned int ui = 0 ; ui < mvQueryString[key].KeyContent.size() ; ui++)
	{
		pComboBox->AddItem(mvQueryString[key].KeyContent[ui].c_str());
		bEdit = false;
	}
	if (key == QUERYKEY_MAP)
	{
		pComboBox->Clear();
		std::vector<CMapInfoDB::MapInfoEntry> vinfo;
		g_pkMapInfoDB->GetMapVInfoFromFlags(1, vinfo);

		for (unsigned int ui = 0 ; ui < vinfo.size() ; ui++)
		{
			unsigned int* pUiMapId = new unsigned int;
			memcpy(pUiMapId, &vinfo[ui].entry, sizeof(unsigned int));
			pComboBox->AddItem(vinfo[ui].desc.c_str(), pUiMapId);
		}
		bEdit =false;
	}
	if(key == QUERYKEY_AERA)
	{
		CMap* pMap = CMap::Get();
		if(!pMap)
			return false;
		pComboBox->Clear();
		std::vector<AreaInfo*> vInfo;
		g_pkAreaDB->GetAreaInfoList(pMap->GetMapId(), vInfo);

		for (unsigned int ui = 0 ; ui < vInfo.size() ; ui++)
		{
			unsigned int* pUiAreaId = new unsigned int;
			memcpy(pUiAreaId, &vInfo[ui]->entry, sizeof(unsigned int));
			pComboBox->AddItem(vInfo[ui]->kName, pUiAreaId);
		}
		bEdit =false;
	}
	return bEdit;
}
void QueryManager::NextPage()
{
	mCurrentPage++;
	if (mCurrentPage >= mMaxPage)
	{
		mCurrentPage = mMaxPage;
	}

	char buf[256];
	sprintf(buf, "%d/%d", mCurrentPage + 1, mMaxPage + 1);
	m_QueryFrame->SetPageText(buf);

	m_pQueryMethod[miCurrentQueryMethod]->InitUIFromCurrent(m_QueryFrame->GetQueryList(), mCurrentPage * QUERYLIST_PAGE_COUNT, QUERYLIST_PAGE_COUNT);
}
void QueryManager::PrevPage()
{
	if (mCurrentPage > 0)
	{
		mCurrentPage--;
	}	

	char buf[256];
	sprintf(buf, "%d/%d", mCurrentPage + 1, mMaxPage + 1);
	m_QueryFrame->SetPageText(buf);

	m_pQueryMethod[miCurrentQueryMethod]->InitUIFromCurrent(m_QueryFrame->GetQueryList(), mCurrentPage * QUERYLIST_PAGE_COUNT, QUERYLIST_PAGE_COUNT);
}