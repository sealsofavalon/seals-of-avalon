//-----------------------------------------------------------
//任务查询，NPC查询，怪物查询，装备查询，地图查询，技能查询
//------------------------------------------------------------
#ifndef QUERYSYS_H
#define QUERYSYS_H
#include "UInc.h"
#include "UITipSystem.h"

struct QueryKeyString
{
	int keyOpCode;
	std::string KeyName;
	std::vector<std::string> KeyContent;
};
enum QueryItemContentType
{
	QueryItemContentType_FindPath,
	QueryItemContentType_BigMap,
	QueryItemContentType_FittingEquip,
};
struct stQueryItemContent
{
	int type;
	uint32 mapid;
	uint32 npcId;
	uint32 Itementry;
	NiPoint3 kPos;
};

enum QueryMethod
{
	QUERY_METHOD_QUEST,
	QUERY_METHOD_NPC,
	QUERY_METHOD_MONSTER,
	QUERY_METHOD_EQUIP,
	QUERY_METHOD_MAP,
	QUERY_METHOD_SPELL,
	QUERY_METHOD_TITLE,
	QUERY_METHOD_MAX
};
enum QueryKey
{
	QUERYKEY_RACE,
	QUERYKEY_MAP,
	QUERYKEY_AERA,
	QUERYKEY_LEVEL,
	QUERYKEY_CLASS,
	QUERYKEY_EQUIPCLASS,
	//QUERYKEY_EQUIPSUBCLASS,
	QUERYKEY_QUILITY,
	QUERYKEY_TITLECLASS,
	QUERYKEY_MAX,
};
enum
{
	QUERYLIST_PAGE_COUNT = 20,
};
class UQueryList : public UControl
{
	UDEC_CLASS(UQueryList);
	//UDEC_MESSAGEMAP();
public:
	struct ItemContent
	{
		ItemContent(){
			UserData = NULL;
			color = LCOLOR_WHITE;
			bKeyDown = false;
		}
		std::string Text;
		void* UserData;
		UPoint Pos;
		UPoint Size;
		UColor color;
		bool bKeyDown;
	}*m_LastContent;
	struct ListItem
	{
		UPoint Pos;
		UPoint Size;
		bool bMouseHover;
		std::vector<ItemContent> vItemContent;

		void ShowToolTip();
		void HideToolTip();
		uint32 entryID;
		int EntryClass;
	};
	UQueryList();
	~UQueryList();
public:
	void ClearColum();
	void AddColum(const char* Name, UPoint &Size,int TextAlign = UT_CENTER);
	void ClearItem();
	void AddItem(std::vector<ItemContent>& Item, int EntryId = 0,int TipType = TIP_TYPE_MAX);

	void SetItemHeight(int height);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	void DrawColumHead(const UPoint& offset);
	void DrawGirdItem(const UPoint& offset);

	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);

	struct ColumProperty
	{
		std::string Name;
		UPoint Size;
		int TextAlign;
	};
	int mColumHeight;
	int mItemHeight;
	std::vector<ColumProperty> m_Colum;
	std::vector<ListItem> m_ListItem;

	UTexturePtr mItemHoverBkg;


	ListItem* m_LastSelect;
	void SetSelect(ListItem* select)
	{
		if (m_LastSelect)
		{
			m_LastSelect->bMouseHover = false;
			m_LastSelect->HideToolTip();
			m_LastSelect = NULL;
		}
		m_LastSelect = select;
		if (m_LastSelect)
		{
			m_LastSelect->bMouseHover = true;
			m_LastSelect->ShowToolTip();
		}
	}
};
//可以套用模版
class QueryBase
{
public:
	virtual ~QueryBase(){};
public:
	virtual void InitiMemberVector() = 0;
	virtual bool RegisterKey(QueryKey key, uint16 Value)
	{
		RegistKey newEntry;
		newEntry.key = key;
		newEntry.Value = Value;
		if (mAcceptKey[key])
		{
			mvRegistKey.push_back(newEntry);
			return true;
		}
		return false;
	};
	virtual int FilterFromKey() = 0;
	virtual void InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount){};
protected:
	struct RegistKey
	{
		QueryKey key;
		uint16 Value;
	};
	std::vector<RegistKey> mvRegistKey;
	bool mAcceptKey[QUERYKEY_MAX];
};
class QuestQuery : public QueryBase
{
public:
	struct QuestQueryItem
	{
		std::string questName;
		uint32 AllowableRace;
		uint32 Allowablelevel;
		uint32 NPCID;
	};
	QuestQuery();
	virtual void InitiMemberVector();
	virtual int FilterFromKey();
	virtual void InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount);
protected:
	void AddMember(QuestQueryItem& QueryItem);
	std::vector<QuestQueryItem> mvQuestQueryList;
};
class NPCQuery : public QueryBase
{
	struct NPCQueryItem
	{
		uint32 Entry;
		uint32 Race;
		uint32 AreaId;
		uint32 MapId;
		NiPoint3 kPos;
	};
public:
	NPCQuery();
	virtual void InitiMemberVector();
	virtual int FilterFromKey();
	virtual void InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount);
protected:
	void AddMember(NPCQueryItem& QueryItem);
	std::vector<NPCQueryItem> mvNPCQueryList;
};
class MonsterQuery : public QueryBase
{
	struct MonsterQueryItem
	{
		uint32 Entry;
		uint32 Level;
		uint32 AreaId;
		uint32 MapId;
		NiPoint3 kPos;
	};
public:
	MonsterQuery();
	virtual void InitiMemberVector();
	virtual int FilterFromKey();
	virtual void InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount);
protected:
	void AddMember(MonsterQueryItem& QueryItem);
	std::vector<MonsterQueryItem> mvMonsterQueryList;
};
class EquipQuery : public QueryBase
{
public:
	struct EquipQueryItem
	{
		uint32 entry;
		uint32 level;
		uint32 AllowableClass;
		uint32 EquipClass;
		uint32 EquipSubClass;
		uint32 Quility;
		std::string Name;
	};
public:
	EquipQuery();
	virtual void InitiMemberVector();
	virtual int FilterFromKey();
	virtual void InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount);
protected:
	void AddMember(EquipQueryItem& QueryItem);
	std::vector<EquipQueryItem> mvEquipQueryList;
};
class MapQuery : public QueryBase
{
	struct MapQueryItem
	{
		std::string AreaName;
		uint16 min_Level;
		uint16 Max_Level;
		uint32 AllowableRace;
		uint32 MapId;
	};
public:
	MapQuery();
	virtual void InitiMemberVector();
	virtual int FilterFromKey();
	virtual void InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount);
protected:
	void AddMember(MapQueryItem& MapItem);
	std::vector<MapQueryItem> mvMapQueryList;
};
class SpellQuery : public QueryBase
{
	struct SpellQueryItem
	{
		uint32 SpellId;
		std::string SpellName;
		uint32 AllowableLevel;
		uint32 AllowableClass;
	};
public:
	SpellQuery();
	virtual void InitiMemberVector();
	virtual int FilterFromKey();
	virtual void InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount);

protected:
	void AddMember(SpellQueryItem& SpellItem);
	std::vector<SpellQueryItem> mvSpellQueryList;
};

class TitleQuery : public QueryBase
{
	struct TitleQueryItem
	{
		uint8 TitleClass;
		uint32 TitleId;
		std::string TitleName;
	};
public:
	TitleQuery();
	virtual void InitiMemberVector();
	virtual int FilterFromKey();
	virtual void InitUIFromCurrent(UQueryList* pQueryList, int Current, int PageCount);

protected:
	void AddMember(TitleQueryItem& TitleItem);
	std::vector<TitleQueryItem> mvTitleQueryList;
};

class UQueryFrame : public UControl
{
	UDEC_CLASS(UQueryFrame);
	UDEC_MESSAGEMAP();
public:
	UQueryFrame();
	~UQueryFrame();
public:
	void SetFrameItemShow(int index,  bool IsShow = true, QueryKey key = QUERYKEY_MAX);
	void SetPageText(char* buf);
	void AddEquipCol();
	void AddQuestCol();
	void AddNpcCol();
	void AddMonsterCol();
	void AddMapCol();
	void AddSpellCol();
	void AddTitleCol();
	UQueryList* GetQueryList();

	virtual void SetVisible(BOOL value);
protected:
	void OnChangeQueryMethod();
	void OnBeginQuery();
	void OnPrevPage();
	void OnNextPage();
	void OnComboBox1();
	void OnComboBox2();
	void OnComboBox3();
	void OnComboBox4();
	void OnClose();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual BOOL OnEscape();

	USkinPtr m_Popo;
	struct QueryStruct 
	{
		QueryKey key;
		int value;
	};
	QueryStruct m_QueryKey[10];
};
#include "Singleton.h"
#define QueryMgr QueryManager::GetSingleton()

class QueryManager : public Singleton<QueryManager>
{
	SINGLETON(QueryManager);
public:
	bool Create(UControl* root);
public:
	void FindPathMethod(ui32 mapid, float x, float y, float z);
	void SetQueryMethod(int iQueryMethod);
	void BeginQuery();
	void Culling(QueryKey key, uint16 Value);
	void EndQuery();
	bool InitiUserInterface(UStaticText* pStaticText, UComboBox* pComboBox, QueryKey key);
protected:
	UQueryFrame* m_QueryFrame;
	QueryBase* m_pQueryMethod[QUERY_METHOD_MAX];
	int miCurrentQueryMethod;
	QueryKeyString mvQueryString[QUERYKEY_MAX];

	uint16 mCurrentPage;
	uint16 mMaxPage;
public:
	void NextPage();
	void PrevPage();
};
#endif