#pragma once

#include "UControl.h"

class UBitmapButton;

class CQuestGrid : public UControl
{
	UDEC_CLASS(CQuestGrid);

public:
	enum ECheckSkin
	{
		BTS_NORMAL	= 0,
		BTS_MOUSEOVER,
		BTS_PUSHED,
		BTS_DISABLE,
	};
public:
	CQuestGrid(void);
	~CQuestGrid(void);

	void SetGridSize(const UPoint& GridSize);
	void SetHeadSize(const UPoint& dim)			{ m_HeadSize = dim; }
	UPoint GetGridSize(){return m_GridSize;};
	virtual void SetGridCount(int nRow, int nCol);
	const UPoint& GetGridCount(void) const		{ return m_GridCount; }
	BOOL SetSel(int row, int col);
	void ClearSel(void)							{ SetSel(-1,-1); }
	const UPoint& GetSelGrid(void) const;
	UPoint GetGrid(const UPoint& position) const;

	void ScrollToSel();
	void ScrollToGrid(const UPoint& cell);

	UScrollBar* GetScrollBar() { return m_pScrollBar;}

	void SetCheckBoxBitmap(const char* pszFileName);

	void SetTreeConBitmap(const char* pszFilenName);

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);

	virtual void DrawColHeader(UPoint offset, UPoint parentOffset, UPoint headerDim);
	virtual void DrawRowHeader(UPoint offset, UPoint parentOffset, UPoint headerDim, UPoint cell);
	virtual void DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver);

	virtual void OnSelChange(int row, int col);
	virtual void OnCellHighlighted(const UPoint& cell);

	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseMove(const UPoint& pt, UINT uFlags);
	virtual void OnMouseDragged(const UPoint& pt, UINT uFlags);
	virtual void OnMouseEnter(const UPoint& pt, UINT uFlags);
	virtual void OnMouseLeave(const UPoint& pt, UINT uFlags);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);

	virtual void ResizeControl(const UPoint &NewPos, const UPoint &newExtent);

	virtual bool OnCheckButtonClick(int col) {return true;}
	virtual void OnTreeButtonClick(int col) {};
protected:
	UPoint m_HeadSize ;		// Header 大小 x 为列大小, y 为行大小 
	UPoint m_GridCount;		// row col 最大数目
	UPoint m_GridSize;		// 单元格尺寸
	UPoint m_SelGrid;		// 选中的单元格坐标
	UPoint m_HoverGrid;		// 鼠标悬停的单元格坐标
	UScrollBar* m_pScrollBar;
	int m_iCheckIndex;

	BOOL m_bHasScrool;	// 如果控件为滚动条子控件,则表示控件有滚动条.
	BOOL m_bHasVScroll;

	UString		m_strBitmapFile;	// CheckBox
	USkinPtr	m_spBtnSkin;
	UString		m_strTreeConFile;	// TreeCon
	USkinPtr	m_spTreeConSkin;
};
