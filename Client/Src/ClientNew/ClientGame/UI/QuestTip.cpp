#include "StdAfx.h"
#include "QuestTip.h"
#include "UISystem.h"
#include "UITipSystem.h"
#include "UFun.h"

#include "../Utils/QuestDB.h"


QuestTip::QuestTip()
{
}
QuestTip::~QuestTip()
{

}

void QuestTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count)
{
	mEntry = ID;
	mID = ID;
	CQuestDB::stQuest* QuestInfo = g_pkQusetInfo->GetQuestInfo(mID);
	if (QuestInfo)
	{
		m_KeepDraw = TRUE;
		INT posx = Ctrl->GetRoot()->GetWidth() - Ctrl->GetWidth();
		INT BOTTOM = Ctrl->GetRoot()->GetHeight() - 40;
		Ctrl->SetPosition( UPoint(posx, 0) );
		Ctrl->SetBottom(BOTTOM);
		Ctrl->SetVisible(TRUE);
	}
	else
	{
		return;
	}
	if (m_Content.size() > 0)
	{
		m_Content.clear();
	}

	for (int i = 0 ; i < QUEST_TIP_MAX ; i++)
	{
		CreateContentItem(i);
	}
}
void QuestTip::DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl , const UPoint &pos,const URect &updateRect)
{
	if (m_KeepDraw)
	{
		//渲染边框以及内部填充区域
		
		UDrawResizeImage(RESIZE_WANDH, pRender, TipSystem->mBkgSkin, pos, updateRect.GetSize());
		//pRender->DrawRectFill(updateRect,pCtrlStl->mFillColor);
		//pRender->DrawRect(updateRect,pCtrlStl->mBorderColor);
		//渲染文字
		int height = DrawContent(pRender,pCtrlStl,pos,updateRect);
		//设置当前提示框的高度
		if (height != Ctrl->GetHeight())
		{
			Ctrl->SetHeight(height);
			INT BOTTOM = Ctrl->GetRoot()->GetHeight() - 40;
			Ctrl->SetBottom(BOTTOM);
		}
	}
}
void QuestTip::DestoryTip()
{
	mEntry = 0;
	mID = 0;
	m_KeepDraw = FALSE;
	m_Content.clear();
}
void QuestTip::CreateContentItem(int Type)
{
	CQuestDB::stQuest* QuestInfo = g_pkQusetInfo->GetQuestInfo(mID);
	QuestTip::ContentItem Temp_CI;
	if (QuestInfo)
	{
		switch (Type)
		{
		case QUEST_TIP_NAME:
			{
				Temp_CI.Type = QUEST_TIP_NAME;
				Temp_CI.str.Set(QuestInfo->strTitle.c_str());
				Temp_CI.FontFace = TipSystem->GetNameFontFace();
				m_Content.push_back(Temp_CI);
			}
			break;
		case QUEST_TIP_QUESTCONTENT:
			{
				Temp_CI.Type = QUEST_TIP_QUESTCONTENT;
				std::string Explains = _TRAN("任务内容：") + QuestInfo->strDetails;
				ChatPopoFliter(Explains);
				Temp_CI.str.Set(Explains.c_str());
				m_Content.push_back(Temp_CI);
			}
			break;
		};
	}
}
int QuestTip::DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect)
{
	int height = m_BoardWide;
	for (unsigned int i = 0 ; i < m_Content.size() ; i++)
	{
		switch (m_Content[i].Type)
		{
		case QUEST_TIP_NAME:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
				
				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				height += size.y;
			}
			break;
		case QUEST_TIP_QUESTCONTENT:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2 * m_BoardWide;
		
				if (m_Content[i].FontFace)
				{
					size.y = UDrawStaticText(pRender,m_Content[i].FontFace,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				}
				else
				{
					size.y = UDrawStaticText(pRender,pCtrlStl->m_spFont,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				}

				height += size.y;
			}
			break;
		}
	}
	return height + m_BoardWide;
}