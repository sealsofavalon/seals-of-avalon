#ifndef QUESTTIP_H
#define QUESTTIP_H
//------------------------------------------------------
//任务显示的鼠标提示
//------------------------------------------------------
//任务显示需要什么？

//****************************
//*任务名称                  *
//*称号类型			 称号时间*
//*称号属性：                *
//*XXXXXXXXXXXXXXXXXXXXXXXXXX*
//*称号获得:				 *
//*XXXXXXXXXXXXXXXXXXXXXXXXXX*
//****************************
//那么后面渲染时就要按照这个样式来进行
#include "MouseTip.h"
enum
{
	QUEST_TIP_NAME,
	QUEST_TIP_QUESTCONTENT,
	QUEST_TIP_MAX
};
class QuestTip :public MouseTip
{
	struct ContentItem  
	{
		ContentItem():Type(QUEST_TIP_MAX),FontFace(NULL),FontColor(UColor(255,255,255,255)){}
		UStringW str;
		UColor FontColor;
		UFontPtr FontFace;
		int Type;
	};
public:
	QuestTip();
	~QuestTip();
public:
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count);
	virtual void CreateContentItem(int Type);

	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect);
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect);

	virtual void DestoryTip();
private:
	vector<ContentItem> m_Content;
};
#endif