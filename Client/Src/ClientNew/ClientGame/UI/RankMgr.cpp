#include "stdafx.h"
#include "RankMgr.h"
#include "UIIndex.h"
#include "Network/PacketBuilder.h"

//3个小时 后才能请求新的排行榜列表
#define  RANKWAITTIME 10800.0f

URankMgr::URankMgr()
{
	m_curRace = RACE_MIN;
	m_RankType = LADDER_LEVEL;
	m_RankUI = NULL;

	m_SendTime = 0.0f;
	m_bStartTime = false ;
}
URankMgr::~URankMgr()
{

}

BOOL URankMgr::CreateRankUI(UControl* ctrl)
{
	if (ctrl)
	{
		if (!m_RankUI)
		{
			UControl* pkCtrol = UControl::sm_System->CreateDialogFromFile("Rank\\Rank.udg");
			m_RankUI = UDynamicCast(URank, pkCtrol);
			if (!m_RankUI)
			{
				return FALSE;
			}else
			{
				ctrl->AddChild(m_RankUI);
				m_RankUI->SetId(FRAME_IG_RANKDLG);
			}
			
		}
		//m_RankUI->SetVisible(FALSE);
		return TRUE;
	}
	return FALSE ;
}
void URankMgr::ParseRankData( int8 category, ladder_vector* pkData)
{
	for( int j = 0; j < RACE_MAX; ++j )
	{
		m_curData.lv[category][j].clear() ;
		for (size_t ui = 0 ; ui < pkData[j].size(); ui++)
		{
			m_curData.lv[category][j].push_back(pkData[j][ui]);
		}
	}
	if( category == LADDER_MAX - 1)
		GetShowData(m_RankType, m_curRace);
}
bool URankMgr::SendShowRank()
{
	if (!m_RankUI->IsVisible())
	{	
		if (!m_bStartTime)
		{
			PacketBuilder->SendLadderReq();	
			m_bStartTime = true ;
		}
		m_RankUI->SetVisible(TRUE);
	}else
	{
		m_RankUI->OnClose();
	}
	return true;
}
void URankMgr::ChangeType(int stType)
{
	if ((stType != m_RankType && stType < LADDER_MAX) || m_RankUI->IsInSeach())
	{
		GetShowData(stType, m_curRace);
	}
}
void URankMgr::ChangRace(Races stRace)
{
	if (stRace != m_curRace  || m_RankUI->IsInSeach())
	{
		GetShowData(m_RankType, stRace);
	}
}

void URankMgr::GetShowData(int stType, Races stRace)
{
	m_RankUI->SetVisible(TRUE);
	m_RankType = stType ;
	m_curRace = stRace ;

	m_RankUI->InitUIList(stType);
	m_RankUI->SetDataList(m_curData.lv[m_RankType][m_curRace]);
}

void URankMgr::InitRankTime()
{
	m_SendTime = 0.0f;
	m_bStartTime = false;
}

void URankMgr::UpdateRankTime(float fTime)
{
	if (m_bStartTime)
	{
		m_SendTime += fTime;
		if (m_SendTime >= RANKWAITTIME)
		{
			m_bStartTime = false ;
			m_SendTime = 0.0f;
		}
	}
}