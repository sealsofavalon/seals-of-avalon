#ifndef __RANKMGR_H__
#define __RANKMGR_H__

//////////////////////////////////////////////////////////////////////////
//                           排行榜                                    ///
//////////////////////////////////////////////////////////////////////////
#include "../../../../SDBase/Public/PlayerDef.h"
#include "URank.h"
#include "Singleton.h"
//
//#ifndef TEST_CLIENT
//const int ShowLadder_category[] = 
//{	
//	LADDER_KILL_COUNT,
//	LADDER_BE_KILL_COUNT,
//	LADDER_TOTAL_HONOR,
//	LADDER_LEVEL,
//	LADDER_COIN,
//	LADDER_GUILD_LEVEL,
//	LADDER_ARENA_1V1,
//	LADDER_ARENA_2V2,
//	LADDER_ARENA_3V3,
//	LADDER_ARENA_4V4,
//	LADDER_ARENA_5V5,
//};
//#endif

#define RankMgr URankMgr::GetSingleton()
class URankMgr : public Singleton<URankMgr>
{
	SINGLETON(URankMgr)
public:
	void ParseRankData( int8 category, ladder_vector* pkData);
	BOOL CreateRankUI(UControl* ctrl);
	
	bool SendShowRank();

	void ChangeType(int stType);
	void ChangRace(Races stRace);
	void GetShowData(int stType, Races stRace);

	void InitRankTime();
	void UpdateRankTime(float fTime);
	//这里会根据UI的选择需求,然后把排行榜里的数据刷新到UI.
	//每次选项变化的时候,都会更新UI数据.
	//UI的分页放在UI里进行

protected:
private:
	int m_RankType ;
	Races m_curRace;
	ladders m_curData ;  //排行版里面的数据。
	URank* m_RankUI;

	float m_SendTime ;
	bool  m_bStartTime;
};
#endif