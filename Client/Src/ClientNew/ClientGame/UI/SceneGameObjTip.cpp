#include "stdafx.h"
#include "UITipSystem.h"
#include "SceneGameObjTip.h"
#include "../ClientGame/Utils/LockDB.h"
#include "../../../../SDBase/Public/DBCStores.h"
#include "ObjectManager.h"

SceneGameObjTip::SceneGameObjTip()
{

}
SceneGameObjTip::~SceneGameObjTip()
{

}
void SceneGameObjTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid ,UINT count)
{
	mID = guid;
	mEntry = ID;
	m_KeepDraw = TRUE;
	Ctrl->SetPosition(pos);
	Ctrl->SetVisible(TRUE);


	CreateContent();
}
void SceneGameObjTip::CreateContent()
{
	m_Content.clear();
	CGameObjectDB::stGameObject* pkData = g_pkGameObjectDB->GetGameObject(mEntry);

	Lock* plock = g_LockObjects->GetObjectLock( pkData->SpellFocus );

	if (pkData)
	{
		SceneGameObjTip::ContentItem pkContentItem;
		pkContentItem.str.Set(pkData->Name.c_str());

		SceneGameObjTip::ContentItem pkContentItemReq;
		//pkContentItemReq.str.Set(_TRAN("可以采集"));

		if ( plock)
		{
			for (int i = 0; i < 8; i ++)
			{
				switch (plock->locktype[i])
				{
				case 2:
					{
						if( plock->lockmisc[i] == LOCKTYPE_HERBALISM)
						{
							CPlayerLocal* plLocal = ObjectMgr->GetLocalPlayer();
							if ( plLocal->GetProfessionState( 182 ) && ( plLocal->GetProfessionState( 182 )->current < plock->minlockskill[i] ) )
							{
								pkContentItemReq.FontColor = 0xffff1111;
								char buff[32] = {0};
								sprintf( buff, "需要岐黄术 %d", plock->minlockskill[i] );
								pkContentItemReq.str.Set( _TRAN(buff) );
							}
						}

						if( plock->lockmisc[i] == LOCKTYPE_MINING)
						{
							CPlayerLocal* plLocal = ObjectMgr->GetLocalPlayer();
					
							uint32 reqitem = pkData->ReqItem;
							if ( reqitem && !plLocal->HasItem( reqitem))
							{
								pkContentItemReq.FontColor = 0xffff1111;
								pkContentItemReq.str.Set( _TRAN("需要采集工具") );
							}

							uint32 skilllock = plock->minlockskill[i];
							if ( ( plLocal->GetProfessionState( 186 ) && ( plLocal->GetProfessionState( 186 )->current < skilllock )) )
							{
								pkContentItemReq.FontColor = 0xffff1111;
								char buff[32] = {0};
								sprintf( buff, "需要矿产学 %d", skilllock );
								pkContentItemReq.str.Set( _TRAN(buff) );
							}

							
						}
					}
					break;
				}
			}
		}
		m_Content.push_back(pkContentItem);
		if ( pkContentItemReq.str.GetLength() )
		{
			m_Content.push_back(pkContentItemReq);
		}
		

	}
}
void SceneGameObjTip::DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect )
{
	if (m_KeepDraw)
	{
		UDrawResizeImage(RESIZE_WANDH, pRender, TipSystem->mBkgSkin, pos, updateRect.GetSize());
		int height = DrawContent(pRender,pCtrlStl,pos,updateRect);
		//设置当前提示框的高度
		if (height != Ctrl->GetHeight())
		{
			Ctrl->SetHeight(height);
		}
	}
}
void SceneGameObjTip::DestoryTip()
{
	m_Content.clear();
	m_KeepDraw = FALSE;
}
int SceneGameObjTip::DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect)
{
	int height = m_BoardWide;
	for (size_t i =0; i < m_Content.size(); i++)
	{
		
		UFontPtr strFont ;
		if (m_Content[i].FontFace)
		{
			strFont = m_Content[i].FontFace;
		}else
		{
			strFont = pCtrlStl->m_spFont;
		}
		UPoint pos,size; 
		size.x = updateRect.GetWidth() - 2*m_BoardWide;
		size.y =strFont->GetHeight();
		pos.Set(Pos.x  + m_BoardWide, Pos.y +height);
		UDrawText(pRender,strFont, pos, size,m_Content[i].FontColor,m_Content[i].str);
		int AddH = strFont->GetHeight() + 2;
		height += AddH ;
	}

	return height + m_BoardWide;
	
}
void SceneGameObjTip::CreateContentItem(int Type)
{
	
}