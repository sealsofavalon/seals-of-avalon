#ifndef __SCENEGAMEOBJTIP_H__
#define __SCENEGAMEOBJTIP_H__
#include "MouseTip.h"
#include "Utils/GameObjectDB.h"

//
// name
// type
// need skill
// need item
// describe




//
enum
{
	SceneOBJ_Name = 0,
	SceneOBJ_Type,
	SceneOBJ_Skill,
	SceneOBJ_Item,
	SceneOBJ_Quest,
	SceneOBJ_Max,
};


class SceneGameObjTip : public MouseTip
{
	struct ContentItem 
	{
		ContentItem():Type(SceneOBJ_Max),FontFace(NULL),FontColor(UColor(255,255,255,255)){}
		UStringW str;
		UColor FontColor;
		UFontPtr FontFace;
		int Type;
	};

public:
	SceneGameObjTip();
	~SceneGameObjTip();
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid ,UINT count);
	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect );
	virtual void DestoryTip();
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect);
	virtual void CreateContentItem(int Type);
protected:
	void CreateContent();
private:
	std::vector<ContentItem> m_Content ;
};
#endif