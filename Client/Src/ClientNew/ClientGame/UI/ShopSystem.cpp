#include "stdafx.h"
#include "ShopSystem.h"
#include "UIGamePlay.h"
#include "UInGameBar.h"
#include "UIItemSystem.h"
#include "UDynamicIconButtonEx.h"
#include "ItemManager.h"
#include "UITipSystem.h"
#include "../Network/PacketBuilder.h"
#include "UFun.h"
#include "ObjectManager.h"
#include "UI/UMessageBox.h"
#include "UIMoneyText.h"

#define PAGE_ITEM_COUNT 8
//////////////////////////////////////////////////////////////////////////////
//ShopItemExplain
//////////////////////////////////////////////////////////////////////////////
class ShopItemExplain : public UControl
{
	UDEC_CLASS(ShopItemExplain);
	UDEC_MESSAGEMAP();
	class ItemExplainCon : public UDynamicIconButtonEx
	{
		UDEC_CLASS(ShopItemExplain::ItemExplainCon);
	public:
		ItemExplainCon();
		~ItemExplainCon();
		virtual void OnMouseDragged(const UPoint& position, UINT flags){};
		virtual void OnMouseEnter(const UPoint& position, UINT flags){return UDynamicIconButtonEx::OnMouseEnter(position,flags);};
		virtual void OnMouseLeave(const UPoint& pt, UINT uFlags){return UDynamicIconButtonEx::OnMouseLeave(pt,uFlags);};
		virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
		{
			ShopItemExplain * pShopItemExplain = UDynamicCast(ShopItemExplain, GetParent());
			if (nRepCnt >= 2 && pShopItemExplain)
			{
				pShopItemExplain->Buy();
			}
			UDynamicIconButtonEx::OnMouseDown(point,nRepCnt,uFlags);
		}
		virtual void OnMouseUp(const UPoint& position, UINT flags){UDynamicIconButtonEx::OnMouseUp(position,flags);}
		virtual void DrawPushButton(const UPoint& offset, const URect& ClipRect)
		{
			URect rect(offset,m_Size);
			if (m_BackGSkin)
			{
				if (m_Active)
				{
					sm_UiRender->DrawSkin(m_BackGSkin,BACKSKIN_ACTIVE,rect);
				}
				else
				{
					sm_UiRender->DrawSkin(m_BackGSkin,BACKSKIN_INACTIVE,rect);
				}
			}
			if (m_IconSkin)
			{
				URect IconRect( UPoint(m_IconOffset,m_IconOffset) , m_OldSize - UPoint(m_IconOffset*2,m_IconOffset*2));
				IconRect.TransformRect(m_OldSize,m_Size);
				IconRect.Offset(offset);
				sm_UiRender->DrawSkin(m_IconSkin,m_IconIndex,IconRect);
			}
			if (m_ActionContener.IsCreate())
			{
				m_ActionContener.DrawActionContener(sm_UiRender,offset,ClipRect);
			}
		}
	};
	friend class ItemExplainCon;
	class ItemExplainItem : public UActionItem
	{
	public:
		virtual void Use();
	};
	class ItemBkgBitmap : public UControl
	{
		UDEC_CLASS(ShopItemExplain::ItemBkgBitmap);
	public:
		ItemBkgBitmap()
		{
			m_spBkgSkin = NULL;
			m_BkgSkinIndex = 0;
		}
		~ItemBkgBitmap()
		{
			m_spBkgSkin = NULL;
			m_BkgSkinIndex= 0;
		}
		virtual BOOL OnCreate()
		{
			if (!UControl::OnCreate())
			{
				return FALSE;
			}
			if (m_strFileName.GetBuffer())
			{
				m_spBkgSkin = sm_System->LoadSkin(m_strFileName.GetBuffer());
				if (m_spBkgSkin == NULL)
				{
					return FALSE;
				}
			}
			return TRUE;
		}
		virtual void OnDestroy()
		{
			m_BkgSkinIndex = 0;
			return UControl::OnDestroy();
		}
		virtual void OnRender(const UPoint& offset,const URect &updateRect)
		{
			sm_UiRender->DrawSkin(m_spBkgSkin,m_BkgSkinIndex,updateRect);
		}
		virtual void OnMouseEnter(const UPoint& position, UINT flags)
		{
			m_BkgSkinIndex = 1;
		};
		virtual void OnMouseLeave(const UPoint& pt, UINT uFlags)
		{
			m_BkgSkinIndex = 0;
		};
		void SetChoose(BOOL choose)
		{
			m_BkgSkinIndex = choose?1:0;
		};
	private:
		UString m_strFileName;
		USkinPtr m_spBkgSkin;
		int m_BkgSkinIndex;
	};
public:
	ShopItemExplain();
	~ShopItemExplain();
public:
	void Buy()
	{
		UInGame * pInGame = UInGame::Get();
		if (pInGame)
		{
			ShopSystem * pShop = INGAMEGETFRAME(ShopSystem,FRAME_IG_SHOPDLG);
			if (pShop)
			{
				pShop->BuyItem(GetItem().id, GetTypeSel());
			}
		}
	}
	void SetItem(MSG_S2C::stShopItem &ItemInfo , int index);
	MSG_S2C::stShopItem & GetItem(){return m_ItemInfo;};
	UINT GetTypeSel(){return m_ItemLife->GetCurSel();};
	void SetChoose(BOOL choose)
	{
		m_ItemBkgBitmap->SetChoose(choose);
		m_bChoose = choose;
	};
	void Clear()
	{
		if(m_ItemExplainCon)
		{
			ActionDataInfo datainfo;
			m_ItemExplainCon->SetItemData(datainfo);
		}
		if (m_ItemName && m_ItemCount && m_ItemLife &&m_ItemPrice)
		{
			m_ItemName->SetText("");
			m_ItemCount->SetText("");
			m_ItemLife->SetVisible(FALSE);
			m_ItemLife->Clear();
			m_ItemPrice->SetText("");
		}
		m_fActivityDisCount = 100;
	}
	void SetActivityDiscount(uint32 discount){m_fActivityDisCount = discount;}
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual UControl * FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */)
	{
		if (m_ItemLife->GetControlRect().PointInRect(pt))
		{
			return UControl::FindHitWindow(pt, initialLayer);
		}
		return this;
	};
	virtual void OnMouseEnter(const UPoint& position, UINT flags)
	{
		m_ItemExplainCon->OnMouseEnter(position,flags);
		m_ItemBkgBitmap->OnMouseEnter(position,flags);
	}	
	virtual void OnMouseLeave(const UPoint& position, UINT flags)
	{
		m_ItemExplainCon->OnMouseLeave(position,flags);
		if (!m_bChoose)
		{
			m_ItemBkgBitmap->OnMouseLeave(position,flags);
		}
	}
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
	{
		m_ItemExplainCon->OnMouseDown(point,nRepCnt,uFlags);
	}
	virtual void OnMouseUp(const UPoint& position, UINT flags)
	{
		m_ItemExplainCon->OnMouseUp(position,flags);
	}

	void OnChangeBuyType()
	{
		int lifesel = m_ItemLife->GetCurSel();
		if (m_ItemInfo.buyprice[lifesel])
		{
			//sprintf(buf, "%d元宝", m_ItemInfo.buyprice[lifesel]);
			uint32 realBuyPrice = uint32(m_ItemInfo.buyprice[lifesel] * m_fActivityDisCount / 100);
			if (realBuyPrice == 0)
				realBuyPrice = 1;
			std::string strRet = _TRAN(N_NOTICE_Z102, _I2A(realBuyPrice).c_str() );
			m_ItemPrice->SetText(strRet.c_str());
		}else{
			m_ItemPrice->SetText(_TRAN("缺货"));
		}
	}
private:
	MSG_S2C::stShopItem m_ItemInfo;
	BOOL m_bChoose;
	ItemBkgBitmap * m_ItemBkgBitmap;
	ItemExplainCon * m_ItemExplainCon;
	UStaticText * m_ItemName;
	UStaticText * m_ItemCount;
	UComboBox * m_ItemLife;
	UStaticText * m_ItemPrice;
	uint32 m_fActivityDisCount;
};
UIMP_CLASS(ShopItemExplain::ItemBkgBitmap,UControl);
void ShopItemExplain::ItemBkgBitmap::StaticInit()
{
	UREG_PROPERTY("bkgskin", UPT_STRING, UFIELD_OFFSET(ShopItemExplain::ItemBkgBitmap, m_strFileName));
}
UIMP_CLASS(ShopItemExplain::ItemExplainCon,UDynamicIconButtonEx);
void ShopItemExplain::ItemExplainCon::StaticInit()
{

}
ShopItemExplain::ItemExplainCon::ItemExplainCon()
{
	m_IsTickable = FALSE;
	m_TypeID = UItemSystem::ICT_INVALID_POS;
	m_ActionContener.SetActionItem(new ShopItemExplain::ItemExplainItem);
}
ShopItemExplain::ItemExplainCon::~ItemExplainCon()
{

}
UIMP_CLASS(ShopItemExplain,UControl);
UBEGIN_MESSAGE_MAP(ShopItemExplain, UControl)
UON_CBOX_SELCHANGE(4, &ShopItemExplain::OnChangeBuyType)
UEND_MESSAGE_MAP();
void ShopItemExplain::ItemExplainItem::Use()
{
	UInGame * pInGame = UInGame::Get();
	if (pInGame)
	{
		ShopSystem * pShop = INGAMEGETFRAME(ShopSystem,FRAME_IG_SHOPDLG);
		if (pShop)
		{
			pShop->SetChoose(GetDataInfo().pos,TRUE);
		}
	}
}
void ShopItemExplain::StaticInit()
{

}
ShopItemExplain::ShopItemExplain()
{
	m_ItemExplainCon = NULL;
	m_ItemBkgBitmap = NULL;
	m_ItemName = NULL;
	m_ItemCount = NULL;
	m_ItemLife = NULL;
	m_ItemPrice = NULL;
	m_bChoose = FALSE;
	m_fActivityDisCount = 100;
}
ShopItemExplain::~ShopItemExplain()
{
	m_ItemExplainCon = NULL;
	m_ItemBkgBitmap = NULL;
	m_ItemName = NULL;
	m_ItemCount = NULL;
	m_ItemLife = NULL;
	m_ItemPrice = NULL;
	m_bChoose = FALSE;
}
BOOL ShopItemExplain::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_ItemExplainCon == NULL)
	{
		m_ItemExplainCon = UDynamicCast(ItemExplainCon,GetChildByID(0));
		if (!m_ItemExplainCon)
		{
			return FALSE;
		}
	}
	if (m_ItemBkgBitmap == NULL)
	{
		m_ItemBkgBitmap = UDynamicCast(ItemBkgBitmap,GetChildByID(1));
		if (!m_ItemBkgBitmap)
		{
			return FALSE;
		}
	}
	if (m_ItemName == NULL)
	{
		m_ItemName = UDynamicCast(UStaticText,GetChildByID(2));
		if (!m_ItemName)
		{
			return FALSE;
		}
	}
	if (m_ItemCount == NULL)
	{
		m_ItemCount = UDynamicCast(UStaticText,GetChildByID(3));
		if (!m_ItemCount)
		{
			return FALSE;
		}
	}
	if (m_ItemLife == NULL)
	{
		m_ItemLife = UDynamicCast(UComboBox,GetChildByID(4));
		if (!m_ItemLife)
		{
			return FALSE;
		}
	}
	if (m_ItemPrice == NULL)
	{
		m_ItemPrice = UDynamicCast(UStaticText,GetChildByID(5));
		if (!m_ItemPrice)
		{
			return FALSE;
		}
	}
	return TRUE;
}
void ShopItemExplain::OnDestroy()
{
	UControl::OnDestroy();
}

void ShopItemExplain::OnRender(const UPoint& offset,const URect &updateRect)
{
	RenderChildWindow(offset,updateRect);
}
void ShopItemExplain::SetItem(MSG_S2C::stShopItem &ItemInfo , int index)
{
	m_ItemInfo = ItemInfo;
	ActionDataInfo dataInfo;
	dataInfo.entry = ItemInfo.itementry;
	/*dataInfo.Guid = ItemInfo.id;*/dataInfo.Guid = 0;
	dataInfo.pos = index;
	dataInfo.num = ItemInfo.itemcnt;
	dataInfo.type = UItemSystem::ITEM_DATA_FLAG;
	if (m_ItemExplainCon)
	{
		m_ItemExplainCon->SetItemData(dataInfo);
	}
	ItemPrototype_Client* CItem = ItemMgr->GetItemPropertyFromDataBase(dataInfo.entry);
	if (CItem && m_ItemName && m_ItemCount && m_ItemLife && m_ItemPrice)
	{
		m_ItemName->SetText(CItem->C_name.c_str());
		char buf[1024];
		_itoa(dataInfo.num,buf,10);
		m_ItemCount->SetText(buf);
		m_ItemLife->Clear();
		m_ItemLife->SetVisible(TRUE);
		for (int i = 0 ; i < 5 ; i++)
		{
			switch (ItemInfo.buytype[i])
			{
			case 1:
				m_ItemLife->AddItem(_TRAN("1天"));
				break;
			case 2:
				m_ItemLife->AddItem(_TRAN("1周"));
				break;
			case 3:
				m_ItemLife->AddItem(_TRAN("1月"));
				break;
			case 4:
				m_ItemLife->AddItem(_TRAN("永久"));
				break;
			}
		}
		m_ItemLife->SetCurSel(0);
		/*sprintf_s(buf,1024,"%d元宝",ItemInfo.buyprice[0]);
		m_ItemPrice->SetText(buf);*/
	}
}
//////////////////////////////////////////////////////////////////////////////
//ShopHotItemExplain
//////////////////////////////////////////////////////////////////////////////
class ShopHotItemExplain : public UControl
{
	UDEC_CLASS(ShopHotItemExplain);
	class HotItemExplainCon : public UDynamicIconButtonEx
	{
		UDEC_CLASS(ShopHotItemExplain::HotItemExplainCon);
	public:
		HotItemExplainCon();
		~HotItemExplainCon();
		virtual void OnMouseDragged(const UPoint& position, UINT flags){};
		virtual void DrawPushButton(const UPoint& offset, const URect& ClipRect)
		{
			URect rect(offset,m_Size);
			if (m_BackGSkin)
			{
				if (m_Active)
				{
					sm_UiRender->DrawSkin(m_BackGSkin,BACKSKIN_ACTIVE,rect);
				}
				else
				{
					sm_UiRender->DrawSkin(m_BackGSkin,BACKSKIN_INACTIVE,rect);
				}
			}
			if (m_IconSkin)
			{
				URect IconRect( UPoint(m_IconOffset,m_IconOffset) , m_OldSize - UPoint(m_IconOffset*2,m_IconOffset*2));
				IconRect.TransformRect(m_OldSize,m_Size);
				IconRect.Offset(offset);
				sm_UiRender->DrawSkin(m_IconSkin,m_IconIndex,IconRect);
			}
			if (m_ActionContener.IsCreate())
			{
				m_ActionContener.DrawActionContener(sm_UiRender,offset,ClipRect);
			}
		}

		virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
		{
			if (nRepCnt >= 2)
			{
				UInGame * pInGame = UInGame::Get();
				if (pInGame)
				{
					ShopSystem * pShop = INGAMEGETFRAME(ShopSystem,FRAME_IG_SHOPDLG);
					if (pShop)
					{
						ActionDataInfo * DataInfo = GetItemData();
						if (DataInfo)
						{
							pShop->BuyItem(int(DataInfo->Guid), 0);
						}
					}
				}
			}
			UDynamicIconButtonEx::OnMouseDown(point,nRepCnt,uFlags);
		}
	};
	friend class HotItemExplainCon;
	class HotItemExplainItem : public UActionItem
	{
	public:
		virtual void Use();
	};
public:
	ShopHotItemExplain();
	~ShopHotItemExplain();
public:
	void SetItem(ActionDataInfo &ItemInfo);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect){RenderChildWindow(offset,updateRect);}
private:
	HotItemExplainCon * m_HotItem;
	UStaticText * m_ItemName;
};
UIMP_CLASS(ShopHotItemExplain::HotItemExplainCon,UDynamicIconButtonEx);
void ShopHotItemExplain::HotItemExplainCon::StaticInit()
{

}
ShopHotItemExplain::HotItemExplainCon::HotItemExplainCon()
{
	m_IsTickable = FALSE;
	m_TypeID = UItemSystem::ICT_INVALID_POS;
	m_ActionContener.SetActionItem(new ShopHotItemExplain::HotItemExplainItem);
}
ShopHotItemExplain::HotItemExplainCon::~HotItemExplainCon()
{

}
UIMP_CLASS(ShopHotItemExplain,UControl);
void ShopHotItemExplain::HotItemExplainItem::Use()
{

}
void ShopHotItemExplain::StaticInit()
{

}
ShopHotItemExplain::ShopHotItemExplain()
{
	m_HotItem = NULL;
	m_ItemName = NULL;
}
ShopHotItemExplain::~ShopHotItemExplain()
{
	m_HotItem = NULL;
	m_ItemName = NULL;
}
BOOL ShopHotItemExplain::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_HotItem == NULL)
	{
		m_HotItem = UDynamicCast(HotItemExplainCon,GetChildByID(0));
		if (!m_HotItem)
		{
			return FALSE;
		}
	}
	if (m_ItemName == NULL)
	{
		m_ItemName = UDynamicCast(UStaticText,GetChildByID(1));
		if (!m_ItemName)
		{
			return FALSE;
		}
	}
	return TRUE;
}
void ShopHotItemExplain::OnDestroy()
{
	UControl::OnDestroy();
}
void ShopHotItemExplain::SetItem(ActionDataInfo &ItemInfo)
{
	if (m_HotItem)
	{
		m_HotItem->SetItemData(ItemInfo);
		ItemPrototype_Client* CItem = ItemMgr->GetItemPropertyFromDataBase(ItemInfo.entry);
		if (CItem)
		{
			m_ItemName->SetText(CItem->C_name.c_str());
		}
	}
}
//////////////////////////////////////////////////////////////////////////////
//Pay
//////////////////////////////////////////////////////////////////////////////
uint32 g_BuyItemId = 0;
uint8 g_BuyItemType = 0;
uint8 g_BuyItemCnt = 1;
void BuyItem()
{
	PacketBuilder->SendShopBuyMsg(g_BuyItemId,g_BuyItemType,g_BuyItemCnt);
}
void cancle(){}
class UPay : public UDialog
{
	UDEC_CLASS(UPay);
	UDEC_MESSAGEMAP();
public:
	UPay()
	{
		m_Name = NULL;
		m_level = NULL;
		m_Icon = NULL;
		mpBuyNum = NULL;
		m_IconIndex = 0;
		m_dicount = 100;
		m_IconRect = URect(UPoint(160,50),UPoint(36,36));
		m_BuyTypeIndex = 0;
		memset(m_CheckBuyTypeBtn, 0, sizeof(m_CheckBuyTypeBtn));
		memset(m_BuyTypeText, 0, sizeof(m_BuyTypeText));
	};
	~UPay()
	{
		m_Name = NULL;
		m_level = NULL;
		m_Icon = NULL;
		m_IconIndex = 0;	
		m_IconRect = URect(UPoint(160,50),UPoint(36,36));
		m_BuyTypeIndex = 0;
	};
public:
	void SetItemData(MSG_S2C::stShopItem& ItemData, UINT SelIndex, UINT cnt, uint32 discount)
	{
		GetRoot()->MoveChildTo(this, NULL);
		SetVisible(TRUE);
		m_ItemInfo = ItemData;
		ItemPrototype_Client* CItem = ItemMgr->GetItemPropertyFromDataBase(ItemData.itementry);
		if (m_Name && m_level && CItem)
		{
			char buf[256];
			UColor _color(0,0,0,0);

			GetQualityColor(CItem->Quality, _color);

			m_Name->SetTextColor(_color);
			std::string ItemName = CItem->C_name ;
			if(cnt > 1)
			{
				sprintf_s(buf,256,"%s*%d",CItem->C_name.c_str(), cnt);
				ItemName = buf;
			}
				
			m_Name->SetText(ItemName.c_str());

			std::string strRet = _TRAN(N_NOTICE_Z103, _I2A(CItem->RequiredLevel).c_str() );
			sprintf_s(buf,256,strRet.c_str());

			m_level->SetText(buf);
			int MaxTextLenght = 0;
			m_dicount = discount;
			for (int i = 0 ; i < 5 ;i++)
			{
				UStringW text;
				uint32 realPrice = m_ItemInfo.buyprice[i] * discount / 100;
				if (realPrice == 0)
					realPrice = 1;
				if (m_ItemInfo.buyprice[i])
				{
					switch (m_ItemInfo.buytype[i])
					{
					case 1:
						strRet = _TRAN(N_NOTICE_Z104, _I2A(realPrice).c_str() );
						sprintf_s(buf,256,strRet.c_str());
						break;
					case 2:
						strRet = _TRAN(N_NOTICE_Z105, _I2A(realPrice).c_str() );
						sprintf_s(buf,256,strRet.c_str());	
						break;
					case 3:
						strRet = _TRAN(N_NOTICE_Z106, _I2A(realPrice).c_str() );
						sprintf_s(buf,256, strRet.c_str());	
						break;
					case 4:
						strRet = _TRAN(N_NOTICE_Z107, _I2A(realPrice).c_str() );
						sprintf_s(buf,256,strRet.c_str());	
						break;
					default:
						sprintf_s(buf,256,_TRAN("无该种类型"));	
						break;
					}
					if (m_BuyTypeText[i] && m_CheckBuyTypeBtn[i])
					{
						m_CheckBuyTypeBtn[i]->SetVisible(TRUE);
						m_BuyTypeText[i]->SetText(buf);
					}
				}
				else
				{
					if (m_BuyTypeText[i] && m_CheckBuyTypeBtn[i])
					{
						m_BuyTypeText[i]->Clear();
						m_CheckBuyTypeBtn[i]->SetVisible(FALSE);
					}
				}
			}
			if(CItem->MaxCount > 1 && cnt == 1)
			{
				GetChildByID(0)->SetVisible(FALSE);
				GetChildByID(1)->SetVisible(FALSE);
				GetChildByID(16)->SetVisible(FALSE);
			}
			else
			{
				GetChildByID(0)->SetVisible(FALSE);
				GetChildByID(1)->SetVisible(FALSE);
				GetChildByID(16)->SetVisible(FALSE);
			}
			m_CheckBuyTypeBtn[SelIndex]->SetCheck(FALSE);
			SetIcon(ItemData.itementry);
			mpBuyNum->SetText("1");
		}
	};
protected:
	virtual BOOL OnCreate()
	{
		if (!UDialog::OnCreate())
		{
			return FALSE;
		}
		if (m_Name == NULL)
		{
			m_Name = UDynamicCast(UStaticText,GetChildByID(3));
			if (!m_Name)
			{
				return FALSE;
			}
			m_Name->SetTextAlign(UT_CENTER);
		}
		if (m_level == NULL)
		{
			m_level = UDynamicCast(UStaticText,GetChildByID(4));
			if (!m_level)
			{
				return FALSE;
			}
			m_level->SetTextAlign(UT_CENTER);
		}
		if(mpBuyNum == NULL)
		{
			mpBuyNum = UDynamicCast(UEdit, GetChildByID(0));
			if(mpBuyNum)
			{
				mpBuyNum->SetText("1");
				mpBuyNum->SetNumberOnly(TRUE);
				mpBuyNum->SetMaxLength(2);
			}
		}
		for (int i = 0 ; i < 4 ; i++)
		{
			m_CheckBuyTypeBtn[i] = UDynamicCast(UButton, GetChildByID(i + 8));
			if (!m_CheckBuyTypeBtn[i])
			{
				return FALSE;
			}
			m_BuyTypeText[i] = UDynamicCast(UStaticText, GetChildByID(i + 12));
			if (!m_BuyTypeText[i])
			{
				return FALSE;
			}
		}

		m_bCanDrag = FALSE;
		return TRUE;
	};
	virtual void OnDestroy()
	{
		UDialog::OnDestroy();
	};
	virtual void OnRender(const UPoint& offset,const URect &updateRect)
	{
		URect oldRect = sm_UiRender->GetClipRect();
		if (m_spBkgSkin)
		{
			sm_UiRender->DrawSkin(m_spBkgSkin, 0, updateRect);
		}
		if (m_Icon)
		{
			URect drawRect = m_IconRect;
			sm_UiRender->DrawSkin(m_Icon,m_IconIndex,drawRect.Offset(offset));
		}
		//URect ClipRect;
		UControl * root = GetRoot();
		if (root)
		{
			RenderChildWindow(offset, updateRect);
		}
	}
	virtual BOOL OnEscape()
	{
		if (!UDialog::OnEscape())
		{
			sm_System->GetCurFrame()->SetFocusControl();
			mpBuyNum->Clear();
			SetVisible(FALSE);
		}
		return TRUE;
	}
	virtual void OnSize(const UPoint& NewSize)
	{
		m_IconRect.TransformRect(UPoint(205,126),NewSize);
	}
	void OnClickBuy()
	{
		char buf[256];
		int cnt = 1;
		if(mpBuyNum->GetText(buf, 256)){
			cnt = atoi(buf);
		}
		if(!cnt) cnt = 1;

		NIASSERT(m_BuyTypeIndex < 5);
		uint32 realprice = m_ItemInfo.buyprice[m_BuyTypeIndex] * m_dicount / 100;
		if (realprice == 0)
			realprice = 1;


		if(ObjectMgr && ObjectMgr->GetLocalPlayer())
		{
			unsigned int uiYuanBao = ObjectMgr->GetLocalPlayer()->GetYuanBao();
			if( uiYuanBao < realprice * cnt)
			{
				UMessageBox::MsgBox_s(_TRAN("您的帐户余额不足，无法完成购买"));
				return;
			}
		}

		std::string strRet = _TRAN(N_NOTICE_Z108,  _I2A(realprice * cnt).c_str());
		UMessageBox::MsgBox(strRet.c_str(),(BoxFunction*)&BuyItem, (BoxFunction*)&cancle);

		g_BuyItemId = m_ItemInfo.id;
		g_BuyItemType = m_ItemInfo.buytype[m_BuyTypeIndex];
		g_BuyItemCnt = cnt;

		mpBuyNum->Clear();
		sm_System->GetCurFrame()->SetFocusControl();
		SetVisible( FALSE );
	};
	void OnClickCancel()
	{
		mpBuyNum->Clear();
		sm_System->GetCurFrame()->SetFocusControl();
		SetVisible(FALSE);
	};
	void OnChangeBuyType()
	{
		for (int i = 0 ; i < 4 ; i++)
		{
			if(m_CheckBuyTypeBtn[i]->IsChecked())
			{
				m_BuyTypeIndex = i;
			}
		}
	};
	void SetIcon(ui32 entry)
	{
		std::string str ;
		ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(entry);
		if(ItemInfo)
		{
			str = ItemInfo->C_icon;	
		}else
		{
			UTRACE("get ItemInfo failed!");
			return;
		}
		if (str.length())
		{	
			int pos = str.find(";");
			std::string skinstr = str.substr(0,pos);
			std::string strIndex = str.substr(pos+1,str.length() -1);

			m_Icon = UControl::sm_System->LoadSkin(skinstr.c_str());
			m_IconIndex = atoi(strIndex.c_str());
		}
		if (m_Icon == NULL)
		{
			UTRACE("load Icon  failed!Change Default instead!");
			m_Icon = UControl::sm_System->LoadSkin("Icon\\ItemIcon\\DefaultItemIcon.skin");
			m_IconIndex = 0;
		}
	}
private:
	MSG_S2C::stShopItem m_ItemInfo;
	UINT         m_BuyTypeIndex;
	UStaticText * m_Name;
	UStaticText * m_level;
	UButton*  m_CheckBuyTypeBtn[5];
	UStaticText* m_BuyTypeText[5];
	UEdit* mpBuyNum;
	USkinPtr      m_Icon;
	UINT          m_IconIndex;
	URect         m_IconRect;
	uint32			m_dicount;
};
UIMP_CLASS(UPay,UDialog)
UBEGIN_MESSAGE_MAP(UPay,UDialog)
UON_BN_CLICKED(6 , &UPay::OnClickBuy)
UON_BN_CLICKED(7 , &UPay::OnClickCancel)
UON_BN_CLICKED(8 , &UPay::OnChangeBuyType)
UON_BN_CLICKED(9 , &UPay::OnChangeBuyType)
UON_BN_CLICKED(10 , &UPay::OnChangeBuyType)
UON_BN_CLICKED(11 , &UPay::OnChangeBuyType)
UEND_MESSAGE_MAP()
void UPay::StaticInit(){}
//////////////////////////////////////////////////////////////////////////////
//ShopSystem
//////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(ShopSystem,UDialog);
UBEGIN_MESSAGE_MAP(ShopSystem,UDialog)
UON_BN_CLICKED(0 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(1 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(2 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(3 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(4 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(5 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(6 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(7 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(8 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(9 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(10 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(11 , &ShopSystem::OnChangeClass)
UON_BN_CLICKED(12 , &ShopSystem::OnButtonBuy)
UON_BN_CLICKED(13 , &ShopSystem::OnButtonGift)
UON_BN_CLICKED(14 , &ShopSystem::OnButtonAddToCategory)
UON_BN_CLICKED(15 , &ShopSystem::OnPrevPage)
UON_BN_CLICKED(16 , &ShopSystem::OnNextPage)
UON_BN_CLICKED(17 , &ShopSystem::OnChongZhi)
UEND_MESSAGE_MAP()
void ShopSystem::StaticInit()
{

}
ShopSystem::ShopSystem()
{
	m_ItemList.resize(0);
	m_CuritemList.resize(0);
	m_category = 0; 
	m_CurChooseId = 0 ;
	m_CurPage = 0;
	m_LastChooseItem = NULL;
	m_YuanBao = NULL;
}
ShopSystem::~ShopSystem()
{
	m_ItemList.clear();
	m_CuritemList.clear();
	m_CurChooseId = 0;
	m_CurPage = 0;
	m_LastChooseItem = NULL;
}
BOOL ShopSystem::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	UScrollText  * pScrollText = UDynamicCast(UScrollText,GetChildByID(18));
	if (pScrollText)
	{
		pScrollText->AddScrollText(0, _TRAN("震撼内测 天下群雄齐聚首！！"));
		pScrollText->AddScrollText(1, _TRAN("与《君·天下Online》一起俯瞰众生，君临天下！！"));
		pScrollText->AddScrollText(2, _TRAN("《君·天下Online》火爆内测进行中……"));
	}
	UBitmapButton * pBitMapButton = UDynamicCast(UBitmapButton,GetChildByID(0));
	if (pBitMapButton)
	{
		pBitMapButton->SetCheck(FALSE);
	}
	m_bCanDrag = FALSE;

	UControl* pControl = GetChildByID(13);
	if(pControl)
		pControl->SetActive(false);

	pControl = GetChildByID(14);
	if(pControl)
		pControl->SetActive(false);

	if(m_YuanBao == NULL)
	{
		m_YuanBao = UDynamicCast(UMoneyText, GetChildByID(33));
		if(!m_YuanBao)
			return FALSE;
	}
	return TRUE;
}
void ShopSystem::OnDestroy()
{
	m_CurPage = 0;
	return UDialog::OnDestroy();
}
void ShopSystem::OnTimer(float fDeltaTime)
{	
	UDialog::OnTimer(fDeltaTime);
}
void ShopSystem::OnRender(const UPoint& offset,const URect &updateRect)
{
	UDialog::OnRender(offset,updateRect);
}
void ShopSystem::OnClose()
{
	//UInGame* pInGame = UInGame::Get();
	//if (pInGame)
	//{
	//	UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	//	if (pGameBar)
	//	{
	//		UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONSHOP);
	//		if (pBtn)
	//		{
	//			pBtn->SetCheck(TRUE);
	//		}
	//	}
	//}
	SetVisible(FALSE);
	SetChoose(0,FALSE);
}
BOOL ShopSystem::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}
void ShopSystem::OnMouseMove(const UPoint& position, UINT flags)
{
	UControl::OnMouseMove(position,flags);
}
void ShopSystem::OnPrevPage()
{	
	SetChoose(0,FALSE);
	if (m_CurPage > 0)
	{
		m_CurPage--;
		SetCurPageItem();
	}
}
void ShopSystem::OnNextPage()
{	
	SetChoose(0,FALSE);
	UINT MaxPage = (m_CuritemList.size() / PAGE_ITEM_COUNT) + ((m_CuritemList.size() % PAGE_ITEM_COUNT)?1:0);
	if (MaxPage && m_CurPage < MaxPage - 1 )
	{	
		m_CurPage++;
		SetCurPageItem();
	}
}

const MSG_S2C::stShopItem* FindRealCategory(const std::vector<MSG_S2C::stShopItem>& m_ItemList, uint32 itementry, uint32 itemcount)
{
	for (std::vector<MSG_S2C::stShopItem>::const_iterator it = m_ItemList.begin(); it != m_ItemList.end() ; ++it)
	{
		const MSG_S2C::stShopItem& shopItem = *it;
		if (shopItem.category != 0 && shopItem.itementry == itementry && shopItem.itemcnt == itemcount)
		{
			return &shopItem;
		}
	}
	return NULL;
}

void ShopSystem::SetCurPageItem()
{
	ShopItemExplain * pItemExplain = NULL;
	int Index = m_CurPage * PAGE_ITEM_COUNT ;
	float discount = FindDisCount(m_category);
	for (int i = 0 ;i < PAGE_ITEM_COUNT ;i++)
	{
		pItemExplain = UDynamicCast(ShopItemExplain,GetChildByID( i + 19 ));
		if (pItemExplain)
		{
			if (m_CuritemList.size())
			{
				if (Index == m_CuritemList.size())
				{
					pItemExplain->Clear();
				} 
				else
				{
					if (m_category == 0)
					{
						const MSG_S2C::stShopItem* pitem = FindRealCategory(m_ItemList, m_CuritemList[Index].itementry, m_CuritemList[Index].itemcnt);
						if (pitem)
						{
							discount = FindDisCount(pitem->category);
						}
					}
					pItemExplain->SetActivityDiscount(discount);
					pItemExplain->SetItem(m_CuritemList[Index++],i);
				}
			}
			else
			{
				pItemExplain->Clear();
			}
		}
	}
	UINT MaxPage = (m_CuritemList.size() / PAGE_ITEM_COUNT) + ((m_CuritemList.size() % PAGE_ITEM_COUNT)?1:0);
	UStaticText * pPageId = UDynamicCast(UStaticText,GetChildByID(100));
	if (pPageId)
	{
		char buf[1024];
		sprintf_s(buf,1024,"%d/%d",m_CurPage + 1,MaxPage);
		pPageId->SetText(buf);
	}
}
void ShopSystem::ParseItemList(const std::vector<MSG_S2C::stShopItem> & itemList, const std::vector<MSG_S2C::stShopHotItem>& hotItemList, const std::vector<MSG_S2C::stShopActivity>& Activitylist)
{
	m_ItemList = itemList;
	m_HotItemList = hotItemList;
	m_ActivityList = Activitylist;
	GetCurItemList();
	GetHotItemList();
}
void ShopSystem::ParseActivityUpdate(uint8 category, uint32 discount)
{
	for (unsigned int ui = 0 ; ui < m_ActivityList.size() ; ++ui)
	{
		if(m_ActivityList[ui].category == category)
		{
			m_ActivityList[ui].discount = discount;
			GetCurItemList();
			GetHotItemList();
			return;
		}
	}
	MSG_S2C::stShopActivity temp;
	temp.category = category;
	temp.discount = discount;
	m_ActivityList.push_back(temp);
	GetCurItemList();
	GetHotItemList();
}

void ShopSystem::GetCurItemList()
{
	if (!m_ItemList.size())
		return;

	m_CuritemList.clear();
	std::vector<MSG_S2C::stShopItem>::const_iterator it = m_ItemList.begin();
	while (it != m_ItemList.end())
	{
		const MSG_S2C::stShopItem & ShopItem = *it++;
		if (ShopItem.category == m_category)
		{
			m_CuritemList.push_back(ShopItem);
		}
	}

	SetCurPageItem();
}
void ShopSystem::GetHotItemList()
{
	if (!m_ItemList.size()){
		return;
	}
	if (!m_HotItemList.size()){
		return;
	}
	ShopHotItemExplain* pHotItemExplain;
	unsigned int size = m_HotItemList.size() > 5 ? 5 : m_HotItemList.size();
	for (unsigned int ui = 0 ; ui < size ; ui++)
	{
		pHotItemExplain = UDynamicCast(ShopHotItemExplain, GetChildByID( ui + 27 ));
		const MSG_S2C::stShopItem* ShopItem = NULL;
		std::vector<MSG_S2C::stShopItem>::const_iterator it = m_ItemList.begin();
		while (it != m_ItemList.end())
		{
			if (it->id == m_HotItemList[ui].id)
			{
				ShopItem = &*it;
			}
			++it;
		}
		if (pHotItemExplain && ShopItem)
		{
			ActionDataInfo ItemDataInfo;
			ItemDataInfo.entry = ShopItem->itementry;
			ItemDataInfo.Guid = ShopItem->id;
			ItemDataInfo.num = ShopItem->itemcnt;
			ItemDataInfo.pos = ui;
			ItemDataInfo.type = UItemSystem::ITEM_DATA_FLAG;
			pHotItemExplain->SetItem(ItemDataInfo);
		}
	}
}

void ShopSystem::OnChangeClass()
{
	m_CurPage = 0;
	SetChoose(0,FALSE);
	for (int i = 0 ; i < 12 ; i++)
	{
		UBitmapButton * pBitMapButton = UDynamicCast(UBitmapButton,GetChildByID(i));
		if (pBitMapButton && pBitMapButton->IsChecked())
		{
			m_category = i;
			GetCurItemList();	
			return;
		}
	}
}
void ShopSystem::SetChoose(int id , BOOL bChoose)
{
	ShopItemExplain * pItemExplain = UDynamicCast(ShopItemExplain,GetChildByID( id + 19 ));
	if (pItemExplain)
	{
		if (bChoose)
		{		
			if (m_LastChooseItem == NULL)
			{
				m_LastChooseItem = pItemExplain;
				m_LastChooseItem->SetChoose(TRUE);
			}
			else
			{
				m_LastChooseItem->SetChoose(FALSE);
				m_LastChooseItem = pItemExplain;
				m_LastChooseItem->SetChoose(TRUE);
			}
		}
		else
		{
			if (m_LastChooseItem)
			{
				m_LastChooseItem->SetChoose(FALSE);
				m_LastChooseItem = NULL;
			}
		}
	}
	UButton * pBtn = UDynamicCast(UButton,GetChildByID(12));
	if (pBtn)
	{
		pBtn->SetActive(bChoose);
	}
}
void ShopSystem::OnButtonBuy()
{
	if (m_LastChooseItem)
	{
		m_LastChooseItem->Buy();
		//int Id = m_LastChooseItem->GetId() - 19;
		//BuyItem(Id);
	}
}
void ShopSystem::OnButtonGift()
{
	//PacketBuilder->SendShopBuyMsg(uint32 id,uint8 buyType,const char * gifttoName = NULL);
}
void ShopSystem::OnButtonAddToCategory()
{
	//PacketBuilder->SendShopAddtocategoryMsg(uint32 id,uint8 buyType);
}
void ShopSystem::BuyItem(int id, int selIndex)
{
	UPay * pPay = INGAMEGETFRAME(UPay,FRAME_IG_SHOPPAYDLG);
	if (!pPay) return;
	std::vector<MSG_S2C::stShopItem>::iterator it = m_ItemList.begin();
	while (it != m_ItemList.end())
	{
		MSG_S2C::stShopItem shopItem = *it++;
		if (shopItem.id == id)
		{
			uint32 realCategory = 0;
			if (shopItem.category == 0)
			{
				const MSG_S2C::stShopItem* pItem = FindRealCategory(m_ItemList, shopItem.itementry, shopItem.itemcnt);
				if(pItem)
				{
					shopItem = *pItem;
					realCategory= shopItem.category;
				}
				if (!realCategory) return;
			}
			pPay->SetItemData(shopItem, selIndex, shopItem.itemcnt, FindDisCount(shopItem.category));
		}
	}
}
#undef PAGE_ITEM_COUNT

void ShopSystem::ShowYuanBao(ui32 uiYuanBao)
{
	m_YuanBao->SetMoney(uiYuanBao,TRUE);
}

uint32 ShopSystem::FindDisCount(uint32 Category)
{
	if (!Category)
		return 100;

	for (unsigned int ui = 0 ; ui < m_ActivityList.size() ; ++ui)
	{
		if(m_ActivityList[ui].category == Category)
		{
			return m_ActivityList[ui].discount;
		}
	}

	return 100;
}

#include "shellapi.h"
void ShopSystem::OnChongZhi()
{
	//内地运营的时候需要修改这里的地址
	ShellExecute(NULL, "open", "http://www.jtxol.com", NULL, NULL, SW_SHOWNORMAL);
}