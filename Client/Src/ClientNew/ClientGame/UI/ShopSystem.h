#ifndef SHOPSYSTEM_H
#define SHOPSYSTEM_H
//--------------------------------------
//道具商城
//--------------------------------------
#include "UInc.h"
#include "../../../../../SDBase/Protocol/S2C_Auction.h"

class ShopSystem : public UDialog
{
	UDEC_CLASS(ShopSystem);
	UDEC_MESSAGEMAP();
public:	
	ShopSystem();
	~ShopSystem();
public:
	void ParseItemList(const std::vector<MSG_S2C::stShopItem> & itemList, const std::vector<MSG_S2C::stShopHotItem>& hotItemList, const std::vector<MSG_S2C::stShopActivity>& Activitylist);
	void ParseActivityUpdate(uint8 category, uint32 discount);
	void SetChoose(int id , BOOL bChoose);
	void BuyItem(int id, int selIndex);
	void ShowYuanBao(ui32 uiYuanBao);	//显示元宝
	uint32 FindDisCount(uint32 Category);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);

	virtual void OnMouseMove(const UPoint& position, UINT flags);

	virtual void OnClose();
	virtual BOOL OnEscape();
protected:
	void OnPrevPage();
	void OnNextPage();
	void OnChangeClass();
	void OnButtonBuy();
	void OnButtonGift();
	void OnButtonAddToCategory();
    void OnChongZhi();
private:
	void SetCurPageItem();
	void GetCurItemList();
	void GetHotItemList();
	class ShopItemExplain * m_LastChooseItem;
	std::vector<MSG_S2C::stShopItem> m_ItemList;
	std::vector<MSG_S2C::stShopHotItem> m_HotItemList;
	std::vector<MSG_S2C::stShopActivity> m_ActivityList;
	std::vector<MSG_S2C::stShopItem> m_CuritemList;
	UINT m_CurChooseId;
	UINT m_CurPage;
	uint8 m_category;
	class UMoneyText* m_YuanBao;
};
#endif