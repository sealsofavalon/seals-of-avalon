#ifndef __SINGLETON_H__
#define __SINGLETON_H__

#define SINGLETON(class_name) \
friend class Singleton< class_name >; \
private:\
class_name();\
~class_name();\
class_name(const class_name&);\
class_name& operator = (const class_name&);\

template<typename T>
class Singleton
{
protected:
	Singleton(){}
	virtual ~Singleton(){}
	Singleton(const Singleton<T>&){}
	Singleton<T>& operator=(const Singleton<T>&){}
public:
	//static T& GetSingleton()
	//{
	//	static T __singleton;
	//	return __singleton;
	//}

	static T* GetSingleton()
	{
		static T __singleton;
		return &__singleton;
	}
};

#endif