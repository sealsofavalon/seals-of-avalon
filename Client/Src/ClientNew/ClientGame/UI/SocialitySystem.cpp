#include "stdafx.h"
#include "SocialitySystem.h"
#include "UIIndex.h"
#include "Network/PacketBuilder.h"
#include "UMessageBox.h"
#include "ItemManager.h"
#include "UIItemSystem.h"
#include "UISystem.h"
#include "UIPlayerEquipment.h"
#include "UIGamePlay.h"
#include "Utils/ChatFileterDB.h"
UIMP_CLASS(UIAddMemberEdit, UDialog);
//事件响应
UBEGIN_MESSAGE_MAP(UIAddMemberEdit,UDialog)
UON_BN_CLICKED(1, &UIAddMemberEdit::OnClickOk)
UON_BN_CLICKED(2, &UIAddMemberEdit::OnClickCancel)
UON_UEDIT_UPDATE(0,&UIAddMemberEdit::OnClickOk)
UEND_MESSAGE_MAP()

void UIAddMemberEdit::StaticInit()
{

}

UIAddMemberEdit::UIAddMemberEdit()
{
	m_pkEdit = NULL;
	m_Type = 0;
	m_bStartTime = FALSE;
	m_ftime = 0.0f;
}

UIAddMemberEdit::~UIAddMemberEdit()
{

}
void UIAddMemberEdit::OnTimer(float fDeltaTime)
{
	UDialog::OnTimer(fDeltaTime);
	if (m_bStartTime)
	{
		m_ftime += fDeltaTime;
		if (m_ftime > 30.f)
		{
			m_bStartTime = FALSE;
			m_ftime = 0.0f;
		}
	}
}
BOOL UIAddMemberEdit::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}

	m_pkEdit = (UEdit*)GetChildByID(0);
	if(m_pkEdit == NULL)
	{
		return FALSE;
	}
	m_pkEdit->SetMaxLength(28);
	m_pkEdit->Clear();
	m_bCanDrag = FALSE;
	m_bShowClose = FALSE;
	return TRUE;
}

void UIAddMemberEdit::OnDestroy()
{
	UDialog::OnDestroy();
}
void UIAddMemberEdit::OnAddMember(int type)
{
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(3));
	if(!pText)
		return;
	if (m_Type == 4 && m_Type != type&& SYState()->UI->GetUItemSystem()->IsUsedAppearItem())
	{
		SYState()->UI->GetUItemSystem()->CancelUseSpecialItem();
	}
	m_Type = type;
	if(type == 0)//添加好友
	{
		pText->SetText(_TRAN("请输入添加好友昵称："));
		SetVisible(TRUE);
	}
	else if(type == 1)//添加部族成员
	{
		pText->SetText(_TRAN("请输入添加族员昵称："));
		SetVisible(TRUE);
	}else if (type == 2)
	{
		pText->SetText(_TRAN("请输入添加团员昵称："));
		SetVisible(TRUE);
	}else if (type == 3)
	{
		pText->SetText(_TRAN("请输入激活码"));
		SetVisible(TRUE);
	}else if (type == 4)
	{
		pText->SetText(_TRAN("请输入传送玩家名字"));
		SetVisible(TRUE);
	}else if (type == 5)
	{
		pText->SetText(_TRAN("输入加入黑名单玩家："));
		SetVisible(TRUE);
	}else if (type == 6)
	{
		pText->SetText(_TRAN("输入修改宠物名称："));
		SetVisible(TRUE);
	}
}

void UIAddMemberEdit::SetVisible(BOOL value)
{
	UDialog::SetVisible(value);
	if (value)
		m_pkEdit->SetFocusControl();
}

void UIAddMemberEdit::OnClose()
{
	sm_System->GetCurFrame()->SetFocusControl();
	SetVisible(FALSE);
	m_pkEdit->Clear();
	if (m_Type == 4)
	{
		SYState()->UI->GetUItemSystem()->CancelUseSpecialItem();
	}
}

void UIAddMemberEdit::OnClickOk()
{
	char szName[56];
	int ret = m_pkEdit->GetText(szName, 28);
	szName[ret] = 0;
	string strName = szName;
	if(strName.size() && SocialitySys)
	{
		if(m_Type == 0)
			SocialitySys->GetFriendSysPtr()->SendNetMessageInviteMem(strName.c_str());
		else if(m_Type == 1)
			SocialitySys->GetGuildSysPtr()->SendMsgInviteMember(strName.c_str());
		else if (m_Type == 2)
		{
			SocialitySys->GetTeamSysPtr()->SendAddMember(strName.c_str());
		}else if (m_Type == 3)
		{
			if (!m_bStartTime)
			{
				PacketBuilder->SendSerialForGift(strName);
				m_bStartTime = TRUE;
			}else
			{
				UMessageBox::MsgBox_s(_TRAN("您发送的太频繁了"), NULL , 10.0f);
			}
			
		}else if (m_Type == 4)
		{
			SYState()->UI->GetUItemSystem()->DoUseSpecialItem(strName, 0);
		}else if (m_Type == 5)
		{
			SocialitySys->GetBlackListSysPtr()->SendAddIgnoreList(strName);
		}else if (m_Type == 6)
		{
			UPetDlg* PetDlg = INGAMEGETFRAME(UPetDlg, FRAME_IG_PLAYERPETDLG);
			if (PetDlg)
			{
				ui64 guid = PetDlg->GetPetGUID();
				if (guid)
				{
					if( strchr(strName.c_str(), ' ')
						|| strchr(strName.c_str(), '\t')
						|| strchr(strName.c_str(), '\r')
						|| strchr(strName.c_str(), '\n'))
					{
						UMessageBox::MsgBox_s( _TRAN("对不起，您的输入中含有空白字符！") );
						m_pkEdit->SetFocusControl();
						return;
					}
					else if (g_pkChatFilterDB->GetFilterChatMsg(strName))
					{
						UMessageBox::MsgBox_s( _TRAN("对不起，您的输入中带有非法字符！") );
						m_pkEdit->SetFocusControl();
						return;
					}
					PacketBuilder->SendPetReName(guid, strName.c_str());
				}
			}
		}
	}
	OnClose();
	m_pkEdit->Clear();
}

void UIAddMemberEdit::OnClickCancel()
{
	OnClose();
}
BOOL UIAddMemberEdit::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
GuildSys* SocialitySystem::GetGuildSysPtr()
{
	if (!mbCreate)
	{
		return NULL;
	}
	return mSocialitySystemPtr.pGuildSys;
}
FriendSys* SocialitySystem::GetFriendSysPtr()
{
	if (!mbCreate)
	{
		return NULL;
	}
	return mSocialitySystemPtr.pFriendSys;
}
OnlinePlayerQuerySys* SocialitySystem::GetOLPlayerQuerySysPtr()
{
	if (!mbCreate)
	{
		return NULL;
	}
	return mSocialitySystemPtr.pOnlinePlayerQuerySys;
}
Teamsys* SocialitySystem::GetTeamSysPtr()
{
	if (!mbCreate)
	{
		return NULL;
	}

	return mSocialitySystemPtr.pTeamSys;
}

ApplyGroupSystem* SocialitySystem::GetApplyGroupSysPtr()
{
	if (!mbCreate)
	{
		return NULL;
	}
	return mSocialitySystemPtr.pApplyGroupSys;
}
BlackListSys* SocialitySystem::GetBlackListSysPtr()
{
	if (!mbCreate)
	{
		return NULL;
	}
	return mSocialitySystemPtr.pBlackListSys;
}

void SocialitySystem::GuildSetCreate(ui32 guildId)
{
	if (!mbCreate)
	{
		return;
	}
	mSocialitySystemPtr.pFriendSys->SetGuildActive(true);
	mSocialitySystemPtr.pOnlinePlayerQuerySys->SetGuildActive(true);
	mSocialitySystemPtr.pTeamSys->SetGuildActive(true);
	mSocialitySystemPtr.pApplyGroupSys->SetGuildActive(true);
	mSocialitySystemPtr.pBlackListSys->SetGuildActive(true);
	mSocialitySystemPtr.pGuildSys->SendMsgGuildQuery();
	mSocialitySystemPtr.pGuildSys->SendMsgGuildRoster();
	mSocialitySystemPtr.pGuildSys->SendMsgQureyCastleState();
}
void SocialitySystem::GuildSetEmpty()
{
	if (!mbCreate)
	{
		return;
	}
	mSocialitySystemPtr.pFriendSys->SetGuildActive(false);
	mSocialitySystemPtr.pOnlinePlayerQuerySys->SetGuildActive(false);
	mSocialitySystemPtr.pTeamSys->SetGuildActive(false);
	mSocialitySystemPtr.pApplyGroupSys->SetGuildActive(false);
	mSocialitySystemPtr.pBlackListSys->SetGuildActive(false);
	mSocialitySystemPtr.pGuildSys->ParseMsgGuildDisBand();
}
SocialitySystem::SocialitySystem():mbCreate(false),m_iShowFrame(1),mBKGtexture(NULL)
{

}
SocialitySystem::~SocialitySystem()
{
	SocialitySys->DestoryFrame();
}
BOOL SocialitySystem::CreateFrame(UControl * Ctrl)
{
	BOOL bSuccess = TRUE;
	//部族
	mSocialitySystemPtr.pGuildSys = new GuildSys;
	if (!mSocialitySystemPtr.pGuildSys)
	{
		return FALSE;
	}
	bSuccess &= mSocialitySystemPtr.pGuildSys->CreateFrame(Ctrl);
	//好友
	mSocialitySystemPtr.pFriendSys = new FriendSys;
	if (!mSocialitySystemPtr.pFriendSys)
	{
		return FALSE;
	}
	bSuccess &= mSocialitySystemPtr.pFriendSys->CreateFrame(Ctrl);
	//在线查询
	mSocialitySystemPtr.pOnlinePlayerQuerySys = new OnlinePlayerQuerySys;
	if (!mSocialitySystemPtr.pOnlinePlayerQuerySys)
	{
		return FALSE;
	}
	bSuccess &= mSocialitySystemPtr.pOnlinePlayerQuerySys->CreateFrame(Ctrl);
	//团队
	mSocialitySystemPtr.pTeamSys = new Teamsys;
	if (!mSocialitySystemPtr.pTeamSys)
	{
		return FALSE;
	}
	bSuccess &= mSocialitySystemPtr.pTeamSys->CreateFrame(Ctrl);

	mSocialitySystemPtr.pApplyGroupSys = new ApplyGroupSystem;
	if (!mSocialitySystemPtr.pApplyGroupSys)
	{
		return FALSE;
	}
	bSuccess &= mSocialitySystemPtr.pApplyGroupSys->CreateFrame(Ctrl);

	mSocialitySystemPtr.pBlackListSys = new BlackListSys;
	if (!mSocialitySystemPtr.pBlackListSys)
	{
		return FALSE;
	}
	bSuccess &= mSocialitySystemPtr.pBlackListSys->CreateFrame(Ctrl);

	if(mBKGtexture == NULL)
	{
		mBKGtexture = UControl::sm_UiRender->LoadTexture("DATA\\UI\\SocialitySystem\\bChoos_Bkg.png");
		if(!mBKGtexture)
			return FALSE;
	}
	UControl* NewCtrl = NULL;
	if (NewCtrl == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\AddMemberEdit.udg");
		if (NewCtrl)
		{
			Ctrl->AddChild(NewCtrl);
			NewCtrl->SetId(FRAME_IG_ADDMEMBERDLG);
		}
		else
		{
			bSuccess &= FALSE;
		}
	}

	mbCreate = bSuccess?true:false;	

	bSuccess &= mSocialitySystemPtr.pGuildSys->GetDBNpcList()?TRUE:FALSE;
	return bSuccess;
}
void SocialitySystem::DestoryFrame()
{
	//部族
	if (mSocialitySystemPtr.pGuildSys)
	{
		mSocialitySystemPtr.pGuildSys->DestoryFrame();
		delete mSocialitySystemPtr.pGuildSys;
		mSocialitySystemPtr.pGuildSys = NULL;
	}
	//好友
	if (mSocialitySystemPtr.pFriendSys)
	{
		mSocialitySystemPtr.pFriendSys->DestoryFrame();
		delete mSocialitySystemPtr.pFriendSys;
		mSocialitySystemPtr.pFriendSys = NULL;
	}
	//在线查询
	if (mSocialitySystemPtr.pOnlinePlayerQuerySys)
	{
		mSocialitySystemPtr.pOnlinePlayerQuerySys->DestoryFrame();
		delete mSocialitySystemPtr.pOnlinePlayerQuerySys;
		mSocialitySystemPtr.pOnlinePlayerQuerySys = NULL;
	}

	if (mSocialitySystemPtr.pTeamSys)
	{
		mSocialitySystemPtr.pTeamSys->DestoryFrame();
		delete mSocialitySystemPtr.pTeamSys;
		mSocialitySystemPtr.pTeamSys = NULL;
	}
	if (mSocialitySystemPtr.pApplyGroupSys)
	{
		mSocialitySystemPtr.pApplyGroupSys->DestoryFrame();
		delete mSocialitySystemPtr.pApplyGroupSys;
		mSocialitySystemPtr.pApplyGroupSys = NULL;
	}
	if (mSocialitySystemPtr.pBlackListSys)
	{
		mSocialitySystemPtr.pBlackListSys->DestoryFrame();
		delete mSocialitySystemPtr.pBlackListSys;
		mSocialitySystemPtr.pBlackListSys = NULL;
	}
	mbCreate = false;
}
void SocialitySystem::ShowFrame(int iFrame)
{
	if (iFrame == 0)
	{
		GetGuildSysPtr()->ShowFrame();
		GetFriendSysPtr()->HideFrame();
		GetOLPlayerQuerySysPtr()->HideFrame();
		GetTeamSysPtr()->HideFrame();
		GetApplyGroupSysPtr()->HideFrame();
		GetBlackListSysPtr()->HideFrame();
		m_iShowFrame = 0;
	}
	else if (iFrame == 1)
	{
		GetFriendSysPtr()->ShowFrame();
		GetGuildSysPtr()->HideFrame();
		GetOLPlayerQuerySysPtr()->HideFrame();
		GetTeamSysPtr()->HideFrame();
		GetApplyGroupSysPtr()->HideFrame();
		GetBlackListSysPtr()->HideFrame();
		m_iShowFrame = 1;
	}
	else if (iFrame == 2)
	{
		GetOLPlayerQuerySysPtr()->ShowFrame();
		GetFriendSysPtr()->HideFrame();
		GetGuildSysPtr()->HideFrame();
		GetTeamSysPtr()->HideFrame();
		GetApplyGroupSysPtr()->HideFrame();
		GetBlackListSysPtr()->HideFrame();
		m_iShowFrame = 2;
	}else if (iFrame == 3)
	{
		GetOLPlayerQuerySysPtr()->HideFrame();
		GetFriendSysPtr()->HideFrame();
		GetGuildSysPtr()->HideFrame();
		GetTeamSysPtr()->ShowFrame();
		GetApplyGroupSysPtr()->HideFrame();
		GetBlackListSysPtr()->HideFrame();
		m_iShowFrame = 3;
	}else if (iFrame == 4)
	{
		GetOLPlayerQuerySysPtr()->HideFrame();
		GetFriendSysPtr()->HideFrame();
		GetGuildSysPtr()->HideFrame();
		GetTeamSysPtr()->HideFrame();
		GetApplyGroupSysPtr()->ShowFrame();
		GetBlackListSysPtr()->HideFrame();
		m_iShowFrame = 4;
	}else if (iFrame == 5)
	{
		GetOLPlayerQuerySysPtr()->HideFrame();
		GetFriendSysPtr()->HideFrame();
		GetGuildSysPtr()->HideFrame();
		GetTeamSysPtr()->HideFrame();
		GetApplyGroupSysPtr()->HideFrame();
		GetBlackListSysPtr()->ShowFrame();
		m_iShowFrame = 5;
	}
}
void SocialitySystem::HideFrame(int iFrame)
{
	if (iFrame == 0)
	{
		GetGuildSysPtr()->HideFrame();
	}else if (iFrame == 1)
	{
		GetFriendSysPtr()->HideFrame();
	}else if (iFrame == 2)
	{
		GetOLPlayerQuerySysPtr()->HideFrame();
	}else if (iFrame == 3)
	{
		GetTeamSysPtr()->HideFrame();
	}else if (iFrame == 4)
	{
		GetApplyGroupSysPtr()->HideFrame();
	}else if (iFrame == 5)
	{
		GetBlackListSysPtr()->HideFrame();
	}
}
void SocialitySystem::Show()
{
	ShowFrame(m_iShowFrame);
}
void SocialitySystem::HideAll()
{
	GetGuildSysPtr()->HideFrame();
	GetFriendSysPtr()->HideFrame();
	GetOLPlayerQuerySysPtr()->HideFrame();
	GetTeamSysPtr()->HideFrame();
	GetApplyGroupSysPtr()->HideFrame();
	GetBlackListSysPtr()->HideFrame();
	m_iShowFrame = 1;
}