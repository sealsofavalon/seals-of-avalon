#ifndef SOCIALITYSYSYTEM_H
#define SOCIALITYSYSYTEM_H
#include "UInc.h"
#include "GuildSys.h"
#include "UFriendsDlg.h"
#include "UOLPlayerQuery.h"
#include "UTeamSYS.h"
#include "Singleton.h"

#define SocialitySys SocialitySystem::GetSingleton()
struct SocialitySystemPtr
{
	GuildSys* pGuildSys;
	FriendSys* pFriendSys;
	OnlinePlayerQuerySys* pOnlinePlayerQuerySys;
	Teamsys* pTeamSys;
	ApplyGroupSystem* pApplyGroupSys;
	BlackListSys* pBlackListSys;
};
class UIAddMemberEdit : public UDialog
{
	UDEC_CLASS(UIAddMemberEdit);
	UDEC_MESSAGEMAP();
public:
	UIAddMemberEdit();
	~UIAddMemberEdit();

	void OnAddMember(int type);

	virtual void SetVisible(BOOL value);

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	
	virtual void OnClose();
	virtual BOOL OnEscape();
	virtual void OnTimer(float fDeltaTime);

	void OnClickOk();
	void OnClickCancel();

private:
	UEdit* m_pkEdit;
	int m_Type;
	float m_ftime ;
	BOOL m_bStartTime ;
};
class SocialitySystem : public Singleton<SocialitySystem>
{
	SINGLETON(SocialitySystem);
public:
	UTexturePtr mBKGtexture;
	int m_iShowFrame;
public:
	BOOL CreateFrame(UControl * Ctrl);
	void DestoryFrame();
	void ShowFrame(int iFrame);
	void HideFrame(int iFrame);
	void Show();
	void HideAll();

	GuildSys* GetGuildSysPtr();
	FriendSys* GetFriendSysPtr();
	OnlinePlayerQuerySys* GetOLPlayerQuerySysPtr();
	Teamsys* GetTeamSysPtr();
	ApplyGroupSystem* GetApplyGroupSysPtr();
	BlackListSys* GetBlackListSysPtr();
public:
	void GuildSetCreate(ui32 guildId);
	void GuildSetEmpty();
private:
	SocialitySystemPtr mSocialitySystemPtr;
	bool mbCreate;
};

#endif