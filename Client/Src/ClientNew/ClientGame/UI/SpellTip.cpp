#include "StdAfx.h"
#include "SpellTip.h"
#include "UISystem.h"
#include "Utils/SpellDB.h"
#include "Skill/SkillManager.h"
#include "UITipSystem.h"
#include "ObjectManager.h"
#include "LocalPlayer.h"

SpellTip::SpellTip()
{
}
SpellTip::~SpellTip()
{

}
void SpellTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count)
{
	//if (ID == mID)
	//{
	//	return;
	//}
	mEntry = ID;
	mID = guid;
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if(pkLocal && mID == 0)
	{
		mID = pkLocal->GetGUID();
	}
	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)mEntry);
	if (pSpellTemplate)
	{
		m_KeepDraw = TRUE;
		INT posx = Ctrl->GetRoot()->GetWidth() - Ctrl->GetWidth();
		INT BOTTOM = Ctrl->GetRoot()->GetHeight() - 40;
		Ctrl->SetPosition( UPoint(posx, 0) );
		Ctrl->SetBottom(BOTTOM);
		Ctrl->SetVisible(TRUE);
	}
	else
	{
		return;
	}
	if (m_Content.size() > 0)
	{
		m_Content.clear();
	}
	if (m_Type == TIP_TYPE_SPELL)
	{
		if (pSpellTemplate->GetAttribute() & ATTRIBUTES_PASSIVE)
		{
			CreateContentItem(SPELL_TIP_NAME);
			CreateContentItem(SPELL_TIP_LEVEL);
			CreateContentItem(SPELL_TIP_SKILLTYPE);
			CreateContentItem(SPELL_TIP_EXPLAIN);
			return;
		}
		if (pSpellTemplate->GetCategory() == SPELL_CATEGORY_PROFESSION_MANUFACTURE
			|| pSpellTemplate->GetCategory() == SPELL_CATEGORY_PROFESSION_GATHER
			|| pSpellTemplate->GetCategory() == SPELL_CATEGORY_PROFESSION_ENCHANTING)
		{
			CreateContentItem(SPELL_TIP_NAME);
			CreateContentItem(SPELL_TIP_LEVEL);
			CreateContentItem(SPELL_TIP_EXPLAIN);
			return;
		}
		for (int i = 0 ; i < SPELL_TIP_MAX ; i++)
		{
			if (i == SPELL_TIP_SKILLTYPE)
			{
				continue;
			}
			CreateContentItem(i);
		}

	}
	if (m_Type == TIP_TYPE_BUFF)
	{
		CreateContentItem(SPELL_TIP_NAME);
		CreateContentItem(SPELL_TIP_LEVEL);
		CreateContentItem(SPELL_TIP_EXPLAIN);
	}
}
void SpellTip::DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl , const UPoint &pos,const URect &updateRect)
{
	if (m_KeepDraw)
	{
		//渲染边框以及内部填充区域
		UDrawResizeImage(RESIZE_WANDH, pRender, TipSystem->mBkgSkin, pos, updateRect.GetSize());
		//pRender->DrawRectFill(updateRect,pCtrlStl->mFillColor);
		//pRender->DrawRect(updateRect,pCtrlStl->mBorderColor);
		//渲染文字
		int height = DrawContent(pRender,pCtrlStl,pos,updateRect);
		//设置当前提示框的高度
		if (height != Ctrl->GetHeight())
		{
			Ctrl->SetHeight(height);
			INT BOTTOM = Ctrl->GetRoot()->GetHeight() - 40;
			Ctrl->SetBottom(BOTTOM);
		}
	}
}
void SpellTip::DestoryTip()
{
	mEntry = 0;
	mID = 0;
	m_KeepDraw = FALSE;
	m_Content.clear();
}
void SpellTip::CreateContentItem(int Type)
{
	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)mEntry);
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	SpellTip::ContentItem Temp_CI;
	if (pSpellTemplate && pkLocal)
	{
		switch (Type)
		{
		case SPELL_TIP_NAME:
			{
				Temp_CI.Type = SPELL_TIP_NAME;
				Temp_CI.str.Set(pSpellTemplate->GetName().c_str());
				Temp_CI.FontFace = TipSystem->GetNameFontFace();
				Temp_CI.FontColor = 0xFF3366FF;
				m_Content.push_back(Temp_CI);
			}
			break;
		case SPELL_TIP_LEVEL:
			{
				unsigned int spelllev = pSpellTemplate->GetSpellLev();
				if (spelllev)
				{
					//char buf[1024];
					//sprintf(buf, "%d级", spelllev);

					std::string strRet = _TRAN(N_NOTICE_Z109,  _I2A(spelllev).c_str() );
					Temp_CI.Type = SPELL_TIP_LEVEL;
					Temp_CI.str.Set(strRet.c_str());
					Temp_CI.FontFace = TipSystem->GetNameFontFace();
					m_Content.push_back(Temp_CI);
				}
				else
				{
					Temp_CI.Type = SPELL_TIP_LEVEL;
					Temp_CI.str.Set(_TRAN("基础等级"));
					Temp_CI.FontFace = TipSystem->GetNameFontFace();
					m_Content.push_back(Temp_CI);
				}
			}
			break;
		case SPELL_TIP_SKILLTYPE:
			{
				Temp_CI.Type = SPELL_TIP_SKILLTYPE;
				if (pSpellTemplate->GetAttribute() & ATTRIBUTES_PASSIVE)
				{				
					Temp_CI.str.Set(_TRAN("被动技能"));
				}else{
					Temp_CI.str.Set(_TRAN("主动技能"));
				}
				m_Content.push_back(Temp_CI);
			}
			break;
		case SPELL_TIP_COLDDOWN:
			{
				char buf[1024];
				Temp_CI.Type = SPELL_TIP_COLDDOWN;
				ui32 timetime = 0;
				if(mID == pkLocal->GetGUID())
					timetime =pSpellTemplate->GetRecoverTime();
				else
					timetime = pSpellTemplate->GetTRecoverTime();
				if (timetime >= 60000)
				{
					int min = timetime/60000;
					int sec = timetime%60000;
					if (sec == 0)
					{
						std::string strRet = _TRAN(N_NOTICE_Z110, _I2A(min).c_str());
						sprintf_s(buf,1024,strRet.c_str());
					}
					else
					{
						float fsec = sec / 1000.f;
						std::string strRet = _TRAN(N_NOTICE_Z111, _I2A(min).c_str(), _F2A(fsec, 2).c_str());

						sprintf_s(buf,1024, strRet.c_str());
					}
				}
				else
				{
					float sec = timetime/1000.f;
					if (timetime == 0)
					{
						sprintf_s(buf,1024,_TRAN("无冷却"));
					}
					else
					{
						std::string strRet = _TRAN(N_NOTICE_Z112, _F2A(sec, 2).c_str());
						sprintf_s(buf,1024,strRet.c_str());
					}
				}
				Temp_CI.str.Set(buf);
				m_Content.push_back(Temp_CI);
			}
			break;
		case SPELL_TIP_COSTMANA:
			{
				Temp_CI.Type = SPELL_TIP_COSTMANA;
				unsigned int uiMPRequest = 0;
				unsigned int uiMPRequestPer = 0;

				if(mID == pkLocal->GetGUID())
				{
					pSpellTemplate->GetMPRequest(uiMPRequest);
				}else
				{
					pSpellTemplate->GetTMPRequest(uiMPRequest);
				}
				Temp_CI.str.Set(_TRAN(N_NOTICE_Z113, _I2A(uiMPRequest).c_str()).c_str());
				m_Content.push_back(Temp_CI);
			}
			break;
		case SPELL_TIP_DISTANCE:
			{
				char buf[1024];
				Temp_CI.Type = SPELL_TIP_DISTANCE;

				float fMinRange = 0.0f, fMaxRange = 0.0f;
				UINT SelectMethod = pSpellTemplate->GetSelectMethod();

				if(mID == pkLocal->GetGUID())
				{
					SpellTemplate::GetRange((SpellID)mEntry, fMinRange, fMaxRange);
				}else
				{
					SpellTemplate::GetTRange((SpellID)mEntry, fMinRange, fMaxRange);
				}
				std::string strRet = _TRAN(N_NOTICE_Z114, _I2A((int)fMaxRange).c_str());
				sprintf_s(buf,1024,strRet.c_str());
				if (((SelectMethod & TARGET_FLAG_SELF) && (SelectMethod & TARGET_FLAG_SOURCE_LOCATION))
					||(SelectMethod == TARGET_FLAG_SELF) || (SelectMethod == TARGET_FLAG_SOURCE_LOCATION))
				{
					return;
				}
				Temp_CI.str.Set(buf);
				m_Content.push_back(Temp_CI);
			}
			break;
		case SPELL_TIP_CASTTIME:
			{
				char buf[1024];
				Temp_CI.Type = SPELL_TIP_CASTTIME;
				uint32 casttime = 0;
				if(mID == pkLocal->GetGUID())
					casttime = pSpellTemplate->GetCastTime();
				else
					casttime = pSpellTemplate->GetTCastTime();
				if (casttime)
				{
					if (casttime >= 60000)
					{
						int min = casttime/60000;
						int sec = casttime%60000;
						if (sec == 0.f)
						{
							std::string strRet = _TRAN(N_NOTICE_Z115,_I2A(min).c_str());
							sprintf_s(buf, 1024, strRet.c_str());
						}
						else
						{
							float fsec = sec / 1000.f;

							std::string strRet = _TRAN(N_NOTICE_Z116, _I2A(min).c_str(), _F2A(fsec, 2).c_str());
							sprintf_s(buf, 1024, strRet.c_str());
						}
					}
					else
					{
						float sec = casttime/1000.f;
						std::string strRet = _TRAN(N_NOTICE_Z117, _F2A(sec, 2).c_str());
						sprintf_s(buf, 1024, strRet.c_str() );
					}
				}
				else
				{
					sprintf_s(buf, 1024, _TRAN("瞬发法术"));
				}
				Temp_CI.str.Set(buf);
				m_Content.push_back(Temp_CI);
			}
			break;
		case SPELL_TIP_EXPLAIN:
			{
				Temp_CI.Type = SPELL_TIP_EXPLAIN;
				std::string _tempbuf;
				if(pSpellTemplate->GetDescription(_tempbuf, mID == pkLocal->GetGUID()) && _tempbuf.length())
				{
					Temp_CI.str.Set(_tempbuf.c_str());
					m_Content.push_back(Temp_CI);
				}
			}
			break;
		};
	}
}
int SpellTip::DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect)
{
	int height = m_BoardWide;
	for (unsigned int i = 0 ; i < m_Content.size() ; i++)
	{
		switch (m_Content[i].Type)
		{
		case SPELL_TIP_NAME:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
				
				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				//height += size.y;
			}
			break;
		case SPELL_TIP_LEVEL:
			{
				//height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}

				height += size.y;
			}
			break;
		case SPELL_TIP_SKILLTYPE:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				height += size.y;
			}
			break;
		case SPELL_TIP_COLDDOWN:
			{
				//height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}

				height += size.y;
			}
			break;
		case SPELL_TIP_COSTMANA:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
						
				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				//height += size.y;
			}
			break;
		case SPELL_TIP_CASTTIME:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				height += size.y;
			}
			break;
		case SPELL_TIP_DISTANCE:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
				
				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				height += size.y;
			}
			break;
		case SPELL_TIP_EXPLAIN:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if (m_Content[i].FontFace)
				{
					size.y = UDrawStaticText(pRender,m_Content[i].FontFace,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				}
				else
				{
					size.y = UDrawStaticText(pRender,pCtrlStl->m_spFont,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				}

				height += size.y;
			}
			break;
		}
	}
	return height + m_BoardWide;
}