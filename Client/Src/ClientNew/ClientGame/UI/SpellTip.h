#ifndef SPELLTIP_H
#define SPELLTIP_H
//------------------------------------------------------
//技能显示的鼠标提示
//------------------------------------------------------
//技能显示需要什么？

//****************************
//*技能名称          技能等级*
//*技能类型				     *
//*技能耗蓝          施法距离*
//*正在冷却时间(可选)        *
//*引导时间                  *瞬发法术
//*技能说明:				 *
//*XXXXXXXXXXXXXXXXXXXXXXXXXX*
//****************************
//那么后面渲染时就要按照这个样式来进行
#include "MouseTip.h"
enum
{
	SPELL_TIP_NAME,
	SPELL_TIP_LEVEL,
	SPELL_TIP_SKILLTYPE,
	SPELL_TIP_COSTMANA,
	SPELL_TIP_COLDDOWN,
	SPELL_TIP_DISTANCE,
	SPELL_TIP_CASTTIME,
	SPELL_TIP_EXPLAIN,
	SPELL_TIP_MAX
};
class SpellTip :public MouseTip
{
	struct ContentItem  
	{
		ContentItem():Type(SPELL_TIP_MAX),FontFace(NULL),FontColor(UColor(255,255,255,255)){}
		UStringW str;
		UColor FontColor;
		UFontPtr FontFace;
		int Type;
	};
public:
	SpellTip();
	~SpellTip();
public:
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count);
	virtual void CreateContentItem(int Type);

	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect);
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect);

	virtual void DestoryTip();
private:
	vector<ContentItem> m_Content;
};
#endif