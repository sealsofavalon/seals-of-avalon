#include "StdAfx.h"
#include "SystemSetup.h"
#include "USystemSet.h"
#include "GameSystemSetupSave.h"
#include "GameSystemSetupLoad.h"
#include "GameSystemSetupMgr.h"

GameSystemSetup::GameSystemSetup()
{
	m_SysSetupMgr = NULL;
	m_SysSetupSave = NULL;
	m_SysSetupLoad = NULL;
}
GameSystemSetup::~GameSystemSetup()
{

}
BOOL GameSystemSetup::InitSystem()
{
	UControl::sm_System->UnLoadAccelertorKey();

	if (m_SysSetupMgr == NULL)
	{
		m_SysSetupMgr = new GameSystemSetupManage;
		if (!m_SysSetupMgr)
		{
			return FALSE;
		}
	}
	if (m_SysSetupSave == NULL)
	{
		m_SysSetupSave = new GameSystemSetupSave;
		if (!m_SysSetupSave)
		{
			return FALSE;
		}
	}
	if (m_SysSetupLoad == NULL)
	{
		m_SysSetupLoad = new GameSystemSetupLoad;
		if (!m_SysSetupLoad)
		{
			return FALSE;
		}
	}
	DefaultSystemSet();
	return TRUE;
}
BOOL GameSystemSetup::CreateUserDirectory()
{
	if (m_SysSetupMgr)
	{
		return m_SysSetupMgr->CreateUserDirectory();
	}
	return FALSE;
}
BOOL GameSystemSetup::RemoveUserDirectory(const char* UserName)
{
	if (m_SysSetupMgr)
	{
		return m_SysSetupMgr->RemoveUserDirectory(UserName);
	}
	return FALSE;
}
void GameSystemSetup::DefaultSystemSet()
{
	if (m_SysSetupMgr)
	{
		m_SysSetupMgr->SetDefaultSetup();
	}
}
void GameSystemSetup::SetGameSystemUserFileRoot(int Partypath, const char* path)
{
	if (m_SysSetupMgr)
	{
		m_SysSetupMgr->SetGameSystemUserFileRoot(Partypath, path);
	}
}
bool GameSystemSetup::GetGameSystemAccountFile(std::string &str)
{
	if (m_SysSetupMgr)
	{
		return m_SysSetupMgr->GetGameSystemAccountFile(str);
	}
	return false;
}
bool GameSystemSetup::GetGameSystemUserFileRoot(std::string &str)
{
	if (m_SysSetupMgr)
	{
		return m_SysSetupMgr->GetGameSystemUserFileRoot(str);
	}
	return false;
}

BOOL GameSystemSetup::GetTextFormOpCode(const char* OpCode, std::string &str)
{
	if (m_SysSetupMgr)
	{
		return m_SysSetupMgr->GetTextFormOpCode(OpCode, str);
	}
	return FALSE;
}

void GameSystemSetup::RegiestCtrl(const char* OpCode, UControl* ctrl)
{
	if (m_SysSetupMgr)
	{
		return m_SysSetupMgr->RegiestCtrl(OpCode, ctrl);
	}
	return;
}

void GameSystemSetup::OnLinkOpCode(const char* OpCode, bool LinkData)
{	
	if (m_SysSetupMgr)
	{
		return m_SysSetupMgr->OnLinkOpCode(OpCode, LinkData);
	}
	return;
}

Setup_struct* GameSystemSetup::GetStructWithOpCode(const char* OpCode)
{
	if (m_SysSetupMgr)
	{
		return m_SysSetupMgr->GetStructWithOpCode(OpCode);
	}
	return NULL;
}
UControl* GameSystemSetup::GetControlWithOpCode(const char* OpCode)
{	
	if (m_SysSetupMgr)
	{
		return m_SysSetupMgr->GetControlWithOpCode(OpCode);
	}
	return NULL;
}

bool GameSystemSetup::GetStructWithSection(const char* section, vector<Setup_struct*>& vec)
{
	if (m_SysSetupMgr)
	{
		return m_SysSetupMgr->GetStructWithSection(section, vec);
	}
	return false;
}

void GameSystemSetup::SetupSetting()
{
	if (m_SysSetupMgr)
	{
		m_SysSetupMgr->SetupSetting();
	}
}

void GameSystemSetup::SetupSettingByDOM(const char* OpCode, SystemSetDOMTool &Dom, bool bRun)
{
	if (m_SysSetupMgr)
	{
		m_SysSetupMgr->SetupSettingByDOM(OpCode, Dom, bRun);
	}
}

void GameSystemSetup::SetSystemSettingFormDOM(const char* Section, SystemSetDOMTool &Dom, bool bRun)
{
	if (m_SysSetupSave)
	{
		m_SysSetupSave->SetSystemSettingFormDOM(Section, Dom, bRun);
	}
}

void GameSystemSetup::SetSystemSettingFormSection(const char* Section)
{
	if (m_SysSetupMgr)
	{
		m_SysSetupMgr->SetupSettingBySection(Section);
	}
}

void GameSystemSetup::QuitWithoutSave()
{
	if (m_SysSetupMgr)
	{
		m_SysSetupMgr->AbandonSetting();
	}
}

void GameSystemSetup::SaveWithoutQuit()
{
	if (m_SysSetupSave)
	{
		m_SysSetupSave->SetupSystemSetting();
	}
}

void GameSystemSetup::QuitAndSave()
{
	if (m_SysSetupSave)
	{
		m_SysSetupSave->SetupSystemSetting();
	}
}

void GameSystemSetup::SetDefault(const char* buf)
{
	if (m_SysSetupLoad)
	{
		m_SysSetupLoad->LoadDefaultSetting(buf);
	}
}
bool GameSystemSetup::GetLastRollName(std::string &name)
{
	if (m_SysSetupLoad)
	{
		return m_SysSetupLoad->LoadLastRollInfo(name);
	}
	return false;
}
bool GameSystemSetup::SaveLastRollName(const char* name)
{
	if (m_SysSetupSave)
	{
		return m_SysSetupSave->SaveAccountFile(name);
	}
	return false;
}

bool GameSystemSetup::SaveChatSetting()
{
	if (m_SysSetupSave)
	{
		return m_SysSetupSave->SaveUserChatSetFile();
	}
	return false;
}

void GameSystemSetup::SetField(const char* OpCode, bool Choose)
{
	if(!IsLoaded())
		return;

	if (m_SysSetupMgr)
	{
		m_SysSetupMgr->SetField(OpCode, Choose);
	}
}
void GameSystemSetup::SetField(const char* OpCode, int date)
{
	if(!IsLoaded())
		return;

	if (m_SysSetupMgr)
	{
		m_SysSetupMgr->SetField(OpCode, date);
	}
}
void GameSystemSetup::SetField(const char* OpCode, UPoint date)
{
	if(!IsLoaded())
		return;

	if (m_SysSetupMgr)
	{
		m_SysSetupMgr->SetField(OpCode, date);
	}
}
void GameSystemSetup::SetField(const char* OpCode, float date)
{
	if(!IsLoaded())
		return;

	if (m_SysSetupMgr)
	{
		m_SysSetupMgr->SetField(OpCode, date);
	}
}
void GameSystemSetup::SaveHookSetFile()
{
	if (m_SysSetupSave)
	{
		m_SysSetupSave->SaveUserHookSetFile();
	}
}
void GameSystemSetup::SaveFile()
{
	if(!IsLoaded())
		return;

	if (m_SysSetupSave)
	{
		m_SysSetupSave->SaveUserSetupFile();
	}
}
BOOL GameSystemSetup::LoadFile(int FILETYPE)
{
	if (FILETYPE == FILE_USERSETTING)
	{
		if (m_SysSetupLoad)
		{
			return m_SysSetupLoad->LoadUserSetupFile();
		}
	}
	if (FILETYPE == FILE_USERKEYMAP)
	{
		if (m_SysSetupLoad)
		{
			return m_SysSetupLoad->LoadUserKeyMapFile();
		}
	}
	if (FILETYPE == FILE_CHATSETTING)
	{
		if (m_SysSetupLoad)
		{
			return m_SysSetupLoad->LoadChatSetup();
		}
	}
	if (FILETYPE == FILE_HOOKSETTING)
	{
		if (m_SysSetupLoad)
		{
			return m_SysSetupLoad->LoadUserHookSetFile();
		}
	}
	return false;
}

bool GameSystemSetup::IsLoaded()
{
	return m_SysSetupLoad->bLoadingEnd;
}