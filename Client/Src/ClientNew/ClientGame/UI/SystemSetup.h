#ifndef __SYSTEMSETUP_H__
#define __SYSTEMSETUP_H__
#include "UInc.h"
#include "GameSystemSetupStruct.h"
#include "Singleton.h"
enum PartyPath
{
	PATH_ACCOUNTNAME,
	PATH_SERVERNAME,
	PATH_ROLENAME,
};
enum
{
	FILE_USERSETTING,
	FILE_USERKEYMAP,
	FILE_CHATSETTING,
	FILE_HOOKSETTING,
};
#define SystemSetup GameSystemSetup::GetSingleton() 

class GameSystemSetup : public Singleton<GameSystemSetup>
{
	SINGLETON(GameSystemSetup);
public:
	void DefaultSystemSet();
public:
	BOOL InitSystem();
	BOOL CreateUserDirectory();
	BOOL RemoveUserDirectory(const char* UserName);
public:
	void SetGameSystemUserFileRoot(int Partypath, const char* path);
	bool GetGameSystemUserFileRoot(std::string &str);
	bool GetGameSystemAccountFile(std::string &str);

	BOOL GetTextFormOpCode(const char* OpCode, std::string &str);
	void RegiestCtrl(const char* OpCode, UControl* ctrl);

	Setup_struct* GetStructWithOpCode(const char* OpCode);
	UControl* GetControlWithOpCode(const char* OpCode);
	bool GetStructWithSection(const char* section, vector<Setup_struct*>& vec);

	void OnLinkOpCode(const char* OpCode, bool LinkData);

	void SetField(const char* OpCode, bool Choose);
	void SetField(const char* OpCode, int date);
	void SetField(const char* OpCode, UPoint date);
	void SetField(const char* OpCode, float date);

	void SaveFile();
	void SaveHookSetFile();
	BOOL LoadFile(int FILETYPE);
	bool IsLoaded();
public:
	void SetupSetting();
	void SetupSettingByDOM(const char* OpCode, class SystemSetDOMTool &Dom, bool bRun = true);
	void SetSystemSettingFormDOM(const char* Section, SystemSetDOMTool &Dom, bool bRun = true);
	void SetSystemSettingFormSection(const char* Section);
public:
	void QuitWithoutSave();
	void SaveWithoutQuit();
	void QuitAndSave();
	void SetDefault(const char* buf);
public:
	bool GetLastRollName(std::string &name);
	bool SaveLastRollName(const char* name);
	bool SaveChatSetting();
protected:
	class GameSystemSetupManage* m_SysSetupMgr;
	class GameSystemSetupSave* m_SysSetupSave;
	class GameSystemSetupLoad* m_SysSetupLoad;
};
#endif