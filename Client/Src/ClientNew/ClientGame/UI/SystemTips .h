#ifndef SYSTEM_TIPS_H
#define SYSTEM_TIPS_H

//--------------------------------------
//
//--------------------------------------
#include "UInc.h"
#include "Utils/LevelTipsDB.h"
class SystemTips : public UControl
{
	UDEC_CLASS(SystemTips);
	UDEC_MESSAGEMAP();
public:	
	SystemTips();
	~SystemTips();

	void GetTipsGroup();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual BOOL OnEscape();
	void OnClickCloseBtn();
protected:
	string GetTitle(UINT TitleType);  //获得标题
	UColor GetTitleColor(UINT TitleType);
	UColor GetContentColor(UINT TitleType); //获得内容的颜色
protected:
	UString m_CloseBtnStrFile;  // 关闭按钮的skin;
	UBitmapButton* m_CloseBtn;
private:
	UINT index;
	USkinPtr m_Popo;
	vector<CLevelTipsDB::stLevelTips> m_TipsGroup;
	UFontPtr m_TitleFont;
	//UStringW m_QuestTips;
	//UStringW m_SystemTips;
};

class UGiftGivenTips : public UStaticText
{
	UDEC_CLASS(UGiftGivenTips);
public:
	UGiftGivenTips();
	~UGiftGivenTips();
public:
	void GetSysGift(uint32 timecount);
private:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual BOOL HitTest(const UPoint& parentCoordPoint);

	void SetClock(float time);

	uint32 mtimecount;
	float  mtimefcount;
};

class USystemGiftNotify : public UStaticText
{
	UDEC_CLASS(USystemGiftNotify);
public:
	USystemGiftNotify();
	~USystemGiftNotify();
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);

	void GetGiftNotify(const char* buf);
private:
	float m_TimeTime;
};
struct MsgInfo 
{
	std::string text;
	float	 LastTime;
	BOOL     IsInstaceTime;
};
class UISystemMsg : public UControl
{
	UDEC_CLASS(UISystemMsg);
public:
	UISystemMsg();
	~UISystemMsg();
public:
	void AppSystemMsg(const char* Msg, float time = 0.0f, bool bTop = false);
	void GetSystemMsg(const char* Msg, float time = 0.0f);
	void GetInstaceTime(const char* Msg, float time = 0.0f);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual BOOL HitTest(const UPoint& parentCoordPoint);
private:
	std::list<MsgInfo> m_MsgList;
	UFontPtr m_Font;
	UColor m_FontColor;
	int m_Bottom;
};
class USystemScrollMsg : public UScrollText
{
	UDEC_CLASS(USystemScrollMsg);
public:
	USystemScrollMsg();
	~USystemScrollMsg();
public:
	void AddMoveMsg(uint32 id, uint32 ntype, std::string tips);
	void RemoveMoveMsg(uint32 id);
public:
	virtual BOOL OnCreate();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual BOOL HitTest(const UPoint& parentCoordPoint){return FALSE;};
};

enum eReCountType
{
	ERECOUNT_TYPE_DAMAGE,
	ERECOUNT_TYPE_DPS,
	ERECOUNT_TYPE_HEALTH,
	ERECOUNT_TYPE_HPS,
	ERECOUNT_TYPE_NUMBER,
};

class UPlug_DPSCount_Dialog : public UDialog
{
	UDEC_CLASS(UPlug_DPSCount_Dialog);
	UDEC_MESSAGEMAP();
public:
	UPlug_DPSCount_Dialog();
	~UPlug_DPSCount_Dialog();

	void Update();
	void Reset();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();

	void OnClickBtnLaba();
	void OnClickBtnxiang();
	void OnClickBtnNext();
	void OnClickBtnPrev();

	UStaticText* m_pTitle;
	class UPlug_DPSCount* m_pPlug_Count;
};
class UPlug_DPSCount : public UControl
{
	UDEC_CLASS(UPlug_DPSCount);
public:
	struct stDpsSingle 
	{
		ui64 guid;
		ui32 Class;
		std::string Name;
		ui32 DamageCount;
		ui32 HealCount;
		float dps;
		float hps;
		std::string countstring;

		UPoint Findpos;

		static ui32 m_MaxDamCount;
		static float m_MaxDpsCount;
		static ui32 m_MaxHealCount;
		static float m_MaxHpsCount;
	};

	UPlug_DPSCount();
	~UPlug_DPSCount();

	void AddItem(ui64 guid, const char* Name = NULL, ui32 Class = 0);

	inline void SetShowType(int showtype){ m_Type = showtype;};
	inline int  GetShowType(){return m_Type;}

	void ResetAll();
	void Updatemember();
	void Updatememberdata();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);

	virtual int DrawCountItem(const stDpsSingle& single, const UPoint& position);

	void UpdatePlayerDamage();
	void UpdatePlayerDps();
	void UpdatePlayerHeal();
	void UpdatePlayerHps();
protected:
	std::vector<stDpsSingle> m_DpsSingle;
	UPoint m_ItemSize;

	int m_Type;
};
class UPlug_GroupShow : public UControl
{
	UDEC_CLASS(UPlug_GroupShow);
public:
	struct stGroupMember
	{
		ui64 GUID;
		ui32 Class;
		std::string Name;

		ui32 HP;
		ui32 MaxHP;
		ui32 MP;
		ui32 MaxMP;
		ui32 state;
		bool bOnline;
		bool bSelect;

		UPoint pos;
		UPoint cell;
	};
public:
	UPlug_GroupShow();
	~UPlug_GroupShow();

	void AddMember(int team, const struct TeamDate* teamdate);
	void UpdateMember();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);

	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);

	void DrawMemberItem(const stGroupMember* entry, const UPoint& position, INT width, INT height, bool bmouseover);

	std::vector<stGroupMember> m_GroupMember[6];
	USkinPtr m_popoSkin;
	USkinPtr m_StateSkin;
	UTexturePtr m_texture;
	UTexturePtr m_background;
	UPoint m_MouseCell;
	UPoint m_Board;
	UPoint m_ptLeftBtnDown;
};

class UPlug_WatchTarget_Mgr
{
public:
	UPlug_WatchTarget_Mgr();
	~UPlug_WatchTarget_Mgr();
public:
	void CreatWatchTarget(ui64 guid);
	void UpdateSpellProcess(ui64 guid, int spellcaststate, const char* text = NULL, ui32 castAge = 0, ui32 castLife = 0);

};
extern UPlug_WatchTarget_Mgr* g_WatchTargetMgr;

class UPlug_LockTarget : public UControl
{
	UDEC_CLASS(UPlug_LockTarget);
public:
	UPlug_LockTarget();
	~UPlug_LockTarget();

	void SetGUID(ui64 guid);
	void SetCharacterToUI(class CCharacter* pchar);
	inline ui64 GetGUID(){return mGUID;};
	void SetType(int type);

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);

	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer = -1);

	class CCharacter* GetObject(ui64 guid);

	int m_type;
	ui64 mGUID;
	UProgressBar_Skin* m_HPBar;
	UProgressBar_Skin* m_MPBar;
	UBitmap* m_DynamicHead;
	UBitmap* m_ClassBitmap;
	UStaticText* m_Name;
	UStaticText* m_Level;
};

class UPlug_LockTargetContent : public UDialog
{
	UDEC_CLASS(UPlug_LockTargetContent);
public:
	UPlug_LockTargetContent();
	~UPlug_LockTargetContent();
public:
	void SetGUID(ui64 guid);
	inline ui64 GetGUID(){return m_PlugLockTarget->GetGUID();};

	void SpellProcessBegin();
	void SpellProcessFailure();
	void SpellProcessEnd();
	void SpellProcessUpdate(const char* text, ui32 age, ui32 life);

	void Removethis();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnClose();

	UPlug_LockTarget* m_PlugLockTarget;
	UPlug_LockTarget* m_PlugLockTargetTarget;
	UPlug_LockTarget* m_PlugLock3LTarget;
	class USpellProcessBar* m_SpellProcessBar;
	class UAuraContent* m_AuraContent;
};

#endif