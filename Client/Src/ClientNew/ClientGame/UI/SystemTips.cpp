#include "stdafx.h"
#include "SystemTips .h"
#include "../Network/PacketBuilder.h"
#include "ObjectManager.h"
#include "LocalPlayer.h"
#include "UIGamePlay.h"
#include "UBshowLevelTips.h"
#include "SocialitySystem.h"
#include "DpsCount.h"
#include "SystemSetup.h"
#include "UIShowPlayer.h"
#include "PlayerInputMgr.h"
#include "UITipSystem.h"
#include "UICastBar.h"
#include "UAuraContent.h"
#include "UFun.h"

#define  TextOffset 4     //行与行之间的间距
#pragma warning(push)
#pragma warning(disable : 4244)

UIMP_CLASS(SystemTips,UControl);
UBEGIN_MESSAGE_MAP(SystemTips,UCmdTarget)
UON_BN_CLICKED(0, &SystemTips::OnClickCloseBtn)
UEND_MESSAGE_MAP()
void SystemTips::StaticInit()
{

}
SystemTips::SystemTips()
{
	index = 0;
	m_Popo = NULL;
	m_CloseBtn = NULL;
	m_TipsGroup.clear();
	m_TitleFont = NULL;
}
SystemTips::~SystemTips()
{
	index = 0;
	m_Popo = NULL;
	m_CloseBtn = NULL;
	m_TitleFont = NULL;
}
void SystemTips::GetTipsGroup()
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (!pkLocalPlayer)
	{
		return ;
	}
	m_TipsGroup.clear();
	if (!g_pkLevelTipsDB->GetTips(pkLocalPlayer->GetLevel(), pkLocalPlayer->GetRace(), pkLocalPlayer->GetClass(),m_TipsGroup))
	{
		m_TipsGroup.clear();
	}
	
}
void SystemTips::OnClickCloseBtn()
{
	UBShowLevelTips* bShowLevelTips = INGAMEGETFRAME(UBShowLevelTips, FRAME_IG_BSHOWLEVELTIPS);
	if (bShowLevelTips)
	{
		bShowLevelTips->SetCheck(TRUE);
	}

	//m_TipsGroup.clear();
	//SetVisible(false);
}
BOOL SystemTips::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_Popo == NULL)
	{
		m_Popo = sm_System->LoadSkin("SystemTips\\bg.skin");
		if (!m_Popo)
		{
			return FALSE;
		}
	}

	if (m_CloseBtn == NULL)
	{
		m_CloseBtn = (UBitmapButton*)UBitmapButton::CreateObject();

		USkin* pSkin = NULL;
		if (m_CloseBtn)
		{
			m_CloseBtn->SetField("style", "UButton");
			m_CloseBtn->SetField("bitmap", "SystemTips\\Close.skin");
			m_CloseBtn->SetId(0);
			m_CloseBtn->SetVisible(true);
			AddChild(m_CloseBtn);
			pSkin = m_CloseBtn->GetSkin();
		}
		else
		{
			return FALSE;
		}

		URect CloseBtnRect;


		CloseBtnRect.SetSize(14, 14);
		CloseBtnRect.SetPosition(m_Size.x - CloseBtnRect.GetSize().x - 4, 4);
		if (m_CloseBtn)
		{
			m_CloseBtn->SetWindowRect(CloseBtnRect);
		}
	}

	if (m_TitleFont == NULL)
	{
		m_TitleFont = sm_UiRender->CreateFont("Arial", 12, GB2312_CHARSET);
	}
	
	if (m_TitleFont == NULL )
	{
		return FALSE;
	}

	return TRUE;
}
void SystemTips::OnDestroy()
{
	return UControl::OnDestroy();
}
string SystemTips::GetTitle(UINT TitleType)  //获得标题
{
	switch(TitleType)
	{
		case SYSTEMTIPS_LEVEL:
			return _TRAN("等级提示:");
		case SYSTEMTIPS_QUEST:
			return _TRAN("可以领取的新任务:");
		case SYSTEMTIPS_SKILL:
			return _TRAN("可学习的新技能:");
		case SYSTEMTIPS_EQUIP:
			return _TRAN("可获得的新装备:");
		case SYSTEMTIPS_ITEM:
			return _TRAN("可使用的新消耗道具:");
		case SYSTEMTIPS_MAP:
			return _TRAN("适合练级的区域:");
		case SYSTEMTIPS_WAY:
			return _TRAN("本级别最适合游戏途径:");
		case SYSTEMTIPS_SYS:
			return  _TRAN("系统提示:");
		default:
			return _TRAN("未知类型:");
	}
}
UColor SystemTips::GetContentColor(UINT TitleType) //获得内容的颜色
{
	switch(TitleType)
	{
	case SYSTEMTIPS_LEVEL:	
	case SYSTEMTIPS_QUEST:
	case SYSTEMTIPS_SKILL:
	case SYSTEMTIPS_EQUIP:
	case SYSTEMTIPS_ITEM:
	case SYSTEMTIPS_MAP:
	case SYSTEMTIPS_WAY:
	case SYSTEMTIPS_SYS:
		default:
			return UColor(255, 204, 51, 255);
	}
}
UColor SystemTips::GetTitleColor(UINT TitleType)
{
	switch(TitleType)
	{
	case SYSTEMTIPS_LEVEL:
	case SYSTEMTIPS_QUEST:
	case SYSTEMTIPS_SKILL:
	case SYSTEMTIPS_EQUIP:
	case SYSTEMTIPS_ITEM:
	case SYSTEMTIPS_MAP:
	case SYSTEMTIPS_WAY:
	case SYSTEMTIPS_SYS:
	default:
		 return UColor(235, 137, 41, 255);
	}
}
void SystemTips::OnTimer(float fDeltaTime)
{	
	UControl::OnTimer(fDeltaTime);
}
void SystemTips::OnRender(const UPoint& offset,const URect &updateRect)
{
	int TextHeight = 0;
	if (updateRect.GetSize().x <= 4 || m_TipsGroup.size() == 0)
	{
		return ;
	}
	UFontPtr Font = m_Style->m_spFont ;
	
	UPoint pkExcursion = m_CloseBtn->GetWindowSize();

	if (m_Popo)
	{
		UDrawResizeImage(RESIZE_WANDH, sm_UiRender,m_Popo,offset,updateRect.GetSize());
	}

	//大标题
	{
		UPoint poffset = offset  + pkExcursion;  //距离顶部pkExcursion.Y
		UDrawText(sm_UiRender, m_TitleFont, poffset, UPoint(updateRect.GetWidth() - pkExcursion.x, m_TitleFont->GetHeight()), LCOLOR_WHITE, _TRAN("升级信息"), UT_CENTER);
		TextHeight += m_TitleFont->GetHeight() * 2 ; //大标题与内容间距标题的高度
	}

	for (UINT i = 0 ; i < m_TipsGroup.size(); i++)
	{
		UINT pkType = m_TipsGroup[i].ntype;
		TextHeight += Font->GetHeight();  // 不同TYPE 之间间距内容字体的高度
		UPoint poffset = offset + UPoint(0,TextHeight) + UPoint(pkExcursion.x, 0) ;
		//Draw标题
		UDrawText(sm_UiRender, m_TitleFont, poffset, UPoint(updateRect.GetWidth() - pkExcursion.x, m_TitleFont->GetHeight()), GetTitleColor(pkType),GetTitle(pkType).c_str(), UT_LEFT);
		
		TextHeight += (m_TitleFont->GetHeight() + TextOffset) ;//标题与内容间距为 TextOffset

		//内容往后缩进标题
		int w = updateRect.GetSize().x - 2* pkExcursion.x - pkExcursion.x * 2;
		UPoint pContentoffset = offset + UPoint(0,TextHeight) + UPoint(pkExcursion.x * 3, 0) ;  //缩进2个pkExcursion.x 

		UStringW pContent ;
		pContent.Set(m_TipsGroup[i].tip.c_str());
		int _height = UDrawStaticText(sm_UiRender, Font, pContentoffset, w, GetContentColor(pkType), pContent , UT_LEFT, TextOffset);
		
		TextHeight += _height;
	}
	if (m_Size.y != TextHeight + pkExcursion.y * 2)
	{
		INT bottom = 655 * GetRoot()->GetWindowSize().y / 768;
		m_Size.y = TextHeight + pkExcursion.y * 2;
		//SetBottom( bottom );
	}
	RenderChildWindow(offset, updateRect);
}
BOOL SystemTips::OnEscape()
{
	if (!UControl::OnEscape())
	{
		OnClickCloseBtn();
	}
	return TRUE;
}
////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UGiftGivenTips, UStaticText);
void UGiftGivenTips::StaticInit()
{

}
UGiftGivenTips::UGiftGivenTips()
{
	mtimecount = 0;
	mtimefcount = 0.0f;
}
UGiftGivenTips::~UGiftGivenTips()
{
	mtimecount = 0;
	mtimefcount = 0.0f;
}
void UGiftGivenTips::GetSysGift(uint32 timecount)
{
	mtimecount = timecount;
	mtimefcount = timecount / 1.0f;
	SetVisible(TRUE);
}
BOOL UGiftGivenTips::OnCreate()
{
	if (!UStaticText::OnCreate())
	{
		return FALSE;
	}
	m_Font = sm_UiRender->CreateFont("ArialBold",14, 134);
	SetTextColor("ffaa23");
	return TRUE;
}
void UGiftGivenTips::OnDestroy()
{
	mtimecount = 0;
	mtimefcount = 0.0f;
	Clear();
	SetVisible(FALSE);
	return UStaticText::OnDestroy();
}
void UGiftGivenTips::OnTimer(float fDeltaTime)
{
	if (mtimefcount > 0.0f)
	{
		mtimefcount -= fDeltaTime;
		SetClock(mtimefcount);
		if (mtimefcount < 0.0f)
		{
			mtimefcount = 0.0f;
		}
	}
	else
	{
		SetVisible(FALSE);
	}
}
void UGiftGivenTips::OnRender(const UPoint& offset,const URect &updateRect)
{	
	if (m_Font && sm_UiRender && m_WStrBuffer.GetLength())
	{
		USetTextEdge(TRUE);
		UDrawText(sm_UiRender, m_Font, offset, updateRect.GetSize(), m_FontColor, m_WStrBuffer, (EUTextAlignment)m_TextAlign);
		USetTextEdge(FALSE);
	}
}
BOOL UGiftGivenTips::HitTest(const UPoint& parentCoordPoint)
{
	return FALSE;
}
void UGiftGivenTips::SetClock(float time)
{
	char buf[1024];
	int hour = 0,minute = 0 ,second = 0;
	hour = (int)time/3600;
	minute = (int)(time - hour*3600)/60;
	second = int(time)%60;
	string strtime = _TRAN("神秘礼物即将送达，请等待 ") ;
	sprintf(buf, "%0*u:", 2, hour);
	strtime += buf;
	sprintf(buf, "%0*u:", 2, minute);
	strtime += buf;
	sprintf(buf, "%0*u", 2, second);
	strtime += buf;
	SetText(strtime.c_str());
}
////////////////////////////////////////////////////////////////////////////
#define NotifyTime 3.0f
UIMP_CLASS(USystemGiftNotify , UStaticText);
void USystemGiftNotify::StaticInit()
{

}
USystemGiftNotify::USystemGiftNotify()
{
	m_TimeTime = 0.0f;
}
USystemGiftNotify::~USystemGiftNotify()
{
	m_TimeTime = 0.0f;
}
BOOL USystemGiftNotify::OnCreate()
{
	if (!UStaticText::OnCreate())
	{
		return FALSE;
	}
	m_Font = sm_UiRender->CreateFont("Arial", 20, 134);
	SetTextColor("ffaa23");
	SetTextAlign(UT_CENTER);
	return TRUE;
}
void USystemGiftNotify::OnDestroy()
{
	UStaticText::OnDestroy();
}
void USystemGiftNotify::OnTimer(float fDeltaTime)
{
	if (m_TimeTime > 0.0f)
	{
		m_TimeTime -= fDeltaTime;
		if (m_TimeTime < 0.0f)
		{
			m_TimeTime = 0.0f;
		}
	}
	else
	{
		SetVisible(FALSE);
	}
}
void USystemGiftNotify::GetGiftNotify(const char* buf)
{
	m_TimeTime = NotifyTime;
	SetText(buf);
	SetVisible(TRUE);
	if (GetParent())
	{
		GetParent()->MoveChildTo(this,NULL);
	}
}
void USystemGiftNotify::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_Font && sm_UiRender && m_WStrBuffer.GetLength())
	{
		USetTextEdge(TRUE);
		UDrawText(sm_UiRender, m_Font, offset, updateRect.GetSize(), m_FontColor, m_WStrBuffer, (EUTextAlignment)m_TextAlign);
		USetTextEdge(FALSE);
	}
}
////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UISystemMsg , UControl);
void UISystemMsg::StaticInit()
{

}
UISystemMsg::UISystemMsg()
{
}
UISystemMsg::~UISystemMsg()
{
}
BOOL UISystemMsg::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	m_Font = sm_UiRender->CreateFont("Arial", 20, 134);
	m_FontColor = UColor(255, 170, 35, 255);
	m_Bottom = GetControlRect().bottom;
	m_Position.y = m_Bottom - 3 * (m_Font->GetHeight() + 4);
	m_Size.y = 3 * (m_Font->GetHeight() + 4);
	return TRUE;
}
void UISystemMsg::OnDestroy()
{
	m_MsgList.clear();
	UControl::OnDestroy();
}
void UISystemMsg::OnTimer(float fDeltaTime)
{
	if (!m_MsgList.size())
	{
		SetVisible( FALSE );
		return;
	}
	SetVisible(TRUE);
	int i = 0;
	int maxsize = min(m_MsgList.size(), 3);
	std::list<MsgInfo>::iterator it = m_MsgList.begin();
	while ( it != m_MsgList.end() &&  i  < maxsize )
	{
		it->LastTime -= fDeltaTime;
		++i;	++it;
	}
}
void UISystemMsg::AppSystemMsg(const char* Msg, float time, bool bTop)
{
	;
}
void UISystemMsg::GetSystemMsg(const char* Msg, float time)
{
	MsgInfo Temp;
	std::string msgtxt = Msg;
	ChatPopoFliter(msgtxt);
	Temp.text = msgtxt;
	if (time > 0.0f)
	{
		Temp.LastTime = time;
	}
	else
	{
		Temp.LastTime = NotifyTime;
	}
	Temp.IsInstaceTime = FALSE;
	m_MsgList.push_back(Temp);
	if (GetParent())
	{
		GetParent()->MoveChildTo(this,NULL);
	}
}
void UISystemMsg::GetInstaceTime( const char* Msg, float time )
{
	std::list<MsgInfo>::iterator it = m_MsgList.begin();
	if (it != m_MsgList.end() && it->IsInstaceTime )
	{
		it->text = Msg;
		it->LastTime = time;
	}else
	{
		MsgInfo Temp;
		Temp.text = Msg;
		Temp.LastTime = time;
		Temp.IsInstaceTime = TRUE;
		m_MsgList.insert(m_MsgList.begin(),Temp);
	}
	if (GetParent())
	{
		GetParent()->MoveChildTo(this,NULL);
	}
}
void UISystemMsg::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_Font && sm_UiRender && m_MsgList.size())
	{
		UPoint pos(offset.x, m_Bottom - min(m_MsgList.size(), 3) * (m_Font->GetHeight() + 4));
		UPoint size(updateRect.GetWidth(), m_Font ->GetHeight());
		int i = 0;
		USetTextEdge(TRUE);
		std::list<MsgInfo>::iterator it = m_MsgList.begin();
		while ( it != m_MsgList.end() &&  i  < 3 )
		{
			++i;
			UDrawText(sm_UiRender, m_Font, pos, size, m_FontColor, it->text.c_str(), UT_CENTER );
			pos.y += m_Font->GetHeight() + 4;
			if (it->LastTime <= 0.0f)
			{
				it = m_MsgList.erase(it);
				continue;
			}
			++it;
		}
		USetTextEdge(FALSE);
	}
}
BOOL UISystemMsg::HitTest(const UPoint& parentCoordPoint)
{
	return FALSE;
}
#undef NotifyTime

UIMP_CLASS(USystemScrollMsg, UScrollText);

void USystemScrollMsg::StaticInit()
{

}

USystemScrollMsg::USystemScrollMsg()
{

}

USystemScrollMsg::~USystemScrollMsg()
{

}

BOOL USystemScrollMsg::OnCreate()
{
	if (!UScrollText::OnCreate())
	{
		return FALSE;
	}
	SetScrollSpeed(33);
	return TRUE;
}

void USystemScrollMsg::OnRender(const UPoint& offset,const URect &updateRect)
{
	sm_UiRender->DrawRectFill(updateRect, UColor(0, 0, 0, 170));
	UScrollText::OnRender(offset, updateRect);
}
void USystemScrollMsg::AddMoveMsg(uint32 id, uint32 ntype, std::string tips)
{
	UColor color;
	switch(ntype)
	{
	case 1:
		color.Set(255, 137, 34, 255);
		break;
	case 2:
		color.Set(255, 137, 34, 255);
		break;
	case 3:
		color.Set(0, 0, 255, 255);
		break;
	case 4:
		color.Set(0, 255, 0, 255);
		break;
	case 5:
		color.Set(255, 0, 0, 255);
		break;
	case 6:
		color.Set(255, 255, 255, 255);
		break;
	}
	AddScrollText(id, tips.c_str(), color);
}
void USystemScrollMsg::RemoveMoveMsg(uint32 id)
{
	RemoveScrollText(id);
}


//////////////////////////////////////////////////////////////////////////
static const int MaxShowType = ERECOUNT_TYPE_NUMBER;
static const char* TitleName[] = {"伤害输出", "每秒输出", "治疗统计", "每秒治疗"};
static UColor TitleColor[] = {0xFFFF0200, 0xFFFF0200, 0xFF00FF00, 0xFF00FF00};

UIMP_CLASS(UPlug_DPSCount_Dialog, UDialog);
UBEGIN_MESSAGE_MAP(UPlug_DPSCount_Dialog, UDialog)
UON_BN_CLICKED(0, &UPlug_DPSCount_Dialog::OnClickBtnLaba)
UON_BN_CLICKED(1, &UPlug_DPSCount_Dialog::OnClickBtnxiang)
UON_BN_CLICKED(2, &UPlug_DPSCount_Dialog::OnClickBtnPrev)
UON_BN_CLICKED(3, &UPlug_DPSCount_Dialog::OnClickBtnNext)
UEND_MESSAGE_MAP()

void UPlug_DPSCount_Dialog::StaticInit()
{

}

UPlug_DPSCount_Dialog::UPlug_DPSCount_Dialog()
{
	m_pTitle = NULL;
	m_pPlug_Count = NULL;
}
UPlug_DPSCount_Dialog::~UPlug_DPSCount_Dialog()
{

}
BOOL UPlug_DPSCount_Dialog::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	if (m_pTitle == NULL)
	{
		m_pTitle = UDynamicCast(UStaticText, GetChildByID(4));
		if (!m_pTitle)
		{
			return FALSE;
		}
	}
	if (m_pPlug_Count == NULL)
	{
		m_pPlug_Count = UDynamicCast(UPlug_DPSCount, GetChildByID(5));
		if (!m_pPlug_Count)
		{
			return FALSE;
		}
	}

	m_CloseBtn->SetPosition(UPoint(149, 9));


	int ShowType = m_pPlug_Count->GetShowType();
	m_pPlug_Count->SetShowType(ShowType);
	m_pPlug_Count->Updatememberdata();
	m_pTitle->SetText(_TRAN(TitleName[ShowType]));
	m_pTitle->SetTextColor(TitleColor[ShowType]);

	return TRUE;
}
void UPlug_DPSCount_Dialog::OnDestroy()
{
	UDialog::OnDestroy();
}
void UPlug_DPSCount_Dialog::OnClose()
{
	SystemSetup->SetField("ReCount", false);
	SystemSetup->SetupSetting();
	SystemSetup->SaveFile();
}

void UPlug_DPSCount_Dialog::OnClickBtnLaba()
{

}
void UPlug_DPSCount_Dialog::OnClickBtnxiang()
{
	Reset();
}
void UPlug_DPSCount_Dialog::OnClickBtnNext()
{
	int ShowType = m_pPlug_Count->GetShowType();
	if (ShowType >= 0 && ShowType < ERECOUNT_TYPE_NUMBER && ShowType + 1 < ERECOUNT_TYPE_NUMBER )
	{
		++ShowType;
		m_pPlug_Count->SetShowType(ShowType);
		m_pPlug_Count->Updatememberdata();
		m_pTitle->SetText(_TRAN(TitleName[ShowType]));
		m_pTitle->SetTextColor(TitleColor[ShowType]);
	}

}
void UPlug_DPSCount_Dialog::OnClickBtnPrev()
{
	int ShowType = m_pPlug_Count->GetShowType();
	if (ShowType >= 0 && ShowType < ERECOUNT_TYPE_NUMBER && ShowType - 1 >= 0 )
	{
		--ShowType;
		m_pPlug_Count->SetShowType(ShowType);
		m_pPlug_Count->Updatememberdata();
		m_pTitle->SetText(_TRAN(TitleName[ShowType]));
		m_pTitle->SetTextColor(TitleColor[ShowType]);
	}
}

void UPlug_DPSCount_Dialog::Update()
{
	m_pPlug_Count->Updatemember();
}

void UPlug_DPSCount_Dialog::Reset()
{
	m_pPlug_Count->ResetAll();
}


//////////////////////////////////////////////////////////////////////////
ui32 UPlug_DPSCount::stDpsSingle::m_MaxHealCount = 0;
ui32 UPlug_DPSCount::stDpsSingle::m_MaxDamCount = 0;
float UPlug_DPSCount::stDpsSingle::m_MaxDpsCount = 0;
float UPlug_DPSCount::stDpsSingle::m_MaxHpsCount = 0;


UIMP_CLASS(UPlug_DPSCount, UControl)
void UPlug_DPSCount::StaticInit()
{

}

UPlug_DPSCount::UPlug_DPSCount()
{
	m_ItemSize.x = 0;
	m_ItemSize.y = 16;
	m_Type = 0;
}

UPlug_DPSCount::~UPlug_DPSCount()
{

}

BOOL UPlug_DPSCount::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	m_ItemSize.x = m_Size.x;
	return TRUE;
}

void UPlug_DPSCount::OnDestroy()
{
	UControl::OnDestroy();
}

bool KeyDamCount(const UPlug_DPSCount::stDpsSingle& left, const UPlug_DPSCount::stDpsSingle& right)
{
	return left.DamageCount > right.DamageCount;
}

bool KeyHealCount(const UPlug_DPSCount::stDpsSingle& left, const UPlug_DPSCount::stDpsSingle& right)
{
	return left.HealCount > right.HealCount;
}

bool KeyDPSCount(const UPlug_DPSCount::stDpsSingle& left, const UPlug_DPSCount::stDpsSingle& right)
{
	return left.dps > right.dps;
}

bool KeyHPSCount(const UPlug_DPSCount::stDpsSingle& left, const UPlug_DPSCount::stDpsSingle& right)
{
	return left.hps > right.hps;
}

void UPlug_DPSCount::OnTimer(float fDeltaTime)
{
	if(!m_Visible)
		return;
	static float ftime2 = 0.0f;
	ftime2 += fDeltaTime;
	if (ftime2 >= 10.0f)
	{
		ftime2 = 0.f;
		Updatemember();
		Updatememberdata();
	}

	static float ftime = 1.0f;
	ftime += fDeltaTime;
	if (ftime >= 1.0f)
	{
		ftime = 0.f;
		Updatememberdata();
	}
}


void UPlug_DPSCount::OnRender(const UPoint& offset,const URect &updateRect)
{
	UPoint Position( offset.x + 2, offset.y);
	int index = 10;
	std::vector<stDpsSingle>::iterator it = m_DpsSingle.begin();
	while (it != m_DpsSingle.end() && index > 0)
	{
		int add = DrawCountItem(*it, Position); 
		it->Findpos = Position;
		if (add)
		{
			--index;
		}
		Position.y += add;
		++it;
	}
}

void UPlug_DPSCount::OnMouseMove(const UPoint& position, UINT flags)
{
	URect FindRect;
	int index = 10;
	std::vector<stDpsSingle>::iterator it = m_DpsSingle.begin();
	while (it != m_DpsSingle.end() && index > 0)
	{
		FindRect.SetPosition(it->Findpos);
		FindRect.SetSize(m_ItemSize);
		if (FindRect.PointInRect(position))
		{
			TipSystem->ShowTip(TIP_TYPE_RECOUNT, m_Type, it->Findpos + m_ItemSize, it->guid);
			return;
		}
		else
		{
			TipSystem->HideTip();
		}
		--index;
		++it;
	}
}

void UPlug_DPSCount::OnMouseEnter(const UPoint& position, UINT flags)
{

}

void UPlug_DPSCount::OnMouseLeave(const UPoint& position, UINT flags)
{
	TipSystem->HideTip();
}


void UPlug_DPSCount::AddItem(ui64 guid, const char* Name,  ui32 Class)
{
	stDpsSingle sTemp;
	sTemp.guid = guid;
	sTemp.Name = Name;
	sTemp.Class = Class;
	sTemp.DamageCount = DamageMgr->GetDamageThis( guid );
	sTemp.dps = DamageMgr->GetDps( guid );
	sTemp.HealCount = DamageMgr->GetHealThis( guid );
	sTemp.hps = DamageMgr->GetHps( guid );

	m_DpsSingle.push_back(sTemp);
}

void UPlug_DPSCount::Updatemember()
{
	m_DpsSingle.clear();
	stDpsSingle::m_MaxDamCount = 0;
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if(pkLocalPlayer)
	{
		AddItem(pkLocalPlayer->GetGUID(), pkLocalPlayer->GetName(), pkLocalPlayer->GetClass());

		std::vector<ui64> vGuid;
		SocialitySys->GetTeamSysPtr()->GetTeamMemGUID(vGuid);

		std::string Name;
		ui32 Class;
		for (unsigned int ui = 0 ; ui < vGuid.size() ; ui++)
		{
			if(vGuid[ui] != pkLocalPlayer->GetGUID())
			{
				SocialitySys->GetTeamSysPtr()->GetProperty( vGuid[ui] , &Name, &Class);
				AddItem( vGuid[ui], Name.c_str(), Class );
			}
		}
	}
}

void UPlug_DPSCount::Updatememberdata()
{
	switch (m_Type)
	{
	case ERECOUNT_TYPE_DAMAGE:
		UpdatePlayerDamage();
		break;;
	case ERECOUNT_TYPE_DPS:
		UpdatePlayerDps();
		break;;
	case ERECOUNT_TYPE_HEALTH:
		UpdatePlayerHeal();
		break;;
	case ERECOUNT_TYPE_HPS:
		UpdatePlayerHps();
		break;;
	}
}

static char Countbuf[256];
void UPlug_DPSCount::UpdatePlayerDamage()
{
	uint64 damage = 0;
	stDpsSingle::m_MaxDamCount = 0;
	std::vector<stDpsSingle>::iterator it = m_DpsSingle.begin();
	while (it != m_DpsSingle.end())
	{
		damage = DamageMgr->GetDamageThis( it->guid );
		it->DamageCount = damage;
		sprintf(Countbuf, "%s %u", it->Name.c_str(), it->DamageCount);
		it->countstring = Countbuf;
		if (damage > stDpsSingle::m_MaxDamCount)
		{
			stDpsSingle::m_MaxDamCount = damage;
		}
		++it;
	}
	std::sort(m_DpsSingle.begin(), m_DpsSingle.end(), &KeyDamCount);
}

void UPlug_DPSCount::UpdatePlayerHeal()
{
	uint64 heal = 0;
	stDpsSingle::m_MaxHealCount = 0;
	std::vector<stDpsSingle>::iterator it = m_DpsSingle.begin();
	while (it != m_DpsSingle.end())
	{
		heal = DamageMgr->GetHealThis( it->guid );
		it->HealCount = heal;
		sprintf(Countbuf, "%s %u", it->Name.c_str(), it->HealCount);
		it->countstring = Countbuf;
		if (heal > stDpsSingle::m_MaxHealCount)
		{
			stDpsSingle::m_MaxHealCount = heal;
		}
		++it;
	}
	std::sort(m_DpsSingle.begin(), m_DpsSingle.end(), &KeyHealCount);
}
void UPlug_DPSCount::UpdatePlayerDps()
{
	float dps = 0;
	stDpsSingle::m_MaxDpsCount = 0;
	std::vector<stDpsSingle>::iterator it = m_DpsSingle.begin();
	while (it != m_DpsSingle.end())
	{
		dps = DamageMgr->GetDps(it->guid);
		it->dps = dps;
		sprintf(Countbuf, "%s %0.1f", it->Name.c_str(), it->dps);
		it->countstring = Countbuf;
		if (dps > stDpsSingle::m_MaxDpsCount)
		{
			stDpsSingle::m_MaxDpsCount = dps;
		}
		++it;
	}
	std::sort(m_DpsSingle.begin(), m_DpsSingle.end(), &KeyDPSCount);
}
void UPlug_DPSCount::UpdatePlayerHps()
{
	float hps = 0;
	stDpsSingle::m_MaxHpsCount = 0;
	std::vector<stDpsSingle>::iterator it = m_DpsSingle.begin();
	while (it != m_DpsSingle.end())
	{
		hps = DamageMgr->GetHps(it->guid);
		it->hps = hps;
		sprintf(Countbuf, "%s %0.1f", it->Name.c_str(), it->hps);
		it->countstring = Countbuf;
		if (hps > stDpsSingle::m_MaxHpsCount)
		{
			stDpsSingle::m_MaxHpsCount = hps;
		}
		++it;
	}
	std::sort(m_DpsSingle.begin(), m_DpsSingle.end(), &KeyHPSCount);
}
static const UColor g_pColor[] = {0xFF00FF00, 0xFF592212, 0xFF3B174A, 0xFF013561, 0xFF166014, 0, 0};
int UPlug_DPSCount::DrawCountItem(const stDpsSingle& single, const UPoint& position)
{
	float width = 0.f;
	switch(m_Type)
	{
	case ERECOUNT_TYPE_DAMAGE:
		if (stDpsSingle::m_MaxDamCount == 0 || single.DamageCount == 0)
			return 0;

		width = (m_ItemSize.x - 4) * float(single.DamageCount) / float(stDpsSingle::m_MaxDamCount) ;
		break;
	case ERECOUNT_TYPE_DPS:
		if (stDpsSingle::m_MaxDpsCount == 0 || single.dps == 0)
			return 0;

		width = (m_ItemSize.x - 4) * float(single.dps) / float(stDpsSingle::m_MaxDpsCount) ;
		break;
	case ERECOUNT_TYPE_HEALTH:
		if (stDpsSingle::m_MaxHealCount == 0 || single.HealCount == 0)
			return 0;

		width = (m_ItemSize.x - 4) * float(single.HealCount) / float(stDpsSingle::m_MaxHealCount) ;
		break;
	case ERECOUNT_TYPE_HPS:
		if (stDpsSingle::m_MaxHpsCount == 0 || single.hps == 0)
			return 0;

		width = (m_ItemSize.x - 4) * float(single.hps) / float(stDpsSingle::m_MaxHpsCount) ;
		break;
	}

	sm_UiRender->DrawRectFill(URect(position, UPoint(width, m_ItemSize.y)), g_pColor[single.Class]);

	UDrawText(sm_UiRender, m_Style->m_spFont, position + UPoint(1,1), m_ItemSize, 0x80000000, single.countstring.c_str(), UT_CENTER);
	UDrawText(sm_UiRender, m_Style->m_spFont, position, m_ItemSize, LCOLOR_WHITE, single.countstring.c_str(), UT_CENTER);

	return m_ItemSize.y + 1;
	return 0;
}

void UPlug_DPSCount::ResetAll()
{
	DamageMgr->ResetAll();
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
const INT ItemWidth = 60;
const INT ItemHeight = 60;

UIMP_CLASS(UPlug_GroupShow, UControl)
void UPlug_GroupShow::StaticInit()
{

}
UPlug_GroupShow::UPlug_GroupShow()
{
	for (int i = 0 ; i < 6 ; i++)
	{
		m_GroupMember[i].reserve(5);
	}
	m_popoSkin = NULL;
	m_texture = NULL;
	m_background = NULL;
	m_StateSkin = NULL;
	m_MouseCell.Set(-1, -1);
	m_Board.Set(8, 36);
}

UPlug_GroupShow::~UPlug_GroupShow()
{
	m_popoSkin = NULL;
	m_texture = NULL;
	m_background = NULL;
	m_StateSkin = NULL;
	m_MouseCell.Set(-1, -1);
	m_Board.Set(8, 36);
}

void UPlug_GroupShow::AddMember(int team, const TeamDate* teamdate)
{
	stGroupMember temp;
	temp.GUID = teamdate->Guid;
	temp.Class = teamdate->Class;
	temp.Name = teamdate->Name;

	temp.HP = teamdate->hp;
	temp.MaxHP = teamdate->max_hp;
	temp.MP = teamdate->mp;
	temp.MaxMP = teamdate->max_mp;
	temp.bOnline = teamdate->islogin;
	temp.state = teamdate->state;
	temp.bSelect = false;

	m_GroupMember[team].push_back(temp);
}

void UPlug_GroupShow::UpdateMember()
{
	std::vector<TeamDate> vec;
	for (int i = 0 ; i < 6 ; i++)
	{
		SocialitySys->GetTeamSysPtr()->GetTeamMemByTeam(i, vec);
		m_GroupMember[i].clear();
		for (size_t uj = 0 ; uj < vec.size() ; uj++)
		{
			AddMember(i, &vec[uj]);
		}
	}
	//
	UPoint pos(0, 0), size(0, 0),cell(0, 0);
	for (int i = 0 ; i < 6 ; i++)
	{
		pos.y = 0;
		cell.y = 0;
		std::vector<stGroupMember>::iterator it = m_GroupMember[i].begin();
		while(it != m_GroupMember[i].end())
		{
			stGroupMember& member = *it;
			member.pos = pos;
			member.cell = cell;

			if(SYState()->LocalPlayerInput->GetTargetID() == member.GUID)
				member.bSelect = true;

			pos.y += ItemHeight + 4;
			size.y = max(size.y, pos.y);
			++cell.y;
			++it;
		}
		if(!m_GroupMember[i].empty())
		{
			pos.x += ItemWidth + 4;
			size.x += ItemWidth + 4;
			++cell.x;
		}
	}
	m_Size = size.Offset(m_Board.x * 2, m_Board.y + 8);
}

BOOL UPlug_GroupShow::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_popoSkin == NULL)
	{
		m_popoSkin = sm_System->LoadSkin("Talkpopo\\popo.skin");
		if (!m_popoSkin)
		{
			return FALSE;
		}
	}
	if (m_texture == NULL)
	{
		m_texture = sm_UiRender->LoadTexture("DATA\\UI\\MouseTip\\HpBar1.png");
		if (!m_texture)
		{
			return FALSE;
		}
	}
	if (m_background == NULL)
	{
		m_background = sm_UiRender->LoadTexture("DATA\\UI\\UPlug\\GroupBkg.png");
		if (!m_background)
		{
			return FALSE;
		}
	}
	if (!m_StateSkin)
	{
		m_StateSkin = sm_System->LoadSkin("SocialitySystem\\TeamMemberState.skin");
		if (!m_StateSkin)
		{
			return FALSE;
		}
	}
	return TRUE;
}

void UPlug_GroupShow::OnDestroy()
{
	for (int i = 0 ; i < 6 ; i++)
	{
		m_GroupMember[i].clear();
	}
	UControl::OnDestroy();
}

void UPlug_GroupShow::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	if(GetParent())
	{
		GetParent()->MoveChildTo(this,NULL);
	}
	m_ptLeftBtnDown = point;
	CaptureControl();

	UPoint pta = ScreenToWindow(point);
	pta -= m_Board;

	UPoint cell(  (pta.x < 0 ? -1 : pta.x / (ItemWidth + 4)), (pta.y < 0 ? -1 : pta.y / (ItemHeight + 4)));
	if (cell.x >= 0 && cell.y >=0)
	{
		for (int i = 0 ; i < 6 ; i++)
		{
			for(size_t j = 0; j < m_GroupMember[i].size() ; j++)
			{
				stGroupMember& member = m_GroupMember[i][j];
				if( m_MouseCell.x == member.cell.x 
					&& m_MouseCell.y == member.cell.y)
				{
					SYState()->LocalPlayerInput->SetTargetID(member.GUID);
				}
			}
		}
	}
}

void UPlug_GroupShow::OnMouseUp(const UPoint& pt, UINT uFlags)
{
	ReleaseCapture();
}

void UPlug_GroupShow::OnMouseMove(const UPoint& position, UINT flags)
{
	UPoint pta = ScreenToWindow(position);
	pta -= m_Board;

	UPoint cell(  (pta.x < 0 ? -1 : pta.x / (ItemWidth + 4)), (pta.y < 0 ? -1 : pta.y / (ItemHeight + 4)));
	if (cell.x >= 0 && cell.y >=0)
	{
		m_MouseCell = cell;
	}
	else
	{
		m_MouseCell.Set(-1, -1);
	}
}
void UPlug_GroupShow::OnMouseEnter(const UPoint& position, UINT flags)
{

}
void UPlug_GroupShow::OnMouseLeave(const UPoint& position, UINT flags)
{
	m_MouseCell.Set(-1, -1);
}

void UPlug_GroupShow::OnRender(const UPoint& offset,const URect &updateRect)
{
	//drawBkg
	UDrawResizeImage(RESIZE_WANDH, sm_UiRender, m_popoSkin,offset, updateRect.GetSize() );
	UPoint textpos(offset.x + m_Board.x, offset.y + 8);
	for (int i = 0 ; i < 6 ; i++)
	{
		for(size_t j = 0; j < m_GroupMember[i].size() ; j++)
		{
			stGroupMember& member = m_GroupMember[i][j];
			bool bhover = false;
			if(m_MouseCell.x != -1 && m_MouseCell.y != -1 
				&& m_MouseCell.x == member.cell.x 
				&& m_MouseCell.y == member.cell.y)
			{
				bhover = true;
			}
			DrawMemberItem(&member, member.pos + offset + m_Board, ItemWidth, ItemHeight, bhover);
		}
		if(m_GroupMember[i].size())
		{
			std::string strbuff =_I2A(i + 1);

			//sprintf(buf, "小队%d", i + 1);
			//LangaugeMgr->FormatText( 1, itoa(i+1));
			UPoint textsize(ItemWidth, 20);
			UDrawText(sm_UiRender, m_Style->m_spFont, textpos + UPoint(1, 1), textsize, LCOLOR_BLACK,  _TRAN( N_NOTICE_F0,strbuff.c_str()).c_str(), UT_CENTER);
			UDrawText(sm_UiRender, m_Style->m_spFont, textpos , textsize, 0xFFFFFF00,  _TRAN( N_NOTICE_F0,strbuff.c_str()).c_str(), UT_CENTER);
			textpos.x += ItemWidth + 4;
		}
	}
}

void UPlug_GroupShow::OnTimer(float fDeltaTime)
{
	if (!m_Visible)
		return;
	UpdateMember();
}

void UPlug_GroupShow::OnMouseDragged(const UPoint& position, UINT flags)
{
	if (!IsCaptured())
	{
		return;
	}
	UPoint delta = position - m_ptLeftBtnDown;
	m_ptLeftBtnDown = position;
	UPoint NewPos = GetWindowPos();
	if (true)
	{
		UControl* pDesktop = GetRoot();
		assert(pDesktop && pDesktop->IsDesktop());
		UPoint DesktopSize = pDesktop->GetWindowSize();
		NewPos += delta;
		
		NewPos.x = LClamp(NewPos.x , 0, DesktopSize.x - m_Size.x); 
		NewPos.y = LClamp(NewPos.y , 0, DesktopSize.y - m_Size.y); 
	}else
	{
		NewPos += delta;
	}
	SetPosition(NewPos);
}

void UPlug_GroupShow::DrawMemberItem(const stGroupMember* entry, const UPoint& position, INT width, INT height, bool bmouseover)
{
	//60 * 60
	UPoint shadowoffset(1, 1);
	UPoint pos = position;
	URect tempRect;
	//draw HP bar
	{
		INT hpbarHeight  = 0.83f * height;
		if (entry->MaxHP)
		{
			hpbarHeight = hpbarHeight * float(entry->HP) / float(entry->MaxHP);
			pos.y = position.y +  0.83f * height - hpbarHeight;
		}
		tempRect.pt0.x = pos.x;
		tempRect.pt0.y = pos.y;
		tempRect.pt1.x = pos.x + width;
		tempRect.pt1.y = pos.y + hpbarHeight;
		sm_UiRender->DrawImage(m_texture, tempRect, g_pColor[entry->Class]);
		if(SocialitySys->GetTeamSysPtr()->GetMemberReadyFlag())
		{
			sm_UiRender->DrawSkin(m_StateSkin, entry->state, position);
		}
	}

	//draw Mp bar
	{
		pos.y = position.y + 0.83f * height;
		INT mpbarwedth = width;
		if (entry->MaxMP)
		{
			mpbarwedth = width * float(entry->MP) / float(entry->MaxMP);
		}
		tempRect.pt0.x = pos.x;
		tempRect.pt0.y = pos.y;
		tempRect.pt1.x = pos.x + mpbarwedth;
		tempRect.pt1.y = pos.y + 0.17f *height ;
		sm_UiRender->DrawImage(m_texture, tempRect, 0xFF003D85);
		if(entry->MaxHP && entry->HP == 0)
		{
			UPoint size(width, 0.17f *height);
			UDrawText(sm_UiRender, m_Style->m_spFont, tempRect.GetPosition() + shadowoffset, size, LCOLOR_BLACK, _TRAN("阵亡"), UT_CENTER);
			UDrawText(sm_UiRender, m_Style->m_spFont, tempRect.GetPosition() , size, LCOLOR_RED, _TRAN("阵亡"), UT_CENTER);
		}
	}

	{
		tempRect.pt0.x = position.x ;
		tempRect.pt0.y = position.y;
		tempRect.pt1.x = position.x + width;
		tempRect.pt1.y = position.y + height;
		sm_UiRender->DrawRect(tempRect, LCOLOR_BLACK);
		if (!entry->bOnline)
			sm_UiRender->DrawRectFill(tempRect, 0x4C808080);
		tempRect.InflateRect(-2, -2);
		if(entry->bSelect)
		{
			sm_UiRender->DrawImage(m_background, tempRect, 0xFF7a7af4);
		}else if (bmouseover)
		{
			sm_UiRender->DrawImage(m_background, tempRect, LCOLOR_WHITE);
		}else if (entry->MaxHP &&  entry->HP && float(entry->HP) / float(entry->MaxHP) < 0.3f)
		{
			sm_UiRender->DrawImage(m_background, tempRect, LCOLOR_RED);
		}else if( entry->HP == 0 && entry->bOnline)
		{
			sm_UiRender->DrawRectFill(tempRect, 0x4C000000);
		}
	}


	pos.x = position.x;
	pos.y = position.y + 0.83f * height - m_Style->m_spFont->GetHeight() - 2;
	UPoint size(width, m_Style->m_spFont->GetHeight());
	UDrawText(sm_UiRender, m_Style->m_spFont, pos + shadowoffset, size, LCOLOR_BLACK, entry->Name.c_str(), UT_CENTER);
	UDrawText(sm_UiRender, m_Style->m_spFont, pos, size, entry->bOnline?LCOLOR_WHITE : 0xFF808080, entry->Name.c_str(), UT_CENTER);

	pos.y = pos.y - m_Style->m_spFont->GetHeight() - 12;
	char buf[256];
	if (entry->MaxHP&& entry->MaxHP - entry->HP && entry->bOnline)
	{
		int abshp =  int(entry->HP) - int(entry->MaxHP);
		sprintf(buf, "%d", abshp);

		UDrawText(sm_UiRender, m_Style->m_spFont, pos +shadowoffset, size, LCOLOR_BLACK, buf, UT_CENTER);
		UDrawText(sm_UiRender, m_Style->m_spFont, pos, size, LCOLOR_WHITE, buf, UT_CENTER);
	}else if (!entry->bOnline)
	{
		UDrawText(sm_UiRender, m_Style->m_spFont, pos + shadowoffset, size, LCOLOR_BLACK, _TRAN("离线"), UT_CENTER);
		UDrawText(sm_UiRender, m_Style->m_spFont, pos, size, LCOLOR_WHITE, _TRAN("离线"), UT_CENTER);
	}
}
//////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UPlug_LockTargetContent, UDialog)
void UPlug_LockTargetContent::StaticInit()
{
}
UPlug_LockTargetContent::UPlug_LockTargetContent()
{
	m_PlugLockTarget = NULL;
	m_SpellProcessBar = NULL;
	m_AuraContent = NULL;
	m_PlugLockTargetTarget = NULL;
	m_PlugLock3LTarget = NULL;
}
UPlug_LockTargetContent::~UPlug_LockTargetContent()
{
}
BOOL UPlug_LockTargetContent::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}
	if(m_PlugLockTarget == NULL)
	{
		m_PlugLockTarget = UDynamicCast(UPlug_LockTarget, GetChildByID(0));
		if(!m_PlugLockTarget)
			return FALSE;
		m_PlugLockTarget->SetType(0);
	}
	if(m_SpellProcessBar == NULL)
	{
		m_SpellProcessBar = UDynamicCast(USpellProcessBar, GetChildByID(1));
		if(!m_SpellProcessBar)
			return FALSE;
	}
	if (m_AuraContent == NULL)
	{
		m_AuraContent = UDynamicCast(UAuraContent , GetChildByID(2));
		if (!m_AuraContent)
			return FALSE;
	}
	if (m_PlugLockTargetTarget == NULL)
	{
		m_PlugLockTargetTarget = UDynamicCast(UPlug_LockTarget, GetChildByID(3));
		if (!m_PlugLockTargetTarget)
			return FALSE;
		m_PlugLockTargetTarget->SetType(1);
	}
	if (m_PlugLock3LTarget == NULL)
	{
		m_PlugLock3LTarget = UDynamicCast(UPlug_LockTarget, GetChildByID(4));
		if (!m_PlugLock3LTarget)
			return FALSE;
		m_PlugLock3LTarget->SetType(2);
	}
	if (m_CloseBtn)
	{
		m_CloseBtn->SetPosition(UPoint(255, 4));
	}
	return TRUE;
}
void UPlug_LockTargetContent::OnDestroy()
{
	UDialog::OnDestroy();
}

void UPlug_LockTargetContent::OnRender(const UPoint& offset,const URect &updateRect)
{
	UDialog::OnRender( offset, updateRect );
}

void UPlug_LockTargetContent::OnClose()
{
	Removethis();
}

void UPlug_LockTargetContent::SetGUID(ui64 guid)
{
	m_PlugLockTarget->SetGUID(guid);
	m_PlugLockTargetTarget->SetGUID(guid);
	m_PlugLock3LTarget->SetGUID(guid);
	m_AuraContent->SetType(0);
	m_AuraContent->SetTargetID(guid);
}
void UPlug_LockTargetContent::SpellProcessBegin()
{
	m_SpellProcessBar->SpellCastBegin();
}
void UPlug_LockTargetContent::SpellProcessFailure()
{
	m_SpellProcessBar->SpellCastFailure();
}
void UPlug_LockTargetContent::SpellProcessEnd()
{
	m_SpellProcessBar->SpellCastEnd();
}
void UPlug_LockTargetContent::SpellProcessUpdate(const char* text, ui32 age, ui32 life)
{
	m_SpellProcessBar->SetSpell(text);
	m_SpellProcessBar->SetSpellCast(age, life);
}
void UPlug_LockTargetContent::Removethis()
{
	m_PlugLockTarget->SetGUID(0);
	m_PlugLockTargetTarget->SetGUID(0);
	m_PlugLock3LTarget->SetGUID(0);
	m_SpellProcessBar->SpellCastEnd();
	SetVisible(FALSE);
}
//////////////////////////////////////////////////////////////////////////////
UPlug_WatchTarget_Mgr* g_WatchTargetMgr = NULL;

UPlug_WatchTarget_Mgr::UPlug_WatchTarget_Mgr()
{
	g_WatchTargetMgr = this;
}
UPlug_WatchTarget_Mgr::~UPlug_WatchTarget_Mgr()
{

}
void UPlug_WatchTarget_Mgr::CreatWatchTarget(ui64 guid)
{
	UInGame* pRoot = UInGame::Get();
	if (pRoot && guid)
	{
		for (int i = FRAME_IG_PLUG_WATCHTARGET1 ; i <= FRAME_IG_PLUG_WATCHTARGET5 ; i++)
		{
			UPlug_LockTargetContent* pCtrl = UDynamicCast(UPlug_LockTargetContent, pRoot->GetChildByID(i));
			if (pCtrl != NULL && !pCtrl->IsVisible())
			{
				pCtrl->SetGUID(guid);
				pCtrl->SetVisible(TRUE);
				return;
			}else if (pCtrl == NULL)
			{
				UPlug_LockTargetContent* pWatch= (UPlug_LockTargetContent*)UControl::sm_System->CreateDialogFromFile("UPlug\\Plug_LockTarget.udg");
				if(pWatch)
				{
					pWatch->SetId(i);
					pRoot->AddChild(pWatch);
					pWatch->SetGUID(guid);
					return;
				}
			}
		}
	}
}
void UPlug_WatchTarget_Mgr::UpdateSpellProcess(ui64 guid, int spellcaststate, const char* text, ui32 castAge, ui32 castLife)
{
	UInGame* pRoot = UInGame::Get();
	if (!pRoot)
		return;
	for (int i = FRAME_IG_PLUG_WATCHTARGET1 ; i <= FRAME_IG_PLUG_WATCHTARGET5 ; i++)
	{
		UPlug_LockTargetContent* pTargetContent = UDynamicCast(UPlug_LockTargetContent ,pRoot->GetChildByID(i));
		//0 begin 1 end 2 failure 3 update
		if(pTargetContent && pTargetContent->GetGUID() == guid)
		{
			if (spellcaststate == 0)
			{
				pTargetContent->SpellProcessBegin();
			}else if (spellcaststate == 1)
			{
				pTargetContent->SpellProcessEnd();
			}else if (spellcaststate == 2)
			{
				pTargetContent->SpellProcessFailure();
			}else if (spellcaststate == 3)
			{
				pTargetContent->SpellProcessUpdate(text, castAge, castLife);
			}
		}
	}
}
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UPlug_LockTarget, UControl)
void UPlug_LockTarget::StaticInit()
{
}
UPlug_LockTarget::UPlug_LockTarget()
{
	mGUID = 0;
	m_HPBar = NULL;
	m_MPBar = NULL;
	m_DynamicHead = NULL;
	m_Name = NULL;
	m_Level = NULL;
	m_type = 0;
	m_ClassBitmap = NULL;
}
UPlug_LockTarget::~UPlug_LockTarget()
{
}
BOOL UPlug_LockTarget::OnCreate()
{
	if(!UControl::OnCreate())
	{
		return FALSE;
	}
	if(m_HPBar == NULL)
	{
		m_HPBar = UDynamicCast(UProgressBar_Skin, GetChildByID(0));
		if(!m_HPBar)
			return FALSE;
		m_HPBar->SetShowText(FALSE);
	}
	if(m_MPBar == NULL)
	{
		m_MPBar = UDynamicCast(UProgressBar_Skin, GetChildByID(1));
		if(!m_MPBar)
			return FALSE;
		m_MPBar->SetShowText(FALSE);
	}
	if(m_Name == NULL)
	{
		m_Name = UDynamicCast(UStaticText, GetChildByID(2));
		if(!m_Name)
			return FALSE;
		m_Name->SetTextAlign(UT_CENTER);
		m_Name->SetTextEdge(TRUE);
		m_Name->SetTextColor("02DEDF");
	}
	if(m_Level == NULL)
	{
		m_Level = UDynamicCast(UStaticText, GetChildByID(4));
	}
	if(m_DynamicHead == NULL)
	{
		m_DynamicHead = UDynamicCast(UBitmap, GetChildByID(3));
		if(!m_DynamicHead)
			return FALSE;
	}
	if (m_ClassBitmap == NULL)
	{
		m_ClassBitmap = UDynamicCast(UBitmap, GetChildByID(6));
	}
	return TRUE;
}
void UPlug_LockTarget::OnDestroy()
{
	mGUID = 0;
	UControl::OnDestroy();
}
void UPlug_LockTarget::SetType(int type)
{
	m_type = type;
	m_HPBar->SetShowText(type == 0);
	m_MPBar->SetShowText(type == 0);
}
void UPlug_LockTarget::SetCharacterToUI(CCharacter* pchar)
{
	ui32 Class = 0;
	if(pchar)
	{
		bool bfriend = ObjectMgr->GetLocalPlayer()->IsFriendly(pchar);
		if (bfriend)
			m_HPBar->SetSkin("UIPlayerShow\\HPBar_Target_Friend.skin");
		else
			m_HPBar->SetSkin("UIPlayerShow\\HPBar_Target.skin");

		m_Name->SetText(pchar->GetName());
		switch(pchar->GetGameObjectType())
		{
		case GOT_PLAYER:
			m_DynamicHead->SetBitMapByStr(CPlayer::GetHeadImageFile((CPlayer*)pchar));
			break;
		case GOT_CREATURE:
			m_DynamicHead->SetBitMapByStr(CCharacter::GetHeadImageFile(pchar));
			break;
		}
		Class = pchar->GetClass();
	}
	else
	{
		TeamDate* Item = SocialitySys->GetTeamSysPtr()->GetTeamDate(mGUID);
		if (Item)
		{
			m_Name->SetText(Item->Name.c_str());
			m_DynamicHead->SetBitMapByStr(CPlayer::GetHeadImageFile(NULL, Item->Race, Item->Gender));
			Class = Item->Class;
		}
	}

	if (m_ClassBitmap)
	{
		switch(Class)
		{
		case CLASS_WARRIOR:
			m_ClassBitmap->SetBitMapSkinByStr("UIPlayerShow\\WuXiu.skin");
			break;
		case CLASS_BOW:
			m_ClassBitmap->SetBitMapSkinByStr("UIPlayerShow\\YuJian.skin");
			break;
		case CLASS_PRIEST:
			m_ClassBitmap->SetBitMapSkinByStr("UIPlayerShow\\XianDao.skin");
			break;
		case CLASS_MAGE:
			m_ClassBitmap->SetBitMapSkinByStr("UIPlayerShow\\ZhenWu.skin");
			break;
		case CLASS_DRUID:
		case CLASS_ROGUE:
			break;
		default:
			m_ClassBitmap->SetBitMapSkinByStr("UIPlayerShow\\Default.skin");
			break;
		};
	}
}

void UPlug_LockTarget::SetGUID(ui64 guid)
{
	mGUID = guid;
	CCharacter* pChar = GetObject(guid);
	SetCharacterToUI(pChar);
}
void UPlug_LockTarget::OnTimer(float fDeltaTime)
{
	//if(!m_Visible && (m_type != 1 || m_type != 2) )
	//	return;
	bool bDelete = (mGUID == 0);
	if(mGUID)
	{
		CCharacter* pChar = GetObject(mGUID);
		if(pChar)
		{
			SetCharacterToUI(pChar);
			m_HPBar->SetProgressPos(pChar->GetHP(), pChar->GetMaxHP());
			m_MPBar->SetProgressPos(pChar->GetMP(),pChar->GetMaxMP());
			sprintf(Countbuf, "%u", pChar->GetLevel());
			if (m_Level)
				m_Level->SetText(Countbuf);

			SetVisible(TRUE);
		}
		else if (m_type == 0)
		{
			TeamDate* Item = SocialitySys->GetTeamSysPtr()->GetTeamDate(mGUID);
			if (Item)
			{
				m_HPBar->SetProgressPos(Item->hp, Item->max_hp);
				m_MPBar->SetProgressPos(Item->mp, Item->max_mp);
				sprintf(Countbuf, "%u", Item->lv);
				if (m_Level)
					m_Level->SetText(Countbuf);
				m_Name->SetText(Item->Name.c_str());
			}
			else
			{
				bDelete = true;
			}
		}
		else
		{
			bDelete = true;
		}
	}

	if(bDelete){
		if (m_type == 0)
		{
			UPlug_LockTargetContent* pContent = UDynamicCast(UPlug_LockTargetContent, GetParent());
			if(pContent)
				pContent->Removethis();
		}
		else if (m_type == 1 || m_type == 2)
		{
			SetVisible(FALSE);
		}
	}
}

void UPlug_LockTarget::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	CCharacter* pChar = GetObject(mGUID);
	if (pChar)
	{
		SYState()->LocalPlayerInput->SetTargetID(pChar->GetGUID());
	}
}

UControl* UPlug_LockTarget::FindHitWindow(const UPoint &pt, INT initialLayer)
{
	return this;
}
CCharacter* UPlug_LockTarget::GetObject(ui64 guid)
{
	CCharacter* pChar = NULL;
	if (m_type == 0)
	{
		pChar = (CCharacter*)ObjectMgr->GetObject(guid);
	}else if (m_type == 1)
	{
		pChar = (CCharacter*)ObjectMgr->GetObject(guid);
		if (pChar)
			pChar = pChar->GetTargetObject();
	}else if (m_type == 2)
	{
		pChar = (CCharacter*)ObjectMgr->GetObject(guid);
		if (pChar)
			pChar = pChar->GetTargetObject();
		if (pChar)
			pChar= pChar->GetTargetObject();
	}
	return pChar;
}
#pragma warning(pop)