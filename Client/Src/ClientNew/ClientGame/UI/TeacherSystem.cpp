#include "stdafx.h"
#include "TeacherSystem.h"
#include "Network/PacketBuilder.h"
#include "Network/PacketParser.h"
#include "Player.h"
#include "UMessageBox.h"
#include "Effect/EffectManager.h"
#include "UFun.h"


TeacherSystem* TeacherSystem::m_TeacherMgr = NULL;
TeacherSystem::TeacherSystem()
{

}
TeacherSystem::~TeacherSystem()
{

}
void TeacherSystem::InitMgr()
{
	m_TeacherMgr = new TeacherSystem ;
}
void TeacherSystem::ShutDown()
{
	if (TeacherSystemMgr)
	{
		delete TeacherSystemMgr;
		TeacherSystemMgr = NULL;
	}
}

void TeacherSystem::AcceptBaishi()  //同意拜师
{
	TeacherSystemMgr->SendTuDiFanKui(TRUE);
}
void TeacherSystem::RefuseBaishi()  //拒绝拜师
{
	TeacherSystemMgr->SendTuDiFanKui(FALSE);
}
void TeacherSystem::SendZhaoTuDi(ui64 guid ,string name) // 招募徒弟
{
	if (guid)
	{
		if (name.length())
		{
			PacketBuilder->SendZhaoTuDi(name);
		}else
		{
			CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(guid);
			if (pkPlayer)
			{
				string tem =pkPlayer->GetName(); 
				PacketBuilder->SendZhaoTuDi(tem);
			}
		}
		
	}
}
void TeacherSystem::SendTuDiFanKui(bool accept) //是否同意拜师
{
	PacketBuilder->SendTuDiFanKui(accept);
}

void TeacherSystem::ParseTuDiFankui(MSG_S2C::stRecruitAck msg) //回馈给老师的答复
{
	//string ChatTxt = "" ;
	//string ScenceTxt = "";

	ui64 Localguid  =  ObjectMgr->GetLocalPlayer()->GetGUID();

	switch(msg.ret)
	{
	case MSG_S2C::stRecruitAck::RESULT_ACCEPT:  
		break;
	case MSG_S2C::stRecruitAck::RESULT_REFUSE:
		break;
	case MSG_S2C::stRecruitAck::RESULT_OFFLINE:
		break;
	case MSG_S2C::stRecruitAck::RESULT_YOU_ALREADY_HAVE:
		break;
	case MSG_S2C::stRecruitAck::RESULT_TARGET_ALREADY_HAVE:
		break;
	case MSG_S2C::stRecruitAck::RESULT_DUPLICATE:
		
		break;
	case MSG_S2C::stRecruitAck::RESULT_LOW_LEVEL:
	
		break;
	case MSG_S2C::stRecruitAck::RESULT_DIFFERENT_FACTION:
		
		break;
	}
	//EffectMgr->AddEffeText(NiPoint3(300.0f,300.0f,0.0f), ScenceTxt.c_str());
	//ClientSystemNotify(ChatTxt.c_str());
}
void TeacherSystem::ParseShiFouBaiShi(string who) //收到被老师招募的通知
{
	UMessageBox::MsgBox(_TRAN( N_NOTICE_F1,who.c_str() ).c_str(), (BoxFunction*)TeacherSystem::AcceptBaishi, (BoxFunction*)TeacherSystem::RefuseBaishi);
}

void TeacherSystem::SendUpdateTeacher(ui32 ID)
{
	PacketBuilder->SendQueryNameMsg(ID);
}
void TeacherSystem::ParseUpdate(sObjBaseInfo ObjInfo)
{
	/*std::vector<UpdateObj*> vPlayerTemp;
	vPlayerTemp.clear();
	ObjectMgr->GetObjectByType(TYPEID_PLAYER, vPlayerTemp);
	for(unsigned int ui = 0; ui < vPlayerTemp.size(); ++ui)
	{
		CPlayer* Player = static_cast<CPlayer*>(vPlayerTemp[ui]);
		if (Player->GetPrefixalGuid() == ObjInfo.guid)
		{
			Player->SetPrefixal(ObjInfo.name);
			break ;
		}
	}*/

}