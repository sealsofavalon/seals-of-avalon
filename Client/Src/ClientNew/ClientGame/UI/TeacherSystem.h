#ifndef  TEACHERSYSTEM_H
#define  TEACHERSYSTEM_H

#include "ObjectManager.h"
#define TeacherSystemMgr TeacherSystem::m_TeacherMgr

class TeacherSystem
{
public:
	TeacherSystem();
	~TeacherSystem();

	static void InitMgr();
	static void ShutDown();
	static TeacherSystem*  m_TeacherMgr;

	static void AcceptBaishi();  //同意拜师
	static void RefuseBaishi();  //拒绝拜师

	void SendZhaoTuDi(ui64 guid ,string name); // 招募徒弟
	void SendTuDiFanKui(bool accept); //是否同意拜师

	void ParseTuDiFankui(MSG_S2C::stRecruitAck msg); //回馈给老师的答复
	void ParseShiFouBaiShi(string who); //收到被老师招募的通知

	void ParseUpdate(sObjBaseInfo ObjInfo);
	void SendUpdateTeacher(ui32 ID); //更新师徒称谓等数据
protected:
private:
};


#endif