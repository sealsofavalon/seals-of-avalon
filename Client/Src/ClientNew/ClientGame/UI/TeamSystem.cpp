#include "StdAfx.h"
#include "TeamSystem.h"
#include "UIShowPlayer.h"
#include "UFun.h"
#include "Player.h"
#include "UIPlayerHead.h"
#include "ObjectManager.h"

TeamSystem::TeamSystem()
{
	m_TeamMember[0] = NULL;
	m_TeamMember[1] = NULL;
	m_TeamMember[2] = NULL;
	m_TeamMember[3] = NULL;

}
TeamSystem::~TeamSystem()
{
	
}
void TeamSystem::ParseMemberList(std::vector<TeamDate*>& p)
{
	for (unsigned int ui = 0 ; ui < p.size() ; ui++)
	{
		if (ui < 4)
		{
			m_TeamMember[ui] = p[ui];
		}
		
	}

	//ˢ��UI 
	for (int j = PLAYER_HEAD_TEAMMEM; j <PLAYER_HEAD_MAX; j++ )
	{
		UIPlayerHead* pkHead = (UIPlayerHead*) TeamShow->GetTeamCtrl(j);
		pkHead->SetGuid(m_TeamMember[j - PLAYER_HEAD_TEAMMEM]->Guid, m_TeamMember[j - PLAYER_HEAD_TEAMMEM]);
	}
}
TeamDate* TeamSystem::GetData(ui64 guid)
{
	if (guid == 0)
	{
		return NULL ;
	}
	for(int i = 0; i < 4 ; i++)
	{
		if (m_TeamMember[i] && m_TeamMember[i]->Guid == guid)
		{
			return m_TeamMember[i];
		}
	}
	return NULL;
}


void TeamSystem::UpdateFromObj(ui64 guid,ui32 mask, BOOL bFromData)
{
	for (int j = PLAYER_HEAD_TARGET; j <PLAYER_HEAD_MAX; j++ )
	{
		UIPlayerHead* pkHead = (UIPlayerHead*) TeamShow->GetTeamCtrl(j);
		if (pkHead->GetGuid() == guid  && guid)
		{
			pkHead->UpdateDate(guid, mask, bFromData);
		}
	}
}
void TeamSystem::ParseGroupDestory()
{
	//ˢ��UI 
	for (int j = PLAYER_HEAD_TEAMMEM; j <PLAYER_HEAD_MAX; j++ )
	{
		UIPlayerHead* pkHead = (UIPlayerHead*) TeamShow->GetTeamCtrl(j);
		pkHead->SetGuid(0);
	}
}