#ifndef TEAMSYSTEM_H
#define TEAMSYSTEM_H
#include "UTeamDate.h"
//由于原来的组队系统中过多的参加的UI的过程 导致头像UI与组队系统中互相包含的冲突，所以更改组队系统，让其更加有利于数据的维护
//并且头像UI的数据源也不会像现在那么的混乱不堪
//由于目前用到组队系统的地方过多,所以组队系统的更改要尽可能的模仿原始的系统,不宜做过多更改
class TeamSystem
{
public:
	TeamSystem();
	~TeamSystem();
public:
	void ParseMemberList(std::vector<TeamDate*>& p);
	void UpdateFromObj(ui64 guid,ui32 mask, BOOL bFromData);
	void ParseGroupDestory();
public:
	TeamDate* GetData(ui64 guid);
private:
	TeamDate* m_TeamMember[4];  //主要的组队数据
};

#endif