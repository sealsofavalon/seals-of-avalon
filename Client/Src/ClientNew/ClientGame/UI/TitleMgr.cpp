#include "stdafx.h"
#include "TitleMgr.h"
#include "Network/PacketBuilder.h"
#include "Utils/CPlayerTitleDB.h"
#include "ui/UFun.h"
#include "ui/UChat.h"
#include "Effect/EffectManager.h"
#include "Utils/ItemEnchant.h"
#include "AudioInterface.h"

UTitleMgr::UTitleMgr()
{
	m_UITitle = NULL;
	m_TitleList.clear();
}

UTitleMgr::~UTitleMgr()
{

}

BOOL UTitleMgr::CreateTitleUI(UControl* ctrl)
{
	if (ctrl)
	{
		if (!m_UITitle)
		{
			UControl* pkContorl = UControl::sm_System->CreateDialogFromFile("Title\\UTitle.udg");
			m_UITitle = UDynamicCast(UTitle, pkContorl);
			if (!m_UITitle)
			{
				return FALSE;
			}else
			{
				ctrl->AddChild(m_UITitle);
				m_UITitle->SetId(FRAME_IG_TITLEDLG);
			}
		}

		//m_UITitle->SetVisible(FALSE);
		return TRUE;
	}
	return FALSE;
}

void UTitleMgr::InitTitleData()//初始化数据
{

}
void UTitleMgr::SetUTitleVisible()
{
	if (m_UITitle)
	{
		m_UITitle->SetVisible(!m_UITitle->IsVisible());
	}
}
void UTitleMgr::ShowCurTitleDis(ui32 TitleID)
{
	m_UITitle->ShowCurTitleDis(TitleID);
}


// c2s
bool UTitleMgr::SendChangTitleMsg(ui32 TitleIndex)
{
	return PacketBuilder->SendChangTitle(TitleIndex) ;
}
//s2c
void UTitleMgr::ParseTitleList(MSG_S2C::stTitleList TitleList)  //称谓列表
{
	m_TitleList.clear();
	m_UITitle->ClearTitle();
	title_map::iterator it = TitleList.vTitleList.begin();
	
	for(it; it != TitleList.vTitleList.end(); ++it)
	{
		m_TitleList.push_back(it->second.title);
		m_UITitle->AddTitle(it->second.title);
	}


	m_UITitle->ShowCurTitleDis();
}
void UTitleMgr::AddTitle(stTitle Title)		//添加称谓
{
	ui32 TitleId = Title.title ;
	bool bFind = false ;
	for (size_t ui = 0; ui < m_TitleList.size(); ui++)
	{
		if (m_TitleList[ui] == TitleId)
		{
			bFind = true;
			break ;
		}
	}

	if (!bFind)
	{
		m_TitleList.push_back(TitleId);
		m_UITitle->AddTitle(TitleId);
		
		TitleInfo* pkTitleInfo = g_pkPlayerTitle->GetTitleInfo(TitleId);
		if (pkTitleInfo)
		{
			if (pkTitleInfo->stProperty)
			{
				ItemEnchantProto pkItemEnchantProto ;
				if (g_pkItemEnchantDB->GetEnchantProperty(pkTitleInfo->stProperty, pkItemEnchantProto))
				{
					//std::string strbuff = _TRAN( N_NOTICE_F2, pkTitleInfo->stTitle.c_str(),pkItemEnchantProto.sName.c_str() );
					/*ClientSystemNotify("<color:FF0000>恭喜获得新的称号:%s<br>%s", 
						pkTitleInfo->stTitle.c_str(),pkItemEnchantProto.sName.c_str());*/
					ClientSystemNotify( _TRAN( N_NOTICE_F2, pkTitleInfo->stTitle.c_str(),pkItemEnchantProto.sName.c_str() ).c_str() );
				}
			}

			ChatSystem->AppendNewTitle();
			SYState()->IAudio->PlayUiSound(UI_title);
		}
	}else
	{
		//....
	}
	
	//更新称谓
}
void UTitleMgr::RemoveTitle(ui32 TitleId)	//删除称谓
{
	for (size_t i = 0; i < m_TitleList.size(); i++)
	{
		if (m_TitleList[i] == TitleId)
		{
			m_TitleList.erase(m_TitleList.begin() + i);
			//更新称谓
			m_UITitle->RemoveTitle(TitleId);
			return ;
		}
	}
}
bool UTitleMgr::IsTitleInPlayer(ui32 TitleId)
{
	for (size_t i = 0; i < m_TitleList.size(); i++)
	{
		if (m_TitleList[i] == TitleId)
			return true;
	}
	return false;
}
bool UTitleMgr::GetTitleStr(ui32 TitleId, std::string& str)
{
	TitleInfo* pkTitleInfo = g_pkPlayerTitle->GetTitleInfo(TitleId);
	if (pkTitleInfo)
	{
		str = pkTitleInfo->stTitle;
		return true; 
	}
	return false;
}