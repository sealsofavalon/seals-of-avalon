#ifndef __TITLEMGR_H__
#define __TITLEMGR_H__

//////////////////////////////////////////////////////////////////////////
//                         称谓管理                                     //
//////////////////////////////////////////////////////////////////////////
#include "../../../../SDBase/Protocol/C2S.h"
#include "../../../../SDBase/Protocol/S2C.h"
#include "UTitle.h"
#include "UInc.h"
#include "UIIndex.h"
#include "Singleton.h"

#define TitleMgr UTitleMgr::GetSingleton() 

class UTitleMgr : public NiMemObject, public Singleton<UTitleMgr>
{
	SINGLETON(UTitleMgr)
public:
	BOOL CreateTitleUI(UControl* ctrl);	
	void InitTitleData(); //初始化数据
	// c2s
	bool SendChangTitleMsg(ui32 TitleIndex);
	//s2c
	void ParseTitleList(MSG_S2C::stTitleList TitleList);  //称谓列表
	void AddTitle(stTitle Title);		//添加称谓
	void RemoveTitle(ui32 TitleId);		//删除称谓
	//c2c
	bool IsTitleInPlayer(ui32 TitleId);    //
	bool GetTitleStr(ui32 TitleId, std::string& str);
public:
	void ShowCurTitleDis(ui32 TitleID);
	void SetUTitleVisible();
protected:
	UTitle* m_UITitle ;
	std::vector<ui32> m_TitleList; //当前称谓列表
};
#endif