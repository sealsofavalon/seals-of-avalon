#include "StdAfx.h"
#include "TitleTip.h"
#include "UISystem.h"
#include "UITipSystem.h"
#include "UFun.h"
#include "ItemManager.h"
#include "ObjectManager.h"
#include "DpsCount.h"
#include "Utils/SpellDB.h"
#include "Skill/SkillManager.h"
#include "SocialitySystem.h"
#include "SystemTips .h"

#include "Utils/CPlayerTitleDB.h"
#include "Utils/ItemEnchant.h"



TitleTip::TitleTip()
{
}
TitleTip::~TitleTip()
{

}

void TitleTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count)
{
	mEntry = ID;
	mID = ID;
	TitleInfo* pTitleInfo = g_pkPlayerTitle->GetTitleInfo(mID);
	if (pTitleInfo)
	{
		m_KeepDraw = TRUE;
		INT posx = Ctrl->GetRoot()->GetWidth() - Ctrl->GetWidth();
		INT BOTTOM = Ctrl->GetRoot()->GetHeight() - 40;
		Ctrl->SetPosition( UPoint(posx, 0) );
		Ctrl->SetBottom(BOTTOM);
		Ctrl->SetVisible(TRUE);
	}
	else
	{
		return;
	}
	if (m_Content.size() > 0)
	{
		m_Content.clear();
	}

	for (int i = 0 ; i < TITLE_TIP_MAX ; i++)
	{
		CreateContentItem(i);
	}
}
void TitleTip::DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl , const UPoint &pos,const URect &updateRect)
{
	if (m_KeepDraw)
	{
		//渲染边框以及内部填充区域
		UDrawResizeImage(RESIZE_WANDH, pRender, TipSystem->mBkgSkin, pos, updateRect.GetSize());
		//pRender->DrawRectFill(updateRect,pCtrlStl->mFillColor);
		//pRender->DrawRect(updateRect,pCtrlStl->mBorderColor);
		//渲染文字
		int height = DrawContent(pRender,pCtrlStl,pos,updateRect);
		//设置当前提示框的高度
		if (height != Ctrl->GetHeight())
		{
			Ctrl->SetHeight(height);
			INT BOTTOM = Ctrl->GetRoot()->GetHeight() - 40;
			Ctrl->SetBottom(BOTTOM);
		}
	}
}
void TitleTip::DestoryTip()
{
	mEntry = 0;
	mID = 0;
	m_KeepDraw = FALSE;
	m_Content.clear();
}
void TitleTip::CreateContentItem(int Type)
{
	TitleInfo* pTitleInfo = g_pkPlayerTitle->GetTitleInfo(mID);
	TitleTip::ContentItem Temp_CI;
	if (pTitleInfo)
	{
		switch (Type)
		{
		case TITLE_TIP_NAME:
			{
				Temp_CI.Type = TITLE_TIP_NAME;
				Temp_CI.str.Set(pTitleInfo->stTitle.c_str());
				Temp_CI.FontFace = TipSystem->GetNameFontFace();
				m_Content.push_back(Temp_CI);
			}
			break;
		case TITLE_TIP_CLASS:
			{
				Temp_CI.Type = TITLE_TIP_CLASS;
				if(pTitleInfo->Class >= 0 && pTitleInfo->Class < sizeof(TitleClass)/sizeof(TitleClass[0]))
				{
					Temp_CI.str.Set(_TRAN(TitleClass[pTitleInfo->Class]));
					m_Content.push_back(Temp_CI);
				}
			}
			break;
		case TITLE_TIP_DURTIME:
			{
				Temp_CI.Type = TITLE_TIP_DURTIME;
				char* DurTime[] = {
					"永久", "一天", "一周"
				};
				if(pTitleInfo->durTimeId >= 0 && pTitleInfo->durTimeId < 3)
				{
					Temp_CI.str.Set(_TRAN(DurTime[pTitleInfo->durTimeId]));
					m_Content.push_back(Temp_CI);
				}
			}
			break;
		case TITLE_TIP_PROPERTYDESC:
			{
				Temp_CI.Type = TITLE_TIP_PROPERTYDESC;
				std::string Explains;
				ItemEnchantProto pkItemEnchantProto ;
				if (g_pkItemEnchantDB->GetEnchantProperty(pTitleInfo->stProperty, pkItemEnchantProto))
				{
					Explains = _TRAN("称号属性：") + pkItemEnchantProto.sName;
					Temp_CI.str.Set(Explains.c_str());
					m_Content.push_back(Temp_CI);
				}
			}
			break;
		case TITLE_TIP_EXPLAIN:
			{
				Temp_CI.Type = TITLE_TIP_EXPLAIN;
				std::string Explains;
				if(pTitleInfo->stDesc.size())
				{
					Explains = _TRAN("获取方式：") + pTitleInfo->stDesc;
					Temp_CI.str.Set(Explains.c_str());
					m_Content.push_back(Temp_CI);
				}
			}
			break;
		case TITLE_TIP_REWARD:
			{
				Temp_CI.Type = TITLE_TIP_REWARD;
				ui32 rewardId = 0;
				ui32 rewardCount = 0;
				if(g_pkPlaeryTitleReward->GetTitleReward(mID, rewardId, rewardCount))
				{
					ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(rewardId);
					if(ItemInfo)
					{
						//char buf[10];
						//itoa( rewardCount, buf, 10);
						std::string strbuff = _TRAN( N_NOTICE_F3, ItemInfo->C_name.c_str(),_I2A(rewardCount).c_str());
						//sprintf(buf, "获得奖励：【%s】* %u", ItemInfo->C_name.c_str(), rewardCount);
						Temp_CI.str.Set(strbuff.c_str());
						m_Content.push_back(Temp_CI);
					}
				}
			}
			break;
		};
	}
}
int TitleTip::DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect)
{
	int height = m_BoardWide;
	for (unsigned int i = 0 ; i < m_Content.size() ; i++)
	{
		switch (m_Content[i].Type)
		{
		case TITLE_TIP_NAME:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				height += size.y;
			}
			break;
		case TITLE_TIP_CLASS:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_LEFT);
				}

				//height += size.y;
			}
			break;
		case TITLE_TIP_DURTIME:
			{
				//height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].str,UT_RIGHT);
				}

				height += size.y;
			}
			break;
		case TITLE_TIP_PROPERTYDESC:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if (m_Content[i].FontFace)
				{
					size.y = UDrawStaticText(pRender,m_Content[i].FontFace,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				}
				else
				{
					size.y = UDrawStaticText(pRender,pCtrlStl->m_spFont,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				}

				height += size.y;
			}
			break;
		case TITLE_TIP_EXPLAIN:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if (m_Content[i].FontFace)
				{
					size.y = UDrawStaticText(pRender,m_Content[i].FontFace,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				}
				else
				{
					size.y = UDrawStaticText(pRender,pCtrlStl->m_spFont,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				}

				height += size.y;
			}
			break;
		case TITLE_TIP_REWARD:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if (m_Content[i].FontFace)
				{
					size.y = UDrawStaticText(pRender,m_Content[i].FontFace,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				}
				else
				{
					size.y = UDrawStaticText(pRender,pCtrlStl->m_spFont,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				}

				height += size.y;
			}
			break;
		}
	}
	return height + m_BoardWide;
}

struct stSpellInfo_ReCount 
{
	ui32 spellId;
	ui32 spellCount; 
};
std::vector<stSpellInfo_ReCount> vSpellReCount;
bool KeyCount(const stSpellInfo_ReCount& left, const stSpellInfo_ReCount& right)
{
	return left.spellCount > right.spellCount;
}

ReCountTip::ReCountTip()
{

}
ReCountTip::~ReCountTip()
{

}

void ReCountTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid ,UINT count)
{
	mEntry = ID;
	mID = guid;
	vSpellReCount.clear();
	MAPSPELLDPS* pSpellMap  = NULL;
	if(mEntry == ERECOUNT_TYPE_DAMAGE || mEntry == ERECOUNT_TYPE_DPS)
		pSpellMap = DamageMgr->GetSpellDamageThis(mID);
	if(mEntry == ERECOUNT_TYPE_HEALTH || mEntry == ERECOUNT_TYPE_HPS)
		pSpellMap = DamageMgr->GetSpellHealThis(mID);
	if (pSpellMap)
	{
		m_KeepDraw = TRUE;
		Ctrl->SetPosition(pos);
		Ctrl->SetVisible(TRUE);
	}
	else
		return;

	if (m_Content.size() > 0)
	{
		m_Content.clear();
	}

	MAPSPELLDPS::iterator it = pSpellMap->begin();
	while(it != pSpellMap->end())
	{
		stSpellInfo_ReCount temp;
		temp.spellId = it->first;
		temp.spellCount =  it->second;
		vSpellReCount.push_back(temp);
		++it;
	}
	std::sort(vSpellReCount.begin(), vSpellReCount.end(), &KeyCount);
	CreateContentItem(RECOUNT_TIP_NAME);
	CreateContentItem(RECOUNT_TIP_SPELLTITLE);
	int _size = min(vSpellReCount.size(), 3);
	for (int i = 0 ; i < _size ; i++)
	{
		CreateContentItem(RECOUNT_TIP_SPELL1 + i);
	}
}
void ReCountTip::CreateContentItem(int Type)
{
	std::string Name;
	ui32 Class;
	ui32 AllCount = 0;
	if(mEntry == ERECOUNT_TYPE_DAMAGE || mEntry == ERECOUNT_TYPE_DPS)
		AllCount = DamageMgr->GetDamageThis(mID);
	if(mEntry == ERECOUNT_TYPE_HEALTH || mEntry == ERECOUNT_TYPE_HPS)
		AllCount = DamageMgr->GetHealThis(mID);

	SocialitySys->GetTeamSysPtr()->GetProperty( mID , &Name, &Class);
	CCharacter* pChar = (CCharacter*)ObjectMgr->GetObject(mID);
	if (pChar && !Name.size())
	{
		Name = pChar->GetName();
	}
	ReCountTip::ContentItem Temp_CI;
	if ( AllCount && Name.size() )
	{
		switch (Type)
		{
		case RECOUNT_TIP_NAME:
			{
				Temp_CI.Type = RECOUNT_TIP_NAME;
				Temp_CI.left.Set(Name.c_str());
				Temp_CI.FontFace = TipSystem->GetNameFontFace();
				m_Content.push_back(Temp_CI);
			}
			break;
		case RECOUNT_TIP_SPELLTITLE:
			{
				Temp_CI.Type = RECOUNT_TIP_SPELLTITLE;
				if(mEntry == ERECOUNT_TYPE_DAMAGE || mEntry == ERECOUNT_TYPE_DPS)
					Temp_CI.left.Set(_TRAN("排行前三的伤害技能："));
				if(mEntry == ERECOUNT_TYPE_HEALTH || mEntry == ERECOUNT_TYPE_HPS)
					Temp_CI.left.Set(_TRAN("排行前三的治疗技能："));
				m_Content.push_back(Temp_CI);
			}
			break;
		case RECOUNT_TIP_SPELL1:
			{
				Temp_CI.Type = RECOUNT_TIP_SPELL1;
				if (vSpellReCount[0].spellId == 0)
				{
					char buf[256];
					sprintf(buf,"%u(%2.1f%%)", vSpellReCount[0].spellCount, 100.f * float(vSpellReCount[0].spellCount)/float(AllCount));
					Temp_CI.left.Set(_TRAN("1.肉搏"));
					Temp_CI.right.Set(buf);
					m_Content.push_back(Temp_CI);
					break;
				}
				SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)vSpellReCount[0].spellId);
				if (pSpellTemplate)
				{
					char buf[256];
					sprintf(buf, "1.%s" , pSpellTemplate->GetName().c_str());
					Temp_CI.left.Set(buf);
					sprintf(buf, "%u(%2.1f%%)", vSpellReCount[0].spellCount,100.f * float(vSpellReCount[0].spellCount)/float(AllCount));
					Temp_CI.right.Set(buf);
					m_Content.push_back(Temp_CI);
				}
			}
			break;
		case RECOUNT_TIP_SPELL2:
			{
				Temp_CI.Type = RECOUNT_TIP_SPELL2;
				if (vSpellReCount[1].spellId == 0)
				{
					char buf[256];
					sprintf(buf,"%u(%2.1f%%)", vSpellReCount[1].spellCount, 100.f * float(vSpellReCount[1].spellCount)/float(AllCount));
					Temp_CI.left.Set(_TRAN("2.肉搏"));
					Temp_CI.right.Set(buf);
					m_Content.push_back(Temp_CI);
					break;
				}
				SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)vSpellReCount[1].spellId);
				if (pSpellTemplate)
				{
					char buf[256];
					sprintf(buf, "2.%s" , pSpellTemplate->GetName().c_str());
					Temp_CI.left.Set(buf);
					sprintf(buf, "%u(%2.1f%%)", vSpellReCount[1].spellCount,100.f * float(vSpellReCount[1].spellCount)/float(AllCount));
					Temp_CI.right.Set(buf);
				}
				m_Content.push_back(Temp_CI);
			}
			break;
		case RECOUNT_TIP_SPELL3:
			{
				Temp_CI.Type = RECOUNT_TIP_SPELL3;
				if (vSpellReCount[2].spellId == 0)
				{
					char buf[256];
					sprintf(buf,"%u(%2.1f%%)", vSpellReCount[2].spellCount, 100.f * float(vSpellReCount[2].spellCount)/float(AllCount));
					Temp_CI.left.Set(_TRAN("3.肉搏"));
					Temp_CI.right.Set(buf);
					m_Content.push_back(Temp_CI);
					break;
				}
				SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)vSpellReCount[2].spellId);
				if (pSpellTemplate)
				{
					char buf[256];
					sprintf(buf, "3.%s" , pSpellTemplate->GetName().c_str());
					Temp_CI.left.Set(buf);
					sprintf(buf, "%u(%2.1f%%)", vSpellReCount[2].spellCount,100.f * float(vSpellReCount[2].spellCount)/float(AllCount));
					Temp_CI.right.Set(buf);
				}
				m_Content.push_back(Temp_CI);
			}
			break;
		}
	}
}

void ReCountTip::DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect)
{
	if (m_KeepDraw)
	{
		//渲染边框以及内部填充区域
		UDrawResizeImage(RESIZE_WANDH, pRender, TipSystem->mBkgSkin, pos, updateRect.GetSize());
		//渲染文字
		int height = DrawContent(pRender, pCtrlStl, pos, updateRect);
		//设置当前提示框的高度
		if (height != Ctrl->GetHeight())
		{
			Ctrl->SetHeight(height);
		}
	}
}
int ReCountTip::DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect)
{
	int height = m_BoardWide;
	for (unsigned int i = 0 ; i < m_Content.size() ; i++)
	{
		switch (m_Content[i].Type)
		{
		case RECOUNT_TIP_NAME:
			{
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].left,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].left,UT_LEFT);
				}

				height += size.y;
			}
			break;
		case RECOUNT_TIP_SPELLTITLE:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawText(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].left,UT_LEFT);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawText(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].left,UT_LEFT);
				}

				height += size.y;
			}
			break;
		case RECOUNT_TIP_SPELL1:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawTextEx(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].FontColor,m_Content[i].left,m_Content[i].right);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawTextEx(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].FontColor,m_Content[i].left,m_Content[i].right);
				}

				height += size.y;
			}
			break;
		case RECOUNT_TIP_SPELL2:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawTextEx(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].FontColor,m_Content[i].left,m_Content[i].right);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawTextEx(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].FontColor,m_Content[i].left,m_Content[i].right);
				}

				height += size.y;
			}
			break;
		case RECOUNT_TIP_SPELL3:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;

				if(m_Content[i].FontFace)
				{
					size.y = m_Content[i].FontFace->GetHeight();
					UDrawTextEx(pRender,m_Content[i].FontFace,pos,size,m_Content[i].FontColor,m_Content[i].FontColor,m_Content[i].left,m_Content[i].right);
				}
				else
				{
					size.y = pCtrlStl->m_spFont->GetHeight();
					UDrawTextEx(pRender,pCtrlStl->m_spFont,pos,size,m_Content[i].FontColor,m_Content[i].FontColor,m_Content[i].left,m_Content[i].right);
				}

				height += size.y;
			}
			break;
		}
	}
	return height + m_BoardWide;
}

void ReCountTip::DestoryTip()
{
	mEntry = 0;
	mID = 0;
	m_KeepDraw = FALSE;
	m_Content.clear();
}

StringListTip::StringListTip()
{
	m_MaxWide = 0;
}
StringListTip::~StringListTip()
{

}
void StringListTip::CreateTip(UControl* Ctrl, UPoint pos, std::vector<std::string> vstr)
{
	m_MaxWide = 0;
	m_Content.clear();
	m_KeepDraw = TRUE;
	Ctrl->SetPosition(pos);
	Ctrl->SetVisible(TRUE);

	vStrList = vstr;

	CreateContentItem(0);
}
void StringListTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid ,UINT count)
{
	m_KeepDraw = TRUE;
	Ctrl->SetPosition(pos);
	Ctrl->SetVisible(TRUE);

}
void StringListTip::CreateContentItem(int Type)
{
	for (unsigned int ui = 0; ui < vStrList.size() ; ++ui)
	{
		ContentItem TEMP_CI;

		TEMP_CI.FontColor = GetColor(C_COLOR_OTHER);
		TEMP_CI.FontFace = UControl::sm_UiRender->CreateFont("Arial", 14, 134);
		TEMP_CI.str.Set( vStrList[ui].c_str() );
		TEMP_CI.LineHAdd = 0;

		//m_MaxWide = max(m_MaxWide, TEMP_CI.FontFace->GetStrWidth(TEMP_CI.str));
		m_Content.push_back(TEMP_CI);
	}
}

void StringListTip::DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect)
{
	if (m_KeepDraw)
	{
		//渲染边框以及内部填充区域
		UDrawResizeImage(RESIZE_WANDH, pRender, TipSystem->mBkgSkin, pos, updateRect.GetSize());
		//渲染文字
		int height = DrawContent(pRender, pCtrlStl, pos, updateRect);
		//设置当前提示框的高度
		if (height != Ctrl->GetHeight())
		{
			Ctrl->SetHeight(height);
		}
	}
}
int StringListTip::DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect)
{
	int height = m_BoardWide;
	IUFont* font = NULL;
	for (unsigned int i = 0 ; i < m_Content.size() ; i++)
	{
		if(m_Content[i].FontFace)
			font = m_Content[i].FontFace;
		else
			font = pCtrlStl->m_spFont;

		DrawToolTipText(Pos, updateRect, m_BoardWide, height, pRender, font, m_Content[i].str, m_Content[i].FontColor);
	}
	return height + m_BoardWide;
}

void StringListTip::DestoryTip()
{
	mEntry = 0;
	mID = 0;
	m_KeepDraw = FALSE;
	m_Content.clear();
	vStrList.clear();
	m_MaxWide = 0;
}
//////////////////////////////////////////////////////////////////////////

PlayerPropertyTip::PlayerPropertyTip()
{

}
PlayerPropertyTip::~PlayerPropertyTip()
{

}

void PlayerPropertyTip::CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid ,UINT count)
{
	m_KeepDraw = TRUE;
	mEntry = ID;
	mID = guid;
	m_Content.clear();
	if(mEntry != FIELD_ROLE_VITALITY)
	{
		Ctrl->SetPosition(pos);
		Ctrl->SetVisible(TRUE);
		CreateContentItem(PLAYERPROPERTY_TIP_NAME);
		CreateContentItem(PLAYERPROPERTY_TIP_NUMBER);
		CreateContentItem(PLAYERPROPERTY_TIP_DESC);
	}
}
std::string GetString(ui32 mEntry,int type)
{
	CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
	if (!pkPlayer)
	{
		return NULL;
	}
	char Numberbuf[256];
	std::string Number;
	std::string Desc;
	if (mEntry == FIELD_ROLE_STRENGTH)
	{
		sprintf(Numberbuf, "%.1f", pkPlayer->GetFloatValue(UNIT_FIELD_STAT0));
		Desc = _TRAN(N_NOTICE_X1,"");
	}else if (mEntry == FIELD_ROLE_VITALITY)
	{
		sprintf(Numberbuf, "%.1f", pkPlayer->GetFloatValue(UNIT_FIELD_STAT5));
		Desc = _TRAN(N_NOTICE_X2,"");
	}else if (mEntry == FIELD_ROLE_AGILITY)
	{
		sprintf(Numberbuf, "%.1f", pkPlayer->GetFloatValue(UNIT_FIELD_STAT1));
		Desc = _TRAN(N_NOTICE_X3,"");
	}else if (mEntry == FIELD_ROLE_INTELLIGENCE)
	{
		sprintf(Numberbuf, "%.1f", pkPlayer->GetFloatValue(UNIT_FIELD_STAT3));
		Desc = _TRAN(N_NOTICE_X4,"");
	}else if (mEntry == FIELD_ROLE_SPIRITE)
	{
		sprintf(Numberbuf, "%.1f", pkPlayer->GetFloatValue(UNIT_FIELD_STAT4));
		Desc = _TRAN(N_NOTICE_X5,"");
	}else if (mEntry == FIELD_ROLE_STAMINA)
	{
		sprintf(Numberbuf, "%.1f", pkPlayer->GetFloatValue(UNIT_FIELD_STAT2));
		Desc = _TRAN(N_NOTICE_X6,"");
	}else if (mEntry == FIELD_ROLE_RESISTANCES)
	{
		sprintf(Numberbuf, "%.1f", pkPlayer->GetFloatValue(UNIT_FIELD_RESISTANCES));
	}else if (mEntry == FIELD_ROLE_DODGE_PERCENTAGE)
	{
		sprintf(Numberbuf, "%.2f%%", pkPlayer->GetFloatValue(PLAYER_DODGE_PERCENTAGE));
	}else if (mEntry == FIELD_ROLE_BLOCK_PERCENTAGE)
	{
		sprintf(Numberbuf, "%.2f%%", pkPlayer->GetFloatValue(PLAYER_BLOCK_PERCENTAGE));
	}else if (mEntry == FIELD_ROLE_ATTACK_POWER)
	{
		sprintf(Numberbuf, "%.1f", pkPlayer->GetFloatValue(UNIT_FIELD_ATTACK_POWER));
	}else if (mEntry == FIELD_ROLE_MINDAMAGE)
	{
		sprintf(Numberbuf, "%.1f - %.1f", pkPlayer->GetFloatValue(UNIT_FIELD_MINDAMAGE), pkPlayer->GetFloatValue(UNIT_FIELD_MAXDAMAGE));
	}else if (mEntry == FIELD_ROLE_CRIT_PERCENTAGE)
	{
		sprintf(Numberbuf, "%.2f%%", pkPlayer->GetFloatValue(PLAYER_CRIT_PERCENTAGE));
	}else if (mEntry == FIELD_ROLE_BASEATTACKTIME)
	{
		sprintf(Numberbuf, "%.2f", float(pkPlayer->GetUInt32Value(UNIT_FIELD_BASEATTACKTIME) * 0.001f));
	}else if (mEntry == FIELD_ROLE_RANGED_ATTACK_POWER)
	{
		sprintf(Numberbuf, "%.1f", pkPlayer->GetFloatValue(UNIT_FIELD_RANGED_ATTACK_POWER));
	}else if (mEntry == FIELD_ROLE_MINRANGEDDAMAGE)
	{
		sprintf(Numberbuf, "%.1f - %.1f", pkPlayer->GetFloatValue(UNIT_FIELD_MINRANGEDDAMAGE), pkPlayer->GetFloatValue(UNIT_FIELD_MAXRANGEDDAMAGE));
	}else if (mEntry == FIELD_ROLE_RANGED_CRIT_PERCENTAGE)
	{
		sprintf(Numberbuf, "%.2f%%", pkPlayer->GetFloatValue(PLAYER_RANGED_CRIT_PERCENTAGE));
	}else if (mEntry == FIELD_ROLE_RANGEDATTACKTIME)
	{
		sprintf(Numberbuf, "%.2f", float(pkPlayer->GetUInt32Value(UNIT_FIELD_RANGEDATTACKTIME) * 0.001f));
	}else if (mEntry == FIELD_ROLE_SPELLDAMAGE)
	{
		sprintf(Numberbuf, "%.1f", pkPlayer->GetFloatValue(UNIT_FIELD_SPELLDAMAGE));
	}else if (mEntry == FIELD_ROLE_SPELL_CRIT_PERCENTAGE)
	{
		sprintf(Numberbuf, "%.2f%%", pkPlayer->GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1));
	}else if (mEntry == FIELD_ROLE_HONORCURRENT)
	{
		sprintf(Numberbuf, "%u", pkPlayer->GetUInt32Value(PLAYER_FIELD_HONOR_CURRENCY));
	}else if (mEntry == FIELD_ROLE_HONORTOTAL)
	{
		sprintf(Numberbuf, "%u", pkPlayer->GetUInt32Value(PLAYER_FIELD_TOTAL_HONOR));
	}else if (mEntry == FIELD_ROLE_KILLCOUNT)
	{
		sprintf(Numberbuf, "%u", pkPlayer->GetUInt32Value(PLAYER_FIELD_KILL_COUNT));
	}else if (mEntry == FIELD_ROLE_BEKILLCOUNT)
	{
		sprintf(Numberbuf, "%u", pkPlayer->GetUInt32Value(PLAYER_FIELD_BE_KILL_COUNT));
	}

	if (type == PLAYERPROPERTY_TIP_NUMBER)
	{
		Number = Numberbuf;
		return Number;
	}else if (type == PLAYERPROPERTY_TIP_DESC)
	{
		return Desc;
	}
	return NULL;
}
void PlayerPropertyTip::CreateContentItem(int Type)
{
	ContentItem Temp_CI;
	CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
	bool BFine = false;
	if (pkPlayer && mEntry >= FIELD_ROLE_PLAYERPROPERTY && mEntry< FIELD_MAX)
	{
		switch (Type)
		{
		case PLAYERPROPERTY_TIP_NAME:
			{
				Temp_CI.Type = Type;
				Temp_CI.str.Set(_TRAN(base_str[mEntry]));
				Temp_CI.FontColor = GetColor(C_COLOR_OTHER);
				Temp_CI.FontFace = UControl::sm_UiRender->CreateFont("Arial", 18, 134);
				Temp_CI.LineHAdd = 0;
				BFine = true;
			}
			break;
		case PLAYERPROPERTY_TIP_NUMBER:
			{
				Temp_CI.Type = Type;
				Temp_CI.str.Set(GetString(mEntry, Type).c_str());
				Temp_CI.FontFace = UControl::sm_UiRender->CreateFont("Arial", 18, 134);
				Temp_CI.LineHAdd = 0;
				BFine = true;
			}
			break;
		case PLAYERPROPERTY_TIP_DESC:
			{
				Temp_CI.Type = Type;
				Temp_CI.str.Set(GetString(mEntry, Type).c_str());
				BFine = true;
			}
			break;
		}
		if (BFine)
		{
			m_Content.push_back(Temp_CI);
		}
	}
}

void PlayerPropertyTip::DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect)
{
	if (m_KeepDraw)
	{
		//渲染边框以及内部填充区域
		UDrawResizeImage(RESIZE_WANDH, pRender, TipSystem->mBkgSkin, pos, updateRect.GetSize());
		//渲染文字
		int height = DrawContent(pRender, pCtrlStl, pos, updateRect);
		//设置当前提示框的高度
		if (height != Ctrl->GetHeight())
		{
			Ctrl->SetHeight(height);
		}
	}
}
int PlayerPropertyTip::DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect)
{
	int height = m_BoardWide;
	IUFont* font = NULL;
	for (unsigned int i = 0 ; i < m_Content.size() ; i++)
	{
		if(m_Content[i].FontFace)
			font = m_Content[i].FontFace;
		else
			font = pCtrlStl->m_spFont;
		switch(m_Content[i].Type)
		{
		case PLAYERPROPERTY_TIP_NAME:
			{
				DrawToolTipText(Pos, updateRect, m_BoardWide, height, pRender, font, m_Content[i].str, m_Content[i].FontColor, UT_LEFT, false);
			}
			break;
		case PLAYERPROPERTY_TIP_NUMBER:
			{
				DrawToolTipText(Pos, updateRect, m_BoardWide, height, pRender, font, m_Content[i].str, m_Content[i].FontColor, UT_RIGHT, true);
			}
			break;
		case PLAYERPROPERTY_TIP_DESC:
			{
				height += 5;
				UPoint pos,size; 

				pos.x = Pos.x + m_BoardWide;
				pos.y = Pos.y + height;

				size.x = updateRect.GetWidth() - 2*m_BoardWide;
				size.y = UDrawStaticText(pRender,font,pos,size.x,m_Content[i].FontColor,m_Content[i].str);
				height += size.y;
			}
			break;
		default:
			{
				DrawToolTipText(Pos, updateRect, m_BoardWide, height, pRender, font, m_Content[i].str, m_Content[i].FontColor);
			}
			break;
		}
	}
	return height + m_BoardWide;
}

void PlayerPropertyTip::DestoryTip()
{
	mEntry = 0;
	mID = 0;
	m_KeepDraw = FALSE;
	m_Content.clear();
}