#ifndef TITLETIP_H
#define TITLETIP_H
//------------------------------------------------------
//称号显示的鼠标提示
//------------------------------------------------------
//称号显示需要什么？

//****************************
//*称号名称                  *
//*称号类型			 称号时间*
//*称号属性：                *
//*XXXXXXXXXXXXXXXXXXXXXXXXXX*
//*称号获得:				 *
//*XXXXXXXXXXXXXXXXXXXXXXXXXX*
//****************************
//那么后面渲染时就要按照这个样式来进行
#include "MouseTip.h"
enum
{
	TITLE_TIP_NAME,
	TITLE_TIP_CLASS,
	TITLE_TIP_DURTIME,
	TITLE_TIP_PROPERTYDESC,
	TITLE_TIP_EXPLAIN,
	TITLE_TIP_REWARD,
	TITLE_TIP_MAX
};
class TitleTip :public MouseTip
{
	struct ContentItem  
	{
		ContentItem():Type(TITLE_TIP_MAX),FontFace(NULL),FontColor(UColor(255,255,255,255)){}
		UStringW str;
		UColor FontColor;
		UFontPtr FontFace;
		int Type;
	};
public:
	TitleTip();
	~TitleTip();
public:
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid,UINT count);
	virtual void CreateContentItem(int Type);

	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect);
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect);

	virtual void DestoryTip();
private:
	vector<ContentItem> m_Content;
};

enum
{
	RECOUNT_TIP_NAME,
	RECOUNT_TIP_SPELLTITLE,
	RECOUNT_TIP_SPELL1,
	RECOUNT_TIP_SPELL2,
	RECOUNT_TIP_SPELL3,
	RECOUNT_TIP_MAX
};
class ReCountTip : public MouseTip
{
	struct ContentItem  
	{
		ContentItem():Type(RECOUNT_TIP_MAX),FontFace(NULL),FontColor(255,255,255,255){}
		UStringW left;
		UStringW right;
		UColor FontColor;
		UFontPtr FontFace;
		int Type;
	};
public:
	ReCountTip();
	~ReCountTip();
public:
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid ,UINT count);
	virtual void CreateContentItem(int Type);

	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect);
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect);

	virtual void DestoryTip();
private:
	vector<ContentItem> m_Content;

};

class StringListTip : public MouseTip
{
	struct ContentItem  
	{
		ContentItem():Type(RECOUNT_TIP_MAX),FontFace(NULL),FontColor(255,255,255,255),LineHAdd(0){}
		UStringW str;
		UColor FontColor;
		UFontPtr FontFace;
		int Type;
		int LineHAdd;
	};
public:
	StringListTip();
	~StringListTip();
public:
	virtual void CreateTip(UControl* Ctrl, UPoint pos, std::vector<std::string> vstr);
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid ,UINT count);
	virtual void CreateContentItem(int Type);

	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect);
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect);

	virtual void DestoryTip();
	vector<ContentItem> m_Content;
	std::vector<std::string> vStrList;
	int m_MaxWide;
};

enum
{
	PLAYERPROPERTY_TIP_NAME,
	PLAYERPROPERTY_TIP_NUMBER,
	PLAYERPROPERTY_TIP_DESC,
	PLAYERPROPERTY_TIP_MAX
};
class PlayerPropertyTip : public MouseTip
{
	struct ContentItem  
	{
		ContentItem():Type(RECOUNT_TIP_MAX),FontFace(NULL),FontColor(255,255,255,255),LineHAdd(0){}
		UStringW str;
		UColor FontColor;
		UFontPtr FontFace;
		int Type;
		int LineHAdd;
	};
public:
	PlayerPropertyTip();
	~PlayerPropertyTip();

public:
	virtual void CreateTip(UControl* Ctrl,ui32 ID,UPoint pos,ui64 guid ,UINT count);
	virtual void CreateContentItem(int Type);

	virtual void DrawTip(UControl * Ctrl,URender * pRender,UControlStyle * pCtrlStl, const UPoint &pos,const URect &updateRect);
	virtual int DrawContent(URender * pRender,UControlStyle * pCtrlStl,const UPoint &Pos,const URect & updateRect);

	virtual void DestoryTip();
	vector<ContentItem> m_Content;
};
#endif