#include "StdAfx.h"
#include "UActionContener.h"
#include "UIItemSystem.h"
#include "ItemManager.h"
#include "Skill/SkillManager.h"
#include "UITipSystem.h"
#include "ObjectManager.h"
#include "Player.h"
#include "Network/PacketBuilder.h"

UActionContener::UActionContener()
{
	m_ActionItem = NULL;
	m_CanUseItem = TRUE;
	m_bCreate    = FALSE;
	m_OverContener = FALSE;
	m_bPress = FALSE;
	m_MaskTexture = NULL;
	m_GlowTexture = NULL;
	m_PubTickTimeLife = 1.0f;
	m_CDTickTimeLife = 4.0f;

	m_PubTickTimeAge = 0.0f;
	m_CDTickTimeAge = 0.0f;
	m_bTicking = FALSE;
	m_bCasting = FALSE;
	m_TipsPos = UPoint(0,0);
	mAutoMaskShow = FALSE;
	mbAutoCast = FALSE;
}
UActionContener::~UActionContener()
{
	if (m_ActionItem)
	{
		ClearActionItem();
	}
}
void UActionContener::SetActionItem(UActionItem *aItem)
{
	if (aItem && m_bCreate == FALSE)
	{
		m_ActionItem = aItem;
		m_bCreate = TRUE;
	}
}
void UActionContener::ClearActionItem()
{
	if (m_ActionItem)
	{
		delete m_ActionItem;
	}
	m_ActionItem = NULL;
	m_bCreate = FALSE;
	m_bPress = FALSE;
	m_bCasting = FALSE;
	m_GlowTexture = NULL;
	m_MaskTexture = NULL;
	mbAutoCast = FALSE;
	mAutoMaskShow = FALSE;
}
void UActionContener::Press(int type)
{
	//if (!m_bTicking)
	{
		m_bPress = TRUE;
		
		SetUsed();
	}
}
void UActionContener::Release(int type, bool bUse)
{
	if (m_bPress)
	{
		m_bPress = FALSE;
		ClearUsed();
		if(!bUse)
			return;
		switch (type)
		{
		case MOUSEUP_LEFT:
			UseItem(MOUSEUP_LEFT);
			break;
		case MOUSEUP_RIGHT:
			UseItem(MOUSEUP_RIGHT);
			break;
		}
	}	
}
void UActionContener::DrawActionContener(URender *pRender, const UPoint &pos, const URect &updatarect)
{
	if (m_GlowTexture == NULL)
	{
		m_GlowTexture = UControl::sm_UiRender->LoadTexture("DATA\\UI\\SKIN\\SkillBtn.png");
	}


	if (m_GlowTexture== NULL )
	{
		return;
	}

	if (m_bPress)
	{
		URect uvRect;
		uvRect.left = 2;
		uvRect.top = 2;
		uvRect.right = 38;
		uvRect.bottom = 38;
		pRender->DrawImage(m_GlowTexture, updatarect, uvRect, LCOLOR_WHITE, 1);
	}
	else if (m_OverContener)
	{
		URect uvRect;
		uvRect.left = 42;
		uvRect.top = 2;
		uvRect.right = 78;
		uvRect.bottom = 38;
		pRender->DrawImage(m_GlowTexture, updatarect, uvRect, LCOLOR_WHITE, 1);
	}
}

void UActionContener::DrawTickTime(URender * pRender, const URect & updatarect)
{
#define WAITTIME 0.25f
	if (m_MaskTexture == NULL)
	{
		//m_MaskTexture = UControl::sm_UiRender->LoadTexture("DATA\\UI\\SKIN\\TimeBtnMask.dds");
		m_MaskTexture = UControl::sm_UiRender->LoadTexture("DATA\\UI\\UPLUG\\GroupBkg.png");
	}

	if (m_MaskTexture == NULL)
	{
		return;
	}

	if (m_bCasting)
	{
		pRender->DrawImage(m_MaskTexture, updatarect, LCOLOR_ARGB(255, 255, 255, 0));
		if (m_GlowTexture)
		{
			URect uvRect;
			uvRect.left = 2;
			uvRect.top = 2;
			uvRect.right = 38;
			uvRect.bottom = 38;
			pRender->DrawImage(m_GlowTexture, updatarect, uvRect, LCOLOR_WHITE, 1);
		}
	}
	if (  mbAutoCast && mAutoMaskShow )
	{
		pRender->DrawRectFill(updatarect, UColor(255, 0, 0, 80));
		if (m_GlowTexture)
		{
			URect uvRect;
			uvRect.left = 2;
			uvRect.top = 2;
			uvRect.right = 38;
			uvRect.bottom = 38;
			pRender->DrawImage(m_GlowTexture, updatarect, uvRect, LCOLOR_WHITE, 1);
		}
	}

	if (m_bTicking && m_CDTickTimeLife)
	{
		//URect uvRect(0,0,64,64);
		//int maskindex = int(m_CDTickTimeAge*60.f/m_CDTickTimeLife);
		//int col = maskindex%8;
		//int row = maskindex/8;
		//uvRect.Offset(col*64,row*64);
		float angle = m_CDTickTimeAge/m_CDTickTimeLife;
		if (m_CDTickTimeAge < 1.5f)
			pRender->DrawImage(m_MaskTexture, updatarect, LCOLOR_ARGB(255, 255, 255, 0));
		pRender->DrawCoolDownMask(updatarect,angle,LCOLOR_ARGB(175, 255,255,255));
		//pRender->DrawImage(m_MaskTexture,updatarect,uvRect,UColor(255,255,255,255),2);
	}else if (m_bTicking && m_PubTickTimeLife)
	{
		//URect uvRect(0,0,64,64);
		//int maskindex = int(m_PubTickTimeAge*60.f/m_PubTickTimeLife);
		//int col = maskindex%8;
		//int row = maskindex/8;
		//uvRect.Offset(col*64,row*64);
		//pRender->DrawImage(m_MaskTexture,updatarect,uvRect,UColor(255,255,255,120),2);
		if (m_PubTickTimeAge < WAITTIME)
			pRender->DrawImage(m_MaskTexture, updatarect, LCOLOR_ARGB(255, 255, 255, 0));
		else
		{
			float angle = (m_PubTickTimeAge - WAITTIME)/(m_PubTickTimeLife - WAITTIME);
			pRender->DrawCoolDownMask(updatarect, angle, LCOLOR_ARGB(120, 255,255,255));
		}
	}
#undef  WAITTIME
}

void UActionContener::AccelorateKeyDown()
{
	//if (!m_bTicking)
	{	
		m_bPress = TRUE;
		SetUsed();
	}
}
void UActionContener::AccelorateKeyUp()
{
	if (m_bPress)
	{
		m_bPress = FALSE;
		ClearUsed();
		UseItem(MOUSEUP_RIGHT);
	}
}
void UActionContener::FouceUse()
{
	if (m_ActionItem)
	{
		m_ActionItem->Use();
	}
}
void UActionContener::UseItem(int type)
{
	if (m_ActionItem && GetUsed() && !IsItemEmpty())
	{
		//if (mbAutoCast)
		//{
		//	PacketBuilder->SendSpellCancelAutoRepeat();
		//	mbAutoCast = FALSE;
		//	mAutoMaskShow = FALSE;
		//	return;
		//}
		if (type == MOUSEUP_RIGHT)
		{
			m_ActionItem->Use();
		}
		if (type == MOUSEUP_LEFT)
		{
			m_ActionItem->UseLeft();
		}
		
	}
}
void UActionContener::SetUsed()
{
	m_CanUseItem = FALSE;
}
void UActionContener::ClearUsed()
{
	m_CanUseItem = TRUE;
}
BOOL UActionContener::GetUsed()
{
	return m_CanUseItem;
}
ActionDataInfo * UActionContener::GetDataInfo()
{
	if (m_ActionItem && IsCreate())
	{
		return &m_ActionItem->GetDataInfo();
	}
	return NULL;
}
void UActionContener::SetDataInfo(ActionDataInfo & di)
{
	if (m_ActionItem && IsCreate())
	{
		m_ActionItem->SetDataInfo(di);

		if (m_OverContener)
		{
			UpdateToolTip();
		}
	}
}
BOOL UActionContener::IsItemEmpty()
{
	if (m_ActionItem && m_bCreate)
	{
		if (GetDataInfo()->entry != 0)
		{
			return FALSE;
		}
	}
	return TRUE;
}
USkinPtr UActionContener::GetIconSkin(int &IconIndex)
{
	if (IsCreate() && !IsItemEmpty())
	{
		USkinPtr pIncoSkin = NULL ;
		std::string str ;

		if(GetDataInfo()->type == UItemSystem::SPELL_DATA_FLAG)
		{
			SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate(GetDataInfo()->entry);
			if(pSpellTemplate)
			{
				pSpellTemplate->GetICON(str);
			}else
			{
				UTRACE("get spellInfo failed!");
			}
		}

		if (GetDataInfo()->type == UItemSystem::ITEM_DATA_FLAG)
		{
			ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(GetDataInfo()->entry);
			if(ItemInfo)
			{
				str = ItemInfo->C_icon;	
			}else
			{
				UTRACE("get ItemInfo failed!");
			}
		}

		if (str.length())
		{	
			int pos = str.find(";");
			std::string skinstr = str.substr(0,pos);
			std::string strIndex = str.substr(pos+1,str.length() -1);

			pIncoSkin = UControl::sm_System->LoadSkin(skinstr.c_str());
			IconIndex = atoi(strIndex.c_str());
		}

		if (pIncoSkin == NULL)
		{
			UTRACE("load skin  failed!Change Default instead!");
			if (GetDataInfo()->type == UItemSystem::ITEM_DATA_FLAG)
				pIncoSkin = UControl::sm_System->LoadSkin("Icon\\ItemIcon\\DefaultItemIcon.skin");
			else
				pIncoSkin = UControl::sm_System->LoadSkin("Icon\\SkillIcon\\DefaultSkillIcon.skin");
			IconIndex = 0;
		}

		return pIncoSkin;
	}
	else
	{
		return NULL;
	}
}
void UActionContener::InContener()
{
	m_OverContener = TRUE;
}
void UActionContener::OutContener()
{
	m_OverContener = FALSE;
}

void UActionContener::DrawToolTip(const UPoint & pos,ui32 Type)
{
	if(m_ActionItem->GetDataInfo().type == UItemSystem::SPELL_DATA_FLAG)
	{			
		TipSystem->ShowTip(TIP_TYPE_SPELL,m_ActionItem->GetDataInfo().entry,pos);
	}
	else if(m_ActionItem->GetDataInfo().type == UItemSystem::ITEM_DATA_FLAG)
	{		
		if (m_ActionItem->GetDataInfo().Data)
		{
			ItemExtraData* pExtraData = (ItemExtraData*)m_ActionItem->GetDataInfo().Data;
			if (pExtraData)
			{
				TipSystem->ShowTip(m_ActionItem->GetDataInfo().entry, pos, *pExtraData);
				return;
			}
		}
		TipSystem->ShowTip(TIP_TYPE_ITEM,m_ActionItem->GetDataInfo().entry,pos,m_ActionItem->GetDataInfo().Guid,m_ActionItem->GetDataInfo().num);
	}

	m_TipsPos = pos ;
}
void UActionContener::UpdateToolTip()
{
	ReMoveToolTip();
	if(m_ActionItem->GetDataInfo().type == UItemSystem::SPELL_DATA_FLAG)
	{			
		TipSystem->ShowTip(TIP_TYPE_SPELL,m_ActionItem->GetDataInfo().entry,m_TipsPos);
	}
	else if(m_ActionItem->GetDataInfo().type == UItemSystem::ITEM_DATA_FLAG)
	{		
		if (m_ActionItem->GetDataInfo().Data)
		{
			ItemExtraData* pExtraData = (ItemExtraData*)m_ActionItem->GetDataInfo().Data;
			if (pExtraData)
			{
				TipSystem->ShowTip(m_ActionItem->GetDataInfo().entry, m_TipsPos, *pExtraData);
				return;
			}
		}
		TipSystem->ShowTip(TIP_TYPE_ITEM,m_ActionItem->GetDataInfo().entry,m_TipsPos,m_ActionItem->GetDataInfo().Guid,m_ActionItem->GetDataInfo().num);
	}
}
void UActionContener::ReMoveToolTip()
{
	if (TipSystem)
	{
		TipSystem->HideTip();
	}
}
void UActionContener::BeginDrag()
{
	m_bPress = FALSE;
}
void UActionContener::EndDrag()
{

}
void UActionContener::BeginTick()
{
	m_bTicking = TRUE;
}
void UActionContener::PubTickTime(float age,float life)
{
	m_PubTickTimeAge = age;
	m_PubTickTimeLife = life;
}
void UActionContener::CDTickTime(float age,float life)
{
	m_CDTickTimeAge = age;
	m_CDTickTimeLife = life;
}
void UActionContener::EndTick()
{
	m_bTicking = FALSE;
}

void UActionContener::BeginCast()
{
	m_bCasting = TRUE;
}
void UActionContener::EndCast()
{
	m_bCasting = FALSE;
}