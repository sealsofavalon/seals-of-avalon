#ifndef __UACTIONCONTENER_H__
#define __UACTIONCONTENER_H__
enum
{
	MOUSEDOWN_LEFT,
	MOUSEUP_LEFT = MOUSEDOWN_LEFT,
	MOUSEDOWN_RIGHT,
	MOUSEUP_RIGHT = MOUSEDOWN_RIGHT,
};
struct ActionDataInfo
{
	inline ActionDataInfo():type(0),pos(-1),Guid(0),entry(0),num(0),Data(NULL),DataSize(0){};
	~ActionDataInfo(){
		if(Data){
			delete Data;
			Data = NULL;
		}
	}
	ui16                type;
	unsigned char        pos;
	ui32                entry;
	ui64                Guid;
	UINT                num;
	UINT				DataSize;
	void*				Data;
};
class UActionItem
{
public:
	virtual void Use() = 0;//需要重载的接口
	virtual void UseLeft(){Use();}
	virtual ActionDataInfo &GetDataInfo(){return m_dataInfo;};
	virtual void SetDataInfo(ActionDataInfo& di)
	{
		m_dataInfo.Guid = di.Guid;
		m_dataInfo.pos = di.pos;
		m_dataInfo.type = di.type;
		m_dataInfo.entry = di.entry;
		m_dataInfo.num = di.num;
		m_dataInfo.DataSize = di.DataSize;
		if (di.Data)
		{
			m_dataInfo.Data = malloc(di.DataSize);
			memcpy(m_dataInfo.Data, di.Data, di.DataSize);
		}
		else
		{
			if (m_dataInfo.Data)
			{
				free(m_dataInfo.Data);
				m_dataInfo.Data = NULL;
			}
		}
	}
protected:
	ActionDataInfo m_dataInfo;
};
class UActionContener
{
public:
	UActionContener();
	~UActionContener();
	BOOL IsCreate(){return m_bCreate;};
	virtual void SetActionItem(UActionItem * aItem);//SetActionItem(new ....);
	virtual void ClearActionItem();

	virtual void Press(int type);
	virtual void Release(int type, bool bUse = true);

	virtual void AccelorateKeyDown();
	virtual void AccelorateKeyUp();

	virtual void InContener();
	virtual void OutContener();

	virtual void BeginDrag();
	virtual void EndDrag();

	virtual void FouceUse();
public:
	virtual void DrawActionContener(URender * pRender,const UPoint & pos,const URect & updatarect);
	virtual void DrawTickTime(URender * pRender, const URect & updatarect);

	virtual void DrawToolTip(const UPoint & pos,ui32 Type);
	virtual void UpdateToolTip();
	virtual void ReMoveToolTip();

	virtual ActionDataInfo * GetDataInfo();
	virtual void SetDataInfo(ActionDataInfo & di);
	virtual BOOL IsItemEmpty();
	virtual USkinPtr GetIconSkin(int &IconIndex);

	virtual void BeginTick();
	virtual void PubTickTime(float age,float life);
	virtual void CDTickTime(float age,float life);
	virtual void EndTick();

	virtual void BeginCast();
	virtual void EndCast();

	virtual void SetAutoCast(BOOL bAuto){mbAutoCast = bAuto;}
	virtual void SetAutoMaskShow(BOOL bShow){mAutoMaskShow = bShow;}

protected:
	virtual void UseItem(int type);
	virtual void SetUsed();
	virtual BOOL GetUsed();
	virtual void ClearUsed();
protected:

	UActionItem * m_ActionItem;
	BOOL         m_CanUseItem;
	BOOL         m_bCreate;
	BOOL         m_bPress;
	BOOL         m_OverContener;
	BOOL			mbAutoCast;
	BOOL			mAutoMaskShow;
	UTexturePtr   m_MaskTexture;
	UTexturePtr   m_GlowTexture;

	float        m_PubTickTimeLife;
	float        m_CDTickTimeLife;

	float        m_PubTickTimeAge;
	float        m_CDTickTimeAge;

	BOOL         m_bTicking;
	BOOL			m_bCasting;

	UPoint		 m_TipsPos;
};
#endif