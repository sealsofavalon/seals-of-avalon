#include "StdAfx.h"
#include "UArenaStateDlg.h"
#include "UInstanceMgr.h"

UIMP_CLASS(UArenaStateDlg,UControl);

void UArenaStateDlg::StaticInit()
{

}
UArenaStateDlg::UArenaStateDlg()
{
	m_Popo = NULL;
	m_Font = NULL;
	m_IsUpdateChild = FALSE;
}
UArenaStateDlg::~UArenaStateDlg()
{
	m_Popo = NULL;
	m_Font = NULL;
}
BOOL UArenaStateDlg::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_Popo == NULL)
	{
		m_Popo = sm_System->LoadSkin("SystemTips\\bg.skin");
		if (!m_Popo)
		{
			return FALSE;
		}
	}

	if (m_Font == NULL)
	{
		m_Font = sm_UiRender->CreateFont("Arial",16,134);
	}
	return TRUE;
}
void UArenaStateDlg::OnDestroy()
{
	SetVisible(FALSE);
	UControl::OnDestroy();
}
void UArenaStateDlg::OnRender(const UPoint& offset, const URect &updateRect)
{
	std::vector<MSG_S2C::stArenaUpdateState::info> TempList;
	InstanceSys->GetArenaUpdateStateInfo(TempList);
	IUFont *Font = m_Style->m_spFont;
	if (m_Font)
	{
		Font = m_Font;
	}
	if (m_Popo)
	{
		UDrawResizeImage(RESIZE_WANDH, sm_UiRender,m_Popo,offset,updateRect.GetSize());
	}
	UPoint poffset = offset + UPoint(9,8);
	UINT heightadd = 8;
	for (UINT i = 0 ; i < TempList.size() ; i++)
	{
		char buf[256];
		sprintf(buf, "%d", TempList[i].kill);
		poffset = UPoint(poffset.x, heightadd + offset.y);
		if (InstanceSys->IsLocalPlayerName(TempList[i].name.c_str()))
		{
			UDrawTextEx(sm_UiRender, Font, poffset, UPoint(updateRect.GetWidth() - 18, Font->GetHeight()),
				UColor(1,167,222,255),LCOLOR_WHITE,TempList[i].name.c_str() , buf);
		}
		else
		{
			UDrawTextEx(sm_UiRender, Font, poffset, UPoint(updateRect.GetWidth() - 18, Font->GetHeight()),
				UColor(222,1,1,255),LCOLOR_WHITE,TempList[i].name.c_str() , buf);
		}
		heightadd += Font->GetHeight() + 4;
	}
	m_Size.y = heightadd + 8;
	RenderChildWindow(offset, updateRect);
}