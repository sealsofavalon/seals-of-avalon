//-----------------------------------------------------------
//竞技场杀人信息更新提示
//------------------------------------------------------------
#ifndef UARENASTATEDLG_H
#define UARENASTATEDLG_H
#include "UInc.h"
//
class UArenaStateDlg : public UControl
{
	UDEC_CLASS(UArenaStateDlg);
	UArenaStateDlg();
	~UArenaStateDlg();
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
protected:
	USkinPtr m_Popo;
	UFontPtr m_Font;
};
#endif