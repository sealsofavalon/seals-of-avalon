#include "stdafx.h"
#include "UAuctionBig.h"
#include "../ObjectManager.h"
#include "../Network/PacketBuilder.h"
#include "../ItemManager.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "UMessageBox.h"
#include "UChat.h"
#include "UAuctionMgr.h"
#include "UMessageBox.h"
#include "UFun.h"


//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UAuctionBidDlg, UDialog);

UBEGIN_MESSAGE_MAP(UAuctionBidDlg, UDialog)
UON_BN_CLICKED(0, &UAuctionBidDlg::OnAuctionBrowse)
UON_BN_CLICKED(1, &UAuctionBidDlg::OnAuctionBidJinBi)
UON_BN_CLICKED(53, &UAuctionBidDlg::OnAuctionBidYuanBao)
UON_BN_CLICKED(2, &UAuctionBidDlg::OnAuctionAuc)
UON_BN_CLICKED(3, &UAuctionBidDlg::OnBeginAucBtnClicked)
UON_BN_CLICKED(4, &UAuctionBidDlg::OnCancleAucBtnClicked)
UON_BN_CLICKED(5, &UAuctionBidDlg::OnPrevBtnClicked)
UON_BN_CLICKED(6, &UAuctionBidDlg::OnNextBtnClicked)
UON_BN_CLICKED(7, &UAuctionBidDlg::OnDoNothing)
UON_BN_CLICKED(8, &UAuctionBidDlg::OnDoNothing)
UON_BN_CLICKED(9, &UAuctionBidDlg::OnDoNothing)
UON_BN_CLICKED(10, &UAuctionBidDlg::OnDoNothing)
UON_BN_CLICKED(11, &UAuctionBidDlg::OnDoNothing)
UON_BN_CLICKED(12, &UAuctionBidDlg::OnDoNothing)
UON_BN_CLICKED(13, &UAuctionBidDlg::CacelAuction)
UON_BN_CLICKED(28, &UAuctionBidDlg::OnRadioBtn1Clicked)
UON_BN_CLICKED(29, &UAuctionBidDlg::OnRadioBtn2Clicked)
UON_BN_CLICKED(30, &UAuctionBidDlg::OnRadioBtn3Clicked)
UON_BN_CLICKED(54, &UAuctionBidDlg::OnRadioBtn4Clicked)
//UON_BN_CLICKED(7, &UAuctionBidDlg::OnSearYuanliaoBtnClicked)
//UON_BN_CLICKED(8, &UAuctionBidDlg::OnSearQitaBtnClicked)
//UON_BN_CLICKED(14, &UAuctionBidDlg::OnSearQitaBtnClicked)
UEND_MESSAGE_MAP()

void UAuctionBidDlg::StaticInit()
{

}

UAuctionBidDlg::UAuctionBidDlg()
:m_pkBtn(NULL)
,m_FromBugPos(0)
{
	m_ItemGUID = 0;
	m_SaleNpcPrice = 0;
}

UAuctionBidDlg::~UAuctionBidDlg()
{
}

BOOL UAuctionBidDlg::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;
	m_pkBtn = (USkillButton*)GetChildByID(13);
	USkillButton::DataInfo kData;
	kData.id = 0;
	kData.type = UItemSystem::ITEM_DATA_FLAG;
	kData.pos = 0;
	kData.num = 0;

	m_pkBtn->SetStateData(&kData);
	m_pkBtn->SetUIType(UItemSystem::ICT_AUC);

	for(unsigned int ui = 0; ui < 6; ui++)
	{
		m_pkIcon[ui] = (USkillButton*)GetChildByID(7 + ui);
		m_pkIcon[ui]->SetStateData(&kData);
	}

	m_pkBidGold = (UEdit*)GetChildByID(15);
	m_pkBidGold->SetMaxLength(6);
	m_pkBidGold->SetTextAlignment(UT_RIGHT);
	m_pkBidGold->SetNumberOnly(TRUE);
	m_pkBidSilver = (UEdit*)GetChildByID(16);
	m_pkBidSilver->SetMaxLength(2);
	m_pkBidSilver->SetTextAlignment(UT_RIGHT);
	m_pkBidSilver->SetNumberOnly(TRUE);
	m_pkBidBronze = (UEdit*)GetChildByID(17);
	m_pkBidBronze->SetMaxLength(2);
	m_pkBidBronze->SetTextAlignment(UT_RIGHT);
	m_pkBidBronze->SetNumberOnly(TRUE);

	m_pkOnePriceGold = (UEdit*)GetChildByID(18);
	m_pkOnePriceGold->SetMaxLength(6);
	m_pkOnePriceGold->SetTextAlignment(UT_RIGHT);
	m_pkOnePriceGold->SetNumberOnly(TRUE);
	m_pkOnePriceSilver = (UEdit*)GetChildByID(19);
	m_pkOnePriceSilver->SetMaxLength(2);
	m_pkOnePriceSilver->SetTextAlignment(UT_RIGHT);
	m_pkOnePriceSilver->SetNumberOnly(TRUE);
	m_pkOnePriceBronze = (UEdit*)GetChildByID(20);
	m_pkOnePriceBronze->SetMaxLength(2);
	m_pkOnePriceBronze->SetTextAlignment(UT_RIGHT);
	m_pkOnePriceBronze->SetNumberOnly(TRUE);

	m_pkList = (USpecListBox*)GetChildByID(24);
	m_pkList->SetGridSize(UPoint(490 ,43));
	m_pkList->SetGridCount(6, 1);
	m_pkList->SetCol1(8, 0);
	m_pkList->SetWidthCol1(136);
	m_pkList->SetCol2(155, 0);
	m_pkList->SetWidthCol2(40);
	m_pkList->SetCol3(210, 0);
	m_pkList->SetWidthCol3(83);
	m_pkList->SetCol4(302, 0);
	m_pkList->SetWidthCol4(43);
	m_pkList->SetCol5(380, 0);
	m_pkList->SetWidthCol5(109);
	//m_pkList->SetLine1(372, 7);
	//m_pkList->SetWidthLine1(109);
	m_pkList->SetLine1(379, 16);
	//m_pkList->SetWidthLine1(109);
	m_pkList->SetWidthLine1(42);
	m_pkList->SetLine1_1(433, 16);
	m_pkList->SetWidthLine1_1(13);
	m_pkList->SetLine1_2(461, 16);
	m_pkList->SetWidthLine1_2(13);
	m_pkList->SetLine2(372, 22);
	m_pkList->SetWidthLine2(109);

	m_pkList->SetLine1_3(379, 16);
	m_pkList->SetWidthLine1_3(93);
	m_pkList->SetLine2_3(379, 16);
	m_pkList->SetWidthLine2_3(93);

	m_pkRadio[0] = (UButton*)GetChildByID(21);
	m_pkRadio[1] = (UButton*)GetChildByID(22);
	m_pkRadio[1]->SetCheck(TRUE);
	m_pkRadio[2] = (UButton*)GetChildByID(23);
	m_pkRadio[3] = (UButton*)GetChildByID(31);

	m_pkTaxGold = (UStaticText*)GetChildByID(25);
	m_pkTaxGold->SetTextAlign(UT_RIGHT);
	m_pkTaxSilver = (UStaticText*)GetChildByID(26);
	m_pkTaxSilver->SetTextAlign(UT_RIGHT);
	m_pkTaxBronze = (UStaticText*)GetChildByID(27);
	m_pkTaxBronze->SetTextAlign(UT_RIGHT);

	m_pkBitmapJinbi = (UBitmap*)GetChildByID(63);

	m_pkYuaoBao = (UEdit*)GetChildByID(61);
	m_pkOnePriceYuanBao = (UEdit*)GetChildByID(62);

	m_pkBitmapYuanBao = (UBitmap*)GetChildByID(60);

	m_pkBitmapXianYouYuanBao = (UBitmap*)GetChildByID(120);
	m_pkXianYouYuanBao = (UStaticText*)GetChildByID(121);
	m_pkXianYouYuanBao->SetTextAlign(UT_RIGHT);

	m_pkXianYouGold = (UStaticText*)GetChildByID(122);
	m_pkXianYouGold->SetTextAlign(UT_RIGHT);
	m_pkXianYouSliver = (UStaticText*)GetChildByID(123);
	m_pkXianYouBronze = (UStaticText*)GetChildByID(124);

	m_bNetMsgInit = false;
	m_PageIndex = 0;

	m_bYuanbaoPaimai = false;

	m_Price = 0;
	m_SaleNpcPrice = 0;

#ifdef TEST_CLIENT
	GetChildByID(53)->SetActive(FALSE);
#endif

	return TRUE;
}

void UAuctionBidDlg::OnDestroy()
{
	m_Visible = FALSE;
	UDialog::OnDestroy();
	/*AucInfo.stAuctionBidderList.vItems.clear();
	AucInfo.stAuctionList.vItems.clear();
	AucInfo.stAuctionOwnerList.vItems.clear();
	m_pkList->Clear();
	for(unsigned int ui = 0; ui < 6; ui++)
	{
		m_pkIcon[ui]->SetVisible(FALSE);
	}*/
}

void UAuctionBidDlg::OnClose()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionNormal);
}
BOOL UAuctionBidDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}


void UAuctionBidDlg::OnTimer(float fDeltaTime)
{
	if (!m_Visible)
		return;
	UDialog::OnTimer(fDeltaTime);
	if (AuctionMgr)
	{
		AuctionMgr->CheckDis();
	}
}

void UAuctionBidDlg::OnDoNothing()
{

}

void UAuctionBidDlg::CacelAuction()
{
	AuctionMgr->ReleaseOwerItem();
}
void UAuctionBidDlg::OnAuctionBrowse()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionBrowse);
}

void UAuctionBidDlg::OnAuctionBidJinBi()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionBidder_G);
}

void UAuctionBidDlg::OnAuctionBidYuanBao()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionBidder_Y);
}

void UAuctionBidDlg::OnAuctionAuc()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionAuc);
}

void UAuctionBidDlg::OnBeginAucBtnClicked()
{
	if (!m_ItemGUID)
		return;
	CHAR Buf[_MAX_PATH] = {};
	uint32 BidPrice = 0;
	uint32 OnePrice = 0;
	if(m_bYuanbaoPaimai)
	{
		if(m_pkYuaoBao->GetText(Buf, _MAX_PATH))
			BidPrice += atoi(Buf);

		if(m_pkOnePriceYuanBao->GetText(Buf, _MAX_PATH))
			OnePrice += atoi(Buf);
	}
	else
	{
		if(m_pkBidGold->GetText(Buf, _MAX_PATH))
			BidPrice += atoi(Buf) * 10000;

		if(m_pkBidSilver->GetText(Buf, _MAX_PATH))
			BidPrice += atoi(Buf) * 100;

		if(m_pkBidBronze->GetText(Buf, _MAX_PATH))
			BidPrice += atoi(Buf);

		if(m_pkOnePriceGold->GetText(Buf, _MAX_PATH))
			OnePrice += atoi(Buf) * 10000;

		if(m_pkOnePriceSilver->GetText(Buf, _MAX_PATH))
			OnePrice += atoi(Buf) * 100;

		if(m_pkOnePriceBronze->GetText(Buf, _MAX_PATH))
			OnePrice += atoi(Buf);
	}

	if(BidPrice > OnePrice)
	{
		ClientSystemNotify( _TRAN("The starting price is greater than a price, it is automatically set to a price！"));
		//UMessageBox::MsgBox("提示：起拍价格大于一口价，自动设置为一口价！");
		OnePrice = BidPrice;
	}

	if(BidPrice == 0 || OnePrice == 0)
	{
		return;
	}

	uint32 eTime = 60 * 12;
	bool bYP = false;
	if(m_pkRadio[0]->IsChecked())
	{
		bYP = true;
		eTime *= 4;
	}
	else
	{
		if(m_pkRadio[2]->IsChecked())
			eTime *= 2;
		if(m_pkRadio[3]->IsChecked())
			eTime *= 4;
	}

	AuctionMgr->AddAuctionSellItem(m_ItemGUID,BidPrice,OnePrice,eTime,bYP,m_bYuanbaoPaimai);
}


void UAuctionBidDlg::OnCancleAucBtnClicked()
{
	int iCurSel = m_pkList->GetCurSel();

	if(iCurSel == -1)
		return;

	AuctionMgr->RemoveAuctionItem(m_stAuctionOwnerList.vItems[iCurSel].auction_id);
}

void UAuctionBidDlg::OnPrevBtnClicked()
{
	m_PageIndex--;
	if (m_PageIndex <= 0)
	{
		//GetChildByID(5)->SetActive(FALSE);
		m_PageIndex = 0;
	}
	ShowDataInfo();
}

void UAuctionBidDlg::OnNextBtnClicked()
{
	if((m_PageIndex + 1) * 6 < (int)m_stAuctionOwnerList.vItems.size())
	{
		m_PageIndex++;
		
	}else
	{
		//GetChildByID(6)->SetActive(FALSE);
	}
	ShowDataInfo();
}

void UAuctionBidDlg::SetYuanBaoPaimaiFlag(bool bFlag)
{
	m_bYuanbaoPaimai = bFlag;
	if(m_bYuanbaoPaimai)
	{
		m_pkYuaoBao->SetVisible(TRUE);
		m_pkOnePriceYuanBao->SetVisible(TRUE);
		m_pkBitmapYuanBao->SetVisible(TRUE);
		m_pkXianYouYuanBao->SetVisible(TRUE);
		m_pkBitmapXianYouYuanBao->SetVisible(TRUE);
		m_pkBidBronze->SetVisible(FALSE);
		m_pkBidSilver->SetVisible(FALSE);
		m_pkBidGold->SetVisible(FALSE);
		m_pkOnePriceBronze->SetVisible(FALSE);
		m_pkOnePriceSilver->SetVisible(FALSE);
		m_pkOnePriceGold->SetVisible(FALSE);
		m_pkBitmapJinbi->SetVisible(FALSE);
		m_pkXianYouBronze->SetVisible(FALSE);
		m_pkXianYouSliver->SetVisible(FALSE);
		m_pkXianYouGold->SetVisible(FALSE);


		
	}
	else
	{
		m_pkBidBronze->SetVisible(TRUE);
		m_pkBidSilver->SetVisible(TRUE);
		m_pkBidGold->SetVisible(TRUE);
		m_pkOnePriceBronze->SetVisible(TRUE);
		m_pkOnePriceSilver->SetVisible(TRUE);
		m_pkOnePriceGold->SetVisible(TRUE);
		m_pkBitmapJinbi->SetVisible(TRUE);
		m_pkXianYouBronze->SetVisible(TRUE);
		m_pkXianYouSliver->SetVisible(TRUE);
		m_pkXianYouGold->SetVisible(TRUE);
		m_pkYuaoBao->SetVisible(FALSE);
		m_pkOnePriceYuanBao->SetVisible(FALSE);
		m_pkBitmapYuanBao->SetVisible(FALSE);
		m_pkXianYouYuanBao->SetVisible(FALSE);
		m_pkBitmapXianYouYuanBao->SetVisible(FALSE);

		m_pkRadio[1]->SetCheck(TRUE);
	}
}
void UAuctionBidDlg::SetBtnId(ui32 id, ui64 guid, ui32 price, ui32 count, std::string strName)
{
	m_ItemGUID = guid;
	USkillButton::DataInfo pData;
	pData.id = id;
	pData.type = UItemSystem::ITEM_DATA_FLAG;
	pData.pos = 0;
	pData.num = count;
	ItemExtraData* TempData = new ItemExtraData;
	SYItem * Item = (SYItem *)ObjectMgr->GetObject(m_ItemGUID);
	if (Item)
	{
		TempData->jinglian_level = Item->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
		for( int i = 0; i < 6; ++i )
			TempData->enchants[i] =  Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + i * 3 );
		pData.pDataInfo = TempData;
	}
	
	m_pkBtn->SetStateData(&pData);

	//计算保管费用
	m_Price = price;
	ItemPrototype_Client* pItemPro = ItemMgr->GetItemPropertyFromDataBase(id);
	if (pItemPro)
	{
		m_SaleNpcPrice = pItemPro->SellPrice * count;
	}else
	{
		m_SaleNpcPrice = 0;
	}

	int Hour = 12;
	if(m_pkRadio[2]->IsChecked())
		Hour = 24;
	if(m_pkRadio[3]->IsChecked())
		Hour = 48;

	int SalePrice = m_SaleNpcPrice * Hour / (4 * 100);

	int iGold = SalePrice / 10000;
	int iSliver = (SalePrice - iGold * 10000) / 100;
	int iBronze = (SalePrice - iGold * 10000 - iSliver * 100);


	char sz[10];
	NiSprintf(sz, 10, "%d", iGold);
	m_pkTaxGold->SetText(sz);
	NiSprintf(sz, 10, "%d", iSliver);
	m_pkTaxSilver->SetText(sz);
	NiSprintf(sz, 10, "%d", iBronze);
	m_pkTaxBronze->SetText(sz);

	((UStaticText*)GetChildByID(55))->SetText(strName.c_str());


}

//void UAuctionBidDlg::OnNetMessage(MSG_S2C::stAuction_Command_Result stMessage)
//{
//	switch(stMessage.auction_result)
//	{
//	case MSG_S2C::AUCTION_CREATE:
//		{
//			m_pkBtn->SetIconId(0);
//			((UStaticText*)GetChildByID(55))->SetText("");
//			m_pkTaxGold->SetText("0");
//			m_pkTaxSilver->SetText("0");
//			m_pkTaxBronze->SetText("0");
//			m_pkYuaoBao->SetText("0");
//			m_pkOnePriceYuanBao->SetText("0");
//			m_pkBidGold->SetText("0");
//			m_pkBidSilver->SetText("0");
//			m_pkBidBronze->SetText("0");
//			m_pkOnePriceGold->SetText("0");
//			m_pkOnePriceSilver->SetText("0");
//			m_pkOnePriceBronze->SetText("0");
//			m_bNetMsgInit = false;
//			UpdateNetMsg();
//		}
//		break;
//	case MSG_S2C::AUCTION_CANCEL:
//		{
//			m_pkBtn->SetIconId(0);
//			((UStaticText*)GetChildByID(55))->SetText("");
//			m_pkTaxGold->SetText("0");
//			m_pkTaxSilver->SetText("0");
//			m_pkTaxBronze->SetText("0");
//			m_pkYuaoBao->SetText("0");
//			m_pkOnePriceYuanBao->SetText("0");
//			m_pkBidGold->SetText("0");
//			m_pkBidSilver->SetText("0");
//			m_pkBidBronze->SetText("0");
//			m_pkOnePriceGold->SetText("0");
//			m_pkOnePriceSilver->SetText("0");
//			m_pkOnePriceBronze->SetText("0");
//			m_bNetMsgInit = false;
//			UpdateNetMsg();
//			UpdateAllInfo(stMessage.auction_id);
//		}
//		break;
//	case MSG_S2C::AUCTION_BID:
//		{
//			UMessageBox::MsgBox_s("竞价成功！");
//			UpdateAllInfo(stMessage.auction_id);
//		}
//		break;
//	case MSG_S2C::AUCTION_BUYOUT:
//		{
//			UMessageBox::MsgBox_s("一口价拍卖成功！");
//			UpdateAllInfo(stMessage.auction_id);
//		}
//		break;
//	}
//
//	switch(stMessage.auction_error)
//	{
//	case MSG_S2C::AUCTION_ERROR_UNK1:
//		break;
//	case MSG_S2C::AUCTION_ERROR_INTERNAL:
//		break;
//	case MSG_S2C::AUCTION_ERROR_MONEY:
//		{
//			SYState()->UI->GetUItemSystem()->AucToBag();
//			UMessageBox::MsgBox_s("金钱不够！");
//		}
//		break;
//	case MSG_S2C::AUCTION_ERROR_ITEM:
//		{
//			UMessageBox::MsgBox_s("该物品不可以拍卖！");
//		}
//		break;
//	}
//}

void UAuctionBidDlg::SetMoney(int num)
{
	int iBidGold = num / 10000;
	int iBidSliver = (num - iBidGold * 10000) / 100;
	int iBidBronze = (num - iBidGold * 10000 - iBidSliver * 100);

	char sz[256];
	NiSprintf(sz, 256, "%d", iBidBronze);
	m_pkXianYouBronze->SetText(sz);
	NiSprintf(sz, 256, "%d", iBidSliver);
	m_pkXianYouSliver->SetText(sz);
	NiSprintf(sz, 256, "%d", iBidGold);
	m_pkXianYouGold->SetText(sz);
}
void UAuctionBidDlg::SetYuanbao(int num)
{
	char sz[256];
	NiSprintf(sz, 256, "%d", num);
	m_pkXianYouYuanBao->SetText(sz);
}
void UAuctionBidDlg::ClearData()
{
	m_pkBtn->SetIconId(0);
	((UStaticText*)GetChildByID(55))->SetText("");
	m_pkTaxGold->SetText("0");
	m_pkTaxSilver->SetText("0");
	m_pkTaxBronze->SetText("0");
	m_pkYuaoBao->SetText("0");
	m_pkOnePriceYuanBao->SetText("0");
	m_pkBidGold->SetText("0");
	m_pkBidSilver->SetText("0");
	m_pkBidBronze->SetText("0");
	m_pkOnePriceGold->SetText("0");
	m_pkOnePriceSilver->SetText("0");
	m_pkOnePriceBronze->SetText("0");
	m_bNetMsgInit = false;
	m_SaleNpcPrice = 0;
}
void UAuctionBidDlg::SetCheckState(int state)
{
	UButton* AucBtn = UDynamicCast(UButton,GetChildByID(2)) ;
	UButton* BrowseBtn = UDynamicCast(UButton, GetChildByID(0));
	UButton* Bidder_GBtn =  UDynamicCast(UButton,GetChildByID(1));
	UButton* Bidder_YBtn =  UDynamicCast(UButton,GetChildByID(53));

	switch(state)
	{
	case AuctionManager::AuctionAuc:
		AucBtn->SetCheckState(TRUE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	case AuctionManager::AuctionBrowse:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(TRUE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	case AuctionManager::AuctionBidder_G:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(TRUE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	case AuctionManager::AuctionBidder_Y:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(TRUE);
		break;
	default:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	}
}
void UAuctionBidDlg::ShowDataInfo()
{
	m_pkList->Clear();
	for(unsigned int ui = 0; ui < 6; ui++)
	{

		m_pkIcon[ui]->SetIconId(0);
		m_pkIcon[ui]->SetVisible(FALSE);
	}

	if (m_PageIndex == 0)
	{
		GetChildByID(5)->SetActive(FALSE);
	}else
	{
		GetChildByID(5)->SetActive(TRUE);
	}

	int PageNum = m_stAuctionOwnerList.vItems.size() / 6 ;
	if (m_stAuctionOwnerList.vItems.size() % 6)
	{
		PageNum++;
	}
	if (m_PageIndex >= PageNum -1)
	{
		GetChildByID(6)->SetActive(FALSE);
	}else
	{
		GetChildByID(6)->SetActive(TRUE);
	}


	if (m_stAuctionOwnerList.vItems.size() == 0)
	{	
		m_PageIndex = 0;
		return;
	}
	
	int iStart = m_PageIndex * 6;
	int iEnd = (iStart + 6) > (int)m_stAuctionOwnerList.vItems.size() ? (int)m_stAuctionOwnerList.vItems.size() : (iStart + 6);
	int iButtonIndex = 0;
	for(int ui = iStart; ui < iEnd; ui++)
	{
		
			ItemPrototype_Client* pkItem = ItemMgr->GetItemPropertyFromDataBase(m_stAuctionOwnerList.vItems[ui].entryid);
			NIASSERT(pkItem);
			string strItemName = pkItem->C_name;
			char sInfo[512];
			int iBidGold = m_stAuctionOwnerList.vItems[ui].HighestBid / 10000;
			int iBidSliver = (m_stAuctionOwnerList.vItems[ui].HighestBid - iBidGold * 10000) / 100;
			int iBidBronze = (m_stAuctionOwnerList.vItems[ui].HighestBid - iBidGold * 10000 - iBidSliver * 100);
			int iOnePriceGold = m_stAuctionOwnerList.vItems[ui].BuyoutPrice / 10000;
			int iOnePriceSliver = (m_stAuctionOwnerList.vItems[ui].BuyoutPrice - iOnePriceGold * 10000) / 100;
			int iOnePriceBronze = (m_stAuctionOwnerList.vItems[ui].BuyoutPrice - iOnePriceGold * 10000 - iOnePriceSliver * 100);
			uint32 uTime = m_stAuctionOwnerList.vItems[ui].timeleft / 1000;
			int iHour = uTime / 3600;
			int iMin = (uTime - iHour * 3600) / 60;
			int iSec = (uTime - iHour * 3600 - iMin * 60);
			if(m_stAuctionOwnerList.vItems[ui].yp)
			{
				if(m_stAuctionOwnerList.vItems[ui].is_yuanbao)
				{
					NiSprintf(sInfo, 512, "<YPimg><color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end_right><line1_3>%d<end_right><YuanBaoimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_stAuctionOwnerList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						m_stAuctionOwnerList.vItems[ui].HighestBid);
				}
				else
				{
					NiSprintf(sInfo, 512, "<YPimg><color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end><Col5><center><line1>%5d<end><Goldimg><line1_1>%2d<end><Silverimg><line1_2>%2d<end><Broneimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_stAuctionOwnerList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						iBidGold, iBidSliver, iBidBronze);
				}
			}
			else
			{
				if(m_stAuctionOwnerList.vItems[ui].is_yuanbao)
				{
					NiSprintf(sInfo, 512, "<color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end_right><line1_3>%d<end_right><YuanBaoimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_stAuctionOwnerList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						m_stAuctionOwnerList.vItems[ui].HighestBid);
				}
				else
				{
					NiSprintf(sInfo, 512, "<color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end><Col5><center><line1>%5d<end><Goldimg><line1_1>%2d<end><Silverimg><line1_2>%2d<end><Broneimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_stAuctionOwnerList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						iBidGold, iBidSliver, iBidBronze);
				}

			}

			m_pkList->AddItem(sInfo);

			USkillButton::DataInfo newItem;
			newItem.id = m_stAuctionOwnerList.vItems[ui].entryid;
			newItem.num = m_stAuctionOwnerList.vItems[ui].stack_count;
			newItem.type = UItemSystem::ITEM_DATA_FLAG;
			newItem.pDataInfo = &m_stAuctionOwnerList.vItems[ui].extra;

			m_pkIcon[iButtonIndex]->SetStateData(&newItem);
			newItem.pDataInfo = NULL;

			//m_pkIcon[iButtonIndex]->SetIconId(m_stAuctionOwnerList.vItems[ui].entryid);
			//m_pkIcon[iButtonIndex]->SetNum(m_stAuctionOwnerList.vItems[ui].stack_count);
			m_pkIcon[iButtonIndex]->SetVisible(TRUE);
			iButtonIndex++;
		
	}

}
void UAuctionBidDlg::SetOwerData(stAuctionOwer Data)
{
	//
	m_pkList->Clear();
	m_stAuctionOwnerList = Data ;
	ShowDataInfo();
}
void UAuctionBidDlg::SetBugPos(ui8 pos)
{
	m_FromBugPos = pos;
}

void UAuctionBidDlg::OnRadioBtn1Clicked()
{
	m_pkRadio[1]->SetCheck(TRUE);
	uint32 price = m_SaleNpcPrice * 3 / 100;
	int iGold = price / 10000;
	int iSliver = (price - iGold * 10000) / 100;
	int iBronze = (price - iGold * 10000 - iSliver * 100);

	
	char sz[10];
	NiSprintf(sz, 10, "%d", iGold);
	m_pkTaxGold->SetText(sz);
	NiSprintf(sz, 10, "%d", iSliver);
	m_pkTaxSilver->SetText(sz);
	NiSprintf(sz, 10, "%d", iBronze);
	m_pkTaxBronze->SetText(sz);
}

void UAuctionBidDlg::OnRadioBtn2Clicked()
{
	m_pkRadio[2]->SetCheck(TRUE);
	//计算保管费用
	uint32 price = m_SaleNpcPrice * 6 /100 ;
	int iGold = price / 10000;
	int iSliver = (price - iGold * 10000) / 100;
	int iBronze = (price - iGold * 10000 - iSliver * 100);

	

	char sz[10];
	NiSprintf(sz, 10, "%d", iGold);
	m_pkTaxGold->SetText(sz);
	NiSprintf(sz, 10, "%d", iSliver);
	m_pkTaxSilver->SetText(sz);
	NiSprintf(sz, 10, "%d", iBronze);
	m_pkTaxBronze->SetText(sz);
}

void UAuctionBidDlg::OnRadioBtn3Clicked()
{
	m_pkRadio[3]->SetCheck(TRUE);
	//计算保管费用
	uint32 price = m_SaleNpcPrice * 12 / 100;
	int iGold = price / 10000;
	int iSliver = (price - iGold * 10000) / 100;
	int iBronze = (price - iGold * 10000 - iSliver * 100);


	char sz[10];
	NiSprintf(sz, 10, "%d", iGold);
	m_pkTaxGold->SetText(sz);
	NiSprintf(sz, 10, "%d", iSliver);
	m_pkTaxSilver->SetText(sz);
	NiSprintf(sz, 10, "%d", iBronze);
	m_pkTaxBronze->SetText(sz);
}

void UAuctionBidDlg::OnRadioBtn4Clicked()
{
	if(m_bYuanbaoPaimai)
	{
		// YP
		m_pkRadio[0]->SetCheck(TRUE);
	}
}