#ifndef __AuctionBig_H__
#define __AuctionBig_H__

#include "UInc.h"
#include "USpecListBox.h"
#include "USkillButton.h"
#include "UAuctionInc.h"

class UAuctionBidDlg : public UDialog
{
	UDEC_CLASS(UAuctionBidDlg);
	UDEC_MESSAGEMAP();
public:
	UAuctionBidDlg();
	~UAuctionBidDlg();

	void SetBugPos(ui8 pos);
	void SetBtnId(ui32 id, ui64 guid, ui32 price, ui32 count, std::string strName);
	ui64 GetItemGuid(){return m_ItemGUID;}

public:
	void SetOwerData(stAuctionOwer Data); //��ʾ����
	void SetYuanBaoPaimaiFlag(bool bFlag);
	void ShowDataInfo();
	void SetCheckState(int state);
	void ClearData();
	void SetMoney(int num);
	void SetYuanbao(int num);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);

	virtual void OnClose();
	virtual BOOL OnEscape();


	void OnAuctionBrowse();
	void OnAuctionBidJinBi();
	void OnAuctionBidYuanBao();
	void OnAuctionAuc();

	void OnBeginAucBtnClicked();
	void OnCancleAucBtnClicked();
	void OnPrevBtnClicked();
	void OnNextBtnClicked();

	void OnRadioBtn1Clicked();
	void OnRadioBtn2Clicked();
	void OnRadioBtn3Clicked();
	void OnRadioBtn4Clicked();

	void OnDoNothing();
	void CacelAuction();

	UEdit* m_pkBidGold;
	UEdit* m_pkBidSilver;
	UEdit* m_pkBidBronze;

	UEdit* m_pkOnePriceGold;
	UEdit* m_pkOnePriceSilver;
	UEdit* m_pkOnePriceBronze;

	UStaticText* m_pkTaxGold;
	UStaticText* m_pkTaxSilver;
	UStaticText* m_pkTaxBronze;

	UEdit* m_pkYuaoBao;
	UEdit* m_pkOnePriceYuanBao;

	UBitmap* m_pkBitmapYuanBao;
	UBitmap* m_pkBitmapJinbi;

	UStaticText* m_pkXianYouGold;
	UStaticText* m_pkXianYouSliver;
	UStaticText* m_pkXianYouBronze;

	UStaticText* m_pkXianYouYuanBao;
	UBitmap* m_pkBitmapXianYouYuanBao;

	UButton* m_pkRadio[4];

	USkillButton* m_pkBtn;
	USkillButton* m_pkIcon[6];
	ui64 m_ItemGUID;

	USpecListBox* m_pkList;

	ui32 m_Price;
	ui32 m_SaleNpcPrice ;
	ui8 m_FromBugPos;
	bool m_bNetMsgInit;
	int m_PageIndex;
	bool m_bYuanbaoPaimai;
	stAuctionOwer m_stAuctionOwnerList;
};
#endif