#include "stdafx.h"
#include "UAuctionBrowse.h"
#include "../ObjectManager.h"
#include "../Network/PacketBuilder.h"
#include "../ItemManager.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "UMessageBox.h"
#include "UChat.h"
#include "UAuctionMgr.h"
#include "UFun.h"

UIMP_CLASS(UAuctionBrowseDlg, UDialog);

UBEGIN_MESSAGE_MAP(UAuctionBrowseDlg, UDialog)
UON_BN_CLICKED(0, &UAuctionBrowseDlg::OnAuctionBrowse)
UON_BN_CLICKED(1, &UAuctionBrowseDlg::OnAuctionBidJinBi)
UON_BN_CLICKED(53, &UAuctionBrowseDlg::OnAuctionBidYuanBao)
UON_BN_CLICKED(2, &UAuctionBrowseDlg::OnAuctionAuc)
UON_BN_CLICKED(3, &UAuctionBrowseDlg::OnSearWuqiBtnClicked)
UON_BN_CLICKED(4, &UAuctionBrowseDlg::OnSearFangjuBtnClicked)
UON_BN_CLICKED(5, &UAuctionBrowseDlg::OnSearJichuBtnClicked)
UON_BN_CLICKED(6, &UAuctionBrowseDlg::OnSearXiaohaoBtnClicked)
UON_BN_CLICKED(7, &UAuctionBrowseDlg::OnSearYuanliaoBtnClicked)
UON_BN_CLICKED(8, &UAuctionBrowseDlg::OnSearQitaBtnClicked)
UON_CBOX_SELCHANGE(13, &UAuctionBrowseDlg::OnRarityClicked)
UON_BN_CLICKED(14, &UAuctionBrowseDlg::OnSearBtnClicked)
UON_BN_CLICKED(21, &UAuctionBrowseDlg::OnBidBtnClicked)
UON_BN_CLICKED(22, &UAuctionBrowseDlg::OnOnePriceBtnClicked)
UON_BN_CLICKED(23, &UAuctionBrowseDlg::OnPrevBtnClicked)
UON_BN_CLICKED(24, &UAuctionBrowseDlg::OnNextBtnClicked)
UON_BN_CLICKED(25, &UAuctionBrowseDlg::OnDoNothing)
UON_BN_CLICKED(26, &UAuctionBrowseDlg::OnDoNothing)
UON_BN_CLICKED(27, &UAuctionBrowseDlg::OnDoNothing)
UON_BN_CLICKED(28, &UAuctionBrowseDlg::OnDoNothing)
UON_BN_CLICKED(29, &UAuctionBrowseDlg::OnDoNothing)
UON_BN_CLICKED(30, &UAuctionBrowseDlg::OnDoNothing)
UON_UGD_CLICKED(9, &UAuctionBrowseDlg::OnGridClick)
UEND_MESSAGE_MAP()

void UAuctionBrowseDlg::StaticInit()
{

}

UAuctionBrowseDlg::UAuctionBrowseDlg()
:m_pkList(NULL)
{
	m_bSelJinBi = true;
}

UAuctionBrowseDlg::~UAuctionBrowseDlg()
{
}

BOOL UAuctionBrowseDlg::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}

	m_pkList = (USpecListBox*)GetChildByID(9);
	if(m_pkList == NULL)
		return FALSE;
	m_bCanDrag = FALSE;

	m_pkList->SetGridSize(UPoint(490 ,43));
	m_pkList->SetGridCount(6, 1);
	m_pkList->SetCol1(8, 0);
	m_pkList->SetWidthCol1(136);
	m_pkList->SetCol2(155, 0);
	m_pkList->SetWidthCol2(40);
	m_pkList->SetCol3(210, 0);
	m_pkList->SetWidthCol3(83);
	m_pkList->SetCol4(302, 0);
	m_pkList->SetWidthCol4(43);
	m_pkList->SetCol5(380, 0);
	m_pkList->SetWidthCol5(109);
	//m_pkList->SetLine1(372, 7);
	m_pkList->SetLine1(378, 7);
	//m_pkList->SetWidthLine1(109);
	m_pkList->SetWidthLine1(42);
	m_pkList->SetLine1_1(433, 7);
	m_pkList->SetWidthLine1_1(13);
	m_pkList->SetLine1_2(460, 7);
	m_pkList->SetWidthLine1_2(13);
	//m_pkList->SetLine2(372, 22);
	m_pkList->SetLine2(378, 22);
	//m_pkList->SetWidthLine2(109);
	m_pkList->SetWidthLine2(42);
	m_pkList->SetLine2_1(433, 22);
	m_pkList->SetWidthLine2_1(13);
	m_pkList->SetLine2_2(460, 22);
	m_pkList->SetWidthLine2_2(13);
	m_pkList->SetLine1_3(378, 7);
	m_pkList->SetWidthLine1_3(93);
	m_pkList->SetLine2_3(378, 22);
	m_pkList->SetWidthLine2_3(93);

	m_pkList->SetCol5(294 + 8 + 41, 22);
	m_pkList->SetWidthCol5(37);

	m_pkSearName = (UEdit*)GetChildByID(10);
	m_pkSearName->SetMaxLength(10);
	m_pkSearLevelLower = (UEdit*)GetChildByID(11);
	m_pkSearLevelLower->SetMaxLength(3);
	m_pkSearLevelUpper = (UEdit*)GetChildByID(12);
	m_pkSearLevelUpper->SetMaxLength(3);
	//m_pkSearQuality = (UEdit*)GetChildByID(13);
	//m_pkSearQuality->SetMaxLength(8);
	m_pkSearQuality = (UComboBox*)GetChildByID(13);
	m_pkSearQuality->AddItem(_TRAN("All"));
	m_pkSearQuality->AddItem(_TRAN("Useless"));
	m_pkSearQuality->AddItem(_TRAN("Normal"));
	m_pkSearQuality->AddItem(_TRAN("Superior"));
	m_pkSearQuality->AddItem(_TRAN("Excellent"));
	m_pkSearQuality->AddItem(_TRAN("Epic"));
	m_pkSearQuality->AddItem(_TRAN("Legendary"));
	m_iSearQuality = -1;

	m_pkGold = (UStaticText*)GetChildByID(15);
	m_pkGold->SetTextAlign(UT_RIGHT);
	m_pkSilver = (UStaticText*)GetChildByID(16);
	m_pkSilver->SetTextAlign(UT_RIGHT);
	m_pkBronze = (UStaticText*)GetChildByID(17);
	m_pkBronze->SetTextAlign(UT_RIGHT);
	m_pkJinBi = (UBitmap*)GetChildByID(35);

	m_pkOnePriceGold = (UEdit*)GetChildByID(18);
	m_pkOnePriceGold->SetMaxLength(6);
	m_pkOnePriceGold->SetTextAlignment(UT_RIGHT);
	m_pkOnePriceGold->SetNumberOnly(TRUE);
	m_pkOnePriceSilver = (UEdit*)GetChildByID(19);
	m_pkOnePriceSilver->SetMaxLength(2);
	m_pkOnePriceSilver->SetTextAlignment(UT_RIGHT);
	m_pkOnePriceSilver->SetNumberOnly(TRUE);
	m_pkOnePriceBronze = (UEdit*)GetChildByID(20);
	m_pkOnePriceBronze->SetMaxLength(2);
	m_pkOnePriceBronze->SetTextAlignment(UT_RIGHT);
	m_pkOnePriceBronze->SetNumberOnly(TRUE);
	m_pkOnePriceJinBi = (UBitmap*)GetChildByID(36);

	m_pkYuanBao = (UStaticText*)GetChildByID(31);
	m_pkYuanBao->SetTextAlign(UT_RIGHT);

	m_pkOnePriceYuanBao = (UEdit*)GetChildByID(32);
	m_pkOnePriceYuanBao->SetTextAlignment(UT_RIGHT);
	m_pkOnePriceYuanBao->SetNumberOnly(TRUE);
	m_pkOnePriceYuanBao->SetMaxLength(6);

	m_pkBitmapYuanBao = (UBitmap*)GetChildByID(33);
	m_pkBitmapOnePriceYuanBao = (UBitmap*)GetChildByID(34);

	USkillButton::DataInfo kData;
	kData.id = 0;
	kData.type = UItemSystem::ITEM_DATA_FLAG;
	kData.pos = 0;
	kData.num = 0;
	for(unsigned int ui = 0; ui < 6; ui++)
	{
		m_pkIcon[ui] = (USkillButton*)GetChildByID(25 + ui);
		m_pkIcon[ui]->SetStateData(&kData);
	}

	//m_bCanDrag = true;

	m_PageIndex = 0;
	m_iSearType = -1;

#ifdef TEST_CLIENT
	GetChildByID(53)->SetActive(FALSE);
#endif

	return TRUE;
}

void UAuctionBrowseDlg::OnDestroy()
{
	m_Visible = FALSE;
	UDialog::OnDestroy();
	m_pkSearQuality->Clear();
	for(unsigned int ui = 0; ui < 6; ui++)
	{
		m_pkIcon[ui]->SetVisible(FALSE);
	}
	if (m_pkSearQuality)
	{
		for (int i=0; i < m_pkSearQuality->GetCount(); i++)
		{
			void* pData = m_pkSearQuality->GetItemData(i);
			if (pData)
			{
				delete pData ;
				pData = NULL;
			}
		}
		m_pkSearQuality->Clear();
	}

}
void UAuctionBrowseDlg::OnTimer(float fDeltaTime)
{
	UDialog::OnTimer(fDeltaTime);
	if (AuctionMgr && IsVisible())
	{
		AuctionMgr->CheckDis();
	}
}
void UAuctionBrowseDlg::OnClose()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionNormal);
}

BOOL UAuctionBrowseDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}

void UAuctionBrowseDlg::OnDoNothing()
{

}

void UAuctionBrowseDlg::OnGridClick()
{
	int iSel = m_pkList->GetCurSel();
	if(iSel != -1 && m_AuctionList.vItems.size() > iSel)
	{
		if(m_AuctionList.vItems[iSel].is_yuanbao)
		{
			m_bSelJinBi = false;
			ui32 uiYuanBao = m_AuctionList.vItems[iSel].HighestBid + 1;
			char sz[256];
			NiSprintf(sz, 256, "%d", uiYuanBao);
			m_pkOnePriceYuanBao->SetText(sz);
			if(m_pkYuanBao->IsVisible() == FALSE)
				m_pkYuanBao->SetVisible(TRUE);

			if(m_pkOnePriceYuanBao->IsVisible() == FALSE)
				m_pkOnePriceYuanBao->SetVisible(TRUE);

			if(m_pkBitmapOnePriceYuanBao->IsVisible() == FALSE)
				m_pkBitmapOnePriceYuanBao->SetVisible(TRUE);

			if(m_pkBitmapYuanBao->IsVisible() == FALSE)
				m_pkBitmapYuanBao->SetVisible(TRUE);

			if(m_pkOnePriceBronze->IsVisible() == TRUE)
				m_pkOnePriceBronze->SetVisible(FALSE);

			if(m_pkOnePriceSilver->IsVisible() == TRUE)
				m_pkOnePriceSilver->SetVisible(FALSE);

			if(m_pkOnePriceGold->IsVisible() == TRUE)
				m_pkOnePriceGold->SetVisible(FALSE);

			if(m_pkJinBi->IsVisible() == TRUE)
				m_pkJinBi->SetVisible(FALSE);

			if(m_pkOnePriceJinBi->IsVisible() == TRUE)
				m_pkOnePriceJinBi->SetVisible(FALSE);
		}
		else
		{
			m_bSelJinBi = true;
			int iBidGold = m_AuctionList.vItems[iSel].HighestBid / 10000;
			int iBidSliver = (m_AuctionList.vItems[iSel].HighestBid - iBidGold * 10000) / 100;
			int iBidBronze = (m_AuctionList.vItems[iSel].HighestBid - iBidGold * 10000 - iBidSliver * 100) + 1;

			char sz[256];
			if(iBidBronze)
			{
				NiSprintf(sz, 256, "%d", iBidBronze);
				m_pkOnePriceBronze->SetText(sz);
			}
			else
			{
				m_pkOnePriceBronze->Clear();
			}
			if(m_pkOnePriceBronze->IsVisible() == FALSE)
				m_pkOnePriceBronze->SetVisible(TRUE);
			if(iBidSliver)
			{
				NiSprintf(sz, 256, "%d", iBidSliver);
				m_pkOnePriceSilver->SetText(sz);
			}
			else
			{
				m_pkOnePriceSilver->Clear();
			}
			if(m_pkOnePriceSilver->IsVisible() == FALSE)
				m_pkOnePriceSilver->SetVisible(TRUE);
			if(iBidGold)
			{
				NiSprintf(sz, 256, "%d", iBidGold);
				m_pkOnePriceGold->SetText(sz);
			}
			else
			{
				m_pkOnePriceGold->Clear();
			}
			if(m_pkOnePriceGold->IsVisible() == FALSE)
				m_pkOnePriceGold->SetVisible(TRUE);

			if(m_pkJinBi->IsVisible() == FALSE)
				m_pkJinBi->SetVisible(TRUE);

			if(m_pkOnePriceJinBi->IsVisible() == FALSE)
				m_pkOnePriceJinBi->SetVisible(TRUE);

			//
			if(m_pkYuanBao->IsVisible() == TRUE)
				m_pkYuanBao->SetVisible(FALSE);

			if(m_pkOnePriceYuanBao->IsVisible() == TRUE)
				m_pkOnePriceYuanBao->SetVisible(FALSE);

			if(m_pkBitmapOnePriceYuanBao->IsVisible() == TRUE)
				m_pkBitmapOnePriceYuanBao->SetVisible(FALSE);

			if(m_pkBitmapYuanBao->IsVisible() == TRUE)
				m_pkBitmapYuanBao->SetVisible(FALSE);
		}
	}
}

void UAuctionBrowseDlg::OnRarityClicked()
{
	if(m_pkSearQuality)
	{
		m_iSearQuality = m_pkSearQuality->GetCurSel();
		m_iSearQuality -= 1;
	}
}

void UAuctionBrowseDlg::OnAuctionBrowse()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionBrowse);
}

void UAuctionBrowseDlg::OnAuctionBidJinBi()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionBidder_G);
}

void UAuctionBrowseDlg::OnAuctionBidYuanBao()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionBidder_Y);
}

void UAuctionBrowseDlg::OnAuctionAuc()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionAuc);
}

void UAuctionBrowseDlg::OnSearWuqiBtnClicked()
{
	m_iSearType = (int)ITEM_CLASS_WEAPON;
	Search(m_iSearType);
}

void UAuctionBrowseDlg::OnSearFangjuBtnClicked()
{
	m_iSearType = (int)ITEM_CLASS_ARMOR;
	Search(m_iSearType);
}

void UAuctionBrowseDlg::OnSearJichuBtnClicked()
{
	m_iSearType = (int)ITEM_CLASS_REAGENT;
	Search(m_iSearType);
}

void UAuctionBrowseDlg::OnSearXiaohaoBtnClicked()
{
	m_iSearType = (int)ITEM_CLASS_CONSUMABLE;
	Search(m_iSearType);
}

void UAuctionBrowseDlg::OnSearYuanliaoBtnClicked()
{
	m_iSearType = (int)ITEM_CLASS_JEWELRY;
	Search(m_iSearType);
}

void UAuctionBrowseDlg::OnSearQitaBtnClicked()
{
	m_iSearType = (int)ITEM_CLASS_MISCELLANEOUS;
	Search(m_iSearType);
}

void UAuctionBrowseDlg::OnSearBtnClicked()
{
	Search(m_iSearType);
}

void UAuctionBrowseDlg::OnBidBtnClicked()
{
	int iCurSel = m_pkList->GetCurSel();

	if(iCurSel == -1)
		return;

	NIASSERT(iCurSel < (int)m_AuctionList.vItems.size());

	CHAR Buf[10] = {};
	uint32 BidPrice = 0 ;// m_AuctionList.vItems[iCurSel].BuyoutPrice;

	if(m_AuctionList.vItems[iCurSel].is_yuanbao)
	{
		if(m_pkOnePriceYuanBao->GetText(Buf, 10))
			BidPrice += atoi(Buf);
	}
	else
	{
		if(m_pkOnePriceGold->GetText(Buf, 10))
			BidPrice += atoi(Buf) * 10000;

		if(m_pkOnePriceSilver->GetText(Buf, 10))
			BidPrice += atoi(Buf) * 100;

		if(m_pkOnePriceBronze->GetText(Buf, 10))
			BidPrice += atoi(Buf);
	}

	// 是否有足够的钱，是否大与目前的最高竞价
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLocalPlayer);
	if (pkLocalPlayer->GetGUID() ==  m_AuctionList.vItems[iCurSel].owner_guid)
	{
		UMessageBox::MsgBox_s(_TRAN("You can not buy their own items！"));
		return ;
	}
	BidPrice = BidPrice > m_AuctionList.vItems[iCurSel].HighestBid + 1 ? BidPrice :m_AuctionList.vItems[iCurSel].HighestBid + 1;
	if(m_AuctionList.vItems[iCurSel].is_yuanbao)
	{
		if(pkLocalPlayer->GetYuanBao() < BidPrice)
		{
			UMessageBox::MsgBox_s(_TRAN("Insufficient Obelisks！"));
			return;
		}
	}
	else
	{
		if(pkLocalPlayer->GetMoney() < BidPrice)
		{
			UMessageBox::MsgBox_s(_TRAN("insufficient Money！"));
			return ;
		}
	}

AuctionMgr->BuyAuction(m_AuctionList.vItems[iCurSel].auction_id,BidPrice );
	//PacketBuilder->SendAuctionPlaceBidMsg(
	//	AucInfo.Vendorguid,
	//	AucInfo.stAuctionList.vItems[iCurSel].auction_id,
	//	BidPrice);
	
}

void UAuctionBrowseDlg::OnOnePriceBtnClicked()
{
	int iCurSel = m_pkList->GetCurSel();

	if(iCurSel == -1)
		return;

	

	NIASSERT(iCurSel < (int)m_AuctionList.vItems.size());

	// 是否有足够的钱，是否大与一口价
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLocalPlayer);
	if (pkLocalPlayer->GetGUID() ==  m_AuctionList.vItems[iCurSel].owner_guid)
	{
		UMessageBox::MsgBox_s(_TRAN("You can not buy their own items！"));
		return ;
	}
	if (m_AuctionList.vItems[iCurSel].is_yuanbao)
	{
		if(pkLocalPlayer->GetYuanBao() < m_AuctionList.vItems[iCurSel].BuyoutPrice)
		{
			UMessageBox::MsgBox_s(_TRAN("Insufficient Obelisks！"));
			return ;
		}
	}else
	{
		if(pkLocalPlayer->GetMoney() < m_AuctionList.vItems[iCurSel].BuyoutPrice)
		{
			UMessageBox::MsgBox_s(_TRAN("Insufficient Money！"));
			return ;
		}
	}
	
	AuctionMgr->BuyAuction(m_AuctionList.vItems[iCurSel].auction_id,m_AuctionList.vItems[iCurSel].BuyoutPrice, true);
	//PacketBuilder->SendAuctionPlaceBidMsg(
	//	AucInfo.Vendorguid,
	//	AucInfo.stAuctionList.vItems[iCurSel].auction_id,
	//	//OnePrice);
	//	AucInfo.stAuctionList.vItems[iCurSel].BuyoutPrice);
}

void UAuctionBrowseDlg::OnPrevBtnClicked()
{
	m_PageIndex--;
	if(m_PageIndex < 0)
	{
		m_PageIndex = 0;
	}
	Search(m_iSearType);
}

void UAuctionBrowseDlg::OnNextBtnClicked()
{
	m_PageIndex++;
	Search(m_iSearType);
}


void UAuctionBrowseDlg::SetBrowseData(stAuctionList AuctionList)
{
	m_pkList->Clear();
	m_AuctionList = AuctionList;
	ShowData();
}
void UAuctionBrowseDlg::SetMoney(int num)
{
	int iBidGold = num / 10000;
	int iBidSliver = (num - iBidGold * 10000) / 100;
	int iBidBronze = (num - iBidGold * 10000 - iBidSliver * 100);

	char sz[256];
	NiSprintf(sz, 256, "%d", iBidBronze);
	m_pkBronze->SetText(sz);
	NiSprintf(sz, 256, "%d", iBidSliver);
	m_pkSilver->SetText(sz);
	NiSprintf(sz, 256, "%d", iBidGold);
	m_pkGold->SetText(sz);
}
void UAuctionBrowseDlg::SetYuanbao(int num)
{
	char sz[256];
	NiSprintf(sz, 256, "%d", num);
	m_pkYuanBao->SetText(sz);

}
void UAuctionBrowseDlg::SetCheckWuQi()
{
	UButton* pkWuqi = UDynamicCast(UButton, GetChildByID(3));
	if (pkWuqi)
	{
		pkWuqi->SetCheck(TRUE);
	}
}
void UAuctionBrowseDlg::ReSeachType()
{
	Search(m_iSearType);
}
void UAuctionBrowseDlg::SetCheckState(int state)
{
	UButton* AucBtn = UDynamicCast(UButton,GetChildByID(2)) ;
	UButton* BrowseBtn = UDynamicCast(UButton, GetChildByID(0));
	UButton* Bidder_GBtn =  UDynamicCast(UButton,GetChildByID(1));
	UButton* Bidder_YBtn =  UDynamicCast(UButton,GetChildByID(53));

	switch(state)
	{
	case AuctionManager::AuctionAuc:
		AucBtn->SetCheckState(TRUE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	case AuctionManager::AuctionBrowse:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(TRUE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	case AuctionManager::AuctionBidder_G:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(TRUE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	case AuctionManager::AuctionBidder_Y:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(TRUE);
		break;
	default:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	}
}
void UAuctionBrowseDlg::ShowData()
{
	m_pkList->Clear();

	for(unsigned int ui = 0; ui < 6; ui++)
	{
		m_pkIcon[ui]->SetIconId(0);
		m_pkIcon[ui]->SetVisible(FALSE);
	}
	if (m_AuctionList.vItems.size() == 0)
	{
		m_PageIndex = 0;
		return;
	}


	int iEnd =  6 > (int)m_AuctionList.vItems.size() ? (int)m_AuctionList.vItems.size() : 6;
	int iButtonIndex = 0;
	for(unsigned int ui = 0; ui < iEnd; ui++)
	{
		
			ItemPrototype_Client* pkItem = ItemMgr->GetItemPropertyFromDataBase(m_AuctionList.vItems[ui].entryid);
			NIASSERT(pkItem);
			string strItemName = pkItem->C_name;
			char sInfo[512];
			int iBidGold = m_AuctionList.vItems[ui].HighestBid / 10000;
			int iBidSliver = (m_AuctionList.vItems[ui].HighestBid - iBidGold * 10000) / 100;
			int iBidBronze = (m_AuctionList.vItems[ui].HighestBid - iBidGold * 10000 - iBidSliver * 100);
			int iOnePriceGold = m_AuctionList.vItems[ui].BuyoutPrice / 10000;
			int iOnePriceSliver = (m_AuctionList.vItems[ui].BuyoutPrice - iOnePriceGold * 10000) / 100;
			int iOnePriceBronze = (m_AuctionList.vItems[ui].BuyoutPrice - iOnePriceGold * 10000 - iOnePriceSliver * 100);
			uint32 uTime = m_AuctionList.vItems[ui].timeleft / 1000;
			int iHour = uTime / 3600;
			int iMin = (uTime - iHour * 3600) / 60;
			int iSec = (uTime - iHour * 3600 - iMin * 60);
			if(m_AuctionList.vItems[ui].yp)
			{
				if(m_AuctionList.vItems[ui].is_yuanbao)
				{
					NiSprintf(sInfo, 512, "<YPimg><color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end><line1_3>%d<end_right><YuanBaoimg><line2_3>%d<end_right><YuanBaoimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_AuctionList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						m_AuctionList.vItems[ui].HighestBid,
						m_AuctionList.vItems[ui].BuyoutPrice);
				}
				else
				{
					NiSprintf(sInfo, 512, "<YPimg><color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end><Col5>%s<end><line1>%5d<end><Goldimg><line1_1>%2d<end><Silverimg><line1_2>%2d<end><Broneimg><line2>%5d<end><Goldimg><line2_1>%2d<end><Silverimg><line2_2>%2d<end><Broneimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_AuctionList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						_TRAN("Price"),
						iBidGold, iBidSliver, iBidBronze,
						iOnePriceGold, iOnePriceSliver, iOnePriceBronze);
				}
			}
			else
			{
				if(m_AuctionList.vItems[ui].is_yuanbao)
				{
					NiSprintf(sInfo, 512, "<color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end><line1_3>%d<end_right><YuanBaoimg><line2_3>%d<end_right><YuanBaoimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_AuctionList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						m_AuctionList.vItems[ui].HighestBid,
						m_AuctionList.vItems[ui].BuyoutPrice);
				}
				else
				{
					NiSprintf(sInfo, 512, "<color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end><Col5>%s<end><line1>%5d<end><Goldimg><line1_1>%2d<end><Silverimg><line1_2>%2d<end><Broneimg><line2>%5d<end><Goldimg><line2_1>%2d<end><Silverimg><line2_2>%2d<end><Broneimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_AuctionList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						_TRAN("Price"),
						iBidGold, iBidSliver, iBidBronze,
						iOnePriceGold, iOnePriceSliver, iOnePriceBronze);
				}
			}
			m_pkList->AddItem(sInfo);

			USkillButton::DataInfo newItem;
			newItem.id = m_AuctionList.vItems[ui].entryid;
			newItem.num = m_AuctionList.vItems[ui].stack_count;
			newItem.type = UItemSystem::ITEM_DATA_FLAG;
			newItem.pDataInfo = &m_AuctionList.vItems[ui].extra;

			m_pkIcon[ui]->SetStateData(&newItem);
			newItem.pDataInfo = NULL;

			//m_pkIcon[ui]->SetIconId(m_AuctionList.vItems[ui].entryid);
			//m_pkIcon[ui]->SetNum(m_AuctionList.vItems[ui].stack_count);
			m_pkIcon[ui]->SetVisible(TRUE);
		
	}
}
void UAuctionBrowseDlg::Search(int iType)
{
	CHAR Buf[64] = {};
	m_pkSearName->GetText(Buf, 64);
	std::string strAuctionString = Buf;

	int iLevelRange1 = 0;
	if(m_pkSearLevelLower->GetText(Buf, 64))
		iLevelRange1 = atoi(Buf);

	int iLevelRange2 = 255;
	if(m_pkSearLevelUpper->GetText(Buf, 64))
		iLevelRange2 = atoi(Buf);

	if(iLevelRange1 > iLevelRange2)
	{
		UMessageBox::MsgBox_s(_TRAN("Search grade range error！"));
		return;
	}

	AuctionMgr->FindAuctionItem(m_PageIndex * 6,
	strAuctionString,
	iLevelRange1,
	iLevelRange2,
	0,
	-1,
	iType,
	-1,
	m_iSearQuality);
}

