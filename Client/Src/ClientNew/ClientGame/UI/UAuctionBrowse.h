#ifndef __UAuctionBrowse_H__
#define __UAuctionBrowse_H__

#include "UInc.h"
#include "USpecListBox.h"
#include "../../../../../SDBase/Protocol/S2C_Auction.h"
#include "USkillButton.h"
#include "UAuctionInc.h"


class UAuctionBrowseDlg : public UDialog
{
	UDEC_CLASS(UAuctionBrowseDlg);
	UDEC_MESSAGEMAP();
public:
	UAuctionBrowseDlg();
	~UAuctionBrowseDlg();

public:
	void SetBrowseData(stAuctionList AuctionList);
	void ShowData();
	void SetCheckState(int state);
	void SetCheckWuQi();
	void ReSeachType();
	void SetMoney(int num);
	void SetYuanbao(int num);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	virtual void OnTimer(float fDeltaTime);
	virtual void OnClose();
	virtual BOOL OnEscape();

	void OnDoNothing();

	void OnGridClick();

	void OnAuctionBrowse();
	void OnAuctionBidJinBi();
	void OnAuctionBidYuanBao();
	void OnAuctionAuc();

	void OnSearWuqiBtnClicked();
	void OnSearFangjuBtnClicked();
	void OnSearJichuBtnClicked();
	void OnSearXiaohaoBtnClicked();
	void OnSearYuanliaoBtnClicked();
	void OnSearQitaBtnClicked();

	void OnSearBtnClicked();
	void OnBidBtnClicked();
	void OnOnePriceBtnClicked();
	void OnPrevBtnClicked();
	void OnNextBtnClicked();

	void OnRarityClicked();

	void Search(int iType);

	USpecListBox* m_pkList;

	UEdit* m_pkSearName;
	UEdit* m_pkSearLevelLower;
	UEdit* m_pkSearLevelUpper;
	//UEdit* m_pkSearQuality;
	UComboBox* m_pkSearQuality;
	int m_iSearQuality;

	UStaticText* m_pkGold;
	UStaticText* m_pkSilver;
	UStaticText* m_pkBronze;
	UBitmap* m_pkJinBi;

	UEdit* m_pkOnePriceGold;
	UEdit* m_pkOnePriceSilver;
	UEdit* m_pkOnePriceBronze;
	UBitmap* m_pkOnePriceJinBi;

	UStaticText* m_pkYuanBao;
	UBitmap* m_pkBitmapYuanBao;
	UEdit* m_pkOnePriceYuanBao;
	UBitmap* m_pkBitmapOnePriceYuanBao;

	USkillButton* m_pkIcon[6];
	//MSG_S2C::stAuction_List_Result m_stAuctionList;

	bool m_bSelJinBi;
	int m_PageIndex;
	int m_iSearType;
	//uint64 guid;

	stAuctionList m_AuctionList;
};
#endif