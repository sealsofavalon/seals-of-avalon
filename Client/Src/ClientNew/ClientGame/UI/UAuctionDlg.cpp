#include "StdAfx.h"
#include "UAuctionDlg.h"
#include "../Network/PacketBuilder.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "UMessageBox.h"
#include "UChat.h"
#include "ItemManager.h"
#include "ObjectManager.h"
#include "LocalPlayer.h"
#include "UFun.h"
#include "UIMoneyText.h"
#pragma warning(push)
#pragma warning(disable : 4018)
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIAuctionItem, UBitmapButton);
UBEGIN_MESSAGE_MAP(UIAuctionItem, UBitmapButton)
UON_BN_CLICKED(0, &UIAuctionItem::OnClickBtn)
UEND_MESSAGE_MAP()
void UIAuctionItem::StaticInit()
{

}
UIAuctionItem::UIAuctionItem()
{
	m_Icon = NULL;
	m_Left1 = NULL;
	m_Left2 = NULL;
	m_Left3 = NULL;
	m_Left4 = NULL;
	m_MoneyBid = NULL;
	m_MoneyOP = NULL;
}
UIAuctionItem::~UIAuctionItem()
{

}
BOOL UIAuctionItem::OnCreate()
{
	if (!UBitmapButton::OnCreate())
	{
		return FALSE;
	}
	m_Icon = UDynamicCast(USkillButton, GetChildByID(0));
	if (m_Icon)
	{
		USkillButton::DataInfo data;
		data.id = 0;
		data.pos = 0;
		data.type = UItemSystem::ITEM_DATA_FLAG;
		data.num = 0;
		m_Icon->SetStateData(&data);	
	}

	m_Left1 = UDynamicCast(UStaticText, GetChildByID(1));
	m_Left2 = UDynamicCast(UStaticText, GetChildByID(2));
	m_Left3 = UDynamicCast(UStaticText, GetChildByID(3));
	m_Left4 = UDynamicCast(UStaticText, GetChildByID(4));

	if (m_Left1 && m_Left2 && m_Left3 && m_Left4)
	{
		m_Left1->SetTextAlign(UT_LEFT);
		m_Left2->SetTextAlign(UT_CENTER);
		m_Left2->SetTextColor("FFFFFF");
		m_Left3->SetTextAlign(UT_CENTER);
		m_Left3->SetTextColor("FFFFFF");
		m_Left4->SetTextAlign(UT_CENTER);
		m_Left4->SetTextColor("FFFFFF");
	}

	m_MoneyBid = UDynamicCast(UMoneyText, GetChildByID(6));
	if (m_MoneyBid)
	{
		m_MoneyBid->SetMoneySample(FALSE);
		m_MoneyBid->SetMoneyTextAlign(UT_RIGHT);
	}
	m_MoneyOP = UDynamicCast(UMoneyText, GetChildByID(7));
	if (m_MoneyOP)
	{
		m_MoneyOP->SetMoneySample(FALSE);
		m_MoneyOP->SetMoneyTextAlign(UT_RIGHT);
	}
	return TRUE;
}
void UIAuctionItem::OnDestroy()
{
	UBitmapButton::OnDestroy();
}


void UIAuctionItem::OnRender(const UPoint& offset,const URect &updateRect)
{
	UPoint pos = offset;
	pos.x += 40;
	URect rect;
	rect.pt0.x = updateRect.pt0.x + 40;
	rect.pt0.y = updateRect.pt0.y;
	rect.pt1.x = rect.pt0.x + updateRect.GetWidth() - 80;
	rect.pt1.y = rect.pt0.y + updateRect.GetHeight();
	UBitmapButton::OnRender(pos, rect);
	RenderChildWindow(offset,updateRect);
}
void UIAuctionItem::SetData(std::string Left3, std::string Left4, ui32 BidNum, ui32 BuyOutNum, bool IsYuanbao, ui32 entry, ui32 statckCount, ItemExtraData* Data, bool bYP, bool bYours, bool bBid)
{
	if ( m_Left3 && m_Left4)
	{
		m_Left3->SetText(Left3.c_str());
		m_Left4->SetText(Left4.c_str());
	}
	if (m_MoneyBid)
	{
		int top = 4 + (BuyOutNum < MAXINT?0:1) * 8;
		m_MoneyBid->SetTop(top);
		m_MoneyBid->SetMoney(BidNum, IsYuanbao);
	}
	if (m_MoneyOP)
	{
		GetChildByID(5)->SetVisible(BuyOutNum < MAXINT?true:false);
		m_MoneyOP->SetVisible(BuyOutNum < MAXINT?true:false);
		m_MoneyOP->SetMoney(BuyOutNum, IsYuanbao);
	}
	ItemPrototype_Client* pkItem = ItemMgr->GetItemPropertyFromDataBase(entry);
	if (pkItem && m_Left1 && m_Left2)
	{
		m_Left1->SetText(pkItem->C_name.c_str());
		UColor color;
		if(GetQualityColor(pkItem->Quality, color))
			m_Left1->SetTextColor(color);

		char buf[256];
		sprintf(buf, "%u", pkItem->RequiredLevel);
		m_Left2->SetText(buf);
	}

	USkillButton::DataInfo newItem;
	newItem.id = entry;
	newItem.num = statckCount;
	newItem.type = UItemSystem::ITEM_DATA_FLAG;
	if (Data)
	{
		newItem.pDataInfo = new ItemExtraData(*Data);
	}
	m_Icon->SetStateData(&newItem);

	if(GetChildByID(8))
	{
		GetChildByID(8)->SetVisible(bYP);
	}

	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(9));
	if (pText)
	{
		pText->SetVisible(bYours || bBid);
		if (bYours){
			pText->SetText(_TRAN("Your bid："));
		}else if(bBid){
			pText->SetText(_TRAN("Someone bid："));
		}
	}
}
void UIAuctionItem::OnClickBtn()
{

}
//////////////////////////////////////////////////////////////////////////
struct  _stAuctionType
{
	int SearchTypeIndex;
	const char* AuctionTypeName;
};
static _stAuctionType AuctionType[] = 
{
	{ -1 ,"全部"},
	{ITEM_CLASS_CONSUMABLE,"消耗品"},
	{ITEM_CLASS_CONTAINER,"容器"},
	{ITEM_CLASS_WEAPON,"武器"},
	//{ITEM_CLASS_JEWELRY,"珠宝"},
	{ITEM_CLASS_ARMOR,"装备"},
	//{ITEM_CLASS_REAGENT,"药剂"},
	{ITEM_CLASS_RECIPE,"技能书"},
	//{ITEM_CLASS_MONEY,"金币"},
	{ITEM_CLASS_QUEST,"任务道具"},
	{ITEM_CLASS_MISCELLANEOUS,"其他"},
	{ITEM_CLASS_USE,"复用物品"},
};
UIMP_CLASS(UAuctionFrame, UDialog);
UBEGIN_MESSAGE_MAP(UAuctionFrame, UDialog)
UON_BN_CLICKED(0, &UAuctionFrame::OnClickBtnBrowse)
UON_BN_CLICKED(1, &UAuctionFrame::OnClickBtnBig)
UON_BN_CLICKED(2, &UAuctionFrame::OnClickBtnJinBi)
UON_BN_CLICKED(3, &UAuctionFrame::OnClickBtnYuanBao)
UON_BN_CLICKED(4, &UAuctionFrame::OnClickBtnWeapon)
UON_BN_CLICKED(5, &UAuctionFrame::OnClickBtnArmor)
UON_BN_CLICKED(6, &UAuctionFrame::OnClickBtnBase)
UON_BN_CLICKED(7, &UAuctionFrame::OnClickBtnConsumable)
UON_BN_CLICKED(8, &UAuctionFrame::OnClickBtnJewelry)
UON_BN_CLICKED(9, &UAuctionFrame::OnClickBtnOther)
UON_BN_CLICKED(17, &UAuctionFrame::OnClickBtnItem)
UON_BN_CLICKED(18, &UAuctionFrame::OnClickBtnItem)
UON_BN_CLICKED(19, &UAuctionFrame::OnClickBtnItem)
UON_BN_CLICKED(20, &UAuctionFrame::OnClickBtnItem)
UON_BN_CLICKED(21, &UAuctionFrame::OnClickBtnItem)
UON_BN_CLICKED(22, &UAuctionFrame::OnClickBtnItem)
UON_BN_CLICKED(23, &UAuctionFrame::OnClickBtnItem)
UON_BN_CLICKED(24, &UAuctionFrame::OnClickBtnPrevPage)
UON_BN_CLICKED(25, &UAuctionFrame::OnClickBtnNextPage)
UON_BN_CLICKED(26, &UAuctionFrame::OnClickBtnCancelBid)
UON_BN_CLICKED(29, &UAuctionFrame::OnClickBtnPrevType)
UON_BN_CLICKED(30, &UAuctionFrame::OnClickBtnNextType)
UEND_MESSAGE_MAP()
void UAuctionFrame::StaticInit(){}

UAuctionFrame::UAuctionFrame()
{
	for (int i = 0 ; i < AuFindListLength ; i++)
	{
		m_AuctionItem[i] = NULL;
	}
	m_bShowYuanBao = false;
	m_AuctionIndex = 0;
}

UAuctionFrame::~UAuctionFrame()
{

}

void UAuctionFrame::SetPage(int page)
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(page - 100));
	if(pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
}

int UAuctionFrame::GetCurPage()
{
	for(int i = AUCTION_PAGEINDEX_BROWSE; i <= AUCTION_PAGEINDEX_YUANBAO; i++)
	{
		UControl* pCtrl = GetChildByID(i);
		if (pCtrl && pCtrl->IsVisible())
		{
			return i;
		}
	}
	return 0;
}

void UAuctionFrame::OnMoneyChange()
{
	UMoneyText* pMoneyText = UDynamicCast(UMoneyText, GetChildByID(11));
	if (!pMoneyText)
		return;

	if (!m_bShowYuanBao)
	{
		pMoneyText->SetMoney(AUMgr->GetMoney());
	}
	else
	{
		pMoneyText->SetMoney(AUMgr->GetYuanBao(), true);
	}
}
void UAuctionFrame::OnBrowseItemList()
{
	AUItemList vList;
	if (AUMgr->GetBrowseItemList(&vList))
	{
		for(int i = 0 ; i < AuFindListLength ; i++)
		{
			UButton* pBtn = UDynamicCast(UButton, GetChildByID(25));
			if(pBtn)
				pBtn->SetActive(vList.size() >= AuFindListLength);
			if(m_AuctionItem[i] && i < vList.size())
			{
				std::string TimeLeftStr;
				int iHour = (vList[i].timeleft / 1000) / 3600;
				if (iHour >= 24)
				{
					TimeLeftStr = _TRAN("Very Long");
				}else if (iHour >= 12 && iHour < 24)
				{
					TimeLeftStr = _TRAN("Long");
				}else if(iHour >=1 && iHour < 12)
				{
					TimeLeftStr = _TRAN("Soon");
				}else if (iHour < 1)
				{
					TimeLeftStr = _TRAN("Immenent");
				}
				m_AuctionItem[i]->SetData(vList[i].owner_name, TimeLeftStr.c_str(), vList[i].HighestBid, vList[i].BuyoutPrice, vList[i].is_yuanbao, vList[i].entryid, vList[i].stack_count, &vList[i].extra, vList[i].yp, ObjectMgr->GetLocalPlayer()->GetGUID() == vList[i].Lastbidder, vList[i].Lastbidder != 0);
				m_AuctionItem[i]->SetVisible(TRUE);
			}
			else
			{
				m_AuctionItem[i]->SetVisible(FALSE);
			}
		}
	}
}

void UAuctionFrame::OnBidItemList()
{
	AUItemList vList;
	int searchpage = 0;
	UIAuctionAuc* pAA = UDynamicCast(UIAuctionAuc, GetChildByID(AUCTION_PAGEINDEX_AUC));
	if (pAA)
	{
		searchpage = pAA->GetPage();
	}
	if (AUMgr->GetBidItemList(searchpage, &vList))
	{
		for(int i = 0 ; i < AuFindListLength ; i++)
		{
			if(m_AuctionItem[i] && i < vList.size())
			{	
				std::string TimeLeftStr;
				int iHour = (vList[i].timeleft / 1000) / 3600;
				if (iHour >= 24)
				{
					TimeLeftStr = _TRAN("Very Long");
				}else if (iHour >= 12 && iHour < 24)
				{
					TimeLeftStr = _TRAN("Long");
				}else if(iHour >=1 && iHour < 12)
				{
					TimeLeftStr = _TRAN("Soon");
				}else if (iHour < 1)
				{
					TimeLeftStr = _TRAN("Immenent");
				}
				m_AuctionItem[i]->SetData(vList[i].HighestBidName, TimeLeftStr.c_str(), vList[i].HighestBid, vList[i].BuyoutPrice, vList[i].is_yuanbao, vList[i].entryid, vList[i].stack_count, &vList[i].extra, vList[i].yp);
				m_AuctionItem[i]->SetVisible(TRUE);
			}else
			{
				m_AuctionItem[i]->SetVisible(FALSE);
			}
		}
	}
}

void UAuctionFrame::SetSaleIcon(ui32 id, ui64 guid, ui32 price, ui32 count, std::string strName)
{
	//if ( GetCurPage() == AUCTION_PAGEINDEX_JINBI)
	{
		UIAuctionJinBi* pAJB = UDynamicCast(UIAuctionJinBi, GetChildByID(AUCTION_PAGEINDEX_JINBI));
		if( pAJB )
		{
			pAJB->SetSaleIcon(id, guid, price, count, strName);
		}
	}

	//}else if ( GetCurPage() == AUCTION_PAGEINDEX_YUANBAO)
	{
		UIAuctionYuanBao* pAYB = UDynamicCast(UIAuctionYuanBao, GetChildByID(AUCTION_PAGEINDEX_YUANBAO));
		if( pAYB )
		{
			pAYB->SetSaleIcon(id, guid, price, count, strName);
		}
	}
}

void UAuctionFrame::Clear()
{
	UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
	if (pAB)
	{
		pAB->Clear();
	}
	UIAuctionAuc* pAA = UDynamicCast(UIAuctionAuc, GetChildByID(AUCTION_PAGEINDEX_AUC));
	if (pAA)
	{
		pAA->Clear();
	}

	UIAuctionJinBi* pAJB = UDynamicCast(UIAuctionJinBi, GetChildByID(AUCTION_PAGEINDEX_JINBI));
	if (pAJB)
	{
		pAJB->Clear();
	}

	UIAuctionYuanBao* pAYB = UDynamicCast(UIAuctionYuanBao, GetChildByID(AUCTION_PAGEINDEX_YUANBAO));
	if (pAYB)
	{
		pAYB->Clear();
	}
}

void UAuctionFrame::Search()
{
	if (GetCurPage() == AUCTION_PAGEINDEX_BROWSE)
	{
		UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
		if (pAB)
		{
			pAB->Search();
		}
	}
}
void UAuctionFrame::SetShowYuanBao(bool vis)
{
	m_bShowYuanBao = vis;
	OnMoneyChange();
}

void UAuctionFrame::OnOwnerItemList()
{
	AUItemList vList;
	int searchpage = 0;
	bool IsYuanBao = false;
	if (GetCurPage() == AUCTION_PAGEINDEX_JINBI)
	{	
		UIAuctionJinBi* pAJB = UDynamicCast(UIAuctionJinBi, GetChildByID(AUCTION_PAGEINDEX_JINBI));
		if (pAJB)
		{
			searchpage = pAJB->GetPage();
		}
	}
	else if (GetCurPage() == AUCTION_PAGEINDEX_YUANBAO)
	{
		UIAuctionYuanBao* pAYB = UDynamicCast(UIAuctionYuanBao, GetChildByID(AUCTION_PAGEINDEX_YUANBAO));
		if (pAYB)
		{
			searchpage = pAYB->GetPage();
		}
		IsYuanBao = true;
	}
	if (AUMgr->GetOwnerItemList(searchpage, IsYuanBao, &vList))
	{
		UButton* pBtn = UDynamicCast(UButton, GetChildByID(25));
		if(pBtn)
			pBtn->SetActive(vList.size() >= AuFindListLength);
		for(int i = 0 ; i < AuFindListLength ; i++)
		{
			if(m_AuctionItem[i] && i < vList.size())
			{	
				std::string TimeLeftStr;
				int iHour = (vList[i].timeleft / 1000) / 3600;
				if (iHour >= 24)
				{
					TimeLeftStr = _TRAN("Very Long");
				}else if (iHour >= 12 && iHour < 24)
				{
					TimeLeftStr = _TRAN("Long");
				}else if(iHour >=1 && iHour < 12)
				{
					TimeLeftStr = _TRAN("Soon");
				}else if (iHour < 1)
				{
					TimeLeftStr = _TRAN("Immenent");
				}
				m_AuctionItem[i]->SetData(vList[i].HighestBidName, TimeLeftStr.c_str(), vList[i].HighestBid, vList[i].BuyoutPrice, vList[i].is_yuanbao, vList[i].entryid, vList[i].stack_count, &vList[i].extra, vList[i].yp);
				m_AuctionItem[i]->SetVisible(TRUE);
			}else
			{
				m_AuctionItem[i]->SetVisible(FALSE);
			}
		}
	}
}

BOOL UAuctionFrame::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}

	if(m_CloseBtn)
		m_CloseBtn->SetPosition(UPoint(837, 23));


	UMoneyText* pMoneyText = UDynamicCast(UMoneyText, GetChildByID(11));
	if (pMoneyText)
	{
		pMoneyText->SetMoneyTextAlign(UT_RIGHT);
	}

	for (int i = 0 ; i < AuFindListLength ; i++)
	{
		m_AuctionItem[i] = UDynamicCast(UIAuctionItem, GetChildByID(17 + i));
		if (!m_AuctionItem[i])
		{
			return FALSE;
		}
		m_AuctionItem[i]->SetVisible(FALSE);
	}
	for (int i = 12 ; i < 17 ; i++)
	{
		UStaticText* pSt = UDynamicCast(UStaticText, GetChildByID(i));
		if (pSt)
		{
			pSt->SetTextEdge(true);
		}
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(27));
	if(pText)
	{
		pText->SetTextColor("ffe900");
		pText->SetTextEdge(TRUE);
	}
	UButton* pCtrl = UDynamicCast(UButton, GetChildByID(4));
	if (pCtrl)
	{
		pCtrl->SetCheck(FALSE);
	}
	return TRUE;
}

void UAuctionFrame::OnDestroy()
{
	m_bShowYuanBao = false;

	for (int i = 0 ; i < AuFindListLength ; i++)
	{
		m_AuctionItem[i]->SetVisible(FALSE);
	}
	UDialog::OnDestroy();
}
void UAuctionFrame::OnTimer(float fDeltaTime)
{
	if (!m_Visible)
		return;
	UDialog::OnTimer(fDeltaTime);
	if (AUMgr)
	{
		AUMgr->CheckDis();
	}
}
void UAuctionFrame::OnClose()
{
	AUMgr->ReleaseItem();
	CPlayerLocal::SetCUState(cus_Normal);
}
BOOL UAuctionFrame::OnEscape()
{
	if(!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UAuctionFrame::ChangePage(int page)
{
	if (GetCurPage() == page)
	{
		return;
	}
	for(int i = AUCTION_PAGEINDEX_BROWSE; i <= AUCTION_PAGEINDEX_YUANBAO; i++)
	{
		UControl* pCtrl = GetChildByID(i);
		if (pCtrl)
		{
			pCtrl->SetVisible(i == page);
		}
	}
	for (int i = 4 ; i < 10 ; i++)
	{
		UButton* pCtrl = UDynamicCast(UButton, GetChildByID(i));
		if (pCtrl)
		{
			pCtrl->SetVisible(page == AUCTION_PAGEINDEX_BROWSE);
		}
	}
	for (int i = 28 ; i < 31; ++i)
	{
		UControl* pCtrl = GetChildByID(i);
		if (pCtrl)
		{
			pCtrl->SetVisible(page == AUCTION_PAGEINDEX_BROWSE);
		}
	}
	UControl* pCtrl = GetChildByID(26);
	UControl* pCtrl1 = GetChildByID(126);
	if (pCtrl)
	{
		bool vis = (page == AUCTION_PAGEINDEX_YUANBAO || page == AUCTION_PAGEINDEX_JINBI);
		pCtrl1->SetVisible(vis);
		pCtrl->SetVisible(vis);
	}
	if (page == AUCTION_PAGEINDEX_BROWSE)
	{
		const char* Text[] = {"物品名称", "等级", "出售者", "剩余时间", "当前价格"};
		for (int i = 12 ; i < 17 ; i++)
		{
			UStaticText* pSt = UDynamicCast(UStaticText, GetChildByID(i));
			if (pSt)
				pSt->SetText(_TRAN(Text[i - 12]));
		}
		OnBrowseItemList();
		SetAuctionIndex(m_AuctionIndex);
	}
	if (page == AUCTION_PAGEINDEX_AUC)
	{
		const char* Text[] = {"物品名称", "等级", "竞拍状态", "剩余时间", "当前价格"};
		for (int i = 12 ; i < 17 ; i++)
		{
			UStaticText* pSt = UDynamicCast(UStaticText, GetChildByID(i));
			if (pSt)
				pSt->SetText(_TRAN(Text[i - 12]));
		}
		OnBidItemList();
	}
	if (page == AUCTION_PAGEINDEX_JINBI || page == AUCTION_PAGEINDEX_YUANBAO)
	{
		const char* Text[] = {"物品名称", "等级", "竞拍者", "剩余时间", "最高出价"};
		for (int i = 12 ; i < 17 ; i++)
		{
			UStaticText* pSt = UDynamicCast(UStaticText, GetChildByID(i));
			if (pSt)
				pSt->SetText(_TRAN(Text[i - 12]));
		}
		OnOwnerItemList();
	}
	for (int i = 0 ; i < AuFindListLength ; i++)
	{
		if(m_AuctionItem[i]->IsChecked())
		{
			m_AuctionItem[i]->SetCheck(TRUE);
		}
	}

	const char* Text[] = {"浏 览 商 品", "竞 拍 商 品", "金 币 拍 卖", "元 宝 拍 卖"};
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(27));
	if(pText)
	{
		pText->SetText(_TRAN(Text[page - 100]));
	}

	SetShowYuanBao(false);
	AUMgr->OnChangePage();
}
void UAuctionFrame::OnClickBtnBrowse()
{
	ChangePage(AUCTION_PAGEINDEX_BROWSE);
}
void UAuctionFrame::OnClickBtnBig()
{
	ChangePage(AUCTION_PAGEINDEX_AUC);
}
void UAuctionFrame::OnClickBtnJinBi()
{
	ChangePage(AUCTION_PAGEINDEX_JINBI);
}
void UAuctionFrame::OnClickBtnYuanBao()
{
	ChangePage(AUCTION_PAGEINDEX_YUANBAO);
}
void UAuctionFrame::OnClickBtnWeapon()
{
	UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
	if (pAB)
	{
		pAB->SetSearchType((int)AuctionType[m_AuctionIndex].SearchTypeIndex);
	}
}
void UAuctionFrame::OnClickBtnArmor()
{
	UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
	if (pAB)
	{
		pAB->SetSearchType((int)AuctionType[m_AuctionIndex + 1].SearchTypeIndex);
	}
}
void UAuctionFrame::OnClickBtnBase()
{
	UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
	if (pAB)
	{
		pAB->SetSearchType((int)AuctionType[m_AuctionIndex + 2].SearchTypeIndex);
	}
}
void UAuctionFrame::OnClickBtnConsumable()
{
	UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
	if (pAB)
	{
		pAB->SetSearchType((int)AuctionType[m_AuctionIndex + 3].SearchTypeIndex);
	}
}
void UAuctionFrame::OnClickBtnJewelry()
{
	UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
	if (pAB)
	{
		pAB->SetSearchType((int)AuctionType[m_AuctionIndex + 4].SearchTypeIndex);
	}
}
void UAuctionFrame::OnClickBtnOther()
{
	UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
	if (pAB)
	{
		pAB->SetSearchType((int)AuctionType[m_AuctionIndex + 5].SearchTypeIndex);
	}
}
void UAuctionFrame::OnClickBtnItem()
{
	if (GetCurPage() == AUCTION_PAGEINDEX_BROWSE)
	{
		UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
		for (int i = 0 ; i < AuFindListLength ; i++)
		{
			if(m_AuctionItem[i]->IsChecked())
			{
				if (pAB)
					pAB->SetSelectItemPos(i);
				return;
			}
		}
		if (pAB)
			pAB->SetSelectItemPos(-1);
	}else if (GetCurPage() == AUCTION_PAGEINDEX_AUC)
	{
		UIAuctionAuc* pAA = UDynamicCast(UIAuctionAuc, GetChildByID(AUCTION_PAGEINDEX_AUC));
		for (int i = 0 ; i < AuFindListLength ; i++)
		{
			if(m_AuctionItem[i]->IsChecked())
			{
				if (pAA)
					pAA->SetSelectItemPos(i);
				return;
			}
		}
		if (pAA)
			pAA->SetSelectItemPos(-1);
	}else if ( GetCurPage() == AUCTION_PAGEINDEX_JINBI)
	{
		UIAuctionJinBi* pAJB = UDynamicCast(UIAuctionJinBi, GetChildByID(AUCTION_PAGEINDEX_JINBI));
		for (int i = 0 ; i < AuFindListLength ; i++)
		{
			if(m_AuctionItem[i]->IsChecked())
			{
				if (pAJB)
					pAJB->SetSelectItemPos(i);
				return;
			}
		}
		if (pAJB)
			pAJB->SetSelectItemPos(-1);
	}else if ( GetCurPage() == AUCTION_PAGEINDEX_YUANBAO)
	{
		UIAuctionYuanBao* pAYB = UDynamicCast(UIAuctionYuanBao, GetChildByID(AUCTION_PAGEINDEX_YUANBAO));
		for (int i = 0 ; i < AuFindListLength ; i++)
		{
			if(m_AuctionItem[i]->IsChecked())
			{
				if (pAYB)
					pAYB->SetSelectItemPos(i);
				return;
			}
		}
		if (pAYB)
			pAYB->SetSelectItemPos(-1);
	}
}

void UAuctionFrame::OnClickBtnPrevPage()
{
	if (GetCurPage() == AUCTION_PAGEINDEX_BROWSE)
	{
		UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
		if (pAB)
		{
			pAB->PrevPage();
		}
	}else if (GetCurPage() == AUCTION_PAGEINDEX_AUC)
	{
		UIAuctionAuc* pAA = UDynamicCast(UIAuctionAuc, GetChildByID(AUCTION_PAGEINDEX_AUC));
		if (pAA)
		{
			pAA->PrevPage();
		}

	}else if ( GetCurPage() == AUCTION_PAGEINDEX_JINBI)
	{
		UIAuctionJinBi* pAJB = UDynamicCast(UIAuctionJinBi, GetChildByID(AUCTION_PAGEINDEX_JINBI));
		if (pAJB)
		{
			pAJB->PrevPage();
		}

	}else if ( GetCurPage() == AUCTION_PAGEINDEX_YUANBAO)
	{
		UIAuctionYuanBao* pAYB = UDynamicCast(UIAuctionYuanBao, GetChildByID(AUCTION_PAGEINDEX_YUANBAO));
		if (pAYB)
		{
			pAYB->PrevPage();
		}
	}
}

void UAuctionFrame::OnClickBtnNextPage()
{
	if (GetCurPage() == AUCTION_PAGEINDEX_BROWSE)
	{
		UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
		if (pAB)
		{
			pAB->NextPage();
		}
	}else if (GetCurPage() == AUCTION_PAGEINDEX_AUC)
	{
		UIAuctionAuc* pAA = UDynamicCast(UIAuctionAuc, GetChildByID(AUCTION_PAGEINDEX_AUC));
		if (pAA)
		{
			pAA->NextPage();
		}

	}else if ( GetCurPage() == AUCTION_PAGEINDEX_JINBI)
	{
		UIAuctionJinBi* pAJB = UDynamicCast(UIAuctionJinBi, GetChildByID(AUCTION_PAGEINDEX_JINBI));
		if (pAJB)
		{
			pAJB->NextPage();
		}

	}else if ( GetCurPage() == AUCTION_PAGEINDEX_YUANBAO)
	{
		UIAuctionYuanBao* pAYB = UDynamicCast(UIAuctionYuanBao, GetChildByID(AUCTION_PAGEINDEX_YUANBAO));
		if (pAYB)
		{
			pAYB->NextPage();
		}
	}
}
void UAuctionFrame::OnClickBtnCancelBid()
{
	int pos = -1;
	for (int i = 0 ; i < AuFindListLength ; i++)
	{
		if(m_AuctionItem[i]->IsChecked())
		{
			pos = i;
			break;
		}
	}
	int searchpage = 0;
	bool IsYuanBao = false;
	if (GetCurPage() == AUCTION_PAGEINDEX_JINBI)
	{	
		UIAuctionJinBi* pAJB = UDynamicCast(UIAuctionJinBi, GetChildByID(AUCTION_PAGEINDEX_JINBI));
		if (pAJB)
		{
			searchpage = pAJB->GetPage();
		}
	}
	else if (GetCurPage() == AUCTION_PAGEINDEX_YUANBAO)
	{
		UIAuctionYuanBao* pAYB = UDynamicCast(UIAuctionYuanBao, GetChildByID(AUCTION_PAGEINDEX_YUANBAO));
		if (pAYB)
		{
			searchpage = pAYB->GetPage();
		}
		IsYuanBao = true;
	}
	AUItemList vList;
	if (AUMgr->GetOwnerItemList(searchpage, IsYuanBao, &vList))
	{
		if(pos != -1 && pos < vList.size())
		{
			AUMgr->SendRemoveAuctionItem(vList[pos].auction_id);
		}
	}
}

void UAuctionFrame::OnClickBtnPrevType()
{
	if (m_AuctionIndex > 0)
	{
		--m_AuctionIndex;
		SetAuctionIndex(m_AuctionIndex);

		UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
		if (pAB)
		{
			pAB->SetSearchType(-1);
		}
		for (int i = 0 ; i < 6 ; ++i)
		{
			UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(i + 4));
			if (pBtn)
			{
				pBtn->SetCheckState(false);
			}
		}
	}
}

void UAuctionFrame::OnClickBtnNextType()
{
	int size = sizeof(AuctionType) / sizeof(AuctionType[0]);
	if (m_AuctionIndex  + 6 < size)
	{
		++m_AuctionIndex;
		SetAuctionIndex(m_AuctionIndex);

		UIAuctionBrowse* pAB = UDynamicCast(UIAuctionBrowse, GetChildByID(AUCTION_PAGEINDEX_BROWSE));
		if (pAB)
		{
			pAB->SetSearchType(-1);
		}
		for (int i = 0 ; i < 6 ; ++i)
		{
			UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(i + 4));
			if (pBtn)
			{
				pBtn->SetCheckState(false);
			}
		}
	}
}

void UAuctionFrame::SetAuctionIndex(int index)
{
	int size = sizeof(AuctionType) / sizeof(AuctionType[0]);
	m_AuctionIndex = index;
	for (int i = 0 ; i < 6 ; i++)
	{
		UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(i + 4));
		if (pBtn && m_AuctionIndex + i < size)
		{
			pBtn->SetText(_TRAN(AuctionType[m_AuctionIndex + i].AuctionTypeName));
		}
	}
}

//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIAuctionBrowse, UControl);
UBEGIN_MESSAGE_MAP(UIAuctionBrowse, UControl)
UON_BN_CLICKED(14, &UIAuctionBrowse::OnClickBtnSearch)
UON_BN_CLICKED(15, &UIAuctionBrowse::OnClickBtnAuc)
UON_BN_CLICKED(16, &UIAuctionBrowse::OnClickBtnBuy)
UEND_MESSAGE_MAP();
void UIAuctionBrowse::StaticInit()
{

}

UIAuctionBrowse::UIAuctionBrowse()
{
	m_Gold = NULL;
	m_Sliver = NULL;
	m_Copper= NULL;
	m_SearchName = NULL;
	m_SearchMin = NULL;
	m_SearchMax = NULL;
	m_Quility = NULL;
	m_SearchType = -1;
	m_SelectPos = -1;
	m_SearchPage = 0;
}
UIAuctionBrowse::~UIAuctionBrowse()
{

}

BOOL UIAuctionBrowse::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	m_Gold = UDynamicCast(UEdit, GetChildByID(11));
	m_Sliver = UDynamicCast(UEdit, GetChildByID(12));
	m_Copper = UDynamicCast(UEdit, GetChildByID(13));
	m_YuanBao = UDynamicCast(UEdit, GetChildByID(17));
	if (m_Gold && m_Sliver && m_Copper)
	{
		m_Gold->SetNumberOnly(TRUE);
		m_Gold->SetMaxLength(8);
		m_Sliver->SetNumberOnly(TRUE);
		m_Sliver->SetMaxLength(2);
		m_Copper->SetNumberOnly(TRUE);
		m_Copper->SetMaxLength(2);

		m_YuanBao->SetNumberOnly(TRUE);
		m_YuanBao->SetMaxLength(8);
	}

	m_Quility = UDynamicCast(UComboBox,  GetChildByID(10));
	if (m_Quility)
	{
		m_Quility->AddItem(_TRAN("All"));
		int size = sizeof(QuilityClass) / sizeof(QuilityClass[0]);
		for (int i = 0 ; i < size ; i++ )
		{
			m_Quility->AddItem(_TRAN(QuilityClass[i]));
		}
		m_Quility->SetCurSel(0);
	}
	m_SearchName = UDynamicCast(UEdit, GetChildByID(7));
	m_SearchMin = UDynamicCast(UEdit, GetChildByID(8));
	m_SearchMax = UDynamicCast(UEdit, GetChildByID(9));

	if (m_SearchName && m_SearchMin && m_SearchMax)
	{
		m_SearchName->SetMaxLength(10);
		m_SearchMin->SetNumberOnly(TRUE);
		m_SearchMin->SetMaxLength(3);
		m_SearchMax->SetNumberOnly(TRUE);
		m_SearchMax->SetMaxLength(3);
	}
	m_SearchType = -1;
	return TRUE;
}
void UIAuctionBrowse::OnDestroy()
{
	m_SearchType = -1;
	m_SelectPos = -1;
	m_SearchPage = 0;
	m_Quility->Clear();
	UControl::OnDestroy();
}

void UIAuctionBrowse::Search()
{
	char Buf[64] = {};
	m_SearchName->GetText(Buf, 64);
	std::string SearchName = Buf;

	int iLevelRange1 = 0;
	if(m_SearchMin->GetText(Buf, 64))
		iLevelRange1 = atoi(Buf);

	int iLevelRange2 = 80;
	if(m_SearchMax->GetText(Buf, 64))
		iLevelRange2 = atoi(Buf);

	int quility = -1;
	if(m_Quility->GetCurSel() > 0)
	{
		quility = m_Quility->GetCurSel() - 1;
	}

	AUMgr->SendFindAuctionItem(
		m_SearchPage * AuFindListLength,
		SearchName,
		iLevelRange1,
		iLevelRange2,
		0,
		-1,
		m_SearchType,
		-1,
		quility);
}
void UIAuctionBrowse::OnClickBtnSearch()
{
	m_SearchPage = 0;
	Search();
}
void UIAuctionBrowse::OnClickBtnAuc()
{
	AUItemList vList;
	if (AUMgr->GetBrowseItemList(&vList))
	{
		if (m_SelectPos < vList.size())
		{
			char Buf[10] = {};
			uint32 BidPrice = 0;
			if(vList[m_SelectPos].is_yuanbao)
			{
				if(m_YuanBao->GetText(Buf, 10))
					BidPrice += atoi(Buf);
			}
			else
			{
				if(m_Gold->GetText(Buf, 10))
					BidPrice += atoi(Buf) * 10000;

				if(m_Sliver->GetText(Buf, 10))
					BidPrice += atoi(Buf) * 100;

				if(m_Copper->GetText(Buf, 10))
					BidPrice += atoi(Buf);
			}
			bool bSuccess = true;
			std::string ErrorStr;
			CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
			if (pkLocalPlayer->GetGUID() ==  vList[m_SelectPos].owner_guid)
			{
				ErrorStr = _TRAN("Can not bid their own items");
				bSuccess = false;
			}
			//BidPrice = max(BidPrice, vList[m_SelectPos].HighestBid + vList[m_SelectPos].Nextbidmodify);
			if (BidPrice < vList[m_SelectPos].HighestBid + vList[m_SelectPos].Nextbidmodify)
			{
				ErrorStr = _TRAN("Your bid is too low");
				bSuccess = false;
			}
			if(vList[m_SelectPos].is_yuanbao)
			{
				if(pkLocalPlayer->GetYuanBao() < BidPrice)
				{
					ErrorStr = _TRAN("Insuffecient Obelisks");
					bSuccess = false;
				}
			}else
			{
				if(pkLocalPlayer->GetMoney() < BidPrice)
				{
					ErrorStr = _TRAN("Insuffecient Gold");
					bSuccess = false;
				}
			}
			if (bSuccess)
			{
				AUMgr->SendBuyAuction(vList[m_SelectPos].auction_id, BidPrice);
				//OnClickBtnSearch();
			}
			else
			{
				UMessageBox::MsgBox_s(ErrorStr.c_str());
			}
		}
	}
}
void UIAuctionBrowse::OnClickBtnBuy()
{
	AUItemList vList;
	if (AUMgr->GetBrowseItemList(&vList))
	{
		if (m_SelectPos < vList.size())
		{
			bool bSuccess = true;
			std::string ErrorStr;
			CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
			if (pkLocalPlayer->GetGUID() ==  vList[m_SelectPos].owner_guid)
			{
				ErrorStr = _TRAN("Can not bid their own items");
				bSuccess = false;
			}
			if(vList[m_SelectPos].is_yuanbao)
			{
				if(pkLocalPlayer->GetYuanBao() < vList[m_SelectPos].BuyoutPrice)
				{
					ErrorStr = _TRAN("Not Enough Obelisks");
					bSuccess = false;
				}
			}else
			{
				if(pkLocalPlayer->GetMoney() <  vList[m_SelectPos].BuyoutPrice)
				{
					ErrorStr = _TRAN("Not Enough Gold");
					bSuccess = false;
				}
			}
			if (bSuccess)
			{
				AUMgr->SendBuyAuction(vList[m_SelectPos].auction_id, vList[m_SelectPos].BuyoutPrice, true);
				//OnClickBtnSearch();
			}
			else
			{
				UMessageBox::MsgBox_s(ErrorStr.c_str());
			}
		}
	}
}
void UIAuctionBrowse::SetMoneyEdit(ui32 money, bool isyuanbao)
{
	m_Gold->SetVisible(!isyuanbao);
	m_Sliver->SetVisible(!isyuanbao);
	m_Copper->SetVisible(!isyuanbao);
	m_YuanBao->SetVisible(isyuanbao);
	UBitmap* pBitmap = UDynamicCast(UBitmap, GetChildByID(0));
	if (pBitmap)
	{
		if (isyuanbao)
			pBitmap->SetBitMapByStr("DATA\\UI\\AuctionDlg\\BrowseBkgYB.png");
		else
			pBitmap->SetBitMapByStr("DATA\\UI\\AuctionDlg\\BrowseBkg.png");
	}
	AUMgr->SetShowYuanBao(isyuanbao);
	char buf[128];
	if (isyuanbao)
	{
		sprintf(buf, "%u", money);
		m_YuanBao->SetText(buf);
	}else
	{
		int iBidGold = money / 10000;
		int iBidSliver = (money - iBidGold * 10000) / 100;
		int iBidBronze =  money % 100;
		sprintf(buf, "%d", iBidGold);
		m_Gold->SetText(buf);
		sprintf(buf, "%02d", iBidSliver);
		m_Sliver->SetText(buf);
		sprintf(buf, "%02d", iBidBronze);
		m_Copper->SetText(buf);
	}
}

void UIAuctionBrowse::SetSearchType(int type)
{
	m_SearchType = type;
	m_SearchPage = 0;
}

void UIAuctionBrowse::SetSelectItemPos(int pos)
{
	//m_SearchPage = 0;
	m_SelectPos = pos;
	AUItemList vList;
	if (AUMgr->GetBrowseItemList(&vList))
	{
		if (m_SelectPos < vList.size())
		{
			SetMoneyEdit(vList[m_SelectPos].HighestBid + vList[m_SelectPos].Nextbidmodify, vList[m_SelectPos].is_yuanbao);
			GetChildByID(16)->SetActive(vList[m_SelectPos].BuyoutPrice < MAXINT);
		}
	}
}
void UIAuctionBrowse::PrevPage()
{
	if ( m_SearchPage > 0 )
	{
		--m_SearchPage;
		Search();
	}
}
void UIAuctionBrowse::NextPage()
{
	m_SearchPage++;
	Search();
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIAuctionAuc, UControl);
UBEGIN_MESSAGE_MAP(UIAuctionAuc, UControl)
UON_BN_CLICKED(5, &UIAuctionAuc::OnClickBtnYuanbaoAuc)
UON_BN_CLICKED(6, &UIAuctionAuc::OnClickBtnYuanbaoBuy)
UON_BN_CLICKED(7, &UIAuctionAuc::OnClickBtnJinbBiAuc)
UON_BN_CLICKED(8, &UIAuctionAuc::OnClickBtnJinBiBuy)
UEND_MESSAGE_MAP()

void UIAuctionAuc::StaticInit()
{

}

UIAuctionAuc::UIAuctionAuc()
{
	m_Yuanbao = NULL;
	m_Gold = NULL;
	m_Sliver = NULL;
	m_Copper = NULL;
	m_SelectPos = -1;
	m_SearchPage = 0;
}

UIAuctionAuc::~UIAuctionAuc()
{

}

BOOL UIAuctionAuc::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	m_Yuanbao = UDynamicCast(UEdit, GetChildByID(1));
	if (m_Yuanbao)
	{
		m_Yuanbao->SetNumberOnly(TRUE);
		m_Yuanbao->SetMaxLength(8);
		m_Yuanbao->SetActive(FALSE);
	}
	m_Gold = UDynamicCast(UEdit, GetChildByID(2));
	if (m_Gold)
	{
		m_Gold->SetNumberOnly(TRUE);
		m_Gold->SetMaxLength(8);
		m_Gold->SetActive(FALSE);
	}
	m_Sliver = UDynamicCast(UEdit, GetChildByID(3));
	if (m_Sliver)
	{
		m_Sliver->SetNumberOnly(TRUE);
		m_Sliver->SetMaxLength(2);
		m_Sliver->SetActive(FALSE);
	}
	m_Copper = UDynamicCast(UEdit, GetChildByID(4));
	if (m_Copper)
	{
		m_Copper->SetNumberOnly(TRUE);
		m_Copper->SetMaxLength(2);
		m_Copper->SetActive(FALSE);
	}
	UControl* pCtrl11 = GetChildByID(5);
	UControl* pCtrl12 = GetChildByID(6);
	UControl* pCtrl21 = GetChildByID(7);
	UControl* pCtrl22 = GetChildByID(8);
	if (pCtrl11 && pCtrl12 && pCtrl21 && pCtrl22)
	{
		pCtrl11->SetActive(FALSE);
		pCtrl12->SetActive(FALSE);
		pCtrl21->SetActive(FALSE);
		pCtrl22->SetActive(FALSE);
	}
	return TRUE;
}

void UIAuctionAuc::OnDestroy()
{
	m_SelectPos = -1;
	m_SearchPage = 0;
	UControl::OnDestroy();
}

void UIAuctionAuc::SetAucType(bool isYuanBao)
{
	UControl* pCtrl11 = GetChildByID(5);
	UControl* pCtrl12 = GetChildByID(6);
	UControl* pCtrl21 = GetChildByID(7);
	UControl* pCtrl22 = GetChildByID(8);
	if (pCtrl11 && pCtrl12 && pCtrl21 && pCtrl22)
	{
		pCtrl11->SetActive(isYuanBao);
		pCtrl12->SetActive(isYuanBao);
		pCtrl21->SetActive(!isYuanBao);
		pCtrl22->SetActive(!isYuanBao);

		m_Gold->SetActive(!isYuanBao);
		m_Sliver->SetActive(!isYuanBao);
		m_Copper->SetActive(!isYuanBao);
		m_Yuanbao->SetActive(isYuanBao);
	}
	AUMgr->SetShowYuanBao(isYuanBao);
}
void UIAuctionAuc::SetSelectItemPos(int pos)
{
	//m_SearchPage = 0;
	m_SelectPos = pos;
	AUItemList vList;
	if (AUMgr->GetBidItemList( m_SearchPage, &vList ))
	{
		if (m_SelectPos < vList.size())
		{
			SetAucType(vList[m_SelectPos].is_yuanbao);
			if (vList[m_SelectPos].is_yuanbao)
			{
				int Price = vList[m_SelectPos].HighestBid + vList[m_SelectPos].Nextbidmodify;
				char sz[10];
				NiSprintf(sz, 10, "%02d", Price);
				m_Yuanbao->SetText(sz);
			}
			else
			{
				int Price = vList[m_SelectPos].HighestBid + vList[m_SelectPos].Nextbidmodify;
				int iGold = Price / 10000;
				int iSliver = (Price - iGold * 10000) / 100;
				int iBronze = Price%100;
				char sz[10];
				NiSprintf(sz, 10, "%d", iGold);
				m_Gold->SetText(sz);
				NiSprintf(sz, 10, "%02d", iSliver);
				m_Sliver->SetText(sz);
				NiSprintf(sz, 10, "%02d", iBronze);
				m_Copper->SetText(sz);
			}

			GetChildByID(8)->SetActive(vList[m_SelectPos].BuyoutPrice < MAXINT);
		}
	}
}

void UIAuctionAuc::PrevPage()
{
	if ( m_SearchPage > 0 )
	{
		--m_SearchPage;
		UAuctionFrame* pFrame = UDynamicCast(UAuctionFrame, GetParent());
		if (pFrame)
		{
			pFrame->OnBidItemList();
		}
	}
}

void UIAuctionAuc::NextPage()
{
	if (m_SearchPage < AUMgr->GetBidItemListMaxPage())
	{
		++m_SearchPage;
		UAuctionFrame* pFrame = UDynamicCast(UAuctionFrame, GetParent());
		if (pFrame)
		{
			pFrame->OnBidItemList();
		}
	}
}

void UIAuctionAuc::OnClickBtnYuanbaoAuc()
{
	AUItemList vList;
	if (AUMgr->GetBidItemList( m_SearchPage, &vList ))
	{
		if (m_SelectPos < vList.size())
		{
			char Buf[10] = {};
			uint32 BidPrice = 0;
			if(m_Yuanbao->GetText(Buf, 10))
				BidPrice = atoi(Buf);
			CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
			if (!pkLocal)
				return;
			bool bSuccess = true;
			std::string ErrorStr;
			if (pkLocal->GetGUID() ==  vList[m_SelectPos].owner_guid)
			{
				ErrorStr = _TRAN("Can not bid their own items");
				bSuccess = false;
			}
			BidPrice = max(BidPrice, vList[m_SelectPos].HighestBid + vList[m_SelectPos].Nextbidmodify);
			if(pkLocal->GetYuanBao() < BidPrice)
			{
				ErrorStr = _TRAN("Not Enough Obelisks");
				bSuccess = false;
			}
			if (bSuccess)
			{
				AUMgr->SendBuyAuction(vList[m_SelectPos].auction_id, BidPrice);
			}
			else
			{
				UMessageBox::MsgBox_s(ErrorStr.c_str());
			}
		}
	}
}

void UIAuctionAuc::OnClickBtnYuanbaoBuy()
{
	AUItemList vList;
	if (AUMgr->GetBidItemList( m_SearchPage, &vList ))
	{
		if (m_SelectPos < vList.size())
		{
			CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
			if (!pkLocal)
				return;
			bool bSuccess = true;
			std::string ErrorStr;
			if (pkLocal->GetGUID() ==  vList[m_SelectPos].owner_guid)
			{
				ErrorStr = _TRAN("Can not bid their own items");
				bSuccess = false;
			}
			if(pkLocal->GetYuanBao() < vList[m_SelectPos].BuyoutPrice)
			{
				ErrorStr = _TRAN("Not enough Obelisks");
				bSuccess = false;
			}
			if (bSuccess)
			{
				AUMgr->SendBuyAuction(vList[m_SelectPos].auction_id, vList[m_SelectPos].BuyoutPrice, true);
			}
			else
			{
				UMessageBox::MsgBox_s(ErrorStr.c_str());
			}
		}
	}
}

void UIAuctionAuc::OnClickBtnJinbBiAuc()
{
	AUItemList vList;
	if (AUMgr->GetBidItemList( m_SearchPage, &vList ))
	{
		if (m_SelectPos < vList.size())
		{
			char Buf[10] = {};
			uint32 BidPrice = 0;
			if(m_Gold->GetText(Buf, 10))
				BidPrice = atoi(Buf) * 10000;

			if(m_Sliver->GetText(Buf, 10))
				BidPrice += atoi(Buf) * 100;

			if(m_Copper->GetText(Buf, 10))
				BidPrice += atoi(Buf);
			CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
			if(!pkLocal)
				return;

			bool bSuccess = true;
			std::string ErrorStr;
			if (pkLocal->GetGUID() ==  vList[m_SelectPos].owner_guid)
			{
				ErrorStr = _TRAN("Can not bid their own items");
				bSuccess = false;
			}
			BidPrice = max(BidPrice, vList[m_SelectPos].HighestBid + vList[m_SelectPos].Nextbidmodify);
			if(pkLocal->GetMoney() < BidPrice)
			{
				ErrorStr = _TRAN("Not enough Gold");
				bSuccess = false;
			}
			if (bSuccess)
			{
				AUMgr->SendBuyAuction(vList[m_SelectPos].auction_id, BidPrice);
			}
			else
			{
				UMessageBox::MsgBox_s(ErrorStr.c_str());
			}
		}
	}
}

void UIAuctionAuc::OnClickBtnJinBiBuy()
{
	AUItemList vList;
	if (AUMgr->GetBidItemList( m_SearchPage, &vList ))
	{
		if (m_SelectPos < vList.size())
		{
			CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
			if(!pkLocal)
				return;
			bool bSuccess = true;
			std::string ErrorStr;
			if (pkLocal->GetGUID() ==  vList[m_SelectPos].owner_guid)
			{
				ErrorStr = _TRAN("Can not bid their own items");
				bSuccess = false;
			}
			if(pkLocal->GetMoney() < vList[m_SelectPos].BuyoutPrice)
			{
				ErrorStr = _TRAN("Not enough Gold");
				bSuccess = false;
			}
			if (bSuccess)
			{
				AUMgr->SendBuyAuction(vList[m_SelectPos].auction_id, vList[m_SelectPos].BuyoutPrice, true);
			}
			else
			{
				UMessageBox::MsgBox_s(ErrorStr.c_str());
			}
		}
	}
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIAuctionJinBi, UControl)
UBEGIN_MESSAGE_MAP(UIAuctionJinBi, UControl)
UON_BN_CLICKED(1, &UIAuctionJinBi::OnClickBtnIcon)
UON_BN_CLICKED(2, &UIAuctionJinBi::OnClickBtn12Hour)
UON_BN_CLICKED(3, &UIAuctionJinBi::OnClickBtn24Hour)
UON_BN_CLICKED(4, &UIAuctionJinBi::OnClickBtn48Hour)
UON_BN_CLICKED(20, &UIAuctionJinBi::OnClickBtnBeginBid)
UEND_MESSAGE_MAP();

void UIAuctionJinBi::StaticInit()
{

}

UIAuctionJinBi::UIAuctionJinBi()
{
	m_SelectPos = -1;
	m_SearchPage = 0;

	m_SellItemBtn =NULL;

	m_BidGold =NULL;
	m_BidSliver =NULL;
	m_BidCopper =NULL;

	m_BuyOutGold = NULL;
	m_BuyOutSliver = NULL;
	m_BuyOutCopper =NULL;

	m_TimeBtn12 = NULL;
	m_TimeBtn24 = NULL;
	m_TimeBtn48 = NULL;

}
UIAuctionJinBi::~UIAuctionJinBi()
{

}
BOOL UIAuctionJinBi::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	m_BidGold = UDynamicCast(UEdit, GetChildByID(5));
	m_BidSliver = UDynamicCast(UEdit, GetChildByID(6));
	m_BidCopper = UDynamicCast(UEdit, GetChildByID(7));

	m_BuyOutGold = UDynamicCast(UEdit, GetChildByID(8));
	m_BuyOutSliver = UDynamicCast(UEdit, GetChildByID(9));
	m_BuyOutCopper = UDynamicCast(UEdit, GetChildByID(10));

	if (m_BidGold && m_BidSliver && m_BidCopper && m_BuyOutGold && m_BuyOutSliver && m_BuyOutCopper)
	{
		m_BidGold->SetNumberOnly(TRUE);
		m_BidGold->SetMaxLength(8);
		m_BidSliver->SetNumberOnly(TRUE);
		m_BidSliver->SetMaxLength(2);
		m_BidCopper->SetNumberOnly(TRUE);
		m_BidCopper->SetMaxLength(2);

		m_BuyOutGold->SetNumberOnly(TRUE);
		m_BuyOutGold->SetMaxLength(8);
		m_BuyOutSliver->SetNumberOnly(TRUE);
		m_BuyOutSliver->SetMaxLength(2);
		m_BuyOutCopper->SetNumberOnly(TRUE);
		m_BuyOutCopper->SetMaxLength(2);
	}

	m_TexMoney = UDynamicCast(UMoneyText, GetChildByID(11));
	if ( m_TexMoney )
	{
		m_TexMoney->SetMoneyTextAlign(UT_RIGHT);
	}

	m_SellItemBtn = UDynamicCast(USkillButton, GetChildByID(1));
	if(m_SellItemBtn)
	{
		USkillButton::DataInfo kData;
		kData.id = 0;
		kData.type = UItemSystem::ITEM_DATA_FLAG;
		kData.pos = 0;
		kData.num = 0;

		m_SellItemBtn->SetStateData(&kData);
		m_SellItemBtn->SetUIType(UItemSystem::ICT_AUC);
		//m_SellItemBtn->SetLockDrag(TRUE);
	}

	m_TimeBtn12 = UDynamicCast(UBitmapButton, GetChildByID(2));
	m_TimeBtn24 = UDynamicCast(UBitmapButton, GetChildByID(3));
	m_TimeBtn48 = UDynamicCast(UBitmapButton, GetChildByID(4));
	if (m_TimeBtn12 && m_TimeBtn24 && m_TimeBtn48)
	{
		m_TimeBtn12->SetCheckState(TRUE);
	}

	UStaticText* pName = UDynamicCast(UStaticText, GetChildByID(14));
	if (pName)
	{
		pName->SetTextAlign(UT_LEFT);
		pName->SetTextColor("090c59");
	}

	return TRUE;
}
void UIAuctionJinBi::OnDestroy()
{
	m_SelectPos = -1;
	m_SearchPage = 0;
	UControl::OnDestroy();
}
void UIAuctionJinBi::PrevPage()
{
	if (m_SearchPage > 0)
	{
		--m_SearchPage;

		UAuctionFrame* pFrame = UDynamicCast(UAuctionFrame, GetParent());
		if (pFrame)
		{
			pFrame->OnOwnerItemList();
		}
	}
}
void UIAuctionJinBi::NextPage()
{
	if (m_SearchPage < AUMgr->GetOwnerItemListMaxPage(false))
	{
		++m_SearchPage;

		UAuctionFrame* pFrame = UDynamicCast(UAuctionFrame, GetParent());
		if (pFrame)
		{
			pFrame->OnOwnerItemList();
		}
	}
}
void UIAuctionJinBi::SetSelectItemPos(int pos)
{
	m_SelectPos = pos;
}
void UIAuctionJinBi::OnClickBtn12Hour()
{
	int count = 1;
	if (m_SellItemBtn->GetStateData())
	{
		count = m_SellItemBtn->GetStateData()->num;
	}
	ui32 TexPrice = AUMgr->GetSaleItemTax(12) * count;
	m_TexMoney->SetMoney(TexPrice);
}
void UIAuctionJinBi::OnClickBtn24Hour()
{
	int count = 1;
	if (m_SellItemBtn->GetStateData())
	{
		count = m_SellItemBtn->GetStateData()->num;
	}
	ui32 TexPrice = AUMgr->GetSaleItemTax(24) * count;
	m_TexMoney->SetMoney(TexPrice);
}
void UIAuctionJinBi::OnClickBtn48Hour()
{
	int count = 1;
	if (m_SellItemBtn->GetStateData())
	{
		count = m_SellItemBtn->GetStateData()->num;
	}
	ui32 TexPrice = AUMgr->GetSaleItemTax(48) * count;
	m_TexMoney->SetMoney(TexPrice);
}
void UIAuctionJinBi::OnClickBtnBeginBid()
{
	if (!AUMgr->GetSaleItemGUID())
		return;
	char Buf[_MAX_PATH] = {};
	uint32 BidPrice = 0;
	uint32 OnePrice = 0;

	if(m_BidGold->GetText(Buf, _MAX_PATH))
		BidPrice += atoi(Buf) * 10000;

	if(m_BidSliver->GetText(Buf, _MAX_PATH))
		BidPrice += atoi(Buf) * 100;

	if(m_BidCopper->GetText(Buf, _MAX_PATH))
		BidPrice += atoi(Buf);

	if(m_BuyOutGold->GetText(Buf, _MAX_PATH))
		OnePrice += atoi(Buf) * 10000;

	if(m_BuyOutSliver->GetText(Buf, _MAX_PATH))
		OnePrice += atoi(Buf) * 100;

	if(m_BuyOutCopper->GetText(Buf, _MAX_PATH))
		OnePrice += atoi(Buf);

	if(BidPrice > OnePrice && OnePrice != 0)
	{
		ClientSystemNotify( _TRAN("The starting price is greater than a price, it is automatically set to a price！"));
		BidPrice = OnePrice;
	}

	if(OnePrice == 0)
	{
		OnePrice = MAXINT;
	}

	if(BidPrice == 0)
	{
		return;
	}

	uint32 eTime = 60 * 12;
	if(m_TimeBtn24->IsChecked())
		eTime = 60 * 24;
	else if(m_TimeBtn48->IsChecked())
		eTime = 60 * 48;

	AUMgr->SendAddAuctionSellItem(AUMgr->GetSaleItemGUID(), BidPrice,OnePrice,eTime, false, false);

	AUMgr->ReleaseItem();
}

void UIAuctionJinBi::OnClickBtnIcon()
{
	AUMgr->ReleaseItem();
}
void UIAuctionJinBi::SetSaleIcon(ui32 id, ui64 guid, ui32 price, ui32 count, std::string strName)
{
	USkillButton::DataInfo pData;
	pData.id = id;
	pData.type = UItemSystem::ITEM_DATA_FLAG;
	pData.pos = 0;
	pData.num = count;
	ItemExtraData* TempData = new ItemExtraData;

	SYItem * Item = (SYItem *)ObjectMgr->GetObject(guid);
	if (Item)
	{
		TempData->jinglian_level = Item->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
		for( int i = 0; i < 6; ++i )
			TempData->enchants[i] =  Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + i * 3 );
		pData.pDataInfo = TempData;
	}

	m_SellItemBtn->SetStateData(&pData);

	int Hour = 12;
	if(m_TimeBtn24->IsChecked())
		Hour = 24;
	if(m_TimeBtn48->IsChecked())
		Hour = 48;

	int TexPrice = AUMgr->GetSaleItemTax(Hour) * count;
	m_TexMoney->SetMoney(TexPrice);
	
	ItemPrototype_Client* pItemPro = ItemMgr->GetItemPropertyFromDataBase(id);
	if(pItemPro)
	{
		char sz[10];
		int _Price = ItemPrice(pItemPro, false) * count;
		int iGold = _Price / 10000;
		int iSliver = (_Price - iGold * 10000) / 100;
		int iBronze = _Price%100;
		NiSprintf(sz, 10, "%d", iGold);
		m_BidGold->SetText(sz);
		NiSprintf(sz, 10, "%02d", iSliver);
		m_BidSliver->SetText(sz);
		NiSprintf(sz, 10, "%02d", iBronze);
		m_BidCopper->SetText(sz);
	}

	UStaticText* pName = UDynamicCast(UStaticText, GetChildByID(14));
	if (pName)
	{
		pName->SetText(strName.c_str());
	}
}

void UIAuctionJinBi::Clear()
{
	if (m_TimeBtn12)
		m_TimeBtn12->SetCheck(FALSE);

	SetSaleIcon(0, 0, 0, 0, "");

	m_BidGold->Clear();
	m_BidSliver->Clear();
	m_BidCopper->Clear();
	m_BuyOutGold->Clear();
	m_BuyOutSliver->Clear();
	m_BuyOutCopper->Clear();
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIAuctionYuanBao, UControl)
UBEGIN_MESSAGE_MAP(UIAuctionYuanBao, UControl)
UON_BN_CLICKED(1, &UIAuctionYuanBao::OnClickBtnIcon)
UON_BN_CLICKED(2, &UIAuctionYuanBao::OnClickBtn12Hour)
UON_BN_CLICKED(3, &UIAuctionYuanBao::OnClickBtn24Hour)
UON_BN_CLICKED(4, &UIAuctionYuanBao::OnClickBtn48Hour)
UON_BN_CLICKED(5, &UIAuctionYuanBao::OnClickBtnYiPai)
UON_BN_CLICKED(20, &UIAuctionYuanBao::OnClickBtnBeginBid)
UEND_MESSAGE_MAP();

void UIAuctionYuanBao::StaticInit()
{

}
UIAuctionYuanBao::UIAuctionYuanBao()
{
	m_SelectPos = -1;
	m_SearchPage = 0;

	m_SellItemBtn = NULL;
	m_BidYuanBao = NULL;
	m_BuyOutYuanBao = NULL;


	m_TimeBtn12 = NULL;
	m_TimeBtn24 = NULL;
	m_TimeBtn48 = NULL;
	m_TimeBtnYP = NULL;


}
UIAuctionYuanBao::~UIAuctionYuanBao()
{

}
void UIAuctionYuanBao::PrevPage()
{
	if ( m_SearchPage > 0 )
	{
		--m_SearchPage;

		UAuctionFrame* pFrame = UDynamicCast(UAuctionFrame, GetParent());
		if (pFrame)
		{
			pFrame->OnOwnerItemList();
		}
	}
}
void UIAuctionYuanBao::NextPage()
{
	if (m_SearchPage < AUMgr->GetOwnerItemListMaxPage(true))
	{
		++m_SearchPage;

		UAuctionFrame* pFrame = UDynamicCast(UAuctionFrame, GetParent());
		if (pFrame)
		{
			pFrame->OnOwnerItemList();
		}
	}
}
void UIAuctionYuanBao::SetSelectItemPos(int pos)
{
	m_SelectPos = pos;
}
BOOL UIAuctionYuanBao::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	m_SellItemBtn = UDynamicCast(USkillButton, GetChildByID(1));
	if(m_SellItemBtn)
	{
		USkillButton::DataInfo kData;
		kData.id = 0;
		kData.type = UItemSystem::ITEM_DATA_FLAG;
		kData.pos = 0;
		kData.num = 0;

		m_SellItemBtn->SetStateData(&kData);
		m_SellItemBtn->SetUIType(UItemSystem::ICT_AUC);
		//m_SellItemBtn->SetLockDrag(TRUE);
	}

	m_BidYuanBao = UDynamicCast(UEdit, GetChildByID(6));
	m_BuyOutYuanBao = UDynamicCast(UEdit, GetChildByID(7));
	if(m_BuyOutYuanBao && m_BidYuanBao)
	{
		m_BidYuanBao->SetNumberOnly(TRUE);
		m_BidYuanBao->SetMaxLength(8);
		m_BuyOutYuanBao->SetNumberOnly(TRUE);
		m_BuyOutYuanBao->SetMaxLength(8);
	}

	m_TexMoney = UDynamicCast(UMoneyText, GetChildByID(8));
	if ( m_TexMoney )
	{
		m_TexMoney->SetMoneyTextAlign(UT_RIGHT);
	}

	m_TimeBtn12 = UDynamicCast(UBitmapButton, GetChildByID(2));
	m_TimeBtn24 = UDynamicCast(UBitmapButton, GetChildByID(3));
	m_TimeBtn48 = UDynamicCast(UBitmapButton, GetChildByID(4));
	m_TimeBtnYP = UDynamicCast(UBitmapButton, GetChildByID(5));
	if (m_TimeBtn12 && m_TimeBtn24 && m_TimeBtn48 && m_TimeBtnYP)
	{
		m_TimeBtn12->SetCheckState(TRUE);
	}

	UStaticText* pName = UDynamicCast(UStaticText, GetChildByID(14));
	if (pName)
	{
		pName->SetTextAlign(UT_LEFT);
		pName->SetTextColor("090c59");
	}
	return TRUE;
}
void UIAuctionYuanBao::OnDestroy()
{
	m_SelectPos = -1;
	m_SearchPage = 0;
	UControl::OnDestroy();
}

void UIAuctionYuanBao::OnClickBtn12Hour()
{
	int count = 1;
	if (m_SellItemBtn->GetStateData())
	{
		count = m_SellItemBtn->GetStateData()->num;
	}
	ui32 TexPrice = AUMgr->GetSaleItemTax(12) * count;
	m_TexMoney->SetMoney(TexPrice);
}
void UIAuctionYuanBao::OnClickBtn24Hour()
{
	int count = 1;
	if (m_SellItemBtn->GetStateData())
	{
		count = m_SellItemBtn->GetStateData()->num;
	}
	ui32 TexPrice = AUMgr->GetSaleItemTax(24) * count;
	m_TexMoney->SetMoney(TexPrice);
}
void UIAuctionYuanBao::OnClickBtn48Hour()
{
	int count = 1;
	if (m_SellItemBtn->GetStateData())
	{
		count = m_SellItemBtn->GetStateData()->num;
	}
	ui32 TexPrice = AUMgr->GetSaleItemTax(48) * count;
	m_TexMoney->SetMoney(TexPrice);
}
void UIAuctionYuanBao::OnClickBtnYiPai()
{

}
void UIAuctionYuanBao::OnClickBtnBeginBid()
{
	if (!AUMgr->GetSaleItemGUID())
		return;
	char Buf[_MAX_PATH] = {};
	uint32 BidPrice = 0;
	uint32 OnePrice = 0;

	if(m_BidYuanBao->GetText(Buf, _MAX_PATH))
		BidPrice += atoi(Buf);

	if(m_BuyOutYuanBao->GetText(Buf, _MAX_PATH))
		OnePrice += atoi(Buf);

	if(BidPrice > OnePrice && OnePrice != 0)
	{
		ClientSystemNotify( _TRAN("he starting price is greater than a price, it is automatically set to a price！"));
		BidPrice = OnePrice;
	}
	if(OnePrice == 0)
	{
		OnePrice = MAXINT;
	}

	if(BidPrice == 0)
	{
		return;
	}

	uint32 eTime = 60 * 12;
	bool bYP = false;
	if(m_TimeBtnYP->IsChecked())
	{
		bYP = true;
		eTime = 60 * 48;
	}
	else
	{
		if(m_TimeBtn24->IsChecked())
			eTime = 60 * 24;
		if(m_TimeBtn48->IsChecked())
			eTime = 60 * 48;
	}
	AUMgr->SendAddAuctionSellItem(AUMgr->GetSaleItemGUID(), BidPrice, OnePrice, eTime, bYP,true);

	AUMgr->ReleaseItem();
}

void UIAuctionYuanBao::OnClickBtnIcon()
{
	AUMgr->ReleaseItem();
}

void UIAuctionYuanBao::SetSaleIcon(ui32 id, ui64 guid, ui32 price, ui32 count, std::string strName)
{
	USkillButton::DataInfo pData;
	pData.id = id;
	pData.type = UItemSystem::ITEM_DATA_FLAG;
	pData.pos = 0;
	pData.num = count;
	ItemExtraData* TempData = new ItemExtraData;

	SYItem * Item = (SYItem *)ObjectMgr->GetObject(guid);
	if (Item)
	{
		TempData->jinglian_level = Item->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
		for( int i = 0; i < 6; ++i )
			TempData->enchants[i] =  Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + i * 3 );
		pData.pDataInfo = TempData;
	}

	m_SellItemBtn->SetStateData(&pData);

	int Hour = 12;
	if(m_TimeBtn24->IsChecked())
		Hour = 24;
	if(m_TimeBtn48->IsChecked())
		Hour = 48;

	ui32 TexPrice = AUMgr->GetSaleItemTax(Hour) * count;
	TexPrice = TexPrice * Hour / (4 * 100);
	m_TexMoney->SetMoney(TexPrice);

	m_BidYuanBao->SetText("1");

	UStaticText* pName = UDynamicCast(UStaticText, GetChildByID(14));
	if (pName)
	{
		pName->SetText(strName.c_str());
	}
}
void UIAuctionYuanBao::Clear()
{
	if (m_TimeBtn12)
		m_TimeBtn12->SetCheck(FALSE);

	SetSaleIcon(0, 0, 0, 0, "");

	m_BidYuanBao->Clear();
	m_BuyOutYuanBao->Clear();
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UAuctionAucDlg, UDialog);

UBEGIN_MESSAGE_MAP(UAuctionAucDlg, UDialog)
UON_BN_CLICKED(0, &UAuctionAucDlg::OnAuctionBrowse)
UON_BN_CLICKED(1, &UAuctionAucDlg::OnAuctionBidJinBi)
UON_BN_CLICKED(53, &UAuctionAucDlg::OnAuctionBidYuanBao)
UON_BN_CLICKED(2, &UAuctionAucDlg::OnAuctionAuc)
UON_BN_CLICKED(3, &UAuctionAucDlg::OnBidBtnClicked)
UON_BN_CLICKED(4, &UAuctionAucDlg::OnOnePriceBtnClicked)
UON_BN_CLICKED(5, &UAuctionAucDlg::OnPrevBtnClicked)
UON_BN_CLICKED(6, &UAuctionAucDlg::OnNextBtnClicked)
UON_BN_CLICKED(7, &UAuctionAucDlg::OnDoNothing)
UON_BN_CLICKED(8, &UAuctionAucDlg::OnDoNothing)
UON_BN_CLICKED(9, &UAuctionAucDlg::OnDoNothing)
UON_BN_CLICKED(10, &UAuctionAucDlg::OnDoNothing)
UON_BN_CLICKED(11, &UAuctionAucDlg::OnDoNothing)
UON_BN_CLICKED(12, &UAuctionAucDlg::OnDoNothing)
UON_UGD_CLICKED(13, &UAuctionAucDlg::OnGridClick)
//UON_BN_CLICKED(7, &UAuctionBidDlg::OnSearYuanliaoBtnClicked)
//UON_BN_CLICKED(8, &UAuctionBidDlg::OnSearQitaBtnClicked)
//UON_BN_CLICKED(14, &UAuctionBidDlg::OnSearQitaBtnClicked)
UEND_MESSAGE_MAP()

void UAuctionAucDlg::StaticInit()
{

}

UAuctionAucDlg::UAuctionAucDlg()
{
}

UAuctionAucDlg::~UAuctionAucDlg()
{
}

BOOL UAuctionAucDlg::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;
	m_pkList = (USpecListBox*)GetChildByID(13);
	if(m_pkList == NULL)
		return FALSE;

	m_pkList->SetGridSize(UPoint(642 ,43));
	m_pkList->SetGridCount(6, 1);
	m_pkList->SetCol1(27, 12);
	m_pkList->SetWidthCol1(136);
	m_pkList->SetCol2(226, 12);
	m_pkList->SetWidthCol2(40);
	m_pkList->SetCol3(312, 12);
	m_pkList->SetWidthCol3(83);
	m_pkList->SetCol4(416, 12);
	m_pkList->SetWidthCol4(43);
	m_pkList->SetCol5(580, 12);
	m_pkList->SetWidthCol5(80);
	//m_pkList->SetLine1(529, 6);
	//m_pkList->SetWidthLine1(109);
	//m_pkList->SetLine2(529, 21);
	//m_pkList->SetWidthLine2(109);
	//m_pkList->SetLine1(372, 7);
	m_pkList->SetLine1(528, 7);
	//m_pkList->SetWidthLine1(109);
	m_pkList->SetWidthLine1(42);
	m_pkList->SetLine1_1(584, 7);
	m_pkList->SetWidthLine1_1(13);
	m_pkList->SetLine1_2(611, 7);
	m_pkList->SetWidthLine1_2(13);
	//m_pkList->SetLine2(372, 22);
	m_pkList->SetLine2(528, 22);
	//m_pkList->SetWidthLine2(109);
	m_pkList->SetWidthLine2(42);
	m_pkList->SetLine2_1(584, 22);
	m_pkList->SetWidthLine2_1(13);
	m_pkList->SetLine2_2(611, 22);
	m_pkList->SetWidthLine2_2(13);
	m_pkList->SetLine1_3(530, 7);
	m_pkList->SetWidthLine1_3(93);
	m_pkList->SetLine2_3(530, 22);
	m_pkList->SetWidthLine2_3(93);

	m_pkList->SetGoldOff(570);
	m_pkList->SetSilverOff(597);
	m_pkList->SetBroneOff(625);

	m_pkList->SetYuanBaoOff(595 + 30);


	m_pkGold = (UStaticText*)GetChildByID(15);
	m_pkGold->SetTextAlign(UT_RIGHT);
	m_pkSilver = (UStaticText*)GetChildByID(16);
	m_pkSilver->SetTextAlign(UT_RIGHT);
	m_pkBronze = (UStaticText*)GetChildByID(17);
	m_pkBronze->SetTextAlign(UT_RIGHT);

	m_pkOnePriceGold = (UEdit*)GetChildByID(18);
	m_pkOnePriceGold->SetMaxLength(6);
	m_pkOnePriceGold->SetTextAlignment(UT_RIGHT);
	m_pkOnePriceGold->SetNumberOnly(TRUE);
	m_pkOnePriceSilver = (UEdit*)GetChildByID(19);
	m_pkOnePriceSilver->SetMaxLength(2);
	m_pkOnePriceSilver->SetTextAlignment(UT_RIGHT);
	m_pkOnePriceSilver->SetNumberOnly(TRUE);
	m_pkOnePriceBronze = (UEdit*)GetChildByID(20);
	m_pkOnePriceBronze->SetMaxLength(2);
	m_pkOnePriceBronze->SetTextAlignment(UT_RIGHT);
	m_pkOnePriceBronze->SetNumberOnly(TRUE);

	m_pkYuanBao = (UStaticText*)GetChildByID(23);
	m_pkYuanBao->SetTextAlign(UT_RIGHT);

	m_pkOnePriceYuanBao = (UEdit*)GetChildByID(24);
	m_pkOnePriceYuanBao->SetMaxLength(6);
	m_pkOnePriceYuanBao->SetTextAlignment(UT_RIGHT);
	m_pkOnePriceYuanBao->SetNumberOnly(TRUE);

	m_pkBitmapYuanBao = (UBitmap*)GetChildByID(25);
	m_pkBitmapOnePriceYuanBao = (UBitmap*)GetChildByID(26);

	m_pkBitmapJinBi = (UBitmap*)GetChildByID(21);
	m_pkBitmapOnePriceJinBi = (UBitmap*)GetChildByID(22);

	USkillButton::DataInfo kData;
	kData.id = 0;
	kData.type = UItemSystem::ITEM_DATA_FLAG;
	kData.pos = 0;
	kData.num = 0;
	for(unsigned int ui = 0; ui < 6; ui++)
	{
		m_pkIcon[ui] = (USkillButton*)GetChildByID(7 + ui);
		m_pkIcon[ui]->SetStateData(&kData);
	}

	m_PageIndex = 0;

	m_bCurSelYuanBao = false;

#ifdef TEST_CLIENT
	GetChildByID(53)->SetActive(FALSE);
#endif
	return TRUE;
}

void UAuctionAucDlg::OnDestroy()
{
	m_Visible = FALSE;
	UDialog::OnDestroy();
}
void UAuctionAucDlg::OnTimer(float fDeltaTime)
{
	if(!IsVisible())
		return;

	UDialog::OnTimer(fDeltaTime);
	if (AuctionMgr)
	{
		AuctionMgr->CheckDis();
	}
}

void UAuctionAucDlg::OnClose()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionNormal);
}
BOOL UAuctionAucDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}
void UAuctionAucDlg::OnDoNothing()
{

}

void UAuctionAucDlg::OnGridClick()
{
	int iCurSel = m_pkList->GetCurSel();
	if(iCurSel == -1 || m_AuctionBidderList.vItems.size() == 0)
		return;


	if(m_AuctionBidderList.vItems[iCurSel].is_yuanbao)
	{
		m_bCurSelYuanBao = true;

		ui32 uiYuanBao = m_AuctionBidderList.vItems[iCurSel].HighestBid + 1;
		char sz[256];
		NiSprintf(sz, 256, "%d", uiYuanBao);
		m_pkOnePriceYuanBao->SetText(sz);

		if(m_pkYuanBao->IsVisible() == FALSE)
			m_pkYuanBao->SetVisible(TRUE);
		if(m_pkOnePriceYuanBao->IsVisible() == FALSE)
			m_pkOnePriceYuanBao->SetVisible(TRUE);
		if(m_pkBitmapYuanBao->IsVisible() == FALSE)
			m_pkBitmapYuanBao->SetVisible(TRUE);
		if(m_pkBitmapOnePriceYuanBao->IsVisible() == FALSE)
			m_pkBitmapOnePriceYuanBao->SetVisible(TRUE);

		if(m_pkBronze->IsVisible() == TRUE)
			m_pkBronze->SetVisible(FALSE);
		if(m_pkSilver->IsVisible() == TRUE)
			m_pkSilver->SetVisible(FALSE);
		if(m_pkGold->IsVisible() == TRUE)
			m_pkGold->SetVisible(FALSE);
		if(m_pkOnePriceBronze->IsVisible() == TRUE)
			m_pkOnePriceBronze->SetVisible(FALSE);
		if(m_pkOnePriceSilver->IsVisible() == TRUE)
			m_pkOnePriceSilver->SetVisible(FALSE);
		if(m_pkOnePriceGold->IsVisible() == TRUE)
			m_pkOnePriceGold->SetVisible(FALSE);
		if(m_pkBitmapJinBi->IsVisible() == TRUE)
			m_pkBitmapJinBi->SetVisible(FALSE);
		if(m_pkBitmapOnePriceJinBi->IsVisible() == TRUE)
			m_pkBitmapOnePriceJinBi->SetVisible(FALSE);

	}
	else
	{
		m_bCurSelYuanBao = false;

		int iBidGold = m_AuctionBidderList.vItems[iCurSel].HighestBid / 10000;
		int iBidSliver = (m_AuctionBidderList.vItems[iCurSel].HighestBid - iBidGold * 10000) / 100;
		int iBidBronze = (m_AuctionBidderList.vItems[iCurSel].HighestBid - iBidGold * 10000 - iBidSliver * 100) + 1;

		char sz[256];
		if(iBidBronze)
		{
			NiSprintf(sz, 256, "%d", iBidBronze);
			m_pkOnePriceBronze->SetText(sz);
		}
		else
		{
			m_pkOnePriceBronze->Clear();
		}
		if(m_pkOnePriceBronze->IsVisible() == FALSE)
			m_pkOnePriceBronze->SetVisible(TRUE);
		if(iBidSliver)
		{
			NiSprintf(sz, 256, "%d", iBidSliver);
			m_pkOnePriceSilver->SetText(sz);
		}
		else
		{
			m_pkOnePriceSilver->Clear();
		}
		if(m_pkOnePriceSilver->IsVisible() == FALSE)
			m_pkOnePriceSilver->SetVisible(TRUE);
		if(iBidGold)
		{
			NiSprintf(sz, 256, "%d", iBidGold);
			m_pkOnePriceGold->SetText(sz);
		}
		else
		{
			m_pkOnePriceGold->Clear();
		}

		if(m_pkBronze->IsVisible() == FALSE)
			m_pkBronze->SetVisible(TRUE);
		if(m_pkSilver->IsVisible() == FALSE)
			m_pkSilver->SetVisible(TRUE);
		if(m_pkGold->IsVisible() == FALSE)
			m_pkGold->SetVisible(TRUE);
		if(m_pkOnePriceBronze->IsVisible() == FALSE)
			m_pkOnePriceBronze->SetVisible(TRUE);
		if(m_pkOnePriceSilver->IsVisible() == FALSE)
			m_pkOnePriceSilver->SetVisible(TRUE);
		if(m_pkOnePriceGold->IsVisible() == FALSE)
			m_pkOnePriceGold->SetVisible(TRUE);
		if(m_pkBitmapJinBi->IsVisible() == FALSE)
			m_pkBitmapJinBi->SetVisible(TRUE);
		if(m_pkBitmapOnePriceJinBi->IsVisible() == FALSE)
			m_pkBitmapOnePriceJinBi->SetVisible(TRUE);

		if(m_pkYuanBao->IsVisible() == TRUE)
			m_pkYuanBao->SetVisible(FALSE);
		if(m_pkOnePriceYuanBao->IsVisible() == TRUE)
			m_pkOnePriceYuanBao->SetVisible(FALSE);
		if(m_pkBitmapYuanBao->IsVisible() == TRUE)
			m_pkBitmapYuanBao->SetVisible(FALSE);
		if(m_pkBitmapOnePriceYuanBao->IsVisible() == TRUE)
			m_pkBitmapOnePriceYuanBao->SetVisible(FALSE);
	}
}

void UAuctionAucDlg::OnAuctionBrowse()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionBrowse);
}

void UAuctionAucDlg::OnAuctionBidJinBi()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionBidder_G);
}

void UAuctionAucDlg::OnAuctionBidYuanBao()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionBidder_Y);
}

void UAuctionAucDlg::OnAuctionAuc()
{
	AuctionMgr->ChangeUAuctionState(AuctionManager::AuctionAuc);
}

void UAuctionAucDlg::OnBidBtnClicked()
{
	int iCurSel = m_pkList->GetCurSel();
	if(iCurSel == -1)
		return;

	CHAR Buf[10] = {};
	uint32 BidPrice = 0;
	if(m_bCurSelYuanBao)
	{
		m_pkOnePriceYuanBao->GetText(Buf, 10);
		BidPrice = atoi(Buf);
	}
	else
	{
		m_pkOnePriceGold->GetText(Buf, 10);
		BidPrice = atoi(Buf) * 10000;

		m_pkOnePriceSilver->GetText(Buf, 10);
		BidPrice += atoi(Buf) * 100;

		m_pkOnePriceBronze->GetText(Buf, 10);
		BidPrice += atoi(Buf);
	}

	NIASSERT(iCurSel < (int)m_AuctionBidderList.vItems.size());

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLocal);
	
	if (pkLocal->GetGUID() ==  m_AuctionBidderList.vItems[iCurSel].owner_guid)
	{
		UMessageBox::MsgBox_s(_TRAN("Can not buy their own items！"));
		return ;
	}
	if (m_AuctionBidderList.vItems[iCurSel].is_yuanbao)
	{
		if (pkLocal->GetYuanBao() < BidPrice)
		{
			UMessageBox::MsgBox_s(_TRAN("Not enough Obelisks！"));
			return;
		}
	}else
	{
		if (pkLocal->GetMoney() < BidPrice)
		{
			UMessageBox::MsgBox_s(_TRAN("Not enough Gold！"));
			return ;
		}
	}

	AuctionMgr->BuyAuction(m_AuctionBidderList.vItems[iCurSel].auction_id,BidPrice);
/*
	PacketBuilder->SendAuctionPlaceBidMsg(
		AucInfo.Vendorguid,
		AucInfo.stAuctionBidderList.vItems[iCurSel].auction_id,
		BidPrice);*/

}

void UAuctionAucDlg::OnOnePriceBtnClicked()
{
	int iCurSel = m_pkList->GetCurSel();
	if(iCurSel == -1)
		return;

	/*CHAR Buf[10] = {};
	uint32 OnePrice = 0;
	if(m_bCurSelYuanBao)
	{
		m_pkOnePriceYuanBao->GetText(Buf, 10);
		OnePrice = atoi(Buf);
	}
	else
	{
		m_pkOnePriceGold->GetText(Buf, 10);
		OnePrice = atoi(Buf) * 10000;

		m_pkOnePriceSilver->GetText(Buf, 10);
		OnePrice += atoi(Buf) * 100;

		m_pkOnePriceBronze->GetText(Buf, 10);
		OnePrice += atoi(Buf);
	}*/

	NIASSERT(iCurSel < (int)m_AuctionBidderList.vItems.size());

	ui32 BidPrice = m_AuctionBidderList.vItems[iCurSel].BuyoutPrice;
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	NIASSERT(pkLocal);

	if (pkLocal->GetGUID() ==  m_AuctionBidderList.vItems[iCurSel].owner_guid)
	{
		UMessageBox::MsgBox_s(_TRAN("Can not buy their own items！"));
		return ;
	}

	if (m_AuctionBidderList.vItems[iCurSel].is_yuanbao)
	{
		if (pkLocal->GetYuanBao() < BidPrice)
		{
			UMessageBox::MsgBox_s(_TRAN("Not enough Obelisks！"));
			return;
		}
	}else
	{
		if (pkLocal->GetMoney() < BidPrice)
		{
			UMessageBox::MsgBox_s(_TRAN("Not Enough Gold！"));
			return ;
		}
	}

	AuctionMgr->BuyAuction(m_AuctionBidderList.vItems[iCurSel].auction_id,BidPrice, true);
/*
	PacketBuilder->SendAuctionPlaceBidMsg(
		AucInfo.Vendorguid,
		AucInfo.stAuctionBidderList.vItems[iCurSel].auction_id,
		OnePrice);*/
}

void UAuctionAucDlg::OnPrevBtnClicked()
{
	m_PageIndex--;
	if(m_PageIndex <= 0)
	{
		m_PageIndex = 0;
	}
	ShowData();
}

void UAuctionAucDlg::OnNextBtnClicked()
{
	if((m_PageIndex + 1) * 6 < (int)m_AuctionBidderList.vItems.size())
	{
		m_PageIndex++;
	}
	ShowData();
}
void UAuctionAucDlg::SetMoney(int num)
{
	int iBidGold = num / 10000;
	int iBidSliver = (num - iBidGold * 10000) / 100;
	int iBidBronze = (num - iBidGold * 10000 - iBidSliver * 100);

	char sz[256];
	NiSprintf(sz, 256, "%d", iBidBronze);
	m_pkBronze->SetText(sz);
	NiSprintf(sz, 256, "%d", iBidSliver);
	m_pkSilver->SetText(sz);
	NiSprintf(sz, 256, "%d", iBidGold);
	m_pkGold->SetText(sz);
}
void UAuctionAucDlg::SetYuanbao(int num)
{
	char sz[256];
	NiSprintf(sz, 256, "%d", num);
	m_pkYuanBao->SetText(sz);

}
void UAuctionAucDlg::SetBidderData(stAuctionBidder AuctionBidderList)
{
	m_AuctionBidderList = AuctionBidderList;
	ShowData();
}
void UAuctionAucDlg::SetCheckState(int state)
{
	UButton* AucBtn = UDynamicCast(UButton,GetChildByID(2)) ;
	UButton* BrowseBtn = UDynamicCast(UButton, GetChildByID(0));
	UButton* Bidder_GBtn =  UDynamicCast(UButton,GetChildByID(1));
	UButton* Bidder_YBtn =  UDynamicCast(UButton,GetChildByID(53));

	switch(state)
	{
	case AuctionManager::AuctionAuc:
		AucBtn->SetCheckState(TRUE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	case AuctionManager::AuctionBrowse:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(TRUE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	case AuctionManager::AuctionBidder_G:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(TRUE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	case AuctionManager::AuctionBidder_Y:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(TRUE);
		break;
	default:
		AucBtn->SetCheckState(FALSE);
		BrowseBtn->SetCheckState(FALSE);
		Bidder_GBtn->SetCheckState(FALSE);
		Bidder_YBtn->SetCheckState(FALSE);
		break;
	}
}
void UAuctionAucDlg::ShowData()
{
	m_pkList->Clear();
	for(unsigned int ui = 0; ui < 6; ui++)
	{
		m_pkIcon[ui]->SetIconId(0);
		m_pkIcon[ui]->SetVisible(FALSE);
	}

	if (m_PageIndex == 0)
	{
		GetChildByID(5)->SetActive(FALSE);
	}else
	{
		GetChildByID(5)->SetActive(TRUE);
	}

	int PageNum = m_AuctionBidderList.vItems.size() / 6 ;
	if (m_AuctionBidderList.vItems.size() % 6)
	{
		PageNum++;
	}
	if (m_PageIndex >= PageNum)
	{
		GetChildByID(6)->SetActive(FALSE);
	}else
	{
		GetChildByID(6)->SetActive(TRUE);
	}

	if (m_AuctionBidderList.vItems.size() == 0)
	{
		m_PageIndex = 0;
		return ;
	}

	//这里可能需要重新请求信息。


	

	int iStart = m_PageIndex * 6;
	int iEnd = (iStart + 6) > (int)m_AuctionBidderList.vItems.size() ? (int)m_AuctionBidderList.vItems.size() : (iStart + 6);
	int iButtonIndex = 0;
	for(int ui = iStart; ui < iEnd; ui++)
	{
		
			ItemPrototype_Client* pkItem = ItemMgr->GetItemPropertyFromDataBase(m_AuctionBidderList.vItems[ui].entryid);
			NIASSERT(pkItem);
			string strItemName = pkItem->C_name;
			char sInfo[512];
			int iBidGold = m_AuctionBidderList.vItems[ui].HighestBid / 10000;
			int iBidSliver = (m_AuctionBidderList.vItems[ui].HighestBid - iBidGold * 10000) / 100;
			int iBidBronze = (m_AuctionBidderList.vItems[ui].HighestBid - iBidGold * 10000 - iBidSliver * 100);
			int iOnePriceGold = m_AuctionBidderList.vItems[ui].BuyoutPrice / 10000;
			int iOnePriceSliver = (m_AuctionBidderList.vItems[ui].BuyoutPrice - iOnePriceGold * 10000) / 100;
			int iOnePriceBronze = (m_AuctionBidderList.vItems[ui].BuyoutPrice - iOnePriceGold * 10000 - iOnePriceSliver * 100);
			uint32 uTime = m_AuctionBidderList.vItems[ui].timeleft / 1000;
			int iHour = uTime / 3600;
			int iMin = (uTime - iHour * 3600) / 60;
			int iSec = (uTime - iHour * 3600 - iMin * 60);

			if(m_AuctionBidderList.vItems[ui].yp)
			{
				if(m_AuctionBidderList.vItems[ui].is_yuanbao)
				{
					NiSprintf(sInfo, 512, "<YPimg><color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end><line1_3>%d<end_right><YuanBaoimg><line2_3>%d<end_right><YuanBaoimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_AuctionBidderList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						m_AuctionBidderList.vItems[ui].HighestBid,
						m_AuctionBidderList.vItems[ui].BuyoutPrice);
				}
				else
				{
					NiSprintf(sInfo, 512, "<YPimg><color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end><line1>%5d<end><Goldimg><line1_1>%2d<end><Silverimg><line1_2>%2d<end><Broneimg><line2>%5d<end><Goldimg><line2_1>%2d<end><Silverimg><line2_2>%2d<end><Broneimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_AuctionBidderList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						iBidGold, iBidSliver, iBidBronze,
						iOnePriceGold, iOnePriceSliver, iOnePriceBronze);
				}

			}
			else
			{
				if(m_AuctionBidderList.vItems[ui].is_yuanbao)
				{
					NiSprintf(sInfo, 512, "<color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end><line1_3>%d<end_right><YuanBaoimg><line2_3>%d<end_right><YuanBaoimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_AuctionBidderList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						m_AuctionBidderList.vItems[ui].HighestBid,
						m_AuctionBidderList.vItems[ui].BuyoutPrice);
				}
				else
				{
					NiSprintf(sInfo, 512, "<color:%s><Col1><center>%s<end><defaultcolor><Col2><center>%d<end><Col3><center>%s<end><Col4><center>%s<end><line1>%5d<end><Goldimg><line1_1>%2d<end><Silverimg><line1_2>%2d<end><Broneimg><line2>%5d<end><Goldimg><line2_1>%2d<end><Silverimg><line2_2>%2d<end><Broneimg>", 
						GetSpecListBoxColorString(pkItem->Quality).c_str(),
						strItemName.c_str(), 
						pkItem->ItemLevel,
						m_AuctionBidderList.vItems[ui].owner_name.c_str(),
						_TRAN(N_NOTICE_F4, _I2A(iHour).c_str()).c_str(),/* iMin, iSec,*/
						iBidGold, iBidSliver, iBidBronze,
						iOnePriceGold, iOnePriceSliver, iOnePriceBronze);
				}

			}
			m_pkList->AddItem(sInfo);

			USkillButton::DataInfo newItem;
			newItem.id = m_AuctionBidderList.vItems[ui].entryid;
			newItem.num = m_AuctionBidderList.vItems[ui].stack_count;
			newItem.type = UItemSystem::ITEM_DATA_FLAG;
			newItem.pDataInfo = &m_AuctionBidderList.vItems[ui].extra;

			m_pkIcon[iButtonIndex]->SetStateData(&newItem);
			newItem.pDataInfo = NULL;

			//m_pkIcon[iButtonIndex]->SetIconId(m_AuctionBidderList.vItems[ui].entryid);
			//m_pkIcon[iButtonIndex]->SetNum(m_AuctionBidderList.vItems[ui].stack_count);
			m_pkIcon[iButtonIndex]->SetVisible(TRUE);
			iButtonIndex++;
		
	}
}

#pragma warning(pop)