#ifndef UAUCTIONDLG_H
#define UAUCTIONDLG_H

#include "UInc.h"
#include "USpecListBox.h"
#include "UAuctionInc.h"
#include "USkillButton.h"
#include "UAuctionMgr.h"

class UIAuctionItem : public UBitmapButton
{
	UDEC_CLASS(UIAuctionItem);
	UDEC_MESSAGEMAP();
public:
	UIAuctionItem();
	~UIAuctionItem();
public:
	void SetData(std::string Left3, std::string Left4, ui32 BidNum, ui32 BuyOutNum, bool IsYuanbao, ui32 entry, ui32 statckCount, ItemExtraData* Data = NULL, bool bYP = false, bool bYours = false, bool bBid = false);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);

	void OnClickBtn();

	USkillButton* m_Icon;
	UStaticText* m_Left1;
	UStaticText* m_Left2;
	UStaticText* m_Left3;
	UStaticText* m_Left4;
	class UMoneyText* m_MoneyBid;
	class UMoneyText* m_MoneyOP;
};

class UAuctionFrame : public UDialog
{
	UDEC_CLASS(UAuctionFrame);
	UDEC_MESSAGEMAP();
public:
	UAuctionFrame();
	~UAuctionFrame();

	void SetPage(int page);
	int GetCurPage();
	void OnMoneyChange();
	void OnBrowseItemList();
	void OnBidItemList();
	void OnOwnerItemList();

	void SetSaleIcon(ui32 id, ui64 guid, ui32 price, ui32 count, std::string strName);
	void Clear();
	void Search();
	void SetShowYuanBao(bool vis);

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);

	virtual void OnClose();
	virtual BOOL OnEscape();


	void ChangePage(int page);
	void OnClickBtnBrowse();
	void OnClickBtnBig();
	void OnClickBtnJinBi();
	void OnClickBtnYuanBao();

	void OnClickBtnWeapon();
	void OnClickBtnArmor();
	void OnClickBtnBase();
	void OnClickBtnConsumable();
	void OnClickBtnJewelry();
	void OnClickBtnOther();

	void OnClickBtnItem();

	void OnClickBtnPrevPage();
	void OnClickBtnNextPage();

	void OnClickBtnCancelBid();

	void OnClickBtnPrevType();
	void OnClickBtnNextType();
	void SetAuctionIndex(int index);
	int m_AuctionIndex;

	UIAuctionItem* m_AuctionItem[AuFindListLength];
	bool m_bShowYuanBao;
};

class UIAuctionBrowse : public UControl
{
	UDEC_CLASS(UIAuctionBrowse);
	UDEC_MESSAGEMAP();
public:
	UIAuctionBrowse();
	~UIAuctionBrowse();

	void SetSearchType(int type);
	void SetSelectItemPos(int pos);

	void PrevPage();
	void NextPage();
	int GetPage(){ return m_SearchPage ;}

	void Clear(){};
	void Search();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	void OnClickBtnSearch();
	void OnClickBtnAuc();
	void OnClickBtnBuy();

	void SetMoneyEdit(ui32 money, bool isyuanbao);

	UEdit* m_SearchName;
	UEdit* m_SearchMin;
	UEdit* m_SearchMax;
	UEdit* m_Gold;
	UEdit* m_Sliver;
	UEdit* m_Copper;
	UEdit* m_YuanBao;
	UComboBox* m_Quility;

	int m_SearchType;
	int m_SelectPos;
	int m_SearchPage;
};

class UIAuctionAuc : public UControl
{
	UDEC_CLASS(UIAuctionAuc);
	UDEC_MESSAGEMAP();
public:
	UIAuctionAuc();
	~UIAuctionAuc();

	void SetAucType(bool isYuanBao);
	void SetSelectItemPos(int pos);

	void PrevPage();
	void NextPage();
	int GetPage(){return m_SearchPage;}

	void Clear(){};
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	void OnClickBtnYuanbaoAuc();
	void OnClickBtnYuanbaoBuy();
	void OnClickBtnJinbBiAuc();
	void OnClickBtnJinBiBuy();

	UEdit* m_Yuanbao;
	UEdit* m_Gold;
	UEdit* m_Sliver;
	UEdit* m_Copper;

	int m_SelectPos;
	int m_SearchPage;
};

class UIAuctionJinBi : public UControl
{
	UDEC_CLASS(UIAuctionJinBi);
	UDEC_MESSAGEMAP();
public:
	UIAuctionJinBi();
	~UIAuctionJinBi();

	void PrevPage();
	void NextPage();
	int GetPage(){return m_SearchPage;}
	void SetSelectItemPos(int pos);
	void SetSaleIcon(ui32 id, ui64 guid, ui32 price, ui32 count, std::string strName);
	void Clear();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	void OnClickBtn12Hour();
	void OnClickBtn24Hour();
	void OnClickBtn48Hour();

	void OnClickBtnBeginBid();
	void OnClickBtnIcon();

	int m_SelectPos;
	int m_SearchPage;
	USkillButton* m_SellItemBtn;

	UEdit* m_BidGold;
	UEdit* m_BidSliver;
	UEdit* m_BidCopper;

	UEdit* m_BuyOutGold;
	UEdit* m_BuyOutSliver;
	UEdit* m_BuyOutCopper;

	UMoneyText* m_TexMoney;

	UBitmapButton* m_TimeBtn12;
	UBitmapButton* m_TimeBtn24;
	UBitmapButton* m_TimeBtn48;
};

class UIAuctionYuanBao : public UControl
{
	UDEC_CLASS(UIAuctionYuanBao);
	UDEC_MESSAGEMAP();
public:
	UIAuctionYuanBao();
	~UIAuctionYuanBao();

	void PrevPage();
	void NextPage();
	int GetPage(){return m_SearchPage;}
	void SetSelectItemPos(int pos);
	void SetSaleIcon(ui32 id, ui64 guid, ui32 price, ui32 count, std::string strName);
	void Clear();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	void OnClickBtn12Hour();
	void OnClickBtn24Hour();
	void OnClickBtn48Hour();
	void OnClickBtnYiPai();

	void OnClickBtnBeginBid();
	void OnClickBtnIcon();

	int m_SelectPos;
	int m_SearchPage;
	USkillButton* m_SellItemBtn;

	UEdit* m_BidYuanBao;
	UEdit* m_BuyOutYuanBao;

	UMoneyText* m_TexMoney;

	UBitmapButton* m_TimeBtn12;
	UBitmapButton* m_TimeBtn24;
	UBitmapButton* m_TimeBtn48;
	UBitmapButton* m_TimeBtnYP;
};

class UAuctionAucDlg : public UDialog
{
	UDEC_CLASS(UAuctionAucDlg);
	UDEC_MESSAGEMAP();
public:
	UAuctionAucDlg();
	~UAuctionAucDlg();

	void SetBidderData(stAuctionBidder AuctionBidderList);
	void ShowData();
	void SetCheckState(int state);
	void SetMoney(int num);
	void SetYuanbao(int num);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);

	virtual void OnClose();
	virtual BOOL OnEscape();

	void OnDoNothing();

	void OnAuctionBrowse();
	void OnAuctionBidJinBi();
	void OnAuctionBidYuanBao();
	void OnAuctionAuc();

	void OnGridClick();

	void OnBidBtnClicked();
	void OnOnePriceBtnClicked();
	void OnPrevBtnClicked();
	void OnNextBtnClicked();

	USpecListBox* m_pkList;

	USkillButton* m_pkIcon[6];

	UStaticText* m_pkGold;
	UStaticText* m_pkSilver;
	UStaticText* m_pkBronze;
	UBitmap* m_pkBitmapJinBi;

	UEdit* m_pkOnePriceGold;
	UEdit* m_pkOnePriceSilver;
	UEdit* m_pkOnePriceBronze;
	UBitmap* m_pkBitmapOnePriceJinBi;

	UStaticText* m_pkYuanBao;
	UBitmap* m_pkBitmapYuanBao;

	UEdit* m_pkOnePriceYuanBao;
	UBitmap* m_pkBitmapOnePriceYuanBao;

	int m_PageIndex;
	bool m_bCurSelYuanBao;

	stAuctionBidder m_AuctionBidderList;
};
#endif