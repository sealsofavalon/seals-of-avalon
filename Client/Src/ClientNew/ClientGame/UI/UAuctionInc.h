#ifndef __UAUCTION_H__
#define __UAUCTION_H__

#include "../../../../SDBase/Protocol/C2S_Auction.h"
#include "../../../../SDBase/Protocol/S2C_Auction.h"

typedef MSG_S2C::stAuction_List_Result        stAuctionList;
typedef MSG_S2C::stAuction_Bidder_List_Result stAuctionBidder;
typedef MSG_S2C::stAuction_Owner_List_Result  stAuctionOwer;
typedef MSG_C2S::stAuction_List_Items  AuctionFindData;

typedef std::vector<MSG_S2C::stAuction_Item> AUItemList;
#endif