#include "stdafx.h"
#include "UIIndex.h"
#include "UAuctionMgr.h"
#include "ObjectManager.h"
#include "ItemSlot.h"
#include "UMessageBox.h"
#include "Network/PacketBuilder.h"
#include "ui/UISystem.h"
#include "ItemManager.h"
#include "UFun.h"
#include "UIGamePlay.h"
#include "UAuctionDlg.h"
#include "UAuctionBig.h"
#include "UAuctionBrowse.h"
//////////////////////////////////////////////////////////////////////////
AUManager* AUManager::m_AUMgr = NULL;
AUManager::AUManager()
{
	m_AUMgr = this;
	m_AuctionFrame = NULL;
	m_MoneyNum = 0;
	m_YuanBaoNum = 0;
	m_SalePos = -1;
	m_SaleItemEntry = 0;
	m_SaleItemGuid = 0;

	InitClientData();
}
AUManager::~AUManager()
{
	ReleaseClientData();
}

AUManager* AUManager::Instance()
{
	if (m_AUMgr == NULL)
	{
		new AUManager;
	}
	return m_AUMgr;
}
void AUManager::Initialize()
{
	m_NPCGuid  = 0;
	m_SalePos = -1;
	m_MoneyNum = 0;
	m_YuanBaoNum = 0;
	m_SaleItemEntry = 0;
	m_SaleItemGuid = 0;

	m_stAuctionList.vItems.clear();
	m_stAuctionBidderList.vItems.clear();
	m_stAuctionOwnerList.vItems.clear();
}
void AUManager::Destory()
{
	if(m_AUMgr)
	{
		delete m_AUMgr;
		m_AUMgr = NULL;
	}
}
BOOL AUManager::CreateFrame(UControl* Frame)
{
	if (!Frame)
		return FALSE;
	if (m_AuctionFrame == NULL && Frame->GetChildByID(FRAME_IG_AUCTIONAUCDLG) == NULL)
	{
		UControl* pkControl = UControl::sm_System->CreateDialogFromFile("AuctionDlg\\AuctionDLG.udg");
		m_AuctionFrame = UDynamicCast(UAuctionFrame, pkControl);
		if (m_AuctionFrame == NULL)
		{
			return FALSE;
		}else
		{
			Frame->AddChild(m_AuctionFrame);
			m_AuctionFrame->SetId(FRAME_IG_AUCTIONAUCDLG);
		}
		UControl* pNewCtrl = NULL;
		pNewCtrl = NULL;
		pNewCtrl = UControl::sm_System->CreateDialogFromFile("AuctionDlg\\AuctionBrowse.udg");
		if(!pNewCtrl)
			return FALSE;
		pNewCtrl->SetId(AUCTION_PAGEINDEX_BROWSE);
		m_AuctionFrame->AddChild(pNewCtrl);

		pNewCtrl = NULL;
		pNewCtrl = UControl::sm_System->CreateDialogFromFile("AuctionDlg\\AuctionAuc.udg");
		if(!pNewCtrl)
			return FALSE;
		pNewCtrl->SetId(AUCTION_PAGEINDEX_AUC);
		m_AuctionFrame->AddChild(pNewCtrl);

		pNewCtrl = NULL;
		pNewCtrl = UControl::sm_System->CreateDialogFromFile("AuctionDlg\\AuctionJinBi.udg");
		if(!pNewCtrl)
			return FALSE;
		pNewCtrl->SetId(AUCTION_PAGEINDEX_JINBI);
		m_AuctionFrame->AddChild(pNewCtrl);

		pNewCtrl = NULL;
		pNewCtrl = UControl::sm_System->CreateDialogFromFile("AuctionDlg\\AuctionYuanBao.udg");
		if(!pNewCtrl)
			return FALSE;
		pNewCtrl->SetId(AUCTION_PAGEINDEX_YUANBAO);
		m_AuctionFrame->AddChild(pNewCtrl);

		//m_AuctionFrame->SetPage(AUCTION_PAGEINDEX_BROWSE);
		return TRUE;
	}
	return TRUE;
}

void AUManager::SetMoney(int num)
{
	m_MoneyNum = num;
	m_AuctionFrame->OnMoneyChange();
}

void AUManager::SetYuanbao(int num)
{
	m_YuanBaoNum = num;
	m_AuctionFrame->OnMoneyChange();
}

int AUManager::GetSaleItemTax(int hour)
{
	ItemPrototype_Client* pItemPro = ItemMgr->GetItemPropertyFromDataBase(m_SaleItemEntry);
	int TexPrice = 30;
	if (pItemPro)
		TexPrice = ItemPrice(pItemPro, false);
	else
		TexPrice = 0;

	TexPrice = int(float(TexPrice) * hour / (4 * 100));
	return TexPrice;
}
void AUManager::SetShowYuanBao(bool vis)
{
	m_AuctionFrame->SetShowYuanBao(vis);
}

void AUManager::CheckDis()
{
	if (m_NPCGuid)
	{
		CPlayerLocal* pkLcol = ObjectMgr->GetLocalPlayer();
		CCreature* pkOBJ = (CCreature*)ObjectMgr->GetObject(m_NPCGuid);

		if (pkLcol == NULL || pkOBJ == NULL)
		{
			return ;
		}else
		{
			NiPoint3 pCreaturePos = pkOBJ->GetPosition();
			NiPoint3 pLocalPlayerPos = pkLcol->GetLocalPlayerPos();

			if ((pLocalPlayerPos - pCreaturePos).Length() > MaxLenToNPC)
			{
				CPlayerLocal::SetCUState(cus_Normal);
			}
		}
	}
}

void AUManager::ShowFrame()
{
	m_AuctionFrame->SetVisible(TRUE);
	SendFindAuctionListOwnerItems();
	SendFindAuctionBidderList();
}

void AUManager::HideFrame()
{
	m_AuctionFrame->SetVisible(FALSE);
}

void AUManager::OnChangePage()
{
	ReleaseItem();
	if (m_AuctionFrame->GetCurPage() == AUCTION_PAGEINDEX_AUC)
	{
		SendFindAuctionBidderList();
	}else if(m_AuctionFrame->GetCurPage() == AUCTION_PAGEINDEX_JINBI || m_AuctionFrame->GetCurPage() == AUCTION_PAGEINDEX_YUANBAO)
	{
		SendFindAuctionListOwnerItems();
	}
}

void AUManager::ParseHelloMsg(MSG_S2C::stAuction_Hello  pkMsg)
{
	m_NPCGuid = pkMsg.vendorguid;
	m_AuctionFrame->SetPage(AUCTION_PAGEINDEX_BROWSE);
	CPlayerLocal::SetCUState(cus_Auction);
}
void AUManager::ParseCommandMsg(MSG_S2C::stAuction_Command_Result pkCommand )
{
	switch(pkCommand.auction_result)
	{
	case MSG_S2C::AUCTION_CREATE:
		{
			m_AuctionFrame->Clear();
			ClientSystemNotify( _TRAN("物品已经开始拍卖"));
			SendFindAuctionListOwnerItems();
			SYState()->IAudio->PlayUiSound("Sound/UI/AuctionWindowClose.wav", NiPoint3::ZERO, 1.0f, 1 );
			//更新拍卖列表
		}
		break;
	case MSG_S2C::AUCTION_CANCEL:
		{
			m_AuctionFrame->Clear();
			SYState()->IAudio->PlayUiSound("Sound/UI/AuctionWindowClose.wav", NiPoint3::ZERO, 1.0f, 1 );
		}
		break;
	case MSG_S2C::AUCTION_BID:
		{
			ClientSystemNotify(_TRAN("You have bid on the auction"));
			SYState()->IAudio->PlayUiSound("Sound/UI/AuctionWindowClose.wav", NiPoint3::ZERO, 1.0f, 1 );
		}
		break;
	case MSG_S2C::AUCTION_BUYOUT:
		{
			ClientSystemNotify(_TRAN("You boughtout the auction"));
			SYState()->IAudio->PlayUiSound("Sound/UI/AuctionWindowClose.wav", NiPoint3::ZERO, 1.0f, 1 );
		}
		break;
	}

	switch(pkCommand.auction_error)
	{
	case MSG_S2C::AUCTION_ERROR_UNK1:
		break;
	case MSG_S2C::AUCTION_ERROR_INTERNAL:
		break;
	case MSG_S2C::AUCTION_ERROR_MONEY:
		{
			UMessageBox::MsgBox_s(_TRAN("Your money is not enough"));
		}
		break;
	case MSG_S2C::AUCTION_ERROR_ITEM:
		{
			UMessageBox::MsgBox_s(_TRAN("Can not bid on their own items"));
		}
		break;
	}
}
void AUManager::ParseAuctionListMsg(MSG_S2C::stAuction_List_Result pkAuctionList)
{
	m_stAuctionList = pkAuctionList;
	if (m_AuctionFrame->GetCurPage() == AUCTION_PAGEINDEX_BROWSE)
	{
		m_AuctionFrame->OnBrowseItemList();
	}
}
void AUManager::ParseAuctionBidderMsg(MSG_S2C::stAuction_Bidder_List_Result  pkBidderList)
{
	m_stAuctionBidderList = pkBidderList;
	if (m_AuctionFrame->GetCurPage() == AUCTION_PAGEINDEX_AUC)
	{
		m_AuctionFrame->OnBidItemList();
	}
}
void AUManager::ParseAuctionOwerList(MSG_S2C::stAuction_Owner_List_Result pkList)
{
	m_stAuctionOwnerList = pkList;
	m_stAuctionYuanBaoList.clear();
	m_stAuctionJinBiList.clear();

	for ( int i = 0 ; i < m_stAuctionOwnerList.vItems.size() ; ++i )
	{
		if (m_stAuctionOwnerList.vItems[i].is_yuanbao)
		{
			m_stAuctionYuanBaoList.push_back(m_stAuctionOwnerList.vItems[i]);
		}
		else
		{
			m_stAuctionJinBiList.push_back(m_stAuctionOwnerList.vItems[i]);
		}
	}
	if (m_AuctionFrame->GetCurPage() == AUCTION_PAGEINDEX_JINBI || m_AuctionFrame->GetCurPage() == AUCTION_PAGEINDEX_YUANBAO)
	{
		m_AuctionFrame->OnOwnerItemList();
	}
}

bool AUManager::SendAddAuctionSellItem(uint64 item,uint32 bid, uint32 buyout, uint32 etime, bool bYP, bool bYuanBao)
{
	PacketBuilder->SendAuctionSellItemMsg(m_NPCGuid, 
		item,
		bid,
		buyout,
		etime,
		bYP,
		bYuanBao);

	return true;
}
bool AUManager::SendRemoveAuctionItem(uint32 auctionid)
{
	if (m_curRemoveAucitonItemData)
	{
		m_curRemoveAucitonItemData->guid = m_NPCGuid;
		m_curRemoveAucitonItemData->auction_id = auctionid;

		UMessageBox::MsgBox(_TRAN("Are you sure you want to cancel the auction？"), (BoxFunction*)AUManager::RemoveSelItem);
	}
	//PacketBuilder->SendAuctionRemoveItemMsg(m_NPCGuid,	auctionid);
	return true;
}
bool AUManager::SendBuyAuction( uint32 aucitonId, uint32 price, bool bOnePirce )
{
	if (m_CurBuyItemData)
	{
		m_CurBuyItemData->guid = m_NPCGuid;
		m_CurBuyItemData->auction_id = aucitonId;
		m_CurBuyItemData->price = price ;

		if (bOnePirce)
		{
			UMessageBox::MsgBox(_TRAN("Are you sure you want to buy this?"), (BoxFunction*)AUManager::BuyCurItem);
		}else
		{
			UMessageBox::MsgBox(_TRAN("Are you sure you want to bid?"), (BoxFunction*)AUManager::BuyCurItem);
		}

	}
	//PacketBuilder->SendAuctionPlaceBidMsg(m_NPCGuid,aucitonId,price);
	return true;
}
bool AUManager::SendFindAuctionItem(uint32 startIndex, std::string& auctionString,uint8 levelRange1, uint8 levelRange2, uint8 usableCheck,int32 inventoryType, int32 itemclass, int32 itemsubclass, int32 rarityCheck)
{
	PacketBuilder->SendAuctionListItemsMsg(m_NPCGuid,
		startIndex,	
		auctionString,
		levelRange1,
		levelRange2,
		usableCheck,
		inventoryType,
		itemclass,
		itemsubclass,
		rarityCheck
		);
	return true;
}
bool AUManager::SendFindAuctionListOwnerItems()
{
	return PacketBuilder->SendAuctionListOwnerItems(m_NPCGuid);
}
bool AUManager::SendFindAuctionBidderList()
{
	return PacketBuilder->SendAuctionListBidderItemsMsg(m_NPCGuid);
}

bool AUManager::CheckOwerState()
{
	return m_AuctionFrame->GetCurPage() == AUCTION_PAGEINDEX_JINBI || m_AuctionFrame->GetCurPage() == AUCTION_PAGEINDEX_YUANBAO;
}

bool AUManager::GetBrowseItemList(AUItemList* pList)
{
	if (!pList)
		return false;
	int size = min(AuFindListLength, m_stAuctionList.vItems.size());
	for ( int i = 0 ; i < size ; i++)
	{
		pList->push_back(m_stAuctionList.vItems[i]);
	}
	return true;
}

bool AUManager::GetBidItemList(int Page, AUItemList* pList)
{
	if (!pList)
		return false;
	int size = min(AuFindListLength, m_stAuctionBidderList.vItems.size() - Page * AuFindListLength);
	for ( int i = 0 ; i < size ; i++)
	{
		pList->push_back(m_stAuctionBidderList.vItems[i + Page * AuFindListLength]);
	}
	return true;
}

bool AUManager::GetOwnerItemList(int Page,  bool IsYuanBao, AUItemList* pList)
{
	if (!pList)
		return false;

	if (!IsYuanBao)
	{
		int size = min(AuFindListLength, m_stAuctionJinBiList.size() - Page * AuFindListLength);
		for ( int i = 0 ; i < size ; i++)
		{
			pList->push_back(m_stAuctionJinBiList[i  + Page * AuFindListLength]);
		}
	}
	else
	{
		int size = min(AuFindListLength, m_stAuctionYuanBaoList.size() - Page * AuFindListLength);
		for ( int i = 0 ; i < size ; i++)
		{
			pList->push_back(m_stAuctionYuanBaoList[i  + Page * AuFindListLength]);
		}
	}
	return true;
}

int AUManager::GetOwnerItemListMaxPage(bool IsYuanBao)
{
	if (IsYuanBao)
	{
		 return m_stAuctionYuanBaoList.size() / (AuFindListLength + 1);
	}
	else
	{
		 return m_stAuctionJinBiList.size() / (AuFindListLength + 1);
	}
	return 0;
}

void AUManager::SetSellItemIn(ui8 Pos)
{
	ReleaseItem();
	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if(localPlayer == NULL)
		return;
	CStorage* pItemSoltContainer = localPlayer->GetItemContainer();
	CItemSlot* pkItemSlot = (CItemSlot*)pItemSoltContainer->GetSlot(Pos);
	SYItem * Item = (SYItem *)ObjectMgr->GetObject(pkItemSlot->GetGUID());
	if(Item && Item->HasFlag( ITEM_FIELD_FLAGS, ITEM_FLAG_QUEST | ITEM_FLAG_SOULBOUND ))
	{
		UMessageBox::MsgBox_s(_TRAN("The item has been bound！"));
		return;
	}
	ItemPrototype_Client* pkItemInfo =  Item->GetItemProperty();
	if (pkItemInfo && pkItemInfo->Class == ITEM_CLASS_QUEST)
	{
		UMessageBox::MsgBox_s(_TRAN("Quest Items! You can not sell."));
		return ; 
	}

	ui32 price = pkItemInfo->SellPrice * Item->GetUInt32Value(ITEM_FIELD_STACK_COUNT);
	//在添加等等。。
	pkItemSlot->SetLock(TRUE);
	SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG, Pos, pkItemSlot->GetCode(),pkItemSlot->GetNum(),UItemSystem::ITEM_DATA_FLAG,true);

	m_SaleItemEntry = pkItemSlot->GetCode();
	m_SaleItemGuid = pkItemSlot->GetGUID();
	m_AuctionFrame->SetSaleIcon(m_SaleItemEntry, m_SaleItemGuid, price, pkItemSlot->GetNum(), pkItemInfo->C_name);
	m_SalePos = Pos;
}

void AUManager::ReleaseItem()
{
	if (m_SalePos == -1)
		return ;

	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if(localPlayer == NULL)
		return;
	CStorage* pItemSoltContainer = localPlayer->GetItemContainer();
	CItemSlot* pkItemSlot = (CItemSlot*)pItemSoltContainer->GetSlot(m_SalePos);
	if(pkItemSlot) 
	{
		pkItemSlot->SetLock(FALSE);
		SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG,m_SalePos, pkItemSlot->GetCode(),pkItemSlot->GetNum(),UItemSystem::ITEM_DATA_FLAG,false);
	}
	m_AuctionFrame->Clear();
	m_SalePos = -1;
	m_SaleItemEntry = 0;
	m_SaleItemGuid = 0;
}

void AUManager::InitClientData()
{
	m_CurSellData = new MSG_C2S::stAuction_Sell_Item ;
	m_curRemoveAucitonItemData = new MSG_C2S::stAuction_Remove_Item;
	m_CurBuyItemData = new MSG_C2S::stAuction_Place_Bid;

	SetCurDefData();
}

void AUManager::SetCurDefData()
{
	if (m_CurSellData)
	{
		m_CurSellData->guid = 0;
		m_CurSellData->item = 0;
		m_CurSellData->bid = 0;
		m_CurSellData->buyout = 0;
		m_CurSellData->etime = 0;
		m_CurSellData->yp = false;
		m_CurSellData->is_yuanbao = false;
	}

	if (m_curRemoveAucitonItemData)
	{
		m_curRemoveAucitonItemData->auction_id = 0;
		m_curRemoveAucitonItemData->guid = 0;
	}

	if (m_CurBuyItemData)
	{
		m_CurBuyItemData->auction_id = 0;
		m_CurBuyItemData->guid = 0;
		m_CurBuyItemData->price = 0;
	}
}

void AUManager::ReleaseClientData()
{
	if (m_CurSellData)
	{
		delete m_CurSellData;
		m_CurSellData = NULL;
	}
	if (m_curRemoveAucitonItemData)
	{
		delete m_curRemoveAucitonItemData;
		m_curRemoveAucitonItemData = NULL;
	}

	if (m_CurBuyItemData)
	{
		delete m_CurBuyItemData;
		m_CurBuyItemData = NULL;
	}
}


void AUManager::AddSelItem()
{
	if (AUMgr->m_CurSellData)
	{
		PacketBuilder->SendAuctionSellItemMsg(AUMgr->m_CurSellData->guid, 
			AUMgr->m_CurSellData->item,
			AUMgr->m_CurSellData->bid,
			AUMgr->m_CurSellData->buyout,
			AUMgr->m_CurSellData->etime,
			AUMgr->m_CurSellData->yp,
			AUMgr->m_CurSellData->is_yuanbao);

		AUMgr->SetCurDefData();
	}
}
void AUManager::RemoveSelItem()
{
	if (AUMgr->m_curRemoveAucitonItemData)
	{
		PacketBuilder->SendAuctionRemoveItemMsg(AUMgr->m_curRemoveAucitonItemData->guid,
			AUMgr->m_curRemoveAucitonItemData->auction_id);
		AUMgr->SetCurDefData();
	}
}

void AUManager::BuyCurItem()
{
	if (AUMgr->m_CurBuyItemData)
	{
		PacketBuilder->SendAuctionPlaceBidMsg(AUMgr->m_CurBuyItemData->guid,
			AUMgr->m_CurBuyItemData->auction_id,
			AUMgr->m_CurBuyItemData->price);
		AUMgr->SetCurDefData();

		AUMgr->m_AuctionFrame->Search();
	}
}

//////////////////////////////////////////////////////////////////////////
AuctionManager::AuctionManager()
{
	m_NPCGuid = 0;
	m_Auc = NULL;
	m_Big = NULL;
	m_Browse = NULL;
	m_State = AuctionNormal;

	m_SalePos = -1;

	m_BGetAuctionList = FALSE;
	m_BGetBidderList = FALSE;
	m_BGetOwerList = FALSE;

	InitClientData();
}
AuctionManager::~AuctionManager()
{
	ReleaseClientData();
}

void AuctionManager::InitClientData()
{
	m_CurSellData = new MSG_C2S::stAuction_Sell_Item ;
	m_curRemoveAucitonItemData = new MSG_C2S::stAuction_Remove_Item;
	m_CurBuyItemData = new MSG_C2S::stAuction_Place_Bid;

	SetCurDefData();
}
void AuctionManager::SetCurDefData()
{
	if (m_CurSellData)
	{
		m_CurSellData->guid = 0;
		m_CurSellData->item = 0;
		m_CurSellData->bid = 0;
		m_CurSellData->buyout = 0;
		m_CurSellData->etime = 0;
		m_CurSellData->yp = false;
		m_CurSellData->is_yuanbao = false;
	}

	if (m_curRemoveAucitonItemData)
	{
		m_curRemoveAucitonItemData->auction_id = 0;
		m_curRemoveAucitonItemData->guid = 0;
	}

	if (m_CurBuyItemData)
	{
		m_CurBuyItemData->auction_id = 0;
		m_CurBuyItemData->guid = 0;
		m_CurBuyItemData->price = 0;
	}
}

void AuctionManager::AddSelItem()
{
	if (AuctionMgr->m_CurSellData && AuctionMgr->m_CurSellData->guid)
	{
		PacketBuilder->SendAuctionSellItemMsg(AuctionMgr->m_CurSellData->guid, 
													AuctionMgr->m_CurSellData->item,
													AuctionMgr->m_CurSellData->bid,
													AuctionMgr->m_CurSellData->buyout,
													AuctionMgr->m_CurSellData->etime,
													AuctionMgr->m_CurSellData->yp,
													AuctionMgr->m_CurSellData->is_yuanbao);

		AuctionMgr->SetCurDefData();
	}
}
void AuctionManager::RemoveSelItem()
{
	if (AuctionMgr->m_curRemoveAucitonItemData && AuctionMgr->m_curRemoveAucitonItemData->guid)
	{
		PacketBuilder->SendAuctionRemoveItemMsg(AuctionMgr->m_curRemoveAucitonItemData->guid,
														AuctionMgr->m_curRemoveAucitonItemData->auction_id);
		AuctionMgr->SetCurDefData();
	}
}
void AuctionManager::BuyCurItem()
{
	if (AuctionMgr->m_CurBuyItemData && AuctionMgr->m_CurBuyItemData->guid)
	{
		PacketBuilder->SendAuctionPlaceBidMsg(AuctionMgr->m_CurBuyItemData->guid,
											  AuctionMgr->m_CurBuyItemData->auction_id,
											  AuctionMgr->m_CurBuyItemData->price);
		AuctionMgr->SetCurDefData();
	}
}
void AuctionManager::ReleaseClientData()
{
	if (m_CurSellData)
	{
		delete m_CurSellData;
		m_CurSellData = NULL;
	}
	if (m_curRemoveAucitonItemData)
	{
		delete m_curRemoveAucitonItemData;
		m_curRemoveAucitonItemData = NULL;
	}

	if (m_CurBuyItemData)
	{
		delete m_CurBuyItemData;
		m_CurBuyItemData = NULL;
	}
}
BOOL AuctionManager::CreateMgrControl(UControl* ctrl)
{
	if (ctrl)
	{
		if (m_Auc == NULL)
		{
			UControl* pkControl = UControl::sm_System->CreateDialogFromFile("AuctionDlg\\AuctionAucDlg.udg");
			m_Auc = UDynamicCast(UAuctionAucDlg, pkControl);
			if (m_Auc == NULL)
			{
				ULOG("create UAuctionAucDlg UI Faild!");
				return FALSE;
			}else
			{
				ctrl->AddChild(m_Auc);
				m_Auc->SetId(FRAME_IG_AUCTIONAUCDLG);
				
			}
		}

		if (m_Browse == NULL)
		{
			UControl* pkControl = UControl::sm_System->CreateDialogFromFile("AuctionDlg\\AuctionBrowseDlg.udg");
			m_Browse = UDynamicCast(UAuctionBrowseDlg, pkControl);
			if (m_Browse == NULL)
			{
				ULOG("create UAuctionBrowseDlg UI Faild!");
				return FALSE;
			}else
			{
				ctrl->AddChild(m_Browse);
				m_Browse->SetId(FRAME_IG_AUCTIONBROWSEDLG);

			}
		}
		if (m_Big == NULL)
		{
			UControl* pkControl = UControl::sm_System->CreateDialogFromFile("AuctionDlg\\AuctionBidDlg.udg");
			m_Big = UDynamicCast(UAuctionBidDlg, pkControl);
			if (m_Big == NULL)
			{
				ULOG("create UAuctionBidDlg UI Faild!");
				return FALSE;
			}else
			{
				ctrl->AddChild(m_Big);
				m_Big->SetId(FRAME_IG_AUCTIONBIDDLG);

			}
		}

		m_Auc->SetVisible(FALSE);
		m_Big->SetVisible(FALSE);
		m_Browse->SetVisible(FALSE);
		return TRUE;
	}

	return FALSE;
	
}
void AuctionManager::ParseCommandMsg(MSG_S2C::stAuction_Command_Result pkCommand )
{
	switch(pkCommand.auction_result)
	{
	case MSG_S2C::AUCTION_CREATE:
		{
			m_Big->ClearData();
			m_BGetOwerList = FALSE;
			m_BGetAuctionList = FALSE;
			m_BGetBidderList = FALSE;
			CheckNeedSendMsg(m_State);
			ClientSystemNotify( _TRAN("Auction Created！"));
			SYState()->IAudio->PlayUiSound("Sound/UI/AuctionWindowClose.wav", NiPoint3::ZERO, 1.0f, 1 );
		//更新拍卖列表
		}
		break;
	case MSG_S2C::AUCTION_CANCEL:
		{
			m_Big->ClearData();
			m_BGetOwerList = FALSE;
			m_BGetAuctionList = FALSE;
			m_BGetBidderList = FALSE;
			CheckNeedSendMsg(m_State);
			SYState()->IAudio->PlayUiSound("Sound/UI/AuctionWindowClose.wav", NiPoint3::ZERO, 1.0f, 1 );
		}
		break;
	case MSG_S2C::AUCTION_BID:
		{
			m_BGetOwerList = FALSE;
			m_BGetAuctionList = FALSE;
			m_BGetBidderList = FALSE;
			CheckNeedSendMsg(m_State);
			ClientSystemNotify(_TRAN("You have successfully Bid on the item！"));
			SYState()->IAudio->PlayUiSound("Sound/UI/AuctionWindowClose.wav", NiPoint3::ZERO, 1.0f, 1 );
		}
		break;
	case MSG_S2C::AUCTION_BUYOUT:
		{
			m_BGetOwerList = FALSE;
			m_BGetAuctionList = FALSE;
			m_BGetBidderList = FALSE;
			CheckNeedSendMsg(m_State);
			UMessageBox::MsgBox_s(_TRAN("You have successfully bought the item！<br>"));
			SYState()->IAudio->PlayUiSound("Sound/UI/AuctionWindowClose.wav", NiPoint3::ZERO, 1.0f, 1 );
			
		}
		break;
	}

	switch(pkCommand.auction_error)
	{
	case MSG_S2C::AUCTION_ERROR_UNK1:
		break;
	case MSG_S2C::AUCTION_ERROR_INTERNAL:
		break;
	case MSG_S2C::AUCTION_ERROR_MONEY:
		{
			UMessageBox::MsgBox_s(_TRAN("Error: Not enough money！"));
		}
		break;
	case MSG_S2C::AUCTION_ERROR_ITEM:
		{
			//ReleaseOwerItem();
			UMessageBox::MsgBox_s(_TRAN("Can not bid on their own items！"));
		}
		break;
	}
}
void AuctionManager::ParseHelloMsg(MSG_S2C::stAuction_Hello  pkMsg)
{
	m_NPCGuid = pkMsg.vendorguid;
	m_Browse->SetCheckWuQi();
	HELPEVENTTRIGGER(TUTORIAL_FLAG_AUCTION);
	//string txt = "";
	//FindAuctionItem(0,txt,0,255,0,-1,-1,-1,-1);
}
void AuctionManager::ParseAuctionListMsg(MSG_S2C::stAuction_List_Result pkAuctionList)
{
	m_BGetAuctionList = TRUE;
	m_stAuctionList = pkAuctionList;
	m_Browse->SetBrowseData(m_stAuctionList);
	if (m_State == AuctionBrowse)
	{
		return ;
	}else
	{
		ChangeUAuctionState(AuctionBrowse);
	}
}
void AuctionManager::ParseAuctionBidderMsg(MSG_S2C::stAuction_Bidder_List_Result  pkBidderList)
{
	m_BGetBidderList = TRUE;
	m_stAuctionBidderList = pkBidderList;
	if (m_stAuctionList.vItems.size() == 0)
	{
		ClientSystemNotify(_TRAN("We did not find related items！"));
		if (m_State == AuctionAuc)
		{
			return ;
		}
	}
	m_Auc->SetBidderData(m_stAuctionBidderList);
	if (m_State == AuctionAuc)
	{
		return ;
	}else
	{
		ChangeUAuctionState(AuctionAuc);
	}

}
void AuctionManager::ParseAuctionOwerList(MSG_S2C::stAuction_Owner_List_Result pkList)
{
	m_BGetOwerList = TRUE;
	//收到玩家拍卖的列表
	m_stAuctionOwnerList = pkList;
	m_Big->SetOwerData(m_stAuctionOwnerList);
	//处理操作
	if (m_State == AuctionBidder_G || m_State == AuctionBidder_Y)
	{
		return ;
	}else
	{
		ChangeUAuctionState(AuctionBidder_G);
	}
	
}
BOOL AuctionManager::CheckNeedSendMsg(UAuctionState state)
{
	switch(state)
	{
		case AuctionNormal:
			break;
		case AuctionBrowse: //浏览总的物品列表
			if (!m_BGetAuctionList)
			{
				m_BGetAuctionList = TRUE;
				m_Browse->ReSeachType();
				return TRUE;
			}
			break;
		case AuctionBidder_Y: //自己拍卖列表
		case AuctionBidder_G:
			if (!m_BGetOwerList)
			{
				m_BGetOwerList = TRUE;
				FindAuctionListOwnerItems();
				return TRUE;
			}
			break;
		case AuctionAuc: //自己购买列表
			if (!m_BGetBidderList)
			{
				m_BGetBidderList = TRUE;
				FindAuctionBidderList();
				return TRUE;
			}
			break;
	}

	return FALSE;
}
void AuctionManager::ChangeUAuctionState(UAuctionState state)
{
	//检查有没有获得服务器数据，
	//如果没有获得,这里会发出请求,
	//等待服务器数据后,会对应的进行处理
	
	if (CheckNeedSendMsg(state))
	{
		return ;
	}
	m_State = state ;
	switch (state)
	{
	case AuctionNormal:
		m_Big->SetVisible(FALSE);
		m_Auc->SetVisible(FALSE);
		m_Browse->SetVisible(FALSE);
		ReleaseOwerItem();
		ClearData();
		CPlayerLocal::SetCUState(cus_Normal);
		break;
	case AuctionBrowse:
		m_Big->SetVisible(FALSE);
		m_Auc->SetVisible(FALSE);
		m_Browse->SetVisible(TRUE);
		ReleaseOwerItem();
		CPlayerLocal::SetCUState(cus_Auction);
		SYState()->IAudio->PlayUiSound("Sound/UI/AuctionWindowOpen.wav", NiPoint3::ZERO, 1.0f, 1 );
	
		break;
	case AuctionBidder_G:
		m_Big->SetVisible(TRUE);
		m_Auc->SetVisible(FALSE);
		m_Browse->SetVisible(FALSE);
		m_Big->SetYuanBaoPaimaiFlag(FALSE);
		ReleaseOwerItem();
		//需要设定
		CPlayerLocal::SetCUState(cus_Auction);
		
		break;
	case AuctionBidder_Y:
		m_Big->SetVisible(TRUE);
		m_Auc->SetVisible(FALSE);
		m_Browse->SetVisible(FALSE);
		m_Big->SetYuanBaoPaimaiFlag(TRUE);
		ReleaseOwerItem();
		CPlayerLocal::SetCUState(cus_Auction);
		
		break;
	case AuctionAuc:
		m_Big->SetVisible(FALSE);
		m_Auc->SetVisible(TRUE);
		m_Browse->SetVisible(FALSE);
		ReleaseOwerItem();
		CPlayerLocal::SetCUState(cus_Auction);
		SYState()->IAudio->PlayUiSound("Sound/UI/AuctionWindowOpen.wav", NiPoint3::ZERO, 1.0f, 1 );
		break;
	}
	m_Big->SetCheckState(state);
	m_Browse->SetCheckState(state);
	m_Auc->SetCheckState(state);
}
void AuctionManager::ClearData()
{
	m_BGetAuctionList = FALSE;
	m_BGetBidderList = FALSE;
	m_BGetOwerList = FALSE;
	m_NPCGuid = 0;
	m_State = AuctionNormal;
	m_SalePos = -1; 
}

//添加操作要求先判断是否已经有物品在列表中,
//如果有的话,先释放,
//然后添加客户端的背包锁定状态,判断相关的数据
void AuctionManager::AddOwerItem( ui8 Pos)
{
	ReleaseOwerItem();

	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if(localPlayer == NULL)
		return;

	CStorage* pItemSoltContainer = localPlayer->GetItemContainer();
	CItemSlot* pkItemSlot = (CItemSlot*)pItemSoltContainer->GetSlot(Pos);
	
	SYItem * Item = (SYItem *)ObjectMgr->GetObject(pkItemSlot->GetGUID());

	if(Item && Item->HasFlag( ITEM_FIELD_FLAGS, ITEM_FLAG_QUEST | ITEM_FLAG_SOULBOUND ))
	{
		UMessageBox::MsgBox_s(_TRAN("该物品已经绑定！"));
		return;
	}

	ItemPrototype_Client* pkItemInfo =  Item->GetItemProperty();

	if (pkItemInfo && pkItemInfo->Class == ITEM_CLASS_QUEST)
	{
		UMessageBox::MsgBox_s(_TRAN("任务道具！不允许当前操作"));
		return ; 
	}

	ui32 price = pkItemInfo->SellPrice * Item->GetUInt32Value(ITEM_FIELD_STACK_COUNT);
	//在添加等等。。
	pkItemSlot->SetLock(TRUE);
	SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG, Pos, pkItemSlot->GetCode(),pkItemSlot->GetNum(),UItemSystem::ITEM_DATA_FLAG,true);

	m_Big->SetBtnId(pkItemSlot->GetCode(), pkItemSlot->GetGUID(), price, pkItemSlot->GetNum(), pkItemInfo->C_name);
	m_SalePos = Pos;
}
void AuctionManager::SetMoney(int num)
{
	m_Auc->SetMoney(num);
	m_Big->SetMoney(num);
	m_Browse->SetMoney(num);
}
void AuctionManager::SetYuanbao(int num)
{
	m_Auc->SetYuanbao(num);
	m_Big->SetYuanbao(num);
	m_Browse->SetYuanbao(num);
}
void AuctionManager::CheckDis()
{
	if (m_NPCGuid)
	{
		CPlayerLocal* pkLcol = ObjectMgr->GetLocalPlayer();
		CCreature* pkOBJ = (CCreature*)ObjectMgr->GetObject(m_NPCGuid);

		if (pkLcol == NULL || pkOBJ == NULL)
		{
			return ;
		}else
		{
			NiPoint3 pCreaturePos = pkOBJ->GetPosition();
			NiPoint3 pLocalPlayerPos = pkLcol->GetLocalPlayerPos();

			if ((pLocalPlayerPos - pCreaturePos).Length() > MaxLenToNPC)
			{
				ChangeUAuctionState(AuctionNormal);
			}
		}
	}
}
//释放的操作要求解除背包的锁定状态。
void AuctionManager::ReleaseOwerItem()
{
	if (m_SalePos == -1)
	{
		return ;
	}

	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if(localPlayer == NULL)
		return;
	
	CStorage* pItemSoltContainer = localPlayer->GetItemContainer();
	CItemSlot* pkItemSlot = (CItemSlot*)pItemSoltContainer->GetSlot(m_SalePos);
	if(pkItemSlot) 
	{
		pkItemSlot->SetLock(FALSE);
		SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG,m_SalePos, pkItemSlot->GetCode(),pkItemSlot->GetNum(),UItemSystem::ITEM_DATA_FLAG,false);
	}
	m_Big->SetBtnId(0, 0, 0, 0, "");
	m_SalePos = -1;
}
bool AuctionManager::FindAuctionItem(uint32 startIndex, std::string& auctionString, uint8 levelRange1, uint8 levelRange2, uint8 usableCheck, int32 inventoryType, int32 itemclass, int32 itemsubclass, int32 rarityCheck)
{
	return PacketBuilder->SendAuctionListItemsMsg(m_NPCGuid,
												 startIndex,
												 auctionString,
												 levelRange1,
												 levelRange2,
												 usableCheck,
												 inventoryType,
												 itemclass,
												 itemsubclass,
												 rarityCheck);
}

bool AuctionManager::FindAuctionListOwnerItems()//查询玩家所出售的道具
{
	return PacketBuilder->SendAuctionListOwnerItems(m_NPCGuid);
}
bool AuctionManager::FindAuctionBidderList()	//列出自己购买的道具
{
	return PacketBuilder->SendAuctionListBidderItemsMsg(m_NPCGuid);
}
bool AuctionManager::AddAuctionSellItem(uint64 item,							//添加物品拍卖信息
										uint32 bid, uint32 buyout, 
										uint32 etime, bool bYP, bool bYuanBao)
{
	SYItem* pkItem = (SYItem*)ObjectMgr->GetObject(item);

	if (pkItem && m_CurSellData)
	{
		ItemPrototype_Client* pkData = pkItem->GetItemProperty();
		NIASSERT(pkData);

		std::string strbuff = _TRAN( N_NOTICE_F5, pkData->C_name.c_str());

		/*char buf[_MAX_PATH];
		NiSprintf(buf, _MAX_PATH, "你确认要拍卖%s吗？", (const char*)pkData->C_name.c_str());*/

		m_CurSellData->guid = m_NPCGuid;
		m_CurSellData->item = item;
		m_CurSellData->bid = bid;
		m_CurSellData->buyout = buyout;
		m_CurSellData->etime = etime;
		m_CurSellData->yp = bYP;
		m_CurSellData->is_yuanbao = bYuanBao;

		UMessageBox::MsgBox(strbuff.c_str(), (BoxFunction*)AuctionManager::AddSelItem);
	
	}

	return true;
	
}
bool AuctionManager::RemoveAuctionItem(uint32 auctionid)//取消物品拍卖
{
	if (m_curRemoveAucitonItemData)
	{
		m_curRemoveAucitonItemData->guid = m_NPCGuid;
		m_curRemoveAucitonItemData->auction_id = auctionid;

		UMessageBox::MsgBox(_TRAN("您需要取消拍卖该物品么？"), (BoxFunction*)AuctionManager::RemoveSelItem);
	}
	return true;
}
bool AuctionManager::BuyAuction( uint32 aucitonId, uint32 price ,bool bOnePirce)
{
	if (m_CurBuyItemData)
	{
		m_CurBuyItemData->guid = m_NPCGuid;
		m_CurBuyItemData->auction_id = aucitonId;
		m_CurBuyItemData->price = price ;
		
		string txt ;
		if (bOnePirce)
		{
			txt = _TRAN("您确认直接购买该物品么？");
		}else
		{
			txt = _TRAN("您确认竞拍该物品么？") ;
		}


		UMessageBox::MsgBox(txt.c_str(), (BoxFunction*)AuctionManager::BuyCurItem);

	}
	return true;
}