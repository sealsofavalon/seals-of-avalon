#ifndef __UAUCTIONMGR_H__
#define __UAUCTIONMGR_H__

//////////////////////////////////////////////////////////////////////////
//                     拍卖行数据管理                                   //
//																		//
//////////////////////////////////////////////////////////////////////////


#include "UInc.h"
#include "UAuctionInc.h"
#include "Singleton.h"

enum AuctionPage
{
	AUCTION_PAGEINDEX_BROWSE = 100,
	AUCTION_PAGEINDEX_AUC,
	AUCTION_PAGEINDEX_JINBI,
	AUCTION_PAGEINDEX_YUANBAO,
};

const static int AuFindListLength = 7;
#define  AUMgr AUManager::Instance()
class AUManager
{
public:
	AUManager();
	~AUManager();

	static AUManager* Instance();
	void Initialize();
	void Destory();
	BOOL CreateFrame(UControl* Frame);
	static AUManager* m_AUMgr;

	void ShowFrame();
	void HideFrame();
	void OnChangePage();
public:

	//	消息处理
	void ParseHelloMsg(MSG_S2C::stAuction_Hello  pkMsg); //收到打开拍卖行的消息
	void ParseCommandMsg(MSG_S2C::stAuction_Command_Result pkCommand ); //收到操作结果
	void ParseAuctionListMsg(MSG_S2C::stAuction_List_Result pkAuctionList); //收到拍卖行查询列表
	void ParseAuctionBidderMsg(MSG_S2C::stAuction_Bidder_List_Result  pkBidderList); //收到玩家购买的道具列表
	void ParseAuctionOwerList(MSG_S2C::stAuction_Owner_List_Result pkList); //收到玩家出售的列表

	bool SendAddAuctionSellItem(uint64 item,uint32 bid, uint32 buyout, uint32 etime, bool bYP, bool bYuanBao);		//bid是竞标价 buyout是一口价
	bool SendRemoveAuctionItem(uint32 auctionid);//取消物品拍卖
	bool SendBuyAuction( uint32 aucitonId, uint32 price, bool bOnePirce = false);
	bool SendFindAuctionItem(uint32 startIndex, std::string& auctionString,uint8 levelRange1, uint8 levelRange2, uint8 usableCheck,int32 inventoryType, int32 itemclass, int32 itemsubclass, int32 rarityCheck); //查询物品
	bool SendFindAuctionListOwnerItems();//查询玩家所出售的道具
	bool SendFindAuctionBidderList();	//查询自己竞标的道具

public:
	void SetMoney(int num);
	void SetYuanbao(int num);
	int GetMoney(){return m_MoneyNum;}
	int GetYuanBao(){return m_YuanBaoNum;}
	int GetSaleItemTax(int hour);
	void SetShowYuanBao(bool vis);
public:
	void SetSellItemIn(ui8 Pos);
	void ReleaseItem();
	ui32 GetSaleItemEntry(){return m_SaleItemEntry;};
	ui64 GetSaleItemGUID(){return m_SaleItemGuid;};
public:
	ui64 GetNPCGuid(){return m_NPCGuid;}
	void CheckDis();
	ui64 m_NPCGuid;
public:
	bool CheckOwerState();
	bool GetBrowseItemList(AUItemList* pList);
	bool GetBidItemList(int Page, AUItemList* pList);
	int GetBidItemListMaxPage(){ return m_stAuctionBidderList.vItems.size() / (AuFindListLength + 1);};
	bool GetOwnerItemList(int Page, bool IsYuanBao,AUItemList* pList);
	int GetOwnerItemListMaxPage(bool IsYuanBao);

public:
	void InitClientData();
	void SetCurDefData();
	void ReleaseClientData();

	static void AddSelItem();
	static void RemoveSelItem();
	static void BuyCurItem();
protected:
	//这里保存主要是为了提示信息的数据保存
	MSG_C2S::stAuction_Sell_Item* m_CurSellData;
	MSG_C2S::stAuction_Remove_Item*  m_curRemoveAucitonItemData; //当前取消拍卖的物品
	MSG_C2S::stAuction_Place_Bid* m_CurBuyItemData; //当前购买的数据。

	class UAuctionFrame* m_AuctionFrame;	

	
	stAuctionList  m_stAuctionList;         //拍卖行道具列表
	stAuctionBidder m_stAuctionBidderList;  //本人竞拍的道具列表
	stAuctionOwer m_stAuctionOwnerList;     //本人拍卖的道具列表

	AUItemList m_stAuctionJinBiList;
	AUItemList m_stAuctionYuanBaoList;

	int m_MoneyNum;
	int m_YuanBaoNum;
	int m_SalePos;

	ui32 m_SaleItemEntry;
	ui64 m_SaleItemGuid;
};
#define AuctionMgr AuctionManager::GetSingleton()
class AuctionManager : public Singleton<AuctionManager>
{
	SINGLETON(AuctionManager);
public:
	//标示UI窗口状态
	enum UAuctionState
	{
		AuctionNormal = 0, // 普通, 代表所有相关拍卖行界面没打开
		AuctionAuc,        // 打开自己竞拍的列表
		AuctionBidder_G,   // 打开自己拍卖 (金币拍卖)
		AuctionBidder_Y,   // 打开自己拍卖 (元宝拍卖)
		AuctionBrowse,     // 打开拍卖行物品列表
	};
	BOOL CreateMgrControl(UControl* ctrl);  //UI
	void ChangeUAuctionState(UAuctionState state); 
	void ClearData();
public:
	//消息相关
	//////////////////////////////////////////////////////////////////////////
	//						服务器发下来的消息处理                          //
	//////////////////////////////////////////////////////////////////////////
	void ParseHelloMsg(MSG_S2C::stAuction_Hello  pkMsg); //收到打开拍卖行的消息
	void ParseCommandMsg(MSG_S2C::stAuction_Command_Result pkCommand ); //收到操作结果
	void ParseAuctionListMsg(MSG_S2C::stAuction_List_Result pkAuctionList); //收到拍卖行查询列表
	void ParseAuctionBidderMsg(MSG_S2C::stAuction_Bidder_List_Result  pkBidderList); //收到玩家购买的道具列表
	void ParseAuctionOwerList(MSG_S2C::stAuction_Owner_List_Result pkList); //收到玩家出售的列表
	
	//////////////////////////////////////////////////////////////////////////
	//							客户端操作信息                              //
	//////////////////////////////////////////////////////////////////////////
	BOOL CheckNeedSendMsg(UAuctionState state);
	
	bool AddAuctionSellItem(uint64 item,							//添加物品拍卖信息
							uint32 bid, uint32 buyout, 
							uint32 etime, bool bYP, bool bYuanBao);		//bid是竞标价 buyout是一口价
	bool RemoveAuctionItem(uint32 auctionid);//取消物品拍卖
	bool BuyAuction( uint32 aucitonId, uint32 price, bool bOnePirce = FALSE);//一口价买进

	bool FindAuctionItem(uint32 startIndex, std::string& auctionString, //查询拍卖行信息
						uint8 levelRange1, uint8 levelRange2, uint8 usableCheck, 
						int32 inventoryType, int32 itemclass, int32 itemsubclass, int32 rarityCheck);
	bool FindAuctionListOwnerItems();//查询玩家所出售的道具
	bool FindAuctionBidderList();	//列出自己购买的道具


	//检查与NPC的距离，来设置相关的数据
	ui64 GetNPCGuid(){return m_NPCGuid;}

	void CheckDis();
	void SetMoney(int num);
	void SetYuanbao(int num);
public:
	void InitClientData();
	void SetCurDefData();
	void ReleaseClientData();

	static void AddSelItem();
	static void RemoveSelItem();
	static void BuyCurItem();
protected:
	//拍卖行操作数据。
	//这里保存主要是为了提示信息的数据保存
	MSG_C2S::stAuction_Sell_Item* m_CurSellData;
	MSG_C2S::stAuction_Remove_Item*  m_curRemoveAucitonItemData; //当前取消拍卖的物品
	MSG_C2S::stAuction_Place_Bid* m_CurBuyItemData; //当前购买的数据。
	 

	stAuctionList  m_stAuctionList;         //拍卖行道具列表
	stAuctionBidder m_stAuctionBidderList;  //本人竞拍的道具列表
	stAuctionOwer m_stAuctionOwnerList;     //本人拍卖的道具列表

	BOOL m_BGetAuctionList ;  //是否获得了列表
	BOOL m_BGetBidderList;    //是否获得了购买的列表
	BOOL m_BGetOwerList;	  //是否获得了出售的列表

	ui64 m_NPCGuid;  //当前拍卖行老板的ID
public:
	bool CheckOwerState(){return (m_State == AuctionBidder_G || m_State == AuctionBidder_Y) ;}
	void AddOwerItem(ui8 Pos);
	void ReleaseOwerItem();
protected:
	int  m_SalePos;   //当前客户端操作物品的Pos

	class UAuctionBidDlg* m_Big;   //自己拍卖的物品界面(包括金币和元宝拍卖)
	class UAuctionAucDlg* m_Auc;	 //自己竞拍的物品界面
	class UAuctionBrowseDlg* m_Browse; //浏览整个拍卖行物品的界面
	UAuctionState m_State ;
};
#endif