#include "StdAfx.h"
#include "UAuraContent.h"
#include "Character.h"
#include "ObjectManager.h"
#include "UIShowPlayer.h"
#include "UITipSystem.h"
#include "Utils/SpellDB.h"
#include "UIIndex.h"
#include "Skill/SkillManager.h"
#include "Network/PacketBuilder.h"
#include "UAuraSys.h"
#include "SocialitySystem.h"

const int ItemPadding  = 3;

void AuraItem::SetData(ui32 SpellID, int count, float dt, float maxdt)
{
	AuraCount = count;
	DurTime = dt;
	MaxDurTime = maxdt;

	if (pSpellTemplate == NULL || SpellId != SpellID)
	{
		pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate(SpellID);
		//if (pSpellTemplate && pSpellTemplate->GetDuration(MaxDurTime))
		//	MaxDurTime /= 1000.f;

		this->SpellId = SpellID;
		IconSkin = NULL;
		IconSkin = GetSpellIcon(ItemIconIndex);
	}

	if ( DurTime > 0.0f )
	{

		if (DurTime >= 5.0f * 3600)
		{
			Timestr = "N/A";
			return;
		}
		int hour = 0,minute = 0 ,second = 0;
		hour = (int)DurTime/3600;
		if (hour >= 1)
		{
			/*char buf[10];
			itoa( hour + 1, buf, 10);*/
		
			//sprintf(buf, "<%dʱ", hour + 1);
			Timestr = _TRAN( N_NOTICE_F6, _I2A(hour + 1).c_str() );
			return;
		}
		minute = (int)(DurTime - hour*3600)/60;
		if (minute >= 1)
		{
			//sprintf(buf, "%d��", minute);
			Timestr = _TRAN( N_NOTICE_F7, _I2A(minute).c_str() );
			return;
		}
		second = int(DurTime)%60;
		if (second >= 0)
		{
			//sprintf(buf, "%d��", second);
			Timestr = _TRAN( N_NOTICE_F8, _I2A(second).c_str() );
			return;
		}
	}
	else
	{
		Timestr = "N/A";
	}
}

USkinPtr AuraItem::GetSpellIcon(int &IconIndex)
{
	if (SpellId == 0)
		return NULL;

	if (IconSkin)
	{
		IconIndex = ItemIconIndex;
		return IconSkin;
	}

	if (pSpellTemplate)
	{
		std::string icon;
		pSpellTemplate->GetICON(icon);

		int pos = icon.find(";");
		std::string skinstr = icon.substr(0,pos);
		std::string strIndex = icon.substr(pos+1,icon.length() -1);
		ItemIconIndex = atoi(strIndex.c_str());
		if (IconSkin == NULL)
		{
			IconSkin = UControl::sm_System->LoadSkin(skinstr.c_str());
		}
		return IconSkin;	
	}
	return NULL;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

INT SmallAuraItem::Render(URender* plugin,  const URect& rect, UControlStyle* style)
{
	if(!IconSkin || !plugin || !style)
		return 0;

	URect screenRect = rect;

	plugin->DrawSkin(IconSkin,ItemIconIndex,screenRect);
	plugin->DrawCoolDownMask(screenRect, DurTime / MaxDurTime, LCOLOR_ARGB(100,255,255,255));

	if(AuraCount > 1)
	{
		char buf[56];
		sprintf(buf, "%d", AuraCount);
		UPoint countpos(rect.pt0.x , rect.pt0.y + rect.GetHeight() - style->m_spFont->GetHeight());
		UPoint countsize(rect.GetWidth(), style->m_spFont->GetHeight());
		UDrawText(plugin, style->m_spFont, countpos + UPoint(1,1), countsize, LCOLOR_BLACK, buf, UT_RIGHT);
		UDrawText(plugin, style->m_spFont, countpos , countsize, 0xffffff00, buf, UT_RIGHT);
	}
	return 1;
}

INT BigAuraItem::Render(URender* plugin,  const URect& rect, UControlStyle* style)
{
	if(!IconSkin || !plugin || !style)
		return 0;

	URect screenRect = rect;

	UPoint timepos(rect.pt0.x , rect.pt0.y + rect.GetHeight() - style->m_spFont->GetHeight());
	UPoint timesize(rect.GetWidth(), style->m_spFont->GetHeight());
	UColor color(255,255,255,255);
	if(DurTime < 20.0f && DurTime > 0.f)
	{
		int iTime = (int)DurTime;
		float falpha = NiSin((DurTime - iTime + iTime%2) * NI_HALF_PI);
		color.a = (BYTE)min(255, 128 * ( 1 + falpha));
	}
	URect skinRect = screenRect;
	skinRect.SetSize(screenRect.GetWidth(), screenRect.GetWidth());
	plugin->DrawSkin(IconSkin,ItemIconIndex,skinRect,color);
	if(slot >= MAX_POSITIVE_AURAS)
		plugin->DrawImage(AuraSystem->m_DebuffTex, skinRect, color);

	USetTextEdge(TRUE);
	UDrawText(plugin, style->m_spFont, timepos, timesize, 0xffffff00, Timestr.c_str(), UT_CENTER);
	USetTextEdge(FALSE);

	if(AuraCount > 1)
	{
		char buf[56];
		sprintf(buf, "%d", AuraCount);
		timepos.Set(rect.pt0.x , rect.pt0.y + skinRect.GetHeight() - style->m_spFont->GetHeight());
		UDrawText(plugin, style->m_spFont, timepos + UPoint(1,1), timesize, LCOLOR_BLACK, buf, UT_RIGHT);
		UDrawText(plugin, style->m_spFont, timepos , timesize, 0xffffff00, buf, UT_RIGHT);
	}
	return 3;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UAuraContent,UControl);
void UAuraContent::StaticInit(){}
UAuraContent::UAuraContent()
{
	mGUID = 0;
	mType = AURAITEM_TYPE_NORMAL;
	mpChar = NULL;
	Clear();
}
UAuraContent::~UAuraContent()
{
	Clear();
}
BOOL UAuraContent::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	return TRUE;
}
void UAuraContent::OnDestroy()
{
	SetTargetID(0);
	UControl::OnDestroy();
}
void UAuraContent::OnRender(const UPoint& offset,const URect &updateRect)
{
	if(!m_AuraItem.size())
		return;
	URect ScreenRect ;
	std::map<UINT,AuraItem*>::iterator it = m_AuraItem.begin();
	//0 - 39buff 40- 55debuff
	while(it != m_AuraItem.end())
	{
		AuraItem* pAuraItem = it->second;
		//����	
		ScreenRect =  pAuraItem->FindThisRect;
		ScreenRect.Offset(offset);
		pAuraItem->Render(sm_UiRender, ScreenRect, m_Style);
		++it;
	}
}
void UAuraContent::OnTimer(float fDeltaTime)
{
	if (mGUID)
	{
		mpChar = (CCharacter*)ObjectMgr->GetObject(mGUID);
		if (mpChar)
		{
			for (int i = 0 ; i < MAX_AURAS ; i++)
			{
				CCharacter::AuraInfo* pAurainfo = mpChar->GetAura(i);
				ui32 spellid = pAurainfo->SpellId;
				bool updatamsk = pAurainfo->UpdateMsk;
				float time = pAurainfo->AuraTime;
				float maxtime = pAurainfo->MaxAuraTime;
				int count = pAurainfo->level;
				SetSlotAura(i, spellid, count, updatamsk, time, maxtime);
			}
		}
		else
		{
			if (SocialitySys->GetTeamSysPtr()->InTeam(mGUID))
			{
				Clear();
				//����
			}
		}	
	}
	else
	{
		Clear();
	}
}
void UAuraContent::OnMouseMove(const UPoint& position, UINT flags)
{
	if (!TipSystem)
	{
		return UControl::OnMouseMove(position,flags);
	}
	if(m_CurrAuraItem)
	{
		UPoint pos = m_CurrAuraItem->FindThisRect.GetPosition() + m_CurrAuraItem->FindThisRect.GetSize()+ WindowToScreen(UPoint(0,0));
		TipSystem->ShowTip(TIP_TYPE_BUFF, m_CurrAuraItem->SpellId,pos);
		UControl::OnMouseMove(position,flags);
		return;
	}
	TipSystem->HideTip();	
}

void UAuraContent::Clear()
{
	if (m_AuraItem.size())
		m_AuraItem.clear();
	
	m_CurrAuraItem = NULL;
}
void UAuraContent::OnMouseEnter(const UPoint& position, UINT flags)
{
	UControl::OnMouseEnter(position,flags);
}
void UAuraContent::OnMouseLeave(const UPoint& position, UINT flags)
{
	if (TipSystem)
	{
		TipSystem->HideTip();
	}
	m_CurrAuraItem = NULL;
}

UControl* UAuraContent::FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */)
{
	std::map<UINT,AuraItem*>::iterator it = m_AuraItem.begin();
	while(it != m_AuraItem.end())
	{
		AuraItem* pAuraItem = it->second;
		if (pAuraItem->FindThisRect.PointInRect(pt))
		{
			m_CurrAuraItem = pAuraItem;
			return UControl::FindHitWindow(pt, initialLayer);
		}++it;
	}
	return GetParent();
}
void UAuraContent::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	if (mGUID != ObjectMgr->GetLocalPlayer()->GetGUID())
	{
		return;
	}
	if( m_CurrAuraItem )
	{
		SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate(m_CurrAuraItem->SpellId);
		if(pSpellTemplate && pSpellTemplate->GetAuraInfo() & SPELL_AVOID_MASK_AURA_CANNOT_REMOVE_BYSELF)
			return;

		PacketBuilder->SendCancelAura(m_CurrAuraItem->SpellId);
	}
}
void UAuraContent::SetTargetID(ui64 guid)
{	
	mGUID = guid;
	if (!mGUID)
	{
		mpChar = NULL;
		SetVisible(FALSE);
		Clear();
		return;
	}
	SetVisible(TRUE);

	mpChar = (CCharacter *)ObjectMgr->GetObject(mGUID);
}
ui64 UAuraContent::GetTargetID()
{
	return mGUID;
}
void UAuraContent::SetType(int type)
{
	mType = type;
	if(mType == AURAITEM_TYPE_NORMAL)
	{
		mItemSize.Set(24,24);
	}else if(mType == AURAITEM_TYPE_WITHCLOCK)
	{
		mItemSize.Set(32,32 + m_Style->m_spFont->GetHeight());
	}else if (mType == AURAITEM_TYPE_SMALL)
	{
		mItemSize.Set(15,15);
	}
}
void UAuraContent::SetSlotAura(int slot , ui32 spellid, int count,  bool updatamsk, float time, float maxtime)
{
	std::map<UINT,AuraItem*>::iterator it = m_AuraItem.find(slot);
	if(it != m_AuraItem.end())
	{
		AuraItem* pAuraItem = it->second;
		if (updatamsk)
		{
			pAuraItem->SetData(spellid, count, time, maxtime);
		}else
		{
			if(m_CurrAuraItem == pAuraItem)
				m_CurrAuraItem = NULL;

			delete pAuraItem;
			m_AuraItem.erase(it);
			CalculatePosition();
		}
		return;
	}
	if (spellid && updatamsk)
	{
		AuraItem* tempItem = NULL;

		if(mType == AURAITEM_TYPE_NORMAL)	{
			tempItem = new SmallAuraItem;
		}else if(mType == AURAITEM_TYPE_WITHCLOCK){
			tempItem = new BigAuraItem;
		}else if (mType == AURAITEM_TYPE_SMALL){
			tempItem = new SmallAuraItem;
		}

		if( tempItem )
		{
			tempItem->SpellId = spellid;
			tempItem->slot = slot;
			tempItem->AuraCount = count;
			m_AuraItem.insert(std::map<UINT,AuraItem*>::value_type(slot, tempItem));
			CalculatePosition();
		}
	}
}

void UAuraContent::CalculatePosition()
{
	int Height = 0; 
	bool bTemp = true;
	INT Padding = 1;
	UPoint Positon(0, 0);
	if(mType == AURAITEM_TYPE_NORMAL)	{
		Padding = 1;
		Positon.Set(Padding, Padding);
	}else if(mType == AURAITEM_TYPE_WITHCLOCK){
		Padding = 3;
		Positon.Set(m_Size.x - (mItemSize.x + Padding), Padding);
	}else if (mType == AURAITEM_TYPE_SMALL){
		Padding = 1;
		Positon.Set(Padding, Padding);
	}
	std::map<UINT,AuraItem*>::iterator it = m_AuraItem.begin();
	int width = mItemSize.x;
	int height = mItemSize.y;
	BOOL bHavePositive = FALSE;
	while(it != m_AuraItem.end())
	{
		AuraItem* pAuraItem = it->second;
		if(pAuraItem->slot < MAX_POSITIVE_AURAS/* && mType != AURAITEM_TYPE_SMALL*/)
		{
			//for buff;
			bHavePositive = TRUE;
			pAuraItem->FindThisRect.SetPosition ( Positon );
			pAuraItem->FindThisRect.SetSize ( width,  height);
			if(mType == AURAITEM_TYPE_NORMAL || mType == AURAITEM_TYPE_SMALL)	{
				Positon.x  += Padding + width ;
				if(Positon.x + Padding + width > m_Size.x)
				{
					Positon.x = Padding;
					Positon.y += mItemSize.y + Padding;
				}
			}else if(mType == AURAITEM_TYPE_WITHCLOCK){
				Positon.x -= mItemSize.x + Padding;
				if (Positon.x - mItemSize.x - Padding < 0)
				{
					Positon.x = m_Size.x - mItemSize.x - Padding;
					Positon.y += mItemSize.y + Padding;
				}
			}
		}
		else if(pAuraItem->slot >= MAX_POSITIVE_AURAS && pAuraItem->slot < MAX_AURAS)
		{
			//for debuff
			if(bTemp)
			{
				bTemp = false;
				if(mType == AURAITEM_TYPE_NORMAL){
					Height +=  bHavePositive * mItemSize.y + Padding;
					Positon.Set(Padding, Height);
				}else if(mType == AURAITEM_TYPE_WITHCLOCK){
					Height += 2 * mItemSize.y + Padding;
					Positon.Set(m_Size.x - (mItemSize.x + Padding), Height);
				}else if(mType == AURAITEM_TYPE_SMALL){
					Height += 0;
					//Positon.Set(Padding, Height);
				}
			}
			pAuraItem->FindThisRect.SetPosition ( Positon );
			if(mType == AURAITEM_TYPE_NORMAL || mType == AURAITEM_TYPE_SMALL){
				if (mType == AURAITEM_TYPE_NORMAL)
				{
					width = mItemSize.x - 4;
					height = mItemSize.y - 4;
				}
				pAuraItem->FindThisRect.SetSize ( width, height);
				Positon.x  += Padding + width;
				if(Positon.x + Padding + width > m_Size.x)
				{
					Positon.x = Padding;
					Positon.y += mItemSize.y + Padding;
				}
			}else if(mType == AURAITEM_TYPE_WITHCLOCK){
				pAuraItem->FindThisRect.SetSize ( width, height);
				Positon.x -= mItemSize.x + Padding;
				if (Positon.x - mItemSize.x - Padding < 0)
				{
					Positon.x = m_Size.x - mItemSize.x - Padding;
					Positon.y += mItemSize.y + Padding;
				}
			}
		}
		Height = Positon.y;
		++it;
	}
	SetHeight(Height + mItemSize.y + Padding);
}