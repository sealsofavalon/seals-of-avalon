#ifndef UAURACONTENT_H
#define UAURACONTENT_H

enum
{
	AURAITEM_TYPE_NORMAL,
	AURAITEM_TYPE_WITHCLOCK,
	AURAITEM_TYPE_SMALL,
};
struct AuraItem
{
	UINT slot;
	ui32 SpellId;
	int AuraCount;
	std::string Timestr;
	URect FindThisRect;
	class SpellTemplate* pSpellTemplate;
	
	USkinPtr IconSkin;
	int ItemIconIndex;

	float DurTime;
	float MaxDurTime;
public:
	AuraItem(){InitData();};
	inline void InitData(){slot = 0; SpellId = 0; DurTime = 0.0f;MaxDurTime = 0.0f;IconSkin = NULL;pSpellTemplate = NULL;};
	inline void SetData(ui32 SpellID, int count, float dt, float maxdt);
	USkinPtr GetSpellIcon(int &IconIndex);
	virtual INT Render(URender* plugin,  const URect& rect, UControlStyle* style = NULL){return 0;};
};

struct SmallAuraItem : public AuraItem
{
	virtual INT Render(URender* plugin,  const URect& rect, UControlStyle* style = NULL);
};

struct BigAuraItem : public AuraItem
{
	virtual INT Render(URender* plugin,  const URect& rect, UControlStyle* style = NULL);
};

class UAuraContent : public UControl
{
	UDEC_CLASS(UAuraContent);
public:
	UAuraContent();
	~UAuraContent();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */);
public:
	void SetTargetID(ui64 guid);
	ui64 GetTargetID();
	void SetType(int type);
	void Clear();
private:
	void SetSlotAura(int slot , ui32 spellid, int count, bool updatamsk, float time, float maxtime);
	void CalculatePosition();
	ui64 mGUID;
	UINT mType;
	UPoint mItemSize;

	class CCharacter* mpChar;
	std::map<UINT,AuraItem*> m_AuraItem;
	AuraItem* m_CurrAuraItem;
};
#endif