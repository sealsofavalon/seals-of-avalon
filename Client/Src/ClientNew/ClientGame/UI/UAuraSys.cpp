#include "stdafx.h"
#include "UAuraSys.h"
#include "UAuraContent.h"
#include "UIIndex.h"


UAuraSys::UAuraSys()
{
	for (int i = 0 ; i < AURA_MAX ; i++)
	{
		m_Aura[i] = NULL;
	}
	m_DebuffTex = NULL;
}
UAuraSys::~UAuraSys()
{

}
BOOL UAuraSys::CreateSystem(UControl *Ctrl)
{
	if (m_Aura[AURA_LOCALSMALL] == NULL)
	{
		m_Aura[AURA_LOCALSMALL] = UControl::sm_System->CreateDialogFromFile("Aura\\AuraLocalSmall.udg");
		if (m_Aura[AURA_LOCALSMALL])
		{
			UAuraContent *uac = UDynamicCast(UAuraContent,m_Aura[AURA_LOCALSMALL]);
			if (uac)
				uac->SetType(AURAITEM_TYPE_NORMAL);
			Ctrl->AddChild(m_Aura[AURA_LOCALSMALL]);
			Ctrl->SetId(FRAME_IG_AURALOCALSMALL);
		}
		else
		{
			return FALSE;
		}
	}

	if (m_Aura[AURA_LOCALBIG] == NULL)
	{
		m_Aura[AURA_LOCALBIG] = UControl::sm_System->CreateDialogFromFile("Aura\\AuraLocalBig.udg");
		if (m_Aura[AURA_LOCALBIG])
		{
			UAuraContent *uac = UDynamicCast(UAuraContent,m_Aura[AURA_LOCALBIG]);
			if (uac)
				uac->SetType(AURAITEM_TYPE_WITHCLOCK);
			Ctrl->AddChild(m_Aura[AURA_LOCALBIG]);
			Ctrl->SetId(FRAME_IG_AURALOCALBIG);
		}
		else
		{
			return FALSE;
		}
	}

	if (m_Aura[AURA_TARGET] == NULL)
	{
		m_Aura[AURA_TARGET] = UControl::sm_System->CreateDialogFromFile("Aura\\AuraTarget.udg");
		if (m_Aura[AURA_TARGET])
		{
			UAuraContent *uac = UDynamicCast(UAuraContent,m_Aura[AURA_TARGET]);
			if (uac)
				uac->SetType(AURAITEM_TYPE_NORMAL);	
			Ctrl->AddChild(m_Aura[AURA_TARGET]);
			Ctrl->SetId(FRAME_IG_AURATARGET);
		}
		else
		{
			return FALSE;
		}
	}
	for ( int i = 0 ; i < 4 ; i++)
	{
		if (m_Aura[AURA_TEAMMEM + i] == NULL)
		{
			char buf[256];
			sprintf(buf, "Aura\\AuraTeammem_%d.udg", i);
			m_Aura[AURA_TEAMMEM + i] = UControl::sm_System->CreateDialogFromFile(buf);
			if (m_Aura[AURA_TEAMMEM + i])
			{
				UAuraContent *uac = UDynamicCast(UAuraContent,m_Aura[AURA_TEAMMEM + i]);
				if (uac)
					uac->SetType(AURAITEM_TYPE_SMALL);
				Ctrl->AddChild(m_Aura[AURA_TEAMMEM + i]);
				Ctrl->SetId(FRAME_IG_AURATEAMMEM1 + i);
			}
			else
			{
				return FALSE;
			}
		}
	}
	if(m_DebuffTex == NULL)
	{
		m_DebuffTex = UControl::sm_UiRender->LoadTexture("DATA\\UI\\Public\\Lock.png");
		if(!m_DebuffTex)
			return FALSE;
	}
	return TRUE;
}
void UAuraSys::SetGuid(int Type, ui64 guid)
{
	//if (guid)
	{
		switch (Type)
		{
		case AURA_LOCALSMALL:
			{
				UAuraContent *uac = UDynamicCast(UAuraContent,m_Aura[AURA_LOCALSMALL]);
				if (uac && uac->GetTargetID() != guid)
				{
					uac->Clear();
					uac->SetTargetID(guid);
				}
			}
			break;
		case AURA_LOCALBIG:
			{
				UAuraContent *uac = UDynamicCast(UAuraContent,m_Aura[AURA_LOCALBIG]);
				if (uac && uac->GetTargetID() != guid)
				{
					uac->Clear();
					uac->SetTargetID(guid);
				}
			}
			break;
		case AURA_TARGET:
			{
				UAuraContent * uac = UDynamicCast(UAuraContent,m_Aura[AURA_TARGET]);
				if (uac && uac->GetTargetID() != guid)
				{
					uac->Clear();
					uac->SetTargetID(guid);
				}
			}
			break;
		case AURA_TEAMMEM:
		case AURA_TEAMMEM + 1:
		case AURA_TEAMMEM + 2:
		case AURA_TEAMMEM + 3:
			{
				UAuraContent * udac = UDynamicCast(UAuraContent,m_Aura[Type]);
				if (udac && udac->GetTargetID() != guid)
				{
					udac->Clear();
					udac->SetTargetID(guid);
				}
			}
			break;
		}
	}
}