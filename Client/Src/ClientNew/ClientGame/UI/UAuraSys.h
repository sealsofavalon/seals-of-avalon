#ifndef UAURASYS_H
#define UAURASYS_H
#include "UInc.h"
#include "Singleton.h"

#define AuraSystem UAuraSys::GetSingleton() 
struct AuraSpellInfo ;
enum
{
	AURA_LOCALSMALL,
	AURA_LOCALBIG,
	AURA_TARGET,
	AURA_TEAMMEM,
	AURA_MAX = AURA_TEAMMEM + 4
};
class UAuraSys  : public Singleton<UAuraSys>
{
	SINGLETON(UAuraSys);
public:
	BOOL CreateSystem(UControl * Ctrl);
public:
	void SetGuid(int Type,ui64 guid);
	UTexturePtr m_DebuffTex;
private:
	UControl * m_Aura[AURA_MAX];//只用前3个队友情况忽略掉~
};

#endif