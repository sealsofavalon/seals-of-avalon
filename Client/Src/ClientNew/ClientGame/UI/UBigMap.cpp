#include "StdAfx.h"
#include "UBigMap.h"
#include "ObjectManager.h"
#include "Map/Map.h"
#include "Utils/MapInfoDB.h"
#include "Utils/NPCListFile.h"
#include "Utils/CreatureSpawnsDB.h"
#include "ui/UIGamePlay.h"
#include "Utils/MapToMapPathInfo.h"
#include "SocialitySystem.h"
#include "AudioDef.h"
#include "ItemManager.h"
#include "ui/SystemTips .h"
#include "UITipSystem.h"
#include "UFun.h"
#include "UInstanceMgr.h"

UIMP_CLASS(UBlendBitmap, UBitmap);
UBEGIN_MESSAGE_MAP(UBlendBitmap, UCmdTarget)
UEND_MESSAGE_MAP()

void UBlendBitmap::StaticInit()
{
}

void UBlendBitmap::OnRender(const UPoint& offset, const URect &updateRect)
{
	URect ClientRect(offset, m_Size);
	if (m_spTexture)
		sm_UiRender->DrawImage(m_spTexture, ClientRect, m_Style->mFillColor);

	RenderChildWindow(offset, updateRect);
}

UIMP_CLASS(UBigMapFrame, UControl);
UBEGIN_MESSAGE_MAP(UBigMapFrame, UCmdTarget)
UON_BN_CLICKED(2, &UBigMapFrame::OnClickClose)
UON_SLIDER_CHANGE(4, &UBigMapFrame::OnChangeBigMapAlpha)
UON_UGD_CLICKED(7, &UBigMapFrame::OnClickSelChange)
UON_UGD_DBCLICKED(7, &UBigMapFrame::OnClickFindPath)
UON_BN_CLICKED(8, &UBigMapFrame::OnClickShow)
UON_BN_CLICKED(9, &UBigMapFrame::OnClickShow)
UON_BN_CLICKED(10, &UBigMapFrame::OnClickShow)
UON_UEDIT_UPDATE(11, &UBigMapFrame::OnClickSeach)
UON_BN_CLICKED(12,&UBigMapFrame::OnClickSeach)
UEND_MESSAGE_MAP()

void UBigMapFrame::StaticInit()
{
}

UBigMapFrame::UBigMapFrame()
:m_pkBigMap(NULL),m_AreaMapName(NULL),m_pkNPCList(NULL),m_ShowNormalNpc(NULL),m_ShowTitleNpc(NULL),m_ShowGuaiwu(NULL),m_Mapid(0),m_SeachText(NULL),m_SeachBtn(NULL)
{
}

BOOL UBigMapFrame::OnCreate()
{
	if (!UControl::OnCreate())
		return FALSE;

	if (m_AreaMapName == NULL)
	{
		m_AreaMapName = UDynamicCast(UStaticText, GetChildByID(3));
		if (!m_AreaMapName)
		{
			return FALSE;
		}
	}
	SetAlpha(1.f);

	m_pkBigMap = (UBigMap*)GetChildByID(1);

	USlider* pSlider = UDynamicCast(USlider, GetChildByID(4));
	if (pSlider)
	{
		pSlider->SetRange(1, 255);
	}

	UScrollBar* pScrollBar = (UScrollBar*)GetChildByID(6);
	if (pScrollBar == NULL)
	{
		return FALSE;
	}
	pScrollBar->SetBarShowMode(USBSM_FORCEON,USBSM_FORCEOFF);
	pScrollBar->scrollRectVisible(URect(0,0,0,0));
	m_pkNPCList = UDynamicCast(UListBox, pScrollBar->GetChildByID(7));
	if (!m_pkNPCList)
	{
		return FALSE;
	}

	m_ShowNormalNpc = UDynamicCast(UBitmapButton, GetChildByID(8));
	m_ShowTitleNpc = UDynamicCast(UBitmapButton, GetChildByID(9));
	m_ShowGuaiwu = UDynamicCast(UBitmapButton,GetChildByID(10));

	if (!m_ShowGuaiwu || !m_ShowTitleNpc || !m_ShowGuaiwu)
	{
		return FALSE;
	}

	m_ShowNormalNpc->SetCheckState(TRUE);
	m_ShowTitleNpc->SetCheckState(TRUE);
	m_ShowGuaiwu->SetCheckState(TRUE);

	m_SeachText = UDynamicCast(UEdit, GetChildByID(11));
	m_SeachBtn = UDynamicCast(UBitmapButton,GetChildByID(12));
	if (!m_SeachBtn || !m_SeachText)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL UBigMapFrame::OnEscape()
{
	if (!UControl::OnEscape())
	{
		OnClickClose();
		return TRUE;
	}
	return FALSE;
}

void UBigMapFrame::OnClickClose()
{
	SetVisible(false);
	m_pkBigMap->SetDrawNpcData(NULL);
}
void UBigMapFrame::OnClickSelChange()
{
	if (m_pkNPCList)
	{
		int curSel = m_pkNPCList->GetCurSel();
		stMapNPCInfo* pkData = (stMapNPCInfo*)m_pkNPCList->GetItemData(curSel);
		if (pkData)
		{
			m_pkBigMap->SetDrawNpcData(pkData);
		}
	}

}
void UBigMapFrame::OnClickShow()
{
	if (m_pkBigMap && m_Mapid)
	{
		m_pkBigMap->UpdataNpc(m_Mapid, m_pkNPCList, m_ShowNormalNpc->IsChecked(), m_ShowGuaiwu->IsChecked(), m_ShowTitleNpc->IsChecked());

		UScrollBar* pScrollBar = (UScrollBar*)GetChildByID(6);
		pScrollBar->ReCalLayout();
		pScrollBar->scrollRectVisible(URect(0,0,0,0));

		m_pkBigMap->SetDrawNpcData(NULL);
	}
}
void UBigMapFrame::OnClickSeach()
{
	char buf[_MAX_PATH];
	if (m_SeachText->GetText(buf,_MAX_PATH, 0))
	{
		if (m_pkBigMap && m_Mapid)
		{
			m_pkBigMap->UpdataNpc(m_Mapid, m_pkNPCList, m_ShowNormalNpc->IsChecked(), m_ShowGuaiwu->IsChecked(), m_ShowTitleNpc->IsChecked(),buf);

			UScrollBar* pScrollBar = (UScrollBar*)GetChildByID(6);
			pScrollBar->ReCalLayout();
			pScrollBar->scrollRectVisible(URect(0,0,0,0));

			m_pkBigMap->SetDrawNpcData(NULL);
		}
	}

	m_SeachText->Clear();
	SetFocusControl(sm_System->GetCurFrame());
}
void UBigMapFrame::OnClickFindPath()
{
	if (m_pkNPCList)
	{
		int curSel = m_pkNPCList->GetCurSel();
		stMapNPCInfo* pkData = (stMapNPCInfo*)m_pkNPCList->GetItemData(curSel);
		if (pkData)
		{
			int iMapId = 0;
			// 检查当前地图ID，如果相同则寻路
			
			CPlayerLocal* pkLcoalPlayer = ObjectMgr->GetLocalPlayer();
			pkLcoalPlayer->MoveToMapAStar(pkData->MapID ,pkData->NpcPos, 0);

			m_pkBigMap->SetDrawNpcData(pkData);
			
				
		}

	}
}
void UBigMapFrame::OnChangeBigMapAlpha()
{
	USlider* pSlider = UDynamicCast(USlider, GetChildByID(4));
	if (pSlider)
	{
		UStaticText* pstaticText = UDynamicCast(UStaticText, GetChildByID(5));
		float fAvalue = (float)pSlider->GetValue();
		SetAlpha(fAvalue);
		//char buf[256];
		//sprintf(buf, "透明度：%d%%", )
		pstaticText->SetText(_TRAN(N_NOTICE_F9, _I2A(int(fAvalue / 255.f * 100)).c_str()).c_str());
		UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(2));
		if (pBtn)
		{
			pBtn->SetAlpha(255 - fAvalue);
		}
		pBtn = UDynamicCast(UBitmapButton, GetChildByID(8));
		if (pBtn)
		{
			pBtn->SetAlpha(255 - fAvalue);
		}
		pBtn = UDynamicCast(UBitmapButton, GetChildByID(9));
		if (pBtn)
		{
			pBtn->SetAlpha(255 - fAvalue);
		}
		pBtn = UDynamicCast(UBitmapButton, GetChildByID(10));
		if (pBtn)
		{
			pBtn->SetAlpha(255 - fAvalue);
		}
	}
}

void UBigMapFrame::SetAlpha(float fAlpha)
{
	m_Style->mFillColor = UColor(255, 255, 255, BYTE(fAlpha*255));
}

float UBigMapFrame::GetAlpha() const
{
	return float(m_Style->mFillColor.a) / float(255.f);
}

void UBigMapFrame::SetVisible(BOOL value)
{
	m_Mapid = 0;
	if(value)
		GetParent()->MoveChildTo(this, NULL);
	UControl::SetVisible(value);
	sm_System->__PlaySound(UI_map);
	SetFocusControl(sm_System->GetCurFrame());
}
void UBigMapFrame::ShowBigMap()
{
	CMap* pkMap = CMap::Get();
	if(pkMap == NULL)
	{
		ULOG("not find cur map");		
		return;
	}
	ShowBigMap(pkMap->GetMapId());
	/*r
	if(m_pkBigMap)
	{
		m_pkBigMap->UpdateMap();
		m_pkBigMap->UpdateMapName(m_AreaMapName);
		m_pkBigMap->UpdataNpc(m_pkNPCList, m_ShowNormalNpc->IsChecked(), m_ShowGuaiwu->IsChecked(), m_ShowTitleNpc->IsChecked());
	}*/
}
void UBigMapFrame::ShowBigMap(ui32 mapid)
{
	SetVisible(true);
	if(m_pkBigMap)
	{
		m_Mapid = mapid ;
		m_pkBigMap->UpdateMap(mapid);
		m_pkBigMap->UpdateMapName(m_AreaMapName);
		m_pkBigMap->UpdataNpc(mapid, m_pkNPCList, m_ShowNormalNpc->IsChecked(), m_ShowGuaiwu->IsChecked(), m_ShowTitleNpc->IsChecked());

		
		UScrollBar* pScrollBar = (UScrollBar*)GetChildByID(6);
		pScrollBar->ReCalLayout();
		pScrollBar->scrollRectVisible(URect(0,0,0,0));
	}
}


UIMP_CLASS(UBigMap,UControl);
void UBigMap::StaticInit()
{
}

UBigMap::UBigMap()
	:m_kMapRegion(0, 0, 512, 512)
{
	m_ArrowTexture = NULL;
	m_MapTexture = NULL;
	m_DrawNpcData = NULL;
	m_BattleGroundFlag = NULL;
	m_bDrawWorldMap = false;
	m_WorldMapTexture = NULL;
	m_HightlightTexture = NULL;
}

UBigMap::~UBigMap()
{
}

BOOL UBigMap::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (m_ArrowTexture == NULL)
	{
		m_ArrowTexture = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\MinimapArrow.png");
	}

	if (!InitTextureList())
	{
		return FALSE;
	}

	if (!m_GlintTexture)
	{
		m_GlintTexture = sm_UiRender->LoadTexture("Data\\UI\\InGameBar\\GLINk.tga");
	}
	if (!m_GlintTexture)
	{
		return FALSE ;
	}

	if (m_BattleGroundFlag == NULL)
	{
		m_BattleGroundFlag = sm_System->LoadSkin("BigMap\\BattleFlag.skin");
		if (!m_BattleGroundFlag)
		{
			return FALSE;
		}
	}

	int index = 0;
	//mapid == 1 赤芒之地
	if (m_stWorldMapHLArea[index].mHighLightTexture == NULL)
	{
		m_stWorldMapHLArea[index].mapid = 1;
		m_stWorldMapHLArea[index].mHighLightTexture = sm_UiRender->LoadTexture("Data\\World\\MiniMaps\\worldmapht_1.png");
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(155, 308);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(138, 353);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(138, 434);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(245, 434);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(281, 363);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(281, 308);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(155, 308);
	}
	index++;
	//mapid == 2 羲和之地
	if (m_stWorldMapHLArea[index].mHighLightTexture == NULL)
	{
		m_stWorldMapHLArea[index].mapid = 2;
		m_stWorldMapHLArea[index].mHighLightTexture = sm_UiRender->LoadTexture("Data\\World\\MiniMaps\\worldmapht_2.png");
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(205, 159);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(156, 194);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(156, 305);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(281, 305);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(281, 253);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(224, 159);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(205, 159);
	}
	index++;
	//mapid == 3 盘灵之地
	if (m_stWorldMapHLArea[index].mHighLightTexture == NULL)
	{
		m_stWorldMapHLArea[index].mapid = 3;
		m_stWorldMapHLArea[index].mHighLightTexture = sm_UiRender->LoadTexture("Data\\World\\MiniMaps\\worldmapht_3.png");
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(295, 33);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(295, 145);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(344, 202);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(435, 202);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(435, 115);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(359, 33);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(295, 33);
	}
	index++;
	//mapid == 7 不周山麓
	if (m_stWorldMapHLArea[index].mHighLightTexture == NULL)
	{
		m_stWorldMapHLArea[index].mapid = 7;
		m_stWorldMapHLArea[index].mHighLightTexture = sm_UiRender->LoadTexture("Data\\World\\MiniMaps\\worldmapht_7.png");
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(283, 237);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(283, 357);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(397, 357);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(446, 312);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(446, 282);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(410, 237);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(283, 237);
	}
	index++;
	//mapid == 10 河图洛书
	if (m_stWorldMapHLArea[index].mHighLightTexture == NULL)
	{
		m_stWorldMapHLArea[index].mapid = 10;
		m_stWorldMapHLArea[index].mHighLightTexture = sm_UiRender->LoadTexture("Data\\World\\MiniMaps\\worldmapht_10.png");
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(400, 355);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(400, 492);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(462, 492);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(518, 461);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(518, 355);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(400, 355);
	}
	index++;
	//mapid == 20 毁灭之土
	if (m_stWorldMapHLArea[index].mHighLightTexture == NULL)
	{
		m_stWorldMapHLArea[index].mapid = 20;
		m_stWorldMapHLArea[index].mHighLightTexture = sm_UiRender->LoadTexture("Data\\World\\MiniMaps\\worldmapht_20.png");
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(282, 362);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(248, 434);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(264, 489);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(314, 522);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(362, 522);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(399, 504);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(399, 379);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(321, 362);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(282, 362);
	}
	index++;
	//mapid == 53 伊娃秘境
	if (m_stWorldMapHLArea[index].mHighLightTexture == NULL)
	{
		m_stWorldMapHLArea[index].mapid = 53;
		m_stWorldMapHLArea[index].mHighLightTexture = sm_UiRender->LoadTexture("Data\\World\\MiniMaps\\worldmapht_53.png");
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(266, 157);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(261, 161);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(261, 215);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(277, 235);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(330, 235);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(342, 227);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(342, 201);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(303, 157);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(266, 157);
	}
	index++;
	//mapid ==78 开心游乐场
	if (m_stWorldMapHLArea[index].mHighLightTexture == NULL)
	{
		m_stWorldMapHLArea[index].mapid = 78;
		m_stWorldMapHLArea[index].mHighLightTexture = sm_UiRender->LoadTexture("Data\\World\\MiniMaps\\worldmapht_78.png");
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(546, 110);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(546, 217);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(604, 217);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(604, 110);
		m_stWorldMapHLArea[index].approximateselection.InsertPoint(546, 110);
	}
	index++;
	return TRUE;
}

void UBigMap::OnDestroy()
{
	m_MapNpcList.clear();
	m_DrawNpcData = NULL;
	UControl::OnDestroy();
}
BOOL UBigMap::InitTextureList()
{
	m_TextureList[MaskTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\MiniMapMask.dds");
	m_TextureList[PlayerTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\Teammem.png");
	m_TextureList[ArrowTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\MiniMapArrow.png");
	m_TextureList[TeamArrow] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\TeamArrow.png");
	m_TextureList[FindOBJArrow] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\FindObj.png");
	m_TextureList[SeeOBJArrow] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\SeeFindObj.png");
	m_TextureList[DotTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\MiniMapDot.png");
	m_TextureList[CanGetQTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\CanQ.png");
	m_TextureList[PerformQTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\DoQ.png");
	m_TextureList[FinishQTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\FinishQ.png");
	m_TextureList[UnActiveQTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\UnAativeQ.png");
	m_TextureList[FriendlyTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\friendly.png");
	m_TextureList[NeutralTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\neutral.png");
	m_TextureList[EnemyTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\Enemy.png");
	m_TextureList[MineTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\mine.png");
	m_TextureList[HerbTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\herb.png");
	m_TextureList[NullTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NullMinimap.jpg");
	m_TextureList[OtherPlayerTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\otherplayer.png");



	for (int i = MaskTexture; i < MaxCountTexture ; i++)
	{
		if (m_TextureList[i] == NULL)
		{
			ULOG("load BigMap Texture Failed!");
			return FALSE ;
		}
	}

	m_WorldMapTexture = sm_UiRender->LoadTexture("Data\\World\\MiniMaps\\worldmap.jpg");
	if (!m_WorldMapTexture)
		return FALSE;

	return TRUE;
}
void UBigMap::OnRender(const UPoint& offset, const URect &updateRect)
{
	if (m_bDrawWorldMap)
	{
		DrawWorldMap(updateRect);
		RenderChildWindow(offset, updateRect);
	}else
	{
		DrawAreaMap(updateRect);
		RenderChildWindow(offset, updateRect);
		//if(m_bDrawPlayerPos)
		{
			DrawNpcPos(updateRect);
			DrawPathFind(updateRect);
			DrawBattleGroundFlag(updateRect);
			DrawPlayerPos(updateRect);
		}
	}
}

void UBigMap::OnMouseMove(const UPoint& position, UINT flags)
{
	if (m_bDrawWorldMap)
	{
		for (int i = 0 ; i < AREA_COUNT ; ++i)
		{
			if (m_stWorldMapHLArea[i].approximateselection.IsPointInPolygon(ScreenToWindow(position)))
			{
				m_HightlightTexture = m_stWorldMapHLArea[i].mHighLightTexture;
				return;
			}
		}
		m_HightlightTexture = NULL;
	}
	else
	{
		UPoint pos = position;
		for (UINT ui = 0 ; ui < m_vTeammemInfo.size() ; ui++)
		{
			if (m_vTeammemInfo[ui].FindRect.PointInRect(pos))
			{
				TipSystem->ShowTip(TIP_TYPE_PLAYER, 0, pos.Offset(-100, -90), m_vTeammemInfo[ui].guid);
				return;
			}
		}
	}
	TipSystem->HideTip();
}
void UBigMap::OnMouseLeave(const UPoint& position, UINT flags)
{
	TipSystem->HideTip();
}

void UBigMap::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{

}

void UBigMap::OnMouseUp(const UPoint& position, UINT flags)
{
	if (m_bDrawWorldMap)
	{
		for (int i = 0 ; i < AREA_COUNT ; ++i)
		{
			if (m_stWorldMapHLArea[i].approximateselection.IsPointInPolygon(ScreenToWindow(position)))
			{
				UpdateMap(m_stWorldMapHLArea[i].mapid);
				return;
			}
		}
	}else
	{
		CMap* pkMap = CMap::Get();
		if (pkMap && m_uiMapID == pkMap->GetMapId())
		{
			NiPoint3 mapPos ;
			if (GetMapPosFromScreePos(mapPos, position))
			{
				mapPos.z = pkMap->GetHeight(mapPos.x, mapPos.y);
// 				static stMapNPCInfo test;
// 				test.MapID = pkMap->GetMapId();
// 				test.NpcPos = mapPos;
// 				test.name = L"test";
// 				SetDrawNpcData(&test);
				CPlayerLocal* pkLcoalPlayer = ObjectMgr->GetLocalPlayer();
				pkLcoalPlayer->MoveToMapAStar(m_uiMapID , mapPos, 0);
			}
		}
		
	}
}

void UBigMap::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{

}

void UBigMap::OnRightMouseUp(const UPoint& position, UINT flags)
{
	m_bDrawWorldMap = true;
}

void UBigMap::OnTimer(float fDeltaTime)
{
	if (mBGlint)
	{
		mGlintTime += fDeltaTime ;
		if (mGlintTime >= 1.0f)
		{
			mGlintTime = 0.f;
			mBGlint = FALSE ;
		}
	}
}

void UBigMap::SetDrawNpcData(stMapNPCInfo* pkData)
{
	m_DrawNpcData = pkData ;
}
void UBigMap::UpdataNpc(UListBox* pkNPCList , BOOL NormalNpc, BOOL Guiwu , BOOL TitleNpc,char* Seachstr)
{
	CMap* pkMap = CMap::Get();
	if(pkMap == NULL)
		return;
	UpdataNpc(pkMap->GetMapId() , pkNPCList, NormalNpc, Guiwu, TitleNpc, Seachstr);
}

void UBigMap::UpdataNpc(ui32 mapid, UListBox* pkNPCList, BOOL NormalNpc, BOOL Guiwu , BOOL TitleNpc, char* Seachstr)
{
	pkNPCList->Clear();
	if (!NormalNpc && !Guiwu && !TitleNpc){
		return ;
	}
	std::vector<CCreatureSpawnsFromClientDB::stCreateSpawnsFromClientInfo> vList;
	if(g_pkCCreatureSpawnsInfo->GetCreateSpawnsList(mapid, vList))
	{
		for (unsigned int ui =0; ui < vList.size(); ui++)
		{
			std::string str;
			BOOL bFind = FALSE ;
			if (NormalNpc && !bFind)
			{
				bFind = ItemMgr->GetNormalNpcName(vList[ui].Entry, str);
			}
			if (Guiwu && !bFind)
			{
				bFind = ItemMgr->GetGuaiWuName(vList[ui].Entry, str );
			}
			if (TitleNpc && !bFind)
			{
				bFind = ItemMgr->GetTitleNpcName(vList[ui].Entry, str);
			}

			if(bFind)
			{
				if (Seachstr &&  strlen(Seachstr))
				{
					 if (str.find(Seachstr) == string::npos)
					 {
						 continue ;
					 }
				}
				stMapNPCInfo* pNpcInfo = new stMapNPCInfo;
				UStringW TempStr = str.c_str();
				pNpcInfo->name = TempStr.GetBuffer();
				pNpcInfo->MapID = vList[ui].MapId;
				pNpcInfo->NpcPos = vList[ui].kPos;
				pkNPCList->AddItem(pNpcInfo->name.c_str(), pNpcInfo);
			}
		}
	}
}

void UBigMap::UpdateMap()
{
	m_bDrawWorldMap = false;
	CMap* pkMap = CMap::Get();
	if(pkMap == NULL)
		return;
	m_bDrawPlayerPos = true;
	char acBuffer[512];
	NiSprintf(acBuffer, sizeof(acBuffer), "Data\\World\\MiniMaps\\%s.jpg", (const char*)pkMap->GetMapName());
	m_MapTexture = sm_UiRender->LoadTexture(acBuffer);

	CMapInfoDB::MapInfoEntry kMapInfo;
	if(g_pkMapInfoDB->GetMapInfo(pkMap->GetMapId(), kMapInfo))
	{
		sscanf_s(kMapInfo.region.c_str(), "%d %d %d %d", &m_kMapRegion.pt0.x, &m_kMapRegion.pt0.y, &m_kMapRegion.pt1.x, &m_kMapRegion.pt1.y);
	}

	if(m_kMapRegion.pt1.x <= m_kMapRegion.pt0.x)
		m_kMapRegion.pt1.x = m_kMapRegion.pt0.x + 512;

	if(m_kMapRegion.pt1.y <= m_kMapRegion.pt0.y)
		m_kMapRegion.pt1.y = m_kMapRegion.pt0.y + 512;
	mBGlint = TRUE;
	mGlintTime = 0.f;
}
void UBigMap::UpdateMap(ui32 mapid)
{
	m_bDrawWorldMap = false;
	CMap* pkMap = CMap::Get();
	if(pkMap == NULL)
		return;
	m_uiMapID = mapid;
	m_bDrawPlayerPos = (m_uiMapID == pkMap->GetMapId());
	CMapInfoDB::MapInfoEntry kMapInfo;
	if(g_pkMapInfoDB->GetMapInfo(mapid, kMapInfo))
	{
		char acBuffer[512];
		NiSprintf(acBuffer, sizeof(acBuffer), "Data\\World\\MiniMaps\\%s.jpg", kMapInfo.name.c_str());
		m_MapTexture = sm_UiRender->LoadTexture(acBuffer);
		if(!m_MapTexture)
		{
			if(UInGame::Get() && UInGame::Get()->GetFrame(FRAME_IG_MINIMAP))
			{
				UButton* pBtn = UDynamicCast(UButton, UInGame::Get()->GetFrame(FRAME_IG_MINIMAP)->GetChildByID(4));
				if(pBtn)
				{
					UISystemMsg* pSGN = INGAMEGETFRAME(UISystemMsg, FRAME_IG_SYSGIFTNOTIFY);
					if (pSGN)
					{
						pSGN->GetSystemMsg(_TRAN("对不起，该地图需要自己探索"));
					}
					pBtn->SetCheck(TRUE);
				}
			}
		}

		sscanf_s(kMapInfo.region.c_str(), "%d %d %d %d", &m_kMapRegion.pt0.x, &m_kMapRegion.pt0.y, &m_kMapRegion.pt1.x, &m_kMapRegion.pt1.y);
	}else
	{
		ULOG("has not find map");
	}

	if(m_kMapRegion.pt1.x <= m_kMapRegion.pt0.x)
		m_kMapRegion.pt1.x = m_kMapRegion.pt0.x + 512;

	if(m_kMapRegion.pt1.y <= m_kMapRegion.pt0.y)
		m_kMapRegion.pt1.y = m_kMapRegion.pt0.y + 512;
	mBGlint = TRUE;
	mGlintTime = 0.f;
}
void UBigMap::UpdateMapName(UStaticText* StaticText)
{
	//CMap* pkMap = CMap::Get();
	//if(pkMap == NULL)
	//	return;
	CMapInfoDB::MapInfoEntry entry;
	if (g_pkMapInfoDB)
	{
		g_pkMapInfoDB->GetMapInfo(m_uiMapID, entry);
	}
	StaticText->SetText(entry.desc.c_str());
	StaticText->SetTextColor(UColor(255,255,255,255));
}

bool UBigMap::GetRenderScreenRect(float x, float y, const URect& updataRect, int drawSize, URect& rect)
{
	float fX = (x - m_kMapRegion.pt0.x)/(m_kMapRegion.pt1.x - m_kMapRegion.pt0.x);
	float fY = (y - m_kMapRegion.pt0.y)/(m_kMapRegion.pt1.y - m_kMapRegion.pt0.y);

	rect.pt0.x = updataRect.pt0.x + int(fX*updataRect.GetWidth()) - drawSize / 2;
	rect.pt0.y = updataRect.pt0.y + int((1.f - fY)*updataRect.GetHeight()) - drawSize /2;
	rect.pt1.x = rect.pt0.x + drawSize;
	rect.pt1.y = rect.pt0.y + drawSize;
	return true;
}
bool UBigMap::GetMapPosFromScreePos(NiPoint3& mapPos,  const UPoint uiPos)
{
	UPoint screenPos = WindowToScreen(UPoint(0, 0));
	
	const URect updataRect = URect(screenPos, GetWindowSize());
	float fx = (float)m_kMapRegion.GetWidth() / (float)updataRect.GetWidth();
	float fy = (float)m_kMapRegion.GetHeight() / (float)updataRect.GetHeight();
	
	mapPos.x = m_kMapRegion.left + (uiPos.x - updataRect.left)  * fx ;
	mapPos.y = m_kMapRegion.top + (updataRect.bottom - uiPos.y) * fy ;

	return true;
}
void UBigMap::DrawBattleGroundFlag(const URect& updateRect)
{
	URect ScreenRect;
	if (m_uiMapID == 69)
	{
		static UPoint flagpos[] =
		{
			UPoint(916, 1101),
			UPoint(757, 829),
			UPoint(690, 1370),
			UPoint(1441, 624),
			UPoint(1175, 1263),
		};
		for(UINT i = 0; i < 5; i++)
		{
			GetRenderScreenRect(flagpos[i].x, flagpos[i].y, updateRect, 64, ScreenRect);
			int skinindex = InstanceSys->GetBattleGroundFlagState(i);
			sm_UiRender->DrawSkin(m_BattleGroundFlag, skinindex, ScreenRect);
		}
	}
}
void UBigMap::DrawPlayerPos(const URect &updateRect)
{
	URect ScreenRect;

	//std::vector<NiPoint3> TemmemPosVec;
	m_vMapPlayer.clear();
	m_vTeammemInfo.clear();
	if (SocialitySys && SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->GetTeamMemByMapid(m_uiMapID, m_vMapPlayer);
		//SocialitySys->GetTeamSysPtr()->GetTeamMemPosition(m_uiMapID, TemmemPosVec);
	}
	//for(UINT i = 0; i < TemmemPosVec.size(); i++)
	//{
	//	GetRenderScreenRect(TemmemPosVec[i].x, TemmemPosVec[i].y, updateRect, 32, ScreenRect);
	//	sm_UiRender->DrawImage(m_TextureList[PlayerTexture], ScreenRect, TEAM_COLOR);
	//}
	for(UINT i = 0; i < m_vMapPlayer.size(); i++)
	{
		TeamDate* pDate = m_vMapPlayer[i];
		if(pDate->islogin)
		{
			GetRenderScreenRect(pDate->pos.x, pDate->pos.y, updateRect, 16, ScreenRect);
			static stTeammemInfo temp;
			temp.FindRect = ScreenRect;
			temp.guid = pDate->Guid;
			m_vTeammemInfo.push_back(temp);
			sm_UiRender->DrawImage(m_TextureList[PlayerTexture], ScreenRect, GetClassColor(pDate->Class));
			static char* teamNum[] = {"1", "2", "3", "4", "5", "6"};
			UDrawText(sm_UiRender, m_Style->m_spFont, ScreenRect.GetPosition() + UPoint(1, 1), ScreenRect.GetSize(), LCOLOR_BLACK, teamNum[pDate->Teamid], UT_CENTER);
			UDrawText(sm_UiRender, m_Style->m_spFont, ScreenRect.GetPosition(), ScreenRect.GetSize(), LCOLOR_WHITE, teamNum[pDate->Teamid], UT_CENTER);
		}
	}

	if(m_bDrawPlayerPos)
	{
		CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
		if( !pkPlayer )
			return;

		GetRenderScreenRect(pkPlayer->GetPosition().x, pkPlayer->GetPosition().y, updateRect, 32 + mBGlint * (1 - mGlintTime) * 100, ScreenRect);
		URect ImageRect;
		ImageRect.pt0.x = 0;
		ImageRect.pt0.y = 0;
		ImageRect.pt1.x = m_ArrowTexture->GetWidth();
		ImageRect.pt1.y = m_ArrowTexture->GetHeight();
		sm_UiRender->DrawRotatedImage(pkPlayer->GetRotate().z + NI_PI, m_ArrowTexture, ScreenRect, ImageRect);

		if (mBGlint)
		{
			float fprogress = mGlintTime;
			UColor Color(255,0,0);
			if (mGlintTime <= 0.5f)
			{
				Color.a = (BYTE)(100 + 155 * fprogress * 2.f);
			}else
			{
				fprogress = (1.0f - mGlintTime);
				Color.a = (BYTE)(100 + 155 * fprogress * 2.f );
			}
			GetRenderScreenRect(pkPlayer->GetPosition().x, pkPlayer->GetPosition().y, updateRect, 64 + 40 * fprogress, ScreenRect);

			//sm_UiRender->DrawImage(m_GlintTexture,ScreenRect,Color);
		}
	}

}

void UBigMap::DrawNpcPos(const URect& updateRect)
{
	CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
	if( !pkPlayer )
		return;

	if (m_DrawNpcData && m_DrawNpcData->MapID == m_uiMapID)
	{
		URect kRect;
		GetRenderScreenRect(m_DrawNpcData->NpcPos.x, m_DrawNpcData->NpcPos.y, updateRect, 32, kRect);
		UPoint MapPos(kRect.GetPosition());
		MapPos.x += 30;
		MapPos.y -= 30;
		UPoint textBounds;
		if (mPoPo[0]){
			textBounds.x = m_Style->m_spFont->GetStrWidth(m_DrawNpcData->name.c_str());
			textBounds.y = m_Style->m_spFont->GetHeight() + 4;
			UDrawResizeBitmap(sm_UiRender, mPoPo[0], MapPos, textBounds);
		}
		MapPos.x -= 4;
		UDrawText(sm_UiRender, m_Style->m_spFont, MapPos, textBounds, mTooltipProfile->m_FontColor, m_DrawNpcData->name.c_str());
		sm_UiRender->DrawImage(m_TextureList[DotTexture], kRect);
	}

	std::vector<NiPoint3>posVec;

	posVec.clear();

	if (pkPlayer->GetCanFinishQuestData(m_uiMapID, posVec))
	{
		for (size_t i = 0; i< posVec.size(); i++)
		{
			URect kRect;
			GetRenderScreenRect(posVec[i].x, posVec[i].y, updateRect, 16, kRect);
			sm_UiRender->DrawImage(m_TextureList[FinishQTexture], kRect);
		}
	}
	if(m_bDrawPlayerPos)
	{
		const stdext::hash_set<UpdateObj*>& temp( ObjectMgr->GetObjectByType(TYPEID_UNIT) );

		for( stdext::hash_set<UpdateObj*>::const_iterator it = temp.begin(); it != temp.end(); ++it )
		{
			UpdateObj* p = *it;
			if( p == pkPlayer)
				continue;

			CCreature* pkCreature = (CCreature*)p;
			if (!pkCreature && pkCreature->GetGameObjectType() != GOT_CREATURE)
			{
				continue ;
			}
			NiPoint3 kPos = pkCreature->GetPosition();

			URect kRect;
			GetRenderScreenRect(kPos.x, kPos.y, updateRect, 16, kRect);

			if (!pkPlayer->IsHostile(pkCreature))
			{
				CCreature::eQuestFlag pQuestFlag = pkCreature->GetQuestFlag();
				if (pQuestFlag == CCreature::eStart)
				{
					sm_UiRender->DrawImage(m_TextureList[CanGetQTexture], kRect);
				}
			}
		}
	}
}

void UBigMap::DrawPathFind(const URect& updateRect)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal && m_bDrawPlayerPos)
	{
		static std::deque<NiPoint3> Temp;
		Temp.clear();
		if (pkLocal->GetPathPoint(Temp))
		{
			size_t tSize = Temp.size();

			if (!tSize)
			{
				return ;
			}

			//渲染玩家到第一个寻路点
			URect LocalRec;
			GetRenderScreenRect(pkLocal->GetPosition().x, pkLocal->GetPosition().y, updateRect, 2, LocalRec);

			URect RecO;
			GetRenderScreenRect(Temp[0].x, Temp[0].y, updateRect, 2, RecO);
			if (LocalRec.GetPosition() != RecO.GetPosition())
			{
				sm_UiRender->DrawLine(LocalRec.GetPosition(), RecO.GetPosition(), 0xc800ff00);
			}
			
			//渲染第一个寻路点到目标寻路点
			for (size_t i =0; i < tSize - 1 ; i++)
			{
				URect kRect;
				GetRenderScreenRect(Temp[i].x, Temp[i].y, updateRect, 16, kRect);
				//sm_UiRender->DrawImage(m_TextureList[DotTexture], kRect,0xff00ff00);	
				URect kRect1;
				GetRenderScreenRect(Temp[i+1].x, Temp[i+1].y, updateRect, 2, kRect1);
				sm_UiRender->DrawLine(kRect1.GetPosition(), kRect.GetPosition() + UPoint(8,8), 0xc800ff00);
			}
			
			////渲染目标点标示
			URect mubiaoRec ;
			GetRenderScreenRect(Temp[tSize - 1].x, Temp[tSize - 1].y, updateRect, 16, mubiaoRec);
			sm_UiRender->DrawImage(m_TextureList[SeeOBJArrow], mubiaoRec,0xc800ff00);//0xff00ff00

		}
	}
}

void UBigMap::DrawAreaMap(const URect& updateRect)
{
	if(!m_MapTexture)
		return;

	URect ImageRect;
	ImageRect.pt0.x = 0;
	ImageRect.pt0.y = 0;
	ImageRect.pt1.x = m_MapTexture->GetWidth();
	ImageRect.pt1.y = m_MapTexture->GetHeight();
	sm_UiRender->DrawImage(m_MapTexture, updateRect, ImageRect, m_Style->mFillColor);
}

void UBigMap::DrawWorldMap(const URect& updateRect)
{
	if(!m_WorldMapTexture)
		return;
	URect ImageRect;
	ImageRect.pt0.x = 0;
	ImageRect.pt0.y = 0;
	ImageRect.pt1.x = m_WorldMapTexture->GetWidth();
	ImageRect.pt1.y = m_WorldMapTexture->GetHeight();
	sm_UiRender->DrawImage(m_WorldMapTexture, updateRect, ImageRect, m_Style->mFillColor);

	if (!m_HightlightTexture)
		return;
	ImageRect;
	ImageRect.pt0.x = 0;
	ImageRect.pt0.y = 0;
	ImageRect.pt1.x = m_HightlightTexture->GetWidth();
	ImageRect.pt1.y = m_HightlightTexture->GetHeight();
	sm_UiRender->DrawImage(m_HightlightTexture, updateRect, ImageRect, m_Style->mFillColor);
}
