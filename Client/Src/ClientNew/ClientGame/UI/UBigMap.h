#ifndef UBIGMAP_H
#define UBIGMAP_H

#include"UInc.h"
#include "UIMiniMap.h"
#include "Utils//NPCListFile.h"

class UBlendBitmap : public UBitmap
{
public:
	UDEC_CLASS(UBlendBitmap);
	UDEC_MESSAGEMAP();

	virtual void OnRender(const UPoint& offset, const URect &updateRect);
};

//只是用来处理消息
class UBigMap;
class UBigMapFrame : public UControl
{
public:
	void ShowBigMap();
	void ShowBigMap(ui32 mapid);
	virtual void SetVisible(BOOL value);
public:
	UDEC_CLASS(UBigMapFrame);
	UDEC_MESSAGEMAP();

	UBigMapFrame();

	virtual BOOL OnCreate();
	virtual BOOL OnEscape();

	void SetAlpha(float fAlpha);
	float GetAlpha() const;

	void OnClickClose();
	void OnChangeBigMapAlpha();
	void OnClickFindPath();
	void OnClickSelChange();

	void OnClickShow();
	void OnClickSeach();

private:
	UBigMap* m_pkBigMap;
	UListBox* m_pkNPCList;
	UStaticText* m_AreaMapName;
	UBitmapButton* m_ShowNormalNpc;
	UBitmapButton* m_ShowTitleNpc;
	UBitmapButton* m_ShowGuaiwu;
	UEdit* m_SeachText;
	UBitmapButton* m_SeachBtn;
	
	ui32  m_Mapid ;
};

class UBigMap : public UControl
{
	UDEC_CLASS(UBigMap)
public:
	UBigMap();
	~UBigMap();
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnTimer(float fDeltaTime);

	void SetDrawNpcData(stMapNPCInfo* pkData);
	void UpdateMap();
	void UpdateMap(ui32 mapid);
	void UpdataNpc(UListBox* pkNPCList, BOOL NormalNpc, BOOL Guiwu , BOOL TitleNpc, char* Seachstr = NULL);
	void UpdataNpc(ui32 mapid, UListBox* pkNPCList,BOOL NormalNpc, BOOL Guiwu , BOOL TitleNpc, char* Seachstr = NULL);
	void UpdateMapName(UStaticText* StaticText);

	bool GetRenderScreenRect(float x, float y, const URect& updataRect, int drawSize, URect& rect);
	bool GetMapPosFromScreePos(NiPoint3& mapPos, const UPoint uiPos);

private:
	BOOL InitTextureList();
	void DrawPlayerPos(const URect& updateRect);
	void DrawNpcPos(const URect& updateRect);
	void DrawAreaMap(const URect& updateRect);
	void DrawWorldMap(const URect& updateRect);
	void DrawPathFind(const URect& updateRect);
	void DrawBattleGroundFlag(const URect& updateRect);

	UTexturePtr m_ArrowTexture;
	UTexturePtr m_MapTexture;

	UTexturePtr m_WorldMapTexture;
	bool m_bDrawWorldMap;
	enum
	{
		AREA_COUNT = 8,
	};
	struct stWorldMapHLArea
	{
		stWorldMapHLArea():mapid(0),mHighLightTexture(NULL){}
		ui32 mapid;
		UPolygon approximateselection;
		UTexturePtr mHighLightTexture;
	}m_stWorldMapHLArea[AREA_COUNT];

	UTexturePtr m_HightlightTexture;
	
	
	UTexturePtr m_TextureList[MaxCountTexture];

	std::vector<stMapNPCInfo*> m_MapNpcList;
	struct stTeammemInfo 
	{
		ui64 guid;
		URect FindRect;
	};
	std::vector<stTeammemInfo> m_vTeammemInfo;
	std::vector<struct TeamDate*> m_vMapPlayer;
	stMapNPCInfo* m_DrawNpcData;

	URect m_kMapRegion;
	uint32 m_uiMapID;
	bool m_bDrawPlayerPos;
	UTexturePtr m_GlintTexture ;
	USkinPtr m_BattleGroundFlag;
};
#endif