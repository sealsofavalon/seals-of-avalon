#include "StdAfx.h"
#include "UBitmapButtonEx.h"
#include "ItemManager.h"
#include "UITipSystem.h"
#include "ClientApp.h"

UIMP_CLASS(UBitmapButtonEx, UBitmapButton);

void UBitmapButtonEx::StaticInit()
{

}

UBitmapButtonEx::UBitmapButtonEx()
{
	m_spInfoSkin = NULL;
	m_uiInfoIndex = 0;

	m_bSelected = false;

	m_kTextColor = 0xffffffff;

	m_uiItemId = 0;

}

UBitmapButtonEx::~UBitmapButtonEx()
{
}

BOOL UBitmapButtonEx::OnCreate()
{
	if(!UBitmapButton::OnCreate())
		return FALSE;

	return TRUE;
}

void UBitmapButtonEx::OnDestroy()
{
	UBitmapButton::OnDestroy();
}

//void UBitmapButtonEx::OnRender(const UPoint& offset, const URect& updateRect)
//{
//	// Draw bkg
//	URect rect(offset, m_Size);
//	sm_UiRender->DrawSkin(m_spBtnSkin, 0, rect);
//
//	// Draw Info
//	if(m_spInfoSkin)
//	{
//		UPoint kOffset(offset.x + 5, offset.y + 3);
//		UPoint kSize(40, 40);
//		URect kRect(kOffset, kSize);
//		sm_UiRender->DrawSkin(m_spInfoSkin, 0, kRect);
//	}
//}

void UBitmapButtonEx::OnMouseEnter(const UPoint& pt, UINT uFlags)
{
	UButton::OnMouseEnter(pt,uFlags);

	OnShowPerproty(m_bMouseOver);
}

void UBitmapButtonEx::OnMouseLeave(const UPoint& pt, UINT uFlags)
{
	UButton::OnMouseLeave(pt,uFlags);
	OnHidePerproty();
}

void UBitmapButtonEx::OnSize(const UPoint& NewSize)
{
	

	UControl::OnSize(NewSize);
}

void UBitmapButtonEx::OnShowPerproty(BOOL isShow)
{
	UPoint t_pt = GetParent()->WindowToScreen(GetWindowPos());
	if(isShow && m_uiItemId)
	{
		int _width = SYState()->ClientApp->GetAppWindow()->GetWidth();
		int _hight = SYState()->ClientApp->GetAppWindow()->GetHeight();
		t_pt.x += GetWidth();
		t_pt.y += GetHeight();
		TipSystem->ShowTip(TIP_TYPE_ITEM, m_uiItemId, t_pt, 0, 1);
	}
}

void UBitmapButtonEx::OnHidePerproty()
{
	TipSystem->HideTip();
}

void UBitmapButtonEx::DrawPushButton(const UPoint& offset, const URect& ClipRect)
{
	URect rect(offset, m_Size);
	
	if(m_spInfoSkin)
	{
		sm_UiRender->DrawSkin(m_spInfoSkin, m_uiInfoIndex, rect);
	}
	if(m_Active)
	{
		sm_UiRender->DrawRect(rect, m_kTextColor);

		if(m_bSelected)
		{
			sm_UiRender->DrawRect(rect, 0xffffffff);
		}
	}
}

void UBitmapButtonEx::SetInfo(unsigned int uiID)
{
	if(uiID == 0)
		return;

	m_uiItemId = uiID;

	USkinPtr pIncoSkin = NULL;
	std::string str ;
	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(uiID);
	if(ItemInfo)
	{
		str = ItemInfo->C_icon;	
	}else
	{
		UTRACE("get ItemInfo failed!");
	}

	if(str.length() == 0)
	{
		UTRACE("load skin  failed!");
	}

	int pos = str.find(";");
	std::string skinstr = str.substr(0,pos);
	std::string strIndex = str.substr(pos+1,str.length() -1);

	m_spInfoSkin = sm_System->LoadSkin(skinstr.c_str());
	m_uiInfoIndex = atoi(strIndex.c_str());
}

//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UDownButton, UBitmapButton);
void UDownButton::StaticInit()
{

}

UDownButton::UDownButton()
{
}
UDownButton::~UDownButton()
{

}

BOOL UDownButton::OnCreate()
{
	if (!UBitmapButton::OnCreate())
	{
		return FALSE;
	}
	return TRUE;
}
void UDownButton::OnDestroy()
{
	UBitmapButton::OnDestroy();
}
void UDownButton::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	UBitmapButton::OnMouseDown(pt,nClickCnt,uFlags);
}
void UDownButton::OnMouseUp(const UPoint& pt, UINT uFlags)
{
	UBitmapButton::OnMouseUp(pt,uFlags);
}
