#pragma once

#include "UButton.h"

class UBitmapButtonEx : public UBitmapButton
{
	UDEC_CLASS(UBitmapButtonEx);

public:
	UBitmapButtonEx();
	virtual ~UBitmapButtonEx();

	void SetInfo(unsigned int uiID);

	void SetSelect(bool b) { m_bSelected = b; }

	void SetTextColor(UColor c) { m_kTextColor = c; }
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	//virtual void OnRender(const UPoint& offset, const URect& updateRect); 
	virtual void DrawPushButton(const UPoint& offset, const URect& ClipRect);
	virtual void OnMouseEnter(const UPoint& pt, UINT uFlags);
	virtual void OnMouseLeave(const UPoint& pt, UINT uFlags);
	virtual void OnSize(const UPoint& NewSize);

	void OnShowPerproty(BOOL isShow);
	void OnHidePerproty();

	USkinPtr m_spInfoSkin;
	unsigned int m_uiInfoIndex;

	bool m_bSelected;

	UColor m_kTextColor;
	unsigned int m_uiItemId;
};


class UDownButton : public UBitmapButton
{
	UDEC_CLASS(UDownButton);
public:
	UDownButton();
	~UDownButton();
	float GetTimer() {return m_timer;}
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
};