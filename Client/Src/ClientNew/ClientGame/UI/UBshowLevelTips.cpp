#include "stdafx.h"
#include "UBshowLevelTips.h"

UIMP_CLASS(UBShowLevelTips,UButton);
void UBShowLevelTips::StaticInit()
{

}
UBShowLevelTips::UBShowLevelTips()
{
	m_CheckEff = NULL;
	//m_UnCheckEff = NULL ;
	m_eBtnType = BTT_CHECK;

}
UBShowLevelTips::~UBShowLevelTips()
{
	m_CheckEff = NULL;
	//m_UnCheckEff = NULL;
}
BOOL UBShowLevelTips::OnCreate()
{
	if (!UButton::OnCreate())
	{
		return FALSE;
	}

	if (m_CheckEff == NULL)
	{
		m_CheckEff = UDynamicCast(UNiAVControlEff, GetChildByID(0));
	}
	//if (m_UnCheckEff == NULL)
	//{
	//	m_UnCheckEff = UDynamicCast(UNiAVControlEff, GetChildByID(1));
	//}

	if (m_CheckEff == NULL )//|| m_UnCheckEff == NULL)
	{
		return FALSE;
	}

	//m_CheckEff->SetVisible(!m_bCheck);
	//m_UnCheckEff->SetVisible(!m_bCheck);

	return TRUE;
}
void UBShowLevelTips::OnDestroy()
{
	UButton::OnDestroy();
}

void UBShowLevelTips::OnRender(const UPoint& offset, const URect &updateRect)
{
	RenderChildWindow(offset,updateRect);
}
void UBShowLevelTips::PreCommand()
{
	if(!m_Active)
		return;
	mBGlint = FALSE;
	if(m_eBtnType == BTT_CHECK)
	{
		m_bCheck = !m_bCheck;
		//m_CheckEff->SetVisible(m_bCheck);
		//m_UnCheckEff->SetVisible(!m_bCheck);
		if (m_CheckEff)
		{
			if (m_bCheck)
			{
				m_CheckEff->SetObjectFile("Data\\UI\\InGameBar\\Check.nif");
			}else
			{
				m_CheckEff->SetObjectFile("Data\\UI\\InGameBar\\UNCheck.nif");
			}
		}
	}
	DispatchNotify(UBN_CLICKED);
}