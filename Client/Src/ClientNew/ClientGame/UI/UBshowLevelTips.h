#ifndef __UBSHOWLEVELTIPS_H__
#define __UBSHOWLEVELTIPS_H__
#include "UNiAVControl.h"

class UBShowLevelTips : public UButton 
{
	UDEC_CLASS(UBShowLevelTips);
public:
	UBShowLevelTips();
	~UBShowLevelTips();
protected:
	BOOL virtual OnCreate();
	virtual void OnDestroy();
	void virtual OnRender(const UPoint& offset, const URect &updateRect);
	virtual void PreCommand();
private:
	UNiAVControlEff* m_CheckEff;
	//UNiAVControlEff* m_UnCheckEff;
};

#endif