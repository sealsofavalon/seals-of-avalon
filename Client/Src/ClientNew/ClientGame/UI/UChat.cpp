#include "StdAfx.h"
#include "UChat.h"
#include "UChatButtonGroup.h"
#include "UChatChannelBox.h"
#include "UChatInputMsgBox.h"
#include "UChatMsgShow.h"
#include "UChatProps.h"
#include "UMessageBox.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "UIRightMouseList.h"
#include "../Network/PacketBuilder.h"
#include "Utils/SpellDB.h"
#include "Skill/SkillManager.h"
#include "UFun.h"
#include "UITipSystem.h"
#include "SocialitySystem.h"
#include "SystemSetup.h"
#include "Utils/QuestDB.h"
#include "ObjectManager.h"
#include "LocalPlayer.h"
#include "SYItem.h"
#include "ItemManager.h"
#include "ItemSlotContainer.h"
#include "ItemSlot.h"
#include "Map/Map.h"
#include "Utils/MapInfoDB.h"
#include "Utils/AreaDB.h"
#include "UFittingRoom.h"
#include "SystemTips .h"
#include "../Network/NetworkManager.h"

RegistChatCallBack(ROLL, OnCommandRoll);
RegistChatCallBack(GDISBAND, OnCommandGuildDisband);
RegistChatCallBack(HP, OnGMModifyHp);
RegistChatCallBack(MP, OnGMModifyMP);
RegistChatCallBack(DAMAGE, OnGMModifyDamage);
RegistChatCallBack(SPEED, OnGMModifySpeed);
RegistChatCallBack(RELOADSPELL, OnReLoadSpell);
RegistChatCallBack(GINVITE, OnCommandGuildInvate);
RegistChatCallBack(HELP, OnChatInpHelp);
RegistChatCallBack(QUERYLADDER, OnQueryLADDER);
RegistChatCallBack(ADULT, OnAdult);
RegistChatCallBack(PLUG ,OnCommandCallPlug);
RegistChatCallBack(POSITION ,OnShowPosition);
RegistChatCallBack(FLY ,OnForceFly);
RegistChatCallBack(IWANTSHOPPING, OnShowShop);
RegistChatCallBack(AFK, OnAFK);
RegistChatCallBack(DND, OnDND);


ChatCallBackFunc* g_pChatFuncHead = NULL;
ChatCallBackFunc::ChatCallBackFunc(const char* pName, ChatCallBack pCB)
{
	this->pCB = pCB;
	this->pszName = pName;
	this->pNext = g_pChatFuncHead;
	g_pChatFuncHead = this;
}
UChat::UChat()
{
	for (int i = 0 ; i < CHATBOX_MAX ; i ++)
	{
		m_ChatBox[i] = NULL;
	}
	m_CurrentChannel = CHAT_MSG_SAY;
	for (int i = 0 ; i < CHAT_MSG_MAX ; i++)
	{
		m_ChannelData[i].IsShildChannel = FALSE;
		m_ChannelData[i].MsgType = -1;
	}
	m_Hide = FALSE;
}
UChat::~UChat()
{

}
void UChat::ShowMsgBox()
{
	UChatMsgShow * cms = UDynamicCast(UChatMsgShow,GetChatBox(CHATBOX_MSGSHOW));
	UChatTableControl * ctb = UDynamicCast(UChatTableControl, GetChatBox(CHATBOX_CHATTABLE));
	if (cms && ctb)
	{
		cms->SetBKGAlpha(255);
		ctb->SetBKGAlpha(1.0f);
	}
}

void UChat::HideMsgBox()
{
	UChatMsgShow * cms = UDynamicCast(UChatMsgShow,GetChatBox(CHATBOX_MSGSHOW));
	UChatTableControl * ctb = UDynamicCast(UChatTableControl, GetChatBox(CHATBOX_CHATTABLE));
	if (cms && ctb)
	{
		cms->SetBKGAlpha(100);
		ctb->SetBKGAlpha(50.0f / 255.0f);
	}
}

BOOL UChat::CreateChat(UControl * Frame)
{
	//创建界面
	if (Frame)
	{
		//输入框创建
		if (m_ChatBox[CHATBOX_INPUTMSG] == NULL)
		{
			m_ChatBox[CHATBOX_INPUTMSG] = UControl::sm_System->CreateDialogFromFile("Chat\\InputMsgBox.udg");
			if (m_ChatBox[CHATBOX_INPUTMSG] == NULL)
			{
				UMessageBox::MsgBox("CHATBOX_INPUTMSG");
				return FALSE;
			}
			else
			{
				Frame->AddChild(m_ChatBox[CHATBOX_INPUTMSG]);
				m_ChatBox[CHATBOX_INPUTMSG]->SetId(FRAME_IG_CHATINPUTMSGBOX);
			}
		}


		//按钮组创建
		if (m_ChatBox[CHATBOX_BUTTON] == NULL)
		{
			m_ChatBox[CHATBOX_BUTTON] = UControl::sm_System->CreateDialogFromFile("Chat\\ButtonGroup.udg");	
			if (m_ChatBox[CHATBOX_BUTTON] == NULL)
			{
				UMessageBox::MsgBox("CHATBOX_BUTTON");
				return FALSE;
			}
			else
			{
				Frame->AddChild(m_ChatBox[CHATBOX_BUTTON]);
				m_ChatBox[CHATBOX_BUTTON]->SetId(FRAME_IG_CHATBUTTONGROUP);
			}
		}
	
		//信息显示框创建
		if (m_ChatBox[CHATBOX_MSGSHOW] == NULL)
		{
			m_ChatBox[CHATBOX_MSGSHOW] = UControl::sm_System->CreateDialogFromFile("Chat\\MsgShowBox.udg");
			if (m_ChatBox[CHATBOX_MSGSHOW] == NULL)
			{
				UMessageBox::MsgBox("CHATBOX_MSGSHOW");
				return FALSE;
			}
			else
			{
				Frame->AddChild(m_ChatBox[CHATBOX_MSGSHOW]);
				m_ChatBox[CHATBOX_MSGSHOW]->SetId(FRAME_IG_CHATMSGSHOW);
			}
		}

		//由于频道选择框有功能依赖于信息显示框的所以在其后创建
		//频道选择框创建
		if (m_ChatBox[CHATBOX_CHANNEL] == NULL)
		{
			m_ChatBox[CHATBOX_CHANNEL] = UControl::sm_System->CreateDialogFromFile("Chat\\ChannelBox.udg");
			if (m_ChatBox[CHATBOX_CHANNEL] == NULL)
			{
				UMessageBox::MsgBox("CHATBOX_CHANNEL");
				return FALSE;
			}
			else
			{
				Frame->AddChild(m_ChatBox[CHATBOX_CHANNEL]);
				m_ChatBox[CHATBOX_CHANNEL]->SetId(FRAME_IG_CHATCHANNELBOX);
				UChatChannelBox * ccb = UDynamicCast(UChatChannelBox,GetChatBox(CHATBOX_CHANNEL));
				if (ccb)
				{
					ccb->InitChannel();
				}
			}
		}

		if (m_ChatBox[CHATBOX_FACEIMGE] == NULL)
		{
			m_ChatBox[CHATBOX_FACEIMGE] = UControl::sm_System->CreateDialogFromFile("Chat\\Face.udg");
			if (m_ChatBox[CHATBOX_FACEIMGE] == NULL)
			{
				//UMessageBox::MsgBox("CHATBOX_CHANNEL");
				return FALSE;
			}
			else
			{
				Frame->AddChild(m_ChatBox[CHATBOX_FACEIMGE]);
				m_ChatBox[CHATBOX_FACEIMGE]->SetId(FRAME_IG_FACEIMAGE);
				m_ChatBox[CHATBOX_FACEIMGE]->SetVisible(FALSE);
			}
		}
		if (m_ChatBox[CHATBOX_FACEIMGE] == NULL)
		{
			m_ChatBox[CHATBOX_FACEIMGE] = UControl::sm_System->CreateDialogFromFile("Chat\\Face.udg");
			if (m_ChatBox[CHATBOX_FACEIMGE] == NULL)
			{
				//UMessageBox::MsgBox("CHATBOX_CHANNEL");
				return FALSE;
			}
			else
			{
				Frame->AddChild(m_ChatBox[CHATBOX_FACEIMGE]);
				m_ChatBox[CHATBOX_FACEIMGE]->SetId(FRAME_IG_FACEIMAGE);
				m_ChatBox[CHATBOX_FACEIMGE]->SetVisible(FALSE);
			}
		}
		if (m_ChatBox[CHATBOX_HIDECHATBUTTON] == NULL)
		{
			m_ChatBox[CHATBOX_HIDECHATBUTTON] = UControl::sm_System->CreateDialogFromFile("Chat\\UHideButton.udg");
			if (m_ChatBox[CHATBOX_HIDECHATBUTTON] == NULL)
			{
				return FALSE;
			}
			else
			{
				Frame->AddChild(m_ChatBox[CHATBOX_HIDECHATBUTTON]);
				m_ChatBox[CHATBOX_HIDECHATBUTTON]->SetId(FRAME_IG_CHATHIDEBUTTON);
			}
		}

		//喊话喇叭
		if (m_ChatBox[CHATBOX_PROPS] == NULL)
		{
			m_ChatBox[CHATBOX_PROPS] = UControl::sm_System->CreateDialogFromFile("Chat\\UChatProps.udg");
			if (m_ChatBox[CHATBOX_PROPS] == NULL)
			{
				return FALSE;
			}
			Frame->AddChild(m_ChatBox[CHATBOX_PROPS]);
			m_ChatBox[CHATBOX_PROPS]->SetId(FRAME_IG_CHATPROPS);
		}

		if (m_ChatBox[CHATBOX_DALABA] == NULL)
		{
			m_ChatBox[CHATBOX_DALABA] = UControl::sm_System->CreateDialogFromFile("Chat\\UChatDALABA.udg");
			if (m_ChatBox[CHATBOX_DALABA] == NULL)
			{
				return FALSE;
			}
			Frame->AddChild(m_ChatBox[CHATBOX_DALABA]);
			m_ChatBox[CHATBOX_DALABA]->SetId(FRAME_IG_DALABA);
		}

		if (m_ChatBox[CHATBOX_CHATTABLE] == NULL)
		{
			m_ChatBox[CHATBOX_CHATTABLE] = UControl::sm_System->CreateDialogFromFile("Chat\\UChatTable.udg");
			if (m_ChatBox[CHATBOX_CHATTABLE] == NULL)
			{
				return FALSE;
			}
			Frame->AddChild(m_ChatBox[CHATBOX_CHATTABLE]);
			m_ChatBox[CHATBOX_CHATTABLE]->SetId(FRAME_IG_CHATTABLE);
		}

		m_MaxHSize = Frame->GetHeight()/2;
		m_MinHSize = Frame->GetHeight()/4*3;
		HideMsgBox();
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
void UChat::ShowInputBox()
{
	UChatInputMsgBox * cimb = UDynamicCast(UChatInputMsgBox,GetChatBox(CHATBOX_INPUTMSG));
	UChatButtonGroup * cbg = UDynamicCast(UChatButtonGroup, GetChatBox(CHATBOX_BUTTON));
	if (cimb && cbg/* && !m_Hide*/)
	{
		if (m_Hide)
		{
			UHideChatButton* uhcb = UDynamicCast(UHideChatButton, GetChatBox(CHATBOX_HIDECHATBUTTON));
			if (uhcb)
			{
				uhcb->OnSetChatShow();
			}
		}
		cimb->ShowThis();
		cbg->SetChannelVisible(TRUE);
	}
}
void UChat::HideInputBox()
{
	UChatInputMsgBox * cimb = UDynamicCast(UChatInputMsgBox,GetChatBox(CHATBOX_INPUTMSG));
	UChatButtonGroup * cbg = UDynamicCast(UChatButtonGroup, GetChatBox(CHATBOX_BUTTON));
	if (cimb && cbg)
	{
		cimb->HideThis();
		cbg->SetChannelVisible(FALSE);
		if (GetChatBox(CHATBOX_FACEIMGE))
		{
			GetChatBox(CHATBOX_FACEIMGE)->SetVisible(FALSE);
		}
		
	}
}
void UChat::AppendNewTitle()
{
	UDALABA* pkDalaba = UDynamicCast(UDALABA,GetChatBox(CHATBOX_DALABA));
	if (pkDalaba)
	{
		pkDalaba->AddNewTitle();
	}
}
void UChat::AppendQuestMsg(ui32 QuestId, ui32 QuestStat)
{
	CQuestDB::stQuest* stQuestinfo = g_pkQusetInfo->GetQuestInfo(QuestId);
	if (!stQuestinfo)
	{
		return ;
	}

	string text = "";
	switch(QuestStat)
	{
	case GetNewQuest:
		{
			text = _TRAN(N_NOTICE_C70, stQuestinfo->strTitle.c_str());
		}
		break;
	case CanFinishQuest:
		{
			text = _TRAN(N_NOTICE_C71, stQuestinfo->strTitle.c_str());
		}break;
	case FinishQuest:
		{
			text = _TRAN(N_NOTICE_C72, stQuestinfo->strTitle.c_str());
		}break;
	case DropQuest:
		{
			text = _TRAN(N_NOTICE_C73, stQuestinfo->strTitle.c_str());
			SYState()->IAudio->PlayUiSound(UI_questFailed);
		}
		
		break;
	case FaildQuest:
		{
			//text = _TRAN(N_NOTICE_C74, stQuestinfo->strTitle.c_str());
		}
		break;
	}
	if (text.length())
	{
		ChatSystem->AppendChatMsg(CHAT_MSG_CLIENTSYSTEM, text.c_str());
	}

	UDALABA* pkDalaba = UDynamicCast(UDALABA,GetChatBox(CHATBOX_DALABA));
	if (pkDalaba)
	{
		pkDalaba->AddQuestMsg(QuestStat);
	}
}
void UChat::AppendChatMsg(int msg , const char * text)
{
	UChatMsgShow * cms = UDynamicCast(UChatMsgShow,GetChatBox(CHATBOX_MSGSHOW));
	UHideChatButton* hcbtn = UDynamicCast(UHideChatButton, GetChatBox(CHATBOX_HIDECHATBUTTON));
	std::string temp;
	temp = text;
	//temp = UTF8ToAnis(temp);
	ParseFaceBuffer(temp);
	temp = ParseItemMsg(temp);
	if (cms && hcbtn)
	{
		if (msg == CHAT_MSG_SYSTEM)
		{
			hcbtn->OnSetChatShow();
			//ShowChatSystem();
		}
		cms->AppendText(msg,temp.c_str());

		//大喇叭
		if (msg == CHAT_MSG_DALABA || msg == CHAT_MSG_SYSTEM_BROADCAST)
		{
			ChannelData * cd = ChatSystem->GetChannelData(CHAT_MSG_DALABA);
			UChatShowDalaba* dalaba = UDynamicCast(UChatShowDalaba,GetChatBox(CHATBOX_DALABA)->GetChildByID(1));
			if (dalaba)
			{
				dalaba->AddDalabaText("<font:ArialBold:18><shadowcolor:20202064><shadow:1:1><color:" + cd->ChannelColor +">" + cd->ChannelName + "<linkcolor:"+cd->ChannelColor +">" + temp);
				GetChatBox(CHATBOX_DALABA)->GetParent()->MoveChildTo(GetChatBox(CHATBOX_DALABA), NULL);
			}
		}
		
	}
}
void UChat::SetChatSystemVisible(BOOL vis)
{
	if (vis)
	{
		ShowChatSystem();
	}
	else
	{
		HideChatSystem();
		m_ChatBox[CHATBOX_CHANNEL]->SetVisible(vis);
	}
	m_ChatBox[CHATBOX_BUTTON]->SetVisible(vis);
}
UControl * UChat::GetChatBox(int index)
{
	if (m_ChatBox[index])
	{
		return m_ChatBox[index];
	}
	else
	{
		return NULL;
	}
}
void UChat::SetScrollText(int way)
{
	if (ChatSystem)
	{
		UChatMsgShow * cms = UDynamicCast(UChatMsgShow,ChatSystem->GetChatBox(CHATBOX_MSGSHOW));
		if (cms)
		{
			cms->ScrollText(way);
		}
	}
}
int UChat::GetCurrentChannel()
{
	return m_CurrentChannel;
}
void UChat::SetCurrentChannel(int channel)
{
	if (m_CurrentChannel != channel)
	{
		m_CurrentChannel = channel;
		UChatChannelBox * ccb = UDynamicCast(UChatChannelBox,GetChatBox(CHATBOX_CHANNEL));
		if (ccb)
		{
			ccb->Refursh();
		}
	}
}

void UChat::SetPTalkPerson(const char * createname , ui64 guid)
{
	m_PData.CreateName = createname;
	m_PData.guid = guid;
	/*UChatInputMsgBox * cimb = UDynamicCast(UChatInputMsgBox,GetChatBox(CHATBOX_INPUTMSG));
	if (cimb)
	{
		cimb->SetPTalkPerson(createname,guid);
	}*/
}
void UChat::CallRMD(BOOL visible,const char * text)
{
	UInGame * pkInGame = UInGame::Get();
	if (pkInGame)
	{
		UIRightMouseList * pRightMouseList = INGAMEGETFRAME(UIRightMouseList,FRAME_IG_RIGHTMOUSELIST);
		if (pRightMouseList)
		{
			if (text == NULL)
			{
				return;
			}
			UPoint pos = UControl::sm_System->GetCursorPos();
			pRightMouseList->Popup(pos,BITMAPLIST_CHATMSG,text,0);
		}
	}
}
BOOL UChat::IsChannelShield(int msgtype)
{
	return m_ChannelData[msgtype].IsShildChannel;
}
void UChat::SetChannelShield(int msgtype,BOOL i)
{
	m_ChannelData[msgtype].IsShildChannel = i;
}
UChat::PersonData * UChat::GetPersonData()
{
	return &m_PData;
}
UChat::ChannelData * UChat::GetChannelData(int msgType)
{
	return &m_ChannelData[msgType];
}

bool UChat::GetAreaChannel(std::string& str)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return false;

	//CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	//if( !pkPlayer )
	//	return false;

	//const NiPoint3& kPos = pkPlayer->GetPosition();

	//unsigned int uiAreaId = pkMap->GetAreaId(kPos.x, kPos.y);

	char tmp[256];
	sprintf( tmp, "zone%d", pkMap->GetMapId());
	str = tmp;
	return true;
}
void UChat::SetWaitMap()
{
	mbWaitMap = true;
}
void UChat::WaitMapLoading()
{
	if(mbWaitMap)
	{
		std::string str;
		if(!ChatSystem->GetAreaChannel(str))
			return;
		ChatSystem->SendChannelJoin(str.c_str(),CHAT_CHANNEL_TYPE_CITY);
	}
}
void UChat::SetPChannelName(string channelname)
{
	if (channelname.size() == 0)
	{
		return;
	}
	m_ChannelData[CHAT_MSG_PRIVATE].ChannelHead = channelname;

	channelname = "["+channelname+"]";
	if (channelname == m_ChannelData[CHAT_MSG_PRIVATE].ChannelName)
	{
		return;
	}
	m_ChannelData[CHAT_MSG_PRIVATE].ChannelName = channelname;
	
}

void UChat::ParseCommand(const std::string &comm,const std::string &Text)
{
	//判断是否是呼叫频道命令
	BOOL IsShow = FALSE;
	for (int i = 0 ; i < CHAT_MSG_MAX ; i++)
	{
		if(GetChannelData(i)->MsgType != -1)
		{
			if(_stricmp(comm.c_str(),GetChannelData(CHAT_MSG_WHISPER)->CallCommand) == 0)
			{
				if (Text.size())
				{
					SetPTalkPerson(Text.c_str());
					SetCurrentChannel(CHAT_MSG_WHISPER);
					IsShow = TRUE;
					break;
				}
			}
			if (_stricmp(comm.c_str(),GetChannelData(i)->CallCommand) == 0)
			{
				SetCurrentChannel(GetChannelData(i)->MsgType);
				IsShow = TRUE;
				break;
			}
		}
	}
	char buf[256];
	std::string sComm = comm;
	sComm = strupr(strcpy(buf,comm.c_str()));
	CommandMap::iterator it = m_CommandMap.find(sComm);
	if(it != m_CommandMap.end())
	{
		std::string sArgs;
		std::vector<std::string> vArgs;
		vArgs.clear();
		int iBlankPos = Text.find_first_of(" ");
		sArgs = Text.substr(0, iBlankPos);
		if(strcmp(sArgs.c_str(), "") != 0)
			vArgs.push_back(sArgs);
		while(iBlankPos != -1)
		{
			int lastBlankPos = iBlankPos;
			iBlankPos = Text.find_first_of(" ", iBlankPos + 1);
			sArgs = Text.substr(lastBlankPos + 1, iBlankPos - lastBlankPos - 1);
			if(strcmp(sArgs.c_str(), "") != 0)
				vArgs.push_back(sArgs);
		}
		(*it->second)(vArgs);
	}
	if (IsShow)
	{
		HideInputBox();
		ShowInputBox();
	}
}
void UChat::AppInputText(const char* Text)
{
	UChatInputMsgBox* cimb = UDynamicCast(UChatInputMsgBox,m_ChatBox[CHATBOX_INPUTMSG]);
	if (cimb)
	{
		cimb->AppUserMessage(Text);
	}
}
void UChat::ShowChatSystem()
{
	UChatMsgShow * cms = UDynamicCast(UChatMsgShow,ChatSystem->GetChatBox(CHATBOX_MSGSHOW));
	UChatTableControl * ctb = UDynamicCast(UChatTableControl, GetChatBox(CHATBOX_CHATTABLE));
	if (cms && ctb)
	{
		cms->SetVisible(TRUE);
		ctb->SetVisible(TRUE);
		m_Hide = FALSE;
	}
}
void UChat::UpdataChannelTime(float fTime)
{
	for (int i = 0 ; i < CHAT_MSG_MAX; i++)
	{

		if (m_ChannelData[i].ChannelWaitTime > 0.0f 
			&& m_ChannelData[i].ChannelWaitTime > m_ChannelData[i].ChannelCurTime)
		{
			m_ChannelData[i].ChannelCurTime += fTime;
		}

	}
}
void UChat::SetVisMsg(int Index)
{
	if (Index < 0 || Index >= CHAT_TABLE_NUM)
		return;
	UChatMsgShow * cms = UDynamicCast(UChatMsgShow,ChatSystem->GetChatBox(CHATBOX_MSGSHOW));
	UChatButtonGroup * cbg = UDynamicCast(UChatButtonGroup, GetChatBox(CHATBOX_BUTTON));
	UChatChannelBox * ccb = UDynamicCast(UChatChannelBox,GetChatBox(CHATBOX_CHANNEL));
	if (cms)
	{
		cms->SetShowType(Index);
	}
	bool bCBtnActive = true;
	switch(Index)
	{
	case CHAT_TABLE_ALL:
		SetCurrentChannel(CHAT_MSG_SAY);
		break;
	case CHAT_TABLE_PARTY:
		if( SocialitySys->GetTeamSysPtr()->IsTuanDui())
			SetCurrentChannel(CHAT_MSG_RAID);
		else
			SetCurrentChannel(CHAT_MSG_PARTY);
		break;
	case CHAT_TABLE_GUILD:
		SetCurrentChannel(CHAT_MSG_GUILD);
		break;
	case CHAT_TABLE_WHISPER:
		SetCurrentChannel(CHAT_MSG_WHISPER);
		break;
	case CHAT_TABLE_AREA:
		SetCurrentChannel(CHAT_MSG_CITY);
		break;
	case CHAT_TABLE_BATTLE:
		SetCurrentChannel(CHAT_MSG_SAY);
		bCBtnActive = false;
		ccb->SetVisible(FALSE);
		break;
	}
	cbg->SetCBtnActive(bCBtnActive);
	cbg->SetChannelButtonBkg();
	UChatInputMsgBox * cimb = UDynamicCast(UChatInputMsgBox,GetChatBox(CHATBOX_INPUTMSG));
	if (cimb->IsVisible())
	{
		cimb->ShowThis();
	}
	m_visStore = Index;
	UChat::MsgStore* pStore = ChatSystem->GetMsgStore(m_visStore);
	if (pStore)
	{
		ccb->SetModify(pStore->VisModify);
	}
}

void UChat::UpdataDALABA()
{
	UChatButtonGroup * cbg = UDynamicCast(UChatButtonGroup, GetChatBox(CHATBOX_BUTTON));

	bool bDALABA = false;
	CPlayer* pLcalPlayer = ObjectMgr->GetLocalPlayer();
	if (pLcalPlayer)
	{
		CStorage* pBagContainer = pLcalPlayer->GetItemContainer();
		if (pBagContainer)
		{
			UINT Count = pBagContainer->GetItemCnt(SPECIAL_ITEM_DALABA);
			if (Count)
			{
				bDALABA = true;
			}
		}
	}
	cbg->UpdataDALABA(bDALABA);
}

void UChat::ShowChatProps(UINT propstype)
{
	UChatPropsMsg* pkMsgBox = UDynamicCast(UChatPropsMsg, ChatSystem->GetChatBox(CHATBOX_PROPS));
	if (pkMsgBox)
	{
		pkMsgBox->SetChatPropsType(propstype);
	}
}
void UChat::HideChatSystem()
{
	UChatMsgShow * cms = UDynamicCast(UChatMsgShow,ChatSystem->GetChatBox(CHATBOX_MSGSHOW));
	UChatTableControl * ctb = UDynamicCast(UChatTableControl, GetChatBox(CHATBOX_CHATTABLE));
	if (cms && ctb)
	{
		cms->SetVisible(FALSE);
		ctb->SetVisible(FALSE);
		HideInputBox();
		m_Hide = TRUE;
	}
}
void UChat::InitChannelData()
{
	//CHAT_MSG_SAY
	{
		m_ChannelData[CHAT_MSG_SAY].MsgType = CHAT_MSG_SAY;
		m_ChannelData[CHAT_MSG_SAY].ChannelName = _TRAN("[Say]");
		m_ChannelData[CHAT_MSG_SAY].ChannelHead = _TRAN("[Say]");
		m_ChannelData[CHAT_MSG_SAY].CallCommand = "p";
		m_ChannelData[CHAT_MSG_SAY].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_SAY].ChannelColor = "FFFFFF";
		m_ChannelData[CHAT_MSG_SAY].ChannelWaitTime = 2.0f;
		m_ChannelData[CHAT_MSG_SAY].ChannelCurTime = 2.0f;
	}
	//CHAT_MSG_PARTY
	{
		m_ChannelData[CHAT_MSG_PARTY].MsgType = CHAT_MSG_PARTY;
		m_ChannelData[CHAT_MSG_PARTY].ChannelName = _TRAN("[Party]");
		m_ChannelData[CHAT_MSG_PARTY].ChannelHead = _TRAN("[Party]");
		m_ChannelData[CHAT_MSG_PARTY].CallCommand = "x";
		m_ChannelData[CHAT_MSG_PARTY].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_PARTY].ChannelColor = "91b2f1";
		m_ChannelData[CHAT_MSG_PARTY].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_PARTY].ChannelCurTime = 0.0f;
	}
	//CHAT_MSG_GUILD
	{
		m_ChannelData[CHAT_MSG_GUILD].MsgType = CHAT_MSG_GUILD;
		m_ChannelData[CHAT_MSG_GUILD].ChannelName = _TRAN("[Guild]");
		m_ChannelData[CHAT_MSG_GUILD].ChannelHead = _TRAN("[Guild]");
		m_ChannelData[CHAT_MSG_GUILD].CallCommand = "b";
		m_ChannelData[CHAT_MSG_GUILD].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_GUILD].ChannelColor = "F58369";
		m_ChannelData[CHAT_MSG_GUILD].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_GUILD].ChannelCurTime = 0.0f;
	}
	//CHAT_MSG_WHISPER
	{
		m_ChannelData[CHAT_MSG_WHISPER].MsgType = CHAT_MSG_WHISPER;
		m_ChannelData[CHAT_MSG_WHISPER].ChannelName = _TRAN("[Whisper]");
		m_ChannelData[CHAT_MSG_WHISPER].ChannelHead = _TRAN("[Whisper]");
		m_ChannelData[CHAT_MSG_WHISPER].CallCommand = "s";
		m_ChannelData[CHAT_MSG_WHISPER].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_WHISPER].ChannelColor = "ee7bfe";
		m_ChannelData[CHAT_MSG_WHISPER].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_WHISPER].ChannelCurTime = 0.0f;

	}
	//CHAT_MSG_WHISPER_INFORM
	{
		m_ChannelData[CHAT_MSG_WHISPER_INFORM].MsgType = CHAT_MSG_WHISPER_INFORM;
		m_ChannelData[CHAT_MSG_WHISPER_INFORM].ChannelName = _TRAN("[Send To]");
		m_ChannelData[CHAT_MSG_WHISPER_INFORM].ChannelHead = "NULL";//如果没有头的话就填为空，一般这种空情况是非玩家输入频道
		m_ChannelData[CHAT_MSG_WHISPER_INFORM].CallCommand = " ";//如果没有唤出命令就用空格
		m_ChannelData[CHAT_MSG_WHISPER_INFORM].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_WHISPER_INFORM].ChannelColor = "ee7bfe";
		m_ChannelData[CHAT_MSG_WHISPER_INFORM].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_WHISPER_INFORM].ChannelCurTime = 0.0f;
	}
	//CHAT_MSG_SYSTEM
	{
		m_ChannelData[CHAT_MSG_SYSTEM].MsgType = CHAT_MSG_SYSTEM;
		m_ChannelData[CHAT_MSG_SYSTEM].ChannelName = _TRAN("[Obelisk]:");
		m_ChannelData[CHAT_MSG_SYSTEM].ChannelHead = "NULL";
		m_ChannelData[CHAT_MSG_SYSTEM].CallCommand = " ";
		m_ChannelData[CHAT_MSG_SYSTEM].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_SYSTEM].ChannelColor = "00EBEE";
		m_ChannelData[CHAT_MSG_SYSTEM].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_SYSTEM].ChannelCurTime = 0.0f;

	}
	//CHAT_MSG_PICKITEM
	{
		m_ChannelData[CHAT_MSG_PICKITEM].MsgType = CHAT_MSG_PICKITEM;
		m_ChannelData[CHAT_MSG_PICKITEM].ChannelName = _TRAN("[Loot]");
		m_ChannelData[CHAT_MSG_PICKITEM].ChannelHead = "NULL";
		m_ChannelData[CHAT_MSG_PICKITEM].CallCommand = " ";
		m_ChannelData[CHAT_MSG_PICKITEM].ChannelFont = MAKEFONT(Arial, 12);
		m_ChannelData[CHAT_MSG_PICKITEM].ChannelColor = "FFFF00";
		m_ChannelData[CHAT_MSG_PICKITEM].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_PICKITEM].ChannelCurTime = 0.0f;
	}	
	//CHAT_MSG_RAID
	{
		m_ChannelData[CHAT_MSG_RAID].MsgType = CHAT_MSG_RAID;
		m_ChannelData[CHAT_MSG_RAID].ChannelName = _TRAN("[Raid]");
		m_ChannelData[CHAT_MSG_RAID].ChannelHead = _TRAN("[Raid]");
		m_ChannelData[CHAT_MSG_RAID].CallCommand = "r";
		m_ChannelData[CHAT_MSG_RAID].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_RAID].ChannelColor = "FF9933";
		m_ChannelData[CHAT_MSG_RAID].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_RAID].ChannelCurTime = 0.0f;
	}
	//CHAT_MSG_RAID_LEADER
	{
		m_ChannelData[CHAT_MSG_RAID_LEADER].MsgType = CHAT_MSG_RAID_LEADER;
		m_ChannelData[CHAT_MSG_RAID_LEADER].ChannelName = _TRAN("[Raid Leader]");
		m_ChannelData[CHAT_MSG_RAID_LEADER].ChannelHead = "NULL";
		m_ChannelData[CHAT_MSG_RAID_LEADER].CallCommand = " ";
		m_ChannelData[CHAT_MSG_RAID_LEADER].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_RAID_LEADER].ChannelColor = "FF6600";
		m_ChannelData[CHAT_MSG_RAID_LEADER].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_RAID_LEADER].ChannelCurTime = 0.0f;
	}
	//CHAT_MSG_CLIENTSYSTEM
	{
		m_ChannelData[CHAT_MSG_CLIENTSYSTEM].MsgType = CHAT_MSG_CLIENTSYSTEM;
		m_ChannelData[CHAT_MSG_CLIENTSYSTEM].ChannelName = "";
		m_ChannelData[CHAT_MSG_CLIENTSYSTEM].ChannelHead = "NULL";
		m_ChannelData[CHAT_MSG_CLIENTSYSTEM].CallCommand = " ";
		m_ChannelData[CHAT_MSG_CLIENTSYSTEM].ChannelFont = MAKEFONT(Arial, 12);
		m_ChannelData[CHAT_MSG_CLIENTSYSTEM].ChannelColor = "FFFF00";
		m_ChannelData[CHAT_MSG_CLIENTSYSTEM].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_CLIENTSYSTEM].ChannelCurTime = 0.0f;
	}	
	//CHAT_MSG_GUILDINFO
	{
		m_ChannelData[CHAT_MSG_GUILDINFO].MsgType = CHAT_MSG_GUILDINFO;
		m_ChannelData[CHAT_MSG_GUILDINFO].ChannelName = _TRAN("Guild Announcement:<br>");
		m_ChannelData[CHAT_MSG_GUILDINFO].ChannelHead = _TRAN("Guild Announcement:<br>");
		m_ChannelData[CHAT_MSG_GUILDINFO].CallCommand = " ";
		m_ChannelData[CHAT_MSG_GUILDINFO].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_GUILDINFO].ChannelColor = "E29501";
		m_ChannelData[CHAT_MSG_GUILDINFO].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_GUILDINFO].ChannelCurTime = 0.0f;
	}
	//CHAT_MSG_CHANNEL_JOIN
	{
		m_ChannelData[CHAT_MSG_CHANNEL_JOIN].MsgType = CHAT_MSG_CHANNEL_JOIN;
		m_ChannelData[CHAT_MSG_CHANNEL_JOIN].ChannelName = "";
		m_ChannelData[CHAT_MSG_CHANNEL_JOIN].ChannelHead = "NULL";
		m_ChannelData[CHAT_MSG_CHANNEL_JOIN].CallCommand = " ";
		m_ChannelData[CHAT_MSG_CHANNEL_JOIN].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_CHANNEL_JOIN].ChannelColor = "996700";
		m_ChannelData[CHAT_MSG_CHANNEL_JOIN].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_CHANNEL_JOIN].ChannelCurTime = 0.0f;
	}
	//CHAT_MSG_CHANNEL_LEAVE
	{
		m_ChannelData[CHAT_MSG_CHANNEL_LEAVE].MsgType = CHAT_MSG_CHANNEL_LEAVE;
		m_ChannelData[CHAT_MSG_CHANNEL_LEAVE].ChannelName = "";
		m_ChannelData[CHAT_MSG_CHANNEL_LEAVE].ChannelHead = "NULL";
		m_ChannelData[CHAT_MSG_CHANNEL_LEAVE].CallCommand = " ";
		m_ChannelData[CHAT_MSG_CHANNEL_LEAVE].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_CHANNEL_LEAVE].ChannelColor = "996700";
		m_ChannelData[CHAT_MSG_CHANNEL_LEAVE].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_CHANNEL_LEAVE].ChannelCurTime = 0.0f;
	}

	// CHAT_MSG_CHANNEL的子类型频道
	//	CHAT_MSG_PRIVATE
	{
		m_ChannelData[CHAT_MSG_PRIVATE].MsgType = CHAT_MSG_PRIVATE;
		m_ChannelData[CHAT_MSG_PRIVATE].ChannelName = _TRAN("PrivateChan");
		m_ChannelData[CHAT_MSG_PRIVATE].ChannelHead = _TRAN("PrivateChan");
		m_ChannelData[CHAT_MSG_PRIVATE].CallCommand = " ";
		m_ChannelData[CHAT_MSG_PRIVATE].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_PRIVATE].ChannelColor = "996700";
		m_ChannelData[CHAT_MSG_PRIVATE].ChannelWaitTime = 2.0f;
		m_ChannelData[CHAT_MSG_PRIVATE].ChannelCurTime = 2.0f;
	}
	//CHAT_MSG_CITY
	{
		m_ChannelData[CHAT_MSG_CITY].MsgType = CHAT_MSG_CITY;
		m_ChannelData[CHAT_MSG_CITY].ChannelName = _TRAN("[City]");
		m_ChannelData[CHAT_MSG_CITY].ChannelHead = _TRAN("[City]");
		m_ChannelData[CHAT_MSG_CITY].CallCommand = "q";
		m_ChannelData[CHAT_MSG_CITY].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_CITY].ChannelColor = "33F000";
		m_ChannelData[CHAT_MSG_CITY].ChannelWaitTime = 2.f;
		m_ChannelData[CHAT_MSG_CITY].ChannelCurTime = 2.f;
	}
	//CHAT_MSG_DALABA
	{
		m_ChannelData[CHAT_MSG_DALABA].MsgType = CHAT_MSG_DALABA;
		m_ChannelData[CHAT_MSG_DALABA].ChannelName = _TRAN("[Obelisk]");
		m_ChannelData[CHAT_MSG_DALABA].ChannelHead = _TRAN("[Obelisk]");
		m_ChannelData[CHAT_MSG_DALABA].CallCommand = " ";
		m_ChannelData[CHAT_MSG_DALABA].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_DALABA].ChannelColor = "FFD188";
		m_ChannelData[CHAT_MSG_DALABA].ChannelWaitTime= 0.0f;
		m_ChannelData[CHAT_MSG_DALABA].ChannelCurTime= 0.0f;
	}
	//CHAT_MSG_SYSTEM_BROADCAST
	{
		m_ChannelData[CHAT_MSG_SYSTEM_BROADCAST].MsgType = CHAT_MSG_DALABA;
		m_ChannelData[CHAT_MSG_SYSTEM_BROADCAST].ChannelName = _TRAN("[Obelisk Announcment]");
		m_ChannelData[CHAT_MSG_SYSTEM_BROADCAST].ChannelHead = _TRAN("[Obelisk Announcment]");
		m_ChannelData[CHAT_MSG_SYSTEM_BROADCAST].CallCommand = " ";
		m_ChannelData[CHAT_MSG_SYSTEM_BROADCAST].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_SYSTEM_BROADCAST].ChannelColor = "FFD188";
		m_ChannelData[CHAT_MSG_SYSTEM_BROADCAST].ChannelWaitTime= 0.0f;
		m_ChannelData[CHAT_MSG_SYSTEM_BROADCAST].ChannelCurTime= 0.0f;
	}
	//CHAT_MSG_XIAOLABA
	{
		m_ChannelData[CHAT_MSG_XIAOLABA].MsgType = CHAT_MSG_XIAOLABA;
		m_ChannelData[CHAT_MSG_XIAOLABA].ChannelName = _TRAN("[Faction]");
		m_ChannelData[CHAT_MSG_XIAOLABA].ChannelHead = _TRAN("[Faction]");
		m_ChannelData[CHAT_MSG_XIAOLABA].CallCommand = " ";
		m_ChannelData[CHAT_MSG_XIAOLABA].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_XIAOLABA].ChannelColor = "FF5E93";
		m_ChannelData[CHAT_MSG_XIAOLABA].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_XIAOLABA].ChannelCurTime = 0.0f;
	}
	//	CHAT_MSG_TRADE
	{
		m_ChannelData[CHAT_MSG_TRADE].MsgType = CHAT_MSG_TRADE;
		m_ChannelData[CHAT_MSG_TRADE].ChannelName = _TRAN("[Trade]");
		m_ChannelData[CHAT_MSG_TRADE].ChannelHead = _TRAN("[Trade]");
		m_ChannelData[CHAT_MSG_TRADE].CallCommand = " ";
		m_ChannelData[CHAT_MSG_TRADE].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_TRADE].ChannelColor = "33F000";
		m_ChannelData[CHAT_MSG_TRADE].ChannelWaitTime = 2.f;
		m_ChannelData[CHAT_MSG_TRADE].ChannelCurTime = 2.f;
	}
	//	CHAT_MSG_LFG
	{
		m_ChannelData[CHAT_MSG_LFG].MsgType = CHAT_MSG_LFG;
		m_ChannelData[CHAT_MSG_LFG].ChannelName = _TRAN("[L.F.G.]");
		m_ChannelData[CHAT_MSG_LFG].ChannelHead = _TRAN("[L.F.G.]");
		m_ChannelData[CHAT_MSG_LFG].CallCommand = " ";
		m_ChannelData[CHAT_MSG_LFG].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_LFG].ChannelColor = "996700";
		m_ChannelData[CHAT_MSG_LFG].ChannelWaitTime = 2.f;
		m_ChannelData[CHAT_MSG_LFG].ChannelCurTime = 2.f;
	}
	// CHAT_MSG_BATTLE
	{
		m_ChannelData[CHAT_MSG_BATTLE].MsgType = CHAT_MSG_BATTLE;
		m_ChannelData[CHAT_MSG_BATTLE].ChannelName = _TRAN("[Battle]");
		m_ChannelData[CHAT_MSG_BATTLE].ChannelHead = "NULL";
		m_ChannelData[CHAT_MSG_BATTLE].CallCommand = " ";
		m_ChannelData[CHAT_MSG_BATTLE].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_BATTLE].ChannelColor = "FF0000";
		m_ChannelData[CHAT_MSG_BATTLE].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_BATTLE].ChannelCurTime = 0.0f;
	}

	// CHAT_MSG_MONSTER_SAY
	{
		m_ChannelData[CHAT_MSG_MONSTER_SAY].MsgType = CHAT_MSG_MONSTER_SAY;
		m_ChannelData[CHAT_MSG_MONSTER_SAY].ChannelName = "";
		m_ChannelData[CHAT_MSG_MONSTER_SAY].ChannelHead = "NULL";
		m_ChannelData[CHAT_MSG_MONSTER_SAY].CallCommand = " ";
		m_ChannelData[CHAT_MSG_MONSTER_SAY].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_MONSTER_SAY].ChannelColor = "ECDB42";
		m_ChannelData[CHAT_MSG_MONSTER_SAY].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_MONSTER_SAY].ChannelCurTime = 0.0f;
	}
	// CHAT_MSG_MONSTER_YELL
	{
		m_ChannelData[CHAT_MSG_MONSTER_YELL].MsgType = CHAT_MSG_MONSTER_YELL;
		m_ChannelData[CHAT_MSG_MONSTER_YELL].ChannelName = "";
		m_ChannelData[CHAT_MSG_MONSTER_YELL].ChannelHead = "NULL";
		m_ChannelData[CHAT_MSG_MONSTER_YELL].CallCommand = " ";
		m_ChannelData[CHAT_MSG_MONSTER_YELL].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_MONSTER_YELL].ChannelColor = "BD0000";
		m_ChannelData[CHAT_MSG_MONSTER_YELL].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_MONSTER_YELL].ChannelCurTime = 0.0f;
	}
	// CHAT_MSG_AFK
	{
		m_ChannelData[CHAT_MSG_AFK].MsgType = CHAT_MSG_AFK;
		m_ChannelData[CHAT_MSG_AFK].ChannelName = _TRAN("(AFK)");
		m_ChannelData[CHAT_MSG_AFK].ChannelHead = _TRAN("(AFK)");
		m_ChannelData[CHAT_MSG_AFK].CallCommand = " ";
		m_ChannelData[CHAT_MSG_AFK].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_AFK].ChannelColor = "ee7bfe";
		m_ChannelData[CHAT_MSG_AFK].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_AFK].ChannelCurTime = 0.0f;
	}

	// CHAT_MSG_DND
	{
		m_ChannelData[CHAT_MSG_DND].MsgType = CHAT_MSG_MONSTER_YELL;
		m_ChannelData[CHAT_MSG_DND].ChannelName =  _TRAN("(DND)");
		m_ChannelData[CHAT_MSG_DND].ChannelHead =  _TRAN("(DND)");
		m_ChannelData[CHAT_MSG_DND].CallCommand = " ";
		m_ChannelData[CHAT_MSG_DND].ChannelFont = MAKEFONT(Arial, 14);
		m_ChannelData[CHAT_MSG_DND].ChannelColor = "ee7bfe";
		m_ChannelData[CHAT_MSG_DND].ChannelWaitTime = 0.0f;
		m_ChannelData[CHAT_MSG_DND].ChannelCurTime = 0.0f;
	}



	////TODO:Loading File
	{
		if(!SystemSetup->LoadFile(FILE_CHATSETTING))
		{
			//因为还没有彻底定好脚本格式，可能会有添加，为了向下兼容不会出错，这里做出错处理
			ChatSystem->SetChatSettingDefault();
			SystemSetup->SaveChatSetting();
			SystemSetup->LoadFile(FILE_CHATSETTING);
		}

		

		//loading end ,Now set current type

		UChatTableControl* ctc = UDynamicCast(UChatTableControl,GetChatBox(CHATBOX_CHATTABLE));
		if (ctc)
		{
			UChatTableButton* pChatTableBtn = UDynamicCast(UChatTableButton, ctc->GetChildByID(CHAT_TABLE_ALL));
			if (pChatTableBtn)
			{
				pChatTableBtn->SetCheck(FALSE);
			}
		}
	}

}

void UChat::SetChatSettingDefault()
{
	SetTableModify(CHAT_TABLE_ALL, MSG_AREA, TRUE);
	SetTableModify(CHAT_TABLE_ALL, MSG_WHISPER, TRUE);
	SetTableModify(CHAT_TABLE_ALL, MSG_GUILD, TRUE);
	SetTableModify(CHAT_TABLE_ALL, MSG_PARTY, TRUE);
	SetTableModify(CHAT_TABLE_ALL, MSG_OTHER, TRUE);
	SetTableModify(CHAT_TABLE_ALL, MSG_BATTLE, FALSE);
	SetTableModify(CHAT_TABLE_ALL, MSG_SAY, TRUE);
	SetTableModify(CHAT_TABLE_ALL, MSG_SYSTEM, TRUE);
	SetTableModify(CHAT_TABLE_ALL, MSG_CLIENTSYSTEM, TRUE);

	SetTableModify(CHAT_TABLE_PARTY, MSG_AREA, FALSE);
	SetTableModify(CHAT_TABLE_PARTY, MSG_WHISPER, FALSE);
	SetTableModify(CHAT_TABLE_PARTY, MSG_GUILD, FALSE);
	SetTableModify(CHAT_TABLE_PARTY, MSG_PARTY, TRUE);
	SetTableModify(CHAT_TABLE_PARTY, MSG_OTHER, FALSE);
	SetTableModify(CHAT_TABLE_PARTY, MSG_BATTLE, FALSE);
	SetTableModify(CHAT_TABLE_PARTY, MSG_SAY, FALSE);
	SetTableModify(CHAT_TABLE_PARTY, MSG_SYSTEM, TRUE);
	SetTableModify(CHAT_TABLE_PARTY, MSG_CLIENTSYSTEM, FALSE);

	SetTableModify(CHAT_TABLE_AREA, MSG_AREA, TRUE);
	SetTableModify(CHAT_TABLE_AREA, MSG_WHISPER, FALSE);
	SetTableModify(CHAT_TABLE_AREA, MSG_GUILD, FALSE);
	SetTableModify(CHAT_TABLE_AREA, MSG_PARTY, FALSE);
	SetTableModify(CHAT_TABLE_AREA, MSG_OTHER, FALSE);
	SetTableModify(CHAT_TABLE_AREA, MSG_BATTLE, FALSE);
	SetTableModify(CHAT_TABLE_AREA, MSG_SAY, FALSE);
	SetTableModify(CHAT_TABLE_AREA, MSG_SYSTEM, TRUE);
	SetTableModify(CHAT_TABLE_AREA, MSG_CLIENTSYSTEM, FALSE);


	SetTableModify(CHAT_TABLE_GUILD, MSG_AREA, FALSE);
	SetTableModify(CHAT_TABLE_GUILD, MSG_WHISPER, FALSE);
	SetTableModify(CHAT_TABLE_GUILD, MSG_GUILD, TRUE);
	SetTableModify(CHAT_TABLE_GUILD, MSG_PARTY, FALSE);
	SetTableModify(CHAT_TABLE_GUILD, MSG_OTHER, FALSE);
	SetTableModify(CHAT_TABLE_GUILD, MSG_BATTLE, FALSE);
	SetTableModify(CHAT_TABLE_GUILD, MSG_SAY, FALSE);
	SetTableModify(CHAT_TABLE_GUILD, MSG_SYSTEM, TRUE);
	SetTableModify(CHAT_TABLE_GUILD, MSG_CLIENTSYSTEM, FALSE);


	SetTableModify(CHAT_TABLE_WHISPER, MSG_AREA, FALSE);
	SetTableModify(CHAT_TABLE_WHISPER, MSG_WHISPER, TRUE);
	SetTableModify(CHAT_TABLE_WHISPER, MSG_GUILD, FALSE);
	SetTableModify(CHAT_TABLE_WHISPER, MSG_PARTY, FALSE);
	SetTableModify(CHAT_TABLE_WHISPER, MSG_OTHER, FALSE);
	SetTableModify(CHAT_TABLE_WHISPER, MSG_BATTLE, FALSE);
	SetTableModify(CHAT_TABLE_WHISPER, MSG_SAY, FALSE);
	SetTableModify(CHAT_TABLE_WHISPER, MSG_SYSTEM, TRUE);
	SetTableModify(CHAT_TABLE_WHISPER, MSG_CLIENTSYSTEM, FALSE);


	SetTableModify(CHAT_TABLE_BATTLE, MSG_AREA, FALSE);
	SetTableModify(CHAT_TABLE_BATTLE, MSG_WHISPER, FALSE);
	SetTableModify(CHAT_TABLE_BATTLE, MSG_GUILD, FALSE);
	SetTableModify(CHAT_TABLE_BATTLE, MSG_PARTY, FALSE);
	SetTableModify(CHAT_TABLE_BATTLE, MSG_OTHER, FALSE);
	SetTableModify(CHAT_TABLE_BATTLE, MSG_BATTLE, TRUE);
	SetTableModify(CHAT_TABLE_BATTLE, MSG_SAY, FALSE);
	SetTableModify(CHAT_TABLE_BATTLE, MSG_SYSTEM, FALSE);
	SetTableModify(CHAT_TABLE_BATTLE, MSG_CLIENTSYSTEM, FALSE);
}

void UChat::Init()
{
	InitChannelData();
	ChatCallBackFunc* pFunc = g_pChatFuncHead;
	while (pFunc)
	{
		// MAP INSERT.
		m_CommandMap.insert(CommandMap::value_type(pFunc->pszName, pFunc->pCB));
		pFunc = pFunc->pNext;
	}
	for (int i = 0; i < CHAT_TABLE_NUM ; i++)
	{
		m_MsgStore[i].Clear();
	}
	UChatMsgShow * cms = UDynamicCast(UChatMsgShow,GetChatBox(CHATBOX_MSGSHOW));
	if (cms)
	{
		cms->ClearText();
	}
	m_PData.CreateName.clear();
	m_PData.guid = 0;
	m_Hide = FALSE;
	mbWaitMap = false;
}
void UChat::SendChannelJoin(const char * channelname,uint32 dbc_id)
{
	PacketBuilder->SendChannelJoin(channelname,dbc_id);
}
void UChat::SendChannelLeave(const char * channelname)
{
	PacketBuilder->SendchannelLeave(channelname);
}


#include "boost/regex.hpp"
void UChat::ParseFaceBuffer(std::string &str)
{
	boost::regex reg("\\[\\/(.*?)\\]");
	str = boost::regex_replace(str,reg,"<img:data/ui/chat/face/$1.png>");
}

void UChat::SendChatMsg(int msgtype,const std::string & str)
{
	string txt = str ;
	//ParseFaceBuffer(txt);
	switch (msgtype)
	{
	case CHAT_MSG_WHISPER:
		{
			if (!ObjectMgr->GetLocalPlayer()->GetName().Equals(m_PData.CreateName.c_str()))
			{
				PacketBuilder->SendPrivateChatMsg(/*UTF8ToAnis*/txt.c_str(),m_PData.guid,m_PData.CreateName.c_str());
			}
			if (m_visStore != CHAT_TABLE_WHISPER)
			{
				SetCurrentChannel(CHAT_MSG_SAY);
			}
		}
		break;
	default:
		{
			//////////////////////////////////////////////////////////////////////////
			//   频道是否符合发言时间判断
			//////////////////////////////////////////////////////////////////////////

			
			if (m_ChannelData[msgtype].ChannelCurTime >= m_ChannelData[msgtype].ChannelWaitTime)
			{
				PacketBuilder->SendChatReqMsg((ChatMsg)msgtype,/*UTF8ToAnis*/(txt).c_str());
				m_ChannelData[msgtype].ChannelCurTime  = 0.0f;
			}else
			{
				ClientSystemNotify(_TRAN("休息一下，您说话太快了！ "));
			}
			

			if (msgtype == CHAT_MSG_XIAOLABA || msgtype == CHAT_MSG_DALABA)
			{
				SetCurrentChannel(CHAT_MSG_SAY);
			}
		}
		break;
	}

}
void UChat::ChannelNotify(ui64 guid,ui8 notifyflag,string name,ui8 oldmemberflag,ui8 memberflag)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	switch (notifyflag)
	{
	case CHANNEL_NOTIFY_FLAG_JOINED		:
	case CHANNEL_NOTIFY_FLAG_LEFT		:
	case CHANNEL_NOTIFY_FLAG_YOUJOINED	:
	case CHANNEL_NOTIFY_FLAG_YOULEFT	:
	case CHANNEL_NOTIFY_FLAG_WRONGPASS	:
	case CHANNEL_NOTIFY_FLAG_NOTON		:
	case CHANNEL_NOTIFY_FLAG_NOTMOD		:
	case CHANNEL_NOTIFY_FLAG_SETPASS	:
	case CHANNEL_NOTIFY_FLAG_CHGOWNER	:
	case CHANNEL_NOTIFY_FLAG_NOT_ON_2	:
	case CHANNEL_NOTIFY_FLAG_NOT_OWNER	:
	case CHANNEL_NOTIFY_FLAG_WHO_OWNER	:
	case CHANNEL_NOTIFY_FLAG_MODE_CHG	:
	case CHANNEL_NOTIFY_FLAG_ENABLE_ANN	:
	case CHANNEL_NOTIFY_FLAG_DISABLE_ANN:
	case CHANNEL_NOTIFY_FLAG_MODERATED	:
	case CHANNEL_NOTIFY_FLAG_UNMODERATED:	
	case CHANNEL_NOTIFY_FLAG_YOUCANTSPEAK:
	case CHANNEL_NOTIFY_FLAG_KICKED		:
	case CHANNEL_NOTIFY_FLAG_YOURBANNED	:
	case CHANNEL_NOTIFY_FLAG_BANNED		:
	case CHANNEL_NOTIFY_FLAG_UNBANNED	:
	case CHANNEL_NOTIFY_FLAG_UNK_1		:
	case CHANNEL_NOTIFY_FLAG_ALREADY_ON	:
	case CHANNEL_NOTIFY_FLAG_INVITED	:	
	case CHANNEL_NOTIFY_FLAG_WRONG_FACT	:
	case CHANNEL_NOTIFY_FLAG_UNK_2		:
	case CHANNEL_NOTIFY_FLAG_UNK_3		:
	case CHANNEL_NOTIFY_FLAG_UNK_4		:
	case CHANNEL_NOTIFY_FLAG_YOU_INVITED:
	case CHANNEL_NOTIFY_FLAG_UNK_5		:
	case CHANNEL_NOTIFY_FLAG_UNK_6		:
	case CHANNEL_NOTIFY_FLAG_UNK_7		:
	case CHANNEL_NOTIFY_FLAG_NOT_IN_LFG	:
	case CHANNEL_NOTIFY_FLAG_VOICE_ON	:
	case CHANNEL_NOTIFY_FLAG_VOICE_OFF	:
		break;
	}; 
}

void UChat::SendQueryPlayer(std::string& Name)
{
	if (!Name.size() || m_QueryName.compare(Name) == 0)
		return;
	m_QueryName = Name;
	PacketBuilder->SendQueryPlayers(1, 80, 0, 0, 0, Name);
}

void UChat::ParseQueryPlayer(std::vector<MSG_S2C::stQueryPlayersAck::player_info>& vList)
{
	if (!m_QueryName.size())
		return;
	for (size_t ui = 0 ; ui < vList.size() ; ++ui)
	{
		if (vList[ui].name.compare(m_QueryName) == 0)
		{
			AppendChatMsg(CHAT_MSG_CLIENTSYSTEM, _TRAN("共找到1个玩家<br>"));
			//char buf[1024];
			std::string mapName;
			CMapInfoDB::MapInfoEntry entry;
			if(g_pkMapInfoDB->GetMapInfo(vList[ui].mapid, entry))
			{
				mapName = entry.desc;
			}else
				mapName = _TRAN("苍之陆"); 
			//sprintf(buf, "%s，种族：%s，等级：%u，职业：%s，现在位置：%s<br>", vList[ui].name.c_str(), race_name[vList[ui].race], vList[ui].level,class_name[vList[ui].cls], mapName.c_str());
			std::string queryStr = _TRAN(N_NOTICE_F10,vList[ui].name.c_str(), _TRAN(race_name[vList[ui].race]), _I2A(vList[ui].level).c_str(), _TRAN(class_name[vList[ui].cls]), mapName.c_str());
			if (vList[ui].guild_name.length())
			{
				queryStr += _TRAN("所属部族：") + vList[ui].guild_name + "<br>";
			}
			AppendChatMsg(CHAT_MSG_CLIENTSYSTEM, queryStr.c_str());
		}
	}
	m_QueryName.clear();
}

static char MsgBuf[1024];
static string Msgbuf;
void UChat::DamageMsg(ui64 target_guid, ui64 caster_guid, ui32 damageNum, ui32 victim_stat, 
					  ui32 hitStats, ui32 damage_abs, ui32 resisted_damage, ui32 blocked_damage)//输出伤害
{
	CCharacter* targerChar = (CCharacter*)ObjectMgr->GetObject(target_guid);
	CCharacter* casterChar = (CCharacter*)ObjectMgr->GetObject(caster_guid);
	if (targerChar && casterChar)
	{
		if (casterChar == (CCharacter*)ObjectMgr->GetLocalPlayer())
		{				
			MsgBuf[0] = '\0';
			Msgbuf.clear() ;
			if (victim_stat == ATTACK)
			{
				Msgbuf = _TRAN(N_NOTICE_F11, targerChar->GetName(), _I2A(damageNum).c_str());
				//sprintf(MsgBuf, "你对%s造成%u点伤害<br>",targerChar->GetName(),damageNum);
				if (hitStats & HITSTATUS_MISS)
				{
					Msgbuf = _TRAN(N_NOTICE_F12, targerChar->GetName());
					//sprintf(MsgBuf, "%s的普通攻击未命中你<br>", targerChar->GetName());
				}
				else if (hitStats & HITSTATUS_CRICTICAL)
				{
					Msgbuf = _TRAN(N_NOTICE_F13, targerChar->GetName(), _I2A(damageNum).c_str());
					//sprintf(MsgBuf, "你对%s造成%u点致命一击伤害<br>", targerChar->GetName(), damageNum);
				}
				if (Msgbuf.length())
				{
					AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
				}
			}
			else
			{
				switch (victim_stat)
				{
				case DODGE:
					{
						Msgbuf = _TRAN(N_NOTICE_F14, targerChar->GetName());
						//sprintf(MsgBuf, "%s躲闪了你的攻击<br>", targerChar->GetName());
						//AppendChatMsg(CHAT_MSG_BATTLE, MsgBuf);
						break;
					}
				case PARRY:
					{
						Msgbuf = _TRAN(N_NOTICE_F15, targerChar->GetName());
						//sprintf(MsgBuf, "%s招架了你的攻击<br>", targerChar->GetName());
						//AppendChatMsg(CHAT_MSG_BATTLE, MsgBuf);
						break;
					}
				case INTERRUPT:
					{
						Msgbuf = _TRAN(N_NOTICE_F16, targerChar->GetName());
						//sprintf(MsgBuf, "%s打断了你的攻击<br>", targerChar->GetName());
						//AppendChatMsg(CHAT_MSG_BATTLE, MsgBuf);
						break;
					}
				case BLOCK:
					{
						Msgbuf = _TRAN(N_NOTICE_F17, targerChar->GetName(),_I2A(damageNum).c_str(), _I2A(blocked_damage).c_str());
						//sprintf(MsgBuf, "你对%s造成%u点伤害(格挡%u点)<br>", targerChar->GetName(), damageNum, blocked_damage);
						//sprintf(MsgBuf, "%s格挡了你的攻击<br>", targerChar->GetName());
						//AppendChatMsg(CHAT_MSG_BATTLE, MsgBuf);
						break;
					}
				case EVADE://避开
					{
						Msgbuf = _TRAN(N_NOTICE_F18, targerChar->GetName());
						//sprintf(MsgBuf, "%s避开了你的攻击<br>", targerChar->GetName());
						//AppendChatMsg(CHAT_MSG_BATTLE, MsgBuf);
						break;
					}
				case IMMUNE://免疫
					{
						Msgbuf = _TRAN(N_NOTICE_F19, targerChar->GetName());
						//sprintf(MsgBuf, "%s对你的攻击免疫<br>", targerChar->GetName());
						//AppendChatMsg(CHAT_MSG_BATTLE, MsgBuf);
						break;
					}
				case DEFLECT://擦过
					{
						Msgbuf = _TRAN(N_NOTICE_F20, targerChar->GetName(), _I2A(damageNum).c_str());
						//sprintf(MsgBuf, "你对%s造成%u点伤害(偏斜)<br>", targerChar->GetName(), damageNum);
						break;
					}
				}

				if (Msgbuf.length())
				{
					AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
				}
			}
		}
		if (targerChar == ObjectMgr->GetLocalPlayer())
		{
			MsgBuf[0] = '\0';
			Msgbuf.clear() ;
			if (victim_stat == ATTACK)
			{
				Msgbuf = _TRAN(N_NOTICE_F21, casterChar->GetName(), _I2A(damageNum).c_str());
				//sprintf(MsgBuf, "%s对你造成%u点伤害<br>", casterChar->GetName(), damageNum);
				if (hitStats & HITSTATUS_MISS)
				{
					Msgbuf = _TRAN(N_NOTICE_F22, casterChar->GetName());
					//sprintf(MsgBuf, "你的普通攻击未命中%s<br>", casterChar->GetName());
				}
				else if (hitStats & HITSTATUS_CRICTICAL)
				{
					if ( hitStats & HITSTATUS_ABSORBED )
					{
						Msgbuf = _TRAN(N_NOTICE_F23, casterChar->GetName(),_I2A(damageNum).c_str(), _I2A(damage_abs).c_str());
						//sprintf(MsgBuf, "%s对你造成了%u点致命一击伤害(吸收%u点)<br>", casterChar->GetName(), damageNum, damage_abs);
					}
					else
					{
						Msgbuf = _TRAN(N_NOTICE_F24,casterChar->GetName(),_I2A(damageNum).c_str());
						//sprintf(MsgBuf, "%s对你造成%u点致命一击伤害<br>", casterChar->GetName(), damageNum);
					}
				}
				else if (hitStats & HITSTATUS_ABSORBED)
				{
					Msgbuf = _TRAN(N_NOTICE_F25, casterChar->GetName(),_I2A(damageNum).c_str(), _I2A(damage_abs).c_str());
					//sprintf(MsgBuf, "%s对你造成了%u点伤害(吸收%u点)<br>", casterChar->GetName(), damageNum, damage_abs);
				}
				if (Msgbuf.length())
				{
					AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
				}
			}
			else
			{
				switch (victim_stat)
				{
				case DODGE:
					{
						Msgbuf = _TRAN(N_NOTICE_F26, casterChar->GetName());
						//sprintf(MsgBuf, "你躲闪了%s的攻击<br>", casterChar->GetName());
						break;
					}
				case PARRY:
					{
						Msgbuf = _TRAN(N_NOTICE_F27, casterChar->GetName());
						//sprintf(MsgBuf, "你招架了%s的攻击<br>", casterChar->GetName());
						break;
					}
				case INTERRUPT:
					{
						Msgbuf = _TRAN(N_NOTICE_F28, casterChar->GetName());
						//sprintf(MsgBuf, "你打断了%s的攻击<br>", casterChar->GetName());
						break;
					}
				case BLOCK:
					{
						Msgbuf = _TRAN(N_NOTICE_F29, casterChar->GetName(),_I2A(damageNum).c_str(), _I2A(blocked_damage).c_str());
						//sprintf(MsgBuf, "%s对你造成%u点伤害(格挡%u点)<br>", casterChar->GetName(), damageNum, blocked_damage);
						break;
					}
				case EVADE://避开
					{
						Msgbuf = _TRAN(N_NOTICE_F30, casterChar->GetName());
						//sprintf(MsgBuf, "你避开了%s的攻击<br>", casterChar->GetName());
						break;
					}
				case IMMUNE://免疫
					{
						if (hitStats & HITSTATUS_ABSORBED)
						{
							Msgbuf = _TRAN(N_NOTICE_F31, casterChar->GetName());
							//sprintf(MsgBuf, "你完全吸收了%s的攻击<br>", casterChar->GetName());
						}
						else{
							Msgbuf = _TRAN(N_NOTICE_F32, casterChar->GetName());
							//sprintf(MsgBuf, "你对%s的攻击免疫<br>", casterChar->GetName());
						}
						break;
					}
				case DEFLECT://擦过
					{
						Msgbuf = _TRAN(N_NOTICE_F33, casterChar->GetName(),_I2A(damageNum).c_str());
						//sprintf(MsgBuf, "%s对你造成%u点伤害(偏斜)<br>", casterChar->GetName(), damageNum);
						break;
					}
				}
				if (Msgbuf.length())
				{
					AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
				}
			}
		}
	}
}
void UChat::SpellDmgMsg(ui64 createby_guid, ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 damageNum, 
						ui32 AbsorbedDamage, ui32 ResistedDamage, ui32 PhysicalDamage, ui32 BlockedDamage, uint8 bCri)//使用战斗技能
{
	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)SpellId);
	CCharacter* targerChar = (CCharacter*)ObjectMgr->GetObject(target_guid);
	CCharacter* casterChar = (CCharacter*)ObjectMgr->GetObject(caster_guid);
	if (pSpellTemplate && targerChar && casterChar)
	{
		std::string isCriStr;
		if(bCri)
			isCriStr = _TRAN("(法术暴击)");
		else
			isCriStr = _TRAN("(普通伤害)");
		if (createby_guid == ObjectMgr->GetLocalPlayer()->GetGUID() || casterChar == (CCharacter*)ObjectMgr->GetLocalPlayer())
		{
			MsgBuf[0] = '\0';
			Msgbuf.clear();
			Msgbuf = _TRAN(N_NOTICE_F34,targerChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str(), isCriStr.c_str());
			//sprintf(MsgBuf, "你对%s使用%s造成%u点伤害%s<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str(), damageNum, isCriStr.c_str());
			if (AbsorbedDamage != 0)
			{
				Msgbuf = _TRAN(N_NOTICE_F35,targerChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str(), isCriStr.c_str(),_I2A(AbsorbedDamage).c_str());
				//sprintf(MsgBuf, "你对%s使用%s造成%u点伤害%s(吸收%u点伤害)<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str(), damageNum, isCriStr.c_str(), AbsorbedDamage);
			}else if (ResistedDamage != 0)
			{
				Msgbuf = _TRAN(N_NOTICE_F36,targerChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str(), isCriStr.c_str(),_I2A(ResistedDamage).c_str());
				//sprintf(MsgBuf, "你对%s使用%s造成%u点伤害%s(抵抗%u点伤害)<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str(), damageNum, isCriStr.c_str(), ResistedDamage);
			}else if (PhysicalDamage != 0)
			{
				Msgbuf = _TRAN(N_NOTICE_F37,targerChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str(), isCriStr.c_str(),_I2A(PhysicalDamage).c_str());
				//sprintf(MsgBuf, "你对%s使用%s造成%u点伤害%s(%u点物理伤害)<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str(), damageNum, isCriStr.c_str(), PhysicalDamage);
			}else if (BlockedDamage != 0)
			{
				Msgbuf = _TRAN(N_NOTICE_F38,targerChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str(), isCriStr.c_str(),_I2A(BlockedDamage).c_str());
				//sprintf(MsgBuf, "你对%s使用%s造成%u点伤害%s(格挡%u点伤害)<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str(), damageNum, isCriStr.c_str(), BlockedDamage);
			}
			if (Msgbuf.length())
			{
				AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
			}
		}
		if (targerChar == ObjectMgr->GetLocalPlayer())
		{
			MsgBuf[0] = '\0';
			Msgbuf.clear();
			Msgbuf = _TRAN(N_NOTICE_F39,casterChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str(), isCriStr.c_str());
			//sprintf(MsgBuf, "%s对你使用%s造成%u点伤害%s<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str(), damageNum, isCriStr.c_str());
			if (AbsorbedDamage != 0)
			{
				Msgbuf = _TRAN(N_NOTICE_F40,casterChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str(), isCriStr.c_str(),_I2A(AbsorbedDamage).c_str());
				//sprintf(MsgBuf, "%s对你使用%s造成%u点伤害%s(吸收%u点伤害)<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str(), damageNum, isCriStr.c_str(), AbsorbedDamage);
			}else if (ResistedDamage != 0)
			{
				Msgbuf = _TRAN(N_NOTICE_F41,casterChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str(), isCriStr.c_str(),_I2A(ResistedDamage).c_str());
				//sprintf(MsgBuf, "%s对你使用%s造成%u点伤害%s(抵抗%u点伤害)<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str(), damageNum, isCriStr.c_str(), ResistedDamage);
			}else if (PhysicalDamage != 0)
			{
				Msgbuf = _TRAN(N_NOTICE_F42,casterChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str(), isCriStr.c_str(),_I2A(PhysicalDamage).c_str());
				//sprintf(MsgBuf, "%s对你使用%s造成%u点伤害%s(%u点物理伤害)<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str(), damageNum, isCriStr.c_str(), PhysicalDamage);
			}else if (BlockedDamage != 0)
			{
				Msgbuf = _TRAN(N_NOTICE_F43,casterChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str(), isCriStr.c_str(),_I2A(BlockedDamage).c_str());
				//sprintf(MsgBuf, "%s对你使用%s造成%u点伤害%s(格挡%u点伤害)<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str(), damageNum, isCriStr.c_str(), BlockedDamage);
			}
			if (Msgbuf.length())
			{
				AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
			}
		}
	}
}
void UChat::SpellMissMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 SpellLogType)//技能Miss
{
	MsgBuf[0] = '\0';
	Msgbuf.clear() ;

	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)SpellId);
	CCharacter* targerChar = (CCharacter*)ObjectMgr->GetObject(target_guid);
	CCharacter* casterChar = (CCharacter*)ObjectMgr->GetObject(caster_guid);
	if (pSpellTemplate && targerChar && casterChar)
	{
		if (SpellLogType == SPELL_LOG_NONE)
		{
		}
		if (SpellLogType == SPELL_LOG_MISS)
		{
			if (casterChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F44, pSpellTemplate->GetName().c_str(),  targerChar->GetName());
				//sprintf(MsgBuf, "你释放的%s未命中%s<br>", pSpellTemplate->GetName().c_str(), targerChar->GetName());
			}
			if (targerChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F45, casterChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "%s释放的%s未命中你<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str());
			}
		}
		if (SpellLogType == SPELL_LOG_RESIST)
		{
			if (casterChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F46, pSpellTemplate->GetName().c_str(),  targerChar->GetName());
				//sprintf(MsgBuf, "你释放的%s被%s抵抗了<br>", pSpellTemplate->GetName().c_str(), targerChar->GetName());
			}
			if (targerChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F47, casterChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "%s释放的%s被你抵抗了<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str());
			}
		}
		if (SpellLogType == SPELL_LOG_DODGE)
		{			
			if (casterChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F48, targerChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "%s躲闪了你释放的%s<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str());
			}
			if (targerChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F49, casterChar->GetName(), pSpellTemplate->GetName().c_str());

				//sprintf(MsgBuf, "你躲闪了%s释放的%s<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str());
			}
		}
		if (SpellLogType == SPELL_LOG_PARRY)
		{
			if (casterChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F50, targerChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "%s招架了你释放的%s<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str());
			}
			if (targerChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F51, casterChar->GetName(), pSpellTemplate->GetName().c_str());

				//sprintf(MsgBuf, "你招架了%s释放的%s<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str());
			}
		}
		if (SpellLogType == SPELL_LOG_BLOCK)
		{
			if (casterChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F52, targerChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "%s格挡了你释放的%s<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str());
			}
			if (targerChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F53, casterChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "你格挡了%s释放的%s<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str());
			}
		}
		if (SpellLogType == SPELL_LOG_EVADE)
		{
		}
		if (SpellLogType == SPELL_LOG_IMMUNE)
		{			
			if (casterChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F54, targerChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "%s免疫了你释放的%s<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str());
			}
			if (targerChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F55, casterChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "你免疫了%s释放的%s<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str());
			}
		}
		if (SpellLogType == SPELL_LOG_IMMUNE2)
		{
			if (casterChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F56, targerChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "%s免疫了你释放的%s<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str());
			}
			if (targerChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F57, casterChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "你免疫了%s释放的%s<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str());
			}
		}
		if (SpellLogType == SPELL_LOG_DEFLECT)
		{
			if (casterChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F58, targerChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "%s对你释放%s(偏斜)<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str());
			}
			if (targerChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F59, casterChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "你对%s释放%s(偏斜)<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str());
			}
		}
		if (SpellLogType == SPELL_LOG_ABSORB)
		{
			if (casterChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F60, targerChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "%s吸收了你释放的%s所造成的伤害<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str());
			}
			if (targerChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F61, casterChar->GetName(), pSpellTemplate->GetName().c_str());
				//sprintf(MsgBuf, "你吸收了%s释放的%s所造成的伤害<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str());
			}
		}
		if (SpellLogType == SPELL_LOG_REFLECT)
		{
		}
		if (Msgbuf.length())
		{
			AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
		}
	}
}

void UChat::SpellHeathMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 HeathNum)//HP治疗
{
	MsgBuf[0] = '\0';

	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)SpellId);
	CCharacter* targerChar = (CCharacter*)ObjectMgr->GetObject(target_guid);
	CCharacter* casterChar = (CCharacter*)ObjectMgr->GetObject(caster_guid);
	if (pSpellTemplate && targerChar && casterChar)
	{
		if (casterChar == ObjectMgr->GetLocalPlayer())
		{
			if (targerChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F62,pSpellTemplate->GetName().c_str(), _I2A(HeathNum).c_str());
				//sprintf(MsgBuf, "你对自己释放%s恢复%u的生命值<br>", pSpellTemplate->GetName().c_str(), HeathNum);
				AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
				return;
			}
			else
			{
				Msgbuf = _TRAN(N_NOTICE_F63,targerChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(HeathNum).c_str());
				//sprintf(MsgBuf, "你对%s释放%s恢复%u的生命值<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str(), HeathNum);
				AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
				return;
			}
		}
		if (targerChar == ObjectMgr->GetLocalPlayer())
		{
			if (casterChar != ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F64,casterChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(HeathNum).c_str());
				//sprintf(MsgBuf, "%s对你了释放了%s恢复你%u的生命值<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str(), HeathNum);
				AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
				return;
			}
		}
	}
}
void UChat::SpellManaMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 ManaNum)//MP治疗
{
	MsgBuf[0] = '\0';

	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)SpellId);
	CCharacter* targerChar = (CCharacter*)ObjectMgr->GetObject(target_guid);
	CCharacter* casterChar = (CCharacter*)ObjectMgr->GetObject(caster_guid);
	if (pSpellTemplate && targerChar && casterChar)
	{
		if (casterChar == ObjectMgr->GetLocalPlayer())
		{
			if (targerChar == ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F65, pSpellTemplate->GetName().c_str(), _I2A(ManaNum).c_str());
				//sprintf(MsgBuf, "你对自己释放%s给自己恢复%u的法力值<br>", pSpellTemplate->GetName().c_str(), ManaNum);
				AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
				return;
			}
			else
			{
				Msgbuf = _TRAN(N_NOTICE_F66, targerChar->GetName(), pSpellTemplate->GetName().c_str(), _I2A(ManaNum).c_str());
				//sprintf(MsgBuf, "你对%s释放%s给恢复其%u的法力值<br>", targerChar->GetName(), pSpellTemplate->GetName().c_str(), ManaNum);
				AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
				return;
			}
		}
		if (targerChar == ObjectMgr->GetLocalPlayer())
		{
			if (casterChar != ObjectMgr->GetLocalPlayer())
			{
				Msgbuf = _TRAN(N_NOTICE_F67, casterChar->GetName(), pSpellTemplate->GetName().c_str(), _I2A(ManaNum).c_str());
				//sprintf(MsgBuf, "%s对你了释放了%s恢复你%u的法力值<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str(), ManaNum);
				AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
				return;
			}
		}
	}
}
void UChat::AuraDmgMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 damageNum, ui32 abs_dmg, ui32 resisted_damage)//debuff伤害
{
	MsgBuf[0] = '\0';

	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)SpellId);
	CCharacter* targerChar = (CCharacter*)ObjectMgr->GetObject(target_guid);
	CCharacter* casterChar = (CCharacter*)ObjectMgr->GetObject(caster_guid);
	if (pSpellTemplate && targerChar && casterChar)
	{
		if (casterChar == ObjectMgr->GetLocalPlayer())
		{
			Msgbuf = _TRAN(N_NOTICE_F68, pSpellTemplate->GetName().c_str(), targerChar->GetName(), _I2A(damageNum).c_str());

			//sprintf(MsgBuf, "你的%s对%s造成%u的伤害<br>", pSpellTemplate->GetName().c_str(), targerChar->GetName(), damageNum);
			AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
			return;
		}
		if (targerChar == ObjectMgr->GetLocalPlayer())
		{
			Msgbuf = _TRAN(N_NOTICE_F69,  casterChar->GetName(),pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str());
			//sprintf(MsgBuf, "%s的%s对你造成%u的伤害<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str(),damageNum);
			AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
			return;
		}
	}
}

void UChat::AuraHealMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 damageNum, ui32 abs_dmg, ui32 resisted_damage)//debuff伤害
{
	MsgBuf[0] = '\0';
	Msgbuf.clear();
	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)SpellId);
	CCharacter* targerChar = (CCharacter*)ObjectMgr->GetObject(target_guid);
	CCharacter* casterChar = (CCharacter*)ObjectMgr->GetObject(caster_guid);
	if (pSpellTemplate && targerChar && casterChar)
	{
		if (casterChar == ObjectMgr->GetLocalPlayer())
		{
			Msgbuf = _TRAN(N_NOTICE_F70,pSpellTemplate->GetName().c_str(),  targerChar->GetName(), _I2A(damageNum).c_str());
			//sprintf(MsgBuf, "你的%s对%s恢复了%u的生命<br>", pSpellTemplate->GetName().c_str(), targerChar->GetName(), damageNum);
			AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
			return;
		}
		if (targerChar == ObjectMgr->GetLocalPlayer())
		{
			Msgbuf = _TRAN(N_NOTICE_F71,casterChar->GetName(), pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str());
			//sprintf(MsgBuf, "%s的%s对你恢复了%u的生命<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str(),damageNum);
			AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
			return;
		}
	}
}

void UChat::AuraHealMpMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 damageNum, ui32 abs_dmg, ui32 resisted_damage)//deMsgBuff伤害
{
	MsgBuf[0] = '\0';

	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)SpellId);
	CCharacter* targerChar = (CCharacter*)ObjectMgr->GetObject(target_guid);
	CCharacter* casterChar = (CCharacter*)ObjectMgr->GetObject(caster_guid);
	if (pSpellTemplate && targerChar && casterChar)
	{
		if (casterChar == ObjectMgr->GetLocalPlayer())
		{
			Msgbuf = _TRAN(N_NOTICE_F72,pSpellTemplate->GetName().c_str(),  targerChar->GetName(), _I2A(damageNum).c_str());
			//sprintf(MsgBuf, "你的%s对%s恢复了%u的魔法<br>", pSpellTemplate->GetName().c_str(), targerChar->GetName(), damageNum);
			AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
			return;
		}
		if (targerChar == ObjectMgr->GetLocalPlayer())
		{
			Msgbuf = _TRAN(N_NOTICE_F73,casterChar->GetName(), pSpellTemplate->GetName().c_str(), _I2A(damageNum).c_str());
			//sprintf(MsgBuf, "%s的%s对你恢复了%u的魔法<br>", casterChar->GetName(), pSpellTemplate->GetName().c_str(),damageNum);
			AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
			return;
		}
	}
}

void UChat::LocalDmgMsg(ui64 target_guid, ui32 damageNum)//战斗所受伤害
{

}
void UChat::DeathMsg(ui64 target_guid, ui64 killer)//死亡
{
	CCharacter* Char = (CCharacter*)ObjectMgr->GetObject(target_guid);
	CCharacter* Killer = (CCharacter*)ObjectMgr->GetObject(killer);
	if (Char && Killer/* && Char != ObjectMgr->GetLocalPlayer() && Killer == ObjectMgr->GetLocalPlayer()*/)
	{
		Msgbuf = _TRAN(N_NOTICE_F74,Killer->GetName(), Char->GetName());
		//sprintf(MsgBuf, "%s消灭了了%s<br>", Killer->GetName(), Char->GetName());
		AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
		Msgbuf = _TRAN(N_NOTICE_F75, Char->GetName());
		//sprintf(MsgBuf, "%s已经被消灭了<br>", Char->GetName());
		AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
	}
}
void UChat::ExpGotMsg(ui32 ExpNum)//战斗获得经验
{
	//sprintf(MsgBuf, "您获得了%u点经验<br>", ExpNum);
	Msgbuf = _TRAN(N_NOTICE_F76,_I2A(ExpNum).c_str());
	AppendChatMsg(CHAT_MSG_CLIENTSYSTEM,Msgbuf.c_str());
}
void UChat::ItemGotMsg(ui32 itemId, ui32 itemNum)//战斗获得物品
{
	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(itemId);
	if (ItemInfo)
	{
		Msgbuf = _TRAN(N_NOTICE_F77,GetQualityColorStr(ItemInfo->Quality).c_str(), _I2A(itemId).c_str(), ItemInfo->C_name.c_str(), _I2A(itemNum).c_str());
		//sprintf(MsgBuf, "获得<linkcolor:%s><a:#Item_%u>[%s]</a>*%u<br>", GetQualityColorStr(ItemInfo->Quality).c_str(), itemId, ItemInfo->C_name.c_str(), itemNum);
		AppendChatMsg(CHAT_MSG_CLIENTSYSTEM, Msgbuf.c_str());
	}
}
void UChat::LevelUpMsg(ui32 level)//升级信息
{
	Msgbuf = _TRAN(N_NOTICE_F78,_I2A(level).c_str());
	AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
}

void UChat::HonorGotMsg(int HonorValue)
{
	if (HonorValue > 0)
	{
		//sprintf(MsgBuf, "获得%d的荣誉值<br>", HonorValue);
		Msgbuf = _TRAN(N_NOTICE_F79,_I2A(HonorValue).c_str());
	}
	else
	{
		//sprintf(MsgBuf, "失去%d的荣誉值<br>", 0-HonorValue);
		Msgbuf = _TRAN(N_NOTICE_F80,_I2A(0 - HonorValue).c_str());
	}
	AppendChatMsg(CHAT_MSG_CLIENTSYSTEM, Msgbuf.c_str());
}

void OnCommandGuildDisband(std::vector<std::string>& text)
{
	PacketBuilder->SendGuildDisBand();
}

void OnCommandGuildInvate(std::vector<std::string>& text)
{
	if (text.size())
	{
		SocialitySys->GetGuildSysPtr()->SendMsgInviteMember(text[0].c_str());
	}
}


void OnShowPosition(std::vector<std::string>& text)
{
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();

	if ( pLocal )
	{
		pLocal->SetShowPositon();
	}
}

void OnForceFly(std::vector<std::string>& text)
{
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();

	if ( pLocal )
	{
		pLocal->SetForceFly();
	}
}


void OnCommandCallPlug(std::vector<std::string>& text)
{
	if(text.size())
	{
		if (text[0].compare("groupshow") == 0)
		{
			UPlug_GroupShow* NewCtrl = NULL;
			if (NewCtrl == NULL && UInGame::Get()->GetChildByID(FRAME_IG_PLUG_GROUPSHOW) == NULL)
			{
				NewCtrl = (UPlug_GroupShow*)UPlug_GroupShow::CreateObject();
				if (NewCtrl)
				{
					NewCtrl->SetField("style", "Plug_GroupShow");
					NewCtrl->SetField("position", "600 350");
					NewCtrl->SetField("extent", "66 66");
					NewCtrl->SetField("visible", "1");

					UInGame::Get()->AddChild(NewCtrl);
					NewCtrl->SetId(FRAME_IG_PLUG_GROUPSHOW);
				}
			}
			else
			{
				NewCtrl = UDynamicCast(UPlug_GroupShow, UInGame::Get()->GetChildByID(FRAME_IG_PLUG_GROUPSHOW));
			}
			if (NewCtrl)
			{
				NewCtrl->UpdateMember();
				if (text.size() >= 2 )
				{
					if( text[1].compare("open") == 0 )
						NewCtrl->SetVisible(TRUE);
					if( text[1].compare("close") == 0 )
						NewCtrl->SetVisible(FALSE);
				}
			}
		}else if (text[0].compare("frame") == 0)
		{
			new UPlug_WatchTarget_Mgr;
		}
		else if (text[0].compare("count") == 0)
		{
			UPlug_DPSCount_Dialog* NewCtrl = NULL;
			if (NewCtrl == NULL && UInGame::Get()->GetChildByID(FRAME_IG_PLUG_DPSCOUNT) == NULL)
			{
				NewCtrl = (UPlug_DPSCount_Dialog*)UControl::sm_System->CreateDialogFromFile("UPlug\\Plug_ReCount.udg");
				if (NewCtrl)
				{
					UInGame::Get()->AddChild(NewCtrl);
					NewCtrl->SetId(FRAME_IG_PLUG_DPSCOUNT);
				}
			}
			else
			{
				NewCtrl = UDynamicCast(UPlug_DPSCount_Dialog, UInGame::Get()->GetChildByID(FRAME_IG_PLUG_DPSCOUNT));
			}
			if (NewCtrl)
			{
				NewCtrl->Update();

				if (text.size() >= 2 )
				{
					if( text[1].compare("open") == 0 )
						NewCtrl->SetVisible(TRUE);
					if( text[1].compare("close") == 0 )
						NewCtrl->SetVisible(FALSE);
				}
			}
		}else if (text[0].compare("lvlup") == 0)
		{
			PacketBuilder->SendGuildLevelUp();
		}
	}
}

void OnCommandRoll(std::vector<std::string>& text)
{
	if (text.size())
	{
		ui32 min = 1, max;
		if (text.size() >= 2)
		{
			min = atoi(text[0].c_str());
			max = atoi(text[1].c_str());
		}
		else
			min = max = atoi(text[0].c_str());
		max = max ? max : 1;
		PacketBuilder->SendRandomRoll(min, max);
	}
	else
	{
		PacketBuilder->SendRandomRoll(1, 100);
	}
}

void OnGMModifyHp(std::vector<std::string>& text)
{
	if (text.size())
	{
		char buf[256];
		ui32 min = 1, max;
		if (text.size() >= 2)
		{
			min = atoi(text[0].c_str());
			max = atoi(text[1].c_str());
		}
		else
			min = max = atoi(text[0].c_str());
		sprintf(buf, ".modify hp %u %u", min, max);
		ChatSystem->SendChatMsg(CHAT_MSG_SAY, buf);
	}
	else
	{
		ChatSystem->SendChatMsg(CHAT_MSG_SAY,".modify hp 999999999 999999999");
	}
}

void OnGMModifyMP(std::vector<std::string>& text)
{
	if (text.size())
	{
		char buf[256];
		ui32 min = 1, max;
		if (text.size() >= 2)
		{
			min = atoi(text[0].c_str());
			max = atoi(text[1].c_str());
		}
		else
			min = max = atoi(text[0].c_str());
		sprintf(buf, ".modify mana %u %u", min, max);
		ChatSystem->SendChatMsg(CHAT_MSG_SAY, buf);
	}
	else
	{
		ChatSystem->SendChatMsg(CHAT_MSG_SAY,".modify mana 999999999 999999999");
	}
}

void OnGMModifySpeed(std::vector<std::string>& text)
{
	if (text.size())
	{
		char buf[256];
		ui32 max = atoi(text[0].c_str());
		sprintf(buf, ".modify speed %u", max);
		ChatSystem->SendChatMsg(CHAT_MSG_SAY, buf);
	}
	else
	{
		ChatSystem->SendChatMsg(CHAT_MSG_SAY,".modify speed 20");
	}
}


void OnGMModifyDamage(std::vector<std::string>& text)
{
	if (text.size())
	{
		char buf[256];
		ui32 min = 1, max;
		if (text.size() >= 2)
		{
			min = atoi(text[0].c_str());
			max = atoi(text[1].c_str());
		}
		else
			min = max = atoi(text[0].c_str());
		sprintf(buf, ".modify damage %u %u", min, max);
		ChatSystem->SendChatMsg(CHAT_MSG_SAY, buf);
	}
	else
	{
		ChatSystem->SendChatMsg(CHAT_MSG_SAY,".modify damage 99999999 99999999");
	}
}

void OnChatInpHelp(std::vector<std::string>& text)
{
	ClientSystemNotify(_TRAN("聊天框命令帮助 /help"));
	ClientSystemNotify(_TRAN("聊天相关"));
	ClientSystemNotify(_TRAN("密语：/s [玩家姓名]"));
	ClientSystemNotify(_TRAN("区域：/q"));
	ClientSystemNotify(_TRAN("小队：/x"));
	ClientSystemNotify(_TRAN("团队：/r"));
	ClientSystemNotify(_TRAN("部族：/b"));
	ClientSystemNotify(_TRAN("交易：/j"));

	ClientSystemNotify(_TRAN("部族相关"));
	ClientSystemNotify(_TRAN("解散部族：/gdisband"));
	ClientSystemNotify(_TRAN("部族收人：/收人 [玩家姓名]"));

	ClientSystemNotify(_TRAN("其它"));
	ClientSystemNotify(_TRAN("掷骰子：/roll [最小值] [最大值]"));
	if (text.size() >= 2 && text[0].compare("allCommand") != -1 && text[1].compare("GM") != -1)
	{
		ClientSystemNotify(_TRAN("GM命令"));
		ClientSystemNotify(_TRAN("调整血量：/hp [最小值] [最大值]"));
		ClientSystemNotify(_TRAN("调整魔法值：/mp [最小值] [最大值]"));
		ClientSystemNotify(_TRAN("调整速度：/speed [速度值]"));
		ClientSystemNotify(_TRAN("调整伤害值：/damage [最小值] [最大值]"));
		ClientSystemNotify(_TRAN("重新读取技能数据库：/reloadspell"));
	}
}

void OnReLoadSpell(std::vector<std::string>& text)
{	
	SYState()->SkillMgr->Destroy();
	ChatSystem->AppendChatMsg(CHAT_MSG_SYSTEM, "reload spell scripts<br>");
	SYState()->SkillMgr->Initialize();
	ChatSystem->AppendChatMsg(CHAT_MSG_SYSTEM, "done<br>");
}

void OnQueryLADDER(std::vector<std::string>& text)
{
	PacketBuilder->SendLadderReq();
}

void OnAdult(std::vector<std::string>& text)
{
	bool bOpenAdult = false;
	if(text.size())
	{
		if(text[0].compare("0") == 0)
			bOpenAdult = false;
		if(text[0].compare("1") == 0)
			bOpenAdult = true;
	}
	SystemSetup->SetField("ChatFileter", bOpenAdult);
	SystemSetup->SaveFile();
	SetFilterChatMsg(bOpenAdult);
}
void OnShowShop(std::vector<std::string>& text)
{
	UControl* pCtrl = UInGame::Get()->GetFrame(FRAME_IG_MINIMAP);
	if(text.size() && pCtrl->GetChildByID(15))
	{
		if(text[0].compare("show") == 0)
			pCtrl->GetChildByID(15)->SetVisible(TRUE);
		if(text[0].compare("close") == 0)
			pCtrl->GetChildByID(15)->SetVisible(FALSE);
	}
}

void OnAFK(std::vector<std::string>& text)
{
	if (text.size())
	{
		PacketBuilder->SendChatReqMsg(CHAT_MSG_AFK, text[0].c_str());
	}
	else
	{
		PacketBuilder->SendChatReqMsg(CHAT_MSG_AFK, " ");
	}
}

void OnDND(std::vector<std::string>& text)
{
	if (text.size())
	{
		PacketBuilder->SendChatReqMsg(CHAT_MSG_DND, text[0].c_str());
	}
	else
	{
		PacketBuilder->SendChatReqMsg(CHAT_MSG_DND, " ");
	}
}

void UChat::AddItemMsgInPagckage(UINT pos, int type)
{
	CStorage* pkStorage = NULL;
	CPlayer* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		if (type == UItemSystem::ICT_BAG)
		{
			pkStorage = pkLocal->GetItemContainer();
		}else if(type == UItemSystem::ICT_BANK)
		{
			pkStorage = pkLocal->GetBankContainer();
		}
		if (!pkStorage)
			return;
		CItemSlot* pkitem = (CItemSlot*)pkStorage->GetSlot( pos );
		if (pkitem && pkitem->GetGUID())
		{
			ChatSystem->AddItemMsg(pkitem->GetCode(), pkitem->GetGUID());
		}
	}
}
void UChat::FittingEquipmentInPagckage(UINT pos, int type)
{
	CStorage* pkStorage = NULL;
	CPlayer* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		if (type == UItemSystem::ICT_BAG)
		{
			pkStorage = pkLocal->GetItemContainer();
		}else if(type == UItemSystem::ICT_BANK)
		{
			pkStorage = pkLocal->GetBankContainer();
		}
		if (!pkStorage)
			return;
		CItemSlot* pkitem = (CItemSlot*)pkStorage->GetSlot( pos );
		if (pkitem && pkitem->GetGUID())
		{
			UFittingRoom* pFittingRoom = INGAMEGETFRAME(UFittingRoom, FRAME_IG_FITTINGROOMDLG);
			if(!pFittingRoom)
				return;
			pFittingRoom->FittingEquipment(pkitem->GetCode(), pkitem->GetGUID());
		}
	}
}

void UChat::OnFittingEquipment(std::string Item)
{
	ui32 ItemEntry = 0;
	ItemExtraData TempData;
	int PropertyNum = GetItemProperty(Item, ItemEntry, TempData);

	UFittingRoom* pFittingRoom = INGAMEGETFRAME(UFittingRoom, FRAME_IG_FITTINGROOMDLG);
	if(!pFittingRoom)
		return;
	//这里是临时变量 传入的指针
	pFittingRoom->FittingEquipment(ItemEntry, &TempData);
}

void UChat::OnClickMsgItem(std::string Item)
{
	//1位是ID
	//后面是附加属性
	//ID_HOLECNT_HOLE1_HOLE2_HOLE3_JINGLIANLEVEL_XIANGQIANDENGJI_PRO1_PRO2
	ui32 ItemEntry = 0;
	ItemExtraData TempData;
	int PropertyNum = GetItemProperty(Item, ItemEntry, TempData);

	TipSystem->ShowTip(ItemEntry, UControl::sm_System->GetCursorPos(), TempData);
}

void UChat::OnClickMsgSpell(std::string Item)
{
	int pos = Item.find("_");
	ui32 guid = atoi(Item.substr(0, pos).c_str());
	std::string sExtraData = Item.substr(pos + 1, Item.size());

	ui32 Spell_ID = atoi(sExtraData.c_str());

	TipSystem->ShowTip(TIP_TYPE_SPELL, Spell_ID, UControl::sm_System->GetCursorPos(), guid);
}

void UChat::AddItemMsg(std::string str)
{
	ui32 ItemEntry = 0;
	ItemExtraData TempData;
	int PropertyNum = GetItemProperty(str, ItemEntry, TempData);

	AddItemMsg(ItemEntry, &TempData);
}

void UChat::AddItemMsg( ui32 Itementry, ui64 ItemGuid /*= 0*/ )
{
	ItemPrototype_Client* pItem = ItemMgr->GetItemPropertyFromDataBase(Itementry);
	if(!pItem)
		return;
	//ID_HOLECNT_HOLE1_HOLE2_HOLE3_JINGLIANLEVEL_XIANGQIANDENGJI_PRO1_PRO2
	SYItem* Item = (SYItem *)ObjectMgr->GetObject(ItemGuid);
	if (Item)
	{
		ItemExtraData NewItemExtraData;
		NewItemExtraData.jinglian_level = Item->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
		for( int i = 0; i < 6; ++i )
			NewItemExtraData.enchants[i] =  Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + i * 3 );
		AddItemMsg(Itementry, &NewItemExtraData);
		return;
	}
	AddItemMsg(Itementry, (ItemExtraData*)NULL);
}



void UChat::AddItemMsg(ui32 Itementry, ItemExtraData* sItemData)
{
	UColor Linkcolor;
	string colorstr;
	ItemPrototype_Client* pItem = ItemMgr->GetItemPropertyFromDataBase(Itementry);
	std::string ItemName = pItem->C_name;

	char buf[256];
	sprintf(buf,"%u_",Itementry);
	std::string EnChant = buf;
	long long mask = 0;
	//string as_s;
	if (sItemData)
	{
		uint32 tempValue = 0;
		tempValue = sItemData->jinglian_level;
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[0];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[1];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[2];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[3];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[4];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[5];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		//for ( unsigned int ui = 0; ui < 8; ui++ ) {
		//	uint32 propid = ((uint8*)sItemData->property)[ui];
		//	if (propid != 0)
		//	{
		//		int i = GetAS(propid);
		//		long long flag = (1 << propid);
		//		if (i != -1)
		//		{
		//			if (!BOOL(mask&flag))
		//			{
		//				if (ui != 0)
		//					as_s += ' ';
		//				as_s += as[i].str;
		//				mask |= flag;
		//			}
		//		}
		//		else
		//			as_s += "未知属性";
		//	}
		//}
	}
	//if (!as_s.empty())
	//{
		//ItemName = as_s + "的" + ItemName;
	//}
	std::string linkcolor = "<linkcolor:" + GetQualityColorStr(pItem->Quality) + ">";
	std::string ItemMess = linkcolor + "<a:#Item_" + EnChant + ">[" + ItemName  + "]</a>";
	if (m_ChatBox[CHATBOX_PROPS]->IsVisible())
	{
		UChatPropsMsg* pChatProp = UDynamicCast(UChatPropsMsg, m_ChatBox[CHATBOX_PROPS]);
		pChatProp->AppChatMsg(ItemMess.c_str());
	}
	else
	{
		ChatSystem->ShowInputBox();
		AppInputText(ItemMess.c_str());
	}
}

void UChat::AddSpellMsg( ui32 SpellId )
{
	UColor Linkcolor;
	string colorstr;
	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)SpellId);
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pSpellTemplate && pkLocal)
	{
		ui32 id = ui32(pkLocal->GetGUID());
		sprintf(MsgBuf, "<linkcolor:3366FFFF><a:#SPEL_%u_%u>[", id, SpellId);
		colorstr = MsgBuf + pSpellTemplate->GetName() + "]</a>";
		if (m_ChatBox[CHATBOX_PROPS]->IsVisible())
		{
			UChatPropsMsg* pChatProp = UDynamicCast(UChatPropsMsg, m_ChatBox[CHATBOX_PROPS]);
			pChatProp->AppChatMsg(colorstr.c_str());
		}
		else
		{
			ChatSystem->ShowInputBox();
			AppInputText(colorstr.c_str());
		}
		//AppInputText(colorstr.c_str());
		//ChatSystem->ShowInputBox();
	}
}

void UChat::SpellLearnMsg(ui32 SpellId)
{
	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)SpellId);
	if(pSpellTemplate)
	{
		Msgbuf = _TRAN(N_NOTICE_F81,  pSpellTemplate->GetName().c_str(), _I2A(pSpellTemplate->GetSpellLev()).c_str());
		//sprintf(MsgBuf, "您已学习了%s%u级<br>", pSpellTemplate->GetName().c_str(), pSpellTemplate->GetSpellLev());	
		AppendChatMsg(CHAT_MSG_SYSTEM, Msgbuf.c_str());
	}
}

void UChat::SpellCCResult(ui64 target_guid, ui64 caster_guid, ui32 SpellId, uint8 effect, uint8 result)
{
	CCharacter* pAtacker = (CCharacter*)ObjectMgr->GetObject(caster_guid);
	CCharacter* pAtkTarget = (CCharacter*)ObjectMgr->GetObject(target_guid);
	SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate((SpellID)SpellId);
	if(pSpellTemplate && pAtacker && pAtkTarget)
	{
		char* Effect[] = {"昏迷", "定身", "恐惧", "减速", "伤害","沉默","嘲讽","睡眠","变形", "击退", "牵引", "", ""};
		char* Result[] = {"免疫了", "抵抗了"};
		if(pAtacker == ObjectMgr->GetLocalPlayer())
		{
			Msgbuf = _TRAN(N_NOTICE_F82,pAtkTarget->GetName(), _TRAN(Result[result]), pSpellTemplate->GetName().c_str(), _TRAN(Effect[effect]));
			//sprintf(MsgBuf, "%s%s你施放的%s造成的%s效果<br>" ,pAtkTarget->GetName(), _TRAN(Result[result]), pSpellTemplate->GetName().c_str(), _TRAN(Effect[effect]));
		}
		if(pAtkTarget == ObjectMgr->GetLocalPlayer())
		{
			Msgbuf = _TRAN(N_NOTICE_F83,_TRAN(Result[result]), pAtacker->GetName(), pSpellTemplate->GetName().c_str(), _TRAN(Effect[effect]));
			//sprintf(MsgBuf, "你%s%s施放的%s造成的%s效果<br>" , _TRAN(Result[result]), pAtacker->GetName(), pSpellTemplate->GetName().c_str(), _TRAN(Effect[effect]));
		}
		AppendChatMsg(CHAT_MSG_BATTLE, Msgbuf.c_str());
	}
}

void UChat::AddFace(const char* FaceImage) //添加表情
{
	if (!m_Hide)
	{
		AppInputText(FaceImage);
	}
}
void UChat::AddQuestMsg( ui32 QuestEntry )
{
	UColor Linkcolor;
	string colorstr;
	CQuestDB::stQuest* QuestInfo = g_pkQusetInfo->GetQuestInfo(QuestEntry);
	if (QuestInfo)
	{
		char buf[1024];
		sprintf(buf, "<linkcolor:3366FFFF><a:#QUES_%u>[", QuestEntry);
		colorstr = buf + QuestInfo->strTitle + "]</a>";
		if (m_ChatBox[CHATBOX_PROPS]->IsVisible())
		{
			UChatPropsMsg* pChatProp = UDynamicCast(UChatPropsMsg, m_ChatBox[CHATBOX_PROPS]);
			pChatProp->AppChatMsg(colorstr.c_str());
		}
		else
		{
			ChatSystem->ShowInputBox();
			AppInputText(colorstr.c_str());
		}
	}
}

UChat::MsgStore* UChat::GetMsgStore(int iStore)
{
	if (iStore < 0 || iStore >= CHAT_TABLE_NUM)
		return NULL;
	return &m_MsgStore[iStore];
}
void UChat::SetTableModify(int iStore, int MsgType, bool vis)
{
	if (vis)
	{
		m_MsgStore[iStore].VisModify |= MsgType;

	}
	else
	{
		m_MsgStore[iStore].VisModify &= ~MsgType;
	}
}
void UChat::SetTableModify(int MsgType, bool vis)
{
	SetTableModify(m_visStore, MsgType, vis);
	SystemSetup->SaveChatSetting();
}