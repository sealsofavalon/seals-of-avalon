//-----------------------------------------------------------------------------------
//聊天系统中对外接口和数据交互，所有的对外交互和数据都放在这里
//这样对于每个聊天系统的子模块都是单独自包含的，对内交互也是调用这里的接口
//-----------------------------------------------------------------------------------
#ifndef UCHAT_H
#define UCHAT_H
#include "Singleton.h"

#define ChatSystem UChat::GetSingleton()
class UChat;

//typedef void(UChat::*Funptr)(std::string);
//#define RegistCommand(Command, function)\
//	ChatSystem->m_CommandMap.insert(CommandMap::value_type(Command, function));

typedef void(*ChatCallBack)(std::vector<std::string>&);
typedef std::map<std::string, ChatCallBack> CommandMap;
#define RegistChatCallBack(Name,func) static ChatCallBackFunc __##func(#Name, func)

struct ChatCallBackFunc
{
	ChatCallBackFunc(const char* pName, ChatCallBack pCB);
	ChatCallBack pCB;
	const char* pszName;
	ChatCallBackFunc* pNext;
};

enum
{
	GetNewQuest = 0,
	CanFinishQuest,
	FinishQuest,
	DropQuest,
	FaildQuest,
};
enum ChatDialog
{
	CHATBOX_INPUTMSG,
	CHATBOX_CHANNEL,
	CHATBOX_MSGSHOW,
	CHATBOX_BUTTON,
	CHATBOX_HIDECHATBUTTON,
	CHATBOX_FACEIMGE,
	CHATBOX_PROPS,
	CHATBOX_DALABA,
	CHATBOX_CHATTABLE,
	CHATBOX_MAX
};
enum Scrollway
{
	WAY_TOP,
	WAY_BOTTOM,
	WAY_UP,
	WAY_DOWN
};
//这里要解释一下，频道在服务器内是双层结构
//当收到CHAT_MSG_CHANNEL的时候判断在这个结构下的频道号，其余的CHAT_MSG_消息都是称为"类型"
//这里我用频道枚举来作为存储的索引，所以把频道消息也归到CHAT_MSG_行列中来
//相当于把这两层结构归为一层来实现，这样对于CHAT_MSG_PRIVATE（自创频道）就有着一些问题，
//由于目前一层结构只能储存一个信息，是否可以考虑用一个向量来储存？
enum ChatMsgEx//转换两层结构为一层，以及自己实现的一些类型
{
	CHAT_MSG_PICKITEM = CHAT_MSG_RESTRICTED + 1,
	CHAT_MSG_BATTLE,
	CHAT_MSG_GUILDINFO,
	CHAT_MSG_PRIVATE,
	CHAT_MSG_CITY,
	CHAT_MSG_TRADE,
	CHAT_MSG_LFG,
	CHAT_MSG_CLIENTSYSTEM,
	CHAT_MSG_MAX
};
enum
{
	CHAT_TABLE_ALL,
	CHAT_TABLE_PARTY,
	CHAT_TABLE_GUILD,
	CHAT_TABLE_WHISPER,
	CHAT_TABLE_AREA,
	CHAT_TABLE_BATTLE,
	CHAT_TABLE_NUM,
};
enum 
{
	MSG_SAY = 1,
	MSG_PARTY = 2,
	MSG_GUILD = 4,
	MSG_WHISPER = 8,
	MSG_AREA = 16,
	MSG_BATTLE = 32,
	MSG_SYSTEM = 64,
	MSG_CLIENTSYSTEM = 128,
	MSG_OTHER = 256,
};

class UChat : public Singleton<UChat>
{
	SINGLETON(UChat);
public:
	struct  ChannelData
	{
		ChannelData()
		{
			MsgType =  0;
			IsShildChannel = FALSE;
			ChannelName = "";
			ChannelColor = "";
			ChannelHead = "";
			CallCommand = "";
			ChannelCurTime = 0.f;
			ChannelWaitTime = 0.f;
		}
		int MsgType;//频道类型
		BOOL IsShildChannel;//是否被屏蔽
		string ChannelName;//频道名称
		string ChannelColor;//频道颜色
		string ChannelFont;//频道字号字体标签
		string ChannelHead;//频道在输入框中的头
		const char * CallCommand;//玩家转换频道的快捷命令
		float ChannelCurTime;
		float ChannelWaitTime; //刷屏时间控制
	};
	struct PersonData
	{
		PersonData():guid(0){};
		std::string CreateName;
		ui64 guid;
	};

	struct MsgStore
	{
		MsgStore()
		{
			VisModify = 0;
		}
		inline void Clear()
		{
			Msg.Clear();
			MsgIndex.clear();
		}
		inline bool IsVis(int flag)
		{
			if (VisModify & flag)
			{
				return true;
			}
			return false;
		}
		UStringW Msg;
		std::deque<int> MsgIndex;
		int VisModify;
	};

public:	
	void Init();
public:
	void ShowMsgBox();
	void HideMsgBox();

	BOOL CreateChat(UControl * Frame);

	UControl * GetChatBox(int index);
	int GetCurrentChannel();
	PersonData * GetPersonData();
	ChannelData * GetChannelData(int msgType);

	bool GetAreaChannel(std::string& str);

	bool mbWaitMap;
	void SetWaitMap();
	void WaitMapLoading();

	BOOL IsChannelShield(int msgtype);

	void SetCurrentChannel(int channel);
	void SetChannelShield(int msgtype,BOOL i = TRUE);
	void SetPTalkPerson(const char * createname , ui64 guid = 0);
	void SetPChannelName(string channelname);
	void SetScrollText(int way);
	void AppInputText(const char* Text);

	void ParseCommand(const std::string &comm,const std::string &Text);

	void CallRMD(BOOL visible,const char * text = NULL);
	//TODO NETWORK
	void SendChannelJoin(const char * channelname,uint32 dbc_id = 0);
	void SendChannelLeave(const char * channelname);

	void ParseFaceBuffer(std::string &str);
	void SendChatMsg(int msgtype,const std::string & str);
	void ChannelNotify(ui64 guid,ui8 notifyflag,string name,ui8 oldmemberflag,ui8 memberflag);

	void SendQueryPlayer(std::string& Name);
	void ParseQueryPlayer(std::vector<MSG_S2C::stQueryPlayersAck::player_info>& vList);
	std::string m_QueryName;

	void ShowInputBox();
	void HideInputBox();
	void AppendChatMsg(int msg , const char * text);
	void AppendQuestMsg(ui32 QuestId, ui32 QuestStat);
	void AppendNewTitle();

	void SetChatSystemVisible(BOOL vis);

	void ShowChatSystem();
	void HideChatSystem();

	void ShowChatProps(UINT propstype);

	void UpdataChannelTime(float fTime);

	void SetVisMsg(int Index);
	void SetChatSettingDefault();
	void UpdataDALABA();

	
	
	inline void SetLastWhisperName(std::string& Name){ m_LastWhisperName = Name;};
	inline std::string GetLastWhisperName(){return m_LastWhisperName;};
	std::string m_LastWhisperName;
public:
	//战斗信息
	void DamageMsg(ui64 target_guid, ui64 caster_guid, ui32 damageNum, ui32 victim_stat, 
		ui32 hitStats, ui32 damage_abs, ui32 resisted_damage, ui32 blocked_damage);//输出伤害
	void SpellDmgMsg(ui64 createby_guid, ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 damageNum, 
		ui32 AbsorbedDamage, ui32 ResistedDamage, ui32 PhysicalDamage, ui32 BlockedDamage, uint8 bCri);//使用战斗技能
	void SpellHeathMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 HeathNum);//HP治疗
	void SpellManaMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 ManaNum);//MP治疗
	void SpellMissMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 SpellLogType);//技能Miss
	void AuraDmgMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 damageNum, ui32 abs_dmg, ui32 resisted_damage);//debuff伤害
	void AuraHealMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 damageNum, ui32 abs_dmg, ui32 resisted_damage);//buff治疗
	void AuraHealMpMsg(ui64 target_guid, ui64 caster_guid, ui32 SpellId, ui32 damageNum, ui32 abs_dmg, ui32 resisted_damage);//buff治疗
	void LocalDmgMsg(ui64 target_guid, ui32 damageNum);//战斗所受伤害
	void DeathMsg(ui64 target_guid, ui64 killer = 0);//死亡
	void ExpGotMsg(ui32 ExpNum);//战斗获得经验
	void ItemGotMsg(ui32 itemId, ui32 itemNum);//战斗获得物品
	void LevelUpMsg(ui32 level);//升级信息
	void SpellLearnMsg(ui32 SpellId);
	void SpellCCResult(ui64 target_guid, ui64 caster_guid, ui32 SpellId, uint8 effect, uint8 result);
	//?战斗信息?
	void HonorGotMsg(int HonorValue);//获得荣誉
public:
	void AddItemMsg(std::string str);
	void AddItemMsg(ui32 Itementry, ui64 ItemGuid = 0);
	void AddItemMsg(ui32 Itementry, ItemExtraData* sItemData = NULL);
	void AddItemMsgInPagckage(UINT pos, int type);
	void AddSpellMsg(ui32 SpellId);
	void AddQuestMsg(ui32 QuestEntry);
	void AddFace(const char* FaceImage); //添加表情

	void OnClickMsgItem(std::string Item);
	void OnFittingEquipment(std::string Item);
	void OnClickMsgSpell(std::string Item);

	void FittingEquipmentInPagckage(UINT pos, int type);
protected:
	void InitChannelData();
public:

	int m_MaxHSize;// = SYState()->ClientApp->GetAppWindow()->GetHeight()/2;
	int m_MinHSize;// = SYState()->ClientApp->GetAppWindow()->GetHeight()/4*3;
	CommandMap m_CommandMap;
private:
	int m_CurrentChannel;
	ChannelData m_ChannelData[CHAT_MSG_MAX];
	PersonData m_PData;
	BOOL m_Hide;
public:
	UControl * m_ChatBox[CHATBOX_MAX];
public:
	MsgStore* GetMsgStore(int iStore);
	inline void SetTableModify(int iStore, int MsgType, bool vis);
	void SetTableModify(int MsgType, bool vis);
	int GetStoreType(){return m_visStore;};
protected:
	MsgStore m_MsgStore[CHAT_TABLE_NUM];
	int m_visStore;
};

#endif