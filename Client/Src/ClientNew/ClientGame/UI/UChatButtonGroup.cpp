#include "StdAfx.h"
#include "UChatButtonGroup.h"
#include "UChat.h"
#include "UIGamePlay.h"
#include "ObjectManager.h"
#include "Player.h"
//////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UHideChatButton, UControl);
UBEGIN_MESSAGE_MAP(UHideChatButton, UControl)
UON_BN_CLICKED(0, &UHideChatButton::OnSetChatHide)
UON_BN_CLICKED(1, &UHideChatButton::OnSetChatShow)
UEND_MESSAGE_MAP()

void UHideChatButton::StaticInit()
{

}
UHideChatButton::UHideChatButton()
{
	m_IsUpdateChild = FALSE;
}
UHideChatButton::~UHideChatButton()
{

}
void UHideChatButton::OnDestroy()
{
	OnSetChatShow();
	UControl::OnDestroy();
}
void UHideChatButton::OnSetChatHide()
{
	UControl* pCtrl1 = GetChildByID(0);
	UControl* pCtrl2 = GetChildByID(1);
	if (pCtrl1 && pCtrl2)
	{
		pCtrl1->SetVisible(FALSE);
		pCtrl2->SetVisible(TRUE);
	}
	ChatSystem->SetChatSystemVisible(FALSE);
}
void UHideChatButton::OnSetChatShow()
{	
	UControl* pCtrl1 = GetChildByID(0);
	UControl* pCtrl2 = GetChildByID(1);
	if (pCtrl1 && pCtrl2)
	{
		pCtrl1->SetVisible(TRUE);
		pCtrl2->SetVisible(FALSE);
	}
	ChatSystem->SetChatSystemVisible(TRUE);
}
//////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UChatButtonGroup,UControl);
UBEGIN_MESSAGE_MAP(UChatButtonGroup,UCmdTarget)
UON_BN_CLICKED(0,&UChatButtonGroup::OnChannelButton)
UON_BN_CLICKED(1,&UChatButtonGroup::OnTopButton)
UON_BN_PRESS(24,&UChatButtonGroup::OnUpButton)
UON_BN_PRESS(25,&UChatButtonGroup::OnDownButton)
UON_BN_CLICKED(2,&UChatButtonGroup::OnBottonButton)
UON_BN_CLICKED(3,&UChatButtonGroup::OnDALABA)
UEND_MESSAGE_MAP()
void UChatButtonGroup::StaticInit()
{

}
UChatButtonGroup::UChatButtonGroup(void)
{
	//m_IsUpdateChild = FALSE;
}

UChatButtonGroup::~UChatButtonGroup(void)
{
}

void UChatButtonGroup::OnDestroy()
{
	UControl::OnDestroy();
}
void UChatButtonGroup::OnChannelButton()
{
	if (ChatSystem)
	{
		UControl * ccb = ChatSystem->GetChatBox(CHATBOX_CHANNEL);
		if (ccb)
		{
			ccb->SetVisible(!ccb->IsVisible());
			if (GetParent())
			{
				GetParent()->MoveChildTo(ccb,NULL);
			}
		}
	}
}
void UChatButtonGroup::OnUpButton()
{
	if (ChatSystem)
	{
		ChatSystem->SetScrollText(WAY_UP);
	}
}
void UChatButtonGroup::OnDownButton()
{
	if (ChatSystem)
	{
		ChatSystem->SetScrollText(WAY_DOWN);
	}
}
void UChatButtonGroup::OnTopButton()
{
	if (ChatSystem)
	{
		ChatSystem->SetScrollText(WAY_TOP);
	}
}
void UChatButtonGroup::OnBottonButton()
{
	if (ChatSystem)
	{
		ChatSystem->SetScrollText(WAY_BOTTOM);
	}
}	
void UChatButtonGroup::OnDALABA()
{
	//CPlayer* pLcalPlayer = ObjectMgr->GetLocalPlayer();
	//if (pLcalPlayer)
	//{
	//	CStorage* pBagContainer = pLcalPlayer->GetItemContainer();
	//	if (pBagContainer)
	//	{
	//		UINT Count = pBagContainer->GetItemCnt(SPECIAL_ITEM_DALABA);
	//		if (Count)
	//		{
				ChatSystem->ShowChatProps(0);
	//		}
	//	}
	//}

}
void UChatButtonGroup::SetCBtnActive(BOOL value)
{
	UButton* btn = UDynamicCast(UButton,GetChildByID(0));
	if (btn)
	{
		btn->SetActive(value);
	}
}
void UChatButtonGroup::SetChannelVisible(BOOL visible)
{
	UButton* btn = UDynamicCast(UButton,GetChildByID(0));
	if (btn)
	{
		if (visible)
		{
			SetChannelButtonBkg();
		}
		//else
		//{		
		//	UControl * ccb = ChatSystem->GetChatBox(CHATBOX_CHANNEL);
		//	if (ccb && ccb->IsVisible())
		//	{
		//		ccb->SetVisible(FALSE);
		//	}
		//}
		//btn->SetVisible(visible);
	}
}
void UChatButtonGroup::SetChannelButtonBkg()
{
	if (ChatSystem)
	{
		std::string filepath;
		switch(ChatSystem->GetCurrentChannel())
		{
		case CHAT_MSG_SAY:
			filepath = "public\\ChannelCommon.skin";
			break;
		case CHAT_MSG_PARTY:
			filepath = "public\\ChannelGroup.skin";
			break;
		case CHAT_MSG_GUILD:
			filepath = "public\\ChannelTrade.skin";
			break;
		case CHAT_MSG_WHISPER:
			filepath = "public\\ChannelWhisper.skin";
			break;
		case CHAT_MSG_CITY:
			filepath = "public\\ChannelGuild.skin";
			break;
		case CHAT_MSG_TRADE:
			filepath = "public\\ChannelGuild.skin";
			break;
		};
		UBitmapButton* pbitBtn = UDynamicCast(UBitmapButton, GetChildByID(0));
		if (pbitBtn && filepath.length())
		{
			pbitBtn->SetBitmap(filepath.c_str());
		}
	}
}
void UChatButtonGroup::UpdataDALABA(bool bshow)
{
	GetChildByID(3)->SetActive(bshow);
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UChatTableControl, UControl)
UBEGIN_MESSAGE_MAP(UChatTableControl, UControl)
UON_BN_CLICKED(0, &UChatTableControl::OnClick)
UON_BN_CLICKED(1, &UChatTableControl::OnClick)
UON_BN_CLICKED(2, &UChatTableControl::OnClick)
UON_BN_CLICKED(3, &UChatTableControl::OnClick)
UON_BN_CLICKED(4, &UChatTableControl::OnClick)
UON_BN_CLICKED(5, &UChatTableControl::OnClick)
UEND_MESSAGE_MAP()
void UChatTableControl::StaticInit()
{

}
UChatTableControl::UChatTableControl(void)
{
	m_IsUpdateChild = FALSE;
}
UChatTableControl::~UChatTableControl(void)
{

}
void UChatTableControl::AddTableButton(const char* Name, vector<int>& vChannel)
{

	//smilience
}
void UChatTableControl::DelTableButton(const char* Name)
{

	//smilience
}
void UChatTableControl::SetTableButtonChannel(vector<int>& vChannel)
{

	//smilience
}

void UChatTableControl::SetBKGAlpha(float falpha)
{
	for(int i = 0 ; i < CHAT_TABLE_NUM ; i++)
	{
		UChatTableButton* pChatTableBtn = UDynamicCast(UChatTableButton, GetChildByID(i));
		if (pChatTableBtn)
		{
			pChatTableBtn->SetBKGAlpha(falpha);
		}
	}
}

void UChatTableControl::OnClick()
{
	for(int i = 0 ; i < CHAT_TABLE_NUM ; i++)
	{
		UChatTableButton* pChatTableBtn = UDynamicCast(UChatTableButton, GetChildByID(i));
		if (pChatTableBtn)
		{
			if (pChatTableBtn->IsChecked())
			{
				ChatSystem->SetVisMsg(pChatTableBtn->GetTableBtnIndex());
			}
		}
	}
}

BOOL UChatTableControl::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	UChatTableButton* pChatTableBtn = UDynamicCast(UChatTableButton, GetChildByID(CHAT_TABLE_ALL));
	if (pChatTableBtn)
	{
		pChatTableBtn->SetText(_TRAN("All"));
		pChatTableBtn->SetTableBtnIndex(CHAT_TABLE_ALL);
	}
	pChatTableBtn = UDynamicCast(UChatTableButton, GetChildByID(CHAT_TABLE_PARTY));
	if (pChatTableBtn)
	{
		pChatTableBtn->SetText(_TRAN("Party"));
		pChatTableBtn->SetTableBtnIndex(CHAT_TABLE_PARTY);
	}
	pChatTableBtn = UDynamicCast(UChatTableButton, GetChildByID(CHAT_TABLE_GUILD));
	if (pChatTableBtn)
	{
		pChatTableBtn->SetText(_TRAN("Guild"));
		pChatTableBtn->SetTableBtnIndex(CHAT_TABLE_GUILD);
	}
	pChatTableBtn = UDynamicCast(UChatTableButton, GetChildByID(CHAT_TABLE_WHISPER));
	if (pChatTableBtn)
	{
		pChatTableBtn->SetText(_TRAN("Whisper"));
		pChatTableBtn->SetTableBtnIndex(CHAT_TABLE_WHISPER);
	}
	pChatTableBtn = UDynamicCast(UChatTableButton, GetChildByID(CHAT_TABLE_AREA));
	if (pChatTableBtn)
	{
		pChatTableBtn->SetText(_TRAN("Area"));
		pChatTableBtn->SetTableBtnIndex(CHAT_TABLE_AREA);
	}
	pChatTableBtn = UDynamicCast(UChatTableButton, GetChildByID(CHAT_TABLE_BATTLE));
	if (pChatTableBtn)
	{
		pChatTableBtn->SetText(_TRAN("Battle"));
		pChatTableBtn->SetTableBtnIndex(CHAT_TABLE_BATTLE);
	}
	return TRUE;
}
void UChatTableControl::OnDestroy()
{
	return UControl::OnDestroy();
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UChatTableButton, UButton)
void UChatTableButton::StaticInit()
{

}
UChatTableButton::UChatTableButton()
{
	m_falpha = 0.0f;
}
UChatTableButton::~UChatTableButton()
{

}
BOOL UChatTableButton::OnCreate()
{
	if (!UButton::OnCreate())
	{
		return FALSE;
	}
	return TRUE;
}
void UChatTableButton::OnDestroy()
{
	return UButton::OnDestroy();
}

void UChatTableButton::DrawCheckBox(const UPoint& offset, const URect& ClipRect)
{
	assert(m_Style);

	int nSkinId = m_bCheck ? CBS_CHECK_NORMAL: CBS_UNCHECK_NORMAL;
	if (m_Active)
	{
		if (m_bMouseOver)
		{
			nSkinId++;
		}
	}else
	{
		nSkinId = CBS_DISABLED;
	}

	USkin* pSkin = m_Style->m_spSkin;
	if (pSkin)
	{
		sm_UiRender->DrawSkin(pSkin, nSkinId, offset);
	}
	else
	{
		URect boundsRect(offset, m_Size);
		if (m_bPressed || m_bCheck)
		{
			sm_UiRender->DrawRectFill(boundsRect, D3DCOLOR_ARGB(int(120 * m_falpha), 1, 1 ,1));
		}else
		{
			boundsRect.pt0.y += 2;
			sm_UiRender->DrawRectFill(boundsRect, D3DCOLOR_ARGB(int(125 * m_falpha), 116, 116 ,116));
		}
	}

	if (!m_strBtnText.Empty() && m_Style->m_spFont)
	{
		int xOfs =  (nSkinId == CBS_CHECK_NORMAL) || (nSkinId == CBS_CHECK_HILIT) ? 2 : 0;
		UPoint Offset(offset.x - xOfs, offset.y);
		UPoint Size(m_Size.x - xOfs, m_Size.y);

		UColor FontColor = m_Active ? (m_bMouseOver ? m_Style->m_FontColorHL : m_Style->m_FontColor) : m_Style->m_FontColorNA;
		FontColor.a =  BYTE(FontColor.a * m_falpha);
		USetTextEdge(TRUE, LCOLOR_ARGB(BYTE(180 * m_falpha),0,0,0));
		UDrawText(sm_UiRender, m_Style->m_spFont, Offset, Size, FontColor, m_strBtnText.GetBuffer(), UT_CENTER);
		USetTextEdge(FALSE);
	}
}

void UChatTableButton::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	UButton::OnMouseDown(pt, nClickCnt, uFlags);
	m_ptLeftBtnDown = pt;
}

void UChatTableButton::OnMouseDragged(const UPoint& pt, UINT uFlags)
{
	UPoint delta = pt - m_ptLeftBtnDown;
	m_ptLeftBtnDown = pt;
	UPoint newpos = GetParent()->GetWindowPos();
	//UPoint oldSize = GetWindowSize();
	newpos.y += delta.y;
	if ( newpos.y > ChatSystem->m_MinHSize  || newpos.y < ChatSystem->m_MaxHSize )
	{
		return;
	}
	GetParent()->SetPosition(newpos);
	UInGame::GetFrame(FRAME_IG_SYSGIFT)->SetPosition(newpos - UPoint(0, 18));
	UInGame::GetFrame(FRAME_IG_CHATMSGSHOW)->SetTop(GetParent()->GetWindowPos().y + GetParent()->GetWindowSize().y);
}
void UChatTableButton::SetTableBtnIndex(int TableBtnIndex)
{
	m_TableBtnIndex = TableBtnIndex;
}
int UChatTableButton::GetTableBtnIndex()
{
	return m_TableBtnIndex;
}
void UChatTableButton::SetBKGAlpha(float falpha)
{
	m_falpha = falpha;
}
void UChatTableButton::OnMouseEnter(const UPoint& pt, UINT uFlags)
{
	UButton::OnMouseEnter(pt, uFlags);
	ChatSystem->ShowMsgBox();
}

void UChatTableButton::OnMouseLeave(const UPoint& pt, UINT uFlags)
{
	UButton::OnMouseLeave(pt, uFlags);
	ChatSystem->HideMsgBox();
}