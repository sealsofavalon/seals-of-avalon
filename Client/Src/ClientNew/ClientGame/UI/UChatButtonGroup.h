//-----------------------------------------------------------------------------------
//聊天系统中的按钮组控件
//-----------------------------------------------------------------------------------
#ifndef UCHATBUTTONGROUP_H
#define UCHATBUTTONGROUP_H
#include "UInc.h"
class UHideChatButton : public UControl
{
	UDEC_CLASS(UHideChatButton);
	UDEC_MESSAGEMAP();
public:
	virtual void OnDestroy();
	UHideChatButton();
	~UHideChatButton();
public:
	void OnSetChatHide();
	void OnSetChatShow();
};
class UChatButtonGroup : public UControl
{
	UDEC_CLASS(UChatButtonGroup);
	UDEC_MESSAGEMAP();
public:
	UChatButtonGroup(void);
	~UChatButtonGroup(void);
public:
	void SetCBtnActive(BOOL value);
	void SetChannelVisible(BOOL visible);
	void SetChannelButtonBkg();
	void UpdataDALABA(bool bshow);

	//virtual BOOL OnCreate();
	virtual void OnDestroy();
private:
	void OnChannelButton();
	void OnUpButton();
	void OnDownButton();
	void OnTopButton();
	void OnBottonButton();
	void OnDALABA();
};
class UChatTableControl : public UControl
{
	UDEC_CLASS(UChatTableControl);
	UDEC_MESSAGEMAP();
public:
	UChatTableControl(void);
	~UChatTableControl(void);
public:
	void AddTableButton(const char* Name, vector<int>& vChannel);
	void DelTableButton(const char* Name);
	void SetTableButtonChannel(vector<int>& vChannel);
	void SetBKGAlpha(float falpha);

	void OnClick();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
};
class UChatTableButton : public UButton
{
	UDEC_CLASS(UChatTableButton);
public:
	UChatTableButton();
	~UChatTableButton();

	void SetTableBtnIndex(int TableBtnIndex);
	int GetTableBtnIndex();

	void SetBKGAlpha(float falpha);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void DrawCheckBox(const UPoint& offset, const URect& ClipRect);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnMouseDragged(const UPoint& pt, UINT uFlags);

	virtual void OnMouseEnter(const UPoint& pt, UINT uFlags);
	virtual void OnMouseLeave(const UPoint& pt, UINT uFlags);

protected:
	int m_TableBtnIndex;
	std::vector<int> m_vVisChannel;
	UPoint m_ptLeftBtnDown;
	float m_falpha;
};
#endif