#include "StdAfx.h"
#include "UChatChannelBox.h"
#include "UChat.h"

UIMP_CLASS(UChatChannelBox,UControl);
UBEGIN_MESSAGE_MAP(UChatChannelBox,UCmdTarget)
UON_BN_CLICKED(0,&UChatChannelBox::CheckNormal)
UON_BN_CLICKED(1,&UChatChannelBox::CheckGroup)
UON_BN_CLICKED(2,&UChatChannelBox::CheckPrivate)
UON_BN_CLICKED(3,&UChatChannelBox::CheckGuild)
//UON_BN_CLICKED(4,&UChatChannelBox::CheckContry)
UON_BN_CLICKED(4,&UChatChannelBox::CheckTrade)
//UON_BN_CLICKED(5,&UChatChannelBox::CheckBattle)
UON_BN_CLICKED(5,&UChatChannelBox::ChooseChannel)
UON_BN_CLICKED(6,&UChatChannelBox::ChooseChannel)
UON_BN_CLICKED(7,&UChatChannelBox::ChooseChannel)
UON_BN_CLICKED(8,&UChatChannelBox::ChooseChannel)
//UON_BN_CLICKED(11,&UChatChannelBox::ChooseChannel)
UON_BN_CLICKED(9,&UChatChannelBox::ChooseChannel)
UEND_MESSAGE_MAP()

int channel[] = 
{
	CHAT_MSG_SAY,
	CHAT_MSG_PARTY,
	CHAT_MSG_WHISPER,
	CHAT_MSG_GUILD,
	//CHAT_MSG_CITY,//应该是国家频道
	CHAT_MSG_CITY//应该是交易频道但是没有找到相应的频道值所以先用一般频道代替
};
void UChatChannelBox::StaticInit()
{

}
UChatChannelBox::UChatChannelBox(void)
{
	for (int i = 0 ; i < 10 ; i++)
	{
		m_button[i] = NULL;
	}
	m_IsUpdateChild = FALSE;
}

UChatChannelBox::~UChatChannelBox(void)
{
}
BOOL UChatChannelBox::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	for (int i = 0 ; i < 10 ; i ++)
	{
		//0 - 4   checkbutton
		//5 - 10  radiobutton
		if (m_button[i] == NULL)
		{
			m_button[i] = GetChildByID(i);
		}
		if (m_button[i] == NULL)
		{
			return FALSE;
		}
	}
	if (m_bkgskin == NULL)
	{
		m_bkgskin = sm_System->LoadSkin("Chat\\back.skin");
	}
	return TRUE;
}
void UChatChannelBox::OnDestroy()
{
	UControl::OnDestroy();
}
void UChatChannelBox::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_bkgskin)
	{
		sm_UiRender->DrawSkin(m_bkgskin,2,updateRect,UColor(255,255,255,255));
	}
	UControl::OnRender(offset,updateRect);
}
UControl* UChatChannelBox::FindHitWindow(const UPoint &pt, INT initialLayer)
{
	UControl* pCtrl = UControl::FindHitWindow(pt, initialLayer);
	switch (ChatSystem->GetStoreType())
	{
	case CHAT_TABLE_PARTY:
		if (pCtrl == GetChildByID(1))
		{
			return this;
		}
		break;
	case CHAT_TABLE_GUILD:
		if (pCtrl == GetChildByID(3))
		{
			return this;
		}
		break;
	case CHAT_TABLE_WHISPER:
		if (pCtrl == GetChildByID(2))
		{
			return this;
		}
		break;
	case CHAT_TABLE_AREA:
		if (pCtrl == GetChildByID(4))
		{
			return this;
		}
		break;
	}
	return pCtrl;
}
void UChatChannelBox::CheckNormal()
{
	if (ChatSystem)
	{
		UButton * btn = UDynamicCast(UButton,m_button[0]);
		if (btn)
		{
			if (btn->IsChecked())
			{
				ChatSystem->SetTableModify(MSG_SAY, true);
			}
			else
			{
				ChatSystem->SetTableModify(MSG_SAY, false);
			}
		}
	}
}
void UChatChannelBox::CheckGroup()
{
	if (ChatSystem)
	{
		UButton * btn = UDynamicCast(UButton,m_button[1]);
		if (btn)
		{
			if (btn->IsChecked())
			{
				ChatSystem->SetTableModify(MSG_PARTY, true);
			}
			else
			{
				ChatSystem->SetTableModify(MSG_PARTY, false);
			}
		}
	}
}
void UChatChannelBox::CheckPrivate()
{
	if (ChatSystem)
	{
		UButton * btn = UDynamicCast(UButton,m_button[2]);
		if (btn)
		{
			if (btn->IsChecked())
			{
				ChatSystem->SetTableModify(MSG_WHISPER, true);
			}
			else
			{
				ChatSystem->SetTableModify(MSG_WHISPER, false);
			}
		}
	}
}
void UChatChannelBox::CheckGuild()
{
	if (ChatSystem)
	{
		UButton * btn = UDynamicCast(UButton,m_button[3]);
		if (btn)
		{
			if (btn->IsChecked())
			{
				ChatSystem->SetTableModify(MSG_GUILD, true);
			}
			else
			{
				ChatSystem->SetTableModify(MSG_GUILD, false);
			}
		}
	}
}
void UChatChannelBox::CheckContry()
{

}
void UChatChannelBox::CheckTrade()
{
	if (ChatSystem)
	{
		UButton * btn = UDynamicCast(UButton,m_button[4]);
		if (btn)
		{
			std::string str;
			if(!ChatSystem->GetAreaChannel(str))
			{
				if(btn->IsChecked())
					ChatSystem->SetWaitMap();
				return;
			}

			if (btn->IsChecked())
			{
				//ChatSystem->SendChannelJoin(str.c_str(),CHAT_CHANNEL_TYPE_CITY);
				ChatSystem->SetTableModify(MSG_AREA, true);
			}
			else
			{
				//ChatSystem->SendChannelLeave(str.c_str());
				ChatSystem->SetTableModify(MSG_AREA, false);
			}
		}
	}
}
void UChatChannelBox::CheckBattle()
{

}
void UChatChannelBox::ChooseChannel()
{
	for (int i = 5 ; i < 10 ; i++)
	{
		UButton * btn = UDynamicCast(UButton,m_button[i]);
		if (btn)
		{
			if(btn->IsChecked())
			{
				if (CursorInWindow())
				{
					if (ChatSystem)
					{
						ChatSystem->SetCurrentChannel(channel[i - 5]);
						ChatSystem->ShowInputBox();
					}
					SetVisible(FALSE);
				}
				return;
			}
		}
	}
}
void UChatChannelBox::Refursh()
{
	for(int i = 0 ; i < 5 ; i++)
	{
		if (channel[i] == ChatSystem->GetCurrentChannel())
		{
			UButton * btn = UDynamicCast(UButton,m_button[i + 5]);
			if (btn)
			{
				btn->SetCheck(FALSE);
				return ;
			}
		}
	}
}
void UChatChannelBox::InitChannel()
{
}
void UChatChannelBox::SetModify(int modify)
{
	UButton * btn;
	if (modify & MSG_SAY)
	{
		btn = UDynamicCast(UButton,m_button[0]);
		btn->SetCheck(FALSE);
	}else
	{
		btn = UDynamicCast(UButton,m_button[0]);
		btn->SetCheck(TRUE);
	}
	if (modify & MSG_PARTY)
	{
		btn = UDynamicCast(UButton,m_button[1]);
		btn->SetCheck(FALSE);
	}else
	{
		btn = UDynamicCast(UButton,m_button[1]);
		btn->SetCheck(TRUE);
	}
	if (modify & MSG_WHISPER)
	{
		btn = UDynamicCast(UButton,m_button[2]);
		btn->SetCheck(FALSE);
	}else
	{
		btn = UDynamicCast(UButton,m_button[2]);
		btn->SetCheck(TRUE);
	}
	if (modify & MSG_GUILD)
	{
		btn = UDynamicCast(UButton,m_button[3]);
		btn->SetCheck(FALSE);
	}else
	{
		btn = UDynamicCast(UButton,m_button[3]);
		btn->SetCheck(TRUE);
	}
	if (modify & MSG_AREA)
	{
		btn = UDynamicCast(UButton,m_button[4]);
		btn->SetCheck(FALSE);
	}else
	{
		btn = UDynamicCast(UButton,m_button[4]);
		btn->SetCheck(TRUE);
	}
}