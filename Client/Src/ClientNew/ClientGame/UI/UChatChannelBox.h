//-----------------------------------------------------------------------------------
//聊天系统中的频道框控件
//-----------------------------------------------------------------------------------
#ifndef UCHATCHANNELBOX_H
#define UCHATCHANNELBOX_H
#include "UInc.h"
class UChatChannelBox : public UControl
{
	UDEC_CLASS(UChatChannelBox);
	UDEC_MESSAGEMAP();
public:
	UChatChannelBox(void);
	~UChatChannelBox(void);

	void Refursh();
	void InitChannel();

	void SetModify(int modify);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */);
private:
	void CheckNormal();
	void CheckGroup();
	void CheckPrivate();
	void CheckGuild();
	void CheckContry();
	void CheckTrade();
	void CheckBattle();
	void ChooseChannel();
private:
	UControl * m_button[10];
	USkinPtr m_bkgskin;
};
#endif