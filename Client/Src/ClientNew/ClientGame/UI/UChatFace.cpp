#include "stdafx.h"
#include "UChatFace.h"
#include "UChat.h"

#define  FaceXCount 10
#define  FaceYCount 5

UIMP_CLASS(UChatFace, UControl);
void UChatFace::StaticInit()
{

}
UChatFace::UChatFace()
{
	m_FaceImage = NULL;
	m_bMouseOver = FALSE;
	m_IsUpdateChild = FALSE;
}
UChatFace::~UChatFace()
{

}
BOOL UChatFace::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (m_FaceImage == NULL)
	{
		m_FaceImage = sm_UiRender->LoadTexture("Data\\UI\\Chat\\Face.png");
	}
	
	return TRUE;
}
void UChatFace::OnMouseUp(const UPoint& position, UINT flags)
{
	if (!m_FaceImage)
	{
		return ;
	}
	UPoint pkLocal = ScreenToWindow(position);
	UINT FaceW = m_FaceImage->GetWidth();
	UINT FaceH = m_FaceImage->GetHeight();

	UPoint FaceIndex;
	FaceIndex.x = pkLocal.x * FaceXCount / m_Size.x ;
	FaceIndex.y = pkLocal.y * FaceYCount / m_Size.y ;

	AddFaceToInput(FaceIndex);
	

}
//void UChatFace::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
//{
//	if (!m_FaceImage)
//	{
//		return ;
//	}
//	UPoint pkLocal = ScreenToWindow(point);
//	UINT FaceW = m_FaceImage->GetWidth();
//	UINT FaceH = m_FaceImage->GetHeight();
//
//	UPoint OverIndex;
//	OverIndex.x = pkLocal * FaceXCount / m_Size.x ;
//	OverIndex.y = pkLocal * FaceYCount / m_Size.y ;
//
//	m_bMouseOver = TRUE ;
//}
void UChatFace::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_FaceImage)
	{
		sm_UiRender->DrawImage(m_FaceImage,updateRect);
	}
}
void UChatFace::AddFaceToInput(UPoint index)
{
	if (index.x >= 0 && index.x < FaceXCount && index.y >= 0 && index.y < FaceYCount)
	{
		UINT ImageIndex = index.y * FaceXCount + index.x + 1; 
		char FaceInfo[_MAX_PATH];
		NiSprintf(FaceInfo, _MAX_PATH, "[/%d]",ImageIndex);
		ChatSystem->AddFace(FaceInfo);
		SetVisible(FALSE);
	}
}