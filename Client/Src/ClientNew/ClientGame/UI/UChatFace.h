#ifndef __UCHATFACE_H__
#define __UCHATFACE_H__

class UChatFace : public UControl
{
	UDEC_CLASS(UChatFace);
public:
	UChatFace();
	~UChatFace();
	void AddFaceToInput(UPoint index); // 添加表情到输入框
protected:
	BOOL virtual OnCreate();
	void virtual OnMouseUp(const UPoint& position, UINT flags);/*
	void virtual OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);*/
	void virtual OnRender(const UPoint& offset,const URect &updateRect);
private:
	UTexturePtr m_FaceImage ;
	BOOL m_bMouseOver ;
};

#endif