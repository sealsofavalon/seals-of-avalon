#include "StdAfx.h"
#include "UChatInputMsgBox.h"
#include "UEditExt.h"
#include "UChat.h"
#include "Utils/ChatFileterDB.h"
#include "UFun.h"

//命令的系统还很不完善暂时先满足命令条件
//需要很多改进

#define  MaxUserInPut 120
#define  MaxSaveUserMsg 30    //保存最多的用户输入.

UIMP_CLASS(UChatInputMsgBox,UControl);
UBEGIN_MESSAGE_MAP(UChatInputMsgBox,UCmdTarget)
UON_UEDIT_UPDATE(0, &UChatInputMsgBox::AppendPlayerMsg)
UON_UEDIT_CHANGED(0,&UChatInputMsgBox::FindChannel)
UON_BN_CLICKED(1, &UChatInputMsgBox::BShowFaceImage)
//目前这个只由空格键点击后在WM_CHAR消息中响应，本来要放在WM_KEYDOWN中可伴随的
//WM_CHAR的消息又会造成一次输入，导致了命令响应完后在输入框中加入一个空格的现象
UEND_MESSAGE_MAP()
UChatInputMsgBox* g_Input = NULL;
void UChatInputMsgBox::StaticInit()
{

}

UChatInputMsgBox::UChatInputMsgBox(void)
{
	m_bkgskin = NULL;
	g_Input = this;
}

UChatInputMsgBox::~UChatInputMsgBox(void)
{
	g_Input = NULL;
}
bool MemberMemoryMsg(UINT nKeyCode, UINT Flags)
{
	if (g_Input)
	{
		return g_Input->OnMemberMsg(nKeyCode, Flags);
	}
	return false;
}
BOOL UChatInputMsgBox::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	UEditExt * eex = UDynamicCast(UEditExt,GetChildByID(0));
	if (eex)
	{
		eex->SetTextOffset( UPoint(0, 1) );
		eex->SetMaxLength(MaxUserInPut);
		eex->GetBody()->fun = &MemberMemoryMsg;
	}
	if (m_bkgskin == NULL)
	{
		m_bkgskin = sm_System->LoadSkin("Chat\\back.skin");
	}
	return TRUE;
}
void UChatInputMsgBox::OnDestroy()
{
	SetVisible(FALSE);
	UControl::OnDestroy();
}
void UChatInputMsgBox::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);
	if (ChatSystem)
	{
		ChatSystem->UpdataChannelTime(fDeltaTime);
	}
}
void UChatInputMsgBox::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_bkgskin)
	{
		sm_UiRender->DrawSkin(m_bkgskin,1,updateRect,UColor(255,255,255,255));
	}
	UControl::OnRender(offset,updateRect);
}
BOOL UChatInputMsgBox::OnEscape()
{
	if (!UControl::OnEscape())
	{
		ChatSystem->HideInputBox();
		//HideThis();
		return TRUE;
	}
	return FALSE;
}
void UChatInputMsgBox::AppendPlayerMsg()
{
	std::string UserMsg;
	if (GetUserMessage(UserMsg))
	{
		CommandElement _cElement;
		_cElement.Command.resize(0);
		_cElement.Text.resize(0);
		if(ParseUserMessage(UserMsg,_cElement))
		{
			ChatSystem->ParseCommand(_cElement.Command,_cElement.Text);
		}
		else
		{
			int msgtype = ChatSystem->GetCurrentChannel();
			/*UEditExt * eex = UDynamicCast(UEditExt,GetChildByID(0));
			if (eex)
			{
				eex->GetBody()->ParseFaceBuffer(UserMsg);
			}*/
			ChatSystem->SendChatMsg(msgtype,UserMsg);
			
// 			ChatSystem->SendChatMsg(msgtype,UserMsg);
// 			m_Saytime = 0.0f;
			SaveMemoryMsg(UserMsg);
		}
	}
	ChatSystem->HideInputBox();
	//HideThis();
}
BOOL UChatInputMsgBox::ParseUserMessage(const std::string & UserMsg, CommandElement &_cElement)
{
	if (UserMsg[0] == '/')
	{
		//解析出CommandElement
		std::string _data = UserMsg.substr(1,UserMsg.size());
		int Comlength = _data.find(' ');
		if (Comlength)
		{
			_cElement.Command = _data.substr(0,Comlength);
			if (Comlength >= 0)
			{
				_cElement.Text = _data.substr(Comlength + 1 , _data.size());
			}
			return TRUE;
		}
		return FALSE;
	}
	else
	{
		return FALSE;
	}
}
BOOL UChatInputMsgBox::GetUserMessage(std::string &UserMsg)
{
	char buf[2048];
	UEditExt * eex = UDynamicCast(UEditExt,GetChildByID(0));
	if (!eex)
	{
		return FALSE;
	}
	int wstrlen = eex->GetBody()->GetText(buf,2048);

	if (!wstrlen)
		TraceLastError();

	if (buf && wstrlen)
	{
		UserMsg = buf;
		return TRUE;
	}

	return FALSE;
}
void UChatInputMsgBox::AppUserMessage(const char* mess)
{
	UEditExt * eex = UDynamicCast(UEditExt,GetChildByID(0));
	if (eex)
	{
		eex->GetBody()->InsertText(mess);
	}
}
void UChatInputMsgBox::BShowFaceImage()
{
	UControl* pCon = ChatSystem->GetChatBox(CHATBOX_FACEIMGE);
	if (pCon)
	{
		pCon->SetVisible(!pCon->IsVisible());
	}

}
void UChatInputMsgBox::SaveMemoryMsg(const std::string &str)
{
	if (str.length())
	{
		if (m_MemoryUserMsg.size() > MaxSaveUserMsg)
		{
			m_MemoryUserMsg.pop_front();
		}
		else
		{
			m_MemoryUserMsg.push_back(str);
		}
		m_MemoryUserMsgPos = m_MemoryUserMsg.size();
	}
}
void UChatInputMsgBox::FindChannel()
{
	std::string UserData;
	UserData.resize(0);
	UEditExt * eet = UDynamicCast(UEditExt,GetChildByID(0));
	if (eet)
	{
		if (GetUserMessage(UserData))
		{
			CommandElement _cElement;
			_cElement.Command.resize(0);
			_cElement.Text.resize(0);
			if(ParseUserMessage(UserData,_cElement))
			{
				UChat::ChannelData * _CDataW = ChatSystem->GetChannelData(CHAT_MSG_WHISPER);
				if (_CDataW &&  _stricmp(_cElement.Command.c_str() , _CDataW->CallCommand) == 0)
				{
					int len = _cElement.Text.size();
					if (len)
					{
						_cElement.Text[len - 1]  = 0;
						ChatSystem->ParseCommand(_cElement.Command,_cElement.Text);
						return;
					}
					else
					{
						return;
					}
				}
				for (int i = 0 ; i < CHAT_MSG_MAX ; i++)
				{
					UChat::ChannelData * _CData = ChatSystem->GetChannelData(i);
					if (_CData && _CData->MsgType != -1 && _stricmp(_cElement.Command.c_str(),_CData->CallCommand) == 0)
					{
						ChatSystem->ParseCommand(_cElement.Command,_cElement.Text);
						return;
					}
				}
			}
		}
	}
}
void UChatInputMsgBox::ShowThis()
{
	if (!ChatSystem)
	{
		return;
	}
	if (IsVisible())
	{
		SetFocusControl();
	}
	if (GetParent())
	{
		GetParent()->MoveChildTo(this,NULL);
	}
	string _headString;
	UChat::ChannelData * cdata = ChatSystem->GetChannelData(ChatSystem->GetCurrentChannel());
	if (cdata->MsgType != -1)
	{	
		if (ChatSystem->GetCurrentChannel() == CHAT_MSG_WHISPER)
		{
			_headString = cdata->ChannelHead + ChatSystem->GetPersonData()->CreateName;
		}
		else
		{
			_headString = cdata->ChannelHead;
		}

		_headString += ":";
		UEditExt * eex = UDynamicCast(UEditExt,GetChildByID(0));
		if (eex)
		{
			eex->SetHeadText(_headString.c_str(),0);
			eex->SetHeadFontColor(cdata->ChannelColor.c_str());
			eex->SetBodyFontColor(cdata->ChannelColor.c_str());
			eex->SetFocusControl();
		}
	}
	SetVisible(TRUE);
}
void UChatInputMsgBox::HideThis()
{
	SetVisible(FALSE);
	UEditExt * eex = UDynamicCast(UEditExt,GetChildByID(0));
	if (eex)
	{
		eex->Clear();
	}
	sm_System->GetCurFrame()->SetFocusControl();
}

bool UChatInputMsgBox::OnMemberMsg(UINT nKeyCode, UINT Flags)
{
	//if (Flags & LKM_ALT)
	{
		if (nKeyCode == LK_UP)
		{
			UpMemoryMsg();
			return true;
		}
		if (nKeyCode == LK_DOWN)
		{
			DownMemoryMsg();
			return true;
		}
	}
	return false;
}

void UChatInputMsgBox::DownMemoryMsg()
{
	int i = m_MemoryUserMsg.size();

	if (i > 0 && m_MemoryUserMsgPos >= 0)
	{
		if (m_MemoryUserMsgPos < i && m_MemoryUserMsgPos >= 0)
		{	
			m_MemoryUserMsgPos++ ;
			if (m_MemoryUserMsgPos < i)
			{
				UEditExt * eex = UDynamicCast(UEditExt,GetChildByID(0));
				if (eex)
					eex->SetBodyText(m_MemoryUserMsg[m_MemoryUserMsgPos].c_str());
				return  ;
			}else
			{
				m_MemoryUserMsgPos = m_MemoryUserMsgPos - 1 ;
				return  ;
			}
		}
	}
}
void UChatInputMsgBox::UpMemoryMsg()
{
	int i = m_MemoryUserMsg.size();
	UEditExt * eex = UDynamicCast(UEditExt,GetChildByID(0));
	if (i > 0 && m_MemoryUserMsgPos >= 0)
	{
		if (m_MemoryUserMsgPos <= i && m_MemoryUserMsgPos > 0)
		{
			if (m_MemoryUserMsgPos == 0)
			{
				if (eex)
					eex->SetBodyText(m_MemoryUserMsg[m_MemoryUserMsgPos].c_str());
				return ;
			}else
			{
				m_MemoryUserMsgPos-- ;
				if (m_MemoryUserMsgPos >= 0)
				{
					if (eex)
						eex->SetBodyText(m_MemoryUserMsg[m_MemoryUserMsgPos].c_str());
					return  ;
				}
			}
		}
	}
}
void UChatInputMsgBox::acceleratorKeyPress( const char * opCode )
{
	if (!ChatSystem)
		return;
	if(strcmp("CallTalk", opCode) == 0)
	{	
		ChatSystem->ShowInputBox();
	}
	if(strcmp("Whisper", opCode) == 0)
	{
		ChatSystem->SetCurrentChannel(CHAT_MSG_WHISPER);
		ChatSystem->SetPTalkPerson(ChatSystem->GetLastWhisperName().c_str());
		ChatSystem->ShowInputBox();
	}
}
void UChatInputMsgBox::acceleratorKeyRelease(const char * opCode)
{

}