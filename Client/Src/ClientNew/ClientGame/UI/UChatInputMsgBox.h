//-----------------------------------------------------------------------------------
//聊天系统中的输入框控件
//-----------------------------------------------------------------------------------
#ifndef UCHATINPUTMSGBOX_H
#define UCHATINPUTMSGBOX_H
#include "UInc.h"
class UChatInputMsgBox:public UControl
{
	struct CommandElement
	{
		std::string Command;
		std::string Text;
	};
	UDEC_CLASS(UChatInputMsgBox);
	UDEC_MESSAGEMAP();
public:
	UChatInputMsgBox(void);
	~UChatInputMsgBox(void);
public:
	void ShowThis();
	void HideThis();
	void AppUserMessage(const char* mess);
	bool OnMemberMsg(UINT nKeyCode, UINT Flags);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual BOOL OnEscape();
	virtual void acceleratorKeyPress(const char * opCode);
	virtual void acceleratorKeyRelease(const char * opCode);
private:
	void AppendPlayerMsg();
	void FindChannel();
	BOOL ParseUserMessage(const std::string &UserMsg, CommandElement &_cElement);
	BOOL GetUserMessage(std::string &UserMsg);
	void SaveMemoryMsg(const std::string &str);
	void BShowFaceImage();
	USkinPtr m_bkgskin;
private:
	//保存用户输入信息
	void UpMemoryMsg();    // up msg
	void DownMemoryMsg();  // down msg
	std::deque<std::string> m_MemoryUserMsg ;              //save user msg
	int m_MemoryUserMsgPos ;       //pos	
};
#endif