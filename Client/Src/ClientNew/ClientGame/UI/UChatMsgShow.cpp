#include "StdAfx.h"
#include "ClientApp.h"
#include "UChatMsgShow.h"
#include "UChat.h"
#include "UITipSystem.h"
#include "UFun.h"


UIMP_CLASS(UChatMsgShow,UControl);
UBEGIN_MESSAGE_MAP(UChatMsgShow,UCmdTarget)
UON_RED_URL_CLICKED(0,&UChatMsgShow::OnURLClicked)
UON_RED_URL_RCLICKED(0,&UChatMsgShow::OnURLRClicked)
UON_RED_URL_SHIFTCLICKED(0,&UChatMsgShow::OnShifClicked)
UON_RED_URL_CTRLCLICKED(0,&UChatMsgShow::OnCtrlClicked)
UEND_MESSAGE_MAP()
void UChatMsgShow::StaticInit(){}
UIMP_CLASS(UChatRichEdit,URichTextCtrl);
void UChatRichEdit::StaticInit(){}

UChatMsgShow::UChatMsgShow(void)
{
	m_bkgskin = NULL;
	m_Alpha = 0;
	m_DragBoard = 5;
	m_Bottom = 0;
	m_DrawDragborad = FALSE;
	m_LockMouseMove = FALSE;
	m_LockScroll = FALSE;
	m_ShowType = 0;
}

UChatMsgShow::~UChatMsgShow(void)
{
}
BOOL UChatMsgShow::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_bkgskin == NULL)
	{
		m_bkgskin = sm_System->LoadSkin("Chat\\back.skin");
	}
	return TRUE;
}
void UChatMsgShow::OnDestroy()
{
	for(int i = 0 ; i < CHAT_TABLE_NUM ; i++)
	{
		UChat::MsgStore* pStore = ChatSystem->GetMsgStore(i);
		if (pStore)
		{
			pStore->Clear();
		}
	}
	UControl::OnDestroy();
}
void UChatMsgShow::OnRender(const UPoint& offset,const URect &updateRect)
{
	//if (m_bkgskin)
	//{
	//	sm_UiRender->DrawSkin(m_bkgskin,0,updateRect,UColor(255,255,255,m_Alpha));
	//}
	//else
	{
		float fAlpha = m_Alpha / 255.0f;
		sm_UiRender->DrawRectFill(updateRect, UColor(1, 1 ,1, 120 * fAlpha));
	}
	
	if (m_DrawDragborad)
	{
		URect rect;
		rect.top = offset.y;
		rect.bottom = offset.y + m_DragBoard;
		rect.left = offset.x;
		rect.right = offset.x + m_Size.x;
		sm_UiRender->DrawRectFill(rect,UColor(0,0,0,m_Alpha));
	}
	

	UControl::OnRender(offset,updateRect);
}
void UChatMsgShow::OnMouseMove(const UPoint& position, UINT flags)
{
	if (m_LockMouseMove)
	{
		ReleaseCapture();
		TipSystem->HideTip();
		m_LockMouseMove = FALSE;
	}
}
void UChatMsgShow::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	m_ptLeftBtnDown = pt;
	m_Bottom = GetWindowPos().y + GetWindowSize().y;
}
void UChatMsgShow::OnMouseDragged(const UPoint& pt, UINT uFlags)
{
	//if (Draginterect(m_ptLeftBtnDown))
	//{
	//	UPoint delta = pt - m_ptLeftBtnDown;
	//	m_ptLeftBtnDown = pt;
	//	UPoint newpos = GetWindowPos();
	//	UPoint oldSize = GetWindowSize();
	//	newpos.y += delta.y;
	//	if ( newpos.y > m_MinHSize  || newpos.y < m_MaxHSize )
	//	{
	//		return;
	//	}
	//	SetTop(newpos.y);
	//	SetHeight( m_Bottom - newpos.y);
	//	//ScrollText(WAY_BOTTOM);
	//}
}
void UChatMsgShow::ResizeControl(const UPoint &NewPos, const UPoint &newExtent)
{
	const UPoint& MinSize = m_MinSize;
	UPoint NewSize = UPoint(__max(MinSize.x, newExtent.x),
		__max(MinSize.y, newExtent.y));

	BOOL SizeChanged = (NewSize != m_Size);
	BOOL WidthChanged = (NewSize.x != m_Size.x);
	BOOL PosChanged = (NewPos != m_Position);
	if (!SizeChanged && !PosChanged ) 
		return;
	if (WidthChanged)
	{
		for (unsigned int i =0; i < m_Childs.size(); i++)
		{
			UControl* pChild = m_Childs[i];
			pChild->SetWidth(NewSize.x);
		}
	}
	if (SizeChanged)
	{
		m_Size = NewSize;
		OnSize(NewSize);
	}
	if( PosChanged )
	{
		UPoint Offset = NewPos - m_Position;
		m_Position = NewPos;
		OnMove(Offset);
	}
}
void UChatMsgShow::SetTop( INT newTop )
{
	//if ( newTop > m_MinHSize  || newTop < m_MaxHSize )
	//{
	//	return;
	//}
	static int OldTop = GetWindowPos().y;
	static int OldHeight = GetWindowSize().y;
	UControl::SetTop(newTop);
	SetHeight( OldHeight + OldTop - newTop );
}
void UChatMsgShow::OnMouseUp(const UPoint& pt, UINT uFlags)
{
}
void UChatMsgShow::OnMouseEnter(const UPoint& position, UINT flags)
{
	ChatSystem->ShowMsgBox();
}
void UChatMsgShow::OnMouseLeave(const UPoint& position, UINT flags)
{
	ChatSystem->HideMsgBox();
}
BOOL UChatMsgShow::Draginterect(UPoint pt)
{
	if (pt.y  > GetWindowPos().y && pt.y < GetWindowPos().y + m_DragBoard)
	{
		return TRUE;
	}
	return FALSE;
}
void UChatMsgShow::ScrollText(int scrollto)
{
	if (GetChildByID(0))
	{
		UChatRichEdit * cre = UDynamicCast(UChatRichEdit,GetChildByID(0));
		if (cre)
		{
			int EndY = GetHeight() - cre->GetContentHeight(); 
			BOOL mark = FALSE;
			switch (scrollto)
			{
			case WAY_BOTTOM:
				{
					cre->SetPosition(UPoint(0,EndY>0?0:EndY));
					//if (EndY>0)
						mark = TRUE;
					SetLockScroll(FALSE);
					break;
				}
			case WAY_DOWN:
				{
					if (cre->GetWindowPos().y + cre->GetContentHeight() > GetHeight())
					{
						cre->SetPosition(UPoint(cre->GetWindowPos().x,cre->GetWindowPos().y - cre->GetLineHeight()));
						mark = TRUE;
					}
					else
					{
						SetLockScroll(FALSE);
					}
					if (cre->GetWindowPos().y + cre->GetContentHeight() < GetHeight())
					{
						int height = GetHeight() - cre->GetContentHeight();
						cre->SetPosition(UPoint(cre->GetWindowPos().x,height<0?height:0));
						SetLockScroll(FALSE);
					}
					break;
				}
			case WAY_TOP:
				{
					if (cre->GetWindowPos() != UPoint(0,0))
					{
						cre->SetPosition(UPoint(0,0));
						mark = TRUE;
					}
					SetLockScroll(TRUE);
					break;
				}
			case WAY_UP:
				{
					if (cre->GetWindowPos() != UPoint(0,0))
					{
						cre->SetPosition(UPoint(cre->GetWindowPos().x,cre->GetWindowPos().y + cre->GetLineHeight()));
						mark = TRUE;
					}
					if (cre->GetWindowPos().y > 0)
					{
						cre->SetPosition(UPoint(0,0));
					}
					SetLockScroll(TRUE);
					break;
				}
			}
			if (ChatSystem)
			{
				if (mark)
					ChatSystem->CallRMD(FALSE);
			}
		}
	}
}

BOOL UChatMsgShow::OnMouseWheel(const UPoint& position, int delta , UINT flags)
{
	ScrollText(delta>0?WAY_UP:WAY_DOWN);
	return TRUE;
}
void UChatMsgShow::SetLockScroll(BOOL lock)
{
	m_LockScroll = lock;
}
void UChatMsgShow::ClearText()
{
	UChatRichEdit * cre = UDynamicCast(UChatRichEdit,GetChildByID(0));
	if (cre)
	{
		cre->SetText("",0);
	}
}
void UChatMsgShow::SetBKGAlpha(int Alpha)
{
	m_Alpha = Alpha;
}
inline int GetFlag(int msgType)
{
	if (msgType == CHAT_MSG_CITY)
	{
		return MSG_AREA;
	}else if (msgType == CHAT_MSG_WHISPER || msgType == CHAT_MSG_WHISPER_INFORM)
	{
		return MSG_WHISPER;
	}else if (msgType == CHAT_MSG_GUILD)
	{
		return MSG_GUILD;
	}else if (msgType == CHAT_MSG_PARTY || msgType == CHAT_MSG_RAID || msgType == CHAT_MSG_RAID_LEADER)
	{
		return MSG_PARTY;
	}else if (msgType == CHAT_MSG_SAY)
	{
		return MSG_SAY;
	}else if (msgType == CHAT_MSG_BATTLE)
	{
		return MSG_BATTLE;
	}else if (msgType == CHAT_MSG_SYSTEM)
	{
		return MSG_SYSTEM;
	}else if(msgType == CHAT_MSG_CLIENTSYSTEM)
	{
		return MSG_CLIENTSYSTEM;
	}else if (msgType > 0 && msgType < CHAT_MSG_MAX)
	{
		return MSG_OTHER;
	}
	return 0;
}
void UChatMsgShow::AppendText(int msgtype,const char * text)
{
#define Channel(str,color) "<left><shadowcolor:202020FF><shadow:1:1><color:"+color+">"+str+"<linkcolor:"+color+">"
	if (!ChatSystem)
		return ;
	UStringW _head;
	UChat::ChannelData * cd = ChatSystem->GetChannelData(msgtype);
	if (cd->MsgType != -1){
		_head.Set((cd->ChannelFont + Channel(cd->ChannelName,cd->ChannelColor)).c_str());
	}
	UChatRichEdit * cre = UDynamicCast(UChatRichEdit,GetChildByID(0));
	if (cre)
	{
		_head.Append(text);
		int l = _head.GetLength();

		for(int i = 0 ; i < CHAT_TABLE_NUM ; i++)
		{
			UChat::MsgStore* pStore = ChatSystem->GetMsgStore(i);
			if (pStore->IsVis(GetFlag(msgtype)))
			{
				pStore->MsgIndex.push_back(l);
				if (pStore->MsgIndex.size() > MAX_MESSNUM)
				{
					pStore->Msg.Cut(0, pStore->MsgIndex[0]);
					pStore->MsgIndex.pop_front();
				}
				pStore->Msg.Append(_head);
			}
			if (i == m_ShowType)
			{
				cre->SetText(pStore->Msg, pStore->Msg.GetLength());
			}
		}
		cre->SetHeight(cre->GetContentHeight());
	}
	if (!m_LockScroll)
		ScrollText(WAY_BOTTOM);
#undef Channel
}
void UChatMsgShow::OnURLClicked(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;
	if (strURL.length() < 6)
		return;
	string str = strURL.substr(6,strURL.length());
	switch (FindTag(strURL))
	{
	case TAG_NAME:
		{
			ChatSystem->SetCurrentChannel(CHAT_MSG_WHISPER);
			ChatSystem->SetPTalkPerson(str.c_str());
			ChatSystem->ShowInputBox();
		}
		break;
	case TAG_ITEM:
		{
			//ui32 _id = atoi(str.c_str());
			//TipSystem->ShowTip(TIP_TYPE_ITEM,_id,sm_System->GetCursorPos());
			ChatSystem->OnClickMsgItem(str);
			m_LockMouseMove = TRUE;
			CaptureControl();
		}
		break;
	case TAG_SPELL:
		{
			ChatSystem->OnClickMsgSpell(str);
			m_LockMouseMove = TRUE;
			CaptureControl();
		}
		break;
	case TAG_QUEST:
		{
			ui32 _id = atoi(str.c_str());
			TipSystem->ShowTip(TIP_TYPE_QUEST,_id,sm_System->GetCursorPos());
			m_LockMouseMove = TRUE;
			CaptureControl();
		}
		break;
	};
}
void UChatMsgShow::OnURLRClicked(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;
	string str = strURL.substr(6,strURL.length());
	switch (FindTag(strURL))
	{
	case TAG_NAME:
		{
			if (ChatSystem)
			{
				ChatSystem->CallRMD(TRUE,str.c_str());
			}
		}
		break;
	};
}
void UChatMsgShow::OnShifClicked(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;
	string str = strURL.substr(6,strURL.length());
	//UStringW CopyStr;
	//CopyStr.Set(str.c_str());
	switch (FindTag(strURL))
	{
	case TAG_NAME:
		{
			CopyString(str.c_str());
			ChatSystem->SendQueryPlayer(str);
		}
		break;
	case TAG_ITEM:
		{
			ChatSystem->AddItemMsg(str);
		}
		break;
	};
}
void UChatMsgShow::OnCtrlClicked(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;
	string str = strURL.substr(6,strURL.length());
	switch (FindTag(strURL))
	{
	case TAG_ITEM:
		{
			ChatSystem->OnFittingEquipment(str);
		}
		break;
	};
}
int UChatMsgShow::FindTag(const string str)
{
	if (str.find("Name_") == 1)
	{
		return TAG_NAME;
	}
	if (str.find("Item_") == 1)
	{
		return TAG_ITEM;
	}
	if (str.find("SPEL_") == 1)
	{
		return TAG_SPELL;
	}
	if (str.find("QUES_") == 1)
	{
		return TAG_QUEST;
	}
	return TAG_UNKNOW;
}
void UChatMsgShow::SetShowType(int ishow)
{
	m_ShowType = ishow;
	UChatRichEdit * cre = UDynamicCast(UChatRichEdit,GetChildByID(0));
	if (cre)
	{
		UChat::MsgStore* pStore = ChatSystem->GetMsgStore(m_ShowType);
		cre->SetText(pStore->Msg, pStore->Msg.GetLength());
		cre->SetHeight(cre->GetContentHeight());
		ScrollText(WAY_BOTTOM);
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////
UChatRichEdit::UChatRichEdit(void)
{

}
UChatRichEdit::~UChatRichEdit(void)
{

}
BOOL UChatRichEdit::OnCreate()
{
	if (!URichTextCtrl::OnCreate())
	{
		return FALSE;
	}
	SetHeight(m_Style->m_FontSize * 100);
	return TRUE;
}
void UChatRichEdit::OnMouseEnter(const UPoint& position, UINT flags)
{
	if (GetParent())
	{
		UChatMsgShow * cms = UDynamicCast(UChatMsgShow,GetParent());
		if (cms)
		{
			cms->OnMouseEnter(position,flags);
		}
	}
}
void UChatRichEdit::OnMouseLeave(const UPoint& position, UINT flags)
{
	if (GetParent())
	{
		UChatMsgShow * cms = UDynamicCast(UChatMsgShow,GetParent());
		if (cms)
		{
			cms->OnMouseLeave(position,flags);
		}
	}
}
void UChatRichEdit::OnMouseMove(const UPoint& position, UINT flags)
{
	if (GetParent())
	{
		UChatMsgShow * cms = UDynamicCast(UChatMsgShow,GetParent());
		if (cms)
		{
			cms->OnMouseMove(position,flags);
		}
	}
}
void UChatRichEdit::OnSize(const UPoint& NewSize)
{
	URichTextCtrl::OnSize(NewSize);
}
void UChatRichEdit::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{	
	if (GetParent())
	{
		UChatMsgShow * cms = UDynamicCast(UChatMsgShow,GetParent());
		if (cms)
		{	
			URichTextCtrl::OnMouseDown(pt,nClickCnt,uFlags);
			cms->OnMouseDown(pt,nClickCnt,uFlags);
		}
	}
}
void UChatRichEdit::OnMouseDragged(const UPoint& pt, UINT uFlags)
{
	if (GetParent())
	{
		UChatMsgShow * cms = UDynamicCast(UChatMsgShow,GetParent());
		if (cms)
		{
			cms->OnMouseDragged(pt,uFlags);
		}
	}
}
void UChatRichEdit::OnMouseUp(const UPoint& pt, UINT uFlags)
{
	if (GetParent())
	{
		UChatMsgShow * cms = UDynamicCast(UChatMsgShow,GetParent());
		if (cms)
		{	
			URichTextCtrl::OnMouseUp(pt,uFlags);
			cms->OnMouseUp(pt,uFlags);
		}
	}
}