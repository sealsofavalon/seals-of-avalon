//-----------------------------------------------------------------------------------
//聊天系统中的输出控件
//-----------------------------------------------------------------------------------
#ifndef UCHATMSGSHOW_H
#define UCHATMSGSHOW_H
#include "UInc.h"

#define MAX_MESSNUM  100

class UChatRichEdit : public URichTextCtrl
{
	UDEC_CLASS(UChatRichEdit);
public:
	UChatRichEdit(void);
	~UChatRichEdit(void);
public:
	virtual BOOL OnCreate();
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnMouseDragged(const UPoint& pt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);	
	virtual void OnSize(const UPoint& NewSize);
};
class UChatMsgShow : public UControl
{
	UDEC_CLASS(UChatMsgShow);
	UDEC_MESSAGEMAP();
public:
	UChatMsgShow(void);
	~UChatMsgShow(void);
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnMouseDragged(const UPoint& pt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
	virtual BOOL OnMouseWheel(const UPoint& position, int delta , UINT flags);
	virtual void ResizeControl(const UPoint &NewPos, const UPoint &newExtent);
	virtual void SetTop( INT newTop );
public:
	void SetBKGAlpha(int Alpha);
	BOOL Draginterect(UPoint pt);
	void ScrollText(int scrollto);
	void AppendText(int msgtype,const char * text);
	void SetLockScroll(BOOL lock);
	void ClearText();

	//用于点击
public:
	enum eURLKind
	{
		TAG_NAME,//点到名字
		TAG_QUEST,//任务
		TAG_ITEM,//物品
		TAG_SPELL,//技能
		TAG_UNKNOW
	};
	void SetShowType(int ishow);
private:
	void OnURLClicked(UNM* pNM);
	void OnURLRClicked(UNM* pNM);
	void OnShifClicked(UNM* pNM);
	void OnCtrlClicked(UNM* pNM);
	int FindTag(const string str);
private:
	int m_DragBoard;
	USkinPtr m_bkgskin;
	UPoint m_ptLeftBtnDown;
	int m_Alpha;
	int m_Bottom;
	BOOL m_DrawDragborad;
	BOOL m_LockScroll;
	BOOL m_LockMouseMove;
	int m_ShowType;
};
#endif