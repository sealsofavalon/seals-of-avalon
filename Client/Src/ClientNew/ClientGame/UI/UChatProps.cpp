#include "stdafx.h"
#include "UChatProps.h"
#include "UMessageBox.h"
#include "UChat.h"
#include "UFun.h"


#define  CHATPROPSCOUNT 100

#define  QuestEffectTime 5.0f

UIMP_CLASS(UDALABA, UControl)
void UDALABA::StaticInit()
{

}

UDALABA::UDALABA()
{
	m_EffectTime = 0.f;
	m_pkEffectOBJ = NULL;
}

UDALABA::~UDALABA()
{

}

BOOL UDALABA::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (!m_pkEffectOBJ)
	{
		m_pkEffectOBJ = UDynamicCast(UNiAVControlEffRe, GetChildByID(2));
		if (!m_pkEffectOBJ)
		{
			return FALSE;
		}
	}

	return TRUE ;
}

void UDALABA::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);
	if (m_pkEffectOBJ->IsVisible())
	{
		m_EffectTime += fDeltaTime;
		if (m_EffectTime < QuestEffectTime)
		{
			m_pkEffectOBJ->OnUpdateOBJ(m_EffectTime);
		}else
		{
			m_pkEffectOBJ->SetVisible(FALSE);
		}
	}else
	{
		m_EffectTime = 0.0f;
	}
}

void UDALABA::AddNewTitle()
{
	m_EffectTime = 0.0f;
	m_pkEffectOBJ->SetVisible(TRUE);
	m_pkEffectOBJ->SetObjectFile("Data\\UI\\Chat\\Title.nif");
	GetParent()->MoveChildTo(this, NULL);
}

void UDALABA::AddQuestMsg(ui32 stat)
{
	string text = "";
	switch(stat)
	{
	case GetNewQuest:
		text = "Data\\UI\\Chat\\GetNewQuest.nif";
		break;
	case CanFinishQuest:
		text = "Data\\UI\\Chat\\CanFinishQuest.nif";
		break;
	case FinishQuest:
		text = "Data\\UI\\Chat\\FinishQuest.nif";
		break;
	case DropQuest:
		text = "Data\\UI\\Chat\\DropQuest.nif";
		break ;
	case FaildQuest:
		text = "Data\\UI\\Chat\\FaildQuest.nif";
		break;
	}

	m_EffectTime = 0.0f;
	m_pkEffectOBJ->SetVisible(TRUE);
	m_pkEffectOBJ->SetObjectFile(text.c_str());
	GetParent()->MoveChildTo(this, NULL);
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UChatShowDalaba,URichTextCtrl)
void UChatShowDalaba::StaticInit()
{

}
UChatShowDalaba::UChatShowDalaba()
{
	m_text = "";
	m_TextList.clear();
}
UChatShowDalaba::~UChatShowDalaba()
{
	m_text = "";
	m_TextList.clear();
}

BOOL UChatShowDalaba::OnCreate()
{
	if (!URichTextCtrl::OnCreate())
	{
		return FALSE;
	}
	m_TextList.clear();
	m_text = "";

	return TRUE;
}
void UChatShowDalaba::OnDestroy()
{
	ClearText();
	m_TextList.clear();
	URichTextCtrl::OnDestroy();
}

void UChatShowDalaba::OnTimer(float fDeltaTime)
{
	URichTextCtrl::OnTimer(fDeltaTime);
	if (m_TextList.size())
	{
		UpdataDalabaText(fDeltaTime);
	}
}
#define  ChatDalabaTextLife 10.0f //冒泡的时间
#define  ChatDalabaCount 5  //
#define  ChatDalabaSpeed 4.0f
#define  ChatDalabaTextColor 0xFFFF6600

void UChatShowDalaba::AddDalabaText(string pkStr)
{
	DalabaText NewText;
	NewText.Color = ChatDalabaTextColor;
	NewText.Life = ChatDalabaTextLife;
	NewText.Age = ChatDalabaTextLife;

	//int nRet = MultiByteToWideChar(CP_ACP, 0, pText, strlen(pText), NewText.wszText, 128 -1);
	if (pkStr.length())
	{
		NewText.wszText = pkStr;
		
		NewText.StrLen = pkStr.length();
		m_TextList.push_back(NewText);
		
		if (m_TextList.size() > ChatDalabaCount)
		{
			
			m_TextList.erase(m_TextList.begin());

			std::vector<DalabaText>::iterator it = m_TextList.begin();
			while (it != m_TextList.end())
			{
				DalabaText& textEntry = *it;
				textEntry.Age = textEntry.Life;
				++it;
			}
		}
	}
}
void UChatShowDalaba::ResetPos()
{
	if (GetParent())
	{
		int pkH = GetParent()->GetHeight();
		int IH = pkH - GetContentHeight();
		SetPosition(UPoint(0,IH>0?0:IH));
	}
	
}
void UChatShowDalaba::OnRender(const UPoint& offset, const URect &updateRect)
{
	//ResetDalabaText();
	if (m_wstrBuffer.Empty())
	{
		return ;
	}
	
	URichTextCtrl::OnRender(offset,updateRect);
}
void UChatShowDalaba::UpdataDalabaText(float fDeltaTime)
{  
	string TemStr = "";

	std::vector<DalabaText>::iterator it = m_TextList.begin();
	while (it != m_TextList.end())
	{
		DalabaText& textEntry = *it;
		if (textEntry.Life > 0.0f)
		{
			textEntry.Life -= fDeltaTime; 

			if (textEntry.Life > 0)
			{
				string tem;
				tem = TemStr +  textEntry.wszText ;

				TemStr = tem;
			}
		}
		++it;
	}

	

	if (TemStr.length())
	{
		if (TemStr != m_text)
		{
			m_text = TemStr;
			SetText(m_text.c_str(), m_text.length());
		}	
	}else
	{
		m_text = "";
		ClearText();
	}

	ResetPos();	
}

//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UChatPropsMsg,UDialog);
UBEGIN_MESSAGE_MAP(UChatPropsMsg, UDialog)
UON_UEDIT_UPDATE(0, &UChatPropsMsg::OnClickSend)
UON_BN_CLICKED(1, &UChatPropsMsg::OnClickSend)
UON_BN_CLICKED(2, &UChatPropsMsg::OnClickCancel)
UEND_MESSAGE_MAP()
void UChatPropsMsg::StaticInit()
{

}

UChatPropsMsg::UChatPropsMsg()
{
	m_PropsType = PROPS_B;
	m_pkEdit = NULL;
}
UChatPropsMsg::~UChatPropsMsg()
{

}

BOOL UChatPropsMsg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	if (m_pkEdit == NULL)
	{
		m_pkEdit = UDynamicCast(UChatEdit,GetChildByID(0));
	}
	if (m_pkEdit == NULL)
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;
	m_pkEdit->SetMaxLength(CHATPROPSCOUNT);
	return TRUE;
}
void UChatPropsMsg::OnDestroy()
{
	UDialog::OnDestroy();
}
BOOL UChatPropsMsg::OnEscape()
{
	if(!UControl::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}
void UChatPropsMsg::OnClose()
{
	if (m_pkEdit)
	{
		m_pkEdit->ClearFocus();
		m_pkEdit->Clear();
	}

	SetFocusControl(sm_System->GetCurFrame());
	SetVisible(FALSE);
}
void UChatPropsMsg::OnClickSend()
{
	//send msg
	
	char Buf[2048];
	if (m_pkEdit)
	{
		UINT count = m_pkEdit->GetText(Buf, 2048);
		if (!count)
		{
			TraceLastError();
			UMessageBox::MsgBox_s(_TRAN("请输入消息"));
			return ;
		}
	}

	UINT msgT ;
	if (m_PropsType == PROPS_B)
	{
		msgT = CHAT_MSG_DALABA ;
	}else
	{
		msgT = CHAT_MSG_XIAOLABA;
	}
	ChatSystem->SendChatMsg(msgT, Buf);
	OnClose();
}
void UChatPropsMsg::OnClickCancel()
{
	//
	OnClose();
}
void UChatPropsMsg::SetChatPropsType(UINT ty)
{
	SetVisible(TRUE);
	m_PropsType = ty;

	UStaticText* pkTitle = UDynamicCast(UStaticText, GetChildByID(3));
	if (pkTitle)
	{
		if (m_PropsType == PROPS_B)
		{
			pkTitle->SetText(_TRAN("全服喊话"));
		}

		if (m_PropsType == PROPS_S)
		{
			pkTitle->SetText(_TRAN("阵营喊话"));
		}
	}
	
	if (m_pkEdit)
	{
		m_pkEdit->SetFocusControl();
	}
}

void UChatPropsMsg::AppChatMsg(const char* msg)
{
	m_pkEdit->InsertText(msg);
}