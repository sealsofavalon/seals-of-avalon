#ifndef __UCHATPROPS_H__
#define __UCHATPROPS_H__

#include "UInc.h"
#include "UEditExt.h"
#include "UNiAVControl.h"

class UDALABA : public UControl
{
	UDEC_CLASS(UDALABA)
public:
	UDALABA();
	~UDALABA();
	void AddQuestMsg(ui32 stat);
	void AddNewTitle();
protected:
	virtual BOOL OnCreate();
	virtual void OnTimer(float fDeltaTime);
	virtual BOOL HitTest(const UPoint& parentCoordPoint)
	{
		return FALSE ;
	}
private:
	float m_EffectTime ;
	UNiAVControlEffRe* m_pkEffectOBJ ;
};

class UChatShowDalaba : public URichTextCtrl
{
	UDEC_CLASS(UChatShowDalaba);
public:
	UChatShowDalaba();
	~UChatShowDalaba();

	struct DalabaText
	{
		float Depth;
		int StrLen;
		string wszText;
		UColor Color;
		float Life;
		float Age ;
	};

	void AddDalabaText(string pkStr);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	void ResetPos();
	virtual BOOL HitTest(const UPoint& parentCoordPoint)
	{
		return FALSE;
	}
	void UpdataDalabaText(float fDeltaTime);
protected:
	std::vector<DalabaText> m_TextList;
	string m_text;
	
};

enum 
{
	PROPS_B = 0,  // ������
	PROPS_S,   //С����
};

class UChatPropsMsg : public UDialog 
{
	UDEC_CLASS(UChatPropsMsg);
	UDEC_MESSAGEMAP();
public:
	UChatPropsMsg();
	~UChatPropsMsg();

	void SetChatPropsType(UINT ty);
	void AppChatMsg(const char* msg);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
protected:
	void OnClickSend();
	void OnClickCancel();
private:
	UINT m_PropsType ;
	UChatEdit* m_pkEdit;
};
#endif