#ifndef UCOMM_H
#define UCOMM_H

struct ComData 
{

};
class URespondCommand
{
public:
	void Test();
	void HAHA();
	void QuitGroup();
	void CreateGuild();
	void AskRoster();
	void AskQuery();
	void OnComm(ComData * CD = NULL);
	static URespondCommand * sm_resfun;
};
#define RespondFun URespondCommand::sm_resfun 
typedef void(URespondCommand::*ptrtype)();
#define ResComm(FxnName) (ptrtype)static_cast< void (URespondCommand::*)(ComData*) >(FxnName)
#endif