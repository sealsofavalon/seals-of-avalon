#include "StdAfx.h"
#include "UDynamicIconButtonEx.h"
#include "UIItemSystem.h"
#include "skill/SkillManager.h"
#include "ItemManager.h"
#include "ObjectManager.h"
#include "Player.h"
#include "ItemStorage.h"
#include "ItemSlot.h"
#include "UChat.h"
#include "AudioDef.h"
#include "UFittingRoom.h"
#include "UIIndex.h"
#include "UIGamePlay.h"

UIMP_CLASS(UDynamicIconButtonEx, UButton)

void UDynamicIconButtonEx::StaticInit()
{
	UREG_PROPERTY("BackSkin", UPT_STRING, UFIELD_OFFSET(UDynamicIconButtonEx, mBackSkinPath));
}
UDynamicIconButtonEx::UDynamicIconButtonEx()
{
	m_BeginDrag = FALSE;
	m_TypeID = UItemSystem::ICT_INVLALID;
	m_IconIndex = 0;
	m_IconSkin = NULL;
	m_BackGSkin = NULL;
	m_IsTickable = TRUE;

	m_WaringTexture = NULL;
	m_IconOffset = 2;

	m_IsJustCD = FALSE;
	m_bInDrag = FALSE;
	m_bKeep = FALSE;
	m_bAcceptDrag = FALSE;
	m_bAcceptReDrag = FALSE;
	mAutoAttackLastFlipped = 0.f;
}
UDynamicIconButtonEx::~UDynamicIconButtonEx()
{
	m_BeginDrag = FALSE;
	m_TypeID = UItemSystem::ICT_INVLALID;
	m_IconIndex = 0;
	m_IconSkin = NULL;
	m_BackGSkin = NULL;
	m_IsTickable = TRUE;
	m_bAcceptDrag = FALSE;
	m_bAcceptReDrag = FALSE;
	ClearActionItem();
}
BOOL UDynamicIconButtonEx::OnCreate()
{
	if (!UButton::OnCreate())
	{
		return FALSE;
	}
	if (m_BackGSkin == NULL && mBackSkinPath.GetBuffer())
	{	
		m_BackGSkin = sm_System->LoadSkin(mBackSkinPath.GetBuffer());
	}
	if (m_BackGSkin == NULL)
	{
		//m_BackGSkin = sm_System->LoadSkin(Default);
	}

	//m_WaringTexture = sm_UiRender->LoadTexture("DATA\\UI\\PUBLIC\\LOCK.png");
	m_WaringTexture = sm_UiRender->LoadTexture("DATA\\UI\\UPLUG\\GroupBkg.png");
	m_OldSize = m_Size;
	return TRUE;
}
void UDynamicIconButtonEx::OnDestroy()
{
	return UButton::OnDestroy();
}
void UDynamicIconButtonEx::OnRender(const UPoint& offset, const URect &updateRect)
{
	DrawPushButton(offset,updateRect);
	RenderChildWindow(offset,updateRect);
	OnRenderGlint(offset, updateRect);
}

bool UDynamicIconButtonEx::SpecMethod(UINT uFlags)
{
	if (uFlags == LKM_SHIFT)
	{
		if (GetItemData()->type == ActionButton_Type_Spell)
		{
			ChatSystem->AddSpellMsg(GetItemData()->entry);
			return true;
		}
		if (GetItemData()->type == ActionButton_Type_Item)
		{
			if (GetItemData()->Data)
			{
				ChatSystem->AddItemMsg(GetItemData()->entry, (ItemExtraData*)GetItemData()->Data);
			}
			else
				ChatSystem->AddItemMsg(GetItemData()->entry, GetItemData()->Guid);
			return true;
		}
	}else if (uFlags == LKM_CTRL)
	{
		if (GetItemData()->type == ActionButton_Type_Item)
		{
			UFittingRoom* pFittingRoom = INGAMEGETFRAME(UFittingRoom, FRAME_IG_FITTINGROOMDLG);
			if(!pFittingRoom)
			return true;
			if (GetItemData()->Data)
				pFittingRoom->FittingEquipment(GetItemData()->entry, (ItemExtraData*)GetItemData()->Data);
			else
				pFittingRoom->FittingEquipment(GetItemData()->entry, GetItemData()->Guid);
			return true;
		}
	}
	return false;
}

void UDynamicIconButtonEx::OnMouseDown(const UPoint &pt, UINT nClickCnt, UINT uFlags)
{
	if (m_ActionContener.IsCreate())
	{
		ActionDataInfo * pData = GetItemData();
		if (pData->entry != 0 && pData->type == ActionButton_Type_Spell)
		{
				UINT pSkillTest = SYState()->SkillMgr->TestSkillState(pData->entry);
				if (pSkillTest == AttributePassive)
					return;
		}

		CaptureControl(this);
		if (m_Active)
		{
			m_ActionContener.Press(MOUSEDOWN_LEFT);
		}
		if (!m_ActionContener.IsItemEmpty())
		{
			m_BeginDrag = TRUE;
			m_OldCursePT = pt;
		}
	}
}
void UDynamicIconButtonEx::OnMouseUp(const UPoint &pt, UINT uFlags)
{
	if (m_ActionContener.IsCreate() && m_Active)
	{
		ActionDataInfo * pData = GetItemData();
		if (pData->entry != 0 && pData->type == ActionButton_Type_Spell)
		{
			UINT pSkillTest = SYState()->SkillMgr->TestSkillState(pData->entry);
			if (pSkillTest == AttributePassive)
				return;
		}
		ReleaseCapture();
		bool bUse = true;
		bUse &= !SpecMethod(uFlags);
		m_ActionContener.Release(MOUSEUP_LEFT, bUse);
		m_BeginDrag = FALSE;

		mBGlint = FALSE ;
	}
}
void UDynamicIconButtonEx::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	if (m_ActionContener.IsCreate())
	{
		m_bPressed = TRUE;
		ActionDataInfo * pData = GetItemData();
		if (pData->entry != 0 && pData->type == ActionButton_Type_Spell)
		{
			UINT pSkillTest = SYState()->SkillMgr->TestSkillState(pData->entry);
			if (pSkillTest == AttributePassive)
				return;
		}
		CaptureControl(this);
		if (!m_ActionContener.IsItemEmpty())
		{
			m_ActionContener.Press(MOUSEDOWN_RIGHT);
		}
	}
}
void UDynamicIconButtonEx::OnRightMouseUp(const UPoint& position, UINT flags)
{
	if (m_ActionContener.IsCreate())
	{
		ActionDataInfo * pData = GetItemData();
		if (pData->entry != 0 && pData->type == ActionButton_Type_Spell)
		{
			UINT pSkillTest = SYState()->SkillMgr->TestSkillState(pData->entry);
			if (pSkillTest == AttributePassive)
				return;
		}
		ReleaseCapture();
		if (m_bPressed)
		{
			DispatchNotify(UBN_RCLICKED);
		}
		m_bPressed = FALSE;
		m_ActionContener.Release(MOUSEUP_RIGHT);

		mBGlint = FALSE ;
	}
}
UINT UDynamicIconButtonEx::GetDataFlag()
{
	return m_TypeID;
}
void UDynamicIconButtonEx::SetActionItem(UActionItem * aitem)
{
	if (aitem)
	{
		if (!m_ActionContener.IsCreate())
		{
			m_ActionContener.SetActionItem(aitem);
		}
	}
}
void UDynamicIconButtonEx::SetItemData(ActionDataInfo &aiteminfo)
{
	if (m_ActionContener.IsCreate())
	{
		m_ActionContener.SetDataInfo(aiteminfo);
		m_IconSkin = m_ActionContener.GetIconSkin(m_IconIndex);
	}
}
void UDynamicIconButtonEx::SetItemData(UActionContener &contener)
{
	ActionDataInfo datainfo;
	datainfo.entry = contener.GetDataInfo()->entry;
	datainfo.pos = GetItemData()->pos;
	datainfo.type = contener.GetDataInfo()->type;
	SetItemData(datainfo);
}
ActionDataInfo * UDynamicIconButtonEx::GetItemData()
{
	if (m_ActionContener.IsCreate())
	{
		return m_ActionContener.GetDataInfo();
	}
	return NULL;
}
void UDynamicIconButtonEx::SetType(ui32 type)
{
	m_TypeID = type;
}
void UDynamicIconButtonEx::SetIconOffset(INT offset)
{
	m_IconOffset = offset;
}
void UDynamicIconButtonEx::ClearActionItem()
{
	if (m_ActionContener.IsCreate())
	{
		m_ActionContener.ClearActionItem();
	}
}
void UDynamicIconButtonEx::OnTimer(float fDeltaTime)
{
	if (m_ActionContener.IsCreate() && GetItemData()->entry != 0 && m_IsTickable)
	{
		ui32 RemainTime;
		ui32 RecoverTime;
		ui32 pSpellID = 0;
		if (GetItemData()->type == ActionButton_Type_Spell)
		{
			pSpellID = GetItemData()->entry;
		}
		if (GetItemData()->type == ActionButton_Type_Item)
		{
			ui32 entry = m_ActionContener.GetDataInfo()->entry; 

			ItemPrototype_Client* pItem  = ItemMgr->GetItemPropertyFromDataBase(entry);
			if (pItem)
			{
				for (int i= 0; i < 5; i++)
				{
					if (pItem->Spells[i].Id != 0)
					{
						pSpellID  = pItem->Spells[i].Id;
						break;
					}
				}
			}   
		}

		int i = 0;
		if(!pSpellID)
			return;

		mAutoAttackLastFlipped += (fDeltaTime*0.5f);
		if (mAutoAttackLastFlipped > 25)
			mAutoAttackLastFlipped = 0;
		UINT pIndex = UINT(mAutoAttackLastFlipped * 60 / 25);
		CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
		if (pLocal)
		{
			m_ActionContener.SetAutoCast(pLocal->IsSpellAutoCast(pSpellID));
			m_ActionContener.SetAutoMaskShow(pIndex % 2);
		}
		if (pLocal && pLocal->IsSpellCast(pSpellID))
		{
			m_ActionContener.BeginCast();
		}
		else
		{
			m_ActionContener.EndCast();
		}

		if ( pLocal && pLocal->isMount() )
		{
			uint32 idSpell = 0;
			pLocal-> GetMountSpell( idSpell );
			if ( idSpell == pSpellID )
			{
				m_ActionContener.BeginCast();
			}
			else
				m_ActionContener.EndCast();
		}


		if ( pLocal && pLocal->isInShapShift() )
		{
			uint32 idSpell = 0;
			pLocal->GetShapeSpell( idSpell );
			if ( idSpell == pSpellID )
			{
				m_ActionContener.BeginCast();
			}
			else
				m_ActionContener.EndCast();
		}

		if(SYState()->SkillMgr->GetCooldownTime(pSpellID, RecoverTime, RemainTime))
		{
			if (RecoverTime)
			{
				m_ActionContener.BeginTick();
				m_ActionContener.CDTickTime(((float)(RecoverTime - RemainTime) * 0.001f),
					(float)(RecoverTime * 0.001f));
				i++;

				//m_IsJustCD = TRUE ;
			}

		}else
		{
			if (m_IsJustCD)
			{
				mBGlint = TRUE;
				m_IsJustCD = FALSE ;
			}
			m_ActionContener.CDTickTime(0.0f, 0.0f);
		}
		if (SYState()->SkillMgr->GetGlobalCooldownTime(pSpellID, RecoverTime, RemainTime))
		{
			m_ActionContener.BeginTick();
			float age = ((float)(RecoverTime - RemainTime) * 0.001f);
			float life = (float)(RecoverTime * 0.001f);
			m_ActionContener.PubTickTime(age, life);
			i++;
		}else
		{
			m_ActionContener.PubTickTime(0.0f, 0.0f);
		}
		if (i == 0)
		{
			m_ActionContener.EndTick();
		}
	}else
	{
		if (m_ActionContener.IsCreate())
		{
			m_ActionContener.CDTickTime(0.0f, 0.0f);
			m_ActionContener.PubTickTime(0.0f, 0.0f);
		}
	}
	UControl::OnTimer(fDeltaTime);
}

BOOL UDynamicIconButtonEx::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (nDataFlag == UItemSystem::ICT_INVLALID || nDataFlag == UItemSystem::ICT_INVALID_POS || !m_Active)
	{
		return FALSE;
	}
	return TRUE;
}
void UDynamicIconButtonEx::OnDragDropReject(UControl* pRejectCtrl, const void* pDragData, UINT nDataFlag)
{
	EndDragDrop();
}
void UDynamicIconButtonEx::OnDragDropAccept(UControl* pAcceptCtrl,const UPoint& point,const void* pDragData, UINT nDataFlag)
{
	EndDragDrop();
	if (m_ActionContener.GetDataInfo()->type == UItemSystem::ITEM_DATA_FLAG)
	{
		sm_System->__PlaySound(UI_moveitem);
	}
	if (m_ActionContener.GetDataInfo()->type == UItemSystem::SPELL_DATA_FLAG)
	{
		sm_System->__PlaySound(UI_dropSkill);
	}
	//if (pAcceptCtrl == this || !m_bAcceptReDrag)
	//	return;
	//WORD* pPos = new WORD;
	//*pPos = (WORD)m_ActionContener.GetDataInfo()->pos;
	//BeginDragDrop(this,m_IconSkin,m_IconIndex,pPos,m_TypeID);
	//m_ActionContener.BeginDrag();
}

void UDynamicIconButtonEx::OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag)
{
	m_bAcceptDrag = TRUE;
}

void UDynamicIconButtonEx::OnDeskTopDragEnd(UControl* pDragFrom, const void* pDragData, UINT nDataFlag)
{
	m_bAcceptDrag = FALSE;
}

void UDynamicIconButtonEx::OnDragDropBegin(const void* pDragData, UINT nDataFlag)
{
	m_bInDrag = (!m_bKeep) & TRUE;
}
void UDynamicIconButtonEx::OnDragDropEnd(UControl* pDragTo, void* pDragData, UINT nDataFlag)
{
	UControl::OnDragDropEnd(pDragTo,pDragData,nDataFlag);
	WORD* pPos = (WORD*)pDragData;
	delete pPos;
	pPos = NULL;
	
	m_bInDrag = FALSE;
}
void UDynamicIconButtonEx::OnMouseDragged(const UPoint& position, UINT flags)
{	
	UPoint _pos = WindowToScreen(UPoint(0,0));
	URect IconRect(_pos,m_Size);
	if (m_BeginDrag && !m_ActionContener.IsItemEmpty() && m_ActionContener.IsCreate() && !IconRect.PointInRect(position))
	{
		WORD* pPos = new WORD;
		*pPos = (WORD)m_ActionContener.GetDataInfo()->pos;
		BeginDragDrop(this,m_IconSkin,m_IconIndex,pPos,m_TypeID);
		if (m_ActionContener.GetDataInfo()->type == UItemSystem::ITEM_DATA_FLAG)
		{
			sm_System->__PlaySound(UI_moveitem);
		}
		if (m_ActionContener.GetDataInfo()->type == UItemSystem::SPELL_DATA_FLAG)
		{
			sm_System->__PlaySound(UI_dragSkill);
		}
		m_ActionContener.BeginDrag();
		m_BeginDrag = FALSE;
	}
}
void UDynamicIconButtonEx::OnMouseMove(const UPoint& position, UINT flags)
{
	UControl::OnMouseMove(position,flags);
}
void UDynamicIconButtonEx::OnMouseEnter(const UPoint& pt, UINT uFlags)
{
	UControl::OnMouseEnter(pt, uFlags);
	if (m_ActionContener.IsCreate() && m_Active)
	{
		m_ActionContener.InContener();
		if(!m_bInDrag)
			OnRenderToolTip();
	}
}
void UDynamicIconButtonEx::OnMouseLeave(const UPoint& pt, UINT uFlags)
{
	UControl::OnMouseLeave(pt, uFlags);
	if (m_ActionContener.IsCreate())
	{
		m_ActionContener.OutContener();
		OnHideToolTip();
	}
}
void UDynamicIconButtonEx::OnRenderToolTip()
{
	UPoint t_pt = GetParent()->WindowToScreen(GetWindowPos());
	if (m_ActionContener.IsCreate())
	{
		t_pt += m_Size;
		m_ActionContener.DrawToolTip(t_pt,m_TypeID);
	}
}
void UDynamicIconButtonEx::OnHideToolTip()
{
	if (m_ActionContener.IsCreate())
	{
		m_ActionContener.ReMoveToolTip();
	}
}
BOOL UDynamicIconButtonEx::renderTooltip(const UPoint& cursorPos, const char* tipText /* = NULL */ )
{
	if (m_ActionContener.IsItemEmpty())
	{
		UControl::renderTooltip(cursorPos, tipText);
	}
	return FALSE;
}
void UDynamicIconButtonEx::buildAcceleratorMap()
{
	UControl::buildAcceleratorMap();
	//mKeyCodeStr
	if (mAcceleratorKey.GetLength() > 0)
	{
		std::string buf = mAcceleratorKey.GetBuffer();
		int CodeLength = buf.find(';',0);
		std::string opCode = buf.substr(0,CodeLength);
		UAccelarKey * ackey = sm_System->FindAcceleratorKey(opCode.c_str());
		if (ackey)
		{	
			std::string Modify;
			if (ackey->mAccelarKeyModify.GetBuffer())
			{
				Modify = ackey->mAccelarKeyModify.GetBuffer();
			}
			if (ackey->mAccelarKeyCode.GetBuffer())
			{
				mKeyCodeStr = ackey->mAccelarKeyCode.GetBuffer();
				if (mKeyCodeStr.find("NULL") == -1)
				{
					if (Modify.size())
					{		
						if (Modify.find("LCONTROL") != -1)
						{
							mKeyCodeStr = "C+" + mKeyCodeStr;
						}
						if (Modify.find("LALT") != -1)
						{
							mKeyCodeStr = "A+" + mKeyCodeStr;
						}
						if (Modify.find("LSHIFT") != -1)
						{
							mKeyCodeStr = "S+" + mKeyCodeStr;
						}
					}
				}
			}
		}
	}
}
void UDynamicIconButtonEx::DrawPushButton(const UPoint& offset, const URect& ClipRect)
{
	URect rect(offset,m_Size);
	if (m_BackGSkin)
		sm_UiRender->DrawSkin(m_BackGSkin,m_Active?BACKSKIN_ACTIVE:BACKSKIN_INACTIVE,rect);

	if(m_bInDrag)
	{
		if (m_ActionContener.IsCreate())
			m_ActionContener.DrawActionContener(sm_UiRender,offset,ClipRect);
		return;
	}
	URect IconRect( m_IconOffset, m_IconOffset, m_OldSize.x - m_IconOffset, m_OldSize.y - m_IconOffset);
	IconRect.TransformRect(m_OldSize,m_Size);
	IconRect.Offset(offset);

	if (m_IconSkin)
		sm_UiRender->DrawSkin(m_IconSkin,m_IconIndex,IconRect);

	int textH = m_Style->m_spFont->GetHeight();

	CPlayer* pLcalPlayer = ObjectMgr->GetLocalPlayer();
	if (!pLcalPlayer)
		return;

	if (m_ActionContener.IsCreate())
	{
		if (m_bAcceptDrag && m_ActionContener.IsItemEmpty())
		{
			sm_UiRender->DrawImage(m_WaringTexture, IconRect, LCOLOR_ARGB(255, 0, 255, 0));
		}

		m_ActionContener.DrawActionContener(sm_UiRender, offset, ClipRect);
		m_ActionContener.DrawTickTime(sm_UiRender, IconRect);
		//如果是药品类。。。。要实时更新数量
		ActionDataInfo * pData = GetItemData();
		
		USetTextEdge(TRUE);
		if (pData->type == ActionButton_Type_Item)
		{
			if (pData->entry != 0 )
			{
				CStorage* pBagContainer = pLcalPlayer->GetItemContainer();
				CItemSlot* pSlot = (CItemSlot*)pBagContainer->GetSlotByEntry(pData->entry);
				UPoint txtPos(offset.x, offset.y + m_Size.y - textH);
				UPoint txtSize(IconRect.GetWidth(), textH);
				if (pSlot && m_TypeID != UItemSystem::ICT_PLAYER_TRADE_TENNET)
				{
					ItemPrototype_Client* pItem = pSlot->GetItemProperty();
					if (pItem && (pItem->Class == ITEM_CLASS_CONSUMABLE || pItem->Class == ITEM_CLASS_USE || pItem->Class == ITEM_CLASS_RECIPE))
					{
						UINT Count = pBagContainer->GetItemCnt(pData->entry);
						if (pItem->MaxCount - 1)
						{
							char buf[10];
							NiSprintf(buf,10,"%d",Count);
							UDrawText(sm_UiRender, m_Style->m_spFont, txtPos, txtSize, LCOLOR_WHITE, buf, UT_RIGHT);
						}
					}
				}else
				{
					ItemPrototype_Client* pItem = ItemMgr->GetItemPropertyFromDataBase(pData->entry);
					if(GetItemData()->num)
					{
						char buf[10];
						NiSprintf(buf,10,"%d",GetItemData()->num);
						if (pItem && (pItem->Class == ITEM_CLASS_CONSUMABLE || pItem->Class == ITEM_CLASS_USE || pItem->Class == ITEM_CLASS_RECIPE))
						{
							UDrawText(sm_UiRender,m_Style->m_spFont,txtPos,txtSize,LCOLOR_WHITE, buf,UT_RIGHT);
						}
					}
					else
					{
						if (pItem && (pItem->Class == ITEM_CLASS_CONSUMABLE || pItem->Class == ITEM_CLASS_USE || pItem->Class == ITEM_CLASS_RECIPE))
						{
							UDrawText(sm_UiRender,m_Style->m_spFont,txtPos,txtSize,LCOLOR_RED,"0",UT_RIGHT);
							sm_UiRender->DrawRectFill(IconRect, 0xB4000000);
						}
					}
				}
			}
		}else if (pData->type == ActionButton_Type_Spell)
		{
			UINT pSkillTest = SYState()->SkillMgr->TestSkillState(pData->entry);
			if (pSkillTest == TooFar || pSkillTest == TooNear)
			{
				if (m_WaringTexture)
				{
					sm_UiRender->DrawRectFill(IconRect,UColor(130,17,17, 100));
					sm_UiRender->DrawImage(m_WaringTexture, IconRect, LCOLOR_RED);
				}else
				{
					sm_UiRender->DrawRectFill(IconRect,UColor(130,17,17, 100));
				}
			}
			if (pSkillTest == NeedMoreMP || pSkillTest == AttributePassive)
			{
				sm_UiRender->DrawRectFill(IconRect, UColor(0,0,0, 100)); //		
			}
		}
	}
	else
		return;
	if (mAcceleratorKey.GetLength() > 0 && mKeyCodeStr.find("NULL") == -1 && !m_ActionContener.IsItemEmpty())
	{
		UPoint AccKeytextpos(offset.x + m_IconOffset, offset.y + m_IconOffset);
		UDrawText(sm_UiRender, m_Style->m_spFont, AccKeytextpos, UPoint(IconRect.GetWidth(), textH), LCOLOR_WHITE,
			mKeyCodeStr.c_str(), UT_RIGHT);
	}
	USetTextEdge(FALSE);
}
void UDynamicIconButtonEx::acceleratorKeyPress(const char * opCode)
{
	if (m_ActionContener.IsCreate() && m_Active)
	{
		m_ActionContener.AccelorateKeyDown();
	}
}
void UDynamicIconButtonEx::acceleratorKeyRelease(const char * opCode)
{
	if (m_ActionContener.IsCreate() && m_Active)
	{
		m_ActionContener.AccelorateKeyUp();
	}
}