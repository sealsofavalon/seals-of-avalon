#ifndef __UDYNAMICICONBUTTONEX_H__
#define __UDYNAMICICONBUTTONEX_H__
#include "UInc.h"
#include "UActionContener.h"
class UDynamicIconButtonEx : public UButton
{
	UDEC_CLASS(UDynamicIconButtonEx);
public:
	UDynamicIconButtonEx();
	~UDynamicIconButtonEx();
	int         m_IconIndex;
	USkinPtr      m_IconSkin;
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
protected:
	enum
	{
		BACKSKIN_ACTIVE = 0,
		BACKSKIN_INACTIVE = 1,
	};
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);

	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);

	virtual void OnMouseDragged(const UPoint& position, UINT flags);
	virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
	virtual void OnDragDropBegin(const void* pDragData, UINT nDataFlag);
	virtual void OnDragDropEnd(UControl* pDragTo, void* pDragData, UINT nDataFlag);
	virtual void OnDragDropAccept(UControl* pAcceptCtrl,const UPoint& point,const void* pDragData, UINT nDataFlag);
	virtual void OnDragDropReject(UControl* pRejectCtrl, const void* pDragData, UINT nDataFlag);

	virtual void OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag);
	virtual void OnDeskTopDragEnd(UControl* pDragFrom, const void* pDragData, UINT nDataFlag);

	virtual void acceleratorKeyPress(const char * opCode);
	virtual void acceleratorKeyRelease(const char * opCode);

	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseEnter(const UPoint& pt, UINT uFlags);
	virtual void OnMouseLeave(const UPoint& pt, UINT uFlags);

	virtual void OnRenderToolTip();
	virtual void OnHideToolTip();
	virtual BOOL renderTooltip(const UPoint& cursorPos, const char* tipText /* = NULL */ );

	virtual void DrawPushButton(const UPoint& offset, const URect& ClipRect);

	virtual void buildAcceleratorMap();

	virtual bool SpecMethod(UINT uFlags);
public:
	virtual UINT GetDataFlag();
	virtual void SetActionItem(UActionItem * aitem);
	virtual ActionDataInfo * GetItemData();
	virtual void SetItemData(ActionDataInfo &aiteminfo);
	virtual void SetItemData(UActionContener &contener);
	virtual void SetType(ui32 type);
	virtual void SetIconOffset(INT offset);
	virtual void ClearActionItem();
	operator UActionContener*(){return &m_ActionContener;};
	operator UActionContener&(){return m_ActionContener;};

	void SetKeepIcon(BOOL bKeep){m_bKeep = bKeep;};
protected:
	INT			m_IconOffset;
	UActionContener m_ActionContener;
	BOOL         m_BeginDrag;
	ui32         m_TypeID;
	USkinPtr      m_BackGSkin;
	UString       mBackSkinPath;
	BOOL          m_IsTickable;
	UPoint        m_OldSize;
	UPoint		m_OldCursePT;
	UTexturePtr m_WaringTexture;
	BOOL m_bAcceptReDrag;

	std::string mKeyCodeStr;

	BOOL m_IsJustCD;
	BOOL m_bInDrag;
	BOOL m_bKeep;
	BOOL m_bAcceptDrag;

	float	mAutoAttackLastFlipped;
};
#endif