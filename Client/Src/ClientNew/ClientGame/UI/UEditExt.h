#ifndef UEDITEXT_H
#define  UEDITEXT_H

#include "UInc.h"
typedef bool(*Editptr)(UINT, UINT);
class UChatEdit : public UControl
{
	UDEC_CLASS(UChatEdit);
public:
	UChatEdit(void);
	virtual ~UChatEdit(void);
	enum
	{
		MAX_TEXT_LENGTH = 1024,
	};
	Editptr fun;
public:
	virtual void SetText(const char* txt);
	virtual void SetText(const WCHAR* txt);
	virtual void InsertText(const char* txt);
	virtual void InsertText(const WCHAR* txt);
	int GetText(char* dest, UINT nMaxLen, UINT nPos = 0);
	int GetText(WCHAR* dest, UINT nMaxLen, UINT nPos = 0);

	void PlaceCaretPos(UINT pos);
	INT PlaceCatetXPos(const UPoint& offset);

	void SetMaxLength(UINT length);
	
	void Clear();
	void SetFontColor(UColor color);
	void SetFontColor(const char * color);

	void ParseFaceBuffer(std::string &str);
protected:
	void Parse();
	UINT CalSourceCaret(UINT CP);

	void ClearSelection();
	void SetSel(int nStartCP, int nEndCP);
	void SelAll();
	BOOL Undo();
	void Copy();
	void Cut();
	void Paste();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnChar(UINT nCharCode, UINT nRepCnt, UINT nFlags);

	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnGetFocus();
	virtual void OnLostFocus();

	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDragged(const UPoint& pt, UINT uFlags);

	virtual void OnMove(const UPoint& NewOffsetDelta);
	//virtual void OnSize(const UPoint& NewSize);

	//virtual BOOL OnEscape();
	virtual void DrawContext(const URect &drawRect, BOOL isFocused );
private:
	UFontPtr m_spFont;

	UStringW m_SourceBuff;
	UStringW m_ParseBuff;
	std::vector<struct BlockContent> m_BlockList;

	UPoint  mTextOffset;
	UColor mFontColor;

	UINT mSelBegin;
	UINT mSelEnd;
	UINT mCaretPos;
	UINT mCaretXPos;
	UINT m_nMaxLength;
	UINT mCaretLeftWidth;
	float mTimeLastCursorFlipped;
	UINT   mMouseDragStart;

	BOOL mCursorOn;
	BOOL mInsertOn;
	BOOL mDragHit;
};
class UEditExt : public UControl
{
	UDEC_CLASS(UEditExt);
	UDEC_MESSAGEMAP();
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnGetFocus();
	virtual void OnLostFocus();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);

	UEditExt(void);
	~UEditExt(void);
public:
	static UEditExt * CreateEditExt(UPoint size,const char * style = "default");

	void SetHeadFontColor(UColor color);
	void SetHeadFontColor(const char * color);
	void SetBodyFontColor(UColor color);
	void SetBodyFontColor(const char * color);

	void SetHeadText(const char * text,int flag = 1);
	void SetHeadText(const WCHAR * text,int flag = 1);
	void SetBodyText(const char * text);
	void SetBodyText(const WCHAR *text);

	UChatEdit * GetBody(){return m_BodyEdit;};

	//void SetAlignment(int align){m_BodyEdit->SetTextAlignment((EUTextAlignment)align);};
	void SetMaxLength(UINT maxlength){m_BodyEdit->SetMaxLength(maxlength);};

	void Clear();
	void UpdataEdit();
	void EditChanged();

	void SetTextOffset(UPoint point);
private:
	UChatEdit * m_BodyEdit;
	UColor m_HeadEditFontColor;
	UPoint m_TextOffset;
	UStringW m_wstr;
};
#endif