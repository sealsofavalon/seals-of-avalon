#include "StdAfx.h"
#include "UEndowment.h"
#include "UMessageBox.h"
#include "UIGamePlay.h"
#include "UInGameBar.h"
#include "Network/PacketBuilder.h"
#include "ObjectManager.h"
#include "UFun.h"

/////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UBoardControl, UControl);
void UBoardControl::StaticInit(){}
UBoardControl::UBoardControl()
{
	m_Board = NULL;
}
UBoardControl::~UBoardControl()
{
}
BOOL UBoardControl::OnCreate()
{
	if(!UControl::OnCreate())
		return FALSE;

	if(m_Board == NULL)
	{
		m_Board = sm_System->LoadSkin("Endowment\\Board.skin");
		if(!m_Board)
			return FALSE;
	}

	return TRUE;
}
void UBoardControl::OnRender(const UPoint& offset,const URect &updateRect)
{
	UControl::OnRender(offset, updateRect);
	UDrawResizeImage(RESIZE_WANDH, sm_UiRender, m_Board, offset, updateRect.GetSize());
}
/////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UEndowmentFrame, UDialog);
void UEndowmentFrame::StaticInit(){}
UEndowmentFrame::UEndowmentFrame()
{}
UEndowmentFrame::~UEndowmentFrame()
{}
BOOL UEndowmentFrame::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}
	if(m_CloseBtn)
		m_CloseBtn->SetPosition(UPoint(938, 55));

	m_bCanDrag = FALSE;
	return TRUE;
}
void UEndowmentFrame::OnDestroy()
{
	UDialog::OnDestroy();
}
BOOL UEndowmentFrame::OnEscape()
{
	if(!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UEndowmentFrame::OnClose()
{
	UInGame* pInGame = UInGame::Get();
	if (pInGame)
	{
		UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
		if (pGameBar)
		{
			UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONSHEQU);
			if (pBtn)
			{
				pBtn->SetCheck(TRUE);
			}
		}
		pInGame->SetFocusControl();
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UEndowmentMain_Content, UControl);
UBEGIN_MESSAGE_MAP(UEndowmentMain_Content, UControl)
UON_BN_CLICKED(13, &UEndowmentMain_Content::OnPrevPhoto)
UON_BN_CLICKED(15, &UEndowmentMain_Content::OnNextPhoto)
UON_CBOX_SELCHANGE(16, &UEndowmentMain_Content::OnEventChange)
UON_BN_CLICKED(7, &UEndowmentMain_Content::OnClickDonation)
UON_RED_URL_CLICKED(12, &UEndowmentMain_Content::OnURLClicked)
UEND_MESSAGE_MAP()
void UEndowmentMain_Content::StaticInit()
{
	UREG_PROPERTY("PhotoRect", UPT_RECT,UFIELD_OFFSET(UEndowmentMain_Content,m_PhotoRect));
}
uint32 UEndowmentMain_Content::m_JZCoins = 0;
uint32 UEndowmentMain_Content::m_EventId = 0;

UEndowmentMain_Content::UEndowmentMain_Content()
{
	m_UiCurrentPhoto = 0;
	m_Event.des_count = 0;
	m_Photo = 0;
}
UEndowmentMain_Content::~UEndowmentMain_Content()
{}
BOOL UEndowmentMain_Content::OnCreate()
{
	if(!UControl::OnCreate())
		return FALSE;
	UEdit* pEdit = UDynamicCast(UEdit, GetChildByID(9));
	if(pEdit)
	{
		pEdit->SetTextAlignment(UT_RIGHT);
		pEdit->SetNumberOnly(TRUE);
		pEdit->SetMaxLength(8);
	}

	return TRUE;
}
void UEndowmentMain_Content::OnDestroy()
{
	m_Photo = NULL;
	UComboBox* pComboBox = UDynamicCast(UComboBox, GetChildByID(16));
	if (pComboBox)
	{
		pComboBox->Clear();
	}
	UControl::OnDestroy();
}
void UEndowmentMain_Content::OnRender(const UPoint& offset,const URect &updateRect)
{
	if(m_Photo)
	{
		URect rect = m_PhotoRect;
		rect.Offset(offset);
		sm_UiRender->DrawImage(m_Photo, rect);
	}
	UControl::OnRender(offset, updateRect);
}

#include "shellapi.h"
void UEndowmentMain_Content::OnURLClicked(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;
	char buf[256];
	sprintf(buf, "http://%s", strURL.c_str());
	ShellExecute(NULL, "open", buf, NULL, NULL, SW_SHOWNORMAL);
}

void UEndowmentMain_Content::OnPrevPhoto()
{
	if(m_UiCurrentPhoto == 0)
		return;

	if(m_UiCurrentPhoto > 0)
		--m_UiCurrentPhoto;

	SetPhoto();
}

void UEndowmentMain_Content::OnNextPhoto()
{
	if(m_UiCurrentPhoto == m_Event.des_count - 1)
		return;

	++m_UiCurrentPhoto;

	m_UiCurrentPhoto = min(m_UiCurrentPhoto, m_Event.des_count - 1);

	SetPhoto();
}

void UEndowmentMain_Content::OnEventChange()
{
	UComboBox* pComboBox = UDynamicCast(UComboBox, GetChildByID(16));
	if (pComboBox)
	{
		EndowmentMgr->SetCurrDonationEventIndex(pComboBox->GetCurSel());
		if(EndowmentMgr->GetCurrDonationEvent(m_Event))
		{
			m_UiCurrentPhoto = 0;
			SetDateToUI();
		}
	}
}

void UEndowmentMain_Content::OnPrevEvent()
{
	if(EndowmentMgr->GetPrevDonationEvent(m_Event))
	{
		m_UiCurrentPhoto = 0;
		SetDateToUI();
	}
}

void UEndowmentMain_Content::OnNextEvent()
{
	if(EndowmentMgr->GetNextDonationEvent(m_Event))
	{
		m_UiCurrentPhoto = 0;
		SetDateToUI();
	}
}

void UEndowmentMain_Content::OnClickDonation()
{
	UEdit* pkEdit = (UEdit*)GetChildByID(9);
	if(pkEdit)
	{
		char sz[256];
		pkEdit->GetText(sz, 256, 0);
		m_JZCoins = atoi(sz);
		m_EventId = m_Event.id;
		ui32 uiCurCoins = ObjectMgr->GetLocalPlayer()->GetYuanBao();
		if(m_JZCoins > 0 && m_JZCoins <= uiCurCoins)
		{
			string str = _TRAN(N_NOTICE_F84, _I2A(m_JZCoins).c_str());
			//sprintf(sz, "确定要捐助%u的元宝?", m_JZCoins);
			UMessageBox::MsgBox(str.c_str(), (BoxFunction*)UEndowmentMain_Content::JZOK, NULL);
		}
		else if(m_JZCoins > uiCurCoins)
		{
			UMessageBox::MsgBox_s(_TRAN("您的帐户余额不足，无法完成捐助。"));
		}
		pkEdit->Clear();
	}
}

void UEndowmentMain_Content::JZOK()
{
	EndowmentMgr->SendDonationReq(m_EventId, m_JZCoins);
	UEndowmentMain_Content::m_JZCoins = 0;
	UEndowmentMain_Content::m_EventId = 0;
}

void UEndowmentMain_Content::SetCurrentContent()
{
	UComboBox* pComboBox = UDynamicCast(UComboBox, GetChildByID(16));
	if (pComboBox)
	{
		std::string name;
		EndowmentMgr->SetCurrDonationEventIndex(0);
		EndowmentMgr->GetCurrDonationEvent(m_Event);
		if(EndowmentMgr->LoadDonationEventName(m_Event.id, name))
			pComboBox->AddItem(name.c_str());

		for ( ; EndowmentMgr->GetNextDonationEvent(m_Event) ;)
		{
			if(EndowmentMgr->LoadDonationEventName(m_Event.id, name))
				pComboBox->AddItem(name.c_str());
		}
		EndowmentMgr->SetCurrDonationEventIndex(0);
		pComboBox->SetCurSel(EndowmentMgr->GetCurrDonationEventIndex());
	}
}

void UEndowmentMain_Content::SetDateToUI()
{
	char buf[256];

	//有效日期
	UStaticText* pDataEdit = (UStaticText*)GetChildByID(11);
	if(pDataEdit)
	{
		std::string Time1, Time2;
		time_t starttimer = m_Event.start;
		struct tm *Stblock, *Etblock;
		Stblock = localtime(&starttimer);
		sprintf(buf, "%d.%d.%d",
		Stblock->tm_year + 1900,
		Stblock->tm_mon + 1,
		Stblock->tm_mday);
		Time1 = buf;
		time_t endtimer = m_Event.expire;
		Etblock = localtime(&endtimer);
		sprintf(buf, "%d.%d.%d",
		Etblock->tm_year + 1900,
		Etblock->tm_mon + 1,
		Etblock->tm_mday);
		Time2 = buf;
		std::string str = _TRAN(N_NOTICE_C85, Time1.c_str(), Time2.c_str());
		pDataEdit->SetText(str.c_str());
		pDataEdit->SetTextAlign(UT_RIGHT);
	}


	SetPhoto();
}

void UEndowmentMain_Content::SetPhoto()
{
	//页码
	UStaticText* pEdit = (UStaticText*)GetChildByID(14);
	if(pEdit)
	{
		char buf[256];
		sprintf(buf, "%d/%d", m_UiCurrentPhoto + 1, m_Event.des_count);
		pEdit->SetText(buf);
	}

	if(m_Photo)
	{
		m_Photo = NULL;
	}

	m_Photo = EndowmentMgr->LoadDonationPhoto(m_Event.id, m_UiCurrentPhoto);

	//图片简介
	URichTextCtrl* pRichCtrl = UDynamicCast(URichTextCtrl, GetChildByID(12));
	if(pRichCtrl)
	{
		std::string URL, desc, All;
		if(EndowmentMgr->LoadDonationEventURL(m_Event.id, URL) && EndowmentMgr->LoadDonationDes(m_Event.id, m_UiCurrentPhoto, desc))
		{
			All = desc + "[<a:" + URL +">"+ _TRAN("详细")+ "</a>]";
		}
		pRichCtrl->SetText(All.c_str(), All.length());
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UEndowmentRank_Content, UControl);
UBEGIN_MESSAGE_MAP(UEndowmentRank_Content, UControl)
UON_BN_CLICKED(6, &UEndowmentRank_Content::OnPrevLadder)
UON_BN_CLICKED(7, &UEndowmentRank_Content::OnNextLadder)
UEND_MESSAGE_MAP()
void UEndowmentRank_Content::StaticInit()
{}
UEndowmentRank_Content::UEndowmentRank_Content()
{
	m_pkAllList = NULL;
	m_pkSingleList = NULL;
}
UEndowmentRank_Content::~UEndowmentRank_Content()
{
	m_pkAllList = NULL;
	m_pkSingleList = NULL;
}
BOOL UEndowmentRank_Content::OnCreate()
{
	if(!UControl::OnCreate())
		return FALSE;

	UStaticText* pStaticText = (UStaticText*)GetChildByID(4);
	if(pStaticText)
	{
		pStaticText->SetTextEdge(true, UColor(255,255,255,255));
		pStaticText->SetTextAlign(UT_CENTER);
	}
	pStaticText = (UStaticText*)GetChildByID(5);
	if(pStaticText)
	{
		pStaticText->SetTextColor(UColor(0,0,0,255));
		pStaticText->SetTextAlign(UT_CENTER);
	}
	
	m_pkAllList = (USpecListBox*)GetChildByID(9);
	m_pkSingleList = (USpecListBox*)GetChildByID(8);
	if(!m_pkAllList || !m_pkSingleList)
		return FALSE;
	m_pkAllList->SetGridSize(UPoint(392, 19));
	m_pkAllList->SetGridCount(20, 1);
	m_pkAllList->SetCol1(0, 0);
	m_pkAllList->SetWidthCol1(39);
	m_pkAllList->SetCol2(63 - 24, 0);
	m_pkAllList->SetWidthCol2(111);
	m_pkAllList->SetCol3(174 - 24, 0);
	m_pkAllList->SetWidthCol3(41);
	m_pkAllList->SetCol4(215 - 24, 0);
	m_pkAllList->SetWidthCol4(109);
	m_pkAllList->SetCol5(324 - 24, 0);
	m_pkAllList->SetWidthCol5(92);

	m_pkSingleList->SetGridSize(UPoint(392, 19));
	m_pkSingleList->SetGridCount(20, 1);
	m_pkSingleList->SetCol1(0, 0);
	m_pkSingleList->SetWidthCol1(44);
	m_pkSingleList->SetCol2(504 - 460, 0);
	m_pkSingleList->SetWidthCol2(115);
	m_pkSingleList->SetCol3(619 -460, 0);
	m_pkSingleList->SetWidthCol3(40);
	m_pkSingleList->SetCol4(659 - 460, 0);
	m_pkSingleList->SetWidthCol4(102);
	m_pkSingleList->SetCol5(761 - 460, 0);
	m_pkSingleList->SetWidthCol5(94);
	return TRUE;
}

void UEndowmentRank_Content::OnDestroy()
{
	m_pkAllList->Clear();
	m_pkSingleList->Clear();
	UControl::OnDestroy();
}

void UEndowmentRank_Content::OnPrevLadder()
{
	if(EndowmentMgr->GetPrevDonationLadder(m_stSingleLadder))
	{
		SetSingleLadder();
		SetSingleLadderOther();
	}
}

void UEndowmentRank_Content::OnNextLadder()
{
	if(EndowmentMgr->GetNextDonationLadder(m_stSingleLadder))
	{
		SetSingleLadder();
		SetSingleLadderOther();
	}
}

void UEndowmentRank_Content::SetTotalLadder()
{
	m_pkAllList->Clear();
	EndowmentManager::singleLadder st;
	if(EndowmentMgr->GetTotalDonationLadder(st))
	{
		char sz[256];
		for(size_t i = 0 ; i < 19 ; i++)
		{
			if( i < st.v.size() - 1)
			{
				NiSprintf(sz, 256, 
					"<defaultcolor><Col1><center>%d<end>"
					"<defaultcolor><Col2><center>%s<end_left>"
					"<defaultcolor><Col3><center>%d<end>"
					"<defaultcolor><Col4><center>%s<end_left>"
					"<defaultcolor><Col5><center>%d<end>",
					i + 1,
					st.v[i].name.c_str(),
					st.v[i].level,
					st.v[i].guild_name.size()?st.v[i].guild_name.c_str():"---",
					st.v[i].yuanbao
					);	
				m_pkAllList->AddItem(sz);
			}
			else
			{
				NiSprintf(sz, 256, 
					"<defaultcolor><Col1><center>%s<end>"
					"<defaultcolor><Col2><center>%s<end_left>"
					"<defaultcolor><Col3><center>%s<end>"
					"<defaultcolor><Col4><center>%s<end_left>"
					"<defaultcolor><Col5><center>%s<end>",
					"-",
					"---",
					"--",
					"---",
					"---"
					);	
				m_pkAllList->AddItem(sz);
			}
		}
		if(st.v[st.v.size() - 1].yuanbao)
		{
			NiSprintf(sz, 256, 
				"<color:R000G200B000A255><Col1><center>%d<end>"
				"<color:R000G200B000A255><Col2><center>%s<end_left>"
				"<color:R000G200B000A255><Col3><center>%d<end>"
				"<color:R000G200B000A255><Col4><center>%s<end_left>"
				"<color:R000G200B000A255><Col5><center>%d<end>",
				st.local_query + 1,
				st.v[st.v.size() - 1].name.c_str(),
				st.v[st.v.size() - 1].level,
				st.v[st.v.size() - 1].guild_name.size()?st.v[st.v.size() - 1].guild_name.c_str():"---",
				st.v[st.v.size() - 1].yuanbao
				);
		}
		else
		{
			NiSprintf(sz, 256, 
				"<color:R000G200B000A255><Col1><center>%s<end>"
				"<color:R000G200B000A255><Col2><center>%s<end_left>"
				"<color:R000G200B000A255><Col3><center>%d<end>"
				"<color:R000G200B000A255><Col4><center>%s<end_left>"
				"<color:R000G200B000A255><Col5><center>%s<end>",
				"-",
				st.v[st.v.size() - 1].name.c_str(),
				st.v[st.v.size() - 1].level,
				st.v[st.v.size() - 1].guild_name.size()?st.v[st.v.size() - 1].guild_name.c_str():"---",
				"---"
				);
		}
		m_pkAllList->AddItem(sz);
	}

}

void UEndowmentRank_Content::SetSingleLadder()
{
	m_pkSingleList->Clear();
	char sz[256];
	for(size_t i = 0 ; i < 19 ; i++)
	{
		if( m_stSingleLadder.v.size() && i < m_stSingleLadder.v.size() - 1)
		{
			NiSprintf(sz, 256, 
				"<defaultcolor><Col1><center>%d<end>"
				"<defaultcolor><Col2><center>%s<end_left>"
				"<defaultcolor><Col3><center>%d<end>"
				"<defaultcolor><Col4><center>%s<end_left>"
				"<defaultcolor><Col5><center>%d<end>",
				i + 1,
				m_stSingleLadder.v[i].name.c_str(),
				m_stSingleLadder.v[i].level,
				m_stSingleLadder.v[i].guild_name.size()?m_stSingleLadder.v[i].guild_name.c_str():"---",
				m_stSingleLadder.v[i].yuanbao
				);	
			m_pkSingleList->AddItem(sz);
		}
		else
		{
			NiSprintf(sz, 256, 
				"<defaultcolor><Col1><center>%s<end>"
				"<defaultcolor><Col2><center>%s<end_left>"
				"<defaultcolor><Col3><center>%s<end>"
				"<defaultcolor><Col4><center>%s<end_left>"
				"<defaultcolor><Col5><center>%s<end>",
				"-",
				"---",
				"--",
				"---",
				"---"
				);	
			m_pkSingleList->AddItem(sz);
		}
	}

	if( m_stSingleLadder.v.size() && m_stSingleLadder.v[m_stSingleLadder.v.size() - 1].yuanbao)
	{
		NiSprintf(sz, 256, 
			"<color:R000G200B000A255><Col1><center>%d<end>"
			"<color:R000G200B000A255><Col2><center>%s<end_left>"
			"<color:R000G200B000A255><Col3><center>%d<end>"
			"<color:R000G200B000A255><Col4><center>%s<end_left>"
			"<color:R000G200B000A255><Col5><center>%d<end>",
			m_stSingleLadder.local_query + 1,
			m_stSingleLadder.v[m_stSingleLadder.v.size() - 1].name.c_str(),
			m_stSingleLadder.v[m_stSingleLadder.v.size() - 1].level,
			m_stSingleLadder.v[m_stSingleLadder.v.size() - 1].guild_name.size()?m_stSingleLadder.v[m_stSingleLadder.v.size() - 1].guild_name.c_str():"---",
			m_stSingleLadder.v[m_stSingleLadder.v.size() - 1].yuanbao
			);
		m_pkSingleList->AddItem(sz);
	}
	else
	{
		NiSprintf(sz, 256, 
			"<color:R000G200B000A255><Col1><center>%s<end>"
			"<color:R000G200B000A255><Col2><center>%s<end_left>"
			"<color:R000G200B000A255><Col3><center>%d<end>"
			"<color:R000G200B000A255><Col4><center>%s<end_left>"
			"<color:R000G200B000A255><Col5><center>%s<end>",
			"-",
			m_stSingleLadder.v[m_stSingleLadder.v.size() - 1].name.c_str(),
			m_stSingleLadder.v[m_stSingleLadder.v.size() - 1].level,
			m_stSingleLadder.v[m_stSingleLadder.v.size() - 1].guild_name.size()?m_stSingleLadder.v[m_stSingleLadder.v.size() - 1].guild_name.c_str():"---",
			"---"
			);
		m_pkSingleList->AddItem(sz);
	}
}

void UEndowmentRank_Content::SetSingleLadderOther()
{
	//名字
	UStaticText* pNameEdit = (UStaticText*)GetChildByID(4);
	if(pNameEdit)
	{
		std::string temp;
		if(EndowmentMgr->LoadDonationEventName(m_stSingleLadder.query_event_id, temp))
		{
			pNameEdit->SetText(temp.c_str());
		}
	}

	//有效日期
	UStaticText* pDataEdit = (UStaticText*)GetChildByID(5);
	ui32 start,end;
	if(pDataEdit && EndowmentMgr->GetDonationEventTime(m_stSingleLadder.query_event_id, start, end ))
	{
		char buf[256];
		std::string Time1, Time2;
		time_t starttimer = start;
		struct tm *Stblock, *Etblock;
		Stblock = localtime(&starttimer);
		sprintf(buf, "%d.%d.%d",
			Stblock->tm_year + 1900,
			Stblock->tm_mon + 1,
			Stblock->tm_mday);
		Time1 = buf;
		time_t endtimer = end;
		Etblock = localtime(&endtimer);
		sprintf(buf, "%d.%d.%d",
			Etblock->tm_year + 1900,
			Etblock->tm_mon + 1,
			Etblock->tm_mday);
		Time2 = buf;
		std::string str = _TRAN(N_NOTICE_C85, Time1.c_str(), Time2.c_str());

		pDataEdit->SetText(str.c_str());
		pDataEdit->SetTextAlign(UT_CENTER);
	}
}

void UEndowmentRank_Content::OnSetLadder(bool is_total)
{
	if(is_total)
	{
		SetTotalLadder();
	}
	else
	{
		if(EndowmentMgr->GetCurrDonationLadder(m_stSingleLadder))
		{
			SetSingleLadder();
			SetSingleLadderOther();
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UEndowmentHistory_Content, UControl);
UBEGIN_MESSAGE_MAP(UEndowmentHistory_Content, UControl)
UON_BN_CLICKED(2, &UEndowmentHistory_Content::OnPrevPage)
UON_BN_CLICKED(4, &UEndowmentHistory_Content::OnNextPage)
UEND_MESSAGE_MAP()
void UEndowmentHistory_Content::StaticInit()
{}
UEndowmentHistory_Content::UEndowmentHistory_Content()
{
	m_pkList = NULL;
	mUiCurrentPage = 0;
	mUiMaxiumPage = 0;
}
UEndowmentHistory_Content::~UEndowmentHistory_Content()
{
	m_pkList = NULL;
	mUiCurrentPage = 0;
	mUiMaxiumPage = 0;
}
BOOL UEndowmentHistory_Content::OnCreate()
{
	if(!UControl::OnCreate())
		return FALSE;
	if(m_pkList == NULL)
		m_pkList = (USpecListBox*)GetChildByID(5);
	if(!m_pkList)
		return FALSE;

	m_pkList->SetGridSize(UPoint(585, 21));
	m_pkList->SetGridCount(20, 1);
	m_pkList->SetCol1(0, 0);
	m_pkList->SetWidthCol1(186);
	m_pkList->SetCol2(330 - 144, 0);
	m_pkList->SetWidthCol2(247);
	m_pkList->SetCol3(577 - 144, 0);
	m_pkList->SetWidthCol3(152);

	SetCurrentEndowmentHistory();
	return TRUE;
}
void UEndowmentHistory_Content::OnDestroy()
{
	m_pkList->Clear();
	UControl::OnDestroy();
}
void UEndowmentHistory_Content::OnNextPage()
{
	++mUiCurrentPage;
	if(mUiCurrentPage > mUiMaxiumPage)
	{
		mUiCurrentPage = mUiMaxiumPage;
	}
	SetCurrentEndowmentHistory();
}
void UEndowmentHistory_Content::OnPrevPage()
{
	if(mUiCurrentPage > 0)
	{
		--mUiCurrentPage;
	}
	SetCurrentEndowmentHistory();
}
void UEndowmentHistory_Content::SetHistorySize(int size)
{
	mUiCurrentPage = 0;
	mUiMaxiumPage = (size / (20 + 1));
	SetCurrentEndowmentHistory();
};
void UEndowmentHistory_Content::SetCurrentEndowmentHistory()
{
	m_pkList->Clear();
	std::vector<MSG_S2C::stDonationHistory::evt> v;
	if(EndowmentMgr->GetDonationHistoryList(mUiCurrentPage, v))
	{
		std::string titleName;
		int size = min( v.size(), 20);
		for(int i = 0 ; i < size ; i++)
		{
			if(EndowmentMgr->LoadDonationEventName( v[i].event_id, titleName))
			{
				AddEndowmentHistoryItem(v[i].datatime, titleName.c_str(), v[i].yuanbao);
			}
		}
	}

	char buf[256];
	sprintf(buf, "%u/%u", mUiCurrentPage + 1, mUiMaxiumPage + 1);
	UStaticText* pStaticText = (UStaticText*)GetChildByID(3);
	if(pStaticText)
		pStaticText->SetText(buf);
}
void UEndowmentHistory_Content::AddEndowmentHistoryItem(ui64 time, const char* content, ui32 yuanbao)
{
	char sz[256];
	time_t timer = time;
	struct tm *tblock;
	tblock = localtime(&timer);
	NiSprintf(sz, 256, 
		"<defaultcolor><Col1><center>%d.%d.%d  %02d:%02d:%02d<end_left>"
		"<defaultcolor><Col2><center>%s<end_left>"
		"<defaultcolor><Col3><center>%s<end_right>",
		tblock->tm_year + 1900,
		tblock->tm_mon + 1,
		tblock->tm_mday,
		tblock->tm_hour,
		tblock->tm_min,
		tblock->tm_sec,
		content,
		_TRAN(N_NOTICE_F85, _I2A(yuanbao).c_str()).c_str());	
	m_pkList->AddItem(sz);
}
/////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UEndowmentTableBtn_Content, UControl);
UBEGIN_MESSAGE_MAP(UEndowmentTableBtn_Content, UControl)
UON_BN_CLICKED(0, &UEndowmentTableBtn_Content::OnClickEndowmentMain)
UON_BN_CLICKED(1, &UEndowmentTableBtn_Content::OnClickEndowmentRank)
UON_BN_CLICKED(2, &UEndowmentTableBtn_Content::OnClickEndowmentHistory)
UON_BN_CLICKED(3, &UEndowmentTableBtn_Content::OnClickEndowmentDesc)
UEND_MESSAGE_MAP()
void UEndowmentTableBtn_Content::StaticInit()
{}
UEndowmentTableBtn_Content::UEndowmentTableBtn_Content()
{}
UEndowmentTableBtn_Content::~UEndowmentTableBtn_Content()
{}
BOOL UEndowmentTableBtn_Content::OnCreate()
{
	if(!UControl::OnCreate())
		return FALSE;

	UButton* pbtn = UDynamicCast(UButton, GetChildByID(0));
	if(pbtn)
	{
		pbtn->SetCheck(FALSE);
	}

	return TRUE;
}
void UEndowmentTableBtn_Content::OnDestroy()
{
	UControl::OnDestroy();
}
void UEndowmentTableBtn_Content::OnClickEndowmentMain()
{
	EndowmentMgr->SetContentShow(E_CONTENT_MAIN);
}
void UEndowmentTableBtn_Content::OnClickEndowmentRank()
{
	EndowmentMgr->SetContentShow(E_CONTENT_RANK);
}
void UEndowmentTableBtn_Content::OnClickEndowmentHistory()
{
	EndowmentMgr->SetContentShow(E_CONTENT_HISTORY);
}
void UEndowmentTableBtn_Content::OnClickEndowmentDesc()
{
	EndowmentMgr->SetContentShow(E_CONTENT_DESC);
}
/////////////////////////////////////////////////////////////////////////////////////////////
EndowmentManager::EndowmentManager()
{
	m_UEmMain = NULL;
	m_UEmRank = NULL;
	m_UEmHistory = NULL;
	m_EndowmentDesc = NULL;
	mUiCurrDonationEvent = 0;
	mUiCurrDonationLadder =0;
}

EndowmentManager::~EndowmentManager()
{
	m_UEmMain = NULL;
	m_UEmRank = NULL;
	m_UEmHistory = NULL;
}

void EndowmentManager::Initialize()
{
	mUiCurrDonationEvent = 0;
	mUiCurrDonationLadder =0;
	mDonationEventList.clear();
	mDonationHistory.clear();
	mDonationSingleLadder.clear();
	mDonationTotalLadder.v.clear();
	mDonationTotalLadder.local_query = 0;
	mDonationTotalLadder.query_event_id = 0;
}

BOOL EndowmentManager::CreateFrame(UControl* Frame)
{
	mUiCurrDonationEvent = 0;
	UControl* pRootContent = NULL;
	UControl* pNewCtrl = NULL;
	int CtrlId = 0;
	if(pNewCtrl == NULL && Frame->GetChildByID(FRAME_IG_ENDOWMENTDLG) == NULL)
	{
		pNewCtrl = UControl::sm_System->CreateDialogFromFile("Donation\\DonationContent.udg");
		if(!pNewCtrl)
			return FALSE;
		pRootContent = pNewCtrl;
		pRootContent->SetId(FRAME_IG_ENDOWMENTDLG);
		Frame->AddChild(pRootContent);

		pNewCtrl = NULL;
		pNewCtrl = UControl::sm_System->CreateDialogFromFile("Donation\\DonationFrame.udg");
		if(!pNewCtrl)
			return FALSE;
		m_EndowmentFrame = pNewCtrl;
		m_EndowmentFrame->SetId(0);
		pRootContent->AddChild(m_EndowmentFrame);

		m_EndowmentDesc = m_EndowmentFrame->GetChildByID(100);


		pNewCtrl = NULL;
		if(pNewCtrl == NULL)
		{
			pNewCtrl = UControl::sm_System->CreateDialogFromFile("Donation\\DonationMainContent.udg");
			if(!pNewCtrl)
				return FALSE;
			pNewCtrl->SetId(CtrlId++);
			m_EndowmentFrame->AddChild(pNewCtrl);
			m_UEmMain = UDynamicCast(UEndowmentMain_Content, pNewCtrl);
		}

		pNewCtrl = NULL;
		if(pNewCtrl == NULL)
		{
			pNewCtrl = UControl::sm_System->CreateDialogFromFile("Donation\\DonationRankContent.udg");
			if(!pNewCtrl)
				return FALSE;
			pNewCtrl->SetId(CtrlId++);
			m_EndowmentFrame->AddChild(pNewCtrl);
			m_UEmRank = UDynamicCast(UEndowmentRank_Content, pNewCtrl);
		}

		pNewCtrl = NULL;
		if(pNewCtrl == NULL)
		{
			pNewCtrl = UControl::sm_System->CreateDialogFromFile("Donation\\DonationHistoryContent.udg");
			if(!pNewCtrl)
				return FALSE;
			pNewCtrl->SetId(CtrlId++);
			m_EndowmentFrame->AddChild(pNewCtrl);
			m_UEmHistory = UDynamicCast(UEndowmentHistory_Content, pNewCtrl);
		}

		pNewCtrl = NULL;
		if(pNewCtrl == NULL)
		{
			pNewCtrl = UControl::sm_System->CreateDialogFromFile("Donation\\DonationTableBtn.udg");
			if(!pNewCtrl)
				return FALSE;
			pNewCtrl->SetId(CtrlId++);
			m_EndowmentFrame->AddChild(pNewCtrl);
		}
	}
	return TRUE;
}

void EndowmentManager::SetContentShow(eContent eIndex)
{
	if(!m_UEmMain || !m_UEmRank || !m_UEmHistory || !m_EndowmentDesc)
		return;

	switch(eIndex)
	{
	case E_CONTENT_MAIN:
		m_UEmMain->SetVisible(TRUE);
		m_UEmRank->SetVisible(FALSE);
		m_UEmHistory->SetVisible(FALSE);
		m_EndowmentDesc->SetVisible(FALSE);
		break;
	case E_CONTENT_RANK:
		m_UEmMain->SetVisible(FALSE);
		m_UEmRank->SetVisible(TRUE);
		m_UEmHistory->SetVisible(FALSE);
		m_EndowmentDesc->SetVisible(FALSE);
		break;
	case E_CONTENT_HISTORY:
		m_UEmMain->SetVisible(FALSE);
		m_UEmRank->SetVisible(FALSE);
		m_UEmHistory->SetVisible(TRUE);
		m_EndowmentDesc->SetVisible(FALSE);
		break;
	case E_CONTENT_DESC:
		m_UEmMain->SetVisible(FALSE);
		m_UEmRank->SetVisible(FALSE);
		m_UEmHistory->SetVisible(FALSE);
		m_EndowmentDesc->SetVisible(TRUE);
		break;
	}
}

void EndowmentManager::UpdateLadder()
{
//#ifndef TEST_CLIENT
	for(size_t ui = 0; ui < mDonationEventList.size() ; ++ui )
	{
		SendQueryDonationLadder(mDonationEventList[ui].id, false);
	}
	SendQueryDonationLadder(0, true);
//#endif
}

void EndowmentManager::ParseDonationEventList( const std::vector<MSG_S2C::stDonationEventList::evt>& vList)
{
	mUiCurrDonationEvent = 0;
	mDonationEventList = vList;
	if(mDonationEventList.size())
	{
		m_UEmMain->SetCurrentContent();
		//上线的时候发送
		UpdateLadder();
	}
}

void EndowmentManager::ParseDonationHistory(const std::vector<MSG_S2C::stDonationHistory::evt>& vList)
{
	mDonationHistory = vList;
	if(mDonationHistory.size())
	{
		m_UEmHistory->SetHistorySize(mDonationHistory.size());
		m_UEmHistory->SetCurrentEndowmentHistory();
	}
}

void EndowmentManager::ParseDonationAck(uint8 result)
{
	switch(result)
	{
	case MSG_S2C::stDonateAck::DONATE_RESULT_OK:
		{
			//UpdateLadder();
			ClientSystemNotify(_TRAN("恭喜您，您捐助成功了，谢谢您对公益事业的支持。"));
		}
		break;
	case MSG_S2C::stDonateAck::DONATE_RESULT_NOT_ENOUGH_YUANBAO:
		ClientSystemNotify(_TRAN("对不起，您没有那么多元宝可以捐助。"));
		break;
	case MSG_S2C::stDonateAck::DONATE_RESULT_EXPIRE:
		ClientSystemNotify(_TRAN("对不起，您所捐助的项目已经过期了。"));
		break;
	case MSG_S2C::stDonateAck::DONATE_NOT_FOUND:
		ClientSystemNotify(_TRAN("对不起，没有找到您所捐助的项目。"));
		break;
	case MSG_S2C::stDonateAck::DONATE_SYSTEM_ERROR :
		ClientSystemNotify(_TRAN("对不起，系统出错了。"));
		break;
	}
}

void EndowmentManager::ParseDonationLadder(bool IsTotal, uint32 query_event_id, uint32 local_query, const std::vector<MSG_S2C::stDonationLadderAck::info>& vList)
{
	if(IsTotal)
	{
		mDonationTotalLadder.query_event_id = 0;
		mDonationTotalLadder.v = vList;
		mDonationTotalLadder.local_query = local_query;
		m_UEmRank->OnSetLadder(true);
	}
	else
	{
		mUiCurrDonationLadder = 0;
		SingleLadderMap::iterator it = mDonationSingleLadder.find(query_event_id);
		if(it != mDonationSingleLadder.end())
		{
			it->second.v = vList;
		}
		else
		{
			singleLadder temp;
			temp.query_event_id = query_event_id;
			temp.v = vList;
			temp.local_query = local_query;
			mDonationSingleLadder.insert(SingleLadderMap::value_type(query_event_id, temp));
		}
		m_UEmRank->OnSetLadder(false);
	}
}

void EndowmentManager::ParseAddDonationHistory(	uint32 event_id, uint32 datatime, uint32 yuanbao)
{
	MSG_S2C::stDonationHistory::evt stTemp;
	stTemp.event_id = event_id;
	stTemp.datatime = datatime;
	stTemp.yuanbao = yuanbao;
	mDonationHistory.push_back(stTemp);
	if(mDonationHistory.size())
	{
		m_UEmHistory->SetHistorySize(mDonationHistory.size());
		m_UEmHistory->SetCurrentEndowmentHistory();
	}
}

void EndowmentManager::SendDonationReq(uint32 event_id, uint32 yuanbao, const char* leave_word)
{
	//TODO:Re
	PacketBuilder->SendDonateReq(event_id, yuanbao, leave_word);
}

void EndowmentManager::SendQueryDonationLadder(uint32 event_id, bool is_total)
{
	PacketBuilder->SendQueryDonationLadder(event_id, is_total);
}

bool EndowmentManager::LoadDonationEventName(ui32 DonationId, std::string& Name)
{
    char* acBuffer = (char*)malloc( 256*1024 );
	sprintf(acBuffer, "Data\\Donation\\Donation_%u\\Title.txt", DonationId);
    NiFile* pkFile = NiFile::GetFile(acBuffer, NiFile::READ_ONLY);
    if(!pkFile || !(*pkFile))
    {
        NiDelete pkFile;
		free( acBuffer );
        return false;
    }

    unsigned int uiSize = pkFile->Read(acBuffer, 256*1024);
    acBuffer[uiSize] = '\0';
    NiDelete pkFile;

	Name = AnisToUTF8(acBuffer);
	free( acBuffer );
	return true;
}

bool EndowmentManager::LoadDonationEventURL(ui32 DonationId, std::string& URL)
{
    char* acBuffer = (char*)malloc(256*1024);
	sprintf(acBuffer, "Data\\Donation\\Donation_%u\\URL.txt", DonationId);
    NiFile* pkFile = NiFile::GetFile(acBuffer, NiFile::READ_ONLY);
    if(!pkFile || !(*pkFile))
    {
        NiDelete pkFile;
		free( acBuffer );
        return false;
    }

    unsigned int uiSize = pkFile->Read(acBuffer, 256*1024);
    acBuffer[uiSize] = '\0';
    NiDelete pkFile;

	URL = acBuffer;
	free( acBuffer );
	return true;
}

bool EndowmentManager::LoadDonationDes(ui32 DonationId, UINT DesNum, std::string& desc)
{
    char* acBuffer = (char*)malloc(256*1024);
	sprintf(acBuffer, "Data\\Donation\\Donation_%u\\Event_des%u.txt", DonationId, DesNum);
    NiFile* pkFile = NiFile::GetFile(acBuffer, NiFile::READ_ONLY);
    if(!pkFile || !(*pkFile))
    {
        NiDelete pkFile;
		free( acBuffer );
        return false;
    }

    unsigned int uiSize = pkFile->Read(acBuffer, 256*1024);
    acBuffer[uiSize] = '\0';
    NiDelete pkFile;

	desc = AnisToUTF8(acBuffer);
	free( acBuffer );
	return true;
}

UTexturePtr EndowmentManager::LoadDonationPhoto(ui32 DonationId, UINT DesNum)
{
    char* acBuffer = (char*)malloc(256*1024);
	sprintf(acBuffer, "Data\\Donation\\Donation_%u\\Event_photo_%u.jpg", DonationId, DesNum);
	return UControl::sm_UiRender->LoadTexture(acBuffer);

	free( acBuffer );
}

bool EndowmentManager::GetDonationEventTime(ui32 DonationId, uint32& starttime, uint32& endtime)
{
	for(size_t ui = 0 ; ui < mDonationEventList.size() ; ++ui)
	{
		if(mDonationEventList[ui].id == DonationId)
		{
			starttime = mDonationEventList[ui].start;
			endtime = mDonationEventList[ui].expire;
			return true;
		}
	}
	return false;
}
UINT EndowmentManager::GetCurrDonationEventIndex()
{
	return mUiCurrDonationEvent;
}

void EndowmentManager::SetCurrDonationEventIndex(UINT index)
{
	mUiCurrDonationEvent = index;
}

bool EndowmentManager::GetCurrDonationEvent(MSG_S2C::stDonationEventList::evt& evt)
{
	if(mUiCurrDonationEvent < 0 || mUiCurrDonationEvent >= mDonationEventList.size())
		return false;

	evt.id = mDonationEventList[mUiCurrDonationEvent].id;
	evt.start = mDonationEventList[mUiCurrDonationEvent].start;
	evt.expire =mDonationEventList[mUiCurrDonationEvent].expire;
	evt.yuanbao = mDonationEventList[mUiCurrDonationEvent].yuanbao;
	evt.des_count = mDonationEventList[mUiCurrDonationEvent].des_count;
	return true;
}

bool EndowmentManager::GetPrevDonationEvent(MSG_S2C::stDonationEventList::evt& evt)
{
	if(mUiCurrDonationEvent == 0)
		return false;

	if(mUiCurrDonationEvent > 0)
		--mUiCurrDonationEvent;

	return GetCurrDonationEvent(evt);
}

bool EndowmentManager::GetNextDonationEvent(MSG_S2C::stDonationEventList::evt& evt)
{
	if(mUiCurrDonationEvent == mDonationEventList.size() - 1)
		return false;

	++mUiCurrDonationEvent;
	mUiCurrDonationEvent = min(mUiCurrDonationEvent, mDonationEventList.size() - 1);

	return GetCurrDonationEvent(evt);
}

bool EndowmentManager::GetCurrDonationLadder(EndowmentManager::singleLadder& st)
{
	if(mUiCurrDonationLadder < 0 || mUiCurrDonationLadder >= mDonationEventList.size())
		return false;

	SingleLadderMap::iterator it = mDonationSingleLadder.find(mDonationEventList[mUiCurrDonationLadder].id);
	if(it != mDonationSingleLadder.end())
	{
		st.query_event_id = mDonationEventList[mUiCurrDonationLadder].id;
		st.v = it->second.v;
		st.local_query = it->second.local_query;
		return true;
	}
	return false;
}

bool EndowmentManager::GetPrevDonationLadder(EndowmentManager::singleLadder& st)
{
	if(mUiCurrDonationLadder == 0)
		return false;

	if(mUiCurrDonationLadder > 0)
		--mUiCurrDonationLadder;

	return GetCurrDonationLadder(st);
}

bool EndowmentManager::GetNextDonationLadder(EndowmentManager::singleLadder& st)
{
	if(mUiCurrDonationLadder == mDonationEventList.size() - 1)
		return false;

	++mUiCurrDonationLadder;
	mUiCurrDonationLadder = min(mUiCurrDonationLadder, mDonationEventList.size() - 1);

	return GetCurrDonationLadder(st);
}

bool EndowmentManager::GetTotalDonationLadder(EndowmentManager::singleLadder& st)
{
	st.local_query = mDonationTotalLadder.local_query;
	st.v = mDonationTotalLadder.v;
	return true;
}

bool EndowmentManager::GetDonationHistoryList(UINT Current, std::vector<MSG_S2C::stDonationHistory::evt>& vList)
{
	vList.clear();
	int size = min(mDonationHistory.size() - Current * 20, 20);
	if(size <= 0)
		return false;
	vList.reserve(size);
	for(int i = 0 ; i < size ; ++i)
	{
		vList.push_back(mDonationHistory[ i + Current * 20]);
	}
	return true;
}