#ifndef __UENDOWMENT_H__
#define __UENDOWMENT_H__

#include "UInc.h"
#include "USpecListBox.h"
#include "Singleton.h"
enum eContent
{
	E_CONTENT_MAIN,
	E_CONTENT_RANK,
	E_CONTENT_HISTORY,
	E_CONTENT_DESC,
};
#define EndowmentMgr EndowmentManager::GetSingleton()
class EndowmentManager : public Singleton<EndowmentManager>
{
	SINGLETON(EndowmentManager)
public:
	struct singleLadder
	{
		uint32 query_event_id;
		std::vector<MSG_S2C::stDonationLadderAck::info> v;
		uint32 local_query;
	};
public:
	void Initialize();
	BOOL CreateFrame(UControl* Frame);
	void SetContentShow(eContent eIndex);

	void UpdateLadder();
public:
	//������Ϣ
	void ParseDonationEventList( const std::vector<MSG_S2C::stDonationEventList::evt>& vList);
	void ParseDonationHistory(const std::vector<MSG_S2C::stDonationHistory::evt>& vList);
	void ParseDonationAck(uint8 result);
	void ParseDonationLadder(bool IsTotal, uint32 query_event_id, uint32 local_query, const std::vector<MSG_S2C::stDonationLadderAck::info>& vList);
	void ParseAddDonationHistory(	uint32 event_id, uint32 datatime, uint32 yuanbao);

	void SendDonationReq(uint32 event_id, uint32 yuanbao, const char* leave_word = NULL);
	void SendQueryDonationLadder(uint32 event_id, bool is_total);
public:
	bool LoadDonationEventName(ui32 DonationId, std::string& Name);
	bool LoadDonationEventURL(ui32 DonationId, std::string& URL);
	bool LoadDonationDes(ui32 DonationId, UINT DesNum, std::string& desc);
	UTexturePtr LoadDonationPhoto(ui32 DonationId, UINT DesNum);

	bool GetDonationEventTime(ui32 DonationId, uint32& starttime, uint32& endtime);

	UINT GetCurrDonationEventIndex();
	void SetCurrDonationEventIndex(UINT index);
	bool GetCurrDonationEvent(MSG_S2C::stDonationEventList::evt& evt);
	bool GetPrevDonationEvent(MSG_S2C::stDonationEventList::evt& evt);
	bool GetNextDonationEvent(MSG_S2C::stDonationEventList::evt& evt);

	bool GetCurrDonationLadder(EndowmentManager::singleLadder& st);
	bool GetPrevDonationLadder(EndowmentManager::singleLadder& st);
	bool GetNextDonationLadder(EndowmentManager::singleLadder& st);

	bool GetTotalDonationLadder(EndowmentManager::singleLadder& st);

	bool GetDonationHistoryList(UINT Current, std::vector<MSG_S2C::stDonationHistory::evt>& vList);
protected:

protected:
	UControl* m_EndowmentFrame;
	UControl* m_EndowmentDesc;
	class UEndowmentMain_Content* m_UEmMain;
	class UEndowmentRank_Content* m_UEmRank;
	class UEndowmentHistory_Content* m_UEmHistory;

	UINT mUiCurrDonationEvent;
	std::vector<MSG_S2C::stDonationEventList::evt> mDonationEventList;

	std::vector<MSG_S2C::stDonationHistory::evt> mDonationHistory;

	UINT mUiCurrDonationLadder;
	typedef std::map<uint32, singleLadder> SingleLadderMap;
	SingleLadderMap mDonationSingleLadder;
	singleLadder mDonationTotalLadder;
};
class UBoardControl : public UControl
{
	UDEC_CLASS(UBoardControl);
public:
	UBoardControl();
	~UBoardControl();
protected:
	virtual BOOL OnCreate();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	USkinPtr m_Board;
};
class UEndowmentFrame : public UDialog
{
	UDEC_CLASS(UEndowmentFrame);
public:
	UEndowmentFrame();
	~UEndowmentFrame();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
};

class UEndowmentMain_Content : public UControl
{
	UDEC_CLASS(UEndowmentMain_Content);
	UDEC_MESSAGEMAP();
public:
	static uint32 m_EventId;
	static uint32 m_JZCoins;
	static void JZOK();

	UEndowmentMain_Content();
	~UEndowmentMain_Content();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);

	void OnURLClicked(UNM* pNM);

	void OnPrevPhoto();
	void OnNextPhoto();

	void OnEventChange();
	void OnPrevEvent();
	void OnNextEvent();

	void OnClickDonation();

	void SetDateToUI();
	void SetPhoto();
public:
	void SetCurrentContent();
protected:
	UINT m_UiCurrentPhoto;
	MSG_S2C::stDonationEventList::evt m_Event;

	UTexturePtr m_Photo;
	URect m_PhotoRect;
};
class UEndowmentRank_Content : public UControl
{
	UDEC_CLASS(UEndowmentRank_Content);
	UDEC_MESSAGEMAP();
public:
	UEndowmentRank_Content();
	~UEndowmentRank_Content();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	void OnPrevLadder();
	void OnNextLadder();

	void SetTotalLadder();
	void SetSingleLadder();
	void SetSingleLadderOther();
public:
	void OnSetLadder(bool is_total);

protected:
	USpecListBox* m_pkAllList;
	USpecListBox* m_pkSingleList;
	EndowmentManager::singleLadder m_stSingleLadder;
};
class UEndowmentHistory_Content : public UControl
{
	UDEC_CLASS(UEndowmentHistory_Content);
	UDEC_MESSAGEMAP();
public:
	UEndowmentHistory_Content();
	~UEndowmentHistory_Content();
public:
	void SetHistorySize(int size);
	void SetCurrentEndowmentHistory();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	
	void OnNextPage();
	void OnPrevPage();
	void AddEndowmentHistoryItem(ui64 time, const char* content, ui32 yuanbao);
protected:
	USpecListBox* m_pkList;
	UINT mUiCurrentPage;
	UINT mUiMaxiumPage;
};
class UEndowmentTableBtn_Content : public UControl
{
	UDEC_CLASS(UEndowmentTableBtn_Content);
	UDEC_MESSAGEMAP();
public:
	UEndowmentTableBtn_Content();
	~UEndowmentTableBtn_Content();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	void OnClickEndowmentMain();
	void OnClickEndowmentRank();
	void OnClickEndowmentHistory();
	void OnClickEndowmentDesc();
};
#endif