#include "stdafx.h"
#include "UEquRefineManager.h"
#include "UMessageBox.h"
#include "Network/PacketBuilder.h"
#include "UIIndex.h"
#include "LocalPlayer.h"
#include "ItemStorage.h"
#include "ObjectManager.h"
#include "UFun.h"
#include "PlayerInputMgr.h"
#include "ItemManager.h"
#include "ItemSlotContainer.h"
#include "ItemSlot.h"
#include "Effect/EffectManager.h"

void ReleaseLock(ui64 ItemGuid)
{
	CPlayer* pLocal = ObjectMgr->GetLocalPlayer();
	if (pLocal)
	{
		CStorage* pBagStorage = pLocal->GetItemContainer();
		if (pBagStorage)
		{
			CItemSlot* pSlot = NULL;
			if (ItemGuid)
			{
				pSlot = (CItemSlot*)pBagStorage->GetSlotByGuid(ItemGuid);
				if (pSlot)
				{
					UINT cBagPos = pBagStorage->GetUIPos(ItemGuid);
					pSlot->SetLock(FALSE);
					SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG, cBagPos, pSlot->GetCode(), pSlot->GetNum(), UItemSystem::ITEM_DATA_FLAG, FALSE);
				}
			}
		}
	}
}

UEquRefineManager::UEquRefineManager()
{
	m_RefineControl = NULL;

	mSlotList[0] = NULL;
	mSlotList[1] = NULL;
	mSlotList[2] = NULL;
}
UEquRefineManager::~UEquRefineManager()
{
	
}
BOOL UEquRefineManager::CreateMgrControl(UControl* ctrl)
{
	ClearSocketGemList();
	if (ctrl)
	{
		if (m_RefineControl == NULL)
		{
			UControl* pkControl = UControl::sm_System->CreateDialogFromFile("refine\\refine.udg");
			m_RefineControl = UDynamicCast(URefineEqu, pkControl);
			if (m_RefineControl == NULL)
			{
				ULOG("create refine UI Faild!");
				return FALSE;
			}else
			{
				ctrl->AddChild(m_RefineControl);
				m_RefineControl->SetId(FRAME_IG_REFINEDLG);	
			}
		}
		m_RefineControl->SetVisible(FALSE);
		return TRUE;
	}
	
	return FALSE;
}
void UEquRefineManager::InitRefineData()
{
	if (m_RefineControl)
	{
		m_RefineControl->InitRefineBtn();
	}
}
BOOL UEquRefineManager::SendAddMaterials(ui8 cPos, ui8 refineSlot, BOOL to_inventory)
{
	//需要判断是否符合条件
		
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return FALSE;
	}
	int BagSlot = pkLocal->GetItemContainer()->GetServerBag(cPos) ;
	int Slot = pkLocal->GetItemContainer()->GetServerPos(cPos);
	if (BagSlot == -1 || Slot == -1)
	{
		return FALSE;
	}
	ULOG("add Material to refine Mgr");

	RefineType pkCurType = m_RefineControl->GetCurRefineType();

	switch(pkCurType)
	{
	case t_Refine:
		return PacketBuilder->SendAddRefineMaterial(BagSlot, Slot, refineSlot, to_inventory);
		break;

	//case t_Compound:
	//	return TRUE;
	//	break;

	case t_Embed:
		{
			if (!to_inventory)
			{
				UpdataGem(refineSlot, cPos, pkCurType);
			}
		}
		return TRUE;
		break;
	}
	return TRUE;
}
BOOL UEquRefineManager::SendRefineMsg(RefineType stType)
{
	switch(stType)
	{
	case t_Refine:
		{
			return  SYState()->LocalPlayerInput->BeginSkillAttack(JINGLIAN_SPELL_ID);	
		}
		break;
	//case t_Compound:
	//	return FALSE;
	//	break;
	case t_Embed:
		return SendSocketGems();
		break;
	}
	return FALSE;
}
BOOL UEquRefineManager::SendSocketGems()
{
	std::vector<ui64> GemsGuid;
	GemsGuid.push_back(mSocketGemList[0]);
	GemsGuid.push_back(mSocketGemList[1]);
	GemsGuid.push_back(mSocketGemList[2]);

	if (mSocketGemList[0] || mSocketGemList[1] || mSocketGemList[2])
	{
		return PacketBuilder->SendSocketGems(mSocketItem, GemsGuid);
	}
	return FALSE;
}
void UEquRefineManager::ParseJingLianResult(ui8 sResult)
{
	if (m_RefineControl)
	{
		m_RefineControl->ParseResult(sResult);
	}

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return ;
	}
	//条件不符合的时候
	std::string text ;
	switch (sResult)
	{
	case JINGLIAN_RESULT_SUCCESS:
		{
	
		}
		return ;
	case JINGLIAN_SPELL_USE_FAILD:
		{
			text = _TRAN("精炼被打断！");
		}
		break;
	case JINGLIAN_RESULT_FAILED:
		{
			
		}
		return ;
	case JINGLIAN_RESULT_CHECK_SUCCESS:
		return ;
	case JINGLIAN_RESULT_NO_MATCH_GEM:
		text = _TRAN("没有相匹配的宝石！");
		break;
	case JINGLIAN_RESULT_NOT_ENOUGH_GOLD:
		text = _TRAN("升级所需要的钱不够！");
		break;
	case JINGLIAN_RESULT_INVALID_ITEM:
		text = _TRAN("无法升级的物品！");
		break;
	case JINGLIAN_RESULT_ALREADY_MAX_LEVEL:
		text = _TRAN("已经是最高级别了！");
		break ;
	}
	EffectMgr->AddEffectText(text.c_str());
	//UMessageBox::MsgBox_s(text.c_str());
	ClientSystemNotify(text.c_str());
}
void UEquRefineManager::ShowDefaultMaterials(ui64 guid)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal || guid == 0)
	{
		return ;
	}
	
	if (guid == pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_ITEM))
	{
		UpDataMaterials(0, guid, 0);
		return ;
	}
	
	if (guid == pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_MAINGEM))
	{
		UpDataMaterials(1, guid, 0);
		return ;
	}

	if (guid == pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM1))
	{
		UpDataMaterials(2, guid, 0);
		return ;
	}
	if (guid == pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM2))
	{
		UpDataMaterials(3, guid, 0);
		return;
	}
	if (guid == pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM3))
	{
		UpDataMaterials(4, guid, 0);
		return ;
	}
	if (guid == pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM4))
	{
		UpDataMaterials(5, guid, 0);
		return;
	}
}
void UEquRefineManager::ChangeRefineType(RefineType stType)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
		return ;

	ClearSocketGemList();
	switch(stType)
	{
	case t_Refine:
		EquRefineMgr->UpDataMaterials(0, pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_ITEM), 0);
		EquRefineMgr->UpDataMaterials(1, pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_MAINGEM), 0);
		EquRefineMgr->UpDataMaterials(2, pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM1), 0);
		EquRefineMgr->UpDataMaterials(3, pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM2), 0);
		EquRefineMgr->UpDataMaterials(4, pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM3), 0);
		EquRefineMgr->UpDataMaterials(5, pkLocal->GetUInt64Value(PLAYER_FIELD_JINGLIAN_SLOT_GEM4), 0);
		break;
	//case t_Compound:
	//	break;
	case t_Embed:
		m_RefineControl->SetInitInfoText(stType);
		ClearSocketGemList();
		break;
	}
}
void UEquRefineManager::UpDataMaterials(ui8 Slot, ui64 guid, UINT  stType)
{
	if (m_RefineControl)
	{
		ui32 itemid = 0; 
		UINT Count = 0;
		if (guid)
		{
			SYItem* pkItem = (SYItem*)ObjectMgr->GetObject(guid);
			if (pkItem)
			{
				itemid = pkItem->GetCode();
				Count = pkItem->GetNum();
			}
		}
		m_RefineControl->UpDataMaterials(Slot,itemid,guid,Count, stType);
	}
	
}

void UEquRefineManager::UpdataGem(ui8 Slot, ui8 cPos, UINT stType)
{
	ui64 guid = 0;
	CPlayer * pLocalplayer = ObjectMgr->GetLocalPlayer();
	CItemSlot* pSlot = NULL;
	CStorage* pItemSlotContainer = NULL;
	if (pLocalplayer)
	{
		pItemSlotContainer = pLocalplayer->GetItemContainer();
		if (pItemSlotContainer)
		{
			pSlot = (CItemSlot*)pItemSlotContainer->GetSlot(cPos);
			if (pSlot)
			{
				guid = pSlot->GetGUID();
			}
		}
	}

	if (m_RefineControl)
	{
		ui32 itemid = 0; 
		UINT Count = 0;
		if (guid)
		{
			SYItem* pkItem = (SYItem*)ObjectMgr->GetObject(guid);
			if (pkItem)
			{
				itemid = pkItem->GetCode();
				Count = pkItem->GetNum();
			}
		}
		ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(itemid);			
		bool bSucc = true;
		switch(Slot)
		{
		case 0:
			if (ItemInfo)
			{
				bSucc &= (ItemInfo->Class == ITEM_CLASS_ARMOR || ItemInfo->Class == ITEM_CLASS_WEAPON);
				int count = 0;
				for (int i = 0 ; i < 3 ; i++)
				{
					count += ItemInfo->Sockets[i].SocketColor > 0;
				}
				bSucc &= count > 0;
			}
			if (bSucc)
			{
				ClearSocketGemList();
				mSocketItem = guid;
			}
			break;
		case 6:case 7: case 8:
			{
				if (ItemInfo)
				{
					bSucc &= (ItemInfo->Class == ITEM_CLASS_JEWELRY);
					if (bSucc)
					{
						ReleaseLock(mSocketGemList[Slot -6]);
						mSocketGemList[Slot - 6] = guid;
						if (pItemSlotContainer && guid)
						{
							mSlotList[Slot - 6] = (CItemSlot*)pItemSlotContainer->GetSlotByGuid(guid);
						}
					}
				}
			}
			break;
		}
		if (bSucc)
		{
			if (guid)
			{
				pSlot->SetLock(TRUE);
				SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG, cPos, pSlot->GetCode(),pSlot->GetNum(),UItemSystem::ITEM_DATA_FLAG,TRUE);
			}
			m_RefineControl->UpDataMaterials(Slot, itemid, guid, Count, stType);
		}
	}
}

void UEquRefineManager::ShowOrHidenRefineUI()
{
	if (m_RefineControl)
	{
		m_RefineControl->SetVisible(!m_RefineControl->IsVisible());
	}
}

void UEquRefineManager::ClearSocketGemList()
{
	ReleaseLock(mSocketItem);
	mSocketItem = 0;
	ReleaseLock(mSocketGemList[0]);
	mSocketGemList[0] = 0;
	ReleaseLock(mSocketGemList[1]);
	mSocketGemList[1] = 0;
	ReleaseLock(mSocketGemList[2]);
	mSocketGemList[2] = 0;


	mSlotList[0] = NULL;
	mSlotList[1] = NULL;
	mSlotList[2] = NULL;
}

void UEquRefineManager::UpdateSocketGemList(uint64 guid)
{
	if (guid == mSocketItem)
	{
		if (mSlotList[0])
			mSlotList[0]->SetLock(FALSE);
		mSocketGemList[0] = 0;
		if (mSlotList[1])
			mSlotList[1]->SetLock(FALSE);
		mSocketGemList[1] = 0;
		if (mSlotList[2])
			mSlotList[2]->SetLock(FALSE);
		mSocketGemList[2] = 0;

		mSlotList[0] = NULL;
		mSlotList[1] = NULL;
		mSlotList[2] = NULL;
	}
}