#ifndef __UEQUREFINEMANAGR_H__
#define __UEQUREFINEMANAGR_H__

#include "URefineEqu.h"
#include "UInc.h"
#include "Singleton.h"

//考虑到以后可能要添加打造的功能， 这里把打造和精炼一起管理

#define EquRefineMgr UEquRefineManager::GetSingleton()

#define  JINGLIAN_SPELL_USE_FAILD  (JINGLIAN_RESULT_CHECK_SUCCESS + 1)
#define  JINGLIAN_SPELL_ID 87

class UEquRefineManager : public Singleton<UEquRefineManager>
{
	SINGLETON(UEquRefineManager)
public:
	//create
	BOOL CreateMgrControl(UControl* ctrl);  //UI
	//init data
	void InitRefineData();
	
	BOOL SendAddMaterials(ui8 cPos, ui8 refineSlot, BOOL to_inventory);  //添加材料 to_inventory = 0 包到精炼
	
	BOOL SendRefineMsg(RefineType stType);	// 发送
	BOOL SendSocketGems();
	
	void ParseJingLianResult(ui8 sResult);	 //处理精炼返回结果										 
	
	// update
	
	void UpDataMaterials(ui8 Slot, ui64 guid, UINT  stType);//更新精炼的材料
	void UpdataGem(ui8 Slot, ui8 cPos, UINT stType);
	
	void ShowDefaultMaterials(ui64 guid);	//显示初始装备
	
	
	void ShowOrHidenRefineUI();

	void ChangeRefineType(RefineType stType);

	void ClearSocketGemList();
	void UpdateSocketGemList(uint64 guid);
protected:
	//UI
	URefineEqu* m_RefineControl ;
public:
	ui64 mSocketItem;
	ui64 mSocketGemList[3];
	class CItemSlot* mSlotList[3];
};
#endif