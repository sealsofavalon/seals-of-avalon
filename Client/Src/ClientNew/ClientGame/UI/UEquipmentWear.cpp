#include "stdafx.h"
#include "UEquipmentWear.h"
#include "LocalPlayer.h"
#include "EquipmentContainer.h"
#include "EquipSlot.h"
#include "ObjectManager.h"

UIMP_CLASS(UColorBitMap, UBitmap);
void UColorBitMap::StaticInit()
{

}

UColorBitMap::UColorBitMap()
{

}
UColorBitMap::~UColorBitMap()
{
	m_CurColor = LCOLOR_WHITE;
}

void UColorBitMap::OnRender(const UPoint& offset, const URect &updateRect)
{
	URect ClientRect(offset, m_Size);
	if (m_spTexture)
	{
		sm_UiRender->DrawImage(m_spTexture, updateRect, m_CurColor);
	}

	RenderChildWindow(offset, updateRect);
}
void UColorBitMap::SetColor(UColor color)
{
	m_CurColor = color;
}

//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////


UIMP_CLASS(UEquipmentWear, UControl);
void UEquipmentWear::StaticInit()
{

}

UEquipmentWear::UEquipmentWear()
{
	for (int index = EQUIPMENT_SLOT_START; index < EQUIPMENT_SLOT_FINGER1; index ++)
	{
		m_Equipment[index] = EQU_None;
	}
	m_IsUpdateChild = FALSE;
}
BOOL UEquipmentWear::OnCreate()
{
	if(!UControl::OnCreate())
		return FALSE;

	for (int index = EQUIPMENT_SLOT_START; index < EQUIPMENT_SLOT_FINGER1; index ++)
	{
		m_Equipment[index] = EQU_None;
	}

	return TRUE;
}
void UEquipmentWear::OnDestroy()
{
	for (int index = EQUIPMENT_SLOT_START; index < EQUIPMENT_SLOT_FINGER1; index ++)
	{
		m_Equipment[index] = EQU_None;
	}
	UControl::OnDestroy();
}
UEquipmentWear::~UEquipmentWear()
{

}

UINT UEquipmentWear::GetEquipState(UINT MaxDurability, UINT Durability)
{
	if (Durability == 0)
	{
		return EQU_Bad;
	}
	
	if (Durability > 10)
	{
		return EQU_CanUse ;
	}else
	{
		return EQU_NeedRepair;
	}
}

UColor UEquipmentWear::GetColor(UINT index)
{
	UColor color = LCOLOR_WHITE;

	switch (index)
	{
	case EQU_CanUse:
		break;
	case EQU_NeedRepair:
		color = UColor(255,255,0);
		break;
	case EQU_Bad:
		color = UColor(255,0,0) ;
		break;
	case EQU_None:
		break;
	default:
		break;
	}

	return color;
}

void UEquipmentWear::RemoveEquipment(UINT index)
{
	if (index >= EQUIPMENT_SLOT_START && index < EQUIPMENT_SLOT_FINGER1 && index != EQUIPMENT_SLOT_NECK)
	{
		UColor color = GetColor(EQU_None);
		UColorBitMap* pkControl = UDynamicCast(UColorBitMap, GetChildByID(index));
		if (pkControl)
		{
			pkControl->SetColor(color);
		}

		m_Equipment[index] = EQU_None;


		SetUIShow();
	}
}
void UEquipmentWear::SetUIShow()
{
	BOOL NeedShowUI = FALSE ;
	BOOL NeedShowWeapon = FALSE ;  //武器是否显示

	for (int i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_FINGER1; i++)
	{
		if (i == EQUIPMENT_SLOT_NECK)
		{
			continue ;
		}

		if (m_Equipment[i] == EQU_Bad || m_Equipment[i] == EQU_NeedRepair)
		{
			NeedShowUI = TRUE;
			if ( i == EQUIPMENT_SLOT_MAINHAND || i == EQUIPMENT_SLOT_OFFHAND)
			{
				NeedShowWeapon = TRUE;
			}
		}
	}

	SetVisible(NeedShowUI);
	GetChildByID(EQUIPMENT_SLOT_MAINHAND)->SetVisible(NeedShowWeapon);
	GetChildByID(EQUIPMENT_SLOT_OFFHAND)->SetVisible(NeedShowWeapon);
}
void UEquipmentWear::UpdataEquipment(ui64 guid)
{
	if (guid == 0)
	{
		return ;
	}

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return ;
	}
	CEquipmentContainer* pkEquipContainer = (CEquipmentContainer*)pkLocal->GetContainer(ICT_EQUIP);
	
	BOOL NeedShow = FALSE;  //是否需要显示装备
	BOOL NeedShowW = FALSE;  //是否需要显示武器

	for (int index = EQUIPMENT_SLOT_START; index < EQUIPMENT_SLOT_FINGER1; index ++)
	{
		if (index == EQUIPMENT_SLOT_NECK)
		{
			continue ; 
		}

		CEquipSlot* pkEquipSlot = (CEquipSlot*)pkEquipContainer->GetSlot(index);

		if (pkEquipSlot && pkEquipSlot->GetGUID() == guid)
		{
			SYItem * Item = (SYItem *)ObjectMgr->GetObject(pkEquipSlot->GetGUID());
			if (!Item)
			{
				return ;
			}

			BYTE MaxDurability = Item->GetMaxDurability();
			BYTE CueDurability = Item->GetDurability();

			UINT EquipState = GetEquipState(MaxDurability, CueDurability);
			UColor color  = GetColor(EquipState);
			
			m_Equipment[index] = EquipState;
			
			UColorBitMap* pkControl = UDynamicCast(UColorBitMap, GetChildByID(index));
			if (pkControl)
			{
				pkControl->SetColor(color);
			}
		}

	}

	SetUIShow();
	
}