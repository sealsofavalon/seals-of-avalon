#ifndef __UEQUIPMENTWEAR_H__
#define __UEQUIPMENTWEAR_H__

#include "UInc.h"
//装备耐久提示

class UColorBitMap : public UBitmap
{
	UDEC_CLASS(UColorBitMap);
public:
	UColorBitMap();
	~UColorBitMap();
	void SetColor(UColor color);
protected:
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
protected:
	UColor m_CurColor;
};

enum
{
	EQU_CanUse,    //可以使用  
	EQU_NeedRepair,//需要修理
	EQU_Bad,       //损坏
	EQU_None,
};

class UEquipmentWear : public UControl
{
	UDEC_CLASS(UEquipmentWear);
public:
	UEquipmentWear();
	~UEquipmentWear();
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	void UpdataEquipment(ui64 guid);
	void RemoveEquipment(UINT index);
	UColor GetColor(UINT index);
	UINT GetEquipState(UINT MaxDurability, UINT Durability);
	
	virtual BOOL HitTest(const UPoint& parentCoordPoint)
	{
		return FALSE;
	}
protected:
	void SetUIShow();
protected:
	UINT m_Equipment[EQUIPMENT_SLOT_FINGER1];
};

#endif