#include "stdafx.h"
#include "UFittingRoom.h"
#include "SYItem.h"
#include "ItemManager.h"
#include "ObjectManager.h"
#include "UNiAVControl.h"
#include "UFun.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UNiAvControlRoleView, UNiAVControlEq)
void UNiAvControlRoleView::StaticInit(){}
UNiAvControlRoleView::UNiAvControlRoleView()
{
	m_TopADD = 160;
}
UNiAvControlRoleView::~UNiAvControlRoleView()
{
}
BOOL UNiAvControlRoleView::OnCreate()
{
	m_TopADD = 160;
	return UNiAVControlEq::OnCreate();
	/*NIASSERT(SYState()->ClientApp);
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	NiRenderer* pRender = SYState()->Render;
	if(pRender == NULL)
		return FALSE;

	if (pRender->GetSorter() == NULL)
	{
		NiAlphaAccumulator* pkAccum = NiNew NiAlphaAccumulator;
		pRender->SetSorter(pkAccum);
	}


	m_VisGeometries.RemoveAll();
	
	m_spDirLight = NiNew NiDirectionalLight;
	m_spDirLight->SetDiffuseColor(NiColor::WHITE * 0.5f);
	m_spDirLight->SetAmbientColor(NiColor::WHITE * 0.5f);

	NiMatrix3 kMat;
	kMat.MakeIdentity();
	NiPoint3 kDir(0.0f, -1.0f, 0.f);
	kDir.Unitize();
	kMat.SetCol(0, kDir);
	m_spDirLight->SetRotate(kMat);
	m_spDirLight->Update(0.f);*/

	//return TRUE;
}
void UNiAvControlRoleView::OnDestroy()
{
	UNiAVControlEq::OnDestroy();
}
void UNiAvControlRoleView::OnTimer(float fDeltaTime)
{
	UNiAVControlEq::OnTimer(fDeltaTime);
}
void UNiAvControlRoleView::AddPlayer(CPlayer* pkPlayer)
{
	UNiAVControlEq::AddPlayer(pkPlayer);
}
void UNiAvControlRoleView::OnRender(const UPoint& offset,const URect &updateRect)
{
	UNiAVControlEq::OnRender(offset, updateRect);
}

void UNiAvControlRoleView::GetPlayerEquipment(CPlayer* pkChar)
{
	UNiAVControlEq::GetPlayerEquipment(pkChar);
}

float UNiAvControlRoleView::GetCharScale()
{

	float scale = 0.8f;
	if (m_Char && m_IsPlayer)
	{
		UINT Race = m_Char->GetRace();
		UINT Sex = m_Char->GetGender();

		switch(Race)
		{
		case RACE_REN:
			{
				if (Sex == GENDER_FEMALE)
				{
					return 1.04f * scale;
				}else
				{
					return 1.0f* scale;
				}
			}
			break;
		case RACE_WU:
			{
				if (Sex == GENDER_FEMALE)
				{
					return 0.99f* scale;
				}else
				{
					return 0.86f* scale;
				}
			}
			break;
		case RACE_YAO:
			{
				if (Sex == GENDER_FEMALE)
				{
					return 1.13f* scale;
				}else
				{
					return 1.04f* scale;
				}
			}
			break;
		}
	}

	return 1.0f* scale;
}

UIMP_CLASS(UFittingRoom, UDialog)
UBEGIN_MESSAGE_MAP(UFittingRoom, UDialog)
UON_BN_CLICKED(1, &UFittingRoom::OnClickReset)
UON_BN_PRESS(20, &UFittingRoom::OnClickLeft)
UON_BN_PRESS(21, &UFittingRoom::OnClickRight)
UEND_MESSAGE_MAP();
void UFittingRoom::StaticInit(){}

UFittingRoom::UFittingRoom()
{
	m_pPlayer = NULL;
	m_pRoleView = NULL;
}
UFittingRoom::~UFittingRoom()
{
	m_pRoleView = NULL;
}
BOOL UFittingRoom::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}
	if(m_pRoleView == NULL)
	{
		m_pRoleView = UDynamicCast(UNiAvControlRoleView, GetChildByID(0));
		if(!m_pRoleView)
			return FALSE;
	}
	return TRUE;
}
void UFittingRoom::OnDestroy()
{
	UDialog::OnDestroy();
}
BOOL UFittingRoom::OnEscape()
{
	if(!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UFittingRoom::OnClose()
{
	SetVisible(FALSE);
}

void UFittingRoom::OnClickReset()
{
	//m_pRoleView->AddPlayer(m_pPlayer);
	m_pRoleView->GetPlayerEquipment(m_pPlayer);
	m_pRoleView->FillItemEffect( m_pPlayer );
}
void UFittingRoom::OnClickLeft()
{
	m_pRoleView->SetRotatePlayer(0.1f);
}
void UFittingRoom::OnClickRight()
{
	m_pRoleView->SetRotatePlayer(-0.1f);
}

void UFittingRoom::UpDateUNiavobject(uint32 visiblebase, ui32 itemid, ui32 SlotIndex, ui32 Effect_id)
{
	if (m_pRoleView)
	{
		m_pRoleView->UpdatePlayer(visiblebase, itemid,SlotIndex,Effect_id);
	}
}

void UFittingRoom::FittingEquipment(ui32 Itementry, ItemExtraData* sItemData)
{
	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(Itementry);
	if(!ItemInfo)
		return;
	if(ItemInfo->Class == ITEM_CLASS_WEAPON || ItemInfo->Class == ITEM_CLASS_ARMOR)
	{
		SetVisible(TRUE);

		//TODO:Fitting Equipment On Avatar


		int SlotIndex = ItemInfo->InventoryType; //物品类型
		int index = 0;
		int itemID = ItemInfo->ItemId;

		if(SlotIndex == INVTYPE_HEAD)
		{
			index = INVTYPE_HOUNTER_TOUBU;
		}
		else if(SlotIndex == INVTYPE_SHOULDERS)
		{
			index = INVTYPE_HOUNTER_JIANBANG;
		}
		else if(SlotIndex == INVTYPE_CHEST)
		{
			index = INVTYPE_HOUNTER_SHANGYI;
		}
		else if(SlotIndex == INVTYPE_TROUSERS)
		{
			index = INVTYPE_HOUNTER_XIAYI;
		}
		else if(SlotIndex == INVTYPE_BOOTS)
		{
			index = INVTYPE_HOUNTER_XIEZI;
		}

		int visiblebase = PLAYER_VISIBLE_ITEM_1_0 + (index * 12);
		ui32 effect_displayid = 0;
		ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (index * 12) + 9 ;

		if(sItemData)
		{
			GetRefineItemEffect(itemID, sItemData->jinglian_level, effect_displayid);
			m_pPlayer->SetUInt32Value(itemeffect,effect_displayid);
		}

		UpDateUNiavobject(visiblebase, itemID, SlotIndex, effect_displayid);
	}
}
void UFittingRoom::FittingEquipment(ui32 Itementry, ui64 guid)
{
	SYItem* Item = (SYItem *)ObjectMgr->GetObject(guid);
	if (Item)
	{
		ItemExtraData NewItemExtraData;
		NewItemExtraData.jinglian_level = Item->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
		for( int i = 0; i < 6; ++i )
			NewItemExtraData.enchants[i] =  Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + i * 3 );
		FittingEquipment(Itementry, &NewItemExtraData);
	}else
		FittingEquipment(Itementry, (ItemExtraData*)NULL);
}

void UFittingRoom::SetVisible(BOOL bShow)
{
	if ( bShow )
	{
		//m_pRoleView->SetZ(1.0f);
		if ( !IsVisible() )
			OnClickReset(); //第一次显示需要reset
	}

	UDialog::SetVisible(bShow);
}

void UFittingRoom::AddViewPlayer(CPlayer* player)
{
	m_pPlayer = player;
	m_pRoleView->AddPlayer(player);
}