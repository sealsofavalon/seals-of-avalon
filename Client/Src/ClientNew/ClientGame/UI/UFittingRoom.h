#ifndef __UFITTINGROOM_H__
#define __UFITTINGROOM_H__
#include "UInc.h"
#include "UIPlayerEquipment.h"

class UNiAvControlRoleView : public UNiAVControlEq
{
	UDEC_CLASS(UNiAvControlRoleView);
public:
	UNiAvControlRoleView();
	~UNiAvControlRoleView();

	virtual void AddPlayer(CPlayer* pkChar);   //����
	virtual BOOL OnCreate();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void GetPlayerEquipment(CPlayer* pkChar);

	virtual float GetCharScale();
};

class UFittingRoom : public UDialog
{
	UDEC_CLASS(UFittingRoom);
	UDEC_MESSAGEMAP();
public:
	UFittingRoom();
	~UFittingRoom();

	void UpDateUNiavobject(uint32 visiblebase, ui32 itemid, ui32 SlotIndex, ui32 Effect_id = 0);

	void AddViewPlayer(class CPlayer* player);

	void FittingEquipment(ui32 Itementry, ItemExtraData* sItemData);
	void FittingEquipment(ui32 Itementry, ui64 guid);

	virtual void SetVisible(BOOL bShow);

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();

	void OnClickReset();
	void OnClickLeft();
	void OnClickRight();

	CPlayer* m_pPlayer;

	class UNiAvControlRoleView* m_pRoleView;
};

#endif