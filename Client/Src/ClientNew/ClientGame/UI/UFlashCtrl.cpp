// ***************************************************************
//  Copyright (C) Sunyou 2007-2008 - All Rights Reserved
//	
//  UFlashCtrl.cpp  Created : P.K. 2008-08-26
//  
// ***************************************************************
#include "StdAfx.h"
#include "UFlashCtrl.h"
#if SUPPORT_GFX
#pragma comment(lib, "winmm.lib")
#pragma comment(lib , "GFx.lib")
//#pragma comment(lib , "GMain.lib")
#pragma comment(lib , "GFx_D3D9.lib")
#pragma comment(lib , "libjpeg.lib")

#ifdef NIDEBUG
#pragma comment(lib, "zlibd.lib")
#else
#pragma comment(lib, "zlib.lib")
#endif 


UIMP_CLASS(UFlashCtrl, UControl)
void UFlashCtrl::StaticInit()
{
	UREG_PROPERTY("swf", UPT_STRING, UFIELD_OFFSET(UFlashCtrl, m_strSWFFile));
}

UFlashCtrl::UFlashCtrl(void)
{
}

UFlashCtrl::~UFlashCtrl(void)
{
}

BOOL UFlashCtrl::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (!m_strSWFFile.Empty())
	{
		SetMovie(m_strSWFFile.GetBuffer());
	}
	return TRUE;
}

void UFlashCtrl::OnDestroy()
{
	m_spMovie = NULL;
	UControl::OnDestroy();
}

void UFlashCtrl::OnMove(const UPoint& NewOffsetDelta)
{
	if (m_spMovie)
	{
		UPoint pt = WindowToScreen(UPoint(0,0));
		m_spMovie->SetViewport(pt.x, pt.y, pt.x + m_Size.x, pt.y + m_Size.y);
	}
}

void UFlashCtrl::OnSize(const UPoint& NewSize)
{
	if (m_spMovie)
	{
		UPoint pt = WindowToScreen(m_Position);
		m_spMovie->SetViewport(pt.x, pt.y, pt.x + NewSize.x, pt.y + NewSize.y);
	}
	
}

void UFlashCtrl::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_spMovie)
	{
		m_spMovie->Render();
	}
}

void UFlashCtrl::OnTimer(float fDeltaTime)
{
	if (m_spMovie)
	{
		m_spMovie->Advance(fDeltaTime);
	}
}

BOOL UFlashCtrl::SetMovie(const char* pszSWF)
{
	if (pszSWF == NULL || pszSWF[0] == 0)
	{
		m_spMovie->SetEventSink(NULL);
		m_spMovie = NULL;
		return TRUE;
	}
	m_spMovie = SyGfxManager::CreateFlashMovie(pszSWF);
	if (m_spMovie)
	{
		UPoint pt = WindowToScreen(m_Position);
		m_spMovie->SetViewport(pt.x, pt.y, pt.x + m_Size.x, pt.y + m_Size.y);
		m_spMovie->SetEventSink(this);
		m_spMovie->SetBackgroundAlpha(0.0f);
		NIASSERT(m_spMovie->GetMovieView());
		return TRUE;
	}
	return FALSE;
}

void UFlashCtrl::OnCommand(const char* pcommand, const char* parg)
{


}

void UFlashCtrl::OnGetFocus()
{
	if (m_bCreated && m_Visible && m_Active && m_spMovie && m_spMovie->IsEnable())
	{
		GFxMovieView* pMovieView = m_spMovie->GetMovieView();
		pMovieView->HandleEvent(GFxEvent::SetFocus);
	}
}

void UFlashCtrl::OnLostFocus()
{
	if (m_bCreated && m_Visible && m_Active && m_spMovie && m_spMovie->IsEnable())
	{
		GFxMovieView* pMovieView = m_spMovie->GetMovieView();
		pMovieView->HandleEvent(GFxEvent::KillFocus);
	}
}

void UFlashCtrl::OnMouseUp(const UPoint& position, UINT flags)
{
	if (m_bCreated && m_Visible && m_Active && m_spMovie && m_spMovie->IsEnable())
	{
		GFxMovieView* pMovieView = m_spMovie->GetMovieView();
		UPoint pt = ScreenToWindow(position);
		GFxMouseEvent MouseEvent(GFxEvent::MouseUp, 0, pt.x, pt.y);
		pMovieView->HandleEvent(MouseEvent);
	}
}
void UFlashCtrl::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	if (m_bCreated && m_Visible && m_Active && m_spMovie && m_spMovie->IsEnable())
	{
		GFxMovieView* pMovieView = m_spMovie->GetMovieView();
		UPoint pt = ScreenToWindow(point);
		GFxMouseEvent MouseEvent(GFxEvent::MouseDown, 0, pt.x, pt.y);
		pMovieView->HandleEvent(MouseEvent);
		SetFocusControl();
	}
}
void UFlashCtrl::OnMouseMove(const UPoint& position, UINT flags)
{
	if (m_bCreated && m_Visible && m_Active && m_spMovie && m_spMovie->IsEnable())
	{
		GFxMovieView* pMovieView = m_spMovie->GetMovieView();
		UPoint pt = ScreenToWindow(position);
		GFxMouseEvent MouseMoveEvent(GFxEvent::MouseMove, 0, pt.x, pt.y);
		pMovieView->HandleEvent(MouseMoveEvent);
	}
}
void UFlashCtrl::OnMouseDragged(const UPoint& position, UINT flags)
{
	OnMouseMove(position, flags);
}

void UFlashCtrl::OnMouseEnter(const UPoint& position, UINT flags)
{
	SetFocusControl();
}

void UFlashCtrl::OnMouseLeave(const UPoint& position, UINT flags)
{
	ClearFocus();
}

BOOL UFlashCtrl::OnChar(UINT nCharCode, UINT nRepCnt, UINT nFlags)
{
	if (m_bCreated && m_Visible && m_Active && m_spMovie && m_spMovie->IsEnable())
	{
		GFxCharEvent charEvent(nCharCode);
		GFxMovieView* pMovieView = m_spMovie->GetMovieView();
		pMovieView->HandleEvent(charEvent);
	}
	return TRUE;
}

BOOL UFlashCtrl::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	if (m_bCreated && m_Visible && m_Active && m_spMovie && m_spMovie->IsEnable())
	{
		GFxMovieView* pMovieView = m_spMovie->GetMovieView();
		GFxKey::Code eGfxCode = SyGfxManager::LKeyToGFxKey((KeyCodes)nKeyCode);
		if (eGfxCode != GFxKey::VoidSymbol)
		{
			GFxKeyEvent KeyEvent(GFxKeyEvent::KeyDown, eGfxCode, eGfxCode);
			
			if (nFlags & LKM_ALT )
			{
				KeyEvent.SpecialKeysState.SetAltPressed(true);
			}
			
			if (nFlags & LKM_CTRL)
			{
				KeyEvent.SpecialKeysState.SetCtrlPressed(true);
			}

			if (nFlags & LKM_SHIFT)
			{
				KeyEvent.SpecialKeysState.SetShiftPressed(true);
			}

			pMovieView->HandleEvent(KeyEvent);
		}
		
	}
	
	return TRUE;
}

BOOL UFlashCtrl::OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	if (m_bCreated && m_Visible && m_Active && m_spMovie && m_spMovie->IsEnable())
	{
		GFxMovieView* pMovieView = m_spMovie->GetMovieView();
		GFxKey::Code eGfxCode = SyGfxManager::LKeyToGFxKey((KeyCodes)nKeyCode);
		if (eGfxCode != GFxKey::VoidSymbol)
		{
			GFxKeyEvent KeyEvent(GFxKeyEvent::KeyUp, eGfxCode, eGfxCode);

			if (nFlags & LKM_ALT )
			{
				KeyEvent.SpecialKeysState.SetAltPressed(true);
			}

			if (nFlags & LKM_CTRL)
			{
				KeyEvent.SpecialKeysState.SetCtrlPressed(true);
			}

			if (nFlags & LKM_SHIFT)
			{
				KeyEvent.SpecialKeysState.SetShiftPressed(true);
			}

			pMovieView->HandleEvent(KeyEvent);
		}

	}
	return TRUE;
}

#endif 