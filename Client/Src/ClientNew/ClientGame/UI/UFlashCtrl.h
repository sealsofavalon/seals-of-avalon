// ***************************************************************
//  Copyright (C) Sunyou 2007-2008 - All Rights Reserved
//	
//  UFlashCtrl.h  Created : P.K. 2008-08-26
//  Scaleform gfx warpper.
// ***************************************************************
#pragma once


#include "SyGfxManager.h"
#if SUPPORT_GFX
#pragma warning(disable : 4005)
#include <GFxLoader.h>
#include <GFxPlayer.h>
#include <GFxImageResource.h>



class UFlashCtrl : public UControl , IFlashMovieEventSink
{
	UDEC_CLASS(UFlashCtrl);
public:
	UFlashCtrl(void);
	virtual ~UFlashCtrl(void);
	
	BOOL SetMovie(const char* pszSWF);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnSize(const UPoint& NewSize);
	virtual void OnMove(const UPoint& NewOffsetDelta);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
	virtual void OnCommand(const char* pcommand, const char* parg);
	//virtual void OnEvent(const GFxEvent& event) = 0;
	virtual void OnGetFocus();
	virtual void OnLostFocus();

	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
	virtual BOOL OnChar(UINT nCharCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
private:
	
protected:
	UString		m_strSWFFile;
	FlashMoviePtr m_spMovie;
};

#endif 