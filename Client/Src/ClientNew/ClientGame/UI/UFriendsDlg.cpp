#include "stdafx.h"
#include "UFriendsDlg.h"
#include "../Network/PacketBuilder.h"
#include "UMessageBox.h"
#include "UIGamePlay.h"
#include "ObjectManager.h"
#include "UIRightMouseList.h"
#include "UInGameBar.h"
#include "UIShowPlayer.h"
#include "SocialitySystem.h"
#include "UFun.h"
//////////////////////////////////////////////////////////////////////////
class UIFriendListItem : public UControl
{
	UDEC_CLASS(UIFriendListItem);
public:
	UIFriendListItem()
	{
		m_NameStateText = NULL;
		m_LevelStateText = NULL;
		m_ClassStateText = NULL;
		m_AreaStateText = NULL;
		m_GUID = 0;
		m_RightMouseDown = FALSE;
		m_bOnline = FALSE;
		m_bChoose = false;
		m_MouseDown = FALSE;
		m_Type = BITMAPLIST_FRIEND;
	}
	~UIFriendListItem()
	{

	}
public:
	void SetBeChoose(BOOL choose)
	{
		m_bChoose = choose;
		UpdataMemTextColor();
	}
	void SetMemberOnline(BOOL sta)
	{
		m_bOnline = sta;
		UpdataMemTextColor();
	}
	void UpdataMemTextColor()
	{
		UColor color = m_bChoose?m_Style->m_FontColorHL:m_Style->m_FontColor;
		if (!m_bOnline){
			color.Set(128, 128, 128, 255);
		}
		m_NameStateText->SetTextColor(color);
		m_AreaStateText->SetTextColor(color);
		m_ClassStateText->SetTextColor(color);
		m_LevelStateText->SetTextColor(color);

		m_NameStateText->SetTextEdge(m_bChoose);
		m_AreaStateText->SetTextEdge(m_bChoose);
		m_ClassStateText->SetTextEdge(m_bChoose);
		m_LevelStateText->SetTextEdge(m_bChoose);

	}
	void SetMemberBase(const char* Name, ui8 Gender, ui32 level, ui32 Class, ui64 guid, const char* Text)
	{
		if (Name)
		{
			m_NameStateText->SetText(Name);
			m_Name = Name;
		}

		char buf[1024];
		buf[0] = 0;
		if (m_bOnline && level)
		{
			itoa(level, buf, 10);
		}
		else
		{
			sprintf_s(buf, 1024, "??");
		}
		m_LevelStateText->SetText(_TRAN(buf));
		if (!m_bOnline) Class = 0;
		char szClass[8][20] = { "??", "武修", "羽箭", "仙道", "真巫", "变身", "刺客", "" };
		m_ClassStateText->SetText(_TRAN(szClass[Class]));

		m_AreaStateText->SetText(Text);

		m_GUID = guid;
		m_Type = BITMAPLIST_FRIEND;
	}
	void SetMemberBase(const char* Name, const char* Text, ui8 race, ui32 Class, ui64 guid )
	{
		if (Name)
		{
			m_NameStateText->SetText(Name);
			m_Name = Name;
		}

		m_LevelStateText->SetText(_TRAN(race_name[race]));

		char szClass[8][20] = { "??", "武修", "羽箭", "仙道", "真巫", "变身", "刺客", "" };
		m_ClassStateText->SetText(_TRAN(szClass[Class]));

		m_AreaStateText->SetText(Text);

		m_GUID = guid;
		m_Type = BITMAPLIST_BLACKLIST;
	}

	bool GetName(std::string& str)
	{
		if (m_Name.size())
		{
			str = m_Name;
			return true;
		}
		return false;
	}
	bool GetGuid(ui64& guid)
	{
		if (m_GUID)
		{
			guid = m_GUID;
			return true;
		}
		return false;
	}
	bool IsMemOnline()
	{
		return m_bOnline?true:false;
	}
protected:
	virtual BOOL OnCreate()
	{
		if (!UControl::OnCreate())
		{
			return FALSE;
		}
		if (m_NameStateText == NULL)
		{
			m_NameStateText = UDynamicCast(UStaticText, GetChildByID(0));
			if (!m_NameStateText)
			{
				return FALSE;
			}
			m_NameStateText->SetTextAlign(UT_CENTER);
		}
		if (m_LevelStateText == NULL)
		{
			m_LevelStateText = UDynamicCast(UStaticText, GetChildByID(1));
			if (!m_LevelStateText)
			{
				return FALSE;
			}
			m_LevelStateText->SetTextAlign(UT_CENTER);
		}
		if (m_ClassStateText == NULL)
		{
			m_ClassStateText = UDynamicCast(UStaticText, GetChildByID(2));
			if (!m_ClassStateText)
			{
				return FALSE;
			}
			m_ClassStateText->SetTextAlign(UT_CENTER);
		}
		if (m_AreaStateText == NULL)
		{
			m_AreaStateText = UDynamicCast(UStaticText, GetChildByID(3));
			if (!m_AreaStateText)
			{
				return FALSE;
			}
			m_AreaStateText->SetTextAlign(UT_CENTER);
		}
		return TRUE;
	}
	virtual void OnDestroy()
	{
		if (m_bChoose)
		{
			if (SocialitySys->GetFriendSysPtr())
			{
				SocialitySys->GetFriendSysPtr()->SetSelectItem(NULL);
			}

			if (SocialitySys->GetBlackListSysPtr())
			{
				SocialitySys->GetBlackListSysPtr()->SetSelectItem(NULL);
			}
		}
		UControl::OnDestroy();
	}
	virtual void OnMouseEnter(const UPoint& position, UINT flags)
	{

	}
	virtual void OnMouseLeave(const UPoint& position, UINT flags)
	{

	}
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
	{
		m_MouseDown = TRUE;
	}
	virtual void OnMouseUp(const UPoint& position, UINT flags)
	{
		//FriendSys SetSelectItem
		if (m_MouseDown)
		{
			m_MouseDown = FALSE;
			if (m_Type == BITMAPLIST_FRIEND)
			{
				if (SocialitySys->GetFriendSysPtr())
				{
					SocialitySys->GetFriendSysPtr()->SetSelectItem(this);
				}
			}else if (m_Type == BITMAPLIST_BLACKLIST)
			{
				if (SocialitySys->GetBlackListSysPtr())
				{
					SocialitySys->GetBlackListSysPtr()->SetSelectItem(this);
				}
			}
		}
	}
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
	{
		m_RightMouseDown = TRUE;
	}
	virtual void OnRightMouseUp(const UPoint& position, UINT flags)
	{
		if (m_RightMouseDown)
		{
			UIRightMouseList* pkRightMouseList = INGAMEGETFRAME(UIRightMouseList,FRAME_IG_RIGHTMOUSELIST);
			if(pkRightMouseList)
			{
				pkRightMouseList->Popup(position, m_Type, m_Name.c_str(), m_GUID);
			}
			m_RightMouseDown = FALSE;

			if (m_Type == BITMAPLIST_FRIEND)
			{
				if (SocialitySys->GetFriendSysPtr())
				{
					SocialitySys->GetFriendSysPtr()->SetSelectItem(this);
				}
			}else if (m_Type == BITMAPLIST_BLACKLIST)
			{
				if (SocialitySys->GetBlackListSysPtr())
				{
					SocialitySys->GetBlackListSysPtr()->SetSelectItem(this);
				}
			}
		}
	}
	virtual void OnRender(const UPoint& offset, const URect& updateRect)
	{
		if(m_bChoose && SocialitySys && SocialitySys->mBKGtexture)
			sm_UiRender->DrawImage(SocialitySys->mBKGtexture, updateRect);

		RenderChildWindow(offset, updateRect);
	}
protected:
	BOOL		 m_bOnline;
	bool		 m_bChoose;
	BOOL		 m_RightMouseDown;
	BOOL		 m_MouseDown;
	ui64		 m_GUID;
	std::string  m_Name;
	UStaticText* m_NameStateText;
	UStaticText* m_LevelStateText;
	UStaticText* m_ClassStateText;
	UStaticText* m_AreaStateText;

	int m_Type;
};
UIMP_CLASS(UIFriendListItem, UControl)
void UIFriendListItem::StaticInit(){}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIFriendList, UControl)
void UIFriendList::StaticInit(){}
UIFriendList::UIFriendList()
{
}
UIFriendList::~UIFriendList()
{

}

void UIFriendList::SetMemberOnline(ui64 guid)
{
	ui64 GUID = 0;
	for (unsigned int ui = 0 ; ui < 20 ; ui++)
	{
		UIFriendListItem* pItem = UDynamicCast(UIFriendListItem, GetChildByID(ui));
		if (pItem)
		{
			pItem->GetGuid(GUID);
			if (GUID == guid)
			{
				pItem->SetMemberOnline(TRUE);
			}
		}
	}
}

void UIFriendList::SetMemberOffline(ui64 guid)
{
	ui64 GUID = 0;
	for (unsigned int ui = 0 ; ui < 20 ; ui++)
	{
		UIFriendListItem* pItem = UDynamicCast(UIFriendListItem, GetChildByID(ui));
		if (pItem)
		{
			pItem->GetGuid(GUID);
			if (GUID == guid)
			{
				pItem->SetMemberOnline(FALSE);
			}
		}
	}
}
BOOL UIFriendList::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	return TRUE;
}
void UIFriendList::OnDestroy()
{
	UControl::OnDestroy();
}	
void UIFriendList::OnRender(const UPoint& offset,const URect &updateRect)
{
	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	g_pRenderInterface->SetScissorRect(sm_UiRender->GetClipRect());

	RenderChildWindow(offset, updateRect);

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
}
void UIFriendList::UpdataList()
{
	std::vector<MSG_S2C::stFriend_List::stFriend> FriendList;
	if(!SocialitySys->GetFriendSysPtr()->GetFriendList(FriendList))
		return;

	int curpage, maxpage;
	SocialitySys->GetFriendSysPtr()->GetPage(curpage, maxpage);

	for (unsigned int ui = 0 ; ui < 20 ; ui++)
	{
		UIFriendListItem* pItem = UDynamicCast(UIFriendListItem, GetChildByID(ui));
		if (pItem)
		{
			UINT pos = curpage * 20 + ui;
			bool bShow = pos < FriendList.size();
			pItem->SetVisible(bShow);
			if (bShow)
			{
				pItem->SetMemberOnline(FriendList[ui].bOnline);
				if (FriendList[ui].bOnline)
				{
					pItem->SetMemberBase(FriendList[pos].name.c_str(), 0, FriendList[pos].level, FriendList[pos].Class, FriendList[pos].guid,_TRAN("苍之陆"));
				}
				else
				{
					pItem->SetMemberBase(FriendList[pos].name.c_str(), 0, 0, 0, FriendList[pos].guid,_TRAN("苍之陆"));
				}
			}
		}
	}
	if (SocialitySys->GetFriendSysPtr())
	{
		SocialitySys->GetFriendSysPtr()->SetSelectItem(NULL);
	}
}
void UIFriendList::ClearList()
{
	if (SocialitySys->GetFriendSysPtr())
	{
		SocialitySys->GetFriendSysPtr()->SetSelectItem(NULL);
	}
	sm_System->SetMouseControl(NULL);
	for (unsigned int ui = 0 ; ui < 20 ; ui++)
	{
		UIFriendListItem* pItem = UDynamicCast(UIFriendListItem, GetChildByID(ui));
		if (pItem)
		{
			pItem->SetVisible(FALSE);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIFriendsFrame, UDialog)
UBEGIN_MESSAGE_MAP(UIFriendsFrame, UDialog)
UON_BN_CLICKED(1,&UIFriendsFrame::OnClickBtnSortByName)
UON_BN_CLICKED(2,&UIFriendsFrame::OnClickBtnSortLevel)
UON_BN_CLICKED(3,&UIFriendsFrame::OnClickBtnSortByClass)
UON_BN_CLICKED(4,&UIFriendsFrame::OnClickBtnSortByArea)
UON_BN_CLICKED(41,&UIFriendsFrame::OnClickBtnRemoveMem)
UON_BN_CLICKED(40,&UIFriendsFrame::OnClickBtnAddMember)
UON_BN_CLICKED(50,&UIFriendsFrame::OnClickBtnFriendPage)
UON_BN_CLICKED(51,&UIFriendsFrame::OnClickBtnGuildPage)
UON_BN_CLICKED(52,&UIFriendsFrame::OnClickBtnOLPlayerPage)
UON_BN_CLICKED(53,&UIFriendsFrame::OnClickBtnTeamPage)
UON_BN_CLICKED(54,&UIFriendsFrame::OnClickBtnGroupApplyPage)
UON_BN_CLICKED(71,&UIFriendsFrame::OnClickBtnBlackListPage)
UON_BN_CLICKED(60,&UIFriendsFrame::OnPrevPage)
UON_BN_CLICKED(62,&UIFriendsFrame::OnNextPage)
UEND_MESSAGE_MAP()
void UIFriendsFrame::StaticInit(){}
UIFriendsFrame::UIFriendsFrame()
{
	m_FriendList = NULL;
	m_IsUpdateChild = FALSE;
}
UIFriendsFrame::~UIFriendsFrame()
{

}
BOOL UIFriendsFrame::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	if (m_FriendList == NULL)
	{
		m_FriendList = UDynamicCast(UIFriendList, GetChildByID(0));
		if (!m_FriendList)
		{
			return FALSE;
		}
	}
	UButton* pCheckButton = (UButton*)GetChildByID(50);
	if (pCheckButton == NULL)
	{
		return FALSE;
	}
	pCheckButton->SetCheck(FALSE);

	pCheckButton = (UButton*)GetChildByID(70);
	if (pCheckButton){
		pCheckButton->SetCheck(FALSE);
	}

	pCheckButton = (UButton*)GetChildByID(51);
	if (pCheckButton == NULL)
	{
		return FALSE;
	}
	pCheckButton->SetActive(false);
	m_bCanDrag = FALSE;
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(42));
	if (pText)
	{
		pText->SetTextEdge(true);
	}
	return TRUE;
}
void UIFriendsFrame::OnDestroy()
{
	m_FriendList->ClearList();
	UDialog::OnDestroy();
}
void UIFriendsFrame::OnClose()
{
	if(SocialitySys) SocialitySys->HideAll();
	UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	if (pGameBar)
	{
		UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONFRIEND);
		if (pBtn)
		{
			pBtn->SetCheckState(FALSE);
		}
		//SocialitySys->GetGuildSysPtr()->HideFrame();
	}
}
BOOL UIFriendsFrame::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}

void UIFriendsFrame::SetVisible(BOOL value)
{
	UDialog::SetVisible(value);
}

void UIFriendsFrame::SetGuildBtnActive(bool active)
{
	UButton* pCheckButton = (UButton*)GetChildByID(51);
	if (pCheckButton == NULL)
	{
		return;
	}
	pCheckButton->SetActive(active);
}
void UIFriendsFrame::OnClickBtnRemoveMem()
{
	if (SocialitySys->GetFriendSysPtr())
	{
		SocialitySys->GetFriendSysPtr()->RemoveSelect();
	}
}
void UIFriendsFrame::OnClickBtnAddMember()
{//	
	if (TeamShow && TeamShow->GetTargetID())
	{
		CPlayer * pkLocalPlayer = ObjectMgr->GetLocalPlayer();
		CCharacter * pChar = (CCharacter*)ObjectMgr->GetObject(TeamShow->GetTargetID());
		if (pChar && pChar->isType(TYPE_PLAYER) && pChar->IsFriendly(pkLocalPlayer) && pChar != pkLocalPlayer)
		{
			SocialitySys->GetFriendSysPtr()->SendNetMessageInviteMem(pChar->GetName());
		}
		else
		{
			SocialitySys->GetFriendSysPtr()->OnCallFriendEdit();
		}
	}
	else if (TeamShow->GetTargetID() == 0 && SocialitySys->GetFriendSysPtr())
	{
		//TODO 输入对方姓名
		SocialitySys->GetFriendSysPtr()->OnCallFriendEdit();
	}
}
void UIFriendsFrame::OnClickBtnSortByName()
{
	SocialitySys->GetFriendSysPtr()->SortMemberByName();
	m_FriendList->UpdataList();
}
void UIFriendsFrame::OnClickBtnSortLevel()
{
	SocialitySys->GetFriendSysPtr()->SortMemberByLevel();
	m_FriendList->UpdataList();
}
void UIFriendsFrame::OnClickBtnSortByClass()
{
	SocialitySys->GetFriendSysPtr()->SortMemberByClass();
	m_FriendList->UpdataList();
}
void UIFriendsFrame::OnClickBtnSortByArea()
{
	SocialitySys->GetFriendSysPtr()->SortMemberByArea();
	m_FriendList->UpdataList();
}
void UIFriendsFrame::OnClickBtnFriendPage()
{

}
void UIFriendsFrame::OnClickBtnGuildPage()
{
	if (SocialitySys){
		SocialitySys->ShowFrame(0);
	}
}
void UIFriendsFrame::OnClickBtnOLPlayerPage()
{
	if (SocialitySys){
		SocialitySys->ShowFrame(2);
	}
}
void UIFriendsFrame::OnClickBtnTeamPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(3);
	}
}
void UIFriendsFrame::OnClickBtnGroupApplyPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(4);
	}
}
void UIFriendsFrame::OnClickBtnBlackListPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(5);
	}
}

void UIFriendsFrame::Show()
{
	SetVisible(TRUE);
}
void UIFriendsFrame::Hide()
{
	SetVisible(FALSE);
	UButton* pCheckButton = (UButton*)GetChildByID(50);
	if (pCheckButton){
		pCheckButton->SetCheck(FALSE);
	}
	pCheckButton = (UButton*)GetChildByID(70);
	if (pCheckButton){
		pCheckButton->SetCheck(FALSE);
	}
}
void UIFriendsFrame::OnPrevPage()
{
	UControl* pBtn = GetChildByID(60);
	if (pBtn)
	{
		pBtn->SetActive(SocialitySys->GetFriendSysPtr()->PrevPage());
	}
	int curpage, maxpage;
	SocialitySys->GetFriendSysPtr()->GetPage(curpage, maxpage);
	pBtn = GetChildByID(62);
	if (pBtn)
	{
		pBtn->SetActive(maxpage > 0 && curpage < maxpage);
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(61));
	if (pText)
	{
		char buf[256];
		sprintf(buf, "%d/%d", curpage + 1, maxpage + 1);
		pText->SetText(buf);
	}
	m_FriendList->UpdataList();
}

void UIFriendsFrame::OnNextPage()
{
	UControl* pBtn = GetChildByID(62);
	if (pBtn)
	{
		pBtn->SetActive(SocialitySys->GetFriendSysPtr()->NextPage());
	}
	int curpage, maxpage;
	SocialitySys->GetFriendSysPtr()->GetPage(curpage, maxpage);
	pBtn = GetChildByID(60);
	if (pBtn)
	{
		pBtn->SetActive(maxpage > 0 && curpage > 0);
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(61));
	if (pText)
	{
		char buf[256];
		sprintf(buf, "%d/%d", curpage + 1, maxpage + 1);
		pText->SetText(buf);
	}
	m_FriendList->UpdataList();
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
FriendSys::FriendSys()
{
	m_FriendFrame = NULL;
	m_bSelectItem = NULL;
	m_FriendsEditDlg = NULL;
	m_CurrentPage = 0;
	m_MaxPage = 0;
}
FriendSys::~FriendSys()
{

}
BOOL FriendSys::CreateFrame(UControl* ctrl)
{
	UControl* NewCtrl = NULL;
	if (m_FriendFrame == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\UFriendDlg.udg");
		m_FriendFrame = UDynamicCast(UIFriendsFrame, NewCtrl);
		if (m_FriendFrame)
		{
			ctrl->AddChild(m_FriendFrame);
			m_FriendFrame->SetId(FRAME_IG_FRIENDDLG);
		}
		else
		{
			return FALSE;
		}
	}
	return TRUE;
}
void FriendSys::DestoryFrame()
{

}
void FriendSys::ShowFrame()
{
	m_FriendFrame->Show();
}
void FriendSys::HideFrame()
{
	m_FriendFrame->Hide();
}
void FriendSys::ParseNetMessage(MSG_S2C::stFriend_Stat stMessage)
{
	m_stFriendStat = stMessage;
	switch(m_stFriendStat.friend_result)
	{
	case FRIEND_LIST_FULL:
		break;
	case FRIEND_ONLINE:
		{
			for(unsigned int ui = 0; ui < m_stFriendList.vFriends.size(); ++ui)
			{
				if(m_stFriendList.vFriends[ui].guid == m_stFriendStat.guid)
				{
					std::string Name = m_stFriendList.vFriends[ui].name;
					m_stFriendList.vFriends[ui].guid = m_stFriendStat.guid;
					m_stFriendList.vFriends[ui].bOnline = true;
					m_stFriendList.vFriends[ui].level = m_stFriendStat.level;
					m_stFriendList.vFriends[ui].Class = m_stFriendStat.Class;
					ParseNetMessage(m_stFriendList);
					
					ClientSystemNotify(_TRAN(N_NOTICE_F86,Name.c_str(),Name.c_str()).c_str());
					SYState()->IAudio->PlayUiSound( "Sound/UI/friendOnline.wav" );
					break;
				}
			}
		}
		break;
	case FRIEND_OFFLINE:
		{
			for(unsigned int ui = 0; ui < m_stFriendList.vFriends.size(); ++ui)
			{
				if(m_stFriendList.vFriends[ui].guid == m_stFriendStat.guid)
				{
					std::string Name = m_stFriendList.vFriends[ui].name;
					m_stFriendList.vFriends[ui].guid = m_stFriendStat.guid;
					m_stFriendList.vFriends[ui].bOnline = false;
					m_stFriendList.vFriends[ui].level = 0;//m_stFriendStat.level;
					m_stFriendList.vFriends[ui].Class = 0;//m_stFriendStat.Class;
					ParseNetMessage(m_stFriendList);
					ClientSystemNotify(_TRAN(N_NOTICE_F87,Name.c_str()).c_str());
					break;
				}
			}
		}
		break;
	case FRIEND_REMOVED:
		{
			std::vector<MSG_S2C::stFriend_List::stFriend>::iterator it = m_stFriendList.vFriends.begin();
			while(it != m_stFriendList.vFriends.end())
			{
				if (it->guid == m_stFriendStat.guid)
				{
					ClientSystemNotify(_TRAN(N_NOTICE_F88, it->name.c_str()).c_str());
					m_stFriendList.vFriends.erase(it);
					if (m_FriendFrame)
					{
						m_FriendFrame->GetFriendList()->UpdataList();
						//m_FriendFrame->GetFriendList()->RemoveMember(m_stFriendStat.guid);
					}
					ParseNetMessage(m_stFriendList);
					break;
				}
				++it;
			}
		}
		break;
	case FRIEND_ADDED_ONLINE:
		{
			MSG_S2C::stFriend_List::stFriend stTemp;
			stTemp.bOnline = true;
			stTemp.Class = m_stFriendStat.Class;
			//stTemp.flag = m_stFriendStat.f
			stTemp.note = m_stFriendStat.note;
			stTemp.guid = m_stFriendStat.guid;
			stTemp.level = m_stFriendStat.level;
			stTemp.name = m_stLastAddName;
			m_stFriendList.vFriends.push_back(stTemp);
			if(m_stLastAddName.size())
			{
				ParseNetMessage(m_stFriendList);
			}
			else
			{
				SendNetMessageQueryName(m_stFriendStat.guid);
			}
		}
		break;
	case FRIEND_ADDED_OFFLINE:
		{
			MSG_S2C::stFriend_List::stFriend stTemp;
			stTemp.bOnline = false;
			stTemp.Class = 0;//m_stFriendStat.Class;
			//stTemp.flag = m_stFriendStat.f
			stTemp.note = m_stFriendStat.note;
			stTemp.guid = m_stFriendStat.guid;
			stTemp.level = 0;//m_stFriendStat.level;
			stTemp.name = m_stLastAddName;
			m_stFriendList.vFriends.push_back(stTemp);
			if(m_stLastAddName.size())
			{
				ParseNetMessage(m_stFriendList);
			}
			else
			{
				SendNetMessageQueryName(m_stFriendStat.guid);
			}
		}
		break;
	case FRIEND_ALREADY:
		{
			UMessageBox::MsgBox_s(_TRAN("已经是好友了"));
		}
		break;
	case FRIEND_SELF:
		{
			UMessageBox::MsgBox_s(_TRAN("无法加自己为好友"));
		}
		break;
	case FRIEND_ENEMY:
		{
			UMessageBox::MsgBox_s(_TRAN("无法加敌对阵营玩家"));
		}
		break;
	case FRIEND_NOT_FOUND:
		{
			UMessageBox::MsgBox_s(_TRAN("没有找到该玩家"));
		}
		break;
	case FRIEND_IGNORE_FULL:
		{
			UMessageBox::MsgBox_s(_TRAN("黑名单已满"));
		}
		break;
	case FRIEND_IGNORE_SELF:
		{
			UMessageBox::MsgBox_s(_TRAN("不能将自己放入黑名单"));
		}
		break;
	case FRIEND_IGNORE_NOT_FOUND:
		{
			UMessageBox::MsgBox_s(_TRAN("没有找到该玩家"));
		}
		break;
	case FRIEND_IGNORE_ALREADY:
		{
			UMessageBox::MsgBox_s(_TRAN("该玩家已经在黑名单中了"));
		}
		break;
	case FRIEND_IGNORE_ADDED:
		{
			MSG_S2C::stFriend_List::stIgnores temp;
			temp.Class = m_stFriendStat.Class;
			temp.guid = m_stFriendStat.guid;
			temp.GuildName = m_stFriendStat.guildname;
			temp.name = m_stFriendStat.name;
			temp.Race = m_stFriendStat.race;
			m_stFriendList.vIgnores.push_back(temp);

			ParseNetMessage(m_stFriendList);
			string str = _TRAN(N_NOTICE_C92, m_stFriendStat.name.c_str());
			ClientSystemNotify(str.c_str());
		}
		break;
	case FRIEND_IGNORE_REMOVED:
		{
			std::vector<MSG_S2C::stFriend_List::stIgnores>::iterator it = m_stFriendList.vIgnores.begin();
			while(it != m_stFriendList.vIgnores.end())
			{
				if (it->guid == m_stFriendStat.guid)
				{
					m_stFriendList.vIgnores.erase(it);
					ParseNetMessage(m_stFriendList);
					break;
				}
				++it;
			}
		}
		break;
	}
	m_MaxPage = m_stFriendList.vFriends.size() / 20;
}

void FriendSys::ParseNetMessage(MSG_S2C::stFriend_List stMessage)
{
	m_stFriendList = stMessage;
	m_MaxPage = m_stFriendList.vFriends.size() / 20;
	if (m_stFriendList.vFriends.size()%20 == 0 && m_MaxPage != 0)
	{
		--m_MaxPage;
	}
	UIFriendList* pFriendList = NULL;
	if (m_FriendFrame)
	{
		m_FriendFrame->OnNextPage();
		m_FriendFrame->OnPrevPage();
		pFriendList = m_FriendFrame->GetFriendList();
	}
	for(unsigned int ui = 0; ui < m_stFriendList.vFriends.size() ; ++ui)
	{
		if (!m_stFriendList.vFriends[ui].bOnline)
		{
			m_stFriendList.vFriends[ui].level = 0;
			m_stFriendList.vFriends[ui].Class = 0;
		}
	}
	if (pFriendList)
	{
		pFriendList->ClearList();
		SortMemberByOnline();
		pFriendList->UpdataList();
	}

	SocialitySys->GetBlackListSysPtr()->ParseIgnoreList(m_stFriendList.vIgnores);
}

void FriendSys::OnNameReflesh()
{
	bool bReflesh = false;
	for(unsigned int ui = 0; ui < m_stFriendList.vFriends.size(); ++ui)
	{
		if(m_stFriendList.vFriends[ui].name.size() == 0)
		{
			sObjBaseInfo* pkBase = ObjectMgr->GetObjBaseInfo(m_stFriendList.vFriends[ui].guid);
			if(pkBase)
			{
				m_stFriendList.vFriends[ui].name = pkBase->name;
				bReflesh = true;
			}
		}
	}

	if(bReflesh)
	{
		ParseNetMessage(m_stFriendList);
	}
}

void FriendSys::SendNetMessageDelMem(ui64 guid)
{
	PacketBuilder->SendFriendDelMsg(guid);
}

void FriendSys::SendNetMessageInviteMem(const char* Name)
{
	std::string strName = Name;
	std::string strNote = Name;
	SetLastAddName(Name);
	PacketBuilder->SendFriendAddMsg(strName, strNote);
}

void FriendSys::SendNetMessageQueryName(ui64 guid)
{
	PacketBuilder->SendQueryNameMsg(guid);
}

void FriendSys::SetSelectItem(UIFriendListItem* item)
{
	//注意 有可能控件已经被删除
	if (m_bSelectItem)
	{
		m_bSelectItem->SetBeChoose(FALSE);
	}
	if (item)
	{
		m_bSelectItem = item;
		m_bSelectItem->SetBeChoose(TRUE);
	}
	else
	{
		m_bSelectItem = NULL;
	}
}

void FriendSys::SetGuildActive(bool active)
{
	if (m_FriendFrame)
	{
		m_FriendFrame->SetGuildBtnActive(active);
	}
}

void FriendSys::DelFriend()
{
	ui64 GUID;
	if(m_bSelectItem->GetGuid(GUID))
	{
		SendNetMessageDelMem(GUID);
	}
}

bool FriendSys::GetFriendList(std::vector<MSG_S2C::stFriend_List::stFriend>& vList)
{
	vList = m_stFriendList.vFriends;
	return true;
}

void FriendSys::AcceptDelFriend()
{
	if (SocialitySys)
	{
		SocialitySys->GetFriendSysPtr()->DelFriend();
	}
}

void FriendSys::DeclineDelFriend()
{

}

void FriendSys::RemoveSelect()
{	
	if (m_bSelectItem)
	{
		UMessageBox::MsgBox(_TRAN("确定删除该好友么？"), 
			(BoxFunction*)FriendSys::AcceptDelFriend,
			(BoxFunction*)FriendSys::DeclineDelFriend);
	}
}

void FriendSys::OnCallFriendEdit()
{
	if(m_FriendsEditDlg == NULL)
		m_FriendsEditDlg = INGAMEGETFRAME(UIAddMemberEdit, FRAME_IG_ADDMEMBERDLG);
	m_FriendsEditDlg->OnAddMember(0);
}

void FriendSys::SetLastAddName(const char* Name)
{
	m_stLastAddName = Name;
}
bool keyName(const MSG_S2C::stFriend_List::stFriend& left, const MSG_S2C::stFriend_List::stFriend& right){
	return left.name > right.name;
}
void FriendSys::SortMemberByName()
{
	//TODO SORT
	std::vector<MSG_S2C::stFriend_List::stFriend> vTemp = m_stFriendList.vFriends;
	std::sort(vTemp.begin(), vTemp.end(), &keyName);
	m_stFriendList.vFriends = vTemp;
}
bool KeyLevel(const MSG_S2C::stFriend_List::stFriend& left, const MSG_S2C::stFriend_List::stFriend& right){
	return left.level > right.level;
}
void FriendSys::SortMemberByLevel()
{
	//TODO SORT
	std::vector<MSG_S2C::stFriend_List::stFriend> vTemp = m_stFriendList.vFriends;
	std::sort(vTemp.begin(), vTemp.end(), &KeyLevel);
	m_stFriendList.vFriends = vTemp;
}
bool KeyClass(const MSG_S2C::stFriend_List::stFriend& left, const MSG_S2C::stFriend_List::stFriend& right){
	return left.Class >= right.Class;
}
void FriendSys::SortMemberByClass()
{
	//TODO SORT
	std::vector<MSG_S2C::stFriend_List::stFriend> vTemp = m_stFriendList.vFriends;
	std::sort(vTemp.begin(), vTemp.end(), &KeyClass);
	m_stFriendList.vFriends = vTemp;
}
void FriendSys::SortMemberByArea()
{
	//TODO SORT
}
void FriendSys::SortMemberByOnline()
{
	//TODO SORT
	std::vector<MSG_S2C::stFriend_List::stFriend> tempVec;
	std::map<std::string, MSG_S2C::stFriend_List::stFriend> tempMapOnline,tempMapOffline;
	for (unsigned int ui = 0 ; ui < m_stFriendList.vFriends.size() ; ui++)
	{
		std::string Name = m_stFriendList.vFriends[ui].name;
		if (m_stFriendList.vFriends[ui].bOnline)
		{
			tempMapOnline.insert(std::map<std::string, MSG_S2C::stFriend_List::stFriend>::value_type(Name, m_stFriendList.vFriends[ui]));
		}
		else
		{
			tempMapOffline.insert(std::map<std::string, MSG_S2C::stFriend_List::stFriend>::value_type(Name, m_stFriendList.vFriends[ui]));
		}
	}
	std::map<std::string, MSG_S2C::stFriend_List::stFriend>::iterator it = tempMapOnline.begin();
	while(it != tempMapOnline.end())
	{
		tempVec.push_back(it->second);
		++it;
	}
	it = tempMapOffline.begin();
	while(it != tempMapOffline.end())
	{
		tempVec.push_back(it->second);
		++it;
	}
	m_stFriendList.vFriends = tempVec;
}

bool FriendSys::PrevPage()
{
	if (m_CurrentPage > 0)
	{
		--m_CurrentPage;
		return m_CurrentPage != 0;
	}
	return false;
}

bool FriendSys::NextPage()
{
	if (m_CurrentPage < m_MaxPage)
	{
		++m_CurrentPage;
		return m_CurrentPage != m_MaxPage;
	}
	return false;
}

bool FriendSys::GetPage(int& CurPage, int& MaxPage)
{
	CurPage = m_CurrentPage;
	MaxPage = m_MaxPage;
	return true;
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIgnoreList, UControl)
void UIgnoreList::StaticInit(){}
UIgnoreList::UIgnoreList()
{
}
UIgnoreList::~UIgnoreList()
{

}

void UIgnoreList::SetMemberOnline(ui64 guid)
{
	ui64 GUID = 0;
	for (unsigned int ui = 0 ; ui < 20 ; ui++)
	{
		UIFriendListItem* pItem = UDynamicCast(UIFriendListItem, GetChildByID(ui));
		if (pItem)
		{
			pItem->GetGuid(GUID);
			if (GUID == guid)
			{
				pItem->SetMemberOnline(TRUE);
			}
		}
	}
}

void UIgnoreList::SetMemberOffline(ui64 guid)
{
	ui64 GUID = 0;
	for (unsigned int ui = 0 ; ui < 20 ; ui++)
	{
		UIFriendListItem* pItem = UDynamicCast(UIFriendListItem, GetChildByID(ui));
		if (pItem)
		{
			pItem->GetGuid(GUID);
			if (GUID == guid)
			{
				pItem->SetMemberOnline(FALSE);
			}
		}
	}
}
BOOL UIgnoreList::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	return TRUE;
}
void UIgnoreList::OnDestroy()
{
	ClearList();
	UControl::OnDestroy();
}	
void UIgnoreList::OnRender(const UPoint& offset,const URect &updateRect)
{
	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	g_pRenderInterface->SetScissorRect(sm_UiRender->GetClipRect());

	RenderChildWindow(offset, updateRect);

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
}
void UIgnoreList::UpdataList()
{
	std::vector<MSG_S2C::stFriend_List::stIgnores> ignoreList;
	if(!SocialitySys->GetBlackListSysPtr()->GetIgnoreList(ignoreList))
		return;

	int curpage, maxpage;
	SocialitySys->GetBlackListSysPtr()->GetPage(curpage, maxpage);

	for (unsigned int ui = 0 ; ui < 20 ; ui++)
	{
		UIFriendListItem* pItem = UDynamicCast(UIFriendListItem, GetChildByID(ui));
		if (pItem)
		{
			UINT pos = curpage * 20 + ui;
			bool bShow = pos < ignoreList.size();
			pItem->SetVisible(bShow);
			if (bShow)
			{
				pItem->SetMemberOnline(true);
				pItem->SetMemberBase(ignoreList[pos].name.c_str(), ignoreList[pos].GuildName.c_str(), ignoreList[pos].Race, ignoreList[pos].Class, ignoreList[pos].guid);
			}
		}
	}
	if (SocialitySys->GetBlackListSysPtr())
	{
		SocialitySys->GetBlackListSysPtr()->SetSelectItem(NULL);
	}
}
void UIgnoreList::ClearList()
{
	if (SocialitySys->GetBlackListSysPtr())
	{
		SocialitySys->GetBlackListSysPtr()->SetSelectItem(NULL);
	}
	sm_System->SetMouseControl(NULL);
	for (unsigned int ui = 0 ; ui < 20 ; ui++)
	{
		UIFriendListItem* pItem = UDynamicCast(UIFriendListItem, GetChildByID(ui));
		if (pItem)
		{
			pItem->SetVisible(FALSE);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UBlackListDlg, UDialog)
UBEGIN_MESSAGE_MAP(UBlackListDlg, UDialog)
UON_BN_CLICKED(40,&UBlackListDlg::OnCallIgnoreEdit)
UON_BN_CLICKED(41,&UBlackListDlg::OnDelIgnore)
UON_BN_CLICKED(50,&UBlackListDlg::OnClickBtnBlackListPage)
UON_BN_CLICKED(51,&UBlackListDlg::OnClickBtnGuildPage)
UON_BN_CLICKED(52,&UBlackListDlg::OnClickBtnOLPlayerPage)
UON_BN_CLICKED(53,&UBlackListDlg::OnClickBtnTeamPage)
UON_BN_CLICKED(54,&UBlackListDlg::OnClickBtnGroupApplyPage)
UON_BN_CLICKED(70,&UBlackListDlg::OnClickBtnFriendPage)
UEND_MESSAGE_MAP()
void UBlackListDlg::StaticInit()
{

}
UBlackListDlg::UBlackListDlg()
{
	m_pIgnoreList = NULL;
}
UBlackListDlg::~UBlackListDlg()
{

}
void UBlackListDlg::Show()
{
	SetVisible(TRUE);
}
void UBlackListDlg::Hide()
{
	SetVisible(FALSE);
	UButton* pCheckButton = (UButton*)GetChildByID(50);
	if (pCheckButton){
		pCheckButton->SetCheck(FALSE);
	}
	pCheckButton = (UButton*)GetChildByID(71);
	if (pCheckButton){
		pCheckButton->SetCheck(FALSE);
	}
}
void UBlackListDlg::SetGuildBtnActive(bool active)
{
	UButton* pCheckButton = (UButton*)GetChildByID(51);
	if (pCheckButton == NULL)
	{
		return;
	}
	pCheckButton->SetActive(active);
}

void UBlackListDlg::OnClickBtnFriendPage()
{
	if (SocialitySys){
		SocialitySys->ShowFrame(1);
	}
}
void UBlackListDlg::OnClickBtnGuildPage()
{
	if (SocialitySys){
		SocialitySys->ShowFrame(0);
	}
}
void UBlackListDlg::OnClickBtnOLPlayerPage()
{
	if (SocialitySys){
		SocialitySys->ShowFrame(2);
	}
}
void UBlackListDlg::OnClickBtnTeamPage()
{
	if (SocialitySys){
		SocialitySys->ShowFrame(3);
	}
}
void UBlackListDlg::OnClickBtnGroupApplyPage()
{
	if (SocialitySys){
		SocialitySys->ShowFrame(4);
	}
}
void UBlackListDlg::OnClickBtnBlackListPage()
{

}


void UBlackListDlg::OnPrevPage()
{
	UControl* pBtn = GetChildByID(60);
	if (pBtn)
	{
		pBtn->SetActive(SocialitySys->GetBlackListSysPtr()->PrevPage());
	}
	int curpage, maxpage;
	SocialitySys->GetBlackListSysPtr()->GetPage(curpage, maxpage);
	pBtn = GetChildByID(62);
	if (pBtn)
	{
		pBtn->SetActive(maxpage > 0 && curpage < maxpage);
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(61));
	if (pText)
	{
		char buf[256];
		sprintf(buf, "%d/%d", curpage + 1, maxpage + 1);
		pText->SetText(buf);
	}
	m_pIgnoreList->UpdataList();
}

void UBlackListDlg::OnNextPage()
{
	UControl* pBtn = GetChildByID(62);
	if (pBtn)
	{
		pBtn->SetActive(SocialitySys->GetBlackListSysPtr()->NextPage());
	}
	int curpage, maxpage;
	SocialitySys->GetBlackListSysPtr()->GetPage(curpage, maxpage);
	pBtn = GetChildByID(60);
	if (pBtn)
	{
		pBtn->SetActive(maxpage > 0 && curpage > 0);
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(61));
	if (pText)
	{
		char buf[256];
		sprintf(buf, "%d/%d", curpage + 1, maxpage + 1);
		pText->SetText(buf);
	}
	m_pIgnoreList->UpdataList();
}

BOOL UBlackListDlg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;
	if (m_pIgnoreList == NULL)
	{
		m_pIgnoreList = UDynamicCast(UIgnoreList, GetChildByID(0));
		if (!m_pIgnoreList)
		{
			return FALSE;
		}
	}
	return TRUE;
}

void UBlackListDlg::OnCallIgnoreEdit()
{
	UIAddMemberEdit*	FriendsEditDlg = INGAMEGETFRAME(UIAddMemberEdit, FRAME_IG_ADDMEMBERDLG);
	if (FriendsEditDlg)
	{
		FriendsEditDlg->OnAddMember(5);
	}
}

void UBlackListDlg::OnDelIgnore()
{
	SocialitySys->GetBlackListSysPtr()->DeleteCurSel();
}

void UBlackListDlg::OnDestroy()
{
	UDialog::OnDestroy();
}
void UBlackListDlg::OnClose()
{
	if(SocialitySys) SocialitySys->HideAll();
	UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	if (pGameBar)
	{
		UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONFRIEND);
		if (pBtn)
			pBtn->SetCheckState(FALSE);
	}
}
BOOL UBlackListDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}


//////////////////////////////////////////////////////////////////////////
BlackListSys::BlackListSys()
{
	m_BlackListDlg = NULL;
	m_CurrentPage = 0;
	m_MaxPage = 0;
	m_bSelectItem = NULL;
}
BlackListSys::~BlackListSys()
{

}
BOOL BlackListSys::CreateFrame(UControl* ctrl)
{
	UControl* NewCtrl = NULL;
	if (m_BlackListDlg == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\UBlackListDlg.udg");
		m_BlackListDlg = UDynamicCast(UBlackListDlg, NewCtrl);
		if (m_BlackListDlg)
		{
			ctrl->AddChild(m_BlackListDlg);
			m_BlackListDlg->SetId(FRAME_IG_BLACKLISTDLG);
		}
		else
		{
			return FALSE;
		}
	}
	m_CurrentPage = 0;
	m_MaxPage = 0;
	m_vIgnoresList.clear();
	return TRUE;
}
void BlackListSys::DestoryFrame()
{
	m_CurrentPage = 0;
	m_MaxPage = 0;
	m_vIgnoresList.clear();
}
void BlackListSys::ShowFrame()
{
	m_BlackListDlg->Show();
}
void BlackListSys::HideFrame()
{
	m_BlackListDlg->Hide();
}

void BlackListSys::SetGuildActive(bool active)
{
	m_BlackListDlg->SetGuildBtnActive(active);
}

void BlackListSys::ParseIgnoreList(std::vector<MSG_S2C::stFriend_List::stIgnores>& vList)
{
	m_vIgnoresList = vList;
	m_MaxPage = vList.size() / 20;
	if (vList.size() % 20 == 0 && m_MaxPage != 0)
	{
		--m_MaxPage;
	}
	m_CurrentPage = 0;
	UIgnoreList* pIgnoreList = UDynamicCast(UIgnoreList, m_BlackListDlg->GetChildByID(0));
	if (pIgnoreList)
	{
		pIgnoreList->UpdataList();
	}
}

bool BlackListSys::PrevPage()
{
	if (m_CurrentPage > 0)
	{
		--m_CurrentPage;
		return m_CurrentPage != 0;
	}
	return false;
}

bool BlackListSys::NextPage()
{
	if (m_CurrentPage < m_MaxPage)
	{
		++m_CurrentPage;
		return m_CurrentPage != m_MaxPage;
	}
	return false;
}

bool BlackListSys::GetPage(int& CurPage, int& MaxPage)
{
	CurPage = m_CurrentPage;
	MaxPage = m_MaxPage;
	return true;
}

bool BlackListSys::GetIgnoreList(std::vector<MSG_S2C::stFriend_List::stIgnores>& vList)
{
	vList = m_vIgnoresList;
	return true;
}

void BlackListSys::SetSelectItem(class UIFriendListItem* item)
{
	if (m_bSelectItem)
	{
		m_bSelectItem->SetBeChoose(FALSE);
	}
	if (item)
	{
		m_bSelectItem = item;
		m_bSelectItem->SetBeChoose(TRUE);
	}
	else
	{
		m_bSelectItem = NULL;
	}
}

void BlackListSys::SendAddIgnoreList(std::string& name)
{
	PacketBuilder->SendFriendAddIgnore(name);
}

void BlackListSys::SendDelIgnoreList(ui64 guid)
{
	PacketBuilder->SendFriendDelIgnore(guid);
}

void BlackListSys::DeleteCurSel()
{
	if (m_bSelectItem)
	{
		ui64 guid = 0;
		if(m_bSelectItem->GetGuid(guid))
		{
			SendDelIgnoreList(guid);
		}
	}
}