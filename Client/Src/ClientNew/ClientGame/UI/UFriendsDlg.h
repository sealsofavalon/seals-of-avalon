#pragma  once
#include "UInc.h"
#include "USpecListBox.h"
class UIFriendList : public UControl
{
	UDEC_CLASS(UIFriendList);
public:
	UIFriendList();
	~UIFriendList();

	void SetMemberOnline(ui64 guid);
	void SetMemberOffline(ui64 guid);
public:
	//void RemoveMember(ui64 guid);
	void UpdataList();
	void ClearList();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
protected:
	//typedef std::map<ui64, class UIFriendListItem*> FriendItemMap;
	//FriendItemMap mFriendItemMap;
};
class UIFriendsFrame : public UDialog
{
	UDEC_CLASS(UIFriendsFrame);
	UDEC_MESSAGEMAP();
public:
	UIFriendsFrame();
	~UIFriendsFrame();
public:
	virtual void SetVisible(BOOL value);

	void Show();
	void Hide();

	UIFriendList* GetFriendList(){return m_FriendList;}
	void SetGuildBtnActive(bool active);

	void OnClickBtnRemoveMem();
	void OnClickBtnAddMember();
	void OnClickBtnSortByName();
	void OnClickBtnSortLevel();
	void OnClickBtnSortByClass();
	void OnClickBtnSortByArea();
	void OnClickBtnFriendPage();
	void OnClickBtnGuildPage();
	void OnClickBtnOLPlayerPage();
	void OnClickBtnTeamPage();
	void OnClickBtnGroupApplyPage();
	void OnClickBtnBlackListPage();

	void OnPrevPage();
	void OnNextPage();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
protected:
	UIFriendList* m_FriendList;
};

class FriendSys
{
public:
	FriendSys();
	~FriendSys();
public:
	BOOL CreateFrame(UControl* ctrl);
	void DestoryFrame();
	void ShowFrame();
	void HideFrame();
public:
	void ParseNetMessage(MSG_S2C::stFriend_Stat stMessage);
	void ParseNetMessage(MSG_S2C::stFriend_List stMessage);
	
	void OnNameReflesh();

	void SendNetMessageDelMem(ui64 guid);
	void SendNetMessageInviteMem(const char* Name);
	void SendNetMessageQueryName(ui64 guid);
	
	void SortMemberByName();
	void SortMemberByLevel();
	void SortMemberByClass();
	void SortMemberByArea();
	void SortMemberByOnline();

	bool GetFriendList(std::vector<MSG_S2C::stFriend_List::stFriend>& vList);
public:
	void OnCallFriendEdit();
	void SetLastAddName(const char* Name);
	void SetSelectItem(class UIFriendListItem* item);
	void SetGuildActive(bool active);
	void RemoveSelect();
	void DelFriend();
	static void AcceptDelFriend();
	static void DeclineDelFriend();

	bool PrevPage();
	bool NextPage();
	bool GetPage(int& CurPage, int& MaxPage);
private:
	MSG_S2C::stFriend_Stat m_stFriendStat;
	MSG_S2C::stFriend_List m_stFriendList;
	
	class UIFriendListItem* m_bSelectItem;
	std::string m_stLastAddName;
	UIFriendsFrame* m_FriendFrame;
	class UIAddMemberEdit* m_FriendsEditDlg;

	int m_CurrentPage;
	int m_MaxPage;
};

class UIgnoreList : public UControl
{
	UDEC_CLASS(UIgnoreList);
public:
	UIgnoreList();
	~UIgnoreList();

	void SetMemberOnline(ui64 guid);
	void SetMemberOffline(ui64 guid);
public:
	void UpdataList();
	void ClearList();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
};
class UBlackListDlg : public UDialog
{
	UDEC_CLASS(UBlackListDlg);
	UDEC_MESSAGEMAP();
public:
	UBlackListDlg();
	~UBlackListDlg();

	void Show();
	void Hide();
	void SetGuildBtnActive(bool active);

	void OnClickBtnFriendPage();
	void OnClickBtnGuildPage();
	void OnClickBtnOLPlayerPage();
	void OnClickBtnTeamPage();
	void OnClickBtnGroupApplyPage();
	void OnClickBtnBlackListPage();

	void OnPrevPage();
	void OnNextPage();

	void OnCallIgnoreEdit();
	void OnDelIgnore();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();

	UIgnoreList* m_pIgnoreList;
};

class BlackListSys
{
public:
	BlackListSys();
	~BlackListSys();
public:
	BOOL CreateFrame(UControl* ctrl);
	void DestoryFrame();
	void ShowFrame();
	void HideFrame();

	void SetGuildActive(bool active);

	void ParseIgnoreList(std::vector<MSG_S2C::stFriend_List::stIgnores>& vList);
	
	void SendAddIgnoreList(std::string& name);
	void SendDelIgnoreList(ui64 guid);

public:
	bool GetIgnoreList(std::vector<MSG_S2C::stFriend_List::stIgnores>& vList);
	void SetSelectItem(class UIFriendListItem* item);

	bool PrevPage();
	bool NextPage();
	bool GetPage(int& CurPage, int& MaxPage);

	void DeleteCurSel();
private:
	std::vector<MSG_S2C::stFriend_List::stIgnores> m_vIgnoresList;
	UBlackListDlg* m_BlackListDlg;
	int m_CurrentPage;
	int m_MaxPage;

	class UIFriendListItem* m_bSelectItem;
};