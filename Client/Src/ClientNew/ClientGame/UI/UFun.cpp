#include "StdAfx.h"
#include "UFun.h"
#include "Effect/EffectManager.h"
#include "ObjectManager.h"
#include "UIMiniMap.h"
#include "Player.h"
#include "UIGamePlay.h"
#include "UIPickItemList.h"
#include "UIIndex.h"
#include "boost/regex.hpp"
#include "Audio/AudioSystem.h"
#include "Utils/ChatFileterDB.h"
#include "UMessageBox.h"
#include "UChat.h"
#include "UIShowPlayer.h"
#include "UInGameBar.h"
#include <ctime>
#include "ItemManager.h"
#include "PlayerInputMgr.h"
#include "UInGameBar.h"
#include "SystemTips .h"
#include "../Utils/SpellDB.h"
#include "../Utils/CreatureLootDB.h"
#include "../Utils/CreatureSpawnsDB.h"


bool GetItemFindPath(ui32 item, ui32& map, NiPoint3& pos)
{
	ui32 monster = 0;
	if (g_CreatureLootDB->FindLootCreature(monster, item))
	{
		float x,y,z;
		if (g_pkCCreatureSpawnsInfo->GetCreateInfomation(monster, map, x, y, z))
		{
			pos.x = x;
			pos.y = y;
			pos.z = z;
			return true;
		}
	}
	return false;
}
bool GetMonsterFindPath(ui32 monster, std::string& str)
{
	float x,y,z;
	ui32 mapid = 0;
	if (g_pkCCreatureSpawnsInfo->GetCreateInfomation(monster, mapid, x, y, z))
	{
		char buf[512];
		sprintf(buf,  "<linkcolor:732C94><a:#NPC %d %.2f %.2f %.2f>%s</a>", mapid, x, y,z, str.c_str());
		str = buf;
		return true;
	}
	return false;
}

int GetItemProperty(std::string& str, ui32& ItemEntry, ItemExtraData& ItemData)
{
	//提取物品Entry
	int pos = str.find("_");
	ItemEntry = atoi(str.substr(0, pos).c_str());

	std::vector<ui32> vEnchantId_temp;
	std::string sExtraData = str.substr(pos + 1, str.size());

	if (pos == -1 || !sExtraData.size())
		return 0;

	//提取物品附加属性
	ui32 TempValue = 0 ;
	//ITEM_FIELD_JINGLIAN_LEVEL
	pos = sExtraData.find("_");
	TempValue = atoi(sExtraData.substr(0, pos).c_str());
	sExtraData = sExtraData.substr(pos + 1, sExtraData.size());
	ItemData.jinglian_level = TempValue;
	//ITEM_FIELD_ENCHANTMENT
	for (int i = 0 ; i < 6; i++)
	{
		pos = sExtraData.find("_");
		TempValue = atoi(sExtraData.substr(0, pos).c_str());
		sExtraData = sExtraData.substr(pos + 1, sExtraData.size());
		ItemData.enchants[i] = TempValue;
	}

	std::string sEquipLevel = sExtraData;
	if (!sEquipLevel.size())
		return 1;
	return 2;
}
std::string UTF8ToAnis(const std::string& str)
{
	char acUnicode[4096];
	char acAnsi[2048];

	unsigned short usLen;

	//UTF8 转换为 Unicode
	usLen = (unsigned short)MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.size(), (LPWSTR)acUnicode, sizeof(acUnicode)/2);

	//Unicode 转换为 Ansi
	usLen = (unsigned short)WideCharToMultiByte(936, 0, (LPCWSTR)acUnicode, usLen, acAnsi, sizeof(acAnsi), NULL, false);

	return std::string(acAnsi, usLen);
}

std::string AnisToUTF8(const std::string& str)
{
	char acUnicode[4096];
	char acUTF8[4096];

	//Ansi 转换为 Unicode
	ui16 usLen = (unsigned short)MultiByteToWideChar(936, 0, str.c_str(), str.size(), (LPWSTR)acUnicode, sizeof(acUnicode)/2);

	//Unicode 转换为 Utf8
	usLen = (unsigned short)WideCharToMultiByte(CP_UTF8, 0, (LPCWSTR)acUnicode, usLen, acUTF8, sizeof(acUTF8), NULL, false);

	//return std::string(acUTF8, usLen);
	////字符串中间可能存在'\0';
	//kString.resize(usLen);
	//kString.replace(0, usLen, acUTF8, usLen);
	return std::string(acUTF8, usLen);
}
std::string ParseItemMsg(const std::string& str, BOOL ShowIcon)
{
	string rstr ;
	size_t TagStart = str.find("<a:#Item_");
	if (TagStart != string::npos )
	{
		size_t TagEnd = str.substr(TagStart, str.length()).find("</a>");
		if (TagEnd == string::npos)
		{
			return str ;
		}
		string tem = str.substr(TagStart, TagEnd + 4); // 4 - "</a>"
		size_t TagPos = tem.find(">");
		if (TagPos != string::npos)
		{	
			if (TagPos != tem.length() - 1)
			{
				ui32 ItemEntry = 0;
				ItemExtraData TempData;
				int PropertyNum = GetItemProperty(tem.substr(9, TagPos), ItemEntry, TempData); // 9 - "<a:#Item_" 
				ItemPrototype_Client* pkItemInfo = ItemMgr->GetItemPropertyFromDataBase(ItemEntry);
				if (pkItemInfo)
				{
					string ItemName = pkItemInfo->C_name;
					std::string ItemIcon = pkItemInfo->C_icon;
					ItemIcon = "<skin:" + ItemIcon + ">";
					if (ShowIcon)
					{
						tem = tem.substr(0, TagPos + 1) + ItemIcon +"</a>";
					}else
					{
						tem = tem.substr(0, TagPos + 1) +"[" + ItemName + "]</a>";
					}
				}
				
			}
			rstr =  str.substr(0, TagStart)+ tem + ParseItemMsg(str.substr(TagStart + TagEnd + 4, str.length()), ShowIcon);// 4 - "</a>"
			return rstr ;
		}else
		{
			return str ;
		}
	}else
	{
		return str ;
	}
	
}
UColor GetColor(ColorIndex index)
{
	USystem * Sys = UControl::sm_System;
	if (!Sys)
		return FALSE;
	char buf[1024];
	switch(index)
	{
	case C_COLOR_ITEMJUNK:
		{
			sprintf(buf,"ItemJunk");
		}
		break;
	case C_COLOR_ITEMCOMMON:
		{
			sprintf(buf,"ItemCommon");
		}
		break;
	case C_COLOR_ITEMUNCOMMON:
		{
			sprintf(buf,"ItemUnCommon");
		}
		break;
	case C_COLOR_ITEMRARE:
		{
			sprintf(buf,"ItemRare");
		}
		break;
	case C_COLOR_ITEMLEGEND:
		{
			sprintf(buf,"ItemLegend");
		}
		break;
	case C_COLOR_ITEMEPIC:
		{
			sprintf(buf,"ItemEpic");
		}
		break;
	case C_COLOR_ITEMARTIFACT:
		{
			sprintf(buf,"ItemArtifact");
		}
		break;
	case C_COLOR_ITEMPROPERTY:
		{
			sprintf(buf, "ItemProperty" );
		}
		break;
	case C_COLOR_DESC:
		{
			sprintf(buf, "ItemDesc" );
		}
		break;
	case C_COLOR_XQ:
		{
			sprintf(buf, "ItemXq" );
		}
		break;
	case C_COLOR_OTHER:
		{
			sprintf(buf, "ItemOther" );
		}
		break;
	case C_COLOR_BANGDING:
		{
			sprintf(buf, "ItemBangding");
		}
		break;
	case C_COLOR_PAY:
		{
			sprintf(buf, "ItemPay");
		}
		break;
	case C_COLOR_WARNING:
		{
			sprintf(buf,"Warning");
		}
		break;
	case C_COLOR_DEFAULT:
		{
			sprintf(buf,"DefaultColor");
		}
		break;
	}
	UColorTable * colortable = Sys->FindColor(buf);
	if (colortable)
	{
		return colortable->mColor;
	}
	return GetColor(C_COLOR_DEFAULT);
}
UColor GetClassColor(ui8 Class)
{
	UColor color ;
	switch(Class)
	{
	case CLASS_BOW: //
		return 0xffb32dc0;
	case CLASS_WARRIOR:
		return 0xffbf4341;
	case CLASS_PRIEST:
		return 0xff3981bc;
	case CLASS_MAGE:
		return  0xff49c03e;
	}
	return 0xfffffff;
}
BOOL GetQualityColor(uint32 Quality, UColor& Color)
{
	switch(Quality)
	{
	case ITEM_QUALITY_POOR_GREY:
		Color = GetColor(C_COLOR_ITEMJUNK);
		break;
	case ITEM_QUALITY_NORMAL_WHITE:
		Color = GetColor(C_COLOR_ITEMCOMMON);
		break;
	case ITEM_QUALITY_UNCOMMON_GREEN:
		Color = GetColor(C_COLOR_ITEMUNCOMMON);
		break;
	case ITEM_QUALITY_RARE_BLUE:
		Color = GetColor(C_COLOR_ITEMRARE);
		break;
	case ITEM_QUALITY_EPIC_PURPLE:
		Color = GetColor(C_COLOR_ITEMLEGEND);
		break;
	case ITEM_QUALITY_LEGENDARY_ORANGE:
		Color = GetColor(C_COLOR_ITEMEPIC);
		break;
	default:
		Color = GetColor(C_COLOR_DEFAULT);
		return FALSE;
	}
	return TRUE;
}

std::string GetColorString(const UColor &color)
{
	std::string colorstr;
	char buf[256];
	sprintf( buf, "%0*X",2, color.r);
	colorstr += buf;
	sprintf( buf, "%0*X",2, color.g);
	colorstr += buf;
	sprintf( buf, "%0*X",2, color.b);
	colorstr += buf;
	sprintf( buf, "%0*X",2, color.a);
	colorstr += buf;
	return colorstr;
}
std::string GetQualityColorStr(ui32 qulity)
{
	UColor Linkcolor;
	switch(qulity)
	{
	case ITEM_QUALITY_POOR_GREY:
		Linkcolor = GetColor(C_COLOR_ITEMJUNK);
		break;
	case ITEM_QUALITY_NORMAL_WHITE:
		Linkcolor = GetColor(C_COLOR_ITEMCOMMON);
		break;
	case ITEM_QUALITY_UNCOMMON_GREEN:
		Linkcolor = GetColor(C_COLOR_ITEMUNCOMMON);
		break;
	case ITEM_QUALITY_RARE_BLUE:
		Linkcolor = GetColor(C_COLOR_ITEMRARE);
		break;
	case ITEM_QUALITY_EPIC_PURPLE:
		Linkcolor = GetColor(C_COLOR_ITEMLEGEND);
		break;
	case ITEM_QUALITY_LEGENDARY_ORANGE:
		Linkcolor = GetColor(C_COLOR_ITEMEPIC);
		break;
	}
	return GetColorString(Linkcolor);
}
std::string GetSpecListBoxColorString(ui32 qulity)
{
	switch(qulity)
	{
	case ITEM_QUALITY_POOR_GREY:
		return "R128G128B128A255";
	case ITEM_QUALITY_NORMAL_WHITE:    
		return "R255G255B255A255";
	case ITEM_QUALITY_UNCOMMON_GREEN  :    
		return "R000G185B000A255";
	case ITEM_QUALITY_RARE_BLUE: 
		return "R000G086B215A255";
	case ITEM_QUALITY_EPIC_PURPLE: 
		return "R156G000B234A255";
	case ITEM_QUALITY_LEGENDARY_ORANGE:  
		return "R223G089B000A255";
	}
	return "R255G255B255A255";
}
void CopyString(const char* str)
{
	UStringW CopyStr;
	CopyStr.Set(str);
	if (!OpenClipboard(NULL))
	{
		return;
	}
	if(!EmptyClipboard())
	{
		return;
	}
	HGLOBAL hBlock = GlobalAlloc( GMEM_MOVEABLE, (CopyStr.GetLength() + 1)* sizeof(WCHAR) );
	if( hBlock )
	{
		WCHAR *pszText = (WCHAR*)GlobalLock( hBlock );
		if( pszText )
		{
			CopyStr.SubStr(pszText, 0, CopyStr.GetLength());
			GlobalUnlock( hBlock );
		}
		HANDLE hcbData = SetClipboardData(CF_UNICODETEXT, hBlock );		
		if (hcbData == NULL)
		{
			DWORD Error = GetLastError();
			UTRACE("SetClipboardData failed , err : %d ", Error);
		}
	}
	CloseClipboard();
	if( hBlock )
		GlobalFree( hBlock );
}

bool InputTestSpellUseItem(ui32 spellEntry)
{

	CPlayerLocal* pLocalplayer = ObjectMgr->GetLocalPlayer();
	std::string Item;
	if( pLocalplayer )
	{
		CStorage * pItemSlotContainer = pLocalplayer->GetItemContainer();
		stSpellInfo* info = g_pkSpellInfo->GetSpellInfo( spellEntry );
		if (pItemSlotContainer && info)
		{
			char buf[256];
			for (int i = 0 ; i < 3 ; i++)
			{
				int count = pItemSlotContainer->GetItemCnt(info->uiEffectMiscValue[i]);
				int baseCount = info->uiEffectBasePoint[i];
				if ((info->uiEffect[i] != 159) && (info->uiEffect[i] != 157) )
					continue;
				if(  info->uiEffectMiscValue[i] && count < baseCount )
				{
					ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(info->uiEffectMiscValue[i]);
					if (ItemInfo)
					{
						if (Item.length())
							Item += ",";
						sprintf(buf, "%s*%d", ItemInfo->C_name.c_str(), baseCount - count);
						Item = Item + buf;
					}
				}
			}
			for (int i = 0 ; i < 2 ; ++i)
			{
				int count = pItemSlotContainer->GetItemCnt(info->uiTotemID[i]);
				if (info->uiTotemID[i] && count == 0)
				{
					ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(info->uiTotemID[i]);
					if (ItemInfo)
					{
						if (Item.length())
							Item += ",";
						sprintf(buf, "%s", ItemInfo->C_name.c_str());
						Item = Item + buf;
					}
				}
			}
			if (Item.size()) Item = _TRAN("Missing items：") + Item;
			EffectMgr->AddEffectText(Item.c_str());
		}
	}
	return Item.size() == 0;
}

uint32 TestManuFacureNum(uint32 Spell_entry)
{
	uint32 BuildNum = 0;
	CPlayerLocal* pLocalplayer = ObjectMgr->GetLocalPlayer();
	if( pLocalplayer )
	{
		CStorage * pItemSlotContainer = pLocalplayer->GetItemContainer();
		stSpellInfo* info = g_pkSpellInfo->GetSpellInfo( Spell_entry );
		if (pItemSlotContainer && info)
		{
			if (info->uiEffectMiscValue[0] && info->uiEffectBasePoint[0])
				BuildNum = pItemSlotContainer->GetItemCnt(info->uiEffectMiscValue[0]) / info->uiEffectBasePoint[0];
			if (info->uiEffectMiscValue[1] && info->uiEffectBasePoint[1])
				BuildNum = min(BuildNum, pItemSlotContainer->GetItemCnt(info->uiEffectMiscValue[1]) / info->uiEffectBasePoint[1]);
			if (info->uiEffectMiscValue[2] && info->uiEffectBasePoint[2])
				BuildNum = min(BuildNum, pItemSlotContainer->GetItemCnt(info->uiEffectMiscValue[2]) / info->uiEffectBasePoint[2]);
		}
	}
	return BuildNum;
}

uint32 GetPackageItemNum(uint32 Item_entry)
{
	CPlayerLocal* pLocalplayer = ObjectMgr->GetLocalPlayer();
	if( pLocalplayer )
	{
		CStorage * pItemSlotContainer = pLocalplayer->GetItemContainer();
		if (pItemSlotContainer)
		{
			return pItemSlotContainer->GetItemCnt(Item_entry);
		}
	}
	return 0;
}

void ShowNPCName(bool vis)
{
	EffectMgr->SetCharNameShow(GOT_CREATURE, vis);
	EffectMgr->SetCharNameShow(GOT_SCENEOBJECT, vis);
}
void ShowPlayerName(bool vis)
{	
	EffectMgr->SetCharNameShow(GOT_PLAYER, vis);
}
void ShowHostHpBar(bool vis)
{
	EffectMgr->SetHostHpBarShow(vis);
}
void ShowHpBarColorful(bool vis)
{
	EffectMgr->SetHostHpBarColorful(vis);
}
void ChatPopoShow(bool vis)
{
	EffectMgr->SetChatPopoShow(vis);
}
void GameBloom(bool vis)
{
	if( vis )
	{
		EffectMgr->CreatePostEffect();
	}
	else
	{
		EffectMgr->CleanupPostEffect();
	}
}
void TargetTargetShow(bool vis)
{
	TeamShow->SetTargetTargetShow(vis);
}

void ActionBar1(bool vis)
{
	UInGameBar* pInGameBar = INGAMEGETFRAME(UInGameBar, FRAME_IG_ACTIONSKILLBAR);
	if(pInGameBar)
	{
		pInGameBar->SetBottomBarState(vis);
	}
}

void ActionBar2(bool vis)
{
	UInGameBar* pInGameBar = INGAMEGETFRAME(UInGameBar, FRAME_IG_ACTIONSKILLBAR);
	if(pInGameBar)
	{
		pInGameBar->SetAddExAcitonBar(vis, 0);
	}
}

void ActionBar2Type(int type)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX2);
	if (pAb)
	{
		pAb->Transform(type);
	}
}

void ActionBar2Pos(UPoint pos)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX2);
	if (pAb)
	{
		pAb->UControl::SetPosition(pos);
	}
}

void ActionBar2Lock(bool vis)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX2);
	if (pAb)
	{
		if (vis)
		{
			pAb->Lock();
		}
		else
		{
			pAb->UnLock();
		}
	}
}

void ActionBar3(bool vis)
{
	UInGameBar* pInGameBar = INGAMEGETFRAME(UInGameBar, FRAME_IG_ACTIONSKILLBAR);
	if(pInGameBar)
	{
		pInGameBar->SetAddExAcitonBar(vis, 1);
	}
}

void ActionBar3Type(int type)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX3);
	if (pAb)
	{
		pAb->Transform(type);
	}
}

void ActionBar3Pos(UPoint pos)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX3);
	if (pAb)
	{
		pAb->UControl::SetPosition(pos);
	}
}

void ActionBar3Lock(bool vis)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX3);
	if (pAb)
	{
		if (vis)
			pAb->Lock();
		else
			pAb->UnLock();
	}
}

void ActionBar4(bool vis)
{
	UInGameBar* pInGameBar = INGAMEGETFRAME(UInGameBar, FRAME_IG_ACTIONSKILLBAR);
	if(pInGameBar)
	{
		pInGameBar->SetAddExAcitonBar(vis, 2);
	}
}

void ActionBar4Type(int type)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX4);
	if (pAb)
	{
		pAb->Transform(type);
	}
}

void ActionBar4Pos(UPoint pos)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX4);
	if (pAb)
	{
		pAb->UControl::SetPosition(pos);
	}
}

void ActionBar4Lock(bool vis)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX4);
	if (pAb)
	{
		if (vis)
			pAb->Lock();
		else
			pAb->UnLock();
	}
}

void ActionBar5(bool vis)
{
	UInGameBar* pInGameBar = INGAMEGETFRAME(UInGameBar, FRAME_IG_ACTIONSKILLBAR);
	if(pInGameBar)
	{
		pInGameBar->SetAddExAcitonBar(vis, 3);
	}
}

void ActionBar5Type(int type)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX5);
	if (pAb)
	{
		pAb->Transform(type);
	}
}

void ActionBar5Pos(UPoint pos)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX5);
	if (pAb)
	{
		pAb->UControl::SetPosition(pos);
	}
}

void ActionBar5Lock(bool vis)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX5);
	if (pAb)
	{
		if (vis)
			pAb->Lock();
		else
			pAb->UnLock();
	}
}

void ActionBar6(bool vis)
{
	UInGameBar* pInGameBar = INGAMEGETFRAME(UInGameBar, FRAME_IG_ACTIONSKILLBAR);
	if(pInGameBar)
	{
		pInGameBar->SetAddExAcitonBar(vis, 4);
	}
}
void ActionBar6Type(int type)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX6);
	if (pAb)
	{
		pAb->Transform(type);
	}
}
void ActionBar6Pos(UPoint pos)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX6);
	if (pAb)
	{
		pAb->UControl::SetPosition(pos);
	}
}
void ActionBar6Lock(bool vis)
{

	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX6);
	if (pAb)
	{
		if (vis)
			pAb->Lock();
		else
			pAb->UnLock();
	}
}
void ActionBar7(bool vis)
{
	UInGameBar* pInGameBar = INGAMEGETFRAME(UInGameBar, FRAME_IG_ACTIONSKILLBAR);
	if(pInGameBar)
	{
		pInGameBar->SetAddExAcitonBar(vis, 5);
	}
}
void ActionBar7Type(int type)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX7);
	if (pAb)
	{
		pAb->Transform(type);
	}
}
void ActionBar7Pos(UPoint pos)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX7);
	if (pAb)
	{
		pAb->UControl::SetPosition(pos);
	}
}
void ActionBar7Lock(bool vis)
{
	UPlug_AcutionBar* pAb = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX7);
	if (pAb)
	{
		if (vis)
			pAb->Lock();
		else
			pAb->UnLock();
	}
}
void AutoPickItem(bool vis)
{
	UPickItemList* pPickItemlist = INGAMEGETFRAME(UPickItemList, FRAME_IG_PICKLISTDLG);
	if (pPickItemlist)
	{
		pPickItemlist->SetAutoPick(vis);
	}
}
void MiniMapShowNPC(bool vis)
{
	UMinimapFrame* pMiniMap = INGAMEGETFRAME(UMinimapFrame, FRAME_IG_MINIMAP);
	if (pMiniMap)
	{
		pMiniMap->MiNiMapShowControl(NPC_FRIENDLY,vis);
	}
}
void MiniMapShowPlayer(bool vis)
{
	UMinimapFrame* pMiniMap = INGAMEGETFRAME(UMinimapFrame, FRAME_IG_MINIMAP);
	if (pMiniMap)
	{
		pMiniMap->MiNiMapShowControl(PLAYER_FRIENDLY, vis);
	}
}
void MiniMapShowMonster(bool vis)
{
	UMinimapFrame* pMiniMap = INGAMEGETFRAME(UMinimapFrame, FRAME_IG_MINIMAP);
	if (pMiniMap)
	{
		pMiniMap->MiNiMapShowControl(CREATURE_ALL,vis);
	}
}
bool ChatPopoFliter(std::string &str)
{
	if (!str.size())
	{
		return false;
	}

	boost::regex reg("<linkcolor:\\w*><a:#\\w*>(.*?)</a>|\\[\\/(.*?)\\]|<color:\\w*>|<font:\\w*:\\w*>|<img:(.*?)>");
	str = boost::regex_replace(str, reg, "$1");
	return true;
}

void TraceLastError()
{
	int iError = GetLastError();
	switch(iError)
	{
	case ERROR_INSUFFICIENT_BUFFER:
		ULOG("ERROR_INSUFFICIENT_BUFFER");
		break;
	case ERROR_INVALID_FLAGS:
		ULOG("ERROR_INVALID_FLAGS");
		break;
	case ERROR_INVALID_PARAMETER:
		ULOG("ERROR_INVALID_PARAMETER");
		break;
	case ERROR_NO_UNICODE_TRANSLATION:
		ULOG("ERROR_NO_UNICODE_TRANSLATION");
		break;
	}
}

bool GetNameFliter(const char* buf)
{
	if( strchr(buf, ' ')
		|| strchr(buf, '\t')
		|| strchr(buf, '\r')
		|| strchr(buf, '\n'))
	{
		UMessageBox::MsgBox_s( _TRAN("Sorry, your entry contains blank characters！") );
		return false;
	}
	else if (!g_pkChatFilterDB->GetFilterChatMsg(std::string(buf)))
	{
		return true;
	}
	else
	{
		UMessageBox::MsgBox_s( _TRAN("Sorry, your entry has illegal characters！") );
		return false;
	}
	return false;
}
static bool IsChatFilter = true;
void SetFilterChatMsg(bool vis)
{
	IsChatFilter = vis;
}
void Show3DHead(bool vis)
{
	if (TeamShow)
	{
		TeamShow->SetHead3DShow(vis);
	}
}
void CCastDist(bool vis)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if (pkLocalPlayer && !pkLocalPlayer->IsHook())
	{
		SYState()->LocalPlayerInput->SetAutoAction(vis);
	}
}

void LockActionBar(bool vis)
{
	if(!g_ActionLock)
		return;

	if(vis)
		g_ActionLock->Lock();
	else
		g_ActionLock->UnLock();
}

void Plug_ReCount(bool vis)
{	
	UPlug_DPSCount_Dialog* NewCtrl = NULL;
	if (NewCtrl == NULL && UInGame::Get()->GetChildByID(FRAME_IG_PLUG_DPSCOUNT) == NULL)
	{
		NewCtrl = (UPlug_DPSCount_Dialog*)UControl::sm_System->CreateDialogFromFile("UPlug\\Plug_ReCount.udg");
		if (NewCtrl)
		{
			UInGame::Get()->AddChild(NewCtrl);
			NewCtrl->SetId(FRAME_IG_PLUG_DPSCOUNT);
		}
	}
	else
	{
		NewCtrl = UDynamicCast(UPlug_DPSCount_Dialog, UInGame::Get()->GetChildByID(FRAME_IG_PLUG_DPSCOUNT));
	}
	if (NewCtrl)
	{
		NewCtrl->Update();
		NewCtrl->SetVisible(vis);
	}
}

void SetMouseMove(bool vis)
{
	CPlayerInputMgr* inputmgr = SYState()->LocalPlayerInput;
	if (inputmgr)
	{
		inputmgr->SetUseMouseToMove(vis);

		CCastDist(vis);
	}
}

void CameraFollow(bool vis)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if(pkLocalPlayer)
	{
		CCamera& kCamera = pkLocalPlayer->GetCamera();
		kCamera.SetAutoRotCamera(vis);
	}
}

static float CameraSemsitivity = 0.5f;

void ChangeMouseSemsitivity(float Semsitivity)
{
	float width = (float)GetSystemMetrics( SM_CXSCREEN );
	float f = 1024.f / width;

	CameraSemsitivity = Semsitivity;
	SYState()->Camera_Move_Speed = 0.1f + 1.9f *Semsitivity * f;
}

void RestoreMouseSemsitivity()
{
	float width = (float)GetSystemMetrics( SM_CXSCREEN );
	float f = 1024.f / width;

	SYState()->Camera_Move_Speed = 0.1f + 1.9f *CameraSemsitivity * f;
}

void ChangeCamearFollowSpeed(float speed)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if(pkLocalPlayer)
	{
		CCamera& kCamera = pkLocalPlayer->GetCamera();
		if ( speed < 0.2 )
			speed = 0.2;

		SYState()->Camera_Follow_Speed = speed;
	}
}

void ChangeCameardistance(float speed)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if(pkLocalPlayer)
	{
		CCamera& kCamera = pkLocalPlayer->GetCamera();
		kCamera.SetMaxDistance(22 + speed * 6);
	}
}

void LockCamera(bool vis)
{
	CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
	if(pkLocalPlayer)
	{
		CCamera& kCamera = pkLocalPlayer->GetCamera();
		kCamera.LockView(vis);
	}
}

bool GetFilterChatMsg(std::string &str)
{
	if(IsChatFilter && g_pkChatFilterDB)
		return g_pkChatFilterDB->GetFilterChatMsg(str);
	return true;
}
#include "utils/ItemDB.h"
#include "utils/ItemEnchant.h"

bool GetItemEffect(ui32 Itementry, ui64 guid, ui32& effect_display_ID)
{
	effect_display_ID = 0;
	uint8 jinglian = 0;

	SYItem* Item = (SYItem *)ObjectMgr->GetObject(guid);

	if (Item)
	{
		jinglian = Item->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
	}

	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(Itementry);
	if(!ItemInfo)
		return false;

	int itemID = ItemInfo->ItemId;

	return GetRefineItemEffect(itemID, jinglian, effect_display_ID);

}

bool GetRefineItemEffect(ui32 uiItemID, UINT jinglianLevel, ui32& effect_display_ID)
{
	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(uiItemID);
	if ( !ItemInfo )
		return false;

	return GetRefineItemEffect(ItemInfo, jinglianLevel, effect_display_ID);
}

bool GetRefineItemEffect(ItemPrototype_Client* pItem, UINT jinglianLevel, ui32& effect_display_ID)
{
	if(g_pkRefineItemEffectDB)
		return g_pkRefineItemEffectDB->GetRefineItemEffect(pItem, jinglianLevel, effect_display_ID);
	return false;
}

bool bEnableMusic = false;
bool bEnableSound = false;
bool bEnable3DSound = false;
float volumMusic = 0.3f;
float volumSound = 0.3f;
float volumSound3D = 0.3f;
void EnableMusic(bool bEnable)
{
    if(bEnable)
        AudioSystem::GetAudioSystem()->SetMusicGainScale(volumMusic);
    else
        AudioSystem::GetAudioSystem()->SetMusicGainScale(0.f);
	bEnableMusic = bEnable;
}

void EnableSound(bool bEnable)
{
    if(bEnable)
        AudioSystem::GetAudioSystem()->SetSoundGainScale(volumSound);
    else
		AudioSystem::GetAudioSystem()->SetSoundGainScale(0.f);
	bEnableSound = bEnable;
}

void Enable3DSound(bool bEnable)
{
    if(bEnable)
        AudioSystem::GetAudioSystem()->Set3DSoundGainScale(volumSound3D);
    else
		AudioSystem::GetAudioSystem()->Set3DSoundGainScale(0.f);
	bEnable3DSound = bEnable;
}

void ChangeMusicVolum(float volum)
{
	if (bEnableMusic)
	{
		AudioSystem::GetAudioSystem()->SetMusicGainScale(volum);
		volumMusic = volum;
	}
}

void ChangeSoundVolum(float volum)
{
	if (bEnableSound)
	{
		AudioSystem::GetAudioSystem()->SetSoundGainScale(volum);
		volumSound = volum;
	}
}

void Change3DSoundVolum(float volum)
{
	if(bEnable3DSound)
	{
		AudioSystem::GetAudioSystem()->Set3DSoundGainScale(volum);	
		volumSound3D = volum;
	}
}

const float priceofstat[20] = 
{
	0.f,0.f,
	6.0f,5.5f,2.5f,1.5f,5.5f,1.5f,
	0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
};
float ItemPrice(ItemPrototype_Client* ItemInfo, bool IsBuy)
{
	float Price = ItemInfo->BuyPrice * 1.f;//(float)(IsBuy?ItemInfo->BuyPrice : ItemInfo->SellPrice);
	float OPrice = 0.0f;
	if (ItemInfo->Class == ITEM_CLASS_CONSUMABLE)
	{
		if (ItemInfo->SubClass == ITEM_SUBCLASS_CONSUMABLE_POTION)
		{
			//药品
			Price = float(ItemInfo->ItemLevel) * 22.5f * 60.f * 0.5f * 0.01f;
			OPrice = IsBuy ? ( Price  + 0.49f ): ( Price * 0.5f + 0.49f );
			return Price>0.0f?OPrice:1;
		}
	}
	if (ItemInfo->Class == ITEM_CLASS_RECIPE)
	{
		//ui32 SpellId = ItemInfo->Spells[0].Id;
		//if (SpellId == 483)
		//{
		//	//技能书
		//	Price = 40.f * ItemInfo->ItemLevel  + 0.5f;
		//	OPrice = IsBuy ? ( Price  + 0.49f ): ( Price * 0.5f + 0.49f );
		//	return Price>0.0f?OPrice:1;
		//}
	}
	if (ItemInfo->Class == ITEM_CLASS_ARMOR || ItemInfo->Class == ITEM_CLASS_WEAPON)
	{
		//装备
		Price = ItemInfo->Damage[0].Max + ItemInfo->Armor;
		for(int i = 0; i < 10; i++)
		{
			if (ItemInfo->Stats[i].Type < 20)
			{
				Price += ItemInfo->Stats[i].Value * priceofstat[ItemInfo->Stats[i].Type]*8;
			}
		}
		OPrice = IsBuy ? ( Price  + 0.49f ): ( Price * 0.5f + 0.49f );
		return Price>0.0f?OPrice:1;
	}

	if( !IsBuy )
		Price /= 2;

	if( !Price )
		Price = 1;
	return Price;
}

std::string GetMoneyRichTextStr(UINT money ,bool isYuanbao)
{
	string str;
	if (!money)
	{
		return "0";
	}
	if (isYuanbao)
	{
		char buf[_MAX_PATH];
		sprintf(buf, "%u<img:DATA\\UI\\public\\YuanBao.png>", money);
		str = buf;
	}else
	{
		UINT gold = money / 10000;
		UINT silver = money % 10000 / 100;
		UINT copper = money % 100 ;

		char goldbuf[_MAX_PATH];
		char silverbuf[_MAX_PATH];
		char copperbuf[_MAX_PATH];

		sprintf(goldbuf, "%u<img:gold.png>", gold);
		sprintf(silverbuf, "%u<img:silver.png>", silver);
		sprintf(copperbuf, "%u<img:copper.png>", copper);

		if (gold)
		{
			str += goldbuf;
		}
		if (silver)
		{
			str += silverbuf;
		}
		if (copper)
		{
			str += copperbuf;
		}
	}
	
	return str ;

}
int GetAS(int id)										  
{															  
	for (int i=0; i<sizeof(as)/sizeof(AS); i++)				  
	{
		if ( as[i].id == id )
		{
			return i;
		}
	}

	return -1;
}
void GetClientTime(uint64& uTime)
{
	uTime = time(NULL);
}
std::string ConvertTimeToString(uint64 time)
{
	char buf[256];
	time_t longtime = time;
	strftime(buf, sizeof(buf), "%H:%M", localtime(&longtime));
	return buf;
}

bool RichTextBitMapURLFun(char*  urlStr , std::string& countstr)
{
	string str = urlStr ;
	int pos = str.find("#_CHOOSEITEMICON_") ;
	int len = strlen("#_CHOOSEITEMICON_");
	if (pos == -1)
	{
		pos = str.find("#_REWITEM_");
		len = strlen("#_REWITEM_");
	}
	/*
	if (pos == -1)
		{
			pos = str.find("#_QUESTITEM_");
			len = strlen("#_QUESTITEM_");
		}*/
	

	if (pos != -1)
	{
		string strdata = str.substr(pos + len , str.length()) ;
		int entryPos = strdata.find("_");
		countstr = strdata.substr(entryPos+1, strdata.length() - entryPos);
		
		if (atoi(countstr.c_str()) == 1)
		{
			return false ;
		}

		return true ;
	}

	return false ;
}
static const int BufSize = 4096;
static char buffer[BufSize];
void ClientSystemNotify(const char *s, ...)
{
	if(!ChatSystem) return;
	
	va_list args;
	va_start(args, s);

	signed int len = _vsnprintf(buffer, BufSize, s, args);
	va_end(args);
	if (len < 0 || len >= BufSize || len + 1>= BufSize)	{
		if (BufSize > 0)
			buffer[BufSize-1] = '\0';
	}else
	{
		buffer[len] = '\n';
		buffer[len + 1] = '\0';
	}
	ChatSystem->AppendChatMsg(CHAT_MSG_CLIENTSYSTEM, buffer);
}
void PickItemNotify(const char *s, ...)
{
	if(!ChatSystem) return;

	va_list args;
	va_start(args, s);

	signed int len = _vsnprintf(buffer, BufSize, s, args);
	va_end(args);
	if (len < 0 || len >= BufSize || len + 1>= BufSize)	{
		if (BufSize > 0)
			buffer[BufSize-1] = '\0';
	}else
	{
		buffer[len] = '\n';
		buffer[len + 1] = '\0';
	}
	ChatSystem->AppendChatMsg(CHAT_MSG_PICKITEM, buffer);
}

void ChatMsgNotify(int msg,const char *s, ...)
{
	if(!ChatSystem) return;

	buffer[0] = 0;
	va_list args;
	va_start(args, s);
	signed int len = _vsnprintf(buffer, BufSize, s, args);
	if (len < 0 || len >= BufSize || len + 1>= BufSize)	{
		if (BufSize > 0)
			buffer[BufSize-1] = '\0';
	}else
	{
		buffer[len] = '\n';
		buffer[len + 1] = '\0';
	}
	ChatSystem->AppendChatMsg(msg, buffer);
}