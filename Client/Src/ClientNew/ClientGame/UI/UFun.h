#ifndef UFUN_H
#define UFUN_H
#include "UInc.h"
#include "UIIndex.h"

#define PROFESSION_DUANZAO_ID 430003011
#define PROFESSION_CAIYAO_ID 430001011
#define PROFESSION_CAIKUANG_ID 430000011
#define PROFESSION_ZHUBAO_ID 430002011
#define PROFESSION_LIANDAN_ID 430004011

bool GetItemFindPath(ui32 item, ui32& map, NiPoint3& pos);
bool GetMonsterFindPath(ui32 monster, std::string& str);
std::string UTF8ToAnis(const std::string& str);
std::string AnisToUTF8(const std::string& str);
int GetItemProperty(std::string& str, ui32& ItemEntry, ItemExtraData& ItemData);
std::string ParseItemMsg(const std::string& str, BOOL ShowIcon = FALSE);
#define MAKEFONT(fontName, fontSize) "<font:"#fontName":"#fontSize">"

UColor GetColor(ColorIndex index);
BOOL GetQualityColor(uint32 Quality, UColor& Color);
UColor GetClassColor(ui8 Class);
std::string GetColorString(const UColor &color);
std::string GetQualityColorStr(ui32 qulity);
std::string GetSpecListBoxColorString(ui32 qulity);
void CopyString(const char* str);
bool InputTestSpellUseItem(ui32 spellEntry);
uint32 TestManuFacureNum(uint32 Spell_entry);
uint32 GetPackageItemNum(uint32 Item_entry);

void TraceLastError();

void ShowNPCName(bool vis);
void ShowPlayerName(bool vis);
void ShowHostHpBar(bool vis);
void ShowHpBarColorful(bool vis);
void AutoPickItem(bool vis);
void MiniMapShowNPC(bool vis);
void MiniMapShowPlayer(bool vis);
void MiniMapShowMonster(bool vis);
void ChatPopoShow(bool vis);
void GameBloom(bool vis);
void TargetTargetShow(bool vis);
void ActionBar1(bool vis);
void ActionBar2(bool vis);
void ActionBar2Type(int type);
void ActionBar2Pos(UPoint pos);
void ActionBar2Lock(bool vis);
void ActionBar3(bool vis);
void ActionBar3Type(int type);
void ActionBar3Pos(UPoint pos);
void ActionBar3Lock(bool vis);
void ActionBar4(bool vis);
void ActionBar4Type(int type);
void ActionBar4Pos(UPoint pos);
void ActionBar4Lock(bool vis);
void ActionBar5(bool vis);
void ActionBar5Type(int type);
void ActionBar5Pos(UPoint pos);
void ActionBar5Lock(bool vis);
void ActionBar6(bool vis);
void ActionBar6Type(int type);
void ActionBar6Pos(UPoint pos);
void ActionBar6Lock(bool vis);
void ActionBar7(bool vis);
void ActionBar7Type(int type);
void ActionBar7Pos(UPoint pos);
void ActionBar7Lock(bool vis);
void SetFilterChatMsg(bool vis);
void LockCamera(bool vis);
void Show3DHead(bool vis);
void CCastDist(bool vis);
void LockActionBar(bool vis);
void Plug_ReCount(bool vis);
void CameraFollow(bool vis);
void SetMouseMove(bool vis);
void RestoreMouseSemsitivity();
void ChangeMouseSemsitivity(float Semsitivity);
void ChangeCamearFollowSpeed(float speed);
void ChangeCameardistance(float speed);

bool ChatPopoFliter(std::string &str);
bool GetNameFliter(const char* buf);
bool GetFilterChatMsg(std::string &str);

bool GetItemEffect(ui32 Itementry, ui64 guid, ui32& effect_display_ID);
bool GetRefineItemEffect(ui32 uiItemID, UINT jinglianLevel, ui32& effect_display_ID);
bool GetRefineItemEffect(struct ItemPrototype_Client* pItem, UINT jinglianLevel, ui32& effect_display_ID);

void EnableMusic(bool bEnable);               
void EnableSound(bool bEnable);
void Enable3DSound(bool bEnable);
void ChangeMusicVolum(float volum);
void ChangeSoundVolum(float volum);
void Change3DSoundVolum(float volum);

void OnCommandGuildDisband(std::vector<std::string>& text);
void OnCommandRoll(std::vector<std::string>& text);
void OnCommandGuildInvate(std::vector<std::string>& text);
void OnCommandCallPlug(std::vector<std::string>& text);
void OnShowPosition(std::vector<std::string>& text);
void OnForceFly(std::vector<std::string>& text);


void OnGMModifyHp(std::vector<std::string>& text);
void OnGMModifyMP(std::vector<std::string>& text);
void OnGMModifySpeed(std::vector<std::string>& text);
void OnGMModifyDamage(std::vector<std::string>& text);

void OnChatInpHelp(std::vector<std::string>& text);
void OnReLoadSpell(std::vector<std::string>& text);

void OnQueryLADDER(std::vector<std::string>& text);
void OnAdult(std::vector<std::string>& text);
void OnShowShop(std::vector<std::string>& text);
void OnAFK(std::vector<std::string>& text);
void OnDND(std::vector<std::string>& text);

float ItemPrice(struct ItemPrototype_Client* ItemInfo, bool IsBuy);
static struct AS {
	std::string str;
	int id;
}as[] = {
	{"灵活",2},
	{"活力",3},
	{"聪慧",4},
	{"暴力",1},
	{"精准",8},
	{"招架",9},
	{"大师",7},
	{"疾速",6},
	{"贪婪",15},
	{"充能",16},
	{"净化",13},
	{"嗜血",14},
	{"缓慢",12},
	{"眩晕",11},
	{"刺甲",18},
	{"冰封",10},
	{"颤抖",17},
	{"坚韧",5},
	{"铁壁",19},
	{"魔增",20},
	{"狂热",21},
	{"献祭",22},
	{"抗性",23},
	{"恢复",24},
	{"知识",25},
	{"穿刺",26},
	{"嗜睡",27},
	{"禁言",28},
	{"心火",29},
	{"夜幕",30},
	{"霜甲",31},
	{"敛财",32},
	{"收藏",33},
	{"催眠",34},
	{"梦魇",35},
	{"抵抗",36},
	{"笼罩",37},
	{"怒视",38},
	{"冰封",39},
};
int GetAS(int id);

void GetClientTime(uint64& uTime);
std::string ConvertTimeToString(uint64 time);
std::string GetMoneyRichTextStr(UINT money ,bool isYuanbao = false); //使用于URichTextCtr里金钱表示 ...
bool RichTextBitMapURLFun(char*  urlStr , std::string& countstr);


void ClientSystemNotify(const char *s, ...);
void PickItemNotify(const char *s, ...);
void ChatMsgNotify(int msg,const char *s, ...);
static const char* cladderCategory[] = {
	"英雄","衰神","荣誉","等级","财富","慈善","部族","充值","竞技1v1","竞技2v2","竞技3v3","竞技4v4","竞技5v5","(LV80)1v1","(LV80)2v2","(LV80)3v3","(LV80)4v4","(LV80)5v5"
};
static const char* TitleClass[] = {
	"等级称号", "任务称号", "部族称号", "荣誉称号", "副本称号", "战场称号","杀怪称号", "排行称号", "道具称号", "封神称号", "竞技场称号", "开心游乐场称号",
};

static const char* QuilityClass[] = {
	"无用", "普通", "优秀", "精良", "史诗", "传说"
};
static const char* BondingType[]={
	"未绑定","拾取绑定","装备绑定","使用绑定","任务绑定",	"任务绑定", "终身绑定"
};

static const char *ConsumableType[]={"药物","食物","其它", "解绑", "传送", "附魔" };
static const char * WeaponType[] =
{
	"单手斧","双手斧","单手剑","双手剑","弓","弩","单手法杖","双手法杖",
	"单手锤","双手锤","单手刀","双手刀","盾","枪","匕首", "宝珠"
};
static const char * ArmorType[]=
{
	"头盔","肩甲","胸甲","腰带","戒指","项链","饰品",
	"发型","手套","外衣","腿甲","鞋子",
	"怪物装背部","怪物装头部","怪物装肩膀","怪物装上衣","怪物装下衣","怪物装鞋子",
};

static const char * RecipeType[] = 
{
	"技能书","制皮","裁缝","工程学","锻造","烹饪","炼金",
	"急救", "采药","钓鱼"
};

static const char* DamageType[]=
{
	"普通伤害：","","","","","",
};
static const char * ItemStat[] = 
{
	"",
	"生命 ",
	"魔法 ",
	"力量 ",
	"敏捷 ",
	"耐力 ",
	"智力 ",
	"精神 ",
	"体力 ",
	"防御几率 ",
	"抵抗几率 ",
	"招架几率 ",
	"盾牌格档几率 ",
	"经验速度 ",
	"生命回复速度 ",
	"法力回复速度 ",
	"近战命中几率 ",
	"近战Miss几率 ",
	"近战暴击几率 ",
	"远程命中几率 ",
	"远程MISS几率 ", 
	"远程爆击几率 ",
	"技能爆击几率 ",
	"技能MISS几率 ",
	"技能命中几率 ",
	"所有的命中几率 ",
	"所有爆击几率 ",
	"所有的MISS几率 ",
	"法伤 ",
};

static const char* base_str[] =
{
	//基础属性
	"力量",	
	"体力",
	"敏捷",
	"智力",	
	"精神",	
	"耐力",

	//防御属性
	"护甲",
	"躲闪率",
	"格挡",

	//近战
	"攻击强度",
	"伤害",
	"暴击率",
	"攻击速度",

	//远程
	"攻击强度",
	"伤害",
	"暴击率",
	"攻击速度",

	//法术
	"法术伤害",
	"法术暴击",

	//社会属性
	"当前荣誉",
	"总荣誉值",
	"击败人数",
	"被击败数",
};

enum victimState
{
	ATTACK = 1,
	DODGE,
	PARRY,
	INTERRUPT,
	BLOCK,
	EVADE,
	IMMUNE,
	DEFLECT
};

struct sTOpCode
{
	const char* OpCode;
	const char* ShowTxt;
};
static sTOpCode osbase[] = {
	{"ChooseSelf", "选择自己"},
	{"ChooseTeam1", "选一号队友"},
	{"ChooseTeam2", "选二号队友"},
	{"ChooseTeam3", "选三号队友"},
	{"ChooseTeam4", "选四号队友"},
	{"HideUI", "隐藏界面"},
	{"CallFB", "副本大厅"},
	{"CallEquip", "装备面板"},
	{"CallFriend", "社交"},
	{"CallGameSet", "游戏设置"},
	{"CallHelp", "帮助面板"},
	{"CallQuest", "任务面板"},
	{"CallEndoment", "慈善捐助"},
	{"CallSkill", "技能面板"},
	{"CallTalk", "呼出聊天"},
	{"Whisper", "回复密语"},
	{"CallBag", "包裹面板"},
	{"ShowMap", "显示大地图"},
	{"AutoChoose", "自动选怪"},
	{"HOOK","辅助设置"}
};
static sTOpCode osMove[] = {
	{"MoveForward", "向前移动"},
	{"MoveBackward", "向后移动"},
	{"MoveLeft", "向左移动"},
	{"MoveRight", "向右移动"},
	{"MoveTurnLeft","镜头左转"},
	{"MoveTurnRight","镜头右转"},
	{"Jump", "跳跃"},
	{"AutoMove", "自动前进"},
};
static sTOpCode osMainBar[] = {
	{"ActionButton1", "快捷键1"},
	{"ActionButton2", "快捷键2"},
	{"ActionButton3", "快捷键3"},
	{"ActionButton4", "快捷键4"},
	{"ActionButton5", "快捷键5"},
	{"ActionButton6", "快捷键6"},
	{"ActionButton7", "快捷键7"},
	{"ActionButton8", "快捷键8"},
	{"ActionButton9", "快捷键9"},
	{"ActionButton10", "快捷键10"},
	{"ActionButton11", "快捷键11"},
	{"ActionButton12", "快捷键12"},
};
static sTOpCode osAddBar1[] = {
	{"ActionButton1_1", "快捷键1"},
	{"ActionButton1_2", "快捷键2"},
	{"ActionButton1_3", "快捷键3"},
	{"ActionButton1_4", "快捷键4"},
	{"ActionButton1_5", "快捷键5"},
	{"ActionButton1_6", "快捷键6"},
	{"ActionButton1_7", "快捷键7"},
	{"ActionButton1_8", "快捷键8"},
	{"ActionButton1_9", "快捷键9"},
	{"ActionButton1_10", "快捷键10"},
	{"ActionButton1_11", "快捷键11"},
	{"ActionButton1_12", "快捷键12"},
	{"ActionButton1_13", "快捷键13"},
	{"ActionButton1_14", "快捷键14"},
	{"ActionButton1_15", "快捷键15"},
	{"ActionButton1_16", "快捷键16"},
	{"ActionButton1_17", "快捷键17"},
	{"ActionButton1_18", "快捷键18"},
	{"ActionButton1_19", "快捷键19"},
	{"ActionButton1_20", "快捷键20"},
	{"ActionButton1_21", "快捷键21"},
	{"ActionButton1_22", "快捷键22"},
};
static sTOpCode osAddBar2[] = {
	{"ActionButton2_1", "快捷键1"},
	{"ActionButton2_2", "快捷键2"},
	{"ActionButton2_3", "快捷键3"},
	{"ActionButton2_4", "快捷键4"},
	{"ActionButton2_5", "快捷键5"},
	{"ActionButton2_6", "快捷键6"},
	{"ActionButton2_7", "快捷键7"},
	{"ActionButton2_8", "快捷键8"},
	{"ActionButton2_9", "快捷键9"},
	{"ActionButton2_10", "快捷键10"},
};
static sTOpCode osAddBar3[] = {
	{"ActionButton3_1", "快捷键1"},
	{"ActionButton3_2", "快捷键2"},
	{"ActionButton3_3", "快捷键3"},
	{"ActionButton3_4", "快捷键4"},
	{"ActionButton3_5", "快捷键5"},
	{"ActionButton3_6", "快捷键6"},
	{"ActionButton3_7", "快捷键7"},
	{"ActionButton3_8", "快捷键8"},
	{"ActionButton3_9", "快捷键9"},
	{"ActionButton3_10", "快捷键10"},
};

static sTOpCode osAddBar4[] = {
	{"ActionButton4_1", "快捷键1"},
	{"ActionButton4_2", "快捷键2"},
	{"ActionButton4_3", "快捷键3"},
	{"ActionButton4_4", "快捷键4"},
	{"ActionButton4_5", "快捷键5"},
	{"ActionButton4_6", "快捷键6"},
	{"ActionButton4_7", "快捷键7"},
	{"ActionButton4_8", "快捷键8"},
	{"ActionButton4_9", "快捷键9"},
	{"ActionButton4_10", "快捷键10"},
};
static sTOpCode osAddBar5[] = {
	{"ActionButton5_1", "快捷键1"},
	{"ActionButton5_2", "快捷键2"},
	{"ActionButton5_3", "快捷键3"},
	{"ActionButton5_4", "快捷键4"},
	{"ActionButton5_5", "快捷键5"},
	{"ActionButton5_6", "快捷键6"},
	{"ActionButton5_7", "快捷键7"},
	{"ActionButton5_8", "快捷键8"},
	{"ActionButton5_9", "快捷键9"},
	{"ActionButton5_10", "快捷键10"},
};
static sTOpCode osAddBar6[] = {
	{"ActionButton6_1", "快捷键1"},
	{"ActionButton6_2", "快捷键2"},
	{"ActionButton6_3", "快捷键3"},
	{"ActionButton6_4", "快捷键4"},
	{"ActionButton6_5", "快捷键5"},
	{"ActionButton6_6", "快捷键6"},
	{"ActionButton6_7", "快捷键7"},
	{"ActionButton6_8", "快捷键8"},
	{"ActionButton6_9", "快捷键9"},
	{"ActionButton6_10", "快捷键10"},
};
static sTOpCode osAddBar7[] = {
	{"ActionButton7_1", "快捷键1"},
	{"ActionButton7_2", "快捷键2"},
	{"ActionButton7_3", "快捷键3"},
	{"ActionButton7_4", "快捷键4"},
	{"ActionButton7_5", "快捷键5"},
	{"ActionButton7_6", "快捷键6"},
	{"ActionButton7_7", "快捷键7"},
	{"ActionButton7_8", "快捷键8"},
	{"ActionButton7_9", "快捷键9"},
	{"ActionButton7_10", "快捷键10"},
};
static struct sTADDKeyCtrl
{
	const char* Title;
	const sTOpCode* pstOpcode;
	int length;
}addkeyctrl[] = {
	{"基本操作设置", osbase, sizeof(osbase)/ sizeof(osbase[0])},
	{"移动", osMove, sizeof(osMove)/ sizeof(osMove[0])},
	{"主快捷栏", osMainBar, sizeof(osMainBar)/ sizeof(osMainBar[0])},
	{"新增快捷栏1", osAddBar1, sizeof(osAddBar1)/ sizeof(osAddBar1[0])},
	{"附加快捷栏1", osAddBar2, sizeof(osAddBar2)/ sizeof(osAddBar2[0])},
	{"附加快捷栏2", osAddBar3, sizeof(osAddBar3)/ sizeof(osAddBar3[0])},
	{"附加快捷栏3", osAddBar4, sizeof(osAddBar4)/ sizeof(osAddBar4[0])},
	{"附加快捷栏4", osAddBar5, sizeof(osAddBar5)/ sizeof(osAddBar5[0])},
	{"附加快捷栏5", osAddBar6, sizeof(osAddBar6)/ sizeof(osAddBar6[0])},
	{"附加快捷栏6", osAddBar7, sizeof(osAddBar7)/ sizeof(osAddBar7[0])},
};

static const char* cSocketsColor[] = {
	"变换插槽",
	"红色插槽",
	"黄色插槽",
	"蓝色插槽",
	"变换插槽",
};

#endif