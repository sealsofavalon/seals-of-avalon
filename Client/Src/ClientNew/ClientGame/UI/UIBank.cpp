#include "stdafx.h"
#include "UIBank.h"
#include "ObjectManager.h"
#include "Player.h"
#include "ItemSlot.h"
#include "UIItemSystem.h"
#include "UISystem.h"
#include "UIGamePlay.h"
#include "UIPackage.h"
#include "UMessageBox.h"
#include "Network/PacketBuilder.h"
#include "Effect/EffectManager.h"
/////////////////////////////////////////////////////////////////////////////////////////////
bool CBankSlotBuyPriceDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
	{
		return false;
	}

	for (unsigned int ui = 0 ; ui < GetRecordCount() ; ++ui)
	{
		stBuyprice temp;
		temp.entry = GetUInt(ui, 0);
		temp.price = GetUInt(ui, 1);
		temp.isYuanBao = GetUInt(ui, 2) > 0;

		m_vPrice.push_back(temp);
	}
	return true;
}

void CBankSlotBuyPriceDB::GetBankSlotBuyPrice(uint32 slot, uint32& price, bool& IsYuanbao)
{
	for (unsigned int ui = 0 ; ui < m_vPrice.size() ; ++ui)
	{
		if (m_vPrice[ui].entry == slot)
		{
			price = m_vPrice[ui].price;
			IsYuanbao = m_vPrice[ui].isYuanBao;
			return;
		}
	}
}

//-----------------------------------------------------------------------------------------
UIMP_CLASS(UBank::BageSolt,UDynamicIconButtonEx);
void UBank::BageSolt::StaticInit()
{

}
UBank::BageSolt::BageSolt()
{
	m_IsTickable = FALSE;
	m_TypeID = UItemSystem::ICT_BANK_SOLT;
	m_ActionContener.SetActionItem(new UBank::BagSoltItem);
}
UBank::BageSolt::~BageSolt()
{

}
void UBank::BageSolt::OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag)
{
	if (!IsActive())
		return;
	switch (nDataFlag)
	{
	case UItemSystem::ICT_BAG:
		{
			m_bAcceptDrag = TRUE;
		}
		break;
	case UItemSystem::ICT_BANK:
		{
			m_bAcceptDrag = TRUE;
		}
		break;
	}
}
BOOL UBank::BageSolt::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (UDynamicIconButtonEx::OnMouseUpDragDrop(pDragFrom,point,pDragData,nDataFlag))
	{
		ui8* FromePos = (ui8*)pDragData;
		switch (nDataFlag)
		{
		case UItemSystem::ICT_BAG:
			{
				SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_BAG,UItemSystem::ICT_BANK_SOLT,*FromePos,GetItemData()->pos,1);
			}
			return TRUE;
		case UItemSystem::ICT_BANK:
			{
				SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_BANK,UItemSystem::ICT_BANK_SOLT,*FromePos,GetItemData()->pos,1);
			}
			return TRUE;
		}
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}
//-----------------------------------------------------------------------------------------
UIMP_CLASS(UBank,UPackageDialog);
UBEGIN_MESSAGE_MAP(UBank,UPackageDialog)
UON_UGD_DBCLICKED(0, &UBank::OnClickToBag)
UON_UGD_UPAG_RBCLICKED(0, &UBank::OnClickToBag)
UON_BN_CLICKED(11, &UBank::BuyBagSoltPos)
UON_BN_CLICKED(12,&UBank::NeatenBank)
UEND_MESSAGE_MAP()

void UBank::BagSoltItem::Use()
{

};
void UBank::StaticInit()
{

}
UBank::UBank()
{
	m_UBankContainer = NULL;
	m_BankSlotBuyPriceDB = NULL;
	m_guid = 0;
}

UBank::~UBank()
{

}
BOOL UBank::OnCreate()
{
	if (!UPackageDialog::OnCreate())
	{
		return FALSE;
	}

	m_UBankContainer = (UPackageContainer*)GetChildByID(0);
	if (m_UBankContainer == NULL)
	{
		return FALSE;
	}
	m_UBankContainer->SetGridCount(6,12);
	m_UBankContainer->SetGridSize(UPoint(42,43));
	m_UBankContainer->SetDragType(UItemSystem::ICT_BANK);

	if (!InitBagSolt())
	{
		return FALSE;
	}

	SetMaxSlot(BANK_SLOT_ITEM_END - BANK_SLOT_ITEM_START);


	if (GetChildByID(12))
	{
		GetChildByID(12)->SetActive(TRUE);
	}else
	{
		return FALSE ;
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(1));
	if (pText)
	{
		pText->SetTextColor("fbc408");
	}
	UMoneyText* pMoney = UDynamicCast(UMoneyText, GetChildByID(2));
	if (pMoney)
	{
		pMoney->SetMoneyTextAlign(UT_RIGHT);
	}

	m_BankSlotBuyPriceDB = NiNew CBankSlotBuyPriceDB;
	NIASSERT(m_BankSlotBuyPriceDB);
	if(m_BankSlotBuyPriceDB && !m_BankSlotBuyPriceDB->Load("DBFiles\\bankslotprices_dbc.db"))
	{
		MessageBox(NULL, "load DBFiles:DBFiles\\bankslotprices_dbc failed!", "error", MB_OK);
		return false;
	}
	return TRUE;
}
BOOL UBank::InitBagSolt()
{
	for (int i = 4; i <= 10 ; i++)
	{
		BageSolt* pBagSolt = NULL;
		pBagSolt = (BageSolt*)GetChildByID(i);

		if (pBagSolt == NULL)
			return FALSE;

		pBagSolt->SetActive(FALSE);
	}

	for (int index = BANK_SLOT_BAG_START; index < BANK_SLOT_BAG_END; index ++)
	{
		ShowUseBagIcon(index,0,FALSE);
	}

	return TRUE;
}
void UBank::OnDestroy()
{
	if (m_BankSlotBuyPriceDB)
	{
		NiDelete m_BankSlotBuyPriceDB;
		m_BankSlotBuyPriceDB = NULL;
	}
	UPackageDialog::OnDestroy();
}
void UBank::OnTimer(float fDeltaTime)
{
	if(!m_Visible)
		return;

	if (m_guid != 0)
	{
		CCreature* pCreature = (CCreature*)ObjectMgr->GetObject(m_guid);
		CPlayerLocal* pLocalPlayer = ObjectMgr->GetLocalPlayer();

		if (pLocalPlayer == NULL || pCreature == NULL)
		{
			return ;
		}else
		{
			NiPoint3 pCreaturePos = pCreature->GetPosition();
			NiPoint3 pLocalPlayerPos = pLocalPlayer->GetLocalPlayerPos();

			if ((pLocalPlayerPos - pCreaturePos).Length() > MaxLenToNPC)
			{
				OnClose();
			}
		}
	}
}

void UBank::OnClose()
{
	CPlayerLocal::SetCUState(cus_Normal);
}
void UBank::SetCUSNormal()
{
	SetVisible(FALSE);
	m_guid = 0;
}

void UBank::SetIconByPos(ui8 pos, ui32 ditemId,UItemSystem::ItemType type, int num)  //设置格子信息    
{
	ItemExtraData temp;
	m_UBankContainer->SetItemByPos(pos, ditemId, num, temp);
	m_UBankContainer->SetDragType(UItemSystem::ICT_BANK);
}
void UBank::SetBankerGuid(ui64 guid)
{
	m_guid = guid;
	CPlayerLocal::SetCUState(cus_Bank);
}

void UBank::ShowMoneyNum(UINT  Num)
{
	if (Num <= 0)
		Num = 0 ;

	UMoneyText* pMoney = UDynamicCast(UMoneyText, GetChildByID(2));
	if (pMoney)
	{
		pMoney->SetMoney(Num);
	}
}

void UBank::SetMaxSlot(UINT count)
{	
	m_pNeedTance = FALSE;

	URect TextRec = GetChildByID(1)->GetControlRect();
	URect GRec = GetChildByID(2)->GetControlRect();
	URect GBkGRec = GetChildByID(3)->GetControlRect();
	URect ZLRecBkg = GetChildByID(112)->GetControlRect();
	URect ZLRec = GetChildByID(12)->GetControlRect();
	
	UPoint Bag_0_Pos = GetChildByID(4)->GetWindowPos();
	UPoint Bag_0_Size = GetChildByID(4)->GetWindowSize();

	UPoint Bag_1_Pos = GetChildByID(5)->GetWindowPos();
	UPoint Bag_1_Size = GetChildByID(5)->GetWindowSize();

	UPoint Bag_2_Pos = GetChildByID(6)->GetWindowPos();
	UPoint Bag_2_Size = GetChildByID(6)->GetWindowSize();

	UPoint Bag_3_Pos = GetChildByID(7)->GetWindowPos();
	UPoint Bag_3_Size = GetChildByID(7)->GetWindowSize();

	UPoint Bag_4_Pos = GetChildByID(8)->GetWindowPos();
	UPoint Bag_4_Size = GetChildByID(8)->GetWindowSize();

	UPoint Bag_5_Pos = GetChildByID(9)->GetWindowPos();
	UPoint Bag_5_Size = GetChildByID(9)->GetWindowSize();

	UPoint Bag_6_Pos = GetChildByID(10)->GetWindowPos();
	UPoint Bag_6_Size = GetChildByID(10)->GetWindowSize();

	UPoint BuyBtnPos = GetChildByID(11)->GetWindowPos();
	UPoint BuyBtnSize = GetChildByID(11)->GetWindowSize();
				
	UPoint WindOldSize = GetControlRect().GetSize();

	// 这里由于父窗口的变化, 需要重新设置子窗口的位置. .... 
	// 由于暂时没有想到比较好的方案. 只能直接记录所有子窗口的变化前后的区域来一个一个进行位置大小的重设.
	m_UBankContainer->ResetCanUseNob(count);

	UPoint WindNewSize = GetControlRect().GetSize();

	GetChildByID(1)->SetWindowRect(TextRec);
	GetChildByID(2)->SetWindowRect(GRec);
	GetChildByID(3)->SetWindowRect(GBkGRec);
	GetChildByID(12)->SetWindowRect(ZLRec);
	GetChildByID(112)->SetWindowRect(ZLRecBkg);

	GetChildByID(4)->SetTop(WindNewSize.y - (WindOldSize.y - Bag_0_Pos.y));
	GetChildByID(4)->SetLeft(WindNewSize.x - (WindOldSize.x - Bag_0_Pos.x));
	GetChildByID(4)->SetSize(Bag_0_Size);

	GetChildByID(5)->SetTop(WindNewSize.y - (WindOldSize.y - Bag_1_Pos.y));
	GetChildByID(5)->SetLeft(WindNewSize.x - (WindOldSize.x - Bag_1_Pos.x));
	GetChildByID(5)->SetSize(Bag_1_Size);

	GetChildByID(6)->SetTop(WindNewSize.y - (WindOldSize.y - Bag_2_Pos.y));
	GetChildByID(6)->SetLeft(WindNewSize.x - (WindOldSize.x - Bag_2_Pos.x));
	GetChildByID(6)->SetSize(Bag_2_Size);

	GetChildByID(7)->SetTop(WindNewSize.y - (WindOldSize.y - Bag_3_Pos.y));
	GetChildByID(7)->SetLeft(WindNewSize.x - (WindOldSize.x - Bag_3_Pos.x));
	GetChildByID(7)->SetSize(Bag_3_Size);

	GetChildByID(8)->SetTop(WindNewSize.y - (WindOldSize.y - Bag_4_Pos.y));
	GetChildByID(8)->SetLeft(WindNewSize.x - (WindOldSize.x - Bag_4_Pos.x));
	GetChildByID(8)->SetSize(Bag_4_Size);

	GetChildByID(9)->SetTop(WindNewSize.y - (WindOldSize.y - Bag_5_Pos.y));
	GetChildByID(9)->SetLeft(WindNewSize.x - (WindOldSize.x - Bag_5_Pos.x));
	GetChildByID(9)->SetSize(Bag_5_Size);

	GetChildByID(10)->SetTop(WindNewSize.y - (WindOldSize.y - Bag_6_Pos.y));
	GetChildByID(10)->SetLeft(WindNewSize.x - (WindOldSize.x - Bag_6_Pos.x));
	GetChildByID(10)->SetSize(Bag_6_Size);

	GetChildByID(11)->SetTop(WindNewSize.y - (WindOldSize.y - BuyBtnPos.y));
	GetChildByID(11)->SetLeft(WindNewSize.x - (WindOldSize.x - BuyBtnPos.x));
	GetChildByID(11)->SetSize(BuyBtnSize);

	if (GetRoot())
	{
		INT Top = 85 * GetRoot()->GetWindowSize().y / 768;
		SetTop( Top );
	}
	RefurbishBank();
}
void UBank::ShowUseBagIcon(UINT index, ui32 bagItemid ,BOOL bActive)
{
	if (index >= BANK_SLOT_BAG_START &&  index < BANK_SLOT_BAG_END)
	{
		BageSolt* pBagSolt = NULL;
		ActionDataInfo data;

		data.pos = index;
		data.entry = bagItemid;
		data.type = UItemSystem::ITEM_DATA_FLAG ;

		switch (index)
		{
		case BANK_SLOT_BAG_1:
			pBagSolt = (BageSolt*)GetChildByID(4);
			break;
		case BANK_SLOT_BAG_2:
			pBagSolt = (BageSolt*)GetChildByID(5);
			break;
		case BANK_SLOT_BAG_3:
			pBagSolt = (BageSolt*)GetChildByID(6);
			break;
		case BANK_SLOT_BAG_4:
			pBagSolt = (BageSolt*)GetChildByID(7);
			break;
		case BANK_SLOT_BAG_5:
			pBagSolt = (BageSolt*)GetChildByID(8);
			break;
		case BANK_SLOT_BAG_6:
			pBagSolt = (BageSolt*)GetChildByID(9);
			break;
		case BANK_SLOT_BAG_7:
			pBagSolt = (BageSolt*)GetChildByID(10);
			break;
		}

		if (pBagSolt)
		{
			pBagSolt->SetItemData(data);
			if (bagItemid != 0)
			{
				pBagSolt->SetActive(TRUE);
			}else
			{
				pBagSolt->SetActive(bActive);
			}
		}
	}

	UControl* pkBtn = GetChildByID(11);
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	if (pLocal == NULL || pkBtn == NULL)
	{
		return ;
	}

	uint32 pvalue = pLocal->GetUInt32Value(PLAYER_BYTES_2);
	uint8 pBankBagCount = (uint8)(pvalue >> 16);

	if (pBankBagCount < 7)
	{
		pkBtn->SetActive(TRUE);	
	}else
	{
		pkBtn->SetActive(FALSE);
	}
	
}
#include "UFun.h"
uint32 g_Price = 0;
void UBank::BuyBagSoltPos()
{
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	if (pLocal == NULL)
		return ;

	uint32 pvalue = pLocal->GetUInt32Value(PLAYER_BYTES_2);
	uint8 pBankBagCount = (uint8)(pvalue >> 16);

	std::string moneystr;
	switch(pBankBagCount)
	{
	case 0:	case 1:	case 2:	case 3:	case 4:	case 5:	case 6:
		{
			bool isyuanbao = false;
			m_BankSlotBuyPriceDB->GetBankSlotBuyPrice(pBankBagCount + 1, g_Price, isyuanbao);
			moneystr = GetMoneyRichTextStr(g_Price, isyuanbao);
		}
		break;
	default:
		UMessageBox::MsgBox_s( _TRAN("无法购买更多的包位。") );
		return ;
	}

	UMessageBox::MsgBox(_TRAN(N_NOTICE_F89, moneystr.c_str()).c_str(),(BoxFunction*)SendBugBankPos);
}
void UBank::NeatenBank()
{
	SYState()->UI->GetUItemSystem()->SetNeateningBag(true,true);
}
void UBank::SendBugBankPos()
{
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	if (pLocal == NULL)
		return ;

	if ( g_Price > pLocal->GetMoney() )
	{
		UMessageBox::MsgBox_s( _TRAN("金钱不足,无法购买更多的包位。") );
		return;
	}
	PacketBuilder->SendBuyBankBagPos();
}
int UBank::GetBankNullPos()
{
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	if (pLocal == NULL)
		return -1;

	int pos = -1;
	CStorage* pbankStroage = pLocal->GetBankContainer();
	for (unsigned int i = 0; i < m_UBankContainer->GetCanUseNob(); i++)
	{
		CItemSlot* pSlot = (CItemSlot*)pbankStroage->GetSlot(i);
		if (pSlot && pSlot->GetCode() == 0)
		{
			pos = i; break;
		}
	}

	return pos;
}
void UBank::OnClickToBag()
{
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	UPackage * pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
	if (!pPackage)
		return ;

	int toPos = pPackage->GetNoneItemPos();
	if (toPos == -1)
	{
		EffectMgr->AddEffectText(_TRAN("包裹已满!"));
	}else
	{
		UPoint uPos = m_UBankContainer->GetSelGrid();
		UPoint uCount = m_UBankContainer->GetGridCount();

		WORD fromPos = uPos.y * uCount.x + uPos.x;
		
		CPlayer* pLocal = ObjectMgr->GetLocalPlayer();
		if (pLocal && pLocal->GetBankContainer())
		{
			if (pLocal->GetBankContainer()->GetSlot((ui8)fromPos))
			{
				UINT num = pLocal->GetBankContainer()->GetSlot((ui8)fromPos)->GetNum();
				SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_BANK,UItemSystem::ICT_BAG,(ui8)fromPos,(ui8)toPos,num);
				return;
			}
		}	
	}
}
void UBank::RefurbishBank()
{
	CPlayer* pLocalp = ObjectMgr->GetLocalPlayer();
	if (pLocalp)
	{
		CStorage* pBankContainer = pLocalp->GetBankContainer();
		if (pBankContainer)
		{
			for (unsigned int i = 0; i < m_UBankContainer->GetCanUseNob(); i ++)
			{
				CItemSlot* pItem = (CItemSlot*)pBankContainer->GetSlot(i);
				if (pItem)
				{
					if (pItem->GetCode() !=0)
					{
						SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BANK,i,pItem->GetCode(),pItem->GetNum(),UItemSystem::ITEM_DATA_FLAG, pItem->IsLocked());
					}else
					{
						SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BANK,i,0,0,UItemSystem::ITEM_DATA_FLAG);
					}
				}
			}
		}
	}
}

void UBank::SetRenderRect()
{
	m_HeadRect = URect(0, 0, 556, 98);        //头部区域
	m_LeftRect = URect(0, 98, 27, 356);        //左边框
	m_RightRect= URect(540, 98, 556, 356);       //右边框
	m_CenterRect= URect(27, 98, 540, 356);      //中间区域
	m_BottomRect = URect(0, 356, 556, 434);      //底部区域
}
BOOL UBank::OnEscape()
{
	if (!UPackageDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}