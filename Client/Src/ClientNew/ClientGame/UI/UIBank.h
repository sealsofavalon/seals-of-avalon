#ifndef __UIBANK_H__
#define __UIBANK_H__

#include "UInc.h"
#include "UPackageDialog.h"
#include "UPackageContainer.h"
#include "USkillButton.h"
#include "UDynamicIconButtonEx.h"
#include "Utils/DBFile.h"


class CBankSlotBuyPriceDB : public CDBFile
{
public:
	struct stBuyprice
	{
		uint32 entry;
		uint32 price;
		bool isYuanBao;
	};
	virtual bool Load(const char* pcName);

	void GetBankSlotBuyPrice(uint32 slot,uint32& price, bool& IsYuanbao);

protected:
	std::vector<stBuyprice> m_vPrice;

};

class UBank : public UPackageDialog
{
	class BageSolt : public UDynamicIconButtonEx
	{
		UDEC_CLASS(UBank::BageSolt);
		BageSolt();
		virtual ~BageSolt();
	protected:
		virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
		virtual void OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag);
	};
	friend class UBank::BageSolt;
	class BagSoltItem : public UActionItem
	{
	public:
		virtual void Use();
	};
	UDEC_CLASS(UBank);
	UDEC_MESSAGEMAP();
public:
	UBank();
	~UBank();
public:
	void SetBankerGuid(ui64 guid);
	ui64 GetNPCGUID(){return m_guid;}
	void SetIconByPos(ui8 pos, ui32 ditemId,UItemSystem::ItemType type, int num);  //设置格子信息    
	void SetMaxSlot(UINT count); //变化可用的格子
	void RefurbishBank(); // 刷新银行里的道具
	void ShowMoneyNum(UINT  Num);//显示金钱
	void ShowUseBagIcon(UINT index, ui32 bagItemid, BOOL bActive);
	void OnClickToBag();
	void BuyBagSoltPos(); //购买包位.
	int GetBankNullPos();


	void SetCUSNormal();

	static void SendBugBankPos();
protected:
	void NeatenBank();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual BOOL OnEscape();
	void virtual SetRenderRect();

	virtual void OnClose();

	BOOL InitBagSolt();
protected:
	UPackageContainer* m_UBankContainer;  //
	CBankSlotBuyPriceDB* m_BankSlotBuyPriceDB;

	ui64 m_guid;
private:
};
#endif

