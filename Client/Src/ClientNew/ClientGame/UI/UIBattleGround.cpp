#include "StdAfx.h"
#include "UIBattleGround.h"
#include "UInstanceMgr.h"
#include "UFun.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "Player.h"

UIMP_CLASS(UBattleGroundPowerUpdata, UControl);

void UBattleGroundPowerUpdata::StaticInit()
{

}
UBattleGroundPowerUpdata::UBattleGroundPowerUpdata()
{
	memset(power, 0, sizeof(power));
	memset(side, 0, sizeof(side));
	memset(num, 0, sizeof(num));
	memset(bDraw, 0, sizeof(bDraw));
	memset(bannerOwnerstate, 0, sizeof(bannerOwnerstate));

	m_IsUpdateChild = FALSE;
	isParty = false;
}
UBattleGroundPowerUpdata::~UBattleGroundPowerUpdata()
{

}
BOOL UBattleGroundPowerUpdata::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	RaceTex[0] = sm_UiRender->LoadTexture("DATA\\UI\\Public\\yao.png");
	RaceTex[1] = sm_UiRender->LoadTexture("DATA\\UI\\Public\\ren.png");
	RaceTex[2] = sm_UiRender->LoadTexture("DATA\\UI\\Public\\wu.png");
	if (!RaceTex[0] || !RaceTex[1] || !RaceTex[2])
	{
		return FALSE;
	}

	PartyTex[0] = sm_UiRender->LoadTexture("DATA\\UI\\Public\\Team1.png");
	PartyTex[1] = sm_UiRender->LoadTexture("DATA\\UI\\Public\\Team2.png");
	PartyTex[2] = sm_UiRender->LoadTexture("DATA\\UI\\Public\\Team3.png");
	if (!PartyTex[0] || !PartyTex[1] || !PartyTex[2])
	{
		return FALSE;
	}

	return TRUE;
}
void UBattleGroundPowerUpdata::OnDestroy()
{
	memset(power, 0, sizeof(power));
	memset(side, 0, sizeof(side));
	memset(num, 0, sizeof(num));
	memset(bDraw, 0, sizeof(bDraw));
	memset(bannerOwnerstate, 0, sizeof(bannerOwnerstate));
	RaceTex[0] = NULL;
    RaceTex[1] = NULL;
	RaceTex[2] = NULL;
	PartyTex[0] = NULL;
	PartyTex[1] = NULL;
	PartyTex[2] = NULL;
	return UControl::OnDestroy();
}
void UBattleGroundPowerUpdata::OnRender(const UPoint& offset, const URect &updateRect)
{
	UFontPtr font = m_Style->m_spFont;
	int imagesize = 32;
	imagesize = max(imagesize , font->GetHeight());
	UPoint Size(updateRect.GetWidth(), imagesize);
	UPoint Offset(offset.x + imagesize, offset.y);
	USetTextEdge(TRUE);
	//0,妖族
	if(bDraw[0])
	{
		UTexturePtr tex = isParty?PartyTex[0]:RaceTex[0];
		sm_UiRender->DrawImage(tex, URect(offset.x, Offset.y, offset.x + imagesize, Offset.y + imagesize));
		UDrawText(sm_UiRender, font, Offset, Size, m_Style->m_FontColor, m_ShowText[0], UT_LEFT);
		Offset.y += imagesize;
	}
	//1,人族
	if(bDraw[1])
	{
		UTexturePtr tex = isParty?PartyTex[1]:RaceTex[1];
		sm_UiRender->DrawImage(tex, URect(offset.x, Offset.y, offset.x + imagesize, Offset.y + imagesize));
		UDrawText(sm_UiRender, font, Offset, Size, m_Style->m_FontColor, m_ShowText[1], UT_LEFT);
		Offset.y += imagesize;
	}
	//2,巫族
	if(bDraw[2])
	{
		UTexturePtr tex = isParty?PartyTex[2]:RaceTex[2];
		sm_UiRender->DrawImage(tex, URect(offset.x, Offset.y, offset.x + imagesize, Offset.y + imagesize));
		UDrawText(sm_UiRender, font, Offset, Size, m_Style->m_FontColor, m_ShowText[2], UT_LEFT);
		Offset.y += imagesize;
	}
	USetTextEdge(FALSE);
}
void UBattleGroundPowerUpdata::Update(uint16* uiPower, uint8* uiSide, uint8* uiNum, bool bparty)
{
	isParty = bparty;
	memcpy(power, uiPower, sizeof(power));
	memcpy(side, uiSide, sizeof(side));
	memcpy(num, uiNum, sizeof(num));
	memset(bDraw, 0, sizeof(bDraw));

 	string str = _TRAN(N_NOTICE_F90, _I2A(num[0]).c_str(), _I2A(power[0]).c_str());
// 	char buf[256];
// 	sprintf(buf, "（%u）战力：%u", num[0], power[0]);
	bDraw[0] = num[0] > 0;
	m_ShowText[0].Set(str.c_str());
	//sprintf(buf, "（%u）战力：%u", num[1], power[1]);
	str = _TRAN(N_NOTICE_F90, _I2A(num[1]).c_str(), _I2A(power[1]).c_str());
	bDraw[1] = num[1] > 0;
	m_ShowText[1].Set(str.c_str());
	//sprintf(buf, "（%u）战力：%u", num[2], power[2]);
	str = _TRAN(N_NOTICE_F90, _I2A(num[2]).c_str(), _I2A(power[2]).c_str());
	bDraw[2] = num[2] > 0;
	m_ShowText[2].Set(str.c_str());

	SetVisible(TRUE);
}

void UBattleGroundPowerUpdata::UpdateResource(uint16* uiresource, uint8* uiNum, int8* iState)
{
	isParty = false;
	memcpy(power, uiresource, sizeof(power));
	memcpy(num, uiNum, sizeof(num));
	memcpy(bannerOwnerstate, iState, sizeof(bannerOwnerstate));
	memset(bDraw, 0, sizeof(bDraw));

	int resourceNum[3] = { 0 };

	static char* state[] = {"", "无人", "妖族", "人族", "巫族", "", "", ""};

	for (int i = 0 ; i < 5 ; i++)
	{
		int ii = bannerOwnerstate[i] / 10;
		int iii = bannerOwnerstate[i] % 10;
		if (bannerOwnerstate[i] == 0)
		{
			//ClientSystemNotify("旗子%d:(%d)无人占领", i, bannerOwnerstate[i] );
		}else if (bannerOwnerstate[i] == 1)
		{
			++resourceNum[0];
			//ClientSystemNotify("旗子%d:(%d)妖族占领", i, bannerOwnerstate[i]);
		}else if (bannerOwnerstate[i] == 2)
		{
			++resourceNum[1];
			//ClientSystemNotify("旗子%d:(%d)人族占领", i, bannerOwnerstate[i]);
		}else if (bannerOwnerstate[i] == 3)
		{
			++resourceNum[2];
			//ClientSystemNotify("旗子%d:(%d)巫族占领", i, bannerOwnerstate[i] );
		}
		//else 
			//ClientSystemNotify("旗子%d:(%d) %s -> %s", i, bannerOwnerstate[i],  state[ii], race[iii]);
	}

	string str = _TRAN(N_NOTICE_F91, _I2A(num[0]).c_str(),_I2A(resourceNum[0]).c_str(), _I2A(power[0]).c_str());
	//char buf[256];
	//sprintf(buf, "（%u）资源(%d)：%u", num[0], resourceNum[0], power[0]);
	bDraw[0] = num[0] > 0 || resourceNum[0] > 0 || power[0] > 0;
	m_ShowText[0].Set(str.c_str());
	str = _TRAN(N_NOTICE_F91, _I2A(num[1]).c_str(),_I2A(resourceNum[1]).c_str(), _I2A(power[1]).c_str());
	//sprintf(buf, "（%u）资源(%d)：%u", num[1], resourceNum[1], power[1]);
	bDraw[1] = num[1] > 0 || resourceNum[1] > 0 || power[1] > 0;
	m_ShowText[1].Set(str.c_str());
	str = _TRAN(N_NOTICE_F91, _I2A(num[2]).c_str(),_I2A(resourceNum[2]).c_str(), _I2A(power[2]).c_str());
	//sprintf(buf, "（%u）资源(%d)：%u", num[2], resourceNum[2], power[2]);
	bDraw[2] = num[2] > 0 || resourceNum[2] > 0 || power[2] > 0;
	m_ShowText[2].Set(str.c_str());

	SetVisible(TRUE);
}

int UBattleGroundPowerUpdata::GetFlagState(int index)
{
	if (index < 0 || index >4)
		return 0;
	int iii = bannerOwnerstate[index] % 10;
	if ( bannerOwnerstate[index] == 0)
		return 0;
	else if ( bannerOwnerstate[index] == 1)
		return 1;
	else if ( bannerOwnerstate[index] == 2)
		return 2;
	else if ( bannerOwnerstate[index] == 3)
		return 3;
	else if(iii)
		return iii + 3;
	return 0;
}


UIMP_CLASS(UBattleGroundSpecList, USpecListBox);
void UBattleGroundSpecList::StaticInit(){}
UBattleGroundSpecList::UBattleGroundSpecList()
{
	m_SelectBkg = NULL;
}

UBattleGroundSpecList::~UBattleGroundSpecList()
{

}

BOOL UBattleGroundSpecList::OnCreate()
{
	if(!USpecListBox::OnCreate())
		return FALSE;

	if(m_SelectBkg == NULL)
	{
		m_SelectBkg = sm_UiRender->LoadTexture("DATA\\UI\\QuerySystem\\ContentBkg.png");
		if (!m_SelectBkg)
		{
			return FALSE;
		}
	}

	return TRUE;
}

void UBattleGroundSpecList::OnDestroy()
{
	USpecListBox::OnDestroy();
}
void UBattleGroundSpecList::OnRightMouseUp(const UPoint& position, UINT flags)
{
	USpecListBox::OnRightMouseUp(position, flags);
}

void UBattleGroundSpecList::DrawGridItemBkg(bool bselect, bool benable, bool bMouseOver, void* pUserDate, URect& drawRect, UColor* fontcolor)
{
	//yao - 1 ren - 2 wu - 3
	int raceIndex = 0;
	if (pUserDate)
	{
		raceIndex = *(int*)pUserDate;
	}
	if (benable)
	{
		if (bselect)
		{
			//高亮区域
			sm_UiRender->DrawImage(m_SelectBkg, drawRect);
			*fontcolor = m_Style->m_FontColorHL;
		}else if (bMouseOver)
		{
			sm_UiRender->DrawImage(m_SelectBkg, drawRect);
			*fontcolor = m_Style->m_FontColorNA;
		}
		else
		{
			sm_UiRender->DrawSkin(m_spSkin, raceIndex, drawRect);
			*fontcolor = m_Style->m_FontColor;
		}
	}else
	{
		if (bselect)
		{
			//高亮区域
			sm_UiRender->DrawImage(m_SelectBkg, drawRect);
			*fontcolor = m_Style->m_FontColorHL;
		}else if (bMouseOver)
		{
			sm_UiRender->DrawImage(m_SelectBkg, drawRect);
			*fontcolor = m_Style->m_FontColorNA;
		}
		else
			sm_UiRender->DrawSkin(m_spSkin, raceIndex, drawRect);
	}
}

UIMP_CLASS(UBattleGroundBillBoard, UDialog)
UBEGIN_MESSAGE_MAP(UBattleGroundBillBoard, UDialog)
UON_BN_CLICKED(0,&UBattleGroundBillBoard::OnSortByName)
UON_BN_CLICKED(1,&UBattleGroundBillBoard::OnSortByClass)
UON_BN_CLICKED(2,&UBattleGroundBillBoard::OnSortByKill)
UON_BN_CLICKED(3,&UBattleGroundBillBoard::OnSortByBeKill)
UON_BN_CLICKED(4,&UBattleGroundBillBoard::OnSortByDamage)
UON_BN_CLICKED(5,&UBattleGroundBillBoard::OnSortByHealing)
UON_BN_CLICKED(6,&UBattleGroundBillBoard::OnSortByhonor)
UON_BN_CLICKED(8,&UBattleGroundBillBoard::OnBtnRaceAll)
UON_BN_CLICKED(9,&UBattleGroundBillBoard::OnBtnRaceYAO)
UON_BN_CLICKED(10,&UBattleGroundBillBoard::OnBtnRaceREN)
UON_BN_CLICKED(11,&UBattleGroundBillBoard::OnBtnRaceWU)
UON_BN_CLICKED(14,&UBattleGroundBillBoard::OnBtnQuit)
UEND_MESSAGE_MAP()
void UBattleGroundBillBoard::StaticInit(){}

UBattleGroundBillBoard::UBattleGroundBillBoard()
{
	m_BattleGroundSpecList = NULL;
	m_BackRace = NULL;
	m_SortType = 0;
	m_SortRace = 0;
	m_bisParty = false;
}

UBattleGroundBillBoard::~UBattleGroundBillBoard()
{
	m_BattleGroundSpecList = NULL;
}

BOOL UBattleGroundBillBoard::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	UScrollBar* pScrollBar = UDynamicCast(UScrollBar, GetChildByID(12));
	if (!pScrollBar)
		return FALSE;
	if(m_BattleGroundSpecList == NULL)
	{
		m_BattleGroundSpecList = UDynamicCast(UBattleGroundSpecList, pScrollBar->GetChildByID(12));
		if (!m_BattleGroundSpecList)
		{
			return FALSE;
		}
	}
	if (m_BackRace == NULL)
	{
		m_BackRace = UDynamicCast(UBitmap, GetChildByID(13));
		if (!m_BackRace)
		{
			return FALSE;
		}
	}

	m_BattleGroundSpecList->SetGridSize(UPoint(687, 17));
	m_BattleGroundSpecList->SetGridCount(20, 1);
	m_BattleGroundSpecList->SetCol1(0, 0);
	m_BattleGroundSpecList->SetWidthCol1(183);
	m_BattleGroundSpecList->SetCol2(256 - 74, 0);
	m_BattleGroundSpecList->SetWidthCol2(53);
	m_BattleGroundSpecList->SetCol3(309 - 74, 0);
	m_BattleGroundSpecList->SetWidthCol3(49);
	m_BattleGroundSpecList->SetCol4(358 - 74, 0);
	m_BattleGroundSpecList->SetWidthCol4(65);
	m_BattleGroundSpecList->SetCol5(423 - 74, 0);
	m_BattleGroundSpecList->SetWidthCol5(81);
	m_BattleGroundSpecList->SetCol6(504 - 74, 0);
	m_BattleGroundSpecList->SetWidthCol6(68);
	m_BattleGroundSpecList->SetCol7(572 - 74, 0);
	m_BattleGroundSpecList->SetWidthCol7(77);
	m_BattleGroundSpecList->SetCol8(649 - 74, 0);
	m_BattleGroundSpecList->SetWidthCol8(112);

	UButton* pBtn = UDynamicCast(UButton, GetChildByID(8));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}

	pScrollBar->SetBarShowMode(USBSM_DYNAMIC, USBSM_FORCEOFF);

	m_bCanDrag = FALSE;
	return TRUE;
}

void UBattleGroundBillBoard::OnDestroy()
{
	m_SortType = 0;
	m_SortRace = 0;
	SetPartyLook(false);
	UDialog::OnDestroy();
}

BOOL UBattleGroundBillBoard::OnEscape()
{
	if(!UDialog::OnEscape())
	{
		InstanceSys->HideFrame();
	}
	return TRUE;
}

void UBattleGroundBillBoard::Update(bool isParty)
{
	if (!m_BattleGroundSpecList)
		return;

	SetPartyLook(isParty);
	std::vector<battle_ground_player_detail_information> vTemp;
	if(InstanceSys->GetBGPlayerDetailInfomatrion(&vTemp, m_SortType, m_SortRace, isParty))
	{
		m_BattleGroundSpecList->Clear();
		for (UINT ui = 0 ; ui < vTemp.size() ; ++ui)
		{
			battle_ground_player_detail_information& info = vTemp[ui];
			AddBGBillBoardList(info.name.c_str(), info.cls, info.kill, info.bekill, info.honor, info.race, info.damage, info.healing, 0);
		}
	}
	else
		m_BattleGroundSpecList->Clear();
}

void UBattleGroundBillBoard::SetPartyLook(bool isParty)
{
	m_bisParty = isParty;

	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(9));
	if (pBtn)
	{
		pBtn->SetBitmap(isParty?"Lobby\\BGBtnTeam1.skin":"Lobby\\BGBtnYao.skin");
	}
	pBtn = UDynamicCast(UBitmapButton, GetChildByID(10));
	if (pBtn)
	{
		pBtn->SetBitmap(isParty?"Lobby\\BGBtnTeam2.skin":"Lobby\\BGBtnRen.skin");
	}
	pBtn = UDynamicCast(UBitmapButton, GetChildByID(11));
	if (pBtn)
	{
		pBtn->SetBitmap(isParty?"Lobby\\BGBtnTeam3.skin":"Lobby\\BGBtnWu.skin");
	}
}

void UBattleGroundBillBoard::AddBGBillBoardList(const char* Name, ui32 Class, ui32 kill, ui32 beKill, int honor, uint8 race, uint32 damage, uint32 healing, uint8 winnerrace)
{
	if (!m_BattleGroundSpecList)
		return;
	
	UColor col = GetClassColor(Class);
	char colorstr[1024];
	NiSprintf(colorstr, 1024,"<color:R%03uG%03uB%03uA%03u>", col.r, col.g, col.b, col.a);

	char sz[1024];
	int* pint = new int(race);
	NiSprintf(sz, 1024, 
		"%s<Col1><center>%s<end>"
		"%s<Col2><center>%s<end>"
		"<defaultcolor><Col3><center>%u<end>"
		"<defaultcolor><Col4><center>%u<end>"
		"<color:R232G094B092A255><Col5><center>%u<end>"
		"<color:R143G251B129A255><Col6><center>%u<end>"
		"<defaultcolor><Col7><center>%d<end>",
		colorstr,
		Name,
		colorstr,
		_TRAN(class_name[Class]),
		kill,
		beKill,
		damage,
		healing,
		honor
		);	
	m_BattleGroundSpecList->AddItem(sz, (void*)pint);
}

void UBattleGroundBillBoard::OnSortByName()
{
	m_SortType = 0;
	Update(m_bisParty);
}
void UBattleGroundBillBoard::OnSortByClass()
{
	m_SortType = 1;
	Update(m_bisParty);
}
void UBattleGroundBillBoard::OnSortByKill()
{
	m_SortType = 2;
	Update(m_bisParty);
}
void UBattleGroundBillBoard::OnSortByBeKill()
{
	m_SortType = 3;
	Update(m_bisParty);
}
void UBattleGroundBillBoard::OnSortByhonor()
{
	m_SortType = 4;
	Update(m_bisParty);
}

void UBattleGroundBillBoard::OnSortByDamage()
{
	m_SortType = 5;
	Update(m_bisParty);
}

void UBattleGroundBillBoard::OnSortByHealing()
{
	m_SortType = 6;
	Update(m_bisParty);
}

void UBattleGroundBillBoard::OnBtnRaceAll()
{
	m_SortRace = 0;
	Update(m_bisParty);
	if (m_BackRace)
	{
		if (m_bisParty)
			m_BackRace->SetBitMapByStr("Data\\UI\\Lobby\\BKGIMAGE_ALL.png");
		else
			m_BackRace->SetBitMapByStr("Data\\UI\\Lobby\\BKGIMAGE_ALL.png");
	}
}
void UBattleGroundBillBoard::OnBtnRaceYAO()
{
	m_SortRace = 1;
	Update(m_bisParty);
	if (m_BackRace)
	{
		if (m_bisParty)
			m_BackRace->SetBitMapByStr("Data\\UI\\Lobby\\BKGIMAGE_TEAM1.png");
		else
			m_BackRace->SetBitMapByStr("Data\\UI\\Lobby\\BKGIMAGE_YAO.png");
	}
}
void UBattleGroundBillBoard::OnBtnRaceREN()
{
	m_SortRace = 2;
	Update(m_bisParty);
	if (m_BackRace)
	{
		if (m_bisParty)
			m_BackRace->SetBitMapByStr("Data\\UI\\Lobby\\BKGIMAGE_TEAM2.png");
		else
			m_BackRace->SetBitMapByStr("Data\\UI\\Lobby\\BKGIMAGE_REN.png");
	}
}
void UBattleGroundBillBoard::OnBtnRaceWU()
{
	m_SortRace = 3;
	Update(m_bisParty);
	if (m_BackRace)
	{
		if (m_bisParty)
			m_BackRace->SetBitMapByStr("Data\\UI\\Lobby\\BKGIMAGE_TEAM3.png");
		else
			m_BackRace->SetBitMapByStr("Data\\UI\\Lobby\\BKGIMAGE_WU.png");
	}
}

void UBattleGroundBillBoard::OnBtnQuit()
{
	InstanceSys->SendInstanceDeadExit(true);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UFairGroundMoneyUpdata, UControl)
void UFairGroundMoneyUpdata::StaticInit(){}
UFairGroundMoneyUpdata::UFairGroundMoneyUpdata()
{
	m_KaixinBi = NULL;
	m_CoinCount = 0;
	m_Scale = 1.0f;
	m_Font = NULL;
	m_KaixinNum = NULL;
}
UFairGroundMoneyUpdata::~UFairGroundMoneyUpdata()
{
}
BOOL UFairGroundMoneyUpdata::OnCreate()
{
	if(!UControl::OnCreate())
	{
		return FALSE;
	}
	if(m_KaixinBi == NULL)
	{
		m_KaixinBi = sm_UiRender->LoadTexture("DATA\\UI\\Lobby\\KaixinBi.png");
		if (!m_KaixinBi)
		{
			return FALSE;
		}
	}
	if (m_KaixinNum == NULL)
	{
		m_KaixinNum = sm_System->LoadSkin("MSGBOX\\Number.skin");
		if (!m_KaixinNum)
		{
			return FALSE;
		}
	}

	m_Font = sm_UiRender->CreateFont("Arial", 20, 134);

	return TRUE;
}
void UFairGroundMoneyUpdata::OnDestroy()
{
	m_CoinCount = 0;
	m_Scale = 1.f;
	m_Visible = FALSE;
	UControl::OnDestroy();
}
void UFairGroundMoneyUpdata::OnRender(const UPoint& offset,const URect &updateRect)
{
	char buf[256];
	sprintf(buf, "X%d", m_CoinCount);

	UPoint Pos = offset;
	if(m_KaixinBi)
	{
		URect rect(Pos + UPoint(0, 5), UPoint(40, 40));
		sm_UiRender->DrawImage(m_KaixinBi, rect);
		Pos.x += 45;
	}

	UPoint size(18, 18);
	size.x = int(size.x * m_Scale);
	size.y = int(size.y * m_Scale);
	UPoint NumPos = Pos;
	NumPos.y += (updateRect.GetHeight() - size.y) / 2;

	int len = strlen(buf);
	int Num = 0;
	for (int i = 0 ; i < len ; ++i)
	{
		Num = buf[i] - '0';
		if (buf[i] == 'X')
		{
			Num = 10;
		}
		sm_UiRender->DrawSkin(m_KaixinNum, Num, URect(NumPos, size),UColor(255, 170, 35, 255));
		NumPos.x += size.x;
	}
	//USetTextEdge(TRUE);
	//UDrawText(sm_UiRender, m_Font, Pos, size, UColor(255, 170, 35, 255), buf, UT_LEFT, 0.f, m_Scale);
	//USetTextEdge(FALSE);
}

void UFairGroundMoneyUpdata::OnTimer(float fDeltaTime)
{
	if (m_Time > 0.f)
	{
		if(1.f - m_Time < 0.15f)
		{
			float angle = ((1.f - m_Time ) / 0.15f) * NI_HALF_PI;
			m_Scale = 1.f + 0.7f * NiSin(angle);
		}
		else 
			m_Scale = 1.f;
		m_Time -= fDeltaTime;
	}
	else
		m_Scale = 1.f;
}

void UFairGroundMoneyUpdata::Update()
{
	CPlayerLocal* pLocalplayer = ObjectMgr->GetLocalPlayer();
	if( pLocalplayer )
	{
		CStorage * pItemSlotContainer = pLocalplayer->GetItemContainer();
		if (pItemSlotContainer)
		{
			int count = pItemSlotContainer->GetItemCnt(56);
			if (count > m_CoinCount)
			{
				m_Time = 1.f;
				m_Scale = 1.f;
			}
			m_CoinCount = count;
		}
	}
}