//-----------------------------------------------------------
//战场方面
//------------------------------------------------------------
#ifndef __UIBATTLEGROUND_H__
#define __UIBATTLEGROUND_H__
#include "UInc.h"
#include "USpecListBox.h"

class UBattleGroundPowerUpdata : public UControl
{
	UDEC_CLASS(UBattleGroundPowerUpdata);
	UBattleGroundPowerUpdata();
	~UBattleGroundPowerUpdata();
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
public:
	void Update(uint16* uiPower, uint8* uiSide, uint8* uiNum, bool bparty);

	void UpdateResource(uint16* uiresource, uint8* uiNum, int8* iState);

	int GetFlagState(int index);
protected:
	virtual BOOL HitTest(const UPoint& parentCoordPoint){return FALSE;};
protected:

	uint16 power[3]; // 战斗力。数组下标 0妖 1人 2巫
	uint8 side[3]; // 战斗方(0-2 根据策划要求做相应显示)。数组下标 0妖 1人 2巫
	uint8 num[3]; // 各阵营人数。 数组下标 0妖 1人 2巫
	uint8 bDraw[3];

	int8 bannerOwnerstate[5];
	UTexturePtr RaceTex[3];
	UTexturePtr PartyTex[3];

	UStringW m_ShowText[3];
	bool isParty;
};


class UBattleGroundSpecList : public USpecListBox
{
	UDEC_CLASS(UBattleGroundSpecList);
public:
	UBattleGroundSpecList();
	~UBattleGroundSpecList();
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
protected:
	virtual void DrawGridItemBkg(bool bselect, bool benable, bool bMouseOver, void* pUserDate, URect& drawRect, UColor* fontcolor);

	UTexturePtr m_SelectBkg;
};

class UBattleGroundBillBoard : public UDialog
{
	UDEC_CLASS(UBattleGroundBillBoard);
	UDEC_MESSAGEMAP();
public:
	UBattleGroundBillBoard();
	~UBattleGroundBillBoard();
public:
	void Update(bool isParty = false);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();

	void AddBGBillBoardList(const char* Name, ui32 Class, ui32 kill, ui32 beKill, int honor, uint8 race, uint32 damage, uint32 healing, uint8 winnerrace);


	void OnSortByName();
	void OnSortByClass();
	void OnSortByKill();
	void OnSortByBeKill();
	void OnSortByhonor();
	void OnSortByDamage();
	void OnSortByHealing();

	void OnBtnRaceAll();
	void OnBtnRaceYAO();
	void OnBtnRaceREN();
	void OnBtnRaceWU();

	void OnBtnQuit();
	void SetPartyLook(bool isParty);

	UBattleGroundSpecList* m_BattleGroundSpecList;
	UBitmap* m_BackRace;

	uint8 m_SortType;
	uint8 m_SortRace;
	bool m_bisParty;
};

class UFairGroundMoneyUpdata : public UControl
{
	UDEC_CLASS(UFairGroundMoneyUpdata);
	UFairGroundMoneyUpdata();
	~UFairGroundMoneyUpdata();
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
	void Update();
protected:
	virtual BOOL HitTest(const UPoint& parentCoordPoint){return FALSE;};

	int m_CoinCount;
	UTexturePtr m_KaixinBi;
	float m_Scale;
	float m_Time;
	UFontPtr m_Font;
	USkinPtr m_KaixinNum;
};

#endif