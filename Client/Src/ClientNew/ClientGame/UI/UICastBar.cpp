#include "stdafx.h"
#include "UICastBar.h"


#define  UDISCASTTIME 0.5f   //打断后显示的时间

UIMP_CLASS(USpellProcessBar, UControl);
void USpellProcessBar::StaticInit()
{
	UREG_PROPERTY("SpellNameAlign",	 UPT_INT,	 UFIELD_OFFSET(USpellProcessBar, mSpellNameAlign));
}
USpellProcessBar::USpellProcessBar()
{
	mbContinuousCaster = FALSE;
	mbEndCast = FALSE;
	m_CurTime = 0;
	m_CastOverTime = 0;
	mAlpha = 255;
	m_Distime = UDISCASTTIME;

	m_pSpellName = 0;
	m_pSpellTime = 0;
	m_CastBar = 0;
	mSpellNameAlign = UT_LEFT;
}

USpellProcessBar::~USpellProcessBar()
{
	mbContinuousCaster = FALSE;
	mbEndCast = FALSE;
	m_CurTime = 0;
	m_CastOverTime = 0;
}

void USpellProcessBar::SetSpell(const char* txt, BOOL bContinuousCaster)
{
	if (m_pSpellName)
		m_pSpellName->SetText(txt);
	mbContinuousCaster = bContinuousCaster;
}

void USpellProcessBar::SetSpellCast(ui32 CurTime, ui32 FullTime)
{
	m_CurTime = CurTime;
	m_CastOverTime = FullTime;
	m_CastBar->SetProgressAge(m_CurTime);
	m_CastBar->SetProgressLife(m_CastOverTime);
	if (!m_Visible)
	{
		if(FullTime)
		{
			mbEndCast = FALSE;
			mAlpha = 255;
			m_CastBar->SetAlpha(mAlpha);
			SetVisible(TRUE);
			m_CastBar->SetNorEnding();
		}
		else
		{
			mbEndCast = TRUE;
		}
	}
	float casttime = m_CurTime * 0.001f;
	float CastOverTime = m_CastOverTime * 0.001f;
	sprintf(m_cTimeStr,"%.2f/%.2f",casttime, CastOverTime);
	if (m_pSpellTime)
		m_pSpellTime->SetText(m_cTimeStr);

}

void USpellProcessBar::SpellCastBegin()
{
	m_CurTime = 0;
	m_CastOverTime = 0;
	mAlpha = 255;
	m_CastBar->SetProgressAge(m_CurTime);
	m_CastBar->SetProgressLife(m_CastOverTime);
	m_CastBar->SetAlpha(mAlpha);
	mbEndCast = FALSE;
	m_Distime = UDISCASTTIME;
	SetVisible(TRUE);
	m_CastBar->SetNorEnding();

	GetParent()->MoveChildTo(this, NULL);
}

void USpellProcessBar::SpellCastFailure()
{
	if(m_CurTime != m_CastOverTime && !mbContinuousCaster)
		m_CurTime = m_CastOverTime;
	sprintf(m_cTimeStr,_TRAN("打断") );
	if (m_pSpellTime)
		m_pSpellTime->SetText(m_cTimeStr);
	else if (m_pSpellName)
		m_pSpellName->SetText(m_cTimeStr);

	mbEndCast = TRUE;
	m_CastBar->SetProgressAge(m_CurTime);
	m_CastBar->SetBadEnding();
}

void USpellProcessBar::SpellCastEnd()
{
	if (!mbEndCast)
	{
		if(m_CurTime != m_CastOverTime && !mbContinuousCaster)
			m_CurTime = m_CastOverTime;
		mbEndCast = TRUE;
		m_CastBar->SetProgressAge(m_CurTime);
	}
}

BOOL USpellProcessBar::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_CastBar == NULL)
	{
		m_CastBar = UDynamicCast(UProgressBar_Skin, GetChildByID(0));
		if (m_CastBar)
		{
			m_CastBar->SetShowText(false);
		}
	}
	if (m_pSpellName == NULL)
	{
		m_pSpellName = UDynamicCast(UStaticText, GetChildByID(1));
	}
	if (m_pSpellName)
	{
		m_pSpellName->SetTextAlign(mSpellNameAlign);
		m_pSpellName->SetTextEdge(true);
	}
	if (m_pSpellTime == NULL)
	{
		m_pSpellTime = UDynamicCast(UStaticText, GetChildByID(2));
	}
	if (m_pSpellTime)
	{
		m_pSpellTime->SetTextAlign(UT_RIGHT);
		m_pSpellTime->SetTextEdge(true);
	}
	return TRUE;
}

void USpellProcessBar::OnDestroy()
{
	UControl::OnDestroy();
	mAlpha = 255;
}

void USpellProcessBar::OnTimer(float fDeltaTime)
{
	if(!m_Visible)
		return;
	if ( mbEndCast )	{
		m_Distime -= fDeltaTime;
		if (m_Distime <= 0.0f)
		{
			mbEndCast = FALSE;
			m_Distime = UDISCASTTIME;
			SetVisible(FALSE);
			m_CastBar->SetNorEnding();
		}
		else
		{
			float detailpos = m_Distime / UDISCASTTIME;
			mAlpha = UINT(255.0f * detailpos);
			m_CastBar->SetAlpha(mAlpha);
		}
	}else if(!m_CastOverTime){
			mbEndCast = TRUE;
	}
}