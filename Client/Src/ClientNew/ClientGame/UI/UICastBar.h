#ifndef __UICASTBAR_H__
#define __UICASTBAR_H__

#include "UInc.h"
class USpellProcessBar : public UControl
{
	UDEC_CLASS(USpellProcessBar);
public:
	USpellProcessBar();
	~USpellProcessBar();

	void SetSpell(const char* txt, BOOL bContinuousCaster = FALSE);
	void SetSpellCast(ui32 CurTime, ui32 FullTime);

	void SpellCastBegin();
	void SpellCastFailure();
	void SpellCastEnd();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
private:
	UProgressBar_Skin* m_CastBar;
	UStaticText* m_pSpellName;
	UStaticText* m_pSpellTime;
	INT mSpellNameAlign;

	BOOL mbContinuousCaster;
	BOOL mbEndCast;
	ui32 m_CurTime;
	ui32 m_CastOverTime;
	float m_Distime;
	UINT mAlpha;

	char m_cTimeStr[256];
};
#endif