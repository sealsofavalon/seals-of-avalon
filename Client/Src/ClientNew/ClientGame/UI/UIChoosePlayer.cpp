#include "StdAfx.h"
#include "UIChoosePlayer.h"
#include "UISystem.h"
#include "Network/PacketBuilder.h"
#include "Network/NetworkManager.h"
#include "../Player.h"
#include "UNiAVControl.h"
#include "UMessageBox.h"
#include "Map/Map.h"
#include "ObjectManager.h"
#include "SystemSetup.h"
#include "ItemManager.h"
#include "UFun.h"

#define MAX_PLAYER 5


UIMP_CLASS(UChoosePlayer,UControl);
void UChoosePlayer::StaticInit()
{

}
//事件响应
UBEGIN_MESSAGE_MAP(UChoosePlayer,UCmdTarget)
UON_BN_CLICKED(1, &UChoosePlayer::ClickRole_Zero)
UON_BN_CLICKED(2, &UChoosePlayer::ClickRole_One)
UON_BN_CLICKED(3, &UChoosePlayer::ClickRole_Two)
UON_BN_CLICKED(4, &UChoosePlayer::ClickRole_Thr)
UON_BN_CLICKED(5, &UChoosePlayer::ClickRole_Four)
UON_BN_CLICKED(6, &UChoosePlayer::ClickRole_Five)
UON_BN_CLICKED(7, &UChoosePlayer::ClickRole_Six)
UON_BN_CLICKED(9, &UChoosePlayer::ClickCreate)
UON_BN_CLICKED(10, &UChoosePlayer::ClickDelete)
UON_BN_CLICKED(11, &UChoosePlayer::ClickStartGame)
UON_BN_CLICKED(12, &UChoosePlayer::ClickBack)
UON_BN_CLICKED(13, &UChoosePlayer::ClickChooseServer)
//UON_BN_CLICKED(14, &UChoosePlayer::ClickLeft)
//UON_BN_CLICKED(15, &UChoosePlayer::ClickRight)
UON_BN_PRESS(14, &UChoosePlayer::ClickLeft)
UON_BN_PRESS(15, &UChoosePlayer::ClickRight)
UEND_MESSAGE_MAP()
UChoosePlayer::UChoosePlayer()
{
	m_Role_Zero = NULL;
	m_Role_One = NULL;
	m_Role_Two = NULL;
	m_Role_Thre = NULL;
	m_Role_Four = NULL;
	m_Role_Five = NULL;
	m_Role_Six = NULL;
	
	m_curRoleData = NULL; //
	m_pkControlR = NULL;

	m_MouseDpt.Set(0,0);
	m_DragRoleRect.Set(0,0,0,0);
}
UChoosePlayer::~UChoosePlayer()
{
	
}
BOOL UChoosePlayer::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	m_pkControlR = UDynamicCast(UNiAVControlR, GetChildByID(0));
	
	if (m_pkControlR == NULL)
	{
		return FALSE;
	}
	m_Role_Zero = (URoleInfo*)GetChildByID(1);
	m_Role_One = (URoleInfo*)GetChildByID(2);
	m_Role_Two = (URoleInfo*)GetChildByID(3);
	m_Role_Thre = (URoleInfo*)GetChildByID(4);
	m_Role_Four = (URoleInfo*)GetChildByID(5);
	m_Role_Five = (URoleInfo*)GetChildByID(6);
	m_Role_Six = (URoleInfo*)GetChildByID(7);

	if (m_Role_Zero == NULL || m_Role_One == NULL || m_Role_Two == NULL || m_Role_Thre == NULL ||
		m_Role_Four == NULL || m_Role_Five == NULL || m_Role_Six == NULL)
	{
		return FALSE;
	}

	for (UINT i = 0 ; i < m_Childs.size() ; i++)
	{
		m_Childs[i]->SetActive(TRUE);
	}

	RefreshRoleList();

	//////////////////////////////////////////////////////////////////////////
	GetChildByID(13)->SetActive(FALSE);
	//////////////////////////////////////////////////////////////////////////
	SystemSetup->SetGameSystemUserFileRoot(PATH_ROLENAME, "");
	if (GetChildByID(11))
	{
		GetChildByID(11)->SetActive(TRUE);
	}

	return TRUE;
}
void UChoosePlayer::OnDestroy()
{
	ClearRoleList();
	UControl::OnDestroy();
}

BOOL UChoosePlayer::OnEscape()
{
	if (!UControl::OnEscape())
	{
		ClickBack();
		return TRUE;
	}
	return FALSE;
}

void UChoosePlayer::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	m_MouseDpt = point;
	return UControl::OnMouseDown(point,nRepCnt,uFlags);
}
void UChoosePlayer::OnMouseUp(const UPoint& position, UINT flags)
{
	return UControl::OnMouseUp(position,flags);
}
void UChoosePlayer::OnMouseDragged(const UPoint& position, UINT flags)
{
	int maxw = m_DragRoleRect.GetWidth();
	float ep = -(position.x - m_MouseDpt.x)*0.01f;
	if(m_pkControlR)
	{
		m_pkControlR->SetRotatePlayer(ep);
	}
	m_MouseDpt = position;
}
UControl * UChoosePlayer::FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */)
{
	int _width = GetRoot()->GetWidth();
	int _height = GetRoot()->GetHeight();

	URect DragPlayerRect;
	DragPlayerRect.pt0.x = (442*_width)/1024;

	DragPlayerRect.pt0.y = (96*_height)/768;

	DragPlayerRect.pt1.x = DragPlayerRect.pt0.x + (346*_width)/1024;

	DragPlayerRect.pt1.y = DragPlayerRect.pt0.y + (569*_height)/768;

	m_DragRoleRect = DragPlayerRect;
	if (m_DragRoleRect.PointInRect(pt))
	{
		return this;
	}
	return UControl::FindHitWindow(pt,initialLayer);
}
void UChoosePlayer::ClickStartGame()
{
	if (m_curRoleData)
	{
		SystemSetup->SetGameSystemUserFileRoot(PATH_ROLENAME, m_curRoleData->name.c_str());
		SystemSetup->SaveLastRollName(m_curRoleData->name.c_str());
		PacketBuilder->SendEnterGameReqMsg(m_curRoleData->name,m_curRoleData->guid);
		
		EnterGameInfo enterGameInfo;
		enterGameInfo.uiRoleID = m_curRoleData->guid;
		enterGameInfo.uiMapID = m_curRoleData->nMapID;
		ClientState->SetEnterGameInfo(enterGameInfo);

		if (GetChildByID(11))
		{
			GetChildByID(11)->SetActive(FALSE);
		}
	}else
	{
		UMessageBox::MsgBox_s( _TRAN("Please create a Char!") );
	}
}
void UChoosePlayer::DBClickRoleEnterGame(INT index)
{
	SetCurRole(index);
	ClickStartGame();
}
void UChoosePlayer::ActiceEnterGameBtn()
{
	if (GetChildByID(11))
	{
		GetChildByID(11)->SetActive(TRUE);
	}
}
//void UChoosePlayer::ClickQuitGame()
//{
//	PostQuitMessage(0);
//}

void UChoosePlayer::ClickRight()                //3D 模型右转
{
	if(m_pkControlR)
	{
		m_pkControlR->SetRotatePlayer(-0.1f);
	}
}
void UChoosePlayer::ClickLeft()                 //3D 模型左转
{
	if(m_pkControlR)
	{
		m_pkControlR->SetRotatePlayer(0.1f);
	}
}
void UChoosePlayer::ClickCreate()
{
	CUISystem::ShowEX(DID_CREATEPLAYER,TRUE,TRUE);
}
void UChoosePlayer::SetCurRole(UINT id)
{
	if (id >= MAX_PLAYER)
	{
		id = MAX_PLAYER -1;
	}

	URoleInfo* pRole = NULL;
	switch (id)
	{
	case 0:
		pRole = m_Role_Zero;
		break;
	case 1:
		pRole = m_Role_One;
		break;
	case 2:
		pRole = m_Role_Two;
		break;
	case 3:
		pRole = m_Role_Thre;
		break;
	case 4:
		pRole = m_Role_Four;
		break;
	case 5:
		pRole = m_Role_Five;
		break;
	case 6:
		pRole = m_Role_Six;
		break;
	}

	if (pRole)
	{
		m_curRoleData = pRole->GetRoleData();

		if (m_curRoleData)
		{
			if (m_curRoleData->nRace == RACE_REN)
			{
				m_pkControlR->SetObjectFile("Data\\UI\\Assert\\ChoosePlayer\\Choose.nif");
			}else if (m_curRoleData->nRace == RACE_YAO)
			{
				m_pkControlR->SetObjectFile("Data\\UI\\Assert\\ChoosePlayer\\y_cha_select.nif");
			}else if (m_curRoleData->nRace == RACE_WU)
			{
				m_pkControlR->SetObjectFile("Data\\UI\\Assert\\ChoosePlayer\\w_cha_select.nif");
			}
		}
		if (!pRole->IsChecked())
		{
			pRole->SetCheck(TRUE);
		}
		m_pkControlR->SetChooseRoleShow(id);

        CPlayer* pkPlayer = m_pkControlR->GetActivePlayer();
        if(pkPlayer && !pkPlayer->IsActorLoading() && !pkPlayer->IsActorLoaded())
        {
            pkPlayer->OnCreateFromUI(m_curRoleData->displayid);
            for(unsigned int ui = 0; ui < EQUIPMENT_SLOT_END - EQUIPMENT_SLOT_START; ui++)
            {
                if(m_curRoleData->equipdisplay[ui])
                {
                    ItemMgr->OnEquipmentChange(m_curRoleData->guid, m_curRoleData->equipdisplay[ui], ui + EQUIPMENT_SLOT_START);
                }
            }
        }
	}
}

void UChoosePlayer::ClickBack()               //返回
{
	if (NetworkMgr)
	{
		NetworkMgr->Disconnect(CHOOSE_CHARACTER);
	}

	//禁止任何消息
	for (UINT i = 0 ; i < m_Childs.size() ; i++)
	{
		m_Childs[i]->SetActive(FALSE);
	}

}
void UChoosePlayer::ClickChooseServer()       //选则服务器
{
	return ;
	/*PacketBuilder->SendReToChooseServer();*/
	/*PacketBuilder->SendExitGameReqMsg();
	NetworkMgr->Disconnect();*/

	CMap * pmap = CMap::Get();
	if (pmap)
	{
		ObjectMgr->RemoveAll();
		pmap->Unload();
		pmap->Reset();
	}

	////连接登录服务器
	//if( !NetworkMgr->Connect(ClientState->GetLoginIP(), ClientState->GetLoginPort()) )
	//{
	//	UTRACE("connect login server failed.");
	//	return;
	//}
	//PacketBuilder->SendVersionMsg(0);

	//禁止任何消息
	for (UINT i = 0 ; i < m_Childs.size() ; i++)
	{
		m_Childs[i]->SetActive(FALSE);
	}
}

RoleInfo* g_RoleData;
void* OnDeleteYes()
{
	if (g_RoleData)
	{
		PacketBuilder->SendDeleteRoleReqMsg(g_RoleData->guid);
		SystemSetup->RemoveUserDirectory(g_RoleData->name.c_str());
	}
	return NULL;
}
void UChoosePlayer::ClickDelete()
{
	if(!m_curRoleData)
	{
		UMessageBox::MsgBox_s( _TRAN("Please Select Char") );
	}
	else
	{
		UMessageBox::MsgBox( _TRAN("Are you sure you want to delete?"), OnDeleteYes, NULL);	 
		g_RoleData = m_curRoleData;
	}
}
void UChoosePlayer::InitRoleList(RoleInfo* data, UINT i)
{
	if (i >= MAX_PLAYER)
	{
		return ;
	}

	URoleInfo* pRole = NULL;
	switch (i)
	{
	case 0:
		pRole = m_Role_Zero;
		break;
	case 1:
		pRole = m_Role_One;
		break;
	case 2:
		pRole = m_Role_Two;
		break;
	case 3:
		pRole = m_Role_Thre;
		break;
	case 4:
		pRole = m_Role_Four;
		break;
	case 5:
		pRole = m_Role_Five;
		break;
	case 6:
		pRole = m_Role_Six;
		break;
	}

	if (pRole)
	{
		if (data)
		{
			pRole->InitInfo(data,i);
		}else
		{
			pRole->InitInfo(NULL,i);
		}
	}
	
}
void UChoosePlayer::RefreshRoleList()
{
	//// Clear 
   ClearRoleList();

	int pRoleCount = 0;
	pRoleCount = ClientState->GetRoleCount();
	if (pRoleCount >= 0)
	{
		for (int i = 0;  i < MAX_PLAYER ; i++)
		{
			RoleInfo* mRoleInfo =ClientState->GetRole(i);
			InitRoleList(mRoleInfo,i);
		}
	}

	//如果已经创建的玩家列表达到最大,则不能到达创建人物界面。
	//如果角色列表为空，删除按钮。。。。

	UControl* pCreateP = GetChildByID(9);  //创建
	UControl* pDelete = GetChildByID(10); //删除
	UControl* pEnterGame = GetChildByID(11); //进入游戏
	if (pRoleCount >= MAX_PLAYER)
	{
		pCreateP->SetActive(FALSE);
	}else
	{
		pCreateP->SetActive(TRUE);
	}

	if (pRoleCount == 0)
	{
		pDelete->SetActive(FALSE);
		pEnterGame->SetActive(FALSE);
	}else
	{
		pDelete->SetActive(TRUE);
		pEnterGame->SetActive(TRUE);
	}
	
	// 更新到最后一个

	
	if(ClientState->GetRoleCount() && m_pkControlR)
	{
		BOOL bShowLast = FALSE ;
		if (SystemSetup)
		{
			string name ;
			if (SystemSetup->GetLastRollName(name) && name.length())
			{
				for (int i = 0;  i < MAX_PLAYER ; i++)
				{
					RoleInfo* mRoleInfo =ClientState->GetRole(i);
					if (mRoleInfo && _stricmp(mRoleInfo->name.c_str(), name.c_str()) == 0)
					{
						SetCurRole(i);
						bShowLast = TRUE;
						break ;
					}
				}
			}
		}

		if (!bShowLast && ClientState->GetRoleCount())
		{
			SetCurRole(ClientState->GetRoleCount() - 1);
		}
	}
}

void UChoosePlayer::ClickRole_Zero()
{
	if (m_curRoleData != m_Role_Zero->GetRoleData())
	{
		m_curRoleData = m_Role_Zero->GetRoleData();
		SetCurRole(0);
	}
	
}
void UChoosePlayer::ClickRole_One()
{
	if (m_curRoleData != m_Role_One->GetRoleData())
	{
		m_curRoleData = m_Role_One->GetRoleData();
		SetCurRole(1);
	}
}
void UChoosePlayer::ClickRole_Two()
{
	if (m_curRoleData != m_Role_Two->GetRoleData())
	{
		m_curRoleData = m_Role_Two->GetRoleData();
		SetCurRole(2);
	}
}
void UChoosePlayer::ClickRole_Thr()
{
	if(m_curRoleData != m_Role_Thre->GetRoleData())
	{
		m_curRoleData = m_Role_Thre->GetRoleData();
		SetCurRole(3);
	}
	
}
void UChoosePlayer::ClickRole_Four()
{
	if(m_curRoleData != m_Role_Four->GetRoleData())
	{
		m_curRoleData = m_Role_Four->GetRoleData();
		SetCurRole(4);
	}
}
void UChoosePlayer::ClickRole_Five()
{
	if(m_curRoleData != m_Role_Five->GetRoleData())
	{
		m_curRoleData = m_Role_Five->GetRoleData();
		SetCurRole(5);
	}
}
void UChoosePlayer::ClickRole_Six()
{
	if(m_curRoleData != m_Role_Six->GetRoleData())
	{
		m_curRoleData = m_Role_Six->GetRoleData();
		SetCurRole(6);
	}
	
}

void UChoosePlayer::ClearRoleList()
{
	/*m_Role_Zero->OnClear();
	m_Role_One->OnClear();
	m_Role_Two->OnClear();
	m_Role_Thre->OnClear();
	m_Role_Four->OnClear();
	m_Role_Five->OnClear();
	m_Role_Six->OnClear();*/

	m_curRoleData = NULL;

}

//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(URoleInfo,UBitmapButton);

void URoleInfo::StaticInit()
{
	
}

URoleInfo::URoleInfo()
{
	m_RoleData = NULL;
	m_RoleID = -1;
	m_RaceImage = NULL;
	m_Name = NULL;
	m_levelInfo = NULL;
	
}
URoleInfo::~URoleInfo()
{

}

BOOL URoleInfo::OnCreate()
{
	if (!UBitmapButton::OnCreate())
	{
		return FALSE;
	}

	m_RaceImage = (UBitmap*)GetChildByID(2);
	m_Name = (UStaticText*)GetChildByID(0);
	m_levelInfo = (UStaticText*)GetChildByID(1);

	if (m_levelInfo == NULL || m_Name == NULL || m_RaceImage == NULL)
	{
		return FALSE;
	}
	InitInfo(NULL, -1);
	return TRUE;
}
void URoleInfo::OnClear()
{
	m_levelInfo->Clear();
	m_Name->Clear();
	m_RoleData = NULL;
	m_RoleID = -1;
}
void URoleInfo::InitInfo(RoleInfo* data, INT id)
{
	m_RoleData = data;
	m_RoleID = id;

	if (m_RoleID == -1 || m_RoleData == NULL)
	{
		SetVisible(FALSE);
		OnClear();
	}else
	{
		//
		SetVisible(TRUE);
		if (m_RoleData->name.length() > 0)
		{
			m_Name->SetText(m_RoleData->name.c_str());	
		}

		UINT pClass = m_RoleData->nClass;
		std::string lev;
		int langindex = 0;
		if (pClass == CLASS_WARRIOR)
		{
			langindex = N_NOTICE_C0;
			//NiSprintf(lev, 100, "等级%d 武修",m_RoleData->nLevel);
		}else if (pClass == CLASS_BOW)
		{
			langindex = N_NOTICE_C1;
			//NiSprintf(lev, 100, "等级%d 羽箭",m_RoleData->nLevel);
		}else if (pClass == CLASS_PRIEST)
		{
			langindex = N_NOTICE_C2;
			//NiSprintf(lev, 100,"等级%d 仙道",m_RoleData->nLevel);
		}else if (pClass == CLASS_MAGE)
		{
			langindex = N_NOTICE_C3;
			//NiSprintf(lev, 100, "等级%d 真巫",m_RoleData->nLevel);
		}
		if( langindex )
		{
			lev = _TRAN( langindex, _I2A( m_RoleData->nLevel ).c_str() );
		}
		

		if (lev.length())
		{
			m_levelInfo->SetTextColor(GetClassColor(pClass));
			m_levelInfo->SetText(lev.c_str());
			string test = UTF8ToAnis(lev.c_str());
		}
		if (m_RoleData->nRace == RACE_YAO)
		{
			m_RaceImage->SetBitMapByStr("Data\\UI\\Public\\yao.png");
		}else if (m_RoleData->nRace == RACE_WU)
		{
			m_RaceImage->SetBitMapByStr("Data\\UI\\Public\\wu.png");
		}else if (m_RoleData->nRace == RACE_REN)
		{
			m_RaceImage->SetBitMapByStr("Data\\UI\\Public\\ren.png");
		}
	}
}
void URoleInfo::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	if (!m_Active)
	{
		return ;
	}

	if (m_RoleData && nClickCnt >= 2)
	{
		UChoosePlayer* pkParent = UDynamicCast(UChoosePlayer, GetParent());
		if (pkParent)
		{
			pkParent->DBClickRoleEnterGame(m_RoleID);
		}
		
	}else
	{
		return UBitmapButton::OnMouseDown(pt, nClickCnt ,uFlags);
	}
	
}
void URoleInfo::OnRender(const UPoint& offset, const URect &updateRect)
{
	UBitmapButton::OnRender(offset,updateRect);
	RenderChildWindow(offset,updateRect);
}

void URoleInfo::OnDestroy()
{
	UBitmapButton::OnDestroy();
}
void URoleInfo::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);
}
