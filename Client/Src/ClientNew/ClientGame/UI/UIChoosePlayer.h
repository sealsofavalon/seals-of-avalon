#pragma once
#include "UInc.h"
#include "ClientState.h"
#include "UListBox.h"
#include "UICreatePlayer.h"


class CPlayer;
class UNiAVControlR;
class URoleInfo : public UBitmapButton
{
	UDEC_CLASS(URoleInfo);
public:
	URoleInfo();
	~URoleInfo();
	void InitInfo(RoleInfo* data, INT id);
	RoleInfo* GetRoleData(){return m_RoleData;}
	void OnClear();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */)
	{
		return this;
	}
protected:
	RoleInfo* m_RoleData;
	INT m_RoleID;
	UBitmap* m_RaceImage;
	UStaticText* m_Name;
	UStaticText* m_levelInfo;
};
class UChoosePlayer : public UControl
{
	UDEC_CLASS(UChoosePlayer);
	UDEC_MESSAGEMAP();
public:
	UChoosePlayer();
	~UChoosePlayer();

	void RefreshRoleList();          //初始化
	UNiAVControlR* GetControlR() { return m_pkControlR; }
	void ActiceEnterGameBtn();
	void DBClickRoleEnterGame(INT index);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);
	virtual UControl * FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */);
protected:
	void SetCurRole(UINT id);
	void InitRoleList(RoleInfo* data, UINT i);
	void ClickCreate();				//进入创建界面
	void ClickDelete();				//删除当前选择的角色
	void ClickBack();               //返回	
	void ClickChooseServer();       //选则服务器
	void ClickStartGame();          //进入游戏
	void ClearRoleList();            //

	//
	void ClickRole_Zero();
	void ClickRole_One();
	void ClickRole_Two();
	void ClickRole_Thr();
	void ClickRole_Four();
	void ClickRole_Five();
	void ClickRole_Six();
	//
	void ClickRight();                //3D 模型右转
	void ClickLeft();                 //3D 模型左转
private:
	//最多7个角色
	URoleInfo* m_Role_Zero;
	URoleInfo* m_Role_One;
	URoleInfo* m_Role_Two;
	URoleInfo* m_Role_Thre;
	URoleInfo* m_Role_Four;
	URoleInfo* m_Role_Five;
	URoleInfo* m_Role_Six;

	RoleInfo* m_curRoleData;
	UNiAVControlR* m_pkControlR;

	UPoint m_MouseDpt;
	URect m_DragRoleRect;
};

