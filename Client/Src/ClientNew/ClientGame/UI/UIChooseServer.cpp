#include "StdAfx.h"
#include "UiSystem.h"
#include "UILogin.h"
#include "UIChooseServer.h"
#include "ClientState.h"
#include "Network/PacketBuilder.h"
#include "Network/NetworkManager.h"
#include "UI/UMessageBox.h"
#include "SystemSetup.h"

UIMP_CLASS(UChooseServer,UControl);


UBEGIN_MESSAGE_MAP(UChooseServer,UCmdTarget)
UON_UGD_DBCLICKED(3,&UChooseServer::OnCilickOK)
UON_BN_CLICKED(4, &UChooseServer::OnCilickOK)
UON_BN_CLICKED(5, &UChooseServer::OnCilickCancel)
UEND_MESSAGE_MAP()
void UChooseServer::StaticInit()
{

}
UChooseServer::UChooseServer()
{
	m_ServerList = NULL;
	m_IswaitServer = FALSE;
}
UChooseServer::~UChooseServer()
{

}
void UChooseServer::SetWaitServerMsg(BOOL iswait)
{
	m_IswaitServer = iswait;
	if (!m_IswaitServer)
	{
		SystemSetup->SetGameSystemUserFileRoot(PATH_SERVERNAME, mLastServerName.c_str());
	}

	if (GetChildByID(4))
	{
		GetChildByID(4)->SetActive(!iswait);
	}
}
BOOL UChooseServer::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
    UControl* pBitmap = GetChildByID(1);
    if(pBitmap == NULL)
        return FALSE;

	UScrollBar* pBar = (UScrollBar*)pBitmap->GetChildByID(2);
	if (pBar == NULL)
	{
		return FALSE;
	}
	pBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);

	m_ServerList  = (USpecListBox*)pBar->GetChildByID(3);
	if (m_ServerList == NULL)
	{
		return FALSE;
	}
	m_ServerList->SetGridSize(UPoint(342,25));
	m_ServerList->SetGridCount(15,1);
	m_ServerList->SetCol1(10,4);
	m_ServerList->SetWidthCol1(214);
	m_ServerList->SetCol2(224,4);
	m_ServerList->SetWidthCol2(116);
	
	OnShowServerList();
//////////////////////////////////////////////////////////////////////////
	//GetChildByID(5)->SetActive(FALSE);
	//////////////////////////////////////////////////////////////////////////

	if (GetChildByID(4))
	{
		GetChildByID(4)->SetActive(TRUE);
	}

	return TRUE;
}

void UChooseServer::OnDestroy()
{
	m_IswaitServer = FALSE;
	UControl::OnDestroy();
}
string UChooseServer::GetServerState(ui8 status)
{
	string statstr ;
	//char pServerSata[_MAX_PATH] ;
	if (status == 4)
	{
		statstr = "<color:R255G000B000A255><center>" ;
		statstr.append( _TRAN("Full"));
		//NiSprintf(pServerSata, _MAX_PATH, "<color:R255G000B000A255><center>.<end><Col3><color:R255G000B000A255><center>����", online, limit);

	}else if (status == 3)
	{
		statstr ="<color:R255G255B000A255><center>";
		statstr.append(_TRAN("Busy"));
		//��æ
		//NiSprintf(pServerSata, _MAX_PATH, "<color:R255G255B000A255><center>.<end><Col3><color:R255G255B000A255><center>��æ", online, limit);
	}else if (status == 2)
	{
		statstr = "<color:R000G128B192A255><center>" ;
		statstr.append(_TRAN("Good"));
		//��ͨ
		//NiSprintf(pServerSata, _MAX_PATH, "<color:R000G128B192A255><center>.<end><Col3><color:R000G128B192A255><center>��ͨ", online, limit);
	}else 
	{
		statstr = "<color:R000G255B000A255><center>"  ;
		statstr.append(_TRAN("Excellent"));
		//����
		//NiSprintf(pServerSata, _MAX_PATH, "<color:R000G255B000A255><center>.<end><Col3><color:R000G255B000A255><center>����", online, limit);
	}
	return statstr ;
}
void UChooseServer::OnShowServerList()
{
	m_ServerList->Clear();
	UINT count = ClientState->GetGroupCount();
	if (count > 0)
	{
		for (UINT i = 0; i< count; i++)
		{
			const GroupInfo* pGroup = ClientState->GetGroup(i);
			char pServer[255];
			bool enable = true;
			std::string strstate ;
			if( pGroup->status == 0 )
			{
				strstate = "<color:R128G128B128A255><center>";
				strstate.append(_TRAN("Maintenance"));
				enable = false ;
			}else
			{
				strstate = GetServerState(pGroup->status);
			}
			std::string GroupName;
			if (enable)
			{
				GroupName = "<color:R033G215B228A255><center>" + pGroup->strGroupName;
			}
			else
			{
				GroupName = "<color:R128G128B128A255><center>" + pGroup->strGroupName;
			}

            NiSprintf(pServer, 255, "<Col1>%s<end_left><Col2>%s<end>", GroupName.c_str(), strstate.c_str());
			char* pServerName = (char*)pGroup->strGroupName.c_str();
			m_ServerList->AddItem(pServer,pServerName);
			m_ServerList->SetItemEnable(i , enable);
		}
	}
}
void UChooseServer::OnCilickOK()
{
	UINT selIndex = m_ServerList->GetCurSel();
	if (selIndex < m_ServerList->GetItemCount() && selIndex >= 0 && !m_IswaitServer && m_ServerList->IsItemEnable(selIndex) )
	{
		char* pServerName = (char*)m_ServerList->GetItemData(selIndex);
		if (pServerName)
		{
			SetWaitServerMsg(TRUE);
			mLastServerName = pServerName;
			int id = ClientState->GetGroupID(pServerName);
			PacketBuilder->SendGroupSelReqMsg(id);
		}
	}	
}
void UChooseServer::OnCilickCancel()
{
	PacketBuilder->SendCanelGroupMsg();
	if (NetworkMgr)
	{
		NetworkMgr->Disconnect(CHOOSE_SERVER);
	}
	
}
BOOL UChooseServer::OnEscape()
{
	if (!UControl::OnEscape())
	{
		OnCilickCancel();
		return TRUE;
	}
	return FALSE;
}