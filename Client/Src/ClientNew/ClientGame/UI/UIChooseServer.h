#pragma once
#include "UInc.h"
#include "USpecListBox.h"

class UChooseServer : public UControl
{
	UDEC_CLASS(UChooseServer);
	UDEC_MESSAGEMAP();
public:
	UChooseServer();
	~UChooseServer();

	void SetWaitServerMsg(BOOL iswait);
	std::string GetLastServerName(){return mLastServerName;}
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();

	void OnCilickOK();
	void OnCilickCancel();
protected:
	void OnShowServerList();
	string GetServerState(ui8 status);
private:
	USpecListBox* m_ServerList;
	BOOL m_IswaitServer;
	std::string mLastServerName;
};