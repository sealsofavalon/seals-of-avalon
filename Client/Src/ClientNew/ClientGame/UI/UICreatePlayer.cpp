#include "StdAfx.h"
#include "UICreatePlayer.h"
#include "UISystem.h"
#include "Network/PacketBuilder.h"
#include "UNiAVControl.h"
#include "UMessageBox.h"
#include "../ItemManager.h"
#include "Utils/ChatFileterDB.h"
#include "UIChoosePlayer.h"
#include "UFun.h"

#define  MAX_NAME_LENGTH 14

UIMP_CLASS(UCreatePlayer,UControl);
UBEGIN_MESSAGE_MAP(UCreatePlayer,UCmdTarget)
//种族
UON_BN_CLICKED(4, &UCreatePlayer::Click_Ren)
UON_BN_CLICKED(5, &UCreatePlayer::Click_Yao)
UON_BN_CLICKED(6, &UCreatePlayer::Click_Wu)

//性别
UON_BN_CLICKED(7, &UCreatePlayer::ClickMale)
UON_BN_CLICKED(8, &UCreatePlayer::ClickFmale)

//职业
UON_BN_CLICKED(9, &UCreatePlayer::ClickCLASS_WARRIOR)
UON_BN_CLICKED(10, &UCreatePlayer::ClickCLASS_BOW)
UON_BN_CLICKED(11, &UCreatePlayer::ClickCLASS_DRUID)
UON_BN_CLICKED(12, &UCreatePlayer::ClickCLASS_ROGUE)

//创建相关
//UON_BN_CLICKED(14, &UCreatePlayer::ClickLeft)
//UON_BN_CLICKED(15, &UCreatePlayer::ClickRight)
UON_BN_PRESS(15, &UCreatePlayer::ClickRight)
UON_BN_PRESS(14, &UCreatePlayer::ClickLeft)
UON_BN_CLICKED(16, &UCreatePlayer::OnClickCreatePlayer)
UON_BN_CLICKED(17, &UCreatePlayer::OnClickBack)
UEND_MESSAGE_MAP()

void UCreatePlayer::StaticInit()
{

}
UCreatePlayer::UCreatePlayer()
{
  m_CreateName = NULL;
  m_RaceIntr = NULL;
  m_ClassIntr = NULL;
  m_SexName = NULL;
  m_ClassName = NULL;
  m_pkNiAVControl = NULL;

  m_Race = RACE_REN;  //
  m_Class = CLASS_WARRIOR;
  m_Sex = GENDER_MALE;

  m_MouseDpt.Set(0,0);
  m_DragRoleRect.Set(0,0,0,0);
}
UCreatePlayer::~UCreatePlayer()
{

}

BOOL UCreatePlayer::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	m_pkNiAVControl = UDynamicCast(UNiAVControlR, GetChildByID(0));

	if (m_pkNiAVControl == NULL)
	{
		return FALSE;
	}
    m_pkNiAVControl->CreateAllRoles();

	m_ClassIntr = (UBitmap*)GetChildByID(19);
	m_ClassName = (UBitmap*)GetChildByID(21);
	m_RaceIntr = (UBitmap*)GetChildByID(18);
	m_SexName = (UBitmap*)GetChildByID(20);

	if (m_ClassIntr == NULL || m_ClassName == NULL || m_SexName == NULL)
	{
		return FALSE;
	}

    //种族 随机
    m_Race = NiRand()%3 + 1;
    UButton* race = (UButton*)GetChildByID(4 + m_Race - 1);
    if (race)
        race->SetCheck(TRUE);

    //性别 女性
	UButton* male = (UButton*)GetChildByID(8);
	if (male == NULL)
	{
		return FALSE;
	}
	male->SetCheck(TRUE);


	//职业 随机
    m_Class = NiRand()%4 + 1;
	UButton* Class = (UButton*)GetChildByID(9 + m_Class - 1);
	if (Class)
	    Class->SetCheck(TRUE);

	m_CreateName = (UEdit*)GetChildByID(13);
	if (m_CreateName == NULL)
	{
		return FALSE;
	}
	else
	{
		m_CreateName->SetFocusControl();
		m_CreateName->SetMaxLength(MAX_NAME_LENGTH);
	}

    if(m_Race == RACE_REN)
    {
        m_pkNiAVControl->SetObjectFile("Data\\UI\\Assert\\ChoosePlayer\\Choose.nif");
        m_RaceIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\ren.png");
    }
    else if(m_Race == RACE_YAO)
    {
		m_pkNiAVControl->SetObjectFile("Data\\UI\\Assert\\ChoosePlayer\\y_cha_select.nif");
		m_RaceIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\yao.png");
    }
    else
    {
		m_pkNiAVControl->SetObjectFile("Data\\UI\\Assert\\ChoosePlayer\\w_cha_select.nif");
		m_RaceIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\wu.png");
    }

    if( m_Sex == GENDER_FEMALE )
        m_SexName->SetBitMapByStr("Data\\UI\\CreatePlayer\\fmale.png");
    else
        m_SexName->SetBitMapByStr("Data\\UI\\CreatePlayer\\male.png");

    if(m_Class == CLASS_WARRIOR)
    {
        m_ClassIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\zhanshi.png");
        m_ClassName->SetBitMapByStr("Data\\UI\\CreatePlayer\\n_zhanshi.png");
    }
    else if(m_Class == CLASS_BOW)
    {
        m_ClassIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\gongjian.png");
        m_ClassName->SetBitMapByStr("Data\\UI\\CreatePlayer\\n_gongjian.png");
    }
    else if(m_Class == CLASS_PRIEST)
    {
        m_ClassIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\xiandao.png");
        m_ClassName->SetBitMapByStr("Data\\UI\\CreatePlayer\\n_xiandao.png");
    }
    else
    {
        m_ClassIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\zhenwu.png");
        m_ClassName->SetBitMapByStr("Data\\UI\\CreatePlayer\\n_zhenwu.png");
    }

	m_pkNiAVControl->SetCreateRoleShow(m_Race , m_Sex , m_Class);
	
	if (GetChildByID(16))
	{
		GetChildByID(16)->SetActive(TRUE);
	}

	return TRUE;
}


void UCreatePlayer::OnDestroy()
{
	UControl::OnDestroy();
}
BOOL UCreatePlayer::OnEscape()
{
	if (!UControl::OnEscape())
	{
		OnClickBack();
		return TRUE;
	}
	return FALSE;
}
 void UCreatePlayer::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
 {
	 m_MouseDpt = point;
	 return UControl::OnMouseDown(point,nRepCnt,uFlags);
 }
 void UCreatePlayer::OnMouseDragged(const UPoint& position, UINT flags)
 {
	 int maxw = m_DragRoleRect.GetWidth();
	 float ep = -(position.x - m_MouseDpt.x)*0.01f;
	 if(m_pkNiAVControl)
	 {
		 m_pkNiAVControl->SetRotatePlayer(ep);
	 }
	 m_MouseDpt = position;
 }
 UControl* UCreatePlayer::FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */)
 {
	 int _width = GetRoot()->GetWidth();
	 int _height = GetRoot()->GetHeight();

	 URect DragPlayerRect;
	 DragPlayerRect.pt0.x = (442*_width)/1024;

	 DragPlayerRect.pt0.y = (96*_height)/768;

	 DragPlayerRect.pt1.x = DragPlayerRect.pt0.x + (346*_width)/1024;

	 DragPlayerRect.pt1.y = DragPlayerRect.pt0.y + (550*_height)/768;

	 m_DragRoleRect = DragPlayerRect;
	 if (m_DragRoleRect.PointInRect(pt))
	 {
		 return this;
	 }
	 return UControl::FindHitWindow(pt,initialLayer);
 }
void UCreatePlayer::OnClickBack()
{
	m_CreateName->Clear();
	CUISystem::ShowEX(DID_CHOOSEPLAYER,TRUE,TRUE);
}
void UCreatePlayer::ActiveBtn()
{
	if (GetChildByID(16))
	{
		GetChildByID(16)->SetActive(TRUE);
	}
}
void UCreatePlayer::OnClickCreatePlayer()    //点击创建
{
	char BUF[64];
	if (m_CreateName->GetText(BUF,64)<= 0)
	{
		UMessageBox::MsgBox_s( _TRAN("Enter a Name!") );
	}
    else
	{
		if(GetNameFliter(BUF))
		{
			PacketBuilder->SendCreateRoleReqMsg(BUF,m_Sex,m_Race,m_Class);
			if (GetChildByID(16))
			{
				GetChildByID(16)->SetActive(FALSE);
			}
		}
  //      if( strchr(BUF, ' ')
  //       || strchr(BUF, '\t')
  //       || strchr(BUF, '\r')
  //       || strchr(BUF, '\n'))
  //      {
  //          UMessageBox::MsgBox_s("对不起，您输入的名字中含有空白字符！");
  //      }
		//else if (!g_pkChatFilterDB->GetFilterChatMsg(std::string(BUF)))
		//{
		//	PacketBuilder->SendCreateRoleReqMsg(BUF,m_Sex,m_Race,m_Class);
		//}
		//else
		//{
		//	UMessageBox::MsgBox_s("对不起，您输入的名字中带有不合法字符！");
		//}
	}
	m_CreateName->Clear();
}

void UCreatePlayer::Click_Ren()
{
	if (m_CreateName)
	{
		m_CreateName->SetFocusControl();
	}
	
	if (m_Race == RACE_REN)
	{
		return ;
	}
	m_Race = RACE_REN;
	
	if(m_pkNiAVControl)
	{
		m_pkNiAVControl->SetObjectFile("Data\\UI\\Assert\\ChoosePlayer\\Choose.nif");
		m_RaceIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\ren.png");
		m_pkNiAVControl->SetCreateRoleShow(m_Race ,m_Sex, m_Class);
	}

}
void UCreatePlayer::Click_Wu()
{
	if (m_CreateName)
	{
		m_CreateName->SetFocusControl();
	}
	if (m_Race == RACE_WU)
	{
		return;
	}
	m_Race = RACE_WU;

	if(m_pkNiAVControl)
	{
		m_pkNiAVControl->SetObjectFile("Data\\UI\\Assert\\ChoosePlayer\\w_cha_select.nif");
		m_RaceIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\wu.png");
		m_pkNiAVControl->SetCreateRoleShow(m_Race ,m_Sex ,m_Class);
	}	
}
void UCreatePlayer::Click_Yao()
{
	if (m_CreateName)
	{
		m_CreateName->SetFocusControl();
	}
	if (m_Race == RACE_YAO)
	{
		return ;
	}
	m_Race = RACE_YAO;
	if(m_pkNiAVControl)
	{
		m_pkNiAVControl->SetObjectFile("Data\\UI\\Assert\\ChoosePlayer\\y_cha_select.nif");
		m_RaceIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\yao.png");
		m_pkNiAVControl->SetCreateRoleShow(m_Race,m_Sex, m_Class);
	}
	
}

//性别选择按钮
void UCreatePlayer::ClickMale()
{
	if (m_CreateName)
	{
		m_CreateName->SetFocusControl();
	}
	if (m_Sex == GENDER_MALE)
	{
		return ;
	}
	m_Sex = GENDER_MALE;

	if (m_pkNiAVControl)
	{
		m_pkNiAVControl->SetCreateRoleShow(m_Race, m_Sex, m_Class);
	}
	m_SexName->SetBitMapByStr("Data\\UI\\CreatePlayer\\male.png");

}
void UCreatePlayer::ClickFmale()
{
	if (m_CreateName)
	{
		m_CreateName->SetFocusControl();
	}
	if (m_Sex == GENDER_FEMALE)
	{
		return ;
	}
	m_Sex = GENDER_FEMALE;
	if (m_pkNiAVControl)
	{
		m_pkNiAVControl->SetCreateRoleShow(m_Race,m_Sex, m_Class);
	}
	m_SexName->SetBitMapByStr("Data\\UI\\CreatePlayer\\fmale.png");
}

//职业选择按钮
void UCreatePlayer::ClickCLASS_WARRIOR()		//	战士	MASK = 1
{
	if (m_CreateName)
	{
		m_CreateName->SetFocusControl();
	}
	if (m_Class == CLASS_WARRIOR)
	{
		return;
	}
	m_Class = CLASS_WARRIOR;
	m_ClassIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\zhanshi.png");
	m_ClassName->SetBitMapByStr("Data\\UI\\CreatePlayer\\n_zhanshi.png");

	m_pkNiAVControl->SetCreateRoleShow(m_Race ,m_Sex, m_Class);
}
void UCreatePlayer::ClickCLASS_BOW()			//	箭师	MASK = 4
{
	if (m_CreateName)
	{
		m_CreateName->SetFocusControl();
	}
	if (m_Class == CLASS_BOW)
	{
		return ;
	}
	m_Class = CLASS_BOW;
	m_ClassIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\gongjian.png");
	m_ClassName->SetBitMapByStr("Data\\UI\\CreatePlayer\\n_gongjian.png");

	m_pkNiAVControl->SetCreateRoleShow(m_Race ,m_Sex, m_Class);
}
void UCreatePlayer::ClickCLASS_DRUID()		//仙道
{
	if (m_CreateName)
	{
		m_CreateName->SetFocusControl();
	}
	if (m_Class == CLASS_PRIEST)
	{
		return ;
	}
	m_Class = CLASS_PRIEST;
	m_ClassIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\xiandao.png");
	m_ClassName->SetBitMapByStr("Data\\UI\\CreatePlayer\\n_xiandao.png");

	m_pkNiAVControl->SetCreateRoleShow(m_Race ,m_Sex, m_Class);
}

void UCreatePlayer::ClickCLASS_ROGUE()		//	真巫
{
	if (m_CreateName)
	{
		m_CreateName->SetFocusControl();
	}
	if (m_Class == CLASS_MAGE)
	{
		return ;
	}
	m_Class = CLASS_MAGE;
	m_ClassIntr->SetBitMapByStr("Data\\UI\\CreatePlayer\\zhenwu.png");
	m_ClassName->SetBitMapByStr("Data\\UI\\CreatePlayer\\n_zhenwu.png");

	m_pkNiAVControl->SetCreateRoleShow(m_Race ,m_Sex, m_Class);
}
void UCreatePlayer::ClickRight()                //3D 模型右转
{
	if (m_CreateName)
	{
		m_CreateName->SetFocusControl();
	}
	if(m_pkNiAVControl)
	{
		m_pkNiAVControl->SetRotatePlayer(-0.05f);
	}
}
void UCreatePlayer::ClickLeft()                 //3D 模型左转
{
	if (m_CreateName)
	{
		m_CreateName->SetFocusControl();
	}
	if(m_pkNiAVControl)
	{
		m_pkNiAVControl->SetRotatePlayer(0.05f);
	}
}