#pragma once
#include "UInc.h"

class UNiAVControlR;
class UCreatePlayer : public UControl
{
	UDEC_CLASS(UCreatePlayer);
	UDEC_MESSAGEMAP();
public:
	
	UCreatePlayer();
	~UCreatePlayer();

	void ActiveBtn();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);
	virtual UControl * FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */);
protected:
	void OnClickCreatePlayer();    //点击创建
	void OnClickBack();

	//种族选择按钮
	void Click_Ren();
	void Click_Wu();
	void Click_Yao();
	//性别选择按钮
	void ClickMale();
	void ClickFmale();
	//职业选择按钮
	void ClickCLASS_WARRIOR();		//	战士	MASK = 1
	void ClickCLASS_BOW();			//	箭师	MASK = 4
	void ClickCLASS_DRUID();		//	仙道	MASK = 2
	void ClickCLASS_ROGUE();		//	真巫	MASK = 8

	void ClickRight();                //3D 模型右转
	void ClickLeft();                 //3D 模型左转
private:	
	UEdit* m_CreateName;
	UBitmap* m_RaceIntr ; //种族说明
	UBitmap* m_ClassIntr ; //职业说明
	UBitmap* m_ClassName; //职业名字显示
	UBitmap* m_SexName; //性别名字显示
	UNiAVControlR* m_pkNiAVControl ;
	int m_Race; // 种族
	int m_Class;// 职业
	int m_Sex;//   性别

	UPoint m_MouseDpt;
	URect m_DragRoleRect;
};