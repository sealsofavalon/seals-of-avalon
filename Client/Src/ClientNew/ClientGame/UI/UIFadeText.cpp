#include "stdafx.h"
#include "UIFadeText.h"

UIMP_CLASS(UIFadeText, UStaticText);

void UIFadeText::StaticInit()
{
}


UIFadeText::UIFadeText()
	:m_fSpeed(0.f)
{
	SetTextEdge(true);
}

void UIFadeText::OnTimer(float fDeltaTime)
{
	if(m_fSpeed != 0.f)
	{
		m_fAlpha += fDeltaTime*m_fSpeed;
		if(m_fAlpha >= 1.f)
		{
			m_fAlpha = 1.f;
			m_fSpeed = 0.f;
		}
		else if(m_fAlpha <= 0.f)
		{
			m_fAlpha = 0.f;
			m_fSpeed = 0.f;
			SetVisible(false);
		}
		m_FontColor.a = unsigned char(m_fAlpha * 255.f);
		m_FontEdgeColor.a = unsigned char(m_fAlpha * 255.f);
	}
}

void UIFadeText::StartFade(float fAlpha, float fSpeed)
{
	SetVisible(true);
	m_fAlpha = fAlpha;
	m_fSpeed = fSpeed;
}