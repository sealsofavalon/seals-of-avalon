
#include "UInc.h"

class UIFadeText : public UStaticText
{
public:
	UIFadeText();

	virtual void OnTimer(float fDeltaTime);

	void StartFade(float fAlpha, float fSpeed);

	UDEC_CLASS(UIFadeText);

private:
	float m_fAlpha;
	float m_fSpeed;
};