#include "StdAfx.h"
#include "UISystem.h"
#include "UIGamePlay.h"
#include "Player.h"
#include "ClientApp.h"
#include "SceneManager.h"
#include "PlayerInputMgr.h"
#include "ClientState.h"
#include "USkillButton.h"
#include "UChat.h"
#include "ObjectManager.h"
#include "../Network/PacketBuilder.h"
#include "UMessageBox.h"
#include "UITipSystem.h"
#include "UIShowPlayer.h"
#include "UAuraSys.h"
#include "UIGameSet.h"
#include "UInGameBar.h"
#include "UIMailSend.h"
#include "UIBank.h"
#include "UIPackage.h"
#include "UIPlayerTrade.h"
#include "UINPCTrade.h"
#include "UNpcDlg.h"
#include "UNpcQuestDlg.h"
#include "UIPickItemList.h"
#include "Effect/EffectManager.h"
#include "UAuctionDlg.h"
#include "SystemTips .h"
#include "SystemSetup.h"
#include "UBshowLevelTips.h"
#include "UEquRefineManager.h"
#include "FreshManHelp.h"
#include "Camera.h"
#include "SocialitySystem.h"
#include "UInstanceMgr.h"
#include "uAuctionMgr.h"
#include "TitleMgr.h"
#include "QuerySys.h"
#include "RankMgr.h"
#include "Hook/HookManager.h"
#include "Hook/UHookSet.h"
#include "UEndowment.h"
//#include "UCreditCard.h"
#include "UFun.h"

UInGame * g_UinGame = NULL;
UIMP_CLASS(UInGame, UControl);
void UInGame::StaticInit()
{

}
UBEGIN_MESSAGE_MAP(UInGame,UCmdTarget)
UON_BN_CLICKED(FRAME_IG_BSHOWLEVELTIPS, &UInGame::ShowORHideLevelTips)
UON_BN_CLICKED(FRAME_IG_FRESHMANHELPBTN, &UInGame::OnFreshManHelp)
UEND_MESSAGE_MAP()

UInGame::UInGame()
{
	g_UinGame = this;
	m_SetUIVisible = TRUE;
	m_MouseKeyModify = 0;
}
UInGame::~UInGame()
{
	//CreditMgr->Destory();
	g_UinGame = NULL;
}

void UInGame::ResetBagAndBank()
{
	UControl* pBag = GetChildByID(FRAME_IG_PACKAGE);	
	UControl* pBank = GetChildByID(FRAME_IG_BANKDLG);

	if (pBag)
	{
		RemoveChild(pBag);
		//pBag->DeleteThis();
		sm_System->DestoryControl(pBag);
		pBag = NULL;
	}

	if (pBank)
	{
		RemoveChild(pBank);
		//pBank->DeleteThis();
		sm_System->DestoryControl(pBank);
		pBank = NULL;
	}
	
}

BOOL UInGame::AddChildUI()
{
	BOOL bSuccess = TRUE;
	////
	//{
	//	bSuccess &= AddSystemSetup();
	//}
	//GameSet
	{
		bSuccess &= AddGameSet();
	}
	//CastBar
	{
		bSuccess &= AddCastBar();

	}
		bSuccess &= AddEndowment();

		bSuccess &= AddHookUI();

		bSuccess &= AddAuctionSystem();

		bSuccess &= AddMailSystem();

		bSuccess &= AddTitleMgr();

		bSuccess &= AddRankMgr();



	//Dialog
	{
		for (int i = FRAME_IG_DLGMIN ; i < FRAME_IG_DLGMAX ; i++)
		{
			bSuccess &= AddDlg(i);
		}
	}
	//ActionBar
	{
		bSuccess &= AddInGameBar();
	}
	//BigMap
	{
		bSuccess &= AddBigMap();
	}
	//right mouse list
	{
		bSuccess &= AddRightMouseList();
	}
	//ToolTips
	{
		bSuccess &= AddToolTipSystem();
	}
	//Chat
	{
		bSuccess &= AddChatSystem();
	}
	//Team and Head
	{
		bSuccess &= AddTeamSystem();
	}
	//Aura
	{
		bSuccess &= AddAuraSystem();
	}
	//Refine
	{
		bSuccess &= AddRefineSystem();
	}
	//SocialitySystem
	{
		bSuccess &= AddSocialitySystem();
	}
	//instanceSystem
	{
		bSuccess &= AddInstanceSystem();
	}
	//
	{
		bSuccess &= AddQuerySystem();
	}
	//MiniMap
	{
		bSuccess &= AddMiniMap();
	}
	return bSuccess;
}
BOOL UInGame::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	ResetBagAndBank();
	if (!AddChildUI())
	{
		return FALSE;
	}
	SetFocusControl();
	if (TeamShow && EquRefineMgr && ChatSystem)
	{
		TeamShow->BeginSystem();
		EquRefineMgr->InitRefineData();
		ChatSystem->Init();
	}
	return TRUE;
}
void UInGame::OnDestroy()
{
	//ResetBagAndBank();
	if(SYState()->LocalPlayerInput)
		SYState()->LocalPlayerInput->SetDecal(0);
	if (TeamShow)
	{
		TeamShow->SetTargetTarget(0);
		TeamShow->EndSystem();
		TeamShow->ClearSystem();
	}
	
	
	if (SYState()->LocalPlayerInput && ObjectMgr->GetLocalPlayer())
	{
		SYState()->LocalPlayerInput->SetTargetID(INVALID_OBJECTID);
	}
	if (SystemSetup)
	{
		SystemSetup->InitSystem();
	}
	EndowmentMgr->Initialize();

	InstanceSys->InitUI();
	SYState()->ClientApp->SetCursor(SYC_ARROW);
	UControl::OnDestroy();
}
void UInGame::OnTimer(float fDeltaTime)
{
	if (!m_bCreated)
	{
		return;
	}
	UControl::OnTimer(fDeltaTime);
	if (TeamShow && TipSystem)
	{
		TipSystem->UpDataTip();
	}
	InstanceSys->Update(fDeltaTime);

	static float timetoUpdateEndowmentLadder = 0.f;
	timetoUpdateEndowmentLadder += fDeltaTime;
	if(timetoUpdateEndowmentLadder > 300.f)
	{
		//EndowmentMgr->UpdateLadder();
		timetoUpdateEndowmentLadder = 0.f;
	}
}
UInGame * UInGame::Get()
{
	return g_UinGame;
}
UControl * UInGame::GetFrame(int index)
{
	if(g_UinGame)
		return g_UinGame->GetChildByID(index);
	else
		return NULL;
}
void UInGame::HelpEventTrigger(UINT index)
{
	if (g_UinGame)
	{
		UFreshManTip* pFreshMantip = INGAMEGETFRAME(UFreshManTip, FRAME_IG_FRESHMANTIP);
		if (pFreshMantip)
		{
			pFreshMantip->HelpEventTrigger(index);
		}
	}
}
BOOL UInGame::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (nDataFlag == UItemSystem::ICT_BAG)
	{
		CUstate pState = CPlayerLocal::GetCUState();
		if (pState == cus_Trade || pState == cus_NPCTrade || pState == cus_Mail || pState == cus_error)
		{
			UMessageBox::MsgBox_s( _TRAN("当前状态不允许摧毁物品!") );
			return TRUE;
		}
		std::string buf;
		const ui8* FromePos = (const ui8*)pDragData;
		CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
		if (localPlayer)
		{
			CStorage* pContainer = localPlayer->GetItemContainer();
			CItemSlot* pSlot = (CItemSlot*)pContainer->GetSlot(*FromePos);	

			if (!pSlot || pSlot->GetCode() == 0)
			{
				return FALSE;
			}
			//sprintf(buf, "摧毁物品：[%s]", pSlot->GetItemProperty()->C_name.c_str());
			buf = _TRAN( N_NOTICE_C4,pSlot->GetItemProperty()->C_name.c_str() );
		}
		SYState()->UI->GetUItemSystem()->SetDropItemPos(*FromePos);
		UMessageBox::MsgBox(buf.c_str(), (BoxFunction*)UItemSystem::ToDestroyItem,(BoxFunction*)UItemSystem::ReSetDestroyPos);
		return TRUE;
	}

	if (nDataFlag == UItemSystem::ICT_INGAME_SKILL)
	{
		const ui8* FromePos = (const ui8*)pDragData;
		SYState()->UI->GetUItemSystem()->SetSkillAciton(*FromePos,0,UItemSystem::SPELL_DATA_FLAG);
		return TRUE;
	}

	if (nDataFlag == UItemSystem::ICT_HOOK_ITEM)
	{
		UHookSet::ActionButton* pkBtn = UDynamicCast(UHookSet::ActionButton, pDragFrom);
		ActionDataInfo datainfo;
		datainfo.entry = 0;
		datainfo.Guid = 0;
		datainfo.type = UItemSystem::ITEM_DATA_FLAG;

		pkBtn->SetItemData(datainfo);
		
		return TRUE ;
	}

	if (nDataFlag == UItemSystem::ICT_HOOK_SKILL)
	{
		UHookSet::ActionButton* pkBtn = UDynamicCast(UHookSet::ActionButton, pDragFrom);
		ActionDataInfo datainfo;
		datainfo.entry = 0;
		datainfo.Guid = 0;
		datainfo.type = UItemSystem::SPELL_DATA_FLAG;

		pkBtn->SetItemData(datainfo);

		return TRUE ;
	}
	return FALSE;
}

BOOL UInGame::OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	//UKeyBoard* pKeyBoard = INGAMEGETFRAME(UKeyBoard, FRAME_IG_KEYBOARD);
	//if(pKeyBoard)
	//{
	//	pKeyBoard->KeyUp(nKeyCode, nFlags);
	//}
	return FALSE;
	//return SYState()->LocalPlayerInput->KeyUp(nKeyCode);
}
BOOL UInGame::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	/*UKeyBoard* pKeyBoard = INGAMEGETFRAME(UKeyBoard, FRAME_IG_KEYBOARD);
	if(pKeyBoard)
	{
		pKeyBoard->KeyDown(nKeyCode, nFlags);
	}*/
		
	return FALSE;
	//return SYState()->LocalPlayerInput->KeyDown(nKeyCode);
}
void UInGame::acceleratorKeyPress(const char * opCode)
{
	UAccelarKey * acKey = sm_System->FindAcceleratorKey(opCode);
	if (!acKey)
	{
		return;
	}
	UINT nKeyCode = StringKey2LK(acKey->mAccelarKeyCode.GetBuffer());
	if (_stricmp(opCode,"MoveForward") == 0)
	{
		SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_W));
	}
	else if (_stricmp(opCode,"MoveBackward") == 0)
	{
		SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_S));
	}
	else if (_stricmp(opCode,"MoveLeft") == 0)
	{
		SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_A));
	}
	else if (_stricmp(opCode,"MoveRight") == 0)
	{
		SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_D));
	}
	else if (_stricmp(opCode,"MoveTurnLeft") == 0)
	{
		SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_Q));
	}
	else if (_stricmp(opCode,"MoveTurnRight") == 0)
	{
		SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_E));
	}
	else if (_stricmp(opCode,"Jump") == 0)
	{
		SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_SPACE));
	}
	else if (_stricmp(opCode,"AutoMove") == 0)
	{
		SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_G));
	}	
	else if (_stricmp(opCode,"AutoChoose") == 0)
	{
		SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_TAB));
	}
	else if (_stricmp(opCode,"ChooseSelf") == 0)
	{
		TeamShow->SelectTarget(PLAYER_HEAD_LOCAL);
	}
	else if (_stricmp(opCode,"ChooseTeam1") == 0)
	{
		TeamShow->SelectTarget(PLAYER_HEAD_TEAMMEM);
	}
	else if (_stricmp(opCode,"ChooseTeam2") == 0)
	{
		TeamShow->SelectTarget(PLAYER_HEAD_TEAMMEM + 1);
	}
	else if (_stricmp(opCode,"ChooseTeam3") == 0)
	{
		TeamShow->SelectTarget(PLAYER_HEAD_TEAMMEM + 2);
	}
	else if (_stricmp(opCode,"ChooseTeam4") == 0)
	{
		TeamShow->SelectTarget(PLAYER_HEAD_TEAMMEM + 3);
	}
	else if(_stricmp(opCode, "HOOK") == 0)
	{
		SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_F11));
	}
    else if(_stricmp(opCode, "HidePlayer") == 0)
    {
        //SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_F6));

		CPlayerLocal* pkLP = (CPlayerLocal*)ObjectMgr->GetLocalPlayer();

		//pkLP->GetCamera().LookPlayer();
		if ( pkLP->isShowPosition() )
		{
			char Pos[256] = {0};
			NiPoint3 ptCur = pkLP->GetPosition();
			float fangle = pkLP->GetFacingAngle();

			sprintf(Pos, "%0.1f, %0.1f, %0.1f, %0.1f", ptCur.x, ptCur.y, ptCur.z, fangle );

			CopyString( Pos );

			ChatSystem->SendChatMsg(CHAT_MSG_SAY,".npc spawn 110000013");
		}
		else
			SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_F12));
		
    }
    else if(_stricmp(opCode, "PrintScreen") == 0)
    {
        SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_SYSRQ));
    }
    else if(_stricmp(opCode, "FullScreen") == 0)
    {
        SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_RETURN));
    }
}
void UInGame::acceleratorKeyRelease(const char * opCode)
{
	UAccelarKey * acKey = sm_System->FindAcceleratorKey(opCode);
	if (!acKey)
	{
		return;
	}
	UINT nKeyCode = StringKey2LK(acKey->mAccelarKeyCode.GetBuffer());
	if (_stricmp(opCode,"MoveForward") == 0)
	{
		SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_W));
	}
	if (_stricmp(opCode,"MoveBackward") == 0)
	{
		SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_S));
	}
	if (_stricmp(opCode,"MoveLeft") == 0)
	{
		SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_A));
	}
	if (_stricmp(opCode,"MoveRight") == 0)
	{
		SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_D));
	}
	if (_stricmp(opCode,"AutoChoose") == 0)
	{
		SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_TAB));
	}
	if (_stricmp(opCode,"MoveTurnLeft") == 0)
	{
		SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_Q));
	}
	if (_stricmp(opCode,"MoveTurnRight") == 0)
	{
		SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_E));
	}
	if (_stricmp(opCode,"Jump") == 0)
	{
		SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_SPACE));
	}	
	if (_stricmp(opCode,"AutoMove") == 0)
	{
		SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_G));
	}
	if(_stricmp(opCode, "AutoAttack") == 0)
	{
		SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_F11));
	}
	if (_stricmp(opCode,"HideUI") == 0)
	{
		m_SetUIVisible = !m_SetUIVisible;
		if (m_SetUIVisible)
		{
			ReleaseCapture();
		}
		else
		{
			CaptureControl();
		}
	}
	//if (_stricmp(opCode,"AreaTriggerTest") == 0)
	//{
	//	SYState()->LocalPlayerInput->KeyUp(LK_T);
	//}
}
void UInGame::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_SetUIVisible)
	{
		UControl::OnRender(offset,updateRect);
	}
}
BOOL UInGame::OnEscape()
{
	if (SYState()->LocalPlayerInput->OnEscape())
		return TRUE;

	if (!UControl::OnEscape())
	{
		UGameSet * ugs = INGAMEGETFRAME(UGameSet,FRAME_IG_GAMESET);
		if (ugs)
		{
			ugs->Show();
			return TRUE;
		}
	}
	else
	{
		return TRUE;
	}
	return FALSE;
}
BOOL UInGame::AddInGameBar()
{
	UControl * InGameBar = NULL;
	if (InGameBar == NULL  && GetChildByID(FRAME_IG_ACTIONSKILLBAR) == NULL)
	{
		InGameBar = sm_System->CreateDialogFromFile("InGameBar\\InGameBar.udg");
		if (InGameBar)
		{
			AddChild(InGameBar);
			InGameBar->SetId(FRAME_IG_ACTIONSKILLBAR);
		}
		else
		{
			return FALSE;
		}
	}

	UControl * bLevelTipsBtn = NULL;
	if (bLevelTipsBtn == NULL && GetChildByID(FRAME_IG_BSHOWLEVELTIPS) == NULL)
	{
		bLevelTipsBtn = sm_System->CreateDialogFromFile("InGameBar\\ShowLevelBUT.udg");
		if (bLevelTipsBtn)
		{
			AddChild(bLevelTipsBtn);
			bLevelTipsBtn->SetId(FRAME_IG_BSHOWLEVELTIPS);
			bLevelTipsBtn->SetVisible(FALSE);
		}
		else
		{
			return FALSE;
		}
	}

	UControl* UEquipWear = NULL;
	if (UEquipWear == NULL && GetChildByID(FRAME_IG_EQUIPWEAR) == NULL)
	{
		UEquipWear = sm_System->CreateDialogFromFile("Equipment\\EquipmentWear.udg");
		if (UEquipWear)
		{
			AddChild(UEquipWear);
			UEquipWear->SetId(FRAME_IG_EQUIPWEAR);
		}else
		{
			return FALSE;
		}
	}
	INT Index = INGAME_SHORTCUTKEY_MAX;
	UControl* ActionBar = NULL;
	if (ActionBar == NULL && GetChildByID(FRAME_IG_ACTIONSKILLBAREX1) == NULL)
	{
		ActionBar = sm_System->CreateDialogFromFile("InGameBar\\NewActionBar_1.udg");
		if (ActionBar)
		{
			AddChild(ActionBar);
			ActionBar->SetId(FRAME_IG_ACTIONSKILLBAREX1);	
			UActionBar * pAb = INGAMEGETFRAME(UActionBar,FRAME_IG_ACTIONSKILLBAREX1);
			if (pAb)
			{
				pAb->SetBarIndex(22, Index);
				Index += 22;
			}
		}
		else
		{
			return FALSE;
		}
	}

	ActionBar = NULL;
	if (ActionBar == NULL && GetChildByID(FRAME_IG_ACTIONSKILLBAREX2) == NULL)
	{
		ActionBar = sm_System->CreateDialogFromFile("InGameBar\\NewBar_2.udg");
		if (ActionBar)
		{
			AddChild(ActionBar);
			ActionBar->SetId(FRAME_IG_ACTIONSKILLBAREX2);	
			UPlug_AcutionBar * pAb = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX2);
			if (pAb)
			{
				pAb->SetBarIndex(10, Index);
				Index += 10;
			}
		}
		else
		{
			return FALSE;
		}
	}

	ActionBar = NULL;
	if (ActionBar == NULL && GetChildByID(FRAME_IG_NEWLEARN) == NULL)
	{
		ActionBar = sm_System->CreateDialogFromFile("InGameBar\\NewLearnBar.udg");
		if (ActionBar)
		{
			AddChild(ActionBar);
			ActionBar->SetId(FRAME_IG_NEWLEARN);	
			ULearnSkillBar * pAb = INGAMEGETFRAME(ULearnSkillBar,FRAME_IG_NEWLEARN);
		}
		else
		{
			return FALSE;
		}
	}

	ActionBar = NULL;
	if (ActionBar == NULL && GetChildByID(FRAME_IG_ACTIONSKILLBAREX3) == NULL)
	{
		ActionBar = sm_System->CreateDialogFromFile("InGameBar\\NewBar_3.udg");
		if (ActionBar)
		{
			AddChild(ActionBar);
			ActionBar->SetId(FRAME_IG_ACTIONSKILLBAREX3);	
			UPlug_AcutionBar * pAb = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX3);
			if (pAb)
			{
				pAb->SetBarIndex(10, Index);
				Index += 10;
			}
		}
		else
		{
			return FALSE;
		}
	}

	ActionBar = NULL;
	if (ActionBar == NULL && GetChildByID(FRAME_IG_ACTIONSKILLBAREX4) == NULL)
	{
		ActionBar = sm_System->CreateDialogFromFile("InGameBar\\NewBar_4.udg");
		if (ActionBar)
		{
			AddChild(ActionBar);
			ActionBar->SetId(FRAME_IG_ACTIONSKILLBAREX4);	
			UPlug_AcutionBar * pAb = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX4);
			if (pAb)
			{
				pAb->SetBarIndex(10, Index);
				Index += 10;
			}
		}
		else
		{
			return FALSE;
		}
	}

	ActionBar = NULL;
	if (ActionBar == NULL && GetChildByID(FRAME_IG_ACTIONSKILLBAREX5) == NULL)
	{
		ActionBar = sm_System->CreateDialogFromFile("InGameBar\\NewBar_5.udg");
		if (ActionBar)
		{
			AddChild(ActionBar);
			ActionBar->SetId(FRAME_IG_ACTIONSKILLBAREX5);	
			UPlug_AcutionBar * pAb = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX5);
			if (pAb)
			{
				pAb->SetBarIndex(10, Index);
				Index += 10;
			}
		}
		else
		{
			return FALSE;
		}
	}

	ActionBar = NULL;
	if (ActionBar == NULL && GetChildByID(FRAME_IG_ACTIONSKILLBAREX6) == NULL)
	{
		ActionBar = sm_System->CreateDialogFromFile("InGameBar\\NewBar_6.udg");
		if (ActionBar)
		{
			AddChild(ActionBar);
			ActionBar->SetId(FRAME_IG_ACTIONSKILLBAREX6);	
			UPlug_AcutionBar * pAb = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX6);
			if (pAb)
			{
				pAb->SetBarIndex(10, Index);
				Index += 10;
			}
		}
		else
		{
			return FALSE;
		}
	}

	ActionBar = NULL;
	if (ActionBar == NULL && GetChildByID(FRAME_IG_ACTIONSKILLBAREX7) == NULL)
	{
		ActionBar = sm_System->CreateDialogFromFile("InGameBar\\NewBar_7.udg");
		if (ActionBar)
		{
			AddChild(ActionBar);
			ActionBar->SetId(FRAME_IG_ACTIONSKILLBAREX7);	
			UPlug_AcutionBar * pAb = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX7);
			if (pAb)
			{
				pAb->SetBarIndex(10, Index);
				Index += 10;
			}
		}
		else
		{
			return FALSE;
		}
	}

	UControl * bSupremeSkill = NULL;
	if (bSupremeSkill == NULL && GetChildByID(FRAME_IG_SUPERSKILL) == NULL)
	{
		bSupremeSkill = sm_System->CreateDialogFromFile("SuperSkill\\SuperKill.udg");
		if (bSupremeSkill)
		{
			AddChild(bSupremeSkill);
			bSupremeSkill->SetId(FRAME_IG_SUPERSKILL);
		}else
		{
			return FALSE;
		}
	}

	g_ActionLock = new ActionBarLock;
	if(!g_ActionLock)
		return FALSE;

	return TRUE;
}
BOOL UInGame::AddSystemSetup()
{
	if (SystemSetup && !SystemSetup->InitSystem())
	{
		return FALSE;
	}
	return TRUE;
}
BOOL UInGame::AddSocialitySystem()
{
	if (SocialitySys && !SocialitySys->CreateFrame(this))
	{
		return FALSE;
	}
	return TRUE;
}
BOOL UInGame::AddMiniMap()
{
	UControl * MiniMap = NULL;
	if (MiniMap == NULL && GetChildByID(FRAME_IG_MINIMAP) == NULL)
	{
		MiniMap = sm_System->CreateDialogFromFile("MiniMap\\MiniMap.udg");
		if (MiniMap)
		{
			AddChild(MiniMap);
			MiniMap->SetId(FRAME_IG_MINIMAP);
		}
		else
		{
			return FALSE;
		}
	}

	return TRUE;
}
BOOL UInGame::AddBigMap()
{
	UControl * BigMap = NULL;
	if (BigMap == NULL && GetChildByID(FRAME_IG_BIGMAP) == NULL)
	{
		BigMap = sm_System->CreateDialogFromFile("BigMap\\BigMap.udg");
		if (BigMap)
		{
			AddChild(BigMap);
			BigMap->SetId(FRAME_IG_BIGMAP);
		}
		else
		{
			return FALSE;
		}
	}

	return TRUE;
}
BOOL UInGame::AddGameSet()
{
	UControl * GameSet = NULL;
	if (GameSet == NULL && GetChildByID(FRAME_IG_GAMESET) == NULL)
	{
		GameSet = sm_System->CreateDialogFromFile("GameSet\\GameSet.udg");
		if (GameSet)
		{
			AddChild(GameSet);
			GameSet->SetId(FRAME_IG_GAMESET);
		}
		else
		{
			return FALSE;
		}
	}

	return TRUE;
}
BOOL UInGame::AddCastBar()
{
	UControl * CastBar = NULL;
	if (CastBar == NULL && GetChildByID(FRAME_IG_CASTBAR) == NULL)
	{
		CastBar = sm_System->CreateDialogFromFile("InGame\\CastBar.udg");
		if (CastBar)
		{
			AddChild(CastBar);
			CastBar->SetId(FRAME_IG_CASTBAR);
		}
		else
		{
			return FALSE;
		}
	}

	CastBar = NULL;
	if (CastBar == NULL && GetChildByID(FRAME_IG_TARGETCASTBAR) == NULL)
	{
		CastBar = sm_System->CreateDialogFromFile("InGame\\TargetCastBar.udg");
		if (CastBar)
		{
			AddChild(CastBar);
			CastBar->SetId(FRAME_IG_TARGETCASTBAR);
		}
		else
		{
			return FALSE;
		}
	}

	CastBar = NULL;
	if (CastBar == NULL && GetChildByID(FRAME_IG_TARGETTARGETCASTBAR) == NULL)
	{
		CastBar = sm_System->CreateDialogFromFile("InGame\\TargetTargetCastBar.udg");
		if (CastBar)
		{
			AddChild(CastBar);
			CastBar->SetId(FRAME_IG_TARGETTARGETCASTBAR);
			//UPoint size = CastBar->GetWindowSize() ;
			//size /= UPoint(2,2);
			//CastBar->SetSize(size);
		}
		else
		{
			return FALSE;
		}
	}
	return TRUE;
}
BOOL UInGame::AddDlg(int Type)
{
	UControl * MainFrameDlg = NULL;
	string filePath;
	BOOL bHide = TRUE;
	switch (Type)
	{
	case FRAME_IG_SKILLDLG:
		filePath = "Skill\\USkill.udg";
		break;
	//case FRAME_IG_FRIENDDLG:
	//	filePatch = "FriendsDlg\\FriendsDlg.udg";
	//	break;
	//case FRAME_IG_FRIENDEDITDLG:
	//	filePatch = "FriendsDlg\\FriendsEditDlg.udg";
	//	break;
	case FRAME_IG_MAILCONTENTDLG:
		filePath = "MailSystem\\MailContent.udg";
		break;
	case FRAME_IG_MAILRECEIVELISTDLG:
		filePath = "MailSystem\\MailReceivel.udg";
		break;
	case FRAME_IG_MAILSENDDLG:
		filePath = "MailSystem\\SendMail.udg";
		break;
	case FRAME_IG_PLAYEREQUIPDLG:
		filePath = "Equipment\\Equipment.udg";
		break;
	case FRAME_IG_PLAYERPETDLG:
		filePath = "Equipment\\PetDlg.udg";
		break;
	case FRAME_IG_FITTINGROOMDLG:
		filePath = "Equipment\\FittingRoom.udg";
		break;
	case FRAME_IG_NPCQUESTDLG:
		filePath = "NpcQuestDlg\\NpcQuestDlg.udg";
		break;
	case FRAME_IG_QUESTMAINDLG:
		filePath = "Quest\\UQuestDlg.udg";
		break;
	case FRAME_IG_QUESTTRACE:
		filePath = "Quest\\UQuestTrace.udg";
		break;
	case FRAME_IG_PICKLISTDLG:
		filePath = "PickItemList\\PickItemList.udg";
		break;
	case FRAME_IG_NPCDLG:
		filePath = "NpcDlg\\NpcDlg.udg";
		break;
	case FRAME_IG_NPCTRADEDLG:
		filePath = "NPCTrade\\NPCTrade.udg";
		break;
	case FRAME_IG_PLAYERTRADEDLG:
		filePath = "PlayerTrade\\UPlayerTrade.udg";
		break;
	case FRAME_IG_BANKDLG:
		filePath = "Bank\\UBank.udg";
		break;
	case FRAME_IG_PACKAGE:
		filePath = "Package\\Package.udg";
		break;
	case FRAME_IG_INPUTAMOUNTDLG:
		filePath = "InPutAmout\\InPut.udg";
		break;
	//case FRAME_IG_GUILDDLG:
	//	filePatch = "Guild\\UGuild.udg";
	//	break;
	//case FRAME_IG_GUILDMESSAGEDLG:
	//	filePatch = "Guild\\UGuildMessage.udg";
	//	break;
	case FRAME_IG_SHOPDLG:
		filePath = "Shop\\ShopSystem.udg";
		break;
	case FRAME_IG_SYSTEMTIPS:
		filePath = "SystemTips\\SystemTips.udg";
		break;
	case FRAME_IG_SHOPPAYDLG:
		filePath = "Shop\\Pay.udg";
		break;
	case FRAME_IG_TEAMPICKSETTING:
		filePath = "UTeamPickSet\\PickItemSet.udg";
		break;
	case FRAME_IG_ROLLITEM:
		filePath = "RollItem\\RollItem.udg";
		break;
	case FRAME_IG_AREA_NAME:
		filePath = "MiniMap\\AreaName.udg";
		break;
    case FRAME_IG_AREA_DESC:
        filePath = "MiniMap\\AreaDesc.udg";
        break;
	case FRAME_IG_DEATHMSGBOX:
		filePath = "ReLive\\DeathMsgBox.udg";
		break;
	case FRAME_IG_RESURRECTMSGBOX:
		filePath = "ReLive\\ReliveMsgBox.udg";
		break;
	case FRAME_IG_NPCQUESTDDLG:
		filePath = "NpcQuestDlg\\NpcQuestDlgD.udg";
		break;
	//case FRAME_IG_LOBBY:
	//	filePatch = "Lobby\\ULobby.udg";
	//	break;
	//case FRAME_IG_BILLBOARD:
	//	filePatch = "Lobby\\UBillBoard.udg";
	//	break;
	//case FRAME_IG_ARENASTATE:
	//	filePatch = "Lobby\\ArenaState.udg";
	//	break;
	//case FRAME_IG_LOBBYQUITDLG:
	//	filePatch = "Lobby\\LobbyQuit.udg";
	//	break;
	case FRAME_IG_SYSGIFT:
		filePath = "SystemTips\\SystemGift.udg";
		break;
	//case FRAME_IG_INSTANCETIME:
	//	filePatch = "Lobby\\InstanceTime.udg";
	//	break;
	case FRAME_IG_SYSGIFTNOTIFY:
		filePath = "SystemTips\\SystemGiftNotify.udg";
		break;
	case FRAME_IG_VIDEOSET:
		filePath = "SystemSet\\VideoSet.udg";
		break;
	case FRAME_IG_AUDIOSET:
		filePath = "SystemSet\\AudioSet.udg";
		break;
	case FRAME_IG_SYSTEMSET:
		filePath = "SystemSet\\InGameSet.udg";
		break;
	case FRAME_IG_KEYSET:
		filePath = "SystemSet\\KeySet.udg";
		break;
	//case FRAME_IG_ENDOWMENTDLG:
	//	filePath = "Endowment\\Endowment.udg";
	//	break;
	case FRAME_IG_NEWBIRD:
		filePath = "Lobby\\UNewBird.udg";
		break;
	case FRAME_IG_HIDEQUESTTRACEBUTTON:
		filePath = "Quest\\UHideButton.udg";
		bHide = FALSE;
		break;
	case FRAME_IG_SYSTEMSCROLLTEXT:
		filePath = "InGameBar\\SystemMsgSText.udg";
		bHide = FALSE;
		break;
	case FRAME_IG_FRESHMANTIP:
		filePath = "FreshManHelp\\UFreshManTip.udg";
		//bHide = FALSE;
		break;
	case FRAME_IG_VIEWPLAYEREQUIPDLG:
		filePath = "Equipment\\ViewEquipment.udg";
		break;
	case FRAME_IG_CLINETSTATETIP:
		filePath = "InGameBar\\ClientStateTip.udg";
		break;
	case FRAME_IG_FRESHMANHELPBTN:
		filePath = "FreshManHelp\\FreshManHelpBtn.udg";
		break;
	case FRAME_IG_MANUFACTURE:
		filePath = "Skill\\UIManufacture.udg";
		break;
	case FRAME_IG_SPELLTRAININGDLG:
		filePath = "Skill\\USkillTrainning.udg";
		break;
	case FRAME_IG_SPECIALITYDLG:
		filePath = "Skill\\USpecialityDlg.udg";
		break;
	}
	if (filePath.length() == 0)
	{
		return TRUE;
	}
	if (MainFrameDlg == NULL && GetChildByID(Type) == NULL)
	{
		MainFrameDlg = sm_System->CreateDialogFromFile(filePath.c_str());
		if (MainFrameDlg)
		{
			AddChild(MainFrameDlg);
			MainFrameDlg->SetId(Type);
		}
		else
		{
			return FALSE;
		}
	}
	if(bHide)
	{
		GetChildByID(Type)->UControl::SetVisible(FALSE);
	}
	return TRUE;
}
BOOL UInGame::AddToolTipSystem()
{
	if (TipSystem && !TipSystem->CreateSystem(this))
	{
		return FALSE;
	}
	return TRUE;
}
BOOL UInGame::AddChatSystem()
{
	if (ChatSystem && !ChatSystem->CreateChat(this))
	{
		return FALSE;
	}
	return TRUE;
}


void UInGame::OnMouseUp(const UPoint& position, UINT flags)
{
	if(m_MouseKeyModify & 1)
		SYState()->LocalPlayerInput->LBClick();

	GAMEMouseUP(NiInputMouse::NIM_LEFT);	
}

void UInGame::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	// 鼠标拾取在CPlayerInputMgr::LBClick()中实现,这里分支造成拾取功能失效
	if (nRepCnt == 1)
	{
		//SYState()->LocalPlayerInput->LBClick();
	}
	else
	{
		SYState()->LocalPlayerInput->LBDblClick();
	}
	GAMEMouseDown(NiInputMouse::NIM_LEFT);
}
void UInGame::OnRightMouseUp(const UPoint& position, UINT flags)
{
	if(m_MouseKeyModify & 2)
		SYState()->LocalPlayerInput->RBClick();

	GAMEMouseUP(NiInputMouse::NIM_RIGHT);	
}
void UInGame::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	// 鼠标拾取在CPlayerInputMgr::RBClick()中实现,这里分支造成拾取功能失效
	//if (nRepCnt == 1)
	//{
	//	SYState()->LocalPlayerInput->RBClick();
	//}
	//else
	//{
	//	SYState()->LocalPlayerInput->RBDblClick();
	//}
	POINT Pos ; 
	Pos.x = position.x ;
	Pos.y = position.y ;
	SYState()->LocalPlayerInput->OnRightMouseDown(Pos, nRepCnt, flags);
	GAMEMouseDown(NiInputMouse::NIM_RIGHT);
}

void UInGame::OnRightMouseDragged(const UPoint& position, UINT flags)
{
	UControl* pHitContorl = FindHitWindow(position);

	POINT Pos ; 
	Pos.x = position.x ;
	Pos.y = position.y ;
	if (pHitContorl && pHitContorl == this)
	{
		if (SYState()->LocalPlayerInput)
		{
			SYState()->LocalPlayerInput->OnMouseMove(Pos,flags);
			SYState()->LocalPlayerInput->MouseMove();
		}
	}
}

BOOL UInGame::HitTestPackage(UPoint pos)
{
	UControl* pHitContorl = FindHitWindow(pos);
	UControl* pCtrl = GetChildByID(FRAME_IG_PACKAGE);
	if (pHitContorl && pCtrl && pCtrl ->GetChildByID(0)  && pHitContorl == pCtrl->GetChildByID(0))
	{
		return TRUE ;
	}
	return FALSE;
}
BOOL UInGame::HitTestIsInGame(UPoint pos )
{
	UControl* pHitContorl = FindHitWindow(pos);
	if (pHitContorl && pHitContorl == this)
	{
		return TRUE ;
	}else
	{
		return FALSE ;
	}
}
void UInGame::OnMouseMove(const UPoint& position, UINT flags)
{
	CPlayerInputMgr* PlayerInput = SYState()->LocalPlayerInput;
	if (!PlayerInput)
		return;
	UNPCTrade* pkNpcTrade = INGAMEGETFRAME(UNPCTrade, FRAME_IG_NPCTRADEDLG);
	if (pkNpcTrade && pkNpcTrade->IsRepair())
		return ;
	POINT p, Pos;
	p.x = position.x;
	p.y = position.y;
	Pos.x = position.x ;
	Pos.y = position.y ;

	if (HitTestIsInGame(position))
	{
		PlayerInput->OnMouseMove(Pos,flags);
		PlayerInput->MouseMove();
	}else if (HitTestPackage(position))
	{
		if (SYState()->UI->GetUItemSystem()->IsUsedBangdItem() )	{
			SYState()->ClientApp->SetCursor(SYC_UNBANGDING);
		}else if (SYState()->UI->GetUItemSystem()->IsUsedEnchantItem()){
			SYState()->ClientApp->SetCursor(SYC_ENCHANT);
		}else{
			SYState()->ClientApp->SetCursor(SYC_ARROW);
		}
	}else if (FindHitWindow(position) == GetChildByID(FRAME_IG_NPCTRADEDLG))
	{
		
	}else
	{
		SYState()->ClientApp->SetCursor(SYC_ARROW);
	}
}
void UInGame::OnMouseLeave(const UPoint& position, UINT flags)
{
	//m_MouseKeyModify = 0;
	//if (SYState()->LocalPlayerInput)
	//{
	//	SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_LEFT));
	//	SYState()->LocalPlayerInput->KeyUp(UILK_2_NIINPUTKEY(LK_RIGHT));
	//}
	m_MouseKeyModify = 0;
}
BOOL UInGame::OnMouseWheel(const UPoint& position, int delta , UINT flags)
{
	CPlayer* pPlayer = ObjectMgr->GetLocalPlayer();
	if (pPlayer)
	{
		pPlayer->GetCamera().SetCameraZ(delta);
		return TRUE;
	}
	return  FALSE;
}
BOOL UInGame::OnChar(UINT nCharCode, UINT nRepCnt, UINT nFlags)
{
	return TRUE;
}
BOOL UInGame::AddTeamSystem()
{
	if (TeamShow && !TeamShow->CreateSystem(this))
	{
		return FALSE;
	}
	return TRUE;
}

BOOL UInGame::AddMailSystem()
{
	if (MailSystemMgr && !MailSystemMgr->CreateMgrControl(this))
	{
		return FALSE ;
	}
	return TRUE;
}
BOOL UInGame::AddRankMgr()
{
	if (RankMgr && !RankMgr->CreateRankUI(this))
	{
		return FALSE;
	}
	RankMgr->InitRankTime();
	return TRUE ;
}
BOOL UInGame::AddHookUI()
{
	if (HookMgr)
	{
		if (!HookMgr->CreateUI(this))
		{
			return FALSE;
		}else
		{
			return TRUE ;
		}
	}
	return FALSE ;
}
BOOL UInGame::AddEndowment()
{
	if(EndowmentMgr)
	{
		if(!EndowmentMgr->CreateFrame(this))
			return FALSE;
		EndowmentMgr->Initialize();
	}
	//if (CreditMgr)
	//{
	//	if (!CreditMgr->CreateFrame(this))
	//		return FALSE;
	//	CreditMgr->Initialize();
	//}
	return TRUE;
}
BOOL UInGame::AddTitleMgr()
{
	if (TitleMgr && !TitleMgr->CreateTitleUI(this))
	{
		return FALSE ;
	}
	return TRUE;
}
BOOL UInGame::AddAuctionSystem()
{
	//if (AuctionMgr == NULL)
	//{
	//	AuctionMgr = AuctionManager::InitMgr();
	//	if (AuctionMgr)
	//	{
	//		if (!AuctionMgr->CreateMgrControl(this))
	//		{
	//			return FALSE;
	//		}
	//	}else
	//	{
	//		return FALSE;
	//	}
	//}
	if (AUMgr && !AUMgr->CreateFrame(this))
	{
		return FALSE;
	}
	AUMgr->Initialize();
	return TRUE;
}
BOOL UInGame::AddRefineSystem()
{
	if (EquRefineMgr && !EquRefineMgr->CreateMgrControl(this))
	{
		return FALSE;
	}
	return TRUE;
}

BOOL UInGame::AddInstanceSystem()
{
	if(InstanceSys && !InstanceSys->CreateUI(this))
	{
		return FALSE;
	}
	return TRUE;
}

BOOL UInGame::AddQuerySystem()
{
	if(QueryMgr)
	{
		if (!QueryMgr->Create(this))
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

BOOL UInGame::AddAuraSystem()
{
	if (AuraSystem && !AuraSystem->CreateSystem(this))
	{
		return FALSE;
	}
	return TRUE;
}
BOOL UInGame::AddRightMouseList()
{
	UControl * RMD = NULL;
	if (RMD == NULL && GetChildByID(FRAME_IG_RIGHTMOUSELIST) == NULL)
	{
		RMD = sm_System->CreateDialogFromFile("RMD\\RMDUI.udg");
		if (RMD)
		{
			AddChild(RMD);
			RMD->SetId(FRAME_IG_RIGHTMOUSELIST);
		}
		else
		{
			return FALSE;
		}
	}

	return TRUE;
}

void UInGame::SetUIState(UINT pOldState, UINT pNewState)
{
	if (pOldState >= cus_Normal && pOldState < cus_error  && pNewState >= cus_Normal && pNewState < cus_error)
	{
		CPlayerLocal * pLocal = ObjectMgr->GetLocalPlayer();
		if (!pLocal)
		{
			return ;
		}

		UBank* pBank = INGAMEGETFRAME(UBank, FRAME_IG_BANKDLG);
		UPlayerTrade* pTrade = INGAMEGETFRAME(UPlayerTrade, FRAME_IG_PLAYERTRADEDLG);
		UNPCTrade* pNPCTrade = INGAMEGETFRAME(UNPCTrade, FRAME_IG_NPCTRADEDLG);
		UNpcDlg* pNPCDlg = INGAMEGETFRAME(UNpcDlg, FRAME_IG_NPCDLG);
		UNpcQuestDlg* pNPCQuestDlg = INGAMEGETFRAME(UNpcQuestDlg, FRAME_IG_NPCQUESTDLG);
		UPickItemList* pPickListDlg = INGAMEGETFRAME(UPickItemList, FRAME_IG_PICKLISTDLG);
		UNpcQuestDDlg* pNPCQuestDDlg = INGAMEGETFRAME(UNpcQuestDDlg, FRAME_IG_NPCQUESTDDLG);
		
		if (pOldState == pNewState && pOldState == cus_Normal)
		{
			if (pBank)
			{
				pBank->SetCUSNormal();  // 
			}
			if (MailSystemMgr)
			{
				MailSystemMgr->SetMailState(CMailSystem::UMailStateNormal);
			}
			if (pTrade)
			{
				pTrade->ReleaseUITrade();
			}
			
			if (pNPCTrade)
			{
				pNPCTrade->SetCUSNormal();
			}
			if (pNPCDlg)
			{
				pNPCDlg->SetCUSNormal();
			}

			if (pNPCQuestDDlg)
			{
				pNPCQuestDDlg->SetCUSNormal();
			}
			if (AUMgr)
			{
				AUMgr->HideFrame();
			}
			
			return ;
		}
		//拍卖
		//UAuctionBrowseDlg* pAucBrow = INGAMEGETFRAME(UAuctionBrowseDlg, FRAME_IG_AUCTIONBROWSEDLG);
		//UAuctionBidDlg* pAucBig = INGAMEGETFRAME(UAuctionBidDlg, FRAME_IG_AUCTIONBIDDLG);
		//UAuctionAucDlg* pAucAuc = INGAMEGETFRAME(UAuctionAucDlg, FRAME_IG_AUCTIONAUCDLG);
		UInGameBar* pkInGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);

		ui64 pOldNPCGUID = 0;
		ui64 pNewNPCGUID = 0;

       //对前一个状态的UI进行操作。
		switch(pOldState)
		{
		case cus_Normal:
			break ;
		case cus_Bank:
			if (pBank)
			{
				pOldNPCGUID = pBank->GetNPCGUID();
				pBank->SetCUSNormal();  // 
			}
			break;
		case cus_Mail:
			if (MailSystemMgr)
			{
				pOldNPCGUID = MailSystemMgr->GetNPCGuid();
				MailSystemMgr->SetMailState(CMailSystem::UMailStateNormal);
			}
			break ;
		case cus_Trade:
			if (pTrade)
			{
				pTrade->ReleaseUITrade();
			}
			break;
		case cus_NPCTrade:
			if (pNPCTrade)
			{
				pOldNPCGUID = pNPCTrade->GetNpcId();
				pNPCTrade->SetCUSNormal();
			}
			break;
		case cus_NPC:
			if (pNPCDlg)
			{
				pOldNPCGUID = pNPCDlg->GetNPCGUID();
				pNPCDlg->SetCUSNormal();
			}
			break;
		case cus_Quest:
			if (pNPCQuestDlg)
			{
				pOldNPCGUID = pNPCQuestDlg->GetNpcGUID();
				pNPCQuestDlg->SetCUSNormal();
			}
			break ;
		case cus_QuestD:
			if(pNPCQuestDDlg)
			{
				pOldNPCGUID = pNPCQuestDDlg->GetNpcGUID();
				pNPCQuestDDlg->SetCUSNormal();
			}
			break;
		case cus_Pick:
			if (pPickListDlg)
			{
				pPickListDlg->SetCUSNormal();
			}
			break;
		case cus_Auction:
			{
				AUMgr->HideFrame();
				sm_System->GetCurFrame()->SetFocusControl();
				pOldNPCGUID = AUMgr->GetNPCGuid();
			}
			break;
		}

		if (pOldNPCGUID)
		{
			CCharacter* pkOldNPC = static_cast<CCharacter*>(ObjectMgr->GetObject(pOldNPCGUID));
			if (pkOldNPC)
			{
				//NPC设置为等待状态
				pkOldNPC->SetWaitState(TRUE);
				if (pkOldNPC->GetGameObjectType() == GOT_CREATURE)
				{
					((CCreature*)pkOldNPC)->SetFacingLast();
				}
				
			}
		}
		//刷新当前的UI状态
		switch(pNewState)
		{
		case cus_Normal:
			break;
		case cus_Bank:
			if (pBank)
			{
				pNewNPCGUID = pBank->GetNPCGUID();
				pBank->SetVisible(TRUE);
				INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE)->SetVisible(TRUE);
			}
			break;
		case cus_Mail:
			if (MailSystemMgr)
			{
				pNewNPCGUID = MailSystemMgr->GetNPCGuid();	
				MailSystemMgr->SetMailState(CMailSystem::UMailState_List);
			}
			break;
		case cus_Trade:
			if (pTrade)
			{
				pTrade->SetVisible(TRUE);
			}
			break;
		case cus_NPCTrade:
			if (pNPCTrade)
			{
				pNewNPCGUID = pNPCTrade->GetNpcId();
				pNPCTrade->SetVisible(TRUE);
				if (pkInGameBar)
				{
					UButton* btn = UDynamicCast(UButton, pkInGameBar->GetChildByID(UINGAMEBAR_BUTTONPACKAGE));
					if (btn && !btn->IsChecked())
					{
						btn->SetCheck(FALSE);
					}
				}
				
			}
			break;
		case cus_NPC:
			if(pNPCDlg)
			{
				pNewNPCGUID = pNPCDlg->GetNPCGUID();
				pNPCDlg->SetVisible(TRUE);
			}
			break;
		case cus_Quest:
			if (pNPCQuestDlg)
			{
				pNewNPCGUID = pNPCQuestDlg->GetNpcGUID();
				pNPCQuestDlg->SetVisible(TRUE);
			}
			break;
		case cus_QuestD:
			if(pNPCQuestDDlg)
			{
				pNewNPCGUID = pNPCQuestDDlg->GetNpcGUID();
				pNPCQuestDDlg->SetVisible(TRUE);
			}
			break;
		case cus_Pick:
			if (pPickListDlg)
			{
				//pPickListDlg->SetVisible(TRUE);
			}
			break;
		case cus_Auction:
			if (AUMgr)
			{
				AUMgr->ShowFrame();
				pNewNPCGUID = AUMgr->GetNPCGuid();
			}
			break;
		}
		
		if (pNewNPCGUID)
		{
			//CCreature* pkNewNPC = (CCreature*)ObjectMgr->GetObject(pNewNPCGUID);
			CCharacter* pkNewNPC = static_cast<CCharacter*>(ObjectMgr->GetObject(pNewNPCGUID));
		
			if (pkNewNPC && pkNewNPC->GetGameObjectType() == GOT_CREATURE)
			{
				CCreature* pkCreature = (CCreature*)pkNewNPC;
				//设置NPC和玩家朝向. 并播放DIALOG动画
				if ( !pLocal->IsMoving() )
				    pLocal->SetDirectionTo(pkCreature->GetPosition());

				if (!pkCreature->GetName().Equals( _TRAN("任务公告板")) )
				{
					pkCreature->SetRecordLastFacing();
					pkCreature->SetDirectionTo(pLocal->GetPosition());
					pkCreature->SetDialogAnimation();
				}	
			}
		}
	}

#ifdef _DEBUG
	char LogTxt[_MAX_PATH];
	string OldTxt;
	string NewTxt;
	
	switch(pNewState)
	{
	case cus_Normal:
		NewTxt = "Normal";
		break;
	case cus_Bank:
		NewTxt = "Bank";
		break;
	case cus_Mail:
		NewTxt = "Mail";
		break;
	case cus_Trade:
		NewTxt = _TRAN("玩家交易界面");
		break;
	case cus_NPCTrade:
		NewTxt = _TRAN("NPC购买界面");
		break;
	case cus_NPC:
		NewTxt = _TRAN("NPC对话界面");
		break;
	case cus_Quest:
		NewTxt = _TRAN("NPC任务界面");
		break;
	case cus_Pick:
		NewTxt = _TRAN("拾取界面");
		break;
	case cus_Auction:
		NewTxt = _TRAN("拍卖界面");
		break;
	}
	switch(pOldState)
	{
	case cus_Normal:
		OldTxt = "Normal";
		break;
	case cus_Bank:
		OldTxt = "Bank";
		break;
	case cus_Mail:
		OldTxt = "Mail";
		break;
	case cus_Trade:
		OldTxt = _TRAN("玩家交易界面");
		break;
	case cus_NPCTrade:
		OldTxt = _TRAN("NPC购买界面");
		break;
	case cus_NPC:
		OldTxt = _TRAN("NPC对话界面");
		break;
	case cus_Quest:
		OldTxt = _TRAN("NPC任务界面");
		break;
	case cus_Pick:
		OldTxt = _TRAN("拾取界面");
		break;
	case cus_Auction:
		OldTxt = _TRAN("拍卖界面");
		break;
	}


	NiSprintf(LogTxt,_MAX_PATH,"newState:%s ... OldState:%s",NewTxt.c_str(), OldTxt.c_str());

	EffectMgr->AddEffeText(NiPoint3(300.f,300.f,0.0f),LogTxt);
	ULOG(LogTxt);
#endif
}
void UInGame::ShowORHideLevelTips()
{
	//SystemTips * pTip = INGAMEGETFRAME(SystemTips,FRAME_IG_SYSTEMTIPS);
	UQueryFrame* pQueryFrame = INGAMEGETFRAME(UQueryFrame, FRAME_IG_QUERYFRAME);
	UBShowLevelTips* pBsHOW = INGAMEGETFRAME(UBShowLevelTips, FRAME_IG_BSHOWLEVELTIPS);
	//if (pTip)
	//{
	//	//if (pBsHOW->IsChecked())
	//	//{
	//	//	ULOG("show Tips");
	//	//}else
	//	//{
	//	//	ULOG("close tips");
	//	//}
	//	pTip->GetTipsGroup();
	//	this->MoveChildTo(pTip, NULL);
	//	pTip->SetVisible(pBsHOW->IsChecked());
	//}
	if (pQueryFrame && pBsHOW)
	{
		BOOL isShow = pBsHOW->IsChecked();
		pQueryFrame->SetVisible(isShow);
		if (isShow)	{
			MoveChildTo(pQueryFrame, NULL);
		}
	}
}
void UInGame::OnFreshManHelp()
{
	//GetFrame(FRAME_IG_FRESHMANHELPBTN)->SetVisible(FALSE);
	//GameTur->ShowTutorial();
}
void UInGame::TurtoralShow()
{
	//GetFrame(FRAME_IG_FRESHMANHELPBTN)->SetVisible(TRUE);
	//GetFrame(FRAME_IG_FRESHMANHELPBTN)->SetGlint(TRUE);
}
UINT UInGame::IsMouseDown(int eButton)
{
	if(eButton == NiInputMouse::NIM_LEFT)
	{
		return m_MouseKeyModify & 1;
	}
	if(eButton ==NiInputMouse::NIM_RIGHT)
	{
		return m_MouseKeyModify & 2;
	}

	return 0 ;
}
void UInGame::GAMEMouseDown(int eButton)
{
	if(eButton == NiInputMouse::NIM_LEFT)
	{
		m_MouseKeyModify |= 1;
	}
	if(eButton == NiInputMouse::NIM_RIGHT)
	{
		m_MouseKeyModify |= 2;
	}
}
void UInGame::GAMEMouseUP(int eButton)
{
	if(eButton == NiInputMouse::NIM_LEFT)
	{
		m_MouseKeyModify &= 1;
	}
	if(eButton == NiInputMouse::NIM_RIGHT)
	{
		m_MouseKeyModify &= 2;
	}
}