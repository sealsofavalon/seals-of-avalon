#pragma once
#include "UInc.h"
#include "UIIndex.h"

#define  INGAME_SHORTCUTKEY_MAX  12
#define INGAMEGETFRAME(classname,index) UDynamicCast(classname,UInGame::GetFrame(index))
#define HELPEVENTTRIGGER(index) UInGame::HelpEventTrigger(index);
class UInGame : public UControl
{
	UDEC_CLASS(UInGame);
	UDEC_MESSAGEMAP();
public:
	UInGame();
	~UInGame();
	//void UseSkillKeyboard(int pos); // 技能快捷
	void ResetBagAndBank();
	static void SetUIState(UINT pOldState, UINT pNewState);
	void ShowORHideLevelTips();
	void TurtoralShow();
	void OnFreshManHelp();
	BOOL HitTestIsInGame(UPoint pos );
	BOOL HitTestPackage(UPoint pos);
private:
	BOOL AddChildUI(); 

	BOOL AddInGameBar();//底边技能快捷，以及按钮栏
	BOOL AddMiniMap();//小地图
	BOOL AddBigMap();//大地图
	BOOL AddGameSet();//游戏设置对话框
	BOOL AddCastBar();//技能吟唱条
	BOOL AddAuctionSystem();
	BOOL AddMailSystem();
	BOOL AddTitleMgr();
	BOOL AddRankMgr();
	BOOL AddHookUI();
	BOOL AddEndowment();

	BOOL AddDlg(int Type);//各个Dialog
	BOOL AddToolTipSystem();//鼠标提示系统
	BOOL AddChatSystem();//聊天系统
	BOOL AddTeamSystem();//组队以及头像系统
	BOOL AddAuraSystem();//buff以及debuff系统
	BOOL AddRightMouseList();//鼠标右键菜单
	BOOL AddSystemSetup();//系统设置
	BOOL AddSocialitySystem();//社交系统

	BOOL AddRefineSystem();
	BOOL AddInstanceSystem();//副本大厅
	BOOL AddQuerySystem();//帮助查询
public:
	static UControl * GetFrame(int index);
	static UInGame  * Get();
	static void HelpEventTrigger(UINT index);
	UINT IsMouseDown(int eButton);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnRightMouseDragged(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
	virtual BOOL OnMouseWheel(const UPoint& position, int delta , UINT flags);
	virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnChar(UINT nCharCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnEscape();
	virtual void acceleratorKeyPress(const char * opCode);
	virtual void acceleratorKeyRelease(const char * opCode);
private:
	BOOL m_SetUIVisible;
	BOOL m_bMouseMoveInGame;
	UINT m_MouseKeyModify;
	void GAMEMouseDown(int eButton);
	void GAMEMouseUP(int eButton);
};
