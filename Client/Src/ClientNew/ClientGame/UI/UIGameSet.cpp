#include "stdafx.h"
#include "UIGameSet.h"
#include "UIShowPlayer.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "UInGameBar.h"
#include "UMessageBox.h"
#include "../Network/PacketBuilder.h"
#include "../Network/NetworkManager.h"
#include "USystemSet.h"
#include "SocialitySystem.h"

UIMP_CLASS(UGameSet,UDialog);
UBEGIN_MESSAGE_MAP(UGameSet,UDialog)
UON_BN_CLICKED(0 , &UGameSet::ClickBaseSet)  //6
UON_BN_CLICKED(1 , &UGameSet::ClickVideoSet)  //0
UON_BN_CLICKED(2 , &UGameSet::ClickSoundSet)  //1
UON_BN_CLICKED(3 , &UGameSet::ClickKeySet)  //2
UON_BN_CLICKED(4 , &UGameSet::ClickToChoosePlayer)  //3
UON_BN_CLICKED(5 , &UGameSet::ClickRestartGame)  //4
UON_BN_CLICKED(6 , &UGameSet::ClickQuitGame)  //5
UON_BN_CLICKED(7 , &UGameSet::ClickReturnGame)  //6
UEND_MESSAGE_MAP()

void UGameSet::StaticInit()
{

}
UGameSet::UGameSet()
{

}
UGameSet::~UGameSet()
{

}
BOOL UGameSet::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	return TRUE;
}
void UGameSet::OnDestroy()
{
	UDialog::OnDestroy();
}
void UGameSet::QuitGame()
{
	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->SendLeaveTeam();
	}
	PacketBuilder->SendExitGameReqMsg();
	PostQuitMessage(0);
}
void UGameSet::QuitCancel()
{
	UGameSet * ugs = INGAMEGETFRAME(UGameSet,FRAME_IG_GAMESET);
	if (ugs)
	{
		ugs->Hide();
	}
}
void UGameSet::ClickQuitGame()     //退出游戏
{
	UMessageBox::MsgBox( _TRAN("您确认要退出游戏么？"), (BoxFunction*)UGameSet::QuitGame, (BoxFunction*)UGameSet::QuitCancel , 15.f, true);
	Hide();
}
void UGameSet::ClickToChoosePlayer()  //重新选人
{
	Hide();
	PacketBuilder->SendLogOutMsg();
}
void UGameSet::ClickReturnGame()			//返回游戏
{
	Hide();
	
}
void UGameSet::ClickVideoSet()		//视频设定
{
	UVideoSet* pVideoSet = INGAMEGETFRAME(UVideoSet, FRAME_IG_VIDEOSET);
	if (pVideoSet)
	{
		pVideoSet->SetVisible(TRUE);
		Hide();
	}
}
void UGameSet::ClickSoundSet()		//声音设定
{
	UAudioSet* pAudioSet = INGAMEGETFRAME(UAudioSet, FRAME_IG_AUDIOSET);
	if (pAudioSet)
	{
		pAudioSet->SetVisible(TRUE);
		Hide();
	}
}
void UGameSet::ClickBaseSet()
{
	UInGameSet* pBaseSet = INGAMEGETFRAME(UInGameSet, FRAME_IG_SYSTEMSET);
	if (pBaseSet)
	{
		pBaseSet->SetVisible(TRUE);
		Hide();
	}
	
}
void UGameSet::ClickKeySet()		//按键设定
{
	UKeySet* pKeySet = INGAMEGETFRAME(UKeySet, FRAME_IG_KEYSET);
	if (pKeySet)
	{
		pKeySet->SetVisible(TRUE);
		Hide();
	}
}
void UGameSet::ClickRestartGame()	//重新启动游戏
{
	Hide();
	NetworkMgr->Disconnect();
	CUISystem::ShowEX(DID_LOGIN,TRUE,TRUE);
}
void UGameSet::SetVisible(BOOL value)
{
	UDialog::SetVisible(value);
}
BOOL UGameSet::OnEscape()
{
	if(!UControl::OnEscape())
	{
		Hide();
		return TRUE;
	}
	return FALSE;
}
void UGameSet::OnMouseMove(const UPoint& position, UINT flags)
{
	return;
}
void UGameSet::Show()
{
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pInGame)
	{
		UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
		if (pGameBar)
		{
			UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONGAMESET);
			if (pBtn)
			{
				pBtn->SetCheck(FALSE);
			}
		}
	}
}
void UGameSet::Hide()
{
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pInGame)
	{
		UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
		if (pGameBar)
		{
			UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONGAMESET);
			if (pBtn)
			{
				pBtn->SetCheck(TRUE);
			}
		}
	}
}