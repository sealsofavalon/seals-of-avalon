#ifndef __UIGAMESET_H__
#define __UIGAMESET_H__
#include "UInc.h"

class UGameSet : public UDialog
{
	UDEC_CLASS(UGameSet);
	UDEC_MESSAGEMAP();
public:
	UGameSet();
	~UGameSet();
public:
	static void QuitGame();
	static void QuitCancel();
protected:
	void ClickQuitGame();     //退出游戏
	void ClickToChoosePlayer();  //重新选人
	void ClickReturnGame();			//返回游戏
	void ClickVideoSet();		//视频设定
	void ClickSoundSet();		//声音设定
	void ClickKeySet();			//按键设定
	void ClickRestartGame();	//重新启动游戏
	void ClickBaseSet();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnMouseMove(const UPoint& position, UINT flags);
public:
	virtual void SetVisible(BOOL value);
	void Show();
	void Hide();
private:
};
#endif