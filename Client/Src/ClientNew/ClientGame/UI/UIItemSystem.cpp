#include "StdAfx.h"
#include "UIItemSystem.h"
#include "UISystem.h"
#include "UIGamePlay.h"
#include "..\Player.h"
#include "..\ObjectManager.h"
#include "ItemManager.h"
#include "ItemSlot.h"
#include "Network/PacketBuilder.h"
#include "UIPlayerTrade.h"
#include "UIPackage.h"
#include "UAuctionDlg.h"
#include "UINPCTrade.h"
#include "UIPlayerEquipment.h"
#include "UIBank.h"
#include "UInGameBar.h"
#include "ui/UMessageBox.h"
#include "UIMailSend.h"
#include "EquipSlot.h"
#include "AudioInterface.h"
#include "ui/UAuctionMgr.h"
#include "ui/UFun.h"
#include "ui/ShopSystem.h"
#include "Effect/EffectManager.h"
#include "ClientApp.h"
#include "SocialitySystem.h"
#include "ui/UChat.h"
#include "PlayerInputMgr.h"
#include "ui/UISkill.h"

using namespace std;

#define NEATENTIME 9.0f
//UI界面对物品的处理
UItemSystem::UItemSystem()
{
	m_DropPos = -1;
	m_isNeaten[0] = false ;
	m_isNeaten[1] = false ;

	m_isBeginNeatenTime[0] = false;
	m_isBeginNeatenTime[1] = false;

	m_NeatenTime[0] = 0.0f;
	m_NeatenTime[1] = 0.0f;

	m_UseSpecialItem = new UseSpecialItem; 
}
UItemSystem:: ~UItemSystem()
{
	if (m_UseSpecialItem)
	{
		m_UseSpecialItem->CancelUseItem() ;
		delete m_UseSpecialItem ;
		m_UseSpecialItem  = NULL ;
	}
}

void UItemSystem::MoveItemMsg(unsigned char dwIctFrom, unsigned char dwIctTo, ui8 nUIPosFrom, int nUIPosTo, int num )
{
	if (m_isNeaten[0] || m_isNeaten[1])
	{
		return ;
	}
	UInGame* pUinGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pUinGame == NULL)
	{
		return ;
	}
	UPlayerTrade * pPlayerTrade = INGAMEGETFRAME(UPlayerTrade,FRAME_IG_PLAYERTRADEDLG);
	UPackage    * pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
	UMailSend* pMailSend = INGAMEGETFRAME(UMailSend,FRAME_IG_MAILSENDDLG);

	if (pPlayerTrade == NULL || pPackage == NULL)
	{
		return ;
	}
	
	BOOL bInTrade = pPlayerTrade->IsVisible(); 
	//可以使用的包裹位置数目
    int BagSlotCount = pPackage->GetBagSlotCount();

	//检测位置是否合法的可用位置. 如果不是的话返回操作. 
	// 修改以满足-1的条件
	if (dwIctTo == UItemSystem::ICT_BAG)
	{
		if (nUIPosTo >= BagSlotCount )
		{
			return ;
		}
	}


	if (dwIctFrom == UItemSystem::ICT_BAG)
	{
		if (nUIPosFrom >= BagSlotCount)
		{
			return ;
		}
	}
	if (dwIctFrom == UItemSystem::ICT_PLAYER_TRADE_TENNET || dwIctTo == UItemSystem::ICT_PLAYER_TRADE_TENNET)
	{
		return ;
	}
	if(dwIctFrom == UItemSystem::ICT_PLAYER_TRADE && dwIctTo == UItemSystem::ICT_PLAYER_TRADE)
	{
		return ;
	}
	//
	if (dwIctFrom == UItemSystem::ICT_BAG && dwIctTo == ICT_AUC)
	{
		//
		BagToAuc(nUIPosFrom);
	}
	else if(dwIctFrom == ICT_AUC && dwIctTo == ICT_BAG)
	{
		AucToBag();
	}

	if (dwIctFrom == UItemSystem::ICT_BAG && dwIctTo == ICT_MAIL)
	{
		BagToMail(nUIPosFrom, nUIPosTo);
	}else if (dwIctFrom == UItemSystem::ICT_MAIL && dwIctTo == UItemSystem::ICT_BAG)
	{
		MailToBag(nUIPosFrom);
	}

	//包裹拖动到交易
	if (dwIctFrom == UItemSystem::ICT_BAG && dwIctTo == UItemSystem::ICT_NPC_TRADE)
	{
		BagToNpcTrade(nUIPosFrom,num);

	}else if (dwIctFrom == UItemSystem::ICT_BAG && dwIctTo == UItemSystem::ICT_PLAYER_TRADE)
	{
		// 包裹到本地交易
		PlayerTradeMoveMsg(nUIPosFrom,nUIPosTo,num);

	}else if( dwIctFrom == UItemSystem::ICT_PLAYER_TRADE && dwIctTo == UItemSystem::ICT_BAG)
	{
		PlayerTradeRelease(nUIPosFrom);
	}
	else if (!bInTrade)
	{  
		// 移动
		ItemMgr->ItemMoveReq(dwIctFrom,dwIctTo, nUIPosFrom,nUIPosTo,num);
	}
}

void UItemSystem::MailToBag(ui8 nUIPosFrom)
{
	MailSystemMgr->m_UMailSend->SetInclosureItem(nUIPosFrom, 0);
}
void UItemSystem::BagToMail(ui8 nUIPosFrom, ui8 nUIPosTo)
{
	MailSystemMgr->AddInclosureItem(nUIPosFrom, nUIPosTo);
}
void UItemSystem::BagToAuc(ui8 nUIPosFrom)
{
	//添加操作要求先判断是否已经有物品在列表中,
	//如果有的话,先释放,
	//然后添加客户端的背包锁定状态,判断相关的数据
	//AuctionMgr->AddOwerItem(nUIPosFrom);
	AUMgr->SetSellItemIn(nUIPosFrom);
}
void UItemSystem::AucToBag()
{
	//释放的操作要求解除背包的锁定状态。
	AUMgr->ReleaseItem();
}

void UItemSystem::BagToNpcTrade(ui8 UIBagPos, int num)
{
	UInGame* pUinGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	UPackage * pPackage = NULL;
	UNPCTrade * pNpcTrade = NULL;
	if (pUinGame)
	{
		pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
		pNpcTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);
	}
	CPlayerLocal* localPlayer = ObjectMgr->GetLocalPlayer();
	if (localPlayer && CPlayerLocal::GetCUState() == cus_NPCTrade)
	{
		ui64 pVendorguid = pNpcTrade->GetNpcId();
		ui32 pItemId = pPackage->GetItemIdByPos(UIBagPos);

		ItemMgr->ItemSellReq(pVendorguid,pItemId, num);
	}		
}
//包裹到交易，锁定包裹里的CItemSlot .同时拷贝一份数据到交易列表里。
void UItemSystem::PlayerTradeMoveMsg(ui8 UIBagPos, ui8 pTradePos, int num)
{
	UInGame* pUinGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	UPlayerTrade * pPlayerTrade = NULL;
	UPackage * pPackage = NULL;
	if (pUinGame)
	{
		pPlayerTrade = INGAMEGETFRAME(UPlayerTrade,FRAME_IG_PLAYERTRADEDLG);
		pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
		BOOL bInTrade = pPlayerTrade->IsVisible(); 

		if (bInTrade && !pPlayerTrade->GetIsLock())
		{
			CPlayerLocal* localPlayer = ObjectMgr->GetLocalPlayer();
			if (localPlayer && CPlayerLocal::GetCUState() == cus_Trade)
			{
				CStorage* pkPackageContainer  = localPlayer->GetItemContainer();
				CPlayerTradeContainer* pkTradeContainer = (CPlayerTradeContainer*)localPlayer->GetContainer(ICT_PLAYERTRADE);
				CItemSlot* pTradeSlot = (CItemSlot*)pkTradeContainer->GetSlot(pTradePos);
				CItemSlot* pBagSlot = (CItemSlot*)pkPackageContainer->GetSlot(UIBagPos);

				if (pBagSlot == NULL)
				{
					return ;
				}
				//如果包裹里的位置是锁定的，取消操作。返回
				if (pBagSlot->GetCode() == 0 && pBagSlot->IsLocked())
				{
					UMessageBox::MsgBox_s( _TRAN("该物品已经锁定！") );
					return;
				}
				SYItem * Item = (SYItem *)ObjectMgr->GetObject(pBagSlot->GetGUID());
				ItemPrototype_Client* pkItemInfo =  Item->GetItemProperty();
				if(!Item)
					return;
				if(!localPlayer->HasFlag(PLAYER_FLAGS, PLAYER_FLAG_GM))
				{
					if(Item->HasFlag( ITEM_FIELD_FLAGS, ITEM_FLAG_QUEST | ITEM_FLAG_SOULBOUND))
					{
						UMessageBox::MsgBox_s( _TRAN("该物品已经绑定！") );
						return ; 
					}

					if (pkItemInfo && pkItemInfo->Class == ITEM_CLASS_QUEST)
					{
						UMessageBox::MsgBox_s( _TRAN("任务道具！无法交易") );
						return ; 
					}
				}				

				//如果交易列表中该位置存在物品.释放对应的包裹里的物品的锁定. 
				if (pTradeSlot->GetCode() != 0)
				{	
					if (pTradeSlot->GetGUID() == pBagSlot->GetGUID())
					{
						return ;
					}
					PlayerTradeRelease(pTradePos);

				}
				//拷贝数据到交易列表,并锁定对应包裹里的物品.
				pBagSlot->SetLock(TRUE);

				CItemSlot CopySlot;
				CopySlot.Copy(*pBagSlot);
				
				if (pTradeSlot->GetCode() != 0)
				{
					pkTradeContainer->UpdateSlot(pTradePos, CopySlot);
				}else
				{
					pkTradeContainer->InsertSlot(pTradePos,CopySlot);
				}
				char pTest[255];
				NiSprintf(pTest,255,"bag to trade %u bag pos %u", pBagSlot->GetGUID(),UIBagPos);
				ULOG(pTest);


				ItemMgr->SendTradeSetItemReq(pTradePos, pkPackageContainer->GetServerBag(UIBagPos), pkPackageContainer->GetServerPos(UIBagPos));
				//显示对应的图标
				SYState()->UI->GetUItemSystem()->SetUIIcon(ICT_BAG,UIBagPos,pBagSlot->GetCode(),pBagSlot->GetNum(),ITEM_DATA_FLAG,true);
				SYState()->UI->GetUItemSystem()->SetUIIcon(ICT_PLAYER_TRADE,pTradePos,pTradeSlot->GetCode(),pTradeSlot->GetNum());
				
			}
		}
	}	

}

void UItemSystem::PlayerTradeRelease(ui8 pTradePos)
{
	CPlayerLocal* localPlayer = ObjectMgr->GetLocalPlayer();
	if (localPlayer && CPlayerLocal::GetCUState() == cus_Trade)
	{
		CStorage* pkPackageContainer  = localPlayer->GetItemContainer();
		CPlayerTradeContainer* pkTradeContainer = (CPlayerTradeContainer*)localPlayer->GetContainer(ICT_PLAYERTRADE);
		CItemSlot* pTradeSlot = (CItemSlot*)pkTradeContainer->GetSlot(pTradePos);
		
		if (pTradeSlot && pTradeSlot->GetCode() != 0)
		{
			CItemSlot* pkBagSlot = (CItemSlot*)pkPackageContainer->GetSlotByGuid(pTradeSlot->GetGUID());	
			assert(pkBagSlot);
			int BagPos = pkPackageContainer->GetUIPos(pTradeSlot->GetGUID());
			pkBagSlot->SetLock(FALSE);

			char pTest[255];
			NiSprintf(pTest,255,"trade to bag %d get bag pos %d", pTradeSlot->GetGUID(),BagPos);
			ULOG(pTest);

			ItemMgr->SendTradeClearItemReq(pTradePos);
			SYState()->UI->GetUItemSystem()->SetUIIcon(ICT_BAG,BagPos,pkBagSlot->GetCode(),pkBagSlot->GetNum(),ITEM_DATA_FLAG,false);
			SYState()->UI->GetUItemSystem()->SetUIIcon(ICT_PLAYER_TRADE,pTradePos,0,0);
			pTradeSlot->Clear();
		}	
	}
}

void UItemSystem::BagItemMsg(ui64 vecdorguid, ui32 dwItemId, ui8 byNum, ui32 amt)
{
	ItemMgr->ItemBuyReq(vecdorguid,dwItemId,byNum, amt);
}

void UItemSystem::SetItemMoney(int moneyAmount)
{
	UInGame* pInGame  = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	UNPCTrade * pNpcTrade = NULL;
	UPackage * pPackage = NULL;
	UBank* pBank = NULL;
	USkillTrainingDlg* pSkillTrainingDlg = NULL;
	if (pInGame)
	{
		pNpcTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);
		pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
		pBank = INGAMEGETFRAME(UBank, FRAME_IG_BANKDLG);
		pSkillTrainingDlg =INGAMEGETFRAME(USkillTrainingDlg, FRAME_IG_SPELLTRAININGDLG);
	}
	if (pInGame)
	{
		pPackage->ShowMoneyNum(moneyAmount);
		pBank->ShowMoneyNum(moneyAmount);
	 
		pNpcTrade->OnShowMoney(moneyAmount);
		pSkillTrainingDlg->ShowMoneyNum(moneyAmount);
	}
	if (AUMgr)
	{
		AUMgr->SetMoney(moneyAmount);
	}
}

void UItemSystem::SetYuanBao(ui32 uiYuanBao)
{
	UInGame* pInGame  = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	//UNPCTrade * pNpcTrade = NULL;
	UPackage * pPackage = NULL;
	ShopSystem* pShop = NULL;
	if (pInGame)
	{
		//pNpcTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);
		pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
		pShop = INGAMEGETFRAME(ShopSystem, FRAME_IG_SHOPDLG);
	}
	if (pInGame && pPackage)
	{
		pPackage->ShowYuanBao(uiYuanBao);

		pShop->ShowYuanBao(uiYuanBao);

		//pNpcTrade->OnShowMoney(moneyAmount);
	}
	if (AUMgr)
	{
		AUMgr->SetYuanbao(uiYuanBao);
	}
}

void UItemSystem::SetSkillAciton(ui8 pos, ui32 spellID, ItemType pType)
{
	if (pType == SPELL_DATA_FLAG)
	{
		SetUIIcon(ICT_INGAME_SKILL,pos,spellID,1,SPELL_DATA_FLAG);
	}else
	{
		SetUIIcon(ICT_INGAME_SKILL,pos,spellID,1,ITEM_DATA_FLAG);
	}
	PacketBuilder->SendSetActionButton(pos,spellID,pType);
}

void UItemSystem::SetItemToMap(ui8 pos)
{
}
//设置UI的Icon
void UItemSystem::SetUIIcon(ui8 dwIct, ui8 wPos, ui32 itemID,int num,ItemType type ,bool bLock)
{
	UInGame* pUI_P = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if(!pUI_P)
		return;
	if (dwIct >ICT_INVLALID && dwIct < ICT_INVALID_POS)
	{
		switch (dwIct)
		{
		case ICT_BAG: 
			{
				UPackage * pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
				if (pPackage)
				{
					pPackage->SetIconByPos(wPos,itemID,type, num,bLock);
				}
			}
			break;
		case ICT_EQUIP:
			{
				UEquipment * pEquipment = INGAMEGETFRAME(UEquipment,FRAME_IG_PLAYEREQUIPDLG);
				if (pEquipment)
				{
					pEquipment->SetIconByPos(wPos,itemID,type);
				}
			}
			break;
		case ICT_NPC_TRADE:
			break;
		case ICT_SKILL:
			break;
		case ICT_PLAYER_TRADE:
			{
				ItemExtraData temp;
				UPlayerTrade * pPlayerTrade = INGAMEGETFRAME(UPlayerTrade,FRAME_IG_PLAYERTRADEDLG);
				if (pPlayerTrade)
				{
					pPlayerTrade->SetIconByPos(wPos,itemID,type,num,temp,TRUE);
				}
			}
			break;
		case ICT_PLAYER_TRADE_TENNET:
			{
				ItemExtraData temp;
				UPlayerTrade * pPlayerTrade = INGAMEGETFRAME(UPlayerTrade,FRAME_IG_PLAYERTRADEDLG);
				if (pPlayerTrade)
				{
					pPlayerTrade->SetIconByPos(wPos,itemID,type,num,temp,FALSE);
				}
			}
			break;
		case ICT_INGAME_SKILL:
			{
				UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
				if (pGameBar)
				{
					pGameBar->SetIconByPos(wPos,itemID,type);
				}
			}
			break;
		case ICT_BANK:
			{
				UBank * pBank = INGAMEGETFRAME(UBank,FRAME_IG_BANKDLG);
				if (pBank)
				{
					pBank->SetIconByPos(wPos,itemID,type,num);
				}
			}
		default:
			break;
		}
	}

}
bool UItemSystem::IsUsedAppearItem()
{
	if (m_UseSpecialItem && m_UseSpecialItem->IsUsing() && m_UseSpecialItem->GetSP_ItemType() == UseSpecialItem::SP_ITEM_TYPE_APPEAR)
	{
		return true;
	}
	return false ;
}
bool UItemSystem::IsUsedBangdItem()
{
	if (m_UseSpecialItem && m_UseSpecialItem->IsUsing() && m_UseSpecialItem->GetSP_ItemType() == UseSpecialItem::SP_ITEM_TYPE_UNBIND)
	{
		return true;
	}
	return false ;
}
bool UItemSystem::IsUsedEnchantItem()
{
	if (m_UseSpecialItem && m_UseSpecialItem->IsUsing() && m_UseSpecialItem->GetSP_ItemType() == UseSpecialItem::SP_ITEM_TYPE_ENCHANT)
	{
		return true;
	}
	return false ;
}
void UItemSystem::CancelUseSpecialItem()
{
	if (m_UseSpecialItem)
	{
		m_UseSpecialItem->CancelUseItem();
	}
}


BOOL UItemSystem::DoUseSpecialItem(string name , ui64 guid)
{
	if (m_UseSpecialItem && m_UseSpecialItem->IsUsing())
	{
		if (guid)
		{
			return 	m_UseSpecialItem->EndUseItem(guid);
		}else
		{
			return m_UseSpecialItem->EndUseItem(name);
		}
	}

	return FALSE ;
}
#include "Skill/SkillManager.h"
//使用消耗品
void UItemSystem::SendUseLeechdomMsg(ui8 bag, ui8 slot, ui32 item_guid, SpellCastTargets& targets)
{
	//发送使用消息
	CPlayerLocal* pLplayer = ObjectMgr->GetLocalPlayer();

	if (!pLplayer)
	{
		return ;
	}
	CStorage* pkBag = pLplayer->GetItemContainer() ;

	if (!pkBag)
	{
		return ;
	}

	CSlot* pkSolt = pkBag->GetSlot(bag, slot);
	ui32 entry = 0;
	if (pkSolt)
	{	
		entry = pkSolt->GetCode();
		
	}
	if (!entry)
	{
		return ;
	}
	
	ItemPrototype_Client* pItemInfo = ItemMgr->GetItemPropertyFromDataBase( entry );

	targets.m_unitTarget = pLplayer->GetGUID();
	uint32 uiMask = TARGET_FLAG_SELF;
	if(pItemInfo && pItemInfo->Spells[0].Id)
	{
		SpellTemplate* pkST = SYState()->SkillMgr->GetSpellTemplate(pItemInfo->Spells[0].Id);
		if (pkST)
		{
			if(!InputTestSpellUseItem(pItemInfo->Spells[0].Id))
				return;

			uiMask = pkST->GetSelectMethod();
			if (((uiMask & TARGET_FLAG_SELF) && pLplayer->GetTargetObject() == pLplayer) || 
				(uiMask == TARGET_FLAG_SELF) ||
				(uiMask & TARGET_FLAG_SOURCE_LOCATION))
			{
				targets.m_unitTarget = pLplayer->GetGUID();

				if (uiMask & TARGET_FLAG_SOURCE_LOCATION)
				{
					targets.m_srcX = pLplayer->GetPosition().x;
					targets.m_srcY = pLplayer->GetPosition().y;
					targets.m_srcZ = pLplayer->GetPosition().z;
				}
			}else if (uiMask & TARGET_FLAG_DEST_LOCATION)
			{
				pkST->SelectTarget(pLplayer);
				CPlayerInputMgr* pkLocalInput = SYState()->LocalPlayerInput;
				float fMin = 0.0f, fMax = 0.0f;
				SpellTemplate::GetRange(pItemInfo->Spells[0].Id, fMin, fMax);
				if (pkLocalInput)
					pkLocalInput->SetSelectItemInfo(fMax, bag, slot);
				return;
			}else if (uiMask & TARGET_FLAG_ITEM)
			{
			}else if (uiMask & TARGET_FLAG_UNIT)
			{
				targets.m_unitTarget = pLplayer->GetTargetObjectID();

				if (!(pkST->GetSelectMethod() & TARGET_FLAG_SELF) && pLplayer->GetTargetObject() == pLplayer)
				{
					EffectMgr->AddEffectText( _TRAN(SpellTemplate::GetSpellErrorString(SPELL_FAILED_BAD_TARGETS)));
					return;
				}
			}
		}
	}
	targets.m_targetMask |= uiMask;
	if (!targets.m_unitTarget)
	{
		return;
	}
	
	if (pItemInfo &&  pLplayer && ( pLplayer->CheckInWater() == WL_UNDER_WATER ) )
	{
		for ( int i = 0; i < 5; ++i )
		{
			uint32 SpellId = pItemInfo->Spells[i].Id;
			if ( g_pkSpellInfo->IsMountSpell(SpellId) || g_pkSpellInfo->IsShapeSpell(SpellId))
			{
				EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("游泳状态不能乘骑或变身!") );
				return;
			}
		}
	}

	CItemSlot* pkItemSlot = (CItemSlot*)pkSolt;
	if (m_UseSpecialItem->IsUsing())
	{
		//提示
		EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), _TRAN("正在使用其他道具!"));
		ChatSystem->AppendChatMsg(CHAT_MSG_SYSTEM, _TRAN("正在使用其他道具!<br>"));
		return ;
		//m_UseSpecialItem->EndUseItem(pkItemSlot->GetGUID());
	}
	else if ( m_UseSpecialItem->BeginUseItem(pkItemSlot->GetGUID(), bag, slot ) )
	{
		
	}
	else
	{
		//ItemMgr->SetItemUsed();
		
		if ( pLplayer->isMount() )
		{
			uint32 MountSpell = 0;
			pLplayer->GetMountSpell( MountSpell );
			for ( int i = 0; i < 5; ++i )
			{
				uint32 SpellId = pItemInfo->Spells[i].Id;

				if ( MountSpell == SpellId )
				{
					PacketBuilder->SendCancelAura(MountSpell);
					return;
				}
			}
		}
		
		if ( pLplayer->isInShapShift() )
		{
			uint32 ShapSpell = 0;
			pLplayer->GetShapeSpell( ShapSpell );
			for ( int i = 0; i < 5; ++i )
			{
				uint32 SpellId = pItemInfo->Spells[i].Id;

				if ( ShapSpell == SpellId )
				{
					PacketBuilder->SendCancelAura(ShapSpell);
					return;
				}
			}
		}
		
		ItemMgr->ItemUseReq(bag, slot, item_guid, targets);
	}

}

void UItemSystem::SendUseSellMsg(ui64 itemguid, ui8 byNum)
{
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	UNPCTrade * pNpcTrade = NULL;
	if (pInGame)
	{
		pNpcTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);
	}
	if (pNpcTrade->IsVisible())
	{
		ui64 vecdorguid = pNpcTrade->GetNpcId(); 
		ItemMgr->ItemSellReq(vecdorguid, itemguid, byNum);
	}
}

void UItemSystem::ToDestroyItem()
{
	SYState()->IAudio->PlayUiSound(UI_destroyitem);
	SYState()->UI->GetUItemSystem()->DropItemToMap();
}
void UItemSystem::ReSetDestroyPos()
{
	SYState()->UI->GetUItemSystem()->SetDropItemPos(-1);
}
void UItemSystem::DropItemToMap()
{
	if (m_DropPos >= 0)
	{
		ItemMgr->DropBackPackItemReq(m_DropPos);
		m_DropPos = -1;
	}
}

void UItemSystem::SendUseEquipmentMsg(ui8 dwIctFrom, ui8 dwIctTo, ui8 wPosFrom, ui8 wPosTo, ui8 wNum)
{

}

void UItemSystem::Clear()
{
	m_UseSpecialItem->CancelUseItem();
}


void UItemSystem::PlayerAgreeTrade()
{
	PacketBuilder->SendTradeBeginMsg();
	//UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	//pInGame->GetUPlayerTrade()->SetVisible(TRUE);
}
void UItemSystem::PlayerRefuseTrade()
{
	PacketBuilder->SendTradeBusyMsg();
}

void UItemSystem::DoRepairAll()
{
	UNPCTrade* pkNPCTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);
	if (pkNPCTrade && CPlayerLocal::GetCUState() == cus_NPCTrade)
	{
		// 0为全部修理
		PacketBuilder->SendRepairItem(pkNPCTrade->GetNpcId(), 0);
	}
}

//装备修理相关
void UItemSystem::RepairAll(ui64 NPCGuid)
{
	//修理全部
	//计算修理全部需要的金钱 /*售价 * (总耐久 - 当前耐久)/ 总耐久*/
	//发送修理全部的消息

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return ;
	}

	CStorage* BagStorage = pkLocal->GetItemContainer();
	CStorage* BankStorage = pkLocal->GetBankContainer();
	CEquipmentContainer* EquipCon = (CEquipmentContainer*)pkLocal->GetContainer(ICT_EQUIP);

	ui32 Money = 0.f ; 
	
	for (UINT ui = 0; ui < BagStorage->GetNumMaxSlotCount() ; ui++)
	{
		CItemSlot* pkSlot = (CItemSlot*)BagStorage->GetSlot(ui);
		if (pkSlot && pkSlot->GetGUID())
		{
			SYItem* pkItem = (SYItem*)ObjectMgr->GetObject(pkSlot->GetGUID());
			if (pkItem && pkItem->GetItemProperty() && (pkItem->GetItemProperty()->Class == ITEM_CLASS_WEAPON || pkItem->GetItemProperty()->Class == ITEM_CLASS_ARMOR)  && pkItem->GetDurability() < pkItem->GetMaxDurability())
			{
				float Price = ItemPrice(pkItem->GetItemProperty(), true);
				ui32 Temp = Price * ((float)pkItem->GetMaxDurability() - (float)pkItem->GetDurability()) / (float)pkItem->GetMaxDurability() ;
				Temp = Temp ? Temp : 1;
				Money += Temp ;
			}else
			{
				continue ;
			}
		}
	}

	for (int uib = 0 ; uib < BankStorage->GetNumMaxSlotCount(); uib++)
	{
		CItemSlot* pkSlot = (CItemSlot*)BankStorage->GetSlot(uib);
		if (pkSlot && pkSlot->GetGUID())
		{
			SYItem* pkItem = (SYItem*)ObjectMgr->GetObject(pkSlot->GetGUID());
			if (pkItem && pkItem->GetItemProperty() && (pkItem->GetItemProperty()->Class == ITEM_CLASS_WEAPON || pkItem->GetItemProperty()->Class == ITEM_CLASS_ARMOR)  && pkItem->GetDurability() < pkItem->GetMaxDurability())
			{
				float Price = ItemPrice(pkItem->GetItemProperty(), true);
				ui32 Temp = Price * ((float)pkItem->GetMaxDurability() - (float)pkItem->GetDurability()) / (float)pkItem->GetMaxDurability() ;
				Temp = Temp ? Temp : 1;
				Money += Temp ;
			}else
			{
				continue ;
			}
		}
	}

	for (int i = 0 ; i < EquipCon->GetMaxSlotNum(); i++)
	{
		CEquipSlot* pkSlot = (CEquipSlot*)EquipCon->GetSlot(i);
		if (pkSlot && pkSlot->GetGUID())
		{
			SYItem* pkItem = (SYItem*)ObjectMgr->GetObject(pkSlot->GetGUID());
			if (pkItem && (pkItem->GetItemProperty()->Class == ITEM_CLASS_WEAPON || pkItem->GetItemProperty()->Class == ITEM_CLASS_ARMOR) && pkItem->GetDurability() < pkItem->GetMaxDurability())
			{
				float Price = ItemPrice(pkItem->GetItemProperty(), true);
				ui32 Temp = Price * ((float)pkItem->GetMaxDurability() - (float)pkItem->GetDurability()) / (float)pkItem->GetMaxDurability() ;
				
				Temp = Temp ? Temp : 1;
				Money += Temp ;
			}else
			{
				continue ;
			}
		}
	}

	if (Money > pkLocal->GetMoney())
	{
		UMessageBox::MsgBox_s( _TRAN("您的钱支付不了修理费用！") );
		return ;
	}
	
	if (Money > 0.f)
	{
		UINT UintMoney = Money ;
		UINT G_m = UintMoney / 10000 ; 
		UINT S_m = UintMoney % 10000 / 100;
		UINT C_m = UintMoney % 10000 % 100;

		//char buf[_MAX_PATH];
		std::string buf;
		if (G_m > 0)
		{
			buf = _TRAN( N_NOTICE_C5 , _I2A(G_m).c_str(),_I2A(S_m).c_str(),_I2A(C_m).c_str() );
			//NiSprintf(buf,_MAX_PATH, "修理需要%d金%d银%d铜", G_m, S_m, C_m);
		}else if (S_m > 0)
		{
			if (C_m)
			{
				buf = _TRAN( N_NOTICE_C6 ,_I2A(S_m).c_str(),_I2A(C_m).c_str() );
				//NiSprintf(buf,_MAX_PATH, "修理需要%d银%d铜", S_m, C_m);
			}else
			{
				buf = _TRAN( N_NOTICE_C7 , _I2A(S_m).c_str() );
				//NiSprintf(buf,_MAX_PATH, "修理需要%d银", S_m);
			}
			
		}else
		{
			buf = _TRAN( N_NOTICE_C8 , _I2A(C_m).c_str() );
			//NiSprintf(buf,_MAX_PATH, "修理需要%d铜", C_m);
		}
		
		UMessageBox::MsgBox(buf.c_str(), (BoxFunction*)UItemSystem::DoRepairAll, NULL);
	}
}
void UItemSystem::RepairOne(ui64 NPCGuid ,ui64 guid)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return ;
	}
	if (!guid )
	{
		return ;
	}

	UNPCTrade* pkNPCTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);
	if (pkNPCTrade && pkNPCTrade->IsRepair())
	{
		// 判断是否需要修理
		SYItem* pItem = (SYItem*)ObjectMgr->GetObject(guid);
		if (pItem && (pItem->GetItemProperty()->Class == ITEM_CLASS_WEAPON || pItem->GetItemProperty()->Class == ITEM_CLASS_ARMOR)  && pItem->GetMaxDurability() > pItem->GetDurability())
		{
			float NeedMoney = ItemPrice(pItem->GetItemProperty(), true) * ((float)pItem->GetMaxDurability() - (float)pItem->GetDurability()) / (float)pItem->GetMaxDurability();
			if (NeedMoney > pkLocal->GetMoney())
			{
				UMessageBox::MsgBox_s( _TRAN("您的钱无法支付修理费用！"),NULL, 10.f);
			}else
			{
				// 发送修理消息
				PacketBuilder->SendRepairItem(NPCGuid,guid);
			}
			
		}
		pkNPCTrade->SetRepair( FALSE);
		// 重置UI修理状态
	}
}
void UItemSystem::Update(float dt)
{
	if (m_isBeginNeatenTime[0])
	{
		m_NeatenTime[0] += dt ;
		if (m_NeatenTime[0] >= NEATENTIME)
		{
			UPackage * pkPag = INGAMEGETFRAME(UPackage, FRAME_IG_PACKAGE);
			if (pkPag)
			{
				pkPag->GetChildByID(13)->SetActive(TRUE);
			}
			m_isBeginNeatenTime[0] = false ;
			m_NeatenTime[0] = 0.0f;
		}
	}

	if (m_isBeginNeatenTime[1])
	{
		m_NeatenTime[1] += dt;
		if (m_NeatenTime[1] >= NEATENTIME)
		{
			UBank * pkBank = INGAMEGETFRAME(UBank, FRAME_IG_BANKDLG);
			if (pkBank)
			{
				pkBank->GetChildByID(12)->SetActive(TRUE);
			}
			m_isBeginNeatenTime[1] = false ;
			m_NeatenTime[1] = 0.0f;
		}
	}
}

void UItemSystem::SetNeateningBag(bool Neaten, bool isBank)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();

	if (pkLocal)
	{
		if (isBank)
		{
			if (m_isNeaten[1] == Neaten)
			{
				return ;
			}
			m_isNeaten[1] = Neaten ;
			if (m_isNeaten[1])
			{
				m_isBeginNeatenTime[1] = true ;
				int len = pkLocal->GetBankContainer()->SortBag();
				UBank * pkBank = INGAMEGETFRAME(UBank, FRAME_IG_BANKDLG);
				if (pkBank)
				{
					pkBank->GetChildByID(12)->SetActive(FALSE);
				}
			}
		}else
		{	
			if (m_isNeaten[0] == Neaten)
			{
				return ;
			}
			m_isNeaten[0] = Neaten ;
			
			if (m_isNeaten[0])
			{
				m_isBeginNeatenTime[0] = true ;
				int len = pkLocal->GetItemContainer()->SortBag();

				UPackage * pkPag = INGAMEGETFRAME(UPackage, FRAME_IG_PACKAGE);			
				if (pkPag)
				{
					pkPag->GetChildByID(13)->SetActive(FALSE);
				}
			}
		}
	}
	
}


UseSpecialItem::UseSpecialItem()
{
	m_ItemGuid = 0 ;
	m_Itemtype = SP_ITEM_TYPE_NONE ;

	m_bag = 0;
	m_slot = 0;

}
bool UseSpecialItem::BeginUseItem(ui64 itemguid, ui8 bag, ui8 slot )
{
	if (m_Itemtype != SP_ITEM_TYPE_NONE)
	{
		//is using
		return false ;
	}

	SYItem * Item = (SYItem *)ObjectMgr->GetObject(itemguid);
	
	if (Item )
	{
		ItemPrototype_Client* pkItemInfo  = Item->GetItemProperty();
		if ((pkItemInfo->Class == ITEM_CLASS_CONSUMABLE))
		{
			if (pkItemInfo->SubClass == ITEM_SUBCLASS_CONSUMABLE_UNBIND)
			{
				m_ItemGuid = itemguid ;
				m_bag = bag;
				m_slot = slot;
				m_Itemtype = SP_ITEM_TYPE_UNBIND ;
				SYState()->ClientApp->SetCursor(SYC_UNBANGDING);
				return true ;
			}

			if (pkItemInfo->SubClass == ITEM_SUBCLASS_CONSUMABLE_ENCHANT)
			{
				m_ItemGuid = itemguid ;
				m_bag = bag;
				m_slot = slot;
				m_Itemtype = SP_ITEM_TYPE_ENCHANT ;
				SYState()->ClientApp->SetCursor(SYC_ENCHANT);
				return true ;
			}


			if ( pkItemInfo->SubClass == ITEM_SUBCLASS_CONSUMABLE_APPEAR)
			{
				m_ItemGuid = itemguid ;
				m_bag = bag;
				m_slot = slot;
				m_Itemtype = SP_ITEM_TYPE_APPEAR ;
				UIAddMemberEdit* pEdit = INGAMEGETFRAME(UIAddMemberEdit, FRAME_IG_ADDMEMBERDLG);
				if (pEdit)
				{
					pEdit->OnAddMember(4);
					return true ;
				}
				return false ;
			}
		}
	}

	return false ;
}
bool UseSpecialItem::IsUsing()
{
	return (m_ItemGuid != 0) ;
}
int UseSpecialItem::GetSP_ItemType()
{
	return m_Itemtype ;
}
void UseSpecialItem::CancelUseItem()
{
	if (m_ItemGuid)
	{
		m_ItemGuid = 0 ;
		if (m_Itemtype == SP_ITEM_TYPE_UNBIND)
		{
			SYState()->ClientApp->SetCursor(SYC_ARROW);
		}

		if ( m_Itemtype == SP_ITEM_TYPE_ENCHANT )
		{
			SYState()->ClientApp->SetCursor(SYC_ARROW);
		}
		m_Itemtype = SP_ITEM_TYPE_NONE;
	}
}
BOOL UseSpecialItem::EndUseItem(ui64 guid)
{
	if (m_Itemtype == SP_ITEM_TYPE_UNBIND )
	{
	
		if (guid == m_ItemGuid)
		{
			//取消
		}else
		{
			PacketBuilder->SendUnBindReqMsg(m_ItemGuid, guid);
		}
		CancelUseItem() ;

		return TRUE;
	}

	if (m_Itemtype == SP_ITEM_TYPE_ENCHANT )
	{

		if (guid == m_ItemGuid)
		{
			//取消
		}else
		{
			SpellCastTargets spllTarget;
 			spllTarget.m_targetMask = TARGET_FLAG_ITEM;
			spllTarget.m_itemTarget = guid;

			SYItem* pkItem = (SYItem*)ObjectMgr->GetObject(m_ItemGuid);
			if (pkItem)
			{
				PacketBuilder->SendItemUseReqMsg( m_bag, m_slot, m_ItemGuid, spllTarget);
			}
		}
		CancelUseItem() ;

		return TRUE;
	}

	return FALSE ;
	
}
BOOL UseSpecialItem::EndUseItem(string name )
{
	if (m_Itemtype == SP_ITEM_TYPE_APPEAR)
	{
		PacketBuilder->SendAppearReq(name.c_str(),m_ItemGuid);
		CancelUseItem() ;
		return TRUE ;
	}
	return FALSE ;
	
}