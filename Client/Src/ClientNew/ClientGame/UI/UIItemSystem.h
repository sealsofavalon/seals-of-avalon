#pragma once
#include "..\Utils\ItemDB.h"
#include "UInc.h"

class UseSpecialItem
{
public:

	UseSpecialItem();
	enum
	{
		SP_ITEM_TYPE_NONE = 0,
		SP_ITEM_TYPE_UNBIND , //解绑道具
		SP_ITEM_TYPE_APPEAR, //床送道具
		SP_ITEM_TYPE_ENCHANT
	};
protected:
	
public:
	bool BeginUseItem(ui64 itemguid, ui8 bag, ui8 slot );
	bool IsUsing();
	void CancelUseItem();
	BOOL EndUseItem(ui64 guid);
	BOOL EndUseItem(string name );
	int GetSP_ItemType();
protected:
	ui64 m_ItemGuid ;
	ui8  m_bag;
	ui8  m_slot;
	UINT m_Itemtype ;
};

class UItemSystem
{
	//UI物品移动处理相关	
public:
	 UItemSystem();
	 virtual ~UItemSystem();
	enum ItemType
	{
		SPELL_DATA_FLAG = ActionButton_Type_Spell,
		ITEM_DATA_FLAG = ActionButton_Type_Item,
	};

	enum UI_TYPE
	{
		ICT_INVLALID = 0,
		ICT_BAG,                   //包裹
		ICT_EQUIP,                 //装备
		ICT_DEPOT,
		ICT_NPC_TRADE,             //NPC交易
		ICT_SKILL,                 //技能
		ICT_INGAME_SKILL,          //InGame界面
		ICT_PLAYER_TRADE,          //交易界面本地玩家.
		ICT_PLAYER_TRADE_TENNET,   //交易界面远程玩家.
		ICT_BAG_SOLT,              //显示使用包裹道具的位置
		ICT_MAP,
		ICT_AUC,
		ICT_BANK,              // 银行
		ICT_BANK_SOLT,		   // 银行包
		ICT_REFINE,            // 玉能神器
		ICT_MAIL,			   // 邮件

		ICT_HOOK_SKILL,  //挂机技能
		ICT_HOOK_ITEM ,  //挂机物品

		ICT_TEAM_FLAG,  //团队拖动标示
		ICT_INVALID_POS = 0xFFFF,
	};

	//根据服务器消息设置UI
	void SetItemToMap(ui8 pos);                                                       //丢弃物品进地图
	void SetItemMoney(int moneyAmount);                     //显示金钱
	void SetYuanBao(ui32 uiYuanBao);
	void SetUIIcon(ui8 dwIct, ui8 wPos, ui32 itemID,int num, ItemType type = ITEM_DATA_FLAG ,bool bLock = false);  //设置物品到对应界面

	// 玩家交易
	static void PlayerAgreeTrade();     //同意交易
	static void PlayerRefuseTrade();    //拒绝交易
	//丢弃道具相关
	static void ToDestroyItem();
	static void ReSetDestroyPos();
	void DropItemToMap();//丢弃道具
	void SetDropItemPos(INT pos){m_DropPos = pos;}
protected:
	INT m_DropPos;  //丢弃道具的pos.  
public:
	void BagItemMsg(ui64 vecdorguid, ui32 dwItemId, ui8 byNum, ui32 amt);//从NPC购买
    
	//从技能界面拖动到快捷
	void SetSkillAciton(ui8 pos, ui32 spellID, ItemType pType);
	void MoveItemMsg(ui8 dwIctFrom, ui8 dwIctTo, ui8 nUIPosFrom, int nUIPosTo, int num ); //客户端所有的移动操作
	void BagToNpcTrade(ui8 UIBagPos, int num);
	void BagToAuc(ui8 nUIPosFrom);
	void AucToBag();

	void MailToBag(ui8 nUIPosFrom);
	void BagToMail(ui8 nUIPosFrom, ui8 nUIPosTo);
	
	void PlayerTradeMoveMsg(ui8 UIBagPos, ui8 pTradePos, int num);
	void PlayerTradeRelease(ui8 pTradePos);

	BOOL DoUseSpecialItem(string name , ui64 guid);
	void CancelUseSpecialItem();
	bool IsUsedBangdItem();
	bool IsUsedEnchantItem();
	bool IsUsedAppearItem();

	void SendUseLeechdomMsg(ui8 bag, ui8 slot, ui32 item_guid, SpellCastTargets& targets);	// 使用道具
	void SendUseSellMsg(ui64 itemguid, ui8 byNum);//卖出道具
	void SendUseEquipmentMsg(ui8 dwIctFrom, ui8 dwIctTo, ui8 wPosFrom, ui8 wPosTo, ui8 wNum);// 使用装备
	void Clear();       //清空所有资源

	//装备修理相关
	static void DoRepairAll();
	void RepairAll(ui64 NPCGuid);  
	void RepairOne(ui64 NPCGuid ,ui64 guid);

	void SetNeateningBag(bool Neaten, bool isBank);


	void Update(float dt);
private:
	bool m_isNeaten[2] ;
	bool m_isBeginNeatenTime[2];
	float m_NeatenTime[2];
	UseSpecialItem* m_UseSpecialItem ;   //使用需要特殊操作的道具
};


