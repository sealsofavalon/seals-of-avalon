#include "StdAfx.h"
#include "SceneManager.h"
#include "UiSystem.h"
#include "UILogin.h"
#include "Player.h"
#include "Network/NetworkManager.h"
#include "Network/PacketBuilder.h"
#include "ClientState.h"
#include "ObjectManager.h"
#include "AudioInterface.h"
#include "ClientApp.h"
#include "UMessageBox.h"
#include "SystemSetup.h"
#include "UIWaitList.h"

#define MaxNameLen 20
#define MaxPwdLen  20

UIMP_CLASS(ULoginFrame, UControl);
void ULoginFrame::StaticInit()
{
	
}
//事件响应
UBEGIN_MESSAGE_MAP(ULoginFrame,UCmdTarget)
UON_BN_CLICKED(4, &ULoginFrame::OnLoginServer)
UON_BN_CLICKED(5, &ULoginFrame::OnExitGame)
UON_BN_CLICKED(6, &ULoginFrame::OnService)
UON_BN_CLICKED(12, &ULoginFrame::OnKeepUserName)
UON_UEDIT_UPDATE(2, &ULoginFrame::OnLoginServer)
UON_UEDIT_UPDATE(3, &ULoginFrame::OnLoginServer)
UON_BN_CLICKED(13, &ULoginFrame::OnShowSoftKey)
UEND_MESSAGE_MAP()

enum KeepUserData
{
	CB_CHECK_NORMAL = 0,
	CB_UNCHECK_NORMAL
};

ULoginFrame::ULoginFrame()
{
	m_UserNameCtrl = NULL;
	m_UserPassCtrl = NULL;
	m_LoginBtn = NULL;
	m_SoftKeyBtn = NULL;
	m_QuitBtn = NULL;
	m_Logo = NULL;

	m_bKeepUserName = FALSE;

	m_ServiceP = NULL;

}
ULoginFrame::~ULoginFrame()
{
}

BOOL ULoginFrame::OnCreate(void)
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	
	UControl* pDlgCtrl = GetChildByID(1);
	if (pDlgCtrl == NULL)
	{
		return FALSE;
	}
    //UserName
	m_UserNameCtrl = (UEdit*)pDlgCtrl->GetChildByID(2);
	if (m_UserNameCtrl == NULL)
	{
		return FALSE;
	}

	m_UserNameCtrl->SetMaxLength(MaxNameLen);

	//m_UserNameCtrl->SetFocusControl();
	//Password
	m_UserPassCtrl =(UEdit*)pDlgCtrl->GetChildByID(3);
	if (m_UserPassCtrl == NULL)
	{
		return FALSE;
	}

	m_UserPassCtrl->SetMaxLength(MaxPwdLen);

    //Login button
	m_LoginBtn = (UBitmapButton*)pDlgCtrl->GetChildByID(4);
	if (m_LoginBtn == NULL)
	{
		return FALSE;
	}
	if (m_SoftKeyBtn == NULL)
	{
		m_SoftKeyBtn = UDynamicCast(UBitmapButton,pDlgCtrl->GetChildByID(13));

		if (!m_SoftKeyBtn)
		{
			return FALSE ;
		}

		m_SoftKeyBtn->SetCheckState( FALSE);
	}
	m_LoginBtn->SetActive(TRUE);

	m_QuitBtn = (UBitmapButton*)GetChildByID(5);
	if (m_QuitBtn == NULL)
	{
		return FALSE;
	}

	//m_QuitBtn->SetGlint(TRUE);
	m_Logo = UDynamicCast(UBitmap, GetChildByID(1));
	if (m_Logo == NULL)
	{
		return FALSE;
	}
	m_Logo->SetAlphaCmd(TRUE);

	m_LogoName = UDynamicCast(UBitmap, GetChildByID(100));
	if (m_LogoName)
	{
		m_LogoName->SetAlphaCmd(TRUE);
	}
	
	if (m_ServiceP == NULL)
	{
		m_ServiceP = (UServiceProtocol*)sm_System->CreateDialogFromFile("Service\\Service.udg");

		if (m_ServiceP != NULL)
		{
			AddChild(m_ServiceP);
			m_ServiceP->SetVisible(FALSE);
		}else
		{	
			return FALSE;
		}
		
	}

	UBitmapButton* pkKeepBtn =UDynamicCast(UBitmapButton, pDlgCtrl->GetChildByID(12));
	if (!pkKeepBtn)
	{
		return FALSE ;
	}
	if (m_UserNameCtrl)
	{
		m_bKeepUserName =  ReadUserData();

		if (m_UserNameCtrl->GetString().Empty())
		{
			SetFocusControl(m_UserNameCtrl);
		}
		else
		{
			SetFocusControl(m_UserPassCtrl);
		}
	}	

	pkKeepBtn->SetCheckState(m_bKeepUserName);

	if (SystemSetup && !SystemSetup->InitSystem())
	{
		return FALSE;
	}
	return TRUE;
}
void ULoginFrame::OnDestroy(void)
{
	UControl::OnDestroy();
}
void ULoginFrame::OnGetFocus(void)
{
	if (m_UserNameCtrl)
	{
		if (m_UserNameCtrl->GetString().Empty())
		{
			SetFocusControl(m_UserNameCtrl);
		}
		else
		{
			SetFocusControl(m_UserPassCtrl);
		}
	}
}

void ULoginFrame::ReLogin()
{
	CMap * pmap = CMap::Get();
	if (pmap)
	{
		ObjectMgr->RemoveAll();
		pmap->Unload();
		pmap->Reset();
	}
	
	UWaitList::BeginEnter();


	CUISystem::ShowEX(DID_LOGIN, TRUE, TRUE);
}

void ULoginFrame::SetKeepUse()
{
	if (m_bKeepUserName)
	{
		SetKeepUserData();
	}
}
void ULoginFrame::ActiveLoginBtn()
{
	if (m_LoginBtn)
	{
		m_LoginBtn->SetActive(TRUE);
	}
}
void ULoginFrame::OnShowSoftKey()
{
	if (m_SoftKeyBtn)
	{
		BOOL isCheck = m_SoftKeyBtn->IsChecked();

		m_UserPassCtrl->SoftKeyBoardShow(isCheck);
		if (isCheck)
		{
			SetFocusControl(m_UserPassCtrl);
		}
	}
}
void ULoginFrame::OnLoginServer()
{
    CHAR Buf[64];
    if(m_UserNameCtrl->GetText(Buf, 64) <= 0)
    {
        UMessageBox::MsgBox_s( _TRAN("please enter account name!") );
        return;
    }else
    {
        ClientState->SetUserName(Buf);
    }

    if(m_UserPassCtrl->GetText(Buf, 64) <= 0)
    {
        UMessageBox::MsgBox_s( _TRAN("Please enter your password!") );
        return;
    }else
    {
        ClientState->SetUserPass(Buf);

        //清除内存当中的密码
        for(unsigned int i = 0; Buf[i]; ++i)
            Buf[i] = 'x';

        m_UserPassCtrl->SetText(Buf);
    }

    m_UserPassCtrl->Clear();

	//连接登录服务器
	NetworkMgr->ConnectLogin(ClientState->GetLoginIP(), ClientState->GetLoginPort());
    UMessageBox::MsgBox_s( _TRAN("Connecting the login server...") );
    //UMessageBox::HideOKButton();
    UMessageBox::DelayShow(2.0); //先隐藏， 2秒之后再显示
    //UMessageBox::DisableEscape();
}

void ULoginFrame::OnExitGame()
{
	PostQuitMessage(0);	
}
void ULoginFrame::OnService()
{
	m_ServiceP->SetVisible(TRUE);
}

bool ULoginFrame::DeleteUserData()
{
	remove("accounts.sav");
	return true ;
}

void ULoginFrame::SetKeepUserData()
{
	char accounts[_MAX_PATH];
	//char password[_MAX_PATH];

	FILE* pUserData;

	int alen = m_UserNameCtrl->GetText(accounts,_MAX_PATH);
	//int plen = m_UserPassCtrl->GetText(password,_MAX_PATH);


	if (alen > 0 /*&& plen > 0*/)
	{
		pUserData= fopen("accounts.sav","wb+");

		//vista  win7 UAC权限设置 导致文件创建可能失败。

		if(pUserData)
		{
			int len = strlen(accounts);
			fwrite(&len, 1, sizeof(int), pUserData);
			fwrite(accounts, 1, len, pUserData);

			/*len = strlen(password);
			fwrite(&len, 1, sizeof(int), pUserData);
			fwrite(password, 1, len, pUserData);*/

			fclose(pUserData);
		}
		
	}
}

void ULoginFrame::OnKeepUserName()
{
	m_bKeepUserName = !m_bKeepUserName;

	if (!m_bKeepUserName)
	{
		DeleteUserData();
	}
}

bool ULoginFrame::ReadUserData()
{
	FILE* fp = fopen("accounts.sav","rb");
	if (fp)
	{
		int len;
		char UserName[256];
		//char Password[256];
		fread(&len, 1, sizeof(int), fp);
		fread(UserName, 1, len, fp);
		UserName[len] = '\0';

	/*	fread(&len, 1, sizeof(int), fp);
		fread(Password, 1, len, fp);
		Password[len] = '\0';*/
		fclose(fp);

		if (strlen(UserName) != 0 /*&& strlen(Password) != 0*/)
		{
			if (m_UserNameCtrl /*&& m_UserPassCtrl*/)
			{
				m_UserNameCtrl->SetText(UserName);
				//m_UserPassCtrl->SetText(Password);
				return true;
			}
		}
	}
	return false;
}
BOOL ULoginFrame::OnEscape()
{
	if (!UControl::OnEscape())
	{
		OnExitGame();
		return TRUE;
	}
	return FALSE;
}