#pragma once
#include "UInc.h"
#include "UIServiceProtocol.h"
#include "ULoginLoading.h"


class ULoginFrame : public UControl
{
	UDEC_CLASS(ULoginFrame);
	UDEC_MESSAGEMAP();
public :
	ULoginFrame(void);
	~ULoginFrame(void);
	static void ReLogin();
	void ActiveLoginBtn();


	void SetKeepUse();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnGetFocus();
	virtual BOOL OnEscape();
private:
	void OnLoginServer();
    void OnExitGame();
	void OnShowSoftKey();
	
	void OnService();
	void SetKeepUserData();
	bool ReadUserData();
	bool DeleteUserData();

	void OnKeepUserName();
	
	
	UBitmap* m_Logo;
	UBitmap* m_LogoName;
	UEdit* m_UserNameCtrl;
	UEdit* m_UserPassCtrl;
    UBitmapButton* m_LoginBtn;
	UBitmapButton* m_QuitBtn;
	UBitmapButton* m_SoftKeyBtn;

	UServiceProtocol* m_ServiceP; 

	BOOL m_bKeepUserName ;
};

