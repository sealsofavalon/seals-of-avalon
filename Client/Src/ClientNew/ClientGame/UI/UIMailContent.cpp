#include "stdafx.h"
#include "UIMailContent.h"
#include "UIMailReceiveList.h"
#include "UISystem.h"
#include "UIGamePlay.h"
#include "USkillButton.h"
#include "Network/PacketBuilder.h"
#include "Network/PacketParser.h"
#include "ObjectManager.h"
#include "UIMailSend.h"
#include "UMailSystem.h"
#include "UIMoneyText.h"
#include "UFun.h"
#include "UITipSystem.h"
#include "../../../../SDBase/Public/MailDef.h"

UIMP_CLASS(UMailContent,UDialog);
UBEGIN_MESSAGE_MAP(UMailContent,UDialog)
UON_BN_CLICKED(0, &UMailContent::OnClickShield)
UON_BN_CLICKED(1, &UMailContent::OnClickReSend)
UON_BN_CLICKED(2, &UMailContent::OnClickRevert)

UON_RED_URL_CLICKED(6,&UMailContent::OnURLClicked)
//附件
UON_BN_CLICKED(7, &UMailContent::OnClickInclosure_0)
UON_BN_CLICKED(8, &UMailContent::OnClickInclosure_1)
UON_BN_CLICKED(9, &UMailContent::OnClickInclosure_2)
UON_BN_CLICKED(10,&UMailContent::OnClickInclosure_3)
UON_BN_CLICKED(11,&UMailContent::OnClickInclosure_4)
UON_BN_CLICKED(12,&UMailContent::OnClickInclosure_5)

UON_BN_CLICKED(15,&UMailContent::OnClickGetMoney)
UON_BN_CLICKED(17,&UMailContent::OnClickDelete)
UEND_MESSAGE_MAP();

void UMailContent::StaticInit()
{

}
UMailContent::UMailContent()
{
	m_Caption = NULL;
	m_Sender = NULL;
	m_Content = NULL;
	m_MailID = NULL;
	m_pMoneytext = NULL;

	m_bCanGet = TRUE; 

	m_Cod = 0;
	m_IsUpdateChild = FALSE;

}
UMailContent::~UMailContent()
{

}

BOOL UMailContent::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	UScrollBar* pScrollBar = UDynamicCast(UScrollBar ,GetChildByID(5));
	if (pScrollBar == NULL)
		return FALSE;
	pScrollBar->SetBarShowMode(USBSM_FORCEON,USBSM_FORCEOFF);

	if (m_Caption == NULL)
	{
		m_Caption = UDynamicCast(UStaticText, GetChildByID(3));
		if (!m_Caption)
			return FALSE;
	}
	m_Caption->SetTextAlign(UT_LEFT);
	m_Caption->SetTextColor("000000");
	if (m_Sender == NULL)
	{
		m_Sender = UDynamicCast(UStaticText, GetChildByID(4));
		if (!m_Sender)
			return FALSE;
	}	
	m_Sender->SetTextAlign(UT_LEFT);
	m_Sender->SetTextColor("000000");
	if (m_Content == NULL)
	{
		m_Content = UDynamicCast(URichTextCtrl, pScrollBar->GetChildByID(6));
		if (!m_Content)
		{
			return FALSE;
		}
	}
	UStaticText* Title = UDynamicCast(UStaticText, GetChildByID(13));
	if (Title)
		Title->SetTextColor("00FF00");
	UStaticText* DlgTitle = UDynamicCast(UStaticText, GetChildByID(14));
	if (DlgTitle)
	{
		DlgTitle->SetTextEdge(true);
		DlgTitle->SetTextColor("23A5DB");
	}

	USkillButton::DataInfo Tempdata;
	for (int i = 0; i < MaxInclosure;  i++)
	{
		USkillButton* pSBtn = (USkillButton*)GetChildByID(i + 7);
		if (pSBtn)
		{
			Tempdata.id = 0;
			Tempdata.pos = i;
			Tempdata.type = UItemSystem::ITEM_DATA_FLAG;
			Tempdata.num = 0;

			pSBtn->SetStateData(&Tempdata);
		}
	}
	m_pMoneytext = UDynamicCast(UMoneyText, GetChildByID(16));
	if (m_pMoneytext)
		m_pMoneytext->SetMoneyTextAlign(UT_RIGHT);

	if (m_CloseBtn)
		m_CloseBtn->SetPosition(UPoint(303, 26));


	m_bCanDrag = FALSE;

	return TRUE;
}

void UMailContent::OnDestroy()
{
	m_Visible = FALSE;
	UDialog::OnDestroy();
}

void UMailContent::OnClose()
{
	SetToDefault();
}

void UMailContent::SetToDefault()
{
	ClearUI();
	SetVisible(FALSE);
}
void UMailContent::OnTimer(float fDeltaTime)
{
	if(!m_Visible)
		return;
	UDialog::OnTimer(fDeltaTime);
	MailSystemMgr->CheckCuState();	
}

void UMailContent::ClearUI()
{
	USkillButton::DataInfo data;
	for (int i = 0; i < MaxInclosure;  i++)
	{
		USkillButton* pSBtn = (USkillButton*)GetChildByID(i + 7);
		if (pSBtn)
		{
			data.id = 0;
			data.pos = i;
			data.type = UItemSystem::ITEM_DATA_FLAG;
			data.num = 0;
			pSBtn->SetStateData(&data);
		}
	}
	m_Caption->Clear();
	m_Sender->Clear();
	m_Content->SetText("",0);
	m_pMoneytext->SetMoney(0);
	m_MailID = 0;
	m_Cod = 0;
	m_bCanGet = TRUE;
}
void UMailContent::SetMailContent(ui32 message_id,  string body)
{
	ClearUI();
	m_bCanGet = TRUE;
	m_MailID = message_id;
	m_strContent = body;

	MSG_S2C::stMail_List_Result::stMail* pMail = MailSystemMgr->GetMailINFO(message_id);
	if (!pMail)
		return;

	std::string head = _TRAN("Title：");
	std::string Caption = head + pMail->subject;
	head = _TRAN( "From：");
	std::string SenderName = head + pMail->sender_name;


	if (pMail->sender_name.length() == 0)
	{
		switch(pMail->message_type)
		{
		case SHOP:
			SenderName += _TRAN("Mall-mail");
			break;
		case GIFT:
			SenderName += _TRAN("Gift-mail");
			break;
		case AUCTION:
			SenderName += _TRAN("Auction Mail");
			break;
		default:
			SenderName += _TRAN("System messages");
			break;
		}
	}
	UBitmapButton* pGetMoneyBtn = UDynamicCast(UBitmapButton, GetChildByID(15));
	if (!pGetMoneyBtn)
		return;
	if (pMail->money > 0)
		m_pMoneytext->SetMoney(pMail->money, pMail->is_yuanbao);

	pGetMoneyBtn->SetBitmap("public\\GetMoney.skin");
	pGetMoneyBtn->SetActive(pMail->money > 0);

	m_Cod = pMail->cod;
	if (pMail->message_type == COD  )
	{
		Caption += _TRAN("（Pay）");
		m_pMoneytext->SetMoney(m_Cod, pMail->is_yuanbao);
		pGetMoneyBtn->SetBitmap("public\\PayForMail.skin");
		pGetMoneyBtn->SetActive(true);
	}

	//显示物品附近
	UINT Size = min(pMail->vMailItems.size(), MaxInclosure);
	USkillButton::DataInfo data;
	for (size_t i =0; i < Size; i++)
	{
		USkillButton* pSBtn = (USkillButton*)GetChildByID(i + 7);
		if (pSBtn && pMail->vMailItems[i].guid != 0)
		{
			data.id = pMail->vMailItems[i].entry;
			data.pos = i;
			data.type = UItemSystem::ITEM_DATA_FLAG;
			data.num = pMail->vMailItems[i].stack_count;
			data.pDataInfo = &pMail->vMailItems[i].extra;
			pSBtn->SetStateData(&data);
			data.pDataInfo = NULL;
		}
	}

	m_Caption->SetText(Caption.c_str());
	m_Sender->SetText(SenderName.c_str());
	string str = ParseItemMsg(m_strContent);
	m_Content->SetText(str.c_str(), str.length());

	SetVisible(TRUE);
}

INT UMailContent::GetSenderName(char* dest, int nMaxLen)
{
	MSG_S2C::stMail_List_Result::stMail* pMail = MailSystemMgr->GetMailINFO(m_MailID);
	if (pMail)
	{
		NiSprintf(dest, 20,  "%s", pMail->sender_name.c_str());
		return pMail->sender_name.length();
	}
	return m_Sender->GetText(dest, nMaxLen, 0);
}
void UMailContent::OnClickInclosure_0()  //
{
	SendInclosure(7);
}
void UMailContent::OnClickInclosure_1()
{	
	SendInclosure(8);
}
void UMailContent::OnClickInclosure_2()
{
	SendInclosure(9);
}
void UMailContent::OnClickInclosure_3()
{
	SendInclosure(10); 
}
void UMailContent::OnClickInclosure_4()
{
	SendInclosure(11);
}
void UMailContent::OnClickInclosure_5()
{
	SendInclosure(12);
}
bool UMailContent::SendInclosure(UINT USkillBtnPos)
{
	if (m_MailID && USkillBtnPos >= 7 && USkillBtnPos <= 12)
	{
		USkillButton* pSkBtn = UDynamicCast(USkillButton, GetChildByID(USkillBtnPos));
		if (pSkBtn)
		{
			USkillButton::DataInfo* data = pSkBtn->GetStateData();
			MSG_S2C::stMail_List_Result::stMail* pMail = MailSystemMgr->GetMailINFO(m_MailID);
			if (!pMail)
				return false;
			size_t  pos = data->pos; 
			if (pos < pMail->vMailItems.size() && data->id != 0)
			{
				if (data->id == pMail->vMailItems[pos].entry && m_bCanGet)
				{
					//发送提取消息
					MailSystemMgr->SendGetItem(m_MailID, pMail->vMailItems[pos].guid);
					m_bCanGet = FALSE;
					return true;
				}
			}
		}
	}
	return false;
}
//
void UMailContent::OnClickReSend()//转发
{
	MailSystemMgr->ReSendToOthers(m_MailID);
}
void UMailContent::OnClickRevert()//回复
{
	MailSystemMgr->RevertMail(m_MailID);
}
void UMailContent::OnClickShield()//屏蔽
{
	MailSystemMgr->ShieldSender(m_MailID);
}
BOOL UMailContent::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}

void UMailContent::OnClickDelete()
{
	MailSystemMgr->SendDelMailByID(m_MailID);
}

void UMailContent::OnURLClicked(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;
	if (strURL.length() < 6)
		return;
	if (strURL.find("Item_") ==1)
	{
		string str = strURL.substr(6,strURL.length());
		ui32 ItemEntry = 0;
		ItemExtraData TempData;
		int PropertyNum = GetItemProperty(str, ItemEntry, TempData);

		TipSystem->ShowTip(ItemEntry, UControl::sm_System->GetCursorPos(), TempData);
		CaptureControl();
	}

}
void UMailContent::OnClickGetMoney()
{
	if (m_Cod > 0 )
	{
		for ( int i = 7 ; i <= 12 ; i++)
		{
			SendInclosure(i);
		}
	}
	else
		MailSystemMgr->SendGetMoney(m_MailID);
}

//BOOL UMailContent::renderTooltip(const UPoint& cursorPos, const char* tipText /* = NULL */ )
//{
//
//	UPoint pt = WindowToScreen(UPoint(0,0));
//	
//	if (m_Cod > 0 && cursorPos.x > pt.x && cursorPos.y > pt.y && cursorPos.x < (pt.x + m_Size.x) && cursorPos.y < (pt.y + m_Size.y))
//	{
//		
//		UINT g = m_Cod / 10000 ;
//		UINT s = (m_Cod % 10000) / 100 ;
//		UINT c = (m_Cod % 10000) % 100 ;
//		char buf[255];
//
//		if (g > 0)
//		{
//			NiSprintf(buf,255,"你需要支付%d金%d银%d铜", g,s,c);
//		}else
//		{
//			if (s > 0 )
//			{
//				NiSprintf(buf,255,"你需要支付%d银%d铜", s,c);
//			}else
//			{
//				NiSprintf(buf,255,"你需要支付%d铜", c);
//			}
//		}
//
//		UStringW ptext;
//		ptext.Set(buf);
//		UPoint t_pt = UPoint(cursorPos.x + 3, cursorPos.y + 3);
//		UINT h = m_Style->m_spFont->GetHeight();
//		UINT w = m_Style->m_spFont->GetStrWidth(ptext.GetBuffer());
//		UPoint t_size = UPoint(w + 6 ,h + 6); 
//		
//		UColor TxtCol = UColor(255, 255, 255, 200); 
//		UColor RecCol = UColor(0,0,0);
//		
//		sm_UiRender->DrawRect(URect(cursorPos, t_size), RecCol);
//		UDrawText(sm_UiRender,m_Style->m_spFont,t_pt,t_size,TxtCol,buf,UT_CENTER);
//	}
//
//	return TRUE;
//	
//}