#pragma once
#include "UInc.h"
#include "../../../../SDBase/Protocol/S2C_Mail.h"

#define MaxInclosure 6

class UMailContent : public UDialog
{
	UDEC_CLASS(UMailContent);
	UDEC_MESSAGEMAP();
public:
	UMailContent();
	~UMailContent();

	void SetMailContent(ui32 message_id, string body);
	ui32 GetMailID(){return m_MailID;}
	INT GetSenderName(char* dest, int nMaxLen);
	void SetCanGet(){m_bCanGet = TRUE ;}

	void SetToDefault();
	
protected:
	//附件位置
	void OnClickInclosure_0();  //
	void OnClickInclosure_1();
	void OnClickInclosure_2();
	void OnClickInclosure_3();
	void OnClickInclosure_4();
	void OnClickInclosure_5();
	bool SendInclosure(UINT USkillBtnPos);
	//
	void OnClickReSend();//转发
	void OnClickRevert();//回复
	void OnClickShield();//屏蔽
	void OnClickDelete();
	void OnClickGetMoney();
	
	void OnURLClicked(UNM* pNM);

	void ClearUI();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	//virtual BOOL renderTooltip(const UPoint& cursorPos, const char* tipText /* = NULL */ );
	virtual void OnTimer(float fDeltaTime);
	virtual void OnClose();
private:
	BOOL m_bCanGet;
	UStaticText* m_Caption;
	UStaticText* m_Sender;
	URichTextCtrl* m_Content;
	class UMoneyText* m_pMoneytext;
	string m_strContent;
	ui32 m_MailID;
	UINT m_Cod;
};