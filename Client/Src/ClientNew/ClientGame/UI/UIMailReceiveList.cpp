#include "stdafx.h"
#include "UIMailReceiveList.h"
#include "../../../../SDBase/Protocol/C2S_Mail.h"
#include "../../../../SDBase/Public/MailDef.h"
#include "Network/PacketBuilder.h"
#include "UMessageBox.h"
#include "UISystem.h"
#include "UIGamePlay.h"
#include "UIMailContent.h"
#include "ObjectManager.h"
#include "UIMailSend.h"
#include "UMailSystem.h"

UIMP_CLASS(UMailReceiveList,UDialog);
UBEGIN_MESSAGE_MAP(UMailReceiveList,UDialog)
UON_BN_CLICKED(0,&UMailReceiveList::OnCheckOutBox)
UON_BN_CLICKED(1,&UMailReceiveList::OnCheckInBox)
UON_BN_CLICKED(2,&UMailReceiveList::SendDeleteMailMsg)
UON_BN_CLICKED(3,&UMailReceiveList::SendOpenMailMsg)
UON_BN_CLICKED(4,&UMailReceiveList::SetPageUp)
UON_BN_CLICKED(5,&UMailReceiveList::SetPageDown)
//
UON_BN_CLICKED(6,&UMailReceiveList::OnClickMail_0)
UON_BN_CLICKED(7,&UMailReceiveList::OnClickMail_1)
UON_BN_CLICKED(8,&UMailReceiveList::OnClickMail_2)
UON_BN_CLICKED(9,&UMailReceiveList::OnClickMail_3)
UON_BN_CLICKED(10,&UMailReceiveList::OnClickMail_4)
UON_BN_CLICKED(11,&UMailReceiveList::OnClickMail_5)
UEND_MESSAGE_MAP();

void UMailReceiveList::StaticInit()
{

}
UMailReceiveList::UMailReceiveList()
{
	m_MailList_Pos0 = NULL;
	m_MailList_Pos1 = NULL;
	m_MailList_Pos2 = NULL;
	m_MailList_Pos3 = NULL;
	m_MailList_Pos4 = NULL;
	m_MailList_Pos5 = NULL;


	m_NumPage = 0 ;
	m_CurPage = 0 ;

	m_CurMailID =  0;
	m_pMailList = NULL;
	m_IsUpdateChild = FALSE;
}
UMailReceiveList::~UMailReceiveList()
{

}

BOOL UMailReceiveList::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	m_MailList_Pos0 = (UMailExplain*)GetChildByID(6);
	m_MailList_Pos1 = (UMailExplain*)GetChildByID(7);
	m_MailList_Pos2 = (UMailExplain*)GetChildByID(8);
	m_MailList_Pos3 = (UMailExplain*)GetChildByID(9);
	m_MailList_Pos4 = (UMailExplain*)GetChildByID(10);
	m_MailList_Pos5 = (UMailExplain*)GetChildByID(11);

	if (m_MailList_Pos0 == NULL || m_MailList_Pos1 == NULL || m_MailList_Pos2 == NULL ||
		m_MailList_Pos3 == NULL || m_MailList_Pos4 == NULL || m_MailList_Pos5 == NULL)
	{
		return FALSE;
	}

	UButton* InBox = (UButton*)GetChildByID(1);
	if (InBox)
		InBox->SetCheck(TRUE);

	if (m_CloseBtn)
		m_CloseBtn->SetPosition(UPoint(319, 22));

	m_bCanDrag = FALSE;

	UStaticText* Title = UDynamicCast(UStaticText, GetChildByID(23));
	if (Title)
		Title->SetTextColor("00FF00");
	UStaticText* DlgTitle = UDynamicCast(UStaticText, GetChildByID(24));
	if (DlgTitle)
	{
		DlgTitle->SetTextEdge(true);
		DlgTitle->SetTextColor("23A5DB");
	}
	return TRUE;
}

void UMailReceiveList::SetUMailList(MSG_S2C::stMail_List_Result* pMailList)
{
	assert(pMailList);
	SetVisible(TRUE);
	ClearUI();
	
	m_pMailList = pMailList ;

	m_NumPage = (m_pMailList->vMails.size()) / (PageListMailMax + 1);

	m_CurPage = 0 ;

	UButton* InBox = (UButton*)GetChildByID(1);
	if (InBox)
	{
		InBox->SetCheck(TRUE);
	}

	OnSelPageMail();
}


void UMailReceiveList::OnCheckInBox()
{

}
void UMailReceiveList::OnClickMail_0()
{
	ui32 Msgid = m_MailList_Pos0->GetMailID();
	m_CurMailID = Msgid;
	if (m_CurMailID == Msgid)
	{
		MailSystemMgr->SendOpenMail(m_CurMailID);
	//
	}else
	{
		m_CurMailID = Msgid;
	}
}
void UMailReceiveList::OnClickMail_1()
{
	ui32 Msgid = m_MailList_Pos1->GetMailID();
	m_CurMailID = Msgid;
	if (m_CurMailID == Msgid)
	{
		MailSystemMgr->SendOpenMail(m_CurMailID);
		//
	}else
	{
		m_CurMailID = Msgid;
	}
}
void UMailReceiveList::OnClickMail_2()
{
	ui32 Msgid = m_MailList_Pos2->GetMailID();
	m_CurMailID = Msgid;
	if (m_CurMailID == Msgid)
	{
		MailSystemMgr->SendOpenMail(m_CurMailID);
		//
	}else
	{
		m_CurMailID = Msgid;
	}
}
void UMailReceiveList::OnClickMail_3()
{
	ui32 Msgid = m_MailList_Pos3->GetMailID();
	m_CurMailID = Msgid;
	if (m_CurMailID == Msgid)
	{
	
		MailSystemMgr->SendOpenMail(m_CurMailID);
		//
	}else
	{
		m_CurMailID = Msgid;
	}
}
void UMailReceiveList::OnClickMail_4()
{
	ui32 Msgid = m_MailList_Pos4->GetMailID();
	m_CurMailID = Msgid;
	if (m_CurMailID == Msgid)
	{
		MailSystemMgr->SendOpenMail(m_CurMailID);
		//
	}else
	{
		m_CurMailID = Msgid;
	}
}
void UMailReceiveList::OnClickMail_5()
{
	ui32 Msgid = m_MailList_Pos5->GetMailID();
	m_CurMailID = Msgid;
	if (m_CurMailID == Msgid)
	{
		MailSystemMgr->SendOpenMail(m_CurMailID);
		//
	}else
	{
		m_CurMailID = Msgid;
	}
}
void UMailReceiveList::OnCheckOutBox()
{
	MailSystemMgr->SetMailState(CMailSystem::UMailState_Send);
}

void UMailReceiveList::SendOpenMailMsg()
{
	if (m_CurMailID)
	{
		MailSystemMgr->SendOpenMail(m_CurMailID);
	}else
	{
		UMessageBox::MsgBox_s( _TRAN("请选择邮件!") );
	}
	
}
void UMailReceiveList::SendDeleteMailMsg()
{
	if (m_CurMailID)
	{
		MailSystemMgr->SendDelMailByID(m_CurMailID);
		
	}else
	{
		UMessageBox::MsgBox_s( _TRAN("请选择邮件!"));
	}
}
void UMailReceiveList::DeleteMail()
{
	

}
void UMailReceiveList::SetPageDown()
{
	if (m_NumPage <= 0)
	{
		return ;
	}

	if (m_CurPage < m_NumPage)
	{
		m_CurPage++;
	}

	OnSelPageMail();
}
void UMailReceiveList::SetPageUp()
{
	if (m_NumPage <= 0)
	{
		return ;
	}

	if (m_CurPage > 0)
	{
		m_CurPage--;
	}

	OnSelPageMail();
}

void UMailReceiveList::OnSelPageMail()
{
	UBitmapButton* pkUp = (UBitmapButton*)GetChildByID(4);
	UBitmapButton* pkDown = (UBitmapButton*)GetChildByID(5);

	
	//第一页
	if (m_CurPage == 0 || m_NumPage == 0)
	{
		if (pkUp)
		{
			pkUp->SetBitmap("Public\\UpToBeginButton.skin");
			pkUp->SetActive(FALSE);
		}
		
	}else
	{
		if (pkUp)
		{
			pkUp->SetBitmap("Public\\UpButton.skin");
			pkUp->SetActive(TRUE);
		}	
	}
	
	//最后一页
	if (m_CurPage ==  m_NumPage || m_NumPage == 0)
	{
		if (pkDown)
		{
			pkDown->SetBitmap("Public\\DownToEndButton.skin");
			pkDown->SetActive(FALSE);
		}
		
	}else
	{
		if (pkDown)
		{
			pkDown->SetBitmap("Public\\DownButton.skin");
			pkDown->SetActive(TRUE);
		}
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(25));
	if (pText)
	{
		char buf[256];
		sprintf(buf, "%u/%u", m_CurPage + 1, m_NumPage + 1 );
		pText->SetText(buf);
		pText->SetTextAlign(UT_CENTER);
	}
	
	vector<MSG_S2C::stMail_List_Result::stMail> pMail = m_pMailList->vMails;
	
	for (int i = 0; i < PageListMailMax; i++)
	{

		if (m_CurPage * PageListMailMax + i < pMail.size())
		{
			OnSelMailToMailExplain(i,pMail[m_CurPage * PageListMailMax + i].message_id);
		}else
		{
			OnSelMailToMailExplain(i, 0);
		}
	}
}
void UMailReceiveList::UpdateMailByID(ui32 MailID)
{

	if (MailID == 0)
	{
		return ;
	}
	if (m_MailList_Pos0->GetMailID() == MailID)
	{
		OnSelMailToMailExplain(0 , MailID);
		return ;
	}

	if (m_MailList_Pos1->GetMailID() == MailID)
	{
		OnSelMailToMailExplain(1 , MailID);
		return ;
	}

	if (m_MailList_Pos2->GetMailID() == MailID)
	{
		OnSelMailToMailExplain(2 , MailID);
		return ;
	}

	if (m_MailList_Pos3->GetMailID() == MailID)
	{
		OnSelMailToMailExplain(3 , MailID);
		return ;
	}

	if (m_MailList_Pos4->GetMailID() == MailID)
	{
		OnSelMailToMailExplain(4 , MailID);
		return ;
	}

	if (m_MailList_Pos5->GetMailID() == MailID)
	{
		OnSelMailToMailExplain(5 , MailID);
		return ;
	}

	return ;
}
void UMailReceiveList::OnSelMailToMailExplain(int pos, ui32 mailID)
{
	if (pos >= 0 && pos < PageListMailMax)
	{
		UMailExplain* pMailExplain = NULL;
		switch (pos)
		{
		case 0:
			pMailExplain = m_MailList_Pos0;
			break;
		case 1:
			pMailExplain = m_MailList_Pos1;
			break;
		case 2:
			pMailExplain = m_MailList_Pos2;
			break;
		case 3:
			pMailExplain = m_MailList_Pos3;
			break;
		case 4:
			pMailExplain = m_MailList_Pos4;
			break;
		case 5:
			pMailExplain = m_MailList_Pos5;
			break;
		}

		if (pMailExplain)
		{
			pMailExplain->SetuMail(mailID);
			if (pMailExplain->IsChecked())
			{
				m_CurMailID = pMailExplain->GetMailID();
			}
		}
	}
}



void UMailReceiveList::ClearUI()
{
	m_pMailList = NULL;
	m_CurPage = 0;
	m_NumPage = 0;

	m_CurMailID = 0;
}
void UMailReceiveList::OnDestroy()
{
	if( m_pMailList )
		m_pMailList->vMails.clear();
	m_Visible = FALSE;
	UDialog::OnDestroy();
}

void UMailReceiveList::OnClose()
{
	CPlayerLocal::SetCUState(cus_Normal);
}

void UMailReceiveList::SetToDefault()
{
	SetVisible(FALSE);
}

BOOL UMailReceiveList::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}
void UMailReceiveList::OnTimer(float fDeltaTime)
{
	if(!m_Visible)
		return;
	UDialog::OnTimer(fDeltaTime);
	MailSystemMgr->CheckCuState();	
}

BOOL UMailReceiveList::OnMouseWheel(const UPoint& position, int delta , UINT flags)
{
	if (delta < 0)
	{
		SetPageDown();
	}else
	{
		SetPageUp();
	}
	return TRUE;
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


UIMP_CLASS(UMailExplain,UBitmapButton);
void  UMailExplain::StaticInit()
{

}

UMailExplain::UMailExplain()
{
	m_Icon = NULL;
	m_Caption = NULL;
	m_Sender = NULL;
	m_Days = NULL;

	m_bRead = FALSE;
	m_MailID = 0 ;

}

UMailExplain::~UMailExplain()
{

}

BOOL UMailExplain::OnCreate()
{
	if (!UBitmapButton::OnCreate())
	{
		return FALSE;
	}

	m_Icon = UDynamicCast(USkillButton, GetChildByID(3));
	m_Caption = UDynamicCast(UStaticText, GetChildByID(0));
	m_Sender = UDynamicCast(UStaticText, GetChildByID(1));
	m_Days = UDynamicCast(UStaticText, GetChildByID(2));
	if (m_Icon == NULL || m_Caption == NULL || m_Sender == NULL || m_Days == NULL)
	{
		return FALSE;
	}

	m_Caption->SetTextAlign(UT_LEFT);
	m_Caption->SetTextColor("000000");
	m_Days->SetTextAlign(UT_RIGHT);
	m_Days->SetTextColor("FFFFFF");
	m_Days->SetTextShadow(true);
	m_Sender->SetTextAlign(UT_LEFT);
	m_Sender->SetTextColor("090c59");

	USkillButton::DataInfo data;
	data.id = 0;
	data.pos = 0;
	data.type = UItemSystem::ITEM_DATA_FLAG;
	data.num = 0;
	m_Icon->SetStateData(&data);	

	return TRUE;
}

void UMailExplain::SetuMail(ui32 MailID)
{
	if (MailID == 0)
	{
		ClearUI();
		SetVisible(FALSE);
		return ;
	}else
	{
		SetVisible(TRUE);
		m_MailID = MailID;
		
		MSG_S2C::stMail_List_Result::stMail* pMailInfo = MailSystemMgr->GetMailINFO(m_MailID);
		if (!pMailInfo)
			return;

		
		//发件人
		std::string SenderName = pMailInfo->sender_name;

		if (pMailInfo->sender_name.length() == 0)
		{
			switch(pMailInfo->message_type)
			{
			case SHOP:
				SenderName += _TRAN("商城");
				break;
			case GIFT:
				SenderName += _TRAN("礼物");
				break;
			case AUCTION:
				SenderName += _TRAN("拍卖行");
				break;
			default:
				SenderName += _TRAN("系统");
				break;
			}
		}
		
		m_Sender->SetText(SenderName.c_str());

		//主题
		std::string SubjectName = pMailInfo->subject;
		if (pMailInfo->message_type == COD)
		{
			SubjectName += _TRAN("（付费）");
		}
		m_Caption->SetText(SubjectName.c_str());
		
		//到期时间
		std::string dayBuf;
		if (pMailInfo->expire_time)
		{
			dayBuf = _TRAN( N_NOTICE_C9, _I2A( pMailInfo->expire_time ).c_str() );
			//NiSprintf(dayBuf, 100, " %d天",pMailInfo->expire_time);
			m_Days->SetText(dayBuf.c_str());
			if(pMailInfo->expire_time < 3)
				m_Days->SetTextColor("FF0000");
			else
				m_Days->SetTextColor("00FF00");
		}else
		{
			m_Days->SetText( _TRAN("永久") );
			m_Days->SetTextColor("00FF00");
		}
		
		USkillButton::DataInfo data;

		if (pMailInfo->vMailItems.size() > 0)
		{
			data.id = pMailInfo->vMailItems[0].entry;
			data.pos = 0 ;
			data.type = UItemSystem::ITEM_DATA_FLAG;
			data.num = pMailInfo->vMailItems[0].stack_count;
			data.pDataInfo = &pMailInfo->vMailItems[0].extra;

			m_Icon->SetBtnBgkByStr(NULL);
		}else
		{
			data.id = 0;
			data.pos = 0;
			data.type = UItemSystem::ITEM_DATA_FLAG;
			data.num = 0;

			if (!pMailInfo->read_flag)
			{
				m_Icon->SetBtnBgkByStr("MailSystem\\UnRead.skin");
			}else
			{
				m_Icon->SetBtnBgkByStr("MailSystem\\Read.skin");
			}
		}
		m_Icon->SetStateData(&data);	
		data.pDataInfo = NULL;
	}
}

void UMailExplain::ClearUI()
{
	m_Caption->Clear();
	m_Days->Clear();
	m_Sender->Clear();

	m_MailID = 0;

	USkillButton::DataInfo data;
	data.id = 0;
	data.pos = 0;
	data.type = UItemSystem::ITEM_DATA_FLAG;
	data.num = 0;

	m_Icon->SetStateData(&data);	
	m_Icon->SetBtnBgkByStr(NULL);

}
void UMailExplain::OnDestroy()
{
	UBitmapButton::OnDestroy();
}
void UMailExplain::OnRender(const UPoint& offset, const URect &updateRect)
{
	UBitmapButton::OnRender(offset,updateRect);
	RenderChildWindow(offset,updateRect);
}