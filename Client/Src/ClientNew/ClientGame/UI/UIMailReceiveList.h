#pragma once
#include "UInc.h"
#include "USkillButton.h"
#include "../../../../SDBase/Protocol/S2C_Mail.h"
#include "../../../../SDBase/Protocol/C2S_Mail.h"

#define PageListMailMax  6            //每页最多显示


class UMailExplain : public UBitmapButton
{
	UDEC_CLASS(UMailExplain);
public:
	UMailExplain();
	~UMailExplain();

	void SetuMail(ui32 MailID);
	ui32 GetMailID(){return m_MailID;}
protected:
	void ClearUI();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */){
		return this;}
private:
	USkillButton* m_Icon;
	UStaticText* m_Caption;
	UStaticText* m_Sender;
	UStaticText* m_Days;
	BOOL*  m_bRead;
	ui32   m_MailID;

};

class UMailReceiveList : public UDialog
{
	UDEC_CLASS(UMailReceiveList);
	UDEC_MESSAGEMAP();
public:
	UMailReceiveList();
	~UMailReceiveList();

	void SetUMailList(MSG_S2C::stMail_List_Result* pMailList);

	void DeleteMail();
	ui32 GetMailID(){return m_CurMailID;}

	void UpdateMailByID(ui32 MailID);

	void SetToDefault();
	
protected:
	void ClearUI();
	void OnSelMailToMailExplain(int pos,  ui32 mailID);
	void OnSelPageMail();

	void SendOpenMailMsg();
	void SendDeleteMailMsg();
	void SetPageUp();
	void SetPageDown();

	void OnCheckInBox();
	void OnCheckOutBox();

	void OnClickMail_0();
	void OnClickMail_1();
	void OnClickMail_2();
	void OnClickMail_3();
	void OnClickMail_4();
	void OnClickMail_5();

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual BOOL OnMouseWheel(const UPoint& position, int delta , UINT flags);
	virtual void OnTimer(float fDeltaTime);
	virtual void OnClose();
private:
	UMailExplain* m_MailList_Pos0;
	UMailExplain* m_MailList_Pos1;
	UMailExplain* m_MailList_Pos2;
	UMailExplain* m_MailList_Pos3;
	UMailExplain* m_MailList_Pos4;
	UMailExplain* m_MailList_Pos5;

	UINT  m_NumPage;
	UINT  m_CurPage;
	ui32  m_CurMailID;  //当前选中的ID

	MSG_S2C::stMail_List_Result* m_pMailList;
	
};