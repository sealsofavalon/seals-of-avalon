#include "stdafx.h"
#include "UIMailSend.h"
#include "Network/PacketBuilder.h"
#include "USkillButton.h"
#include "../../../../SDBase/Protocol/C2S_Mail.h"
#include "UIMailReceiveList.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "UMessageBox.h"
#include "SYItem.h"
#include "ObjectManager.h"
#include "ItemSlot.h"
#include "UIItemSystem.h"
#include "Utils/ChatFileterDB.h"
#include "UMailSystem.h"

#define NameMaxLen 20
#define MaxSubjectLen 16
#define MaxLenContent 400

UIMP_CLASS(UMailSend,UDialog);
UBEGIN_MESSAGE_MAP(UMailSend,UDialog)
UON_BN_CLICKED(0, &UMailSend::OnCheckOutBox)
UON_BN_CLICKED(1, &UMailSend::OnCheckInBox)
UON_BN_CLICKED(2, &UMailSend::OnClickCancel)
UON_BN_CLICKED(3, &UMailSend::OnClickSend)

UON_BN_CLICKED(8, &UMailSend::OnCheckSendMoney)
UON_BN_CLICKED(9, &UMailSend::OnCheckCarrier)

UON_BN_CLICKED(14, &UMailSend::OnClickInclosure_0)
UON_BN_CLICKED(15, &UMailSend::OnClickInclosure_1)
UON_BN_CLICKED(16, &UMailSend::OnClickInclosure_2)
UON_BN_CLICKED(17, &UMailSend::OnClickInclosure_3)
UON_BN_CLICKED(18, &UMailSend::OnClickInclosure_4)
UON_BN_CLICKED(19, &UMailSend::OnClickInclosure_5)
UEND_MESSAGE_MAP();

void UMailSend::StaticInit()
{

}
UMailSend::UMailSend()
{
	m_Subject = NULL; // 主题
	m_Content = NULL; // 内容
    m_Recepient = NULL; //收件人

	m_Gold = NULL;
	m_Silver = NULL;
	m_Copper = NULL;

	m_Postage = NULL; //邮费

	m_bSaleOrSend = TRUE;
	m_IsUpdateChild = FALSE;

	for (int i = 0; i < InclosureMax; i++)
	{
		m_Inclosures[i] = 0;
	}
}
UMailSend::~UMailSend()
{

}

BOOL UMailSend::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	UScrollBar* pScrollBar = (UScrollBar*)GetChildByID(6);
	if (pScrollBar == NULL)
		return FALSE;
	pScrollBar->SetBarShowMode(USBSM_FORCEON,USBSM_FORCEOFF);

	USkillButton::DataInfo dataInfo ;
	for (int i = 14; i <= 19; i++)
	{
		dataInfo.id = 0;
		dataInfo.pos = i - 14 ;  // pos form 0 to 13
		dataInfo.type = UItemSystem::ITEM_DATA_FLAG;  //物品类型
		dataInfo.num = 0;
		USkillButton* pSkBtn = UDynamicCast(USkillButton,GetChildByID(i));
		if (pSkBtn)
		{
			pSkBtn->SetUIType(UItemSystem::ICT_MAIL);
			pSkBtn->SetStateData(&dataInfo);
		}
	}

	m_Content = UDynamicCast(UStaticTextEdit, pScrollBar->GetChildByID(7));
	m_Postage = UDynamicCast(UStaticText, GetChildByID(10));
	m_Gold = UDynamicCast(UEdit, GetChildByID(11));
	m_Silver = UDynamicCast(UEdit, GetChildByID(12));
	m_Copper = UDynamicCast(UEdit, GetChildByID(13));
	m_Subject = UDynamicCast(UEdit, GetChildByID(4));
	m_Recepient = UDynamicCast(UEdit, GetChildByID(5));

	if (m_Subject == NULL || m_Recepient == NULL || m_Content == NULL || m_Postage == NULL ||
		m_Gold == NULL || m_Silver == NULL || m_Copper == NULL)
	{
		return FALSE;
	}
	m_Postage->SetText( _TRAN("邮资：30银") );
	m_Postage->SetTextColor("FFFF00");
	m_Subject->SetMaxLength(MaxSubjectLen);
	m_Recepient->SetMaxLength(NameMaxLen);
	m_Content->SetTextMaxLen(MaxLenContent);
	m_Gold->SetMaxLength(8);
	m_Gold->SetNumberOnly(TRUE);
	m_Silver->SetMaxLength(2);  //最高输入2位
	m_Silver->SetNumberOnly(TRUE);
	m_Copper->SetMaxLength(2);  //最高输入2位
	m_Copper->SetNumberOnly(TRUE);

	UButton* OutBox = UDynamicCast(UButton, GetChildByID(0));
	if (OutBox)
		OutBox->SetCheck(FALSE);

	UButton* SendMoneyBtn = UDynamicCast(UButton, GetChildByID(8));
	if (SendMoneyBtn)
		SendMoneyBtn->SetCheck(FALSE);

	UStaticText* Title = UDynamicCast(UStaticText, GetChildByID(23));
	if (Title)
		Title->SetTextColor("00FF00");
	UStaticText* DlgTitle = UDynamicCast(UStaticText, GetChildByID(24));
	if (DlgTitle)
	{
		DlgTitle->SetTextEdge(true);
		DlgTitle->SetTextColor("23A5DB");
	}

	if (m_CloseBtn)
	{
		m_CloseBtn->SetPosition(UPoint(319, 22));
	}

	m_bCanDrag = FALSE;
	return TRUE;
}

void UMailSend::OnDestroy()
{
	m_Visible = FALSE;
	UDialog::OnDestroy();
}

void UMailSend::OnClose()
{
	CPlayerLocal::SetCUState(cus_Normal);
}
void UMailSend::SetToDefault()
{
	ClearUI();
	sm_System->GetCurFrame()->SetFocusControl();
	SetVisible(FALSE);
	UButton* SendMoneyBtn = UDynamicCast(UButton, GetChildByID(8));
	if (SendMoneyBtn)
		SendMoneyBtn->SetCheck(FALSE);
}

void UMailSend::OnTimer(float fDeltaTime)
{
	if(!m_Visible)
		return;
	UDialog::OnTimer(fDeltaTime);
	MailSystemMgr->CheckCuState();
}

BOOL UMailSend::SetInclosureItem(UINT pos , ui64 guid)
{
	for (int i = 0 ; i < InclosureMax; i++)
	{
		if (m_Inclosures[i] == guid && guid)
		{
			UMessageBox::MsgBox_s( _TRAN("已经在发送列表内!") ) ;
			return FALSE;
		}
	}
	ReleaseBagLockByPos(pos);
	if (pos >= 0 && pos < InclosureMax)
	{
		USkillButton::DataInfo dataInfo ;
		dataInfo.pos = pos ;  // pos form 0 to 13
		dataInfo.type = UItemSystem::ITEM_DATA_FLAG;  //物品类型

		USkillButton* pUSkBtn = NULL;
		if (guid != 0)
		{
			SYItem* pSYItem= (SYItem*)ObjectMgr->GetObject(guid);
			if (!pSYItem)
			{
				return FALSE;
			}
			dataInfo.id = pSYItem->GetCode();
			dataInfo.num = pSYItem->GetNum();

			ItemExtraData* TempData = new ItemExtraData;
			TempData->jinglian_level = pSYItem->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
			for( int i = 0; i < 6; ++i )
				TempData->enchants[i] =  pSYItem->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + i * 3 );
			dataInfo.pDataInfo = TempData;
		}else
		{
			dataInfo.id = 0;
			dataInfo.num = 0;
		}
		
		switch (pos)
		{
		case 0 :
			pUSkBtn = (USkillButton*)GetChildByID(14);
			break;
		case 1 :
			pUSkBtn = (USkillButton*)GetChildByID(15);
			break;
		case 2 :
			pUSkBtn = (USkillButton*)GetChildByID(16);
			break;
		case 3 :
			pUSkBtn = (USkillButton*)GetChildByID(17);
			break;
		case 4 :
			pUSkBtn = (USkillButton*)GetChildByID(18);
			break;
		case 5 :
			pUSkBtn = (USkillButton*)GetChildByID(19);
			break;
		}

		if (pUSkBtn)
		{
			pUSkBtn->SetStateData(&dataInfo);
			m_Inclosures[pos] = guid;
		}
	}

	return TRUE ;
}

int UMailSend::GetNullPos()
{
	// i对应的是控件的ID
	for (int i = 14; i <= 19; i++)
	{
		USkillButton::DataInfo* dataInfo ;
		USkillButton* pSkBtn = UDynamicCast(USkillButton,GetChildByID(i));
		if (pSkBtn)
		{
			dataInfo = pSkBtn->GetStateData();
			if (dataInfo && dataInfo->id == 0)
			{
				return dataInfo->pos;
			}
		}
	}
	return -1;
}

void UMailSend::SetSubject(string subject)
{
	if (subject.length() > 0)
	{
		m_Subject->SetText(subject.c_str());
	}
}
void UMailSend::SetContent(string Content)
{
	if (Content.length() > 0)
	{
		m_Content->SetText(Content.c_str());
	}
}
void UMailSend::SetRecepient(const char* Recepient)
{
	m_Recepient->SetText(Recepient);	
}
void UMailSend::ClearMailItem()
{
	for (int i = 0; i < InclosureMax; i++)
	{
		SetInclosureItem(i, 0);
	}
}
void UMailSend::ReleaseBagLockByPos(int pos)
{
	if (pos >= 0 && pos < InclosureMax)
	{
		if (m_Inclosures[pos])
		{
			CPlayer* pLocal = ObjectMgr->GetLocalPlayer();
			assert(pLocal);
			CStorage* pBagStorage = pLocal->GetItemContainer();
			assert(pBagStorage);

			CItemSlot* pSlot = (CItemSlot*)pBagStorage->GetSlotByGuid(m_Inclosures[pos]);
			assert(pSlot);
			UINT cBagPos = pBagStorage->GetUIPos(m_Inclosures[pos]);
			m_Inclosures[pos] = 0;

			pSlot->SetLock(FALSE);
			SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG,cBagPos,pSlot->GetCode(),pSlot->GetNum(),UItemSystem::ITEM_DATA_FLAG,FALSE);
		}
	}
}
void UMailSend::OnClickInclosure_0()  //
{
	SetInclosureItem(0,0);
}
void UMailSend::OnClickInclosure_1()
{
	SetInclosureItem(1,0);
}
void UMailSend::OnClickInclosure_2()
{
	SetInclosureItem(2,0);
}
void UMailSend::OnClickInclosure_3()
{
	SetInclosureItem(3,0);
}
void UMailSend::OnClickInclosure_4()
{
	SetInclosureItem(4,0);
}
void UMailSend::OnClickInclosure_5()
{
	SetInclosureItem(5,0);
}
//
void UMailSend::OnClickSend()
{
	char Recepient[NameMaxLen * 5 + 4];
	char Subject[MaxSubjectLen * 5  + 4];
	char body[MaxLenContent * 5  + 4];

	uint32 money = 0;
	uint32 cod = 0;
	vector<MSG_C2S::stMail_Send::stMailItem> vMailItems;

	int rLen = m_Recepient->GetText(Recepient,NameMaxLen * 5 + 4);
	if (rLen == 0)
	{
		UMessageBox::MsgBox_s( _TRAN("请输入收件人姓名!") );
		m_Recepient->SetFocusControl();
		return ;
	}
    Recepient[rLen] = '\0';

	int sLen = m_Subject->GetText(Subject,MaxSubjectLen * 5  + 4);
	if (sLen == 0)
		sprintf(Subject, _TRAN("请查收") );
	else
		Subject[sLen] = '\0';

    if (g_pkChatFilterDB->GetFilterChatMsg(std::string(Subject)))
    {
        UMessageBox::MsgBox_s( _TRAN("您的输入的主题中带有非法字符！") );
		m_Subject->SetFocusControl();
        return ;
    }

	int cLen = m_Content->GetText(body,MaxLenContent * 5  + 4);
	if (cLen == 0)
	{
		UMessageBox::MsgBox_s( _TRAN("请输入邮件内容") );
		m_Content->SetFocusControl();
		return;
	}
    body[cLen] = '\0';
    if (g_pkChatFilterDB->GetFilterChatMsg(std::string(body)))
	{
		UMessageBox::MsgBox_s( _TRAN("您的输入的内容中带有非法字符！") );
		m_Content->SetFocusControl();
		return ;
	}

	CPlayerLocal* localPlayer = ObjectMgr->GetLocalPlayer();
	if (localPlayer == NULL)
		return ;

	MSG_C2S::stMail_Send::stMailItem mailItem ;
	for (int i =0 ; i < InclosureMax ; i++)
	{
		if (m_Inclosures[i] != 0)
		{
			mailItem.itemguid = m_Inclosures[i];
			mailItem.itemslot = i;
			vMailItems.push_back(mailItem);
		}
	}

	if (!m_bSaleOrSend)
	{
		cod = 0 ;
		money = GetInputMoney();
		//去除邮资的设置
		if (money > localPlayer->GetMoney() - 3000)
		{
			money = localPlayer->GetMoney() - 3000;
			SetLockMoney(money);
			UMessageBox::MsgBox_s( _TRAN("您所发送的金钱超过当前拥有金钱。") );
			return;
		}
	}else
	{
		cod =  GetInputMoney();
		money  = 0 ;
		if (vMailItems.size() == 0 && cod > 0)
		{
			cod = 0 ;
			UMessageBox::MsgBox_s( _TRAN("付款邮件必须拥有一个附件。") );
			return;
		}	
	}

	MSG_C2S::stMail_Send pMail;
	pMail.body = body;
	pMail.cod = cod;
	pMail.money = money;
	pMail.recepient = Recepient;
	pMail.subject = Subject;
	pMail.vMailItems = vMailItems;

	MailSystemMgr->SetSendMail(pMail);
}
void UMailSend::OnClickCancel()
{
	ClearUI();
}
void UMailSend::ClearUI()
{
	ClearMailItem();
	SetLockMoney(0);
	m_Subject->SetText("");
	m_Content->SetText("");
	m_Recepient->SetText("");
	//BLockInclosure();
}
void UMailSend::OnCheckInBox()
{
	MailSystemMgr->SetMailState(CMailSystem::UMailState_List);
	MailSystemMgr->SendbReQuestList();
}
void UMailSend::OnCheckOutBox()
{

}
ui32 UMailSend::GetInputMoney()
{
	char gold[100];
	char silver[100];
	char copper[100];

	ui32 uiMoney = 0;
	int G_len = m_Gold->GetText(gold,100);
	int S_len = m_Silver->GetText(silver,100);
	int C_len = m_Copper->GetText(copper,100);

	if (G_len > 0)
	{
		uiMoney = uiMoney + atoi(gold) * 10000;
	}
	if (S_len > 0)
	{
		uiMoney = uiMoney + atoi(silver) * 100;
	}
	if (C_len)
	{
		uiMoney = uiMoney + atoi(copper);
	}

	return uiMoney;

}


void UMailSend::SetLockMoney(unsigned int num)
{
	char gold[100];
	char silver[100];
	char copper[100];

	int I_Gold = num /10000 ;
	int I_Silver = (num % 10000) / 100 ;
	int I_Copper = (num % 10000) % 100 ;


	sprintf(gold, "%d", I_Gold);
	sprintf(silver, "%d", I_Silver);
	sprintf(copper, "%d", I_Copper);

	if (m_Gold && m_Silver && m_Copper)
	{
		m_Gold->SetText(gold);
		m_Silver->SetText(silver);
		m_Copper->SetText(copper);
	}
	

}

void UMailSend::OnCheckSendMoney()//发送钱币
{
	m_bSaleOrSend = FALSE ;
	SetLockMoney(0);
	//ClearMailItem();
	//BLockInclosure();

	GetChildByID(108)->SetActive(TRUE);
	GetChildByID(109)->SetActive(FALSE);
}
void UMailSend::OnCheckCarrier()// 付费取货
{
	if (!m_bSaleOrSend)
	{
		m_bSaleOrSend = TRUE;
		//BLockInclosure();
	}
	SetLockMoney(0);

	GetChildByID(108)->SetActive(FALSE);
	GetChildByID(109)->SetActive(TRUE);
}
BOOL UMailSend::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}