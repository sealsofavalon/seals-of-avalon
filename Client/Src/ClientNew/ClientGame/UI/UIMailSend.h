#pragma once
#include "UInc.h"

#define  InclosureMax 6   //最大附件数

class UMailSend : public UDialog
{
	UDEC_CLASS(UMailSend);
	UDEC_MESSAGEMAP();
public:
	UMailSend();
	~UMailSend();

	
	
	void SetSubject(string subject);
	void SetContent(string Content);
	void SetRecepient(const char* Recepient);
	BOOL IsSaleItem(){return m_bSaleOrSend;}

	void SetToDefault();
	void ClearUI();
protected:
	//附件位置
	void OnClickInclosure_0();  //
	void OnClickInclosure_1();
	void OnClickInclosure_2();
	void OnClickInclosure_3();
	void OnClickInclosure_4();
	void OnClickInclosure_5();

	//
	void OnClickSend();
	void OnClickCancel();
	void OnCheckInBox();  //请求获得邮件列表
	void OnCheckOutBox();
	void OnCheckSendMoney();  //发送钱币
	void OnCheckCarrier(); // 付费取货
public:
	void ClearMailItem(); //选择发送钱币的时候，释放背包里的锁定物品， 并且清除附近里的物品。
	void ReleaseBagLockByPos(int pos);//释放背包位置
	BOOL SetInclosureItem(UINT pos , ui64 guid);
	int  GetNullPos();   //返回空的Pos
protected:
	ui32 GetInputMoney();
	void SetLockMoney(unsigned int num);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnClose();
private:
	BOOL m_bSaleOrSend;
	ui64 m_Inclosures[InclosureMax];
	UEdit* m_Subject; // 主题
	UStaticTextEdit* m_Content; // 内容
	UEdit* m_Recepient; //收件人

	UEdit* m_Gold;
	UEdit* m_Silver;
	UEdit* m_Copper;

	UStaticText* m_Postage;


};