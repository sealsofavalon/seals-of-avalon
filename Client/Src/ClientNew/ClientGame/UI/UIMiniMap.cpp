#include "StdAfx.h"
#include "UISystem.h"
#include "UIMinimap.h"
#include "Map/Map.h"
#include "Utils/MapInfoDB.h"
#include "UIGamePlay.h"
#include "Utils/AreaDB.h"
#include "UIFadeText.h"
#include "UIShowPlayer.h"
#include "Network/PacketBuilder.h"
#include "AudioDef.h"
#include "AudioInterface.h"
#include "UInstanceMgr.h"
#include "RankMgr.h"
#include "UBigMap.h"
#include "UFun.h"
#include "PlayerInputMgr.h"
#include "SocialitySystem.h"
#include "UITipSystem.h"
#include "../../../../SDBase/Public/CreatureDef.h"


UIMP_CLASS(UMinimapFrame,UControl);
UBEGIN_MESSAGE_MAP(UMinimapFrame, UCmdTarget)
UON_BN_CLICKED(2, &UMinimapFrame::OnZoomMap)
UON_BN_CLICKED(3, &UMinimapFrame::OnReduceMap)
UON_BN_CLICKED(4, &UMinimapFrame::OnShowBigMap)
UON_BN_CLICKED(7, &UMinimapFrame::OnShowMinimap)
UON_BN_CLICKED(9, &UMinimapFrame::OnExitInstance)
UON_BN_CLICKED(10, &UMinimapFrame::OnSendShowRank)
UON_BN_CLICKED(13, &UMinimapFrame::OnClickQuery)
UON_BN_CLICKED(14, &UMinimapFrame::OnClickHOOK)
UON_BN_CLICKED(15, &UMinimapFrame::OnClickShow)
UON_BN_CLICKED(16, &UMinimapFrame::OnClickApplyGroup)
UEND_MESSAGE_MAP()

void UMinimapFrame::StaticInit()
{
}

void UMinimapFrame::OnZoomMap()
{
	BOOL bActive = g_pkMiniMap->ZoomMap();
	GetChildByID(2)->SetActive(bActive);
	GetChildByID(3)->SetActive(TRUE);
}
void  UMinimapFrame::MiNiMapShowControl(MINIMAPSHOWTYPE ShowType, BOOL vis)
{
	g_pkMiniMap->MiNiMapShowControl(ShowType,vis);
}

void UMinimapFrame::OnReduceMap()
{
	BOOL bActive = g_pkMiniMap->ReduceMap();
	GetChildByID(2)->SetActive(TRUE);
	GetChildByID(3)->SetActive(bActive);
}

void UMinimapFrame::OnShowBigMap() //显示大地图
{
	UInGame* pkInGame = UInGame::Get();
	if (pkInGame)
	{
		UBigMapFrame* pBigMap = INGAMEGETFRAME(UBigMapFrame, FRAME_IG_BIGMAP);
		if (pBigMap)
		{
			if (pBigMap->IsVisible()){
				pBigMap->SetVisible(false);
			}
			else
				pBigMap->ShowBigMap();
			//pBigMap->SetVisible(!pBigMap->IsVisible());
			pkInGame->MoveChildTo(pBigMap, NULL);
		}
	}
}

void  UMinimapFrame::OnExitInstance()
{
	UControl * pGameBar = UInGame::GetFrame(FRAME_IG_ACTIONSKILLBAR);
	if (pGameBar)
	{
		UButton* pBtn =  UDynamicCast(UButton, pGameBar->GetChildByID(UINGAMEBAR_BUTTONINSTANCE));
		if (pBtn)
		{
			pBtn->SetCheck(pBtn->IsChecked());
		}
	}
}
void  UMinimapFrame::OnSendShowRank()
{
	if (RankMgr)
	{
		RankMgr->SendShowRank();
	}
}

void  UMinimapFrame::OnClickQuery()
{	
	UButton* pBsHOW = INGAMEGETFRAME(UButton, FRAME_IG_BSHOWLEVELTIPS);
	if(pBsHOW)
	{
		pBsHOW->SetCheck(pBsHOW->IsChecked());
	}
}
void  UMinimapFrame::OnClickHOOK()
{
	SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_F11));
}

void UMinimapFrame::OnClickShow()
{
	UControl * pShop = UInGame::GetFrame(FRAME_IG_SHOPDLG);
	if (pShop)
	{
		pShop->SetVisible(!pShop->IsVisible());
	}
}
#include "SocialitySystem.h"
void  UMinimapFrame::OnClickApplyGroup()
{
	SocialitySys->GetApplyGroupSysPtr()->CallApplyGroupDlg();
}

void UMinimapFrame::SetApplyGroupVisible(bool vis)
{
	UButton* pBtn =  UDynamicCast(UButton, GetChildByID(16));
	if (pBtn)
	{
		pBtn->SetVisible(vis);
		if (vis)
			pBtn->SetGlint(TRUE);
	}
}

void UMinimapFrame::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);
	if (RankMgr)
	{
		RankMgr->UpdateRankTime(fDeltaTime);
	}
	ui64 time;
	GetClientTime(time);
	m_Time->SetText(ConvertTimeToString(time).c_str());
}
void UMinimapFrame::SetDrawFindOBJPos(bool bDraw, NiPoint3 kPos )
{
	if (g_pkMiniMap)
	{
		g_pkMiniMap->SetDrawFindOBJPos(bDraw,kPos);
	}
}
void  UMinimapFrame::OnShowMail(BOOL bShow)
{
	UBitmap* pMailT = UDynamicCast(UBitmap, GetChildByID(8));
	if (pMailT)
	{
		if(bShow)
		{
			sm_System->__PlaySound(UI_mail);
		}
		pMailT->SetVisible(bShow);
	}
}
void  UMinimapFrame::OnShowMinimap()
{
	BOOL bShow = g_pkMiniMap->IsVisible();
	for (unsigned int i =0; i < m_Childs.size(); i++)
	{
		UControl* pControl = GetChildByID(i);  //背景
		pControl->SetVisible(!bShow);
	}
		
	UBitmapButton* pBTN = UDynamicCast(UBitmapButton,GetChildByID(7));
	if (pBTN)
	{
		if (!bShow)
		{
			pBTN->SetBitmap("MiniMap\\SMiNimap.skin");
		}else
		{
			pBTN->SetBitmap("MiniMap\\HMiNimap.skin");
		}
		pBTN->SetVisible(TRUE);
	}
	
}
BOOL UMinimapFrame::OnCreate()
{
	if (!UControl::OnCreate())
		return FALSE;

	m_Time = UDynamicCast(UStaticText, GetChildByID(11));
	if(!m_Time)
		return FALSE;
	m_Time->SetTextAlign(UT_CENTER);
	m_Time->SetTextColor(UColor(0, 255, 0, 255));

	UStaticText* pTitle = UDynamicCast(UStaticText,GetChildByID(5));
	if (pTitle)
	{
		pTitle->SetTextAlign(UT_CENTER);
		pTitle->SetTextEdge(TRUE);
		pTitle->SetTextColor(UColor(255, 211, 0, 255));
	}

	UStaticText* pPostion = UDynamicCast(UStaticText, GetChildByID(6));
	if (pPostion)
	{
		pPostion->SetTextAlign(UT_CENTER);
		pPostion->SetTextShadow(TRUE);
		pPostion->SetTextColor(UColor(255, 211, 0, 255));
	}

	g_pkMiniMap->SetMapNameControl((UStaticText*)GetChildByID(5));
	g_pkMiniMap->SetMapCoordinateControl((UStaticText*)GetChildByID(6));
	OnShowMail(FALSE);
	return TRUE;
}
void UMinimapFrame::OnDestroy()
{
	UBitmap* pMailT = UDynamicCast(UBitmap, GetChildByID(8));
	if (pMailT)
	{
		pMailT->SetVisible(false);
	}
	UControl::OnDestroy();
}

void UMinimapFrame::OnMouseMove(const UPoint& position, UINT flags)
{
	TipSystem->HideTip();
	std::vector<std::string> vName;
	if(g_pkMiniMap->GetObjectNameByPosition(position, vName))
	{
		TipSystem->ShowTip(position + UPoint(-100, 0), vName);
	}
}
void UMinimapFrame::OnMouseEnter(const UPoint& position, UINT flags)
{
	TipSystem->HideTip();
}
void UMinimapFrame::OnMouseLeave(const UPoint& position, UINT flags)
{
	TipSystem->HideTip();
}


UMinimap* g_pkMiniMap = NULL;
static bool bCalPosition = true;

UIMP_CLASS(UMinimap,UControl);
UBEGIN_MESSAGE_MAP(UMinimap,UCmdTarget)
UEND_MESSAGE_MAP()
void UMinimap::StaticInit()
{
}

UMinimap::UMinimap()
{
	g_pkMiniMap = this;

	m_MapName = NULL;
	m_MapCoordinate = NULL;

	for( int i = 0; i < CACHE_SIZE; i++ )
	{
		m_kCacheTexture[i].m_iX = -2;
		m_kCacheTexture[i].m_iY = -2;
	}

	m_fScale = 0.3f;

	m_bDrawFindObjPos = false;
	FindOBJMapId = 0;
	m_kFindObjPos = NiPoint3::ZERO;

	m_ShowFlag = 0;

	m_LocalPosX = 0.0f;
	m_LocalPosY = 0.0f;
}

UMinimap::~UMinimap()
{
}
void UMinimap::MiNiMapShowControl(MINIMAPSHOWTYPE ShowType,  BOOL vis)
{
	m_ObjectRenderList.clear();
	if (vis)
	{
		m_ShowFlag |= ShowType;
	}
	else
	{
		m_ShowFlag &= ~ShowType;
	}
}
BOOL UMinimap::InitTextureList()
{
	m_TextureList[MaskTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\MiniMapMask.dds");
	m_TextureList[PlayerTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\Teammem.png");
	m_TextureList[OtherPlayerTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\otherplayer.png");
	m_TextureList[ArrowTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\MiniMapArrow.png");
	m_TextureList[TeamArrow] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\TeamArrow.png");
	m_TextureList[FindOBJArrow] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\FindObj.png");
	m_TextureList[SeeOBJArrow] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\SeeFindObj.png");
	m_TextureList[DotTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\MiniMapDot.png");
	m_TextureList[CanGetQTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\CanQ.png");
	m_TextureList[PerformQTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\DoQ.png");
	m_TextureList[FinishQTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\FinishQ.png");
	m_TextureList[UnActiveQTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\UnAativeQ.png");
	m_TextureList[FriendlyTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\friendly.png");
	m_TextureList[NeutralTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\neutral.png");
	m_TextureList[EnemyTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\Enemy.png");
	m_TextureList[MineTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\mine.png");
	m_TextureList[HerbTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\herb.png");
	m_TextureList[NullTexture] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NullMinimap.jpg");

	for (int i = MaskTexture; i < MaxCountTexture ; i++)
	{
		if (m_TextureList[i] == NULL)
		{
			ULOG("load MINIMAP Texture Failed!");
			return FALSE ;
		}
	}
	
	m_NpcFlagTextureList[NPC_TAXIVENDOR] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_TAXIVENDOR.png");
	m_NpcFlagTextureList[NPC_INNKEEPER] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_INNKEEPER.png");
	m_NpcFlagTextureList[NPC_BANKER] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_BANKER.png");
	m_NpcFlagTextureList[NPC_ARENACHARTER] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_ARENACHARTER.png");
	m_NpcFlagTextureList[NPC_BATTLEFIELDPERSON] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_BATTLEFIELDPERSON.png");
	m_NpcFlagTextureList[NPC_AUCTIONEER] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_AUCTIONEER.png");
	m_NpcFlagTextureList[NPC_GUILD_BANK] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_GUILD_BANK.png");
	m_NpcFlagTextureList[NPC_MAIL] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_MAIL.png");
	m_NpcFlagTextureList[NPC_TRANSPORTER] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_TRANSPORTER.png");
	m_NpcFlagTextureList[NPC_GUILD] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_GUILD.png");
	m_NpcFlagTextureList[NPC_VENDOR_ZAHUO] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_TAXIVENDOR.png");
	m_NpcFlagTextureList[NPC_VENDOR_YAOSHUI] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_VENDOR_YAOSHUI.png");
	m_NpcFlagTextureList[NPC_VENDOR_WUQI] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_VENDOR_WUQI.png");
	m_NpcFlagTextureList[NPC_VENDOR_FANGJU] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_VENDOR_FANGJU.png");
	m_NpcFlagTextureList[NPC_VENDOR_SHOUSHI] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_VENDOR_SHOUSHI.png");
	m_NpcFlagTextureList[NPC_VENDOR_ZUOQI] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_VENDOR_ZUOQI.png");
	m_NpcFlagTextureList[NPC_TRAINER_WUXIU] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_TRAINER_WUXIU.png");
	m_NpcFlagTextureList[NPC_TRAINER_YUJIAN] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_TRAINER_YUJIAN.png");
	m_NpcFlagTextureList[NPC_TRAINER_XIANDAO] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_TRAINER_XIANDAO.png");
	m_NpcFlagTextureList[NPC_TRAINER_ZHENWU] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_TRAINER_ZHENWU.png");
	m_NpcFlagTextureList[NPC_JIAYUAN] = sm_UiRender->LoadTexture("Data\\UI\\MiniMap\\NPC_JIAYUAN.png");


	for (int i = NPC_TAXIVENDOR; i < NPC_FLAG_TEXTURE_COUNT; i++ )
	{
		if (m_NpcFlagTextureList[i] == NULL)
		{
			ULOG("load MINIMAP Texture Failed!");
			return FALSE;
		}
	}

	return TRUE;
}
void UMinimap::ShutDownTextureList()
{
	for (int i = MaskTexture; i < MaxCountTexture; i++)
	{
		m_TextureList[i] = NULL ;
	}

	for (int i = NPC_TAXIVENDOR; i < NPC_FLAG_TEXTURE_COUNT; i++ )
	{
		m_NpcFlagTextureList[i] = NULL;	
	}
}
BOOL UMinimap::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (!InitTextureList())
	{
		return FALSE;
	}


	m_LocalPosX = 0.0f;
	m_LocalPosY = 0.0f;

	return TRUE;
}

void UMinimap::OnDestroy()
{
	ShutDownTextureList();
	UnloadCache();
	UControl::OnDestroy();
	m_kMapName = NULL;
}

void UMinimap::OnRender(const UPoint& offset, const URect &updateRect)
{
	URect mapRect = URect(updateRect.GetPosition(),updateRect.GetSize());
	DrawMinimap(offset, mapRect);
	RenderChildWindow(offset,updateRect);
}

#define MINIMAP_SIZE 256

//判断rec 是否是在圆圈内
BOOL UMinimap::CheckPos(const URect &updateRect, NiPoint3 kPos, float fXLeft, float fYTop)
{
	//UINT x = updateRect.GetWidth();
	//UINT y = updateRect.GetHeight();

	//椭圆 圆心
	URect kRect;
	kRect.pt0.x = int(updateRect.pt0.x + (kPos.x - fXLeft * TILE_SIZE) / (TILE_SIZE * m_fScale) * updateRect.GetWidth() - 8);
	kRect.pt1.x = kRect.pt0.x + 16;
	kRect.pt0.y = int(updateRect.pt0.y + (fYTop * TILE_SIZE - kPos.y) / (TILE_SIZE * m_fScale) * updateRect.GetHeight() - 8);
	kRect.pt1.y = kRect.pt0.y + 16;
		
	float CxPos = updateRect.left + updateRect.GetWidth() / 2.f;
	float CyPos = updateRect.top + updateRect.GetHeight() / 2.f;

	// 需要判断的点
	float xPos =  kRect.left + kRect.GetWidth() / 2.f ;
	float yPos =  kRect.top + kRect.GetHeight() / 2.f ;

	// 椭圆的长 的和短的半径
	float fRX=(updateRect.pt0.x - updateRect.pt1.x) / 2.f; 
	float fRY=(updateRect.pt1.y - updateRect.pt0.y) / 2.f;
	 
	if((xPos - CxPos) * (xPos - CxPos) / (fRX * fRX) +  (yPos - CyPos) * (yPos - CyPos)/ (fRY * fRY) <= 1.0f)
	{
		return TRUE;
	}

	return FALSE;

}
void UMinimap::GetDirectionPos(const URect &updateRect, const URect &rec, float &x, float &y, float &fAngle)
{
	//椭圆 圆心

	float CxPos = updateRect.left + updateRect.GetWidth() / 2.f;
	float CyPos = updateRect.top + updateRect.GetHeight() / 2.f;

	// 需要判断的点
	float xPos =  rec.left + rec.GetWidth() / 2.f ;
	float yPos =  rec.top + rec.GetHeight() / 2.f ;

	// 椭圆的长 的和短的半径
	float fRX=(updateRect.pt0.x - updateRect.pt1.x) / 2.f; 
	float fRY=(updateRect.pt1.y - updateRect.pt0.y) / 2.f;

	if ((xPos - CxPos) * (xPos - CxPos) / (fRX * fRX) +  (yPos - CyPos) * (yPos - CyPos)/ (fRY * fRY) <= 1.0f)
	{
		x = xPos;
		y = yPos;

		return ;
	}

	//
	float a1x = xPos - CxPos;
	float a1y = yPos - CyPos ;
	
	float tq = a1x / a1y ;

	float fRX2 = fRX * fRX;
	float fRY2 = fRY * fRY;
	
	float pa = 1.0f / (tq * tq / fRX2 + 1.0f / fRY2);

	//如果点在椭圆外，求直线与椭圆相交点
	//求解了2个点。和椭圆相交 （x1, y1） 和 (x2, y2)
	//判断并返回在点与圆心之间的点
	
	if(a1x * a1x / fRX2 +  (a1y * a1y)/ fRY2 >= 1.0f)
	{
		float y1 = CyPos + sqrt(pa);
		float y2 = CyPos - sqrt(pa);

		float x1 = y1 * tq + CxPos - tq * CyPos;
		float x2 = y2 * tq + CxPos - tq * CyPos ;

		float Asin = a1y / sqrt(a1x * a1x + a1y * a1y);

		fAngle = asin(Asin);
		if (CxPos > xPos)
		{
			fAngle = NI_PI  - fAngle ;
		}
		

		//float xMax = max(CxPos, xPos);
		//float xMin = min(CxPos, xPos);
		float  fLen1 = (xPos - x1) * (xPos - x1) + (yPos - y1) * (yPos - y1);
		float  fLen2 = (xPos - x2) * (xPos - x2) + (yPos - y2) * (yPos - y2);
		if (fLen1 < fLen2)
		{
			x = x1;
			y = y1;
		}else
		{
			x = x2;
			y = y2;
		}
		
	}
}
void UMinimap::DrawDirection(NiPoint3 kPos, const URect &updateRect, float fXLeft, float fYTop ,UINT Index)
{
	URect kRect;
	kRect.pt0.x = int(updateRect.pt0.x + (kPos.x - fXLeft * TILE_SIZE) / (TILE_SIZE * m_fScale) * updateRect.GetWidth() - 8);
	kRect.pt1.x = kRect.pt0.x + 16;
	kRect.pt0.y = int(updateRect.pt0.y + (fYTop * TILE_SIZE - kPos.y) / (TILE_SIZE * m_fScale) * updateRect.GetHeight() - 8);
	kRect.pt1.y = kRect.pt0.y + 16;

	if (!CheckPos(updateRect, kPos,fXLeft,fYTop))
	{
		float x = 0.f;
		float y = 0.f;
		float angle = 0.0f;

		GetDirectionPos(updateRect, kRect, x , y, angle);

		URect DIRrec ;
		DIRrec.Set(x - 10, y - 10 , 20, 20);
		if (Index >= MaskTexture && Index < MaxCountTexture)
		{
			sm_UiRender->DrawRotatedImage(angle + NI_PI / 2.0f, m_TextureList[Index], DIRrec, URect(0,0,32,32));
		}
	}
}
void UMinimap::DrawFlag(const URect &updateRect, float fXLeft, float fYTop)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	if( !pkPlayer )
		return;

	std::vector<ui32> MapIDVec;
	std::vector<NiPoint3> PosVEC;
	PosVEC.clear();
	MapIDVec.clear();
	int curMapID = CMap::Get()->GetMapId();

	if (SocialitySys && SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->GetTeamMemPosition(curMapID, PosVEC);
	}
	
	for (size_t i = 0; i < PosVEC.size(); i++)
	{
		DrawDirection(PosVEC[i],updateRect,fXLeft,fYTop, TeamArrow);
	}

	if(m_bDrawFindObjPos)
	{
		NiPoint3 kPos = m_kFindObjPos;
		if (CheckPos(updateRect,kPos,fXLeft,fYTop))
		{
			NiPoint3 LocalPos = pkPlayer->GetPosition();
			if ((LocalPos - kPos).Length() >= MaxLenToNPC)
			{
				URect kRect;
				kRect.pt0.x = int(updateRect.pt0.x + (kPos.x - fXLeft * TILE_SIZE) / (TILE_SIZE * m_fScale) * updateRect.GetWidth() - 8);
				kRect.pt1.x = kRect.pt0.x + 16;
				kRect.pt0.y = int(updateRect.pt0.y + (fYTop * TILE_SIZE - kPos.y) / (TILE_SIZE * m_fScale) * updateRect.GetHeight() - 8);
				kRect.pt1.y = kRect.pt0.y + 16;

				//sm_UiRender->DrawImage(m_TextureList[SeeOBJArrow], kRect,MASTER_FN_COLOR);
				AddRenderItem(SeeOBJArrow, kRect, MASTER_FN_COLOR);
			}else
			{
				m_bDrawFindObjPos = false;
				m_kFindObjPos = NiPoint3::ZERO ;
				FindOBJMapId = 0;
			}


		}else
		{
			if (curMapID == FindOBJMapId)
			{
				DrawDirection(kPos,updateRect,fXLeft,fYTop,FindOBJArrow);
			}
		}
	}
}
void UMinimap::DrawScenceObj(const URect &updateRect, float fXLeft, float fYTop)
{
	//return;
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	if( !pkPlayer )
		return;

	// Scene obj
	const stdext::hash_set<UpdateObj*>& temp( ObjectMgr->GetObjectByType(TYPEID_GAMEOBJECT) );

	for( stdext::hash_set<UpdateObj*>::const_iterator it = temp.begin(); it != temp.end(); ++it )
	{
		UpdateObj* p = *it;

		if(p == pkPlayer)
			continue;

		CSceneGameObj* pkCreature = static_cast<CSceneGameObj*>(p);
		if (!pkCreature || pkCreature->GetGameObjectType() != GOT_SCENEOBJECT || !pkCreature->CanSeeCollection() )
		{
			continue ;
		}
		const NiPoint3& kPos = pkCreature->GetPosition();
		URect kRect;
		if (GetRenderScreenRect(kPos, updateRect, fXLeft, fYTop, 16, kRect))
		{
			if ( pkCreature->GetReqSpell() == 430000010 /*&& pkPlayer->*/ )
			{
				AddRenderItem(MineTexture, kRect, 0xffffffff, 0 ,pkCreature);
			}

			if ( pkCreature->GetReqSpell() == 430001010 )
			{
				AddRenderItem(HerbTexture, kRect, 0xffffffff, 0 ,pkCreature);
			}
			//AddRenderItem(DotTexture, kRect, SCENE_OBJ);
		}
	}
}
void UMinimap::DrawObject(const URect &updateRect, float fXLeft, float fYTop)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	if( !pkPlayer )
		return;

	URect kRect;
	//Npc
	const stdext::hash_set<UpdateObj*>& Creature(  ObjectMgr->GetObjectByType(TYPEID_UNIT));
	for( stdext::hash_set<UpdateObj*>::const_iterator it = Creature.begin(); it != Creature.end(); ++it )
	{
		UpdateObj* p = *it;
		if(p == pkPlayer)
			continue;

		CCreature* pkCreature = static_cast<CCreature*>(p);
		if (!pkCreature || pkCreature->GetGameObjectType() != GOT_CREATURE || pkCreature->IsDead())
		{
			if (pkCreature->IsDead())
			{
				std::map<ui64, RenderItem>::iterator itr = m_ObjectRenderList.find(pkCreature->GetGUID());
				if(itr != m_ObjectRenderList.end())
				{
					m_ObjectRenderList.erase(itr);
				}
			}
			continue ;
		}
		const NiPoint3& kPos = pkCreature->GetPosition();
		if (!GetRenderScreenRect(kPos, updateRect, fXLeft, fYTop, 16, kRect))
		{	
			std::map<ui64, RenderItem>::iterator itr = m_ObjectRenderList.find(pkCreature->GetGUID());
			if(itr != m_ObjectRenderList.end())
			{
				m_ObjectRenderList.erase(itr);
			}
			continue;
		}

		if (pkPlayer->IsHostile(pkCreature))  //敌对
		{
			if (m_ShowFlag & CREATURE_ALL)  //所有怪物。 敌对势力都是怪物 没有NPC区分
			{
				//AddRenderItem(DotTexture, kRect, MASTER_HOSTILITY_ATTACK, 0 ,pkCreature);
				AddRenderItem(EnemyTexture, kRect, 0xffffffff, 0 ,pkCreature);
			}			
		}else   //友好
		{
			CCreature::eQuestFlag pQuestFlag = pkCreature->GetQuestFlag();

			if (pQuestFlag == CCreature::eFinish) //任务完成
			{
				AddRenderItem(FinishQTexture, kRect, 0xffffffff, 0 ,pkCreature);
			}else if (pQuestFlag == CCreature::eStart)
			{
				AddRenderItem(CanGetQTexture, kRect, 0xffffffff, 0 ,pkCreature);
			}else 
			{
				ui32 pkCreatureFlag = pkCreature->GetUInt32Value(UNIT_NPC_FLAGS);
				if (pkCreatureFlag & UNIT_NPC_FLAG_NONE) 
				{
					if (m_ShowFlag & CREATURE_ALL) //中立怪物
					{
						//AddRenderItem(DotTexture, kRect, MASTER_HOSTILITY_UNATTACK, 0 ,pkCreature);
						AddRenderItem(NeutralTexture, kRect, 0xffffffff, 0 ,pkCreature);
					}

				}else
				{
					if (m_ShowFlag & NPC_FRIENDLY) // 友好和中立NPC
					{
						if (pkCreatureFlag & UNIT_NPC_FLAG_TAXIVENDOR)
						{
							//飞行管理员
							AddRenderItem(NPC_TAXIVENDOR, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_INNKEEPER)
						{
							//旅店老板
							AddRenderItem(NPC_INNKEEPER, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_BANKER)
						{
							//银行老板
							AddRenderItem(NPC_BANKER, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_ARENACHARTER)
						{
							//竞技场管理员
							AddRenderItem(NPC_ARENACHARTER, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_BATTLEFIELDPERSON)
						{
							//战场管理员
							AddRenderItem(NPC_BATTLEFIELDPERSON, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_AUCTIONEER)
						{
							//拍卖行管理员
							AddRenderItem(NPC_AUCTIONEER, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_GUILD_BANK)
						{
							//工会银行
							AddRenderItem(NPC_GUILD_BANK, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_MAIL)
						{
							//邮件管理员
							AddRenderItem(NPC_MAIL, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_TRANSPORTER)
						{
							//传送
							AddRenderItem(NPC_TRANSPORTER, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_GUILD)
						{
							//部族管理员
							AddRenderItem(NPC_GUILD, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_VENDOR_ZAHUO)
						{
							//杂货
							AddRenderItem(NPC_VENDOR_ZAHUO, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_VENDOR_YAOSHUI)
						{
							//药水
							AddRenderItem(NPC_VENDOR_YAOSHUI, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_VENDOR_WUQI)
						{
							//装备
							AddRenderItem(NPC_VENDOR_WUQI, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_VENDOR_FANGJU)
						{
							//防具
							AddRenderItem(NPC_VENDOR_FANGJU, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_VENDOR_SHOUSHI)
						{
							//首饰
							AddRenderItem(NPC_VENDOR_SHOUSHI, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_VENDOR_ZUOQI)
						{
							//坐骑
							AddRenderItem(NPC_VENDOR_ZUOQI, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_TRAINER_WUXIU)
						{
							//武修
							AddRenderItem(NPC_TRAINER_WUXIU, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_TRAINER_YUJIAN)
						{
							//羽箭
							AddRenderItem(NPC_TRAINER_YUJIAN, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_TRAINER_XIANDAO)
						{
							//仙道
							AddRenderItem(NPC_TRAINER_XIANDAO, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_TRAINER_ZHENWU)
						{
							//真巫
							AddRenderItem(NPC_TRAINER_ZHENWU, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else if (pkCreatureFlag & UNIT_NPC_FLAG_JIAYUAN)
						{
							//家园
							AddRenderItem(NPC_JIAYUAN, kRect, 0xFFFFFFFF, 1 ,pkCreature);
						}else
						{
							//AddRenderItem(DotTexture, kRect, MASTER_FN_COLOR ,0,pkCreature);
							if (pkPlayer->IsFriendly(pkCreature) )
							{
								AddRenderItem(FriendlyTexture, kRect, 0xffffffff, 0 ,pkCreature);
							}
							else
							{
								AddRenderItem(NeutralTexture, kRect, 0xffffffff, 0 ,pkCreature);
							}
							
						}
					}
				}

			}
		}	
	}
	//是否显示玩家 

	const stdext::hash_set<UpdateObj*>& temp3( ObjectMgr->GetObjectByType(TYPEID_PLAYER) );

	for( stdext::hash_set<UpdateObj*>::const_iterator it = temp3.begin(); it != temp3.end(); ++it )
	{
		UpdateObj* p = *it;
		if(p == pkPlayer)
			continue;

		CPlayer* otherPlayer = static_cast<CPlayer*>(p);
		const NiPoint3& kPos = otherPlayer->GetPosition();
		if (!GetRenderScreenRect(kPos, updateRect, fXLeft, fYTop, 16, kRect))
		{
			std::map<ui64, RenderItem>::iterator itr = m_ObjectRenderList.find(otherPlayer->GetGUID());
			if(itr != m_ObjectRenderList.end())
			{
				m_ObjectRenderList.erase(itr);
			}
			continue;
		}
		if (pkPlayer->IsFriendly(otherPlayer))
		{
			if (SocialitySys->GetTeamSysPtr()->InTeam(otherPlayer->GetGUID()))
			{
				/*URect FRec = kRect;
				FRec.pt0.x -= 4;
				FRec.pt1.x += 8;
				FRec.pt0.y -= 4;
				FRec.pt1.y += 8;
				sm_UiRender->DrawImage(m_TextureList[DotTexture], FRec, TEAM_COLOR);*/
				AddRenderItem(PlayerTexture, kRect, TEAM_UNPVP_COLOR, 0, otherPlayer);
			}else
			{
				if (m_ShowFlag & PLAYER_FRIENDLY)  //是否显示本方玩家 队友是始终显示的
				{
					//AddRenderItem(DotTexture, kRect, TEAM_UNPVP_COLOR, 0, otherPlayer);
					AddRenderItem(OtherPlayerTexture, kRect, TEAM_UNPVP_COLOR, 0, otherPlayer);
					
				}
			}
		}else
		{	
			//敌对势力，红点
			//AddRenderItem(DotTexture, kRect, HOSTILITY_PLAYER_COLOR, 0, otherPlayer);
			AddRenderItem(EnemyTexture, kRect, 0xffffffff, 0 ,otherPlayer);
		}
	}
}
int UMinimap::findSide(int iX, int iY)
{
	for (int i = 0 ; i < 4 ; i++)
	{
		if (iX == m_kMiniMapRect[i].m_iX
			&& iY == m_kMiniMapRect[i].m_iY)
		{
			return i;
		}
	}
	return -1;
}

BOOL UMinimap::GetRenderScreenRect(NiPoint3 kPos ,const URect& updateRect,float fXLeft, float fYTop,int  drawSize,URect& kRect)
{
	//CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	//if( !pkPlayer )
	//	return FALSE;
	//NiPoint3 LocalPos( pkPlayer->GetPosition() );

	//float fw = updateRect.GetWidth() * 0.5 - ( LocalPos.x - kPos.x ) / (TILE_SIZE * m_fScale) * updateRect.GetWidth();
	//float fh = updateRect.GetHeight() * 0.5 - ( kPos.y - LocalPos.y ) / (TILE_SIZE * m_fScale) * updateRect.GetHeight();

	////float fw = (kPos.x - fXLeft * TILE_SIZE) / (TILE_SIZE * m_fScale) * updateRect.GetWidth() + 0.49f;
	////float fh = (fYTop * TILE_SIZE - kPos.y) / (TILE_SIZE * m_fScale) * updateRect.GetHeight() + 0.49f;
	//kRect.pt0.x = int(float(updateRect.pt0.x) - drawSize * 0.5f + fw + 0.49f);
	//kRect.pt1.x = kRect.pt0.x + drawSize;
	//kRect.pt0.y = int(float(updateRect.pt0.y) - drawSize * 0.5f + fh + 0.49f);
	//kRect.pt1.y = kRect.pt0.y + drawSize;

	int iX = TILE_INDEX(kPos.x);
	int iY = TILE_INDEX(kPos.y);
	int corrX =  int(kPos.x - TILE_SIZE * iX);//常数
	int corrY =  int(TILE_SIZE * (iY+1) - kPos.y);//常数
	int Side = findSide(iX, iY);
	if (Side != -1)
	{
		corrX -= m_kMiniMapRect[Side].m_kRect.pt0.x;
		corrY -= m_kMiniMapRect[Side].m_kRect.pt0.y;

		kRect.pt0.x = m_kMiniMapRect[Side].m_kRenderRect.pt0.x + corrX * ( 0.3f / m_fScale ) - drawSize * 0.5f;
		kRect.pt1.x = kRect.pt0.x + drawSize;
		kRect.pt0.y = m_kMiniMapRect[Side].m_kRenderRect.pt0.y + corrY * ( 0.3f / m_fScale ) - drawSize * 0.5f;
		kRect.pt1.y = kRect.pt0.y + drawSize;
		return CheckPos(updateRect,kPos,fXLeft, fYTop);
	}
	return FALSE;
}
void UMinimap::DrawPathFind(const URect& updateRect ,float fXLeft, float fYTop)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		std::deque<NiPoint3> Temp;
		if (pkLocal->GetPathPoint(Temp))
		{

			size_t tSize = Temp.size();
			if (!tSize)
			{
				return ;
			}

			//渲染玩家到第一个寻路点
			URect localRec ;
			GetRenderScreenRect(pkLocal->GetPosition(), updateRect, fXLeft, fYTop, 2,localRec);
			URect Rec0 ;
			UPoint Pos0 ;
			if (GetRenderScreenRect(Temp[0], updateRect, fXLeft, fYTop, 2,Rec0))
			{
				Pos0 = Rec0.GetPosition();
			}else
			{
				//如果点在圆圈外, 找到切点使用
				float x0 , y0, angel0;
				GetDirectionPos(updateRect, Rec0, x0,y0,angel0);
				Pos0.x = x0;
				Pos0.y = y0;
			}
			if (Pos0 != localRec.GetPosition())
			{
				sm_UiRender->DrawLine(Pos0,localRec.GetPosition() , 0xff00ff00);
			}

			
			//渲染第一个寻路点到目标寻路点
			for (int i =0; i < tSize - 1; i++)
			{
				URect kRect1;
				URect kRect2;
				UPoint TPos ;
				UPoint NextPos ;
				bool bInMiniMap = GetRenderScreenRect(Temp[i], updateRect, fXLeft, fYTop, 2,kRect1);
				bool bNextInMiniMap = GetRenderScreenRect(Temp[i+1], updateRect, fXLeft, fYTop, 2,kRect2);
				if (bInMiniMap || bNextInMiniMap)
				{
					float x , y, angel;
					if (bInMiniMap)
					{
						TPos = kRect1.GetPosition();
						URect RendeRec ;
						RendeRec.SetPosition(kRect1.GetPosition() - UPoint(6,6));
						RendeRec.SetSize(kRect1.GetSize() + UPoint(7,7));
						//sm_UiRender->DrawImage(m_TextureList[DotTexture], RendeRec,0xff00ff00);
						AddRenderItem(DotTexture, RendeRec, HOSTILITY_PLAYER_COLOR);
					}else
					{
						GetDirectionPos(updateRect, kRect1, x,y,angel);
						TPos.x = x;
						TPos.y = y;
					}
					
					if (bNextInMiniMap)
					{
						NextPos = kRect2.GetPosition();
					}else
					{
						//如果点在圆圈外, 找到切点使用	 
						GetDirectionPos(updateRect, kRect2, x,y,angel);
						NextPos.x = x;
						NextPos.y = y;
					}
					sm_UiRender->DrawLine(NextPos, TPos, 0xff00ff00);
				}
			}
		}
	}
}

void UMinimap::UpdateLocalPlayer()
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	if( !pkPlayer )
		return;

	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	if (m_kMapName != pkMap->GetMapName())
	{
		OnMapChanged();
	}


	if (NiAbs(m_LocalPosX - pkPlayer->GetPosition().x) > 0.5f)
		m_LocalPosX = int(pkPlayer->GetPosition().x);

	if (NiAbs(m_LocalPosY - pkPlayer->GetPosition().y) > 0.5f)
		m_LocalPosY = int(pkPlayer->GetPosition().y);
}

void UMinimap::DrawMinimap(UPoint offset, const URect &updateRect)
{
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	if( !pkPlayer )
		return;

	IUTexture* pkTexture;
	float fXCenter = m_LocalPosX / TILE_SIZE;
	float fYCenter = m_LocalPosY / TILE_SIZE;

	//设定坐标
	char buf[100];
	NiSprintf(buf,100,"%d . %d",(int)(fXCenter*TILE_SIZE), (int)(fYCenter*TILE_SIZE));
	m_MapCoordinate->SetText(buf);

	float fXLeft = fXCenter - m_fScale*0.5f;
	float fXRight = fXCenter + m_fScale*0.5f;

	float fYTop = fYCenter + m_fScale*0.5f;
	float fYBottom = fYCenter - m_fScale*0.5f;


	float texcoords[4];
	float alphaTexcoords[4];
	float destRect[4];
	int iX = 0;
	int iY = 0;

	//Left Top
	iX = TILE_INDEX(fXLeft*TILE_SIZE);
	iY = TILE_INDEX(fYTop*TILE_SIZE);
	pkTexture = LoadTexture(iX, iY);
	if( pkTexture )
	{
		texcoords[0] = NiFmod(fXLeft, 1.f);
		texcoords[1] = (1.f - NiFmod(fYTop, 1.f));
		texcoords[2] = texcoords[0] + m_fScale;
		texcoords[3] = texcoords[1] + m_fScale;

		if( texcoords[2] > 1.f )
			texcoords[2] = 1.f;

		if( texcoords[3] > 1.f )
			texcoords[3] = 1.f;

		destRect[0] = float(updateRect.pt0.x);
		destRect[1] = float(updateRect.pt0.y);
		destRect[2] = destRect[0] + float(updateRect.GetWidth() * ((texcoords[2] - texcoords[0]) / m_fScale));
		destRect[3] = destRect[1] + float(updateRect.GetHeight() * ((texcoords[3] - texcoords[1]) / m_fScale));

		alphaTexcoords[0] = 0.f;
		alphaTexcoords[1] = 0.f;
		alphaTexcoords[2] = (texcoords[2] - texcoords[0]) / m_fScale;
		alphaTexcoords[3] = (texcoords[3] - texcoords[1]) / m_fScale;

		SetMiniMapInfo(0, iX, iY, texcoords, destRect);
		sm_UiRender->DrawMaskImage(pkTexture, texcoords, m_TextureList[MaskTexture], alphaTexcoords, destRect);
	}

	//Left Bottom
	iX = TILE_INDEX(fXLeft*TILE_SIZE);
	iY = TILE_INDEX(fYBottom*TILE_SIZE);
	pkTexture = LoadTexture(iX, iY);
	if( pkTexture )
	{
		texcoords[0] = NiFmod(fXLeft, 1.f);
		texcoords[1] = (1.f - NiFmod(fYTop, 1.f));
		texcoords[2] = texcoords[0] + m_fScale;
		texcoords[3] = texcoords[1] + m_fScale;

		if( texcoords[2] > 1.f )
			texcoords[2] = 1.f;

		if( texcoords[3] > 1.f )
		{
			texcoords[1] = 0.f;
			texcoords[3] -= 1.f;
		}

		destRect[0] = float(updateRect.pt0.x);
		destRect[1] = float(updateRect.pt0.y + updateRect.GetHeight() * (1.f - (texcoords[3] - texcoords[1]) / m_fScale));
		destRect[2] = float(updateRect.pt0.x + updateRect.GetWidth() * ((texcoords[2] - texcoords[0]) / m_fScale));
		destRect[3] = float(updateRect.pt1.y);

		alphaTexcoords[0] = 0.f;
		alphaTexcoords[1] = 1.f - (texcoords[3] - texcoords[1]) / m_fScale;
		alphaTexcoords[2] = (texcoords[2] - texcoords[0]) / m_fScale;
		alphaTexcoords[3] = 1.f;

		SetMiniMapInfo(1, iX, iY, texcoords, destRect);
		sm_UiRender->DrawMaskImage(pkTexture, texcoords, m_TextureList[MaskTexture], alphaTexcoords, destRect);
	}

	//Right Top
	iX = TILE_INDEX(fXRight*TILE_SIZE);
	iY = TILE_INDEX(fYTop*TILE_SIZE);
	pkTexture = LoadTexture(iX, iY);
	if( pkTexture )
	{
		texcoords[0] = NiFmod(fXLeft, 1.f);
		texcoords[1] = (1.f - NiFmod(fYTop, 1.f));
		texcoords[2] = texcoords[0] + m_fScale;
		texcoords[3] = texcoords[1] + m_fScale;

		if( texcoords[2] > 1.f )
		{
			texcoords[0] = 0.f;
			texcoords[2] -= 1.f;
		}

		if( texcoords[3] >= 1.f )
		{
			texcoords[3] = 1.f;
		}

		destRect[0] = float(updateRect.pt0.x + updateRect.GetWidth() * (1.f - (texcoords[2] - texcoords[0]) / m_fScale));
		destRect[1] = float(updateRect.pt0.y);
		destRect[2] = float(updateRect.pt1.x);
		destRect[3] = float(updateRect.pt0.y + updateRect.GetHeight() * ((texcoords[3] - texcoords[1]) / m_fScale));

		alphaTexcoords[0] = 1.f - (texcoords[2] - texcoords[0]) / m_fScale;
		alphaTexcoords[1] = 0.f;
		alphaTexcoords[2] = 1.f;
		alphaTexcoords[3] = (texcoords[3] - texcoords[1]) / m_fScale;

		SetMiniMapInfo(2, iX, iY, texcoords, destRect);
		sm_UiRender->DrawMaskImage(pkTexture, texcoords, m_TextureList[MaskTexture], alphaTexcoords, destRect);
	}

	//Right Bottom
	iX = TILE_INDEX(fXRight*TILE_SIZE);
	iY = TILE_INDEX(fYBottom*TILE_SIZE);
	pkTexture = LoadTexture(iX, iY);
	if( pkTexture )
	{
		texcoords[0] = NiFmod(fXLeft, 1.f);
		texcoords[1] = (1.f - NiFmod(fYTop, 1.f));
		texcoords[2] = texcoords[0] + m_fScale;
		texcoords[3] = texcoords[1] + m_fScale;

		if( texcoords[2] > 1.f )
		{
			texcoords[0] = 0.f;
			texcoords[2] -= 1.f;
		}

		if( texcoords[3] > 1.f )
		{
			texcoords[1] = 0.f;
			texcoords[3] -= 1.f;
		}

		destRect[0] = float(updateRect.pt0.x + updateRect.GetWidth() * (1.f - (texcoords[2] - texcoords[0]) / m_fScale));
		destRect[1] = float(updateRect.pt0.y + updateRect.GetHeight() * (1.f - (texcoords[3] - texcoords[1]) / m_fScale));
		destRect[2] = float(updateRect.pt1.x);
		destRect[3] = float(updateRect.pt1.y);

		alphaTexcoords[0] = 1.f - (texcoords[2] - texcoords[0]) / m_fScale;
		alphaTexcoords[1] = 1.f - (texcoords[3] - texcoords[1]) / m_fScale;
		alphaTexcoords[2] = 1.f;
		alphaTexcoords[3] = 1.f;

		SetMiniMapInfo(3, iX, iY, texcoords, destRect);
		sm_UiRender->DrawMaskImage(pkTexture, texcoords, m_TextureList[MaskTexture], alphaTexcoords, destRect);
	}

	URect ScreenRect;
	URect ImageRect;


	//if(bCalPosition)
	{
		//DrawPathFind(updateRect,fXLeft,fYTop);
		DrawObject(updateRect,fXLeft,fYTop);
		DrawScenceObj(updateRect,fXLeft,fYTop);
		DrawFlag(updateRect,fXLeft,fYTop);
		bCalPosition = false;
	}
	std::map<ui64, RenderItem>::iterator it = m_ObjectRenderList.begin();
	while( it != m_ObjectRenderList.end())
	{
		if(!ObjectMgr->GetObject(it->first))
		{
			it = m_ObjectRenderList.erase(it);
			continue;
		}
		RenderItem* pkRenderItem = &it->second;
		if(pkRenderItem->TextureType)
			sm_UiRender->DrawImage(m_NpcFlagTextureList[pkRenderItem->TextureId], pkRenderItem->DrawRect, pkRenderItem->color);
		else
			sm_UiRender->DrawImage(m_TextureList[pkRenderItem->TextureId], pkRenderItem->DrawRect, pkRenderItem->color);
		++it;
	}
	//角色的箭头
	ScreenRect.pt0.x = (updateRect.pt0.x + updateRect.pt1.x)/2 - 16;
	ScreenRect.pt0.y = (updateRect.pt0.y + updateRect.pt1.y)/2 - 16;
	ScreenRect.pt1.x = (updateRect.pt0.x + updateRect.pt1.x)/2 + 16;
	ScreenRect.pt1.y = (updateRect.pt0.y + updateRect.pt1.y)/2 + 16;

	ImageRect.pt0.x = 0;
	ImageRect.pt0.y = 0;
	ImageRect.pt1.x = 32;
	ImageRect.pt1.y = 32;

	

	//角色面向的是-Y
	sm_UiRender->DrawRotatedImage(pkPlayer->GetRotate().z + NI_PI, m_TextureList[ArrowTexture], ScreenRect, ImageRect);
}

void UMinimap::SetMiniMapInfo(int Side, int iX, int iY, const float* texcoord, const float* destRect)
{
	m_kMiniMapRect[Side].m_iX = iX;
	m_kMiniMapRect[Side].m_iY = iY;

	m_kMiniMapRect[Side].m_kRect.pt0.x = texcoord[0] * TILE_SIZE;
	m_kMiniMapRect[Side].m_kRect.pt0.y = texcoord[1] * TILE_SIZE;
	m_kMiniMapRect[Side].m_kRect.pt1.x = texcoord[2] * TILE_SIZE;
	m_kMiniMapRect[Side].m_kRect.pt1.y = texcoord[3] * TILE_SIZE;

	m_kMiniMapRect[Side].m_kRenderRect.pt0.x = destRect[0];
	m_kMiniMapRect[Side].m_kRenderRect.pt0.y = destRect[1];
	m_kMiniMapRect[Side].m_kRenderRect.pt1.x = destRect[2];
	m_kMiniMapRect[Side].m_kRenderRect.pt1.y = destRect[3];
}

void UMinimap::UnloadCache()
{
	for( int i = 0; i < CACHE_SIZE; i++ )
	{
		m_kCacheTexture[i].m_kTexture = NULL;
	}
}


IUTexture* UMinimap::LoadTexture(int iX, int iY)
{
	int iBestIdx = -1;
	int iBest = 0;

	for( int i = 0; i < CACHE_SIZE; i++ )
	{
		if( m_kCacheTexture[i].m_iX == iX
			&& m_kCacheTexture[i].m_iY == iY )
		{
			if (m_kCacheTexture[i].m_kTexture == NULL)
			{

				char acFilename[1024];
				NiSprintf(acFilename, sizeof(acFilename), "Data\\World\\MiniMaps\\%s\\%s_%d_%d.jpg", (const char*)m_kMapName, (const char*)m_kMapName, iX, iY);

				m_kCacheTexture[i].m_iX = iX;
				m_kCacheTexture[i].m_iY = iY;
				m_kCacheTexture[i].m_kTexture = sm_UiRender->LoadTexture(acFilename);
				if(m_kCacheTexture[i].m_kTexture == NULL)
					m_kCacheTexture[i].m_kTexture = m_TextureList[NullTexture];
			}

			return m_kCacheTexture[i].m_kTexture;
		}

		if( m_kCacheTexture[i].m_kTexture == NULL )
		{
			iBestIdx = i;
			iBest = 9999;
		}
		else if( iBest < abs( m_kCacheTexture[i].m_iX - iX ) + abs( m_kCacheTexture[i].m_iY - iY ) )
		{
			iBest = abs( m_kCacheTexture[i].m_iX - iX ) + abs( m_kCacheTexture[i].m_iY - iY );
			iBestIdx = i;
		}
	}

	NIASSERT( iBest >= 4 );

	char acFilename[1024];
	NiSprintf(acFilename, sizeof(acFilename), "Data\\World\\MiniMaps\\%s\\%s_%d_%d.jpg", (const char*)m_kMapName, (const char*)m_kMapName, iX, iY);

	m_kCacheTexture[iBestIdx].m_iX = iX;
	m_kCacheTexture[iBestIdx].m_iY = iY;
	m_kCacheTexture[iBestIdx].m_kTexture = sm_UiRender->LoadTexture(acFilename);
	if(m_kCacheTexture[iBestIdx].m_kTexture == NULL)
		m_kCacheTexture[iBestIdx].m_kTexture = m_TextureList[NullTexture];

	return m_kCacheTexture[iBestIdx].m_kTexture;
}

IUTexture* UMinimap::LoadTexture(float fX, float fY)
{
	int iX = TILE_INDEX(fX);
	int iY = TILE_INDEX(fY);

	return LoadTexture(iX, iY);
}
void UMinimap::SetMapName(const char* name)
{
	m_MapName->SetText(name);
}
void UMinimap::OnMapChanged()
{
	UnloadCache();

	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	m_kMapName = pkMap->GetMapName();


	//更换地图名称
	CMapInfoDB::MapInfoEntry stMapInfoEntry;
	if (g_pkMapInfoDB->GetMapInfo(pkMap->GetMapId(), stMapInfoEntry))
		m_MapName->SetText(stMapInfoEntry.desc.c_str());
	else
		m_MapName->Clear();
}
void UMinimap::OnTimer(float fDeltaTime)
{
	static float s_fLastTime = 0;
    float fTime = NiGetCurrentTimeInSec();
	CPlayer* pkPlayer = ObjectMgr->GetLocalPlayer();
	if(fTime - s_fLastTime > 0.5f)
	{
		bCalPosition = true;
		s_fLastTime = fTime;
	}
	UpdateLocalPlayer();
	UControl::OnTimer(fDeltaTime);
}

BOOL  UMinimap::ZoomMap()        //放大
{
	m_fScale -= 0.05f;
	m_fScale = NiClamp(m_fScale, 0.30f, 0.6f);
	if (m_fScale == 0.30f)
	{
		return FALSE;
	}
	return TRUE;
}
BOOL  UMinimap::ReduceMap()      //缩小
{
	m_fScale += 0.05f;
	m_fScale = NiClamp(m_fScale, 0.30f, 0.6f);

	if (m_fScale == 0.6f)
	{
		return FALSE ;
	}
	return TRUE;
}

void UMinimap::SetDrawFindOBJPos(bool bDraw, NiPoint3 kPos /* = NiPoint3::ZERO */)
{
	m_bDrawFindObjPos = bDraw;

	m_kFindObjPos = kPos;

	FindOBJMapId = CMap::Get()->GetMapId();
}

void UMinimap::AddRenderItem(int TextureId, URect& DrawRect, DWORD color,int TextureType, UpdateObj* obj)
{
	ui64 guid = 0;
	if (obj)
	{
		guid = obj->GetGUID();
	}
	if (!guid)
		return;
	std::map<ui64, RenderItem>::iterator it = m_ObjectRenderList.find(guid);
	if (it != m_ObjectRenderList.end())
	{
		it->second.DrawRect = DrawRect;
		it->second.color = color;
		it->second.TextureType = TextureType;
		it->second.TextureId = TextureId;
	}
	else
	{
		RenderItem NewItem;
		NewItem.TextureId = TextureId;
		NewItem.DrawRect = DrawRect;
		NewItem.color = color;
		NewItem.TextureType = TextureType;
		m_ObjectRenderList.insert(std::map<ui64, RenderItem>::value_type(guid, NewItem));
	}
}

bool UMinimap::GetObjectNameByPosition(UPoint pos, std::vector<std::string>& vName)
{
	std::map<ui64, RenderItem>::iterator it = m_ObjectRenderList.begin();
	while(it != m_ObjectRenderList.end())
	{
		if (it->second.DrawRect.PointInRect(pos))
		{
			CCharacter* pChar = (CCharacter*)ObjectMgr->GetObject(it->first);
			if (pChar)
			{
				std::string temp = pChar->GetName();
				vName.push_back(temp);
			}
		}
		++it;
	}
	return vName.size() > 0;
}
//void UMinimap::AddRenderItem(UpdateObj* obj, int TextureId, URect& DrawRect, DWORD color,int TextureType )
//{
//	ui64 guid = obj->GetGUID();
//	CGameObject* pGameObj =  static_cast<CGameObject*>(obj);
//	if (!pGameObj && !guid)
//		return;
//
//	std::map<ui64, RenderItem>::iterator it = m_ObjectRenderList.find(guid);
//	if (it != m_ObjectRenderList.end())
//	{
//		it->second.Position = pGameObj->GetPosition();
//		//if ((DrawRect.GetPosition() - it->second.DrawRect.GetPosition()).Length() > 1.414f)
//		{
//			it->second.DrawRect = DrawRect;
//		}
//		//else
//		{
//			//it->second.DrawRect.SetWidth(DrawRect.GetWidth() + NiAbs(DrawRect.GetPosition().x - it->second.DrawRect.GetPosition().x));
//			//it->second.DrawRect.SetHeight(DrawRect.GetHeight()+ NiAbs(DrawRect.GetPosition().y - it->second.DrawRect.GetPosition().y));
//		}
//		it->second.color = color;
//		it->second.TextureType = TextureType;
//		it->second.TextureId = TextureId;
//	}
//	else
//	{
//		RenderItem NewItem;
//		NewItem.TextureId = TextureId;
//		NewItem.DrawRect = DrawRect;
//		NewItem.color = color;
//		NewItem.TextureType = TextureType;
//		m_ObjectRenderList.insert(std::map<ui64, RenderItem>::value_type(guid, NewItem));
//	}
//}