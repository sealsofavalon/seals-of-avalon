#pragma once
#include "UInc.h"
#include <hash_set>
#include "ObjectManager.h"

#define CACHE_SIZE 16
//这只的颜色列表
#define  TEAM_COLOR					0xFF65FFFF   //队友
#define  TEAM_UNPVP_COLOR           0xFF3396FF   //友好玩家
#define  HOSTILITY_PLAYER_COLOR     0xFFFF0000   //敌对玩家
#define  MASTER_FN_COLOR			0xFFFFFF00   //友好(F)或者中立
#define  MASTER_HOSTILITY_ATTACK    0xFFFF6600   //主动攻击(H)
#define  SCENE_OBJ                 0xFFBEF8FC
#define  MASTER_HOSTILITY_UNATTACK  0xFFFFFF00   //被动攻击(H)

//小地图显示控制
enum MINIMAPSHOWTYPE
{
	NPC_FRIENDLY		=	0x0001,		//友好NPC
	PLAYER_FRIENDLY		=	0x0004,	    //友好玩家
	CREATURE_ALL		=   0x0008,     //周围怪物   包括(敌对 中立怪物. (敌对NPC当怪物处理))
							//	0X0020,		
	ShowALL = (NPC_FRIENDLY | PLAYER_FRIENDLY | CREATURE_ALL),
};

// 小地图需要加载的纹理的列表
enum 
{
	MaskTexture = 0,
	ArrowTexture ,
	DotTexture,

	PlayerTexture,
	OtherPlayerTexture,//小地图其他成员

	TeamArrow,   //队伍成员方向
	FindOBJArrow, //寻找对象的方向
	SeeOBJArrow, //在地图显示范围后,寻找对象的标示

	CanGetQTexture,    //可接受任务
	PerformQTexture,   //正在执行的任务
	FinishQTexture,    //完成的任务
	UnActiveQTexture,  //未激活的任务

	FriendlyTexture,   //友好目标
	EnemyTexture,	   //敌对目标
	NeutralTexture,		//中立目标

	MineTexture,		//矿石
	HerbTexture,		//草药

	NullTexture,
	MaxCountTexture,
};
enum
{
	//NPC flag 
	//NPC_TRAINER_CLASS = 0,	//职业技能训练师
	//NPC_TRAINER_PROF,		//专业训练师
	//NPC_VENDOR,		//道具商
	//NPC_ARMORER = 0,	//修理 
	NPC_TAXIVENDOR, //飞行管理员
	NPC_INNKEEPER,  //旅店老板
	NPC_BANKER,		//银行管理员
	NPC_ARENACHARTER, //竞技场管理员
	NPC_BATTLEFIELDPERSON, //战场管理员
	NPC_AUCTIONEER, //拍卖行管理员
	NPC_GUILD_BANK, //工会银行
	NPC_MAIL,		//邮箱管理员
	NPC_TRANSPORTER, //传送
	NPC_GUILD,       //部族管理员

	NPC_VENDOR_ZAHUO      ,	//杂货
	NPC_VENDOR_YAOSHUI	,	//药水
	NPC_VENDOR_WUQI	,	//武器
	NPC_VENDOR_FANGJU		,	//防具
	NPC_VENDOR_SHOUSHI	,	//首饰
	NPC_VENDOR_ZUOQI		,	//坐骑
	NPC_TRAINER_WUXIU		,	//武修
	NPC_TRAINER_YUJIAN	,	//羽箭
	NPC_TRAINER_XIANDAO	,	//仙道
	NPC_TRAINER_ZHENWU	,	//真巫
	NPC_JIAYUAN			,	//家园

	NPC_FLAG_TEXTURE_COUNT,
};

struct MiniMapTexture
{
	int m_iX;
	int m_iY;
	UTexturePtr m_kTexture;
};
struct MiniMapRect 
{
	int m_iX;
	int m_iY;
	URect m_kRect;
	URect m_kRenderRect;
};

//只是用来处理消息
class UMinimapFrame : public UControl
{
public:
	UDEC_CLASS(UMinimapFrame);
	UDEC_MESSAGEMAP();

	void  OnZoomMap();        //放大
	void  OnReduceMap();      //缩小
	void  OnShowBigMap();     //显示大地图
	void  OnShowMinimap();    //是否显示小地图
	void  OnExitInstance();
	void  OnSendShowRank();   //显示排行
	void  OnClickQuery();
	void  OnClickHOOK();
	void OnClickShow();
	void  OnClickApplyGroup();

public:
	void SetApplyGroupVisible(bool vis);
	
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
public:
	void  MiNiMapShowControl(MINIMAPSHOWTYPE ShowType, BOOL vis);
	void  OnShowMail(BOOL bShow);       //显示邮件提示
	void  SetDrawFindOBJPos(bool bDraw, NiPoint3 kPos = NiPoint3::ZERO);
protected:
	UStaticText* m_Time;
};

class UMinimap : public UControl
{
public:
	UDEC_CLASS(UMinimap);
	UDEC_MESSAGEMAP();
	UMinimap();
	~UMinimap();
	//void Update(float dt);
	enum skinID
	{
		UMinimapBgk = 0,
		UMinimapBlind ,
	};

public:
	void SetDrawFindOBJPos(bool bDraw, NiPoint3 kPos = NiPoint3::ZERO);
	BOOL ZoomMap(); //放大
	BOOL ReduceMap(); //缩小
	void SetMapNameControl(UStaticText* pkControl) { m_MapName = pkControl;}
	void SetMapCoordinateControl(UStaticText* pkControl) { m_MapCoordinate = pkControl;}
public:
	void MiNiMapShowControl(MINIMAPSHOWTYPE ShowType, BOOL vis);
	void SetMapName(const  char* name);
	void OnMapChanged();
protected:
	BOOL InitTextureList();
	void ShutDownTextureList();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
	void DrawMinimap(UPoint offset, const URect &updateRect);
	void DrawPathFind(const URect& updateRect,float fXLeft, float fYTop);
	BOOL GetRenderScreenRect(NiPoint3 kPos ,const URect& updateRect,float fXLeft, float fYTop,int  drawSize, URect& kRect);
	//小地图渲染对象
	//NPC对象
	void DrawObject(const URect &updateRect, float fXLeft, float fYTop);
	//采集对象
	void DrawScenceObj(const URect &updateRect, float fXLeft, float fYTop);
	//小地图渲染标示
	void DrawFlag(const URect &updateRect, float fXLeft, float fYTop);
	//渲染地图外的点与地图的方向标
	void DrawDirection(NiPoint3 kPos, const URect &updateRect, float fXLeft, float fYTop , UINT Index);
	//检查椭圆与点的关系
	BOOL CheckPos(const URect &updateRect, NiPoint3 kPos, float fXLeft, float fYTop);
	//求椭圆外点  到圆心直线之间的焦点...以及角度
	void GetDirectionPos(const URect &updateRect, const URect &rec, float &x, float &y, float &fAngle); //
	
	

	UStaticText* m_MapName ; //地图名字
	UStaticText* m_MapCoordinate; //地图坐标

	//UTexturePtr m_kMaskTexture;
	//UTexturePtr m_kArrowTexture;
	//UTexturePtr m_kDotTexture;
	//UTexturePtr m_kNullTexture;

	UTexturePtr m_TextureList[MaxCountTexture];
	UTexturePtr m_NpcFlagTextureList[NPC_FLAG_TEXTURE_COUNT];
	float		m_fScale;

	//小地图显示查找NPC的方向
	bool m_bDrawFindObjPos;
	NiPoint3 m_kFindObjPos;
	int FindOBJMapId ;

	NiFixedString m_kMapName;

	MiniMapTexture m_kCacheTexture[CACHE_SIZE];

	void SetMiniMapInfo(int Side, int iX, int iY, const float* texcoord, const float* destRect);
	int findSide(int iX, int iY);
	MiniMapRect m_kMiniMapRect[4];

	void UnloadCache();
	IUTexture* LoadTexture(float fX, float fY);
	IUTexture* LoadTexture(int iX, int iY);

	//显示控制
	UINT m_ShowFlag;
	struct RenderItem
	{
		RenderItem():TextureId(DotTexture),color(LCOLOR_WHITE),TextureType(0){}
		NiPoint3 Position;
		URect DrawRect;
		UColor color;
		int TextureType;
		int TextureId;
	};
	void AddRenderItem(int TextureId, URect& DrawRect, DWORD color,int TextureType = 0, UpdateObj* obj = NULL);
	std::map<ui64, RenderItem> m_ObjectRenderList;	

	void UpdateLocalPlayer();
	float m_LocalPosX;
	float m_LocalPosY;
public:
	bool GetObjectNameByPosition(UPoint pos, std::vector<std::string>& vName);
};

extern UMinimap* g_pkMiniMap;