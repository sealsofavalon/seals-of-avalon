#include "stdafx.h"
#include "UIMoneyText.h"
UMoney::UMoney(void)
{
	m_MoneyNum = 0;
	m_MoneyTexture[0] = m_MoneyTexture[1] = m_MoneyTexture[2] = m_MoneyTexture[3] = NULL;
	isYuanbao = FALSE;
	m_bSample = TRUE;
}

UMoney::~UMoney(void)
{
}
BOOL UMoney::Create(URender * pRender)
{
	if (m_MoneyTexture[0] == NULL)
	{
		m_MoneyTexture[0] = pRender->LoadTexture("DATA\\UI\\public\\gold.png");
	}
	if (!m_MoneyTexture[0])
	{
		return FALSE;
	}
	if (m_MoneyTexture[1] == NULL)
	{
		m_MoneyTexture[1] = pRender->LoadTexture("DATA\\UI\\public\\silver.png");
	}
	if (!m_MoneyTexture[1])
	{
		return FALSE;
	}
	if (m_MoneyTexture[2] == NULL)
	{
		m_MoneyTexture[2] = pRender->LoadTexture("DATA\\UI\\public\\copper.png");
	}
	if (!m_MoneyTexture[2])
	{
		return FALSE;
	}

	if (m_MoneyTexture[3] == NULL)
	{
		m_MoneyTexture[3] = pRender->LoadTexture("DATA\\UI\\public\\YuanBao.png");
	}
	if (!m_MoneyTexture[3])
	{
		return FALSE;
	}
	return TRUE;
}
void UMoney::DrawMoney(URender * pRender,IUFont *pFont,const UPoint &offset,const UPoint &Size,int Align)
{
	UPoint pos = offset;

	if (m_MoneyNum == 0)
	{
		if (Align == UT_RIGHT)
		{
			pos.Set(offset.x + Size.x - GetWidth(pFont),offset.y);
		}
		if (Align == UT_CENTER)
		{
			pos.Set(offset.x + (Size.x - GetWidth(pFont)) / 2 ,offset.y);
		}
		pRender->DrawTextN(pFont,pos,L"0",3,NULL,LCOLOR_WHITE);
		return;
	}

	if (isYuanbao)
	{
		char buf[_MAX_PATH];
		itoa(m_MoneyNum , buf, 10);
		UStringW txtTem ; 
		txtTem.Set(buf);

		UINT textWidth  = pFont->GetStrNWidth(txtTem,txtTem.GetLength());

		if (Align == UT_RIGHT)
		{
			pos.Set(offset.x + Size.x - textWidth - m_MoneyTexture[3]->GetWidth() ,offset.y);
		}

		if (Align == UT_CENTER)
		{

			pos.Set(offset.x + (Size.x - textWidth - m_MoneyTexture[3]->GetWidth()) / 2 ,offset.y);
		}

		pRender->DrawTextN(pFont,pos,txtTem,txtTem.GetLength(),NULL,LCOLOR_WHITE);
		pos.x += pFont->GetStrNWidth(txtTem,txtTem.GetLength());
		URect screenrect;
		screenrect.pt0.x = pos.x;
		screenrect.pt0.y = pos.y;
		screenrect.pt1.x = pos.x + m_MoneyTexture[3]->GetWidth();
		screenrect.pt1.y = pos.y + m_MoneyTexture[3]->GetHeight();
		pRender->DrawImage(m_MoneyTexture[3],screenrect);


	}else
	{
		if (Align == UT_RIGHT)
		{
			pos.Set(offset.x + Size.x - GetWidth(pFont),offset.y);
		}

		if (Align == UT_CENTER)
		{
			pos.Set(offset.x + (Size.x - GetWidth(pFont)) / 2,offset.y);
		}

		for (int i = 0 ; i < 3 ; i++ )
		{
			if (m_money[i].GetLength())
			{
				pRender->DrawTextN(pFont,pos,m_money[i],m_money[i].GetLength(),NULL,LCOLOR_WHITE);
				pos.x += pFont->GetStrNWidth(m_money[i],m_money[i].GetLength()) + 3;
				URect screenrect;
				screenrect.pt0.x = pos.x;
				screenrect.pt0.y = pos.y;
				screenrect.pt1.x = pos.x + m_MoneyTexture[i]->GetWidth();
				screenrect.pt1.y = pos.y + m_MoneyTexture[i]->GetHeight();
				pRender->DrawImage(m_MoneyTexture[i],screenrect);
				pos.x += m_MoneyTexture[i]->GetWidth() + 3;
			}
		}
	}
}
int UMoney::GetWidth(IUFont * pFont)
{
	int _width = 0;
	if (m_MoneyNum == 0)
	{
		return pFont->GetStrNWidth(L"N/A",3);
	}
	for (int i = 0 ; i < 3 ; i++ )
	{
		if (m_money[i].GetLength())
		{
			_width += pFont->GetStrNWidth(m_money[i],m_money[i].GetLength());
			_width += m_MoneyTexture[i]->GetWidth();
			_width += 6;
		}
	}
	return _width;
}
void UMoney::SetMoney(UINT copper, BOOL bYuanbao)
{
	if (copper < 0)
	{
		m_MoneyNum = 0;
		return;
	}
	isYuanbao = bYuanbao ;
	m_MoneyNum = copper;
	if (!isYuanbao)
	{
		CalculateMoney();
	}	
}
void UMoney::Destory()
{
	m_MoneyNum = 0;
}
void UMoney::CalculateMoney()
{
	for (int i = 0 ; i < 3 ; i++)
	{
		m_money[i].SetEmpty();
	}
	char buf[10];
	UINT gold = m_MoneyNum / 10000;
	if (gold != 0)
	{
		NiSprintf(buf, 10, "%u", gold);
		m_money[0].Set(buf);
	}
	UINT silver = (m_MoneyNum - gold*10000)/100;
	if (silver != 0 || !m_bSample)
	{
		if (m_bSample)
			NiSprintf(buf, 10, "%u", silver);
		else
			NiSprintf(buf, 10, "%02u", silver);
		m_money[1].Set(buf);
	}
	UINT copper = m_MoneyNum % 100;
	if (copper != 0 || !m_bSample)
	{
		if (m_bSample)
			NiSprintf(buf, 10, "%u", copper);
		else
			NiSprintf(buf, 10, "%02u", copper);
		m_money[2].Set(buf);
	}

}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UMoneyText,UControl);
void UMoneyText::StaticInit()
{
	
}
UMoneyText::UMoneyText()
{
	m_Alignment = UT_CENTER;
}
UMoneyText::~UMoneyText()
{

}
BOOL UMoneyText::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (!m_uMoney.Create(sm_UiRender))
	{
		return FALSE;
	}

	return TRUE;
}

void UMoneyText::OnRender(const UPoint& offset,const URect &updateRect)
{
	UControl::OnRender(offset, updateRect);
	m_uMoney.DrawMoney(sm_UiRender, m_Style->m_spFont, offset, updateRect.GetSize(), m_Alignment);
}

void UMoneyText::SetMoney(UINT money, BOOL isYuanbao)
{
	m_uMoney.SetMoney(money,isYuanbao);
}