#pragma once
#include "UInc.h"

class UMoney
{
public:
	UMoney(void);
	~UMoney(void);
public:
	BOOL Create(URender * pRender);
	void Destory();
	void  DrawMoney(URender * pRender,IUFont *pFont,const UPoint &offset,const UPoint &Size,int Align);
	int GetWidth(IUFont *pFont);
	void SetMoney(UINT copper, BOOL bYuanbao = FALSE);
	void CalculateMoney();
	BOOL m_bSample;
private:
	UINT m_MoneyNum;
	UStringW m_money[3];
	UTexturePtr m_MoneyTexture[4];
	BOOL isYuanbao ;
};

class UMoneyText : public UControl
{
	UDEC_CLASS(UMoneyText);
public:
	UMoneyText();
	~UMoneyText();

	void SetMoney(UINT money, BOOL isYuanbao = FALSE) ;
	void SetMoneyTextAlign(EUTextAlignment align){m_Alignment = align ;}
	void SetMoneySample(BOOL bSample){m_uMoney.m_bSample = bSample;}
protected:
	virtual BOOL OnCreate();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual BOOL HitTest(const UPoint& parentCoordPoint){return FALSE;}

protected:
	UMoney m_uMoney;
	EUTextAlignment m_Alignment;
};