#include "StdAfx.h"
#include "UINPCTrade.h"
#include "UISystem.h"
#include "ItemManager.h"
#include "ClientApp.h"
#include "UMessageBox.h"
#include "Utils/ItemDB.h"
#include "Utils/ItemExtendedCostentry.h"
#include "Creature.h"
#include "ObjectManager.h"
#include "UITipSystem.h"
#include "UInputAmount.h"
#include "UIGamePlay.h"
#include "UIPackage.h"
#include "UFun.h"
#include "ItemSlot.h"
#include "UISkill.h"
#include "UIMoneyText.h"
#include "Network/PacketBuilder.h"

UIMP_CLASS(UNPCTrade,UDialog);

UBEGIN_MESSAGE_MAP(UNPCTrade,UDialog)
UON_BN_CLICKED(12,&UNPCTrade::OnPageUp)
UON_BN_CLICKED(13,&UNPCTrade::OnPageDown)
UON_BN_CLICKED(14,&UNPCTrade::OnRepairAll)
UON_BN_CLICKED(15,&UNPCTrade::OnRepairOne)
UON_BN_CLICKED(40,&UNPCTrade::OnClickTypeShop)
UON_BN_CLICKED(41,&UNPCTrade::OnClickTypeBuyBack)
UON_CBOX_SELCHANGE(28, &UNPCTrade::OnSelChangShowType)
UEND_MESSAGE_MAP()


void UNPCTrade::StaticInit()
{

}

UNPCTrade::UNPCTrade()
{
	m_NPCguid = 0;
	m_numPage = 0;
	m_curPage = 0;

	m_bRepair = FALSE;

	m_SalePos_0 = NULL;
	m_SalePos_1 = NULL;
	m_SalePos_2 = NULL;
	m_SalePos_3 = NULL;
	m_SalePos_4 = NULL;
	m_SalePos_5 = NULL;
	m_SalePos_6 = NULL;
	m_SalePos_7 = NULL;
	m_SalePos_8 = NULL;
	m_SalePos_9 = NULL;
	m_SalePos_10 = NULL;
	m_SalePos_11 = NULL;

	m_CurSelItem = NULL;
	m_TypeComboBox = NULL;

	m_ShowType = USYWareClass ;

	m_bRepairNpc = FALSE;
	m_bBuyBack = false;
}
UNPCTrade::~UNPCTrade()
{

}

BOOL UNPCTrade::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;

	m_SalePos_0 = (UNPCTradeExplain*)GetChildByID(0);
	m_SalePos_1 = (UNPCTradeExplain*)GetChildByID(1);
	m_SalePos_2 = (UNPCTradeExplain*)GetChildByID(2);
	m_SalePos_3 = (UNPCTradeExplain*)GetChildByID(3);
	m_SalePos_4 = (UNPCTradeExplain*)GetChildByID(4);
	m_SalePos_5 = (UNPCTradeExplain*)GetChildByID(5);
	m_SalePos_6 = (UNPCTradeExplain*)GetChildByID(6);
	m_SalePos_7 = (UNPCTradeExplain*)GetChildByID(7);
	m_SalePos_8 = (UNPCTradeExplain*)GetChildByID(8);
	m_SalePos_9 = (UNPCTradeExplain*)GetChildByID(9);
	m_SalePos_10 = (UNPCTradeExplain*)GetChildByID(10);
	m_SalePos_11 = (UNPCTradeExplain*)GetChildByID(11);


	if (m_SalePos_0 == NULL || m_SalePos_1 == NULL || m_SalePos_2 == NULL || m_SalePos_3 == NULL ||
		m_SalePos_4 == NULL || m_SalePos_5 == NULL || m_SalePos_6 == NULL || m_SalePos_7 == NULL ||
		m_SalePos_8 == NULL || m_SalePos_9 == NULL || m_SalePos_10 == NULL || m_SalePos_11 == NULL)
	{
		return FALSE;
	}

	if (m_TypeComboBox == NULL)
	{
		m_TypeComboBox = UDynamicCast(UComboBox, GetChildByID(28));
	}
	if (m_TypeComboBox == NULL )
	{
		return FALSE;
	}

	m_TypeComboBox->AddItem( _TRAN("全部") );
	m_TypeComboBox->AddItem( _TRAN("职业") );
	m_TypeComboBox->AddItem( _TRAN("可用") );
	UStaticText* ptext = UDynamicCast(UStaticText, GetChildByID(31));
	if (ptext)
	{
		ptext->SetTextAlign(UT_RIGHT);
	}

	m_bRepairNpc = FALSE;
	m_bRepair = FALSE;

	UButton* pBtn = UDynamicCast(UButton, GetChildByID(40));
	if (pBtn)
	{
		pBtn->SetCheckState(TRUE);
	}
	return TRUE;
}

void UNPCTrade::OnDestroy()
{
	m_TypeComboBox->Clear();
	UDialog::OnDestroy();
}

void UNPCTrade::ClearUI()
{
	m_ShowSaleItemList.clear();
	m_SaleItemList.clear();
	m_CurSelItem = NULL;

	m_ShowType = USYWareClass;
	//m_bRepair = FALSE;
	m_bRepairNpc = FALSE;
	m_SalePos_0->OnClear();
	m_SalePos_1->OnClear();
	m_SalePos_2->OnClear();
	m_SalePos_3->OnClear();
	m_SalePos_4->OnClear();
	m_SalePos_5->OnClear();
	m_SalePos_6->OnClear();
	m_SalePos_7->OnClear();
	m_SalePos_8->OnClear();
	m_SalePos_9->OnClear();
	m_SalePos_10->OnClear();
	m_SalePos_11->OnClear();
	SetRepair(FALSE);
}
void UNPCTrade::OnTimer(float fDeltaTime)
{
	UDialog::OnTimer(fDeltaTime);

	//如果距离超过10M,自动关闭!
	if (m_NPCguid)
	{
		CCreature* pCreature = (CCreature*)ObjectMgr->GetObject(m_NPCguid);
		CPlayerLocal* pLocalPlayer = ObjectMgr->GetLocalPlayer();

		if (pLocalPlayer == NULL || pCreature == NULL)
		{
			return ;
		}else
		{
			NiPoint3 pCreaturePos = pCreature->GetPosition();
			NiPoint3 pLocalPlayerPos = pLocalPlayer->GetLocalPlayerPos();

			if ((pLocalPlayerPos - pCreaturePos).Length() > MaxLenToNPC)
			{

				OnClose();
			}
		}
	}

	//SYState()->ClientApp->SetCursor(SYC_ARROW);
	//SYState()->ClientApp->SetCursor(SYC_REPAIR);
}
void UNPCTrade::OnSelChangShowType()
{
	if (m_TypeComboBox)
	{
		int sType = m_TypeComboBox->GetCurSel();
		if ( sType >= 0 && sType < m_TypeComboBox->GetCount())
		{
			m_ShowType = (USYWareShowType) sType;
			SetShowList(m_ShowType);
		}
	}
}
void UNPCTrade::SetShowList(USYWareShowType pType)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return ;
	}
	int pkCalssMask = pkLocal->GetClassMask();                           //职业
	unsigned int pkLevel = pkLocal->GetUInt32Value(UNIT_FIELD_LEVEL);     //等级
	int pkRaceMask = pkLocal->GetRaceMask();                             //种族
	int pkSex = pkLocal->GetGender();                            //性别

	m_ShowSaleItemList.clear();
	if (m_SaleItemList.size() > 0)
	{
		switch(pType)
		{
		case USYWareAll:
			{
				m_ShowSaleItemList = m_SaleItemList;
			}
			break;
		case USYWareClass:
			{
				for (UINT i = 0; i < m_SaleItemList.size(); i++)
				{
					ui32 pkItemID = m_SaleItemList[i].itemid;
					if (pkItemID)
					{
						ItemPrototype_Client* pkSYItem = ItemMgr->GetItemPropertyFromDataBase(pkItemID);
						if (pkSYItem && (pkSYItem->AllowableClass & pkCalssMask))
						{
							m_ShowSaleItemList.push_back(m_SaleItemList[i]);
						}
					}
	
				}
			}
			break;
		case USYWareCanUse:
			{
				for (UINT i = 0; i < m_SaleItemList.size(); i++)
				{
					ui32 pkItemID = m_SaleItemList[i].itemid;
					if (pkItemID)
					{
						ItemPrototype_Client* pkSYItem = ItemMgr->GetItemPropertyFromDataBase(pkItemID);
						bool bSuccess = true;
						if (pkSYItem->Spells[0].Id == 483)
						{
							UISkill * pSkill = INGAMEGETFRAME(UISkill,FRAME_IG_SKILLDLG);
							if (!pSkill)
								break;
							if(pkSYItem->Spells[1].Id && pkLocal->HasLearnSkill(pkSYItem->Spells[1].Id))
								bSuccess = false;
						}

						if (pkSYItem && (pkSYItem->AllowableClass & pkCalssMask) && (pkSYItem->AllowableRace & pkRaceMask) && pkSYItem->RequiredLevel <= pkLevel && bSuccess)
						{
							m_ShowSaleItemList.push_back(m_SaleItemList[i]);
						}
					}

				}
			}
			break;	
		}


		m_SalePos_0->OnClear();
		m_SalePos_1->OnClear();
		m_SalePos_2->OnClear();
		m_SalePos_3->OnClear();
		m_SalePos_4->OnClear();
		m_SalePos_5->OnClear();
		m_SalePos_6->OnClear();
		m_SalePos_7->OnClear();
		m_SalePos_8->OnClear();
		m_SalePos_9->OnClear();
		m_SalePos_10->OnClear();
		m_SalePos_11->OnClear();

		if (m_ShowSaleItemList.size() > 0)
		{
			if (m_TypeComboBox)
			{
				m_TypeComboBox->SetCurSel((int)m_ShowType);
			}

			UINT listCount = m_ShowSaleItemList.size();

			m_numPage = listCount / NPCTradePageCount;

			if (listCount % NPCTradePageCount >0 )
			{
				m_numPage ++;
			}

			if (m_curPage > m_numPage -1 || m_curPage < 0)
			{
				m_curPage = 0;  //设置当前页为0
			}
			OnSetPageList();
		}
	}
}

void UNPCTrade::OnItemList(MSG_S2C::stItem_List_Inventroy& sList)
{
	ClearUI();
	m_NPCguid = sList.vendorguid;
	//m_ShowSaleItemList = sList.vItem;
	m_SaleItemList = sList.vItem; 
	SetNPCHead();

	CPlayerLocal::SetCUState(cus_NPCTrade);
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(40));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	SetShowList(m_ShowType);
	if (m_ShowSaleItemList.size() == 0 )
	{
		m_ShowType = USYWareAll;
		SetShowList(m_ShowType);
	}
}
void UNPCTrade::OnSetPageList()
{
	if (m_bBuyBack)
	{
		CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
		if (!pkLocal)
		{
			return;
		}
		for (int i = 0; i < NPCTradePageCount; i++)
		{
			m_BuyBackList[i].count = 1;
			m_BuyBackList[i].amt = 1;
			m_BuyBackList[i].ExtendedCost = 0;
			SYItem* pItem = (SYItem*)ObjectMgr->GetObject(pkLocal->m_LocalBuyBack[i]);
			if (pItem)
			{
				m_BuyBackList[i].itemid = pItem->GetCode();
				m_BuyBackList[i].BuyCount = pItem->GetNum();
				OnSetListToExplain(i,&m_BuyBackList[i]);
			}else
				OnSetListToExplain(i, NULL);
		}

	}else
	{
		UBitmapButton* pkUP = UDynamicCast(UBitmapButton, GetChildByID(12));
		if (!pkUP) return;
		UBitmapButton* pkDown = UDynamicCast(UBitmapButton, GetChildByID(13));
		if (!pkDown) return;
		pkUP->SetActive(m_curPage != 0);
		pkDown->SetActive(m_curPage != m_numPage - 1);
		for (int i = 0; i < NPCTradePageCount; i++)
		{
			if (m_curPage * NPCTradePageCount + i < m_ShowSaleItemList.size())
				OnSetListToExplain(i,&m_ShowSaleItemList[m_curPage * NPCTradePageCount + i]);
			else
				OnSetListToExplain(i,NULL);
		}
	}
}
void UNPCTrade::OnSetListToExplain(UINT pos, MSG_S2C::stItem_List_Inventroy::stItem* data)
{
	if (pos >= 0 && pos < NPCTradePageCount)
	{
		UNPCTradeExplain* pExplain = NULL;
		switch (pos)
		{
		case 0:pExplain = m_SalePos_0;break;
		case 1:pExplain = m_SalePos_1;break;
		case 2:pExplain = m_SalePos_2;break;
		case 3:pExplain = m_SalePos_3;break;
		case 4:pExplain = m_SalePos_4;break;
		case 5:pExplain = m_SalePos_5;break;
		case 6:pExplain = m_SalePos_6;break;
		case 7:pExplain = m_SalePos_7;break;
		case 8:pExplain = m_SalePos_8;break;
		case 9:pExplain = m_SalePos_9;break;
		case 10:pExplain = m_SalePos_10;break;
		case 11:pExplain = m_SalePos_11;break;
		}

		if (pExplain)
		{
			pExplain->OnInit(data,pos,m_NPCguid, m_bBuyBack);
		}
	}
}
void UNPCTrade::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	UDialog::OnRightMouseDown(position,nRepCnt,flags);
}
void UNPCTrade::OnMouseMove(const UPoint& position, UINT flags)
{
	//UDialog::OnMouseMove(position,flags);
	//SYState()->ClientApp->SetCursor(SYC_BUY);
}
void UNPCTrade::OnClose()
{
	CPlayerLocal::SetCUState(cus_Normal);
}
void UNPCTrade::SetCUSNormal()
{
	ClearUI();
	SetVisible(FALSE);
	m_NPCguid = 0;
	m_curPage = 0;
	UInputAmount::SetAmoutTypeDefault();
}
void UNPCTrade::OnPageUp()      //翻页
{
	if (m_curPage >0)
	{
		m_curPage--;
	}
	OnSetPageList();
}
void UNPCTrade::OnPageDown()    //翻页
{
	if (m_curPage < m_numPage - 1)
	{
		m_curPage++;
	}
	OnSetPageList();
}

void UNPCTrade::OnRepairOne()      //修理一件
{
	SetRepair(TRUE);
}
void UNPCTrade::SetNPCHead()
{
	CCreature* pkNPC = (CCreature*)ObjectMgr->GetObject(m_NPCguid);
	UBitmap* pkHead = (UBitmap*)GetChildByID(33);

	if (pkNPC)
	{
		ui32 guid = pkNPC->GetUInt32Value(OBJECT_FIELD_ENTRY);
		if (pkHead)
		{
			pkHead->SetBitMapByStr(CCharacter::GetHeadImageFile(pkNPC));
		}
		if (pkNPC->HasFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_ARMORER))
		{
			m_bRepairNpc = TRUE ;
		}else
		{
			m_bRepairNpc = FALSE ;
		}
		if (GetChildByID(14)) 
		{
			GetChildByID(14)->SetActive(m_bRepairNpc);
		}
		if (GetChildByID(15))
		{
			GetChildByID(15)->SetActive(m_bRepairNpc);
		}
	}
}
void UNPCTrade::SetCurSelItem(MSG_S2C::stItem_List_Inventroy::stItem* pItem, int pos)
{
	if (pItem)
	{
		m_CurSelItem = pItem;
	}else
	{
		m_CurSelItem = NULL;
	}
	m_buyslot = pos;
}
void UNPCTrade::OnShowMoney(UINT Num)
{
	UMoneyText* pMoney = UDynamicCast(UMoneyText, GetChildByID(17));
	
	if (pMoney)
	{
		pMoney->SetMoney(Num);
		pMoney->SetMoneyTextAlign(UT_RIGHT);
	}
}
void UNPCTrade::SetRepair(BOOL repair)
{
	if (m_bRepairNpc && repair)
	{
		m_bRepair = TRUE;
	}else
	{
		m_bRepair = FALSE ;
	}
	if (m_bRepair)
	{
		SYState()->ClientApp->SetCursor(SYC_REPAIR); 
	}else
	{
		SYState()->ClientApp->SetCursor(SYC_ARROW); 
	}
	
}
void UNPCTrade::OnRepairAll()
{
	SYState()->UI->GetUItemSystem()->RepairAll(m_NPCguid);
}

void UNPCTrade::OnClickTypeShop()
{
	for (int i = 12 ; i < 16 ; i++)
	{
		GetChildByID(i)->SetVisible(TRUE);
	}
	GetChildByID(28)->SetVisible(TRUE);
	GetChildByID(32)->SetVisible(TRUE);
	GetChildByID(34)->SetVisible(FALSE);
	GetChildByID(29)->SetVisible(TRUE);
	GetChildByID(30)->SetVisible(FALSE);
	m_spBkgSkin = sm_System->LoadSkin("NPCTrade\\NPCTradeBgk.skin");
	m_bBuyBack = false;

	OnSetPageList();

}

void UNPCTrade::OnClickTypeBuyBack()
{
	for (int i = 12 ; i < 16 ; i++)
	{
		GetChildByID(i)->SetVisible(FALSE);
	}
	GetChildByID(28)->SetVisible(FALSE);
	GetChildByID(32)->SetVisible(FALSE);
	GetChildByID(34)->SetVisible(TRUE);
	GetChildByID(29)->SetVisible(FALSE);
	GetChildByID(30)->SetVisible(TRUE);
	m_spBkgSkin = sm_System->LoadSkin("NPCTrade\\NPCBuyBackBgk.skin");
	m_bBuyBack = true;


	OnSetPageList();
}

BOOL UNPCTrade::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}
BOOL UNPCTrade::OnMouseUpDragDrop(UControl* pDragFrom,const UPoint& point, const void* DragData, UINT nDataFlag)
{
	if (!m_Active)
	{
		return FALSE;
	}

	if (nDataFlag != UItemSystem::ICT_BAG)
	{
		return FALSE;
	}
	
	const ui8* FromePos = (const ui8*)DragData;
	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if (localPlayer)
	{
		CStorage* pContainer = localPlayer->GetItemContainer();
		CItemSlot* pSlot = (CItemSlot*)pContainer->GetSlot(*FromePos);
		

		ItemPrototype_Client* itemPrototype = pSlot->GetItemProperty();
		if (!itemPrototype) return FALSE;

		if (itemPrototype->SellPrice <= 0)
		{
			ClientSystemNotify( _TRAN("该物品无法出售!") );
			return FALSE;
		}

		if (itemPrototype->Quality >= ITEM_QUALITY_UNCOMMON_GREEN )
		{
			//弹出对话框。
			UPackage* bag = INGAMEGETFRAME(UPackage, FRAME_IG_PACKAGE);
			bag->SetCurSel(*FromePos);
			UMessageBox::MsgBox( _TRAN("贵重物品，确认出售？") ,(BoxFunction*)UPackage::SaleItem, (BoxFunction*)UPackage::CancelSale);
			
		}else
		{
			//发送出售消息
			SYState()->UI->GetUItemSystem()->SendUseSellMsg(pSlot->GetGUID(),pSlot->GetNum());
			
		}
	}

	return TRUE ;
	
}
void UNPCTrade::OnRender(const UPoint& offset, const URect &updateRect)
{
	UDialog::OnRender(offset,updateRect);
	//RenderChildWindow(offset,updateRect);
}

void UNPCTrade::BuyCancel()
{
	UNPCTrade*pNPCTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);
	if (pNPCTrade)
	{
		pNPCTrade->SetCurSelItem(NULL, 0);
	}
}

void UNPCTrade::BuyItem()
{
	UINT Count = UInputAmount::GetAmount();
	UNPCTrade* pNPCTrade = NULL; 
	pNPCTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);

	if (pNPCTrade && Count && pNPCTrade->m_CurSelItem)
	{			// 发送购买请求
		SYState()->UI->GetUItemSystem()->BagItemMsg(pNPCTrade->m_NPCguid,pNPCTrade->m_CurSelItem->itemid,Count, pNPCTrade->m_CurSelItem->amt);	
		pNPCTrade->SetCurSelItem(NULL, 0);
	}
}

void UNPCTrade::BuyOneItem()
{
	UNPCTrade* pNPCTrade = NULL; 
	pNPCTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);

	if (pNPCTrade && pNPCTrade->m_CurSelItem)
	{
		SYState()->UI->GetUItemSystem()->BagItemMsg(pNPCTrade->m_NPCguid,pNPCTrade->m_CurSelItem->itemid,1, pNPCTrade->m_CurSelItem->amt);	
	}
	pNPCTrade->SetCurSelItem(NULL, 0);
}
//////////////////////////////////////////////////////////////////////////
/////////
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UNPCTradeExplain,UControl);

UBEGIN_MESSAGE_MAP(UNPCTradeExplain,UCmdTarget)
//UON_BN_RCLICKED(1,&UNPCTradeExplain::OnClickBuy)
UEND_MESSAGE_MAP()

void UNPCTradeExplain::StaticInit()
{

}
UNPCTradeExplain::UNPCTradeExplain()
{
	m_ItemInfo = NULL;
	m_SaleItem = NULL;
	m_ItemName = NULL;
	m_bBuyBack = false;
	m_pkPayControl = NULL;

	m_NPCguid = 0;
	m_nClickBuy = 0;
	m_Clicktime = 0.f;
}
UNPCTradeExplain::~UNPCTradeExplain()
{

}

void UNPCTradeExplain::OnInit(MSG_S2C::stItem_List_Inventroy::stItem* pSaleItem, UINT pos, ui64 guid, bool bBuyBack)
{
	OnClear();
	m_bBuyBack = bBuyBack;
	if (pSaleItem && guid != 0)
	{
		SetVisible(TRUE);
		m_NPCguid = guid;
		m_SaleItem = pSaleItem;

		USkillButton::DataInfo data;
		data.id = pSaleItem->itemid;
		data.pos = pos;
		data.type = UItemSystem::ITEM_DATA_FLAG;
		data.num = pSaleItem->BuyCount;
		data.pDataInfo = NULL;

		m_ItemInfo->SetStateData(&data);
		ItemPrototype_Client* pItemPrototype = ItemMgr->GetItemPropertyFromDataBase(pSaleItem->itemid);
		if (pItemPrototype)
		{
			m_ItemName->SetText(pItemPrototype->C_name.c_str());
			UColor _color(0,0,0,0);
			GetQualityColor(pItemPrototype->Quality, _color);
			m_ItemName->SetTextColor(_color);
		}

		SetShowPayData(); // 价格显示

	}else
	{
		SetVisible(FALSE);
	}
}
BOOL UNPCTradeExplain::CheckCanBuy(string& text)
{
	CPlayerLocal* pklocal = ObjectMgr->GetLocalPlayer();
	assert(pklocal);
	if (m_SaleItem->ExtendedCost)
	{
		ItemExtendedCostentry::ItemExtendeData* pkItemExtendeData = ItemMgr->GetItemExtendeDataFromDataBase(m_SaleItem->ExtendedCost);
		if (pkItemExtendeData)
		{
			if (pkItemExtendeData->Gold > pklocal->GetMoney())
			{
				text = _TRAN("金钱不足!");
				return FALSE;
			}
			if (pkItemExtendeData->Yuanbao > pklocal->GetYuanBao())
			{
				text = _TRAN("元宝不足！");
				return FALSE;
			}
			if (pkItemExtendeData->Honor > pklocal->GetUInt32Value(PLAYER_FIELD_HONOR_CURRENCY))
			{
				text = _TRAN("荣誉不够！");
				return FALSE;
			}
			 //当前荣誉
			if (pkItemExtendeData->Item1)
			{
				UINT count = pklocal->GetItemContainer()->GetItemCnt(pkItemExtendeData->Item1);
				if (pkItemExtendeData->ItemCount1 > count)
				{
					text = _TRAN("道具数量不够！");
					return FALSE;
				}
			}
			
			return TRUE;
			
		}
	}else
	{
		ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(m_SaleItem->itemid);
		if (ItemInfo && pklocal)
		{
			USkillButton::DataInfo* pData = m_ItemInfo->GetStateData();
			ui32 ItemCost= (ui32)ItemPrice(ItemInfo, true);
			if (m_bBuyBack)
			{
				ItemCost = pklocal->m_LocalBuyBackPrice[pData->pos];
			}
			if( ItemCost > pklocal->GetMoney())
			{
				text = _TRAN("金钱不足!");
				return FALSE ;
			}
			return TRUE;
		}
	}
	text = _TRAN("无法购买");
	return FALSE;
}

void UNPCTradeExplain::OnClickBuy()
{
	USkillButton::DataInfo* pData = m_ItemInfo->GetStateData();
	CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetLocalPlayer();
	if (pkPlayer == NULL)
	{
		return ;
	}
	UNPCTrade* pTrade = INGAMEGETFRAME(UNPCTrade, FRAME_IG_NPCTRADEDLG);
	if (!pTrade)return;
	string Text ;
	if (CheckCanBuy(Text) && pData->id !=0)
	{
		if (m_bBuyBack)
		{
			PacketBuilder->SendItemBuyBack(m_NPCguid, pData->pos);
			return;
		}
		ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(pData->id);
		if (ItemInfo && ItemInfo->Quality >= ITEM_QUALITY_UNCOMMON_GREEN)
		{
			pTrade->SetCurSelItem(m_SaleItem, pData->pos);
			UMessageBox::MsgBox( _TRAN("您确认购买?"), (BoxFunction*)UNPCTrade::BuyOneItem, (BoxFunction*)UNPCTrade::BuyCancel);
			return ;
		}
		SYState()->UI->GetUItemSystem()->BagItemMsg(m_NPCguid,pData->id,1, m_SaleItem->amt);
	}else
	{
		UMessageBox::MsgBox_s(Text.c_str());
	}
}
BOOL UNPCTradeExplain::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	m_ItemInfo = UDynamicCast(USkillButton, GetChildByID(1));
	m_ItemName = UDynamicCast(UStaticText, GetChildByID(0));
	m_pkPayControl = UDynamicCast(UPayControl, GetChildByID(2));

	if (m_ItemName == NULL || m_ItemInfo == NULL || m_pkPayControl == NULL)
	{
		return FALSE;
	}

	USkillButton::DataInfo data;
	data.id = 0;
	data.pos = -1;
	data.type = UItemSystem::ITEM_DATA_FLAG;	
	data.num = 0;

	m_ItemInfo->SetUIType(UItemSystem::ICT_NPC_TRADE);
	m_ItemInfo->SetStateData(&data);
	return TRUE;
}
void UNPCTradeExplain::OnDestroy()
{
	UControl::OnDestroy();
}
void UNPCTradeExplain::OnRender(const UPoint& offset,const URect &updateRect)
{
	RenderChildWindow(offset,updateRect);
}

void UNPCTradeExplain::OnTimer(float fDeltaTime)
{
	if(m_Clicktime > 0.f)
		m_Clicktime -= fDeltaTime;
	if(m_Clicktime <= 0.f)
	{
		m_Clicktime = 0.f;
		m_nClickBuy = 0;
	}
	UControl::OnTimer(fDeltaTime);
}

void UNPCTradeExplain::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	if (!m_Active)
		return;
	USkillButton::DataInfo* pData = m_ItemInfo->GetStateData(); 
	if (pData && pData->id > 0)
	{
		if (flags == LKM_SHIFT && !m_bBuyBack)
		{
			ShowInPut();
		}
		if (flags == LKM_NONE)
		{
			OnClickBuy();
		}

		//else
			//m_ItemInfo->OnRightMouseDown(position, nRepCnt, flags);
	}
	return UControl::OnRightMouseDown(position, nRepCnt, flags);
}

void UNPCTradeExplain::OnMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	if (!m_Active)
		return;
	if (nRepCnt > 1)
	{
		//OnClickBuy();
	}
	m_ItemInfo->OnMouseDown(position, nRepCnt, flags);
}
void UNPCTradeExplain::OnRightMouseUp(const UPoint& position, UINT flags)
{
	if (!m_Active)
		return;
	
	//m_ItemInfo->OnRightMouseUp(position, flags);
	return UControl::OnRightMouseUp(position,flags);
}
void UNPCTradeExplain::OnMouseUp(const UPoint& position, UINT flags)
{
	m_ItemInfo->OnMouseUp(position, flags);
	return UControl::OnMouseUp(position,flags);
}
void UNPCTradeExplain::OnMouseMove(const UPoint& position, UINT flags)
{
	URect ItemBtnRec = m_ItemInfo->GetControlRect();
	URect PayDataRec = m_pkPayControl->GetControlRect();
	UPoint tPos = GetParent()->WindowToScreen(m_Position);

	if (ItemBtnRec.PointInRect(position - tPos))
	{
		m_pkPayControl->ShowItemTips(tPos + UPoint(ItemBtnRec.right , ItemBtnRec.bottom));
		
	}else
	{
		if(PayDataRec.PointInRect(position - tPos))
		{
			m_pkPayControl->ShowTips(position - tPos);
		}else
		{
			m_pkPayControl->HidTips();
		}
		//SYState()->ClientApp->SetCursor(SYC_ARROW);
	}
	SYState()->ClientApp->SetCursor(SYC_PICK);
	//m_ItemInfo->OnMouseMove(position, flags);
	UControl::OnMouseMove(position,flags);
}
void UNPCTradeExplain::SetShowPayData()
{
	PayData pkPayData;
	//价格查询
	if (m_SaleItem->ExtendedCost)
	{
		ItemExtendedCostentry::ItemExtendeData* pkItemExtendeData = ItemMgr->GetItemExtendeDataFromDataBase(m_SaleItem->ExtendedCost);
		if (pkItemExtendeData)
		{
			pkPayData.mMoney = pkItemExtendeData->Gold;
			pkPayData.mYuanbao = pkItemExtendeData->Yuanbao;
			pkPayData.mHonor = pkItemExtendeData->Honor;
			pkPayData.mGuid_Gongxian = pkItemExtendeData->Guild_Contribute;
			pkPayData.mGuid_Jifen = pkItemExtendeData->Guild_Score;
			pkPayData.mArena = pkItemExtendeData->arena;
			pkPayData.mItemId = pkItemExtendeData->Item1;
			pkPayData.mItemCount = pkItemExtendeData->ItemCount1;

		}else
		{
			ULOG("id为%d没有找到" ,m_SaleItem->ExtendedCost);
		}
	}else
	{
		if (m_bBuyBack)
		{
			USkillButton::DataInfo* pData = m_ItemInfo->GetStateData();
			if (pData)
			{
				CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
				if (pkPlayer == NULL)
					return;
				pkPayData.mMoney = pkPlayer->m_LocalBuyBackPrice[pData->pos];
			}
		}else
		{
			ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(m_SaleItem->itemid);
			if (ItemInfo)
			{
				pkPayData.mMoney = (ui32)ItemPrice(ItemInfo, true);
			}
		}
	}	
	m_pkPayControl->SetPayData(&pkPayData, m_SaleItem->itemid);
}
void UNPCTradeExplain::OnClear()
{
	USkillButton::DataInfo data;
	data.id = 0;
	data.pos = -1;
	data.type = UItemSystem::ITEM_DATA_FLAG;
	data.num = 0;
	//m_Money.Destory();
	m_ItemInfo->SetStateData(&data);
	m_ItemName->Clear();
	SetVisible(FALSE);
}
void UNPCTradeExplain::ShowInPut()
{
	if (!m_SaleItem)
	{
		return ;
	}
	if(m_SaleItem->BuyCount > 1)
	{
		return;
	}
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	if (pLocal)
	{
		UINT pMoney = pLocal->GetMoney();
		 
		ItemPrototype_Client* pItemInfo = ItemMgr->GetItemPropertyFromDataBase(m_SaleItem->itemid);
		assert(pItemInfo);
		UINT pMaxCount = (UINT)(pMoney / ItemPrice(pItemInfo, true)) ;

		
		UINT pCount = 0;
		PayData pkPayData;
		if (m_SaleItem->ExtendedCost)
		{
			ItemExtendedCostentry::ItemExtendeData* pkItemExtendeData = ItemMgr->GetItemExtendeDataFromDataBase(m_SaleItem->ExtendedCost);
			if (pkItemExtendeData)
			{
				pkPayData.mMoney = pkItemExtendeData->Gold;
				pkPayData.mYuanbao = pkItemExtendeData->Yuanbao;
				pkPayData.mHonor = pkItemExtendeData->Honor;
				pkPayData.mGuid_Gongxian = pkItemExtendeData->Guild_Contribute;
				pkPayData.mGuid_Jifen = pkItemExtendeData->Guild_Score;
				pkPayData.mArena = pkItemExtendeData->arena;
				pkPayData.mItemId = pkItemExtendeData->Item1;
				pkPayData.mItemCount = pkItemExtendeData->ItemCount1;

				pMaxCount = 0;
				if(pkPayData.mMoney > 0)
				{
					if(pCount)
					{
						if(pCount > pMoney / pkPayData.mMoney)
						{
							pCount = pMoney / pkPayData.mMoney ;
						}
						
					}else
					{
						pCount  = pMoney / pkPayData.mMoney ;	
					}
					
				}

				if(pkPayData.mYuanbao > 0)
				{
					if(pCount)
					{
						if(pCount > pLocal->GetYuanBao() / pkPayData.mYuanbao)
						{
							pCount = pLocal->GetYuanBao() / pkPayData.mYuanbao ;
						}
					}else
					{
						pCount = pLocal->GetYuanBao() / pkPayData.mYuanbao ;
					}
					
				}

				if(pkPayData.mHonor > 0)
				{
					if (pCount)
					{
						if(pCount > pLocal->GetUInt32Value(PLAYER_FIELD_HONOR_CURRENCY) / pkPayData.mHonor)
						{
							pCount = pLocal->GetUInt32Value(PLAYER_FIELD_HONOR_CURRENCY) / pkPayData.mHonor;
						}
					}else
					{
						pCount = pLocal->GetUInt32Value(PLAYER_FIELD_HONOR_CURRENCY) / pkPayData.mHonor;
					}
					
					
				}

				
				if(pkPayData.mItemId > 0 && pkPayData.mItemCount > 0)
				{
					CStorage* pkBagStorage = pLocal->GetItemContainer();
					if (pkBagStorage)
					{
						if (pCount)
						{
							if(pCount > pkBagStorage->GetItemCnt(pkPayData.mItemId ) / pkPayData.mItemCount)
							{
								pCount = pkBagStorage->GetItemCnt(pkPayData.mItemId ) / pkPayData.mItemCount;
							}
						}else
						{
							pCount = pkBagStorage->GetItemCnt(pkPayData.mItemId ) / pkPayData.mItemCount;
						}		
					}
					
				}


				pMaxCount = pCount;
			}
		}

		if (pMaxCount > pItemInfo->MaxCount)
		{
			pMaxCount = pItemInfo->MaxCount;
		}

		if (pMaxCount > 255)
		{
			pMaxCount = 255 ;
		}


		if (pMaxCount)
		{
			UNPCTrade* pTrade = INGAMEGETFRAME(UNPCTrade, FRAME_IG_NPCTRADEDLG);
			if (pTrade)
			{
				pTrade->SetCurSelItem(m_SaleItem, 0);
				UInputAmount::InputAmount(BUY_GAME_ITEN, pMaxCount, (BoxFunction*)UNPCTrade::BuyItem, (BoxFunction*)UNPCTrade::BuyCancel);
			}
			
		}else
		{
			UMessageBox::MsgBox_s( _TRAN("没有那么多钱！") );
		}
	}
}
void UNPCTradeExplain::OnMouseEnter(const UPoint& position, UINT flags)
{
	UControl::OnMouseEnter(position,flags);
	UControl* pCtrl = GetChildByID(3);
	if(pCtrl)
		pCtrl->SetVisible(TRUE);
	m_ItemInfo->OnMouseEnter(position, flags);
	SYState()->ClientApp->SetCursor(SYC_ARROW);
}
void UNPCTradeExplain::OnMouseLeave(const UPoint& position, UINT flags)
{
	m_pkPayControl->HidTips();
	UControl::OnMouseLeave(position,flags);
	UControl* pCtrl = GetChildByID(3);
	if(pCtrl)
		pCtrl->SetVisible(FALSE);
	m_ItemInfo->OnMouseLeave(position, flags);
	SYState()->ClientApp->SetCursor(SYC_ARROW);
}

UControl* UNPCTradeExplain::FindHitWindow(const UPoint &pt, INT initialLayer)
{
	//UControl* pCtrl = GetChildByID(1);
	//if(pCtrl && pCtrl->GetControlRect().PointInRect(pt))
	//{
	//	return pCtrl;
	//}
	//return UControl::FindHitWindow(pt, initialLayer);
	return this;
}
UIMP_CLASS(UNPCTradeExplain::UPayControl,UControl);

UBEGIN_MESSAGE_MAP(UNPCTradeExplain::UPayControl,UCmdTarget)
UEND_MESSAGE_MAP()
void UNPCTradeExplain::UPayControl::StaticInit()
{

}
UNPCTradeExplain::UPayControl::UPayControl()
{
	mPayData = new PayData;
	mItemSkin = NULL;
	mItemSkinIndex = 0;
	mEntry = 0;
}
UNPCTradeExplain::UPayControl::~UPayControl()
{
	if (mPayData)
	{
		delete mPayData;
		mPayData = NULL;
	}
	ClearData();
}
BOOL UNPCTradeExplain::UPayControl::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (!mMoney.Create(sm_UiRender))
	{
		return FALSE;
	}
	return InitTexture();
}
void UNPCTradeExplain::UPayControl::OnDestroy()
{
	ClearData();
	UControl::OnDestroy();
}
void UNPCTradeExplain::UPayControl::ClearData()
{
	mItemSkin = NULL;
	mItemSkinIndex = 0;
	mEntry = 0;
}
void UNPCTradeExplain::UPayControl::SetPayData(PayData* data,  ui32 Entry)
{
	ClearData();

	mEntry = Entry;
	mPayData->mArena = data->mArena;
	mPayData->mGuid_Gongxian = data->mGuid_Gongxian;
	mPayData->mGuid_Jifen = data->mGuid_Jifen;
	mPayData->mHonor = data->mHonor;
	mPayData->mItemCount = data->mItemCount;
	mPayData->mItemId = data->mItemId;
	mPayData->mMoney = data->mMoney;
	mPayData->mYuanbao = data->mYuanbao;

	if (mPayData->mItemId)
	{
		ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(mPayData->mItemId);
		if (ItemInfo)
		{
			string str = ItemInfo->C_icon;
			if (str.length() == 0 )
			{
				UTRACE("load skin  failed!");
			}

			int pos = str.find(";");
			std::string skinstr = str.substr(0,pos);
			std::string strIndex = str.substr(pos+1,str.length() -1);


			mItemSkin = sm_System->LoadSkin(skinstr.c_str());
			mItemSkinIndex = atoi(strIndex.c_str());
		}
	}
	if (mPayData->mMoney)
	{
		mMoney.SetMoney(mPayData->mMoney);
	}
	else
	{
		mMoney.SetMoney(mPayData->mYuanbao, TRUE);
	}
	
	
}
void UNPCTradeExplain::UPayControl::ShowTips(UPoint pt)
{

	TipSystem->HideTip();
	if (mPayData && mPayData->mItemId)
	{
		char buf[MAX_PATH];
		string Tem ;
		Tem = itoa(mPayData->mItemCount, buf, 10);
		UStringW TemStr;
		TemStr.Set(Tem.c_str());
		int TextWidth = m_Style->m_spFont->GetStrNWidth(TemStr, TemStr.GetLength());


		URect PayDataRec = GetControlRect() ;
		URect payIconRec = URect(PayDataRec.left , PayDataRec.bottom - 16  , PayDataRec.left + 16 + TextWidth, PayDataRec.bottom);

		UPoint tPos = GetParent()->WindowToScreen(m_Position); 

		if (payIconRec.PointInRect(pt))
		{
			TipSystem->ShowTip(TIP_TYPE_ITEM,mPayData->mItemId,tPos + UPoint(PayDataRec.left + 16 + TextWidth, PayDataRec.bottom),0, mPayData->mItemCount);
		}
	}

	
}

void UNPCTradeExplain::UPayControl::ShowItemTips(UPoint pt)
{
	if (mPayData)
	{
		TipSystem->ShowTip(mEntry,pt,mPayData,mItemSkin,mItemSkinIndex);
	}
}
void UNPCTradeExplain::UPayControl::HidTips()
{
	TipSystem->HideTip();
}
void UNPCTradeExplain::UPayControl::OnMouseEnter(const UPoint& pt, UINT uFlags)
{
	UControl::OnMouseEnter(pt, uFlags);
	ShowTips(pt);
}
void UNPCTradeExplain::UPayControl::OnMouseLeave(const UPoint& pt, UINT uFlags)
{
	UControl::OnMouseLeave(pt, uFlags);
	HidTips();
	
}
void UNPCTradeExplain::UPayControl::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (mPayData)
	{
		UPoint DrawOffset ;
		DrawOffset = offset;
		
		UPoint DrawIconSize;
		DrawIconSize.Set(16,16); 
		
		URect DrawIconRec ;
		char buf[MAX_PATH];
		
		

		string Tem ;
		UStringW TemStr;
		URect TextureRec ;

		UINT TextWidth = 0;
		UPoint TextSize ;
		TextSize.Set(0, updateRect.GetHeight());

		if (mPayData->mItemId && mItemSkin)
		{
			//draw item
			Tem = itoa(mPayData->mItemCount, buf, 10);
			TemStr.Set(Tem.c_str());

			TextWidth = m_Style->m_spFont->GetStrNWidth(TemStr, TemStr.GetLength());
			
			TextSize.x = DrawOffset.x + TextWidth;
			//text
			UDrawText(sm_UiRender, m_Style->m_spFont,DrawOffset, TextSize, LCOLOR_WHITE, Tem.c_str());
			
			DrawOffset.x += TextWidth ;

			DrawIconRec.SetPosition(DrawOffset);
			DrawIconRec.SetSize(DrawIconSize);

			sm_UiRender->DrawSkin(mItemSkin, mItemSkinIndex, DrawIconRec);
			DrawOffset.x += DrawIconRec.GetWidth() + 2;
			
		}
		if (mPayData->mHonor)
		{
			Tem = itoa(mPayData->mHonor, buf, 10);
			TemStr.Set(Tem.c_str());

			TextWidth = m_Style->m_spFont->GetStrNWidth(TemStr, TemStr.GetLength());

			TextSize.x = DrawOffset.x + TextWidth;
			//text
			UDrawText(sm_UiRender, m_Style->m_spFont,DrawOffset, TextSize, LCOLOR_WHITE, Tem.c_str());

			DrawOffset.x += TextWidth ;

			DrawIconRec.SetPosition(DrawOffset);
			DrawIconRec.SetSize(DrawIconSize);
			
			TextureRec.SetPosition(0,0);
			TextureRec.SetSize(mTexture[0]->GetWidth(), mTexture[0]->GetHeight());

			sm_UiRender->DrawImage(mTexture[0],DrawIconRec,TextureRec);
			DrawOffset.x += DrawIconRec.GetWidth() + 2;
		}
		if (mPayData->mGuid_Gongxian)
		{
			Tem = itoa(mPayData->mGuid_Gongxian, buf, 10);
			TemStr.Set(Tem.c_str());
			
			TextWidth = m_Style->m_spFont->GetStrNWidth(TemStr, TemStr.GetLength());

			TextSize.x = DrawOffset.x + TextWidth;
			//text
			UDrawText(sm_UiRender, m_Style->m_spFont,DrawOffset, TextSize, LCOLOR_WHITE, Tem.c_str());

			DrawOffset.x += TextWidth ;

			DrawIconRec.SetPosition(DrawOffset);
			DrawIconRec.SetSize(DrawIconSize);

			TextureRec.SetPosition(0,0);
			TextureRec.SetSize(mTexture[2]->GetWidth(), mTexture[2]->GetHeight());

			sm_UiRender->DrawImage(mTexture[2],DrawIconRec,TextureRec);
			DrawOffset.x += DrawIconRec.GetWidth() + 2;
			
		}
		if (mPayData->mGuid_Jifen)
		{
			Tem = itoa(mPayData->mGuid_Jifen, buf, 10);
			TemStr.Set(Tem.c_str());
			
			TextWidth = m_Style->m_spFont->GetStrNWidth(TemStr, TemStr.GetLength());

			TextSize.x = DrawOffset.x + TextWidth;
			//text
			UDrawText(sm_UiRender, m_Style->m_spFont,DrawOffset, TextSize, LCOLOR_WHITE, Tem.c_str());

			DrawOffset.x += TextWidth ;

			DrawIconRec.SetPosition(DrawOffset);
			DrawIconRec.SetSize(DrawIconSize);

			TextureRec.SetPosition(0,0);
			TextureRec.SetSize(mTexture[1]->GetWidth(), mTexture[1]->GetHeight());

			sm_UiRender->DrawImage(mTexture[1],DrawIconRec,TextureRec);
			DrawOffset.x += DrawIconRec.GetWidth() + 2;
		
		}
		if (mPayData->mArena)
		{
			Tem = itoa(mPayData->mArena, buf, 10);
			TemStr.Set(Tem.c_str());

			TextWidth = m_Style->m_spFont->GetStrNWidth(TemStr, TemStr.GetLength());

			TextSize.x = DrawOffset.x + TextWidth;
			//text
			UDrawText(sm_UiRender, m_Style->m_spFont,DrawOffset, TextSize, LCOLOR_WHITE, Tem.c_str());

			DrawOffset.x += TextWidth ;

			DrawIconRec.SetPosition(DrawOffset);
			DrawIconRec.SetSize(DrawIconSize);

			TextureRec.SetPosition(0,0);
			TextureRec.SetSize(mTexture[3]->GetWidth(), mTexture[3]->GetHeight());

			sm_UiRender->DrawImage(mTexture[3],DrawIconRec,TextureRec);
			DrawOffset.x += DrawIconRec.GetWidth() + 2;
		}
		if (mPayData->mMoney || mPayData->mYuanbao)
		{
			mMoney.DrawMoney(sm_UiRender, m_Style->m_spFont, DrawOffset, updateRect.GetSize(), UT_LEFT);
		}

	}
}

BOOL UNPCTradeExplain::UPayControl::InitTexture()
{
	mTexture[0] = sm_UiRender->LoadTexture("Data\\UI\\NPCTrade\\honor.png");
	mTexture[1] = sm_UiRender->LoadTexture("Data\\UI\\NPCTrade\\sorce.png");
	mTexture[2] = sm_UiRender->LoadTexture("Data\\UI\\NPCTrade\\gongxian.png");
	mTexture[3] = sm_UiRender->LoadTexture("Data\\UI\\NPCTrade\\Arena.png");

	for (int i = 0; i < 4; i++)
	{
		if (mTexture[i] == NULL)
		{
			return FALSE;
		}
	}

	return TRUE;
}