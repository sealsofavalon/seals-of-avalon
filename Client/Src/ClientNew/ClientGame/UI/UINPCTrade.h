#pragma once 
#include "UInc.h"
#include "UItemContainerEx.h"
#include "USkillButton.h"
#include "UPayDataInc.h"
#include "ItemTip.h"

#define NPCTradePageCount 12

enum USYWareShowType
{
	USYWareAll = 0,   //全部
	USYWareClass,     //职业  默认
	USYWareCanUse,    //可用
};
struct stExItem : public MSG_S2C::stItem_List_Inventroy::stItem
{
	stExItem():price(0){}
	ui32 price;

	stExItem & operator=(const MSG_S2C::stItem_List_Inventroy::stItem& item)
	{
		if (this == &item)
		{
			return *this;
		}
		count = item.count;
		itemid = item.itemid;
		amt = item.amt;
		BuyCount = item.BuyCount;
		ExtendedCost = item.ExtendedCost;
		price = 0;
		return *this;
	}
};

class  UNPCTradeExplain : public UControl
{
	UDEC_CLASS(UNPCTradeExplain);
	UDEC_MESSAGEMAP();
public:
	UNPCTradeExplain();
	virtual ~UNPCTradeExplain();
public:
	void OnInit(MSG_S2C::stItem_List_Inventroy::stItem* pSaleItem, UINT pos, ui64 guid, bool bBuyBack = false);
	void OnClear();
	//需要支付的数据显示窗口。
	class UPayControl : public UControl
	{
		UDEC_CLASS(UNPCTradeExplain::UPayControl);
		UDEC_MESSAGEMAP();
	public:
		UPayControl();
		~UPayControl();
		virtual BOOL OnCreate();
		virtual void OnDestroy();
		void SetPayData(PayData* data, ui32 Entry);
		void ClearData();
	public:
		void ShowTips(UPoint pt);
		void HidTips();
		void ShowItemTips(UPoint pt);

		virtual void OnMouseEnter(const UPoint& pt, UINT uFlags);
		virtual void OnMouseLeave(const UPoint& pt, UINT uFlags);
	protected:
		BOOL InitTexture();
		virtual void OnRender(const UPoint& offset,const URect &updateRect);
		
	private:
		UMoney  mMoney ; //钱 (单位铜)
		ui32 mEntry;
		PayData* mPayData;
		UTexturePtr mTexture[4]; // 0--荣誉 1--部族积分 2--部族贡献， 3--竞技积分
		USkinPtr mItemSkin ;
		UINT  mItemSkinIndex ;
	};


protected:
	BOOL CheckCanBuy(string& text);
	void OnClickBuy();   //点击购买
	
	void SetShowPayData();
	void ShowInPut();
protected:
	BOOL virtual OnCreate();
	void virtual OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
	virtual void OnMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
	virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */);
protected:
	MSG_S2C::stItem_List_Inventroy::stItem* m_SaleItem;
	USkillButton* m_ItemInfo;
	UStaticText* m_ItemName;

	UPayControl* m_pkPayControl;

	ui64 m_NPCguid;
	int m_nClickBuy;
	float m_Clicktime;
	bool m_bBuyBack;
private:
};


class UNPCTrade : public UDialog
{
	UDEC_CLASS(UNPCTrade);
	UDEC_MESSAGEMAP();
public:
	UNPCTrade();
	virtual ~UNPCTrade();
	void OnItemList(MSG_S2C::stItem_List_Inventroy& sList);
	ui64 GetNpcId(){return m_NPCguid;}
	BOOL IsRepair(){return m_bRepair;} // 是否进入修理状态
	void SetRepair(BOOL repair);
	void SetNPCHead();
	void OnShowMoney(UINT Num);

	//////////////////////////////////////////////////////////////////////////
	static void BuyItem();
	static void BuyOneItem();
	static void BuyCancel();
	void SetCurSelItem(MSG_S2C::stItem_List_Inventroy::stItem* pItem, int pos);
	void SetCUSNormal();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnClose();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual BOOL OnEscape();

	virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom,const UPoint& point, const void* DragData, UINT nDataFlag);
protected:
	void OnSetListToExplain(UINT pos, MSG_S2C::stItem_List_Inventroy::stItem* data);
private:
	void ClearUI();
	void OnPageUp();      //翻页
	void OnPageDown();    //翻页
	void OnRepairOne();   //修理一件
	void OnRepairAll();   //修理所有

	void OnClickTypeShop();
	void OnClickTypeBuyBack();

	void SetShowList(USYWareShowType pType);
	void OnSelChangShowType();
public:
	void OnSetPageList();
	bool GetBuyBackState(){return m_bBuyBack;}
protected:
	vector<MSG_S2C::stItem_List_Inventroy::stItem> m_ShowSaleItemList;
	vector<MSG_S2C::stItem_List_Inventroy::stItem> m_SaleItemList;
	MSG_S2C::stItem_List_Inventroy::stItem m_BuyBackList[12];
	ui64 m_NPCguid;
	UINT m_numPage;   //总页数
	UINT m_curPage;   //当前页数

	BOOL m_bRepair;   //是否修理
	BOOL m_bRepairNpc; //NPC是否具有修理功能

	MSG_S2C::stItem_List_Inventroy::stItem* m_CurSelItem;
	int m_buyslot;

	//列表  每页11个位置
	UNPCTradeExplain*  m_SalePos_0;
	UNPCTradeExplain*  m_SalePos_1;
	UNPCTradeExplain*  m_SalePos_2;
	UNPCTradeExplain*  m_SalePos_3;
	UNPCTradeExplain*  m_SalePos_4;
	UNPCTradeExplain*  m_SalePos_5;
	UNPCTradeExplain*  m_SalePos_6;
	UNPCTradeExplain*  m_SalePos_7;
	UNPCTradeExplain*  m_SalePos_8;
	UNPCTradeExplain*  m_SalePos_9;
	UNPCTradeExplain*  m_SalePos_10;
	UNPCTradeExplain*  m_SalePos_11;

	USYWareShowType m_ShowType;
	UComboBox* m_TypeComboBox ;

	bool m_bBuyBack;
};