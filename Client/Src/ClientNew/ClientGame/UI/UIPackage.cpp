#include "StdAfx.h"
#include "UIPackage.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "UIItemSystem.h"
#include "Player.h"
#include "ObjectManager.h"
#include "ItemSlot.h"
#include "Network/PacketBuilder.h"
#include "UIMailSend.h"
#include "UInputAmount.h"
#include "UI/UMessageBox.h"
#include "UIPlayerTrade.h"
#include "UFun.h"
#include "UChat.h"
#include "UIBank.h"
#include "UINPCTrade.h"
#include "UInGameBar.h"
#include "ItemManager.h"
#include "SyServerTime.h"
#include "UAuctionMgr.h"
#include "ClientApp.h"
#include "SocialitySystem.h"
#include "../PlayerInputMgr.h"

//-----------------------------------------------------------------------------------------
UIMP_CLASS(UPackage::BageSolt,UDynamicIconButtonEx);
void UPackage::BageSolt::StaticInit()
{

}
UPackage::BageSolt::BageSolt()
{
	m_IsTickable = FALSE;
	m_TypeID = UItemSystem::ICT_BAG_SOLT;
	m_ActionContener.SetActionItem(new UPackage::BagSoltItem);
}
UPackage::BageSolt::~BageSolt()
{

}
void UPackage::BageSolt::OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag)
{
	switch (nDataFlag)
	{
	case UItemSystem::ICT_BAG:
		if (GetParent())
		{
			UPackage * pPackage = UDynamicCast(UPackage,GetParent());
			if (pPackage)
			{
				const ui8 * Frompos = (const ui8*)pDragData;
				const ui32 ItemId = pPackage->GetItemIdByPos(*Frompos);
				ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(ItemId);
				if (ItemInfo && ItemInfo->Class == ITEM_CLASS_CONTAINER)
				{
					m_bAcceptDrag = TRUE;
				}
			}
		}
		break;
	}
}
BOOL UPackage::BageSolt::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (UDynamicIconButtonEx::OnMouseUpDragDrop(pDragFrom,point,pDragData,nDataFlag))
	{
		switch (nDataFlag)
		{
		case UItemSystem::ICT_BAG:
			{
				if (GetParent())
				{
					UPackage * pPackage = UDynamicCast(UPackage,GetParent());
					if (pPackage)
					{
						const ui8 * Frompos = (const ui8*)pDragData;
						const ui32 ItemId = pPackage->GetItemIdByPos(*Frompos);
						ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(ItemId);
						if (ItemInfo && ItemInfo->Class == ITEM_CLASS_CONTAINER)
						{
							SYState()->UI->GetUItemSystem()->MoveItemMsg(nDataFlag,m_TypeID,*Frompos,GetItemData()->pos,1);
						}
					}
				}
			}
			return TRUE;
		}
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}
//-----------------------------------------------------------------------------------------
UIMP_CLASS(UPackage,UPackageDialog);
void UPackage::StaticInit()
{

}
UBEGIN_MESSAGE_MAP(UPackage,UPackageDialog)
UON_UGD_DBCLICKED(0, &UPackage::OnLeftClickBag)
UON_UGD_UPAG_RBCLICKED(0,&UPackage::OnRightClickBag)
//UON_UGD_UPAG_LBCLICKED(0,&UPackage::OnLbClickBag)
UON_BN_CLICKED(13,&UPackage::NeatenBag)
UON_UGD_UPAG_SHIFT_AND_RBCLICKED(0,&UPackage::OnSplit)
UEND_MESSAGE_MAP()


UPackage::UPackage()
{
	m_ItemContainer = NULL;

	m_Gold = NULL;

	m_PagSolt_1 = NULL;
	m_PagSolt_2 = NULL;
	m_PagSolt_3 = NULL;
	m_PagSolt_4 = NULL;
	m_PagSolt_5 = NULL;
	m_PagSolt_6 = NULL;
	m_PagSolt_7 = NULL;
	m_PagSolt_8 = NULL;

	m_CurSelPos = -1;
	m_SplitPos = -1;
	m_bCanDrag = TRUE;

}
UPackage::~UPackage()
{

}
void UPackage::BagSoltItem::Use()
{

}
BOOL UPackage::OnCreate()
{
	if (!UPackageDialog::OnCreate())
	{
		return FALSE;
	}

	m_ItemContainer = (UPackageContainer*)GetChildByID(0);
	if (m_ItemContainer == NULL)
	{
		return FALSE;
	}
	m_ItemContainer->SetGridCount(6,9);
	m_ItemContainer->SetGridSize(UPoint(42,43));
	m_ItemContainer->SetDragType(UItemSystem::ICT_BAG);

	m_Gold   = UDynamicCast(UMoneyText,GetChildByID(1));
	m_YuanBao = UDynamicCast(UMoneyText, GetChildByID(12));

	if (m_Gold == NULL)
	{
		return FALSE;
	}

	if (!InitBagSolt())
	{
		return FALSE;
	}
	
	SetCanUseSlort(INVENTORY_SLOT_ITEM_END - INVENTORY_SLOT_ITEM_START);
	
	if (GetChildByID(13))
	{
		GetChildByID(13)->SetActive(TRUE);
	}else
	{
		return FALSE ;
	}
	

	return TRUE;
}

BOOL UPackage::InitBagSolt()
{
	m_PagSolt_1 = (BageSolt*) GetChildByID(4);
	m_PagSolt_2 = (BageSolt*) GetChildByID(5);
	m_PagSolt_3 = (BageSolt*) GetChildByID(6);
	m_PagSolt_4 = (BageSolt*) GetChildByID(7);
	m_PagSolt_5 = (BageSolt*) GetChildByID(8);
	m_PagSolt_6 = (BageSolt*) GetChildByID(9);
	m_PagSolt_7 = (BageSolt*) GetChildByID(10);
	m_PagSolt_8 = (BageSolt*) GetChildByID(11);

	if (m_PagSolt_1 == NULL || m_PagSolt_2 == NULL || m_PagSolt_3 == NULL || m_PagSolt_4 == NULL ||
		m_PagSolt_5 == NULL || m_PagSolt_6 == NULL || m_PagSolt_7 == NULL || m_PagSolt_8 == NULL )
	{
		return FALSE;
	}
	//初始化

	for (int index = INVENTORY_SLOT_BAG_START; index < INVENTORY_SLOT_BAG_END; index++)
	{
		ShowUseBag(index, 0);
	}

	return TRUE;
}

void UPackage::OnDestroy()
{
	UPackageDialog::OnDestroy();
}

void UPackage::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	UPackageDialog::OnRightMouseDown(position,nRepCnt,flags);
	//OnClose();
}
void UPackage::OnClose()
{
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pInGame)
	{
		UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
		if (pGameBar)
		{
			UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONPACKAGE);
			if (pBtn)
			{
				pBtn->SetCheck(TRUE);
			}
		}
	}
}
void UPackage::OnTimer(float fDeltaTime)
{
	//UControl::OnTimer(fDeltaTime);
}
void UPackage::SetIconByPos(ui8 pos, ui32 ditemId,UItemSystem::ItemType type, int num,bool bLock)
{
	//m_ItemContainer->SetItemType(type);
	ItemMgr->HandleFreshManTip(ditemId);
	ItemExtraData temp;
	m_ItemContainer->SetItemByPos(pos, ditemId, num, temp, bLock);
	m_ItemContainer->SetDragType(UItemSystem::ICT_BAG);
}

ui32 UPackage::GetItemIdByPos(int pos)
{
	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if (localPlayer)
	{
		CStorage* pContainer = localPlayer->GetItemContainer();

		if (pContainer)
		{
			CItemSlot* pSolt = (CItemSlot*)pContainer->GetSlot(pos);
			if (pSolt && pSolt->GetCode() != 0)
			{
				if ( pSolt->GetItemProperty() )
				{
					return pSolt->GetItemProperty()->ItemId ;
				}

				return 0;
				
			}else
			{
				return 0 ;
			}
		}		

	}
	return 0 ;
}

int  UPackage::GetNumByPos(int pos)
{
	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if (localPlayer)
	{
		CStorage* pContainer = localPlayer->GetItemContainer();

		if (pContainer)
		{
			CItemSlot* pSolt = (CItemSlot*)pContainer->GetSlot(pos);
			if (pSolt)
			{
				return pSolt->GetNum();
			}else
			{
				return 0 ;
			}
		}		

	}

	return 0;
}

void UPackage::RefurbishPag()
{
	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if (localPlayer)
	{
		CStorage* pContainer = localPlayer->GetItemContainer();

		if (pContainer)
		{
			for (unsigned int i = 0; i < m_ItemContainer->GetCanUseNob();i++)
			{
				CItemSlot* pSolt = (CItemSlot*)pContainer->GetSlot(i);
				if (pSolt)
				{
					if (pSolt->GetCode() !=0)
					{
						SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG,i,pSolt->GetCode(),pSolt->GetNum(),UItemSystem::ITEM_DATA_FLAG,pSolt->IsLocked());
					}else
					{
						SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG,i,0,0,UItemSystem::ITEM_DATA_FLAG);
					}
				}
			}
		}		

	}
}

void UPackage::OnLeftClickBag()
{
	//UPoint u_Pos = m_ItemContainer->GetSelGrid();
	//UPoint u_Count = m_ItemContainer->GetGridCount();

	//WORD pos = u_Pos.y * u_Count.x + u_Pos.x;

	//if (pos >= 0 && pos < m_ItemContainer->GetCanUseNob())
	//{
	//	CPlayerLocal* localplayer = ObjectMgr->GetLocalPlayer();
	//	if (localplayer == NULL)
	//	{
	//		return ;
	//	}
	//	CStorage* pkItemSlotCon =  localplayer->GetItemContainer();
	//	CItemSlot* pkItemSlot = (CItemSlot*)pkItemSlotCon->GetSlot((ui8)pos);

	//	if (pkItemSlot == NULL)
	//	{
	//		return;
	//	}

	//	SYItem * Item = (SYItem *)ObjectMgr->GetObject(pkItemSlot->GetGUID());
	//	if (Item == NULL)
	//	{
	//		return ;
	//	}

	//	// 左键点击物品栏操作, 
	//	// 修理操作
	//	// 玩家自己的交易. 也使用左键点击自动到交易界面.
	//	// 邮件发送界面. 
	//	if (!UseRepairItem(pkItemSlot,pos))
	//	{
	//		if (!UseToAuctionItem(pkItemSlot, pos))
	//		{
	//			UseToMailItem(pkItemSlot,pos);
	//		}
	//	}
	//}
}
int8 UPackage::GetNullBagSlot()
{
	int8 toBagSlot = -1 ;

	if (m_PagSolt_1->GetItemData()->entry == 0)
	{
		toBagSlot = m_PagSolt_1->GetItemData()->pos ;
	}else if (m_PagSolt_2->GetItemData()->entry == 0)
	{
		toBagSlot = m_PagSolt_2->GetItemData()->pos ;
	}else if (m_PagSolt_3->GetItemData()->entry == 0)
	{
		toBagSlot = m_PagSolt_3->GetItemData()->pos ;
	}else if (m_PagSolt_4->GetItemData()->entry == 0)
	{
		toBagSlot = m_PagSolt_4->GetItemData()->pos ;
	}else if (m_PagSolt_5->GetItemData()->entry == 0)
	{
		toBagSlot = m_PagSolt_5->GetItemData()->pos ;
	}else if (m_PagSolt_6->GetItemData()->entry == 0)
	{
		toBagSlot = m_PagSolt_6->GetItemData()->pos ;
	}else if (m_PagSolt_7->GetItemData()->entry == 0)
	{
		toBagSlot = m_PagSolt_7->GetItemData()->pos ;
	}else if (m_PagSolt_8->GetItemData()->entry == 0)
	{
		toBagSlot = m_PagSolt_8->GetItemData()->pos ;
	}

	return toBagSlot;
}
void UPackage::NeatenBag()
{
	//涉及背包LOCK操作.无法整理背包. 
	CPlayer* localplayer = ObjectMgr->GetLocalPlayer();
	if (localplayer == NULL)
	{
		return ;
	}
	CStorage* pkItemSlotCon =  localplayer->GetItemContainer();
	if (pkItemSlotCon->HasLockSolt())
	{
		UMessageBox::MsgBox_s( _TRAN("背包里有物品锁定，无法整理!") );
		ClientSystemNotify( _TRAN("背包里有物品锁定，无法整理!") );
		return ;
	}
	
	SYState()->UI->GetUItemSystem()->SetNeateningBag(true,false);
}
void UPackage::OnSplit() //拆分
{
	UNPCTrade * pNpcTrade = NULL;
	UMailSend* pMailSend = NULL;
	UPlayerTrade * pPlayerTrade = NULL;

	pNpcTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);
	pMailSend = INGAMEGETFRAME(UMailSend,FRAME_IG_MAILSENDDLG);
	pPlayerTrade = INGAMEGETFRAME(UPlayerTrade,FRAME_IG_PLAYERTRADEDLG);
	
	if (pNpcTrade && pNpcTrade->IsVisible())
	{
		return ;
	}

	if (pMailSend && pMailSend->IsVisible())
	{
		return;
	}
	if (pPlayerTrade && pPlayerTrade->IsVisible())
	{
		return ;
	}

	if (m_SplitPos >= 0)
	{
		return ;
	}
	UPoint u_Pos = m_ItemContainer->GetSelGrid();
	UPoint u_Count = m_ItemContainer->GetGridCount();

	WORD pos = u_Pos.y * u_Count.x + u_Pos.x;

	if (pos >= 0 && pos < u_Count.x * u_Count.y)
	{
		CPlayer* localplayer = ObjectMgr->GetLocalPlayer();
		if (localplayer == NULL)
		{
			return ;
		}
		CStorage* pkItemSlotCon =  localplayer->GetItemContainer();
		CItemSlot* pkItemSlot = (CItemSlot*)pkItemSlotCon->GetSlot((ui8)pos);

		if (pkItemSlot == NULL || pkItemSlot->GetCode() == 0)
		{
			return;
		}

		if (pkItemSlot->GetNum() == 1)
		{
			return ;
		}
		if (pkItemSlot->IsLocked())
		{
			return ;
		}
		m_SplitPos = pos ; 
		
		pkItemSlot->SetLock(true);
		SetIconByPos((ui8)pos,pkItemSlot->GetCode(), UItemSystem::ITEM_DATA_FLAG ,pkItemSlot->GetNum(), true);
		
		UInputAmount::InputAmount(BAG_SPLIT, pkItemSlot->GetNum(),(BoxFunction*)UPackage::SplitItem, (BoxFunction*)UPackage::SplitCancel);
	}

	
}
void UPackage::SetCurSel(UINT pos)
{
	if (pos >= 0 && pos < m_ItemContainer->GetCanUseNob())
	{
		m_CurSelPos = pos ;
	}
}
void UPackage::SaleItem()
{
	UPackage* pPackage = INGAMEGETFRAME(UPackage, FRAME_IG_PACKAGE);
	if (pPackage && pPackage->m_CurSelPos != -1)
	{
		UPoint u_Count = pPackage->m_ItemContainer->GetGridCount();

		WORD pos = pPackage->m_CurSelPos;

		if (pos >= 0 && pos < u_Count.x * u_Count.y)
		{
			CPlayer* localplayer = ObjectMgr->GetLocalPlayer();
			if (localplayer == NULL)
			{
				pPackage->m_CurSelPos = -1 ;
				return ;
			}
			CStorage* pkItemSlotCon =  localplayer->GetItemContainer();
			CItemSlot* pkItemSlot = (CItemSlot*)pkItemSlotCon->GetSlot((ui8)pos);

			if (pkItemSlot == NULL || pkItemSlot->GetCode() == 0)
			{
				pPackage->m_CurSelPos = -1;
				return;
			}
			
			SYState()->UI->GetUItemSystem()->SendUseSellMsg(pkItemSlot->GetGUID(),pkItemSlot->GetNum());
		}

		pPackage->m_CurSelPos = -1;
		
	}

}
void UPackage::CancelSale()
{
	UPackage* pPackage = INGAMEGETFRAME(UPackage, FRAME_IG_PACKAGE);
	if (pPackage)
	{
		pPackage->m_CurSelPos = -1;
		
	}
}

void UPackage::SplitCancel()
{
	UPackage* pPackage = INGAMEGETFRAME(UPackage, FRAME_IG_PACKAGE);
	if (pPackage && pPackage->m_SplitPos != -1)
	{
		UPoint u_Count = pPackage->m_ItemContainer->GetGridCount();
		WORD pos = pPackage->m_SplitPos;

		if (pos >= 0 && pos < u_Count.x * u_Count.y)
		{
			CPlayer* localplayer = ObjectMgr->GetLocalPlayer();
			if (localplayer == NULL)
			{
				pPackage->m_SplitPos = -1 ;
				return ;
			}
			CStorage* pkItemSlotCon =  localplayer->GetItemContainer();
			CItemSlot* pkItemSlot = (CItemSlot*)pkItemSlotCon->GetSlot((ui8)pos);

			if (pkItemSlot == NULL)
			{
				pPackage->m_SplitPos = -1 ;
				return;
			}
			pkItemSlot->SetLock(false);
			pPackage->SetIconByPos((ui8)pos,pkItemSlot->GetCode(), UItemSystem::ITEM_DATA_FLAG ,pkItemSlot->GetNum(), false);
		}

		pPackage->m_SplitPos = -1 ;

	}
}
void UPackage::SplitItem()
{

	UPackage* pPackage = INGAMEGETFRAME(UPackage, FRAME_IG_PACKAGE);
	if (pPackage && pPackage->m_SplitPos != -1)
	{
		UPoint u_Count = pPackage->m_ItemContainer->GetGridCount();

		WORD pos = pPackage->m_SplitPos;

		if (pos >= 0 && pos < u_Count.x * u_Count.y)
		{
			CPlayer* localplayer = ObjectMgr->GetLocalPlayer();
			if (localplayer == NULL)
			{
				SplitCancel();
				return ;
			}
			CStorage* pkItemSlotCon =  localplayer->GetItemContainer();
			CItemSlot* pkItemSlot = (CItemSlot*)pkItemSlotCon->GetSlot((ui8)pos);

			if (pkItemSlot == NULL)
			{
				SplitCancel();
				return;
			}
			int  uToSplitpos = pPackage->GetNoneItemPos() ;
			if (uToSplitpos == -1)
			{
				SplitCancel();
				UMessageBox::MsgBox_s( _TRAN("没有空位置！") );
				return ;
			}

			UINT SplitCount = UInputAmount::GetAmount();
			
			ui8 srcBag = pkItemSlotCon->GetServerBag(pos);
			ui8 srcPos = pkItemSlotCon->GetServerPos(pos);

			ui8 toBag = pkItemSlotCon->GetServerBag(uToSplitpos);
			ui8 toPos = pkItemSlotCon->GetServerPos(uToSplitpos);

			if (srcPos != -1 && srcBag != -1 && toBag != -1 && toPos != -1 && SplitCount >0 && SplitCount < pkItemSlot->GetNum())
			{
				PacketBuilder->SendSplitItem(srcBag,srcPos,toBag,toPos,SplitCount);
			}

		}
	}	

	SplitCancel();
}

void UPackage::ShowYuanBao(ui32 uiYuanBao)
{
	m_YuanBao->SetMoney(uiYuanBao,TRUE);
}

void UPackage::ShowMoneyNum(UINT Num)
{
	if (Num <= 0)
	{
		Num = 0 ;
	}
	m_Gold->SetMoney(Num);
}
void UPackage::OnRender(const UPoint& offset, const URect &updateRect)
{
	UPackageDialog::OnRender(offset,updateRect);
}

void UPackage::SetRenderRect()
{
	m_HeadRect = URect(0, 0 , 411, 90);        //头部区域
	m_LeftRect = URect(0, 90, 21, 348);        //左边框
	m_RightRect= URect(406 , 90, 411, 348);       //右边框
	m_CenterRect= URect(21, 90, 406, 348);      //中间区域
	m_BottomRect = URect(0, 348, 411, 401);      //底部区域
}

void UPackage::ResizeControl(const UPoint &NewPos, const UPoint &newExtent)
{
	return UPackageDialog::ResizeControl(NewPos,newExtent);
}

// 便于快速换装寻找空位置
int  UPackage::GetNoneItemPos()
{
	UPoint u_Count = m_ItemContainer->GetGridCount();

	int nPos = -1 ;

	for ( int pos = 0; pos < u_Count.x * u_Count.y; pos++)
	{
		if (GetItemIdByPos(pos) == 0)
		{
			nPos = pos ;

			return nPos;
		}
	}

	return nPos ;
	
}

void UPackage::ShowUseBag(UINT index, ui32 bagItemid)
{
	if (index >= INVENTORY_SLOT_BAG_START && index < INVENTORY_SLOT_BAG_END)
	{

		ActionDataInfo dataInfo;
		dataInfo.entry = bagItemid;
		dataInfo.type = UItemSystem::ITEM_DATA_FLAG;
		dataInfo.pos = index ;

		if (m_PagSolt_1 == NULL || m_PagSolt_2 == NULL || m_PagSolt_3 == NULL || m_PagSolt_4 == NULL ||
			m_PagSolt_5 == NULL || m_PagSolt_6 == NULL || m_PagSolt_7 == NULL || m_PagSolt_8 == NULL )
		{
			return ;
		}
		BageSolt * pBagSolt = NULL;
		switch (index)
		{
		case INVENTORY_SLOT_BAG_1:
			pBagSolt = m_PagSolt_1;
			break;
		case INVENTORY_SLOT_BAG_2:
			pBagSolt = m_PagSolt_2;
			break;
		case INVENTORY_SLOT_BAG_3:
			pBagSolt = m_PagSolt_3;
			break;
		case INVENTORY_SLOT_BAG_4:
			pBagSolt = m_PagSolt_4;
			break;
		case INVENTORY_SLOT_BAG_5:
			pBagSolt = m_PagSolt_5;
			break;
		case INVENTORY_SLOT_BAG_6:
			pBagSolt = m_PagSolt_6;
			break;
		case INVENTORY_SLOT_BAG_7:
			pBagSolt = m_PagSolt_7;
			break;
		case INVENTORY_SLOT_BAG_8:
			pBagSolt = m_PagSolt_8;
			break;
		}
		if (pBagSolt)
		{
			pBagSolt->SetItemData(dataInfo);
		}
	}
}
void UPackage::SetVisible(BOOL value)
{
	UPackageDialog::SetVisible(value);
	if (value)
	{
		HELPEVENTTRIGGER(TUTORIAL_FLAG_HOW_TO_EQUIP);
	}
}
UINT UPackage::GetBagSlotCount()
{
	return m_ItemContainer->GetCanUseNob();
}

void UPackage::SetCanUseSlort(UINT count)
{
	m_pNeedTance = FALSE;

	URect GRec = m_Gold->GetControlRect();
	URect YBRec = m_YuanBao->GetControlRect();
	URect ZLBec = GetChildByID(13)->GetControlRect();

	UPoint Pag1_pos = m_PagSolt_1->GetWindowPos();
	UPoint Pag1_Size = m_PagSolt_1->GetWindowSize();

	UPoint Pag2_pos = m_PagSolt_2->GetWindowPos();
	UPoint Pag2_Size = m_PagSolt_2->GetWindowSize();

	UPoint Pag3_pos = m_PagSolt_3->GetWindowPos();
	UPoint Pag3_Size = m_PagSolt_3->GetWindowSize();

	UPoint Pag4_pos = m_PagSolt_4->GetWindowPos();
	UPoint Pag4_Size = m_PagSolt_4->GetWindowSize();

	UPoint Pag5_pos = m_PagSolt_5->GetWindowPos();
	UPoint Pag5_Size = m_PagSolt_5->GetWindowSize();

	UPoint Pag6_pos = m_PagSolt_6->GetWindowPos();
	UPoint Pag6_Size = m_PagSolt_6->GetWindowSize();

	UPoint Pag7_pos = m_PagSolt_7->GetWindowPos();
	UPoint Pag7_Size = m_PagSolt_7->GetWindowSize();

	UPoint Pag8_pos = m_PagSolt_8->GetWindowPos();
	UPoint Pag8_Size = m_PagSolt_8->GetWindowSize();

	UPoint PagOldSize = GetControlRect().GetSize();


	m_ItemContainer->ResetCanUseNob(count);

	UPoint PagNewSize = GetControlRect().GetSize();

	m_Gold->SetWindowRect(GRec);
	m_YuanBao->SetWindowRect(YBRec);
	GetChildByID(13)->SetWindowRect(ZLBec);

	//重设底部控件的相对位置
	m_PagSolt_1->SetTop(PagNewSize.y - (PagOldSize.y - Pag1_pos.y));
	m_PagSolt_1->SetLeft(PagNewSize.x - (PagOldSize.x - Pag1_pos.x));
	m_PagSolt_1->SetSize(Pag1_Size);

	m_PagSolt_2->SetTop(PagNewSize.y - (PagOldSize.y - Pag2_pos.y));
	m_PagSolt_2->SetLeft(PagNewSize.x - (PagOldSize.x - Pag2_pos.x));
	m_PagSolt_2->SetSize(Pag2_Size);

	m_PagSolt_3->SetTop(PagNewSize.y - (PagOldSize.y - Pag3_pos.y));
	m_PagSolt_3->SetLeft(PagNewSize.x - (PagOldSize.x - Pag3_pos.x));
	m_PagSolt_3->SetSize(Pag3_Size);

	m_PagSolt_4->SetTop(PagNewSize.y - (PagOldSize.y - Pag4_pos.y));
	m_PagSolt_4->SetLeft(PagNewSize.x - (PagOldSize.x - Pag4_pos.x));
	m_PagSolt_4->SetSize(Pag4_Size);

	m_PagSolt_5->SetTop(PagNewSize.y - (PagOldSize.y - Pag5_pos.y));
	m_PagSolt_5->SetLeft(PagNewSize.x - (PagOldSize.x - Pag5_pos.x));
	m_PagSolt_5->SetSize(Pag5_Size);

	m_PagSolt_6->SetTop(PagNewSize.y - (PagOldSize.y - Pag6_pos.y));
	m_PagSolt_6->SetLeft(PagNewSize.x - (PagOldSize.x - Pag6_pos.x));
	m_PagSolt_6->SetSize(Pag6_Size);

	m_PagSolt_7->SetTop(PagNewSize.y - (PagOldSize.y - Pag7_pos.y));
	m_PagSolt_7->SetLeft(PagNewSize.x - (PagOldSize.x - Pag7_pos.x));
	m_PagSolt_7->SetSize(Pag7_Size);

	m_PagSolt_8->SetTop(PagNewSize.y - (PagOldSize.y - Pag8_pos.y));
	m_PagSolt_8->SetLeft(PagNewSize.x - (PagOldSize.x - Pag8_pos.x));
	m_PagSolt_8->SetSize(Pag8_Size);

	RefurbishPag();
	if (GetRoot())
	{
		INT Bottom = 710 * GetRoot()->GetWindowSize().y / 768;
		SetBottom( Bottom );
	}
}

void UPackage::OnRightClickBag()
{
	//鼠标右键点击物品栏.
	//优先原则是先存入银行, 
	// 如果银行界面没有展开, 则优先实行出售操作, 
	// 如果商店系统没有打开, 则优先实行换装操作, 
	// 如果不是可装备物品. 这实行使用道具操作. 
	UPoint u_Pos = m_ItemContainer->GetSelGrid();
	UPoint u_Count = m_ItemContainer->GetGridCount();

	WORD pos = u_Pos.y * u_Count.x + u_Pos.x;


	if (pos >= 0 && pos < u_Count.x * u_Count.y)
	{
		CPlayerLocal* localplayer = ObjectMgr->GetLocalPlayer();
		if (localplayer == NULL)
		{
			return ;
		}

		CUstate pState = CPlayerLocal::GetCUState();

		CStorage* pkItemSlotCon =  localplayer->GetItemContainer();
		CItemSlot* pkItemSlot = (CItemSlot*)pkItemSlotCon->GetSlot((ui8)pos);
		if (pkItemSlot == NULL)
		{
			return;
		}
		SYItem* item = (SYItem*)ObjectMgr->GetObject(pkItemSlot->GetGUID());
		if (!item)
		{
			return ;
		}
		ItemPrototype_Client* itemPrototype = pkItemSlot->GetItemProperty();
		if (!itemPrototype)
		{
			return ;
		}

		int  num = pkItemSlot->GetNum(); 

		if (m_CurSelPos != -1) 
		{
			return ;
		}
		m_CurSelPos = pos;


		if ( SYState()->LocalPlayerInput->IsSelectingItem() )
		{
			SpellCastTargets spllTarget;
			spllTarget.m_targetMask = TARGET_FLAG_ITEM;
			spllTarget.m_itemTarget = pkItemSlot->GetGUID();

			SYState()->LocalPlayerInput->OnSkillToItemAction( spllTarget );
			m_CurSelPos = -1;
			return;
		}
		// 这里会进行判断是否能进行操作。因为在出售高品质物品时候会有对话框来阻断操作。
		// 
		
		
		
		if (!UseItemToBank(pkItemSlot,pos)) //存银行
		{
			if (!UseRepairItem(pkItemSlot,pos)) //修理单个
			{
				if (!UseToAuctionItem(pkItemSlot, pos)) //拍卖行
				{
					if (!UseToMailItem(pkItemSlot,pos)) //邮箱
					{
						if (!UseToPlayerTrade(pkItemSlot,pos)) //  玩家交易
						{
							if (!UseSaleItem(pkItemSlot,pos)) //出售
							{
								if (!UseDoSpecialItem(pkItemSlot,pos)) //特殊道具使用
								{
									if (!UseBagItem(pkItemSlot,pos)) //使用背包类道具
									{
										if (!UseToEquipment(pkItemSlot,pos)) //换装
										{
											if (!UseChatProps(pkItemSlot, pos)) //大喇叭 和小喇叭
											{
												UseItemToObj(pkItemSlot,pos); //使用物品
											}
										}
									}
								}
							}else
							{
								return ; // 出售的时候需要保存当前的数据
							}
						}
					}
				}
				
			}
		}
		m_CurSelPos = -1;
	}	
}

// void UPackage::OnLbClickBag()
// {
// 	UPoint u_Pos = m_ItemContainer->GetSelGrid();
// 	UPoint u_Count = m_ItemContainer->GetGridCount();
// 
// 	WORD pos = u_Pos.y * u_Count.x + u_Pos.x;
// 
// 
// 	if (pos >= 0 && pos < u_Count.x * u_Count.y)
// 	{
// 		CPlayerLocal* localplayer = ObjectMgr->GetLocalPlayer();
// 		if (localplayer == NULL)
// 		{
// 			return ;
// 		}
// 		CUstate pState = CPlayerLocal::GetCUState();
// 
// 		CStorage* pkItemSlotCon =  localplayer->GetItemContainer();
// 		CItemSlot* pkItemSlot = (CItemSlot*)pkItemSlotCon->GetSlot((ui8)pos);
// 
// 		ui64 guid = pkItemSlot->GetGUID();
// 
// 		//if ( ItemMgr->GetUsingItem() && guid )
// 		//{
// 		//	ItemMgr->UnBindItemReq( guid );
// 		//	SYState()->ClientApp->SetCursor(SYC_ARROW);
// 		//}
// 	}
// 
// 	//ItemMgr->SetItemUsed();
// }

//左键操作
BOOL UPackage::UseRepairItem(CItemSlot* pkItemSlot, int pos) //修理操作
{
	UNPCTrade* pkNPCTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);
	if (pkNPCTrade && pkNPCTrade->IsRepair() && CPlayerLocal::GetCUState() == cus_NPCTrade)
	{
		SYState()->UI->GetUItemSystem()->RepairOne(pkNPCTrade->GetNpcId(), pkItemSlot->GetGUID());
		return  TRUE;
	}
	return FALSE;
}
BOOL UPackage::UseToMailItem(CItemSlot* pkItemSlot, int pos) //邮件附件操作
{
	SYItem * Item = (SYItem *)ObjectMgr->GetObject(pkItemSlot->GetGUID());
	ItemPrototype_Client* pkItemInfo =  Item->GetItemProperty();
	UMailSend* pMailSend = INGAMEGETFRAME(UMailSend, FRAME_IG_MAILSENDDLG);

	if (pMailSend && CPlayerLocal::GetCUState() == cus_Mail)
	{
		if(!Item)
			return FALSE;

		CPlayerLocal* localPlayer = ObjectMgr->GetLocalPlayer();
		if(!localPlayer->HasFlag(PLAYER_FLAGS, PLAYER_FLAG_GM))
		{
			if(Item->HasFlag( ITEM_FIELD_FLAGS, ITEM_FLAG_QUEST | ITEM_FLAG_SOULBOUND ))
			{
				UMessageBox::MsgBox_s( _TRAN("该物品已经绑定！") );
				return TRUE; 
			}
			if (pkItemInfo && pkItemInfo->Class == ITEM_CLASS_QUEST)
			{
				UMessageBox::MsgBox_s( _TRAN("任务道具！") );
				return TRUE; 
			}
		}		

		BOOL bSendMail = pMailSend->IsVisible();
		if (bSendMail /*&& pMailSend->IsSaleItem()*/)
		{
			int sNUllpos = pMailSend->GetNullPos();
			if (sNUllpos != -1 && !pkItemSlot->IsLocked())
			{
				SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_BAG,UItemSystem::ICT_MAIL,(ui8)pos,(ui8)sNUllpos,pkItemSlot->GetNum());
			}else
			{
				if (sNUllpos == -1)
				{
					UMessageBox::MsgBox_s( _TRAN("附件已满！") );
				}
				if (pkItemSlot->IsLocked())
				{
					UMessageBox::MsgBox_s( _TRAN("改物品已经被锁定！") );
				}
			}
		}

		return TRUE;
	}
	return FALSE;
}
BOOL UPackage::UseToAuctionItem(CItemSlot* pkItemSlot, int pos)
{
	SYItem * Item = (SYItem *)ObjectMgr->GetObject(pkItemSlot->GetGUID());
	ItemPrototype_Client* pkItemInfo =  Item->GetItemProperty();

	if (CPlayerLocal::GetCUState() == cus_Auction && !pkItemSlot->IsLocked())
	{
		if(!Item)
			return FALSE;
		
		if(Item->HasFlag( ITEM_FIELD_FLAGS, ITEM_FLAG_QUEST | ITEM_FLAG_SOULBOUND ))
		{
			UMessageBox::MsgBox_s( _TRAN("该物品已经绑定！") );
			return TRUE; 
		}
		if (pkItemInfo && pkItemInfo->Class == ITEM_CLASS_QUEST)
		{
			UMessageBox::MsgBox_s( _TRAN("任务道具无法交易！") );
			return TRUE; 
		}
		 

		if (AUMgr->CheckOwerState())
		{
			SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_BAG, UItemSystem::ICT_AUC, pos, 0, pkItemSlot->GetNum());
			return TRUE;
		}else
		{
			return TRUE ;
		}
	}

	return FALSE;
}
BOOL UPackage::UseToPlayerTrade(CItemSlot* pkItemSlot, int pos) //到玩家交易界面
{
	SYItem * Item = (SYItem *)ObjectMgr->GetObject(pkItemSlot->GetGUID());
	ItemPrototype_Client* pkItemInfo =  Item->GetItemProperty();
	UPlayerTrade* pPlayerTrade = INGAMEGETFRAME(UPlayerTrade,FRAME_IG_PLAYERTRADEDLG);


	if (CPlayerLocal::GetCUState() == cus_Trade  && pPlayerTrade && !pkItemSlot->IsLocked())
	{
		m_CurSelPos = -1;
		if(!Item)
			return FALSE;
		CPlayerLocal* localPlayer = ObjectMgr->GetLocalPlayer();
		if(!localPlayer->HasFlag(PLAYER_FLAGS, PLAYER_FLAG_GM))
		{
			if(Item->HasFlag( ITEM_FIELD_FLAGS, ITEM_FLAG_QUEST | ITEM_FLAG_SOULBOUND ))
			{
				UMessageBox::MsgBox_s( _TRAN("该物品已经绑定！") );
				return TRUE; 
			}
			if (pkItemInfo && pkItemInfo->Class == ITEM_CLASS_QUEST)
			{
				UMessageBox::MsgBox_s( _TRAN("任务道具无法交易！") );
				return TRUE; 
			}
		} 		

		BOOL bCanTrade = pPlayerTrade->IsVisible();
		BOOL bLockTrade = pPlayerTrade->GetIsLock();

		if (bLockTrade)
		{
			UMessageBox::MsgBox_s( _TRAN("交易已锁定!") );
			return TRUE;
		}

		if (bCanTrade && !bLockTrade)
		{
			// 查找可用的 本地空的位置.
			m_CurSelPos = -1;
			int nTradePos  = pPlayerTrade->GetTradeNullPos();
			if (nTradePos != -1)
			{
				SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_BAG,UItemSystem::ICT_PLAYER_TRADE,(ui8)pos,(ui8)nTradePos,pkItemSlot->GetNum());
			}else
			{
				UMessageBox::MsgBox_s( _TRAN("交易栏位置已满!") );
			}
			return TRUE;
		}
	}


	return FALSE;
}
BOOL UPackage::UseDoSpecialItem(CItemSlot* pkItemSlot, int pos)
{
	if (SYState()->UI->GetUItemSystem()->IsUsedBangdItem() || SYState()->UI->GetUItemSystem()->IsUsedEnchantItem() )
	{
		
		ItemPrototype_Client* pkItem = pkItemSlot->GetItemProperty();
		if (pkItem && (pkItem->Class == ITEM_CLASS_WEAPON ||  pkItem->Class == ITEM_CLASS_ARMOR))
		{
			return SYState()->UI->GetUItemSystem()->DoUseSpecialItem("",pkItemSlot->GetGUID());
		}else
		{
			UMessageBox::MsgBox_s( _TRAN("该道具只能对装备和武器使用!") );
			SYState()->UI->GetUItemSystem()->CancelUseSpecialItem();
			return TRUE ;
		}
			
	}
	return FALSE ;
}
//右键操作
BOOL UPackage::UseItemToBank(CItemSlot* pkItemSlot, int pos) //存入银行
{
	UBank* pBank = INGAMEGETFRAME(UBank, FRAME_IG_BANKDLG);
	ItemPrototype_Client* itemPrototype = pkItemSlot->GetItemProperty();
	if (pBank->IsVisible() && CPlayerLocal::GetCUState() == cus_Bank)
	{
		int BankNullPos = pBank->GetBankNullPos();
		if (BankNullPos == -1)
		{
			UMessageBox::MsgBox_s( _TRAN("银行位置已满!") );
			return TRUE;
		}

		if (itemPrototype && itemPrototype->Class == ITEM_CLASS_QUEST)
		{
			UMessageBox::MsgBox_s( _TRAN("任务物品！无法进行当前操作！") );
			return TRUE;
		}

		SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_BAG,UItemSystem::ICT_BANK,(ui8)pos,BankNullPos,pkItemSlot->GetNum());
		return TRUE;

	}

	return FALSE;
}
BOOL UPackage::UseSaleItem(CItemSlot* pkItemSlot, int pos) //出售到系统NPC
{
	ItemPrototype_Client* itemPrototype = pkItemSlot->GetItemProperty();
	UNPCTrade* pNpcTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);

	if (pNpcTrade->IsVisible() && CPlayerLocal::GetCUState() == cus_NPCTrade )   //出售操作
	{
		ui64 guid = pkItemSlot->GetGUID();
		int  num = pkItemSlot->GetNum(); 


		if (!itemPrototype) 
		{
			m_CurSelPos = -1;
			return TRUE;
		}
		if (itemPrototype && itemPrototype->Class == ITEM_CLASS_QUEST)
		{
		
			UMessageBox::MsgBox_s( _TRAN("任务物品！无法出售！") );
			m_CurSelPos = -1;
			return TRUE;
		}

		if (itemPrototype->SellPrice <= 0)
		{
			
			ClientSystemNotify( _TRAN("该物品无法出售!") );
			m_CurSelPos = -1;
			return TRUE;
		}

		if (itemPrototype->Quality >= ITEM_QUALITY_UNCOMMON_GREEN )
		{
			//弹出对话框。
			UMessageBox::MsgBox( _TRAN("贵重物品，确认出售？"),(BoxFunction*)SaleItem,(BoxFunction*)CancelSale);
		}else
		{
			//发送出售消息
			SYState()->UI->GetUItemSystem()->SendUseSellMsg(guid,num);	
			m_CurSelPos = -1;
		}
		return TRUE;
	}

	return FALSE;
}
BOOL UPackage::UseBagItem(CItemSlot* pkItemSlot, int pos)
{
	CUstate pState = CPlayerLocal::GetCUState();
	if( pkItemSlot->GetItemProperty() &&  (pState == cus_Normal || pState == cus_NPC || pState == cus_Quest))
	{
		// 背包类道具. 使用后给
		if (pkItemSlot->GetItemProperty()->Class == ITEM_CLASS_CONTAINER)
		{
			//发使用增加包裹格子的消息

			int8 to_bag_slot = GetNullBagSlot();

			if (to_bag_slot != -1)
			{
				SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_BAG,UItemSystem::ICT_BAG_SOLT,(ui8)pos,to_bag_slot,1);
			}else
			{
				UMessageBox::MsgBox_s( _TRAN("物品栏包位已满!") );
			}
			return TRUE;

		}
		return FALSE;
	}

	return TRUE;
}
ui8 g_EquipMentPos = 0;
BOOL g_bIsEquip = FALSE;
void Equip()
{
	g_bIsEquip = TRUE;
	SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_BAG, UItemSystem::ICT_EQUIP, g_EquipMentPos, 0, 1);
}
void CancelEquip()
{
	g_bIsEquip = FALSE;
}

BOOL UPackage::UseToEquipment(CItemSlot* pkItemSlot, int pos)//换装操作
{
	CUstate pState = CPlayerLocal::GetCUState();
	if( pkItemSlot->GetItemProperty() &&  (pState == cus_Normal || pState == cus_NPC || pState == cus_Quest))
	{
		if( pkItemSlot->GetItemProperty()->Class == ITEM_CLASS_WEAPON || pkItemSlot->GetItemProperty()->Class == ITEM_CLASS_ARMOR )
		{
			//换装
			if(pkItemSlot->GetItemProperty()->Bonding == ITEM_BIND_ON_EQUIP && !pkItemSlot->IsBonding())
			{
				g_EquipMentPos = pos;
				UMessageBox::MsgBox( _TRAN("装备后将绑定，是否继续？") , (BoxFunction*)Equip, (BoxFunction*)CancelEquip);
				return g_bIsEquip;
			}
			else
			{
				SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_BAG,UItemSystem::ICT_EQUIP,(ui8)pos,0,1);
				return TRUE;
			}
		}
		return FALSE;
	}
	return TRUE;
}
BOOL UPackage::UseChatProps(CItemSlot* pkItemSlot, int pos) //使用聊天喇叭
{
	CUstate pState = CPlayerLocal::GetCUState();
	if( pkItemSlot->GetItemProperty() &&  (pState == cus_Normal || pState == cus_NPC || pState == cus_Quest))
	{
		if (pkItemSlot->GetCode() == SPECIAL_ITEM_XIAOLABA)
		{
			//小喇叭
			ChatSystem->ShowChatProps(1);
			return TRUE;
		}
		if (pkItemSlot->GetCode() == SPECIAL_ITEM_DALABA)
		{
			//大喇叭
			ChatSystem->ShowChatProps(0);
			return TRUE;
		}

		return FALSE;
	}
	return TRUE;
}
BOOL UPackage::UseItemToObj(CItemSlot* pkItemSlot, int pos)//对对象使用道具操作
{
	CUstate pState = CPlayerLocal::GetCUState();
	if( pkItemSlot->GetItemProperty() &&  (pState == cus_Normal || pState == cus_NPC || pState == cus_Quest))
	{
		CPlayerLocal* localplayer = ObjectMgr->GetLocalPlayer();
		CStorage* pkItemSlotCon =  localplayer->GetItemContainer();
		/*CItemSlot* pkItemSlot = (CItemSlot*)pkItemSlotCon->GetSlot((ui8)pos);*/

		if(pkItemSlot->IsPetEgg())
		{
			float fTotalTime = (float)pkItemSlot->PetEggTotalTime();
			float fLeftTime = (float)(::time(NULL) - gServerTime.GetDelayTime()  - pkItemSlot->PetEggGenTime());
			if(fLeftTime - fTotalTime < 0)
			{
				UMessageBox::MsgBox_s( _TRAN("没到使用时间!") );
				return FALSE;
			}
		}

		ui8 bag = pkItemSlotCon->GetServerBag(pos);
		ui8 slot = pkItemSlotCon->GetServerPos(pos);

		SpellCastTargets targets;
		if( ( pkItemSlot->GetItemProperty()->Class == ITEM_CLASS_CONSUMABLE
			&& ( pkItemSlot->GetItemProperty()->SubClass == ITEM_SUBCLASS_CONSUMABLE_POTION
			|| pkItemSlot->GetItemProperty()->SubClass == ITEM_SUBCLASS_CONSUMABLE_OTHER 
			|| pkItemSlot->GetItemProperty()->SubClass == ITEM_SUBCLASS_CONSUMABLE_FOOD ) )
			|| pkItemSlot->GetItemProperty()->Class == ITEM_CLASS_RECIPE
			|| pkItemSlot->GetItemProperty()->Class == ITEM_CLASS_USE
			|| (pkItemSlot->GetItemProperty()->Class == ITEM_CLASS_QUEST && pkItemSlot->GetItemProperty()->Bonding != ITEM_BIND_QUEST_TRIGGER))
		{
			//targets.m_targetMask |= TARGET_FLAG_SELF;
			targets.m_unitTarget = ObjectMgr->GetLocalPlayer()->GetGUID();
		}

		SYState()->UI->GetUItemSystem()->SendUseLeechdomMsg(bag, slot, 0, targets);
	}
	return TRUE;
}
BOOL UPackage::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}