#pragma once
#include "UInc.h"
#include "UPackageDialog.h"
#include "UPackageContainer.h"
#include "UDynamicIconButtonEx.h"
#include "ItemSlot.h"
#include "UIMoneyText.h"

class UPackage : public UPackageDialog
{
	class BageSolt : public UDynamicIconButtonEx
	{
		UDEC_CLASS(UPackage::BageSolt);
		BageSolt();
		virtual ~BageSolt();
	protected:
		virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
		virtual void OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag);
	};
	friend class UPackage::BageSolt;
	class BagSoltItem : public UActionItem
	{
	public:
		virtual void Use();
	};
	UDEC_CLASS(UPackage);
	UDEC_MESSAGEMAP();
public:
	UPackage();
	~UPackage();
	void SetIconByPos(ui8 pos, ui32 ditemId,UItemSystem::ItemType type, int num,bool bLock = false);  //设置格子信息                          
	ui32 GetItemIdByPos(int pos);//获取格子对应的ID
	int  GetNumByPos(int pos);//获得当前格子重叠物品的信息
	int  GetNoneItemPos();//获得包裹里的一个随机空位置
	int8 GetNullBagSlot();
	void OnLeftClickBag(); //

	void OnRightClickBag();
	//void OnLbClickBag();
	void RefurbishPag();   //刷新包裹.
	void ShowMoneyNum(UINT  Num);//显示金钱
	void ShowYuanBao(ui32 uiYuanBao);	//显示元宝
	
	void OnSplit(); //拆分
	static void SplitItem();
	static void SplitCancel();
	
	//
	void SetCurSel(UINT pos);
	static void SaleItem(); //确认出售
	static void CancelSale(); //取消出售

	//设置可以使用的格子
	void SetCanUseSlort(UINT count);
	UINT GetBagSlotCount();

	//设置使用道具包裹显示
	BOOL InitBagSolt();
	void ShowUseBag(UINT index, ui32 bagItemid);
	virtual void SetVisible(BOOL value);
protected:
	//左键操作
	BOOL UseRepairItem(CItemSlot* pkItemSlot, int pos); //修理操作
	BOOL UseToMailItem(CItemSlot* pkItemSlot, int pos); //邮件附件操作
	BOOL UseToPlayerTrade(CItemSlot* pkItemSlot, int pos); //到玩家交易界面
	BOOL UseToAuctionItem(CItemSlot* pkItemSlot, int pos); //到拍卖界面
	//右键操作
	BOOL UseDoSpecialItem(CItemSlot* pkItemSlot, int pos);
	BOOL UseItemToBank(CItemSlot* pkItemSlot, int pos); //存入银行
	BOOL UseSaleItem(CItemSlot* pkItemSlot, int pos); //出售到系统NPC
	BOOL UseBagItem(CItemSlot* pkItemSlot, int pos); //小包类物品
	BOOL UseToEquipment(CItemSlot* pkItemSlot, int pos);//换装操作
	BOOL UseItemToObj(CItemSlot* pkItemSlot, int pos);//对对象使用道具操作
	BOOL UseChatProps(CItemSlot* pkItemSlot, int pos); //使用聊天喇叭
	
	void NeatenBag();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnTimer(float fDeltaTime);
	virtual void OnClose();
	virtual BOOL OnEscape();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void SetRenderRect();
	virtual void ResizeControl(const UPoint &NewPos, const UPoint &newExtent);
private:
	UPackageContainer*  m_ItemContainer;
	INT m_CurSelPos ;
	INT m_SplitPos;
	//show money
	UMoneyText* m_Gold;

	UMoneyText* m_YuanBao;

	BageSolt*  m_PagSolt_1;
	BageSolt*  m_PagSolt_2;
	BageSolt*  m_PagSolt_3;
	BageSolt*  m_PagSolt_4;
	BageSolt*  m_PagSolt_5;
	BageSolt*  m_PagSolt_6;
	BageSolt*  m_PagSolt_7;
	BageSolt*  m_PagSolt_8;

};