#include "stdafx.h"
#include "UIPickItemList.h"
#include "ItemManager.h"
#include "Network/PacketBuilder.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "UFun.h"
#include "UITipSystem.h"
#include "ObjectManager.h"
#include "AudioDef.h"
#include "Hook/HookManager.h"

UIMP_CLASS(UPickItemList,UDialog)
UBEGIN_MESSAGE_MAP(UPickItemList,UDialog)
UON_BN_CLICKED(0,&UPickItemList::PickAllItem)
UON_BN_CLICKED(1,&UPickItemList::OnPageUp)
UON_BN_CLICKED(2,&UPickItemList::OnPageDown)
UEND_MESSAGE_MAP()

void UPickItemList::StaticInit()
{

}
UPickItemList::UPickItemList()
{
	m_ItemOne = NULL;
	m_ItemTwo = NULL;
	m_ItemThr = NULL;
	m_ItemFou = NULL;

	m_numPage = 0;
	m_curPage = 0;

	m_Guid = 0 ;  //对象
	m_loot_type = 0 ; //类型
	m_gold = 0; //金钱

	m_bGetAll = FALSE ;
	m_BAutoPickAll = FALSE;
	m_IsUpdateChild = FALSE;
	m_bNeedRoll = FALSE ;

	m_HookPickAll = FALSE ;

}
UPickItemList::~UPickItemList()
{

}
BOOL UPickItemList::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	m_ItemOne = (UItemExplain*)GetChildByID(3);
	m_ItemTwo = (UItemExplain*)GetChildByID(4);
	m_ItemThr = (UItemExplain*)GetChildByID(5);
	m_ItemFou = (UItemExplain*)GetChildByID(6);

	if (m_ItemOne == NULL || m_ItemTwo == NULL || m_ItemThr == NULL || m_ItemFou == NULL)
	{
		return FALSE;
	}

	return TRUE;
}

void UPickItemList::OnDestroy()
{
	UDialog::OnDestroy();
}
void UPickItemList::SetCUSNormal()
{
	ClearAll();
	SetVisible(FALSE);
	ULOG("UPickItemList::OnClose");
	TipSystem->HideTip();
}
void UPickItemList::OnClose()
{
	//很容易造成卡状态。强制转换。。。。
	if(CPlayerLocal::GetCUState() == cus_Normal && IsVisible())	{
		SetCUSNormal();
	}

	CPlayerLocal::SetCUState(cus_Normal);
	ULOG( _TRAN("拾取相关(cus_Normal)") );
}
void UPickItemList::SetVisible(BOOL value)
{
	if(value)
	{
		URect ListRect(sm_System->GetCursorPos(), m_Size);
		ListRect.Offset(-47, -110);
		ListRect.SetIntersect(GetRoot()->GetControlRect());
		if(ListRect.GetWidth() < m_Size.x)
			ListRect.Offset(ListRect.GetWidth() - m_Size.x, 0);
		if(ListRect.GetHeight() < m_Size.y)
			ListRect.Offset(0, ListRect.GetHeight() - m_Size.y);

		if (ListRect.pt0.x < 0 )
		{
			ListRect.pt0.x = 0 ;
		}
		if (ListRect.pt0.y < 0)
		{
			ListRect.pt0.y = 0;
		}
		SetPosition(ListRect.pt0);
	}
	UDialog::SetVisible(value);
}
		
void UPickItemList::OnTimer(float fDeltaTime)
{
	if(!m_Visible)
		return;
	UDialog::OnTimer(fDeltaTime);

	//如果距离超过10M,自动关闭!
	if (m_Guid)
	{
		CGameObject* pGameOBJ = (CGameObject*)ObjectMgr->GetObject(m_Guid);
		CPlayerLocal* pLocalPlayer = ObjectMgr->GetLocalPlayer();
		if (pLocalPlayer == NULL)
		{
			return ;
		}

		if (pGameOBJ == NULL)
		{
			OnClose();
			return ;
		}else
		{
			NiPoint3 pGameOBJPos = pGameOBJ->GetPosition();
			NiPoint3 pLocalPlayerPos = pLocalPlayer->GetLocalPlayerPos();

			if ((pLocalPlayerPos - pGameOBJPos).Length() > MaxLenToNPC)
			{
				OnClose();
			}
		}
	}
}
void UPickItemList::ReMoveMoney()
{
	UINT count = m_vLootItems.size();
	if (count > 0)
	{
		if (m_vLootItems[0].entry == 1)
		{
			m_vLootItems.erase(m_vLootItems.begin());
		}

		if (m_vLootItems.size() !=0)
		{
			//RefurbishUI();
			if (m_bGetAll)
			{
				if (m_bNeedRoll)
				{
					RefurbishUI();
				}
				PickAllItem();

			}else
			{
				RefurbishUI();
			}
		}else
		{
			if (m_Guid)
			{
				PacketBuilder->SendReleaseOneItem(m_Guid);
			}
			OnClose();
		}
	}
}
void UPickItemList::ReMoveItemByPos(UINT pos)
{
	BOOL bHasItem = FALSE;

	if (pos >=0)
	{
		int pClientPos = 0 ;

		for (; pClientPos < (int)m_vLootItems.size(); pClientPos++)
		{
			if (m_vLootItems[pClientPos].x == pos)
			{
				bHasItem = TRUE;
				break;
			}
		}
		if (bHasItem)
		{
			m_vLootItems.erase(m_vLootItems.begin() + pClientPos);	
		}else
		{
			ULOG("NOT FIND ITEM");
		}
		
		if (m_vLootItems.size() !=0)
		{
			
			if (m_bGetAll)
			{
				if (m_bNeedRoll)
				{
					RefurbishUI();
				}
				PickAllItem();
			}else
			{
				RefurbishUI();
			}
			
		}else
		{
			if (m_Guid)
			{
				PacketBuilder->SendReleaseOneItem(m_Guid);
			}
			OnClose();
		}
		

	}
}
void UPickItemList::OnItemList(vector<MSG_S2C::stLoot_Response::stLootItem> vLootItems,ui64 guid, ui32 gold,ui32 loot_type)
{
	ClearAll();
	
	unsigned int nCount = vLootItems.size();

	for (unsigned int i = 0 ;i <nCount; i++)
	{
		//PickItemData pItemData;
		//pItemData.pItem = vLootItems[i];
		//pItemData.ServerPos = i;

		MSG_S2C::stLoot_Response::stLootItem pItem ;
		pItem.x = vLootItems[i].x;
		pItem.entry = vLootItems[i].entry;
		pItem.count = vLootItems[i].count;
		pItem.display_id =  vLootItems[i].display_id;
		pItem.factor = vLootItems[i].factor;
		pItem.random_property = vLootItems[i].random_property;
		pItem.slot_type = vLootItems[i].slot_type;
		m_vLootItems.push_back(pItem);
	}

//	m_ListCount = m_vLootItems.size();

	m_Guid = guid;
	m_gold = gold;
	m_loot_type = loot_type;
	if (!m_HookPickAll)
	{
		this->SetVisible(TRUE);
	}
	
	//CPlayerLocal::SetCUState(cus_Pick);
	RefurbishUI();

	
	if (m_BAutoPickAll || m_HookPickAll)
	{
		PickAllItem();
	}
	//如果自动拾取。则设置拾取全部 不可点击状态
	

	if (m_HookPickAll)
	{
		GetChildByID(0)->SetActive(FALSE);
	}else
	{
		GetChildByID(0)->SetActive(!m_BAutoPickAll);
	}
	

	
}

void UPickItemList::RefurbishUI()
{
	UINT nCount = m_vLootItems.size();

	m_numPage =  nCount / PageItemCount;

	if (nCount % PageItemCount > 0)
	{
		m_numPage ++ ;
	}
	if (m_curPage < 0 || m_curPage > m_numPage -1)
	{
		m_curPage = 0;
	}

	OnSelPageItem();	
	
}
void UPickItemList::BagFull() //背包已满
{
	//if (m_bGetAll)
	{
		if (m_vLootItems.size())
		{
			RefurbishUI();
		}else
		{
			if (m_Guid)
			{
				PacketBuilder->SendReleaseOneItem(m_Guid);
			}
			OnClose();
		}
	}
}
void UPickItemList::OnSelItemToItemExplain(int pos, MSG_S2C::stLoot_Response::stLootItem* pickItem)
{
	
	if (pos >=0 && pos<PageItemCount)
	{
		switch (pos)
		{
		case 0:
			if (pickItem)
			{
				m_ItemOne->OnSetItem(pickItem);
			}else
			{
				m_ItemOne->OnSetItem(NULL);
			}
			break;
		case 1:
			if (pickItem)
			{
				m_ItemTwo->OnSetItem(pickItem);
			}else
			{
				m_ItemTwo->OnSetItem(NULL);
			}
			break;
		case 2:
			if (pickItem)
			{
				m_ItemThr->OnSetItem(pickItem);
			}else
			{
				m_ItemThr->OnSetItem(NULL);
			}
			break;
		case 3:
			if (pickItem)
			{
				m_ItemFou->OnSetItem(pickItem);
			}else
			{
				m_ItemFou->OnSetItem(NULL);
			}
			break;
		}
	}
}
void UPickItemList::PickAllItem()
{
	m_bGetAll = TRUE ;
	
	if (m_bGetAll)
	{
		GetChildByID(0)->SetActive(FALSE);
	}
	UINT count = m_vLootItems.size();
	if (count)
	{
		if (m_vLootItems[count - 1].entry == 1)
		{
			PacketBuilder->SendLootMoney();
			ULOG("Send Loot Money");
		}else
		{
			PacketBuilder->SendLootOneItem(m_vLootItems[count - 1].x);
		}
	}
}
void UPickItemList::OnPageUp()
{
	if (m_curPage > 0 )
	{
		m_curPage -- ;
		OnSelPageItem();
	}
}
void UPickItemList::OnPageDown()
{
	if (m_curPage < m_numPage -1)
	{
		m_curPage ++ ;
		OnSelPageItem();
	}
}

void UPickItemList::OnSelPageItem()
{
	UBitmapButton* pkUP = (UBitmapButton*)GetChildByID(1);
	UBitmapButton* pkDown = (UBitmapButton*)GetChildByID(2);
	if (m_curPage == 0)
	{
		if (pkUP)
		{
			//pkUP->SetBitmap("Public\\UpToBeginButton.skin");
			pkUP->SetActive(FALSE);
		}
	}else
	{
		if (pkUP)
		{
			//pkUP->SetBitmap("Public\\UpButton.skin");
			pkUP->SetActive(TRUE);
		}
	}

	if (m_curPage == m_numPage - 1)
	{
		if (pkDown)
		{
			//pkDown->SetBitmap("Public\\DownToEndButton.skin");
			pkDown->SetActive(FALSE);
		}
	}else
	{
		if (pkDown)
		{
			//pkDown->SetBitmap("Public\\DownButton.skin");
			pkDown->SetActive(TRUE);
		}
	}
	for (int i = 0; i < PageItemCount; i++)
	{
		if (m_curPage * PageItemCount + i < m_vLootItems.size())
		{
			OnSelItemToItemExplain(i,&m_vLootItems[m_curPage * PageItemCount + i]);
		}else
		{
			OnSelItemToItemExplain(i,NULL);
		}
	}
}

void UPickItemList::ClearAll()  //清楚所有的信息
{
	m_vLootItems.clear();
	m_ItemOne->OnClear();
	m_ItemTwo->OnClear();
	m_ItemThr->OnClear();
	m_ItemFou->OnClear();

	m_curPage = 0;
	m_numPage = 0;
	m_gold = 0; 
	m_Guid = 0;
	
	m_bGetAll = FALSE;
	m_bNeedRoll = FALSE;
	if (GetChildByID(0))
	{
		GetChildByID(0)->SetActive(TRUE);
	}
}
BOOL UPickItemList::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------------------------------
UIMP_CLASS(UItemExplain::PickItemSlot,UDynamicIconButtonEx);
void UItemExplain::PickItemSlot::StaticInit()
{

}
UItemExplain::PickItemSlot::PickItemSlot()
{
	m_IsTickable = FALSE;
	m_BKGIndex = 0;
	m_ActionContener.SetActionItem(new UItemExplain::PickedItem);
}
UItemExplain::PickItemSlot::~PickItemSlot()
{

}
void UItemExplain::PickItemSlot::SetBkg()
{
	ItemPrototype_Client* pItemPrototype = ItemMgr->GetItemPropertyFromDataBase(GetItemData()->entry);
	int bkgindex = 0;
	if (pItemPrototype)
	{
		switch (pItemPrototype->Quality)
		{
		case ITEM_QUALITY_POOR_GREY       :
			bkgindex = 0;
			break;
		case ITEM_QUALITY_NORMAL_WHITE     :   
			bkgindex = 0;
			break;
		case ITEM_QUALITY_UNCOMMON_GREEN   :  
			bkgindex = 1;
			break;
		case ITEM_QUALITY_RARE_BLUE        : 
			bkgindex = 2;
			break;
		case ITEM_QUALITY_EPIC_PURPLE      :   
			bkgindex = 3;
			break;
		case ITEM_QUALITY_LEGENDARY_ORANGE :  
			bkgindex = 4;
			break;
		}
	}
	m_BKGIndex = bkgindex;
}
void UItemExplain::PickItemSlot::DrawPushButton(const UPoint& offset, const URect& ClipRect)
{
	URect rect(offset,m_Size);

	URect IconRect( UPoint(m_IconOffset,m_IconOffset) , m_OldSize - UPoint(m_IconOffset*2,m_IconOffset*2));
	IconRect.TransformRect(m_OldSize,m_Size);
	IconRect.Offset(offset);

	if (m_IconSkin)
	{
		sm_UiRender->DrawSkin(m_IconSkin,m_IconIndex,IconRect);
	}	
	if (m_BackGSkin)
	{
		sm_UiRender->DrawSkin(m_BackGSkin,m_BKGIndex,offset - UPoint(2, 2));
	}
	if (m_ActionContener.IsCreate())
	{
		m_ActionContener.DrawActionContener(sm_UiRender,offset,ClipRect);
	}
}
void UItemExplain::PickItemSlot::OnMouseDragged(const UPoint& position, UINT flags)
{

}
//----------------------------------------------------------------------------------------------
UIMP_CLASS(UItemExplain,UBitmap);
UBEGIN_MESSAGE_MAP(UItemExplain,UBitmap)
UON_BN_CLICKED(1,&UItemExplain::PickItem)
UEND_MESSAGE_MAP()

void UItemExplain::PickedItem::Use()
{
	int pos = m_dataInfo.pos;
	ui32 id = m_dataInfo.entry;
	if (pos >= 0 && id >0 && !m_bSendPick)
	{
		m_bSendPick = TRUE ;
		if (id == 1 || id == 2 || id == 3)
		{
			PacketBuilder->SendLootMoney();
		}else
		{
			PacketBuilder->SendLootOneItem(pos);
		}
	}
}
void UItemExplain::StaticInit()
{

}
UItemExplain::UItemExplain()
{
	m_Iocn = NULL;
	m_Explain = NULL;
	m_Item = NULL;
	m_Money = NULL;
	m_ItemBkg = NULL;
}
UItemExplain::~UItemExplain()
{

}
BOOL UItemExplain::OnCreate()
{
	if (!UBitmap::OnCreate())
	{
		return FALSE;
	}

	m_Iocn = UDynamicCast(PickItemSlot,GetChildByID(1));
	m_Explain = UDynamicCast(UStaticText,GetChildByID(0));
	m_Money = UDynamicCast(UMoneyText, GetChildByID(2));
	if (m_Explain == NULL || m_Iocn == NULL || m_Money == NULL)
	{
		return FALSE;
	}
	m_Money->SetMoneyTextAlign(UT_LEFT);
	m_Explain->SetTextAlign(UT_LEFT);
	ActionDataInfo pData;
	pData.type = UItemSystem::ITEM_DATA_FLAG;	

	m_Iocn->SetItemData(pData);

	if (m_ItemBkg == NULL)
	{
		m_ItemBkg = sm_UiRender->LoadTexture("DATA\\UI\\PickItemList\\PickItemHL.png");
		if (!m_ItemBkg)
		{
			return FALSE;
		}
	}

	return TRUE;
}
void UItemExplain::OnDestroy()
{
	UBitmap::OnDestroy();
}

void UItemExplain::OnMouseEnter(const UPoint& position, UINT flags)
{
	UControl::OnMouseEnter(position, flags);
}

void UItemExplain::OnMouseLeave(const UPoint& position, UINT flags)
{
	UControl::OnMouseLeave(position, flags);
}

void UItemExplain::OnRender(const UPoint& offset, const URect &updateRect)
{
	if (m_MouseHover)
	{
		sm_UiRender->DrawImage(m_ItemBkg, offset + UPoint(38, 1));
	}
	UControl::OnRender(offset, updateRect);
}

void UItemExplain::OnSetItem(MSG_S2C::stLoot_Response::stLootItem* pItem)
{
	ActionDataInfo pData ;
	if (pItem == NULL)
	{
		pData.type = UItemSystem::ITEM_DATA_FLAG;
		m_Item = NULL;
		m_Iocn->SetItemData(pData);
		m_Explain->Clear();
		m_Explain->SetVisible(FALSE);
		m_Money->SetMoney(0);
		m_Money->SetVisible(FALSE);
		SetVisible(FALSE);
		return ;
	}else
	{
		pData.entry = pItem->entry;
		pData.pos = pItem->x;
		pData.num = pItem->count;
		pData.type = UItemSystem::ITEM_DATA_FLAG;
		SetVisible(TRUE);
		m_Item = pItem;
	}

	m_Iocn->SetItemData(pData);

	if (pData.entry == 1)
	{
		m_Explain->SetVisible(FALSE);
		m_Money->SetVisible(TRUE);
		m_Iocn->SetBkg();

		m_Money->SetMoney(pData.num);
	}else
	{
		m_Explain->SetVisible(TRUE);
		m_Money->SetVisible(FALSE);
		ItemPrototype_Client* pItemPrototype = ItemMgr->GetItemPropertyFromDataBase(pItem->entry);
		if (pItemPrototype)
		{
			string itemName = pItemPrototype->C_name;
			char buf[_MAX_PATH];
			m_Iocn->SetBkg();
			NiSprintf(buf,_MAX_PATH ,"%s(%d)", itemName.c_str(), pItem->count);

			m_Explain->SetText(buf);
			UColor _color(0,0,0,0) ;//= TipSystem->GetColor(pItemPrototype->Quality);

			GetQualityColor(pItemPrototype->Quality, _color);

			m_Explain->SetTextColor(_color);
		}	
	}
	
}

void UItemExplain::OnSetMoney(UINT money,UINT pos)
{

}
void UItemExplain::OnClear()
{
	m_Item = NULL;
	OnSetItem(NULL);
}
void UItemExplain::PickItem()
{
	//int pos = m_Iocn->GetStateData()->pos;
	//ui32 id = m_Iocn->GetStateData()->id;
	//if (pos >= 0 && id >0 && m_bCanPick)
	//{
	//	if (id == 1)
	//	{
	//		PacketBuilder->SendLootMoney();
	//	}else
	//	{
	//		PacketBuilder->SendLootOneItem(pos);
	//	}
	//	m_bCanPick = FALSE;
	//}
}