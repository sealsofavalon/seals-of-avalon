#pragma once
#include "UInc.h"
#include "USkillButton.h"
#include "../../../../SDBase/Protocol/S2C_Loot.h"
#include "UDynamicIconButtonEx.h"
#include "UIMoneyText.h"

//拾取列表
//每页显示4个道具
#define  PageItemCount 4  

class UItemExplain : public UBitmap
{
	class PickItemSlot : public UDynamicIconButtonEx
	{
		int m_BKGIndex;
		UDEC_CLASS(UItemExplain::PickItemSlot);
		PickItemSlot();
		~PickItemSlot();
		virtual void DrawPushButton(const UPoint& offset, const URect& ClipRect);
		virtual void OnMouseDragged(const UPoint& position, UINT flags);
	public:
		void SetBkg();
	};
	friend class PickItemSlot;
	class PickedItem : public UActionItem
	{
	public:
		virtual void Use();
		virtual void SetDataInfo(ActionDataInfo & di)
		{
			m_dataInfo.Guid = di.Guid;
			m_dataInfo.pos = di.pos;
			m_dataInfo.type = di.type;
			m_dataInfo.entry = di.entry;
			m_dataInfo.num = di.num;
			
			if (m_dataInfo.entry  == 1)
			{
				 if (m_dataInfo.num >= 10000)
				 {
						m_dataInfo.entry  = 3;
				 }else
				 {
					 if (m_dataInfo.num >= 100)
					 {
						 m_dataInfo.entry = 2;
					 }
				 }

			}
			m_bSendPick = FALSE ;
		}
	protected:
		BOOL m_bSendPick; //是否已经发送了拾取消息。
	};
	UDEC_CLASS(UItemExplain);
	UDEC_MESSAGEMAP();
public:
	UItemExplain();
	~UItemExplain();
	void OnSetItem(MSG_S2C::stLoot_Response::stLootItem* pItem);
	void OnSetMoney(UINT money,UINT pos);
	void OnClear();   //清楚显示的信息
	void PickItem();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
private:
	PickItemSlot* m_Iocn;   //图标
	UStaticText* m_Explain;  //说明
	UMoneyText* m_Money ;
	UTexturePtr m_ItemBkg;
	MSG_S2C::stLoot_Response::stLootItem* m_Item; //物品
	
};

class UPickItemList : public UDialog
{
	UDEC_CLASS(UPickItemList);
	UDEC_MESSAGEMAP();
public:
	UPickItemList();
	~UPickItemList();

	void OnItemList(vector<MSG_S2C::stLoot_Response::stLootItem> vLootItems,ui64 guid, ui32 gold,ui32 loot_type);
	void ReMoveItemByPos(UINT pos);
	void ReMoveMoney();
	void ClearAll();  //清楚所有的信息

	void PickAllItem();
	void OnPageUp();
	void OnPageDown();
	void SetCUSNormal();
	ui64 GetPickGuid(){return m_Guid;}
	void SetNeedRoll(){ m_bNeedRoll = TRUE ;}
	void SetAutoPick(BOOL bAuto){m_BAutoPickAll = bAuto;}
	void SetHookAutoPick(BOOL bAuto){m_HookPickAll = bAuto;}
	BOOL IsHookAutoPick(){return m_HookPickAll;}

	virtual void OnClose();
	virtual void SetVisible(BOOL value);
	void BagFull(); //背包已满
protected:
	void OnSelItemToItemExplain(int pos, MSG_S2C::stLoot_Response::stLootItem* pickItem);
	void OnSelPageItem();
	void RefurbishUI();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	
	virtual void OnTimer(float fDeltaTime);
	virtual BOOL OnEscape();
private:
	UItemExplain* m_ItemOne;
	UItemExplain* m_ItemTwo;
	UItemExplain* m_ItemThr;
	UItemExplain* m_ItemFou;

	vector<MSG_S2C::stLoot_Response::stLootItem> m_vLootItems;

	UINT m_curPage;
	UINT m_numPage;

	ui64 m_Guid ;  //对象
	ui32 m_loot_type; //类型
	ui32 m_gold; //金钱

	BOOL m_bGetAll; // 是否是失去全部。
	BOOL m_bNeedRoll;
	BOOL m_BAutoPickAll;
	BOOL m_HookPickAll;
};

