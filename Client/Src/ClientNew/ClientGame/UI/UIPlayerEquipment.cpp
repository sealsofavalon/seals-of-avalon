#include "StdAfx.h"
#include "UIPlayerEquipment.h"
#include "USkillButton.h"
#include "UIGamePlay.h"
#include "UIItemSystem.h"
#include "UIPackage.h"
#include "UISystem.h"
#include "UMessageBox.h"
#include "../Player.h"
#include "../ObjectManager.h"
#include "UEditExt.h"
#include "UInGameBar.h"
#include "ItemManager.h"
#include "ItemSlotContainer.h"
#include "../Network/PacketBuilder.h"
#include "ItemSlot.h"
#include "ClientApp.h"
#include "ClientState.h"
#include "UChat.h"
#include "UINPCTrade.h"
#include "EquipSlot.h"
#include "UEquRefineManager.h"
#include "TitleMgr.h"
#include "UITipSystem.h"
#include "UFun.h"
#include "UIRightMouseList.h"
#include "PlayerInputMgr.h"

#define  EQUIPMENT_MAX 14 //EQUIPMENT_SLOT_END  //装备格子
#define  FORSHOW3DAVTAER  80  //预留40像素
UIMP_CLASS(UEquipment,UDialog);
UBEGIN_MESSAGE_MAP(UEquipment,UDialog)
UON_BN_PRESS(20, &UEquipment::ClickLeft)
UON_BN_PRESS(21, &UEquipment::ClickRight)
UON_BN_CLICKED(32, &UEquipment::OnClickPet)
UON_BN_CLICKED(31, &UEquipment::OnClickPlayer)
UON_BN_CLICKED(40, &UEquipment::OnClickShiZhuang)
UON_BN_CLICKED(33, &UEquipment::OnClickGodTool)
UON_BN_CLICKED(35, &UEquipment::OnClickShiZhuangState)
UON_BN_CLICKED(38, &UEquipment::OnClickToukuiState)
UON_BN_CLICKED(37, &UEquipment::OnClickTitle)
UEND_MESSAGE_MAP()

//----------------------------------------------------------------------------------------------
UIMP_CLASS(UEquipment::EquipSlot,UDynamicIconButtonEx);
void UEquipment::EquipSlot::StaticInit()
{

}
UEquipment::EquipSlot::EquipSlot()
{
	m_ActionContener.SetActionItem(new UEquipment::EquipSlotItem);
	m_TypeID = UItemSystem::ICT_EQUIP;
	m_IsTickable = FALSE;
}
UEquipment::EquipSlot::~EquipSlot()
{

}		
BOOL UEquipment::EquipSlot::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (UDynamicIconButtonEx::OnMouseUpDragDrop(pDragFrom,point,pDragData,nDataFlag))
	{
		switch (nDataFlag)
		{
		case ICT_BAG:
			{
				UPackage * pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
				if (pPackage)
				{
					const ui8 * Frompos = (const ui8*)pDragData;
					const ui32 ItemId = pPackage->GetItemIdByPos(*Frompos);
					ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(ItemId);
					if (ItemInfo && ItemInfo->Class == ITEM_CLASS_ARMOR || ItemInfo->Class == ITEM_CLASS_WEAPON)
					{
						SYState()->UI->GetUItemSystem()->MoveItemMsg(nDataFlag/*ICT_BAG*/,m_TypeID/*ICT_EQUIP*/,*Frompos,GetItemData()->pos,1);
					}
				}
			}
			return TRUE;
		}
	}
	else
	{
		return FALSE;
	}
	return FALSE;
}
void UEquipment::EquipSlot::OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag)
{
	switch (nDataFlag)
	{
	case UItemSystem::ICT_BAG:
		{
			UPackage * pPackage = INGAMEGETFRAME(UPackage, FRAME_IG_PACKAGE);
			if (pPackage)
			{
				const ui8 * Frompos = (const ui8*)pDragData;
				const ui32 ItemId = pPackage->GetItemIdByPos(*Frompos);
				ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(ItemId);
				if (ItemInfo && ItemInfo->Class == ITEM_CLASS_ARMOR || ItemInfo->Class == ITEM_CLASS_WEAPON)
				{
					m_bAcceptDrag = TRUE;
				}
			}
		}
		break;
	}
}
#include "UFittingRoom.h"
void UEquipment::EquipSlot::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	if (m_ActionContener.IsCreate())
	{
		CaptureControl();
		if (!m_ActionContener.IsItemEmpty())
		{
			m_BeginDrag = TRUE;
		}
		m_ActionContener.Press(MOUSEDOWN_LEFT);
	}
};

void UEquipment::EquipSlot::OnRenderToolTip()
{
	UPoint t_pt = GetParent()->WindowToScreen(GetWindowPos());
	if (m_ActionContener.IsCreate())
	{
		t_pt += m_Size;
		TipSystem->SetShowPlayerGuid(ObjectMgr->GetLocalPlayer()->GetGUID());
		m_ActionContener.DrawToolTip(t_pt,m_TypeID);
	}
}

//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
void UEquipment::EquipSlotItem::UseLeft()
{
	int fromPos = m_dataInfo.pos;
	ULOG("客户端%d\n", fromPos);

	UNPCTrade* pkNPCTrade = INGAMEGETFRAME(UNPCTrade,FRAME_IG_NPCTRADEDLG);

	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return ;
	}

	CEquipmentContainer* pkEquipCON = (CEquipmentContainer*)pkLocal->GetContainer(ICT_EQUIP);
	CEquipSlot* pkSlot = (CEquipSlot*)pkEquipCON->GetSlot(fromPos);
	ui64 ItemGuid = pkSlot->GetGUID(); 
	if (pkNPCTrade && pkNPCTrade->IsRepair() && CPlayerLocal::GetCUState() == cus_NPCTrade)
	{
		if (ItemGuid)
		{
			SYState()->UI->GetUItemSystem()->RepairOne(pkNPCTrade->GetNpcId(), ItemGuid);  //发送修理消息	
		}
		return ;
	}
}
void UEquipment::EquipSlotItem::Use()
{
	//右键响应
	int fromPos = m_dataInfo.pos;
	ULOG("客户端%d\n", fromPos);

	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	
	UPackage* Pag = NULL;
	if (pInGame)
	{
		Pag = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
	}

	if (Pag->GetNoneItemPos() == -1)
	{
		UMessageBox::MsgBox_s( _TRAN("包裹已满!") );
	}else
	{
		//SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_EQUIP,UItemSystem::ICT_BAG,fromPos,Pag->GetNoneItemPos(),1);
		SYState()->UI->GetUItemSystem()->MoveItemMsg(UItemSystem::ICT_EQUIP,UItemSystem::ICT_BAG,fromPos,-1,1);
	}
}
void UEquipment::StaticInit()
{

}

UEquipment::UEquipment()
{
	m_bLimitDrag = TRUE;
	m_IsCheckShizhuang = FALSE ;
	m_RESISTANCES_Ctrl[Role_RESISTANCES_01] = NULL;
	m_RESISTANCES_Ctrl[Role_RESISTANCES_02] = NULL;
	m_RESISTANCES_Ctrl[Role_RESISTANCES_03] = NULL;
	m_RESISTANCES_Ctrl[Role_RESISTANCES_04] = NULL;
	m_RESISTANCES_Ctrl[Role_RESISTANCES_05] = NULL;
	m_RESISTANCES_Ctrl[Role_RESISTANCES_06] = NULL;
}
UEquipment::~UEquipment()
{

}
void UEquipment::UpdatePlayerRESISTANCES(ui32 data, ui32 flag)
{
	int iData = int(data / 1000.f);
	switch(flag)
	{
	case UNIT_FIELD_RESISTANCES_01  ://神圣
		m_RESISTANCES_Ctrl[Role_RESISTANCES_01]->SetText(_I2A(iData).c_str());
		break;
	case UNIT_FIELD_RESISTANCES_02  ://火焰
		m_RESISTANCES_Ctrl[Role_RESISTANCES_04]->SetText(_I2A(iData).c_str());
		break;
	case UNIT_FIELD_RESISTANCES_03  ://自然
		m_RESISTANCES_Ctrl[Role_RESISTANCES_05]->SetText(_I2A(iData).c_str());
		break;
	case UNIT_FIELD_RESISTANCES_04  ://冰霜
		m_RESISTANCES_Ctrl[Role_RESISTANCES_03]->SetText(_I2A(iData).c_str());
		break;
	case UNIT_FIELD_RESISTANCES_05  ://暗影
		m_RESISTANCES_Ctrl[Role_RESISTANCES_02]->SetText(_I2A(iData).c_str());
		break;
	case UNIT_FIELD_RESISTANCES_06  ://神秘
		m_RESISTANCES_Ctrl[Role_RESISTANCES_06]->SetText(_I2A(iData).c_str());
		break;
	}
}
void UEquipment::UpdatePlayerPropety(ui32 data , int field)
{
	UPropertyInfo* pkPlayInfo = UDynamicCast(UPropertyInfo, GetChildByID(41));
	if (pkPlayInfo)
	{
		pkPlayInfo->SetData(data, field);
	}
	pkPlayInfo = UDynamicCast(UPropertyInfo, GetChildByID(42));
	if (pkPlayInfo)
	{
		pkPlayInfo->SetData(data, field);
	}
	pkPlayInfo = UDynamicCast(UPropertyInfo, GetChildByID(43));
	if (pkPlayInfo)
	{
		pkPlayInfo->SetData(data, field);
	}
}

BOOL UEquipment::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;
	//初始化装备格子
	for (int i = EQUIPMENT_SLOT_START ; i < EQUIPMENT_SLOT_END; i++)
	{
		if (GetChildByID(i) == NULL)
		{
			return FALSE;
		}

		ActionDataInfo dataInfo ;
		dataInfo.entry = 0;
		dataInfo.pos = i ;  // pos form 0 to 13
		dataInfo.type = UItemSystem::ITEM_DATA_FLAG;  //物品类型

		EquipSlot* eq = (EquipSlot*)GetChildByID(i);
		if (eq == NULL)
		{
			return FALSE;
		}
		eq->SetItemData(dataInfo);
	}

	UButton* btn = UDynamicCast(UButton, GetChildByID(31));
	if (btn)
	{
		btn->SetCheck(FALSE);
	}
	
	m_RESISTANCES_Ctrl[Role_RESISTANCES_01] = UDynamicCast(UStaticText, GetChildByID(47));
	m_RESISTANCES_Ctrl[Role_RESISTANCES_02] = UDynamicCast(UStaticText, GetChildByID(48));
	m_RESISTANCES_Ctrl[Role_RESISTANCES_03] = UDynamicCast(UStaticText, GetChildByID(49));
	m_RESISTANCES_Ctrl[Role_RESISTANCES_04] = UDynamicCast(UStaticText, GetChildByID(50));
	m_RESISTANCES_Ctrl[Role_RESISTANCES_05] = UDynamicCast(UStaticText, GetChildByID(51));
	m_RESISTANCES_Ctrl[Role_RESISTANCES_06] = UDynamicCast(UStaticText, GetChildByID(52));

	if (!m_RESISTANCES_Ctrl[Role_RESISTANCES_01] || !m_RESISTANCES_Ctrl[Role_RESISTANCES_02] || !m_RESISTANCES_Ctrl[Role_RESISTANCES_03] ||
		!m_RESISTANCES_Ctrl[Role_RESISTANCES_04] || !m_RESISTANCES_Ctrl[Role_RESISTANCES_05] || !m_RESISTANCES_Ctrl[Role_RESISTANCES_06])
	{
		return FALSE ;
	}
	m_RESISTANCES_Ctrl[Role_RESISTANCES_01]->SetTextEdge(TRUE);
	m_RESISTANCES_Ctrl[Role_RESISTANCES_02]->SetTextEdge(TRUE);
	m_RESISTANCES_Ctrl[Role_RESISTANCES_03]->SetTextEdge(TRUE);
	m_RESISTANCES_Ctrl[Role_RESISTANCES_04]->SetTextEdge(TRUE);
	m_RESISTANCES_Ctrl[Role_RESISTANCES_05]->SetTextEdge(TRUE);
	m_RESISTANCES_Ctrl[Role_RESISTANCES_06]->SetTextEdge(TRUE);

	UPropertyInfo* pkPlayInfo = UDynamicCast(UPropertyInfo, GetChildByID(41));
	if (pkPlayInfo)
	{
		pkPlayInfo->SetShowType(UPropertyInfo::PROPERTY_TYPE_BASE);
	}
	pkPlayInfo = UDynamicCast(UPropertyInfo, GetChildByID(42));
	if (pkPlayInfo)
	{
		pkPlayInfo->SetShowType(UPropertyInfo::PROPERTY_TYPE_ATTACK);
	}
	pkPlayInfo = UDynamicCast(UPropertyInfo, GetChildByID(43));
	if (pkPlayInfo)
	{
		pkPlayInfo->SetShowType(UPropertyInfo::PROPERTY_TYPE_SOCIETY);
	}

	return TRUE;
}

void UEquipment::OnDestroy()
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(35));
	if (pBtn)
	{
		pBtn->SetCheckState(FALSE);
	}
	UDialog::OnDestroy();
}

void UEquipment::OnClose()
{
	SetVisible(FALSE);
	UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	if (pGameBar)
	{
		UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONEQUIP);
		if (pBtn)
		{
			pBtn->SetCheckState(FALSE);
		}
	}
}
void UEquipment::SetIconByPos(ui8 pos, ui32 ditemId,UItemSystem::ItemType type)
{
	if (pos < EQUIPMENT_SLOT_START && pos > EQUIPMENT_MAX)
	{
		return;
	}

	CPlayer * pLocalplayer = ObjectMgr->GetLocalPlayer();

	ui64 guid = 0;
	if (pLocalplayer)
	{
		CItemSlotContainer * pItemSlotContainer = NULL;
		pItemSlotContainer = (CItemSlotContainer *)pLocalplayer->GetContainer(ICT_EQUIP);		
		if (pItemSlotContainer)
		{
			guid = ((CItemSlot*)pItemSlotContainer->GetSlot(pos))->GetGUID();
		}
	}
	EquipSlot * pEquipSlot = NULL;
	switch (pos)
	{
	case EQUIPMENT_SLOT_HEAD:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_GLOVES:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_CHEST:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_TROUSERS:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_BOOTS:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_MAINHAND:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_OFFHAND:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_NECK:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_SHOULDERS:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_WAIST:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_FINGER1:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_FINGER2:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_TRINKET1:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_TRINKET2:
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_BEIBU:				/*14背部*/
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_TOUBU:				/*15头部*/
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_JIANBANG:			/*16肩膀*/
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_SHANGYI:		/*17上衣*/
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_XIAYI:				/*18下衣*/
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_XIEZI: /*19鞋子*/
		pEquipSlot = UDynamicCast(EquipSlot,GetChildByID(pos));
		break;
	}

	ActionDataInfo dataInfo;

	dataInfo.entry = ditemId;
	dataInfo.pos = pos ;
	dataInfo.type = type;
	dataInfo.Guid = guid;

	
	if (pEquipSlot)
	{
		pEquipSlot->SetItemData(dataInfo);
		TipSystem->ReCreateItemTip();
	}
}

void UEquipment::SetLevel(ui32 level)
{
	UStaticText* pPlayerLev = (UStaticText*)GetChildByID(25);
	//char lev[1024];
	//std::string lev;
	if(pPlayerLev)
	{
		//NiSprintf(lev,1024,"等级 %d级",level);
		//lev = _TRAN( N_NOTICE_C21,  );
		pPlayerLev->SetText(_I2A( level ).c_str());
	}
}
void UEquipment::SetHeadImage(ui8 race,ui8 gender,ui8 Class)
{
	UBitmap* pkHead = UDynamicCast(UBitmap,GetChildByID(23));
	UBitmap* pkBgk = UDynamicCast(UBitmap, GetChildByID(53));
	UPropertyInfo* pkPlayInfo = UDynamicCast(UPropertyInfo, GetChildByID(42));
	char* strfile = NULL ;
	char* bgkfile = NULL;
	if (pkHead)
	{
		if (race == RACE_YAO)
		{
			strfile = "Data\\UI\\Public\\Yao.png";
			bgkfile = "Data\\UI\\Equipment\\Yao.png";
		}else if (race == RACE_REN)
		{
			strfile = "Data\\UI\\Public\\Ren.png";
			bgkfile = "Data\\UI\\Equipment\\ren.png";
		}else if (race == RACE_WU)
		{
			strfile = "Data\\UI\\Public\\Wu.png";
			bgkfile = "Data\\UI\\Equipment\\wu.png";
		}
		pkHead->SetBitMapByStr(strfile);
	}
	char buf[1024];
	if (m_IsCheckShizhuang)
	{
		bgkfile = "Data\\UI\\Equipment\\sz.png";
	}
	if (pkBgk)
	{
		pkBgk->SetBitMapByStr(bgkfile);
	}
	UStaticText* pPlayerClass = (UStaticText*)GetChildByID(26);
	if (pPlayerClass)
	{
		if (Class == CLASS_WARRIOR)
		{
			NiSprintf(buf,1024, _TRAN("武修") );
			if (pkPlayInfo)
			{
				pkPlayInfo->SetShowType(UPropertyInfo::PROPERTY_TYPE_ATTACK);
			}
		}else if (Class == CLASS_BOW)
		{
			NiSprintf(buf,1024, _TRAN("羽箭") );
			if (pkPlayInfo)
			{
				pkPlayInfo->SetShowType(UPropertyInfo::PROPERTY_TYPE_RANGE);
			}
		}else if (Class == CLASS_PRIEST)
		{
			NiSprintf(buf,1024, _TRAN("仙道") );
			if (pkPlayInfo)
			{
				pkPlayInfo->SetShowType(UPropertyInfo::PROPERTY_TYPE_SPELL);
			}
			
		}else if (Class == CLASS_MAGE)
		{
			NiSprintf(buf,1024, _TRAN("真巫") );
			if (pkPlayInfo)
			{
				pkPlayInfo->SetShowType(UPropertyInfo::PROPERTY_TYPE_SPELL);
			}
		}
		pPlayerClass->SetText(buf);
	}
}
void UEquipment::SetName(const char * name)
{
	UStaticText* pPlayerName  = (UStaticText*)GetChildByID(24);
	if (pPlayerName)
	{
		pPlayerName->SetText(name);
	}
}
//void UEquipment::SetPlayerPropety(ui32 data , int field)
//{
//	if (field < FIELD_ROLE_STRENGTH || field > FIELD_MAX)
//	{
//		return ;
//	}
//	//m_PlayerProperty->SetData(data,field);
//}
BOOL UEquipment::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}

void UEquipment::SetAddLocalData(CPlayer* pkChar)
{
	UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
	if (pNiavobject)
	{
		pNiavobject->AddCharactor(pkChar);
	}
}
void UEquipment::UpDateUNiavobject(uint32 visiblebase, ui32 itemid, ui32 SlotIndex, ui32 Effect_id)
{
	UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
	if (pNiavobject)
	{
		pNiavobject->UpdatePlayer(visiblebase, itemid,SlotIndex,Effect_id);
	}
}



void UEquipment::OnRender(const UPoint& offset, const URect &updateRect)
{
	UDialog::OnRender(offset,updateRect);

}
void UEquipment::OnMouseDragged(const UPoint& position, UINT flags)
{

	//由于上面为了显示3D模型不封顶，顶部预留了40像素。这里需要进行处理！
	//否则3D模型的RENDER 会有问题!

	//

	if (!m_bCanDrag || !IsCaptured())
	{
		return;
	}

	//这里计算顶部偏移
	INT TopAdd = 0;
	UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
	if (pNiavobject)
	{
		TopAdd = pNiavobject->GetTopAdd();
	}

	UPoint delta = position - m_ptLeftBtnDown;
	m_ptLeftBtnDown = position;
	UPoint NewPos = GetWindowPos();
	if (m_bLimitDrag)
	{
		UControl* pDesktop = GetRoot();
		assert(pDesktop && pDesktop->IsDesktop());
		UPoint DesktopSize = pDesktop->GetWindowSize();
		NewPos += delta;

		NewPos.x = LClamp(NewPos.x , 0, DesktopSize.x - m_Size.x); 
		NewPos.y = LClamp(NewPos.y , TopAdd, DesktopSize.y - m_Size.y); 
	}else
	{
		NewPos += delta;
	}
	SetPosition(NewPos);
}
void UEquipment::SetVisible(BOOL value)
{
	UDialog::SetVisible(value);
	//UPlayeInfo* pkPlayInfo = INGAMEGETFRAME(UPlayeInfo,FRAME_IG_PROPERTYDLG);
	//if (pkPlayInfo)
	//{
	//	pkPlayInfo->SetVisible(value);
	//}

	UPetDlg* pPetDlg = INGAMEGETFRAME(UPetDlg, FRAME_IG_PLAYERPETDLG);
	if (pPetDlg)
	{
		pPetDlg->SetVisible(FALSE);
	}
	if (value)
	{
		UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
		if (pNiavobject)
		{
			pNiavobject->SetDirectionTo();
		}
	}
	UViewPlayerEquipment* pViewPlayerEqu = INGAMEGETFRAME(UViewPlayerEquipment, FRAME_IG_VIEWPLAYEREQUIPDLG);
	if (pViewPlayerEqu)
		pViewPlayerEqu->SetVisible(pViewPlayerEqu->IsVisible());
}
void UEquipment::SetPosition(const UPoint &NewPos)
{
	UPoint OldPos = m_Position ;
	UPoint Offset = NewPos - OldPos ;
	UControl::SetPosition(NewPos);
	//UPlayeInfo* pkPlayInfo = INGAMEGETFRAME(UPlayeInfo,FRAME_IG_PROPERTYDLG);
	//if (pkPlayInfo)
	//{
	//	UPoint playInfoOldPos = pkPlayInfo->GetWindowPos();
	//	pkPlayInfo->SetPosition(playInfoOldPos + Offset);
	//}
}

void UEquipment::ClickRight()                //3D 模型右转
{
	UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
	if (pNiavobject)
	{
		pNiavobject->SetRotatePlayer(-0.1f);
	}
}
void UEquipment::ClickLeft()                 //3D 模型左转
{
	UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
	if (pNiavobject)
	{
		pNiavobject->SetRotatePlayer(0.1f);
	}
}
void UEquipment::OnClickPet()
{
	SetVisible(FALSE);
	UPetDlg* pPetDlg = INGAMEGETFRAME(UPetDlg, FRAME_IG_PLAYERPETDLG);
	if (pPetDlg)
	{
		pPetDlg->SetVisible(TRUE);
	}
	UButton* btn = UDynamicCast(UButton, GetChildByID(31));
	if (btn)
	{
		btn->SetCheck(FALSE);
	}
}
void UEquipment::OnClickShiZhuang()
{
	m_IsCheckShizhuang = TRUE;
	UControl* pkCtrl = NULL;
	for (int i = 0; i < 20; i++)
	{
		pkCtrl = GetChildByID(i);
		if (pkCtrl)
		{
			if (i > 13)
			{
				pkCtrl->SetVisible(TRUE);
			}else
			{
				pkCtrl->SetVisible(FALSE);
			}
		}
	}

	UBitmap* pkBgk = UDynamicCast(UBitmap, GetChildByID(53));
	if (pkBgk)
	{
		pkBgk->SetBitMapByStr("Data\\UI\\Equipment\\sz.png");
	}
}
void UEquipment::OnClickPlayer()
{
	m_IsCheckShizhuang = FALSE ;
	UControl* pkCtrl = NULL ;
	for (int i = 0; i < 20; i++)
	{
		pkCtrl = GetChildByID(i);
		if (pkCtrl)
		{
			if (i <= 13)
			{
				pkCtrl->SetVisible(TRUE);
			}else
			{
				pkCtrl->SetVisible(FALSE);
			}
		}
	}

	UBitmap* pkBgk = UDynamicCast(UBitmap, GetChildByID(53));

	char* bgkfile = NULL;
	CPlayer* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal && pkBgk)
	{
		ui8 race = pkLocal->GetRace();
		if (race == RACE_YAO)
		{
			bgkfile = "Data\\UI\\Equipment\\Yao.png";
		}else if (race == RACE_REN)
		{
			bgkfile = "Data\\UI\\Equipment\\ren.png";
		}else if (race == RACE_WU)
		{
			bgkfile = "Data\\UI\\Equipment\\wu.png";
		}
		if (pkBgk)
		{
			pkBgk->SetBitMapByStr(bgkfile);
		}
	}
	
	
}
void UEquipment::OnClickGodTool()
{
	if (EquRefineMgr)
	{
		EquRefineMgr->ShowOrHidenRefineUI();
	}
}
void UEquipment::OnClickTitle()
{
	if (TitleMgr)
	{
		TitleMgr->SetUTitleVisible();
	}
}
void UEquipment::SetIsShowShiZhuang(bool value)
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(35));
	if (pBtn)
	{
		pBtn->SetCheckState(value);
	}
}
void UEquipment::SetIsShowHelm(bool value)
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(38));
	if (pBtn)
	{
		pBtn->SetCheckState(value);
	}
}
void UEquipment::SetShowEQHead(BOOL bshow, ui64 guid)
{
	UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
	if (pNiavobject)
	{
		pNiavobject->SetShowHead(bshow, guid);
	}
}
void UEquipment::OnClickShiZhuangState()
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(35));
	UButton* pBtn1 = UDynamicCast(UButton, GetChildByID(38));
	uint8 value = 0;
	if (pBtn1 && pBtn)
	{
		if (pBtn->IsChecked())
			value |= 1;

		if (pBtn1->IsChecked())
			value |= 2;
	}
	PacketBuilder->SendShowShiZhuang(value);
}

void UEquipment::OnClickToukuiState()
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(35));
	UButton* pBtn1 = UDynamicCast(UButton, GetChildByID(38));
	uint8 value = 0;
	if (pBtn1 && pBtn)
	{
		if (pBtn->IsChecked())
			value |= 1;

		if (pBtn1->IsChecked())
			value |= 2;
	}
	PacketBuilder->SendShowShiZhuang(value);
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UNiAVControlEq,UNiAVControl);
void UNiAVControlEq::StaticInit()
{

}
UNiAVControlEq::UNiAVControlEq()
{
	m_Char = NULL;
	m_spCamera = NULL;
	m_IsPlayer = FALSE;
	m_spCameTr = NiPoint3(0.0f, 3.0f, 1.0f);
	m_Guid = 0;
	m_TopADD = FORSHOW3DAVTAER ;
	m_isOnLine = FALSE;
    m_BNeed3DHead = TRUE;

	m_isNeedSetInfo = FALSE;

	m_bShowHeadEq = TRUE ;
}
UNiAVControlEq::~UNiAVControlEq()
{
    RemoveOBJ();
	m_Guid = 0;
	m_isOnLine = FALSE;
	m_isNeedSetInfo = FALSE;
}
void UNiAVControlEq::SetRotatePlayer(float fRot)
{
	if (m_spAVObject)
	{
		NiNode* pkNode = NiDynamicCast(NiNode, m_spAVObject);
		if(pkNode)
		{
			NiMatrix3 kMat;
			kMat.MakeZRotation(fRot);
			pkNode->SetRotate(kMat * pkNode->GetRotate());
		}
	}
}
BOOL UNiAVControlEq::OnCreate()
{
	NIASSERT(SYState()->ClientApp);
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	NiRenderer* pRender = SYState()->Render;
	if(pRender == NULL)
		return FALSE;

	if (pRender->GetSorter() == NULL)
	{
		NiAlphaAccumulator* pkAccum = NiNew NiAlphaAccumulator;
		pRender->SetSorter(pkAccum);
	}


	m_VisGeometries.RemoveAll();
	
	m_spDirLight = NiNew NiDirectionalLight;
	m_spDirLight->SetDiffuseColor(NiColor::WHITE * 0.5f);
	m_spDirLight->SetAmbientColor(NiColor::WHITE * 0.5f);

	NiMatrix3 kMat;
	kMat.MakeIdentity();
	NiPoint3 kDir(0.0f, -1.0f, 0.f);
	kDir.Unitize();
	kMat.SetCol(0, kDir);
	m_spDirLight->SetRotate(kMat);
	m_spDirLight->Update(0.f);

	return TRUE;
}
void UNiAVControlEq::UpdatePlayer(uint32 visiblebase, ui32 itemid, ui32 SlotIndex, ui32 effectid)
{
	if (!m_IsPlayer || !m_Char)
	{
		return ;
	}

	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(itemid);
	bool bCanAttach = (ItemInfo && ItemInfo->SubClass != ITEM_SUBCLASS_WEAPON_BAOZHU);
	//if ((PLAYER_VISIBLE_ITEM_7_0 == visiblebase || PLAYER_VISIBLE_ITEM_6_0 == visiblebase) && bCanAttach)
	//{
	//	if (PLAYER_VISIBLE_ITEM_6_0 == visiblebase)
	//		ItemMgr->OnEuipmentChange(m_Char, itemid, SlotIndex, AS_BacksideRight);
	//	else if (PLAYER_VISIBLE_ITEM_7_0 == visiblebase)
	//		ItemMgr->OnEuipmentChange(m_Char, itemid, SlotIndex, AS_BacksideLeft);
	//}
	//else
	//{
	//	ItemMgr->OnEuipmentChange(m_Char, itemid, SlotIndex);
	//}

	//更新下武器和装备精练的特效
	ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (SlotIndex * 12) + 9 ;
	m_Char->SetUInt32Value(itemeffect,effectid);
	
	//

	if ((PLAYER_VISIBLE_ITEM_7_0 == visiblebase || PLAYER_VISIBLE_ITEM_6_0 == visiblebase))
	{
		//ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(itemid);

		if (PLAYER_VISIBLE_ITEM_7_0 == visiblebase )
		{
			m_Char->SetUInt32Value(PLAYER_VISIBLE_ITEM_7_0 , itemid);
		}else
		{
			m_Char->SetUInt32Value(PLAYER_VISIBLE_ITEM_6_0 , itemid);
		}

		//bool bCanAttach = (ItemInfo && ItemInfo->SubClass != ITEM_SUBCLASS_WEAPON_BAOZHU);
		if (bCanAttach)
		{
			if (PLAYER_VISIBLE_ITEM_6_0 == visiblebase)
				ItemMgr->OnEquipmentChange((CPlayer*)m_Char, itemid, SlotIndex, AS_BacksideRight);
			else if (PLAYER_VISIBLE_ITEM_7_0 == visiblebase)
				ItemMgr->OnEquipmentChange((CPlayer*)m_Char, itemid, SlotIndex, AS_BacksideLeft);
			
		}
		else
		{
			ItemMgr->OnEquipmentChange((CPlayer*)m_Char, itemid, SlotIndex);
			if (itemid == 0)
			{
				if (PLAYER_VISIBLE_ITEM_6_0 == visiblebase)
					ItemMgr->OnEquipmentChange((CPlayer*)m_Char, itemid, SlotIndex, AS_BacksideRight);
				else if (PLAYER_VISIBLE_ITEM_7_0 == visiblebase)
					ItemMgr->OnEquipmentChange((CPlayer*)m_Char, itemid, SlotIndex, AS_BacksideLeft);
			}
		}

		
	}
	else
	{
		ItemMgr->OnEquipmentChange((CPlayer*)m_Char, itemid, SlotIndex);
	}
}
void UNiAVControlEq::RemoveOBJ()
{
	m_Guid = 0;
    
    if(m_Char)
    {
        if(m_Char->GetAsPlayer())
            ObjectMgr->RemoveClientPlayer(m_Char->GetGUID());
        else
            NiDelete m_Char;
        m_Char = NULL;
    }

	m_isNeedSetInfo = FALSE;
	m_VisGeometries.RemoveAll();
	m_isOnLine = FALSE;
	m_spAVObject = NULL;
	m_IsPlayer = FALSE;
}
float UNiAVControlEq::GetCharScale()
{
	float scale = 0.8f;
	if (m_Char && m_IsPlayer)
	{
		UINT Race = m_Char->GetRace();
		UINT Sex = m_Char->GetGender();

		switch(Race)
		{
		case RACE_REN:
			{
				if (Sex == GENDER_FEMALE)
				{
					return 1.04f * scale;
				}else
				{
					return 1.0f* scale;
				}
			}
			break;
		case RACE_WU:
			{
				if (Sex == GENDER_FEMALE)
				{
					return 0.99f* scale;
				}else
				{
					return 0.86f* scale;
				}
			}
			break;
		case RACE_YAO:
			{
				if (Sex == GENDER_FEMALE)
				{
					return 1.13f* scale;
				}else
				{
					return 1.04f* scale;
				}
			}
			break;
		}
	}

	return 1.0f* scale;
}
void UNiAVControlEq::SetCharInfo()
{
	m_spAVObject = NULL;

	NiNode* pLocalNode = m_Char->GetSceneNode();
	NIASSERT(pLocalNode);

	m_spAVObject = pLocalNode;

	RecursiveSetCycleType(m_spAVObject, NiTimeController::LOOP);

	pLocalNode->SetScale(GetCharScale() * 0.9f);
	
	pLocalNode->SetTranslate(NiPoint3(0.0f, 0.0f, 0.0f));
	m_spDirLight->AttachAffectedNode(pLocalNode);


	//SetCamera();

	m_spAVObject->UpdateEffects();
	m_spAVObject->UpdateProperties();
	m_spAVObject->Update(0.0f, false);


	m_VisGeometries.RemoveAll();
	CollectRendElement(m_spAVObject);
}

void UNiAVControlEq::SetZ(float fZ)
{
	if ( m_spAVObject )
		m_spAVObject->SetTranslate(NiPoint3(0.0f, fZ, 0.0f));
}

void UNiAVControlEq::SetCamera()
{
	if (m_spCamera == NULL)
	{
		m_spCamera = NiNew NiCamera;
	}
	
	NiPoint3 kTranslation(0.0f, 3.8f, 1.0f);

	m_spCameTr = kTranslation;
	NiMatrix3 kRotX;
	NiMatrix3 kRotZ;
	kRotX.MakeXRotation(-NI_HALF_PI);
	kRotZ.MakeZRotation(NI_HALF_PI);

	m_spCamera->SetRotate(kRotZ * kRotX);
	m_spCamera->SetTranslate(kTranslation);
	m_spCamera->Update(0.0f);

	//面向
	SetDirectionTo();
}
void UNiAVControlEq::AddCCreature(CCreature* pkChar)
{
    if(m_Char)
    {
        if(m_Char->GetAsPlayer())
            ObjectMgr->RemoveClientPlayer(m_Char->GetGUID());
        else
            NiDelete m_Char;
    }

	m_isNeedSetInfo = FALSE ;
	CCreature* pkCreature = NiNew CCreature;
	m_Char = pkCreature;
 

	//((CCreature*)m_Char)->SetRace(pkChar->GetRace());
	((CCreature*)m_Char)->SetGender(pkChar->GetGender());
	ui32 displayid = pkChar->GetUInt32Value(UNIT_FIELD_DISPLAYID);
	std::string strFileName = "";
	ItemMgr->GetDisplayInfo(displayid, strFileName, pkChar->GetRace(), pkChar->GetGender(), true);

	ui32 entryid = pkChar->GetUInt32Value(OBJECT_FIELD_ENTRY);
	std::string strCreatureName = "";
	if(!ItemMgr->GetCreatureName(entryid, strCreatureName))
	{
		strCreatureName = _TRAN("未知");
	}

	if(!((CCreature*)m_Char)->Create(strFileName.c_str(), strCreatureName.c_str()))
	{
		NiDelete m_Char;
		m_Char = NULL;
		return ;
	}

	m_Guid = pkChar->GetGUID();
	m_IsPlayer = FALSE ;
}

void UNiAVControlEq::AddCharactor(CCharacter* pkChar)
{
	if (m_Char == pkChar)
	{
		return ;
	}

	if (pkChar->GetGUID() == m_Guid)
	{
		return ;
	}
	if (pkChar->GetGameObjectType() == GOT_PLAYER )
	{
		m_isOnLine = TRUE;
		AddPlayer((CPlayer*)pkChar);
		return ;
	}
	
	if (pkChar->GetGameObjectType() == GOT_CREATURE)
	{
		m_isOnLine = TRUE;
		AddCCreature((CCreature*)pkChar);
		return ;
	}
}
void UNiAVControlEq::GetPlayerEquipment(CPlayer* pkChar)
{
	if (m_IsPlayer && m_Guid == pkChar->GetGUID())
	{

		//BOOL bUpdateEQUI = FALSE;
		BOOL bArm = pkChar->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);

		for( ui16 index = EQUIPMENT_SLOT_START; index < EQUIPMENT_SLOT_END; index++ )
		{
			ui32 GuidBase = PLAYER_FIELD_INV_SLOT_HEAD + (index * 2);
			ui32 visiblebase = PLAYER_VISIBLE_ITEM_1_0 + (index * 12);
			
				
			uint32 entry = pkChar->GetUInt32Value(visiblebase);
			if ((PLAYER_VISIBLE_ITEM_7_0 == visiblebase || PLAYER_VISIBLE_ITEM_6_0 == visiblebase))
			{
				continue ;
			}
			else
			{
				BOOL isShowShizhuang = (pkChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_SHOW_SHIZHUANG) & 1);
				bool bChange = true;
				if(index == INVTYPE_HEAD)
				{
					uint32 Testentry = pkChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_TOUBU * 12));
					if(Testentry)
						bChange = false;
				}
				else if(index == INVTYPE_SHOULDERS)
				{
					uint32 Testentry = pkChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_JIANBANG * 12));
					if(Testentry)
						bChange = false;
				}
				else if(index == INVTYPE_CHEST)
				{
					uint32 Testentry = pkChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_SHANGYI * 12));
					if(Testentry)
						bChange = false;
				}
				else if(index == INVTYPE_TROUSERS)
				{
					uint32 Testentry = pkChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_XIAYI * 12));
					if(Testentry)
						bChange = false;
				}
				else if(index == INVTYPE_BOOTS)
				{
					uint32 Testentry = pkChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_XIEZI * 12));
					if(Testentry)
						bChange = false;
				}
				if(bChange) 
				{
					ItemMgr->OnEquipmentChange((CPlayer*)m_Char, entry, index);

				}


				// 如果是怪物装，还原
				if(entry == 0 || !isShowShizhuang)
				{
					bool bHunterEquip = false;
					ui32 SlotIndex = 0;
					if(index == INVTYPE_HOUNTER_TOUBU)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_HEAD;
					}
					else if(index == INVTYPE_HOUNTER_JIANBANG)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_SHOULDERS;
					}
					else if(index == INVTYPE_HOUNTER_SHANGYI)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_CHEST;
					}
					else if(index == INVTYPE_HOUNTER_XIAYI)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_TROUSERS;
					}
					else if(index == INVTYPE_HOUNTER_XIEZI)
					{
						bHunterEquip = true;
						SlotIndex = INVTYPE_BOOTS;
					}
					if(bHunterEquip)
					{
						ui32 visiblebases = PLAYER_VISIBLE_ITEM_1_0 + (SlotIndex * 12);
						uint32 entrys = pkChar->GetUInt32Value(visiblebases);
						ItemMgr->OnEquipmentChange((CPlayer*)m_Char, entrys, SlotIndex);

					}
				}
			}
			
		}
	}
}
void UNiAVControlEq::AddPlayer(CPlayer* pkChar)
{
    if(m_Char)
    {
        if(m_Char->GetAsPlayer())
            ObjectMgr->RemoveClientPlayer(m_Char->GetGUID());
        else
            NiDelete m_Char;


		m_Char = NULL;
    }

	m_isNeedSetInfo = FALSE;
	m_IsPlayer = TRUE ;
	CPlayer* pkPlayer = ObjectMgr->CreateClientPlayer();
	m_Char = pkPlayer;
	//m_Char->SetGUID(pkChar->GetGUID());

    m_Guid = pkChar->GetGUID();
    ((CPlayer*)m_Char)->SetRace(pkChar->GetRace());
    ((CPlayer*)m_Char)->SetGender(pkChar->GetGender());

	//装载模型
	bool LoadOK = ((CPlayer*)m_Char)->CreatePlayer(pkChar->GetUInt32Value(UNIT_FIELD_DISPLAYID)) > 0;
	
	FillItemEffect(pkChar);
	GetPlayerEquipment(pkChar);
	
}

BOOL UNiAVControlEq::SetShowHead(BOOL bshow, ui64 guid)
{
	if (guid && m_IsPlayer && m_Char && m_Guid == guid)
	{
		CPlayer* pkplayer = ObjectMgr->GetPlayer(guid);
		if (pkplayer)
		{
			ui32 visiblebase = PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HEAD * 12);
			ui32 visiblebase2 = PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_TOUBU * 12);
			ui32 value = pkplayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_SHOW_SHIZHUANG);
			if (bshow)
			{
				if ((value & 1)&& pkplayer->GetUInt32Value(visiblebase2))
				{
					UpdatePlayer(visiblebase2, pkplayer->GetUInt32Value(visiblebase2),EQUIPMENT_HOUNTER_TOUBU);
				}else
				{
					ui32 effect_displayid = 0;
					int GuidBase = PLAYER_FIELD_INV_SLOT_HEAD + (EQUIPMENT_SLOT_HEAD * 2);
					ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (EQUIPMENT_SLOT_HEAD * 12) + 9 ;
					effect_displayid = pkplayer->GetUInt32Value(itemeffect);

					GetItemEffect(pkplayer->GetUInt32Value(visiblebase), pkplayer->GetUInt64Value(GuidBase), effect_displayid);
					UpdatePlayer(visiblebase, pkplayer->GetUInt32Value(visiblebase),EQUIPMENT_SLOT_HEAD);
				}
			}else
			{
				UpdatePlayer(visiblebase,0,EQUIPMENT_SLOT_HEAD,0);
				UpdatePlayer(visiblebase2,0, EQUIPMENT_HOUNTER_TOUBU, 0);
			}

			return  TRUE ;
		}

	}

	return FALSE ;
}
void UNiAVControlEq::FillItemEffect(CPlayer* pPlayer)
{
	for( ui16 index = EQUIPMENT_SLOT_START; index < EQUIPMENT_SLOT_END; index++ )
	{
		ui32 itemeffect = PLAYER_VISIBLE_ITEM_1_0 + (index * 12) + 9 ;
		m_Char->SetUInt32Value(itemeffect,pPlayer->GetUInt32Value(itemeffect));
	}
}

void UNiAVControlEq::SetDirectionTo()
{
	NiPoint3 kDir = m_spCameTr;
	kDir.Unitize();

	float fZ = NiATan2(-kDir.y, kDir.x) - NI_PI/2;
	if( fZ < 0.f )
		fZ += NI_TWO_PI;

	if( fZ > NI_TWO_PI )
		fZ -= NI_TWO_PI;	

	NiMatrix3 pkRotate;
	pkRotate.MakeZRotation(fZ);
	if (m_spAVObject)
	{
		m_spAVObject->SetRotate(pkRotate);
	}
	
}
UControl* UNiAVControlEq::FindHitWindow(const UPoint &pt, INT initialLayer)
{
	UControl* pkParent = GetParent();

	if (UDynamicCast(UEquipment,pkParent))
	{
		return pkParent ;
	}
	return UControl::FindHitWindow(pt,initialLayer);
}
void UNiAVControlEq::OnRender(const UPoint& offset,const URect &updateRect)
{
	if(m_spCamera == NULL ||m_spAVObject == NULL || m_VisGeometries.GetCount() == 0)
	{
		return;
	}

	NiCamera* pCam = m_spCamera;
	NIASSERT(pCam);

	NiAppWindow* pWindow = NiApplication::ms_pkApplication->GetAppWindow();
	float fWindowWidth =  (float)pWindow->GetWidth();
	float fWindowHeight = (float)pWindow->GetHeight();
	NiRect<float> ViewPort;
	ViewPort.m_left = updateRect.left/fWindowWidth;
	ViewPort.m_top =  1.0f - (updateRect.top - m_TopADD)/fWindowHeight;
	ViewPort.m_right = updateRect.right/fWindowWidth ;
	ViewPort.m_bottom = 1.0f - updateRect.bottom/fWindowHeight;



	float fFov =  40;
	float fFar = ClientState->GetCamFarDist();
	float fNear = ClientState->GetCamNearDist();

	float fAspectRatio = 1.0f;
	
	fAspectRatio = abs((float)(updateRect.right - updateRect.left) / (float)(updateRect.bottom -  updateRect.top + m_TopADD));


	// Setup the camera frustum and viewport
	float fVerticalFieldOfViewRad = NI_PI / 180.0f * fFov;
	float fViewPlaneHalfHeight = tanf(fVerticalFieldOfViewRad * 0.5f);
	float fViewPlaneHalfWidth = fViewPlaneHalfHeight * fAspectRatio;

	NiFrustum kFrustum = NiFrustum(
		-fViewPlaneHalfWidth, fViewPlaneHalfWidth, 
		fViewPlaneHalfHeight, -fViewPlaneHalfHeight,
		fNear, fFar, false);

	pCam->SetViewFrustum(kFrustum);

	pCam->SetViewPort(ViewPort);
	pCam->FitNearAndFarToBound(m_spAVObject->GetWorldBound());
	m_kVisible.RemoveAll();
	NiCullingProcess kCuller(&m_kVisible);

	sm_UiRender->EndRender();


	
	NiD3DRenderer* pRenderer = (NiD3DRenderer*)NiRenderer::GetRenderer();
	NIASSERT(pRenderer);

	
	NiDX9RenderState* pkRenderState = pRenderer->GetRenderState();
	pkRenderState->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	pRenderer->SetCameraData(pCam);
	PushNiState();
	NiDrawScene(pCam, m_spAVObject, kCuller);
	PopNiState();

	ViewPort.m_left = 0.0f;
	ViewPort.m_right = 1.0f;
	ViewPort.m_bottom = 0.0f;
	ViewPort.m_top = 1.0f;

	pRenderer->SetScreenSpaceCameraData(&ViewPort);
	sm_UiRender->BeginRender();
}
void UNiAVControlEq::OnDestroy()
{
	RemoveOBJ();
	UNiAVControl::OnDestroy();
}
void UNiAVControlEq::OnTimer(float fDeltaTime)
{
    if(m_Char && m_Char->IsActorLoaded())
    {
		if (!m_isNeedSetInfo)
		{
			SetCharInfo();
			SetCamera();
			SetHeadB3D(m_BNeed3DHead);
			SetShowHead(m_bShowHeadEq,m_Guid);
			m_isNeedSetInfo = TRUE;
		}

		if (m_Char->GetActorManager())
		{
			AnimationID Anim = m_Char->GetActorManager()->GetCurAnimation();
			NiControllerSequence* pkCurSeq = m_Char->GetActorManager()->GetSequence(Anim);
			
			if (!pkCurSeq)
			{
				return ;
			}
			BOOL bNeedResetAnim = FALSE ;
			if (m_BNeed3DHead)
			{
				if (pkCurSeq->GetCycleType() != NiTimeController::LOOP)
				{
					bNeedResetAnim = TRUE;
				}
			}else
			{
				if (pkCurSeq->GetCycleType() != NiTimeController::CLAMP)
				{
					bNeedResetAnim = TRUE;
				}
			}

			if (bNeedResetAnim)
			{
				SetHeadB3D(m_BNeed3DHead);
			}
		}
    }



	UNiAVControl::OnTimer(fDeltaTime);
}

void UNiAVControlEq::SetHeadB3D(BOOL b3D)
{
	m_BNeed3DHead = b3D;
	if (m_Guid && m_IsPlayer && m_Char)
	{
		NiControllerSequence* Seq = m_Char->SetCurAnimation("stand");			
		if(Seq)
		{
			if (m_BNeed3DHead)
			{
				Seq->SetCycleType(NiTimeController::LOOP);
			}else
			{
				Seq->SetCycleType(NiTimeController::CLAMP);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
uint64 g_viewplayerguid = 0;
void UViewPlayerEquipment::ViewEquipSlotItem::Use()
{

}

UIMP_CLASS(UViewPlayerEquipment::ViewEquipSlot, UDynamicIconButtonEx);
void UViewPlayerEquipment::ViewEquipSlot::StaticInit(){}
UViewPlayerEquipment::ViewEquipSlot::ViewEquipSlot()
{
	m_ActionContener.SetActionItem(new UViewPlayerEquipment::ViewEquipSlotItem);
	m_TypeID = UItemSystem::ICT_INVLALID;
	m_IsTickable = FALSE;
}
UViewPlayerEquipment::ViewEquipSlot::~ViewEquipSlot(){}

void UViewPlayerEquipment::ViewEquipSlot::OnRenderToolTip()
{
	UPoint t_pt = GetParent()->WindowToScreen(GetWindowPos());
	if (m_ActionContener.IsCreate())
	{
		t_pt += m_Size;
		TipSystem->SetShowPlayerGuid(g_viewplayerguid);
		m_ActionContener.DrawToolTip(t_pt,m_TypeID);
	}
}


UIMP_CLASS(UViewPlayerEquipment, UDialog);
UBEGIN_MESSAGE_MAP(UViewPlayerEquipment, UDialog)
UON_BN_PRESS(20, &UViewPlayerEquipment::OnClickLeft)
UON_BN_PRESS(21, &UViewPlayerEquipment::OnClickRight)
UEND_MESSAGE_MAP()
void UViewPlayerEquipment::StaticInit()
{

}
UViewPlayerEquipment::UViewPlayerEquipment()
{

}
UViewPlayerEquipment::~UViewPlayerEquipment()
{

}
BOOL UViewPlayerEquipment::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_OrgenPos = m_Position;
	for (int i = EQUIPMENT_SLOT_START ; i < EQUIPMENT_SLOT_END; i++)
	{
		if (GetChildByID(i) == NULL)
		{
			return FALSE;
		}

		ActionDataInfo dataInfo ;
		dataInfo.entry = 0;
		dataInfo.pos = i ;  // pos form 0 to 13
		dataInfo.type = UItemSystem::ITEM_DATA_FLAG;  //物品类型

		ViewEquipSlot* eq = UDynamicCast(ViewEquipSlot, GetChildByID(i));
		if (eq == NULL)
			return FALSE;
		eq->SetItemData(dataInfo);
	}
	m_bCanDrag = FALSE;
	return TRUE;
}
void UViewPlayerEquipment::OnDestroy()
{
	UDialog::OnDestroy();
}
void UViewPlayerEquipment::OnClose()
{
	SetVisible(FALSE);
}
BOOL UViewPlayerEquipment::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}

void UViewPlayerEquipment::OnClickRight()
{
	UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
	if (pNiavobject)
	{
		pNiavobject->SetRotatePlayer(-0.1f);
	}
}
void UViewPlayerEquipment::OnClickLeft()
{
	UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
	if (pNiavobject)
	{
		pNiavobject->SetRotatePlayer(0.1f);
	}
}
void UViewPlayerEquipment::SetViewPlayer(CCharacter* pChar)
{
	if (pChar != ObjectMgr->GetLocalPlayer())
	{
		m_ViewChar = pChar;
	}
	else
	{
		g_viewplayerguid = 0;
		return;
	}
	if (pChar == NULL)
	{
		SetVisible(FALSE);
		g_viewplayerguid = 0;
		return;
	}
	g_viewplayerguid = pChar->GetGUID();
	UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
	if (!pNiavobject)
		return;
	pNiavobject->AddCharactor(pChar);
	if (pChar->GetGameObjectType() == GOT_PLAYER)
	{
		for( ui16 index = EQUIPMENT_SLOT_START; index < EQUIPMENT_SLOT_END; index++ )
		{
			int GuidBase = PLAYER_FIELD_INV_SLOT_HEAD + (index * 2);
			int visiblebase = PLAYER_VISIBLE_ITEM_1_0 + (index * 12);

			ui64 Guid = pChar->GetUInt64Value(GuidBase);
			uint32 entry = pChar->GetUInt32Value(visiblebase);

			//ui32 effect_displayid = 0;
			//ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (index * 12) + 9 ;
			//effect_displayid = pChar->GetUInt32Value(itemeffect);
			////pNiavobject->UpdatePlayer(visiblebase, pChar->GetUInt32Value(visiblebase), index, effect_displayid);
			//UpDateUNiavobject((CPlayer*)pChar, visiblebase,entry, index, effect_displayid);

			SetIconByPos((ui8)index, entry, Guid);
		}
		SetVisible(TRUE);
	}
	UBitmap* pBitmap = UDynamicCast(UBitmap, GetChildByID(122));
	if (pBitmap)
	{
		switch(pChar->GetRace())
		{
		case RACE_REN:
			pBitmap->SetBitMapByStr("DATA\\UI\\LOBBY\\BKGIMAGE_REN.png");
			break;
		case RACE_YAO:
			pBitmap->SetBitMapByStr("DATA\\UI\\LOBBY\\BKGIMAGE_YAO.png");
			break;
		case RACE_WU:
			pBitmap->SetBitMapByStr("DATA\\UI\\LOBBY\\BKGIMAGE_WU.png");
			break;
		}
	}
}
void UViewPlayerEquipment::UpDateUNiavobject(CPlayer* pkplayer, uint32 visiblebase, ui32 itemid, ui32 SlotIndex, ui32 Effect_id)
{
	if (pkplayer != m_ViewChar)
		return;
	UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
	if (!pNiavobject)
		return;

	bool bChange = true;
	if(SlotIndex == INVTYPE_HEAD )
	{
		uint32 Testentry = pkplayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_TOUBU * 12));
		if(Testentry)
			bChange = false;
	}
	else if(SlotIndex == INVTYPE_SHOULDERS)
	{
		uint32 Testentry = pkplayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_JIANBANG * 12));
		if(Testentry)
			bChange = false;
	}
	else if(SlotIndex == INVTYPE_CHEST)
	{
		uint32 Testentry = pkplayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_SHANGYI * 12));
		if(Testentry)
			bChange = false;
	}
	else if(SlotIndex == INVTYPE_TROUSERS)
	{
		uint32 Testentry = pkplayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_XIAYI * 12));
		if(Testentry)
			bChange = false;
	}
	else if(SlotIndex == INVTYPE_BOOTS)
	{
		uint32 Testentry = pkplayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + (INVTYPE_HOUNTER_XIEZI * 12));
		if(Testentry)
			bChange = false;
	}
	if(bChange)
	{
		ui32 effect_displayid = 0;
		ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (SlotIndex * 12) + 9 ;
		effect_displayid = pkplayer->GetUInt32Value(itemeffect);

		pNiavobject->UpdatePlayer(visiblebase, itemid, SlotIndex, effect_displayid);
	}

	//// 如果是怪物装，还原

	ui32 HYitemid = 0;
	if(pkplayer->GetUInt32Value(visiblebase) == 0)
	{
		bool bHunterEquip = false;
		ui32 Index = 0;
		if(SlotIndex == INVTYPE_HOUNTER_TOUBU)
		{
			bHunterEquip = true;
			Index = INVTYPE_HEAD;
		}
		else if(SlotIndex == INVTYPE_HOUNTER_JIANBANG)
		{
			bHunterEquip = true;
			Index = INVTYPE_SHOULDERS;
		}
		else if(SlotIndex == INVTYPE_HOUNTER_SHANGYI)
		{
			bHunterEquip = true;
			Index = INVTYPE_CHEST;
		}
		else if(SlotIndex == INVTYPE_HOUNTER_XIAYI)
		{
			bHunterEquip = true;
			Index = INVTYPE_TROUSERS;
		}
		else if(SlotIndex == INVTYPE_HOUNTER_XIEZI)
		{
			bHunterEquip = true;
			Index = INVTYPE_BOOTS;
		}
		if(bHunterEquip)
		{
			ui32 effect_displayid = 0;
			ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (Index * 12) + 9 ;
			effect_displayid = pkplayer->GetUInt32Value(itemeffect);

			ui32  NVisibleBase = PLAYER_VISIBLE_ITEM_1_0 + (Index * 12); 
			HYitemid = pkplayer->GetUInt32Value(NVisibleBase);
			

			pNiavobject->UpdatePlayer(NVisibleBase, HYitemid, Index, effect_displayid);
		}
	}
	int GuidBase = PLAYER_FIELD_INV_SLOT_HEAD + (SlotIndex * 2);

	ui64 Guid = pkplayer->GetUInt64Value(GuidBase);

	SetIconByPos(SlotIndex, itemid, Guid);
}
void UViewPlayerEquipment::SetShowHeadEQ(BOOL bshow, ui64 guid)
{
	UNiAVControlEq* pNiavobject = UDynamicCast(UNiAVControlEq,GetChildByID(22));
	if (pNiavobject)
	{
		pNiavobject->SetShowHead(bshow, guid);
	}
}
void UViewPlayerEquipment::SetIconByPos(ui8 pos, ui32 ditemId, ui64 ItemGuid)
{
	if (pos < EQUIPMENT_SLOT_START && pos > EQUIPMENT_MAX)
	{
		return;
	}
	ViewEquipSlot * pEquipSlot = NULL;
	switch (pos)
	{
	case EQUIPMENT_SLOT_HEAD:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_GLOVES:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_CHEST:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_TROUSERS:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_BOOTS:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_MAINHAND:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_OFFHAND:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_NECK:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_SHOULDERS:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_WAIST:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_FINGER1:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_FINGER2:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_TRINKET1:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_SLOT_TRINKET2:
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_BEIBU:				/*14背部*/
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_TOUBU:				/*15头部*/
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_JIANBANG:			/*16肩膀*/
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_SHANGYI:		/*17上衣*/
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_XIAYI:				/*18下衣*/
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	case EQUIPMENT_HOUNTER_XIEZI: /*19鞋子*/
		pEquipSlot = UDynamicCast(ViewEquipSlot,GetChildByID(pos));
		break;
	}

	ActionDataInfo dataInfo;

	dataInfo.entry = ditemId;
	dataInfo.pos = pos ;
	dataInfo.type = UItemSystem::ITEM_DATA_FLAG;
	dataInfo.Guid = ItemGuid;
	if(m_ViewChar)
	{
		ItemExtraData* pNewData = new ItemExtraData;
		pNewData->jinglian_level = m_ViewChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + pos * 12 + 1);
		pNewData->enchants[0] = m_ViewChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + pos * 12 + 2);
		pNewData->enchants[1] = m_ViewChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + pos * 12 + 3);
		pNewData->enchants[2] = m_ViewChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + pos * 12 + 4);
		pNewData->enchants[3] = m_ViewChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + pos * 12 + 5);
		pNewData->enchants[4] = m_ViewChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + pos * 12 + 6);
		pNewData->enchants[5] = m_ViewChar->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0 + pos * 12 + 7);
		dataInfo.Data = pNewData;
		dataInfo.DataSize = sizeof(ItemExtraData);
	}

	if (pEquipSlot)
	{
		pEquipSlot->SetItemData(dataInfo);
	}
}
void UViewPlayerEquipment::SetVisible(BOOL value)
{
	UControl* pCtrl = UInGame::GetFrame(FRAME_IG_PLAYEREQUIPDLG);
	UControl* pCtrlTitle = UInGame::GetFrame(FRAME_IG_TITLEDLG);
	if (pCtrl->IsVisible())
	{
		UPoint Newpos = m_OrgenPos;
		Newpos.x += pCtrl->GetWidth();
		SetPosition(Newpos);
	}
	else
	{
		SetPosition(m_OrgenPos);
	}
	UDialog::SetVisible(value);
}

//////////////////////////////////////////////////////////////////////////
const char* property_type_name[] = 
{
	"基础属性",
	"防御属性",
	"近战属性",
	"远程属性",
	"施法属性",
	"社会属性",
};

UIMP_CLASS(UPropertyInfo,UControl);
UBEGIN_MESSAGE_MAP(UPropertyInfo, UControl)
UON_CBOX_SELCHANGE(1, &UPropertyInfo::OnSelShowChange)
UEND_MESSAGE_MAP()
void UPropertyInfo::StaticInit()
{

}
UPropertyInfo::UPropertyInfo()
{
	m_ComboBox = NULL ;
	m_PropertyType = PROPERTY_TYPE_BASE;
	for(int i = 0 ; i < FIELD_MAX ; i++)
	{
		SetData(0, i);
	}
}
UPropertyInfo::~UPropertyInfo()
{
	for(int i = 0 ; i < FIELD_MAX ; i++)
	{
		SetData(0, i);
	}
}
void UPropertyInfo::OnDestroy()
{
	for(int i = 0 ; i < FIELD_MAX ; i++)
	{
		SetData(0, i);
	}
	UControl::OnDestroy();
}
BOOL UPropertyInfo::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	

	m_ComboBox = UDynamicCast(UComboBox, GetChildByID(1));
	if (!m_ComboBox)
	{
		return FALSE ;
	}
	m_ComboBox->Clear();
	m_ComboBox->AddItem(_TRAN(property_type_name[PROPERTY_TYPE_BASE]));
	m_ComboBox->AddItem(_TRAN(property_type_name[PROPERTY_TYPE_DEFENSE]));
	m_ComboBox->AddItem(_TRAN(property_type_name[PROPERTY_TYPE_ATTACK]));
	m_ComboBox->AddItem(_TRAN(property_type_name[PROPERTY_TYPE_RANGE]));
	m_ComboBox->AddItem(_TRAN(property_type_name[PROPERTY_TYPE_SPELL]));
	m_ComboBox->AddItem(_TRAN(property_type_name[PROPERTY_TYPE_SOCIETY]));

	m_ComboBox->SetCurSel(m_PropertyType);

	return TRUE;
}
void UPropertyInfo::OnSelShowChange()
{
	m_PropertyType = m_ComboBox->GetCurSel();
	if (m_PropertyType >= PROPERTY_TYPE_MAX)
	{
		m_PropertyType = PROPERTY_TYPE_BASE;
	}
	UPoint offset(0, 0);
	int Height = 21;
	const int border = 3;
	switch (m_PropertyType)
	{
	case PROPERTY_TYPE_BASE:
		SetRenderRect(offset, FIELD_ROLE_STRENGTH, border, Height);
		SetRenderRect(offset, FIELD_ROLE_AGILITY, border, Height);
		SetRenderRect(offset, FIELD_ROLE_STAMINA, border, Height);
		SetRenderRect(offset, FIELD_ROLE_INTELLIGENCE, border, Height);
		SetRenderRect(offset, FIELD_ROLE_SPIRITE, border, Height);
		SetRenderRect(offset, FIELD_ROLE_VITALITY, border, Height);
		break;
	case PROPERTY_TYPE_DEFENSE:
		SetRenderRect(offset, FIELD_ROLE_RESISTANCES, border, Height);
		SetRenderRect(offset, FIELD_ROLE_DODGE_PERCENTAGE, border, Height);
		SetRenderRect(offset, FIELD_ROLE_BLOCK_PERCENTAGE, border, Height);
		break;
	case PROPERTY_TYPE_ATTACK:
		SetRenderRect(offset, FIELD_ROLE_ATTACK_POWER, border, Height);
		SetRenderRect(offset, FIELD_ROLE_MINDAMAGE, border, Height);
		SetRenderRect(offset, FIELD_ROLE_CRIT_PERCENTAGE, border, Height);
		SetRenderRect(offset, FIELD_ROLE_BASEATTACKTIME, border, Height);
		break;
	case PROPERTY_TYPE_RANGE:
		SetRenderRect(offset, FIELD_ROLE_RANGED_ATTACK_POWER, border, Height);
		SetRenderRect(offset, FIELD_ROLE_MINRANGEDDAMAGE, border, Height);
		SetRenderRect(offset, FIELD_ROLE_RANGED_CRIT_PERCENTAGE, border, Height);
		SetRenderRect(offset, FIELD_ROLE_RANGEDATTACKTIME, border, Height);
		break;
	case PROPERTY_TYPE_SPELL:
		SetRenderRect(offset, FIELD_ROLE_SPELLDAMAGE, border, Height);
		SetRenderRect(offset, FIELD_ROLE_SPELL_CRIT_PERCENTAGE, border, Height);
		break;
	case PROPERTY_TYPE_SOCIETY:
		SetRenderRect(offset, FIELD_ROLE_HONORCURRENT, border, Height);
		SetRenderRect(offset, FIELD_ROLE_HONORTOTAL, border, Height);
		SetRenderRect(offset, FIELD_ROLE_KILLCOUNT, border, Height);
		SetRenderRect(offset, FIELD_ROLE_BEKILLCOUNT, border, Height);
		break;
	}
}

void UPropertyInfo::SetRenderRect(const UPoint& offset, int field, int border, int& height)
{
	if (field >= FIELD_ROLE_PLAYERPROPERTY && field < FIELD_MAX)
	{
		UPoint Pos = offset;
		Pos.x += border;
		Pos.y += height ;
		UPoint size = m_Size;
		size.x -= 2* border;
		size.y = m_Style->m_spFont->GetHeight();

		m_RoleProperty[field].RenderRect.SetPosition(Pos);
		m_RoleProperty[field].RenderRect.SetSize(size);

		height += m_Style->m_spFont->GetHeight() + 1;
	}
}


void UPropertyInfo::RenderData(const UPoint& offset, int field)
{
	if (field >= FIELD_ROLE_PLAYERPROPERTY && field < FIELD_MAX)
	{
		UDrawTextEx(sm_UiRender, m_Style->m_spFont, m_RoleProperty[field].RenderRect.GetPosition() + offset, m_RoleProperty[field].RenderRect.GetSize(),
			0xfffed200,0xffffffff, m_RoleProperty[field].strname.c_str(),m_RoleProperty[field].str.c_str());
	}

}

void UPropertyInfo::OnMouseMove(const UPoint& position, UINT flags)
{
	UPoint ShowPoint = WindowToScreen(UPoint(0,0));
	ShowPoint.x += GetWindowSize().x;
	switch (m_PropertyType)
	{
	case PROPERTY_TYPE_BASE:
		for(int i = FIELD_ROLE_STRENGTH ; i <= FIELD_ROLE_STAMINA ; ++i )
		{
			URect rect = m_RoleProperty[i].RenderRect;
			rect.Offset(WindowToScreen(UPoint(0,0)));
			if(rect.PointInRect(position))
			{
				TipSystem->ShowTip(TIP_TYPE_PLAYERPROPERTY, i, ShowPoint);
				return;
			}
		}
		break;
	case PROPERTY_TYPE_DEFENSE:
		for(int i = FIELD_ROLE_RESISTANCES ; i <= FIELD_ROLE_BLOCK_PERCENTAGE ; ++i )
		{
			URect rect = m_RoleProperty[i].RenderRect;
			rect.Offset(WindowToScreen(UPoint(0,0)));
			if(rect.PointInRect(position))
			{
				TipSystem->ShowTip(TIP_TYPE_PLAYERPROPERTY, i, ShowPoint);
				return;
			}
		}
		break;
	case PROPERTY_TYPE_ATTACK:
		for(int i = FIELD_ROLE_ATTACK_POWER ; i <= FIELD_ROLE_BASEATTACKTIME ; ++i )
		{
			URect rect = m_RoleProperty[i].RenderRect;
			rect.Offset(WindowToScreen(UPoint(0,0)));
			if(rect.PointInRect(position))
			{
				TipSystem->ShowTip(TIP_TYPE_PLAYERPROPERTY, i, ShowPoint);
				return;
			}
		}
		break;
	case PROPERTY_TYPE_RANGE:
		for(int i = FIELD_ROLE_RANGED_ATTACK_POWER ; i <= FIELD_ROLE_RANGEDATTACKTIME ; ++i )
		{
			URect rect = m_RoleProperty[i].RenderRect;
			rect.Offset(WindowToScreen(UPoint(0,0)));
			if(rect.PointInRect(position))
			{
				TipSystem->ShowTip(TIP_TYPE_PLAYERPROPERTY, i, ShowPoint);
				return;
			}
		}
		break;
	case PROPERTY_TYPE_SPELL:
		for(int i = FIELD_ROLE_SPELLDAMAGE ; i <= FIELD_ROLE_SPELL_CRIT_PERCENTAGE ; ++i )
		{
			URect rect = m_RoleProperty[i].RenderRect;
			rect.Offset(WindowToScreen(UPoint(0,0)));
			if(rect.PointInRect(position))
			{
				TipSystem->ShowTip(TIP_TYPE_PLAYERPROPERTY, i, ShowPoint);
				return;
			}
		}
		break;
	case PROPERTY_TYPE_SOCIETY:
		for(int i = FIELD_ROLE_HONORCURRENT ; i <= FIELD_ROLE_BEKILLCOUNT ; ++i )
		{
			URect rect = m_RoleProperty[i].RenderRect;
			rect.Offset(WindowToScreen(UPoint(0,0)));
			if(rect.PointInRect(position))
			{
				TipSystem->ShowTip(TIP_TYPE_PLAYERPROPERTY, i, ShowPoint);
				return;
			}
		}
		break;
	}
	TipSystem->HideTip();
}

void UPropertyInfo::OnMouseLeave(const UPoint& position, UINT flags)
{
	TipSystem->HideTip();
}

void UPropertyInfo::OnRender(const UPoint& offset, const URect &updateRect)
{
	RenderChildWindow(offset, updateRect);
	USetTextEdge(TRUE);
	switch (m_PropertyType)
	{
	case PROPERTY_TYPE_BASE:
		RenderData(offset, FIELD_ROLE_STRENGTH);
		RenderData(offset, FIELD_ROLE_AGILITY);
		RenderData(offset, FIELD_ROLE_STAMINA);
		RenderData(offset, FIELD_ROLE_INTELLIGENCE);
		RenderData(offset, FIELD_ROLE_SPIRITE);
		RenderData(offset, FIELD_ROLE_VITALITY);
		break;
	case PROPERTY_TYPE_DEFENSE:
		RenderData(offset, FIELD_ROLE_RESISTANCES);
		RenderData(offset, FIELD_ROLE_DODGE_PERCENTAGE);
		RenderData(offset, FIELD_ROLE_BLOCK_PERCENTAGE);
		break;
	case PROPERTY_TYPE_ATTACK:
		RenderData(offset, FIELD_ROLE_ATTACK_POWER);
		RenderData(offset, FIELD_ROLE_MINDAMAGE);
		RenderData(offset, FIELD_ROLE_CRIT_PERCENTAGE);
		RenderData(offset, FIELD_ROLE_BASEATTACKTIME);
		break;
	case PROPERTY_TYPE_RANGE:
		RenderData(offset, FIELD_ROLE_RANGED_ATTACK_POWER);
		RenderData(offset, FIELD_ROLE_MINRANGEDDAMAGE);
		RenderData(offset, FIELD_ROLE_RANGED_CRIT_PERCENTAGE);
		RenderData(offset, FIELD_ROLE_RANGEDATTACKTIME);
		break;
	case PROPERTY_TYPE_SPELL:
		RenderData(offset, FIELD_ROLE_SPELLDAMAGE);
		RenderData(offset, FIELD_ROLE_SPELL_CRIT_PERCENTAGE);
		break;
	case PROPERTY_TYPE_SOCIETY:
		RenderData(offset, FIELD_ROLE_HONORCURRENT);
		RenderData(offset, FIELD_ROLE_HONORTOTAL);
		RenderData(offset, FIELD_ROLE_KILLCOUNT);
		RenderData(offset, FIELD_ROLE_BEKILLCOUNT);
		break;
	}
	USetTextEdge(FALSE);
}
void UPropertyInfo::SetShowType(int stype)
{
	if (stype >= PROPERTY_TYPE_BASE && stype < PROPERTY_TYPE_MAX)
	{
		m_ComboBox->SetCurSel(stype);
	}
}
void UPropertyInfo::SetData(ui32 data , int field)
{
	NIASSERT(field >= FIELD_ROLE_PLAYERPROPERTY && field < FIELD_MAX);

	m_RoleProperty[field].RoleProperty = data;
	m_RoleProperty[field].strname = _TRAN(base_str[field]);

	char buf[256];
	_itoa(data, buf, 10);
	if (field < FIELD_ROLE_SOCIETYPROPERTY)
	{
		if (field == FIELD_ROLE_DODGE_PERCENTAGE || field == FIELD_ROLE_BLOCK_PERCENTAGE || field == FIELD_ROLE_CRIT_PERCENTAGE || field == FIELD_ROLE_RANGED_CRIT_PERCENTAGE
			|| field == FIELD_ROLE_SPELL_CRIT_PERCENTAGE || field == FIELD_ROLE_BASEATTACKTIME || field == FIELD_ROLE_RANGEDATTACKTIME)
		{
			if (field == FIELD_ROLE_BASEATTACKTIME || field == FIELD_ROLE_RANGEDATTACKTIME)
			{
				sprintf_s(buf, 256, "%.2f", float(data * 0.001f));
			}
			else
			{
				sprintf_s(buf, 256, "%.2f%%", float(data * 0.001f));
			}
		}else
		{
			sprintf_s(buf, 256, "%.1f", float(data * 0.001f));
		}
	}else
	{
		if(data >= 100000)
		{
			sprintf_s(buf, 256, "%uW", data/10000);
		}
	}
	
	m_RoleProperty[field].str = buf;
}
///////////////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UPetItem, UControl)
UBEGIN_MESSAGE_MAP(UPetItem, UControl)
UON_BN_CLICKED(1, &UPetItem::OnClickChangeName)
UON_BN_CLICKED(11, &UPetItem::OnClickDelPet)
UON_BN_CLICKED(2, &UPetItem::OnClickUnSatable)
UON_BN_CLICKED(12, &UPetItem::OnClickStable)
UON_BN_CLICKED(13, &UPetItem::OnClickPetProperty)
UEND_MESSAGE_MAP()
void UPetItem::PetActionButtonItem::Use()
{

}
UIMP_CLASS(UPetItem::PetActionButton, UDynamicIconButtonEx);
void UPetItem::PetActionButton::StaticInit()
{

}
UPetItem::PetActionButton::PetActionButton()
{
	m_ActionContener.SetActionItem(new UPetItem::PetActionButtonItem);
}
UPetItem::PetActionButton::~PetActionButton()
{

}		
void UPetItem::StaticInit()
{

}
UPetItem::UPetItem()
{
	mNameStaticText = NULL;
    mPetIcon = NULL;
}
UPetItem::~UPetItem()
{

}
BOOL UPetItem::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (mNameStaticText == NULL)
	{
		mNameStaticText = UDynamicCast(UStaticText, GetChildByID(4));
		if (!mNameStaticText)
		{
			return FALSE;
		}

        mPetIcon = UDynamicCast(UBitmap, GetChildByID(13));
        //if(!mPetIcon)
        //    return FALSE;
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(4));
	if (pText)
	{
		pText->SetTextColor("00ff00");
		pText->SetTextEdge(true);
	}
	return TRUE;
}
void UPetItem::OnDestroy()
{
	UControl::OnDestroy();
}
void UPetItem::OnClickStable()
{
	CPlayerInputMgr * InputMsg = SYState()->LocalPlayerInput;
	if (InputMsg)
	{
		if ( InputMsg->IsSelectingTarget() )
		{
			InputMsg->EndSelectTarget();
			return;
		}
		InputMsg->BeginSkillAttack(502);
	}
	//PacketBuilder->SendPetStable();
}
void UPetItem::OnClickUnSatable()
{
	PacketBuilder->SendPetUnStable(mPetData.pet_no);
}
void UPetItem::OnClickDelPet()
{

}
void UPetItem::OnClickChangeName()
{

}

void UPetItem::OnClickPetProperty()
{
	UButton* btn2 = UDynamicCast(UButton, GetChildByID(12));
	UPetDlg* petDlg = UDynamicCast(UPetDlg, GetParent());
	if (btn2 && btn2->IsVisible() && petDlg)
	{
		UIRightMouseList* pRML = INGAMEGETFRAME(UIRightMouseList, FRAME_IG_RIGHTMOUSELIST);
		if (pRML)
		{
			pRML->Popup(WindowToScreen(UPoint(0,0)) + UPoint(20, 20), BITMAPLIST_PET , "", petDlg->GetPetGUID());
		}
	}
}

void UPetItem::SetPetData(MSG_S2C::stPets_List_Stabled::stPet &pet)
{
	mPetData.entry_id = pet.entry_id;
	mPetData.level = pet.level;
	mPetData.loyaltylvl = pet.loyaltylvl;
	mPetData.name = pet.name;
	mPetData.pet_no = pet.pet_no;
	mPetData.state = pet.state;
	SetDataToUI();
}
void UPetItem::SetDataToUI()
{
	mNameStaticText->SetText(mPetData.name.c_str());

	if (mPetData.entry_id)
	{
        if(mPetIcon)
        {
            char strFile[255];
            NiSprintf(strFile, 255, "Data\\UI\\Icon\\Monster\\Icon_%d.png", mPetData.entry_id);
            mPetIcon->SetBitMapByStr(strFile);
        }

		SetVisible(TRUE);
	}
}
void UPetItem::SetCanStable(BOOL value)
{
	UButton* btn = UDynamicCast(UButton, GetChildByID(2));
	UButton* btn2 = UDynamicCast(UButton, GetChildByID(12));
	if (btn && btn2)
	{
		btn->SetVisible(!value);
		btn2->SetVisible(value);
	}
}

void UPetItem::SetUnStableActive(BOOL value)
{
	UButton* btn = UDynamicCast(UButton, GetChildByID(2));
	if (btn)
	{
		btn->SetActive(value);
	}
}

ui32 UPetItem::GetPetNumber()
{
	return mPetData.pet_no;
}
void UPetItem::SetPetExper(ui32 exp, ui32 nextexp)
{/*
	mPetExp->SetCurExp(exp);
	mPetExp->SetNextLevelExp(nextexp);*/
}
void UPetItem::Clear()
{
	SetVisible(FALSE);
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UPetDlg, UDialog)
UBEGIN_MESSAGE_MAP(UPetDlg, UDialog)
UON_BN_CLICKED(31, &UPetDlg::OnClickPlayer)
UON_BN_CLICKED(32, &UPetDlg::OnClickPet)
UON_BN_CLICKED(40, &UPetDlg::OnClickPlayerFation)
UON_BN_CLICKED(34, &UPetDlg::OnClickUp)
UON_BN_CLICKED(35, &UPetDlg::OnClickDown)
UEND_MESSAGE_MAP()
void UPetDlg::StaticInit()
{

}
UPetDlg::UPetDlg()
{
	for (int i = 0 ; i < 8; i++)
		m_PetItem[i] = NULL;
	m_StartPos = 0;
	m_PetGuid = 0;
	m_PetNumber = 0;
	m_CurPage = 0;
	m_MaxPage = 0;
}
UPetDlg::~UPetDlg()
{

}
BOOL UPetDlg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;
	UButton* btn = UDynamicCast(UButton, GetChildByID(32));
	if (btn)
	{
		btn->SetCheck(FALSE);
	}
	for (int i = 0 ; i < 8; i++)
	{
		m_PetItem[i] = UDynamicCast(UPetItem, GetChildByID(i));
		if (!m_PetItem[i])
		{
			return FALSE;
		}
		m_PetItem[i]->SetVisible(FALSE);
	}
	return TRUE;
}
void UPetDlg::OnDestroy()
{
	m_StartPos = 0;
	m_PetGuid = 0;
	m_PetNumber = 0;
	m_CurPage = 0;
	m_MaxPage = 0;
	UDialog::OnDestroy();
}
void UPetDlg::OnClickPlayer()
{
	this->SetVisible(FALSE);
	UEquipment* pEquip = INGAMEGETFRAME(UEquipment, FRAME_IG_PLAYEREQUIPDLG);
	if (pEquip)
	{
		pEquip->SetVisible(TRUE);
	}
	UButton* btn = UDynamicCast(UButton, GetChildByID(32));
	if (btn)
	{
		btn->SetCheck(FALSE);
	}
}

void UPetDlg::OnClickPlayerFation()
{
	this->SetVisible(FALSE);
	UEquipment* pEquip = INGAMEGETFRAME(UEquipment, FRAME_IG_PLAYEREQUIPDLG);
	if (pEquip)
	{
		pEquip->SetVisible(TRUE);
		UButton* btn = UDynamicCast(UButton, pEquip->GetChildByID(40));
		if (btn)
		{
			btn->SetCheck(FALSE);
		}
	}

	UButton* btn = UDynamicCast(UButton, GetChildByID(32));
	if (btn)
	{
		btn->SetCheck(FALSE);
	}
}

void UPetDlg::OnClickPet()
{

}
void UPetDlg::OnClickDown()
{
	if (m_CurPage < m_MaxPage)
	{
		m_CurPage++;
		UpData();
	}
}
void UPetDlg::OnClickUp()
{
	if (m_CurPage > 0)
	{
		m_CurPage--;
		UpData();
	}
}

void UPetDlg::OnClose()
{
	this->SetVisible(FALSE);
	UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	if (pGameBar)
	{
		UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONEQUIP);
		if (pBtn)
		{
			pBtn->SetCheckState(FALSE);
		}
	}
}
BOOL UPetDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UPetDlg::SetVisible(BOOL value)
{
	if(value)
	{
		PacketBuilder->SendPetsListStabled();
	}
	UDialog::SetVisible(value);
}
void UPetDlg::UpData()
{
	//int EndPos = (m_vPets.size() > m_StartPos + 4) ? m_StartPos + 4 : m_vPets.size() - m_StartPos;
	for (int i = 0 ; i < 8 ; i++)
	{
		if (i + m_CurPage * 8 < m_vPets.size())
		{
			m_PetItem[i]->SetPetData(m_vPets[i + m_CurPage * 8]);
		}
		else
		{
			m_PetItem[i]->Clear();
		}
		m_PetItem[i]->SetCanStable(m_PetItem[i]->GetPetNumber() == m_PetNumber);
		m_PetItem[i]->SetUnStableActive(m_PetNumber == 0);
	}
	UStaticText* pEdit = UDynamicCast(UStaticText, GetChildByID(36));
	if (pEdit)
	{
		char buf[256];
		sprintf(buf, "%d/%d", m_CurPage + 1, m_MaxPage + 1);
		pEdit->SetText(buf);
	}
}
void UPetDlg::ParsePetList(ui8 StableSlotCount, std::vector<MSG_S2C::stPets_List_Stabled::stPet> &vPets)
{
	m_vPets = vPets;

	m_MaxPage = vPets.size() / 8;
	if (vPets.size()%8 == 0 && vPets.size() != 0)
		--m_MaxPage;

	m_CurPage = 0;
	UpData();
}
void UPetDlg::OnPetNumberChange(ui64 petguid, ui64 guid, ui32 number)
{
	if (ObjectMgr->GetLocalPlayer() && ObjectMgr->GetLocalPlayer()->GetGUID() == guid)
	{
		m_PetGuid = petguid;
		m_PetNumber = number;
		UpData();
	}
}
bool UPetDlg::OnPetPlayerGuid(ui64 guid, std::string &str)
{
	CCharacter* pChar = (CCharacter*)ObjectMgr->GetObject(guid);
	if (pChar)
	{
		str = pChar->GetName() ;
		str += _TRAN("的宠物");
		return true;
	}

	return false;
}

void UPetDlg::OnPetStableSuccess()
{
	m_PetGuid = 0;
	m_PetNumber = 0;
	UpData();
}

BOOL UPetDlg::IsLocalPet(ui64 guid)
{
	return m_PetGuid == guid && m_PetGuid != 0;
}
void UPetDlg::OnPetPlayerDel(ui64 guid, ui32 number)
{
	//if (ObjectMgr->GetLocalPlayer() && ObjectMgr->GetLocalPlayer()->GetGUID() == guid)
	//{
	//	for (int i = 0 ; i < 8 ; i++)
	//	{
	//		if(m_PetItem[i]->GetPetNumber() == number)
	//		{
	//			m_PetItem[i]->SetCanStable(FALSE);
	//		}
	//	}
	//}
}

void UPetDlg::OnPetExperience(ui32 exp, ui32 nextexp, ui32 number)
{
	for (int i = 0 ; i < 8 ; i++)
	{
		if(m_PetItem[i]->GetPetNumber() == number)
		{
			m_PetItem[i]->SetPetExper(exp, nextexp);
		}
	}
}

void UPetDlg::OnPetNameChange(ui64 guid, std::string& Name)
{
	CCharacter* pChar = (CCharacter*)ObjectMgr->GetObject(guid);
	if (pChar)
	{
		pChar->SetName(Name.c_str());
		if (m_PetGuid == guid)
		{
			ui32 PetNum =  pChar->GetUInt32Value(UNIT_FIELD_PETNUMBER);
			for (unsigned int ui = 0 ; ui < m_vPets.size(); ++ui)
			{
				if(m_vPets[ui].pet_no == PetNum)
				{
					m_vPets[ui].name = Name;
					UpData();
				}
			}
		}
	}
}