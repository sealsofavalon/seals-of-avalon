#pragma once
#include "UInc.h"
#include "UIItemSystem.h"
#include "UDynamicIconButtonEx.h"
//设置 人物属性
//显示装备
enum
{
	FIELD_ROLE_PLAYERPROPERTY,//角色属性
	FIELD_ROLE_STRENGTH = FIELD_ROLE_PLAYERPROPERTY,	//力量
	FIELD_ROLE_VITALITY,								//体力
	FIELD_ROLE_AGILITY,									//敏捷
	FIELD_ROLE_INTELLIGENCE,							//智力
	FIELD_ROLE_SPIRITE,									//精神
	FIELD_ROLE_STAMINA,									//耐力
	
	//防御属性
	FIELD_ROLE_RESISTANCES,								//护甲
	FIELD_ROLE_DODGE_PERCENTAGE,						//闪避
	FIELD_ROLE_BLOCK_PERCENTAGE,						//格挡
	
	//近战属性
	FIELD_ROLE_ATTACK_POWER,							//近战攻击强度
	FIELD_ROLE_MINDAMAGE,								//近战伤害
	FIELD_ROLE_CRIT_PERCENTAGE,							//近战暴击
	FIELD_ROLE_BASEATTACKTIME,							//近战攻击速度
	
	//远程属性
	FIELD_ROLE_RANGED_ATTACK_POWER,						//远程攻击强度
	FIELD_ROLE_MINRANGEDDAMAGE,							//远程伤害
	FIELD_ROLE_RANGED_CRIT_PERCENTAGE,					//远程暴击
	FIELD_ROLE_RANGEDATTACKTIME,						//远程攻击速度

	//法术属性
	FIELD_ROLE_SPELLDAMAGE,								//法术伤害
	FIELD_ROLE_SPELL_CRIT_PERCENTAGE,					//法术暴击

	//社会属性
	FIELD_ROLE_SOCIETYPROPERTY,
	FIELD_ROLE_HONORCURRENT = FIELD_ROLE_SOCIETYPROPERTY,
	FIELD_ROLE_HONORTOTAL,
	FIELD_ROLE_KILLCOUNT,
	FIELD_ROLE_BEKILLCOUNT,
	FIELD_MAX
};

enum
{
	Role_RESISTANCES_01, //神圣
	Role_RESISTANCES_02, //暗影
	Role_RESISTANCES_03, //冰霜
	Role_RESISTANCES_04, //火焰
	Role_RESISTANCES_05, //自然
	Role_RESISTANCES_06, //神秘

	Role_RESISTANCES_Max,
};
#include "UNiAVControl.h"
#include "Creature.h"
class UNiAVControlEq : public UNiAVControl
{
	UDEC_CLASS(UNiAVControlEq);
public:
	UNiAVControlEq();
	~UNiAVControlEq();
	void AddCharactor(CCharacter* pkChar);
	void SetRotatePlayer(float fRot);
	void UpdatePlayer(uint32 visiblebase, ui32 itemid, ui32 SlotIndex, ui32 effectid = 0);
	virtual void SetDirectionTo();
	UINT GetTopAdd(){return m_TopADD;}

	void SetZ(float fZ);
	void FillItemEffect(CPlayer* pPlayer);
	BOOL SetShowHead(BOOL bshow, ui64 guid);
protected:
	virtual void AddPlayer(CPlayer* pkChar);   //人物
	virtual void AddCCreature(CCreature* pkChar); //怪物
	virtual void GetPlayerEquipment(CPlayer* pkChar);
	virtual void SetCharInfo();
	virtual void SetCamera();
	virtual float GetCharScale();
public:
	virtual void RemoveOBJ();
    void SetHeadB3D(BOOL b3D);

protected:
	virtual BOOL OnCreate();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer = -1);
	
protected:
	CCharacter* m_Char;
	NiPoint3 m_spCameTr;
	BOOL m_IsPlayer ;
	ui64 m_Guid ;   //复制对象的GUID
	UINT m_TopADD ; // 顶部偏移量   以后扩展功能时， 需要在构造函数重设改值为0 即可
	BOOL m_isOnLine;
	BOOL m_isNeedSetInfo;
    BOOL m_BNeed3DHead;
	BOOL m_bShowHeadEq ;
};
class UPetItem : public UControl
{	
	class PetActionButton : public UDynamicIconButtonEx
	{
		UDEC_CLASS(UPetItem::PetActionButton);
		PetActionButton();
		virtual ~PetActionButton();
	protected:
		//virtual BOOL OnDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
	};
	friend class PetActionButton;
	class PetActionButtonItem : public UActionItem
	{
	public:
		virtual void Use();
	};
	UDEC_CLASS(UPetItem);
	UDEC_MESSAGEMAP();
public:
	UPetItem();
	~UPetItem();
	void SetPetData(MSG_S2C::stPets_List_Stabled::stPet &pet);
	void SetDataToUI();
	void SetCanStable(BOOL value);
	void SetUnStableActive(BOOL value);
	inline ui32 GetPetNumber();
	void SetPetExper(ui32 exp, ui32 nextexp);
	void Clear();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	void OnClickStable();
	void OnClickUnSatable();
	void OnClickDelPet();
	void OnClickPetProperty();
	void OnClickChangeName();

	MSG_S2C::stPets_List_Stabled::stPet mPetData;
	UStaticText* mNameStaticText;
    UBitmap* mPetIcon;
};
class UPetDlg : public UDialog
{
	UDEC_CLASS(UPetDlg);
	UDEC_MESSAGEMAP();
public:
	UPetDlg();
	~UPetDlg();
public:
	void ParsePetList(ui8 StableSlotCount, std::vector<MSG_S2C::stPets_List_Stabled::stPet> &vPets);

	void OnPetNumberChange(ui64 petguid, ui64 guid, ui32 number);
	bool OnPetPlayerGuid(ui64 guid, std::string &str);
	void OnPetPlayerDel(ui64 guid, ui32 number);
	void OnPetExperience(ui32 exp, ui32 nextexp, ui32 number);
	void OnPetStableSuccess();
	BOOL IsLocalPet(ui64 guid);
	virtual void SetVisible(BOOL value);
	ui64 GetPetGUID(){return m_PetGuid;}
	void OnPetNameChange(ui64 guid, std::string& Name);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
protected:
	void OnClickPlayer();
	void OnClickPlayerFation();
	void OnClickPet();
	void OnClickDown();
	void OnClickUp();

	void UpData();
private:
	std::vector<MSG_S2C::stPets_List_Stabled::stPet> m_vPets;
	ui64	  m_PetGuid;
	ui32  m_PetNumber;
	UINT	  m_StartPos;
	UINT m_CurPage;
	UINT m_MaxPage;
	UPetItem* m_PetItem[8];
};
class UEquipment : public UDialog
{
	class EquipSlot : public UDynamicIconButtonEx
	{
		UDEC_CLASS(UEquipment::EquipSlot);
		EquipSlot();
		~EquipSlot();
		virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
		virtual void OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag);
		virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
		virtual void OnRenderToolTip();
	};
	friend class EquipSlot;
	class EquipSlotItem : public UActionItem
	{
	public:
		virtual void Use();
		virtual void UseLeft();
	};
	UDEC_CLASS(UEquipment);
	UDEC_MESSAGEMAP();
public:
	UEquipment();
	~UEquipment();
	void UpdatePlayerPropety(ui32 data , int field); // 更新属性
	void UpdatePlayerRESISTANCES(ui32 data, ui32 flag);
	void SetAddLocalData(CPlayer* pkChar);
	void UpDateUNiavobject(uint32 visiblebase, ui32 itemid, ui32 SlotIndex, ui32 Effect_id = 0);
	void SetIconByPos(ui8 pos, ui32 ditemId,UItemSystem::ItemType type);
	void SetHeadImage(ui8 race,ui8 gender,ui8 Class);
	void SetLevel(ui32 level);
	void SetName(const char * name);
	void SetIsShowShiZhuang(bool value);
	void SetIsShowHelm(bool value);
	void SetShowEQHead(BOOL bshow, ui64 guid);
	virtual void SetVisible(BOOL value);
	virtual void SetPosition(const UPoint &NewPos);

	void OnClickShiZhuang();
protected:
	void ClickRight();                //3D 模型右转
	void ClickLeft();                 //3D 模型左转
	void OnClickPet();
	void OnClickPlayer();
	void OnClickGodTool();
	void OnClickTitle();
	void OnClickShiZhuangState();
	void OnClickToukuiState();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);

	UStaticText* m_RESISTANCES_Ctrl[Role_RESISTANCES_Max];
	BOOL m_IsCheckShizhuang ;
private:
};
class UViewPlayerEquipment : public UDialog
{
	class ViewEquipSlot : public UDynamicIconButtonEx
	{
		UDEC_CLASS(UViewPlayerEquipment::ViewEquipSlot);
		ViewEquipSlot();
		~ViewEquipSlot();
		virtual void OnMouseDragged(const UPoint& position, UINT flags){};
		virtual void OnRenderToolTip();
	};
	friend class ViewEquipSlot;
	class ViewEquipSlotItem : public UActionItem
	{
	protected:
		virtual void Use();
	};
	UDEC_CLASS(UViewPlayerEquipment);
	UDEC_MESSAGEMAP();
public:
	UViewPlayerEquipment();
	~UViewPlayerEquipment();
	void UpDateUNiavobject(CPlayer* pkplayer, uint32 visiblebase, ui32 itemid, ui32 SlotIndex, ui32 Effect_id = 0);
	void SetIconByPos(ui8 pos, ui32 ditemId, ui64 ItemGuid);
	void SetShowHeadEQ(BOOL bshow, ui64 guid);
	virtual void SetVisible(BOOL value);
	void SetViewPlayer(CCharacter* pChar);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();

	void OnClickRight();                //3D 模型右转
	void OnClickLeft();                 //3D 模型左转
protected:
	UPoint m_OrgenPos;
	CCharacter* m_ViewChar;
};

class UPropertyInfo : public UControl
{
	
	UDEC_CLASS(UPropertyInfo);
	UDEC_MESSAGEMAP();
	typedef struct  
	{
		ui32 RoleProperty;
		std::string strname;
		std::string str;
		URect RenderRect;
	}PropertyRecode;
public:
	UPropertyInfo();
	~UPropertyInfo();

	void OnSelShowChange();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	void RenderData(const UPoint& offset, int field);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
	PropertyRecode m_RoleProperty[FIELD_MAX];
	int m_PropertyType;
	UComboBox* m_ComboBox ;
public:
	enum
	{
		PROPERTY_TYPE_BASE,			//基础属性
		PROPERTY_TYPE_DEFENSE,		//防御属性
		PROPERTY_TYPE_ATTACK,		//近战属性
		PROPERTY_TYPE_RANGE,		//远程属性
		PROPERTY_TYPE_SPELL,		//施法属性
		PROPERTY_TYPE_SOCIETY,		//社会属性

		PROPERTY_TYPE_MAX,
	};
	void SetData(ui32 data , int field);
	void SetShowType(int stype);
	void SetRenderRect(const UPoint& offset, int field, int border, int& height);
	
};