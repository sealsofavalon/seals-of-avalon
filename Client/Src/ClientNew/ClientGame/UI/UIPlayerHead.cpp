#include "stdafx.h"
#include "UIPlayerHead.h"
#include "UIShowPlayer.h"
#include "Player.h"
#include "PlayerInputMgr.h"
#include "ObjectManager.h"
#include "UITipSystem.h"
#include "UAuraSys.h"
#include "RenderTargetToTexture.h"
#include "ClientState.h"
#include "Utils\DefaultDisplayInfoDB.h"
#include "ItemManager.h"
#include "UIIndex.h"
#include "UIGamePlay.h"
#include "SocialitySystem.h"

//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UNiAVControlHead,UNiAVControlEq)
void UNiAVControlHead::StaticInit()
{

}
UNiAVControlHead::UNiAVControlHead()
{
	//ms_pkD3DTexture = NULL;
	//m_pkLastRenderTarget = NULL;
	m_MaskTexture = NULL;
	m_NotOnLineTexture = NULL;
	m_TopADD = 0;
	m_MasterTexture = NULL;
	m_FarPlayeTexture = NULL;
    m_BNeed3DHead = FALSE;
	Init();
}
UNiAVControlHead::~UNiAVControlHead()
{
	ShutDown();
}
void UNiAVControlHead::Init()
{
	NiRenderer* pRenderer = NiRenderer::GetRenderer();
	NIASSERT(pRenderer);

	NiRenderTargetGroup* pDefaultRenderTarget = pRenderer->GetDefaultRenderTargetGroup();
	NIASSERT(pDefaultRenderTarget);

	unsigned int uiWidth = pDefaultRenderTarget->GetWidth(0);
	unsigned int uiHeight = pDefaultRenderTarget->GetHeight(0);

	NiTexture::FormatPrefs kPrefs;
	kPrefs.m_ePixelLayout = NiTexture::FormatPrefs::TRUE_COLOR_32;
	kPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
	kPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::NONE;

	m_spTexture = NiRenderedTexture::Create(128, 128, pRenderer, kPrefs);
	m_spTarget = NiRenderTargetGroup::Create(m_spTexture->GetBuffer(), pRenderer, true,true);

	m_BackColor = NiColorA(0.3f, 0.4f, 0.5f, 0.3f);
}
void UNiAVControlHead::ShutDown()
{
}

void UNiAVControlHead::AddCharactorBYData(ui64 guid, TeamDate* pData)
{
	if (guid)
	{
		CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(guid);
		if (pkChar)
		{
			AddCharactor(pkChar);
		}else
		{
			if (pData && pData->Guid)
			{
				AddPlayerBYData(pData);
			}	
		}

	}

}
void UNiAVControlHead::SetBgkColor( UINT r, UINT g, UINT b, UINT a)
{
	m_BackColor.r =  r / 255.0f ;
	m_BackColor.g =  g / 255.0f ;
	m_BackColor.b =  b / 255.0f ;
	m_BackColor.a =  a / 255.0f ;
}

void UNiAVControlHead::UpdataOBJHeadBYData(TeamDate* pData)
{
	if (pData->Guid == 0 || pData->Guid != m_Guid || !m_IsPlayer)
	{
		return ;
	}
	CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(pData->Guid);
	if (!pkPlayer)
	{
		for (int i =0; i < 4; i++)
		{
			if (pData->equiphead[i])
			{
				ui32 Index ;
				if (i == 0 )
				{
					Index = EQUIPMENT_SLOT_HEAD;
				}
				if (i == 1)
				{
					Index = EQUIPMENT_HOUNTER_TOUBU;
				}

				if (i == 2)
				{
					Index = EQUIPMENT_SLOT_CHEST;
				}

				if (i == 3)
				{
					Index = EQUIPMENT_HOUNTER_SHANGYI;
				}
				 
				ui32 visiblebase = PLAYER_VISIBLE_ITEM_1_0 + (Index * 12);
				UpdatePlayer(visiblebase,pData->equiphead[i],Index);

				//ItemMgr->OnEquipmentChange((CPlayer*)m_Char, pData->equiphead[i], Index);
			}

		}
	}
}
void UNiAVControlHead::UpdateOBJHead(ui64 guid, uint32 visiblebase, ui32 itemid, ui32 SlotIndex)
{
	if (guid == 0 || guid != m_Guid || !m_IsPlayer)
	{
		return ;
	}

	CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(guid);
	if(pkPlayer == NULL)
		return;

	if (SlotIndex == EQUIPMENT_SLOT_HEAD || SlotIndex == EQUIPMENT_HOUNTER_TOUBU || SlotIndex == EQUIPMENT_SLOT_CHEST || SlotIndex == EQUIPMENT_HOUNTER_SHANGYI )
	{
		ui32 effect_displayid = 0;
		ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (SlotIndex * 12) + 9 ;
		effect_displayid = pkPlayer->GetUInt32Value(itemeffect);
		
		UpdatePlayer(visiblebase,itemid,SlotIndex,effect_displayid);
	}
	//SetHeadB3D(m_BNeed3DHead);

	
}	
float UNiAVControlHead::GetCharScale()
{
	return 1.0f;
}
void UNiAVControlHead::AddPlayerBYData(TeamDate* pData)
{
    if(m_Char)
    {
        if(m_Char->GetAsPlayer())
            ObjectMgr->RemoveClientPlayer(m_Char->GetGUID());
        else
            NiDelete m_Char;

		m_Char = NULL ;
    }
	
	m_isNeedSetInfo = FALSE;
	m_IsPlayer = TRUE ;
	m_isOnLine = TRUE ;
    CPlayer* pkPlayer = ObjectMgr->CreateClientPlayer();
	m_Char = pkPlayer;

	ui32 displayID;
	if (!g_pkDefaultDisplayInfo->FindDefaultModelId(pData->Race, pData->Class, pData->Gender,displayID))
	{
		return ;
	}
	((CPlayer*)m_Char)->SetRace(pData->Race);
	((CPlayer*)m_Char)->SetGender(pData->Class);
	//m_Char->SetGUID( pData->guid);
	m_Guid = pData->Guid;

	//装载模型
	BOOL LoadOK = ((CPlayer*)m_Char)->CreatePlayer(displayID);

	

	//GetPlayerEquipment(pkChar);

	UpdataOBJHeadBYData(pData);
   // SetHeadB3D(m_BNeed3DHead);
}

void UNiAVControlHead::AddPlayer(CPlayer* pkChar)   //人物
{
	UNiAVControlEq::AddPlayer(pkChar);

	NiNode* pLocalNode = pkChar->GetSceneNode();
	NIASSERT(pLocalNode);
	m_spDirLight->AttachAffectedNode(pLocalNode);

	//SetHeadB3D(m_BNeed3DHead);
}
void UNiAVControlHead::AddCCreature(CCreature* pkChar) //怪物
{
	RemoveOBJ();
	m_Guid = pkChar->GetGUID();
	m_isOnLine = TRUE;
	m_IsPlayer = FALSE;
	m_isNeedSetInfo = TRUE;
	char strFile[255];
	if (pkChar)
	{
		ui32 pkCharEntry = pkChar->GetUInt32Value(OBJECT_FIELD_ENTRY);
		NiSprintf(strFile,255,"Data\\UI\\Icon\\Monster\\Icon_%d.png" ,pkCharEntry);	
	}
	if (strFile)
	{
		m_MasterTexture = sm_UiRender->LoadTexture(strFile);
	}	
}
void UNiAVControlHead::SetCamera()
{
	NiAVObject* pkCa = m_spAVObject->GetObjectByName("Camera04");
	if (pkCa)
	{
		if (NiIsKindOf(NiCamera,pkCa))
		{
			m_spCamera = (NiCamera*)pkCa;
			return ;
		}
	}

	/*if (m_spCamera == NULL)
	{
		m_spCamera = NiNew NiCamera;
	}
	NiPoint3 kTranslation(0.0f, 0.5f, 1.8f);

	m_spCameTr = kTranslation;
	NiMatrix3 kRotX;
	NiMatrix3 kRotZ;
	kRotX.MakeXRotation(-NI_HALF_PI);
	kRotZ.MakeZRotation(NI_HALF_PI);

	m_spCamera->SetRotate(kRotZ * kRotX);
	m_spCamera->SetTranslate(kTranslation);
	m_spCamera->Update(0.0f);*/

	//面向
	//SetDirectionTo();
}
BOOL UNiAVControlHead::OnCreate()
{
	if (!UNiAVControlEq::OnCreate())
	{
		return FALSE;
	}

	m_MaskTexture = sm_UiRender->LoadTexture("data\\ui\\UIPlayerShow\\Mask.dds");
	m_NotOnLineTexture = sm_UiRender->LoadTexture("data\\ui\\UIPlayerShow\\Player_NotLogin.png");
	if (!m_MaskTexture || !m_NotOnLineTexture)
	{
		return FALSE;
	}
	return TRUE;
}
void UNiAVControlHead::OnTimer(float fDeltaTime)
{
	UNiAVControlEq::OnTimer(fDeltaTime);
}
void UNiAVControlHead::OnRender(const UPoint& offset,const URect &updateRect)
{

	//不在线
	if (!m_isOnLine)
	{
		sm_UiRender->DrawImage(m_NotOnLineTexture,updateRect);
		return;
	}

	//怪物
	if (!m_IsPlayer)
	{
		if (m_MasterTexture)
		{
			sm_UiRender->DrawImage(m_MasterTexture,updateRect);
		}
		return ;
	}else
	{
		if (m_Char && !m_Char->IsActorLoaded())
		{
			return ;
		}
	}

	if(m_spCamera == NULL ||m_spAVObject == NULL || m_VisGeometries.GetCount() == 0 )
	{
		////距离太远
		//if (m_FarPlayeTexture)
		//{
		//	sm_UiRender->DrawImage(m_FarPlayeTexture,updateRect);
		//}
		return;
	}

	NiCamera* pCam = m_spCamera;
	NIASSERT(pCam);
	pCam->FitNearAndFarToBound(m_spAVObject->GetWorldBound());
	static NiVisibleArray kVisible(1024, 1024);
	NiCullingProcess kCuller(&kVisible);

	sm_UiRender->EndRender();

	NiD3DRenderer* pRenderer = (NiD3DRenderer*)NiRenderer::GetRenderer();
	NIASSERT(pRenderer);
	NiDX9RenderState* pkRenderState = pRenderer->GetRenderState();

	////////////////////////////////////////////////////////////////////////////
	
	NiRenderTargetGroup* pkLastRenderTarget = (NiRenderTargetGroup*)pRenderer->GetCurrentRenderTargetGroup();
    LPDIRECT3DBASETEXTURE9 pkD3DTexture = NULL;
	if(pkLastRenderTarget)
    {
        NiColorA kLastColor;
        pRenderer->GetBackgroundColor(kLastColor);
        pRenderer->SetBackgroundColor(m_BackColor);
        pRenderer->EndUsingRenderTargetGroup();
        pRenderer->BeginUsingRenderTargetGroup(m_spTarget, NiRenderer::CLEAR_ALL);
        pRenderer->SetCameraData(pCam);

        //////////////////////////////////////////////////////////////////////////


        PushNiState();
        NiDrawScene(pCam, m_spAVObject, kCuller);
        PopNiState();


        //////////////////////////////////////////////////////////////////////////

        bool bS0Mipmap;
        bool bNonPow2;
        bool bChanged;
        pkD3DTexture = pRenderer->GetTextureManager()->PrepareTextureForRendering(m_spTexture, bChanged, bS0Mipmap, bNonPow2);
        pRenderer->EndUsingRenderTargetGroup();
        pRenderer->BeginUsingRenderTargetGroup(pkLastRenderTarget, NiRenderer::CLEAR_NONE);
        pRenderer->SetBackgroundColor(kLastColor);
    }

	sm_UiRender->BeginRender();
    if(pkD3DTexture)
    {
        static const float rec[4] = {0.f,0.f,1.f,1.f};
        float rRec[4] = {(float)updateRect.left, (float)updateRect.top, (float)updateRect.right, (float)updateRect.bottom}; 
	    sm_UiRender->DrawMaskImageD(pkD3DTexture, rec, m_MaskTexture, rec, rRec);
    }
}
//////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UProgressBar, UControl);
void UProgressBar::StaticInit()
{
	UREG_PROPERTY("BarTexture", UPT_STRING, UFIELD_OFFSET(UProgressBar, m_strBitmapFile));
	UREG_PROPERTY("BackTexture", UPT_STRING, UFIELD_OFFSET(UProgressBar, m_strBackBitmapFile));

}
UProgressBar::UProgressBar()
{
	m_TextVisible = TRUE;
	m_IsShowText = TRUE;
	m_BkgTexture = NULL;
	m_BackTexture = NULL;
	m_BarFont = NULL;
	m_agePos = 0;
	m_lifePos = 1;
	m_fProgressPos = 0.f;
	m_bShowNum = TRUE;
	m_BarColor.Set(255,255,255,255);
}
UProgressBar::~UProgressBar()
{

}
BOOL UProgressBar::OnCreate()
{
	if(!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_strBitmapFile.GetBuffer())
	{
		if (m_BkgTexture == NULL)
		{
			m_BkgTexture = sm_UiRender->LoadTexture(m_strBitmapFile.GetBuffer());
		}
	}
	if (m_strBackBitmapFile.GetBuffer())
	{
		if (m_BackTexture == NULL)
		{
			m_BackTexture = sm_UiRender->LoadTexture(m_strBackBitmapFile.GetBuffer());
		}
	}
	if (m_BarFont == NULL)
	{
		m_BarFont = m_Style->m_spFont;
	}
	return TRUE;
}
void UProgressBar::OnDestroy()
{
	m_TextVisible = TRUE;
	m_fProgressPos = 0.f;
	m_agePos = 0;
	m_lifePos = 1;
	UControl::OnDestroy();
}
void UProgressBar::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_BkgTexture == NULL)
	{
		return UControl::OnRender(offset,updateRect);
	}

	if (m_BackTexture)
	{
		sm_UiRender->DrawImage(m_BackTexture, updateRect,0xff808080);
	}
	URect DrawBarRect(offset, m_Size);
	DrawBarRect.SetWidth(m_Size.x * m_fProgressPos);

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	g_pRenderInterface->SetScissorRect(DrawBarRect);
	sm_UiRender->DrawImage(m_BkgTexture, updateRect, m_BarColor);
	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);

	if (m_IsShowText && m_TextVisible && m_BarFont)
	{
		UStringW rendertext(mShowText.c_str());
		int height = m_BarFont->GetHeight();
		URect DrawRect;
		DrawRect.pt0.x = offset.x;
		DrawRect.pt0.y = offset.y + m_Size.y - height;
		DrawRect.pt1.x = offset.x + m_Size.x;
		DrawRect.pt1.y = offset.y + m_Size.y;		
		UDrawText(sm_UiRender, m_BarFont, DrawRect.GetPosition(), DrawRect.GetSize(), LCOLOR_WHITE, rendertext, UT_CENTER);
	}
}
void UProgressBar::OnMouseEnter(const UPoint& position, UINT flags)
{
	UControl::OnMouseEnter(position, flags);
	if (m_IsShowText)
		m_TextVisible = TRUE;
}
void UProgressBar::OnMouseLeave(const UPoint& position, UINT flags)
{
	UControl::OnMouseLeave(position, flags);
	if (m_IsShowText)
		m_TextVisible = FALSE;
}
void UProgressBar::SetTextVisible(BOOL visible)
{
	m_IsShowText = visible;
	m_TextVisible = visible;
}
void UProgressBar::SetProgressPos(ui32 agePos, ui32 lifePos)
{
	m_agePos = agePos;
	m_lifePos = lifePos;
	SetProgressShowModel(m_lifePos < 1000000);
	UpdateText();
}
void UProgressBar::SetProgressAge(ui32 age)
{
	m_agePos = age;
	UpdateText();
}
void UProgressBar::SetProgressLife(ui32 life)
{
	m_lifePos = life;
	SetProgressShowModel(m_lifePos < 1000000);
	UpdateText();
}
void UProgressBar::SetProgressPos(float pos)
{
	m_agePos = ui32(m_lifePos * pos);
	UpdateText();
}
void UProgressBar::SetTexture(const char* fileName)
{
	if (fileName)
	{
		IUTexture* tempTexture = sm_UiRender->LoadTexture(m_strBitmapFile.GetBuffer());
		if (tempTexture)
		{
			m_BkgTexture = tempTexture;
		}
	}
}
void UProgressBar::UpdateText()
{
	char buf[256];	
	if(!m_lifePos)
		return;
	m_fProgressPos = (float)m_agePos/(float)m_lifePos;
	if(m_bShowNum)
		sprintf(buf, "%u/%u", m_agePos, m_lifePos);
	else{
		sprintf(buf, "%5.2f%%", m_fProgressPos * 100);
	}
	if(m_fProgressPos > 1.f) m_fProgressPos = 1.f;
	if(m_fProgressPos < 0.f) m_fProgressPos = 0.f;
	mShowText = buf;
}
//////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UMaskCtrl, UControl)
void UMaskCtrl::StaticInit()
{

}
UMaskCtrl::UMaskCtrl()
{
	bPress = false;
	m_Guid =  0;
	m_MaskSkin = NULL;
	m_Biaoji = NULL;
	m_BKG = NULL;
}
UMaskCtrl::~UMaskCtrl()
{

}
BOOL UMaskCtrl::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_MaskSkin == NULL)
	{
		m_MaskSkin = sm_System->LoadSkin("UIPlayerShow\\CharMask.skin");
		if (!m_MaskSkin)
		{
			return FALSE;
		}
	}
	if(m_Biaoji == NULL)
	{
		m_Biaoji = sm_UiRender->LoadTexture("DATA\\UI\\UIPlayerShow\\Biaoji.png");
		if (!m_Biaoji)
		{
			return FALSE;
		}
	}
	if (m_BKG == NULL)
	{
		m_BKG = sm_UiRender->LoadTexture("DATA\\UI\\UIPlayerShow\\MaskBkg.png");
		if (!m_BKG)
		{
			return FALSE;
		}
	}
	return TRUE;
}
void UMaskCtrl::OnDestroy()
{
	bPress = false;
	m_Guid = 0;
	m_MaskIndex = 0;
	UControl::OnDestroy();
}

void UMaskCtrl::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	bPress = true;
	CaptureControl();
}
void UMaskCtrl::OnMouseUp(const UPoint& position, UINT flags)
{
	ReleaseCapture();
	if (!ObjectMgr->GetLocalPlayer())
		return;
	if (bPress && SocialitySys->GetTeamSysPtr()->IsTeamLeader(ObjectMgr->GetLocalPlayer()->GetGUID()))
	{
		TeamShow->CallRMD(TRUE, WindowToScreen(UPoint(0,0)) + m_Size, m_Guid,  PLAYER_HEAD_MAX + 1);
	}
	bPress = false;
}
void UMaskCtrl::OnRender(const UPoint& offset,const URect &updateRect)
{
	UINT mask = 8;
	CCharacter* pChar = (CCharacter*)ObjectMgr->GetObject(m_Guid);
	if (!pChar)
	{
		if(m_Guid && SocialitySys && !SocialitySys->GetTeamSysPtr()->GetTargetIcon(&mask, m_Guid))
		{
			mask = 8;
		}
	}
	else
	{
		if(m_Guid && SocialitySys && !SocialitySys->GetTeamSysPtr()->GetTargetIcon(&mask, m_Guid, pChar->GetInt32Value(OBJECT_FIELD_UNIQUE_ID)))
		{
			mask = 8;
		}
	}
	m_MaskIndex = mask;
	sm_UiRender->DrawImage(m_BKG, updateRect);
	URect contrect = updateRect;
	contrect.InflateRect(5, 5);
	if (m_MaskIndex < 0 || m_MaskIndex >= 8)
	{
		sm_UiRender->DrawImage(m_Biaoji, updateRect);
	}
	else
		sm_UiRender->DrawSkin(m_MaskSkin, m_MaskIndex, contrect);
}
//////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIPlayerHead,UControl);
UBEGIN_MESSAGE_MAP(UIPlayerHead,UControl)
UON_BN_CLICKED(MaskCtrlID, &UIPlayerHead::OnClickLeaderMask)
UEND_MESSAGE_MAP()
void UIPlayerHead::StaticInit()
{

}
UIPlayerHead::UIPlayerHead()
{
	m_Type = PLAYER_HEAD_MAX;
	m_guid = 0;
	m_HPBar = NULL;
	m_MPBar = NULL;
	m_level = NULL;
	m_Name = NULL;
	m_HeadImage = NULL;
	m_LeaderMask = NULL;
	m_IsCallRMD = FALSE;
	m_LevelBkg = NULL;
	m_RaceBkg = NULL;
	m_BattleTexture = NULL;
	m_BattleAlpha = 0;
	m_fAccTime = 0.f;

	m_pkDate = NULL;
	m_FriendBkg = NULL;
	m_ArmaBkg = NULL;
	m_CombatBkg = NULL;
}
UIPlayerHead::~UIPlayerHead()
{

}
void UIPlayerHead::UpdateOBJHead(ui64 guid, uint32 visiblebase, ui32 itemid, ui32 SlotIndex)
{
	if (guid && guid == m_guid)
	{
		m_HeadImage->UpdateOBJHead(guid, visiblebase, itemid, SlotIndex);

		if (m_pkDate)
		{
			if (SlotIndex == EQUIPMENT_SLOT_HEAD)
			{
				m_pkDate->equiphead[0] = itemid;
			}else if (SlotIndex == EQUIPMENT_HOUNTER_TOUBU)
			{
				m_pkDate->equiphead[1] = itemid;
			}else if (SlotIndex == EQUIPMENT_SLOT_CHEST )
			{
				m_pkDate->equiphead[2] = itemid;
			}else if (SlotIndex == EQUIPMENT_HOUNTER_SHANGYI)
			{
				m_pkDate->equiphead[3] = itemid;
			}
		}	
	}
}
void UIPlayerHead::SetHeadEQShow(BOOL bshow, ui64 guid)
{
	if (m_HeadImage)
	{
		m_HeadImage->SetShowHead(bshow, guid);
	}
}
void UIPlayerHead::SetHead3DShow(BOOL B3D)
{
	if (m_HeadImage)
	{
		m_HeadImage->SetHeadB3D(B3D);
	}
}
BOOL UIPlayerHead::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_HPBar == NULL)
	{
		m_HPBar = UDynamicCast(UProgressBar_Skin,GetChildByID(0));
	}
	if (m_MPBar == NULL)
	{
		m_MPBar = UDynamicCast(UProgressBar_Skin,GetChildByID(1));
	}
	if (m_level == NULL)
	{
		m_level = UDynamicCast(UStaticText,GetChildByID(4));
		m_level->SetTextEdge(true, UColor(32,2,99));
	}
	if (m_HeadImage == NULL)
	{
		m_HeadImage = UDynamicCast(UNiAVControlHead,GetChildByID(3));
	}
	if (m_Name == NULL)
	{
		m_Name = UDynamicCast(UStaticText,GetChildByID(2));
	}
	if (m_Name)
	{
		m_Name->SetTextAlign(UT_CENTER);
		m_Name->SetTextEdge(TRUE);
		m_Name->SetTextColor("02DEDF");
	}
	if (m_LeaderMask == NULL)
	{
		m_LeaderMask = UDynamicCast(UBitmap,GetChildByID(5));
	}
	if (m_LevelBkg == NULL)
	{
		m_LevelBkg = UDynamicCast(UBitmap,GetChildByID(7));
	}
	if (m_RaceBkg == NULL)
	{
		m_RaceBkg = UDynamicCast(UBitmap,GetChildByID(6));
	}
	if(m_BattleTexture == NULL)
	{
		m_BattleTexture = sm_UiRender->LoadTexture("Data//UI//MiniMap//MinimapMask.dds");
		if(!m_BattleTexture)
			return FALSE;
	}
	m_FriendBkg = GetChildByID(102);
	m_ArmaBkg = GetChildByID(103);
	m_CombatBkg = GetChildByID(101);

	if (m_HPBar && m_MPBar && m_Name && m_HeadImage && m_LeaderMask)
	{
		m_guid = 0;
		SetUI();
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
void UIPlayerHead::OnDestroy()
{
	UControl::OnDestroy();
	m_guid = 0;
	m_IsCallRMD = FALSE;
	m_BattleAlpha = 0;
	m_fAccTime = 0.f;
	m_pkDate = NULL;
}
void UIPlayerHead::SetLocalPlayer(CPlayer* pLocal)
{
	if (GetType() == PLAYER_HEAD_LOCAL)
	{
		if (pLocal)
		{
			ui64 _id = pLocal->GetGUID();
			SetGuid(_id);
			if (AuraSystem)
			{
				AuraSystem->SetGuid(AURA_LOCALSMALL,_id);
				AuraSystem->SetGuid(AURA_LOCALBIG,_id);
			}
			m_HeadImage->AddCharactor(pLocal);
		}
	}
}
void UIPlayerHead::OnTimer(float fDeltaTime)
{
	m_fAccTime += fDeltaTime;
	if(m_fAccTime >= 2.f)
		m_fAccTime = 0.f;

	if(m_fAccTime < 1.0f)
		m_BattleAlpha = min((BYTE)(100 * m_fAccTime  + 1), 100);
	else
		m_BattleAlpha = max((BYTE)(100 * (2.f - m_fAccTime ) + 1), 0);

	UControl::OnTimer(fDeltaTime);
}
void UIPlayerHead::OnMouseEnter(const UPoint& position, UINT flags)
{
	UControl::OnMouseEnter(position,flags);
}
void UIPlayerHead::OnMouseLeave(const UPoint& position, UINT flags)
{
	UControl::OnMouseLeave(position,flags);
	m_IsCallRMD = FALSE;
	if (GetType() == PLAYER_HEAD_TEAMMEM
		||GetType() == PLAYER_HEAD_TEAMMEM +1
		||GetType() == PLAYER_HEAD_TEAMMEM + 2
		||GetType() == PLAYER_HEAD_TEAMMEM + 3)
	{
		TipSystem->HideTip();
	}
}
UControl * UIPlayerHead::FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */)
{
	UControl* Ctrl = GetChildByID(MaskCtrlID);
	if ( Ctrl && Ctrl->IsVisible() && Ctrl->GetControlRect().PointInRect(pt))
	{
		return Ctrl;
	}
	return this;
}
void UIPlayerHead::OnRightMouseUp(const UPoint& position, UINT flags)
{
	if (!m_IsCallRMD)
	{
		return;
	}
	switch (GetType())
	{
	case PLAYER_HEAD_LOCAL:
		{
			TeamShow->CallRMD(TRUE,UPoint(GetWindowPos().x + GetWidth()/2,GetWindowPos().y + GetHeight()),m_guid,GetType());
		}
		break;
	case PLAYER_HEAD_TARGET:
		{
			TeamShow->CallRMD(TRUE,UPoint(GetWindowPos().x ,GetWindowPos().y + GetHeight()),m_guid,GetType());
		}
		break;
	case PLAYER_HEAD_TEAMMEM:
	case PLAYER_HEAD_TEAMMEM + 1:
	case PLAYER_HEAD_TEAMMEM + 2:
	case PLAYER_HEAD_TEAMMEM + 3:
		{
			TeamShow->CallRMD(TRUE,UPoint(GetWindowPos().x + GetWidth()/2,GetWindowPos().y + GetHeight()),m_guid,GetType());
		}
		break;
	};
}
void UIPlayerHead::OnRightMouseDown(const UPoint &position, UINT nRepCnt, UINT flags)
{
	m_IsCallRMD = TRUE;
}
void UIPlayerHead::OnMouseDown(const UPoint &point, UINT nRepCnt, UINT uFlags)
{
	if (GetType() == PLAYER_HEAD_TARGET)
	{
		return;
	}
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		if(!pkLocal->IsHook())
			SYState()->LocalPlayerInput->SetTargetID(m_guid);
	}
}
void UIPlayerHead::OnRender(const UPoint& offset,const URect &updateRect)
{
	UControl::OnRender(offset,updateRect);
	//TODO在这里渲染队长图标
	CCharacter* pChar = (CCharacter*)ObjectMgr->GetObject(m_guid);

	if (pChar && pChar->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT))
	{
		sm_UiRender->DrawImage(m_BattleTexture, URect(m_HeadImage->WindowToScreen(UPoint(0,0)), m_HeadImage->GetWindowSize()), LCOLOR_ARGB( m_BattleAlpha, 255, 0, 0 ), 1);
		if (GetType() == PLAYER_HEAD_TARGET && m_CombatBkg)
			m_CombatBkg->SetVisible(TRUE);
	}
	else
	{
		if (GetType() == PLAYER_HEAD_TARGET && m_CombatBkg)
			m_CombatBkg->SetVisible(FALSE);
	}
}
void UIPlayerHead::SetType(int Type)
{
	m_Type = Type;
	//if (m_Type == PLAYER_HEAD_TARGET && m_Name)
	//{
	//	m_Name->SetTextAlign(UT_RIGHT);
	//}
	if (m_HeadImage)
	{
		if (m_Type == PLAYER_HEAD_TARGET)
		{
			m_HeadImage->SetBgkColor(62, 6, 0, 255);
		}else
		{
			m_HeadImage->SetBgkColor(21, 42, 0, 255);
		}
	}
	
}
int UIPlayerHead::GetType()
{
	return m_Type;
}
void UIPlayerHead::ShowTeamLeaderFlag(BOOL bShow)
{
	if (m_LeaderMask)
	{
		m_LeaderMask->SetVisible(bShow);
	}

	UMaskCtrl* Ctrl = UDynamicCast(UMaskCtrl, GetChildByID(MaskCtrlID));
	if(Ctrl)
	{
		Ctrl->SetVisible( SocialitySys->GetTeamSysPtr()->IsTuanDui() );
	}
}
void UIPlayerHead::UpdateDate(ui64 guid, ui32 mask, BOOL bFromData)
{
	if (m_guid && m_guid == guid)
	{
		CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(m_guid);
		if (pkChar && !bFromData)
		{
			if (m_pkDate && !m_pkDate->islogin)
			{
				m_MPBar->SetVisible(FALSE);
				m_HPBar->SetVisible(FALSE);
				m_HeadImage->RemoveOBJ();
			}else
			{
				m_HPBar->SetVisible(TRUE);
				m_MPBar->SetVisible(TRUE);

				char lv[256];
				NiSprintf(lv,256,"%d",pkChar->GetLevel());
				m_level->SetText(lv);
				
				m_HPBar->SetProgressPos(pkChar->GetHP(),pkChar->GetMaxHP());
				m_MPBar->SetProgressPos(pkChar->GetMP(),pkChar->GetMaxMP());
				if (!m_HeadImage->isOnLine())
				{
					m_HeadImage->AddCharactorBYData(m_guid, m_pkDate);
				}
			}
			
		}else
		{
			if (m_pkDate)
			{
				if (m_pkDate->islogin)
				{
					if (mask & GROUP_UPDATE_FLAG_ONLINE)
					{
						if (!m_HeadImage->isOnLine())
						{
							m_HeadImage->AddCharactorBYData(m_guid, m_pkDate);
						}else
						{
							m_HeadImage->UpdataOBJHeadBYData(m_pkDate);
						}
					}
					if(mask & GROUP_UPDATE_FLAG_EQUIPMENT)
					{
						if (!m_HeadImage->isOnLine())
						{
							m_HeadImage->AddCharactorBYData(m_guid, m_pkDate);
						}else
						{
							m_HeadImage->UpdataOBJHeadBYData(m_pkDate);
						}
					}

					if (mask & GROUP_UPDATE_FLAG_HEALTH || mask &  GROUP_UPDATE_FLAG_MAXHEALTH)
					{
						m_HPBar->SetVisible(TRUE);
							m_HPBar->SetProgressPos(m_pkDate->hp,m_pkDate->max_hp);
					}
					
					if (mask & GROUP_UPDATE_FLAG_POWER || mask & GROUP_UPDATE_FLAG_MAXPOWER)
					{
						m_MPBar->SetVisible(TRUE);
						m_MPBar->SetProgressPos(m_pkDate->mp,m_pkDate->max_mp);
					}

					if (mask & GROUP_UPDATE_FLAG_LEVEL)
					{
						char lv[256];
						NiSprintf(lv,256,"%d",m_pkDate->lv);
						m_level->SetText(lv);
					}
					
				}else
				{
					m_MPBar->SetVisible(FALSE);
					m_HPBar->SetVisible(FALSE);
					m_HeadImage->RemoveOBJ();
				}
			}
		}
	}
}
void UIPlayerHead::SetUI()
{
	if (m_guid == 0)
	{
		SetVisible(FALSE);
		return;
	}
	else
	{
		SetVisible(TRUE);
		CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(m_guid);
		CPlayerLocal* pkLocalPlayer = ObjectMgr->GetLocalPlayer();
		std::string name ;
		char lv[256];
		BOOL bLeader = TeamShow->IsTeamLeader(m_guid);
		m_LeaderMask->SetVisible(bLeader);
		int clevel = 0 ;
		BOOL ShowClass = TRUE ;
		ui8  Class = 0;

		UMaskCtrl* Ctrl = UDynamicCast(UMaskCtrl, GetChildByID(MaskCtrlID));
		if(Ctrl)
		{
			Ctrl->SetVisible( SocialitySys->GetTeamSysPtr()->IsTuanDui() );
			Ctrl->SetGuid(m_guid);
		}


		if (pkChar)
		{
			if (pkChar->GetName().GetLength())
				name = pkChar->GetName();
			else
				name = "  ";

			NiSprintf(lv,256,"%d",pkChar->GetLevel());
			m_Name->SetText(name.c_str());
			m_level->SetText(lv);
			m_level->SetTextAlign(UT_CENTER);
			m_level->SetTextColor("FFFF00");

			m_HPBar->SetVisible(TRUE);
			m_MPBar->SetVisible(TRUE);
			m_HPBar->SetProgressPos(pkChar->GetHP(),pkChar->GetMaxHP());
			m_MPBar->SetProgressPos(pkChar->GetMP(),pkChar->GetMaxMP());
			if (GetType() == PLAYER_HEAD_TARGET)
			{
				bool bFriend = pkLocalPlayer->IsFriendly(pkChar);
				if (bFriend)
				{
					m_HPBar->SetSkin("UIPlayerShow\\HPBar_Target_Friend.skin");
				}else
				{
					m_HPBar->SetSkin("UIPlayerShow\\HPBar_Target.skin");
				}
				if (m_FriendBkg && m_ArmaBkg)
				{
					m_FriendBkg->SetVisible(bFriend);
					m_ArmaBkg->SetVisible(!bFriend);
				}
			}
			m_HeadImage->AddCharactorBYData(m_guid,m_pkDate);
			clevel = pkLocalPlayer->GetLevel() - pkChar->GetLevel();

			uint32 id = 0;
			if (!pkChar->GetAsPlayer())
			{
				Class = 0;
				//ShowClass = FALSE;
				ItemMgr->GetCreatureRankId(pkChar->GetUInt32Value(OBJECT_FIELD_ENTRY), id);
			}else
			{
				Class = pkChar->GetAsPlayer()->GetClass();
			}
			UControl* pCtrl = GetChildByID(200);
			if ( pCtrl )
			{
				pCtrl->SetVisible(id > 0);
			}

		}else
		{
			if (m_pkDate)
			{
				name = m_pkDate->Name;
				NiSprintf(lv,256,"%d",m_pkDate->lv);
				m_Name->SetText(name.c_str());
				m_level->SetTextAlign(UT_CENTER);
				m_level->SetText(lv);
				m_level->SetTextColor("FFFF00");
				clevel =  pkLocalPlayer->GetLevel() - m_pkDate->lv;
				if (m_pkDate->islogin)
				{
					m_HPBar->SetVisible(TRUE);
					m_MPBar->SetVisible(TRUE);
					m_HPBar->SetProgressPos(m_pkDate->hp,m_pkDate->max_hp);
					m_MPBar->SetProgressPos(m_pkDate->mp,m_pkDate->max_mp);
					m_HeadImage->AddCharactorBYData(m_guid,m_pkDate);	
					if (GetType() == PLAYER_HEAD_TARGET)
					{
						m_HPBar->SetSkin("UIPlayerShow\\HPBar_Target_Friend.skin");
						if (m_FriendBkg && m_ArmaBkg)
						{
							m_FriendBkg->SetVisible(TRUE);
							m_ArmaBkg->SetVisible(FALSE);
						}
					}
				}else
				{
					m_MPBar->SetVisible(FALSE);
					m_HPBar->SetVisible(FALSE);
					if (GetType() == PLAYER_HEAD_TARGET)
					{
						if (m_FriendBkg && m_ArmaBkg)
						{
							m_FriendBkg->SetVisible(TRUE);
							m_ArmaBkg->SetVisible(FALSE);
						}
					}
					m_HeadImage->RemoveOBJ();
				}

				Class = m_pkDate->Class;
			}else
			{
				ShowClass = FALSE ;
			}
			UControl* pCtrl = GetChildByID(200);
			if ( pCtrl )
			{
				pCtrl->SetVisible(FALSE);
			}
		}
		if (m_Type== PLAYER_HEAD_TARGET)
		{
			if (clevel <= -4)
				m_level->SetTextColor("FF0000");
			else if (NiAbs(float(clevel)) < 4.f )
				m_level->SetTextColor("00FF00");
			else if (clevel >= 4)
				m_level->SetTextColor("BABABA");
			else
				m_level->SetTextColor("FFFF00");
		}

		if (m_RaceBkg)
		{
			m_RaceBkg->SetVisible(ShowClass);
			if (ShowClass)
			{
				switch(Class)
				{
				case CLASS_WARRIOR:
					m_RaceBkg->SetBitMapSkinByStr("UIPlayerShow\\WuXiu.skin");
					break;
				case CLASS_BOW:
					m_RaceBkg->SetBitMapSkinByStr("UIPlayerShow\\YuJian.skin");
					break;
				case CLASS_PRIEST:
					m_RaceBkg->SetBitMapSkinByStr("UIPlayerShow\\XianDao.skin");
					break;
				case CLASS_MAGE:
					m_RaceBkg->SetBitMapSkinByStr("UIPlayerShow\\ZhenWu.skin");
					break;
				case CLASS_DRUID:
				case CLASS_ROGUE:
					break;
				default:
					m_RaceBkg->SetBitMapSkinByStr("UIPlayerShow\\Default.skin");
					break;
				};
			}
		}
		
	} 
}

void UIPlayerHead::OnClickLeaderMask()
{
	UPoint pos;
	UControl* Ctrl = GetChildByID(MaskCtrlID);
	if(Ctrl)
	{
		pos = Ctrl->WindowToScreen(UPoint(0,0)) + Ctrl->GetWindowSize();
	}
	TeamShow->CallRMD(TRUE, pos, m_guid,  PLAYER_HEAD_MAX + 1);
}

void UIPlayerHead::SetGuid(ui64 guid, TeamDate* pkDate)
{
	BOOL bResetUI = FALSE ;
	if (guid != m_guid)
	{
		bResetUI = TRUE ;
	}
	m_guid = guid;
	m_pkDate = pkDate ;
	if (guid == 0 && GetType() == PLAYER_HEAD_TARGET)
	{
		TeamShow->SetTargetTarget(0);
	}
	switch(GetType())
	{
	case PLAYER_HEAD_TEAMMEM:
		AuraSystem->SetGuid(AURA_TEAMMEM,guid);
		break;
	case PLAYER_HEAD_TEAMMEM + 1:
		AuraSystem->SetGuid(AURA_TEAMMEM + 1,guid);
		break;
	case PLAYER_HEAD_TEAMMEM + 2:
		AuraSystem->SetGuid(AURA_TEAMMEM + 2,guid);
		break;
	case PLAYER_HEAD_TEAMMEM + 3:
		AuraSystem->SetGuid(AURA_TEAMMEM + 3,guid);
		break;
	}

	UMaskCtrl* Ctrl = UDynamicCast(UMaskCtrl, GetChildByID(MaskCtrlID));
	if(Ctrl)
	{
		Ctrl->SetVisible( SocialitySys->GetTeamSysPtr()->IsTuanDui() );
		Ctrl->SetGuid(guid);
	}
	if (bResetUI)
	{
		SetUI();
	}
}
ui64 UIPlayerHead::GetGuid()
{
	return m_guid;
}
BOOL UIPlayerHead::renderTooltip(const UPoint& cursorPos, const char* tipText /* = NULL */ )
{
	if (m_MouseHover)
	{
		if (GetType() == PLAYER_HEAD_TEAMMEM
			||GetType() == PLAYER_HEAD_TEAMMEM +1
			||GetType() == PLAYER_HEAD_TEAMMEM + 2
			||GetType() == PLAYER_HEAD_TEAMMEM + 3)
		{
			TipSystem->ShowTip(TIP_TYPE_PLAYER,0,GetWindowPos() + GetWindowSize(),m_guid);
			return TRUE;
		}
	}
	return FALSE;
}
void UIPlayerHead::GetName(char * name)
{
    if(m_Name)
	    m_Name->GetText(name,1024);
    else
        strcpy(name, " "); 
}
BOOL UIPlayerHead::OnEscape()
{
	if(!UControl::OnEscape())
	{
		CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
		if (pkLocal && !pkLocal->IsHook())
		{
			SYState()->LocalPlayerInput->SetTargetID(INVALID_OBJECTID);
		}
	}
	return TRUE;
}

UIMP_CLASS(UITargetTarget, UControl)
void UITargetTarget::StaticInit()
{

}

UITargetTarget::UITargetTarget()
{
	//mpChar = NULL;
	muCharGuid = 0;

	m_HPBar = NULL;
	m_MPBar = NULL;
	m_Name = NULL;
	mHeadBitmap =NULL;

	mbfriend = false;
	mbShow = true;
}

UITargetTarget::~UITargetTarget()
{
	//mpChar = NULL;
	muCharGuid = 0;

	m_HPBar = NULL;
	m_MPBar = NULL;
	m_Name = NULL;
	mHeadBitmap =NULL;
}

BOOL UITargetTarget::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_HPBar == NULL)
	{
		m_HPBar = UDynamicCast(UProgressBar_Skin, GetChildByID(0));
		m_HPBar->SetShowText(false);
	}
	if (m_MPBar == NULL)
	{
		m_MPBar = UDynamicCast(UProgressBar_Skin, GetChildByID(1));
		m_MPBar->SetShowText(false);
	}
	if (m_Name == NULL )
	{
		m_Name = UDynamicCast(UStaticText, GetChildByID(2));
	}
	if (m_Name)
	{
		m_Name->SetTextAlign(UT_LEFT);
		m_Name->SetTextEdge(true);
	}
	if (mHeadBitmap == NULL )
	{
		mHeadBitmap = UDynamicCast(UBitmap, GetChildByID(3));
	}
	//mpChar = NULL;
	muCharGuid = 0;
	return TRUE;
}

void UITargetTarget::OnDestroy()
{
	UControl::OnDestroy();
}
UControl * UITargetTarget::FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */)
{
	return this;
}

void UITargetTarget::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	SYState()->LocalPlayerInput->SetTargetID(muCharGuid);
}

void UITargetTarget::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_Visible)
		RenderChildWindow(offset, updateRect);
}

void UITargetTarget::SetField(int field, ui32 content)
{
	if (field == FIELD_TARGETTARGET_HP)
	{
		m_HPBar->SetProgressAge(content);
		if (mbfriend)
			m_HPBar->SetSkin("UIPlayerShow\\HPBar_TargetTarget_friend.skin");
		else
			m_HPBar->SetSkin("UIPlayerShow\\HPBar_TargetTarget.skin");

	}
	if (field == FIELD_TARGETTARGET_MAXHP)
	{
		m_HPBar->SetProgressLife(content);
		if (mbfriend)
			m_HPBar->SetSkin("UIPlayerShow\\HPBar_TargetTarget_friend.skin");
		else
			m_HPBar->SetSkin("UIPlayerShow\\HPBar_TargetTarget.skin");
	}
	if (field == FIELD_TARGETTARGET_MP)
	{
		m_MPBar->SetProgressAge(content);
	}
	if (field == FIELD_TARGETTARGET_MAXMP)
	{
		m_MPBar->SetProgressLife(content);
	}
}
void UITargetTarget::SetShow(bool isShow)
{
	mbShow = isShow;
	if (!isShow)
	{
		SetVisible(FALSE);
		UInGame::GetFrame(FRAME_IG_TARGETTARGETCASTBAR)->SetVisible(FALSE);
	}
	else
		SetTarget((CCharacter*)ObjectMgr->GetObject(muCharGuid));
};

void UITargetTarget::SetTarget(CCharacter* pChar)
{
	if (pChar)
	{
		mbfriend = ObjectMgr->GetLocalPlayer()->IsFriendly(pChar);
		m_HPBar->SetProgressPos(pChar->GetHP(), pChar->GetMaxHP());
		m_MPBar->SetProgressPos(pChar->GetMP(), pChar->GetMaxMP());
		m_Name->SetText(pChar->GetName());
		if (mbfriend)
			m_HPBar->SetSkin("UIPlayerShow\\HPBar_TargetTarget_friend.skin");
		else
			m_HPBar->SetSkin("UIPlayerShow\\HPBar_TargetTarget.skin");

		CCharacter* pkchar = (CCharacter*)ObjectMgr->GetObject(muCharGuid);
		if (pkchar)
			pkchar->SetTargetTarget(false);
		muCharGuid = pChar->GetGUID();
		pChar->SetTargetTarget(true);
		switch(pChar->GetGameObjectType())
		{
		case GOT_PLAYER:
			{
				if(mbShow)
					SetVisible(TRUE);
				mHeadBitmap->SetBitMapByStr(CPlayer::GetHeadImageFile((CPlayer*)pChar, pChar->GetRace(), pChar->GetGender()));
			}
			break;
		case GOT_CREATURE:
			{
				if(mbShow)
					SetVisible(TRUE);
				mHeadBitmap->SetBitMapByStr(CCharacter::GetHeadImageFile(pChar));
			}
			break;
		}
	}
	else
	{
		UControl* pSpellProcessBar = UInGame::GetFrame(FRAME_IG_TARGETTARGETCASTBAR);
		if(pSpellProcessBar)
			pSpellProcessBar->SetVisible(FALSE);
		muCharGuid = 0;
		SetVisible(FALSE);
	}
}