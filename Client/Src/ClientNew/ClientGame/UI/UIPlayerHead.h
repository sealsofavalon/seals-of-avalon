#ifndef UIPLAYERHEAD_H
#define UIPLAYERHEAD_H
#include "UInc.h"
#include "UIPlayerEquipment.h"
struct TeamDate;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////



class UNiAVControlHead : public UNiAVControlEq
{
	UDEC_CLASS(UNiAVControlHead);
public:
	UNiAVControlHead();
	~UNiAVControlHead();
	void AddCharactorBYData(ui64 guid, TeamDate* pData);
	void UpdataOBJHeadBYData(TeamDate* pData);
	void SetBgkColor( UINT r, UINT g, UINT b, UINT a);
	void UpdateOBJHead(ui64 guid, uint32 visiblebase, ui32 itemid, ui32 SlotIndex);
	BOOL isOnLine(){return m_isOnLine;}
protected:
	virtual void SetCamera();
	virtual float GetCharScale();
	virtual void AddCCreature(CCreature* pkChar); //怪物
	virtual void AddPlayer(CPlayer* pkChar);   //人物
	virtual void AddPlayerBYData(TeamDate* pData);
	void Init();
	void ShutDown();
protected:
	BOOL virtual OnCreate();
	void virtual OnRender(const UPoint& offset,const URect &updateRect);
	void virtual OnTimer(float fDeltaTime);
protected:
	NiRenderedTexturePtr m_spTexture;
	NiRenderTargetGroupPtr m_spTarget;
	UTexturePtr m_MaskTexture; 
	UTexturePtr m_NotOnLineTexture; //不在线标志
	UTexturePtr m_MasterTexture; // 费玩家用图
	NiColorA m_BackColor; //背景颜色

	UTexturePtr m_FarPlayeTexture; //太远的时候使用
	ui8 m_Race;
	ui8 m_Gender;
};




class UProgressBar : public UControl
{
	UDEC_CLASS(UProgressBar);
public:
	UProgressBar();
	~UProgressBar();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
public:
	virtual void SetProgressPos(ui32 agePos, ui32 lifePos);
	void SetProgressShowModel(BOOL bNum){ m_bShowNum = bNum;};
	void SetTextVisible(BOOL visible);
	void SetProgressAge(ui32 age);
	void SetProgressLife(ui32 life);
	void SetProgressPos(float pos);
	void SetTexture(const char* fileName);
	void UpdateText();
	void SetBarColor(DWORD color){ m_BarColor = color;};
protected:
	BOOL m_IsShowText;
	BOOL m_TextVisible;
	UString m_strBitmapFile;
	UString m_strBackBitmapFile;
	UTexturePtr m_BkgTexture;
	UTexturePtr m_BackTexture;

	ui32 m_agePos;
	ui32 m_lifePos;
	float m_fProgressPos;
	BOOL m_bShowNum;
	UFontPtr m_BarFont;
	std::string mShowText;
	UColor m_BarColor;
};

#define MaskCtrlID 911
class UMaskCtrl : public UControl
{
	UDEC_CLASS(UMaskCtrl);
public:
	UMaskCtrl();
	~UMaskCtrl();

	inline void SetGuid(ui64 guid){m_Guid = guid;};
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);

	bool bPress;
	USkinPtr m_MaskSkin;
	UINT m_MaskIndex;
	UTexturePtr m_Biaoji;
	UTexturePtr m_BKG;
	ui64 m_Guid;
};
class UIPlayerHead : public UControl
{
	UDEC_CLASS(UIPlayerHead);
	UDEC_MESSAGEMAP();
public:
	UIPlayerHead();
	~UIPlayerHead();
public:
	void UpdateOBJHead(ui64 guid, uint32 visiblebase, ui32 itemid, ui32 SlotIndex);
	void UpdateDate(ui64 guid, ui32 mask, BOOL bFromData); //有数据发下来后。重新设置
	void ShowTeamLeaderFlag(BOOL bShow);
	void SetHead3DShow(BOOL B3D);
	void SetHeadEQShow(BOOL bshow, ui64 guid);
	void SetLocalPlayer(CPlayer* pLocal);
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnTimer(float fDeltaTime);
	virtual UControl * FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual BOOL OnEscape();
	virtual BOOL renderTooltip(const UPoint& cursorPos, const char* tipText /* = NULL */ );
public:
	void SetType(int Type);
	int GetType();
	BOOL IsTeammember(){return (m_pkDate != NULL);}
	TeamDate* GetTeamDate(){return m_pkDate;}
	void SetGuid(ui64 guid, TeamDate* pkDate = NULL);
	ui64 GetGuid();
	void GetName(char * name);

	void SetUI();

	void OnClickLeaderMask();
protected:
	void ShowTip(float fDelteTime);
private:
	TeamDate* m_pkDate;
	UProgressBar_Skin *m_HPBar,*m_MPBar;
	UStaticText *m_Name,*m_level;
	UNiAVControlHead * m_HeadImage;
	UBitmap * m_LeaderMask;
	UBitmap * m_LevelBkg;
	UBitmap * m_RaceBkg;

	UControl* m_FriendBkg;
	UControl* m_CombatBkg;
	UControl* m_ArmaBkg;

	int m_Type;
	ui64 m_guid;
	BOOL m_IsCallRMD;
	BOOL m_ClassChanged;
	UTexturePtr m_BattleTexture;
	BYTE m_BattleAlpha;
	float m_fAccTime;
};

class UITargetTarget : public UControl
{
	UDEC_CLASS(UITargetTarget);
public:
	UITargetTarget();
	~UITargetTarget();

	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual UControl * FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */);

	void SetTarget(CCharacter* pChar);
	void SetField(int field, ui32 content);
	void SetShow(bool isShow);
	inline bool GetShow(){return mbShow;}
private:
	UProgressBar_Skin *m_HPBar,*m_MPBar;
	UStaticText *m_Name;
	UBitmap* mHeadBitmap;

	//CCharacter* mpChar;
	ui64 muCharGuid;
	bool mbfriend;

	bool mbShow;
};
#endif