#include "StdAfx.h"
#include "Network/PacketBuilder.h"
#include "UIPlayerTrade.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "UMessageBox.h"
#include "ItemSlot.h"
#include "ObjectManager.h"
#include "UIPackage.h"
#include "ItemManager.h"
#include "Effect/EffectManager.h"
#include "UFun.h"

//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UPlayerTrade_TradeItem::TradeItemSlot, UDynamicIconButtonEx)
void UPlayerTrade_TradeItem::TradeItemSlot::StaticInit()
{

}
UPlayerTrade_TradeItem::TradeItemSlot::TradeItemSlot()
{
	m_ActionContener.SetActionItem(new UPlayerTrade_TradeItem::TradeItemSlotItem);
	m_IsTickable = FALSE;
}
UPlayerTrade_TradeItem::TradeItemSlot::~TradeItemSlot()
{

}
BOOL UPlayerTrade_TradeItem::TradeItemSlot::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (UDynamicIconButtonEx::OnMouseUpDragDrop(pDragFrom,point,pDragData,nDataFlag))
	{
		if (nDataFlag == UItemSystem::ICT_BAG)
		{
			const ui8* FromePos = (const ui8*)pDragData;
			SYState()->UI->GetUItemSystem()->MoveItemMsg((ui8)nDataFlag, (ui8)m_TypeID, *FromePos, GetItemData()->pos, GetItemData()->num);
			return TRUE;
		}
	}
	return FALSE;
}
void UPlayerTrade_TradeItem::TradeItemSlot::OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag)
{

}
void UPlayerTrade_TradeItem::TradeItemSlot::DrawPushButton(const UPoint& offset, const URect& ClipRect)
{
	URect rect(offset,m_Size);
	if (m_BackGSkin)
		sm_UiRender->DrawSkin(m_BackGSkin,m_Active?BACKSKIN_ACTIVE:BACKSKIN_INACTIVE,rect);

	if(m_bInDrag)
	{
		if (m_ActionContener.IsCreate())
			m_ActionContener.DrawActionContener(sm_UiRender,offset,ClipRect);
		return;
	}
	URect IconRect( m_IconOffset, m_IconOffset, m_OldSize.x - m_IconOffset, m_OldSize.y - m_IconOffset);
	IconRect.TransformRect(m_OldSize,m_Size);
	IconRect.Offset(offset);

	if (m_IconSkin)
		sm_UiRender->DrawSkin(m_IconSkin,m_IconIndex,IconRect);

	int textH = m_Style->m_spFont->GetHeight();

	CPlayer* pLcalPlayer = ObjectMgr->GetLocalPlayer();
	if (!pLcalPlayer)
		return;

	if (m_ActionContener.IsCreate())
	{
		if (m_bAcceptDrag && m_ActionContener.IsItemEmpty())
		{
			sm_UiRender->DrawImage(m_WaringTexture, IconRect, LCOLOR_ARGB(255, 0, 255, 0));
		}

		m_ActionContener.DrawActionContener(sm_UiRender, offset, ClipRect);
		m_ActionContener.DrawTickTime(sm_UiRender, IconRect);
		//如果是药品类。。。。要实时更新数量
		ActionDataInfo * pData = GetItemData();

		USetTextEdge(TRUE);
		if (pData && pData->type == ActionButton_Type_Item)
		{
			if (pData->entry != 0 )
			{
				
				UPoint txtPos(offset.x, offset.y + m_Size.y - textH);
				UPoint txtSize(IconRect.GetWidth(), textH);
				
				ItemPrototype_Client* pItem = ItemMgr->GetItemPropertyFromDataBase(pData->entry);
				if(GetItemData()->num)
				{
						char buf[10];
						NiSprintf(buf,10,"%d",GetItemData()->num);
						if (pItem && (pItem->Class == ITEM_CLASS_CONSUMABLE || pItem->Class == ITEM_CLASS_USE || pItem->Class == ITEM_CLASS_RECIPE))
						{
							UDrawText(sm_UiRender,m_Style->m_spFont,txtPos,txtSize,LCOLOR_WHITE, buf,UT_RIGHT);
						}
				}
				else
				{
						if (pItem && (pItem->Class == ITEM_CLASS_CONSUMABLE || pItem->Class == ITEM_CLASS_USE || pItem->Class == ITEM_CLASS_RECIPE))
						{
							UDrawText(sm_UiRender,m_Style->m_spFont,txtPos,txtSize,LCOLOR_RED,"0",UT_RIGHT);
							sm_UiRender->DrawRectFill(IconRect, 0xB4000000);
						}
				}
				
			}
		}
	}
	if (mAcceleratorKey.GetLength() > 0 && mKeyCodeStr.find("NULL") == -1 && !m_ActionContener.IsItemEmpty())
	{
		UPoint AccKeytextpos(offset.x + m_IconOffset, offset.y + m_IconOffset);
		UDrawText(sm_UiRender, m_Style->m_spFont, AccKeytextpos, UPoint(IconRect.GetWidth(), textH), LCOLOR_WHITE,
			mKeyCodeStr.c_str(), UT_RIGHT);
	}
	USetTextEdge(FALSE);
}
void UPlayerTrade_TradeItem::TradeItemSlot::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	if (m_TypeID == UItemSystem::ICT_PLAYER_TRADE_TENNET)
	{
		return;
	}
	if (m_ActionContener.IsCreate())
	{
		//if (!m_ActionContener.IsItemEmpty())
		//{
		//	m_BeginDrag = TRUE;
		//}
		CaptureControl();
		m_ActionContener.Press(MOUSEDOWN_LEFT);
	}
}
//////////////////////////////////////////////////////////////////////////
void UPlayerTrade_TradeItem::TradeItemSlotItem::Use()
{
	UPlayerTrade * pPlayerTrade = INGAMEGETFRAME(UPlayerTrade,FRAME_IG_PLAYERTRADEDLG);
	if (pPlayerTrade)
	{
		pPlayerTrade->ReleaseTradeItem(m_dataInfo.pos);
	}
}

void UPlayerTrade_TradeItem::TradeItemSlotItem::UseLeft()
{

}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UPlayerTrade_TradeItem, UControl)
void UPlayerTrade_TradeItem::StaticInit()
{

}
UPlayerTrade_TradeItem::UPlayerTrade_TradeItem()
{

}
UPlayerTrade_TradeItem::~UPlayerTrade_TradeItem()
{

}
BOOL UPlayerTrade_TradeItem::OnCreate()
{
	if(!UControl::OnCreate())
	{
		return FALSE;
	}
	return TRUE;
}
void UPlayerTrade_TradeItem::OnDestroy()
{
	UControl::OnDestroy();
}


//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UPlayerTradeContent, UControl);
void UPlayerTradeContent::StaticInit()
{

}
UPlayerTradeContent::UPlayerTradeContent()
{
	m_bLock = false;
	m_LockTex = NULL;

}
UPlayerTradeContent::~UPlayerTradeContent()
{

}
BOOL UPlayerTradeContent::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_LockTex == NULL)
	{
		m_LockTex = sm_UiRender->LoadTexture("DATA\\UI\\PlayerTrade\\LockRect.png");
		if (!m_LockTex)
		{
			return FALSE;
		}
	}
	for (int i = 0 ; i < 8 ; i++)
	{
		UPlayerTrade_TradeItem* Item = UDynamicCast(UPlayerTrade_TradeItem, GetChildByID(i));
		if (Item)
		{
			UDynamicIconButtonEx* pSlot = UDynamicCast(UDynamicIconButtonEx, Item->GetChildByID(0));
			if (pSlot)
			{
				ActionDataInfo datainfo;
				datainfo.pos = i;
				pSlot->SetItemData(datainfo);
			}
		}
	}
	return TRUE;
}
void UPlayerTradeContent::OnDestroy()
{
	m_bLock = false;
	m_LockTex = NULL;
	UControl::OnDestroy();
}
void UPlayerTradeContent::OnRender(const UPoint& offset,const URect &updateRect)
{
	RenderChildWindow(offset, updateRect);
	if (m_bLock)
	{
		sm_UiRender->DrawImage(m_LockTex, offset);
	}
}

void UPlayerTradeContent::SetIconByPos(ui8 pos, ui32 dwitemid, ItemExtraData& stItemExtraData, ui16 type, int num)
{
	ActionDataInfo datainfo;
	if (pos < 0 || pos >= 8)
	{
		return;
	}
	datainfo.entry = dwitemid;
	datainfo.Guid = 0;
	datainfo.pos = pos;
	datainfo.type = type;
	datainfo.num = num;

	ItemExtraData* pNewData = new ItemExtraData(stItemExtraData);
	datainfo.Data = pNewData;
	datainfo.DataSize = sizeof(ItemExtraData);

	UPlayerTrade_TradeItem * Item = UDynamicCast(UPlayerTrade_TradeItem, GetChildByID(pos));
	if (Item)
	{
		ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(dwitemid);
		if (ItemInfo)
		{
			UStaticText* pText = UDynamicCast(UStaticText, Item->GetChildByID(1));
			if (pText)
			{
				pText->SetText(ItemInfo->C_name.c_str());
				pText->SetTextAlign(UT_LEFT);
				UColor color;
				if(GetQualityColor(ItemInfo->Quality, color))
					pText->SetTextColor(color);
			}
			pText = UDynamicCast(UStaticText, Item->GetChildByID(2));
			if (pText)
			{
				pText->SetText( _TRAN("未绑定") );
			}
			pText = UDynamicCast(UStaticText, Item->GetChildByID(3));
			if (pText)
			{
				pText->SetText( _TRAN("可出售") );
			}
		}
		else
		{
			UStaticText* pText = UDynamicCast(UStaticText, Item->GetChildByID(1));
			if (pText)
			{
				pText->Clear();
			}
			pText = UDynamicCast(UStaticText, Item->GetChildByID(2));
			if (pText)
			{
				pText->Clear();
			}
			pText = UDynamicCast(UStaticText, Item->GetChildByID(3));
			if (pText)
			{
				pText->Clear();
			}

		}
		UDynamicIconButtonEx* button = UDynamicCast(UDynamicIconButtonEx, Item->GetChildByID(0));
		if (button)
		{
			button->SetItemData(datainfo);
		}
	}
}
INT UPlayerTradeContent::FindNullPos()
{
	int pos = -1;
	for (int i = 0 ; i < 8 ; i++)
	{
		UPlayerTrade_TradeItem* Item = UDynamicCast(UPlayerTrade_TradeItem, GetChildByID(i));
		if (Item)
		{
			UDynamicIconButtonEx* pSlot = UDynamicCast(UDynamicIconButtonEx, Item->GetChildByID(0));
			if (pSlot)
			{
				if(pSlot->GetItemData() && pSlot->GetItemData()->entry == 0)
				{
					pos = i;
					break;
				}
			}
		}
	}
	return pos;
}
void UPlayerTradeContent::SetType(ui32 type)
{
	for (int i = 0 ; i < 8 ; i++)
	{
		UPlayerTrade_TradeItem* Item = UDynamicCast(UPlayerTrade_TradeItem, GetChildByID(i));
		if (Item)
		{
			UDynamicIconButtonEx* pSlot = UDynamicCast(UDynamicIconButtonEx, Item->GetChildByID(0));
			if (pSlot)
			{
				pSlot->SetType(type);
			}
		}
	}
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UPlayerTrade,UDialog);

UBEGIN_MESSAGE_MAP(UPlayerTrade,UDialog)
UON_BN_CLICKED(4, &UPlayerTrade::SendTradeOK)
UON_BN_CLICKED(5, &UPlayerTrade::SendTradeCancel)
UON_UGD_DBCLICKED(7, &UPlayerTrade::ClickLocalTradeList)
UEND_MESSAGE_MAP()
void UPlayerTrade::StaticInit()
{

}

UPlayerTrade::UPlayerTrade()
{
	m_TennetPlayerName = NULL;   //名字
	m_LocalPlayerContent = NULL;
	m_TennetPlayerContent = NULL;

	//本地玩家(自己)
	m_LocalPlayerName = NULL;
	m_LocalGold = NULL;
	m_LocalSilver = NULL;
	m_LocalCopper = NULL;

	SetLockState(false);
	m_TelnetLock = FALSE;
	m_guid = 0;
}

UPlayerTrade::~UPlayerTrade()
{

}
BOOL UPlayerTrade::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	m_TennetPlayerName = (UStaticText*)GetChildByID(1);
	m_LocalPlayerName = (UStaticText*)GetChildByID(3);
	m_LocalGold = (UEdit*)GetChildByID(21);
	m_LocalSilver = (UEdit*)GetChildByID(22);
	m_LocalCopper = (UEdit*)GetChildByID(23);
	m_LocalPlayerContent = (UPlayerTradeContent*)GetChildByID(8);
	m_TennetPlayerContent = (UPlayerTradeContent*)GetChildByID(9);

	if (m_TennetPlayerName == NULL || m_LocalPlayerName == NULL 
		|| m_LocalGold == NULL || m_LocalSilver == NULL 
		|| m_LocalCopper == NULL || m_LocalPlayerContent == NULL || m_TennetPlayerContent == NULL)
	{
		return FALSE;
	}
	
	m_LocalPlayerContent->SetType(UItemSystem::ICT_PLAYER_TRADE);
	m_TennetPlayerContent->SetType(UItemSystem::ICT_PLAYER_TRADE_TENNET);

	m_LocalGold->SetActive(TRUE);
	m_LocalGold->SetMaxLength(8);
	m_LocalGold->SetNumberOnly(TRUE);

	m_LocalCopper->SetActive(TRUE);
	m_LocalCopper->SetMaxLength(2);
	m_LocalCopper->SetNumberOnly(TRUE);

	m_LocalSilver->SetActive(TRUE);
	m_LocalSilver->SetMaxLength(2);
	m_LocalSilver->SetNumberOnly(TRUE);

	m_bCanDrag = FALSE;

	if (m_CloseBtn)
	{
		m_CloseBtn->SetPosition(UPoint(396, 22));
	}
	UMoneyText* pMoneytext = UDynamicCast(UMoneyText, GetChildByID(6));
	if (pMoneytext)
	{
		pMoneytext->SetMoneyTextAlign(UT_RIGHT);
		pMoneytext->SetMoney(0);
	}
	return TRUE;
}


void UPlayerTrade::OnDestroy()
{
	//SendTradeCancel();
	UDialog::OnDestroy();
}

void UPlayerTrade::OnTimer(float fDeltaTime)
{
	SetLockBtnSkin();

	if (IsVisible() && m_guid)
	{
		CPlayerLocal* pkLocalPayer = ObjectMgr->GetLocalPlayer();
		CPlayer* pkTennetPlayer = (CPlayer*)ObjectMgr->GetObject(m_guid);

		if (pkTennetPlayer == NULL)
		{
			ClientSystemNotify( _TRAN("距离太远,无法交易") );
			SendTradeCancel();
		}

		if (pkLocalPayer && pkTennetPlayer)
		{
			NiPoint3 PlayerPos = pkLocalPayer->GetLocalPlayerPos();
			NiPoint3 TargetPos = pkTennetPlayer->GetPosition();
			float Distance = (TargetPos - PlayerPos).Length();
		// 如果大于10M. 取消交易.
			if (Distance > 10.0f)
			{
				ClientSystemNotify( _TRAN("距离太远,无法交易"));
				SendTradeCancel();
			}
		}
	}
	//不允许输入
	UDialog::OnTimer(fDeltaTime);
	
}
void UPlayerTrade::SetLockBtnSkin()
{
	UBitmapButton* pOKBtn = (UBitmapButton*)GetChildByID(4);
	if (!m_Lock)
	{
		pOKBtn->SetBitmap("Public\\Lock.skin");
		m_LocalGold->SetActive(TRUE);
		m_LocalCopper->SetActive(TRUE);
		m_LocalSilver->SetActive(TRUE);
		pOKBtn->SetActive(TRUE);
	}else
	{
		pOKBtn->SetBitmap("Public\\OK.skin");
		if (m_TelnetLock)
		{
			pOKBtn->SetActive(TRUE);
		}else
		{
			pOKBtn->SetActive(FALSE);
		}
		m_LocalGold->SetActive(FALSE);
		m_LocalCopper->SetActive(FALSE);
		m_LocalSilver->SetActive(FALSE);
	}
}
void UPlayerTrade::OnClose()
{
	SendTradeCancel();
}

void UPlayerTrade::SetIconByPos(ui8 pos, ui32 ditemId, UItemSystem::ItemType type,int num, ItemExtraData& stItemExtraData, BOOL localPlayer )
{
	ItemExtraData temp;
	if (localPlayer)
	{
		//m_LocalItemContainer->SetItemType(type);
		CPlayer* pLocalPlayer = ObjectMgr->GetLocalPlayer();
		CPlayerTradeContainer* pContainer = NULL;
		CItemSlot* pSolt = NULL;
		if (pLocalPlayer)
		{
			pContainer = (CPlayerTradeContainer*)pLocalPlayer->GetContainer(ICT_PLAYERTRADE);
			if(pContainer)
				pSolt = (CItemSlot*)pContainer->GetSlot(pos);
		}
		if (pSolt)
		{
			SYItem* pkItem = (SYItem*)ObjectMgr->GetObject(pSolt->GetGUID());
			temp.jinglian_level = pkItem->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
			for( int i = 0; i < 6; ++i )
				temp.enchants[i] =  pkItem->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + i * 3 );
		}
		m_LocalPlayerContent->SetIconByPos(pos, ditemId, temp, type, num);
		//m_LocalItemContainer->SetItemByPos(pos, ditemId, num, temp);
		//m_LocalItemContainer->SetDragType(UItemSystem::ICT_PLAYER_TRADE);
	}else
	{
		m_TennetPlayerContent->SetIconByPos(pos, ditemId, stItemExtraData, type, num);
		//m_TelnetItemContainer->SetItemType(type);
		//m_TelnetItemContainer->SetItemByPos(pos, ditemId,num,stItemExtraData);
		//m_TelnetItemContainer->SetDragType(UItemSystem::ICT_PLAYER_TRADE_TENNET);
	}
}

int UPlayerTrade::GetTradeNullPos()
{
	int pos = -1;
	if (!m_Lock)
	{
		pos = m_LocalPlayerContent->FindNullPos();
		//UPoint pLocalGridCount = m_LocalItemContainer->GetGridCount();
		//for (int i =0; i <pLocalGridCount.y; i++)
		//{
		//	for(int j=0; j < pLocalGridCount.x; j++)
		//	{
		//		UItemData* pData = NULL;
		//		pData = (UItemData*)m_LocalItemContainer->GetItemData(i,j);
		//		if ((pData && pData->pItemID == 0) || pData == NULL)
		//		{
		//			pos = pLocalGridCount.x * i + j;
		//			return pos;
		//		}
		//	}
		//}
	}
	return pos;
}

void UPlayerTrade::RefurbishLocalPag()
{
	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if (localPlayer)
	{
		CPlayerTradeContainer* pTradeContainer = (CPlayerTradeContainer*)localPlayer->GetContainer(ICT_PLAYERTRADE);
		if (pTradeContainer)
		{
			//for (int i = 0; i < m_LocalItemContainer->GetGridCount().x * m_LocalItemContainer->GetGridCount().y;i++)
			for(int i = 0 ; i < 8 ; i++)
			{
				CItemSlot* pSolt = (CItemSlot*)pTradeContainer->GetSlot(i);
				if (pSolt)
				{
					if (pSolt->GetCode() != 0)
					{
						SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_PLAYER_TRADE,i,pSolt->GetCode(),pSolt->GetNum(),UItemSystem::ITEM_DATA_FLAG);
					}else
					{
						SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_PLAYER_TRADE,i,0,0,UItemSystem::ITEM_DATA_FLAG);
					}
				}
			}
		}
	}
}
ui32 UPlayerTrade::GetItemIdByPos(int pos)
{
	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if (localPlayer)
	{
		CPlayerTradeContainer* pPlayerTradeContainer = (CPlayerTradeContainer*)localPlayer->GetContainer(ICT_PLAYERTRADE);

		if (pPlayerTradeContainer)
		{
			CItemSlot* pSolt = (CItemSlot*)pPlayerTradeContainer->GetSlot(pos);
			if (pSolt && pSolt->GetCode() != 0)
			{
				return pSolt->GetItemProperty()->ItemId ;
			}else
			{
				return 0 ;
			}
		}		
	}

	return 0 ;
}
int  UPlayerTrade::GetNumByPos(int pos)
{
	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	if (localPlayer)
	{
		CPlayerTradeContainer* pPlayerTradeContainer = (CPlayerTradeContainer*)localPlayer->GetContainer(ICT_PLAYERTRADE);

		if (pPlayerTradeContainer)
		{
			CItemSlot* pSolt = (CItemSlot*)pPlayerTradeContainer->GetSlot(pos);
			if (pSolt)
			{
				return pSolt->GetNum(); ;
			}else
			{
				return 0 ;
			}
		}		
	}

	return 0 ;
}
void UPlayerTrade::SetTelnetItemContainerLock()
{
	if (m_TennetPlayerContent)
	{
		m_TennetPlayerContent->SetLock(true);
		m_TelnetLock = TRUE;
		ULOG( _TRAN("对方锁定交易！") );
	}
}

void UPlayerTrade::SetLockState(bool bLock)
{
	m_Lock = bLock?TRUE:FALSE;

	if (m_LocalPlayerContent)
	{
		m_LocalPlayerContent->SetLock(bLock);
		//m_LocalItemContainer->SetLockDrag(bLock);
	}
}

void UPlayerTrade::ClearAll()
{
	//unsigned int uiCount = m_LocalItemContainer->GetGridCount().x * m_LocalItemContainer->GetGridCount().y;
	ItemExtraData temp;
	for(unsigned int ui = 0; ui < 8;  ui++)
	{
		m_LocalPlayerContent->SetIconByPos(ui, 0, temp, UItemSystem::ITEM_DATA_FLAG);
		m_TennetPlayerContent->SetIconByPos(ui, 0, temp, UItemSystem::ITEM_DATA_FLAG);
	}
	m_TennetPlayerName->SetText("");   //名字
	//本地玩家(自己)
	m_LocalPlayerName->SetText("");
	m_LocalGold->SetText("0");
	m_LocalSilver->SetText("0");
	m_LocalCopper->SetText("0");

	m_TelnetLock = FALSE;
	SetLockState(false);
	m_TennetPlayerContent->SetLock(false);
	m_guid = 0;
	UMoneyText* pMoneytext = UDynamicCast(UMoneyText, GetChildByID(6));
	if (pMoneytext)
	{
		pMoneytext->SetMoneyTextAlign(UT_RIGHT);
		pMoneytext->SetMoney(0);
	}
}

void UPlayerTrade::SendTradeOK()
{
	if (m_Lock)
	{
		if (m_TelnetLock)
		{
			PacketBuilder->SendTradeAcceptMsg();
		}else
		{
			EffectMgr->AddEffeText(NiPoint3(300.0f,300.0f,0.0f), _TRAN("对方未锁定交易") );
		}
		
	}else
	{
		SendLocalMoney();
		SetLockState(true);
		PacketBuilder->SendTradeLockMsg();
	}
	
}
void UPlayerTrade::ReleaseUITrade()
{
	UPackage * pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
	UPlayerTrade * pPlayerTrade = INGAMEGETFRAME(UPlayerTrade,FRAME_IG_PLAYERTRADEDLG);
	CPlayer* pLocal = ObjectMgr->GetLocalPlayer();
	if (pLocal == NULL || pPackage == NULL || pPlayerTrade == NULL)
	{
		return;
	}
	CPlayerTradeContainer* pTradeContainer = (CPlayerTradeContainer*)pLocal->GetContainer(ICT_PLAYERTRADE);
	if (pTradeContainer)
	{
		ItemMgr->TradeConToBagCon();
		pPlayerTrade->ClearAll();	
		pTradeContainer->ClearAll();
		pPlayerTrade->SetVisible(FALSE);

	}

	SetLockState(false);
	SetLockBtnSkin();
	m_guid = 0;
}

void UPlayerTrade::SendTradeCancel()
{
	if (PacketBuilder->SendTradeCancelMsg())
	{
		CPlayerLocal::SetCUState(cus_Normal);
		ULOG( _TRAN("取消交易(cus_Normal)") );
	}
}
void UPlayerTrade::ClickLocalTradeList()
{
	if (!m_Lock)
	{
	/*	UPoint uPos = m_LocalItemContainer->GetSelGrid();
		UPoint uCount = m_LocalItemContainer->GetGridCount();
		WORD pos = uPos.y * uCount.x + uPos.x;
		SYState()->UI->GetUItemSystem()->PlayerTradeRelease((ui8)pos);*/
	}else
	{
		UMessageBox::MsgBox_s( _TRAN("交易已经锁定!") );
	}
	return;
}
void UPlayerTrade::ReleaseTradeItem(int pos)
{
	if (!m_Lock)
	{
		SYState()->UI->GetUItemSystem()->PlayerTradeRelease((ui8)pos);
	}else
	{
		UMessageBox::MsgBox_s( _TRAN("交易已经锁定!") );
	}
}
#include "UIMoneyText.h"
BOOL UPlayerTrade::ShowTennetMoney(ui32 num) //显示远程玩家的金钱数
{
	if (num <= 0)
		num = 0 ;

	UMoneyText* pMoneytext = UDynamicCast(UMoneyText, GetChildByID(6));
	if (pMoneytext)
	{
		pMoneytext->SetMoney(num);
	}
	return TRUE;
}
void UPlayerTrade::SendLocalMoney()  //发送本地输入的金钱数
{
	char gold[100];
	char silver[100];
	char copper[100];

	ui32 uiMoney = 0;
	int G_len = m_LocalGold->GetText(gold,100);
	int S_len = m_LocalSilver->GetText(silver,100);
	int C_len = m_LocalCopper->GetText(copper,100);

	if (G_len > 0)
	{
		uiMoney = uiMoney + atoi(gold) * 10000;
	}
	if (S_len > 0)
	{
		uiMoney = uiMoney + atoi(silver) * 100;
	}
	if (C_len)
	{
		uiMoney = uiMoney + atoi(copper);
	}

	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	
//判断是否有那么多钱
	if (uiMoney > localPlayer->GetMoney())
	{
		uiMoney = localPlayer->GetMoney();
		SetLockMoney(uiMoney);
	}
	PacketBuilder->SendTradeSetGoldMsg(uiMoney);	
}
void UPlayerTrade::SetTradePlayerName(ui64 guid)
{
	CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
	CPlayer* tennetPlayer = (CPlayer*)ObjectMgr->GetObject(guid);
	
	m_guid = guid;
	if (localPlayer && tennetPlayer && m_LocalPlayerName && m_TennetPlayerName)
	{
		m_LocalPlayerName->SetText(localPlayer->GetName());
		m_TennetPlayerName->SetText(tennetPlayer->GetName());
	}

	CPlayerLocal::SetCUState(cus_Trade);
}
void UPlayerTrade::SetLockMoney(unsigned int num)
{
	char buf[100];
	NiSprintf(buf,100,"%d",num);
	if (m_Lock)
	{
		UMessageBox::MsgBox_s( _TRAN("交易已经锁定!") );
	}else
	{
		char gold[100];
		char silver[100];
		char copper[100];

		int I_Gold = num /10000 ;
		int I_Silver = (num % 10000) / 100 ;
		int I_Copper = (num % 10000) % 100 ;


		NiSprintf(gold,100,"%d",I_Gold);
		NiSprintf(silver,100,"%d",I_Silver);
		NiSprintf(copper,100,"%d",I_Copper);

		if (m_LocalGold && m_LocalSilver && m_LocalCopper)
		{
			m_LocalGold->SetText(gold);
			m_LocalGold->SetTextAlignment(UT_RIGHT);
			m_LocalGold->SetActive(FALSE);
			m_LocalGold->ResetTextOffset();

			m_LocalSilver->SetText(silver);
			m_LocalSilver->SetTextAlignment(UT_RIGHT);
			m_LocalGold->SetActive(FALSE);
			m_LocalSilver->ResetTextOffset();

			m_LocalCopper->SetText(copper);
			m_LocalCopper->SetTextAlignment(UT_RIGHT);
			m_LocalGold->SetActive(FALSE);
			m_LocalCopper->ResetTextOffset();
		}
	}
}
BOOL UPlayerTrade::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}

void UPlayerTrade::SetVisible(BOOL value)
{
	SetLockState(false);
	if (value)
	{
		m_LocalGold->SetActive(TRUE);
		m_LocalSilver->SetActive(TRUE);
		m_LocalCopper->SetActive(TRUE);
		m_TelnetLock = FALSE;
		SetLockBtnSkin();
	}else
	{
		ClearAll();
		SetFocusControl(sm_System->GetCurFrame());
	}

	return UDialog::SetVisible(value);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UTelnetItemContainerEx, UItemContainerEx);
void UTelnetItemContainerEx::StaticInit()
{

}
void UTelnetItemContainerEx::SetLockAllItem(bool bLock)
{
	UINT x = m_GridCount.x;
	UINT y = m_GridCount.y;

	for (UINT i = 0; i < y; i++)
	{
		for(UINT j = 0; j < x; j++)
		{
			if (HasItem(i,j))
			{
				UItemData* pData = (UItemData*)GetItemData(i,j);
				if (pData)
				{
					pData->bLock = bLock;
				}
			}
		}
	}
}