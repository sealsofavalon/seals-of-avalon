#pragma  once
#include "UInc.h"
#include "UItemContainerEx.h"
#include "UDynamicIconButtonEx.h"

class UPlayerTrade_TradeItem : public UControl
{
	class TradeItemSlot : public UDynamicIconButtonEx
	{
		UDEC_CLASS(UPlayerTrade_TradeItem::TradeItemSlot);
		TradeItemSlot();
		~TradeItemSlot();
		virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
		virtual void OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag);
		virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	protected:
		virtual void DrawPushButton(const UPoint& offset, const URect& ClipRect);
	};
	friend class TradeItemSlot;
	class TradeItemSlotItem : public UActionItem
	{
	public:
		virtual void Use();
		virtual void UseLeft();
	};

	UDEC_CLASS(UPlayerTrade_TradeItem);
public:
	UPlayerTrade_TradeItem();
	~UPlayerTrade_TradeItem();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
};

class UPlayerTradeContent : public UControl
{
	UDEC_CLASS(UPlayerTradeContent);
public:
	UPlayerTradeContent();
	~UPlayerTradeContent();

	inline void SetLock(bool bLock){m_bLock = bLock;};
	inline bool GetLock(){return m_bLock;};

	void SetIconByPos(ui8 pos, ui32 dwitemid, ItemExtraData& stItemExtraData, ui16 type, int num = 0);
	void SetType(ui32 type);
	INT FindNullPos();

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);

	bool m_bLock;
	UTexturePtr m_LockTex;
};


//显示远程玩家的信息
class UTelnetItemContainerEx : public UItemContainerEx
{
	UDEC_CLASS(UTelnetItemContainerEx);
public:
	void SetLockAllItem(bool bLock);
};

class UPlayerTrade : public UDialog
{
	UDEC_CLASS(UPlayerTrade);
	UDEC_MESSAGEMAP();
public:
	UPlayerTrade();
	~UPlayerTrade();
	void SetIconByPos(ui8 pos, ui32 ditemId,UItemSystem::ItemType type,int num, ItemExtraData& stItemExtraData, BOOL localPlayer = TRUE);// 设置UI 相关

	ui32 GetItemIdByPos(int pos); // 根据位置获得ID ,.
	int  GetNumByPos(int pos);    // 根据位置获得数量
	void RefurbishLocalPag();     // 刷新交易框

	int GetTradeNullPos();        // 获得可以添加交易物品的位置.

	BOOL ShowTennetMoney(ui32 num);  //显示远程玩家的金钱数
	void SendLocalMoney();  //发送本地输入的金钱数

	void SetLockMoney(unsigned int num);  //设置本地交易的钱

	void ReleaseUITrade();
	//按钮响应
	void SendTradeOK();     // 
	void SendTradeCancel();
	//点击本地交易列表相应. 
	void ClickLocalTradeList();

	void ReleaseTradeItem(int pos);

	void ClearAll();

	void SetLockState(bool bLock);
	BOOL GetIsLock(){return m_Lock;}
	
	void SetTelnetItemContainerLock();

	// 下面是设置UI相关.
	void SetTradePlayerName(ui64 guid);
public:
	virtual void SetVisible(BOOL value);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);

	virtual void OnClose();
	virtual BOOL OnEscape();
	
	void SetLockBtnSkin();
private:
	//远程玩家(交易对象)
	UStaticText* m_TennetPlayerName;   //名字
	//本地玩家(自己)
	UStaticText* m_LocalPlayerName;
	UEdit* m_LocalGold;
	UEdit* m_LocalSilver;
	UEdit* m_LocalCopper;

	BOOL m_Lock;   // lock?
	BOOL m_TelnetLock ;
	ui64 m_guid;//

	UPlayerTradeContent* m_LocalPlayerContent;
	UPlayerTradeContent* m_TennetPlayerContent;
};