#include "StdAfx.h"
#include "../Network/PacketBuilder.h"
#include "UIRightMouseList.h"
#include "UIShowPlayer.h"
#include "Character.h"
#include "ObjectManager.h"
#include "UChat.h"
#include "UPickItemSet.h"
#include "UIIndex.h"
#include "UIGamePlay.h"
#include "UFun.h"
#include "SocialitySystem.h"
#include "TeacherSystem.h"
#include "UIPlayerEquipment.h"
#include "SystemTips .h"
#include "UInGameBar.h"
#include "SystemSetup.h"


struct stListcontent
{
	std::string str;
	UINT Id;
};
const stListcontent Content[]={
	{"取消",MESS_CANCLE},
	{"离开队伍",MESS_LEAVETEAM},
	{"离开团队",MESS_LEAVETEAMGROUP},
	{"队长更改",MESS_LEADERTRANS},
	{"团长更改",MESS_LEADERTRANSGROUP},
	{"跟随",MESS_FOLLOWPLAYERITEM},
	{"悄悄话",MESS_PRIVATETALK},
	{"移出队伍",MESS_REMOVEMEMFORMTEAM},
	{"移出团队",MESS_REMOVEMEMFORMTEAMGROUP},
	{"邀请组队",MESS_TEAMINVATE},
	{"邀请组团",MESS_TEAMINVATEGROUP},
	{"决斗",MESS_FIGHT},
	{"观察",MESS_VIEW},
	{"加为好友",MESS_FRIENDADD},
	{"交易",MESS_TRADE},
	{"删除好友",MESS_DELFRIEND},
	{"邀请骑乘",MESS_MOUNT},
	{"邀请部族",MESS_GUILDINVITE},
	{"提升阶位",MESS_GUILDPROMOTEMEM},
	{"降低阶位",MESS_GUILDDEMOTEMEM},
	{"逐出部族",MESS_GUILDREMOVEMEM},
	{"移交会长",MESS_GUILDLEADERTRANS},
	{"组队分配",MESS_TEAMFENPEI},
	{"团队分配",MESS_TEAMFENPEIGROUP},
	{"复制姓名",MESS_COPYPLAYERNAME},
	{"取消骑乘",MESS_MOUNTCANCEL},
	{"招募徒弟",MESS_TEACHER},
	{"取消标记",MESS_CANCELMASK},
	{"监视目标",MESS_LOCKTARGET},
	{"加入黑名单", MESS_ADDBLACKLIST},
	{"取消黑名单", MESS_DELBLACKLIST},
	{"宠物改名", MESS_PETCHANGENAME},
	{"横向排列", MESS_TRANSFORMTYPE1},
	{"趣味排列", MESS_TRANSFORMTYPE2},
	{"纵向排列", MESS_TRANSFORMTYPE3},
	{"锁定", MESS_ACTIONBARLOCK},
	{"解锁",MESS_ACTIONBARUNLOCK},
	{"添加动作条",MESS_ACTIONBARADD},
	{"删除动作条",MESS_ACTIONBARDEL},
	{"组队申请",MESS_APPLYGROUP},
};

UIMP_CLASS(UIRightMouseList,UControl);
UBEGIN_MESSAGE_MAP(UIRightMouseList,UCmdTarget)
UON_BMLB_CLICKED(0,&UIRightMouseList::OnReqestMsg)
UEND_MESSAGE_MAP()
void UIRightMouseList::StaticInit()
{

}
UIRightMouseList::UIRightMouseList(void)
{
	m_BitMapList = NULL; 
	m_IsUpdateChild = FALSE;
	m_Skin = NULL;
}

UIRightMouseList::~UIRightMouseList(void)
{
	m_BitMapList = NULL;
	m_Skin = NULL;
}

BOOL UIRightMouseList::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_BitMapList == NULL)
	{
		m_BitMapList = (UBitMapListBox * )GetChildByID(0);
		if (m_BitMapList)
		{
		}
		else
		{
			return FALSE;
		}
	}
	if (m_Skin == NULL)
	{
		m_Skin = sm_System->LoadSkin("UIPlayerShow//CharMask.skin");
		if (!m_Skin)
		{
			return FALSE;
		}
	}
	return TRUE;
}
void UIRightMouseList::OnDestroy()
{
	UControl::OnDestroy();
}

void UIRightMouseList::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	ClosePop();
	UControl::OnMouseDown(point,nRepCnt,uFlags);
}
BOOL UIRightMouseList::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	ClosePop();
	return UControl::OnKeyDown(nKeyCode, nRepCnt, nFlags);
}

void UIRightMouseList::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	ClosePop();
	UControl::OnRightMouseDown(position, nRepCnt, flags);
}
void UIRightMouseList::OnReqestMsg()
{
	char strbuf[1024];
	m_BitMapList->GetSelectedText(strbuf,1024);
	void* pDate = NULL;
	if(m_BitMapList->GetCurSel() >= 0 && m_BitMapList->GetCurSel() < m_BitMapList->GetItemCount())
		pDate = m_BitMapList->GetItemData(m_BitMapList->GetCurSel());
	switch(FindMess(strbuf, pDate))
	{
	case MESS_CANCLE:
		ClosePop();
		return;
	case MESS_LEAVETEAM:
	case MESS_LEAVETEAMGROUP:
		if (SocialitySys->GetTeamSysPtr())
		{
			SocialitySys->GetTeamSysPtr()->SendLeaveTeam();
		}

		break;
	case MESS_LEADERTRANS:
	case MESS_LEADERTRANSGROUP:
		if (SocialitySys->GetTeamSysPtr())
		{
			SocialitySys->GetTeamSysPtr()->SendChangeTeamLeader(m_GUID);
		}
		break;
	case MESS_REMOVEMEMFORMTEAM:
	case MESS_REMOVEMEMFORMTEAMGROUP:
		if (SocialitySys->GetTeamSysPtr())
		{
			SocialitySys->GetTeamSysPtr()->SendRemoveMember(m_GUID,m_CName.c_str());
		}
		break;
	case MESS_PRIVATETALK:
		{
			if (ChatSystem)
			{
				ChatSystem->SetCurrentChannel(CHAT_MSG_WHISPER);
				ChatSystem->SetPTalkPerson(m_CName.c_str(),m_GUID);
				ChatSystem->ShowInputBox();
			}
		}
		break;
	case MESS_TEAMINVATE:
	case MESS_TEAMINVATEGROUP:
		if (SocialitySys->GetTeamSysPtr())
		{
			SocialitySys->GetTeamSysPtr()->SendAddMember(m_CName.c_str());
		}
		break;
	case MESS_FRIENDADD:
		{
			PacketBuilder->SendFriendAddMsg(m_CName, m_CName);
		}
		break;
	case MESS_TRADE:
		if (m_GUID)
		{
			PacketBuilder->SendTradeInitMsg(m_GUID);	
		}
		break;
	case MESS_FOLLOWPLAYERITEM:
		{
			ObjectMgr->GetLocalPlayer()->FollowPlayer(m_GUID);
		}
		break;
	case MESS_DELFRIEND:
		PacketBuilder->SendFriendDelMsg(m_GUID);
		break;
	case MESS_MOUNT:
		PacketBuilder->SendMountInvite(m_GUID);
		break;
	case MESS_GUILDINVITE:
		if(SocialitySys && SocialitySys->GetGuildSysPtr())
			SocialitySys->GetGuildSysPtr()->SendMsgInviteMember(m_CName.c_str());
		break;
	case MESS_GUILDPROMOTEMEM:
		{
			ui32 RankId = 0;
			if(SocialitySys->GetGuildSysPtr()->GetRankIdByGuid(m_GUID, RankId))
			{
				RankId = ((RankId - 1) < 1)?1:(RankId - 1);
				SocialitySys->GetGuildSysPtr()->SendMsgChangeMemRank(m_CName.c_str(), RankId);
			}
		}
		break;
	case MESS_GUILDDEMOTEMEM:
		{
			ui32 RankId = 0;
			if(SocialitySys->GetGuildSysPtr()->GetRankIdByGuid(m_GUID, RankId))
			{
				RankId = ((RankId + 1) > 3)?3:(RankId + 1);
				SocialitySys->GetGuildSysPtr()->SendMsgChangeMemRank(m_CName.c_str(), RankId);
			}
		}
		break;
	case MESS_GUILDREMOVEMEM:
		{
			if (SocialitySys)
			{
				SocialitySys->GetGuildSysPtr()->RemoveSelect();
			}
		}
		break;
	case MESS_GUILDLEADERTRANS:
		{
			PacketBuilder->SendSetLeader(m_CName.c_str());
		}
		break;
	case MESS_TEAMFENPEI:
	case MESS_TEAMFENPEIGROUP:
		{
			//组队分配
			if (TEAMPICKITEMSET)
			{
				TEAMPICKITEMSET->SetVisible(TRUE);
			}
				
		}
		break;
	case MESS_COPYPLAYERNAME:
		{
			if (m_CName.length())
			{
				CopyString(m_CName.c_str());
			}
		}
		break;
	case MESS_MOUNTCANCEL:
		{
			PacketBuilder->SendMountDisband();
		}
		break;
	case MESS_TEACHER:
		{
			TeacherSystemMgr->SendZhaoTuDi(m_GUID,m_CName);
		}
		break;
	case MESS_VIEW:
		{
			CCharacter* Char = (CCharacter*)ObjectMgr->GetObject(m_GUID);
			UViewPlayerEquipment* pViewPlayerEqu = INGAMEGETFRAME(UViewPlayerEquipment, FRAME_IG_VIEWPLAYEREQUIPDLG);
			if (pViewPlayerEqu)
				pViewPlayerEqu->SetViewPlayer(Char);
		}
		break;
	case MESS_CANCELMASK:
		{
			SocialitySys->GetTeamSysPtr()->SendChangeMemberIcon(m_GUID, TATGET_ICON_MAX);
		}
		break;
	case MESS_LOCKTARGET:
		{ 
			std::vector<std::string> text;
			text.push_back("frame");
			OnCommandCallPlug(text);
			g_WatchTargetMgr->CreatWatchTarget(m_GUID);
		}
		break;
	case MESS_ADDBLACKLIST:
		{
			SocialitySys->GetBlackListSysPtr()->SendAddIgnoreList(m_CName);
		}
		break;
	case MESS_DELBLACKLIST:
		{
			SocialitySys->GetBlackListSysPtr()->SendDelIgnoreList(m_GUID);
		}
		break;
	case MESS_PETCHANGENAME:
		{
			UIAddMemberEdit* pAME = INGAMEGETFRAME(UIAddMemberEdit, FRAME_IG_ADDMEMBERDLG);
			if (pAME)
			{
				pAME->OnAddMember(6);
			}
		}
		break;
	case MESS_TRANSFORMTYPE1:
		{
			UPlug_AcutionBar* pAcution = INGAMEGETFRAME(UPlug_AcutionBar, m_GUID);
			if (pAcution)
			{
				pAcution->Transform(1);
			}
		}
		break;
	case MESS_TRANSFORMTYPE2:
		{
			UPlug_AcutionBar* pAcution = INGAMEGETFRAME(UPlug_AcutionBar, m_GUID);
			if (pAcution)
			{
				pAcution->Transform(2);
			}
		}
		break;
	case MESS_TRANSFORMTYPE3:
		{
			UPlug_AcutionBar* pAcution = INGAMEGETFRAME(UPlug_AcutionBar, m_GUID);
			if (pAcution)
			{
				pAcution->Transform(3);
			}
		}
		break;
	case MESS_ACTIONBARLOCK:
		{
			UPlug_AcutionBar* pAcution = INGAMEGETFRAME(UPlug_AcutionBar, m_GUID);
			if (pAcution)
			{
				pAcution->Lock();
			}
		}
		break;
	case MESS_ACTIONBARUNLOCK:
		{
			UPlug_AcutionBar* pAcution = INGAMEGETFRAME(UPlug_AcutionBar, m_GUID);
			if (pAcution)
			{
				pAcution->UnLock();
			}
		}
		break;
	case MESS_ACTIONBARADD:
		{
			for (int i = 0 ; i < 6 ; ++i)
			{
				UPlug_AcutionBar* pAction = INGAMEGETFRAME(UPlug_AcutionBar, FRAME_IG_ACTIONSKILLBAREX2 + i);
				if (pAction && !pAction->IsVisible())
				{
					char buf[256];
					sprintf(buf, "AddActionBar%d", i + 2);
					SystemSetup->SetField(buf, true);
					SystemSetup->SetupSetting();
					SystemSetup->SaveFile();
					break;
				}
			}
		}
		break;
	case MESS_ACTIONBARDEL:
		{
			UPlug_AcutionBar* pAction = INGAMEGETFRAME(UPlug_AcutionBar, m_GUID);
			if (pAction && pAction->IsVisible())
			{
				char buf[256];
				sprintf(buf, "AddActionBar%d",m_GUID - FRAME_IG_ACTIONSKILLBAREX2 + 2);
				SystemSetup->SetField(buf, false);
				SystemSetup->SetupSetting();
				SystemSetup->SaveFile();
			}
		}
		break;
	case MESS_APPLYGROUP:
		{
			SocialitySys->GetApplyGroupSysPtr()->SendGroupApplyForJoining(m_GUID);
		}
		break;
	};
	ClosePop();
}
ListMessage UIRightMouseList::FindMess(const char * buf, const void* pDate)
{
	int max = sizeof(Content) / sizeof(stListcontent);
	for (int i = 0 ; i < max ; i++)
	{
		if (_stricmp(_TRAN(Content[i].str.c_str()), buf) == 0)
		{
			return (ListMessage)Content[i].Id;
		}
	}
	if (strstr(buf,_TRAN("标记") ) && pDate)
	{
		UINT* pUINT = (UINT*)pDate;
		if(pUINT)
			SocialitySys->GetTeamSysPtr()->SendChangeMemberIcon(m_GUID, *pUINT);
	}
	ui32 rankId = 0;
	if (SocialitySys->GetGuildSysPtr()->GetRankIdByRankName(buf, rankId))
	{
		SocialitySys->GetGuildSysPtr()->SendMsgChangeMemRank(m_CName.c_str(), rankId);
	}
	return MESS_CANCLE;
}

void UIRightMouseList::AddItem(int type)
{
	int max = sizeof(Content) / sizeof(stListcontent);
	for (int i = 0 ; i < max ; i++)
	{
		if (Content[i].Id == type)
		{
			return m_BitMapList->AddItem(_TRAN(Content[i].str.c_str()));
		}
	}
}

void UIRightMouseList::ChangeItemState(const char *str,int istate)
{
	if (m_BitMapList == NULL)
		return;
	int index = m_BitMapList->FindItem(str);
	if (index != -1)
		m_BitMapList->SetItemEnable(index,(istate == ItemEnable)?true:false);
}
void UIRightMouseList::Popup(UControl * toCtrl , int type , const char * CreateName , ui64 guid)
{
	m_CName = CreateName;
	m_GUID = guid;
	if (toCtrl && m_BitMapList)
	{
		GetParent()->MoveChildTo(this,NULL);
		//TODO
		UPoint pos = toCtrl->GetWindowPos();
		int height = toCtrl->GetHeight();
		pos.y += height;
		m_BitMapList->SetPosition(pos);
		m_BitMapList->CaptureControl();
		AddList(type);
	}
}
void UIRightMouseList::Popup(const UPoint & toPos , int type , const char * CreateName , ui64 guid)
{
	SetVisible(TRUE);
	m_CName = CreateName;
	m_GUID = guid;
	if (m_BitMapList)
	{
		m_BitMapList->SetCurSel(-1);
		GetParent()->MoveChildTo(this,NULL);
		//TODO
		m_BitMapList->SetPosition(toPos);
		m_BitMapList->CaptureControl();
		AddList(type);
	}
}
void UIRightMouseList::ClosePop()
{
	m_BitMapList->Clear();
	m_BitMapList->ReleaseCapture();
	SetVisible(FALSE);
}
void UIRightMouseList::AddList(int type)
{
	if (m_BitMapList == NULL)
	{
		return;
	}
	m_BitMapList->Clear();
	CPlayer*  _player =( CPlayer *)ObjectMgr->GetLocalPlayer();
	CCharacter* Char = (CCharacter*)ObjectMgr->GetObject(m_GUID);

	BOOL pkLocalCanTORP = _player->GetCanTeacherOrP();
	BOOL pkCanTORP = FALSE ;
	if (Char && Char->GetGameObjectType() == GOT_PLAYER)
	{
		CPlayer* pkPlayer = (CPlayer*)Char ;
		if (pkPlayer)
		{
			pkCanTORP = pkPlayer->GetCanTeacherOrP();
		}
	}
	
	switch(type)
	{
	case BITMAPLIST_LOCALPLAYER:
		{
			BOOL BClose = FALSE;
			if (_player->IsMountPlayer() || _player->IsMountByPlayer())
			{
				AddItem(MESS_MOUNTCANCEL);
				BClose = TRUE;
			}
			if(TeamShow->GetTeamLeaderGuid())
			{
				
				if (SocialitySys && SocialitySys->GetTeamSysPtr() && SocialitySys->GetTeamSysPtr()->IsTuanDui())
				{
					if (_player && TeamShow->IsTeamLeader(_player->GetGUID()))
					{	
						AddItem(MESS_TEAMFENPEIGROUP);
					}
					AddItem(MESS_LEAVETEAMGROUP);
				}else
				{
					if (_player && TeamShow->IsTeamLeader(_player->GetGUID()))
					{	
						AddItem(MESS_TEAMFENPEI);
					}
					AddItem(MESS_LEAVETEAM);
				}
				AddItem(MESS_CANCLE);
			}
			else
			{
				if (!BClose)
				{
					ClosePop();
				}
			}
		}
		break;
	case BITMAPLIST_TEAMMEM:
		{
			if (_player)
			{			
				if (Char)
					AddItem(MESS_VIEW);
				AddItem(MESS_FOLLOWPLAYERITEM);
				AddItem(MESS_FRIENDADD);
				AddItem(MESS_PRIVATETALK);
				if (TeamShow->IsTeamLeader(_player->GetGUID()))
				{
					if (SocialitySys->GetTeamSysPtr()->IsTuanDui())
					{
						AddItem(MESS_REMOVEMEMFORMTEAMGROUP);
						AddItem(MESS_LEADERTRANSGROUP);
					}else
					{
						AddItem(MESS_LEADERTRANS);
						AddItem(MESS_REMOVEMEMFORMTEAM);
					}
				}
				ui32 rankid = SocialitySys->GetGuildSysPtr()->GetLocalRankId();
				if (SocialitySys->GetGuildSysPtr()->GetRankRightVis(rankid, GR_RIGHT_INVITE))
				{
					AddItem(MESS_GUILDINVITE);
				}
				/*if (pkCanTORP && pkLocalCanTORP)
				{
					m_BitMapList->AddItem("招募徒弟");
				}*/
				AddItem(MESS_CANCLE);
			}
			else
			{
				ClosePop();
			}
		}
		break;
	case BITMAPLIST_TENNET:
		{
			AddItem(MESS_LOCKTARGET);
			if (!Char){
				if (!SocialitySys->GetTeamSysPtr()->InTeam(m_GUID))	{
					ClosePop();
					return;
				}
			}
			else if (Char->GetGameObjectType() == GOT_CREATURE)	{
				//ClosePop();
				return;
			}
			AddItem(MESS_PRIVATETALK);
			if(_player && _player->GetGUID() == m_GUID)	{
				ClosePop();
				return;
			}
			
			if (Char)
				AddItem(MESS_VIEW);
			AddItem(MESS_TRADE);
			if(!SocialitySys->GetTeamSysPtr()->InTeam(m_GUID))
			{
				if (SocialitySys->GetTeamSysPtr()->IsTuanDui())
				{
					AddItem(MESS_TEAMINVATEGROUP);
				}else
				{
					AddItem(MESS_TEAMINVATE);
				}
				AddItem(MESS_APPLYGROUP);
			}
			if( _player->GetUInt32Value(UNIT_FIELD_FORMDISPLAYID) && !_player->HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_SHAPE_CANTMOUNT))	
			{
				AddItem(MESS_MOUNT);
			}
			if (_player->IsMountPlayer() || _player->IsMountByPlayer())
			{
				AddItem(MESS_MOUNTCANCEL);
			}
			AddItem(MESS_FRIENDADD);

			ui32 rankid = SocialitySys->GetGuildSysPtr()->GetLocalRankId();
			if (SocialitySys->GetGuildSysPtr()->GetRankRightVis(rankid, GR_RIGHT_INVITE))
			{
				AddItem(MESS_GUILDINVITE);
			}

			/*if (pkCanTORP && pkLocalCanTORP)
			{
				m_BitMapList->AddItem("招募徒弟");
			}*/
			
			//m_BitMapList->AddItem("决斗");
			AddItem(MESS_FOLLOWPLAYERITEM);
			AddItem(MESS_COPYPLAYERNAME);	
			AddItem(MESS_CANCLE);
		}
		break;
	case BITMAPLIST_FRIEND:
		{
			AddItem(MESS_PRIVATETALK);
			ui32 rankid = SocialitySys->GetGuildSysPtr()->GetLocalRankId();
			if (SocialitySys->GetGuildSysPtr()->GetRankRightVis(rankid, GR_RIGHT_INVITE))
			{
				AddItem(MESS_GUILDINVITE);
			}
			if(!SocialitySys->GetTeamSysPtr()->InTeam(m_GUID))
			{
				if (SocialitySys->GetTeamSysPtr()->IsTuanDui())
				{
					AddItem(MESS_TEAMINVATEGROUP);
				}else
				{
					AddItem(MESS_TEAMINVATE);
				}
			}
			AddItem(MESS_COPYPLAYERNAME);
			AddItem(MESS_ADDBLACKLIST);
			AddItem(MESS_CANCLE);
		}
		break;
	case BITMAPLIST_CHATMSG:
		{
			if (_player && _player->GetName().Equals(m_CName.c_str()))
			{
				ClosePop();
				return;
			}

			/*if (pkCanTORP && pkLocalCanTORP)
			{
				m_BitMapList->AddItem("招募徒弟");
				}*/
			AddItem(MESS_PRIVATETALK);
			if (SocialitySys->GetTeamSysPtr()->IsTuanDui())
			{
				AddItem(MESS_TEAMINVATEGROUP);
			}else
			{
				AddItem(MESS_TEAMINVATE);
			}
			AddItem(MESS_FRIENDADD);
			ui32 rankid = SocialitySys->GetGuildSysPtr()->GetLocalRankId();
			if (SocialitySys->GetGuildSysPtr()->GetRankRightVis(rankid, GR_RIGHT_INVITE))
			{
				AddItem(MESS_GUILDINVITE);
			}
			AddItem(MESS_COPYPLAYERNAME);
			AddItem(MESS_ADDBLACKLIST);
			AddItem(MESS_CANCLE);
		}
		break;
	case BITMAPLIST_GUILD:
		{
			if(_player)
			{
				if (_player->GetName().Equals(m_CName.c_str()))
				{
					ClosePop();
					return;
				}
			}
			AddItem(MESS_PRIVATETALK);
			AddItem(MESS_FRIENDADD);

			ui32 rankid = SocialitySys->GetGuildSysPtr()->GetLocalRankId();
			if (SocialitySys->GetGuildSysPtr()->GetRankRightVis(rankid, GR_RIGHT_REMOVE))
			{
				AddItem(MESS_GUILDREMOVEMEM);
			}
			if(!SocialitySys->GetTeamSysPtr()->InTeam(m_GUID))
			{
				if (SocialitySys->GetTeamSysPtr()->IsTuanDui())
				{
					AddItem(MESS_TEAMINVATEGROUP);
				}else
				{
					AddItem(MESS_TEAMINVATE);
				}
			}
			if (rankid == 0)
			{
				m_BitMapList->AddLine(0xFFFFFFFF);
				m_BitMapList->AddItem( _TRAN("更改阶位：") , UT_LEFT, 0xFFFFFFFF, NULL, false);
				SocialitySys->GetGuildSysPtr()->SetRankNameToBitMapList(m_BitMapList);
			}
		}
		break;
	case BITMAPLIST_OLPLAYERQUERY:
		{
			if (_player && _player->GetName().Equals(m_CName.c_str()))
			{
				ClosePop();
				return;
			}
			AddItem(MESS_PRIVATETALK);
			if (SocialitySys->GetTeamSysPtr()->IsTuanDui())
			{
				AddItem(MESS_TEAMINVATEGROUP);
			}else
			{
				AddItem(MESS_TEAMINVATE);
			}
			AddItem(MESS_FRIENDADD);
			ui32 rankid = SocialitySys->GetGuildSysPtr()->GetLocalRankId();
			if (SocialitySys->GetGuildSysPtr()->GetRankRightVis(rankid, GR_RIGHT_INVITE))
			{
				AddItem(MESS_GUILDINVITE);
			}
			AddItem(MESS_COPYPLAYERNAME);
			AddItem(MESS_ADDBLACKLIST);
			AddItem(MESS_CANCLE);
		}
		break;
	case BITMAPLIST_TEAMGROUP:
		{
			if (_player && _player->GetName().Equals(m_CName.c_str()))
			{
				ClosePop();
				return;
			}
			if (SocialitySys->GetTeamSysPtr()->IsTeamLeader(_player->GetGUID()))
			{
				if (SocialitySys->GetTeamSysPtr()->IsTuanDui())
				{
					AddItem(MESS_LEADERTRANSGROUP);
					AddItem(MESS_REMOVEMEMFORMTEAMGROUP);
				}else
				{
					AddItem(MESS_LEADERTRANS);
					AddItem(MESS_REMOVEMEMFORMTEAM);
				}
				
			}
			if (SocialitySys->GetTeamSysPtr()->IsTuanDui())
			{
				AddItem(MESS_LEAVETEAMGROUP);
			}else
			{
				AddItem(MESS_LEAVETEAM);
			}
			AddItem(MESS_PRIVATETALK);
			AddItem(MESS_FRIENDADD);
			AddItem(MESS_CANCLE);
		}
		break;
	case BITMAPLIST_LEADERMASK:
		{
			UINT* uiInt = new UINT(0);
			m_BitMapList->AddItem( _TRAN("标记---"),m_Skin,0,(void*)uiInt);
			uiInt = new UINT(3);
			m_BitMapList->AddItem( _TRAN("标记---"),m_Skin,3,(void*)uiInt);
			uiInt = new UINT(2);
			m_BitMapList->AddItem( _TRAN("标记---"),m_Skin,2,(void*)uiInt);
			uiInt = new UINT(7);
			m_BitMapList->AddItem( _TRAN("标记---"),m_Skin,7,(void*)uiInt);
			uiInt = new UINT(1);
			m_BitMapList->AddItem( _TRAN("标记---"),m_Skin,1,(void*)uiInt);
			uiInt = new UINT(4);
			m_BitMapList->AddItem( _TRAN("标记---"),m_Skin,4,(void*)uiInt);
			uiInt = new UINT(5);
			m_BitMapList->AddItem( _TRAN("标记---"),m_Skin,5,(void*)uiInt);
			uiInt = new UINT(6);
			m_BitMapList->AddItem( _TRAN("标记---"),m_Skin,6,(void*)uiInt);
			AddItem(MESS_CANCELMASK);
		}
		break;
	case  BITMAPLIST_BLACKLIST:
		{
			AddItem(MESS_DELBLACKLIST);
			AddItem(MESS_COPYPLAYERNAME);
			AddItem(MESS_CANCLE);
		}
		break;
	case BITMAPLIST_PET:
		{
			AddItem(MESS_PETCHANGENAME);
			AddItem(MESS_CANCLE);
		}
		break;
	case BITMAPLIST_ACTIONBAR:
		{
			UPlug_AcutionBar* pAcution = INGAMEGETFRAME(UPlug_AcutionBar, m_GUID);
			if (pAcution)
			{
				m_BitMapList->AddTitle( _TRAN("排列方式：") , UT_LEFT, 0xFFFFFF00, NULL);
				AddItem(MESS_TRANSFORMTYPE1);
				AddItem(MESS_TRANSFORMTYPE2);
				AddItem(MESS_TRANSFORMTYPE3);
				m_BitMapList->AddTitle( _TRAN("拖动锁定：") , UT_LEFT, 0xFFFFFF00, NULL);
				if (pAcution->IsLock())
				{
					AddItem(MESS_ACTIONBARUNLOCK);
				}else
				{
					AddItem(MESS_ACTIONBARLOCK);
				}
				m_BitMapList->AddTitle( _TRAN("其它操作：") , UT_LEFT, 0xFFFFFF00, NULL);
				AddItem(MESS_ACTIONBARADD);
				AddItem(MESS_ACTIONBARDEL);
				AddItem(MESS_CANCLE);
			}
		}
		break;
	}
}