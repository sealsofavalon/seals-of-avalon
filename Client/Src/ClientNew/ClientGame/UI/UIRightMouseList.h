#ifndef UIRIGHTMOUSELIST_H
#define UIRIGHTMOUSELIST_H
#include "UInc.h"
enum 
{
	BITMAPLIST_LOCALPLAYER = 0,  //自己
	BITMAPLIST_TEAMMEM,          //队伍
	BITMAPLIST_TENNET,           //目标
	BITMAPLIST_FRIEND,           //好友
	BITMAPLIST_CHATMSG,          //聊天
	BITMAPLIST_GUILD,			 //工会
	BITMAPLIST_OLPLAYERQUERY,	 //
	BITMAPLIST_TEAMGROUP,		 //团队操作
	BITMAPLIST_LEADERMASK,
	BITMAPLIST_BLACKLIST,
	BITMAPLIST_PET,
	BITMAPLIST_ACTIONBAR,
};
enum ListMessage
{
	MESS_CANCLE,               //取消
	MESS_LEAVETEAM,            //离开队伍
	MESS_LEAVETEAMGROUP,       //离开团队
	MESS_LEADERTRANS,          //队长更改
	MESS_LEADERTRANSGROUP,     //团长更改
	MESS_FOLLOWPLAYERITEM,     //跟随
	MESS_PRIVATETALK,          //私聊
	MESS_REMOVEMEMFORMTEAM,    //踢出队伍
	MESS_REMOVEMEMFORMTEAMGROUP, //踢出团队
	MESS_TEAMINVATE,           //邀请组队
	MESS_TEAMINVATEGROUP,      //邀请组团
	MESS_FIGHT,                //决对
	MESS_VIEW,                 //查看
	MESS_FRIENDADD,            //添加好友
	MESS_TRADE,                //交易
	MESS_DELFRIEND,			   //删除好友
	MESS_MOUNT,                //骑乘
	MESS_APPLYGROUP,

	//工会相关
	MESS_GUILDINVITE,
	MESS_GUILDPROMOTEMEM,
	MESS_GUILDDEMOTEMEM,
	MESS_GUILDREMOVEMEM,
	MESS_GUILDLEADERTRANS,

	MESS_TEAMFENPEI,        //队伍分配
	MESS_TEAMFENPEIGROUP,   //团队分配
	MESS_COPYPLAYERNAME,    //复制玩家名字
	MESS_MOUNTCANCEL,		//取消骑乘
	MESS_TEACHER,			//拜师
	MESS_RANK1,   //
	MESS_RANK2,
	MESS_RANK3,

	MESS_LOCKTARGET,
	MESS_ADDBLACKLIST,
	MESS_DELBLACKLIST,

	MESS_PETCHANGENAME,

	MESS_TRANSFORMTYPE1,
	MESS_TRANSFORMTYPE2,
	MESS_TRANSFORMTYPE3,
	MESS_ACTIONBARLOCK,
	MESS_ACTIONBARUNLOCK,
	MESS_ACTIONBARADD,
	MESS_ACTIONBARDEL,

	MESS_CANCELMASK,

};
class UIRightMouseList : public UControl
{
	UDEC_CLASS(UIRightMouseList);
	UDEC_MESSAGEMAP();
	UIRightMouseList();
	virtual ~UIRightMouseList();
public:
	void Popup(UControl * toCtrl , int type , const char * CreateName , ui64 guid);
	void Popup(const UPoint & toPos , int type , const char * CreateName , ui64 guid);
	void ClosePop();
	ui64 GetGUID(){return m_GUID;};
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL HitTest(const UPoint& parentCoordPoint){return FALSE;}
private:
	void AddList(int type);
	void OnReqestMsg();
	ListMessage FindMess(const char * buf, const void* pDate);
	void AddItem(int type);
private:
	UBitMapListBox *  m_BitMapList;
	std::string       m_CName;
	ui64             m_GUID;
	USkinPtr  m_Skin;
public:
	enum
	{
		ItemEnable = 0,
		ItemDisable,
	};
	void ChangeItemState(const char *str,int istate);
};
#endif