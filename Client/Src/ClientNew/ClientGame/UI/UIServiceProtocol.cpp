#include "stdafx.h"
#include "UIServiceProtocol.h"


UIMP_CLASS(UServiceProtocol,UBitmap);
UBEGIN_MESSAGE_MAP(UServiceProtocol,UBitmap)
UON_BN_CLICKED(2, &UServiceProtocol::Accept)
UON_BN_CLICKED(3, &UServiceProtocol::refuse)
UEND_MESSAGE_MAP()

void UServiceProtocol::StaticInit()
{

}

UServiceProtocol::UServiceProtocol()
{
	m_ServiceTxt = NULL; 
}
UServiceProtocol::~UServiceProtocol()
{

}
void UServiceProtocol::Accept()
{
	SetVisible(FALSE);
	sm_System->GetCurFrame()->SetFocusControl();
}
void UServiceProtocol::refuse()
{
	PostQuitMessage(0);
}
BOOL UServiceProtocol::OnCreate()
{
	if (!UBitmap::OnCreate())
	{
		return FALSE;
	}

	UScrollBar* pBar = (UScrollBar*)GetChildByID(0);
	if (pBar == NULL)
	{
		return FALSE;
	}

	pBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);
	m_ServiceTxt = (URichTextCtrl*)pBar->GetChildByID(1);

	if (m_ServiceTxt == NULL)
	{
		return FALSE;
	}


	return TRUE;
}
void UServiceProtocol::SetServiceProtocol(const char* title , const char* protocol)
{
	if (title && protocol)
	{
		char pTitle[40];
		NiSprintf(pTitle, 40, "<br><br><br><font:Arial:18><center>%s</center><br> ",title);
		m_ServiceTxt->AppendText(pTitle,40);

		char pProtocol[2048];
		NiSprintf(pProtocol, 2048, "<br><font:Arial:12>%s<br>", protocol);
		
		m_ServiceTxt->AppendText(pProtocol,2048, 1);
	}
}
void UServiceProtocol::OnDestroy()
{
	UBitmap::OnDestroy();
}