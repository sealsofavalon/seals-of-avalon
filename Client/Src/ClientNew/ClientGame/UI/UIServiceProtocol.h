#ifndef __UISERBICEPROTOCOL_H__
#define __UISERBICEPROTOCOL_H__
#include "UInc.h"

class UServiceProtocol : public UBitmap
{
	UDEC_CLASS(UServiceProtocol);
	UDEC_MESSAGEMAP();
public:
	UServiceProtocol();
	~UServiceProtocol();

	void SetServiceProtocol(const char* title , const char* protocol);
protected:
	void Accept();
	void refuse();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
private:
	URichTextCtrl* m_ServiceTxt ; 
};

#endif