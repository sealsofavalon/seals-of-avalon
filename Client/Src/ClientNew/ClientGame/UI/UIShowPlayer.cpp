#include "StdAfx.h"
#include "UIShowPlayer.h"
#include "UIPlayerHead.h"
#include "TeamSystem.h"
#include "Network/PacketBuilder.h"
#include "UIGamePlay.h"
#include "ObjectManager.h"
#include "UIRightMouseList.h"
#include "PlayerInputMgr.h"
#include "UAuraSys.h"
#include "UInstanceMgr.h"
#include "SocialitySystem.h"

UIShowPlayer::UIShowPlayer()
{
	m_TeamDate = NULL;
	for (int i =0 ; i < PLAYER_HEAD_MAX ; i++)
	{
		m_PlayerHead[i] = NULL;
	}
	m_LeaderGuid = 0;
	m_RootCtrl = NULL;
	m_SystemState = TRUE;
	m_TargetTarget = NULL;
}

UIShowPlayer::~UIShowPlayer()
{
	TeamShow->DestorySystem();
}

BOOL UIShowPlayer::CreateSystem(UControl * Ctrl)
{
	if (m_PlayerHead[PLAYER_HEAD_LOCAL] == NULL)
	{
		m_PlayerHead[PLAYER_HEAD_LOCAL] = UControl::sm_System->CreateDialogFromFile("UIPlayerShow\\Local.udg");
		if (m_PlayerHead[PLAYER_HEAD_LOCAL])
		{
			Ctrl->AddChild(m_PlayerHead[PLAYER_HEAD_LOCAL]);
			m_PlayerHead[PLAYER_HEAD_LOCAL]->SetId(FRAME_IG_HEADIMAGELOCAL);
		}
		else
		{
			return FALSE;
		}
	}
	UIPlayerHead * uph = UDynamicCast(UIPlayerHead,m_PlayerHead[PLAYER_HEAD_LOCAL]);
	if (uph)
	{
		uph->SetType(PLAYER_HEAD_LOCAL);
	}

	if (m_PlayerHead[PLAYER_HEAD_TARGET] == NULL)
	{
		m_PlayerHead[PLAYER_HEAD_TARGET] = UControl::sm_System->CreateDialogFromFile("UIPlayerShow\\Target.udg");
		if (m_PlayerHead[PLAYER_HEAD_TARGET])
		{
			Ctrl->AddChild(m_PlayerHead[PLAYER_HEAD_TARGET]);
			m_PlayerHead[PLAYER_HEAD_TARGET]->SetId(FRAME_IG_HEADIMAGETARGET);
		}
		else
		{
			return FALSE;
		}
	}
	uph = UDynamicCast(UIPlayerHead,m_PlayerHead[PLAYER_HEAD_TARGET]);
	if (uph)
	{
		uph->SetType(PLAYER_HEAD_TARGET);
	}
	if (m_TargetTarget == NULL)
	{
		m_TargetTarget = UControl::sm_System->CreateDialogFromFile("UIPlayerShow\\TargetTarget.udg");
		if (m_TargetTarget)
		{
			Ctrl->AddChild(m_TargetTarget);
			m_TargetTarget->SetId(FRAME_IG_TARGETTARGET);
		}
		else
		{
			return FALSE;
		}
	}

	int hight = 0;
	for (int i = PLAYER_HEAD_TEAMMEM ; i < PLAYER_HEAD_MAX ; i++)
	{
		if (m_PlayerHead[i] == NULL)
		{
			m_PlayerHead[i] = UControl::sm_System->CreateDialogFromFile("UIPlayerShow\\TeamMem.udg");		
			if (m_PlayerHead[i])
			{
				Ctrl->AddChild(m_PlayerHead[i]);
				m_PlayerHead[i]->SetId(FRAME_IG_HEADIMAGETEAMMEM1 + i - PLAYER_HEAD_TEAMMEM);
				m_PlayerHead[i]->SetPosition(UPoint(0, m_PlayerHead[i]->GetWindowPos().y + hight));
				hight += m_PlayerHead[i]->GetHeight();
			}
			else
			{
				return FALSE;
			}
		}
		uph = UDynamicCast(UIPlayerHead,m_PlayerHead[i]);
		if (uph)
		{
			uph->SetType(i);
		}
	}

	if (m_TeamDate == NULL)
	{
		m_TeamDate = new TeamSystem;
	}
	if (!m_TeamDate)
	{
		return FALSE;
	}
	m_RootCtrl = Ctrl;
	return TRUE;
}
void UIShowPlayer::DestorySystem()
{
	//TODO::同样的UI用自己的销毁机制就可以
	//在这里只是销毁组队系统就可以
	if (m_TeamDate)
	{
		//m_TeamSystem->Destory();
		delete m_TeamDate;
		m_TeamDate = NULL;
	}
}
void UIShowPlayer::BeginSystem()
{
	ClearSystem();
	m_SystemState = TRUE;
}
void UIShowPlayer::EndSystem()
{
	m_SystemState = FALSE;
}
void UIShowPlayer::ClearSystem()
{
	
}
void UIShowPlayer::UpdateTarget(ui64 guid)
{
	UIPlayerHead * uph = UDynamicCast(UIPlayerHead,m_PlayerHead[PLAYER_HEAD_TARGET]);
	if (uph)
	{
		if (!uph->IsTeammember())
		{
			ui64 uguid = uph->GetGuid();
			if (uguid == guid)
			{
				uph->UpdateDate(guid, 0, FALSE);
			}
		}
	}
}
void UIShowPlayer::UpdateLocal()
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return ;
	}
	UIPlayerHead * uph = UDynamicCast(UIPlayerHead,m_PlayerHead[PLAYER_HEAD_LOCAL]);
	if (uph)
	{
		uph->UpdateDate(pkLocal->GetGUID(), 0, FALSE);
	}
}

void UIShowPlayer::OnLocalPlayerCreate(CPlayer* pLocal)
{
	UIPlayerHead * uph = UDynamicCast(UIPlayerHead,m_PlayerHead[PLAYER_HEAD_LOCAL]);
	if (uph)
	{
		uph->SetLocalPlayer(pLocal);
	}
}

void UIShowPlayer::SetTeamLeaderGuid(ui64 guid)
{
	m_LeaderGuid = guid;
	InstanceSys->OnTeamUpdata();

	if(SocialitySys->m_iShowFrame == 4)
		SocialitySys->ShowFrame(4);
	
	for (int i = PLAYER_HEAD_LOCAL; i < PLAYER_HEAD_MAX; i++)
	{
		UIPlayerHead* pkHead = UDynamicCast(UIPlayerHead,m_PlayerHead[i]);
		if (pkHead)
		{
			BOOL bShow = FALSE ;
			if (m_LeaderGuid == pkHead->GetGuid() && guid)
			{
				bShow = TRUE;
			}
			pkHead->ShowTeamLeaderFlag(bShow);
		}
	}
}


ui64 UIShowPlayer::GetTeamLeaderGuid()
{
	return m_LeaderGuid;
}
BOOL UIShowPlayer::IsTeamLeader(ui64 guid)
{
	if (guid != 0)
	{
		return guid == m_LeaderGuid;
	}
	return FALSE;
}
void UIShowPlayer::UpdateObjHead(ui64 guid, uint32 visiblebase, ui32 itemid, ui32 SlotIndex)
{
	for (int i = PLAYER_HEAD_LOCAL; i < PLAYER_HEAD_MAX; i++)
	{
		if (m_PlayerHead[i])
		{
			UIPlayerHead* pUIHead = UDynamicCast(UIPlayerHead, m_PlayerHead[i]);
			if (pUIHead)
			{
				pUIHead->UpdateOBJHead(guid,visiblebase,itemid,SlotIndex);
			}
		}
	}
}
UControl* UIShowPlayer::GetTeamCtrl(UINT index)
{
	if (index >= PLAYER_HEAD_TARGET && index < PLAYER_HEAD_MAX)
	{
		return m_PlayerHead[index];
	}

	return NULL ;
}
void UIShowPlayer::SetHeadEQShow(BOOL bshow, ui64 guid)
{
	for (int i = PLAYER_HEAD_LOCAL; i < PLAYER_HEAD_MAX; i++)
	{
		if (m_PlayerHead[i])
		{
			UIPlayerHead* pUIHead = UDynamicCast(UIPlayerHead, m_PlayerHead[i]);
			if (pUIHead)
			{
				pUIHead->SetHeadEQShow(bshow, guid);
			}
		}
	}
}
void UIShowPlayer::SetHead3DShow(BOOL B3D)
{
	for (int i = PLAYER_HEAD_LOCAL; i < PLAYER_HEAD_MAX; i++)
	{
		if (m_PlayerHead[i])
		{
			UIPlayerHead* pUIHead = UDynamicCast(UIPlayerHead, m_PlayerHead[i]);
			if (pUIHead)
			{
				pUIHead->SetHead3DShow(B3D);
			}
		}
	}
}
void UIShowPlayer::UpdateTargetUI()
{
	UIPlayerHead * uph = UDynamicCast(UIPlayerHead,m_PlayerHead[PLAYER_HEAD_TARGET]);
	if(uph)
	{
		uph->SetUI();
	}
}
void UIShowPlayer::SetTargetGUID(ui64 guid)
{
	UIPlayerHead * uph = UDynamicCast(UIPlayerHead,m_PlayerHead[PLAYER_HEAD_TARGET]);
	CCharacter* pkObject = (CCharacter*)ObjectMgr->GetObject(guid);
	TeamDate* Item = SocialitySys->GetTeamSysPtr()->GetTeamDate(guid);
	SocialitySys->GetTeamSysPtr()->SetUIByCurDate(Item);
	if (pkObject == NULL)
	{
		SetTargetTarget(0);
	}
	if(pkObject== NULL &&  Item == NULL)
	{
		uph->SetGuid(0);
		AuraSystem->SetGuid(AURA_TARGET,0);
		TeamShow->CallRMD(FALSE);
		return;
	}
	bool isFriend = false ;
	if (Item)
	{
		isFriend = true;
	}else
	{
		isFriend = pkObject->IsFriendly(ObjectMgr->GetLocalPlayer());
	}
	if (uph)
	{
		uph->SetGuid(guid, Item);
		AuraSystem->SetGuid(AURA_TARGET,guid);
		TeamShow->CallRMD(FALSE);
	}
	UViewPlayerEquipment* pViewPlayerEqu = INGAMEGETFRAME(UViewPlayerEquipment, FRAME_IG_VIEWPLAYEREQUIPDLG);
	if (pViewPlayerEqu && pViewPlayerEqu->IsVisible())
		pViewPlayerEqu->SetViewPlayer(pkObject);
}

void UIShowPlayer::SetTargetTarget(ui64 guid)
{
	if (m_TargetTarget)
	{
		UITargetTarget* pTargetTarget = UDynamicCast(UITargetTarget, m_TargetTarget);
		if (pTargetTarget)
		{
			if(guid)
			{
				CCharacter* pChar = (CCharacter*)ObjectMgr->GetObject(guid);
				pTargetTarget->SetTarget(pChar);
			}
			else
			{
				pTargetTarget->SetTarget(NULL);
			}
		}
	}
}

void UIShowPlayer::SetTargetTargetField(int field, ui32 content)
{
	UITargetTarget* pTargetTarget = UDynamicCast(UITargetTarget, m_TargetTarget);
	if (pTargetTarget)
	{
		pTargetTarget->SetField(field, content);
	}
}

void UIShowPlayer::SetTargetTargetShow(bool bShow)
{
	UITargetTarget* pTargetTarget = UDynamicCast(UITargetTarget, m_TargetTarget);
	if (pTargetTarget)
	{
		pTargetTarget->SetShow(bShow);
	}

}
bool UIShowPlayer::GetTargetTargetShow()
{
	UITargetTarget* pTargetTarget = UDynamicCast(UITargetTarget, m_TargetTarget);
	if (pTargetTarget)
	{
		return pTargetTarget->GetShow();
	}
	return false;
}

void UIShowPlayer::SetTargetID(int Type)
{
	switch (Type)
	{
	case PLAYER_HEAD_LOCAL:	
		{
			CPlayer * pkPlayer = ObjectMgr->GetLocalPlayer();
			if (pkPlayer)
			{
				SetTargetGUID(pkPlayer->GetGUID());
			}
		}
		break;
	case PLAYER_HEAD_TARGET:
		break;
	case PLAYER_HEAD_TEAMMEM:case PLAYER_HEAD_TEAMMEM + 1:case PLAYER_HEAD_TEAMMEM + 2:case PLAYER_HEAD_TEAMMEM + 3:
		{
			UIPlayerHead * uphT = UDynamicCast(UIPlayerHead,m_PlayerHead[Type]);
			if (uphT && uphT->GetGuid())
			{
				SetTargetGUID(uphT->GetGuid());
			}
		}
		break;
	};
}
void UIShowPlayer::SelectTarget(int Type)
{
	CPlayerLocal * pkPlayer = ObjectMgr->GetLocalPlayer();
	if (pkPlayer && pkPlayer->IsHook())
	{
		return ;
	}
	UIPlayerHead * uph = UDynamicCast(UIPlayerHead,m_PlayerHead[PLAYER_HEAD_TARGET]);
	if (uph)
	{
		switch (Type)
		{
		case PLAYER_HEAD_LOCAL:	
			{
				if (pkPlayer)
				{
					SYState()->LocalPlayerInput->SetTargetID(pkPlayer->GetGUID());
					//uph->SetGuid(pkPlayer->GetGUID());
				}
			}
			break;
		case PLAYER_HEAD_TARGET:
			break;
		case PLAYER_HEAD_TEAMMEM:case PLAYER_HEAD_TEAMMEM + 1:case PLAYER_HEAD_TEAMMEM + 2:case PLAYER_HEAD_TEAMMEM + 3:
			{
				UIPlayerHead * uphT = UDynamicCast(UIPlayerHead,m_PlayerHead[Type]);
				if (uphT && uphT->GetGuid())
				{
					SYState()->LocalPlayerInput->SetTargetID(uphT->GetGuid());
					//uph->SetGuid(uphT->GetGuid());
				}
			}
			break;
		};
	}
}
ui64 UIShowPlayer::GetTargetID()
{
	UIPlayerHead * uph = UDynamicCast(UIPlayerHead,m_PlayerHead[PLAYER_HEAD_TARGET]);
	if ( uph )
	{
		ui64 guid = uph->GetGuid();
		return guid;
	}
	return 0;
}

void UIShowPlayer::CallRMD(BOOL visible,const UPoint & pos,ui64 guid ,int Type)
{
	UInGame * pkInGame = UDynamicCast(UInGame,m_RootCtrl);
	char buf[1024];
	for (int i = 0 ; i < PLAYER_HEAD_MAX ; i++)
	{
		UIPlayerHead* uph = UDynamicCast(UIPlayerHead,m_PlayerHead[i]);
		if (uph)
		{
			if (uph->GetGuid() == guid)
			{
				uph->GetName(buf);
			}
		}
	}
	if (visible && Type != PLAYER_HEAD_MAX + 1)
	{
	/*	CCharacter* pChar = (CCharacter*)ObjectMgr->GetObject(guid);
		if(pChar && !ObjectMgr->GetLocalPlayer()->IsFriendly(pChar))
			return;*/

		//if (pChar && pChar->GetGameObjectType() == GOT_PLAYER )
		//{
		//	if (pChar->GetRace() != ObjectMgr->GetLocalPlayer()->GetRace())
		//	{
		//		return ;
		//	}
		//}

	}
	if (pkInGame)
	{
		UIRightMouseList * uRmd = INGAMEGETFRAME(UIRightMouseList,FRAME_IG_RIGHTMOUSELIST);
		if (visible == FALSE)
		{
			if (uRmd)
			{
				uRmd->ClosePop();
			}
			return;
		}
		if (visible && uRmd)
		{
			if (uRmd)
			{
				switch (Type)
				{
				case PLAYER_HEAD_LOCAL:
					uRmd->Popup(pos,BITMAPLIST_LOCALPLAYER,buf,guid);
					break;
				case PLAYER_HEAD_TARGET:
					uRmd->Popup(pos,BITMAPLIST_TENNET,buf,guid);
					break;
				case PLAYER_HEAD_TEAMMEM:
				case PLAYER_HEAD_TEAMMEM + 1:
				case PLAYER_HEAD_TEAMMEM + 2:
				case PLAYER_HEAD_TEAMMEM + 3:
					uRmd->Popup(pos,BITMAPLIST_TEAMMEM,buf,guid);
					break;
				case PLAYER_HEAD_MAX + 1:
					uRmd->Popup(pos,BITMAPLIST_LEADERMASK,buf,guid);
					break;
				}
			}
		}
	}
}

void UIShowPlayer::ReceiveMemberList(std::vector<TeamDate*>& p)
{
	if (m_TeamDate)
	{
		m_TeamDate->ParseMemberList(p);
	}
}
void UIShowPlayer::ResetTargetDate()
{
	//当团队有数据移动操作的时候，当前目标的数据指针也需要跟着变化
	//去寻找到正确的数据.这个借口支针对团队里小队的数据移动操作.
	//而且这个接口支针对选取的对象窗口进行TEAMDATE指针的重置。
	//UIPlayerHead* pkHead = UDynamicCast(UIPlayerHead, m_PlayerHead[PLAYER_HEAD_FRIENDTARGET]);
	//if (pkHead)
	//{
	//	ui64 guid = pkHead->GetGuid();
	//	TeamDate* pkDate = pkHead->GetTeamDate();
	//	if (pkDate)
	//	{
	//		if (pkDate->Guid != guid) // 有数据切换，需要寻找新的数据
	//		{
	//			TeamDate* newDate = SocialitySys->GetTeamSysPtr()->GetTeamDate(guid);
	//			SocialitySys->GetTeamSysPtr()->SetUIByCurDate(newDate);
	//			if (newDate)
	//			{	
	//				//找到了新的数据
	//				pkHead->SetGuid(guid,newDate);
	//				AuraSystem->SetGuid(AURA_TARGET,guid);
	//				AuraSystem->SetGuid(AURA_DEFTARGET,0);
	//			}else
	//			{
	//				//没招到新的数据
	//				//先判断是否在当前列表
	//				CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(guid);
	//				if (pkChar)
	//				{
	//					//在当前列表
	//					pkHead->SetGuid(guid);
	//					AuraSystem->SetGuid(AURA_TARGET,guid);
	//					AuraSystem->SetGuid(AURA_DEFTARGET,0);
	//				}else
	//				{
	//					//不在当前列表
	//					pkHead->SetGuid(0);
	//					AuraSystem->SetGuid(AURA_TARGET,0);
	//					AuraSystem->SetGuid(AURA_DEFTARGET,0);
	//				}
	//			}
	//		}
	//	}
	//}
	UIPlayerHead* pkHead = UDynamicCast(UIPlayerHead, m_PlayerHead[PLAYER_HEAD_TARGET]);
	if (pkHead)
	{
		ui64 guid = pkHead->GetGuid();
		TeamDate* pkDate = pkHead->GetTeamDate();
		if (pkDate)
		{
			if (pkDate->Guid != guid) // 有数据切换，需要寻找新的数据
			{
				TeamDate* newDate = SocialitySys->GetTeamSysPtr()->GetTeamDate(guid);
				SocialitySys->GetTeamSysPtr()->SetUIByCurDate(newDate);
				if (newDate)
				{
					//找到了新的数据
					pkHead->SetGuid(guid,newDate);
					AuraSystem->SetGuid(AURA_TARGET,0);
				}else
				{
					//如果没找到。标示离队啦
					CCharacter* pkChar = (CCharacter*)ObjectMgr->GetObject(guid);
					if (pkChar)
					{
						//在当前可见列表
						pkHead->SetGuid(guid);
						AuraSystem->SetGuid(AURA_TARGET,0);
					}else
					{
						//不再当前可见列表
						pkHead->SetGuid(0);
						AuraSystem->SetGuid(AURA_TARGET,0);
					}
				}
			}
		}
	}
}
void UIShowPlayer::ReceiveDestoryGroup()
{
	if (m_TeamDate)
	{
		m_TeamDate->ParseGroupDestory();
	}
}
void UIShowPlayer::UpdateFromObj(ui64 guid, ui32 mask, BOOL bFromData)
{
	if (m_TeamDate)
	{
		m_TeamDate->UpdateFromObj(guid,mask, bFromData);
	}
}

TeamDate * UIShowPlayer::GetData(ui64 guid)
{
	if (m_TeamDate)
	{
		return m_TeamDate->GetData(guid);
	}
	return NULL;
}