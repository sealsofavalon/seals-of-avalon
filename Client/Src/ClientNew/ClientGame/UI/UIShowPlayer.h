#ifndef UISHOWPLAYER_H
#define UISHOWPLAYER_H
#include "UInc.h"
#include "Singleton.h"

//用这个来管理头像UI的显示以及组队系统,只将这个头文件对外接口
//可是组队系统本身就是个独立的系统存在,这里对组队系统的操作其实
//就是把组队系统作为一个子系统来存在,并且和头像显示放在同一级别进行
#define TeamShow UIShowPlayer::GetSingleton()
//这里先用静态指针来保存系统，以后会改掉，用一个头文件来保存所有的系统指针
enum
{
	PLAYER_HEAD_LOCAL,
	PLAYER_HEAD_TARGET,
	PLAYER_HEAD_TEAMMEM,
	PLAYER_HEAD_MAX =  PLAYER_HEAD_TEAMMEM + 4
};
enum
{
	FIELD_TARGETTARGET_HP,
	FIELD_TARGETTARGET_MAXHP,
	FIELD_TARGETTARGET_MP,
	FIELD_TARGETTARGET_MAXMP,
};
struct TeamDate;
class UIShowPlayer : public Singleton<UIShowPlayer>
{
	SINGLETON(UIShowPlayer);
	//friend class PlayerData;
	friend class TeamSystem;
	friend class UIPlayerHead;
public:
	BOOL CreateSystem(UControl * Ctrl);
	void DestorySystem();
	void BeginSystem();
	void ClearSystem();
	void EndSystem();
public:
	void OnLocalPlayerCreate(class CPlayer* pLocal);
	void UpdateLocal();
	void UpdateTargetUI();
	void UpdateTarget(ui64 guid); //更新非组队成员
	void UpdateObjHead(ui64 guid, uint32 visiblebase, ui32 itemid, ui32 SlotIndex); //更新头像
	void UpdateFromObj(ui64 guid,ui32 mask, BOOL bFromData);//更新在队伍或者团队的成员
	void SetHead3DShow(BOOL B3D);
	void SetHeadEQShow(BOOL bshow, ui64 guid);

	void SetTeamLeaderGuid(ui64 guid);
	ui64 GetTeamLeaderGuid();
	BOOL IsTeamLeader(ui64 guid);

	void CallRMD(BOOL visible,const UPoint & pos = UPoint(0,0),ui64 guid = 0,int Type = PLAYER_HEAD_LOCAL);
	TeamDate * GetData(ui64 guid);
public:
	void SelectTarget(int Type);
	void SetTargetGUID(ui64 guid);
	void SetTargetID(int Type);
	ui64 GetTargetID();

	void SetTargetTarget(ui64 guid);
	void SetTargetTargetField(int field, ui32 content);
	void SetTargetTargetShow(bool bShow);
	bool GetTargetTargetShow();

	UControl* GetTeamCtrl(UINT index);
public:
	void ReceiveMemberList(std::vector<TeamDate*>& p);
	void ReceiveDestoryGroup();
	void ResetTargetDate();
protected:
	UControl * m_PlayerHead[PLAYER_HEAD_MAX];
	UControl * m_TargetTarget;
	UControl * m_RootCtrl;
	ui64 m_LeaderGuid;
	TeamSystem* m_TeamDate;
	BOOL m_SystemState;
};
#endif