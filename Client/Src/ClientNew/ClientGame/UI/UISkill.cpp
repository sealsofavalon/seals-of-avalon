#include "StdAfx.h"
#include "UISkill.h"
#include "UIItemSystem.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "Skill/SkillInfo.h"
#include "Skill/SkillManager.h"
#include "PlayerInputMgr.h"
#include "LocalPlayer.h"
#include "ObjectManager.h"
#include "UInGameBar.h"
#include "UChat.h"
#include "USpecListBox.h"
#include "ItemManager.h"
#include "../Utils/SpellDB.h"
#include "UFun.h"
#include "UIMoneyText.h"
#include "Network/PacketBuilder.h"
#include "UMessageBox.h"

uint32 g_vLineClass[5][5] = {
	{0,0,0,0,0},
	{1000, 1001, 1002, 1016, 1017},
	{1012, 1013, 1014, 1016, 1017},
	{1004, 1005, 1006, 1016, 1017},
	{1008, 1009, 1010, 1016, 1017}
};

UIMP_CLASS(UISkill,UDialog);
UBEGIN_MESSAGE_MAP(UISkill,UDialog)
UON_BN_CLICKED(12,&UISkill::OnPageUp)
UON_BN_CLICKED(13,&UISkill::OnPageDown)
UON_BN_CLICKED(73,&UISkill::OnChooseClass1)
UON_BN_CLICKED(74,&UISkill::OnChooseClass2)
UON_BN_CLICKED(75,&UISkill::OnChooseClass3)
UON_BN_CLICKED(76,&UISkill::OnChooseClass4)
UON_BN_CLICKED(77,&UISkill::OnChooseClass5)
UON_BN_CLICKED(71,&UISkill::OnChooseSkillBook)
UON_BN_CLICKED(72,&UISkill::OnChooseProfBook)
UON_CBOX_SELCHANGE(14, &UISkill::OnShowTypeChang)
UEND_MESSAGE_MAP()

void UISkill::StaticInit()
{

}
UISkill::UISkill()
{
	m_curPage = 0;
	m_numPage = 0;

	memset(m_SkillItem, NULL, 12);

	m_ShowType = USYMaxLev ;
	m_SkillShowComboBox = NULL ;
	m_ProfSkill1 = NULL;
	m_ProfSkill2 = NULL;

	for (int i = 0 ; i < 5 ; i++)
	{
		m_ClassButton[i] = NULL;
	}
}

UISkill::~UISkill()
{
}
BOOL UISkill::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_ShowType = USYMaxLev ;
	m_bCanDrag = FALSE;

	for (int i = 0 ; i < UPageSkillMax; i++)
	{
		m_SkillItem[i] = UDynamicCast(USkillExplain, GetChildByID(i));
		if (!m_SkillItem[i])
		{
			return FALSE;
		}
	}

	if (m_SkillShowComboBox == NULL)
	{
		m_SkillShowComboBox = UDynamicCast(UComboBox, GetChildByID(14));
	}

	if (m_SkillShowComboBox == NULL)
	{
		return FALSE;
	}

	m_SkillShowComboBox->AddItem( _TRAN("全部") );
	m_SkillShowComboBox->AddItem( _TRAN("最高等级") );

	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(15));
	if (pText)
	{
		pText->SetTextEdge(true);
	}
	if (m_ProfSkill1 == NULL)
	{
		m_ProfSkill1 = UDynamicCast(UProfSkillExplain, GetChildByID(52));
		if (!m_ProfSkill1)
		{
			return FALSE;
		}
	}
	if (m_ProfSkill2 == NULL)
	{
		m_ProfSkill2 = UDynamicCast(UProfSkillExplain, GetChildByID(53));
		if (!m_ProfSkill2)
		{
			return FALSE;
		}
	}
	for (int i = 0 ; i < 5 ; i++)
	{
		m_ClassButton[i] = UDynamicCast(UBitmapButton, GetChildByID(73 + i));
		if (!m_ClassButton[i])
		{
			return FALSE;
		}
	}
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(71));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	pBtn = UDynamicCast(UButton, GetChildByID(73));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	return TRUE;
}
void UISkill::OnDestroy()
{
	if (m_SkillShowComboBox)
	{
		m_SkillShowComboBox->Clear();
	}
	UDialog::OnDestroy();
}

void UISkill::OnClose()
{
	UInGame* pInGame = (UInGame*)UInGame::Get();
	if (!pInGame)
		return;

	UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	if (pGameBar)
	{
		UButton* pBtn =  UDynamicCast(UButton,pGameBar->GetChildByID(UINGAMEBAR_BUTTONSKILL));
		if (pBtn)
			pBtn->SetCheck(TRUE);
	}
}	
void UISkill::OnInitSkillList(vector<ui32> &pSpells)
{
	ClearUI();
	m_Spells = pSpells;
	SetShowSkillType(m_ShowType);
	
}
BOOL UISkill::FindSpellInLocal(ui32 pSpell)
{
	for (size_t i = 0 ; i < m_Spells.size() ; i++)
	{
		if (pSpell == m_Spells[i])
		{
			return TRUE;
		}
	}
	return FALSE;
}
void UISkill::OnUpdateSkillList(ui32 pSpell)
{
	InsertSkillVec(pSpell);
	SetShowSkillType(m_ShowType);
}
void UISkill::InsertSkillVec(ui32 spellid)
{	
	SpellTemplate* pkSpellTem = SYState()->SkillMgr->GetSpellTemplate(spellid);
	if (!pkSpellTem)
	{
		return ;
	}
	UINT skillGroup = pkSpellTem->GetSpellGroup(); 

	int  POS  = m_Spells.size();

	for (size_t i = 0 ; i < m_Spells.size(); i++)
	{
		SpellTemplate* pkSpellTemA = SYState()->SkillMgr->GetSpellTemplate(m_Spells[i]);
		if (!pkSpellTemA)
		{
			continue;
		}
		
		if (pkSpellTemA->GetSpellGroup() == skillGroup)
		{
			POS = i ;
		}
	}

	int c = m_Spells.size() - 1;
	if (POS < c)
	{
		m_Spells.insert(m_Spells.begin() + POS + 1, spellid);
	}else
	{
		m_Spells.push_back(spellid);
	}
	ChatSystem->SpellLearnMsg(spellid);
}
BOOL UISkill::OnDelSkillByID(ui32 pSpell)
{
	BOOL bInList = FALSE;
	if (pSpell)
	{
		for (size_t i = 0 ; i < m_Spells.size() ; i++)
		{
			if (pSpell == m_Spells[i])
			{
				bInList = TRUE;
				m_Spells.erase(m_Spells.begin() + i);
				break;
			}
		}
		if (bInList)
		{
			SetShowSkillType(m_ShowType);
		}
	}
	return bInList;
}
void UISkill::OnSetCurPage()
{
	UINT count = m_ShowSpells.size();
	m_numPage = count / UPageSkillMax ;
	if (count % UPageSkillMax > 0)
		m_numPage ++;

	UBitmapButton* pkUP = (UBitmapButton*)GetChildByID(12);
	UBitmapButton* pkDown = (UBitmapButton*)GetChildByID(13);
	if (m_curPage == 0)
	{
		pkUP->SetVisible(FALSE);
	}else
	{
		pkUP->SetVisible(TRUE);
	}

	if (m_curPage == m_numPage - 1 || m_numPage == 0)
	{
		pkDown->SetVisible(FALSE);
	}else
	{
		pkDown->SetVisible(TRUE);
	}


	for (int i = 0 ; i < UPageSkillMax ; i++)
	{
		if (m_curPage * UPageSkillMax + i < m_ShowSpells.size())
		{
			OnSetSkillByPos(i,m_ShowSpells[m_curPage * UPageSkillMax + i]);
		}else
		{
			OnSetSkillByPos(i,0);
		}
	}

	CPlayerLocal* pkplayer = ObjectMgr->GetLocalPlayer();
	if (pkplayer)
	{
		int PlayerClass = pkplayer->GetClass();
		for (int i = 0 ; i < 5; i++)
		{
			std::string name;
			if (g_SkillLineNameInfo->GetSkillLineName(g_vLineClass[PlayerClass][i], name))
			{
				m_ClassButton[i]->SetText(name.c_str());
			}
		}
	}
	UpdateBtnVisible(4);
}
void UISkill::OnSetSkillByPos(UINT pos, ui32 pSpellID)
{
	if (pos >= 0  && pos < UPageSkillMax)
	{
		USkillExplain* pSkillExplain = NULL;
		pSkillExplain = m_SkillItem[pos];
		if (pSkillExplain)
		{
			pSkillExplain->InitSkill(pSpellID,pos);
		}
	}
}
void UISkill::OnPageUp()
{
	if (m_curPage > 0)
	{
		m_curPage-- ;
	}
	OnSetCurPage();
}
void UISkill::OnPageDown()
{
	if (m_curPage < m_numPage -1)
	{
		m_curPage++;
	}
	OnSetCurPage();
}
void UISkill::ClearUI()
{
	m_Spells.clear();
	m_ShowSpells.clear();
	for(int i = 0 ; i < UPageSkillMax ; i++)
	{
		m_SkillItem[i]->OnClear();
	}
}
void UISkill::SetShowSkillType(USYSkillShowType pSkillShowType)
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(72));
	if (pBtn && pBtn->IsChecked())
	{
		return;
	}
	m_ShowSpells.clear();

	if (m_Spells.size() == 0)
	{
		return ;
	}
	SortSpells(0);

	for(int i = 0 ; i < UPageSkillMax ; i++)
	{
		m_SkillItem[i]->OnClear();
	}

	m_SkillShowComboBox->SetCurSel(pSkillShowType);

	//UINT count = m_ShowSpells.size();
	//m_numPage = count / UPageSkillMax ;

	//
	//if (count % UPageSkillMax > 0)
	//{
	//	m_numPage ++;
	//}

	m_curPage = 0;
	OnSetCurPage();
}
void UISkill::OnShowTypeChang()
{
	if (m_SkillShowComboBox)
	{
		UINT CEL =  m_SkillShowComboBox->GetCurSel();
		if (CEL >= 0)
		{
			SetShowSkillType((USYSkillShowType)CEL);
		}
	}
	
}

void UISkill::OnProfLevelChange(uint32 spellLockId, uint32 current)
{
	std::string name;
	if( g_SkillLineNameInfo->GetSkillLineName(spellLockId, name))
	{
		ClientSystemNotify("%s%s%u", name.c_str(), _TRAN("提高到了"), current);
	}
	USkillTrainingDlg* pTrainingDlg = INGAMEGETFRAME(USkillTrainingDlg, FRAME_IG_SPELLTRAININGDLG);
	if (pTrainingDlg)
	{
		pTrainingDlg->UpdateSpellList();
	}
}

void UISkill::RefreshProf()
{
	CPlayerLocal* pkplayer = ObjectMgr->GetLocalPlayer();
	if (!pkplayer)
		return;
	std::vector<uint32> vSpellEntry;
	m_ProfSkill1->SetSkillEntry(0);
	m_ProfSkill2->SetSkillEntry(0);
	if (pkplayer && pkplayer->GetProfessionEntryList(vSpellEntry))
	{
		int size = min(vSpellEntry.size(), 2);
		for(int i = 0 ; i < size; i++)
		{
			if ( i == 0)
			{
				m_ProfSkill1->SetSkillEntry(vSpellEntry[i]);
			}else if( i == 1)
			{
				m_ProfSkill2->SetSkillEntry(vSpellEntry[i]);
			}
		}
	}
}

void UISkill::SetVisible(BOOL value)
{
	UDialog::SetVisible(value);
}

void UISkill::FindMaxLevelSkill()
{
	//m_ShowSpells.push_back(m_Spells[0]);
	std::vector<ui32> vSpell;
	for (UINT i = 0; i < m_ShowSpells.size(); i++)
	{
		SpellTemplate* pSpellTemplateA = SYState()->SkillMgr->GetSpellTemplate(m_ShowSpells[i]);
		if(!pSpellTemplateA)
		{
//			NIASSERT(0);
			continue;
		}
		UINT spellGA = pSpellTemplateA->GetSpellGroup();
		UINT spellLA = pSpellTemplateA->GetSpellLev();

		BOOL bNeedAdd = TRUE;
		if (spellGA)
		{
			for (UINT j =0 ; j < vSpell.size(); j++)
			{
				SpellTemplate* pSpellTemplateB = SYState()->SkillMgr->GetSpellTemplate(vSpell[j]);
				if(!pSpellTemplateA)
				{
					NIASSERT(0);
					continue;
				}
				UINT spellGB = pSpellTemplateB->GetSpellGroup();
				UINT spellLB = pSpellTemplateB->GetSpellLev();

				if (spellGA == spellGB)
				{
					bNeedAdd = FALSE;
					if (spellLA > spellLB)
					{
						vSpell[j] = m_ShowSpells[i];
					}
				}
			}
		}
		if (bNeedAdd)
		{
			vSpell.push_back(m_ShowSpells[i]);
		}
	}
	m_ShowSpells = vSpell;
}
BOOL UISkill::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}

void UISkill::UpdateBtnVisible(int btnIndex)
{
	CPlayerLocal* pkplayer = ObjectMgr->GetLocalPlayer();
	if (!pkplayer)
		return;

	bool isEmpty = false;
	for (unsigned int ui = 0 ; ui < m_Spells.size() ; ++ui)
	{
		SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate(m_Spells[ui]);
		uint32 entry = g_vLineClass[pkplayer->GetClass()][btnIndex];
		if (pSpellTemplate && pSpellTemplate->GetLineNameEntry() == entry)
		{
			isEmpty = true;
			break;;
		}
	}

	m_ClassButton[btnIndex]->SetVisible(isEmpty);
}

void UISkill::OnChooseSkillBook()
{
	if(GetChildByID(51) && GetChildByID(50) && GetChildByID(14))
	{
		GetChildByID(51)->SetVisible(FALSE);
		GetChildByID(50)->SetVisible(TRUE);
		GetChildByID(14)->SetVisible(TRUE);
	}
	m_ProfSkill1->SetVisible(FALSE);
	m_ProfSkill2->SetVisible(FALSE);
	for (int i = 0 ; i < 4 ; i++)
	{
		m_ClassButton[i]->SetVisible(TRUE);
	}
	UpdateBtnVisible(4);
	SetShowSkillType(m_ShowType);
}
void UISkill::OnChooseProfBook()
{
	if(GetChildByID(51) && GetChildByID(50) && GetChildByID(14))
	{
		GetChildByID(51)->SetVisible(TRUE);
		GetChildByID(50)->SetVisible(FALSE);
		GetChildByID(14)->SetVisible(FALSE);
	}
	m_ProfSkill1->SetVisible(TRUE);
	m_ProfSkill2->SetVisible(TRUE);
	UBitmapButton* pkUP = UDynamicCast(UBitmapButton, GetChildByID(12));
	if (pkUP)
	{
		pkUP->SetVisible(FALSE);
	}
	UBitmapButton* pkDown = UDynamicCast(UBitmapButton, GetChildByID(13));
	if (pkDown)
	{
		pkDown->SetVisible(FALSE);
	}

	for(int i = 0 ; i < UPageSkillMax ; i++)
	{
		m_SkillItem[i]->OnClear();
	}
	for (int i = 0 ; i < 5 ; i++)
	{
		m_ClassButton[i]->SetVisible(FALSE);
	}
	RefreshProf();
	//CPlayerLocal* pkplayer = ObjectMgr->GetLocalPlayer();
	//if (!pkplayer)
	//	return;
	//std::vector<uint32> vSpellEntry;
	//if (pkplayer && pkplayer->GetProfessionEntryList(vSpellEntry))
	//{
	//	int size = min(vSpellEntry.size(), 2);
	//	for(int i = 0 ; i < size; i++)
	//	{
	//		if ( i == 0)
	//		{
	//			m_ProfSkill1->SetSkillEntry(vSpellEntry[i]);
	//		}else if( i == 1)
	//		{
	//			m_ProfSkill2->SetSkillEntry(vSpellEntry[i]);
	//		}
	//	}
	//}

}
void UISkill::OnChooseClass1()
{
	SortSpells(0);
}
void UISkill::OnChooseClass2()
{
	SortSpells();
}
void UISkill::OnChooseClass3()
{
	SortSpells();
}
void UISkill::OnChooseClass4()
{
	SortSpells();
}
void UISkill::OnChooseClass5()
{
	SortSpells();
}

void UISkill::SortSpells(int ClassNum)
{
	UINT CEL = USYSkillAll;
	if (m_SkillShowComboBox)
	{
		CEL =  m_SkillShowComboBox->GetCurSel();
	}
	int ClassIndex = 0;
	for (int i = 0 ; i < 5 ; i++)
	{
		if (m_ClassButton[i]->IsChecked())
		{
			ClassIndex = i;
			break;
		}
	}
	CPlayerLocal* pkplayer = ObjectMgr->GetLocalPlayer();
	if (!pkplayer)
		return;
	m_ShowSpells.clear();
	for (unsigned int ui = 0 ; ui < m_Spells.size() ; ++ui)
	{
		SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate(m_Spells[ui]);
		uint32 entry = g_vLineClass[pkplayer->GetClass()][ClassIndex];
		if (pSpellTemplate && pSpellTemplate->GetLineNameEntry() == entry)
		{
			m_ShowSpells.push_back(m_Spells[ui]);
		}
	}

	if (CEL == USYSkillAll)
		m_ShowSpells = m_ShowSpells;
	else if (CEL == USYMaxLev)
		FindMaxLevelSkill();

	m_curPage = 0;
	OnSetCurPage();
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(USkillExplain,UControl);


void USkillExplain::StaticInit()
{

}
USkillExplain::USkillExplain()
{
	m_SkillInfo = NULL;
	m_SkillName = NULL;
	m_SkillLev = NULL;
}

USkillExplain::~USkillExplain()
{

}
void USkillExplain::SkillInfoButtonItem::Use()
{
	CPlayerInputMgr * InputMsg = SYState()->LocalPlayerInput;
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal && pkLocal->IsHook())
	{
		return ;
	}

	if (InputMsg && m_dataInfo.entry != 0 && m_dataInfo.pos != 255)
	{
		if (m_dataInfo.type == UItemSystem::SPELL_DATA_FLAG)
		{		
			if (InputMsg->IsSelectingTarget())
			{
				InputMsg->EndSelectTarget();
				return;
			}
			InputMsg->BeginSkillAttack((int)m_dataInfo.Guid);
		}
		if (m_dataInfo.type == UItemSystem::ITEM_DATA_FLAG)
		{

		}
	}
}
BOOL USkillExplain::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	m_SkillName = (UStaticText*)GetChildByID(0);
	m_SkillLev = (UStaticText*)GetChildByID(1);
	m_SkillInfo = UDynamicCast(UDynamicIconButtonEx, GetChildByID(3));


	if (m_SkillName == NULL || m_SkillLev == NULL || m_SkillInfo == NULL)
	{
		return FALSE;
	}
	m_SkillName->SetTextColor("ffffff");
	m_SkillName->SetTextShadow(true);
	m_SkillName->SetTextAlign(UT_LEFT);

	m_SkillLev->SetTextColor("ffff00");
	m_SkillLev->SetTextShadow(true);
	m_SkillLev->SetTextAlign(UT_LEFT);

	m_SkillInfo->SetActionItem(new SkillInfoButtonItem);
	m_SkillInfo->SetType(UItemSystem::ICT_SKILL);
	m_SkillInfo->SetKeepIcon(TRUE);

	return TRUE;
}

void USkillExplain::OnDestroy()
{
	UControl::OnDestroy();
}
void USkillExplain::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{

}
void USkillExplain::OnClear()
{
	m_SkillLev->Clear();
	m_SkillName->Clear();

	ActionDataInfo datainfo;
	m_SkillInfo->SetItemData(datainfo);
	SetVisible(FALSE);
}
void USkillExplain::InitSkill(ui32 pSpellID, UINT pos)
{
	OnClear();
	if (pSpellID == 0)
	{
		SetVisible(FALSE);
	}else
	{
		SetVisible(TRUE);
		ActionDataInfo datainfo;
		datainfo.Guid = pSpellID;
		datainfo.entry = pSpellID; 
		datainfo.pos = pos;
		datainfo.type = UItemSystem::SPELL_DATA_FLAG;
		
		m_SkillInfo->SetItemData(datainfo);
		SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate(pSpellID);
		
		if (pSpellTemplate)
		{
			m_SkillName->SetText(pSpellTemplate->GetName().c_str());

			/*char buf[_MAX_PATH];
			NiSprintf(buf,_MAX_PATH,"%d级",pSpellTemplate->GetSpellLev());*/
			std::string buff = _TRAN( N_NOTICE_C28, _I2A( pSpellTemplate->GetSpellLev() ).c_str() );
			m_SkillLev->SetText(buff.c_str());
		}
		
	}
}
void USkillExplain::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UProfSkillExplain, UControl);
UBEGIN_MESSAGE_MAP(UProfSkillExplain, UControl)
UON_BN_CLICKED(3, &UProfSkillExplain::OnBtnClickUnLearn)
UEND_MESSAGE_MAP()
void UProfSkillExplain::StaticInit()
{

}
UProfSkillExplain::UProfSkillExplain()
{
	m_SkillLine = 0;
}
UProfSkillExplain::~UProfSkillExplain()
{

}

void UProfSkillExplain::SetSkillEntry(uint32 entry)
{
	UBitmap* pBitmap = UDynamicCast(UBitmap, GetChildByID(104));
	if (!pBitmap)
		return;
	CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
	if (!pkPlayer)
		return;

	UProgressBar_Skin* pProgressBar = UDynamicCast(UProgressBar_Skin, GetChildByID(4));
	if(!pProgressBar)
		return;

	//164 430003011 初级锻造
	//182 430001011 初级岐黄术（采药）
	//186 430000011 初级矿产学（采矿）
	//770 430002011 初级珠宝加工
	//771 430004011 初级炼丹

	m_SkillLine = entry;
	uint32 currentLine = 0;
	uint32 maxLine = 0;
	PlayerProfessionLevel* ppl = pkPlayer->GetProfessionState(entry);
	if (ppl)
	{
		currentLine = ppl->current;
		maxLine = ppl->Max;
		pProgressBar->SetProgressPos(ppl->current, ppl->Max);
	}
	if (entry == 0)
	{
		pProgressBar->SetProgressPos(0.f);
	}
	pProgressBar->SetShowText(entry != 0);
	pBitmap->SetVisible(entry != 0);
	uint32 skillID = 0;
	if (entry == 164)
	{
		pBitmap->SetBitMapByStr("DATA\\UI\\Skill\\duanzao.png");
		skillID = 430003011;
	}
	if (entry == 182)
	{
		pBitmap->SetBitMapByStr("DATA\\UI\\Skill\\qihuang.png");
		skillID = 430001011;
	}
	if (entry == 186)
	{
		pBitmap->SetBitMapByStr("DATA\\UI\\Skill\\kuangchan.png");
		skillID = 430000011;
	}
	if (entry == 770)
	{
		pBitmap->SetBitMapByStr("DATA\\UI\\Skill\\zhubao.png");
		skillID = 430002011;
	}
	if (entry == 771)
	{
		pBitmap->SetBitMapByStr("DATA\\UI\\Skill\\liandan.png");
		skillID = 430004011;
	}

	if (maxLine > 75 && maxLine <= 150)
	{
		skillID += 1;
	}else if(maxLine > 150 && maxLine <= 225)
	{
		skillID += 2;
	}

	USkillExplain* pSkillExplain = UDynamicCast(USkillExplain, GetChildByID(1));
	if (pSkillExplain)
	{
		pSkillExplain->InitSkill(skillID, 0);
	}
	
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(3));
	if (pBtn)
	{
		pBtn->SetActive(entry != 0);
	}
}

BOOL UProfSkillExplain::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(3));
	if (pBtn)
	{
		pBtn->SetActive(FALSE);
	}
	USkillExplain* pSkillExplain = UDynamicCast(USkillExplain, GetChildByID(1));
	if (pSkillExplain)
	{
		pSkillExplain->OnClear();
	}
	pSkillExplain = UDynamicCast(USkillExplain, GetChildByID(2));
	if (pSkillExplain)
	{
		pSkillExplain->OnClear();
	}
	return TRUE;
}
void UProfSkillExplain::OnDestroy()
{
	m_SkillLine = 0;
	UControl::OnDestroy();
}
uint32 g_UnLearnSkillLine = 0;
void UnLearnSkill()
{
	if (g_UnLearnSkillLine)
	{
		PacketBuilder->SendSkillUnLearn(g_UnLearnSkillLine);
		g_UnLearnSkillLine = 0;
	}
}

void Cancel()
{
	g_UnLearnSkillLine = 0;
}

void UProfSkillExplain::OnBtnClickUnLearn()
{
	g_UnLearnSkillLine = m_SkillLine;
	UMessageBox::MsgBox( _TRAN("您确定要遗忘该专业技能么？"), (BoxFunction*)UnLearnSkill, (BoxFunction*)Cancel);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UIManufactureNeedItem, UControl)
void UIManufactureNeedItem::StaticInit()
{

}

UIManufactureNeedItem::UIManufactureNeedItem()
{
	m_ItemBtn = NULL;
	m_ItemName = NULL;
}

UIManufactureNeedItem::~UIManufactureNeedItem()
{

}

BOOL UIManufactureNeedItem::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (m_ItemBtn == NULL)
	{
		m_ItemBtn = UDynamicCast(USkillButton, GetChildByID(0));
		if ( !m_ItemBtn )
		{
			return FALSE;
		}
	}
	USkillButton::DataInfo kData;
	kData.id = 0;
	kData.type = UItemSystem::ITEM_DATA_FLAG;
	kData.pos = 0;
	kData.num = 0;

	m_ItemBtn->SetStateData(&kData);
	m_ItemBtn->SetLockDrag(TRUE);

	if (m_ItemName == NULL)
	{
		m_ItemName = UDynamicCast(UStaticText, GetChildByID(2));
		if (!m_ItemName)
		{
			return FALSE;
		}
	}

	return TRUE;
}

void UIManufactureNeedItem::OnDestroy()
{
	UControl::OnDestroy();
}

void UIManufactureNeedItem::SetManufactureNeedItem(ui32 entry, int num)
{
	if (!m_ItemBtn)
		return;

	USkillButton::DataInfo kData;
	kData.id = entry;
	kData.type = UItemSystem::ITEM_DATA_FLAG;
	kData.pos = 0;
	kData.num = num;
	m_ItemBtn->SetStateData(&kData);

	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(entry);
	if (ItemInfo)
	{
		m_ItemName->SetText(ItemInfo->C_name.c_str());
	}
}

void UIManufactureNeedItem::Clear()
{
	USkillButton::DataInfo kData;
	kData.id = 0;
	kData.type = UItemSystem::ITEM_DATA_FLAG;
	kData.pos = 0;
	kData.num = 0;

	m_ItemBtn->SetStateData(&kData);
	m_ItemName->Clear();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UIManufacture, UDialog)
UBEGIN_MESSAGE_MAP(UIManufacture, UDialog)
UON_UGD_CLICKED(1, &UIManufacture::OnChooseList)
UON_BN_CLICKED(8, &UIManufacture::OnCheckCanDo)
UON_BN_CLICKED(9, &UIManufacture::OnCheckCantDo)
UON_BN_CLICKED(14, &UIManufacture::OnClickManufacture)
UON_BN_CLICKED(16, &UIManufacture::OnIncNum)
UON_BN_CLICKED(15, &UIManufacture::OnDecNum)
UON_BN_CLICKED(18, &UIManufacture::OnClickAllManufacture)
UON_BN_CLICKED(22, &UIManufacture::OnClose)
UEND_MESSAGE_MAP()
void UIManufacture::StaticInit()
{

}

UIManufacture::UIManufacture()
{
	m_ManufactureList = NULL;
	m_NeedItem[0] = NULL;
	m_NeedItem[1] = NULL;
	m_NeedItem[2] = NULL;
	m_NeedItem[3] = NULL;
	m_BuildItemBtn = NULL;
	m_RichEdit = NULL;
	m_BuildItemName = NULL;
	m_VisibleMask = 0;
	m_pNumEdit = NULL;
	m_buildNum = 0;
	m_bContiuneManufacture = false;
	m_LockSpellEntry = 0;
}

UIManufacture::~UIManufacture()
{

}

BOOL UIManufacture::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	if (m_CloseBtn)
	{
		m_CloseBtn->SetPosition(UPoint(396, 31));
	}
	GetChildByID(108)->SetActive(false);
	GetChildByID(109)->SetActive(false);

	UScrollBar* pBar = UDynamicCast(UScrollBar, GetChildByID(0));
	if (pBar == NULL)
	{
		return FALSE;
	}
	pBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);

	m_ManufactureList  = (USpecListBox*)pBar->GetChildByID(1);
	if (m_ManufactureList == NULL)
	{
		return FALSE;
	}
	m_ManufactureList->SetGridSize(UPoint(334,25));
	m_ManufactureList->SetGridCount(7,1);
	m_ManufactureList->SetCol1(0, 0);
	m_ManufactureList->SetWidthCol1(278);
	m_ManufactureList->SetCol2(278, 0);
	m_ManufactureList->SetWidthCol2(56);

	for (int i = 0 ; i < 4 ; i++)
	{
		if (m_NeedItem[i] == NULL)
		{
			m_NeedItem[i] = UDynamicCast(UIManufactureNeedItem, GetChildByID(2 + i));
			if (!m_NeedItem[i])
			{
				return FALSE;
			}
		}
		m_NeedItem[i]->SetVisible(FALSE);
	}

	if (m_BuildItemBtn == NULL)
	{
		m_BuildItemBtn = UDynamicCast(USkillButton, GetChildByID(10));
		if (!m_BuildItemBtn)
		{
			return FALSE;
		}
	}
	USkillButton::DataInfo kData;
	kData.id = 0;
	kData.type = UItemSystem::ITEM_DATA_FLAG;
	kData.pos = 0;
	kData.num = 0;
	m_BuildItemBtn->SetStateData(&kData);
	m_BuildItemBtn->SetLockDrag(TRUE);

	if (m_RichEdit == NULL)
	{
		m_RichEdit = UDynamicCast(URichTextCtrl, GetChildByID(12));
		if (!m_RichEdit)
		{
			return FALSE;
		}
	}

	if (m_BuildItemName == NULL)
	{
		m_BuildItemName = UDynamicCast(UStaticText, GetChildByID(11));
		if (!m_BuildItemName)
		{
			return FALSE;
		}
	}
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(8));
	if(pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	pBtn = UDynamicCast(UButton, GetChildByID(9));
	if(pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	if(m_pNumEdit == NULL)
	{
		m_pNumEdit = UDynamicCast(UEdit, GetChildByID(17));
		if (!m_pNumEdit)
		{
			return FALSE;
		}
	}
	m_pNumEdit->SetNumberOnly(TRUE);
	m_pNumEdit->SetMaxLength(3);

	UStaticText* pTitle = UDynamicCast(UStaticText, GetChildByID(21));
	if (pTitle)
	{
		pTitle->SetTextEdge(true);
	}

	return TRUE;
}

void UIManufacture::OnDestroy()
{
	m_ShowSpellsList.clear();
	m_SpellsList.clear();
	m_LockSpellEntry = 0;
	m_VisibleMask = 0;
	m_bContiuneManufacture = false;
	UDialog::OnDestroy();
}

BOOL UIManufacture::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}

void UIManufacture::OnClose()
{
	UInGame* pInGame = (UInGame*)UInGame::Get();
	if (!pInGame)
		return;

	UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	if (pGameBar)
	{
		UButton* pBtn =  UDynamicCast(UButton,pGameBar->GetChildByID(UINGAMEBAR_BUTTONSHOP));
		if (pBtn)
			pBtn->SetCheck(TRUE);
	}
}

void UIManufacture::OnTimer(float fDetaltime)
{
	if (!m_Visible)
	{
		m_bContiuneManufacture = false;
		return;
	}
	UDialog::OnTimer(fDetaltime);
	if (m_bContiuneManufacture  && m_buildNum > 0)
	{
		LockBuild();
		m_buildNum--;
		Build();
	}
}

void UIManufacture::OnCheckCanDo()
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(8));
	if(pBtn)
	{
		GetChildByID(108)->SetActive(pBtn->IsChecked());
		if (pBtn->IsChecked())	{
			m_VisibleMask += 1;
		}else{
			m_VisibleMask -= 1;
		}
		UpdateVisibleList();
		UpdateManuFacture();
	}
}

void UIManufacture::OnCheckCantDo()
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(9));
	if(pBtn)
	{
		GetChildByID(109)->SetActive(pBtn->IsChecked());
		if (pBtn->IsChecked())	{
			m_VisibleMask += 2;
		}else{
			m_VisibleMask -= 2;
		}
		UpdateVisibleList();
		UpdateManuFacture();
	}
}

void UIManufacture::OnChooseList()
{
	void* pDate = m_ManufactureList->GetItemData(m_ManufactureList->GetCurSel());
	if (pDate)
	{
		uint32 SpellEntry = *(uint32*)(pDate);
		uint32 itemEntry = g_pkSpellGambleInfo->GetItemEntry(SpellEntry);
		uint32 minCount = 0, maxCount = 0;
		g_pkSpellGambleInfo->GetItemCount(SpellEntry, &minCount, &maxCount);

		USkillButton::DataInfo kData;
		kData.id = itemEntry;
		kData.type = UItemSystem::ITEM_DATA_FLAG;
		kData.pos = 0;
		kData.num = minCount;
		m_BuildItemBtn->SetStateData(&kData);
		if (SpellEntry != m_LockSpellEntry)
		{
			m_LockSpellEntry = 0;
		}
		ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(itemEntry);
		if (ItemInfo)
		{
			m_BuildItemName->SetText(ItemInfo->C_name.c_str());
		}

		stSpellInfo* info = g_pkSpellInfo->GetSpellInfo( SpellEntry );
		std::string NeedItem;
		char buf[256];
		if (info)
		{
			for (int i = 0 ; i < 3; i++)
			{
				if (info->uiEffectMiscValue[i])
				{
					GetChildByID(13)->SetVisible(TRUE);
					m_NeedItem[i]->SetVisible(TRUE);
					m_NeedItem[i]->SetManufactureNeedItem(info->uiEffectMiscValue[i],  info->uiEffectBasePoint[i]);
					ItemInfo = ItemMgr->GetItemPropertyFromDataBase(info->uiEffectMiscValue[i]);
					if (ItemInfo)
					{
						if (NeedItem.length())
							NeedItem += ",";
						sprintf(buf, "%s*%u", ItemInfo->C_name.c_str(), info->uiEffectBasePoint[i]);
						NeedItem = NeedItem +  buf;
					}
				}else
				{
					m_NeedItem[i]->SetVisible(FALSE);
				}
			}
		}
		NeedItem = _TRAN("详细") + string(":")+NeedItem;
		m_RichEdit->SetText(NeedItem.c_str(), NeedItem.length());

		uint32 bNum = TestManuFacureNum(SpellEntry);
		//GetChildByID(18)->SetActive(bNum);
		//GetChildByID(14)->SetActive(bNum);
	}
}

void UIManufacture::OnClickAllManufacture()
{
	int curSel = m_ManufactureList->GetCurSel();
	if (curSel == -1)
		return;
	void* pDate = m_ManufactureList->GetItemData(curSel);
	if (pDate)
	{
		uint32 SpellEntry = *(uint32*)(pDate);
		m_LockSpellEntry = SpellEntry;
		char buf[256];
		m_buildNum = TestManuFacureNum(m_LockSpellEntry);
		m_pNumEdit->SetText(itoa(m_buildNum, buf, 10));
		m_buildNum++;
		UnLockBuild();
	}
}

void UIManufacture::OnClickManufacture()
{
	int curSel = m_ManufactureList->GetCurSel();
	if (curSel == -1)
		return;
	void* pDate = m_ManufactureList->GetItemData(curSel);
	if (pDate)
	{
		uint32 SpellEntry = *(uint32*)(pDate);
		m_LockSpellEntry = SpellEntry;

		char buf[256];
		m_pNumEdit->GetText(buf, 256, 0);
		m_buildNum = atoi(buf);
		m_buildNum = min(m_buildNum, TestManuFacureNum(m_LockSpellEntry));
		if (m_buildNum == 0)
			m_buildNum = 1;
		m_pNumEdit->SetText(itoa(m_buildNum, buf, 10));
		m_buildNum++;
		UnLockBuild();
	}
}

void UIManufacture::LockBuild()
{
	m_bContiuneManufacture = false;
}

void UIManufacture::Build()
{
	if (!m_LockSpellEntry /*|| !InputTestManuFacture()*/)
		return;

	char buf[256];
	if(m_pNumEdit)
		m_pNumEdit->SetText(itoa(m_buildNum, buf, 10));

	if (m_buildNum <= 0)
		return;

	CPlayerInputMgr * InputMsg = SYState()->LocalPlayerInput;
	if (InputMsg)
	{
		if ( InputMsg->IsSelectingTarget() )
		{
			InputMsg->EndSelectTarget();
			return;
		}
		InputMsg->BeginSkillAttack(m_LockSpellEntry);
	}
}

void UIManufacture::UnLockBuild()
{
	m_bContiuneManufacture = true;
}

void UIManufacture::OnIncNum()
{
	char buf[256];
	m_pNumEdit->GetText(buf, 256, 0);
	m_buildNum = atoi(buf);
	m_buildNum++;
	m_pNumEdit->SetText(itoa(m_buildNum, buf, 10));
}

void UIManufacture::OnDecNum()
{
	char buf[256];
	m_pNumEdit->GetText(buf, 256, 0);
	m_buildNum = atoi(buf);

	if (m_buildNum > 0)
	{
		m_buildNum--;
		m_pNumEdit->SetText(itoa(m_buildNum, buf, 10));
	}
}

void UIManufacture::SetSkillList(vector<ui32> &pSpells)
{
	m_SpellsList = pSpells;
	UpdateVisibleList();
	UpdateManuFacture();
}

void UIManufacture::AddNewSkill(ui32 pSpell)
{
	m_SpellsList.push_back(pSpell);
	UpdateVisibleList();
	UpdateManuFacture();
}

bool UIManufacture::InputTestManuFacture()
{
	return InputTestSpellUseItem(m_LockSpellEntry);
}

void UIManufacture::UpdateVisibleList()
{
	m_ShowSpellsList.clear();
	for (unsigned int ui = 0 ; ui < m_SpellsList.size() ; ++ui)
	{
		uint32 BuildNum = TestManuFacureNum( m_SpellsList[ui] );
		if ( BuildNum )
		{
			if (m_VisibleMask & 1)
			{
				m_ShowSpellsList.push_back(m_SpellsList[ui]);
			}
		}else if(m_VisibleMask & 2)
		{
			m_ShowSpellsList.push_back(m_SpellsList[ui]);
		}
	}
}

void UIManufacture::UpdateManuFacture()
{
	m_ManufactureList->Clear();

	char pServer[255];
	int SelIndex = -1;
	USkillButton::DataInfo* kData = m_BuildItemBtn->GetStateData();
	for (unsigned int ui = 0 ; ui < m_ShowSpellsList.size() ; ui++)
	{
		SpellTemplate* pkSpellTem = SYState()->SkillMgr->GetSpellTemplate(m_ShowSpellsList[ui]);
		if (!pkSpellTem)
			continue;
		uint32 BuildNum = TestManuFacureNum( m_ShowSpellsList[ui] );
		if(BuildNum)
		{
			NiSprintf(pServer, 255, "<Col1><color:R000G255B000A255><center>%s%s<end_left><Col2><color:R000G255B000A255><center>%u<end>", _TRAN("合成："),pkSpellTem->GetName().c_str() , BuildNum);
		}else
		{
			NiSprintf(pServer, 255, "<Col1><color:R128G128B128A255><center>%s%s<end_left>", _TRAN("合成："), pkSpellTem->GetName().c_str());
		}
		ui32* spellentry = new ui32(m_ShowSpellsList[ui]);
		m_ManufactureList->AddItem(pServer, spellentry);
		if (kData &&  g_pkSpellGambleInfo->GetItemEntry(m_ShowSpellsList[ui]) == kData->id)
		{
			SelIndex = (int)ui;
		}
	}
	if (SelIndex != -1)
	{
		m_ManufactureList->SetCurSel(SelIndex);
	}else
	{
		ClearBuild();
	}
}

void UIManufacture::ClearBuild()
{
	USkillButton::DataInfo kData;
	kData.id = 0;
	kData.type = UItemSystem::ITEM_DATA_FLAG;
	kData.pos = 0;
	kData.num = 0;
	m_BuildItemBtn->SetStateData(&kData);
	
	m_BuildItemName->Clear();

	m_RichEdit->ClearText();
	
	GetChildByID(13)->SetVisible(FALSE);
	
	m_NeedItem[0]->SetVisible(FALSE);
	m_NeedItem[1]->SetVisible(FALSE);
	m_NeedItem[2]->SetVisible(FALSE);
	m_NeedItem[3]->SetVisible(FALSE);

	//GetChildByID(18)->SetActive(FALSE);
	//GetChildByID(14)->SetActive(FALSE);
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(USkillTrainingInfo, UControl);
void USkillTrainingInfo::StaticInit()
{

}

USkillTrainingInfo::USkillTrainingInfo()
{

}

USkillTrainingInfo::~USkillTrainingInfo()
{

}

BOOL USkillTrainingInfo::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	USkillButton* ItemBtn = UDynamicCast(USkillButton, GetChildByID(0));
	if (ItemBtn)
	{
		USkillButton::DataInfo kData;
		kData.id = 0;
		kData.type = UItemSystem::SPELL_DATA_FLAG;
		kData.pos = 0;
		kData.num = 0;
		ItemBtn->SetStateData(&kData);
		ItemBtn->SetLockDrag(TRUE);
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(1));
	if (pText)
	{
		pText->SetTextColor("430038");
	}
	pText = UDynamicCast(UStaticText, GetChildByID(3));
	if (pText)
	{
		pText->SetTextColor("430038");
	}
	return TRUE;
}

void USkillTrainingInfo::OnDestroy()
{
	UControl::OnDestroy();
}

void USkillTrainingInfo::ShowInfo(ui32 spell_ID, ui32 cost, ui32 Require_Spell, ui32 Require_level, ui32 Require_Skill, ui32 Require_SkillLevel)
{
	SetVisible(TRUE);
	SpellTemplate* pkSpellTem = SYState()->SkillMgr->GetSpellTemplate(spell_ID);
	SpellTemplate* pkRequireSpellTem = SYState()->SkillMgr->GetSpellTemplate(Require_Spell);
	if (!pkSpellTem)
		return;

	USkillButton::DataInfo kData;
	kData.id = spell_ID;
	kData.type = UItemSystem::SPELL_DATA_FLAG;
	kData.pos = 0;
	kData.num = 0;
	if(g_SkillLineNameInfo->GetSkillLineType(pkSpellTem->GetLineNameEntry()) == 11 && pkSpellTem->GetSubLineNameEntry() != 0)
	{
		if (spell_ID == PROFESSION_DUANZAO_ID
			|| spell_ID == PROFESSION_CAIYAO_ID
			|| spell_ID == PROFESSION_CAIKUANG_ID
			|| spell_ID == PROFESSION_ZHUBAO_ID
			|| spell_ID == PROFESSION_LIANDAN_ID
			||spell_ID == PROFESSION_DUANZAO_ID + 1
			|| spell_ID == PROFESSION_CAIYAO_ID + 1
			|| spell_ID == PROFESSION_CAIKUANG_ID + 1
			|| spell_ID == PROFESSION_ZHUBAO_ID + 1
			|| spell_ID == PROFESSION_LIANDAN_ID + 1
			||spell_ID == PROFESSION_DUANZAO_ID+2
			|| spell_ID == PROFESSION_CAIYAO_ID+ 2
			|| spell_ID == PROFESSION_CAIKUANG_ID+ 2
			|| spell_ID == PROFESSION_ZHUBAO_ID + 2
			|| spell_ID == PROFESSION_LIANDAN_ID + 2
			)
		{
		}
		else
		{
			uint32 itemEntry = g_pkSpellGambleInfo->GetItemEntry(spell_ID);
			kData.id = itemEntry;
			kData.type = UItemSystem::ITEM_DATA_FLAG;
		}
	}

	USkillButton* ItemBtn = UDynamicCast(USkillButton, GetChildByID(0));
	if (ItemBtn)
	{
		ItemBtn->SetStateData(&kData);
	}

	char buf[1024];
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(1));
	if (pText)
	{
		sprintf(buf, "%s (%s)", pkSpellTem->GetName().c_str(), _TRAN(N_NOTICE_Z100, _I2A(pkSpellTem->GetSpellLev()).c_str()).c_str());
		pText->SetText(buf);
		pText->SetTextAlign(UT_LEFT);
	}

	URichTextCtrl* pRichText = UDynamicCast(URichTextCtrl, GetChildByID(2));
	if (pRichText)
	{
		uint32 LineClass = Require_Skill;
		//switch (Require_Skill)
		//{
		//case PROFESSION_DUANZAO_ID:
		//case PROFESSION_DUANZAO_ID + 1:
		//case PROFESSION_DUANZAO_ID + 2:
		//	LineClass = 164;
		//	break;
		//case PROFESSION_CAIYAO_ID:
		//case PROFESSION_CAIYAO_ID + 1:
		//case PROFESSION_CAIYAO_ID + 2:
		//	LineClass = 182;
		//	break;
		//case PROFESSION_CAIKUANG_ID:
		//case PROFESSION_CAIKUANG_ID + 1:
		//case PROFESSION_CAIKUANG_ID + 2:
		//	LineClass = 186;
		//	break;
		//case PROFESSION_ZHUBAO_ID:
		//case PROFESSION_ZHUBAO_ID + 1:
		//case PROFESSION_ZHUBAO_ID + 2:
		//	LineClass = 770;
		//	break;
		//case PROFESSION_LIANDAN_ID:
		//case PROFESSION_LIANDAN_ID + 1:
		//case PROFESSION_LIANDAN_ID + 2:
		//	LineClass = 771;
		//	break;
		//}
		std::string name;
		g_SkillLineNameInfo->GetSkillLineName(LineClass, name);
		if (Require_Spell == 0)
		{
			if (name.size())
			{
				sprintf(buf, "<color:430038><font:arial:14>%s(%d),%s(%d)", _TRAN("需要：人物等级"), Require_level, name.c_str(), Require_SkillLevel);
			}else
			{
				sprintf(buf, "<color:430038><font:arial:14>%s(%d)", _TRAN("需要：人物等级"), Require_level);
			}
			pRichText->SetText(buf, strlen(buf));
		}
		else if(pkRequireSpellTem)
		{
			if (name.size())
			{
				sprintf(buf, "<color:430038><font:arial:14>%s(%d),%s (%s),%s(%d)", _TRAN("需要：人物等级"), Require_level, pkRequireSpellTem->GetName().c_str(), _TRAN(N_NOTICE_Z100, _I2A(pkRequireSpellTem->GetSpellLev()).c_str()).c_str(), name.c_str(), Require_SkillLevel);
			}else
			{	
				sprintf(buf, "<color:430038><font:arial:14>%s(%d),%s (%s)", _TRAN("需要：人物等级"), Require_level, pkRequireSpellTem->GetName().c_str(), _TRAN(N_NOTICE_Z100, _I2A(pkRequireSpellTem->GetSpellLev()).c_str()).c_str());
			}
			pRichText->SetText(buf, strlen(buf));
		}
	}

	UMoneyText* pMoney = UDynamicCast(UMoneyText, GetChildByID(4));
	if (pMoney)
	{
		pMoney->SetMoney(cost);
		pMoney->SetMoneyTextAlign(UT_LEFT);
	}

	pRichText = UDynamicCast(URichTextCtrl, GetChildByID(5));
	if (pRichText)
	{
		std::string desc;
		if(pkSpellTem->GetDescription(desc))
		{
			desc = "<color:2a1706><font:arial:14>" + desc;
			pRichText->SetText(desc.c_str(), desc.length());
		}
	}
}

//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(USkillTrainingDlg, UDialog);
UBEGIN_MESSAGE_MAP(USkillTrainingDlg, UDialog)
UON_UGD_CLICKED(1, &USkillTrainingDlg::OnChooseList)
UON_UGD_CLICKED(2, &USkillTrainingDlg::OnChooseClass)
UON_BN_CLICKED(8, &USkillTrainingDlg::OnCheckCanDo)
UON_BN_CLICKED(9, &USkillTrainingDlg::OnCheckCantDo)
UON_BN_CLICKED(22, &USkillTrainingDlg::OnClickBtnLearn)
UON_BN_CLICKED(23, &USkillTrainingDlg::OnClickBtnExit)
UEND_MESSAGE_MAP()
void USkillTrainingDlg::StaticInit(){}
USkillTrainingDlg::USkillTrainingDlg()
{
	m_SkillTrainingInfo = NULL;
	m_pListBox = NULL;
	m_SpellList = NULL;
	m_GUID = 0;
	m_VisibleMask = 0;
	m_LocalMoney = 0;
}

USkillTrainingDlg::~USkillTrainingDlg()
{

}

BOOL USkillTrainingDlg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	if (m_SkillTrainingInfo == NULL)
	{
		m_SkillTrainingInfo = UDynamicCast(USkillTrainingInfo, GetChildByID(3));
		if (!m_SkillTrainingInfo)
		{
			return FALSE;
		}
	}
	if (m_pListBox == NULL)
	{
		m_pListBox = UDynamicCast(UListBox, GetChildByID(2));
		if (!m_pListBox)
		{
			return FALSE;
		}
	}
	m_pListBox->SetGridSize(UPoint(89, 23));
	
	UScrollBar* pBar = UDynamicCast(UScrollBar, GetChildByID(0));
	if (pBar == NULL)
	{
		return FALSE;
	}
	pBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);

	m_SpellList  = UDynamicCast(USpecListBox, pBar->GetChildByID(1));
	if (m_SpellList == NULL)
	{
		return FALSE;
	}
	m_SpellList->SetGridSize(UPoint(260, 17));
	m_SpellList->SetGridCount(10,1);
	m_SpellList->SetCol1(0, 0);
	m_SpellList->SetWidthCol1(171);
	m_SpellList->SetCol2(171, 0);
	m_SpellList->SetWidthCol2(89);

	m_StoreClassName = _TRAN("全部");
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(8));
	if(pBtn)
	{
		pBtn->SetCheck(FALSE);
	}

	pBtn = UDynamicCast(UButton, GetChildByID(9));
	if(pBtn)
	{
		pBtn->SetCheck(FALSE);
	}

	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(21));
	if (pText)
	{
		pText->SetTextEdge(true);
	}

	//vector<MSG_S2C::stNPC_Trainer_List::stTrainerSpell> vSpells;
	//MSG_S2C::stNPC_Trainer_List::stTrainerSpell temp;
	//for(int i = 0 ; i <  10 ; i++)
	//{
	//	temp.name = _TRAN("斩虚");
	//	temp.LearnSpell_id = 10901 + i;
	//	temp.RequiredLevel = 10 * (i + 1);
	//	temp.Cost = 12000 + 10000 * i;
	//	vSpells.push_back(temp);
	//}
	//ParseTraingingList(0, 0, vSpells, std::string("123"));
	pText = UDynamicCast(UStaticText, GetChildByID(108));
	if (pText)
	{
		pText->SetTextAlign(UT_RIGHT);
		pText->SetTextColor("00FF00");
	}
	pText = UDynamicCast(UStaticText, GetChildByID(109));
	if (pText)
	{
		pText->SetTextAlign(UT_RIGHT);
		pText->SetTextColor("FF0000");
	}
	return TRUE;
}

void USkillTrainingDlg::OnDestroy()
{
	m_StoreClassName = _TRAN("全部");
	m_SpellClassMap.clear();
	m_GUID = 0;
	m_VisibleMask = 0;
	m_LocalMoney = 0;
	UDialog::OnDestroy();
}

BOOL USkillTrainingDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}

void USkillTrainingDlg::OnClose()
{
	SetVisible(FALSE);
	m_GUID = 0;
	m_SkillTrainingInfo->SetVisible(FALSE);
}
void USkillTrainingDlg::OnTimer(float fDeltaTime)
{
	//m_Visible = true;

	if (m_GUID)
	{
		CCreature* pCreature = (CCreature*)ObjectMgr->GetObject(m_GUID);
		CPlayerLocal* pLocalPlayer = ObjectMgr->GetLocalPlayer();
		if (pLocalPlayer == NULL || pCreature == NULL)
		{
			return ;
		}else
		{
			NiPoint3 pCreaturePos = pCreature->GetPosition();
			NiPoint3 pLocalPlayerPos = pLocalPlayer->GetLocalPlayerPos();
			if ((pLocalPlayerPos - pCreaturePos).Length() > MaxLenToNPC)
			{
				OnClose();
			}
		}
	}
}

void USkillTrainingDlg::OnClickBtnLearn()
{
	CPlayerLocal* pLocalplayer = ObjectMgr->GetLocalPlayer();
	int cursel = m_SpellList->GetCurSel();
	if (cursel >= 0 && m_SpellList->GetItemCount() && pLocalplayer)
	{
		stSpellTrainerInfo* pSpell = (stSpellTrainerInfo*)m_SpellList->GetItemData(cursel);
		if(pSpell)
		{
			if (pSpell->spellcost > m_LocalMoney)
			{
				UMessageBox::MsgBox(_TRAN("你没有足够的金钱!!"));
				return;
			}
			SpellTemplate* pkSpellTem = SYState()->SkillMgr->GetSpellTemplate(pSpell->learn_spell);
			if (pkSpellTem)
			{
				std::vector<uint32> vSpellEntry;
				pLocalplayer->GetProfessionEntryList(vSpellEntry);
				if(g_SkillLineNameInfo->GetSkillLineType(pkSpellTem->GetLineNameEntry()) == 11 &&  !pLocalplayer->IsLearnProfession(pkSpellTem->GetLineNameEntry()) &&vSpellEntry.size() >= 2)
				{
					UMessageBox::MsgBox(_TRAN("只能学习两个专业技能!!"));
					return;
				}
			}
			PacketBuilder->SendNPCTrainerBuySpell(m_GUID, pSpell->learn_spell);
		}
	}
}

void USkillTrainingDlg::OnClickBtnExit()
{
	OnClose();
}

void USkillTrainingDlg::OnChooseList()
{
	CPlayerLocal* pLocalplayer = ObjectMgr->GetLocalPlayer();
	int cursel = m_SpellList->GetCurSel();
	stSpellTrainerInfo* pSpell = (stSpellTrainerInfo*)m_SpellList->GetItemData(cursel);
	if(pSpell)
	{
		if (CanLearnSpell(pSpell))
		{
			m_SpellList->SetBitmap("Skill\\SkillTrainingSpecList.skin");
		}
		else
		{
			m_SpellList->SetBitmap("Skill\\SkillTrainingSpecList1.skin");
		}
		m_SkillTrainingInfo->ShowInfo(pSpell->learn_spell, pSpell->spellcost, pSpell->require_spell, pSpell->require_Level, pSpell->require_skill, pSpell->require_Skillvalue);
	}
}

void USkillTrainingDlg::OnChooseClass()
{
	char buf[256];
	int bRet = m_pListBox->GetSelectedText(buf, 256);
	if(bRet)
	{
		m_StoreClassName = buf;
		UpdateSpellList();
	}
}

void USkillTrainingDlg::OnCheckCanDo()
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(8));
	if(pBtn)
	{
		GetChildByID(108)->SetActive(pBtn->IsChecked());
		if (pBtn->IsChecked())	{
			m_VisibleMask += 1;
		}else{
			m_VisibleMask -= 1;
		}
		UpdateSpellList();
	}
}

void USkillTrainingDlg::OnCheckCantDo()
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(9));
	if(pBtn)
	{
		GetChildByID(109)->SetActive(pBtn->IsChecked());
		if (pBtn->IsChecked())	{
			m_VisibleMask += 2;
		}else{
			m_VisibleMask -= 2;
		}
		UpdateSpellList();
	}
}

void USkillTrainingDlg::ParseTrainingAck(uint64 guid, uint32 TrainerType, uint32 ID)
{
	std::vector<stSpellTrainerInfo> vinfo;
	std::string str;
	if (g_pkSpellTrainerInfo->GetSpellTrainerList(ID, vinfo))
	{
		ParseTraingingList(guid, TrainerType, vinfo, str);
	}
}

void USkillTrainingDlg::ParseTraingingList( uint64 guid, uint32 TrainerType, 	vector<stSpellTrainerInfo>& vSpells,	std::string message )
{
	m_GUID = guid;
	SetVisible(TRUE);
	m_SpellClassMap.clear();
	for (unsigned int ui = 0 ; ui < vSpells.size() ; ++ui)
	{
		AddClassMap(vSpells[ui].name, vSpells[ui]);
	}
	m_StoreClassName = _TRAN("全部");
	UpdateList();
}

void USkillTrainingDlg::AddClassMap(std::string& str, stSpellTrainerInfo& TrainerSpell)
{
	SpellClassMap::iterator it =  m_SpellClassMap.find(str);
	if (it != m_SpellClassMap.end())
	{
		it->second.push_back(TrainerSpell);
		return;
	}
	std::vector<stSpellTrainerInfo> newlist;
	newlist.push_back(TrainerSpell);
	m_SpellClassMap.insert(SpellClassMap::value_type(str, newlist));
}

void USkillTrainingDlg::UpdateList()
{
	m_pListBox->Clear();
	m_pListBox->AddItem(_TRAN("全部"));
	SpellClassMap::iterator it =  m_SpellClassMap.begin();
	while(it != m_SpellClassMap.end())
	{
		m_pListBox->AddItem(it->first.c_str());
		++it;
	}
	UpdateSpellList();
	m_pListBox->SetCurSel(0);
}

bool USkillTrainingDlg::CanLearnSpell(stSpellTrainerInfo* SpellInfo)
{
	CPlayerLocal* pLocalplayer = ObjectMgr->GetLocalPlayer();
	if (!pLocalplayer || !SpellInfo)
		return false;
	
	SpellTemplate* pkSpellTem = SYState()->SkillMgr->GetSpellTemplate(SpellInfo->learn_spell);
	if (!pkSpellTem)
		return false;
	bool canLearn = true;
	if (SpellInfo->require_skill)
	{
		uint32 LineClass = SpellInfo->require_skill;
		PlayerProfessionLevel* pProf = pLocalplayer->GetProfessionState(LineClass);
		if (pProf)
		{
			canLearn = pProf->current >= SpellInfo->require_Skillvalue;
		}
		else
		{
			canLearn = 1>= SpellInfo->require_Skillvalue;
		}
	}
	//是否学习了前置技能，前置技能如果是0也是可以学习的
	bool bLearnspell = (pLocalplayer->HasLearnSkill(SpellInfo->require_spell) || SpellInfo->require_spell == 0);
	//是否学习了前置专业，前置专业如果是0也是可以学习的
	bool bLearnSkill = (pLocalplayer->IsLearnProfession(SpellInfo->require_skill) || SpellInfo->require_skill == 0);
	//是否满足人物等级，是否满足专业技能等级
	bool bRquire = pLocalplayer->GetLevel() >= SpellInfo->require_Level && canLearn;

	return bLearnspell & bRquire & bLearnSkill;
}

void USkillTrainingDlg::UpdateSpellList()
{
	CPlayerLocal* pLocalplayer = ObjectMgr->GetLocalPlayer();
	if (!pLocalplayer)
		return;
	m_SpellList->Clear();
	SpellClassMap::iterator it =  m_SpellClassMap.begin();
	int NextChoose = 0;
	int count = 0;
	bool bChoose = false;
	while(it != m_SpellClassMap.end())
	{
		if(m_StoreClassName.compare(it->first.c_str()) == 0 || m_StoreClassName.compare(_TRAN("全部")) == 0)
		{
			for (unsigned int ui = 0 ; ui < it->second.size() ; ++ui)
			{
				SpellTemplate* pkSpellTem = SYState()->SkillMgr->GetSpellTemplate(it->second[ui].learn_spell);
				if (!pkSpellTem)
					return;
				char buf[256];
				if (CanLearnSpell(&it->second[ui]))
				{
					if (m_VisibleMask & 1 )
					{
						if (!pLocalplayer->HasLearnSkill(it->second[ui].learn_spell))
						{
							if (!bChoose)
							{
								NextChoose = count;
								bChoose = true;
							}

							NiSprintf(buf, 255, "<Col1><color:R000G255B000A255><center>%s<end_left><Col2><color:R000G255B000A255><center>%s<end>"
								,pkSpellTem->GetName().c_str() , _TRAN(N_NOTICE_Z100, _I2A(pkSpellTem->GetSpellLev()).c_str()).c_str());
							m_SpellList->AddItem(buf, &it->second[ui]);
							++count;
						}
					}
				}else 
				{
					if (m_VisibleMask & 2)
					{
						if ( !pLocalplayer->HasLearnSkill(it->second[ui].learn_spell) )
						{
							++count;
							NiSprintf(buf, 255, "<Col1><color:R255G000B000A255><center>%s<end_left><Col2><color:R255G000B000A255><center>%s<end>"
								,pkSpellTem->GetName().c_str() , _TRAN(N_NOTICE_Z100, _I2A(pkSpellTem->GetSpellLev()).c_str()).c_str());
							m_SpellList->AddItem(buf, &it->second[ui]);
						}
					}
				}
			}
		}
		++it;
	}
	if (m_SpellList->GetItemCount())
	{
		m_SpellList->SetCurSel(NextChoose);
	}
	else
	{
		m_SkillTrainingInfo->SetVisible(FALSE);
	}
}
void USkillTrainingDlg::ShowMoneyNum(UINT  Num)
{
	if (Num <= 0)
		Num = 0 ;

	UMoneyText* pMoney = UDynamicCast(UMoneyText, GetChildByID(24));
	if (pMoney)
	{
		pMoney->SetMoney(Num);
		pMoney->SetMoneyTextAlign(UT_RIGHT);
	}
	m_LocalMoney = Num;
}
UIMP_CLASS(USpecialityNeedItemInfo, UControl);
void USpecialityNeedItemInfo::StaticInit(){}
USpecialityNeedItemInfo::USpecialityNeedItemInfo()
{

}

USpecialityNeedItemInfo::~USpecialityNeedItemInfo()
{

}

BOOL USpecialityNeedItemInfo::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	USkillButton* ItemBtn = UDynamicCast(USkillButton, GetChildByID(1));
	if (ItemBtn)
	{
		USkillButton::DataInfo kData;
		kData.id = 0;
		kData.type = UItemSystem::ITEM_DATA_FLAG;
		kData.pos = 0;
		kData.num = 0;
		ItemBtn->SetStateData(&kData);
		ItemBtn->SetLockDrag(TRUE);
	}

	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(2));
	if (pText)
	{
		pText->SetTextAlign(UT_LEFT);
		pText->SetTextEdge(true);
	}
	return TRUE;
}

void USpecialityNeedItemInfo::OnDestroy()
{
	UControl::OnDestroy();
}

void USpecialityNeedItemInfo::SetItem(uint32 ItemEntry, uint32 num)
{
	USkillButton* ItemBtn = UDynamicCast(USkillButton, GetChildByID(1));
	int curNum = GetPackageItemNum(ItemEntry);
	if (ItemBtn)
	{
		USkillButton::DataInfo kData;
		kData.id = ItemEntry;
		kData.type = UItemSystem::ITEM_DATA_FLAG;
		kData.pos = 0;
		kData.num = num;
		kData.CurNum = curNum;
		ItemBtn->SetStateData(&kData);
		ItemBtn->SetLockDrag(TRUE);
	}

	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(ItemEntry);
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(2));
	if (pText && ItemInfo)
	{
		pText->SetText(ItemInfo->C_name.c_str());
		if (curNum < num)
		{
			pText->SetTextColor("808080");
		}else
			pText->SetTextColor("FFFFFF");
		pText->SetTextShadow(true);
	}
}

UIMP_CLASS(USpecialityInfo, UControl);
void USpecialityInfo::StaticInit(){}
USpecialityInfo::USpecialityInfo()
{
	for (int i = 0 ; i < 3; i++)
	{
		m_NeedItem[i] = NULL;
	}
}

USpecialityInfo::~USpecialityInfo()
{

}

BOOL USpecialityInfo::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	USkillButton* ItemBtn = UDynamicCast(USkillButton, GetChildByID(0));
	if (ItemBtn)
	{
		USkillButton::DataInfo kData;
		kData.id = 0;
		kData.type = UItemSystem::ITEM_DATA_FLAG;
		kData.pos = 0;
		kData.num = 0;
		ItemBtn->SetStateData(&kData);
		ItemBtn->SetLockDrag(TRUE);
	}

	for (int i = 0 ; i < 3; i++)
	{
		if (m_NeedItem[i] == NULL)
		{
			m_NeedItem[i] = UDynamicCast(USpecialityNeedItemInfo, GetChildByID(i + 3));
			if (!m_NeedItem[i])
			{
				return FALSE;
			}
		}
	}

	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(1));
	if (pText)
	{
		pText->SetTextAlign(UT_LEFT);
		pText->SetTextColor("00ff00");
		pText->SetTextEdge(true);
	}
	return TRUE;
}
void USpecialityInfo::OnTimer(float fDetaltime)
{
	if (m_Visible && m_spellID)
	{
		SetInfoTimer();
	}
}


void USpecialityInfo::OnDestroy()
{
	m_spellID = 0;
	UControl::OnDestroy();
}
void MakeIntStringNoZero1(char * buf, int num)
{
	//itoa(num,buf,10);
	sprintf(buf,"%u",num);
}

string ConvertTimeStampToString1(uint32 timestamp)
{
	int seconds = (int)timestamp;
	int mins=0;
	int hours=0;
	int days=0;
	int months=0;
	int years=0;
	if(seconds >= 60)
	{
		mins = seconds / 60;
		if(mins)
		{
			seconds -= mins*60;
			if(mins >= 60)
			{
				hours = mins / 60;
				if(hours)
				{
					mins -= hours*60;
					if(hours >= 24)
					{
						days = hours/24;
						if(days)
						{
							hours -= days*24;
							if(days >= 30)
							{
								months = days / 30;
								if(months)
								{
									days -= months*30;
									if(months >= 12)
									{
										years = months / 12;
										if(years)
										{
											months -= years*12;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	char szTempBuf[100];
	string szResult;

	if(years) {
		MakeIntStringNoZero1(szTempBuf, years);
		szResult += szTempBuf;
		szResult += _TRAN(" 年, ");
	}

	if(months) {
		MakeIntStringNoZero1(szTempBuf, months);
		szResult += szTempBuf;
		szResult += _TRAN(" 月, ");
	}

	if(days) {
		MakeIntStringNoZero1(szTempBuf, days);
		szResult += szTempBuf;
		szResult += _TRAN(" 日, ");
	}

	if(hours) {
		MakeIntStringNoZero1(szTempBuf, hours);
		szResult += szTempBuf;
		szResult += _TRAN(" 小时, ");
	}

	if(mins) {
		MakeIntStringNoZero1(szTempBuf, mins);
		szResult += szTempBuf;
		szResult += _TRAN(" 分钟, ");
	}

	if(seconds) {
		MakeIntStringNoZero1(szTempBuf, seconds);
		szResult += szTempBuf;
		szResult += _TRAN(" 秒");
	}

	return szResult;
}
void USpecialityInfo::SetSpecialitySpellId(uint32 spellID)
{
	m_spellID = spellID;
	SetVisible(spellID > 0);
	uint32 itemEntry = g_pkSpellGambleInfo->GetItemEntry(spellID);
	uint32 minCount = 0, maxCount = 0;
	g_pkSpellGambleInfo->GetItemCount(spellID, &minCount, &maxCount);

	USkillButton* ItemBtn = UDynamicCast(USkillButton, GetChildByID(0));
	if (ItemBtn)
	{
		USkillButton::DataInfo kData;
		kData.id = itemEntry;
		kData.type = UItemSystem::ITEM_DATA_FLAG;
		kData.pos = 0;
		kData.num = minCount;
		ItemBtn->SetStateData(&kData);
	}

	stSpellInfo* info = g_pkSpellInfo->GetSpellInfo( spellID );
	std::string NeedItem;
	if (info)
	{
		for (int i = 0 ; i < 3; i++)
		{
			if (info->uiEffectMiscValue[i])
			{
				m_NeedItem[i]->SetVisible(TRUE);
				m_NeedItem[i]->SetItem(info->uiEffectMiscValue[i],  info->uiEffectBasePoint[i]);
				//ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(info->uiEffectMiscValue[i]);
				//if (ItemInfo)
				//{
				//	if (NeedItem.length())
				//		NeedItem += ",";
				//	sprintf(buf, "%s*%u", ItemInfo->C_name.c_str(), info->uiEffectBasePoint[i]);
				//	NeedItem = NeedItem +  buf;
				//}
			}else
			{
				m_NeedItem[i]->SetVisible(FALSE);
			}
		}
	}
	URichTextCtrl* pRichText = UDynamicCast(URichTextCtrl, GetChildByID(2));
	if (pRichText)
	{
		SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate(spellID);
		if (pSpellTemplate)
		{
			pSpellTemplate->GetDescription(NeedItem);
		}
		NeedItem = string("<color:430038><font:arial:12>") +_TRAN("详细") + string(":")+ NeedItem;
		pRichText->SetText(NeedItem.c_str(), NeedItem.size());
	}



	ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(itemEntry);
	if (ItemInfo)
	{
		UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(1));
		if (pText)
		{
			pText->SetText(ItemInfo->C_name.c_str());
		}
	}
}
void USpecialityInfo::SetInfoTimer()
{
	if (m_spellID == 0)
	{
		return;
	}
	ui32 RemainTime;
	ui32 RecoverTime;
	URichTextCtrl* pRichText = UDynamicCast(URichTextCtrl, GetChildByID(6));
	if(SYState()->SkillMgr->GetCooldownTime(m_spellID, RecoverTime, RemainTime) && RemainTime >= 1000)
	{
		if (pRichText)
		{
			std::string time = _TRAN("剩余时间") + string(":") + ConvertTimeStampToString1(uint32(RemainTime * 0.001f));
			time = "<color:ff0000><font:arial:12><shadow:1:1>" + time;
			pRichText->SetText(time.c_str(), time.size());
		}
	}
	else
	{
		if (pRichText)
			pRichText->ClearText();
	}
}


UIMP_CLASS(USpecialityDlg, UDialog);
UBEGIN_MESSAGE_MAP(USpecialityDlg, UDialog)
UON_UGD_CLICKED(1, &USpecialityDlg::OnChooseList)
UON_UGD_CLICKED(2, &USpecialityDlg::OnChooseClassList)
UON_BN_CLICKED(8, &USpecialityDlg::OnCheckCanDo)
UON_BN_CLICKED(9, &USpecialityDlg::OnCheckCantDo)
UON_BN_CLICKED(26, &USpecialityDlg::OnIncNum)
UON_BN_CLICKED(24, &USpecialityDlg::OnDecNum)
UON_BN_CLICKED(27, &USpecialityDlg::OnBtnClickBuild)
UON_BN_CLICKED(28, &USpecialityDlg::OnBtnClickBuildAll)
UON_BN_CLICKED(23, &USpecialityDlg::OnBtnClickCancel)
UEND_MESSAGE_MAP()
void USpecialityDlg::StaticInit(){}

USpecialityDlg::USpecialityDlg()
{
	m_pListBox = NULL;
	m_SpellList = NULL;
	m_SpecialityInfo = NULL;
	m_pNumEdit = NULL;
	m_bContiuneManufacture = false;
	m_LockSpellEntry = 0;
	m_LineClass = 0;
	m_buildNum = 0;
	m_VisibleMask = 0;
	m_ChooseSpellEntry = 0;
}
USpecialityDlg::~USpecialityDlg()
{

}
BOOL USpecialityDlg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_pListBox = UDynamicCast(UListBox, GetChildByID(2));
	if (!m_pListBox)
	{
		return FALSE;
	}
	m_pListBox->SetGridSize(UPoint(89, 23));

	m_StoreClassName = _TRAN("全部");

	UScrollBar* pBar = UDynamicCast(UScrollBar, GetChildByID(0));
	if (pBar == NULL)
	{
		return FALSE;
	}
	pBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);

	m_SpellList  = UDynamicCast(USpecListBox, pBar->GetChildByID(1));
	if (m_SpellList == NULL)
	{
		return FALSE;
	}
	m_SpellList->SetGridSize(UPoint(260, 17));
	m_SpellList->SetGridCount(10,1);
	m_SpellList->SetCol1(0, 0);
	m_SpellList->SetWidthCol1(171);
	m_SpellList->SetCol2(171, 0);
	m_SpellList->SetWidthCol2(89);

	m_SpecialityInfo = UDynamicCast(USpecialityInfo, GetChildByID(3));
	if (!m_SpecialityInfo)
	{
		return FALSE;
	}

	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(21));
	if (pText)
	{
		pText->SetTextEdge(true);
	}

	if(m_pNumEdit == NULL)
	{
		m_pNumEdit = UDynamicCast(UEdit, GetChildByID(25));
		if (!m_pNumEdit)
		{
			return FALSE;
		}
	}
	m_pNumEdit->SetNumberOnly(TRUE);
	m_pNumEdit->SetMaxLength(3);

	UButton* pBtn = UDynamicCast(UButton, GetChildByID(8));
	if(pBtn)
	{
		pBtn->SetCheck(FALSE);
	}

	pBtn = UDynamicCast(UButton, GetChildByID(9));
	if(pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	pText = UDynamicCast(UStaticText, GetChildByID(108));
	if (pText)
	{
		pText->SetTextAlign(UT_RIGHT);
		pText->SetTextColor("00FF00");
	}
	pText = UDynamicCast(UStaticText, GetChildByID(109));
	if (pText)
	{
		pText->SetTextAlign(UT_RIGHT);
		pText->SetTextColor("FF0000");
	}

	return TRUE;
}
void USpecialityDlg::OnDestroy()
{
	m_StoreClassName = _TRAN("全部");
	m_SpellClassMap.clear();
	m_VisibleMask = 0;
	m_LineClass = 0;
	UDialog::OnDestroy();
}
BOOL USpecialityDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void USpecialityDlg::OnClose()
{
	SetVisible(FALSE);
	m_LineClass = 0;
}
void USpecialityDlg::OnTimer(float fDetaltime)
{
	if (!m_Visible)
	{
		m_bContiuneManufacture = false;
		return;
	}
	UDialog::OnTimer(fDetaltime);
	if (m_bContiuneManufacture && m_buildNum > 0)
	{
		LockBuild();
		m_buildNum--;
		Build();
	}
}


void USpecialityDlg::OnChooseClassList()
{
	char buf[256];
	int bRet = m_pListBox->GetSelectedText(buf, 256);
	if(bRet)
	{
		m_StoreClassName = buf;
		UpdateSpellList();
	}
}

void USpecialityDlg::OnChooseList()
{
	CPlayerLocal* pLocalplayer = ObjectMgr->GetLocalPlayer();
	int cursel = m_SpellList->GetCurSel();
	uint32* pSpell = (uint32*)m_SpellList->GetItemData(cursel);
	if(pSpell)
	{
		if (pLocalplayer && TestManuFacureNum(*pSpell))
		{
			m_SpellList->SetBitmap("Skill\\SkillTrainingSpecList.skin");
		}
		else
		{
			m_SpellList->SetBitmap("Skill\\SkillTrainingSpecList1.skin");
		}
		m_SpecialityInfo->SetSpecialitySpellId(*pSpell);
		m_ChooseSpellEntry = *pSpell;
	}
}

void USpecialityDlg::OnBtnClickBuildAll()
{
	int curSel = m_SpellList->GetCurSel();
	if (curSel == -1)
		return;
	void* pDate = m_SpellList->GetItemData(curSel);
	if (pDate)
	{
		uint32 SpellEntry = *(uint32*)(pDate);
		m_LockSpellEntry = SpellEntry;
		char buf[256];
		m_buildNum = TestManuFacureNum(m_LockSpellEntry);
		m_pNumEdit->SetText(itoa(m_buildNum, buf, 10));
		m_buildNum++;
		UnLockBuild();
	}
}

void USpecialityDlg::OnBtnClickBuild()
{
	int curSel = m_SpellList->GetCurSel();
	if (curSel == -1)
		return;
	void* pDate = m_SpellList->GetItemData(curSel);
	if (pDate)
	{
		uint32 SpellEntry = *(uint32*)(pDate);
		m_LockSpellEntry = SpellEntry;

		char buf[256];
		m_pNumEdit->GetText(buf, 256, 0);
		m_buildNum = atoi(buf);
		m_buildNum = min(m_buildNum, TestManuFacureNum(m_LockSpellEntry));
		if (m_buildNum == 0)
			m_buildNum = 1;
		m_pNumEdit->SetText(itoa(m_buildNum, buf, 10));
		m_buildNum++;
		UnLockBuild();
	}
}

void USpecialityDlg::OnBtnClickCancel()
{
	OnClose();
}

void USpecialityDlg::OnCheckCanDo()
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(8));
	if(pBtn)
	{
		GetChildByID(108)->SetActive(pBtn->IsChecked());
		if (pBtn->IsChecked())	{
			m_VisibleMask += 1;
		}else{
			m_VisibleMask -= 1;
		}
		UpdateSpellList();
	}
}

void USpecialityDlg::OnCheckCantDo()
{
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(9));
	if(pBtn)
	{
		GetChildByID(109)->SetActive(pBtn->IsChecked());
		if (pBtn->IsChecked())	{
			m_VisibleMask += 2;
		}else{
			m_VisibleMask -= 2;
		}
		UpdateSpellList();
	}
}

void USpecialityDlg::OnIncNum()
{
	char buf[256];
	m_pNumEdit->GetText(buf, 256, 0);
	m_buildNum = atoi(buf);
	m_buildNum++;
	m_pNumEdit->SetText(itoa(m_buildNum, buf, 10));
}

void USpecialityDlg::OnDecNum()
{
	char buf[256];
	m_pNumEdit->GetText(buf, 256, 0);
	m_buildNum = atoi(buf);

	if (m_buildNum > 0)
	{
		m_buildNum--;
		m_pNumEdit->SetText(itoa(m_buildNum, buf, 10));
	}
}

void USpecialityDlg::SetCurList()
{
	if (m_LineClass == 0)
	{
		return;
	}
	m_SpellClassMap.clear();
	SetVisible(TRUE);
	CPlayerLocal* pkplayer = ObjectMgr->GetLocalPlayer();
	if (!pkplayer)
		return;
	
	vector<uint32> SkillList;
	pkplayer->GetSkillList(SkillList);
	for (unsigned int ui = 0 ; ui < SkillList.size(); ++ui)
	{
		SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate(SkillList[ui]);
		if (pSpellTemplate && pSpellTemplate->GetLineNameEntry() == m_LineClass)
		{
			std::string name;
			if (SkillList[ui] == PROFESSION_DUANZAO_ID
				|| SkillList[ui] == PROFESSION_CAIYAO_ID
				|| SkillList[ui] == PROFESSION_CAIKUANG_ID
				|| SkillList[ui] == PROFESSION_ZHUBAO_ID
				|| SkillList[ui] == PROFESSION_LIANDAN_ID
				||SkillList[ui] == PROFESSION_DUANZAO_ID + 1
				|| SkillList[ui] == PROFESSION_CAIYAO_ID + 1
				|| SkillList[ui] == PROFESSION_CAIKUANG_ID + 1
				|| SkillList[ui] == PROFESSION_ZHUBAO_ID + 1
				|| SkillList[ui] == PROFESSION_LIANDAN_ID + 1
				||SkillList[ui] == PROFESSION_DUANZAO_ID+2
				|| SkillList[ui] == PROFESSION_CAIYAO_ID+ 2
				|| SkillList[ui] == PROFESSION_CAIKUANG_ID+ 2
				|| SkillList[ui] == PROFESSION_ZHUBAO_ID + 2
				|| SkillList[ui] == PROFESSION_LIANDAN_ID + 2
				)
			{
				continue;
			}
			if (g_SkillLineNameInfo->GetSkillLineName(pSpellTemplate->GetSubLineNameEntry(), name))
			{
				SpellClassMap::iterator it =  m_SpellClassMap.find(name);
				if (it != m_SpellClassMap.end())
				{
					it->second.push_back(SkillList[ui]);
					continue;
				}
				std::vector<uint32> newlist;
				newlist.push_back(SkillList[ui]);
				m_SpellClassMap.insert(SpellClassMap::value_type(name, newlist));
			}
		}
	}
	PlayerProfessionLevel* pProf = pkplayer->GetProfessionState(m_LineClass);
	if (pProf)
	{
		UProgressBar_Skin* pBar = UDynamicCast(UProgressBar_Skin, GetChildByID(29));
		if (pBar)
		{
			pBar->SetProgressPos(pProf->current, pProf->Max);
		}
	}
	UpdateList();
}

bool USpecialityDlg::CallBuild(ui32 SpellID)
{
	uint32 lineclass = 0;
	int IconIndex = 0;
	bool bSuccess = false;
	switch (SpellID)
	{
	case PROFESSION_DUANZAO_ID:
	case PROFESSION_DUANZAO_ID + 1:
	case PROFESSION_DUANZAO_ID + 2:
		 IconIndex = 0;
		lineclass = 164;
		bSuccess = true;
		break;
	case PROFESSION_CAIYAO_ID:
	case PROFESSION_CAIYAO_ID + 1:
	case PROFESSION_CAIYAO_ID + 2:
		 IconIndex = 1;
		lineclass = 182;
		bSuccess = true;
		break;
	case PROFESSION_CAIKUANG_ID:
	case PROFESSION_CAIKUANG_ID + 1:
	case PROFESSION_CAIKUANG_ID + 2:
		IconIndex = 3;
		lineclass = 186;
		bSuccess = true;
		break;
	case PROFESSION_ZHUBAO_ID:
	case PROFESSION_ZHUBAO_ID + 1:
	case PROFESSION_ZHUBAO_ID + 2:
		IconIndex = 4;
		lineclass = 770;
		bSuccess = true;
		break;
	case PROFESSION_LIANDAN_ID:
	case PROFESSION_LIANDAN_ID + 1:
	case PROFESSION_LIANDAN_ID + 2:
		IconIndex = 2;
		lineclass = 771;
		bSuccess = true;
		break;
	}
	if (lineclass == m_LineClass)
	{
		OnClose();
		return bSuccess;
	}
	if (bSuccess)
	{
		m_LineClass = lineclass;
		SetCurList();
		UBitmap* pBitMap = UDynamicCast(UBitmap, GetChildByID(30));
		if (pBitMap)
		{
			pBitMap->SetSkinIndex(IconIndex);
		}
	}
	return bSuccess;
}

void USpecialityDlg::ClearBuild()
{
	m_SpecialityInfo->SetSpecialitySpellId(0);
}

void USpecialityDlg::LockBuild()
{
	m_bContiuneManufacture = false;
}

void USpecialityDlg::Build()
{
	if (!m_LockSpellEntry /*|| !InputTestManuFacture()*/)
		return;

	char buf[256];
	if(m_pNumEdit)
		m_pNumEdit->SetText(itoa(m_buildNum, buf, 10));

	if (m_buildNum <= 0)
		return;

	CPlayerInputMgr * InputMsg = SYState()->LocalPlayerInput;
	if (InputMsg)
	{
		if ( InputMsg->IsSelectingTarget() )
		{
			InputMsg->EndSelectTarget();
			return;
		}
		InputMsg->BeginSkillAttack(m_LockSpellEntry);
	}
}

void USpecialityDlg::UnLockBuild()
{
	m_bContiuneManufacture = true;
}

void USpecialityDlg::UpdateList()
{
	m_pListBox->Clear();
	m_pListBox->AddItem(_TRAN("全部"));
	SpellClassMap::iterator it =  m_SpellClassMap.begin();
	while(it != m_SpellClassMap.end())
	{
		m_pListBox->AddItem(it->first.c_str());
		++it;
	}
	UpdateSpellList();
	m_pListBox->SetCurSel(0);
}
void AddItemToSpellList(USpecListBox* pList, const char* pName, ui32 buildNum, int colorIndex, uint32* content)
{
	char buf[256];
	char* color[]={
		"<color:R128G128B128A255>",
		"<color:R000G255B000A255>",
		"<color:R255G209B000A255>",
		"<color:R193G083B020A255>",
		"<color:R255G000B000A255>",
	};

	NiSprintf(buf, 255, "<Col1>%s<center>%s<end_left><Col2>%s<center>%u<end>",
		color[colorIndex], pName, color[colorIndex], buildNum);

	pList->AddItem(buf, content);
}

void USpecialityDlg::UpdateSpellList()
{
	CPlayerLocal* pLocalplayer = ObjectMgr->GetLocalPlayer();
	m_SpellList->Clear();
	int SelIndex = -1;
	int count = 0;
	SpellClassMap::iterator it =  m_SpellClassMap.begin();
	while(it != m_SpellClassMap.end())
	{
		if(m_StoreClassName.compare(it->first.c_str()) == 0 || m_StoreClassName.compare(_TRAN("全部")) == 0)
		{
			for (unsigned int ui = 0 ; ui < it->second.size() ; ++ui)
			{
				SpellTemplate* pkSpellTem = SYState()->SkillMgr->GetSpellTemplate(it->second[ui]);
				if (!pkSpellTem)
					return;
				if ( m_LockSpellEntry == it->second[ui] || m_ChooseSpellEntry == it->second[ui])
				{
					SelIndex = count;
				}
				ui32 BuildNum = TestManuFacureNum(it->second[ui]);
				if ( BuildNum )
				{
					if (m_VisibleMask & 1)
					{
						stSkillLineSpell* pSkillLineSpell = g_SkillLineSpellInfo->GetSkillLineSpell(it->second[ui]);
						if (pSkillLineSpell)
						{
							int ColorIndex = 0;
							PlayerProfessionLevel* pProf = pLocalplayer->GetProfessionState(pSkillLineSpell->skillLine);
							if (pProf)
							{
								if(pProf->current >= pSkillLineSpell->grey)
									ColorIndex = 0;
								else if (pProf->current >= ((pSkillLineSpell->grey - pSkillLineSpell->green) / 2 + pSkillLineSpell->green))
									ColorIndex = 1;
								else if (pProf->current >= pSkillLineSpell->green)
									ColorIndex = 2;
								else
									ColorIndex = 3;
							}
							AddItemToSpellList(m_SpellList, pkSpellTem->GetName().c_str(), BuildNum, ColorIndex, &it->second[ui]);
						}
						else
						{
							AddItemToSpellList(m_SpellList, pkSpellTem->GetName().c_str(), BuildNum, 0, &it->second[ui]);
						}
						++count;
					}
				}else
				{
					if (m_VisibleMask & 2)
					{
						AddItemToSpellList(m_SpellList, pkSpellTem->GetName().c_str(), BuildNum, 4, &it->second[ui]);
						++count;
					}
				}
			}
		}
		++it;
	}
	if ( m_SpellList->GetItemCount() && SelIndex != -1 )
	{
		m_SpellList->SetCurSel(SelIndex);
	}
	else
	{
		ClearBuild();
	}
}