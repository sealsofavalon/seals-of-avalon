#pragma once
#include "UInc.h"
#include "UDynamicIconButtonEx.h"
#include "USkillButton.h"

#define UPageSkillMax 12 // 每页最多

enum USYSkillShowType
{
	USYSkillAll = 0,   //全部
	USYMaxLev,         //最高等级 默认
};

class USkillExplain : public UControl
{
	class SkillInfoButtonItem : public UActionItem
	{
	public:
		virtual void Use();
	};
	UDEC_CLASS(USkillExplain);
public:
	USkillExplain();
	~USkillExplain();
public:
	void InitSkill(ui32 pSpellID, UINT pos);
	void OnClear();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnTimer(float fDeltaTime);
protected:
	UDynamicIconButtonEx * m_SkillInfo;   //技能信息
	UStaticText* m_SkillName;   //技能名字
	UStaticText* m_SkillLev;    //技能等级
	UStaticText* m_SkillType;   //技能类型
private:
};
class UProfSkillExplain : public UControl
{
	UDEC_CLASS(UProfSkillExplain);
	UDEC_MESSAGEMAP();
public:
	UProfSkillExplain();
	~UProfSkillExplain();

	void SetSkillEntry(uint32 entry);
	uint32 GetSkillEntry(){return m_SkillLine;}
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	void OnBtnClickUnLearn();

	uint32 m_SkillLine;
};
class UISkill : public UDialog
{
	UDEC_CLASS(UISkill);
	UDEC_MESSAGEMAP();
public:
	UISkill(void);
	~UISkill(void);
public:
	void OnInitSkillList(vector<ui32> &pSpells);    //初始化技能列表
	void OnUpdateSkillList(ui32 pSpell);
	BOOL OnDelSkillByID(ui32 pSpell);
	BOOL FindSpellInLocal(ui32 pSpell);
	void SortSpells(int ClassNum = 0);
	void OnProfLevelChange(uint32 spellLockId, uint32 current);
	void RefreshProf();
	//
	virtual void SetVisible(BOOL value);
protected:
	void OnSetCurPage();   //刷新到当前页数 
	void OnSetSkillByPos(UINT pos, ui32 pSpellID); // skill info
	void OnPageUp();   //翻页
	void OnPageDown(); //翻页

	void OnChooseSkillBook();
	void OnChooseProfBook();

	void OnChooseClass1();
	void OnChooseClass2();
	void OnChooseClass3();
	void OnChooseClass4();
	void OnChooseClass5();

	void ClearUI();

	void SetShowSkillType(USYSkillShowType pSkillShowType);
	void InsertSkillVec(ui32 spellid);
	void FindMaxLevelSkill();

	void OnShowTypeChang();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
	
	void UpdateBtnVisible(int btnIndex);
protected:
	UINT m_curPage;   //当前页
	UINT m_numPage;   //总页数

	USkillExplain* m_SkillItem[UPageSkillMax];
	UComboBox* m_SkillShowComboBox;

	USYSkillShowType m_ShowType ;

	vector<ui32>m_ShowSpells;  //显示在UI的技能列表的ID
	vector<ui32> m_Spells;  //技能ID列表

	UProfSkillExplain* m_ProfSkill1;
	UProfSkillExplain* m_ProfSkill2;
	UBitmapButton* m_ClassButton[5];
};

class UIManufactureNeedItem : public UControl
{
	UDEC_CLASS(UIManufactureNeedItem);

public:
	UIManufactureNeedItem();
	~UIManufactureNeedItem();

	void SetManufactureNeedItem(ui32 entry, int num);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	void Clear();

	USkillButton* m_ItemBtn;
	UStaticText* m_ItemName;
};
class UIManufacture : public UDialog
{
	UDEC_CLASS(UIManufacture);
	UDEC_MESSAGEMAP();
public:
	UIManufacture();
	~UIManufacture();

	void SetSkillList(vector<ui32> &pSpells);
	void AddNewSkill(ui32 pSpell);
	void UpdateVisibleList();
	void UpdateManuFacture();
	bool InputTestManuFacture();
	void ClearBuild();
	void LockBuild();
	void Build();
	void UnLockBuild();
	uint32 GetLockSpellEntry(){return m_LockSpellEntry;}
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
	virtual void OnTimer(float fDetaltime);

	void OnCheckCanDo();
	void OnCheckCantDo();
	void OnChooseList();
	void OnClickAllManufacture();
	void OnClickManufacture();
	void OnIncNum();
	void OnDecNum();

	std::vector<ui32> m_ShowSpellsList;
	std::vector<ui32> m_SpellsList;

	class USpecListBox* m_ManufactureList;
	UIManufactureNeedItem* m_NeedItem[4];
	USkillButton* m_BuildItemBtn;
	URichTextCtrl* m_RichEdit;
	UStaticText* m_BuildItemName;
	UEdit* m_pNumEdit;
	ui32 m_buildNum;
	uint32 m_LockSpellEntry;

	ui32 m_VisibleMask;
	bool m_bContiuneManufacture;
};

class USkillTrainingInfo : public UControl
{
	UDEC_CLASS(USkillTrainingInfo);
public:
	USkillTrainingInfo();
	~USkillTrainingInfo();

	void ShowInfo(ui32 spell_ID, ui32 cost, ui32 Require_Spell, ui32 Require_level, ui32 Require_Skill, ui32 Require_SkillLevel);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
};

class USkillTrainingDlg : public UDialog
{
	UDEC_CLASS(USkillTrainingDlg);
	UDEC_MESSAGEMAP();
public:
	USkillTrainingDlg();
	~USkillTrainingDlg();

	void ParseTrainingAck(uint64 guid,uint32 TrainerType, uint32 ID);
	void ParseTraingingList( uint64 guid, uint32 TrainerType, 	vector<struct stSpellTrainerInfo>& vSpells,	std::string message );
	void AddClassMap(std::string& str, stSpellTrainerInfo& TrainerSpell);
	void UpdateList();
	void UpdateSpellList();
	void ShowMoneyNum(UINT  Num);
	bool CanLearnSpell(stSpellTrainerInfo* SpellInfo);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
	virtual void OnTimer(float fDeltaTime);

	void OnClickBtnLearn();
	void OnClickBtnExit();
	void OnChooseList();
	void OnChooseClass();
	void OnCheckCanDo();
	void OnCheckCantDo();

	USkillTrainingInfo* m_SkillTrainingInfo;
	typedef std::map<std::string, std::vector<stSpellTrainerInfo>> SpellClassMap;
	SpellClassMap m_SpellClassMap;
	UListBox* m_pListBox;
	class USpecListBox* m_SpellList;
	std::string m_StoreClassName;
	ui64 m_GUID;
	ui32 m_VisibleMask;
	ui32 m_LocalMoney;
};

class USpecialityNeedItemInfo : public UControl
{
	UDEC_CLASS(USpecialityNeedItemInfo);
public:
	USpecialityNeedItemInfo();
	~USpecialityNeedItemInfo();

	void SetItem(uint32 ItemEntry, uint32 num);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
};

class USpecialityInfo : public UControl
{
	UDEC_CLASS(USpecialityInfo);
public:
	USpecialityInfo();
	~USpecialityInfo();

	void SetSpecialitySpellId(uint32 spellID);
	void SetInfoTimer();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDetaltime);

	uint32 m_spellID;
	USpecialityNeedItemInfo* m_NeedItem[3];
};

class USpecialityDlg : public UDialog
{
	UDEC_CLASS(USpecialityDlg);
	UDEC_MESSAGEMAP();
public:
	USpecialityDlg();
	~USpecialityDlg();

	void SetCurList();
	bool CallBuild(ui32 SpellID);
	void ClearBuild();
	void LockBuild();
	void Build();
	void UnLockBuild();
	uint32 GetLockSpellEntry(){return m_LockSpellEntry;}

	void UpdateList();
	void UpdateSpellList();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
	virtual void OnTimer(float fDetaltime);


	void OnChooseClassList();
	void OnChooseList();
	void OnBtnClickBuildAll();
	void OnBtnClickBuild();
	void OnBtnClickCancel();

	void OnCheckCanDo();
	void OnCheckCantDo();

	void OnIncNum();
	void OnDecNum();

	typedef std::map<std::string, std::vector<uint32>> SpellClassMap;
	SpellClassMap m_SpellClassMap;

	UListBox* m_pListBox;
	class USpecListBox* m_SpellList;
	USpecialityInfo* m_SpecialityInfo;
	UEdit* m_pNumEdit;
	ui32 m_buildNum;
	uint32 m_LockSpellEntry;
	uint32 m_ChooseSpellEntry;
	ui32 m_VisibleMask;
	uint32 m_LineClass;
	bool m_bContiuneManufacture;
	std::string m_StoreClassName;
};