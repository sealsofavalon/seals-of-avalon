#include "StdAfx.h"
#include "ClientApp.h"
#include "UISystem.h"
#include "UILogin.h"
#include "UIGamePlay.h"
#include "UIChoosePlayer.h"
#include "UICreatePlayer.h"
#include "..\Player.h"
#include "..\ObjectManager.h"
#include "ItemManager.h"
#include "UIItemSystem.h"
#include "AudioInterface.h"
#include "console.h"
#include "PlayerInputMgr.h"
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>

struct UID_ID_TEMPLATE
{
	EDialogIdent DID;
	const char* Template;
};


#define TEMPLATE(id,template) {id, template}, 
const UID_ID_TEMPLATE _DIALOG_TEMPLATE_[] = 
{
	TEMPLATE(DID_LOADING,"Login\\LoginLoading.udg")
	TEMPLATE(DID_LOGIN, "Login\\Login.udg")
	TEMPLATE(DID_CHOOSESERVER,"ChooseServer\\ChooseServer.udg")
	TEMPLATE(DID_INGAME_MAIN,"InGame\\InGame.udg")
	TEMPLATE(DID_CHOOSEPLAYER,"ChoosePlayer\\ChoosePlayer.udg")
	TEMPLATE(DID_CREATEPLAYER,"CreatePlayer\\CreatePlayer.udg")
	TEMPLATE(DID_MSGBOX,"MSGBOX\\MsgBox.udg")
	TEMPLATE(DID_WAITLIST,"WAITLIST\\WaitList.udg")
	TEMPLATE(DID_FRESHMANHELP,"FreshManHelp\\UInputTutorial.udg")
};
#undef TEMPLATE

struct SYUiSoundInterf : public IUSoundInterface 
{
	virtual void PlaySound(const char* pszSound)
	{
		if(SYState()->IAudio)
		{
			SYState()->IAudio->PlayUiSound(pszSound, NiPoint3::ZERO, 1.0f);
		}
	}

	virtual void PlaySound(int Index)
	{
		if (SYState()->IAudio)
		{
			SYState()->IAudio->PlayUiSound(Index);
		}
	}
	
}g_SySI;

class CURenderInterface : public IURenderInterface
{
public:
    virtual void SetRenderState(D3DRENDERSTATETYPE State, DWORD Value)
    {
        m_pkRenderState->SetRenderState(State, Value);
    }

    virtual DWORD GetRenderState(D3DRENDERSTATETYPE State)
    {
        return m_pkRenderState->GetRenderState(State);
    }

    virtual void SetFVF(DWORD FVF)
    {
        m_pkRenderState->SetFVF(FVF);
    }

    virtual void SetVertexShader(void* pShader)
    {
        DASSERT(pShader == NULL);
        m_pkRenderState->SetVertexShader(NULL);
    }

    virtual void SetPixelShader(void* pShader)
    {
        DASSERT(pShader == NULL);
        m_pkRenderState->SetPixelShader(NULL);
    }

    virtual void SetSamplerState(DWORD Sampler, D3DSAMPLERSTATETYPE Type, DWORD Value)
    {
        m_pkRenderState->SetSamplerState(Sampler, Type, Value);
    }

    virtual void SetTextureStageState(DWORD Stage, D3DTEXTURESTAGESTATETYPE Type, DWORD Value)
    {
        m_pkRenderState->SetTextureStageState(Stage, Type, Value);
    }

    virtual void SetTexture(DWORD Sampler, IDirect3DBaseTexture9 * pTexture)
    {
        m_pkRenderState->SetTexture(Sampler, pTexture);
    }

    virtual HRESULT DrawIndexedPrimitiveUP(D3DPRIMITIVETYPE PrimitiveType, UINT MinVertexIndex, UINT NumVertices, UINT PrimitiveCount, CONST void * pIndexData, D3DFORMAT IndexDataFormat, CONST void* pVertexStreamZeroData, UINT VertexStreamZeroStride)
    {
        return m_pD3DDevice->DrawIndexedPrimitiveUP(PrimitiveType, MinVertexIndex, NumVertices, PrimitiveCount, pIndexData, IndexDataFormat, pVertexStreamZeroData, VertexStreamZeroStride);
    }

    virtual HRESULT DrawPrimitiveUP(D3DPRIMITIVETYPE PrimitiveType, UINT PrimitiveCount, CONST void* pVertexStreamZeroData, UINT VertexStreamZeroStride)
    {
        return m_pD3DDevice->DrawPrimitiveUP(PrimitiveType, PrimitiveCount, pVertexStreamZeroData, VertexStreamZeroStride);
    }

    virtual HRESULT SetScissorRect(const URect& ClipRect)
    {
        RECT rc;
        rc.left = ClipRect.left;
        rc.top = ClipRect.top;
        rc.bottom = ClipRect.bottom;
        rc.right = ClipRect.right;

        return m_pD3DDevice->SetScissorRect(&rc);
    }

    virtual HRESULT CreateTextureFromFileInMemoryEx(LPCVOID pSrcData, UINT SrcDataSize, UINT Width, UINT Height, UINT MipLevels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, DWORD Filter, DWORD MipFilter, D3DCOLOR ColorKey, D3DXIMAGE_INFO * pSrcInfo, PALETTEENTRY * pPalette, LPDIRECT3DTEXTURE9 * ppTexture)
    {
        return D3DXCreateTextureFromFileInMemoryEx(m_pD3DDevice, pSrcData, SrcDataSize, Width, Height, MipLevels, Usage, Format, Pool, Filter, MipFilter, ColorKey, pSrcInfo, pPalette, ppTexture);
    }

    virtual HRESULT CreateTexture(UINT Width, UINT Height, UINT Levels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DTexture9** ppTexture, HANDLE* pSharedHandle)
    {
        return m_pD3DDevice->CreateTexture(Width, Height, Levels, Usage, Format, Pool, ppTexture, pSharedHandle);
    }

    CURenderInterface(NiDX9RenderState* pkRenderState, IDirect3DDevice9* pD3DDevice)
        :m_pkRenderState(pkRenderState)
        ,m_pD3DDevice(pD3DDevice)
    {
    }

private:
    NiDX9RenderState* m_pkRenderState;
    IDirect3DDevice9* m_pD3DDevice;
};

CUISystem::CUISystem(void)
{
	memset(m_pDlgControls,0,sizeof(m_pDlgControls));
	m_ItemSystem = NULL;

	m_LastUpdataKeyStateTime = 0.0f;

	m_LastDownKey = 0;
	m_LastKeyWait = TRUE ;
}

CUISystem::~CUISystem(void)
{
	Destroy();
}
void CUISystem::SetImeHwnd(HWND hWnd)
{
	if (m_pUSystem)
		m_pUSystem->IniIMEWnd(hWnd);
}

BOOL CUISystem::Initialize(NiRenderer* Render, HWND hWnd)
{
	if (Render == NULL)
	{
		return FALSE;
	}


    g_pRenderInterface = new CURenderInterface(((NiDX9Renderer*)Render)->GetRenderState(), ((NiDX9Renderer*)Render)->GetD3DDevice());

	m_pUSystem = USystem::Create();
	if (m_pUSystem == NULL)
	{
		return FALSE;
	}
	if(!m_pUSystem->Initialize(hWnd))
	{
		m_pUSystem->Release();
		m_pUSystem = NULL;
		return FALSE;
	}
	
	RECT rcWnd;
	::GetWindowRect(hWnd, &rcWnd);
	int width = rcWnd.right - rcWnd.left;
	int height = rcWnd.bottom - rcWnd.top;

	
	NiDX9Renderer* DR = (NiDX9Renderer*)(NiRenderer*)Render;
	NiRenderTargetGroup* pkDefaultRenderTarget = Render->GetDefaultRenderTargetGroup();
	if(pkDefaultRenderTarget)
	{
		width = pkDefaultRenderTarget->GetWidth(0);
		height = pkDefaultRenderTarget->GetHeight(0);
		
	}
	m_pUSystem->Resize(width, height);
	m_pUSystem->ReszieWindow(width, height);
	IDirect3DDevice9* D3dDev = DR->GetD3DDevice();
	m_pUSystem->SetDevice(D3dDev);
	
	if (!m_pUSystem->LoadControlStyle("Core\\Default.stl"))
	{
		return FALSE;
	}
	m_pUSystem->EnableIME(TRUE);
	if (!m_pUSystem->LoadControlStyle("Style\\syui.stl"))
	{
		return FALSE;
	}
	if (!m_pUSystem->LoadControlStyle("Style\\Tip.stl"))
	{
		return FALSE;
	}
	
	//UI声效
	m_pUSystem->SetSoundInterface(&g_SySI);
	//if (!m_pUSystem->LoadAcceleratorKey("AcceleratorKey\\InGame.ackey"))
	//{
	//	return FALSE;
	//}
	if (!m_pUSystem->LoadColorTable("ColorTable.color"))
	{
		return FALSE;
	}

	FillUIMapping();

	m_ItemSystem = new UItemSystem;

	m_IsIMEEable = FALSE;

	if (!m_pUSystem->InitSoftKeyBoardControl())
	{
		return FALSE ;
	}
	return TRUE;
}

void CUISystem::PostResize(INT Width, INT Height)
{
	if (m_pUSystem)
	{
		m_pUSystem->Resize(Width, Height);
        m_pUSystem->ReszieWindow(Width, Height);
	}
}

void CUISystem::Update(float dt)
{
	if (m_pUSystem)
	{
		m_pUSystem->Update(dt);
	}
	if (m_ItemSystem)
	{
		m_ItemSystem->Update(dt);
	}
}

void CUISystem::UpdateKeyMap(float dt)
{
	 NiInputKeyboard* InputKeyBoard = NULL ;
	 
	 if (SYState()->Input)
	 {
		 InputKeyBoard = SYState()->Input->GetKeyboard();
	 }
	 // 如果无法获得InputKeyBoard 
	if (!InputKeyBoard)
	{
		return ;
	}
	for (int i =0; i < SY_KEY_MAX ; i++)
	{
		if (m_UIKeyMapping[i].mkeycode)
		{
			if (InputKeyBoard->KeyWasPressed((NiInputKeyboard::KeyCode)m_UIKeyMapping[i].mkeycode) && m_UIKeyMapping[i].mstate == UNIInputKey::DOWN)
			{
				if ( i == DIK_LCONTROL || i == DIK_RCONTROL || i == DIK_LALT || i == DIK_RALT || i == DIK_LSHIFT || i == DIK_RSHIFT)
				{
					m_UIKeyMapping[i].mstate = UNIInputKey::PRESS ;
					Queue_Key[Key_Pess][i] = UDikToLKey(i);
					continue ;
				}
			}

			if (InputKeyBoard->KeyIsDown((NiInputKeyboard::KeyCode)m_UIKeyMapping[i].mkeycode) && m_UIKeyMapping[i].mstate == UNIInputKey::UP)
			{
				m_UIKeyMapping[i].mstate = UNIInputKey::DOWN;
				m_LastDownKey = UDikToLKey(i) ;	
				Queue_Key[Key_Down][i] = UDikToLKey(i);
				continue ;
			}

			if (InputKeyBoard->KeyWasReleased((NiInputKeyboard::KeyCode)m_UIKeyMapping[i].mkeycode) && m_UIKeyMapping[i].mstate != UNIInputKey::UP)
			{
				m_UIKeyMapping[i].mstate = UNIInputKey::UP;
				
				m_LastDownKey = 0 ;	
				m_LastKeyWait = TRUE;

				Queue_Key[Key_Up][i] = UDikToLKey(i);
				//PushKeyUpMsg(UDikToLKey(i));
				continue ;
			}
			
		}
	}

	//处理删除键等等的一直按下的情况。
	// 在等待1.0S后, 开始0.3S发一次激发键的消息

	/*if (m_LastDownKey == LK_BACKSPACE)
	{
		m_LastUpdataKeyStateTime += dt;
		
		float WaitTime = 0.3f;
		if (m_LastKeyWait)
		{
			WaitTime += 1.0f;
			m_LastKeyWait = FALSE ;
		}
		if (m_LastUpdataKeyStateTime >= WaitTime)
		{
			PushKeyDownMsg(LK_BACKSPACE);
			m_LastUpdataKeyStateTime = 0.0f;
		}
		
	}*/
	
	//如果是IME变化，这里重设键盘状态
	if (m_IsIMEEable != m_pUSystem->IsIMEEable())
	{
		m_IsIMEEable = m_pUSystem->IsIMEEable();
		
		ReSetKeyState() ;
	}	
	
	//如果IME处于激活状态，那么这里就不发送UI消息
	//而是交给WINDOWS 消息处理
	if (!m_IsIMEEable)
	{
		SendUIKeyMsg();
	}
	//一直更新键盘行走消息
	SYState()->LocalPlayerInput->UpdateMoveKeys();
	
	
}
void CUISystem::Destroy()
{
	for (int id = DID_LOADING; id < DID_MAX ;id++ )
	{
		if (m_pDlgControls[id] != NULL)
		{
			m_pUSystem->DestoryControl(m_pDlgControls[id]);
			m_pDlgControls[id] = NULL;
		}
	}
	if (m_pUSystem)
	{
		m_pUSystem->Release();
		m_pUSystem = NULL;
	}

	if(m_ItemSystem)
	{
		delete m_ItemSystem;
		m_ItemSystem = NULL;
	}

    if(g_pRenderInterface)
    {
        delete g_pRenderInterface;
        g_pRenderInterface = NULL;
    }
}

UControl* CUISystem::GetDialogEX(EDialogIdent DID)
{
	NIASSERT(DID >= 0 && DID < DID_MAX);
	return m_pDlgControls[DID];
}

UControl* CUISystem::LoadDialogEX(EDialogIdent DID)
{
	NIASSERT(DID >= 0 && DID < DID_MAX);
	if (m_pDlgControls[DID])
	{
		return m_pDlgControls[DID];
	}

	// 查找模板.
	// 查找模板.
	INT NumTemplates = sizeof(_DIALOG_TEMPLATE_)/sizeof(_DIALOG_TEMPLATE_[0]);
	INT Index = 0;
	for( ;Index < NumTemplates; Index++)
	{
		if (_DIALOG_TEMPLATE_[Index].DID == DID)
		{
			break;
		}
	}

	if (Index == NumTemplates)
	{
		// 没有找到模板定义
		return NULL;
	}

	UControl* pDlgRoot = m_pUSystem->CreateDialogFromFile(_DIALOG_TEMPLATE_[Index].Template);
	m_pDlgControls[DID] = pDlgRoot;
	return pDlgRoot;
}

BOOL CUISystem::IsCurFrame(EDialogIdent DID)
{
	CUISystem* pSystem = SYState()->UI;
	NIASSERT(pSystem);

	UControl* pkDlg = pSystem->LoadDialogEX(DID);
	if( !pkDlg )
		return FALSE;

	if (pSystem->m_pUSystem->GetCurFrame() && pSystem->m_pUSystem->GetCurFrame() == pkDlg)
	{
		return TRUE ;
	}

	return FALSE;
}
void CUISystem::ShowEX(EDialogIdent DID, BOOL bShow, BOOL bSheet)
{
	CUISystem* pSystem = SYState()->UI;
	NIASSERT(pSystem);

	UControl* pkDlg = pSystem->LoadDialogEX(DID);
	if( !pkDlg )
		return;
	

	if( bSheet )
	{
		//SYState()->IAudio->StopAllSound();
		if (pSystem->m_pUSystem->GetCurFrame() != pkDlg)
		{
			pSystem->m_pUSystem->SetFrameControl(pkDlg);
		}
		if (DID == DID_LOGIN || DID == DID_CHOOSEPLAYER || DID == DID_CREATEPLAYER || DID == DID_CHOOSESERVER)
		{
			int index = SYState()->IAudio->PlayMusic(LOGIN_BGM.c_str(), 2);
			SYState()->IAudio->OnlyPlaySound(index);
		}else
		{
			SYState()->IAudio->StopAllSound();
		}
	}
	else
	{
		UControl* pControl = pSystem->m_pUSystem->GetCurFrame();
		if (pControl)
		{
			if (bShow)
			{
				pControl->AddChild(pkDlg);
			}else
			{
				pControl->RemoveChild(pkDlg);
			}	
		}
	}
}

void CUISystem::PushRenderState()
{
}

void CUISystem::PopRenderState()
{
}

void CUISystem::RenderEX()
{
	if (m_pUSystem)
	{
		PushRenderState();
		m_pUSystem->Draw();
		PopRenderState();
	}
}
void CUISystem::FillUIMapping()
{
	memset((void*)m_UIKeyMapping, 0, sizeof(m_UIKeyMapping));

	m_UIKeyMapping[DIK_ESCAPE].mkeycode = NiInputKeyboard::KEY_ESCAPE;
	m_UIKeyMapping[DIK_1].mkeycode = NiInputKeyboard::KEY_1;
	m_UIKeyMapping[DIK_2].mkeycode = NiInputKeyboard::KEY_2;
	m_UIKeyMapping[DIK_3].mkeycode = NiInputKeyboard::KEY_3;
	m_UIKeyMapping[DIK_4].mkeycode = NiInputKeyboard::KEY_4;
	m_UIKeyMapping[DIK_5].mkeycode = NiInputKeyboard::KEY_5;
	m_UIKeyMapping[DIK_6].mkeycode = NiInputKeyboard::KEY_6;
	m_UIKeyMapping[DIK_7].mkeycode = NiInputKeyboard::KEY_7;
	m_UIKeyMapping[DIK_8].mkeycode = NiInputKeyboard::KEY_8;
	m_UIKeyMapping[DIK_9].mkeycode = NiInputKeyboard::KEY_9;
	m_UIKeyMapping[DIK_0].mkeycode = NiInputKeyboard::KEY_0;
	m_UIKeyMapping[DIK_MINUS].mkeycode = NiInputKeyboard::KEY_MINUS;
	m_UIKeyMapping[DIK_EQUALS].mkeycode = NiInputKeyboard::KEY_EQUALS;
	m_UIKeyMapping[DIK_BACK].mkeycode = NiInputKeyboard::KEY_BACK;
	m_UIKeyMapping[DIK_TAB].mkeycode = NiInputKeyboard::KEY_TAB;
	m_UIKeyMapping[DIK_Q].mkeycode = NiInputKeyboard::KEY_Q;
	m_UIKeyMapping[DIK_W].mkeycode = NiInputKeyboard::KEY_W;
	m_UIKeyMapping[DIK_E].mkeycode = NiInputKeyboard::KEY_E;
	m_UIKeyMapping[DIK_R].mkeycode = NiInputKeyboard::KEY_R;
	m_UIKeyMapping[DIK_T].mkeycode = NiInputKeyboard::KEY_T;
	m_UIKeyMapping[DIK_Y].mkeycode = NiInputKeyboard::KEY_Y;
	m_UIKeyMapping[DIK_U].mkeycode = NiInputKeyboard::KEY_U;
	m_UIKeyMapping[DIK_I].mkeycode = NiInputKeyboard::KEY_I;
	m_UIKeyMapping[DIK_O].mkeycode = NiInputKeyboard::KEY_O;
	m_UIKeyMapping[DIK_P].mkeycode = NiInputKeyboard::KEY_P;
	m_UIKeyMapping[DIK_LBRACKET].mkeycode = NiInputKeyboard::KEY_LBRACKET;
	m_UIKeyMapping[DIK_RBRACKET].mkeycode = NiInputKeyboard::KEY_RBRACKET;
	m_UIKeyMapping[DIK_RETURN].mkeycode = NiInputKeyboard::KEY_RETURN;
	m_UIKeyMapping[DIK_LCONTROL].mkeycode = NiInputKeyboard::KEY_LCONTROL;
	m_UIKeyMapping[DIK_A].mkeycode = NiInputKeyboard::KEY_A;
	m_UIKeyMapping[DIK_S].mkeycode = NiInputKeyboard::KEY_S;
	m_UIKeyMapping[DIK_D].mkeycode = NiInputKeyboard::KEY_D;
	m_UIKeyMapping[DIK_F].mkeycode = NiInputKeyboard::KEY_F;
	m_UIKeyMapping[DIK_G].mkeycode = NiInputKeyboard::KEY_G;
	m_UIKeyMapping[DIK_H].mkeycode = NiInputKeyboard::KEY_H;
	m_UIKeyMapping[DIK_J].mkeycode = NiInputKeyboard::KEY_J;
	m_UIKeyMapping[DIK_K].mkeycode = NiInputKeyboard::KEY_K;
	m_UIKeyMapping[DIK_L].mkeycode = NiInputKeyboard::KEY_L;
	m_UIKeyMapping[DIK_SEMICOLON].mkeycode = NiInputKeyboard::KEY_SEMICOLON;
	m_UIKeyMapping[DIK_APOSTROPHE].mkeycode = NiInputKeyboard::KEY_APOSTROPHE;
	m_UIKeyMapping[DIK_GRAVE].mkeycode = NiInputKeyboard::KEY_GRAVE;
	m_UIKeyMapping[DIK_LSHIFT].mkeycode = NiInputKeyboard::KEY_LSHIFT;
	m_UIKeyMapping[DIK_BACKSLASH].mkeycode = NiInputKeyboard::KEY_BACKSLASH;
	m_UIKeyMapping[DIK_Z].mkeycode = NiInputKeyboard::KEY_Z;
	m_UIKeyMapping[DIK_X].mkeycode = NiInputKeyboard::KEY_X;
	m_UIKeyMapping[DIK_C].mkeycode = NiInputKeyboard::KEY_C;
	m_UIKeyMapping[DIK_V].mkeycode = NiInputKeyboard::KEY_V;
	m_UIKeyMapping[DIK_B].mkeycode = NiInputKeyboard::KEY_B;
	m_UIKeyMapping[DIK_N].mkeycode = NiInputKeyboard::KEY_N;
	m_UIKeyMapping[DIK_M].mkeycode = NiInputKeyboard::KEY_M;
	m_UIKeyMapping[DIK_COMMA].mkeycode = NiInputKeyboard::KEY_COMMA;
	m_UIKeyMapping[DIK_PERIOD].mkeycode = NiInputKeyboard::KEY_PERIOD;
	m_UIKeyMapping[DIK_SLASH].mkeycode = NiInputKeyboard::KEY_SLASH;
	m_UIKeyMapping[DIK_RSHIFT].mkeycode = NiInputKeyboard::KEY_RSHIFT;
	m_UIKeyMapping[DIK_MULTIPLY].mkeycode = NiInputKeyboard::KEY_MULTIPLY;
	m_UIKeyMapping[DIK_LMENU].mkeycode = NiInputKeyboard::KEY_LMENU;
	m_UIKeyMapping[DIK_SPACE].mkeycode = NiInputKeyboard::KEY_SPACE;
	m_UIKeyMapping[DIK_CAPITAL].mkeycode = NiInputKeyboard::KEY_CAPITAL;
	m_UIKeyMapping[DIK_F1].mkeycode = NiInputKeyboard::KEY_F1;
	m_UIKeyMapping[DIK_F2].mkeycode = NiInputKeyboard::KEY_F2;
	m_UIKeyMapping[DIK_F3].mkeycode = NiInputKeyboard::KEY_F3;
	m_UIKeyMapping[DIK_F4].mkeycode = NiInputKeyboard::KEY_F4;
	m_UIKeyMapping[DIK_F5].mkeycode = NiInputKeyboard::KEY_F5;
	m_UIKeyMapping[DIK_F6].mkeycode = NiInputKeyboard::KEY_F6;
	m_UIKeyMapping[DIK_F7].mkeycode = NiInputKeyboard::KEY_F7;
	m_UIKeyMapping[DIK_F8].mkeycode = NiInputKeyboard::KEY_F8;
	m_UIKeyMapping[DIK_F9].mkeycode = NiInputKeyboard::KEY_F9;
	m_UIKeyMapping[DIK_F10].mkeycode = NiInputKeyboard::KEY_F10;
	m_UIKeyMapping[DIK_NUMLOCK].mkeycode = NiInputKeyboard::KEY_NUMLOCK;
	m_UIKeyMapping[DIK_SCROLL].mkeycode = NiInputKeyboard::KEY_SCROLL;
	m_UIKeyMapping[DIK_NUMPAD7].mkeycode = NiInputKeyboard::KEY_NUMPAD7;
	m_UIKeyMapping[DIK_NUMPAD8].mkeycode = NiInputKeyboard::KEY_NUMPAD8;
	m_UIKeyMapping[DIK_NUMPAD9].mkeycode = NiInputKeyboard::KEY_NUMPAD9;
	m_UIKeyMapping[DIK_SUBTRACT].mkeycode = NiInputKeyboard::KEY_SUBTRACT;
	m_UIKeyMapping[DIK_NUMPAD4].mkeycode = NiInputKeyboard::KEY_NUMPAD4;
	m_UIKeyMapping[DIK_NUMPAD5].mkeycode = NiInputKeyboard::KEY_NUMPAD5;
	m_UIKeyMapping[DIK_NUMPAD6].mkeycode = NiInputKeyboard::KEY_NUMPAD6;
	m_UIKeyMapping[DIK_ADD].mkeycode = NiInputKeyboard::KEY_ADD;
	m_UIKeyMapping[DIK_NUMPAD1].mkeycode = NiInputKeyboard::KEY_NUMPAD1;
	m_UIKeyMapping[DIK_NUMPAD2].mkeycode = NiInputKeyboard::KEY_NUMPAD2;
	m_UIKeyMapping[DIK_NUMPAD3].mkeycode = NiInputKeyboard::KEY_NUMPAD3;
	m_UIKeyMapping[DIK_NUMPAD0].mkeycode = NiInputKeyboard::KEY_NUMPAD0;
	m_UIKeyMapping[DIK_DECIMAL].mkeycode = NiInputKeyboard::KEY_DECIMAL;
	m_UIKeyMapping[DIK_OEM_102].mkeycode = NiInputKeyboard::KEY_OEM_102;
	m_UIKeyMapping[DIK_F11].mkeycode = NiInputKeyboard::KEY_F11;
	m_UIKeyMapping[DIK_F12].mkeycode = NiInputKeyboard::KEY_F12;
	m_UIKeyMapping[DIK_F13].mkeycode = NiInputKeyboard::KEY_F13;
	m_UIKeyMapping[DIK_F14].mkeycode = NiInputKeyboard::KEY_F14;
	m_UIKeyMapping[DIK_F15].mkeycode = NiInputKeyboard::KEY_F15;
	m_UIKeyMapping[DIK_KANA].mkeycode = NiInputKeyboard::KEY_KANA;
	m_UIKeyMapping[DIK_ABNT_C1].mkeycode = NiInputKeyboard::KEY_ABNT_C1;
	m_UIKeyMapping[DIK_CONVERT].mkeycode = NiInputKeyboard::KEY_CONVERT;
	m_UIKeyMapping[DIK_NOCONVERT].mkeycode = NiInputKeyboard::KEY_NOCONVERT;
	m_UIKeyMapping[DIK_YEN].mkeycode = NiInputKeyboard::KEY_YEN;
	m_UIKeyMapping[DIK_ABNT_C2].mkeycode = NiInputKeyboard::KEY_ABNT_C2;
	m_UIKeyMapping[DIK_NUMPADEQUALS].mkeycode = NiInputKeyboard::KEY_NUMPADEQUALS;
	m_UIKeyMapping[DIK_PREVTRACK].mkeycode = NiInputKeyboard::KEY_PREVTRACK;
	m_UIKeyMapping[DIK_AT].mkeycode = NiInputKeyboard::KEY_AT;
	m_UIKeyMapping[DIK_COLON].mkeycode = NiInputKeyboard::KEY_COLON;
	m_UIKeyMapping[DIK_UNDERLINE].mkeycode = NiInputKeyboard::KEY_UNDERLINE;
	m_UIKeyMapping[DIK_KANJI].mkeycode = NiInputKeyboard::KEY_KANJI;
	m_UIKeyMapping[DIK_STOP].mkeycode = NiInputKeyboard::KEY_STOP;
	m_UIKeyMapping[DIK_AX].mkeycode = NiInputKeyboard::KEY_AX;
	m_UIKeyMapping[DIK_UNLABELED].mkeycode = NiInputKeyboard::KEY_UNLABELED;
	m_UIKeyMapping[DIK_NEXTTRACK].mkeycode = NiInputKeyboard::KEY_NEXTTRACK;
	m_UIKeyMapping[DIK_NUMPADENTER].mkeycode = NiInputKeyboard::KEY_NUMPADENTER;
	m_UIKeyMapping[DIK_RCONTROL].mkeycode = NiInputKeyboard::KEY_RCONTROL;
	m_UIKeyMapping[DIK_MUTE].mkeycode = NiInputKeyboard::KEY_MUTE;
	m_UIKeyMapping[DIK_CALCULATOR].mkeycode = NiInputKeyboard::KEY_CALCULATOR;
	m_UIKeyMapping[DIK_PLAYPAUSE].mkeycode = NiInputKeyboard::KEY_PLAYPAUSE;
	m_UIKeyMapping[DIK_MEDIASTOP].mkeycode = NiInputKeyboard::KEY_MEDIASTOP;
	m_UIKeyMapping[DIK_VOLUMEDOWN].mkeycode = NiInputKeyboard::KEY_VOLUMEDOWN;
	m_UIKeyMapping[DIK_VOLUMEUP].mkeycode = NiInputKeyboard::KEY_VOLUMEUP;
	m_UIKeyMapping[DIK_WEBHOME].mkeycode = NiInputKeyboard::KEY_WEBHOME;
	m_UIKeyMapping[DIK_NUMPADCOMMA].mkeycode = NiInputKeyboard::KEY_NUMPADCOMMA;
	m_UIKeyMapping[DIK_DIVIDE].mkeycode = NiInputKeyboard::KEY_DIVIDE;
	m_UIKeyMapping[DIK_SYSRQ].mkeycode = NiInputKeyboard::KEY_SYSRQ;
	m_UIKeyMapping[DIK_RMENU].mkeycode = NiInputKeyboard::KEY_RMENU;
	m_UIKeyMapping[DIK_PAUSE].mkeycode = NiInputKeyboard::KEY_PAUSE;
	m_UIKeyMapping[DIK_HOME].mkeycode = NiInputKeyboard::KEY_HOME;
	m_UIKeyMapping[DIK_UP].mkeycode = NiInputKeyboard::KEY_UP;
	m_UIKeyMapping[DIK_PRIOR].mkeycode = NiInputKeyboard::KEY_PRIOR;
	m_UIKeyMapping[DIK_LEFT].mkeycode = NiInputKeyboard::KEY_LEFT;
	m_UIKeyMapping[DIK_RIGHT].mkeycode = NiInputKeyboard::KEY_RIGHT;
	m_UIKeyMapping[DIK_END].mkeycode = NiInputKeyboard::KEY_END;
	m_UIKeyMapping[DIK_DOWN].mkeycode = NiInputKeyboard::KEY_DOWN;
	m_UIKeyMapping[DIK_NEXT].mkeycode = NiInputKeyboard::KEY_NEXT;
	m_UIKeyMapping[DIK_INSERT].mkeycode = NiInputKeyboard::KEY_INSERT;
	m_UIKeyMapping[DIK_DELETE].mkeycode = NiInputKeyboard::KEY_DELETE;
	m_UIKeyMapping[DIK_LWIN].mkeycode = NiInputKeyboard::KEY_LWIN;
	m_UIKeyMapping[DIK_RWIN].mkeycode = NiInputKeyboard::KEY_RWIN;
	m_UIKeyMapping[DIK_APPS].mkeycode = NiInputKeyboard::KEY_APPS;
	m_UIKeyMapping[DIK_POWER].mkeycode = NiInputKeyboard::KEY_POWER;
	m_UIKeyMapping[DIK_SLEEP].mkeycode = NiInputKeyboard::KEY_SLEEP;
	m_UIKeyMapping[DIK_WAKE].mkeycode = NiInputKeyboard::KEY_WAKE;
	m_UIKeyMapping[DIK_WEBSEARCH].mkeycode = NiInputKeyboard::KEY_WEBSEARCH;
	m_UIKeyMapping[DIK_WEBFAVORITES].mkeycode = NiInputKeyboard::KEY_WEBFAVORITES;
	m_UIKeyMapping[DIK_WEBREFRESH].mkeycode = NiInputKeyboard::KEY_WEBREFRESH;
	m_UIKeyMapping[DIK_WEBSTOP].mkeycode = NiInputKeyboard::KEY_WEBSTOP;
	m_UIKeyMapping[DIK_WEBFORWARD].mkeycode = NiInputKeyboard::KEY_WEBFORWARD;
	m_UIKeyMapping[DIK_WEBBACK].mkeycode = NiInputKeyboard::KEY_WEBBACK;
	m_UIKeyMapping[DIK_MYCOMPUTER].mkeycode = NiInputKeyboard::KEY_MYCOMPUTER;
	m_UIKeyMapping[DIK_MAIL].mkeycode = NiInputKeyboard::KEY_MAIL;
	m_UIKeyMapping[DIK_MEDIASELECT].mkeycode = NiInputKeyboard::KEY_MEDIASELECT;

	
	ReSetKeyState();
	
}

void CUISystem::ReSetKeyState()
{
	for (int i =0; i < Key_Max ; i++)
	{
		for (int j = 0; j < SY_KEY_MAX; j++)
		{
			Queue_Key[i][j] = 0;
		}
	}
}
void CUISystem::KillKeyMsg()
{
	if(SYState()->LocalPlayerInput)
	{
		SYState()->LocalPlayerInput->ResetMoveKeys();
	}

	ReSetKeyState();
}
DWORD CUISystem::LK2NIINPUT8KEY(DWORD lk)
{
	UINT dik = LKeyToDik((KeyCodes)lk);
	if (dik >=0 && dik < SY_KEY_MAX)
	{
		return m_UIKeyMapping[dik].mkeycode;
	}

	return 0;
}
BOOL CUISystem::SendUIKeyMsg()
{

	for ( int i =0; i< SY_KEY_MAX; i++)
	{
		if (Queue_Key[Key_Pess][i])
		{
			m_pUSystem->KeyPress(Queue_Key[Key_Pess][i]); 
			Queue_Key[Key_Pess][i] = 0 ;
		}
	}
	
	for ( int i =0; i< SY_KEY_MAX; i++)
	{
		if (Queue_Key[Key_Down][i])
		{
			m_pUSystem->KeyDown(Queue_Key[Key_Down][i]); 
			Queue_Key[Key_Down][i] = 0;
		}
	}

	for ( int i =0; i< SY_KEY_MAX; i++)
	{
		if (Queue_Key[Key_Up][i])
		{
			m_pUSystem->KeyUp(Queue_Key[Key_Up][i]);
			Queue_Key[Key_Up][i] = 0;
		}
	}

	return TRUE;
}
BOOL CUISystem::ProcessMessage(NiEventRef pEventRecord)
{
	if (m_pUSystem == NULL)
	{
		return FALSE ;
	}

	BOOL bProceded = FALSE;
	UINT Msg = pEventRecord->uiMsg;
	DWORD wParam = pEventRecord->wParam;
	long lParam = pEventRecord->lParam;
	HWND hWnd = pEventRecord->hWnd;
	RECT rcClient;
	GetClientRect(hWnd, &rcClient);
	// TODO
	BOOL bUnicode = ::IsWindowUnicode(pEventRecord->hWnd);

	switch(Msg)
	{
	case WM_SIZE:
		m_pUSystem->Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_MOUSEMOVE:
		bProceded = m_pUSystem->MouseMove(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_LBUTTONDOWN:
		bProceded = m_pUSystem->MouseBtnDown(LK_LEFTBTN);
		break;
	case WM_LBUTTONUP:
		bProceded = m_pUSystem->MouseBtnUp(LK_LEFTBTN);
		break;
	case WM_RBUTTONDOWN:
		bProceded = m_pUSystem->MouseBtnDown(LK_RIGHTBTN);
		break;
	case WM_RBUTTONUP:
		bProceded = m_pUSystem->MouseBtnUp(LK_RIGHTBTN);
		break;
	case WM_CHAR:
	case WM_SYSKEYUP:
	case WM_SYSKEYDOWN:
		if (bUnicode)
		{
			m_pUSystem->KeyMessageW(Msg, wParam, lParam); 
		}else
		{
			m_pUSystem->KeyMessageA(Msg, wParam, lParam); 
		}

		bProceded = TRUE;
		break;
	case WM_KEYDOWN:
	case WM_KEYUP:
		//
		if (m_pUSystem->IsIMEEable())  // key down key up 消息只针对IME激活的时候使用
		{
			if (bUnicode)
			{
				m_pUSystem->KeyMessageW(Msg, wParam, lParam); 
			}else
			{
				m_pUSystem->KeyMessageA(Msg, wParam, lParam); 
			}
		}
		bProceded = TRUE;
		break;
	case WM_MBUTTONDOWN:
		bProceded = m_pUSystem->MouseBtnDown(LK_MIDBTN);
		break;

	case WM_MBUTTONUP:
		bProceded = m_pUSystem->MouseBtnUp(LK_MIDBTN);
		break;

	case 0x020A: //WM_MOUSEWHEEL:
		bProceded = m_pUSystem->MouseWheel(GET_WHEEL_DELTA_WPARAM(wParam)/120);
		break;

	case WM_INPUTLANGCHANGE:
	case WM_IME_SETCONTEXT:
	case WM_IME_STARTCOMPOSITION:
	case WM_IME_COMPOSITION:
	case WM_IME_ENDCOMPOSITION:
	case WM_IME_NOTIFY:
		if (m_pUSystem->IsIMEEable())
		{
			bProceded = m_pUSystem->ProcessIMEMessage(Msg, wParam, lParam);
		}
		break;
	case WM_SETFOCUS:
	case WM_KILLFOCUS:
	case WM_INITMENUPOPUP:
		{
			KillKeyMsg();
			bProceded = m_pUSystem->WindowKillFouce();
		}
		break;
	}

	//return NiApplication::OnDefault(pEventRecord);
	return bProceded;
}