#pragma once
#include "..\Utils\ItemDB.h"
#include "UInc.h"
#include "UIItemSystem.h"

#define  MaxLenToNPC  5.0f
enum EDialogIdent
{
	DID_LOADING = 0,
	DID_LOGIN,
	DID_CHOOSESERVER,
	DID_MAINMENU,
	DID_CHOOSEPLAYER,			//选择人物界面
	DID_CREATEPLAYER,
	DID_INGAME_MAIN,
	DID_MSGBOX,
	DID_WAITLIST,
	DID_FRESHMANHELP,
	DID_MAX,
};

struct UNIInputKey
{
	enum
	{
		UP = 0,
		DOWN,
		PRESS,
	};

	UNIInputKey()
	{
		mkeycode = 0;
		mstate = UP;
		bistate = 0;
	}
	unsigned int mkeycode;
	int mstate;
	int bistate;

	void Init(unsigned int keycode, int state) {
		mkeycode = keycode;
		mstate = state;
	}

	bool Compare(unsigned int keycode) {
		return bool(mkeycode == keycode);
	}

	unsigned int GetCode() {
		return mkeycode;
	}

	void SetState(int state) {
		mstate = state;
	}

	int GetState() {
		return mstate;
	}
};

#define UILK_2_NIINPUTKEY(x) SYState()->UI->LK2NIINPUT8KEY(x)

class NDLUIRender;
class CUISystem
{
public:
	CUISystem(void);
	virtual ~CUISystem(void);
	BOOL Initialize(NiRenderer* Render, HWND hWnd);
	void SetImeHwnd(HWND hWnd);
	void Update(float dt);
	void UpdateKeyMap(float dt);
	
	void Destroy();
	// 通知UI系统重新计算布局
	void PostResize(INT Width, INT Height);
	BOOL ProcessMessage(NiEventRef pEventRecord);
	
	//ui的LK 转换成NIINPUT 的KEY
	DWORD LK2NIINPUT8KEY(DWORD lk);
	//KEY_UP  KEY_DOWN 消息 从另外途径更新
	//
	BOOL SendUIKeyMsg();

	UItemSystem* GetUItemSystem(){return m_ItemSystem;}
	inline USystem* GetUSystem() { return m_pUSystem;}
public:
	UControl* GetDialogEX(EDialogIdent DID);
	UControl* LoadDialogEX(EDialogIdent DID);
	static void ShowEX(EDialogIdent DID, BOOL bShow, BOOL bSheet);
	static BOOL IsCurFrame(EDialogIdent DID);
	void RenderEX();
	void PushRenderState();
	void PopRenderState();

	enum
	{
		SY_KEY_MAX = 256
	};
	enum
	{
		Key_Down = 0,
		Key_Pess,
		Key_Up,

		Key_Max ,
	};
protected:
	 void FillUIMapping();
	 void ReSetKeyState();
	 void KillKeyMsg();
protected:
	USystem* m_pUSystem;
protected:
	UControl* m_pDlgControls[DID_MAX];
	UItemSystem* m_ItemSystem ;
	UINT m_LastDownKey ;
	BOOL m_LastKeyWait ;
	UNIInputKey m_UIKeyMapping[SY_KEY_MAX];

	DWORD Queue_Key[Key_Max][SY_KEY_MAX];

	float m_LastUpdataKeyStateTime ;

	BOOL m_IsIMEEable;
};