#include "StdAfx.h"
#include "UITipSystem.h"
#include "ItemTip.h"
#include "MonsterTip.h"
#include "SpellTip.h"
#include "PlayerTip.h"
#include "TitleTip.h"
#include "UTip.h"
#include "QuestTip.h"
#include "UIGamePlay.h"
#include "SceneGameObjTip.h"
#include "Network/PacketBuilder.h"

UTipSystem::UTipSystem()
{
	for (int i = 0 ; i < TIP_TYPE_MAX ; i++)
	{
		m_MouseTip[i] = NULL;
	}
	m_TipUI_Frame = NULL;
	m_TipUI_Compara_Frame[0]=m_TipUI_Compara_Frame[1]=NULL;
	m_TipUI_Art = NULL;
	m_TipUI_Instance = NULL;
	m_CurType = TIP_TYPE_MAX;
	m_NameFontFace = NULL;
	m_pos.Set(0,0);
	mIsUpdateServerTime = false;
	mServerTime = 0;
	mLastUpdate = 0;
	mBkgSkin = NULL;
	m_PlayerGuid = 0;
}
UTipSystem::~UTipSystem()
{
	TipSystem->DestorySystem();
}

BOOL UTipSystem::CreateSystem(UControl * Ctrl)
{
	//TODO 创建子系统
	if(m_MouseTip[TIP_TYPE_SPELL] == NULL)
	{
		m_MouseTip[TIP_TYPE_SPELL] = new SpellTip;
	}
	if (m_MouseTip[TIP_TYPE_SPELL])
	{
		m_MouseTip[TIP_TYPE_SPELL]->SetType(TIP_TYPE_SPELL);
	}
	else
	{
		return FALSE;
	}
	if(m_MouseTip[TIP_TYPE_BUFF] == NULL)
	{
		m_MouseTip[TIP_TYPE_BUFF] = new SpellTip;
	}
	if (m_MouseTip[TIP_TYPE_BUFF])
	{
		m_MouseTip[TIP_TYPE_BUFF]->SetType(TIP_TYPE_BUFF);
	}
	else
	{
		return FALSE;
	}
	if(m_MouseTip[TIP_TYPE_PLAYER] == NULL)
	{
		m_MouseTip[TIP_TYPE_PLAYER] = new PlayerTip;
	}
	if (m_MouseTip[TIP_TYPE_PLAYER])
	{
		m_MouseTip[TIP_TYPE_PLAYER]->SetType(TIP_TYPE_PLAYER);
	}
	else
	{
		return FALSE;
	}
	if(m_MouseTip[TIP_TYPE_MONSTER] == NULL)
	{
		m_MouseTip[TIP_TYPE_MONSTER] = new MonsterTip;
	}
	if (m_MouseTip[TIP_TYPE_MONSTER])
	{
		m_MouseTip[TIP_TYPE_MONSTER]->SetType(TIP_TYPE_MONSTER);
	}
	else
	{
		return FALSE;
	}
	if(m_MouseTip[TIP_TYPE_ITEM] == NULL)
	{
		m_MouseTip[TIP_TYPE_ITEM] = new ItemTip;
	}
	if (m_MouseTip[TIP_TYPE_ITEM])
	{
		m_MouseTip[TIP_TYPE_ITEM]->SetType(TIP_TYPE_ITEM);
	}
	else
	{
		return FALSE;
	}
	if (m_MouseTip[TIP_TYPE_SCENEGAMEOBJ] == NULL)
	{
		m_MouseTip[TIP_TYPE_SCENEGAMEOBJ] = new SceneGameObjTip;
	}
	if (m_MouseTip[TIP_TYPE_SCENEGAMEOBJ])
	{
		m_MouseTip[TIP_TYPE_SCENEGAMEOBJ]->SetType(TIP_TYPE_SCENEGAMEOBJ);
	}else
	{
		return FALSE;
	}
	if(m_MouseTip[TIP_TYPE_ITEM_COMPARA1] == NULL)
	{
		m_MouseTip[TIP_TYPE_ITEM_COMPARA1] = new ItemTip;
	}
	if (m_MouseTip[TIP_TYPE_ITEM_COMPARA1])
	{
		m_MouseTip[TIP_TYPE_ITEM_COMPARA1]->SetType(TIP_TYPE_ITEM_COMPARA1);
	}
	else
	{
		return FALSE;
	}
	if(m_MouseTip[TIP_TYPE_ITEM_COMPARA2] == NULL)
	{
		m_MouseTip[TIP_TYPE_ITEM_COMPARA2] = new ItemTip;
	}
	if (m_MouseTip[TIP_TYPE_ITEM_COMPARA2])
	{
		m_MouseTip[TIP_TYPE_ITEM_COMPARA2]->SetType(TIP_TYPE_ITEM_COMPARA2);
	}
	else
	{
		return FALSE;
	}
	if (m_MouseTip[TIP_TYPE_TITLE] == NULL)
	{
		m_MouseTip[TIP_TYPE_TITLE] = new TitleTip;
	}
	if (m_MouseTip[TIP_TYPE_TITLE])
	{
		m_MouseTip[TIP_TYPE_TITLE]->SetType(TIP_TYPE_TITLE);
	}else
	{
		return FALSE;
	}

	if (m_MouseTip[TIP_TYPE_QUEST] == NULL)
	{
		m_MouseTip[TIP_TYPE_QUEST] = new QuestTip;
	}
	if (m_MouseTip[TIP_TYPE_QUEST])
	{
		m_MouseTip[TIP_TYPE_QUEST]->SetType(TIP_TYPE_QUEST);
	}else
	{
		return FALSE;
	}

	if (m_MouseTip[TIP_TYPE_INSTANCE] == NULL)
	{
		m_MouseTip[TIP_TYPE_INSTANCE] = new InstanceTip;
	}
	if (m_MouseTip[TIP_TYPE_INSTANCE])
	{
		m_MouseTip[TIP_TYPE_INSTANCE]->SetType(TIP_TYPE_INSTANCE);
	}else
	{
		return FALSE;
	}

	if (m_MouseTip[TIP_TYPE_RECOUNT] == NULL)
	{
		m_MouseTip[TIP_TYPE_RECOUNT] = new ReCountTip;
	}
	if (m_MouseTip[TIP_TYPE_RECOUNT])
	{
		m_MouseTip[TIP_TYPE_RECOUNT]->SetType(TIP_TYPE_RECOUNT);
	}else
	{
		return FALSE;
	}

	if (m_MouseTip[TIP_TYPE_STRINGLIST] == NULL)
	{
		m_MouseTip[TIP_TYPE_STRINGLIST] = new StringListTip;
	}
	if (m_MouseTip[TIP_TYPE_STRINGLIST])
	{
		m_MouseTip[TIP_TYPE_STRINGLIST]->SetType(TIP_TYPE_STRINGLIST);
	}else
	{
		return FALSE;
	}	

	if (m_MouseTip[TIP_TYPE_PLAYERPROPERTY] == NULL)
	{
		m_MouseTip[TIP_TYPE_PLAYERPROPERTY] = new PlayerPropertyTip;
	}
	if (m_MouseTip[TIP_TYPE_PLAYERPROPERTY])
	{
		m_MouseTip[TIP_TYPE_PLAYERPROPERTY]->SetType(TIP_TYPE_PLAYERPROPERTY);
	}else
	{
		return FALSE;
	}
	//TODO 创建UI这里只创建了一个空白信息的UI,后面也许需要创建一个拥有美术资源的UI，后面需要添加
	//或者说把美术资源的UI也放到这个空白UI中用MouseTip中的DrawTip来实现美术资源的更行，也许可行
	if (m_TipUI_Frame == NULL)
	{
		m_TipUI_Frame = UControl::sm_System->CreateDialogFromFile("MouseTip\\NULLTip.udg");
		if(m_TipUI_Frame)
		{
			Ctrl->AddChild(m_TipUI_Frame);
			m_TipUI_Frame->SetId(FRAME_IG_MOUSETIP);
			m_TipUI_Frame->SetVisible(FALSE);
		}
		else
		{
			return FALSE;
		}
	}

	if (m_TipUI_Art == NULL)
	{
		m_TipUI_Art = UControl::sm_System->CreateDialogFromFile("MouseTip\\ArtTip.udg");
		if(m_TipUI_Art)
		{
			Ctrl->AddChild(m_TipUI_Art);
			m_TipUI_Art->SetId(FRAME_IG_MOUSTARTTIP);
			m_TipUI_Art->SetVisible(FALSE);
		}
		else
		{
			return FALSE;
		}
	}

	if (m_TipUI_Instance == NULL)
	{
		m_TipUI_Instance = UControl::sm_System->CreateDialogFromFile("MouseTip\\InstanceTip.udg");
		if(m_TipUI_Instance)
		{
			Ctrl->AddChild(m_TipUI_Instance);
			m_TipUI_Instance->SetId(FRAME_IG_MOUSEINSTANCETIP);
			m_TipUI_Instance->SetVisible(FALSE);
		}
		else
		{
			return FALSE;
		}
	}
	if (mBkgSkin == NULL)
	{
		mBkgSkin = UControl::sm_System->LoadSkin("Talkpopo\\popo_1.skin");
		if (!mBkgSkin)
			return FALSE;
	}

	for (int i = 0 ; i < 2; i++)
	{
		if (m_TipUI_Compara_Frame[i] == NULL)
		{
			m_TipUI_Compara_Frame[i] = UControl::sm_System->CreateDialogFromFile("MouseTip\\NULLTip.udg");
			if (m_TipUI_Compara_Frame[i])
			{
				Ctrl->AddChild(m_TipUI_Compara_Frame[i]);
				m_TipUI_Compara_Frame[i]->SetId(FRAME_IG_MOUSECOMTIP1 + i);
				m_TipUI_Compara_Frame[i]->SetVisible(FALSE);
			}
			else
			{
				return FALSE;
			}
		}

	}
	m_RootCtrl = Ctrl;
	InitData();
	return TRUE;
}
void UTipSystem::DestorySystem()
{
	//由于UI有自己的销毁机制所以在这里就不进行ui的销毁工作
	//只是销毁掉创建出来的子系统而已，并且和系统指针一同销毁
	for(int i = 0 ; i < TIP_TYPE_MAX; i++)
	{
		if (m_MouseTip[i])
		{
			delete m_MouseTip[i];
			m_MouseTip[i] = NULL;
		}
	}
}
void UTipSystem::ShowTip(ui32 id, UPoint pos, PayData* pkData, USkinPtr PayItemSkin, UINT SkinIndex)  //PAYDATA   TIPS
{
	UTip * ut;
	UPoint NewPos = pos;
	m_CurType = TIP_TYPE_ITEM;
	m_pos = pos;

	ut = UDynamicCast(UTip,m_TipUI_Frame);
	if (ut)
	{
		ItemTip* pItemTip = (ItemTip*)m_MouseTip[TIP_TYPE_ITEM];
		pItemTip->CreateTip(ut,id,NewPos,pkData, PayItemSkin, SkinIndex);
	}
	ut->SetType(TIP_TYPE_ITEM);
	m_RootCtrl->MoveChildTo(ut,NULL);
	FitTipInWindow();
}

void UTipSystem::ShowTip(UPoint pos, std::vector<std::string>& Str)
{
	UTip* ut;
	UPoint NewPos = pos;
	m_CurType = TIP_TYPE_STRINGLIST;
	m_pos = pos;

	ut = UDynamicCast(UTip, m_TipUI_Frame);
	if (ut)
	{
		StringListTip* pListTip = (StringListTip*)m_MouseTip[TIP_TYPE_STRINGLIST];
		pListTip->CreateTip(ut, NewPos, Str);
	}

	ut->SetType(TIP_TYPE_STRINGLIST);
	m_RootCtrl->MoveChildTo(ut, NULL);
	FitTipInWindow();
}

void UTipSystem::ShowTip(int Type, ui32 id, UPoint pos,ui64 guid ,UINT count)
{
	UTip * ut;
	UPoint NewPos = pos;

	switch (Type)
	{
	case TIP_TYPE_ITEM_COMPARA1:case TIP_TYPE_ITEM_COMPARA2:
		{
			NewPos.Set(m_pos.x + m_TipUI_Frame->GetWidth() + 3, m_pos.y);
			ut = UDynamicCast(UTip,m_TipUI_Compara_Frame[Type - TIP_TYPE_ITEM_COMPARA1]);
		}
		break;/*
	case TIP_TYPE_MONSTER:
		{
			m_pos = pos;
			NewPos = pos;
			m_CurType = Type;
			ut = UDynamicCast(UTip,m_TipUI_Frame);
		}
		break;
	case TIP_TYPE_SCENEGAMEOBJ:
		{
			m_pos = pos;
			NewPos = pos;
			m_CurType = Type;
			ut = UDynamicCast(UTip, m_TipUI_Frame);
		}
		break;*/
	default:
		{
			m_pos = pos;
			NewPos = pos;
			m_CurType = Type;
			ut = UDynamicCast(UTip,m_TipUI_Frame);
		}
	}
	if (ut)
	{
		if (Type >= TIP_TYPE_SPELL && Type < TIP_TYPE_MAX)
		{
			if (m_MouseTip[Type])
			{
				m_MouseTip[Type]->CreateTip(ut,id,NewPos,guid,count);
			}
		}
		else
		{
			return;
		}
		//TODO UI的显示并且传入到底是什么类型的Tip用于ui的选取	
		ut->SetType(Type);

		m_RootCtrl->MoveChildTo(ut,NULL);
		FitTipInWindow();
	}
}

void UTipSystem::ShowTip(ui32 entryid, UPoint pos, ItemExtraData& Data)
{
	UTip * ut;
	UPoint NewPos = pos;

	m_CurType = TIP_TYPE_ITEM;

	m_pos = pos;

	ut = UDynamicCast(UTip,m_TipUI_Frame);
	if (ut)
	{
		ItemTip* pItemTip = (ItemTip*)m_MouseTip[TIP_TYPE_ITEM];
		pItemTip->m_ItemSpecProperty.IsCreate = true;
		pItemTip->m_ItemSpecProperty.ItemEntry = entryid;
		pItemTip->m_ItemSpecProperty.itemdata.jinglian_level = Data.jinglian_level;
		for( int i = 0; i < 6; ++i )
			pItemTip->m_ItemSpecProperty.itemdata.enchants[i] =  Data.enchants[i];

		pItemTip->CreateTip(ut,entryid,NewPos);
		ut->SetType(TIP_TYPE_ITEM);
	}
	m_RootCtrl->MoveChildTo(ut,NULL);
	FitTipInWindow();
}

void UTipSystem::ReCreateItemTip()
{
	if(m_CurType == TIP_TYPE_ITEM)
	{
		ItemTip* pItemTip = (ItemTip*)m_MouseTip[TIP_TYPE_ITEM];
		pItemTip->ReCreateTip();

		FitTipInWindow();
	}
}

void UTipSystem::ShowInstanceTip(int32 mapid, UPoint pos)
{
	UTip * ut;
	UPoint NewPos = pos;
	m_CurType = TIP_TYPE_INSTANCE;
	ut = UDynamicCast(UTip,m_TipUI_Instance);
	if (ut)
	{
		InstanceTip* pInstanceTip = (InstanceTip*)m_MouseTip[TIP_TYPE_INSTANCE];
		pInstanceTip->CreateTip(ut, mapid, NewPos, 0, 0);
		ut->SetType(TIP_TYPE_INSTANCE);
	}
	m_RootCtrl->MoveChildTo(ut,NULL);
}

void UTipSystem::HideTip()
{
	//TODO UI的隐藏对系统发出隐藏的信息
	if (m_TipUI_Frame)
	{
		m_TipUI_Frame->SetVisible(FALSE);
	}
	HideComTip();
	//TODO 子系统的内容是否需要清空？其实清空很简单,仅仅是清掉传入系统的ID
	if (m_CurType >= TIP_TYPE_MAX || m_CurType < 0)
	{
		return;
	}
	if(m_CurType == TIP_TYPE_INSTANCE)
		m_TipUI_Instance->SetVisible(FALSE);
	if (m_MouseTip[m_CurType])
	{
		m_MouseTip[m_CurType]->DestoryTip();
	}
	m_CurType = TIP_TYPE_MAX;
	m_pos.Set(0,0);

	m_PlayerGuid = 0;
}
//void UTipSystem::HideMonsterTip()
//{
//	if (m_CurType == TIP_TYPE_MONSTER)
//	{
//		m_TipUI_Art->SetVisible(FALSE);
//	}
//}
void UTipSystem::HideComTip()
{
	for (int i = 0 ; i < 2 ; i++)
	{
		if(m_TipUI_Compara_Frame[i])
		{
			m_TipUI_Compara_Frame[i]->SetVisible(FALSE);
			m_MouseTip[i + TIP_TYPE_ITEM_COMPARA1]->DestoryTip();
		}
	}
}
BOOL UTipSystem::IsComparaTip(MouseTip * mt)
{
	return mt->GetType() == TIP_TYPE_ITEM_COMPARA1 || mt->GetType() == TIP_TYPE_ITEM_COMPARA2;
}
MouseTip * UTipSystem::GetTip(int Type)
{
	if (Type >= TIP_TYPE_SPELL && Type < TIP_TYPE_MAX)
	{
		return m_MouseTip[Type];
	}
	return NULL;
}
void UTipSystem::InitData()
{
	if (m_NameFontFace == NULL)
	{
		m_NameFontFace = UControl::sm_UiRender->CreateFont("ArialBold", 18, 134);
	}
}

UFontPtr UTipSystem::GetNameFontFace()
{
	if (m_NameFontFace)
	{
		return m_NameFontFace;
	}
	return UControl::sm_UiRender->CreateFont("Arial", 18, 134);
}
void UTipSystem::FitTipInWindow()
{
	if (!m_TipUI_Art||!m_TipUI_Frame || !m_TipUI_Compara_Frame[0] || !m_TipUI_Compara_Frame[1] )
	{
		return;
	}
	if (m_CurType == TIP_TYPE_MONSTER)
	{
		if (m_TipUI_Art->IsVisible())
		{
			UPoint NewPos = m_TipUI_Art->GetWindowPos();
			if (m_TipUI_Art->GetWidth() + m_TipUI_Art->GetWindowPos().x > m_RootCtrl->GetWidth())
			{
				NewPos.x = m_RootCtrl->GetWidth() - m_TipUI_Art->GetWidth();
			}
			if (m_TipUI_Art->GetHeight() + m_TipUI_Art->GetWindowPos().y > m_RootCtrl->GetHeight())
			{
				NewPos.y = m_RootCtrl->GetHeight() - m_TipUI_Art->GetHeight();
			}
			m_TipUI_Art->SetPosition(NewPos);
			return;
		}
	}	
	if (m_CurType == TIP_TYPE_INSTANCE)
	{
		if (m_TipUI_Instance->IsVisible())
		{
			UPoint NewPos = m_TipUI_Instance->GetWindowPos();
			if (m_TipUI_Instance->GetWidth() + m_TipUI_Instance->GetWindowPos().x > m_RootCtrl->GetWidth())
			{
				NewPos.x = m_RootCtrl->GetWidth() - m_TipUI_Instance->GetWidth();
			}
			if (m_TipUI_Instance->GetHeight() + m_TipUI_Instance->GetWindowPos().y > m_RootCtrl->GetHeight())
			{
				NewPos.y = m_RootCtrl->GetHeight() - m_TipUI_Instance->GetHeight();
			}
			m_TipUI_Instance->SetPosition(NewPos);
			return;
		}
	}
	int _width = m_TipUI_Frame->GetWidth();
	int _height = m_TipUI_Frame->GetHeight();

	int _Max_width = m_RootCtrl->GetWidth();
	int _Max_Heigth = m_RootCtrl->GetHeight();

	int Type = m_TipUI_Frame->IsVisible() + m_TipUI_Compara_Frame[0]->IsVisible() + m_TipUI_Compara_Frame[1]->IsVisible();
	switch (Type)
	{
	case 0:
		{

		}
		break;
	case 1:
		{
			UPoint _NewPos = m_pos;
			if (m_TipUI_Frame->IsVisible())
			{
				if (_width + m_pos.x > _Max_width)
				{
					_NewPos.x = m_pos.x - _width;
				}
				if (_height + m_pos.y > _Max_Heigth)
				{
					_NewPos.y = m_pos.y - _height;
					if (_NewPos.y <= 0 )
					{
						_NewPos.y = _Max_Heigth - _height  ;
					}
				}
				//if (m_CurType != TIP_TYPE_SPELL)
				{
					m_TipUI_Frame->SetPosition(_NewPos);
				}
			}
		}
		break;
	case 2:
		{
			UPoint _NewPos1 = m_pos;
			UPoint _NewPos2 = UPoint(m_pos.x +_width + 3,m_pos.y);
			if (m_TipUI_Frame->IsVisible() && m_TipUI_Compara_Frame[0]->IsVisible())
			{
				if (2*_width + m_pos.x + 3 > _Max_width)
				{
					_NewPos1.x = m_pos.x - _width;
					_NewPos2.x = m_pos.x - 2*_width - 3;
				}
				if (_height + m_pos.y > _Max_Heigth)
				{
					_NewPos1.y = m_pos.y - _height;
				}
				if (m_TipUI_Compara_Frame[0]->GetHeight() + m_pos.y > _Max_Heigth)
				{
					_NewPos2.y = m_pos.y - m_TipUI_Compara_Frame[0]->GetHeight();
				}

				if (_NewPos1.y < 0 || _NewPos2.y < 0)
				{
					int len = 0;
					if (_height > m_TipUI_Compara_Frame[0]->GetHeight())
					{
						len = _height;
					}else
					{
						len = m_TipUI_Compara_Frame[0]->GetHeight();
					}

					_NewPos1.y = _Max_Heigth - len ;
					_NewPos2.y = _Max_Heigth - len;
				}
				m_TipUI_Frame->SetPosition(_NewPos1);
				m_TipUI_Compara_Frame[0]->SetPosition(_NewPos2);
			}
		}
		break;
	case 3:
		{

		}
		break;
	}
}
void UTipSystem::UpDataTip()
{
	FitTipInWindow();
}
void UTipSystem::QueryServerTime()
{
	mIsUpdateServerTime = false;
	PacketBuilder->SendQueryServerTime();
}
void UTipSystem::OnGetServerTime(ui64 _time)
{
	mIsUpdateServerTime = true;
	mServerTime = _time;
	mLastUpdate = time(NULL);
}
UControl * UTipSystem::GetRootCtrl()
{
	return m_RootCtrl;
}
void UTipSystem::CurseInWindow(int Type)
{
	UInGame * pkInGame = UDynamicCast(UInGame,m_RootCtrl);
	if (pkInGame)
	{
	}
}

void DrawToolTipText(const UPoint& Pos, const URect & updateRect, int BoardWide, int& lineHPos, URender* pPlug, IUFont* font, const WCHAR* text, const UColor& color,EUTextAlignment ta, bool bChangeLine)
{
	UPoint pos,size; 
	pos.x = Pos.x + BoardWide;
	pos.y = Pos.y + lineHPos;
	size.x = updateRect.GetWidth() - 2*BoardWide;
	size.y = font->GetHeight();

	if (!font || !text) 
	{
		if (bChangeLine)
			lineHPos += size.y;
		return;
	}

	UDrawText(pPlug, font, pos, size, color, text, ta);

	if (bChangeLine)
		lineHPos += size.y;
}