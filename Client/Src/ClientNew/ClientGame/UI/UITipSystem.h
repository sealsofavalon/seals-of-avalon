#ifndef UITIPSYSTEM_H
#define UITIPSYSTEM_H
#include "MouseTip.h"
#include "UPayDataInc.h"
#include "Singleton.h"
//Tip系统
//由于原来的Tip系统太多的接口暴露在外面需要调用时非常麻烦，而且在整个界面中的排版也是分开进行实现的，
//所以现在缩减接口，界面中的排版以及显示隐藏功能都放到内部去实现，方便了使用，但是同时代码量也有明显增长
//不过这样会方便以后加入美术资源后的鼠标提示系统，方便查看，这种方法来实现，那么鼠标提示的UI缩放呢？没有考虑周全！！

#define TipSystem UTipSystem::GetSingleton()//这个是整个鼠标提示系统的系统指针只需调用其就可操纵所有的提示信息
enum 
{
	COLOR_POOR_GREY							  = 0,
	COLOR_NORMAL_WHITE						  = 1,
	COLOR_UNCOMMON_GREEN				  = 2,
	COLOR_RARE_BLUE								  = 3,
	COLOR_EPIC_PURPLE							  = 4,
	COLOR_LEGENDARY_ORANGE				  = 5,
	COLOR_ARTIFACT_LIGHT_YELLOW		  = 6,
	COLOR_RED											  = 7,
	COLOR_DEAFULT                            =8,
	COLOR_MAX
};

class UTipSystem : public Singleton<UTipSystem>
{
	SINGLETON(UTipSystem);
public:
	//基本操作：创建，销毁，显示，隐藏
	BOOL CreateSystem(UControl * Ctrl);
	void ShowTip(ui32 id, UPoint pos, PayData* pkData, USkinPtr PayItemSkin, UINT SkinIndex);  //PAYDATA   TIPS
	void ShowTip(int Type , ui32 id , UPoint pos , ui64 guid = 0 ,UINT count = 0);
	void ShowTip(ui32 entryid, UPoint pos, ItemExtraData& Data);
	void ShowTip(UPoint pos, std::vector<std::string>& Str);

	void SetShowPlayerGuid(uint64 guid){m_PlayerGuid = guid;}
	ui64 GetShowPlayerGuid(){return m_PlayerGuid;}

	void ReCreateItemTip();
	void ShowInstanceTip(int32 mapid, UPoint pos);
	void HideTip();//隐藏 隐藏的时候要不要把每个Tip的内容清空？
	//void HideMonsterTip();
public:
	MouseTip * GetTip(int Type);
	//const UColor & GetColor(int Type);
	UFontPtr GetNameFontFace();
	UControl * GetRootCtrl();
	void FitTipInWindow();
	BOOL IsComparaTip(MouseTip * mt);
	void CurseInWindow(int Type);
	void UpDataTip();

	void QueryServerTime();
	void OnGetServerTime(ui64 _time);
	bool mIsUpdateServerTime;
	ui64 mServerTime;
	time_t mLastUpdate;
	USkinPtr mBkgSkin;
private:
	void DestorySystem();
	void InitData();
	void HideComTip();
private:
	MouseTip * m_MouseTip[TIP_TYPE_MAX];
	UControl * m_TipUI_Frame;
	UControl * m_TipUI_Compara_Frame[2];//因为最大的对比框出现是两个所以先用两个
	UControl * m_TipUI_Art;
	UControl * m_TipUI_Instance;
	int m_CurType;
	UControl * m_RootCtrl;
	UColor m_Color[COLOR_MAX];
	UFontPtr m_NameFontFace;
	UPoint m_pos;
	uint64 m_PlayerGuid;
};
void DrawToolTipText(const UPoint& Pos, const URect & updateRect, int BoardWide, int& lineHPos, URender* pPlug, IUFont* font, const WCHAR* text, const UColor& color,EUTextAlignment ta = UT_LEFT, bool bChangeLine = true);

#endif