#include "stdafx.h"
#include "UIWaitList.h"
#include "../Network/PacketBuilder.h"
#include "UISystem.h"
#include "UIChoosePlayer.h"
#include "UIChooseServer.h"

#define  WAIT_LIST_ENABLE_CANCEL_TIME 10.0f

UIMP_CLASS(UWaitList,UDialog);
//事件响应
UBEGIN_MESSAGE_MAP(UWaitList,UDialog)
UON_BN_CLICKED(3, &UWaitList::OnClickCancelWait)
UEND_MESSAGE_MAP()
void UWaitList::StaticInit()
{

}
UWaitList::UWaitList()
{
	m_ServerCtrl = NULL;
	m_WaitNumberCtrl = NULL;
	m_WaitTimeCtrl = NULL;

	m_WaitNum = 0;
	m_Waittime = 0;
	m_CancelBtn = NULL;
	m_CancelBtnBgk = NULL;

	m_bCanDrag = FALSE;
	m_ActiveCancelTime = 0.0f;
	m_ActiveBtn = FALSE;
}

UWaitList::~UWaitList()
{

}
void UWaitList::WaitList(UINT number, UINT time)
{
	UControl* pMsgParent = SYState()->UI->LoadDialogEX(DID_WAITLIST);	
	sm_System->PushDialog(pMsgParent);
	UWaitList* pMessageBox = UDynamicCast(UWaitList, pMsgParent->GetChildByID(100));

	pMessageBox->m_Waittime = time;
	pMessageBox->m_WaitNum = number;
	if (!pMessageBox->m_ActiveBtn)
	{
		pMessageBox->m_ActiveCancelTime = 0.0f;
		pMessageBox->m_ActiveBtn = TRUE;
		pMessageBox->m_CancelBtn->SetVisible(FALSE);
		pMessageBox->m_CancelBtnBgk->SetVisible(FALSE);
	}
	
	
	char buf[_MAX_PATH];
	UINT h, m;
	h = pMessageBox->m_Waittime / 3600;
	m = pMessageBox->m_Waittime % 3600 / 60;
	string str = _TRAN("预计时间:");
	if (h > 0)
	{
		str += _TRAN(N_NOTICE_C91, _I2A(h).c_str(), _I2A(m).c_str());
	}else
	{	if (m == 0)
		{
			m = 1;
		}
		str += _TRAN(N_NOTICE_C93,  _I2A(m).c_str());
	}
	
	sprintf_s(buf, _MAX_PATH, "%s%d", _TRAN("队列位置:"), pMessageBox->m_WaitNum);
	pMessageBox->m_WaitNumberCtrl->SetText(buf);
	pMessageBox->m_WaitTimeCtrl->SetText(str.c_str());

	UChooseServer* pkChoose = UDynamicCast(UChooseServer, SYState()->UI->GetDialogEX(DID_CHOOSESERVER));
	if (pkChoose)
	{
		string serverstr = _TRAN("服务器:");
		serverstr += pkChoose->GetLastServerName();
		pMessageBox->m_ServerCtrl->SetText(serverstr.c_str());
	}
}

void UWaitList::BeginEnter()
{
	UControl* pMsgParent = SYState()->UI->LoadDialogEX(DID_WAITLIST);
	UWaitList* pMessageBox = UDynamicCast(UWaitList, pMsgParent->GetChildByID(100));
	if (pMessageBox)
	{
		pMessageBox->m_Waittime = 0;
		pMessageBox->m_WaitNum = 0;
		pMessageBox->m_ActiveCancelTime = 0.0f;
		pMessageBox->m_ActiveBtn = FALSE;
		pMessageBox->m_CancelBtn->SetVisible(FALSE);
		pMessageBox->m_CancelBtnBgk->SetVisible(FALSE);
	}
	sm_System->PopDialog(pMsgParent);
	
	
}
void UWaitList::OnTimer(float fDeltaTime)
{
	UDialog::OnTimer(fDeltaTime);
	if (m_ActiveBtn && !m_CancelBtn->IsVisible())
	{
		m_ActiveCancelTime += fDeltaTime;
		if (m_ActiveCancelTime >= WAIT_LIST_ENABLE_CANCEL_TIME )
		{
			m_CancelBtn->SetVisible(TRUE);
			m_CancelBtnBgk->SetVisible(TRUE);
		}
	}
}
BOOL UWaitList::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	m_ServerCtrl = UDynamicCast(UStaticText, GetChildByID(0));
	m_WaitNumberCtrl = UDynamicCast(UStaticText, GetChildByID(1));
	m_WaitTimeCtrl = UDynamicCast(UStaticText, GetChildByID(2));
	m_CancelBtn = UDynamicCast(UBitmapButton, GetChildByID(3));
	m_CancelBtnBgk = GetChildByID(103);

	if (!m_ServerCtrl || !m_WaitNumberCtrl || !m_WaitTimeCtrl || !m_CancelBtn || !m_CancelBtnBgk )
	{
		return FALSE ;
	}

	m_ServerCtrl->SetTextColor("dda303");
	m_WaitNumberCtrl->SetTextColor("35a6ce");
	m_WaitTimeCtrl->SetTextColor("dda303");
	m_ServerCtrl->SetTextAlign(UT_CENTER);
	m_WaitNumberCtrl->SetTextAlign(UT_CENTER);
	m_WaitTimeCtrl->SetTextAlign(UT_CENTER);
	m_CancelBtn->SetVisible(FALSE);
	m_CancelBtnBgk->SetVisible(FALSE);

	return TRUE ;
}

void UWaitList::OnClickCancelWait()
{
	PacketBuilder->SendCancelEnterGameLine();
	UWaitList::BeginEnter();
	//重新显示选人界面
	UChoosePlayer* pRechoose = UDynamicCast(UChoosePlayer, SYState()->UI->GetDialogEX(DID_CHOOSEPLAYER));
	if (pRechoose)
	{
		pRechoose->ActiceEnterGameBtn();
	}
	
}