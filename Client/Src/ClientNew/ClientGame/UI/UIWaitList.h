#ifndef __UI_WAIT_LIST_H__
#define __UI_WAIT_LIST_H__

#include "UInc.h"
class UWaitList : public UDialog
{
	UDEC_CLASS(UWaitList);
	UDEC_MESSAGEMAP();
public:
	UWaitList();
	~UWaitList();
	static void WaitList(UINT number, UINT time);
	static void BeginEnter();
protected:
	virtual BOOL OnCreate();
	void OnClickCancelWait();
	virtual void OnTimer(float fDeltaTime);
private:
	UStaticText* m_ServerCtrl;
	UStaticText* m_WaitNumberCtrl;
	UStaticText* m_WaitTimeCtrl;
	UBitmapButton* m_CancelBtn;
	UControl* m_CancelBtnBgk;

	UINT m_WaitNum;
	UINT m_Waittime;
	float m_ActiveCancelTime ;
	BOOL m_ActiveBtn;
};
#endif