#include "stdafx.h"
#include "UInGameBar.h"
#include "UIItemSystem.h"
#include "PlayerInputMgr.h"
#include "UIGamePlay.h"
#include "UIItemSystem.h"
#include "UISystem.h"
#include "ObjectManager.h"
#include "ItemStorage.h"
#include "ItemSlot.h"
#include "UInstanceMgr.h"
#include "SocialitySystem.h"
#include "ItemManager.h"
#include "SystemSetup.h"
#include "USkillButton.h"
#include "UIRightMouseList.h"
#include "Skill/SkillManager.h"

//----------------------------------------------------------------------------------------------------------
ActionBarLock* g_ActionLock = NULL;

#define UAGLINTSIZE_X 16
#define UAGLINTSIZE_Y 16

UIMP_CLASS(UExpBar,UControl);
void UExpBar::StaticInit()
{
	UREG_PROPERTY("ExpBarTexture", UPT_STRING, UFIELD_OFFSET(UExpBar, mExpBarTexturePath));
}
UExpBar::UExpBar()
{
	m_NextLevelExp = 0;
	m_CurExp = 0;
	m_ExpBarTexture = NULL;
	m_ExpFont = NULL;
	m_ExpBarFront = NULL;
}
UExpBar::~UExpBar()
{
	m_NextLevelExp = 0;
	m_CurExp = 0;
	m_ExpBarTexture = NULL;
	m_ExpFont = NULL;
	m_ExpBarFront = NULL;
}
void UExpBar::SetCurExp(ui32 xp)
{
	m_CurExp = xp;
	if (m_NextLevelExp)
	{
		char buf[1024];
		sprintf_s(buf,1024,"%d/%d(%5.2f%%)",m_CurExp,m_NextLevelExp,  ((float)m_CurExp / (float) m_NextLevelExp) * 100.f);
		mRendertext.Set(buf);
	}
}
void UExpBar::SetNextLevelExp(ui32 xp)
{
	m_NextLevelExp = xp;
	if (m_NextLevelExp)
	{
		char buf[1024];
		sprintf_s(buf,1024,"%d/%d(%5.2f%%)",m_CurExp,m_NextLevelExp,  ((float)m_CurExp / (float) m_NextLevelExp) * 100.f);
		mRendertext.Set(buf);
	}
}
BOOL UExpBar::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_ExpBarTexture == NULL)
	{
		if (mExpBarTexturePath.GetBuffer())
		{
			m_ExpBarTexture = sm_UiRender->LoadTexture(mExpBarTexturePath.GetBuffer());
		}
	}
	if (m_ExpBarFront == NULL)
	{
		m_ExpBarFront = sm_UiRender->LoadTexture("DATA\\UI\\InGameBar\\ExpBar_Front.png");
		if (!m_ExpBarFront)
		{
			return FALSE;
		}
	}
	if (m_ExpBarTexture == NULL)
	{
		return FALSE;
	}
	if (m_ExpFont == NULL)
	{
		m_ExpFont = sm_UiRender->CreateFont("Arial",12,134);
		if (!m_ExpFont)
		{
			return FALSE;
		}
	}
	mRendertext.Set(L"0/0(0%)");
	return TRUE;
}
void UExpBar::OnDestroy()
{
	m_NextLevelExp = 0;
	m_CurExp = 0;
	return UControl::OnDestroy();
}
void UExpBar::OnRender(const UPoint& o,const URect &updateRect)
{
	UPoint offset( o );
	if (m_ExpBarTexture == NULL)
	{
		return UControl::OnRender(offset,updateRect);
	}
	int TextureWidth = m_ExpBarTexture->GetWidth();
	int TextureHeight = m_ExpBarTexture->GetHeight();

	URect uvRect;
	uvRect.pt0.x = 0;
	uvRect.pt0.y = 0;
	URect imageRect = updateRect;
	float fexpPos = 0.0f;
	if (m_NextLevelExp == 0 )
	{
		uvRect.pt1.x = 0;
		imageRect.SetWidth(0);
	}
	else
	{
		fexpPos = (float)m_CurExp / (float) m_NextLevelExp;
		if (fexpPos > 1.0f) fexpPos = 1.0f;
		uvRect.pt1.x = int(TextureWidth * fexpPos);
		imageRect.SetWidth(float(updateRect.GetWidth() * fexpPos));
	}
	uvRect.pt1.y = TextureHeight;

	sm_UiRender->DrawImage(m_ExpBarTexture,imageRect,uvRect,LCOLOR_WHITE);
	sm_UiRender->DrawImage(m_ExpBarFront, UPoint(72 + updateRect.pt0.x, updateRect.pt0.y - 3));
	

	if (m_ExpFont)
	{
		int width = m_ExpFont->GetStrWidth(mRendertext);
		int pad = (m_ExpFont->GetHeight() - m_Size.y)/2;
		offset.y -= pad;
		URect DrawRect;
		DrawRect.pt0  = offset;
		DrawRect.pt1.x = offset.x + m_Size.x;
		DrawRect.pt1.y = offset.y + m_ExpFont->GetHeight();

		USetTextEdge(TRUE, UColor(0,0,0,192));
		UDrawText(sm_UiRender, m_ExpFont, DrawRect.GetPosition(), DrawRect.GetSize(), 0xFFFF9900, mRendertext, UT_CENTER);
		USetTextEdge(FALSE);
	}
}
//----------------------------------------------------------------------------------------------------------

#include "SyServerTime.h"
UIMP_CLASS(UClientStateTip,UControl);
void UClientStateTip::StaticInit(){}
UClientStateTip::UClientStateTip()
{
	m_Popo = NULL;
	m_TitleFont = NULL;

	m_Tipstr[0].Set(_TRAN("游戏画面显示的流畅程度，如果画面帧数较低，可以通过视频选项来提高。"));
	m_Tipstr[1].Set(_TRAN("和游戏服务器通讯的平均时间，如果延迟一直很高(红色)的话，那么你的互联网连接可能有问题。"));
}
UClientStateTip::~UClientStateTip(){}
BOOL UClientStateTip::OnCreate()
{
	if(!UControl::OnCreate())
	{
		return FALSE;
	}

	if (m_Popo == NULL)
	{
		m_Popo = sm_System->LoadSkin("SystemTips\\bg.skin");
		if (!m_Popo)
		{
			return FALSE;
		}
	}
	if(m_TitleFont == NULL)
	{
		m_TitleFont = sm_UiRender->CreateFont("Arial", 14, 134);
		if(!m_TitleFont)
			return FALSE;
	}
	return TRUE;
}

void UClientStateTip::OnDestroy()
{
	m_strFPS.clear();
	UControl::OnDestroy();
}

void UClientStateTip::OnTimer(float fDeltaTime)
{
	static float s_fLastTime = 0;
	static DWORD s_uiDelayTotal = 0;
	static unsigned int s_uiFrameNum = 0;
	float fTime = NiGetCurrentTimeInSec();
	s_uiFrameNum += 2;
	s_uiDelayTotal += SYState()->CurDelay;
	if(fTime - s_fLastTime > 1.f)
	{
		float fFps = s_uiFrameNum/(fTime - s_fLastTime);
		/*std::string acFps[256];
		NiSprintf(acFps, sizeof(acFps), "当前帧数：%5.2f fps", fFps);*/
		m_strFPS = _TRAN( N_NOTICE_C10, _F2A( fFps, 1 ).c_str() );
		

		DWORD Delay = s_uiDelayTotal / s_uiFrameNum;
		//NiSprintf(acFps, sizeof(acFps), "当前延迟：%u ms", Delay);
		m_strDelay = _TRAN( N_NOTICE_C11, _I2A( (int)Delay ).c_str() );

		s_fLastTime = fTime;
		s_uiFrameNum = 0;
		s_uiDelayTotal = 0;
	}
}

void UClientStateTip::DO_RenderFPS(UPoint& offset, const URect& updateRect)
{
	offset.y += 3;
	//DrawFps
	UDrawText(sm_UiRender, m_TitleFont, offset, UPoint(updateRect.GetWidth(), m_TitleFont->GetHeight()), LCOLOR_WHITE, m_strFPS.c_str(), UT_LEFT);
	offset.y += m_TitleFont->GetHeight() + 3;
	
	offset.y += UDrawStaticText(sm_UiRender, m_Style->m_spFont, offset, updateRect.GetWidth() - 2 * 3, m_Style->m_FontColor, m_Tipstr[0] ) + 3;

}
void UClientStateTip::DO_RenderDelay(UPoint& offset, const URect& updateRect)
{
	offset.y += 3;
	//DrawDelay;
	UDrawText(sm_UiRender, m_TitleFont, offset, UPoint(updateRect.GetWidth(), m_TitleFont->GetHeight()), LCOLOR_WHITE, m_strDelay.c_str(), UT_LEFT);
	offset.y += m_TitleFont->GetHeight() + 3;
	offset.y += UDrawStaticText(sm_UiRender, m_Style->m_spFont, offset, updateRect.GetWidth() - 2 * 3, m_Style->m_FontColor, m_Tipstr[1] ) + 3;
}
void UClientStateTip::OnRender(const UPoint& offset,const URect &updateRect)
{
	UPoint Pos = offset;
	UDrawResizeImage(RESIZE_WANDH, sm_UiRender, m_Popo, Pos, updateRect.GetSize());
	Pos.x += 6;
	Pos.y += 3;
	DO_RenderDelay(Pos, updateRect);
	DO_RenderFPS(Pos, updateRect);
}
//----------------------------------------------------------------------------------------------------------
UIMP_CLASS(UClientStateCtrl,UControl);

void UClientStateCtrl::StaticInit(){}

UClientStateCtrl::UClientStateCtrl()
{
	mIdleColor.Set(0, 255, 0, 255);
	mBusyColor.Set(255, 0, 0, 255);
	mClientStateBar = NULL;
}
UClientStateCtrl::~UClientStateCtrl()
{
	mClientStateBar = NULL;
}
BOOL UClientStateCtrl::OnCreate()
{
	if(!UControl::OnCreate())
	{
		return FALSE;
	}
	if(mClientStateBar == NULL)
	{
		mClientStateBar = sm_UiRender->LoadTexture("DATA\\UI\\InGameBar\\ClientStateBar.png");
		if(!mClientStateBar)
			return FALSE;
	}
	return TRUE;
}
void UClientStateCtrl::OnDestroy()
{
	UControl::OnDestroy();
}
void UClientStateCtrl::OnRender(const UPoint& offset,const URect &updateRect)
{
	float fDelay = SYState()->CurDelay / 600.f;
	UColor StateColor = mIdleColor;
	if(fDelay > 0.f && fDelay < 0.5f)
		StateColor = mIdleColor;
	else if(fDelay >= 0.5f && fDelay < 1.f)
		StateColor = mIdleColor + mBusyColor;
	else if(fDelay >= 1.f)
		StateColor = mBusyColor;
	sm_UiRender->DrawImage(mClientStateBar, updateRect, StateColor);
	//sm_UiRender->DrawRectFill(updateRect, StateColor);
}
void UClientStateCtrl::OnMouseEnter(const UPoint& position, UINT flags)
{
	UClientStateTip* pStateTip = INGAMEGETFRAME(UClientStateTip, FRAME_IG_CLINETSTATETIP);
	if(pStateTip)
	{
		pStateTip->SetVisible(TRUE);
	}
}
void UClientStateCtrl::OnMouseLeave(const UPoint& position, UINT flags)
{
	UClientStateTip* pStateTip = INGAMEGETFRAME(UClientStateTip, FRAME_IG_CLINETSTATETIP);
	if(pStateTip)
	{
		pStateTip->SetVisible(FALSE);
	}
}
//----------------------------------------------------------------------------------------------------------
UIMP_CLASS(UInGameBar::ActionButton,UDynamicIconButtonEx)
void UInGameBar::ActionButton::StaticInit(){}
UInGameBar::ActionButton::ActionButton()
{
	m_TypeID = UItemSystem::ICT_INGAME_SKILL;
	m_ActionContener.SetActionItem(new UInGameBar::ActionButtonItem);
	m_GlintTexture = NULL ;
	m_bAcceptReDrag = TRUE;
}
UInGameBar::ActionButton::~ActionButton()
{

}
void UInGameBar::ActionButton::FouceUse()
{
	if (m_ActionContener.IsCreate())
	{
		m_ActionContener.FouceUse();
	}

}
void UInGameBar::ActionButton::OnRender(const UPoint& offset, const URect &updateRect)
{

	UDynamicIconButtonEx::OnRender(offset,updateRect);
	OnRenderGlint(offset,updateRect);
}
BOOL UInGameBar::ActionButton::OnCreate()
{
	if (!UDynamicIconButtonEx::OnCreate())
	{
		return FALSE ;
	}

	if (!m_GlintTexture)
	{
		m_GlintTexture = sm_UiRender->LoadTexture("Data\\UI\\InGameBar\\GLINk.tga");
	}
	if (!m_GlintTexture)
	{
		return FALSE ;
	}

	return TRUE ;

}
void UInGameBar::ActionButton::OnTimer(float fDeltaTime)
{
	UDynamicIconButtonEx::OnTimer(fDeltaTime);

	if (mBGlint)
	{
		mGlintTime += fDeltaTime ;
		if (mGlintTime >= 2.0f)
		{
			mGlintTime = 0.f;
			mBGlint = FALSE ;
		}
	}
}

void UInGameBar::ActionButton::OnRenderGlint(const UPoint& offset,const URect &updateRect)
{
	if (mBGlint)
	{
		URect OldClip = sm_UiRender->GetClipRect();

		URect ClipRec;
		ClipRec.pt0.x = updateRect.pt0.x - (UAGLINTSIZE_X>>1);
		ClipRec.pt0.y = updateRect.pt0.y - (UAGLINTSIZE_Y>>1);
		ClipRec.pt1.x = ClipRec.pt0.x + updateRect.GetWidth() + (UAGLINTSIZE_X>>1);
		ClipRec.pt1.y = ClipRec.pt0.y + updateRect.GetHeight() + (UAGLINTSIZE_Y>>1);

		sm_UiRender->SetClipRect(ClipRec);

		UColor Color(255,255,200);
		if (mGlintTime <= 1.0f)
		{
			Color.a = (BYTE)(100 + 155 * mGlintTime);
		}else
		{
			Color.a = (BYTE)(100 + 155 * (2.0f - mGlintTime));
		}
		//Color.g *= alpha ;
		sm_UiRender->DrawImage(m_GlintTexture,ClipRec,Color);
		sm_UiRender->SetClipRect(OldClip);
	}
}
void UInGameBar::ActionButton::OnMouseUp(const UPoint &pt, UINT uFlags)
{
	if (m_ActionContener.IsCreate() && m_Active)
	{
		ReleaseCapture();
		bool bUse = true;
		if(g_ActionLock->IsLock())
		{
			UPoint pos = WindowToScreen(UPoint(0, 0));
			URect rect(pos, m_Size);
			bUse = rect.PointInRect(pt);
		}
		bUse &= !SpecMethod(uFlags);
		m_ActionContener.Release(MOUSEUP_LEFT,  bUse);
		m_BeginDrag = FALSE;
		mBGlint = FALSE ;
	}
}
void UInGameBar::ActionButton::OnMouseDragged(const UPoint& position, UINT flags)
{
	UPoint _pos = WindowToScreen(UPoint(0,0));
	URect IconRect(_pos,m_Size);
	if (m_BeginDrag && !m_ActionContener.IsItemEmpty() && m_ActionContener.IsCreate() && !IconRect.PointInRect(position) && (!g_ActionLock->IsLock() || flags == LKM_SHIFT))
	{
		WORD* pPos = new WORD;
		*pPos = (WORD)m_ActionContener.GetDataInfo()->pos;
		BeginDragDrop(this,m_IconSkin,m_IconIndex,pPos,m_TypeID);
		if (m_ActionContener.GetDataInfo()->type == UItemSystem::ITEM_DATA_FLAG)
		{
			sm_System->__PlaySound(UI_moveitem);
		}
		if (m_ActionContener.GetDataInfo()->type == UItemSystem::SPELL_DATA_FLAG)
		{
			sm_System->__PlaySound(UI_dragSkill);
		}
		m_ActionContener.BeginDrag();
		m_BeginDrag = FALSE;
	}
}

BOOL UInGameBar::ActionButton::OnDropThis(UControl* pDragTo, UINT nDataFlag)
{
	return nDataFlag == m_TypeID;
}

BOOL UInGameBar::ActionButton::OnMouseDownDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	UDynamicIconButtonEx * pDib = UDynamicCast(UDynamicIconButtonEx,pDragFrom);
	if (nDataFlag == UItemSystem::ICT_INGAME_SKILL)
	{
		if (!pDib)
		{
			return FALSE;
		}

		ui32 prevEntry = (GetItemData()->entry == pDib->GetItemData()->entry)?0: GetItemData()->entry;
		ui16 type = GetItemData()->type;

		SYState()->UI->GetUItemSystem()->SetSkillAciton(GetItemData()->pos,pDib->GetItemData()->entry,
			(UItemSystem::ItemType)pDib->GetItemData()->type);

		if (pDib->GetItemData()->pos != GetItemData()->pos)
		{
			SYState()->UI->GetUItemSystem()->SetSkillAciton(pDib->GetItemData()->pos, 0,
				(UItemSystem::ItemType)0);
		}
		//
		SYState()->UI->GetUItemSystem()->SetSkillAciton(255, prevEntry,
			(UItemSystem::ItemType)type);

		return TRUE;
	}
	return OnMouseUpDragDrop(pDragFrom, point, pDragData, nDataFlag);
}

BOOL UInGameBar::ActionButton::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (UDynamicIconButtonEx::OnMouseUpDragDrop(pDragFrom,point,pDragData,nDataFlag))
	{
		UDynamicIconButtonEx * pDib = UDynamicCast(UDynamicIconButtonEx,pDragFrom);

		switch (nDataFlag)
		{
		case UItemSystem::ICT_SKILL:
			{
				if (!pDib)
				{
					return FALSE;
				}

				ui32 prevEntry = (GetItemData()->entry == pDib->GetItemData()->entry)?0:GetItemData()->entry;
				ui16 type = GetItemData()->type;

				SYState()->UI->GetUItemSystem()->SetSkillAciton(GetItemData()->pos,
					pDib->GetItemData()->entry,(UItemSystem::ItemType)pDib->GetItemData()->type);
				//
				SYState()->UI->GetUItemSystem()->SetSkillAciton(255, prevEntry,
					(UItemSystem::ItemType)type);
			}
			return TRUE;
		case UItemSystem::ICT_INGAME_SKILL:
			{
				if (!pDib)
				{
					return FALSE;
				}

				ActionDataInfo datainfo;
				datainfo.entry = GetItemData()->entry;
				datainfo.pos = GetItemData()->pos;
				datainfo.type = GetItemData()->type;

				SYState()->UI->GetUItemSystem()->SetSkillAciton(datainfo.pos,pDib->GetItemData()->entry,
					(UItemSystem::ItemType)pDib->GetItemData()->type);
				SYState()->UI->GetUItemSystem()->SetSkillAciton(pDib->GetItemData()->pos,datainfo.entry,
					(UItemSystem::ItemType)datainfo.type);
			}
			return TRUE;
		case UItemSystem::ICT_BAG:
			{
				ActionDataInfo datainfo;
				ui8* FromePos = (ui8*)pDragData;
				CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
				if (localPlayer)
				{
					CStorage* pContainer = localPlayer->GetItemContainer();
					CItemSlot* pSlot = (CItemSlot*)pContainer->GetSlot(*FromePos);	

					if (!pSlot || pSlot->GetCode() == 0)
					{
						return FALSE;
					}


					if (pSlot->GetItemProperty()->Class == ITEM_CLASS_CONSUMABLE || pSlot->GetItemProperty()->Class == ITEM_CLASS_USE || pSlot->GetItemProperty()->Class == ITEM_CLASS_RECIPE)
					{
						datainfo.num = pContainer->GetItemCnt(pSlot->GetCode());
						datainfo.entry = pSlot->GetCode();
						datainfo.Guid = pSlot->GetGUID();
						datainfo.pos = GetItemData()->pos;
						datainfo.type = ActionButton_Type_Item;

						ui32 prevEntry = (GetItemData()->entry == datainfo.entry)?0: GetItemData()->entry;
						ui16 type = GetItemData()->type;

						SYState()->UI->GetUItemSystem()->SetSkillAciton(datainfo.pos,datainfo.entry,(UItemSystem::ItemType)datainfo.type);
						//
						SYState()->UI->GetUItemSystem()->SetSkillAciton(255, prevEntry,(UItemSystem::ItemType)type);
					}
				}		
				return TRUE;
			}
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

void UInGameBar::ActionButton::OnReciveDragDrop(UControl* pDragFrom, const UPoint& point,const void* pDragData, UINT nDataFlag )
{
	UDynamicIconButtonEx* pTempDib = UDynamicCast(UDynamicIconButtonEx, UInGame::GetFrame(FRAME_IG_ACTIONSKILLBAR)->GetChildByID(10000));
	if (pTempDib)
	{
		WORD* pPos = new WORD;
		*pPos = (WORD)pTempDib->GetItemData()->pos;
		BeginDragDrop(pTempDib, pTempDib->m_IconSkin, pTempDib->m_IconIndex, pPos, UItemSystem::ICT_INGAME_SKILL);
	}

}

void UInGameBar::ActionButton::OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag)
{
	switch (nDataFlag)
	{
	case UItemSystem::ICT_SKILL:
		m_bAcceptDrag = TRUE;
		break;
	case UItemSystem::ICT_INGAME_SKILL:
		m_bAcceptDrag = TRUE;
		break;
	case UItemSystem::ICT_BAG:
		{
			ActionDataInfo datainfo;
			ui8* FromePos = (ui8*)pDragData;
			CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
			if (localPlayer)
			{
				CStorage* pContainer = localPlayer->GetItemContainer();
				CItemSlot* pSlot = (CItemSlot*)pContainer->GetSlot(*FromePos);	

				if (!pSlot || pSlot->GetCode() == 0)
				{
					return;
				}

				if (pSlot->GetItemProperty()->Class == ITEM_CLASS_CONSUMABLE || pSlot->GetItemProperty()->Class == ITEM_CLASS_USE || pSlot->GetItemProperty()->Class == ITEM_CLASS_RECIPE)
				{
					m_bAcceptDrag = TRUE;
				}
			}		
		}
		break;
	}
}
//------------------------------------------------------------------------------------------------------------------
UIMP_CLASS(UInGameBar,UControl);
UBEGIN_MESSAGE_MAP(UInGameBar,UCmdTarget)
UON_BN_CLICKED(UINGAMEBAR_BUTTONEQUIP,&UInGameBar::OnButtonEquip)
UON_BN_CLICKED(UINGAMEBAR_BUTTONPACKAGE,&UInGameBar::OnButtonPackage)
UON_BN_CLICKED(UINGAMEBAR_BUTTONSKILL,&UInGameBar::OnButtonSkill)
UON_BN_CLICKED(UINGAMEBAR_BUTTONFRIEND,&UInGameBar::OnButtonFriend)
UON_BN_CLICKED(UINGAMEBAR_BUTTONSHEQU,&UInGameBar::OnButtonJuanZhu)
UON_BN_CLICKED(UINGAMEBAR_BUTTONQUEST,&UInGameBar::OnButtonQuest)
UON_BN_CLICKED(UINGAMEBAR_BUTTONSHOP,&UInGameBar::OnButtonShop)
UON_BN_CLICKED(UINGAMEBAR_BUTTONGAMESET,&UInGameBar::OnButtonGameSet)
UON_BN_CLICKED(UINGAMEBAR_BUTTONINSTANCE,&UInGameBar::OnButtonInstance)
UON_BN_CLICKED(UINGAMEBAR_BUTTONHELP,&UInGameBar::OnButtonHelp)
UON_BN_CLICKED(UINGAMEBAR_ADDActionBar,&UInGameBar::OnButtonAddBar)
UON_BN_CLICKED(UINGAMEBAR_DelActionBar,&UInGameBar::OnButtonDelBar)
UEND_MESSAGE_MAP()
void OnBarAction(ActionDataInfo& DataInfo)
{
	CPlayerInputMgr * InputMsg = SYState()->LocalPlayerInput;
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal && pkLocal->IsHook())
	{
		return ;
	}
	if (InputMsg && DataInfo.entry != 0 && DataInfo.pos != 255)
	{
		if (DataInfo.type == UItemSystem::SPELL_DATA_FLAG)
		{	
			if ( InputMsg->IsSelectingTarget() )
			{
				InputMsg->EndSelectTarget();
				return;
			}
			InputMsg->BeginSkillAttack((int)DataInfo.entry);
		}else if (DataInfo.type == UItemSystem::ITEM_DATA_FLAG)
		{
			//
			if (DataInfo.entry !=0 )
			{
				CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
				CStorage* pBagItemContainer = pLocal->GetItemContainer();
				CItemSlot* pSlot = (CItemSlot*)pBagItemContainer->GetSlotByEntry(DataInfo.entry);	
				if (!pSlot)
					return ;

				UINT cPos = pBagItemContainer->GetUIPos(pSlot->GetGUID());
				SpellCastTargets pTargets;
				SYState()->UI->GetUItemSystem()->SendUseLeechdomMsg(pBagItemContainer->GetServerBag(cPos), pBagItemContainer->GetServerPos(cPos), 0, pTargets);
			}
		}
	}
}
void UInGameBar::ActionButtonItem::Use()
{
	OnBarAction(m_dataInfo);
}
void UInGameBar::StaticInit()
{

}
UInGameBar::UInGameBar()
{

}
UInGameBar::~UInGameBar()
{

}
BOOL UInGameBar::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	for (int i = UINGAMEBAR_ACTIONBUTTON0 ; i <= UINGAMEBAR_ACTIONBUTTON11 ; i++)
	{
		ActionButton * ab = UDynamicCast(ActionButton,GetChildByID(i));
		if (ab)
		{
			ActionDataInfo datainfo;
			datainfo.pos = i;
			ab->SetItemData(datainfo);
		}
	}

	UButton * pBtn2 = UDynamicCast(UButton,GetChildByID(UINGAMEBAR_DelActionBar)); 
	pBtn2->SetActive(FALSE);
//#ifdef TEST_CLIENT
//	UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONSHEQU));
//	if (btn)
//	{
//		btn->SetActive(FALSE);
//	}
//#endif
	return TRUE;
}
void UInGameBar::OnDestroy()
{
	OnButtonDelBar();
	for (int i = UINGAMEBAR_BUTTONEQUIP; i <=UINGAMEBAR_BUTTONGAMESET ; i++)
	{
		UButton* btn = UDynamicCast(UButton, GetChildByID(i));
		if (btn)
		{
			btn->SetCheck(TRUE);
		}
	}
	return UControl::OnDestroy();
}
void UInGameBar::OnTimer(float fDeltaTime)
{
	return UControl::OnTimer(fDeltaTime);
}
void UInGameBar::UpdateLevelUI(ui32 Level)
{
	INT ID = -1;
	switch (Level)
	{
	case 1:
	case 6:
	case 7:
		ID = UINGAMEBAR_BUTTONQUEST;
		break;
	case 2:
		ID = UINGAMEBAR_BUTTONSKILL;
		break;
	case 3:
		ID = UINGAMEBAR_BUTTONGAMESET;
		break;
	case 4:
	case 12:
		ID = UINGAMEBAR_BUTTONSHEQU; //社会》？
		break;
	case 5:
	case 10:
	case 15:
	case 20:
	case 25:
	case 30:
		ID = UINGAMEBAR_BUTTONINSTANCE;
		break;
	case 8:
	case 11:
		ID = UINGAMEBAR_BUTTONEQUIP;
		break;
	}
	if (ID != -1)
	{
		//GlintInGameBtn(ID,TRUE);
	}

}

void UInGameBar::GlintInGameBtn(UINT index, BOOL bGlint)
{
	UControl* pControl = GetChildByID(index);
	if (pControl)
	{
		pControl->SetGlint(bGlint);
	}
}

void UInGameBar::GlintHelpBtn()
{
	GlintInGameBtn(UINGAMEBAR_BUTTONHELP,TRUE);
	GlintInGameBtn(UINGAMEBAR_BUTTONSHEQU,TRUE);
}

void UInGameBar::OnButtonEquip()
{
	UControl * pEquipFrame = UInGame::GetFrame(FRAME_IG_PLAYEREQUIPDLG);
	if (pEquipFrame)
	{
		UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONEQUIP));
		if (btn)
		{
			pEquipFrame->SetVisible(btn->IsChecked());
		}
	}
}
void UInGameBar::OnButtonPackage()
{
	UControl * pPackageFrame = UInGame::GetFrame(FRAME_IG_PACKAGE);
	if (pPackageFrame)
	{
		UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONPACKAGE));
		if (btn)
		{
			pPackageFrame->SetVisible(btn->IsChecked());

			// when close package , cancel use bangditem .
			if (SYState()->UI->GetUItemSystem()->IsUsedBangdItem() && !btn->IsChecked())
			{
				SYState()->UI->GetUItemSystem()->CancelUseSpecialItem();
			}

			if (SYState()->UI->GetUItemSystem()->IsUsedEnchantItem() && !btn->IsChecked())
			{
				SYState()->UI->GetUItemSystem()->CancelUseSpecialItem();
			}
		}
	}
}
void UInGameBar::OnButtonSkill()
{
	UControl * pSkillFrame = UInGame::GetFrame(FRAME_IG_SKILLDLG);
	if (pSkillFrame)
	{
		UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONSKILL));
		if (btn)
		{
			pSkillFrame->SetVisible(btn->IsChecked());
		}
	}
}
void UInGameBar::OnButtonFriend()
{
	UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONFRIEND));
	if (SocialitySys)
	{
		if(btn->IsChecked())
		{
			SocialitySys->Show();
		}
		else
		{
			SocialitySys->HideAll();
		}
	}
	
}
void UInGameBar::OnButtonJuanZhu()
{
	UControl * pEndowment = UInGame::GetFrame(FRAME_IG_ENDOWMENTDLG);
	if (pEndowment)
	{
		UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONSHEQU));
		if (btn)
		{
			pEndowment->SetVisible(btn->IsChecked());
		}
	}
}
void UInGameBar::OnButtonQuest()
{
	UControl * pQuestFrame = UInGame::GetFrame(FRAME_IG_QUESTMAINDLG);
	if (pQuestFrame)
	{
		UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONQUEST));
		if (btn)
		{
			pQuestFrame->SetVisible(btn->IsChecked());
		}
	}
}
void UInGameBar::OnButtonShop()
{
//#ifndef TEST_CLIENT
//	UControl * pShop = UInGame::GetFrame(FRAME_IG_SHOPDLG);
//	if (pShop)
//	{
//		UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONSHOP));
//		if (btn)
//		{
//			pShop->SetVisible(btn->IsChecked());
//		}
//	}
//#endif
	//SYState()->LocalPlayerInput->KeyDown(UILK_2_NIINPUTKEY(LK_F11));

	UControl * pControl = UInGame::GetFrame(FRAME_IG_MANUFACTURE);
	if (pControl)
	{
		UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONSHOP));
		if (btn)
		{
			pControl->SetVisible(btn->IsChecked());
		}
	}
}
void UInGameBar::OnButtonGameSet()
{
	UControl * pGameSet = UInGame::GetFrame(FRAME_IG_GAMESET);
	if (pGameSet)
	{
		UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONGAMESET));
		if (btn)
		{
			pGameSet->SetVisible(btn->IsChecked());
		}
	}
}
void UInGameBar::OnButtonHelp()
{
	UControl * pNewBird = UInGame::GetFrame(FRAME_IG_NEWBIRD);
	if (pNewBird)
	{
		UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONHELP));
		if (btn)
		{
			pNewBird->SetVisible(btn->IsChecked());
		}
	}
}
void UInGameBar::OnButtonInstance()
{
	UButton* btn = UDynamicCast(UButton, GetChildByID(UINGAMEBAR_BUTTONINSTANCE));
	if (btn)
	{
		if (btn->IsChecked()){
			InstanceSys->ShowFrame();
		}
		else{
			InstanceSys->HideFrame();
		}
	}
}
UPlug_AcutionBar * GetActionBar(INT index)
{
	UPlug_AcutionBar *temp = NULL;
	switch (index)
	{
	case 0:
		temp = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX2);
		break;
	case 1:
		temp = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX3);
		break;
	case 2:
		temp = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX4);
		break;
	case 3:
		temp = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX5);
		break;
	case 4:
		temp = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX6);
		break;
	case 5:
		temp = INGAMEGETFRAME(UPlug_AcutionBar,FRAME_IG_ACTIONSKILLBAREX7);
		break;
	}
	return temp;
}
void UInGameBar::SetIconByPos(ui8 pos, ui32 ditemId, ui16 type,int num)
{
	ActionDataInfo datainfo;
	if (pos < 0  || pos >= INGAME_SHORTCUTKEY_MAX) 
	{
		UActionBar * pAb = INGAMEGETFRAME(UActionBar,FRAME_IG_ACTIONSKILLBAREX1);
		if (pAb)
		{
			pAb->SetIconByPos(pos,ditemId,type,num);
		}
		if (pos >= 34)
		{
			int index = (pos - 34)/10;
			pAb = GetActionBar(index);
			if (pAb)
				pAb->SetIconByPos(pos,ditemId,type,num);
		}
		UDynamicIconButtonEx* pTempDib = UDynamicCast(UDynamicIconButtonEx, GetChildByID(10000));
		if (pTempDib && pos == 255)
		{
			datainfo.entry = ditemId;
			datainfo.Guid = 0;
			datainfo.pos = pos;
			datainfo.type = type;
			pTempDib->SetItemData(datainfo);
		}
		return ;
	}
	datainfo.entry = ditemId;
	datainfo.Guid = 0;
	datainfo.pos = pos;
	datainfo.type = type;


	UDynamicIconButtonEx * button = NULL;

	switch (pos)
	{
	case 0:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON0);
		break;
	case 1:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON1);
		break;
	case 2:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON2);
		break;
	case 3:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON3);
		break;
	case 4:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON4);
		break;
	case 5:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON5);
		break;
	case 6:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON6);
		break;
	case 7:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON7);
		break;
	case 8:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON8);
		break;
	case 9:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON9);
		break;
	case 10:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON10);
		break;
	case 11:
		button = (UDynamicIconButtonEx*)GetChildByID(UINGAMEBAR_ACTIONBUTTON11);
		break;
	}
	if (button)
	{
		button->SetItemData(datainfo);
	}
}
void UInGameBar::UseActionButton(int pos)
{
	UControl * abut = GetChildByID(pos);
	if (abut)
	{
		ActionButton * abtn = UDynamicCast(ActionButton,abut);
		if (abtn)
		{
			abtn->FouceUse();
		}
	}
}
void UInGameBar::SetCurExp(ui32 xp)
{
	UControl * ExpBar = GetChildByID(UINGAMEBAR_EXPBAR);
	if (ExpBar)
	{
		UExpBar * pExpBar = UDynamicCast(UExpBar,ExpBar);
		if (pExpBar)
		{
			pExpBar->SetCurExp(xp);
		}
	}
}
void UInGameBar::SetNLExp(ui32 xp)
{
	UControl * ExpBar = GetChildByID(UINGAMEBAR_EXPBAR);
	if (ExpBar)
	{
		UExpBar * pExpBar = UDynamicCast(UExpBar,ExpBar);
		if (pExpBar)
		{
			pExpBar->SetNextLevelExp(xp);
		}
	}
}
void UInGameBar::SetBottomBarState(bool bstate)
{
	UButton * pBtn1 = UDynamicCast(UButton,GetChildByID(UINGAMEBAR_ADDActionBar)); 
	UButton * pBtn2 = UDynamicCast(UButton,GetChildByID(UINGAMEBAR_DelActionBar)); 
	UActionBar * pAb = INGAMEGETFRAME(UActionBar, FRAME_IG_ACTIONSKILLBAREX1);
	if (pBtn1 && pBtn2 && pAb)
	{
		BOOL bChange =  bstate?!pAb->IsVisible() : pAb->IsVisible();
		if (bChange)
		{
			pAb->SetVisible(bstate);
			UControl* pSSkill = UInGame::GetFrame(FRAME_IG_SUPERSKILL);
			if (pSSkill)
			{
				UPoint pos = pSSkill->GetWindowPos();
				if(bstate)
					pos.y -= pAb->GetHeight();
				else
					pos.y += pAb->GetHeight();
				pSSkill->SetPosition(pos);
			}
		}
		pBtn1->SetActive(!bstate);
		pBtn2->SetActive(bstate);
	}
}

void UInGameBar::SetAddExAcitonBar(bool bstate, int BarIndex)
{
	UPlug_AcutionBar * pAb = GetActionBar(BarIndex);
	if (pAb)
	{
		pAb->SetVisible(bstate);
	}
}

void UInGameBar::OnButtonAddBar()
{
	SetBottomBarState(true);
}
void UInGameBar::OnButtonDelBar()
{
	SetBottomBarState(false);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UActionBar::ActionButton,UDynamicIconButtonEx)
void UActionBar::ActionButton::StaticInit(){}
UActionBar::ActionButton::ActionButton()
{
	m_TypeID = UItemSystem::ICT_INGAME_SKILL;
	m_ActionContener.SetActionItem(new UActionBar::ActionButtonItem);
	m_GlintTexture = NULL ;
	m_bAcceptReDrag = TRUE;
}
UActionBar::ActionButton::~ActionButton()
{

}
void UActionBar::ActionButton::FouceUse()
{
	if (m_ActionContener.IsCreate())
	{
		m_ActionContener.FouceUse();
	}

}

void UActionBar::ActionButton::OnRender(const UPoint& offset, const URect &updateRect)
{

	UDynamicIconButtonEx::OnRender(offset,updateRect);
	OnRenderGlint(offset,updateRect);
}
BOOL UActionBar::ActionButton::OnCreate()
{
	if (!UDynamicIconButtonEx::OnCreate())
	{
		return FALSE ;
	}

	if (!m_GlintTexture)
	{
		m_GlintTexture = sm_UiRender->LoadTexture("Data\\UI\\InGameBar\\GLINk.tga");

	}
	if (!m_GlintTexture)
	{
		return FALSE ;
	}

	return TRUE ;

}
void UActionBar::ActionButton::OnTimer(float fDeltaTime)
{
	UDynamicIconButtonEx::OnTimer(fDeltaTime);

	if (mBGlint)
	{
		mGlintTime += fDeltaTime ;
		if (mGlintTime >= 2.0f)
		{
			mGlintTime = 0.f;
			mBGlint = FALSE ;
		}
	}
}
#define UAGLINTSIZE_X 16
#define UAGLINTSIZE_Y 16
void UActionBar::ActionButton::OnRenderGlint(const UPoint& offset,const URect &updateRect)
{
	if (mBGlint)
	{
		URect OldClip = sm_UiRender->GetClipRect();

		URect ClipRec;
		ClipRec.pt0.x = updateRect.pt0.x - (UAGLINTSIZE_X>>1);
		ClipRec.pt0.y = updateRect.pt0.y - (UAGLINTSIZE_Y>>1);
		ClipRec.pt1.x = ClipRec.pt0.x + updateRect.GetWidth() + (UAGLINTSIZE_X>>1) ;
		ClipRec.pt1.y = ClipRec.pt0.y + updateRect.GetHeight() + (UAGLINTSIZE_Y>>1) ;

		sm_UiRender->SetClipRect(ClipRec);

		UColor Color(255,255,200);
		if (mGlintTime <= 1.0f)
		{
			Color.a = (BYTE)(100 + 155 * mGlintTime);
		}else
		{
			Color.a = (BYTE)(100 + 155 * (2.0f - mGlintTime));
		}
		//Color.g *= alpha ;
		sm_UiRender->DrawImage(m_GlintTexture,ClipRec,Color);
		sm_UiRender->SetClipRect(OldClip);
	}
}

void UActionBar::ActionButton::OnMouseUp(const UPoint &pt, UINT uFlags)
{
	if (m_ActionContener.IsCreate() && m_Active)
	{
		ReleaseCapture();
		bool bUse = true;
		if(g_ActionLock->IsLock())
		{
			UPoint pos = WindowToScreen(UPoint(0, 0));
			URect rect(pos, m_Size);
			bUse = rect.PointInRect(pt);
		}
		bUse &= !SpecMethod(uFlags);

		m_ActionContener.Release(MOUSEUP_LEFT,  bUse);
		m_BeginDrag = FALSE;
		mBGlint = FALSE ;
	}
}

void UActionBar::ActionButton::OnMouseDragged(const UPoint& position, UINT flags)
{
	UPoint _pos = WindowToScreen(UPoint(0,0));
	URect IconRect(_pos,m_Size);
	if (m_BeginDrag && !m_ActionContener.IsItemEmpty() && m_ActionContener.IsCreate() && !IconRect.PointInRect(position) && (!g_ActionLock->IsLock() || flags == LKM_SHIFT))
	{
		WORD* pPos = new WORD;
		*pPos = (WORD)m_ActionContener.GetDataInfo()->pos;
		BeginDragDrop(this,m_IconSkin,m_IconIndex,pPos,m_TypeID);
		if (m_ActionContener.GetDataInfo()->type == UItemSystem::ITEM_DATA_FLAG)
		{
			sm_System->__PlaySound(UI_moveitem);
		}
		if (m_ActionContener.GetDataInfo()->type == UItemSystem::SPELL_DATA_FLAG)
		{
			sm_System->__PlaySound(UI_dragSkill);
		}
		m_ActionContener.BeginDrag();
		m_BeginDrag = FALSE;
	}
}
BOOL UActionBar::ActionButton::OnMouseDownDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	UDynamicIconButtonEx * pDib = UDynamicCast(UDynamicIconButtonEx,pDragFrom);
	if (nDataFlag == UItemSystem::ICT_INGAME_SKILL)
	{
		if (!pDib)
		{
			return FALSE;
		}

		ui32 prevEntry = (GetItemData()->entry == pDib->GetItemData()->entry)?0: GetItemData()->entry;
		ui16 type = GetItemData()->type;

		SYState()->UI->GetUItemSystem()->SetSkillAciton(GetItemData()->pos,pDib->GetItemData()->entry,
			(UItemSystem::ItemType)pDib->GetItemData()->type);

		if (pDib->GetItemData()->pos != GetItemData()->pos)
		{
			SYState()->UI->GetUItemSystem()->SetSkillAciton(pDib->GetItemData()->pos, 0,
				(UItemSystem::ItemType)0);
		}
		//
		SYState()->UI->GetUItemSystem()->SetSkillAciton(255, prevEntry,
			(UItemSystem::ItemType)type);

		return TRUE;
	}
	return OnMouseUpDragDrop(pDragFrom, point, pDragData, nDataFlag);
}
BOOL UActionBar::ActionButton::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{	if (UDynamicIconButtonEx::OnMouseUpDragDrop(pDragFrom,point,pDragData,nDataFlag))
	{
		UDynamicIconButtonEx * pDib = UDynamicCast(UDynamicIconButtonEx,pDragFrom);

		switch (nDataFlag)
		{
		case UItemSystem::ICT_SKILL:
			{
				if (!pDib)
				{
					return FALSE;
				}

				ui32 prevEntry = (GetItemData()->entry == pDib->GetItemData()->entry)?0:GetItemData()->entry;
				ui16 type = GetItemData()->type;

				SYState()->UI->GetUItemSystem()->SetSkillAciton(GetItemData()->pos,
					pDib->GetItemData()->entry,(UItemSystem::ItemType)pDib->GetItemData()->type);
				//
				SYState()->UI->GetUItemSystem()->SetSkillAciton(255, prevEntry,
					(UItemSystem::ItemType)type);
			}
			return TRUE;
		case UItemSystem::ICT_INGAME_SKILL:
			{
				if (!pDib)
				{
					return FALSE;
				}

				ActionDataInfo datainfo;
				datainfo.entry = GetItemData()->entry;
				datainfo.pos = GetItemData()->pos;
				datainfo.type = GetItemData()->type;

				SYState()->UI->GetUItemSystem()->SetSkillAciton(datainfo.pos,pDib->GetItemData()->entry,
					(UItemSystem::ItemType)pDib->GetItemData()->type);
				SYState()->UI->GetUItemSystem()->SetSkillAciton(pDib->GetItemData()->pos,datainfo.entry,
					(UItemSystem::ItemType)datainfo.type);
			}
			return TRUE;
		case UItemSystem::ICT_BAG:
			{
				ActionDataInfo datainfo;
				ui8* FromePos = (ui8*)pDragData;
				CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
				if (localPlayer)
				{
					CStorage* pContainer = localPlayer->GetItemContainer();
					CItemSlot* pSlot = (CItemSlot*)pContainer->GetSlot(*FromePos);	

					if (!pSlot || pSlot->GetCode() == 0)
					{
						return FALSE;
					}


					if (pSlot->GetItemProperty()->Class == ITEM_CLASS_CONSUMABLE || pSlot->GetItemProperty()->Class == ITEM_CLASS_USE || pSlot->GetItemProperty()->Class == ITEM_CLASS_RECIPE)
					{
						datainfo.num = pContainer->GetItemCnt(pSlot->GetCode());
						datainfo.entry = pSlot->GetCode();
						datainfo.Guid = pSlot->GetGUID();
						datainfo.pos = GetItemData()->pos;
						datainfo.type = ActionButton_Type_Item;

						ui32 prevEntry = (GetItemData()->entry == datainfo.entry)?0: GetItemData()->entry;
						ui16 type = GetItemData()->type;

						SYState()->UI->GetUItemSystem()->SetSkillAciton(datainfo.pos,datainfo.entry,(UItemSystem::ItemType)datainfo.type);
						//
						SYState()->UI->GetUItemSystem()->SetSkillAciton(255, prevEntry,(UItemSystem::ItemType)type);
					}
				}		
				return TRUE;
			}
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

BOOL UActionBar::ActionButton::OnDropThis(UControl* pDragTo, UINT nDataFlag)
{
	return nDataFlag == m_TypeID;
}

void UActionBar::ActionButton::OnReciveDragDrop(UControl* pDragFrom, const UPoint& point,const void* pDragData, UINT nDataFlag )
{
	UDynamicIconButtonEx* pTempDib = UDynamicCast(UDynamicIconButtonEx, UInGame::GetFrame(FRAME_IG_ACTIONSKILLBAR)->GetChildByID(10000));
	if (pTempDib)
	{
		WORD* pPos = new WORD;
		*pPos = (WORD)pTempDib->GetItemData()->pos;
		BeginDragDrop(pTempDib, pTempDib->m_IconSkin, pTempDib->m_IconIndex, pPos, UItemSystem::ICT_INGAME_SKILL);
	}
}

void UActionBar::ActionButton::OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag)
{
	switch (nDataFlag)
	{
	case UItemSystem::ICT_SKILL:
		m_bAcceptDrag = TRUE;
		break;
	case UItemSystem::ICT_INGAME_SKILL:
		m_bAcceptDrag = TRUE;
		break;
	case UItemSystem::ICT_BAG:
		{
			ActionDataInfo datainfo;
			ui8* FromePos = (ui8*)pDragData;
			CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
			if (localPlayer)
			{
				CStorage* pContainer = localPlayer->GetItemContainer();
				CItemSlot* pSlot = (CItemSlot*)pContainer->GetSlot(*FromePos);	

				if (!pSlot || pSlot->GetCode() == 0)
				{
					return;
				}

				if (pSlot->GetItemProperty()->Class == ITEM_CLASS_CONSUMABLE || pSlot->GetItemProperty()->Class == ITEM_CLASS_USE || pSlot->GetItemProperty()->Class == ITEM_CLASS_RECIPE)
				{
					m_bAcceptDrag = TRUE;
				}
			}		
		}
		break;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UActionBar,UDialog);
void UActionBar::StaticInit(){}
void UActionBar::ActionButtonItem::Use()
{
	OnBarAction(m_dataInfo);
}
UActionBar::UActionBar()
{
	m_Index = 0;
	m_ButtonNum = 0;
	//m_bTrans = FALSE;
}
UActionBar::~UActionBar()
{
	m_Index = 0;
}
BOOL UActionBar::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	//if (!m_bTrans)
	//{
	//	m_OldPos = m_Position;
	//}else
	//{
	//	//m_spBkgSkin = NULL;
	//}
	return TRUE;
}
void UActionBar::OnDestroy()
{
	//SetVisible(FALSE);
	UControl::OnDestroy();
}
void UActionBar::OnSize(const UPoint& NewSize)
{
	//////////////////////////////////////////////////////////////////////////

	UControl::OnSize(NewSize);
	//////////////////////////////////////////////////////////////////////////
}
void UActionBar::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	UControl::OnMouseDown(pt,nClickCnt,uFlags);
	//m_OldPos = m_Position;
}
void UActionBar::OnMouseUp(const UPoint& pt, UINT uFlags)
{
	UControl::OnMouseUp(pt,uFlags);

	//UControl* pDesktop = GetRoot();
	//assert(pDesktop && pDesktop->IsDesktop());
	//UPoint DesktopSize = pDesktop->GetWindowSize();

	//UInGameBar* pInGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	//if (!pInGameBar)
	//{
	//	return ;
	//}
	////计算右边
	//if (m_Position.x >= DesktopSize.x - m_Size.x && !m_bTrans)
	//{
	//	vector<UINT> pkUActionBarTransR = pInGameBar->GetUActionBarTransR();

	//	Transforn(UPoint(DesktopSize.x - m_Size.y * (pkUActionBarTransR.size() + 1), (DesktopSize.y - m_Size.x)* 0.5f + m_Size.y));
	//	pInGameBar->AddUActionBarTransR(m_Index);
	//	return ;
	//}
	//
	////右边还原时候
	//if (m_Position.x < DesktopSize.x - m_Size.x && m_bTrans)
	//{
	//	Transforn(m_OldPos);
	//	pInGameBar->DelUActionBarTransR(m_Index);

	//	vector<UINT> pkUActionBarTransR = pInGameBar->GetUActionBarTransR();
	//	for(int i= 0; i < pInGameBar->GetUActionBarTransR().size(); i++)
	//	{
	//		UActionBar* pActionBar = INGAMEGETFRAME(UActionBar, pkUActionBarTransR[i]);
	//		if (pActionBar)
	//		{
	//			pActionBar->SetPosition(UPoint(DesktopSize.x - m_Size.y * (i + 1), (DesktopSize.y - m_Size.x)* 0.5f + m_Size.y));
	//		}
	//	}
	//	return ;
	//}


}
void UActionBar::SetBarIndex(INT ButtonNum, INT Index)
{
	m_Index = Index;
	m_ButtonNum = ButtonNum;
	for (int i = 0 ; i < ButtonNum ; i++)
	{
		ActionButton * ab = UDynamicCast(ActionButton,GetChildByID(i));
		if (ab)
		{
			ActionDataInfo datainfo;
			datainfo.pos = Index + i;
			ab->SetItemData(datainfo);
		}
	}
	char buf[1024];
	_itoa((Index- 34)/10 + 1,buf,10);
	UStaticText * ptext = UDynamicCast(UStaticText,GetChildByID(10));
	if (ptext)
	{
		ptext->SetText(buf);
		ptext->SetTextAlign(UT_CENTER);
		ptext->SetTextEdge(true);
	}
}
//void UActionBar::Transforn(UPoint newPos)
//{
//	UPoint ControlnewSize = UPoint(m_Size.y , m_Size.x);
//	vector<UPoint> ChildPos;
//	vector<UPoint> ChilSize;
//	for (int i= 0; i < m_Childs.size(); i++)
//	{
//		UPoint newPos  = m_Childs[i]->GetWindowPos();
//		UPoint newSize = m_Childs[i]->GetWindowSize();
//
//		newPos = UPoint(newPos.y , newPos.x);
//		newSize = UPoint(newSize.y , newSize.x);
//
//		ChildPos.push_back(newPos);
//		ChilSize.push_back(newSize);
//	}
//
//	SetSize(ControlnewSize);
//	SetPosition(newPos);
//	m_bTrans = !m_bTrans;
//
//	for (int i= 0; i < m_Childs.size(); i++)
//	{
//		m_Childs[i]->SetPosition(ChildPos[i]);
//		m_Childs[i]->SetSize(ChilSize[i]);
//
//		if (m_Childs[i]->GetId() == 10)
//		{
//		}
//	}
//
//	if (m_bTrans)
//	{
//		m_spBkgSkin = NULL;
//	}else
//	{
//		m_spBkgSkin = sm_System->LoadSkin(m_strFileName.GetBuffer());
//	}
//	
//	
//
//}
void UActionBar::SetIconByPos(ui8 pos, ui32 ditemId, ui16 type,int num)
{
	int InPos = pos - m_Index;
	if (InPos < 0 || InPos >= m_ButtonNum) 
	{
		return ;
	}
	ActionDataInfo datainfo;
	datainfo.entry = ditemId;
	datainfo.Guid = 0;
	datainfo.pos = pos;
	datainfo.type = type;

	UDynamicIconButtonEx * button = NULL;

	button = UDynamicCast(UDynamicIconButtonEx, GetChildByID(InPos));
	switch (InPos)
	{
	case 0:
		button = (UDynamicIconButtonEx*)GetChildByID(0);
		break;
	case 1:
		button = (UDynamicIconButtonEx*)GetChildByID(1);
		break;
	case 2:
		button = (UDynamicIconButtonEx*)GetChildByID(2);
		break;
	case 3:
		button = (UDynamicIconButtonEx*)GetChildByID(3);
		break;
	case 4:
		button = (UDynamicIconButtonEx*)GetChildByID(4);
		break;
	case 5:
		button = (UDynamicIconButtonEx*)GetChildByID(5);
		break;
	case 6:
		button = (UDynamicIconButtonEx*)GetChildByID(6);
		break;
	case 7:
		button = (UDynamicIconButtonEx*)GetChildByID(7);
		break;
	case 8:
		button = (UDynamicIconButtonEx*)GetChildByID(8);
		break;
	case 9:
		button = (UDynamicIconButtonEx*)GetChildByID(9);
		break;
	}
	if (button)
	{
		button->SetItemData(datainfo);
	}
}

//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UPlug_AcutionBar, UActionBar)
void UPlug_AcutionBar::StaticInit()
{

}

UPlug_AcutionBar::UPlug_AcutionBar()
{
	m_BarHead = NULL;
	m_CurType = 0;
	mLockDrag = false;
}
UPlug_AcutionBar::~UPlug_AcutionBar()
{

}

BOOL UPlug_AcutionBar::OnCreate()
{
	if (!UActionBar::OnCreate())
	{
		return FALSE;
	}
	if (m_BarHead == NULL)
	{
		m_BarHead = sm_System->LoadSkin("InGameBar\\BarBkg.skin");
		if (!m_BarHead)
		{
			return FALSE;
		}
	}
	mLockDrag = false;
	return TRUE;
}

void UPlug_AcutionBar::OnDestroy()
{
	UActionBar::OnDestroy();
}

void UPlug_AcutionBar::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	m_ptLeftBtnDown = pt;
	CaptureControl();
	GetParent()->MoveChildTo(this, NULL);
}

void UPlug_AcutionBar::OnMouseUp(const UPoint& pt, UINT uFlags)
{
	ReleaseCapture();

	char buf[256];
	sprintf(buf, "ActionBar%dPos", m_nCtrlID - FRAME_IG_ACTIONSKILLBAREX2 + 2);
	SystemSetup->SetField(buf, GetWindowPos());
	SystemSetup->SaveFile();
}

void UPlug_AcutionBar::OnMouseDragged(const UPoint& position, UINT flags)
{
	if (!IsCaptured() || mLockDrag)
	{
		return;
	}
	UPoint delta = position - m_ptLeftBtnDown;
	m_ptLeftBtnDown = position;
	UPoint NewPos = GetWindowPos();
	if (this)
	{
		UControl* pDesktop = GetRoot();
		assert(pDesktop && pDesktop->IsDesktop());
		UPoint DesktopSize = pDesktop->GetWindowSize();
		NewPos += delta;
		
		NewPos.x = LClamp(NewPos.x , 0, DesktopSize.x - m_Size.x); 
		NewPos.y = LClamp(NewPos.y , 0, DesktopSize.y - m_Size.y); 
	}else
	{
		NewPos += delta;
	}
	UControl::SetPosition(NewPos);
}

void UPlug_AcutionBar::SetBarIndex(INT ButtonNum, INT Index)
{
	UActionBar::SetBarIndex(ButtonNum, Index);
}

void UPlug_AcutionBar::SetIconByPos(ui8 pos, ui32 ditemId, ui16 type,int num)
{
	int InPos = pos - m_Index;
	if (InPos < 0 || InPos >= m_ButtonNum) 
	{
		return ;
	}
	ActionDataInfo datainfo;
	datainfo.entry = ditemId;
	datainfo.Guid = 0;
	datainfo.pos = pos;
	datainfo.type = type;

	UDynamicIconButtonEx * button = NULL;
	switch (InPos)
	{
	case 0:
		button = (UDynamicIconButtonEx*)GetChildByID(0);
		break;
	case 1:
		button = (UDynamicIconButtonEx*)GetChildByID(1);
		break;
	case 2:
		button = (UDynamicIconButtonEx*)GetChildByID(2);
		break;
	case 3:
		button = (UDynamicIconButtonEx*)GetChildByID(3);
		break;
	case 4:
		button = (UDynamicIconButtonEx*)GetChildByID(4);
		break;
	case 5:
		button = (UDynamicIconButtonEx*)GetChildByID(5);
		break;
	case 6:
		button = (UDynamicIconButtonEx*)GetChildByID(6);
		break;
	case 7:
		button = (UDynamicIconButtonEx*)GetChildByID(7);
		break;
	case 8:
		button = (UDynamicIconButtonEx*)GetChildByID(8);
		break;
	case 9:
		button = (UDynamicIconButtonEx*)GetChildByID(9);
		break;
	}
	if (button)
	{
		button->SetItemData(datainfo);
	}
}

void UPlug_AcutionBar::Lock()
{
	mLockDrag = true;
	char buf[256];
	sprintf(buf, "AcitonBar%dLock", m_nCtrlID - FRAME_IG_ACTIONSKILLBAREX2 + 2);
	SystemSetup->SetField(buf, mLockDrag);
	SystemSetup->SaveFile();
}

void UPlug_AcutionBar::UnLock()
{
	mLockDrag = false;
	char buf[256];
	sprintf(buf, "AcitonBar%dLock", m_nCtrlID - FRAME_IG_ACTIONSKILLBAREX2 + 2);
	SystemSetup->SetField(buf, mLockDrag);
	SystemSetup->SaveFile();
}

void UPlug_AcutionBar::OnRender(const UPoint& offset,const URect &updateRect)
{
	sm_UiRender->DrawSkin(m_BarHead, 0, offset);
	UActionBar::OnRender(offset, updateRect);
}

void UPlug_AcutionBar::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{

}

void UPlug_AcutionBar::OnRightMouseUp(const UPoint& position, UINT flags)
{
	UIRightMouseList* pRML = INGAMEGETFRAME(UIRightMouseList, FRAME_IG_RIGHTMOUSELIST);
	if (pRML)
	{
		pRML->Popup(position, BITMAPLIST_ACTIONBAR, "", m_nCtrlID);
	}
}

void UPlug_AcutionBar::Transform(int type)
{
	UPoint StartPos;
	StartPos.Set(24, 5);
	int Height = 46;
	int Width = 433;
	if (type == 1)
	{
		UPoint pos = StartPos;
		Width = StartPos.x;
		for (int i = 0 ; i < 10 ; ++i)
		{
			UControl* pCtrl = GetChildByID(i);
			if (pCtrl)
			{
				pCtrl->SetPosition(pos);
				int widthAdd = pCtrl->GetWidth() + 1;
				pos.x += widthAdd;
				Width +=  widthAdd;
			}
		}
	}else if(type == 2)
	{
		UPoint pos = StartPos;
		int PosyAdd = 0 ;
		int MaxWidthAdd = 0;
		for (int i = 0 ; i < 3 ; i++)
		{
			UControl* pCtrl = GetChildByID(i);
			if (pCtrl)
			{
				pCtrl->SetPosition(pos);
				int widthAdd = pCtrl->GetWidth() + 1;
				pos.x += widthAdd;
				PosyAdd = max(PosyAdd, pCtrl->GetHeight() + 1);
			}
		}
		pos.x = 0;
		pos.y += PosyAdd;
		for (int i = 0 ; i < 4; i++)
		{
			UControl* pCtrl = GetChildByID(i + 3);
			if (pCtrl)
			{
				pCtrl->SetPosition(pos);
				int widthAdd = pCtrl->GetWidth() + 1;
				pos.x += widthAdd;
				PosyAdd = max(PosyAdd, pCtrl->GetHeight() + 1) ;
			}
		}
		pos.x = StartPos.x;
		pos.y += PosyAdd;

		for (int i = 0 ; i < 3; i++)
		{
			UControl* pCtrl = GetChildByID(i + 7);
			if (pCtrl)
			{
				pCtrl->SetPosition(pos);
				int widthAdd = pCtrl->GetWidth() + 1;
				pos.x += widthAdd;
				PosyAdd = max(PosyAdd, pCtrl->GetHeight() + 1);
			}
		}
		Width = 41 * 4;
		Height = pos.y + PosyAdd - StartPos.y + 5;
	}else if (type == 3)
	{
		UPoint pos = StartPos;
		int MaxChildWidth  = 0;
		for (int i = 0 ; i < 10 ; ++i)
		{
			UControl* pCtrl = GetChildByID(i);
			if (pCtrl)
			{
				pCtrl->SetPosition(pos);
				int heightAdd = pCtrl->GetHeight() + 1;
				pos.y += heightAdd;
				Height +=  heightAdd;
				MaxChildWidth = max(MaxChildWidth, pCtrl->GetWidth());
			}
		}
		Width = StartPos.x + MaxChildWidth + 1;
	}

	m_Size.x = Width;
	m_Size.y = Height;


	char buf[256];
	sprintf(buf, "ActionBar%dType", m_nCtrlID - FRAME_IG_ACTIONSKILLBAREX2 + 2);
	SystemSetup->SetField(buf, type);
	SystemSetup->SaveFile();
	SetPosition(GetWindowPos());

}

void UPlug_AcutionBar::SetPosition( const UPoint &NewPos )
{
	UPoint newnewpos = NewPos;
	UControl* pDesktop = GetRoot();
	assert(pDesktop && pDesktop->IsDesktop());
	UPoint DesktopSize = pDesktop->GetWindowSize();
	newnewpos.x = LClamp(newnewpos.x , 0, DesktopSize.x - m_Size.x); 
	newnewpos.y = LClamp(newnewpos.y , 0, DesktopSize.y - m_Size.y); 

	UControl::SetPosition(newnewpos);

	char buf[256];
	sprintf(buf, "ActionBar%dPos", m_nCtrlID - FRAME_IG_ACTIONSKILLBAREX2 + 2);
	SystemSetup->SetField(buf, GetWindowPos());
	SystemSetup->SaveFile();
}

/////////////////////////////////////////////////////////


ULearnSkillBar::UlearnItem::UlearnItem()
{
}

ULearnSkillBar::UlearnItem::~UlearnItem()
{

}
UIMP_CLASS(ULearnSkillBar::NewSkillButton, UDynamicIconButtonEx)
void ULearnSkillBar::NewSkillButton::StaticInit(){}
ULearnSkillBar::NewSkillButton::NewSkillButton()
{
	mBGlint = TRUE;
}

ULearnSkillBar::NewSkillButton::~NewSkillButton()
{

}


void ULearnSkillBar::NewSkillButton::OnDragDropAccept(UControl* pAcceptCtrl,const UPoint& point,const void* DragData, UINT nDataFlag)
{
	std::list< uint32 >::iterator itSpell;
	
	m_pFather->EraseSpell( GetItemData()->entry );

}

UIMP_CLASS(ULearnSkillBar, UActionBar)
UBEGIN_MESSAGE_MAP(ULearnSkillBar, UActionBar)
UON_BN_RCLICKED(0, &ULearnSkillBar::OnRightClickBtn1)
UON_BN_RCLICKED(1, &ULearnSkillBar::OnRightClickBtn2)
UON_BN_RCLICKED(2, &ULearnSkillBar::OnRightClickBtn3)
UON_BN_RCLICKED(3, &ULearnSkillBar::OnRightClickBtn4)
UEND_MESSAGE_MAP()
void ULearnSkillBar::StaticInit()
{
	
}

ULearnSkillBar::ULearnSkillBar()
{
}

ULearnSkillBar::~ULearnSkillBar()
{

}

void ULearnSkillBar::OnRightClickBtn( int Pos )
{
	if ( Pos > 3 )
	{
		return;
	}
	NewSkillButton* pSkillBtn = UDynamicCast(NewSkillButton, m_Childs[Pos]);
	if (pSkillBtn && pSkillBtn->IsVisible() )
	{
		uint32 spell = pSkillBtn->GetItemData()->entry;
		EraseSpell(spell);
		return;
	}
}

void ULearnSkillBar::OnRightClickBtn1()
{
	OnRightClickBtn( 0 );
}

void ULearnSkillBar::OnRightClickBtn2()
{
	OnRightClickBtn( 1 );
}
void ULearnSkillBar::OnRightClickBtn3()
{
	OnRightClickBtn( 2 );
}
void ULearnSkillBar::OnRightClickBtn4()
{
	OnRightClickBtn( 3 );
}

BOOL ULearnSkillBar::OnCreate()
{
	if (!UActionBar::OnCreate())
	{
		return FALSE;
	}
	for ( int i = 0; i < 4; i++ )
	{
		NewSkillButton* button = NULL;
		button = (NewSkillButton*)GetChildByID(i);
		button->SetVisible( false );
		button->SetActionItem(new UlearnItem );
		button->SetType(UItemSystem::ICT_SKILL );
		button->SetFatherUi( this );
	}
	return TRUE;
}

void ULearnSkillBar::OnDestroy()
{
	UActionBar::OnDestroy();
}


void ULearnSkillBar::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{


}

void ULearnSkillBar::OnMouseUp(const UPoint& pt, UINT uFlags)
{

}

void ULearnSkillBar::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{

}


void ULearnSkillBar::OnRightMouseUp(const UPoint& position, UINT flags)
{
	//UPoint pt = position - GetWindowPos();
	//for (int i = 0 ; i < m_Childs.size() ; ++i)
	//{
	//	if(m_Childs[i]->GetControlRect().PointInRect(pt))
	//	{
	//		UDynamicIconButtonEx* pSkillBtn = UDynamicCast(UDynamicIconButtonEx, m_Childs[i]);
	//		if (pSkillBtn && pSkillBtn->IsVisible() )
	//		{
	//			//pSkillBtn->OnMouseDown(pt, nClickCnt, uFlags);
	//			std::list< uint32 >::iterator itSpell;
	//			uint32 spell = pSkillBtn->GetItemData()->entry;
	//			for (  itSpell = m_learnskills.begin(); itSpell != m_learnskills.end(); ++itSpell )
	//			{
	//				if ( *itSpell == spell )
	//				{
	//					m_learnskills.erase( itSpell );
	//					break;
	//				}
	//			}
	//			ReFresh();
	//			return;
	//		}
	//	}
	//}

}


void ULearnSkillBar::OnMouseDragged(const UPoint& position, UINT flags)
{
}

//UControl* ULearnSkillBar::FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */)
//{
//	return this;
//}



void ULearnSkillBar::ReFresh()
{
	if ( m_learnskills.size() )
	{
		SetVisible( true );
	} 
	else
	{
		SetVisible( false );
	}

	std::list< uint32 >::iterator itSpell;

	for ( int i = 0; i < 4; i++ )
	{
		NewSkillButton* button = NULL;
		button = (NewSkillButton*)GetChildByID(i);
		button->SetVisible( false );
	}
	int pos = 0;
	for (  itSpell = m_learnskills.begin(); itSpell != m_learnskills.end(); ++itSpell )
	{
		NewSkillButton* button = NULL;
		ActionDataInfo info;
		info.type = UItemSystem::SPELL_DATA_FLAG;
		info.entry = *itSpell;


		button = (NewSkillButton*)GetChildByID(pos);
		button->SetItemData( info );
		button->SetVisible( true );
		pos++;
		if ( pos > 3 )
			return;
	}
}

void ULearnSkillBar::AddNewLearnSkill( uint32 spell )
{
	UINT pSkillTest = SYState()->SkillMgr->TestSkillState(spell);
	if (pSkillTest == AttributePassive)
		return;

	m_learnskills.push_back(spell);

	ReFresh();
}

void ULearnSkillBar::EraseSpell( uint32 spell )
{
	std::list< uint32 >::iterator itSpell;
	for (  itSpell = m_learnskills.begin(); itSpell != m_learnskills.end(); ++itSpell )
	{
		if ( *itSpell == spell )
		{
			m_learnskills.erase( itSpell );
			break;
		}
	}
	ReFresh();
}

void ULearnSkillBar::Clear( )
{
	m_learnskills.clear();
	ReFresh();
}



void ULearnSkillBar::OnRender(const UPoint& offset,const URect &updateRect)
{
	UActionBar::OnRender(offset, updateRect);
}

int ULearnSkillBar::GetChildId( const UPoint& position )
{
	return 1;
}