#ifndef __UINGAMEBAR_H__
#define __UINGAMEBAR_H__
#include "UInc.h"
#include "UDynamicIconButtonEx.h"
enum
{
	UINGAMEBAR_ACTIONBUTTON0     = 0 ,
	UINGAMEBAR_ACTIONBUTTON1,
	UINGAMEBAR_ACTIONBUTTON2,
	UINGAMEBAR_ACTIONBUTTON3,
	UINGAMEBAR_ACTIONBUTTON4,
	UINGAMEBAR_ACTIONBUTTON5,
	UINGAMEBAR_ACTIONBUTTON6,
	UINGAMEBAR_ACTIONBUTTON7,
	UINGAMEBAR_ACTIONBUTTON8,
	UINGAMEBAR_ACTIONBUTTON9,
	UINGAMEBAR_ACTIONBUTTON10,
	UINGAMEBAR_ACTIONBUTTON11,
	UINGAMEBAR_BUTTONEQUIP,
	UINGAMEBAR_BUTTONPACKAGE,
	UINGAMEBAR_BUTTONSKILL,
	UINGAMEBAR_BUTTONFRIEND,
	UINGAMEBAR_BUTTONQUEST,
	UINGAMEBAR_BUTTONSHEQU,
	UINGAMEBAR_BUTTONINSTANCE,
	UINGAMEBAR_BUTTONSHOP,
	UINGAMEBAR_BUTTONHELP,
	UINGAMEBAR_BUTTONGAMESET,
	UINGAMEBAR_EXPBAR,
	UINGAMEBAR_BKG,
	UINGAMEBAR_ADDActionBar,
	UINGAMEBAR_DelActionBar
};
class UExpBar : public UControl
{
	UDEC_CLASS(UExpBar);
public:
	UExpBar();
	~UExpBar();
	void SetNextLevelExp(ui32 xp);
	void SetCurExp(ui32 xp);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
private:
	UINT m_NextLevelExp;
	UINT m_CurExp;
	UFontPtr m_ExpFont;
	UTexturePtr m_ExpBarTexture;
	UTexturePtr m_ExpBarFront;
	UString   mExpBarTexturePath;
	UStringW mRendertext;
};
class UClientStateTip : public UControl
{
	UDEC_CLASS(UClientStateTip);
public:
	UClientStateTip();
	~UClientStateTip();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);

	void DO_RenderFPS(UPoint& offset, const URect& updateRect);
	void DO_RenderDelay(UPoint& offset, const URect& updateRect);

	USkinPtr m_Popo;
	UFontPtr m_TitleFont;
	std::string m_strFPS;
	std::string m_strDelay;
	UStringW m_Tipstr[2];
};
class UClientStateCtrl : public UControl
{
	UDEC_CLASS(UClientStateCtrl);
public:
	UClientStateCtrl();
	~UClientStateCtrl();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);

	UColor mIdleColor;
	UColor mBusyColor;
	UTexturePtr mClientStateBar;
};

class ActionBarLock
{
public:
	ActionBarLock(){bActionBarLock = false;};
	inline bool IsLock(){return bActionBarLock;};
	inline void Lock(){bActionBarLock = true;};
	inline void UnLock(){bActionBarLock = false;};

	bool bActionBarLock;
};
extern ActionBarLock* g_ActionLock;

class UInGameBar : public UControl
{
	class ActionButton : public UDynamicIconButtonEx
	{
		UDEC_CLASS(UInGameBar::ActionButton);
		ActionButton();
		virtual ~ActionButton();
	public:
		virtual void FouceUse();
	protected:
		virtual BOOL OnCreate();
		virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
		virtual void OnMouseDragged(const UPoint& position, UINT flags);
		virtual void OnRender(const UPoint& offset, const URect &updateRect);
		virtual void OnRenderGlint(const UPoint& offset,const URect &updateRect);
		virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
		virtual BOOL OnMouseDownDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
		virtual void OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag);
		virtual BOOL OnDropThis(UControl* pDragTo, UINT nDataFlag);
		virtual void OnReciveDragDrop(UControl* pDragFrom, const UPoint& point,const void* pDragData, UINT nDataFlag );
		virtual void OnTimer(float fDeltaTime);
	protected:
		UTexturePtr m_GlintTexture ;
	};
	friend class ActionButton;
	class ActionButtonItem : public UActionItem
	{
	public:
		virtual void Use();
	};
	UDEC_CLASS(UInGameBar);
	UDEC_MESSAGEMAP();
public:
	UInGameBar();
	virtual ~UInGameBar();

	void SetIconByPos(ui8 pos, ui32 ditemId,  ui16 type,int num = 0);
	void UseActionButton(int pos);
	void SetCurExp(ui32 xp);
	void SetNLExp(ui32 xp);
	void UpdateLevelUI(ui32 Level);
	void GlintHelpBtn();
	void GlintInGameBtn(UINT index, BOOL bGlint);
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
private:
	void OnButtonEquip();
	void OnButtonPackage();
	void OnButtonSkill();
	void OnButtonFriend();
	void OnButtonJuanZhu();
	void OnButtonQuest();
	void OnButtonShop();
	void OnButtonGameSet();
	void OnButtonHelp();
	void OnButtonInstance();
	void OnButtonAddBar();
	void OnButtonDelBar();
public:
	void SetBottomBarState(bool bstate);
	void SetAddExAcitonBar(bool bstate, int BarIndex);
};
class UActionBar : public UControl
{	
protected:
	class ActionButton : public UDynamicIconButtonEx
	{
		UDEC_CLASS(UActionBar::ActionButton);
		ActionButton();
		virtual ~ActionButton();
	public:
		virtual void FouceUse();
	protected:
		virtual BOOL OnCreate();
		virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
		virtual void OnMouseDragged(const UPoint& position, UINT flags);
		virtual void OnRender(const UPoint& offset, const URect &updateRect);
		virtual void OnRenderGlint(const UPoint& offset,const URect &updateRect);
		virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
		virtual BOOL OnMouseDownDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
		virtual void OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag);
		virtual BOOL OnDropThis(UControl* pDragTo, UINT nDataFlag);
		virtual void OnReciveDragDrop(UControl* pDragFrom, const UPoint& point,const void* pDragData, UINT nDataFlag );
		virtual void OnTimer(float fDeltaTime);
	protected:
		UTexturePtr m_GlintTexture ;
	};
	friend class ActionButton;
	class ActionButtonItem : public UActionItem
	{
	public:
		virtual void Use();
	};
	UDEC_CLASS(UActionBar);
public:
	UActionBar();
	~UActionBar();

	void Lock();
	void UnLock();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnSize(const UPoint& NewSize);
public:
	virtual void SetBarIndex(INT ButtonNum, INT Index);
	virtual void SetIconByPos(ui8 pos, ui32 ditemId, ui16 type,int num = 0);
	//void Transforn(UPoint newPos);
protected:
	int m_Index;
	int m_ButtonNum;
	//BOOL m_bTrans;
	//UPoint m_OldPos;
};
class UPlug_AcutionBar : public UActionBar
{
	UDEC_CLASS(UPlug_AcutionBar);
public:
	UPlug_AcutionBar();
	~UPlug_AcutionBar();

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);

public:
	virtual void SetPosition( const UPoint &NewPos );
	virtual void SetBarIndex(INT ButtonNum, INT Index);
	virtual void SetIconByPos(ui8 pos, ui32 ditemId, ui16 type,int num = 0);
	void Transform(int type);

	void Lock();
	void UnLock();
	bool IsLock(){return mLockDrag;}
protected:
	UPoint m_ptLeftBtnDown;
	USkinPtr m_BarHead;
	int m_CurType;
	bool mLockDrag;
};


class ULearnSkillBar : public UActionBar
{

	class UlearnItem : public UActionItem
	{
	public:
		UlearnItem();
		virtual~UlearnItem();

		virtual void Use(){;}
	};

	class NewSkillButton : public UDynamicIconButtonEx
	{
		UDEC_CLASS(ULearnSkillBar::NewSkillButton);

		NewSkillButton();
		virtual ~NewSkillButton();

	public:
		void SetFatherUi(ULearnSkillBar* pfather ){ m_pFather = pfather; }
		virtual void SetVisible( BOOL b ){
			UDynamicIconButtonEx::SetVisible( b );
			mBGlint = b;
		}

	protected:
		

		virtual void OnDragDropAccept(UControl* pAcceptCtrl,const UPoint& point,const void* DragData, UINT nDataFlag);


		ULearnSkillBar* m_pFather;
	};

	friend class NewSkillButton;
	friend class UlearnItem;
	UDEC_CLASS(ULearnSkillBar);
	UDEC_MESSAGEMAP();
public:
	ULearnSkillBar();
	~ULearnSkillBar();
	void AddNewLearnSkill( uint32 spell );
	void EraseSpell( uint32 spell );
	void Clear();

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);
	//virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */);

	int  GetChildId( const UPoint& position );

	void ReFresh();

	void OnRightClickBtn( int Pos );

	void OnRightClickBtn1();
	void OnRightClickBtn2();
	void OnRightClickBtn3();
	void OnRightClickBtn4();

	

private:
	std::list< uint32 > m_learnskills;
};


#endif