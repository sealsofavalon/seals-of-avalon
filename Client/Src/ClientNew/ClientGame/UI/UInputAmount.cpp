#include "stdafx.h"
#include "UInputAmount.h"
#include "UIGamePlay.h"
#include "UISystem.h"

UIMP_CLASS(UInputAmount,UDialog);
//事件响应
UBEGIN_MESSAGE_MAP(UInputAmount,UDialog)
UON_BN_CLICKED(0, &UInputAmount::OnClickOK)
UON_BN_CLICKED(1, &UInputAmount::OnClickCancel)
UEND_MESSAGE_MAP()

void UInputAmount::StaticInit()
{

}
UInputAmount::UInputAmount()
{
	m_bOKBtn = NULL;
	m_Amount = NULL;
	m_AmountTxt = NULL;
	m_bCancelBtn = NULL;

	m_MaxCount = 0;
	m_OkFun = NULL;
	m_CancelFun = NULL;

	m_useType = USE_Default ;

}
UInputAmount::~UInputAmount()
{

}
BOOL UInputAmount::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	
	m_bOKBtn = (UBitmapButton*)GetChildByID(0);
	m_Amount = (UEdit*)GetChildByID(3);
	m_bCancelBtn = (UBitmapButton*)GetChildByID(1);
	m_AmountTxt = (UStaticText*)GetChildByID(2);
	if (m_bOKBtn == NULL || m_Amount == NULL || m_bCancelBtn == NULL || m_AmountTxt == NULL)
	{
		return FALSE;
	}
	m_Amount->SetMaxLength(20);
	m_AmountTxt->SetTextColor(UColor(0, 208, 58));
	return TRUE;
}

void UInputAmount::OnDestroy()
{
	UDialog::OnDestroy();
}
void UInputAmount::OnClickCancel()
{
	if (m_CancelFun)
	{
		(*m_CancelFun)();
	}
	OnClose();
}
void UInputAmount::OnClickOK()
{
	char txt[_MAX_PATH];
	if (m_Amount->GetText(txt,_MAX_PATH) == 0)
	{
		UMessageBox::MsgBox_s( _TRAN("Please Enter Quantity!") );
		return;
	}

	if (m_OkFun)
	{
		(*m_OkFun)();
	}

	//这里只是设置为隐藏，由于是确定，肯定会调用GetAmount()来获取数量。 所以在GetAmount()里进行UI的Clear()操作
	SetVisible(FALSE);

	SetFocusControl(sm_System->GetCurFrame());
	
}

string UInputAmount::GetAumontText()
{
	switch(m_useType)
	{
	case BAG_SPLIT:
		return _TRAN("Split quantity") ;
	case BUY_GAME_ITEN:
		return _TRAN("Purchase quantity") ;
	case BUY_GAME_SHOP:
		return _TRAN("Quantity (Mall)");
	}
	return _TRAN("Unknown");
}
string UInputAmount::GetInputText()
{
	char buf[1024];
	m_Amount->GetText(buf,1024);
	return buf;
}
void UInputAmount::SetAmoutTypeDefault()
{
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	UInputAmount* pInPutAount = NULL;
	if (pInGame)
	{
		pInPutAount = INGAMEGETFRAME(UInputAmount,FRAME_IG_INPUTAMOUNTDLG);
	}

	if (pInPutAount->IsVisible() && pInPutAount->m_useType == BUY_GAME_ITEN)
	{
		pInPutAount->OnClose();
	}
}
void UInputAmount::InputAmount(InputAmoutType iType, UINT maxCount, BoxFunction OKFun , BoxFunction CancelFun)
{
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	UInputAmount* pInPutAount = NULL;
	if (pInGame)
	{
		pInPutAount = INGAMEGETFRAME(UInputAmount,FRAME_IG_INPUTAMOUNTDLG);
	}
	
	pInPutAount->m_MaxCount = maxCount;
	//char buf[_MAX_PATH];
	//NiSprintf(buf,_MAX_PATH,"%d", maxCount);
	pInPutAount->m_Amount->SetText("1");
	pInPutAount->m_Amount->SetNumberOnly(TRUE);
	pInPutAount->m_Amount->SetFocusControl();
	pInPutAount->m_useType = iType;
	
	string txt =pInPutAount->GetAumontText();
	pInPutAount->m_AmountTxt->SetText(txt.c_str());
	pInPutAount->m_AmountTxt->SetTextAlign(UT_CENTER);
	pInPutAount->m_OkFun = OKFun;
	pInPutAount->m_CancelFun = CancelFun;
	pInPutAount->SetVisible(TRUE);
}

void UInputAmount::OnClose()
{
	SetVisible(FALSE);
	m_Amount->SetText("");
	m_useType = USE_Default;
	m_MaxCount = 0; 

	SetFocusControl(sm_System->GetCurFrame());
}
int  UInputAmount::GetAmount()
{
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	UInputAmount* pInPutAount = NULL;
	if (pInGame)
	{
		pInPutAount = INGAMEGETFRAME(UInputAmount,FRAME_IG_INPUTAMOUNTDLG);
	}
	char txt[_MAX_PATH];
	if (pInPutAount->m_Amount->GetText(txt,_MAX_PATH) == 0)
	{
		UMessageBox::MsgBox_s( _TRAN("Please enter amount!") );
		return 0;
	}

	UINT Count = atoi(txt);
	if (Count > pInPutAount->m_MaxCount || Count <= 0)
	{
		UMessageBox::MsgBox_s( _TRAN("The number you entered is too large！") );
		Count = 0;
	}
	pInPutAount->m_Amount->SetText("");	
	pInPutAount->m_MaxCount = 0;

	return  Count ;
}