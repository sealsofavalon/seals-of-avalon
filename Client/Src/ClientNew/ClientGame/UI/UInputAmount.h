#ifndef __UINPUTAMOUNT_H__
#define __UINPUTAMOUNT_H__

#include "UMessageBox.h"
#include "UInc.h"

enum InputAmoutType
{
	USE_Default = 0,
	BAG_SPLIT ,   //物品拆分
	BUY_GAME_ITEN,	  //普通NPC处购买
	BUY_GAME_SHOP,    //游戏商城购买
};

class UInputAmount : public UDialog
{
	UDEC_CLASS(UInputAmount);
	UDEC_MESSAGEMAP();
public:
	UInputAmount();
	~UInputAmount();

	static void InputAmount(InputAmoutType iType,UINT maxCount, BoxFunction OKFun , BoxFunction CancelFun = NULL);
	static int  GetAmount();
	static void SetAmoutTypeDefault();
	
	void OnClickOK();
	void OnClickCancel();
	string GetAumontText();
	string GetInputText();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
private:
	UBitmapButton*  m_bOKBtn;
	UBitmapButton* m_bCancelBtn;
	UEdit* m_Amount;
	UStaticText* m_AmountTxt ;
	UINT m_MaxCount ;
	InputAmoutType m_useType ;

	BoxFunction* m_OkFun;
	BoxFunction* m_CancelFun;
};
#endif