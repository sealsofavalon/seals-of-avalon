#include "StdAfx.h"
#include "UInstanceBillBoardDlg.h"
#include "UInstanceMgr.h"
UIMP_CLASS(UBillBoradItem, UControl)

void UBillBoradItem::StaticInit()
{

}
UBillBoradItem::UBillBoradItem()
{
	m_IndexText = NULL;
	m_NameText = NULL;
	m_MarkText = NULL;
	m_ExpText = NULL;
	m_ItemText = NULL;
	m_RaceImage = NULL;
}
UBillBoradItem::~UBillBoradItem()
{
	m_IndexText = NULL;
	m_NameText = NULL;
	m_MarkText = NULL;
	m_ExpText = NULL;
	m_ItemText = NULL;
	m_RaceImage = NULL;
}
BOOL UBillBoradItem::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_IndexText == NULL)
	{
		m_IndexText = UDynamicCast(UStaticText, GetChildByID(0));
		if (m_IndexText)
		{
			m_IndexText->SetTextAlign(UT_CENTER);
		}
		else
		{
			return FALSE;
		}
	}
	if (m_NameText == NULL)
	{
		m_NameText = UDynamicCast(UStaticText, GetChildByID(2));
		if (!m_NameText)
		{
			return FALSE;
		}
		else
		{
			m_NameText->SetTextAlign(UT_CENTER);
		}
	}
	if (m_MarkText == NULL)
	{
		m_MarkText = UDynamicCast(UStaticText, GetChildByID(3));
		if (!m_MarkText)
		{
			return FALSE;
		}
		else
		{
			m_MarkText->SetTextAlign(UT_CENTER);
		}
	}
	if (m_ExpText == NULL)
	{
		m_ExpText = UDynamicCast(UStaticText, GetChildByID(4));
		if (!m_ExpText)
		{
			return FALSE;
		}
		else
		{
			m_ExpText->SetTextAlign(UT_CENTER);
		}
	}
	if (m_ItemText == NULL)
	{
		m_ItemText = UDynamicCast(UStaticText, GetChildByID(5));
		if (!m_ItemText)
		{
			return FALSE;
		}
		else
		{
			m_ItemText->SetTextAlign(UT_CENTER);
		}
	}
	if (m_RaceImage == NULL)
	{
		m_RaceImage = UDynamicCast(UBitmap, GetChildByID(1));
		if (!m_RaceImage)
		{
			return FALSE;
		}
	}
	Clear();
	return TRUE;
}
void UBillBoradItem::OnDestroy()
{
	return UControl::OnDestroy();
}
void UBillBoradItem::SetRank(uint8 rank)
{
	if(m_IndexText)
	{
		char buf[1024];
		sprintf(buf, "%u", rank);

		m_IndexText->SetText(buf);
	}
}
void UBillBoradItem::SetName(const char* Name)
{
	//if (m_NameText)
	//{
	//	m_NameText->SetText(Name);
	//	CPlayer* pPlayer = ObjectMgr->GetLocalPlayer();
	//	if (pPlayer && pPlayer->GetName().Equals(Name))
	//	{
	//		SetColor(UColor(3,168,223,255));
	//	}
	//	else
	//	{
	//		SetColor("eaae9b");
	//	}
	//}
}
void UBillBoradItem::SetMark(ui32 mark)
{
	char buf[1024];
	sprintf(buf, "%u", mark);
	if (m_MarkText)
	{
		m_MarkText->SetText(buf);
	}
}
void UBillBoradItem::SetExp(ui32 exp)
{
	char buf[1024];
	sprintf(buf, "%u", exp);
	if (m_ExpText)
	{
		m_ExpText->SetText(buf);
	}
}
void UBillBoradItem::SetItem(ui32 gold)
{
	char buf[1024];
	sprintf(buf, "%u", gold);
	if (m_ItemText)
	{
		m_ItemText->SetText(buf);
	}
}
void UBillBoradItem::SetColor(UColor color)
{
	m_IndexText->SetTextColor(color);
	m_NameText->SetTextColor(color);
	m_MarkText->SetTextColor(color);
	m_ExpText->SetTextColor(color);
	m_ItemText->SetTextColor(color);
}
void UBillBoradItem::SetColor(const char* color)
{
	m_IndexText->SetTextColor(color);
	m_NameText->SetTextColor(color);
	m_MarkText->SetTextColor(color);
	m_ExpText->SetTextColor(color);
	m_ItemText->SetTextColor(color);
}
void UBillBoradItem::SetRace(ui8 race)
{
	m_RaceImage;
}
void UBillBoradItem::Clear()
{
	m_IndexText->Clear();
	m_NameText->Clear();
	m_MarkText->Clear();
	m_ExpText->Clear();
	m_ItemText->Clear();
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UInstanceBillBoardDlg,UDialog);
UBEGIN_MESSAGE_MAP(UInstanceBillBoardDlg, UDialog)
UON_BN_CLICKED(0, &UInstanceBillBoardDlg::OnClickOk)
UEND_MESSAGE_MAP()
//

void UInstanceBillBoardDlg::StaticInit()
{

}

UInstanceBillBoardDlg::UInstanceBillBoardDlg()
{
	memset(m_BillBoardItem, 0, sizeof(m_BillBoardItem));
	m_bShowClose = false;
}

UInstanceBillBoardDlg::~UInstanceBillBoardDlg()
{

}

BOOL UInstanceBillBoardDlg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	m_bCanDrag = false;
	for (int i = 0 ; i < 5 ; i++)
	{
		UStaticText* pstatext = UDynamicCast(UStaticText, GetChildByID(21+i));
		if (pstatext)
		{
			pstatext->SetTextColor(UColor(238,146,0,255));
		}
	}
	for (int i = 0 ; i < 10 ; i++)
	{
		m_BillBoardItem[i] = UDynamicCast(UBillBoradItem, GetChildByID(i + 1));
		if (!m_BillBoardItem[i])
		{
			return FALSE;
		}
		else
		{
			m_BillBoardItem[i]->SetColor("eaae9b");
		}
	}

	return TRUE;
}

void UInstanceBillBoardDlg::OnDestroy()
{
	UDialog::OnDestroy();
}


void UInstanceBillBoardDlg::OnClickOk()
{
	SetVisible(FALSE);
	for (UINT i = 0 ; i < 10 ; i++)
	{
		m_BillBoardItem[i]->Clear();
	}
}

void UInstanceBillBoardDlg::ShowBillBoard()
{
	SetVisible(TRUE);
	std::vector<MSG_S2C::stInstanceBillboard::info> vinfo;
	
	if(InstanceSys->GetBillBoardInfo(vinfo))
	{
		for (UINT i = 0 ; i < vinfo.size() ; i++)
		{
			m_BillBoardItem[i]->SetName(vinfo[i].name.c_str());
			m_BillBoardItem[i]->SetMark(vinfo[i].kill);
			m_BillBoardItem[i]->SetExp(vinfo[i].exp);
			m_BillBoardItem[i]->SetItem(vinfo[i].gold);
			m_BillBoardItem[i]->SetRace(vinfo[i].race);
			m_BillBoardItem[i]->SetRank(vinfo[i].rank);
		}
	}
}
