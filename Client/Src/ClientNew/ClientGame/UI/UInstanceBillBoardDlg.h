//-----------------------------------------------------------
//竞技场结束，记分牌
//------------------------------------------------------------
#ifndef UINSTANCEBILLBOARDDLG_H
#define UINSTANCEBILLBOARDDLG_H
#include "UInc.h"

class UBillBoradItem : public UControl
{
	UDEC_CLASS(UBillBoradItem);
public:
	UBillBoradItem();
	virtual ~UBillBoradItem();
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
public:
	void SetName(const char* Name);
	void SetMark(ui32 mark);
	void SetExp(ui32 exp);
	void SetItem(ui32 gold);
	void SetRace(ui8 race);
	void SetRank(uint8 rank);

	void SetColor(UColor color);
	void SetColor(const char* color);


	void Clear();

private:
	UStaticText* m_IndexText;
	UStaticText* m_NameText;
	UStaticText* m_MarkText;
	UStaticText* m_ExpText;
	UStaticText* m_ItemText;

	UBitmap* m_RaceImage;
};

class UInstanceBillBoardDlg : public UDialog
{
	UDEC_CLASS(UInstanceBillBoardDlg);
	UDEC_MESSAGEMAP();
public:
	UInstanceBillBoardDlg();
	~UInstanceBillBoardDlg();
public:
	void ShowBillBoard();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	void OnClickOk();
protected:
	UBillBoradItem* m_BillBoardItem[10];
};
#endif