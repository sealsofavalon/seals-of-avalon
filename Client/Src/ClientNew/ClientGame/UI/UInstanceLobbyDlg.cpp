#include "StdAfx.h"
#include "UInstanceLobbyDlg.h"
#include "UInstanceMgr.h"
#include "UITipSystem.h"
#include "AudioInterface.h"

UIMP_CLASS(ULobbyItem, UControl)
UBEGIN_MESSAGE_MAP(ULobbyItem, UControl)
UON_BN_CLICKED(5,&ULobbyItem::OnClickQueue)
UON_BN_CLICKED(6,&ULobbyItem::OnClickQuitQueue)
UEND_MESSAGE_MAP();

void ULobbyItem::StaticInit()
{

}

ULobbyItem::ULobbyItem()
{
	m_NameText = NULL;
	m_LevelLimitText = NULL;
	m_InstacePrint = NULL;
	m_QueueInstanceBtn = NULL;
	m_QuitQueueInstanceBtn = NULL;
	m_InstancePay = NULL;
	m_isQueue = FALSE;
}

ULobbyItem::~ULobbyItem()
{
	m_NameText = NULL;
	m_LevelLimitText = NULL;
	m_InstacePrint = NULL;
	m_QueueInstanceBtn = NULL;
	m_QuitQueueInstanceBtn = NULL;
	m_InstancePay = NULL;
	m_isQueue = FALSE;
}

BOOL ULobbyItem::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_NameText == NULL)
	{
		m_NameText = UDynamicCast(UStaticText, GetChildByID(2));
		if (!m_NameText)
		{
			return FALSE;
		}
	}
	m_NameText->SetTextColor(UColor(0,188,255,255));
	m_NameText->SetTextAlign(UT_CENTER);
	if (m_LevelLimitText == NULL)
	{
		m_LevelLimitText = UDynamicCast(UStaticText, GetChildByID(3));
		if (!m_LevelLimitText)
		{
			return FALSE;
		}
	}
	m_LevelLimitText->SetTextColor("ffe97e");
	m_LevelLimitText->SetTextAlign(UT_CENTER);
	if (m_InstancePay == NULL)
	{
		m_InstancePay = UDynamicCast(UStaticText, GetChildByID(7));
		if (!m_InstancePay)
		{
			return FALSE;
		}
	}
	m_InstancePay->SetTextAlign(UT_CENTER);
	if (m_InstacePrint == NULL)
	{
		m_InstacePrint = UDynamicCast(UBitmap, GetChildByID(4));
		if (!m_InstacePrint)
		{
			return FALSE;
		}
	}
	if (m_QueueInstanceBtn == NULL)
	{
		m_QueueInstanceBtn = UDynamicCast(UBitmapButton, GetChildByID(5));
		if (!m_QueueInstanceBtn)
		{
			return FALSE;
		}
	}
	if (m_QuitQueueInstanceBtn == NULL)
	{
		m_QuitQueueInstanceBtn = UDynamicCast(UBitmapButton, GetChildByID(6));
		if (!m_QuitQueueInstanceBtn)
		{
			return FALSE;
		}
	}
	return TRUE;
}

void ULobbyItem::OnDestroy()
{
	Clear();
	UControl::OnDestroy();
}
UControl* ULobbyItem::FindHitWindow(const UPoint &pt, INT initialLayer)
{
	if(m_InstacePrint->GetControlRect().PointInRect(pt))
	{
		return this;
	}
	return UControl::FindHitWindow(pt, initialLayer);
}

void ULobbyItem::OnMouseEnter(const UPoint& position, UINT flags)
{
	TipSystem->ShowInstanceTip(m_Data.mapid, WindowToScreen(UPoint(0, 0)) + m_Size);
}
void ULobbyItem::OnMouseLeave(const UPoint& position, UINT flags)
{
	TipSystem->HideTip();
}
void ULobbyItem::SetVisible(BOOL value)
{
	for (UINT i = 0 ; i < m_Childs.size() ; i++)
	{
		if (m_Childs[i]->GetId() == 0)
		{
			continue;
		}
		m_Childs[i]->SetVisible(value);
	}
}

void ULobbyItem::OnClickQueue()
{
	InstanceSys->SendQueueInstance(m_Data.mapid);
}

void ULobbyItem::OnClickQuitQueue()
{
	InstanceSys->SendQuitQueueInstance(m_Data.mapid);
}

void ULobbyItem::SetQueueState(bool isQueue)
{
	m_isQueue = isQueue;
	if (isQueue)
	{
		m_QueueInstanceBtn->SetBitmap("Public\\InQueue.skin");
	}
	else
	{
		m_QueueInstanceBtn->SetBitmap("Public\\Queue.skin");
	}
	m_QueueInstanceBtn->SetActive(!isQueue);
	m_QuitQueueInstanceBtn->SetActive(isQueue);
}

void ULobbyItem::SetQueueAbility(bool bQueue)
{
	m_QueueInstanceBtn->SetActive(bQueue);
}

void ULobbyItem::SetTeamState(bool isTeam)
{
	if (isTeam)
	{
		m_QueueInstanceBtn->SetBitmap("Public\\TeamInstance.skin");
	}
	else
	{
		m_QueueInstanceBtn->SetBitmap("Public\\SoloInstance.skin");
	}
}
void ULobbyItem::ValueIt(Sunyou_Instance_ConfigurationEx Data)
{
	m_Data.category = Data.category;
	m_Data.expire = Data.expire;
	m_Data.mapid = Data.mapid;
	m_Data.maxlevel = Data.maxlevel;
	m_Data.maxplayer = Data.maxplayer;
	m_Data.minlevel = Data.minlevel;
	if (!Data.mapid)
		return;

	this->SetVisible(TRUE);

	std::string temp;

	if (m_NameText && InstanceSys->GetInstanceName(Data.mapid, Data.category, temp))
	{
		m_NameText->SetText(temp.c_str());
	}

	if (m_LevelLimitText && InstanceSys->GetInstanceLevelStr(Data.minlevel, Data.maxlevel, temp))
	{
		m_LevelLimitText->SetText(temp.c_str());
	}

	if (m_InstancePay && InstanceSys->GetInstancePayString(Data.cost_type, Data.cost_value1, Data.cost_value2, temp))
	{
		m_InstancePay->SetText(temp.c_str());
	}

	InstanceSys->GetInstanceMiniBitmap(Data.iconid, m_InstacePrint);

	SetQueueState(Data.isQueue);

	SetQueueAbility(Data.isActive);

	SetTeamState(Data.isTeam);
}
void ULobbyItem::Clear()
{
	m_Data.category = INSTANCE_CATEGORY_ARENA;
	m_Data.expire = 0;
	m_Data.mapid = 0;
	m_Data.maxlevel = 0;
	m_Data.maxplayer = 0;
	m_Data.minlevel = 0;
	m_LevelLimitText->Clear();
	m_NameText->Clear();
	SetQueueState(false);
	SetVisible(FALSE);
}
////////////////////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UInstanceLobbyDlg,UDialog);

UBEGIN_MESSAGE_MAP(UInstanceLobbyDlg, UDialog)
UON_BN_CLICKED(16, &UInstanceLobbyDlg::OnClickNextPage)
UON_BN_CLICKED(15, &UInstanceLobbyDlg::OnClickPrevPage)
UON_BN_CLICKED(19, &UInstanceLobbyDlg::OnClickSortAll)
UON_BN_CLICKED(20, &UInstanceLobbyDlg::OnClickSortMonster)
UON_BN_CLICKED(21, &UInstanceLobbyDlg::OnClickSortDynamic)
UON_BN_CLICKED(22, &UInstanceLobbyDlg::OnClickSortEscape)
UON_BN_CLICKED(23, &UInstanceLobbyDlg::OnClickSortPvp)
UON_BN_CLICKED(24, &UInstanceLobbyDlg::OnClickSortRaid)
UON_BN_CLICKED(25, &UInstanceLobbyDlg::OnClickSortFairground)
UEND_MESSAGE_MAP()

void UInstanceLobbyDlg::StaticInit()
{

}

UInstanceLobbyDlg::UInstanceLobbyDlg()
{
	m_CloseBtnPos = UPoint(742,56);
	memset(m_LobbyItem, 0, sizeof(m_LobbyItem));
	m_IsUpdateChild = FALSE;
}

UInstanceLobbyDlg::~UInstanceLobbyDlg()
{

}

void UInstanceLobbyDlg::SetVisible(BOOL value)
{
	if (!value)
		OnClose();
	UDialog::SetVisible(value);
}

BOOL UInstanceLobbyDlg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;

	for (int i = 0 ; i < InstanceNum_InOnePage ; i++)
	{
		if (m_LobbyItem[i] == NULL)
		{
			m_LobbyItem[i] = UDynamicCast(ULobbyItem, GetChildByID(i));

			if (!m_LobbyItem[i])
				return FALSE;

			m_LobbyItem[i]->SetVisible(FALSE);
			m_LobbyItem[i]->SetQueueState(false);
		}
	}

	UBitmapButton* pBitbtn = UDynamicCast(UBitmapButton, GetChildByID(19));
	if (pBitbtn)
	{
		pBitbtn->SetCheck(FALSE);
	}

	return TRUE;
}

void UInstanceLobbyDlg::OnDestroy()
{
	for (int i = 0 ; i < InstanceNum_InOnePage ; i++)
	{
		if (m_LobbyItem[i])
			m_LobbyItem[i]->Clear();
	}
	UDialog::OnDestroy();
}

void UInstanceLobbyDlg::OnClose()
{
	UDialog::SetVisible(FALSE);
	UInGame* pInGame = UInGame::Get();
	if (pInGame)
	{
		UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
		if (pGameBar)
		{
			UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONINSTANCE);
			if (pBtn)
			{
				pBtn->SetCheckState(FALSE);
				//pBtn->SetCheck(TRUE);
			}
		}
	}
}

BOOL UInstanceLobbyDlg::OnEscape()
{
	if (!UControl::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}

void UInstanceLobbyDlg::SetCurrentPage()
{
	std::vector<Sunyou_Instance_ConfigurationEx> vList;
	if(InstanceSys->GetCurrentList(vList))
	{
		int size = vList.size();

		for (int i = 0 ; i < InstanceNum_InOnePage ; i++)
		{
			if (i < size)
			{
				m_LobbyItem[i]->ValueIt(vList[i]);
			}
			else
			{
				m_LobbyItem[i]->Clear();
			}
		}
	}else
	{
		for (int i = 0 ; i < InstanceNum_InOnePage ; i++)
		{
			m_LobbyItem[i]->Clear();
		}
	}
	std::string temp;
	InstanceSys->GetCurrentPageStr(temp);

	UStaticText* pPage = UDynamicCast(UStaticText, GetChildByID(17));
	if (pPage)
	{
		pPage->SetText(temp.c_str());
		pPage->SetTextAlign(UT_CENTER);
	}
}

void UInstanceLobbyDlg::OnClickNextPage()
{
	InstanceSys->NextPage();
	SetCurrentPage();
}

void UInstanceLobbyDlg::OnClickPrevPage()
{
	InstanceSys->PrevPage();
	SetCurrentPage();
}

void UInstanceLobbyDlg::OnClickSortAll()
{
	InstanceSys->SetCurrentListAs(LISTSORT_ALL);
}

void UInstanceLobbyDlg::OnClickSortMonster()
{
	InstanceSys->SetCurrentListAs(LISTSORT_MONSTER);
}

void UInstanceLobbyDlg::OnClickSortDynamic()
{
	InstanceSys->SetCurrentListAs(LISTSORT_DYNAMIC);
}

void UInstanceLobbyDlg::OnClickSortEscape()
{
	InstanceSys->SetCurrentListAs(LISTSORT_ESCAPE);
}

void UInstanceLobbyDlg::OnClickSortPvp()
{
	InstanceSys->SetCurrentListAs(LISTSORT_PVP);
}

void UInstanceLobbyDlg::OnClickSortRaid()
{
	InstanceSys->SetCurrentListAs(LISTSORT_RAID);
}

void UInstanceLobbyDlg::OnClickSortFairground()
{
	InstanceSys->SetCurrentListAs(LISTSORT_FAIRGROUND);
}
