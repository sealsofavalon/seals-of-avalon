//-----------------------------------------------------------
//副本大厅主界面
//------------------------------------------------------------
#ifndef UINSTANCELOBBYDLG_H
#define UINSTANCELOBBYDLG_H
#include "UInc.h"
class ULobbyItem : public UControl
{
	UDEC_CLASS(ULobbyItem);
	UDEC_MESSAGEMAP();
public:
	ULobbyItem();
	~ULobbyItem();
public:
	virtual void SetVisible(BOOL value);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer = -1);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
public:
	void Clear();//清空当前ITem的内容
	void ValueIt(struct Sunyou_Instance_ConfigurationEx Data);//给当前ITEM赋值
	void SetQueueState(bool isQueue);//设置当前ITEM的排队情况
	void SetQueueAbility(bool bQueue);//设置是否可以排队
	void SetTeamState(bool isTeam);//设置自己的队伍状态，是不是队长
protected:
	void OnClickQueue();//按钮排队消息响应函数
	void OnClickQuitQueue();//按钮取消排队消息响应函数

	UStaticText* m_NameText;
	UStaticText* m_LevelLimitText;
	UStaticText* m_InstancePay;

	UBitmapButton* m_QueueInstanceBtn;
	UBitmapButton* m_QuitQueueInstanceBtn;

	UBitmap* m_InstacePrint;

	BOOL m_isQueue;

	struct sunyou_instance_configuration m_Data;
};

class UInstanceLobbyDlg : public UDialog
{
	UDEC_CLASS(UInstanceLobbyDlg);
	UDEC_MESSAGEMAP();
public:
	UInstanceLobbyDlg();
	~UInstanceLobbyDlg();
public:
	virtual void SetVisible(BOOL value);
public:
	void SetCurrentPage();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
protected:
	void OnClickNextPage();
	void OnClickPrevPage();
	void OnClickSortAll();
	void OnClickSortMonster();
	void OnClickSortDynamic();
	void OnClickSortEscape();
	void OnClickSortPvp();
	void OnClickSortRaid();
	void OnClickSortFairground();

	ULobbyItem* m_LobbyItem[8];
};
#endif