#include "StdAfx.h"
#include "UInstanceMgr.h"

#include "UArenaStateDlg.h"
#include "UInstanceBillBoardDlg.h"
#include "UInstanceLobbyDlg.h"
#include "UInstanceTimeText.h"
#include "UInstanceQuitDlg.h"
#include "UIBattleGround.h"

#include "SystemTips .h"
#include "ObjectManager.h"
#include "Player.h"
#include "UIShowPlayer.h"
#include "ItemManager.h"
#include "SocialitySystem.h"
#include "UFun.h"

#include "Utils/MapInfoDB.h"
#include "Utils/InstanceMsg.h"
#include "Network/PacketBuilder.h"

#include "UQuestLog.h"

BOOL UInstanceMgr::CreateUI(UControl* RootCtrl)
{
	BOOL bSuccess = TRUE;
	//副本大厅主界面
	UControl* NewCtrl = NULL;
	if (NewCtrl == NULL && RootCtrl->GetChildByID(FRAME_IG_LOBBY) == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("Lobby\\ULobby.udg");
		if (NewCtrl)
		{
			RootCtrl->AddChild(NewCtrl);
			NewCtrl->SetId(FRAME_IG_LOBBY);
		}
		else
		{
			bSuccess &= FALSE;
		}
	}
	NewCtrl = NULL;
	//副本大厅记分牌
	if (NewCtrl == NULL && RootCtrl->GetChildByID(FRAME_IG_BILLBOARD) == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("Lobby\\UBillBoard.udg");
		if (NewCtrl)
		{
			RootCtrl->AddChild(NewCtrl);
			NewCtrl->SetId(FRAME_IG_BILLBOARD);
		}
		else
		{
			bSuccess &= FALSE;
		}
	}
	//PVP副本状态更新
	NewCtrl = NULL;
	if (NewCtrl == NULL && RootCtrl->GetChildByID(FRAME_IG_ARENASTATE) == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("Lobby\\ArenaState.udg");
		if (NewCtrl)
		{
			RootCtrl->AddChild(NewCtrl);
			NewCtrl->SetId(FRAME_IG_ARENASTATE);
		}
		else
		{
			bSuccess &= FALSE;
		}
	}
	NewCtrl = NULL;
	//副本退出界面
	if (NewCtrl == NULL && RootCtrl->GetChildByID(FRAME_IG_LOBBYQUITDLG) == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("Lobby\\LobbyQuit.udg");
		if (NewCtrl)
		{
			RootCtrl->AddChild(NewCtrl);
			NewCtrl->SetId(FRAME_IG_LOBBYQUITDLG);
		}
		else
		{
			bSuccess &= FALSE;
		}
	}
	NewCtrl = NULL;
	//副本计时器
	if (NewCtrl == NULL && RootCtrl->GetChildByID(FRAME_IG_INSTANCETIME) == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("Lobby\\InstanceTime.udg");
		if (NewCtrl)
		{
			RootCtrl->AddChild(NewCtrl);
			NewCtrl->SetId(FRAME_IG_INSTANCETIME);
		}
		else
		{
			bSuccess &= FALSE;
		}
	}

	NewCtrl = NULL;
	if (NewCtrl == NULL && RootCtrl->GetChildByID(FRAME_IG_BGPOWERUPDATE) == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("Lobby\\BGPowerUpdate.udg");
		if (NewCtrl)
		{
			RootCtrl->AddChild(NewCtrl);
			NewCtrl->SetId(FRAME_IG_BGPOWERUPDATE);
		}
		else
		{
			bSuccess &= FALSE;
		}
	}

	NewCtrl = NULL;
	if (NewCtrl == NULL && RootCtrl->GetChildByID(FRAME_IG_BGBILLBOARD) == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("Lobby\\UBGBillBoard.udg");
		if (NewCtrl)
		{
			RootCtrl->AddChild(NewCtrl);
			NewCtrl->SetId(FRAME_IG_BGBILLBOARD);
		}
		else
		{
			bSuccess &= FALSE;
		}
	}

	NewCtrl = NULL;
	if (NewCtrl == NULL && RootCtrl->GetChildByID(FRAME_IG_FAIRGROUNDMONEY) == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("Lobby\\FairgroundMoney.udg");
		if (NewCtrl)
		{
			RootCtrl->AddChild(NewCtrl);
			NewCtrl->SetId(FRAME_IG_FAIRGROUNDMONEY);
		}
		else
		{
			bSuccess &= FALSE;
		}
	}
	return bSuccess;
}

void UInstanceMgr::InitUI()
{
	mPlayerInInstanceMapid = 0;
	mIsTeamLeader = true;
	mtimecount = 0;
	mtimefcount = 0.0f;
	mLocalLevel = 0;
	m_InstanceMsg.clear();
	m_vBattleGroundPlayer.clear();
	
	mCurrentCategory = -1;
}

void UInstanceMgr::ShowFrame()
{
	UInstanceLobbyDlg* pinstanceLobbyDlg = INGAMEGETFRAME(UInstanceLobbyDlg, FRAME_IG_LOBBY);
	UInstanceQuitDlg* pinstanceQuitDlg = INGAMEGETFRAME(UInstanceQuitDlg, FRAME_IG_LOBBYQUITDLG);
	UBattleGroundBillBoard* pBGBillBoard =	INGAMEGETFRAME(UBattleGroundBillBoard, FRAME_IG_BGBILLBOARD);
	if (mPlayerInInstanceMapid)
	{
		if (mCurrentCategory == INSTANCE_CATEGORY_BATTLE_GROUND || mCurrentCategory == INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND || mCurrentCategory == INSTANCE_CATEGORY_NEW_BATTLE_GROUND)
		{
			pBGBillBoard->SetVisible(TRUE);
		}
		else
			pinstanceQuitDlg->SetVisible(TRUE);
	}
	else
	{
		pinstanceLobbyDlg->SetVisible(TRUE);
	}
}

void UInstanceMgr::HideFrame()
{
	UInstanceLobbyDlg* pinstanceLobbyDlg = INGAMEGETFRAME(UInstanceLobbyDlg, FRAME_IG_LOBBY);
	UInstanceQuitDlg* pinstanceQuitDlg = INGAMEGETFRAME(UInstanceQuitDlg, FRAME_IG_LOBBYQUITDLG);
	UBattleGroundBillBoard* pBGBillBoard =	INGAMEGETFRAME(UBattleGroundBillBoard, FRAME_IG_BGBILLBOARD);
	//UBattleGroundPowerUpdata* pBGPowerUpdate = INGAMEGETFRAME(UBattleGroundPowerUpdata, FRAME_IG_BGPOWERUPDATE);
	//pBGPowerUpdate->SetVisible(FALSE);
	pinstanceLobbyDlg->SetVisible(FALSE);
	pinstanceQuitDlg->SetVisible(FALSE);
	pBGBillBoard->SetVisible(FALSE);
}

UInstanceMgr::UInstanceMgr()
{
	mCurPage = 0;
	mMaxPage = 0;
	m_QueueInstanceMapID.resize(0);
	m_TranceTime = 0.0f;
	mPlayerInInstanceMapid = 0;
	mIsTeamLeader = false;
	mtimecount = 0;
	mtimefcount = 0.0f;
	mLocalLevel = 0;

	mCurrentCategory = -1;

	mIsShowInstanceMsg = false;
	m_CurSortClass = LISTSORT_ALL ;
	m_vBattleGroundPlayer.clear();
}

UInstanceMgr::~UInstanceMgr()
{
	mCurPage = 0;
	mMaxPage = 0;
	m_TranceTime = 0.0f;
	mtimecount = 0;
	mtimefcount = 0.0f;
	m_CurSortClass = LISTSORT_ALL ;
	
	mCurrentCategory = -1;
	m_vBattleGroundPlayer.clear();
}

void UInstanceMgr::ParseQueueUpdata(uint32* mapid)
{
	m_QueueInstanceMapID.clear();
	m_QueueInstanceMapID.push_back(mapid[0]);
	m_QueueInstanceMapID.push_back(mapid[1]);
	m_QueueInstanceMapID.push_back(mapid[2]);
	UpdateCurrentState();
}


bool keyInstanceLevel(sunyou_instance_configuration& l, sunyou_instance_configuration& r)
{
	return l.maxlevel < r.minlevel;
}

void UInstanceMgr::ParseInstanceList(std::vector<sunyou_instance_configuration> &vlist)
{
	mvInstanceList = vlist;
	m_QueueInstanceMapID.resize(0);

	//store the subCategory
	//for(unsigned int ui = 0 ; ui < mvInstanceList.size() ; ui++)
	//{
	//		m_subCategory[mvInstanceList[ui].category].insert(mvInstanceList[ui].iconid);
	//}
	std::map<int, std::vector<sunyou_instance_configuration>>::iterator it ;
	for (unsigned int ui = 0 ; ui < mvInstanceList.size() ; ui++)
	{
		it = m_InstanceMap.find(mvInstanceList[ui].iconid);
		if(it != m_InstanceMap.end())
		{
			it->second.push_back(mvInstanceList[ui]);
		}
		else
		{
			std::vector<sunyou_instance_configuration> vNew;
			vNew.push_back(mvInstanceList[ui]);
			m_InstanceMap.insert(std::map<int, std::vector<sunyou_instance_configuration>>::value_type(mvInstanceList[ui].iconid, vNew));
		}
	}
	for (it = m_InstanceMap.begin(); it != m_InstanceMap.end(); ++it)
	{
		std::sort(it->second.begin(), it->second.end(), &keyInstanceLevel);
	}
	SetCurrentListAs(m_CurSortClass);
	UInstanceLobbyDlg* pInstanceLobbyDlg = INGAMEGETFRAME(UInstanceLobbyDlg, FRAME_IG_LOBBY);
	if (pInstanceLobbyDlg)
	{
		pInstanceLobbyDlg->SetCurrentPage();
	}
}

void UInstanceMgr::ParseInstanceCountDown(uint32 mapid, uint8 second)
{
	m_TranceTime = second;
	CMapInfoDB::MapInfoEntry entry;
	if (g_pkMapInfoDB)
	{
		g_pkMapInfoDB->GetMapInfo(mapid, entry);
	}
	CPlayerLocal* pPlayer = ObjectMgr->GetLocalPlayer();
	if (pPlayer)
	{
		pPlayer->m_TempMapId = mapid;
	}
	m_TranceMapName = entry.desc;
}

void UInstanceMgr::ParseEnterInstance(uint32 timecount, ui32 mapid, ui32 category)
{
	//清理进入副本前
	m_TranceTime = 0.0f;
	mtimecount = 0;
	mtimefcount = 0.f;
	m_TranceMapName.clear();
	m_InstanceMsg.clear();
	mCurrentCategory = category;

	mPlayerInInstanceMapid = mapid;
	if (timecount)
	{
		CPlayerLocal* pPlayer = ObjectMgr->GetLocalPlayer();
		if ( pPlayer )
		{
			if (category == INSTANCE_CATEGORY_ARENA)
			{
				UArenaStateDlg* pArenaStateDlg = INGAMEGETFRAME(UArenaStateDlg, FRAME_IG_ARENASTATE);
				if (pArenaStateDlg)
				{
					pArenaStateDlg->SetVisible(TRUE);
				}
				pPlayer->EnterArena(mapid);
			}
			else
			{
				pPlayer->EnterArena(0);
			}
		}
		mtimecount = timecount;
		mtimefcount = timecount * 1.0f;
		g_pkInstanceMsgDB->GetInstanceVMsg(mapid, mtimefcount, m_InstanceMsg);
	}
	UInstanceQuitDlg* pInstanceQuitDlg = INGAMEGETFRAME(UInstanceQuitDlg, FRAME_IG_LOBBYQUITDLG);
	if (pInstanceQuitDlg)
	{
		pInstanceQuitDlg->SetInstanceMap(mapid);
		pInstanceQuitDlg->UpdataInstanceInfo();
	}

	if (category == INSTANCE_CATEGORY_FAIRGROUND)
	{
		UFairGroundMoneyUpdata* pDlg = INGAMEGETFRAME(UFairGroundMoneyUpdata, FRAME_IG_FAIRGROUNDMONEY);
		if (pDlg)
		{
			pDlg->Update();
			pDlg->SetVisible(TRUE);
		}
	}

	/*UInstanceTimeText* pinstanceTimetext = INGAMEGETFRAME(UInstanceTimeText, FRAME_IG_INSTANCETIME);*/
	if (/*pinstanceTimetext &&*/ category != INSTANCE_CATEGORY_UNIQUE_CASTLE 
		&& category != INSTANCE_CATEGORY_BATTLE_GROUND)
	{
		mIsShowInstanceMsg = true;
		//pinstanceTimetext->Open();
	}
	UHideQuestTraceButton* pkBtnCtr = INGAMEGETFRAME(UHideQuestTraceButton, FRAME_IG_HIDEQUESTTRACEBUTTON);
	pkBtnCtr->OnSetChatHide();

	HideFrame();

	SocialitySys->GetGuildSysPtr()->SendMsgQureyCastleState();
	UInGame::Get()->GetFrame(FRAME_IG_MINIMAP)->GetChildByID(9)->SetVisible(TRUE);
}

void UInstanceMgr::ParseLeaveInstance(ui64 guid, std::string Name)
{
	CPlayer* pPlayer = (CPlayer*)ObjectMgr->GetObject(guid);
	if (pPlayer == ObjectMgr->GetLocalPlayer())
	{
		mPlayerInInstanceMapid = 0;
		mCurrentCategory = -1;

		m_InstanceMsg.clear();

		mtimecount = 0;
		mtimefcount = 0.0f;
		mIsShowInstanceMsg = false ;

		UInstanceTimeText* pinstanceTimetext = INGAMEGETFRAME(UInstanceTimeText, FRAME_IG_INSTANCETIME);
		if (pinstanceTimetext)
		{
			pinstanceTimetext->Close();
		}
		UArenaStateDlg* pArenaStateDlg = INGAMEGETFRAME(UArenaStateDlg, FRAME_IG_ARENASTATE);
		if (pArenaStateDlg)
		{
			pArenaStateDlg->SetVisible(FALSE);
		}
		UpdateCurrentState();

		HideFrame();
		UBattleGroundPowerUpdata* pBGPowerUpdate = INGAMEGETFRAME(UBattleGroundPowerUpdata, FRAME_IG_BGPOWERUPDATE);
		if( pBGPowerUpdate )
			pBGPowerUpdate->SetVisible(FALSE);
		UInGame::Get()->GetFrame(FRAME_IG_MINIMAP)->GetChildByID(9)->SetVisible(FALSE);

		UFairGroundMoneyUpdata* pDlg = INGAMEGETFRAME(UFairGroundMoneyUpdata, FRAME_IG_FAIRGROUNDMONEY);
		if (pDlg)
		{
			pDlg->SetVisible(FALSE);
		}
	}
}

//PVP副本
bool UInstanceMgr::IsUpdateOrInsert(MSG_S2C::stArenaUpdateState::info& Info)
{
	for (UINT i = 0 ; i < mvArenaStateInfo.size() ; i++)
	{
		if (Info.name.compare(mvArenaStateInfo[i].name) == 0)
		{
			mvArenaStateInfo[i].kill = Info.kill;
			return TRUE;
		}
	}
	return FALSE;
}

void UInstanceMgr::ParseAreanUpdateState(std::vector<MSG_S2C::stArenaUpdateState::info> &vinfo)
{
	for (UINT j = 0 ; j < vinfo.size() ; j++)
	{
		std::string str = _TRAN( N_NOTICE_C13, vinfo[j].name.c_str(), _I2A(vinfo[j].kill).c_str() );
		ChatMsgNotify(CHAT_MSG_SYSTEM, str.c_str() /*"%s战胜了%u个人", vinfo[j].name.c_str(), vinfo[j].kill*/);

		if (IsUpdateOrInsert(vinfo[j]))
		{
			continue;
		}
		mvArenaStateInfo.push_back(vinfo[j]);
	}
}

void UInstanceMgr::ParseAreanBillBoard(std::vector<MSG_S2C::stInstanceBillboard::info> &vinfo)
{
	//
	mvArenaStateInfo.clear();
	UArenaStateDlg* pArenaStateDlg = INGAMEGETFRAME(UArenaStateDlg, FRAME_IG_ARENASTATE);
	if (pArenaStateDlg)
	{
		pArenaStateDlg->SetVisible(FALSE);
	}
	//
	mvBillBoardInfo = vinfo;

	UInstanceBillBoardDlg* pBillBoard = INGAMEGETFRAME(UInstanceBillBoardDlg, FRAME_IG_BILLBOARD);
	if (pBillBoard)
	{
		pBillBoard->ShowBillBoard();
	}
	for (UINT i = 0 ; i < mvBillBoardInfo.size() ; i++)
	{
		std::string str = _TRAN( N_NOTICE_C14, mvBillBoardInfo[i].name.c_str(), _I2A(mvBillBoardInfo[i].kill).c_str(), _I2A(mvBillBoardInfo[i].exp).c_str() );
		ChatMsgNotify(CHAT_MSG_SYSTEM, str.c_str() /*"%s战胜了%u个人奖励%u经验值<br>", mvBillBoardInfo[i].name.c_str(), mvBillBoardInfo[i].kill, mvBillBoardInfo[i].exp*/);
	}
}

void UInstanceMgr::ParseBattleGroundPowerUpdate(MSG_S2C::stBattleGroundPowerUpdate& msg)
{
	UBattleGroundPowerUpdata* pBGPowerUpdate = INGAMEGETFRAME(UBattleGroundPowerUpdata, FRAME_IG_BGPOWERUPDATE);
	if (!pBGPowerUpdate)
		return;
	if (!IsPlayerInInstance())
		return;
	pBGPowerUpdate->Update(msg.power, msg.side, msg.num, msg.isParty);
}

void UInstanceMgr::ParseBattleGroundResourceUpdate(MSG_S2C::stBattleGroundResourceUpdate& msg)
{
	UBattleGroundPowerUpdata* pBGPowerUpdate = INGAMEGETFRAME(UBattleGroundPowerUpdata, FRAME_IG_BGPOWERUPDATE);
	if (!pBGPowerUpdate)
		return;
	if (!IsPlayerInInstance())
		return;

	pBGPowerUpdate->UpdateResource(msg.resource, msg.player_count, msg.banner_owner_state);
}

void UInstanceMgr::ParseBattleGroundDetailInformation(MSG_S2C::stBattleGroundDetailInformation& msg)
{
	UBattleGroundBillBoard* pBGBillBoard =	INGAMEGETFRAME(UBattleGroundBillBoard, FRAME_IG_BGBILLBOARD);
	if (!pBGBillBoard)
		return;
	if (!IsPlayerInInstance())
		return;

	//winnerRace == 0 就是战斗进行中
	m_vBattleGroundPlayer = msg.v;
	pBGBillBoard->Update(msg.IsByParty);
	if (msg.WinnerRace >= 1)
	{
		ShowFrame();
	}
}

void UInstanceMgr::SendQueueInstance(uint32 mapid)
{
	PacketBuilder->SendInstanceQueue(mapid);
}

void UInstanceMgr::SendQuitQueueInstance(uint32 mapid)
{
	PacketBuilder->SendInstanceQuitQueue(mapid);
}

void UInstanceMgr::SendInstanceDeadExit(bool bExit)
{
	if(mCurrentCategory == INSTANCE_CATEGORY_TEAM_ARENA)
	{
		UISystemMsg* pSGN = INGAMEGETFRAME(UISystemMsg, FRAME_IG_SYSGIFTNOTIFY);
		if (pSGN)
		{
			pSGN->GetSystemMsg( _TRAN("副本信息：对不起，竞技场内不能退出。"), 4.0f);
			ClientSystemNotify( _TRAN("副本信息：对不起，竞技场内不能退出。") );
		}
		return;
	}
	PacketBuilder->SendInstanceDeadExit(bExit);
}

void UInstanceMgr::OnLevelUpdata(ui32 level)
{
	mLocalLevel = level;
	UpdateCurrentState();
}

void UInstanceMgr::OnTeamUpdata()
{
	CPlayer* pPlayer = (CPlayer*)ObjectMgr->GetLocalPlayer();
	if (pPlayer)
	{
		mIsTeamLeader = TeamShow->IsTeamLeader(pPlayer->GetGUID())?true:false;
	}
	//ui64 LeaderGuid = TeamShow->GetTeamLeaderGuid();
	//if (!LeaderGuid)
	//{
	//	mIsTeamLeader = true;
	//	return;
	//}
	//if (pPlayer && pPlayer->GetGUID() != LeaderGuid)
	//{
	//	mIsTeamLeader = false;
	//}
	UpdateCurrentState();
}

bool UInstanceMgr::GetTeamState()
{
	CPlayer* pPlayer = (CPlayer*)ObjectMgr->GetLocalPlayer();
	if (pPlayer)
	{
		mIsTeamLeader = TeamShow->IsTeamLeader(pPlayer->GetGUID())?true:false;
	}
	return mIsTeamLeader;
}

bool UInstanceMgr::DynamicInstanceShowTest(int type, ui32 mapid)
{
	std::map<int, std::vector<sunyou_instance_configuration>>::iterator it = m_InstanceMap.find(type);
	if (it != m_InstanceMap.end())
	{
		sunyou_instance_configuration* pConfiguration ;
		//findbegin;
		pConfiguration = &*it->second.begin();
		if (mLocalLevel < pConfiguration->minlevel)
		{
			return mapid == pConfiguration->mapid;
		}
		//findlast;
		pConfiguration = &it->second[it->second.size() - 1];
		if (mLocalLevel > pConfiguration->maxlevel)
		{
			return mapid ==  pConfiguration->mapid;
		}
		//if not begin or last , should be Middle.find out.......................
		unsigned int ui;
		for ( ui = 0 ; ui < it->second.size() ; ui++ )
		{
			pConfiguration = &it->second[ui];
			if (mLocalLevel >= pConfiguration->minlevel && mLocalLevel <= pConfiguration->maxlevel)
			{
				return mapid ==  pConfiguration->mapid;
			}
		}
		//if not? yes, you are wrong.
		return false;
	}
	return false;
}

void UInstanceMgr::UpdateCurrentState()
{
	int CurrentShowNum = 0;
	CPlayer* pPlayer = ObjectMgr->GetLocalPlayer();
	if(!pPlayer)
		return;
	for (unsigned int ui = 0 ; ui < mvCurInstanceList.size() ; ui++)
	{
		bool isInQueue = FindInstanceInQueue(mvCurInstanceList[ui].mapid);
		if ( !mvCurInstanceList[ui].isQueue &&  isInQueue )
		{
			if ( mvCurInstanceList[ui].category == INSTANCE_CATEGORY_BATTLE_GROUND 
				|| mvCurInstanceList[ui].category ==INSTANCE_CATEGORY_TEAM_ARENA
				|| mvCurInstanceList[ui].category ==INSTANCE_CATEGORY_NEW_BATTLE_GROUND
				|| mvCurInstanceList[ui].category ==INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND)
			{
				SYState()->IAudio->PlayUiSound( "Sound/UI/inQueue.wav", NiPoint3::ZERO, 1.f, 1 );
			}
		}

		if ( mvCurInstanceList[ui].isQueue &&  !isInQueue )
		{
			/*if ( mvCurInstanceList[ui].category == INSTANCE_CATEGORY_BATTLE_GROUND || mvCurInstanceList[ui].category ==INSTANCE_CATEGORY_TEAM_ARENA) 
			{
				SYState()->IAudio->PlayUiSound( UI_questFailed );
			}*/
		}
		

		mvCurInstanceList[ui].isQueue = isInQueue;
		if (mLocalLevel < mvCurInstanceList[ui].minlevel || mLocalLevel > mvCurInstanceList[ui].maxlevel || m_TranceTime > 0.0f || pPlayer->GetUInt32Value(PLAYER_FIELD_INSTANCE_BUSY))
		{
			mvCurInstanceList[ui].isActive = false;
		}
		else
		{
			//这里符合副本等级，且不在传送副本中
			if (mvCurInstanceList[ui].category != INSTANCE_CATEGORY_ARENA)
			{
				//不是竞技场副本
				if(TeamShow->GetTeamLeaderGuid())
				{
					//组队状况
					if(GetTeamState())
					{
						//是队长
						
						mvCurInstanceList[ui].isActive = true;
						if (mvCurInstanceList[ui].isQueue)
						{
							mvCurInstanceList[ui].isActive = false;
						}
					}
					else
					{
						//不是队长
						mvCurInstanceList[ui].isActive = false;
					}
				}
				else
				{
					//未组队状况
					mvCurInstanceList[ui].isActive = true;
					if (mvCurInstanceList[ui].isQueue)
					{
						mvCurInstanceList[ui].isActive = false;
					}
				}
			}
			else
			{
				//是竞技场副本					
				mvCurInstanceList[ui].isActive = true;
				if (mvCurInstanceList[ui].isQueue)
				{
					mvCurInstanceList[ui].isActive = false;
				}
			}
			//if ( !mIsTeamLeader && mvCurInstanceList[ui].category != INSTANCE_CATEGORY_ARENA)
			//{
			//	mvCurInstanceList[ui].isActive = false;
			//}else if (!mvCurInstanceList[ui].isQueue)
			//{
			//	mvCurInstanceList[ui].isActive = true;
			//}
		}
		mvCurInstanceList[ui].isTeam = mvCurInstanceList[ui].category != INSTANCE_CATEGORY_ARENA?GetTeamState():false;

		mvCurInstanceList[ui].bDynamicShow = DynamicInstanceShowTest(mvCurInstanceList[ui].iconid, mvCurInstanceList[ui].mapid);
		if (mvCurInstanceList[ui].bDynamicShow)
		{
			CurrentShowNum++;
		}
	}

	//mMaxPage = mvCurInstanceList.size() / (InstanceNum_InOnePage + 1);

	mMaxPage = CurrentShowNum/ (InstanceNum_InOnePage + 1);

	UInstanceLobbyDlg* pInstanceLobbyDlg = INGAMEGETFRAME(UInstanceLobbyDlg, FRAME_IG_LOBBY);
	if (pInstanceLobbyDlg)
	{
		pInstanceLobbyDlg->SetCurrentPage();
	}
}

void UInstanceMgr::OnUpdateFairGroundIcon()
{
	UFairGroundMoneyUpdata* pDlg = INGAMEGETFRAME(UFairGroundMoneyUpdata, FRAME_IG_FAIRGROUNDMONEY);
	if (pDlg)
	{
		pDlg->Update();
	}
}

bool UInstanceMgr::GetInstanceName(ui32 mapid, instance_category_t category, std::string &str)
{
	CMapInfoDB::MapInfoEntry entry;
	if (g_pkMapInfoDB)
	{
		g_pkMapInfoDB->GetMapInfo(mapid, entry);
	}
	if (entry.desc.empty())
		return false;

	if(category >= INSTANCE_CATEGORY_NUMBER)
		return false;

	str = _TRAN(CInstanceCategory[category]) + entry.desc;

	return true;
}

bool UInstanceMgr::GetInstanceMiniBitmap(ui32 iconid, UBitmap* pBitMapCtrl)
{
	char buf[256];
	sprintf(buf, CInstanceMiniMapPath, iconid);

	if (!pBitMapCtrl)
		return false;

	return pBitMapCtrl->SetBitMapByStr(buf)?true:false;
}

bool UInstanceMgr::GetInstanceLevelStr(ui32 minLevel, ui32 MaxLevel, std::string &str)
{
	/*char buf[256];
	sprintf(buf, "适合等级：%u - %u", minLevel, MaxLevel);

	if (!strlen(buf))
		return false;*/

	str = _TRAN( N_NOTICE_C15, _I2A(minLevel).c_str(), _I2A(MaxLevel).c_str() );

	return true;
}
void UInstanceMgr::ShowOrHide(BOOL Show)
{
	UInstanceTimeText* pinstanceTimetext = INGAMEGETFRAME(UInstanceTimeText, FRAME_IG_INSTANCETIME);
	if(!pinstanceTimetext)
		return;

	if (mIsShowInstanceMsg && Show && mtimefcount > 0.0f)
		pinstanceTimetext->Open();
	else
		pinstanceTimetext->Close();
}

int UInstanceMgr::GetBattleGroundFlagState(int index)
{
	UBattleGroundPowerUpdata* pBGPowerUpdate = INGAMEGETFRAME(UBattleGroundPowerUpdata, FRAME_IG_BGPOWERUPDATE);
	if (pBGPowerUpdate)
	{
		return pBGPowerUpdate->GetFlagState(index);
	}
	return 0;
}

void UInstanceMgr::NextPage()
{
	mCurPage++;
	if (mCurPage >= mMaxPage)
	{
		mCurPage = mMaxPage;
	}
	UpdateCurrentState();
}

void UInstanceMgr::PrevPage()
{
	if (mCurPage > 0)
	{
		mCurPage--;
	}
	UpdateCurrentState();
}

void UInstanceMgr::SetCurrentListAs(ListSortClass LSCLASS)
{
	//不需要每次都查询！
	//if (m_CurSortClass == LSCLASS)
	//{
		//UpdateCurrentState();
		//return ;
	//}
	m_CurSortClass = LSCLASS;

	mCurPage = 0;
	mvCurInstanceList.clear();
	switch(LSCLASS)
	{
	case LISTSORT_ALL:
		{
			for (unsigned int ui = 0 ; ui < mvInstanceList.size() ; ui++)
			{
					Sunyou_Instance_ConfigurationEx temp;
					temp = mvInstanceList[ui];
					temp.isQueue = FindInstanceInQueue(temp.mapid);
					mvCurInstanceList.push_back(temp);
			}
		}
		break;
	case LISTSORT_MONSTER:
		{
			for (unsigned int ui = 0 ; ui < mvInstanceList.size() ; ui++)
			{
				if(mvInstanceList[ui].category == INSTANCE_CATEGORY_REFRESH_MONSTER)
				{
					Sunyou_Instance_ConfigurationEx temp;
					temp = mvInstanceList[ui];
					temp.isQueue = FindInstanceInQueue(temp.mapid);
					mvCurInstanceList.push_back(temp);
				}
			}
		}
		break;
	case LISTSORT_DYNAMIC:
		{
			for (unsigned int ui = 0 ; ui < mvInstanceList.size() ; ui++)
			{
				if(mvInstanceList[ui].category == INSTANCE_CATEGORY_DYNAMIC)
				{
					Sunyou_Instance_ConfigurationEx temp;
					temp = mvInstanceList[ui];
					temp.isQueue = FindInstanceInQueue(temp.mapid);
					mvCurInstanceList.push_back(temp);
				}
			}
		}
		break;
	case LISTSORT_ESCAPE:
		{
			for (unsigned int ui = 0 ; ui < mvInstanceList.size() ; ui++)
			{
				if(mvInstanceList[ui].category == INSTANCE_CATEGORY_ESCAPE)
				{
					Sunyou_Instance_ConfigurationEx temp;
					temp = mvInstanceList[ui];
					temp.isQueue = FindInstanceInQueue(temp.mapid);
					mvCurInstanceList.push_back(temp);
				}
			}
		}
		break;
	case LISTSORT_PVP:
		{
			for (unsigned int ui = 0 ; ui < mvInstanceList.size() ; ui++)
			{
				//if(mvInstanceList[ui].category == INSTANCE_CATEGORY_ARENA)
				if (mvInstanceList[ui].category == INSTANCE_CATEGORY_BATTLE_GROUND
					||mvInstanceList[ui].category == INSTANCE_CATEGORY_TEAM_ARENA
					||mvInstanceList[ui].category == INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND
					||mvInstanceList[ui].category == INSTANCE_CATEGORY_NEW_BATTLE_GROUND)
				{
					Sunyou_Instance_ConfigurationEx temp;
					temp = mvInstanceList[ui];
					temp.isQueue = FindInstanceInQueue(temp.mapid);
					mvCurInstanceList.push_back(temp);
				}
			}
		}
		break;
	case LISTSORT_RAID:
		{
			for (unsigned int ui = 0 ; ui < mvInstanceList.size() ; ui++)
			{
				if (mvInstanceList[ui].category == INSTANCE_CATEGORY_RAID)
				{
					Sunyou_Instance_ConfigurationEx temp;
					temp = mvInstanceList[ui];
					temp.isQueue = FindInstanceInQueue(temp.mapid);
					mvCurInstanceList.push_back(temp);
				}
			}
		}
		break;
	case LISTSORT_FAIRGROUND:
		{
			for (unsigned int ui = 0 ; ui < mvInstanceList.size() ; ui++)
			{
				if (mvInstanceList[ui].category == INSTANCE_CATEGORY_FAIRGROUND)
				{
					Sunyou_Instance_ConfigurationEx temp;
					temp = mvInstanceList[ui];
					temp.isQueue = FindInstanceInQueue(temp.mapid);
					mvCurInstanceList.push_back(temp);
				}
			}
		}
		break;
	}
	UpdateCurrentState();
}
bool UInstanceMgr::GetCurrentList(std::vector<Sunyou_Instance_ConfigurationEx> &vList)
{
	if (!mvCurInstanceList.size())
		return false;
	std::vector<Sunyou_Instance_ConfigurationEx> vtemp;

	for (unsigned int ui = 0 ; ui < mvCurInstanceList.size() ; ui++)
	{
		if (mvCurInstanceList[ui].bDynamicShow)
		{
			vtemp.push_back(mvCurInstanceList[ui]);
		}
	}
	int size = min(vtemp.size() - mCurPage * InstanceNum_InOnePage, 8);

	for (unsigned int ui = mCurPage * InstanceNum_InOnePage ; ui < mCurPage * InstanceNum_InOnePage + size ; ui++)
	{
		vList.push_back(vtemp[ui]);
	}
	return true;
}

bool UInstanceMgr::GetCurrentPageStr(std::string &str)
{
	char buf[256];

	sprintf(buf, "%u/%u", mCurPage + 1, mMaxPage + 1);

	str = buf;

	return true;
}

bool UInstanceMgr::FindInstanceInQueue(ui32 mapid)
{
	for (unsigned int ui = 0 ; ui < m_QueueInstanceMapID.size() ; ui++)
	{
		if (mapid == m_QueueInstanceMapID[ui])
		{
			return true;
		}
	}
	return false;
}

bool UInstanceMgr::IsPlayerInInstance()
{
	return mPlayerInInstanceMapid != 0;
}

bool UInstanceMgr::GetInstanceInfo(ui32 mapid, std::string &Name, std::string &SInfo)
{
	CMapInfoDB::MapInfoEntry entry;
	if (g_pkMapInfoDB)
	{
		g_pkMapInfoDB->GetMapInfo(mapid, entry);
	}

	Name = entry.desc;

	char buf[256];
	std::string str;
	sprintf(buf, CInstanceInstanceTextPath, mapid);
	NiFile* pkFile = NiFile::GetFile(buf, NiFile::READ_ONLY);
	if(!pkFile || !(*pkFile))
	{
		NiDelete pkFile;
		return false;
	}

	if(pkFile->GetFileSize())
	{
		str.resize(pkFile->GetFileSize() + 1);
		unsigned int uiSize = pkFile->Read(&str[0],  pkFile->GetFileSize());
		str[uiSize] = '\0';
	}

	NiDelete pkFile;

	SInfo = AnisToUTF8(str);

	return true;
}

bool UInstanceMgr::GetInstanceTimeString(std::string &TimeStr)
{
	char buf[256];
	int hour = 0,minute = 0 ,second = 0;
	hour = (int)mtimefcount/3600;
	minute = (int)(mtimefcount - hour*3600)/60;
	second = int(mtimefcount)%60;
	string strtime = _TRAN("剩余时间:");
	sprintf(buf, "%0*u:", 2, hour);
	strtime += buf;
	sprintf(buf, "%0*u:", 2, minute);
	strtime += buf;
	sprintf(buf, "%0*u", 2, second);
	strtime += buf;
	TimeStr = strtime;
	return true;
}

bool UInstanceMgr::GetArenaUpdateStateInfo(std::vector<MSG_S2C::stArenaUpdateState::info> &vInfo)
{
	if (mvArenaStateInfo.size())
	{
		vInfo = mvArenaStateInfo;
		return true;
	}
	return false;
}

bool UInstanceMgr::IsLocalPlayerName(const char* Name)
{
	CPlayer* pPlayer = ObjectMgr->GetLocalPlayer();
	if (!pPlayer)
		return false;

	return pPlayer->GetName().Equals(Name);
}

bool UInstanceMgr::GetBillBoardInfo(std::vector<MSG_S2C::stInstanceBillboard::info>& vInfo)
{
	if (mvBillBoardInfo.size())
	{
		vInfo = mvBillBoardInfo;
		return true;
	}
	return false;
}

bool UInstanceMgr::GetInstancePayString(uint32 cost_type, uint32 cost_value1, uint32 cost_value2, std::string &PayString)
{
	//type = 1 : 元宝 value1 = count
	//type = 2 : 道具 value1 = entry value2 = count
	//type = 3 : 金币 value1 = count

	switch(cost_type)
	{
	case 0:
		{
			PayString = _TRAN("费用：免费");
		}
		return true;
	case 1:
		{
			if (cost_value1)
			{
				//sprintf(buf, "费用：%u个元宝", cost_value1);
				PayString = _TRAN( N_NOTICE_C16, _I2A(cost_value1).c_str() );
			}
			else
			{
				PayString = _TRAN("费用：免费");
			}
		}
		return true;
	case 2:
		{
			ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(cost_value1);
			if (ItemInfo)
			{
				//sprintf(buf, "费用：%u个%s", cost_value2, ItemInfo->C_name.c_str() );
				PayString = _TRAN( N_NOTICE_C17, _I2A(cost_value2).c_str(), ItemInfo->C_name.c_str() );
			}
			else
			{
				return false;
			}
		}
		return true;
	case 3:
		{
			if (cost_value1)
			{
				//sprintf(buf, "费用：%u个金币", cost_value1);
				PayString = _TRAN( N_NOTICE_C18, _I2A(cost_value1).c_str() );
			}
			else
			{
				PayString = _TRAN("费用：免费");
			}
		}
		return true;
	}
	return false;
}

bool UInstanceMgr::GetInstanceReliveTime(ui32 mapid, ui32& reliveTime)
{
	for(unsigned int ui = 0 ; ui < mvInstanceList.size() ; ui++)
	{
		if (mvInstanceList[ui].mapid == mapid)
		{
			reliveTime = mvInstanceList[ui].resurrect_time;
			return true;
		}
	}
	return false;
}

bool UInstanceMgr::GetInstanceReliveTime(ui32& reliveTime)
{
	return GetInstanceReliveTime(mPlayerInInstanceMapid, reliveTime);
	//for(unsigned int ui = 0 ; ui < mvInstanceList.size() ; ui++)
	//{
	//	if (mvInstanceList[ui].mapid == mPlayerInInstanceMapid)
	//	{
	//		reliveTime = mvInstanceList[ui].resurrect_time;
	//		return true;
	//	}
	//}
	//return false;
}

void UInstanceMgr::Update(float fDeltaTime)
{
	//判断时间
	time_t t = time(NULL);
	tm * pTM = localtime(&t);
	static char buf[_MAX_PATH];
	UISystemMsg* pSGN = INGAMEGETFRAME(UISystemMsg, FRAME_IG_SYSGIFTNOTIFY);
	if(!pTM || !pSGN)
		return;

	uint8 hour = pTM->tm_hour;
	for (unsigned int ui = 0 ; ui < mvCurInstanceList.size() ; ui++)
	{
		for (int i = 0 ; i < 6 ; i++)
		{
			mvCurInstanceList[ui].isActive = hour > mvCurInstanceList[ui].open_start[i] && hour < mvCurInstanceList[ui].open_end[i];
		}
	}

	//传送前奏
	if (m_TranceTime > 0.0f)
	{
		m_TranceTime -= fDeltaTime;

		if (m_TranceTime < 0.0f)
			m_TranceTime = 0.0f;

		uint8 second = (int)m_TranceTime;

		//sprintf(buf, "您将于%d秒后被传送至%s", second, m_TranceMapName.c_str());
		std::string strTele = _TRAN( N_NOTICE_C19, _I2A( second ).c_str(), m_TranceMapName.c_str() );
		pSGN->GetInstaceTime(strTele.c_str(), m_TranceTime);

		UpdateCurrentState();
	}
	//副本计时
	if ( mIsShowInstanceMsg )
	{
		if (mtimefcount > 0.0f)
		{
			mtimefcount -= fDeltaTime;
			if (mtimefcount < 40.0f)
			{
				//sprintf(buf, "%d秒后将要传出副本", (int)mtimefcount);
				std::string strTeleOut = _TRAN( N_NOTICE_C20, _I2A( (int)mtimefcount ).c_str());
				pSGN->GetInstaceTime(strTeleOut.c_str(), 1.f);
			}
			if (mtimefcount < 0.0f)
				mtimefcount = 0.0f;
		}
		//进入副本
		if( IsPlayerInInstance() )
		{
			std::vector<InstaceMsgstruct>::iterator it = m_InstanceMsg.begin();
			if (it != m_InstanceMsg.end())
			{
				if (it->time - mtimefcount > 0.0f)
				{
					pSGN->GetSystemMsg(it->descmsg.c_str(), 4.0f);
					ClientSystemNotify(it->descmsg.c_str());
					m_InstanceMsg.erase(it);
				}
			}
		}
	}
}

int g_sorttype = 0;

bool KeySort(const battle_ground_player_detail_information& l, const battle_ground_player_detail_information& r)
{
	bool bTemp = true;
	//0 - name 1 - class 2 - kill 3 - bekill 4 - honor
	switch(g_sorttype)
	{
	case 0:
		bTemp = l.name > r.name;
		break;
	case 1:
		bTemp = l.cls > r.cls;
		break;
	case 2:
		bTemp = l.kill > r.kill;
		break;
	case 3:
		bTemp = l.bekill > r.bekill;
		break;
	case 4:
		bTemp = l.honor > r.honor;
		break;
	case 5:
		bTemp = l.damage > r.damage;
		break;
	case 6:
		bTemp = l.healing > r.healing;
		break;
	}
	return bTemp;
}

bool UInstanceMgr::GetBGPlayerDetailInfomatrion(std::vector<battle_ground_player_detail_information>* pV, int sortType,uint8 race, bool bparty)
{
	g_sorttype = sortType;
	pV->clear();
	for(UINT ui = 0 ; ui < m_vBattleGroundPlayer.size() ; ui++)
	{
		battle_ground_player_detail_information* pInfo = &m_vBattleGroundPlayer[ui];
		if (race != 0 )
		{
			if (bparty)
			{
				if (pInfo->team + 1== race)
				{
					pV->push_back(*pInfo);
				}
			}else
			{
				if(pInfo->race == race)
				{
					pV->push_back(*pInfo);
				}
			}
		}
		else
		{
			pV->push_back(*pInfo);
		}
	}
	if (pV->size())
	{
		std::sort(pV->begin(), pV->end(), &KeySort);
		return true;
	}
	return false;
}