//-----------------------------------------------------------
//副本大厅总体管理
//------------------------------------------------------------
#ifndef UINSTANCEMGR_H
#define UINSTANCEMGR_H

#include "UIGamePlay.h"
#include "UInGameBar.h"
#include "Singleton.h"

#define InstanceSys UInstanceMgr::GetSingleton()

static const char* CInstanceCategory[] = 
{
	"(PVP战场)",//INSTANCE_CATEGORY_ARENA
	"(升级副本)",//INSTANCE_CATEGORY_REFRESH_MONSTER
	"(动态副本)",//INSTANCE_CATEGORY_DYNAMIC
	"(生存副本)",//INSTANCE_CATEGORY_ESCAPE
	"",//INSTANCE_CATEGORY_UNIQUE_PVP_ZONE
	"",//INSTANCE_CATEGORY_TOWER_DEFENSE
	"",//INSTANCE_CATEGORY_UNIQUE_CASTLE,
	"(战场)",//INSTANCE_CATEGORY_BATTLE_GROUND,
	"(竞技场)",//INSTANCE_CATEGORY_TEAM_ARENA
	"(团队)",//INSTANCE_CATEGORY_RAID
	"(战场)",//INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND
	"(游乐场)",//INSTANCE_CATEGORY_FAIRGROUND
	"(战场)",//INSTANCE_CATEGORY_NEW_BATTLE_GROUND
	"",
	"",
};
static const char* CInstanceMiniMapPath = "Data\\UI\\Lobby\\InstanceMiniMap\\Instance_%u.png";
static const char* CInstanceInstanceTextPath = "DATA\\UI\\Lobby\\InstanceText\\Instance_%u.txt";
static const INT InstanceNum_InOnePage = 8;

enum ListSortClass
{
	LISTSORT_ALL,//所有副本
	LISTSORT_MONSTER,//打怪副本
	LISTSORT_DYNAMIC,//动态副本
	LISTSORT_ESCAPE,//生存副本
	LISTSORT_PVP,//PVP副本
	LISTSORT_RAID,//团队副本
	LISTSORT_FAIRGROUND,//游乐场

	LISTSORT_NONE,
};
struct Sunyou_Instance_ConfigurationEx : public sunyou_instance_configuration
{
	Sunyou_Instance_ConfigurationEx& operator=(const sunyou_instance_configuration &ln)
	{
		category = ln.category;
		expire = ln.expire;
		mapid = ln.mapid;
		iconid = ln.iconid;
		maxlevel = ln.maxlevel;
		minlevel = ln.minlevel;
		maxplayer = ln.maxplayer;
		random_vrl = ln.random_vrl;
		cost_type = ln.cost_type;
		cost_value1 = ln.cost_value1;
		cost_value2 = ln.cost_value2;
		enter_limit = ln.enter_limit;
		bg_faction_min_player = ln.bg_faction_min_player;
		bg_faction_max_player = ln.bg_faction_max_player;
		bg_faction_count = ln.bg_faction_count;
		resurrect_time = ln.resurrect_time;
		vsi = ln.vsi;
		isQueue = false;
		isActive = true;
		isTeam = false;
		bDynamicShow = true;

		for (int i = 0 ; i < 6 ; i++)
		{
			open_start[i] = ln.open_start[i];
			open_end[i] = ln.open_end[i];
		}
		return *this; 
	}
	bool isQueue;
	bool isActive;
	bool isTeam;
	bool bDynamicShow;
};

class UInstanceMgr : public Singleton<UInstanceMgr>
{
	SINGLETON(UInstanceMgr);
public:
	BOOL CreateUI(UControl* RootCtrl);
	void InitUI();
	void ShowFrame();
	void HideFrame();
public://localplayer状态变化通知
	void OnLevelUpdata(ui32 level);//等级变化通知
	void OnTeamUpdata();//队长变化通知
	bool GetTeamState();
	void UpdateCurrentState();//把变化过的通知自己
public://输入
	//一般副本
	void ParseQueueUpdata(uint32* mapid);//副本排队列表更行
	void ParseInstanceList(std::vector<sunyou_instance_configuration> &vlist);//副本大厅副本列表
	void ParseInstanceCountDown(uint32 mapid, uint8 second);//进入副本前的剩余时间
	void ParseEnterInstance(uint32 timecount, ui32 mapid, ui32 category);//进入副本
	void ParseLeaveInstance(ui64 guid, std::string Name);//离开副本
	//PVP副本
	void ParseAreanUpdateState(std::vector<MSG_S2C::stArenaUpdateState::info> &vinfo);//PVP副本杀人数更新
	bool IsUpdateOrInsert(MSG_S2C::stArenaUpdateState::info& Info);
	void ParseAreanBillBoard(std::vector<MSG_S2C::stInstanceBillboard::info> &vinfo);//PVP副本结束记分牌
	//战场
	void ParseBattleGroundPowerUpdate(MSG_S2C::stBattleGroundPowerUpdate& msg);
	void ParseBattleGroundResourceUpdate(MSG_S2C::stBattleGroundResourceUpdate& msg);
	void ParseBattleGroundDetailInformation(MSG_S2C::stBattleGroundDetailInformation& msg);
public://输出
	void SendQueueInstance(uint32 mapid);
	void SendQuitQueueInstance(uint32 mapid);
	void SendInstanceDeadExit(bool bExit);
public://其他功能
	//获得副本的名字"(副本分类)副本名称"
	//input:地图ID,副本分类
	//output:是否得到了名字信息，得到的话str作为引用输出
	bool GetInstanceName(ui32 mapid, instance_category_t category, std::string &str);

	//获得并设置副本缩略图
	//input:地图ID,并且把UI容器指针传入
	//output:是否设置成功
	bool GetInstanceMiniBitmap(ui32 iconid, UBitmap* pBitMapCtrl);

	//获得副本等级字符串
	//input:最低等级 最高等级
	//output:是否字符串成功，成功用引用输出
	bool GetInstanceLevelStr(ui32 minLevel, ui32 MaxLevel, std::string &str);

	//获取当前页数的字符串
	bool GetCurrentPageStr(std::string &str);

	//查找当前副本是否在排队中 
	bool FindInstanceInQueue(ui32 mapid);

	//玩家是否在副本中
	inline bool IsPlayerInInstance();

	//进入副本介绍信息取得
	bool GetInstanceInfo(ui32 mapid, std::string &Name, std::string &SInfo);

	//副本所剩时间字符串
	bool GetInstanceTimeString(std::string &TimeStr);

	//PVP副本杀人计数器列表获取
	bool GetArenaUpdateStateInfo(std::vector<MSG_S2C::stArenaUpdateState::info> &vInfo);

	//姓名判读
	bool IsLocalPlayerName(const char* Name);

	//获得结束记分牌的存储
	bool GetBillBoardInfo(std::vector<MSG_S2C::stInstanceBillboard::info>& vInfo);
	
	//获得收费栏字符串
	bool GetInstancePayString(uint32 cost_type, uint32 cost_value1, uint32 cost_value2, std::string &PayString);

	//获取副本复活时间
	bool GetInstanceReliveTime(ui32 mapid, ui32& reliveTime);
	bool GetInstanceReliveTime(ui32& reliveTime);

	void ShowOrHide(BOOL Show);

	int GetBattleGroundFlagState(int index);
protected:
	std::vector<MSG_S2C::stArenaUpdateState::info> mvArenaStateInfo;//PVP副本记分牌的储存
	std::vector<MSG_S2C::stInstanceBillboard::info> mvBillBoardInfo;//PVP副本结束标记牌的存储

	std::vector<ui32> m_QueueInstanceMapID;//已经在排队的副本列表

	ui32 mPlayerInInstanceMapid;

//副本大厅列表管理
public:
	void NextPage();//下一页
	void PrevPage();//上一页

	//对列表进行分类处理便于显示
	//输入详情见ListSortClass
	//得到mvCurInstanceList的新列表用于显示
	void SetCurrentListAs(ListSortClass LSCLASS);

	//取得当前页列表
	bool GetCurrentList(std::vector<Sunyou_Instance_ConfigurationEx> &vList);


	//动态副本测试
	//输入：类型 输出：地图ID bool值暂时无用
	bool DynamicInstanceShowTest(int type, ui32 mapid);
public:
	void OnUpdateFairGroundIcon();
protected:
	std::vector<sunyou_instance_configuration> mvInstanceList;//副本大厅副本列表
	std::vector<Sunyou_Instance_ConfigurationEx> mvCurInstanceList;//副本大厅分类列表

	UINT mCurPage;//副本大厅当前页数
	UINT mMaxPage;//副本大厅最大页数
	bool mIsTeamLeader;
	int32 mLocalLevel;
	ListSortClass m_CurSortClass ;

//副本进入前时间
public:
	void Update(float fDeltaTime);
protected:
	float m_TranceTime;//单位：秒。已经排队成功，倒计时排队副本时间
	std::string m_TranceMapName;//排队成功的副本名称
//进入副本后时间部分
	std::vector<struct InstaceMsgstruct> m_InstanceMsg;//副本系统信息
	uint32 mtimecount;
	float  mtimefcount;

	bool mIsShowInstanceMsg;

	std::map<int, std::vector<sunyou_instance_configuration>> m_InstanceMap;

	int mCurrentCategory;//当前进入的副本类型
public:
	inline int GetCurrentCategory(){return mCurrentCategory;};


protected:
	std::vector<battle_ground_player_detail_information> m_vBattleGroundPlayer;
public:
	bool GetBGPlayerDetailInfomatrion(std::vector<battle_ground_player_detail_information>* pV, int sortType = 0,uint8 race = 0,bool bparty = false);
};

#endif