#include "StdAfx.h"
#include "UInstanceQuitDlg.h"
#include "UInstanceMgr.h"

UIMP_CLASS(UInstanceQuitDlg,UDialog);

UBEGIN_MESSAGE_MAP(UInstanceQuitDlg, UDialog)
UON_BN_CLICKED(1,&UInstanceQuitDlg::OnClickBtnInsQuit)
UON_BN_CLICKED(2,&UInstanceQuitDlg::OnClickBtnCloseDlg)
UEND_MESSAGE_MAP();

void UInstanceQuitDlg::StaticInit()
{

}

UInstanceQuitDlg::UInstanceQuitDlg()
{
	m_CloseBtnPos = UPoint(225, 32);
	m_MapId = 0;
	m_IsUpdateChild = FALSE;
}

UInstanceQuitDlg::~UInstanceQuitDlg()
{

}

void UInstanceQuitDlg::SetVisible(BOOL value)
{
	if (!value)
		OnClose();
	UDialog::SetVisible(value);
}

void UInstanceQuitDlg::SetInstanceMap(ui32 mapid)
{
	m_MapId = mapid;
}

void UInstanceQuitDlg::UpdataInstanceInfo()
{
	std::string Name, SInfo;

	InstanceSys->GetInstanceInfo(m_MapId, Name, SInfo);

	UStaticText* pStaticText = UDynamicCast(UStaticText, GetChildByID(0));
	if (pStaticText)
	{
		pStaticText->SetText(Name.c_str());
	}

	URichTextCtrl* pRichtextCtrl = UDynamicCast(URichTextCtrl, GetChildByID(3));
	if (pRichtextCtrl)
	{
		pRichtextCtrl->SetText(SInfo.c_str(), SInfo.length());
	}
}

BOOL UInstanceQuitDlg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	m_bCanDrag = FALSE;

	return TRUE;
}

void UInstanceQuitDlg::OnDestroy()
{
	UDialog::OnDestroy();
}

void UInstanceQuitDlg::OnClose()
{
	UDialog::SetVisible(FALSE);
	UInGame* pInGame = UInGame::Get();
	if (pInGame)
	{
		UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
		if (pGameBar)
		{
			UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONINSTANCE);
			if (pBtn)
			{
				pBtn->SetCheckState(FALSE);
			}
		}
	}
}

BOOL UInstanceQuitDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}

void UInstanceQuitDlg::OnClickBtnInsQuit()
{
	InstanceSys->SendInstanceDeadExit(true);
}

void UInstanceQuitDlg::OnClickBtnCloseDlg()
{
	OnClose();
}