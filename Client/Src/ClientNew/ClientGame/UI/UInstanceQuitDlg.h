//-----------------------------------------------------------
//副本退出界面
//------------------------------------------------------------
#ifndef UINSTANCEQUITDLG_H
#define UINSTANCEQUITDLG_H
#include "UInc.h"

class UInstanceQuitDlg : public UDialog
{
	UDEC_CLASS(UInstanceQuitDlg);
	UDEC_MESSAGEMAP();
public:
	UInstanceQuitDlg();
	~UInstanceQuitDlg();
public:
	virtual void SetVisible(BOOL value);

	void SetInstanceMap(ui32 mapid);
	void UpdataInstanceInfo();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	virtual void OnClose();
	virtual BOOL OnEscape();

	void OnClickBtnInsQuit();
	void OnClickBtnCloseDlg();
protected:
	ui32 m_MapId;
};
#endif