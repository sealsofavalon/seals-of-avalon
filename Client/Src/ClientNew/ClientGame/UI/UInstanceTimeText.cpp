#include "StdAfx.h"
#include "UInstanceTimeText.h"
#include "UInstanceMgr.h"

UIMP_CLASS(UInstanceTimeText,UStaticText);

void UInstanceTimeText::StaticInit()
{

}

UInstanceTimeText::UInstanceTimeText()
{
	m_IsUpdateChild = FALSE;

}

UInstanceTimeText::~UInstanceTimeText()
{

}

BOOL UInstanceTimeText::OnCreate()
{
	if (!UStaticText::OnCreate())
	{
		return FALSE;
	}

	m_Font = sm_UiRender->CreateFont("ArialBold",18, 134);
	SetTextColor("ffaa23");

	return TRUE;
}

void UInstanceTimeText::OnDestroy()
{
	Clear();
	SetVisible(FALSE);
	UStaticText::OnDestroy();
}

void UInstanceTimeText::OnRender(const UPoint& offset, const URect &updateRect)
{
	std::string TimeStr;
	if (InstanceSys->GetInstanceTimeString(TimeStr))
	{
		SetText(TimeStr.c_str());
	}
	if (m_Font && sm_UiRender && m_WStrBuffer.GetLength())
	{
		USetTextEdge(TRUE);
		UDrawText(sm_UiRender, m_Font, offset, updateRect.GetSize(), m_FontColor, m_WStrBuffer, (EUTextAlignment)m_TextAlign );
		USetTextEdge(FALSE);
	}
}

void UInstanceTimeText::Open()
{
	SetVisible(TRUE);
}

void UInstanceTimeText::Close()
{
	Clear();
	SetVisible(FALSE);
}