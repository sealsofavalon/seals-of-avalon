//-----------------------------------------------------------
//副本计时器面板
//------------------------------------------------------------
#ifndef UINSTANCETIMETEXT_H
#define UINSTANCETIMETEXT_H
#include "UInc.h"

class UInstanceTimeText : public UStaticText
{
	UDEC_CLASS(UInstanceTimeText);
	UInstanceTimeText();
	~UInstanceTimeText();
public:
	void Open();
	void Close();
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	//virtual void OnTimer(float fDeltaTime);
};
#endif