#include "StdAfx.h"
#include "UItemContainerEx.h"
#include "ItemManager.h"
#include "UIItemSystem.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "Utils/SpellDB.h"
#include "UMessageBox.h"
#include "Player.h"
#include "ItemSlot.h"
#include "ItemSlotContainer.h"
#include "ObjectManager.h"
#include "ClientApp.h"
#include "UITipSystem.h"
#include "UEquRefineManager.h"
#include "AudioDef.h"


UIMP_CLASS(UItemContainerEx,UItemContainer);
void UItemContainerEx::StaticInit()
{

}
UItemContainerEx::UItemContainerEx(void)
{
	m_Spell = UItemSystem::ITEM_DATA_FLAG;
	m_InTipCell = UPoint(-1,-1);
	m_LockTexture = NULL;
	m_bLock = false;
	m_bDrop = true;
}

UItemContainerEx::~UItemContainerEx(void)
{
}
BOOL UItemContainerEx::OnCreate()
{
	if (!UItemContainer::OnCreate())
	{
		return FALSE;
	}

	m_LockTexture = sm_UiRender->LoadTexture("DATA\\UI\\PUBLIC\\LOCK.png");

	return TRUE;
}
void UItemContainerEx::DeleteItem(int nRow, int nCol)
{
	ITEM* pDestItem = &m_pItems[m_GridCount.x * nRow + nCol];
	pDestItem->spSkin = NULL; 
	pDestItem->nIconIndex = -1 ;
	pDestItem->nItemNum = 0 ;
	if (pDestItem->UserData)
	{
		delete (pDestItem->UserData) ;
	}
	pDestItem->UserData = NULL;

}


BOOL UItemContainerEx::SetItemByPos(ui8 pos, ui32 dwitemid, int num, ItemExtraData& stItemExtraData, bool bLock)
{
	//根据位置获得行列
	if (pos <0 && pos > m_SelGrid.y * m_GridCount.x + m_SelGrid.x)
	{
		UTRACE("pos is  too big !");
		return FALSE;
	}
	 int col = pos % m_GridCount.x ;
	 int row = pos / m_GridCount.x ;
	
	 USkinPtr pIncoSkin;
	 int index; 

	 if (dwitemid == 0)
	 {
		 DeleteItem(row,col);
		 return TRUE;
	 }

	  std::string str ;
	  
	 //设物品
	  bool bOnly = false;
	 if (m_Spell == UItemSystem::ITEM_DATA_FLAG)
	 {
		 ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(dwitemid);
		 if(ItemInfo)
		 {
			 //查询icon信息
			 str = ItemInfo->C_icon;
			 bOnly = ItemInfo->MaxCount - 1 ? false : true;
		 }else
		 {
			 //UMessageBox::MsgBox("错误!没有这个物品");
			 UTRACE("get ItemInfo failed!");
			 return FALSE;
		 }
	 }

	 if (str.length() == 0 )
	 {
		 UTRACE("load skin  failed!");
		 return FALSE;
	 }
	 int strpos = str.find(";");
	 std::string skinstr = str.substr(0,strpos);
	 pIncoSkin = sm_System->LoadSkin(skinstr.c_str());

	 std::string strIndex = str.substr(strpos+1,str.length() -1);
	 index = atoi(strIndex.c_str());


	 if (index == -1)
	 {
		// assert(0&&"获取icon的索引出错!不存在的索引");
		 pIncoSkin = sm_System->LoadSkin("Icon\\ItemIcon\\default.skin");
		 index = 0;
		 index = 0;
		 //return FALSE;
	 }
	 if (pIncoSkin == NULL)
	 {
		 //assert(0&&"获取icon的skin出错");
		 pIncoSkin = sm_System->LoadSkin("Icon\\ItemIcon\\default.skin");
		 index = 0;
		 //return FALSE;
	 }
	 
	 UItemData* pItemData = new UItemData;
	 pItemData->pItemID = dwitemid;
	 pItemData->bLock = bLock;
	 pItemData->bOnly = bOnly;
	 pItemData->stItemExtraData.jinglian_level = stItemExtraData.jinglian_level;
	 for (int i = 0 ; i < 6 ; i++)
		 pItemData->stItemExtraData.enchants[i] = stItemExtraData.enchants[i];

	 if (HasItem(row,col))
	 { 
		 DeleteItem(row,col);
		 AddItem((USkin*)pIncoSkin,index,row,col,pItemData,num);
	 }else
	 {
		 AddItem((USkin*)pIncoSkin,index,row,col,pItemData,num);
	 }

	 if(m_HoverGrid.x == col && m_HoverGrid.y == row)
	 {
		 OnHideTipsInfo();
		 OnShowTipsInfo();
	 }

	 return TRUE;

}
void UItemContainerEx::OnSize(const UPoint& NewSize)
{
	UControl::OnSize(NewSize);
	m_GridSize.x = int(NewSize.x /(float)m_GridCount.x) ;
	m_GridSize.y = int(NewSize.y / (float)m_GridCount.y) ; 
}
void UItemContainerEx::DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver)
{
	UItemContainer::DrawGridItem(offset,cell,selected,mouseOver);
	
	if (m_pItems[cell.y * m_GridCount.x + cell.x].nItemNum != 0)
	{
		UItemData* pData = (UItemData*)m_pItems[cell.y * m_GridCount.x + cell.x].UserData;
		if (pData && pData->bLock)
		{
			//画出锁定的图标。
			if (m_LockTexture)
			{
				URect pRect = URect(offset,m_GridSize); 
				sm_UiRender->DrawImage(m_LockTexture, pRect);
			}
		}

		if (pData && !pData->bOnly)
		{
			char buf[8];
			NiSprintf(buf, 8, "%d",m_pItems[cell.y * m_GridCount.x + cell.x].nItemNum);
			int textH = m_Style->m_spFont->GetHeight();
			UPoint txtPos(offset.x , offset.y + m_GridSize.y - textH - 6);
			UPoint txtSize(m_GridSize.x - 7, textH);
			USetTextEdge(TRUE);
			//要求字渲染在右下角
			UDrawText(sm_UiRender,m_Style->m_spFont,txtPos,txtSize,m_Style->m_FontColor,buf,UT_RIGHT);
			USetTextEdge(FALSE);
		}
	}	
}
int  UItemContainerEx::GetNumByPos(int pos)
{
	if (pos >= 0 && pos < m_GridCount.x * m_GridCount.y)
	{
		return m_pItems[pos].nItemNum;
	}

	return 0 ;
}
void UItemContainerEx::OnMouseMove(const UPoint& pt, UINT uFlags)
{
	UGridCtrl::OnMouseMove(pt, uFlags);
	if (m_bDragBegin && m_ItemTypeID != UItemSystem::ICT_INVLALID && m_ItemTypeID != UItemSystem::ICT_PLAYER_TRADE_TENNET && m_ItemTypeID != UItemSystem::ICT_NPC_TRADE)
	{
	 	/*SYDragData * pDragData = new SYDragData;
		pDragData->pItem = pItem;
		pDragData->col = m_SelGrid.x;
		pDragData->row = m_SelGrid.y;*/

		UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);

		if (pInGame == NULL)
			return ;

		if (m_SelGrid.x >= 0 && m_SelGrid.y >= 0)
		{
			WORD pos = m_SelGrid.y * m_GridCount.x + m_SelGrid.x ;
			ITEM* pItem = &m_pItems[pos];
			WORD* pPos = new WORD;
			*pPos = pos ;
			UItemData* pData = (UItemData*)pItem->UserData;
			if (pData && pData->bLock)
			{
				//锁定了。
				return;
			}
			//发送数据 位置 窗口.

			BOOL bDragSuc = FALSE;
			if (GetParent())
			{
				URect IconRect;
				UPoint offset = GetParent()->WindowToScreen(GetWindowPos());
				IconRect.pt0.x = offset.x + m_SelGrid.x * m_GridSize.x + 3;
				IconRect.pt0.y = offset.y + m_SelGrid.y * m_GridSize.y + 3;
				IconRect.SetSize(m_GridSize.x - 6, m_GridSize.y - 6);
				bDragSuc = IconRect.PointInRect(pt);
			}
			if (!m_bLock  && !bDragSuc)
			{
				BeginDragDrop(this, pItem->spSkin, pItem->nIconIndex, pPos, (UINT)m_ItemTypeID);
				m_DragItem = m_SelGrid;
				m_bDragBegin = FALSE;
			}
		}	
	}
	//OnShowTipsInfo();
}

void UItemContainerEx::OnMouseEnter(const UPoint& position, UINT flags)
{
	m_bDrop = true;
	UItemContainer::OnMouseEnter(position,flags);
}

void UItemContainerEx::OnMouseLeave(const UPoint& pt, UINT uFlags)
{
	m_bDrop = false;
	UItemContainer::OnMouseLeave(pt,uFlags);
	OnHideTipsInfo();
}

UINT UItemContainerEx::GetDataFlag()
{
	return m_ItemTypeID;
}

BOOL UItemContainerEx::renderTooltip(const UPoint& cursorPos, const char* tipText /* = NULL */ )
{
	UPoint Tip_sit(m_HoverGrid.x*m_GridSize.x,m_HoverGrid.y*m_GridSize.y);
	UPoint t_pt = WindowToScreen(Tip_sit);
	URect itemrect(t_pt,m_GridSize);
	if (itemrect.PointInRect(cursorPos))
	{
		//OnHideTipsInfo();
		//OnShowTipsInfo();
		return TRUE;
	}
	return FALSE;
}
void UItemContainerEx::OnCellHighlighted(const UPoint& cell)
{
	if (m_HoverGrid.x != -1 &&m_HoverGrid. y != -1)
	{
		if (m_InTipCell != cell)
		{
			OnHideTipsInfo();
			OnShowTipsInfo();
		}
	}
	m_InTipCell = cell;
}

void UItemContainerEx::OnMouseDragged(const UPoint& pt, UINT uFlags)
{
	bool PlaySound = false;
	if (!sm_System->IsDraging())
	{
		PlaySound = true;
	}
	UItemContainer::OnMouseDragged(pt, uFlags);
	if (PlaySound && sm_System->IsDraging())
	{
		sm_System->__PlaySound(UI_moveitem);
	}
}
void UItemContainerEx::OnDragDropAccept(UControl* pAcceptCtrl, const UPoint& point,const void* DragData, UINT nDataFlag)
{
	UItemContainer::OnDragDropAccept(pAcceptCtrl, point, DragData, nDataFlag);
	sm_System->__PlaySound(UI_moveitem);
}
void UItemContainerEx::OnDragDropReject(UControl* pRejectCtrl, const void* DragData, UINT nDataFlag)
{
	UItemContainer::OnDragDropReject(pRejectCtrl, DragData, nDataFlag);
}

void UItemContainerEx::OnDragDropEnd(UControl* pDragTo, void* DragData, UINT nDataFlag)
{
	WORD* pDragData = (WORD*)DragData;
	delete pDragData;
	m_DragItem.Set(-1, -1);
}
void UItemContainerEx::SetGridCount(int nRow, int nCol)
{
	UItemContainer::SetGridCount(nRow,nCol);
}

BOOL UItemContainerEx::OnMouseUpDragDrop(UControl* pDragFrom,const UPoint& point, const void* DragData, UINT nDataFlag)
{
	if (nDataFlag == UItemSystem::ICT_INVLALID)
	{
		return FALSE;
	}
	
	UPoint DropGrid = GetGrid(point);
	if (DropGrid.x == -1 || DropGrid.y == -1)
	{
		return FALSE;
	}
	

	ui8 ToPos = m_GridCount.x * DropGrid.y + DropGrid.x;
	UItemData* pData = (UItemData*)m_pItems[ToPos].UserData;
	const ui8* FromePos = (const ui8*)DragData;
	//锁定
	if (pData && pData->bLock)
	{
		return FALSE;
	}

	if (nDataFlag == UItemSystem::ICT_REFINE)
	{
		//这里是将精炼的材料返回
		if (pData && (pData->pItemID != 0 || pData->bLock))
		{
			ULOG("this pos is not null !");
			return FALSE;
		}
		//发送消息
		if (EquRefineMgr)
		{
			EquRefineMgr->SendAddMaterials(ToPos, *FromePos,TRUE);
		}
		return TRUE;
	}

	
	
	int num = 1;

	if (nDataFlag == UItemSystem::ICT_BAG)
	{
		CPlayer* localPlayer = ObjectMgr->GetLocalPlayer();
		if (localPlayer)
		{
			CStorage* pContainer = localPlayer->GetItemContainer();
			CItemSlot* pSlot = (CItemSlot*)pContainer->GetSlot(*FromePos);
			num = pSlot->GetNum();

			ItemPrototype_Client* itemPrototype = pSlot->GetItemProperty();
			if (itemPrototype && itemPrototype->Class == ITEM_CLASS_QUEST && m_ItemTypeID == ICT_BANK)
			{
				UMessageBox::MsgBox_s( _TRAN("Quest Item! The current operation can not be performed！") );
				return FALSE;
			}

		}
	}
   

	SYState()->UI->GetUItemSystem()->MoveItemMsg((ui8)nDataFlag, (ui8)m_ItemTypeID, *FromePos, ToPos, num);
	return TRUE;
}

//属性鼠标悬停显示
void UItemContainerEx::OnShowTipsInfo()
{
	int pos = m_GridCount.x*m_HoverGrid.y+m_HoverGrid.x;
	UPoint Tip_sit((m_HoverGrid.x+1)*m_GridSize.x,(m_HoverGrid.y+1)*m_GridSize.y);
	UPoint t_pt = WindowToScreen(Tip_sit);
	if (m_HoverGrid.x == -1 || m_HoverGrid.y == -1)
	{
		return ;
	}
	UItemData* pData = (UItemData*)GetItemData(m_HoverGrid.y,m_HoverGrid.x);
	if (pData && pData->pItemID != 0)
	{
		TipSystem->ShowTip(pData->pItemID, t_pt, pData->stItemExtraData);
		//TipSystem->ShowTip(TIP_TYPE_ITEM,pData->pItemID,t_pt);
	}
	
}
void UItemContainerEx::OnHideTipsInfo()
{
	TipSystem->HideTip();
}

BOOL UItemContainerEx::OnDropThis(UControl* pDragTo, UINT nDataFlag)
{
	if (m_bDrop)
	{
		return true;
	}else
	{
		m_bDrop = true;
		if (nDataFlag != UItemSystem::ICT_INGAME_SKILL)
			return false;
	}
	return true;
}