#pragma once
#include "UIItemSystem.h"
#include "UInc.h"
#include "../../../../SDBase/Public/SpellDef.h"
// 物品容器控件

struct UItemData
{	
	UItemData()
	{
		pItemID = 0;
		bLock = false;
	}
	ui32 pItemID;
	bool bLock ;
	bool bOnly;
	ItemExtraData stItemExtraData;
};

class UItemContainerEx : public UItemContainer 
{
	UDEC_CLASS(UItemContainerEx);
public:
	UItemContainerEx(void);
	virtual ~UItemContainerEx(void);
	void SetDragType(UItemSystem::UI_TYPE t_UI){m_ItemTypeID = t_UI;}
	BOOL SetItemByPos(ui8 pos, ui32 dwitemid, int num ,ItemExtraData& stItemExtraData, bool bLock = false);
	
	int  GetNumByPos(int pos);
	UItemSystem::ItemType GetItemType(){return m_Spell;}

	inline void SetLockDrag(bool bLock){m_bLock = bLock;};

public:
	virtual void SetGridCount(int nRow, int nCol);
	virtual UINT GetDataFlag();
protected:
	virtual BOOL OnCreate();
	virtual void OnMouseDragged(const UPoint& pt, UINT uFlags);
	virtual void OnDragDropAccept(UControl* pAcceptCtrl, const UPoint& point,const void* DragData, UINT nDataFlag);
	virtual void OnDragDropReject(UControl* pRejectCtrl, const void* DragData, UINT nDataFlag);
	virtual void OnDragDropEnd(UControl* pDragTo, void* DragData, UINT nDataFlag);
	virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom,const UPoint& point, const void* DragData, UINT nDataFlag);
	virtual void DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver);
	virtual void OnSize(const UPoint& NewSize);

	virtual void OnMouseMove(const UPoint& pt, UINT uFlags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& pt, UINT uFlags);
	virtual void OnCellHighlighted(const UPoint& cell);

	virtual void DeleteItem(int nRow, int nCol);

	virtual BOOL renderTooltip(const UPoint& cursorPos, const char* tipText /* = NULL */ );
	virtual void OnShowTipsInfo();
	virtual void OnHideTipsInfo();
	virtual BOOL OnDropThis(UControl* pDragTo, UINT nDataFlag);
protected:
	UItemSystem::ItemType m_Spell;		// 是否为技能包裹
	UItemSystem::UI_TYPE  m_ItemTypeID;
	UPoint m_InTipCell;
	UTexturePtr m_LockTexture ;
	bool m_bLock;
	bool m_bDrop;
};
