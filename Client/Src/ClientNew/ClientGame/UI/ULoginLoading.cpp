#include "StdAfx.h"
#include "ULoginLoading.h"
#include "UISystem.h"
#include "map/Map.h"
#include "Network/PacketBuilder.h"
#include "ObjectManager.h"
#include "UIPlayerHead.h"
#include "SystemSetup.h"

#include "PlayerInputMgr.h"
#include "Utils/MapInfoDB.h"
#include "UChat.h"
#include "UICastBar.h"
#include "UIIndex.h"
#include "UIGamePlay.h"

int g_LoadingTipRandIndex = 0;
const WCHAR* g_wcLoadingTip[] ={
	L"小提示：长按鼠标右键可旋转镜头，角色朝向不变",
	L"小提示：您可通过鼠标中键拉伸镜头调整可视范围",
	L"小提示：您可按住鼠标左右双键自由移动方向",
	L"小提示：按键盘“G”，人物即自动向前跑动",
	L"小提示：鼠标右键单击NPC即可与NPC对话",
	L"小提示：单击Enter键弹出聊天框，可选择频道进行对话",
	L"小提示：双击鼠标左键或鼠标右键单击敌人或怪物可实现自动攻击",
	L"小提示：您可使用“Tab”键切换选择怪物",
	L"小提示：鼠标右键单击怪物尸体可以拾取装备、任务道具及金钱奖励",
	L"小提示：鼠标左键点击技能不放，可将技能拖至快捷栏",
	L"小提示：您可在“游戏设置”中选择自动拾取物品的功能",
	L"小提示：绿色背景“！”的NPC接任务，红色背景“√”的NPC交任务",
	L"小提示：黄色背景“！”的NPC表示当前没有完成的任务",
	L"小提示：灰色背景“！”的NPC表示当前您的等级过低，不能接到NPC携带的任务",
	L"小提示：您可通过“按键设置”改变各种功能的快捷键",
	L"小提示：您可按“F9”隐藏所有用户界面",
	L"小提示：您可将道具拖放在快捷栏以方便使用",
	L"小提示：当别人帮助了你，别忘记说谢谢",
	L"小提示：您可按“B”直接打开背包",
	L"小提示：和平相处，细水长流",
	L"小提示：您可通过系统设置增加多一行快捷栏",
	L"小提示：善待他人，善待自己",
	L"小提示：您可装备不同部位的“时装”使自己看起来更有型",
	L"小提示：您可通过“帮助”查看生肖配对技巧",
	L"小提示：您可鼠标左键双击NPC售卖的物品来购买一件物品",
	L"小提示：您可使用键盘Shift+鼠标右键来设置购买物品的数量",
	L"小提示：请至各族主城寻找NPC部族长老选择创建及加入部族",
	L"小提示：鼠标右键单击变身图标即可取消变身",
	L"小提示：按键盘“P”即可选择适合您的副本",
	L"小提示：您可以通过 “游戏设置”来增加一行动作条",
	L"小提示：您可以使用键盘CTRL+鼠标左键单击装备来试穿一下",
	L"小提示：您可按空格键使飞行坐骑上升，按住鼠标右键不放同时点鼠标左键来控制飞行坐骑上下升降",
	L"小提示：您可调整好舒服的视角，通过游戏设置中的“锁定视角”功能来锁定它",
	L"小提示：在PK时，您可通过游戏设置中开启“血条职业颜色”更快的辨认出敌对方的职业",
	L"小提示：游戏设置中的“目标的目标”以及“显示血条”选项，都能帮助您更好的PK",
	L"小提示：丰富多彩的各种节日活动，可让您获得意外的惊喜",
	L"小提示：你可在聊天框输入/dnd “刷怪中” 申明当前自己的勿扰原因；再次输入/dnd可取消自己的勿扰状态",
	L"小提示：你可在聊天框输入/afk “吃饭中” 申明当前自己的暂离原因；再次输入/afk可取消自己的暂离状态",
	L"小提示：你的时装也可以精炼，并且精炼成功率100%哦",
	L"小提示：“装备精炼”可提高很多属性，有些部位精炼了还会发出耀眼的光芒哦",
	L"小提示：你可在“游戏设置”里面调出“增加附加/下方动作条”，可以把技能拖放到你想要的位置方便操作",
	L"小提示：“增加附加动作条”中还带有“横向/纵向”以及“趣味排列”功能",
	L"小提示：如果你需要自由旋转镜头，可在“游戏设置”里面把“镜头跟随”选项去掉。",
	L"小提示：你可以在“按键设置”中定义几种自己常用的快捷技能键，PK时会非常管用哦",
	L"小提示：在中立地图，你不但可以在陆地行走，还可以骑乘你喜欢的飞行坐骑遨游君天下，感受翻山越岭的畅快",
	L"小提示：“轻功术”可有效的帮你在急速下落中减缓下降速度。",
	L"小提示：不要去太高的地方哦，有可能会不小心掉下来摔死。",
	L"小提示：在游戏中你可以选择最多两种专业技能学习",
	L"小提示：你可以通过小地图上方的问号“？”查询称号的获得方式以及相关称号奖励",
	L"小提示：你在游戏中获得的称号越多，你就可以获得越多别人没有的坐骑哦",
	L"小提示：按M键，你不但可以了解自己当前的位置，还可以按右键了解君天下的世界地图分布哦",
	L"小提示：君天下大部分地区都很安全，可在一些个别地区，你要做好被强制PK的准备哦",
	L"小提示：“月钻宝石”和“星钻宝石”都是提高精炼几率的辅助类宝石，在精炼时不可一起使用哦",
	L"小提示：“可邀请坐骑”的种类丰富多彩，快带上你的朋友一起周游君天下吧",
	L"小提示：君天下中的时装造型各异，并且带有一定的属性，很具有收藏意义哦",
	L"小提示：风之云的全体员工希望大家玩君天下的同时能为贫困地区的孩子们读书生活捐献出一份爱心",
	L"小提示：限时开放的各类节日活动都会带有大量经验以及特殊道具奖励",
	L"小提示：不同等级的部族不但有特殊的部族技能，还会有自己的城池可以建设",
	L"小提示：如果你在练习生产技能时遇到了缺少材料的情况，别忘记问问看学习其他专业技能的朋友",
	L"小提示：开心游乐场里面的通用货币是：开心币。",
	L"小提示：你可通过自己的努力用赢来的开心币换取各种游乐场专有道具",
	L"小提示：开心游乐场里面的一些游乐项目需要你带有一些些小运气，祝你好运",
	L"小提示：怎样赢得更多的开心币呢？这就需要你掌握一些游乐场小窍门啦，自己去发掘吧！",
	L"小提示：“传送之石”可以帮你快速的瞬移到你的朋友身边哦",
	L"小提示：“换人大法”可以实现你与敌对玩家的位置对换，在PK时很有用的",
	L"小提示：还在为不用了的装备发愁吗？“解绑装备”道具可以使废弃的装备变的有价值哦",
	L"小提示：专业技能不但可以从技能师那里学到，有些BOSS也会掉一些极品技能配方哦",
	L"小提示：“蝙蝠-艾娃”是“战场大师”称号奖励的稀有飞行坐骑",
	L"小提示：“月亮船女王”是“游乐场达人”称号奖励的稀有飞行坐骑",
	L"小提示：打“勇敢的心”竞技场，可以兑换最快最酷的飞行坐骑——黑色幼龙",
	L"小提示：可战斗坐骑，可使你骑乘在坐骑上PK，追人、逃跑速度更快",
	L"小提示：在打开“君天下OL”时，别忘记想想看自己还有没有更加重要的工作或学习没做完呢？",
	L"小提示：合理分配你的工作，学习和娱乐时间，会使你的生活更充实快乐而不枯燥。",
	L"小提示：以诚待人，别人才会以诚回报。",
	L"小提示：别为小事发怒，游戏中也需要提高自身修养。",
	L"小提示：气量是一种情操，更是一种修养。只有拥有“气量”的人才真正懂得善待自己，善待他人。",
	//L"小提示：一男生对女朋友说：“我想分手，我觉得烦了，就没有感觉了。”女朋友对他说了一段让他顿时无语的话。“亿万中国人民对国足早就烦了。早就没感觉了，为什么国足还没有解散？13亿人的烦都没能解散一个11个人的队伍，现在你一个人说烦了，要解散两个人的队伍？！",
	//L"小提示：据新华社报道，美国加州一个餐馆因使用不卫生餐具，造成200名食客腹泻呕吐住院，唯有一名来自中国的十五岁女生，不仅没有任何不适感觉，而且还参与了抢救患者，记者采访时该女生说道：“毒奶粉，苏丹红，地沟油都弄不死我，你们这点东西算个屁啊，老娘能在中国活十五年，这点儿抵抗力还是有的。” ",
	//L"小提示：女子抵抗强奸的一个好办法是“迅速在裤裆里拉一坨大便”。"
};

const float MaxLoadingTime = 30.f;
static float LoadingTime = 0.f;
UIMP_CLASS(ULoadingFrame, UControl)
void ULoadingFrame::StaticInit()
{

}

ULoadingFrame::ULoadingFrame()
{
	m_IsLoading = FALSE;
	m_BKGTexture = NULL;
	m_ProBar = NULL;
	mELoadingType = LOGININLOADING;
	mLoginLoadingOK = FALSE;
	
	m_LastTranMap = 0;
	m_NowWorldMap = 0;
}

ULoadingFrame::~ULoadingFrame()
{

}

BOOL ULoadingFrame::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_BKGTexture == NULL)
	{
		m_BKGTexture = sm_UiRender->LoadTexture("Data\\World\\Loading\\loading_0.jpg");
		if (!m_BKGTexture)
		{
			return FALSE;
		}
	}
	if (m_ProBar == NULL)
	{
		m_ProBar = UDynamicCast(UProgressBar, GetChildByID(0));
		if (!m_ProBar)
		{
			return FALSE;
		}
		m_ProBar->SetTextVisible(FALSE);
	}
	return TRUE;
}

void ULoadingFrame::OnDestroy()
{
	m_IsLoading = FALSE;
	UControl::OnDestroy();
}

void ULoadingFrame::OnTimer(float fDeltaTime)
{
	if (m_IsLoading)
	{
		LoadingTime += fDeltaTime;
		UpDataLoading();
	}
	UControl::OnTimer(fDeltaTime);
}

void ULoadingFrame::OnRender(const UPoint& offset,const URect &updateRect)
{
	sm_UiRender->DrawImage(m_BKGTexture,m_BKGImageRect);
	RenderChildWindow(offset, updateRect);
	DrawNewBirdTip();
}


void ULoadingFrame::InitLoading(ELoadingType type)
{
	//sm_System->PushDialog(this);
	//mELoadingType = type;
	//LoadingTime = 0.f;
	//mLoginLoadingOK = FALSE;
	//m_LastTranMap = 0;
}

void ULoadingFrame::InitLoading(ELoadingType type, ui32 mapid)
{
	sm_System->PushDialog(this);
	mELoadingType = type;
	LoadingTime = 0.f;
	mLoginLoadingOK = FALSE;

	CMapInfoDB::MapInfoEntry entry;
	if (g_pkMapInfoDB)
	{
		g_pkMapInfoDB->GetMapInfo(mapid, entry);
	}
	m_NowWorldMap = mapid;
	char buf[256];
	sprintf(buf, "Data\\World\\Loading\\loading_%d.jpg", entry.screenid);
	m_BKGTexture = sm_UiRender->LoadTexture(buf);
	if (!m_BKGTexture)
	{
		m_BKGTexture = sm_UiRender->LoadTexture("Data\\World\\Loading\\loading_0.jpg");
	}
	int imageheight = GetWindowSize().y - GetChildByID(1)->GetWindowSize().y - GetChildByID(2)->GetWindowSize().y + 20;
	int xpos = (GetWindowSize().x - m_BKGTexture->GetWidth())>>1;
	int ypos = (GetWindowSize().y - m_BKGTexture->GetHeight())>>1;
	m_BKGImageRect.pt0.x = min(xpos,0);
	int yPosPad = GetChildByID(1)->GetWindowSize().y -10;
	m_BKGImageRect.pt0.y = min(ypos, yPosPad);
	m_BKGImageRect.pt1.x = m_BKGImageRect.pt0.x + max(m_BKGTexture->GetWidth(), GetWindowSize().x);
	m_BKGImageRect.pt1.y = m_BKGImageRect.pt0.y + max(m_BKGTexture->GetHeight(), imageheight);
}

void ULoadingFrame::BeginLoading()
{
	m_IsLoading = TRUE;
	CaptureControl();
	SetFocusControl();
	CPlayerLocal * pkPlayer = ObjectMgr->GetLocalPlayer();
	if (pkPlayer){
		pkPlayer->StopMove();
		pkPlayer->SetChangMap(TRUE);
	}
	int LoadingTipMax = sizeof(g_wcLoadingTip)/sizeof(g_wcLoadingTip[0]);
	g_LoadingTipRandIndex = rand()%LoadingTipMax;

	//设置传送过程中是安全的
	PacketBuilder->SendEnterLeavePVPZone(true, 2);
}

void ULoadingFrame::UpDataLoading()
{
	m_ProBar->SetProgressPos(int(LoadingTime*1000), int(MaxLoadingTime*1000));
	CPlayer * pkPlayer = ObjectMgr->GetLocalPlayer();
	CMap* pkmap =CMap::Get();
	if (pkmap && pkmap->IsLoaded())
	{
		if ( mELoadingType == LOGININLOADING && !mLoginLoadingOK)
		{
			PacketBuilder->SendLoadingOKMsg();
			LoadingTime = max(29.f, LoadingTime);
			mLoginLoadingOK = TRUE;
		}
		LoadingTime = max(25.f, LoadingTime);
		if (pkPlayer && pkPlayer->IsActorLoaded())
		{
			pkPlayer->GetCamera().LookPlayer();
			LoadingTime = max(28.f, LoadingTime);
			if (LoadingTime >= MaxLoadingTime)
			{
				EndLoading();
			}
		}
	}
}

void ULoadingFrame::EndLoading()
{
	m_IsLoading = FALSE;
	if (mELoadingType == LOGININLOADING)
	{
		if (!SystemSetup->LoadFile(FILE_USERSETTING))
		{
			ULOG( _TRAN("用户配置加载成功！！") );
		}
		PacketBuilder->SendRequestNeedRead();
		PacketBuilder->SendShopGetList();
		ChatSystem->WaitMapLoading();
		char tmp[256];
		sprintf( tmp, "zone%d", m_NowWorldMap);
		ChatSystem->SendChannelJoin(tmp,CHAT_CHANNEL_TYPE_CITY);
		m_LastTranMap = m_NowWorldMap;
	}
	if (mELoadingType == MAPLOADING)
	{
		char tmp[256];
		sprintf( tmp, "zone%d", m_LastTranMap);
		ChatSystem->SendChannelLeave(tmp);
		sprintf( tmp, "zone%d", m_NowWorldMap);
		ChatSystem->SendChannelJoin(tmp,CHAT_CHANNEL_TYPE_CITY);

		m_LastTranMap = m_NowWorldMap;
	}
	sm_System->PopDialog(this);


	SYState()->LocalPlayerInput->ClearInput();
	CPlayerLocal * pkPlayer = ObjectMgr->GetLocalPlayer();
	if (pkPlayer)
	{
		pkPlayer->SetChangMap(FALSE);
		pkPlayer->ContinuePathMove();
	}
	USpellProcessBar* pSpellProcessBar = INGAMEGETFRAME(USpellProcessBar, FRAME_IG_CASTBAR);
	if (pSpellProcessBar)
		pSpellProcessBar->SpellCastEnd();
	pSpellProcessBar = INGAMEGETFRAME(USpellProcessBar, FRAME_IG_TARGETCASTBAR);
	if (pSpellProcessBar)
		pSpellProcessBar->SpellCastEnd();
	pSpellProcessBar = INGAMEGETFRAME(USpellProcessBar, FRAME_IG_TARGETTARGETCASTBAR);
	if (pSpellProcessBar)
		pSpellProcessBar->SpellCastEnd();
}

BOOL ULoadingFrame::IsLoading()
{
	return m_IsLoading;
}
void ULoadingFrame::DrawNewBirdTip()
{
	UPoint TipPos;
	TipPos.y = m_ProBar->GetControlRect().bottom + 5;
	TipPos.x = 0;
	USetTextEdge(TRUE);
	UDrawStaticText(sm_UiRender, m_Style->m_spFont, TipPos, 
		m_Size.x, m_Style->m_FontColor, g_wcLoadingTip[g_LoadingTipRandIndex], UT_CENTER);
	USetTextEdge(FALSE);
}