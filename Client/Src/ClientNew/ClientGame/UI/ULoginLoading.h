#ifndef ULOGINLOADING_H
#define ULOGINLOADING_H

#include "UInc.h"
enum ELoadingType
{
	LOGININLOADING,
	MAPLOADING
};
class ULoadingFrame : public UControl
{
	UDEC_CLASS(ULoadingFrame);
public:
	ULoadingFrame();
	~ULoadingFrame();
public:
	void InitLoading(ELoadingType type);
	void InitLoading(ELoadingType type, ui32 mapid);
	void BeginLoading();
	void UpDataLoading();
	void EndLoading();

	BOOL IsLoading();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
	virtual BOOL HitTest(const UPoint& parentCoordPoint){return TRUE;};
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags){return TRUE;};
	virtual BOOL renderTooltip(UPoint cursorPos, const char* tipText  = NULL  ){return FALSE;};
protected:
	void DrawNewBirdTip();
private:
	BOOL m_IsLoading;
	BOOL mLoginLoadingOK;
	ELoadingType mELoadingType;
	UTexturePtr m_BKGTexture;
	class UProgressBar* m_ProBar;
	URect m_BKGImageRect;

	ui32 m_NowWorldMap;
	ui32 m_LastTranMap;
};
#endif