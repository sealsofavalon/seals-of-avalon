#include "stdafx.h"
#include "UMailSystem.h"
#include "Network/PacketBuilder.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "../LocalPlayer.h"
#include "ObjectManager.h"
#include "UMessageBox.h"
#include "ItemSlot.h"


#include "UIMailContent.h"
#include "UIMailReceiveList.h"
#include "UIMailSend.h"

BOOL CMailSystem::CreateMgrControl(UControl* ctrl)
{
	if (ctrl)
	{
		if (m_UMailSend == NULL)
		{
			UControl* pkControl = UControl::sm_System->CreateDialogFromFile("MailSystem\\SendMail.udg");
			m_UMailSend = UDynamicCast(UMailSend, pkControl);
			if (m_UMailSend == NULL)
			{
				ULOG("create UAuctionAucDlg UI Faild!");
				return FALSE;
			}else
			{
				ctrl->AddChild(m_UMailSend);
				m_UMailSend->SetId(FRAME_IG_MAILSENDDLG);
				
			}
		}

		if (m_UMailList == NULL)
		{
			UControl* pkContorl = UControl::sm_System->CreateDialogFromFile("MailSystem\\MailReceivel.udg");
			m_UMailList = UDynamicCast(UMailReceiveList, pkContorl);
			if (m_UMailList == NULL)
			{
				ULOG("create UAuctionAucDlg UI Faild!");
				return FALSE;
			}else
			{
				ctrl->AddChild(m_UMailList);
				m_UMailList->SetId(FRAME_IG_MAILRECEIVELISTDLG);
				
			}
		}

		if (m_UMailContent == NULL)
		{
			UControl* pkContorl = UControl::sm_System->CreateDialogFromFile("MailSystem\\MailContent.udg");
			m_UMailContent = UDynamicCast(UMailContent, pkContorl);
			if (m_UMailContent == NULL)
			{
				ULOG("create UAuctionAucDlg UI Faild!");
				return FALSE;
			}else
			{
				ctrl->AddChild(m_UMailContent);
				m_UMailContent->SetId(FRAME_IG_MAILCONTENTDLG);
				
			}
		}
		m_UMailContent->SetVisible(FALSE);
		m_UMailList->SetVisible(FALSE);
		m_UMailSend->SetVisible(FALSE);
	
		return TRUE;
	}

	return FALSE;
}
CMailSystem::CMailSystem()
{
	m_StMailInfo =  new MSG_S2C::stMail_List_Result;
	m_CurMailbody = new MSG_S2C::stMail_Text_Query_Response;

	m_bNeedRequest = FALSE;
	m_CurMailID = 0;
	m_CurItemGuid = 0;
	m_NPCguid = 0;
	m_NeedReadCount = 0;


	m_UMailSend = NULL;
	m_UMailContent = NULL;
	m_UMailList = NULL;
	m_MailState = UMailStateNormal;
}

CMailSystem::~CMailSystem()
{
	m_CurMailID = 0;
	m_CurItemGuid = 0;
	m_MailState = UMailStateNormal;
	if (m_StMailInfo)
	{
		delete m_StMailInfo;
		m_StMailInfo = NULL;
	}
	if (m_CurMailbody)
	{
		delete m_CurMailbody;
		m_CurMailbody = NULL;
	}
}

void CMailSystem::InitMailList(const MSG_S2C::stMail_List_Result& stmail)
{
	m_StMailInfo->vMails = stmail.vMails;

	assert(m_StMailInfo);
	//UI显示邮件列表。

	m_CurMailID = 0;
	m_CurItemGuid = 0;
	m_NPCguid = stmail.npc_guid;
	
	CPlayerLocal::SetCUState(cus_Mail);
	if (m_StMailInfo->vMails.size() > MaxListMailNum)
	{
		assert(0);
	}
	if (m_StMailInfo->vMails.size() == MaxListMailNum)
	{
		m_bNeedRequest = TRUE;
	}else
	{
		m_bNeedRequest = FALSE;
	}
	m_UMailList->SetUMailList(m_StMailInfo);

	HELPEVENTTRIGGER(TUTORIAL_FLAG_MAILBOX);
}

MSG_S2C::stMail_List_Result::stMail* CMailSystem::GetMailINFO(ui32 Mail_id) 
{
	if (m_StMailInfo && m_StMailInfo->vMails.size() > 0)
	{
		for(unsigned int i = 0; i < m_StMailInfo->vMails.size(); i++)
		{
			if (m_StMailInfo->vMails[i].message_id == Mail_id)
			{
				return &(m_StMailInfo->vMails[i]);
			}

		}
	}

	return NULL;
	
}

void CMailSystem::UpdateMailByMailID(ui32 Mail_id, ui64 itemGuid, ui32 itemCount)
{
	MSG_S2C::stMail_List_Result::stMail* pMail = GetMailINFO(Mail_id);
	assert(pMail);

	for( vector<MSG_S2C::stMail_List_Result::stMail::stMailItem>::iterator it= pMail->vMailItems.begin(); it < pMail->vMailItems.end(); ++it)
	{
		MSG_S2C::stMail_List_Result::stMail::stMailItem pItem = *it;
		if (pItem.guid == itemGuid)
		{
			pMail->vMailItems.erase(it);
			break ;
		}
	}
	pMail->cod = 0;

	//刷新UI。
	
	
	m_UMailList->UpdateMailByID(Mail_id);
	
	if (m_UMailContent->GetMailID() == Mail_id)
	{
		m_UMailContent->SetMailContent(m_CurMailbody->message_id,m_CurMailbody->body);
	}
	
}

void CMailSystem::UpdateMailMoney(ui32 Mail_id)
{
	MSG_S2C::stMail_List_Result::stMail* pMail = GetMailINFO(Mail_id);
	assert(pMail);
	pMail->money = 0;

	//刷新UI。

	if (m_UMailContent->GetMailID() == Mail_id && Mail_id == m_CurMailbody->message_id)
	{
		m_UMailContent->SetMailContent(m_CurMailbody->message_id,m_CurMailbody->body);
	}
}
void CMailSystem::SetNeedReadConut(UINT count)
{
	m_NeedReadCount = count;
	if (count > 0)
	{
		m_bNeedRequest = TRUE;
	}
}
void CMailSystem::UpdateContent()
{
	m_UMailContent->SetCanGet();
}
void CMailSystem::SendbReQuestList() //判断是否需要重新请求邮件列表信息
{
	if (m_bNeedRequest)
	{
		PacketBuilder->SendRequestMailList();
	}
}

BOOL CMailSystem::DeletaMailByID(ui32 Mail_id)
{
	MSG_S2C::stMail_List_Result::stMail* pMail = GetMailINFO(Mail_id);
	assert(pMail);

	//判断删除的邮件是否是正在显示的邮件

	if (Mail_id == m_UMailContent->GetMailID())
	{
		m_UMailContent->SetToDefault();
		m_CurMailbody->message_id = 0;
		m_CurMailbody->body = "";
	}

	//如果开始列表有50封邮件，就需要重新请求。
	if (m_bNeedRequest)
	{
		SendbReQuestList();
		return TRUE;
	}

	if (pMail && pMail->read_flag == 0)
	{
		m_NeedReadCount--;

		if (m_NeedReadCount <= 0)
		{
			m_NeedReadCount = 0;
			PacketBuilder->SendRequestNeedRead();
		}
	}

	for(vector<MSG_S2C::stMail_List_Result::stMail>::iterator  it = m_StMailInfo->vMails.begin(); it< m_StMailInfo->vMails.end(); ++it)
	{
		MSG_S2C::stMail_List_Result::stMail pMail = *it ;
		if (pMail.message_id == Mail_id)
		{
			m_StMailInfo->vMails.erase(it);
			InitMailList(*m_StMailInfo);
			return TRUE;
		}
	}

	return FALSE;
}
void CMailSystem::SendBRead(ui32 Mail_id)
{
	//发送已经阅读的消息
	PacketBuilder->SendReadMail(Mail_id);
}
void CMailSystem::SendOpenMail(ui32 Mail_id)
{
	//发送打开邮件请求即可。
	PacketBuilder->SendRequestMailContent(Mail_id);
}
void CMailSystem::SendGetMoney(ui32 Mail_id)
{
	m_CurMailID = Mail_id;
	//UMessageBox::MsgBox("您确认提取金钱么？", (BoxFunction*)CMailSystem::stGetMoney, (BoxFunction*)CMailSystem::stCancel);
	
	CMailSystem::stGetMoney();
	//发送提取钱的消息
}
void CMailSystem::SendGetItem(ui32 Mail_id, ui64 Guid)
{
	//发送提取物品的消息
	m_CurMailID = Mail_id; 
	m_CurItemGuid = Guid;

	MSG_S2C::stMail_List_Result::stMail*  pkMail = GetMailINFO(Mail_id);
	if (pkMail)
	{
		//char buf[255];

		if (pkMail->cod)
		{
			UINT g = pkMail->cod / 10000 ;
			UINT s = (pkMail->cod % 10000) / 100 ;
			UINT c = (pkMail->cod % 10000) % 100 ;
			//NiSprintf(buf,255,"提取：你需要支付%d金%d银%d铜", g,s,c);
			std::string buff = _TRAN( N_NOTICE_C29, _I2A( g ).c_str(), _I2A( s ).c_str(), _I2A( c ).c_str());

			UMessageBox::MsgBox(buff.c_str(),(BoxFunction*)CMailSystem::stGetItem, (BoxFunction*)CMailSystem::stCancel);
		}else
		{
			CMailSystem::stGetItem();
			//UMessageBox::MsgBox("您确认需要提取物品？",(BoxFunction*)CMailSystem::stGetItem ,(BoxFunction*)CMailSystem::stCancel);
		}
	}
	
	
}
void CMailSystem::SendDelMailByID(ui32 Mail_id)
{
	m_CurMailID = Mail_id;
	MSG_S2C::stMail_List_Result::stMail*  pkMail = GetMailINFO(Mail_id);

	if (pkMail)
	{
		string Txt = "";
		if (pkMail->cod)
		{
			Txt = _TRAN("您没支付货款，删除无法恢复！确认删除？");
		}else if (pkMail->vMailItems.size())
		{
			Txt = _TRAN("还有物品没有提取，确认删除么？");
		}else if (pkMail->money)
		{
			Txt = _TRAN("还有金钱没有提取，确认删除么？");
		}else
		{
			Txt = _TRAN("确认要删除邮件么？");
		}
		UMessageBox::MsgBox(Txt.c_str(),(BoxFunction*)CMailSystem::stDelMail, (BoxFunction*)CMailSystem::stCancel);
	}

	

	
}

void CMailSystem::stGetMoney()
{
	PacketBuilder->SendGetMailMoney(MailSystemMgr->m_CurMailID);
	MailSystemMgr->m_CurMailID = 0;
	MailSystemMgr->m_CurItemGuid = 0;
	
}
void CMailSystem::stGetItem()
{
	PacketBuilder->SendGetMailItem(MailSystemMgr->m_CurMailID, (ui32)MailSystemMgr->m_CurItemGuid);
	MailSystemMgr->m_CurMailID = 0;
	MailSystemMgr->m_CurItemGuid = 0;
}
void CMailSystem::stDelMail()
{
	if (MailSystemMgr)
	{
		PacketBuilder->SendDeleteMail(MailSystemMgr->m_CurMailID);
		MailSystemMgr->m_CurMailID = 0;
		MailSystemMgr->m_CurItemGuid = 0;
	}
	
}
void CMailSystem::stCancel()
{
	if (MailSystemMgr)
	{
		MailSystemMgr->m_CurMailID = 0;
		MailSystemMgr->m_CurItemGuid = 0;
		MailSystemMgr->m_UMailContent->SetCanGet();
	}
}
void CMailSystem::ReSendToOthers(ui32 Mail_id) //转发
{
	//操作当前打开的邮件的内容
	if (Mail_id == m_UMailContent->GetMailID())
	{
		m_UMailSend->SetContent(m_CurMailbody->body);
		MailSystemMgr->SetMailState(CMailSystem::UMailState_Send);
	}
}
void CMailSystem::RevertMail(ui32 Mail_id)//回复
{
	MSG_S2C::stMail_List_Result::stMail* pMailInfo  = GetMailINFO(Mail_id);
	
	assert(pMailInfo);
	if (Mail_id == m_UMailContent->GetMailID())
	{
		char recepient[20];  
		if (m_UMailContent->GetSenderName(recepient,20))
		{
			m_UMailSend->SetRecepient(recepient);
		}
		MailSystemMgr->SetMailState(CMailSystem::UMailState_Send);
	}
}
void CMailSystem::ShieldSender(ui32 Mail_id)//屏蔽
{
	//
}
void CMailSystem::SetSendMail(MSG_C2S::stMail_Send Mail)   //设定发送的邮件的相关信息。
{
	smMail = Mail;

	if (Mail.money > 0)
	{
		UMessageBox::MsgBox( _TRAN("您的信中要寄送金钱，是否继续？"),(BoxFunction*)CMailSystem::SendMail);
	}
	else
	{
		SendMail();
	}
}

void CMailSystem::SendMail()
{
	PacketBuilder->SendMail(MailSystemMgr->smMail);
	MailSystemMgr->smMail.body.clear();
	MailSystemMgr->smMail.cod = 0;
	MailSystemMgr->smMail.money = 0;
	MailSystemMgr->smMail.recepient.clear();
	MailSystemMgr->smMail.subject.clear();
	MailSystemMgr->smMail.vMailItems.clear();

	MailSystemMgr->m_UMailSend->ClearUI();
	ObjectMgr->GetLocalPlayer()->ReleaseLock();
	
}
void CMailSystem::SetCurReadMail(MSG_S2C::stMail_Text_Query_Response pMail)
{
	
	m_CurMailbody->body = pMail.body;
	m_CurMailbody->message_id = pMail.message_id;

	//显示邮件内容

	m_UMailContent->SetMailContent(m_CurMailbody->message_id,m_CurMailbody->body);


	MSG_S2C::stMail_List_Result::stMail* pkMailP = GetMailINFO(pMail.message_id);
	if (pkMailP && pkMailP->read_flag == 0)
	{
		SendBRead(pMail.message_id);
		//更新阅读标示
		pkMailP->read_flag  = 1;
		m_UMailList->UpdateMailByID(pMail.message_id);
		//

		m_NeedReadCount--;

		if (m_NeedReadCount <= 0)
		{
			m_NeedReadCount = 0;
			PacketBuilder->SendRequestNeedRead();
		}
	}
}

void CMailSystem::AddInclosureItem(ui8 pkPos, ui8 ToPos)  //添加新的
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	CItemSlot* pkSolt = (CItemSlot*)pkLocal->GetItemContainer()->GetSlot(pkPos);
	ui64 Itemguid = pkSolt->GetGUID();
	if (m_UMailSend->SetInclosureItem(ToPos, Itemguid))
	{
		pkSolt->SetLock(TRUE);
		SYState()->UI->GetUItemSystem()->SetUIIcon(UItemSystem::ICT_BAG, pkPos, pkSolt->GetCode(),pkSolt->GetNum(),UItemSystem::ITEM_DATA_FLAG,true);

	}
}
void CMailSystem::SetDefaultCUState()
{
	if (m_UMailContent && m_UMailSend && m_UMailList)
	{
		m_UMailSend->SetToDefault();
		m_UMailContent->SetToDefault();
		m_UMailList->SetToDefault();
		m_NPCguid = 0 ;
	}
}
void CMailSystem::SetMailState(UMailState state)
{
	m_MailState = state;
	switch(state)
	{
	case UMailStateNormal:
		SetDefaultCUState();
		break;
	case UMailState_List:
		{
			m_UMailList->SetVisible(TRUE);
			m_UMailList->SetPosition(m_UMailSend->GetWindowPos());
			m_UMailSend->SetVisible(FALSE);

			UButton* pBTN1 = UDynamicCast(UButton, m_UMailList->GetChildByID(0));
			UButton* pBTN2 = UDynamicCast(UButton, m_UMailList->GetChildByID(1));

			if (pBTN1)
			{
				pBTN1->SetCheckState(FALSE);
			}

			if (pBTN2)
			{
				pBTN2->SetCheckState(TRUE);
			}
		}
		break;
	case UMailState_Send:
		{
			m_UMailSend->SetVisible(TRUE);
			m_UMailSend->SetPosition(m_UMailList->GetWindowPos());
			m_UMailList->SetVisible(FALSE);

			UButton* pBTN1 = UDynamicCast(UButton, m_UMailSend->GetChildByID(0));
			UButton* pBTN2 = UDynamicCast(UButton, m_UMailSend->GetChildByID(1));

			if (pBTN1)
			{
				pBTN1->SetCheckState(TRUE);
			}

			if (pBTN2)
			{
				pBTN2->SetCheckState(FALSE);
			}
		}
		break;
	}
}
void CMailSystem::CheckCuState()
{
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	if (!pLocal)
	{
		return ;
	}

	if (!m_NPCguid)
	{
		if (CPlayerLocal::GetCUState() == cus_Mail)
		{
			CPlayerLocal::SetCUState(cus_Normal);
			ULOG( _TRAN("邮件相关(cus_Normal)") );
		}
		return ;
	}

	CCharacter* pkObj = (CCharacter*) ObjectMgr->GetObject(m_NPCguid);
	if (!pkObj)
	{
		return ;
	}

	NiPoint3 pCreaturePos = pkObj->GetPosition();
	NiPoint3 pLocalPlayerPos = pLocal->GetLocalPlayerPos();

	if ((pLocalPlayerPos - pCreaturePos).Length() > MaxLenToNPC)
	{
		CPlayerLocal::SetCUState(cus_Normal);
		ULOG( _TRAN("邮件相关(cus_Normal)") );
		return ;
		
	}
}