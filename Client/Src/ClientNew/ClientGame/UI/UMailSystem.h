#ifndef __UMAILSYSTEM_H__ 
#define __UMAILSYSTEM_H__

#include "../../../../SDBase/Protocol/S2C_Mail.h"
#include "../../../../SDBase/Protocol/C2S_Mail.h"
#include "Singleton.h"
#define MaxListMailNum   50			  //收件箱最多邮件
#define MailSystemMgr CMailSystem::GetSingleton() 
class CMailSystem : public Singleton<CMailSystem>
{
	SINGLETON(CMailSystem)
public:
	enum UMailState
	{
		UMailStateNormal = 0,
		UMailState_List, 
		UMailState_Send,   
	};
	BOOL CreateMgrControl(UControl* ctrl);  //UI
	void InitMailList(const MSG_S2C::stMail_List_Result& stmail);					//初始化邮件
	MSG_S2C::stMail_List_Result::stMail* GetMailINFO(ui32 Mail_id);			//查询邮件相关信息
	ui64 GetNPCGuid(){return m_NPCguid;}
	void UpdateMailByMailID(ui32 Mail_id, ui64 itemGuid, ui32 itemCount);	//更新单个邮件(物品信息)
	void UpdateMailMoney(ui32 Mail_id);										//刷新单个邮件的钱。
	BOOL DeletaMailByID(ui32 Mail_id);										//删除邮件
	void UpdateContent(); // 设置显示邮箱内容为可提取状态
	void SetNeedReadConut(UINT count);
	void SetAddNeedReadCount(){m_NeedReadCount++ ; m_bNeedRequest = TRUE;}
	void SendbReQuestList(); //判断是否需要重新请求邮件列表信息
	//////////////////////////////////////////////////////////////////////////
	void SendBRead(ui32 Mail_id);						//标记已读
	void SendOpenMail(ui32 Mail_id);					//打开相关邮件
	void SendGetMoney(ui32 Mail_id);					//发送提取钱
	void SendGetItem(ui32 Mail_id, ui64 Guid);			//发送提取物品
	void SendDelMailByID(ui32 Mail_id);					//请求删除邮件
	//////////////////////////////////////////////////////////////////////////
	static void stGetMoney();
	static void stGetItem();
	static void stDelMail();
	static void stCancel();
	//////////////////////////////////////////////////////////////////////////
	void ReSendToOthers(ui32 Mail_id);					//转发
	void RevertMail(ui32 Mail_id);						//回复
	void ShieldSender(ui32 Mail_id);					//屏蔽
	MSG_C2S::stMail_Send smMail;
	static void SendMail();
	void SetSendMail(MSG_C2S::stMail_Send Mail);					//设定发送的邮件的相关信息。
	void SetCurReadMail(MSG_S2C::stMail_Text_Query_Response pMail);
	void CheckCuState();
	void SetDefaultCUState();
	void SetMailState(UMailState state);
public:
	void AddInclosureItem(ui8 pkPos, ui8 ToPos);  //添加新的
public:
	class UMailSend* m_UMailSend;
	class UMailContent* m_UMailContent;
	class UMailReceiveList* m_UMailList;
protected:
	ui64 m_NPCguid;
	BOOL m_bNeedRequest;  // 删除邮件后是否需要重新请求邮件列表信息
	UINT  m_NeedReadCount; // 标示未读邮件的数量
	UMailState m_MailState;
	MSG_S2C::stMail_List_Result* m_StMailInfo;				// 邮件列表
	MSG_S2C::stMail_Text_Query_Response* m_CurMailbody;		//当前请求的邮件的信息
private:
	//当前操作的邮件对象的ID. 这里操作包括提取物品，提取金钱，删除操作
	ui32 m_CurMailID; //在向服务器发完消息后， 这里有家ID设为0.
	ui64 m_CurItemGuid;
};

#endif