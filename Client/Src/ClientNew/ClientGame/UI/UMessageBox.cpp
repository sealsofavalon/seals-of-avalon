#include "stdafx.h"
#include "UMessageBox.h"
#include "UISystem.h"

UIMP_CLASS(UBkgControl,UControl);
void UBkgControl::StaticInit()
{

}

//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UMessageBox,UDialog);
//事件响应
UBEGIN_MESSAGE_MAP(UMessageBox,UDialog)
UON_BN_CLICKED(1, &UMessageBox::OnClickOK)
UON_BN_CLICKED(2, &UMessageBox::OnClickCancel)
UEND_MESSAGE_MAP()

void UMessageBox::StaticInit()
{

}

UMessageBox::UMessageBox()
{
	m_bOKBtn = NULL;
	m_bCancelBtn = NULL;
	m_bShowText = NULL;
	m_pkShowTime = NULL;

	m_WaitTime = 0.0f;
	m_bShow = FALSE;
	m_CallOKFunFirst = false;
    m_fDelayShowTime = 0.f;
    m_bDisableEscape = false;
}
UMessageBox::~UMessageBox()
{
	
}

BOOL UMessageBox::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE ;
	}
	
	m_bShowText = UDynamicCast(UMessageTxt,GetChildByID(0));
	if (m_bShowText == NULL)
	{
		return FALSE;
	}
	m_bOKBtn = UDynamicCast(UBitmapButton, GetChildByID(1));
	if (m_bOKBtn == NULL)
	{
		return FALSE ;
	}
	m_bCancelBtn = UDynamicCast(UBitmapButton,GetChildByID(2));

	if (m_bCancelBtn == NULL)
	{
		return FALSE;
	}
	m_pkShowTime = UDynamicCast(UMessageTime, GetChildByID(3));
	if (m_pkShowTime == NULL)
	{
		return FALSE ;
	}

	return TRUE;
}
void UMessageBox::OnDestroy()
{
	UDialog::OnDestroy();
}


void UMessageBox::OnClickOK()
{
	if (m_OKFun)
	{
		(*m_OKFun)();
	}

    Pop();
}
void UMessageBox::OnClose()
{
	if (m_CancelFun)
	{
		OnClickCancel();
	}else
	{
		OnClickOK();
	}
	
}
void UMessageBox::OnClickCancel()
{
	if (m_CancelFun)
	{
		(*m_CancelFun)();
	}

    Pop();
}

void UMessageBox::Pop()
{
    m_bShow = FALSE;
    m_MsgQueue.pop();
    //继续显示下一条
    ShowMessage();
}


void UMessageBox::OnShowDagText(const char* txt, float WaitTime)
{
	int num = strlen(txt);
	if (m_bShowText)
	{
		m_bShowText->Clear();
		m_bShowText->SetText(txt);
		if (WaitTime > 0.0f)
		{
			m_pkShowTime->SetWaitTime(WaitTime);
		}
	}
}
void UMessageBox::OnShowDagText(const WCHAR* txt, float WaitTime)
{
	if ( !txt )
	{
		return;
	}
	int num = wcslen(txt);
	if (m_bShowText)
	{
		m_bShowText->SetText(txt);

		if (WaitTime > 0.0f)
		{
			m_pkShowTime->SetWaitTime(WaitTime);
		}
	}
}

void UMessageBox::ShowMessage()
{
    m_fDelayShowTime = 0.0f;
    m_bDisableEscape = false;

	//如果没有了消息。 POP掉！
	if (m_MsgQueue.size() == 0)
	{
		UControl* pMsgParent = SYState()->UI->LoadDialogEX(DID_MSGBOX);
		sm_System->PopDialog(pMsgParent);

		m_WaitTime = 0.0f;
		m_bShowText->SetText("");
		sm_System->PopDialog(GetParent());
		m_bShow = FALSE;
		m_CallOKFunFirst = false;
		SetFocusControl(sm_System->GetCurFrame());
		m_bShow = FALSE;

		return ;
	}
	if (m_bShow == TRUE)
	{
		return ;
	}
	m_bShow = TRUE;
	uSYMessageData pData = m_MsgQueue.front();

	OnShowDagText(pData.pMessageTxt, pData.pWaitTime);
	if (pData.pShowCancel)
	{
		m_WaitTime = pData.pWaitTime;
		m_OKFun = pData.pOKFunction;
		m_CancelFun = pData.pCancelFunction;
		m_CallOKFunFirst = pData.pCallOkFunFirst;

		UINT pWidth = GetWidth();
		UINT OKBtnWidth = m_bOKBtn->GetWidth();
		m_bOKBtn->SetLeft((pWidth - OKBtnWidth)/2);
		m_bCancelBtn->SetVisible(TRUE);
	}else
	{
		m_WaitTime = pData.pWaitTime;
		m_OKFun = pData.pOKFunction;
		m_CallOKFunFirst = pData.pCallOkFunFirst;
		m_CancelFun = NULL;

		UINT CancelLeft = m_bCancelBtn->GetLeft();
		m_bOKBtn->SetLeft(CancelLeft);
		m_bCancelBtn->SetVisible(FALSE);	
	}
    m_bOKBtn->SetVisible(TRUE);
	SetVisible(TRUE);
}
void UMessageBox::MsgBox_s(const char* txt,BoxFunction OKFun,float WaitTime)
{
	UControl* pMsgParent = SYState()->UI->LoadDialogEX(DID_MSGBOX);
	sm_System->PushDialog(pMsgParent);
	UMessageBox* pMessageBox = (UMessageBox*)pMsgParent->GetChildByID(100);

	uSYMessageData pMSGData = uSYMessageData(txt, OKFun,NULL,WaitTime,FALSE, TRUE);
	pMessageBox->m_MsgQueue.push(pMSGData);
	pMessageBox->m_fDelayShowTime = 0.f;
	if (!pMessageBox->m_bShow)
	{
		pMessageBox->ShowMessage();
	}
}

void UMessageBox::MsgBox(const char* txt,BoxFunction OKFun,BoxFunction CancelFun,float WaitTime, bool pCallOkFunFirst)
{
	UControl* pMsgParent = SYState()->UI->LoadDialogEX(DID_MSGBOX);	
	sm_System->PushDialog(pMsgParent);
	UMessageBox* pMessageBox = (UMessageBox*)pMsgParent->GetChildByID(100);

	uSYMessageData pMSGData = uSYMessageData(txt, OKFun,CancelFun,WaitTime,TRUE,pCallOkFunFirst);
	pMessageBox->m_MsgQueue.push(pMSGData);
	if (!pMessageBox->m_bShow)
	{
		pMessageBox->ShowMessage();
	}
	
}

void UMessageBox::PopMsgBox()
{
	UControl* pMsgParent = SYState()->UI->LoadDialogEX(DID_MSGBOX);	
    if(pMsgParent)
    {
	    UMessageBox* pMessageBox = (UMessageBox*)pMsgParent->GetChildByID(100);
        if(pMessageBox)
            pMessageBox->Pop();
    }
}

void UMessageBox::HideOKButton()
{
    UControl* pMsgParent = SYState()->UI->LoadDialogEX(DID_MSGBOX);	
    if(pMsgParent)
    {
        UMessageBox* pMessageBox = (UMessageBox*)pMsgParent->GetChildByID(100);
        if(pMessageBox)
        {
            UControl* pOKButton = pMessageBox->GetChildByID(1);
            if(pOKButton)
                pOKButton->SetVisible(false);
        }
    }
}

void UMessageBox::DelayShow(float fTime)
{
    UControl* pMsgParent = SYState()->UI->LoadDialogEX(DID_MSGBOX);	
    if(pMsgParent)
    {
        UMessageBox* pMessageBox = (UMessageBox*)pMsgParent->GetChildByID(100);
        if(pMessageBox)
        {
            pMessageBox->m_fDelayShowTime = fTime;
            pMessageBox->SetVisible(false);
        }
    }
}

void UMessageBox::DisableEscape()
{
    UControl* pMsgParent = SYState()->UI->LoadDialogEX(DID_MSGBOX);	
    if(pMsgParent)
    {
        UMessageBox* pMessageBox = (UMessageBox*)pMsgParent->GetChildByID(100);
        if(pMessageBox)
            pMessageBox->m_bDisableEscape = true;
    }
}




BOOL UMessageBox::OnEscape()
{
    if(m_bDisableEscape)
        return TRUE;

	if (!UControl::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}

void UMessageBox::OnTimer(float fDeltaTime)
{
	UDialog::OnTimer(fDeltaTime);
	if (m_WaitTime > 0.0f)
	{
		//计算等待时间
		m_WaitTime -= fDeltaTime ;
		if (m_WaitTime <= 0.0f)
		{
			//如果有m_CancelFun的函数，默认优先调用取消函数 
			//如果没，则m_OKFun函数。 调用之！

			if (m_CallOKFunFirst || !m_CancelFun)
			{
				OnClickOK();
			}else
			{
				OnClickCancel();	
			}
			
			m_CallOKFunFirst = false;
			m_WaitTime = 0.0f;
		}
	}

    if(m_fDelayShowTime > 0.0f)
    {
        m_fDelayShowTime -= fDeltaTime;
        if(m_fDelayShowTime < 0.f)
        {
            m_fDelayShowTime = 0.f;
            SetVisible(true);
        }
    }
}


//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UMessageTxt, UStaticText);
void UMessageTxt::StaticInit()
{

}
UMessageTxt::UMessageTxt()
{
	m_WaitTime = 0.0f;
	m_bEnableWait = FALSE;
}
UMessageTxt::~UMessageTxt()
{

}
void UMessageTxt::OnTimer(float fDeltaTime)
{
	UStaticText::OnTimer(fDeltaTime);
	
}

void UMessageTxt::OnRender(const UPoint& offset,const URect &updateRect)
{
	UControl::OnRender(offset,updateRect);
	if (m_Font && sm_UiRender && m_WStrBuffer.GetLength())
	{
		UDrawStaticText(sm_UiRender,m_Font, offset, updateRect.GetWidth(), m_FontColor, m_WStrBuffer, (EUTextAlignment)m_TextAlign);
		//UDrawText(sm_UiRender,m_Font,offset,updateRect.GetSize(),m_FontColor,m_WStrBuffer,(EUTextAlignment)m_TextAlign);
	}
}

//////////////////////////////////////////////////////////////////////////



UIMP_CLASS(UMessageTime, UControl);
void UMessageTime::StaticInit()
{

}
UMessageTime::UMessageTime()
{
	m_WaitTime = 0.0f;
}
UMessageTime::~UMessageTime()
{

}

BOOL UMessageTime::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	
	m_Numberskin = sm_System->LoadSkin("MSGBOX\\number.skin");
	if (m_Numberskin == NULL)
	{
		return FALSE;
	}

	m_WaitTime = 0.f ;
	return TRUE ;
}


void UMessageTime::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);
	if (m_WaitTime > 0.0f)
	{
		m_WaitTime -= fDeltaTime;

		if (m_WaitTime <= 0.0f)
		{
			m_WaitTime = 0.0f;
		}
	}
}
void UMessageTime::SetWaitTime(float time)
{
	m_WaitTime = time ;
}
void UMessageTime::OnRender(const UPoint& offset,const URect &updateRect)
{
	UControl::OnRender(offset, updateRect);

	if (!m_Numberskin || m_WaitTime == 0.0f)
	{
		return ;
	}
	UINT Time = (UINT)m_WaitTime ;
	
	char buf[_MAX_PATH];
	sprintf(buf, "%d", Time);
	int len = strlen(buf);
	
	int Hight = updateRect.GetHeight();
	UPoint DrawNumSize = UPoint(Hight, Hight);
	int Width = updateRect.GetWidth();
	if (len * Hight > Width)
	{
		DrawNumSize.x = Width / len ;
		DrawNumSize.y = Width / len ;
	}
	
	UINT left = (Width - len * DrawNumSize.x) / 2;
	char tem ;
	for (int i =0; i < len; i++)
	{
		tem = buf[i];
		URect DrawRec ;
		DrawRec.SetPosition(offset.x + left, offset.y);
		DrawRec.SetSize(DrawNumSize);
		sm_UiRender->DrawSkin(m_Numberskin, tem - '0', DrawRec,0xffe6a456);
		left += DrawNumSize.x ;
	}
}