#ifndef __UMESSAGEBOX_H__
#define __UMESSAGEBOX_H__
#include "UInc.h"
#include <queue>

typedef void* (BoxFunction)();
class UBkgControl : public UControl
{
	UDEC_CLASS(UBkgControl);
public:
	virtual BOOL HitTest(const UPoint& parentCoordPoint){
		for (unsigned int i = 0 ; i < m_Childs.size() ; i++)
		{
			if (m_Childs[i]->GetControlRect().PointInRect(parentCoordPoint))
			{
				return TRUE;
			}
		}
		return FALSE;
	};
};

class UMessageTime : public UControl
{
	UDEC_CLASS(UMessageTime);
public:
	UMessageTime();
	~UMessageTime();
	void SetWaitTime(float time);
	virtual BOOL OnCreate();
protected:
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
private:
	USkinPtr m_Numberskin;
	float m_WaitTime;
};


class UMessageTxt : public UStaticText
{
	UDEC_CLASS(UMessageTxt);
public:
	UMessageTxt();
	~UMessageTxt();
protected:
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
protected:
	float m_WaitTime ;
	BOOL m_bEnableWait; 
};

struct	uSYMessageData
{
	uSYMessageData(const char* pTxt, BoxFunction OKFun = NULL,BoxFunction CancelFun = NULL,float WaitTime = 0.0f,BOOL bShowCancel = TRUE, bool CallOkFunFirst = FALSE)
	{
		pMessageTxt.Set(pTxt);
		pOKFunction = OKFun;
		pCancelFunction = CancelFun;
		pWaitTime = WaitTime ;
		pShowCancel = bShowCancel;
		pCallOkFunFirst = CallOkFunFirst;
	}
	UStringW pMessageTxt;
	BoxFunction* pOKFunction;
	BoxFunction* pCancelFunction;
	float pWaitTime;
	BOOL pShowCancel ;
	bool pCallOkFunFirst; //时间到了后是否优先CALL OKFUN.

	inline BOOL operator==(const uSYMessageData& pMessageData) const
	{
		if (_wcsicmp(pMessageData.pMessageTxt.GetBuffer(), pMessageTxt.GetBuffer()) == 0)
		{
			
			if (pOKFunction == pMessageData.pOKFunction && pCancelFunction == pMessageData.pCancelFunction)
			{
				return TRUE;
			}
		}
		return FALSE;
	}
};

class UMessageBox : public UDialog
{
	UDEC_CLASS(UMessageBox);
	UDEC_MESSAGEMAP();
public:
	UMessageBox();
	virtual ~UMessageBox();

	static void MsgBox(const char* txt,BoxFunction OKFun = NULL,BoxFunction CancelFun = NULL,float WaitTime = 0.0f, bool pCallOkFunFirst = false);
	static void MsgBox_s(const char* txt,BoxFunction OKFun = NULL,float WaitTime = 0.0f);
    static void PopMsgBox();
    static void HideOKButton();
    static void DelayShow(float fTime);
    static void DisableEscape();

	void ShowMessage();
    void Pop();

	void OnClickOK();
	void OnClickCancel();
	void OnShowDagText(const char* txt , float WaitTime = 0.0f);
	void OnShowDagText(const WCHAR* txt, float WaitTime = 0.0f);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();

	virtual void OnClose();
	virtual BOOL OnEscape();
	virtual void OnTimer(float fDeltaTime);

private:
	UBitmapButton*  m_bOKBtn;
	UBitmapButton*  m_bCancelBtn;
	UMessageTxt*  m_bShowText;
	UMessageTime* m_pkShowTime;

	BoxFunction*  m_OKFun;
	BoxFunction*  m_CancelFun;

	float m_WaitTime ;
	bool  m_CallOKFunFirst;   //如果有等待时间的话， 时间到了后是否优先调用OK FUN. 。。。。默认是false 
	BOOL m_bShow;  //是否已经在使用

	queue<uSYMessageData> m_MsgQueue;  //消息队列
    float m_fDelayShowTime;
    bool m_bDisableEscape;

};

#endif

