#include "StdAfx.h"
#include "UNewBirdHelp.h"
#include "UInGameBar.h"
#include "UIGamePlay.h"
#include "Utils/NewBirdDB.h"
#include "ObjectManager.h"
#include "Map/PathNode.h"
#include "Map/Map.h"
#include "Utils/MapToMapPathInfo.h"
#include "ui/UIMiniMap.h"
#include "UFun.h"


UIMP_CLASS(UNewBirdHelp,UControl);
UBEGIN_MESSAGE_MAP(UNewBirdHelp, UControl)
UON_BN_CLICKED(0,&UNewBirdHelp::OnClickType)
UON_BN_CLICKED(1,&UNewBirdHelp::OnClickType)
UON_BN_CLICKED(2,&UNewBirdHelp::OnClickType)
UON_BN_CLICKED(3,&UNewBirdHelp::OnClickType)
UON_BN_CLICKED(4,&UNewBirdHelp::OnClickType)
UON_BN_CLICKED(5,&UNewBirdHelp::OnClickType)
UON_BN_CLICKED(6,&UNewBirdHelp::OnClickType)
UON_UGD_CLICKED(8,&UNewBirdHelp::OnClickList)
UON_BN_CLICKED(0xffbb,&UNewBirdHelp::Close)
UON_RED_URL_CLICKED(9, &UNewBirdHelp::OnURLClicked)
UEND_MESSAGE_MAP()

void UNewBirdHelp::StaticInit()
{
	UREG_PROPERTY("bkgskin", UPT_STRING, UFIELD_OFFSET(UNewBirdHelp, m_strFileName));
}
UNewBirdHelp::UNewBirdHelp():m_spBkgSkin(NULL),m_Category(0),m_CloseBtn(NULL),m_pListBox(NULL),m_pRichTextCtrl(NULL)
{
}
UNewBirdHelp::~UNewBirdHelp()
{

}
BOOL UNewBirdHelp::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_strFileName.GetBuffer())
	{
		m_spBkgSkin = sm_System->LoadSkin(m_strFileName.GetBuffer());
		if (m_spBkgSkin == NULL)
		{
			return FALSE;
		}
	}
	USkin* pSkin = NULL;
	if (m_CloseBtn == NULL)
	{
		m_CloseBtn = (UBitmapButton*)UBitmapButton::CreateObject();

		if (m_CloseBtn)
		{
			m_CloseBtn->SetField("style", "UButton");
			m_CloseBtn->SetField("bitmap", "Public\\Close.skin");
			m_CloseBtn->SetId(0xffbb);
			m_CloseBtn->SetVisible(TRUE);
			AddChild(m_CloseBtn);
			pSkin = m_CloseBtn->GetSkin();
			if (m_CloseBtn)
			{
				URect rect(742,56,742+18,56+18);
				m_CloseBtn->SetWindowRect(rect);
			}
		}
		else
		{
			return FALSE;
		}
	}

	

	
	UScrollBar* pkScrollBar = UDynamicCast(UScrollBar, GetChildByID(7));
	if(pkScrollBar == NULL)
		return FALSE;

	pkScrollBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);
	pkScrollBar->ReCalLayout();


	if (m_pListBox == NULL)
	{
		m_pListBox = UDynamicCast(UListBox, GetChildByID(8));
		if (!m_pListBox)
		{
			return FALSE;
		}
	}
	m_SlipRect.Set(229,131,3,538);
	if(m_pRichTextCtrl == NULL)
	{
		m_pRichTextCtrl = UDynamicCast(URichTextCtrl, GetChildByID(7)->GetChildByID(9));
		if (!m_pRichTextCtrl)
		{
			return FALSE;
		}
	}
	UBitmapButton* pbtn = UDynamicCast(UBitmapButton, GetChildByID(m_Category));
	if (pbtn)
	{
		pbtn->SetCheck(FALSE);
	}
	return TRUE;
}
void UNewBirdHelp::OnDestroy()
{
	m_Category = 0;
	return UControl::OnDestroy();
}
void UNewBirdHelp::SetVisible(BOOL value)
{
	if (value)
	{
		if (!m_Style->mSoundOpen.Empty())
		{
			sm_System->__PlaySound(m_Style->mSoundOpen.GetBuffer());
		}
		UControl* parent = GetParent();
		if (parent)
		{
			parent->MoveChildTo(this, NULL);
		}
		UControl* pFocus = FindFirstFocus();
		SetFocusControl(pFocus);
	}
	else
	{
		if (!m_Style->mSoundClose.Empty())
		{
			sm_System->__PlaySound(m_Style->mSoundClose.GetBuffer());
		}
	}
	UControl::SetVisible(value);
}
#define COLOR_Fill	0xFF6C5757
void UNewBirdHelp::OnRender(const UPoint& offset, const URect &updateRect)
{
	if (m_spBkgSkin)
	{
		sm_UiRender->DrawSkin(m_spBkgSkin, 0, updateRect);
	}
	URect rect = m_SlipRect;
	//rect.TransformRect(UPoint(1024,768), GetRoot()->GetWindowSize());
	rect.Offset(offset);
	sm_UiRender->DrawRectFill(rect, COLOR_Fill);
	RenderChildWindow(offset, updateRect);
}
#undef COLOR_Fill

void UNewBirdHelp::OnClickType()
{
	for (int i = 0 ; i < 7; i++)
	{
		UBitmapButton* pbtn = UDynamicCast(UBitmapButton, GetChildByID(i));
		if (pbtn->IsChecked())
		{
			m_Category = i;
			SetCurrentType();
			return;
		}
	}
}
void UNewBirdHelp::OnClickList()
{
	int id = m_pListBox->GetCurSel();
	if (id >= 0)
	{
		std::string desc;
		
		if (GetDesc(m_Currenttemp[id].Id, desc) && m_pRichTextCtrl)
		{
			m_pRichTextCtrl->ClearText();
			if (desc.length())
			{
				m_pRichTextCtrl->AppendText(desc.c_str(), strlen(desc.c_str()), 1);
			}
			UScrollBar* pkScrollBar = UDynamicCast(UScrollBar, GetChildByID(7));
			if (pkScrollBar)
			{
				//pkScrollBar->ReCalLayout();
				//pkScrollBar->scrollTo(0, 0);
			}
		}
	}
}
void UNewBirdHelp::Close()
{
	UInGame* pInGame = UInGame::Get();
	if (pInGame)
	{
		UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
		if (pGameBar)
		{
			UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONHELP);
			if (pBtn)
			{
				pBtn->SetCheck(TRUE);
			}
		}
	}
}
BOOL UNewBirdHelp::OnEscape()
{
	if (!UControl::OnEscape())
	{
		Close();
	}
	return TRUE;
}
void UNewBirdHelp::SetCurrentType()
{
	m_pListBox->Clear();
	m_Currenttemp.clear();
	int index = 0;
	m_pRichTextCtrl->ClearText();
	if(g_pkNewBirdCategroyDB->GetCategroyContent(m_Category, m_Currenttemp))
	{
		std::vector<SubCategroyStruct>::iterator it = m_Currenttemp.begin();
		while (it != m_Currenttemp.end())
		{
			index++;
			m_pListBox->AddItem(it->title.c_str());
			++it;
		}
	}
	if (m_pListBox->GetItemCount())
	{
		m_pListBox->SetCurSel(0);
	}
	m_Category;
}

BOOL UNewBirdHelp::GetDesc(uint16 Id, std::string &str)
{
    str.clear();

	char buf[1024];
	sprintf(buf, "DATA\\UI\\NewBird\\New_Bird_%d.txt", Id);
    NiFile* pkFile = NiFile::GetFile(buf, NiFile::READ_ONLY);
    if(!pkFile || !(*pkFile))
    {
        NiDelete pkFile;
        return FALSE;
    }

    if(pkFile->GetFileSize())
    {
		string tem ;
       tem.resize(pkFile->GetFileSize() + 1);
       unsigned int uiSize = pkFile->Read(&tem[0],  pkFile->GetFileSize());
       tem[uiSize] = '\0';

	   str = AnisToUTF8(tem);
    }

    NiDelete pkFile;

	return TRUE;
}

void UNewBirdHelp::OnURLClicked(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;

	// 任务点击寻路
	if(strURL.find("#NPC") != -1 || strURL.find("#MONSTER") != -1)
	{
		int iMapId = 0;
		int iPosX, iPosY, iPosZ;

		NiPoint3 kPos = NiPoint3::ZERO ;

		int iTmp0, iTmp1;
		iTmp0 = strURL.find_first_of(" ");
		iTmp1 = strURL.find(" ", iTmp0 + 1);
		iMapId = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());

		iTmp0 = strURL.find(" ", iTmp1);
		//iTmp0 = strURL.find_first_of(" ");
		iTmp1 = strURL.find(" ", iTmp0 + 1);
		iPosX = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());

		iTmp0 = strURL.find(" ", iTmp1);
		iTmp1 = strURL.find(" ", iTmp0 + 1);
		iPosY = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());

		iTmp0 = strURL.find(" ", iTmp1);
		iTmp1 = strURL.find(" ", iTmp0 + 1);
		iPosZ = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());


		// 检查当前地图ID，如果相同则寻路
	
		CPlayerLocal* pkLcoalPlayer = ObjectMgr->GetLocalPlayer();
		
		kPos = NiPoint3(iPosX, iPosY, iPosZ);
		pkLcoalPlayer->MoveToMapAStar(iMapId, kPos, 0);
		
	}
}