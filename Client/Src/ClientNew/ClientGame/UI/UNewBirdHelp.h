#ifndef __UNEWBIRDHELP_H__
#define __UNEWBIRDHELP_H__
#include "UInc.h"
struct DescStruct
{
	uint16 ID;
	std::string Desc;
};
class UNewBirdHelp : public UControl
{
	UDEC_CLASS(UNewBirdHelp);
	UDEC_MESSAGEMAP();
public:
	UNewBirdHelp();
	~UNewBirdHelp();

	virtual void SetVisible(BOOL value);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual BOOL OnEscape();
	virtual void OnTimer(float fDeltaTime){};

	void OnClickType();
	void OnClickList();
	void Close();

	void SetCurrentType();
	BOOL GetDesc(uint16 Id, std::string &str);

	void OnURLClicked(UNM* pNM);
private:
	USkinPtr m_spBkgSkin;
	UString m_strFileName;
	URect m_SlipRect;
	int m_Category;
	UBitmapButton* m_CloseBtn;
	UListBox* m_pListBox;
	URichTextCtrl* m_pRichTextCtrl;
	std::vector<struct SubCategroyStruct> m_Currenttemp;
	std::vector<DescStruct> m_CurrentDesc;
};
#endif