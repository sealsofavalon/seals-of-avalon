#include "stdafx.h"
#include <windows.h>
#ifndef min 
#define min __min
#endif 
#ifndef max 
#define max __max
#endif 
#include "UNiAVControl.h"
#include <NiAnimation.h>
#include "UISystem.h"
#include "../ObjectManager.h"
#include "../ClientState.h"
#include "../Utils/ClientUtils.h"
#include "Effect/EffectManager.h"

#include "ClientApp.h"
#include "ResourceManager.h"
#include "ItemManager.h"
#include "UFun.h"

UIMP_CLASS(UNiAVControl,UControl);
void UNiAVControl::StaticInit()
{
	UREG_PROPERTY("avobj", UPT_STRING, UFIELD_OFFSET(UNiAVControl,m_strFileName));
}

void UNiAVControl::DeleteObject()
{
	NiDelete this;
}

UNiAVControl::UNiAVControl(void)
    :m_kVisible(32, 16)
{
	m_Active = TRUE;		// 设置为真, 用来接收OnTimer 消息

	m_fTime = 0.0f;

}

UNiAVControl::~UNiAVControl(void)
{
}

BOOL UNiAVControl::SetObjectFile(const char* strFile)
{
	NiAVObject* pkAVObject = g_ResMgr->LoadNif(strFile);
	g_ResMgr->FreeNif(m_spAVObject);

	m_spAVObject = NiNew NiNode;
	((NiNode*)(NiAVObject*)m_spAVObject)->AttachChild( pkAVObject );
	RecursiveSetCycleType(m_spAVObject, NiTimeController::LOOP);
	m_VisGeometries.RemoveAll();
	CollectRendElement(m_spAVObject);

	m_spAVObject->Update(0.0f, false);
	m_spAVObject->UpdateProperties();
	m_spAVObject->UpdateEffects();

	NiCamera* pCamNode = FindCamera(m_spAVObject);
	if (pCamNode == NULL)
	{
		UTRACE("can't find camera.");
		return FALSE;
	}

	m_spCamera = pCamNode;
	m_spCamera->Update(0.0f);
	m_spCamera->FitNearAndFarToBound(m_spAVObject->GetWorldBound());

	return TRUE;
}
void UNiAVControl::PushNiState()
{
	SYState()->UI->PushRenderState();
}

void UNiAVControl::PopNiState()
{
	SYState()->UI->PopRenderState();
}

NiCamera* UNiAVControl::FindCamera(NiAVObject* pAVObj)
{
	if (NiIsKindOf(NiCamera, pAVObj))
	{
		return (NiCamera*) pAVObj;
	}
	else if (NiIsKindOf(NiNode, pAVObj))
	{
		NiNode* pkNode = (NiNode*) pAVObj;
		for (unsigned int ui = 0; ui < pkNode->GetArrayCount(); ui++)
		{
			NiCamera* pkFoundCamera = FindCamera(pkNode->GetAt(ui));
			if (pkFoundCamera)
				return pkFoundCamera;
		}
	}
	return NULL;

}

void UNiAVControl::CollectRendElement(NiAVObject* pObject)
{
	if(NiIsKindOf(NiNode, pObject))
	{
		NiNode* pkNode = (NiNode*) pObject;

		for (unsigned int ui = 0; ui < pkNode->GetArrayCount(); ui++)
		{
			NiAVObject* pChild = pkNode->GetAt(ui);
			if(pChild && !pChild->GetAppCulled())
				CollectRendElement(pChild);
		}
	}else if (NiIsKindOf(NiGeometry, pObject))
	{
		NiGeometry* pGeom = (NiGeometry*)pObject;

		if (pGeom->GetExtraData("LOD geometry") != 0)
		{
			_asm nop
		}

		NIASSERT(!pGeom->GetAppCulled())
		if (pGeom && !pGeom->GetAppCulled())
			m_VisGeometries.Add(*pGeom);
		else
			NIASSERT(0);
	}
}

BOOL UNiAVControl::OnCreate()
{
	NIASSERT(SYState()->ClientApp);
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	
	NiRenderer* pRender = SYState()->Render;
	if(pRender == NULL)
		return FALSE;

	if (pRender->GetSorter() == NULL)
	{
		 NiAlphaAccumulator* pkAccum = NiNew NiAlphaAccumulator;
		pRender->SetSorter(pkAccum);
	}

	m_VisGeometries.RemoveAll();
	if (m_strFileName.Empty())
	{
		return FALSE;
	}

	NiAVObject* pkAVObject = g_ResMgr->LoadNif(m_strFileName.GetBuffer());
	g_ResMgr->FreeNif(m_spAVObject);

	m_spAVObject = NiNew NiNode;
	((NiNode*)(NiAVObject*)m_spAVObject)->AttachChild( pkAVObject );
	RecursiveSetCycleType(m_spAVObject, NiTimeController::LOOP);

	CollectRendElement(m_spAVObject);

	if (m_VisGeometries.GetCount() == 0)
	{
		UTRACE("can't find any elment to render.");
		return FALSE;
	}

	m_spDirLight = NiNew NiDirectionalLight;
	m_spDirLight->SetDiffuseColor(NiColor::WHITE * 0.5f);
	m_spDirLight->SetAmbientColor(NiColor::WHITE * 0.5f);

	NiMatrix3 kMat;
	kMat.MakeIdentity();
	NiPoint3 kDir(1.f, 1.f, -1.f);
	kDir.Unitize();
	kMat.SetCol(0, kDir);
	m_spDirLight->SetRotate(kMat);
	m_spDirLight->Update(0.f);

	m_spAVObject->Update(0.0f, false);
	m_spAVObject->UpdateProperties();
	m_spAVObject->UpdateEffects();

	NiCamera* pCamNode = FindCamera(m_spAVObject);
	if (pCamNode == NULL)
	{
		UTRACE("can't find camera.");
		return FALSE;
	}

	m_spCamera = pCamNode;
	m_spCamera->Update(0.0f);
	m_spCamera->FitNearAndFarToBound(m_spAVObject->GetWorldBound());

	return TRUE;
}

void UNiAVControl::OnDestroy()
{
	m_spCamera = NULL;

	g_ResMgr->FreeNif(m_spAVObject);
	m_spAVObject = NULL;

	m_spDirLight = NULL;
	m_VisGeometries.RemoveAll();
	
	UControl::OnDestroy();
}

void UNiAVControl::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);

	if (m_spAVObject)
	{
		float fCurTime = SYState()->ClientApp->GetAccumTime();
		m_spAVObject->Update(fCurTime);
	}

	
}

void UNiAVControl::OnSize(const UPoint& NewSize)
{
	UControl::OnSize(NewSize);


}

void UNiAVControl::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_spAVObject == NULL || m_VisGeometries.GetCount() == 0)
	{
		return;
	}
	sm_UiRender->EndRender(); // flush our ui render state
	PopNiState();		// restor the nirender state.

	RenderObject();

	PushNiState();     // save the nirender state
	sm_UiRender->BeginRender(); // set our ui render state
}

void UNiAVControl::RenderObject()
{
	NiRenderer* pRender = SYState()->Render;


	//Add By Hays!!

	float fFov = ClientState->GetCamFOV();
	float fFar = ClientState->GetCamFarDist();
	float fNear = ClientState->GetCamNearDist();

	float fAspectRatio = 1.0f;
	NiRenderer* Render = SYState()->Render;
	if (Render)
	{
		Ni2DBuffer* pkBackbuffer = Render->GetDefaultBackBuffer();
		fAspectRatio = (float)pkBackbuffer->GetWidth() / 
			(float)pkBackbuffer->GetHeight();
	}
	else
	{
		NiAppWindow* pWindow = SYState()->ClientApp->GetAppWindow();
		NIASSERT(pWindow);
		fAspectRatio = (float)pWindow->GetWidth() / (float)pWindow->GetHeight();
	}

	// Setup the camera frustum and viewport
	float fVerticalFieldOfViewRad = NI_PI / 180.0f * fFov;
	float fViewPlaneHalfHeight = tanf(fVerticalFieldOfViewRad * 0.5f);
	float fViewPlaneHalfWidth = fViewPlaneHalfHeight * fAspectRatio;

	NiFrustum kFrustum = NiFrustum(
		-fViewPlaneHalfWidth, fViewPlaneHalfWidth, 
		fViewPlaneHalfHeight, -fViewPlaneHalfHeight,
		fNear, fFar);
	NiRect<float> kPort(0.0f, 1.0f, 1.0f, 0.0f);
	m_spCamera->SetViewFrustum(kFrustum);
	m_spCamera->SetViewPort(kPort);

	//End By Hays

	pRender->SetCameraData(m_spCamera);
	// NiAccumulatorPtr spSorter = pRender->GetSorter();
 //   if (spSorter)
	//{
	//	 spSorter->StartAccumulating(m_spCamera);
	//	 spSorter->RegisterObjectArray(m_VisGeometries);
	//	 spSorter->FinishAccumulating();
	//}else
	//{
	//	unsigned int uiQuantity = m_VisGeometries.GetCount();
	//	unsigned int i;
 //       
 //       for (i = 0; i < uiQuantity; i++)
 //           m_VisGeometries.GetAt(i).RenderImmediate(pRender);
	//}

    m_kVisible.RemoveAll();
	NiCullingProcess kCuller(&m_kVisible);

	NiDrawScene(m_spCamera, m_spAVObject, kCuller);
	
}


#include "../Utils/DefaultDisplayInfoDB.h"

UIMP_CLASS(UNiAVControlR , UNiAVControl);
void UNiAVControlR::StaticInit()
{

}

UNiAVControlR::UNiAVControlR()
:m_spCreateRoleNode(NULL)
,m_spChooseRoleNode(NULL)
,m_pkActivePlayer(NULL)
{
	memset(m_pkCreateRoles, 0, sizeof(m_pkCreateRoles));
	m_pkEndCreateAimationTime = 0.0f;
}

UNiAVControlR::~UNiAVControlR()
{

}

void UNiAVControlR::OnDestroy()
{
	m_spCreateRoleNode = NULL;
	m_spChooseRoleNode = NULL;
	m_vPlayer.clear();
	m_vName.clear();
	m_pkActivePlayer = NULL;

	for(unsigned int ui = 0; ui < MAX_ROLE_MODEL; ui++)
	{
		if(m_pkCreateRoles[ui])
		{
           ObjectMgr->RemoveClientPlayer(m_pkCreateRoles[ui]->GetGUID());
			m_pkCreateRoles[ui] = NULL;
		}
	}

	m_spAVObject = NULL;
	UNiAVControl::OnDestroy();
}


BOOL UNiAVControlR::OnCreate()
{
	UNiAVControl::OnCreate();

	m_spChooseRoleNode = NiNew NiNode;
	NIASSERT(m_spChooseRoleNode);

	m_spCreateRoleNode = NiNew NiNode;
	NIASSERT(m_spCreateRoleNode);

	//CreateAllRoles();
	CreateAllPlayers();

	for (int i=0; i<RACE_MAX; i++)
		for (int j=0; j<GENDER_NONE; j++)
		{
			m_arrDisplayid[i][j] = 0;
		}

	for (int i=0; i<CLASS_MAX; i++)
	{
		for(unsigned int ui = 0; ui < EQUIPMENT_SLOT_END - EQUIPMENT_SLOT_START; ui++)
		{
			m_stOccDisplayInfo[i].equipdisplay[ui] = 0;
		}
	}

	//人族
	m_arrDisplayid[RACE_REN][GENDER_MALE] = 100000000;
	m_arrDisplayid[RACE_REN][GENDER_FEMALE] = 200000000;
	//巫族
	m_arrDisplayid[RACE_WU][GENDER_MALE] = 100000002;
	m_arrDisplayid[RACE_WU][GENDER_FEMALE] = 200000002;
	//妖族
	m_arrDisplayid[RACE_YAO][GENDER_MALE] = 100000001;
	m_arrDisplayid[RACE_YAO][GENDER_FEMALE] = 200000001;


	//武修
	m_stOccDisplayInfo[CLASS_WARRIOR].equipdisplay[EQUIPMENT_SLOT_HEAD]			= 100210500;		//头
	m_stOccDisplayInfo[CLASS_WARRIOR].equipdisplay[EQUIPMENT_SLOT_SHOULDERS]	= 100410500;		//肩部
	m_stOccDisplayInfo[CLASS_WARRIOR].equipdisplay[EQUIPMENT_SLOT_CHEST]		= 100510500;		//胸部
	m_stOccDisplayInfo[CLASS_WARRIOR].equipdisplay[EQUIPMENT_SLOT_WAIST]		= 100610500;		//腰带
	m_stOccDisplayInfo[CLASS_WARRIOR].equipdisplay[EQUIPMENT_SLOT_TROUSERS]		= 100710500;		//裤子
	m_stOccDisplayInfo[CLASS_WARRIOR].equipdisplay[EQUIPMENT_SLOT_GLOVES]		= 100310500;		//手套
	m_stOccDisplayInfo[CLASS_WARRIOR].equipdisplay[EQUIPMENT_SLOT_BOOTS]		= 100810500;		//鞋子
	m_stOccDisplayInfo[CLASS_WARRIOR].equipdisplay[EQUIPMENT_SLOT_MAINHAND]		= 101302008;	//主手武器

	//羽箭
	m_stOccDisplayInfo[CLASS_BOW].equipdisplay[EQUIPMENT_SLOT_HEAD]				= 100210503;		//头
	m_stOccDisplayInfo[CLASS_BOW].equipdisplay[EQUIPMENT_SLOT_SHOULDERS]		= 100410503;		//肩部
	m_stOccDisplayInfo[CLASS_BOW].equipdisplay[EQUIPMENT_SLOT_CHEST]			= 100510503;		//胸部
	m_stOccDisplayInfo[CLASS_BOW].equipdisplay[EQUIPMENT_SLOT_WAIST]			= 100610503;		//腰带
	m_stOccDisplayInfo[CLASS_BOW].equipdisplay[EQUIPMENT_SLOT_TROUSERS]			= 100710503;		//裤子
	m_stOccDisplayInfo[CLASS_BOW].equipdisplay[EQUIPMENT_SLOT_GLOVES]			= 100310503;		//手套
	m_stOccDisplayInfo[CLASS_BOW].equipdisplay[EQUIPMENT_SLOT_BOOTS]			= 100810503;		//鞋子
	m_stOccDisplayInfo[CLASS_BOW].equipdisplay[EQUIPMENT_SLOT_MAINHAND]			= 103101008;	//主手武器

	//仙道
	m_stOccDisplayInfo[CLASS_PRIEST].equipdisplay[EQUIPMENT_SLOT_HEAD]			= 100210501;		//头
	m_stOccDisplayInfo[CLASS_PRIEST].equipdisplay[EQUIPMENT_SLOT_SHOULDERS]		= 100410501;		//肩部
	m_stOccDisplayInfo[CLASS_PRIEST].equipdisplay[EQUIPMENT_SLOT_CHEST]			= 100510501;		//胸部
	m_stOccDisplayInfo[CLASS_PRIEST].equipdisplay[EQUIPMENT_SLOT_WAIST]			= 100610501;		//腰带
	m_stOccDisplayInfo[CLASS_PRIEST].equipdisplay[EQUIPMENT_SLOT_TROUSERS]		= 100710501;		//裤子
	m_stOccDisplayInfo[CLASS_PRIEST].equipdisplay[EQUIPMENT_SLOT_GLOVES]		= 100310501;		//手套
	m_stOccDisplayInfo[CLASS_PRIEST].equipdisplay[EQUIPMENT_SLOT_BOOTS]			= 100810501;		//鞋子
	m_stOccDisplayInfo[CLASS_PRIEST].equipdisplay[EQUIPMENT_SLOT_MAINHAND]		= 101901008;	//主手武器


	//真巫
	m_stOccDisplayInfo[CLASS_MAGE].equipdisplay[EQUIPMENT_SLOT_HEAD]			= 100210502;		//头
	m_stOccDisplayInfo[CLASS_MAGE].equipdisplay[EQUIPMENT_SLOT_SHOULDERS]		= 100410502;		//肩部
	m_stOccDisplayInfo[CLASS_MAGE].equipdisplay[EQUIPMENT_SLOT_CHEST]			= 100510502;		//胸部
	m_stOccDisplayInfo[CLASS_MAGE].equipdisplay[EQUIPMENT_SLOT_WAIST]			= 100610502;		//腰带
	m_stOccDisplayInfo[CLASS_MAGE].equipdisplay[EQUIPMENT_SLOT_TROUSERS]		= 100710502;		//裤子
	m_stOccDisplayInfo[CLASS_MAGE].equipdisplay[EQUIPMENT_SLOT_GLOVES]			= 100310502;		//手套
	m_stOccDisplayInfo[CLASS_MAGE].equipdisplay[EQUIPMENT_SLOT_BOOTS]			= 100810502;		//鞋子
	m_stOccDisplayInfo[CLASS_MAGE].equipdisplay[EQUIPMENT_SLOT_MAINHAND]		= 101901008;        //主手武器

	return TRUE;
}

void UNiAVControlR::OnTimer(float fDeltaTime)
{
	UNiAVControl::OnTimer(fDeltaTime);

	if(m_pkActivePlayer)
	{
		m_pkActivePlayer->Update(fDeltaTime);
	}
}


void UNiAVControlR::CreateAllRoles()
{
	int iTotalRoleCount = 6;//g_pkCreateDefaultDisplayInfo->GetDefaultModelCount();
	NIASSERT(iTotalRoleCount != 0);

	//vector<std::string> Vid;	
	//for(int i = 0; i < iTotalRoleCount; i++)
	//{
	//	char pName[_MAX_PATH];
	//	g_pkCreateDefaultDisplayInfo->GetDefaultModelName(i,pName);
	//	Vid.push_back(pName);
	//}

	for(unsigned int i = 0; i < 3; i++)
		for (unsigned int j = 0; j < 2; j++)
	{
		CPlayer* pkPlayer = ObjectMgr->CreateClientPlayer();
		m_pkCreateRoles[i*2 + j] = pkPlayer;

		pkPlayer->GetSceneNode()->SetScale(1.0f);
		pkPlayer->SetPosition(NiPoint3(0.0f, 0.0f, 0.0f));

		if ( i == 0 )
			pkPlayer->GetSceneNode()->SetAppCulled(false); //默认显示
		else
			pkPlayer->GetSceneNode()->SetAppCulled(true);

		char szName[_MAX_PATH];
		NiSprintf(szName, _MAX_PATH, "%d_%d", i+1, j);
		pkPlayer->GetSceneNode()->SetName( szName );

		m_spCreateRoleNode->AttachChild(pkPlayer->GetSceneNode());
		m_spDirLight->AttachAffectedNode(pkPlayer->GetSceneNode());
	}

	m_spCreateRoleNode->UpdateEffects();
	m_spCreateRoleNode->UpdateProperties();
	m_spCreateRoleNode->Update(0.0f);
}

void UNiAVControlR::CreateAllPlayers()
{
	for(unsigned short i = 0; i < ClientState->GetRoleCount(); i++)
	{
		CPlayer* pkPlayer = (CPlayer*)ObjectMgr->GetObject(ClientState->GetRole(i)->guid);
		if(pkPlayer)
		{
			if(i == 0)
				pkPlayer->GetSceneNode()->SetAppCulled(false);
			else
				pkPlayer->GetSceneNode()->SetAppCulled(true);
			AddPlayer(pkPlayer);
		}
	}

	((NiNode*)(NiAVObject*)m_spAVObject)->AttachChild(m_spChooseRoleNode);
	//m_spDirLight->AttachAffectedNode((NiNode*)(NiAVObject*)m_spAVObject);
	m_spAVObject->UpdateEffects();
	m_spAVObject->UpdateProperties();
	m_spAVObject->Update(0.0f, false);
	m_VisGeometries.RemoveAll();
	CollectRendElement(m_spAVObject);
}

void UNiAVControlR::AddPlayer(CPlayer* pkPlayer)
{
	bool bAdd = false;
	for(unsigned int ui = 0; ui < m_vPlayer.size(); ui++)
	{
		if(m_vPlayer[ui] == pkPlayer)
		{
			bAdd = true;
			break;
		}
	}

	if(!bAdd)
	{	
		m_vPlayer.push_back(pkPlayer);
		pkPlayer->GetSceneNode()->SetScale(1.0f);
		pkPlayer->SetPosition(NiPoint3(0.0f, 0.0f, 0.0f));
		m_spChooseRoleNode->AttachChild(pkPlayer->GetSceneNode());
		m_spDirLight->AttachAffectedNode(m_spChooseRoleNode);
		m_spChooseRoleNode->UpdateEffects();
		m_spChooseRoleNode->UpdateProperties();
		m_spChooseRoleNode->Update(0.0f, false);
	}
}

void UNiAVControlR::DeletePlayer(unsigned int uiIndex)
{
	NIASSERT(uiIndex < m_vPlayer.size());

	m_spChooseRoleNode->DetachChild(m_vPlayer[uiIndex]->GetSceneNode());
	ObjectMgr->RemoveObject(m_vPlayer[uiIndex]->GetGUID());
	m_vPlayer.erase(m_vPlayer.begin() + uiIndex);
	m_pkActivePlayer = NULL;
}

void UNiAVControlR::SetChooseRoleShow(unsigned int uiIndex)
{
	//NIASSERT(uiIndex < m_vPlayer.size());
	if( uiIndex >= m_vPlayer.size() )
		return;

	for(unsigned int ui = 0; ui < m_spChooseRoleNode->GetArrayCount(); ui++)
	{
		NiNode* pkNode = (NiNode*)m_spChooseRoleNode->GetAt(ui);
		if(pkNode)
		{
			if(pkNode == m_vPlayer[uiIndex]->GetSceneNode())
			{
				pkNode->SetAppCulled(false);
				pkNode->SetRotate(NiMatrix3::IDENTITY);


			}
			else
			{
				pkNode->SetAppCulled(true);
			}
		}
	}

	NiNode* pkRoot = NiDynamicCast(NiNode, m_spAVObject);
	if(pkRoot)
	{
		pkRoot->DetachChild(m_spChooseRoleNode);
		pkRoot->DetachChild(m_spCreateRoleNode);
		pkRoot->AttachChild(m_spChooseRoleNode);

		if(m_vPlayer.size())
		{
			m_pkActivePlayer = m_vPlayer[uiIndex];
		}
		else
		{
			m_pkActivePlayer = NULL;
		}
	}

	m_spAVObject->UpdateEffects();
	m_spAVObject->UpdateProperties();


	m_VisGeometries.RemoveAll();
	if (m_pkActivePlayer)
	{
		m_pkActivePlayer->SetCurAnimation("landing",1.0f,TRUE,TRUE);
	}
	CollectRendElement(m_spAVObject);
}

void UNiAVControlR::SetCreateRoleShow(int pRace, int pSex, int playClass)
{
	for(unsigned int ui = 0; ui < m_spCreateRoleNode->GetArrayCount(); ui++)
	{
		NiNode* pkNode = (NiNode*)m_spCreateRoleNode->GetAt(ui);
		if(pkNode)
		{
			{
				pkNode->SetAppCulled(true);
			}
		}
	}

	unsigned int uiIndex = 0;
	for(unsigned int ui = 0; ui < m_spCreateRoleNode->GetArrayCount(); ui++)
	{
		NiNode* pkNode = (NiNode*)m_spCreateRoleNode->GetAt(ui);
		if(pkNode)
		{
			char szName[_MAX_PATH];
			NiSprintf(szName, _MAX_PATH, "%d_%d", pRace, pSex);
			if(strcmp(szName, pkNode->GetName()) == 0)
			{
				pkNode->SetAppCulled(false);
				pkNode->SetRotate(NiMatrix3::IDENTITY);
				uiIndex = ui;
				break;
			}
		}
	}

	NiNode* pkRoot = NiDynamicCast(NiNode, m_spAVObject);
	if(pkRoot)
	{
		pkRoot->DetachChild(m_spChooseRoleNode);
		pkRoot->DetachChild(m_spCreateRoleNode);
		pkRoot->AttachChild(m_spCreateRoleNode);

		m_pkActivePlayer = m_pkCreateRoles[uiIndex];
        if(!m_pkActivePlayer->IsActorLoading() && !m_pkActivePlayer->IsActorLoaded()) 
            m_pkActivePlayer->OnCreateFromUI( m_arrDisplayid[pRace][pSex] );

		m_pkActivePlayer->SetRace(pRace);
		m_pkActivePlayer->SetGender(pSex);

		for(unsigned int ui = 0; ui < EQUIPMENT_SLOT_END - EQUIPMENT_SLOT_START; ui++)
		{
			if(m_stOccDisplayInfo[playClass].equipdisplay[ui])
			{
				ui32 nItemID = m_stOccDisplayInfo[playClass].equipdisplay[ui];
				ui32 itemeffect =   PLAYER_VISIBLE_ITEM_1_0 + (ui * 12) + 9 ;
				ui32 effectid = 0;

				GetRefineItemEffect(nItemID, 9, effectid);
				m_pkActivePlayer->SetUInt32Value(itemeffect,effectid);
				
				ItemMgr->OnEquipmentChange(m_pkActivePlayer, nItemID, ui + EQUIPMENT_SLOT_START);
			}
		}

	}
	
	m_VisGeometries.RemoveAll();
	m_pkActivePlayer->SetCurAnimation("stand001",1.0f,TRUE,TRUE);
	
	m_spAVObject->UpdateEffects();
	m_spAVObject->UpdateProperties();
    m_spAVObject->UpdateNodeBound();
	CollectRendElement(m_spAVObject);	
}

void UNiAVControlR::SetRotatePlayer(float fRot)
{
	if(m_pkActivePlayer)
	{
		NiNode* pkNode = m_pkActivePlayer->GetSceneNode();
		if(pkNode)
		{
			NiMatrix3 kMat;
			kMat.MakeZRotation(fRot);
			pkNode->SetRotate(kMat * pkNode->GetRotate());
		}

		NiAVObject* pFace = (NiAVObject*)pkNode->GetObjectByName("Face");

		if ( pFace )
		{
			NiNode* pParent = pFace->GetParent();
			if ( pParent )
				pParent->DetachChild(pFace);

			NiStream kStream;
			if ( kStream.Load("face1.nif") )
			{
				CAvatar kAvatar;
				NiNode* pChild = (NiNode*)kStream.GetObjectAt(0);
				kAvatar.ChangeAvatarSZ(pkNode, pChild, "Face", m_pkActivePlayer->GetLODController());
			}			 
			pParent->Update(0.0f);
		}
	}
}

//////////////////////////////////////////////////////////////////////////


//
UIMP_CLASS(UNiAVControlEff, UNiAVControl);
void UNiAVControlEff::StaticInit()
{

}
UNiAVControlEff::UNiAVControlEff()
{

}
UNiAVControlEff::~UNiAVControlEff()
{

}
BOOL UNiAVControlEff::OnCreate()
{
	if (!UNiAVControl::OnCreate())
	{
		return FALSE;
	}
	return TRUE;
}
void UNiAVControlEff::OnTimer(float fDeltaTime)
{
	UNiAVControl::OnTimer(fDeltaTime);
}
void UNiAVControlEff::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_spAVObject == NULL || m_VisGeometries.GetCount() == 0)
	{
		return;
	}
	NiCamera* pCam = m_spCamera;
	NIASSERT(pCam);

	NiAppWindow* pWindow = NiApplication::ms_pkApplication->GetAppWindow();
	float fWindowWidth =  (float)pWindow->GetWidth();
	float fWindowHeight = (float)pWindow->GetHeight();
	NiRect<float> ViewPort;
	ViewPort.m_left = updateRect.left/fWindowWidth;
	ViewPort.m_top =  1.0f - (updateRect.top)/fWindowHeight;
	ViewPort.m_right = updateRect.right/fWindowWidth ;
	ViewPort.m_bottom = 1.0f - updateRect.bottom/fWindowHeight;

	float fFov =  45.0f;
	float fFar = 300.f;
	float fNear = 1.0f;
	float fAspectRatio = 1.0f;

	fAspectRatio = abs((float)(updateRect.right - updateRect.left) / (float)(updateRect.bottom -  updateRect.top));


	// Setup the camera frustum and view port
	float fVerticalFieldOfViewRad = NI_PI / 180.0f * fFov;
	float fViewPlaneHalfHeight = tanf(fVerticalFieldOfViewRad * 0.5f);
	float fViewPlaneHalfWidth = fViewPlaneHalfHeight * fAspectRatio;

	const NiFrustum kFrustum = NiFrustum(
		-fViewPlaneHalfWidth, fViewPlaneHalfWidth, 
		fViewPlaneHalfHeight, -fViewPlaneHalfHeight,
		fNear, fFar, false);

	pCam->SetViewFrustum(kFrustum);
	pCam->SetViewPort(ViewPort);
	m_kVisible.RemoveAll();
	NiCullingProcess kCuller(&m_kVisible);
	sm_UiRender->EndRender();

	NiD3DRenderer* pRenderer = (NiD3DRenderer*)NiRenderer::GetRenderer();
	NIASSERT(pRenderer);

	PushNiState();
	NiDrawScene(pCam, m_spAVObject, kCuller);
	PopNiState();

	ViewPort.m_left = 0.0f;
	ViewPort.m_right = 1.0f;
	ViewPort.m_bottom = 0.0f;
	ViewPort.m_top = 1.0f;
	pRenderer->SetScreenSpaceCameraData(&ViewPort);
	sm_UiRender->BeginRender();

}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UNiAVControlEffRe, UNiAVControlEff);
void UNiAVControlEffRe::StaticInit()
{

}
UNiAVControlEffRe::UNiAVControlEffRe()
{

}
UNiAVControlEffRe::~UNiAVControlEffRe()
{

}
void UNiAVControlEffRe::OnUpdateOBJ(float fTime)
{
	if (m_spAVObject)
	{
		m_spAVObject->Update(fTime);
		//ULOG("%.2f",fTime);
	}
}
BOOL UNiAVControlEffRe::SetObjectFile(const char* strFile)
{
	NiAVObject* pkAVObject = g_ResMgr->LoadNif(strFile);
	g_ResMgr->FreeNif(m_spAVObject);

	m_spAVObject = NiNew NiNode;
	((NiNode*)(NiAVObject*)m_spAVObject)->AttachChild(pkAVObject);

	RecursiveSetCycleType(m_spAVObject, NiTimeController::CLAMP);
	m_VisGeometries.RemoveAll();
	CollectRendElement(m_spAVObject);

	m_spAVObject->Update(0.0f, false);
	m_spAVObject->UpdateProperties();
	m_spAVObject->UpdateEffects();

	NiCamera* pCamNode = FindCamera(m_spAVObject);
	if (pCamNode == NULL)
	{
		UTRACE("can't find camera.");
		return FALSE;
	}

	m_spCamera = pCamNode;
	m_spCamera->Update(0.0f);
	m_spCamera->FitNearAndFarToBound(m_spAVObject->GetWorldBound());

	return TRUE;
}
void UNiAVControlEffRe::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);
}
