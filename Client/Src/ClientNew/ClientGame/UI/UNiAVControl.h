#ifndef __UNIAVCONTROL_H__
#define __UNIAVCONTROL_H__
#include "UInc.h"
#include <NiApplication.h>
#include "../Player.h"

// NiAVObject Rendering control
class UNiAVControl : public UControl
{
	UDEC_CLASS(UNiAVControl);
public:
	UNiAVControl(void);
	~UNiAVControl(void);

	virtual BOOL SetObjectFile(const char* strFile);
protected:
	void PushNiState();
	void PopNiState();
	NiCamera* FindCamera(NiAVObject* pAVObj);
	virtual void DeleteObject();
	virtual void RenderObject();
	void CollectRendElement(NiAVObject* pObject);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void OnSize(const UPoint& NewSize);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	float m_fTime;
	NiAVObjectPtr m_spAVObject;
	NiCameraPtr m_spCamera;
	NiDirectionalLightPtr m_spDirLight;
	NiVisibleArray m_VisGeometries;
	UString m_strFileName;
    NiVisibleArray m_kVisible;
};
//职业
struct OccDisplayInfo
{
	ui32 equipdisplay[EQUIPMENT_SLOT_END-EQUIPMENT_SLOT_START];
};

#define MAX_ROLE_MODEL 6
class UNiAVControlR : public UNiAVControl
{
	UDEC_CLASS(UNiAVControlR);
public:
	UNiAVControlR();
	virtual ~UNiAVControlR();

	void AddPlayer(CPlayer* pkPlayer);
	void DeletePlayer(unsigned int uiIndex);

	void SetCreateRoleShow(int pRace, int pSex, int playClass);
	void SetChooseRoleShow(unsigned int uiIndex);
	void SetRotatePlayer(float fRot);
    void CreateAllRoles();
    CPlayer* GetActivePlayer() const { return m_pkActivePlayer; }

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();  //这里要释放对应动作的NIF . 
	virtual void OnTimer(float fDeltaTime);

	void CreateAllPlayers();
	NiNodePtr m_spCreateRoleNode;
	NiNodePtr m_spChooseRoleNode;

	ui32 m_arrDisplayid[RACE_MAX][GENDER_NONE];
	OccDisplayInfo	m_stOccDisplayInfo[CLASS_MAX]; //四个职业的装备信息

	CPlayer* m_pkCreateRoles[MAX_ROLE_MODEL];
	CPlayer* m_pkActivePlayer;
	
	std::vector<std::string> m_vName;
	std::vector<CPlayer*> m_vPlayer;

	float m_pkEndCreateAimationTime ;  //获得动作停止的时间，来停止播放特效

	
};

class UNiAVControlEff : public UNiAVControl
{
	UDEC_CLASS(UNiAVControlEff);
public:
	UNiAVControlEff();
	~UNiAVControlEff();
protected:
	virtual BOOL OnCreate();
	void virtual OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
	BOOL virtual HitTest(const UPoint& parentCoordPoint)
	{
		return FALSE;
	}
};

class UNiAVControlEffRe : public UNiAVControlEff
{
	UDEC_CLASS(UNiAVControlEffRe);
public:
	UNiAVControlEffRe();
	~UNiAVControlEffRe();
	virtual BOOL SetObjectFile(const char* strFile);
	void OnUpdateOBJ(float fTime);
protected:
	virtual void OnTimer(float fDeltaTime);
};


#endif