#include "StdAfx.h"
#include "UNpcDlg.h"
#include "../Utils/QuestDB.h"
#include "../Utils/NpcTextDB.h"
#include "../Network/PacketBuilder.h"
#include "Network/NetworkManager.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "UNpcQuestDlg.h"
#include "Player.h"
#include "ObjectManager.h"
#include "UMessageBox.h"

UIMP_CLASS(UNpcDlg, UDialog);
UBEGIN_MESSAGE_MAP(UNpcDlg, UDialog)
UON_RED_URL_CLICKED(3, &UNpcDlg::OnURLClicked)
UON_BN_CLICKED(0, &UNpcDlg::OnClickOK)
UON_BN_CLICKED(1, &UNpcDlg::OnClickCancel)
UEND_MESSAGE_MAP()

UNpcDlg* g_NpcDlg = NULL;
void UNpcDlg::StaticInit()
{

}

UNpcDlg::UNpcDlg()
{
	g_NpcDlg = this;
	m_pkTextCtrl = NULL;
}

UNpcDlg::~UNpcDlg()
{
}

BOOL UNpcDlg::OnCreate()
{
	if(!UDialog::OnCreate())
		return FALSE;


	m_bCanDrag = FALSE;
	UScrollBar* pkScrollBar = (UScrollBar*)GetChildByID(2);
	if(pkScrollBar == NULL)
		return FALSE;

	pkScrollBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);

	m_pkTextCtrl = (URichTextCtrl*)pkScrollBar->GetChildByID(3);
	if(m_pkTextCtrl == NULL)
		return FALSE;

	return TRUE;
}

void UNpcDlg::OnDestroy()
{
	UDialog::OnDestroy();
}

void UNpcDlg::OnClose()
{
	CPlayerLocal::SetCUState(cus_Normal);
	ULOG( _TRAN("NPC对话(cus_Normal)") );
}
void UNpcDlg::SetCUSNormal()
{
	SetVisible(FALSE);
	//m_stMessage.CreatureGuid = 0;
}
void UNpcDlg::Close()
{
	OnClose();
}
BOOL UNpcDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	
	return FALSE;
}
void UNpcDlg::OnTimer(float fDeltaTime)
{
	if(!m_Visible)
		return;
	UDialog::OnTimer(fDeltaTime);

	//如果距离超过10M,自动关闭!
	if (m_stMessage.CreatureGuid != 0 && m_Visible)
	{
		CCreature* pCreature = (CCreature*)ObjectMgr->GetObject(m_stMessage.CreatureGuid);
		CPlayerLocal* pLocalPlayer = ObjectMgr->GetLocalPlayer();

		if (pLocalPlayer == NULL || pCreature == NULL)
		{
			return ;
		}else
		{
			NiPoint3 pCreaturePos = pCreature->GetPosition();
			NiPoint3 pLocalPlayerPos = pLocalPlayer->GetLocalPlayerPos();

			if ((pLocalPlayerPos - pCreaturePos).Length() > MaxLenToNPC)
			{
				OnClose();
			}
		}
	}
}

void UNpcDlg::OnClickOK()
{
	CPlayerLocal::SetCUState(cus_Normal);
	ULOG( _TRAN("NPC对话(cus_Normal)") );
}

void UNpcDlg::OnClickCancel()
{
	OnClose();
}
uint64 g_SetHomeCreatureID = 0;
int g_SetIndex = 0;

void SetHome()
{
	PacketBuilder->SendNpcGossipSelectOptionMsg(g_SetHomeCreatureID, g_SetIndex);
	g_NpcDlg->Close();
}
void CancelSet()
{
}

void UNpcDlg::OnURLClicked(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;
	int iIndex = FindIndex(strURL);
	int id = FindTagNpcOrQuest(strURL);
	if(id == Tag_Close)
	{
		m_pkTextCtrl->SetText("", 0);
		OnClose();
	}
	else if(id == Tag_Back)
	{
	}else if (id == Tag_MailList)
	{
		PacketBuilder->SendRequestMailList();
	}else if (id == Tag_MailSend)
	{
		UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
		if (pInGame)
		{
			UInGame::GetFrame(FRAME_IG_MAILSENDDLG)->SetVisible(TRUE);
		}
	}else if(id == Tag_Npc && id != -1)
	{
		PacketBuilder->SendNpcGossipSelectOptionMsg(m_stMessage.CreatureGuid, iIndex);
		OnClose();
	}else if(id == Tag_SetHome && id != -1)
	{
		g_SetHomeCreatureID = m_stMessage.CreatureGuid;
		g_SetIndex = iIndex;
		UMessageBox::MsgBox( _TRAN("是否将这里设置为您的新家？"), (BoxFunction*)SetHome, (BoxFunction*)CancelSet);
	}else if(id == Tag_Quest && id != -1)
	{
		if(m_stMessage.vQuests[iIndex].stat == QMGR_QUEST_FINISHED || 
			m_stMessage.vQuests[iIndex].stat == QMGR_QUEST_REPEATABLE_FINISHED)
		{
			//UInGame* pkInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
			//UNpcQuestDlg * pNpcQuestDlg = NULL;
			//if (pkInGame)
			//{
			//	pNpcQuestDlg = INGAMEGETFRAME(UNpcQuestDlg,FRAME_IG_NPCQUESTDLG);
			//	if (pNpcQuestDlg)
			//	{
			//		pNpcQuestDlg->SetQuestContent(m_stMessage.CreatureGuid, m_stMessage.vQuests[iIndex].quest_id);
			//		pNpcQuestDlg->SetVisible(TRUE);
			//		//PacketBuilder->SendQuestGiverCompleteQuestMsg(m_stMessage.CreatureGuid,m_stMessage.vQuests[iIndex].quest_id);
			//	}
			//}
			PacketBuilder->SendQuestGiverCompleteQuestMsg(m_stMessage.CreatureGuid,m_stMessage.vQuests[iIndex].quest_id);
			//OnClose();

			//UInGame* pkInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
			//UNpcQuestDlg * pNpcQuestDlg = NULL;
			//if (pkInGame)
			//{
			//	pNpcQuestDlg = INGAMEGETFRAME(UNpcQuestDlg,FRAME_IG_NPCQUESTDLG);
			//	if (pNpcQuestDlg)
			//	{
			//		
			//	}
			//}
		}else if (m_stMessage.vQuests[iIndex].stat == QMGR_QUEST_NOT_AVAILABLE)
		{
			//显示任务详细信息		

		}else if (m_stMessage.vQuests[iIndex].stat == QMGR_QUEST_AVAILABLE || m_stMessage.vQuests[iIndex].stat == QMGR_QUEST_REPEATABLE ||
			m_stMessage.vQuests[iIndex].stat == QMGR_QUEST_NOT_FINISHED)
		{
			UInGame* pkInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
			UNpcQuestDlg * pNpcQuestDlg = NULL;
			if (pkInGame)
			{
				pNpcQuestDlg = INGAMEGETFRAME(UNpcQuestDlg,FRAME_IG_NPCQUESTDLG);
				if (pNpcQuestDlg)
				{
					//pNpcQuestDlg->SetNpcHead(m_stMessage.CreatureGuid);
					if(m_stMessage.vQuests[iIndex].stat == QMGR_QUEST_NOT_FINISHED)
					{
						pNpcQuestDlg->SetNpcHead(m_stMessage.CreatureGuid);
						pNpcQuestDlg->SetDlgByRequestItems(m_stMessage.vQuests[iIndex].quest_id);
						//pNpcQuestDlg->SetVisible(TRUE);
					}
					else
					{
						pNpcQuestDlg->OnQuest(m_stMessage, iIndex);
						//pNpcQuestDlg->SetVisible(TRUE);
					}
				}
			}
		}
	}
}

void UNpcDlg::OnNetmessage(MSG_S2C::stNPC_Gossip_Message& stMessage)
{
	m_stMessage = stMessage;
	SetContentNpcDlg();
	AddContentByQuest();

// 	string str = "<br><img:DATA\\UI\\NpcDlg\\TalkMask.png><linkcolor:000000><a:#quest_npc_colse>";
// 	str.append(_TRAN("再见"));
// 	str += "</a><br>";
//	m_pkTextCtrl->AppendText(str.c_str(), str.length(), TRUE);

	//SetNpcHead();
	CPlayerLocal::SetCUState(cus_NPC);	
	
}
//void UNpcDlg::SetNpcHead()
//{
//	CCreature* pkNPC = (CCreature*)ObjectMgr->GetObject(m_stMessage.CreatureGuid);
//	UBitmap* pkHead = (UBitmap*)GetChildByID(4);
//
//	if (pkNPC)
//	{
//		ui32 guid = pkNPC->GetUInt32Value(OBJECT_FIELD_ENTRY);
//		char strFile[255];
//		
//		if (guid)
//		{
//			NiSprintf(strFile,255,"Data\\UI\\Icon\\Monster\\Icon_%d.png" ,guid);	
//		}
//		if (strFile)
//		{
//			if (pkHead)
//			{
//				pkHead->SetBitMapByStr(strFile);
//				return;
//			}
//		}
//		
//	}
//}
//const string UNpcDlg::QuestStateToString(ui32 uiQuestState)
//{
//	std::string str = "";
//	switch(uiQuestState)
//	{
//	case QMGR_QUEST_NOT_AVAILABLE:
//		str = "任务未激活";
//		break;
//	case QMGR_QUEST_AVAILABLE:
//		str = "任务可接受";
//		break;
//	case QMGR_QUEST_FINISHED:
//		str = "任务可完成";
//		break;
//	case QMGR_QUEST_REPEATABLE_FINISHED:
//		str = "任务已完成";
//		break;
//	}
//
//	return str;
//}

void UNpcDlg::SetContentNpcDlg()
{
	NIASSERT(m_pkTextCtrl);
	m_pkTextCtrl->SetText("", 1);
	// 填写标题
	std::string strText = "";

	if(!g_pkNpcTex->GetNpcText(m_stMessage.TextId, strText))
	{
		if (!g_pkNpcTex->GetNpcText(2, strText))
		{
			return;
		}
	}
	strText = "<br><font:Arial:14><color:000000>" + strText;
	m_pkTextCtrl->SetText(strText.c_str(), strText.length());
	m_pkTextCtrl->FormatContent();

	for(unsigned int ui = 0; ui < m_stMessage.vMenuItems.size(); ++ui)
	{
		char szTxt[1024];
		if( m_stMessage.vMenuItems[ui].Text.compare(_TRAN("将这里设为你的家")) == 0)
		{
			NiSprintf(szTxt, 1024, "<br><img:DATA\\UI\\NpcDlg\\TalkMask.png><linkcolor:000000><a:#H_%d><font:Arial:13>%s</a> ",ui, (m_stMessage.vMenuItems[ui].Text.c_str()));
			m_pkTextCtrl->AppendText(szTxt, strlen(szTxt), TRUE);
			continue;
		}
		NiSprintf(szTxt, 1024, "<br><img:DATA\\UI\\NpcDlg\\TalkMask.png><linkcolor:000000><a:#N_%d><font:Arial:13>%s</a> ",ui, _TRAN(m_stMessage.vMenuItems[ui].Text.c_str(), true));
		m_pkTextCtrl->AppendText(szTxt, strlen(szTxt), TRUE);
	}
}

void UNpcDlg::AddContentByQuest()
{
	for(unsigned int ui = 0; ui < m_stMessage.vQuests.size(); ui++)
	{
		char sTemp[1024];
		//ui
		NiSprintf(sTemp, 1024, "<br><img:DATA\\UI\\NpcDlg\\%s><linkcolor:000000><a:#Q_%d><font:Arial:13>%s (%s)</a>", QuestStateToImage(m_stMessage.vQuests[ui].stat).c_str(),ui,
			/*UTF8ToAnis*/_TRAN((m_stMessage.vQuests[ui].title).c_str()), 
			QuestStateToString(m_stMessage.vQuests[ui].stat).c_str());

		m_pkTextCtrl->AppendText(sTemp, strlen(sTemp), TRUE);
	}
}
const string UNpcDlg::QuestStateToImage(ui32 uiQuestState)
{
	std::string str = "TalkMask.png";
	switch(uiQuestState)
	{
	case QMGR_QUEST_NOT_AVAILABLE:
		str = "UnAativeQ.png";
		break;
	case QMGR_QUEST_AVAILABLE:
	case QMGR_QUEST_REPEATABLE:
		str = "CanQ.png";
		break;
	case QMGR_QUEST_FINISHED:
	case QMGR_QUEST_REPEATABLE_FINISHED:
		str = "FinishQ.png";
		break;
	case QMGR_QUEST_NOT_FINISHED:
		str = "DoQ.png";
		break;
	}

	return str;
}

const string UNpcDlg::QuestStateToString(ui32 uiQuestState)
{
	std::string str = "";
	switch(uiQuestState)
	{
	case QMGR_QUEST_NOT_AVAILABLE:
		str = _TRAN("未激活");
		break;
	case QMGR_QUEST_AVAILABLE:
	case QMGR_QUEST_REPEATABLE:
		str = _TRAN("可接受");
		break;
	case QMGR_QUEST_FINISHED:
		str = _TRAN("可交付");
		break;
	case QMGR_QUEST_REPEATABLE_FINISHED:
		str = _TRAN("可交付");
		break;
	case QMGR_QUEST_NOT_FINISHED:
		str = _TRAN("进行中");
		break;
	default:
		str = _TRAN("已完成");
		break;
	}

	return str;
}

int UNpcDlg::FindTagNpcOrQuest(const string& str)
{
	if (str == "#quest_npc_colse")
	{
		return Tag_Close;
	}

	if (str == "#quest_npc_back")
	{
		return Tag_Back;
	}

	if (str == "#listMail")
	{
		return Tag_MailList;
	}

	if (str == "#sendMail")
	{
		return Tag_MailSend;
	}

	if (str.find("#Q_") >= 0 && str.find("#Q_") < str.length())
	{
		return Tag_Quest;
	}

	if (str.find("#N_") >=0 && str.find("#N_") < str.length())
	{
		return Tag_Npc;
	}

	if (str.find("#R_") >=0 && str.find("#R_") < str.length())
	{
		return Tag_Reward;
	}

	if (str.find("#H_") >=0 && str.find("#H_") < str.length())
	{
		return Tag_SetHome;
	}
	
	
	return Tag_Invlaid;
}

int UNpcDlg::FindIndex(const string& str)
{
	int index = -1;
	string strIndex;

	if (FindTagNpcOrQuest(str) == Tag_Npc 
		|| FindTagNpcOrQuest(str) == Tag_Quest 
		|| FindTagNpcOrQuest(str) == Tag_Reward
		|| FindTagNpcOrQuest(str) == Tag_SetHome
		)
	{
		strIndex = str.substr(3,str.length());  // 标识符为 Q_,N_.从第2为开始就是index;
		index = atoi(strIndex.c_str());
	}

	return index;
}