#ifndef UNPCDLG_H
#define UNPCDLG_H

#include "UInc.h"
#include "../../../../../SDBase/Protocol/S2C_Quest.h"
#include "UQuestListCtrl.h"

class UNpcDlg : public UDialog
{
	UDEC_CLASS(UNpcDlg);
	UDEC_MESSAGEMAP();

public:
	UNpcDlg();
	virtual ~UNpcDlg();

	void OnNetmessage(MSG_S2C::stNPC_Gossip_Message& stMessage);

	enum eTagType
	{
		Tag_Invlaid,	// 非法
		Tag_Back,		// 返回上层		<a:#quest_npc_back>....</a>
		Tag_Close,		// 关闭标签		<a:#quest_npc_colse>....</a>
		Tag_Npc,		// Npc			<a:#N_id>....</a>
		Tag_Quest,		// 任务 Quest	<a:#Q_id>....</a>
		Tag_Reward,		// 奖励 Reward	<a:#R_id>....</a>
		Tag_Lack,		// 缺少的物品	<a:#L_id>....</a>  暂时不考虑,以后是否需要.
		Tag_MailList,   // 邮件列表     <a:#listMail>....</a>
		Tag_MailSend,   // 发送邮件     <a:#sendMail>....</a>
		Tag_SetHome,    // 将这里设为你的家 <a:#H_id>...</a>
	};
	
	void SendClickMessage(const string& str);

	//void SetNpcHead();
	ui64 GetNPCGUID(){return m_stMessage.CreatureGuid;}
	void SetCUSNormal();
	void Close();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
	virtual void OnTimer(float fDeltaTime);

	void OnClickOK();
	void OnClickCancel();

	void OnURLClicked(UNM* pNM);

	void SetContentNpcDlg();
	void AddContentByQuest();

	const string QuestStateToString(ui32 uiQuestState);
	const string QuestStateToImage(ui32 uiQuestState);

	int FindTagNpcOrQuest(const string& str);
	int FindIndex(const string& str);

private:
	URichTextCtrl* m_pkTextCtrl;
	MSG_S2C::stNPC_Gossip_Message m_stMessage;	//对话信息
};

#endif