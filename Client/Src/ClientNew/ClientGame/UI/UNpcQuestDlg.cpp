#include "stdafx.h"
#include "UNpcQuestDlg.h"
#include "../Utils/QuestDB.h"
#include "../Utils/NpcTextDB.h"
#include "../Network/PacketBuilder.h"
#include "Network/NetworkManager.h"
#include "ItemManager.h"
#include "Creature.h"
#include "ObjectManager.h"
#include "UMessageBox.h"
#include "UISystem.h"
#include "UBitmapButtonEx.h"
#include "UFun.h"
#include "UIGamePlay.h"
#include "UITipSystem.h"
#include "AudioInterface.h"
#include "UFittingRoom.h"

#define  TITLECOLOR  "<font:ArialBold:14><color:3f2404>"




UIMP_CLASS(UNpcQuestDlg, UDialog);
UBEGIN_MESSAGE_MAP(UNpcQuestDlg, UDialog)
UON_BN_CLICKED(4, &UNpcQuestDlg::OnClickAccept)
UON_BN_CLICKED(5, &UNpcQuestDlg::OnClickRefuse)
UON_BN_CLICKED(8, &UNpcQuestDlg::OnDoNothing)
UON_BN_CLICKED(9, &UNpcQuestDlg::OnDoNothing)
UON_BN_CLICKED(10, &UNpcQuestDlg::OnDoNothing)
UON_BN_CLICKED(11, &UNpcQuestDlg::OnDoNothing)
UON_BN_CLICKED(12, &UNpcQuestDlg::OnDoNothing)
UON_BN_CLICKED(13, &UNpcQuestDlg::OnDoNothing)
UON_RED_URL_CLICKED(3, &UNpcQuestDlg::OnURLClicked)
UON_RED_URL_MOUSE_MOVE(3, &UNpcQuestDlg::OnURLMove)
UON_RED_URL_OUT_MOUSE(3, &UNpcQuestDlg::OnURLLeave)
UON_RED_URL_CTRLCLICKED(3, &UNpcQuestDlg::OnURLCtrlClick)
UEND_MESSAGE_MAP()

void UNpcQuestDlg::StaticInit()
{

}

UNpcQuestDlg::UNpcQuestDlg()
{
	m_pkTextCtrl = NULL;
	m_iIndex = -1;

	m_guid = 0;
	m_bCanDrag = FALSE;
}

UNpcQuestDlg::~UNpcQuestDlg()
{

}

BOOL UNpcQuestDlg::OnCreate()
{
	if(!UDialog::OnCreate())
		return FALSE;

	m_bCanDrag = FALSE;
	UScrollBar* pkScrollBar = (UScrollBar*)GetChildByID(2);
	if(pkScrollBar == NULL)
		return FALSE;

	pkScrollBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);

	m_pkTextCtrl = (URichTextCtrl*)pkScrollBar->GetChildByID(3);
	if(m_pkTextCtrl == NULL)
		return FALSE;
	m_pkTextCtrl->SetBitMapURLFun(RichTextBitMapURLFun);

	return TRUE;
}
void UNpcQuestDlg::OnTimer(float fDeltaTime)
{
	if(!m_Visible)
		return;
	UDialog::OnTimer(fDeltaTime);

	//如果距离超过10M,自动关闭!
	
	CCreature* pCreature = static_cast<CCreature*>(ObjectMgr->GetObject(m_guid));
	CPlayerLocal* pLocalPlayer = ObjectMgr->GetLocalPlayer();

	if (pLocalPlayer == NULL || pCreature == NULL)
	{
		return ;
	}else
	{
		NiPoint3 pCreaturePos = pCreature->GetPosition();
		NiPoint3 pLocalPlayerPos = pLocalPlayer->GetLocalPlayerPos();

		if ((pLocalPlayerPos - pCreaturePos).Length() > MaxLenToNPC)
		{
			OnClose();
		}
	}
	
}

void UNpcQuestDlg::OnDestroy()
{
	UDialog::OnDestroy();

	m_Details.giver_guid = 0;
	m_Details.quest_id = 0;
}

void UNpcQuestDlg::OnClose()
{
	CPlayerLocal::SetCUState(cus_Normal);
	ULOG( _TRAN("NPC任务相关(cus_Normal)") );
}
void UNpcQuestDlg::SetCUSNormal()
{
	m_guid = 0;
	SetVisible(FALSE);
}
BOOL UNpcQuestDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}



void UNpcQuestDlg::OnClickAccept()
{
	if(m_iIndex != -1)
	{
		PacketBuilder->SendQuestGiverAcceptQuestMsg(m_stMessage.CreatureGuid,m_stMessage.vQuests[m_iIndex].quest_id);
		OnClose();
	}
	else if(m_Details.giver_guid != 0 && m_Details.quest_id != 0)
	{
		PacketBuilder->SendQuestGiverAcceptQuestMsg(m_Details.giver_guid,  m_Details.quest_id);
		OnClose();
	}
}



void UNpcQuestDlg::OnDoNothing()
{

}

void UNpcQuestDlg::SetNpcHead(ui64 guid)
{
	m_guid = guid;
	CPlayerLocal::SetCUState(cus_Quest);
	
}
void UNpcQuestDlg::OnClickRefuse()
{
	OnClose();
}



void UNpcQuestDlg::ClearControl()
{
	m_pkTextCtrl->SetText("", 0);
}

// 任务说明
void UNpcQuestDlg::OnQuest(MSG_S2C::stNPC_Gossip_Message& stMessage, int iIndex)
{	
	ClearControl();

	m_stMessage = stMessage;
	m_iIndex = iIndex;

	CQuestDB::stQuest* pkQuest = g_pkQusetInfo->GetQuestInfo(stMessage.vQuests[iIndex].quest_id);
	if(pkQuest == NULL)
	{
		char szBuf[128];
		NiSprintf(szBuf, 128, "Don't find the task ID as %d，please check the database!", stMessage.vQuests[iIndex].quest_id);
		UMessageBox::MsgBox_s(szBuf);
		return;
	}

	// 任务标题
	char szBuf[2048];
	szBuf[0] = '\0';
	NiSprintf(szBuf, 2048, "<font:ArialBold:14><color:3f2404>%s", g_pkQusetInfo->GetQuestInfo(stMessage.vQuests[iIndex].quest_id)->strTitle.c_str());
	m_pkTextCtrl->SetText(szBuf, strlen(szBuf));

	
	std::string strDetails = g_pkQusetInfo->GetQuestInfo(stMessage.vQuests[iIndex].quest_id)->strDetails.c_str();
	int iNamePos = strDetails.find("<a:#PlayerName></a>");
	if(iNamePos != -1)
	{
		std::string strPlayerName;
		strPlayerName = ObjectMgr->GetLocalPlayer()->GetName();
		std::string str1, str2;
		str1 = strDetails.substr(0, iNamePos);
		str2 = strDetails.substr(iNamePos + 19, strDetails.size() - 19 - iNamePos);
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s%s%s%s<br>",_TRAN("　　"),str1.c_str(), strPlayerName.c_str(), str2.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}
	else
	{
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s%s<br>",_TRAN("　　"), g_pkQusetInfo->GetQuestInfo(stMessage.vQuests[iIndex].quest_id)->strDetails.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}

	NiSprintf(szBuf, 2048, "<br>%s%s" ,TITLECOLOR, _TRAN("任务目标"));
	m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);

	std::string strObjective = _TRAN("　　");
	strObjective += pkQuest->strObjective.c_str();
	iNamePos = strObjective.find("<a:#PlayerName></a>");
	if(iNamePos != -1)
	{
		std::string strPlayerName;
		strPlayerName = ObjectMgr->GetLocalPlayer()->GetName();
		std::string str1, str2;
		str1 = strObjective.substr(0, iNamePos);
		str2 = strObjective.substr(iNamePos + 19, strObjective.size() - 19 - iNamePos);
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s%s%s<br>", str1.c_str(), strPlayerName.c_str(), str2.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}
	else
	{
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s<br>", strObjective.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}

	// 任务奖品
	for(unsigned int uiIndex = 0; uiIndex < 4; uiIndex++)
	{
		if (!pkQuest->uiRewItem[uiIndex])
		{
			continue ;
		}
		if(uiIndex == 0)
		{
			szBuf[0] = '\0';
			NiSprintf(szBuf, 2048, "<br>%s%s<br>",TITLECOLOR, _TRAN("你可以获得"));
			m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
		}

		ItemPrototype_Client* pkItemInfo = ItemMgr->GetItemPropertyFromDataBase(pkQuest->uiRewItem[uiIndex]);
		if(pkItemInfo)
		{
			char buf[256];
			char count[256];
			itoa(pkQuest->uiRewItem[uiIndex], buf, 10);
			itoa(pkQuest->uiRewItemCount[uiIndex], count , 10);

			std::string strItemID = buf ;
			std::string strItemCN = count;

			std::string str = pkItemInfo->C_icon;
			str = "<skin:" + str + ">";
			str = "<a:#_REWITEM_"+ strItemID +"_" + strItemCN +">" + str + "</a> " ;

			m_pkTextCtrl->AppendText(str.c_str(), str.length(), TRUE);
		}
	}


	// 任务可选奖品
	for(unsigned int uiIndex = 0; uiIndex < 6; uiIndex++)
	{
		if (!pkQuest->uiRewChoiceItem[uiIndex])
		{
			continue ;
		}
		if(uiIndex == 0)
		{
			szBuf[0] = '\0';
			NiSprintf(szBuf, 2048, "<br>%s%s<br>",TITLECOLOR, _TRAN("你可以选择"));
			m_pkTextCtrl->AppendText(szBuf, strlen(szBuf),TRUE);
		}
		ItemPrototype_Client* pkItemInfo = ItemMgr->GetItemPropertyFromDataBase(pkQuest->uiRewChoiceItem[uiIndex]);
		if(pkItemInfo)
		{
			char buf[256];
			char count[256];
			itoa(pkQuest->uiRewChoiceItem[uiIndex], buf, 10);
			itoa(pkQuest->uiRewChoiceItemCount[uiIndex], count , 10);

			std::string strItemCN = count;
			std::string strItemID = buf ;
			std::string str = pkItemInfo->C_icon;
			str = "<skin:" + str + ">";
			str = "<a:#_CHOOSEITEMICON_"+ strItemID + "_"+strItemCN +">" + str + "</a> "  ;

			m_pkTextCtrl->AppendText(str.c_str(), str.length(), TRUE);
		}
	}

	if (pkQuest->uiRewHonor)
	{
		std::string strbuff = _TRAN( N_NOTICE_C35, _I2A(pkQuest->uiRewHonor).c_str() );
		//NiSprintf(szBuf, 2048, strbuff.c_str()/*"<br><font:ArialBold:14><color:3f2404>获得荣誉 : <font:Arial:14><color:4d2f07>%d <br>", pkQuest->uiRewHonor*/);
		m_pkTextCtrl->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
	}
	if (pkQuest->uiRewMoney)
	{
		std::string strbuff = _TRAN( N_NOTICE_C36, GetMoneyRichTextStr(pkQuest->uiRewMoney).c_str() );
		//NiSprintf(szBuf, 2048, strbuff.c_str()/*"<br><font:ArialBold:14><color:3f2404>获得金钱 : <font:Arial:14><color:4d2f07> %s <br>",GetMoneyRichTextStr(pkQuest->uiRewMoney).c_str()*/);
		m_pkTextCtrl->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
	}
	if (pkQuest->uiRewXP)
	{
		// 
		ui32 EXP = pkQuest->uiRewXP;
		// 在这个区间的任务的经验奖励是按升级的百分比计算的
		if (pkQuest->id >= 700001 && pkQuest->id <= 799999 && EXP <= 100)
		{
			CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
			if (pkLocal)
			{
				EXP = EXP * pkLocal->GetMaxExp() / 100;  
			}
		}

		std::string strbuff = _TRAN( N_NOTICE_C37, _I2A(EXP).c_str() );
		//NiSprintf(szBuf, 2048, strbuff.c_str()/*"<br><font:ArialBold:14><color:3f2404>获得经验 : <font:Arial:14><color:4d2f07>%d <br>", EXP*/);
		m_pkTextCtrl->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
	}

	((UScrollBar*)GetChildByID(2))->ReCalLayout();
	

	if(stMessage.vQuests[iIndex].stat != QMGR_QUEST_NOT_FINISHED)
		GetChildByID(4)->SetVisible(TRUE);
	else
		GetChildByID(4)->SetVisible(FALSE);
	GetChildByID(5)->SetVisible(TRUE);

	SetNpcHead(m_stMessage.CreatureGuid);
	
}

// 选择奖励
void UNpcQuestDlg::OnNetMessage(MSG_S2C::stQuestGiver_Offer_Reward &stMessage)
{
	ClearControl();
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if(pInGame)
	{
		UNpcQuestDDlg* pNPCQuestDDlg = INGAMEGETFRAME(UNpcQuestDDlg, FRAME_IG_NPCQUESTDDLG);
		pNPCQuestDDlg->OnNetMessage(stMessage);
		pNPCQuestDDlg->SetVisible(TRUE);
	}

}

// 任务完成的提示
void UNpcQuestDlg::OnNetMessage(MSG_S2C::stQuestGiver_Request_Items &stMessage)
{
	
	ClearControl();
	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if(pInGame)
	{
		UNpcQuestDDlg* pNPCQuestDDlg = INGAMEGETFRAME(UNpcQuestDDlg, FRAME_IG_NPCQUESTDDLG);
		pNPCQuestDDlg->OnNetMessage(stMessage);
		pNPCQuestDDlg->SetVisible(TRUE);
	}
}

void UNpcQuestDlg::OnNetMessage(MSG_S2C::stQuestGiver_Quest_Details& stMessage)
{
	ClearControl();

	m_Details = stMessage;
	m_iIndex = -1;

	CQuestDB::stQuest* pkQuest = g_pkQusetInfo->GetQuestInfo(stMessage.quest_id);
	if(pkQuest == NULL)
	{
		char szBuf[128];
		NiSprintf(szBuf, 128, "Don't find the task ID as %d，please check the database!", stMessage.quest_id);
		UMessageBox::MsgBox_s(szBuf);
		return;
	}

	// 任务标题
	char szBuf[2048];
	szBuf[0] = '\0';
	NiSprintf(szBuf, 2048, "<font:ArialBold:14><color:3f2404>%s", g_pkQusetInfo->GetQuestInfo(stMessage.quest_id)->strTitle.c_str());
	m_pkTextCtrl->SetText(szBuf, strlen(szBuf));

	std::string strDetails = g_pkQusetInfo->GetQuestInfo(stMessage.quest_id)->strDetails.c_str();
	int iNamePos = strDetails.find("<a:#PlayerName></a>");
	if(iNamePos != -1)
	{
		std::string strPlayerName;
		strPlayerName = ObjectMgr->GetLocalPlayer()->GetName();
		std::string str1, str2;
		str1 = strDetails.substr(0, iNamePos);
		str2 = strDetails.substr(iNamePos + 19, strDetails.size() - 19 - iNamePos);
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s%s%s%s<br>", _TRAN("　　"),str1.c_str(), strPlayerName.c_str(), str2.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}
	else
	{
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s%s<br>",_TRAN("　　"), g_pkQusetInfo->GetQuestInfo(stMessage.quest_id)->strDetails.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}

	NiSprintf(szBuf, 2048, "<br>%s%s",TITLECOLOR, _TRAN("任务目标"));
	m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);

	std::string strObjective = _TRAN("　 ");
	strObjective += pkQuest->strObjective.c_str();
	iNamePos = strObjective.find("<a:#PlayerName></a>");
	if(iNamePos != -1)
	{
		std::string strPlayerName;
		strPlayerName = ObjectMgr->GetLocalPlayer()->GetName();
		std::string str1, str2;
		str1 = strObjective.substr(0, iNamePos);
		str2 = strObjective.substr(iNamePos + 19, strObjective.size() - 19 - iNamePos);
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s%s%s<br>", str1.c_str(), strPlayerName.c_str(), str2.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}
	else
	{
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s<br>", strObjective.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}

	// 任务奖品
	for(unsigned int uiIndex = 0; uiIndex < 4; uiIndex++)
	{
		if (!pkQuest->uiRewItem[uiIndex])
		{
			continue ;
		}
		if(uiIndex == 0)
		{
			szBuf[0] = '\0';
			NiSprintf(szBuf, 2048, "<br>%s%s<br>",TITLECOLOR, _TRAN("你可以获得"));
			m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
		}

		ItemPrototype_Client* pkItemInfo = ItemMgr->GetItemPropertyFromDataBase(pkQuest->uiRewItem[uiIndex]);
		if(pkItemInfo)
		{
			char buf[256];
			char count[256];
			itoa(pkQuest->uiRewItem[uiIndex], buf, 10);
			itoa(pkQuest->uiRewItemCount[uiIndex], count , 10);

			std::string strItemID = buf ;
			std::string strItemCN = count;

			std::string str = pkItemInfo->C_icon;
			str = "<skin:" + str + ">";
			str = "<a:#_REWITEM_"+ strItemID +"_" + strItemCN +">" + str + "</a> " ;
			
			m_pkTextCtrl->AppendText(str.c_str(), str.length(), TRUE);
		}
	}

	
	// 任务可选奖品
	for(unsigned int uiIndex = 0; uiIndex < 6; uiIndex++)
	{
		if (!pkQuest->uiRewChoiceItem[uiIndex])
		{
			continue ;
		}
		if(uiIndex == 0)
		{
			szBuf[0] = '\0';
			NiSprintf(szBuf, 2048, "<br>%s%s<br>",TITLECOLOR, _TRAN("你可以选择"));
			m_pkTextCtrl->AppendText(szBuf, strlen(szBuf),TRUE);
		}
		ItemPrototype_Client* pkItemInfo = ItemMgr->GetItemPropertyFromDataBase(pkQuest->uiRewChoiceItem[uiIndex]);
		if(pkItemInfo)
		{
			char buf[256];
			char count[256];
			itoa(pkQuest->uiRewChoiceItem[uiIndex], buf, 10);
			itoa(pkQuest->uiRewChoiceItemCount[uiIndex], count , 10);

			std::string strItemCN = count;
			std::string strItemID = buf ;
			std::string str = pkItemInfo->C_icon;
			str = "<skin:" + str + ">";
			str = "<a:#_CHOOSEITEMICON_"+ strItemID + "_"+strItemCN +">" + str + "</a> "  ;
			
			m_pkTextCtrl->AppendText(str.c_str(), str.length(), TRUE);
		}
	}

	((UScrollBar*)GetChildByID(2))->ReCalLayout();
	GetChildByID(4)->SetVisible(TRUE);
	GetChildByID(5)->SetVisible(TRUE);
	SetNpcHead(stMessage.giver_guid);
}

void UNpcQuestDlg::OnURLCtrlClick(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;

	int pos = strURL.find("#_CHOOSEITEMICON_") ;
	int len = strlen("#_CHOOSEITEMICON_");
	if (pos == -1)
	{
		pos = strURL.find("#_REWITEM_");
		len = strlen("#_REWITEM_");
	}

	if (pos != -1)
	{
		string strdata = strURL.substr(pos + len , strURL.length()) ;
		int entryPos = strdata.find("_");
		string entrystr = strdata.substr(0, entryPos);
		ui32 entry = atoi(entrystr.c_str());

		UFittingRoom* pFittingRoom = INGAMEGETFRAME(UFittingRoom, FRAME_IG_FITTINGROOMDLG);
		if(!pFittingRoom)
			return;
		pFittingRoom->FittingEquipment(entry, (ui64)0);
	}
}
void UNpcQuestDlg::OnURLClicked(UNM* pNM)
{
}
void UNpcQuestDlg::OnURLMove(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;

	int pos = strURL.find("#_CHOOSEITEMICON_") ;
	int len = strlen("#_CHOOSEITEMICON_");
	if (pos == -1)
	{
		pos = strURL.find("#_REWITEM_");
		len = strlen("#_REWITEM_");
	}

	if (pos != -1)
	{
		string strdata = strURL.substr(pos + len , strURL.length()) ;

		int entryPos = strdata.find("_");

		string entrystr = strdata.substr(0, entryPos);
		string countstr = strdata.substr(entryPos+1, strdata.length() - entryPos);

		UPoint TipsPos = sm_System->GetCursorPos();
		ui32 entry = atoi(entrystr.c_str());
		ui32 count = atoi(countstr.c_str());
		TipSystem->ShowTip(TIP_TYPE_ITEM, entry, TipsPos,0, count);
	}
}
void UNpcQuestDlg::OnURLLeave()
{
	TipSystem->HideTip();
}
void UNpcQuestDlg::SetDlgByRequestItems(ui32 uiQuestId)
{
	ClearControl();

	UInGame* pInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if(pInGame)
	{
		UNpcQuestDDlg* pNPCQuestDDlg = INGAMEGETFRAME(UNpcQuestDDlg, FRAME_IG_NPCQUESTDDLG);
		pNPCQuestDDlg->SetDlgByRequestItems(uiQuestId);
		pNPCQuestDDlg->SetNpcHead(m_guid);
		pNPCQuestDDlg->SetVisible(TRUE);
	}
}





UIMP_CLASS(UNpcQuestDDlg, UDialog);
UBEGIN_MESSAGE_MAP(UNpcQuestDDlg, UDialog)
UON_BN_CLICKED(0, &UNpcQuestDDlg::OnClickOK)
UON_BN_CLICKED(6, &UNpcQuestDDlg::OnClickQuestComplete)
UON_RED_URL_CLICKED(3, &UNpcQuestDDlg::OnURLClicked)
UON_RED_URL_MOUSE_MOVE(3, &UNpcQuestDDlg::OnURLMove)
UON_RED_URL_OUT_MOUSE(3, &UNpcQuestDDlg::OnURLLeave)
UON_RED_URL_CTRLCLICKED(3, &UNpcQuestDDlg::OnURLCtrlClick)
UEND_MESSAGE_MAP()

void UNpcQuestDDlg::StaticInit()
{

}

UNpcQuestDDlg::UNpcQuestDDlg()
{
	m_pkTextCtrl = NULL;
	m_iRewardEntry = 0;
	m_guid = 0;
	m_bCanDrag = FALSE;
}

UNpcQuestDDlg::~UNpcQuestDDlg()
{

}

BOOL UNpcQuestDDlg::OnCreate()
{
	if(!UDialog::OnCreate())
		return FALSE;

	UScrollBar* pkScrollBar = (UScrollBar*)GetChildByID(2);
	if(pkScrollBar == NULL)
		return FALSE;

	pkScrollBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);

	m_pkTextCtrl = (URichTextCtrl*)pkScrollBar->GetChildByID(3);
	if(m_pkTextCtrl == NULL)
		return FALSE;
	m_pkTextCtrl->SetBitMapURLFun(RichTextBitMapURLFun);

	return TRUE;
}
void UNpcQuestDDlg::OnTimer(float fDeltaTime)
{
	if(IsVisible() == false)
		return;

	UDialog::OnTimer(fDeltaTime);

	//如果距离超过10M,自动关闭!

	CCreature* pCreature = (CCreature*)ObjectMgr->GetObject(m_guid);
	CPlayerLocal* pLocalPlayer = ObjectMgr->GetLocalPlayer();

	if (pLocalPlayer == NULL || pCreature == NULL)
	{
		return ;
	}else
	{
		NiPoint3 pCreaturePos = pCreature->GetPosition();
		NiPoint3 pLocalPlayerPos = pLocalPlayer->GetLocalPlayerPos();

		if ((pLocalPlayerPos - pCreaturePos).Length() > MaxLenToNPC)
		{
			OnClose();
		}
	}

}

void UNpcQuestDDlg::OnDestroy()
{
	UDialog::OnDestroy();
}

void UNpcQuestDDlg::OnClose()
{
	CPlayerLocal::SetCUState(cus_Normal);
	ULOG("NPC任务相关(cus_Normal)");
}
void UNpcQuestDDlg::SetCUSNormal()
{
	m_guid = 0;
	SetVisible(FALSE);
}
BOOL UNpcQuestDDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}

void UNpcQuestDDlg::OnClickOK()
{
	OnClose();
}

void UNpcQuestDDlg::OnDoNothing()
{

}
void UNpcQuestDDlg::SetNpcHead(ui64 guid)
{
	//UBitmap* pkHead = (UBitmap*)GetChildByID(7);

	//CCreature* pkNPC = (CCreature*)ObjectMgr->GetObject(guid);
	m_guid = guid;
	CPlayerLocal::SetCUState(cus_QuestD);
	/*if (pkNPC)
	{
		ui32 id = pkNPC->GetUInt32Value(OBJECT_FIELD_ENTRY);

		char strFile[255];
		if (id)
		{
			NiSprintf(strFile,255,"Data\\UI\\Icon\\Monster\\Icon_%d.png" ,id);	
		}
		if (strFile)
		{
			if (pkHead)
			{
				pkHead->SetBitMapByStr(strFile);
				return;
			}
		}
	}*/
}


void UNpcQuestDDlg::OnClickQuestComplete()
{
	if(m_ChooseReward.vChoice.size() && !m_iRewardEntry)
	{
		UMessageBox::MsgBox_s( _TRAN("请选择奖励物品！！") );
		return;
	}
	
	UINT index = 0;
	for ( ; index < m_ChooseReward.vChoice.size(); index++)
	{
		if (m_ChooseReward.vChoice[index].item_entry == m_iRewardEntry)
		{
			break ;
		}
	}
	PacketBuilder->SendQuestGiverChooseRewardMsg(m_ChooseReward.giver_guid, m_ChooseReward.questid, index);
	m_iRewardEntry = 0;
	OnClose();
}

void UNpcQuestDDlg::ClearControl()
{
	m_pkTextCtrl->SetText("", 0);
	
}

// 选择奖励
void UNpcQuestDDlg::OnNetMessage(MSG_S2C::stQuestGiver_Offer_Reward &stMessage)
{
	m_ChooseReward = stMessage;

	SetDlgByOfferReward();

	GetChildByID(0)->SetVisible(FALSE);
	GetChildByID(6)->SetVisible(TRUE);
	SetNpcHead(stMessage.giver_guid);

}

// 任务完成的提示
void UNpcQuestDDlg::OnNetMessage(MSG_S2C::stQuestGiver_Request_Items &stMessage)
{
	m_LackItem = stMessage;

	SetDlgByRequestItems(m_LackItem.questid);

	GetChildByID(0)->SetVisible(TRUE);

	GetChildByID(6)->SetVisible(FALSE);
	SetNpcHead(stMessage.giver_guid);

}
void UNpcQuestDDlg::OnURLMove(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;

	int pos = strURL.find("#_CHOOSEITEMICON_") ;
	int len = strlen("#_CHOOSEITEMICON_");
	if (pos == -1)
	{
		pos = strURL.find("#_REWITEM_");
		len = strlen("#_REWITEM_");
	}

	if (pos != -1)
	{
		UPoint TipsPos = sm_System->GetCursorPos();
		ui32 entry = atoi(strURL.substr(pos + len , strURL.length()).c_str());
		TipSystem->ShowTip(TIP_TYPE_ITEM, entry, TipsPos);
	}
}
void UNpcQuestDDlg::OnURLLeave()
{
	TipSystem->HideTip();
}
void UNpcQuestDDlg::OnURLClicked(UNM* pNM)
{
	//
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;
	int pos = strURL.find("#_CHOOSEITEMICON_") ;
	int len = strlen("#_CHOOSEITEMICON_");
	/*if (pos == -1)
	{
		pos = strURL.find("#_REWITEM_");
		len = strlen("#_REWITEM_");
	}*/

	if (pos != -1)
	{
		string strdata = strURL.substr(pos + len , strURL.length()) ;

		int entryPos = strdata.find("_");

		string entrystr = strdata.substr(0, entryPos);

		ui32 entry = atoi(entrystr.c_str());

		m_iRewardEntry = entry ;
	}else
	{
		m_pkTextCtrl->ClearHitURL();
	}
	
}
void UNpcQuestDDlg::SetDlgByOfferReward()
{
	ClearControl();

	CQuestDB::stQuest* pkQuest = g_pkQusetInfo->GetQuestInfo(m_ChooseReward.questid);
	if(pkQuest == NULL)
	{
		char szBuf[128];
		szBuf[0] = '\0';
		NiSprintf(szBuf, 128, "Don't find the task ID as %d，please check the database!", m_ChooseReward.questid);
		UMessageBox::MsgBox_s(szBuf);
		return;
	}
	char szBuf[2048];
	szBuf[0] = '\0';
	string strReward = pkQuest->strCompletionText;
	NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s<br><br>", strReward.c_str());
	m_pkTextCtrl->SetText(szBuf, strlen(szBuf));

	NiSprintf(szBuf, 2048, "%s%s<br>",TITLECOLOR, _TRAN("奖励"));
	m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);

	if (pkQuest->uiRewHonor)
	{
		UINT Honor = 0;
		CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
		if (pkLocal)
		{
			Honor = pkLocal->GetUInt32Value(PLAYER_FIELD_HONOR_CURRENCY);
		}
		std::string strbuff = _TRAN( N_NOTICE_C38, _I2A(Honor).c_str(), _I2A( pkQuest->uiReqHonor ).c_str() );
		m_pkTextCtrl->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
	}

	if (pkQuest->uiRewXP)
	{
		// 
		ui32 EXP = pkQuest->uiRewXP;
		if (pkQuest->id >= 700001 && pkQuest->id <= 799999 && EXP <= 100)
		{
			CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
			if (pkLocal)
			{
				EXP = EXP * pkLocal->GetMaxExp() / 100;  
			}
		}

		std::string strbuff = _TRAN( N_NOTICE_C37, _I2A(EXP).c_str() );
		m_pkTextCtrl->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
	}
	if (m_ChooseReward.reward_money != 0)
	{
		char sRewardMoney[_MAX_PATH];
		sRewardMoney[0] = '\0';
		ui32 money = m_ChooseReward.reward_money;
	
		std::string strbuff = _TRAN( N_NOTICE_C36, GetMoneyRichTextStr(money).c_str()  );
		//NiSprintf(sRewardMoney, _MAX_PATH, strbuff.c_str()/*"<font:Arial:14><color:4d2f07>获得:%s<br>", GetMoneyRichTextStr(money).c_str()*/);
		
		m_pkTextCtrl->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
	}

	for(unsigned int ui = 0; ui < m_ChooseReward.vReward.size(); ++ui)
	{
		if(m_ChooseReward.vReward[ui].count == 0)
			continue;

		char sReward[2048];
		if(ui == 0)
		{
			sReward[0] = '\0';
			NiSprintf(sReward, 2048, "%s%s<br>",TITLECOLOR, _TRAN("你可以获得"));
			m_pkTextCtrl->AppendText(sReward, strlen(sReward), TRUE);
		}

		ItemPrototype_Client* pkItemInfo = ItemMgr->GetItemPropertyFromDataBase(m_ChooseReward.vReward[ui].item_entry);
		if(pkItemInfo)
		{
			char buf[256];
			char count[256];

			itoa(m_ChooseReward.vReward[ui].item_entry, buf, 10);
			itoa(m_ChooseReward.vReward[ui].count, count, 10);

			std::string strItemID = buf ;
			std::string strItemCN = count ;

			std::string str = pkItemInfo->C_icon;
			str = "<skin:" + str + ">";
			str = "<a:#_REWITEM_"+ strItemID +"_" + strItemCN + ">" + str + "</a> " ;
			

			m_pkTextCtrl->AppendText(str.c_str(), str.length(), TRUE);
		}
	}
	for(unsigned int ui = 0; ui < m_ChooseReward.vChoice.size(); ++ui)
	{
		if(m_ChooseReward.vChoice[ui].count == 0)
			continue;

		char sChoice[2048];
		ItemPrototype_Client* pkItemInfo = ItemMgr->GetItemPropertyFromDataBase(m_ChooseReward.vChoice[ui].item_entry);
		
		if(ui == 0)
		{
			sChoice[0] = '\0';
			NiSprintf(sChoice, 2048, "<br>%s%s<br>",TITLECOLOR, _TRAN("你可以选择"));
			m_pkTextCtrl->AppendText(sChoice, strlen(sChoice),TRUE);
		}
		if (pkItemInfo)
		{

			char buf[256];
			char count[256];
			itoa(m_ChooseReward.vChoice[ui].item_entry, buf, 10);
			itoa(m_ChooseReward.vChoice[ui].count, count , 10);

			std::string strItemCN = count;
			std::string strItemID = buf ;

			std::string str = pkItemInfo->C_icon;
			str = "<skin:" + str + ">";
			str = "<a:#_CHOOSEITEMICON_"+ strItemID + "_"+strItemCN +">" + str + "</a> " ;
			
			m_pkTextCtrl->AppendText(str.c_str(), str.length(), TRUE);
		}else
		{
			ULOG("load item %u error", m_ChooseReward.vChoice[ui].item_entry);
		}
	}

	((UScrollBar*)GetChildByID(2))->ReCalLayout();
}

void UNpcQuestDDlg::SetDlgByRequestItems(ui32 uiQuestId)
{
	ClearControl();

	CQuestDB::stQuest* pkQuest = g_pkQusetInfo->GetQuestInfo(uiQuestId);
	if(pkQuest == NULL)
	{
		char szBuf[128];
		NiSprintf(szBuf, 128, "Don't find the task ID as %d，please check the database!", m_LackItem.questid);
		UMessageBox::MsgBox_s(szBuf);
		return;
	}

	char szBuf[2048];
	szBuf[0] = '\0';
	NiSprintf(szBuf, 2048, "<font:ArialBold:14><color:3f2404>%s<br>", pkQuest->strTitle.c_str());
	m_pkTextCtrl->SetText(szBuf, strlen(szBuf));

	NiSprintf(szBuf, 2048, "<font:Arial:14><color:4d2f07>%s%s<br>", _TRAN("　　"),pkQuest->strInCompleteText.c_str());
	m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);

	//SetNpcHead(m_guid);

	GetChildByID(0)->SetVisible(TRUE);
	GetChildByID(6)->SetVisible(FALSE);
}

void UNpcQuestDDlg::OnURLCtrlClick(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;

	int pos = strURL.find("#_CHOOSEITEMICON_") ;
	int len = strlen("#_CHOOSEITEMICON_");
	if (pos == -1)
	{
		pos = strURL.find("#_REWITEM_");
		len = strlen("#_REWITEM_");
	}

	if (pos != -1)
	{
		string strdata = strURL.substr(pos + len , strURL.length()) ;
		int entryPos = strdata.find("_");
		string entrystr = strdata.substr(0, entryPos);
		ui32 entry = atoi(entrystr.c_str());

		UFittingRoom* pFittingRoom = INGAMEGETFRAME(UFittingRoom, FRAME_IG_FITTINGROOMDLG);
		if(!pFittingRoom)
			return;
		pFittingRoom->FittingEquipment(entry, (ui64)0);
	}
}