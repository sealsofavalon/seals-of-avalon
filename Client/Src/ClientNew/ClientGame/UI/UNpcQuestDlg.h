#ifndef UNPCQUESTDLG_H
#define UNPCQUESTDLG_H

#include "UInc.h"
#include "../../../../../SDBase/Protocol/S2C_Quest.h"

class UBitmapButtonEx;

class UNpcQuestDlg : public UDialog
{
	UDEC_CLASS(UNpcQuestDlg);
	UDEC_MESSAGEMAP();
public:
	UNpcQuestDlg();
	virtual ~UNpcQuestDlg();

	void OnQuest(MSG_S2C::stNPC_Gossip_Message& stMessage, int iIndex);
	void OnNetMessage(MSG_S2C::stQuestGiver_Offer_Reward& stMessage); //选择奖励
	void OnNetMessage(MSG_S2C::stQuestGiver_Request_Items& stMessage); //任务完成的提示 
	void OnNetMessage(MSG_S2C::stQuestGiver_Quest_Details& stMessage);

	void SetNpcHead(ui64 guid);
	ui64 GetNpcGUID(){return m_guid;}
	void SetCUSNormal();
	
	void SetDlgByRequestItems(ui32 uiQuestId = 0); //没达到提交任务要求后的提示
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
	virtual void OnTimer(float fDeltaTime);
protected:
	void OnClickAccept();
	void OnClickRefuse();
	void OnURLClicked(UNM* pNM);
	void OnURLMove(UNM* pNM);
	void OnURLCtrlClick(UNM* pNM);
	void OnURLLeave();
	
	void OnDoNothing();
	void ClearControl();

private:
	URichTextCtrl* m_pkTextCtrl;

	// 对话信息相关数据
	int m_iIndex;	
	MSG_S2C::stNPC_Gossip_Message m_stMessage;	//对话信息
	MSG_S2C::stQuestGiver_Quest_Details m_Details;
	ui64 m_guid;
};


class UNpcQuestDDlg : public UDialog
{
	UDEC_CLASS(UNpcQuestDDlg);
	UDEC_MESSAGEMAP();
public:
	UNpcQuestDDlg();
	virtual ~UNpcQuestDDlg();

	void OnNetMessage(MSG_S2C::stQuestGiver_Offer_Reward& stMessage); //选择奖励
	void OnNetMessage(MSG_S2C::stQuestGiver_Request_Items& stMessage); //任务完成的提示 

	void SetNpcHead(ui64 guid);
	ui64 GetNpcGUID(){return m_guid;}
	void SetCUSNormal();

	void SetDlgByRequestItems(ui32 uiQuestId = 0); //没达到提交任务要求后的提示

protected:
	virtual BOOL OnCreate();

	virtual void OnDestroy();


	virtual void OnClose();
	virtual BOOL OnEscape();
	virtual void OnTimer(float fDeltaTime);

	void OnClickOK();
	void OnDoNothing();

	void OnClickQuestComplete();

	void OnURLClicked(UNM* pNM);
	void OnURLMove(UNM* pNM);
	void OnURLCtrlClick(UNM* pNM);
	void OnURLLeave();

	void SetDlgByOfferReward();  //任务奖励
	void ClearControl();

private:
	URichTextCtrl* m_pkTextCtrl;
	ui32 m_iRewardEntry;

	MSG_S2C::stQuestGiver_Offer_Reward m_ChooseReward; //任务奖励列表
	MSG_S2C::stQuestGiver_Request_Items m_LackItem; //任务缺少的东西列表
	ui64 m_guid;
};

#endif