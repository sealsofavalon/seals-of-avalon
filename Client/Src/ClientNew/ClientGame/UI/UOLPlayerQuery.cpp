#include "stdafx.h"
#include "UOLPlayerQuery.h"
#include "../Network/PacketBuilder.h"
#include "UIGamePlay.h"
#include "UInGameBar.h"
#include "SocialitySystem.h"
#include "UFun.h"
#include "UIRightMouseList.h"
#include "Utils/MapInfoDB.h"
#include "ObjectManager.h"
static const char* race_String[] = {"不限", "妖族", "人族", "巫族"};
static const char* class_String[] = {"不限","武修","羽箭","仙道","真巫"};
const int QueryList_MaxCount = 20;
//////////////////////////////////////////////////////////////////////////
class UIQueryListItem : public UControl
{
	UDEC_CLASS(UIQueryListItem);
public:
	UIQueryListItem()
	{
		m_NameStateText = NULL;
		m_LevelStateText = NULL;
		m_ClassStateText = NULL;
		m_MapStateText = NULL;
		m_RaceStateText = NULL;

		m_GUID = 0;
		m_RightMouseDown = FALSE;
		m_bChoose = FALSE;
		m_MouseDown = FALSE;
		m_MemRace = 0;
	}
	~UIQueryListItem()
	{

	}
public:
	void SetBeChoose(BOOL choose)
	{
		m_bChoose = choose;
		UpdataMemTextColor();
	}
	void UpdataMemTextColor()
	{
		UColor color = m_bChoose?m_Style->m_FontColorHL:m_Style->m_FontColor;

		m_NameStateText->SetTextColor(color);
		m_MapStateText->SetTextColor(color);
		m_ClassStateText->SetTextColor(color);
		m_LevelStateText->SetTextColor(color);
		m_RaceStateText->SetTextColor(color);

		m_NameStateText->SetTextEdge(m_bChoose);
		m_MapStateText->SetTextEdge(m_bChoose);
		m_ClassStateText->SetTextEdge(m_bChoose);
		m_LevelStateText->SetTextEdge(m_bChoose);
		m_RaceStateText->SetTextEdge(m_bChoose);
	}
	void SetMemberBase(ui32 mapid, const char* Name, ui8 Race, ui32 level, ui32 Class, ui64 guid)
	{
		if (Name){
			m_NameStateText->SetText(Name);
			m_Name = Name;
		}

		char buf[256];
		buf[0] = 0;
		if (level){
			itoa(level, buf, 10);
		}else{
			sprintf_s(buf, 256, "??");
		}
		m_LevelStateText->SetText(buf);

		m_RaceStateText->SetText( _TRAN(race_String[Race]));
		m_MemRace = Race;

		m_ClassStateText->SetText(_TRAN(class_String[Class]));
		CMapInfoDB::MapInfoEntry entry;
		if(g_pkMapInfoDB->GetMapInfo(mapid, entry))
		{
			m_MapStateText->SetText(entry.desc.c_str());
		}else
			m_MapStateText->SetText(_TRAN("苍之陆"));

		m_GUID = guid;
		UpdataMemTextColor();
	}
	bool GetName(std::string& str)
	{
		if (m_Name.size())
		{
			str = m_Name;
			return true;
		}
		return false;
	}
	bool GetGuid(ui64& guid)
	{
		if (m_GUID)
		{
			guid = m_GUID;
			return true;
		}
		return false;
	}
protected:
	virtual BOOL OnCreate()
	{
		if (!UControl::OnCreate())
		{
			return FALSE;
		}
		if (m_NameStateText == NULL)
		{
			m_NameStateText = UDynamicCast(UStaticText, GetChildByID(0));
			if (!m_NameStateText)
			{
				return FALSE;
			}
			m_NameStateText->SetTextAlign(UT_CENTER);
		}
		if (m_LevelStateText == NULL)
		{
			m_LevelStateText = UDynamicCast(UStaticText, GetChildByID(1));
			if (!m_LevelStateText)
			{
				return FALSE;
			}
			m_LevelStateText->SetTextAlign(UT_CENTER);
		}
		if (m_ClassStateText == NULL)
		{
			m_ClassStateText = UDynamicCast(UStaticText, GetChildByID(2));
			if (!m_ClassStateText)
			{
				return FALSE;
			}
			m_ClassStateText->SetTextAlign(UT_CENTER);
		}
		if(m_RaceStateText == NULL)
		{
			m_RaceStateText = UDynamicCast(UStaticText, GetChildByID(3));
			if (!m_RaceStateText)
			{
				return FALSE;
			}
			m_RaceStateText->SetTextAlign(UT_CENTER);
		}
		if (m_MapStateText == NULL)
		{
			m_MapStateText = UDynamicCast(UStaticText, GetChildByID(4));
			if (!m_MapStateText)
			{
				return FALSE;
			}
			m_MapStateText->SetTextAlign(UT_CENTER);
		}
		return TRUE;
	}
	virtual void OnDestroy()
	{
		if (m_bChoose)
		{
			if (SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
			{
				SocialitySys->GetOLPlayerQuerySysPtr()->SetSelectItem(NULL);
			}
		}
		UControl::OnDestroy();
	}
	virtual void OnMouseEnter(const UPoint& position, UINT flags)
	{

	}
	virtual void OnMouseLeave(const UPoint& position, UINT flags)
	{

	}
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
	{
		m_MouseDown = TRUE;
	}
	virtual void OnMouseUp(const UPoint& position, UINT flags)
	{
		if (m_MouseDown)
		{
			m_MouseDown = FALSE;
			if (SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
			{
				SocialitySys->GetOLPlayerQuerySysPtr()->SetSelectItem(this);
			}
		}
	}
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
	{
		m_RightMouseDown = TRUE;
	}
	virtual void OnRightMouseUp(const UPoint& position, UINT flags)
	{
		if (m_RightMouseDown)
		{
			UIRightMouseList* pkRightMouseList = INGAMEGETFRAME(UIRightMouseList,FRAME_IG_RIGHTMOUSELIST);
			if(pkRightMouseList)
			{
				pkRightMouseList->Popup(position, BITMAPLIST_OLPLAYERQUERY, m_Name.c_str(), m_GUID);
			}
			m_RightMouseDown = FALSE;
			if (SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
			{
				SocialitySys->GetOLPlayerQuerySysPtr()->SetSelectItem(this);
			}
		}
	}
	virtual void OnRender(const UPoint& offset, const URect& updateRect)
	{
		if(m_bChoose && SocialitySys && SocialitySys->mBKGtexture)
			sm_UiRender->DrawImage(SocialitySys->mBKGtexture, updateRect);

		RenderChildWindow(offset, updateRect);
	}
protected:
	BOOL		 m_bChoose;
	BOOL		 m_RightMouseDown;
	BOOL		 m_MouseDown;
	uint8          m_MemRace;
	ui64			 m_GUID;
	std::string   m_Name;
	UStaticText* m_NameStateText;
	UStaticText* m_RaceStateText;
	UStaticText* m_LevelStateText;
	UStaticText* m_ClassStateText;
	UStaticText* m_MapStateText;
};
UIMP_CLASS(UIQueryListItem, UControl)
void UIQueryListItem::StaticInit(){}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UIPlayerQueryList, UControl)
void UIPlayerQueryList::StaticInit(){}
UIPlayerQueryList::UIPlayerQueryList()
{
}
UIPlayerQueryList::~UIPlayerQueryList()
{

}
BOOL UIPlayerQueryList::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	return TRUE;
}
void UIPlayerQueryList::OnDestroy()
{
	ClearList();
	UControl::OnDestroy();
}	
void UIPlayerQueryList::OnRender(const UPoint& offset,const URect &updateRect)
{
	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	g_pRenderInterface->SetScissorRect(sm_UiRender->GetClipRect());

	RenderChildWindow(offset, updateRect);

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
}
void UIPlayerQueryList::UpdataList()
{
	std::vector<MSG_S2C::stQueryPlayersAck::player_info> Sortlist;
	if(!SocialitySys->GetOLPlayerQuerySysPtr()->GetQueryPlayersList(Sortlist))
		return;

	int curpage, maxpage;
	SocialitySys->GetOLPlayerQuerySysPtr()->GetPage(curpage, maxpage);
	for (unsigned int ui = 0 ; ui < QueryList_MaxCount ; ui++)
	{
		UIQueryListItem* pItem = UDynamicCast(UIQueryListItem, GetChildByID(ui));
		if (pItem)
		{
			UINT pos = curpage * QueryList_MaxCount + ui;
			bool bShow = pos < Sortlist.size();
			pItem->SetVisible(bShow);
			if (bShow)
			{
				pItem->SetMemberBase(Sortlist[pos].mapid, Sortlist[pos].name.c_str(), Sortlist[pos].race, Sortlist[pos].level, Sortlist[pos].cls, Sortlist[pos].id);
			}
		}
	}
	if (SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
	{
		SocialitySys->GetOLPlayerQuerySysPtr()->SetSelectItem(NULL);
	}
}
void UIPlayerQueryList::ClearList()
{
	if (SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
	{
		SocialitySys->GetOLPlayerQuerySysPtr()->SetSelectItem(NULL);
	}
	sm_System->SetMouseControl(NULL);
	for (unsigned int ui = 0 ; ui < QueryList_MaxCount ; ui++)
	{
		UIQueryListItem* pItem = UDynamicCast(UIQueryListItem, GetChildByID(ui));
		if (pItem)
		{
			pItem->SetVisible(FALSE);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UOLPlayerQueryFrame, UDialog)
UBEGIN_MESSAGE_MAP(UOLPlayerQueryFrame, UDialog)
UON_BN_CLICKED(1,&UOLPlayerQueryFrame::OnSortByName)
UON_BN_CLICKED(2,&UOLPlayerQueryFrame::OnSortByLevel)
UON_BN_CLICKED(3,&UOLPlayerQueryFrame::OnSortByClass)
UON_BN_CLICKED(4,&UOLPlayerQueryFrame::OnSortByRace)
UON_BN_CLICKED(5,&UOLPlayerQueryFrame::OnSortByMap)
UON_BN_CLICKED(40,&UOLPlayerQueryFrame::OnClickBtnSendQuery)
UON_BN_CLICKED(50,&UOLPlayerQueryFrame::OnClickBtnFriendPage)
UON_BN_CLICKED(51,&UOLPlayerQueryFrame::OnClickBtnGuildPage)
UON_BN_CLICKED(52,&UOLPlayerQueryFrame::OnClickBtnOLPlayerPage)
UON_BN_CLICKED(53,&UOLPlayerQueryFrame::OnClickBtnTeamPage)
UON_BN_CLICKED(54,&UOLPlayerQueryFrame::OnClickBtnGroupApplyPage)
UON_BN_CLICKED(60,&UOLPlayerQueryFrame::OnPrevPage)
UON_BN_CLICKED(62,&UOLPlayerQueryFrame::OnNextPage)
UON_UEDIT_UPDATE(15, &UOLPlayerQueryFrame::OnClickBtnSendQuery)
UEND_MESSAGE_MAP()
void UOLPlayerQueryFrame::StaticInit(){}

UOLPlayerQueryFrame::UOLPlayerQueryFrame()
{
	m_pPlayerQueryList = NULL;
	SendWaitTime = 0.f;
	m_IsUpdateChild = FALSE;
}
UOLPlayerQueryFrame::~UOLPlayerQueryFrame()
{}
BOOL UOLPlayerQueryFrame::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}
	if(m_pPlayerQueryList == NULL)
	{
		m_pPlayerQueryList = UDynamicCast(UIPlayerQueryList, GetChildByID(0));
		if(!m_pPlayerQueryList)
			return FALSE;
	}
	UButton* pCheckButton = (UButton*)GetChildByID(52);
	if (pCheckButton == NULL)
	{
		return FALSE;
	}
	pCheckButton->SetCheck(FALSE);
	pCheckButton = (UButton*)GetChildByID(51);
	if (pCheckButton == NULL)
	{
		return FALSE;
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(42));
	if (!pText)
	{
		return FALSE;
	}
	pText->SetTextEdge(true);
	pCheckButton->SetActive(false);
	InitQueryFrame();
	m_bCanDrag = FALSE;
	return TRUE;
}
void UOLPlayerQueryFrame::SetGuildBtnActive(bool active)
{
	UButton* pCheckButton = (UButton*)GetChildByID(51);
	if (pCheckButton == NULL)
	{
		return;
	}
	pCheckButton->SetActive(active);
}
void UOLPlayerQueryFrame::OnDestroy()
{
	if (SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
	{
		SocialitySys->GetOLPlayerQuerySysPtr()->SetSelectItem(NULL);
	}
	UDialog::OnDestroy();
}
void UOLPlayerQueryFrame::OnClose()
{	
	sm_System->GetCurFrame()->SetFocusControl();
	if(SocialitySys) SocialitySys->HideAll();
	UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	if (pGameBar)
	{
		UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONFRIEND);
		if (pBtn)
		{
			pBtn->SetCheckState(FALSE);
		}
	}
}
void UOLPlayerQueryFrame::OnTimer(float fDeltaTime)
{
	if(SendWaitTime > 0.f)
		SendWaitTime -= fDeltaTime;
	else
		GetChildByID(40)->SetActive(TRUE);
}
BOOL UOLPlayerQueryFrame::OnEscape()
{
	if(!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UOLPlayerQueryFrame::OnClickBtnFriendPage()
{
	if(SocialitySys)
		SocialitySys->ShowFrame(1);
}
void UOLPlayerQueryFrame::OnClickBtnGuildPage()
{
	if(SocialitySys)
		SocialitySys->ShowFrame(0);
}
void UOLPlayerQueryFrame::OnClickBtnOLPlayerPage()
{

}
void UOLPlayerQueryFrame::OnClickBtnTeamPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(3);
	}
}
void UOLPlayerQueryFrame::OnClickBtnGroupApplyPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(4);
	}
}
void UOLPlayerQueryFrame::OnClickBtnSendQuery()
{
	if(SendWaitTime > 0.f)
		return;
	SendWaitTime = 10.f;
	GetChildByID(40)->SetActive(FALSE);
	uint8 min_level = 1, max_level = 80, race = 0, cls = 0;
	uint16 mapId = 0;
	std::string Name;

	GetQueryInfo(min_level, max_level, mapId, race, cls, Name);

	if(SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
		SocialitySys->GetOLPlayerQuerySysPtr()->SendMsgQueryPlayers(min_level, max_level, mapId, race, cls, Name);
}

void UOLPlayerQueryFrame::OnSortByName()
{
	if(SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
		SocialitySys->GetOLPlayerQuerySysPtr()->SortMemberByName();

	m_pPlayerQueryList->UpdataList();
}
void UOLPlayerQueryFrame::OnSortByLevel()
{
	if(SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
		SocialitySys->GetOLPlayerQuerySysPtr()->SortMemberByLevel();

	m_pPlayerQueryList->UpdataList();
}
void UOLPlayerQueryFrame::OnSortByClass()
{
	if(SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
		SocialitySys->GetOLPlayerQuerySysPtr()->SortMemberByClass();

	m_pPlayerQueryList->UpdataList();
}
void UOLPlayerQueryFrame::OnSortByRace()
{
	if(SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
		SocialitySys->GetOLPlayerQuerySysPtr()->SortMemberByRacel();

	m_pPlayerQueryList->UpdataList();
}
void UOLPlayerQueryFrame::OnSortByMap()
{
	if(SocialitySys && SocialitySys->GetOLPlayerQuerySysPtr())
		SocialitySys->GetOLPlayerQuerySysPtr()->SortMemberByMap();

	m_pPlayerQueryList->UpdataList();
}

void UOLPlayerQueryFrame::OnPrevPage()
{
	UControl* pBtn = GetChildByID(60);
	if (pBtn)
	{
		pBtn->SetActive(SocialitySys->GetOLPlayerQuerySysPtr()->PrevPage());
	}
	int curpage, maxpage;
	SocialitySys->GetOLPlayerQuerySysPtr()->GetPage(curpage, maxpage);

	 pBtn = GetChildByID(62);
	if (pBtn)
	{
		pBtn->SetActive(maxpage > 0 && curpage < maxpage);
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(61));
	if (pText)
	{
		char buf[256];
		pText->SetText(itoa(curpage + 1, buf, 10));
	}
	m_pPlayerQueryList->UpdataList();
}

void UOLPlayerQueryFrame::OnNextPage()
{
	UControl* pBtn = GetChildByID(62);
	if (pBtn)
	{
		pBtn->SetActive(SocialitySys->GetOLPlayerQuerySysPtr()->NextPage());
	}
	int curpage, maxpage;
	SocialitySys->GetOLPlayerQuerySysPtr()->GetPage(curpage, maxpage);
	pBtn = GetChildByID(60);
	if (pBtn)
	{
		pBtn->SetActive(maxpage > 0 && curpage > 0);
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(61));
	if (pText)
	{
		char buf[256];
		pText->SetText(itoa(curpage + 1, buf, 10));
	}
	m_pPlayerQueryList->UpdataList();
}
void UOLPlayerQueryFrame::Show()
{
	SetVisible(TRUE);
}
void UOLPlayerQueryFrame::Hide()
{
	SetVisible(FALSE);
	UButton* pCheckButton = (UButton*)GetChildByID(52);
	if (pCheckButton){
		pCheckButton->SetCheck(FALSE);
	}
}
UIPlayerQueryList* UOLPlayerQueryFrame::GetQueryListCtrl()
{
	return m_pPlayerQueryList;
}
void UOLPlayerQueryFrame::InitQueryFrame()
{
	//12 - Map UComboBox
	//13 - race UComboBox
	//14 - class UComboBox
	//15 - name UEdit
	//16 - minLevel UEdit
	//17 - maxlevel UEdit

	UComboBox* pComboBox = NULL;
	pComboBox = UDynamicCast(UComboBox, GetChildByID(12));
	if(pComboBox)
	{
		pComboBox->Clear();
		std::vector<CMapInfoDB::MapInfoEntry> vinfo;
		g_pkMapInfoDB->GetMapVInfoFromFlags(1, vinfo);

		pComboBox->AddItem(_TRAN("不限"));
		for (unsigned int ui = 0 ; ui < vinfo.size() ; ui++)
		{
			unsigned int* pUiMapId = new unsigned int;
			memcpy(pUiMapId, &vinfo[ui].entry, sizeof(unsigned int));
			pComboBox->AddItem(vinfo[ui].desc.c_str(), pUiMapId);
		}
		pComboBox->SetCurSel(0);
	}
	pComboBox = UDynamicCast(UComboBox, GetChildByID(13));
	if(pComboBox)
	{
		pComboBox->Clear();
		int size = sizeof(race_String) / sizeof(race_String[0]); 
		for(int i = 0 ; i < size ; i++)
		{
			pComboBox->AddItem( _TRAN(race_String[i]) );
		}
		pComboBox->SetCurSel(0);
	}
	pComboBox = UDynamicCast(UComboBox, GetChildByID(14));
	if(pComboBox)
	{
		pComboBox->Clear();
		int size = sizeof(class_String) / sizeof(class_String[0]); 
		for(int i = 0 ; i < size ; i++)
		{
			pComboBox->AddItem(_TRAN(class_String[i]));
		}
		pComboBox->SetCurSel(0);
	}
	UEdit* pEdit = NULL;
	pEdit = UDynamicCast(UEdit, GetChildByID(16));
	if(pEdit)
	{
		pEdit->Clear();
		pEdit->SetNumberOnly(TRUE);
		pEdit->SetText("1");
		pEdit->SetMaxLength(3);
		pEdit->SetTextAlignment(UT_CENTER);
	}
	pEdit = UDynamicCast(UEdit, GetChildByID(17));
	if(pEdit)
	{
		pEdit->Clear();
		pEdit->SetNumberOnly(TRUE);
		pEdit->SetText("80");
		pEdit->SetMaxLength(3);
		pEdit->SetTextAlignment(UT_CENTER);
	}
}
void UOLPlayerQueryFrame::GetQueryInfo(uint8& min_level, uint8& max_level, uint16& mapid, uint8& race, uint8& cls, std::string& str)
{
	//12 - Map UComboBox
	//13 - race UComboBox
	//14 - class UComboBox
	//15 - name UEdit
	//16 - minLevel UEdit
	//17 - maxlevel UEdit

	UComboBox* pComboBox = NULL;
	int iIndex = 0;
	pComboBox = UDynamicCast(UComboBox, GetChildByID(12));
	if(pComboBox)
	{
		iIndex = pComboBox->GetCurSel();
		if(iIndex != -1)
		{
			unsigned int* pUiMapId = (unsigned int* )pComboBox->GetItemData(iIndex);
			if(pUiMapId)
				mapid = *pUiMapId;
			else
				mapid = 0;
		}
	}
	pComboBox = UDynamicCast(UComboBox, GetChildByID(13));
	if(pComboBox)
	{
		iIndex = pComboBox->GetCurSel();
		if(iIndex != -1)
		{
			race = iIndex;
		}
	}
	pComboBox = UDynamicCast(UComboBox, GetChildByID(14));
	if(pComboBox)
	{
		iIndex = pComboBox->GetCurSel();
		if(iIndex != -1)
		{
			cls = iIndex;
		}
	}

	char buf[256];
	UEdit* pEdit = NULL;
	pEdit = UDynamicCast(UEdit, GetChildByID(15));
	if(pEdit)
	{
		iIndex = pEdit->GetText(buf, 256);
		buf[iIndex] = 0;
		str = buf;
	}
	pEdit = UDynamicCast(UEdit, GetChildByID(16));
	if(pEdit)
	{
		iIndex = pEdit->GetText(buf, 256);
		buf[iIndex] = 0;
		min_level = atoi(buf);
	}
	pEdit = UDynamicCast(UEdit, GetChildByID(17));
	if(pEdit)
	{
		iIndex = pEdit->GetText(buf, 256);
		buf[iIndex] = 0;
		max_level = atoi(buf);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
OnlinePlayerQuerySys::OnlinePlayerQuerySys()
{
	m_pQueryFrame = NULL;
	m_bSelectItem =NULL;
	m_CurrentPage = 0;
	m_MaxPage = 0;
}
OnlinePlayerQuerySys::~OnlinePlayerQuerySys()
{
}
BOOL OnlinePlayerQuerySys::CreateFrame(UControl* ctrl)
{
	UControl* NewCtrl = NULL;
	if (m_pQueryFrame == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\OLPlayerQueryDlg.udg");
		m_pQueryFrame = UDynamicCast(UOLPlayerQueryFrame, NewCtrl);
		if (m_pQueryFrame)
		{
			ctrl->AddChild(m_pQueryFrame);
			m_pQueryFrame->SetId(FRAME_IG_ONLINEPLAYERQUERYDLG);
		}
		else
		{
			return FALSE;
		}
	}
	return TRUE;
}
void OnlinePlayerQuerySys::DestoryFrame()
{
}
void OnlinePlayerQuerySys::ShowFrame()
{
	m_pQueryFrame->Show();
}
void OnlinePlayerQuerySys::HideFrame()
{
	m_pQueryFrame->Hide();
}
void OnlinePlayerQuerySys::ParseMsgQueryPlayersAck(std::vector<MSG_S2C::stQueryPlayersAck::player_info>& vList)
{
	m_QueryPlayerList = vList;
	m_CurrentPage = 0;
	m_MaxPage = m_QueryPlayerList.size() / QueryList_MaxCount;
	if (m_QueryPlayerList.size() % QueryList_MaxCount == 0)
	{
		--m_MaxPage;
	}
	mSortMemberVec.clear();
	if(!m_pQueryFrame)
		return;
	m_pQueryFrame->OnNextPage();
	m_pQueryFrame->OnPrevPage();
	UIPlayerQueryList* pPlayerQueryList = m_pQueryFrame->GetQueryListCtrl();
	if(!pPlayerQueryList)
		return;
	pPlayerQueryList->ClearList();
	SortMemberByName();
	pPlayerQueryList->UpdataList();
}
void OnlinePlayerQuerySys::SendMsgQueryPlayers(uint8 min_level, uint8 max_level, uint16 mapid, uint8 race, uint8 cls, std::string& str)
{
	PacketBuilder->SendQueryPlayers(min_level, max_level, mapid, race, cls, str);
}
void OnlinePlayerQuerySys::SetSelectItem(UIQueryListItem* item)
{
	//注意 有可能控件已经被删除
	if (m_bSelectItem)
	{
		m_bSelectItem->SetBeChoose(FALSE);
	}
	if (item)
	{
		m_bSelectItem = item;
		m_bSelectItem->SetBeChoose(TRUE);
	}
	else
	{
		m_bSelectItem = NULL;
	}
}
bool KeyOLPlayerName(const MSG_S2C::stQueryPlayersAck::player_info& L_S, const MSG_S2C::stQueryPlayersAck::player_info& R_S)
{
	return (L_S.name > R_S.name);
}
bool KeyOLPlayerLevel(const MSG_S2C::stQueryPlayersAck::player_info& L_S, const MSG_S2C::stQueryPlayersAck::player_info& R_S)
{
	return (L_S.level > R_S.level);
}
bool KeyOLPlayerRace(const MSG_S2C::stQueryPlayersAck::player_info& L_S, const MSG_S2C::stQueryPlayersAck::player_info& R_S)
{
	return (L_S.race > R_S.race);
}
bool KeyOLPlayerClass(const MSG_S2C::stQueryPlayersAck::player_info& L_S, const MSG_S2C::stQueryPlayersAck::player_info& R_S)
{
	return (L_S.cls > R_S.cls);
}
bool KeyOLPlayerMap(const MSG_S2C::stQueryPlayersAck::player_info& L_S, const MSG_S2C::stQueryPlayersAck::player_info& R_S)
{
	return (L_S.mapid > R_S.mapid);
}
#define SORTOLPLAYER(FUNCPTR, vec) std::sort(m_QueryPlayerList.begin(), m_QueryPlayerList.end(), FUNCPTR);\
	for(size_t ui = 0; ui < m_QueryPlayerList.size() ; ui++)\
		vec.push_back(m_QueryPlayerList[ui].id);\
	mSortMemberVec = vec;

void OnlinePlayerQuerySys::SortMemberByName()
{
	std::vector<ui64> temp;
	SORTOLPLAYER(&KeyOLPlayerName, temp);
}
void OnlinePlayerQuerySys::SortMemberByLevel()
{
	std::vector<ui64> temp;
	SORTOLPLAYER(&KeyOLPlayerLevel, temp);
}
void OnlinePlayerQuerySys::SortMemberByRacel()
{
	std::vector<ui64> temp;
	SORTOLPLAYER(&KeyOLPlayerRace, temp);
}
void OnlinePlayerQuerySys::SortMemberByClass()
{
	std::vector<ui64> temp;
	SORTOLPLAYER(&KeyOLPlayerClass, temp);
}
void OnlinePlayerQuerySys::SortMemberByMap()
{
	std::vector<ui64> temp;
	SORTOLPLAYER(&KeyOLPlayerMap, temp);
}
#undef SORTOLPLAYER
bool OnlinePlayerQuerySys::GetSortList(std::vector<ui64>& vlist)
{
	vlist = mSortMemberVec;
	return true;
}

bool OnlinePlayerQuerySys::GetQueryPlayersList(std::vector<MSG_S2C::stQueryPlayersAck::player_info>& vList)
{
	vList = m_QueryPlayerList;
	return true;
}

void OnlinePlayerQuerySys::SetGuildActive(bool active)
{
	if (m_pQueryFrame)
	{
		m_pQueryFrame->SetGuildBtnActive(active);
	}
}

bool OnlinePlayerQuerySys::PrevPage()
{
	if (m_CurrentPage > 0)
	{
		--m_CurrentPage;
		return m_CurrentPage != 0;
	}
	return false;
}

bool OnlinePlayerQuerySys::NextPage()
{
	if (m_CurrentPage < m_MaxPage)
	{
		++m_CurrentPage;
		return m_CurrentPage != m_MaxPage;
	}
	return false;
}

bool OnlinePlayerQuerySys::GetPage(int& CurPage, int& MaxPage)
{
	CurPage = m_CurrentPage;
	MaxPage = m_MaxPage;
	return true;
}