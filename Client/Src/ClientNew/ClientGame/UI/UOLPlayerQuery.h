#ifndef UOLPLAYERQUERY_H
#define UOLPLAYERQUERY_H

#include "UInc.h"

class UIPlayerQueryList : public UControl
{
	UDEC_CLASS(UIPlayerQueryList);
public:
	UIPlayerQueryList();
	~UIPlayerQueryList();
public:
	void UpdataList();
	void ClearList();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
protected:
	//typedef std::map<ui64, class UIQueryListItem*>QueryItemMap;
	//QueryItemMap mQueryItemMap;
};

class UOLPlayerQueryFrame : public UDialog
{
	UDEC_CLASS(UOLPlayerQueryFrame);
	UDEC_MESSAGEMAP();
public:
	UOLPlayerQueryFrame();
	~UOLPlayerQueryFrame();

	void Show();
	void Hide();

	UIPlayerQueryList* GetQueryListCtrl();
	void SetGuildBtnActive(bool active);
	void InitQueryFrame();
	void GetQueryInfo(uint8& min_level, uint8& max_level, uint16& mapid, uint8& race, uint8& cls, std::string& str);

	void OnPrevPage();
	void OnNextPage();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
	virtual void OnTimer(float fDeltaTime);

	void OnClickBtnFriendPage();
	void OnClickBtnGuildPage();
	void OnClickBtnOLPlayerPage();
	void OnClickBtnTeamPage();
	void OnClickBtnGroupApplyPage();
	void OnClickBtnSendQuery();

	void OnSortByName();
	void OnSortByLevel();
	void OnSortByClass();
	void OnSortByRace();
	void OnSortByMap();


	UIPlayerQueryList* m_pPlayerQueryList;
	float SendWaitTime;
};
class OnlinePlayerQuerySys
{
public:
	OnlinePlayerQuerySys();
	~OnlinePlayerQuerySys();
public:
	BOOL CreateFrame(UControl* ctrl);
	void DestoryFrame();
	void ShowFrame();
	void HideFrame();

	void SetSelectItem(class UIQueryListItem* item);
	void SetGuildActive(bool active);
public:
	void SortMemberByName();
	void SortMemberByLevel();
	void SortMemberByRacel();
	void SortMemberByClass();
	void SortMemberByMap();
	bool GetSortList(std::vector<ui64>& vlist);
	bool GetQueryPlayersList(std::vector<MSG_S2C::stQueryPlayersAck::player_info>& vList);

	bool PrevPage();
	bool NextPage();
	bool GetPage(int& CurPage, int& MaxPage);
private:
	class UIQueryListItem* m_bSelectItem;
	std::vector<ui64> mSortMemberVec;
public:
	void ParseMsgQueryPlayersAck(std::vector<MSG_S2C::stQueryPlayersAck::player_info>& vList);
public:
	void SendMsgQueryPlayers(uint8 min_level, uint8 max_level, uint16 mapid, uint8 race, uint8 cls, std::string& str);
protected:
	std::vector<MSG_S2C::stQueryPlayersAck::player_info> m_QueryPlayerList;
	//USERINTERFACE
	UOLPlayerQueryFrame* m_pQueryFrame;

	int m_CurrentPage;
	int m_MaxPage;
};
#endif