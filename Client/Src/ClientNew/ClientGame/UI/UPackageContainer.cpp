#include "stdafx.h"
#include "UPackageContainer.h"
#include "UPackageDialog.h"
#include "UITipSystem.h"
#include "Player.h"
#include "ItemSlotContainer.h"
#include "ItemSlot.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "Skill/SkillManager.h"
#include "UFun.h"
#include "UChat.h"
#include "UIGamePlay.h"
#include "SyServerTime.h"

#define MaxCellCount 144
#define MinCellCount 9

UIMP_CLASS(UPackageContainer, UItemContainerEx);

void UPackageContainer::StaticInit()
{
	UREG_PROPERTY("CellSkin", UPT_STRING, UFIELD_OFFSET(UPackageContainer, m_CellSkinStr));
}
UPackageContainer::UPackageContainer()
{
	m_MaskTexture = NULL;
	m_CellSkin = NULL;
	m_CanUseNob = 30;
	m_ItemQuality = NULL;
}
UPackageContainer::~UPackageContainer()
{

}
BOOL UPackageContainer::OnCreate()
{
	if (!UItemContainerEx::OnCreate())
	{
		return FALSE;
	}
	if (!m_CellSkinStr.Empty())
	{
		m_CellSkin = sm_System->LoadSkin(m_CellSkinStr.GetBuffer());
	}else
	{
		m_CellSkin = sm_System->LoadSkin("Package\\GridCell.skin");
	}
	if (m_MaskTexture == NULL)
	{
		m_MaskTexture = sm_UiRender->LoadTexture("DATA\\UI\\SKIN\\TimeBtnMask.dds");
	}
	m_ItemQuality = sm_System->LoadSkin("PickItemList/ItemQuilty.skin");
	if (m_CellSkin == NULL || m_MaskTexture == NULL || m_ItemQuality == NULL)
	{
		return FALSE;
	}
	return TRUE;
}
void UPackageContainer::OnDestroy()
{
	m_CellSkin = NULL;
	UItemContainerEx::OnDestroy();
}

void UPackageContainer::OnSize(const UPoint& NewSize)
{
	UControl::OnSize(NewSize);
	m_GridSize.x = int(NewSize.x /(float)m_GridCount.x) ;
	m_GridSize.y = int(NewSize.y / (float)m_GridCount.y) ; 
}
void UPackageContainer::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	if (!m_Active || !m_bCreated || !m_Visible)
	{
		return ;
	}

	UPoint pta = ScreenToWindow(position);
	pta.x -= m_HeadSize.x;
	pta.y -= m_HeadSize.y;

	UPoint cell(  (pta.x < 0 ? -1 : pta.x / m_GridSize.x), (pta.y < 0 ? -1 : pta.y / m_GridSize.y));
	if (cell.x >= 0 && cell.y >=0  && cell.x <= m_GridCount.x && cell.y <= m_GridCount.y)
	{
		UPoint prevSelected = m_SelGrid;
		SetSel(cell.y, cell.x);
		//

		UItemData* pData = (UItemData*)m_pItems[cell.y * m_GridCount.x + cell.x].UserData;
		if (pData && pData->bLock)
		{
			return;
		}
		if (nRepCnt == 1/* && prevSelected == m_SelGrid*/)
		{
			if (flags == LKM_SHIFT)
			{
				DispatchNotify(UGD_UPAG_SHIFT_AND_RBCLICK);
			}else
			{
				DispatchNotify(UGD_UPAG_RBCLICK);
			}
		 
		}
	}
}

void UPackageContainer::OnRightMouseUp(const UPoint& position, UINT flags)
{
	if (!m_Active || !m_bCreated || !m_Visible)
	{
		return ;
	}
	SetSel(-1,-1);
}
void UPackageContainer::DrawMask(UPoint offset,UPoint cell)
{
	if ( unsigned int(cell.y * m_GridCount.x + cell.x) >= m_CanUseNob)
	{
		return ;
	}

	ui32 CDLife = 0;
	ui32 CDMain = 0;
	ui32 SpelllID = 0;

	UItemData* pData = (UItemData*)GetItemData(cell.y,cell.x);
	if (pData && pData->pItemID != 0)
	{
		ItemPrototype_Client* pItem = ItemMgr->GetItemPropertyFromDataBase(pData->pItemID);

		if (pItem && (pItem->Class == ITEM_CLASS_CONSUMABLE || pItem->Class == ITEM_CLASS_USE || pItem->Class == ITEM_CLASS_RECIPE))
		{
			for (int i= 0; i < 5; i++)
			{
				if (pItem->Spells[i].Id != 0)
				{
					SpelllID  = pItem->Spells[i].Id;
					
					SYState()->SkillMgr->GetCooldownTime(SpelllID,CDLife,CDMain);
					if (CDLife)
					{
						URect rc(offset,m_GridSize);
						
						//URect uvRect(0,0,64,64);
						//int maskindex = ((CDLife - CDMain)*60 / CDLife);
						//int col = maskindex%8;
						//int row = maskindex/8;
						//uvRect.Offset(col*64,row*64);

						//sm_UiRender->DrawImage(m_MaskTexture,rc,uvRect,UColor(255,255,255,255),2);
						float age = float(CDLife - CDMain);
						float Life = float(CDLife);

						float angle = age/Life;
						sm_UiRender->DrawCoolDownMask(rc,angle,UColor(255,255,255,175));
					}
					break;
				}
			}

			// 判断宠物蛋
			CItemSlot* pItemSlot = ItemMgr->GetItemSlot(0, cell.y * m_GridCount.x + cell.x);
			if(pItemSlot && pItemSlot->GetGUID() && pItemSlot->IsPetEgg())
			{
				URect rc(offset,m_GridSize);
				float fTotalTime = (float)pItemSlot->PetEggTotalTime();
				float fLeftTime = (float)(::time(NULL) - gServerTime.GetDelayTime()  - pItemSlot->PetEggGenTime());
				if(fLeftTime > 0.0f)
				{
					sm_UiRender->DrawCoolDownMask(rc, fLeftTime / fTotalTime, UColor(255, 255, 255, 175));
				}				
			}
		}
	}
}
void UPackageContainer::DrawItemQualityMask(UPoint offset, UPoint cell)
{
	if ( unsigned int(cell.y * m_GridCount.x + cell.x) >= m_CanUseNob)
	{
		return ;
	}
	UItemData* pData = (UItemData*)GetItemData(cell.y, cell.x);
	if (pData && pData->pItemID != 0)
	{
		ItemPrototype_Client* pItem = ItemMgr->GetItemPropertyFromDataBase(pData->pItemID);

		if (m_ItemQuality)
		{
			int bkgindex = 0;
			switch (pItem->Quality)
			{
			case ITEM_QUALITY_POOR_GREY       :
				bkgindex = 0;
				break;
			case ITEM_QUALITY_NORMAL_WHITE     :   
				bkgindex = 0;
				break;
			case ITEM_QUALITY_UNCOMMON_GREEN   :  
				bkgindex = 1;
				break;
			case ITEM_QUALITY_RARE_BLUE        : 
				bkgindex = 2;
				break;
			case ITEM_QUALITY_EPIC_PURPLE      :   
				bkgindex = 3;
				break;
			case ITEM_QUALITY_LEGENDARY_ORANGE :  
				bkgindex = 4;
				break;
			}
			UControl::sm_UiRender->DrawSkin(m_ItemQuality, bkgindex, URect(offset, m_GridSize));
		}
	}
}
void UPackageContainer::DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver)
{
	//超过当前可用的 不渲染
	URect rc(offset, m_GridSize);
	if (unsigned int(cell.y * m_GridCount.x + cell.x) < m_CanUseNob)
	{
		if (m_CellSkin)
			sm_UiRender->DrawSkin(m_CellSkin, 0, rc);		

		DrawItemQualityMask(offset, cell);
		UItemContainerEx::DrawGridItem(offset,cell,selected,mouseOver);
		DrawMask(offset,cell);
		if (m_DragItem == cell)
		{
			sm_UiRender->DrawRectFill(rc, 0xB4000000);
		}
	}
	
}

void UPackageContainer::ResetCanUseNob(int nub)
{
	m_CanUseNob = nub;

	if (m_CanUseNob > MaxCellCount)
	{
		m_CanUseNob = MaxCellCount;
	}

	if (m_CanUseNob < MinCellCount)
	{
		m_CanUseNob = MinCellCount;
	}


//	if (m_CanUseNob > m_GridCount.x * m_GridCount.y)
	{
		int newRow = m_CanUseNob / m_GridCount.x ;

		//外加1行格子
		if (m_CanUseNob % m_GridCount.x >0)
		{
			newRow ++ ;
		}
		 
		SetGridRow(newRow);
	}
}
void UPackageContainer::SetGridRow(int nRow)
{
	float Height = float(m_GridCount.y * m_GridSize.y);
	UPoint pos = GetWindowPos();

	UItemContainerEx::SetGridCount(nRow,m_GridCount.x);

	float newHeight = float(m_GridCount.y * m_GridSize.y);
	
	//
	char buf[_MAX_PATH];
	NiSprintf(buf, _MAX_PATH,"old H %f new H %f", Height, newHeight);
	ULOG(buf);

	//由于这个控件的父窗口就是包裹界面. 在这里重设包裹的大小
	UPackageDialog* pD = (UPackageDialog*)GetParent();
	if (pD == NULL)
	{
		ULOG( _TRAN("Backpack bank error") );
	}
	pD->ToAddHeight(int(newHeight - Height));

	SetPosition(pos);
	SetHeight(int(newHeight));

	
}
#include "UFittingRoom.h"
void UPackageContainer::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	UItemContainerEx::OnMouseDown(point,nRepCnt,uFlags);
	int pos = m_GridCount.x*m_HoverGrid.y+m_HoverGrid.x;
	if (uFlags == LKM_SHIFT)
	{
		ChatSystem->AddItemMsgInPagckage(pos, m_ItemTypeID);
		m_bDragBegin = FALSE;
		OnShowTipsInfo();
	}else if (uFlags == LKM_CTRL)
	{
		ChatSystem->FittingEquipmentInPagckage(pos, m_ItemTypeID);
		m_bDragBegin = FALSE;
	}

	if (pos > m_CanUseNob)
	{
		UDialog* pkParent = UDynamicCast(UDialog, GetParent());
		if (pkParent)
		{
			pkParent->ActiveWindow();
		}
	}
}

void UPackageContainer::OnMouseUp(const UPoint& position, UINT flags)
{
	UPoint pta = ScreenToWindow(position);
	pta.x -= m_HeadSize.x;
	pta.y -= m_HeadSize.y;
	UPoint cell(  (pta.x < 0 ? -1 : pta.x / m_GridSize.x), (pta.y < 0 ? -1 : pta.y / m_GridSize.y));
	if (m_bDragBegin && m_ItemTypeID != UItemSystem::ICT_INVLALID && m_ItemTypeID != UItemSystem::ICT_PLAYER_TRADE_TENNET && m_ItemTypeID != UItemSystem::ICT_NPC_TRADE && cell == m_SelGrid)
	{
		UInGame* pInGame = UInGame::Get();

		if (pInGame == NULL)
			return ;

		if (m_SelGrid.x >= 0 && m_SelGrid.y >= 0)
		{
			WORD pos = m_SelGrid.y * m_GridCount.x + m_SelGrid.x ;
			ITEM* pItem = &m_pItems[pos];
			WORD* pPos = new WORD;
			*pPos = pos ;
			UItemData* pData = (UItemData*)pItem->UserData;
			if (pData && pData->bLock)
				return;

			if (!m_bLock)
			{
				BeginDragDrop(this, pItem->spSkin, pItem->nIconIndex, pPos, (UINT)m_ItemTypeID);
				OnHideTipsInfo();
				m_DragItem = m_SelGrid;
				m_bDragBegin = FALSE;
			}
		}
	}
	UItemContainerEx::OnMouseUp(position, flags);
}

void UPackageContainer::OnShowTipsInfo()
{
	if (m_HoverGrid == m_DragItem)
		return;
	int pos = m_GridCount.x*m_HoverGrid.y+m_HoverGrid.x;
	CPlayer * pLocalplayer = ObjectMgr->GetLocalPlayer();
	UPoint Tip_sit((m_HoverGrid.x+1)*m_GridSize.x,(m_HoverGrid.y+1)*m_GridSize.y);
	UPoint t_pt = WindowToScreen(Tip_sit);
	ui64 ItemGUID = 0;
	if (pLocalplayer)
	{
		CStorage * pItemSlotContainer = NULL;
		switch (m_ItemTypeID)
		{
		case UItemSystem::ICT_BANK:
			TipSystem->SetShowPlayerGuid(pLocalplayer->GetGUID());
			pItemSlotContainer = pLocalplayer->GetBankContainer();
			break;
		case UItemSystem::ICT_BAG:
			TipSystem->SetShowPlayerGuid(pLocalplayer->GetGUID());
			pItemSlotContainer = pLocalplayer->GetItemContainer();
			break;
		}
		if (pItemSlotContainer)
		{
			CItemSlot* pSlot = (CItemSlot*)pItemSlotContainer->GetSlot(pos);
			if (pSlot)
			{
				ItemGUID = pSlot->GetGUID();
			}
			
		}
	}
	if (ItemGUID)
	{
		UItemData* pData = (UItemData*)GetItemData(m_HoverGrid.y,m_HoverGrid.x);
		if (pData && pData->pItemID != 0)
		{
			TipSystem->ShowTip(TIP_TYPE_ITEM,pData->pItemID,t_pt,ItemGUID);
		}
	}
}
void UPackageContainer::OnHideTipsInfo()
{
	TipSystem->HideTip();
}