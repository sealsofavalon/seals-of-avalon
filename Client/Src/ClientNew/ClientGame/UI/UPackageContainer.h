#pragma once
#include "UInc.h"
#include "UItemContainerEx.h"
class UPackageContainer : public UItemContainerEx
{
	UDEC_CLASS(UPackageContainer);
public:
	UPackageContainer();
	~UPackageContainer();
public:
	void  SetGridRow(int nRow);
protected:
	BOOL virtual OnCreate();
	void virtual OnDestroy();
	void virtual DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver);
	virtual void OnSize(const UPoint& NewSize);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void DrawMask(UPoint offset,UPoint cell);
	virtual void DrawItemQualityMask(UPoint offset, UPoint cell);

	virtual void OnShowTipsInfo();
	virtual void OnHideTipsInfo();
public:
	void ResetCanUseNob(int nub);
	UINT GetCanUseNob(){return m_CanUseNob;}
private:
	USkinPtr m_CellSkin;
	USkinPtr m_ItemQuality;
	UString  m_CellSkinStr;
	UTexturePtr m_MaskTexture;
    //当前可用的包裹数目
	UINT m_CanUseNob;
};