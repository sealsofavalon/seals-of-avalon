#include "stdafx.h"
#include "UPackageDialog.h"


UIMP_CLASS(UPackageDialog, UDialog);
void UPackageDialog::StaticInit()
{
	UREG_PROPERTY("headRec",UPT_RECT,UFIELD_OFFSET(UPackageDialog,m_HeadRect));
	UREG_PROPERTY("leftbordRec",UPT_RECT,UFIELD_OFFSET(UPackageDialog,m_LeftRect));
	UREG_PROPERTY("rightbordRec",UPT_RECT,UFIELD_OFFSET(UPackageDialog,m_RightRect));
	UREG_PROPERTY("centerRec",UPT_RECT,UFIELD_OFFSET(UPackageDialog,m_CenterRect));
	UREG_PROPERTY("bottomRec",UPT_RECT,UFIELD_OFFSET(UPackageDialog,m_BottomRect));
}

UPackageDialog::UPackageDialog()
{
	m_pOldSize = UPoint(0,0);
	m_pNeedTance = TRUE ;
}
UPackageDialog::~UPackageDialog()
{

}

BOOL UPackageDialog::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	

	SetRenderRect();//由于每次重新选择人物后， 这个类的所有的界面都重新从文件里加载了一次， 所以这里不需要硬编码设定区域。

	m_pOldSize.Set(m_Size.x,m_Size.y);
	return TRUE;

}

void UPackageDialog::OnDestroy()
{
	SetRenderRect();
	UDialog::OnDestroy();
}
void UPackageDialog::SetRenderRect()
{
	
}

void UPackageDialog::OnSize(const UPoint& NewSize)
{
	
	if (m_pNeedTance)
	{
		// 如果需要变换的话, 针对前面的size 和后面的size变换渲染区域. 
		// 以便适应不同的分辨率下的显示信息.
		m_HeadRect.TransformRect(m_pOldSize,NewSize);
		m_LeftRect.TransformRect(m_pOldSize,NewSize);
		m_RightRect.TransformRect(m_pOldSize,NewSize);
		m_CenterRect.TransformRect(m_pOldSize,NewSize);
		m_BottomRect.TransformRect(m_pOldSize,NewSize);

		ULOG( _TRAN("Resolution, backpacks and warehouse conversion area") );
	}
	

	m_pOldSize.x = NewSize.x ;
	m_pOldSize.y = NewSize.y ;

	m_pNeedTance = TRUE;

	UControl::OnSize(NewSize);
}
void UPackageDialog::OnRender(const UPoint& offset, const URect &updateRect)
{

	URect pRect = updateRect;

	if (m_spBkgSkin && m_spBkgSkin->GetItemNum() > centerBgk)
	{  	
		sm_UiRender->DrawSkin(m_spBkgSkin,head_top,WinRectToScreenRect(m_HeadRect,pRect));
		sm_UiRender->DrawSkin(m_spBkgSkin,bottom,WinRectToScreenRect(m_BottomRect,pRect));

		sm_UiRender->DrawSkin(m_spBkgSkin,left_center,WinRectToScreenRect(m_LeftRect,pRect));
		sm_UiRender->DrawSkin(m_spBkgSkin,right_center,WinRectToScreenRect(m_RightRect,pRect));
		sm_UiRender->DrawSkin(m_spBkgSkin,centerBgk,WinRectToScreenRect(m_CenterRect,pRect));
	}

	RenderChildWindow(offset, updateRect);
}

URect UPackageDialog::WinRectToScreenRect(URect childRect, URect parantRect)
{
	UPoint NewPos;
	UPoint NewSize;

	NewPos.x = parantRect.GetPosition().x + childRect.GetPosition().x;
	NewPos.y = parantRect.GetPosition().y + childRect.GetPosition().y;

	NewSize = childRect.GetSize();

	return URect(NewPos,NewSize);
}

void UPackageDialog::ToAddHeight(int newH)
{
	URect CloseBtnRec =  m_CloseBtn->GetControlRect();
	m_pNeedTance = FALSE; 

	SetHeight(GetHeight() + newH);

	m_LeftRect.SetHeight(float(m_LeftRect.GetHeight() + newH));
	m_CenterRect.SetHeight(float(m_CenterRect.GetHeight() + newH));
	m_RightRect.SetHeight(float(m_RightRect.GetHeight() + newH));

	char buf[_MAX_PATH];
	NiSprintf(buf, _MAX_PATH, "BAG(BANK)变换前底部高度 %d, %d 位置", m_BottomRect.GetPosition().x, m_BottomRect.GetPosition().y);
	ULOG(buf);

	UPoint Pos = m_BottomRect.GetPosition(); 
	m_BottomRect.SetPosition(Pos + UPoint(0,newH));

	if ((m_BottomRect.GetPosition().y) != (m_LeftRect.GetPosition().y + m_LeftRect.GetSize().y))
	{
		//为了防止异常的出现.这里需要重设对应的数据!
		ULOG( _TRAN("Abnormal") );
	}

	char bufa[_MAX_PATH];
	NiSprintf(bufa, _MAX_PATH, "BAG(BANK)变换后底部高度 %d, %d 位置", m_BottomRect.GetPosition().x, m_BottomRect.GetPosition().y);
	ULOG(bufa);

	//这里要重设定子窗口的位置. 使得其位置不变化. 因为整个窗体的大小变化, 如果不重设会导致不可预料的显示效果.
	m_CloseBtn->SetWindowRect(CloseBtnRec);

	// 这里上面已经设定好了渲染的区域, 这里分辨率没有改变. 所以不需要变化渲染的区域. 
	
}