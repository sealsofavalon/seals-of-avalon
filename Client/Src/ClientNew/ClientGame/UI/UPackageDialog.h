#pragma  once

#include "UInc.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////
///使用这个界面. 由于有特殊性,在每次调用ToAddHeight()函数大小改变的时候要重新设定所有子窗口的位置和大小///
//////////////////////////////////////////////////////////////////////////////////////////////////////////


class UPackageDialog : public UDialog
{
	UDEC_CLASS(UPackageDialog);
public:
	enum
	{
		head_top =0 ,
		left_center ,  //左边框
		right_center,  //右框架
		centerBgk,    //中间背景
		bottom,
	};
	UPackageDialog();
	~UPackageDialog();

	virtual void ToAddHeight(int newH);   //针对增加包裹格数的的UI相应函数. 
protected:
	BOOL virtual OnCreate();
	void virtual OnDestroy();
	void virtual OnRender(const UPoint& offset, const URect &updateRect);
	void virtual OnSize(const UPoint& NewSize);
	void virtual SetRenderRect();   //这里设定初始的UI区域......
private:
	//相对位置来或者绝对位置
	URect WinRectToScreenRect(URect childRect, URect parantRect);


protected:
	//
	URect m_HeadRect;        //头部区域
	URect m_LeftRect;        //左边框
	URect m_RightRect;       //右边框
	URect m_BottomRect;      //底部区域
	URect m_CenterRect;      //中间区域

	UPoint m_pOldSize ;  //存贮前面的size. 以便针对不同的分辨率下的变换
	BOOL m_pNeedTance;   //是否使用变化
};