#ifndef __UPAYDATA_H__
#define __UPAYDATA_H__

struct PayData
{
	PayData()
	{
		mMoney = 0;
		mYuanbao = 0;
		mItemId = 0;
		mItemCount = 0;
		mHonor = 0;
		mArena = 0;
		mGuid_Gongxian = 0;
		mGuid_Jifen = 0;

	}
	ui32 mMoney;      //钱的数量
	ui32 mYuanbao;    //元宝数量
	ui32 mItemId, mItemCount;  //物品的ID 和数量
	ui32 mHonor, mArena;  //荣誉值 和 竞技积分
	ui32 mGuid_Jifen, mGuid_Gongxian;  //部族积分  和  部族贡献值。

};

#endif