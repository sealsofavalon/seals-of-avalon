#include "stdafx.h"
#include "UPickItemSet.h"
#include "Network/PacketBuilder.h"
#include "ui/UIItemSystem.h"
#include "ItemManager.h"
#include "LocalPlayer.h"
#include "Network/PacketBuilder.h"
#include "ObjectManager.h"
#include "UIShowPlayer.h"
#include "UITipSystem.h"
#include "UFun.h"
#include "Effect/EffectManager.h"
#include "UIIndex.h"
#include "ui/ULoginLoading.h"
#include "ui/UISystem.h"

#define  RollTime 30.0   //默认ROLL的时间，过了这个时间就算放弃 

UIMP_CLASS(UTimeBar,UControl);
void UTimeBar::StaticInit()
{

}
UTimeBar::UTimeBar()
{
	m_pTexture = NULL;
	m_pkTimer = 0.0f;
	m_DisTime = 30.f;


	m_bBegin = FALSE;
}
UTimeBar::~UTimeBar()
{
	
}

BOOL UTimeBar::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	m_pTexture = sm_UiRender->LoadTexture("Data\\UI\\RollItem\\Time.png");
	if (m_pTexture == NULL)
	{
		return FALSE;
	}
	
	m_DisTime = RollTime ;
	return TRUE;
}
void UTimeBar::OnDestroy()
{
	UControl::OnDestroy();
	m_pTexture = NULL;
	m_pkTimer = 0.0f;
}
void UTimeBar::OnRender(const UPoint& offset,const URect &updateRect)
{	
	URect pRec = updateRect;
	if (m_pTexture)
	{
		float p = (m_DisTime - m_pkTimer) / m_DisTime ;

		URect BarRec;
		BarRec.SetPosition(offset);
		BarRec.SetSize(int(pRec.GetWidth() * p),pRec.GetHeight());

		URect ImageRec;
		ImageRec.SetPosition(0,0);
		ImageRec.SetSize(int(m_pTexture->GetWidth() * p), m_pTexture->GetHeight());

		sm_UiRender->DrawImage(m_pTexture,BarRec,ImageRec);
	}
}
void UTimeBar::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);

	if (m_pkTimer >= m_DisTime && m_bBegin && IsVisible())
	{
		//注册计时器消息
		DispatchNotify(UTIMEBAE_TIME_FINISH);
		m_pkTimer = 0.0f;
		m_bBegin = FALSE;
		SetVisible(FALSE);
	}
	if (m_bBegin && IsVisible())
	{
		m_pkTimer += fDeltaTime;
	}else
	{
		m_pkTimer = 0.0f;
	}
	

}




UIMP_CLASS(URollItem,UBitmap);
UBEGIN_MESSAGE_MAP(URollItem, UBitmap)
UON_UTIMEBAR_TIMEFINISH(3, &URollItem::OnCancelRoll)
UON_BN_CLICKED(0, &URollItem::OnClickRoll)
UON_BN_CLICKED(1, &URollItem::OnCancelRoll)
UEND_MESSAGE_MAP()

void URollItem::StaticInit()
{

}
URollItem::URollItem()
{
	 m_RollItem = NULL;
	 m_pText = NULL;
	 m_pTimeBar = NULL;

	 m_pkRollItemList.clear();
	 
}
URollItem::~URollItem()
{

}

BOOL URollItem::OnCreate()
{
	if (!UBitmap::OnCreate())
	{
		return FALSE;
	}

	m_RollItem = (USkillButton*)GetChildByID(2);
	if (m_RollItem == NULL)
	{
		return FALSE;
	}

	USkillButton::DataInfo pData ;
	pData.id = 0;
	pData.num = 0;
	pData.pos = 0;
	pData.type = UItemSystem::ITEM_DATA_FLAG;

	m_RollItem->SetStateData(&pData);


	m_pText = (UStaticText*)GetChildByID(4);
	if (m_pText == NULL)
	{
		return FALSE;
	}


	m_pTimeBar = (UTimeBar*)GetChildByID(3);
	if (m_pTimeBar == NULL)
	{
		return FALSE;
	}

	return TRUE ;
}
void URollItem::OnDestroy()
{
	UBitmap::OnDestroy();

	m_RollItem = NULL;
	m_pText = NULL;
	m_pTimeBar = NULL;
}
void URollItem::StartRollItem()
{
	if (m_pkRollItemList.size())
	{
		ItemPrototype_Client* pItemInfo =  ItemMgr->GetItemPropertyFromDataBase(m_pkRollItemList[0].itemid);
		
		assert(pItemInfo);

		USkillButton::DataInfo pData ;
		pData.id = m_pkRollItemList[0].itemid;
		pData.num = 1;
		pData.pos = m_pkRollItemList[0].x; 
		pData.type = UItemSystem::ITEM_DATA_FLAG;

		m_RollItem->SetStateData(&pData);
		m_pText->SetText(pItemInfo->C_name.c_str());

		UColor _color(0,0,0,0) ;//= TipSystem->GetColor(pItemInfo->Quality);

		GetQualityColor(pItemInfo->Quality, _color);
		
		m_pText->SetTextColor(_color);


		SetVisible(TRUE);
		m_pTimeBar->BeginOrEnd(TRUE);
		m_pTimeBar->SetRollTime(m_pkRollItemList[0].countdown / 1000.0f);
		

		
	}else
	{
		ClearUI();
	}
}
void URollItem::AddRollItem(MSG_S2C::stMsg_Loot_Start_Roll RollItem)
{
	MSG_S2C::stMsg_Loot_Start_Roll pkRoll ;

	pkRoll.itemid = RollItem.itemid;
	pkRoll.loot_guid = RollItem.loot_guid;
	pkRoll.factor = RollItem.factor;
	pkRoll.RandomProperty = RollItem.RandomProperty;
	pkRoll.x =RollItem.x;
	pkRoll.countdown = RollItem.countdown;
	pkRoll.mapid = RollItem.mapid ;
	pkRoll.instanceid = RollItem.instanceid;

	BOOL bNeed = TRUE;
	if (m_pkRollItemList.size() > 0)
	{
		bNeed = FALSE;
	}

	m_pkRollItemList.push_back(pkRoll);
	
	//如果m_pkRollItemList在push_back前里面有需要ROLL的道具， 说明了界面正在处理数据。
	if (bNeed)
	{
		StartRollItem();
	}
}
void URollItem::OnClickRoll()						// roll
{
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	assert(pLocal);
    PacketBuilder->SendStartRoll(m_pkRollItemList[0].loot_guid,m_pkRollItemList[0].x,1,m_pkRollItemList[0].mapid,m_pkRollItemList[0].instanceid);

	m_pkRollItemList.erase(m_pkRollItemList.begin());	
	m_pTimeBar->BeginOrEnd(FALSE);   //设置计时停止

	StartRollItem();

	ULOG("ROll ITEN");
}
void URollItem::OnCancelRoll()
{
	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	assert(pLocal);
	PacketBuilder->SendStartRoll(m_pkRollItemList[0].loot_guid,m_pkRollItemList[0].x,128,m_pkRollItemList[0].mapid,m_pkRollItemList[0].instanceid);

	m_pkRollItemList.erase(m_pkRollItemList.begin());
	m_pTimeBar->BeginOrEnd(FALSE);

	StartRollItem();
	ULOG("Cancel Roll ItEM");
	
}
void URollItem::ClearUI()
{
	USkillButton::DataInfo pData ;
	pData.id = 0;
	pData.num = 0;
	pData.pos = 0;
	pData.type = UItemSystem::ITEM_DATA_FLAG;

	m_RollItem->SetStateData(&pData);

	SetVisible(FALSE);
	m_pText->Clear();
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UTeamPickSet,UDialog);
UBEGIN_MESSAGE_MAP(UTeamPickSet, UDialog)
UON_BN_CLICKED(2, &UTeamPickSet::OnClickSet)
UON_BN_CLICKED(3, &UTeamPickSet::OnClickCancel)
UEND_MESSAGE_MAP()


void UTeamPickSet::StaticInit()
{

}
UTeamPickSet::UTeamPickSet()
{
	 m_PickSet = PARTY_LOOT_FFA;						//默认自由拾取
	 m_ItemQuality = ITEM_QUALITY_UNCOMMON_GREEN;		//默认品质为优秀
	                            //是否是队长

	 m_PartyLootSet = NULL;
	 m_QulitySet = NULL;
	m_IsUpdateChild = FALSE;
}
UTeamPickSet::~UTeamPickSet()
{

}
BOOL UTeamPickSet::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	m_PartyLootSet = UDynamicCast(UComboBox, GetChildByID(0));
	m_QulitySet = UDynamicCast(UComboBox, GetChildByID(1));

	if (m_QulitySet == NULL || m_PartyLootSet == NULL)
	{
		return FALSE;
	}
	
	UINT* LOOT_FFA = new UINT ;
	*LOOT_FFA = PARTY_LOOT_FFA;

	UINT* LOOT_GROUP = new UINT;
	*LOOT_GROUP = PARTY_LOOT_GROUP;

	m_PartyLootSet->AddItem( _TRAN("自由拾取"), LOOT_FFA);   //0
	//m_PartyLootSet->AddItem("轮流拾取");   //1
	//m_PartyLootSet->AddItem("队长分配");   //2
	m_PartyLootSet->AddItem( _TRAN("队伍分配"), LOOT_GROUP);   //3
	//m_PartyLootSet->AddItem("按需分配");   //4

	m_PartyLootSet->SetCurSel(1);


	//ITEM_QUALITY* QWhite = new ITEM_QUALITY;
	//*QWhite = ITEM_QUALITY_NORMAL_WHITE;

	ITEM_QUALITY* QGreen = new ITEM_QUALITY;
	*QGreen = ITEM_QUALITY_UNCOMMON_GREEN;

	ITEM_QUALITY* QBlue = new ITEM_QUALITY;
	*QBlue = ITEM_QUALITY_RARE_BLUE;

	ITEM_QUALITY* QPurple = new ITEM_QUALITY;
	*QPurple = ITEM_QUALITY_EPIC_PURPLE;

	ITEM_QUALITY* QOrange = new ITEM_QUALITY;
	*QOrange = ITEM_QUALITY_LEGENDARY_ORANGE;


//	m_QulitySet->AddItem("普通",QWhite);  
	m_QulitySet->AddItem( _TRAN("优秀"),QGreen);  
	m_QulitySet->AddItem( _TRAN("精良"),QBlue);  
	m_QulitySet->AddItem( _TRAN("史诗"),QPurple);  
	m_QulitySet->AddItem( _TRAN("传说"),QOrange);  

	m_QulitySet->SetCurSel(0); //QGreen

	//SetVisible(FALSE);

	return TRUE;
}
void UTeamPickSet::OnDestroy()
{
	for (int i = 0; i < m_QulitySet->GetCount(); i++)
	{
		ITEM_QUALITY* pData = (ITEM_QUALITY*)m_QulitySet->GetItemData(i);
		if (pData)
		{
			delete pData;
			pData = NULL;
		}
	}

	for(int i = 0; i < m_PartyLootSet->GetCount(); i++)
	{
		UINT* pData = (UINT*)m_PartyLootSet->GetItemData(i);
		if (pData)
		{
			delete pData;
			pData = NULL;
		}
	}
	m_PartyLootSet->Clear();
	m_QulitySet->Clear();

	UDialog::OnDestroy();
}

void UTeamPickSet::SetTeamPick(UINT pickset, ITEM_QUALITY Quality)
{
	if (pickset >= PARTY_LOOT_FFA && pickset <= PARTY_LOOT_NBG && Quality >ITEM_QUALITY_POOR_GREY && Quality <= ITEM_QUALITY_LEGENDARY_ORANGE)
	{
		m_PickSet = pickset;

		char pBUf[_MAX_PATH];
		char qBUF[_MAX_PATH];

		for(int i = 0; i < m_PartyLootSet->GetCount(); i++)
		{
			UINT pkSet = *(UINT*)m_PartyLootSet->GetItemData(i);
			if (pkSet == m_PickSet)
			{
				m_PartyLootSet->SetCurSel(i);
				m_PartyLootSet->GetItemText(i,pBUf,_MAX_PATH);
			}
		}
	
		m_ItemQuality = Quality;
		for (int i= 0; i < m_QulitySet->GetCount(); i++)
		{
			ITEM_QUALITY p = *(ITEM_QUALITY*)m_QulitySet->GetItemData(i);
			if (p == Quality)
			{
				m_QulitySet->SetCurSel(i);
				m_QulitySet->GetItemText(i, qBUF, _MAX_PATH);
				break;
			}
		}
	
		std::string buf = _TRAN( N_NOTICE_C31,pBUf, qBUF );

		UControl * loading = SYState()->UI->LoadDialogEX(DID_LOADING);
		if (loading)
		{
			ULoadingFrame* pLoadingFrame = UDynamicCast(ULoadingFrame, loading);
		
			if (!pLoadingFrame->IsLoading())
			{
				EffectMgr->AddEffeText(NiPoint3(300.f, 300.f, 0.0f), buf.c_str());
			}
		}
		ClientSystemNotify(buf.c_str());
	}
}
void UTeamPickSet::OnClickSet()
{
	if (m_PartyLootSet->GetCurSel() == -1 && m_QulitySet->GetCurSel() == -1)
	{
		return ;
	}
	//m_PickSet = m_PartyLootSet->GetCurSel();
	UINT index = m_QulitySet->GetCurSel();
	UINT pIndex = m_PartyLootSet->GetCurSel();
	//m_ItemQuality = *(ITEM_QUALITY*)m_QulitySet->GetItemData(index);

	CPlayerLocal* pLocal = ObjectMgr->GetLocalPlayer();
	
	if (pLocal && TeamShow->IsTeamLeader(pLocal->GetGUID())) 
	{
		PacketBuilder->SendTeamPickSetting(*(UINT*)m_PartyLootSet->GetItemData(pIndex), *(ITEM_QUALITY*)m_QulitySet->GetItemData(index));
	}
	
	SetVisible(FALSE);

}
void UTeamPickSet::OnClickCancel()	
{
	SetVisible(FALSE);
}