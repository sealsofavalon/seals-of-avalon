#ifndef  __UPICKITEMSET_H__ 
#define  __UPICKITEMSET_H__

#include "UInc.h"
#include "USkillButton.h"
#include "../../../../SDBase/Protocol/S2C_Loot.h"


#define  TEAMROLLITEM     INGAMEGETFRAME(URollItem, FRAME_IG_ROLLITEM)
#define  TEAMPICKITEMSET  INGAMEGETFRAME(UTeamPickSet, FRAME_IG_TEAMPICKSETTING)



// 计时器
class UTimeBar : public UControl
{
	UDEC_CLASS(UTimeBar);
public:
	UTimeBar();
	~UTimeBar();

	void BeginOrEnd(BOOL bBegin){m_bBegin = bBegin;}
	void SetRollTime(float time){m_DisTime = time;}
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
protected:
	UTexturePtr m_pTexture;
	float m_pkTimer;
	float m_DisTime;
	BOOL  m_bBegin;  //是否开始计时。
};

class  URollItem : public UBitmap
{
	UDEC_CLASS(URollItem);
	UDEC_MESSAGEMAP();
public:
	URollItem();
	~URollItem();
	void AddRollItem(MSG_S2C::stMsg_Loot_Start_Roll pRollItem);
	void StartRollItem();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
protected:
	void OnClickRoll();						// roll
	void OnCancelRoll();					//取消 roll
	void ClearUI();
protected:
	USkillButton* m_RollItem ;
	UStaticText* m_pText;						//Roll的 物品的GUID . 用来判断相关的Item信息
	UTimeBar* m_pTimeBar;
	vector<MSG_S2C::stMsg_Loot_Start_Roll> m_pkRollItemList ;

};

class UTeamPickSet : public UDialog
{
	UDEC_CLASS(UTeamPickSet);
	UDEC_MESSAGEMAP();
public:
	UTeamPickSet();
	~UTeamPickSet();
	void OnClickSet();									//设置。
	void OnClickCancel();	//取消设置

	void SetTeamPick(UINT pickset, ITEM_QUALITY Quality);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
private:
	//#define PARTY_LOOT_FFA	  0 自由拾取
	//#define PARTY_LOOT_MASTER   2 队长分配
	//#define PARTY_LOOT_RR	      1 轮流拾取
	//#define PARTY_LOOT_NBG	  4 按需分配
	//#define PARTY_LOOT_GROUP	  3 队伍分配
	UINT m_PickSet ;									//上面分配的类型。
	ITEM_QUALITY m_ItemQuality ;						//执行分配制度的物品品质  


	UComboBox* m_PartyLootSet ;
	UComboBox* m_QulitySet;

};
#endif