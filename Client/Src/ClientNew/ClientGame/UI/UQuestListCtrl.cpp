#include "StdAfx.h"
#include "UQuestListCtrl.h"
#include "UISystem.h"
#include "UIGamePlay.h"

UIMP_CLASS(UQuestListCtrl, UControl);

UBEGIN_MESSAGE_MAP(UQuestListCtrl, UControl)
UON_RED_URL_CLICKED(0xFFFF, &UQuestListCtrl::OnURLClicked)
UEND_MESSAGE_MAP()

void UQuestListCtrl::StaticInit()
{

}

UQuestListCtrl::UQuestListCtrl(void):m_pQuestDesc(NULL)
{
}

UQuestListCtrl::~UQuestListCtrl(void)
{
}


BOOL UQuestListCtrl::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	
	m_pQuestDesc = (URichTextCtrl*)URichTextCtrl::CreateObject();
	m_pQuestDesc->SetId(0xFFFF);
	m_pQuestDesc->SetControlStyle(m_Style);
	m_pQuestDesc->SetPosition(UPoint(0,0));
	m_pQuestDesc->SetSize(m_Size);
	AddChild(m_pQuestDesc);
	return TRUE;
}

void UQuestListCtrl::OnDestroy()
{
	UControl::OnDestroy();
}

void UQuestListCtrl::OnURLClicked(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;

	UInGame* pkInGame = (UInGame*)SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pkInGame)
	{
		pkInGame->GetUQuestDlg()->SendClickMessage(pREDURLNM->pszURL);
	}
}