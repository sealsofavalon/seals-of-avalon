#pragma once
#include "UInc.h"

class UQuestListCtrl : public UControl
{
	UDEC_CLASS(UQuestListCtrl);
	UDEC_MESSAGEMAP();
public:
	UQuestListCtrl(void);
	~UQuestListCtrl(void);
	URichTextCtrl*  GetRichText(){return m_pQuestDesc;}
	
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
private:
	void OnURLClicked(UNM* pNM);
protected:

	URichTextCtrl* m_pQuestDesc;		// ��������
};
