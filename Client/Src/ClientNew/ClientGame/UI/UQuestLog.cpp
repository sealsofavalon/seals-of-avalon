#include "stdafx.h"
#include "UQuestLog.h"
#include "../Utils/QuestDB.h"
#include "../Network/NetworkManager.h"
#include "Network/PacketBuilder.h"
#include "../ItemManager.h"
#include "../LocalPlayer.h"
#include "../ObjectManager.h"
#include "UISystem.h"
#include "UInGameBar.h"
#include "UIGamePlay.h"
#include "UBitmapButtonEx.h"
#include "UFun.h"
#include "UMessageBox.h"
#include "Map/PathNode.h"
#include "Map/Map.h"
#include "Utils/MapToMapPathInfo.h"
#include "Utils/QuestFinisherDB.h"
#include "Utils/CreatureSpawnsDB.h"
#include "ui/UIMiniMap.h"
#include "ui/UChat.h"
#include "UInstanceMgr.h"
#include "UITipSystem.h"
#include "Utils/QuestStarterDB.h"


void OnURLItemOn(UNM* pNM)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;

	// 任务点击寻路
	if(strURL.find("#NPC") != -1 || strURL.find("#MONSTER") != -1)
	{
		// 寻路
		return ;
	}
	int pos = strURL.find("#_CHOOSEITEMICON_") ;
	int len = strlen("#_CHOOSEITEMICON_");
	if (pos == -1)
	{
		pos = strURL.find("#_REWITEM_");
		len = strlen("#_REWITEM_");
	}
	if (pos == -1)
	{
		pos = strURL.find("#_QUESTITEM_");
		len = strlen("#_QUESTITEM_");
	}

	if (pos != -1)
	{
		string strdata = strURL.substr(pos + len , strURL.length()) ;

		int entryPos = strdata.find("_");

		string entrystr = strdata.substr(0, entryPos);
		string countstr = strdata.substr(entryPos+1, strdata.length() - entryPos);

		UPoint TipsPos = UControl::sm_System->GetCursorPos();
		ui32 entry = atoi(entrystr.c_str());
		ui32 count = atoi(countstr.c_str());
		TipSystem->ShowTip(TIP_TYPE_ITEM, entry, TipsPos,0, count);
	}
}
void OnURLItemLeave()
{
	TipSystem->HideTip();
}
void OnURMoveMap(UNM* pNM)
{
	CPlayerLocal* pkLcoalPlayer = ObjectMgr->GetLocalPlayer();
	if (!pkLcoalPlayer)
	{
		return ;
	}
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;


	// 任务点击寻路
	if(strURL.find("#NPC") != -1 || strURL.find("#MONSTER") != -1)
	{
		NIASSERT(strURL.size() > 9);

		int iMapId = 0;
		int iPosX, iPosY, iPosZ;

		NiPoint3 kPos = NiPoint3::ZERO ;

		int iTmp0, iTmp1;
		iTmp0 = strURL.find_first_of(" ");
		iTmp1 = strURL.find(" ", iTmp0 + 1);
		iMapId = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());

		iTmp0 = strURL.find(" ", iTmp1);
		//iTmp0 = strURL.find_first_of(" ");
		iTmp1 = strURL.find(" ", iTmp0 + 1);
		iPosX = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());

		iTmp0 = strURL.find(" ", iTmp1);
		iTmp1 = strURL.find(" ", iTmp0 + 1);
		iPosY = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());

		iTmp0 = strURL.find(" ", iTmp1);
		iTmp1 = strURL.find(" ", iTmp0 + 1);
		iPosZ = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());



		kPos = NiPoint3(iPosX, iPosY, iPosZ);
		pkLcoalPlayer->MoveToMapAStar(iMapId, kPos, 0);

	}

	int pos = strURL.find("#_QUESTITEM_") ;
	if (pos != -1)
	{
		int len = strlen("#_QUESTITEM_");
		string strdata = strURL.substr(pos + len , strURL.length()) ;
		int entryPos = strdata.find("_");
		ui32 entry = atoi(strdata.substr(0, entryPos).c_str());
		ui32 count = atoi(strdata.substr(entryPos+1, strdata.length() - entryPos).c_str());

		ui32 map;
		NiPoint3 pos;
		if (GetItemFindPath(entry,map,pos))
			pkLcoalPlayer->MoveToMapAStar(map, pos, 0);
	}
}
UIMP_CLASS(UQuestList,UControl);
UBEGIN_MESSAGE_MAP(UQuestList,UControl)
//UON_UGD_DBCLICKED(3, &UQuestList::QuestContent)
UON_UGD_CLICKED(3, &UQuestList::QuestContent)
UON_BN_CLICKED(0,&UQuestList::OnClickDrop)
UON_BN_CLICKED(1,&UQuestList::OnClickShare)
UON_BN_CLICKED(6, &UQuestList::OnClickCancel)
UON_BN_CLICKED(3, &UQuestList::OnTraceQuest)
UON_UGD_SHIFT_AND_RBCLICKED(3, &UQuestList::OnSendQuest)
UEND_MESSAGE_MAP()
void UQuestList::StaticInit(){}

UQuestList::UQuestList()
{
	//m_QuestLog = NULL;
	m_QuestList = NULL;

	m_bAutoQuest = TRUE;
	m_IsUpdateChild = FALSE;
}
UQuestList::~UQuestList()
{
	//m_QuestLog = NULL;
	m_QuestList = NULL;
}
BOOL UQuestList::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	UScrollBar* pQuestBar = (UScrollBar*)GetChildByID(2);
	if (pQuestBar == NULL)
	{
		return FALSE;
	}
	pQuestBar->SetBarShowMode(USBSM_FORCEON,USBSM_FORCEOFF);

	m_QuestList = (USpecListBox*)pQuestBar->GetChildByID(3);
	if (m_QuestList == NULL)
	{
		return FALSE;
	}

	m_QuestList->SetGridSize(UPoint(285 ,19));
	m_QuestList->SetGridCount(19, 1);
	m_QuestList->SetCol1(0 + 6 + 12, 0);
	m_QuestList->SetWidthCol1(120 - 12 + 25);
	m_QuestList->SetCol2(120 + 6 + 20 + 5 + 40 + 25, 0);
	m_QuestList->SetWidthCol2(91 - 20 - 5 - 40/* + 18*/);
	m_QuestList->SetCheckBoxBitmap("Public\\CheckButton2.skin");
	m_QuestList->SetTreeConBitmap("Public\\JiaJianButton.skin");

	//if (m_QuestLog == NULL)
	//{
	//	m_QuestLog = INGAMEGETFRAME(UQuestLog, FRAME_IG_QUESTLOGDLG);
	//	if (!m_QuestLog)
	//	{
	//		return FALSE;
	//	}
	//}
	return TRUE;
}

void UQuestList::OnDestroy()
{
	if (m_QuestList)
	{
		m_QuestList->Clear();
	}
	//m_vQuestInfo.clear();
	m_vMainQuestInfo.clear();
	m_vFanisheQuestInfo.clear();
	m_QuestFinishTem.clear();
	UQuestTrace* pQuestTraceDlg = INGAMEGETFRAME(UQuestTrace, FRAME_IG_QUESTTRACE);
	if(pQuestTraceDlg)
	{
		pQuestTraceDlg->ClearAll();
	}
	UControl::OnDestroy();
}

void UQuestList::OnClickDrop()
{
	int uiId = m_QuestList->GetSelId();
	if(uiId != -1)
	{
		stUIQuestInfo stQuest ;
		if (FindQuestByID(uiId, stQuest) && stQuest.uiQuestSlotId != 0xff)
		{
			PacketBuilder->SendQuestLogRemoveQuestMsg(stQuest.uiQuestSlotId);
		}
	}
}

void UQuestList::OnClickShare()
{
	int uiId = m_QuestList->GetSelId();
	if(uiId != -1)
	{
		stUIQuestInfo stQuest ;
		if (FindQuestByID(uiId, stQuest))
		{
			PacketBuilder->SendQuestPushToPartyMsg(uiId);
		}
	}
}

void UQuestList::OnClickCancel()
{
	//OnClose();
}
void UQuestList::UpdataHonor()
{
	UQuestTrace* pQuestTraceDlg = INGAMEGETFRAME(UQuestTrace, FRAME_IG_QUESTTRACE);
	if (pQuestTraceDlg)
	{
		pQuestTraceDlg->UpdataHonor();
	}
}
void UQuestList::UpdateTraceQuest(ui32 Questid)
{
	UQuestTrace* pQuestTraceDlg = INGAMEGETFRAME(UQuestTrace, FRAME_IG_QUESTTRACE);
	UQuestDlg* pQuestDlg = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
	if(pQuestTraceDlg && pQuestDlg)
	{
		stUIQuestInfo stQuest ;
		if (FindQuestByID(Questid,stQuest))
		{
			//如果状态改变为未激活或者完成状态，那么立即完成任务
			if (stQuest.uiQuestState == QMGR_QUEST_NOT_AVAILABLE ||
				stQuest.uiQuestState == QMGR_QUEST_FINISHED + 1)
			{
				pQuestTraceDlg->DeleteText(Questid);
				pQuestDlg->GetQuestLog()->ClearAllTextContent();
				m_QuestList->SetCheckState(Questid,false);
				return ;
			}

			bool HasInShow = pQuestTraceDlg->IsHasQuest(Questid);
			if (HasInShow)
			{
				pQuestTraceDlg->UpdateText(stQuest);
			}else
			{
				if (pQuestTraceDlg->GetCurTraceCount() < 5 && m_bAutoQuest)
				{
					m_QuestList->SetCheckState(Questid, true);
					SetTraceQuest(Questid);
				}
			}
			pQuestDlg->GetQuestLog()->UpdataQuestContent(stQuest);
		}
	}

}
void UQuestList::SetTraceQuest(ui32 Questid)
{
	UQuestTrace* pQuestTraceDlg = INGAMEGETFRAME(UQuestTrace, FRAME_IG_QUESTTRACE);
	if(pQuestTraceDlg)
	{
		stUIQuestInfo stQuest ;
		if (FindQuestByID(Questid,stQuest))
		{
			int bCheck = m_QuestList->GetCheckStateById(Questid);
			bool HasInShow = pQuestTraceDlg->IsHasQuest(Questid);
			if (HasInShow)
			{
				if(bCheck)
				{
					pQuestTraceDlg->UpdateText(stQuest);
				}else
				{
					pQuestTraceDlg->DeleteText(stQuest.uiQuestId);
				}
			}else
			{
				if(pQuestTraceDlg->GetCurTraceCount() <= 4)
				{
					pQuestTraceDlg->AddText(stQuest);
				}else
				{
					ui32 pkLast = pQuestTraceDlg->GetLastQuestId();
					pQuestTraceDlg->DeleteText(pkLast);
					m_QuestList->SetCheckState(pkLast, false);
					pQuestTraceDlg->AddText(stQuest);
				}
			}
			
		}
	}
}

void UQuestList::OnTraceQuest()
{
	UQuestTrace* pQuestTraceDlg = INGAMEGETFRAME(UQuestTrace, FRAME_IG_QUESTTRACE);
	if(pQuestTraceDlg)
	{
		int Questid = m_QuestList->GetSelId();
		if (Questid)
		{
			SetTraceQuest(Questid);
		}	
	}
}
void UQuestList::OnSendQuest()
{
	int sel = m_QuestList->GetCurSel();
	if (sel >= 0)
	{
		int Questid = m_QuestList->GetSelId();
		ChatSystem->AddQuestMsg(Questid);
	}
}
string UQuestList::QuestoString(const stUIQuestInfo& uiQuest)
{
	std::string strState = "";   //状态
	char sTemp[_MAX_PATH];
	CQuestDB::stQuest* QuestInfo = g_pkQusetInfo->GetQuestInfo(uiQuest.uiQuestId);
	string strTitle = QuestInfo->strTitle; //标题
	string strColor = "R106G054B000A255";

	if(strTitle.size() > 25)
	{
		strTitle.substr(0, 24);
		strTitle += ".";
	}

	switch(uiQuest.uiQuestState)
	{
	case QMGR_QUEST_NOT_AVAILABLE:
		strState = _TRAN("未激活");
		strColor = "R125G125B125A255";
		break;
	case QMGR_QUEST_AVAILABLE:
	case QMGR_QUEST_REPEATABLE:
		strState = _TRAN("可接受");
		strColor = "R240G130B021A255";
		break;
	case QMGR_QUEST_FINISHED:
		strState = _TRAN("可交付");
		strColor = "R205G020B064A255";
		break;
	case QMGR_QUEST_REPEATABLE_FINISHED:
		strState = _TRAN("可交付");
		strColor = "R205G020B064A255";
		break;
	case QMGR_QUEST_NOT_FINISHED:
		strState = _TRAN("进行中");
		if (QuestInfo->uiIsMainQuest)
		{
			strColor = "R043G047B099A255";
		}else
		{
			strColor = "R060G106B217A255";
		}
		break;
	default:
		if (QuestInfo->uiIsMainQuest)
		{
			strColor = "R121G066B088A255";
		}else
		{
			strColor = "R106G054B000A255";
		}
		strState = _TRAN("已完成");
		
		break;
	}
	NiSprintf(sTemp, _MAX_PATH, "<color:%s><Col1><center>%s<endnc><Col2><center>%s<endnc>", 
		(char*)strColor.c_str(),
		(char*)strTitle.c_str(), 
		(char*)strState.c_str());

	return sTemp;
}
void UQuestList::QuestContent(unsigned int QuestID)
{
	UQuestDlg* pQuestDlg = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
	if (pQuestDlg->GetQuestLog())
	{
		pQuestDlg->GetQuestLog()->ClearAllTextContent();
		pQuestDlg->GetQuestLog()->SetVisible(TRUE);
		SetPosition(GetWindowPos());
	}

	stUIQuestInfo stQuestInfo;
	if (FindQuestByID(QuestID,stQuestInfo))
	{
		//m_QuestLog->OnQuestContent(stQuestInfo);
		m_QuestList->SetCurSelByID(QuestID);
	}
}
void UQuestList::QuestContent()
{
	UQuestDlg* pQuestDlg = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
	if (pQuestDlg->GetQuestLog())
	{
		pQuestDlg->GetQuestLog()->ClearAllTextContent();
		pQuestDlg->GetQuestLog()->SetVisible(TRUE);
		SetPosition(GetWindowPos());
		if (m_QuestList->GetItemCount() != 0 && m_QuestList->GetCurSel() != -1)
		{
			int uiId = m_QuestList->GetSelId();
			if(uiId != -1)
			{
				stUIQuestInfo stQuestInfo;
				if (FindQuestByID(uiId,stQuestInfo))
				{
					pQuestDlg->GetQuestLog()->OnQuestContent(stQuestInfo);
					//SetTraceQuest(stQuestInfo.uiQuestId);
				}
			}
		}
	}
}

bool UQuestList::FindChildQuestList(vector<stUIQuestInfo>& MainChildQuestList, vector<stUIQuestInfo>& FinishChildQuestList)
{
	//需要先判断主线任务的所有支线任务是不是都在已经完成的支线任务列表里。
	//如果在的话，我们把这些支线任务从完成支线任务列表移除，
	//这样避免我们在添加支线任务的时候找不到主线任务而出现报错.
	//如果不在的话,那么就需要把主线任务列表添加进UI. 

	int Count = 0;

	if (MainChildQuestList.size() == 0)
	{
		return true;
	}
	for (int i = 0; i < MainChildQuestList.size(); i++)
	{
		bool bFind = false ;
		for (int j =0 ; j < FinishChildQuestList.size(); j++)
		{
			if (MainChildQuestList[i].uiQuestId == FinishChildQuestList[j].uiQuestId)
			{
				MainChildQuestList[i].uiQuestState = QMGR_QUEST_FINISHED + 1;
				bFind = true;
				break;
			}
		}

		if (bFind)
		{
			Count++;
		}
	}

	if (Count == MainChildQuestList.size())
	{
		//移除
		for (int i = 0; i < MainChildQuestList.size(); i++)
		{
			
			for (int j =0 ; j < FinishChildQuestList.size(); j++)
			{
				if (MainChildQuestList[i].uiQuestId == FinishChildQuestList[j].uiQuestId)
				{
					FinishChildQuestList.erase(FinishChildQuestList.begin() + j);
					break;
				}
			}
		}
		return true;
	}

	return false;
}
void UQuestList::PareseFinishQuest(MSG_S2C::stFinishQuests& Msg)
{
	m_QuestFinishTem.clear();
	for(set<uint32>::const_iterator it = Msg.quests.begin(); it != Msg.quests.end(); ++it)
	{
		if((*it) == 0)
			continue;

		m_QuestFinishTem.push_back(*it);
	}
}
void UQuestList::OrderFinishQuestTem()
{
	CPlayerLocal * pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return;
	}
	//第一次发下来的完成任务列表，这里要先对他进行处理，
	//对主线任务先进行查找插入UI
	//然后更新完成支线任务的状态
	//如果所有支线任务完成,则Ui不进行显示

	vector<stUIQuestInfo> TemMainList;
	vector<stUIQuestInfo> TemChildList;
	for(int i = 0;  i < m_QuestFinishTem.size(); i++)
	{
		stUIQuestInfo stTemp;
		stTemp.uiQuestId = m_QuestFinishTem[i];
		stTemp.uiQuestSlotId = 0xff;
		stTemp.uiQuestState = QMGR_QUEST_FINISHED + 1;
		CQuestDB::stQuest* QuestInfo = g_pkQusetInfo->GetQuestInfo(stTemp.uiQuestId);

		if (QuestInfo)
		{
			if (QuestInfo->uiIsMainQuest)
			{
				TemMainList.push_back(stTemp);
			}else
			{
				TemChildList.push_back(stTemp);
			}
		}
	}

	m_QuestFinishTem.clear();

	for (int i = 0; i < TemMainList.size(); i++)
	{
		//AddMainQuest(TemMainList[i]);
		CQuestDB::stQuest* QuestInfo = g_pkQusetInfo->GetQuestInfo(TemMainList[i].uiQuestId);
		if (QuestInfo->uiIsMainQuest)
		{
			stUIMainQuestInfo stMainQuest ; 
			stMainQuest.uiMainQuest = TemMainList[i];
			if (g_pkQusetInfo->GetChildQuest(QuestInfo->id, stMainQuest.uiChildList, pkLocal->GetClassMask() , pkLocal->GetRaceMask()))
			{
				bool bNeedAdd = FindChildQuestList(stMainQuest.uiChildList, TemChildList);
				if (!bNeedAdd)
				{
					AddMainQuest(TemMainList[i]);
				}else
				{
					m_vFanisheQuestInfo.push_back(stMainQuest);
				}
			}

				
		}else
		{
			ULog("error");
		}
	

	}
	for (int i = 0; i < TemChildList.size(); i++)
	{
		UpdataQuest(TemChildList[i]);
	}

	
	
	//for(set<uint32>::const_iterator it = Msg.quests.begin(); it != Msg.quests.end(); ++it)
	//{
	//	if((*it) == 0)
	//		continue;

	//	stUIQuestInfo stTemp;
	//	stTemp.uiQuestId = *it;

	//	if(!g_pkQusetInfo->GetQuestInfo(stTemp.uiQuestId))
	//	{
	//		assert(0&&"not found questid");
	//		continue;
	//	}
	//	stTemp.uiQuestState = QMGR_QUEST_FINISHED + 1;
	//	//stTemp.vCreatureList = 0;
	//	stTemp.uiQuestSlotId = 0;
	//	m_vFinishedQuestInfo.push_back(stTemp);

	//	char sTemp[_MAX_PATH];
	//	string strTitle = g_pkQusetInfo->GetQuestInfo(stTemp.uiQuestId)->strTitle;
	//	if(strTitle.size() > 25)
	//	{
	//		strTitle.substr(0, 24);
	//		strTitle += ".";
	//	}
	//	NiSprintf(sTemp, _MAX_PATH, "<color:R155G155B155A255><Col1><center>%s<endnc><Col2><center>%s<endnc>", 
	//		(char*)strTitle.c_str(), 
	//		(char*)QuestStateToString(stTemp.uiQuestState).c_str());

	//	if(g_pkQusetInfo->GetQuestInfo(stTemp.uiQuestId)->uiIsMainQuest)
	//		m_QuestList->AddItem(sTemp, (void*)&stTemp.uiQuestId);
	//}
}

void UQuestList::AddMainQuest(const stUIQuestInfo& uiQuest)  //添加一个主线任务
{
	CPlayerLocal * pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return;
	}

	CQuestDB::stQuest* QuestInfo = g_pkQusetInfo->GetQuestInfo(uiQuest.uiQuestId);
	if (QuestInfo)
	{
		if (QuestInfo->uiIsMainQuest)
		{
			stUIMainQuestInfo uiMainQuest ;
			uiMainQuest.uiMainQuest = uiQuest;
			g_pkQusetInfo->GetChildQuest(uiQuest.uiQuestId, uiMainQuest.uiChildList , pkLocal->GetClassMask(), pkLocal->GetRaceMask());
			
			m_vMainQuestInfo.push_back(uiMainQuest);
			AddMainQuestToUI(uiMainQuest);
		}
	}
}
void UQuestList::AddMainQuestToUI(const stUIMainQuestInfo& uiQuest)
{
	CQuestDB::stQuest* MainQuest = g_pkQusetInfo->GetQuestInfo(uiQuest.uiMainQuest.uiQuestId);

	//插入主线任务进UI
	m_QuestList->InsertItem(0, (char*)QuestoString(uiQuest.uiMainQuest).c_str(), uiQuest.uiMainQuest.uiQuestState ,(void*)&uiQuest.uiMainQuest.uiQuestId);

	//插入支线任务进UI
	for (int i = 0; i < uiQuest.uiChildList.size(); i++)
	{
		int iRoot = -1, iSub = -1;
		
		m_QuestList->GetRootSub(MainQuest->id, iRoot, iSub);
		if (iRoot == -1)
		{
			UMessageBox::MsgBox( _TRAN("出现错误！") );
			continue ;
		}
		m_QuestList->InsertSubItem(0, iRoot, (char*)QuestoString(uiQuest.uiChildList[i]).c_str(), uiQuest.uiChildList[i].uiQuestState,(void*)&uiQuest.uiChildList[i].uiQuestId);
	}

	UpdateTraceQuest(uiQuest.uiMainQuest.uiQuestId);
}

bool UQuestList::FindQuestByID(ui32 Questid, stUIQuestInfo& stQuest)
{
	for (int i =0 ; i < m_vMainQuestInfo.size(); i++)
	{
		stUIMainQuestInfo MainTem = m_vMainQuestInfo[i];
		if (MainTem.uiMainQuest.uiQuestId == Questid)
		{
			stQuest = MainTem.uiMainQuest;
			return true;
		}

		for (int j =0 ; j < MainTem.uiChildList.size() ; j++)
		{
			if (MainTem.uiChildList[j].uiQuestId == Questid)
			{
				stQuest = MainTem.uiChildList[j];
				return true;
			}
		}
	}

	return false;
}

bool UQuestList::FindQuestMain(stUIQuestInfo& stMainQuest, const stUIQuestInfo& stQuest)
{
	//查找当前任务的主线任务，并且更新当前任务的状态
	ui32 Questid = stQuest.uiQuestId;
	for (int i =0 ; i < m_vMainQuestInfo.size(); i++)
	{
		stUIMainQuestInfo MainTem = m_vMainQuestInfo[i];
		if (MainTem.uiMainQuest.uiQuestId == Questid)
		{
			m_vMainQuestInfo[i].uiMainQuest = stQuest;
			/*m_vMainQuestInfo[i].uiMainQuest.uiQuestSlotId = stQuest.uiQuestSlotId;
			m_vMainQuestInfo[i].uiMainQuest.uiQuestState = stQuest.uiQuestState;
			m_vMainQuestInfo[i].uiMainQuest.vCreatureList = stQuest.vCreatureList;*/
			
			stMainQuest = m_vMainQuestInfo[i].uiMainQuest;
			return true;
		}

		for (int j =0 ; j < MainTem.uiChildList.size() ; j++)
		{
			if (MainTem.uiChildList[j].uiQuestId == Questid)
			{
				m_vMainQuestInfo[i].uiChildList[j] = stQuest;
				/*m_vMainQuestInfo[i].uiChildList[j].uiQuestSlotId = stQuest.uiQuestSlotId;
				m_vMainQuestInfo[i].uiChildList[j].uiQuestState = stQuest.uiQuestState;
				m_vMainQuestInfo[i].uiChildList[j].vCreatureList = stQuest.vCreatureList;*/

				stMainQuest = m_vMainQuestInfo[i].uiMainQuest;
				return true;
			}
		}
	}

	return false;

}
bool UQuestList::CheckDeleteMainQuest(ui32 Questid)
{
	//检查是否需要删除任务。这里的ID一定需要时主线任务的ID . 
	
	if (m_vMainQuestInfo.size() == 0)
	{
		return false;
	}

	bool CanDelete = true ;
	bool bFinish = true;
	int i ;
	for (i =0 ; i < m_vMainQuestInfo.size(); i++)
	{
		stUIMainQuestInfo MainTem = m_vMainQuestInfo[i];
		if (MainTem.uiMainQuest.uiQuestId == Questid)
		{
			//如果主线任务为未激活状态
			//那么可以删除,并且任务标示为未完成
			//这里主要是处理放弃主线任务的操作。
			if (MainTem.uiMainQuest.uiQuestState == QMGR_QUEST_NOT_AVAILABLE)
			{
				//
				bFinish = false;
				CanDelete = true;
				break ;
			}else
			{
				//如果主线任务为完成状态
				//
				if (MainTem.uiMainQuest.uiQuestState == QMGR_QUEST_FINISHED + 1)
				{
					//判断支线任务是不是都完成了.
					//如果有一个支线任务没完成.
					//那么就不可以删除
					for (int j =0 ; j < MainTem.uiChildList.size() ; j++)
					{
						stUIQuestInfo ChildTem = MainTem.uiChildList[j];
						if (ChildTem.uiQuestState != QMGR_QUEST_FINISHED + 1)
						{
							CanDelete = false;
							bFinish = false;
							break;
						}
					}
				}else
				{
					//如果主线任务没完成, 那么就不能删除.
					//
					CanDelete = false ;
					bFinish = false;
				}
				
			}
			
			break ;
		}
	}

	//如果所有支线都完成了。 
	//从UI中删除这个任务.
	//从当前列表中删除

	if (CanDelete)
	{
		if (bFinish)
		{
			//这里是主线任务和所有的支线任务都被玩家完成后,
			//进行的操作,所以这里要把这个任务丢进已经完成的任务列表里进行后续的处理
			//需要先从UI删除数据，然后在操作对象。
			m_vFanisheQuestInfo.push_back(m_vMainQuestInfo[i]);
			ULOG("完成主线任务%d以及其支线任务!",m_vMainQuestInfo[i].uiMainQuest.uiQuestId);
		}else
		{
			//这里是由于人为的操作,放弃了当前的主线任务导致的,
			//由于是玩家手动放弃的主线任务,
			//所以这里需要对任务说明和任务追踪页面先进行更新后
			//再从UI里处理数据.
			UpdateTraceQuest(Questid);
		}
		m_QuestList->DeleteItemById(m_vMainQuestInfo[i].uiMainQuest.uiQuestId);
		m_vMainQuestInfo.erase(m_vMainQuestInfo.begin() + i);
	}

	return CanDelete ;
}
//更新任务状态。
void UQuestList::UpdataQuest(const stUIQuestInfo& stQuest)		   //更新任务
{
	CQuestDB::stQuest* stQuestInfo = g_pkQusetInfo->GetQuestInfo(stQuest.uiQuestId);
	if (stQuestInfo)
	{
		//查找任务ID对应的主线和支线任务
		stUIQuestInfo stMainQuest; //该任务所在的主线. 有可能是任务本身就是主线

		if (!FindQuestMain(stMainQuest,stQuest))
		{
#ifdef _DEBUG
			UMessageBox::MsgBox_s( _TRAN("出现错误！当前主线任务不在列表！") );
#endif
			ClientSystemNotify("出现错误！当前主线任务不在列表.任务ID%d %s",
				stQuestInfo->id, (const char*)stQuestInfo->strTitle.c_str());
			return ;
		}
		
		
		//这里检查下是否能删除当前主线任务.
		//如果能删除，可以直接返回。
		if (CheckDeleteMainQuest(stMainQuest.uiQuestId))
		{
			return ;
		}
		
		//查找任务信息,是否是主线任务.
		int iRoot = -1, iSub = -1;
		m_QuestList->GetRootSub(stQuest.uiQuestId, iRoot, iSub);
		if(iRoot != -1 && iSub != -1)
		{
			//如果是支线任务.更新状态
			m_QuestList->SetSubItemText(iRoot, iSub, (char*)QuestoString(stQuest).c_str(), stQuest.uiQuestState);
		}
		else
		{
			m_QuestList->SetItemText(iRoot, (char*)QuestoString(stQuest).c_str(), stQuest.uiQuestState);
		}
		UpdateTraceQuest(stQuest.uiQuestId);
	}
}

ui32 UQuestList::GetQuestIdBySlot(ui32 uiQuestSlotId)
{
	if (uiQuestSlotId == 0xff)
	{
		return 0 ;
	}
	for (int i =0; i < m_vMainQuestInfo.size(); i++)
	{
		if (m_vMainQuestInfo[i].uiMainQuest.uiQuestSlotId == uiQuestSlotId)
		{
			return m_vMainQuestInfo[i].uiMainQuest.uiQuestId;
		}

		for (int j = 0; j < m_vMainQuestInfo[i].uiChildList.size(); j++)
		{
			if (m_vMainQuestInfo[i].uiChildList[j].uiQuestSlotId == uiQuestSlotId)
			{
				return m_vMainQuestInfo[i].uiChildList[j].uiQuestId;
			}
		}
	}

	return 0 ;
}

bool UQuestList:: GetCanFinishQuestData(uint32 MapID, vector<NiPoint3>& PosVec)
{
	for (int i =0; i < m_vMainQuestInfo.size(); i++)
	{
		if (m_vMainQuestInfo[i].uiMainQuest.uiQuestState == QMGR_QUEST_FINISHED + 1)
		{
			//遍历子任务
			for (int j=0; j< m_vMainQuestInfo[i].uiChildList.size(); j++)
			{
				if (m_vMainQuestInfo[i].uiChildList[j].uiQuestState == QMGR_QUEST_FINISHED ||
					m_vMainQuestInfo[i].uiChildList[j].uiQuestState == QMGR_QUEST_REPEATABLE_FINISHED)
				{
					ui32 FinishNPCEntry = 0 ;
					if (g_pkQuestFinisherDB->GetNpcId(m_vMainQuestInfo[i].uiChildList[j].uiQuestId,FinishNPCEntry))
					{
						ui32 mapid;
						float x,y,z;
						std::string name;
						if(g_pkCCreatureSpawnsInfo->GetCreateInfomation(FinishNPCEntry,mapid,x,y,z))
						{
							if (mapid == MapID)
							{
								PosVec.push_back(NiPoint3(x,y,z));
							}
						}
					}
				}
			}
		}else
		{
			if (m_vMainQuestInfo[i].uiMainQuest.uiQuestState == QMGR_QUEST_FINISHED ||
				m_vMainQuestInfo[i].uiMainQuest.uiQuestState == QMGR_QUEST_REPEATABLE_FINISHED)
			{
				ui32 FinishNPCEntry = 0 ;
				if (g_pkQuestFinisherDB->GetNpcId(m_vMainQuestInfo[i].uiMainQuest.uiQuestId,FinishNPCEntry))
				{
					ui32 mapid;
					float x,y,z;
					std::string name;
					
					if(g_pkCCreatureSpawnsInfo->GetCreateInfomation(FinishNPCEntry,mapid,x,y,z))
					{
						if (mapid == MapID)
						{
							PosVec.push_back(NiPoint3(x,y,z));
						}
					}
				}						
			}
		}

	}


	return PosVec.size() != 0;
}
void UQuestList::UpdataQuestBySlot(ui32 uiQuestSlotId,ui32 uiQuestState,std::vector<ui32>& vCreatureCount,ui32& Questid)
{
	for (int i =0; i < m_vMainQuestInfo.size(); i++)
	{
		if (m_vMainQuestInfo[i].uiMainQuest.uiQuestSlotId == uiQuestSlotId)
		{
			m_vMainQuestInfo[i].uiMainQuest.uiQuestState = uiQuestState;
			m_vMainQuestInfo[i].uiMainQuest.vCreatureList = vCreatureCount;
			Questid = m_vMainQuestInfo[i].uiMainQuest.uiQuestId;
			UpdataQuest(m_vMainQuestInfo[i].uiMainQuest);
			return ;
		}

		for (int j = 0; j < m_vMainQuestInfo[i].uiChildList.size(); j++)
		{
			if (m_vMainQuestInfo[i].uiChildList[j].uiQuestSlotId == uiQuestSlotId)
			{
				m_vMainQuestInfo[i].uiChildList[j].uiQuestState = uiQuestState;
				m_vMainQuestInfo[i].uiChildList[j].vCreatureList = vCreatureCount;
				Questid = m_vMainQuestInfo[i].uiChildList[j].uiQuestId;
				UpdataQuest(m_vMainQuestInfo[i].uiChildList[j]);
				return ;
			}
		}
	}
}
void UQuestList::OnUpdate(
						 ui32 uiQuestId, 
						 ui32 uiQuestState, 
						 std::vector<ui32>& vCreatureCount,
						 ui32 uiQuestSlotId)
{
	//由于服务器在客户端创建LOCALPLAYER 前就把已经完成的任务列表发下来了。
	//导致客户端无法处理,所以临时存贮起来.
	//这里先处理这个临时的表.
	//处理完后会清空这个VECTER.
	if (m_QuestFinishTem.size())
	{
		OrderFinishQuestTem();
	}

	if(uiQuestId == 0)
	{
		//当ID为0时候表示任务被放弃。
		if (m_vMainQuestInfo.size())
		{
			ui32 DropQuestID = 0;
			UpdataQuestBySlot(uiQuestSlotId,QMGR_QUEST_NOT_AVAILABLE,vCreatureCount,DropQuestID);
			ChatSystem->AppendQuestMsg(DropQuestID,DropQuest);
		}
		return ;
	}

	stUIQuestInfo stTemp;
	stTemp.uiQuestId = uiQuestId;
	stTemp.uiQuestState = uiQuestState;
	stTemp.vCreatureList = vCreatureCount;
	stTemp.uiQuestSlotId = uiQuestSlotId;
	
	CQuestDB::stQuest* stQuestinfo = g_pkQusetInfo->GetQuestInfo(uiQuestId);
	if (!stQuestinfo)
	{
		return ;
	}
	stUIQuestInfo MainQuest;
	if (!FindQuestMain(MainQuest,stTemp) && stQuestinfo->uiIsMainQuest)
	{
		if (stQuestinfo->uiIsMainQuest)
		{
			AddMainQuest(stTemp);
		}else
		{
			ClientSystemNotify("出现错误！当前主线任务不在列表.任务ID%d %s",
				stQuestinfo->id, (const char*)stQuestinfo->strTitle.c_str());
#ifdef _DEBUG
			UMessageBox::MsgBox_s( _TRAN("出现错误！当前主线任务不在列表") );
#endif
		}
	}else
	{
		UpdataQuest(stTemp);
	}
}
#define MAX_TITLE_STRCOUNT 36
#define MAX_QUESTDES_STRCOUNT 200

void UQuestList::OnQuestFinish(ui32 uiQuestId)
{
	stUIQuestInfo uiQuest ;
	uiQuest.uiQuestId = uiQuestId;
	uiQuest.uiQuestState = QMGR_QUEST_FINISHED + 1;
	uiQuest.uiQuestSlotId = 0xff;
	UpdataQuest(uiQuest);

	UQuestTrace* pQuestTraceDlg = INGAMEGETFRAME(UQuestTrace, FRAME_IG_QUESTTRACE);
	if(pQuestTraceDlg)
	{
		pQuestTraceDlg->DeleteText(uiQuest.uiQuestId);
	}
	UQuestDlg* pQuestDlg = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
	pQuestDlg->GetQuestLog()->ClearAllTextContent();
}
void UQuestList::SetPosition( const UPoint &NewPos )
{
	UPoint _pos = NewPos;
	UControl::SetPosition(_pos);
}

uint32 UQuestList::mQstID = 0;
uint32 UQuestList::mNpcId = 0;
void UQuestList::ParseQuestEscortNotify(std::string who,uint32 qst_id,uint32 npc_id)
{
	mQstID = qst_id;
	mNpcId = npc_id;
	std::string str = who + _TRAN("接受了护送任务，是否一同护送?");
	UMessageBox::MsgBox(str.c_str(), (BoxFunction*)UQuestList::DO_AcceptQuestEscort, (BoxFunction*)UQuestList::DO_Nothing);
}
void UQuestList::DO_AcceptQuestEscort()
{
	PacketBuilder->SendstQuestEscortReply(true,mQstID, mNpcId);
	mQstID = 0;
	mNpcId = 0;
}
void UQuestList::DO_Nothing()
{
	mQstID = 0;
	mNpcId = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UQuestLog, UControl);
//事件响应
UBEGIN_MESSAGE_MAP(UQuestLog, UControl)
UON_RED_URL_CLICKED(2, &UQuestLog::OnURLClicked)
UON_RED_URL_MOUSE_MOVE(2, &UQuestLog::OnURLMove)
UON_RED_URL_OUT_MOUSE(2, &UQuestLog::OnURLLeave)
UEND_MESSAGE_MAP()
void UQuestLog::StaticInit()
{

}

UQuestLog::UQuestLog()
{
	m_pkTextCtrl = NULL;
	m_pkQuestId = 0;
}
UQuestLog::~UQuestLog()
{
	m_pkTextCtrl = NULL;
}

BOOL UQuestLog::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	UScrollBar* pQuestContentBar = (UScrollBar*)GetChildByID(1);
	if (pQuestContentBar == NULL)
	{
		return FALSE;
	}
	pQuestContentBar->SetBarShowMode(USBSM_FORCEON,USBSM_FORCEOFF);

	//GetChildByID(14)->SetActive(FALSE);

	m_pkTextCtrl = (URichTextCtrl*)pQuestContentBar->GetChildByID(2);
	if (m_pkTextCtrl == NULL)
	{
		return FALSE;
	}

	m_pkTextCtrl->SetBitMapURLFun(RichTextBitMapURLFun);

	return TRUE;
}

void UQuestLog::OnDestroy()
{
	m_pkQuestId = 0;
	UControl::OnDestroy();
}

bool UQuestLog::MakeStartQuestStr(ui32 Questid, std::string& str)
{
	ui32 FinishNPCEntry = 0 ;
	if (g_pkQuestStarterDB->GetNpcId(Questid,FinishNPCEntry))
	{
		ui32 mapid;
		float x,y,z;
		std::string name;
		if (ItemMgr->GetCreatureName(FinishNPCEntry, name))
		{
			//char buf[_MAX_PATH];
			if (g_pkCCreatureSpawnsInfo->GetCreateInfomation(FinishNPCEntry,mapid,x,y,z))
			{
				//sprintf(buf, "<br><font:Arial:14><color:4d2f07>您可以找<linkcolor:137300><a:#NPC %d %d %d %d>%s</a>领取本任务<br>", mapid, (UINT)x, (UINT)y, (UINT)z, name.c_str());
				str = _TRAN( N_NOTICE_C33, _I2A(mapid).c_str(), _I2A((UINT)x).c_str(), _I2A((UINT)y).c_str(), _I2A((UINT)z).c_str(), name.c_str() );
			}else
			{
				str = _TRAN( N_NOTICE_C34,name.c_str() );
				//sprintf(buf, "<br><font:Arial:14><color:4d2f07>您可以找%s</a>领取本任务<br>", name.c_str());

			}
			
			//str = buf ;
			return true ;
		}
	}

	return false ;
}

void UQuestLog::OnURLMove(UNM* pNM)
{
	OnURLItemOn(pNM);
}
void UQuestLog::OnURLLeave()
{
	OnURLItemLeave();
	
}
void UQuestLog::OnURLClicked(UNM* pNM)
{
	OnURMoveMap(pNM);
}

void UQuestLog::UpdataQuestContent(const stUIQuestInfo& vQuestInfo)
{
	if (m_pkQuestId == vQuestInfo.uiQuestId && m_pkQuestId)
	{
		OnQuestContent(vQuestInfo);
	}
}
void UQuestLog::OnQuestContent(const stUIQuestInfo& vQuestInfo)
{
	//获取人物的内容
	CQuestDB::stQuest* pkQuest = g_pkQusetInfo->GetQuestInfo(vQuestInfo.uiQuestId);

	NIASSERT(pkQuest);
	

	ClearAllTextContent();
	m_pkQuestId = vQuestInfo.uiQuestId ;


	// 任务标题
	char szBuf[2048];
	szBuf[0] = '\0';
	int len = 0;
	NiSprintf(szBuf, 2048, "<font:ArialBold:14><color:3f2404>%s", pkQuest->strTitle.c_str());

	m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);

	if (vQuestInfo.uiQuestState == QMGR_QUEST_NOT_AVAILABLE) //未激活状态 不能接受
	{
		std::string str ;
		if (MakeStartQuestStr(vQuestInfo.uiQuestId, str))
		{
			m_pkTextCtrl->AppendText(str.c_str(), str.length(), TRUE);
		}else
		{
			ULOG("Quest start DB not Find Quest NPC");
		}

		return ;
	}else if (vQuestInfo.uiQuestState == QMGR_QUEST_AVAILABLE || vQuestInfo.uiQuestState == QMGR_QUEST_REPEATABLE)
	{
		std::string str ;
		if (MakeStartQuestStr(vQuestInfo.uiQuestId, str))
		{
			m_pkTextCtrl->AppendText(str.c_str(), str.length(), TRUE);
		}else
		{
			ULOG("Quest start DB not Find Quest NPC");
		}
		return ;
	}
	

	

	// 任务概要
	std::string strObjective = _TRAN("　　");
	strObjective += pkQuest->strObjective.c_str();
	int iNamePos = strObjective.find("<a:#PlayerName></a>");
	if(iNamePos != -1)
	{
		std::string strPlayerName;
		strPlayerName = ObjectMgr->GetLocalPlayer()->GetName();
		std::string str1, str2;
		str1 = strObjective.substr(0, iNamePos);
		str2 = strObjective.substr(iNamePos + 19, strObjective.size() - 19 - iNamePos);
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s%s%s<br>", str1.c_str(), strPlayerName.c_str(), str2.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}
	else
	{
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s<br>", strObjective.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}

	// 杀怪信息
	for(unsigned int ui = 0; ui < 4; ui++)
	{
		if(pkQuest->uiReqItemCount[ui] == 0)
			continue;

		ItemPrototype_Client* pkItem = ItemMgr->GetItemPropertyFromDataBase(pkQuest->uiReqItemId[ui]);
		
		//GetItemFindPath(pkQuest->uiReqItemId[ui], strItemName);
		if(pkItem)
		{
			char buf[256];
			char count[256];
			itoa(pkQuest->uiReqItemId[ui], buf, 10);
			itoa(pkQuest->uiReqItemCount[ui], count, 10);
			std::string strItemID = buf ;
			std::string strItemCN = count ;
			std::string strItemName = "<a:#_QUESTITEM_"+ strItemID +"_" + strItemCN +">" + pkItem->C_name + "</a> ";

			NiSprintf(szBuf, 2048, "<br>  %s : %u / %u<br>",
				strItemName.c_str(),
				vQuestInfo.vCreatureList[ui],
				pkQuest->uiReqItemCount[ui]);
			m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
		}
	}

	//需要荣誉
	if (pkQuest->uiReqHonor)
	{
		UINT Honor = 0;
		CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
		if (pkLocal)
		{
			Honor = pkLocal->GetUInt32Value(PLAYER_FIELD_HONOR_CURRENCY);
		}
		std::string strbuff = _TRAN( N_NOTICE_C38, _I2A(Honor).c_str(), _I2A( pkQuest->uiReqHonor ).c_str() );
		//NiSprintf(szBuf, 2048, strbuff.c_str()/*"需要荣誉 : %d / %d<br>", Honor,pkQuest->uiReqHonor*/ );
		m_pkTextCtrl->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
	}

	std::string strCreatureNames[4];
	int CreatureCount = (int)vQuestInfo.vCreatureList.size();
	for(int i = 0; i < CreatureCount; ++i)
	{
		if(pkQuest->uiReqKillMobOrGOCount[i] == 0)
			continue;

		ItemMgr->GetCreatureName(pkQuest->uiReqKillMobOrGOId[i], strCreatureNames[i]);
		
		GetMonsterFindPath(pkQuest->uiReqKillMobOrGOId[i],  strCreatureNames[i]);
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "  %s : %2d / %2d<br>", 
			strCreatureNames[i].c_str(),
			vQuestInfo.vCreatureList[i], 
			pkQuest->uiReqKillMobOrGOCount[i]);
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}

	NiSprintf(szBuf, 2048, "<br><font:ArialBold:14><color:3f2404>%s", _TRAN("任务描述"));
	m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	// 任务详细
	std::string strDetails = _TRAN("　　");
	strDetails += pkQuest->strDetails.c_str();
	iNamePos = strDetails.find("<a:#PlayerName></a>");
	if(iNamePos != -1)
	{
		std::string strPlayerName;
		strPlayerName = ObjectMgr->GetLocalPlayer()->GetName();
		std::string str1, str2;
		str1 = strDetails.substr(0, iNamePos);
		str2 = strDetails.substr(iNamePos + 19, strDetails.size() - 19 - iNamePos);
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s%s%s<br>", str1.c_str(), strPlayerName.c_str(), str2.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}
	else
	{
		szBuf[0] = '\0';
		NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:4d2f07>%s<br>", strDetails.c_str());
		m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);
	}


	if (pkQuest->uiRewHonor)
	{
		std::string strbuff = _TRAN( N_NOTICE_C35, _I2A(pkQuest->uiRewHonor).c_str() );
		//NiSprintf(szBuf, 2048, strbuff.c_str()/*"<br><font:ArialBold:14><color:3f2404>获得荣誉 : <font:Arial:14><color:4d2f07>%d <br>", pkQuest->uiRewHonor*/);
		m_pkTextCtrl->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
	}
	if (pkQuest->uiRewMoney)
	{
		std::string strbuff = _TRAN( N_NOTICE_C36, GetMoneyRichTextStr(pkQuest->uiRewMoney).c_str() );
		//NiSprintf(szBuf, 2048, strbuff.c_str()/*"<br><font:ArialBold:14><color:3f2404>获得金钱 : <font:Arial:14><color:4d2f07> %s <br>",GetMoneyRichTextStr(pkQuest->uiRewMoney).c_str()*/);
		m_pkTextCtrl->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
	}
	if (pkQuest->uiRewXP)
	{
		// 
		ui32 EXP = pkQuest->uiRewXP;
		// 在这个区间的任务的经验奖励是按升级的百分比计算的
		if (pkQuest->id >= 700001 && pkQuest->id <= 799999 && EXP <= 100)
		{
			CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
			if (pkLocal)
			{
				EXP = EXP * pkLocal->GetMaxExp() / 100;  
			}
		}

		std::string strbuff = _TRAN( N_NOTICE_C37, _I2A(EXP).c_str() );
		//NiSprintf(szBuf, 2048, strbuff.c_str()/*"<br><font:ArialBold:14><color:3f2404>获得经验 : <font:Arial:14><color:4d2f07>%d <br>", EXP*/);
		m_pkTextCtrl->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
	}

	// 任务奖品
	for(unsigned int uiIndex = 0; uiIndex < 4; uiIndex++)
	{
		if (!pkQuest->uiRewItem[uiIndex])
		{
			break ;
		}
		ItemPrototype_Client* pkItemInfo = ItemMgr->GetItemPropertyFromDataBase(pkQuest->uiRewItem[uiIndex]);
		if(pkItemInfo)
		{
			if(uiIndex == 0)
			{
				szBuf[0] = '\0';
				NiSprintf(szBuf, 2048, "<br><font:ArialBold:14><color:3f2404>%s<br>",_TRAN("你可以获得") );
				m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);	
			}
			
			char buf[256];
			char count[256];

			itoa(pkQuest->uiRewItem[uiIndex], buf, 10);
			itoa(pkQuest->uiRewItemCount[uiIndex], count, 10);

			std::string strItemID = buf ;
			std::string strItemCN = count ;

			std::string str = pkItemInfo->C_icon;
			str = "<skin:" + str + ">";
			str = "<a:#_REWITEM_"+ strItemID +"_" + strItemCN + ">" + str +  "</a> " ;


			m_pkTextCtrl->AppendText(str.c_str(), str.length(), TRUE);
		}
	}
	// 任务可选奖品
	for(unsigned int uiIndex = 0; uiIndex < 6; uiIndex++)
	{
		
		if (!pkQuest->uiRewChoiceItem[uiIndex])
		{
			continue ;
		}
		ItemPrototype_Client* pkItemInfo = ItemMgr->GetItemPropertyFromDataBase(pkQuest->uiRewChoiceItem[uiIndex]);
		if(pkItemInfo)
		{
			if(uiIndex == 0)
			{
				szBuf[0] = '\0';
				NiSprintf(szBuf, 2048,"<br><font:ArialBold:14><color:3f2404>%s<br>", _TRAN("你可以选择") );
				m_pkTextCtrl->AppendText(szBuf, strlen(szBuf), TRUE);	
			}


			char buf[256];
			char count[256];
			itoa(pkQuest->uiRewChoiceItem[uiIndex], buf, 10);
			itoa(pkQuest->uiRewChoiceItemCount[uiIndex], count , 10);

			std::string strItemCN = count;
			std::string strItemID = buf ;
			std::string str = pkItemInfo->C_icon;
			str = "<skin:" + str + ">";
			str = "<a:#_CHOOSEITEMICON_"+ strItemID + "_"+strItemCN +">" + str +"</a> "  ;
			

			m_pkTextCtrl->AppendText(str.c_str(), str.length(), TRUE);
			
		}
	}

	UScrollBar* pkBar = UDynamicCast(UScrollBar, GetChildByID(1));
	if (pkBar)
	{
		pkBar->ReCalLayout();
	}
}

void UQuestLog::ClearAllTextContent()
{
	m_pkTextCtrl->SetText(" ", 1);
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UQuestDlg, UDialog)
void UQuestDlg::StaticInit()
{

}

UQuestDlg::UQuestDlg()
{
	m_pQuestList = NULL;
	m_pQuestLog = NULL;
}

UQuestDlg::~UQuestDlg()
{

}

BOOL UQuestDlg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	if (m_pQuestList == NULL)
	{
		m_pQuestList = UDynamicCast(UQuestList, GetChildByID(0));
		if (!m_pQuestList)
		{
			return FALSE;
		}
	}
	if (m_pQuestLog == NULL)
	{
		m_pQuestLog = UDynamicCast(UQuestLog, GetChildByID(1));
		if (!m_pQuestLog)
		{
			return FALSE;
		}
	}
	UStaticText* pStaticText = UDynamicCast(UStaticText, GetChildByID(3));
	if (pStaticText)
	{
		pStaticText->SetTextAlign(UT_CENTER);
		pStaticText->SetTextColor("01FFFF");
	}
	pStaticText = UDynamicCast(UStaticText, GetChildByID(4));
	if (pStaticText)
	{
		pStaticText->SetTextAlign(UT_CENTER);
		pStaticText->SetTextColor("01FFFF");
	}
	return TRUE;
}

void UQuestDlg::OnDestroy()
{
	UDialog::OnDestroy();
}

void UQuestDlg::OnClose()
{
	m_pQuestLog->ClearAllTextContent();
	UControl* pInGame = SYState()->UI->GetDialogEX(DID_INGAME_MAIN);
	if (pInGame)
	{
		UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
		if (pGameBar)
		{
			UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONQUEST);
			if (pBtn)
			{
				pBtn->SetCheck(TRUE);
			}
		}
	}
}

BOOL UQuestDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}


//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UHideQuestTraceButton, UControl);
UBEGIN_MESSAGE_MAP(UHideQuestTraceButton, UControl)
UON_BN_CLICKED(0, &UHideQuestTraceButton::OnSetChatHide)
UON_BN_CLICKED(1, &UHideQuestTraceButton::OnSetChatShow)
UEND_MESSAGE_MAP()

void UHideQuestTraceButton::StaticInit()
{

}
UHideQuestTraceButton::UHideQuestTraceButton()
{
	m_IsUpdateChild = FALSE;
}
UHideQuestTraceButton::~UHideQuestTraceButton()
{

}
void UHideQuestTraceButton::OnDestroy()
{
	OnSetChatShow();
	UControl::OnDestroy();
}
void UHideQuestTraceButton::OnSetChatHide()
{
	UControl* pCtrl1 = GetChildByID(0);
	UControl* pCtrl2 = GetChildByID(1);
	if (pCtrl1 && pCtrl2)
	{
		pCtrl1->SetVisible(FALSE);
		pCtrl2->SetVisible(TRUE);
	}
	UQuestTrace* pQuestTrace = INGAMEGETFRAME(UQuestTrace, FRAME_IG_QUESTTRACE);
	if (pQuestTrace)
	{
		pQuestTrace->SetTraceVis(FALSE);
	}

	if (InstanceSys)
	{
		InstanceSys->ShowOrHide(TRUE);
	}
}
void UHideQuestTraceButton::OnSetChatShow()
{	
	UControl* pCtrl1 = GetChildByID(0);
	UControl* pCtrl2 = GetChildByID(1);
	if (pCtrl1 && pCtrl2)
	{
		pCtrl1->SetVisible(TRUE);
		pCtrl2->SetVisible(FALSE);
	}
	UQuestTrace* pQuestTrace = INGAMEGETFRAME(UQuestTrace, FRAME_IG_QUESTTRACE);
	if (pQuestTrace)
	{
		pQuestTrace->SetTraceVis(TRUE);
	}

	if (InstanceSys)
	{
		InstanceSys->ShowOrHide(FALSE);
	}
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UQuestTrace, UDialog);
//事件响应
UBEGIN_MESSAGE_MAP(UQuestTrace, UDialog)
UON_RED_URL_CLICKED(0, &UQuestTrace::OnURLClick0)
UON_RED_URL_CLICKED(1, &UQuestTrace::OnURLClick1)
UON_RED_URL_CLICKED(2, &UQuestTrace::OnURLClick2)
UON_RED_URL_CLICKED(3, &UQuestTrace::OnURLClick3)
UON_RED_URL_CLICKED(4, &UQuestTrace::OnURLClick4)

UON_RED_URL_MOUSE_MOVE(0, &UQuestTrace::OnURLMove)
UON_RED_URL_MOUSE_MOVE(1, &UQuestTrace::OnURLMove)
UON_RED_URL_MOUSE_MOVE(2, &UQuestTrace::OnURLMove)
UON_RED_URL_MOUSE_MOVE(3, &UQuestTrace::OnURLMove)
UON_RED_URL_MOUSE_MOVE(4, &UQuestTrace::OnURLMove)

UON_RED_URL_OUT_MOUSE(0, &UQuestTrace::OnURLLeave)
UON_RED_URL_OUT_MOUSE(1, &UQuestTrace::OnURLLeave)
UON_RED_URL_OUT_MOUSE(2, &UQuestTrace::OnURLLeave)
UON_RED_URL_OUT_MOUSE(3, &UQuestTrace::OnURLLeave)
UON_RED_URL_OUT_MOUSE(4, &UQuestTrace::OnURLLeave)
UEND_MESSAGE_MAP()

void UQuestTrace::StaticInit()
{

}

UQuestTrace::UQuestTrace()
{
	m_Hide = FALSE;
	m_MouseFlag = FALSE;
	m_IsUpdateChild = FALSE;
}

UQuestTrace::~UQuestTrace()
{
	ClearAll();
}

BOOL UQuestTrace::OnCreate()
{
	if(!UControl::OnCreate())
	{
		return FALSE;
	}

	m_QuestData[0].pText= (URichTextCtrl*)GetChildByID(0);
	m_QuestData[0].pText->SetLineOffset(3);
	m_QuestData[1].pText = (URichTextCtrl*)GetChildByID(1);
	m_QuestData[1].pText->SetLineOffset(3);
	m_QuestData[2].pText = (URichTextCtrl*)GetChildByID(2);
	m_QuestData[2].pText->SetLineOffset(3);
	m_QuestData[3].pText = (URichTextCtrl*)GetChildByID(3);
	m_QuestData[3].pText->SetLineOffset(3);
	m_QuestData[4].pText = (URichTextCtrl*)GetChildByID(4);
	m_QuestData[4].pText->SetLineOffset(3);

	if (!CheckQuestData())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;
	UControl* HideButton = UInGame::GetFrame(FRAME_IG_HIDEQUESTTRACEBUTTON);
	if (HideButton)
	{
		HideButton->SetVisible(TRUE);
	}
	

	return TRUE;
}

BOOL UQuestTrace::CheckQuestData()
{
	if (!m_QuestData[0].pText || !m_QuestData[1].pText || !m_QuestData[2].pText ||!m_QuestData[3].pText || !m_QuestData[4].pText)
	{
		return FALSE;
	}

	return TRUE ;
}
void UQuestTrace::OnDestroy()
{
	ClearAll();
	UDialog::OnDestroy();
}
void UQuestTrace::ReLayout()
{
	UPoint pos(0,0);
	for (int i =0 ; i<5; i++)
	{
		if (m_QuestData[i].Questid)
		{
			m_QuestData[i].pText->SetPosition(pos);
			m_QuestData[i].pText->SetVisible(TRUE);
			pos.y += m_QuestData[i].pText->GetControlRect().GetHeight() ;

		}else
		{
			m_QuestData[i].pText->SetVisible(FALSE);
		}
	}
}
void UQuestTrace::SetTraceVis(BOOL vis)
{
	m_Hide = !vis;
	SetVisible(vis);
}

int UQuestTrace::CalIndex()
{
	for (int i = 0; i <5 ; i++)
	{
		if (!m_QuestData[i].Questid && m_QuestData[i].pText)
		{
			return i ;
		}
	}

	return 4 ;
}

std::string QuestStateToUI(ui32 uiQuestState)
{
	if(uiQuestState == QMGR_QUEST_FINISHED)
		return "<img:DATA\\UI\\Public\\WanChengFuBen.png>";

	return "<img:DATA\\UI\\Public\\JinXingZhongFuBen.png>";
}

void UQuestTrace::AddText(stUIQuestInfo& vQuestInfo)
{
	
	int iCtrlIndex = CalIndex();

	m_QuestData[iCtrlIndex].pText->SetText(" ", 1);

	//获取人物的内容
	CQuestDB::stQuest* pkQuest = g_pkQusetInfo->GetQuestInfo(vQuestInfo.uiQuestId);
	if (!pkQuest)
		return;
	NIASSERT(pkQuest);
	m_QuestData[iCtrlIndex].Questid = vQuestInfo.uiQuestId;

	// 任务标题
	char szBuf[2048];
	szBuf[0] = '\0';
	int len = 0;

	BOOL CanFinishQuest = (vQuestInfo.uiQuestState == QMGR_QUEST_FINISHED);
	NiSprintf(szBuf, 2048, "<shadowcolor:202020FF><shadow:1:1><font:Arial:14><color:ff9600>%-20s%s <br><font:Arial:14>", pkQuest->strTitle.c_str(), QuestStateToUI(vQuestInfo.uiQuestState).c_str());

	m_QuestData[iCtrlIndex].pText->AppendText(szBuf, strlen(szBuf), TRUE);

	// 任务概要
	

	bool bFindPepoleQuest = true ;
	bool isFind = false ;

	if (!CanFinishQuest)
	{
		// 杀怪信息
		for(unsigned int ui = 0; ui < 4; ui++)
		{
			if(pkQuest->uiReqItemCount[ui] == 0)
				continue;

			bFindPepoleQuest = false ;
			ItemPrototype_Client* pkItem = ItemMgr->GetItemPropertyFromDataBase(pkQuest->uiReqItemId[ui]);
			//std::string strItemName = pkItem->C_name;
			//GetItemFindPath(pkQuest->uiReqItemId[ui], strItemName);
			if(pkItem)
			{
				char buf[256];
				char count[256];
				itoa(pkQuest->uiReqItemId[ui], buf, 10);
				itoa(pkQuest->uiReqItemCount[ui], count, 10);
				std::string strItemID = buf ;
				std::string strItemCN = count ;
				std::string strItemName = "<a:#_QUESTITEM_"+ strItemID +"_" + strItemCN +">" + pkItem->C_name + "</a> ";

				NiSprintf(szBuf, 2048, "%s : %2d / %2d<br>",
					strItemName.c_str(),
					vQuestInfo.vCreatureList[ui],
					pkQuest->uiReqItemCount[ui]);
				m_QuestData[iCtrlIndex].pText->AppendText(szBuf, strlen(szBuf), TRUE);
			}
		}

		//需要荣誉
		if (pkQuest->uiReqHonor)
		{
			UINT Honor = 0;
			CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
			if (pkLocal)
			{
				Honor = pkLocal->GetUInt32Value(PLAYER_FIELD_HONOR_CURRENCY);
			}
			bFindPepoleQuest = false ;
			std::string strbuff = _TRAN( N_NOTICE_C38, _I2A(Honor).c_str(), _I2A( pkQuest->uiReqHonor ).c_str() );
			//NiSprintf(szBuf, 2048, strbuff.c_str()/* "需要荣誉 : %d / %d<br>", Honor,pkQuest->uiReqHonor*/ );
			m_QuestData[iCtrlIndex].pText->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
		}

		std::string strCreatureNames[4];
		int CreatureCount = (int)vQuestInfo.vCreatureList.size();
		for(int i = 0; i < CreatureCount; ++i)
		{
			if(pkQuest->uiReqKillMobOrGOCount[i] == 0)
				continue;

			bFindPepoleQuest = false ;
			ItemMgr->GetCreatureName(pkQuest->uiReqKillMobOrGOId[i], strCreatureNames[i]);
			GetMonsterFindPath(pkQuest->uiReqKillMobOrGOId[i],  strCreatureNames[i]);
			szBuf[0] = '\0';
			NiSprintf(szBuf, 2048, "%s : %2d / %2d<br>", 
				strCreatureNames[i].c_str(),
				vQuestInfo.vCreatureList[i], 
				pkQuest->uiReqKillMobOrGOCount[i]);
			m_QuestData[iCtrlIndex].pText->AppendText(szBuf, strlen(szBuf), TRUE);
		}
	}else 
	{
		std::string str;
		isFind = MakeFinishQuestStr(vQuestInfo.uiQuestId, str);

		if (isFind)
		{
			m_QuestData[iCtrlIndex].pText->AppendText(str.c_str(), str.length(), TRUE);
		}else
		{
			ULOG( "任务%s ： 没有找到完成NPC的数据" ,pkQuest->strTitle.c_str());
		}
	}
	
	if (bFindPepoleQuest && !isFind)
	{
		std::string strObjective = "　　";
		strObjective += pkQuest->strObjective.c_str();
		int iNamePos = strObjective.find("<a:#PlayerName></a>");
		if(iNamePos != -1)
		{
			std::string strPlayerName;
			strPlayerName = ObjectMgr->GetLocalPlayer()->GetName();
			std::string str1, str2;
			str1 = strObjective.substr(0, iNamePos);
			str2 = strObjective.substr(iNamePos + 19, strObjective.size() - 19 - iNamePos);
			szBuf[0] = '\0';
			NiSprintf(szBuf, 2048, "<font:Arial:14><color:eaab81>%s%s%s<br>", str1.c_str(), strPlayerName.c_str(), str2.c_str());
			m_QuestData[iCtrlIndex].pText->AppendText(szBuf, strlen(szBuf), TRUE);
		}
		else
		{
			szBuf[0] = '\0';
			NiSprintf(szBuf, 2048, "<font:Arial:14><color:eaab81>%s<BR>", strObjective.c_str());
			m_QuestData[iCtrlIndex].pText->AppendText(szBuf, strlen(szBuf), TRUE);
		}
	}
	string strtext = "<a:#INFO_QUEST><linkcolor:FFFF00>";
	strtext.append(_TRAN("详细</a>"));
	m_QuestData[iCtrlIndex].pText->AppendText(strtext.c_str(), strtext.length(), TRUE);


	ReLayout();

	if (!m_Hide)
	{
		SetVisible(TRUE);
	}
}
bool UQuestTrace::IsHasQuest(unsigned int QuestID)
{
	for ( int i =0; i<5 ; i++)
	{
		if (m_QuestData[i].Questid == QuestID)
		{
			return true ;
		}
	}

	return false ;
}
unsigned int UQuestTrace::GetCurTraceCount() 
{
	int count = 0;
	for ( int i =0; i<5 ; i++)
	{
		if (m_QuestData[i].Questid != 0)
		{
			count ++ ;
		}
	}

	return count ;
}
unsigned int UQuestTrace::GetLastQuestId() 
{
	for ( int i = 4; i >= 0 ; i--)
	{
		if (m_QuestData[i].Questid != 0)
		{
			return m_QuestData[i].Questid;
		}
	}
	return 0 ;
}
void UQuestTrace::DeleteText(unsigned int uiQuestId)
{
	for (int i = 0; i < 5; i++)
	{
		if (m_QuestData[i].Questid == uiQuestId)
		{
			m_QuestData[i].pText->ClearText();
			m_QuestData[i].Questid = 0;
			m_QuestData[i].pText->SetVisible(FALSE);
			break ;
		}
	}

	ReLayout();
	
}

void UQuestTrace::UpdataHonor()
{
	for(unsigned int uiQuest = 0; uiQuest < 5; uiQuest++)
	{
		if (!m_QuestData[uiQuest].Questid)
		{
			continue ;
		}

		CQuestDB::stQuest* pkQuest = g_pkQusetInfo->GetQuestInfo(m_QuestData[uiQuest].Questid);
		if (pkQuest && pkQuest->uiReqHonor)
		{
			UQuestDlg* pNpcQuestDlg = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
			if(pNpcQuestDlg)
			{
				pNpcQuestDlg->GetQuestList()->UpdateTraceQuest(pkQuest->id);
			}
		}
	}
}
void UQuestTrace::UpdateText(stUIQuestInfo& vQuestInfo)
{
	URichTextCtrl* pText = NULL ;
	for(unsigned int uiQuest = 0; uiQuest < 5; uiQuest++)
	{
		if(m_QuestData[uiQuest].Questid == vQuestInfo.uiQuestId && vQuestInfo.uiQuestId)
		{
			pText = m_QuestData[uiQuest].pText ;

			pText->SetText(" ", 1);

			//获取人物的内容
			CQuestDB::stQuest* pkQuest = g_pkQusetInfo->GetQuestInfo(vQuestInfo.uiQuestId);

			NIASSERT(pkQuest);

			// 任务标题
			char szBuf[2048];
			szBuf[0] = '\0';
			int len = 0;
			BOOL CanFinishQuest = (vQuestInfo.uiQuestState == QMGR_QUEST_FINISHED);

			NiSprintf(szBuf, 2048, "<font:Arial:14><shadowcolor:202020FF><shadow:1:1><color:ff9600>%-20s%s <br><font:Arial:14>", pkQuest->strTitle.c_str(), QuestStateToUI(vQuestInfo.uiQuestState).c_str());
			//NiSprintf(szBuf, 2048, "<font:Arial:14><color:ff9600>%s", pkQuest->strTitle.c_str());

			pText->AppendText(szBuf, strlen(szBuf), TRUE);

			bool bFindPepoleQuest = true ;
			bool isFind = false ;
			
			if (!CanFinishQuest)
			{
				// 杀怪信息
				for(unsigned int ui = 0; ui < 4; ui++)
				{
					if(pkQuest->uiReqItemCount[ui] == 0)
						continue;

					bFindPepoleQuest = false ;
					ItemPrototype_Client* pkItem = ItemMgr->GetItemPropertyFromDataBase(pkQuest->uiReqItemId[ui]);
					//std::string strItemName = pkItem->C_name;
					//GetItemFindPath(pkQuest->uiReqItemId[ui], strItemName);
					if(pkItem)
					{
						char buf[256];
						char count[256];
						itoa(pkQuest->uiReqItemId[ui], buf, 10);
						itoa(pkQuest->uiReqItemCount[ui], count, 10);
						std::string strItemID = buf ;
						std::string strItemCN = count ;
						std::string strItemName = "<a:#_QUESTITEM_"+ strItemID +"_" + strItemCN +">" + pkItem->C_name + "</a> ";

						NiSprintf(szBuf, 2048, "%s : %2d / %2d<br>",
							strItemName.c_str(),
							vQuestInfo.vCreatureList[ui],
							pkQuest->uiReqItemCount[ui]);
						pText->AppendText(szBuf, strlen(szBuf), TRUE);
					}
				}

				std::string strCreatureNames[4];
				int CreatureCount = (int)vQuestInfo.vCreatureList.size();
				for(int i = 0; i < CreatureCount; ++i)
				{
					if(pkQuest->uiReqKillMobOrGOCount[i] == 0)
						continue;
					bFindPepoleQuest = false ;
					ItemMgr->GetCreatureName(pkQuest->uiReqKillMobOrGOId[i], strCreatureNames[i]);
					GetMonsterFindPath(pkQuest->uiReqKillMobOrGOId[i],  strCreatureNames[i]);
					szBuf[0] = '\0';
					NiSprintf(szBuf, 2048, "%s : %2d / %2d<br>", 
						strCreatureNames[i].c_str(),
						vQuestInfo.vCreatureList[i], 
						pkQuest->uiReqKillMobOrGOCount[i]);
					pText->AppendText(szBuf, strlen(szBuf), TRUE);
				}

				//需要荣誉
				if (pkQuest->uiReqHonor)
				{
					UINT Honor = 0;
					CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
					if (pkLocal)
					{
						Honor = pkLocal->GetUInt32Value(PLAYER_FIELD_HONOR_CURRENCY);
					}
					bFindPepoleQuest = false ;
					std::string strbuff = _TRAN( N_NOTICE_C38, _I2A(Honor).c_str(), _I2A( pkQuest->uiReqHonor ).c_str() );
					//NiSprintf(szBuf, 2048, strbuff.c_str()/* "需要荣誉 : %d / %d<br>", Honor,pkQuest->uiReqHonor*/ );
					pText->AppendText(strbuff.c_str(), strbuff.length(), TRUE);
				}
			}else 
			{
				std::string str;
				isFind = MakeFinishQuestStr(vQuestInfo.uiQuestId, str);

				if (isFind)
				{
					pText->AppendText(str.c_str(), str.length(), TRUE);
				}else
				{
					ULOG( "任务%s ： 没有找到完成NPC的数据" ,pkQuest->strTitle.c_str());
				}
			}
			if (bFindPepoleQuest && !isFind)
			{
				std::string strObjective = "　　";
				strObjective += pkQuest->strObjective.c_str();
				int iNamePos = strObjective.find("<a:#PlayerName></a>");
				if(iNamePos != -1)
				{
					std::string strPlayerName;
					strPlayerName = ObjectMgr->GetLocalPlayer()->GetName();
					std::string str1, str2;
					str1 = strObjective.substr(0, iNamePos);
					str2 = strObjective.substr(iNamePos + 19, strObjective.size() - 19 - iNamePos);
					szBuf[0] = '\0';
					NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:eaab81>%s%s%s<br>", str1.c_str(), strPlayerName.c_str(), str2.c_str());
					pText->AppendText(szBuf, strlen(szBuf), TRUE);
				}
				else
				{
					szBuf[0] = '\0';
					NiSprintf(szBuf, 2048, "<br><font:Arial:14><color:eaab81>%s<BR>", strObjective.c_str());
					pText->AppendText(szBuf, strlen(szBuf), TRUE);
				}
			}
			string strtext = "<a:#INFO_QUEST><linkcolor:FFFF00>";
			strtext.append(_TRAN("详细</a>"));
			pText->AppendText(strtext.c_str(), strtext.length(), TRUE);
			
			ReLayout();
			return ;
		}
	}
}
BOOL UQuestTrace::HitTest(const UPoint& parentCoordPoint)
{
	UPoint ptemp = parentCoordPoint - m_Position;
	for (int i = 0; i <m_Childs.size(); i++)
	{
		URichTextCtrl* pkChild = UDynamicCast(URichTextCtrl, m_Childs[i]);
		if (pkChild && pkChild->HitTest(ptemp))
		{
			return TRUE;
		}
	}
	return FALSE;
}
UControl* UQuestTrace::FindHitWindow(const UPoint &pt, INT initialLayer)
{
	UControl* pkHitWindow = UDialog::FindHitWindow(pt, initialLayer);
	/*if (pkHitWindow == this)
	{
		return  this->GetParent();
	}*/
	return pkHitWindow ;
}
void UQuestTrace::ClearAll()
{
	for ( int i =0; i<5 ; i++)
	{
		m_QuestData[i].Questid = 0;
		m_QuestData[i].pText->ClearText();
	}
}
void UQuestTrace::OnURLClick0(UNM* pNM)
{
	ClearURL(0);
	OnURLQuestInfo(pNM, 0);
	OnURLClicked(pNM);
}
void UQuestTrace::OnURLClick1(UNM* pNM)
{
	ClearURL(1);
	OnURLQuestInfo(pNM, 1);
	OnURLClicked(pNM);
}
void UQuestTrace::OnURLClick2(UNM* pNM)
{
	OnURLQuestInfo(pNM, 2);
	OnURLClicked(pNM);
	ClearURL(2);
}
void UQuestTrace::OnURLClick3(UNM* pNM)
{
	ClearURL(3);
	OnURLQuestInfo(pNM, 3);
	OnURLClicked(pNM);
}
void UQuestTrace::OnURLClick4(UNM* pNM)
{
	ClearURL(4);
	OnURLQuestInfo(pNM, 4);
	OnURLClicked(pNM);
}

void UQuestTrace::ClearURL(UINT ID)
{
	switch (ID)
	{
	case 0:
		m_QuestData[1].pText->ClearHitURL();
		m_QuestData[2].pText->ClearHitURL();
		m_QuestData[3].pText->ClearHitURL();
		m_QuestData[4].pText->ClearHitURL();

		break;
	case 1:
		m_QuestData[0].pText->ClearHitURL();
		m_QuestData[2].pText->ClearHitURL();
		m_QuestData[3].pText->ClearHitURL();
		m_QuestData[4].pText->ClearHitURL();
		break;
	case 2:
		m_QuestData[1].pText->ClearHitURL();
		m_QuestData[0].pText->ClearHitURL();
		m_QuestData[3].pText->ClearHitURL();
		m_QuestData[4].pText->ClearHitURL();;
		break;
	case 3:
		m_QuestData[1].pText->ClearHitURL();
		m_QuestData[2].pText->ClearHitURL();
		m_QuestData[0].pText->ClearHitURL();
		m_QuestData[4].pText->ClearHitURL();
		break;
	case 4:
		m_QuestData[1].pText->ClearHitURL();
		m_QuestData[2].pText->ClearHitURL();
		m_QuestData[3].pText->ClearHitURL();
		m_QuestData[0].pText->ClearHitURL();
		break;
	}
}
int UQuestTrace::FindQuestID(UINT cid)
{
	if (cid >=0 && cid < 5)
	{
		return m_QuestData[cid].Questid;
	}
	return 0 ;
}
void UQuestTrace::OnURLQuestInfo(UNM* pNM, UINT id)
{
	URichEditURLClickNM* pREDURLNM = (URichEditURLClickNM*)pNM;
	const string strURL = pREDURLNM->pszURL;
	if (strURL.compare("#INFO_QUEST") == 0)
	{
		UQuestDlg* pNpcQuestDlg = INGAMEGETFRAME(UQuestDlg, FRAME_IG_QUESTMAINDLG);
		pNpcQuestDlg->SetVisible(TRUE);
		pNpcQuestDlg->GetQuestList()->QuestContent(FindQuestID(id));
	}
}

bool UQuestTrace::MakeFinishQuestStr(ui32 Questid, std::string& str)
{
	ui32 FinishNPCEntry = 0 ;
	if (g_pkQuestFinisherDB->GetNpcId(Questid,FinishNPCEntry))
	{
		ui32 mapid;
		float x,y,z;
		std::string name;
		if ( ItemMgr->GetCreatureName(FinishNPCEntry, name))
		{
			//char buf[_MAX_PATH];
			if (g_pkCCreatureSpawnsInfo->GetCreateInfomation(FinishNPCEntry,mapid,x,y,z))
			{
				str = _TRAN( N_NOTICE_C40, _I2A(mapid).c_str(),_I2A((UINT)x).c_str(),_I2A((UINT)y).c_str(),_I2A((UINT)z).c_str(), name.c_str() );
				//sprintf(buf, "恭喜！可以找<linkcolor:137300><a:#NPC %d %d %d %d>%s</a>提交本任务了 <br>", mapid, (UINT)x, (UINT)y, (UINT)z, name.c_str());
			}else
			{
				//sprintf(buf, "<color:137300>恭喜！可以找%s提交本任务了 <br>", name.c_str());
				str = _TRAN( N_NOTICE_C41, name.c_str() );
			}
			
			return true ;
		}
	}

	return false ;
}
void UQuestTrace::OnURLClicked(UNM* pNM)
{
	OnURMoveMap(pNM);
}
void UQuestTrace::OnURLMove(UNM* pNM)
{
	OnURLItemOn(pNM);
}
void UQuestTrace::OnURLLeave()
{
	OnURLItemLeave();
}