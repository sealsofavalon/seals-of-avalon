#pragma  once

#ifndef UQUSETLOG_H
#define UQUSETLOG_H

#include "UInc.h"
#include "../../../../../SDBase/Protocol/S2C_Quest.h"
#include "USpecListBox.h"

class UBitmapButtonEx;

void OnURMoveMap(UNM* pNM);
void OnURLItemOn(UNM* pNM);
void OnURLItemLeave();
struct stUIQuestInfo
{
	stUIQuestInfo()
	{
		uiQuestSlotId = 0xff;
		uiQuestState = QMGR_QUEST_NOT_AVAILABLE;
		for (int i =0 ; i < 4 ; i++)
		{
			vCreatureList.push_back(0);
		}
	}
	static bool sTemp(const stUIQuestInfo& A, const stUIQuestInfo& B)
	{
		return (A.uiQuestId >= B.uiQuestId);
	}
	ui32 uiQuestId;
	ui32 uiQuestState;
	ui32 uiQuestSlotId;
	vector<ui32> vCreatureList;
};

struct stUIMainQuestInfo
{
	stUIQuestInfo uiMainQuest; // 主线     
	vector<stUIQuestInfo> uiChildList; //支线
};

class UQuestLog  : public UControl 
{
	UDEC_CLASS(UQuestLog);
	UDEC_MESSAGEMAP();
public:
	UQuestLog();
	~UQuestLog();
	void OnQuestContent(const stUIQuestInfo& vQuestInfo);
	void UpdataQuestContent(const stUIQuestInfo& vQuestInfo);
	void ClearAllTextContent();
	bool MakeStartQuestStr(ui32 Questid, std::string& str);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	void OnURLClicked(UNM* pNM);
	void OnURLMove(UNM* pNM);
	void OnURLLeave();
private:
	URichTextCtrl* m_pkTextCtrl;
	ui32  m_pkQuestId;
};
////////////////////////////////////////////////////////////
class UQuestList : public UControl
{
	UDEC_CLASS(UQuestList);
	UDEC_MESSAGEMAP();
public:
	UQuestList();
	~UQuestList();
	void OnQuestFinish(ui32 uiQuestId);

	void OnUpdate(
		ui32 uiQuestId, 
		ui32 uiQuestState, 
		std::vector<ui32>& vCreatureCount,
		ui32 uiQuestSlotId);

	void OrderFinishQuestTem();
	void PareseFinishQuest(MSG_S2C::stFinishQuests& Msg);

	void SetTraceQuest(ui32 Questid);
	void UpdateTraceQuest(ui32 Questid);
	
	void UpdataHonor();
	//////////////////////////////////////////////////////////////////////////


	bool GetCanFinishQuestData(uint32 MapID, vector<NiPoint3>& PosVec);

	ui32 GetQuestIdBySlot(ui32 uiQuestSlotId);

	void SetAutoQuest(BOOL bAuto){m_bAutoQuest = bAuto ;}

	void QuestContent(unsigned int QuestID);
protected:

	//////////////////////////////////////////////////////////////////////////
	// 这里处理当服务器更新下来这段内存的任务即将被放弃，
	// 这里有可能是任务被完成, 也有可能是任务被玩家放弃,
	// 如果任务是完成了的话,由于通过OnQuestFinish()函数已经更新了完成的任务的uiQuestSlotId为0xff
	// 所以通过uiQuestSlotId是查找不到完成的任务,所以这个函数只会处理玩家放弃的任务,
	// 任务放弃后,这里会设置任务状态为未激活状态
	void UpdataQuestBySlot(ui32 uiQuestSlotId,ui32 uiQuestState,std::vector<ui32>& vCreatureCount, ui32& Questid);
	//////////////////////////////////////////////////////////////////////////
	void AddMainQuest(const stUIQuestInfo& uiQuest);  //添加一个主线任务
	void UpdataQuest(const stUIQuestInfo& stQuest);			   //更新任务

	bool FindQuestByID(ui32 Questid, stUIQuestInfo& stQuest);
	//根据一个任务的信息，来查找任务的主线。返回前会更新任务的状态
	bool FindQuestMain(stUIQuestInfo& MainQuest,const stUIQuestInfo& stQuest);
	bool CheckDeleteMainQuest(ui32 MainQuestid);
	void AddMainQuestToUI(const stUIMainQuestInfo& uiQuest);
	void UpdataMainQuest(ui32 Questid); //更新UI . 
	//////////////////////////////////////////////////////////////////////////

	bool FindChildQuestList(vector<stUIQuestInfo>& MainChildQuestList, vector<stUIQuestInfo>& FinishChildQuestList);
	//////////////////////////////////////////////////////////////////////////

	
protected:

	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void SetPosition( const UPoint &NewPos );
	void OnClickDrop();
	void OnClickShare();
	void OnClickCancel();
	
	void OnSendQuest();
	void OnTraceQuest();
	void QuestContent();
	
	string QuestoString(const stUIQuestInfo& uiQuest);
	ui64 m_Guid;
private:
	USpecListBox* m_QuestList;
	//UQuestLog * m_QuestLog;

	BOOL m_bAutoQuest ;// 任务追踪面板是否自动追踪任务
	//获取主线信息
	vector<stUIMainQuestInfo> m_vMainQuestInfo;   //如果主线或者支线有一个没完成,则还在这里
	vector<stUIMainQuestInfo> m_vFanisheQuestInfo;  //如果主线所有的支线完成后,丢进这里处理

	//由于LocalPlayer 没创建好就收到了完成的任务的列表
	//导致本地无法处理,这里实现存储起来完成的任务.
	//然后等本地玩家能更新消息后,在来处理这个列表.
	//处理完成后即可.
	vector<ui32>m_QuestFinishTem;




public:
	void ParseQuestEscortNotify(std::string who,uint32 qst_id,uint32 npc_id);
	static void DO_AcceptQuestEscort();
	static void DO_Nothing();
	static uint32 mQstID;
	static uint32 mNpcId;
};

class UQuestDlg : public UDialog
{
	UDEC_CLASS(UQuestDlg);
public:
	UQuestDlg();
	~UQuestDlg();
public:
	UQuestList* GetQuestList(){return m_pQuestList;}
	UQuestLog* GetQuestLog(){return m_pQuestLog;}
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
protected:
	UQuestList* m_pQuestList;
	UQuestLog* m_pQuestLog;
};

class UHideQuestTraceButton : public UControl
{
	UDEC_CLASS(UHideQuestTraceButton);
	UDEC_MESSAGEMAP();
public:
	virtual void OnDestroy();
	UHideQuestTraceButton();
	~UHideQuestTraceButton();
public:
	void OnSetChatHide();
	void OnSetChatShow();
};
class UQuestTrace : public UDialog
{
	UDEC_CLASS(UQuestTrace);
	UDEC_MESSAGEMAP();

public:
	UQuestTrace();
	~UQuestTrace();

	void SetTraceVis(BOOL vis);
	void AddText(stUIQuestInfo& vQuestInfo);
	void UpdateText(stUIQuestInfo& vQuestInfo);
	void DeleteText(unsigned int QuestID);
	bool IsHasQuest(unsigned int QuestID);

	void UpdataHonor();

	unsigned int GetCurTraceCount() ;
	unsigned int GetLastQuestId() ;

	void ClearAll();
	
protected:
	void OnURLClick0(UNM* pNM);
	void OnURLClick1(UNM* pNM);
	void OnURLClick2(UNM* pNM);
	void OnURLClick3(UNM* pNM);
	void OnURLClick4(UNM* pNM);
	void OnURLQuestInfo(UNM* pNM, UINT id);
	int FindQuestID(UINT cid);
	void ClearURL(UINT ID);

	void OnURLClicked(UNM* pNM);
	void OnURLMove(UNM* pNM);
	void OnURLLeave();

	bool MakeFinishQuestStr(ui32 Questid, std::string& str);

	BOOL CheckQuestData();
	void ReLayout();
	int CalIndex();

	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL HitTest(const UPoint& parentCoordPoint);
	virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer = -1);

	struct TraceData 
	{
		URichTextCtrl* pText ;
		ui32 Questid ;

		TraceData()
		{
			pText = NULL ;
			Questid = 0;
		}
	};

	TraceData m_QuestData[5];
	BOOL m_Hide;
	BOOL m_MouseFlag;
};
#endif