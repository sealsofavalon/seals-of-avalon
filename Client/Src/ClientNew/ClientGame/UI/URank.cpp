#include "stdafx.h"
#include "URank.h"
#include "RankMgr.h"
#include "UFun.h"

UIMP_CLASS(URank, UDialog)
UBEGIN_MESSAGE_MAP(URank, UDialog)
UON_BN_CLICKED(1,&URank::OnClickTable)
UON_BN_CLICKED(2,&URank::OnClickTable)
UON_BN_CLICKED(3,&URank::OnClickTable)
UON_BN_CLICKED(4,&URank::OnClickTable)
UON_BN_CLICKED(5,&URank::OnClickTable)
UON_BN_CLICKED(6,&URank::OnClickTable)
UON_BN_CLICKED(7,&URank::OnClickTable)
//UON_BN_CLICKED(8,&URank::ChangeCHARGE)

UON_BN_CLICKED(9,&URank::ChangeRace_All)
UON_BN_CLICKED(10,&URank::ChangeRace_Ren)
UON_BN_CLICKED(11,&URank::ChangeRace_Yao)
UON_BN_CLICKED(12,&URank::ChangeRace_Wu)

UON_BN_CLICKED(13,&URank::PageDown)
UON_BN_CLICKED(15,&URank::PageUP)
UON_BN_CLICKED(17,&URank::GotoPage)

UON_BN_CLICKED(19,&URank::SeachDataByName)

UON_BN_CLICKED(21,&URank::DoNothing)
UON_BN_CLICKED(22,&URank::DoNothing)
UON_BN_CLICKED(23,&URank::DoNothing)
UON_BN_CLICKED(24,&URank::DoNothing)
UON_BN_CLICKED(25,&URank::DoNothing)
UON_BN_CLICKED(26,&URank::TablePrev)
UON_BN_CLICKED(27,&URank::TableNext)
UEND_MESSAGE_MAP()


#define RANKPAGECOUNT 14

void URank::StaticInit()
{

}
URank::URank()
{
	m_ShowList = NULL;
	m_curPage = 0;
	m_numPage = 0;
	m_SeachName = NULL;
	m_GotoPage = NULL;

	m_ShowBtn[0] = NULL;
	m_ShowBtn[1] = NULL;
	m_ShowBtn[2] = NULL;
	m_ShowBtn[3] = NULL;
	m_ShowBtn[4] = NULL;

	m_stType = LADDER_LEVEL;

	m_bCanDrag = FALSE ;

	m_SeachList.clear();
	m_bInSeach = FALSE;
	mTableIndex = 0;
}
URank::~URank()
{

}
void URank::OnClose()
{
	UDialog::OnClose();
	SetVisible(FALSE);
	SetFocusControl(sm_System->GetCurFrame());
}
BOOL URank::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		return TRUE;
	}
	return FALSE;
}
BOOL URank::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}
	
	m_CloseBtn->SetPosition(UPoint(604,56));
	m_SeachName = UDynamicCast(UEdit, GetChildByID(18));
	m_GotoPage = UDynamicCast(UEdit, GetChildByID(16));

	if (!m_SeachName || !m_GotoPage)
	{
		return FALSE;
	}

	m_GotoPage->SetNumberOnly(TRUE);
	m_ShowBtn[0] = UDynamicCast(UBitmapButton, GetChildByID(21));
	m_ShowBtn[1] = UDynamicCast(UBitmapButton, GetChildByID(22));
	m_ShowBtn[2] = UDynamicCast(UBitmapButton, GetChildByID(23));
	m_ShowBtn[3] = UDynamicCast(UBitmapButton, GetChildByID(24));
	m_ShowBtn[4] = UDynamicCast(UBitmapButton, GetChildByID(25));


	if (!m_ShowBtn[0] || !m_ShowBtn[1] || !m_ShowBtn[2] || !m_ShowBtn[3] || !m_ShowBtn[4])
	{
		return FALSE ;
	}

	m_ShowBtn[0]->SetActive(FALSE);
	m_ShowBtn[1]->SetActive(FALSE); 
	m_ShowBtn[2]->SetActive(FALSE); 
	m_ShowBtn[3]->SetActive(FALSE); 
	m_ShowBtn[4]->SetActive(FALSE);


	m_ShowList = UDynamicCast(USpecListBox, GetChildByID(20));
	if (!m_ShowList)
	{
		return FALSE;
	}

	m_ShowList->SetGridSize(UPoint(455 ,20));
	m_ShowList->SetGridCount(15, 1);

	m_ShowList->SetCol1(0, 0);
	m_ShowList->SetWidthCol1(65);

	m_ShowList->SetCol2(65, 0);
	m_ShowList->SetWidthCol2(133);

	m_ShowList->SetCol3(198, 0);
	m_ShowList->SetWidthCol3(65);

	m_ShowList->SetCol4(263, 0);
	m_ShowList->SetWidthCol4(65);

	m_ShowList->SetCol5(328, 0);
	m_ShowList->SetWidthCol5(127);
	m_ShowList->SetActive(FALSE);

	UBitmapButton* pkLevel = UDynamicCast(UBitmapButton, GetChildByID(1));
	if (pkLevel)
	{
		pkLevel->SetCheckState(TRUE);
	}
	UBitmapButton* pkRaceAll = UDynamicCast(UBitmapButton, GetChildByID(9));
	if (pkRaceAll)
	{
		pkRaceAll->SetCheckState(TRUE);
	}

	m_SeachList.clear();
	m_bInSeach = FALSE;
//#ifndef TEST_CLIENT
	for(int i = 0; i < 7 ; i++)
	{
		UButton* pbtn = UDynamicCast(UButton, GetChildByID(i + 1));
		if(pbtn )
		{
			int category = min(mTableIndex + LADDER_KILL_COUNT + i, LADDER_MAX - 1);
			pbtn->SetText(_TRAN(cladderCategory[category]));
		}
	}
//#else
//	for(int i = 0; i < 7 ; i++)
//	{
//		UButton* pbtn = UDynamicCast(UButton, GetChildByID(i + 1));
//		if(pbtn )
//		{
//			int category = ShowLadder_category[mTableIndex + i];
//			category = min(category, LADDER_MAX - 1);
//			pbtn->SetText(cladderCategory[category]);
//		}
//	}
//#endif

	return TRUE;
}
void URank::OnDestroy()
{
	mTableIndex = 0;
	UControl::OnDestroy();
}
bool URank::IsInSeach()
{
	return m_bInSeach?true:false ;
}
void URank::InitUIList(int stType)
{
	switch(stType)
	{
	case LADDER_KILL_COUNT:
		m_ShowBtn[1]->SetText( _TRAN("玩家") );
		m_ShowBtn[3]->SetText(_TRAN("职业") );
		m_ShowBtn[4]->SetText(_TRAN("击杀") );
		break;
	case LADDER_BE_KILL_COUNT:
		m_ShowBtn[1]->SetText(_TRAN("玩家") );
		m_ShowBtn[3]->SetText(_TRAN("职业") );
		m_ShowBtn[4]->SetText(_TRAN("被击杀") );
		break;
	case LADDER_TOTAL_HONOR:
		m_ShowBtn[1]->SetText(_TRAN("玩家") );
		m_ShowBtn[3]->SetText(_TRAN("职业") );
		m_ShowBtn[4]->SetText(_TRAN("荣誉") );
		break;
	case LADDER_LEVEL:
		m_ShowBtn[1]->SetText(_TRAN("玩家") );
		m_ShowBtn[3]->SetText(_TRAN("职业") );
		m_ShowBtn[4]->SetText(_TRAN("等级") );
		break;
	case LADDER_COIN:
		m_ShowBtn[1]->SetText(_TRAN("玩家") );
		m_ShowBtn[3]->SetText(_TRAN("职业") );
		m_ShowBtn[4]->SetText(_TRAN("金钱(金)") );
		break;
	case LADDER_TOTAL_CONTRIBUTE:
		m_ShowBtn[1]->SetText(_TRAN("玩家") );
		m_ShowBtn[3]->SetText(_TRAN("职业") );
		m_ShowBtn[4]->SetText(_TRAN("捐助") );
		break;
	case LADDER_TOTAL_CHARGE:
		m_ShowBtn[1]->SetText(_TRAN("玩家") );
		m_ShowBtn[3]->SetText(_TRAN("职业") );
		m_ShowBtn[4]->SetText(_TRAN("充值") );
		break;
	case LADDER_GUILD_LEVEL:
		m_ShowBtn[1]->SetText(_TRAN("部族") );
		m_ShowBtn[3]->SetText(_TRAN("等级") );
		m_ShowBtn[4]->SetText(_TRAN("会长") );
		break;	
	case LADDER_ARENA_1V1:
	case LADDER_ARENA_2V2:
	case LADDER_ARENA_3V3:
	case LADDER_ARENA_4V4:
	case LADDER_ARENA_5V5:
	case LADDER_ARENA_LV80_1V1:
	case LADDER_ARENA_LV80_2V2:
	case LADDER_ARENA_LV80_3V3:
	case LADDER_ARENA_LV80_4V4:
	case LADDER_ARENA_LV80_5V5:
		m_ShowBtn[4]->SetText( _TRAN("胜利场数") );
		break;	
	}
	m_stType = stType ;
}
void URank::SetDataList(ladder_vector pkData)
{
	m_SeachList.clear();
	m_bInSeach = FALSE;
	m_curData.clear();
	for (size_t ui = 0; ui < pkData.size(); ui++)
	{
		m_curData.push_back(pkData[ui]);
	}
	
	m_numPage = m_curData.size() / RANKPAGECOUNT;
	if (m_curData.size() % RANKPAGECOUNT)
	{
		m_numPage ++ ;
	}

	m_curPage = 0; 

	ShowPageList(); 
}
void URank::SHowSeachList()
{
	m_ShowList->Clear();
	for (int i = 0; i < RANKPAGECOUNT; i++)
	{
		UINT ui  = m_curPage * RANKPAGECOUNT + i ;
		if (ui < m_SeachList.size())
		{
			size_t index =  m_SeachList[ui];
			AddListData(index);
		}
	}

	UStaticText* pkText = UDynamicCast(UStaticText, GetChildByID(14));
	if (pkText)
	{
		char buf[_MAX_PATH];
		UINT uiShow = m_numPage ;
		if (m_numPage == 0)
		{
			uiShow++;
		}
		sprintf_s(buf,_MAX_PATH,"%d/%d ", m_curPage +1 , uiShow);
		pkText->SetText(buf);
	}
}

void URank::ShowPageList()
{
	m_ShowList->Clear();
	for (int i = 0; i < RANKPAGECOUNT; i++)
	{
		UINT index  = m_curPage * RANKPAGECOUNT + i ;
		AddListData(index);
	}

	UStaticText* pkText = UDynamicCast(UStaticText, GetChildByID(14));
	if (pkText)
	{
		char buf[_MAX_PATH];
		UINT uiShow = m_numPage ;
		if (m_numPage == 0)
		{
			uiShow++;
		}
		sprintf_s(buf,_MAX_PATH,"%d/%d ", m_curPage +1 , uiShow);
		pkText->SetText(buf);
	}
}

void URank::ChangeKill()   //英雄
{
	RankMgr->ChangeType(LADDER_KILL_COUNT);
}
void URank::ChangeBeKill()  //衰神
{
	RankMgr->ChangeType(LADDER_BE_KILL_COUNT);
}
void URank::ChangeHONOR()  //荣誉
{
	RankMgr->ChangeType(LADDER_TOTAL_HONOR);
}
void URank::ChangeLEVEL() //等级
{
	RankMgr->ChangeType(LADDER_LEVEL);
}
void URank::ChangeCOIN() //金钱
{
	RankMgr->ChangeType(LADDER_COIN);
}
void URank::ChangeCONTRIBUTE() //慈善
{
	RankMgr->ChangeType(LADDER_TOTAL_CONTRIBUTE);
}
void URank::ChangeGUILD() //工会
{	
	RankMgr->ChangeType(LADDER_GUILD_LEVEL);
}
void URank::ChangeCHARGE() //充值
{
	RankMgr->ChangeType(LADDER_TOTAL_CHARGE);
}
void URank::ChangeRace_All()
{
	RankMgr->ChangRace(RACE_MIN);
}
void URank::ChangeRace_Ren()
{
	RankMgr->ChangRace(RACE_REN);
}
void URank::ChangeRace_Yao()
{
	RankMgr->ChangRace(RACE_YAO);
}
void URank::ChangeRace_Wu()
{
	RankMgr->ChangRace(RACE_WU);
}
void URank::DoNothing()
{

}
//#ifndef TEST_CLIENT
void URank::OnClickTable()
{
	for(int i = 0; i < 7 ; i++)
	{
		UButton* pbtn = UDynamicCast(UButton, GetChildByID(i + 1));
		if(pbtn && pbtn->IsChecked())
		{
			RankMgr->ChangeType(mTableIndex + LADDER_KILL_COUNT + i);
		}
	}
}

void URank::TableNext()
{
	mTableIndex++;
	if(mTableIndex + 6 >= LADDER_MAX )
	{
		mTableIndex = LADDER_MAX - 6;
		return;
	}
	TableChange();
}

void URank::TableChange()
{
	for(int i = 0; i < 7 ; i++)
	{
		UButton* pbtn = UDynamicCast(UButton, GetChildByID(i + 1));
		if(pbtn )
		{
			int category = min(mTableIndex + LADDER_KILL_COUNT + i, LADDER_MAX - 1);
			pbtn->SetText(_TRAN(cladderCategory[category]));
			if(pbtn->IsChecked())
				RankMgr->ChangeType(category);
		}
	}
}
//#else
//void URank::OnClickTable()
//{
//	for(int i = 0; i < 7 ; i++)
//	{
//		UButton* pbtn = UDynamicCast(UButton, GetChildByID(i + 1));
//		if(pbtn && pbtn->IsChecked())
//		{
//			int category = ShowLadder_category[mTableIndex + i];
//			category = min(category, LADDER_MAX - 1);
//			RankMgr->ChangeType(category);
//		}
//	}
//}
//
//void URank::TableNext()
//{
//	int maxShow = sizeof(ShowLadder_category) / sizeof(ShowLadder_category[0]) - 1;
//	mTableIndex++;
//	if(mTableIndex + 6 >=  maxShow )
//	{
//		mTableIndex = maxShow - 6;
//		return;
//	}
//	TableChange();
//}
//void URank::TableChange()
//{
//	for(int i = 0; i < 7 ; i++)
//	{
//		UButton* pbtn = UDynamicCast(UButton, GetChildByID(i + 1));
//		if(pbtn )
//		{
//			int category = ShowLadder_category[mTableIndex + i];
//			category = min(category, LADDER_MAX - 1);
//			pbtn->SetText(cladderCategory[category]);
//			if(pbtn->IsChecked())
//				RankMgr->ChangeType(category);
//		}
//	}
//}
//#endif

void URank::TablePrev()
{
	mTableIndex--;
	if(mTableIndex < 0)
	{
		mTableIndex = 0;
		return;
	}
	TableChange();
}

void URank::PageUP()
{
	if (m_curPage < m_numPage - 1)
	{
		m_curPage++;
		if (m_bInSeach)
		{
			SHowSeachList();
		}else
		{
			ShowPageList();
		}
	}
}
void URank::PageDown()
{
	if (m_curPage > 0)
	{
		m_curPage-- ;
		if (m_bInSeach)
		{
			SHowSeachList();
		}else
		{
			ShowPageList();
		}
	}
}
void URank::GotoPage()
{
	char buf[_MAX_PATH];
	if (m_GotoPage->GetText(buf,_MAX_PATH, 0))
	{
		UINT page = atoi(buf);
		if (page > 0 && page <= m_numPage && (page - 1) != m_curPage)
		{
			m_curPage = page - 1 ;
			if (m_bInSeach)
			{
				SHowSeachList();
			}else
			{
				ShowPageList();
			}
		}
	}
	m_GotoPage->Clear();

}
void URank::AddListData(size_t index)
{
	if (index < m_curData.size())
	{
		std::string strRace = GetRaceStr(m_curData[index].nrace);
		if (m_stType < LADDER_MAX)
		{
			char stText[512];
			if (m_stType == LADDER_COIN)
			{
				std::string strClass = GetClassStr(m_curData[index].nclass);
				UINT Gold = m_curData[index].key / 10000;
				UINT Silver = (m_curData[index].key  - Gold*10000) / 100;
				UINT Copper = m_curData[index].key % 100;
				UINT pkMoney = Gold;
				NiSprintf(stText, 512, 
					"<Col1><center>%d<end><Col2><center>%s<end><Col3><center>%s<end><Col4><center>%s<end><Col5>%d<end>",
					index+1,
					m_curData[index].playername.c_str(), 
					strRace.c_str(),
					strClass.c_str(),
					Gold
					);

			}else if (m_stType != LADDER_GUILD_LEVEL)
			{
				std::string strClass = GetClassStr(m_curData[index].nclass);
				NiSprintf(stText, 512, 
					"<Col1><center>%d<end><Col2><center>%s<end><Col3><center>%s<end><Col4><center>%s<end><Col5>%d<end>",
					index+1,
					m_curData[index].playername.c_str(), 
					strRace.c_str(),
					strClass.c_str(), 
					m_curData[index].key);
			}else
			{

				int pos = m_curData[index].playername.find(" ");
				std::string guildName = m_curData[index].playername.substr(0,pos);
				std::string LeaderName = m_curData[index].playername.substr(pos+1, m_curData[index].playername.length() -1);

				NiSprintf(stText, 512, 
					"<Col1><center>%d<end><Col2><center>%s<end><Col3><center>%s<end><Col4><center>%d<end><Col5><center>%s<end>",
					index+1,
					guildName.c_str(), 
					strRace.c_str(), 
					m_curData[index].key,
					LeaderName.c_str());
			}
			//显示单个选项。。。
			m_ShowList->AddItem(stText);
		}
	}
}

void URank::SeachDataByName()
{
	m_SeachList.clear();
	char buf[_MAX_PATH];
	if (m_SeachName->GetText(buf,_MAX_PATH, 0))
	{
		for (size_t ui = 0; ui < m_curData.size() ; ui++)
		{
			if (m_curData[ui].playername.find(buf) != string::npos)
			{
				m_SeachList.push_back(ui);
			}
		}
		if (m_SeachList.size())
		{
			m_bInSeach = TRUE;
			m_numPage = m_SeachList.size() / RANKPAGECOUNT;
			if (m_SeachList.size() % RANKPAGECOUNT)
			{
				m_numPage ++ ;
			}
			m_curPage = 0;
			SHowSeachList();
		}else
		{
			ClientSystemNotify( _TRAN("没有搜索到相关数据！") );
			RankMgr->ChangeType(m_stType);
		}
	}else
	{
		ClientSystemNotify( _TRAN("请输入查询名字！") );
	}

	m_SeachName->Clear();
}

std::string URank::GetRaceStr(uint8 stRace)
{
	switch(stRace)
	{
	case RACE_REN:
	case RACE_WU:
	case RACE_YAO:
		return _TRAN(race_name[stRace]);
	}
	return "";
}
std::string URank::GetClassStr(uint8 stClasses)
{
	switch(stClasses)
	{
	case CLASS_WARRIOR:
		return _TRAN("武修");
	case CLASS_BOW:
		return _TRAN("羽箭");
	case CLASS_PRIEST:
		return _TRAN("仙道");
	case CLASS_MAGE:
		return _TRAN("真巫");
	}
	return "";
}