#ifndef __URANK_H__
#define __URANK_H__

#include "UInc.h"
#include "USpecListBox.h"
#include "../../../../SDBase/Public/PlayerDef.h"

//分页显示数量
class URank : public UDialog
{
	UDEC_CLASS(URank);
	UDEC_MESSAGEMAP();
public:
	URank();
	~URank();
	void SetDataList(ladder_vector pkData);
	void InitUIList(int stType);
	bool IsInSeach();
protected:
	//响应函数
	// 改变类型
	void ChangeKill();   //英雄
	void ChangeBeKill();  //被杀
	void ChangeHONOR();  //荣誉
	void ChangeLEVEL(); //等级
	void ChangeCOIN(); //金钱
	void ChangeCONTRIBUTE(); //慈善
	void ChangeGUILD(); //工会
	void ChangeCHARGE(); //充值

	void OnClickTable();
	void TableNext();
	void TablePrev();

	void TableChange();
	int mTableIndex;


	//改变种族
	void ChangeRace_All();
	void ChangeRace_Ren();
	void ChangeRace_Yao();
	void ChangeRace_Wu();


	// do nothing 
	void DoNothing();


	//翻页操作
	void PageUP();
	void PageDown();
	void GotoPage();

	//查找操作
	void SeachDataByName();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
public:
	virtual void OnClose();

protected:
	void ShowPageList();
	void AddListData(size_t index);
	void SHowSeachList();

	std::string GetRaceStr(uint8 stRace);
	std::string GetClassStr(uint8 stClasses);
private:
	USpecListBox* m_ShowList ;
	ladder_vector  m_curData ;
	UBitmapButton* m_ShowBtn[5];
	UEdit* m_SeachName ;
	UEdit* m_GotoPage;
	
	UINT m_curPage;   //当前页数 
	UINT m_numPage;   //总页数

	int m_stType ;
	std::vector<size_t> m_SeachList ; //搜索出来的列表的下标。
	BOOL m_bInSeach ;
};
#endif