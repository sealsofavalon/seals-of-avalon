#include "stdafx.h"
#include "URefineEqu.h"
#include "UIItemSystem.h"
#include "UIPackage.h"
#include "ItemManager.h"
#include "UIGamePlay.h"
#include "UEquRefineManager.h"
#include "UChat.h"
#include "UMessageBox.h"
#include "AudioDef.h"
#include "ObjectManager.h"
#include "UFun.h"
#include "Utils/ItemEnchant.h"
#include "ui/UIMoneyText.h"
#include "Skill/SkillManager.h"

#define  RefineBarTimeMax 3.0f
#define  RefineWaitTime 1.0f
#define  RefineEffectTime 1.7f

UIMP_CLASS(URefineEqu, UDialog);
UBEGIN_MESSAGE_MAP(URefineEqu,UDialog)
UON_BN_CLICKED(9,&URefineEqu::OnStart)
UON_BN_CLICKED(10,&URefineEqu::OnRefine)
UON_BN_CLICKED(11,&URefineEqu::OnCompound)
UON_BN_CLICKED(12,&URefineEqu::OnEmbed)
UEND_MESSAGE_MAP()

void URefineEqu::StaticInit()
{

}

URefineEqu::URefineEqu()
{
	m_RefineType = t_Refine;
	m_BarTime = 0.0f;;
	sm_RefineState = _Idle_State_;
	m_EffectControl = NULL ;
	m_EffectTime = 0.0f;

	m_RefineText = NULL;
}
URefineEqu::~URefineEqu()
{

}

BOOL URefineEqu::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}
	
	/*if (m_CloseBtn)
	{
		URect CloseRec(488,16, 506,34);
		m_CloseBtn->SetWindowRect(CloseRec);
	}*/

	if (m_EffectControl == NULL)
	{
		m_EffectControl = UDynamicCast(UNiAVControlEffRe, GetChildByID(15));
	}
	if (!m_EffectControl)
	{
		return FALSE ;
	}
	UMoneyText* pkMoney = UDynamicCast(UMoneyText, GetChildByID(7));
	if (pkMoney)
	{
		pkMoney->SetMoney(0);
	}

	UScrollBar* pQuestBar = UDynamicCast(UScrollBar, GetChildByID(14));
	if (pQuestBar == NULL)
	{
		return FALSE;
	}
	pQuestBar->SetBarShowMode(USBSM_FORCEON,USBSM_FORCEOFF);


	if (m_RefineText == NULL)
	{
		m_RefineText =UDynamicCast(URichTextCtrl, pQuestBar->GetChildByID(16));
	}

	if (m_RefineText == NULL)
	{
		return FALSE ;
	}
	if (!InitRefineBtn())
	{
		return FALSE;
	}
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(10));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}

	SetVisible(FALSE);
	
	return TRUE;
}
BOOL URefineEqu::InitRefineBtn()
{
	for (int i =0 ; i < 6; i++)
	{
		URefineBtn* pkBtn = UDynamicCast(URefineBtn,GetChildByID(i));
		if (!pkBtn)
		{
			return FALSE;
		}

		ActionDataInfo dataInfo;
		dataInfo.entry = 0;
		dataInfo.num = 0;
		dataInfo.Guid = 0;
		dataInfo.type = UItemSystem::ITEM_DATA_FLAG;
		dataInfo.pos = i ;

		pkBtn->SetItemData(dataInfo);
		pkBtn->SetActive(TRUE);
	}
	for (int i = 0 ;i < 3 ; i++)
	{
		URefineBtn* pkBtn = UDynamicCast(URefineBtn,GetChildByID(i + 20));
		if (!pkBtn)
		{
			return FALSE;
		}

		ActionDataInfo dataInfo;
		dataInfo.entry = 0;
		dataInfo.num = 0;
		dataInfo.Guid = 0;
		dataInfo.type = UItemSystem::ITEM_DATA_FLAG;
		dataInfo.pos = i ;

		pkBtn->SetItemData(dataInfo);
		pkBtn->SetActive(TRUE);
	}

	m_RefineType = t_Refine;
	//m_BarTime = 0.0f;
	SetCurState(_Idle_State_);

	UBitmapButton* pkRefine = UDynamicCast(UBitmapButton, GetChildByID(10));
	if (pkRefine)
	{
		pkRefine->SetCheck(TRUE);
	}
	SetInitInfoText(m_RefineType);

	GetChildByID(10)->SetActive(TRUE);
	return TRUE;
}

void URefineEqu::SetInitInfoText(RefineType type)
{
	char* buf[] = {
		"DATA\\UI\\Refine\\RefineInfo.txt",
		"DATA\\UI\\Refine\\EmbleInfo.txt"
	};
	m_RefineText->ClearText();
	char* acBuffer = (char*)malloc( 256*1024 );
	NiFile* pkFile = NiFile::GetFile(buf[type], NiFile::READ_ONLY);
	if(!pkFile || !(*pkFile))
	{
		NiDelete pkFile;
		free( acBuffer );
	}
	unsigned int uiSize = pkFile->Read(acBuffer, 256*1024);
	NIASSERT(uiSize < 256*1024 - 1);
	acBuffer[uiSize] = '\0';
	NiDelete pkFile;

	string str = AnisToUTF8(acBuffer);
	m_RefineText->SetText(str.c_str(), str.length());
	m_RefineText->FormatContent();
}

void URefineEqu::SetActiveRefineBtn(UINT id , BOOL bActive)
{
	if ( id >= 0 && id < 6)
	{
		URefineBtn* pkBtn = UDynamicCast(URefineBtn,GetChildByID(id));
		if (!pkBtn)
		{
			return ;
		}
		pkBtn->SetActive(bActive);
	}
}
void URefineEqu::OnDestroy()
{
	m_Visible = FALSE;
	UDialog::OnDestroy();
}
void URefineEqu::OnClose()
{
	SetVisible(FALSE);
	UButton* pBtn = UDynamicCast(UButton, GetChildByID(10));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	EquRefineMgr->ClearSocketGemList();
}
BOOL URefineEqu::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
		
	}
	return TRUE;
	
}
void URefineEqu::OnTimer(float fDeltaTime)
{
	UDialog::OnTimer(fDeltaTime);
	if (sm_RefineState == _DoBar_State_)
	{
		m_BarTime += fDeltaTime;
		DoBar();
	}
	if (sm_RefineState == _DoEffect_State_)
	{
		m_EffectTime += fDeltaTime;
		DoEffectTime();
	}
	
}
void URefineEqu::LockRefineType(BOOL bLock)
{
	if (bLock)
	{
		if (m_RefineType == t_Refine)
		{
			GetChildByID(10)->SetActive(TRUE);
			//GetChildByID(11)->SetActive(FALSE);
			GetChildByID(12)->SetActive(FALSE);
		}

		if (m_RefineType == t_Embed)
		{
			GetChildByID(10)->SetActive(FALSE);
			//GetChildByID(11)->SetActive(TRUE);
			GetChildByID(12)->SetActive(FALSE);
		}
	}else
	{
		GetChildByID(10)->SetActive(TRUE);
		//GetChildByID(11)->SetActive(TRUE);
		GetChildByID(12)->SetActive(TRUE);
	}
}
void URefineEqu::SetCurState(RefineState state)
{
	sm_RefineState = state ;

	if (state != _DoBar_State_  && state != _DoEffect_State_)
	{
		m_BarTime = 0.0f;

		URefineBar* pkBar = UDynamicCast(URefineBar,GetChildByID(6));
		if (pkBar)
		{
			pkBar->SetProgressPos(0,1);
		}
		pkBar->SetTextVisible(FALSE);
	}

	if (state != _DoEffect_State_)
	{
		m_EffectTime = 0.0f;
		m_EffectControl->SetVisible(FALSE);
	}else
	{
		m_EffectControl->SetVisible(TRUE);
	}

	BOOL bIdle = TRUE ;
	if (sm_RefineState != _Idle_State_)
	{
		bIdle = FALSE;
	}
	GetChildByID(9)->SetActive(bIdle);
	LockRefineType(!bIdle);
	for (int i = 0; i< 6; i++)
	{
		SetActiveRefineBtn(i, bIdle);
	}

	
}

void URefineEqu::DoBar()
{
	UProgressBar* pkBar = UDynamicCast(UProgressBar,GetChildByID(6));
	if (pkBar)
	{
		if (m_BarTime >= RefineBarTimeMax)
		{
			pkBar->SetProgressPos(1000,1000);

		}else
		{
			pkBar->SetProgressPos(ui32(m_BarTime * 1000), ui32(RefineBarTimeMax * 1000));
		}
		pkBar->SetTextVisible(FALSE);
	}
}
void URefineEqu::DoEffectTime() // 成功或失败特效控制
{
	if (m_EffectTime >= RefineEffectTime)
	{
		
		SetCurState(_Idle_State_);
		//m_EffectTime = 0.0f;
	}else
	{
		m_EffectControl->OnUpdateOBJ(m_EffectTime);
	}
}

void URefineEqu::DoEffectAndMsg(BOOL bSuccess)
{
	if (m_EffectControl)
	{
		//m_EffectControl->SetVisible(TRUE);
		if (bSuccess)
		{
			m_EffectControl->SetObjectFile("data\\ui\\refine\\EA0021.nif");  //成功
			sm_System->__PlaySound(UI_makesuccess);
		}else
		{
			m_EffectControl->SetObjectFile("data\\ui\\refine\\EA0022.nif"); //失败
			sm_System->__PlaySound(UI_makefail);
		}
	}


	if (m_RefineType == t_Refine)
	{
		if (bSuccess)
		{
			ClientSystemNotify( _TRAN("恭喜您，精炼物品成功！") );
		}else
		{
			ClientSystemNotify( _TRAN("对不起，精炼物品失败！") );
		}
	}
}
void URefineEqu::ParseResult(ui8 bSuccess)
{
	URefineBar* pkBar = UDynamicCast(URefineBar,GetChildByID(6));

	if (bSuccess == JINGLIAN_RESULT_SUCCESS)
	{
		DoEffectAndMsg(TRUE);

		SetCurState(_DoEffect_State_);
		
		if (pkBar)
		{
			pkBar->SetShow(URefineBar::ShowSuccess);
		}

	}else if (bSuccess == JINGLIAN_RESULT_FAILED)
	{
		DoEffectAndMsg(FALSE);

		SetCurState(_DoEffect_State_);

		if (pkBar)
		{
			pkBar->SetShow(URefineBar::ShowFaild);
		}

	}else if (bSuccess == JINGLIAN_RESULT_CHECK_SUCCESS)
	{
		SetCurState(_DoBar_State_);
	}else if(bSuccess == JINGLIAN_SPELL_USE_FAILD)
	{
		SetCurState(_Idle_State_);
	}
	else
	{
		SetCurState(_Idle_State_);
	}
}
BOOL URefineEqu::CheckCanStart()
{
	//先判断状态，
	// 然后判断是否有2种以上的材料
	if(m_RefineType == t_Refine)
	{
		if (sm_RefineState == _Idle_State_)
		{
			UINT Count = 0;
			URefineBtn* pkBtn = UDynamicCast(URefineBtn,GetChildByID(0));
			if (pkBtn)
			{

				if (pkBtn->GetItemData() && pkBtn->GetItemData()->Guid == 0)
				{
					UMessageBox::MsgBox_s(_TRAN("请放入精炼装备！"));
					return FALSE ;
				}
			}
			for (int id = 1; id < 6; id++)
			{
				URefineBtn* pkMBtn = UDynamicCast(URefineBtn,GetChildByID(id));
				if (pkMBtn)
				{
					if (pkMBtn->GetItemData() && pkMBtn->GetItemData()->Guid > 0)
					{
						Count ++ ;
					}
				}
			}
			if (Count >= 1)
			{
				return TRUE ;
			}else
			{
				UMessageBox::MsgBox_s( _TRAN("请放入精炼材料！") );
				return FALSE ;
			}
		}
	}
	if( m_RefineType == t_Embed )
	{
		return TRUE;
	}
	return FALSE;
}
#include "Network/PacketBuilder.h"
void URefineEqu::OnStart()     //
{
	if (EquRefineMgr && CheckCanStart())
	{
		if (m_RefineType == t_Refine)
		{
			if (EquRefineMgr->SendRefineMsg(m_RefineType))
			{
				SetCurState(_Wait_State_);
			}
		}else if (m_RefineType == t_Embed)
		{
			EquRefineMgr->SendRefineMsg(m_RefineType);
		}
	}
}
void URefineEqu::ChangeRefineType()
{
	ActionDataInfo dataInfo;
	dataInfo.entry = 0;
	dataInfo.num = 0;
	dataInfo.Guid = 0;
	dataInfo.type = UItemSystem::ITEM_DATA_FLAG;
	

	URefineBtn* pkBtn[9] ;
	pkBtn[0] = UDynamicCast(URefineEqu::URefineBtn,GetChildByID(0));
	pkBtn[1] = UDynamicCast(URefineEqu::URefineBtn,GetChildByID(1));
	pkBtn[2] = UDynamicCast(URefineEqu::URefineBtn,GetChildByID(2));
	pkBtn[3] = UDynamicCast(URefineEqu::URefineBtn,GetChildByID(3));
	pkBtn[4] = UDynamicCast(URefineEqu::URefineBtn,GetChildByID(4));
	pkBtn[5] = UDynamicCast(URefineEqu::URefineBtn,GetChildByID(5));

	pkBtn[6] = UDynamicCast(URefineEqu::URefineBtn,GetChildByID(20));
	pkBtn[7] = UDynamicCast(URefineEqu::URefineBtn,GetChildByID(21));
	pkBtn[8] = UDynamicCast(URefineEqu::URefineBtn,GetChildByID(22));

	dataInfo.pos = 0 ;
	pkBtn[0]->SetItemData(dataInfo);
	dataInfo.pos = 1 ;
	pkBtn[1]->SetItemData(dataInfo);
	dataInfo.pos = 2 ;
	pkBtn[2]->SetItemData(dataInfo);
	dataInfo.pos = 3 ;
	pkBtn[3]->SetItemData(dataInfo);
	dataInfo.pos = 4 ;
	pkBtn[4]->SetItemData(dataInfo);
	dataInfo.pos = 5 ;
	pkBtn[5]->SetItemData(dataInfo);

	dataInfo.pos = 6 ;
	pkBtn[6]->SetItemData(dataInfo);
	dataInfo.pos = 7 ;
	pkBtn[7]->SetItemData(dataInfo);
	dataInfo.pos = 8 ;
	pkBtn[8]->SetItemData(dataInfo);


	pkBtn[0]->SetVisible(TRUE);
	pkBtn[1]->SetVisible(TRUE);
	pkBtn[2]->SetVisible(TRUE);
	pkBtn[3]->SetVisible(TRUE);
	pkBtn[4]->SetVisible(TRUE);
	pkBtn[5]->SetVisible(TRUE);
	pkBtn[6]->SetVisible(TRUE);
	pkBtn[7]->SetVisible(TRUE);
	pkBtn[8]->SetVisible(TRUE);

	pkBtn[0]->SetActive(TRUE);
	pkBtn[1]->SetActive(TRUE);
	pkBtn[2]->SetActive(TRUE);
	pkBtn[3]->SetActive(TRUE);
	pkBtn[4]->SetActive(TRUE);
	pkBtn[5]->SetActive(TRUE);
	pkBtn[6]->SetActive(TRUE);
	pkBtn[7]->SetActive(TRUE);
	pkBtn[8]->SetActive(TRUE);

	UControl* pScallBar = GetChildByID(14);
	if (pScallBar)
	{
		pScallBar->SetVisible(m_RefineType == t_Refine || m_RefineType == t_Embed);
	}

	UPoint pkBtnPos[6];
	switch(m_RefineType)
	{
	case t_Refine:
		pkBtn[0]->SetPosition(UPoint(135, 64));
		pkBtn[1]->SetPosition(UPoint(46, 337));
		pkBtn[2]->SetPosition(UPoint(110, 350));
		pkBtn[3]->SetPosition(UPoint(151, 350));
		pkBtn[4]->SetPosition(UPoint(192, 350));
		pkBtn[5]->SetPosition(UPoint(233, 350));

		pkBtn[6]->SetVisible(FALSE);
		pkBtn[7]->SetVisible(FALSE);
		pkBtn[8]->SetVisible(FALSE);

		pkBtn[6]->SetActive(FALSE);
		pkBtn[7]->SetActive(FALSE);
		pkBtn[8]->SetActive(FALSE);
		break;
	//case t_Compound:
	//	pkBtn[0]->SetPosition(UPoint(95,  197));
	//	pkBtn[1]->SetPosition(UPoint(135, 197));
	//	pkBtn[2]->SetPosition(UPoint(175, 197));

	//	pkBtn[3]->SetVisible(FALSE);
	//	pkBtn[4]->SetVisible(FALSE);
	//	pkBtn[5]->SetVisible(FALSE);

	//	pkBtn[3]->SetActive(FALSE);
	//	pkBtn[4]->SetActive(FALSE);
	//	pkBtn[5]->SetActive(FALSE);
	//	break;
	case t_Embed:
		pkBtn[0]->SetPosition(UPoint(135, 64));
		pkBtn[6]->SetActive(FALSE);
		pkBtn[7]->SetActive(FALSE);
		pkBtn[8]->SetActive(FALSE);

		pkBtn[6]->SetBkGIndex(0);
		pkBtn[7]->SetBkGIndex(0);
		pkBtn[8]->SetBkGIndex(0);

		pkBtn[6]->SetGlint(FALSE);
		pkBtn[7]->SetGlint(FALSE);
		pkBtn[8]->SetGlint(FALSE);


		pkBtn[1]->SetVisible(FALSE);
		pkBtn[2]->SetVisible(FALSE);
		pkBtn[3]->SetVisible(FALSE);
		pkBtn[4]->SetVisible(FALSE);
		pkBtn[5]->SetVisible(FALSE);

		pkBtn[1]->SetActive(FALSE);
		pkBtn[2]->SetActive(FALSE);
		pkBtn[3]->SetActive(FALSE);
		pkBtn[4]->SetActive(FALSE);
		pkBtn[5]->SetActive(FALSE);
		break;
	}

	EquRefineMgr->ChangeRefineType(m_RefineType);
}
void URefineEqu::OnRefine()	//精炼
{
	if (sm_RefineState == _Idle_State_ && m_RefineType != t_Refine)
	{
		m_RefineType = t_Refine;
		ChangeRefineType();
	}
}
void URefineEqu::OnCompound()  //合成
{
	//if (sm_RefineState == _Idle_State_ && m_RefineType != t_Compound)
	//{
	//	m_RefineType = t_Compound;
	//	ChangeRefineType();
	//}
}
void URefineEqu::OnEmbed() //演化
{
	if (sm_RefineState == _Idle_State_ && m_RefineType != t_Embed)
	{
		m_RefineType = t_Embed;
		ChangeRefineType();
	}
}

void URefineEqu::UpDataMaterials(ui8 Slot, ui32 itemid,ui64 guid,UINT Count,UINT stType)
{
	if (Slot >= 0 && Slot < 9 &&  (RefineType)stType == m_RefineType)
	{
		int childId = Slot;
		if (Slot > 5)
		{
			childId = 20 + Slot - 6;
		}
		URefineBtn* pkControl = UDynamicCast(URefineBtn,GetChildByID(childId));
		if (pkControl)
		{
			ActionDataInfo data;

			if (guid == 0)
			{
				Count = 0;
			}
			data.pos = Slot;
			data.Guid = guid;
			data.entry = itemid;
			data.type = UItemSystem::ITEM_DATA_FLAG ;
			data.num = Count;

			pkControl->SetItemData(data);
			pkControl->SetGlint(FALSE);

			if (Slot == 0)
			{
				UpdataSlotInfo(guid);
			}

			if (stType == t_Embed && Slot != 0)
			{
				ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(itemid);
				if (ItemInfo)
				{
					int colorindex = 0;
					if(ItemInfo->GemColorMask & 1){
						colorindex = 4;
					}else if(ItemInfo->GemColorMask & 2){
						colorindex = 1;
					}else if(ItemInfo->GemColorMask & 4){
						colorindex = 2;
					}else if(ItemInfo->GemColorMask & 8){
						colorindex = 3;
					}
					if(pkControl->GetBkgIndex() == colorindex)
					{
						pkControl->SetGlint(TRUE);
					}
				}
			}
		}
	}
	
}
void URefineEqu::UpdataSlotInfo(ui64 guid)
{
	UINT money = 0;
	SetInitInfoText(m_RefineType);

	switch(m_RefineType)
	{
	case t_Refine:
		{
			if (guid)
			{
				m_RefineText->ClearText();
				SYItem* pkItem = (SYItem*)ObjectMgr->GetObject(guid);
				ItemPrototype_Client* ItemInfo = NULL;
				if ( pkItem )
				{
					ItemInfo = pkItem->GetItemProperty();
				}
				
				if (pkItem && ItemInfo)
				{
					ui32 Qut =  ItemInfo->Quality  ;

					if (Qut == ITEM_QUALITY_NORMAL_WHITE)
					{
						money = 200 ;
					}else if (Qut == ITEM_QUALITY_UNCOMMON_GREEN)
					{
						money = 300 ;
					}else if (Qut == ITEM_QUALITY_RARE_BLUE)
					{
						money = 500;
					}else if (Qut == ITEM_QUALITY_EPIC_PURPLE)
					{
						money = 700;
					}else if (Qut == ITEM_QUALITY_LEGENDARY_ORANGE)
					{
						money = 1000;
					}				
				}
				else
					break;


				UColor Color = m_Style->m_FontColor ;
				string DefColorStr = "<color:" + GetColorString(Color) + ">";
				string GreenColorStr = "<color:" + GetColorString(UColor(0, 200, 0, 255)) + ">";
				//name
				if (ItemInfo->C_name.size())
				{
					string name = ItemInfo->C_name;
					long long mask = 0;

					string as_s;
					if (pkItem)
					{
						for ( unsigned int ui = 0; ui < 8; ui++ ) {
							uint32 propid = pkItem->GetByte(ITEM_FIELD_VARIABLE_PROPERTIES_01 + ui/4, ui&3);
							if (propid != 0)
							{
								int i = GetAS(propid);
								long long flag = (1 << propid);

								if (i != -1)
								{
									if (!bool(mask&flag))
									{
										if (ui != 0)
											as_s += ' ';
										as_s += _TRAN(as[i].str.c_str());
										mask |= flag;
									}
								}
								else
									as_s += _TRAN("未知属性");
							}
						}
					}

					if (!as_s.empty())
					{
						//name = as_s + "的" + name;
					}

					GetQualityColor(ItemInfo->Quality, Color);

					char NameBUF[1024];
					sprintf_s(NameBUF,1024,"<br><color:%s>%s<br>", GetColorString(Color).c_str(), name.c_str());

					m_RefineText->AppendText(NameBUF, strlen(NameBUF) ,TRUE);
				}

				//精炼等级
				ui32 RefineLv = pkItem->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
				std::string strbuf;
				if (RefineLv < 9)
				{
					strbuf = _TRAN( N_NOTICE_C42, _I2A( RefineLv ).c_str(), "1" );
				}else
				{
					strbuf = _TRAN( N_NOTICE_C43, _I2A( RefineLv ).c_str());
				}

				m_RefineText->AppendText(strbuf.c_str(), strbuf.length() ,TRUE);
				//绑定

				string Bonding ;
				const char* BondingType[]=
				{"未绑定<br>","拾取绑定<br>","装备绑定<br>","使用绑定<br>","任务绑定<br>",	"任务绑定<br>", "终身绑定<br>"};
				Bonding = _TRAN(BondingType[ItemInfo->Bonding]);

				if(pkItem && pkItem->HasFlag( ITEM_FIELD_FLAGS, ITEM_FLAG_QUEST | ITEM_FLAG_SOULBOUND ))
				{
					Bonding = DefColorStr + _TRAN("已绑定<br>");
				}

				m_RefineText->AppendText( Bonding.c_str(), Bonding.size(), TRUE);

				//防御

				if (ItemInfo->Class == ITEM_CLASS_ARMOR)
				{
					//char ArmorBuf[1024];
					UINT armorRate = ItemMgr->GetItemRefineRate(pkItem->GetCode() , 1);
					ui32 armor = ItemInfo->Armor;

					if (RefineLv < 9)
					{
						strbuf = _TRAN( N_NOTICE_C44, DefColorStr.c_str(), _I2A( armor + armorRate* RefineLv ).c_str(), _I2A( armorRate ).c_str() );
						//sprintf_s(ArmorBuf,1024, strbuf.c_str()/*_TRAN("%s护甲 +%d <color:A73CE8>+ ↑%d<br>"),DefColorStr.c_str(),armor + armorRate* RefineLv,armorRate*/);
					}else
					{
						strbuf = _TRAN( N_NOTICE_C45, DefColorStr.c_str(), _I2A( armor + armorRate* RefineLv ).c_str() );
						//sprintf_s(ArmorBuf,1024, strbuf.c_str()/*_TRAN("%s护甲 +%d<br>") ,DefColorStr.c_str(),armor + armorRate* RefineLv*/);
					}

					m_RefineText->AppendText(strbuf.c_str(), strbuf.length() ,TRUE);
				}

				//类型
				if (ItemInfo->Class == ITEM_CLASS_WEAPON && ItemInfo->SubClass >= 0 && ItemInfo->SubClass < ITEM_SUBCLASS_WEAPON_MAX)
				{
					const char * Weapon[] =
					{
						"单手斧","双手斧","单手剑","双手剑","弓","弩","单手戈","双手杖",
						"单手锤","双手锤","单手刀","双手刀","盾","枪","匕首", "宝珠"
					};
					std::string strtip = _TRAN(Weapon[ItemInfo->SubClass]);
					switch(ItemInfo->InventoryType)
					{
					case INVTYPE_WEAPONMAINHAND:
						strtip += _TRAN(",需要主手");
						break;
					case INVTYPE_WEAPONOFFHAND:
						strtip += _TRAN(",需要副手");
						break;
					case INVTYPE_2HWEAPON:
						strtip += _TRAN(",需要双手");
						break;
					}

					char WeaponBuf[_MAX_PATH];
					sprintf_s(WeaponBuf, _MAX_PATH, "%s%s<br>",DefColorStr.c_str(), strtip.c_str());

					m_RefineText->AppendText(WeaponBuf, strlen(WeaponBuf) ,TRUE);
				}

				if (ItemInfo->Class == ITEM_CLASS_ARMOR && ItemInfo->SubClass  >= 0 && ItemInfo->SubClass < ITEM_SUBCLASS_ARMOR_MAX)
				{
					const char * Armor[]=
					{
						"头盔","肩甲","胸甲","腰带","戒指","项链","饰品",
						"发型","手套","外衣","腿甲","鞋子",
						"怪物装背部","怪物装头部","怪物装肩膀","怪物装上衣","怪物装下衣","怪物装鞋子",
					};

					char ArmorTBuf[_MAX_PATH];
					sprintf_s(ArmorTBuf, _MAX_PATH, "%s%s<br>", _TRAN(DefColorStr.c_str()), _TRAN(Armor[ItemInfo->SubClass]));
					m_RefineText->AppendText( ArmorTBuf, strlen(ArmorTBuf) ,TRUE);
				}

				////宝石孔
				//ui32 HoleCnt = pkItem->GetUInt32Value(ITEM_FIELD_HOLE_CNT);
				////char HoleBuf[_MAX_PATH];
				std::string strbuff;
				//if (HoleCnt >= 3)
				//{
				//	strbuff = _TRAN( N_NOTICE_C46, GreenColorStr.c_str(), _I2A(HoleCnt).c_str() );
				//	//sprintf_s(HoleBuf, _MAX_PATH, strbuff.c_str()/* _TRAN("%s开孔 %d <br>未镶嵌 <br>未镶嵌 <br>未镶嵌 <br>"),GreenColorStr.c_str(), HoleCnt*/);
				//	m_RefineText->AppendText(strbuff.c_str(), strbuff.length() ,TRUE);

				//	//显示镶嵌的宝石属性
				//}else if (HoleCnt == 2)
				//{
				//	if (RefineLv == 8)
				//	{
				//		strbuff = _TRAN( N_NOTICE_C47, GreenColorStr.c_str(), _I2A(HoleCnt).c_str(), "1", GreenColorStr.c_str() );
				//		//sprintf_s(HoleBuf, _MAX_PATH, strbuff.c_str()/*_TRAN("%s开孔 %d <color:A73CE8>+ ↑%d<br>%s未镶嵌 <br>未镶嵌 <br>"),GreenColorStr.c_str(), HoleCnt,1, GreenColorStr.c_str()*/);
				//	}else
				//	{
				//		strbuff = _TRAN( N_NOTICE_C48, GreenColorStr.c_str(), _I2A(HoleCnt).c_str() );
				//		//sprintf_s(HoleBuf, _MAX_PATH, strbuff.c_str()/*_TRAN("%s开孔 %d <br>未镶嵌 <br>未镶嵌 <br>"),GreenColorStr.c_str(), HoleCnt*/);
				//	}

				//	m_RefineText->AppendText(strbuff.c_str(), strbuff.length() ,TRUE);
				//}else if (HoleCnt == 1)
				//{
				//	if (RefineLv == 6)
				//	{
				//		strbuff = _TRAN( N_NOTICE_C49, GreenColorStr.c_str(), _I2A(HoleCnt).c_str(), "1", GreenColorStr.c_str() );
				//		//sprintf_s(HoleBuf, _MAX_PATH, strbuff.c_str()/*_TRAN("%s开孔 %d <color:A73CE8>+ ↑%d<br>%s未镶嵌 <br>"),GreenColorStr.c_str(), HoleCnt,1,GreenColorStr.c_str()*/);
				//	}else
				//	{
				//		strbuff = _TRAN( N_NOTICE_C50, GreenColorStr.c_str(), _I2A(HoleCnt).c_str() );
				//		//sprintf_s(HoleBuf, _MAX_PATH,strbuff.c_str() /*_TRAN("%s开孔 %d <br>未镶嵌 <br>"),GreenColorStr.c_str(), HoleCnt*/);
				//	}

				//	m_RefineText->AppendText(strbuff.c_str(), strbuff.length() ,TRUE);
				//}else
				//{
				//	if (RefineLv == 3)
				//	{

				//		strbuff = _TRAN( N_NOTICE_C51, GreenColorStr.c_str(), _I2A(HoleCnt).c_str(), "1" );
				//		//sprintf_s(HoleBuf, _MAX_PATH, strbuff.c_str()/* _TRAN("%s开孔 %d <color:A73CE8>+ ↑%d<br>"),GreenColorStr.c_str(), HoleCnt,1*/);
				//	}else
				//	{
				//		strbuff = _TRAN( N_NOTICE_C52, GreenColorStr.c_str(), _I2A(HoleCnt).c_str() );
				//		//sprintf_s(HoleBuf, _MAX_PATH, strbuff.c_str()/*_TRAN("%s开孔 %d <br>"),GreenColorStr.c_str(), HoleCnt*/);
				//	}

				//	m_RefineText->AppendText(strbuff.c_str(), strbuff.length() ,TRUE);
				//}

				//伤害
				if (ItemInfo->Class == ITEM_CLASS_WEAPON)
				{
					const char* DamageType[]=
					{
						"普通伤害：","","","","","",
					};
					char Damagebuf[_MAX_PATH];

					UINT DamageRate = ItemMgr->GetItemRefineRate(pkItem->GetCode() , 0);

					for (int i = 0 ; i < 5 ; i++)
					{
						if (!ItemInfo->Damage[i].Max == 0 && ItemInfo->Damage[i].Type < 5)
						{
							float min = ItemInfo->Damage[i].Min + RefineLv * DamageRate;
							float max = ItemInfo->Damage[i].Max + RefineLv * DamageRate;

							if (RefineLv < 9)
							{
								sprintf_s(Damagebuf,_MAX_PATH,_TRAN("%s%s %5.0f - %5.0f <color:A73CE8>+ ↑%d<br>"),DefColorStr.c_str(),_TRAN(DamageType[ItemInfo->Damage[i].Type]),min,max, DamageRate);
							}else
							{
								sprintf_s(Damagebuf,_MAX_PATH,_TRAN("%s%s %5.0f - %5.0f <br>"),DefColorStr.c_str(),_TRAN(DamageType[ItemInfo->Damage[i].Type]),min,max);
							}

							m_RefineText->AppendText(Damagebuf, strlen(Damagebuf) ,TRUE);
						}
					}
				}



				UINT RefineADD = ItemMgr->GetItemRefineRate(pkItem->GetCode(), 2) ;
				char StatsBuf[256];
				for (int i = 0 ;i < 10 ; i++)
				{
					if (!ItemInfo->Stats[i].Value == 0)
					{
						if (ItemInfo->Stats[i].Type > STAT_NONE && ItemInfo->Stats[i].Type <= SPELL_DAMAGE)
						{

							if (ItemInfo->Stats[i].Type > VITALITY)
							{
								if (ItemInfo->Stats[i].Type  > SHIELD_BLOCK_RATING && ItemInfo->Stats[i].Type <= SPELL_DAMAGE)
								{
									sprintf_s(StatsBuf,256,"%s%s  +%d <br>",DefColorStr.c_str(),_TRAN(ItemStat[ItemInfo->Stats[i].Type]),ItemInfo->Stats[i].Value);
								}else
								{
									sprintf_s(StatsBuf,256,"%s%s  +%d%% <br>",DefColorStr.c_str(),_TRAN(ItemStat[ItemInfo->Stats[i].Type]),ItemInfo->Stats[i].Value);
								}

							}else
							{
								if (RefineLv < 9)
								{
									sprintf_s(StatsBuf,256,"%s%s  +%d <color:A73CE8>+ %s%d<br>",DefColorStr.c_str(),_TRAN(ItemStat[ItemInfo->Stats[i].Type]),(ItemInfo->Stats[i].Value + RefineADD * RefineLv), _TRAN("↑"),RefineADD);
								}else
								{
									sprintf_s(StatsBuf,256,"%s%s  +%d <br>",DefColorStr.c_str(),_TRAN(ItemStat[ItemInfo->Stats[i].Type]),(ItemInfo->Stats[i].Value + RefineADD * RefineLv));
								}
							}
							m_RefineText->AppendText(StatsBuf, strlen(StatsBuf) ,TRUE);	
						}
					}
				}

				if (pkItem)
				{
					for ( unsigned int ui = 0; ui < 8; ui++ )
					{
						uint32 propid = pkItem->GetByte(ITEM_FIELD_VARIABLE_PROPERTIES_01 + ui/4, ui&3);
						if (propid != 0)
						{
							ItemEnchantProto enchant_infos;
							g_pkItemEnchantDB->GetEnchantProperty(propid, enchant_infos);
							sprintf_s(StatsBuf,256,"%s%s<br>",GreenColorStr.c_str(),enchant_infos.sName.c_str());

							m_RefineText->AppendText(StatsBuf, strlen(StatsBuf) ,TRUE);	
						}
					}
				}

				//需要等级

				//char EquLvbuf[_MAX_PATH];
				strbuff = _TRAN( N_NOTICE_C55, DefColorStr.c_str(), _I2A( ItemInfo->RequiredLevel ).c_str() );
				//sprintf_s(EquLvbuf,_MAX_PATH, strbuff.c_str()/*"%s需求等级  %d级<br>",DefColorStr.c_str(),ItemInfo->RequiredLevel*/);

				m_RefineText->AppendText(strbuff.c_str(), strbuff.length() ,TRUE);	

				//职业
				std::string classStr = _TRAN("适用职业：");
				int ClassMask = 1 + 2 + 4 + 8;
				if(ItemInfo->AllowableClass & CLASS_WARRIOR)
				{
					classStr += _TRAN("武修 ");
					ClassMask -= 1;
				}
				if(ItemInfo->AllowableClass & 2)
				{
					classStr += _TRAN("羽箭 ");
					ClassMask -= 2;
				}
				if(ItemInfo->AllowableClass & 4)
				{
					classStr += _TRAN("仙道 ");
					ClassMask -= 4;
				}
				if(ItemInfo->AllowableClass & 8)
				{
					classStr += _TRAN("真巫 ");
					ClassMask -= 8;
				}
				if (!ClassMask)
				{
					classStr = _TRAN("适用职业：所有职业");
				}

				char Classbuf[_MAX_PATH];
				sprintf_s(Classbuf,_MAX_PATH,"%s%s<br>",DefColorStr.c_str(),classStr.c_str());
				m_RefineText->AppendText(Classbuf, strlen(Classbuf) ,TRUE);	

				//描述
				//if (ItemInfo->C_desc.size())
				//{
				//	if (strcmp(ItemInfo->C_desc.c_str(),"0") != 0)
				//	{
				//		char DescBuf[1024];
				//		sprintf_s(DescBuf,_MAX_PATH,"%s%s<br>",DefColorStr.c_str(),ItemInfo->C_desc.c_str());
				//		m_RefineText->AppendText(DescBuf, strlen(DescBuf) ,TRUE);	
				//	}
				//}
			}
		}
		break;
	case t_Embed:
		{
			if (guid)
			{
				SYItem * Item = (SYItem *)ObjectMgr->GetObject(guid);
				if (Item)
				{
					uint32 entry = Item->GetCode();
					ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(entry);
					if (!ItemInfo)
						return;
					for (int i = 0 ; i < 3 ; i++)
					{
						URefineBtn* pkControl = UDynamicCast(URefineBtn,GetChildByID(i + 20));
						if (ItemInfo && pkControl)
						{
							pkControl->SetActive( ItemInfo->Sockets[i].SocketColor > 0 );
							int colorindex = 0;
							if(ItemInfo->Sockets[i].SocketColor & 1){
								colorindex = 4;
							}else if(ItemInfo->Sockets[i].SocketColor & 2){
								colorindex = 1;
							}else if(ItemInfo->Sockets[i].SocketColor & 4){
								colorindex = 2;
							}else if(ItemInfo->Sockets[i].SocketColor & 8){
								colorindex = 3;
							}
							if (pkControl)
							{
								pkControl->SetBkGIndex(colorindex);
							}
						}
						uint32 gem = Item->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + (2 + i) * 3 );
						if (gem && pkControl)
						{
							ItemEnchantProto enchant_infos;
							g_pkItemEnchantDB->GetEnchantProperty(gem, enchant_infos);
							ActionDataInfo data;
							data.pos = i + 6;
							data.Guid = 0;
							data.entry = enchant_infos.uiGemEntry;
							data.type = UItemSystem::ITEM_DATA_FLAG ;
							data.num = 1;
							pkControl->SetItemData(data);
							pkControl->SetGlint(FALSE);
							ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(enchant_infos.uiGemEntry);
							if (ItemInfo)
							{
								int colorindex = 0;
								if(ItemInfo->GemColorMask & 1){
									colorindex = 4;
								}else if(ItemInfo->GemColorMask & 2){
									colorindex = 1;
								}else if(ItemInfo->GemColorMask & 4){
									colorindex = 2;
								}else if(ItemInfo->GemColorMask & 8){
									colorindex = 3;
								}
								if(pkControl->GetBkgIndex() == colorindex)
								{
									pkControl->SetGlint(TRUE);
								}
							}
						}else if (pkControl)
						{
							ActionDataInfo data;
							data.pos = i + 6;
							data.Guid = 0;
							data.entry = 0;
							data.type = UItemSystem::ITEM_DATA_FLAG ;
							data.num = 1;
							pkControl->SetItemData(data);
							pkControl->SetGlint(FALSE);
						}
					}
				}
			}else
			{
				for (int i = 0 ; i < 3 ; i++)
				{
					URefineBtn* pkControl = UDynamicCast(URefineBtn,GetChildByID(i + 20));
					if (pkControl)
					{
						ActionDataInfo data;
						data.pos = i + 6;
						data.Guid = 0;
						data.entry = 0;
						data.type = UItemSystem::ITEM_DATA_FLAG ;
						data.num = 0;
						pkControl->SetItemData(data);
						pkControl->SetBkGIndex(0);
					}
				}
			}
		}
		break;
	}
	UpDataRefineMoney(money);	
}
void URefineEqu::UpDataRefineMoney(UINT money)
{
	UMoneyText* pkMoney = UDynamicCast(UMoneyText, GetChildByID(7));
	if (pkMoney)
	{
		pkMoney->SetMoney(money);
	}

}


//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(URefineEqu::URefineBtn, UDynamicIconButtonEx);

void URefineEqu::URefineBtn::StaticInit()
{

}
URefineEqu::URefineBtn::URefineBtn()
{
	m_IsTickable = FALSE;
	m_TypeID = UItemSystem::ICT_REFINE;
	m_ActionContener.SetActionItem(new URefineEqu::URefineBtnItem);
	BkgIndex = BACKSKIN_ACTIVE;
}
URefineEqu::URefineBtn::~URefineBtn()
{

}

BOOL URefineEqu::URefineBtn::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (UDynamicIconButtonEx::OnMouseUpDragDrop(pDragFrom,point,pDragData,nDataFlag))
	{
		if (nDataFlag == UItemSystem::ICT_BAG)
		{
			UPackage * pPackage = INGAMEGETFRAME(UPackage,FRAME_IG_PACKAGE);
			if (pPackage)
			{
				const ui8 * Frompos = (const ui8*)pDragData;
				const ui32 ItemId = pPackage->GetItemIdByPos(*Frompos);
				ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(ItemId);
				if (ItemInfo)
				{
					//发送添加精炼材料消息
					EquRefineMgr->SendAddMaterials(*Frompos,m_ActionContener.GetDataInfo()->pos,FALSE);
				}
				return TRUE;
			}
		}
		return FALSE;
	}
	return FALSE;
}
void URefineEqu::URefineBtn::DrawPushButton(const UPoint& offset, const URect& ClipRect)
{
	URect rect(offset,m_Size);

	UColor color = LCOLOR_WHITE ;
	
	if (!m_Active)
	{
		color = UColor(128,128,128) ;
	}
	if (m_BackGSkin)
	{
		sm_UiRender->DrawSkin(m_BackGSkin, BkgIndex, rect );
	}
	if (m_IconSkin)
	{
		URect skinRec ; 
		skinRec.SetPosition(offset.x + 2, offset.y + 2);
		skinRec.SetSize(m_Size.x - 4, m_Size.y - 4);
		sm_UiRender->DrawSkin(m_IconSkin,m_IconIndex,skinRec,color);
	}
	if (m_ActionContener.IsCreate())
	{
		m_ActionContener.DrawActionContener(sm_UiRender,offset,ClipRect);

		//数量
		ActionDataInfo * pData = GetItemData();
		if (pData && pData->Guid && pData->num >1)
		{
			int textH = m_Style->m_spFont->GetHeight();
			UPoint txtPos = UPoint(offset.x, offset.y + m_Size.y - textH);
			UPoint txtSize = UPoint(m_Size.x, textH);

			char buf[10];
			NiSprintf(buf,10,"%d",pData->num);
			UDrawText(sm_UiRender,m_Style->m_spFont,txtPos,txtSize,color,buf,UT_RIGHT);
		}
	}
}
void URefineEqu::URefineBtn::OnRightMouseUp(const UPoint& position, UINT flags)
{
	if (m_ActionContener.IsCreate())
	{
		ActionDataInfo * pData = GetItemData();
		if (pData->entry != 0 && pData->type == ActionButton_Type_Spell)
		{
			UINT pSkillTest = SYState()->SkillMgr->TestSkillState(pData->entry);
			if (pSkillTest == AttributePassive)
				return;
		}
		ReleaseCapture();
		m_ActionContener.Release(MOUSEUP_RIGHT);
	}
}
void URefineEqu::URefineBtn::OnMouseUp(const UPoint& pt, UINT uFlags)
{
	if (m_ActionContener.IsCreate() && m_Active)
	{
		ActionDataInfo * pData = GetItemData();
		if (pData->entry != 0 && pData->type == ActionButton_Type_Spell)
		{
			UINT pSkillTest = SYState()->SkillMgr->TestSkillState(pData->entry);
			if (pSkillTest == AttributePassive)
				return;
		}
		ReleaseCapture();
		bool bUse = true;
		bUse &= !SpecMethod(uFlags);
		m_ActionContener.Release(MOUSEUP_LEFT, bUse);
		m_BeginDrag = FALSE;
	}
}

//////////////////////////////////////////////////////////////////////////
void URefineEqu::URefineBtnItem::Use()
{

}
void URefineEqu::URefineBtnItem::UseLeft()
{

}
//////////////////////////////////////////////////////////////////////////
///  					  refine  bar								//////
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(URefineBar, UProgressBar);
void URefineBar::StaticInit()
{
	
}

URefineBar::URefineBar()
{
	m_TopTexture = NULL;
	m_SuccessTexture = NULL;
	m_FaildTexture = NULL;
	m_curShow = DoShowBar;
}
URefineBar::~URefineBar()
{

}

BOOL URefineBar::OnCreate()
{
	if (!UProgressBar::OnCreate())
	{
		return FALSE;
	}
	
	if (!m_TopTexture)
	{
		m_TopTexture = sm_UiRender->LoadTexture("data\\ui\\refine\\Top.png");
	}

	if (!m_SuccessTexture)
	{
		m_SuccessTexture = sm_UiRender->LoadTexture("data\\ui\\refine\\Success.png");
	}

	if (!m_FaildTexture)
	{
		m_FaildTexture = sm_UiRender->LoadTexture("data\\ui\\refine\\Faild.png");
	}


	if (!m_FaildTexture || !m_SuccessTexture || !m_TopTexture)
	{
		return FALSE;
	}

	m_curShow = DoShowBar;
	return TRUE;
}

void URefineBar::SetShow(UINT stType)
{
	m_curShow = stType ;
}
void URefineBar::SetProgressPos(ui32 agePos, ui32 lifePos)
{
	m_curShow = DoShowBar ;
	UProgressBar::SetProgressPos(agePos, lifePos);
}
void URefineBar::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_BkgTexture == NULL)
	{
		return UControl::OnRender(offset,updateRect);
	}

	URect DrawBarRect(updateRect);
	
	if (m_curShow == ShowSuccess)
	{
		DrawBarRect.SetHeight(m_SuccessTexture->GetHeight());
		DrawBarRect.SetWidth(m_SuccessTexture->GetWidth());
		DrawBarRect.SetPosition(updateRect.left - (m_SuccessTexture->GetWidth() - updateRect.GetWidth()) / 2,
			updateRect.top - (m_SuccessTexture->GetHeight() - updateRect.GetHeight()) / 2);

		sm_UiRender->SetClipRect(DrawBarRect);
		sm_UiRender->DrawImage(m_SuccessTexture, DrawBarRect);
	}else if (m_curShow == ShowFaild)
	{
		sm_UiRender->DrawImage(m_FaildTexture, updateRect);
	}else if (m_curShow == DoShowBar)
	{
		
		URect DrawTopRect(0,0,0,0);
		if (m_lifePos == 0 )
		{
			DrawBarRect.SetWidth(0);
		}
		else
		{
			float fw = updateRect.GetWidth() * float(m_agePos)/float(m_lifePos);
			DrawBarRect.SetWidth(fw);

			URect ImageRect;
			ImageRect.pt0.x = 0; 
			ImageRect.pt0.y = 0;
			ImageRect.pt1.x = m_BkgTexture->GetWidth() * float(m_agePos)/float(m_lifePos); 
			ImageRect.pt1.y = m_BkgTexture->GetHeight();
			sm_UiRender->DrawImage(m_BkgTexture, DrawBarRect, ImageRect);
			

			if (fw > 0.0f)
			{
				DrawTopRect.SetSize(m_TopTexture->GetWidth(), m_TopTexture->GetHeight());
				DrawTopRect.SetPosition(DrawBarRect.right - m_TopTexture->GetWidth() / 2, 
					DrawBarRect.top - (m_TopTexture->GetHeight() - updateRect.GetHeight()) / 2 );

				sm_UiRender->SetClipRect(URect(DrawBarRect.left,DrawTopRect.top, DrawTopRect.right, DrawTopRect.bottom));

				sm_UiRender->DrawImage(m_TopTexture, DrawTopRect);
			}
		}
	}
}


