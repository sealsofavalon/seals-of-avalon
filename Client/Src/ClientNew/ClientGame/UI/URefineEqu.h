#pragma once
#include "UInc.h"
#include "UDynamicIconButtonEx.h"
#include "UIPlayerHead.h"
#include "UNiAVControl.h"


// 玉能神器 操作模式
enum RefineType
{
	t_Refine =  0,  //精炼装备
	//t_Compound,		//合成宝石
	t_Embed,	//镶嵌宝石
};

//////////////////////////////////////////////////////////////////////////
//状态控制客户端的操作, 只有在空闲状态下才能进行 拖放材料, 切换操作模式
//等等操作.
//读条状态会在服务器返回成功或者失败消息前调用, 状态结束在返回的成功或失败
//消息. 在收到成功或失败消息后, 状态会自动去切换到空闲状态,来允许玩家进行
//客户端材料拖放, 切换操作模式等操作.
//////////////////////////////////////////////////////////////////////////

enum RefineState
{
	_Wait_State_ = 0,  //点击开始按钮后到收到消息前为等待状态,
	_DoBar_State_,     //收到读条消息
	_DoEffect_State_,  //播放成功或失败特效时间
	_Idle_State_,      //空闲状态 
};


class URefineBar : public UProgressBar 
{
	UDEC_CLASS(URefineBar);
public:
	URefineBar();
	~URefineBar();

	enum
	{
		DoShowBar = 0,
		ShowSuccess, 
		ShowFaild ,
	};

	void SetShow(UINT stType);
	virtual void SetProgressPos(ui32 agePos, ui32 lifePos);
protected:
	virtual BOOL OnCreate();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
private:
	UTexturePtr m_TopTexture;
	UTexturePtr m_SuccessTexture;
	UTexturePtr m_FaildTexture;

	UINT m_curShow ;
};



class URefineEqu : public UDialog
{
	class URefineBtn : public UDynamicIconButtonEx
	{
		UDEC_CLASS(URefineEqu::URefineBtn);
	public:
		URefineBtn();
		~URefineBtn();
		void SetBkGIndex(int index){ BkgIndex = index;}
		int GetBkgIndex(){return BkgIndex;}
	protected:
		virtual void OnRightMouseUp(const UPoint& position, UINT flags);
		virtual void OnMouseUp(const UPoint& pt, UINT uFlags);

		virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);  //接受拖动数据
		virtual void DrawPushButton(const UPoint& offset, const URect& ClipRect); //渲染
	private:
		int BkgIndex;
	};

	friend class URefineBtn;
	class URefineBtnItem : public UActionItem
	{
	public:
		virtual void Use();
		virtual void UseLeft();
	};

	UDEC_CLASS(URefineEqu);
	UDEC_MESSAGEMAP();
public:
	URefineEqu();
	~URefineEqu();

public:
	//UpData
	
	void UpDataMaterials(ui8 Slot,ui32 itemid ,ui64 guid,UINT Count, UINT stType);  //材料
	
	void UpdataSlotInfo(ui64 guid);
	void UpDataRefineMoney(UINT money);//金钱
	

	void ChangeRefineType();

	RefineType GetCurRefineType(){return m_RefineType ;}
	
	BOOL InitRefineBtn();//初始化数据 -- UI

	void SetInitInfoText(RefineType type);
	
	void ParseResult(ui8 bSuccess); //返回处理结果 这里主要是状态切换

protected:
	void LockRefineType(BOOL bLock); //是否锁定状态数据.在非空闲状态下.所有的数据都要锁定.只有在IDLE 状态下才能进行数据交互
	
	void SetActiveRefineBtn(UINT id ,BOOL bActive);//拖放数据的SLOT的锁定控制

	void SetCurState(RefineState state);//切换状态
	
	void DoEffectAndMsg(BOOL bSuccess); //处理成功 或者 失败 的数据.

	
	void DoBar();		//播放进度条
	
	void DoEffectTime(); // 成功或失败特效控制


	//客户的简易的判断是否需要发送消息
	BOOL CheckCanStart();
	

protected:
	void OnStart();     //发送消息

	//状态的切换管理
	void OnRefine();	//精炼装备
	void OnCompound();  //合成宝石
	void OnEmbed(); //镶嵌宝石


	
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual void OnTimer(float fDeltaTime);
	virtual BOOL OnEscape();
protected:
	RefineType m_RefineType;
	float m_BarTime;
	RefineState sm_RefineState ;
	UNiAVControlEffRe* m_EffectControl ;
	float m_EffectTime ;

	URichTextCtrl* m_RefineText; 
};