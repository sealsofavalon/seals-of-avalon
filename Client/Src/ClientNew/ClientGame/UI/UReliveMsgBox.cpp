#include "StdAfx.h"
#include "UReliveMsgBox.h"
#include "../Network/PacketBuilder.h"
#include "Network/NetworkManager.h"
#include "UFun.h"
#include "ObjectManager.h"
#include "UIGamePlay.h"
#include "UInstanceMgr.h"

#define ReliveWaitTime (300.f)
#define ReliveInInstance (5.f)
UIMP_CLASS(UDeathMsgBox,UControl)
UBEGIN_MESSAGE_MAP(UDeathMsgBox,UControl)
UON_BN_CLICKED(0, &UDeathMsgBox::OnClickRelive)
UON_BN_CLICKED(1, &UDeathMsgBox::OnClickReliveAtCity)
UEND_MESSAGE_MAP()

void UDeathMsgBox::StaticInit()
{

}
UDeathMsgBox::UDeathMsgBox()
{
	mTimeShow = NULL;
	mBkgTexture = NULL;
	mWaitReliveTime = 0.0f;
	m_IsUpdateChild = FALSE;
}
UDeathMsgBox::~UDeathMsgBox()
{
	mTimeShow = NULL;
	mWaitReliveTime = 0.0f;
}
BOOL UDeathMsgBox::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (mTimeShow == NULL)
	{
		mTimeShow = UDynamicCast(UStaticText, GetChildByID(4));
		if (mTimeShow)
		{
			mTimeShow->SetTextAlign(UT_CENTER);
		}
		else
		{
			return FALSE;
		}
	}
	UStaticText * USt1 = UDynamicCast(UStaticText, GetChildByID(2));
	if (USt1)
	{
		USt1->SetText( _TRAN("您已经无法战斗") );
		USt1->SetTextAlign(UT_CENTER);
		USt1->SetTextColor(GetColor(C_COLOR_WARNING));
	}else
	{
		return FALSE;
	}
	UStaticText * USt2 = UDynamicCast(UStaticText, GetChildByID(3));
	if (USt2)
	{
		USt2->SetText( _TRAN("请选择复活方式") );
		USt2->SetTextAlign(UT_CENTER);
		//USt1->SetTextColor(LCOLOR_WHITE);
	}else
	{
		return FALSE;
	}
	if (mBkgTexture == NULL)
	{
		mBkgTexture = sm_UiRender->LoadTexture("Data\\UI\\Relive\\DeathMsgBox.png");
		if (!mBkgTexture)
		{
			return FALSE;
		}
	}
	return TRUE;
}
void UDeathMsgBox::OnDestroy()
{
	SetVisible(FALSE);
	return UControl::OnDestroy();
}
void UDeathMsgBox::OnTimer(float fDetleTime)
{
	if (!m_Visible)
	{
		return;
	}
	if (mWaitReliveTime > 0)
	{
		mWaitReliveTime -= fDetleTime;
		INT Time = INT( mWaitReliveTime / 60.0f );
		std::string strbuff;
		if (Time)
		{
			strbuff = _TRAN( N_NOTICE_C56, _I2A( Time + 1 ).c_str() );
			//sprintf(buf, "%d分钟", Time + 1);
			mTimeShow->SetText(strbuff.c_str());
		}
		else
		{
			INT _second = INT(mWaitReliveTime);
			strbuff = _TRAN( N_NOTICE_C57, _I2A( _second ).c_str() );
			//sprintf(buf, "%d秒", _second);
			mTimeShow->SetText(strbuff.c_str());
		}
	}
}
void UDeathMsgBox::SetLocalDeath()
{
	if (InstanceSys->IsPlayerInInstance())
	{
		//mWaitReliveTime = ReliveInInstance;
		if(InstanceSys->GetCurrentCategory() == INSTANCE_CATEGORY_TEAM_ARENA)
			return;
		ui32 waittime = (ui32)ReliveInInstance;
		InstanceSys->GetInstanceReliveTime(waittime);
		mWaitReliveTime = 1.0f * waittime;
		SetVisible(TRUE);

		CPlayer* pLcalPlayer = ObjectMgr->GetLocalPlayer();
		if (pLcalPlayer)
		{
			CStorage* pBagContainer = pLcalPlayer->GetItemContainer();
			if (pBagContainer)
			{
				UINT Count = pBagContainer->GetItemCnt(10);
				if (Count == 0 || InstanceSys->GetCurrentCategory() == INSTANCE_CATEGORY_RAID)
				{
					GetChildByID(0)->SetActive(FALSE);
				}
				else
				{
					GetChildByID(0)->SetActive(TRUE);
				}
			}
		}
		GetChildByID(1)->SetActive(FALSE);
	}
	else
	{
		mWaitReliveTime = ReliveWaitTime;
		SetVisible(TRUE);

		CPlayer* pLcalPlayer = ObjectMgr->GetLocalPlayer();
		if (pLcalPlayer)
		{
			CStorage* pBagContainer = pLcalPlayer->GetItemContainer();
			if (pBagContainer)
			{
				UINT Count = pBagContainer->GetItemCnt(10);
				if (Count == 0)
				{
					GetChildByID(0)->SetActive(FALSE);
				}
				else
				{
					GetChildByID(0)->SetActive(TRUE);
				}
			}
		}
		GetChildByID(1)->SetActive(TRUE);
	}
}
void UDeathMsgBox::SetLocalRebrith()
{
	SetVisible(FALSE);
	mWaitReliveTime = 0.0f;
}
void UDeathMsgBox::OnClickRelive()
{
	//SetVisible(FALSE);
	//mWaitReliveTime = 0.0f;

	MSG_C2S::stReliveReq Msg;
	Msg.type = MSG_C2S::stReliveReq::Relive_Item;
	NetworkMgr->SendPacket(Msg);
	//PacketBuilder->SendResurrectResponse(0);
}
void UDeathMsgBox::OnClickReliveAtCity()
{
	//SetVisible(FALSE);
	MSG_C2S::stReliveReq Msg;
	Msg.type = MSG_C2S::stReliveReq::Relive_Home;
	NetworkMgr->SendPacket(Msg);
	//PacketBuilder->SendResurrectResponse(0);
}
void UDeathMsgBox::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (mBkgTexture)
	{
		sm_UiRender->DrawImage(mBkgTexture, updateRect);
	}
	RenderChildWindow(offset, updateRect);
}
/////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UReliveMsgBox,UControl)
UBEGIN_MESSAGE_MAP(UReliveMsgBox,UControl)
UON_BN_CLICKED(0, &UReliveMsgBox::OnClickAccept)
UON_BN_CLICKED(1, &UReliveMsgBox::OnClickRefuse)
UEND_MESSAGE_MAP()

void UReliveMsgBox::StaticInit()
{

}
UReliveMsgBox::UReliveMsgBox()
{
	mBkgTexture = NULL;
	m_IsUpdateChild = FALSE;
}
UReliveMsgBox::~UReliveMsgBox()
{
	mBkgTexture = NULL;
}
BOOL UReliveMsgBox::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	UStaticText * USt2 = UDynamicCast(UStaticText, GetChildByID(3));
	if (USt2)
	{
		USt2->SetText( _TRAN("想要复活你 是否接受") );
		USt2->SetTextAlign(UT_CENTER);
	}else
	{
		return FALSE;
	}
	if (mBkgTexture == NULL)
	{
		mBkgTexture = sm_UiRender->LoadTexture("Data\\UI\\Relive\\ReliveMsgBox.png");
		if (!mBkgTexture)
		{
			return FALSE;
		}
	}
	return TRUE;
}
void UReliveMsgBox::OnDestroy()
{
	SetVisible(FALSE);
	return UControl::OnDestroy();
}
void UReliveMsgBox::ResurrectRespond(const char* Name)
{
	if (Name)
	{
		ULog("%s要复活你",Name);
		UStaticText * USt2 = UDynamicCast(UStaticText, GetChildByID(2));
		if (USt2)
		{
			USt2->SetText(Name);
			USt2->SetTextAlign(UT_CENTER);
		}else
		{
			return;
		}
		SetVisible(TRUE);
		UDeathMsgBox* udmb = INGAMEGETFRAME(UDeathMsgBox, FRAME_IG_DEATHMSGBOX);
		if (udmb)
		{
			udmb->SetVisible(FALSE);
		}
	}
}
void UReliveMsgBox::OnClickAccept()
{
	SetVisible(FALSE);
	PacketBuilder->SendResurrectByOther(TRUE);
}
void UReliveMsgBox::OnClickRefuse()
{
	SetVisible(FALSE);
	PacketBuilder->SendResurrectByOther(FALSE);
	UDeathMsgBox* udmb = INGAMEGETFRAME(UDeathMsgBox, FRAME_IG_DEATHMSGBOX);
	if (udmb)
	{
		udmb->SetVisible(TRUE);
	}
}
void UReliveMsgBox::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (mBkgTexture)
	{
		sm_UiRender->DrawImage(mBkgTexture, updateRect);
	}
	RenderChildWindow(offset, updateRect);
}
#undef ReliveWaitTime