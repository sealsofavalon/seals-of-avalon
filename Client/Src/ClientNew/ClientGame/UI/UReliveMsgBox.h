#ifndef __URELIVEMSGBOX_H__
#define __URELIVEMSGBOX_H__
#include "UInc.h"

class UDeathMsgBox : public UControl
{
	UDEC_CLASS(UDeathMsgBox);
	UDEC_MESSAGEMAP();
public:
	UDeathMsgBox();
	~UDeathMsgBox();
public:
	void SetLocalDeath();
	void SetLocalRebrith();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDetleTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
private:
	void OnClickRelive();
	void OnClickReliveAtCity();
public:
	float mWaitReliveTime;
	UStaticText* mTimeShow;

	UTexturePtr mBkgTexture;
};

class UReliveMsgBox : public UControl
{
	UDEC_CLASS(UReliveMsgBox);
	UDEC_MESSAGEMAP();
public:
	UReliveMsgBox();
	~UReliveMsgBox();
public:
	void ResurrectRespond(const char* Name);
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
protected:
	void OnClickAccept();
	void OnClickRefuse();
	UTexturePtr mBkgTexture;
};
#endif