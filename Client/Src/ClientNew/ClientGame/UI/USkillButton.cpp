#include "stdafx.h"
#include <windows.h>
#include "UButton.h"
#include "UISystem.h"
#include "USkillButton.h"
#include "ItemManager.h"
#include "UIGamePlay.h"
#include "Utils/SpellDB.h"
#include "ClientApp.h"
#include "Skill/SkillManager.h"
#include "UITipSystem.h"
#include "UChat.h"

UIMP_CLASS(USkillButton, UDynamicIconButton);

void USkillButton::StaticInit()
{
	UREG_PROPERTY("showbgk",UPT_BOOL,UFIELD_OFFSET(USkillButton,m_bShowBgk));
	UREG_PROPERTY("SkinBgk", UPT_STRING, UFIELD_OFFSET(USkillButton, m_SkillBtnSkinStr));
	UREG_PROPERTY("LockDrag",UPT_BOOL,UFIELD_OFFSET(USkillButton,m_bLockDrag));
}
USkillButton::USkillButton()
{
	m_Size.Set(38,38);
	m_bEnableFreeze = FALSE;
	m_bShowBgk = TRUE;

	m_BtnDataInfo = new DataInfo;

	m_ItemTypeID = UItemSystem::ICT_INVLALID;
	////
	m_bBeginDrag = FALSE;
	if (m_bLockDrag)
		m_bLockDrag = TRUE;
	m_bMouseDownPt.Set(0,0);

	m_SkillBtnSkin = NULL;
}
USkillButton::~USkillButton()
{
	if (m_BtnDataInfo)
	{
		delete m_BtnDataInfo;
		m_BtnDataInfo= NULL;
	}
}
BOOL USkillButton::OnCreate()
{
	if (!UDynamicIconButton::OnCreate())
		return FALSE;

	
	if (!m_SkillBtnSkinStr.Empty())
	{
		m_SkillBtnSkin = sm_System->LoadSkin(m_SkillBtnSkinStr.GetBuffer());
	}
	return TRUE;
}

void USkillButton::OnTimer(float fDeltaTime)
{
	if (m_bTicking)
	{
		m_nMaskIndex = (WORD)(m_fTimer*60.f/m_FreezeTime);
	}

	//if (m_ItemTypeID != UItemSystem::ICT_INGAME_SKILL || m_ItemTypeID != UItemSystem::ICT_SKILL)
	//{
	//	m_bEnableFreeze = FALSE; //只要技能快捷界面需要冷却效果
	//}
}
void USkillButton::OnDestroy()
{
	m_SkillBtnSkin = NULL ;
	UDynamicIconButton::OnDestroy();
}
void USkillButton::OnRender(const UPoint& offset, const URect &updateRect)
{
	URect recp(offset, m_Size);
	URect rec = URect(recp.left + 3,recp.top + 3 ,recp.right - 3, recp.bottom - 3 );

	if (m_bShowBgk && (m_spBtnSkin == NULL || m_nIconIndex != -1) && m_Active && m_SkillBtnSkin)
	{
		sm_UiRender->DrawSkin(m_SkillBtnSkin,0,recp);
	}

	UDynamicIconButton::OnRender(offset,updateRect);
	
	if (m_BtnDataInfo->id > 1 && m_BtnDataInfo->type == UItemSystem::ITEM_DATA_FLAG )
	{
		char buf[255];
		bool bDraw = true;
		if (m_BtnDataInfo->CurNum != -1)
		{
			NiSprintf(buf, 255, "%d/%d", m_BtnDataInfo->CurNum, m_BtnDataInfo->num);
			if (strlen(buf) >= 5)
			{
				NiSprintf(buf, 255, "../%d", m_BtnDataInfo->num);
			}
			if (m_BtnDataInfo->CurNum < m_BtnDataInfo->num)
			{
				sm_UiRender->DrawRectFill(recp, UColor(0,0,0, 180)); //		
			}
		}else
		{
			NiSprintf(buf, 255, "%d",m_BtnDataInfo->num);
			bDraw = m_BtnDataInfo->num > 1;
		}
		if (bDraw)
		{
			int textH = m_Style->m_spFont->GetHeight();
			UPoint txtPos = UPoint(offset.x, offset.y + rec.GetHeight() - textH + 2);
			UPoint txtSize = UPoint(rec.GetWidth(), textH);
			USetTextEdge(TRUE);
			UDrawText(sm_UiRender,m_Style->m_spFont,txtPos,txtSize,m_Style->m_FontColor,buf,UT_RIGHT);
			USetTextEdge(FALSE);
		}
	}
	//if (!m_Active)
	//{
	//	sm_UiRender->DrawRect(recp,UColor(133,25,58,80));
	//}else
	//{
	//	sm_UiRender->DrawRect(recp,UColor(216,219,132,80));
	//}
}


void USkillButton::OnMouseMove(const UPoint& position, UINT flags)
{
	UDynamicIconButton::OnMouseMove(position,flags);
}

#include "UFittingRoom.h"
void USkillButton::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	if (uFlags == LKM_SHIFT)
	{
		if (m_BtnDataInfo->type == ActionButton_Type_Spell)
		{
			ChatSystem->AddSpellMsg(m_BtnDataInfo->id);
			return;
		}
		if (m_BtnDataInfo->type == ActionButton_Type_Item)
		{
			if (m_BtnDataInfo->pDataInfo)
			{
				ChatSystem->AddItemMsg(m_BtnDataInfo->id, (ItemExtraData*)m_BtnDataInfo->pDataInfo);
			}
			else
				ChatSystem->AddItemMsg(m_BtnDataInfo->id, (ui64)0);
			return;
		}
	}else if (uFlags == LKM_CTRL)
	{
		if (m_BtnDataInfo->type == ActionButton_Type_Item)
		{
			UFittingRoom* pFittingRoom = INGAMEGETFRAME(UFittingRoom, FRAME_IG_FITTINGROOMDLG);
			if(!pFittingRoom)
				return;
			pFittingRoom->FittingEquipment(m_BtnDataInfo->id, (ItemExtraData*)NULL);
			return;
		}
	}else
	{
		UDynamicIconButton::OnMouseDown(pt,nClickCnt,uFlags);
		m_bMouseDownPt = pt;
		if (nClickCnt == 1 && m_BtnDataInfo->id != 0 && m_BtnDataInfo->pos != -1)
		{
			m_bBeginDrag = TRUE;
			m_bEnableFreeze = TRUE;
		}
	}
}
void USkillButton::OnMouseUp(const UPoint& pt, UINT uFlags)
{
	UDynamicIconButton::OnMouseUp(pt,uFlags);
}
void USkillButton::OnMouseDragged(const UPoint& position, UINT flags)
{
	if (m_bLockDrag)
	{
		return;
	}
	UDynamicIconButton::OnMouseDragged(position,flags);
	//m_bMouseDownPt = position;
	if (m_bBeginDrag && m_nIconIndex != -1  && m_BtnDataInfo->id !=0  && m_ItemTypeID != UItemSystem::ICT_NPC_TRADE)
	{
		WORD* pPos = new WORD;
		*pPos = (WORD)m_BtnDataInfo->pos;
		//数据   界面 ,位置.
		BeginDragDrop(this,m_spBtnSkin,m_nIconIndex,pPos,(UINT)m_ItemTypeID);
		m_bBeginDrag = FALSE;
	}
}
void USkillButton::OnDragDropEnd(UControl* pDragTo, void* pDragData, UINT nDataFlag)
{
	WORD* pPos = (WORD*)pDragData;
	delete pPos;
}
BOOL USkillButton::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (nDataFlag == UItemSystem::ICT_INVLALID || nDataFlag == UItemSystem::ICT_INVALID_POS || !m_Active)
	{
		return FALSE;
	}

	if (nDataFlag == UItemSystem::ICT_SKILL &&  m_ItemTypeID == UItemSystem::ICT_INGAME_SKILL)
	{
		//暂时只针对技能从技能界面拖动到快捷. 
		//这里的多位置以后需要根据服务器的位置进行 调整, 暂时只是用了在主界面快捷键的按钮的个数. (10个)
		//位置就是用个数的位置来代替. 以后根据需求要求调整. 
		USkillButton* pSkillFrom = UDynamicCast(USkillButton,pDragFrom);
		if (pSkillFrom)
		{
			DataInfo* pData = pSkillFrom->GetStateData();
			SYState()->UI->GetUItemSystem()->SetSkillAciton(m_BtnDataInfo->pos,pData->id,pData->type);
			return TRUE ;
		}
		return FALSE;
	}
	if (nDataFlag == UItemSystem::ICT_INGAME_SKILL &&  m_ItemTypeID == UItemSystem::ICT_INGAME_SKILL)
	{
		USkillButton* pSkillFrom = UDynamicCast(USkillButton,pDragFrom);
		if (pSkillFrom)
		{
			const ui8* FromePos = (const ui8*)pDragData;
			DataInfo* pData = pSkillFrom->GetStateData();
			DataInfo pFormData;
			pFormData.id = GetStateData()->id;
			pFormData.pos = GetStateData()->pos;
			pFormData.type = GetStateData()->type;
			pFormData.num = 1;
			SYState()->UI->GetUItemSystem()->SetSkillAciton(m_BtnDataInfo->pos,pData->id,pData->type);
			SYState()->UI->GetUItemSystem()->SetSkillAciton(*FromePos,pFormData.id,pFormData.type);
			return TRUE ;
		}
		return FALSE;
	}

	DWORD UIFrome = (DWORD)nDataFlag;
	DWORD UITo = (DWORD)m_ItemTypeID;

	const unsigned char* FromePos = (const unsigned char*)pDragData;
	unsigned char ToPos = m_BtnDataInfo->pos;

	if (ToPos == -1)
	{
		return FALSE;
	}

	int num = 1 ;

	SYState()->UI->GetUItemSystem()->MoveItemMsg((ui8)UIFrome, (ui8)UITo, (ui8)*FromePos, (ui8)ToPos,num);


	//发消息
	return TRUE;
	
}

void USkillButton::OnDragDropReject(UControl* pRejectCtrl, const void* pDragData, UINT nDataFlag)
{
	EndDragDrop();
	ReleaseCapture();
}


void USkillButton::SetStateData(DataInfo* dataInfo)
{
	m_BtnDataInfo->id = dataInfo->id;
	m_BtnDataInfo->pos = dataInfo->pos;
	m_BtnDataInfo->type = dataInfo->type;
	m_BtnDataInfo->num = dataInfo->num;
	m_BtnDataInfo->CurNum = dataInfo->CurNum;
	if (dataInfo->pDataInfo)
	{
		if(!m_BtnDataInfo->pDataInfo)
			m_BtnDataInfo->pDataInfo = new ItemExtraData;
		memcpy(m_BtnDataInfo->pDataInfo, dataInfo->pDataInfo, sizeof(ItemExtraData));
	}
	else
	{
		if(m_BtnDataInfo->pDataInfo)
		{
			delete m_BtnDataInfo->pDataInfo;
			m_BtnDataInfo->pDataInfo = NULL;
			
		}
	}

	GetBtnIcon();

}

void USkillButton::SetIconId(int id)
{
	m_BtnDataInfo->id = id;

	GetBtnIcon();
}

void USkillButton::SetNum(ui32 Num)
{
	m_BtnDataInfo->num = Num;
}

void USkillButton::GetBtnIcon(void)
{
	//根据DB数据查询Icon的Skin
	if (m_BtnDataInfo->id == 0)
	{
		m_spBtnSkin = NULL;
		return;
	}
	USkinPtr pIncoSkin = NULL ;
	std::string str ;
	//技能
	if(m_BtnDataInfo->type == UItemSystem::SPELL_DATA_FLAG)
	{
		SpellTemplate* pSpellTemplate = SYState()->SkillMgr->GetSpellTemplate(m_BtnDataInfo->id);
		if(pSpellTemplate)
		{
			// str = pSpellTemplate->m_Icon;
			pSpellTemplate->GetICON(str);
		}else
		{
			UTRACE("get spellInfo failed!");
		}
	}
	//物品

	if (m_BtnDataInfo->type == UItemSystem::ITEM_DATA_FLAG)
	{
		ItemPrototype_Client* ItemInfo = ItemMgr->GetItemPropertyFromDataBase(m_BtnDataInfo->id);
		if(ItemInfo)
		{
			 str = ItemInfo->C_icon;	
		}else
		{
			UTRACE("get ItemInfo failed!");
		}
	}
	

	if (str.length() == 0 )
	{
		UTRACE("load skin  failed!");
	}
	int pos = str.find(";");
	std::string skinstr = str.substr(0,pos);
	std::string strIndex = str.substr(pos+1,str.length() - pos - 1);


	pIncoSkin = sm_System->LoadSkin(skinstr.c_str());
	m_nIconIndex = atoi(strIndex.c_str());



	if (pIncoSkin == NULL)
	{
		UTRACE("load skin  failed!Change Default instead!");

		if (m_BtnDataInfo->type == UItemSystem::ITEM_DATA_FLAG)
			pIncoSkin = sm_System->LoadSkin("Icon\\ItemIcon\\DefaultItemIcon.skin");
		else
			pIncoSkin = sm_System->LoadSkin("Icon\\SkillIcon\\DefaultSkillIcon.skin");

		m_nIconIndex = 0;
	}

	m_spBtnSkin = pIncoSkin;
}
void USkillButton::OnMouseEnter(const UPoint& pt, UINT uFlags)
{
	UButton::OnMouseEnter(pt,uFlags);
	OnShowPerproty(m_bMouseOver);

}
void USkillButton::OnMouseLeave(const UPoint& pt, UINT uFlags)
{
	UButton::OnMouseLeave(pt,uFlags);
	OnHidePerproty();

}
void USkillButton::OnRightMouseUp(const UPoint& position, UINT flags)
{
	UDynamicIconButton::OnRightMouseUp(position, flags);
}
void USkillButton::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	UDynamicIconButton::OnRightMouseDown(position, nRepCnt, flags);
}

void USkillButton::OnShowPerproty(BOOL isShow)
{
	UPoint t_pt= GetParent()->WindowToScreen(GetWindowPos());
	if (isShow&&m_BtnDataInfo->id)
	{
		t_pt.x += GetWidth();
		t_pt.y += GetHeight();
		if(m_BtnDataInfo->type == UItemSystem::SPELL_DATA_FLAG)
		{			
			TipSystem->ShowTip(TIP_TYPE_SPELL,m_BtnDataInfo->id,t_pt);
		}
		else if(m_BtnDataInfo->type == UItemSystem::ITEM_DATA_FLAG)
		{		
			if (m_BtnDataInfo->pDataInfo)
			{
				TipSystem->ShowTip(m_BtnDataInfo->id, t_pt, *m_BtnDataInfo->pDataInfo);
			}
			else
				TipSystem->ShowTip(TIP_TYPE_ITEM,m_BtnDataInfo->id,t_pt,0,m_BtnDataInfo->num);
		}
	}
}
void USkillButton::OnHidePerproty()
{
	TipSystem->HideTip();
}
void USkillButton::SetBtnBgkByStr(const char* strfile)
{
	if (strfile)
	{
		USkinPtr pBGk = sm_System->LoadSkin(strfile);
		if (pBGk)
		{
			m_SkillBtnSkin = pBGk;
		}else
		{
			if (m_SkillBtnSkinStr.GetBuffer())
			{
				pBGk = sm_System->LoadSkin(m_SkillBtnSkinStr.GetBuffer());
				m_SkillBtnSkin = pBGk;
			}
			else
			{
				m_SkillBtnSkin = NULL;
			}
		}
	}else
	{
		if (m_SkillBtnSkinStr.GetBuffer())
		{
			USkinPtr pBGk = sm_System->LoadSkin(m_SkillBtnSkinStr.GetBuffer());
			m_SkillBtnSkin = pBGk;
		}
		else
		{
			m_SkillBtnSkin = NULL;
		}
	}
}