#ifndef __USKILLBUTTON_H__
#define __USKILLBUTTON_H__
#include "UInc.h"
#include "UIItemSystem.h"
#include "UItemContainerEx.h"
class  USkillButton : public UDynamicIconButton
{
	UDEC_CLASS(USkillButton);
public:
	USkillButton();
	~USkillButton();

	struct DataInfo 
	{
		inline DataInfo():type(UItemSystem::ITEM_DATA_FLAG),pos(-1),id(0),num(0),pDataInfo(NULL),CurNum(-1)
	    {
	    }
		~DataInfo()
		{
			if (pDataInfo)
			{
				delete pDataInfo;
				pDataInfo = NULL;
			}
		}
		UItemSystem::ItemType type ;   //ITEM_DATA_FLAG ? ITEM_DATA_FLAG?
		unsigned char pos;     //Pos
		ui32 id ;     //id
		int CurNum;
		UINT num ;
		ItemExtraData* pDataInfo;
	};
	DataInfo* GetStateData(){return m_BtnDataInfo;}
	void     SetStateData(DataInfo* dataInfo);

	void	SetIconId(int id);
	void	SetNum(ui32 Num);

	void     SetUIType(UItemSystem::UI_TYPE ui_type){m_ItemTypeID = ui_type;}
	void     SetLockDrag(BOOL bDrag){m_bLockDrag = bDrag;}

	void GetBtnIcon(void);
	
	void   SetBtnBgkByStr(const char* strfile);

public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);

	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
    virtual void OnMouseDragged(const UPoint& position, UINT flags);
	virtual void OnMouseEnter(const UPoint& pt, UINT uFlags);
	virtual void OnMouseLeave(const UPoint& pt, UINT uFlags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);

	virtual void OnDragDropEnd(UControl* pDragTo, void* pDragData, UINT nDataFlag);
	virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
	virtual void OnDragDropReject(UControl* pRejectCtrl, const void* pDragData, UINT nDataFlag);

	virtual void OnTimer(float fDeltaTime);
public:
	virtual void OnShowPerproty(BOOL isShow);
	virtual void OnHidePerproty();
protected:
	BOOL m_bBeginDrag;

	DataInfo*  m_BtnDataInfo;
	USkinPtr   m_SkillBtnSkin ;
	UString    m_SkillBtnSkinStr;
	BOOL       m_bShowBgk;
	BOOL       m_bLockDrag;
	UPoint     m_bMouseDownPt;
	UItemSystem::UI_TYPE  m_ItemTypeID;
};
#endif