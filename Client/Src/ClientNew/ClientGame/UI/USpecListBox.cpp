#include "StdAfx.h"
#include "USpecListBox.h"
#include "UIRightMouseList.h"
#include "UIGamePlay.h"
#include "UISystem.h"
#include "../../ui/Src/UPrivate.h"

struct SpecListBoxItem 
{
	SpecListBoxItem():UserData(NULL)
	{
		text[0] = 0;
		bEnable = true;
		iCheckState = 0;
		bQuestState = QMGR_QUEST_NOT_AVAILABLE;
	}
	WCHAR text[USpecListBox::ITEM_TEXT_MAX + 4];
	void* UserData;


	unsigned int uiId;
	bool bEnable;
	int iCheckState;
	int bQuestState;
};

struct SpecListBoxItemEx
{
	SpecListBoxItemEx()
	{
		bShowSubText = true;
	}
	SpecListBoxItem Title;
	vector<SpecListBoxItem> m_SubText;
	bool bShowSubText;
};

struct SpecListBoxContext
{
	std::vector<SpecListBoxItemEx> Items;

	inline int GetSize() const 
	{
		return (int)Items.size();
	}

	SpecListBoxItem* GetListItem(int index){
		return &Items[index].Title;
	}
};


UIMP_CLASS(USpecListBox, UGridCtrl);

static int sortColumn;
static bool sIncreasing;

static const char *GetColumn(const char*text)
{
	int ct = sortColumn;
	while(ct--)
	{
		text = strchr(text ,'\t');
		if (!text)
		{
			return"";
			text++;
		}
	}
	return text;
}

void USpecListBox::StaticInit()
{
	UREG_PROPERTY("EnumeRate", UPT_BOOL, UFIELD_OFFSET(USpecListBox,m_Enumerate));
	UREG_PROPERTY("ResizeCell",UPT_BOOL, UFIELD_OFFSET(USpecListBox,m_ResizeCell));
	UREG_PROPERTY("FitParentWidth",UPT_BOOL, UFIELD_OFFSET(USpecListBox,m_FitParentWidth));
	UREG_PROPERTY("ClipColumnText",UPT_BOOL, UFIELD_OFFSET(USpecListBox,m_ClipColumnText));
	UREG_PROPERTY("bitmap", UPT_STRING, UFIELD_OFFSET(USpecListBox, m_strBitmapFile));
}

USpecListBox::USpecListBox(void)
{
	m_Active = TRUE;
	m_Enumerate = TRUE;
	m_ResizeCell = TRUE;
	m_GridCount.Set(1,0);
	m_FitParentWidth = TRUE;
	m_ClipColumnText = FALSE;
	m_pListBoxCtx = new SpecListBoxContext;
	assert(m_pListBoxCtx);

	mCol1.x = 0;
	mCol1.y = 0;
	mWidthCol1 = 0;
	mCol2.x = 0;
	mCol2.y = 0;
	mWidthCol2 = 0;
	mCol3.x = 0;
	mCol3.y = 0;
	mWidthCol3 = 0;
	mCol4.x = 0;
	mCol4.y = 0;
	mWidthCol4 = 0;
	mCol5.x = 0;
	mCol5.y = 0;
	mWidthCol5 = 0;
	mLine1.x = 0;
	mLine1.y = 0;
	mWidthLine1 = 0;
	mLine2.x = 0;
	mLine2.y = 0;
	mWidthLine2 = 0;
	m_OldSize = UPoint(0,0);

	mLine1_1 = UPoint(0,0);
	mLine1_2 = UPoint(0,0);
	mWidthLine1_1 = 0;
	mWidthLine1_2 = 0;
	mLine2_1 = UPoint(0,0);
	mLine2_2 = UPoint(0,0);
	mWidthLine2_1 = 0;
	mWidthLine2_2 = 0;
	mLine1_3 = UPoint(0,0);
	mWidthLine1_3 = 0;
	mLine2_3 = UPoint(0,0);
	mWidthLine2_3 = 0;

	mGoldOff = 420;
	mSilverOff = 446;
	mBroneOff = 473;
	mYuanBaoOff = 473;

	m_YPTexture = UControl::sm_UiRender->LoadTexture("DATA\\UI\\AuctionDlg\\yi.png");

	m_GoldTexture = UControl::sm_UiRender->LoadTexture("DATA\\UI\\Public\\gold.png");
	m_SilverTexture = UControl::sm_UiRender->LoadTexture("DATA\\UI\\Public\\silver.png");
	m_BroneTexture = UControl::sm_UiRender->LoadTexture("DATA\\UI\\Public\\copper.png");

	m_YuanBaoTexture = UControl::sm_UiRender->LoadTexture("DATA\\UI\\Public\\YuanBao.png");
}

USpecListBox::~USpecListBox(void)
{
	if (m_pListBoxCtx)
	{
		delete m_pListBoxCtx;
	}
}

void USpecListBox::GetRootSub(unsigned int uiId, int& iRoot, int& iSub)
{
	for(int uiRoot = 0; uiRoot < m_pListBoxCtx->GetSize(); uiRoot++)
	{
		unsigned int uiUserData = m_pListBoxCtx->Items[uiRoot].Title.uiId;//*(unsigned int*)(m_pListBoxCtx->Items[uiRoot].Title.UserData);
		if(uiUserData == uiId)
		{
			iRoot = uiRoot;
			return;
		}
		else
		{
			for(unsigned int uiSub = 0; uiSub < m_pListBoxCtx->Items[uiRoot].m_SubText.size(); uiSub++)
			{
				uiUserData = m_pListBoxCtx->Items[uiRoot].m_SubText[uiSub].uiId;
				if(uiUserData == uiId)
				{
					iRoot = uiRoot;
					iSub = uiSub;
					return;
				}
			}
		}
	}
}

int USpecListBox::GetSelId()
{
	UINT pkSelIndex = m_SelGrid.y;
	if (pkSelIndex >= 0)
	{
		int iSelIndex = pkSelIndex;
		int iCount = 0;
		for(int uiRoot = 0; uiRoot < m_pListBoxCtx->GetSize(); uiRoot++)
		{
			if(iCount == iSelIndex)
			{
				return m_pListBoxCtx->Items[uiRoot].Title.uiId;
			}
			iCount++;
		
			if(m_pListBoxCtx->Items[uiRoot].bShowSubText)
			{
				for(unsigned int uiSub = 0; uiSub < m_pListBoxCtx->Items[uiRoot].m_SubText.size(); uiSub++)
				{
					if(iCount == iSelIndex)
					{
						return m_pListBoxCtx->Items[uiRoot].m_SubText[uiSub].uiId;
					}
					iCount++;
				}
			}
			
		}
	}
	return -1;
}
	

void USpecListBox::GetShowIndex(unsigned int idx, int& iRoot, int& iSub)
{
	int iCount = 0;
	for(int uiRoot = 0; uiRoot < m_pListBoxCtx->GetSize(); uiRoot++)
	{
		if(iCount == idx)
		{
			iRoot = uiRoot;
			return;
		}
		//if(m_pListBoxCtx->Items[uiRoot].bMainQuestFinished == false)
			iCount++;
		if(m_pListBoxCtx->Items[uiRoot].bShowSubText)
		{
			for(unsigned int uiSub = 0; uiSub < m_pListBoxCtx->Items[uiRoot].m_SubText.size(); uiSub++)
			{
				if(iCount == idx)
				{
					iRoot = uiRoot;
					iSub = uiSub;
					return;
				}
				iCount++;
			}
		}
	}
}

int USpecListBox::GetCurSel() const
{
	return m_SelGrid.y;
}

void USpecListBox::SetCurSel(int index)
{
	SetSel(index, 0);
}
bool USpecListBox::SetCheckState(UINT uiId, bool bCheck)
{
	for(int uiRoot = 0; uiRoot < m_pListBoxCtx->GetSize(); uiRoot++)
	{
		if(uiId == m_pListBoxCtx->Items[uiRoot].Title.uiId)
		{
			m_pListBoxCtx->Items[uiRoot].Title.iCheckState = bCheck;
			return true ;
		}
		for(unsigned int uiSub = 0; uiSub < m_pListBoxCtx->Items[uiRoot].m_SubText.size(); uiSub++)
		{
			if(uiId == m_pListBoxCtx->Items[uiRoot].m_SubText[uiSub].uiId)
			{
				m_pListBoxCtx->Items[uiRoot].m_SubText[uiSub].iCheckState = bCheck ;
				return true;
			}
		}
	}

	return false;
}
void USpecListBox::SetCurSelByID(UINT uiid)
{
	int iroot = -1; 
	int isub = -1;

	GetRootSub(uiid, iroot, isub);
	if (iroot != -1 && isub != -1)
	{
		m_pListBoxCtx->Items[iroot].bShowSubText = true;
		SetItemCount(GetShowItemCount());
		ClearSel();
	}

	UINT index =  0;

	for(int uiRoot = 0; uiRoot < m_pListBoxCtx->GetSize(); uiRoot++)
	{
		if(uiid == m_pListBoxCtx->Items[uiRoot].Title.uiId)
		{
			break ;
		}
		index ++ ;
		BOOL bNeedBreak = FALSE ;
		if(m_pListBoxCtx->Items[uiRoot].bShowSubText)
		{
			for(unsigned int uiSub = 0; uiSub < m_pListBoxCtx->Items[uiRoot].m_SubText.size(); uiSub++)
			{
				if(uiid == m_pListBoxCtx->Items[uiRoot].m_SubText[uiSub].uiId)
				{
					bNeedBreak = TRUE;
					break ;
				}else
				{
					index++;
				}
			}
		}

		if (bNeedBreak)
		{
			break ;
		}
	}

	SetCurSel(index);
}
int USpecListBox::GetCheckStateById(unsigned int uiId)
{
	for(int uiRoot = 0; uiRoot < m_pListBoxCtx->GetSize(); uiRoot++)
	{
		if(uiId == m_pListBoxCtx->Items[uiRoot].Title.uiId)
		{
			return m_pListBoxCtx->Items[uiRoot].Title.iCheckState;
		}
		if(m_pListBoxCtx->Items[uiRoot].bShowSubText)
		{
			for(unsigned int uiSub = 0; uiSub < m_pListBoxCtx->Items[uiRoot].m_SubText.size(); uiSub++)
			{
				if(uiId == m_pListBoxCtx->Items[uiRoot].m_SubText[uiSub].uiId)
				{
					return m_pListBoxCtx->Items[uiRoot].m_SubText[uiSub].iCheckState ;
				}
			}
		}

	}

	return -1;
}

void USpecListBox::SetItemEnable(int index, bool bEnable)
{
	SpecListBoxItem* pItem = m_pListBoxCtx->GetListItem(index);
	if (pItem == NULL)
		return;

	pItem->bEnable = bEnable;
}

bool USpecListBox::IsItemEnable(int index)
{
	SpecListBoxItem* pItem = m_pListBoxCtx->GetListItem(index);
	if (pItem == NULL)
		return false;

	return pItem->bEnable;
}

UINT USpecListBox::GetSelectedRow()
{
	return m_SelGrid.y;
}
void USpecListBox::OnSelChange(int row, int col)
{
	CQuestGrid::OnSelChange(row, col);
}

void USpecListBox::InsertItem(int index, const char *text, int bFinish , void* pUserData )
{
	if (text == NULL || text[0] == '\0')
	{
		return;
	}
	SpecListBoxItemEx e;
	int Ret = MultiByteToWideChar(CP_UTF8, 0, text, strlen(text), e.Title.text, ITEM_TEXT_MAX);
	e.Title.text[Ret] = 0;
	e.Title.UserData = pUserData;
	if (bFinish > QMGR_QUEST_NOT_AVAILABLE && bFinish <= QMGR_QUEST_FINISHED)
	{
		e.Title.bEnable = true;
	}else
	{
		e.Title.bEnable = false;
	}
	e.Title.bQuestState =  bFinish;
	e.Title.uiId = *(unsigned int*)pUserData;
	e.bShowSubText = false;

	if (m_pListBoxCtx->Items.empty())
	{
		m_pListBoxCtx->Items.push_back(e);
	}
	else
	{
		if (index > (int)m_pListBoxCtx->Items.size() )
		{
			index = m_pListBoxCtx->Items.size();
		}
		m_pListBoxCtx->Items.insert(m_pListBoxCtx->Items.begin()+ index,e);
		m_pListBoxCtx->Items[index] = e;
	}

	SetItemCount(GetShowItemCount());
}

void USpecListBox::InsertSubItem(unsigned int uiRootIndex, unsigned int uiSubIndex, const char* text,int bFinish, void* pUserData )
{
	if (text == NULL || text[0] == '\0')
	{
		return;
	}

	if((unsigned int)m_pListBoxCtx->GetSize() < uiRootIndex)
	{
		return;
	}

	SpecListBoxItem e;
	int Ret = MultiByteToWideChar(CP_UTF8, 0, text, strlen(text), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
	e.UserData = pUserData;
	if (bFinish > QMGR_QUEST_NOT_AVAILABLE && bFinish <= QMGR_QUEST_FINISHED)
	{
		e.bEnable = true;
	}else
	{
		e.bEnable = false;
	}
	e.bQuestState = bFinish;
	e.uiId = *(unsigned int*)pUserData;

	if (m_pListBoxCtx->Items.empty())
	{
		SpecListBoxItemEx ex;
		ex.Title = e;
		m_pListBoxCtx->Items.push_back(ex);
	}
	else
	{
		if (uiSubIndex > (int)m_pListBoxCtx->Items[uiRootIndex].m_SubText.size() )
		{
			uiSubIndex = m_pListBoxCtx->Items[uiRootIndex].m_SubText.size();
		}
		m_pListBoxCtx->Items[uiRootIndex].m_SubText.insert(m_pListBoxCtx->Items[uiRootIndex].m_SubText.begin() + uiSubIndex, e);
		m_pListBoxCtx->Items[uiRootIndex].m_SubText[uiSubIndex] = e;
	}

	SetItemCount(GetShowItemCount());
}
void USpecListBox::AddItem(const char *text, void* pUserData)
{
	AddItem(text,QMGR_QUEST_FINISHED,pUserData);
}
void USpecListBox::InsertItem(int index, const char *text, void* pUserData)
{
	InsertItem(index,text,QMGR_QUEST_FINISHED,pUserData);
}

void USpecListBox::AddItem(const char *text,int bFinish, void* pUserData)
{
	if (text == NULL || text[0] == '\0')
	{
		return;
	}
	SpecListBoxItemEx e ;
	e.Title.UserData = pUserData;
	if (bFinish > QMGR_QUEST_NOT_AVAILABLE && bFinish <= QMGR_QUEST_FINISHED)
	{
		e.Title.bEnable = true;
	}else
	{
		e.Title.bEnable = false;
	}
	e.Title.bQuestState = bFinish;
	e.bShowSubText = false;
	if(pUserData)
		e.Title.uiId = *(unsigned int*)pUserData;
	int Ret = MultiByteToWideChar(CP_UTF8, 0, text, strlen(text), e.Title.text, ITEM_TEXT_MAX);
	e.Title.text[Ret] = 0;
	m_pListBoxCtx->Items.push_back(e);
	SetItemCount(GetShowItemCount());
}

void USpecListBox::AddSubItem(unsigned int uiRootIndex, const char* text, int bFinish, void* pUserData /* = NULL */ )
{
	if(text == NULL || text[0] == '\0')
	{
		return;
	}

	if((unsigned int)m_pListBoxCtx->GetSize() < uiRootIndex)
	{
		return;
	}
	SpecListBoxItem e;
	e.UserData = pUserData;
	e.bQuestState = bFinish;
	if (bFinish > QMGR_QUEST_NOT_AVAILABLE && bFinish <= QMGR_QUEST_FINISHED)
	{
		e.bEnable = true;
	}else
	{
		e.bEnable = false;
	}
	e.uiId = *(unsigned int*)pUserData;
	int Ret = MultiByteToWideChar(CP_UTF8, 0, text, strlen(text), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
	m_pListBoxCtx->Items[uiRootIndex].m_SubText.push_back(e);

	SetItemCount(GetShowItemCount());
}

void USpecListBox::AddItem(const WCHAR* text,int bFinish, void* pUserData)
{
	if (text == NULL || text[0] == 0)
	{
		return;
	}
	SpecListBoxItemEx e ;
	e.Title.UserData = pUserData;
	if (bFinish > QMGR_QUEST_NOT_AVAILABLE && bFinish <= QMGR_QUEST_FINISHED)
	{
		e.Title.bEnable = true;
	}else
	{
		e.Title.bEnable = false;
	}
	e.Title.bQuestState =bFinish; 
	e.Title.uiId = *(unsigned int*)pUserData;
	e.bShowSubText = false;
	wcscpy(e.Title.text, text);
	m_pListBoxCtx->Items.push_back(e);
	SetItemCount(m_pListBoxCtx->GetSize());
}

void USpecListBox::AddSubItem(unsigned int uiRootIndex, const WCHAR* text, int Finish, void* pUserData /* = NULL */ )
{
	if(text == NULL || text[0] == 0)
	{
		return;
	}

	if((unsigned int)m_pListBoxCtx->GetSize() < uiRootIndex)
	{
		return;
	}

	SpecListBoxItem e;
	e.UserData = pUserData;
	e.bQuestState = Finish;
	if (Finish > QMGR_QUEST_NOT_AVAILABLE && Finish <= QMGR_QUEST_FINISHED)
	{
		e.bEnable = true;
	}else
	{
		e.bEnable = false;
	}
	e.uiId = *(unsigned int*)pUserData;
	wcscpy(e.text, text);
	m_pListBoxCtx->Items[uiRootIndex].m_SubText.push_back(e);

	SetItemCount(GetShowItemCount());
}

void USpecListBox::SetItemText(int index, const char *text ,int bFinish )
{
	if (index < 0 || index >= m_pListBoxCtx->GetSize())
	{
		UTRACE("UListBox::SetItemText(index) index 越界");
		return;
	}

	if (text == NULL || text[0] == '\0')
	{
		UTRACE("text is null.");
		return;
	}
	SpecListBoxItemEx& e = m_pListBoxCtx->Items[index];
	int Ret = MultiByteToWideChar(CP_UTF8, 0, text, strlen(text), e.Title.text, ITEM_TEXT_MAX);
	e.Title.text[Ret] = 0;
	if (bFinish > QMGR_QUEST_NOT_AVAILABLE && bFinish <= QMGR_QUEST_FINISHED)
	{
		e.Title.bEnable = true;
	}else
	{
		e.Title.bEnable = false;
	}
	e.Title.bQuestState = bFinish;
	//SetGridCount(m_pListBoxCtx->GetSize(), 1);
}

void USpecListBox::SetSubItemText(unsigned int uiRootIndex, unsigned int uiSubIndex, const char* text, int bFinish)
{
	if(uiRootIndex >= (unsigned int)m_pListBoxCtx->GetSize())
	{
		return;
	}

	if(text == NULL || text[0] == '\0')
	{
		return;
	}

	if(uiSubIndex >= m_pListBoxCtx->Items[uiRootIndex].m_SubText.size())
	{
		return;
	}

	SpecListBoxItem& e = m_pListBoxCtx->Items[uiRootIndex].m_SubText[uiSubIndex];
	int Ret = MultiByteToWideChar(CP_UTF8, 0, text, strlen(text), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
	if (bFinish > QMGR_QUEST_NOT_AVAILABLE && bFinish <= QMGR_QUEST_FINISHED)
	{
		e.bEnable = true;
	}else
	{
		e.bEnable = false;
	}
	e.bQuestState = bFinish;
	
}

INT USpecListBox::FindItemByData(void* pUserData)
{
	for (UINT i = 0 ; i < (UINT)m_pListBoxCtx->GetSize(); i++)
	{
		if (m_pListBoxCtx->Items[i].Title.UserData == pUserData)
		{
			return i ;
		}
	}
	return -1 ;
}

INT USpecListBox::FindItem(const char *text)
{
	assert(text);
	WCHAR wszBuf[ITEM_TEXT_MAX];
	int nRet = MB2WC(text, strlen(text), wszBuf, ITEM_TEXT_MAX -1);
	wszBuf[nRet] = 0;
	return FindItem(wszBuf);
}

INT USpecListBox::FindItem(const WCHAR *text)
{
	assert(text);
	for (UINT i = 0 ; i < (UINT)m_pListBoxCtx->GetSize(); i++)
	{
		if (wcscmp(m_pListBoxCtx->Items[i].Title.text, text) == 0)
		{
			return i ;
		}
	}
	return -1;
}

void USpecListBox::SetItemCount(int nNumItems)
{
	m_GridCount.x = 1;
	m_GridCount.y = nNumItems;

	//if (m_Style && m_Style->m_spFont)
	//{
	//	/*  if ( mSize.x == 1 && mFitParentWidth )
	//	{
	//	UScrollBar* parent = dynamic_cast<UScrollBar *>(getParent());
	//	if ( parent )
	//	mCellSize.x = parent->GetContentSize().x;
	//	}
	//	else
	//	{*/
	//	// Find the maximum width cell:
	//	/*   INT maxWidth = 1;
	//	for ( UINT i = 0; i < mList.size(); i++ )
	//	{
	//	UINT rWidth = getRowWidth( &mList[i] );
	//	if ( rWidth > maxWidth )
	//	maxWidth = rWidth;
	//	}

	//	mCellSize.x = maxWidth + 8;*/
	//	// }

	//	m_GridSize.y = m_Style->m_spFont->GetHeight() + 2;
	//}

	//// 计算新控件大小, 只放大, 注意这个地方只适合于父窗口是滚动条的情况, 否则, 控件行为有些怪异
	//UPoint NewSize(m_GridCount.x * m_GridSize.x + m_HeadSize.x, m_GridCount.y * m_GridSize.y + m_HeadSize.y);
	//NewSize.x = __max(m_GridCount.x * m_GridSize.x + m_HeadSize.x , m_Size.x);
	//NewSize.y = __max(m_GridCount.y * m_GridSize.y + m_HeadSize.y, m_Size.y);
	//SetSize(NewSize);
	SetHeight(m_GridCount.y * m_GridSize.y + m_HeadSize.y);
	UScrollBar* pSB = UDynamicCast(UScrollBar,GetParent());
	if (pSB)
	{
		//SetHeight(m_GridCount.y * m_GridSize.y + m_HeadSize.y);
		pSB->ReCalLayout();
		//pSB->scrollTo(0,m_GridCount.y * m_GridSize.y + m_HeadSize.y);
	}
}

BOOL USpecListBox::OnCreate()
{
	if (!CQuestGrid::OnCreate())
	{
		return FALSE;
	}

	SetBitmap(m_strBitmapFile.GetBuffer());
	m_OldSize = m_Size;
	return TRUE;
}

void USpecListBox::OnDestroy()
{
	m_spSkin = NULL;
	CQuestGrid::OnDestroy();
}

BOOL USpecListBox::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	return FALSE;
}
void USpecListBox::GetScrollLineSizes(UINT *rowHeight, UINT *columnWidth)
{
	*columnWidth = 20;
	*rowHeight = m_GridSize.y;
}

void USpecListBox::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	if (!m_Active || !m_bCreated || !m_Visible)
	{
		return ;
	}

	UPoint pta = ScreenToWindow(position);
	pta.x -= m_HeadSize.x;
	pta.y -= m_HeadSize.y;

	UPoint cell(  (pta.x < 0 ? -1 : pta.x / m_GridSize.x), (pta.y < 0 ? -1 : pta.y / m_GridSize.y));
	if (cell.x >= 0 && cell.y >=0  && cell.x <= m_GridCount.x && cell.y <= m_GridCount.y)
	{
		UPoint prevSelected = m_SelGrid;
		SetSel(cell.y, cell.x);
		//SelRightMouseDown(cell.y,cell.x);
	}
}

void USpecListBox::OnRightMouseUp(const UPoint& position, UINT flags)
{
	if (!m_Active || !m_bCreated || !m_Visible)
	{
		return ;
	}

	UPoint pta = ScreenToWindow(position);
	pta.x -= m_HeadSize.x;
	pta.y -= m_HeadSize.y;

	UPoint cell(  (pta.x < 0 ? -1 : pta.x / m_GridSize.x), (pta.y < 0 ? -1 : pta.y / m_GridSize.y));

	if (flags == LKM_SHIFT)
	{
		if (cell.x >= 0 && cell.x < m_GridSize.x && cell.y < m_GridCount.y && cell.y >= 0)
		{

			SetSel(cell.y, 0);
			DispatchNotify(UGD_SHIFT_AND_RBCLICK);
		}
		return ;
	}

	if (cell.x >= 0 && cell.y >=0  && cell.x <= m_GridCount.x && cell.y <= m_GridCount.y)
	{
		UPoint prevSelected = m_SelGrid;
		SetSel(cell.y, cell.x);
		SelRightMouseDown(cell.y,cell.x);
	}
}

void USpecListBox::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	if (!m_Active || !m_bCreated || !m_Visible)
	{
		return ;
	}

	UPoint pta = ScreenToWindow(point);
	pta.x -= m_HeadSize.x;
	pta.y -= m_HeadSize.y;


	// 检查CheckButton
	UPoint cellC((pta.x < 0 ? -1 : pta.x), (pta.y < 0 ? -1 : pta.y / m_GridSize.y));
	if (cellC.x >= (m_GridSize.x - 15) && cellC.y >=0  && cellC.x < m_GridSize.x && cellC.y < m_GridCount.y)
	{
		SetSel(cellC.y, 0);
		if (CheckCanClickBtn(cellC.y))
		{
			DispatchNotify(UBN_CLICKED);
		}
		return;
	}
	
	// 检查TreeButton
	UPoint cellT((pta.x < 0 ? -1 : pta.x), (pta.y < 0 ? -1 : pta.y / m_GridSize.y));
	if (cellT.x >= 0 && cellT.y >=0  && cellT.x < 16 && cellT.y < m_GridCount.y)
	{
		SetSel(-1, -1);
		if (!CheckIsEnable(cellT.y))
		{
			OnTreeButtonClick(cellT.y);
		}
		return;
	}

	UPoint cell(  (pta.x < 0 ? -1 : pta.x / m_GridSize.x), (pta.y < 0 ? -1 : pta.y / m_GridSize.y));
	if (cell.x >= 0 && cell.y >=0  && cell.x < m_GridCount.x && cell.y < m_GridCount.y)
	{
		UPoint prevSelected = m_SelGrid;
		if (OnlyCheckParentIsEnable(cell.y)) // (cell.y))
		{
			SetSel(cell.y, cell.x);
			if (nRepCnt > 1 && prevSelected == m_SelGrid)
			{
				DispatchNotify(UGD_DBCLICK);
			}
		}else
		{
			SetSel(-1, -1);
			OnTreeButtonClick(cellT.y);
			return;
		}
	}
}

void USpecListBox::SelRightMouseDown(int row ,int col)
{
	UScrollBar * _temp = (UScrollBar *)GetParent();
	if (_temp)
	{
		_temp->DispatchChildNotify(UGD_RBCLICK);
		return;
	}
	DispatchNotify(UGD_RBCLICK);
}
bool USpecListBox::OnlyCheckParentIsEnable(int col) // 
{
	int iRoot = -1;
	int iSub = -1;
	GetShowIndex(col, iRoot, iSub);
	if(iRoot != -1 && iSub != -1)
	{
		int state = m_pListBoxCtx->Items[iRoot].m_SubText[iSub].bQuestState;
		if (state <= QMGR_QUEST_FINISHED)
		{
			return true;
		}
	}else
	{
		if (iRoot == -1)
		{
			return false;
		}
		if (m_pListBoxCtx->Items.size() &&m_pListBoxCtx->Items[iRoot].Title.bEnable)
		{
			return true;
		}
	}
	return false;
}
bool USpecListBox::CheckIsEnable(int col)
{
	int iRoot = -1;
	int iSub = -1;
	GetShowIndex(col, iRoot, iSub);
	if(iRoot != -1 && iSub != -1)
	{
		if (m_pListBoxCtx->Items[iRoot].m_SubText[iSub].bEnable)
		{
			return true;
		}
	}else
	{
		if (iRoot == -1)
		{
			return false;
		}
		if (m_pListBoxCtx->Items.size() &&m_pListBoxCtx->Items[iRoot].Title.bEnable)
		{
			return true;
		}
	}
	return false;
}
bool USpecListBox::CheckCanClickBtn(int col)
{
	int iRoot = -1;
	int iSub = -1;
	GetShowIndex(col, iRoot, iSub);
	if(iRoot != -1 && iSub != -1)
	{
		if (m_pListBoxCtx->Items[iRoot].m_SubText[iSub].bEnable)
		{
			m_pListBoxCtx->Items[iRoot].m_SubText[iSub].iCheckState = !m_pListBoxCtx->Items[iRoot].m_SubText[iSub].iCheckState;
			return true;
		}
	}else
	{
		if (m_pListBoxCtx->Items.size() && m_pListBoxCtx->Items[iRoot].Title.bEnable)
		{
			m_pListBoxCtx->Items[iRoot].Title.iCheckState = !m_pListBoxCtx->Items[iRoot].Title.iCheckState ;
			if (m_pListBoxCtx->Items[iRoot].Title.bQuestState > QMGR_QUEST_NOT_AVAILABLE 
				&& m_pListBoxCtx->Items[iRoot].Title.bQuestState <= QMGR_QUEST_FINISHED)
			{
				return true;
			}else
			{
				return false;
			}
		}
	}
	return false;
}

void USpecListBox::OnTreeButtonClick(int col)
{
	int iRoot = -1;
	int iSub = -1;
	GetShowIndex(col, iRoot, iSub);

	if(iRoot == -1)
		return;

	if(iRoot != -1 && iSub == -1)
	{
		m_pListBoxCtx->Items[iRoot].bShowSubText = !m_pListBoxCtx->Items[iRoot].bShowSubText;
	}
	SetItemCount(GetShowItemCount());
}

UINT USpecListBox::GetItemCount()
{
	return (UINT)m_pListBoxCtx->Items.size();
}

UINT USpecListBox::GetShowItemCount()
{
	UINT iCount = 0;
	iCount += m_pListBoxCtx->GetSize();
	for(unsigned int uiRoot = 0; uiRoot < (unsigned int)m_pListBoxCtx->GetSize(); uiRoot++)
	{
		if(m_pListBoxCtx->Items[uiRoot].bShowSubText == true)
		{
			iCount += m_pListBoxCtx->Items[uiRoot].m_SubText.size();
		}
	}

	return iCount;
}

UINT USpecListBox::GetShowItemCount(unsigned int idx)
{
	UINT iCount = 0;
	UINT uiIndex = 0;
	for(unsigned int uiRoot = 0; uiRoot < (unsigned int)m_pListBoxCtx->GetSize(); uiRoot++)
	{
		if(uiIndex == idx)
			return iCount;
		iCount++;
		uiIndex++;
		if(m_pListBoxCtx->Items[uiRoot].bShowSubText)
		{
			for(unsigned int uiSub = 0; uiSub < (unsigned int)m_pListBoxCtx->Items[uiRoot].m_SubText.size(); uiSub++)
			{
				if(uiIndex == idx)
				{
					return iCount;
				}
				iCount++;
				uiIndex++;
			}
		}
	}

	return iCount;
}


UINT USpecListBox::GetSubItemCount(unsigned int uiRootIndex)
{
	assert(uiRootIndex < GetItemCount());
	return (UINT)m_pListBoxCtx->Items[uiRootIndex].m_SubText.size();
}

void* USpecListBox::GetItemData(int index)
{
	assert(index >= 0 && index < (int)GetItemCount());
	return m_pListBoxCtx->Items[index].Title.UserData;
}

void USpecListBox::Clear()
{
	m_pListBoxCtx->Items.clear();
	SetItemCount(0);
	m_HoverGrid.Set(-1,-1);
	ClearSel();
}

void USpecListBox::Sort(UINT column, bool increasing /* = TRUE */)
{

}

void USpecListBox::SortNumerical(UINT column, bool increasing /* = TRUE */)
{

}

void USpecListBox::OnRemove()
{
	Clear();
	CQuestGrid::OnRemove();
}

void USpecListBox::DeleteItem(INT index)
{
	//if (index < 0 && index > (int)m_pListBoxCtx->Items.size())
	//{
	//	return;
	//}
	//m_pListBoxCtx->Items.erase(m_pListBoxCtx->Items.begin()+index);
	//SetItemCount(m_pListBoxCtx->GetSize());
	//ClearSel();
	if(index < 0) return;


	int iRoot = -1;
	int iSub = -1;
	GetShowIndex(index, iRoot, iSub);

	if(iRoot != -1 && iSub != -1)
	{
		m_pListBoxCtx->Items[iRoot].m_SubText.erase(m_pListBoxCtx->Items[iRoot].m_SubText.begin() + iSub);
	}
	else if(iRoot != -1)
	{
		m_pListBoxCtx->Items.erase(m_pListBoxCtx->Items.begin() + iRoot);
	}

	SetItemCount(GetShowItemCount());
	ClearSel();
}

void USpecListBox::DeleteItemById(unsigned int uiId)
{
	for(int uiRoot = 0; uiRoot < m_pListBoxCtx->GetSize(); uiRoot++)
	{
		unsigned int uiUserData = m_pListBoxCtx->Items[uiRoot].Title.uiId;//*(unsigned int*)(m_pListBoxCtx->Items[uiRoot].Title.UserData);
		if(uiUserData == uiId)
		{
			m_pListBoxCtx->Items.erase(m_pListBoxCtx->Items.begin() + uiRoot);
			SetItemCount(GetShowItemCount());
			ClearSel();
			return;
		}
		for(unsigned int uiSub = 0; uiSub < m_pListBoxCtx->Items[uiRoot].m_SubText.size(); uiSub++)
		{
			uiUserData = m_pListBoxCtx->Items[uiRoot].m_SubText[uiSub].uiId;
			if(uiUserData == uiId)
			{
				m_pListBoxCtx->Items[uiRoot].m_SubText.erase(m_pListBoxCtx->Items[uiRoot].m_SubText.begin() + uiSub);
				SetItemCount(GetShowItemCount());
				ClearSel();
				return;
			}
		}
	}
}

int USpecListBox::GetSelectedText(char* pText, int nMaxLen)
{
	if (m_SelGrid.y == -1 || m_SelGrid.y >= (int)GetItemCount())
	{
		return 0;
	}

	if (pText == NULL || nMaxLen <= 0)
	{
		return wcslen(m_pListBoxCtx->Items[m_SelGrid.y].Title.text);
	}

	int nRet = WideCharToMultiByte(CP_UTF8, 0, m_pListBoxCtx->Items[m_SelGrid.y].Title.text, -1, pText, nMaxLen, NULL, NULL);
	return nRet;
}

void USpecListBox::OnRender(const UPoint& offset, const URect &updateRect)
{
	CQuestGrid::OnRender(offset,updateRect);
}

void USpecListBox::DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver)
{

	if(m_pListBoxCtx->Items.size() == 0)
		return;

	UPoint mOldSize = m_OldSize;
	float pxRule = 1.0f;//(float)m_Size.x / (float)mOldSize.x;
	float pyRule = 1.0f;//(float)m_Size.y / (float)mOldSize.y;

	if (m_Style->mBorder)
	{
		offset.x += 1;
	}
	
	int iCount = 0;
	bool bEnable = false;
	bool bFinish = false;
	const WCHAR* text;
	ECheckSkin eSkinID;
	void* pUserDate = NULL;

	int iRoot = -1;
	int iSub = -1;
	GetShowIndex(cell.y, iRoot, iSub);
	if(iRoot != -1 && iSub != -1)
	{
		bEnable = m_pListBoxCtx->Items[iRoot].m_SubText[iSub].bEnable;
		text = m_pListBoxCtx->Items[iRoot].m_SubText[iSub].text;
		eSkinID = (ECheckSkin)m_pListBoxCtx->Items[iRoot].m_SubText[iSub].iCheckState;
		if (m_pListBoxCtx->Items[iRoot].m_SubText[iSub].bQuestState == QMGR_QUEST_FINISHED + 1)
		{
			bFinish = true;
		}
	}
	else if(iRoot != -1)
	{
		bEnable = m_pListBoxCtx->Items[iRoot].Title.bEnable;
		text = m_pListBoxCtx->Items[iRoot].Title.text;
		eSkinID = (ECheckSkin)m_pListBoxCtx->Items[iRoot].Title.iCheckState;
		if (m_pListBoxCtx->Items[iRoot].Title.bQuestState == QMGR_QUEST_FINISHED + 1)
		{
			bFinish = true;
		}
		pUserDate = m_pListBoxCtx->Items[iRoot].Title.UserData;
	}
	

	URect hightLightRect = URect(offset,m_GridSize);


	UColor fontColor = m_Style->m_FontColor ;
	DrawGridItemBkg(selected, bEnable, mouseOver, pUserDate, hightLightRect, &fontColor);
	
	if (GetParent())
	{
		UScrollBar* pkParent =  UDynamicCast(UScrollBar, GetParent());
		if (pkParent)
		{
			int NewOffset = m_Position.x ;

			UINT ParentWidth = GetParent()->GetWidth();
			
			hightLightRect.SetWidth((float)pkParent->GetContentSize().x);
			
		}
	}
	
	
	UPoint renderoffset = offset;
	UColor renderColor = fontColor;
	int iStartPos = 0;
	int iSerPos = 0;
	int iSize = wcslen(text);
	WCHAR RenderText[256];
	int iCurWidthOffset = 0;
	for( ; iSerPos < iSize; )
	{
		if(text[iSerPos] == '<')
		{
			UINT isStartPos = iSerPos;
			// it's probably some kind of tag:
			const WCHAR* pSubStr = &text[iSerPos];
			if(_wcsnicmp(pSubStr + 1, L"color:", 6) == 0)
			{
				if(pSubStr[16 + 7] == '>')
				{
					iSerPos += 7;
					static char szTemp[4] = {};
					szTemp[0] = (char)pSubStr[7 + 1]; szTemp[1] = (char)pSubStr[7 + 2]; szTemp[2] = (char)pSubStr[7 + 3];
					szTemp[3] = 0;
					renderColor.r = atoi(szTemp);
					szTemp[0] = (char)pSubStr[7 + 5]; szTemp[1] = (char)pSubStr[7 + 6]; szTemp[2] = (char)pSubStr[7 + 7];
					szTemp[3] = 0;
					renderColor.g = atoi(szTemp);
					szTemp[0] = (char)pSubStr[7 + 9]; szTemp[1] = (char)pSubStr[7 + 10]; szTemp[2] = (char)pSubStr[7 + 11];
					szTemp[3] = 0;
					renderColor.b = atoi(szTemp);
					szTemp[0] = (char)pSubStr[7 + 13]; szTemp[1] = (char)pSubStr[7 + 14]; szTemp[2] = (char)pSubStr[7 + 15];
					szTemp[3] = 0;
					renderColor.a = atoi(szTemp);

					iSerPos += 17;
				}
				else
				{
					RenderText[iStartPos] = text[iSerPos];
					iStartPos++;
					iSerPos++;
				}
			}
			if(_wcsnicmp(pSubStr + 1, L"defaultcolor>", 13) == 0)
			{
				iSerPos += 14;
				renderColor = fontColor;
			}
			if(_wcsnicmp(pSubStr + 1, L"center>", 7) == 0)
			{
				renderoffset.y = offset.y + (m_GridSize.y - m_Style->m_spFont->GetHeight()) / 2;
				iSerPos += 8;
			}
			if(_wcsnicmp(pSubStr + 1, L"Col1>", 5) == 0)
			{
				renderoffset.x = (int)(offset.x + mCol1.x * pxRule);
				renderoffset.y = (int)(offset.y + mCol1.y * pyRule);
				iSerPos += 6;
				iCurWidthOffset = mWidthCol1;
			}
			if(_wcsnicmp(pSubStr + 1, L"Col2>", 5) == 0)
			{
				renderoffset.x = (int)(offset.x + mCol2.x * pxRule);
				renderoffset.y = (int)(offset.y + mCol2.y * pyRule);
				iSerPos += 6;
				iCurWidthOffset = mWidthCol2;
			}
			if(_wcsnicmp(pSubStr + 1, L"Col3>", 5) == 0)
			{
				renderoffset.x = (int)(offset.x + mCol3.x * pxRule);
				renderoffset.y = (int)(offset.y + mCol3.y * pyRule);
				iSerPos += 6;
				iCurWidthOffset = mWidthCol3;
			}
			if(_wcsnicmp(pSubStr + 1, L"Col4>", 5) == 0)
			{
				renderoffset.x = (int)(offset.x + mCol4.x * pxRule);
				renderoffset.y = (int)(offset.y + mCol4.y * pyRule);
				iSerPos += 6;
				iCurWidthOffset = mWidthCol4;
			}
			if(_wcsnicmp(pSubStr + 1, L"Col5>", 5) == 0)
			{
				renderoffset.x = (int)(offset.x + mCol5.x * pxRule);
				renderoffset.y = (int)(offset.y + mCol5.y * pyRule);
				iSerPos += 6;
				iCurWidthOffset = mWidthCol5;
			}
			if(_wcsnicmp(pSubStr + 1, L"Col6>", 5) == 0)
			{
				renderoffset.x = (int)(offset.x + mCol6.x * pxRule);
				renderoffset.y = (int)(offset.y + mCol6.y * pyRule);
				iSerPos += 6;
				iCurWidthOffset = mWidthCol6;
			}
			if(_wcsnicmp(pSubStr + 1, L"Col7>", 5) == 0)
			{
				renderoffset.x = (int)(offset.x + mCol7.x * pxRule);
				renderoffset.y = (int)(offset.y + mCol7.y * pyRule);
				iSerPos += 6;
				iCurWidthOffset = mWidthCol7;
			}
			if(_wcsnicmp(pSubStr + 1, L"Col8>", 5) == 0)
			{
				renderoffset.x = (int)(offset.x + mCol8.x * pxRule);
				renderoffset.y = (int)(offset.y + mCol8.y * pyRule);
				iSerPos += 6;
				iCurWidthOffset = mWidthCol8;
			}
			if(_wcsnicmp(pSubStr + 1, L"end>", 4) == 0)
			{
				iSerPos += 5;
				int iStrWidth = m_Style->m_spFont->GetStrNWidth(RenderText, iStartPos);
				renderoffset.x += (iCurWidthOffset - iStrWidth) / 2;
				
				sm_UiRender->DrawTextN(m_Style->m_spFont, renderoffset, RenderText, iStartPos, NULL, renderColor);
				iStartPos = 0;
			}
			if(_wcsnicmp(pSubStr + 1, L"end_right>", 10) == 0)
			{
				iSerPos += 11;
				int iStrWidth = m_Style->m_spFont->GetStrNWidth(RenderText, iStartPos);
				renderoffset.x += (iCurWidthOffset - iStrWidth);

				
				sm_UiRender->DrawTextN(m_Style->m_spFont, renderoffset, RenderText, iStartPos, NULL, renderColor);
				iStartPos = 0;
			}
			if(_wcsnicmp(pSubStr + 1, L"end_left>", 9) == 0)
			{
				iSerPos += 10;
				//int iStrWidth = m_Style->m_spFont->GetStrNWidth(RenderText, iStartPos);
				//renderoffset.x += (iCurWidthOffset - iStrWidth) / 2;

				
				sm_UiRender->DrawTextN(m_Style->m_spFont, renderoffset, RenderText, iStartPos, NULL, renderColor);
				iStartPos = 0;
			}
			if(_wcsnicmp(pSubStr + 1, L"endnc>", 6) == 0)
			{
				iSerPos += 7;

				
				sm_UiRender->DrawTextN(m_Style->m_spFont, renderoffset, RenderText, iStartPos, NULL, renderColor);
				iStartPos = 0;
			}
			if(_wcsnicmp(pSubStr + 1, L"line1>", 6) == 0)
			{				
				// Line 1
				iSerPos += 7;
				renderoffset.x = (int)(offset.x + mLine1.x * pxRule);
				renderoffset.y = (int)(offset.y + mLine1.y * pyRule);
				iCurWidthOffset = mWidthLine1;
			}
			if(_wcsnicmp(pSubStr + 1, L"line1_1>", 8) == 0)
			{				
				// Line 1
				iSerPos += 9;
				renderoffset.x = (int)(offset.x + mLine1_1.x * pxRule);
				renderoffset.y = (int)(offset.y + mLine1_1.y * pyRule);
				iCurWidthOffset = mWidthLine1_1;
			}
			if(_wcsnicmp(pSubStr + 1, L"line1_2>", 8) == 0)
			{				
				// Line 1
				iSerPos += 9;
				renderoffset.x = (int)(offset.x + mLine1_2.x * pxRule);
				renderoffset.y = (int)(offset.y + mLine1_2.y * pyRule);
				iCurWidthOffset = mWidthLine1_2;
			}
			if(_wcsnicmp(pSubStr + 1, L"line1_3>", 8) == 0)
			{				
				// Line 1
				iSerPos += 9;
				renderoffset.x = (int)(offset.x + mLine1_3.x * pxRule);
				renderoffset.y = (int)(offset.y + mLine1_3.y * pyRule);
				iCurWidthOffset = mWidthLine1_3;
			}
			if(_wcsnicmp(pSubStr + 1, L"line2>", 6) == 0)
			{
				// Line 2
				iSerPos += 7;
				renderoffset.x = (int)(offset.x + mLine2.x * pxRule);
				renderoffset.y = (int)(offset.y + mLine2.y * pyRule);
				iCurWidthOffset = mWidthLine2;
			}
			if(_wcsnicmp(pSubStr + 1, L"line2_1>", 8) == 0)
			{
				// Line 2
				iSerPos += 9;
				renderoffset.x = (int)(offset.x + mLine2_1.x * pxRule);
				renderoffset.y = (int)(offset.y + mLine2_1.y * pyRule);
				iCurWidthOffset = mWidthLine2_1;
			}
			if(_wcsnicmp(pSubStr + 1, L"line2_2>", 8) == 0)
			{
				// Line 2
				iSerPos += 9;
				renderoffset.x = (int)(offset.x + mLine2_2.x * pxRule);
				renderoffset.y = (int)(offset.y + mLine2_2.y * pyRule);
				iCurWidthOffset = mWidthLine2_2;
			}
			if(_wcsnicmp(pSubStr + 1, L"line2_3>", 8) == 0)
			{
				// Line 2
				iSerPos += 9;
				renderoffset.x = (int)(offset.x + mLine2_3.x * pxRule);
				renderoffset.y = (int)(offset.y + mLine2_3.y * pyRule);
				iCurWidthOffset = mWidthLine2_3;
			}
			if(_wcsnicmp(pSubStr +1, L"YPimg>", 6) == 0)
			{
				URect uvRect;
				uvRect.left = 0;
				uvRect.top = 0;
				uvRect.right = 23;
				uvRect.bottom = 23;
				renderoffset.y = offset.y + (m_GridSize.y - 23) / 2;
				sm_UiRender->DrawImage(m_YPTexture, renderoffset, uvRect, LCOLOR_WHITE, 1);
				iSerPos += 7;
			}
			if(_wcsnicmp(pSubStr + 1, L"Goldimg>", 8) == 0)
			{
				URect uvRect;
				uvRect.left = 0;
				uvRect.top = 0;
				uvRect.right = 12;
				uvRect.bottom = 12;
				//renderoffset.y = offset.y + (m_GridSize.y - 12) / 2;
				renderoffset.x = offset.x + mGoldOff;
				sm_UiRender->DrawImage(m_GoldTexture, renderoffset, uvRect, LCOLOR_WHITE, 1);
				iSerPos += 9;
			}
			if(_wcsnicmp(pSubStr + 1, L"Silverimg>", 10) == 0)
			{
				URect uvRect;
				uvRect.left = 0;
				uvRect.top = 0;
				uvRect.right = 12;
				uvRect.bottom = 12;
				renderoffset.x = offset.x + mSilverOff;
				//renderoffset.y = offset.y + (m_GridSize.y - 12) / 2;
				sm_UiRender->DrawImage(m_SilverTexture, renderoffset, uvRect, LCOLOR_WHITE, 1);
				renderoffset.x += 12;
				iSerPos += 11;
			}
			if(_wcsnicmp(pSubStr + 1, L"Broneimg>", 9) == 0)
			{
				URect uvRect;
				uvRect.left = 0;
				uvRect.top = 0;
				uvRect.right = 12;
				uvRect.bottom = 12;
				//renderoffset.y = offset.y + (m_GridSize.y - 12) / 2;
				renderoffset.x = offset.x + mBroneOff;
				sm_UiRender->DrawImage(m_BroneTexture, renderoffset, uvRect, LCOLOR_WHITE, 1);
				iSerPos += 10;
			}
			if(_wcsnicmp(pSubStr + 1, L"YuanBaoimg>", 11) == 0)
			{
				URect uvRect;
				uvRect.left = 0;
				uvRect.top = 0;
				uvRect.right = 12;
				uvRect.bottom = 12;
				//renderoffset.y = offset.y + (m_GridSize.y - 12) / 2;
				renderoffset.x = offset.x + mYuanBaoOff;
				sm_UiRender->DrawImage(m_YuanBaoTexture, renderoffset, uvRect, LCOLOR_WHITE, 1);
				iSerPos += 12;
			}
			// 都不是，那显然错了。。。
			
			//
			if (isStartPos == iSerPos )
			{
				//RenderText[iStartPos] = text[iSerPos];
				iStartPos++;
				iSerPos++;
			}
		}
		else
		{
			RenderText[iStartPos] = text[iSerPos];
			iStartPos++;
			iSerPos++;
		}
	}

	//ECheckSkin eSkinID = (ECheckSkin)m_pListBoxCtx->Items[cell.y].Title.iCheckState;
	URect rect(UPoint(offset.x + m_GridSize.x - 15, offset.y + 2), UPoint(15, 15));
	if(m_spBtnSkin && bEnable && !bFinish)
	{
		sm_UiRender->DrawSkin(m_spBtnSkin, eSkinID, rect);
	}


	if(iRoot != -1 && iSub == -1)
	{
		URect rect1(UPoint(offset.x + 1, offset.y + 2), UPoint(16, 16));
		if(m_spTreeConSkin)
		{
			ECheckSkin SkinIndex = (ECheckSkin)m_pListBoxCtx->Items[iRoot].bShowSubText;
			if (m_pListBoxCtx->Items[iRoot].Title.bEnable)
			{
				SkinIndex = BTS_DISABLE;
			}
			sm_UiRender->DrawSkin(m_spTreeConSkin, SkinIndex, rect1);
		}
	}
	//sm_UiRender->DrawTextN(m_Style->m_spFont, renderoffset, RenderText, iStartPos, NULL, fontColor);
}
void USpecListBox::DrawGridItemBkg(bool bselect, bool benable, bool bMouseOver,  void* pUserDate, URect& drawRect, UColor* fontcolor)
{
	if (benable)
	{
		if (bselect)
		{
			//高亮区域
			sm_UiRender->DrawSkin(m_spSkin, 2, drawRect);
			*fontcolor = m_Style->m_FontColorHL;
		}else if (bMouseOver)
		{
			sm_UiRender->DrawSkin(m_spSkin, 1, drawRect);
			*fontcolor = m_Style->m_FontColorNA;
		}
		else
		{
			sm_UiRender->DrawSkin(m_spSkin, 0, drawRect);
			*fontcolor = m_Style->m_FontColor;
		}
	}else
	{
		if (bselect)
		{
			//高亮区域
			sm_UiRender->DrawSkin(m_spSkin, 2, drawRect);
			*fontcolor = m_Style->m_FontColorHL;
		}else if (bMouseOver)
		{
			sm_UiRender->DrawSkin(m_spSkin, 1, drawRect);
			*fontcolor = m_Style->m_FontColorNA;
		}
	}
}

void USpecListBox::SetBitmap(const char* szFileName)
{
	m_strBitmapFile.Set(szFileName);
	if (!IsCreated())
	{
		return;
	}

	if (!m_strBitmapFile.Empty())
	{
		m_spSkin = sm_System->LoadSkin(m_strBitmapFile.GetBuffer());
	}else
	{
		m_spSkin = NULL;
	}
}

void USpecListBox::SetCol1(int x, int y)
{
	mCol1.x = x;
	mCol1.y = y;
}

void USpecListBox::SetWidthCol1(int iWidth)
{
	mWidthCol1 = iWidth;
}

void USpecListBox::SetCol2(int x, int y)
{
	mCol2.x = x;
	mCol2.y = y;
}

void USpecListBox::SetWidthCol2(int iWidth)
{
	mWidthCol2 = iWidth;
}

void USpecListBox::SetCol3(int x, int y)
{
	mCol3.x = x;
	mCol3.y = y;
}

void USpecListBox::SetWidthCol3(int iWidth)
{
	mWidthCol3 = iWidth;
}

void USpecListBox::SetCol4(int x, int y)
{
	mCol4.x = x;
	mCol4.y = y;
}

void USpecListBox::SetWidthCol4(int iWidth)
{
	mWidthCol4 = iWidth;
}

void USpecListBox::SetCol5(int x, int y)
{
	mCol5.x = x;
	mCol5.y = y;
}

void USpecListBox::SetWidthCol5(int iWidth)
{
	mWidthCol5 = iWidth;
}

void USpecListBox::SetCol6(int x, int y)
{
	mCol6.x = x;
	mCol6.y = y;
}

void USpecListBox::SetWidthCol6(int iWidth)
{
	mWidthCol6 = iWidth;
}

void USpecListBox::SetCol7(int x, int y)
{
	mCol7.x = x;
	mCol7.y = y;
}

void USpecListBox::SetWidthCol7(int iWidth)
{
	mWidthCol7 = iWidth;
}

void USpecListBox::SetCol8(int x, int y)
{
	mCol8.x = x;
	mCol8.y = y;
}

void USpecListBox::SetWidthCol8(int iWidth)
{
	mWidthCol8 = iWidth;
}

void USpecListBox::SetLine1(int x, int y)
{
	mLine1.x = x;
	mLine1.y = y;
}

void USpecListBox::SetWidthLine1(int iWidth)
{
	mWidthLine1 = iWidth;
}

void USpecListBox::SetLine1_1(int x, int y)
{
	mLine1_1.x = x;
	mLine1_1.y = y;
}

void USpecListBox::SetLine1_2(int x, int y)
{
	mLine1_2.x = x;
	mLine1_2.y = y;
}

void USpecListBox::SetLine2(int x, int y)
{
	mLine2.x = x;
	mLine2.y = y;
}

void USpecListBox::SetLine2_1(int x, int y)
{
	mLine2_1.x = x;
	mLine2_1.y = y;
}

void USpecListBox::SetLine2_2(int x, int y)
{
	mLine2_2.x = x;
	mLine2_2.y = y;
}

void USpecListBox::SetWidthLine2(int iWidth)
{
	mWidthLine2 = iWidth;
}

void USpecListBox::SetWidthLine1_1(int iWidth)
{
	mWidthLine1_1 = iWidth;
}

void USpecListBox::SetWidthLine1_2(int iWidth)
{
	mWidthLine1_2 = iWidth;
}

void USpecListBox::SetWidthLine2_1(int iWidth)
{
	mWidthLine2_1 = iWidth;
}

void USpecListBox::SetWidthLine2_2(int iWidth)
{
	mWidthLine2_2 = iWidth;
}

void USpecListBox::SetLine1_3(int x, int y)
{
	mLine1_3.x = x;
	mLine1_3.y = y;
}

void USpecListBox::SetLine2_3(int x, int y)
{
	mLine2_3.x = x;
	mLine2_3.y = y;
}

void USpecListBox::SetWidthLine1_3(int iWidth)
{
	mWidthLine1_3 = iWidth;
}

void USpecListBox::SetWidthLine2_3(int iWidth)
{
	mWidthLine2_3 = iWidth;
}