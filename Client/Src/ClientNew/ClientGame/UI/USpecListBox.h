#ifndef SPECLISTBOX_H
#define SPECLISTBOX_H

#include "UInc.h"
#include "QuestGrid.h"

class USpecListBox : public CQuestGrid
{
	UDEC_CLASS(USpecListBox);

public:
	enum 
	{
		ITEM_TEXT_MAX = 1024,	// ITEM 支持的最大字符数目
	};

	USpecListBox();
	virtual ~USpecListBox();

	UINT GetItemCount();
	UINT GetSubItemCount(unsigned int uiRootIndex);

	void Clear();
	void AddItem(const char *text, void* pUserData = NULL);
	void AddItem(const char *text,  int bFinish,void* pUserData = NULL);
	void AddItem(const WCHAR* text,  int bFinish,void* pUserData = NULL);
	void AddSubItem(unsigned int uiRootIndex, const char* text, int bFinish, void* pUserData = NULL);
	void AddSubItem(unsigned int uiRootIndex, const WCHAR* text, int bFinish, void* pUserData = NULL);
	void InsertItem(int index, const char *text, void* pUserData = NULL);
	void InsertItem(int index, const char *text,  int bFinish , void* pUserData = NULL);
	void InsertSubItem(unsigned int uiRootIndex, unsigned int uiSubIndex, const char* text, int bFinish , void* pUserData = NULL);
	void DeleteItem(int index);
	void DeleteItemById(unsigned int uiId);
	void DeleteSubItem(unsigned int uiRootIndex, unsigned int uiSubIndex);
	void DeleteItemByText(const char* text);
	

	void GetRootSub(unsigned int uiId, int& iRoot, int& iSub); //通过ID查找根的位置和子位置
	void GetShowIndex(unsigned int idx, int& iRoot, int& iSub);
	UINT GetShowItemCount();
	UINT GetShowItemCount(unsigned int idx);

	int GetSelId(); //获得当前选择的UI32 的ID/、

	int GetCurSel() const;
	void SetCurSel(int index);
	void SetCurSelByID(UINT uiid);
	int GetCheckStateById(unsigned int uiId);
	bool SetCheckState(UINT uiid,bool bCheck);

	void SetItemEnable(int index, bool bEnable);
	bool IsItemEnable(int index);

	void SetItemText(int index, const char *text, int bFinish );
	void SetSubItemText(unsigned int uiRootIndex, unsigned int uiSubIndex, const char* text, int bFinish);

	INT FindItemByData(void* pUserData);
	INT FindItem(const char *text);
	INT FindItem(const WCHAR *text);
	void* GetItemData(int index);

	void SetBitmap(const char* szBitmapFile);

	void SetCol1(int x, int y);
	void SetWidthCol1(int iWidth);
	void SetCol2(int x, int y);
	void SetWidthCol2(int iWidth);
	void SetCol3(int x, int y);
	void SetWidthCol3(int iWidth);
	void SetCol4(int x, int y);
	void SetWidthCol4(int iWidth);
	void SetCol5(int x, int y);
	void SetWidthCol5(int iWidth);
	void SetCol6(int x, int y);
	void SetWidthCol6(int iWidth);
	void SetCol7(int x, int y);
	void SetWidthCol7(int iWidth);
	void SetCol8(int x, int y);
	void SetWidthCol8(int iWidth);
	void SetLine1(int x, int y);
	void SetWidthLine1(int iWidth);
	void SetLine1_1(int x, int y);
	void SetLine1_2(int x, int y);
	void SetLine1_3(int x, int y);
	void SetWidthLine1_1(int iWidth);
	void SetWidthLine1_2(int iWidth);
	void SetWidthLine1_3(int iWidth);
	void SetLine2(int x, int y);
	void SetWidthLine2(int iWidth);
	void SetLine2_1(int x, int y);
	void SetLine2_2(int x, int y);
	void SetLine2_3(int x, int y);
	void SetWidthLine2_1(int iWidth);
	void SetWidthLine2_2(int iWidth);
	void SetWidthLine2_3(int iWidth);

	void SetGoldOff(int iOff) { mGoldOff = iOff; }
	void SetSilverOff(int iOff) { mSilverOff = iOff; }
	void SetBroneOff(int iOff) { mBroneOff = iOff; }
	void SetYuanBaoOff(int iOff) { mYuanBaoOff = iOff; }

	virtual void GetScrollLineSizes(UINT *rowHeight, UINT *columnWidth);
protected:
	void SetItemCount(int nNumItems);

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver);
	virtual void DrawGridItemBkg(bool bselect, bool benable, bool bMouseOver, void* pUserDate, URect& drawRect, UColor* fontcolor);

	virtual void OnSelChange(int row, int col);
	virtual void OnRemove();
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void SelRightMouseDown(int row ,int col);
	virtual void OnTreeButtonClick(int col);
protected:
	bool CheckIsEnable(int col);
	bool OnlyCheckParentIsEnable(int col); // 
	bool CheckCanClickBtn(int col);

protected:
	virtual void Sort(UINT column, bool increasing = TRUE);
	virtual void SortNumerical(UINT column, bool increasing = TRUE);
public:
	UINT GetSelectedRow();
	//UINT GetRowWidth(Entry *row);

	int GetSelectedText(char* pText, int nMaxLen);


protected:
	BOOL m_Enumerate;  //列举
	BOOL m_ResizeCell;
	BOOL m_FitParentWidth;
	BOOL m_ClipColumnText;
	struct SpecListBoxContext* m_pListBoxCtx;

	UString		m_strBitmapFile;
	USkinPtr	m_spSkin;

	UTexturePtr  m_YPTexture;

	UTexturePtr m_GoldTexture;
	UTexturePtr m_SilverTexture;
	UTexturePtr m_BroneTexture;
	UTexturePtr m_YuanBaoTexture;
	UTexturePtr m_TreeConTexture;

	UPoint		mCol1;
	int			mWidthCol1;
	UPoint		mCol2;
	int			mWidthCol2;
	UPoint		mCol3;
	int			mWidthCol3;
	UPoint		mCol4;
	int			mWidthCol4;
	UPoint		mCol5;
	int			mWidthCol5;
	UPoint		mCol6;
	int			mWidthCol6;
	UPoint		mCol7;
	int			mWidthCol7;
	UPoint		mCol8;
	int			mWidthCol8;
	UPoint		mLine1;
	int			mWidthLine1;
	UPoint		mLine1_1;
	int			mWidthLine1_1;
	UPoint		mLine1_2;
	int			mWidthLine1_2;
	UPoint		mLine1_3;
	int			mWidthLine1_3;
	UPoint		mLine2;
	int			mWidthLine2;
	UPoint		mLine2_1;
	int			mWidthLine2_1;
	UPoint		mLine2_2;
	int			mWidthLine2_2;
	UPoint		mLine2_3;
	int			mWidthLine2_3;

	int			mGoldOff;
	int			mSilverOff;
	int			mBroneOff;
	int			mYuanBaoOff;

	UPoint      m_OldSize ;
};

#endif