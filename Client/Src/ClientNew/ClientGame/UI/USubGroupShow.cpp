#include "stdafx.h"
#include "USubGroupShow.h"
#include "UFun.h"
#include "SocialitySystem.h"

//////////////////////////////////////////////////////////////////////////
// SUB GROUP DRAG BTN
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(USubGroupShow::UDragBtn, UControl)
void USubGroupShow::UDragBtn::StaticInit()
{

}
USubGroupShow::UDragBtn::UDragBtn()
{
	m_ptLeftBtnDown.Set(0,0);
}
USubGroupShow::UDragBtn::~UDragBtn()
{

}
BOOL USubGroupShow::UDragBtn::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	return TRUE;
}
void USubGroupShow::UDragBtn::OnDestroy()
{
	UControl::OnDestroy();
}
void USubGroupShow::UDragBtn::OnMouseUp(const UPoint& position, UINT flags)
{
	UControl::OnMouseUp(position,flags);
	ReleaseCapture();
}
void USubGroupShow::UDragBtn::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	UControl::OnMouseDown(point,nRepCnt,uFlags);
	m_ptLeftBtnDown = point;
	CaptureControl();

}
void USubGroupShow::UDragBtn::OnMouseEnter(const UPoint& position, UINT flags)
{
	UControl::OnMouseEnter(position,flags);
	USubGroupShow* pkParent = UDynamicCast(USubGroupShow, GetParent());
	if (pkParent)
	{
		pkParent->SetMouseHover(TRUE);
	}
}
void USubGroupShow::UDragBtn::OnMouseLeave(const UPoint& position, UINT flags)
{
	UControl::OnMouseLeave(position,flags);
	USubGroupShow* pkParent = UDynamicCast(USubGroupShow, GetParent());
	if (pkParent)
	{
		pkParent->SetMouseHover(FALSE);
	}
}
void USubGroupShow::UDragBtn::OnMouseDragged(const UPoint& position, UINT flags)
{

	USubGroupShow* pkParent = UDynamicCast(USubGroupShow, GetParent());
	if (pkParent)
	{
		UPoint delta = position - m_ptLeftBtnDown;
		m_ptLeftBtnDown = position;
		UPoint NewPos = pkParent->GetWindowPos();
		NewPos += delta;
		pkParent->SetPosition(NewPos);
	}
	UControl::OnMouseDragged(position,flags);
}
void USubGroupShow::UDragBtn::OnRender(const UPoint& offset,const URect &updateRect)
{
	//sm_UiRender->DrawRectFill(updateRect, m_Style->mFillColorHL);
	if (m_Showtext.GetLength())
	{
		UColor ucol ;
		if (m_MouseHover)
		{
			ucol = m_Style->m_FontColorHL;
		}else
		{
			ucol = m_Style->m_FontColor;
		}
		UDrawText(sm_UiRender, m_Style->m_spFont, offset, updateRect.GetSize(), ucol, m_Showtext.GetBuffer(), UT_CENTER);
	}

}
//////////////////////////////////////////////////////////////////////////
// sub group member show 
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(USubGroupShow::USubGroupMemberCtr,UControl)
void USubGroupShow::USubGroupMemberCtr::StaticInit()
{

}
USubGroupShow::USubGroupMemberCtr::USubGroupMemberCtr()
{
	m_TeamDate = NULL;
	m_bSel = FALSE;
	m_MouseDown = FALSE;
	m_HPBar = NULL;
	m_MPBar = NULL;
	m_Name = NULL;
	m_bReadyFlag = FALSE ;
	m_StateSkin = NULL ;
}
USubGroupShow::USubGroupMemberCtr::~USubGroupMemberCtr()
{

}
BOOL USubGroupShow::USubGroupMemberCtr::UpdateUI()
{
	if (m_TeamDate && m_TeamDate->Guid)
	{
		if (!m_TeamDate->islogin)
		{
			m_HPBar->SetProgressPos(0.0f, 1.0f);
			m_MPBar->SetProgressPos(0.0f, 1.0f);
		}else
		{
			m_HPBar->SetProgressPos(m_TeamDate->hp, m_TeamDate->max_hp);
			m_MPBar->SetProgressPos(m_TeamDate->mp, m_TeamDate->max_mp);
		}
		m_HPBar->SetTextVisible(FALSE);
		m_MPBar->SetTextVisible(FALSE);
		m_Name->SetGuid(m_TeamDate->Guid);
		m_Name->SetText(m_TeamDate->Name.c_str());
		UColor color =GetClassColor(m_TeamDate->Class); 
		if (!m_TeamDate->islogin)
		{
			color = 0xFF808080;
		}
		m_Name->SetTextColor(color);
		UBitmap* pkBit = UDynamicCast(UBitmap, GetChildByID(2));
		if (pkBit)
		{
			switch(m_TeamDate->Class)
			{
			case CLASS_WARRIOR:
				pkBit->SetBitMapByStr("DATA\\UI\\UIPlayerShow\\wuxiu.png");
				break;
			case CLASS_BOW:
				pkBit->SetBitMapByStr("DATA\\UI\\UIPlayerShow\\yujian.png");
				break;
			case CLASS_PRIEST:
				pkBit->SetBitMapByStr("DATA\\UI\\UIPlayerShow\\xiandao.png");
				break;
			case CLASS_MAGE:
				pkBit->SetBitMapByStr("DATA\\UI\\UIPlayerShow\\zhenwu.png");
				break;
			}
		}
		SetVisible(TRUE);
		return TRUE;
	}else
	{
		SetVisible(FALSE);
		return FALSE;
	}
}
BOOL USubGroupShow::USubGroupMemberCtr::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE ;
	}

	if (m_StateSkin == NULL)
	{
		m_StateSkin = sm_System->LoadSkin("SocialitySystem\\TeamMemberState.skin");
		if (!m_StateSkin)
		{
			return FALSE;
		}
	}
	if (m_HPBar == NULL)
	{
		m_HPBar = UDynamicCast(UProgressBar, GetChildByID(0));
		if (!m_HPBar)
		{
			return FALSE ;
		}
	}

	if (m_MPBar == NULL)
	{
		m_MPBar = UDynamicCast(UProgressBar, GetChildByID(1));
		if (!m_MPBar)
		{
			return FALSE ;
		}
	}

	if (m_Name == NULL)
	{
		m_Name = UDynamicCast(USubGroupShow::USubGroupName, GetChildByID(3));
		if (!m_Name)
		{
			return FALSE ;
		}
	}


	return TRUE ;
}
void USubGroupShow::USubGroupMemberCtr::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	m_MouseDown = TRUE;
}
void USubGroupShow::USubGroupMemberCtr::OnRender(const UPoint& offset,const URect &updateRect)
{
	RenderChildWindow(offset, updateRect);
	if (SocialitySys && SocialitySys->GetTeamSysPtr()->GetMemberReadyFlag() && m_StateSkin )
	{
		if (m_TeamDate && m_TeamDate->isTeamLeader)
		{
			sm_UiRender->DrawSkin(m_StateSkin, 1, offset + UPoint(4,16));
		}else
		{
			sm_UiRender->DrawSkin(m_StateSkin, m_TeamDate->state, offset + UPoint(4,16));
		}
	}
}
void USubGroupShow::USubGroupMemberCtr::OnRightMouseUp(const UPoint& position, UINT flags)
{
	UControl::OnRightMouseUp(position, flags);
	if (flags & LKM_SHIFT)
	{
		if (SocialitySys)
		{
			SocialitySys->GetTeamSysPtr()->SortAllTeamDate();
		}	
	}
}
void USubGroupShow::USubGroupMemberCtr::OnMouseUp(const UPoint& position, UINT flags)
{
	if (m_MouseDown)
	{
		if (m_TeamDate && m_TeamDate->Guid)
		{
			SocialitySys->GetTeamSysPtr()->SetCurSel(m_TeamDate);
		}
		m_MouseDown = FALSE ;
	}
}
void USubGroupShow::USubGroupMemberCtr::OnDestroy()
{
	UControl::OnDestroy();
}
//////////////////////////////////////////////////////////////////////////
// Name ctrl
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(USubGroupShow::USubGroupName, UStaticText)
void USubGroupShow::USubGroupName::StaticInit()
{

}
USubGroupShow::USubGroupName::USubGroupName()
{
	m_MaskSkin = NULL;
	m_guid = 0;
}
USubGroupShow::USubGroupName::~USubGroupName()
{

}
BOOL USubGroupShow::USubGroupName::OnCreate()
{
	if (!UStaticText::OnCreate())
	{
		return FALSE ;
	}
	if (m_MaskSkin == NULL)
	{
		m_MaskSkin =  sm_System->LoadSkin("UIPlayerShow\\CharMask.skin");
		if (!m_MaskSkin)
		{
			return FALSE ;
		}
	}

	return TRUE ;
}
void USubGroupShow::USubGroupName::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_Font && sm_UiRender && m_WStrBuffer.GetLength())
	{
		UINT index = 8;
		if (m_guid && SocialitySys && SocialitySys->GetTeamSysPtr()->GetTargetIcon( &index, m_guid))
		{
			UPoint MaskSize = UPoint(12,12);
			int textWidth = m_Font->GetStrWidth(m_WStrBuffer);
			UINT MaskWid = updateRect.GetWidth() - textWidth;
			if ( MaskWid <= MaskSize.y )
			{
				MaskSize.y = MaskWid  ;
				MaskSize.x = MaskWid ;
			}
			URect NameRec = updateRect;
			NameRec.SetWidth(updateRect.GetWidth() - MaskSize.y / 2)  ; 
			URect MaskRec ;

			MaskRec.SetPosition(offset.x +( NameRec.GetWidth() + textWidth) / 2,  offset.y);
			MaskRec.SetSize(MaskSize);
			sm_UiRender->DrawSkin(m_MaskSkin, index, MaskRec);

			USetTextEdge(TRUE);
			UDrawText(sm_UiRender, m_Font, offset , NameRec.GetSize() , m_FontColor, m_WStrBuffer, UT_CENTER );
			USetTextEdge(FALSE);


		}else
		{
			UStaticText::OnRender(offset, updateRect);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
//	SubGroup Show
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(USubGroupShow,UControl);
void USubGroupShow::StaticInit()
{
	UREG_PROPERTY("bkgskin", UPT_STRING, UFIELD_OFFSET(USubGroupShow, m_strFileName));
}
USubGroupShow::USubGroupShow()
{
	m_SubMember[0] = NULL;
	m_SubMember[1] = NULL;
	m_SubMember[2] = NULL;
	m_SubMember[3] = NULL;
	m_SubMember[4] = NULL;

	m_DragBtn = NULL;
	m_BbkSkin = NULL;
	m_pkHoverDrag = FALSE ;
	m_bLockBgk = TRUE;
}
USubGroupShow::~USubGroupShow()
{

}
BOOL USubGroupShow::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (m_strFileName.GetBuffer())
	{
		m_BbkSkin = sm_System->LoadSkin(m_strFileName.GetBuffer());
		if (m_BbkSkin == NULL)
		{
			return FALSE;
		}
	}

	if (m_DragBtn == NULL)
	{
		m_DragBtn = UDynamicCast(USubGroupShow::UDragBtn, GetChildByID(0));
		if (!m_DragBtn)
		{
			return FALSE ;
		}
	}
	for (int i = 0; i < 5 ; i++)
	{
		if (m_SubMember[i] == NULL)
		{
			UControl* pkCtrl =sm_System->CreateDialogFromFile("SocialitySystem\\SubGroupMemberCtr.udg");
			m_SubMember[i] = UDynamicCast(USubGroupShow::USubGroupMemberCtr, pkCtrl);
			if (m_SubMember[i])
			{
				AddChild(m_SubMember[i]);
				m_SubMember[i]->SetId(0xff + i);
				m_SubMember[i]->SetVisible(TRUE);
			}else
			{
				return FALSE ;
			}
		}
	}


	return TRUE ;
}
void USubGroupShow::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_BbkSkin && (m_pkHoverDrag || m_bLockBgk))
	{
		UDrawResizeImage(RESIZE_HEIGHT,sm_UiRender,m_BbkSkin,offset,updateRect.GetSize());
	}
	RenderChildWindow(offset, updateRect);
}
void USubGroupShow::OnDestroy()
{
	UControl::OnDestroy();
}
BOOL USubGroupShow::SetCurSel(TeamDate* pkdate)
{
	for (int i = 0; i < 5; i++)
	{
		if (m_SubMember[i]->GetMemberDate() && pkdate == m_SubMember[i]->GetMemberDate())
		{
			m_SubMember[i]->SetSel(TRUE);
		}else
		{
			m_SubMember[i]->SetSel(FALSE);
		}
	}

	return TRUE;
}
void USubGroupShow::ResetUIPos()
{
	UINT top = m_BbkSkin->GetSkinItemRect(0)->GetHeight();
	UINT bo = m_BbkSkin->GetSkinItemRect(2)->GetHeight();
	top += 4;
	for (int i = 0; i < 5; i++)
	{
		if (m_SubMember[i]->IsVisible())
		{
			m_SubMember[i]->SetTop(top);
			top += m_SubMember[i]->GetHeight();
		}
	}
	m_Size.y = top + bo + 4;
}
void USubGroupShow::UpdateUI()
{
	BOOL bNeedUpdate  = FALSE ;
	for (int i = 0; i < 5; i++)
	{
		m_SubMember[i]->UpdateUI();
	}
	ResetUIPos();
}
void USubGroupShow::UpdateUI(TeamDate* pkdate)
{
	BOOL bNeedUpdate  = FALSE ;
	for (int i = 0; i < 5; i++)
	{
		TeamDate* date = m_SubMember[i]->GetMemberDate();
		if (date && date == pkdate)
		{
			bNeedUpdate = m_SubMember[i]->UpdateUI();
			break ;
		}
	}

	if (!bNeedUpdate)
	{
		ResetUIPos();
	}
}
BOOL USubGroupShow::AddSubGroup(TeamDate** data, ui32 id)    //添加新的团队成员
{
	for (int i = 0; i < 5 ; i++)
	{
		m_SubMember[i]->SetMemberDate(data[i]);
	}
	if (m_DragBtn)
	{
		char buf[128];
		std::string str = _TRAN( N_NOTICE_C59, _I2A(id+1).c_str());
		sprintf(buf, str.c_str()/*"小队%d", (id+1)*/);
		m_DragBtn->Settext(buf);
	}
	ResetUIPos();
	return TRUE ;
}
void USubGroupShow::TeamDestroy() //指针数据设置为空
{
	for (int i = 0; i < 5 ; i++)
	{
		m_SubMember[i]->SetMemberDate(NULL);
	}
}
