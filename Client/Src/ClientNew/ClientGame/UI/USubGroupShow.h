#pragma once
#include "UTeamDate.h"
#include "UInc.h"
#include "UIPlayerHead.h"

//////////////////////////////////////////////////////////////////////////
// 团队追踪界面
//////////////////////////////////////////////////////////////////////////
//单个小队追踪UI 
class USubGroupShow : public UControl
{
	UDEC_CLASS(USubGroupShow);
public:
	USubGroupShow();
	~USubGroupShow();

	BOOL SetCurSel(TeamDate* pkdate);
	BOOL AddSubGroup(TeamDate** data, ui32 id);     //添加新的团队成员
	void UpdateUI(TeamDate* pkdate);
	void UpdateUI();
	void ResetUIPos();
	void SetMouseHover(BOOL bHover){m_pkHoverDrag = bHover;}
	void SetLockBgk(BOOL bLock){m_bLockBgk = bLock;}
	void TeamDestroy(); //指针数据设置为空
	class USubGroupName : public UStaticText
	{
		UDEC_CLASS(USubGroupName)
	public:
		USubGroupName();
		~USubGroupName();
		void SetGuid(ui64 guid){m_guid = guid;}
	protected:
		virtual BOOL OnCreate();
		virtual void OnRender(const UPoint& offset,const URect &updateRect);
	protected:
		USkinPtr m_MaskSkin ;
		ui64 m_guid;
		
	private:
	};
	class UDragBtn : public UControl
	{
		UDEC_CLASS(UDragBtn)
	public:
		UDragBtn();
		~UDragBtn();
		void Settext(char * buf){m_Showtext.Set(buf);}
	protected:
		BOOL virtual OnCreate();
		void virtual OnDestroy();
		virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
		virtual void OnMouseUp(const UPoint& position, UINT flags);
		virtual void OnMouseEnter(const UPoint& position, UINT flags);
		virtual void OnMouseLeave(const UPoint& position, UINT flags);
		//virtual void OnRightMouseDragged(const UPoint& position, UINT flags);
		virtual void OnMouseDragged(const UPoint& position, UINT flags);
		virtual void OnRender(const UPoint& offset,const URect &updateRect);
	private:
		UString m_Showtext;
		UPoint m_ptLeftBtnDown;
	};
	class USubGroupMemberCtr : public UControl
	{
		UDEC_CLASS(USubGroupMemberCtr);
	public:
		USubGroupMemberCtr();
		~USubGroupMemberCtr();
		TeamDate* GetMemberDate(){return m_TeamDate;}
		BOOL SetMemberDate(TeamDate* pkDate){m_TeamDate = pkDate; return UpdateUI();}
		void SetSel(BOOL sel){m_bSel = sel;}
		BOOL UpdateUI();
	protected:
		virtual BOOL OnCreate();
		virtual void OnDestroy();
		virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
		virtual void OnMouseUp(const UPoint& position, UINT flags);
		virtual void OnRightMouseUp(const UPoint& position, UINT flags);
		virtual void OnRender(const UPoint& offset,const URect &updateRect);
		virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer /* = -1 */)
		{
			return this ;
		}
	private:
		TeamDate* m_TeamDate ;
		UProgressBar* m_HPBar;
		UProgressBar* m_MPBar;
		USubGroupShow::USubGroupName* m_Name;
		BOOL m_MouseDown;
		BOOL m_bSel ;
		BOOL m_bReadyFlag ;
		USkinPtr m_StateSkin;

	};

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
private:
	USubGroupShow::USubGroupMemberCtr* m_SubMember[5];
	USubGroupShow::UDragBtn* m_DragBtn;
	USkinPtr m_BbkSkin;
	UString m_strFileName;
	BOOL m_pkHoverDrag;
	BOOL m_bLockBgk ;
};