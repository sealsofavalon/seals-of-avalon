#include "stdafx.h"
#include "USuperSkill.h"
#include "ClientState.h"
#include "PlayerInputMgr.h"
#include "UITipSystem.h"
#include "UIGamePlay.h"
#include "UIIndex.h"
#include "LocalPlayer.h"
#include "ObjectManager.h"

UIMP_CLASS(USuperSkill, UControl);
UBEGIN_MESSAGE_MAP(USuperSkill,UControl)
UEND_MESSAGE_MAP()
void USuperSkill::StaticInit()
{

}
USuperSkill::USuperSkill()
{
	m_SuperSkillExp = NULL;
	
	m_CanUse = FALSE;
	

	m_pTest = NULL;

}
USuperSkill::~USuperSkill()
{
	
}

void USuperSkill::SetSuperSkillExp(UINT EXP)
{
	if (m_SuperSkillExp)
	{
		m_CanUse = m_SuperSkillExp->SetSuperSkillExp(EXP);
		m_pTest->SetVisible(m_CanUse);
	}
}
void USuperSkill::SetSuperSkillMaxExp(UINT exp)
{
	if (m_SuperSkillExp)
	{
		m_SuperSkillExp->SetSuperSkillMaxExp(exp);
	}
}
BOOL USuperSkill::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (m_SuperSkillExp == NULL)
	{
		m_SuperSkillExp = UDynamicCast(USuperExp,GetChildByID(1));
	}
		

	if (m_pTest == NULL)
	{
		m_pTest = UDynamicCast(UNiAVControlEff,GetChildByID(0));
	}

	if (m_SuperSkillExp == NULL || m_pTest == NULL)
	{
		return FALSE;
	}

	m_CanUse = FALSE;
	m_pTest->SetVisible(m_CanUse);

	return TRUE;
}
void USuperSkill::OnDestroy()
{
	UControl::OnDestroy();
}
void USuperSkill::OnRender(const UPoint& offset,const URect &updateRect)
{

	UControl::OnRender(offset,updateRect);
	
}
void USuperSkill::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);
}

void USuperSkill::ResizeControl(const UPoint &NewPos, const UPoint &newExtent)
{
	UPoint sTNewPos = NewPos;
	UControl* pkControl = UInGame::GetFrame(FRAME_IG_ACTIONSKILLBAREX1);
	if (pkControl)
	{
		sTNewPos.x = pkControl->GetControlRect().right - GetWidth();
		
	}
	UControl::ResizeControl(sTNewPos,newExtent);
}


//
UIMP_CLASS(USuperExp, UControl);
void USuperExp::StaticInit()
{
	UREG_PROPERTY("BgkTexture", UPT_STRING, UFIELD_OFFSET(USuperExp, m_BgkExpTexturePath));
	UREG_PROPERTY("ExpTexture", UPT_STRING, UFIELD_OFFSET(USuperExp, m_ExpTexturePath));
}
USuperExp::USuperExp()
{
	m_MaxSuperExp = XP_TRIGGER_BAR_MAX;
	m_CurSuperExp = 0;
	m_ExpTexture = NULL;
	m_BgkExpTexture = NULL;
	m_isExpFull = FALSE;
}
USuperExp::~USuperExp()
{
	m_MaxSuperExp = XP_TRIGGER_BAR_MAX;
	m_CurSuperExp = 0;
	m_ExpTexture = NULL;
	m_BgkExpTexture = NULL;
	m_isExpFull = FALSE;
}
BOOL USuperExp::SetSuperSkillExp(UINT EXP)
{
	m_CurSuperExp = EXP ;
	if (m_CurSuperExp >= m_MaxSuperExp && m_MaxSuperExp > 0)
	{
		m_isExpFull = TRUE;
	}else
	{
		m_isExpFull = FALSE;
	}

	return m_isExpFull;
}
void USuperExp::SetSuperSkillMaxExp(UINT exp)
{
	m_MaxSuperExp = exp;
}
BOOL USuperExp::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (m_BgkExpTexture == NULL && m_BgkExpTexturePath.GetBuffer())
	{
		m_BgkExpTexture = sm_UiRender->LoadTexture(m_BgkExpTexturePath.GetBuffer());
	}

	if (m_ExpTexture == NULL && m_ExpTexturePath.GetBuffer())
	{
		m_ExpTexture = sm_UiRender->LoadTexture(m_ExpTexturePath.GetBuffer());
	}

	if (m_BgkExpTexture == NULL || m_ExpTexture == NULL)
	{
		return FALSE;
	}

	m_MaxSuperExp = XP_TRIGGER_BAR_MAX;
	m_CurSuperExp = 0;

	return TRUE;
}

 void USuperExp::OnMouseEnter(const UPoint& position, UINT flags)
 {
	 UControl::OnMouseEnter(position,flags);
	 UPoint t_pt= GetParent()->WindowToScreen(GetWindowPos());
	
	 t_pt.x += GetWidth();
	 t_pt.y += GetHeight();
				
	TipSystem->ShowTip(TIP_TYPE_SPELL,XP_TRIGGER_SPELL,t_pt);
 }
 void USuperExp::OnMouseLeave(const UPoint& position, UINT flags)
 {
	 UControl::OnMouseLeave(position,flags);
	 TipSystem->HideTip();
 }
void USuperExp::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	if (m_isExpFull)
	{
		//
		CPlayerInputMgr * InputMsg = SYState()->LocalPlayerInput;
		if (InputMsg)
		{
			CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
			if (pkLocal && pkLocal->IsHook())
			{
				return ;
			}
			InputMsg->BeginSkillAttack(XP_TRIGGER_SPELL);
			ULOG( _TRAN("ʹ���սἼ") );
		}
	}
	
	UControl::OnMouseDown(point,nRepCnt,uFlags);
}
void USuperExp::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_BgkExpTexture == NULL || m_ExpTexture == NULL  || m_MaxSuperExp == 0)
	{
		return UControl::OnRender(offset, updateRect);
	}

	UINT Width  = m_ExpTexture->GetWidth();
	UINT Height = m_ExpTexture->GetHeight();
	UINT RecHeight = updateRect.GetHeight();

	INT NeedExp = m_MaxSuperExp - m_CurSuperExp ;
	if (NeedExp <= 0)
	{
		NeedExp = 0;
	}

	UINT ImageRecTop = NeedExp* Height/ m_MaxSuperExp;
	UINT expRecTop = NeedExp * RecHeight / m_MaxSuperExp;

	URect ImageRec ;
	ImageRec.Set(0,ImageRecTop,Width,Height - ImageRecTop);

	URect expRec; 
	expRec.Set(updateRect.left, updateRect.top + expRecTop, updateRect.GetWidth(), RecHeight - expRecTop);
	
	sm_UiRender->DrawImage(m_BgkExpTexture,updateRect);
	sm_UiRender->DrawImage(m_ExpTexture,expRec,ImageRec);

}
