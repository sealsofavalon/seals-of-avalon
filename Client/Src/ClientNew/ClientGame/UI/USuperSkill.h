#ifndef  __USUPERSKILL_H__
#define  __USUPERSKILL_H__
#include "UNiAVControl.h"


class USuperExp : public UControl
{
	UDEC_CLASS(USuperExp);
public:
	USuperExp();
	~USuperExp();

	BOOL SetSuperSkillExp(UINT EXP);
	void SetSuperSkillMaxExp(UINT exp);
protected:
	BOOL virtual OnCreate();
	void virtual OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
private:
	UINT m_MaxSuperExp;
	UINT m_CurSuperExp;
	BOOL m_isExpFull;
	UTexturePtr m_BgkExpTexture;
	UTexturePtr m_ExpTexture;
	UString   m_BgkExpTexturePath;
	UString   m_ExpTexturePath;

};

class USuperSkill :public UControl
{
	UDEC_CLASS(USuperSkill);
	UDEC_MESSAGEMAP();
public:
	USuperSkill();
	~USuperSkill();
	void SetSuperSkillExp(UINT EXP);
	void SetSuperSkillMaxExp(UINT exp);
	void UseSuperSkill();
protected:
	BOOL virtual OnCreate();
	void virtual OnDestroy();
	void virtual OnRender(const UPoint& offset,const URect &updateRect);
	void virtual OnTimer(float fDeltaTime);
	virtual void ResizeControl(const UPoint &NewPos, const UPoint &newExtent);
	virtual BOOL HitTest(const UPoint& parentCoordPoint)
	{
		UPoint newCoordPoint = parentCoordPoint - m_Position ;

		UPoint pos = m_SuperSkillExp->GetWindowPos();
		UPoint pSize = m_SuperSkillExp->GetWindowSize();

		INT xt = newCoordPoint.x - pos.x;
		INT yt = newCoordPoint.y - pos.y;
		return xt >= 0 && yt >= 0 && xt < pSize.x && yt < pSize.y;
	}
private:
	USuperExp* m_SuperSkillExp ;  //终极技能进度
	BOOL m_CanUse;
	UNiAVControlEff* m_pTest ;
	
};

#endif