#include "StdAfx.h"
#include "USystemSet.h"
#include "UIGamePlay.h"
#include "SystemSetup.h"
#include "KeySetControl.h"
#include "ClientState.h"
#include "ClientApp.h"
#include "Effect/EffectManager.h"
#include "Map/WaterShader.h"
UIMP_CLASS(USetSlider, UControl)
UBEGIN_MESSAGE_MAP(USetSlider, UControl)
UON_SLIDER_CHANGE(1, &USetSlider::OnSet)
UEND_MESSAGE_MAP()
void USetSlider::StaticInit()
{
	UREG_PROPERTY("SetItem",UPT_STRING,UFIELD_OFFSET(USetSlider,mSetName));
}
BOOL USetSlider::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	UStaticText* ptext = UDynamicCast(UStaticText, GetChildByID(0));
	if (ptext && mSetName.GetBuffer())
	{
		std::string tempName;
		if(SystemSetup->GetTextFormOpCode(mSetName.GetBuffer(), tempName))
		{
			SystemSetup->RegiestCtrl(mSetName.GetBuffer(), this);
			ptext->SetText(tempName.c_str());
			ptext->SetTextAlign(UT_LEFT);
			SetInField();
		}
	}
	return TRUE;
}
void USetSlider::OnDestroy()
{
	UControl::OnDestroy();
}
void USetSlider::OnSet()
{
	USlider* pSlider = UDynamicCast(USlider, GetChildByID(1));
	if (!pSlider)
		return;
	float fValue = pSlider->GetFValue();
	SystemSetup->SetField(mSetName.GetBuffer(), fValue);
	UStaticText* ptext = UDynamicCast(UStaticText, GetChildByID(0));
	char buf[256];
	std::string tempName;
	if(SystemSetup->GetTextFormOpCode(mSetName.GetBuffer(), tempName))
	{
		sprintf(buf, "%s(%d%%)", tempName.c_str(), int(fValue * 100));
		ptext->SetText(buf);
	}
};
void USetSlider::SetBarPosition(float fvalue)
{
	USlider* pSlider = UDynamicCast(USlider, GetChildByID(1));
	if (pSlider)
	{
		pSlider->SetValue(int(fvalue * 100));
	}
}

void USetSlider::SetInField()
{
	if (mSetName.GetBuffer())
	{
		Setup_structfloat* pfloat = (Setup_structfloat*)SystemSetup->GetStructWithOpCode(mSetName.GetBuffer());
		if (pfloat)
		{
			SetBarPosition(pfloat->Date);
		}
	}
}

UIMP_CLASS(USetItem,UControl);
UBEGIN_MESSAGE_MAP(USetItem, UControl)
UON_BN_CLICKED(0 , &USetItem::OnSet)
UEND_MESSAGE_MAP();
void USetItem::StaticInit()
{
	UREG_PROPERTY("SetItem",UPT_STRING,UFIELD_OFFSET(USetItem,mSetName));
}

BOOL USetItem::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	UStaticText* ptext = UDynamicCast(UStaticText, GetChildByID(1));
	if (ptext && mSetName.GetBuffer())
	{
		std::string tempName;
		if(SystemSetup->GetTextFormOpCode(mSetName.GetBuffer(), tempName))
		{
			SystemSetup->RegiestCtrl(mSetName.GetBuffer(), this);
			ptext->SetText(tempName.c_str());
			ptext->SetTextAlign(UT_LEFT);
			//ptext->SetTextColor("c0c0c0");
			SetInField();
		}
	}
	return TRUE;
}
void USetItem::OnDestroy()
{
	UControl::OnDestroy();
}
void USetItem::SetInField()
{
	if (mSetName.GetBuffer())
	{
		Setup_structbool* pbool = (Setup_structbool*)SystemSetup->GetStructWithOpCode(mSetName.GetBuffer());
		if (pbool)
		{
			SetChoose(pbool->Date);
		}
	}
}
void USetItem::OnSet()
{
	UBitmapButton* pbtn = UDynamicCast(UBitmapButton, GetChildByID(0));
	if (pbtn)
	{
		bool btemp = pbtn->IsChecked()?true:false;
		SystemSetup->OnLinkOpCode(mSetName.GetBuffer(), btemp);
		SystemSetup->SetField(mSetName.GetBuffer(), btemp);
	}
}
void USetItem::SetChoose(BOOL bcheck)
{	
	UBitmapButton* pbtn = UDynamicCast(UBitmapButton, GetChildByID(0));
	if (pbtn)
	{
		SystemSetup->OnLinkOpCode(mSetName.GetBuffer(), bcheck?true:false);
		pbtn->SetCheckState(bcheck);
	}
}
void USetItem::SetCanUse(BOOL CanUse)
{
	UStaticText* ptext = UDynamicCast(UStaticText, GetChildByID(1));
	UBitmapButton* pbtn = UDynamicCast(UBitmapButton, GetChildByID(0));
	if (pbtn && ptext)
	{
		pbtn->SetActive(CanUse);
		ptext->SetActive(CanUse);
	}
}
////////////////////////////////////////////////////////////////////////////
//注册4个控件
UIMP_CLASS(UVideoSet, UDialog);
UIMP_CLASS(UAudioSet, UDialog);
UIMP_CLASS(UInGameSet, UDialog);
UIMP_CLASS(UKeySet, UDialog);
void UVideoSet::StaticInit(){}
void UAudioSet::StaticInit(){}
void UInGameSet::StaticInit(){}
void UKeySet::StaticInit(){}
///////////////////////////////////////////////////////////////////////////
//消息映射
UBEGIN_MESSAGE_MAP(UVideoSet, UDialog)
UON_BN_CLICKED(1 , &UVideoSet::OnClickSystemSet)
UON_BN_CLICKED(2 , &UVideoSet::OnClickVideoSet)
UON_BN_CLICKED(3 , &UVideoSet::OnClickAudioSet)
UON_BN_CLICKED(4 , &UVideoSet::OnClickKeySet)
UON_BN_CLICKED(5 , &UVideoSet::OnClickDefault)
UON_BN_CLICKED(6 , &UVideoSet::OnClickUse)
UON_BN_CLICKED(7 , &UVideoSet::OnClickOK)
UON_BN_CLICKED(8 , &UVideoSet::OnClickCancel)
UON_BN_CLICKED(18, &UVideoSet::OnClickChuizhitongbu)
UEND_MESSAGE_MAP()

UBEGIN_MESSAGE_MAP(UAudioSet, UDialog)
UON_BN_CLICKED(1 , &UAudioSet::OnClickSystemSet)
UON_BN_CLICKED(2 , &UAudioSet::OnClickVideoSet)
UON_BN_CLICKED(3 , &UAudioSet::OnClickAudioSet)
UON_BN_CLICKED(4 , &UAudioSet::OnClickKeySet)
UON_BN_CLICKED(5 , &UAudioSet::OnClickDefault)
UON_BN_CLICKED(6 , &UAudioSet::OnClickUse)
UON_BN_CLICKED(7 , &UAudioSet::OnClickOK)
UON_BN_CLICKED(8 , &UAudioSet::OnClickCancel)
UEND_MESSAGE_MAP()

UBEGIN_MESSAGE_MAP(UInGameSet, UDialog)
UON_BN_CLICKED(1 , &UInGameSet::OnClickSystemSet)
UON_BN_CLICKED(2 , &UInGameSet::OnClickVideoSet)
UON_BN_CLICKED(3 , &UInGameSet::OnClickAudioSet)
UON_BN_CLICKED(4 , &UInGameSet::OnClickKeySet)
UON_BN_CLICKED(5 , &UInGameSet::OnClickDefault)
UON_BN_CLICKED(6 , &UInGameSet::OnClickUse)
UON_BN_CLICKED(7 , &UInGameSet::OnClickOK)
UON_BN_CLICKED(8 , &UInGameSet::OnClickCancel)
UEND_MESSAGE_MAP()

UBEGIN_MESSAGE_MAP(UKeySet, UDialog)
UON_BN_CLICKED(1 , &UKeySet::OnClickSystemSet)
UON_BN_CLICKED(2 , &UKeySet::OnClickVideoSet)
UON_BN_CLICKED(3 , &UKeySet::OnClickAudioSet)
UON_BN_CLICKED(4 , &UKeySet::OnClickKeySet)
UON_BN_CLICKED(5 , &UKeySet::OnClickDefault)
UON_BN_CLICKED(6 , &UKeySet::OnClickUse)
UON_BN_CLICKED(7 , &UKeySet::OnClickOK)
UON_BN_CLICKED(8 , &UKeySet::OnClickCancel)
UEND_MESSAGE_MAP()
////////////////////////////////////////////////////////////////////////////
//构造析构
UVideoSet::UVideoSet()
{
}
UVideoSet::~UVideoSet()
{}

UAudioSet::UAudioSet()
{
}
UAudioSet::~UAudioSet()
{}

UInGameSet::UInGameSet()
{
}
UInGameSet::~UInGameSet()
{}

UKeySet::UKeySet()
{
	m_KeySetCtrl = NULL;
}
UKeySet::~UKeySet()
{}
////////////////////////////////////////////////////////////////////////////
//OnCreate OnDestroy
BOOL UVideoSet::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}
	m_bCanDrag = FALSE;
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(2));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	m_chuizhitongbu = NULL;
	m_sanjihuancun = NULL;

    m_FullScreen = UDynamicCast(UButton, GetChildByID(9));
    m_Resolution = UDynamicCast(UComboBox, GetChildByID(13));
	m_Bloom = UDynamicCast(UButton, GetChildByID(14));
	m_WaterReflect = UDynamicCast(UButton, GetChildByID(16));
	m_chuizhitongbu = UDynamicCast(UButton, GetChildByID(18));
	m_sanjihuancun = UDynamicCast(UButton, GetChildByID(20));

	m_MultiSample = UDynamicCast(UButton, GetChildByID(22));

	if(m_sanjihuancun && GetChildByID(19))
	{
		m_sanjihuancun->SetVisible(FALSE);
		GetChildByID(21)->SetVisible(FALSE);
	}
	return TRUE;
}
void UVideoSet::OnDestroy()
{
	UDialog::OnDestroy();
}

void UVideoSet::SetResolutions()
{
    unsigned int uiWidth = ClientState->GetWidth();
    unsigned int uiHeight = ClientState->GetHeight();

    NiDX9Renderer* pkRenderer = (NiDX9Renderer*)NiRenderer::GetRenderer();
    const NiDX9AdapterDesc* pkAdapter = pkRenderer->GetAdapterDesc();

    m_Resolution->Clear();

    NiTPrimitiveSet<unsigned int> kModeSet;
    
    unsigned int uiModeCount = pkAdapter->GetModeCount();
    for (unsigned int i = 0; i < uiModeCount; i++)
    {
        const NiDX9AdapterDesc::ModeDesc* pkMode = pkAdapter->GetMode(i);
        if(pkMode->m_uiWidth < 1024 || pkMode->m_uiHeight < 768)//1024 * 768 以下的分辨率不可选
            continue;

        //防止重复的分辨率
        unsigned int uiUserData = (pkMode->m_uiWidth << 16) + pkMode->m_uiHeight;
        if( kModeSet.Find(uiUserData) != -1 )
            continue;

        kModeSet.Add(uiUserData);

        char acStr[256];
        NiSprintf(acStr, 256, "%u x %u", pkMode->m_uiWidth, pkMode->m_uiHeight);

        m_Resolution->AddItem(acStr, (void*)uiUserData);
        if(pkMode->m_uiWidth == uiWidth && pkMode->m_uiHeight == uiHeight)
            m_Resolution->SetCurSel(m_Resolution->GetCount() - 1);
    }
}

void UVideoSet::SetVisible(BOOL visible)
{
    UDialog::SetVisible(visible);
    if(visible)
    {
        //初始化全屏状态
        m_FullScreen->SetCheckState(ClientState->GetFullScreen());

        //初始化分辨率
        SetResolutions();

        m_Bloom->SetCheckState(ClientState->IsBloomEnabled());
        m_WaterReflect->SetCheckState(ClientState->GetWaterReflect());
		m_MultiSample->SetCheckState(ClientState->GetMultiSample());
 
		if(m_chuizhitongbu && m_sanjihuancun)
		{
			m_chuizhitongbu->SetCheck(!(ClientState->GetPresentationInterval() > 0));
			m_sanjihuancun->SetCheckState(ClientState->GetPresentationInterval() == 3);
		}
    }
}

BOOL UAudioSet::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}	
	m_bCanDrag = FALSE;
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(3));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	return TRUE;
}
void UAudioSet::OnDestroy()
{
	UDialog::OnDestroy();
}

BOOL UInGameSet::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}	
	m_bCanDrag = FALSE;
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(1));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	return TRUE;
}
void UInGameSet::OnDestroy()
{
	UDialog::OnDestroy();
}

BOOL UKeySet::OnCreate()
{
	if(!UDialog::OnCreate())
	{
		return FALSE;
	}	
	m_bCanDrag = FALSE;
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(4));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}	
	UScrollBar* pkScrollBar = UDynamicCast(UScrollBar, GetChildByID(9));
	if(pkScrollBar == NULL)
		return FALSE;

	pkScrollBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);

	if (m_KeySetCtrl == NULL)
	{
		m_KeySetCtrl = UDynamicCast(KeySetcontrol, pkScrollBar->GetChildByID(0));
		if (!m_KeySetCtrl)
		{
			return FALSE;
		}
	}
	return TRUE;
}
void UKeySet::OnDestroy()
{
	UDialog::OnDestroy();
}
////////////////////////////////////////////////////////////////////////////
//Render
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
//映射响应函数
void UAudioSet::OnClickVideoSet()
{
	this->SetVisible(FALSE);
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(3));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	UVideoSet* pVideoSet = INGAMEGETFRAME(UVideoSet, FRAME_IG_VIDEOSET);
	if (pVideoSet)
	{
		pVideoSet->SetVisible(TRUE);
	}
}
void UAudioSet::OnClickAudioSet()
{
	if(SystemSetup)
	{
		SystemSetup->QuitWithoutSave();
	}
}
void UAudioSet::OnClickSystemSet()
{	
	this->SetVisible(FALSE);
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(3));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	UInGameSet* pGameSet = INGAMEGETFRAME(UInGameSet, FRAME_IG_SYSTEMSET);
	if (pGameSet)
	{
		pGameSet->SetVisible(TRUE);
	}
}
void UAudioSet::OnClickKeySet()
{
	this->SetVisible(FALSE);
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(3));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	UKeySet* pKeySet = INGAMEGETFRAME(UKeySet, FRAME_IG_KEYSET);
	if (pKeySet)
	{
		pKeySet->SetVisible(TRUE);
	}
}
void UAudioSet::OnClickCancel()
{
	OnClose();
}
void UAudioSet::OnClickUse()
{
	if(SystemSetup)
	{
		SystemSetup->SaveWithoutQuit();
	}
}
void UAudioSet::OnClickDefault()
{
	if (SystemSetup)
	{
		SystemSetup->SetDefault("audio");
	}
}
void UAudioSet::OnClickOK()
{
	SetVisible(FALSE);
	if(SystemSetup)
	{
		SystemSetup->QuitAndSave();
	}
}

void UVideoSet::OnClickVideoSet()
{
	if(SystemSetup)
	{
		SystemSetup->QuitWithoutSave();
	}
}
void UVideoSet::OnClickAudioSet()
{
	this->SetVisible(FALSE);
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(2));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	UAudioSet* pAudioSet = INGAMEGETFRAME(UAudioSet, FRAME_IG_AUDIOSET);
	if (pAudioSet)
	{
		pAudioSet->SetVisible(TRUE);
	}
}
void UVideoSet::OnClickSystemSet()
{
	this->SetVisible(FALSE);
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(2));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	UInGameSet* pGameSet = INGAMEGETFRAME(UInGameSet, FRAME_IG_SYSTEMSET);
	if (pGameSet)
	{
		pGameSet->SetVisible(TRUE);
	}
}
void UVideoSet::OnClickKeySet()
{
	this->SetVisible(FALSE);
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(2));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	UKeySet* pKeySet = INGAMEGETFRAME(UKeySet, FRAME_IG_KEYSET);
	if (pKeySet)
	{
		pKeySet->SetVisible(TRUE);
	}
}
void UVideoSet::OnClickCancel()
{
	OnClose();
}
void UVideoSet::OnClickUse()
{
	if(SystemSetup)
	{
		SystemSetup->SaveWithoutQuit();
	}

    bool bBloomChanged = false;
    if(ClientState->IsBloomEnabled() && !m_Bloom->IsChecked())
    {
        ClientState->SetBloom(false);
        EffectMgr->CleanupPostEffect();
        bBloomChanged = true;
    }
    else if(!ClientState->IsBloomEnabled() && m_Bloom->IsChecked())
    {
        ClientState->SetBloom(true);
        EffectMgr->CreatePostEffect();
        bBloomChanged = true;
    }

    bool bWaterChanged = false;
    if(ClientState->GetWaterReflect() && !m_WaterReflect->IsChecked())
    {
        bWaterChanged = true;
        ClientState->SetWaterReflect(false);
        CWaterShader::SetWaterRR(false);
    }
    else if(!ClientState->GetWaterReflect() && m_WaterReflect->IsChecked())
    {
        bWaterChanged = true;
        ClientState->SetWaterReflect(true);
        CWaterShader::SetWaterRR(true);
    }

	bool bMutisampleChanged = false;
	if(ClientState->GetMultiSample() && !m_MultiSample->IsChecked())
	{
		bMutisampleChanged = true;
		ClientState->SetMultiSample(false);
	}
	else if(!ClientState->GetMultiSample() && m_MultiSample->IsChecked())
	{
		bMutisampleChanged = true;
		ClientState->SetMultiSample(true);
	}


	bool bPresentationIntervalChanged = false;
	int iPI = 0;
	if(ClientState->GetPresentationInterval()>0 && !m_chuizhitongbu->IsChecked())
	{
        bPresentationIntervalChanged = true;
		iPI = 0;
	}
	else if(ClientState->GetPresentationInterval() == 0 && m_chuizhitongbu->IsChecked())
	{
        bPresentationIntervalChanged = true;
		iPI = 1;
	}
	if(m_sanjihuancun->IsActive())
	{
		//bool bsanji = false;
		//if(ClientState->GetPresentationInterval() == 3  && !m_sanjihuancun->IsChecked())
		//{
		//	bsanji = true;
		//	iPI = 1;
		//}
		//else if(ClientState->GetPresentationInterval() != 3 && m_sanjihuancun->IsChecked())
		//{
		//	bsanji = true;
		//	iPI = 3;
		//}
		//bPresentationIntervalChanged =  bPresentationIntervalChanged || bsanji;
	}
	if(bPresentationIntervalChanged)
	{
        ClientState->SetPresentationInterval(iPI);
	}

    bool bVideoChanged = false;
    unsigned int uiUserData = (unsigned int)m_Resolution->GetItemData(m_Resolution->GetCurSel());
    if(uiUserData != 0)
    {
        unsigned int uiWidth = ((uiUserData >> 16) & 0x0000FFFF);
        unsigned int uiHeight = (uiUserData & 0x0000FFFF);

        if(ClientState->GetWidth() != uiWidth || ClientState->GetHeight() != uiHeight)
        {
            bVideoChanged = true;
            ClientState->SetWidth(uiWidth);
            ClientState->SetHeight(uiHeight);
        }
    }

    if( (ClientState->GetFullScreen() && !m_FullScreen->IsChecked())
     || (!ClientState->GetFullScreen() && m_FullScreen->IsChecked()) )
    {
        bVideoChanged = true;
        ClientState->SetFullScreen(m_FullScreen->IsChecked() ? true : false);
    }

    if( bVideoChanged )
    {
        SYState()->ClientApp->RecreateRender();
    }

	if ( bMutisampleChanged )
	{
		SYState()->ClientApp->ResetMutiSample();
	}

    if(bBloomChanged || bVideoChanged || bWaterChanged || bPresentationIntervalChanged||bMutisampleChanged)
    {
        ClientState->SaveLogin();
    }
}
void UVideoSet::OnClickDefault()
{
	if (SystemSetup)
	{
		SystemSetup->SetDefault("video");
	}
}
void UVideoSet::OnClickOK()
{
	SetVisible(FALSE);
    OnClickUse();
}
void UVideoSet::OnClickChuizhitongbu()
{
	if(m_sanjihuancun && GetChildByID(19))
	{
		m_sanjihuancun->SetActive(m_chuizhitongbu->IsChecked());
		GetChildByID(21)->SetActive(m_chuizhitongbu->IsChecked());
	}
}

void UInGameSet::OnClickVideoSet()
{	
	OnClose();
	UVideoSet* pVideoSet = INGAMEGETFRAME(UVideoSet, FRAME_IG_VIDEOSET);
	if (pVideoSet)
	{
		pVideoSet->SetVisible(TRUE);
	}
}
void UInGameSet::OnClickAudioSet()
{
    OnClose();
	UAudioSet* pAudioSet = INGAMEGETFRAME(UAudioSet, FRAME_IG_AUDIOSET);
	if (pAudioSet)
	{
		pAudioSet->SetVisible(TRUE);
	}
}
void UInGameSet::OnClickSystemSet()
{
	//if(SetupSystem)
	//{
	//	SetupSystem->QuitWithoutSave();
	//}
}
void UInGameSet::OnClickKeySet()
{
	OnClose();
	UKeySet* pKeySet = INGAMEGETFRAME(UKeySet, FRAME_IG_KEYSET);
	if (pKeySet)
	{
		pKeySet->SetVisible(TRUE);
	}
}
void UInGameSet::OnClickCancel()
{
	OnClose();
}
void UInGameSet::OnClickUse()
{
	if(SystemSetup)
	{
		SystemSetup->SaveWithoutQuit();
	}
}
void UInGameSet::OnClickDefault()
{
	if (SystemSetup)
	{
		SystemSetup->SetDefault("system");
	}
}
void UInGameSet::OnClickOK()
{
	SetVisible(FALSE);
	if(SystemSetup)
	{
		SystemSetup->QuitAndSave();
	}
}

void UKeySet::OnClickVideoSet()
{
	OnClose();
	UVideoSet* pVideoSet = INGAMEGETFRAME(UVideoSet, FRAME_IG_VIDEOSET);
	if (pVideoSet)
	{
		pVideoSet->SetVisible(TRUE);
	}
}
void UKeySet::OnClickAudioSet()
{
	OnClose();
	UAudioSet* pAudioSet = INGAMEGETFRAME(UAudioSet, FRAME_IG_AUDIOSET);
	if (pAudioSet)
	{
		pAudioSet->SetVisible(TRUE);
	}
}
void UKeySet::OnClickSystemSet()
{
	OnClose();
	UInGameSet* pGameSet = INGAMEGETFRAME(UInGameSet, FRAME_IG_SYSTEMSET);
	if (pGameSet)
	{
		pGameSet->SetVisible(TRUE);
	}
}
void UKeySet::OnClickKeySet()
{	
	//if(SetupSystem)
	//{
	//	SetupSystem->QuitWithoutSave();
	//}
}
void UKeySet::OnClickCancel()
{
	OnClose();
}
void UKeySet::OnClickUse()
{
	if (m_KeySetCtrl)
	{
		m_KeySetCtrl->SaveWithoutQuit();
	}
}
void UKeySet::OnClickDefault()
{
	if (SystemSetup)
	{
		SystemSetup->SetDefault("key");
		m_KeySetCtrl->SetDefault();
	}
}
void UKeySet::OnClickOK()
{
	if (m_KeySetCtrl)
	{
		m_KeySetCtrl->QuitAndSave();
	}
	SetVisible(FALSE);
}
///////////////////////////////////////////////////////////////////////////////
//OnEscape() OnClose();
void UInGameSet::OnClose()
{	
	this->SetVisible(FALSE);
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(1));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	SystemSetup->QuitWithoutSave();
}
void UKeySet::OnClose()
{
	this->SetVisible(FALSE);
	UBitmapButton* pBtn = UDynamicCast(UBitmapButton, GetChildByID(4));
	if (pBtn)
	{
		pBtn->SetCheck(FALSE);
	}
	SystemSetup->QuitWithoutSave();
	m_KeySetCtrl->QuitWithoutSave();
}
void UVideoSet::OnClose()
{
	SetVisible(FALSE);
	SystemSetup->QuitWithoutSave();
}
void UAudioSet::OnClose()
{
	SetVisible(FALSE);
	SystemSetup->QuitWithoutSave();
}
BOOL UInGameSet::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
BOOL UKeySet::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
BOOL UVideoSet::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
BOOL UAudioSet::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
///////////////////////////////////////////////////////////////////////////////