#ifndef __USYSTEMSET_H__
#define __USYSTEMSET_H__
#include "UInc.h"
class USetSlider : public UControl
{
	UDEC_CLASS(USetSlider);
	UDEC_MESSAGEMAP();
public:
	void SetBarPosition(float fvalue);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	void SetInField();

	void OnSet();
private:
	UString mSetName;
	std::string mTrueName;
};
class USetItem : public UControl
{
	UDEC_CLASS(USetItem);
	UDEC_MESSAGEMAP();
public:
	void SetChoose(BOOL bcheck);
	void SetCanUse(BOOL CanUse);
	void SetInField();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
private:
	void OnSet();
	UString mSetName;
};
class UVideoSet : public UDialog
{
	//��Ƶ����
	UDEC_CLASS(UVideoSet);
	UDEC_MESSAGEMAP();
public:
	UVideoSet();
	~UVideoSet();
    virtual void SetVisible(BOOL value);
    void SetResolutions();

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
    
private:
	void OnClickVideoSet();
	void OnClickAudioSet();
	void OnClickSystemSet();
	void OnClickKeySet();
	void OnClickCancel();
	void OnClickUse();
	void OnClickDefault();
	void OnClickOK();

	void OnClickChuizhitongbu();

private:
    UButton* m_FullScreen;
    UComboBox* m_Resolution;
    UButton* m_Bloom;
	UButton* m_MultiSample;
    UButton* m_WaterReflect;
	UButton* m_chuizhitongbu;
	UButton* m_sanjihuancun;
};
class UAudioSet : public UDialog
{
	//��Ƶ����
	UDEC_CLASS(UAudioSet);
	UDEC_MESSAGEMAP();
public:
	UAudioSet();
	~UAudioSet();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
private:
	void OnClickVideoSet();
	void OnClickAudioSet();
	void OnClickSystemSet();
	void OnClickKeySet();
	void OnClickCancel();
	void OnClickUse();
	void OnClickDefault();
	void OnClickOK();
};
class UInGameSet : public UDialog
{
	//��Ϸ��������
	UDEC_CLASS(UInGameSet);
	UDEC_MESSAGEMAP();
public:
	UInGameSet();
	~UInGameSet();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
private:
	void OnClickVideoSet();
	void OnClickAudioSet();
	void OnClickSystemSet();
	void OnClickKeySet();
	void OnClickCancel();
	void OnClickUse();
	void OnClickDefault();
	void OnClickOK();
};
class UKeySet : public UDialog
{
	//�������ã���ݼ�
	UDEC_CLASS(UKeySet);
	UDEC_MESSAGEMAP();
public:
	UKeySet();
	~UKeySet();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
private:
	void OnClickVideoSet();
	void OnClickAudioSet();
	void OnClickSystemSet();
	void OnClickKeySet();
	void OnClickCancel();
	void OnClickUse();
	void OnClickDefault();
	void OnClickOK();
private:
	class KeySetcontrol * m_KeySetCtrl;
};
#endif