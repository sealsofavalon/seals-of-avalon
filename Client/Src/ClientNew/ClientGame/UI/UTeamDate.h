#pragma once

struct TeamDate  //团队的队员的数据
{
	ui64 Guid;
	string Name;
	ui8 Race;
	ui8 Gender;
	ui8 Class ;
	ui32 Teamid;
	ui16 lv;
	ui32 state ; //状态
	bool isTeamLeader ; // 团长标示
	bool islogin ; 
	ui32 equiphead[4];
	ui32 hp;
	ui32 max_hp;
	ui32 mp;
	ui32 max_mp;
	NiPoint3 pos;
	ui32 mapid;

	TeamDate & operator=(const TeamDate & item)
	{
		if (this == &item)
		{
			return *this;
		}

		Guid = item.Guid;
		Name = item.Name;
		Race = item.Race;
		Gender = item.Gender;
		Class = item.Class;
		//Teamid = item.Teamid;
		lv = item.lv;
		state = item.state;
		isTeamLeader = item.isTeamLeader;
		islogin = item.islogin;
		equiphead[0] = item.equiphead[0];
		equiphead[1] = item.equiphead[1];
		equiphead[2] = item.equiphead[2];
		equiphead[3] = item.equiphead[3];
		hp = item.hp;
		mp = item.mp;
		max_hp = item.max_hp;
		max_mp = item.max_mp;
		pos = item.pos;
		mapid = item.mapid;

		return *this;
	}
	static bool Sort(TeamDate& A_Date, TeamDate& B_Date)
	{
		if (A_Date.islogin)
		{
			if (B_Date.islogin)
			{
				return A_Date.Class > B_Date.Class ;
			}
			return true;
		}else
		{
			if (B_Date.islogin)
			{
				return false ;
			}
			return A_Date.Class > B_Date.Class ;
		}
	}
	void ResetDate()
	{
		Guid = 0;
		Name.clear();
		Race = 0;
		Class = 0;
		//Teamid = 0;
		lv = 0;
		state = 0;
		isTeamLeader = false;
		islogin = false;
		Gender = 0 ;
		equiphead[0] = 0;
		equiphead[1] = 0;
		equiphead[2] = 0;
		equiphead[3] = 0;

		hp = 0;
		max_hp = 0 ;
		mp = 0 ;
		max_mp = 0;
		pos = NiPoint3::ZERO;
		mapid = 0;
	}
};