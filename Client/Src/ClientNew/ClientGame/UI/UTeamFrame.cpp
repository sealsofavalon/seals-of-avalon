#include "stdafx.h"
#include "UTeamFrame.h"
#include "UIGamePlay.h"
#include "UIItemSystem.h"
#include "UFun.h"
#include "SocialitySystem.h"
#include "UInGameBar.h"
#include "UIRightMouseList.h"
#include "Utils/MapInfoDB.h"
#include "ObjectManager.h"
#include "Player.h"
#include "UIShowPlayer.h"
#include "utils/ChatFileterDB.h"
#include "UMessageBox.h"

////单个团○显示的UI
UIMP_CLASS(UTeamCtr::UTeamMemberCtrl, UControl);
void UTeamCtr::UTeamMemberCtrl::StaticInit()
{
	UREG_PROPERTY("bkgskin", UPT_STRING, UFIELD_OFFSET(UTeamCtr::UTeamMemberCtrl, m_pkSkinFileStr));
}
UTeamCtr::UTeamMemberCtrl::UTeamMemberCtrl()
{
	m_TeamDate = NULL;
	m_bSel = FALSE;

	m_BGKSkin = NULL;
	m_LeaderFlag = NULL;
	m_StateSkin = NULL;
	m_isBeginDrag = FALSE ;
	m_bMouseOver = FALSE ;
	m_MaskSkin = NULL;

	m_RenderPos[0].Set(8,1);
	m_RenderPos[1].Set(32,2);
	m_RenderPos[2].Set(49,2);
	m_RenderPos[3].Set(162, 2);
	m_RenderPos[4].Set(200, 2);

	m_ReadyFlag = TRUE;
	m_RightMouseDown = FALSE ;
}

UTeamCtr::UTeamMemberCtrl::~UTeamMemberCtrl()
{
	m_TeamDate = NULL;
	m_bSel = FALSE ;
};
BOOL UTeamCtr::UTeamMemberCtrl::SetMemberDate(TeamDate*  date)
{
	m_TeamDate = date ;
	return TRUE;
}
void UTeamCtr::UTeamMemberCtrl::SetReadyFlag(BOOL Ready)
{
	m_ReadyFlag = Ready;
	if (m_TeamDate && !m_ReadyFlag)
	{
		m_TeamDate->state = 0 ;
	}
}
BOOL UTeamCtr::UTeamMemberCtrl::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE ;
	}

	//Load BGK skin
	if (m_pkSkinFileStr.GetBuffer())
	{
		m_BGKSkin = sm_System->LoadSkin(m_pkSkinFileStr.GetBuffer());
		if (m_BGKSkin == NULL)
		{
			return FALSE;
		}
	}
	if (!m_LeaderFlag)
	{
		m_LeaderFlag = sm_UiRender->LoadTexture("data\\UI\\SocialitySystem\\TeamLeader.png");
		if (!m_LeaderFlag)
		{
			return FALSE ;
		}

	}
	if (!m_StateSkin)
	{
		m_StateSkin = sm_System->LoadSkin("SocialitySystem\\TeamMemberState.skin");
		if (!m_StateSkin)
		{
			return FALSE;
		}
	}

	if (m_MaskSkin == NULL)
	{
		m_MaskSkin =  sm_System->LoadSkin("UIPlayerShow\\CharMask.skin");
		if (!m_MaskSkin)
		{
			return FALSE ;
		}
	}

	return TRUE;
}
void UTeamCtr::UTeamMemberCtrl::OnDestroy()
{
	UControl::OnDestroy();
}


void UTeamCtr::UTeamMemberCtrl::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_TeamDate)
	{
		//render
		if (m_TeamDate->Guid != 0)
		{
			if (m_bSel || m_bMouseOver)
			{
				sm_UiRender->DrawSkin(m_BGKSkin,1, offset);
			}else
			{
				sm_UiRender->DrawSkin(m_BGKSkin,0, offset);
			}

			//团长标识
			if (m_TeamDate->isTeamLeader && m_LeaderFlag)
			{
				sm_UiRender->DrawImage(m_LeaderFlag,offset + m_RenderPos[0]);
			}

			//就为标示
			if (m_ReadyFlag && m_StateSkin)
			{
				ui8 state = m_TeamDate->state;
				if (m_TeamDate->isTeamLeader)
				{
					sm_UiRender->DrawSkin(m_StateSkin, 1,offset + m_RenderPos[1]);
				}else
				{
					if (state >= 0 && state < 3)
					{
						sm_UiRender->DrawSkin(m_StateSkin, state,offset + m_RenderPos[1]);
					}
				}

			}else
			{
				UINT index = 8;
				if (SocialitySys && SocialitySys->GetTeamSysPtr()->GetTargetIcon( &index, m_TeamDate->Guid))
				{
					URect rec ;
					rec.SetSize(14,14);
					rec.SetPosition(offset + m_RenderPos[1]);
					sm_UiRender->DrawSkin(m_MaskSkin, index,rec);
				}
			}

			UColor color = GetClassColor(m_TeamDate->Class);
			if (!m_TeamDate->islogin)
			{
				color = 0xFF808080;
			}
			//名字
			if (m_TeamDate->Name.length())
			{
				sm_UiRender->DrawTextN(m_Style->m_spFont, offset + m_RenderPos[2], m_TeamDate->Name.c_str(),
					m_TeamDate->Name.length(),NULL,color);
			}

			char lv[128];
			sprintf(lv, "%d", m_TeamDate->lv);
			sm_UiRender->DrawTextN(m_Style->m_spFont, offset + m_RenderPos[3], lv,strlen(lv),NULL,color);
			//职业	
			std::string classstr = _TRAN("武修");
			if (m_TeamDate->Class == CLASS_BOW)
			{
				classstr = _TRAN("羽箭");
			}else if (m_TeamDate->Class == CLASS_PRIEST)
			{
				classstr = _TRAN("仙道");
			}else if (m_TeamDate->Class == CLASS_MAGE )
			{
				classstr = _TRAN("真巫");
			}
			sm_UiRender->DrawTextN(m_Style->m_spFont, offset + m_RenderPos[4], classstr.c_str(),classstr.length(),NULL,color);

		}else
		{
			sm_UiRender->DrawSkin(m_BGKSkin, 2 , offset);
		}
	}else
	{
		sm_UiRender->DrawSkin(m_BGKSkin, 2 , offset);
	}
}
void UTeamCtr::UTeamMemberCtrl::OnMouseUp(const UPoint& position, UINT flags)
{
	if (m_Active)
	{
		m_isBeginDrag = FALSE;	

		if (m_TeamDate)
		{
			SocialitySys->GetTeamSysPtr()->SetCurSel(m_TeamDate);
		}

	}
}
void UTeamCtr::UTeamMemberCtrl::OnMouseEnter(const UPoint& position, UINT flags)
{
	if (m_Active)
	{
		if (m_TeamDate)
		{
			m_bMouseOver = TRUE ;
		}
	}
}
void UTeamCtr::UTeamMemberCtrl::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	m_RightMouseDown = TRUE;
}
void UTeamCtr::UTeamMemberCtrl::OnRightMouseUp(const UPoint& position, UINT flags)
{
	if(m_RightMouseDown)
	{
		if (m_TeamDate && m_TeamDate->Guid)
		{
			UIRightMouseList* pkRightMouseList = INGAMEGETFRAME(UIRightMouseList,FRAME_IG_RIGHTMOUSELIST);
			if(pkRightMouseList)
			{
				pkRightMouseList->Popup(position, BITMAPLIST_TEAMGROUP, m_TeamDate->Name.c_str(), m_TeamDate->Guid);
			}
		}

		m_RightMouseDown = FALSE;
	}
}
void UTeamCtr::UTeamMemberCtrl::OnMouseLeave(const UPoint& position, UINT flags)
{
	if (m_Active)
	{
		m_bMouseOver = FALSE ;
	}
}
void UTeamCtr::UTeamMemberCtrl::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	if (m_Active)
	{
		if (m_TeamDate)
		{
			m_isBeginDrag = TRUE;	
		}
	}
}
void UTeamCtr::UTeamMemberCtrl::OnMouseDragged(const UPoint& position, UINT flags)
{
	if (m_Active)
	{
		if (m_isBeginDrag && m_TeamDate && m_TeamDate->Guid)
		{
			BeginDragDrop(this,m_BGKSkin,0, m_TeamDate, UItemSystem::ICT_TEAM_FLAG, (char*)m_TeamDate->Name.c_str(),m_Style->m_spFont,false,true,m_Size);
			m_isBeginDrag = FALSE;
		}
	}
}
BOOL UTeamCtr::UTeamMemberCtrl::OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (nDataFlag == UItemSystem::ICT_TEAM_FLAG)
	{
		UTeamCtr::UTeamMemberCtrl* pkFromCtr = UDynamicCast(UTeamCtr::UTeamMemberCtrl, pDragFrom);
		if (!pkFromCtr)
		{
			return FALSE;
		}

		TeamDate* pkDate = (TeamDate*)pDragData;
		if (!pkDate)
		{
			return FALSE;
		}

		if (m_TeamDate && m_TeamDate->Guid)
		{
			if (m_TeamDate->Teamid == pkDate->Teamid)
			{
				return TRUE;
			}
			SocialitySys->GetTeamSysPtr()->SendChangeMemberSubGruopByGuid(pkDate->Guid, m_TeamDate->Guid);
		}else
		{
			if (m_TeamDate && m_TeamDate->Teamid == pkDate->Teamid)
			{
				return TRUE ;
			}

			SocialitySys->GetTeamSysPtr()->SendChangeMemberSubGroup(pkDate->Name.c_str(), m_TeamDate->Teamid);
		}

		//发送消息给服务器。更换队伍队员情况
		return TRUE;
	}
	return FALSE ;
}
void UTeamCtr::UTeamMemberCtrl::OnDragDropEnd(UControl* pDragTo, void* pDragData, UINT nDataFlag)
{
	UControl::OnDragDropEnd(pDragTo, pDragData,nDataFlag);
}
void UTeamCtr::UTeamMemberCtrl::OnDragDropAccept(UControl* pAcceptCtrl,const UPoint& point,const void* pDragData, UINT nDataFlag)
{
	UControl::OnDragDropAccept(pAcceptCtrl,point, pDragData,nDataFlag);
}
void UTeamCtr::UTeamMemberCtrl::OnDragDropReject(UControl* pRejectCtrl, const void* pDragData, UINT nDataFlag)
{
	UControl::OnDragDropReject(pRejectCtrl, pDragData,nDataFlag);
}


//////////////////////////////////////////////////////////////////////////
///   小队UI 
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UTeamCtr, UControl);
UBEGIN_MESSAGE_MAP(UTeamCtr, UControl)
UEND_MESSAGE_MAP()
void UTeamCtr::StaticInit()
{

}
UTeamCtr::UTeamCtr()
{
	m_TeamMemberCtrl[0] = NULL;
	m_TeamMemberCtrl[1] = NULL;
	m_TeamMemberCtrl[2] = NULL;
	m_TeamMemberCtrl[3] = NULL;
	m_TeamMemberCtrl[4] = NULL;
}
UTeamCtr::~UTeamCtr()
{
}
BOOL UTeamCtr::AddTeamMember(TeamDate* data, int index)   //
{
	if (index >= 0 && index < 5)
	{
		m_TeamMemberCtrl[index]->SetMemberDate(data);
		return TRUE;

	}
	return FALSE;
}
BOOL UTeamCtr::SetCurSel(TeamDate* pkDate)
{
	for (int i = 0; i < 5; i++)
	{
		if (m_TeamMemberCtrl[i]->GetMemberDate() && pkDate == m_TeamMemberCtrl[i]->GetMemberDate())
		{
			m_TeamMemberCtrl[i]->SetSel(TRUE);
		}else
		{
			m_TeamMemberCtrl[i]->SetSel(FALSE);
		}
	}

	return TRUE;
}
void UTeamCtr::SetReadyFlag(BOOL flag)
{
	m_TeamMemberCtrl[0]->SetReadyFlag(flag);
	m_TeamMemberCtrl[1]->SetReadyFlag(flag);
	m_TeamMemberCtrl[2]->SetReadyFlag(flag);
	m_TeamMemberCtrl[3]->SetReadyFlag(flag);
	m_TeamMemberCtrl[4]->SetReadyFlag(flag);
}
BOOL UTeamCtr::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	UPoint pos;
	pos.Set(0,0);
	for (int i =0; i < 5; i++)
	{
		if (m_TeamMemberCtrl[i] == NULL)
		{
			UControl* pkCtrl = sm_System->CreateDialogFromFile("SocialitySystem\\UTeamMemberCtrl.udg");
			if(pkCtrl)
			{
				m_TeamMemberCtrl[i] = UDynamicCast(UTeamCtr::UTeamMemberCtrl, pkCtrl);
				if (m_TeamMemberCtrl[i])
				{
					AddChild(m_TeamMemberCtrl[i]);
					m_TeamMemberCtrl[i]->SetId(0xff+ i);
					m_TeamMemberCtrl[i]->SetVisible(TRUE);
					m_TeamMemberCtrl[i]->SetPosition(pos);

					pos.y += pkCtrl->GetHeight();
				}else
				{
					return FALSE;
				}
			}
		}
	}

	return TRUE;
}
void UTeamCtr::OnDestroy()
{
	return UControl::OnDestroy();
}

//////////////////////////////////////////////////////////////////////////
// 团队界面
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UTeamFrame,UDialog);
UBEGIN_MESSAGE_MAP(UTeamFrame, UDialog)
UON_BN_CLICKED(6,&UTeamFrame::OnClickBtnAddMember)
UON_BN_CLICKED(7,&UTeamFrame::OnClickBtnReadyRequest)
UON_BN_CLICKED(8,&UTeamFrame::OnClickBtnChangToTeam)
UON_BN_CLICKED(10,&UTeamFrame::OnClickBtnFriendPage)
UON_BN_CLICKED(11,&UTeamFrame::OnClickBtnGuildPage)
UON_BN_CLICKED(12,&UTeamFrame::OnClickBtnOLPlayerPage)
UON_BN_CLICKED(13,&UTeamFrame::OnClickBtnTeamPage)
UON_BN_CLICKED(54,&UTeamFrame::OnClickBtnGroupApplyPage)
UON_BN_CLICKED(14,&UTeamFrame::OnClickBtnShowTeam1)
UON_BN_CLICKED(15,&UTeamFrame::OnClickBtnShowTeam2)
UON_BN_CLICKED(16,&UTeamFrame::OnClickBtnShowTeam3)
UON_BN_CLICKED(17,&UTeamFrame::OnClickBtnShowTeam4)
UON_BN_CLICKED(18,&UTeamFrame::OnClickBtnShowTeam5)
UON_BN_CLICKED(19,&UTeamFrame::OnClickBtnShowTeam6)
UON_BN_CLICKED(20,&UTeamFrame::OnClickBtnShowTeamBgk)
UON_BN_CLICKED(22,&UTeamFrame::OnClickBtnShowGrid)
UEND_MESSAGE_MAP()

UTeamFrame::UTeamFrame()
{
	m_Team[0] = NULL;
	m_Team[1] = NULL;
	m_Team[2] = NULL;
	m_Team[3] = NULL;
	m_Team[4] = NULL;
	m_Team[5] = NULL;

	m_TeamBtn[0] = NULL;
	m_TeamBtn[1] = NULL;
	m_TeamBtn[2] = NULL;
	m_TeamBtn[3] = NULL;
	m_TeamBtn[4] = NULL;
	m_TeamBtn[5] = NULL;

	m_ShowSubGroupBgkBtn = NULL;
	m_ShowGridBtn = NULL;
	m_bActiveTime = FALSE;
	m_fTime = 0.0f;
	m_bCanDrag = FALSE ;
}
UTeamFrame::~UTeamFrame()
{

}
void UTeamFrame::StaticInit()
{

}
void UTeamFrame::Show()
{
	SetVisible(TRUE);
}
BOOL UTeamFrame::AddSubGroup(TeamDate** data, ui32 id)
{
	bool bActive = FALSE ;
	for (int i = 0 ; i < 5; i++)
	{
		if (data[i] && data[i]->Guid)
		{
			bActive = TRUE;
		}
		m_Team[id]->AddTeamMember(data[i], i);
	}
	m_TeamBtn[id]->SetActive(bActive);
	return TRUE;
}
BOOL UTeamFrame::SetCurSel(TeamDate* pkDate)
{
	for (int i = 0 ; i < 6; i++)
	{
		m_Team[i]->SetCurSel(pkDate);
	}
	return TRUE;
}
void UTeamFrame::SetReadyFlag(bool flag)
{
	m_Team[0]->SetReadyFlag(flag);
	m_Team[1]->SetReadyFlag(flag);
	m_Team[2]->SetReadyFlag(flag);
	m_Team[3]->SetReadyFlag(flag);
	m_Team[4]->SetReadyFlag(flag);
	m_Team[5]->SetReadyFlag(flag);
	m_readyflag = flag;
}
BOOL UTeamFrame::TeamDestroy() //团队解散
{
	for (int i = 0; i < 6 ; i++)
	{
		for (int j = 0 ; j < 5; j++)
		{
			m_Team[i]->AddTeamMember(NULL, j);
		}
		m_TeamBtn[i]->SetCheck(TRUE);
		m_TeamBtn[i]->SetActive(FALSE);
	}

	m_ShowSubGroupBgkBtn->SetCheck(FALSE);
	m_ShowGridBtn->SetCheck(TRUE);
	SetReadyFlag(false);
	//按钮重置下。 
	return TRUE;

}
void UTeamFrame::Hide()
{
	SetVisible(FALSE);
	UButton* pCheckButton = (UButton*)GetChildByID(13);
	if (pCheckButton){
		pCheckButton->SetCheck(FALSE);
	}
}
void UTeamFrame::SetActiveShowTeam(UINT index, BOOL bActive)
{
	if (index >= 0 && index < 6)
	{
		if (!bActive)
		{
			m_TeamBtn[index]->SetCheck(TRUE);
		}
		m_TeamBtn[index]->SetActive(bActive);
	}
}
void UTeamFrame::SetTeamBtnActive(BOOL bTD, BOOL isleader)
{
	m_bActiveTime = FALSE;
	m_fTime = 0.0f;

	if (bTD)
	{
		if (GetChildByID(8))
		{
			GetChildByID(8)->SetActive(FALSE);
		}
		if (GetChildByID(6))
		{
			GetChildByID(6)->SetActive(isleader);
		}
		if (GetChildByID(7))
		{
			GetChildByID(7)->SetActive(isleader);
		}

	}else
	{
		if (GetChildByID(6))
		{
			GetChildByID(6)->SetActive(FALSE);
		}
		if (GetChildByID(7))
		{
			GetChildByID(7)->SetActive(FALSE);
		}
		if (GetChildByID(8))
		{
			GetChildByID(8)->SetActive(isleader);	
		}
	}

	
}
void UTeamFrame::SetGuildBtnActive(bool active)
{
	//
	UButton* pCheckButton = (UButton*)GetChildByID(11);
	if (pCheckButton == NULL)
	{
		return;
	}
	pCheckButton->SetActive(active);
}
BOOL UTeamFrame::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	for (int i =0; i<6; i++)
	{
		if (m_Team[i] == NULL)
		{
			m_Team[i] = UDynamicCast(UTeamCtr, GetChildByID(i));
		}

		if (m_Team[i] == NULL)
		{
			return FALSE;
		}
	}
	for (int i = 0; i < 6; i++)
	{
		if (m_TeamBtn[i] == NULL)
		{
			m_TeamBtn[i] = UDynamicCast(UBitmapButton, GetChildByID(i + 14));
		}

		if (m_TeamBtn[i] == NULL)
		{
			return FALSE;
		}else
		{
			m_TeamBtn[i]->SetCheckState(FALSE);
			m_TeamBtn[i]->SetActive(FALSE);
		}
	}

	if (m_ShowSubGroupBgkBtn == NULL)
	{
		m_ShowSubGroupBgkBtn = UDynamicCast(UButton, GetChildByID(20));
		if (!m_ShowSubGroupBgkBtn)
		{
			return FALSE ;
		}
		m_ShowSubGroupBgkBtn->SetCheckState(TRUE);
	}
	if (m_ShowGridBtn == NULL)
	{
		m_ShowGridBtn = UDynamicCast(UButton, GetChildByID(22));
		if (!m_ShowGridBtn)
		{
			return FALSE;
		}
		m_ShowGridBtn->SetCheckState(FALSE);
	}
	GetChildByID(6)->SetActive(FALSE);
	GetChildByID(7)->SetActive(FALSE);
	GetChildByID(8)->SetActive(FALSE);	
	GetChildByID(11)->SetActive(FALSE);

	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(21));
	if(pText)
	{
		pText->SetTextColor("dfa805");
		pText->SetTextAlign(UT_RIGHT);
	}

	pText = UDynamicCast(UStaticText, GetChildByID(23));
	if(pText)
	{
		pText->SetTextColor("dfa805");
		pText->SetTextAlign(UT_RIGHT);
	}

	pText = UDynamicCast(UStaticText, GetChildByID(9));
	if(pText)
	{
		pText->SetTextEdge(true);
	}
	for(int i = 114 ; i < 120 ; i++)
	{
		pText = UDynamicCast(UStaticText, GetChildByID(i));
		if(pText)
		{
			pText->SetTextColor("4cb3d2");
			pText->SetTextEdge(true, UColor(128, 128 ,128, 128));
		}
	}
	return TRUE;

}
void UTeamFrame::OnDestroy()
{
	m_bActiveTime = FALSE;
	m_fTime = 0.0f;
	UDialog::OnDestroy();
}
void UTeamFrame::OnTimer(float fDeltaTime)
{
	UDialog::OnTimer(fDeltaTime);
	if (m_bActiveTime)
	{
		m_fTime += fDeltaTime;
		if (m_fTime >= 45.0f)
		{
			GetChildByID(7)->SetActive(TRUE);
			m_bActiveTime = FALSE ;
			m_fTime = 0.0f;
			SetReadyFlag(false);
		}
	}
}
void UTeamFrame::OnRightMouseUp(const UPoint& position, UINT flags)
{
	UDialog::OnRightMouseUp(position, flags);
	if (flags & LKM_SHIFT)
	{
		if (SocialitySys)
		{
			SocialitySys->GetTeamSysPtr()->SortAllTeamDate();
		}	
	}
	if (flags & LKM_CTRL)
	{
		if (SocialitySys)
		{
			SocialitySys->GetTeamSysPtr()->ChangeSortModel();
		}	
	}
}

BOOL UTeamFrame::OnEscape()
{
	if(!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}

void UTeamFrame::OnClose()
{
	if(SocialitySys) SocialitySys->HideAll();
	UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	if (pGameBar)
	{
		UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONFRIEND);
		if (pBtn)
		{
			pBtn->SetCheckState(FALSE);
		}
	}
}
void UTeamFrame::OnClickBtnFriendPage()
{
	if(SocialitySys)
		SocialitySys->ShowFrame(1);
}
void UTeamFrame::OnClickBtnGuildPage()
{
	if(SocialitySys)
		SocialitySys->ShowFrame(0);
}
void UTeamFrame::OnClickBtnOLPlayerPage()
{
	if(SocialitySys)
		SocialitySys->ShowFrame(2);
}
void UTeamFrame::OnClickBtnTeamPage()
{

}
void UTeamFrame::OnClickBtnGroupApplyPage()
{
	if(SocialitySys)
		SocialitySys->ShowFrame(4);
}
void UTeamFrame::OnClickBtnShowTeam1()
{
	SocialitySys->GetTeamSysPtr()->SetSubGroupShow(0, m_TeamBtn[0]->IsChecked());
}
void UTeamFrame::OnClickBtnShowTeam2()
{
	SocialitySys->GetTeamSysPtr()->SetSubGroupShow(1, m_TeamBtn[1]->IsChecked());
}
void UTeamFrame::OnClickBtnShowTeam3()
{
	SocialitySys->GetTeamSysPtr()->SetSubGroupShow(2, m_TeamBtn[2]->IsChecked());
}
void UTeamFrame::OnClickBtnShowTeam4()
{
	SocialitySys->GetTeamSysPtr()->SetSubGroupShow(3, m_TeamBtn[3]->IsChecked());
}
void UTeamFrame::OnClickBtnShowTeam5()
{
	SocialitySys->GetTeamSysPtr()->SetSubGroupShow(4, m_TeamBtn[4]->IsChecked());
}
void UTeamFrame::OnClickBtnSort()
{
	SocialitySys->GetTeamSysPtr()->SortAllTeamDate();
}
void UTeamFrame::OnClickBtnShowGrid()
{
	// GRID 
	std::vector<std::string> text;
	text.push_back("groupshow");
	UButton* ptn = UDynamicCast(UButton, GetChildByID(22));
	if(ptn)
	{
		if(ptn->IsChecked())
			text.push_back("open");
		else
			text.push_back("close");

		OnCommandCallPlug(text);
	}
}
void UTeamFrame::OnClickBtnShowTeam6()
{
	SocialitySys->GetTeamSysPtr()->SetSubGroupShow(5, m_TeamBtn[5]->IsChecked());
}
void UTeamFrame::OnClickBtnShowTeamBgk()
{
	SocialitySys->GetTeamSysPtr()->SetLockSubGroupShowBGK(m_ShowSubGroupBgkBtn->IsChecked());	
}
void UTeamFrame::OnClickBtnChangToTeam()  //转化团队
{
	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->SendChangeToTeam();
	}
}
void UTeamFrame::OnClickBtnAddMember() //添加成员
{
	UIAddMemberEdit* pEdit = NULL;
	pEdit = INGAMEGETFRAME(UIAddMemberEdit, FRAME_IG_ADDMEMBERDLG);
	if (pEdit)
	{
		pEdit->OnAddMember(2);
	}
}
void UTeamFrame::OnClickBtnReadyRequest() // 就位询问
{
	if (SocialitySys->GetTeamSysPtr())
	{
		SocialitySys->GetTeamSysPtr()->SendMemberReady(0);
		m_bActiveTime = TRUE ;
		GetChildByID(7)->SetActive(FALSE);
	}
}
//////////////////////////////////////////////////////////////////////////
class UGroupApplyItem : public UControl
{
	UDEC_CLASS(UGroupApplyItem);
public:
	UGroupApplyItem()
	{
		m_GUID = 0;
		m_NameStateText = NULL;
		m_LevelStateText = NULL;
		m_ZhaoMuStateText = NULL;
		m_ZhaoMuIntroText = NULL;
		m_GroupMemNumText = NULL;
		m_RightMouseDown = FALSE;
		m_bChoose = FALSE;
		m_MouseDown = FALSE;
	}
	~UGroupApplyItem()
	{

	}
public:
	void SetBeChoose(BOOL choose)
	{
		m_bChoose = choose;
		UpdataMemTextColor();
	}
	void UpdataMemTextColor()
	{
		UColor color = m_bChoose?m_Style->m_FontColorHL:m_Style->m_FontColor;
		m_NameStateText->SetTextColor(color);
		m_GroupMemNumText->SetTextColor(color);
		m_ZhaoMuIntroText->SetTextColor(color);
		m_LevelStateText->SetTextColor(color);

		m_NameStateText->SetTextEdge(m_bChoose);
		m_GroupMemNumText->SetTextEdge(m_bChoose);
		m_ZhaoMuIntroText->SetTextEdge(m_bChoose);
		m_ZhaoMuStateText->SetTextEdge(m_bChoose);
		m_LevelStateText->SetTextEdge(m_bChoose);

	}
	void SetMemberBase(const char* Name, ui32 level, ui32 CurMemNum, ui32 MaxMemNum, ui64 guid, std::string& Intro, bool ApplyState)
	{
		//姓名
		if (Name)
		{
			m_NameStateText->SetText(Name);
			m_Name = Name;
		}

		//等级
		char buf[256];buf[0] = 0;
		itoa(level, buf, 10);
		m_LevelStateText->SetText(buf);
		//人数
		sprintf(buf, "%u/%u", CurMemNum, MaxMemNum);
		m_GroupMemNumText->SetText(buf);

		//招募状态
		if (ApplyState)
		{
			m_ZhaoMuStateText->SetText(_TRAN("开放"));
			m_ZhaoMuStateText->SetTextColor("00FF00");
		}else
		{
			m_ZhaoMuStateText->SetText(_TRAN("关闭"));
			m_ZhaoMuStateText->SetTextColor("FF0000");
		}
		//招募说明
		m_ZhaoMuIntroText->SetText(Intro.c_str());
		m_GUID = guid;
	}
	bool GetName(std::string& str)
	{
		if (m_Name.size())
		{
			str = m_Name;
			return true;
		}
		return false;
	}
	ui64 GetGuid()
	{
		return m_GUID;	
	}
protected:
	virtual BOOL OnCreate()
	{
		if (!UControl::OnCreate())
		{
			return FALSE;
		}
		if (m_NameStateText == NULL)
		{
			m_NameStateText = UDynamicCast(UStaticText, GetChildByID(0));
			if (!m_NameStateText)
			{
				return FALSE;
			}
			m_NameStateText->SetTextAlign(UT_CENTER);
		}
		if (m_LevelStateText == NULL)
		{
			m_LevelStateText = UDynamicCast(UStaticText, GetChildByID(1));
			if (!m_LevelStateText)
			{
				return FALSE;
			}
			m_LevelStateText->SetTextAlign(UT_CENTER);
		}
		if (m_GroupMemNumText == NULL)
		{
			m_GroupMemNumText = UDynamicCast(UStaticText, GetChildByID(2));
			if (!m_GroupMemNumText)
			{
				return FALSE;
			}
			m_GroupMemNumText->SetTextAlign(UT_CENTER);
		}
		if (m_ZhaoMuStateText == NULL)
		{
			m_ZhaoMuStateText = UDynamicCast(UStaticText, GetChildByID(3));
			if (!m_ZhaoMuStateText)
			{
				return FALSE;
			}
			m_ZhaoMuStateText->SetTextAlign(UT_CENTER);
		}
		if (m_ZhaoMuIntroText == NULL)
		{
			m_ZhaoMuIntroText = UDynamicCast(UStaticText, GetChildByID(4));
			if (!m_ZhaoMuIntroText)
			{
				return FALSE;
			}
			m_ZhaoMuIntroText->SetTextAlign(UT_CENTER);
		}
		return TRUE;
	}
	virtual void OnDestroy()
	{
		if (m_bChoose)
		{
			if (SocialitySys->GetApplyGroupSysPtr())
			{
				SocialitySys->GetApplyGroupSysPtr()->SetSelectItem(NULL);
			}
		}
		UControl::OnDestroy();
	}
	virtual void OnMouseEnter(const UPoint& position, UINT flags)
	{

	}
	virtual void OnMouseLeave(const UPoint& position, UINT flags)
	{

	}
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
	{
		m_MouseDown = TRUE;
	}
	virtual void OnMouseUp(const UPoint& position, UINT flags)
	{
		if (m_MouseDown)
		{
			m_MouseDown = FALSE;
			if (SocialitySys->GetApplyGroupSysPtr())
			{
				SocialitySys->GetApplyGroupSysPtr()->SetSelectItem(this);
			}
		}
	}
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
	{
		m_RightMouseDown = TRUE;
	}
	virtual void OnRightMouseUp(const UPoint& position, UINT flags)
	{
		if (m_RightMouseDown)
		{
			//UIRightMouseList* pkRightMouseList = INGAMEGETFRAME(UIRightMouseList,FRAME_IG_RIGHTMOUSELIST);
			//if(pkRightMouseList)
			//{
			//	pkRightMouseList->Popup(position, BITMAPLIST_FRIEND, m_Name.c_str(), m_GUID);
			//}
			m_RightMouseDown = FALSE;
			if (SocialitySys->GetApplyGroupSysPtr())
			{
				SocialitySys->GetApplyGroupSysPtr()->SetSelectItem(this);
			}
		}
	}
	virtual void OnRender(const UPoint& offset, const URect& updateRect)
	{
		if(m_bChoose && SocialitySys && SocialitySys->mBKGtexture)
			sm_UiRender->DrawImage(SocialitySys->mBKGtexture, updateRect);

		RenderChildWindow(offset, updateRect);
	}
protected:
	BOOL		 m_bChoose;
	BOOL		 m_RightMouseDown;
	BOOL		 m_MouseDown;
	ui64		 m_GUID;
	std::string  m_Name;
	UStaticText* m_NameStateText;
	UStaticText* m_LevelStateText;
	UStaticText* m_GroupMemNumText;
	UStaticText* m_ZhaoMuStateText;
	UStaticText* m_ZhaoMuIntroText;
};
UIMP_CLASS(UGroupApplyItem, UControl)
void UGroupApplyItem::StaticInit(){}

void ApplyGroupSystem::SetSelectItem(class UGroupApplyItem* item)
{
	if(m_bSelectItem)
	{
		m_bSelectItem->SetBeChoose(FALSE);
	}
	m_bSelectItem = item;
	if (m_bSelectItem)
	{
		m_bSelectItem->SetBeChoose(TRUE);
	} 
}

//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UGroupApplyList, UControl);
void UGroupApplyList::StaticInit()
{

}

UGroupApplyList::UGroupApplyList()
{

}
UGroupApplyList::~UGroupApplyList()
{

}
void UGroupApplyList::UpdataList()
{
	std::vector<MSG_S2C::stGroupListAck::GroupFindInfo> GroupList;
	if(!SocialitySys->GetApplyGroupSysPtr()->GetGroupList(GroupList))
		return;

	int curpage, maxpage;
	SocialitySys->GetApplyGroupSysPtr()->GetPage(curpage, maxpage);

	for (unsigned int ui = 0 ; ui < 20 ; ui++)
	{
		UGroupApplyItem* pItem = UDynamicCast(UGroupApplyItem, GetChildByID(ui));
		if (pItem)
		{
			UINT pos = curpage * 20 + ui;
			bool bShow = pos < GroupList.size();
			pItem->SetVisible(bShow);
			if (bShow)
			{
				pItem->SetMemberBase(GroupList[pos].name.c_str(), GroupList[pos].Level, GroupList[pos].CurCount, GroupList[pos].MaxCount, GroupList[pos].guid, GroupList[pos].title, GroupList[pos].bOpen);
			}
		}
	}
	if (SocialitySys->GetApplyGroupSysPtr())
	{
		SocialitySys->GetApplyGroupSysPtr()->SetSelectItem(NULL);
	}
}
void UGroupApplyList::ClearList()
{
	if (SocialitySys->GetApplyGroupSysPtr())
	{
		SocialitySys->GetApplyGroupSysPtr()->SetSelectItem(NULL);
	}
	sm_System->SetMouseControl(NULL);
	for (unsigned int ui = 0 ; ui < 20 ; ui++)
	{
		UGroupApplyItem* pItem = UDynamicCast(UGroupApplyItem, GetChildByID(ui));
		if (pItem)
		{
			pItem->SetVisible(FALSE);
		}
	}
}
BOOL UGroupApplyList::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	ClearList();
	return TRUE;
}
void UGroupApplyList::OnDestroy()
{
	UControl::OnDestroy();
}
void UGroupApplyList::OnRender(const UPoint& offset,const URect &updateRect)
{
	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	g_pRenderInterface->SetScissorRect(sm_UiRender->GetClipRect());

	RenderChildWindow(offset, updateRect);

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
}

UIMP_CLASS(UGroupApplyFrame,UDialog);
UBEGIN_MESSAGE_MAP(UGroupApplyFrame, UDialog)
UON_BN_CLICKED(40,&UGroupApplyFrame::OnClickBtnOpenApply)
UON_BN_CLICKED(41,&UGroupApplyFrame::OnClickBtnCloseApply)
UON_BN_CLICKED(42,&UGroupApplyFrame::OnClickBtnApplyIntro)
UON_BN_CLICKED(43,&UGroupApplyFrame::OnClickBtnApplyGroup)
UON_BN_CLICKED(50,&UGroupApplyFrame::OnClickBtnFriendPage)
UON_BN_CLICKED(51,&UGroupApplyFrame::OnClickBtnGuildPage)
UON_BN_CLICKED(52,&UGroupApplyFrame::OnClickBtnOLPlayerPage)
UON_BN_CLICKED(53,&UGroupApplyFrame::OnClickBtnTeamPage)
UON_BN_CLICKED(54,&UGroupApplyFrame::OnClickBtnGroupApplyPage)
UON_BN_CLICKED(60,&UGroupApplyFrame::OnPrevPage)
UON_BN_CLICKED(62,&UGroupApplyFrame::OnNextPage)
UON_CBOX_SELCHANGE(45, &UGroupApplyFrame::OnQueryAreaChange)
UEND_MESSAGE_MAP()

void UGroupApplyFrame::StaticInit()
{

}
UGroupApplyFrame::UGroupApplyFrame()
{
	m_GroupList = NULL;
}
UGroupApplyFrame::~UGroupApplyFrame()
{

}

void UGroupApplyFrame::Show()
{
	SetVisible(TRUE);
	OnQueryAreaChange();
	CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
	if (pkPlayer)
	{
		bool IsLeader = TeamShow->IsTeamLeader(pkPlayer->GetGUID());
		UButton* pCheckButton = (UButton*)GetChildByID(40);
		if (pCheckButton){
			pCheckButton->SetActive(IsLeader);
		}
		pCheckButton = (UButton*)GetChildByID(41);
		if (pCheckButton){
			pCheckButton->SetActive(IsLeader);
		}
		pCheckButton = (UButton*)GetChildByID(42);
		if (pCheckButton){
			pCheckButton->SetActive(IsLeader);
		}
		pCheckButton = (UButton*)GetChildByID(43);
		if (pCheckButton){
			pCheckButton->SetActive(TeamShow->GetTeamLeaderGuid() == 0);
		}
	}
}

void UGroupApplyFrame::Hide()
{
	SetVisible(FALSE);
	UButton* pCheckButton = (UButton*)GetChildByID(54);
	if (pCheckButton){
		pCheckButton->SetCheck(FALSE);
	}
}

void UGroupApplyFrame::SetGuildBtnActive(bool active)
{
	UButton* pCheckButton = (UButton*)GetChildByID(51);
	if (pCheckButton == NULL)
	{
		return;
	}
	pCheckButton->SetActive(active);
}

BOOL UGroupApplyFrame::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(49));
	if (pText)
	{
		pText->SetTextEdge(true);
	}
	if(m_GroupList == NULL)
	{
		m_GroupList = UDynamicCast(UGroupApplyList, GetChildByID(0));
		if (!m_GroupList)
		{
			return FALSE;
		}
	}

	UComboBox* pComboBox = NULL;
	pComboBox = UDynamicCast(UComboBox, GetChildByID(45));
	if(pComboBox)
	{
		pComboBox->Clear();
		std::vector<CMapInfoDB::MapInfoEntry> vinfo;
		g_pkMapInfoDB->GetMapVInfoFromFlags(1, vinfo);

		pComboBox->AddItem(_TRAN("不限"));
		for (unsigned int ui = 0 ; ui < vinfo.size() ; ui++)
		{
			unsigned int* pUiMapId = new unsigned int;
			memcpy(pUiMapId, &vinfo[ui].entry, sizeof(unsigned int));
			pComboBox->AddItem(vinfo[ui].desc.c_str(), pUiMapId);
		}
		pComboBox->SetCurSel(0);
	}
	m_bCanDrag = FALSE;
	return TRUE;
}

void UGroupApplyFrame::OnDestroy()
{
	UDialog::OnDestroy();
}

void UGroupApplyFrame::OnClose()
{
	if(SocialitySys) SocialitySys->HideAll();
	UInGameBar * pGameBar = INGAMEGETFRAME(UInGameBar,FRAME_IG_ACTIONSKILLBAR);
	if (pGameBar)
	{
		UButton* pBtn =  (UButton*)pGameBar->GetChildByID(UINGAMEBAR_BUTTONFRIEND);
		if (pBtn)
		{
			pBtn->SetCheckState(FALSE);
		}
	}
}

BOOL UGroupApplyFrame::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UGroupApplyFrame::OnClickBtnFriendPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(1);
	}
}
void UGroupApplyFrame::OnClickBtnGuildPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(0);
	}
}
void UGroupApplyFrame::OnClickBtnOLPlayerPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(2);
	}
}
void UGroupApplyFrame::OnClickBtnTeamPage()
{
	if (SocialitySys)
	{
		SocialitySys->ShowFrame(3);
	}
}

void UGroupApplyFrame::OnClickBtnGroupApplyPage()
{

}

void UGroupApplyFrame::OnClickBtnSortByName()
{
	SocialitySys->GetApplyGroupSysPtr()->SortGroupByName();
	m_GroupList->UpdataList();
}

void UGroupApplyFrame::OnClickBtnSortLevel()
{
	SocialitySys->GetApplyGroupSysPtr()->SortGroupByLevel();
	m_GroupList->UpdataList();
}

void UGroupApplyFrame::OnClickBtnSortByState()
{
	SocialitySys->GetApplyGroupSysPtr()->SortGroupByState();
	m_GroupList->UpdataList();
}

void UGroupApplyFrame::OnClickBtnSortByMemNum()
{
	SocialitySys->GetApplyGroupSysPtr()->SortGroupByNum();
	m_GroupList->UpdataList();
}

void UGroupApplyFrame::OnClickBtnSortByIntro()
{
	SocialitySys->GetApplyGroupSysPtr()->SortGroupByIntro();
	m_GroupList->UpdataList();
}

void UGroupApplyFrame::OnClickBtnOpenApply()
{
	SocialitySys->GetApplyGroupSysPtr()->SendGroupModifyCanApplyForJoining(true);
}

void UGroupApplyFrame::OnClickBtnCloseApply()
{
	SocialitySys->GetApplyGroupSysPtr()->SendGroupModifyCanApplyForJoining(false);
}

void UGroupApplyFrame::OnClickBtnApplyIntro()
{
	SocialitySys->GetApplyGroupSysPtr()->CallGroupApplyIntro();
}

void UGroupApplyFrame::OnClickBtnApplyGroup()
{
	ui64 guid = 0;
	if (SocialitySys->GetApplyGroupSysPtr()->GetSelectItem())
	{
		guid = SocialitySys->GetApplyGroupSysPtr()->GetSelectItem()->GetGuid();
	}
	if(guid)
	{
		SocialitySys->GetApplyGroupSysPtr()->SendGroupApplyForJoining(guid);
	}
}

void UGroupApplyFrame::OnQueryAreaChange()
{
	int iIndex = 0;
	uint32 mapid = 0;
	UComboBox* pComboBox = NULL;
	pComboBox = UDynamicCast(UComboBox, GetChildByID(45));
	if(pComboBox)
	{
		iIndex = pComboBox->GetCurSel();
		if(iIndex != -1)
		{
			unsigned int* pUiMapId = (unsigned int* )pComboBox->GetItemData(iIndex);
			if(pUiMapId)
				mapid = *pUiMapId;
			else
				mapid = 0;
		}
	}
	SocialitySys->GetApplyGroupSysPtr()->SendGroupListReq(mapid);
}

void UGroupApplyFrame::OnPrevPage()
{
	UControl* pBtn = GetChildByID(60);
	if (pBtn)
	{
		pBtn->SetActive(SocialitySys->GetApplyGroupSysPtr()->PrevPage());
	}
	int curpage, maxpage;
	SocialitySys->GetApplyGroupSysPtr()->GetPage(curpage, maxpage);
	pBtn = GetChildByID(62);
	if (pBtn)
	{
		pBtn->SetActive(maxpage > 0 && curpage < maxpage);
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(61));
	if (pText)
	{
		char buf[256];
		sprintf(buf, "%d/%d", curpage + 1, maxpage + 1);
		pText->SetText(buf);
	}
	m_GroupList->UpdataList();
}

void UGroupApplyFrame::OnNextPage()
{
	UControl* pBtn = GetChildByID(62);
	if (pBtn)
	{
		pBtn->SetActive(SocialitySys->GetApplyGroupSysPtr()->NextPage());
	}
	int curpage, maxpage;
	SocialitySys->GetApplyGroupSysPtr()->GetPage(curpage, maxpage);
	pBtn = GetChildByID(60);
	if (pBtn)
	{
		pBtn->SetActive(maxpage > 0 && curpage > 0);
	}
	UStaticText* pText = UDynamicCast(UStaticText, GetChildByID(61));
	if (pText)
	{
		char buf[256];
		sprintf(buf, "%d/%d", curpage + 1, maxpage + 1);
		pText->SetText(buf);
	}
	m_GroupList->UpdataList();
}

void UGroupApplyFrame::SetApplyForJoiningState(bool bOpen)
{
	CPlayerLocal* pkPlayer = ObjectMgr->GetLocalPlayer();
	if (pkPlayer)
	{
		bool IsLeader = TeamShow->IsTeamLeader(pkPlayer->GetGUID());
		if (IsLeader)
		{
			UControl* pBtnOpen = GetChildByID(40);
			UControl* pBtnClose = GetChildByID(41);
			if (pBtnOpen)
				pBtnOpen->SetActive(!bOpen);
			if (pBtnClose)
				pBtnClose->SetActive(bOpen);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UGroupApplyIntro, UDialog)
UBEGIN_MESSAGE_MAP(UGroupApplyIntro, UDialog)
UON_BN_CLICKED(1, &UGroupApplyIntro::OnClickBtnOK)
UON_BN_CLICKED(2, &UGroupApplyIntro::OnClickBtnCancel)
UEND_MESSAGE_MAP()
void UGroupApplyIntro::StaticInit(){}

UGroupApplyIntro::UGroupApplyIntro()
{

}
UGroupApplyIntro::~UGroupApplyIntro()
{

}
BOOL UGroupApplyIntro::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	UStaticTextEdit* pTextEdit = UDynamicCast(UStaticTextEdit, GetChildByID(3));
	if (pTextEdit)
	{
		pTextEdit->SetTextMaxLen(20);
	}
	return TRUE;
}
void UGroupApplyIntro::OnDestroy()
{
	UDialog::OnDestroy();
}
BOOL UGroupApplyIntro::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UGroupApplyIntro::OnClose()
{
	SetVisible(FALSE);
	UStaticTextEdit* pTextEdit = UDynamicCast(UStaticTextEdit, GetChildByID(3));
	if (pTextEdit)
	{
		pTextEdit->SetText("");
	}
}
void UGroupApplyIntro::OnClickBtnOK()
{
	char buf[1024];
	UStaticTextEdit* pTextEdit = UDynamicCast(UStaticTextEdit, GetChildByID(3));
	if (pTextEdit)
	{
		pTextEdit->GetText(buf, 1024);
		std::string Title = buf;

		if (!g_pkChatFilterDB->GetFilterChatMsg(std::string(buf)))
		{
			SocialitySys->GetApplyGroupSysPtr()->SendGroupModifyTitleReq(Title);
		}
		else
		{
			UMessageBox::MsgBox_s(_TRAN("您的输入中带有非法字符！"));
		}
		OnClose();
	}
}
void UGroupApplyIntro::OnClickBtnCancel()
{
	OnClose();
}

//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UApplyGroupListDlg, UDialog)
UBEGIN_MESSAGE_MAP(UApplyGroupListDlg, UDialog)
UON_BN_CLICKED(1, &UApplyGroupListDlg::OnClickBtnInvite)
UON_BN_CLICKED(2, &UApplyGroupListDlg::OnClickBtnRefuse)
UON_BN_CLICKED(6, &UApplyGroupListDlg::OnClickBtnSortByName)
UON_BN_CLICKED(7, &UApplyGroupListDlg::OnClickBtnSortByClass)
UON_BN_CLICKED(8, &UApplyGroupListDlg::OnClickBtnSortByGender)
UON_BN_CLICKED(9, &UApplyGroupListDlg::OnClickBtnSortByLevel)
UON_UGD_CLICKED(100, &UApplyGroupListDlg::OnChooseList)
UEND_MESSAGE_MAP()
void UApplyGroupListDlg::StaticInit(){}

UApplyGroupListDlg::UApplyGroupListDlg()
{

}
UApplyGroupListDlg::~UApplyGroupListDlg()
{

}

void UApplyGroupListDlg::UpdateList()
{
	m_SpellList->Clear();
	UButton* pBtn1 = UDynamicCast(UButton, GetChildByID(1));
	UButton* pBtn2 = UDynamicCast(UButton, GetChildByID(2));
	pBtn1->SetActive(false);
	pBtn2->SetActive(false);
	char buf[512];
	std::vector<stApplyGroupMem> ApplyGroupList;
	if(SocialitySys->GetApplyGroupSysPtr()->GetApplyGroupList(ApplyGroupList))
	{
		char szClass[8][20] = { "", "武修", "羽箭", "仙道", "真巫", "变身", "刺客", "" };
		for (unsigned int ui = 0 ; ui < ApplyGroupList.size() ; ++ui)
		{			
			NiSprintf(buf, 512, 
				"<Col1><color:R000G255B000A255><center>%s<end>"
				"<Col2><color:R000G255B000A255><center>%s<end>"
				"<Col3><color:R000G255B000A255><center>%s<end>"
				"<Col4><color:R000G255B000A255><center>%u<end>",
				ApplyGroupList[ui].name.c_str(),
				_TRAN(szClass[ApplyGroupList[ui].classMask]),
				ApplyGroupList[ui].Gender ? _TRAN("男") : _TRAN("女"),
				ApplyGroupList[ui].Level);
			std::string* pName = new std::string;
			*pName = ApplyGroupList[ui].name;
			m_SpellList->AddItem(buf, pName);
		}
	}else{
		pBtn1->SetActive(false);
		pBtn2->SetActive(false);
		m_SpellList->Clear();
	}
}

BOOL UApplyGroupListDlg::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	UScrollBar* pBar = UDynamicCast(UScrollBar, GetChildByID(0));
	if (pBar == NULL)
	{
		return FALSE;
	}
	pBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);

	m_SpellList  = UDynamicCast(USpecListBox, pBar->GetChildByID(100));
	if (m_SpellList == NULL)
	{
		return FALSE;
	}
	m_SpellList->SetGridSize(UPoint(250, 15));
	m_SpellList->SetGridCount(8,1);
	m_SpellList->SetCol1(0, 0);
	m_SpellList->SetWidthCol1(106);
	m_SpellList->SetCol2(108, 0);
	m_SpellList->SetWidthCol2(46);
	m_SpellList->SetCol3(156, 0);
	m_SpellList->SetWidthCol3(46);
	m_SpellList->SetCol4(204, 0);
	m_SpellList->SetWidthCol4(46);

	UButton* pBtn1 = UDynamicCast(UButton, GetChildByID(1));
	UButton* pBtn2 = UDynamicCast(UButton, GetChildByID(2));
	pBtn1->SetActive(false);
	pBtn2->SetActive(false);

	return TRUE;
}
void UApplyGroupListDlg::OnDestroy()
{
	m_SpellList->Clear();
	UDialog::OnDestroy();
}
BOOL UApplyGroupListDlg::OnEscape()
{
	if (!UDialog::OnEscape())
	{
		OnClose();
	}
	return TRUE;
}
void UApplyGroupListDlg::OnClose()
{
	SetVisible(FALSE);
}

void UApplyGroupListDlg::OnChooseList()
{
	UButton* pBtn1 = UDynamicCast(UButton, GetChildByID(1));
	UButton* pBtn2 = UDynamicCast(UButton, GetChildByID(2));
	int cursel = m_SpellList->GetCurSel();
	if (cursel == -1)
	{
		pBtn1->SetActive(false);
		pBtn2->SetActive(false);
		return;
	}
	pBtn1->SetActive(true);
	pBtn2->SetActive(true);
}

void UApplyGroupListDlg::OnClickBtnInvite()
{
	int cursel = m_SpellList->GetCurSel();
	if (cursel == -1)
		return;
	void* pDate = m_SpellList->GetItemData(cursel);
	if (pDate)
	{
		std::string name = *(std::string*)pDate;
		SocialitySys->GetApplyGroupSysPtr()->SendGroupApplyForJoiningAck(name, 1);
	}
}
void UApplyGroupListDlg::OnClickBtnRefuse()
{
	int cursel = m_SpellList->GetCurSel();
	if (cursel == -1)
		return;
	void* pDate = m_SpellList->GetItemData(cursel);
	if (pDate)
	{
		std::string name = *(std::string*)pDate;
		SocialitySys->GetApplyGroupSysPtr()->SendGroupApplyForJoiningAck(name, 0);
	}
}
void UApplyGroupListDlg::OnClickBtnSortByName()
{
	SocialitySys->GetApplyGroupSysPtr()->SortMemberByName();
	UpdateList();
}
void UApplyGroupListDlg::OnClickBtnSortByClass()
{
	SocialitySys->GetApplyGroupSysPtr()->SortMemberByClass();
	UpdateList();
}
void UApplyGroupListDlg::OnClickBtnSortByGender()
{
	SocialitySys->GetApplyGroupSysPtr()->SortMemberByGender();
	UpdateList();;
}
void UApplyGroupListDlg::OnClickBtnSortByLevel()
{
	SocialitySys->GetApplyGroupSysPtr()->SortMemberByLevel();
	UpdateList();
}