#pragma once
#include "UTeamDate.h"
#include "UInc.h"

class UProgressBar ;
//单个小队UI
class UTeamCtr : public UControl  //一个小队
{
	UDEC_CLASS(UTeamCtr);
	UDEC_MESSAGEMAP();
public:
	UTeamCtr();
	~UTeamCtr();


	class UTeamMemberCtrl : public UControl  //单个团○显示的UI
	{
		UDEC_CLASS(UTeamMemberCtrl);
		//UDEC_MESSAGEMAP();
	public:
		UTeamMemberCtrl();
		~UTeamMemberCtrl();
	public:
		BOOL SetMemberDate(TeamDate*  date);
		TeamDate* GetMemberDate(){return m_TeamDate;}

		void SetSel(BOOL sel){m_bSel = sel;}
		void SetReadyFlag(BOOL Ready);
	protected:
		BOOL virtual OnCreate();
		void virtual OnDestroy();

		void virtual OnRender(const UPoint& offset,const URect &updateRect);
		virtual void OnMouseUp(const UPoint& position, UINT flags);
		virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
		virtual void OnMouseEnter(const UPoint& position, UINT flags);
		virtual void OnMouseLeave(const UPoint& position, UINT flags);

		virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
		virtual void OnRightMouseUp(const UPoint& position, UINT flags);

		virtual void OnMouseDragged(const UPoint& position, UINT flags);
		virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
		virtual void OnDragDropEnd(UControl* pDragTo, void* pDragData, UINT nDataFlag);
		virtual void OnDragDropAccept(UControl* pAcceptCtrl,const UPoint& point,const void* pDragData, UINT nDataFlag);
		virtual void OnDragDropReject(UControl* pRejectCtrl, const void* pDragData, UINT nDataFlag);
	protected:
		TeamDate* m_TeamDate ;
		USkinPtr m_BGKSkin ;
		UString m_pkSkinFileStr;
		UTexturePtr m_LeaderFlag;
		USkinPtr m_StateSkin;
		USkinPtr m_MaskSkin ;
		UPoint m_RenderPos[5];
		BOOL m_bSel ;
		BOOL m_bMouseOver ;
		BOOL m_RightMouseDown;
		BOOL m_isBeginDrag; 
		BOOL m_ReadyFlag;
	};

public:
	BOOL AddTeamMember(TeamDate* data, int index);     //
	void SetReadyFlag(BOOL flag);
	BOOL SetCurSel(TeamDate* pkDate);
protected:
	BOOL virtual OnCreate();
	void virtual OnDestroy();
protected:
	UTeamMemberCtrl* m_TeamMemberCtrl[5]; //
};

//团队UI 

class UTeamFrame : public UDialog
{
	UDEC_CLASS(UTeamFrame);
	UDEC_MESSAGEMAP();
public:
	UTeamFrame();
	~UTeamFrame();

	void Show();
	void Hide();
	void SetGuildBtnActive(bool active);
	void SetTeamBtnActive(BOOL bTD, BOOL isleader);
	void SetActiveShowTeam(UINT index, BOOL bActive);

	ui8 m_readyflag;
public:
	//data
	BOOL AddSubGroup(TeamDate** data, ui32 id);     //添加新的团队成员
	void SetReadyFlag(bool flag);
	BOOL SetCurSel(TeamDate* pkDate);
	// clear ui
	BOOL TeamDestroy(); //团队解散
protected:
	void OnClickBtnFriendPage();
	void OnClickBtnGuildPage();
	void OnClickBtnOLPlayerPage();
	void OnClickBtnTeamPage();
	void OnClickBtnGroupApplyPage();

	void OnClickBtnChangToTeam();  //转化团队
	void OnClickBtnAddMember(); //添加成员
	void OnClickBtnReadyRequest(); // 就位询问

	void OnClickBtnShowTeam1();
	void OnClickBtnShowTeam2();
	void OnClickBtnShowTeam3();
	void OnClickBtnShowTeam4();
	void OnClickBtnShowTeam5();
	void OnClickBtnShowTeam6();

	void OnClickBtnShowTeamBgk();
	void OnClickBtnShowGrid();
	void OnClickBtnSort();
protected:
	BOOL virtual OnCreate();
	void virtual OnDestroy();
	virtual void OnClose();
	void virtual OnTimer(float fDeltaTime);
	virtual BOOL OnEscape();
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
private:
	UTeamCtr* m_Team[6];
	UBitmapButton* m_TeamBtn[6];
	UButton* m_ShowSubGroupBgkBtn;
	UButton* m_ShowGridBtn;
	BOOL m_bActiveTime ;
	float m_fTime;
};

class UGroupApplyList : public UControl
{
	UDEC_CLASS(UGroupApplyList);
public:
	UGroupApplyList();
	~UGroupApplyList();
public:
	void UpdataList();
	void ClearList();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
};

class UGroupApplyFrame : public UDialog
{
	UDEC_CLASS(UGroupApplyFrame);
	UDEC_MESSAGEMAP();
public:
	UGroupApplyFrame();
	~UGroupApplyFrame();

	void Show();
	void Hide();
	void SetGuildBtnActive(bool active);
	UGroupApplyList* GetUGroupApplyList(){return m_GroupList;}
	void SetApplyForJoiningState(bool bOpen);
	void OnQueryAreaChange();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();
protected:

	void OnClickBtnFriendPage();
	void OnClickBtnGuildPage();
	void OnClickBtnOLPlayerPage();
	void OnClickBtnTeamPage();
	void OnClickBtnGroupApplyPage();

	
	void OnClickBtnSortByName();
	void OnClickBtnSortLevel();
	void OnClickBtnSortByState();
	void OnClickBtnSortByMemNum();
	void OnClickBtnSortByIntro();

	void OnClickBtnOpenApply();
	void OnClickBtnCloseApply();
	void OnClickBtnApplyIntro();
	void OnClickBtnApplyGroup();

	void OnPrevPage();
	void OnNextPage();

protected:
	UGroupApplyList* m_GroupList;
};

class UGroupApplyIntro : public UDialog
{
	UDEC_CLASS(UGroupApplyIntro);
	UDEC_MESSAGEMAP();
public:
	UGroupApplyIntro();
	~UGroupApplyIntro();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
protected:
	void OnClickBtnOK();
	void OnClickBtnCancel();
};

class UApplyGroupListDlg : public UDialog
{
	UDEC_CLASS(UApplyGroupListDlg);
	UDEC_MESSAGEMAP();
public:
	UApplyGroupListDlg();
	~UApplyGroupListDlg();

	void UpdateList();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnEscape();
	virtual void OnClose();
protected:
	void OnChooseList();
	void OnClickBtnInvite();
	void OnClickBtnRefuse();
	void OnClickBtnSortByName();
	void OnClickBtnSortByClass();
	void OnClickBtnSortByGender();
	void OnClickBtnSortByLevel();

	class USpecListBox* m_SpellList;
};