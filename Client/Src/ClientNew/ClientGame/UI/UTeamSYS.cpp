#include "stdafx.h"
#include "UTeamSYS.h"
#include "UIIndex.h"
#include "SocialitySystem.h"
#include "Network/PacketBuilder.h"
#include "LocalPlayer.h"
#include "ObjectManager.h"
#include "UIShowPlayer.h"
#include "UMessageBox.h"
#include "Map/Map.h"
#include "UChat.h"
#include "PlayerInputMgr.h"
#include "UFun.h"
#include "UIMiniMap.h"
#include "UIGamePlay.h"




//////////////////////////////////////////////////////////////////////////
// Team system
//////////////////////////////////////////////////////////////////////////

Teamsys::Teamsys()
{
	for (int i =0; i < 6 ; i++)
	{
		for (int j =0; j < 5; j++)
		{
			m_TeamDate[i][j] = new TeamDate;
			m_TeamDate[i][j]->ResetDate();
		}
	}

	for (int i = 0 ; i < TATGET_ICON_MAX ; i++)
	{
		m_TargetIcon[i] = 0;
	}
	
	for (int i = 0; i < 6; i++)
	{
		m_SubGroupShow[i] = NULL;
	}
	m_localSub = 6;
	m_bTD = FALSE;
	m_bAutoSort = FALSE;
	m_TeamFrame = NULL;
	m_TeamLeader = 0;
	m_scTeamLeader.clear();
}
Teamsys::~Teamsys()
{
	for (int i =0; i < 6 ; i++)
	{
		for (int j =0; j < 5; j++)
		{
			if(m_TeamDate[i][j])
			{
				delete m_TeamDate[i][j];
				m_TeamDate[i][j] = NULL;
			}
		}
	}
};
void Teamsys::SendAcceptGroupInvite()
{
	PacketBuilder->SendGroupAcceptMsg();
}
void Teamsys::SendRefuseGroupInvite()
{
	PacketBuilder->SendRefuseGroupMsg();
}
void Teamsys::SendReadyOK()
{
	PacketBuilder->SendMemberReady(1);
}
void Teamsys::SendNotReady()
{
	PacketBuilder->SendMemberReady(2);
}
BOOL Teamsys::InTeam(ui64 guid)
{
	return (GetTeamDate(guid)!= NULL);
}
BOOL Teamsys::GetSortIndex(ui64 guid, UINT* index)
{
	if (!guid)
	{
		return FALSE ;
	}
	for (int i =0; i < 6 ; i++)
	{
		for (int j =0; j < 5; j++)
		{
			if(m_TeamDate[i][j]->Guid == guid)
			{
				*index = i ;
				return TRUE;
			}
		}
	}

	return FALSE;
}
TeamDate* Teamsys::GetTeamDate(ui64 guid)
{
	for (int i =0; i < 6 ; i++)
	{
		for (int j =0; j < 5; j++)
		{
			if(m_TeamDate[i][j]->Guid == guid)
			{
				return m_TeamDate[i][j];
			}
		}
	}

	return NULL;
}
BOOL Teamsys::AddSubGroupInfo(MSG_S2C::stGroupList::stSubGroup subGroup, UINT id, ui64 Leader)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return FALSE ;
	}

	BOOL bLocalSubGroup = FALSE;
	for (int i = 0 ; i < 5; i++)
	{
		m_TeamDate[id][i]->ResetDate();
	}
	int Local = 0;
	for ( int i =0; i < 5 /*subGroup.vMembers.size()*/; i++)
	{
		if (i < subGroup.vMembers.size())
		{
			m_TeamDate[id][i]->Guid = subGroup.vMembers[i].guid;
			m_TeamDate[id][i]->Name = subGroup.vMembers[i].name;
			m_TeamDate[id][i]->Race = subGroup.vMembers[i].Race;
			m_TeamDate[id][i]->Gender = subGroup.vMembers[i].Gender;
			m_TeamDate[id][i]->Class = subGroup.vMembers[i].Class;
			m_TeamDate[id][i]->state = subGroup.vMembers[i].flags;
			m_TeamDate[id][i]->lv = subGroup.vMembers[i].level;
			m_TeamDate[id][i]->islogin = subGroup.vMembers[i].isloggedin;
			m_TeamDate[id][i]->equiphead[0] = subGroup.vMembers[i].equiphead[0];
			m_TeamDate[id][i]->equiphead[1] = subGroup.vMembers[i].equiphead[1];
			m_TeamDate[id][i]->equiphead[2] = subGroup.vMembers[i].equiphead[2];
			m_TeamDate[id][i]->equiphead[3] = subGroup.vMembers[i].equiphead[3];
			if (Leader == subGroup.vMembers[i].guid)
			{
				m_TeamDate[id][i]->isTeamLeader = true;

				m_scTeamLeader = m_TeamDate[id][i]->Name;				
			}else
			{
				m_TeamDate[id][i]->isTeamLeader = false;
			}
			
		
			if (m_TeamDate[id][i]->Guid == pkLocal->GetGUID())
			{
				Local = i ;
				bLocalSubGroup = TRUE ; //是否是本地玩家所在的小队
			}

			CPlayer* pkPlayer = ObjectMgr->GetPlayer(m_TeamDate[id][i]->Guid);
			if (pkPlayer)
			{
				m_TeamDate[id][i]->hp = pkPlayer->GetHP();
				m_TeamDate[id][i]->max_hp = pkPlayer->GetMaxHP();
				m_TeamDate[id][i]->mp = pkPlayer->GetMP();
				m_TeamDate[id][i]->max_mp = pkPlayer->GetMaxMP();
				m_TeamDate[id][i]->islogin = true;
			}

		}
		m_TeamDate[id][i]->Teamid = subGroup.ID;
	}

	
	SortDateByIndex(id);
	
	TeamDate* p[5];
	p[0] = m_TeamDate[id][0];
	p[1] = m_TeamDate[id][1];
	p[2] = m_TeamDate[id][2];
	p[3] = m_TeamDate[id][3];
	p[4] = m_TeamDate[id][4];
	
	
	//是团队
	if (m_bTD)
	{
		m_TeamFrame->AddSubGroup(p,id);
		m_SubGroupShow[id]->AddSubGroup(p, id);
		//m_SubGroupShow[id]->AddSubGroup(p,id);
	}

	if (bLocalSubGroup)
	{
		m_localSub = id;
		
		UpdateLocalSubGroup();
	}
	TeamShow->SetTeamLeaderGuid(Leader);

	return TRUE;
}
BOOL Teamsys::MemberReadyChange(ui64 guid ,ui8 ready)
{
	if (ready == 0)
	{
		UMessageBox::MsgBox( _TRAN("是否准备好"), (BoxFunction*)Teamsys::SendReadyOK,(BoxFunction*)Teamsys::SendNotReady, 15.f);
		UMessageBox::DisableEscape();
		SYState()->IAudio->PlaySound("Sound/UI/checkready.wav", NiPoint3::ZERO, 1.0f, 1 );
		return  TRUE;
	}
	
	TeamDate* pkDate = GetTeamDate(guid);
	if (pkDate)
	{
		pkDate->state = ready;
	}

	return TRUE;
	
}
BOOL Teamsys::AddGroupList(const vector<MSG_S2C::stGroupList::stSubGroup> &pkTeamDate, ui64 Leader, ui32 subGroupCount)
{
	if (pkTeamDate.size() == 0)
	{
		return FALSE ;
	}
	m_bTD = !(subGroupCount == 1);
	for (int i = 0 ; i < subGroupCount; i++)
	{
		if (i <  6)
		{
			AddSubGroupInfo(pkTeamDate[i], i, Leader);
		}
	}

	//如果不是团队。UI重置
	if (!m_bTD)
	{
		m_TeamFrame->TeamDestroy();
		for (int i = 0; i < 6; i++)
		{
			m_SubGroupShow[i]->TeamDestroy();
		}
	}
	
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal && pkLocal->GetGUID() == Leader)
	{
		m_TeamFrame->SetTeamBtnActive(m_bTD, TRUE);
	}else
	{
		//TeamDate* pkDate = GetTeamDate(Leader);
		//if (pkDate)
		//{
		//	TeamSysNotify("恭喜你加入了[%s]的团", pkDate->Name.c_str());
		//}
		m_TeamFrame->SetTeamBtnActive(m_bTD, FALSE);
	}
	m_TeamFrame->SetReadyFlag(false);
	m_TeamLeader = Leader;
	return TRUE;
}
void Teamsys::ErrorText(string & name , int res)
{
	switch (res)
	{	
	case ERR_PARTY_NO_ERROR:
		break;
	case ERR_PARTY_CANNOT_FIND:
		TeamSysNotify( _TRAN("请求失败，无法找到这个玩家。") );
		break;
	case ERR_PARTY_IS_NOT_IN_YOUR_PARTY:
		TeamSysNotify("ERR_PARTY_IS_NOT_IN_YOUR_PARTY");
		break;
	case ERR_PARTY_UNK:
		TeamSysNotify("ERR_PARTY_UNK");
		break;
	case ERR_PARTY_IS_FULL:	
		TeamSysNotify( _TRAN("请求失败，您的队伍已满。") );
		break;
	case ERR_PARTY_ALREADY_IN_GROUP:		
		TeamSysNotify( _TRAN("请求失败，您所邀请的玩家已经有队伍了。") );
		break;
	case ERR_PARTY_YOU_ARENT_IN_A_PARTY:
		TeamSysNotify("ERR_PARTY_YOU_ARENT_IN_A_PARTY");
		break;
	case ERR_PARTY_YOU_ARE_NOT_LEADER:	
		TeamSysNotify( _TRAN("请求失败，您不是队长。"));
		break;
	case ERR_PARTY_WRONG_FACTION:	
		TeamSysNotify( _TRAN("请求失败，错误的阵营。") );
		break;
	case ERR_PARTY_IS_IGNORING_YOU:	
		TeamSysNotify( _TRAN("请求失败，队伍拒绝了你。") );
		break;
	case ERR_PARTY_IN_ARENA_OR_BG:
		TeamSysNotify(_TRAN("竞技场或战场中不能进行此操作。"));
		break;;
	}
}
void Teamsys::UpdateTeamGroupDate(ui64 guid, ui32 mask)
{
	if (mask)
	{
		CPlayer* pkPlayer = ObjectMgr->GetPlayer(guid);
		if (!pkPlayer)
		{
			return ;
		}
		BOOL bNeedSort =  FALSE ;
		TeamDate* pkData = GetTeamDate(guid);
		if (!pkData)
		{
			return ;
		}
		if (!pkData->islogin)
		{
			bNeedSort = TRUE ;
			pkData->islogin = TRUE;
		}
		if(mask == GROUP_UPDATE_FLAG_HEALTH)		
		{
			pkData->hp = pkPlayer->GetHP();
		}
		if(mask == GROUP_UPDATE_FLAG_MAXHEALTH)
		{
			pkData->max_hp = pkPlayer->GetMaxHP();
		}
		if(mask == GROUP_UPDATE_FLAG_POWER)		
		{
			pkData->mp = pkPlayer->GetMP();
		}
		if(mask == GROUP_UPDATE_FLAG_MAXPOWER)	
		{
			pkData->max_mp = pkPlayer->GetMaxMP();
		}
		if(mask == GROUP_UPDATE_FLAG_LEVEL)		
		{
			pkData->lv = pkPlayer->GetLevel();
		}

		if (bNeedSort && m_bAutoSort)
		{
			UINT index = 0;
			if (GetSortIndex(guid,&index))
			{
				SortDateByIndex(index);
				if (index == m_localSub)
				{
					UpdateLocalSubGroup();
				}
			}else
			{
				;//
			}
		}else
		{
			for (int i =0 ; i < 6; i++)
			{
				m_SubGroupShow[i]->UpdateUI(pkData);
			}
			TeamShow->UpdateFromObj(guid,mask, FALSE);
		}
		
	}
	
	
}
void Teamsys::UpdateGroupStat(ui64 guid, ui32 mask, const std::string &data)
{
	//判断是不是在本地小队

	BOOL bNeedSort = FALSE;
	TeamDate* pkDate = GetTeamDate(guid);
	if (pkDate)
	{
		ByteBuffer t_data;
		t_data.append( data.c_str(), data.size() );
		if ( mask == GROUP_UPDATE_FLAG_NONE  )
		{
			// 下线
			if (pkDate->islogin)
			{
				bNeedSort = TRUE;
			}
			pkDate->islogin = FALSE; 
		}else
		{
			if (!pkDate->islogin)
			{
				bNeedSort = TRUE ;
			}
			pkDate->islogin = TRUE;
			if(mask & GROUP_UPDATE_FLAG_ONLINE)
			{
				ui8 Tem;
				t_data>>Tem;
				if (Tem & 0x02)
				{
					;//PVP
				}
				if (Tem & 0x08)
				{
					;//CORPSE
				}
				if (Tem & 0x10)
				{
					;// deadth
				}
			}
			
			if(mask & GROUP_UPDATE_FLAG_HEALTH)		
			{
				t_data>>pkDate->hp;
			}
			if(mask & GROUP_UPDATE_FLAG_MAXHEALTH)
			{
				t_data>>pkDate->max_hp;
			}
			if(mask & GROUP_UPDATE_FLAG_POWER_TYPE)		
			{
				ui8 tem;
				t_data>>tem;
			}
			if(mask & GROUP_UPDATE_FLAG_POWER)		
			{
				t_data>>pkDate->mp;
			}
			if(mask & GROUP_UPDATE_FLAG_MAXPOWER)	
			{
				t_data>>pkDate->max_mp;
			}
			if(mask & GROUP_UPDATE_FLAG_LEVEL)		
			{
				t_data>>pkDate->lv;
			}

			if(mask & GROUP_UPDATE_FLAG_ZONEID)
			{
				uint16 temp;
				t_data>>temp;
			}
			if(mask &GROUP_UPDATE_FLAG_POSITION	)		
			{
				ui16 x, y ;
				t_data>>x>>y;
				pkDate->pos.x = x ;
				pkDate->pos.y = y;
				pkDate->pos.z = 0;
			}
			if(mask & GROUP_UPDATE_TYPE_FULL_REQUEST_REPLY)
			{
				uint64 temp64;uint8 temp8;
				t_data>>temp64>>temp8>>temp64;
			}
			if(mask &GROUP_UPDATE_FLAG_PLAYER_AURAS)
			{
				//	CPlayer* pChar = (CPlayer*)ObjectMgr->GetObject(guid);
				//	if(!pChar)
				//		return;
				for(uint32 i = 0; i < MAX_AURAS; ++i)
				{
					if(true/*auramask & (uint64(1) << i)*/)
					{
						ui16 aura;ui8 temp;
						t_data >> aura;
						t_data >> temp;

						ULOG("%u, %u", aura, temp);
						//			pChar->SetUInt32Value(UNIT_FIELD_AURA + i, aura);
						//			// todo
					}
				}
			}
			if(mask & GROUP_UPDATE_FLAG_EQUIPMENT)		
			{
				t_data >> pkDate->equiphead[0]>> pkDate->equiphead[1]>> pkDate->equiphead[2]>> pkDate->equiphead[3];


			}
			if(mask & GROUP_UPDATE_FLAG_MAPID)		
			{
				t_data>> pkDate->mapid;
			}
		}	

		if (bNeedSort && m_bAutoSort)
		{
			UINT index = 0;
			if (GetSortIndex(guid,&index))
			{
				SortDateByIndex(index);
				if (index == m_localSub)
				{
					UpdateLocalSubGroup();
				}
			}else
			{
				;//
			}
		}else
		{
			for (int i =0 ; i < 6; i++)
			{
				m_SubGroupShow[i]->UpdateUI(pkDate);
			}
		}	

		TeamShow->UpdateFromObj(guid,mask, TRUE);
	}

}
BOOL Teamsys::DestroyTeamInfo()
{
	for (int i =0; i < 6 ; i++)
	{
		for (int j =0; j < 5; j++)
		{
			if(m_TeamDate[i][j])
			{
				m_TeamDate[i][j]->ResetDate();
			}
		}
	}
	for (int i = 0 ; i < TATGET_ICON_MAX ; i++)
	{
		m_TargetIcon[i] = 0;
	}
	
	TeamShow->SetTeamLeaderGuid(0);
	TeamShow->ReceiveDestoryGroup();

	m_bTD = FALSE;
	m_localSub = 6;
	m_TeamLeader = 0;
	m_scTeamLeader.clear();
	m_bAutoSort = FALSE ;
	m_TeamFrame->SetTeamBtnActive(FALSE,FALSE);
	for (int i = 0; i < 6 ; i++)
	{
		m_SubGroupShow[i]->TeamDestroy();
	}
	return m_TeamFrame->TeamDestroy();
}
BOOL Teamsys::GetTargetIcon(UINT* index, ui64 guid, int uniqueId)
{
	for (int i = 0; i < TATGET_ICON_MAX; i++)
	{
		if (guid && m_TargetIcon[i] == guid && m_TargetIconUniqueID[i] == uniqueId)
		{
			*index = i;
			return  TRUE;
		}
	}

	return FALSE ;
}
void Teamsys::GetTeamMemByTeam(int teamid, std::vector<TeamDate>& vec)
{
	if (teamid < 0 || teamid >= 6)
		return;
	vec.clear();
	vec.reserve(5);
	for (int i = 0 ; i < 5 ; i++)
	{
		if (m_TeamDate[teamid][i]->Guid)
		{
			vec.push_back(*m_TeamDate[teamid][i]);
		}
	}
}
BOOL Teamsys::GetTargetIcon(UINT* index, ui64 guid)
{
	for (int i = 0; i < TATGET_ICON_MAX; i++)
	{
		if (guid && m_TargetIcon[i] == guid)
		{
			*index = i;
			return  TRUE;
		}
	}
	return FALSE ;
}
void Teamsys::UpdateLocalSubGroup()
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return ;
	}
	if (m_localSub < 6)
	{
		std::vector<TeamDate*> vTem;
		for (int i = 0; i < 5; i++)
		{
			if ( m_TeamDate[m_localSub][i]->Guid != pkLocal->GetGUID())
			{
				vTem.push_back(m_TeamDate[m_localSub][i]);
			}
		}
		TeamShow->ReceiveMemberList(vTem);
	}
}
void Teamsys::MemberTeamChange(ui64 guid,ui8 team)
{
	//
	if (!guid)
	{
		return ;
	}
	TeamDate tem ;
	TeamDate* pkDate = NULL;
	TeamDate* ToDate = NULL;
	UINT Index = 0; 
	UINT ToIndex = 0;
	BOOL bUpdateLocalTeam = FALSE;

	ui64 localGuid = 0 ;
	if (ObjectMgr->GetLocalPlayer())
	{
		localGuid = ObjectMgr->GetLocalPlayer()->GetGUID();
	}else
	{
		return ;
	}

	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 5 ; j++ )
		{
			if (m_TeamDate[i][j]->Guid == guid)
			{
				tem = *m_TeamDate[i][j];
				pkDate = m_TeamDate[i][j];
				Index = i ;
			}
			if (m_TeamDate[i][j]->Teamid == team && m_TeamDate[i][j]->Guid == 0 && ToDate == NULL )
			{
				ToDate = m_TeamDate[i][j];
				ToIndex = i ;
			}
		}

		if (pkDate && ToDate)
		{
			break ;
		}
	}

	if (pkDate && ToDate)
	{
		if (Index == m_localSub || ToIndex == m_localSub)
		{
			bUpdateLocalTeam = TRUE ;
		}
		if (guid == localGuid)
		{
			m_localSub = ToIndex;
			bUpdateLocalTeam = TRUE ;
		}
		pkDate->ResetDate();
		*ToDate = tem ;

		SortDateByIndex(Index);
		SortDateByIndex(ToIndex);
	}

	if (bUpdateLocalTeam)
	{
		UpdateLocalSubGroup();
	}
}
void Teamsys::AddMember(MSG_S2C::stGroupAddMember::stMember newMember, ui8 team)
{
	if (!newMember.guid)
	{
		return ;
	}
	TeamDate* ToDate = NULL;
	UINT ToIndex = 0;
	BOOL bUpdateLocalTeam = FALSE;

	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 5 ; j++ )
		{
			if (m_TeamDate[i][j]->Teamid == team && m_TeamDate[i][j]->Guid == 0 && ToDate == NULL)
			{
				ToDate = m_TeamDate[i][j];
				ToIndex = i ;
				if (i == m_localSub)
				{
					bUpdateLocalTeam = TRUE;
				}
				break ;
			}
		}

		if (ToDate)
		{
			break ;
		}
	}

	if (ToDate)
	{
		ToDate->Guid = newMember.guid;
		ToDate->Name = newMember.name;
		ToDate->Race = newMember.Race;
		ToDate->Class = newMember.Class ;
		ToDate->Gender = newMember.Gender;
		ToDate->Teamid = team;
		ToDate->state = newMember.flags;
		ToDate->islogin = newMember.isloggedin;
		ToDate->lv = newMember.level;
		ToDate->equiphead[0] = newMember.equiphead[0];
		ToDate->equiphead[1] = newMember.equiphead[1];
		ToDate->equiphead[2] = newMember.equiphead[2];
		ToDate->equiphead[3] = newMember.equiphead[3];
		
		if (m_bTD)
		{
			std::string str = _TRAN( N_NOTICE_C60, ToDate->Name.c_str() );
			TeamSysNotify(str.c_str()/*"[%s]加入了团队", ToDate->Name.c_str()*/);
			
			//ClientSystemNotify("[%s]加入了团队", ToDate->Name.c_str());
		}else
		{
			std::string str = _TRAN( N_NOTICE_C61, ToDate->Name.c_str() );
			TeamSysNotify(str.c_str()/*"[%s]加入了队伍", ToDate->Name.c_str()*/);
			//ClientSystemNotify("[%s]加入了队伍", ToDate->Name.c_str());
		}

		SortDateByIndex(ToIndex);
	}

	if (bUpdateLocalTeam)
	{
		UpdateLocalSubGroup();
	}
	
}
void Teamsys::RemoveMember(ui64 guid)
{
	if (guid == 0)
	{
		return ;
	}
	CPlayerLocal * pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return ;
	}
	UINT SortIndex = 0 ;
	BOOL bUpdateLocal = FALSE ;
	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 5 ; j++ )
		{
			if (m_TeamDate[i][j]->Guid == guid)
			{
				if (m_bTD)
				{
					std::string str = _TRAN( N_NOTICE_C62, m_TeamDate[i][j]->Name.c_str() );
					TeamSysNotify(str.c_str()/*"[%s]离开了团队", m_TeamDate[i][j]->Name.c_str()*/);
				}else
				{
					std::string str = _TRAN( N_NOTICE_C63, m_TeamDate[i][j]->Name.c_str() );
					TeamSysNotify( str.c_str()/*"[%s]离开了队伍", m_TeamDate[i][j]->Name.c_str()*/);
				}

				m_TeamDate[i][j]->ResetDate();
			
				if (i == m_localSub)
				{
					bUpdateLocal = TRUE;
				}
				SortIndex =  i ;
				break ;
			}
		}
	}
	

	SortDateByIndex(SortIndex);

	if (bUpdateLocal)
	{
		UpdateLocalSubGroup();
	}
}
void Teamsys::MemberTeamSwep(ui64 desguid, ui64 srcGuid)
{
	if (desguid == 0 || srcGuid == 0)
	{
		return ;
	}
	TeamDate* DesPtr = NULL ;
	TeamDate* SrcPtr = NULL ;
	TeamDate TemDes;
	TeamDate TemSrc;

	UINT srcIndex = 0;
	UINT desIndex = 0;
	ui64 localGuid = 0 ;


	if (ObjectMgr->GetLocalPlayer())
	{
		localGuid = ObjectMgr->GetLocalPlayer()->GetGUID();
	}else
	{
		return ;
	}
	BOOL bUpdateLocalTeam = FALSE;
	
	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 5 ; j++ )
		{
			if (m_TeamDate[i][j]->Guid == desguid)
			{
				DesPtr = m_TeamDate[i][j];
				TemDes = *m_TeamDate[i][j];
				desIndex = i ;
			}

			if (m_TeamDate[i][j]->Guid == srcGuid)
			{
				SrcPtr = m_TeamDate[i][j];
				TemSrc = *m_TeamDate[i][j];
				srcIndex = i ;
			}
		}


		if (SrcPtr && DesPtr)
		{
			break ;
		}
	}

	if (DesPtr && SrcPtr)
	{
		if (srcIndex == m_localSub || desIndex == m_localSub)
		{
			bUpdateLocalTeam = TRUE ;
		}
		if (localGuid == desguid)
		{
			m_localSub = srcIndex;
		}else
		{
			if (localGuid == srcGuid)
			{
				m_localSub = desIndex;
			}
		}
		
		*DesPtr = TemSrc;
		*SrcPtr = TemDes;

		SortDateByIndex(desIndex);
		SortDateByIndex(srcIndex);
	}

	
	if (bUpdateLocalTeam)
	{
		UpdateLocalSubGroup();
	}
}
static const int BufSize = 4096;
static char buffer[BufSize];
void Teamsys::TeamSysNotify(const char *s, ...)
{
	if(!ChatSystem) return;

	va_list args;
	va_start(args, s);

	signed int len = _vsnprintf(buffer, BufSize, s, args);
	va_end(args);
	if (len < 0 || len >= BufSize || len + 1>= BufSize)	{
		if (BufSize > 0)
			buffer[BufSize-1] = '\0';
	}else
	{
		buffer[len] = '\n';
		buffer[len + 1] = '\0';
	}
	if (m_bTD)
	{
		ChatSystem->AppendChatMsg(CHAT_MSG_RAID, buffer);
	}else
	{
		ChatSystem->AppendChatMsg(CHAT_MSG_PARTY, buffer);
	}
}
BOOL Teamsys::MemberIconChange(MSG_S2C::stGroupSetPlayerIcon teamIcon) //团员标识改编
{
	//先取消GUID的标示
	for ( int  i =0 ; i < TATGET_ICON_MAX; i++)
	{
		m_TargetIcon[i] = teamIcon.m_targetIcons[i];
		m_TargetIconUniqueID[i] = teamIcon.m_targetUniqueID[i];
	}
	return TRUE;
}
BOOL Teamsys::LeaderChange(const char* name)
{
	UINT SortIndex0 = 0 ;
	UINT SortIndex1 = 0 ;
	for (int i =0; i < 6 ; i++)
	{
		for (int j =0; j < 5; j++)
		{
			if(m_TeamDate[i][j]->Guid)
			{
				if (_stricmp(m_TeamDate[i][j]->Name.c_str(), name) == 0)
				{
					m_TeamDate[i][j]->isTeamLeader = true;
					m_TeamLeader = m_TeamDate[i][j]->Guid;
					m_scTeamLeader = name;
					SortIndex0 = i ;
					if (m_TeamDate[i][j]->Guid == ObjectMgr->GetLocalPlayer()->GetGUID())
					{
						if (m_bTD)
						{
							TeamSysNotify( _TRAN("您已成为团长") );
							
						}else
						{
							TeamSysNotify(_TRAN("您已成为队长") );
						}
						m_TeamFrame->SetTeamBtnActive(m_bTD, TRUE);
						
					}else
					{
						m_TeamFrame->SetTeamBtnActive(m_bTD, FALSE);
						m_TeamFrame->SetReadyFlag(false);
						if (m_bTD)
						{
							std::string str = _TRAN( N_NOTICE_C64, name );
							TeamSysNotify(str.c_str()/*"[%s]成为团长", name*/);

						}else
						{
							std::string str = _TRAN( N_NOTICE_C65, name );
							TeamSysNotify(str.c_str()/*"[%s]成为队长", name*/);
						}
						
					}
					TeamShow->SetTeamLeaderGuid(m_TeamDate[i][j]->Guid);
				}else
				{
					if (m_TeamDate[i][j]->isTeamLeader)
					{
						SortIndex1 = i ;
						m_TeamDate[i][j]->isTeamLeader = false;
					}
				}
			}
		}
	}

	if (m_bAutoSort)
	{
		SortDateByIndex(SortIndex0);
		SortDateByIndex(SortIndex1);
		if (SortIndex0 == m_localSub || SortIndex1 == m_localSub)
		{
			UpdateLocalSubGroup();
		}
	}

	
	return TRUE;
}
BOOL Teamsys::CreateFrame(UControl* ctrl)
{
	UControl* NewCtrl = NULL;
	for (int i = 0; i < 6; i++)
	{
		if (m_SubGroupShow[i] == NULL)
		{
			UControl* pkCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\SubGroupShow.udg");
			m_SubGroupShow[i] = UDynamicCast(USubGroupShow,pkCtrl);
			if (m_SubGroupShow[i])
			{
				ctrl->AddChild(m_SubGroupShow[i]);
				m_SubGroupShow[i]->SetId(FRAME_IG_TEAM_SUBDLG_0 + i);
				UPoint Pos = m_SubGroupShow[i]->GetWindowPos();
				UPoint Size = m_SubGroupShow[i]->GetWindowSize();

				m_SubGroupShow[i]->SetPosition(Pos + UPoint( Size.x * (i / 2), Size.y * (i % 2)));
			}else
			{
				return FALSE;
			}
		}
	}

	if (m_TeamFrame == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\UTeamFrame.udg");
		m_TeamFrame = UDynamicCast(UTeamFrame, NewCtrl);
		if (m_TeamFrame)
		{
			ctrl->AddChild(m_TeamFrame);
			m_TeamFrame->SetId(FRAME_IG_TEAMDLG);
		}
		else
		{
			return FALSE;
		}
	}
	return TRUE;
}
void Teamsys::DestoryFrame()
{
	for (int i =0; i < 6 ; i++)
	{
		for (int j =0; j < 5; j++)
		{
			if(m_TeamDate[i][j])
			{
				m_TeamDate[i][j]->ResetDate();
			}
		}
	}
	for (int i = 0 ; i < TATGET_ICON_MAX ; i++)
	{
		m_TargetIcon[i] = 0;
	}
	m_TeamLeader = 0;
	m_scTeamLeader.clear();
}
void Teamsys::ShowFrame()
{
	m_TeamFrame->Show();
}
void Teamsys::HideFrame()
{
	m_TeamFrame->Hide();
}
void Teamsys::GetTeamMemPosition(ui32 mapid, std::vector<NiPoint3> &vec)
{
	for(int i = 0; i < 6 ; i++)
	{
		for (int j = 0; j < 5 ; j++)
		{
			if (m_TeamDate[i][j] && m_TeamDate[i][j]->Guid)
			{
				ui64 guid = m_TeamDate[i][j]->Guid;
				CPlayer* pkchar = ObjectMgr->GetPlayer(guid);
				if (!pkchar && m_TeamDate[i][j]->mapid == mapid)
				{
					NiPoint3 pos = m_TeamDate[i][j]->pos;
					vec.push_back(pos);
				}else if(pkchar && !pkchar->IsLocalPlayer())
				{
					m_TeamDate[i][j]->pos = pkchar->GetPosition();
					m_TeamDate[i][j]->mapid = mapid;
					vec.push_back(m_TeamDate[i][j]->pos);
				}
			}
		}
	}
}
void Teamsys::GetTeamMemByMapid(ui32 mapid, std::vector<TeamDate*> &vec)
{
	for(int i = 0; i < 6 ; i++)
	{
		for (int j = 0; j < 5 ; j++)
		{
			if (m_TeamDate[i][j] && m_TeamDate[i][j]->Guid)
			{
				ui64 guid = m_TeamDate[i][j]->Guid;
				CPlayer* pkchar = ObjectMgr->GetPlayer(guid);
				if (!pkchar && m_TeamDate[i][j]->mapid == mapid)
				{
					vec.push_back(m_TeamDate[i][j]);
				}else if(pkchar && !pkchar->IsLocalPlayer())
				{
					m_TeamDate[i][j]->pos = pkchar->GetPosition();
					m_TeamDate[i][j]->mapid = mapid;
					vec.push_back(m_TeamDate[i][j]);
				}
			}
		}
	}
}
void Teamsys::GetTeamMemMapID(std::vector<ui32> &vec)
{
	for(int i = 0; i < 6 ; i++)
	{
		for (int j = 0; j < 5 ; j++)
		{
			if (m_TeamDate[i][j] && m_TeamDate[i][j]->Guid)
			{
				ui64 guid = m_TeamDate[i][j]->Guid;
				CPlayer* pkchar = ObjectMgr->GetPlayer(guid);
				if (!pkchar)
				{
					ui16 mapid = m_TeamDate[i][j]->mapid;
					vec.push_back(mapid);
				}else if(!pkchar->IsLocalPlayer())
				{
					vec.push_back(CMap::Get()->GetMapId());
				}
			}
		}
	}
}
void Teamsys::GetTeamMemGUID(std::vector<ui64>& vec)
{
	for(int i = 0; i < 6 ; i++)
	{
		for (int j = 0; j < 5 ; j++)
		{
			if(m_TeamDate[i][j]->Guid)
				vec.push_back(m_TeamDate[i][j]->Guid);
		}
	}
}
ui8 Teamsys::GetMemberReadyFlag()
{
	if (!m_TeamFrame)
	{
		return 0;
	}
	return m_TeamFrame->m_readyflag;
}
void Teamsys::ChangeSortModel()
{
	m_bAutoSort = !m_bAutoSort;
	if (m_bAutoSort)
	{
		SortAllTeamDate();
	}
}
void Teamsys::SortAllTeamDate()
{
	if (m_bTD)
	{
		for (int  i = 0; i < 6 ; i ++)
		{
			SortDateByIndex(i , TRUE);
		}

		UpdateLocalSubGroup();
	}
}
void Teamsys::SetUIByCurDate(TeamDate* pkDate)
{
	m_TeamFrame->SetCurSel(pkDate);
	m_SubGroupShow[0]->SetCurSel(pkDate);
	m_SubGroupShow[1]->SetCurSel(pkDate);
	m_SubGroupShow[2]->SetCurSel(pkDate);
	m_SubGroupShow[3]->SetCurSel(pkDate);
	m_SubGroupShow[4]->SetCurSel(pkDate);
	m_SubGroupShow[5]->SetCurSel(pkDate);
}
void Teamsys::SetLockSubGroupShowBGK(BOOL bshow)
{
	for (int i = 0; i < 6 ; i++)
	{
		m_SubGroupShow[i]->SetLockBgk(bshow);
	}
}
void Teamsys::SetSubGroupShow(UINT index , BOOL bCheck)
{
	if (index >= 0 && index < 6)
	{
		m_SubGroupShow[index]->SetVisible(bCheck);
	}
}
void Teamsys::SetCurSel(TeamDate* pkDate)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (pkLocal)
	{
		if(!pkLocal->IsHook())
			SYState()->LocalPlayerInput->SetTargetID(pkDate->Guid);
	}
}
void Teamsys::GetProperty(ui64 guid, std::string* Name, ui32* Class, ui32* Race)
{
	for(int i = 0; i < 6 ; i++)
	{
		for (int j = 0; j < 5 ; j++)
		{
			if(m_TeamDate[i][j]->Guid == guid)
			{
				if (Name)
				{
					*Name = m_TeamDate[i][j]->Name;
				}
				if (Class)
				{
					*Class = m_TeamDate[i][j]->Class;
				}
				if (Race)
				{
					*Race = m_TeamDate[i][j]->Race;
				}
			}
		}
	}

}

BOOL Teamsys::SortDateByIndex(int index, BOOL bSort)
{
	if (index >= 0 && index < 6)
	{
		std::vector<TeamDate> vTem ;
		vTem.clear();
		for (int i = 0; i < 5; i++)
		{
			if (m_TeamDate[index][i]->Guid != 0)
			{
				TeamDate sTem ; 
				sTem = *m_TeamDate[index][i];
				vTem.push_back(sTem);
			}	
		}

		if (bSort || m_bAutoSort)
		{
			std::sort(vTem.begin(), vTem.end(), TeamDate::Sort);
		}

		BOOL bActiveShowTeam = (vTem.size() > 0) ;
		for (int i = 0; i < 5; i ++)
		{
			if (i < vTem.size())
			{
				*m_TeamDate[index][i] = vTem[i] ;	
			}else
			{
				m_TeamDate[index][i]->ResetDate();
			}
		}
		
		TeamDate* p[5];
		p[0] = m_TeamDate[index][0];
		p[1] = m_TeamDate[index][1];
		p[2] = m_TeamDate[index][2];
		p[3] = m_TeamDate[index][3];
		p[4] = m_TeamDate[index][4];

		if (m_bTD)
		{
			m_SubGroupShow[index]->UpdateUI();
			m_TeamFrame->SetActiveShowTeam(index, bActiveShowTeam);
		}

		TeamShow->ResetTargetDate();
		
		return TRUE ;
	}
	return FALSE ;
}
void Teamsys::SetGuildActive(bool active)
{
	if (m_TeamFrame)
	{
		m_TeamFrame->SetGuildBtnActive(active);
	}
}

//MSG
bool Teamsys::SendLeaveTeam()
{
	return PacketBuilder->SendGroupDisbandMsg();
}
bool Teamsys::SendChangeTeamLeader(ui64 newLeader)
{	
	return PacketBuilder->SendReSetGroupLeaderMsg(newLeader);
}
//取消ICON的时候 ICON 大于等于TATGET_ICON_MAX
bool Teamsys::SendChangeMemberIcon(ui64 guid, ui8 icon)
{
	if (!guid)
	{
		return false;
	}
	if (icon >= TATGET_ICON_MAX)
	{
		for (int i = 0; i < TATGET_ICON_MAX; i++)
		{
			if (m_TargetIcon[i] == guid)
			{
				return  PacketBuilder->SendChangeMemberIcon(0,  i);
			}
		}

		return false ;
	}
	return PacketBuilder->SendChangeMemberIcon(guid, icon);
}
bool Teamsys::SendRemoveMember(ui64 guid,const char* name)
{
	if (guid)
	{
		PacketBuilder->SendRemoveMemberMsg(guid);
	}
	else
	{
		if (name)
		{
			PacketBuilder->SendRemoveMemberMsg(name);
		}
	}

	return false;
}

bool Teamsys::SendAddMember(const char* membername)
{
	if (membername)
	{
		return PacketBuilder->SendGroupInviteMsg(membername);
	}

	return false;
}
bool Teamsys::SendChangeToTeam()
{
	return PacketBuilder->SendChangeToTeam();
}
bool Teamsys::SendChangeMemberSubGruopByGuid(ui64 desguid, ui64 srcguid)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return false ;
	}
	if (m_TeamLeader != pkLocal->GetGUID())
	{
		return false ;
	}
	return PacketBuilder->SendChangeMemberSubGruopByGuid(desguid, srcguid);
}
bool Teamsys::SendChangeMemberSubGroup(const char* name, ui8 groupid)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return false ;
	}
	if (m_TeamLeader != pkLocal->GetGUID())
	{
		return false ;
	}
	return PacketBuilder->SendChangeMemberSubGroup(name, groupid);
}
bool Teamsys::SendMemberReady(ui8 ready)
{
	CPlayerLocal* pkLocal = ObjectMgr->GetLocalPlayer();
	if (!pkLocal)
	{
		return false ;
	}
	if (m_TeamLeader != pkLocal->GetGUID())
	{
		return false ;
	}

	m_TeamFrame->SetReadyFlag(true);
	return PacketBuilder->SendMemberReady(ready);
}



ApplyGroupSystem::ApplyGroupSystem()
{
	m_GroupApplyDlg = NULL;
	m_GroupApplyIntro = NULL;
	m_GroupApplyList = NULL;
	m_bSelectItem = NULL;
	m_CurrentPage = 0;
	m_MaxPage = 0;
}
ApplyGroupSystem::~ApplyGroupSystem()
{

}
BOOL ApplyGroupSystem::CreateFrame(UControl* ctrl)
{
	UControl* NewCtrl = NULL;
	if (m_GroupApplyDlg == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\UGroupApplyDlg.udg");
		m_GroupApplyDlg = UDynamicCast(UGroupApplyFrame, NewCtrl);
		if (m_GroupApplyDlg)
		{
			ctrl->AddChild(m_GroupApplyDlg);
			m_GroupApplyDlg->SetId(FRAME_IG_GROUPAPPLYDLG);
		}
		else
		{
			return FALSE;
		}
	}

	if (m_GroupApplyIntro == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\GroupApplyIntro.udg");
		m_GroupApplyIntro = UDynamicCast(UGroupApplyIntro, NewCtrl);
		if (m_GroupApplyDlg)
		{
			ctrl->AddChild(m_GroupApplyIntro);
			m_GroupApplyIntro->SetId(FRAME_IG_GROUPAPPLYINTRODLG);
		}
		else
		{
			return FALSE;
		}
	}

	if (m_GroupApplyList == NULL)
	{
		NewCtrl = UControl::sm_System->CreateDialogFromFile("SocialitySystem\\ApplyGroupListDlg.udg");
		m_GroupApplyList = UDynamicCast(UApplyGroupListDlg, NewCtrl);
		if (m_GroupApplyDlg)
		{
			ctrl->AddChild(m_GroupApplyList);
			m_GroupApplyList->SetId(FRAME_IG_GROUPAPPLYLISTDLG);
		}
		else
		{
			return FALSE;
		}
	}
	return TRUE;
}

void ApplyGroupSystem::DestoryFrame()
{

}

void ApplyGroupSystem::ShowFrame()
{
	m_GroupApplyDlg->Show();
	SendGroupApplyForJoiningStateReq();
}

void ApplyGroupSystem::HideFrame()
{
	m_GroupApplyDlg->Hide();
}

void ApplyGroupSystem::SetGuildActive(bool active)
{
	m_GroupApplyDlg->SetGuildBtnActive(active);
}



bool ApplyGroupSystem::PrevPage()
{
	if (m_CurrentPage > 0)
	{
		--m_CurrentPage;
		return m_CurrentPage != 0;
	}
	return false;
}

bool ApplyGroupSystem::NextPage()
{
	if (m_CurrentPage < m_MaxPage)
	{
		++m_CurrentPage;
		return m_CurrentPage != m_MaxPage;
	}
	return false;
}

bool ApplyGroupSystem::GetPage(int& CurPage, int& MaxPage)
{
	CurPage = m_CurrentPage;
	MaxPage = m_MaxPage;
	return true;
}
void ApplyGroupSystem::SortGroupByName()
{

}
void ApplyGroupSystem::SortGroupByLevel()
{

}
void ApplyGroupSystem::SortGroupByNum()
{

}
void ApplyGroupSystem::SortGroupByState()
{

}
void ApplyGroupSystem::SortGroupByIntro()
{

}
bool ApplyGroupSystem::GetGroupList(std::vector<MSG_S2C::stGroupListAck::GroupFindInfo>& vList)
{
	vList = m_GroupList;
	return vList.size() > 0;
}

void ApplyGroupSystem::SortMemberByName()
{

}
void ApplyGroupSystem::SortMemberByGender()
{

}
void ApplyGroupSystem::SortMemberByLevel()
{

}
void ApplyGroupSystem::SortMemberByClass()
{

}
bool ApplyGroupSystem::GetApplyGroupList(std::vector<stApplyGroupMem>& vList)
{
	if (m_ApplyGroupList.size())
	{
		vList = m_ApplyGroupList;
		return true;
	}
	return false;
}

void ApplyGroupSystem::ParseGroupApplyForJoiningAck( std::string& name, uint8 type )
{
	if(type == 2)
	{
		char buf[256];
		sprintf(buf, "%s %s..", name.c_str(), _TRAN("拒绝了你的组队申请"));
		ClientSystemNotify(buf);
	}
}

void ApplyGroupSystem::ParseGroupModifyTitleAck(uint8 type)
{
	if (m_GroupApplyDlg->IsVisible())
	{
		m_GroupApplyDlg->OnQueryAreaChange();
	}
}

void ApplyGroupSystem::ParseGroupModifyCanApplyForJoiningAck(uint8 type)
{
	if(type == 0)
		m_GroupApplyDlg->SetApplyForJoiningState(true);
	else if(type == 1)
		m_GroupApplyDlg->SetApplyForJoiningState(false);
	else if(type == 2)
	{}
	else if(type == 3)
	{}

	if (m_GroupApplyDlg->IsVisible())
	{
		m_GroupApplyDlg->OnQueryAreaChange();
	}
}

void ApplyGroupSystem::ParsestGroupApplyForJoiningStateAck(uint8 bOpen)
{
	m_GroupApplyDlg->SetApplyForJoiningState(bOpen);
}

void ApplyGroupSystem::ParseGroupListAck(std::vector<MSG_S2C::stGroupListAck::GroupFindInfo>& vList)
{
	m_GroupList = vList;
	
	m_MaxPage = m_GroupList.size() / 20;
	if (m_GroupList.size() % 20 == 0)
	{
		--m_MaxPage;
	}
	m_GroupApplyDlg->GetUGroupApplyList()->UpdataList();
}

void ApplyGroupSystem::ParseGroupApplyForJoiningResult(std::string& name, uint8 type)
{
	char buf[256];
	DeleteFromApplyList(name.c_str());
	if (type == 0)
	{
		sprintf(buf, "%s %s.", name.c_str(), _TRAN("成功的加入队伍"));
	}else if (type == 1)
	{
		sprintf(buf, "%s %s.", name.c_str(), _TRAN("已经有队伍了"));
	}else if (type == 2)
	{
		sprintf(buf, "%s", _TRAN("你的队伍已满."));
	}else if (type == 3)
	{
		sprintf(buf, "%s", _TRAN("你不是队长."));
	}
	ClientSystemNotify(buf);
}

void ApplyGroupSystem::ParseGroupApplyForJoining(std::string& name, uint8 ClassMask, uint16 Level, uint8 Gender)
{
	stApplyGroupMem stTemp;
	for(unsigned int ui = 0 ; ui < m_ApplyGroupList.size() ; ++ui)
	{
		if(m_ApplyGroupList[ui].name.compare(name) == 0)
		{
			m_ApplyGroupList[ui].classMask = ClassMask;
			m_ApplyGroupList[ui].Level = Level;
			m_ApplyGroupList[ui].Gender = Gender;
			m_GroupApplyList->UpdateList();
			return;
		}
	}
	stTemp.name = name;
	stTemp.classMask = ClassMask;
	stTemp.Level = Level;
	stTemp.Gender = Gender;
	m_ApplyGroupList.push_back(stTemp);
	m_GroupApplyList->UpdateList();

	UMinimapFrame* pMinimapFrame = INGAMEGETFRAME(UMinimapFrame, FRAME_IG_MINIMAP);
	if (pMinimapFrame && pMinimapFrame->GetChildByID(16) && !pMinimapFrame->GetChildByID(16)->IsVisible())
	{
		pMinimapFrame->SetApplyGroupVisible(true);
	}
}

void ApplyGroupSystem::SendGroupApplyForJoining(ui64 guid)
{
	PacketBuilder->SendGroupApplyForJoining(guid);
}

void ApplyGroupSystem::SendGroupApplyForJoiningAck(std::string name, ui8 type)
{
	PacketBuilder->SendGroupApplyForJoiningAck(name, type);
	if (!type)
		DeleteFromApplyList(name.c_str());
}

void ApplyGroupSystem::SendGroupListReq(uint64 mapid)
{
	PacketBuilder->SendGroupListReq(mapid);
}

void ApplyGroupSystem::SendGroupModifyTitleReq(std::string& Title)
{
	PacketBuilder->SendGroupModifyTitleReq(Title);
}

void ApplyGroupSystem::SendGroupModifyCanApplyForJoining(bool bOpen)
{
	PacketBuilder->SendGroupModifyCanApplyForJoining(bOpen);
}

void ApplyGroupSystem::SendGroupApplyForJoiningStateReq()
{
	PacketBuilder->SendGroupApplyForJoiningStateReq();
}

void ApplyGroupSystem::CallGroupApplyIntro()
{
	m_GroupApplyIntro->SetVisible(TRUE);
}

void ApplyGroupSystem::CallApplyGroupDlg()
{
	m_GroupApplyList->SetVisible(TRUE);
}

void ApplyGroupSystem::DeleteFromApplyList(const char* name)
{
	std::vector<stApplyGroupMem>::iterator it = m_ApplyGroupList.begin();
	while(it != m_ApplyGroupList.end())
	{
		if ( it->name.compare(name) == 0 )
		{
			it = m_ApplyGroupList.erase(it);


			UMinimapFrame* pMinimapFrame = INGAMEGETFRAME(UMinimapFrame, FRAME_IG_MINIMAP);
			if (pMinimapFrame && m_ApplyGroupList.size() == 0)
			{
				pMinimapFrame->SetApplyGroupVisible(false);
				m_GroupApplyList->SetVisible(FALSE);
			}
			m_GroupApplyList->UpdateList();
			return;
		}
	}
}