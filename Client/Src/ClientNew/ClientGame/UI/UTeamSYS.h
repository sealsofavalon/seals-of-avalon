#pragma once
#include "UTeamFrame.h"
#include "USubGroupShow.h"
#include "UTeamDate.h"
enum
{
	TATGET_ICON_MAX = 8 ,
};
// 团队管理
class Teamsys  
{
public:
	Teamsys();
	~Teamsys();
public:
	//UI相关
	BOOL CreateFrame(UControl* ctrl);
	void DestoryFrame();
	void ShowFrame();
	void HideFrame();
	void SetGuildActive(bool active);


	void GetTeamMemPosition(ui32 mapid, std::vector<NiPoint3> &vec);
	void GetTeamMemByMapid(ui32 mapid, std::vector<TeamDate*> &vec);
	void GetTeamMemMapID(std::vector<ui32> &vec);
	void GetTeamMemGUID(std::vector<ui64>& vec);
	void GetTeamMemByTeam(int teamid, std::vector<TeamDate>& vec);

	void GetProperty(ui64 guid, std::string* Name, ui32* Class, ui32* Race = NULL);
	void SetSubGroupShow(UINT index, BOOL bCheck);
	void SetLockSubGroupShowBGK(BOOL bshow);
	void SetCurSel(TeamDate* pkDate);
	void SetUIByCurDate(TeamDate* pkDate);

	void SortAllTeamDate();
	void ChangeSortModel();
	ui8 GetMemberReadyFlag();

public:
	//C2S MSG
	bool SendLeaveTeam();
	bool SendChangeTeamLeader(ui64 newLeader);   //更换团长
	bool SendChangeMemberIcon(ui64 guid, ui8 icon);  //更换标记   //取消ICON的时候 ICON 大于等于TATGET_ICON_MAX
	bool SendRemoveMember(ui64 guid,const char* name); //踢人
	bool SendAddMember(const char* membername);
	bool SendChangeToTeam(); //小队队长转化为团
	bool SendChangeMemberSubGroup(const char* name, ui8 groupid); //添加到新的组队
	bool SendChangeMemberSubGruopByGuid(ui64 desguid, ui64 srcguid);  //互换2个位置
	bool SendMemberReady(ui8 ready); // 如果是队长 标识询问所有队员是否READY /  0 团长询问， 1 OK ，2NOT
protected:
	BOOL SortDateByIndex(int index , BOOL bSort = FALSE);
public:
	//数据相关
	BOOL AddSubGroupInfo(MSG_S2C::stGroupList::stSubGroup subGroup, UINT id, ui64 Leader);
	TeamDate* GetTeamDate(ui64 guid);
	BOOL GetSortIndex(ui64 guid, UINT* index);
	BOOL InTeam(ui64 guid);
	BOOL IsTuanDui(){return m_bTD;}
	BOOL IsTeamLeader(ui64 guid){return (m_TeamLeader == guid) ;}
	BOOL IsTeamLeader(const char* name){return m_scTeamLeader.compare(name) == 0;}
	BOOL GetTargetIcon(UINT* index, ui64 guid, int uniqueId);
	BOOL GetTargetIcon(UINT* index, ui64 guid);
	void UpdateLocalSubGroup(); // 更新本地玩家所在的小队
	void UpdateTeamGroupDate(ui64 guid, ui32 mask);
	static void SendReadyOK();
	static void SendNotReady();
	//TODO NETWORK
	static void SendAcceptGroupInvite();
	static void SendRefuseGroupInvite();


	//S2C MSG
	BOOL DestroyTeamInfo();//团队解散。数据重置
	void UpdateGroupStat(ui64 guid, ui32 mask, const std::string &data); // 更新血量等等状态
	void MemberTeamChange(ui64 guid,ui8 team);  //团队位置更换
	void MemberTeamSwep(ui64 desguid, ui64 srcGuid); //团队队员互换位置
	void RemoveMember(ui64 guid); //踢出玩家离队
	void AddMember(MSG_S2C::stGroupAddMember::stMember newMember, ui8 team); //添加玩家进队
	void ErrorText(string & name , int res);//操作出错提示 
	BOOL AddGroupList(const vector<MSG_S2C::stGroupList::stSubGroup> &pkTeamDate, ui64 Leader, ui32 subGroupCount); //队伍初始列表
	BOOL MemberReadyChange(ui64 guid ,ui8 ready); //询问准备
	BOOL LeaderChange(const char* name);  //队长变换
	BOOL MemberIconChange(MSG_S2C::stGroupSetPlayerIcon teamIcon); //团员标识改编
	
	void TeamSysNotify(const char *s, ...);
protected:
	TeamDate* m_TeamDate[6][5];
	ui64 m_TargetIcon[TATGET_ICON_MAX];
	int m_TargetIconUniqueID[TATGET_ICON_MAX];
	BOOL m_bTD; //是否是团队 
	UINT m_localSub; //本地玩家所在小队的序列、
	ui64 m_TeamLeader;
	std::string m_scTeamLeader;
	BOOL m_bAutoSort ;
	UTeamFrame* m_TeamFrame;
	USubGroupShow* m_SubGroupShow[6];
};

struct stApplyGroupMem
{
	string name;
	uint8 classMask;
	uint16 Level;
	uint8 Gender;
};

class ApplyGroupSystem
{
public:
	ApplyGroupSystem();
	~ApplyGroupSystem();
public:
	//UI相关
	BOOL CreateFrame(UControl* ctrl);
	void DestoryFrame();
	void ShowFrame();
	void HideFrame();
	void SetGuildActive(bool active);
	void SetSelectItem(class UGroupApplyItem* item);
	class UGroupApplyItem* GetSelectItem(){return m_bSelectItem;};

	void CallGroupApplyIntro();
	void CallApplyGroupDlg();
	void DeleteFromApplyList(const char* name);
public:
	void ParseGroupApplyForJoiningAck( std::string& name, uint8 type );
	void ParseGroupModifyTitleAck(uint8 type);
	void ParseGroupModifyCanApplyForJoiningAck(uint8 type);
	void ParseGroupListAck(std::vector<MSG_S2C::stGroupListAck::GroupFindInfo>& vList);
	void ParseGroupApplyForJoiningResult(std::string& name, uint8 type);
	void ParseGroupApplyForJoining(std::string& name, uint8 ClassMask, uint16 Level, uint8 Gender);
	void ParsestGroupApplyForJoiningStateAck(uint8 bOpen);

	void SendGroupApplyForJoining(ui64 guid);
	void SendGroupApplyForJoiningAck(std::string name, ui8 type);
	void SendGroupListReq(uint64 mapid);
	void SendGroupModifyTitleReq(std::string& Title);
	void SendGroupModifyCanApplyForJoining(bool bOpen);
	void SendGroupApplyForJoiningStateReq();
public:
	bool PrevPage();
	bool NextPage();
	bool GetPage(int& CurPage, int& MaxPage);
public:
	void SortMemberByName();
	void SortMemberByGender();
	void SortMemberByLevel();
	void SortMemberByClass();
	bool GetApplyGroupList(std::vector<stApplyGroupMem>& vList);
public:
	void SortGroupByName();
	void SortGroupByLevel();
	void SortGroupByNum();
	void SortGroupByState();
	void SortGroupByIntro();
	bool GetGroupList(std::vector<MSG_S2C::stGroupListAck::GroupFindInfo>& vList);
protected:
	class UGroupApplyItem* m_bSelectItem;
	UGroupApplyIntro* m_GroupApplyIntro;
	UGroupApplyFrame* m_GroupApplyDlg;
	UApplyGroupListDlg* m_GroupApplyList;
	std::vector<MSG_S2C::stGroupListAck::GroupFindInfo> m_GroupList;
	std::vector<stApplyGroupMem> m_ApplyGroupList;
	
	int m_CurrentPage;
	int m_MaxPage;
};