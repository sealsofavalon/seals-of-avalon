#include "StdAfx.h"
#include "UTip.h"
#include "UITipSystem.h"

UIMP_CLASS(UTip,UControl);

void UTip::StaticInit()
{

}
UTip::UTip()
{
	m_CurType = TIP_TYPE_MAX;
	m_IsUpdateChild = FALSE;
}
UTip::~UTip()
{

}
BOOL UTip::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	SetActive(FALSE);
	return TRUE;
}
void UTip::OnDestroy()
{
	return UControl::OnDestroy();
}
void UTip::OnRender(const UPoint& offset, const URect &updateRect)
{
	if (m_CurType == TIP_TYPE_MAX)
	{
		return UControl::OnRender(offset,updateRect);
	}
	MouseTip * mt = TipSystem->GetTip(m_CurType);
	if (mt)
	{
		mt->DrawTip(this,sm_UiRender,m_Style,offset,updateRect);
		if (m_CurType == TIP_TYPE_INSTANCE)
		{
			RenderChildWindow(offset, updateRect);
		}
		//这样把每个ui的渲染都下放到各种鼠标提示中分开实现有助于修改,
		//而且渲染所需要的东西不会出现重复现象，但是对于有美术资源的渲染这样还可行么？
		//美术资源中的一些控件功能又该如何去实现暂时没有考虑，不过后面会加入
	}
	//TipSystem->FitTipInWindow();
	//一般有自己的渲染情况下拒绝UControl的渲染模式
	//return UControl::OnRender(offset,updateRect);
}
void UTip::SetType(int type)
{
	m_CurType = type;
	if(m_CurType == TIP_TYPE_MONSTER || 
		m_CurType == TIP_TYPE_SCENEGAMEOBJ)
	{
		m_Size.x = 140;
	}else if (m_CurType == TIP_TYPE_STRINGLIST)
	{
		m_Size.x = 100;
	}else
	{
		m_Size.x = 195;
	}
}
BOOL UTip::HitTest(const UPoint& parentCoordPoint)
{
	return FALSE;
}