//-----------------------------------------------------------
//鼠标提示的UI 固定宽度的UI长度随文本长度变化
//------------------------------------------------------------
#ifndef UTIP_H
#define UTIP_H
#include "UInc.h"
//这个是鼠标提示系统中的UI总控件
//暂时想的是所有的UI只创建这一个，只是需要更改其内部渲染以及控件形式即可
//即这个只是个框架存在,提供整体格式以及ui指针
class UTip : public UControl
{
	UDEC_CLASS(UTip);
	UTip();
	~UTip();
public:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
public:
	void SetType(int type); 
protected:
	virtual BOOL HitTest(const UPoint& parentCoordPoint);
private:
	int m_CurType;
};
#endif