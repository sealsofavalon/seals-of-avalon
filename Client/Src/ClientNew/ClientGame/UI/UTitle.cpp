#include "stdafx.h"
#include "UTitle.h"
#include "TitleMgr.h"
#include "Utils/CPlayerTitleDB.h"
#include "Utils/ItemEnchant.h"
#include "UIGamePlay.h"
#include "UMessageBox.h"

UIMP_CLASS(UTitle, UDialog)
UBEGIN_MESSAGE_MAP(UTitle, UDialog)
UON_UGD_CLICKED(1, &UTitle::OnClickTitle)
UON_BN_CLICKED(4,&UTitle::OnClickUseTitle)
UON_BN_CLICKED(5,&UTitle::OnClickHideTitle)
UEND_MESSAGE_MAP()

void UTitle::StaticInit()
{

}
UTitle::UTitle()
{
	m_TitleList = NULL;
	m_SelTitleDis = NULL;
	m_CurTitleDis = NULL;

	m_bCanDrag = FALSE ;
	m_pkcurTitle = 0;
}
UTitle::~UTitle()
{
	m_pkcurTitle = 0;
}

BOOL UTitle::OnCreate()
{
	if (!UDialog::OnCreate())
	{
		return FALSE;
	}

	UScrollBar* pkBar = UDynamicCast(UScrollBar, GetChildByID(0));
	if (!pkBar)
	{
		return FALSE;
	}
	pkBar->SetBarShowMode(USBSM_FORCEON,USBSM_FORCEOFF);
	m_TitleList = UDynamicCast(USpecListBox, pkBar->GetChildByID(1));
	m_TitleList->SetGridSize(UPoint(146 , 22));
	m_TitleList->SetGridCount(16, 1);
	m_TitleList->SetCol1(20, 2);
	m_TitleList->SetWidthCol1(146);
	if (!m_TitleList)
	{
		return FALSE;
	}

	m_SelTitleDis = UDynamicCast(URichTextCtrl, GetChildByID(2));
	m_CurTitleDis = UDynamicCast(URichTextCtrl, GetChildByID(3));
	if (!m_SelTitleDis || !m_CurTitleDis)
	{
		return FALSE;
	}
	m_CurTitleDis->ClearText();

	m_OldPos = m_Position ;
	m_pkcurTitle = 0;
	if (!GetChildByID(5))
	{
		return FALSE;
	}

	GetChildByID(5)->SetActive(FALSE);
	return TRUE;
}
void UTitle::OnDestroy()
{
	m_TitleList->Clear();
	m_CurTitleDis->ClearText();
	m_SelTitleDis->ClearText();
	UDialog::OnDestroy();
}
void UTitle::OnClose()
{
	SetVisible(FALSE);
}
BOOL UTitle::OnEscape()
{
	OnClose();
	return UControl::OnEscape();
}

void UTitle::AddTitle(ui32 titleid)
{
	TitleInfo* pkTitleInfo = g_pkPlayerTitle->GetTitleInfo(titleid);
	if (pkTitleInfo)
	{
		char buf[_MAX_PATH];
		sprintf_s(buf, _MAX_PATH, "<color:R060G106B217A255><Col1><center>%s<endnc>", pkTitleInfo->stTitle.c_str());
		m_TitleList->InsertItem(0,buf, (void*)&pkTitleInfo->id);
	}
}
void UTitle::RemoveTitle(ui32 titleid)
{
	m_TitleList->DeleteItemById(titleid);
}
void UTitle::ClearTitle()
{
	m_TitleList->Clear();
}
void UTitle::SetVisible(BOOL value)
{
	/*UControl* pkCtrl = UInGame::GetFrame(FRAME_IG_PLAYEREQUIPDLG);
	UControl* pkCtrlView = UInGame::GetFrame(FRAME_IG_VIEWPLAYEREQUIPDLG);

	if (value)
	{
		UPoint NewPos = m_OldPos;
		if (pkCtrl->IsVisible())
		{
			NewPos.x += pkCtrl->GetWidth() ; 
		}
		if (pkCtrlView->IsVisible())
		{
			NewPos.x += pkCtrlView->GetWidth();
		}
		SetPosition(NewPos);
	}*/

	UDialog::SetVisible(value);
}
void UTitle::ShowCurTitleDis()
{
	ShowCurTitleDis(m_pkcurTitle);
}
void UTitle::ShowCurTitleDis(ui32 titleid)
{
	m_CurTitleDis->ClearText();
	m_pkcurTitle = titleid ;
	if (titleid == 0)
	{
		m_TitleList->ClearSel();
		return ;
	}
	m_TitleList->SetCurSelByID(titleid);

	TitleInfo* pkTitleInfo = g_pkPlayerTitle->GetTitleInfo(titleid);
	if (pkTitleInfo)
	{
		std::string strbuf;
		if (pkTitleInfo->stProperty)
		{
			ItemEnchantProto pkItemEnchantProto ;
			if (g_pkItemEnchantDB->GetEnchantProperty(pkTitleInfo->stProperty, pkItemEnchantProto))
			{
				 strbuf = _TRAN( N_NOTICE_C66, pkTitleInfo->stTitle.c_str(), pkItemEnchantProto.sName.c_str() );
				//sprintf_s(buf,_MAX_PATH, strbuf.c_str()/*"<font:Arial:14><color:415194><center>当前称号:%s<br><font:Arial:13><color:20C301><left>效果:%s<br>", pkTitleInfo->stTitle.c_str(),pkItemEnchantProto.sName.c_str()*/);
				
			}
		}else
		{
			 strbuf = _TRAN( N_NOTICE_C67, pkTitleInfo->stTitle.c_str() );
			//sprintf_s(buf,_MAX_PATH,strbuf.c_str()/*"<font:Arial:14><color:415194><center>当前称号:%s<br>", pkTitleInfo->stTitle.c_str()*/);
		}

		m_CurTitleDis->SetText(strbuf.c_str(), strbuf.length());
	}
}
void UTitle::OnClickUseTitle()
{
	int32 Titleid = m_TitleList->GetSelId();
	if (Titleid > 0 )
	{
		if (Titleid != m_pkcurTitle)
		{
			TitleMgr->SendChangTitleMsg(Titleid);
		}
	}
}
void UTitle::SendRemoveTitle()
{
	UTitle* pkCtrl = INGAMEGETFRAME(UTitle,FRAME_IG_TITLEDLG);
	if (pkCtrl)
	{
		int32 Titleid = pkCtrl->m_TitleList->GetSelId();
		if (Titleid > 0)
		{
			TitleMgr->RemoveTitle(Titleid);
		}
	}
}
void UTitle::OnClickRemoveTitle()
{
	UMessageBox::MsgBox( _TRAN("您确认要删除当前称谓么？"), (BoxFunction*)UTitle::SendRemoveTitle);
}
void UTitle::OnClickHideTitle()
{
	TitleMgr->SendChangTitleMsg(0);
	GetChildByID(5)->SetActive(FALSE);
}

void UTitle::OnClickTitle()
{
	int32 Titleid = m_TitleList->GetSelId();
	if (Titleid <= 0)
	{
		return ;
	}
	TitleInfo* pkTitleInfo = g_pkPlayerTitle->GetTitleInfo(Titleid);
	if (pkTitleInfo)
	{
		std::string strbuf;
		if (pkTitleInfo->stProperty)
		{
			ItemEnchantProto pkItemEnchantProto ;
			if (g_pkItemEnchantDB->GetEnchantProperty(pkTitleInfo->stProperty, pkItemEnchantProto))
			{
				 strbuf = _TRAN( N_NOTICE_C68, pkTitleInfo->stTitle.c_str(),pkTitleInfo->stDesc.c_str(), pkItemEnchantProto.sName.c_str() );
				//sprintf_s(buf,_MAX_PATH, strbuf.c_str()/*"<font:Arial:14><color:415194><center>%s<br><br><color:DA9B01><left><font:Arial:13>描述:%s<br><br><color:20C301>效果:%s<br>", pkTitleInfo->stTitle.c_str(),pkTitleInfo->stDesc.c_str(), pkItemEnchantProto.sName.c_str()*/);
				
			}
		}else
		{
			strbuf = _TRAN( N_NOTICE_C69, pkTitleInfo->stTitle.c_str(),pkTitleInfo->stDesc.c_str() );
			//sprintf_s(buf,_MAX_PATH, strbuf.c_str()/*"<font:Arial:14><color:415194><center>%s<br><br><color:DA9B01><left><font:Arial:13>描述:%s<br>", pkTitleInfo->stTitle.c_str(),pkTitleInfo->stDesc.c_str()*/);
		}

		m_SelTitleDis->SetText(strbuf.c_str(),strbuf.length());
	}

	if (m_pkcurTitle == Titleid)
	{
		GetChildByID(5)->SetActive(TRUE);

	}else
	{
		GetChildByID(5)->SetActive(FALSE);
	}

}	