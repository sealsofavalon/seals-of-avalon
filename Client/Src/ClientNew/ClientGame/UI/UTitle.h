#ifndef __UTITLE_H__
#define __UTITLE_H__
#include "UInc.h"
#include "USpecListBox.h"

class UTitle : public UDialog
{
	UDEC_CLASS(UTitle);
	UDEC_MESSAGEMAP();
public:
	UTitle();
	~UTitle();
	void AddTitle(ui32 titleid);
	void RemoveTitle(ui32 titleid);
	void ShowCurTitleDis(ui32 titleid);
	void ShowCurTitleDis();
	static void SendRemoveTitle();

	void ClearTitle();
public:
	virtual void SetVisible(BOOL value);
protected:
	void OnClickUseTitle();
	void OnClickRemoveTitle();
	void OnClickTitle(); 
	void OnClickHideTitle();
	
protected:
	BOOL virtual OnCreate();
	void virtual OnDestroy();
	virtual void OnClose();
	virtual BOOL OnEscape();


private:
	USpecListBox* m_TitleList ;
	URichTextCtrl* m_SelTitleDis;
	URichTextCtrl* m_CurTitleDis;
	UPoint m_OldPos ;
	ui32 m_pkcurTitle ;
};

#endif