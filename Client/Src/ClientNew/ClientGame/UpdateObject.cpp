#include "stdafx.h"
#include "UpdateObject.h"
void UpdateObj::_OnUpdateMovement(ByteBuffer* data, ui8 update_flags)
{
	ui32	move_flags = 0;
	ui32	move_time = 0;

	if( update_flags & UPDATEFLAG_LIVING )
	{
		*data >> move_flags >> move_time;
	}
	if (update_flags & UPDATEFLAG_HASPOSITION)
	{
		*data >> m_positionX >> m_positionY >> m_positionZ >> m_positionO;

		ui32 low_guid;ui32 high_guid;
		if((update_flags & UPDATEFLAG_LIVING) && (move_flags & MOVEFLAG_TAXI))
		{
			*data >> low_guid >> high_guid;
			m_taxi_guid = (ui64)high_guid << 32 | low_guid;
			*data >> m_taxi_positionX >> m_taxi_positionY >> m_taxi_positionZ >> m_taxi_positionO;
		}
	}
	if (update_flags & UPDATEFLAG_LIVING)
	{
		*data >> m_walkSpeed;	 // walk speed
		*data >> m_runSpeed;	  // run speed
		*data >> m_backWalkSpeed; // backwards walk speed
		*data >> m_swimSpeed;	 // swim speed
		*data >> m_backSwimSpeed; // backwards swim speed
		*data >> m_flySpeed;		// fly speed
		*data >> m_backFlySpeed;	// back fly speed
		*data >> m_turnRate;	  // turn rate
	}

	ui32 low_guid,high_guid;
	if(update_flags & UPDATEFLAG_LOWGUID)
	{
		*data >> low_guid;
		SetUInt32Value(OBJECT_FIELD_GUID, low_guid);
		if(update_flags & UPDATEFLAG_HIGHGUID)
		{
			*data >> high_guid;
			SetUInt32Value(OBJECT_FIELD_GUID_01, high_guid);
		}
	}
	else if(update_flags & UPDATEFLAG_HIGHGUID)
	{
		*data >> high_guid;
		SetUInt32Value(OBJECT_FIELD_GUID, high_guid);
	}

	if(update_flags & UPDATEFLAG_TRANSPORT)
	{
		ui32 taxi_time;
		*data >> taxi_time;
	}

}


void UpdateObj::OnCreateMsg(ByteBuffer* data, ui8 update_flags)
{
	_OnUpdateMovement(data, update_flags);
	UpdateMask mask = _OnUpdateValue(data);
	OnCreate(data, update_flags);
	m_justCreate = true;
	PostOnCreate();
	OnValueChanged(&mask);
	m_justCreate = false;
}

void UpdateObj::OnUpdateMsg(ByteBuffer* data)
{
	UpdateMask mask = _OnUpdateValue(data);
	OnValueChanged(&mask);
}
	
UpdateMask UpdateObj::_OnUpdateValue(ByteBuffer* data)
{
	UpdateMask updateMask;
	ui8 maskblock;
	ui32 value_count;
	*data >> maskblock >> value_count;
	updateMask.SetCount(maskblock<<5);
	data->read( (ui8*)updateMask.GetMask(), updateMask.GetLength());

    DASSERT(value_count <= m_valuesCount);
	for( ui32 index = 0; index < value_count+1; index ++ )
	{
		if( updateMask.GetBit( index ) )
		{
			// send in current format (float as float, uint32 as uint32)
			*data >> m_uint32Values[ index ];
			maskblock = 1;
		}
	}
	return updateMask;
}