#pragma once

class UpdateObj : public NiMemObject
{
public:
	UpdateObj() : m_typeid( TYPEID_UNUSED ){}
	virtual ~UpdateObj(){
		if( m_uint32Values )
			delete m_uint32Values;
	}

	//property interface 任何带有属性的物体构造函数需要调用_InitValues()
	inline void SetGUID(ui64 Guid) { SetUInt64Value(0, Guid); guid = Guid; }
	inline ui64 GetGUID() const { return GetUInt64Value(0); }
	virtual bool IsMapItem() const = 0;

	ui64 guid;

	//	position
	float	m_positionX,m_positionY,m_positionZ,m_positionO;
	//	taxi position
	ui64 m_taxi_guid;
	float	m_taxi_positionX,m_taxi_positionY,m_taxi_positionZ,m_taxi_positionO;
	//	living speed
	float m_walkSpeed;
	float m_runSpeed;
	float m_backWalkSpeed;
	float m_swimSpeed;
	float m_backSwimSpeed;
	float m_turnRate;
	float m_flySpeed;
	float m_backFlySpeed;
	float m_rushSpeed;
	uint8 m_typeid;
	bool m_justCreate;

	union
	{
		i32  *m_int32Values;
		ui32 *m_uint32Values;
		float *m_floatValues;
	};

	ui16 m_valuesCount;
	void _InitValues()
	{
		m_uint32Values = new ui32[ m_valuesCount ];
		memset(m_uint32Values, 0, m_valuesCount*sizeof(ui32));
	}
	inline TYPE GetType() const { return (TYPE)GetInt32Value(OBJECT_FIELD_TYPE);}
	inline uint8 GetTypeID() const { return m_typeid; }
	inline bool isType(TYPE mask) const { return 0xFFFFFFFF && (mask & GetInt32Value(OBJECT_FIELD_TYPE)); }
	inline bool isType(HIGHGUID_TYPE eTypeMask) const {
		ui64 uiGUID = GetGUID();
		if (GET_TYPE_FROM_GUID(uiGUID) == eTypeMask)
			return true;

		return false;
	}
	inline i32 GetInt32Value( ui16 index ) const
	{
		assert( index < m_valuesCount );
		return m_int32Values[ index ];
	}

	inline ui32 GetUInt32Value( ui16 index ) const
	{
		assert( index < m_valuesCount );
		return m_uint32Values[ index ];
	}

	inline void SetUInt32Value( ui16 index, ui32 value )
	{
		assert( index < m_valuesCount );
		m_uint32Values[ index ] = value;
	}
	//! Set uint64 property
	inline void SetUInt64Value( ui16 index, ui64 value )
	{
		assert( index + 1 < m_valuesCount );
		m_uint32Values[ index ] = *((ui32*)&value);
		m_uint32Values[ index + 1 ] = *(((ui32*)&value) + 1);
	}
	inline ui64 GetUInt64Value( ui16 index ) const
	{
		assert( index + 1 < m_valuesCount );
		return *((ui64*)&(m_uint32Values[ index ]));
	}

	inline float GetFloatValue( ui16 index ) const
	{
		assert( index < m_valuesCount );
		return m_floatValues[ index ];
	}

	inline ui8 GetByteValue( ui16 index, ui8 offset) const
	{
		assert( index < m_valuesCount );
		assert( offset < 4 );
		return *(((ui8*)&m_uint32Values[ index ])+offset);
	}

	inline ui8 GetUInt16Value( ui16 index, ui8 offset) const
	{
		assert( index < m_valuesCount );
		assert( offset < 2 );
		return (ui8)*(((ui16*)&m_uint32Values[ index ])+offset);
	}
	inline void SetByte(uint32 index, uint32 index1,uint8 value)
	{
		assert( index < m_valuesCount );
		uint8 * v =&((uint8*)m_uint32Values)[index*4+index1];
		if(*v == value)
			return;
		*v = value;
	}
	inline uint8 GetByte(uint32 i,uint32 i1)
	{
		assert( i < m_valuesCount);
		assert(i1 < 4);
		return ((uint8*)m_uint32Values)[i*4+i1];
	}

	inline void SetFlag(ui32 index, ui32 newFlag)
	{
		assert( index < m_valuesCount );
		m_uint32Values[ index ] |= newFlag;
	}
	inline void RemoveFlag(ui32 index, ui32 oldFlag)
	{
		assert( index < m_valuesCount );
		m_uint32Values[ index ] &= ~oldFlag;
	}
	inline BOOL HasFlag(ui32 index, ui32 flag)
	{
		assert( index < m_valuesCount );
		return (m_uint32Values[ index ] & flag) != 0;
	}
	virtual void OnValueChanged(class UpdateMask* mask){}
	virtual void OnCreate(class ByteBuffer* data, ui8 update_flags) = 0;
	virtual void PostOnCreate() = 0;

	void _OnUpdateMovement(class ByteBuffer* data, ui8 update_flags);
	UpdateMask _OnUpdateValue(class ByteBuffer* data);
	void OnCreateMsg(class ByteBuffer* data, ui8 update_flags);
	void OnUpdateMsg(class ByteBuffer* data);
};