#include "StdAfx.h"
#include "AreaDB.h"

CAreaDB* g_pkAreaDB = NULL;
bool CAreaDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	unsigned int uiAreaId;
	AreaInfo* pkAreaInfo;
	std::string str;
    char* pDesc;
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
        uiAreaId = GetUInt(ui, CAreaDB::EntryID);
        GetString(ui, CAreaDB::Name, str);


        pkAreaInfo = NiNew AreaInfo;

        //�ֽ����ֺ�����
        pDesc = (char*)strchr(str.c_str(), '(');
        if(pDesc)
        {
            pkAreaInfo->kDesc = pDesc;
            *pDesc = '\0';
            pkAreaInfo->kName = str.c_str();
			if (LangaugeMgr)
			{
				pkAreaInfo->kDesc = LangaugeMgr->GetNoticeText(pDesc).c_str();
				pkAreaInfo->kName = LangaugeMgr->GetNoticeText(str.c_str()).c_str();
			}
        }
        else
        {
			if (LangaugeMgr)
			{
				pkAreaInfo->kName = LangaugeMgr->GetNoticeText(str.c_str()).c_str();
			}
        }

		pkAreaInfo->uiFlags = GetUInt(ui, CAreaDB::Flags);

		pkAreaInfo->mapId = GetUInt(ui, CAreaDB::MapID);
		pkAreaInfo->minLevel = GetUInt(ui, CAreaDB::Min_Level);
		pkAreaInfo->maxLevel = GetUInt(ui, CAreaDB::Max_Level);
		pkAreaInfo->AllowableRace = GetUInt(ui, CAreaDB::Race);
		pkAreaInfo->entry = uiAreaId;

		m_kAreaMap.SetAt(uiAreaId, pkAreaInfo);
	}

	return true;
}

void CAreaDB::Unload()
{
	NiTMapIterator kPos = m_kAreaMap.GetFirstPos();
	unsigned int uiAreaId;
	AreaInfo* pkAreaInfo;
	while (kPos)
	{
		m_kAreaMap.GetNext(kPos, uiAreaId, pkAreaInfo);
		NiDelete pkAreaInfo;
	}

	CDBFile::Unload();
}

bool CAreaDB::GetAreaName(unsigned int uiAreaID, NiFixedString& kAreaName) const
{
	const AreaInfo* pkAreaInfo = GetAreaInfo(uiAreaID);
	if(pkAreaInfo)
	{
		kAreaName = pkAreaInfo->kName;
		return true;
	}

	return false;
}

const AreaInfo* CAreaDB::GetAreaInfo(unsigned int uiAreaID) const
{
	AreaInfo* pkAreaInfo;
	if(m_kAreaMap.GetAt(uiAreaID, pkAreaInfo))
		return pkAreaInfo;

	return NULL;
}

bool CAreaDB::GetAreaInfoList(std::vector<AreaInfo*>& vAreaInfo)
{
	NiTMapIterator kPos = m_kAreaMap.GetFirstPos();
	unsigned int uiAreaId;
	AreaInfo* pkAreaInfo;
	while (kPos)
	{
		m_kAreaMap.GetNext(kPos, uiAreaId, pkAreaInfo);
		vAreaInfo.push_back(pkAreaInfo);
	}
	return vAreaInfo.size()?true:false;
}
bool CAreaDB::GetAreaInfoList(ui32 mapid, std::vector<AreaInfo*>& vAreaInfo)
{
	NiTMapIterator kPos = m_kAreaMap.GetFirstPos();
	unsigned int uiAreaId;
	AreaInfo* pkAreaInfo;
	while (kPos)
	{
		m_kAreaMap.GetNext(kPos, uiAreaId, pkAreaInfo);
		if (pkAreaInfo->mapId == mapid)
		{
			vAreaInfo.push_back(pkAreaInfo);
		}
	}
	return vAreaInfo.size()?true:false;
}