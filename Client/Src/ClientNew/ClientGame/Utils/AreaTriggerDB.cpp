#include "StdAfx.h"
#include "AreaTriggerDB.h"

CAreaTriggerDB* g_pkAreaTriggerDB = NULL;

bool CAreaTriggerDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		stAreaTrigger sTemp;
		std::string str;
		sTemp.id = GetUInt(ui, 0);
		sTemp.type = GetUInt(ui, 1);
		sTemp.map = GetUInt(ui, 2);
		sTemp.screen = GetUInt(ui, 3);
		GetTranString(ui, 4, str);
		sTemp.name = (char*)str.c_str();
		m_AreaTriggers[sTemp.id] = sTemp;
	}

	return true;
}

CAreaTriggerDB::stAreaTrigger* CAreaTriggerDB::GetAreaTrigger(ui32 Entry)
{
	stdext::hash_map<ui32, stAreaTrigger>::iterator it = m_AreaTriggers.find(Entry);
	
	if(it != m_AreaTriggers.end())
		return &it->second;
	
	return NULL;
}