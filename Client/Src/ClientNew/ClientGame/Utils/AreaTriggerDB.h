#ifndef AREATRIGGERDB_H
#define AREATRIGGERDB_H

#include "DBFile.h"

class CAreaTriggerDB : public CDBFile
{
public:
	struct stAreaTrigger
	{
		ui32 id;
		ui32 type;
		ui32 map;
		ui32 screen;
		std::string name;
	};

	virtual bool Load(const char* pcName);

	stAreaTrigger* GetAreaTrigger(ui32 uiQuestId);


private:
	stdext::hash_map<ui32, stAreaTrigger> m_AreaTriggers;
};

extern CAreaTriggerDB* g_pkAreaTriggerDB;

#endif