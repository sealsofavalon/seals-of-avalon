#pragma once
#include "DBFile.h"

struct TitleInfo
{
	ui32 id ;
	std::string stTitle ;
	ui32 stProperty;
	ui8 Class;
	std::string stDesc;
	ui8 durTimeId;
};
struct TitleRewardInfo
{
	ui32 TitleId;
	ui32 RewardItemID;
	ui32 RewardItemCount;
};
class TitleDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	TitleInfo* GetTitleInfo(ui32 TitleID);
	bool GetTitleInfoList(std::vector<TitleInfo>& vInfo);
protected:
	std::map<ui32 , TitleInfo> m_TitleMap;
private:
};
class TiTleRewardDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	bool GetTitleReward(ui32 titleId, ui32& RewardItemid, ui32& RewardItemCount);

	std::vector<TitleRewardInfo> m_vTitleReward;

};

extern TiTleRewardDB* g_pkPlaeryTitleReward;
extern TitleDB* g_pkPlayerTitle;