#include "StdAfx.h"
#include "ChatFileterDB.h"
#include "../Network/NetworkManager.h"
#include "../ui/UFun.h"

CChatFilterDB* g_pkChatFilterDB = NULL;
bool CChatFilterDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	std::string str;
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
        GetString(ui, CChatFilterDB::Text, str);

		str = AnisToUTF8(str);

		if (str.length() > 1 && MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.length(), NULL, 0) > 1)
		{
			
			m_vTextSet.insert(str);
		}
	}

	std::set<std::string>::iterator it = m_vTextSet.begin();
	if(it != m_vTextSet.end())
	{
		std::string regText = it->c_str();
		while(it != m_vTextSet.end())
		{
			++it;
			if (it != m_vTextSet.end())
			{
				regText += '|';
				regText += it->c_str();
			}
		}
		UINT uiSize = MultiByteToWideChar(CP_UTF8, 0, regText.c_str(), regText.length(), NULL, 0);
		WCHAR* wregText = (WCHAR*)malloc((uiSize + 10)*sizeof(WCHAR));
		MultiByteToWideChar(CP_UTF8, 0, regText.c_str(), regText.length(), wregText, uiSize);
		wregText[uiSize] = 0;
		m_wreg.assign(wregText, boost::wregex::icase);

		free(wregText);
	}
	return true;
}
void GetRandomString(std::basic_string<wchar_t>& replacestr,int length)
{
	static wchar_t rplacestr[] = {L'@', L'#', L'$', L'%', L'^', L'&', L'*'};
	srand(time(0));
	wchar_t buf[256];
	int size = sizeof(rplacestr)/sizeof(rplacestr[0]);

	for(int i = 0 ; i < length ; i++)
		buf[i] = rplacestr[rand()%size];
	buf[length] = 0;

	replacestr = buf;
}
bool CChatFilterDB::GetFilterChatMsg(std::string &TextMsg)
{
	UINT uiSize = MultiByteToWideChar(CP_UTF8, 0, TextMsg.c_str(), TextMsg.length(), NULL, 0);
	WCHAR* WTextMsg = (WCHAR*)malloc((uiSize + 10)*sizeof(WCHAR));
	MultiByteToWideChar(CP_UTF8, 0, TextMsg.c_str(), TextMsg.length(), WTextMsg, uiSize);
	WTextMsg[uiSize] = 0;

	std::basic_string<wchar_t> WText = WTextMsg;
	bool bFind = boost::regex_search(WText, m_wreg);
	if( bFind )
	{
		std::basic_string<wchar_t> replaceStr;
		GetRandomString(replaceStr, rand()%6 + 1);
		WText = boost::regex_replace(WText,m_wreg,replaceStr);
		UINT Ret = WideCharToMultiByte(CP_UTF8, 0, WText.c_str(), -1, NULL, NULL, NULL, NULL);

		char* pRtfContent = (char*)malloc(Ret + 1);
		WideCharToMultiByte(CP_UTF8, 0, WText.c_str(), -1, pRtfContent, Ret, NULL, NULL);
		pRtfContent[Ret] = 0;
		TextMsg = pRtfContent;
		free(pRtfContent);
	}
	free(WTextMsg);
	return bFind;
}