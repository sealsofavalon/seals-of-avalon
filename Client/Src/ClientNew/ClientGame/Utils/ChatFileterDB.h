#pragma once

#include "DBFile.h"
#include "boost\regex.hpp"

class CChatFilterDB : public CDBFile
{
public:
    enum
	{
        Text = 0, //string
	};

    virtual bool Load(const char* pcName);

	bool GetFilterChatMsg(std::string &TextMsg);

	std::set<std::string> m_vTextSet;
	boost::wregex m_wreg;
};

extern CChatFilterDB* g_pkChatFilterDB;