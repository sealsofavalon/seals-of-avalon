#include "StdAfx.h"
#include "ClientApp.h"

void WorldPtToUIPt(NiPoint3& kPos, const NiCamera* pkCamera)
{
	NiAppWindow* pkAppWindow = SYState()->ClientApp->GetAppWindow();

	float fX, fY, fZ;
	pkCamera->WorldPtToScreenPt3(kPos, fX, fY, fZ, 0.f);
	kPos.x = fX*pkAppWindow->GetWidth();
	kPos.y = (1.0f-fY)*pkAppWindow->GetHeight();
	kPos.z = fZ;
}

void RecursiveSetAlpha(NiAVObject* pkRoot, float fAlpha, bool bAlpha)
{
	if(NiIsKindOf(NiGeometry, pkRoot))
	{
		NiGeometry* pkGeometry = (NiGeometry*)pkRoot;
		NiMaterialProperty* pkMaterial = NiDynamicCast(NiMaterialProperty, pkRoot->GetProperty(NiProperty::MATERIAL));
		if(!pkMaterial)
		{
			pkMaterial = NiNew NiMaterialProperty;
			pkGeometry->AttachProperty(pkMaterial);
		}
		pkMaterial->SetAlpha(fAlpha);

		NiAlphaPropertyPtr pkAlpha = NiDynamicCast(NiAlphaProperty, pkRoot->GetProperty(NiProperty::ALPHA));
		if(!pkAlpha)
		{
			pkAlpha = NiNew NiAlphaProperty;
			pkGeometry->AttachProperty(pkAlpha);			
		}
		pkAlpha->SetTestRef(128);
		pkAlpha->SetAlphaBlending(bAlpha);
		pkAlpha->SetAlphaTesting(true);

		NiStencilProperty* pkStencil = NiDynamicCast(NiStencilProperty, pkRoot->GetProperty(NiProperty::STENCIL));
		if(!pkStencil)
		{
			pkStencil = NiNew NiStencilProperty;
			pkGeometry->AttachProperty(pkStencil);
		}
		if(bAlpha)
		{
			pkStencil->SetDrawMode(NiStencilProperty::DRAW_BOTH);
		}
		else
		{
			pkStencil->SetDrawMode(NiStencilProperty::DRAW_CCW);
		}
	}

	if(NiIsKindOf(NiNode, pkRoot))
	{
		NiNode* pkNode = (NiNode*)pkRoot;
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ui++)
		{
			NiAVObject* pkObj = pkNode->GetAt(ui);
			if(pkObj)
			{
				RecursiveSetAlpha(pkObj, fAlpha, bAlpha);
			}		
		}
	}
}

void RecursiveResetMaterialEx(NiAVObject* pkRoot)
{
	if(NiIsKindOf(NiGeometry, pkRoot))
	{
		NiGeometry* pkGeometry = (NiGeometry*)pkRoot;
		NiMaterialProperty* pkMaterialProperty = (NiMaterialProperty*)pkGeometry->GetProperty(NiProperty::MATERIAL);
		pkMaterialProperty->SetAmbientColor(NiColor::WHITE);
		pkMaterialProperty->SetDiffuseColor(NiColor::WHITE);
		pkMaterialProperty->SetSpecularColor(NiColor::BLACK);
		pkMaterialProperty->SetEmittance(NiColor::WHITE);
	}

	if(NiIsKindOf(NiNode, pkRoot))
	{
		NiNode* pkNode = (NiNode*)pkRoot;
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ui++)
		{
			RecursiveResetMaterialEx(pkNode->GetAt(ui));
		}
	}
}

void RecursiveResetMaterial(NiAVObject* pkRoot)
{
	if(NiIsKindOf(NiGeometry, pkRoot))
	{
		NiGeometry* pkGeometry = (NiGeometry*)pkRoot;
		NiMaterialProperty* pkMaterialProperty = (NiMaterialProperty*)pkGeometry->GetProperty(NiProperty::MATERIAL);
		pkMaterialProperty->SetAmbientColor(NiColor::WHITE);
		pkMaterialProperty->SetDiffuseColor(NiColor::WHITE);
		pkMaterialProperty->SetSpecularColor(NiColor::BLACK);
		pkMaterialProperty->SetEmittance(NiColor::BLACK);
	}

	if(NiIsKindOf(NiNode, pkRoot))
	{
		NiNode* pkNode = (NiNode*)pkRoot;
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ui++)
		{
			RecursiveResetMaterial(pkNode->GetAt(ui));
		}
	}
}

void RecursiveSetCycleType(NiAVObject* pkRoot, NiTimeController::CycleType cType)
{
	NiTimeController* pkControl = pkRoot->GetControllers();
	for (/**/; pkControl; pkControl = pkControl->GetNext())
		pkControl->SetCycleType(cType);

	if(NiIsKindOf(NiNode, pkRoot))
	{
		NiNode* pkNode = (NiNode*)pkRoot;
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ui++)
		{
			NiAVObject* pkObj = pkNode->GetAt(ui);
			if(pkObj)
			{
				RecursiveSetCycleType(pkObj, cType);
			}		
		}
	}
}


void RecursiveSetAnimType(NiAVObject* pkRoot, NiTimeController::AnimType AType)
{
	NiTimeController* pkControl = pkRoot->GetControllers();
	for (/**/; pkControl; pkControl = pkControl->GetNext())
		pkControl->SetAnimType(AType);

	if(NiIsKindOf(NiNode, pkRoot))
	{
		NiNode* pkNode = (NiNode*)pkRoot;
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ui++)
		{
			NiAVObject* pkObj = pkNode->GetAt(ui);
			if(pkObj)
			{
				RecursiveSetAnimType(pkObj, AType);
			}		
		}
	}
}

void RecursiveGetGeometry(NiAVObject* pObject, NiGeometry** pGeometry)
{
	if(NiIsKindOf(NiGeometry, pObject))
	{
		*pGeometry = (NiGeometry*)pObject;
	}
	else if(NiIsKindOf(NiNode, pObject))
	{
		NiNode* pkNode = (NiNode*)pObject;
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ++ui)
		{
			if(*pGeometry == NULL)
			{
				NiAVObject* pkChild = pkNode->GetAt(ui);
				if(pkChild)
					RecursiveGetGeometry(pkChild, pGeometry);
			}
			else
			{
				return;
			}
		}
	}
}

void RecursiveGetGeometry(NiAVObject* pkObject, std::vector<NiGeometry*>& stGeometry)
{
	if(NiIsKindOf(NiGeometry, pkObject))
	{
		stGeometry.push_back((NiGeometry*)pkObject);
	}
	else if(NiIsKindOf(NiNode, pkObject))
	{
		NiNode* pkNode = (NiNode*)pkObject;
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ++ui)
		{
			NiAVObject* pkChild = pkNode->GetAt(ui);
			if(pkChild)
				RecursiveGetGeometry(pkChild, stGeometry);
		}
	}
}

bool RecursiveFindAnimation(NiObjectNET* pkObject)
{
	if(pkObject == NULL)
		return false;
	
	NiTimeController* pkController = pkObject->GetControllers();
	while(pkController != NULL)
	{
		if( pkController->GetActive()
		 && pkController->GetCycleType() == NiTimeController::LOOP
		 && pkController->GetBeginKeyTime() < pkController->GetEndKeyTime() )
		{
			return true;
		}

		pkController = pkController->GetNext();
	}
	
	if(NiIsKindOf(NiAVObject, pkObject))
	{
		// NiProperties can have time controllers, so search them too
		NiAVObject* pkAVObject = (NiAVObject*) pkObject;
		NiPropertyList* pkPropList = &(pkAVObject->GetPropertyList());
		NiTListIterator kIter = pkPropList->GetHeadPos();
		NiProperty* pkProperty;
		while(pkPropList != NULL && !pkPropList->IsEmpty()&& kIter)
		{
			pkProperty = pkPropList->GetNext(kIter);
			if(RecursiveFindAnimation(pkProperty))
				return true;
		}
	}

	if(NiIsKindOf(NiNode, pkObject))
	{
		NiNode* pkNode = (NiNode*) pkObject;
		NiAVObject* pkAVObject;

		// Search all of the children
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ui++)
		{
			pkAVObject = pkNode->GetAt(ui);
			if(RecursiveFindAnimation(pkAVObject))
				return true;
		}

		// NiDynamicEffects can have time controllers, so search them too
		const NiDynamicEffectList* pkEffectList= &(pkNode->GetEffectList());
		NiDynamicEffect* pkEffect;

		NiTListIterator kIter = pkEffectList->GetHeadPos();
		while(pkEffectList != NULL && !pkEffectList->IsEmpty() && kIter)
		{
			pkEffect = pkEffectList->GetNext(kIter);
			if(RecursiveFindAnimation(pkEffect))
				return true;
		}
	}

	return false;
}

NiD3DVertexShader* CreateVertexShader(const char* pcFilename, const char* pcShaderName)
{
    char* acBuffer = (char*)malloc(1024*256); //希望不要有大于256K的Shader
    NiFile* pkFile = NiFile::GetFile(pcFilename, NiFile::READ_ONLY);
    if( !pkFile || !(*pkFile) )
    {
        NiDelete pkFile;
		free( acBuffer );
        return NULL;
    }

    unsigned int uiBytes = pkFile->Read(acBuffer, 256*1024);
    NiDelete pkFile;

    acBuffer[uiBytes] = '\0';

    return NiD3DShaderProgramFactory::CreateVertexShaderFromBuffer(
                acBuffer, 
                uiBytes, 
                ".hlsl", 
                pcShaderName, 
                pcShaderName, 
                "vs_2_0",
                NULL, 
                0,
                false);

	free( acBuffer );
}

NiD3DPixelShader* CreatePixelShader(const char* pcFilename, const char* pcShaderName)
{
    char* acBuffer = (char*)malloc(1024*256); //希望不要有大于256K的Shader
    NiFile* pkFile = NiFile::GetFile(pcFilename, NiFile::READ_ONLY);
    if( !pkFile || !(*pkFile) )
    {
        NiDelete pkFile;
		free( acBuffer );
        return NULL;
    }

    unsigned int uiBytes = pkFile->Read(acBuffer, 256*1024);
    NiDelete pkFile;

    acBuffer[uiBytes] = '\0';


    return NiD3DShaderProgramFactory::CreatePixelShaderFromBuffer(
        acBuffer, uiBytes, ".hlsl",
        pcShaderName,
        pcShaderName,
        "ps_2_0",
        false);

	free( acBuffer );
}
