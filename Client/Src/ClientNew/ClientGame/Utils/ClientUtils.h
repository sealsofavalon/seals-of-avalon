#pragma once
#include "string"
void WorldPtToUIPt(NiPoint3& kPos, const NiCamera* pkCamera);

// fast int abs
static inline int int32abs( const int value )
{
	return (value ^ (value >> 31)) - (value >> 31);
}

// fast int abs and recast to unsigned
static inline uint32 int32abs2uint32( const int value )
{
	return (uint32)(value ^ (value >> 31)) - (value >> 31);
}

/// Fastest Method of float2int32
static inline int float2int32(const float value)
{
#if !defined(X64) && COMPILER == COMPILER_MICROSOFT && !defined(USING_BIG_ENDIAN)
	int i;
	__asm {
		fld value
		frndint
		fistp i
	}
	return i;
#else
	union { int asInt[2]; double asDouble; } n;
	n.asDouble = value + 6755399441055744.0;

#if USING_BIG_ENDIAN
	return n.asInt [1];
#else
	return n.asInt [0];
#endif
#endif
}

/// Fastest Method of long2int32
static inline int long2int32(const double value)
{
#if !defined(X64) && COMPILER == COMPILER_MICROSOFT && !defined(USING_BIG_ENDIAN)
	int i;
	__asm {
		fld value
		frndint
		fistp i
	}
	return i;
#else
  union { int asInt[2]; double asDouble; } n;
  n.asDouble = value + 6755399441055744.0;

#if USING_BIG_ENDIAN
  return n.asInt [1];
#else
  return n.asInt [0];
#endif
#endif
}


struct TimeTrigger
{
	float m_fDelayTime;
	float m_fStartTime;
	float m_fCurrTime;
	bool m_bTrigger;
	bool m_bPause;

	TimeTrigger()
	{
		m_fDelayTime = 0.0f;
		m_fStartTime = 0.0f;
		m_fCurrTime = 0.0f;
		m_bTrigger = false;
		m_bPause = false;
	}
	void Start(float fDelayTime)
	{
		m_fDelayTime = fDelayTime;
		m_fCurrTime = m_fStartTime = 0.0f;
	}
	void Update(float _dt)
	{
		if(!m_bPause)
			m_fCurrTime += _dt;
		if(m_fCurrTime - m_fStartTime > m_fDelayTime)
		{
			m_bTrigger = true;
		}
	}
	void ReSet()
	{
		m_fDelayTime = 0.0f;
		m_fStartTime = 0.0f;
		m_fCurrTime = 0.0f;
		m_bTrigger = false;
		m_bPause = false;
	}
	void Pause() { m_bPause = true; }
	void Continue() { m_bPause = false; }
};

void RecursiveSetAlpha(NiAVObject* pkRoot, float fAlpha, bool bAlpha);
void RecursiveResetMaterial(NiAVObject* pkRoot);
void RecursiveResetMaterialEx(NiAVObject* pkRoot);
void RecursiveSetCycleType(NiAVObject* pkRoot, NiTimeController::CycleType cType = NiTimeController::LOOP);
void RecursiveSetAnimType(NiAVObject* pkRoot, NiTimeController::AnimType AType = NiTimeController::APP_TIME);
void RecursiveGetGeometry(NiAVObject* pObject, NiGeometry** pGeometry);
void RecursiveGetGeometry(NiAVObject* pkObject, std::vector<NiGeometry*>& stGeometry);
bool RecursiveFindAnimation(NiObjectNET* pkObject);

NiD3DVertexShader* CreateVertexShader(const char* pcFilename, const char* pcShaderName);
NiD3DPixelShader* CreatePixelShader(const char* pcFilename, const char* pcShaderName);