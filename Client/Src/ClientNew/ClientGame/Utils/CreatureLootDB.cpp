#include "stdafx.h"
#include "CreatureLootDB.h"

CreatureLootDB* g_CreatureLootDB = NULL;

bool CreatureLootDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;
	
	unsigned int ui = 0;
	for ( ui; ui < GetRecordCount(); ui++ )
	{
		ui32 monster = GetUInt(ui, 1);
		ui32 item = GetUInt(ui, 2);
		m_CreatureLootSet.insert(std::multimap<ui32, ui32>::value_type(item, monster));
	}

	return true;
}
bool CreatureLootDB::FindLootCreature(ui32& monster, ui32 item)
{
	std::multimap<ui32, ui32>::iterator it = m_CreatureLootSet.find(item);
	if (it != m_CreatureLootSet.end())
	{
		monster = it->second;
		return  true;
	}
	return false;
}