#ifndef __CREATURELOOTDB_H__
#define __CREATURELOOTDB_H__

#include "DBFile.h"

class CreatureLootDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	bool FindLootCreature(ui32& monster, ui32 item);
protected:
	std::multimap<ui32, ui32> m_CreatureLootSet ;
};
extern CreatureLootDB* g_CreatureLootDB ;

#endif