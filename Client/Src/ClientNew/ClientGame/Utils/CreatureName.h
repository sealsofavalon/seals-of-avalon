#pragma once

#include "DBFile.h"
#include <hash_map>
class CCreatureNameDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	bool FindName(ui32 entry, string& name);
	bool FindAllName(ui32 entry, string& name);
	bool FindTitleName(ui32 entry, std::string& titleName);
	bool FindModelId(ui32 entry, ui32& modelid);
	bool FindWalkSpeed(ui32 entry, float& value);
	bool FindRunSpeed(ui32 entry, float& value);
	bool IsNPC(ui32 entry);
	bool FindRankID(ui32 entry, uint32& RankId);

	struct stCNDB
	{
		string name;
		string titlename;
		uint32 RankID;
		ui32 modelid;
		float anispeedWalk;
		float anispeedRun;
		ui32 sound_death;
		ui32 sound_attack1;
		ui32 sound_attack2;
		ui32 sound_stand;
		ui32 sound_run;
		uint8 Type;
	};

	bool FindCNDB(ui32 entry, CCreatureNameDB::stCNDB& cndb);
private:
	stdext::hash_map<ui32, stCNDB> m_mCreatureName;
};
