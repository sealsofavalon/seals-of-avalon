#include "StdAfx.h"
#include "CreatureSpawnsDB.h"

CreatureSpawnsDB* g_pkCreatureSpawns = NULL;

bool CreatureSpawnsDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		stCreatureSpawns sTemp;
		unsigned int uiCreatureId = GetUInt(ui, 0);
		sTemp.kPos.x = GetFloat(ui, 2);
		sTemp.kPos.y = GetFloat(ui, 3);
		sTemp.kPos.z = GetFloat(ui, 4);

		m_CreatureSpawns.insert(make_pair(uiCreatureId, sTemp));
	}

	return true;
}

CreatureSpawnsDB::stCreatureSpawns* CreatureSpawnsDB::GetCreatureSpawns(ui32 uiNpcId)
{
	std::map<ui32, stCreatureSpawns>::iterator it = m_CreatureSpawns.find(uiNpcId);

	if(it != m_CreatureSpawns.end())
		return &it->second;

	return NULL;
}

CCreatureSpawnsFromClientDB* g_pkCCreatureSpawnsInfo = NULL;
bool CCreatureSpawnsFromClientDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		stCreateSpawnsFromClientInfo sTemp;
		sTemp.Entry = GetUInt(ui, 0);
		sTemp.uiLevel = GetUInt(ui, 1);
		sTemp.race = GetUInt(ui, 2);
		sTemp.MapId = GetUInt(ui, 3);
		sTemp.AreaId = GetUInt(ui, 4);
		sTemp.kPos.x = GetFloat(ui, 5);
		sTemp.kPos.y = GetFloat(ui, 6);
		sTemp.kPos.z = GetFloat(ui, 7);

		m_CreatureSpawns.insert(make_pair(sTemp.Entry, sTemp));
	}
	return true;
};
bool CCreatureSpawnsFromClientDB::GetCreateSpawnsList(std::vector<stCreateSpawnsFromClientInfo>& vList)
{
	std::map<ui32, stCreateSpawnsFromClientInfo>::iterator it = m_CreatureSpawns.begin();
	while(it != m_CreatureSpawns.end())
	{
		vList.push_back(it->second);
		++it;
	}
	std::sort(vList.begin(),vList.end(),stCreateSpawnsFromClientInfo::Sort);
	return vList.size()?true:false;
}

bool CCreatureSpawnsFromClientDB::GetCreateSpawnsList(uint32 mapid, std::vector<stCreateSpawnsFromClientInfo>& vList)
{
	std::map<ui32, stCreateSpawnsFromClientInfo>::iterator it = m_CreatureSpawns.begin();
	while(it != m_CreatureSpawns.end())
	{
		if (it->second.MapId == mapid)
		{
			vList.push_back(it->second);
		}
		++it;
	}
	std::sort(vList.begin(),vList.end(),stCreateSpawnsFromClientInfo::Sort);
	return vList.size()?true:false;
}

bool CCreatureSpawnsFromClientDB::GetCreateInfomation(ui32 NpcID, ui32& mapid, float& x, float& y, float&z)
{
	std::map<ui32, stCreateSpawnsFromClientInfo>::iterator it = m_CreatureSpawns.find(NpcID);
	if (it != m_CreatureSpawns.end())
	{
		mapid = it->second.MapId;
		x = it->second.kPos.x;
		y = it->second.kPos.y;
		z = it->second.kPos.z;
		return true;
	}
	return false;
}

bool CCreatureSpawnsFromClientDB::GetCreateInfomation(ui32 NpcID, ui32& mapid, ui32& AreaId)
{
	std::map<ui32, stCreateSpawnsFromClientInfo>::iterator it = m_CreatureSpawns.find(NpcID);
	if (it != m_CreatureSpawns.end())
	{
		mapid = it->second.MapId;
		AreaId = it->second.AreaId;
		return true;
	}
	return false;
}