#ifndef CREATURESPAWNSDB_H
#define CREATURESPAWNSDB_H

#include "DBFile.h"

class CreatureSpawnsDB : public CDBFile
{
public:
	struct stCreatureSpawns
	{
		NiPoint3 kPos;
	};

	virtual bool Load(const char* pcName);

	stCreatureSpawns* GetCreatureSpawns(ui32 uiNpcId);

private:
	std::map<ui32, stCreatureSpawns> m_CreatureSpawns;
};
class CCreatureSpawnsFromClientDB : public CDBFile
{
public:
	struct stCreateSpawnsFromClientInfo
	{
		uint32 Entry;
		uint32 MapId;
		uint32 AreaId;
		NiPoint3 kPos;
		unsigned int race;
		uint32 uiLevel;

		static bool Sort(const stCreateSpawnsFromClientInfo& A, const stCreateSpawnsFromClientInfo& B)
		{
			if (A.MapId > B.MapId)
			{
				return true;
			}else if (A.MapId == B.MapId)
			{
				if (A.AreaId > B.AreaId)
				{
					return true ;
				}else if (A.AreaId == B.AreaId)
				{
					if (A.Entry >= B.Entry)
					{
						return true;
					}else
					{
						return false ;
					}
				}else
				{
					return false;
				}
			}

			return false;
		}
	};
		virtual bool Load(const char* pcName);
		bool GetCreateSpawnsList(std::vector<stCreateSpawnsFromClientInfo>& vList);
		bool GetCreateSpawnsList(uint32 mapid, std::vector<stCreateSpawnsFromClientInfo>& vList);
		bool GetCreateInfomation(ui32 NpcID, ui32& mapid, float& x, float& y, float&z);
		bool GetCreateInfomation(ui32 NpcID, ui32& mapid, ui32& AreaId);
private:
	std::map<ui32, stCreateSpawnsFromClientInfo> m_CreatureSpawns;
};

extern CreatureSpawnsDB* g_pkCreatureSpawns;
extern CCreatureSpawnsFromClientDB* g_pkCCreatureSpawnsInfo;

#endif