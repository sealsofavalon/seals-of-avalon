#ifndef _CYCLIC_BUFFER_H_
#define _CYCLIC_BUFFER_H_


template<class T, unsigned int uiInitializeSize>
class CyclicBuffer
{
public:
	CyclicBuffer();
	CyclicBuffer(T val);
	~CyclicBuffer(){};

	T AddHead(T val);
	void Clear(T val);

	T operator[](unsigned int ui) const;
	T& operator[](unsigned int ui);

public:
	T m_Buffer[uiInitializeSize];
	unsigned int m_uiCurInd;
};

template<class T, unsigned int uiInitializeSize>
inline
CyclicBuffer<T, uiInitializeSize>::CyclicBuffer() : m_uiCurInd(0)
{
	NIASSERT(uiInitializeSize > 1);
}

template<class T, unsigned int uiInitializeSize>
inline
CyclicBuffer<T, uiInitializeSize>::CyclicBuffer(T val) : m_uiCurInd(0)
{
	NIASSERT(uiInitializeSize > 1);
	Clear(val);
}

template<class T, unsigned int uiInitializeSize>
void CyclicBuffer<T, uiInitializeSize>::Clear(T val)
{
	for (unsigned int ui = 0; ui < uiInitializeSize; ui++)
		m_Buffer[ui] = val;
}

template<class T, unsigned int uiInitializeSize>
inline
T CyclicBuffer<T, uiInitializeSize>::AddHead(T val)
{
	T prevVal;

	if (m_uiCurInd == 0)
		m_uiCurInd = uiInitializeSize - 1;
	else
		m_uiCurInd--;

	prevVal = m_Buffer[m_uiCurInd];
	m_Buffer[m_uiCurInd] = val;

	return prevVal;
}

template<class T, unsigned int uiInitializeSize>
inline
T CyclicBuffer<T, uiInitializeSize>::operator[](unsigned int ui) const
{
	return m_Buffer[(m_uiCurInd + ui) % uiInitializeSize];
}

template<class T, unsigned int uiInitializeSize>
inline
T& CyclicBuffer<T, uiInitializeSize>::operator[](unsigned int ui)
{
	return m_Buffer[(m_uiCurInd + ui) % uiInitializeSize];
}


#endif //_CYCLIC_BUFFER_H_