#include "stdafx.h"
#include "DBFile.h"
#include "../../../../new_common/Source/ssl/crc32.h"


CDBFile::CDBFile()
	:m_pcData(NULL),
	m_puiRecordOffsets(NULL)
{
}

CDBFile::~CDBFile()
{
	Unload();
}

bool CDBFile::Load(const char* pcName)
{
	NiFile* pkFile = NiFile::GetFile(pcName, NiFile::READ_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return false;
	}

	m_uiSize = pkFile->GetFileSize();
	if( m_uiSize == 0 )
	{
		NiDelete pkFile;
		return false;
	}

	m_pcData = NiAlloc(char, m_uiSize);

	pkFile->Read(m_pcData, m_uiSize);
	NiDelete pkFile;

	m_pkHeader = (DBHeader*)m_pcData;

	//-------------------------------------------------------------------------------------------
	//字段信息
	//
	FieldInfo* pkFieldInfo;
	pkFieldInfo = (FieldInfo*)(m_pcData + 0x78);
	unsigned int uiFieldOffset = 0;

	for( unsigned char uc = 0; uc < m_pkHeader->m_ucFieldNum; uc++ )
	{
		m_uiFieldOffsets[uc] = uiFieldOffset;
		uiFieldOffset += pkFieldInfo->m_ucLength;
		pkFieldInfo++;
	}

	NIASSERT(uiFieldOffset == m_pkHeader->m_usRecordLength);

	//加上最后一个, 方便计算FieldLength
	m_uiFieldOffsets[m_pkHeader->m_ucFieldNum] = uiFieldOffset;


	//--------------------------------------------------------------------------------------------
	//Record
	//
	m_puiRecordOffsets = NiAlloc(unsigned int, m_pkHeader->m_uiRecordNum);
	unsigned int uiRecordCount = 0;

	unsigned int uiBlockSize = m_pkHeader->m_ucBlockSize * 1024;

	unsigned int uiOffset = m_pkHeader->m_usHeaderLength + (m_pkHeader->m_usFirstBlock - 1)*uiBlockSize;
	unsigned int uiLastRecordOffset;

	//If the block is empty, the offset is set to 0 minus record length.
	unsigned short usEmptyBlockOffset = unsigned short(0 - m_pkHeader->m_usRecordLength);

	DBBlock* pkBlock;
	for( unsigned short us = 0; us < m_pkHeader->m_usBlockNum; us++ )
	{
		pkBlock = (DBBlock*)(m_pcData + uiOffset);
		
		if( pkBlock->m_usLastBlockOffset != usEmptyBlockOffset )
		{
			uiOffset += 6; //定位到第一条Record
			uiLastRecordOffset = uiOffset + pkBlock->m_usLastBlockOffset;//定位到最后一条Record

			for( ;uiOffset <=  uiLastRecordOffset; uiOffset += m_pkHeader->m_usRecordLength )
			{
				m_puiRecordOffsets[uiRecordCount++] = uiOffset;
			}
		}

		if( pkBlock->m_usNextBlock == 0 )
			break;

		//定位到下一个Block
		uiOffset = m_pkHeader->m_usHeaderLength + (pkBlock->m_usNextBlock - 1)*uiBlockSize;
	}

	NIASSERT(uiRecordCount == m_pkHeader->m_uiRecordNum);

	return true;
}

void CDBFile::Unload()
{
	if (m_puiRecordOffsets != NULL)
	{
		NiFree(m_puiRecordOffsets);
		m_puiRecordOffsets = NULL;
	}

	if (m_pcData != NULL)
	{
		NiFree(m_pcData);
		m_pcData = NULL;
	}
}
void CDBFile::GetTranAddString(unsigned int uiRecord, unsigned int uiField, std::string& str)
{
	string stradd = "";
	GetTranString(uiRecord, uiField, stradd);
	if (stradd.length())
	{
		str += stradd;
	}
}
void CDBFile::GetTranString(unsigned int uiRecord, unsigned int uiField, std::string& str)
{
	char acBuffer[2048];

	unsigned int uiLen = GetFieldLength(uiField);
	NIASSERT(uiLen <= 2048);
	memcpy(acBuffer, GetField(uiRecord, uiField), uiLen);
	acBuffer[uiLen] = 0;

	if (LangaugeMgr)
	{
		str = LangaugeMgr->GetNoticeText( acBuffer );
		if ( str.length() == 0 )
		{
			str = acBuffer;
		}
	}else
	{
		str = acBuffer;
	}
	 	
}
void CDBFile::GetString(unsigned int uiRecord, unsigned int uiField, std::string& str)
{
	char acBuffer[2048];

	unsigned int uiLen = GetFieldLength(uiField);
	NIASSERT(uiLen <= 2048);
	memcpy(acBuffer, GetField(uiRecord, uiField), uiLen);
	acBuffer[uiLen] = 0;
	
	str = acBuffer;
}

void CDBFile::GetUTF8String(unsigned int uiRecord, unsigned int uiField, std::string& str)
{
	char acAnsi[512];

	unsigned int uiLen = GetFieldLength(uiField);
	memcpy(acAnsi, GetField(uiRecord, uiField), uiLen);
	acAnsi[uiLen] = 0;
	uiLen = strlen(acAnsi);

	wchar_t acUnicode[1024];
	char acUTF8[1024];

	//Ansi 转换为 Unicode
	long wcLength = MultiByteToWideChar(CP_ACP, 0, acAnsi, -1, NULL, 0 );
	uiLen = MultiByteToWideChar(CP_ACP, 0, acAnsi, -1, acUnicode, wcLength );

	//Unicode 转换为 Utf8
	long utf8Length = WideCharToMultiByte(CP_UTF8, 0, acUnicode, -1, NULL, 0, NULL, false);
	uiLen = WideCharToMultiByte(CP_UTF8, 0, acUnicode, -1, acUTF8, utf8Length, NULL, false);

	acUTF8[utf8Length] = 0;

	str.resize(utf8Length);
	str.replace(0, utf8Length, acUTF8, utf8Length);
}
