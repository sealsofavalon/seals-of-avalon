#include "StdAfx.h"
#include "DefaultDisplayInfoDB.h"

CDefaultDisplayInfoDB* g_pkDefaultDisplayInfo = NULL;
CDefaultDisplayInfoDB* g_pkCreateDefaultDisplayInfo = NULL;

bool CDefaultDisplayInfoDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	unsigned int uiRace;
	unsigned int uiClass;
	unsigned int uiGender;
	unsigned int uiValidId;
	for( unsigned int ui = 0; ui < GetRecordCount(); ui++ )
	{
		uiRace = GetUInt(ui, 0);
		uiClass = GetUInt(ui, 1);
		uiGender = GetUInt(ui, 2);
		//uiValidId = uiRace + (uiClass<<8) + (uiGender<<16);
		uiValidId = uiRace + (uiGender<<16);
		DefaultDisplayInfo DefaultInfo;
		DefaultInfo.sClass = uiClass;
		DefaultInfo.sRace = uiRace;
		DefaultInfo.sGender = uiGender;
		DefaultInfo.sValidId = uiValidId;
		DefaultInfo.sModel = GetUInt(ui, 3);
		for(int i = 0; i < 5; i++)
		{
			ui32 uiId = GetUInt(ui, 4 + i);
			DefaultInfo.sDefault[i] = uiId;
		}
		m_DefaultInfo.push_back(DefaultInfo);
	}

	return true;
}

bool CDefaultDisplayInfoDB::FindDefaultDisplayInfo(ui32 displayid, ui32& uiModelId, unsigned int uiDefaultPart, bool bReturnShort /* = false */)
{
	for(unsigned int ui = 0; ui < m_DefaultInfo.size(); ++ui)
	{
		if(m_DefaultInfo[ui].sValidId == displayid)
		{
			if(!bReturnShort)
			{
				uiModelId = m_DefaultInfo[ui].sDefault[uiDefaultPart];
			}
			else
			{
				uiModelId = m_DefaultInfo[ui].sDefault[uiDefaultPart];
			}

			return true;
		}
	}

	return false;
}

bool CDefaultDisplayInfoDB::FindDefaultModelId(uint8 nRace, uint8 nClass, uint8 nGender, uint32& displayid)
{
	for(unsigned int ui = 0; ui < m_DefaultInfo.size(); ++ui)
	{
		if(m_DefaultInfo[ui].sRace == nRace && m_DefaultInfo[ui].sClass == nClass && m_DefaultInfo[ui].sGender == nGender)
		{
			displayid = m_DefaultInfo[ui].sModel;

			return true;
		}
	}

	return false;
}
bool CDefaultDisplayInfoDB::FindDefaultModelInfo(ui32 displayid, ui32& strModelId)
{
	for(unsigned int ui = 0; ui < m_DefaultInfo.size(); ++ui)
	{
		if(m_DefaultInfo[ui].sValidId == displayid)
		{
			strModelId = m_DefaultInfo[ui].sModel;

			return true;
		}
	}

	return false;
}

int CDefaultDisplayInfoDB::GetDefaultModelCount()
{
	return m_DefaultInfo.size();
}

ui32 CDefaultDisplayInfoDB::GetDefaultModelId(int iIndex)
{
	NIASSERT(iIndex < (int)m_DefaultInfo.size());

	return m_DefaultInfo[iIndex].sModel;
}
int CDefaultDisplayInfoDB::GetDefaultModelName(int iIndex, char* pDest)
{
	NIASSERT(iIndex < (int)m_DefaultInfo.size());

	int rec = NiSprintf(pDest,_MAX_PATH,"%d_%d_%d",m_DefaultInfo[iIndex].sRace, m_DefaultInfo[iIndex].sGender, m_DefaultInfo[iIndex].sClass);
	return rec ;
}

ui32 CDefaultDisplayInfoDB::GetDefaultModelValidId(int iIndex)
{
	NIASSERT(iIndex < (int)m_DefaultInfo.size());

	return m_DefaultInfo[iIndex].sValidId;
}