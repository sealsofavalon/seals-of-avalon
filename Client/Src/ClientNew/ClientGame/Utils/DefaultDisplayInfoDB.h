#pragma once
#include "DBFile.h"
//#include <hash_map>

class CDefaultDisplayInfoDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);

	bool FindDefaultDisplayInfo(ui32 displayid, ui32& strModelId, unsigned int uiDefaultPart, bool bReturnShort = false);

	bool FindDefaultModelInfo(ui32 displayid, ui32& strModelId);
	bool FindDefaultModelId(uint8 nRace, uint8 nClass, uint8 nGender, uint32& displayid);
	int GetDefaultModelCount();

	ui32 GetDefaultModelId(int iIndex);
	int GetDefaultModelName(int iIndex, char* pDest);
	ui32 GetDefaultModelValidId(int iIndex);

	struct DefaultDisplayInfo
	{
		//DisplayInfo sHead;
		//DisplayInfo sGloves;
		//DisplayInfo sShirt;
		//DisplayInfo sPants;
		//DisplayInfo sBoots;
		ui32 sValidId;
		UINT sRace;
		UINT sGender;
		UINT sClass;
		ui32 sModel;
		ui32 sDefault[5];
	};
private:
	std::vector<DefaultDisplayInfo> m_DefaultInfo;
};

extern CDefaultDisplayInfoDB* g_pkDefaultDisplayInfo;
extern CDefaultDisplayInfoDB* g_pkCreateDefaultDisplayInfo;