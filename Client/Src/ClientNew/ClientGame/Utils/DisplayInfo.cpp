
#include "StdAfx.h"
#include "DisplayInfo.h"
#include "ClientState.h"
#include "ResourceManager.h"

bool CDisplayInfoDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	std::string strModel;
	unsigned int usValue;
	for( unsigned int ui = 0; ui < GetRecordCount(); ui++ )
	{
		usValue = GetUInt(ui, 0);
		GetString(ui, 1, strModel);
		std::string str = strModel;
		int pos = str.find(";");
		DisplayInfo displayInfo;
		displayInfo.strLongName = strModel;
		if(pos != -1)
		{
			std::string str1 = strModel.substr(0, pos);
			std::string str2 = strModel.substr(pos + 1, strModel.size());

			displayInfo.strLongName = str1;
			displayInfo.strShortName = str2;
		}

		m_mDisplayInfo.insert(make_pair(usValue, displayInfo));
	}

    if(ClientState->GetCheckFile())
        CheckFile();

	return true;
}

bool CDisplayInfoDB::FindDisplayInfo(ui32 displayid, string& strModel, bool bReturnShort /* = false */)
{
	stdext::hash_map<ui32, DisplayInfo>::iterator itr = m_mDisplayInfo.find(displayid);
	if( itr != m_mDisplayInfo.end() )
	{
		if(!bReturnShort)
			strModel = itr->second.strLongName;
		else
			strModel = itr->second.strShortName;
		return true;
	}
	return false;
}

void CDisplayInfoDB::CheckFile()
{
    stdext::hash_map<ui32, DisplayInfo>::iterator itr;
    NiFixedString kFilename;
    char aBuffer[1024];
    NiFile* pkFile;
    char aNifFile[1024];

    unsigned int uiErrorCount = 0;
    for(itr = m_mDisplayInfo.begin(); itr != m_mDisplayInfo.end(); ++itr)
    {
        kFilename = itr->second.strLongName.c_str();
        if(!NiFile::Access(kFilename, NiFile::READ_ONLY))
        {
            NiSprintf(aBuffer, sizeof(aBuffer), "display id %d %s File not found\n", itr->first, (const char*)kFilename);
            OutputDebugStringA(aBuffer);
            ++uiErrorCount;
            continue;
        }

        if(!IsMfFile(kFilename))
            continue;

        aNifFile[0] = '\0';
        bool bOK = true;

        strcpy(aNifFile, kFilename);
        char* splash = strrchr(aNifFile, '\\');
        if(splash)
        {
            ++splash;
            *splash = '\0';
        }
        else
        {
            bOK = false;
        }

        if(bOK)
        {
            pkFile = NiFile::GetFile(kFilename, NiFile::READ_ONLY);

            if(pkFile && *pkFile)
            {
                pkFile->GetLine(aBuffer, sizeof(aBuffer));
                strcat(aNifFile, aBuffer);
            }
            else
            {
                bOK = false;
            }

            NiDelete pkFile;
        }

        if(bOK)
        {
            if( NiFile::Access(aNifFile, NiFile::READ_ONLY) )
                continue;
        }

        NiSprintf(aBuffer, sizeof(aBuffer), "Display id %d %s %s Nif not found\n", itr->first, (const char*)kFilename, aNifFile);
        OutputDebugStringA(aBuffer);
        ++uiErrorCount;
    }

    NiSprintf(aBuffer, sizeof(aBuffer), "Display id Total Count %d Error Count %d\n", m_mDisplayInfo.size(), uiErrorCount);
    OutputDebugStringA(aBuffer);
}