#pragma once
#include "DBFile.h"
#include <hash_map>

class CDisplayInfoDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);

	bool FindDisplayInfo(ui32 displayid, string& strModel, bool bReturnShort = false);
    void CheckFile();

	struct DisplayInfo
	{
		std::string strLongName;
		std::string strShortName;
	};

private:
	stdext::hash_map<ui32, DisplayInfo> m_mDisplayInfo;
};