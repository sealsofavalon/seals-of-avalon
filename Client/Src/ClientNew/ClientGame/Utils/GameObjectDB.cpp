#include "StdAfx.h"
#include "GameObjectDB.h"

CGameObjectDB* g_pkGameObjectDB = NULL;

bool CGameObjectDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		stGameObject sTemp;
		std::string str;
		sTemp.ID = GetUInt(ui, 0);
		sTemp.Type = GetUInt(ui, 1);
		sTemp.DisplayID = GetUInt(ui, 2);
		GetTranString(ui, 3, str);
		sTemp.Name = (char*)str.c_str();
		sTemp.SpellFocus = GetUInt(ui, 4);
		sTemp.CastSpell = GetUInt(ui, 5);
		sTemp.ReqSpell = GetUInt(ui, 6);
		sTemp.QuestID = GetUInt(ui, 7);
		sTemp.ReqItem = GetUInt(ui, 8);
		sTemp.SpawnTime = GetUInt(ui, 9);
		sTemp.NewObj = GetUInt(ui, 10);
		sTemp.MineMin = GetUInt(ui, 11);
		sTemp.MineMax = GetUInt(ui, 12);
		sTemp.QuestItemID = GetUInt(ui,13);

		m_GameObjects[sTemp.ID] = sTemp;
	}

	return true;
}

CGameObjectDB::stGameObject* CGameObjectDB::GetGameObject(ui32 Entry)
{
	stdext::hash_map<ui32, stGameObject>::iterator it = m_GameObjects.find(Entry);
	
	if(it != m_GameObjects.end())
		return &it->second;
	
	return NULL;
}
