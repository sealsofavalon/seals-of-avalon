#include "StdAfx.h"
#include "ItemEnchant.h"

CItemEnchantDB* g_pkItemEnchantDB = NULL;
CItemEnchantDB::CItemEnchantDB()
{

}

CItemEnchantDB::~CItemEnchantDB()
{
	m_ItemEnchant.clear();
}

bool CItemEnchantDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	std::string sName;
	ItemEnchantProto sBaseInfo;
	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		sBaseInfo.uiEntryId = GetUInt(ui, EntryID);

		sBaseInfo.uiType[0] = GetUInt(ui, TYPE1);
		sBaseInfo.uiType[1] = GetUInt(ui, TYPE2);
		sBaseInfo.uiType[2] = GetUInt(ui, TYPE3);

		sBaseInfo.uiMin[0] = GetUInt(ui, MIN1);
		sBaseInfo.uiMin[1] = GetUInt(ui, MIN2);
		sBaseInfo.uiMin[2] = GetUInt(ui, MIN3);

		sBaseInfo.uiMax[0] = GetUInt(ui, MAX1);
		sBaseInfo.uiMax[1] = GetUInt(ui, MAX2);
		sBaseInfo.uiMax[2] = GetUInt(ui, MAX3);

		sBaseInfo.uiSpell[0] = GetUInt(ui, SPELL1);
		sBaseInfo.uiSpell[1] = GetUInt(ui, SPELL2);
		sBaseInfo.uiSpell[2] = GetUInt(ui, SPELL3);

		GetTranString(ui, NAME, sBaseInfo.sName);

		sBaseInfo.uiVisual = GetUInt(ui, VISUAL);
		sBaseInfo.uiEnchantGroups = GetUInt(ui, ENCHANTGROUPS);
		sBaseInfo.uiGemEntry = GetUInt(ui, GEMENTRY);
		sBaseInfo.uiProcChance = GetUInt(ui, PROCCHANCE);

		m_ItemEnchant.insert(std::make_pair(sBaseInfo.uiEntryId, sBaseInfo));
	}

	return true;
}

bool CItemEnchantDB::GetEnchantProperty(ui32 EntryId , ItemEnchantProto& itemEnchantProto)
{
	std::map<ui32, ItemEnchantProto>::iterator it = m_ItemEnchant.find(EntryId);
	if (it != m_ItemEnchant.end())
	{
		itemEnchantProto = it->second;
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "ItemDB.h"
CRefineitemeffectDB* g_pkRefineItemEffectDB = NULL;

CRefineitemeffectDB::CRefineitemeffectDB()
{
}

CRefineitemeffectDB::~CRefineitemeffectDB()
{
}

bool CRefineitemeffectDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	refineItemEffect NewEntry;
	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		NewEntry.entry = GetUInt(ui, 0);
		NewEntry.rarity = GetUInt(ui, 1);
		NewEntry.min_refineLevel =GetUInt(ui, 2);
		NewEntry.max_refineLevel = GetUInt(ui, 3);
		NewEntry.InventoryType = GetUInt(ui,4);
		NewEntry.displayId = GetUInt(ui, 5);
		m_ItemEnchant.push_back(NewEntry);
	}
	return true;
}

bool CRefineitemeffectDB::GetRefineItemEffect(ItemPrototype_Client* pItem, UINT jinglianLevel, ui32& effect_display_ID)
{
	if(!pItem)
		return false;
	std::vector<refineItemEffect>::iterator it = m_ItemEnchant.begin();
	uint32 inventorytype;
	while(it != m_ItemEnchant.end())
	{
		if(jinglianLevel <= it->max_refineLevel 
			&& jinglianLevel >= it->min_refineLevel
			&& pItem->Quality == it->rarity)
		{
			inventorytype =pItem->InventoryType;
			if(inventorytype == INVTYPE_WEAPONMAINHAND 
				|| inventorytype ==INVTYPE_WEAPONOFFHAND 
				|| inventorytype ==INVTYPE_2HWEAPON)
			{
				inventorytype = 100;
			}
			if( inventorytype == it->InventoryType )
			{
				effect_display_ID = it->displayId;
				return true;
			}
		}
		++it;
	}
	return false;
}