#include "stdafx.h"
#include "ItemExtendedCostentry.h"


ItemExtendedCostentry::ItemExtendedCostentry()
{

}
bool ItemExtendedCostentry::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
	{
		return false;
	}

	ItemExtendeData pData ;
	for (unsigned int i = 0; i <GetRecordCount() ; i++)
	{
		pData.Costentry = GetUInt(i ,0);
		pData.Honor = GetUInt(i ,1);
		pData.arena = GetUInt(i ,2);

		pData.Item1 = GetUInt(i ,3);
		pData.Item2 = GetUInt(i ,4);
		pData.Item3 = GetUInt(i ,5);
		pData.Item4 = GetUInt(i ,6);
		pData.Item5 = GetUInt(i ,7);


		pData.ItemCount1 = GetUInt(i ,8);
		pData.ItemCount2 = GetUInt(i ,9);
		pData.ItemCount3 = GetUInt(i ,10);
		pData.ItemCount4 = GetUInt(i ,11);
		pData.ItemCount5 = GetUInt(i ,12);

		pData.Gold = GetUInt(i ,13);
		pData.Yuanbao = GetUInt(i ,14);
		pData.Guild_Score = GetUInt(i ,15);
		pData.Guild_Contribute = GetUInt(i ,16);


		m_ItemExtende.insert(std::make_pair(pData.Costentry, pData));
	}


	return true;
}

ItemExtendedCostentry::ItemExtendeData* ItemExtendedCostentry::GetItemExtende(ui32 Costentry)
{
	ItemExtendeData* Info = NULL;

	std::map<ui32, ItemExtendeData>::iterator it = m_ItemExtende.find(Costentry);
	if(it != m_ItemExtende.end())
	{
		Info = &it->second;
	}

	return Info;
}


ItemSetDB* g_ItemSetDB = NULL;

bool ItemSetDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
	{
		return false;
	}
	stItemSetSingel singel;
	for (unsigned int ui = 0 ; ui < GetRecordCount() ; ++ui)
	{
		singel.entry = GetUInt(ui, 0);
		GetTranString(ui, 1, singel.name);
		singel.flag = GetUInt(ui, 2);
		singel.itemID[0] = GetUInt(ui, 3);
		singel.itemID[1] = GetUInt(ui, 4);
		singel.itemID[2] = GetUInt(ui, 5);
		singel.itemID[3] = GetUInt(ui, 6);
		singel.itemID[4] = GetUInt(ui, 7);
		singel.itemID[5] = GetUInt(ui, 8);
		singel.itemID[6] = GetUInt(ui, 9);
		singel.itemID[7] = GetUInt(ui, 10);

		for (int i = 0 ; i < 8 ; ++i)
		{
			singel.spell[i].SpellID = GetUInt(ui, 11 + i);
		}
		
		for (int i = 0 ; i < 8 ; ++i)
		{
			singel.spell[i].itemscount = GetUInt(ui, 19 + i);
		}

		singel.requireskillID = GetUInt(ui, 27);
		singel.requireskillamt = GetUInt(ui, 28);

		m_vItemSet.push_back(singel);
	}
	return true;
}

bool ItemSetDB::GetItemSetSingel(uint32 ItemEntry, stItemSetSingel& itemset)
{
	std::vector<stItemSetSingel>::iterator it = m_vItemSet.begin();
	while(it != m_vItemSet.end())
	{
		stItemSetSingel pSingel = *it++;
		for(int i = 0 ; i < 8 ; ++i)
		{
			if(pSingel.itemID[i] == ItemEntry)
			{
				itemset = pSingel;
				return true;
			}
		}
	}
	return false;
}