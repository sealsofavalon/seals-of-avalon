#pragma once
#include "DBFile.h"
#include <hash_map>

class ItemExtendedCostentry : public CDBFile
{
public:
	ItemExtendedCostentry();
	virtual bool Load(const char* pcName);

	
	// 目前道具只开放， ITEM1 和ITENCOUNT1  这2个数据段，
	// 其他的有关道具的 ITEM2,ITEM3,ITEM4,ITEM5 以及ItemCount1, ItemCount2, ItemCount3,ItemCount4,ItemCount5
	// 等字段预留. 如果需要, 客户度也需要相应的修改
	struct ItemExtendeData
	{
		ItemExtendeData()
		{
			Costentry = 0;
			Honor = 0;
			Gold = 0;
			Yuanbao = 0;
			arena = 0;
			Guild_Contribute = 0;
			Guild_Score = 0;

			Item1 = 0;
			Item2 = 0;
			Item3 = 0;
			Item4 = 0;
			Item5 = 0;

			ItemCount1 = 0;
			ItemCount2 = 0;
			ItemCount3 = 0;
			ItemCount4 = 0;
			ItemCount5 = 0;

		}
		ui32 Costentry ;
		UINT Honor ;
		UINT Gold;
		UINT Yuanbao;
		UINT arena ;
		UINT Guild_Score;
		UINT Guild_Contribute;

		ui32 Item1,Item2,Item3,Item4,Item5;
		ui32 ItemCount1,ItemCount2,ItemCount3,ItemCount4,ItemCount5;
	};

	ItemExtendeData* GetItemExtende(ui32 Costentry);

protected:
	std::map<ui32, ItemExtendeData> m_ItemExtende;
private:
};
struct SetCountSpell
{
	uint32 SpellID;
	uint32 itemscount;
};
struct stItemSetSingel
{
	uint8 entry;
	std::string name;
	uint8 flag;
	uint32 itemID[8];
	SetCountSpell spell[8];
	uint32 requireskillID;
	uint32 requireskillamt;
};
class ItemSetDB : public CDBFile
{
	virtual bool Load(const char* pcName);
public:
	bool GetItemSetSingel(uint32 ItemEntry, stItemSetSingel& itemset);
	
protected:
	std::vector<stItemSetSingel> m_vItemSet;

};
extern ItemSetDB* g_ItemSetDB;