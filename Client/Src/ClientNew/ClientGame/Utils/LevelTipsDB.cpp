#include "StdAfx.h"
#include "LevelTipsDB.h"

CLevelTipsDB* g_pkLevelTipsDB = NULL;
bool CLevelTipsDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	stLevelTips tip;
	
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
        tip.ntype  = GetUInt(ui, 1);
		tip.race   = GetUInt(ui, 2);
		tip.nclass  = GetUInt(ui, 3);
		tip.level  = GetUInt(ui, 4);
        GetTranString(ui, 5, tip.tip);

		vLvlTips.push_back(tip);
	}

	return true;
}

bool CLevelTipsDB::GetTips(uint32 level, uint32 race, uint32 nclass, std::vector<stLevelTips>& vTips) const
{
	bool bFound = false;
	for(int i = 0; i < (int)vLvlTips.size(); i++)
	{
		if(vLvlTips[i].level == level && vLvlTips[i].race & 1<<(race-1) && vLvlTips[i].nclass & 1<<(nclass-1))
		{
			vTips.push_back(vLvlTips[i]);
			bFound = true;
		}
	}
	return bFound;
}

CGuildLevelUpDB* g_pkGuildLevelUpDB = NULL;
bool CGuildLevelUpDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;
	stGuildLevelUpInfo GuildLevelUpInfo;
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
		GuildLevelUpInfo.level  = GetUInt(ui, 0);
		GuildLevelUpInfo.require_item_entry   = GetUInt(ui, 1);
		GuildLevelUpInfo.require_score  = GetUInt(ui, 2);
		GuildLevelUpInfo.nextLev_member_count  = GetUInt(ui, 3);
		GetTranString(ui, 4, GuildLevelUpInfo.How_Get);
		GuildLevelUpInfo.require_member_min = GetUInt(ui, 5);
		GuildLevelUpInfo.require_gold = GetUInt(ui, 6);
		vlvlUp.push_back(GuildLevelUpInfo);
	}
	return true;
}

bool CGuildLevelUpDB::GetGuildLevelUpInfo(uint32 level, stGuildLevelUpInfo& info)
{
	for (unsigned int ui = 0 ; ui < vlvlUp.size() ; ui++)
	{
		if (vlvlUp[ui].level == level)
		{
			info = vlvlUp[ui];
			return true;
		}
	}
	return false;
}

ui32 CGuildLevelUpDB::Getmembercount(uint32 level)
{
	for (unsigned int ui = 0 ; ui < vlvlUp.size() ; ui++)
	{
		if (vlvlUp[ui].level == level)
		{
			return vlvlUp[ui].nextLev_member_count;
		}
	}
	return 0;
}