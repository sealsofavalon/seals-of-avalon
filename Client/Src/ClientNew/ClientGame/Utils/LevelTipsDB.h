#pragma once

#include "DBFile.h"

class CLevelTipsDB : public CDBFile
{
public:
	struct stLevelTips
	{
		uint32 id;
		uint32 ntype;
		uint32 race;
		uint32 nclass;
		uint32 level;
		std::string tip;
	};
    virtual bool Load(const char* pcName);
    bool GetTips(uint32 level, uint32 race, uint32 nclass, std::vector<stLevelTips>& vTips) const;

private:
	std::vector<stLevelTips> vLvlTips;
};

class CGuildLevelUpDB : public CDBFile
{
public:
	struct stGuildLevelUpInfo
	{
		uint32 level;
		uint32 require_item_entry;
		uint32 require_score;
		uint32 nextLev_member_count;
		std::string How_Get;
		uint32 require_member_min;
		uint32 require_gold;
	};
	virtual bool Load(const char* pcName);
	bool GetGuildLevelUpInfo(uint32 level, stGuildLevelUpInfo& info);
	ui32 Getmembercount(uint32 level);
private:
	std::vector<stGuildLevelUpInfo> vlvlUp;
};

extern CGuildLevelUpDB* g_pkGuildLevelUpDB;
extern CLevelTipsDB* g_pkLevelTipsDB;