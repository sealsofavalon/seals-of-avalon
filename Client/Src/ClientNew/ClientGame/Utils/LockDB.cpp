#include "StdAfx.h"
#include "LockDB.h"
#include "../../../../../SDBase/Public/DBCStores.h"

CLockDB* g_LockObjects = NULL;

bool CLockDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		Lock sTemp;
		sTemp.Id = GetUInt(ui, 0);
		sTemp.locktype[0] = GetUInt(ui, 1);
		sTemp.locktype[1] = GetUInt(ui, 2);
		sTemp.locktype[2] = GetUInt(ui, 3);
		sTemp.locktype[3] = GetUInt(ui, 4);
		sTemp.locktype[4] = GetUInt(ui, 5);
		sTemp.locktype[5] = GetUInt(ui, 6);
		sTemp.locktype[6] = GetUInt(ui, 7);
		sTemp.locktype[7] = GetUInt(ui, 8);
		
		sTemp.lockmisc[0] = GetUInt(ui, 9);
		sTemp.lockmisc[1] = GetUInt(ui, 10);
		sTemp.lockmisc[2] = GetUInt(ui, 11);
		sTemp.lockmisc[3] = GetUInt(ui, 12);
		sTemp.lockmisc[4] = GetUInt(ui, 13);
		sTemp.lockmisc[5] = GetUInt(ui, 14);
		sTemp.lockmisc[6] = GetUInt(ui, 15);
		sTemp.lockmisc[7] = GetUInt(ui, 16);

		sTemp.minlockskill[0] = GetUInt(ui, 17);
		sTemp.minlockskill[1] = GetUInt(ui, 18);
		sTemp.minlockskill[2] = GetUInt(ui, 19);
		sTemp.minlockskill[3] = GetUInt(ui, 20);
		sTemp.minlockskill[4] = GetUInt(ui, 21);
		sTemp.minlockskill[5] = GetUInt(ui, 22);
		sTemp.minlockskill[6] = GetUInt(ui, 23);
		sTemp.minlockskill[7] = GetUInt(ui, 24);

		m_LockObjects[sTemp.Id] = sTemp;
	}

	return true;
}

Lock* CLockDB::GetObjectLock(ui32 SpellFocus)
{
	stdext::hash_map<ui32, Lock>::iterator it = m_LockObjects.find(SpellFocus);
	
	if(it != m_LockObjects.end())
		return &it->second;
	
	return NULL;
}
