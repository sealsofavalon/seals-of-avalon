
#include "DBFile.h"

struct Lock;

class CLockDB : public CDBFile
{
public:

	virtual bool Load(const char* pcName);

	Lock* GetObjectLock(ui32 SpellFocus );


private:
	stdext::hash_map<ui32, Lock > m_LockObjects;
};

extern CLockDB* g_LockObjects;
