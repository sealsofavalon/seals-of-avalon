
#include "StdAfx.h"
#include "MagicDB.h"

bool CMagicDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	std::string strMagicName;
	unsigned short usValue;
	for( unsigned int ui = 0; ui < GetRecordCount(); ui++ )
	{
		usValue = GetUShort(ui, CMagicDB::ID); 
		GetString(ui, CMagicDB::Name, strMagicName);

		usValue = GetUShort(ui, CMagicDB::HP);
		usValue = GetUShort(ui, CMagicDB::MP);
		usValue = GetUShort(ui, CMagicDB::HPPercent);
		usValue = GetUShort(ui, CMagicDB::MPPercent);
		usValue = GetUShort(ui, CMagicDB::MaxDistance);
		usValue = GetUShort(ui, CMagicDB::MinDistance);
		usValue = GetUShort(ui, CMagicDB::FacingAngle);
		usValue = GetUShort(ui, CMagicDB::MagicAttackMax);
		usValue = GetUShort(ui, CMagicDB::MagicAttackMin);
		usValue = GetUShort(ui, CMagicDB::CoolDown);
		usValue = GetUShort(ui, CMagicDB::NatureType);
		usValue = GetUShort(ui, CMagicDB::MotionID);
	}

	return true;
}