#pragma once

#include "DBFile.h"
class CMagicDB : public CDBFile
{
public:
	enum
	{
		ID = 0, //unsigned short
		Name = 1, //string
		HP = 2, //unsigned short
		MP = 3, //unsigned short
		HPPercent = 4, //unsigned short
		MPPercent = 5, //unsigned short
		MaxDistance = 6, //unsigned short
		MinDistance = 7, //unsigned short
		FacingAngle = 8, //unsigned short
		MagicAttackMax = 9, //unsigned short
		MagicAttackMin = 10, //unsigned short
		CoolDown = 11, //unsigned short
		NatureType = 12,//unsigned short
		MotionID = 13, //unsigned short
	};

	virtual bool Load(const char* pcName);
};