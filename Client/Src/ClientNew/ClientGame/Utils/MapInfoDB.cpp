#include "StdAfx.h"
#include "MapInfoDB.h"

CMapInfoDB* g_pkMapInfoDB = NULL;

CMapInfoDB::CMapInfoDB()
{
}

CMapInfoDB::~CMapInfoDB()
{
}

bool CMapInfoDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	uint32 uiRecordCount = GetRecordCount();
	for (unsigned int i=0; i<uiRecordCount; i++) {
		MapInfoEntry stMapInfo;
		stMapInfo.entry = this->GetUInt(i, 0); //I
		stMapInfo.screenid = this->GetUInt(i, 1); //I
		stMapInfo.type = GetUInt(i, 2); //I
		stMapInfo.maxplayers = GetUInt(i, 3); //I
		stMapInfo.minlevel = GetUInt(i, 4); //I
		stMapInfo.repopx = GetFloat(i, 5); //N
		stMapInfo.repopy = GetFloat(i, 6); //N
		stMapInfo.repopz = GetFloat(i, 7); //N
		stMapInfo.repopenty = GetUInt(i, 8); //I
		GetTranString(i, 9, stMapInfo.area_name); //A 255
		stMapInfo.flags = GetUInt(i, 10); //I
		stMapInfo.cooldown = GetUInt(i, 11); //I
		stMapInfo.lvl_mod_a = GetUInt(i, 12); //I
		stMapInfo.required_quest = GetUInt(i, 13); //I
		stMapInfo.required_item = GetUInt(i, 14);	//I
		stMapInfo.heroic_keyid_1 = GetUInt(i, 15); //I
		stMapInfo.heroic_keyid_2 = GetUInt(i, 16); //I
		stMapInfo.viewingDistance = GetFloat(i, 17); //N
		stMapInfo.required_checkpoint = GetUInt(i, 18); //I
		GetTranString(i, 19, stMapInfo.name); //A
		GetTranString(i, 20, stMapInfo.desc);
		GetTranString(i, 21, stMapInfo.region);

		m_mMapInfo.insert(MAPINFO_MAP_VAL(stMapInfo.entry, stMapInfo));
	}
	return true;
}

bool CMapInfoDB::GetMapInfo(unsigned int uiMapID, MapInfoEntry& mapInfo)
{
	MAPINFO_ITER it = m_mMapInfo.find(uiMapID);
	if (it != m_mMapInfo.end()) {
		mapInfo = it->second;
		return true;
	}

	return false;
}

bool CMapInfoDB::GetMapVInfoFromFlags(unsigned int flags, std::vector<MapInfoEntry>& vmapInfo)
{
	MAPINFO_ITER it = m_mMapInfo.begin();
	while(it != m_mMapInfo.end())
	{
		if (it->second.flags == flags)
		{
			vmapInfo.push_back(it->second);
		}
		++it;
	}
	return vmapInfo.size()?true:false;
}