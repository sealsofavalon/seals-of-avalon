#ifndef _MAP_DB_
#define _MAP_DB_


#include "DBFile.h"


class CMapInfoDB : public CDBFile
{
public:
	CMapInfoDB();
	~CMapInfoDB();

	//I		Long Integer
	//A		1 - 255	Alpha
	//N		Number

	struct MapInfoEntry
	{
		unsigned int entry; //I
		unsigned int screenid; //I
		unsigned int type; //I
		unsigned int maxplayers; //I
		unsigned int minlevel; //I
		float repopx; //N
		float repopy; //N
		float repopz; //N
		unsigned int repopenty; //I
		std::string area_name; //A
		unsigned int flags; //I
		unsigned int cooldown; //I
		unsigned int lvl_mod_a; //I
		unsigned int required_quest; //I
		unsigned int required_item;	//I
		unsigned int heroic_keyid_1; //I
		unsigned int heroic_keyid_2; //I
		float viewingDistance; //N
		unsigned int required_checkpoint; //I
		std::string name; //A
		std::string desc; //A
		std::string region;
	};
	typedef std::map<unsigned int, MapInfoEntry> MAPINFO_MAP;
	typedef MAPINFO_MAP::iterator MAPINFO_ITER;
	typedef MAPINFO_MAP::value_type MAPINFO_MAP_VAL;

	virtual bool Load(const char* pcName);
	bool GetMapInfo(unsigned int uiMapID, MapInfoEntry& mapInfo);
	bool GetMapVInfoFromFlags(unsigned int flags, std::vector<MapInfoEntry>& vmapInfo);

private:
	MAPINFO_MAP	m_mMapInfo;
};

extern CMapInfoDB* g_pkMapInfoDB;


#endif //_MAP_DB_