#include "StdAfx.h"
#include "MapToMapPathInfo.h"
#include "../../../TinyXML/TinyXML.h"

CMapToMapPathInfo* g_pkMapToMapPathInfo = NULL;

CMapToMapPathInfo::CMapToMapPathInfo(void)
{

}

CMapToMapPathInfo::~CMapToMapPathInfo(void)
{
}

bool CMapToMapPathInfo::LoadMapFile()
{
	TiXmlDocument xDoc;
	NiFile* pkFile = NiFile::GetFile("Data\\Map.xml",  NiFile::READ_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return false;
	}

	char* pkBuffer = NiAlloc(char, pkFile->GetFileSize() + 1);
	pkFile->Read(pkBuffer, pkFile->GetFileSize());
	pkBuffer[pkFile->GetFileSize()] = '\0';
	xDoc.Parse(pkBuffer);
	NiFree(pkBuffer);
	NiDelete pkFile;

	TiXmlElement* pkRoot = xDoc.RootElement();
	if(pkRoot == NULL)
	{
		return false;
	}
	if(!NiStricmp(pkRoot->Value(), "Map"))
	{
		return false;
	}

	TiXmlElement* pkMapdElem = pkRoot->FirstChildElement("Map");
	const char* pszValue = pkMapdElem->Attribute("MapId");
	while(pkMapdElem)
	{
		pszValue = pkMapdElem->Attribute("MapId");
		unsigned int uiMapId = (unsigned int)atoi(pszValue);
		NpcLists TempNpcList;
		TiXmlElement* pkNpcElem = pkMapdElem->FirstChildElement("NPC");
		while(pkNpcElem)
		{
			stMapToMapPath stTemp;
			pszValue = pkNpcElem->Attribute("NpcId");
			stTemp.uiNpcId = (unsigned int)atoi(pszValue);

			pszValue = pkNpcElem->Attribute("PosX");
			float fX = (float)atoi(pszValue);
			pszValue = pkNpcElem->Attribute("PosY");
			float fY = (float)atoi(pszValue);
			pszValue = pkNpcElem->Attribute("PosZ");
			float fZ = (float)atoi(pszValue);
			stTemp.NpcPos = NiPoint3(fX, fY, fZ);
			pszValue = pkNpcElem->Attribute("NpcName");
			stTemp.NpcName = _TRAN(pszValue) ;

			TiXmlElement* pkMapIndex = pkNpcElem->FirstChildElement();
			while(pkMapIndex)
			{
				stMapToMapPath::MapTranse stTTemp ;
				pszValue = pkMapIndex->Attribute("Index");
				stTTemp.MapID = atoi(pszValue);
				pszValue = pkMapIndex->Attribute("TraceIndex");
				stTTemp.TraceIndex = atoi(pszValue);
				pszValue = pkMapIndex->Attribute("TraceRace");
				stTTemp.UserRace = atoi(pszValue);

				stTemp.MapIndex.push_back(stTTemp);
				pkMapIndex = pkMapIndex->NextSiblingElement();
			}
			pkNpcElem = pkNpcElem->NextSiblingElement();
			TempNpcList.push_back(stTemp);
		}

		m_MapToMapPathInfo.insert(std::make_pair(uiMapId, TempNpcList));

		pkMapdElem = pkMapdElem->NextSiblingElement();
	}
	return true;
}
bool CMapToMapPathInfo::GetMapToMapInfo(unsigned int uiSrcMapId, unsigned int uiDestMapId,vector<stMapToMapPath>& Info,int Race, vector<int>& delMap)
{
	//先查找查询过的地图在已经排除列表里
	//如果在 直接返回FALSE
	for (int uiY = 0; uiY < delMap.size(); uiY++)
	{
		if (delMap[uiY] == uiSrcMapId)
		{
			return false ;
		}
	}
	std::map<unsigned int, NpcLists>::iterator it = m_MapToMapPathInfo.find(uiSrcMapId);
	if(it != m_MapToMapPathInfo.end())
	{
		NpcLists tempList = it->second;

		for(unsigned int ui = 0; ui < tempList.size(); ui++)
		{
			UINT NeedContinu = 0;
			for(unsigned int uiX = 0; uiX < tempList[ui].MapIndex.size(); uiX++)
			{
				
				//权值. 种族不对不允许通过
				bool bRaceUser = (tempList[ui].MapIndex[uiX].UserRace == RACE_MIN || tempList[ui].MapIndex[uiX].UserRace == Race);
				if(tempList[ui].MapIndex[uiX].MapID == uiDestMapId && bRaceUser )
				{
					//先查找能否从本地图直接能寻找到下一个地图；

					stMapToMapPath Trance ;
					Trance.NpcName = tempList[ui].NpcName;
					Trance.uiNpcId = tempList[ui].uiNpcId;
					Trance.NpcPos = tempList[ui].NpcPos;
					Trance.MapIndex.push_back(tempList[ui].MapIndex[uiX]);

					Info.push_back(Trance);
					return true;
				}
			}
		}
		delMap.push_back(uiSrcMapId); //把已经查询过的地图丢进列表
		
		//在从当前可以到达的子地图寻找
		for (unsigned int ui = 0; ui < tempList.size(); ui++)
		{//
			for(unsigned int uiX = 0; uiX < tempList[ui].MapIndex.size(); uiX++)
			{
				//权值 如果当前地图是不可到达的 子地图也不需要遍历
				bool bRaceUser = (tempList[ui].MapIndex[uiX].UserRace == RACE_MIN || tempList[ui].MapIndex[uiX].UserRace == Race);
				if (!bRaceUser)
				{
					Info.clear() ;
					continue ;
				}

				unsigned int newMapID = tempList[ui].MapIndex[uiX].MapID ;
				if (GetMapToMapInfo(newMapID, uiDestMapId ,Info,Race, delMap))
				{
					//返回跳转到下一个地图的信息。然后从下一个地图着手。

					stMapToMapPath Trance ;
					Trance.NpcName = tempList[ui].NpcName;
					Trance.uiNpcId = tempList[ui].uiNpcId;
					Trance.NpcPos = tempList[ui].NpcPos;
					Trance.MapIndex.push_back(tempList[ui].MapIndex[uiX]);

					Info.push_back(Trance);
					return true ;
				}else 
				{
					Info.clear() ;
				}
			}	
		}
	}
	return false ;
}

bool CMapToMapPathInfo::GetNpcPos(unsigned int uiSrcMapId, unsigned int uiDestMapId, NiPoint3& kPos)
{
	std::map<unsigned int, NpcLists>::iterator it = m_MapToMapPathInfo.find(uiSrcMapId);
	if(it != m_MapToMapPathInfo.end())
	{
		NpcLists tempList = it->second;
	
		for(unsigned int ui = 0; ui < tempList.size(); ui++)
		{
			for(unsigned int uiX = 0; uiX < tempList[ui].MapIndex.size(); uiX++)
			{
				if(tempList[ui].MapIndex[uiX].MapID == uiDestMapId)
				{
					kPos = tempList[ui].NpcPos;

					return true;
				}
			}
		}
		//for(unsigned int ui = 0; ui < stTemp.MapIndex.size(); ui++)
		//{
		//	if(stTemp.MapIndex[ui] == uiDestMapId)
		//	{
		//		kPos = stTemp.NpcPos;

		//		return true;
		//	}
		//}
	}
	
	return false;
}