#pragma once

struct stMapToMapPath
{
	unsigned int uiNpcId;
	NiPoint3 NpcPos;
	std::string NpcName ;
	struct MapTranse
	{
		unsigned int MapID ;
		unsigned int TraceIndex;
		unsigned int UserRace ;
	};
	std::vector<MapTranse> MapIndex;

	stMapToMapPath   operator= (stMapToMapPath info)
	{
		if (this == &info)
		{
			return *this ;
		}
		uiNpcId = info.uiNpcId ;
		NpcPos = info.NpcPos ;
		NpcName = info.NpcName ;
		MapIndex = info.MapIndex ;

		return *this;
	}
};

class CMapToMapPathInfo
{
public:
	CMapToMapPathInfo();
	~CMapToMapPathInfo();

	bool LoadMapFile();

	bool GetNpcPos(unsigned int uiSrcMapId, unsigned int uiDestMapId, NiPoint3& kPos);
	bool GetMapToMapInfo(unsigned int uiSrcMapId, unsigned int uiDestMapId,vector<stMapToMapPath>& Info, int Race, vector<int>& delMap);

protected:
	typedef std::vector<stMapToMapPath> NpcLists;
	NpcLists m_vNpcLists;
	std::map<unsigned int, NpcLists> m_MapToMapPathInfo;

};

extern CMapToMapPathInfo* g_pkMapToMapPathInfo;