#include "stdafx.h"
#include "NPCListFile.h"


NPCLisdFile* g_pkMapNpcInfo = NULL ;
NPCLisdFile::NPCLisdFile()
:m_pcData(NULL)
{

}
NPCLisdFile::~NPCLisdFile()
{
	UnLoad();
}

static BOOL scanforchar(const WCHAR *pSubStr, int& idx, WCHAR c)
{
	UINT startidx = idx;
	while(pSubStr[idx] != c && pSubStr[idx] && pSubStr[idx] != L':' && pSubStr[idx] != L'>' && pSubStr[idx] != L'\n')
		idx++;
	return pSubStr[idx] == c && startidx != idx;
}
static BOOL scanforchar(const char *pSubStr, int& idx, char c)
{
	UINT startidx = idx;
	while(pSubStr[idx] != c && pSubStr[idx] && pSubStr[idx] != ':' && pSubStr[idx] != '>' && pSubStr[idx] != '\n')
		idx++;
	return pSubStr[idx] == c && startidx != idx;
}

bool  NPCLisdFile::Load(const char *pName)
{
	NiFile* pkFile = NiFile::GetFile(pName, NiFile::READ_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return false;
	}

	m_uiSize = pkFile->GetFileSize();
	if( m_uiSize == 0 )
	{
		NiDelete pkFile;
		return false;
	}

	m_pcData = NiAlloc(char, m_uiSize);

	pkFile->Read(m_pcData, m_uiSize);
	NiDelete pkFile;


	int curURL = 0; 
	int URLTEXTBEGIN = 0;
	int URLTEXTEND = 0;
	int ScanPos = 0;
	int idx = 0;
	UINT len;
	int textStart = 0;
	int BeginPos = 0;
	int EndPos = 0;

	WCHAR pSubStrData[1024];
	m_DescW.Set(m_pcData);
	stMapNPCInfo* newNPC = NULL ;
	const int nStrLen = m_DescW.GetLength();
	for(;;)
	{
		const WCHAR curChar = m_DescW[ScanPos];

		if(!curChar)
			break;
		if(curChar == L'<')
		{
			const WCHAR* pSubStr = &m_DescW[ScanPos];
			if(!wcsnicmp(pSubStr +1, L"a:", 2))
			{
				BeginPos = ScanPos;
				curURL = ScanPos;
				idx = 3;
				if(!scanforchar(pSubStr, idx, L'>'))
					goto textemit;

				if (!m_DescW.SubStr(pSubStrData, ScanPos + 3, idx - 3))
					break;
				
				newNPC = new stMapNPCInfo;
				Parse(pSubStrData, newNPC);

				ScanPos += idx + 1;
				URLTEXTBEGIN = ScanPos;
				continue;
			}

			if(!wcsnicmp(pSubStr+1, L"/a>", 3))
			{
				EndPos = ScanPos + 4;
				URLTEXTEND = ScanPos;
				WCHAR pSubStrT[1024];
				if ( m_DescW.SubStr(pSubStrT, URLTEXTBEGIN, URLTEXTEND - URLTEXTBEGIN) )
				{
					newNPC->name = pSubStrT;
					MapNpcList.push_back(newNPC);
				}
				URLTEXTBEGIN = 0;
				URLTEXTEND = 0;
				curURL = 0;
				ScanPos += 4;
				continue;
			}
		}
textemit:
		textStart = ScanPos;
		idx = 1;
		while( m_DescW[ScanPos+idx] != L'\t' 
			&& m_DescW[ScanPos+idx] != L'<' 
			&& m_DescW[ScanPos+idx] != L'\n' 
			&& m_DescW[ScanPos+idx] )
		{
			idx++;
		}
		len = idx;
		ScanPos += idx;
	}

	return true ;
}
bool NPCLisdFile::GetMapNPCList(int mapid, vector<stMapNPCInfo*>& NpcList)
{
	bool bGet = false ;
	for (size_t ui = 0; ui <MapNpcList.size(); ui++)
	{
		if (MapNpcList[ui]->MapID == mapid)
		{
			NpcList.push_back(MapNpcList[ui]);
			bGet = true;
		}
	}

	return bGet ;
}
bool NPCLisdFile::Parse(const WCHAR* Data, stMapNPCInfo* pkNew)
{
	int iMapId = 0;
	int iPosX, iPosY, iPosZ;

	NiPoint3 kPos = NiPoint3::ZERO ;
	UString ustr;
	ustr.Set(Data);

	const string strURL = ustr.GetBuffer();

	int iTmp0, iTmp1;
	iTmp0 = strURL.find_first_of(" ");
	iTmp1 = strURL.find(" ", iTmp0 + 1);
	iMapId = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());

	iTmp0 = strURL.find(" ", iTmp1);
	//iTmp0 = strURL.find_first_of(" ");
	iTmp1 = strURL.find(" ", iTmp0 + 1);
	iPosX = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());

	iTmp0 = strURL.find(" ", iTmp1);
	iTmp1 = strURL.find(" ", iTmp0 + 1);
	iPosY = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());

	iTmp0 = strURL.find(" ", iTmp1);
	iTmp1 = strURL.find(" ", iTmp0 + 1);
	iPosZ = atoi(strURL.substr(iTmp0, iTmp1 - iTmp0).c_str());

	kPos.x = (float)iPosX;
	kPos.y = (float)iPosY;
	kPos.z = (float)iPosZ;

	pkNew->MapID = iMapId;
	pkNew->NpcPos = kPos;
	
	return true;

}
void NPCLisdFile::UnLoad()
{
	for (size_t ui=0; ui<MapNpcList.size(); ui++)
	{
		if (MapNpcList[ui])
		{
			delete MapNpcList[ui];
			MapNpcList[ui] = NULL;
		}
	}
	if (m_pcData != NULL)
	{
		NiFree(m_pcData);
		m_pcData = NULL;
	}
}