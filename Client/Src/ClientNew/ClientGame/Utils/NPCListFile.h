#ifndef __NPCLisdFile_H__
#define __NPCLisdFile_H__



struct stMapNPCInfo
{
	basic_string<WCHAR> name ;
	NiPoint3 NpcPos;
	int MapID;
};

class  NPCLisdFile : public NiMemObject
{
public:
	NPCLisdFile();
	~NPCLisdFile();

	bool Load(const char* pName);
	void UnLoad();

	bool Parse(const WCHAR* Data, stMapNPCInfo* pkNew);
	bool GetMapNPCList(int mapid, vector<stMapNPCInfo*>& NpcList);
protected:
	vector<stMapNPCInfo*> MapNpcList ;
	char* m_pcData;
	UStringW m_DescW;
	unsigned int m_uiSize;
private:
};

extern NPCLisdFile* g_pkMapNpcInfo;
#endif