#pragma once

#include "DBFile.h"

//class CNewBirdDB : public CDBFile
//{
//public:
//    enum
//	{
//        ID = 0,
//		SubCategroy,
//		Desc,//string
//	};
//
//    virtual bool Load(const char* pcName);
//
//	bool GetDesc(uint16 Id, std::string &str);
//
//	std::map<uint16, std::string> m_NewbirdDescMap;
//};
//extern CNewBirdDB* g_pkNewBirdDB;

struct SubCategroyStruct
{
	uint16 Id;
	std::string title;
};
struct NewBirdCategroyStruct
{
	uint16 CategoryID;
	std::vector<SubCategroyStruct> SubCatgory;
};
class CNewBirdCategroyDB : public CDBFile
{
public:
	enum
	{
		ID = 0,
		Categroy,
		SubCategroy,
		Title, //string
	};

	virtual bool Load(const char* pcName);

	bool GetCategroyContent(uint16 CategoryID, std::vector<SubCategroyStruct> &vec);

	void AppNewItem(uint16 Id, uint16 SubId, const char* title);

	std::vector<NewBirdCategroyStruct> m_VCategroy;
};

extern CNewBirdCategroyDB* g_pkNewBirdCategroyDB;