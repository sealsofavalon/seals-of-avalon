#include "StdAfx.h"
#include "NewBirdEventDB.h"

CNewBirdEventDB* g_pkNewBirdEventDB = NULL;
bool CNewBirdEventDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	std::string title;
	std::string info;
	unsigned int Eventid;

	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
		Eventid = GetUInt(ui, CNewBirdEventDB::eventId);
		GetTranString(ui, CNewBirdEventDB::title, title);
		GetTranString(ui, CNewBirdEventDB::info, info);
		AddNewEntry(Eventid, title, info);
	}
	return true;
}
void CNewBirdEventDB::AddNewEntry(unsigned int eventId, std::string& title, std::string& info)
{
	for (UINT i = 0 ; i < m_NewBirdEventStr.size(); i++)
	{
		if (m_NewBirdEventStr[i].eventId == eventId)
		{
			m_NewBirdEventStr[i].info.push_back(info);
			return;
		}
	}
	NewBirdEventStruct newentry;
	newentry.eventId = eventId;
	newentry.title = title;
	newentry.info.push_back(info);
	m_NewBirdEventStr.push_back(newentry);
}
bool CNewBirdEventDB::GetNewBirdEventStr(unsigned int eventid, std::vector<std::string>& info)
{
	for (UINT i = 0 ; i < m_NewBirdEventStr.size(); i++)
	{
		if (m_NewBirdEventStr[i].eventId == eventid)
		{
			for (UINT j = 0 ; j < m_NewBirdEventStr[i].info.size() ; j++)
			{
				info.push_back(m_NewBirdEventStr[i].info[j]);
			}
			return true;
		}
	}
	return false;
}
bool CNewBirdEventDB::GetNewBirdEventTitle(unsigned int eventid, std::string& title)
{
	for (UINT i = 0 ; i < m_NewBirdEventStr.size(); i++)
	{
		if (m_NewBirdEventStr[i].eventId == eventid)
		{
			title = m_NewBirdEventStr[i].title;
			return true;
		}
	}
	return false;
}