#include "StdAfx.h"
#include "NpcTextDB.h"

CNpcTextDB* g_pkNpcTex = NULL;

bool CNpcTextDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		unsigned int uiNpcTextId;
		std::string strText;
		std::string str ;
	
		uiNpcTextId = GetUInt(ui, 0);
		GetString(ui, 1, strText);
		str += strText;
		GetString(ui, 2, strText);
		str += strText;
		GetString(ui, 3, strText);
		str += strText;
		GetString(ui, 4, strText);
		str += strText;
		GetString(ui, 5, strText);
		str += strText;
		if (LangaugeMgr)
		{
			str = LangaugeMgr->GetNoticeText(str.c_str());
		}
		

		m_NpcText.insert(make_pair(uiNpcTextId, str));
	}

	return true;
}

bool CNpcTextDB::GetNpcText(unsigned int uiTextId, std::string& strText)
{
	std::map<unsigned int, std::string>::iterator it = m_NpcText.find(uiTextId);

	if(it != m_NpcText.end())
	{
		strText = it->second;

		return true;
	}

	return false;
}