#ifndef NPCTEXTDB_H
#define NPCTEXTDB_H

#include "DBFile.h"


class CNpcTextDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);

	bool GetNpcText(unsigned int uiTextId, std::string& strText);

private:
	std::map<unsigned int, std::string> m_NpcText;
};

extern CNpcTextDB* g_pkNpcTex;

#endif