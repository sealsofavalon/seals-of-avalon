#include "StdAfx.h"
#include "QuestFinisherDB.h"

CQuestFinisherDB* g_pkQuestFinisherDB = NULL;

bool CQuestFinisherDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	stdext::hash_map<unsigned int, unsigned int>::iterator it;
	for(unsigned int ui = 0; ui < GetRecordCount(); ++ui)
	{
		unsigned int uiNpcId = GetUInt(ui, 0);
		unsigned int uiQuestId = GetUInt(ui, 1);

		if( m_vQuestFinisherDB.find(uiQuestId) != m_vQuestFinisherDB.end())
		{
			/*char szbuf[128];
			sprintf(szbuf, "quest finisher[%d] has already existed!\n", uiQuestId);
			OutputDebugString(szbuf);*/
		}
		m_vQuestFinisherDB.insert(make_pair(uiQuestId, uiNpcId));
	}

	return true;
}

bool CQuestFinisherDB::GetNpcId(unsigned int uiQuestId, unsigned int& uiNpcId)
{
	stdext::hash_map<unsigned int, unsigned int>::iterator it = m_vQuestFinisherDB.find(uiQuestId);
	if(it != m_vQuestFinisherDB.end())
	{
		uiNpcId = it->second;

		return true;
	}

	return false;
}

uint32 CQuestFinisherDB::GetQuestByNpc(uint32 npc)
{
	stdext::hash_map<unsigned int, unsigned int>::iterator it = m_vQuestFinisherDB.begin();
	while(it != m_vQuestFinisherDB.end())
	{
		uint32 uiNpcId = it->second;
		if( uiNpcId == npc )
			return it->first;
		++it;
	}
	return 0;
}

vector<uint32> CQuestFinisherDB::QuestList(uint32 npc)
{
	vector<uint32> v;
	stdext::hash_map<unsigned int, unsigned int>::iterator it = m_vQuestFinisherDB.begin();
	while(it != m_vQuestFinisherDB.end())
	{
		uint32 uiNpcId = it->second;
		if( uiNpcId == npc )
			v.push_back(it->first);
		++it;
	}
	return v;
}