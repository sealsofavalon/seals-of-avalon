#ifndef QUESTFINISHERDB_H
#define QUESTFINISHERDB_H

#include "DBFile.h"
#include <hash_map>

class CQuestFinisherDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);

	bool GetNpcId(unsigned int uiQuestId, unsigned int& uiNpcId);
	uint32 GetQuestByNpc(uint32 npc);
	vector<uint32> QuestList(uint32 npc);
private:
	stdext::hash_map<unsigned int, unsigned int>	m_vQuestFinisherDB;
};

extern CQuestFinisherDB* g_pkQuestFinisherDB;

#endif