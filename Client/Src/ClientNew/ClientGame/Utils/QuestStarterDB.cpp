#include "StdAfx.h"
#include "QuestStarterDB.h"

CQuestStarterDB* g_pkQuestStarterDB = NULL;

bool CQuestStarterDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	map< unsigned int, vector<stQuestStarter> >::iterator it;
	stQuestStarter sTemp;
	for(unsigned int ui = 0; ui < GetRecordCount(); ++ui)
	{
		sTemp.uiNpcId = GetUInt(ui, 0);
		sTemp.uiQuestId = GetUInt(ui, 1);

		it = m_vQuestStarterDB.find(sTemp.uiNpcId);
		if(it == m_vQuestStarterDB.end())
		{
			vector<stQuestStarter> v;
			v.push_back(sTemp);
			m_vQuestStarterDB.insert(make_pair(sTemp.uiNpcId, v));
		}
		else
		{
			vector<stQuestStarter>& v = it->second;
			v.push_back(sTemp);
		}
	}

	return true;
}

bool CQuestStarterDB::IsQuestNpc(unsigned int uiNpcId)
{
	map< unsigned int, vector<stQuestStarter> >::iterator it = m_vQuestStarterDB.find(uiNpcId);
	if(it != m_vQuestStarterDB.end())
		return true;

	return false;
}
bool CQuestStarterDB::GetNpcId(unsigned int uiQuestId, unsigned int& uiNpcId)
{
	map<unsigned int, vector<stQuestStarter>>::iterator it = m_vQuestStarterDB.begin();
	while(it != m_vQuestStarterDB.end())
	{
		vector<stQuestStarter>& v  = it->second;
		for (size_t i =0; i < v.size(); i++)
		{
			if (v[i].uiQuestId == uiQuestId)
			{
				uiNpcId = v[i].uiNpcId;
				return true;
			}
		}
		++it;
	}
	return false;
}

vector<CQuestStarterDB::stQuestStarter>* CQuestStarterDB::QuestList(unsigned int uiNpcId)
{
	map< unsigned int, vector<stQuestStarter> >::iterator it = m_vQuestStarterDB.find(uiNpcId);
	if(it != m_vQuestStarterDB.end())
	{
		return &it->second;
	}

	return NULL;
}