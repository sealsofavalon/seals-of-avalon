#ifndef QUESTSTARTERDB_H
#define QUESTSTARTERDB_H

#include "DBFile.h"

class CQuestStarterDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);

	struct stQuestStarter
	{
		unsigned int uiNpcId;
		unsigned int uiQuestId;
	};

	bool IsQuestNpc(unsigned int uiNpcId);
	bool GetNpcId(unsigned int uiQuestId, unsigned int& uiNpcId);

	vector<CQuestStarterDB::stQuestStarter>* QuestList(unsigned int uiNpcId);

private:
	map<unsigned int, vector<stQuestStarter> >	m_vQuestStarterDB;
};

extern CQuestStarterDB* g_pkQuestStarterDB;

#endif