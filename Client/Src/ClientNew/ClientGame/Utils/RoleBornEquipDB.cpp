#include "StdAfx.h"
#include "RoleBornEquipDB.h"

CRoleBornEquipDB* g_pkRoleBornEquip = NULL;

bool CRoleBornEquipDB::Load(const char *pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	unsigned int uiRace;
	unsigned int uiClass;
	unsigned int uiGender;
	unsigned int uiValidId;
	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		uiRace = GetUInt(ui, 0);
		uiClass = GetUInt(ui, 1);
		uiGender = GetUInt(ui, 2);
		//uiValidId = uiRace + (uiClass<<8) + (uiGender<<16);
		uiValidId = uiRace + (uiGender<<16);
		sRoleBornEquip RoleBornEquip;
		RoleBornEquip.uiValidId = uiValidId;
		RoleBornEquip.uiModel = GetUInt(ui, 3);
		for(int i = 0; i < 5; i++)
		{
			ui32 uiId = GetUInt(ui, 4 + i);
			RoleBornEquip.uiDefault[i] = uiId;
		}
		m_RoleBornEquip.push_back(RoleBornEquip);
	}

	return true;
}

bool CRoleBornEquipDB::FindRoleBornEquip(ui32 uiValidId, ui32 uiPart, ui32& uiModelId)
{
	for(unsigned int ui = 0; ui < m_RoleBornEquip.size(); ui++)
	{
		if(m_RoleBornEquip[ui].uiValidId = uiValidId)
		{
			uiModelId = m_RoleBornEquip[ui].uiDefault[uiPart];

			return true;
		}
	}

	return false;
}
