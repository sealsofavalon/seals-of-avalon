#ifndef ROLEBORNEQUIP_H
#define ROLEBORNEQUIP_H

#include "DBFile.h"

class CRoleBornEquipDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);

	struct sRoleBornEquip
	{
		ui32 uiValidId;
		ui32 uiModel;
		ui32 uiDefault[5];
	};

	bool FindRoleBornEquip(ui32 uiValidId, ui32 uiPart, ui32& uiModelId);

private:
	std::vector<sRoleBornEquip> m_RoleBornEquip;
};

extern CRoleBornEquipDB* g_pkRoleBornEquip;

#endif