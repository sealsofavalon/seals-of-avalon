#include "stdafx.h"
#include "Soundid.h"

#include "AudioInterface.h"

SoundDB* g_pkSoundFiles = 0;

bool SoundDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	unsigned int ui = 0;
	for ( ui; ui < GetRecordCount(); ui++ )
	{
		soundfile sf;

		sf.id = GetInt(ui, 0);
		GetString(ui, 1, sf.filename);

		m_soundfiles.insert(std::make_pair(sf.id, sf));

		/*char soundfile[64];

		_snprintf(soundfile, 64, "sound/char/%s.wav", sf.filename.c_str());

		ALAudioSource pkSound;
		SYState()->IAudio->PlaySound(&pkSound, soundfile);
		pkSound.Stop();*/

		/*FILE* file = NULL;

		file = fopen("LoadAudio.txt", "a");
		if ( file )
		{
			fprintf( file, "preload: %s\n", soundfile );
			fclose(file);
		}*/

	}

	return true;
}

bool SoundDB::GetSoundFile(unsigned int id, soundfile& sf)
{
	std::map<int, soundfile>::iterator it = m_soundfiles.find(id);
	if (it != m_soundfiles.end())
	{
		sf = it->second;
		return true;
	}

	return false;
}


