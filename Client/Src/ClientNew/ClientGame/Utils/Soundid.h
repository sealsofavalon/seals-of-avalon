#pragma once

#include "DBFile.h"

class SoundDB : public CDBFile
{
public:
	struct soundfile
	{
		int id;
		std::string filename;
	};

	virtual bool Load(const char* pcName);
	bool GetSoundFile(unsigned int id, soundfile& sf);

private:
	std::map<int, soundfile> m_soundfiles;
};


extern SoundDB* g_pkSoundFiles;

