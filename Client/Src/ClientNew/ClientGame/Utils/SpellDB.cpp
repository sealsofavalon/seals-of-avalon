#include "StdAfx.h"
#include "SpellDB.h"

CSpellDB* g_pkSpellInfo = NULL;
CSpellRangeDB* g_pkSpellRangeInfo = NULL;
CSpellDurationDB* g_pkSpellDurationInfo = NULL;

bool CSpellDB::GetSpellCDTIncrease(uint32 uiSpellId, uint32& cdtspellid, uint32& cdtincrease)
{	
	stSpellInfo* info = GetSpellInfo( uiSpellId );
	if( info )
	{
		for( int i = 0; i < 3; i++ )
		{
			if( info->uiEffectApplyAuraName[i] == 250 )
			{
				cdtspellid = info->uiEffectBasePoint[i];
				cdtincrease = info->uiEffectMiscValue[i];
				return true;
			}

		}
	}
	return false;
}

bool CSpellDB::IsMountSpell(uint32 uiSpellId)
{
	stSpellInfo* info = GetSpellInfo( uiSpellId );
	if( info )
	{
		for( int i = 0; i < 3; i++ )
		{
			if( info->uiEffectApplyAuraName[i] >= 248 && info->uiEffectApplyAuraName[i] <= 249 )
			{
				return true;
			}

		}
	}
	return false;
}

bool CSpellDB::IsShapeSpell(uint32 uiSpellId)
{
	stSpellInfo* info = GetSpellInfo( uiSpellId );
	if( info )
	{
		for( int i = 0; i < 3; i++ )
		{
			if( info->uiEffectApplyAuraName[i] >= 244 && info->uiEffectApplyAuraName[i] <= 247 )
			{
				return true;
			}

		}
	}
	return false;
}

bool CSpellDB::IsSkillBook(uint32 uiSpellId)
{
	stSpellInfo* info = GetSpellInfo( uiSpellId );
	if( info )
	{
		for( int i = 0; i < 3; i++ )
		{
			if( info->uiEffectApplyAuraName[i] == 483 )
			{
				return true;
			}
		}
	}
	return false;
}

bool CSpellDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		stSpellInfo stTemp;

		stTemp.uiId = GetUInt(ui, 0);						// 技能id
		stTemp.uiCategory = GetUInt(ui, 1);					// spell category
		stTemp.uiAttribute = GetUInt(ui, 4);				// spell attribute
		stTemp.uiCastIndex = GetUInt(ui, 15);				// cast index
		stTemp.uiTargets = GetUInt(ui, 10);					// 目标类型
		stTemp.uiTargetCreatureType = GetUInt(ui, 11);		// 目标对象类型
		stTemp.uiRecoverTime = GetUInt(ui, 16);				// 
		stTemp.uiCategoryRecoveryTime = GetUInt(ui, 17);	// 冷却时间
		stTemp.uiMaxLevel = GetUInt(ui, 24);				// 最大等级
		stTemp.uiBaseLevel = GetUInt(ui, 25);				// 基础等级
		stTemp.uiSpellLevel = GetUInt(ui, 26);				// 魔法等级
		stTemp.uiDurationIndex = GetUInt(ui, 27);			// duration index
		stTemp.uiManaCost = GetUInt(ui, 29);				// 魔法消耗
		stTemp.uiRangeIndex = GetUInt(ui, 33);				// range idx [ranget table]
		stTemp.uiVisualKit = GetUInt(ui, 112);				// 对应的效果包
		stTemp.flyspeed = GetFloat(ui, 34);					// 飞行速度
		stTemp.MaxStack = GetUInt(ui, 36);
		stTemp.uiManaCostPercentage = GetUInt(ui, 121);
		stTemp.uiAuraInfo = GetUInt(ui, 113);

		stTemp.uiTotemID[0] = GetUInt(ui, 37);
		stTemp.uiTotemID[1] = GetUInt(ui, 38);

		stTemp.uiEffect[0] = GetUInt(ui, 58);
		stTemp.uiEffect[1] = GetUInt(ui, 59);
		stTemp.uiEffect[2] = GetUInt(ui, 60);

		stTemp.uiEffectDieSides[0] = GetUInt(ui, 61);
		stTemp.uiEffectDieSides[1] = GetUInt(ui, 62);
		stTemp.uiEffectDieSides[2] = GetUInt(ui, 63);

		stTemp.uiEffectBasePoint[0] = GetUInt(ui, 73);
		stTemp.uiEffectBasePoint[1] = GetUInt(ui, 74);
		stTemp.uiEffectBasePoint[2] = GetUInt(ui, 75);
		stTemp.uiEffectMiscValue[0] = GetUInt(ui, 103);
		stTemp.uiEffectMiscValue[1] = GetUInt(ui, 104);
		stTemp.uiEffectMiscValue[2] = GetUInt(ui, 105);

		stTemp.uiImplicitTarget[0] = GetUInt(ui, 79);
		stTemp.uiImplicitTarget[1] = GetUInt(ui, 80);
		stTemp.uiImplicitTarget[2] = GetUInt(ui, 81);

		stTemp.uiEffectRadius[0] = GetUInt(ui, 85);
		stTemp.uiEffectRadius[1] = GetUInt(ui, 86);
		stTemp.uiEffectRadius[2] = GetUInt(ui, 87);

		stTemp.uiEffectApplyAuraName[0] = GetUInt(ui, 88);
		stTemp.uiEffectApplyAuraName[1] = GetUInt(ui, 89);
		stTemp.uiEffectApplyAuraName[2] = GetUInt(ui, 90);

		stTemp.uiEffectChainTarget[0] = GetUInt(ui, 97);
		stTemp.uiEffectChainTarget[1] = GetUInt(ui, 98);
		stTemp.uiEffectChainTarget[2] = GetUInt(ui, 99);

		GetTranString(ui, 117, stTemp.name);					// spell name

		stTemp.uiStartRecoveryTime = GetUInt(ui, 122);
		stTemp.uiMaxTargets = GetUInt(ui, 126);				// max target nb
		GetTranString(ui, 119, stTemp.description);				// spell intro
		GetString(ui, 135, stTemp.strIcon);					// icon 
		stTemp.uiSpellGroupType = GetUInt(ui, 125);					// SpellGroupType 
		stTemp.uiLineNameEntry = GetUInt(ui, 144);
		stTemp.uiSubLineNameEntry = GetUInt(ui, 150);

		m_SpellInfo.insert(std::make_pair(stTemp.uiId, stTemp));
	}

	return true;
}

stSpellInfo* CSpellDB::GetSpellInfo(unsigned int uiSpellId)
{
	std::map<unsigned int, stSpellInfo>::iterator it = m_SpellInfo.find(uiSpellId);

	if(it != m_SpellInfo.end())
		return &it->second;

	return NULL;
}

std::map<unsigned int, stSpellInfo>& CSpellDB::GetSpellInfos()
{
	return m_SpellInfo;
}

bool CSpellRangeDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stSpellRangeInfo record;
		record.entry = GetUInt(ui, 0);
		record.minrange = GetFloat(ui, 1);
		record.maxrange = GetFloat(ui, 2);

		m_SpellRangeInfo.insert(std::make_pair(record.entry, record));
	}

	return true;
}


stSpellRangeInfo* CSpellRangeDB::GetSpellRangeInfo(unsigned int uiEntryIdx)
{
	std::map<uint32, stSpellRangeInfo>::iterator it = m_SpellRangeInfo.find(uiEntryIdx);

	if (it != m_SpellRangeInfo.end())
		return &it->second;

	return NULL;
}


bool CSpellDurationDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stSpellDurationInfo record;
		record.entry = GetUInt(ui, 0);
		record.duration1 = GetUInt(ui, 1);
		record.duration2 = GetUInt(ui, 2);
		record.duration3 = GetUInt(ui, 3);

		m_SpellDurationInfo.insert(std::make_pair(record.entry, record));
	}

	return true;
}

stSpellDurationInfo* CSpellDurationDB::GetSpellDurationInfo(unsigned int uiEntryIdx)
{
	std::map<uint32, stSpellDurationInfo>::iterator it = m_SpellDurationInfo.find(uiEntryIdx);
	if (it != m_SpellDurationInfo.end())
		return &it->second;

	return NULL;
}

CQuerySpellDB* g_pkQuerySpellInfo = NULL;
bool CQuerySpellDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stQuerySpellInfo record;
		record.SpellId = GetUInt(ui, 0);
		record.AllowableClass = GetUInt(ui, 1);
		record.AllowableLevel = GetUInt(ui, 2);

		m_QuerySpellInfo.push_back(record);
	}
	return true; 
}

bool CQuerySpellDB::GetQuerySpellList(std::vector<stQuerySpellInfo>& vList)
{
	if (m_QuerySpellInfo.size())
	{
		vList = m_QuerySpellInfo;
		return true;
	}
	return false;
}
CSpellRadiusDB* g_pkSpellRadiusInfo = NULL ;
bool CSpellRadiusDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;


	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stSpellRadius record;
		record.index = GetUInt(ui, 0);
		record.radius = GetFloat(ui, 1);

		m_SpellEffectRadius.push_back(record);
	}

	return true;
}

bool CSpellRadiusDB::GetRadius(ui32 index, float& fRadius)
{
	for ( int i =0; i < m_SpellEffectRadius.size(); i++)
	{
		if (m_SpellEffectRadius[i].index == index)
		{
			fRadius = m_SpellEffectRadius[i].radius;
			return true;
		}
	}
	return false ;
}


CSpellGambleDB* g_pkSpellGambleInfo = NULL;
bool CSpellGambleDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stSpellGambleInfo stTemp;
		stTemp.entry = GetUInt(ui, 0);
		stTemp.ItemEntry = GetUInt(ui, 1);
		stTemp.ItemProb = GetUInt(ui, 2);
		stTemp.min_count = GetUInt(ui, 3);
		stTemp.max_count = GetUInt(ui, 4);

		m_SpellGambleMap.insert(std::make_pair(stTemp.entry, stTemp));
	}
	return true;
}

uint32 CSpellGambleDB::GetItemEntry(uint32 SpellId)
{
	std::map<uint32 , stSpellGambleInfo>::iterator it = m_SpellGambleMap.find(SpellId);
	if (it != m_SpellGambleMap.end())
	{
		return it->second.ItemEntry;
	}
	return 0;
}

bool CSpellGambleDB::GetItemCount(uint32 SpellId, uint32* minCount, uint32* maxCount)
{
	std::map<uint32 , stSpellGambleInfo>::iterator it = m_SpellGambleMap.find(SpellId);
	if (it != m_SpellGambleMap.end())
	{
		*minCount = it->second.min_count;
		*maxCount = it->second.max_count;
		return true;
	}
	return false;
}

CSpellTrainerDB* g_pkSpellTrainerInfo = NULL;
bool CSpellTrainerDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stSpellTrainerInfo stTemp;

		uint32 entry = GetUInt(ui, 0);
		stTemp.cast_spell = GetUInt(ui, 1);
		stTemp.learn_spell = GetUInt(ui, 2);
		stTemp.spellcost = GetUInt(ui, 3);
		stTemp.require_spell = GetUInt(ui, 4);
		stTemp.require_skill = GetUInt(ui, 5);
		stTemp.require_Skillvalue = GetUInt(ui, 6);
		stTemp.require_Level = GetUInt(ui, 7);
		stTemp.deletespell = GetUInt(ui, 8);
		stTemp.is_prof = GetUInt(ui, 9);
		GetTranString(ui, 10, stTemp.name);


		std::map<uint32, std::vector<stSpellTrainerInfo>>::iterator it = m_SpellTrainerList.find(entry);
		if (it != m_SpellTrainerList.end())
		{
			it->second.push_back(stTemp);
		}
		else
		{
			std::vector<stSpellTrainerInfo> vList;
			vList.push_back(stTemp);
			m_SpellTrainerList.insert(std::make_pair(entry, vList));
		}
	}
	return true;
}

bool CSpellTrainerDB::GetSpellTrainerList(uint32 entry, std::vector<stSpellTrainerInfo>& vList)
{
	std::map<uint32, std::vector<stSpellTrainerInfo>>::iterator it = m_SpellTrainerList.find(entry);
	if (it != m_SpellTrainerList.end())
	{
		vList = it->second;
		return true;
	}
	return false;
}

CSkillLineNameDB* g_SkillLineNameInfo = NULL;
bool CSkillLineNameDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stSkillLineEntry stTemp;

		stTemp.entry = GetUInt(ui, 0);
		stTemp.type = GetUInt(ui, 1);
		GetTranString(ui, 2, stTemp.name);

		m_SkillLineMap.insert(std::make_pair(stTemp.entry, stTemp));
	}
	return true;
}

bool CSkillLineNameDB::GetSkillLineName(uint32 entry, std::string& Name)
{
	std::map<uint32, stSkillLineEntry>::iterator it = m_SkillLineMap.find(entry);
	if (it != m_SkillLineMap.end())
	{
		Name = it->second.name;
		return true;
	}
	return false;

}
uint32 CSkillLineNameDB::GetSkillLineType(uint32 entry)
{
	std::map<uint32, stSkillLineEntry>::iterator it = m_SkillLineMap.find(entry);
	if (it != m_SkillLineMap.end())
	{
		return it->second.type;
	}
	return false;
}

CSkillLineSpellDB* g_SkillLineSpellInfo = NULL;

bool CSkillLineSpellDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stSkillLineSpell stTemp;

		stTemp.entry = GetUInt(ui, 0);
		stTemp.skillLine = GetUInt(ui, 1);
		stTemp.spell = GetUInt(ui, 2);
		stTemp.next = GetUInt(ui, 3);
		stTemp.minrank = GetUInt(ui, 4);
		stTemp.grey = GetUInt(ui, 5);
		stTemp.green = GetUInt(ui, 6);

		m_SkillLineSpellMap.insert(std::make_pair(stTemp.spell, stTemp));
	}
	return true;
}
stSkillLineSpell* CSkillLineSpellDB::GetSkillLineSpell(uint32 spellId)
{
	std::map<uint32, stSkillLineSpell>::iterator it = m_SkillLineSpellMap.find(spellId);
	if (it != m_SkillLineSpellMap.end())
	{
		return &it->second;
	}
	return NULL;
}