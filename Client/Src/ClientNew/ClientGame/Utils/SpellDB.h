#ifndef SPELLDB_H
#define SPELLDB_H

#include "DBFile.h"


struct stSpellInfo
{
	unsigned int uiId;
	unsigned int uiAttribute;
	unsigned int uiTargets;
	unsigned int uiTargetCreatureType;
	unsigned int uiRecoverTime;
	unsigned int uiCastIndex;
	unsigned int uiCategory;
	unsigned int uiCategoryRecoveryTime;
	unsigned int uiMaxLevel;
	unsigned int uiBaseLevel;
	unsigned int uiSpellLevel;
	unsigned int uiDurationIndex;
	unsigned int uiRangeIndex;
	unsigned int uiVisualKit;
	unsigned int uiMaxTargets;
	unsigned int uiManaCost;
	unsigned int uiStartRecoveryTime;
	unsigned int uiManaCostPercentage;
	unsigned int uiAuraInfo;
	unsigned int uiTotemID[2];
	unsigned int MaxStack;

	float	flyspeed;

	unsigned int uiEffect[3];
	unsigned int uiEffectDieSides[3];
	unsigned int uiEffectApplyAuraName[3];
	unsigned int uiEffectBasePoint[3];
	unsigned int uiEffectMiscValue[3];
	unsigned int uiImplicitTarget[3];
	unsigned int uiEffectChainTarget[3];

	unsigned int uiEffectRadius[3];

	std::string name;
	std::string strIcon;
	std::string description;

	unsigned int uiSpellGroupType;

	unsigned int uiLineNameEntry;
	unsigned int uiSubLineNameEntry;
};
struct stSpellRadius
{
	uint32 index ;
	float radius;
};
struct stSpellRangeInfo
{
	uint32	entry;
	double	minrange;
	double	maxrange;
};

struct stSpellDurationInfo
{
	uint32 entry;
	uint32 duration1;
	uint32 duration2;
	uint32 duration3;
};

struct stQuerySpellInfo
{
	uint32 SpellId;
	uint32 AllowableClass;
	uint32 AllowableLevel;
};

struct stSpellGambleInfo
{
	uint32 entry;
	uint32 ItemEntry;
	uint32 ItemProb;
	uint32 min_count;
	uint32 max_count;
};

struct stSpellTrainerInfo
{
	uint32 cast_spell;
	uint32 learn_spell;
	uint32 spellcost;
	uint32 require_spell;
	uint32 require_skill;
	uint32 require_Skillvalue;
	uint32 require_Level;
	uint32 deletespell;
	uint32 is_prof;
	std::string name;
};

struct stSkillLineEntry
{
	uint32 entry;
	uint32 type;
	std::string name;
};

struct stSkillLineSpell
{
	uint32 entry;
	uint32 skillLine;
	uint32 spell;
	uint32 next;
	uint32 minrank;
	uint32 grey;
	uint32 green;
};

class CSpellDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	stSpellInfo* GetSpellInfo(unsigned int uiSpellId);
	std::map<unsigned int, stSpellInfo>& GetSpellInfos();
	bool IsMountSpell(uint32 uiSpellId);
	bool IsShapeSpell(uint32 uiSpellId);
	bool IsSkillBook(uint32 uiSpellId);

	bool GetSpellCDTIncrease(uint32 uiSpellId, uint32& cdtspellid, uint32& cdtincrease);
private:
	std::map<unsigned int, stSpellInfo> m_SpellInfo;
};

class CSpellRangeDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	stSpellRangeInfo* GetSpellRangeInfo(unsigned int uiEntryIdx);

private:
	std::map<uint32, stSpellRangeInfo> m_SpellRangeInfo;
};

class CSpellDurationDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	stSpellDurationInfo* GetSpellDurationInfo(unsigned int uiEntryIdx);

private:
	std::map<uint32, stSpellDurationInfo> m_SpellDurationInfo;
};
class CQuerySpellDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	bool GetQuerySpellList(std::vector<stQuerySpellInfo>& vList);

	std::vector<stQuerySpellInfo> m_QuerySpellInfo;
};
class CSpellRadiusDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	bool GetRadius(ui32 index, float& fRadius);

	std::vector<stSpellRadius> m_SpellEffectRadius;
};
class CSpellGambleDB: public CDBFile
{
public:
	virtual bool Load(const char* pcName);

	uint32 GetItemEntry(uint32 SpellId);
	bool GetItemCount(uint32 SpellId, uint32* minCount, uint32* maxCount);
	
	std::map<uint32 , stSpellGambleInfo> m_SpellGambleMap;
};
class CSpellTrainerDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	bool GetSpellTrainerList(uint32 entry, std::vector<stSpellTrainerInfo>& vList);

	std::map<uint32, std::vector<stSpellTrainerInfo>> m_SpellTrainerList;
};
class CSkillLineNameDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);

	bool GetSkillLineName(uint32 entry, std::string& Name);
	uint32 GetSkillLineType(uint32 entry);

	std::map<uint32, stSkillLineEntry> m_SkillLineMap;
};
class CSkillLineSpellDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	stSkillLineSpell* GetSkillLineSpell(uint32 spellId);

	std::map<uint32, stSkillLineSpell> m_SkillLineSpellMap;
};

extern CSpellDB* g_pkSpellInfo;
extern CSpellRangeDB* g_pkSpellRangeInfo;
extern CSpellDurationDB* g_pkSpellDurationInfo;
extern CSpellRadiusDB* g_pkSpellRadiusInfo;
extern CQuerySpellDB* g_pkQuerySpellInfo;
extern CSpellGambleDB* g_pkSpellGambleInfo;
extern CSpellTrainerDB* g_pkSpellTrainerInfo;
extern CSkillLineNameDB* g_SkillLineNameInfo;
extern CSkillLineSpellDB* g_SkillLineSpellInfo;

#endif

