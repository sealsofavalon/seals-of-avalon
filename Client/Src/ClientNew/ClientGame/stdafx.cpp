// stdafx.cpp : 只包括标准包含文件的源文件
// ClientGame.pch 将成为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"

// TODO: 在 STDAFX.H 中
//引用任何所需的附加头文件，而不是在此文件中引用

#if NIMETRICS

const char SYClientMetrics::ms_acNames
[SYClientMetrics::NUM_METRICS][NIMETRICS_NAMELENGTH] =
{
	"Frame Time",
	"Net update",
	"TerrDraw",
	"TerrCull",
	"TerrUpdate"
	"SceneDraw",
	"SceneUpdate",
	"SceneCull",
	"ShadowDrawTime",
	"LoadMap",
};

float SYClientMetrics::ms_afPerFrameTimes[NUM_METRICS];

#endif

#pragma comment(lib, "NiSystem.lib")
#pragma comment(lib, "NiMain.lib")
#pragma comment(lib, "NiInput.lib")
#pragma comment(lib, "NiApplication.lib")
#pragma comment(lib, "NiCollision.lib")
#pragma comment(lib, "NiAnimation.lib")
#pragma comment(lib, "NiDX9Renderer.lib")
#pragma comment(lib, "NiParticle.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "d3d9.lib")

#pragma comment(lib, "NiVisualTracker.lib")


#ifdef NDEBUG
namespace boost
{

	void throw_exception( const std::exception& e )
	{

	}
}
#endif