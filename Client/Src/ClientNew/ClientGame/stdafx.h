// stdafx.h : 标准系统包含文件的包含文件，
// 或是常用但不常更改的项目特定的包含文件
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// 从 Windows 头中排除极少使用的资料
// Windows 头文件:
#include <windows.h>
#pragma warning(push)
#pragma warning(disable : 4819)
#include <NiApplication.h>
#include <NiDX9Renderer.h>
#include <NiParticle.h>
#include <NiCollision.h>
#include <NiAnimation.h>
#include <NiMemStream.h>
#include <NiD3DUtility.h>

#include <NiApplicationMetrics.h>
#include <NiDx9RendererMetrics.h>
#include <NiD3DShaderProgramFactory.h>
#include <NiDynamicGeometryGroup.h>
#include <NiVBChip.h>
#include <NiDX9RenderedTextureData.h>
#include <NiDX9SystemDesc.h>

#pragma warning(pop)

#include "USystem.h"

#include "../../../../SDBase/Public/ItemDef.h"
#include "../../../../SDBase/Public/PlayerDef.h"
#include "../../../../SDBase/Public/SpellDef.h"
#include "../../../../SDBase/Public/TypeDef.h"
#include "../../../../SDBase/Public/UpdateFields.h"
#include "../../../../SDBase/Public/MapDef.h"
#include "../../../../SDBase/Public/TypeDef.h"
#include "../../../../SDBase/Public/Unitdef.h"
#include "../../../../SDBase/Public/ByteBuffer.h"
#include "../../../../SDBase/Public/UpdateMask.h"
#include "../../../../SDBase/Public/QuestDef.h"


#include "../../../../SDBase/Protocol/S2C.h"
#include "../../../../SDBase/Protocol/S2C_Quest.h"
#include "../../../../SDBase/Protocol/S2C_Chat.h"
#include "../../../../SDBase/Protocol/S2C_Trade.h"
#include "../../../../SDBase/Protocol/S2C_Item.h"
#include "../../../../SDBase/Protocol/S2C_Move.h"
#include "../../../../SDBase/Protocol/S2C_Spell.h"
#include "../../../../SDBase/Protocol/S2C_AI.h"
#include "../../../../SDBase/Protocol/S2C_Player.h"
#include "../../../../SDBase/Protocol/S2C_Group.h"
#include "../../../../../SDBase/Protocol/S2C_Guild.h"
#include "../../../../../SDBase/Protocol/S2C_Channel.h"
#include "../../../../../SDBase/Protocol/S2C_Pet.h"

#include "../../../../../SDBase/Protocol/C2S_Guild.h"
#include "../../../../../SDBase/Protocol/C2S_Pet.h"
#include "../../../../SDBase/Protocol/C2S_Move.h"
#include "../../../../SDBase/Protocol/C2S_Item.h"
#include "../../../../SDBase/Protocol/C2S_Chat.h"
#include "../../../../SDBase/Protocol/C2S_Move.h"
#include "../../../../SDBase/Protocol/C2S_Trade.h"
#include "../../../../SDBase/Protocol/C2S_Spell.h"
#include "../../../../SDBase/Protocol/C2S_Quest.h"
#include "../../../../SDBase/Protocol/C2S_Group.h"
#include "../../../../../SDBase/Protocol/C2S_Channel.h"

#include "../../../../SDBase/Protocol/C2LS.h"
#include "../../../../SDBase/Protocol/LS2C.h"
#include "Langauge/LangaugeNotice.h"

#include <hash_map>
#include <deque>
#include <queue>
#include <vector>
#include <string>
// 方便访问一些常用对象而不需要通过单件模式.

struct SY_State
{
	class CClientApp*		ClientApp;
	class CUISystem*		UI;
	NiRenderer*		Render;
	NiInputSystem* Input;

	class CSceneManager* SceneMgr;
	class CLightManager* LightMgr;

	class CObjectManager* ObjectMgr;
	class CNetworkManager* NetworkMgr;
	class CPacketBuilder* PacketBuilder;
	class CPacketParser* PacketParser;
	class CClientState* ClientState;
	class CPlayerInputMgr* LocalPlayerInput;
	class CEffectManager* EffectMgr;
	class CAudioInterface* IAudio;
	class CSkillManager* SkillMgr;
	class CItemManager* ItemMgr;
	class CDpsCount* DamageMgr;
	class LangaugeNotice* LangaugeMgr;
	DWORD CurTime;
	DWORD CurDelay;
	DWORD LastPing;

	float Camera_Move_Speed;
	float Camera_Follow_Speed;
};

extern SY_State __SY__;
inline SY_State* SYState()
{
	return &__SY__;
}

inline IDirect3DDevice9* SYD3DDevice()
{
	NiDX9Renderer* dr = (NiDX9Renderer*)SYState()->Render;
	NIASSERT(dr);
	return dr->GetD3DDevice();
}

extern IDirect3DTexture9* SYD3DTexture(NiTexture* NiTex);


typedef unsigned __int64  SYObjID;
#define CUR_TIME   ((DWORD)(__SY__.CurTime))

#define SY_LOG NILOG

#define NetworkMgr (SYState()->NetworkMgr)
#define ObjectMgr (SYState()->ObjectMgr)
#define ClientState (SYState()->ClientState)
#define PacketBuilder (SYState()->PacketBuilder)
#define PacketParser (SYState()->PacketParser)
#define ItemMgr (SYState()->ItemMgr)
#define SceneMgr (SYState()->SceneMgr)
#define VFileSys (SYState()->VFileSys)

#define EffectMgr (SYState()->EffectMgr)
#define DamageMgr (SYState()->DamageMgr)

#define LangaugeMgr (SYState()->LangaugeMgr)

class FixedStringCompare
{
public:
	enum
	{	// parameters for hash table
		bucket_size = 4,	// 0 < bucket_size
		min_buckets = 8		// min_buckets = 2 ^^ N, 0 < N
	};	

	FixedStringCompare()
	{	
	}
	size_t operator()(const NiFixedString& key) const
	{	
		// 使用指针哈希
		return stdext::hash_value((size_t)*key);
	}
	// less
	bool operator()(const NiFixedString& Key1, const NiFixedString& Key2) const
	{	
		NIASSERT(Key1 && Key2);
		return  NiStricmp(Key1, Key2) < 0 ;
	}
};

// map
struct NiFixedStringLess
{	
	bool operator()(const NiFixedString& Left, const NiFixedString& Right) const
	{	
		return (size_t)((const char*)Left) < (size_t)((const char*)Right);
	}
};

template<class _Val>
class FixedStringHashMap : public stdext::hash_map<NiFixedString,_Val,FixedStringCompare>
{
public:
	
};

// Tree



#if NIMETRICS

class SYClientMetrics
{
public:
	enum EMetrics
	{
		UPDATE_TIME = 0,
		NETWORK_UPDATETIME,
		TERRAIN_RENDERTIME,
		TERRAIN_CULLTIME,
		TERRAIN_UPDATETIME,
		SCENE_RENDERTIME,
		SCENE_UPDATETIME,
		SCENE_CULLTIME,
		SHADOW_RENDERTIME,
		MAP_LOAD_TIME,
		NUM_METRICS
	};

	const static char ms_acNames[NUM_METRICS][NIMETRICS_NAMELENGTH];

	static void RecordMetrics();
	static void IncrementMetric(EMetrics eMetric, int iInc = 1)
	{

	}
	static void RecordTime(EMetrics eMetric, float fTime)
	{
		ms_afPerFrameTimes[eMetric] += fTime;
	}
protected:
	static float ms_afPerFrameTimes[NUM_METRICS];
};

class  SYScopeTimer : public NiPerformanceCounterTimer
{
public:
	SYScopeTimer(SYClientMetrics::EMetrics eMetric)
	{
		m_eMetric = eMetric;
		Start();
	}
	~SYScopeTimer()
	{
		Stop();
		SYClientMetrics::RecordTime(m_eMetric, GetElapsed());
	}
protected:
	SYClientMetrics::EMetrics m_eMetric;
};

#endif 


#if NIMETRICS
#define CLIENT_METRIC_ADDVALUE(eMetric, fValue) \
	NIMETRICS_ADDVALUE(SYClientMetrics::ms_acNames[ \
	SYClientMetrics::eMetric], fValue);
#define CLIENT_METRIC_TIMER(classname, kTimer, eMetric) \
	classname kTimer( \
	SYClientMetrics::ms_acNames[SYClientMetrics::eMetric]);
#define CLIENT_METRIC_STARTTIMER(kTimer) \
	NIMETRICS_STARTTIMER(kTimer)
#define CLIENT_METRIC_ENDTIMER(kTimer) \
	NIMETRICS_ENDTIMER(kTimer)
#define CLIENT_METRIC_EVENT(eMetric, fValue) \
	NIMETRICS_ADDEVENT( \
	SYClientMetrics::ms_acNames[SYClientMetrics::eMetric], \
	fValue);

#define CLIENT_METRIC_RECORDMETRICS() \
	SYClientMetrics::RecordMetrics();
#define CLIENT_METRIC_SCOPETIMER(eMetric) \
	SYScopeTimer kMetricsTimer(SYClientMetrics::eMetric);



inline void SYClientMetrics::RecordMetrics()
{
	CLIENT_METRIC_ADDVALUE(TERRAIN_CULLTIME, ms_afPerFrameTimes[TERRAIN_CULLTIME]);
	CLIENT_METRIC_ADDVALUE(TERRAIN_RENDERTIME, ms_afPerFrameTimes[TERRAIN_RENDERTIME]);
	memset(ms_afPerFrameTimes,0, sizeof(ms_afPerFrameTimes));
}
#else //#if NIMETRICS
#define CLIENT_METRIC_ADDVALUE(eMetric, fValue)
#define CLIENT_METRIC_TIMER(classname, kTimer, eMetric)
#define CLIENT_METRIC_STARTTIMER(kTimer)
#define CLIENT_METRIC_ENDTIMER(kTimer)
#define CLIENT_METRIC_EVENT(eMetric, fValue)
#define CLIENT_METRIC_SCOPETIMER(eMetric)
#define CLIENT_METRIC_RECORDMETRICS()
#endif //#if NIMETRICS

#if defined(CLIENT_DEBUG)
    #define DASSERT(value) {if ((value) == 0) __debugbreak();}
#elif defined(_DEBUG)
    #define DASSERT(value) {assert(value);}
#else
    #define DASSERT(value)
#endif
