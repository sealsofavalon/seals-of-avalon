#pragma once

class CUnzipper;

class CPakExtracter
{
public:
	CPakExtracter();
	~CPakExtracter(void);

	char*				GetData()		{ return m_pcData; }
	unsigned long		GetDataSize()	{ return m_ulSize; }

	char**				GetDataAddress()	{ return &m_pcData; }
	unsigned long*		GetDataSizePtr()	{ return &m_ulSize; }

protected:
	char*				m_pcData;
	unsigned long		m_ulSize;
};

class CPakExtracterSelfDelete : public CPakExtracter
{
public:
	CPakExtracterSelfDelete()	{}
	~CPakExtracterSelfDelete(void);
};


typedef struct _PAKED_FILEINFO{
	int				nIndex;
	CUnzipper*		pOwner;
	unsigned long	ulOffset;
} PAKED_FILEINFO;

typedef std::vector<CUnzipper*>	STLVTR_ZIP;
typedef std::map<std::string, PAKED_FILEINFO*>	STLMAP_PAKED_FILE;

class CPakManager
{
public:
	CPakManager(void);
	~CPakManager(void);

	static CPakManager* GetInstance();

	void				Init();
	void				Destroy();

	bool				Create();

	CUnzipper*			AddZip(const char* strFilename);

	bool				GetFile(const std::string& strFilename, CPakExtracter* pExtracter);
	bool				GetFile(const char* pcFilename, CPakExtracter* pExtracter );

	bool				GetFile(const std::string& strFilename, void** ppData, unsigned long* pulSize);
	bool				GetFile(const char* pcFilename, void** ppData, unsigned long* pulSize);

	bool				IsExistFile(const char* pcFilename);
	bool				IsExistFile(const std::string& strFilename);

	void				ParseFileName(CUnzipper* pUnzipper, std::vector<std::string>& v);
	void				IndexingPakedFile(CUnzipper* pUnzipper);

protected:
	void				DeleteAllZip();

	STLVTR_ZIP			m_vectorZip;
	STLMAP_PAKED_FILE	m_mapPakedFile;
	STLMAP_PAKED_FILE	m_mapTempFile;

	static CPakManager* ms_pkInstance;
	//	STLVCTR_ZIP	m_vectorZip;
};
