#ifndef __LUIBASE_H__
#define __LUIBASE_H__

#ifndef UISTATIC
#	ifdef UI_EXPORTS
#		define UIAPI __declspec(dllexport)
#	else
#		define UIAPI __declspec(dllimport)
#	endif 
#else
#	define UIAPI 
#endif 

#include "UInput.h"

#include <vector>


class UIAPI UPoint
{
public:
	int x;
	int y;

	inline UPoint(void) {	}
	inline UPoint(int _x, int _y) : x(_x), y(_y){}
	inline UPoint(const UPoint& pt):x(pt.x),y(pt.y){}
	inline void Set(int _x, int _y)
	{
		x = _x;
		y = _y;
	}
	inline UPoint& operator*=(const UPoint& pt)
	{
		x *= pt.x;
		y *= pt.y;
		return *this;
	}

	inline UPoint& operator/=(const UPoint& pt)
	{
		x /= pt.x;
		y /= pt.y;
		return *this;
	}

	inline UPoint& operator+=(const UPoint& pt)
	{
		x += pt.x;
		y += pt.y;

		return *this;
	}

	inline UPoint& operator-=(const UPoint& pt)
	{
		x -= pt.x;
		y -= pt.y;

		return *this;
	}

	inline UPoint operator+(const UPoint& pt) const
	{
		return UPoint(x + pt.x, y + pt.y);
	}

	inline UPoint operator-(const UPoint& pt) const
	{
		return UPoint(x - pt.x, y - pt.y);
	}

	inline UPoint operator*(const UPoint& pt) const
	{
		return UPoint(x * pt.x, y * pt.y);
	}
	inline UPoint operator*(int scale) const
	{
		return UPoint(x * scale, y * scale);
	}
	inline BOOL operator==(const UPoint& pt) const
	{
		return ((x == pt.x) && (y == pt.y));
	}

	inline BOOL	operator!=(const UPoint& pt) const
	{
		return !(operator==(pt));
	}

	inline UPoint& Offset(int _x , int _y)
	{
		x += _x;
		y += _y;
		return *this;
	}
	inline float Length()
	{
		return sqrt(float(x*x + y*y));
	}
};

typedef UPoint UPointF;

#pragma warning(push)
#pragma warning(disable : 4244)
/*
*	矩形. 整形精度,
*/
class UIAPI URect
{
public:
	union
	{
		struct {
			int	left;
			int top;
			int right;
			int bottom;
		};
		struct {
			UPoint pt0;
			UPoint pt1;
		};
	};


	inline URect(void):left(0),top(0),right(0),bottom(0) 
	{}
	inline URect(int _left, int _top, int _right, int _bottom)
		:left(_left),top(_top),right(_right),bottom(_bottom)
	{}
	inline URect(const UPoint& _pt0, const UPoint& size)
		:left(_pt0.x), top(_pt0.y),right(pt0.x + size.x),bottom(pt0.y + size.y)
	{
	}
	inline const UPoint& GetPosition(void) const		{	return pt0; }
	inline int	GetWidth(void) const		{	return right - left; }
	inline int	GetHeight(void) const		{	return bottom - top; }
	inline UPoint GetSize(void) const				{	return UPoint(GetWidth(), GetHeight()); }
	inline void	SetPosition(const UPoint& pt)
	{
		int w = right - left;
		int h = bottom - top;
		left = pt.x;
		top = pt.y;

		right = left + w;
		bottom = top + h;
	}
	inline void	SetPosition(int x, int y)
	{
		int w = right - left;
		int h = bottom - top;
		left = x;
		top = y;

		right = left + w;
		bottom = top + h;

	}
	inline void Set(int x, int y, int width, int height)
	{
		left = x; top = y;
		right = x + width;
		bottom = y + height;
	}
	inline void	SetWidth(float width)		{	right = left + width;}
	inline void	SetHeight(float height)		{	bottom = top + height;}
	inline void	SetSize(const UPoint& sz)	{
		right = left + sz.x;
		bottom = top + sz.y;
	}
	inline void SetSize(int cx, int cy) {
		right = left + cx;
		bottom = top + cy;
	}
	
	inline BOOL Intersect(const URect& rect) const
	{
		if ((right > rect.left) &&
			(left < rect.right) &&
			(bottom > rect.top) &&
			(top < rect.bottom))
		{
			return TRUE;
		}
		return FALSE;
	}
	inline BOOL SetIntersect(const URect& rect)
	{
		if (Intersect(rect))
		{
			// fill in temp with the intersection
			left = (left > rect.left) ? left : rect.left;
			right = (right < rect.right) ? right : rect.right;
			top = (top > rect.top) ? top : rect.top;
			bottom = (bottom < rect.bottom) ? bottom : rect.bottom;

			return TRUE;
		}
		return FALSE;
	}
	inline URect GetIntersection(const URect& rect) const
	{
		if (Intersect(rect))
		{
			URect temp;

			// fill in temp with the intersection
			temp.left = (left > rect.left) ? left : rect.left;
			temp.right = (right < rect.right) ? right : rect.right;
			temp.top = (top > rect.top) ? top : rect.top;
			temp.bottom = (bottom < rect.bottom) ? bottom : rect.bottom;

			return temp;
		}
		else
		{
			return URect(0, 0, 0, 0);
		}
	}

	inline void Union(const URect& rect)
	{
		left = min(left,rect.left);
		top = min(top,rect.top);
		right = max(right,rect.right);
		bottom = max(bottom,rect.bottom);
	}
	inline URect& TransformRect(UPoint oldRule, UPoint newRule)
	{
		UPoint NewPos = GetPosition();
		UPoint NewSize = GetSize();


		INT deltaX = newRule.x - oldRule.x;
		INT deltaY = newRule.y - oldRule.y;



		if (deltaX == 0 && deltaY == 0 )
		{
			left = NewPos.x;
			right = NewPos.x + NewSize.x;
			top = NewPos.y;
			bottom = NewPos.y + NewSize.y;
			return *this;
		}

		UPoint NewParentSize = newRule;
		UPoint OldParentSize = oldRule;

		if (OldParentSize.x != 0)
		{
			INT newLeft = (NewPos.x * NewParentSize.x) / OldParentSize.x;
			INT newRight = ((NewPos.x + NewSize.x) * NewParentSize.x) / OldParentSize.x;

			NewPos.x = newLeft;
			NewSize.x = newRight - newLeft;
		}

		if (OldParentSize.y != 0)
		{
			INT newTop = (NewPos.y * NewParentSize.y) / OldParentSize.y;
			INT newBottom = ((NewPos.y + NewSize.y) * NewParentSize.y) / OldParentSize.y;

			NewPos.y = newTop;
			NewSize.y = newBottom - newTop;
		}

		left = NewPos.x;
		right = NewPos.x + NewSize.x;
		top = NewPos.y;
		bottom = NewPos.y + NewSize.y;
		return *this;
	}
	inline URect& Offset(const UPoint& pt)
	{
		left += pt.x;
		right += pt.x;
		top	+= pt.y;
		bottom += pt.y;
		return *this;
	}
	inline URect& Offset(int x , int y)
	{
		left += x;
		right += x;
		top	+= y;
		bottom += y;
		return *this;
	}
	inline BOOL	PointInRect(const UPoint& pt) const
	{
		if ((left > pt.x) ||
			(right < pt.x) ||
			(top > pt.y) ||
			(bottom < pt.y))
		{
			return FALSE;
		}
		return TRUE;
	}
	inline URect& InflateRect(const URect& rect)
	{
		left += rect.left;
		top += rect.top;
		right -= rect.right;
		bottom -= rect.bottom;
		return *this;
	}
		inline URect& InflateRect(int x, int y)
	{
		left += x;
		top += y;
		right -= x;
		bottom -= y;
		return *this;
	}
	inline BOOL	operator==(const URect& rhs) const
	{
		return ((left == rhs.left) && (right == rhs.right) && (top == rhs.top) && (bottom == rhs.bottom));
	}

	inline BOOL	operator!=(const URect& rhs) const		{return !operator==(rhs);}

	inline URect&	operator=(const URect& rhs)
	{
		left = rhs.left;
		top = rhs.top;
		right = rhs.right;
		bottom = rhs.bottom;
		return *this;
	}

	inline URect operator*(float scalar) const      { return URect(left * scalar, top * scalar, right * scalar, bottom * scalar); }
	inline const URect& operator*=(float scalar)    { left *= scalar; top *= scalar; right *= scalar; bottom *= scalar; return *this; }
};

class UIAPI UPolygon
{
public:
	UPolygon()
	{
		m_PointNum = 0;
	}
	~UPolygon()
	{
		m_PointNum = 0;
	}
	enum
	{
		Polygon_Point_Max = 36,
	};
public:
	//这个只支持凸多边形
	BOOL IsPointInPolygon(const UPoint& point)
	{
		if(m_PointNum < 3)
			return FALSE;

		for (int i = 0 ; i < m_PointNum - 1 ; ++i)
		{
			UPoint v(Point[i + 1].x -Point[i].x, Point[i + 1].y -Point[i].y);
			UPoint v1(point.x -Point[i].x, point.y -Point[i].y);
			int temp = v1.x * v.y - v.x * v1.y;
			if (temp < 0)
				return FALSE;
		}
		return TRUE;
	}
	BOOL InsertPoint(const UPoint& point)
	{
		if (m_PointNum >= Polygon_Point_Max)
			return FALSE;
		Point[m_PointNum++] = point;
		return TRUE;
	}

	BOOL InsertPoint(int x, int y)
	{
		if (m_PointNum >= Polygon_Point_Max)
			return FALSE;
		Point[m_PointNum].x = x;
		Point[m_PointNum++].y = y;
		return TRUE;
	}
public:
	UPoint Point[Polygon_Point_Max];
	int m_PointNum;
};

#pragma warning(pop)

class UIAPI UColor
{
public:
	union 
	{ 
		struct{ 
			BYTE b;
			BYTE g;
			BYTE r;
			BYTE a; 
		};	
		DWORD dwColorRef; // argb
	};
	UColor() 
	{
	}
	UColor( BYTE InR, BYTE InG, BYTE InB, BYTE InA = 255 )
		:	r(InR), g(InG), b(InB), a(InA) 
	{
	}

	UColor( DWORD InColor ):dwColorRef(InColor)
	{ 
	}
	void Set(BYTE InR, BYTE InG, BYTE InB, BYTE InA)
	{
		r = InR;
		g = InG;
		b = InB;
		a = InA;
	}
	DWORD& ColorValue(void) 
	{
		return *((DWORD*)this);
	}
	const DWORD& ColorValue(void) const 
	{
		return *((DWORD*)this);
	}
	DWORD RGB24toGray()
	{
		int r,g,b,t;
		r=(dwColorRef>>16);
		g=(dwColorRef & 0x00ff00) >>8;
		b=dwColorRef & 0x0000ff;
		t=(r*3+g*6+b)/10;
		return (t<<16)|(t<<8)|t;
	}
	BOOL operator==( const UColor &C ) const
	{
		return dwColorRef == C.dwColorRef;
	}
	BOOL operator!=( const UColor& C ) const
	{
		return dwColorRef != C.dwColorRef;
	}
	inline void operator+=(UColor C)
	{
		r = (BYTE) min((INT) r + (INT) C.r,255);
		g = (BYTE) min((INT) g + (INT) C.g,255);
		b = (BYTE) min((INT) b + (INT) C.b,255);
		a = (BYTE) min((INT) a + (INT) C.a,255);
	}
	inline UColor operator+(UColor C)
	{
		return UColor((BYTE) min((INT) r + (INT) C.r,255),
			(BYTE) min((INT) g + (INT) C.g,255),
			(BYTE) min((INT) b + (INT) C.b,255),
			(BYTE) min((INT) a + (INT) C.a,255));
	}
	inline void operator-=(UColor C)
	{
		r = (BYTE) max((INT) r - (INT) C.r,0);
		g = (BYTE) max((INT) g - (INT) C.g,0);
		b = (BYTE) max((INT) b - (INT) C.b,0);
		a = (BYTE) max((INT) a - (INT) C.a,0);
	}
	inline UColor operator-(UColor C)
	{
		return UColor((BYTE) max((INT) r - (INT) C.r,0),
			(BYTE) max((INT) g - (INT) C.g,0),
			(BYTE) max((INT) b - (INT) C.b,0),
			(BYTE) max((INT) a - (INT) C.a,0));
	}

	inline operator DWORD() const 
	{ 
		return dwColorRef; 
	}
};


#define LCOLOR_ARGB(a,r,g,b) \
	((UColor)((((a)&0xff)<<24)|(((r)&0xff)<<16)|(((g)&0xff)<<8)|((b)&0xff)))
#define LCOLOR_RGBA(r,g,b,a)	LCOLOR_ARGB(a,r,g,b)
#define LCOLOR_XRGB(r,g,b)		LCOLOR_ARGB(0xff,r,g,b)
#define LCOLOR_RGB(r,g,b)		LCOLOR_ARGB(0xff,r,g,b)	

#define LCOLOR_COLORVALUE(r,g,b,a) \
	LCOLOR_RGBA((DWORD)((r)*255.f),(DWORD)((g)*255.f),(DWORD)((b)*255.f),(DWORD)((a)*255.f))

#define LCOLOR_WHITE	0xFFFFFFFF
#define LCOLOR_BLACK	0xFF000000
#define LCOLOR_RED		0xFFFF0000
#define LCOLOR_GREEN	0XFF00FF00
#define LCOLOR_BLUE		0XFF0000FF
#define LCOLOR_SHADOW	0xFF202020

// 封装一些针对UI操作的接口.
class UIAPI UString
{
	UINT m_nSize;
	UINT m_nMaxSize;
	char* m_pBuffer;
public:
	enum
	{
		MAX_SIZE = 0xFFFF,
	};
	UString(int InitSize = 0):m_nSize(0), m_nMaxSize(0),m_pBuffer(NULL)
	{
		if (InitSize > 0)
		{
			Resize(InitSize);
		}
	};
	UString(const UString &copy):m_nSize(0), m_nMaxSize(0),m_pBuffer(NULL)
	{
		Set(&copy);
	};
	UString(const UString* copy);
	UString(const char* cstr);
	UString(const WCHAR* wstr);
	~UString();
	inline BOOL Empty() const
	{
		if (m_nSize == 0 || m_pBuffer[0] == '\0')
		{
			return TRUE;
		}
		return FALSE;
	}
	void Clear();
	BOOL Resize(INT NewSize);
	void Append(const UString& Buf);
	void Insert(UINT Pos, const UString &Buf);
	UINT GetLength() const { return m_nSize; }
	UINT GetCapacity () const { return m_nMaxSize; }

	// pDest 的存储必须事先分配好. size = len - start +1
	BOOL SubStr(char* pDest, UINT start, const UINT len) const;
	void Cut(UINT start, const UINT len, char* pDest = NULL);
	WCHAR GetChar(UINT offset) const;

	void Set(const UString* in);
	void Set(const char* in);
	void Set(const WCHAR* in);

	void Get(char *buff, UINT buffSize) const;
	void Get(WCHAR *buff, UINT buffSize) const;
	
	const char* GetBuffer() const { return m_pBuffer; }
	char* GetBuffer() { return m_pBuffer; }
	const char& operator[]( int n ) const { return m_pBuffer[n]; }
	char& operator[]( int n )	{ return m_pBuffer[n];}
};


// 封装一些针对UI操作的接口. unicode 实现
class UIAPI UStringW
{
	UINT m_nSize;
	UINT m_nMaxSize;
	WCHAR* m_pBuffer;
public:
	enum
	{
		MAX_SIZE = 0xFFFF,
	};
	UStringW(int InitSize = 0):m_nSize(0), m_nMaxSize(0),m_pBuffer(NULL)
	{
		if (InitSize > 0)
		{
			Resize(InitSize);
		}
	};
	UStringW(const UStringW &copy):m_nSize(0), m_nMaxSize(0),m_pBuffer(NULL)
	{
		Set(&copy);
	};
	UStringW(const UStringW* copy);
	UStringW(const char* cstr);
	UStringW(const WCHAR* wstr);
	~UStringW();
	inline BOOL Empty() const
	{
		if (m_nSize == 0 || m_pBuffer[0] == 0)
		{
			return TRUE;
		}
		return FALSE;
	}
	// free memory
	void Clear();
	// zero but keep the memory.
	void SetEmpty();
	BOOL Resize(INT NewSize);
	void Append(const UStringW& Buf);
	void Append(WCHAR wChar);
	void Append(const WCHAR* wStr, int nLen = -1);
	void Insert(UINT Pos, const UStringW &Buf);
	void Insert(UINT Pos, WCHAR wChar);
	void Insert(UINT Pos, const WCHAR* wStr, int Len = -1);
	inline UINT GetLength() const { return m_nSize; }
	UINT GetAnsiLength() const;
	inline UINT GetCapacity () const { return m_nMaxSize; }

	// pDest 的存储必须事先分配好. size = len - start +1
	BOOL SubStr(WCHAR* pDest, UINT start, UINT len) const;
	void Cut(UINT start, const UINT len, WCHAR* pDest = NULL);
	WCHAR GetChar(UINT offset) const;

	void Set(const UStringW* in);
	void Set(const char* in);
	void Set(const WCHAR* in);

	int GetAnsi(char* pDest, int nDestLen, int start) const;

	inline operator WCHAR*() const { return m_pBuffer;}
	inline const WCHAR* GetBuffer() const { return m_pBuffer; }
	inline WCHAR* GetBuffer() { return m_pBuffer; }
	inline const WCHAR& operator[]( int n ) const { return m_pBuffer[n]; }
	inline WCHAR& operator[]( int n )	{ return m_pBuffer[n];}

	// 慎用.
	inline void SetLength(int nCurLen) { m_nSize = nCurLen; }
};


// UXML Stream.
class UStream
{

};



inline int LSprintf(char* pcDest, size_t stDestSize, const char* pcFormat, ...)
{
	if (stDestSize == 0)
		return TRUE;
	va_list kArgs;
	va_start(kArgs, pcFormat);
#if _MSC_VER >= 1400
	int iRet = vsprintf_s(pcDest, stDestSize, pcFormat, kArgs);
#else 
	int iRet = _vsnprintf(pcDest, stDestSize, pcFormat, kArgs);

	if (iRet >= 0 && ((unsigned int)iRet == stDestSize - 1) &&
		pcDest[stDestSize - 1] != '\0')
	{
		iRet = -1;
	}
#endif 
	va_end(kArgs);
	pcDest[stDestSize - 1] = '\0';
	return iRet;
}

template<typename T> 
inline T LClamp(const T& Value, const T& Min, const T& Max) 
{ return Value < Min ? Min : ( Value > Max ? Max : Value) ; }


class IUTexture;
class GBitmap;
class IUFont;
class ILUiSound;

/// Represents a single GUI event.
///
/// This is passed around to all the relevant cOntrols so they know what's going On.
struct GuiEvent
{
	WORD			ascii;             ///< ascii character code 'a', 'A', 'b', '*', etc (if device==keyboard) - possibly a uchar or something
	BYTE			modifier;          ///< SI_LSHIFT, etc
	BYTE			keyCode;           ///< for unprintables, 'tab', 'return', ...
	UPoint		mousePoint;        ///< for mouse events
	BYTE			mouseClickCount;   ///< to determine double clicks, etc...
};

class UClass;
class UObject;
class UCmdTarget;
class UControl;
class UScrollBar;

enum EUPropType
{
	UPT_INT =0,
	UPT_BOOL,
	UPT_FLOAT,
	UPT_STRING,		// 必须为UString 类型. 不支持char*
	UPT_STRINGW,	// UStringW.
	UPT_POINT,
	UPT_RECT,
	UPT_COLOR,
	UPT_CTRLSTYLE,	// control style.
	UPT_USER0 = 9,
	// ... 
	UPT_MAX = 16,
};


#define UFIELD_OFFSET(Class, Field) ((unsigned int)((void*)&(((Class*)0)->Field)))


// 动态属性
struct UIAPI  UProperty 
{
	char* FieldName;    
	char* GroupName; 
	char* FieldDocs;   
	UINT	type;         
	UINT	Offset;       
	INT		ElementCount;
};

struct UPropertyVisitor
{
	// set the target property from string .
	// return -1 意味着可变长度.
	//virtual int SizeOf() const = 0;
	virtual BOOL Set(void* Target, const char* Src, INT Size = -1) = 0;

	// get the property as string
	// Target == NULL Size != NULL 通过Size 返回所需要分配的字符串长度.
	virtual BOOL Get(char* Target, INT* Size, const void* Src) = 0;
};

typedef void (*LTHWNDSTATICINITFN)();
typedef UObject* (*LFACTORYCREATEWNDFN)();

/*
*	窗口类定义
*/
class UIAPI UClass
{
	friend class UObject;
	friend struct UProperty;
public:
	UClass(UClass* Parent, const char* name,LTHWNDSTATICINITFN pStaticInit, LFACTORYCREATEWNDFN pCreateFn);
	const UProperty* FindProperty(const char* name) const;
	void RegisterField(const char* FieldName, UINT Type, UINT ofs,const char* Comment = NULL);
	void StaticInit();
	UObject* CreateObject() const;
	inline const char* GetClassName() const  { return pszClassName;}
	inline UClass* GetNextClass()  { return pNext;}
	inline const UClass* GetParentClass() const { return pParentClass;}
	void UnRegister();
	BOOL IsBaseOf(const UClass* ChildClass) const;
private:
	UINT m_Flags;
	const char*			pszClassName;			// 类名
	LTHWNDSTATICINITFN	pfnStaticInit;			// 类初始化指针
	LFACTORYCREATEWNDFN pfnDynCreateObject;		// 创建对象函数指针
	UClass* pParentClass;						// 父类
	// 内部数据
	void*			pFields;		// 动态属性链表,初始化时将注册.
	UClass*		pNext;					// 链表
	UINT		uClsId;					// 类ID,自动赋值.	
};

#define UDEC_CLASS(Class)		\
	private:					\
	static UClass _DynClass;		\
	static void StaticInit();	\
	public:				\
	static UObject* CreateObject();\
	static UClass* GetStaticClass();\
	static UClass* GetParentStaticClass();\
	virtual const UClass* GetClass() const;\
	private:	\


#define UIMP_CLASS(Class,BaseClass)\
	UClass* Class::GetStaticClass() { return &Class::_DynClass;} \
	UClass* Class::GetParentStaticClass() { return BaseClass::GetStaticClass();}\
	UObject* Class::CreateObject()	{ return new Class(); }	\
	UClass Class::_DynClass(Class::GetParentStaticClass(),#Class,Class::StaticInit, Class::CreateObject);\
	const UClass* Class::GetClass() const{ return &Class::_DynClass;}\


#define UREG_PROPERTY(name, type, offset) RegisterField(GetStaticClass(),name,type,offset,NULL);


// 动态数据定义, RTT, 
class UIAPI UObject
{
public:
	UObject(void);
	virtual ~UObject(void);
	static BOOL RegisterPropertyType(UINT Typeid,  UPropertyVisitor* pVisitor);
	virtual void DeleteThis();
	static UObject* CastTo(UObject* pObject, const UClass* pClass);
	BOOL SetField(const char* fieldName, const char* Value);
protected:
	static void RegisterField(UClass* pClass, const char* FieldName, UINT Type, UINT ofs,const char* Comment = NULL);
private:
	static UObject* CreateObject();
	static UClass _DynClass;	
	static void StaticInit();
public:	
	static UClass* GetStaticClass();
	static UClass* GetParentStaticClass();

	virtual const UClass* GetClass() const;
};

#define UDynamicCast(ClassName, pUObject)  ((ClassName*) UObject::CastTo(pUObject, ClassName::GetStaticClass()))



class UIAPI URefTarget
{
public:
	inline URefTarget():m_RefCounter(0)
	{
	}
	virtual ~URefTarget()
	{
		//assert(m_RefCounter == 0);
	}
	inline void AddRef()	{ ++m_RefCounter; }
	virtual void Release()
	{
		if (--m_RefCounter == 0)
			delete this;
	}
	inline int NumRef()		{ return m_RefCounter; }
protected:
	//virtual void DeleteThis() { delete this; }
	UINT m_RefCounter;
};

template <class Type> 
class UIAPI USmartPtr 
{
private:
	Type* m_pObj;
public:
	inline USmartPtr(Type* p_ = (Type*) 0)
	{
		m_pObj = p_;
		if (m_pObj)
			m_pObj->AddRef();
	}

	inline USmartPtr(const USmartPtr &p_)
	{
		m_pObj = p_.m_pObj;
		if (m_pObj)
			m_pObj->AddRef();
	}
	inline ~USmartPtr()
	{
		if (m_pObj)
			m_pObj->Release();
	}
	inline operator Type*() const { return m_pObj; }
	//inline operator const Type*() const { return m_pObj; }
	inline Type& operator*() const { return *m_pObj; }
	inline Type* operator->(void) const { return m_pObj; }
	inline USmartPtr&  operator=(Type* newp)
	{
		if (newp)
			newp->AddRef();
		if (m_pObj)
			m_pObj->Release();
		m_pObj = newp;
		return *this;
	}
	inline USmartPtr&  operator=(const USmartPtr &newp)
	{
		if (newp.m_pObj)
			newp.m_pObj->AddRef();
		if (m_pObj)
			m_pObj->Release();
		m_pObj = newp.m_pObj;
		return *this;
	}

	inline bool  operator ==(Type* p2) const { return m_pObj == p2; };
	inline bool  operator !=(Type* p2) const {	return m_pObj != p2; };
	inline bool  operator ==(const USmartPtr &p2) const { return m_pObj == p2.m_pObj; };
	inline bool  operator !=(const USmartPtr &p2) const { return m_pObj != p2.m_pObj; };
};


class IUTexture : public URefTarget
{
public:
	virtual INT GetWidth() =0;
	virtual INT GetHeight() = 0;
};

#define UTAB_WIDTH 3
class IUFont : public URefTarget
{
public:
	// TODO Implement ANSI character operation.

	virtual INT GetStrWidth(const char* Str) = 0;
	virtual INT GetStrWidth(const WCHAR* wStr) = 0;
	virtual INT GetCharWidth(WCHAR wChar) =0;
	virtual INT GetStrNWidth(const WCHAR* Str, int Num) = 0;
	virtual INT GetHeight() = 0;
	virtual UINT GetBaseline() const =0;
	virtual UINT GetDescent() const=0;
	virtual INT GetCharXIncrement(WCHAR wChar) = 0;
	virtual BOOL IsValidChar(WCHAR wChar) = 0;
	virtual UINT GetBreakPos(const WCHAR *string, UINT strlen, UINT width, BOOL breakOnWhitespace) = 0;
	virtual UINT GetStrNWidthPrecise(const WCHAR*, UINT n) = 0;
};

class ILUiSound 
{
};

struct USKINITEM
{
	int		id;
	URect	SubSec;	
};

class UIAPI USkin : public URefTarget
{
public:
	virtual BOOL IsAniSkin() const = 0;
	virtual const URect* GetSkinItemRect(int index) const =0;
	virtual const USKINITEM* GetSkinItem(int index) const = 0;
	virtual int GetItemNum() const = 0;
	virtual const char* GetSkinName() const = 0;
	virtual IUTexture* GetTexture() const = 0;
};


typedef USmartPtr<IUFont> UFontPtr;
typedef USmartPtr<IUTexture> UTexturePtr;
typedef USmartPtr<USkin> USkinPtr;




class UIAPI UCursor : public UObject
{
private:
	typedef UObject Parent;
	UString m_strIconFile;

	UPoint mHotSpot;
	UPointF mRenderOffSet;
	UPoint mExtent;
	IUTexture* mTextureObject;

public:
	UPoint GetHotSpot() { return mHotSpot; }
	UPoint GetWindowSize() { return mExtent; }

	//   DECLARE_CONOBJECT(UCursor);
	UCursor(void);
	~UCursor(void);
	static void initPersistFields();

	BOOL OnAdd(void);
	void OnRemove();
	void render(const UPoint &pos);
};

// TODO 变为抽象类. 不允许外部构造, 可以避免不必要的麻烦. 将属性暴露出来, 当作一个结构集成, 可以允许类似
// WIN32 RegisterClass 那样的编码构造. 
// eg.
/*

struct UCONTROLSTYLE
{
const char* Name;
BOOL CanTab;
...
};

UCONTROLSTYLE stl;
...

USystem::RegisterStyle(&stl);

USystem::CreateControl(&stl, ...);
*/

enum EUTextAlignment
{
	UT_LEFT = 0,
	UT_RIGHT,
	UT_CENTER
};
enum EUResizeImageType
{
	RESIZE_WIDTH,
	RESIZE_HEIGHT,
	RESIZE_WANDH,
};
// Control stryle 用来修饰控件不经常修改的公共属性. 
class UIAPI UControlStyle : public UObject
{
	UDEC_CLASS(UControlStyle);
	INT  m_nRefCnt; 
public:
	UString m_Name;							// style 名称
	BOOL m_bTabStop;						// 是否能被tab获得焦点
	BOOL m_bKeyFocus;						// 是否可以获得键盘输入, 如果可以获得键盘输入,则必然能通过Tab获得焦点.
	BOOL m_bHandleDragDrop;					// 是否响应DragDrop 事件
	BOOL mModal;                                    ///< True if this is a Modeless dialog meaning it will pass input through instead of taking it all

	BOOL mOpaque;                                   ///< True if this object is not translucent
	UColor mFillColor;                              ///< Fill color, this is used to fill the bounds of the cOntrol if it is opaque
	UColor mFillColorHL;                            ///< This is used inSetead of mFillColor if the object is highlited
	UColor mFillColorNA;                            ///< This is used to instead of mFillColor if the object is not active or disabled

	INT mBorder;                                    ///< For most cOntrols, if mBorder is > 0 a border will be drawn, some cOntrols use this to draw different types of borders however @see guiDefaultCOntrolRender.cc
	INT mBorderThickness;                           ///< Border thickness
	UColor mBorderColor;                            ///< Border color, used to draw a border around the bounds if border is enabled
	UColor mBorderColorHL;                          ///< Used instead of mBorderColor when the object is highlited
	UColor mBorderColorNA;                          ///< Used instead of mBorderColor when the object is not active or disabled

	UColor mBevelColorHL;                          ///< Used for the high-light part of the bevel
	UColor mBevelColorLL;                          ///< Used for the low-light part of the bevel

	// Font members
	UString  m_FontFace;                    ///< Font face name for the cOntrol
	INT		m_FontSize;                    ///< Font size for the cOntrol
	enum {
		BaseColor = 0,
		ColorHL,
		ColorNA,
		ColorSEL,
		ColorUser0,
		ColorUser1,
		ColorMax,
	};
	//UColor  m_FontColors[ColorMax];                        ///< Array of Font colors used for UDrawText with escape characters for changing color mid-string
	UColor m_FontColor;                             ///< Main Font color
	UColor m_FontColorHL;                           ///< Highlited Font color
	UColor m_FontColorNA;                           ///< Font color when object is not active/disabled
	UColor m_FontColorSEL;                          ///< Font color when object/text is selected
	UColor m_FontColorUser0;
	UColor m_FontColorUser1;
	UINT  m_FontCharSet;                              ///< Font character Set

	EUTextAlignment mAlignment;                       ///< HorizOntal text alignment
	BOOL mAutoSizeWidth;                            ///< Auto-size the width-bounds of the cOntrol to fit it's cOntents
	BOOL mAutoSizeHeight;                           ///< Auto-size the height-bounds of the cOntrol to fit it's cOntents
	BOOL mReturnTab;                                ///< Used in UEdit to specify if a tab-event should be simulated when return is pressed.
	BOOL mNumbersOnly;                              ///< For text cOntrols, TRUE if this should Only accept numerical data
	BOOL mMouseOverSelected;                        ///< True if this object should be "selected" while the mouse is over it
	UColor mCursorColor;                            ///< Color for the blinking cursor in text fields (for example)

	UString mBitmapBase;
	UPoint mTextOffset;                            ///< Text offset for the cOntrol


	UString strSkinName;
	// sound members
	UString mSoundButtOnDown;
	UString mSoundButtOnOver;
	UString mSoundOpen;
	UString mSoundClose;

	// 
	UFontPtr   m_spFont;                        ///< Font resource 
	USkinPtr m_spSkin;
public:
	UControlStyle();
	virtual ~UControlStyle();
	BOOL Create();
	void Destroy();
	int GetRefCnt() { return m_nRefCnt; }
	void AddRef();
	void Release();
};
//用来记录快捷键的属性
class UIAPI UAccelarKey : public UObject
{
	UDEC_CLASS(UAccelarKey);
public:
	UString mName;
	UString mActionName;
	UString mAccelarKeyCode;
	UString mAccelarKeyModify;
	//BOOL mKeyState;
public:
	UAccelarKey();
	virtual ~UAccelarKey();
};
//ColorTable
class UIAPI UColorTable : public UObject
{
	UDEC_CLASS(UColorTable);
public:
	UColorTable();
	virtual ~UColorTable();
public:
	UString mColorDesc;
	UColor  mColor;
};
void RegisterGuiTypes(void);

enum ELCursorID
{
	LCUR_ARROW,
	LCUR_SIZEUPDOWN,
	LCUR_SIZELEFTRIGHT,
	LCUR_SIZENESW,
	LCUR_SIZENWSE,
	LCUR_TEXTEDIT,
};

enum EUMessageType
{
	UMSG_NOTIFY = 1,
	UMSG_COMMAND,
};

typedef void (UCmdTarget::*UNOTIFY_METHOD)(void);

struct UMSGMAP_ENTRY
{
	UINT nMsg;   // Message Type
	UINT nCode;      // control code or WM_NOTIFY code
	UINT nID;        // control ID (or 0 for windows messages)
	UINT nSig;       // signature type (action) or pointer to message #
	UNOTIFY_METHOD pfn;    // routine to call (or special value)
};

struct UMSGMAP
{
	const UMSGMAP* pBaseMap;
	const UMSGMAP_ENTRY* lpEntries;
};

#define UDEC_MESSAGEMAP() \
private: \
	static const UMSGMAP_ENTRY _cdispentries[]; \
protected: \
	static const UMSGMAP cdispmap; \
	virtual const UMSGMAP* GetMessageMap() const; \


#define UBEGIN_MESSAGE_MAP(theClass, baseClass) \
	const UMSGMAP* theClass::GetMessageMap() const \
{ return &theClass::cdispmap; } \
	const UMSGMAP theClass::cdispmap = \
{ &baseClass::cdispmap, &theClass::_cdispentries[0] }; \
	const UMSGMAP_ENTRY theClass::_cdispentries[] = \
{ \


#define UEND_MESSAGE_MAP() \
{0, 0, 0, UMD_UNKNOWN, (UNOTIFY_METHOD)0 } \
}; \


#include "UEvent.h"



// command TarGet, event dispatch
class UIAPI UCmdTarget : public UObject
{
public:
	UCmdTarget(void);
	~UCmdTarget(void);
protected:
	void DispatchNotify(UINT NotifyMsg);
	void DispatchNotify(UINT NotifyMsg, const UNM* pNM);
	void Lock();
	void Unlock();
private: 
	BOOL ReceiveCmdNotify(UINT cmd, UINT id, const void* pExtra);
	static const UMSGMAP_ENTRY _cdispentries[]; 
protected: 
	static const UMSGMAP cdispmap; 
	virtual const UMSGMAP* GetMessageMap() const; 
};




UIAPI void ULog(const char* fmt, ...);
UIAPI void UError(const char* fmt, ...);

#define ULOG	ULog	
#define UTRACE   ULog
#define UERROR   UError

#endif 