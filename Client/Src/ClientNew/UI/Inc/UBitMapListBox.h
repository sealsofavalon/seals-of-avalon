#ifndef __UBITMAPLISTBOX_H__
#define __UBITMAPLISTBOX_H__

#include "UControl.h"
//#include "UBase.h"

class UIAPI UBitMapListBox : public UControl
{	
	enum
	{
		RECT_TOPANDLEFT   = 0,
		RECT_TOPANDRIGHT,
		RECT_BOTTOMANDLEFT,
		RECT_BOTTOMANDRIGHT,
		RECT_TOP,
		RECT_RIGHT,
		RECT_BOTTOM,
		RECT_LEFT,
		RECT_MIDDLE,
		RECT_MAX
	};
	UDEC_CLASS(UBitMapListBox);
	UDEC_MESSAGEMAP();
public:
	enum 
	{
		ITEM_TEXT_MAX = 256,	// ITEM 支持的最大字符数目
	};
	UBitMapListBox(void);
	~UBitMapListBox(void);
public:
	void Clear();
	void AddLine(UColor linecolor);
	void AddItem(const char* ItemName,void* ItemData = NULL);
	void AddItem(const char* ItemName, USkinPtr skin, UINT skinIndex, void* ItemData = NULL);
	void AddItem(const char* ItemName, UINT Alignment, UColor fontcolor, UFontPtr font = NULL, bool enable = true, void* ItemData = NULL);
	void AddTitle(const char* ItemName, UINT Alignment, UColor fontcolor, UFontPtr font = NULL);
	void AddItem(const WCHAR* ItemName,void* ItemData = NULL);
	void AddItem(const WCHAR* ItemName, USkinPtr skin, UINT skinIndex, void* ItemData = NULL);
	void AddItem(const WCHAR* ItemName, UINT Alignment, UColor fontcolor, UFontPtr font = NULL, bool enable = true, void* ItemData = NULL);
	void AddTitle(const WCHAR* ItemName, UINT Alignment, UColor fontcolor, UFontPtr font = NULL);
	void InsertItem(int Index,const char* ItemName ,void* ItemData = NULL);
	void DeleteItem(int Index);
	void DeleteItemByText(const char* text);
public:
	int  GetCurSel() const;
	void SetCurSel(int Index);
	void SetCurSelAndDispatch(int Index);
	void SetItemEnable(int Index , bool bEnable);
	BOOL IsItemEnable(int Index);
	void SetItemText(int Index,const char* text);
	void SetGridSize(const UPoint& size);
	virtual void GetScrollLineSizes(UINT *rowHeight, UINT *columnWidth);
public:
	int GetSelectedText(char* pText, int nMaxLen);
	INT FindItem(const char* text);
	INT FindItem(const WCHAR* text);
	void* GetItemData(int index);
	UINT GetItemCount();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnSize(const UPoint& NewSize);
	virtual void OnMove(const UPoint& NewOffsetDelta);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
private:
	void DrawChildItem(int Index,const UPoint &offset);
	void SetListHeight(int Height);
	void AllocateItemPos();
	void CalItemRect(const UPoint &size);
protected:
	UString  m_BkgSkinFileName;
private:
	USkinPtr m_BkgSkin;
	URect    m_SkinRect[RECT_MAX];
	struct BitMapListBoxContext * m_pListBoxCtx;
	UPoint m_ItemSize;
	class UScrollBar* m_pScrollBar;
};
#endif