#ifndef __UBITMAP_H__
#define __UBITMAP_H__
#include "UControl.h"

class UIAPI UBitmap : public UControl
{
	UDEC_CLASS(UBitmap);
public:
	UBitmap(void);
	~UBitmap(void);
	BOOL SetBitMapByStr(const char* strFile);
	BOOL SetBitMapSkinByStr(const char* strFile);
	void SetSkinIndex(int index){m_SkinIndex = index ;};

	virtual void SetAlphaCmd(BOOL nbAlphaCMD) {m_bAlphaCmd = nbAlphaCMD;}
	virtual void SetAlpha(BYTE alpha) {m_Alpha = alpha;}
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
protected:
	int m_SkinIndex;
	UString m_strFileName;
	UString m_strSkinFileName;
	UTexturePtr m_spTexture;
	USkinPtr m_spSkin;

	BYTE m_Alpha;
	BOOL m_bAlphaCmd;
	float m_Timer;
};

#endif 
