#ifndef __UBUTTON_H__
#define __UBUTTON_H__
#include "UControl.h"

class UIAPI UButton : public UControl
{
	UDEC_CLASS(UButton);
	UDEC_MESSAGEMAP()
public:
	enum EButtonType 
	{
		BTT_PUSH = 0,
		BTT_CHECK,
		BTT_RADIO,
	};

	enum EButtonSkin
	{
		BTS_NORMAL	= 0,
		BTS_MOUSEOVER,
		BTS_PUSHED,
		BTS_DISABLE,
	};

	enum ECheckBoxSkin		// see CheckBox.skin
	{
		CBS_UNCHECK_NORMAL	= 0,
		CBS_UNCHECK_HILIT,
		CBS_CHECK_NORMAL,
		CBS_CHECK_HILIT,
		CBS_DISABLED = CBS_UNCHECK_NORMAL,		// �ݲ�֧��
	};

	UButton();
	virtual ~UButton();
	int GetButtonType() const { return (int)m_eBtnType;}
	BOOL IsChecked() const { return m_bCheck;}
	BOOL IsPress() {return m_bPressed;}
	void SetCheck(BOOL bCheck);
	void SetCheckState(BOOL bCheck);
	void SetText(const char *text);
	const char* GetText();
	void acceleratorKeyPress(const char * opCode);
	void acceleratorKeyRelease(const char * opCode);
protected:
	void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void RenderGlint(const URect &updateRect, float fTime ,UINT Speed,float alpha);
	virtual void OnRenderGlint(const UPoint& offset,const URect &updateRect);
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRightMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnTimer(float fDeltaTime);
	void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	void OnMouseUp(const UPoint& pt, UINT uFlags);
	void OnMouseEnter(const UPoint& pt, UINT uFlags);
	void OnMouseLeave(const UPoint& pt, UINT uFlags);
	BOOL OnKeyDown(const GuiEvent &event);
	BOOL OnKeyUp(const GuiEvent &event);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	void SetScriptValue(const char *value);
	const char *GetScriptValue();
	//void OnMessage(UControl *,INT msg);
	// 
	virtual void PreCommand();
protected:
	virtual void DrawPushButton(const UPoint& offset, const URect& ClipRect);
	virtual void DrawRadioButton(const UPoint& offset, const URect& ClipRect);
	virtual void DrawCheckBox(const UPoint& offset, const URect& ClipRect);
protected:
	BOOL		m_bPressed;
	BOOL		m_bMouseOver;
	BOOL		m_bCheck;
	BOOL		m_bHori;
	EButtonType	m_eBtnType;
	INT			m_nRadioGroup;
	UString		m_strBtnText;
	UTexturePtr m_GlintTexture[5];
	float m_timer;
};

class UIAPI UBitmapButton : public UButton
{
	UDEC_CLASS(UBitmapButton);
public:
	UBitmapButton();
	virtual ~UBitmapButton();
	void SetBitmap(const char* szBitmapFile);
	USkinPtr GetSkin() { return m_spBtnSkin;}

	virtual void SetAlphaCmd(BOOL nbAlphaCMD) {m_bAlphaCmd = nbAlphaCMD;}
	virtual void SetAlpha(BYTE alpha) {m_Alpha = alpha;}
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnTimer(float fDeltaTime);
	virtual void DrawPushButton(const UPoint& offset, const URect& ClipRect);
	virtual void DrawCheckBox(const UPoint& offset, const URect& ClipRect);
	UString		m_strBitmapFile;
	USkinPtr	m_spBtnSkin;

	BYTE m_Alpha;
	BOOL m_bAlphaCmd;
	float m_Timer;
};


#endif 
