#ifndef __UCOMCTRL_H__
#define __UCOMCTRL_H__
#include "UControl.h"

class UIAPI UComboBox : public UControl
{
	UDEC_CLASS(UComboBox);
public:
	UComboBox(void);
	virtual ~UComboBox(); 

	void AddItem(const char* itemText, void* pUserData = NULL);
	void AddItem(const WCHAR* itemText, void* pUserData = NULL);

	int DeleteItem(int nIndex);
	int InsertItem(int nIndex, const char* pItemText, void* pUserData = NULL);
	void Clear();

	void* GetItemData(int nIndex);
	void SetItemData(int nIndex, void* pUserData);

	int GetCount() const;
	int GetCurSel() const;
	void SetCurSel(int nSelect);

	int FindItem(const char* text) const;
	int FindItem(const WCHAR* text) const;

	int FindItemByData(void* pUserData);
	int GetItemText(int nIndex, char* pDest, int nMaxLen);
	int GetItemTextLen(int nIndex);
	int GetMaxPopupHeight(){return mMaxPopupHeight;};

	void replaceText(int);

	void Sort(BOOL ByUserData);

	BOOL SetSkin(const char* name);
	BOOL SetSkin(USkinPtr& spSkin);
protected:
	enum EComboBoxSkin
	{
		CBBS_ARROW_N = 0,
		CBBS_ARROW_H = 1,
		CBBS_ARROW_D = 2,
	};

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);

	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);


	void setupAutoScroll(const GuiEvent &event);
	void autoScroll();
	BOOL onKeyDown(const GuiEvent &event);

	void onAction();

protected:
	
	int m_nSelIndex;
	int mMaxPopupHeight;
	float mIncValue;
	float mScrollCount;
	int mLastYvalue;
	
	BOOL mMouseOver; //  Added

	UString m_strIconFile; 
	USkinPtr m_spIconSkin;
private:
	struct ComboBox* m_pComboBox;
};


class UComCtrl
{
public:
	UComCtrl(void);
	~UComCtrl(void);
};


#endif 