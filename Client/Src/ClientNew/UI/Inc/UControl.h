#ifndef __UCONTROL_H__
#define __UCONTROL_H__

#include <assert.h>
#include "UBase.h"
#include "URender.h"

#pragma warning(push)
#pragma warning(disable : 4251)

class USystem;
class LUiEdit;
class UControlStyle;
class UCursor;

// DragDrop 修改键, 
// 控件样式如果能相应dragdrop 消息(m_bHandleDragDrop), 那么在DRAGDROP的过程中,依然会接收到OnMouseEnter Leave, Down Up Drag,
// 等消息, 并且,在这些消息的标记(uFlags)中会携带 LKM_DRAGDROP 的标识.
#define LKM_DRAGDROP   0x1000

enum EUAnchorHori
{
	UAH_RELATIVE = 0,		// 相对地对左 右 和宽度产生缩放.
	UAH_WIDTH,				// 固定左,右相对位置, 将对宽度产生缩放. 
	UAH_LEFT,				// 固定右边和宽度, 对左边进行修正.
	UAH_RIGHT,				// 固定左边和宽度, 对右边进行修正.
	UAH_CENTER				// 与父窗口中线对齐,始终保持相对位置处于中线.
};

enum EUAnchorVert
{
	UHV_RELATIVE =0,		///< ResizeControl relative
	UHV_HEIGHT,				///< fixed On the top and bottom
	UHV_TOP,				///< fixed in height and On the bottom
	UHV_BOTTOM,				///< fixed On the top and in height
	UHV_CENTER
};


class UIAPI UControl :  public UCmdTarget//public SimGroup
{
	UDEC_CLASS(UControl);
public:
	BOOL SetId(UINT id);
	inline UINT GetId() const { return m_nCtrlID;}

	void SetSizing(INT horz, INT vert);
public:
	UControl();
	virtual ~UControl();
	inline const UPoint&	GetWindowPos() const		{ return m_Position; }
	inline const UPoint&	GetWindowSize() const		{ return m_Size; }
	inline const URect&		GetControlRect() const		{
		static URect r;
		r.left = m_Position.x;
		r.top = m_Position.y;
		r.right = m_Position.x + m_Size.x;
		r.bottom = m_Position.y + m_Size.y;
		return r;
	}
	inline const UPoint&	GetMinSize() const		{ return m_MinSize; } 
	inline INT				GetLeft() const			{ return m_Position.x; } 
	inline INT				GetTop() const			{ return m_Position.y; }
	inline INT				GetWidth() const			{ return m_Size.x; }
	inline INT				GetHeight() const		{ return m_Size.y; }

	void AddChild(UControl *obj);
	void RemoveChild(UControl *obj);
	UControl* GetParent() const; 
	UControl* GetRoot(); 
	inline int GetNumChilds() const	{ return (int)m_Childs.size();}
	inline UControl* GetChildCtrl(int Index) const
	{
		assert(Index >= 0 && Index < (int)m_Childs.size());
		return m_Childs[Index];
	}
	UControl* GetChildByID(INT nID) const;
	BOOL MoveChildTo(UControl* pChild, UControl* pInsertBerfore);
	virtual BOOL IsChildWindow(UControl *child);

	const UControlStyle* GetStyle() const { return m_Style;}

	virtual void SetVisible(BOOL value);
	inline BOOL IsVisible() const { return m_Visible; } 
	void SetActive(BOOL value);
	BOOL IsActive() const	{ return m_Active; } 
	BOOL IsCreated() const  { return m_bCreated; }


	virtual void GetScrollLineSizes(UINT *rowHeight, UINT *columnWidth);
	virtual void OnSetCursor(UCursor*&cursor, BOOL &showCursor, const GuiEvent &lastGuiEvent);

	static inline USystem* GetSystem() { return sm_System; }
	// ui系统有2个特殊窗口, 桌面和IME组字窗口, 这2个窗口并不暴露, 因此也不需要去处理以下2个接口.
	virtual BOOL IsDesktop() const;
	virtual BOOL IsIMEControl() const { return FALSE;}
	//virtual BOOL CanGetInputFocus() const { return FALSE;}
	UPoint WindowToScreen(const UPoint &src) const;
	UPoint ScreenToWindow(const UPoint &src) const;

	EUAnchorVert GetAnchorVert() const { return (EUAnchorVert)mVertSizing; }
	EUAnchorHori GetAnchorHori() const { return (EUAnchorHori)mHorizSizing; }
	virtual void ResizeControl(const UPoint &NewPos, const UPoint &newExtent);
	virtual void SetPosition( const UPoint &NewPos );
	virtual void SetSize( const UPoint &newExtent );
	virtual void SetWindowRect( const URect &newBounds );
	

	virtual void SetLeft( INT newLeft );
	virtual void SetTop( INT newTop );
	virtual void SetBottom( INT newBottom);
	virtual void SetWidth( INT newWidth );
	virtual void SetHeight( INT newHeight );

	virtual void SetGlint(BOOL bGlint);

	BOOL Create();   
	void Destroy();          

	BOOL CursorInWindow();
	virtual UControl* FindHitWindow(const UPoint &pt, INT initialLayer = -1);

	void CaptureControl(UControl *pCapTarget);
	void CaptureControl();
	void ReleaseCapture();
	BOOL IsCaptured() const;

	BOOL IsDragSrcCtrl() const;

	virtual UControl* FindFirstFocus();
	virtual UControl* FindLastFocus();
	virtual UControl* FindPrevFocus(UControl* pCurFocus);
	virtual UControl* FindNextFocus(UControl* pCurFocus);

	static void SetFocusControl(UControl* pFocusCtrl);
	static UControl* GetFocus();
	BOOL IsFocused();
	void SetFocusControl();
	void ClearFocus();

	static BOOL BeginDragDrop(UControl* pCtrl, USkin* pDragIconSkin, int nIconIndex, void* pUserData, DWORD DataFlag, char* text = NULL, IUFont* font = NULL,bool bDrawBgk = true, bool drawtext = false, UPoint drawsize = UPoint(36,36));
	static void EndDragDrop();

	void AddAcceleratorKey();

	/// Adds this cOntrol's accelerator key to the accelerator map, and
	/// recursively tells all children to do the same.
	virtual void buildAcceleratorMap();

	void SetControlStyle(UControlStyle *prof);
protected:
	void RenderChildWindow(const UPoint& offset, const URect &updateRect);
	virtual BOOL renderTooltip(const UPoint& cursorPos, const char* tipText = NULL );
	
	
	virtual void DrawControlEdge(const URect& rcCtrl);
protected:	// 消息响应
	friend class UDesktop;
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRemove();
	virtual void OnChildRemoved( UControl *child );
	virtual void OnChildAdded( UControl *child );
	virtual BOOL HitTest(const UPoint& parentCoordPoint);
	// 子窗口缩放后调用,以重新计算布局
	//virtual void OnChildResized(UControl *child);
	virtual void OnSize(const UPoint& NewSize);
	virtual void OnChange(bool bResized);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnRenderGlint(const UPoint& offset,const URect &updateRect); //窗口闪烁
	virtual void RenderGlint(const URect &updateRect, float fTime ,UINT Speed, float alpha);
	virtual void OnTimer(float fDeltaTime);
	virtual void OnShow(BOOL bShow);
	virtual void OnMove(const UPoint& NewOffsetDelta);

	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);

	virtual BOOL OnMouseWheel(const UPoint& position, int delta , UINT flags);

	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnRightMouseDragged(const UPoint& position, UINT flags);

	virtual void OnMiddleMouseDown(const UPoint& position, UINT flags);
	virtual void OnMiddleMouseUp(const UPoint& position, UINT flags);
	virtual void OnMiddleMouseDragged(const UPoint& position, UINT flags);

	// TODO : OnCommand();
	virtual void OnAction();

	// 失去焦点
	virtual void OnGetFocus();
	virtual void OnLostFocus();

	virtual void acceleratorKeyPress(const char * opCode);
	virtual void acceleratorKeyRelease(const char * opCode);

	virtual BOOL OnChar(UINT nCharCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags);

	virtual void OnDialogPush();
	virtual void OnDialogPop();

	virtual void OnDragDropBegin(const void* pDragData, UINT nDataFlag);
	virtual void OnDragDropEnd(UControl* pDragTo, void* pDragData, UINT nDataFlag);

	virtual void OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag);
	virtual void OnDeskTopDragEnd(UControl* pDragFrom, const void* pDragData, UINT nDataFlag);
	// Return 
	virtual UINT GetDataFlag();
	virtual BOOL OnDropThis(UControl* pDragTo, UINT nDataFlag);
	virtual void OnReciveDragDrop(UControl* pDragFrom, const UPoint& point,const void* pDragData, UINT nDataFlag );
	// True if you accept the dd-item. false if you reject the dd-item.
	virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
	virtual BOOL OnMouseDownDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag);
	// 如果目标空间接受了对象, 则源控件相应此消息,否则相应Reject消息.
	virtual void OnDragDropAccept(UControl* pAcceptCtrl,const UPoint& point,const void* pDragData, UINT nDataFlag);
	virtual void OnDragDropReject(UControl* pRejectCtrl, const void* pDragData, UINT nDataFlag);

	virtual BOOL OnEscape();
	virtual BOOL IsEscapeFouce();
	virtual void SetEscapeFouce(BOOL exitfouce);
	UDEC_MESSAGEMAP();
protected:
	UControlStyle*	m_Style;
	UControl*		m_Parent;
	INT				m_nCtrlID;
	UPoint			m_Position;		// 窗口位置
	UPoint			m_Size;			// 窗口大小
	UPoint			m_MinSize;		// 最小窗口大小
	short			m_Layer;		// Z 层 Z越大,则层次越靠前(指向屏幕外面)
	BOOL           m_MouseHover;
	INT				mHorizSizing;   
	INT				mVertSizing;
	BOOL			m_Visible;
	BOOL			m_Active;
	BOOL          m_CanEscapeFocuse;
	BOOL			m_IsUpdateChild;
	UINT			m_bCreated:1;
	
	UControlStyle*	mTooltipProfile; 
	INT				mTipHoverTime;
	BOOL           mTipRender;
	float          mBHoverTime;
	BOOL           mBGlint; //窗口闪烁
	float		   mGlintTime ;

	USkinPtr		mPoPo[4] ;
	
	// UINT m_bTopParent:1;  顶级窗口拦截所有消息, 不再发往父级窗口. 并维护所有子级窗口的ID. 顶级窗口下所有的子
	//窗口, 如果拥有ID, 则ID 不能重复, 此规则是递归的,也就是说对子窗口的子窗口也起限制.
	

	typedef std::vector<UControl*> UControlList;
	typedef UControlList::iterator CtrlIterator;
	typedef UControlList::const_iterator CtrlConstIterator;
	UControlList m_Childs;	// 子窗口

public:
	/// @}
	static URender* sm_UiRender;
	static USystem* sm_System;		// UI系统指针, 在调用USystem::Initialize() 后有效,并在 USystem::Shutdown()后无效
protected:

	UString mAcceleratorKey;
	UString m_ToolTips;
};


#pragma warning(pop)
#endif
