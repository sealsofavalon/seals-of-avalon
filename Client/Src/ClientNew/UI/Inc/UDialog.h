#ifndef __UDIALOG_H__
#define __UDIALOG_H__
#include "UControl.h"
#include "UButton.h"

// a dialog control with a background texture and can drag by mouse.
class UIAPI UDialog : public UControl
{
	UDEC_CLASS(UDialog);
	UDEC_MESSAGEMAP();
public:
	UDialog();
	virtual ~UDialog();
public:
	virtual void SetVisible(BOOL value);
protected:
	virtual void OnOk();
	virtual void OnCancle();
	virtual void OnClose();
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);
	virtual BOOL OnEscape();
	virtual void OnSize(const UPoint& NewSize);
protected:
	void OnClickCloseBtn();
public:
	void ActiveWindow();
	
private:
	
protected:
	UString m_strFileName;		// 背景文件名.
	UString m_CloseBtnStrFile;  // 关闭按钮的skin;
	UString m_DlgTitle;
	BOOL m_bShowClose;
	USkinPtr m_spBkgSkin;		// 背景皮肤.
	BOOL m_bCanDrag;			// 是否可以被拖放.
	BOOL m_bLimitDrag;			// 是否限制拖放. 如果允许拖放,且限制拖放, 则窗口不可能逾越桌面.
	UPoint m_ptLeftBtnDown;
	UBitmapButton* m_CloseBtn;
	UPoint m_CloseBtnPos ;
};

class UMessageBoxDialog : public UDialog
{

};

class UResizeDialog : public UDialog
{
};



#endif 