#ifndef __UEDIT_H__
#define __UEDIT_H__
#include "UControl.h"


class UIAPI UEdit : public UControl
{
	UDEC_CLASS(UEdit);
public:
	enum
	{
		MAX_TEXT_LENGTH = 1024,
	};
	UEdit(void);
	virtual ~UEdit(void);
	int GetText(char* dest, int nMaxLen, int nPos = 0);
	int GetText(WCHAR* dest, int nMaxLen, int nPos = 0);
	const UStringW& GetString() const { return m_wstrBuffer;}
	virtual void SetText(const char* txt);
	virtual void SetText(const WCHAR* txt);
	void SetMaxLength(unsigned int uiMax) { m_nMaxLength = uiMax; }
	int   GetCaretCP() const  { return  m_nCaretCP ; }

	void ClearSelection();
	void SetSel(int nStartCP, int nEndCP);
	void SelAll();
	BOOL Undo();
	void Clear();
	void Copy();
	void Cut();
	void Paste();
	
	int GetCareteXPos() const { return m_nCaretXPos;}
	INT PlaceCaretPos(const UPoint &offset);
	void PlaceCaretAt(INT nCP);
	void ResetTextOffset();
	void SetTextAlignment(EUTextAlignment TextAlignment){m_Alignment = TextAlignment;ResetTextOffset();};
	void SetFontColor(UColor color){mFontColor = color;};
	void SetFontColor(const char * color);
	void SetNumberOnly(BOOL NumberOnly){mNumberOnly = NumberOnly;};

	void SoftKeyBoardChar(UINT nCharCode, UINT nRepCnt, UINT nFlags);
	void SoftKeyBoardKeyMsg(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	BOOL SoftKeyBoardShow(BOOL bshow);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual BOOL OnChar(UINT nCharCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnMouseDragged(const UPoint& pt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
	virtual void OnGetFocus();
	virtual void OnLostFocus();
	virtual void OnSize(const UPoint& NewSize);
	virtual void OnMove(const UPoint& NewPos);
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void DrawContext(const URect &drawRect, BOOL isFocused );
	virtual void DrawMask(const URect &drawRect, BOOL isFocused);
	virtual BOOL OnEscape();
	//virtual BOOL ValidateText();
	void updateHistory(UStringW *txt, BOOL moveIndex);
	
	void playDeniedSound();

protected:
	UStringW m_wstrBuffer;	// UNICODE 字符缓存. 
	UStringW m_wStrTipString;
	UFontPtr m_spFont;		// 字体
	INT		 m_nMaxLength;	// 最大允许的字符长度
	// for animating the cursor
	INT		mNumFramesElapsed;
	float	mTimeLastCursorFlipped;
	UColor mCursorColor;
	UColor mFontColor;
	BOOL     mCursorOn;
	BOOL m_bEnableHistory;	// 是否保存历史记录
	//BOOL	m_bReadOnly;  disabled mean readonly.
	BOOL     mNumberOnly;
	BOOL     mInsertOn;
	INT      mMouseDragStart;
	UPoint  mTextOffset;
	BOOL     mTextOffsetReset;
	BOOL     mDragHit;
	BOOL     mTabComplete;
	BOOL    mbFouceChoose;
	INT      mScrollDir;
	EUTextAlignment m_Alignment;

	INT      m_nSelStart;
	INT      m_nSelEnd;
	INT      m_nCaretCP;

	int m_nCaretXPos;
	BOOL	mHistoryDirty;
	INT	mHistoryLast;
	INT	mHistoryIndex;
	INT	mHistorySize;
	BOOL m_bPassword;
	WCHAR m_PasswordMask;

	/// If set, any non-ESC key is handled here or not at all
	BOOL    mSinkAllKeyEvents;   
	WCHAR   **mHistoryBuf;
	struct EditContext* m_pContext;
	struct EditHistory* m_pHistory;

	BOOL m_bUserSoftKey;
	BOOL m_bShowSoftKey;
};

#endif 