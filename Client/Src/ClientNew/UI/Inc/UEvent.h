
enum EUNotifyMessage
{
	UBN_PRESS = 10,
	UBN_CLICKED = 11,		// 按钮(CHECKBOX,RADIOBOX)被点击
	UBN_RCLICKED = 12,

	UED_CHANGED = 15,		// Edit 字符改变
	UED_UPDATE = 16,		// Edit 字符更新, 当回车,TAB的时候,编辑框会通知更新消息.	

	UGD_UPAG_LBCLICK = 17,
	UGD_UPAG_SHIFT_AND_RBCLICK = 18, //鼠标右键+ Shift
	UGD_UPAG_RBCLICK = 19,  //

	UGD_DBCLICK = 20,		// grid 控件双击单元格
	UGD_SELCHANGE = 21,		// GRID 控件单元格选择改变.
	UGD_RBCLICK = 22,       
	UGD_SHIFT_AND_RBCLICK = 23 ,
	ULB_SELCHANGE = 30,


	UCBB_SELCHANGE = ULB_SELCHANGE,		// combobox 选择发生改变

	UBMLB_SELECT  = 31,

	URED_CLICKURL = 40,
	URED_RIGHTCLICKURL = 41,
	URED_SHIFTCLICKURL = 42,
	URED_CTRLCLICKURL = 43,

	URED_ONMOUSEMOVEURL = 44,
	URED_OUTMOUSEMOVEURL = 45,

	// 计时器
	UTIMEBAE_TIME_FINISH = 50 ,

	USLIDER_CHANGE,
};


struct UNM
{

};

struct URichEditURLClickNM : public UNM
{
	const char* pszURL;
};

enum EUMsgDispFlag
{
	UMD_UNKNOWN = 0,
	UMDCMD_V,
	UMDNOTIFY_V,
	UMDNOTIFY_V_U,			// void (UNM*)
};


#define UON_COMMAND(id, memberFxn) \
{ UMSG_COMMAND, 0, (WORD)id, UMDCMD_V, \
	(UNOTIFY_METHOD)(memberFxn) },

#define UON_NOTIFY(wNotifyCode, id, memberFxn) \
{ UMSG_NOTIFY, (WORD)(int)wNotifyCode, (WORD)id, UMDNOTIFY_V, \
	(UNOTIFY_METHOD)(memberFxn) },

#define UON_CONTROL(wNotifyCode, id, memberFxn) \
{ UMSG_COMMAND, (WORD)wNotifyCode, (WORD)id, UMDCMD_V, \
	(static_cast< UNOTIFY_METHOD > (memberFxn)) },

#define UON_BN_CLICKED(id, memberFxn) \
	UON_CONTROL(UBN_CLICKED, id, memberFxn)

#define UON_BN_RCLICKED(id, memberFxn) \
	UON_CONTROL(UBN_RCLICKED, id, memberFxn)

#define UON_BN_PRESS(id, memberFxn) \
	UON_CONTROL(UBN_PRESS, id, memberFxn)

#define UON_UGD_DBCLICKED(id, memberFxn) \
	UON_CONTROL(UGD_DBCLICK, id, memberFxn)

#define UON_UGD_UPAG_RBCLICKED(id, memberFxn)\
	UON_CONTROL(UGD_UPAG_RBCLICK, id, memberFxn)

#define UON_UGD_UPAG_LBCLICKED(id, memberFxn)\
	UON_CONTROL(UGD_UPAG_LBCLICK, id, memberFxn)


#define UON_UGD_UPAG_SHIFT_AND_RBCLICKED(id, memberFxn)\
	UON_CONTROL(UGD_UPAG_SHIFT_AND_RBCLICK, id, memberFxn)

#define UON_UGD_SHIFT_AND_RBCLICKED(id, memberFxn)\
	UON_CONTROL(UGD_SHIFT_AND_RBCLICK, id, memberFxn)

#define UON_SLIDER_CHANGE(id, memberFxn)\
	UON_CONTROL(USLIDER_CHANGE, id, memberFxn)

#define  UON_UGD_CLICKED(id, memberFxn) \
	UON_CONTROL(UGD_SELCHANGE, id, memberFxn)

#define  UON_BMLB_CLICKED(id, memberFxn) \
	UON_CONTROL(UBMLB_SELECT, id, memberFxn)

#define  UON_UGD_RBCLICKED(id,memberFxn) \
	UON_CONTROL(UGD_RBCLICK, id , memberFxn)

#define  UON_CBOX_SELCHANGE(id, memberFxn)\
	UON_CONTROL(UCBB_SELCHANGE, id, memberFxn)

#define  UON_UEDIT_UPDATE(id, memberFxn)\
	UON_CONTROL(UED_UPDATE, id, memberFxn)

#define  UON_UEDIT_CHANGED(id, memberFxn)\
	UON_CONTROL(UED_CHANGED, id, memberFxn)

#define  UON_UTIMEBAR_TIMEFINISH(id, memberFxn)\
	UON_CONTROL(UTIMEBAE_TIME_FINISH, id, memberFxn)

#define UON_RED_URL_CLICKED(id, memberFxn) \
{ UMSG_COMMAND, (WORD)URED_CLICKURL, (WORD)id, UMDNOTIFY_V_U, \
	((UNOTIFY_METHOD)static_cast< void (UCmdTarget::*)(UNM*) > (memberFxn)) },

#define UON_RED_URL_RCLICKED(id, memberFxn) \
{ UMSG_COMMAND, (WORD)URED_RIGHTCLICKURL, (WORD)id, UMDNOTIFY_V_U, \
	((UNOTIFY_METHOD)static_cast< void (UCmdTarget::*)(UNM*) > (memberFxn)) },

#define UON_RED_URL_SHIFTCLICKED(id, memberFxn) \
{ UMSG_COMMAND, (WORD)URED_SHIFTCLICKURL, (WORD)id, UMDNOTIFY_V_U, \
	((UNOTIFY_METHOD)static_cast< void (UCmdTarget::*)(UNM*) > (memberFxn)) },

#define UON_RED_URL_CTRLCLICKED(id, memberFxn) \
{ UMSG_COMMAND, (WORD)URED_CTRLCLICKURL, (WORD)id, UMDNOTIFY_V_U, \
	((UNOTIFY_METHOD)static_cast< void (UCmdTarget::*)(UNM*) > (memberFxn)) },

#define UON_RED_URL_MOUSE_MOVE(id, memberFxn) \
{ UMSG_COMMAND, (WORD)URED_ONMOUSEMOVEURL, (WORD)id, UMDNOTIFY_V_U, \
	((UNOTIFY_METHOD)static_cast< void (UCmdTarget::*)(UNM*) > (memberFxn)) },

#define  UON_RED_URL_OUT_MOUSE(id, memberFxn)\
	UON_CONTROL(URED_OUTMOUSEMOVEURL,id,memberFxn)