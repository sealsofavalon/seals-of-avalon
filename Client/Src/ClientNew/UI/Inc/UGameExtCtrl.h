/*
*		定义MMO游戏中常用的扩展控件
*
*/
#ifndef __UGAMEEXTCTRL_H__
#define __UGAMEEXTCTRL_H__

#include "UControl.h"
#include "UButton.h"
#include "UGrid.h"

// 动态图标按钮, 可以随意跟换图标, 并有冷却计时功能
class UIAPI UDynamicIconButton : public UBitmapButton
{
	UDEC_CLASS(UDynamicIconButton);
	enum
	{
		MASK_FRAME_MAX = 60,		// 60 frame.
	};
public:
	UDynamicIconButton();
	virtual ~UDynamicIconButton();
	void SetIcon(USkin* pSkin, int nIndex);
	void StartTick(float tickTime);
	void EndTick();
	inline void SetTickTime(float fTime, float fTotalTime) { m_fTimer = fTime;m_bEnableFreeze = TRUE; m_bTicking = TRUE; m_FreezeTime = fTotalTime;}
	BOOL     GetIsTicking(){return m_bTicking;}
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void DrawPushButton(const UPoint& offset, const URect& ClipRect);
	virtual void OnTimer(float fDeltaTime);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
protected:
	int m_nIconIndex;
	float m_FreezeTime;
	float m_fTimer;
	BOOL m_bTicking;
	BOOL m_bEnableFreeze;
	WORD m_nMaskIndex;
	UTexturePtr m_spMaskTexture;
	UTexturePtr m_spGlowTexture;
};

// 处理物品拖放
class UIAPI UItemContainer : public UGridCtrl
{
	UDEC_CLASS(UItemContainer);
	
public:
	
	UItemContainer();
	virtual ~UItemContainer();
	virtual void SetGridCount(int nRow, int nCol);
	BOOL AddItem(const char* pszSkinName, int nIconIndex, int nRow, int nCol, void* UserData, int num = 0);
	BOOL AddItem(USkin* pSkin, int nIconIndex, int nRow, int nCol, void* UserData, int num = 0);
	void DeleteItem(int nRow, int nCol);
	void* GetItemData(int nRow, int nCol) const;
	void SetItemData(int nRow, int nCol, void* UserData);
	BOOL HasItem(int nRow, int nCol) const;
protected:
	struct ITEM
	{
		inline ITEM():spSkin(NULL),nIconIndex(-1),nItemNum(0),UserData(NULL)
		{
		}
		USkinPtr spSkin;	
		int nIconIndex;
		int nItemNum;
		void* UserData;
	};

	struct DragData
	{
		enum 
		{ 
			DATA_FLAG = 0xD0E0A0D0,
		};
		void* ItemUserData;
	};
	// pItem1 = pItem2 , pItem2 = pItem1;
	void SwapItem(ITEM* pItem1, ITEM* pItem2);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseMove(const UPoint& pt, UINT uFlags);
	virtual void OnMouseDragged(const UPoint& pt, UINT uFlags);
	virtual void OnMouseEnter(const UPoint& pt, UINT uFlags);
	virtual void OnMouseLeave(const UPoint& pt, UINT uFlags);
	virtual void OnDragDropEnd(UControl* pDragTo, void* DragData, UINT nDataFlag);
	virtual BOOL OnMouseUpDragDrop(UControl* pDragFrom,const UPoint& point, const void* DragData, UINT nDataFlag);
	virtual void OnDragDropAccept(UControl* pAcceptCtrl, const UPoint& point,const void* DragData, UINT nDataFlag);
	virtual void OnDragDropReject(UControl* pRejectCtrl, const void* DragData, UINT nDataFlag);

	virtual void OnShowTipsInfo();
protected:
	ITEM* m_pItems; 
	BOOL m_bDragBegin;
	UTexturePtr m_spItemMask;
	UPoint m_DragItem;
};

#endif 
