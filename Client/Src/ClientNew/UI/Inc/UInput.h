#ifndef __UINPUT_H__
#define __UINPUT_H__

//////////////////////////////////////////////////////////////////////////
enum EKeyModFlag
{
	LKM_NONE		= 0x0000,
	LKM_LCTRL		= 0x0001,
	LKM_RCTRL		= 0x0002,
	LKM_LALT		= 0x0004,
	LKM_RALT		= 0x0008,
	LKM_LSHIFT		= 0x0010,
	LKM_RSHIFT		= 0x0020,
	LKM_CAPSLOCK	= 0x0040,
	LKM_NUMLOCK		= 0x0080,
	LKM_CTRL		=	(LKM_LCTRL	| LKM_RCTRL),
	LKM_ALT			=	(LKM_LALT	| LKM_RALT),
	LKM_SHIFT		=	(LKM_LSHIFT	| LKM_RSHIFT),
};

//////////////////////////////////////////////////////////////////////
// 从dinput.h 拷贝而来, 但是不要武断地认为 (DIK)keycode == DIK. 或许将来, 这些名字相近
// 的键名将对应不同的值.
enum KeyCodes 
{
	LK_NULL				= 0x00,
	LK_ESCAPE          = 0x01,
	LK_1               = 0x02,
	LK_2               = 0x03,
	LK_3               = 0x04,
	LK_4               = 0x05,
	LK_5               = 0x06,
	LK_6               = 0x07,
	LK_7               = 0x08,
	LK_8               = 0x09,
	LK_9               = 0x0A,
	LK_0               = 0x0B,
	LK_MINUS           = 0x0C,   /* - on main keyboard */
	LK_EQUALS          = 0x0D,		// = +
	LK_BACK		= 0x0E,    /* backspace */
	LK_TAB             = 0x0F,
	LK_Q               = 0x10,
	LK_W               = 0x11,
	LK_E               = 0x12,
	LK_R               = 0x13,
	LK_T               = 0x14,
	LK_Y               = 0x15,
	LK_U               = 0x16,
	LK_I               = 0x17,
	LK_O               = 0x18,
	LK_P               = 0x19,
	LK_LBRACKET        = 0x1A,		// [{
	LK_RBRACKET        = 0x1B,		// ]}
	LK_RETURN          = 0x1C,    /* Enter on main keyboard */
	LK_LCONTROL        = 0x1D,
	LK_A               = 0x1E,
	LK_S               = 0x1F,
	LK_D               = 0x20,
	LK_F               = 0x21,
	LK_G               = 0x22,
	LK_H               = 0x23,
	LK_J               = 0x24,
	LK_K               = 0x25,
	LK_L               = 0x26,
	LK_SEMICOLON       = 0x27,		// : ;
	LK_APOSTROPHE      = 0x28,		// ' "
	LK_GRAVE           = 0x29,    /* ~ ` accent grave */
	LK_LSHIFT          = 0x2A,
	LK_BACKSLASH       = 0x2B,		// \ |
	LK_Z               = 0x2C,
	LK_X               = 0x2D,
	LK_C               = 0x2E,
	LK_V               = 0x2F,
	LK_B               = 0x30,
	LK_N               = 0x31,
	LK_M               = 0x32,
	LK_COMMA           = 0x33,		// , on main keyboard
	LK_PERIOD          = 0x34,    /* . on main keyboard */
	LK_SLASH           = 0x35,    /* / on main keyboard */
	LK_RSHIFT          = 0x36,
	LK_MULTIPLY        = 0x37,    /* * on numeric keypad */
	LK_LALT           = 0x38,    /* left Alt */
	LK_SPACE           = 0x39,
	LK_CAPSLOCK         = 0x3A,
	LK_F1              = 0x3B,
	LK_F2              = 0x3C,
	LK_F3              = 0x3D,
	LK_F4              = 0x3E,
	LK_F5              = 0x3F,
	LK_F6              = 0x40,
	LK_F7              = 0x41,
	LK_F8              = 0x42,
	LK_F9              = 0x43,
	LK_F10             = 0x44,
	LK_NUMLOCK         = 0x45,
	LK_SCROLL          = 0x46,    /* Scroll Lock */
	LK_NUMPAD7         = 0x47,		// NP Home, 7 
	LK_NUMPAD8         = 0x48,		// NP Up , 8
	LK_NUMPAD9         = 0x49,
	LK_SUBTRACT        = 0x4A,    /* - on numeric keypad */
	LK_NUMPAD4         = 0x4B,
	LK_NUMPAD5         = 0x4C,
	LK_NUMPAD6         = 0x4D,
	LK_ADD				= 0x4E,    /* + on numeric keypad */
	LK_NUMPAD1         = 0x4F,
	LK_NUMPAD2         = 0x50,
	LK_NUMPAD3         = 0x51,
	LK_NUMPAD0         = 0x52,
	LK_DECIMAL			= 0x53,   /* . on numeric keypad */
	LK_OEM_102         = 0x56,    /* <> or \| on RT 102-key keyboard (Non-U.S.) */
	LK_F11             = 0x57,
	LK_F12             = 0x58,
	LK_F13             = 0x64,    /*                     (NEC PC98) */
	LK_F14             = 0x65,    /*                     (NEC PC98) */
	LK_F15             = 0x66,    /*                     (NEC PC98) */
	LK_KANA            = 0x70,    /* (Japanese keyboard)            */
	LK_ABNT_C1         = 0x73,    /* /? on Brazilian keyboard */
	LK_CONVERT         = 0x79,    /* (Japanese keyboard)            */
	LK_NOCONVERT       = 0x7B,    /* (Japanese keyboard)            */
	LK_YEN             = 0x7D,    /* (Japanese keyboard)            */
	LK_ABNT_C2         = 0x7E,    /* Numpad . on Brazilian keyboard */
	LK_NUMPADEQUALS    = 0x8D,    /* = on numeric keypad (NEC PC98) */
	LK_PREVTRACK       = 0x90,    /* Previous Track (LK_CIRCUMFLEX on Japanese keyboard) */
	LK_AT              = 0x91,    /*                     (NEC PC98) */
	LK_COLON           = 0x92,    /*                     (NEC PC98) */
	LK_UNDERLINE       = 0x93,    /*                     (NEC PC98) */
	LK_KANJI           = 0x94,    /* (Japanese keyboard)            */
	LK_STOP            = 0x95,    /*                     (NEC PC98) */
	LK_AX              = 0x96,    /*                     (Japan AX) */
	LK_UNLABELED       = 0x97,    /*                        (J3100) */
	LK_NEXTTRACK       = 0x99,    /* Next Track */
	LK_NUMPADENTER     = 0x9C,    /* Enter on numeric keypad */
	LK_RCONTROL        = 0x9D,
	LK_MUTE            = 0xA0,    /* Mute */
	LK_CALCULATOR      = 0xA1,    /* Calculator */
	LK_PLAYPAUSE       = 0xA2,    /* Play / Pause */
	LK_MEDIASTOP       = 0xA4,    /* Media Stop */
	LK_VOLUMEDOWN      = 0xAE,    /* Volume - */
	LK_VOLUMEUP        = 0xB0,   /* Volume + */
	LK_WEBHOME         = 0xB2,    /* Web home */
	LK_NUMPADCOMMA     = 0xB3,    /* , on numeric keypad (NEC PC98) */
	LK_DIVIDE		= 0xB5,    /* / on numeric keypad */
	LK_SYSRQ           = 0xB7,
	LK_RALT           = 0xB8,    /* right Alt */
	LK_PAUSE           = 0xC5,    /* Pause */
	LK_HOME            = 0xC7,    /* Home on arrow keypad */
	LK_UP              = 0xC8,    /* SBR_UP on arrow keypad */
	LK_PGUP           = 0xC9,    /* PgUp on arrow keypad */
	LK_LEFT            = 0xCB,    /* SBR_LEFT on arrow keypad */
	LK_RIGHT           = 0xCD,    /* SBR_RIGHT on arrow keypad */
	LK_END             = 0xCF,    /* End on arrow keypad */
	LK_DOWN            = 0xD0,    /* SBR_DOWN on arrow keypad */
	LK_PGDN            = 0xD1,    /* PgDn on arrow keypad */
	LK_INSERT          = 0xD2,    /* Insert on arrow keypad */
	LK_DELETE          = 0xD3,    /* Delete on arrow keypad */
	LK_LWIN            = 0xDB,    /* Left Windows key */
	LK_RWIN            = 0xDC,    /* Right Windows key */
	LK_APPS            = 0xDD,    /* AppMenu key */
	LK_POWER           = 0xDE,    /* System Power */
	LK_SLEEP           = 0xDF,    /* System Sleep */
	LK_WAKE            = 0xE3,    /* System Wake */
	LK_WEBSEARCH       = 0xE5,    /* Web Search */
	LK_WEBFAVORITES    = 0xE6,    /* Web Favorites */
	LK_WEBREFRESH      = 0xE7,    /* Web Refresh */
	LK_WEBSTOP         = 0xE8,    /* Web Stop */
	LK_WEBFORWARD      = 0xE9,    /* Web Forward */
	LK_WEBBACK         = 0xEA,    /* Web Back */
	LK_MYCOMPUTER      = 0xEB,    /* My Computer */
	LK_MAIL            = 0xEC,    /* Mail */
	LK_MEDIASELECT     = 0xED,    /* Media Select */
};

#define LK_BACKSPACE       LK_BACK            /* backspace */
#define LK_NUMPADSTAR      LK_MULTIPLY        /* * on numeric keypad */
#define LK_LMENU			LK_LALT				/* left Alt */
#define	LK_CAPITAL			LK_CAPSLOCK			/* CapsLock */
#define LK_NUMPADMINUS     LK_SUBTRACT        /* - on numeric keypad */
#define LK_NUMPADPLUS      LK_ADD             /* + on numeric keypad */
#define LK_NUMPADPERIOD    LK_DECIMAL         /* . on numeric keypad */
#define LK_NUMPADSLASH     LK_DIVIDE          /* / on numeric keypad */
#define LK_RMENU			LK_RALT				/* right Alt */
#define LK_UPARROW         LK_UP              /* SBR_UP on arrow keypad */
#define	LK_PRIOR			LK_PGUP				/* PgUp on arrow keypad */
#define LK_LEFTARROW       LK_LEFT            /* SBR_LEFT on arrow keypad */
#define LK_RIGHTARROW      LK_RIGHT           /* SBR_RIGHT on arrow keypad */
#define LK_DOWNARROW       LK_DOWN            /* SBR_DOWN on arrow keypad */
#define LK_NEXT				LK_PGDN				/* PgDn on arrow keypad */

enum EMouseCode
{
	LK_LEFTBTN = 0x01,
	LK_MIDBTN = 0x02,
	LK_RIGHTBTN = 0x03,

	LKEY_MOUSE_BUTTON3			= 0x00040000,
	LKEY_MOUSE_BUTTON4			= 0x00050000,
	LKEY_MOUSE_BUTTON5			= 0x00060000,
	LKEY_MOUSE_BUTTON6			= 0x00070000,
	LKEY_MOUSE_BUTTON7			= 0x00080000,
	LKEY_MWHEEL_UP				= 0x00090000,
	LKEY_MWHEEL_DOWN			= 0x000A0000,
	LKEY_MAXIS_X				= 0x000B0000,
	LKEY_MAXIS_Y				= 0x000C0000,
	//	LKEY_MAXIS_Z				= 0x000D0000,
};


enum JoystickCodes 
{
	LKEY_XPOV           = 0x204,
	LKEY_YPOV           = 0x205,
	LKEY_UPOV           = 0x206,
	LKEY_DPOV           = 0x207,
	LKEY_LPOV           = 0x208,
	LKEY_RPOV           = 0x209,
	LKEY_XAXIS          = 0x20B,		
	LKEY_YAXIS          = 0x20C,
	LKEY_ZAXIS          = 0x20D,
	LKEY_RXAXIS         = 0x20E,
	LKEY_RYAXIS         = 0x20F,
	LKEY_RZAXIS         = 0x210,
	LKEY_SLIDER         = 0x211,
	LKEY_XPOV2          = 0x212,
	LKEY_YPOV2          = 0x213,
	LKEY_UPOV2          = 0x214,
	LKEY_DPOV2          = 0x215,
	LKEY_LPOV2          = 0x216,
	LKEY_RPOV2          = 0x217
};

enum GamePadCode
{
	LKEY_GP_A   	    = 0x01000000,
	LKEY_GP_B       	= 0x02000000,
	LKEY_GP_X   	    = 0x03000000,
	LKEY_GP_Y   	    = 0x04000000,
	LKEY_GP_BLACK       = 0x05000000,
	LKEY_GP_WHITE       = 0x06000000,
	LKEY_GP_LEFT_TRIGGER  = 0x07000000,
	LKEY_GP_RIGHT_TRIGGER = 0x08000000,

	LKEY_GP_DPAD_UP       = 0x11000000,
	LKEY_GP_DPAD_DOWN     = 0x12000000,
	LKEY_GP_DPAD_LEFT     = 0x13000000,
	LKEY_GP_DPAD_RIGHT    = 0x14000000,
	LKEY_GP_START         = 0x15000000,
	LKEY_GP_BACK          = 0x16000000,
	LKEY_GP_LEFT_THUMB    = 0x17000000,
	LKEY_GP_RIGHT_THUMB   = 0x18000000,

	LKEY_GP_STHUMBLUP     = 0x19000000,
	LKEY_GP_STHUMBLDOWN   = 0x1a000000,
	LKEY_GP_STHUMBLLEFT   = 0x1b000000,
	LKEY_GP_STHUMBLRIGHT  = 0x1c000000,

	LKEY_GP_STHUMBLX      = 0x21000000,
	LKEY_GP_STHUMBLY      = 0x22000000,
	LKEY_GP_STHUMBRX      = 0x23000000,
	LKEY_GP_STHUMBRY      = 0x24000000,
};
//////////////////////////////////////////////////////////////////////////
//#define LKEY_MAX		(LKEY_OEM_102+1)			///< 最大键盘实例数
#define LKEY_UNKNOWN   0x01
#define LKEY_BUTTON    0x02
#define LKEY_POV       0x03
#define LKEY_KEY       0x0A
#define SI_TEXT      0x10

inline BOOL IsVirtualKey(WORD vk)
{
	if (vk >= 0x08 && vk <= 0x12)
	{
		return TRUE;
	}
	if (vk == 0x1B)
	{
		return TRUE;
	}
	if (vk >= 0x20 && vk <= 0x2E)
	{
		return TRUE;
	}
	if (vk >= 0x30 && vk <= 0x39)
	{
		return TRUE;
	}
	if (vk >= 0x41 && vk <= 0x5A)
	{
		return TRUE;
	}
	if (vk >= 0x70 && vk <= 0x7B)
	{
		return TRUE;
	}
	return FALSE;
}

KeyCodes UIAPI UDikToLKey(BYTE dik);
BYTE  UIAPI LKeyToDik(KeyCodes lk);
KeyCodes UIAPI VKToLKey(BYTE vk);
BYTE UIAPI LKeyToVK(BYTE lk);
void InitKeyMapTable();
UINT UIAPI StringKey2LK( const char * key );
const char UIAPI*  LK2StringKey(UINT lk);
#endif 