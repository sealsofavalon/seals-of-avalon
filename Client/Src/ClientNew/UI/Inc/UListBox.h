#ifndef __ULISTBOX_H__
#define __ULISTBOX_H__
#include "UGrid.h"
class UScrollBar;

// 一个简单的ListCtrl , 仅仅只存储字符节点
class UIAPI UListBox : public UGridCtrl
{
	UDEC_CLASS(UListBox);
	
public:
	enum 
	{
		ITEM_TEXT_MAX = 256,	// ITEM 支持的最大字符数目
	};

	UListBox(void);
	virtual ~UListBox(void);

	UINT GetItemCount();

	void Clear();
	void AddItem(const char *text, void* pUserData = NULL);
	void AddItem(const WCHAR* text, void* pUserData = NULL);
	void InsertItem(int index, const char *text, void* pUserData = NULL);
	void DeleteItem(int index);
	void DeleteItemByText(const char* text);

	int GetCurSel() const;
	void SetCurSel(int index);

	void SetItemEnable(int index, bool bEnable);
	bool IsItemEnable(int index);

	void SetItemText(int index, const char *text);
	
	INT FindItemByData(void* pUserData);
	INT FindItem(const char *text);
	INT FindItem(const WCHAR *text);
	void* GetItemData(int index);
protected:
	void SetItemCount(int nNumItems);

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver);

	virtual void OnSelChange(int row, int col);
	virtual void OnRemove();
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
protected:
	virtual void Sort(UINT column, bool increasing = TRUE);
	virtual void SortNumerical(UINT column, bool increasing = TRUE);
public:
	UINT GetSelectedRow();
	//UINT GetRowWidth(Entry *row);

	int GetSelectedText(char* pText, int nMaxLen);


protected:
	BOOL m_Enumerate;  //列举
	BOOL m_ResizeCell;
	BOOL m_FitParentWidth;
	BOOL m_ClipColumnText;
	struct ListBoxContext* m_pListBoxCtx;
	UPoint m_HightLightSize;
	UString  m_ChooseBkgSkinFileName;
	UTexturePtr m_ChooseBkg;
};
#endif