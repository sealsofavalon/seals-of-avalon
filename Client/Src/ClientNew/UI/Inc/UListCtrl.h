#ifndef __ULISTCTRL_H__
#define __ULISTCTRL_H__
#include "UControl.h"

struct ULCITEM 
{
	const char* tableEntryText;
	BOOL        isSelected;
	void*       itemDate;
	UColor      color;
	BOOL        hasColor;
};
typedef std::vector<INT> vInt;
class UIAPI UListCtrl : public UControl
{
	UDEC_CLASS(UListCtrl);
public:
	
	UListCtrl(void);
	virtual ~UListCtrl(void);
protected:
	INT GetItemCount();
	INT GetSelectCount();
    INT GetSelectedItem();
	void GetSelectedItems(vInt& Items);
	INT GetItemIndex(ULCITEM* item);
	const char* GetItemText(INT index);

	void SetCurSelected(INT index);
	void SetCurSelectedRang(INT start , INT stop);
	void SetItemText(INT index,const char* text);

	INT AddItem(const char* text, void*itemDate = NULL);
	INT AddItemWithColour(const char* text,UColor color = UColor(-1,-1,-1), void*itemDate = NULL);
	void AddSeletion(ULCITEM *item, INT index );
	INT InsertItem(INT index, const char* text,void*itemDate = NULL);
	INT InsertItemWithColour(INT index, const char* text,UColor color = UColor(-1,-1,-1), void*itemDate = NULL);
	INT FindItemByText(const char* text,BOOL caseSensitive = FALSE);

	void SetItemColor(INT index,UColor color);
	void ClearItemColor(INT index);

	void DeleteItem(INT index);
	void ClearItems();
	void ClearSelection();
	void RemoveSelection(ULCITEM* item, INT index);
	void RemoveSelection(INT index);

	inline void SetMultipleSelection(BOOL allowMultipleSelect = TRUE){m_MultipeSelections = allowMultipleSelect;}

	void UpdateSize();
	virtual void OnRenderItem( URect itemRect, ULCITEM *item );
	void DrawBox(const UPoint &box, INT size, UColor &outlineColor, UColor &boxColor);
	static INT        getStringElementCount( const char *inString );
	static const char* getStringElement( const char* inString, const INT index );
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);
protected:
	BOOL           m_MultipeSelections;
	UPoint         m_ItemSize;
	BOOL           m_FitParentWidth;
	ULCITEM*        m_LastClickItem;
	struct ListCtrlContex* m_pListCtx ;
};
#endif