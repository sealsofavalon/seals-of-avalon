#ifndef __UPROGRESS_H__
#define __UPROGRESS_H__
#include "UControl.h"

class UIAPI UProgressCtrl : public UControl
{
	UDEC_CLASS(UProgressCtrl);
public:
	enum Progress_v_Skin
	{
        NormalCenterSkin = 0,
		LightCenterSkin,
		LeftOrTopSkin,  //顶部或者左边
		RightOrButtomSkin,   //底部或者右边
	};
	UProgressCtrl(void);
	virtual ~UProgressCtrl(void);
	float GetPos(void){return m_Progress;}
	void  SetPos(float pos);
	void  SetMaxPos(void){m_Progress = 1.0f;}  //最大1
	void  SetMinPos(void){m_Progress = 0.0f;}  //最小0
	void  SetText(int a, int b);
	
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
protected:
	float m_Progress ;
	BOOL m_bVertical ; //是否垂直
    BOOL m_bShowtext ; //是否显示文字
	UString m_text;
	UString	m_strBitmapFile;      
	USkinPtr m_ProgressSkin;  //背景纹理
};

class UIAPI UProgressBar_Skin : public UControl
{
	UDEC_CLASS(UProgressBar_Skin);
public:
	UProgressBar_Skin();
	~UProgressBar_Skin();
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseEnter(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
public:
	virtual void SetProgressPos( unsigned int agePos,  unsigned int lifePos);
	void SetProgressAge( unsigned int age);
	void SetProgressLife( unsigned int life);
	void SetProgressPos(float pos);
	void SetSkin(const char* fileName);
	void UpdateText();
	void SetShowText(bool bShow){m_bShowText = bShow;};
	void SetAlpha(BYTE alpha){mAlpha = alpha;};
	void SetNorEnding();
	void SetBadEnding();
protected:
	UString m_strSkinFile;
	UString m_strBarHSkinFile;
	USkinPtr m_BarSkin;
	USkinPtr m_BarHeadSkin;
	UString mShowText;

	 unsigned int m_agePos;
	 unsigned int m_lifePos;
	float m_fProgressPos;

	BYTE mAlpha;
	bool mbBadEnding;

	bool m_bShowText;
};

#endif