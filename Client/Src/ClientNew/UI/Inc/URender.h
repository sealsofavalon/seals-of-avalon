#pragma once
#include "UBase.h"

// TODO : 将接口整合到URender 中, 并修改URender 为 IUPlugin. 
struct IUSoundInterface
{
	virtual void PlaySound(const char* pszSoundFile) = 0;		// NOTE: 并不在UI里面去维护任何句柄形式的音频缓冲和生源, 用户自行实现. 
	virtual void PlaySound(int Index) = 0;
};


struct IURenderInterface
{
    virtual void SetRenderState(D3DRENDERSTATETYPE State, DWORD Value) = 0;
    virtual DWORD GetRenderState(D3DRENDERSTATETYPE State) = 0;

    virtual void SetFVF(DWORD FVF) = 0;
    virtual void SetVertexShader(void* pShader) = 0;
    virtual void SetPixelShader(void* pShader) = 0;
    virtual void SetSamplerState(DWORD Sampler, D3DSAMPLERSTATETYPE Type, DWORD Value) = 0;
    virtual void SetTextureStageState(DWORD Stage, D3DTEXTURESTAGESTATETYPE Type, DWORD Value) = 0;
    virtual void SetTexture(DWORD Sampler, IDirect3DBaseTexture9 * pTexture) = 0;

    virtual HRESULT DrawIndexedPrimitiveUP(D3DPRIMITIVETYPE PrimitiveType, UINT MinVertexIndex, UINT NumVertices, UINT PrimitiveCount, CONST void * pIndexData, D3DFORMAT IndexDataFormat, CONST void* pVertexStreamZeroData, UINT VertexStreamZeroStride) = 0;
    virtual HRESULT DrawPrimitiveUP(D3DPRIMITIVETYPE PrimitiveType, UINT PrimitiveCount, CONST void* pVertexStreamZeroData, UINT VertexStreamZeroStride) = 0;
    virtual HRESULT SetScissorRect(const URect& ClipRect) = 0;
    virtual HRESULT CreateTextureFromFileInMemoryEx(LPCVOID pSrcData, UINT SrcDataSize, UINT Width, UINT Height, UINT MipLevels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, DWORD Filter, DWORD MipFilter, D3DCOLOR ColorKey, D3DXIMAGE_INFO * pSrcInfo, PALETTEENTRY * pPalette, LPDIRECT3DTEXTURE9 * ppTexture) = 0;
    virtual HRESULT CreateTexture(UINT Width, UINT Height, UINT Levels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DTexture9** ppTexture, HANDLE* pSharedHandle) = 0;
};

extern UIAPI IURenderInterface* g_pRenderInterface;

// should renamed as UPlugin.
class URender
{
public:
	
	
	virtual UFontPtr CreateFont(const char* faceName, int size, UINT uCharSet) = 0;
	virtual UTexturePtr LoadTexture(const char* texName, const char* pRelPath = NULL) = 0;
	virtual BOOL Initialize() = 0;
	virtual void Shutdown() = 0;
	// for batch setup render.
	virtual void BeginRender() = 0;
	virtual void EndRender() = 0;

	virtual void DrawRect(const URect& rect, UColor Color, float z = 0.0f) = 0;
	virtual void DrawRectFill(const URect& rect, UColor Color, float z = 0.0f) = 0;
	virtual void DrawPolygon(UPolygon polygon, const UColor& Color, float z = 0.0f) = 0;
	virtual void DrawLine(const UPoint& pt0, const UPoint& pt1, UColor Color, float z = 0.0f) =0 ;
	virtual void DrawImage(IUTexture* texture, const URect& ScreenRect, const URect& ImageRect,const UColor& Diffuse = LCOLOR_WHITE, int nBlend = 0, float z = 0.0f) = 0; 
 	virtual void DrawMaskImage(IUTexture *texture, const float* texcoords, IUTexture *alpha, const float* alphaTexcoords, const float* destRect, float z = 0.0f) = 0;
	virtual void DrawMaskImageD(IDirect3DBaseTexture9 *texture, const float* texcoords, IUTexture *alpha, const float* alphaTexcoords, const float* destRect, float z = 0.0f) = 0;
	virtual void DrawRotatedImage(float fAngle, IUTexture* texture, const URect& ScreenRect, const URect& ImageRect,const UColor& Diffuse = LCOLOR_WHITE, int nBlend = 0, float z = 0.0f) = 0; 
	virtual void DrawSkin(USkin* skin, int index, const URect& ScreenRect,const UColor& Diffuse = LCOLOR_WHITE,BOOL bGlow = FALSE, float z = 0.0f) = 0; 
	virtual void DrawSkin(USkin* skin, int index, const UPoint& pt, const UColor& Diffuse = LCOLOR_WHITE,BOOL bGlow = FALSE, float z = 0.0f) = 0; 
	
	virtual void DrawCoolDownMask(const URect& rect , float fAngle , const UColor& Color = LCOLOR_WHITE, float z = 0.0f) = 0;

	//virtual INT UDrawText( IUFont* Font, const UPoint& Pos, const char* szText, UColor* ColorRemap, UColor Color = LCOLOR_WHITE) = 0;
	virtual INT DrawTextN( IUFont* Font, const UPoint& Pos, const char* szText, INT Num, UColor* ColorRemap, const UColor& Color = LCOLOR_WHITE, float z = 0.0f, float scale = 1.0f) = 0;
	//virtual INT UDrawText( IUFont* Font, const UPoint& Pos, const WCHAR* szText, UColor* ColorRemap, UColor Color = LCOLOR_WHITE) = 0;
	virtual INT DrawTextN( IUFont* Font, const UPoint& Pos, const WCHAR* szText, INT Num, UColor* ColorRemap, const UColor& Color = LCOLOR_WHITE, float z = 0.0f, float scale = 1.0f) = 0;

	virtual void SetClipRect(const URect& ClipRect) = 0;
	virtual const URect& GetClipRect() = 0;

	// Helper Function
	// Draw bitmap

	inline void DrawImage(IUTexture* texture, const UPoint& point, const UColor& Diffuse = LCOLOR_WHITE,int nBlend = 0, float z = 0.0f)
	{
		URect uvrect( 0, 0, texture->GetWidth(), texture->GetHeight());
		URect screenrect(point.x, point.y, texture->GetWidth() + point.x, texture->GetHeight()+point.y );
		DrawImage(texture, screenrect, uvrect,Diffuse, nBlend, z);
	}

	inline void DrawImage(IUTexture* texture, const URect& ScreenRect,const UColor& Diffuse = LCOLOR_WHITE,int nBlend = 0, float z = 0.0f)
	{
		URect uvrect( 0, 0, texture->GetWidth(), texture->GetHeight() );
		DrawImage( texture, ScreenRect, uvrect,Diffuse, nBlend, z);
	}

	inline void DrawImage( IUTexture* texture, const UPoint& point, const URect& uvRect, const UColor& Diffuse = LCOLOR_WHITE,int nBlend = 0, float z = 0.0f)
	{
		URect Screen( point.x, point.y, uvRect.GetWidth() +point.x, uvRect.GetHeight() +point.y );
		DrawImage( texture, Screen, uvRect,Diffuse, nBlend, z);
	}
};
void UIAPI USetTextEdge(BOOL bEdge, UColor& EdgeColor = UColor(0,0,0,255));
void UIAPI UDrawText(URender* plugin, IUFont* pFont, const UPoint& offset, const UPoint& size, const UColor& color, const char* text, EUTextAlignment ta = UT_LEFT, float depth = 0.0f, float scale = 1.0f, bool bHori = true);
void UIAPI UDrawText(URender* plugin, IUFont* pFont, const UPoint& offset, const UPoint& size, const UColor& color, const WCHAR* text, EUTextAlignment ta = UT_LEFT, float depth = 0.0f, float scale = 1.0f, bool bHori = true);
void UIAPI UDrawTextEx(URender* plugin, IUFont* pFont, const UPoint& offset, const UPoint& size, const UColor& color_left,const UColor& color_right, const char* text_Left,const char *text_Right);
void UIAPI UDrawTextEx(URender* plugin, IUFont* pFont, const UPoint& offset, const UPoint& size, const UColor& color_left,const UColor& color_right, const WCHAR* text_Left,const WCHAR *text_Right);
int UIAPI UDrawStaticText(URender * plugin,IUFont * pFont,const UPoint &offset,const int& width,const UColor& color,UStringW &wstr, EUTextAlignment ta = UT_LEFT, UINT pCharoffset = 0, float depth = 0.0f);
int UIAPI UDrawStaticText(URender * plugin,IUFont * pFont,const UPoint &offset,const int& width,const UColor& color,const WCHAR* wstr, EUTextAlignment ta = UT_LEFT, UINT pCharoffset = 0, float depth = 0.0f);
URect UIAPI UDrawResizeBitmap(URender * plugin , USkinPtr skin, const UPoint & pos , const UPoint & size, float depth = 0.0f);
void UIAPI UDrawResizeImage(EUResizeImageType type,URender * plugin,USkin * skin , const UPoint & pos,const UPoint &size, float depth = 0.0f);