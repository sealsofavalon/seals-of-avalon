#ifndef __URICHEDIT_H__
#define __URICHEDIT_H__
#include "UControl.h"

// the ruler: 
/**
所有标签必须以 "<" 开始, 以">"结束.
1. 对齐方式
	<left>				左对齐							eg: <left>sth
	<center>			中间对齐
	<right>				右对齐

2. 图像
	<img:{relative image path}>		插入图像				eg: <img:images\logo.png>
	<fac:{relative image path}>     插入序列帧    目前序列帧的大小定义为 32 * 32 eg: <fac:images\logo.png>
	<skin:{relative Skin path};index> ??
3. 段落
	<br>				换行,等同于\n					eg: sth text<br>next line.


4. 文本


5. 超链接
	<a:url>tag</a>		链接						eg: <a:www.where.net>HomeSite</a>
	<a:#cmd>tag</a>		命令热区					eg: <a:#OnClickShowText>ShowText</a>

<font:{facename}:size [text] >  定义字体
<br>							换行

*/
#define  UFACEFRAMETIME 150

typedef bool (* ContentBitMapURLFun)(char* , std::string& );

class UIAPI URichTextCtrl 
	: public UControl
{
	UDEC_CLASS(URichTextCtrl);
public:
	URichTextCtrl();
	virtual ~URichTextCtrl();

	void SetBitMapURLFun(ContentBitMapURLFun Fun);  //慎用！ 这里是为了接口预留给带有URL的图片的出来。 主要是SKIN文件
	int GetTextLength() const;
	int GetText(int nStartCP, char* pDestBuf, int nMaxLen) const;
	int GetText(int nStartCP, WCHAR* pDestBuf, int nMaxLen) const;
	void SetText(const char* textBuffer, UINT numChars);
	void SetText(const WCHAR* textBuffer, UINT numChars);
	int InsertText(int nCharPos, const char* text, int nLen);
	int InsertText(int nCharPos, const WCHAR* text, int nLen);
	void AppendText(const char* text, UINT numChars, BOOL bReformat = FALSE);
	void AppendText(const WCHAR* text, UINT numChars, BOOL bReformat = FALSE);
	void DeleteText(int nStart, int nEnd);
	void ClearText();
	// 获得内容高度.
	int GetContentHeight() const ;
	int GetLineHeight() const;

	void setAlpha(float alpha) { mAlpha = alpha;}
	BOOL SetCaretCP(int nCharPos);
	void ScrollCaretVisible();

	void scrollToTag( UINT id );
	void scrollToTop();
	void scrollToBottom();

	void ClearSel();
	void SetSel(int nStart, int nEnd);
	BOOL GetSel(int* nStart, int* nEnd);

	int GetCaretPos()  {return m_nCaretCP; }
	int GetCharPos(const UPoint& ptCtrl);
	void FormatContent();

	void SetLineOffset(unsigned int uiPixel) { m_uiLineOffset = uiPixel; }

	void Copy();
	void Copy(int nStart, int nEnd);
	//  将rich format text 转换为flat format text.
	static int TrimRichFormat(const WCHAR* pInRFT, char* pOutFFT, int nMaxSize);

	IUTexture* FindTextureAtom(const char* Name) const;

	void ClearHitURL();
	virtual void SetVisible(BOOL value);
protected:
	void GetCaretInfo(UPoint &cursorTop, UPoint &cursorBottom, UColor &color);
	BOOL IsSelActive() const;
public:
	virtual BOOL HitTest(const UPoint& parentCoordPoint)
	{
		return UControl::HitTest(parentCoordPoint);
	}
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void OnSize(const UPoint& NewSize);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual void OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags);
	virtual void OnMouseDragged(const UPoint& pt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& pt, UINT uFlags);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags);
	virtual void OnRightMouseUp(const UPoint& position, UINT flags);
	virtual void OnClickedLinkTag(const char* pCmd);
	virtual void OnRightClickedLinkTag(const char *pCmd);
	virtual void UpDataFaceIndex(float time);
	virtual void OnTimer(float fDeltaTime);
protected:
	struct RichEdit* m_pRichEdit;
	UStringW	m_wstrBuffer;	// 字符内容(unicode)
	UString		m_strRtfFile;	// 文本数据定义文件. 如果存在, 则从此文件填充字符内容.
	BOOL m_bDirty;				// 内容是否已经发生改变	
	BOOL m_bDrag;
	int m_nCaretCP;				// 光标字符位置
	int	m_nMaxSize;				// 最大字符数目, 小于等于0为无限制.
	float mAlpha;
	BOOL m_bSelActive;
	UINT  m_nSelStart;
	UINT  m_nSelEnd;
	UINT  mVertMoveAnchor;
	BOOL	mVertMoveAnchorValid;
	INT     mSelectionAnchor;
	UPoint mSelectionAnchorDropped;
	// Font resource
	UFontPtr mFont;
	UINT	mMinSensibleWidth;
	// Console settable parameters
	UINT mLineSpacingPixels;
	BOOL  mAllowColorChars;
	unsigned int m_uiLineOffset;
	UINT m_FaceTime;

	BOOL m_bDrawBgk;
	BOOL m_ActiveOnMouseURL;
	BOOL m_ActiveBigMapURL;

	ContentBitMapURLFun m_pkBitMapURLFun;
		// Too many chars sound:
	// SFXProfile*  mDeniedSound;
};


class UIAPI URichEdit : public URichTextCtrl
{
	UDEC_CLASS(URichEdit);
public:
	URichEdit(void);
	~URichEdit(void);
	
	void Cut();
	void Paste();
protected:
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void OnSize(const UPoint& NewSize);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
};



#endif 