#ifndef __USCROLLBAR_H__
#define __USCROLLBAR_H__
#include "UControl.h"

//CScrollBar
enum EUScrollBarShowMode
{
	USBSM_FORCEON = 0,
	USBSM_FORCEOFF = 1,
	USBSM_DYNAMIC = 2,
};

class UIAPI UScrollBar : public UControl
{
	UDEC_CLASS(UScrollBar);
public:
	enum EUSBRegion
	{
		SBR_UNKNOWN = 0,
		SBR_UP,
		SBR_DOWN,
		SBR_LEFT,
		SBR_RIGHT,
		SBR_PGUP,
		SBR_PGDN,
		SBR_PGLEFT,
		SBR_PGRIGHT,
		SBR_VTHUMB,
		SBR_HTHUMB,
	};
	enum ESBSkin 
	{
		SBS_UP_ARROW_N = 0,
		SBS_UP_ARROW_H = 1,
		SBS_UP_ARROW_D = 2,

		SBS_DOWN_ARROW_N = 3,
		SBS_DOWN_ARROW_H = 4,
		SBS_DOWN_ARROW_D = 5,

		SBS_LEFT_ARROW_N = 6,
		SBS_LEFT_ARROW_H = 7,
		SBS_LEFT_ARROW_D = 8,

		SBS_RIGHT_ARROW_N = 9,
		SBS_RIGHT_ARROW_H = 10,
		SBS_RIGHT_ARROW_D = 11,

		SBS_VERT_THUMB_N = 12,
		SBS_VERT_THUMB_D = 13,
		SBS_HORI_THUMB_N = 14,
		SBS_HORI_THUMB_D = 15,

		SBS_VERT_BG = 16,
		SBS_HORI_BG = 17,

		SBS_VERT_THUMBTOPCAP_N = 18,		// 如果SKIN中没有定义 CAP(18 - 21) 则滚动条为固定长度. 
		SBS_VERT_THUMBBOTTOMCAP_D = 19,
		SBS_HORI_THUMBTOPCAP_N = 20,
		SBS_HORI_THUMBBOTTOMCAP_D = 21,
		SBS_MAX,
	};
	

	EUSBRegion HitTestScrollBar(const UPoint & point) const;
public:
	UScrollBar();
	virtual ~UScrollBar();

	void autoScroll(EUSBRegion reg);

	void scrollTo(int x, int y);
	void scrollDelta(int x, int y);
	void scrollRectVisible(const URect& rect);

	void SetBarShowMode(EUScrollBarShowMode eVertShowMode, EUScrollBarShowMode eHoriShowMode);
	void ReCalLayout();


	//	void addObject(SimObject *obj);
	void childResized(UControl *child);
	UPoint GetChildPos();
	UPoint GetChildRelPos();
	UPoint GetChildSize();
	UPoint GetContentSize();
	UPoint GetMargin() { return mChildMargin; } //  Added to aid in sizing calculations
	
	// 获得滚动条大小(pixel), 对于垂直滚动条, 获得的是宽度, 水平滚动条, 获得的是高度. 
	int GetBarSize() const;
	int getBorderThickness(void) { return mBorderThickness; }

	BOOL HasHBar() const;
	BOOL HasVBar() const;
	BOOL IsHBarEnable() const;
	BOOL IsVBarEnable() const;

	BOOL wantsTabListMembership();
	BOOL becomeFirstResponder();
	BOOL loseFirstResponder();
	void DispatchChildNotify(int mess);

	EUSBRegion getCurHitRegion(void) { return m_eCurActiveRegion; }
protected:
	// TODO 可能会移到UControl中, 让UControl响应 OnGetScrollInfo 来获取消息.
	virtual BOOL CalcChildSize(UPoint& Pos, UPoint& Size);
	void UpdateThumb();
	void scrollByRegion(EUSBRegion reg);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnSize(const UPoint& NewSize);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual void OnMouseDown(const UPoint& pt, UINT nRepCnt, UINT uFlags);
//	virtual void onMouseRepeat(const GuiEvent &event);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);
	virtual BOOL OnMouseWheel(const UPoint& position, int delta , UINT flags);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void drawBorder(const UPoint &offset, BOOL isFirstResponder);
	virtual void drawVScrollBar(const UPoint &offset);
	virtual void drawHScrollBar(const UPoint &offset);
	virtual void drawScrollCorner(const UPoint &offset);
	virtual UControl* FindHitWindow(const UPoint &pt, int initialLayer = -1);
protected:
	BOOL m_bBarDepressed;				// 是否点击
	EUSBRegion m_eCurActiveRegion;		// 当前活动区域
	BOOL mScrollDisabled; ///< Are the Scrollbars disabled
	int mForceHScrollBar; ///< Force showing the Horizontal scrollbar
	int mForceVScrollBar; ///< Force showing the Vertical scrollbar

	BOOL mWillFirstRespond;     // for automatically handling arrow keys

	int mBorderThickness;           // this gets set per class in the constructor
	UPoint mChildMargin;               // the thickeness of the margin around the child controls

	bool mbShowScrollBkg;
	BOOL mbAllShowScrollBar ;	//BAR 是否一直显示，还是只有窗口需要BAR的时候， 才显示
	int mScrollBarDragTolerance;    // maximal distance from scrollbar at which a scrollbar drag is still valid

	struct ScrollContext* m_pScrollCtx;
};

#endif 