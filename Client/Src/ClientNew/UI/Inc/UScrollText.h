#ifndef __USCROLLTEXT_H__
#define __USCROLLTEXT_H__
#include "UControl.h"
class UIAPI UScrollText : public UControl
{
	UDEC_CLASS(UScrollText);
public:
	UScrollText();
	~UScrollText();
public:
	void SetScrollSpeed(UINT Time);
	void AddScrollText(UINT ID, const char * text , UColor color = LCOLOR_WHITE);
	void AddScrollText(UINT ID, const WCHAR * text , UColor color = LCOLOR_WHITE);
	void RemoveScrollText(const char * text);
	void RemoveScrollText(const WCHAR * text);
	void RemoveScrollText(UINT ID);
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnSize(const UPoint& NewSize);
	virtual void OnTimer(float fDeltaTime);
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
protected:
	class ScrollText * mScrollText;
};
#endif 