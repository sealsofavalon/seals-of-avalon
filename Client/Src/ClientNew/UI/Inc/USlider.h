#ifndef __USLIDER_H__
#define __USLIDER_H__
#include "UControl.h"
/////////////////////////////////////////////////////////////////////
//Slider Control
//目前只支持横向 纵向的没有加入
/////////////////////////////////////////////////////////////////////
class UIAPI USlider : public UControl
{
	enum
	{
		SLIDERBUTTON_NORMAL,
		SLIDERBUTTON_HOVER,
		SLIDERBUTTON_PRESS,
		SLIDERBUTTON_INACTIVE,
	};
	enum
	{
		SLIDERBAR_NORMAL,
		SLIDERBAR_INACTIVE,
	};
	UDEC_CLASS(USlider);
public:
	USlider();
	~USlider();
public:
	void SetValue( int nValue ) { SetValueInternal( nValue, false ); }
	int  GetValue() const { return m_nValue; };
	float GetFValue() const {return float(m_nValue) / float(m_nMax);}

	void GetRange( int &nMin, int &nMax ) const { nMin = m_nMin; nMax = m_nMax; }
	void SetRange( int nMin, int nMax );

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnChange(bool bResized);

	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void UpdateRects(); 

	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseMove(const UPoint& position, UINT flags);
	virtual void OnMouseLeave(const UPoint& position, UINT flags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);
	virtual BOOL HitTest(const UPoint& parentCoordPoint);
protected:
	void SetValueInternal( int nValue, bool bFromInput );
	int  ValueFromPos( int x ); 

	int m_nValue;

	int m_nMax;
	int m_nMin;

	int m_nDragX;      // Mouse position at start of drag
	int m_nDragOffset; // Drag offset from the center of the button
	int m_nButtonX;

	bool m_bPressed;
	URect m_rcButton;
	int m_rcButtonState;

	UString m_ButtonSkinName;
	UString m_BarSkinName;

	USkinPtr m_ButtonSkin;
	USkinPtr m_BarSkin;
};
#endif