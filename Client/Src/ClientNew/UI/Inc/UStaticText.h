#ifndef __USTATICTEXT_H__
#define __USTATICTEXT_H__
#include "UControl.h"
//静态文本框:单项交互控件不允许输入只是用来显示文本
class UIAPI UStaticText : public UControl
{
	UDEC_CLASS(UStaticText)
public:
	UStaticText();
	virtual ~UStaticText();
	virtual void SetText(const char * str);
	virtual void SetText(const WCHAR * wstr);
	virtual void Clear();
	virtual int GetText(char *dest,int nMaxLen,int npos = 0);
	virtual int GetText(WCHAR *dest,int nMaxLen,int npos = 0);
	const UStringW& GetString() const {return m_WStrBuffer;};
	virtual void SetTextColor(UColor &color);
	virtual void SetTextColor(const char *color);
	virtual void SetTextAlign(int align);
	virtual void SetTextEdge(bool bedge, UColor& color = UColor(0,0,0,255));
	virtual void SetTextShadow(bool bShadow){ m_bShadow = bShadow;};
protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnTimer(float fDeltaTime);
	virtual BOOL HitTest(const UPoint& parentCoordPoint)
	{
		return FALSE;
	};//静态文本框显示数据并不妨碍其他控件的响应 所以静态文本框总是返回非真值
protected:
	UStringW m_WStrBuffer;
	UFontPtr m_Font;
	UColor   m_FontColor;
	int      m_TextAlign;
	bool m_bFontBoard;
	UColor m_FontEdgeColor;
	bool m_bShadow;
};


#endif 
