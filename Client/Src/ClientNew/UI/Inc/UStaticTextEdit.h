#ifndef __USTATICTEXTEDIT_H__
#define __USTATICTEXTEDIT_H__
#include "UControl.h"



class UIAPI UStaticTextEdit : public  UControl
{
	UDEC_CLASS(UStaticTextEdit);
public:
	UStaticTextEdit();
	~UStaticTextEdit();
public:
	virtual int GetText(char* dest, int nMaxLen, int nPos = 0);
	virtual int GetText(WCHAR* dest, int nMaxLen, int nPos = 0);
	virtual void SetText(const char* txt);
	virtual void SetText(const WCHAR* txt);
	void OnSetCaretPos();
	void SetTextMaxLen(UINT len){m_nMaxLength = len;}

protected:
	virtual BOOL OnCreate();
	virtual void OnDestroy();
	virtual void OnRender(const UPoint& offset, const URect &updateRect);
	virtual void DrawContent(UPoint offset);
	virtual BOOL OnChar(UINT nCharCode, UINT nRepCnt, UINT nFlags);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual BOOL OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	virtual void OnSize(const UPoint& NewSize);
	virtual void OnGetFocus();
	virtual void OnLostFocus();
protected:
	 struct StaticText* m_pTextContent;
private:
	 UStringW m_wstrBuffer; // 
	 INT  m_nMaxLength;
	 UINT m_nCaretPos;
};

#endif