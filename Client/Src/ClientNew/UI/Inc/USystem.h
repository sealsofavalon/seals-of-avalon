#ifndef __USYSTEM_H__
#define __USYSTEM_H__
#include "UBase.h"
#include "UControl.h"
class USystemImp;
class UControl;


// TODO : 考虑下, 将USystem 中的接口写到UControl 中?

//  USystemImp wrapper , for a clear interface.
class UIAPI USystem
{
public:
	static USystem* Create();
	void SetSoundInterface(IUSoundInterface* iSI);
	// temp interface.
	void SetDevice(void* pd3ddevice);

	BOOL Initialize(HWND hWnd);
	BOOL EnableIME(BOOL Enable = TRUE);
	void SetIMEEnable(BOOL Enable = TRUE);
	BOOL IsIMEEable();
	void IniIMEWnd(HWND hWnd);
	void Shutdown();
	void Release();
	void SetDragCursorSize(const UPoint& CursorSize);

	void Update(float fDeltaTime);
	void Draw();
	
	void Resize(int x, int y);
	void ReszieWindow(int x, int y);

	BOOL MouseMove(int x, int y);
	BOOL MouseBtnDown(UINT Btn);
	BOOL MouseBtnUp(UINT Btn);
	BOOL MouseWheel(int Delta);
	BOOL KeyDown(UINT Key);
	BOOL KeyUp(UINT Key);
	BOOL KeyPress(UINT Key);

	inline BOOL IsDraging() const;

	// 在UI 系统的内部, 针对字符的处理是以UNICODE 为标准的,因此
	// 对于WM_CHAR 的处理, 必须要严格区分. 如果窗口类, 窗口, 窗口过程 为ANSI构造的.
	// 那么必须调用 ANIS 版本的处理流程, 反之调用 UTF16 的. 
	// 同样, 建议构造UNICODE 窗口, 这样UI底层处理起来也会快得多, 也会更加安全. 
	void KeyMessageA(UINT nMsg, UINT wParam, UINT lParam);
	void KeyMessageW(UINT nMsg, UINT wParam, UINT lParam);

	BOOL WindowKillFouce();
	BOOL ProcessIMEMessage(UINT nMsg, UINT wParam, UINT lParam);
	// 装载控件样式, 在创建控件前,必须确保其相应的样式已经加载. 
	// (参考 WIN32: CreateWindow 前必须先 RegisterClass)
	BOOL LoadControlStyle(const char* StyleFileName);
	UControlStyle* FindControlStyle(const char* StyleName);
	//BOOL RegisterControlStyle(const char* );

	BOOL LoadAcceleratorKey(const char* keyFileName, bool bCover);
	BOOL SaveAcceleratorKey(const char* keyFileName);
	void UnLoadAccelertorKey();
	UAccelarKey * FindAcceleratorKey(const char * AcceleratorKeyName);
	BOOL SetAcceleratorKey(const char* AcceleratorKeyName, UINT nKeycode, UINT nFlag);

	BOOL LoadColorTable(const char* ColorFileName);
	UColorTable* FindColor(const char * ColorName);

	USkinPtr LoadSkin(const char* filename);
	void DestorySkin(USkin* pSkin);
	// Create a dialog from template file.
	UControl* CreateDialogFromFile(const char* TemplateName);
	UControl* CreateDialogFromMemory(const char* pTemplate, int nSize);
	
	void DestoryControl(UControl* pControl);

	BOOL SetFrameControl(UControl* pFrameCtrl);		// Frame Control 类似于MFC FrameWnd 的行为, 成为当前桌面的活动框架,并替换原来的框架.
	UControl* GetCurFrame();
	void PushDialog(UControl* pDialog);
	void PopDialog(UControl* pDialog);
	const UPoint& GetCursorPos() const;
	UControl* GetCapturedCtrl();
	void CaptureControl(UControl* pTargetCtrl);
	void ReleaseCapture(UControl* pCapturedCtrl);
	UControl* GetFocus();

	const char* GetRootPath() const;
	void SetRootPath();
	URender* GetPlugin();
	
	void __PlaySound(const char* pszSnd);
	void __PlaySound(int Index);
	void SetMouseControl(UControl* pWnd);
	BOOL InitSoftKeyBoardControl();
private:
	USystem(void);
	~USystem(void);
	USystemImp* m_pSystem;


	BOOL m_IsIMEEable;

};

#endif 