#include "StdAfx.h"
#include "d3duirender.h"

// temp interface.
#include "USystem.h"
#include "UControl.h"
#include "USystemImp.h"

#include "UFont.h"

#define TEXT_MAG 1

UIAPI IURenderInterface* g_pRenderInterface = NULL;

void USystem::SetDevice(void* pd3ddevice)
{
	if (UControl::sm_UiRender == NULL)
	{
		IDirect3DDevice9* Device = (IDirect3DDevice9*)pd3ddevice;
		UControl::sm_UiRender = new D3DUIRender(Device);
	}
}


//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
D3D9Font::D3D9Font()
{
	m_pImpFont = NULL;
	m_FontSize = 0;
}
D3D9Font::~D3D9Font()
{
	Destroy();
}

BOOL D3D9Font::Create(const char* FaceName, int size, UINT charset)
{
	Destroy();
	m_pImpFont = UFont::CreateFont(FaceName, size, charset);
	m_FaceName = FaceName;
	m_FontSize = size;
	return m_pImpFont != NULL;
}

void D3D9Font::Destroy()
{
	if (m_pImpFont)
	{
		D3DUIRender* pRender = (D3DUIRender*)UControl::sm_UiRender;
		assert(pRender);
		pRender->DestroyFont(m_pImpFont->GetFontFileName());
		delete m_pImpFont;
		m_pImpFont = NULL;
	}
}

void D3D9Font::SetFileName(const std::string& FileName)
{
	if (m_pImpFont)
	{
		m_pImpFont->SetFontFileName(FileName.c_str());
	}
}

const CharInfo* D3D9Font::GetCharDesc(WCHAR wc)
{
	assert(m_pImpFont);
	return &m_pImpFont->GetCharDesc(wc);
}

UINT D3D9Font::GetBaseLine()
{
	assert(m_pImpFont);
	return m_pImpFont->GetBaseline();
}
IDirect3DTexture9* D3D9Font::GetTexture(int page)
{
	assert(m_pImpFont);
	return  (IDirect3DTexture9*)m_pImpFont->GetSurface(page);
}

INT D3D9Font::GetStrWidth(const char* Str)
{
	//assert(m_pImpFont);
	//return m_pImpFont->GetStrWidth(Str);
	assert(0);
	return 0;
}

INT D3D9Font::GetStrWidth(const WCHAR* wStr)
{
	assert(m_pImpFont);
	return m_pImpFont->GetStrNWidth(wStr, wcslen(wStr));
}

UINT D3D9Font::GetStrNWidthPrecise(const WCHAR* wstr, UINT n)
{
	assert(m_pImpFont);
	return m_pImpFont->GetStrNWidthPrecise(wstr, n);
}

INT D3D9Font::GetCharWidth(WCHAR wChar)
{
	assert(m_pImpFont);
	return m_pImpFont->GetCharWidth(wChar);
}

INT D3D9Font::GetStrNWidth(const WCHAR* Str, int Num)
{
	assert(m_pImpFont);
	return m_pImpFont->GetStrNWidth(Str, Num);
}

INT D3D9Font::GetHeight()
{
	assert(m_pImpFont);
	return m_pImpFont->GetHeight();
}

UINT D3D9Font::GetBaseline() const
{
	assert(m_pImpFont);
	return m_pImpFont->GetBaseline();
}

UINT D3D9Font::GetDescent() const
{
	assert(m_pImpFont);
	return m_pImpFont->GetDescent();
}

INT D3D9Font::GetCharXIncrement(WCHAR wChar)
{
	assert(m_pImpFont);
	return m_pImpFont->GetCharXIncrement(wChar);
}

BOOL D3D9Font::IsValidChar(WCHAR wChar)
{
	assert(m_pImpFont);
	return m_pImpFont->IsValidChar(wChar);
}

UINT D3D9Font::GetBreakPos(const WCHAR *string, UINT strlen, UINT width, BOOL breakOnWhitespace)
{
	assert(m_pImpFont);
	return m_pImpFont->GetBreakPos(string, strlen, width, breakOnWhitespace);
}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
D3D9Texture::D3D9Texture()
{
	m_pTexture = NULL;
}

D3D9Texture::~D3D9Texture()
{
	if (m_pTexture)
	{
		m_pTexture->Release();
	}
}
void D3D9Texture::Release()
{
	if (--m_RefCounter == 0)
	{
		D3DUIRender* pRender = (D3DUIRender*)UControl::sm_UiRender;
		assert(pRender);
		pRender->DestroyTexture(this);
	}
}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////


D3DUIRender::FontRenderBatcher::FontRenderBatcher()
{
	mSheets.resize(4);
	mSheets.reserve(4);
	m_PageOfs.resize(4);
	m_z = 0.0f;
	m_Scale = 1.0f;

	m_MemPool.resize( 1024000 );
	BYTE* pPoolHead = &m_MemPool[0];
	// 增量地设置大小后, Page所指向的地址已经无效.
	for (int i = 0; i < (int)mSheets.size(); i++)
	{
		if (mSheets[i])
		{
			mSheets[i] = (Page*)(pPoolHead + m_PageOfs[i]);
		}
	}
}


void D3DUIRender::FontRenderBatcher::End(const UPoint& offset)
{
	if( mLength == 0 )
		return;

	g_pRenderInterface->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	g_pRenderInterface->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);

	g_pRenderInterface->SetTextureStageState(0,D3DTSS_COLORARG1,D3DTA_TEXTURE);
	g_pRenderInterface->SetTextureStageState(0,D3DTSS_COLORARG2,D3DTA_CURRENT);
	g_pRenderInterface->SetTextureStageState(0,D3DTSS_COLOROP, D3DTOP_ADD);

	g_pRenderInterface->SetTextureStageState(0,D3DTSS_ALPHAARG1,D3DTA_TEXTURE);
	g_pRenderInterface->SetTextureStageState(0,D3DTSS_ALPHAARG2,D3DTA_CURRENT);
	g_pRenderInterface->SetTextureStageState(0,D3DTSS_ALPHAOP, D3DTOP_MODULATE);

	g_pRenderInterface->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	g_pRenderInterface->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	g_pRenderInterface->SetSamplerState(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	g_pRenderInterface->SetSamplerState(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

	//g_pRenderInterface->SetRenderState( D3DRS_MULTISAMPLEANTIALIAS, false );
	
	float U0,U1,V0,V1,X0,X1,Y0,Y1;
	float XPos,YPos;
	static float fInvWidth = 1.0f/(float)UFont::TextureSheetSize;
	for( int i = 0; i < mSheets.size(); i++ )
	{
		// Do some early outs...
		if(!mSheets[i])
			continue;

		if(!mSheets[i]->numChars)
			continue;

		int currentPt = 0;
		mSheets[i]->startVertex = currentPt;
		IDirect3DTexture9 *tex = mFont->GetTexture(i);

		D3DUIRender::UVertex * pVertics = (D3DUIRender::UVertex*)Alloc(sizeof(D3DUIRender::UVertex)* (mSheets[i]->numChars) *6);
		assert(pVertics);

		for( int j = 0; j < mSheets[i]->numChars; j++ )
		{
			// Get some general info to proceed with...
			const CharMarker &m = mSheets[i]->charIndex[j];
			const CharInfo* pci = mFont->GetCharDesc( m.c );
			assert(pci);

			// Where are we drawing it?
			YPos = (float)offset.y + (float)( mFont->GetBaseLine() - pci->yOrigin * TEXT_MAG ) * m_Scale;
			XPos = (float)offset.x + (float)( m.x + pci->xOrigin ) * m_Scale;
			
			U0 = (float)(pci->xOffset + 0.5f) * fInvWidth;
			U1 = (float)(pci->xOffset + pci->width + 0.5f) * fInvWidth;
			V0 = (float)(pci->yOffset + 0.5f) * fInvWidth;
			V1 = (float)(pci->yOffset + pci->height + 0.5f) * fInvWidth;

			X0 = XPos;
			X1 = XPos + (float)pci->width * m_Scale;
			Y0 = YPos;
			Y1 = YPos + (float)pci->height * m_Scale;

			/*
			   0 --- 1
			   |     |	
               |     |
			   2 ----3
			*/

			pVertics[currentPt].SetPos(X0, Y0, m_z);
			pVertics[currentPt].SetUV(U0,V0);
			pVertics[currentPt++].SetColor(m.color);

			pVertics[currentPt].SetPos(X1, Y0, m_z);
			pVertics[currentPt].SetUV(U1,V0);
			pVertics[currentPt++].SetColor(m.color);

			pVertics[currentPt].SetPos(X0, Y1, m_z);
			pVertics[currentPt].SetUV(U0,V1);
			pVertics[currentPt++].SetColor(m.color);


			pVertics[currentPt].SetPos(X0, Y1, m_z);
			pVertics[currentPt].SetUV(U0,V1);
			pVertics[currentPt++].SetColor(m.color);

			pVertics[currentPt].SetPos(X1, Y0, m_z);
			pVertics[currentPt].SetUV(U1,V0);
			pVertics[currentPt++].SetColor(m.color);

			pVertics[currentPt].SetPos(X1, Y1, m_z);
			pVertics[currentPt].SetUV(U1,V1);
			pVertics[currentPt++].SetColor(m.color);
		}

		assert(currentPt <= mLength *6);
		g_pRenderInterface->SetTexture(0, mFont->GetTexture(i));
		g_pRenderInterface->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 
			mSheets[i]->numChars*2,
			pVertics,
			sizeof(D3DUIRender::UVertex));

	}

	g_pRenderInterface->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	g_pRenderInterface->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	g_pRenderInterface->SetTextureStageState(0,D3DTSS_COLOROP, D3DTOP_MODULATE);
	//g_pRenderInterface->SetRenderState( D3DRS_MULTISAMPLEANTIALIAS, true );
}

void D3DUIRender::FontRenderBatcher::BatchCharacter( WCHAR c, int &currentX,const UColor &currentColor )
{
	const CharInfo* pci = mFont->GetCharDesc( c );
	assert(pci);
	int sidx = pci->bitmapIndex;

	if( pci->width != 0 && pci->height != 0 )
	{
		Page &sm = GetPage(sidx);

		CharMarker &m = sm.charIndex[sm.numChars++];
	
		m.c = c;
		m.x = currentX;
		m.color = currentColor;
	}

	currentX += pci->xIncrement;
}

D3DUIRender::FontRenderBatcher::Page& D3DUIRender::FontRenderBatcher::GetPage( int sheetID )
{
	// Allocate if it doesn't exist...
	if(mSheets.size() <= sheetID || !mSheets[sheetID])
	{
		if(sheetID >= mSheets.size())
		{
			mSheets.resize(sheetID+1);
			m_PageOfs.resize(sheetID +1);
		}	
		size_t ofs = m_PoolPos;
		mSheets[sheetID] = (Page*)Alloc(sizeof(Page) + mLength * sizeof( CharMarker ) );
		mSheets[sheetID]->numChars = 0;
		m_PageOfs[sheetID] = ofs;
	}

	return *mSheets[sheetID];
}

void D3DUIRender::FontRenderBatcher::Begin( D3D9Font *font, int n , float z, float scale)
{
	// Null everything and reset our count.
	memset(&mSheets[0], 0, sizeof(Page*)*mSheets.size());
	m_z = z;
	mFont = font;
	mLength = n;
	m_PoolPos = 0;
	m_Scale = scale;
}


//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////




D3DUIRender::D3DUIRender(IDirect3DDevice9* pDev)
{
	m_bBegined = FALSE;
}

D3DUIRender::~D3DUIRender(void)
{
}
BOOL D3DUIRender::Initialize()
{
	g_pFrameAllocator = new FrameAllocator;
	if(g_pFrameAllocator == NULL)
		return FALSE;
	return TRUE;
}

void D3DUIRender::Shutdown()
{
	TextureMap::iterator it = m_mapTextures.begin();
	for ( ; it != m_mapTextures.end(); ++it)
	{
		D3D9Texture* pTexture = it->second;
	}

	if (g_pFrameAllocator)
	{
		delete g_pFrameAllocator;
		g_pFrameAllocator = NULL;
	}
	delete this;
}

void D3DUIRender::BeginRender()
{
	if (m_bBegined)
	{
		return;
	}
	g_pRenderInterface->SetFVF(UVERTEX_FVF);
	g_pRenderInterface->SetVertexShader(NULL);
	g_pRenderInterface->SetPixelShader(NULL);
	
	g_pRenderInterface->SetRenderState(D3DRS_ZENABLE, FALSE);
	g_pRenderInterface->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	g_pRenderInterface->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	g_pRenderInterface->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	g_pRenderInterface->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	//// setup texture addressing settings
	g_pRenderInterface->SetSamplerState( 0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	g_pRenderInterface->SetSamplerState( 0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

	// setup colour calculations
	g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
	g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);

	// setup alpha calculations
	g_pRenderInterface->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	g_pRenderInterface->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
	g_pRenderInterface->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);

	// setup filtering
	g_pRenderInterface->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	g_pRenderInterface->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);

	// disable texture stages we do not need.
	g_pRenderInterface->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);

	//// setup scene alpha blending
	g_pRenderInterface->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

    g_pRenderInterface->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	g_pRenderInterface->SetRenderState( D3DRS_MULTISAMPLEANTIALIAS, false );

	m_bBegined = TRUE; 
}

void D3DUIRender::EndRender()
{
	if (!m_bBegined)
	{
		return;
	}
    g_pRenderInterface->SetRenderState(D3DRS_ZENABLE, TRUE);
	g_pRenderInterface->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	g_pRenderInterface->SetSamplerState( 0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	g_pRenderInterface->SetSamplerState( 0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	m_bBegined = FALSE;

	g_pRenderInterface->SetRenderState( D3DRS_MULTISAMPLEANTIALIAS, true );
}
void D3DUIRender::DrawRect(const URect& rect, UColor Color, float z)
{
	SetupRectForLine(rect,Color,z);
	g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_DISABLE);

	/*
	   0 ------ 1
	   |		|
	   |		|
	   2------- 3
	*/
	static WORD Indics[5] = 
	{
		0,1,3,2,0
	};
	g_pRenderInterface->DrawIndexedPrimitiveUP(D3DPT_LINESTRIP, 0, 4, 4, &Indics, 
		D3DFMT_INDEX16,
		&m_RectVertics,
		sizeof(UVertex));
}

void D3DUIRender::DrawPolygon(UPolygon polygon, const UColor& Color, float z )
{
	if (polygon.m_PointNum > 1)
	{
		for (int i = 0 ; i < polygon.m_PointNum - 1 ; ++i)
		{
			UPoint pt0 = polygon.Point[i];
			UPoint pt1 = polygon.Point[i + 1];
			SetupLine(pt0, pt1, Color);
			g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_DISABLE);
			g_pRenderInterface->DrawPrimitiveUP(
				D3DPT_LINELIST,
				1,
				m_RectVertics,
				sizeof(UVertex)
				);
		}
	}
}

void D3DUIRender::DrawRectFill(const URect& rect, UColor Color, float z)
{
	assert(g_pRenderInterface);
	SetupRect(rect,Color,z);
	g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_DISABLE);
	g_pRenderInterface->DrawPrimitiveUP(
		D3DPT_TRIANGLESTRIP,
		2,
		m_RectVertics,
		sizeof(UVertex)
		);

	g_pRenderInterface->SetTextureStageState(0,D3DTSS_COLOROP, D3DTOP_MODULATE);
}

void D3DUIRender::DrawLine(const UPoint& pt0, const UPoint& pt1, UColor Color, float z)
{
	SetupLine(pt0, pt1, Color);
	g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_DISABLE);
	g_pRenderInterface->DrawPrimitiveUP(
		D3DPT_LINELIST,
		1,
		m_RectVertics,
		sizeof(UVertex)
		);
}

void D3DUIRender::DrawImage(IUTexture *texture, const URect& dstRect, const URect& srcRect, const UColor& Diffuse,int nBlend, float z)
{
	if(!texture)
      return;

	D3D9Texture* pTexture = (D3D9Texture*)texture;

#ifdef HW_SUPPORT_NONPOWOF2
   SetupRect(dstRect, srcRect,pTexture->Width, pTexture->Height, Diffuse,z);
#else 
	SetupRect(dstRect, srcRect,pTexture->RelWidth, pTexture->RelHeight, Diffuse);
#endif 
	if (nBlend == 0)
	{
		g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	}else if (nBlend == 1)
	{
		g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}else if (nBlend == 2)
	{
		g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
		g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	}

	g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	g_pRenderInterface->SetTexture(0, pTexture->m_pTexture);
	g_pRenderInterface->DrawPrimitiveUP(
		D3DPT_TRIANGLESTRIP,
		2,
		m_RectVertics,
		sizeof(UVertex)
		);
	g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
}

struct MaskVertex
{
	float pos[4]; //vertex
	float tex0[2]; //tex0
	float tex1[2]; //tex1

	enum
	{
		FVF = D3DFVF_XYZRHW|D3DFVF_TEX2,
	};
};
void D3DUIRender::DrawMaskImageD(IDirect3DBaseTexture9 *texture, const float* texcoords, IUTexture *alpha, const float* alphaTexcoords, const float* destRect, float z )
{
	MaskVertex rectVertex[4];
	memset(rectVertex, 0, sizeof(rectVertex));

	float left = destRect[0];
	float top = destRect[1];
	float right = destRect[2];
	float bottom = destRect[3];

	float leftTex0 = texcoords[0];
	float topTex0 = texcoords[1];
	float rightTex0 = texcoords[2];
	float bottomTex0 = texcoords[3];

	float leftTex1 = alphaTexcoords[0];
	float topTex1 = alphaTexcoords[1];
	float rightTex1 = alphaTexcoords[2];
	float bottomTex1 = alphaTexcoords[3];

	rectVertex[0].pos[0] = left;
	rectVertex[0].pos[1] = top;
	rectVertex[0].pos[2] = 0.f;
	rectVertex[0].pos[3] = 1.f;
	rectVertex[0].tex0[0] = leftTex0;
	rectVertex[0].tex0[1] = topTex0;
	rectVertex[0].tex1[0] = leftTex1;
	rectVertex[0].tex1[1] = topTex1;

	rectVertex[1].pos[0] = right;
	rectVertex[1].pos[1] = top;
	rectVertex[1].pos[2] = 0.f;
	rectVertex[1].pos[3] = 1.f;
	rectVertex[1].tex0[0] = rightTex0;
	rectVertex[1].tex0[1] = topTex0;
	rectVertex[1].tex1[0] = rightTex1;
	rectVertex[1].tex1[1] = topTex1;

	rectVertex[2].pos[0] = left;
	rectVertex[2].pos[1] = bottom;
	rectVertex[2].pos[2] = 0.f;
	rectVertex[2].pos[3] = 1.f;
	rectVertex[2].tex0[0] = leftTex0;
	rectVertex[2].tex0[1] = bottomTex0;
	rectVertex[2].tex1[0] = leftTex1;
	rectVertex[2].tex1[1] = bottomTex1;

	rectVertex[3].pos[0] = right;
	rectVertex[3].pos[1] = bottom;
	rectVertex[3].pos[2] = 0.f;
	rectVertex[3].pos[3] = 1.f;
	rectVertex[3].tex0[0] = rightTex0;
	rectVertex[3].tex0[1] = bottomTex0;
	rectVertex[3].tex1[0] = rightTex1;
	rectVertex[3].tex1[1] = bottomTex1;

	g_pRenderInterface->SetFVF(MaskVertex::FVF);
	g_pRenderInterface->SetTexture(0, texture);
	g_pRenderInterface->SetTexture(1, ((D3D9Texture*)alpha)->m_pTexture);

	g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	
	g_pRenderInterface->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);
	g_pRenderInterface->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 1);

	//g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	//g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);

	//g_pRenderInterface->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	//g_pRenderInterface->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	
	//////////////////////////////////////////////////////////////////////////
	g_pRenderInterface->SetTextureStageState(1, D3DTSS_COLOROP,	D3DTOP_SELECTARG2);
	g_pRenderInterface->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_CURRENT);

	g_pRenderInterface->SetTextureStageState(1, D3DTSS_ALPHAOP,	D3DTOP_SELECTARG1);
	g_pRenderInterface->SetTextureStageState(1, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);


	g_pRenderInterface->DrawPrimitiveUP(
		D3DPT_TRIANGLESTRIP,
		2,
		rectVertex,
		sizeof(MaskVertex));

	g_pRenderInterface->SetFVF(UVERTEX_FVF);
	g_pRenderInterface->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
	g_pRenderInterface->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
}

void D3DUIRender::DrawMaskImage(IUTexture *texture, const float* texcoords, IUTexture *alpha, const float* alphaTexcoords, const float* destRect, float z)
{
	MaskVertex rectVertex[4];
	memset(rectVertex, 0, sizeof(rectVertex));

	float left = destRect[0];
	float top = destRect[1];
	float right = destRect[2];
	float bottom = destRect[3];

	float leftTex0 = texcoords[0];
	float topTex0 = texcoords[1];
	float rightTex0 = texcoords[2];
	float bottomTex0 = texcoords[3];

	float leftTex1 = alphaTexcoords[0];
	float topTex1 = alphaTexcoords[1];
	float rightTex1 = alphaTexcoords[2];
	float bottomTex1 = alphaTexcoords[3];

	rectVertex[0].pos[0] = left;
	rectVertex[0].pos[1] = top;
	rectVertex[0].pos[2] = 0.f;
	rectVertex[0].pos[3] = 1.f;
	rectVertex[0].tex0[0] = leftTex0;
	rectVertex[0].tex0[1] = topTex0;
	rectVertex[0].tex1[0] = leftTex1;
	rectVertex[0].tex1[1] = topTex1;

	rectVertex[1].pos[0] = right;
	rectVertex[1].pos[1] = top;
	rectVertex[1].pos[2] = 0.f;
	rectVertex[1].pos[3] = 1.f;
	rectVertex[1].tex0[0] = rightTex0;
	rectVertex[1].tex0[1] = topTex0;
	rectVertex[1].tex1[0] = rightTex1;
	rectVertex[1].tex1[1] = topTex1;

	rectVertex[2].pos[0] = left;
	rectVertex[2].pos[1] = bottom;
	rectVertex[2].pos[2] = 0.f;
	rectVertex[2].pos[3] = 1.f;
	rectVertex[2].tex0[0] = leftTex0;
	rectVertex[2].tex0[1] = bottomTex0;
	rectVertex[2].tex1[0] = leftTex1;
	rectVertex[2].tex1[1] = bottomTex1;

	rectVertex[3].pos[0] = right;
	rectVertex[3].pos[1] = bottom;
	rectVertex[3].pos[2] = 0.f;
	rectVertex[3].pos[3] = 1.f;
	rectVertex[3].tex0[0] = rightTex0;
	rectVertex[3].tex0[1] = bottomTex0;
	rectVertex[3].tex1[0] = rightTex1;
	rectVertex[3].tex1[1] = bottomTex1;

	g_pRenderInterface->SetFVF(MaskVertex::FVF);
	g_pRenderInterface->SetTexture(0, ((D3D9Texture*)texture)->m_pTexture);
	g_pRenderInterface->SetTexture(1, ((D3D9Texture*)alpha)->m_pTexture);

	g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	g_pRenderInterface->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_SELECTARG2);
	g_pRenderInterface->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_CURRENT);

	g_pRenderInterface->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	g_pRenderInterface->SetTextureStageState(1, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);

	g_pRenderInterface->DrawPrimitiveUP(
		D3DPT_TRIANGLESTRIP,
		2,
		rectVertex,
		sizeof(MaskVertex));

	g_pRenderInterface->SetFVF(UVERTEX_FVF);
	g_pRenderInterface->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
	g_pRenderInterface->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
}

void D3DUIRender::DrawRotatedImage(float fAngle, IUTexture* texture, const URect& dstRect, const URect& srcRect,const UColor& Diffuse, int nBlend, float z)
{
	if(!texture)
      return;

	D3D9Texture* pTexture = (D3D9Texture*)texture;

#ifdef HW_SUPPORT_NONPOWOF2
   SetupRect(dstRect, srcRect,pTexture->Width, pTexture->Height, Diffuse,z);
#else 
	SetupRect(dstRect, srcRect,pTexture->RelWidth, pTexture->RelHeight, Diffuse);
#endif

	float x = float(dstRect.pt0.x + dstRect.pt1.x)/2;
	float y = float(dstRect.pt0.y + dstRect.pt1.y)/2;
	//float z = 0.f;

	D3DXMATRIX kMat;
	D3DXMATRIX kMat1;

	D3DXMatrixIdentity(&kMat);

	D3DXMatrixTranslation(&kMat1, -x, -y, -z);
	D3DXMatrixMultiply(&kMat, &kMat, &kMat1);
	
	D3DXMatrixRotationZ(&kMat1, fAngle);
	D3DXMatrixMultiply(&kMat, &kMat, &kMat1);
	
	D3DXMatrixTranslation(&kMat1, x, y, z);
	D3DXMatrixMultiply(&kMat, &kMat, &kMat1);

	D3DXVec3TransformCoord((D3DXVECTOR3 *)m_RectVertics[0].pos, (D3DXVECTOR3 *)m_RectVertics[0].pos, &kMat);
	D3DXVec3TransformCoord((D3DXVECTOR3 *)m_RectVertics[1].pos, (D3DXVECTOR3 *)m_RectVertics[1].pos, &kMat);
	D3DXVec3TransformCoord((D3DXVECTOR3 *)m_RectVertics[2].pos, (D3DXVECTOR3 *)m_RectVertics[2].pos, &kMat);
	D3DXVec3TransformCoord((D3DXVECTOR3 *)m_RectVertics[3].pos, (D3DXVECTOR3 *)m_RectVertics[3].pos, &kMat);
	if (nBlend == 0)
	{
		g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	}else if (nBlend == 1)
	{
		g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}else if (nBlend == 2)
	{
		g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
		g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	}
	else if (nBlend == 3)
	{
		g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_DESTALPHA);
		g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVDESTALPHA);
	}

	g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	g_pRenderInterface->SetTexture(0, pTexture->m_pTexture);
	g_pRenderInterface->DrawPrimitiveUP(
		D3DPT_TRIANGLESTRIP,
		2,
		m_RectVertics,
		sizeof(UVertex)
		);
	g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_DISABLE);
}

void D3DUIRender::DrawSkin(USkin* skin, int index, const URect& ScreenRect, const UColor& Diffuse,BOOL bGlow, float z)
{
	assert(skin);
	USkinImp* pSkin = (USkinImp*)skin;
	assert(index >= 0 && index < pSkin->m_Items.size());
	if( index >= pSkin->m_Items.size() )
		return;
	const USKINITEM& Item = pSkin->m_Items[index];
	DrawImage(skin->GetTexture(), ScreenRect, Item.SubSec, Diffuse, 0, z);
}

void D3DUIRender::DrawSkin(USkin* skin, int index, const UPoint& pt,  const UColor& Diffuse,BOOL bGlow, float z)
{
	assert(skin);
	USkinImp* pSkin = (USkinImp*)skin;
	assert(index >= 0 && index < pSkin->m_Items.size());
	if( index >= pSkin->m_Items.size() )
		return;
	const USKINITEM& Item = pSkin->m_Items[index];
	URect Screen;
	Screen.left = pt.x;
	Screen.top = pt.y;
	Screen.right = pt.x + Item.SubSec.GetWidth();
	Screen.bottom = pt.y + Item.SubSec.GetHeight();
	DrawImage(skin->GetTexture(), Screen, Item.SubSec, Diffuse, 0, z);
}
void D3DUIRender::DrawCoolDownMask(const URect& rect  , float fAngle ,  const UColor& Color, float z)
{
	/*
	v2-------------v1--------------v8
	|              |               |
	|              |               |
	|              |               |
	|              |               |
	v3-------------v0--------------v7
	|              |               |
	|              |               |
	|              |               |
	|              |               |
	v4-------------v5--------------v6*/
	UVertex TrigleVertics[10];
	float left = (float)rect.left;
	float top = (float)rect.top;
	float right = (float)rect.right;
	float bottom = (float)rect.bottom;
	float middleWide = (right - left)/2.0f + left;
	float middleHegiht = (bottom - top)/2.0f + top;

	//v0 v1固定
	TrigleVertics[0].SetPos(middleWide,middleHegiht, 0.0f);
	TrigleVertics[1].SetPos(middleWide, top, 0.0f);
	int PrimitiveNum = int(fAngle/0.125f);


	//v2
	TrigleVertics[8].SetPos(right,top, 0.0f);
	//v3
	TrigleVertics[7].SetPos(right,middleHegiht, 0.0f);
	//v4
	TrigleVertics[6].SetPos(right,bottom, 0.0f);
	//v5
	TrigleVertics[5].SetPos(middleWide,bottom, 0.0f);
	//v6
	TrigleVertics[4].SetPos(left,bottom, 0.0f);
	//v7
	TrigleVertics[3].SetPos(left,middleHegiht, 0.0f);
	//v8
	TrigleVertics[2].SetPos(left,top, 0.0f);
	//v9
	TrigleVertics[9].SetPos(middleWide, top, 0.0f);

	float woffset = (rect.GetWidth() / 2.0f) * ((fAngle - PrimitiveNum * 0.125f)/0.125f);
	float hoffset = (rect.GetHeight() / 2.0f) * ((fAngle - PrimitiveNum * 0.125f)/0.125f);
	switch (PrimitiveNum)
	{
	case 0:
		TrigleVertics[9 - PrimitiveNum].SetPos(middleWide + woffset , top , 0.0f);
		break;

	case 1:
		TrigleVertics[9 - PrimitiveNum].SetPos(right , top + hoffset , 0.0f);
		break;

	case 2:
		TrigleVertics[9 - PrimitiveNum].SetPos(right , middleHegiht + hoffset , 0.0f);
		break;

	case 3:
		TrigleVertics[9 - PrimitiveNum].SetPos(right - woffset , bottom , 0.0f);
		break;

	case 4:
		TrigleVertics[9 - PrimitiveNum].SetPos(middleWide - woffset , bottom , 0.0f);
		break;

	case 5:
		TrigleVertics[9 - PrimitiveNum].SetPos(left , bottom -  hoffset , 0.0f);
		break;

	case 6:
		TrigleVertics[9 - PrimitiveNum].SetPos(left , middleHegiht -  hoffset , 0.0f);
		break;

	case 7:
		TrigleVertics[9 - PrimitiveNum].SetPos(left +  woffset , top , 0.0f);
		break;
	default:
		PrimitiveNum = 8;
		break;
	}

	TrigleVertics[0].SetColor(Color);
	TrigleVertics[1].SetColor(Color);
	TrigleVertics[2].SetColor(Color);
	TrigleVertics[3].SetColor(Color);
	TrigleVertics[4].SetColor(Color);
	TrigleVertics[5].SetColor(Color);
	TrigleVertics[6].SetColor(Color);
	TrigleVertics[7].SetColor(Color);
	TrigleVertics[8].SetColor(Color);
	TrigleVertics[9].SetColor(Color);

	g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
	g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	g_pRenderInterface->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_DISABLE);
	g_pRenderInterface->DrawPrimitiveUP(
		D3DPT_TRIANGLEFAN,
		8 -PrimitiveNum,
		TrigleVertics,
		sizeof(UVertex)
		);
	g_pRenderInterface->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	g_pRenderInterface->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	g_pRenderInterface->SetTextureStageState(0,D3DTSS_COLOROP, D3DTOP_MODULATE);
}
INT D3DUIRender::DrawTextN( IUFont* Font, const UPoint& Pos, const char* szText, INT Num, UColor* ColorRemap, const UColor& Color,  float z, float scale)
{
	static WCHAR wBuf[1024 * 512];

	INT Len =  strlen(szText);
	int Ret = MultiByteToWideChar(CP_UTF8, 0, szText, Len, wBuf, Len);
	if (Ret > 0)
	{
		wBuf[Ret] = 0;
	}else
	{
		wBuf[0] = 0;
		return 0 ;
	}
	return DrawTextN(Font,Pos,wBuf,Ret,ColorRemap,Color,z,scale);
}

/*
  \c
*/
INT D3DUIRender::DrawTextN( IUFont* Font, const UPoint& Pos, const WCHAR* szText, INT Num, UColor* ColorRemap, const UColor& Color,  float z, float scale)
{
	if( Num < 1 )
		return Pos.x;

	// If it's over about 4000 verts we want to break it up
	if( Num > 666 )
	{
		int left = DrawTextN(Font, Pos, szText, 666, ColorRemap, Color,z);

		UPoint newDrawPt(left, Pos.y);
		
		return DrawTextN(Font, newDrawPt, &(szText[666]), Num - 666, ColorRemap, Color, z);
	}

	UPoint pt(0,0);
	UColor currentColor = Color;
	m_FontBatcher.Begin((D3D9Font*)Font, Num, z, scale);
	WCHAR c = szText[0]; 
	for(INT i = 0; szText[i] && i < Num; i++, c = szText[i])
	{
		// We have to do a little dance here since \t = 0x9, \n = 0xa, and \r = 0xd
		if ((c >=  1 && c <=  7) ||
			(c >= 11 && c <= 12) ||
			(c == 14)) 
		{
			// Color code
			if (ColorRemap) 
			{
				static BYTE remap[15] = 
				{ 
					0x0,
						0x0, 
						0x1, 
					0x2, 
						0x3, 
						0x4, 
						0x5, 
						0x6, 
						0x0, 
						0x0,
					0x0, 
						0x7, 
						0x8,
						0x0,
						0x9 
				};

				BYTE remapped = remap[c];

	//			// Ignore if the color is greater than the specified max index:
	//			if ( remapped <= maxColorIndex )
	//			{
	//				const UColor &clr = colorTable[remapped];
	//				currentColor = mBitmapModulation = clr;
	//			}

			}

			// And skip rendering this character.
			continue;
		}
		else if ( c == 15 )
		{
			//currentColor = mBitmapModulation = mTextAnchorColor;

			// And skip rendering this character.
			continue;
		}
	//	// push color:
		else if ( c == 16 )
		{
	//		mTextAnchorColor = mBitmapModulation;

	//		// And skip rendering this character.
			continue;
		}
	//	// pop color:
		else if ( c == 17 )
		{
	//		currentColor = mBitmapModulation = mTextAnchorColor;

	//		// And skip rendering this character.
			continue;
		}
	//	// Tab character
		else if ( c == L'\t' ) 
		{
	//		const PlatformFont::CharInfo &ci = font->getCharInfo( dT(' ') );
	//		pt.x += ci.xIncrement * GFont::TabWidthInSpaces;

	//		// And skip rendering this character.
			continue;
		}
		// Don't draw invalid characters.
		else if( !Font->IsValidChar( c ) ) 
		{
			continue;
		}	
		// Queue char for rendering..
		m_FontBatcher.BatchCharacter(c, pt.x, currentColor);
	}

	m_FontBatcher.End(Pos);

	return pt.x - Pos.x;
}

void D3DUIRender::SetClipRect(const URect& ClipRect)
{
    m_rcScissor = ClipRect;
    if (g_Desktop)
    {
        UINT MaxX = g_Desktop->GetWindowSize().x ;
        UINT MaxY = g_Desktop->GetWindowSize().y ;

        if (m_rcScissor.left < 0)
        {
            m_rcScissor.left = 0;
        }

        if (m_rcScissor.top < 0)
        {
            m_rcScissor.top = 0;
        }

        if (m_rcScissor.right > MaxX)
        {
            m_rcScissor.right = MaxX;
        }

        if (m_rcScissor.bottom > MaxY)
        {
            m_rcScissor.bottom = MaxY;
        }
    }
}

const URect& D3DUIRender::GetClipRect()
{
	return m_rcScissor;
}

UFontPtr D3DUIRender::CreateFont(const char* faceName, int size, UINT uCharSet)
{
    //现在只支持一种字体
    //faceName = "黑体";
    uCharSet = 0;

	assert(faceName);
	const char* FontCachePath = "Data\\ui\\Font\\";		// hardcode. should be defined by config.
	char FontFileName[MAX_PATH];
	memset(FontFileName, 0, MAX_PATH);
	LSprintf(FontFileName, MAX_PATH, "%s%s_%d_%d.uft", FontCachePath, faceName, size, uCharSet);
	strupr(FontFileName);
	std::string str = FontFileName;
	FontMap::const_iterator it = m_mapLoadedFont.find(str);
	if (it != m_mapLoadedFont.end())
	{
		IUFont* Font = (IUFont*)it->second;
		return Font;
	}

	// new font.
	D3D9Font* NewFont = new D3D9Font;
	assert(NewFont);
	if (!NewFont->Create(faceName, size, uCharSet))
	{
		delete NewFont;
		return NULL;
	}

	// add to map
	NewFont->SetFileName(str);
	m_mapLoadedFont.insert(FontMap::value_type(str, NewFont));
	return (IUFont*)NewFont;
}

UTexturePtr D3DUIRender::LoadTexture(const char* filename, const char* pRelPath)
{
	if (filename == NULL || filename[0] == '\0' || g_pRenderInterface == NULL)
	{
		return NULL;
	}
	char FileName[MAX_PATH];
	BOOL bPathValidate = FALSE;
	if (pRelPath && pRelPath[0] != 0)
	{
		strcpy(FileName, pRelPath);

		int nTail = strlen(FileName);
		if (FileName[nTail - 1] == '/')
		{
			FileName[nTail -1] = '\\';
		}else if (FileName[nTail - 1] != '\\')
		{
			FileName[nTail++] = '\\';
		}
		strcpy(&FileName[nTail], filename);
		bPathValidate = TRUE;
	}
	std::string strFileName = bPathValidate ? FileName : filename;
	strupr(&strFileName[0]);
	RevisionPath(&strFileName[0]);
	
	// find texture
	TextureMap::const_iterator it = m_mapTextures.find(strFileName);
	if (it != m_mapTextures.end())
	{
		return (IUTexture*)it->second;
	}

    CFileStream kFile;
    if( !kFile.Open(strFileName.c_str()) )
    {
        UTRACE("Load texture %s failed.", strFileName.c_str());
        return NULL;
    }

    unsigned int uiSize = kFile.GetSize();
    char* pBuffer = (char*)UMalloc(uiSize);
    assert(pBuffer);
    uiSize = kFile.Read(pBuffer, uiSize);

	// new texture
	IDirect3DTexture9* pTexture;
	D3DXIMAGE_INFO ImageInfo;
#ifdef HW_SUPPORT_NONPOWOF2
	UINT uTexSize = D3DX_DEFAULT_NONPOW2; 
#else
	UINT uTexSize = D3DX_DEFAULT;
#endif 

	HRESULT hr = g_pRenderInterface->CreateTextureFromFileInMemoryEx(pBuffer, uiSize, uTexSize, uTexSize,
		1,
		0,
		D3DFMT_UNKNOWN,
		D3DPOOL_MANAGED,
		D3DX_FILTER_NONE,
		D3DX_FILTER_NONE,
		0,
		&ImageInfo,
		NULL,
		&pTexture);

	UFree(pBuffer);

	if (FAILED(hr))
	{
		UTRACE("Load texture %s failed.", strFileName.c_str());
		return NULL;
	}

	assert(pTexture);
	D3D9Texture* pNewTexture = new D3D9Texture;
	assert(pNewTexture);
	pNewTexture->m_pTexture = pTexture;
	pNewTexture->Width = ImageInfo.Width;
	pNewTexture->Height = ImageInfo.Height;
	pNewTexture->m_Name = strFileName;

#ifndef HW_SUPPORT_NONPOWOF2
	D3DSURFACE_DESC SurfDesc;
	if(FAILED(pTexture->GetLevelDesc(0, &SurfDesc)))
	{
		return NULL;
	}
	pNewTexture->RelWidth = SurfDesc.Width;
	pNewTexture->RelHeight = SurfDesc.Height;
#endif

	// add texture.
	m_mapTextures.insert(TextureMap::value_type(strFileName, pNewTexture));
	
	//pNewTexture->AddRef();
	return pNewTexture;
	return NULL;
}

void D3DUIRender::DestroyTexture(D3D9Texture* pTexture)
{
	assert(pTexture && !pTexture->m_Name.empty());
	TextureMap::iterator it = m_mapTextures.find(pTexture->m_Name);
	if (it != m_mapTextures.end())
	{
		m_mapTextures.erase(it);
		delete pTexture;
	}
}
void D3DUIRender::DestroyFont(const char * filename)
{
	assert(filename);
	FontMap::iterator it = m_mapLoadedFont.find(filename);
	if (it != m_mapLoadedFont.end())
	{
		m_mapLoadedFont.erase(it);
	}
}