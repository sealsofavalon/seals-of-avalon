#pragma once
#include "URender.h"

struct CharInfo;

class D3D9Font : public IUFont
{
public:
	D3D9Font();
	virtual ~D3D9Font();
	BOOL Create(const char* FaceName, int size, UINT charset);
	void Destroy();
	void SetFileName(const std::string& FileName);
	const CharInfo* GetCharDesc(WCHAR wc);
	IDirect3DTexture9* GetTexture(int page);
	UINT GetBaseLine();
	std::string m_FaceName;
	int m_FontSize;
private:
	virtual INT GetStrWidth(const char* Str);
	virtual INT GetStrWidth(const WCHAR* wStr);
	virtual UINT GetStrNWidthPrecise(const WCHAR*, UINT n);
	virtual INT GetCharWidth(WCHAR wChar);
	virtual INT GetStrNWidth(const WCHAR* Str, int Num);
	virtual INT GetHeight();
	virtual UINT GetBaseline() const;
	virtual UINT GetDescent() const;
	virtual INT GetCharXIncrement(WCHAR wChar);
	virtual BOOL IsValidChar(WCHAR wChar);
	virtual UINT GetBreakPos(const WCHAR *string, UINT strlen, UINT width, BOOL breakOnWhitespace);
	class UFont* m_pImpFont;
};


#define HW_SUPPORT_NONPOWOF2  // 定义这个假定硬件支持non-pow-of-2 

class D3D9Texture : public IUTexture
{
	virtual void Release();
public:
	D3D9Texture();
	virtual ~D3D9Texture();
	virtual INT GetWidth() { return Width;}
	virtual INT GetHeight() { return Height; }
	INT Width;
	INT Height;
#ifndef HW_SUPPORT_NONPOWOF2
	INT RelWidth;	// for non power of 2.
	INT RelHeight;
#endif 
	IDirect3DTexture9* m_pTexture;
	std::string m_Name;
};

class D3DUIRender : public URender
{
	struct UVertex
	{
		UVertex()
		{
			pos[3] = 1.0f;
		}
		float pos[4];
		D3DCOLOR c;
		float uv[2];
		inline void SetPos(float x, float y, float z)
		{
			pos[0] = x; 
			pos[1] = y;
			pos[2] = z;
			pos[3] = 1.0f;
		}
		inline void SetPos(float x, float y, float z, float w)
		{
			pos[0] = x; 
			pos[1] = y;
			pos[2] = z;
			pos[3] = w;
		}
		inline void SetUV(float u, float v)
		{
			uv[0] = u;
			uv[1] = v;
		}
		inline void SetColor(const UColor& color)
		{
			c = D3DCOLOR_RGBA(color.r, color.g, color.b, color.a);
		}
		inline void SetColor(DWORD color)
		{
			c = color;
		}
	};
	enum
	{
		UVERTEX_FVF = D3DFVF_XYZRHW | D3DFVF_TEX1 | D3DFVF_DIFFUSE,
	};


	class FontRenderBatcher
	{
	public:
		FontRenderBatcher();

		struct CharMarker 
		{
			WORD c;
			WORD x;
			DWORD color; 
		//	CharInfo *ci;
		};

		struct Page
		{
			INT numChars;
			INT startVertex;
			CharMarker charIndex[1];
		};

		std::vector<BYTE> m_MemPool;
		int m_PoolPos;
		inline BYTE* Alloc(int size)
		{
			int bufsize = m_PoolPos + size;

			if (bufsize > (int)m_MemPool.size() )
			{
				m_MemPool.resize(int(bufsize*1.6f));
				
				BYTE* pPoolHead = &m_MemPool[0];
				// 增量地设置大小后, Page所指向的地址已经无效.
				for (int i = 0; i < (int)mSheets.size(); i++)
				{
					if (mSheets[i])
					{
						mSheets[i] = (Page*)(pPoolHead + m_PageOfs[i]);
					}
				}
			}
			BYTE* ret = &m_MemPool[bufsize- size];
			m_PoolPos+= size;
			return ret;
		}

		std::vector<Page*> mSheets;
		std::vector<size_t> m_PageOfs;
		D3D9Font *mFont;
		int mLength;
		float m_z;
		float m_Scale;
		Page &GetPage(int sheetID);
		void Begin(D3D9Font *font, int n, float z, float scale);
		void BatchCharacter(WCHAR c, int &currentX, const UColor&currentColor);
		void End( const UPoint& offset );
	};

public:
	D3DUIRender(IDirect3DDevice9* pDev);
	~D3DUIRender(void);

	void DestroyTexture(D3D9Texture* pTexture);
	void DestroyFont(const char * filename);
private:
	virtual UFontPtr CreateFont(const char* faceName, int size, UINT uCharSet);
	virtual UTexturePtr LoadTexture(const char* texName, const char* pRelPath = NULL);
	
		virtual BOOL Initialize();
	virtual void Shutdown();
	virtual void BeginRender();
	virtual void EndRender();
	virtual void DrawRect(const URect& rect, UColor Color, float z = 0.0f);
	virtual void DrawRectFill(const URect& rect, UColor Color, float z = 0.0f) ;
	virtual void DrawPolygon(UPolygon polygon, const UColor& Color, float z = 0.0f);
	//virtual void DrawRectFill(const UPoint& LeftTop, const UPoint& RightBottom, UColor Color) = 0;
	virtual void DrawLine(const UPoint& pt0, const UPoint& pt1, UColor Color, float z = 0.0f);
//	virtual void DrawLine(int x0, int y0, int x1, int y1, UColor Color) =0 ;
	virtual void DrawImage( IUTexture *texture, const URect &dstRect, const URect &srcRect,const UColor& Diffuse = LCOLOR_WHITE,int nBlend =0, float z = 0.0f); 
	virtual void DrawMaskImage(IUTexture *texture, const float* texcoords, IUTexture *alpha, const float* alphaTexcoords, const float* destRect, float z = 0.0f);
	virtual void DrawMaskImageD(IDirect3DBaseTexture9 *texture, const float* texcoords, IUTexture *alpha, const float* alphaTexcoords, const float* destRect, float z = 0.0f) ;
	virtual void DrawRotatedImage(float fAngle, IUTexture* texture, const URect& dstRect, const URect& srcRect,const UColor& Diffuse = LCOLOR_WHITE, int nBlend = 0, float z = 0.0f);
	virtual void DrawSkin(USkin* skin, int index, const URect& ScreenRect, const UColor& Diffuse = LCOLOR_WHITE, BOOL bGlow = FALSE, float z = 0.0f); 
	virtual void DrawSkin(USkin* skin, int index, const UPoint& pt, const UColor& Diffuse = LCOLOR_WHITE, BOOL bGlow = FALSE, float z = 0.0f); 

	virtual void DrawCoolDownMask(const URect& rect , float fAngle ,  const UColor& Color = LCOLOR_WHITE, float z = 0.0f);

	//virtual INT UDrawText( IUFont* Font, const UPoint& Pos, const char* szText, UColor* ColorRemap, UColor Color = LCOLOR_WHITE) = 0;
	virtual INT DrawTextN( IUFont* Font, const UPoint& Pos, const char* szText, INT Num, UColor* ColorRemap, const UColor& Color = LCOLOR_WHITE, float z = 0.0f, float scale = 1.0f);
	//virtual INT UDrawText( IUFont* Font, const UPoint& Pos, const WCHAR* szText, UColor* ColorRemap, UColor Color = LCOLOR_WHITE) = 0;
	virtual INT DrawTextN( IUFont* Font, const UPoint& Pos, const WCHAR* szText, INT Num, UColor* ColorRemap, const UColor& Color = LCOLOR_WHITE, float z = 0.0f, float scale = 1.0f);

	virtual void SetClipRect(const URect& ClipRect);
	virtual const URect& GetClipRect();
//
	private:
	inline void SetupLine(const UPoint& pt0, const UPoint& pt1, const UColor& color, float z = 0.0f);
	inline void SetupRect(const URect& rect, const UColor& color, float z = 0.0f);
	inline void SetupRectForLine(const URect& rect, const UColor& color, float z = 0.0f);

	inline void SetupRect(const URect& rect, const URect& uvrect,float uSize, float vSize, const UColor& color, float z = 0.0f);
 
	BOOL m_bBegined;
	UVertex m_RectVertics[4];
	URect m_rcScissor;
	FontRenderBatcher m_FontBatcher;
	typedef std::map<std::string, D3D9Font*> FontMap;
	typedef std::map<std::string, D3D9Texture*> TextureMap;
	TextureMap m_mapTextures;
	FontMap m_mapLoadedFont;

};

inline void D3DUIRender::SetupLine(const UPoint& pt0, const UPoint& pt1, const UColor& color, float z)
{
	m_RectVertics[0].SetPos((float)pt0.x, (float)pt0.y, z);
	m_RectVertics[1].SetPos((float)pt1.x, (float)pt1.y, z);
	m_RectVertics[0].SetColor(color);
	m_RectVertics[1].SetColor(color);
}
/*
   0 --------- 1
	|         |
	|		  |
	|_________|
	2		  3
*/
inline void D3DUIRender::SetupRect(const URect& rect, const UColor& color, float z)
{
	float left = (float)rect.left;
	float top = (float)rect.top;
	float right = (float)rect.right;
	float bottom = (float)rect.bottom;

	m_RectVertics[0].SetPos(left, top, z);
	m_RectVertics[1].SetPos(right,top, z);
	m_RectVertics[2].SetPos(left,bottom, z);
	m_RectVertics[3].SetPos(right, bottom, z);
	m_RectVertics[0].SetColor(color);
	m_RectVertics[1].SetColor(color);
	m_RectVertics[2].SetColor(color);
	m_RectVertics[3].SetColor(color);
}

inline void D3DUIRender::SetupRectForLine(const URect& rect, const UColor& color, float z)
{
	float left = (float)rect.left;
	float top = (float)rect.top;
	float right = (float)rect.right-1;
	float bottom = (float)rect.bottom-1;

	m_RectVertics[0].SetPos(left, top, z);
	m_RectVertics[1].SetPos(right,top, z);
	m_RectVertics[2].SetPos(left,bottom, z);
	m_RectVertics[3].SetPos(right, bottom, z);
	m_RectVertics[0].SetColor(color);
	m_RectVertics[1].SetColor(color);
	m_RectVertics[2].SetColor(color);
	m_RectVertics[3].SetColor(color);
}

inline void D3DUIRender::SetupRect(const URect& rect, const URect& uvrect, float uSize, float vSize, const UColor& color, float z)
{
	float left = (float)rect.left;
	float top = (float)rect.top;
	float right = (float)rect.right;
	float bottom = (float)rect.bottom;

	m_RectVertics[0].SetPos(left, top, z);
	m_RectVertics[1].SetPos(right,top, z);
	m_RectVertics[2].SetPos(left,bottom, z);
	m_RectVertics[3].SetPos(right, bottom, z);
	m_RectVertics[0].SetColor(color);
	m_RectVertics[1].SetColor(color);
	m_RectVertics[2].SetColor(color);
	m_RectVertics[3].SetColor(color);

	float u0, u1,v0,v1;
	uSize = 1.0f/uSize;
	vSize = 1.0f/vSize;
	u0 = float(uvrect.left +  0.5f)*uSize;
	u1 = float(uvrect.right +  0.5f)*uSize;
	v0 = float(uvrect.top +  0.5f) *vSize;
	v1 = float(uvrect.bottom +  0.5f)*vSize;

	m_RectVertics[0].SetUV(u0, v0);
	m_RectVertics[1].SetUV(u1, v0);
	m_RectVertics[2].SetUV(u0, v1);
	m_RectVertics[3].SetUV(u1, v1);
}