#include "StdAfx.h"
#include "PakManager.h"
#include "Unzipper.h"

CPakManager* CPakManager::ms_pkInstance = NULL;

CPakManager::CPakManager(void)
{
}

CPakManager::~CPakManager(void)
{
	Destroy();
}

CPakManager* CPakManager::GetInstance()
{
	if(ms_pkInstance == NULL)
	{
		ms_pkInstance = new CPakManager;
	}

	return ms_pkInstance;
}

void CPakManager::Init()
{
	// 
	CPakManager::GetInstance()->AddZip("Objects\\Effect\\Effect.cpk");
	CPakManager::GetInstance()->AddZip("Objects\\Character\\Item\\TierB\\TierB.cpk");
	CPakManager::GetInstance()->AddZip("Objects\\Character\\Item\\TierK\\TierK.cpk");
	CPakManager::GetInstance()->AddZip("Objects\\Character\\Item\\TierP\\TierP.cpk");
	CPakManager::GetInstance()->AddZip("Objects\\Character\\Item\\TierT\\TierT.cpk");
	CPakManager::GetInstance()->AddZip("Objects\\Character\\Item\\TierX\\TierX.cpk");
	CPakManager::GetInstance()->AddZip("Objects\\Character\\Weapon\\Weapon.cpk");
}

void CPakManager::Destroy()
{
	PAKED_FILEINFO* pInfo;
	STLMAP_PAKED_FILE::iterator itr;
	for(itr = m_mapPakedFile.begin(); itr != m_mapPakedFile.end(); ++itr)
	{
		pInfo = (*itr).second;
		delete pInfo; pInfo = NULL;
	}
	m_mapPakedFile.clear();

	DeleteAllZip();
}

bool CPakManager::Create()
{
	return true;
}

void CPakManager::DeleteAllZip()
{
	CUnzipper* pUnzipper;
	for(STLVTR_ZIP::iterator itrtr = m_vectorZip.begin() ; itrtr != m_vectorZip.end(); ++itrtr)
	{
		pUnzipper = *itrtr;
		pUnzipper->CloseZip();

		delete (*itrtr);
	}

	m_vectorZip.clear();
}

CUnzipper* CPakManager::AddZip(const char* strFilename)
{
	CUnzipper* pUnzipper = new CUnzipper;
	if(!pUnzipper->OpenZip(strFilename))
	{
		// zip
		delete pUnzipper;
		return NULL;
		//		_assert(0);
	}

	m_vectorZip.push_back(pUnzipper);

	IndexingPakedFile(pUnzipper);

	return pUnzipper;
}

void CPakManager::IndexingPakedFile(CUnzipper* pUnzipper)
{
	int nTotalFileNum = pUnzipper->GetFileCount();
	int nFileCount = 0;
	int nFolderCount = 0;
	int nIndex = 0;
	PAKED_FILEINFO* pPakFile;

	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	char filename[MAX_PATH];

	unsigned long ulOffset;

	if(pUnzipper->GotoFirstFile(NULL))
	{
		do
		{
			UZ_FileInfo info;
			pUnzipper->GetFileInfo(info);
			ulOffset = pUnzipper->GetOffset();

			pPakFile = new PAKED_FILEINFO;
			pPakFile->nIndex	= nIndex++;
			pPakFile->pOwner	= pUnzipper;
			pPakFile->ulOffset	= ulOffset;

			_splitpath_s(info.szFileName, drive, _MAX_DRIVE, dir, _MAX_DIR, fname, _MAX_FNAME, ext, _MAX_EXT);
			_strlwr_s(fname);
			_strlwr_s(ext);

			if(strlen(ext))
			{
				sprintf_s(filename, "%s%s%s", dir, fname, ext);

				_strupr(&filename[0]);

				std::string strFilename = filename;

				m_mapPakedFile.insert(std::pair<std::string, PAKED_FILEINFO*>(strFilename, pPakFile));

				if(!info.bFolder)
				{
					nFileCount++;
				}
				else
					nFolderCount++;
			}	
		}
		while (pUnzipper->GotoNextFile(NULL));
	}
}

void CPakManager::ParseFileName(CUnzipper* pUnzipper, std::vector<std::string>& v)
{
	int nTotalFileNum = pUnzipper->GetFileCount();
	int nFileCount = 0;
	int nFolderCount = 0;
	int nIndex = 0;
	PAKED_FILEINFO* pPakFile;

	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	char filename[MAX_PATH];

	unsigned long ulOffset;

	if(pUnzipper->GotoFirstFile(NULL))
	{
		do
		{
			UZ_FileInfo info;
			pUnzipper->GetFileInfo(info);
			ulOffset = pUnzipper->GetOffset();

			pPakFile = new PAKED_FILEINFO;
			pPakFile->nIndex	= nIndex++;
			pPakFile->pOwner	= pUnzipper;
			pPakFile->ulOffset	= ulOffset;

			_splitpath_s(info.szFileName, drive, _MAX_DRIVE, dir, _MAX_DIR, fname, _MAX_FNAME, ext, _MAX_EXT);
			_strlwr_s(fname);
			_strlwr_s(ext);

			if(strlen(ext))
			{
				sprintf_s(filename, "%s%s%s", dir, fname, ext);

				_strupr(&filename[0]);

				std::string strFilename = filename;


				if(!info.bFolder && strcmp(ext, "cpk"))
				{

					v.push_back(strFilename);
				}
				else
				{
				}
			}	
		}
		while (pUnzipper->GotoNextFile(NULL));
	}
}

bool CPakManager::GetFile(const std::string& strFilename, CPakExtracter* pExtracter)
{
	return GetFile(strFilename, (void**)pExtracter->GetDataAddress(), pExtracter->GetDataSizePtr());
}

bool CPakManager::GetFile(const char* pcFilename, CPakExtracter* pExtracter)
{
	return GetFile(pcFilename, (void**)pExtracter->GetDataAddress(), pExtracter->GetDataSizePtr());
}

bool CPakManager::GetFile(const char* pcFilename, void** ppData, unsigned long* pulSize)
{
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	char filename[MAX_PATH];

	_splitpath_s(pcFilename,drive,dir,fname,ext);
	_strlwr_s(fname);
	_strlwr_s(ext);

	sprintf_s(filename, "%s%s%s", dir, fname, ext);

	PAKED_FILEINFO* pPakedFile = NULL;
	STLMAP_PAKED_FILE::iterator itr;

	_strupr(&filename[0]);

	std::string str = filename;
	int pos = str.find("\\\\");
	while( pos != -1 )
	{
		str.erase(pos, 1);
		pos = str.find("\\\\");
	}
	
	if(m_mapPakedFile.find(str) != m_mapPakedFile.end())
	{
		pPakedFile = m_mapPakedFile[str];
	}

	if(pPakedFile == NULL)
	{
		std::string strTemp = dir;
		strTemp = strTemp.substr(0, strTemp.size() - 1);
		int iPos = strTemp.find_last_of("\\");
		if(iPos != -1)
			strTemp = strTemp.substr(iPos + 1);
		sprintf_s(filename, "%s%s%s", dir, strTemp.c_str(), ".cpk");
		AddZip(filename);
	}

	if(m_mapPakedFile.find(str) != m_mapPakedFile.end())
	{
		pPakedFile = m_mapPakedFile[str];
	}

	if( pPakedFile )
	{
		pPakedFile->pOwner->SetOffset( pPakedFile->ulOffset);
		/*		UZ_FileInfo info;
		pPakedFile->pOwner->GetFileInfo(info);
		*pulSize = info.dwUncompressedSize;
		*ppData = new char[info.dwUncompressedSize];*/
		//if( !pPakedFile->pOwner->UnzipMemoryCS(ppData, pulSize))
		if(!pPakedFile->pOwner->UnzipMemory(ppData, pulSize))
		{
			//NiDebugStringOutput(_T("CPakManager : File unpack failed : %s"), str.c_str());
			return false;
		}
		else
			return true;
	}
	else
	{
		//NiDebugStringOutput(_T("CPakManager : File not found : %s"), str.c_str());
		return false;
	}

}

bool CPakManager::GetFile(const std::string& strFilename, void** ppData, unsigned long* pulSize)
{
	char acFilename[MAX_PATH];
	//MkAString(acFilename, MAX_PATH , strFilename.c_str());

	return GetFile(acFilename, ppData, pulSize);
}

bool CPakManager::IsExistFile(const char* pcFilename)
{
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	char filename[MAX_PATH];

	_splitpath_s(pcFilename,drive,dir,fname,ext);
	_strlwr_s(fname);
	_strlwr_s(ext);

	sprintf_s(filename, "%s%s", fname, ext);

	PAKED_FILEINFO* pPakedFile = NULL;
	STLMAP_PAKED_FILE::iterator itr;

	std::string str = filename;
	if(m_mapPakedFile.find(str) != m_mapPakedFile.end())
	{
		return true;
	}
	else
		return false;
}

bool CPakManager::IsExistFile(const std::string& strFilename)
{
	char acFilename[MAX_PATH];
	//MkAString(acFilename, MAX_PATH , strFilename.c_str() );

	return IsExistFile(acFilename);
}

CPakExtracter::CPakExtracter()
{
	m_pcData = NULL;
	m_ulSize = 0;
}

CPakExtracter::~CPakExtracter(void)
{

}

CPakExtracterSelfDelete::~CPakExtracterSelfDelete(void)
{
	delete[] m_pcData; m_pcData = NULL;
}
