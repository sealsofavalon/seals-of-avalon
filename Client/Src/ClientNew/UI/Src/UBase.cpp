#include "StdAfx.h"
#include "UBase.h"
#include "UControl.h"
#include "USystem.h"
#include "UMemory.h"

UString::UString(const UString* _Copy)
:m_nSize(0), m_nMaxSize(0),m_pBuffer(NULL)
{
	Set(_Copy);
}

UString::UString(const char* cstr)
:m_nSize(0), m_nMaxSize(0),m_pBuffer(NULL)
{
	Set(cstr);
}

UString::UString(const WCHAR* wstr)
:m_nSize(0), m_nMaxSize(0),m_pBuffer(NULL)
{
	Set(wstr);
}

UString::~UString()
{
	if (m_pBuffer)
	{
		UFree(m_pBuffer);
		m_pBuffer = NULL;
	}
}

void UString::Clear()
{
	if (m_pBuffer)
	{
		UFree(m_pBuffer);
		m_pBuffer = NULL;
	}
	m_nSize = m_nMaxSize = 0;
}

BOOL UString::Resize(INT NewSize)
{
	if (NewSize == 0)
	{
		Clear();
	}
	else if (m_pBuffer == NULL)
	{
		// 新分配内存
		UINT AllocSize = max(NewSize, 32);
		AllocSize = (AllocSize +3)&~3;
		m_pBuffer = (char*)UMalloc(AllocSize);
		m_nSize = 0;
		m_pBuffer[0] = '\0';
		m_nMaxSize = AllocSize;
	}
	else if (NewSize > m_nMaxSize)	
	{	
		UINT NewMaxSize = (NewSize + 3)&~3;
		char* pNewData = (char*)UMalloc(NewMaxSize);
		// 拷贝数据
		memcpy(pNewData, m_pBuffer, m_nSize);
		pNewData[m_nSize] = '\0';
		UFree(m_pBuffer);
		m_pBuffer = pNewData;
		m_nMaxSize = NewMaxSize;
	}
	return TRUE;
}

void UString::Set(const UString* _Copy)
{
	if (_Copy == this)
	{
		return;
	}

	if (_Copy && !_Copy->Empty())
	{
		Resize(_Copy->GetLength() + 1);
		memcpy(m_pBuffer, _Copy->m_pBuffer, _Copy->GetLength());
	}else
	{
		Clear();
	}
}

void UString::Set(const char* cstr)
{
	if (cstr == NULL || cstr[0] == '\0')
	{
		Clear();
		return;
	}
int i;
	if (cstr >= m_pBuffer && cstr < (m_pBuffer + m_nSize))
	{
		if(cstr == m_pBuffer)
			return;

		int Left = cstr - m_pBuffer;
		int right = strlen(cstr);
		for (i =0; i < right; i++)
		{
			m_pBuffer[i] = cstr[i];
		}
		m_pBuffer[i] = '\0';
		m_nSize = right;
		return;
	}

	int Len = strlen(cstr);
	if(Resize(Len+1))
	{
		memcpy(m_pBuffer, cstr, Len);
		m_pBuffer[Len] = '\0';
		m_nSize = Len;
		return;
	}else
	{
		m_pBuffer[0] = '\0';
		m_nSize = 0;
	}

}

void UString::Set(const WCHAR* wstr)
{
	if(wstr == NULL || wstr[0] == 0)
	{
		Clear();
		return;
	}
	int Len = wcslen(wstr);
	int cstrlen = WideCharToMultiByte(CP_UTF8, 0, wstr, Len, NULL, 0, NULL, NULL);
	if (!Resize(cstrlen +1))
	{
		return;
	}

	int Ret = WideCharToMultiByte( CP_UTF8, 0, wstr, Len, m_pBuffer, cstrlen, NULL, NULL);
	if (Ret > 0 && Ret <= cstrlen)
	{
		m_pBuffer[Ret] = 0;
		m_nSize = Ret;
	}else
	{
		m_pBuffer[0] = 0;	// 发生错误, 保留内存
		m_nSize = 0;
	}
}

void UString::Append(const UString& Buf)
{
	int Len = GetLength();
	int BufLen = Buf.GetLength();
	int NewLen = Len + BufLen;
	if(Resize(NewLen+1))
	{
		memcpy(&m_pBuffer[Len], Buf.m_pBuffer, BufLen);
		m_pBuffer[NewLen] = 0;
	}
}

void UString::Insert(UINT charOffset, const UString& Buf)
{
	int Len = GetLength();
	if(charOffset >= Len)
	{
		// 直接在尾部追加
		Append(Buf);
		return;
	}
	int BufLen = Buf.GetLength();
	int NewLen = Len + BufLen;
	if (!Resize(NewLen + 1))
	{
		return;
	}

	const CHAR* CopyStart = &m_pBuffer[charOffset];

	const INT CopyNum = (INT)(GetLength() - charOffset);
	if(Len > 0)
	{
		for(INT i= CopyNum - 1; i >= 0; i--)
		{
			m_pBuffer[charOffset + i + BufLen] = m_pBuffer[charOffset+i];
		}
	}
	memcpy(&m_pBuffer[charOffset], Buf.m_pBuffer, BufLen);
}

BOOL UString::SubStr(char* pDest, UINT start, UINT len) const
{
	if(start >= GetLength() || len == 0)
	{
		return FALSE;
	}
	assert(len > 0);
	assert(start+len <= GetLength());

	pDest[len] = 0;
	for(INT i=0; i<len; i++)
	{
		pDest[i] = m_pBuffer[start + i];
	}
	return TRUE;
}

void UString::Cut(UINT start, UINT len, char* pDest)
{
	assert(start < GetLength());
	assert(start+len <= GetLength());
	assert(len > 0);

	if (pDest)
	{
		pDest[len] = '\0';
		for(int i=0; i<len; i++)
		{
			pDest[i] = m_pBuffer[start + i];
		}
	}
	
	// 移动尾部字符
	int Tail = m_nSize - len;
	for(int i = start; i< Tail; i++)
	{
		m_pBuffer[i] = m_pBuffer[i+len];
		m_pBuffer[i + len] = 0;		// 不减少存储. 只修改字符为NULL.
	}
	m_nSize -= len; 
}

WCHAR UString::GetChar(UINT offset) const
{
	assert(offset< m_nSize);
	return m_pBuffer[offset];
}

void UString::Get(char *buff, const UINT buffSize) const
{
	assert(0);
	assert(buff && buffSize && m_pBuffer);
	//	WideCharToMultiByte(CP_UTF8,0, m_pBuffer, GetLength(), buff, buffSize, NULL, NULL);
}

void UString::Get(WCHAR *buff,UINT buffSize) const
{
	assert(0);
	assert(buff && buffSize && m_pBuffer);
	int CopySize = min(buffSize, GetLength());
	memcpy(buff, m_pBuffer, CopySize);
}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////


UStringW::UStringW(const UStringW* _Copy)
:m_nSize(0), m_nMaxSize(0),m_pBuffer(NULL)
{
	Set(_Copy);
}

UStringW::UStringW(const char* cstr)
:m_nSize(0), m_nMaxSize(0),m_pBuffer(NULL)
{
	Set(cstr);
}

UStringW::UStringW(const WCHAR* wstr)
:m_nSize(0), m_nMaxSize(0),m_pBuffer(NULL)
{
	Set(wstr);
}

UStringW::~UStringW()
{
	if (m_pBuffer)
	{
		UFree(m_pBuffer);
	}
}

void UStringW::Clear()
{
	if (m_pBuffer)
	{
		UFree(m_pBuffer);
		m_pBuffer = NULL;
	}
	m_nSize = m_nMaxSize = 0;
}

void UStringW::SetEmpty()
{
	m_nSize = 0;
	if (m_pBuffer)
	{
		m_pBuffer[0] = 0;
	}
	
}

BOOL UStringW::Resize(INT NewSize)
{
	if (NewSize == 0)
	{
		Clear();
	}
	else if (m_pBuffer == NULL)
	{
		// 新分配内存
		UINT AllocSize = max(NewSize, 32);
		AllocSize = (AllocSize +3)&~3;
		m_pBuffer = (WCHAR*)UMalloc(AllocSize*sizeof(WCHAR));
		m_nSize = 0;
		m_pBuffer[0] = 0;
		m_nMaxSize = AllocSize;
	}
	else if (NewSize > m_nMaxSize)	
	{	
		UINT NewMaxSize = (((NewSize - 1)>>5) +1)*32;
		WCHAR* pNewData = (WCHAR*)UMalloc(NewMaxSize*sizeof(WCHAR));
		// 拷贝数据
		memcpy(pNewData, m_pBuffer, m_nSize*sizeof(WCHAR));
		pNewData[m_nSize] = '\0';
		UFree(m_pBuffer);
		m_pBuffer = pNewData;
		m_nMaxSize = NewMaxSize;
	}
	return TRUE;
}

void UStringW::Set(const UStringW* _Copy)
{
	if (_Copy == this)
	{
		return;
	}

	if (_Copy && !_Copy->Empty())
	{
		Resize(_Copy->GetLength() + 1);
		memcpy(m_pBuffer, _Copy->m_pBuffer, _Copy->GetLength()*sizeof(WCHAR));
		m_pBuffer[_Copy->GetLength()] = 0;

		m_nSize = _Copy->GetLength();
	}else
	{
		Clear();
	}
}

void UStringW::Set(const char* cstr)
{
	if (cstr == NULL || cstr[0] == '\0')
	{
		Clear();
		return;
	}
	if (m_pBuffer && m_nSize > 0)
	{
		WCHAR* pWstr = (WCHAR*)cstr;
		if (pWstr >= m_pBuffer && pWstr <= m_pBuffer + m_nSize)
		{
			UERROR("UStringW::Set(ptr), ptr inside the buffer.");
			return;
		}
	}
	
	int Len = strlen(cstr);
	Resize(Len + 1);
	int Ret = MultiByteToWideChar(CP_UTF8, 0, cstr, Len, m_pBuffer, Len);
	if (Ret > 0)
	{
		m_pBuffer[Ret] = 0;
		m_nSize = Ret;
	}else
	{
		m_pBuffer[0] = 0;
		m_nSize = 0;
	}
}

UINT UStringW::GetAnsiLength() const
{
	if(m_nSize == 0)
	{
		return 0;
	}
	int nRet = WideCharToMultiByte(CP_UTF8, 0, m_pBuffer, m_nSize, NULL, NULL, NULL, NULL);
	return nRet;
}

int UStringW::GetAnsi(char* pDest, int nDestLen, int start) const
{
	if (start >= m_nSize)
	{
		return 0;
	}

	if (pDest == NULL || nDestLen == 0)
	{
		return GetAnsiLength();
	}
	

	int nRet = WideCharToMultiByte(CP_UTF8, 0, m_pBuffer + start, m_nSize - start, pDest, nDestLen, NULL, NULL);
	pDest[nRet] = '\0';
	int iError = GetLastError();
	switch(iError)
	{
	case ERROR_INSUFFICIENT_BUFFER:
		ULOG("ERROR_INSUFFICIENT_BUFFER");
		break;
	case ERROR_INVALID_FLAGS:
		ULOG("ERROR_INVALID_FLAGS");
		break;
	case ERROR_INVALID_PARAMETER:
		ULOG("ERROR_INVALID_PARAMETER");
		break;
	case ERROR_NO_UNICODE_TRANSLATION:
		ULOG("ERROR_NO_UNICODE_TRANSLATION");
		break;
	}

	return nRet;
}

void UStringW::Set(const WCHAR* wstr)
{
	if (wstr == NULL || wstr[0] == 0)
	{
		Clear();
		return;
	}
	int i;
	if (wstr >= m_pBuffer && wstr < (m_pBuffer + m_nSize))
	{
		if(wstr == m_pBuffer)
			return;

		int Left = wstr - m_pBuffer;
		int right = wcslen(wstr);
		for (i =0; i < right; i++)
		{
			m_pBuffer[i] = wstr[i];
		}
		m_pBuffer[i] = 0;
		m_nSize = right;
		return;
	}

	int Len = wcslen(wstr);
	if(Resize(Len+1))
	{
		memcpy(m_pBuffer, wstr, Len * sizeof(WCHAR));
		m_pBuffer[Len] = 0;
		m_nSize = Len;
		return;
	}else
	{
		m_pBuffer[0] = 0;
		m_nSize = 0;
	}
}

void UStringW::Append(const UStringW& Buf)
{
	int Len = GetLength();
	int BufLen = Buf.GetLength();
	int NewLen = Len + BufLen;
	if(Resize(NewLen+1))
	{
		memcpy(&m_pBuffer[Len], Buf.m_pBuffer, BufLen*sizeof(WCHAR));
		m_pBuffer[NewLen] = 0;
		m_nSize = NewLen;
	}
}

void UStringW::Append(const WCHAR* Buf, int nLen)
{
	assert(Buf);
	if (nLen == -1)
	{
		nLen = wcslen(Buf);
	}
	if (nLen == 0)
	{
		return;
	}
	int Len = GetLength();
	int NewLen = Len + nLen;
	if(Resize(NewLen+1))
	{
		memcpy(&m_pBuffer[Len], Buf, nLen*sizeof(WCHAR));
		m_pBuffer[NewLen] = 0;
		m_nSize = NewLen;
	}else
	{
		SetEmpty();
	}
}

void UStringW::Append(WCHAR wChar)
{
	WCHAR wStr[] = {wChar, 0};
	Append(wStr, 1);
}

void UStringW::Insert(UINT charOffset, const UStringW& Buf)
{
	int Len = GetLength();
	if(charOffset >= Len)
	{
		// 直接在尾部追加
		Append(Buf);
		return;
	}
	int BufLen = Buf.GetLength();
	int NewLen = Len + BufLen;
	if (!Resize(NewLen + 1))
	{
		return;
	}

	const WCHAR* CopyStart = &m_pBuffer[charOffset];

	const INT CopyNum = (INT)(GetLength() - charOffset);
	if(Len > 0)
	{
		for(INT i= CopyNum - 1; i >= 0; i--)
		{
			m_pBuffer[charOffset + i + BufLen] = m_pBuffer[charOffset+i];
		}
	}
	memcpy(&m_pBuffer[charOffset], Buf.m_pBuffer, sizeof(WCHAR)*BufLen);
}

void UStringW::Insert(UINT Pos, WCHAR wChar)
{
	WCHAR wstr[] = {wChar, 0};
	Insert(Pos,wstr,1);
}

void UStringW::Insert(UINT Pos, const WCHAR* wStr, int nLen)
{
	assert(wStr);
	if (nLen == -1)
	{
		nLen = wcslen(wStr);
	}
	if (nLen == 0)
	{
		return;
	}
	int Len = GetLength();
	if(Pos >= Len)
	{
		// 直接在尾部追加
		Append(wStr, nLen);
		return;
	}

	int NewLen = Len + nLen;
	if (!Resize(NewLen + 1))
	{
		return;
	}
	const WCHAR* CopyStart = &m_pBuffer[Pos];
	const INT CopyNum = (INT)(NewLen - Pos);
	if(Len > 0)
	{
		for(INT i= CopyNum - 1; i >= 0; i--)
		{
			m_pBuffer[Pos + i + nLen] = m_pBuffer[Pos+i];
		}
	}
	memcpy(&m_pBuffer[Pos], wStr, sizeof(WCHAR)*nLen);
	m_nSize = NewLen;
	m_pBuffer[NewLen] = 0;
}
 
BOOL UStringW::SubStr(WCHAR* pDest, UINT start, UINT len) const
{
	if(start >= GetLength() || len == 0)
	{
		return FALSE;
	}
	assert(len > 0);
	assert(start+len <= GetLength());

	pDest[len] = 0;
	for(INT i=0; i<len; i++)
	{
		pDest[i] = m_pBuffer[start + i];
	}
	return TRUE;
}

void UStringW::Cut(UINT start, UINT len, WCHAR* pDest)
{
	assert(start < GetLength());
	assert(start+len <= GetLength());
	assert(len > 0);

	if (pDest)
	{
		pDest[len] = '\0';
		for(int i=0; i<len; i++)
		{
			pDest[i] = m_pBuffer[start + i];
		}
	}
	
	// 移动尾部字符
	int Tail = m_nSize - len;
	for(int i = start; i< Tail; i++)
	{
		m_pBuffer[i] = m_pBuffer[i+len];
		m_pBuffer[i + len] = 0;		// 不减少存储. 只修改字符为NULL.
	}
	m_nSize -= len; 
	m_pBuffer[m_nSize] = 0;
}

WCHAR UStringW::GetChar(UINT offset) const
{
	assert(offset< m_nSize);
	return m_pBuffer[offset];
}
//
//void UStringW::Get(char *buff, const UINT buffSize) const
//{
//	assert(0);
//	assert(buff && buffSize && m_pBuffer);
//	//	WideCharToMultiByte(CP_UTF8,0, m_pBuffer, GetLength(), buff, buffSize, NULL, NULL);
//}
//
//void UString::Get(WCHAR *buff,UINT buffSize) const
//{
//	assert(0);
//	assert(buff && buffSize && m_pBuffer);
//	int CopySize = min(buffSize, GetLength());
//	memcpy(buff, m_pBuffer, CopySize);
//}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
#define  UTF8CODE 1
namespace
{
	std::string AnisToUTF8(const std::string& str)
	{
		char acUnicode[4096];
		char acUTF8[4096];

		//Ansi 转换为 Unicode
		unsigned short usLen = (unsigned short)MultiByteToWideChar(936, 0, str.c_str(), str.size(), (LPWSTR)acUnicode, sizeof(acUnicode)/2);

		//Unicode 转换为 Utf8
		usLen = (unsigned short)WideCharToMultiByte(CP_UTF8, 0, (LPCWSTR)acUnicode, usLen, acUTF8, sizeof(acUTF8), NULL, false);

		return std::string(acUTF8, usLen);
	}

	class UPropertyVisitorINT : public UPropertyVisitor
	{
		virtual BOOL Set(void* Target, const char* Src, INT Size)
		{
			assert(Src && Target);
			int value = atoi(Src);
			int* pi = (int*)Target;
			*pi = value;
			return TRUE;
		}
		virtual BOOL Get(char* Target, INT* Size, const void* Src)
		{
			int value = *((const int*)Src);
			LSprintf(Target,*Size,"%d",value);
			return TRUE;
		}
	} g_PropIntVisitor;

	class UPropertyVisitorBOOL : public UPropertyVisitor
	{
		virtual BOOL Set(void* Target, const char* Src, INT Size)
		{
			assert(Src && Target);
			BOOL* pb = (BOOL*)Target;
			if (strlen(Src) > 1)
			{
				if (stricmp(Src,"TRUE") == 0)
				{
					*pb = 1;
					return TRUE;
				}else if (stricmp(Src, "FALSE") == 0)
				{
					*pb = 0;
					return TRUE;
				}
			}else
			{
				int bv = atoi(Src);
				if (bv == 0)
				{
					*pb = 0;
				}else
				{
					*pb = 1;
				}
				return TRUE;
			}

			return FALSE;
		}
		virtual BOOL Get(char* Target, INT* Size, const void* Src)
		{
			if (Target == NULL && Size != NULL)
			{
				*Size = 2;
				return TRUE;
			}

			BOOL value = *((const BOOL*)Src);
			if (value)
			{
				Target[0] = '1';
			}else
			{
				Target[0] = '0';
			}
			return TRUE;
		}
	} g_PropBOOLVisitor;

	class UPropertyVisitorFloat : public UPropertyVisitor
	{
		virtual BOOL Set(void* Target, const char* Src, INT Size)
		{
			assert(Src && Target);
			float* pf = (float*)Target;
			float value = (float)atof(Src);
			*pf = value;
			return TRUE;
		}
		virtual BOOL Get(char* Target, INT* Size, const void* Src)
		{
			float fValue = *((const float*)Src);
			if (Target == NULL && Size != NULL)
			{
				char Buf[256];
				LSprintf(Buf, 256, "%.6f", fValue);
				*Size = strlen(Buf); 
				return TRUE;
			}

			LSprintf(Target,*Size,"%.6f",fValue);
			return TRUE;
		}
	} g_PropFloatVisitor;

	
	class UPropertyVisitorString : public UPropertyVisitor
	{
		virtual BOOL Set(void* Target, const char* Src, INT Size)
		{
			
			UString* pStr = (UString*)Target;
			if (UTF8CODE)
			{
				std::string str = AnisToUTF8(Src);
				pStr->Set(str.c_str());
			}else
			{
				pStr->Set(Src);
			}
			return TRUE;
		}
		virtual BOOL Get(char* Target, INT* Size, const void* Src)
		{
			const UString* pStr = (const UString*)Src;
			if (Target == NULL && Size != NULL)
			{
				*Size = pStr->GetLength();
				return TRUE;
			}
			memcpy(Target, pStr->GetBuffer(), *Size);		

			return TRUE;
		}
	} g_PropStrVisitor;

	class UPropertyVisitorStringW : public UPropertyVisitor
	{
		virtual BOOL Set(void* Target, const char* Src, INT Size)
		{
			UStringW* pStr = (UStringW*)Target;
			if (UTF8CODE)
			{
				std::string str = AnisToUTF8(Src);
				pStr->Set(str.c_str());
			}else
			{
				pStr->Set(Src);
			}
			return TRUE;
		}
		virtual BOOL Get(char* Target, INT* Size, const void* Src)
		{
			const UStringW* pStr = (const UStringW*)Src;
			if (Target == NULL && Size != NULL)
			{
				*Size = pStr->GetAnsiLength();
				return TRUE;
			}
			memcpy(Target, pStr->GetBuffer(), *Size);		
			pStr->GetAnsi(Target, *Size, 0);
			return TRUE;
		}
	} g_PropStrWVisitor;
	class UPropertyVisitorPoint2 : public UPropertyVisitor
	{
		virtual BOOL Set(void* Target, const char* Src, INT Size)
		{
			UPoint* ppt = (UPoint*)Target;
			int x,y;
			if(sscanf(Src,"%d %d", &x, &y) != 2)
			{
				return FALSE;
			}
			ppt->Set(x,y);
			return TRUE;
		}
		virtual BOOL Get(char* Target, INT* Size, const void* Src)
		{
			const UPoint* ppt = (const UPoint*)Src;
			if (Target == NULL && Size != NULL)
			{
				//char 
				return TRUE;
			}
			//memcpy(Target, pStr->GetBuffer(), *Size);		

			return TRUE;
		}
	}g_PropPointVisitor;

	class UPropertyVisitorRect : public UPropertyVisitor
	{
		virtual BOOL Set(void* Target, const char* Src, INT Size)
		{
			URect* ppt = (URect*)Target;
			int l,t,r,b;
			if(sscanf(Src,"%d %d %d %d", &l, &t, &r, &b) != 4)
			{
				return FALSE;
			}
			ppt->Set(l, t, r-l, b-t);
			return TRUE;
		}
		virtual BOOL Get(char* Target, INT* Size, const void* Src)
		{
			if (Target == NULL && Size != NULL)
			{
				*Size = 48;		// fixed size.
				return TRUE;
			}
			URect* pRect = (URect*)Src;
			LSprintf(Target, *Size, "%d %d %d %d", pRect->left, pRect->top, pRect->right, pRect->bottom);
			return TRUE;
		}
	}g_PropRectVisitor;

	class UPropertyVisitorColor : public UPropertyVisitor
	{
		virtual BOOL Set(void* Target, const char* Src, INT Size)
		{
			UColor* pcl = (UColor*)Target;
			int r,g,b,a = 255;
			int nRet = sscanf(Src,"%d %d %d %d", &r, &g, &b, &a);
			if (nRet < 3 )
			{
				return FALSE;
			}else if (nRet == 3)
			{
				a = 255;
			}
			pcl->Set((BYTE)r, (BYTE)g, (BYTE)b, (BYTE)a);
			return TRUE;
		}
		virtual BOOL Get(char* Target, INT* Size, const void* Src)
		{
			if (Target == NULL && Size != NULL)
			{
				*Size = 32;		// fixed size.
				return TRUE;
			}
			UColor* pcl = (UColor*)Src;
			LSprintf(Target, *Size, "%d %d %d %d", pcl->r, pcl->g, pcl->b, pcl->a);
			return TRUE;
		}
	}g_PropColorVisitor;


	class UPropertyVisitorStyle : public UPropertyVisitor
	{
		virtual BOOL Set(void* Target, const char* Src, INT Size)
		{
			UControlStyle** pstl = (UControlStyle**)Target;
			if(UControl::sm_System == NULL)
			{
				return FALSE;
			}
			UControlStyle* pStyle = UControl::sm_System->FindControlStyle(Src);
			if (pStyle == *pstl)
			{
				return TRUE;
			}

			if (*pstl)
			{
				(*pstl)->Release();
			}
			*pstl = pStyle;
			pStyle->AddRef();
			return TRUE;
		}
		virtual BOOL Get(char* Target, INT* Size, const void* Src)
		{
			if (Target == NULL && Size != NULL)
			{
				UControlStyle** pstl = (UControlStyle**)Src;
				*Size = (*pstl)->m_Name.GetLength() + 1;
				return TRUE;
			}
			UControlStyle** pstl = (UControlStyle**)Src;
			strcpy(Target, (*pstl)->m_Name.GetBuffer());
			return TRUE;
		}
	}g_PropStyleVisitor;


	static UPropertyVisitor* g_PropVisitor[UPT_MAX] =
	{
		&g_PropIntVisitor,	// int
			&g_PropBOOLVisitor,
			&g_PropFloatVisitor,//UPT_FLOAT,
			&g_PropStrVisitor, //UPT_STRING,		// 必须为UString 类型. 不支持char*
			&g_PropStrWVisitor,
			&g_PropPointVisitor,//UPT_POINT,
			&g_PropRectVisitor,//UPT_RECT,
			&g_PropColorVisitor,//UPT_COLOR,
			&g_PropStyleVisitor,	// style
			NULL,//UPT_USER0 = 9,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL
	};
}// namespace

UClass* g_ClassHead = NULL;
typedef std::vector<UProperty> UPropList;


UClass::UClass(UClass* parentCls, const char* name,LTHWNDSTATICINITFN pStaticInit, LFACTORYCREATEWNDFN pCreateFn)
:pParentClass(parentCls),pszClassName(name),pfnStaticInit(pStaticInit),pfnDynCreateObject(pCreateFn)
{
	pFields = NULL;
	uClsId = 0;
	this->pNext = g_ClassHead;
	g_ClassHead = this;
	m_Flags = 0;
}

BOOL UClass::IsBaseOf(const UClass* ChildClass) const
{
	assert(ChildClass);
	const UClass* pClass = ChildClass;
	while (pClass)
	{
		if (pClass == this)
		{
			return TRUE;
		}
		pClass = pClass->pParentClass;
	}
	return FALSE;
}

const UProperty* UClass::FindProperty(const char* name) const
{
	assert(name);
	
	UPropList* pPropList = (UPropList*)pFields;
	if (pPropList && !pPropList->empty())
	{
		for (int i = 0; i < pPropList->size(); i++)
		{
			if (stricmp(name, pPropList->at(i).FieldName) == 0)
			{
				return &pPropList->at(i);
			}
		}
	}
	// 向下搜索
	const UClass* pClass = pParentClass;
	const UProperty* pProp;
	while(pClass)
	{
		pProp = pClass->FindProperty(name);
		if (pProp)
		{
			return pProp;
		}
		pClass = pClass->GetParentClass();
	}
	return NULL;
}

void UClass::StaticInit()
{
	m_Flags = 1;
	pfnStaticInit();
}

UObject* UClass::CreateObject() const
{
	return pfnDynCreateObject();
}

void UClass::UnRegister()
{
	if (pFields)
	{
		UPropList* pPropList = (UPropList*)pFields;
		for (int f = 0; f < pPropList->size(); f++)
		{
			UProperty& pProp = pPropList->at(f);
			if (pProp.FieldName)
			{
				UFree(pProp.FieldName); 
				pProp.FieldName = NULL;
			}
		}
		pPropList->clear();
		delete pFields;
		pFields = NULL;
	}
	m_Flags = 0;
#if 0 
	UClass* pClass = g_ClassHead;
	if (pClass == this)
	{
		g_ClassHead = pNext;
		return;
	}
	while (pClass)
	{
		UClass* pNext = pClass->pNext;
		if (pNext == this)
		{
			pClass->pNext = pNext;
			return;
		}
		pClass = pNext;
	}
#endif 
}

// called by staticinit
void UClass::RegisterField(const char* FieldName, UINT Type, UINT ofs,const char* Comment)
{
	if (m_Flags == 0)	// 没有初始化, 必须是在初始化的时候才能调用这个方法.
	{
		return;
	}
	if (pFields == NULL)
	{
		pFields = new UPropList; // allocate on heap.
	}
	assert(pFields);
	UProperty NewProp;
	memset(&NewProp,0,sizeof(UProperty));
	int len = strlen(FieldName);
	NewProp.FieldName = (char*)UMalloc(len + 1);
	strcpy(NewProp.FieldName,FieldName);
	NewProp.FieldName[len] = '\0';
	NewProp.type = Type;
	NewProp.Offset = ofs;
	((UPropList*)pFields)->push_back(NewProp);
}



UObject::UObject(void)
{
}

UObject::~UObject(void)
{
}

UObject* UObject::CastTo(UObject* pObject, const UClass* pClass)
{
	if (pObject == NULL)
	{
		return NULL;
	}
	const UClass* pThisCls = pObject->GetClass();
	if (pClass->IsBaseOf(pThisCls))
	{
		return pObject;
	}
	return NULL;
}

void UObject::DeleteThis()
{
	delete this;
}

UObject* UObject::CreateObject()
{
	return new UObject;
}
UClass UObject::_DynClass(NULL,"UObject", UObject::StaticInit, UObject::CreateObject);	
void UObject::StaticInit()
{
}
UClass* UObject::GetStaticClass()
{
	return &_DynClass;
}
UClass* UObject::GetParentStaticClass()
{
	return NULL;
}
const UClass* UObject::GetClass() const
{
	return &UObject::_DynClass;
}

BOOL UObject::SetField(const char* fieldName, const char* Value)
{
	// if the code is utf8 and the fieldtype  is Ustring or UstringW the value mast use ansi
	if (!fieldName || !Value)
	{
		return FALSE;
	}
	const UClass* pClass = GetClass();
	const UProperty* pProp = pClass->FindProperty(fieldName);
	if (pProp == NULL)
	{
		UTRACE("field %s is undefined in class %s.", fieldName, pClass->GetClassName());
		return FALSE;
	}
	UPropertyVisitor* pVisitor = g_PropVisitor[pProp->type];
	if (pVisitor == NULL)
	{
		UTRACE("unknown property type.");
		return FALSE;
	}
	
	void* TargetValue = (void*)(((BYTE*)this) + pProp->Offset);
	if(!pVisitor->Set(TargetValue, Value))
	{
		UTRACE("set filed %s = %s failed.", fieldName, Value);
		return FALSE;
	}
	return TRUE;
}

void UObject::RegisterField(UClass* pClass, const char* FieldName, UINT Type, UINT ofs,const char* Comment)
{
	pClass->RegisterField(FieldName, Type, ofs, Comment);
}

BOOL UObject::RegisterPropertyType(UINT Typeid,  UPropertyVisitor* pVisitor)
{
	if (Typeid >= UPT_MAX || Typeid < UPT_USER0)
	{
		return FALSE;
	}
	g_PropVisitor[Typeid] = pVisitor;
	return TRUE;
}
//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////


const UMSGMAP UCmdTarget::cdispmap =
{
	NULL, &UCmdTarget::_cdispentries[0]
};

const UMSGMAP* UCmdTarget::GetMessageMap() const
{
	return &UCmdTarget::cdispmap;
}

const UMSGMAP_ENTRY UCmdTarget::_cdispentries[] =
{
	{ 0, 0, UMD_UNKNOWN, 0, NULL }  
};



UCmdTarget::UCmdTarget(void)
{
}

UCmdTarget::~UCmdTarget(void)
{
}


inline const UMSGMAP_ENTRY* FindMessageEntry(const UMSGMAP_ENTRY* lpEntry,
					UINT nMsgType, UINT nMsgCode, UINT nID)
{
	while (lpEntry->nSig != UMD_UNKNOWN)
	{
		if (lpEntry->nMsg == nMsgType && lpEntry->nCode == nMsgCode &&
			nID == lpEntry->nID)
		{
			return lpEntry;
		}
		lpEntry++;
	}
	return NULL;
}


union MessageMapFuncs
{
	UNOTIFY_METHOD pfn;   // generic member function pointer
	void (UCmdTarget::*pfnCmd_v_v)();  // void Func(void)
	void (UCmdTarget::*pfnCmd_v_unm)(UNM* pNM);
/*
	BOOL (AFX_MSG_CALL CCmdTarget::*pfn_b_b)(BOOL);
	BOOL (AFX_MSG_CALL CCmdTarget::*pfn_b_u)(UINT);
	BOOL (AFX_MSG_CALL CCmdTarget::*pfn_b_h)(HANDLE);
	BOOL (AFX_MSG_CALL CCmdTarget::*pfn_b_W_u_u)(CWnd*, UINT, UINT);
	BOOL (AFX_MSG_CALL CCmdTarget::*pfn_b_W_COPYDATASTRUCT)(CWnd*, COPYDATASTRUCT*);
	BOOL (AFX_MSG_CALL CCmdTarget::*pfn_b_HELPINFO)(LPHELPINFO);
	HBRUSH (AFX_MSG_CALL CCmdTarget::*pfn_B_D_W_u)(CDC*, CWnd*, UINT);
	HBRUSH (AFX_MSG_CALL CCmdTarget::*pfn_B_D_u)(CDC*, UINT);
	int (AFX_MSG_CALL CCmdTarget::*pfn_i_u_W_u)(UINT, CWnd*, UINT);
	int (AFX_MSG_CALL CCmdTarget::*pfn_i_u_u)(UINT, UINT);
	int (AFX_MSG_CALL CCmdTarget::*pfn_i_W_u_u)(CWnd*, UINT, UINT);
	int (AFX_MSG_CALL CWnd::*pfn_i_s)(LPTSTR);
	LRESULT (AFX_MSG_CALL CWnd::*pfn_l_w_l)(WPARAM, LPARAM);
	LRESULT (AFX_MSG_CALL CWnd::*pfn_l_u_u_M)(UINT, UINT, CMenu*);
	void (AFX_MSG_CALL CWnd::*pfn_v_v)();
	int (AFX_MSG_CALL CWnd::*pfn_i_u)(UINT);
	HCURSOR (AFX_MSG_CALL CWnd::*pfn_C_v)();
	UINT (AFX_MSG_CALL CWnd::*pfn_u_u)(UINT);
	BOOL (AFX_MSG_CALL CWnd::*pfn_b_v)();
	void (AFX_MSG_CALL CWnd::*pfn_v_u)(UINT);
	void (AFX_MSG_CALL CWnd::*pfn_v_u_u)(UINT, UINT);
	void (AFX_MSG_CALL CWnd::*pfn_v_i_i)(int, int);
	void (AFX_MSG_CALL CWnd::*pfn_v_u_u_u)(UINT, UINT, UINT);
	void (AFX_MSG_CALL CWnd::*pfn_v_u_i_i)(UINT, int, int);
	void (AFX_MSG_CALL CWnd::*pfn_v_w_l)(WPARAM, LPARAM);
	void (AFX_MSG_CALL CWnd::*pfn_v_b_W_W)(BOOL, CWnd*, CWnd*);
	void (AFX_MSG_CALL CWnd::*pfn_v_D)(CDC*);
	void (AFX_MSG_CALL CWnd::*pfn_v_M)(CMenu*);
	void (AFX_MSG_CALL CWnd::*pfn_v_M_u_b)(CMenu*, UINT, BOOL);
	void (AFX_MSG_CALL CWnd::*pfn_v_W)(CWnd*);
	void (AFX_MSG_CALL CWnd::*pfn_v_W_u_u)(CWnd*, UINT, UINT);
	void (AFX_MSG_CALL CWnd::*pfn_v_W_p)(CWnd*, CPoint);
	void (AFX_MSG_CALL CWnd::*pfn_v_W_h)(CWnd*, HANDLE);
	void (AFX_MSG_CALL CWnd::*pfn_v_u_W)(UINT, CWnd*);
	void (AFX_MSG_CALL CWnd::*pfn_v_u_W_b)(UINT, CWnd*, BOOL);
	void (AFX_MSG_CALL CWnd::*pfn_v_u_u_W)(UINT, UINT, CWnd*);
	void (AFX_MSG_CALL CWnd::*pfn_v_s)(LPTSTR);
	void (AFX_MSG_CALL CWnd::*pfn_v_u_cs)(UINT, LPCTSTR);
	void (AFX_MSG_CALL CWnd::*pfn_v_i_s)(int, LPTSTR);
	int (AFX_MSG_CALL CWnd::*pfn_i_i_s)(int, LPTSTR);
	UINT (AFX_MSG_CALL CWnd::*pfn_u_p)(CPoint);
	UINT (AFX_MSG_CALL CWnd::*pfn_u_v)();
	void (AFX_MSG_CALL CWnd::*pfn_v_b_NCCALCSIZEPARAMS)(BOOL, NCCALCSIZE_PARAMS*);
	void (AFX_MSG_CALL CWnd::*pfn_v_v_WINDOWPOS)(WINDOWPOS*);
	void (AFX_MSG_CALL CWnd::*pfn_v_u_u_M)(UINT, UINT, HMENU);
	void (AFX_MSG_CALL CWnd::*pfn_v_u_p)(UINT, CPoint);
	void (AFX_MSG_CALL CWnd::*pfn_v_u_pr)(UINT, LPRECT);
	BOOL (AFX_MSG_CALL CWnd::*pfn_b_u_s_p)(UINT, short, CPoint);
	LRESULT (AFX_MSG_CALL CWnd::*pfn_l_v)();
	void (AFX_MSG_CALL CCmdTarget::*pfnCmd_v_v)();
	BOOL (AFX_MSG_CALL CCmdTarget::*pfnCmd_b_v)();
	void (AFX_MSG_CALL CCmdTarget::*pfnCmd_v_u)(UINT);
	BOOL (AFX_MSG_CALL CCmdTarget::*pfnCmd_b_u)(UINT);

	void (AFX_MSG_CALL CCmdTarget::*pfnNotify_v_NMHDR_pl)(NMHDR*, LRESULT*);
	BOOL (AFX_MSG_CALL CCmdTarget::*pfnNotify_b_NMHDR_pl)(NMHDR*, LRESULT*);
	void (AFX_MSG_CALL CCmdTarget::*pfnNotify_v_u_NMHDR_pl)(UINT, NMHDR*, LRESULT*);
	BOOL (AFX_MSG_CALL CCmdTarget::*pfnNotify_b_u_NMHDR_pl)(UINT, NMHDR*, LRESULT*);
	void (AFX_MSG_CALL CCmdTarget::*pfnCmdUI_v_C)(CCmdUI*);
	void (AFX_MSG_CALL CCmdTarget::*pfnCmdUI_v_C_u)(CCmdUI*, UINT);

	void (AFX_MSG_CALL CCmdTarget::*pfnCmd_v_pv)(void*);
	BOOL (AFX_MSG_CALL CCmdTarget::*pfnCmd_b_pv)(void*);

	void    (AFX_MSG_CALL CWnd::*pfn_vPOS)(WINDOWPOS*);
	void    (AFX_MSG_CALL CWnd::*pfn_vCALC)(BOOL, NCCALCSIZE_PARAMS*);
	void    (AFX_MSG_CALL CWnd::*pfn_vwp)(UINT, CPoint);
	void    (AFX_MSG_CALL CWnd::*pfn_vwwh)(UINT, UINT, HANDLE);
	BOOL    (AFX_MSG_CALL CWnd::*pfn_bwsp)(UINT, short, CPoint);
*/
};

static inline BOOL DispatchCmdMsg(UCmdTarget* pTarget, UINT nID, int nCode,
										  UNOTIFY_METHOD pfn, const void* pExtra, UINT nSig)
										  // return TRUE to stop routing
{
	assert(pTarget);
	union MessageMapFuncs mmf;
	mmf.pfn = pfn;
	BOOL bResult = TRUE; // default is ok

	
	switch (nSig)
	{
	default:    // illegal
		assert(FALSE);
		return 0;
		break;

	case UMDCMD_V:
		(pTarget->*mmf.pfnCmd_v_v)();
		break;
	case UMDNOTIFY_V_U:
		(pTarget->*mmf.pfnCmd_v_unm)((UNM*)pExtra);
	}
	return bResult;
}


BOOL UCmdTarget::ReceiveCmdNotify(UINT cmd, UINT id, const void* pExtra)
{
	const UMSGMAP_ENTRY* lpEntry;
	// 遍历所有映射
	const UMSGMAP* pMessageMap = NULL;
	for (pMessageMap = GetMessageMap(); pMessageMap != NULL;
		pMessageMap = pMessageMap->pBaseMap)
	{
		lpEntry = FindMessageEntry(pMessageMap->lpEntries, UMSG_COMMAND, cmd, id);
		if (lpEntry != NULL)
		{
			return DispatchCmdMsg(this, id, cmd, lpEntry->pfn, pExtra, lpEntry->nSig);
		}
	}

	return FALSE;
}
/*
 TODO : 记得回头来看这里:
 目前我们的窗口可以随意组合成父子关系, 比如存在这样的父子关系

 Dialog <- Bitmap <- Button
 Dialog 是我们的消息响应者, Button 的消息必须要能分发到Dialog 中.
 如果按照WIN32的做法, 这种困惑是不存在的, 但是上面说了,我们可以随意
 组合父子关系, 那么这种情况就出来了. 因此,在下面的处理上, 我们不停地
 向上遍历.一旦找到处理路由, 则退出分发.

 还存在一种解决方法, 那就是我们为每个CmdTarget 保留一个侦听链表, 这个必然
 是动态的, 在运行期, 我们通过在Button 上 注册一个侦听的对象, 当Button 开发
 分发消息时会去检测是否具备相应的侦听者,然后传达给它.
 这种做法要灵活很多, 但是也有弊端, 那就是我们需要在运行期确定. 而且每一个对象
 都将维护这样一个链表,幸运的是多数对象的链表是空的.

 我们需要在后期来进行评估孰优孰劣. 
*/
void UCmdTarget::DispatchNotify(UINT NotifyMsg)
{
	UControl* pSender = (UControl*)this;
	UINT ID = pSender->GetId();
	UControl* pTarget = pSender->GetParent();
	while (pTarget && !pTarget->IsDesktop())
	{
		if (pTarget->ReceiveCmdNotify(NotifyMsg, ID, NULL))
		{
			return;
		}
		pTarget = pTarget->GetParent();
	}
}

void UCmdTarget::DispatchNotify(UINT NotifyMsg, const UNM* pNM)
{
	UControl* pSender = (UControl*)this;
	UINT ID = pSender->GetId();
	UControl* pTarget = pSender->GetParent();
	while (pTarget && !pTarget->IsDesktop())
	{
		if (pTarget->ReceiveCmdNotify(NotifyMsg, ID, pNM))
		{
			return;
		}
		pTarget = pTarget->GetParent();
	}
}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////


UCursor::UCursor()
{
	mHotSpot.Set(0,0);
	mRenderOffSet.Set(0.0f,0.0f);
	mExtent.Set(1,1);
	mTextureObject = NULL;
}

UCursor::~UCursor()
{
}

void UCursor::initPersistFields()
{
	/* Parent::initPersistFields();
	//RegisterField("hotSpot",     UPT_POINT,   UFIELD_OFFSET(mHotSpot, UCursor));
	//RegisterField("renderOffSet",LPROPTYPE_POINT2F,   UFIELD_OFFSET(mRenderOffSet, UCursor));
	//RegisterField("bitmapName",  UPT_STRING,  UFIELD_OFFSET(m_strIconFile, UCursor));*/
}

BOOL UCursor::OnAdd()
{
	//if(!Parent::OnAdd())
	//   return FALSE;

	//Sim::GetGuiDataGroup()->AddChild(this);
	// 注册到GUI系统中
	return TRUE;
}

void UCursor::OnRemove()
{

}

void UCursor::render(const UPoint &pos)
{
	//if (!mTextureObject && m_strIconFile && m_strIconFile[0])
	//{
	//   mTextureObject.Set( m_strIconFile, &GFXGuiCursorProfile);
	//   if(!mTextureObject)
	//      return;
	//   mExtent.Set(mTextureObject->GetWidth(), mTextureObject->GetHeight());
	//}

	//// Render the cursor centered according to dimensiOns of texture
	//INT texWidth = mTextureObject.GetWidth();
	//INT texHeight = mTextureObject.GetHeight();

	//UPoint renderPos = pos;
	//renderPos.x -= ( texWidth  * mRenderOffSet.x );
	//renderPos.y -= ( texHeight * mRenderOffSet.y );

	//sm_UiRender->clearBitmapModulatiOn();
	//sm_UiRender->DrawImage(mTextureObject, renderPos);
}


UIMP_CLASS(UControlStyle,UObject);
//
//static EnumTable::Enums alignEnums[] =
//{
//   { UControlStyle::UT_LEFT,          "left"      },
//   { UControlStyle::UT_CENTER,        "center"    },
//   { UControlStyle::UT_RIGHT,         "right"     }
//};
//static EnumTable gAlignTable(3, &alignEnums[0]);
//
//static EnumTable::Enums charSetEnums[]=
//{
//    { TGE_ANSI_CHARSET,         "ANSI" },
//    { TGE_SYMBOL_CHARSET,       "SYMBOL" },
//    { TGE_SHIFTJIS_CHARSET,     "SHIFTJIS" },
//    { TGE_HANGEUL_CHARSET,      "HANGEUL" },
//    { TGE_HANGUL_CHARSET,       "HANGUL" },
//    { TGE_GB2312_CHARSET,       "GB2312" },
//    { TGE_CHINESEBIG5_CHARSET,  "CHINESEBIG5" },
//    { TGE_OEM_CHARSET,          "OEM" },
//    { TGE_JOHAB_CHARSET,        "JOHAB" },
//    { TGE_HEBREW_CHARSET,       "HEBREW" },
//    { TGE_ARABIC_CHARSET,       "ARABIC" },
//    { TGE_GREEK_CHARSET,        "GREEK" },
//    { TGE_TURKISH_CHARSET,      "TURKISH" },
//    { TGE_VIETNAMESE_CHARSET,   "VIETNAMESE" },
//    { TGE_THAI_CHARSET,         "THAI" },
//    { TGE_EASTEUROPE_CHARSET,   "EASTEUROPE" },
//    { TGE_RUSSIAN_CHARSET,      "RUSSIAN" },
//    { TGE_MAC_CHARSET,          "MAC" },
//    { TGE_BALTIC_CHARSET,       "BALTIC" },
//};
//
//#define NUMCHARSETENUMS     (sizeof(charSetEnums) / sizeof(EnumTable::Enums))
//
//static EnumTable gCharSetTable(NUMCHARSETENUMS, &charSetEnums[0]);

UControlStyle::UControlStyle()
{
	m_nRefCnt = 0;
	
	mBorderThickness = 1;
	mMouseOverSelected = FALSE;
	
	//m_FontCharSet   = TGE_ANSI_CHARSET;

	UControlStyle *def = g_USystem->FindControlStyle("default");//dynamic_cast<UControlStyle*>(Sim::findObject("GuiDefaultProfile"));
	
	if (def)
	{
		m_bTabStop       = def->m_bTabStop;
		m_bKeyFocus   = def->m_bKeyFocus;

		mOpaque        = def->mOpaque;
		mFillColor     = def->mFillColor;
		mFillColorHL   = def->mFillColorHL;
		mFillColorNA   = def->mFillColorNA;

		mBorder        = def->mBorder;
		mBorderThickness = def->mBorderThickness;
		mBorderColor   = def->mBorderColor;
		mBorderColorHL = def->mBorderColorHL;
		mBorderColorNA = def->mBorderColorNA;

		mBevelColorHL = def->mBevelColorHL;
		mBevelColorLL = def->mBevelColorLL;

		// default Font
		m_FontFace.Set(def->m_FontFace.GetBuffer());
		m_FontSize      = def->m_FontSize;
		m_FontCharSet   = def->m_FontCharSet;

		//for(UINT i = 0; i < 10; i++)
		//	m_FontColors[i] = def->m_FontColors[i];
		m_FontColor = def->m_FontColor;
		m_FontColorHL = def->m_FontColorHL;
		m_FontColorSEL = def->m_FontColorSEL;
		m_FontColorNA = def->m_FontColorNA;
		m_FontColorUser0 = def->m_FontColorUser0;
		m_FontColorUser1 = def->m_FontColorUser1;
		// default bitmap
		strSkinName.Set(def->strSkinName.GetBuffer());
		mTextOffset    = def->mTextOffset;

		// default sound
		mSoundButtOnDown = def->mSoundButtOnDown;
		mSoundButtOnOver = def->mSoundButtOnOver;

		//used by GuiTextCtrl
		mModal         = def->mModal;
		mAlignment     = def->mAlignment;
		mAutoSizeWidth = def->mAutoSizeWidth;
		mAutoSizeHeight= def->mAutoSizeHeight;
		mReturnTab     = def->mReturnTab;
		mNumbersOnly   = def->mNumbersOnly;
		mCursorColor   = def->mCursorColor;
		m_bHandleDragDrop = def->m_bHandleDragDrop;
	}else
	{
		mModal = FALSE;
		mNumbersOnly = FALSE;
		m_bHandleDragDrop = FALSE;
	}
}

UControlStyle::~UControlStyle()
{

}


void UControlStyle::StaticInit()
{
	//   Parent::initPersistFields();

	UREG_PROPERTY("tab",           UPT_BOOL,       UFIELD_OFFSET( UControlStyle, m_bTabStop));
	UREG_PROPERTY("canKeyFocus",   UPT_BOOL,       UFIELD_OFFSET(UControlStyle, m_bKeyFocus));
	UREG_PROPERTY("dragdrop", UPT_BOOL,       UFIELD_OFFSET( UControlStyle, m_bHandleDragDrop));
	UREG_PROPERTY("mouseOverSelected", UPT_BOOL,   UFIELD_OFFSET(UControlStyle, mMouseOverSelected));

	UREG_PROPERTY("modal",         UPT_BOOL,       UFIELD_OFFSET(UControlStyle, mModal));
	UREG_PROPERTY("opaque",        UPT_BOOL,       UFIELD_OFFSET(UControlStyle, mOpaque));
	UREG_PROPERTY("fillColor",     UPT_COLOR,     UFIELD_OFFSET(UControlStyle, mFillColor));
	UREG_PROPERTY("fillColorHL",   UPT_COLOR,     UFIELD_OFFSET(UControlStyle, mFillColorHL));
	UREG_PROPERTY("fillColorNA",   UPT_COLOR,     UFIELD_OFFSET(UControlStyle, mFillColorNA));
	
	UREG_PROPERTY("border",        UPT_INT,        UFIELD_OFFSET(UControlStyle, mBorder));
	UREG_PROPERTY("borderThickness",UPT_INT,       UFIELD_OFFSET(UControlStyle, mBorderThickness));
	UREG_PROPERTY("borderColor",   UPT_COLOR,     UFIELD_OFFSET(UControlStyle, mBorderColor));
	UREG_PROPERTY("borderColorHL", UPT_COLOR,     UFIELD_OFFSET(UControlStyle, mBorderColorHL));
	UREG_PROPERTY("borderColorNA", UPT_COLOR,     UFIELD_OFFSET(UControlStyle, mBorderColorNA));

	UREG_PROPERTY("bevelColorHL", UPT_COLOR,     UFIELD_OFFSET(UControlStyle, mBevelColorHL));
	UREG_PROPERTY("bevelColorLL", UPT_COLOR,     UFIELD_OFFSET(UControlStyle, mBevelColorLL));

	UREG_PROPERTY("FontType",      UPT_STRING,     UFIELD_OFFSET(UControlStyle, m_FontFace));
	UREG_PROPERTY("FontSize",      UPT_INT,        UFIELD_OFFSET(UControlStyle, m_FontSize));
	UREG_PROPERTY("FontCharSet",   UPT_INT,       UFIELD_OFFSET(UControlStyle, m_FontCharSet));
	// UREG_PROPERTY("FontColors",    UPT_COLOR,     UFIELD_OFFSET(UControlStyle, m_FontColors), 10);
	UREG_PROPERTY("FontColor",     UPT_COLOR,     UFIELD_OFFSET(UControlStyle, m_FontColor));
	UREG_PROPERTY("FontColorHL",   UPT_COLOR,     UFIELD_OFFSET(UControlStyle, m_FontColorHL));
	UREG_PROPERTY("FontColorNA",   UPT_COLOR,     UFIELD_OFFSET(UControlStyle, m_FontColorNA));
	UREG_PROPERTY("FontColorSEL",  UPT_COLOR,     UFIELD_OFFSET(UControlStyle, m_FontColorSEL));
	UREG_PROPERTY("FontColorUsr0", UPT_COLOR,     UFIELD_OFFSET(UControlStyle, m_FontColorUser0));
	UREG_PROPERTY("FontColorUsr1", UPT_COLOR,     UFIELD_OFFSET(UControlStyle, m_FontColorUser1));

	UREG_PROPERTY("justify",       UPT_INT,       UFIELD_OFFSET(UControlStyle, mAlignment));
	UREG_PROPERTY("textOffSet",    UPT_POINT,    UFIELD_OFFSET(UControlStyle, mTextOffset));
	UREG_PROPERTY("autoSizeWidth", UPT_BOOL,       UFIELD_OFFSET(UControlStyle, mAutoSizeWidth));
	UREG_PROPERTY("autoSizeHeight",UPT_BOOL,       UFIELD_OFFSET(UControlStyle, mAutoSizeHeight));
	UREG_PROPERTY("returnTab",     UPT_BOOL,       UFIELD_OFFSET(UControlStyle, mReturnTab));
	UREG_PROPERTY("numbersOnly",   UPT_BOOL,       UFIELD_OFFSET(UControlStyle, mNumbersOnly));
	UREG_PROPERTY("cursorColor",   UPT_COLOR,     UFIELD_OFFSET(UControlStyle, mCursorColor));

	UREG_PROPERTY("skin",        UPT_STRING,   UFIELD_OFFSET(UControlStyle, strSkinName));
	UREG_PROPERTY("soundDown", UPT_STRING,  UFIELD_OFFSET(UControlStyle, mSoundButtOnDown));
	UREG_PROPERTY("soundOver", UPT_STRING,  UFIELD_OFFSET(UControlStyle, mSoundButtOnOver));
	UREG_PROPERTY("soundClose", UPT_STRING,  UFIELD_OFFSET(UControlStyle, mSoundClose));
	UREG_PROPERTY("soundOpen", UPT_STRING,  UFIELD_OFFSET(UControlStyle, mSoundOpen));
}

BOOL UControlStyle::Create()
{

	//Sim::GetGuiDataGroup()->AddChild(this);
	// 注册到GUI 系统中
	return TRUE;
}


void UControlStyle::AddRef()
{
	assert(UControl::sm_System);
	if(!m_nRefCnt++)
	{
		m_spFont = UControl::sm_UiRender->CreateFont(m_FontFace.GetBuffer(), m_FontSize, m_FontCharSet);
		if (m_spFont == NULL)
		{
			// error
		}
		
		if (strSkinName.GetBuffer())
		{
			m_spSkin = UControl::sm_System->LoadSkin(strSkinName.GetBuffer());
		}
	}
}

// 我们只在这里处理样式所携带的资源,在引用为0 的时候,销毁所有资源, 但保留类样式本身, 因为将来还有用.
void UControlStyle::Release()
{
	//AssertFatal(m_nRefCnt, "UControlStyle::Release: zero ref count");
	if(!m_nRefCnt)
		return;

	if(!--m_nRefCnt)
	{
		m_spFont = NULL;
		m_spSkin = NULL;
	}
	// 不要调用 delete this! 我们将会在程序退出的时候自动销毁.
}

UIMP_CLASS(UAccelarKey,UObject);
void UAccelarKey::StaticInit()
{
	UREG_PROPERTY("ActionCode", UPT_STRING,  UFIELD_OFFSET(UAccelarKey, mActionName));//<property ActionCode = "OpCode">;
	UREG_PROPERTY("KeyCode", UPT_STRING,  UFIELD_OFFSET(UAccelarKey, mAccelarKeyCode));//<property AccelarKeyCode = "KeyCode">;
	UREG_PROPERTY("KeyModify",UPT_STRING, UFIELD_OFFSET(UAccelarKey,mAccelarKeyModify));//<property AccelarKeyModify = "RCONTROL+RALT+RSHIFT">;
	//UREG_PROPERTY("KeyState", UPT_BOOL , UFIELD_OFFSET(UAccelarKey,mKeyState));//<property KeyState = "TRUE">;
}
UAccelarKey::UAccelarKey()
{
	//mKeyState = TRUE;
}
UAccelarKey::~UAccelarKey()
{

}
//colortable
UIMP_CLASS(UColorTable,UObject);
void UColorTable::StaticInit()
{
	UREG_PROPERTY("ColorDesc", UPT_STRING, UFIELD_OFFSET(UColorTable, mColorDesc));
	UREG_PROPERTY("Color", UPT_COLOR, UFIELD_OFFSET(UColorTable, mColor));
}
UColorTable::UColorTable()
{

}
UColorTable::~UColorTable()
{

}
void ULog(const char* fmt, ...)
{
#ifndef NDEBUG
	char Buf[1032];
	strcpy(Buf,"<UI>:");
	char* pBuf = Buf +5;
	va_list args;
	va_start( args, fmt );
	int len = _vsnprintf(pBuf, 1024, fmt, args);
	va_end(args);
	pBuf+= len;
	*pBuf++ = '\n';
	*pBuf = '\0';
	OutputDebugString(Buf);
#endif
}

void UError(const char* fmt, ...)
{
#ifndef NDEBUG
	char Buf[1032];
	strcpy(Buf,"<UI>:");
	char* pBuf = Buf +5;
	va_list args;
	va_start( args, fmt );
	int len = _vsnprintf(pBuf, 1024, fmt, args);
	va_end(args);
	pBuf+= len;
	*pBuf++ = '\n';
	*pBuf = '\0';
	OutputDebugString(Buf);
	//RaiseException();
	DebugBreak();
#endif
}