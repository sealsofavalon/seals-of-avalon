#include "StdAfx.h"
#include "UBitMapListBox.h"
#include "USystem.h"
#include "UScrollBar.h"
UIMP_CLASS(UBitMapListBox,UControl);
UBEGIN_MESSAGE_MAP(UBitMapListBox,UControl)
UEND_MESSAGE_MAP();
struct BitMapListBoxItem 
{
	BitMapListBoxItem():UserData(NULL)
	{
		mAlignment = UT_LEFT;
		text[0] = 0;
		bEnable = TRUE;
		bSelect = FALSE;
		bMouseOver = FALSE;
		Position.Set(0,0);
		Size.Set(0,0);
		spFont = NULL;
		IsLine = FALSE;
		IsTitle = FALSE;
		spSkin = NULL;
		uiSkinIndex = 0;
	}
	WCHAR text[UBitMapListBox::ITEM_TEXT_MAX + 4];
	void* UserData;

	UINT mAlignment;
	UFontPtr spFont;
	BOOL bEnable;
	BOOL bSelect;
	BOOL bMouseOver;
	BOOL IsLine;
	BOOL IsTitle;
	UPoint Position;
	UPoint Size;
	UColor FontColor;

	USkinPtr spSkin;
	UINT uiSkinIndex;
};

struct BitMapListBoxContext
{
	BitMapListBoxContext():SelectItem(NULL),MouseOverItem(NULL),SelIndex(-1){};
	std::vector<BitMapListBoxItem> Items ;
	BitMapListBoxItem * SelectItem;
	BitMapListBoxItem * MouseOverItem;
	int          SelIndex;

	inline int GetSize() const 
	{
		return (int)Items.size();
	}
	inline void SetSelect(int index){
		ClearSelect();
		if(index >= 0){
			SelIndex = index;
			SelectItem = &Items[index];
			if (SelectItem)
				SelectItem->bSelect = TRUE;
		}
	}
	inline void ClearSelect(){	
		if (SelectItem)
			SelectItem->bSelect = FALSE;
		SelectItem = NULL;
		SelIndex = -1;
	}
	inline void SetMouseOver(int index){
		ClearMouseOver();
		MouseOverItem = &Items[index];
		if (MouseOverItem)
			MouseOverItem->bMouseOver = TRUE;
	}
	inline void ClearMouseOver(){
		if(MouseOverItem)
			MouseOverItem->bMouseOver = FALSE;
		MouseOverItem = NULL;
	}
	BitMapListBoxItem* GetListItem(int index){
		return &Items[index];
	}
	inline int GetCurSel(){
		return SelIndex;
	}
};
static const URect WinRectToScreenRect(URect childRect, URect parantRect)
{
	UPoint NewPos;
	UPoint NewSize;

	NewPos.x = parantRect.GetPosition().x + childRect.GetPosition().x;
	NewPos.y = parantRect.GetPosition().y + childRect.GetPosition().y;

	NewSize = childRect.GetSize();

	return URect(NewPos,NewSize);
}
void UBitMapListBox::StaticInit()
{
	UREG_PROPERTY("ListBKGSkin",UPT_STRING, UFIELD_OFFSET(UBitMapListBox,m_BkgSkinFileName));
}
UBitMapListBox::UBitMapListBox(void)
{
	m_BkgSkin = NULL;
	for (int i = 0 ; i < RECT_MAX ; i++)
	{
		m_SkinRect[i].Set(0,0,0,0);
	}
	m_pListBoxCtx = new BitMapListBoxContext;
	assert(m_pListBoxCtx);
	m_BkgSkinFileName.Set("Style\\ComBoxBG.skin");
	m_pScrollBar = NULL;
}

UBitMapListBox::~UBitMapListBox(void)
{
	m_BkgSkin = NULL;
	if (m_pListBoxCtx)
	{
		delete m_pListBoxCtx;
	}
}

BOOL UBitMapListBox::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_BkgSkin == NULL && m_BkgSkinFileName.GetBuffer())
	{
		m_BkgSkin = sm_System->LoadSkin(m_BkgSkinFileName.GetBuffer());
	}
	if (m_BkgSkin == NULL)
	{
		m_BkgSkin = sm_System->LoadSkin("Style\\ComBoxBG.skin");
	}
	m_ItemSize.Set(0,m_Style->m_spFont->GetHeight() + 2);
	m_pScrollBar = UDynamicCast(UScrollBar, GetParent());
	CalItemRect(m_Size);
	return TRUE;
}
void UBitMapListBox::OnDestroy()
{
	return UControl::OnDestroy();
}
void UBitMapListBox::OnSize(const UPoint& NewSize)
{
	return CalItemRect(NewSize);
}
void UBitMapListBox::OnMove(const UPoint& NewOffsetDelta)
{
	//CalItemRect(GetWindowSize());
	return UControl::OnMove(NewOffsetDelta);
}
void UBitMapListBox::OnRender(const UPoint& offset,const URect &updateRect)
{
    g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
    g_pRenderInterface->SetScissorRect(sm_UiRender->GetClipRect());

	if (m_BkgSkin && m_BkgSkin->GetItemNum() == RECT_MAX)
	{
		if (m_pScrollBar)
		{
			UDrawResizeImage(RESIZE_WANDH, sm_UiRender, m_BkgSkin, m_pScrollBar->GetWindowPos(), updateRect.GetSize());
			CalItemRect(updateRect.GetSize());
		}
		else
		{
			UDrawResizeImage(RESIZE_WANDH, sm_UiRender, m_BkgSkin, offset, updateRect.GetSize());
		}

	}
	for (int i = 0 ; i < m_pListBoxCtx->Items.size() ; i++)
	{
		DrawChildItem(i,offset);
	}

    g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
}
void UBitMapListBox::GetScrollLineSizes(UINT *rowHeight, UINT *columnWidth)
{
	*rowHeight = m_ItemSize.y;
	*columnWidth = 30;
}
BOOL UBitMapListBox::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	return TRUE;
}

void UBitMapListBox::OnMouseMove(const UPoint& position, UINT flags)
{
	UPoint _pos = ScreenToWindow(position);
	for (size_t i = 0 ; i < m_pListBoxCtx->Items.size() ; i++)
	{
		if (m_pScrollBar)
		{
			UPoint pos_pos = m_pListBoxCtx->Items[i].Position + m_SkinRect[RECT_MIDDLE].GetPosition();
			UPoint tran_pos1 = WindowToScreen(pos_pos);
			UPoint tran_pos2 = m_pScrollBar->ScreenToWindow(tran_pos1);
			UPoint tran_posBottom;
			tran_posBottom.x = tran_pos2.x;
			tran_posBottom.y = tran_pos2.y + m_ItemSize.y;
			URect cliprect( pos_pos, m_ItemSize);
			if (cliprect.PointInRect(_pos) /*&& m_SkinRect[RECT_MIDDLE].PointInRect(tran_pos2) && 
				m_SkinRect[RECT_MIDDLE].PointInRect(tran_posBottom)*/)
			{
				if(m_pListBoxCtx->Items[i].bEnable && !m_pListBoxCtx->Items[i].IsLine && !m_pListBoxCtx->Items[i].IsTitle)
					m_pListBoxCtx->SetMouseOver(i);
				else
					m_pListBoxCtx->ClearMouseOver();
				return;
			}
		}
		else
		{
			URect rect(m_pListBoxCtx->Items[i].Position + m_SkinRect[RECT_MIDDLE].GetPosition(),m_ItemSize);
			if (rect.PointInRect(_pos))
			{
				if(m_pListBoxCtx->Items[i].bEnable && !m_pListBoxCtx->Items[i].IsLine && !m_pListBoxCtx->Items[i].IsTitle)
					m_pListBoxCtx->SetMouseOver(i);
				else
					m_pListBoxCtx->ClearMouseOver();
				return;
			}
		}
	}
}
void UBitMapListBox::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	UPoint _pos = ScreenToWindow(point);
	for (size_t i = 0 ; i < m_pListBoxCtx->Items.size() ; i++)
	{
		if (m_pScrollBar)
		{
			UPoint pos_pos = m_pListBoxCtx->Items[i].Position + m_SkinRect[RECT_MIDDLE].GetPosition();
			UPoint tran_pos1 = WindowToScreen(pos_pos);
			UPoint tran_pos2 = m_pScrollBar->ScreenToWindow(tran_pos1);
			UPoint tran_posBottom;
			tran_posBottom.x = tran_pos2.x;
			tran_posBottom.y = tran_pos2.y + m_ItemSize.y;
			URect cliprect( pos_pos, m_ItemSize);
			if (cliprect.PointInRect(_pos) /*&& m_SkinRect[RECT_MIDDLE].PointInRect(tran_pos2) && 
				m_SkinRect[RECT_MIDDLE].PointInRect(tran_posBottom)*/)
			{
				if(m_pListBoxCtx->Items[i].bEnable && !m_pListBoxCtx->Items[i].IsLine && !m_pListBoxCtx->Items[i].IsTitle)
					SetCurSel(i);
				return;
			}
		}
		else
		{
			URect rect(m_pListBoxCtx->Items[i].Position + m_SkinRect[RECT_MIDDLE].GetPosition(),m_ItemSize);
			if (rect.PointInRect(_pos))
			{
				if(m_pListBoxCtx->Items[i].bEnable && !m_pListBoxCtx->Items[i].IsLine && !m_pListBoxCtx->Items[i].IsTitle)
					SetCurSel(i);
				return;
			}
		}
	}
}
void UBitMapListBox::OnMouseUp(const UPoint& position, UINT flags)
{
	if(m_pListBoxCtx->SelectItem )
	{		
		if(m_pListBoxCtx->SelectItem->bEnable)
			DispatchNotify(UBMLB_SELECT);
	}
	else
	{
		SetCurSel(-1);
		DispatchNotify(UBMLB_SELECT);
	}

	//分派消息

}

void UBitMapListBox::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	UPoint _pos = ScreenToWindow(position);
	for (size_t i = 0 ; i < m_pListBoxCtx->Items.size() ; i++)
	{
		if (m_pScrollBar)
		{
			UPoint pos_pos = m_pListBoxCtx->Items[i].Position + m_SkinRect[RECT_MIDDLE].GetPosition();
			UPoint tran_pos1 = WindowToScreen(pos_pos);
			UPoint tran_pos2 = m_pScrollBar->ScreenToWindow(tran_pos1);
			UPoint tran_posBottom;
			tran_posBottom.x = tran_pos2.x;
			tran_posBottom.y = tran_pos2.y + m_ItemSize.y;
			URect cliprect( pos_pos, m_ItemSize);
			if (cliprect.PointInRect(_pos) && m_SkinRect[RECT_MIDDLE].PointInRect(tran_pos2) && 
				m_SkinRect[RECT_MIDDLE].PointInRect(tran_posBottom))
			{
				if(m_pListBoxCtx->Items[i].bEnable && !m_pListBoxCtx->Items[i].IsLine && !m_pListBoxCtx->Items[i].IsTitle)
					SetCurSel(i);
				return;
			}
		}
		else
		{
			URect rect(m_pListBoxCtx->Items[i].Position + m_SkinRect[RECT_MIDDLE].GetPosition(),m_ItemSize);
			if (rect.PointInRect(_pos))
			{
				if(m_pListBoxCtx->Items[i].bEnable && !m_pListBoxCtx->Items[i].IsLine && !m_pListBoxCtx->Items[i].IsTitle)
					SetCurSel(i);
				return;
			}
		}
	}
}
void UBitMapListBox::OnRightMouseUp(const UPoint& position, UINT flags)
{
	if(m_pListBoxCtx->SelectItem )
	{		
		if(m_pListBoxCtx->SelectItem->bEnable)
			DispatchNotify(UBMLB_SELECT);
	}
	else
	{
		SetCurSel(-1);
		DispatchNotify(UBMLB_SELECT);
	}

	//分派消息

}
void UBitMapListBox::CalItemRect(const UPoint & size)
{
	UPoint BitMapSize;
	if (m_BkgSkin && m_BkgSkin->GetItemNum() == RECT_MAX)
	{
		BitMapSize.Set(m_BkgSkin->GetTexture()->GetWidth(),m_BkgSkin->GetTexture()->GetHeight());


		UPoint os = size - BitMapSize;
		{
			m_SkinRect[RECT_TOPANDLEFT] = *m_BkgSkin->GetSkinItemRect(RECT_TOPANDLEFT);
		}
		{
			m_SkinRect[RECT_TOPANDRIGHT] = *m_BkgSkin->GetSkinItemRect(RECT_TOPANDRIGHT);
			m_SkinRect[RECT_TOPANDRIGHT].SetPosition(m_SkinRect[RECT_TOPANDRIGHT].GetPosition().x + os.x,0);
		}
		{
			m_SkinRect[RECT_BOTTOMANDLEFT] = *m_BkgSkin->GetSkinItemRect(RECT_BOTTOMANDLEFT);
			m_SkinRect[RECT_BOTTOMANDLEFT].SetPosition(0,m_SkinRect[RECT_BOTTOMANDLEFT].GetPosition().y + os.y);
		}
		{
			m_SkinRect[RECT_BOTTOMANDRIGHT] = *m_BkgSkin->GetSkinItemRect(RECT_BOTTOMANDRIGHT);
			m_SkinRect[RECT_BOTTOMANDRIGHT].SetPosition(m_SkinRect[RECT_BOTTOMANDRIGHT].GetPosition() + os);
		}
		{
			m_SkinRect[RECT_LEFT] = *m_BkgSkin->GetSkinItemRect(RECT_LEFT);
			m_SkinRect[RECT_LEFT].SetHeight(m_SkinRect[RECT_LEFT].GetHeight() + os.y);
		}
		{
			m_SkinRect[RECT_BOTTOM] = *m_BkgSkin->GetSkinItemRect(RECT_BOTTOM);
			m_SkinRect[RECT_BOTTOM].SetPosition(m_SkinRect[RECT_BOTTOM].GetPosition().x,m_SkinRect[RECT_BOTTOM].GetPosition().y + os.y);
			m_SkinRect[RECT_BOTTOM].SetWidth(m_SkinRect[RECT_BOTTOM].GetWidth() + os.x);
		}
		{
			m_SkinRect[RECT_RIGHT] = *m_BkgSkin->GetSkinItemRect(RECT_RIGHT);
			m_SkinRect[RECT_RIGHT].SetPosition(m_SkinRect[RECT_RIGHT].GetPosition().x + os.x,m_SkinRect[RECT_RIGHT].GetPosition().y);
			m_SkinRect[RECT_RIGHT].SetHeight(m_SkinRect[RECT_RIGHT].GetHeight() + os.y);
		}
		{
			m_SkinRect[RECT_TOP] = *m_BkgSkin->GetSkinItemRect(RECT_TOP);		
			m_SkinRect[RECT_TOP].SetPosition(m_SkinRect[RECT_TOP].GetPosition().x,0);
			m_SkinRect[RECT_TOP].SetWidth(m_SkinRect[RECT_TOP].GetWidth() + os.x);
		}
		{
			m_SkinRect[RECT_MIDDLE] = *m_BkgSkin->GetSkinItemRect(RECT_MIDDLE);
			m_SkinRect[RECT_MIDDLE].SetHeight(m_SkinRect[RECT_MIDDLE].GetHeight() + os.y);
			m_SkinRect[RECT_MIDDLE].SetWidth(m_SkinRect[RECT_MIDDLE].GetWidth() + os.x);
		}
	}
	m_ItemSize.x = m_SkinRect[RECT_MIDDLE].GetWidth();
}
void UBitMapListBox::Clear()
{
	m_pListBoxCtx->Items.clear();
	AllocateItemPos();
}
void UBitMapListBox::AddLine(UColor linecolor)
{
	BitMapListBoxItem e;
	e.FontColor = m_Style->m_FontColor;
	e.Size = UPoint(0, m_Style->m_spFont->GetHeight() + 2);
	e.FontColor = linecolor;
	e.IsLine = TRUE;
	m_pListBoxCtx->Items.push_back(e);
	AllocateItemPos();
}
void UBitMapListBox::AddItem(const char * ItemName,void * ItemData)
{
	if (ItemName == NULL || ItemName[0] == '\0')
	{
		return;
	}
	BitMapListBoxItem e ;
	e.UserData = ItemData;
	e.FontColor = m_Style->m_FontColor;
	e.mAlignment = m_Style->mAlignment;
	e.Size = UPoint(0,m_Style->m_spFont->GetHeight() + 2);
	int Ret = MultiByteToWideChar(CP_UTF8, 0, ItemName, strlen(ItemName), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
	m_pListBoxCtx->Items.push_back(e);
	AllocateItemPos();
}

void UBitMapListBox::AddItem(const char* ItemName, USkinPtr skin, UINT skinIndex, void* ItemData)
{
	if (ItemName == NULL || ItemName[0] == '\0')
	{
		return;
	}
	BitMapListBoxItem e ;
	e.UserData = ItemData;
	e.FontColor = m_Style->m_FontColor;
	e.Size = UPoint(0,m_Style->m_spFont->GetHeight() + 2);
	int Ret = MultiByteToWideChar(CP_UTF8, 0, ItemName, strlen(ItemName), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
	e.spSkin = skin;
	e.uiSkinIndex = skinIndex;
	m_pListBoxCtx->Items.push_back(e);
	AllocateItemPos();
}

void UBitMapListBox::AddItem(const char* ItemName, UINT Alignment, UColor fontcolor, UFontPtr font, bool enable, void* ItemData)
{
	if (ItemName == NULL || ItemName[0] == '\0')
	{
		return;
	}
	BitMapListBoxItem e ;
	e.UserData = ItemData;
	e.mAlignment = Alignment;
	e.FontColor = fontcolor;
	e.spFont = font;
	e.bEnable = enable;
	e.Size = UPoint(0,m_Style->m_spFont->GetHeight() + 2);
	int Ret = MultiByteToWideChar(CP_UTF8, 0, ItemName, strlen(ItemName), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
	m_pListBoxCtx->Items.push_back(e);
	AllocateItemPos();
}
void UBitMapListBox::AddTitle(const char* ItemName, UINT Alignment, UColor fontcolor, UFontPtr font )
{
	if (ItemName == NULL || ItemName[0] == 0)
	{
		return;
	}
	BitMapListBoxItem e ;
	e.mAlignment = Alignment;
	e.FontColor = fontcolor;
	e.spFont = font;
	e.IsTitle = true;
	e.Size = UPoint(0,m_Style->m_spFont->GetHeight() + 2);
	int Ret = MultiByteToWideChar(CP_UTF8, 0, ItemName, strlen(ItemName), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
	m_pListBoxCtx->Items.push_back(e);
	AllocateItemPos();
}
void UBitMapListBox::AddItem(const WCHAR * ItemName,void * ItemData)
{
	if (ItemName == NULL || ItemName[0] == 0)
	{
		return;
	}
	BitMapListBoxItem e ;
	e.UserData = ItemData;
	e.FontColor = m_Style->m_FontColor;
	e.mAlignment = m_Style->mAlignment;
	e.Size = UPoint(0,m_Style->m_spFont->GetHeight() + 2);
	wcscpy(e.text, ItemName);
	m_pListBoxCtx->Items.push_back(e);
	AllocateItemPos();
}
void UBitMapListBox::AddItem(const WCHAR* ItemName, USkinPtr skin, UINT skinIndex, void* ItemData )
{
	if (ItemName == NULL || ItemName[0] == 0)
	{
		return;
	}
	BitMapListBoxItem e ;
	e.UserData = ItemData;
	e.FontColor = m_Style->m_FontColor;
	e.Size = UPoint(0,m_Style->m_spFont->GetHeight() + 2);
	wcscpy(e.text, ItemName);
	m_pListBoxCtx->Items.push_back(e);
	e.spSkin = skin;
	e.uiSkinIndex = skinIndex;
	AllocateItemPos();
}

void UBitMapListBox::AddItem(const WCHAR* ItemName, UINT Alignment, UColor fontcolor, UFontPtr font, bool enable, void* ItemData)
{
	if (ItemName == NULL || ItemName[0] == 0)
	{
		return;
	}
	BitMapListBoxItem e ;
	e.UserData = ItemData;
	e.mAlignment = Alignment;
	e.FontColor = fontcolor;
	e.spFont = font;
	e.bEnable = enable;
	e.Size = UPoint(0,m_Style->m_spFont->GetHeight() + 2);
	wcscpy(e.text, ItemName);
	m_pListBoxCtx->Items.push_back(e);
	AllocateItemPos();
}

void UBitMapListBox::AddTitle(const WCHAR* ItemName, UINT Alignment, UColor fontcolor, UFontPtr font )
{
	if (ItemName == NULL || ItemName[0] == 0)
	{
		return;
	}
	BitMapListBoxItem e ;
	e.mAlignment = Alignment;
	e.FontColor = fontcolor;
	e.spFont = font;
	e.IsTitle = true;
	e.Size = UPoint(0,m_Style->m_spFont->GetHeight() + 2);
	wcscpy(e.text, ItemName);
	m_pListBoxCtx->Items.push_back(e);
	AllocateItemPos();
}

void UBitMapListBox::InsertItem(int Index,const char * ItemName ,void * ItemData)
{
	if (ItemName == NULL || ItemName[0] == '\0')
	{
		return;
	}
	BitMapListBoxItem e;
	int Ret = MultiByteToWideChar(CP_UTF8, 0, ItemName, strlen(ItemName), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
	e.UserData = ItemData;

	if (m_pListBoxCtx->Items.empty())
	{
		m_pListBoxCtx->Items.push_back(e);
	}
	else
	{
		if (Index > m_pListBoxCtx->Items.size() )
		{
			Index = m_pListBoxCtx->Items.size();
		}
		m_pListBoxCtx->Items.insert(m_pListBoxCtx->Items.begin()+ Index,e);
		m_pListBoxCtx->Items[Index] = e;
	}
	AllocateItemPos();
}
void UBitMapListBox::DeleteItem(int Index)
{
	if (Index < 0 && Index > m_pListBoxCtx->Items.size())
	{
		return;
	}
	m_pListBoxCtx->Items.erase(m_pListBoxCtx->Items.begin()+Index);
	AllocateItemPos();
}
void UBitMapListBox::DeleteItemByText(const char * text)
{

}
int UBitMapListBox::GetCurSel() const
{
	return m_pListBoxCtx->GetCurSel();
}
void UBitMapListBox::SetCurSel(int Index)
{
	m_pListBoxCtx->SetSelect(Index);
}
void UBitMapListBox::SetCurSelAndDispatch(int Index)
{
	if (m_pListBoxCtx->GetSize())
	{
		m_pListBoxCtx->SetSelect(Index);
		DispatchNotify(UBMLB_SELECT);
	}
}
void UBitMapListBox::SetItemEnable(int Index , bool bEnable)
{	
	BitMapListBoxItem* pItem = m_pListBoxCtx->GetListItem(Index);
	if (pItem == NULL)
		return;
	pItem->bEnable = bEnable;
}
BOOL UBitMapListBox::IsItemEnable(int Index)
{
	BitMapListBoxItem* pItem = m_pListBoxCtx->GetListItem(Index);
	if (pItem == NULL)
		return false;
	return pItem->bEnable;
}
void UBitMapListBox::SetItemText(int Index,const char * text)
{
	if (Index < 0 || Index >= m_pListBoxCtx->GetSize())
	{
		return;
	}

	if (text == NULL || text[0] == '\0')
	{
		return;
	}
	BitMapListBoxItem& e = m_pListBoxCtx->Items[Index];
	int Ret = MultiByteToWideChar(CP_UTF8, 0, text, strlen(text), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
}

void UBitMapListBox::DrawChildItem(int Index,const UPoint &offset)
{
	if (m_pListBoxCtx->Items[Index].IsLine)
	{
		UPoint pos1,pos2;
		UPoint size = m_pListBoxCtx->Items[Index].Size;
		pos1 = offset + m_pListBoxCtx->Items[Index].Position + m_SkinRect[RECT_MIDDLE].GetPosition() + UPoint(0, (size.y>>1));
		pos2 = offset + m_pListBoxCtx->Items[Index].Position + m_SkinRect[RECT_MIDDLE].GetPosition() + UPoint(m_SkinRect[RECT_MIDDLE].GetWidth(), (size.y>>1));
		sm_UiRender->DrawLine(pos1, pos2, m_pListBoxCtx->Items[Index].FontColor);
		return;
	}
	UINT Alignment = m_pListBoxCtx->Items[Index].mAlignment;;
	BOOL bEnable = m_pListBoxCtx->Items[Index].bEnable;
	BOOL bSelect = m_pListBoxCtx->Items[Index].bSelect;
	BOOL bMouseOver = m_pListBoxCtx->Items[Index].bMouseOver;	
	UColor fontcolor = bEnable?m_pListBoxCtx->Items[Index].FontColor:m_Style->m_FontColorNA;
	const WCHAR * text = m_pListBoxCtx->Items[Index].text;
	if (text == NULL)
	{
		return ;
	}
	UPoint textpos = m_pListBoxCtx->Items[Index].Position + m_SkinRect[RECT_MIDDLE].GetPosition() + offset;
	if (bEnable)
	{
		if (bSelect)
		{
			URect hightLightRect = URect(textpos,m_ItemSize);
			sm_UiRender->DrawRectFill(hightLightRect,m_Style->mFillColorNA);
			textpos.Offset(2, 2);
		}
		else if(bMouseOver)
		{
			URect hightLightRect = URect(textpos,m_ItemSize);
			sm_UiRender->DrawRectFill(hightLightRect,m_Style->mFillColorHL);
		}
	}

	IUFont* pFont = m_Style->m_spFont;
	assert(pFont);
	int txt_w = pFont->GetStrWidth(text);

	UPoint localStart ;
	localStart.x = 0;
	// 垂直中线对齐
	localStart.y = 0;

	switch ( Alignment )//m_Style->mAlignment)
	{
	case UT_RIGHT:
		{
			localStart.x = m_SkinRect[RECT_MIDDLE].GetWidth() - txt_w;
		}
		break;
	case UT_CENTER:
		{
			localStart.x = (m_SkinRect[RECT_MIDDLE].GetWidth() - txt_w)/2;
		}
		break;
	default:
		{
			localStart.x = m_Style->mTextOffset.x;
		}
		break;
	}

	UFontPtr font = m_pListBoxCtx->Items[Index].spFont;
	if (!font)
	{
		font = m_Style->m_spFont;
	}
	if (m_pScrollBar)
	{
		URect cliprect = m_SkinRect[RECT_MIDDLE];
		//cliprect.Offset(offset);
		UPoint text_Rect_pos1 = m_pScrollBar->ScreenToWindow(textpos);
		UPoint text_Rect_pos2 = m_pScrollBar->ScreenToWindow(UPoint(textpos.x, textpos.y + font->GetHeight()));
		if (cliprect.PointInRect(text_Rect_pos1) && cliprect.PointInRect(text_Rect_pos2))
		{
			sm_UiRender->DrawTextN(font, textpos + localStart,text,wcslen(text),NULL,fontcolor);
		}
	}
	else
	{
		sm_UiRender->DrawTextN(font, textpos + localStart,text,wcslen(text),NULL,fontcolor);
	}
	if (m_pListBoxCtx->Items[Index].spSkin)
	{
		URect skinRect;
		skinRect.pt0.x = textpos.x + m_SkinRect[RECT_MIDDLE].GetWidth() - m_ItemSize.y;
		skinRect.pt0.y = textpos.y;
		skinRect.pt1.x = skinRect.pt0.x + m_ItemSize.y;
		skinRect.pt1.y = skinRect.pt0.y +m_ItemSize.y;
		sm_UiRender->DrawSkin(m_pListBoxCtx->Items[Index].spSkin, m_pListBoxCtx->Items[Index].uiSkinIndex, skinRect);
	}
}
void UBitMapListBox::SetListHeight(int Height)
{
	UPoint NewSize = m_Size;
	NewSize.y = m_SkinRect[RECT_TOP].GetHeight() + m_SkinRect[RECT_BOTTOM].GetHeight() + Height;
	SetSize(NewSize);
}
void UBitMapListBox::AllocateItemPos()
{
	int ListHeight = 0;
	BitMapListBoxItem * lbItem = NULL;
	for (int i = 0 ; i < m_pListBoxCtx->GetSize() ; i++)
	{
		lbItem = &m_pListBoxCtx->Items[i];
		if (lbItem)
		{
			lbItem->Position.x = 0;
			lbItem->Position.y = ListHeight;
			ListHeight  += m_ItemSize.y;
		}
	}
	SetListHeight(ListHeight);
}
void UBitMapListBox::SetGridSize(const UPoint & size)
{
	m_ItemSize = size;
}
int UBitMapListBox::GetSelectedText(char* pText, int nMaxLen)
{
	BitMapListBoxItem * Select =  m_pListBoxCtx->SelectItem;
	if (!Select)
	{
		return 0;
	}
	if (pText == NULL || nMaxLen <= 0)
	{
		return wcslen(Select->text);
	}

	int nRet = WideCharToMultiByte(CP_UTF8, 0, Select->text, -1, pText, nMaxLen, NULL, NULL);
	return nRet;
}
INT UBitMapListBox::FindItem(const char *text)
{
	assert(text);
	WCHAR wszBuf[ITEM_TEXT_MAX];
	int nRet = MB2WC(text, strlen(text), wszBuf, ITEM_TEXT_MAX -1);
	wszBuf[nRet] = 0;
	return FindItem(wszBuf);
}
INT UBitMapListBox::FindItem(const WCHAR *text)
{
	assert(text);
	for (UINT i = 0 ; i < m_pListBoxCtx->GetSize(); i++)
	{
		if (wcscmp(m_pListBoxCtx->Items[i].text, text) == 0)
		{
			return i ;
		}
	}
	return -1;
}
void * UBitMapListBox::GetItemData(int index)
{
	assert(index >= 0 && index < GetItemCount());
	return m_pListBoxCtx->Items[index].UserData;
}
UINT UBitMapListBox::GetItemCount()
{
	return m_pListBoxCtx->GetSize();
}