#include "StdAfx.h"
#include "UBitmap.h"
#include "USystem.h"

UIMP_CLASS(UBitmap, UControl);
void UBitmap::StaticInit()
{
	UREG_PROPERTY("bitmap", UPT_STRING, UFIELD_OFFSET(UBitmap, m_strFileName));
	UREG_PROPERTY("Skin", UPT_STRING, UFIELD_OFFSET(UBitmap, m_strSkinFileName));
}

UBitmap::UBitmap(void)
{
	m_spTexture = NULL;
	m_spSkin = NULL;
	m_SkinIndex = 0;


	m_Alpha = 255;
	m_bAlphaCmd = FALSE;
	m_Timer = 0.f;
}

UBitmap::~UBitmap(void)
{
	m_spTexture = NULL;
	m_spSkin = NULL;
}


BOOL UBitmap::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	
	if (m_strFileName.GetBuffer())
	{
		m_spTexture = sm_UiRender->LoadTexture(m_strFileName.GetBuffer());
	}
	if (m_strSkinFileName.GetBuffer())
	{
		m_spSkin = sm_System->LoadSkin(m_strSkinFileName.GetBuffer());
	}
	return TRUE;
}

void UBitmap::OnDestroy()
{
	m_spTexture = NULL;
	m_spSkin = NULL;
	UControl::OnDestroy();
}

void UBitmap::OnRender(const UPoint& offset, const URect &updateRect)
{
	URect ClientRect(offset, m_Size);
	UColor pColor = UColor(255,255,255,m_Alpha);

	if (m_spTexture)
	{
		sm_UiRender->DrawImage(m_spTexture, ClientRect, pColor);
	}else if (m_spSkin)
	{
		sm_UiRender->DrawSkin(m_spSkin, m_SkinIndex, ClientRect, pColor);
	}

	if (m_Style->mBorder)
	{
		sm_UiRender->DrawRect(ClientRect, m_Style->mBorderColor);
	}

	if (!m_bAlphaCmd)
	{
		RenderChildWindow(offset, updateRect);
	}
}

void UBitmap::OnTimer(float fDeltaTime)
{
	if (m_bAlphaCmd)
	{
		m_Timer += fDeltaTime;
		if (m_Timer <= 2.0f)
		{
			m_Alpha = BYTE(128.0f * m_Timer);

			if (m_Alpha >= 240)
			{
				m_Alpha = 255;
				m_bAlphaCmd = FALSE;
			}
		}
		else
		{
			m_Alpha = 255;
			m_bAlphaCmd = FALSE;
		}
	}

	UControl::OnTimer(fDeltaTime);
}

BOOL UBitmap::SetBitMapByStr(const char* strFile)
{
	UTexturePtr pTexture = sm_UiRender->LoadTexture(strFile);
	if (pTexture)
	{
		m_spTexture = pTexture;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

BOOL UBitmap::SetBitMapSkinByStr(const char* strFile)
{
	USkinPtr pSkin = sm_System->LoadSkin(strFile);
	if (pSkin)
	{
		m_spSkin = pSkin;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}