#include "StdAfx.h"
#include "UButton.h"
#include "USystem.h"
UIMP_CLASS(UButton,UControl);

#define  GlintX 4 
#define  GlintY 4

void UButton::StaticInit()
{
	UREG_PROPERTY("text", UPT_STRING, UFIELD_OFFSET(UButton, m_strBtnText));
	UREG_PROPERTY("groupNum", UPT_INT, UFIELD_OFFSET(UButton, m_nRadioGroup));
	UREG_PROPERTY("buttonType", UPT_INT, UFIELD_OFFSET(UButton, m_eBtnType));
	UREG_PROPERTY("IsHoriText", UPT_BOOL, UFIELD_OFFSET(UButton, m_bHori));
}

UBEGIN_MESSAGE_MAP(UButton,UControl)
UEND_MESSAGE_MAP()


UButton::UButton()
{
	m_bPressed = FALSE;
	m_bMouseOver = FALSE;
	m_Active = TRUE;
	m_bCheck = FALSE;
	m_nRadioGroup = -1;
	m_eBtnType = BTT_PUSH;

	m_GlintTexture[0] = NULL;
	m_GlintTexture[1] = NULL;
	m_GlintTexture[2] = NULL;
	m_GlintTexture[3] = NULL;
	m_GlintTexture[4] = NULL;
	m_bHori = TRUE;
	m_timer = 0.f;
}

UButton::~UButton()
{
	m_strBtnText.Clear();
	m_GlintTexture[0] = NULL;
	m_GlintTexture[1] = NULL;
	m_GlintTexture[2] = NULL;
	m_GlintTexture[3] = NULL;
	m_GlintTexture[4] = NULL;
}

BOOL UButton::OnCreate()
{
	if(!UControl::OnCreate())
		return FALSE;

	m_GlintTexture[0]= sm_UiRender->LoadTexture("Data\\UI\\Core\\star_b_0.tga");
	m_GlintTexture[1]= sm_UiRender->LoadTexture("Data\\UI\\Core\\star_b_1.tga");
	m_GlintTexture[2]= sm_UiRender->LoadTexture("Data\\UI\\Core\\star_b_2.tga");
	m_GlintTexture[3]= sm_UiRender->LoadTexture("Data\\UI\\Core\\star_b_3.tga");
	m_GlintTexture[4]= sm_UiRender->LoadTexture("Data\\UI\\Core\\star_b_4.tga");

	if (m_GlintTexture[0] == NULL ||
	m_GlintTexture[1] == NULL  ||
	m_GlintTexture[2] == NULL  ||
	m_GlintTexture[3] == NULL  ||
	m_GlintTexture[4] == NULL)
	{
		
		return FALSE;
	}
	return TRUE;
}

void UButton::OnDestroy()
{
	m_bPressed = FALSE;
	m_bMouseOver = FALSE;
	m_bCheck = FALSE;
	m_timer = 0.f;
	UControl::OnDestroy();
}

void UButton::SetText(const char *text)
{
	m_strBtnText.Set(text);
}

const char *UButton::GetText()
{
	return m_strBtnText.GetBuffer();;
}

void UButton::acceleratorKeyPress(const char * opCode)
{
	if (!m_Active)
		return;
	m_bPressed = TRUE;
	if (m_Style->m_bTabStop)
		SetFocusControl();
}

void UButton::acceleratorKeyRelease(const char * opCode)
{
	if (!m_Active)
		return;
	if (m_bPressed)
	{
		m_bPressed = FALSE;
		//perform the actiOn
		PreCommand();
	}
}

void UButton::OnRightMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	if (!m_Active)
		return;
	if (m_Style->m_bKeyFocus)
		SetFocusControl();

	if (!m_Style->mSoundButtOnDown.Empty())
	{
		//		AUDIOHANDLE handle = alxCreateSource(m_Style->mSoundButtOnDown);
		//		alxPlay(handle);
		sm_System->__PlaySound(m_Style->mSoundButtOnDown.GetBuffer());
	}
	//lock the mouse
	CaptureControl();
	m_bPressed = TRUE;
	
}
void UButton::OnRightMouseUp(const UPoint& position, UINT flags)
{
	if (!m_Active)
		return;
	ReleaseCapture();
	if (m_bPressed)
	{
		DispatchNotify(UBN_RCLICKED);
	}
	m_bPressed = FALSE;
}
void UButton::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	if (!m_Active)
		return;
	if (m_Style->m_bKeyFocus)
		SetFocusControl();

	if (!m_Style->mSoundButtOnDown.Empty())
	{
		//		AUDIOHANDLE handle = alxCreateSource(m_Style->mSoundButtOnDown);
		//		alxPlay(handle);
		sm_System->__PlaySound(m_Style->mSoundButtOnDown.GetBuffer());

	}
	//lock the mouse
	CaptureControl();
	m_bPressed = TRUE;
}

void UButton::OnTimer(float fDeltaTime)
{
	if (m_bPressed)
	{
		m_timer += fDeltaTime;
		if (m_timer > 0.05f)
		{
			m_timer = 0.0f;
			DispatchNotify(UBN_PRESS);
		}
	}else
	{
		m_timer = 0.0f;
	}
	UControl::OnTimer(fDeltaTime);
}

void UButton::OnMouseEnter(const UPoint& pt, UINT uFlags)
{
	if(IsCaptured())
	{
		m_bPressed = TRUE;
		m_bMouseOver = TRUE;
	}
	else
	{
		if ( m_Active && !m_Style->mSoundButtOnOver.Empty() )
		{
			//		AUDIOHANDLE handle = alxCreateSource(m_Style->mSoundButtOnOver);
			//		alxPlay(handle);
			sm_System->__PlaySound(m_Style->mSoundButtOnOver.GetBuffer());
		}
		m_bMouseOver = TRUE;
	}
	UControl::OnMouseEnter(pt,uFlags);
}

void UButton::OnMouseLeave(const UPoint& pt, UINT uFlags)
{
	if(IsCaptured())
		m_bPressed = FALSE;
	m_bMouseOver = FALSE;

	UControl::OnMouseLeave(pt,uFlags);
}

void UButton::OnMouseUp(const UPoint& pt, UINT uFlags)
{
	ReleaseCapture();
	if (m_bPressed)
	{
		m_bPressed = FALSE;
		if (!m_Active)
			return;
		PreCommand();
	}
	m_bPressed = FALSE;
}

BOOL UButton::OnKeyDown(const GuiEvent &event)
{
	////if the cOntrol is a dead end, kill the event
	//if (!m_Active)
	//	return TRUE;

	////see if the key down is a return or space or not
	//if ((event.keyCode == LK_RETURN || event.keyCode == LKEY_SPACE)
	//	&& event.modifier == 0) 
	//{
	//	if ( m_Style->mSoundButtOnDown )
	//	{
	//		float pan = ( float( event.mousePoint.x ) / float( Canvas->m_Size.x ) * 2.0f - 1.0f ) * 0.8f;
	//		AUDIOHANDLE handle = alxCreateSource( m_Style->mSoundButtOnDown );
	//		alxPlay( handle );
	//	}
	//	return TRUE;
	//}
	////otherwise, pass the event to it's parent
	//return Parent::OnKeyDown(event);
	return FALSE;
}

//--------------------------------------------------------------------------
BOOL UButton::OnKeyUp(const GuiEvent &event)
{
	////if the cOntrol is a dead end, kill the event
	//if (!m_Active)
	//	return TRUE;

	////see if the key down is a return or space or not
	//if (m_bPressed &&
	//	(event.keyCode == LKEY_RETURN || event.keyCode == LKEY_SPACE) &&
	//	event.modifier == 0)
	//{
	//	OnAction();
	//	return TRUE;
	//}

	////otherwise, pass the event to it's parent
	//return Parent::OnKeyUp(event);
	return FALSE;
}
BOOL UButton::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	return UControl::OnKeyDown(nKeyCode,nRepCnt,nFlags);
}
BOOL UButton::OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	return UControl::OnKeyUp(nKeyCode,nRepCnt,nFlags);
}
//--------------------------------------------------------------------------- 
void UButton::SetCheck(BOOL bCheck)
{
	m_bCheck = bCheck;

	// NOTIFY MESSAGE
	PreCommand();
}

void UButton::SetCheckState(BOOL bCheck)
{
	m_bCheck = bCheck;
}

void UButton::PreCommand()
{
	// Notify Message.
	//if(!m_Active)
		//return;

	mBGlint = FALSE;
	if(m_eBtnType == BTT_CHECK)
	{
		m_bCheck = !m_bCheck;
		
	}
	else if(m_eBtnType == BTT_RADIO)
	{
		m_bCheck = TRUE;
		// 通知单选框组. 
		UControl* pParent = GetParent();
		if(pParent && !pParent->IsDesktop())
		{
			for (int c = 0; c < pParent->GetNumChilds(); c++)
			{
				UControl* pChild  = pParent->GetChildCtrl(c);
				if (pChild == this)
				{
					continue;
				}
				UButton* pButton = UDynamicCast(UButton, pChild);
			
				if (pButton && pButton->m_eBtnType == BTT_RADIO && pButton->m_nRadioGroup == m_nRadioGroup )
				{
					pButton->m_bCheck = FALSE; // 不要让组里的每一个单选框都引起通知.
				}
			}
		}
	}		
	DispatchNotify(UBN_CLICKED);
}

void UButton::OnRender(const UPoint& offset, const URect &updateRect)
{	
	
	if (m_eBtnType  == BTT_PUSH)
	{
		DrawPushButton(offset,updateRect);
	}else
	{
		assert(m_eBtnType == BTT_CHECK || m_eBtnType == BTT_RADIO);
		DrawCheckBox(offset,updateRect);
	}	
	//RenderChildWindow(offset, updateRect);
	OnRenderGlint(offset, updateRect);
}
void UButton::OnRenderGlint(const UPoint& offset,const URect &updateRect)
{
	if (mBGlint)
	{
		//RenderGlint(updateRect,mGlintTime , 60);
		UINT Speed = 80 ;
		float ftime = 0.0375f;
		UINT MaxLen = 2 * (m_Size.x + m_Size.y); 
		UINT Time = MaxLen * 1000 / Speed; 
		UINT CurT = mGlintTime * 1000;

		UINT count  = Time / (ftime*1000);

		for (int i = 0 ; i < count ; i++)
		{
			float alpha =  (float)(i % 10) / 10.0f ;
			//是否需要运行过程， 如果需要 则是 mGlintTime >= i*ftime ;
			//如果不需要。则要等待 ((float)MaxLen) / (float) Speed。 就是第一个粒子运行一圈的时间。
			if (mGlintTime >= i*ftime )
			{
				RenderGlint(updateRect,mGlintTime - ftime * i, Speed, 1.0f- alpha);
			}
		}	
	}

}
void UButton::RenderGlint(const URect &updateRect, float fTime ,UINT Speed, float alpha)
{
	URect OldClip = sm_UiRender->GetClipRect();

	URect ClipRec;
	ClipRec.pt0.x = updateRect.pt0.x - (GlintX>>1);
	ClipRec.pt0.y = updateRect.pt0.y - (GlintY>>1);
	ClipRec.pt1.x = ClipRec.pt0.x + updateRect.GetWidth() + GlintX;
	ClipRec.pt1.y = ClipRec.pt0.y + updateRect.GetHeight() + GlintY;
	//ClipRec.SetPosition(updateRect.GetPosition() - UPoint(GlintX / 2, GlintY / 2));
	//ClipRec.SetSize(updateRect.GetSize() + UPoint(GlintX, GlintY));

	sm_UiRender->SetClipRect(ClipRec);

	UPoint GlintPos( ClipRec.GetPosition() );
	UINT hight =	ClipRec.GetHeight() - (GlintY>>1);   //高度
	UINT width =	ClipRec.GetWidth() - (GlintX>>1);   //宽度

	URect uRect ;

	uRect.SetSize(GlintX, GlintY);

	UINT Len = Speed * fTime;

	UColor Color(255,255,255);
	BOOL bNeed = Len /( 2 * ( hight + width )- GlintY - GlintX ); // 
	URect uvrect;
	Len = Len % (2 * (hight + width) - GlintX - GlintY);

	if (Len >= 0 && Len < width - (GlintX>>1))
	{
		GlintPos.x += Len;	

	}else if (Len >= width - (GlintX>>1) && Len < (width  - (GlintX>>1) + hight - (GlintY>>1)))
	{
		GlintPos.x += width  - (GlintX>>1);
		GlintPos.y += Len - (width - (GlintX>>1)) ;
	}else if (Len >= (width  - (GlintX>>1) + hight - (GlintY>>1)) && Len < (2 * width - GlintX + hight - (GlintY>>1)))
	{
		GlintPos.x += 2 * width  - GlintX + hight - (GlintY>>1) - Len;
		GlintPos.y += hight - (GlintY>>1);

	}else 
	{
		GlintPos.y += 2 * (width + hight) - GlintX - GlintY - Len;	
	}

	uRect.SetPosition(GlintPos);
	UINT index = 4  ;  //rand() % 5 ;
	
	//Color.a = 55 + 200 * alpha ;
	Color.a *= alpha ;
	if (alpha == 1.0f)
	{
		index = 0;
	}else if (alpha == 0.9f )
	{
		index = 1 ;
	}else if (alpha == 0.8f)
	{
		index = 2 ;
	}else if (alpha == 0.7f)
	{
		index = 3 ;
	}else
	{
		index = 4 ;
	}
	sm_UiRender->DrawImage(m_GlintTexture[index],uRect,Color);
	sm_UiRender->SetClipRect(OldClip);
}
void UButton::DrawPushButton(const UPoint& offset, const URect& ClipRect)
{
	BOOL highlight = m_bMouseOver;
	BOOL depressed = m_bPressed;

	UColor FontColor   = m_Active ? (highlight ? m_Style->m_FontColorHL : m_Style->m_FontColor) : m_Style->m_FontColorNA;
	URect boundsRect(offset, m_Size);

	if (m_bPressed || m_bCheck)
	{
		//renderLoweredBox(boundsRect, m_Style);
		sm_UiRender->DrawRectFill(boundsRect, m_Style->mFillColorHL);
		sm_UiRender->DrawRect(boundsRect, m_Style->mBorderColorHL);
		
	}else
	{
		//renderRaisedBox(boundsRect, m_Style);
		sm_UiRender->DrawRectFill(boundsRect, m_Style->mFillColor);
		sm_UiRender->DrawRect(boundsRect, m_Style->mBorderColor);
		
	}
	UPoint textPos = offset;
	if(depressed)
		textPos += UPoint(1,1);

	if (m_Style->m_spFont && !m_strBtnText.Empty())
	{
		UDrawText(sm_UiRender, m_Style->m_spFont, offset, m_Size, FontColor, m_strBtnText.GetBuffer(), m_Style->mAlignment, 0.0f, 1.0f, m_bHori);
	}
}

void UButton::DrawRadioButton(const UPoint& offset, const URect& ClipRect)
{
	assert(m_Style);
	DrawCheckBox(offset, ClipRect);
}

void UButton::DrawCheckBox(const UPoint& offset, const URect& ClipRect)
{
	assert(m_Style);

	int nSkinId = m_bCheck ? CBS_CHECK_NORMAL: CBS_UNCHECK_NORMAL;
	if (m_Active)
	{
		if (m_bMouseOver)
		{
			nSkinId++;
		}
	}else
	{
		nSkinId = CBS_DISABLED;
	}

	USkin* pSkin = m_Style->m_spSkin;
	if (m_Style->m_spSkin)
	{
		sm_UiRender->DrawSkin(pSkin, nSkinId, offset);
	}
	else
	{
		URect boundsRect(offset, m_Size);
		if (m_bCheck)
		{
			//renderLoweredBox(boundsRect, m_Style);
			sm_UiRender->DrawRectFill(boundsRect, m_Style->mFillColorHL);
			sm_UiRender->DrawRect(boundsRect, m_Style->mBorderColorHL);

		}else
		{
			//renderRaisedBox(boundsRect, m_Style);
			sm_UiRender->DrawRectFill(boundsRect, m_Style->mFillColor);
			sm_UiRender->DrawRect(boundsRect, m_Style->mBorderColor);

		}
	}
	
	if (!m_strBtnText.Empty() && m_Style->m_spFont)
	{
		//const URect* pRect = pSkin->GetSkinItemRect(nSkinId);
		int xOfs =  (nSkinId == CBS_CHECK_NORMAL) || (nSkinId == CBS_CHECK_HILIT) ? 5 : 0;//pRect ? pRect->GetWidth() : 0 ;
		UPoint Offset(offset.x + xOfs, offset.y);
		UPoint Size(m_Size.x - xOfs, m_Size.y);

		UColor FontColor = m_Active ? (m_bMouseOver ? m_Style->m_FontColorHL : m_Style->m_FontColor) : m_Style->m_FontColorNA;
		UDrawText(sm_UiRender, m_Style->m_spFont, Offset, Size, FontColor, m_strBtnText.GetBuffer(), m_Style->mAlignment, 0.0f, 1.0f, m_bHori);
	}
	
}


//////////////////////////////////////////////////////////////////////////
//  UBitmapButton
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UBitmapButton, UButton);

void UBitmapButton::StaticInit()
{
	UREG_PROPERTY("bitmap", UPT_STRING, UFIELD_OFFSET(UBitmapButton, m_strBitmapFile));
}


UBitmapButton::UBitmapButton()
{
	m_Size.Set(100, 20);

	m_Alpha = 255;
	m_bAlphaCmd = FALSE;
	m_Timer = 0.f;
}

UBitmapButton::~UBitmapButton()
{
	
}

BOOL UBitmapButton::OnCreate()
{
	if (!UButton::OnCreate())
		return FALSE;
	//SetActive(TRUE);
	SetBitmap(m_strBitmapFile.GetBuffer());
	return TRUE;
}

void UBitmapButton::OnDestroy()
{
	m_spBtnSkin = NULL;
	UButton::OnDestroy();
}

void UBitmapButton::SetBitmap(const char* szFileName)
{
	m_strBitmapFile.Set(szFileName);
	if (!IsCreated())
	{
		return;
	}
	
	if (!m_strBitmapFile.Empty())
	{
		m_spBtnSkin = sm_System->LoadSkin(m_strBitmapFile.GetBuffer());
	}else
	{
		m_spBtnSkin = NULL;
	}
}

void UBitmapButton::OnTimer(float fDeltaTime)
{
	if (m_bAlphaCmd)
	{
		m_Timer += fDeltaTime;
		if (m_Timer <= 2.0f)
		{
			m_Alpha = BYTE(128.0f * m_Timer);

			if (m_Alpha >= 250)  
			{
				m_Alpha = 255;
			}
		}

		if (m_Alpha == 255)
		{
			m_bAlphaCmd = FALSE;
		}
	}
	UButton::OnTimer(fDeltaTime);
}

void UBitmapButton::DrawPushButton(const UPoint& offset, const URect& ClipRect)
{
	UColor pColor = UColor(255,255,255,m_Alpha);
	if (m_spBtnSkin == NULL)
	{
		UButton::DrawPushButton(offset, ClipRect);
		return;
	}
	EButtonSkin eSkinID = BTS_NORMAL;
	if (m_Active)
	{
		if (m_bMouseOver)
		{
			eSkinID = BTS_MOUSEOVER;
		}

		if (m_bPressed || m_bCheck)
		{
			eSkinID = BTS_PUSHED;
		}
	}else
	{
		eSkinID = BTS_DISABLE;
	}
	
	int nItems = m_spBtnSkin->GetItemNum();
	assert(nItems > 0);
	if (eSkinID >= nItems)
	{
		eSkinID = BTS_NORMAL;
	}
	URect rect(offset, m_Size);
	sm_UiRender->DrawSkin(m_spBtnSkin, eSkinID, rect, pColor);

	if (!m_strBtnText.Empty() && m_Style->m_spFont)
	{

		UPoint Offset(offset.x , offset.y);
		UPoint Size(m_Size.x , m_Size.y);

		UColor FontColor;
		if (eSkinID == BTS_PUSHED)
		{
			FontColor = m_Style->m_FontColor;
		}else if (eSkinID == BTS_DISABLE)
		{
			FontColor = m_Style->m_FontColorHL;
		}
		else
		{
			FontColor = m_Style->m_FontColorNA;
		}

		//m_Style->m_spFont->GetStrWidth(m_strBtnText.GetBuffer());
		UDrawText(sm_UiRender, m_Style->m_spFont, Offset + UPoint(1, 1), Size, LCOLOR_ARGB(m_Alpha, 0, 0, 0), m_strBtnText.GetBuffer(), m_Style->mAlignment, 0.0f, 1.0f, m_bHori);
		if (eSkinID == BTS_NORMAL)
			USetTextEdge(TRUE, LCOLOR_ARGB(m_Alpha, 0, 0, 0));
		UDrawText(sm_UiRender, m_Style->m_spFont, Offset, Size, LCOLOR_ARGB(min(FontColor.a, m_Alpha), FontColor.r, FontColor.g, FontColor.b), m_strBtnText.GetBuffer(), m_Style->mAlignment, 0.0f, 1.0f, m_bHori);
		USetTextEdge(FALSE);
	}
}

void UBitmapButton::DrawCheckBox(const UPoint& offset, const URect& ClipRect)
{
	UColor pColor = UColor(255,255,255,m_Alpha);
	if (m_spBtnSkin == NULL)
	{
		UButton::DrawCheckBox(offset, ClipRect);
		return;
	}
	EButtonSkin eSkinID = BTS_NORMAL;
	if (m_Active)
	{
		if (m_bMouseOver)
		{
			eSkinID = BTS_MOUSEOVER;
		}

		if (m_bPressed || m_bCheck)
		{
			eSkinID = BTS_PUSHED;
		}
	}else
	{
		eSkinID = BTS_DISABLE;
	}
	
	int nItems = m_spBtnSkin->GetItemNum();
	assert(nItems > 0);
	if (eSkinID >= nItems)
	{
		eSkinID = BTS_NORMAL;
	}
	URect rect(offset, m_Size);
	sm_UiRender->DrawSkin(m_spBtnSkin, eSkinID, rect, pColor);

	if (!m_strBtnText.Empty() && m_Style->m_spFont)
	{
		
		UPoint Offset(offset.x , offset.y);
		UPoint Size(m_Size.x , m_Size.y);

		UColor FontColor;
		if (eSkinID == BTS_PUSHED)
		{
			FontColor = m_Style->m_FontColor;
		}else
		{
			FontColor = m_Style->m_FontColorHL;
		}

		UDrawText(sm_UiRender, m_Style->m_spFont, Offset + UPoint(1, 1), Size, LCOLOR_ARGB(m_Alpha, 0, 0, 0), m_strBtnText.GetBuffer(), m_Style->mAlignment, 0.0f, 1.0f, m_bHori);
		if (eSkinID == BTS_NORMAL)
			USetTextEdge(TRUE, LCOLOR_ARGB(m_Alpha, 0, 0, 0));
		UDrawText(sm_UiRender, m_Style->m_spFont, Offset, Size, LCOLOR_ARGB(min(FontColor.a, m_Alpha), FontColor.r, FontColor.g, FontColor.b), m_strBtnText.GetBuffer(), m_Style->mAlignment, 0.0f, 1.0f, m_bHori);
		USetTextEdge(FALSE);
	}
}


