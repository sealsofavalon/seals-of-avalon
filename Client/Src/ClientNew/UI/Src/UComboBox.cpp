#include "StdAfx.h"
#include "UComCtrl.h"
#include "UListBox.h"
#include "UScrollBar.h"
#include "USystem.h"
#include "UDesktop.h"
#include "UBitMapListBox.h"

#define COMBOITEM_TEXT_MAX  UListBox::ITEM_TEXT_MAX

struct COMBOITEM
{
	WCHAR buf[COMBOITEM_TEXT_MAX];
	void* UserData;
	int IconIndex;
};

struct ComboBox
{
	class TextListBox;

	class MaskCtrl : public UControl
	{
	protected:
		UComboBox *m_pComboCtrl;
		ComboBox* m_pComboData;
		TextListBox *mTextList; 
		UScrollBar* m_pScrollBar;
	public:
		MaskCtrl(UComboBox *ctrl, ComboBox* pCBBData);
		BOOL CreateControl();
		void Popup();
		void ClosePopup();
		void FillTextList();
		TextListBox* GetListBox() { return mTextList;}
		UScrollBar* GetScrollBar() { return m_pScrollBar;}
		void SetMaxHeight(int nMaxHeight);
		void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	};

	class TextListBox : public UBitMapListBox
	{
	protected:
		UComboBox *m_pComboCtrl;
		MaskCtrl* m_pMaskCtrl;
		int m_nMaxWidth;
	public:
		TextListBox();
		TextListBox(UComboBox *ctrl, MaskCtrl* pMask);
		
	private:
		virtual void OnSelChange(int row, int col);
		virtual void OnMouseUp(const UPoint& position, UINT flags);
		virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
		virtual BOOL OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
		//virtual void DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver);
	};

	
	ComboBox(UComboBox* pComboBox)
	{
		pComboBoxCtrl = pComboBox;
		ScrollDir = UScrollBar::SBR_UNKNOWN;
		mBackgroundCancel = FALSE;
		mInAction = FALSE;
		mBackground = NULL;
	}
	UComboBox* pComboBoxCtrl;
	UScrollBar::EUSBRegion ScrollDir;
	MaskCtrl *mBackground;
	BOOL mBackgroundCancel;
	BOOL mInAction;
	std::vector<COMBOITEM> mEntries;
	void Destroy()
	{
		if (mBackground)
		{
			mBackground->ClosePopup();
			mBackground->DeleteThis();
			mBackground = NULL;
		}
	}
	TextListBox* GetTextList()
	{
		if (mBackground)
		{
			return mBackground->GetListBox();
		}
		return NULL;
	}
	BOOL CreateControls()
	{
		if (mBackground == NULL)
		{
			mBackground = new MaskCtrl(pComboBoxCtrl, this);
			assert(mBackground);
			return mBackground->CreateControl();
		}
		return TRUE;
	}
};

ComboBox::MaskCtrl::MaskCtrl(UComboBox *ctrl, ComboBox* pCBBData)
{
	m_pComboCtrl = ctrl;
	m_pComboData = pCBBData;
	mTextList = NULL;
	m_pScrollBar = NULL;
}

BOOL ComboBox::MaskCtrl::CreateControl()
{
	if (mTextList == NULL)
	{
		mTextList = new ComboBox::TextListBox(m_pComboCtrl, this);
		assert(mTextList);
		mTextList->SetField("style", m_pComboCtrl->GetStyle()->m_Name.GetBuffer());
	}

	if (m_pScrollBar == NULL)
	{
		m_pScrollBar = new UScrollBar;
		assert(m_pScrollBar);
		m_pScrollBar->SetField("style", "UScrollBar");
		m_pScrollBar->SetPosition(UPoint(0,0));
	}
	if (!m_pScrollBar)
		return FALSE;
	m_Style = g_USystem->FindControlStyle("default");

	// 连接控件父子关系
	AddChild(m_pScrollBar);
	m_pScrollBar->AddChild(mTextList);
	m_pScrollBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);
	//AddChild(mTextList);
	
	return TRUE;
}

// TODO : 
void ComboBox::MaskCtrl::Popup()
{
	UDesktop *root = g_Desktop;
	UPoint windowExt = root->GetWindowSize();
	
	// fit with the desktop
	ResizeControl(UPoint(0,0), windowExt);

	int textWidth = 0;
	const int textSpace = 2;
	BOOL setScroll = FALSE;

	// 根据ComboBox 的字体计算字符宽度
	const UControlStyle* pComboBoxStyle = m_pComboCtrl->GetStyle();
	IUFont* pFont = pComboBoxStyle->m_spFont;
	
	for(UINT i=0; i < m_pComboData->mEntries.size(); ++i)
	{
		int rowWidth = pFont->GetStrWidth(m_pComboData->mEntries[i].buf);
		if(rowWidth > textWidth)
			textWidth = rowWidth;
	}
	int TextListHeight  = m_pComboData->mEntries.size() * (pFont->GetHeight() + textSpace);
	const UPoint& ComboCtrlSize = m_pComboCtrl->GetWindowSize();
	UScrollBar* pScrollBar = GetScrollBar();
	const UControlStyle* pSBStyle = pScrollBar->GetStyle();
	const UPoint& SBMargin = pScrollBar->GetMargin();
	ComboBox::TextListBox* pTextList = GetListBox();
	
	// 计算宽度
	int sbWidth = pSBStyle->mBorderThickness * 2 + pScrollBar->GetBarSize();
	int nEdgePadPixel = sbWidth + pComboBoxStyle->mTextOffset.x + SBMargin.x * 2;
	int nComboTextVisWidth = ComboCtrlSize.x - nEdgePadPixel;
	int width = ComboCtrlSize.x;
	if(textWidth > nComboTextVisWidth) 
	{
		textWidth += nEdgePadPixel; 
		width = textWidth;
		if(SBMargin.x == 0)
			width += 2;
	}

	pTextList->SetGridSize(UPoint(width, pFont->GetHeight() + textSpace)); //  Modified the above line to use textSpace rather than the '3' as this is what is used below.

	// 计算滚动条位置
	UPoint CombCtrlPos = m_pComboCtrl->WindowToScreen(UPoint(0,0));
	UPoint scrollPoint(CombCtrlPos.x, CombCtrlPos.y + ComboCtrlSize.y); 
	int MaxPopupHeight = m_pComboCtrl->GetMaxPopupHeight();

	//Calc max Y distance, so Scroll Ctrl will fit on window 
	int sbBorder = pSBStyle->mBorderThickness * 2 + SBMargin.y * 2;
	int maxYdis = windowExt.y - CombCtrlPos.y - ComboCtrlSize.y - sbBorder; // 滚动条放置于控件下方时最大高度

	int nSelIndex = m_pComboCtrl->GetCurSel();
	// 
	if(maxYdis < TextListHeight + sbBorder) 
	{	
		if(maxYdis < CombCtrlPos.y )	// 在控件上方弹出
		{

			maxYdis = CombCtrlPos.y; 
			//Does the menu need a scroll bar 
			if(maxYdis < TextListHeight + sbBorder)
			{
				setScroll = true;
			}
			else
			{
				// 不需要滚动条
				maxYdis = TextListHeight + sbBorder; 
				if(maxYdis > MaxPopupHeight) maxYdis = MaxPopupHeight;
			}
			scrollPoint.Set(CombCtrlPos.x, CombCtrlPos.y - maxYdis);
		} 
		else
		{
			// 在下方弹出
			
		//if ( nSelIndex >= 0 )
		//{
		//		pTextList->SetSel( nSelIndex, 0); 
		//}
			setScroll = true;
		}
	}
	else
	{
		//if ( nSelIndex >= 0 )
		//	pTextList->SetCurSel(nSelIndex); 
		maxYdis = TextListHeight + sbBorder;
	}

	// 设置滚动条矩形
	URect SBRect;
	SBRect.left = scrollPoint.x;
	SBRect.top = scrollPoint.y;

	if(SBRect.left + width > GetWidth())
	{
		if(width - ComboCtrlSize.x > 0)
		{
			int ofs = width - ComboCtrlSize.x;
			SBRect.left -= ofs;
		}
	}
	SBRect.right = SBRect.left + width + pScrollBar->GetBarSize();
	SBRect.bottom = SBRect.top + MaxPopupHeight;
	pScrollBar->SetWindowRect(SBRect); 

	m_pComboData->mBackgroundCancel = FALSE; 
	sm_System->PushDialog(this);

	pTextList->SetSize(UPoint(width,pFont->GetHeight()));
	for(UINT j=0; j<m_pComboData->mEntries.size(); ++j)
	{
		pTextList->AddItem(m_pComboData->mEntries[j].buf, m_pComboData->mEntries[j].UserData);
	}
	//SBRect.bottom = SBRect.top + 100;//+ __max(maxYdis,pTextList->GetHeight());
	//pTextList->SetPosition(SBRect.GetPosition());
	pTextList->SetPosition(UPoint(0,0));
	//pScrollBar->SetWindowRect(SBRect); 
	if (pTextList->GetHeight() < MaxPopupHeight)
	{
		m_pScrollBar->SetBarShowMode(USBSM_FORCEOFF, USBSM_FORCEOFF);
	}
	else
	{
		m_pScrollBar->SetBarShowMode(USBSM_FORCEON, USBSM_FORCEOFF);
		pScrollBar->ReCalLayout();
		pScrollBar->scrollRectVisible(URect(0,0,0,0));
	}

	//root->pushDialogControl(mBackground, 99);

	//if ( setScroll )
	//{
	//	if ( nSelIndex )
	//		pTextList->ScrollToGrid( UPoint(0, nSelIndex));
	//	else
	//		pTextList->ScrollToGrid( UPoint( 0, 0 ) );
	//}

	pTextList->SetFocusControl();

	m_pComboData->mInAction = TRUE;
}

void ComboBox::MaskCtrl::ClosePopup()
{
	if ( !m_pComboData->mInAction )
		return;

	
	// Get the selection from the text list:
	m_pComboCtrl->SetCurSel(mTextList->GetCurSel());
	
	m_pComboData->mInAction = FALSE;
	mTextList->Clear();
	mTextList->ReleaseCapture();
	
	g_Desktop->PopDialog(this);
	m_pComboCtrl->SetFocusControl();
}

void ComboBox::MaskCtrl::SetMaxHeight(int nMaxHeight)
{

}

void ComboBox::MaskCtrl::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	m_pComboData->mBackgroundCancel = TRUE; 
	ClosePopup();
}

//------------------------------------------------------------------------------
ComboBox::TextListBox::TextListBox()
{
	m_pComboCtrl = NULL;
	m_nMaxWidth = 1;
}


ComboBox::TextListBox::TextListBox(UComboBox *ctrl, ComboBox::MaskCtrl* pMask)
{
	m_pComboCtrl = ctrl;
	m_pMaskCtrl = pMask;
	m_nMaxWidth = 1;
}

void ComboBox::TextListBox::OnSelChange(int row, int col)
{
	//  The old function is above.  This new one will only call the the select
	//      functions if we were not cancelled by a background click.

	//  Check if we were cancelled by the user clicking on the Background ie: anywhere
	//      other than within the text list.
	//		if(m_pComboCtrl->mBackgroundCancel)
	//			return;

	//if( isMethod( "onSelect" ) )
	//	Con::executef(this, "onSelect", Con::getFloatArg(cell.x), Con::getFloatArg(cell.y));

	////call the console function
	//execConsoleCallback();
	////if (mConsoleCommand[0])
	////   Con::evaluate(mConsoleCommand, FALSE);

}

BOOL ComboBox::TextListBox::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	//if the control is a dead end, don't process the input:
	if ( !m_Visible || !m_Active || !m_bCreated )
		return FALSE;

	if (nFlags == 0)
	{
		if (nKeyCode == LK_RETURN)
		{
			m_pMaskCtrl->ClosePopup();
			return TRUE;
		}else if (nKeyCode == LK_ESCAPE)
		{
			m_pMaskCtrl->ClosePopup();
			//SetSel(-1, -1);
			return TRUE;
		}
	}
	
	return UBitMapListBox::OnKeyDown(nKeyCode, nRepCnt, nFlags);
}

void ComboBox::TextListBox::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	//UListBox::OnMouseDown(point, nRepCnt, uFlags);
	UBitMapListBox::OnMouseDown(point,nRepCnt,uFlags);
}

void ComboBox::TextListBox::OnMouseUp(const UPoint& position, UINT flags)
{
	UBitMapListBox::OnMouseDown(position, 1, flags);
	m_pMaskCtrl->ClosePopup();
	UBitMapListBox::OnMouseUp(position, flags);
	
}

//void ComboBox::TextListBox::DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver)
//{
//	UListBox::DrawGridItem(offset, cell, selected, mouseOver);
//}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////

UIMP_CLASS(UComboBox, UControl);

void UComboBox::StaticInit(void)
{
	/*addField("maxPopupHeight",           TypeS32,          Offset(mMaxPopupHeight, UComboBox));
	addField("sbUsesNAColor",            TypeBool,         Offset(mRenderScrollInNA, UComboBox));
	addField("reverseTextList",          TypeBool,         Offset(mReverseTextList, UComboBox));
	addField("bitmap",                   TypeFilename,     Offset(m_strIconFile, UComboBox));
	addField("bitmapBounds",             TypePoint2I,      Offset(mBitmapBounds, UComboBox));*/
}

UComboBox::UComboBox(void)
{
	m_nSelIndex = -1;
	m_Active = true;
	mMaxPopupHeight = 200;
//	mScrollDir = UScrollBar::None;
	mScrollCount = 0;
	mLastYvalue = 0;
	mIncValue = 0;
	
	mMouseOver = FALSE; //  Added

	//m_strIconFile = StringTable->insert(""); //  Added

	m_pComboBox = new ComboBox(this);
	assert(m_pComboBox);
}

UComboBox::~UComboBox()
{
	if (m_pComboBox)
	{
		m_pComboBox->Destroy();
		delete m_pComboBox;
	}
}

int UComboBox::GetCount() const  
{ 
	
	return( m_pComboBox->mEntries.size() ); 
}

BOOL UComboBox::OnCreate()
{
	if (!UControl::OnCreate())
		return FALSE;

	// Set the bitmap for the popup.
	//SetSkin(m_strIconFile);

	return TRUE;
}

void UComboBox::OnDestroy()
{
	m_spIconSkin = NULL;
	if (m_pComboBox)
	{
		m_pComboBox->Destroy();
	}
	
	UControl::OnDestroy();
}

//------------------------------------------------------------------------------
void UComboBox::Clear()
{
	m_pComboBox->mEntries.clear();
	//setText("");
	m_nSelIndex = -1;
}

//------------------------------------------------------------------------------
static int __cdecl textCompare(const void *a,const void *b)
{
	COMBOITEM *ea = (COMBOITEM *) (a);
	COMBOITEM *eb = (COMBOITEM *) (b);
	return (wcsicmp(ea->buf, eb->buf));
} 

//  Added to sort by entry ID
//------------------------------------------------------------------------------
static int __cdecl idCompare(const void *a,const void *b)
{
	COMBOITEM *ea = (COMBOITEM *) (a);
	COMBOITEM *eb = (COMBOITEM *) (b);
	return ( (ea->UserData < eb->UserData) ? -1 : ((ea->UserData > eb->UserData) ? 1 : 0) );
} 

BOOL UComboBox::SetSkin(const char *name)
{
	return FALSE;
	/*m_strIconFile = StringTable->insert(name);
	if(!isAwake())
		return;

	if (*m_strIconFile)
	{
		char buffer[1024];
		char *p;
		dStrcpy(buffer, name);
		p = buffer + dStrlen(buffer);

		dStrcpy(p, "_n");
		mTextureNormal = GFXTexHandle((StringTableEntry)buffer, &GFXDefaultGUIProfile);

		dStrcpy(p, "_d");
		mTextureDepressed = GFXTexHandle((StringTableEntry)buffer, &GFXDefaultGUIProfile);
		if (!mTextureDepressed)
			mTextureDepressed = mTextureNormal;
	}
	else
	{
		mTextureNormal = NULL;
		mTextureDepressed = NULL;
	}
	setUpdate();*/
}   

//------------------------------------------------------------------------------
void UComboBox::Sort(BOOL ByUserData)
{
	int size = m_pComboBox->mEntries.size();
	if( size > 0 )
	{
		if (ByUserData)
		{
			qsort(&m_pComboBox->mEntries[0], size, sizeof(COMBOITEM), idCompare);
		}else
		{
			qsort(&m_pComboBox->mEntries[0], size, sizeof(COMBOITEM), textCompare);
		}
		
	}
}


void UComboBox::AddItem(const WCHAR* itemText, void* pUserData /* = NULL */)
{
	if( !itemText )
	{
		return;
	}

	COMBOITEM e;
	wcsncpy(e.buf, itemText,COMBOITEM_TEXT_MAX);
	e.UserData = pUserData;
	e.IconIndex = -1;

	m_pComboBox->mEntries.push_back(e);

	if ( m_pComboBox->mInAction && m_pComboBox->GetTextList() )
	{
		// Add the new entry:
		m_pComboBox->GetTextList()->AddItem(e.buf, e.UserData);

	}
}

void* UComboBox::GetItemData(int nIndex)
{
	if (nIndex < 0 || nIndex >= m_pComboBox->mEntries.size())
	{
		return NULL;
	}
	return m_pComboBox->mEntries[nIndex].UserData;
}

void UComboBox::AddItem(const char* itemText, void* pUserData /* = NULL */)
{
	if( !itemText )
	{
		return;
	}
	int nChars = strlen(itemText);
	if (nChars == 0)
	{
		return;
	}
	UFrameMarker<WCHAR> wszBuf(nChars + 1);
	int nRet = MultiByteToWideChar(CP_UTF8, 0, itemText, nChars, wszBuf, nChars);
	if (nRet > 0)
	{
		wszBuf[nRet] = 0;
		AddItem(wszBuf, pUserData);
	}
}

int UComboBox::GetItemText(int nIndex, char* pDest, int nMaxLen)
{
	if (nIndex < 0 || nIndex >= m_pComboBox->mEntries.size())
	{
		return 0;
	}
	int Ret = WideCharToMultiByte(CP_UTF8,0, m_pComboBox->mEntries[nIndex].buf, -1, pDest,nMaxLen, NULL, NULL);
	
	return Ret;
}

int UComboBox::GetCurSel() const
{
	return m_nSelIndex;
}

int UComboBox::FindItem(const WCHAR* text) const
{
	assert(text);
	for ( UINT i = 0; i < m_pComboBox->mEntries.size(); i++ )
	{
		if ( wcscmp( text, m_pComboBox->mEntries[i].buf ) == 0 )
			return i;     
	}
	return -1;
}

int UComboBox::FindItem( const char* text ) const
{
	assert(text);
	int nStrlen = strlen(text);
	UFrameMarker<WCHAR> wszBuf(nStrlen + 1);
	int nRet = MB2WC(text, nStrlen, wszBuf, nStrlen);
	wszBuf[nRet] = 0;
	return FindItem(wszBuf);
}

void UComboBox::SetCurSel(int index)
{
	if (m_nSelIndex != index)
	{
		if (index >= m_pComboBox->mEntries.size())
		{
			UTRACE("index is invalidate");
			return;
		}
		m_nSelIndex = index;
		DispatchNotify(UCBB_SELCHANGE);
	}
}
//
////------------------------------------------------------------------------------
////  Added to set the first item as selected.
//void UComboBox::setFirstSelected()
//{
//	//if (m_pComboBox->mEntries.size() > 0)
//	//{
//	//	m_nSelIndex = 0;
//	//	if(mReplaceText) //  Only change the displayed text if appropriate.
//	//	{
//	//		setText(m_pComboBox->mEntries[0].buf);
//	//	}
//
//	//	// Now perform the popup action:
//	//	char idval[24];
//	//	dSprintf( idval, sizeof(idval), "%d", m_pComboBox->mEntries[m_nSelIndex].id );
//	//	if(isMethod("onSelect"))
//	//		Con::executef( this, "onSelect", idval, m_pComboBox->mEntries[m_nSelIndex].buf );
//	//	return;
//	//}
//
//	//if(mReplaceText) //  Only change the displayed text if appropriate.
//	//{
//	//	setText("");
//	//}
//	//m_nSelIndex = -1;
//
//	//Con::executef( this, "onCancel" );
//
//	//// Execute the popup console command:
//	//execConsoleCallback();
//	////if ( mConsoleCommand[0] )
//	////   Con::evaluate( mConsoleCommand, FALSE );
//}
//
////------------------------------------------------------------------------------
////  Added to set no items as selected.
//void UComboBox::setNoneSelected()
//{
//	//if(mReplaceText) //  Only change the displayed text if appropriate.
//	//{
//	//	setText("");
//	//}
//	//m_nSelIndex = -1;
//}

void UComboBox::OnRender(const UPoint& offset, const URect &updateRect)
{
	//updateRect;
	UPoint localStart;

	//if(mScrollDir != UScrollBar::None)
	//	autoScroll();

	int nArrowWidth = 0;

	if (m_Style->m_spSkin)
	{
		nArrowWidth = m_Style->m_spSkin->GetSkinItemRect(0)->GetWidth();
	}

	URect r(offset, updateRect.GetSize());
	r.SetWidth(updateRect.GetWidth() - nArrowWidth);
	if(m_pComboBox->mInAction)
	{
		//int l = r.left, r2 = r.right - 1;
		//int t = r.top, b = r.bottom - 1;

		//// Do we render a bitmap border or lines?
		//if(mProfile->mProfileForChildren && mProfile->mBitmapArrayRects.size())
		//{
		//	// Render the fixed, filled in border
		//	renderFixedBitmapBordersFilled(r, 3, mProfile);

		//} else
		//{
		//	//renderSlightlyLoweredBox(r, mProfile);
		//	GFX->getDrawUtil()->drawRectFill( r, mProfile->mFillColor);
		//}

		////  Draw a bitmap over the background?
		//if(mTextureDepressed)
		//{
		//	URect rect(offset, mBitmapBounds);
		//	GFX->getDrawUtil()->clearBitmapModulation();
		//	GFX->getDrawUtil()->drawBitmapStretch(mTextureDepressed, rect);
		//} else if(mTextureNormal)
		//{
		//	URect rect(offset, mBitmapBounds);
		//	GFX->getDrawUtil()->clearBitmapModulation();
		//	GFX->getDrawUtil()->drawBitmapStretch(mTextureNormal, rect);
		//}

		//// Do we render a bitmap border or lines?
		//if(!(mProfile->mProfileForChildren && mProfile->mBitmapArrayRects.size()))
		//{
		//	GFX->getDrawUtil()->drawLine(l, t, l, b, colorWhite);
		//	GFX->getDrawUtil()->drawLine(l, t, r2, t, colorWhite);
		//	GFX->getDrawUtil()->drawLine(l + 1, b, r2, b, mProfile->mBorderColor);
		//	GFX->getDrawUtil()->drawLine(r2, t + 1, r2, b - 1, mProfile->mBorderColor);
		//}

		//sm_UiRender->DrawRectFill(r, m_Style->mFillColorHL);
	}
	else
	{	// TODO: Implement
		if(mMouseOver) // TODO: Add onMouseEnter() and onMouseLeave() and a definition of mMouseOver (see guiButtonBaseCtrl) for this to work.
		{
			int l = r.left, r2 = r.right - 1;
			int t = r.top, b = r.bottom - 1;
			
			//sm_UiRender->DrawRectFill(r, m_Style->mFillColorHL);
			sm_UiRender->DrawRectFill(r, m_Style->mBorderColorHL);
		

			// Do we render a bitmap border or lines?
			/*if(!(mProfile->mProfileForChildren && mProfile->mBitmapArrayRects.size()))
			{
				GFX->getDrawUtil()->drawLine(l, t, l, b, colorWhite);
				GFX->getDrawUtil()->drawLine(l, t, r2, t, colorWhite);
				GFX->getDrawUtil()->drawLine(l + 1, b, r2, b, mProfile->mBorderColor);
				GFX->getDrawUtil()->drawLine(r2, t + 1, r2, b - 1, mProfile->mBorderColor);
			}*/
		}else
		{

			sm_UiRender->DrawRectFill( r, m_Style->mFillColor);
			sm_UiRender->DrawRect(r, m_Style->mBorderColor);
		}
	}
	
	// draw text.
	IUFont* pFont = m_Style->m_spFont;
	assert(pFont);
	const WCHAR* pwszText = NULL;
	if (m_nSelIndex != -1)
	{
		pwszText = m_pComboBox->mEntries[m_nSelIndex].buf;
	}
	if (pwszText != NULL)
	{
		int txt_w = pFont->GetStrWidth(pwszText);
		localStart.x = 0;
		// 垂直中线对齐
		localStart.y = (m_Size.y - pFont->GetHeight()) / 2;

		int LocalRight = m_Size.x - nArrowWidth;
		// 水平排列
		switch (m_Style->mAlignment)
		{
		case UT_RIGHT:
			{
				localStart.x = LocalRight - txt_w;
			}
			break;
		case UT_CENTER:
			{
				localStart.x = (LocalRight - txt_w)/2;
			}
			break;
		default:
			{
				localStart.x = m_Style->mTextOffset.x;
			}
			break;
		}

		UPoint ScreenStart = WindowToScreen(localStart);
		UColor fontColor = m_Active ? (m_pComboBox->mInAction ? m_Style->m_FontColorHL : m_Style->m_FontColor) : m_Style->m_FontColorNA;
		int len = pFont->GetBreakPos(pwszText, wcslen(pwszText), r.GetWidth(), true);
		bool bClip = len < wcslen(pwszText);
		if (bClip) --len;
		if (m_Style->mBorder)
		{
			sm_UiRender->DrawTextN(pFont, ScreenStart - UPoint(0, 1), pwszText,len, NULL,LCOLOR_BLACK);
			sm_UiRender->DrawTextN(pFont, ScreenStart - UPoint(0, -1), pwszText,len, NULL,LCOLOR_BLACK);
			sm_UiRender->DrawTextN(pFont, ScreenStart - UPoint(1, 0), pwszText,len, NULL,LCOLOR_BLACK);
			sm_UiRender->DrawTextN(pFont, ScreenStart - UPoint(-1, 0), pwszText,len, NULL,LCOLOR_BLACK);
		}
		sm_UiRender->DrawTextN(pFont, ScreenStart, pwszText, len, NULL,fontColor);
		if (bClip)
		{
			ScreenStart.x += pFont->GetStrNWidth(pwszText, len);
			sm_UiRender->DrawTextN(pFont, ScreenStart, L"...", 3, NULL,fontColor);
		}
	}

	// Draw Arrow
	UPoint ptLeftTop;
	int ArrowWidth = updateRect.GetHeight();
	int ArrowHeight = updateRect.GetHeight();
	if (m_Style->m_spSkin)
	{
		ArrowWidth =m_Style->m_spSkin->GetSkinItemRect(0)->GetSize().x;
		ArrowHeight = m_Style->m_spSkin->GetSkinItemRect(0)->GetSize().y;
	}
	ptLeftTop.x = updateRect.GetSize().x - ArrowWidth;
	ptLeftTop.y = (updateRect.GetSize().y - ArrowHeight)>>1;
	ptLeftTop = WindowToScreen(ptLeftTop);
	URect ArrowRect;
	ArrowRect.left = ptLeftTop.x;
	ArrowRect.top = ptLeftTop.y;
	ArrowRect.right = ArrowRect.left + ArrowWidth;
	ArrowRect.bottom = ArrowRect.top + ArrowHeight;
	int nSkinIndex = CBBS_ARROW_N;
	if (mMouseOver)
	{
		nSkinIndex = CBBS_ARROW_H;
	}else if (m_pComboBox->mInAction)
	{
		nSkinIndex = CBBS_ARROW_D;
	}
	sm_UiRender->DrawSkin(m_Style->m_spSkin, nSkinIndex, ArrowRect);
		//// Draw the text
		//UPoint globalStart = localToGlobalCoord(localStart);
		//UColor fontColor   = m_Active ? (mInAction ? mProfile->mFontColor : mProfile->mFontColorNA) : mProfile->mFontColorNA;
		//GFX->getDrawUtil()->setBitmapModulation(fontColor); //  was: (mProfile->mFontColor);

		////  Get the number of columns in the text
		//int colcount = getColumnCount(mText, "\t");

		////  Are there two or more columns?
		//if(colcount >= 2)
		//{
		//	char buff[256];

		//	// Draw the first column
		//	getColumn(mText, buff, 0, "\t");
		//	GFX->getDrawUtil()->drawText(mFont, globalStart, buff, mProfile->mFontColors);

		//	// Draw the second column to the right
		//	getColumn(mText, buff, 1, "\t");
		//	int txt_w = mFont->getStrWidth(buff);
		//	if(mProfile->mProfileForChildren && mProfile->mBitmapArrayRects.size())
		//	{
		//		// We're making use of a bitmap border, so take into account the
		//		// right cap of the border.
		//		URect* mBitmapBounds = mProfile->mBitmapArrayRects.address();
		//		UPoint textpos = localToGlobalCoord(UPoint(getWidth() - txt_w - mBitmapBounds[2].extent.x,localStart.y));
		//		GFX->getDrawUtil()->drawText(mFont, textpos, buff, mProfile->mFontColors);

		//	} else
		//	{
		//		UPoint textpos = localToGlobalCoord(UPoint(getWidth() - txt_w - 12,localStart.y));
		//		GFX->getDrawUtil()->drawText(mFont, textpos, buff, mProfile->mFontColors);
		//	}

		//} else
		//{
		//	GFX->getDrawUtil()->drawText(mFont, globalStart, mText, mProfile->mFontColors);
		//}

		//// If we're rendering a bitmap border, then it will take care of the arrow.
		//if(!(mProfile->mProfileForChildren && mProfile->mBitmapArrayRects.size()))
		//{
		//	//  Draw a triangle (down arrow)
		//	int left = r.point.x + r.extent.x - 12;
		//	int right = left + 8;
		//	int middle = left + 4;
		//	int top = r.extent.y / 2 + r.point.y - 4;
		//	int bottom = top + 8;

		//	PrimBuild::color( mProfile->mFontColor );

		//	PrimBuild::begin( GFXTriangleList, 3 );
		//	PrimBuild::vertex2fv( Point3F( (float)left, (float)top, 0.0f ) );
		//	PrimBuild::vertex2fv( Point3F( (float)right, (float)top, 0.0f ) );
		//	PrimBuild::vertex2fv( Point3F( (float)middle, (float)bottom, 0.0f ) );
		//	PrimBuild::end();
		//}
}

//------------------------------------------------------------------------------
BOOL UComboBox::onKeyDown(const GuiEvent &event)
{
	//if the control is a dead end, don't process the input:
	if ( !m_Visible || !m_Active || !m_bCreated )
		return FALSE;

	//see if the key down is a <return> or not
	//if ( event.keyCode == KEY_RETURN && event.modifier == 0 )
	//{
	//	onAction();
	//	return true;
	//}

	////otherwise, pass the event to its parent
	//return Parent::onKeyDown( event );
	return TRUE;
}

//------------------------------------------------------------------------------
void UComboBox::onAction()
{
	m_pComboBox->CreateControls();
	assert(m_pComboBox && m_pComboBox->mBackground);
	m_pComboBox->mBackground->Popup();
}


void UComboBox::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	onAction();
}

void UComboBox::OnMouseUp(const UPoint& position, UINT flags)
{
}

void UComboBox::OnMouseEnter(const UPoint& position, UINT flags)
{
	mMouseOver = true;
}

void UComboBox::OnMouseLeave(const UPoint& position, UINT flags)
{
	mMouseOver = FALSE;
}

//------------------------------------------------------------------------------
void UComboBox::setupAutoScroll(const GuiEvent &event)
{
	/*UControl *parent = getParent();
	if (! parent) return;

	UPoint mousePt = m_pComboBox->ScrollBar->globalToLocalCoord(event.mousePoint);

	mEventSave = event;      

	if(mLastYvalue != mousePt.y)
	{
		mScrollDir = UScrollBar::None;
		if(mousePt.y > m_pComboBox->ScrollBar->getHeight() || mousePt.y < 0)
		{
			int topOrBottom = (mousePt.y > m_pComboBox->ScrollBar->getHeight()) ? 1 : 0;
			m_pComboBox->ScrollBar->scrollTo(0, topOrBottom);
			return;
		}   

		float percent = (float)mousePt.y / (float)m_pComboBox->ScrollBar->getHeight();
		if(percent > 0.7f && mousePt.y > mLastYvalue)
		{
			mIncValue = percent - 0.5f;
			mScrollDir = UScrollBar::DownArrow;
		}
		else if(percent < 0.3f && mousePt.y < mLastYvalue)
		{
			mIncValue = 0.5f - percent;         
			mScrollDir = UScrollBar::UpArrow;
		}
		mLastYvalue = mousePt.y;
	}*/
}

//------------------------------------------------------------------------------
void UComboBox::autoScroll()
{
	/*mScrollCount += mIncValue;

	while(mScrollCount > 1)
	{
		m_pComboBox->ScrollBar->autoScroll(mScrollDir);
		mScrollCount -= 1;
	}
	m_pComboBox->TextList->onMouseMove(mEventSave);*/
}

