#include "StdAfx.h"
#include "UControl.h"
#include "USystem.h"
#include "UDesktop.h"

USystem* UControl::sm_System = NULL;
URender* UControl::sm_UiRender = NULL;

UIMP_CLASS(UControl,UCmdTarget);

void UControl::StaticInit()
{
	UREG_PROPERTY("style", UPT_CTRLSTYLE, UFIELD_OFFSET(UControl,m_Style));
	UREG_PROPERTY("AnchorHori", UPT_INT, UFIELD_OFFSET(UControl,mHorizSizing)); 
	UREG_PROPERTY("AnchorVert",	UPT_INT,	UFIELD_OFFSET(UControl,mVertSizing));
	UREG_PROPERTY("Position",	UPT_POINT,	UFIELD_OFFSET(UControl,m_Position));
	UREG_PROPERTY("Extent",	UPT_POINT,	UFIELD_OFFSET(UControl,m_Size));
	UREG_PROPERTY("MinExtent",	UPT_POINT,	UFIELD_OFFSET(UControl,m_MinSize));
	UREG_PROPERTY("Visible", UPT_BOOL, UFIELD_OFFSET(UControl,m_Visible));
	UREG_PROPERTY("CanEscapeFouce", UPT_BOOL, UFIELD_OFFSET(UControl,m_CanEscapeFocuse));
	UREG_PROPERTY("Accelerator",UPT_STRING,UFIELD_OFFSET(UControl,mAcceleratorKey));
	UREG_PROPERTY("tooltipprofile",UPT_CTRLSTYLE,UFIELD_OFFSET(UControl,mTooltipProfile));
	UREG_PROPERTY("tooltip",UPT_STRING,UFIELD_OFFSET(UControl,m_ToolTips));
	UREG_PROPERTY("hovertime",UPT_INT,UFIELD_OFFSET(UControl,mTipHoverTime));

	// //  RegisterField("Profile",           TypeGuiProfile,		UFIELD_OFFSET(m_Style, UControl));
	//   RegisterField("AnchorHori",       UPT_INT,         UFIELD_OFFSET(UControl,mHorizSizing));
	//   RegisterField("AnchorVert",        UPT_INT,         UFIELD_OFFSET(UControl,mVertSizing));
	//
	//   RegisterField("PositiOn",          UPT_POINT,      UFIELD_OFFSET(UControl,m_Position));
	//   RegisterField("Extent",            UPT_POINT,      UFIELD_OFFSET(UControl,m_Size));
	//   RegisterField("MinExtent",         UPT_POINT,      UFIELD_OFFSET(UControl,m_MinSize));
	//   //RegisterField("canSave",           UPT_BOOL,         UFIELD_OFFSET(UControl,mCanSave));
	//   //RegisterField("Visible",           UPT_BOOL,         UFIELD_OFFSET(UControl,m_Visible));
	//   //addDepricatedField("Modal");
	//   //addDepricatedField("SetFocusControl");
	//
	//   //RegisterField("Variable",          UPT_STRING,       UFIELD_OFFSET(UControl,mCOnsoleVariable));
	//   //RegisterField("Command",           UPT_STRING,       UFIELD_OFFSET(UControl,m_CommandScript));
	//   //RegisterField("AltCommand",        UPT_STRING,       UFIELD_OFFSET(UControl,mAltCOnsoleCommand));
	//   //RegisterField("Accelerator",       UPT_STRING,       UFIELD_OFFSET(UControl,mAcceleratorKey));
	//  // endGroup("Parent");	
	//
	//   //addGroup("ToolTip");
	//   ////RegisterField("tooltipprofile",    TypeGuiProfile,   UFIELD_OFFSET(mTooltipProfile, UControl));
	//   //RegisterField("tooltip",           UPT_STRING,       UFIELD_OFFSET(UControl,m_ToolTips));
	//   //RegisterField("hovertime",         UPT_INT,       UFIELD_OFFSET(UControl,mTipHoverTime));
	//  // endGroup("ToolTip");
	//
	//
	//  // addGroup("I18N");
	//  // //RegisterField("langTableMod",      UPT_STRING,       UFIELD_OFFSET(mLangTableName, UControl));
	//  // endGroup("I18N");
}

UBEGIN_MESSAGE_MAP(UControl, UCmdTarget)
UEND_MESSAGE_MAP()

UControl::UControl()
{
	m_nCtrlID = 0;
	m_Layer = 0;
	m_Position.Set(0,0);
	m_Size.Set(64,64);
	m_MinSize.Set(2, 2);	
	m_Style = NULL;
	m_Parent = NULL;
	mAcceleratorKey	= NULL;
	m_Visible	= TRUE;
	m_Active	= TRUE;
	m_bCreated	= FALSE;
	mHorizSizing         = UAH_RELATIVE;
	mVertSizing          = UHV_RELATIVE;
	mTooltipProfile      = NULL;
	m_ToolTips             = NULL;
	mTipHoverTime        = 1000;
	m_CanEscapeFocuse = FALSE;
	m_MouseHover = FALSE;
	mBHoverTime = 0.0f;
	mTipRender = FALSE;

	mBGlint = FALSE;
	mGlintTime = 0.0f;
	m_IsUpdateChild = TRUE;
	for(int i = 0 ; i < 4 ; i++) mPoPo[i] = NULL;
}

UControl::~UControl()
{
	if (m_bCreated)
	{
		Destroy();
	}
	if (!m_Childs.empty())
	{
		for (int c =0; c < m_Childs.size(); c++)
		{
			delete m_Childs[c];
		}
	}
}

BOOL UControl::SetId(UINT id)
{
	m_nCtrlID = id;
	return TRUE;
}

UControl* UControl::GetChildByID(INT nID) const
{
	if (!m_bCreated)
	{
		return NULL;
	}
	if (!m_Childs.empty())
	{
		for (int i = 0; i <m_Childs.size(); i++ )
		{
			if (m_Childs[i]->GetId() == nID)
			{
				return m_Childs[i];
			}
		}
	}

	return NULL;
}

void UControl::OnChildAdded( UControl *child )
{

}

struct EnumMap
{
	UINT EnumValue;
	const char* StrValue;
};

static EnumMap horzEnums[] =
{
	{ UAH_RIGHT,      "right"     },
	{ UAH_WIDTH,      "width"     },
	{ UAH_LEFT,       "left"      },
	{ UAH_CENTER,     "center"    },
	{ UAH_RELATIVE,   "relative"  }
};
//static EnumTable gHorizSizingTable(5, &horzEnums[0]);

static EnumMap vertEnums[] =
{
	{ UHV_BOTTOM,      "bottom"     },
	{ UHV_HEIGHT,      "height"     },
	{ UHV_TOP,         "top"        },
	{ UHV_CENTER,      "center"     },
	{ UHV_RELATIVE,    "relative"   }
};
//static EnumTable gVertSizingTable(5, &vertEnums[0]);

void UControl::AddChild(UControl* pCtrl)
{
	if(pCtrl->GetParent() == this)
		return;

	if (pCtrl->GetParent())
	{
		pCtrl->GetParent()->RemoveChild(pCtrl);
	}

	if(m_bCreated)
	{
		if (!pCtrl->Create())
		{
			UTRACE("AddChild failed.");
			return;
		}
	}
	m_Childs.push_back(pCtrl);
	pCtrl->m_Parent = this;
	OnChildAdded( pCtrl );
}

void UControl::RemoveChild(UControl *pCtrl)
{
	assert(pCtrl);
	if (pCtrl->m_bCreated)
	{
		pCtrl->Destroy();
	}
	pCtrl->m_Parent = NULL;
	if (!m_Childs.empty())
	{
		std::vector<UControl*>::iterator it = m_Childs.begin();
		std::vector<UControl*>::iterator End = m_Childs.end();
		for (; it != End; ++it )
		{
			if ( *it == pCtrl)
			{
				m_Childs.erase(it);
				break;
			}
		}
	}
	OnChildRemoved(pCtrl);
}

UControl *UControl::GetParent() const
{
	return m_Parent;
}

BOOL UControl::MoveChildTo(UControl* pChild, UControl* pInsertBerfore)
{
	if (pChild == pInsertBerfore)
	{
		return TRUE;
	}

	CtrlIterator itSrc, itDest;
	itSrc = std::find(m_Childs.begin(), m_Childs.end(), pChild);
	if (itSrc == m_Childs.end())
	{
		return FALSE;	// pChild 不在队列中
	}


	if ( pInsertBerfore == NULL)    // if no target, then put to back of list
	{
		if ( itSrc != (m_Childs.end()-1) )      // don't move if already last object
		{
			m_Childs.erase(itSrc);    // remove object from its current location
			m_Childs.push_back(pChild); // push it to the back of the list
		}
	}
	else              // if target, insert object in front of target
	{
		if ( (itDest = std::find(m_Childs.begin(),m_Childs.end(),pInsertBerfore)) == m_Childs.end() )
			return FALSE;              // target must be in list

		m_Childs.erase(itSrc);

		//Tinman - once itrS has been erased, itrD won't be pointing at the same place anymore - re-find...
		itDest = std::find(m_Childs.begin(),m_Childs.end(),pInsertBerfore);
		m_Childs.insert(itDest,pChild);

		// Notify On ZChange???
	}

	return TRUE;
}

UControl* UControl::GetRoot()
{
	UControl *root = NULL;
	UControl *parent = GetParent();
	while (parent)
	{
		root = parent;
		parent = parent->GetParent();
	}
	return root;
}

BOOL UControl::IsDesktop() const
{
	return FALSE;	
}

UPoint UControl::WindowToScreen(const UPoint &src) const
{
	UPoint ret = src;
	ret += m_Position;
	UControl *walk = GetParent();
	while(walk)
	{
		ret += walk->GetWindowPos();
		walk = walk->GetParent();
	}
	return ret;
}

UPoint UControl::ScreenToWindow(const UPoint &src) const
{
	UPoint ret = src;
	ret -= m_Position;
	UControl *walk = GetParent();
	while(walk)
	{
		ret -= walk->GetWindowPos();
		walk = walk->GetParent();
	}
	return ret;
}

inline void AlignmentChild(const UPoint& OldParentSize, const UPoint& NewParentSize,
					const UPoint& OldChildPos, const UPoint& OldChildSize,
					EUAnchorHori HorizAnchor, EUAnchorVert VertAnchor,
					UPoint& NewPos, UPoint& NewSize)
{
	NewPos = OldChildPos;
	NewSize = OldChildSize;
	
	INT deltaX = NewParentSize.x - OldParentSize.x;
	INT deltaY = NewParentSize.y - OldParentSize.y;

	if (HorizAnchor == UAH_WIDTH)
	{
		NewSize.x += deltaX;
	}
	else if (HorizAnchor == UAH_CENTER)
	{
		NewPos.x = (NewParentSize.x - OldChildSize.x) >> 1;
	}
	else if(HorizAnchor == UAH_LEFT)
	{
		NewPos.x += deltaX;
	}else if (HorizAnchor == UAH_RIGHT)
	{
		NewSize.x;
	}
	else if (HorizAnchor == UAH_RELATIVE && OldParentSize.x != 0)
	{
		float sizeNum = 1.0f / OldParentSize.x;
		float newLeft = float(NewPos.x * NewParentSize.x) * sizeNum;
		float newRight = float((NewPos.x + NewSize.x) * NewParentSize.x) * sizeNum;

		NewPos.x = newLeft + 0.5f;
		NewSize.x = newRight - newLeft + 0.5f;
	}else
	{
		// default  width
		NewSize.x += deltaX;
	}

	if (VertAnchor == UHV_HEIGHT)
	{
		NewSize.y += deltaY;
	}
	else if (VertAnchor == UHV_CENTER)
	{
		NewPos.y = (NewParentSize.y - OldChildSize.y) >> 1;
	}
	else if (VertAnchor == UHV_TOP)
	{
		NewPos.y += deltaY;
	}else if (VertAnchor == UHV_BOTTOM)
	{
		NewSize.y ; //大小不需变化
	}
	else if(VertAnchor == UHV_RELATIVE && OldParentSize.y != 0)
	{
		float sizeNum = 1.0f / OldParentSize.y;
		float newTop = float(NewPos.y * NewParentSize.y) * sizeNum;
		float newBottom = float((NewPos.y + NewSize.y) * NewParentSize.y) * sizeNum;

		NewPos.y = INT(newTop + 0.5f);
		NewSize.y = INT(newBottom - newTop + 0.5f);
	}else
	{
		// default height.
		NewSize.y += deltaY;
	}
}

void UControl::ResizeControl(const UPoint& NewPos, const UPoint& _NewSize)
{
	const UPoint& MinSize = m_MinSize;
	UPoint NewSize = UPoint(__max(MinSize.x, _NewSize.x),
		__max(MinSize.y, _NewSize.y));

	BOOL SizeChanged = (NewSize != m_Size);
	BOOL PosChanged = (NewPos != m_Position);

	if (!SizeChanged && !PosChanged ) 
		return;

	if( SizeChanged )
	{
		//URect NewRect(NewPos, NewSize);
		// 更新子窗口大小
		UPoint NewChildPos;
		UPoint NewChildSize;
		for (int i =0; i < m_Childs.size(); i++)
		{
			UControl* pChild = m_Childs[i];
			AlignmentChild(m_Size, NewSize,
				pChild->m_Position, pChild->m_Size,
				(EUAnchorHori)pChild->mHorizSizing, (EUAnchorVert)pChild->mVertSizing,
				NewChildPos, NewChildSize);
			pChild->ResizeControl(NewChildPos, NewChildSize);
		}
		m_Size = NewSize;
		OnSize(NewSize);
		//	if (m_Parent)
		//			m_Parent->OnChildResized(this);
	}

	// Update Position
	if( PosChanged )
	{
		UPoint Offset = NewPos - m_Position;
		m_Position = NewPos;
		OnMove(Offset);
	}

	OnChange(SizeChanged || PosChanged);
}

void UControl::SetPosition( const UPoint &NewPos )
{
	ResizeControl( NewPos, m_Size );
}

void UControl::SetSize( const UPoint &newExtent )
{
	ResizeControl( m_Position, newExtent );
}

void UControl::SetWindowRect( const URect &newBounds )
{
	ResizeControl( newBounds.GetPosition(), newBounds.GetSize() );
}

void UControl::SetLeft( INT newLeft )
{
	ResizeControl( UPoint( newLeft, m_Position.y), m_Size );
}

void UControl::SetTop( INT newTop )
{
	ResizeControl( UPoint( m_Position.x, newTop ), m_Size );
}

void UControl::SetBottom( INT newBottom)
{
	ResizeControl( UPoint( m_Position.x, newBottom - m_Size.y ), m_Size );
}

void UControl::SetWidth( INT newWidth )
{
	ResizeControl( m_Position, UPoint( newWidth, m_Size.y ) );
}

void UControl::SetHeight( INT newHeight )
{
	ResizeControl( m_Position, UPoint( m_Size.x, newHeight ) );
}


void UControl::OnSize(const UPoint& NewSize)
{
	
}

void UControl::OnChange(bool bResized)
{
	for (int i =0; i < m_Childs.size(); i++)
	{
		UControl* pChild = m_Childs[i];

		pChild->OnChange(bResized);
	}
}

void UControl::OnShow(BOOL bShow)
{
}

void UControl::OnMove(const UPoint& NewOffsetDelta)
{
	for (int i =0; i < m_Childs.size(); i++)
	{
		UControl* pChild = m_Childs[i];
		
		pChild->OnMove(NewOffsetDelta);
	}
}

void UControl::OnTimer(float fDeltaTime)
{
	if (!m_bCreated)
		return;

	if (m_MouseHover)
	{
		mBHoverTime += fDeltaTime;
		mTipRender = mBHoverTime >= mTipHoverTime * 0.001f;
	}
	else
	{
		mBHoverTime = 0.0f;
		mTipRender = FALSE;
	}

	if(!m_IsUpdateChild)
		return;

	if (mBGlint)
	{
		mGlintTime += fDeltaTime;
	}else
	{
		mGlintTime = 0.0f;
	}
	int csize = m_Childs.size();
	for (int i = 0; i < csize; i++)
	{
		UControl* pChildCtrl = m_Childs[i];
		if (pChildCtrl->m_Active && pChildCtrl->m_bCreated)
		{
			pChildCtrl->OnTimer(fDeltaTime);
		}
	}
}

void UControl::DrawControlEdge(const URect& rcCtrl)
{
	sm_UiRender->DrawRect(rcCtrl, m_Style->mBorderColor);
}
void UControl::SetGlint(BOOL bGlint)
{
	mBGlint = bGlint;
}
void UControl::RenderGlint(const URect &updateRect, float fTime, UINT Speed, float alpha)
{
//	UTexturePtr pTexture[4];
//
//	pTexture[0]= sm_UiRender->LoadTexture("Data\\UI\\Core\\star_b_1.tga");
//	pTexture[1]= sm_UiRender->LoadTexture("Data\\UI\\Core\\star_b_2.tga");
//	pTexture[2]= sm_UiRender->LoadTexture("Data\\UI\\Core\\star_b_3.tga");
//	pTexture[3]= sm_UiRender->LoadTexture("Data\\UI\\Core\\star_b_4.tga");
//
//	if (!pTexture[0] || !pTexture[1] || !pTexture[2] || !pTexture[3])
//	{
//		return ;
//	}
//
//#define  GlintX 32 
//#define  GlintY 32
//
//	URect ClipRec;
//	ClipRec.SetPosition(updateRect.GetPosition() - UPoint(GlintX / 2, GlintY / 2));
//	ClipRec.SetSize(updateRect.GetSize() + UPoint(GlintX, GlintY));
//	sm_UiRender->SetClipRect(ClipRec);
//
//	UPoint GlintPos = ClipRec.GetPosition();
//
//
//	UINT hight =	ClipRec.GetHeight() - GlintY / 2;   //高度
//	UINT width =	ClipRec.GetWidth() - GlintX / 2;   //宽度
//
//	URect uRect ;
//
//	uRect.SetSize(GlintX, GlintY);
//
//	UINT Len = Speed * fTime;
//
//	UColor Color = UColor(233,196,23);
//
//	BOOL bNeed = Len /(2 * (hight + width)- GlintY - GlintX); // 
//	URect uvrect;
//
//	Len = Len % (2 * (hight + width) - GlintX - GlintY);
//
//	if (Len >= 0 && Len < width - GlintX / 2)
//	{
//		GlintPos.x += Len;	
//
//	}else if (Len >= width - GlintX / 2 && Len < (width  - GlintX / 2 + hight - GlintY / 2))
//	{
//		GlintPos.x += width  - GlintX / 2;
//		GlintPos.y += Len - (width - GlintX / 2) ;
//		//Angle = UIPI / 2;
//		
//
//	}else if (Len >= (width  - GlintX / 2 + hight - GlintY / 2) && Len < (2 * width - GlintX + hight - GlintY / 2))
//	{
//		GlintPos.x += 2 * width  - GlintX + hight - GlintY / 2 - Len;
//		GlintPos.y += hight - GlintY / 2;
//		//Angle = UIPI ;
//
//	}else 
//	{
//		GlintPos.y += 2 * (width + hight) - GlintX - GlintY - Len;	
//		//Angle = UIPI + UIPI / 2 ;
//	}
//
//	uRect.SetPosition(GlintPos);
//	UINT index =  rand() % 4 ;
//	sm_UiRender->DrawImage(pTexture[index],uRect,Color,1);
}
void UControl::OnRenderGlint(const UPoint& offset,const URect &updateRect)
{
	if (mBGlint)
	{
		//do nothing
	}

}
void UControl::OnRender(const UPoint& offset, const URect &updateRect)
{
	if (!m_Visible)
	{
		return;
	}
	URect ctrlRect(offset, m_Size);

	//if opaque, fill the update rect with the fill color
	if (m_Style->mOpaque)
		sm_UiRender->DrawRectFill(ctrlRect, m_Style->mFillColor);

	//if there's a border, draw the border
	if (m_Style->mBorder)
	{
		DrawControlEdge(ctrlRect);
	}
	
	OnRenderGlint(offset,updateRect); 

	RenderChildWindow(offset, updateRect);
}

BOOL UControl::renderTooltip(const UPoint& cursorPos, const char* tipText )
{	
	if (!m_Active)
		return FALSE;
	//for (int i = 0; i < m_Childs.size(); i++)
	//{
	//	UControl* pChildCtrl = m_Childs[i];
	//	if (pChildCtrl->m_Visible)
	//	{
	//		if (pChildCtrl->renderTooltip(cursorPos,tipText))
	//			return TRUE;
	//	}
	//}
	// Short Circuit.
	if (!mTipRender)
		return FALSE;
	if (!m_bCreated) 
	   return FALSE;
	
	if (m_ToolTips.GetLength() == 0  && ( tipText == NULL || strlen( tipText ) == 0 ) )
	   return FALSE;

	UStringW renderTip;
	if( m_ToolTips.GetBuffer() == NULL )
	   renderTip.Set(tipText);
	else
		renderTip.Set(m_ToolTips.GetBuffer());
	
	if (mAcceleratorKey.GetBuffer())
	{	
		std::string keystr = mAcceleratorKey.GetBuffer();
		int l = keystr.find(';');
		keystr = keystr.substr(0,l);
		UAccelarKey* pkey = sm_System->FindAcceleratorKey(keystr.c_str());
		if (pkey)
		{
			char buf[1024];
			sprintf(buf,"(%s)",pkey->mAccelarKeyCode.GetBuffer());
			UStringW ackey;
			ackey.Set(buf);
			renderTip.Append(ackey);
		}
	}

	//// Need to have root.
	UDesktop *root = (UDesktop*)GetRoot();
	if ( !root )
	   return FALSE;

	if (!mTooltipProfile)
	   mTooltipProfile = m_Style;

	IUFont *Font = mTooltipProfile->m_spFont;
	//
	////Vars used:
	////Screensize (for positiOn check)
	////UFIELD_OFFSET to Get positiOn of cursor
	////textBounds for text extent.
	UPoint offset(cursorPos.x + 10, cursorPos.y - 45);

	//UFIELD_OFFSET below cursor image
	//offset.y += root->GetCursorExtent().y; // Commented out for TGEA -patw
	//Create text bounds.
	INT textWidth = Font->GetStrWidth(renderTip);
	UPoint textBounds(textWidth, Font->GetHeight() + 4);

	// Check positiOn/width to make sure all of the tooltip will be rendered
	// 5 is given as a buffer against the edge
	if (root->GetWidth() < offset.x + textBounds.x + 5)
	   offset.x = root->GetWidth() - textBounds.x - 5;

	// And ditto for the height
	if(root->GetHeight() < offset.y + textBounds.y + 5)
	   offset.y = cursorPos.y - textBounds.y - 5;
	
	URect oldClip = sm_UiRender->GetClipRect();

	// Set rectangle for the box, and Set the clip rectangle.
	//Change This render to destop,so we didn't Need this Clip
	//sm_UiRender->SetClipRect(rect);

	// Draw Filler bit, then border On top of that
	/*
	sm_UiRender->DrawRectFill(rect, mTooltipProfile->mFillColor );
	sm_UiRender->DrawRect( rect, mTooltipProfile->mBorderColor );*/
	if(offset.x > 0 && offset.y > 0)
	{
		UDrawResizeBitmap(sm_UiRender,mPoPo[0],offset,textBounds);
	}
	else
	{ 
		offset.y +=  textBounds.y + 70;
		UDrawResizeBitmap(sm_UiRender,mPoPo[1],offset,textBounds);
	}
	
	// Draw the text centered in the tool tip box
	//sm_UiRender->SetBitmapModulatiOn( mTooltipProfile->m_FontColor );
	UPoint start(( textBounds.x - textWidth) * 0.5f - 8, ( textBounds.y - Font->GetHeight() ) * 0.5f );
	sm_UiRender->DrawTextN(Font, start + offset, renderTip, renderTip.GetLength(), NULL, mTooltipProfile->m_FontColor);
	sm_UiRender->SetClipRect( oldClip );
	return TRUE;
}

void UControl::RenderChildWindow(const UPoint& offset, const URect &updateRect)
{
	// offset is the upper-left corner of this cOntrol in screen coordinates
	// updateRect is the intersectiOn rectangle in screen coords of the cOntrol
	// hierarchy.  This can be Set as the clip rectangle in most cases.
	int csize = m_Childs.size();
	for (int i = 0; i < csize; i++)
	{
		UControl* pChildCtrl = m_Childs[i];
		if (pChildCtrl->m_Visible)
		{
			UPoint ChildPos(offset.x + pChildCtrl->m_Position.x, offset.y + pChildCtrl->m_Position.y);
			URect ChildRect( ChildPos.x, ChildPos.y, ChildPos.x+ pChildCtrl->m_Size.x, ChildPos.y + pChildCtrl->m_Size.y );

			if (ChildRect.SetIntersect(updateRect))
			{
				sm_UiRender->SetClipRect( ChildRect );
				pChildCtrl->OnRender( ChildPos, ChildRect );
			}
		}
	}
}

BOOL UControl::Create()
{
	assert(!m_bCreated);
	if(m_bCreated)
		return TRUE;

	for (int i = 0; i < m_Childs.size(); i++)
	{
		UControl* pChild = m_Childs[i];
		assert(!pChild->IsCreated());
		if (!pChild->IsCreated())
		{
			if(!pChild->Create())
			{
				return FALSE;
			}
		}
	}

	assert(!m_bCreated);
	if(!m_bCreated)
	{
		if(!OnCreate())
		{
			DeleteThis();
			return FALSE;
		}
	}
	return TRUE;
}

void UControl::Destroy()
{
	assert(m_bCreated);
	if(!m_bCreated)
		return;

	for (int i = 0; i < m_Childs.size(); i++)
	{
		UControl* pChild = m_Childs[i];
		assert(pChild->IsCreated());
		if (pChild->IsCreated())
		{
			pChild->Destroy();
		}
	}
	assert(m_bCreated);
	if(m_bCreated)
		OnDestroy();
}

BOOL UControl::OnCreate()
{
	assert(m_bCreated == 0);
	if( m_bCreated )
		return FALSE;

	// [tom, 4/18/2005] Cause mLangTable to be refreshed in case it was changed
	//   mLangTable = NULL;

	//make sure we have a profile
	if( !m_Style )
	{
		// First lets try to Get a profile based On the classname of this object
		//const char *cName = GetClassName();

		//// Ensure this is a valid name...
		//if( cName && cName[0] )
		//{
		//   INT pos = 0;

		//   for( pos = 0; pos <= strlen( cName ); pos++ )
		//      if( !strncmp( cName + pos, "Ctrl", 4 ) )
		//         break;

		//   if( pos != 0 ) {
		//      char buff[255];
		//      strncpy( buff, cName, pos );
		//      buff[pos] = '\0';
		//      strcat( buff, "Profile\0" );

		//      UObject *obj = Sim::findObject( buff );

		//      if( obj )
		//         m_Style = dynamic_cast<UControlStyle*>( obj );
		//   }
		//}

		//// Ok lets check to see if that worked
		//if( !m_Style ) {
		//   UObject *obj = Sim::findObject( "GuiDefaultProfile" );

		//   if( obj )
		//      m_Style = dynamic_cast<UControlStyle*>(obj);
		//}

		//AssertFatal( m_Style, avar( "UControl: %s created with no profile.", GetName() ) );
	}

	//Set the flag
	m_bCreated = TRUE;

	//Set the layer
	UControl *parent = GetParent();
	if (parent && !parent->IsDesktop())
		m_Layer = parent->m_Layer;

	//see if we should force this cOntrol to be the first respOnder
	if (m_Style->m_bTabStop && m_Style->m_bKeyFocus)
		SetFocusControl();

	m_Style->AddRef();

	if (!mPoPo[0])
	{
		mPoPo[0] = sm_System->LoadSkin("Talkpopo\\popoLB.skin");
		mPoPo[1] = sm_System->LoadSkin("Talkpopo\\popoLT.skin");
		mPoPo[2] = sm_System->LoadSkin("Talkpopo\\popoRB.skin");
		mPoPo[3] = sm_System->LoadSkin("Talkpopo\\popoRT.skin");
	}
	
	
	return TRUE;
}

void UControl::OnDestroy()
{
	assert(m_bCreated);
	if(!m_bCreated)
		return;

	if( m_Style != NULL )
		m_Style->Release();
	ClearFocus();
	ReleaseCapture();
	if (IsDragSrcCtrl())
	{
		EndDragDrop();
	}
	m_bCreated = FALSE;
	mTipRender = FALSE;
}

void UControl::SetControlStyle(UControlStyle* pStyle)
{
	assert(pStyle);
	if(pStyle == m_Style)
		return;
	if(m_bCreated)
		m_Style->Release();
	m_Style = pStyle;
	if(m_bCreated)
		m_Style->AddRef();

}


void UControl::OnRemove()
{
	// Only invoke script callbacks if they can be received
	/*  if( isMethod("OnRemove") )
	Con::executef(this, 1, "OnRemove");*/

	ClearFocus();

	//Parent::OnRemove();

	// If we are a child, notify our parent that we've been removed
	UControl *parent = GetParent();
	if( parent )
		parent->OnChildRemoved( this );


}

void UControl::OnChildRemoved( UControl *child )
{
	;
}

void UControl::SetSizing(INT horz, INT vert)
{
	mHorizSizing = horz;
	mVertSizing = vert;
}


BOOL UControl::CursorInWindow()
{
	assert(sm_System);
	if(!m_Visible)
	{
		return FALSE;
	}
	UPoint pt = sm_System->GetCursorPos();
	UPoint offset = WindowToScreen(UPoint(0, 0));
	if (pt.x >= offset.x && pt.y >= offset.y &&
		pt.x < offset.x + m_Size.x && pt.y < offset.y + m_Size.y)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

BOOL UControl::HitTest(const UPoint& parentCoordPoint)
{
	INT xt = parentCoordPoint.x - m_Position.x;
	INT yt = parentCoordPoint.y - m_Position.y;
	return xt >= 0 && yt >= 0 && xt < m_Size.x && yt < m_Size.y;
}

UControl* UControl::FindHitWindow(const UPoint &pt, INT initialLayer)
{
	int Last = m_Childs.size() - 1;
	for (; Last >= 0; Last--)
	{
		UControl* pCtrl = m_Childs[Last];
		if (initialLayer >= 0 && pCtrl->m_Layer > initialLayer && !pCtrl->m_bCreated)
		{
			continue;
		}else if(pCtrl->m_Visible && pCtrl->HitTest(pt))
		{
			UPoint ptemp = pt - pCtrl->m_Position;
			UControl* hitCtrl = pCtrl->FindHitWindow(ptemp);
			return hitCtrl;
		}
	}
	return this;
}


BOOL UControl::IsCaptured() const
{
	return sm_System ? sm_System->GetCapturedCtrl() == this : FALSE;
}

BOOL UControl::IsDragSrcCtrl() const
{
	return g_Desktop ? g_Desktop->GetDragSrcCtrl() == this : FALSE;
}

void UControl::CaptureControl(UControl* pCapTarget)
{
	if (sm_System)
		sm_System->CaptureControl(pCapTarget);
}

void UControl::CaptureControl()
{
	if (sm_System)
		sm_System->CaptureControl(this);
}

void UControl::ReleaseCapture()
{
	if (sm_System)
		sm_System->ReleaseCapture(this);
}

void UControl::OnMouseUp(const UPoint& position, UINT flags)
{
}

void UControl::OnMouseDown(const UPoint& pt, UINT nRepCnt, UINT uFlags)
{
}

void UControl::OnMouseMove(const UPoint& position, UINT flags)
{
	//if this cOntrol is a dead end, make sure the event stops here
	if ( !m_Visible || !m_bCreated )
		return;

	//pass the event to the parent
	UControl *parent = GetParent();
	if ( parent )
		parent->OnMouseMove( position, flags );
}

void UControl::OnMouseDragged(const UPoint& position, UINT flags)
{
	m_MouseHover = FALSE;
}

void UControl::OnMouseEnter(const UPoint& position, UINT flags)
{
	m_MouseHover = TRUE;
}

void UControl::OnMouseLeave(const UPoint& position, UINT flags)
{
	m_MouseHover = FALSE;
}

BOOL UControl::OnMouseWheel(const UPoint& position, int delta, UINT flags)
{
	//if this cOntrol is a dead end, make sure the event stops here
	if ( !m_Visible || !m_bCreated )
		return TRUE;

	//pass the event to the parent
	UControl *parent = GetParent();
	if ( parent )
		return parent->OnMouseWheel(position, delta, flags);
	else
		return FALSE;
}

void UControl::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
}

void UControl::OnRightMouseUp(const UPoint& position, UINT flags)
{
}

void UControl::OnRightMouseDragged(const UPoint& position, UINT flags)
{
}

void UControl::OnMiddleMouseDown(const UPoint& position, UINT flags)
{
}

void UControl::OnMiddleMouseUp(const UPoint& position, UINT flags)
{
}

void UControl::OnMiddleMouseDragged(const UPoint& position, UINT flags)
{
}


void UControl::OnDragDropBegin(const void* DragData, UINT nDataFlag)
{

}

void UControl::OnDragDropEnd(UControl* pDragTo, void* DragData, UINT nDataFlag)
{
	ReleaseCapture();
}

void UControl::OnDeskTopDragBegin(UControl* pDragFrom, const void* pDragData, UINT nDataFlag)
{
	int csize = m_Childs.size();
	for (int i = 0; i < csize; i++)
	{
		UControl* pChildCtrl = m_Childs[i];
		if (pChildCtrl->m_Visible)
		{
			pChildCtrl->OnDeskTopDragBegin( pDragFrom, pDragData, nDataFlag );
		}
	}
}

void UControl::OnDeskTopDragEnd(UControl* pDragFrom, const void* pDragData, UINT nDataFlag)
{
	int csize = m_Childs.size();
	for (int i = 0; i < csize; i++)
	{
		UControl* pChildCtrl = m_Childs[i];
		if (pChildCtrl->m_Visible)
		{
			pChildCtrl->OnDeskTopDragEnd( pDragFrom, pDragData, nDataFlag );
		}
	}
}
UINT UControl::GetDataFlag()
{
	return 0;
}

BOOL UControl::OnDropThis(UControl* pDragTo, UINT nDataFlag)
{
	return TRUE;
}

void UControl::OnReciveDragDrop(UControl* pDragFrom, const UPoint& point,const void* pDragData, UINT nDataFlag )
{

}

BOOL UControl::OnMouseUpDragDrop(UControl* pDragFrom,const UPoint& point,const void* DragData, UINT nDataFlag)
{
	return FALSE;
}
BOOL UControl::OnMouseDownDragDrop(UControl* pDragFrom, const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	return OnMouseUpDragDrop(pDragFrom, point, pDragData, nDataFlag);
}

void UControl::OnDragDropAccept(UControl* pAcceptCtrl,const UPoint& point,const void* DragData, UINT nDataFlag)
{
	EndDragDrop();
	ReleaseCapture();
}

void UControl::OnDragDropReject(UControl* pRejectCtrl, const void* DragData, UINT nDataFlag)
{
	EndDragDrop();
	ReleaseCapture();
}

UControl* UControl::FindFirstFocus()
{
	UControl* pFocusCtrl = NULL;
	if (!m_Childs.empty() && m_Visible)
	{
		for (int c = 0; c < m_Childs.size(); c++)
		{
			pFocusCtrl = m_Childs[c]->FindFirstFocus();
			if (pFocusCtrl)
			{
				return pFocusCtrl;
			}
		}
	}
	if (m_Style != NULL && m_Style->m_bTabStop && m_bCreated && m_Visible)
	{
		return this;
	}
	return NULL;
}

UControl* UControl::FindLastFocus()
{
	UControl* pLastFocus = NULL;
	//if (m_Style->m_bTabStop)
	//	pLastFocus = this;

	// 沿子窗口顺序一直遍历下去,找到最后一个有效的
	// 如果当前窗口被隐藏 子窗口在当前窗口中的属性仍为显示状态 所以会找到一些看不见的控件
	// 所以这里我们设父窗口一旦被隐藏那么就不按子窗口顺序遍历 直接跳出
	if (!m_Childs.empty() && m_Visible)
	{
		for (int c = 0; c < m_Childs.size(); c++)
		{
			UControl* pCtrl = m_Childs[c]->FindLastFocus();	
			if (pCtrl)
			{
				pLastFocus = pCtrl;
				return pLastFocus;
			}
		}
	}
	if (m_Style != NULL && m_Style->m_bTabStop && m_bCreated && m_Visible)
	{
		pLastFocus = this;
		return pLastFocus;
	}
	return NULL;
}

struct UFindControlFocus : public UControl
{
public:
	static UControl* FindNextFocus(UControl* pFindTarget, UControl* pCurFocus, UControl** ppMark)
	{
		// 如果当前焦点为目标窗口, 那么设置标记, 意味着下一个有效窗口就是我们需要的结果
		if (pCurFocus == pFindTarget)
		{
		  *ppMark = pFindTarget;
		}
		else if (*ppMark)	
		{
			if (pFindTarget->GetStyle()->m_bTabStop && 
				pFindTarget->IsCreated() && 
				pFindTarget->IsVisible() && 
				pFindTarget->IsActive())
			{
				return pFindTarget;
			}
		}
		// 遍历所有子控件
		UControl* pFocus = NULL;
		for (int c = 0; c < pFindTarget->GetNumChilds(); c++)
		{
			if (pFindTarget->IsVisible())
			{
				pFocus = FindNextFocus(pFindTarget->GetChildCtrl(c), pCurFocus, ppMark);
				if (pFocus) 
				{
					break;
				}
			}
		}
		return pFocus;
	}

};

UControl* UControl::FindNextFocus(UControl* pCurFocus)
{
	UControl* pMark = NULL;
	return UFindControlFocus::FindNextFocus(this, pCurFocus, &pMark);
}

UControl* UControl::FindPrevFocus(UControl *curResponder)
{
	//if (firstCall)
	//	smPrevResponder = NULL;

	////if this is the current repOnder, return the previous One
	//if (curResponder == this)
	//	return smPrevResponder;

	////else if this is a respOnder, store it in case the next found is the current respOnder
	//else if ( m_Style->m_bTabStop && m_bCreated && m_Visible && m_Active )
	//	smPrevResponder = this;

	////loop through, checking each child to see if it is the One that follows the firstResponder
	//UControl *tabCtrl = NULL;
	//for (int c = 0; c < m_Childs.size(); c++)
	//{
	//	tabCtrl = m_Childs[c]->FindPrevFocus(curResponder, FALSE);
	//	if (tabCtrl) break;

	//}

	//m_pFocusCtrl = tabCtrl;
	//return tabCtrl;
	return NULL;
}

void UControl::OnGetFocus()
{
}

void UControl::OnLostFocus()
{
	// Since many cOntrols have visual cues when they are the firstResponder...
	//	SetUpdate();
}

BOOL UControl::IsChildWindow(UControl *child)
{
	//functiOn returns TRUE if this cOntrol, or One of it's children is the child cOntrol
	if (child == this)
		return TRUE;

	//loop through, checking each child to see if it is ,or cOntains, the firstResponder
	/*  iterator i;
	for (i = begin(); i != end(); i++)
	{
	UControl *ctrl = static_cast<UControl *>(*i);
	if (ctrl->IsChildWindow(child)) return TRUE;
	}*/

	//not found, therefore FALSE
	return FALSE;
}

UControl* UControl::GetFocus()
{
	if (g_Desktop)
	{
		return g_Desktop->GetFocusCtrl();
	}
	return NULL;
}


BOOL UControl::IsFocused()
{
	return GetFocus() == this;
}

void UControl::SetFocusControl( UControl* pFocusCtrl )
{
	if (g_Desktop)
	{
		g_Desktop->SetFocusControl(pFocusCtrl);
	}
}

void UControl::SetFocusControl()
{
	if ( m_bCreated && m_Visible )
	{
		SetFocusControl(this);
	}
}

void UControl::ClearFocus()
{
	UControl* pFocus = GetFocus();
	if (pFocus == this)
	{
		SetFocusControl(NULL);
	}
}


BOOL UControl::BeginDragDrop(UControl* pCtrl, USkin* pDragIconSkin, int nIconIndex, void* pUserData, DWORD DataFlag, char* text,IUFont* font, bool bDrawBgk, bool drawtext, UPoint drawsize)
{
	if (g_Desktop)
	{
	 return	g_Desktop->BeginDragDrop(pCtrl, pDragIconSkin, nIconIndex, pUserData, DataFlag,text,font,bDrawBgk,drawtext,drawsize);
	}
	return FALSE;
}

void UControl::EndDragDrop()
{
	if (g_Desktop)
	{
		g_Desktop->EndDragDrop();
	}
}

void UControl::buildAcceleratorMap()
{
	//add my own accel key
	AddAcceleratorKey();

	//add all my childrens keys
	UControlList::iterator i;
	for(i = m_Childs.begin(); i != m_Childs.end(); i++)
	{
		UControl *ctrl = static_cast<UControl *>(*i);
		ctrl->buildAcceleratorMap();
	}
}

void UControl::AddAcceleratorKey()
{
	//see if we have an accelerator
	if (mAcceleratorKey.Empty())
		return;

	
	//EventDescriptor accelEvent;
	//ActiOnMap::createEventDescriptor(mAcceleratorKey, &accelEvent);

	////now we have a modifier, and a key, add them to the canvas
	std::string buf = mAcceleratorKey.GetBuffer();
	int ScanPos = 0;
	while(TRUE)
	{
		if(!buf.find(';',0) || ScanPos >= buf.length())
		{
			break;
		}
		int CodeLength = buf.find(';',ScanPos);
		std::string opCode = buf.substr(ScanPos,CodeLength - ScanPos);
		UAccelarKey * ackey = sm_System->FindAcceleratorKey(opCode.c_str());
		if (!ackey)
		{
			ULog("Can't Load AcceleratorKey, OpCode = %s",opCode.c_str());
		}
		else
		{
			UINT keycode = StringKey2LK(ackey->mAccelarKeyCode.GetBuffer());
			UDesktop *root = (UDesktop*)GetRoot();
			std::string Modify;
			if (ackey->mAccelarKeyModify.GetBuffer())
			{
				Modify = ackey->mAccelarKeyModify.GetBuffer();
			}
			UINT KeyModify = 0;
			if (Modify.size())
			{
				if (Modify.find("LCONTROL") != -1)
				{
					KeyModify |= LKM_CTRL;
				}
				if (Modify.find("LALT") != -1)
				{
					KeyModify |= LKM_ALT;
				}
				if (Modify.find("LSHIFT") != -1)
				{
					KeyModify |= LKM_SHIFT;
				}
			}
			if (root)
				root->AddAcceleratorKey(this,ackey->mActionName.GetBuffer() , keycode, KeyModify);
		}
		ScanPos += CodeLength - ScanPos + 1;
	}
}

void UControl::acceleratorKeyPress( const char * opCode )
{
	opCode;
	OnAction();
}

void UControl::acceleratorKeyRelease(const char * opCode)
{
	//index;
	//do nothing
}

BOOL UControl::OnChar(UINT nCharCode, UINT nRepCnt, UINT nFlags)
{
	// TODO : 发出出错声音. beep.
	return TRUE;
}

BOOL UControl::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	//pass the event to the parent
	UControl *parent = GetParent();
	if (parent)
		return parent->OnKeyDown(nKeyCode, nRepCnt, nFlags);
	else
		return FALSE;
}

BOOL UControl::OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	//pass the event to the parent
	//BOOL bpass = FALSE;
	//if(nKeyCode == LK_ESCAPE)
	//{
	//	bpass = OnEscape();
	//}
	//if(!bpass)
	{
		UControl *parent = GetParent();
		if (parent)
			return parent->OnKeyUp(nKeyCode, nRepCnt, nFlags);
		else
			return FALSE;
	}
}

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //

void UControl::OnAction()
{
	if (! m_Active)
		return;

	//if (m_CommandScript != NULL)
	//{
	// // 调用脚本
	//	 /* char buf[16];
	//   LSprintf(buf, sizeof(buf), "%d", GetId());
	//   Con::SetVariable("$ThisCOntrol", buf);
	//   Con::evaluate(m_CommandScript, FALSE);*/
	//}
	//else
	{
		// 分发消息
		//DispatchNotify(ONCOMMAND);
	}
}


void UControl::OnDialogPush()
{
	// Notify Script.
	//   if( isMethod("OnDialogPush") )
	//     Con::executef(this, 1, "OnDialogPush");

}

void UControl::OnDialogPop()
{
	// Notify Script.
	//  if( isMethod("OnDialogPop") )
	//     Con::executef(this, 1, "OnDialogPop");
}

//------------------------------------------------------------------------------
void UControl::SetVisible(BOOL value)
{
	m_Visible = value;
	/* iterator i;
	for(i = begin(); i != end(); i++)
	{
	UControl *ctrl = static_cast<UControl *>(*i);
	ctrl->clearFirstResponder();
	}*/

	//	UControl *parent = GetParent();
	//	if (parent)
	//	   parent->OnChildResized(this);
}

void UControl::SetActive( BOOL value )
{
	m_Active = value;
	if ( !m_Active )
		ClearFocus();
}

void UControl::GetScrollLineSizes(UINT *rowHeight, UINT *columnWidth)
{
	// default to 10 pixels in y, 30 pixels in x
	*columnWidth = 30;
	*rowHeight = 10;
}

void UControl::OnSetCursor(UCursor *&cursor, BOOL &showCursor, const GuiEvent &lastGuiEvent)
{
	lastGuiEvent;

	cursor = NULL;
	showCursor = TRUE;
}

BOOL UControl::OnEscape()
{
	if (!m_CanEscapeFocuse)
	{
		return FALSE;
	}
	if(m_Childs.empty())
	{
		return FALSE;
	}
	for (size_t i = m_Childs.size() ; i > 0 ; --i)
	{
		UControl * pControl = m_Childs[i - 1];
		if (pControl)
		{
			if (pControl->IsVisible() && 
				pControl->IsEscapeFouce())
			{
				if(pControl->OnEscape())
					return TRUE;
			}
		}
	}
	return FALSE;
}
BOOL UControl::IsEscapeFouce()
{
	return m_CanEscapeFocuse;
}
void UControl::SetEscapeFouce(BOOL exitfouce)
{
	m_CanEscapeFocuse = exitfouce;
}