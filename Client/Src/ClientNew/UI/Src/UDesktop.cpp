#include "StdAfx.h"
#include "UBase.h"
#include "UControl.h"
#include "UDesktop.h"
#include "USystem.h"

UINT gLastEventTime = 0;
UDesktop* g_Desktop = NULL;
//UDefaultDragDropSink UDesktop::UDragDrop::smDefaultSink;
//
//void UDefaultDragDropSink::OnRender(const UDragDropItem& DragItem)
//{
//}
//
//void UDefaultDragDropSink::OnBeginDrag(const UDragDropItem& DragItem)
//{
//}
//
//void UDefaultDragDropSink::OnEndDrag(const UDragDropItem& DragItem)
//{
//
//}

UDesktop::UDesktop()
{
	m_Position.Set(0,0);
	m_Size.Set(800, 600);
	m_WindowSize.Set(800, 600);
	m_bCreated = TRUE;
	mPixelsPerMickey = 1.0f;
	lastCursorON = FALSE;
	cursorON    = TRUE;
	mShowCursor = TRUE;
	rLastFrameTime = 0.0f;

	m_CapturedWnd = NULL;
	m_pMouseCtrl = NULL;
	m_bMouseCtrlClicked = FALSE;
	m_LeftBtnDown = FALSE;
	m_RightBtnDown = FALSE;
	m_CanEscapeFocuse = TRUE;

	lastCursor = NULL;
	lastCursorPt.Set(0,0);
	m_ptCursorPos.Set(0,0);

	mLastMouseClickCount = 0;
	mLastMouseDownTime = 0;
	m_PrevMouseTime = 0;
	defaultCursor = NULL;
	mRightMouseLast = FALSE;

	mRenderFrOnt = FALSE;
	m_uModifierFlags = 0;
	m_pIMEControl = NULL;

	m_pFocusCtrl = NULL;
	g_Desktop = this;

	m_DragDropItem.nIconIndex = -1;
	m_DragDropItem.pDragFrom = NULL;
	m_DragDropItem.DataFlag = -1;
	m_DragDropItem.UserData = NULL;
	m_DragDropItem.spSkin = NULL;
	m_DragDropItem.DragSkinSize.Set(36,36);

	keyboardCOntrol = NULL;
}

UDesktop::~UDesktop()
{
	g_Desktop = NULL;
}

//------------------------------------------------------------------------------
void UDesktop::SetCursor(UCursor *curs)
{
	defaultCursor = curs;
}

void UDesktop::ShowCursor(BOOL OnOff)
{
	cursorON = OnOff;
	if(!cursorON)
		m_pMouseCtrl = NULL;
}
void UDesktop::ReRigestAcceleratorKey()
{
	mAcceleratorMap.clear();
	for(UControlList::iterator i = m_Childs.end(); i != m_Childs.begin() ; )
	{
		i--;
		UControl *ctrl = static_cast<UControl *>(*i);
		ctrl->buildAcceleratorMap();

		if (ctrl->m_Style->mModal)
			break;
	}
}
void UDesktop::AddAcceleratorKey(UControl *ctrl, const char * opCode, UINT keyCode, UINT modifier)
{
	if (keyCode > 0 && ctrl)
	{
		AccKeyMap newMap;
		newMap.ctrl = ctrl;
		newMap.opCode = opCode;
		newMap.keyCode = keyCode;
		newMap.modifier = modifier;
		mAcceleratorMap.push_back(newMap);
	}
}

void UDesktop::TabNext(void)
{
	UControl *ctrl = Last();
	if (ctrl)
	{
		//save the old
		UControl *oldResponder = m_pFocusCtrl;

		UControl* newResponder = ctrl->FindNextFocus(m_pFocusCtrl);
		if ( !newResponder )
			newResponder = ctrl->FindFirstFocus();

		if ( newResponder && newResponder->IsVisible() && newResponder->m_Style->m_bTabStop && newResponder != oldResponder )
		{
			if ( oldResponder )
				oldResponder->OnLostFocus();
			newResponder->SetFocusControl();

			
		}
	}
}

void UDesktop::TabPrev(void)
{
	UControl *ctrl = Last();
	if (ctrl)
	{
		//save the old
		UControl *oldResponder = m_pFocusCtrl;

		UControl* newResponder = ctrl->FindPrevFocus(m_pFocusCtrl);
		if ( !newResponder )
			newResponder = ctrl->FindLastFocus();

		if ( newResponder && newResponder->IsVisible() && newResponder->m_Style->m_bTabStop && newResponder != oldResponder )
		{
			newResponder->SetFocusControl();

			if ( oldResponder )
				oldResponder->OnLostFocus();
		}
	}
}

 void UDesktop::SetImeControl(UControl* pImeCtrl)
 {
	 m_pIMEControl = pImeCtrl;
 }

 BOOL UDesktop::HitTestSoftKeyControl(const UPoint& pt)
 {
	 if (keyboardCOntrol && keyboardCOntrol->IsVisible())
	 {
		 return keyboardCOntrol->HitTest(pt);
	 }
	 return FALSE ;
 }
 BOOL UDesktop::HitTestImeControl(const UPoint& pt)
 {
	if (m_pIMEControl && m_pIMEControl->IsVisible())
	{
		return m_pIMEControl->HitTest(pt);
	}
	return FALSE;
 }
 bool g_DownDraging = false;

void UDesktop::LeftBtnDown()
{
	if (IsDraging())
	{
		FindMouseControl(m_ptCursorPos);
		if(m_pMouseCtrl)
		{
			if (!m_pMouseCtrl->m_Style->m_bHandleDragDrop)
			{
				m_DragDropItem.pDragFrom->OnDragDropReject(m_pMouseCtrl, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
			}else
			{
				//if (m_DragDropItem.pDragFrom->OnDropThis(m_pMouseCtrl, m_pMouseCtrl->GetDataFlag()))
				{
					BOOL bAccept = m_pMouseCtrl->OnMouseDownDragDrop(m_DragDropItem.pDragFrom, m_ptCursorPos, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
					if (bAccept)
					{
						m_DragDropItem.pDragFrom->OnDragDropAccept(m_pMouseCtrl,m_ptCursorPos, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
						m_pMouseCtrl->OnReciveDragDrop(m_DragDropItem.pDragFrom, m_ptCursorPos, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
						g_DownDraging = true;
					}else
					{
						m_DragDropItem.pDragFrom->OnDragDropReject(m_pMouseCtrl, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
					}
				}
			}
		}
		return;
	}
	UINT curTime = timeGetTime();
	mNextMouseTime = curTime + mInitialMouseDelay;

	//if the last buttOn pressed was the left...
	if (mLeftMouseLast)
	{
		//if it was within the double click time count the clicks
		if (curTime - mLastMouseDownTime <= 500)
			mLastMouseClickCount++;
		else
			mLastMouseClickCount = 1;
	}
	else
	{
		mLeftMouseLast = TRUE;
		mLastMouseClickCount = 1;
	}
	mLastMouseDownTime = curTime;

	m_PrevMouseTime = curTime;
	m_LeftBtnDown = TRUE;

	const UPoint& ptCursor = m_ptCursorPos;
	UINT uFlags = m_uModifierFlags;
	//传入到当前捕捉的控件中
	if (m_CapturedWnd)
	{
		m_CapturedWnd->OnMouseDown(ptCursor,mLastMouseClickCount,  uFlags);
	}else if(HitTestImeControl(ptCursor))
	{
		m_pIMEControl->OnMouseDown(ptCursor,mLastMouseClickCount,  uFlags);
	}else if(HitTestSoftKeyControl(ptCursor))
	{
		UPoint ptPos = ptCursor - keyboardCOntrol->GetWindowPos();
		UControl* pHitCtrl = keyboardCOntrol->FindHitWindow(ptPos);
		if (pHitCtrl->IsActive())
		{
			pHitCtrl->OnMouseDown(ptPos - pHitCtrl->GetWindowPos(),mLastMouseClickCount,  uFlags);
		}
		
	}else
	{
		// 遍历所有顶级窗口

		for (int i = (m_Childs.size() -1); i >= 0; i--)
		{
			UControl* pTopCtrl = m_Childs[i];
			if (!pTopCtrl->IsVisible())
			{
				continue;
			}
#pragma message("Need optimize: 我们应该提供一个首先遍历模态控件,然后再遍历其它控件. 这样的话, 那么FindHitWindow 就不需要总是返回非NULL值了.")
			// pHitCtrl 始终有效.
			UControl* pHitCtrl = pTopCtrl->FindHitWindow(ptCursor);
			// 如果为非模态控件,则忽略
			//if ((!pHitCtrl->m_Active) && (pHitCtrl->m_Style->mModal))
			if (!pHitCtrl->m_Active)
			{
				continue;
			}else
			{
				// 模态或者焦点控件
				pHitCtrl->OnMouseDown(ptCursor,mLastMouseClickCount,  uFlags);
				break;
			}
		}
	}

	if (m_pMouseCtrl)
		m_bMouseCtrlClicked = TRUE;
}

void UDesktop::FindMouseControl(const UPoint& pos)
{
	if(m_Childs.size() == 0)
	{
		m_pMouseCtrl = NULL;
		return;
	}
	UControl* pHitCtrl = NULL ;
	if (HitTestSoftKeyControl(pos))
	{
		UPoint ptPos = pos - keyboardCOntrol->GetWindowPos();
		pHitCtrl = keyboardCOntrol->FindHitWindow(ptPos);
	}else
	{
		pHitCtrl = FindHitWindow(pos);
	}
	if(pHitCtrl != m_pMouseCtrl)
	{
		if(m_pMouseCtrl)
			m_pMouseCtrl->OnMouseLeave(pos, m_uModifierFlags);
		m_pMouseCtrl = pHitCtrl;
        if(m_pMouseCtrl)
		    m_pMouseCtrl->OnMouseEnter(pos, m_uModifierFlags);
	}
}

void UDesktop::ReFindMouseCtrl()
{
	FindMouseControl(m_ptCursorPos);
}

void UDesktop::LeftMouseUp()
{
	m_PrevMouseTime = timeGetTime();
	m_LeftBtnDown = FALSE;

	if (IsDraging())
	{
		FindMouseControl(m_ptCursorPos);
		if(m_pMouseCtrl)
		{
			if (!m_pMouseCtrl->m_Style->m_bHandleDragDrop)
			{
				m_DragDropItem.pDragFrom->OnDragDropReject(m_pMouseCtrl, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
			}else
			{
				if (m_DragDropItem.pDragFrom->OnDropThis(m_pMouseCtrl, m_pMouseCtrl->GetDataFlag()) && !g_DownDraging)
				{
					BOOL bAccept = m_pMouseCtrl->OnMouseUpDragDrop(m_DragDropItem.pDragFrom, m_ptCursorPos, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
					if (bAccept)
					{
						m_DragDropItem.pDragFrom->OnDragDropAccept(m_pMouseCtrl,m_ptCursorPos, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
						m_pMouseCtrl->OnReciveDragDrop(m_DragDropItem.pDragFrom, m_ptCursorPos, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
					}else
					{
						m_DragDropItem.pDragFrom->OnDragDropReject(m_pMouseCtrl, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
					}
				}
			}
		}
	}else
	{
		//pass the event to the mouse locked cOntrol
		if (m_CapturedWnd)
		{
			m_CapturedWnd->OnMouseUp(m_ptCursorPos, m_uModifierFlags);
		}else if (HitTestImeControl(m_ptCursorPos))
		{
			m_pIMEControl->OnMouseUp(m_ptCursorPos, m_uModifierFlags);
		}
		else
		{
			FindMouseControl(m_ptCursorPos);
			if(m_pMouseCtrl)
				m_pMouseCtrl->OnMouseUp(m_ptCursorPos, m_uModifierFlags);
		}
	}
	
}

void UDesktop::CheckLockMouseMove(const UPoint& position)
{
	UControl* pHitCtrl = FindHitWindow(position);
	if(pHitCtrl != m_pMouseCtrl)
	{
		if(m_pMouseCtrl == m_CapturedWnd)
		{
            if(m_CapturedWnd)
			    m_CapturedWnd->OnMouseLeave(position, m_uModifierFlags);

            if(pHitCtrl)
			    pHitCtrl->OnMouseEnter(position, m_uModifierFlags);
		}
		else if(pHitCtrl == m_CapturedWnd)
		{
            if(m_CapturedWnd)
			    m_CapturedWnd->OnMouseEnter(position, m_uModifierFlags);

            if(m_pMouseCtrl)
			    m_pMouseCtrl->OnMouseLeave(position, m_uModifierFlags);
		}
		else
		{
            if(m_pMouseCtrl)
			    m_pMouseCtrl->OnMouseLeave(position, m_uModifierFlags);

            if(pHitCtrl)
			    pHitCtrl->OnMouseEnter(position, m_uModifierFlags);
		}
		m_pMouseCtrl = pHitCtrl;
	}
}

void UDesktop::LeftMouseDragged()
{
	if (IsDraging())
	{
		// DRAGDROP 状态下, 我们始终锁定源控件, 但是消息不能总是发给它.
		CheckLockMouseMove(m_ptCursorPos);
		if(m_pMouseCtrl && m_pMouseCtrl->m_Style->m_bHandleDragDrop)
		{
			m_pMouseCtrl->OnMouseDragged(m_ptCursorPos, m_uModifierFlags);
		}
	}else
	{
		if (m_CapturedWnd)
		{
			CheckLockMouseMove(m_ptCursorPos);
			m_CapturedWnd->OnMouseDragged(m_ptCursorPos, m_uModifierFlags);
		}else if (HitTestImeControl(m_ptCursorPos))
		{
			m_pIMEControl->OnMouseDragged(m_ptCursorPos, m_uModifierFlags);
		}else if (HitTestSoftKeyControl(m_ptCursorPos))
		{
			keyboardCOntrol->OnMouseDragged(m_ptCursorPos, m_uModifierFlags);
		}
		else
		{
			FindMouseControl(m_ptCursorPos);
			if(m_pMouseCtrl)
				m_pMouseCtrl->OnMouseDragged(m_ptCursorPos, m_uModifierFlags);
		}
	}
}

void UDesktop::MouseMove(int x, int y)
{
	BOOL bMoved = FALSE;
	UPoint oldpt(m_ptCursorPos.x, m_ptCursorPos.y);
	UPoint pt(x, y);
	pt.x = pt.x * m_Size.x / m_WindowSize.x;
	pt.y = pt.y * m_Size.y / m_WindowSize.y;

	m_ptCursorPos.x = max(0, min(pt.x, m_Size.x - 1));
	if (oldpt.x != m_ptCursorPos.x)
	{
		bMoved = TRUE;
	}

	m_ptCursorPos.y = max(0, min(pt.y, m_Size.y - 1));
	if (oldpt.y != m_ptCursorPos.y)
	{
		bMoved = TRUE;
	}

	if (bMoved)
	{
		mLeftMouseLast = FALSE;
		mRightMouseLast = FALSE;
		if (m_LeftBtnDown)
		{
			LeftMouseDragged();
		}
		else if (m_RightBtnDown)
		{
			RightMouseDragged();
		}else if (IsDraging())
		{
			LeftMouseDragged();
		}
		else
		{
			if (!IsDraging())	// DRAGDROP 状态下, 不可能发生MOUSEMOVE的事件.
			{
				const UPoint& MousePos = m_ptCursorPos;
				if (m_CapturedWnd)
				{
					CheckLockMouseMove(MousePos);
					m_CapturedWnd->OnMouseMove(MousePos, m_uModifierFlags);
				}
				else
				{
					FindMouseControl(MousePos);
					if(m_pMouseCtrl)
					{
						m_pMouseCtrl->OnMouseMove(MousePos, m_uModifierFlags);
					}
				}
			}
			
		}
	}
}

void UDesktop::RightBtnDown()
{	
	if (IsDraging())
	{
		if (m_DragDropItem.pDragFrom)
			m_DragDropItem.pDragFrom->OnDragDropReject(m_pMouseCtrl, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
		return;
	}
	UINT curTime = timeGetTime();


	if (mRightMouseLast)
	{
		//if it was within the double click time count the clicks
		if (curTime - mLastMouseDownTime <= 500)
			mLastMouseClickCount++;
		else
			mLastMouseClickCount = 1;
	}
	else
	{
		mRightMouseLast = TRUE;
		mLastMouseClickCount = 1;
	}

	mLastMouseDownTime = curTime;
	mLastEvent.mouseClickCount = mLastMouseClickCount;

	m_PrevMouseTime = curTime;
	m_RightBtnDown = TRUE;

	if (m_CapturedWnd)
		m_CapturedWnd->OnRightMouseDown(m_ptCursorPos,mLastMouseClickCount, m_uModifierFlags);
	else
	{
		FindMouseControl(m_ptCursorPos);
		if(m_pMouseCtrl)
		{
			m_pMouseCtrl->OnRightMouseDown(m_ptCursorPos,mLastMouseClickCount, m_uModifierFlags);
		}
	}
}

void UDesktop::RightMouseUp()
{
	m_PrevMouseTime = timeGetTime();
	m_RightBtnDown = FALSE;

	if (m_CapturedWnd)
		m_CapturedWnd->OnRightMouseUp(m_ptCursorPos,m_uModifierFlags);
	else
	{
		FindMouseControl(m_ptCursorPos);

		if(m_pMouseCtrl)
			m_pMouseCtrl->OnRightMouseUp(m_ptCursorPos,m_uModifierFlags);
	}
}

void UDesktop::RightMouseDragged()
{
	m_PrevMouseTime = timeGetTime();

	if (m_CapturedWnd)
	{
		CheckLockMouseMove(m_ptCursorPos);
		m_CapturedWnd->OnRightMouseDragged(m_ptCursorPos,m_uModifierFlags);
	}
	else
	{
		FindMouseControl(m_ptCursorPos);
		if(m_pMouseCtrl)
			m_pMouseCtrl->OnRightMouseDragged(m_ptCursorPos,m_uModifierFlags);
	}
}

void UDesktop::MouseWheel(int delta)
{
	if (m_CapturedWnd)
		m_CapturedWnd->OnMouseWheel(m_ptCursorPos, delta ,m_uModifierFlags);
	else
	{
		FindMouseControl(m_ptCursorPos);

		if (m_pMouseCtrl)
			m_pMouseCtrl->OnMouseWheel(m_ptCursorPos,delta ,m_uModifierFlags);
	}
}

BOOL UDesktop::Char(UINT nCharCode, UINT nRepCnt, UINT nFlags)
{
	if (m_pFocusCtrl)
	{
		if(m_pFocusCtrl->OnChar(nCharCode, nRepCnt, nFlags))
			return TRUE;
	}
	return FALSE;
}

BOOL UDesktop::FindLastRespond(const char* opCode)
{
	std::vector<LastRespond>::iterator it = mLastRespond.begin();
	while (it != mLastRespond.end())
	{
		if (strcmp(opCode,it->opCodes.c_str()) == 0)
		{
			return TRUE;
		}
		++it;
	}
	return FALSE;
}
BOOL UDesktop::KeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	if (m_pFocusCtrl)
	{
		if(m_pFocusCtrl->OnKeyDown(nKeyCode, nRepCnt, nFlags))
			return TRUE;
	}

	if (nKeyCode == LK_RETURN || nKeyCode == LK_NUMPADENTER)
	{
		nKeyCode = LK_RETURN ;
	}

	for (UINT i = 0; i < mAcceleratorMap.size(); i++)
	{
		UINT UintKey = (UINT)mAcceleratorMap[i].keyCode;
		if (UintKey== LK_NUMPADENTER )
		{
			UintKey = LK_RETURN ;
			
		}
		if (UintKey == nKeyCode && (UINT)mAcceleratorMap[i].modifier == nFlags)
		{
			if (nRepCnt == 0)
			{
				mAcceleratorMap[i].ctrl->acceleratorKeyPress(mAcceleratorMap[i].opCode.c_str());
				if (!FindLastRespond(mAcceleratorMap[i].opCode.c_str()))
				{
					LastRespond temp;
					temp.ctrl = mAcceleratorMap[i].ctrl;
					temp.KeyCode = UintKey;
					temp.modifier = mAcceleratorMap[i].modifier;
					temp.opCodes = mAcceleratorMap[i].opCode;
					mLastRespond.push_back(temp);
				}
			}
			return TRUE;
		}
	}
	// 处理TAB
	if ( IsCursorOn() && ( nKeyCode == LK_TAB ) )
	{
		if (!m_Childs.empty())
		{
			if (nFlags & LKM_SHIFT)
			{
				TabPrev();
				return TRUE;

			}
			if (nFlags == 0)
			{
				TabNext();
				return TRUE;
			}
		}
	}
	return FALSE;
}

BOOL UDesktop::KeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	if(m_pFocusCtrl && m_pFocusCtrl->OnKeyUp(nKeyCode, nRepCnt, nFlags))
		return TRUE;
	if (nKeyCode == LK_ESCAPE)
	{
		OnEscape();
		return TRUE;
	}

    //LK_SYSRQ 不发送KeyDown消息
    if(nKeyCode == LK_SYSRQ)
    {
        //KeyDown(nKeyCode, 0, nFlags);
    }

	if (nKeyCode == LK_NUMPADENTER)
	{
		nKeyCode = LK_RETURN ;
	}

	std::vector<LastRespond>::iterator it = mLastRespond.begin();
	while (it != mLastRespond.end())
	{
		if (it->KeyCode == nKeyCode)
		{
			it->ctrl->acceleratorKeyRelease(it->opCodes.c_str());
			it = mLastRespond.erase(it);
			return TRUE;
		}
		++it;
	}
	//UINT i;
	//for (i = 0; i < mAcceleratorMap.size(); i++)
	//{
	//	if (((UINT)mAcceleratorMap[i].keyCode == nKeyCode && (UINT)mAcceleratorMap[i].modifier == nFlags))
	//	{
	//		//mAcceleratorMap[i].ctrl->acceleratorKeyRelease(mAcceleratorMap[i].opCode.c_str());

	//		std::vector<LastRespond>::iterator it = mLastRespond.begin();
	//		while (it != mLastRespond.end())
	//		{
	//			if (strcmp(mAcceleratorMap[i].opCode.c_str(),it->Opcodes.c_str()) == 0)
	//			{
	//				it->ctrl->acceleratorKeyRelease(it->Opcodes.c_str());
	//				it = mLastRespond.erase(it);
	//				continue;
	//			}
	//			it++;
	//		}
	//		return TRUE;
	//	}
	//}
	return FALSE;
}
BOOL UDesktop::InitSoftKeyBoardControl()
{
	if (keyboardCOntrol == NULL)
	{
		keyboardCOntrol = sm_System->CreateDialogFromFile("core\\SoftKeyBoard\\SoftKeyBoard.udg");
		if (!keyboardCOntrol)
		{
			return FALSE;
		}

		if (!keyboardCOntrol->Create())
		{
			return FALSE ;
		}
	}

	return TRUE;
}
void UDesktop::SetFrameWindow(UControl *pFrameCtrl)
{
	if(!pFrameCtrl)
		return;

	
	UControl* pOldFrame = NULL;
	 if (!m_Childs.empty())
	 {
		pOldFrame = m_Childs[0];
	 }
	 

	//remove all dialogs On layer 0
	 if (!m_Childs.empty())
	 {
		 std::vector<UControl*>::iterator it = m_Childs.begin();
		 for (; it != m_Childs.end(); )
		 {
			 UControl* pControl = *it;
			 if ( pControl->m_Layer == 0)
			 {
				 if (pControl->m_bCreated)
				 {
					 pControl->Destroy();
				 }
				 pControl->m_Parent = NULL;
				 it = m_Childs.erase(it);
			 }else
			 {
				 ++it;
			 }
		 }
	 }

	//  the first respOnder from the old GUI
	UControl *oldResponder = m_pFocusCtrl;
	m_pFocusCtrl = pFrameCtrl->FindFirstFocus();
	if(oldResponder && oldResponder != m_pFocusCtrl)
		oldResponder->OnLostFocus();

	//add the gui to the frOnt
	if(!m_Childs.size() || pFrameCtrl != m_Childs[0])
	{
		// automatically wakes objects in UControl::OnCreate
		AddChild(pFrameCtrl);
		if (m_Childs.size() >= 2)
			MoveChildTo(pFrameCtrl, *m_Childs.begin());
	}


	//rebuild the accelerator map
	//mAcceleratorMap.clear();

	//for(UControlList::iterator i = m_Childs.end(); i != m_Childs.begin() ; )
	//{
	//	i--;
	//	UControl *ctrl = static_cast<UControl *>(*i);
	//	ctrl->buildAcceleratorMap();

	//	if (ctrl->m_Style->mModal)
	//		break;
	//}
	ReRigestAcceleratorKey();
	ReFindMouseCtrl();
	ReCalcLayerOut();
}

void UDesktop::ReCalcLayerOut()
{
	UPoint size = m_Size;	// 这里存放的就是我们桌面大小. 

	if(size.x == -1 || size.y == -1)
		return;

	URect DesktopRect(0, 0, size.x, size.y);
	SetWindowRect(DesktopRect);

	// 所有顶级窗口必须和桌面大小一致.
	for (int i  =0; i < m_Childs.size(); i++)
	{
		UControl* pTopCtrl = m_Childs[i];
		assert(pTopCtrl->m_bCreated);

		if (pTopCtrl->IsIMEControl())
		{
			continue;
		}
		URect CtrlRect = pTopCtrl->GetControlRect();
		if (CtrlRect != DesktopRect)
		{
			pTopCtrl->ResizeControl(UPoint(0,0),size);
		}

	}
}

UControl* UDesktop::GetCurFrame()
{
	if(m_Childs.size() > 0)
		return m_Childs[0];
	return NULL;
}

void UDesktop::DestroyControl(UControl* pCtrl)
{
	//if (pCtrl == m_pMouseCtrl)
	{
		m_pMouseCtrl = NULL;
	}
	// check everything!!!

	CtrlIterator it = std::find(m_Childs.begin(), m_Childs.end(), pCtrl);
	if (it != m_Childs.end())
	{
		RemoveChild(pCtrl);
	}
	
}


void UDesktop::PushDialogControl(UControl *gui, INT layer)
{
	//add the gui
	gui->m_Layer = layer;

	// UControl::AddChild wakes the object
	BOOL wakedGui = !gui->IsCreated();
	AddChild(gui);

	// sort by layer.
	//std::sort(m_Childs.begin(), m_Childs.end(),std::greater<int>);
	//reorder it to the correct layer
	CtrlIterator it;
	for (it = m_Childs.begin(); it != m_Childs.end(); ++it)
	{
	   UControl *ctrl = *it;
	   if (ctrl->m_Layer > gui->m_Layer)
	   {
	     MoveChildTo(gui, ctrl);
	     break;
	   }
	}

	//call the dialog push method
	gui->OnDialogPush();

	//find the top most dialog
	UControl *topCtrl = m_Childs[m_Childs.size() -1 ];

	////save the old
	UControl *oldResponder = m_pFocusCtrl;

	//find the first respOnder
	m_pFocusCtrl = gui->FindFirstFocus();

	if (oldResponder && oldResponder != m_pFocusCtrl)
	   oldResponder->OnLostFocus();

	//rebuild the accelerator map
	//mAcceleratorMap.clear();
	//if (!m_Childs.empty())
	//{
	//   UControl *ctrl = static_cast<UControl*>(m_Childs[m_Childs.size() - 1]);
	//   ctrl->buildAcceleratorMap();
	//}
	ReRigestAcceleratorKey();
	ReCalcLayerOut();
	ReFindMouseCtrl();
	
}

void UDesktop::PopDialog(UControl *gui)
{
	// 必须大于1
	if (m_Childs.size() < 1)
	  return;

	//first, find the dialog, and call the "OnDialogPop()" method
	UControl *ctrl = NULL;
	if (gui)
	{
		//make sure the gui really exists On the stack
		CtrlConstIterator it = m_Childs.begin();
		BOOL found = FALSE;
		for(; it != m_Childs.end(); ++it)
		{
			UControl *pChild = *it;
			if (pChild == gui)
			{
				ctrl = pChild;
				found = TRUE;
			}
		}
		if (! found)
			return;
	}
	else
	{
		// 弹出最后一个
		ctrl = m_Childs[m_Childs.size() -1];
	}

	//call the "On pop" functiOn
	ctrl->OnDialogPop();

	// Sleep the object
	BOOL didSleep = ctrl->IsCreated();

	//now pop the last child (will Sleep if awake)
	RemoveChild(ctrl);

	//// Save the old respOnder:
	UControl *oldResponder = m_pFocusCtrl;

	//Sim::GetGuiGroup()->AddChild(ctrl);

	if (m_Childs.size() > 0)
	{
	   UControl *ctrl = m_Childs[m_Childs.size() -1];
	   UControl* pFocusCtrl = ctrl->FindFirstFocus();
	   //m_pFocusCtrl = ctrl->m_pFocusCtrl;
	}
	else
	{
	   m_pFocusCtrl = NULL;
	}

	// 危险, 有可能焦点空间已经被删除了. 
	//if (oldResponder && oldResponder != m_pFocusCtrl)
	//   oldResponder->OnLostFocus();



	//rebuild the accelerator map
	//mAcceleratorMap.clear();
	//if (m_Childs.size() > 0)
	//{
	//   UControl *ctrl = static_cast<UControl*>(m_Childs[m_Childs.size() - 1]);
	//   ctrl->buildAcceleratorMap();
	//}
	ReRigestAcceleratorKey();
	ReFindMouseCtrl();
}

void UDesktop::PopDialog(INT layer)
{
	//if (size() < 1)
	//   return;

	//UControl *ctrl = NULL;
	//iterator i = end(); // find in z order (last to first)
	//while (i != begin())
	//{
	//   i--;
	//   ctrl = static_cast<UControl*>(*i);
	//   if (ctrl->m_Layer == layer)
	//      break;
	//}
	//if (ctrl)
	//   PopDialog(ctrl);
}
BOOL UDesktop::BeginDragDrop(UControl* pCtrl, USkin* pDragIconSkin, int nIconIndex,void* pUserData, 
				   DWORD DataFlag, char* text, IUFont* font, bool bDrawBgk , bool drawtext , UPoint drawsize)
{
	if (IsDraging())
	{
		EndDragDrop();
	}
	if (pCtrl == NULL || pDragIconSkin == NULL || nIconIndex < 0 )
	{
		return FALSE;
	}
	
	if (nIconIndex >= pDragIconSkin->GetItemNum())
	{
		return FALSE;
	}

	m_DragDropItem.nIconIndex = nIconIndex;
	m_DragDropItem.spSkin = pDragIconSkin;
	m_DragDropItem.pDragFrom = pCtrl;
	m_DragDropItem.UserData = pUserData;
	m_DragDropItem.DataFlag = DataFlag;
	if (text)
		m_DragDropItem.text = text;
	else
		m_DragDropItem.text.clear();

	m_DragDropItem.bDrawBgk = bDrawBgk;
	m_DragDropItem.bDrawtext = drawtext;
	m_DragDropItem.DragSkinSize = drawsize;
	m_DragDropItem.spFont = font;
	m_uModifierFlags |= LKM_DRAGDROP;
	// 确保Capture 拖拽的源窗口, 以便于其能拦截所有键盘输入.
	CaptureControl(pCtrl);

	pCtrl->OnDragDropBegin(m_DragDropItem.UserData, m_DragDropItem.DataFlag);

	for (int c = 0; c < m_Childs.size(); c++)
	{
		UControl* pRoot = m_Childs[c];
		pRoot->OnDeskTopDragBegin(pCtrl, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
	}
	g_DownDraging = false;
	return TRUE;
}	

void UDesktop::EndDragDrop()
{
	if (m_DragDropItem.pDragFrom != NULL)
	{
		m_uModifierFlags &= ~LKM_DRAGDROP;
		m_DragDropItem.pDragFrom->OnDragDropEnd(NULL, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
		m_DragDropItem.nIconIndex = -1;
		m_DragDropItem.DataFlag = -1;
		m_DragDropItem.spSkin = NULL;
		m_DragDropItem.UserData = NULL;
		m_DragDropItem.text.clear();
		m_DragDropItem.bDrawBgk = true;
		m_DragDropItem.bDrawtext = false;
		for (int c = 0; c < m_Childs.size(); c++)
		{
			UControl* pRoot = m_Childs[c];
			pRoot->OnDeskTopDragEnd(m_DragDropItem.pDragFrom, m_DragDropItem.UserData, m_DragDropItem.DataFlag);
		}
		m_DragDropItem.pDragFrom = NULL;
		g_DownDraging = false;
	}
}

void UDesktop::CaptureControl(UControl *pCapTarget)
{
	if (m_CapturedWnd)
		return;
	m_CapturedWnd = pCapTarget;
	if(m_pMouseCtrl && m_pMouseCtrl != m_CapturedWnd)
	{
		m_pMouseCtrl->OnMouseLeave(m_ptCursorPos, 0/*m_uModifierFlags*/);
	}
}

void UDesktop::ReleaseCapture(UControl *pCapTarget)
{
	if (m_CapturedWnd != pCapTarget)
		return;

	UControl * pHitCtrl = FindHitWindow(m_ptCursorPos);
	if(pHitCtrl != m_CapturedWnd)
	{
		if(m_pMouseCtrl)
			m_pMouseCtrl->OnMouseLeave(m_ptCursorPos, 0);
		m_pMouseCtrl = pHitCtrl;
		m_bMouseCtrlClicked = FALSE;
		if(m_pMouseCtrl)
			m_pMouseCtrl->OnMouseEnter(m_ptCursorPos, 0);
	}
	m_CapturedWnd = NULL;
}

UColor gCanvasClearColor( 255, 0, 255 ); ///< For sm_UiRender->clear

#include "D3DUIRender.h"
void UDesktop::Render(BOOL preRenderOnly)
{
	UPoint size = m_Size;
	if(size.x == 0 || size.y == 0)
		return;

	URect screenRect(0, 0, size.x, size.y);
	//  m_rcWindow = screenRect;

	// 实现到OnSize 中
	//  //all bottom level cOntrols should be the same dimensiOns as the canvas
	//  //this is necessary for passing mouse events accurately
	//  iterator i;
	//  for (i = begin(); i != end(); i++)
	//  {
	//     //AssertFatal(static_cast<UControl*>((*i))->IsCreated(), "UDesktop::Render: ctrl is not awake");
	//     UControl *ctrl = static_cast<UControl*>(*i);
	//     UPoint ext = ctrl->GetWindowSize();
	//     UPoint pos = ctrl->GetWindowPos();

	//     if(pos != screenRect.point || ext != screenRect.extent)
	//     {
	//        ctrl->ResizeControl(screenRect.point, screenRect.extent);
	//        reSetUpdateRegiOns();
	//     }
	//  }
	UControl::sm_UiRender->BeginRender();
	if (!m_Childs.empty())
	{
		sm_UiRender->SetClipRect(screenRect);
	}

	for (int c = 0; c < m_Childs.size(); c++)
	{
		UControl* pRoot = m_Childs[c];
		pRoot->OnRender(pRoot->GetWindowPos(), screenRect);
	}
	sm_UiRender->SetClipRect(screenRect);
	if (m_pIMEControl && m_pIMEControl->IsVisible())
	{
		m_pIMEControl->OnRender(UPoint(0,0), screenRect);
	}

	if (keyboardCOntrol && keyboardCOntrol->IsVisible())
	{
		keyboardCOntrol->OnRender(keyboardCOntrol->GetWindowPos(), keyboardCOntrol->GetControlRect());
	}
	//for (int c = 0 ; c < m_Childs.size(); c++)
	//{
	//	UControl* pRoot = m_Childs[c];
	//	if(pRoot->renderTooltip(GetCursorPos()/*,"No ToolTip Message",pRoot->GetStaticClass()->GetClassName()*/))
	//		break;
	//}
	if(m_pMouseCtrl)
		m_pMouseCtrl->renderTooltip(GetCursorPos(), NULL);
	
	if (IsDraging())
	{
		URect DragCursor(m_ptCursorPos, m_DragDropItem.DragSkinSize);
		DragCursor.Offset(-m_DragDropItem.DragSkinSize.x>>1, -m_DragDropItem.DragSkinSize.y>>1);

		sm_UiRender->DrawSkin(m_DragDropItem.spSkin, m_DragDropItem.nIconIndex, DragCursor);
		if (m_DragDropItem.bDrawtext && m_DragDropItem.text.length() )
		{
			UDrawText(sm_UiRender,m_DragDropItem.spFont,DragCursor.GetPosition(), DragCursor.GetSize(),LCOLOR_GREEN, m_DragDropItem.text.c_str(), UT_CENTER);
		}
	}
	UControl::sm_UiRender->EndRender();
}

void UDesktop::Update(float fDeltaTime)
{
	for (int c = 0; c < m_Childs.size(); c++)
	{
		UControl* pRoot = m_Childs[c];
		pRoot->OnTimer(fDeltaTime);
	}

	if (m_pIMEControl)
	{
		m_pIMEControl->OnTimer(fDeltaTime);
	}
}

void UDesktop::SetFocusControl( UControl* pNewFocus )
{
	if (m_pFocusCtrl == pNewFocus)
	{
		return;
	}

	if (pNewFocus && pNewFocus->m_Style && !pNewFocus->m_Style->m_bKeyFocus && !pNewFocus->IsVisible())
	{
		return;
	}

	if (m_pFocusCtrl)
	{
		m_pFocusCtrl->OnLostFocus();
	}
	
	m_pFocusCtrl = pNewFocus;
	if (m_pFocusCtrl)
	{
		m_pFocusCtrl->OnGetFocus();
	}

}
void UDesktop::WindowKillFouce()
{
	m_uModifierFlags = 0;
}
void UDesktop::SetWindowSize(INT Widht, INT Height)
{
	m_WindowSize.Set(Widht, Height);
}