#ifndef _GUICANVAS_H_
#define _GUICANVAS_H_

#include "UControl.h"


class UDesktop : public UControl
{
	friend class USystemImp;
private:
	typedef UControl Parent;
	typedef UObject Grandparent;
	struct UDragDrop
	{
		UControl* pDragFrom;
		USkinPtr spSkin;
		int nIconIndex;
		void* UserData;
		UINT DataFlag;
		UPoint DragSkinSize;
		std::string text;
		bool bDrawtext;
		bool bDrawBgk;
		UFontPtr spFont;
	};
	/// @name Rendering
	/// @{
	URect      mOldUpdateRects[2];
	URect      mCurUpdateRect;
	float        rLastFrameTime;
	/// @}

	/// @name Cursor Properties
	/// @{

	float         mPixelsPerMickey; ///< This is the scale factor which relates mouse movement in pixels
	///  to units in the GUI.
	BOOL        cursorON;
	BOOL        mShowCursor;
	BOOL        mRenderFrOnt;
	UPointF     m_ptCursorPos;
	UPoint     lastCursorPt;
	UCursor   *defaultCursor;
	UCursor   *lastCursor;
	BOOL        lastCursorON;



	UControl* m_pFocusCtrl;		// 第一个可以获得焦点的子控件
	UControl* m_CapturedWnd;  // 已捕捉的窗口
	UControl*   m_pMouseCtrl;          ///< the cOntrol the mouse was last seen in unless some other On captured it
	BOOL                       m_bMouseCtrlClicked;   ///< whether the current ctrl has been clicked - used by helpctrl
	UINT                        m_PrevMouseTime;         ///< this determines how lOng the mouse has been in the same cOntrol
	UINT                        mNextMouseTime;         ///< used for OnMouseRepeat()
	UINT                        mInitialMouseDelay;     ///< also used for OnMouseRepeat()
	BOOL                       m_LeftBtnDown;       ///< Flag to determine if the buttOn is depressed
	BOOL                       m_RightBtnDown;  ///< BOOL to determine if the right buttOn is depressed
	UINT m_uModifierFlags;		// 控制键标记
	GuiEvent                   mLastEvent;

	BYTE                         mLastMouseClickCount;
	INT                        mLastMouseDownTime;
	BOOL                       mLeftMouseLast;
	BOOL                       mRightMouseLast;
	UControl* m_pIMEControl;
	UPoint				m_WindowSize;
	UDragDrop			m_DragDropItem;
	inline BOOL IsDraging() const { return m_DragDropItem.pDragFrom != NULL; }
	void FindMouseControl(const UPoint& pos);
	void ReFindMouseCtrl();
	/// @}

	/// @name Keyboard Input
	/// @{

	UControl   *keyboardCOntrol;                     ///<  All keyboard events will go to this ctrl first
	UINT          nextKeyTime;


	/// Accelerator key map
	struct AccKeyMap
	{
		AccKeyMap()
		{
			ctrl = NULL;
			keyCode = LK_NULL;
			modifier = 0;
		}
		UControl *ctrl;
		std::string opCode;
		UINT keyCode;
		UINT modifier;
	};
	std::vector<AccKeyMap> mAcceleratorMap;
	struct LastRespond
	{
		LastRespond()
		{
			ctrl = NULL;
		};
		UControl *ctrl;
		UINT KeyCode;
		UINT modifier;
		std::string opCodes;
	};
	std::vector<LastRespond> mLastRespond;
	BOOL FindLastRespond(const char* opCode);

	/// @}
	virtual BOOL IsDesktop() const	{	return TRUE;	}
public:
	// DECLARE_CONOBJECT(UDesktop);
	static void initPersistFields();

	UDesktop();
	~UDesktop();

	inline UINT GetModiferFlags() const { return m_uModifierFlags; }

	BOOL InitSoftKeyBoardControl();
	UCursor* FindCursorByID(UINT Id);
	UControlStyle* FindStyleByName(const char* StyleName);

	void Render(BOOL preRenderOnly);

	/// Updates various reflective materials in the scenegraph
	void updateReflectiOns();

	/// Repaints the entire canvas by calling reSetUpdateRegiOns() and then Render()
	void paint();

	/// Adds a dirty area to the canvas so it will be updated On the next frame
	/// @param   pos   Screen-coordinates of the upper-left hand corner of the dirty area
	/// @param   ext   Width/height of the dirty area
	void addUpdateRegiOn(UPoint pos, UPoint ext);

	/// ReSets the update regiOns so that the next call to Render will
	/// repaint the whole canvas
	void reSetUpdateRegiOns();

	/// This builds a rectangle which encompasses all of the dirty regiOns to be
	/// repainted
	/// @param   updateUniOn   (out) Rectangle which surrounds all dirty areas
	void buildUpdateUniOn(URect *updateUniOn);
	/// @}
	void WindowKillFouce();
	// IME
	void SetImeControl(UControl* pImeCtrl);
	BOOL HitTestImeControl(const UPoint& pt);
	BOOL HitTestSoftKeyControl(const UPoint& pt);
	/// @name Canvas COntent Management
	/// @{

	/// This Sets the cOntent cOntrol to something different
	/// @param   gui   New cOntent cOntrol
	void SetFrameWindow(UControl *gui);
	void ReCalcLayerOut();
	/// Returns the cOntent cOntrol
	UControl* GetCurFrame();

	void DestroyControl(UControl* pCtrl);
	/// Adds a dialog cOntrol Onto the stack of dialogs
	/// @param   gui   Dialog to add
	/// @param   layer   Layer to put dialog On
	void PushDialogControl(UControl *gui, INT layer = 0);

	void PopDialog(INT layer = 0);

	void PopDialog(UControl *gui);
	///@}

	/// This turns On/off frOnt-buffer rendering
	/// @param   frOnt   True if all rendering should be dOne to the frOnt buffer
	void SetRenderFrOnt(BOOL frOnt) { mRenderFrOnt = frOnt; }

	/// @name Cursor commands
	/// A cursor can be On, but not be shown. If a cursor is not On, than it does not
	/// process input.
	/// @{

	/// Sets the cursor for the canvas.
	/// @param   cursor   New cursor to use.
	void SetCursor(UCursor *cursor);

	/// Returns TRUE if the cursor is On.
	inline BOOL IsCursorOn() {return cursorON; }

	/// Turns the cursor On or off.
	/// @param   OnOff   True if the cursor should be On.
	inline void ShowCursor(BOOL bShow = TRUE);

	/// Sets the positiOn of the cursor
	/// @param   pt   Point, in screenspace for the cursor
	inline void SetCursorPos(const UPoint &pt)   { m_ptCursorPos = pt; }

	/// Returns the point, in screenspace, at which the cursor is located.
	inline const UPoint& GetCursorPos() const                 { return m_ptCursorPos; }
	inline void showCursor(BOOL state)            { mShowCursor = state; }
	BOOL isCursorShown()                   { return(mShowCursor); }

	void CaptureControl(UControl* pWnd);
	void ReleaseCapture(UControl* pWnd);
	inline void SetMouseControl(UControl* pWnd){m_pMouseCtrl = pWnd;};
	inline UControl* GetMouseControl()       { return m_pMouseCtrl; }
	inline UControl* GetCapturedCtrl()			{ return m_CapturedWnd; }
	inline UControl* GetFocusCtrl()			{ return m_pFocusCtrl;}
	inline UControl* GetDragSrcCtrl()			{ return m_DragDropItem.pDragFrom;}
	inline UControl* GetKeyBoardCtrl() {return keyboardCOntrol;}

	inline void SetDragCursorSize(const UPoint& NewSize)	{  m_DragDropItem.DragSkinSize = NewSize; }
	inline BOOL mouseButtOnDown(void) { return m_LeftBtnDown; }
	inline BOOL mouseRightButtOnDown(void) { return m_RightBtnDown; }
	void CheckLockMouseMove(const UPoint& pos);


	BOOL BeginDragDrop(UControl* pCtrl, USkin* pDragIconSkin, int nIconIndex, void* pUserData, DWORD DataFlag, char* text = NULL,IUFont* font = NULL ,bool bDrawBgk = true, bool drawtext = false, UPoint drawsize = UPoint(36,36));
	void EndDragDrop();

	void LeftMouseUp();
	void LeftBtnDown();
	void MouseMove(int x, int y);
	void LeftMouseDragged();

	void RightBtnDown();
	void RightMouseUp();
	void RightMouseDragged();

	void MouseWheel(int delta);

	BOOL KeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	BOOL KeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags);
	BOOL Char(UINT nCharCode, UINT nRepCnt, UINT nFlags);
	void Update(float fDeltaTime);
	/// @name Keyboard input methods
	/// First respOnders
	///
	/// A first respOnder is a the UControl which respOnds first to input events
	/// before passing them off for further processing.
	/// @{

	/// Moves the first respOnder to the next tabable cOntrole
	void TabNext(void);

	/// Moves the first respOnder to the previous tabable cOntrol
	void TabPrev(void);
	UControl* Last() const
	{
		int Size = m_Childs.size();
		if (Size > 0)
		{
			return m_Childs[Size -1];
		}
		return NULL;
	}
	/// Setups a keyboard accelerator which maps to a UControl.
	///
	/// @param   ctrl       UControl to map to.
	/// @param   index
	/// @param   keyCode    Key code.
	/// @param   modifier   Shift, ctrl, etc.
	void ReRigestAcceleratorKey();
	void AddAcceleratorKey(UControl *ctrl, const char * opCode, UINT keyCode, UINT modifier);
	void SetFocusControl(UControl *firstResponder);
	void SetWindowSize(INT Widht, INT Height);
};


#endif
