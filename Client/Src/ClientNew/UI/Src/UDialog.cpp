#include "StdAfx.h"
#include "UDialog.h"
#include "USystem.h"
#include "UBase.h"
#include "UMemory.h"
UIMP_CLASS(UDialog, UControl);
UBEGIN_MESSAGE_MAP(UDialog,UCmdTarget)
UON_BN_CLICKED(0xffbb, &UDialog::OnClickCloseBtn)
UEND_MESSAGE_MAP()

void UDialog::StaticInit()
{
	UREG_PROPERTY("bkgskin", UPT_STRING, UFIELD_OFFSET(UDialog, m_strFileName));
	UREG_PROPERTY("closeBtnSkin", UPT_STRING, UFIELD_OFFSET(UDialog, m_CloseBtnStrFile));
	UREG_PROPERTY("showclose", UPT_BOOL, UFIELD_OFFSET(UDialog, m_bShowClose));
	UREG_PROPERTY("DlgTitle", UPT_STRING,UFIELD_OFFSET(UDialog,m_DlgTitle));
	UREG_PROPERTY("CloseBtnPos", UPT_POINT, UFIELD_OFFSET(UDialog,m_CloseBtnPos));
}

UDialog::UDialog():m_bCanDrag(TRUE),m_bLimitDrag(TRUE),m_ptLeftBtnDown(0,0),m_CloseBtn(NULL),m_bShowClose(TRUE)
{
	m_CloseBtnPos = UPoint(0,0);
}

UDialog::~UDialog()
{

}


void UDialog::ActiveWindow()
{
	// TODO . check parent is desktop.
	UControl* parent = GetParent();
   if (parent)
   {
      parent->MoveChildTo(this, NULL);
   }
   UControl* pFocus = FindFirstFocus();
   SetFocusControl(pFocus);
}

void UDialog::OnClose()
{
	//sm_System->PopDialog(this);
}

void UDialog::OnClickCloseBtn()
{
	OnClose();
}
void UDialog::OnOk()
{
}

void UDialog::OnCancle()
{
}

BOOL UDialog::OnCreate()
{
	if(!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_strFileName.GetBuffer())
	{
		m_spBkgSkin = sm_System->LoadSkin(m_strFileName.GetBuffer());
		if (m_spBkgSkin == NULL)
		{
			return FALSE;
		}
	}
	USkin* pSkin = NULL;
	if (m_CloseBtn == NULL)
	{
		m_CloseBtn = (UBitmapButton*)UBitmapButton::CreateObject();

		if (m_CloseBtn)
		{
			m_CloseBtn->SetField("style", "UButton");
			m_CloseBtn->SetField("bitmap", "Public\\Close.skin");
			m_CloseBtn->SetId(0xffbb);
			m_CloseBtn->SetVisible(m_bShowClose);
			AddChild(m_CloseBtn);
			pSkin = m_CloseBtn->GetSkin();
		}
		else
		{
			return FALSE;
		}

		INT CBWidth = 32;
		INT CBHeight = 32;
		if (pSkin)
		{
			const URect* pRect = pSkin->GetSkinItemRect(0);
			if (pRect)
			{
				CBWidth = pRect->GetWidth();
				CBHeight = pRect->GetHeight();
			}
		}

		
		if (m_CloseBtnPos == UPoint(0,0))
		{
			URect CloseBtnRect;
			CloseBtnRect.left = m_Size.x - CBWidth - 8;
			CloseBtnRect.top = 22;
			CloseBtnRect.SetSize(CBWidth + 1, CBHeight + 1);

			m_CloseBtnPos = CloseBtnRect.GetPosition() ;
		}

		
		if (m_CloseBtn)
		{
			m_CloseBtn->SetPosition(m_CloseBtnPos);
			m_CloseBtn->SetSize(UPoint(CBWidth + 1, CBHeight + 1));
		}
	}
	return TRUE;
}

void UDialog::OnDestroy()
{
	m_spBkgSkin = NULL;
	//m_Visible = FALSE;
	UControl::OnDestroy();	
}
BOOL UDialog::OnEscape()
{
	//if(!UControl::OnEscape())
	//{
	//	OnClose();
	//	return TRUE;
	//}
	//return FALSE
	return UControl::OnEscape();
}
void UDialog::OnSize(const UPoint& NewSize)
{
	URect CloseBtnRec;
	if (m_CloseBtn)
	{
		CloseBtnRec = m_CloseBtn->GetControlRect();
	}
	UControl::OnSize(NewSize);
	if (m_CloseBtn)
	{
		m_CloseBtn->SetWindowRect(CloseBtnRec);
	}
}
void UDialog::SetVisible(BOOL value)
{
	if (value == m_Visible)
		return;
	if (value)
	{
		if (!m_Style->mSoundOpen.Empty())
		{
			sm_System->__PlaySound(m_Style->mSoundOpen.GetBuffer());
		}
		ActiveWindow();
	}
	else
	{
		if (!m_Style->mSoundClose.Empty())
		{
			sm_System->__PlaySound(m_Style->mSoundClose.GetBuffer());
		}
	}
	return UControl::SetVisible(value);
}

void UDialog::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	ActiveWindow();
	m_ptLeftBtnDown = pt;
	CaptureControl();
}

void UDialog::OnMouseUp(const UPoint& pt, UINT uFlags)
{
	ReleaseCapture();
}

// 限制窗口在桌面以内.
void UDialog::OnMouseDragged(const UPoint& position, UINT flags)
{
	if (!m_bCanDrag || !IsCaptured())
	{
		return;
	}
	UPoint delta = position - m_ptLeftBtnDown;
	m_ptLeftBtnDown = position;
	UPoint NewPos = GetWindowPos();
	
	/*UControl* pParent = GetParent();
	if (pParent->IsDesktop())
	{

	}else
	{

	}*/
	if (m_bLimitDrag)
	{
		UControl* pDesktop = GetRoot();
		assert(pDesktop && pDesktop->IsDesktop());
		UPoint DesktopSize = pDesktop->GetWindowSize();
		NewPos += delta;
		
		NewPos.x = LClamp(NewPos.x , 0, DesktopSize.x - m_Size.x); 
		NewPos.y = LClamp(NewPos.y , 0, DesktopSize.y - m_Size.y); 
	}else
	{
		NewPos += delta;
	}
	SetPosition(NewPos);
}

void UDialog::OnRender(const UPoint& offset, const URect& updateRect)
{
	if (m_spBkgSkin)
	{
		if (m_spBkgSkin->GetItemNum() == 1)
		{
			sm_UiRender->DrawSkin(m_spBkgSkin, 0, updateRect);
		}
		if (m_spBkgSkin->GetItemNum() == 3)
		{
			UDrawResizeImage(RESIZE_HEIGHT,sm_UiRender,m_spBkgSkin,offset,updateRect.GetSize());
			if (m_DlgTitle.GetBuffer())
			{
				UPoint _size(GetWidth(),20);
				UDrawText(sm_UiRender,m_Style->m_spFont,offset,
					_size,LCOLOR_BLUE,m_DlgTitle.GetBuffer(),UT_CENTER);
			}
		}
	}
	else
	{
		URect ctrlRect(offset, m_Size);
		if (m_Style->mOpaque)
			sm_UiRender->DrawRectFill(ctrlRect, m_Style->mFillColor);
	}

	RenderChildWindow(offset, updateRect);
}