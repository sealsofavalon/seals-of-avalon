#include "StdAfx.h"
#include "UEdit.h"
#include "UInput.h"
#include "UIME.h"
#include "UDesktop.h"
#include "USystem.h"
#include "USoftKeyboard.h"

UIMP_CLASS(UEdit, UControl);
void UEdit::StaticInit()
{
	UREG_PROPERTY("text",			UPT_STRINGW, UFIELD_OFFSET(UEdit,m_wstrBuffer));
	UREG_PROPERTY("Tiptext",			UPT_STRINGW, UFIELD_OFFSET(UEdit, m_wStrTipString));
	UREG_PROPERTY("historySize",	UPT_INT,	UFIELD_OFFSET(UEdit,mHistorySize));
	UREG_PROPERTY("bfoucesel",		UPT_BOOL,	UFIELD_OFFSET(UEdit,mbFouceChoose));
	UREG_PROPERTY("password",		UPT_BOOL,	UFIELD_OFFSET(UEdit,m_bPassword));     
	//	UREG_PROPERTY("tabComplete",       TypeBool,      Offset(mTabComplete,       UEdit));     
	//	UREG_PROPERTY("deniedSound",       TypeSFXProfilePtr, Offset(mDeniedSound, UEdit));
	//	UREG_PROPERTY("sinkAllKeyEvents",  UPT_BOOL,      Offset(mSinkAllKeyEvents,  UEdit));
	UREG_PROPERTY("passwordMask",	UPT_INT,	UFIELD_OFFSET(UEdit,m_PasswordMask)); 
	UREG_PROPERTY("useSoftKeyboard", UPT_BOOL, UFIELD_OFFSET(UEdit, m_bUserSoftKey));
}

struct EditHistory
{

};

struct EditContext
{
	EditContext()
	{
		UndoSelStart = UndoSelEnd = UndoCursorPos = 0;
		CaretLeftStrWidth = 0;
	}
	void SaveUndo(const UStringW* str, int start, int end, int cp)
	{
		UndoText.Set(str);
		UndoSelStart = start;
		UndoSelEnd = end;
		UndoCursorPos = cp;
		CaretLeftStrWidth = 0;
	}
	//undo members
	UStringW UndoText;
	INT			UndoSelStart;
	INT			UndoSelEnd;
	INT			UndoCursorPos;

	INT       CaretLeftStrWidth;	// 光标左边的字符宽度
};

UEdit::UEdit()
{
	m_pContext = NULL;
	mInsertOn = TRUE;
	m_nMaxLength = MAX_TEXT_LENGTH;
	m_nSelStart = 0;
	m_nSelEnd = 0;
	m_nCaretCP = 0;
	mCursorOn = TRUE;
	mNumFramesElapsed = 0;
	mNumberOnly = FALSE;

	mTimeLastCursorFlipped = 0.0f;
	mDragHit = FALSE;
	mTabComplete = FALSE;
	m_bUserSoftKey = FALSE ;
	m_bShowSoftKey = FALSE ;
	mScrollDir = 0;

	m_bPassword = FALSE;

	mSinkAllKeyEvents = FALSE;

	m_Active = TRUE;
	m_bEnableHistory = FALSE;
	mTextOffsetReset = TRUE;

	mHistoryDirty = FALSE;
	mHistorySize = 0;
	mHistoryLast = -1;
	mHistoryIndex = 0;
	mHistoryBuf = NULL;

	m_PasswordMask = L'*';
	m_Alignment = UT_LEFT;
	mbFouceChoose = FALSE;
}

UEdit::~UEdit()
{
	//delete the history buffer if it exists
	if (mHistoryBuf)
	{
		for (INT i = 0; i < mHistorySize; i++)
			delete [] mHistoryBuf[i];

		delete [] mHistoryBuf;
	}
}
//
//BOOL UEdit::onAdd()
//{
//	if ( ! Parent::onAdd() )
//		return FALSE;
//
//	//create the history buffer
//	if ( mHistorySize > 0 )
//	{
//		mHistoryBuf = new WCHAR*[mHistorySize];
//		for ( INT i = 0; i < mHistorySize; i++ )
//		{
//			mHistoryBuf[i] = new WCHAR[GuiTextCtrl::MAX_STRING_LENGTH + 1];
//			mHistoryBuf[i][0] = '\0';
//		}
//	}
//
//	if( mText && mText[0] )
//	{
//		SetText(mText);
//	}
//
//	return TRUE;
//}

BOOL UEdit::OnCreate()
{
	if (!UControl::OnCreate())
		return FALSE;
	m_pContext = new EditContext;
	if (m_pContext == NULL)
	{
		return FALSE;
	}

	m_spFont = m_Style->m_spFont;
	if (m_Style->mAlignment == UT_LEFT || m_Style->mAlignment == UT_RIGHT || m_Style->mAlignment == UT_CENTER)
	{
		m_Alignment = m_Style->mAlignment;
	}
	mFontColor = m_Style->m_FontColor;
	mNumberOnly = m_Style->mNumbersOnly;
	if (m_spFont == NULL)
	{
		UTRACE("Edit control's font is null.");
		return FALSE;
	}

	ResetTextOffset();
	return TRUE;
}

void UEdit::OnDestroy()
{
	if (m_pContext)
	{
		delete m_pContext;
		m_pContext = NULL;
	}
	m_spFont = NULL;
	UControl::OnDestroy();

	//GuiCanvas *root = getRoot();

	//// If this is the last awake text edit control, disable keyboard translation
	//--smNumAwake;
	//if (smNumAwake == 0)
	//	root->disableKeyboardTranslation();

	// If we're still the first responder then
	// restore the accelerators.
	//if ( m_pFocusCtrl == this )
	//	root->setNativeAcceleratorsEnabled( TRUE );
}

void UEdit::updateHistory( UStringW *inTxt, BOOL moveIndex )
{
	//const WCHAR* txt = inTxt->getPtr();

	//if(!txt)
	//	return;

	//if(!mHistorySize)
	//	return;

	//// see if it's already in
	//if(mHistoryLast == -1 || dStrcmp(txt, mHistoryBuf[mHistoryLast]))
	//{
	//	if(mHistoryLast == mHistorySize-1) // we're at the history limit... shuffle the pointers around:
	//	{
	//		WCHAR *first = mHistoryBuf[0];
	//		for(UINT i = 0; i < mHistorySize - 1; i++)
	//			mHistoryBuf[i] = mHistoryBuf[i+1];
	//		mHistoryBuf[mHistorySize-1] = first;
	//		if(mHistoryIndex > 0)
	//			mHistoryIndex--;
	//	}
	//	else
	//		mHistoryLast++;

	//	inTxt->getCopy(mHistoryBuf[mHistoryLast], GuiTextCtrl::MAX_STRING_LENGTH);
	//	mHistoryBuf[mHistoryLast][GuiTextCtrl::MAX_STRING_LENGTH] = '\0';
	//}

	//if(moveIndex)
	//	mHistoryIndex = mHistoryLast + 1;
}

int UEdit::GetText(char* dest, int nMaxLen, int nPos)
{
	return m_wstrBuffer.GetAnsi(dest, nMaxLen, nPos);
}  

int UEdit::GetText(WCHAR* dest, int nMaxLen, int nPos)
{
	if(nPos >= m_wstrBuffer.GetLength() || nMaxLen == 0)
	{
		return 0;
	}
	assert(nMaxLen > 0);
	
	nMaxLen = __min(nMaxLen, m_wstrBuffer.GetLength() - nPos );
	for(INT i=0; i<nMaxLen; i++)
	{
		dest[i] = m_wstrBuffer[nPos + i];
	}
	return nMaxLen;
}

void UEdit::SetText( const char *txt )
{
	if(txt && txt[0] != 0)
	{
		m_wstrBuffer.Set( txt );
	}
	else
	{
		m_wstrBuffer.SetEmpty();
	}

	int nCaretCP = m_wstrBuffer.GetLength();
	PlaceCaretAt(nCaretCP);
}

void UEdit::SetText( const WCHAR* txt)
{
	if(txt && txt[0] != 0)
	{
		m_wstrBuffer.Set(txt);
	}
	else
	{
		m_wstrBuffer.SetEmpty();
	}

	int nCaretCP = m_wstrBuffer.GetLength();
	PlaceCaretAt(nCaretCP);
}

void UEdit::SelAll()
{
	m_nSelStart = 0;
	m_nSelEnd = m_wstrBuffer.GetLength();
}

void UEdit::SetSel(int nStartCP, int nEndCP)
{
	int Len = m_wstrBuffer.GetLength();
	m_nSelStart = LClamp(nStartCP, 0, Len);
	m_nSelEnd = LClamp(nEndCP, 0, Len);
	if (m_nSelStart == m_nSelEnd)
	{
		m_nSelStart = m_nSelEnd = 0;
	}
}

INT UEdit::PlaceCaretPos( const UPoint &offset )
{
	if(m_wstrBuffer.GetLength() == 0)
	{
		if (m_nCaretCP != 0)
		{
			PlaceCaretAt(0);
		}
		return 0;
	}
		

	UPoint ctrlOffset = WindowToScreen( UPoint( 0, 0 ) );
	INT charLength = 0;
	INT curX = offset.x - ctrlOffset.x;

	//if the cursor is too far to the left
	if ( curX < 0 )
		return -1;

	//if the cursor is too far to the right
	if ( curX >= ctrlOffset.x + m_Size.x )
		return -2;

	curX = offset.x - mTextOffset.x;
	INT count=0;
	
	assert(m_spFont);
	for(count = 0; count < m_wstrBuffer.GetLength(); count++)
	{
		WCHAR c = m_wstrBuffer[count];
		
		if(!m_bPassword && !m_spFont->IsValidChar(c))
			continue;

		if(m_bPassword)
			charLength += m_spFont->GetCharXIncrement( m_PasswordMask );
		else
			charLength += m_spFont->GetCharXIncrement( c );

		if ( charLength > curX )
			break;      
	}
	PlaceCaretAt(count);
	return count;
}


void UEdit::PlaceCaretAt(INT nCP)
{
	/*
	|<pad>|<--Realy draw Text--> |    
	|         |     |                      |      |
	text      Client                              Text end.
	start     Start

	仅仅只pad 一次, 要么左边, 要么右边, 看填充情况.
	*/
	if (!m_pContext)
		return;
	if(m_wstrBuffer.GetLength() == 0)
	{
		m_nCaretCP = 0;
		m_pContext->CaretLeftStrWidth = 0;
		UPoint Position = WindowToScreen(UPoint(0, 0));
		mTextOffset.x = Position.x;
		m_nCaretXPos = Position.x;
		return;
	}
	// clear selection.
	//m_nSelStart = m_nSelEnd = 0;

	// 
	UPoint Position = WindowToScreen(UPoint(0, 0));
	INT Padding = m_Style->mTextOffset.x == 0 ? 3 : m_Style->mTextOffset.x;
	INT ClientWidth = m_Size.x;
	INT ClientHeight = m_Size.y;
	INT ClientRight = Position.x + ClientWidth;
	INT ClientPaddedRight = ClientRight - Padding;
	INT ClientPaddedLeft = Position.x + Padding;
	INT TextClientWidth = ClientWidth - Padding;
	m_nCaretCP = LClamp<INT>(nCP,0, m_wstrBuffer.GetLength());

	// 计算字符偏移
	INT textWidth = 0;
	INT nMaskWidth = 0;
	if (m_bPassword)
	{
		nMaskWidth = m_spFont->GetCharXIncrement(m_PasswordMask);
		textWidth = nMaskWidth * m_wstrBuffer.GetLength();
	}else
	{
		textWidth = m_spFont->GetStrNWidth(m_wstrBuffer, m_wstrBuffer.GetLength());
	}
		

	if ( TextClientWidth <= textWidth )
	{
		// 在没有焦点的情况下, 如果不能全部显示, 那么将非左对齐的尽量右移
		if ( m_Alignment == UT_RIGHT || m_Alignment == UT_CENTER )
		{
			if ( (mTextOffset.x + textWidth) < ClientPaddedRight)
			{
				mTextOffset.x = ClientPaddedRight - textWidth;
			}
		}
	}

	// 根据光标再次修正偏移

	WCHAR CaretChar = m_wstrBuffer[m_nCaretCP];
	INT CaretLeftStrWidth = 0;
	INT CaretCharWidth;
	// Alright, we want to terminate things momentarily.
	if(m_nCaretCP > 0)
	{
		CaretLeftStrWidth =  m_bPassword ? (nMaskWidth*m_nCaretCP) : m_spFont->GetStrNWidth(m_wstrBuffer, m_nCaretCP);
	}
	else
	{
		CaretLeftStrWidth = 0;
	}

	if ( CaretChar )
	{
		CaretCharWidth = m_bPassword ? nMaskWidth : m_spFont->GetCharWidth( CaretChar );
	}
	else
		CaretCharWidth = Padding;	// or 2.?

	// 判断光标是否超出范围
	if( (mTextOffset.x + CaretLeftStrWidth + CaretCharWidth) >= ClientPaddedRight)
	{
		// 将文字前移整个窗口宽度的25%
		INT skipForward = m_Size.x / 4;
		if ( CaretLeftStrWidth + skipForward > textWidth )
		{
			// 如果前移的时候会让尾部空余出来, 则将字符尾部正好至于文本客户区末尾.
			mTextOffset.x = ClientPaddedRight - textWidth;
		}
		else
		{
			mTextOffset.x -= skipForward;
		}
	}
	else if( mTextOffset.x + CaretLeftStrWidth < ClientPaddedLeft )
	{
		// 将文字后移, 以显示完整的光标附近的字符
		INT skipBackward = m_Size.x/ 4;

		if ((CaretLeftStrWidth - skipBackward) < 0 )
		{
			mTextOffset.x = ClientPaddedLeft;
		}
		else
		{
			mTextOffset.x += skipBackward;
		}
	}

	// 现在我们获得了正确的字符偏移, 我们要计算出第一个可以显示的字符.
	// 只考虑左边的情况.
	//if (mTextOffset.x < Position.x)
	//{
	//	INT CP;
	//	INT CharPos = mTextOffset.x;
	//	for(CP = 0; CP < m_wstrBuffer.GetLength(); CP++)
	//	{
	//		if(m_bPassword)
	//		{
	//			CharPos += m_spFont->GetCharXIncrement( m_PasswordMask );
	//		}
	//		else
	//		{
	//			WCHAR c = m_wstrBuffer[CP];
	//			if(!m_spFont->IsValidChar(c))
	//			{
	//				continue;
	//			}
	//			CharPos += m_spFont->GetCharXIncrement( c );
	//		}

	//		if ( CharPos > Position.x )
	//			break;      
	//	}

	//	// 现在CP 就是第一个可见的字符, 我们需要让其完全可见.
	//	mTextOffset.x += (CharPos - Position.x);
	//}	

	// 缓存变量, 便于渲染
	m_pContext->CaretLeftStrWidth = CaretLeftStrWidth;
	m_nCaretXPos = mTextOffset.x + CaretLeftStrWidth;
}


void UEdit::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	mDragHit = FALSE;

	if (nClickCnt == 2)
	{
		SelAll();
	}else
	{
		m_nSelStart = m_nSelEnd = 0;
	}

	INT pos = PlaceCaretPos( pt );

	if ( pos == -1 )
		m_nCaretCP = 0;
	else if ( pos == -2 ) //else if the position is to the right
		m_nCaretCP = m_wstrBuffer.GetLength();
	else 
		m_nCaretCP = pos;

	//save the mouseDragPos
	mMouseDragStart = m_nCaretCP;

	// lock the mouse
	CaptureControl();

	//set the drag var
	mDragHit = TRUE;

	//let the parent get the event
	SetFocusControl();
}

void UEdit::OnMouseDragged(const UPoint& pt, UINT uFlags)
{
	INT pos = PlaceCaretPos(pt);

	// if the position is to the left
	if ( pos == -1 )
		mScrollDir = -1;
	else if ( pos == -2 ) // the position is to the right
		mScrollDir = 1;
	else // set the new cursor position
	{
		mScrollDir = 0;
		m_nCaretCP = pos;
	}

	// update the block:
	m_nSelStart = __min( m_nCaretCP, mMouseDragStart );
	m_nSelEnd = __max( m_nCaretCP, mMouseDragStart );
	if ( m_nSelStart < 0 )
		m_nSelStart = 0;

	if ( m_nSelStart == m_nSelEnd )
		m_nSelStart = m_nSelEnd = 0;

	//let the parent get the event
	UControl::OnMouseDragged(pt, uFlags);
}

void UEdit::OnMouseUp(const UPoint& pt, UINT uFlags)
{
	mDragHit = FALSE;
	mScrollDir = 0;
	ReleaseCapture();
}

void UEdit::Cut()
{
	if(m_bPassword)
		return;

	if (m_nSelEnd > 0 && OpenClipboard(NULL))
	{
		EmptyClipboard();

		HGLOBAL hBlock = GlobalAlloc( GMEM_MOVEABLE, ( m_nSelEnd - m_nSelStart + 1 )*sizeof(WCHAR) );
		if( hBlock )
		{
			WCHAR *pszText = (WCHAR*)GlobalLock( hBlock );
			if( pszText )
			{
				m_wstrBuffer.Cut(m_nSelStart, m_nSelEnd - m_nSelStart, pszText);
				GlobalUnlock( hBlock );
			}
			SetClipboardData( CF_UNICODETEXT, hBlock );
		}
		CloseClipboard();
		if( hBlock )
			GlobalFree( hBlock );

		
		PlaceCaretAt(m_nSelStart);
		m_nSelStart = 0;
		m_nSelEnd = 0;
	}
}

void UEdit::Copy()
{
	if(m_bPassword)
		return;

	if (m_nSelEnd > 0 )
	{
		if (!OpenClipboard(NULL))
		{
			return;
		}

		BOOL bSupport = IsClipboardFormatAvailable(CF_UNICODETEXT);
		if (!bSupport)
		{
			return ;
		}
		if(!EmptyClipboard())
		{
			UTRACE("can't empty clipboard.");
			return;
		}
		HGLOBAL hBlock = GlobalAlloc( GMEM_MOVEABLE, (m_nSelEnd - m_nSelStart + 1 )*sizeof(WCHAR) );
		if( hBlock )
		{
			WCHAR *pszText = (WCHAR*)GlobalLock( hBlock );
			if( pszText )
			{
				m_wstrBuffer.SubStr(pszText, m_nSelStart, m_nSelEnd - m_nSelStart);
				GlobalUnlock( hBlock );
			}
			HANDLE hcbData = SetClipboardData(CF_UNICODETEXT, hBlock );
			if (hcbData == NULL)
			{
				DWORD Error = GetLastError();
				UTRACE("SetClipboardData failed , err : %d ", Error);
			}
		}
		CloseClipboard();
		if( hBlock )
			GlobalFree( hBlock );
	}
}

//
void UEdit::Clear()
{
	m_wstrBuffer.SetEmpty();
	this->PlaceCaretAt(0);
}
//

void UEdit::Paste()
{           
	 if( OpenClipboard( NULL ) )
    {
        HANDLE handle = GetClipboardData( CF_UNICODETEXT );
        if( handle )
        {
			WCHAR *pwszText = (WCHAR*)GlobalLock( handle );
			if( pwszText )
			{
				// 首先删除选择内容.
				if (m_pContext)
				{
					//m_pContext->SaveUndo(&m_wstrBuffer, m_nSelStart, m_nSelEnd, m_nCaretCP);
				}
				int InsertCP = 0;
				if (m_nSelEnd > 0)
				{
					m_wstrBuffer.Cut(m_nSelStart, m_nSelEnd - m_nSelStart);
					InsertCP = m_nSelStart;
					m_nSelStart = 0;
					m_nSelEnd = 0;
				}
				else
				{
					InsertCP = m_nCaretCP;
				}
					
				UStringW wszText;
				wszText.Set(pwszText);
				INT SrcLen = wszText.GetAnsiLength()/*wcslen(pwszText)*/;
				INT CurLen = m_wstrBuffer.GetAnsiLength()/*m_wstrBuffer.GetLength()*/;
				if (SrcLen + CurLen <= m_nMaxLength)
				{
					INT WLen = wcslen(pwszText);
					m_wstrBuffer.Insert(InsertCP, pwszText, WLen);
					PlaceCaretAt(InsertCP + WLen);
				}
				else
				{
					SrcLen = m_nMaxLength - CurLen/* - 1*/;

					UFrameMarker<char> Markerchar(SrcLen + 1);
					int nRet = WideCharToMultiByte(CP_UTF8, 0, pwszText,wcslen(pwszText), Markerchar,SrcLen, NULL, NULL);
					Markerchar[SrcLen] = 0;

					UFrameMarker<WCHAR> Marker(SrcLen + 1);
					nRet = MultiByteToWideChar(CP_UTF8, 0, Markerchar, SrcLen, Marker, SrcLen);
					Marker[nRet] =0;

					m_wstrBuffer.Insert(InsertCP,Marker,-1);
					PlaceCaretAt(InsertCP + nRet);
				}
				GlobalUnlock( handle );
            }
        }else
		{
			DWORD ec = GetLastError();
			UTRACE("Can't get clipboard data %d.", ec);
		}
        CloseClipboard();
    }
}

BOOL UEdit::Undo()
{
	UTRACE("UEdit::Undo() unimplement yet.");
	//StringBuffer tempBuffer;
	//INT tempBlockStart;
	//INT tempBlockEnd;
	//INT tempCursorPos;

	////save the current
	//tempBuffer.set(&m_wstrBuffer);
	//tempBlockStart = m_nSelStart;
	//tempBlockEnd   = m_nSelEnd;
	//tempCursorPos  = m_nCaretCP;

	////restore the prev
	//m_wstrBuffer.set(&UndoText);
	//m_nSelStart = UndoSelStart;
	//m_nSelEnd   = UndoSelEnd;
	//m_nCaretCP  = UndoCursorPos;

	////update the undo
	//UndoText.set(&tempBuffer);
	//UndoSelStart = tempBlockStart;
	//UndoSelEnd   = tempBlockEnd;
	//UndoCursorPos  = tempCursorPos;
	return TRUE;
}
BOOL UEdit::SoftKeyBoardShow(BOOL bshow)
{
	if (m_bUserSoftKey && m_Active && IsVisible())
	{
		m_bShowSoftKey = bshow ;
		UControl* pkCtrl = g_Desktop->GetKeyBoardCtrl();
		if (pkCtrl)
		{
			USoftKeyBoard* pkKeyBoard = UDynamicCast(USoftKeyBoard, pkCtrl);
			if (pkKeyBoard)
			{
				if (bshow)
				{
					pkKeyBoard->Show(this);
				}else
				{
					pkKeyBoard->Hiden();
				}
				return TRUE ;
			}
		}
	}
	return FALSE ;
}
void UEdit::SoftKeyBoardKeyMsg(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	OnKeyDown(nKeyCode,nRepCnt, nFlags);
}
void UEdit::SoftKeyBoardChar(UINT nCharCode, UINT nRepCnt, UINT nFlags)
{
	OnChar(nCharCode, nRepCnt, nFlags);
}

// WM_CHAR 总是跟随在WM_KEYDOWN 之后, 所以我开始想抛弃对KeyDown的响应
// 但后来发现对EDIT 的操作, 有时候光靠CHAR 是不行的, 起码,非主键盘的按键并不
// 产生WM_CHAR 消息, 而仅仅只有WM_KEYDOWN. 因此, 我们必须分开处理.
BOOL UEdit::OnChar(UINT nCharCode, UINT nRepCnt, UINT nFlags)
{
	assert(m_pContext);
	if(!m_Active)
		return FALSE;

	// 处理字符输入
	if ( m_spFont->IsValidChar((WCHAR)nCharCode) )
	{
		//see if it's a number field
		if ( mNumberOnly )
		{
			if (nCharCode == L'-')
			{
				//a minus sign only exists at the beginning, and only a single minus sign
				if ( m_nCaretCP != 0 )
				{
					playDeniedSound();
					return TRUE;
				}

				if ( mInsertOn && ( m_wstrBuffer[0] == L'-' ) ) 
				{
					playDeniedSound();
					return TRUE;
				}
			}
			else if ( nCharCode < L'0' || nCharCode > L'9' )
			{
				playDeniedSound();
				return TRUE;
			}
		}

		//save the current state
		//m_pContext->SaveUndo(&m_wstrBuffer, m_nSelStart, m_nSelEnd, m_nCaretCP);

		//delete anything highlighted
		if ( m_nSelEnd > 0 )
		{
			m_wstrBuffer.Cut(m_nSelStart, m_nSelEnd-m_nSelStart);
			PlaceCaretAt(m_nSelStart);
			m_nSelStart = 0;
			m_nSelEnd   = 0;
		}

		INT TextLen = /*m_wstrBuffer.GetLength();*/m_wstrBuffer.GetAnsiLength();

		if ( ( mInsertOn && ( TextLen < m_nMaxLength ) ) ||
			( !mInsertOn && ( m_nCaretCP < m_nMaxLength ) ) )
		{
			if ( m_nCaretCP == TextLen )
			{
				m_wstrBuffer.Append(nCharCode);
				PlaceCaretAt(m_nCaretCP+1);
			}
			else
			{
				if ( mInsertOn )
				{
					m_wstrBuffer.Insert(m_nCaretCP, nCharCode);
					PlaceCaretAt(m_nCaretCP+1);
				}
				else
				{
					m_wstrBuffer.Cut(m_nCaretCP, 1);
					m_wstrBuffer.Insert(m_nCaretCP, nCharCode);
					PlaceCaretAt(m_nCaretCP+1);
				}
			}
		}
		else
		{
			playDeniedSound();
			if (mNumberOnly)
			{
				char buf[256];
				m_wstrBuffer.Set(itoa(pow(10.f, m_nMaxLength) - 1, buf, 10));
				PlaceCaretAt(m_wstrBuffer.GetLength()+1);
			}
		}			
		if (nCharCode == 32)
		{
			DispatchNotify(UED_CHANGED);
		}
		//reset the history index
		mHistoryDirty = TRUE;

		//	//execute the console command if it exists
		//	execConsoleCallback();

		return TRUE;
	}
	return TRUE;
}
BOOL UEdit::OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	return UControl::OnKeyUp(nKeyCode,nRepCnt,nFlags);
}
BOOL UEdit::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	if(!m_Active )
		return FALSE;

	assert(m_pContext);
	UINT KeyCode = nKeyCode;
	UINT Flags = nFlags;

	INT stringLen = m_wstrBuffer.GetLength();
	// 优先处理退格操作.
	if(KeyCode == LK_BACKSPACE)
	{
		//save the current state
			//m_pContext->SaveUndo(&m_wstrBuffer, m_nSelStart, m_nSelEnd, m_nCaretCP);
			if (m_nSelEnd > 0) // 如果有选择字符
			{
				m_wstrBuffer.Cut(m_nSelStart, m_nSelEnd-m_nSelStart);
				PlaceCaretAt(m_nSelStart);
				m_nSelStart = 0;
				m_nSelEnd   = 0;
				mHistoryDirty = TRUE;
				//DispatchNotify(UED_CHANGED);
			}
			else if (m_nCaretCP > 0) // 如果当前光标不在起点
			{
				m_wstrBuffer.Cut(m_nCaretCP-1, 1);
				PlaceCaretAt(m_nCaretCP - 1);
				mHistoryDirty = TRUE;
				//DispatchNotify(UED_CHANGED);
			}
			return TRUE;
	}else if (KeyCode == LK_RETURN || KeyCode == LK_NUMPADENTER)
	{
		updateHistory(&m_wstrBuffer, TRUE);
		mHistoryDirty = FALSE;
		DispatchNotify(UED_UPDATE);
		if (m_Style->mReturnTab)
		{
			OnLostFocus();
		}

		if (m_Style->mReturnTab)
		{
			UDesktop* root = (UDesktop*)GetRoot();
			if (root)
			{
				root->TabNext();
				return TRUE;
			}
		}
		return TRUE;
	}else if (Flags & LKM_SHIFT)
	{
		switch (KeyCode)
		{
		case LK_TAB:
			if ( mTabComplete )
			{
				DispatchNotify(UED_UPDATE);
				return( TRUE );
			}
			break; 

		case LK_HOME:		// 光标移到起点
			m_nSelStart = 0;
			m_nSelEnd = m_nCaretCP;
			PlaceCaretAt(0);
			return TRUE;

		case LK_END:		// 光标移到终点
			m_nSelStart = m_nCaretCP;
			m_nSelEnd = stringLen;
			PlaceCaretAt(stringLen);
			return TRUE;

		case LK_LEFT:
			if (m_nCaretCP > 0 && stringLen > 0)
			{
				if (m_nCaretCP == m_nSelEnd)
				{
					PlaceCaretAt(m_nCaretCP-1);
					m_nSelEnd--;
					if (m_nSelEnd == m_nSelStart)
					{
						// 取消选择
						m_nSelStart = 0;
						m_nSelEnd = 0;
					}
				}
				else 
				{
					//m_nCaretCP--;
					PlaceCaretAt(m_nCaretCP-1);
					m_nSelStart = m_nCaretCP;

					if (m_nSelEnd == 0)
					{
						m_nSelEnd = m_nCaretCP + 1;
					}
				}
			}
			return TRUE;

		case LK_RIGHT:
			if (m_nCaretCP < stringLen)
			{
				if ((m_nCaretCP == m_nSelStart) && (m_nSelEnd > 0))
				{
					PlaceCaretAt(m_nCaretCP+1);
					m_nSelStart++;
					if (m_nSelStart == m_nSelEnd)
					{
						m_nSelStart = 0;
						m_nSelEnd = 0;
					}
				}
				else
				{
					if (m_nSelEnd == 0)
					{
						m_nSelStart = m_nCaretCP;
						m_nSelEnd = m_nCaretCP;
					}
					PlaceCaretAt(m_nCaretCP+1);
					m_nSelEnd++;
				}
			}
			return TRUE;
		}
	}
	else if (Flags & LKM_CTRL)
	{
		switch(KeyCode)
		{
		case LK_A:
			{
				return TRUE;
			}
		case LK_C:
			{
				Copy();
				return TRUE;
			}
		case LK_X:
			{
				Cut();
				// Notify
				return TRUE;
			}
		case LK_V:
			{
				Paste();
				// Notify.
				return TRUE;
			}

		case LK_Z:
			if (!mDragHit)
			{
				Undo();
				return TRUE;
			}


		case LK_DELETE:
		case LK_BACKSPACE:
			//save the current state
			//m_pContext->SaveUndo(&m_wstrBuffer, m_nSelStart, m_nSelEnd, m_nCaretCP);
			//delete everything in the field
			m_wstrBuffer.SetEmpty();
			PlaceCaretAt(0);
			m_nSelStart = 0;
			m_nSelEnd   = 0;
			//DispatchNotify(UED_CHANGED);
			return TRUE;
		}
	}else
	{
		switch(KeyCode)
		{
			/*	case LK_ESCAPE:
			if ( mEscapeCommand[0] )
			{
			Con::setVariable("$ThisControl", avar("%d", getId()));
			Con::evaluate( mEscapeCommand );
			return( TRUE );
			}
			return( Parent::onKeyDown( event ) );*/

		

		//case LK_UP:
		//	{
		//		if(mHistoryDirty)
		//		{
		//			updateHistory(&m_wstrBuffer, FALSE);
		//			mHistoryDirty = FALSE;
		//		}

		//		mHistoryIndex--;

		//		if(mHistoryIndex >= 0 && mHistoryIndex <= mHistoryLast)
		//			SetText(mHistoryBuf[mHistoryIndex]);
		//		else if(mHistoryIndex < 0)
		//			mHistoryIndex = 0;

		//		return TRUE;
		//	}

		//case LK_DOWN:
		//	if(mHistoryDirty)
		//	{
		//		updateHistory(&m_wstrBuffer, FALSE);
		//		mHistoryDirty = FALSE;
		//	}
		//	mHistoryIndex++;
		//	if(mHistoryIndex > mHistoryLast)
		//	{
		//		mHistoryIndex = mHistoryLast + 1;
		//		SetText((WCHAR*)NULL);
		//	}
		//	else
		//		SetText(mHistoryBuf[mHistoryIndex]);
		//	return TRUE;

		case LK_LEFT:
			m_nSelStart = 0;
			m_nSelEnd = 0;
			if (m_nCaretCP > 0)
			{
				PlaceCaretAt(m_nCaretCP-1);
			}
			return TRUE;

		case LK_RIGHT:
			m_nSelStart = 0;
			m_nSelEnd = 0;
			if (m_nCaretCP < stringLen)
			{
				PlaceCaretAt(m_nCaretCP+1);
			}
			return TRUE;
		case LK_DELETE:
			//save the current state
			//m_pContext->SaveUndo(&m_wstrBuffer, m_nSelStart, m_nSelEnd, m_nCaretCP);

			if (m_nSelEnd > 0) // 如果有选择字符
			{
				mHistoryDirty = TRUE;
				m_wstrBuffer.Cut(m_nSelStart, m_nSelEnd-m_nSelStart);

				PlaceCaretAt(m_nSelStart);
				m_nSelStart = 0;
				m_nSelEnd = 0;

				//DispatchNotify(UED_CHANGED);
			}
			else if (m_nCaretCP < stringLen) // 删除后面一个字符
			{
				mHistoryDirty = TRUE;
				m_wstrBuffer.Cut(m_nCaretCP, 1);

				//DispatchNotify(UED_CHANGED);
			}
			return TRUE;

		case LK_INSERT:
			mInsertOn = !mInsertOn;
			return TRUE;

		case LK_HOME:
			m_nSelStart = 0;
			m_nSelEnd   = 0;
			PlaceCaretAt(0);
			return TRUE;

		case LK_END:
			m_nSelStart = 0;
			m_nSelEnd   = 0;
			PlaceCaretAt(stringLen);
			return TRUE;
		}
	}

	switch ( KeyCode )
	{
	case LK_TAB:
		if ( mTabComplete )
		{
			DispatchNotify(UED_UPDATE);
			return( TRUE );
		}
	case LK_UP:
		
	case LK_DOWN:
		
	case LK_ESCAPE:
		return UControl::OnKeyDown( nKeyCode, nRepCnt, nFlags );
	}

	if (mSinkAllKeyEvents)
		return TRUE;

	return TRUE;
	//return UControl::OnKeyDown( nKeyCode, nRepCnt, nFlags );
}

void UEdit::OnGetFocus()
{
	UControl::OnGetFocus();
	
	sm_System->SetIMEEnable(TRUE);
	if(mbFouceChoose){
		SelAll();
		PlaceCaretAt(m_nSelEnd);
	}

	if (m_bUserSoftKey && m_bShowSoftKey )
	{
		UControl* pkCtrl = g_Desktop->GetKeyBoardCtrl();
		if (pkCtrl)
		{
			USoftKeyBoard* pkKeyBoard = UDynamicCast(USoftKeyBoard, pkCtrl);
			if (pkKeyBoard)
			{
				pkKeyBoard->Show(this);
			}
		}
	}

	//UIME::EnableIME();
	//GuiCanvas *root = getRoot();
	//root->enableKeyboardTranslation();

	//// If the native OS accelerator keys are not disabled
	//// then some key events like Delete, ctrl+V, etc may
	//// not make it down to us.
	//root->setNativeAcceleratorsEnabled( FALSE );
}

void UEdit::OnLostFocus()
{
	//UIME::DisableIME();
	sm_System->SetIMEEnable(FALSE);
	if(mbFouceChoose)
	{
		SetSel(0,0);
	}
	
	if (m_bUserSoftKey  && m_bShowSoftKey)
	{
		UControl* pkCtrl = g_Desktop->GetKeyBoardCtrl();
		if (pkCtrl)
		{
			USoftKeyBoard* pkKeyBoard = UDynamicCast(USoftKeyBoard, pkCtrl);
			if (pkKeyBoard)
			{
				pkKeyBoard->Hiden();
			}
		}
	}
	
	
	//GuiCanvas *root = getRoot();
	//root->setNativeAcceleratorsEnabled( TRUE );
	//root->disableKeyboardTranslation();

	////first, update the history
	//updateHistory( &m_wstrBuffer, TRUE );

	////execute the validate command
	//if ( mValidateCommand[0] )
	//{
	//	char buf[16];
	//	dSprintf(buf, sizeof(buf), "%d", getId());
	//	Con::setVariable("$ThisControl", buf);
	//	Con::evaluate( mValidateCommand, FALSE );
	//}

	//if( isMethod( "onValidate" ) )
	//	Con::executef( this, "onValidate" );

	//// Redraw the control:
	//setUpdate();
}

void UEdit::ResetTextOffset()
{
	if (!m_wstrBuffer.Empty())
	{
		UPoint Position = WindowToScreen(UPoint(0, 0));
		INT Padding = m_Style->mTextOffset.x == 0 ? 3 : m_Style->mTextOffset.x;
		// 计算字符偏移
		if(!m_spFont)return;
		INT textWidth = m_spFont->GetStrNWidth(m_wstrBuffer, m_wstrBuffer.GetLength());
		INT OffsetX = Position.x;
		INT TextClientWidth = m_Size.x - Padding;
		if ( TextClientWidth > textWidth )
		{
			switch( m_Alignment )
			{
			case UT_RIGHT:
				OffsetX += ( TextClientWidth - textWidth);
				break;
			case UT_CENTER:
				OffsetX += ( ( m_Size.x - textWidth ) / 2 );
				break;
			default:
			case UT_LEFT:
				OffsetX += Padding;
				break;
			}
		}
		else
		{
			OffsetX += Padding;
		}

		mTextOffset.x = OffsetX;
	}else
	{
		UPoint Position = WindowToScreen(UPoint(0, 0));
		mTextOffset.x = Position.x;
	}
}
INT getHexVal(char c)
{
	if(c >= '0' && c <= '9')
		return c - '0';
	else if(c >= 'A' && c <= 'Z')
		return c - 'A' + 10;
	else if(c >= 'a' && c <= 'z')
		return c - 'a' + 10;
	return -1;
}
void UEdit::SetFontColor(const char * color)
{
	int num = 0;
	while (color[num++] != '\0');
	num--;
	if (num != 6 && num != 8)
		return;
	mFontColor.r   = getHexVal(color[0]) * 16 + getHexVal(color[1]);
	mFontColor.g = getHexVal(color[2]) * 16 + getHexVal(color[3]);
	mFontColor.b  = getHexVal(color[4]) * 16 + getHexVal(color[5]);
	if (num == 8)
		mFontColor.a = getHexVal(color[6]) * 16 + getHexVal(color[7]);
	else
		mFontColor.a = 255;
}

void UEdit::OnSize(const UPoint& NewSize)
{
	UControl::OnSize(NewSize);
	
	ResetTextOffset();
}

void UEdit::OnMove(const UPoint& NewPos)
{
	//ResetTextOffset();
	//mTextOffset.x += NewPos.x;
	mTextOffset += NewPos;
}

void UEdit::OnRender(const UPoint& offset, const URect& updateRect)
{
    g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
    g_pRenderInterface->SetScissorRect(sm_UiRender->GetClipRect());

	URect ctrlRect( offset, m_Size);
	//if opaque, fill the update rect with the fill color
	if ( m_Style->mOpaque )
	{
		if(IsFocused())
		{
			sm_UiRender->DrawRectFill(ctrlRect, m_Style->mFillColorHL);
		}
		else
		{
			sm_UiRender->DrawRectFill(ctrlRect, m_Style->mFillColor);
		}
	}

	//if there's a border, draw the border
	if ( m_Style->mBorder )
		sm_UiRender->DrawRect(ctrlRect, m_Style->mBorderColor);
	
	if (!m_bPassword)
	{
		DrawContext( ctrlRect, IsFocused() );
	}else{
		DrawMask(ctrlRect, IsFocused());
	}
	
    g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
}

void UEdit::OnTimer(float fDeltaTime)
{
	//if ( IsFocused() )
	//{
	//	UINT timeElapsed = Platform::getVirtualMilliseconds() - mTimeLastCursorFlipped;
	//	mNumFramesElapsed++;
	//	if ( ( timeElapsed > 500 ) && ( mNumFramesElapsed > 3 ) )
	//	{
	//		mCursorOn = !mCursorOn;
	//		mTimeLastCursorFlipped = Platform::getVirtualMilliseconds();
	//		mNumFramesElapsed = 0;
	//		setUpdate();
	//	}

	if (IsFocused())
	{
		//控制输入框光标闪烁
		mTimeLastCursorFlipped += (fDeltaTime*0.5);
		if (mTimeLastCursorFlipped > 10)
		{
			mTimeLastCursorFlipped = 0;
		}
		UINT pIndex = (mTimeLastCursorFlipped * 60 / 10);
		mCursorOn = pIndex % 2;	 
	}

	//	//update the cursor if the text is scrolling
	//	if ( mDragHit )
	//	{
	//		if ( ( mScrollDir < 0 ) && ( m_nCaretCP > 0 ) )
	//			m_nCaretCP--;
	//		else if ( ( mScrollDir > 0 ) && ( m_nCaretCP < (INT) m_wstrBuffer.length() ) )
	//			m_nCaretCP++;
	//	}
	//}
}

void UEdit::DrawMask(const URect& drawRect, BOOL isFocused)
{
	/*
	|<pad>|<--Realy draw Text--> |    
	|         |     |                      |      |
	text      Client                              Text end.
	start     Start

	*/

	UPoint drawPoint(drawRect.left, drawRect.top);
	UPoint Padding;
	Padding.Set(( m_Style->mTextOffset.x != 0 ? m_Style->mTextOffset.x : 3 ), m_Style->mTextOffset.y);

	INT ClientWidth = drawRect.GetWidth();
	INT ClientHeight = drawRect.GetHeight();
	INT TextClientWidth = ClientWidth - Padding.x;

	// 垂直中线对齐
	drawPoint.y += (( ( ClientHeight - m_spFont->GetHeight() ) / 2 ) + Padding.y);
	mTextOffset.y = drawPoint.y;
	mFontColor = m_Active ? mFontColor : m_Style->m_FontColorNA;

	// now draw the text
	UPoint cursorStart, cursorEnd;

	// calculate the cursor
	if ( isFocused )
	{
		cursorStart.x = mTextOffset.x + m_pContext->CaretLeftStrWidth;
		cursorEnd.x = cursorStart.x;

		INT cursorHeight = m_spFont->GetHeight();
		if ( cursorHeight < drawRect.GetHeight() )
		{
			cursorStart.y = drawPoint.y;
			cursorEnd.y = cursorStart.y + cursorHeight;
		}
		else
		{
			cursorStart.y = drawRect.top;
			cursorEnd.y = drawRect.bottom;
		}
	}

	//draw the text
	if ( !isFocused )
		m_nSelStart = m_nSelEnd = 0;

	//also verify the block start/end
	if (m_nSelStart > m_wstrBuffer.GetLength() || 
		m_nSelEnd > m_wstrBuffer.GetLength() || 
		m_nSelStart > m_nSelEnd
		)
	{
		m_nSelStart = m_nSelEnd = 0;
	}

	if (!m_wstrBuffer.Empty())
	{
		UPoint tempOffset = mTextOffset;

		INT wStrLen = m_wstrBuffer.GetLength();
		INT MaskCharWidth = m_spFont->GetCharXIncrement(m_PasswordMask);

		UFrameMarker<WCHAR> MaskedStr(wStrLen + 1);
		//WCHAR* pwszMasked = (WCHAR*)UMalloc((wStrLen + 1)*sizeof(WCHAR));
		//pwszMasked[wStrLen] = 0;
		MaskedStr[wStrLen] = 0;
		for (int i =0; i < wStrLen; i++)
		{
			MaskedStr[i] = m_PasswordMask;
		}
		//渲染选择区域前的字符
		if ( m_nSelStart > 0 )
		{
			int nWidth = sm_UiRender->DrawTextN(m_spFont, tempOffset, MaskedStr, m_nSelStart, NULL, mFontColor);
			int StrWidth = MaskCharWidth * m_nSelStart;
			tempOffset.x += StrWidth;
		}

		//渲染高亮字符
		if ( m_nSelEnd > 0 )
		{
			const WCHAR* HLText = MaskedStr + m_nSelStart;
			UINT HLLen = m_nSelEnd-m_nSelStart;

			INT StrWidth = MaskCharWidth *HLLen;

			// 先渲染背景矩形
			URect HLRect;
			HLRect.left = tempOffset.x;
			HLRect.top = drawRect.top;
			HLRect.right = HLRect.left + StrWidth;
			HLRect.bottom = drawRect.bottom -1;
			sm_UiRender->DrawRectFill(HLRect, m_Style->m_FontColorSEL);
			sm_UiRender->DrawTextN(m_spFont, tempOffset, HLText, HLLen, NULL, m_Style->m_FontColorHL );
			tempOffset.x += StrWidth;
		}

		////draw the portion after the highlite
		if(m_nSelEnd < m_wstrBuffer.GetLength())
		{
			const WCHAR* Tail = MaskedStr + m_nSelEnd;
			UINT ToDraw = wStrLen - m_nSelEnd;
			sm_UiRender->DrawTextN(m_spFont, tempOffset,Tail, ToDraw, /*m_Style->m_FontColors*/NULL, mFontColor);
		}
		//UFree(pwszMasked);
	}
	
	//draw the cursor
	if ( isFocused && mCursorOn )
	{

		sm_UiRender->DrawRectFill( URect(cursorStart, UPoint(3, cursorEnd.y - cursorStart.y)), m_Style->mCursorColor );
		//sm_UiRender->DrawLine( cursorStart, cursorEnd, m_Style->mCursorColor );
	}
}

void UEdit::DrawContext( const URect &drawRect, BOOL isFocused )
{
	/*
	|<pad>|<--Realy draw Text--> |    
	|         |     |                      |      |
	text      Client                              Text end.
	start     Start

	*/
	if (m_wstrBuffer.Empty() && !isFocused)
	{
		sm_UiRender->DrawTextN(m_spFont, drawRect.GetPosition(), m_wStrTipString, m_wStrTipString.GetLength(), NULL, mFontColor);
		return;
	}
	UPoint drawPoint(drawRect.left, drawRect.top);
	UPoint Padding;
	Padding.Set(( m_Style->mTextOffset.x != 0 ? m_Style->mTextOffset.x : 3 ), m_Style->mTextOffset.y);

	INT ClientWidth = drawRect.GetWidth();
	INT ClientHeight = drawRect.GetHeight();
	INT TextClientWidth = ClientWidth - Padding.x;

	// 垂直中线对齐
	drawPoint.y += (( ( ClientHeight - m_spFont->GetHeight() ) / 2 ) + Padding.y);
	mTextOffset.y = drawPoint.y;
	mFontColor = m_Active ? mFontColor : m_Style->m_FontColorNA;

	// now draw the text
	UPoint cursorStart, cursorEnd;

	// calculate the cursor
	if ( isFocused )
	{
		cursorStart.x = mTextOffset.x + m_pContext->CaretLeftStrWidth;
		cursorEnd.x = cursorStart.x;

		INT cursorHeight = m_spFont->GetHeight();
		if ( cursorHeight < drawRect.GetHeight() )
		{
			cursorStart.y = drawPoint.y;
			cursorEnd.y = cursorStart.y + cursorHeight;
		}
		else
		{
			cursorStart.y = drawRect.top;
			cursorEnd.y = drawRect.bottom;
		}
	}

	//draw the text
	if ( !isFocused )
		m_nSelStart = m_nSelEnd = 0;

	//also verify the block start/end
	if (m_nSelStart > m_wstrBuffer.GetLength() || 
		m_nSelEnd > m_wstrBuffer.GetLength() || 
		m_nSelStart > m_nSelEnd
		)
	{
		m_nSelStart = m_nSelEnd = 0;
	}

	UPoint tempOffset = mTextOffset;

	//渲染选择区域前的字符
	if ( m_nSelStart > 0 )
	{
		int nWidth = sm_UiRender->DrawTextN(m_spFont, tempOffset, m_wstrBuffer, m_nSelStart, NULL, mFontColor);
		int StrWidth = m_spFont->GetStrNWidth(m_wstrBuffer, m_nSelStart);
		tempOffset.x += StrWidth;
	}

	//渲染高亮字符
	if ( m_nSelEnd > 0 )
	{
			const WCHAR* HLText = m_wstrBuffer.GetBuffer() + m_nSelStart;
			UINT HLLen = m_nSelEnd-m_nSelStart;

			INT StrWidth = m_spFont->GetStrNWidth(HLText, HLLen);
			
			// 先渲染背景矩形
			URect HLRect;
			HLRect.left = tempOffset.x;
			HLRect.top = drawRect.top;
			HLRect.right = HLRect.left + StrWidth;
			HLRect.bottom = drawRect.bottom -1;
			sm_UiRender->DrawRectFill(HLRect, m_Style->m_FontColorSEL);
			sm_UiRender->DrawTextN(m_spFont, tempOffset, HLText, HLLen, NULL, m_Style->m_FontColorHL );
			tempOffset.x += StrWidth;
	}

	////draw the portion after the highlite
	if(m_nSelEnd < m_wstrBuffer.GetLength())
	{
		const WCHAR* Tail = m_wstrBuffer.GetBuffer() + m_nSelEnd;
		UINT ToDraw = m_wstrBuffer.GetLength() - m_nSelEnd;

		//	GFX->getDrawUtil()->setBitmapModulation( fontColor );
		sm_UiRender->DrawTextN(m_spFont, tempOffset,Tail, ToDraw, /*m_Style->m_FontColors*/NULL, mFontColor);
	}

	//draw the cursor
	if ( isFocused && mCursorOn )
	{
		sm_UiRender->DrawRectFill( URect(cursorStart, UPoint(3, cursorEnd.y - cursorStart.y)), m_Style->mCursorColor );
		//sm_UiRender->DrawLine( cursorStart, cursorEnd, m_Style->mCursorColor );
	}
}


void UEdit::playDeniedSound()
{
	//if ( mDeniedSound )
	//	SFX->playOnce( mDeniedSound );
}
BOOL UEdit::OnEscape()
{
	if (m_wstrBuffer.Empty())
	{
		return FALSE;
	}
	Clear();
	PlaceCaretAt(0);
	return TRUE;
}