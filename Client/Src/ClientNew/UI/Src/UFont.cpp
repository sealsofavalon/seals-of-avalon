#include "StdAfx.h"
#include "UFont.h"

//#include "zlib/zlib.h"
 
#include "UMemory.h"
// hack
#include "D3DUIRender.h"
#include "UControl.h"


//
//ResourceInstance* constructNewFont(Stream& stream, ResourceObject *)
//{
//   UFont *ret = new UFont;
//
//   if(!ret->read(stream))
//   {
//      SAFE_DELETE(ret);
//   }
//
//   if(ret)
//   {
//      ret->m_FontInterface = createPlatformFont(ret->m_strFaceName, ret->m_nSize, ret->m_uCharset);
//   }
//   
//   return ret;
//}

/// Used for repacking in UFont::importStrip.
//struct GlyphMap
//{
//   UINT charId;
//   GBitmap *bitmap;
//};
//
//static int __cdecl GlyphMapCompare(const void *a, const void *b)
//{
//   int ha = ((GlyphMap *) a)->bitmap->height;
//   int hb = ((GlyphMap *) b)->bitmap->height;
//
//   return hb - ha;
//}


void UFont::getFontCacheFilename(const char *faceName, UINT size, UINT buffLen, char *outBuff)
{
	// LSprintf(outBuff, buffLen, "%s/%s %d (%s).uft", Con::getVariable("$GUI::fontCacheDirectory"), faceName, size, getCharSetName(0));
}


UFont* UFont::CreateFont(const char *faceName, UINT size, UINT charset, BOOL bTureTypeInterface)
{
	UFontInterface *platFont =  NULL;
	if (!bTureTypeInterface)
	{
		platFont = new UWin32FontInterface;
		if (platFont == NULL)
		{
			return NULL;
		}
		if (!platFont->Create(faceName, size, charset))
		{
			delete platFont;
			return NULL;
		}
	}else
	{

	}
	if (platFont == NULL)
	{
		return CreateFont("����", size, charset, bTureTypeInterface);
	}

	UFont *resFont = new UFont;
	resFont->m_FontInterface = platFont;
	resFont->CreateFontSurface();
	//resFont->m_strFileName = buf;
	resFont->m_strFaceName = faceName;
	resFont->m_nSize = size;
	resFont->m_uCharset = charset;

	resFont->mHeight   = platFont->GetHeight();
	resFont->mBaseline = platFont->GetBaseLine();
	resFont->mAscent   = platFont->GetBaseLine();
	resFont->mDescent  = platFont->GetHeight() - platFont->GetBaseLine();


	return resFont;
}

//-------------------------------------------------------------------------
int UFont::ms_iTextureFormat = D3DFMT_A8;
UFont::UFont()
{
	memset(m_RemapTable, -1, sizeof(m_RemapTable));
	m_nCurX = m_nCurY = m_nCurSheet = -1;
	m_FontInterface = NULL;
	m_nSize = 0;
	m_uCharset = 0;
	m_bDirty = FALSE;
	m_CharList.reserve( 1000 );
	//  mMutex = Mutex::createMutex();
}

UFont::~UFont()
{
	if(m_bDirty)
	{
		/*  FileStream stream;
		if(ResourceManager->openFileForWrite(stream, m_strFileName)) 
		{
		write(stream);
		stream.close();
		}*/
	}

	for(int i=0; i < (int)m_FontSurface.size(); i++)
	{
		IDirect3DTexture9* pTex = (IDirect3DTexture9*)m_FontSurface[i];
		pTex->Release();
	}
	if (m_FontInterface)
	{
		delete m_FontInterface; m_FontInterface = NULL;
	}
	// SAFE_DELETE(m_FontInterface);

	//  Mutex::destroyMutex(mMutex);
}

BOOL UFont::CacheCharDesc(WCHAR ch)
{
	if(m_RemapTable[ch] != -1)
		return TRUE; 

	if(m_FontInterface && m_FontInterface->IsValidChar(ch))
	{
		// Mutex::lockMutex(mMutex); // the CharInfo returned by m_FontInterface is static data, must protect from changes.
		static CharInfo ci; 
		static UFontGlyphData Glyph;
		BOOL bGlyphData = m_FontInterface->GetCharDesc(ch, ci, Glyph);

		if(bGlyphData)
			AddGlyphData(ci, Glyph);

		m_CharList.push_back(ci);
		m_RemapTable[ch] = m_CharList.size() - 1;

		m_bDirty = TRUE;

		// Mutex::unlockMutex(mMutex);
		return TRUE;
	}

	return FALSE;
}

void UFont::AddGlyphData(CharInfo &charInfo, UFontGlyphData& glyph)
{
	UINT nextCurX = UINT(m_nCurX + charInfo.width  + 7) & ~0x3;
	UINT nextCurY = UINT(m_nCurY + m_FontInterface->GetHeight() + 7) & ~0x3;

	// These are here for postmortem debugging.
	bool routeA = FALSE, routeB = FALSE;

	if(m_nCurSheet == -1 || nextCurY >= TextureSheetSize)
	{
		routeA = TRUE;
		CreateFontSurface();

		// Recalc our nexts.
		nextCurX = UINT(m_nCurX + charInfo.width + 7) & ~0x3;
		nextCurY = UINT(m_nCurY + m_FontInterface->GetHeight() + 7) & ~0x3;
	}

	if( nextCurX >= TextureSheetSize)
	{
		routeB = TRUE;
		m_nCurX = 0;
		m_nCurY = nextCurY;

		// Recalc our nexts.
		nextCurX = UINT(m_nCurX + charInfo.width + 7) & ~0x3;
		nextCurY = UINT(m_nCurY + m_FontInterface->GetHeight() + 7) & ~0x3;
	}

	// Check the Y once more - sometimes we advance to a new row and run off
	// the end.
	if(nextCurY >= TextureSheetSize)
	{
		routeA = TRUE;
		CreateFontSurface();

		// Recalc our nexts.
		nextCurX = UINT(m_nCurX + charInfo.width + 7) & ~0x3;
		nextCurY = UINT(m_nCurY + m_FontInterface->GetHeight() + 7) & ~0x3;
	}

	charInfo.bitmapIndex = m_nCurSheet;
	charInfo.xOffset = m_nCurX;
	charInfo.yOffset = m_nCurY;

	m_nCurX = nextCurX;

	int x, y;

	// hack way.
	IDirect3DTexture9* pSurface = (IDirect3DTexture9*)m_FontSurface[m_nCurSheet];
	assert(pSurface);

	D3DLOCKED_RECT LockedRect;
	RECT rcToLock;
	rcToLock.left = charInfo.xOffset;
	rcToLock.top = charInfo.yOffset;
	rcToLock.right = charInfo.xOffset + charInfo.width;
	rcToLock.bottom = charInfo.yOffset + charInfo.height;
	if (FAILED(pSurface->LockRect(0, &LockedRect, &rcToLock, 0)))
	{
		return;
	}
	BYTE* Dest = (BYTE*)LockedRect.pBits;
	for(y = 0;y < charInfo.height;y++)
	{
		for(x = 0;x < charInfo.width;x++)
		{
			if(ms_iTextureFormat == D3DFMT_A8)
			{
				Dest[x + y*LockedRect.Pitch] = glyph.pPixels[y][x];
			}
			else
				Dest[x*4 + y*LockedRect.Pitch + 3] = glyph.pPixels[y][x];
		}
	}

	pSurface->UnlockRect(0);
}

void UFont::CreateFontSurface()
{
	/* 
	hack way to create the font surface texture  P.K. 
	*/
	IDirect3DTexture9* pTexture;

	if(ms_iTextureFormat == D3DFMT_A8)
	{
		if(FAILED(g_pRenderInterface->CreateTexture(TextureSheetSize, TextureSheetSize, 1, 0,
			D3DFMT_A8, D3DPOOL_MANAGED, &pTexture, NULL)))
		{
			UTRACE("UFont::CreateFontSurface D3DFMT_A8 failed\n");
			ms_iTextureFormat = D3DFMT_A8R8G8B8;
		}
	}

	if(ms_iTextureFormat == D3DFMT_A8R8G8B8)
	{
		if(FAILED(g_pRenderInterface->CreateTexture(TextureSheetSize, TextureSheetSize, 1, 0,
		   D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &pTexture, NULL)))
		{
			UTRACE("UFont::CreateFontSurface D3DFMT_A8R8G8B8 failed\n");
			return;
		}
	}

	// clear the texture.
	//D3DLOCKED_RECT LockRect;
	//if(FAILED(pTexture->LockRect(0, &LockRect, NULL, D3DLOCK_DISCARD)))
	//{
	//	return;
	//}
	//memset(LockRect.pBits, 0, LockRect.Pitch*TextureSheetSize);
	//pTexture->UnlockRect(0);
	
	m_FontSurface.push_back(pTexture);
	m_nCurX = 0;
	m_nCurY = 0;
	m_nCurSheet = m_FontSurface.size() - 1;
}

//-----------------------------------------------------------------------------

const CharInfo &UFont::GetCharDesc(WCHAR in_charIndex)
{
	assert(in_charIndex);
	if(m_RemapTable[in_charIndex] == -1)
		CacheCharDesc(in_charIndex);
	return m_CharList[m_RemapTable[in_charIndex]];
}

const CharInfo &UFont::getDefaultCharInfo()
{
	static CharInfo c;
	// c is initialized by the CharInfo default constructor.
	return c;
}

UINT UFont::GetStrWidth(const CHAR* in_pString)
{
	assert(in_pString != NULL);
	if (in_pString == NULL || *in_pString == '\0')
		return 0;

	return GetStrNWidth(in_pString, strlen(in_pString));
}

UINT UFont::GetStrWidthPrecise(const CHAR* in_pString)
{
	assert(in_pString != NULL);
	if (in_pString == NULL)
		return 0;
	return GetStrNWidthPrecise(in_pString, strlen(in_pString));
}

UINT UFont::GetStrNWidth(const CHAR *str, UINT n)
{
	// TODO . Cache the covert buffer.
	// WCHAR wBuf[]
	// FrameTemp<WCHAR> str16(n + 1);
	// convertUTF8toUTF16(str, str16, n + 1);
	// return GetStrNWidth(str16, strlen(str16));
	return 0;
}

UINT UFont::GetStrNWidth(const WCHAR *str, UINT n)
{
	assert(str != NULL);
	if (str == NULL || str[0] == 0 || n == 0)   
		return 0;

	UINT totWidth = 0;
	WCHAR curChar;
	UINT charCount;

	for(charCount = 0; charCount < n; charCount++)
	{
		curChar = str[charCount];
		if(curChar == NULL)
			break;

		if(IsValidChar(curChar))
		{
			const CharInfo& rChar = GetCharDesc(curChar);
			totWidth += rChar.xIncrement;
		}
		else if (curChar == L'\t')
		{
			const CharInfo& rChar = GetCharDesc(L' ');
			totWidth += rChar.xIncrement * TabWidthInSpaces;
		}
	}

	return(totWidth);
}

UINT UFont::GetStrNWidthPrecise(const CHAR *str, UINT n)
{
	// FrameTemp<WCHAR> str16(n + 1);
	// convertUTF8toUTF16(str, str16, n);
	//  return GetStrNWidthPrecise(str16, n);
	return 0;
}

UINT UFont::GetStrNWidthPrecise(const WCHAR *str, UINT n)
{
	assert(str != NULL);

	if (str == NULL || str[0] == NULL || n == 0)   
		return(0);

	UINT totWidth = 0;
	WCHAR curChar;
	UINT charCount = 0;

	for(charCount = 0; charCount < n; charCount++)
	{
		curChar = str[charCount];
		if(curChar == NULL)
			break;

		if(IsValidChar(curChar))
		{
			const CharInfo& rChar = GetCharDesc(curChar);
			totWidth += rChar.xIncrement;
		}
		else if (curChar == L'\t')
		{
			const CharInfo& rChar = GetCharDesc(L' ');
			totWidth += rChar.xIncrement * TabWidthInSpaces;
		}
	}

	WCHAR endChar = str[min(charCount,n-1)];

	if (IsValidChar(endChar))
	{
		const CharInfo& rChar = GetCharDesc(endChar);
		if (rChar.width > rChar.xIncrement)
			totWidth += (rChar.width - rChar.xIncrement);
	}

	return(totWidth);
}

UINT UFont::GetBreakPos(const WCHAR *str16, UINT slen, UINT width, BOOL breakOnWhitespace)
{
	// Some early out cases.
	if(slen==0)
		return 0;

	UINT ret = 0;
	UINT lastws = 0;
	WCHAR c;
	UINT charCount = 0;

	for( charCount=0; charCount < slen; charCount++)
	{
		c = str16[charCount];
		if(c == NULL)
			break;

		if(c == L'\t')
			c = L' ';

		if(!IsValidChar(c))
		{
			ret++;
			continue;
		}

		if(c == L' ')
			lastws = ret+1;

		const CharInfo& rChar = GetCharDesc(c);
		if(rChar.width > width || rChar.xIncrement > width)
		{
			if(lastws && breakOnWhitespace)
				return lastws;
			return ret;
		}

		width -= rChar.xIncrement;

		ret++;
	}
	return ret;
}
