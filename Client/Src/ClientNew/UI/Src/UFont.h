#ifndef __UFONT_H__
#define __UFONT_H__

#include <vector>

#pragma pack(push)
#pragma pack(1)
struct CharInfo
{
	BYTE bitmapIndex;     ///< @note -1 indicates character is NOT to be
	///        rendered, i.e., \n, \r, etc.

	BYTE  width;          ///< width of character (pixels)
	BYTE  height;         ///< height of character (pixels)
	BYTE  xIncrement;
	WORD  xOffset;        ///< x offset into bitmap sheet
	WORD  yOffset;        ///< y offset into bitmap sheet
	CHAR  xOrigin;
	CHAR  yOrigin;
};
#pragma pack(pop)

// optimize for dynamic get character glyph outline data.
struct UFontGlyphData
{
	BYTE pPixels[256][256];	// MAX SIZE 64 KB.
	WORD Width;
	WORD Height;
};


class UFontInterface
{
public:
	virtual BOOL IsValidChar(WCHAR ch) const = 0;
	// virtual BOOL IsValidChar(const CHAR *str) const = 0;
	virtual UINT GetHeight() const = 0;
	virtual UINT GetBaseLine() const = 0;
	virtual BOOL GetCharDesc(WCHAR ch, CharInfo& cinfo, UFontGlyphData& GlyphData) const = 0;
	// virtual CharInfo& GetCharDesc(const CHAR *str) const = 0;
	// charset = 1 : DEFAULT_CHARSET
	virtual BOOL Create(const char *name, UINT size, UINT charset = 1) = 0;
	// static void enumeratePlatformFonts( Vector<StringTableEntry>& fonts, WCHAR* fontFamily = NULL );
};

class UWin32FontInterface : public UFontInterface
{
private:
	inline BYTE GetBit(BYTE* pSrc, int nBit) const
	{
		//1. get byte.
		//2. operator AND the remain bit.
		return (BYTE)(pSrc[nBit>>3])&(1 <<(7 -(nBit%8)));
		//return (BYTE)(pSrc[nBit>>3])>>(nBit%8)&1;
	}
	HFONT m_hFont;
	TEXTMETRIC mTextMetric;
	static HDC sm_hFontDC;
	static HBITMAP sm_hFontBMP;
	static int sm_nRefCnt;
	static void InitFontDC(void);
	static void ReleaseFontDC(void);
public:
	UWin32FontInterface();
	virtual ~UWin32FontInterface();

	// UFontInterface virtual methods
	virtual BOOL IsValidChar(WCHAR ch) const;
	// virtual BOOL IsValidChar(const CHAR *str) const;

	inline virtual UINT GetHeight() const
	{
		return mTextMetric.tmHeight;
	}

	inline virtual UINT GetBaseLine() const
	{
		return mTextMetric.tmAscent;
	}

	virtual BOOL GetCharDesc(WCHAR ch, CharInfo& cinfo, UFontGlyphData& GlyphData) const;
	//virtual CharInfo &GetCharDesc(const CHAR *str) const;

	virtual BOOL Create(const char *name, UINT size, UINT charset = 0);
};

typedef void* UFontSurface;

class UFont
{
	// friend UFont* constructNewFont(Stream& stream, ResourceObject *);

public:
	
	enum Constants 
	{
		BFT_VERSION = 1,
		TabWidthInSpaces = 3,
		TextureSheetSize = 256,
	};
	// Enumerations and structures available to derived classes
private:
	UFontInterface*	m_FontInterface;	// 系统字体接口.
	int m_nCurX;
	int m_nCurY;
	int m_nCurSheet;

	BOOL m_bDirty;
	std::string m_strFileName;
	std::string m_strFaceName;
	UINT m_nSize;
	UINT m_uCharset;

	UINT mHeight;
	UINT mBaseline;
	UINT mAscent;
	UINT mDescent;
	// TODO  可以使用unsigned short.   0xFFFF 表示无效字符.
	int m_RemapTable[65536];					// 字符映射列表
	std::vector<UFontSurface>	m_FontSurface;		// 字体纹理.
	std::vector<CharInfo>		m_CharList;		// 字符列表

	static int ms_iTextureFormat;

public:
	UFont();
	virtual ~UFont();
protected:
	BOOL CacheCharDesc(WCHAR ch);
	void AddGlyphData(CharInfo &charInfo, UFontGlyphData& glyph);
	void CreateFontSurface(void);
	void *mMutex;
public:
	static UFont* CreateFont(const char *faceName, UINT size, UINT charset = 1, BOOL bTureTypeInterface = FALSE);

	UFontSurface GetSurface(int index) const
	{
		return m_FontSurface[index];
	}

	const CharInfo& GetCharDesc(WCHAR in_charIndex);
	static const CharInfo& getDefaultCharInfo();

	UINT  GetCharHeight(WCHAR in_charIndex);
	UINT  GetCharWidth(WCHAR in_charIndex);
	UINT  GetCharXIncrement(WCHAR in_charIndex);

	BOOL IsValidChar(const WCHAR in_charIndex) const;

	UINT GetHeight() const   { return mHeight; }
	UINT GetBaseline() const { return mBaseline; }
	UINT GetAscent() const   { return mAscent; }
	UINT GetDescent() const  { return mDescent; }

	UINT GetBreakPos(const WCHAR *string, UINT strlen, UINT width, BOOL breakOnWhitespace);

	/// These are the preferred width functions.
	UINT GetStrNWidth(const WCHAR*, UINT n);
	UINT GetStrNWidthPrecise(const WCHAR*, UINT n);

	/// These CHAR versions of the width functions will be deprecated, please avoid them.
	UINT GetStrWidth(const CHAR*);   // Note: ignores c/r
	UINT GetStrNWidth(const CHAR*, UINT n);
	UINT GetStrWidthPrecise(const CHAR*);   // Note: ignores c/r
	UINT GetStrNWidthPrecise(const CHAR*, UINT n);

	BOOL HasPlatformFont() const
	{
		return m_FontInterface != 0;
	}

	/// Get the filename for a cached font.
	static void getFontCacheFilename(const char *faceName, UINT faceSize, UINT buffLen, char *outBuff);

	/// Get the face name of the font.
	const char* getFontFaceName() const { return m_strFaceName.c_str(); }
	void SetFontFileName(const char* pFileName) { m_strFileName = pFileName;}
	const char* GetFontFileName() const { return m_strFileName.c_str();}
};

inline UINT UFont::GetCharXIncrement(const WCHAR in_charIndex)
{
	const CharInfo& rChar = GetCharDesc(in_charIndex);
	return rChar.xIncrement;
}

inline UINT UFont::GetCharWidth(const WCHAR in_charIndex)
{
	const CharInfo& rChar = GetCharDesc(in_charIndex);
	return rChar.width;
}

inline UINT UFont::GetCharHeight(const WCHAR in_charIndex)
{
	const CharInfo& rChar = GetCharDesc(in_charIndex);
	return rChar.height;
}

inline BOOL UFont::IsValidChar(const WCHAR in_charIndex) const
{
	if(m_RemapTable[in_charIndex] != -1)
		return TRUE;

	if(m_FontInterface)
		return m_FontInterface->IsValidChar(in_charIndex);

	return FALSE;
}


#endif 