#include "StdAfx.h"
#include "UGameExtCtrl.h"
#include "USystem.h"
#include "D3DUIRender.h"
UIMP_CLASS(UDynamicIconButton, UBitmapButton);
void UDynamicIconButton::StaticInit()
{
	UREG_PROPERTY("iconidx", UPT_INT, UFIELD_OFFSET(UDynamicIconButton, m_nIconIndex));
}

UDynamicIconButton::UDynamicIconButton()
{
	m_bEnableFreeze = TRUE;
	m_nIconIndex = 0;
	m_bTicking = FALSE;
	m_nMaskIndex = 0;
	m_spMaskTexture = NULL;
	m_fTimer = 0.0f;
	m_FreezeTime = 4.0f;
}

UDynamicIconButton::~UDynamicIconButton()
{

}

void UDynamicIconButton::StartTick(float tickTime)
{
	m_bEnableFreeze = TRUE;
	m_FreezeTime = tickTime;
	m_bTicking = TRUE;
	m_fTimer = 0.0f;
}

void UDynamicIconButton::EndTick()
{
	m_bTicking = FALSE;
}

void UDynamicIconButton::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	UBitmapButton::OnMouseDown(pt, nClickCnt, uFlags);
}
BOOL UDynamicIconButton::OnCreate()
{
	if (!UBitmapButton::OnCreate())
	{
		return FALSE;
	}
	m_spMaskTexture = sm_UiRender->LoadTexture("data\\ui\\skin\\TimeBtnMask.dds");
	if (m_spMaskTexture == NULL)
	{
		return FALSE;
	}

	m_spGlowTexture = sm_UiRender->LoadTexture("DATA\\UI\\SKIN\\TimeBtnGlow.dds");
	if (m_spGlowTexture == NULL)
	{
		return FALSE;
	}
	return TRUE;
}

void UDynamicIconButton::OnDestroy()
{
	m_spGlowTexture = NULL;
	m_spMaskTexture = NULL;
	UBitmapButton::OnDestroy();
}

void UDynamicIconButton::OnTimer(float fDeltaTime)
{
	if (m_bTicking)
	{
		m_fTimer += (fDeltaTime*0.5);
		if (m_fTimer > m_FreezeTime)
		{
			m_fTimer = 0.0f;
			m_bTicking = FALSE;
		}

		m_nMaskIndex = m_fTimer*60/m_FreezeTime;
	}

}


void UDynamicIconButton::DrawPushButton(const UPoint& offset, const URect& ClipRect)
{
	//if (m_spBtnSkin == NULL)
	//{
	//	UButton::DrawPushButton(offset, ClipRect);
	//	return;
	//}

	URect rect(offset, m_Size);
	if (m_spBtnSkin)
	{
		URect drawSkinRect = rect;
		drawSkinRect.InflateRect(URect(2,2,2,2));
		sm_UiRender->DrawSkin(m_spBtnSkin, m_nIconIndex, drawSkinRect);
	}

	// draw a glow overlay
	if (m_Active && m_spGlowTexture)
	{
		if (m_bPressed || m_bCheck)
		{
			URect uvrect;
			uvrect.left = 0;
			uvrect.top = 0;
			uvrect.right = 64;
			uvrect.bottom = 64;
			sm_UiRender->DrawImage(m_spGlowTexture, rect, uvrect, LCOLOR_WHITE, 1);
		}else if (m_bMouseOver)
		{
			URect uvrect;
			uvrect.left = 64;
			uvrect.top = 0;
			uvrect.right = 128;
			uvrect.bottom = 64;
			sm_UiRender->DrawImage(m_spGlowTexture, rect, uvrect, LCOLOR_WHITE, 1);
		} 
	}

	if (m_bEnableFreeze  && m_bTicking)
	{
		URect uvrect(0,0,64,64);
		int col = m_nMaskIndex%8;
		int row = m_nMaskIndex/8;
		uvrect.Offset(col*64, row*64);
		sm_UiRender->DrawImage(m_spMaskTexture, rect, uvrect, LCOLOR_WHITE, 2);
	}

}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UItemContainer, UGridCtrl)

void UItemContainer::StaticInit()
{

}

UItemContainer::UItemContainer()
{
	m_GridSize.Set(50,50);
	m_GridCount.Set(5,4);
	m_HeadSize.Set(0,0);
	m_DragItem.Set(-1, -1);
	m_pItems = NULL;
	m_bDragBegin = FALSE;
}

UItemContainer::~UItemContainer()
{
}

void UItemContainer::SetGridCount(int nRow, int nCol)
{
	if (nRow == m_GridCount.y  && nCol == m_GridCount.x)
	{
		return;
	}

	assert((nRow * nCol ) > 0);

	if (m_bCreated)
	{
		ITEM* pItems = new ITEM[nRow * nCol];
		assert(pItems);
		if (m_pItems)
		{
			// 拷贝数据
			/*for ( int y = 0; y < m_GridCount.y; y++)
			{
				for (int x = 0; x < m_GridCount.x; x++)
				{

				}
			}*/
			delete [] m_pItems;
		}
		m_pItems = pItems;
	}
		m_GridCount.x = nCol;
		m_GridCount.y = nRow;
	
}

BOOL UItemContainer::HasItem(int nRow, int nCol) const
{
	if (m_pItems == NULL)
	{
		return FALSE;
	}
	if(nRow >= m_GridCount.y || nCol >= m_GridCount.x)
	{
		return FALSE;
	}
	return (m_pItems[nRow*m_GridCount.x + nCol].nIconIndex != -1);
}

void UItemContainer::SwapItem(UItemContainer::ITEM* pItem1, UItemContainer::ITEM* pItem2)
{
	assert(pItem1 != pItem2);
	ITEM Temp;
	Temp.nIconIndex = pItem1->nIconIndex;
	Temp.nItemNum = pItem1->nItemNum;
	Temp.spSkin = pItem1->spSkin;
	Temp.UserData = pItem1->UserData;
	pItem1->nIconIndex = pItem2->nIconIndex;
	pItem1->nItemNum = pItem2->nItemNum;
	pItem1->spSkin = pItem2->spSkin;
	pItem1->UserData = pItem2->UserData;
	pItem2->nIconIndex =Temp.nIconIndex;
	pItem2->nItemNum = Temp.nItemNum;
	pItem2->spSkin = Temp.spSkin;
	pItem2->UserData = Temp.UserData;
}

BOOL UItemContainer::AddItem(const char* pszSkinName,int nIconIndex, int nRow, int nCol, void* UserData ,int num)
{
	assert(pszSkinName && nIconIndex >= 0);
	if (HasItem(nRow, nCol))
	{
		UTRACE("slot is not empty.");
		return FALSE;
	}
	USkinPtr spSkin = sm_System->LoadSkin(pszSkinName);
	if (spSkin == NULL)
	{
		UTRACE("invalid icon skin.");
		return FALSE;
	}
	return AddItem(spSkin, nIconIndex, nRow, nCol, UserData,num);
}

BOOL UItemContainer::AddItem(USkin* pSkin,int nIconIndex, int nRow, int nCol, void* UserData, int num)
{
	if (pSkin == NULL || nIconIndex < 0)
	{
		return FALSE;
	}
	if (num < 0)
	{
		return FALSE;
	}
	if (HasItem(nRow, nCol))
	{
		return FALSE;
	}
	if (nRow >= m_GridCount.y || nCol >= m_GridCount.x)
	{
		return FALSE;
	}
	if (pSkin->GetItemNum() <= nIconIndex)
	{
		return FALSE;
	}
	if (m_pItems == NULL)
	{
		m_pItems = new ITEM[m_GridCount.x * m_GridCount.y];
		assert(m_pItems);
	}
	ITEM* pItem = &m_pItems[nRow * m_GridCount.x + nCol];
	pItem->nIconIndex = nIconIndex;
	pItem->spSkin = pSkin;
	pItem->nItemNum = num;
	pItem->UserData = UserData;
	return TRUE;
}

void UItemContainer::DeleteItem(int nRow, int nCol)
{
}

void* UItemContainer::GetItemData(int nRow, int nCol) const
{
	return m_pItems[m_GridCount.x * nRow + nCol].UserData;
}

void UItemContainer::SetItemData(int nRow, int nCol, void* UserData)
{
}

BOOL UItemContainer::OnCreate()
{
	if (!UGridCtrl::OnCreate())
	{
		return FALSE;
	}
	if (m_GridCount.x <= 0 || m_GridCount.y <= 0)
	{
		return FALSE;
	}
	m_pItems = new ITEM[m_GridCount.x * m_GridCount.y];
	if (m_pItems == NULL)
	{
		return FALSE;
	}
	m_spItemMask = sm_UiRender->LoadTexture("DATA\\UI\\SKIN\\ItemMask.dds");
	return TRUE;
}

void UItemContainer::OnDestroy()
{
	if (m_pItems)
	{
		delete [] m_pItems;
	}
	m_spItemMask = NULL;
	UGridCtrl::OnDestroy();
}

void UItemContainer::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	UGridCtrl::OnMouseDown(point, nRepCnt, uFlags);

	const UPoint& SelGrid = GetSelGrid();
	if (HasItem(SelGrid.y, SelGrid.x))
	{
		m_bDragBegin = TRUE;

		CaptureControl();
	}
}

void UItemContainer::OnMouseUp(const UPoint& position, UINT flags)
{
	m_bDragBegin = FALSE;
	ReleaseCapture();
}
void UItemContainer::OnMouseMove(const UPoint& pt, UINT uFlags)
{
	UGridCtrl::OnMouseMove(pt, uFlags);

	if (m_bDragBegin)
	{
		ITEM* pItem = &m_pItems[m_SelGrid.y * m_GridCount.x + m_SelGrid.x];
		
		DragData* pDragData = new DragData;
		pDragData->ItemUserData = pItem;
		BeginDragDrop(this, pItem->spSkin, pItem->nIconIndex, pDragData, DragData::DATA_FLAG);
		m_bDragBegin = FALSE;
	}

	OnShowTipsInfo();
}

void UItemContainer::OnMouseDragged(const UPoint& pt, UINT uFlags)
{
	UGridCtrl::OnMouseDragged(pt, uFlags);
}

void UItemContainer::OnMouseEnter(const UPoint& pt, UINT uFlags)
{
	UGridCtrl::OnMouseEnter(pt, uFlags);
	if (uFlags & LKM_DRAGDROP)
	{
		UTRACE("DragDrop Enter");
	}

}

void UItemContainer::OnMouseLeave(const UPoint& pt, UINT uFlags)
{
	UGridCtrl::OnMouseLeave(pt, uFlags);
	if (uFlags & LKM_DRAGDROP)
	{
		UTRACE("DragDrop Leave");
	}
}

void UItemContainer::OnDragDropEnd(UControl* pDragTo, void* pDragData, UINT nDataFlag)
{
	assert(nDataFlag == DragData::DATA_FLAG);
	if (nDataFlag == DragData::DATA_FLAG)
	{
		DragData* pItemDragData = (DragData*)pDragData;
		delete pItemDragData;
	}
	m_DragItem.Set(-1, -1);
}

BOOL UItemContainer::OnMouseUpDragDrop(UControl* pDragFrom,const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	if (nDataFlag != DragData::DATA_FLAG)
	{
		return FALSE;
	}

	const DragData* pDragItemData = (const DragData*)pDragData;
	if (pDragFrom == this)
	{
		UPoint DropGrid = GetGrid(point);
		if (DropGrid.x == -1 || DropGrid.y == -1)
		{
			return FALSE;
		}
		
		ITEM* pSrcItem =  (ITEM*)pDragItemData->ItemUserData;
		ITEM* pDestItem = &m_pItems[m_GridCount.x * DropGrid.y + DropGrid.x];
		if (pSrcItem == pDestItem)
		{
			return FALSE;
		}
		SwapItem(pSrcItem, pDestItem);
		SetSel(DropGrid.y, DropGrid.x);
		return TRUE;
	}else 
	{
		UItemContainer* pSrcCtrl = UDynamicCast(UItemContainer, pDragFrom);
		if (pSrcCtrl == NULL)
		{
			// 这里简单返回, 实际中, 只要能重现数据, 那么所有可知的控件都可以.
			// 而不用非得是UItemContainer类型.
			return FALSE;
		}
		UPoint DropGrid = GetGrid(point);
		if (DropGrid.x == -1 || DropGrid.y == -1)
		{
			return FALSE;
		}
	
		ITEM* pSrcItem =  (ITEM*)pDragItemData->ItemUserData;
		ITEM* pDestItem = &m_pItems[m_GridCount.x * DropGrid.y + DropGrid.x];
		if (pSrcItem == pDestItem)
		{
			// always not equal.
			return FALSE;
		}
		SwapItem(pSrcItem, pDestItem);
		SetSel(DropGrid.y, DropGrid.x);
	}
	return TRUE;
}

void UItemContainer::OnDragDropAccept(UControl* pAcceptCtrl,const UPoint& point, const void* pDragData, UINT nDataFlag)
{
	UControl::OnDragDropAccept(pAcceptCtrl, point, pDragData, nDataFlag);
	if (pAcceptCtrl != this)
	{
		ClearSel();
	}
}

void UItemContainer::OnDragDropReject(UControl* pRejectCtrl, const void* pDragData, UINT nDataFlag)
{
	EndDragDrop();
	ReleaseCapture();
}

void UItemContainer::DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver)
{
	if (m_pItems == NULL)
	{
		return;
	}
	
	if (cell.x != -1 && cell.y != -1)
	{
		ITEM* pItem = &m_pItems[cell.y * m_GridCount.x + cell.x];
		if (pItem->nIconIndex != -1)
		{
			// Hardcode . test.
			URect IconRect;
			IconRect.left = offset.x + 5;
			IconRect.top = offset.y + 5;
			IconRect.SetSize(m_GridSize.x - 10, m_GridSize.y - 10);
			sm_UiRender->DrawSkin(pItem->spSkin, pItem->nIconIndex, IconRect);
		}

	}
	if (m_spItemMask)
	{
		if (mouseOver)
		{
			URect rect(offset, m_GridSize);
			
			URect uvrect;
			uvrect.left = 0;
			uvrect.top = 0;
			uvrect.right = 42;
			uvrect.bottom = 43;
			sm_UiRender->DrawImage(m_spItemMask, rect, uvrect, LCOLOR_WHITE, 1);
		}else if (selected)
		{
			URect rect(offset, m_GridSize);
			
			URect uvrect;
			uvrect.left = 42;
			uvrect.top = 0;
			uvrect.right = 84;
			uvrect.bottom = 43;
			sm_UiRender->DrawImage(m_spItemMask, rect, uvrect, LCOLOR_WHITE, 1);
		}
	}
}

void UItemContainer::OnShowTipsInfo()
{

}