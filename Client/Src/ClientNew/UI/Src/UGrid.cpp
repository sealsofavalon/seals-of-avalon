#include "StdAfx.h"
#include "UGrid.h"
#include "UScrollBar.h"

UIMP_CLASS(UGridCtrl,UControl);
void UGridCtrl::StaticInit()
{
	UREG_PROPERTY("vertsb", UPT_BOOL, UFIELD_OFFSET(UGridCtrl,m_bHasScrool));
}

UGridCtrl::UGridCtrl(void)
{
	m_Active = TRUE;
	m_GridSize.Set(80,30);
	m_GridCount.Set(5,10);
	m_SelGrid.Set(-1,-1);
	m_HoverGrid.Set(-1,-1);
	m_HeadSize.Set(0, 0);
	m_bHasScrool = FALSE;
}

UGridCtrl::~UGridCtrl(void)
{
}

BOOL UGridCtrl::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	return TRUE;
}

void UGridCtrl::OnDestroy()
{
	UControl::OnDestroy();
}

void UGridCtrl::SetGridSize(const UPoint& GridSize)
{
	m_GridSize = GridSize;
}

//重新设定m_GridCount
void UGridCtrl::SetGridCount(int nRow, int nCol)
{
	if (nRow < 1)
	{
		nRow = 1;
	}
	if (nCol < 1)
	{
		nCol = 1;
	}
	m_GridCount.x = nCol;
	m_GridCount.y = nRow;
	UPoint newExtent(m_GridCount.x * m_GridSize.x + m_HeadSize.x,m_GridCount.y * m_GridSize.y + m_HeadSize.y);
	SetSize(newExtent);
}

BOOL UGridCtrl::SetSel(int row, int col)
{
	if (col < 0 || col > m_GridCount.x || row < 0 || row > m_GridCount.y)
	{
		m_SelGrid.x = -1;
		m_SelGrid.y = -1;
		return FALSE;
	}
	m_SelGrid.x = col;
	m_SelGrid.y = row;
	ScrollToSel();
	OnSelChange(row, col);
	return TRUE;
}

void UGridCtrl::OnSelChange(int row, int col)
{
	DispatchNotify(UGD_SELCHANGE);
}

void UGridCtrl::OnCellHighlighted(const UPoint& cell)
{

}

const UPoint& UGridCtrl::GetSelGrid() const
{
	return m_SelGrid;
}

UPoint UGridCtrl::GetGrid(const UPoint& position) const
{
	UPoint pta = ScreenToWindow(position);
	pta.x -= m_HeadSize.x;
	pta.y -= m_HeadSize.y;

	UPoint cell(  (pta.x < 0 ? -1 : pta.x / m_GridSize.x), (pta.y < 0 ? -1 : pta.y / m_GridSize.y));
	if (cell.x >= 0 && cell.y >=0  && cell.x <= m_GridCount.x && cell.y <= m_GridCount.y)
	{
		return cell;
	}else
	{
		return UPoint(-1,-1);
	}
}

void UGridCtrl::ScrollToSel()
{
	ScrollToGrid(m_SelGrid);
}

void UGridCtrl::ScrollToGrid(const UPoint& cell)
{
	//make sure we have a parent
	//make sure we have a valid cell selected
	UScrollBar *parent = UDynamicCast(UScrollBar, GetParent());
	if(!parent || cell.x < 0 || cell.y < 0)
		return;

	URect cellBounds(cell.x * m_GridSize.x, cell.y * m_GridSize.y, cell.x * m_GridSize.x + m_GridSize.x,  cell.y * m_GridSize.y + m_GridSize.y);
	parent->scrollRectVisible(cellBounds);
}


void UGridCtrl::DrawColHeader(UPoint offset, UPoint parentOffset, UPoint headerDim)
{
	if (m_Style->mBorder)
	{
		URect cellR(offset.x + headerDim.x,parentOffset.y,GetWidth() - headerDim.x ,headerDim.y);
		sm_UiRender->DrawRectFill(cellR,m_Style->mBorderColor);
	}
}

void UGridCtrl::DrawRowHeader(UPoint offset, UPoint parentOffset, UPoint headerDim, UPoint cell)
{
	UColor color ;
	URect  cellR ;
	if (cell.x % 2)
	{
		color.Set(255,0,0,255);
	}
	else
	{
		color.Set(0,255,0,255);
	}
	cellR.SetPosition(parentOffset.x, offset.y);
	cellR.SetSize(headerDim.x,m_GridSize.y);
	sm_UiRender->DrawRectFill(cellR,color);
}

void UGridCtrl::DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver)
{
	UColor color(255 * (cell.x % 2), 255 * (cell.y % 2), 255 * ((cell.x + cell.y) % 2),255);
	if (selected)
	{
		color.Set(255, 0, 0,255);
	}else if (mouseOver)
	{
		color.Set(0, 0, 255,255);
	}

	URect cellR(offset,m_GridSize);
	sm_UiRender->DrawRectFill(cellR,color);
}
void UGridCtrl::OnRender(const UPoint& o, const URect &updateRect)
{
	UPoint offset( o );
	UControl::OnRender(offset,updateRect);

	UControl*  parent = GetParent();
	if (!parent)
	{
		return ;
	}

	int i , j ;
	URect headerCliper ;
	URect clipRect = updateRect ;
	UPoint parentOffset = parent->WindowToScreen(UPoint(0,0));
	
	// HACK 
	parentOffset = m_Position;

	if (m_HeadSize.y > 0)
	{
		headerCliper.SetPosition(parentOffset.x + m_HeadSize.x, parentOffset.y);
		headerCliper.SetSize(clipRect.GetSize().x, m_HeadSize.y);
		if (headerCliper.Intersect(clipRect))
		{
			sm_UiRender->SetClipRect(headerCliper);
			DrawColHeader(offset,parentOffset,m_HeadSize);
			clipRect.top += (m_HeadSize.y - 1);
		}
		offset.y +=m_HeadSize.y; 
	}

	if (m_HeadSize.x > 0)
	{
		int xStart = parentOffset.x + m_HeadSize.x;
		if (clipRect.left < xStart)
		{
			clipRect.left = xStart;
		}
		offset.x += m_HeadSize.x ;
	}

	URect  origClipRect = clipRect;
	// 计算可见的列区间
	int nMinCol = 0, nMaxCol = m_GridCount.x;
	for (i = 0; i < m_GridCount.x; i++)
	{
		if ((i + 1) * m_GridSize.x + offset.x < updateRect.left)
		{
			nMaxCol = i + 1;
			continue;
		}

		//break once past the last visible column
		if (i * m_GridSize.x + offset.x >= updateRect.right)
		{
			nMaxCol = i;
			break;
		}
	}

	int cellx, celly;
	for (j = 0; j < m_GridCount.y; j++)
	{
		//排除在可视范围之外的行
		if ((j+1)*m_GridSize.y + offset.y < updateRect.top)
		{
			continue;
		}

		if ( (j*m_GridSize.y + offset.y) >= updateRect.bottom)
		{
			break;
		}

		//Render header
		if (m_HeadSize.x > 0)
		{
			headerCliper.left = parentOffset.x;
			headerCliper.top = offset.y+j * m_GridSize.y;
			headerCliper.right = headerCliper.left + m_HeadSize.x;
			headerCliper.bottom = headerCliper.top + m_GridSize.y;

			sm_UiRender->SetClipRect(headerCliper);
			DrawRowHeader(UPoint(0,offset.y + j*m_GridSize.y),UPoint(parentOffset.x,offset.y + j*m_GridSize.y),m_HeadSize,UPoint(0,j));  
		}
		
		for (int col = nMinCol; col < nMaxCol; col++)
		{
			cellx = offset.x + col * m_GridSize.x;
			celly = offset.y + j * m_GridSize.y;
			
			INT Width = m_GridSize.x;
			INT Height = m_GridSize.y;
			m_GridSize.x = __min(Width , m_Size.x);
			m_GridSize.y = __min(Height , m_Size.y);

			//headerCliper.Set(cellx, celly, m_GridSize.x, m_GridSize.y);

			//	//set the clip rect
			//	sm_UiRender->SetClipRect(headerCliper);

				//render the cell
				DrawGridItem(UPoint(cellx, celly), UPoint(col, j),
					col == m_SelGrid.x && j == m_SelGrid.y,
					col == m_HoverGrid.x && j == m_HoverGrid.y);
			
		}
	}
}

void UGridCtrl::OnMouseDown(const UPoint& point,UINT nRepCnt, UINT uFlags)
{
	if (!m_Active || !m_bCreated || !m_Visible)
	{
		return ;
	}

	UPoint pta = ScreenToWindow(point);
	pta.x -= m_HeadSize.x;
	pta.y -= m_HeadSize.y;

	UPoint cell(  (pta.x < 0 ? -1 : pta.x / m_GridSize.x), (pta.y < 0 ? -1 : pta.y / m_GridSize.y));
	if (cell.x >= 0 && cell.y >=0  && cell.x < m_GridCount.x && cell.y < m_GridCount.y)
	{
		UPoint prevSelected = m_SelGrid;
		SetSel(cell.y, cell.x);
		//
		if (nRepCnt > 1 && prevSelected == m_SelGrid)
		{
			DispatchNotify(UGD_DBCLICK);
		}
	}
}

void UGridCtrl::OnMouseMove(const UPoint& pt, UINT uFlags)
{
	if (!m_Active || !m_bCreated || !m_Visible)
	{
		return ;
	}

	UControl::OnMouseMove(pt,uFlags);

	UPoint pta = ScreenToWindow(pt);
	pta.x -= m_HeadSize.x;
	pta.y -= m_HeadSize.y;

	UPoint cell(  (pta.x < 0 ? -1 : pta.x / m_GridSize.x), (pta.y < 0 ? -1 : pta.y / m_GridSize.y));
	if (cell.x >= 0 && cell.y >=0  && cell.x <= m_GridCount.x && cell.y <= m_GridCount.y)
	{
		UPoint prevMouseOver = m_HoverGrid;
		m_HoverGrid = cell;
		//
		OnCellHighlighted(m_HoverGrid);
	}else
	{
		m_HoverGrid.Set(-1,-1);
	}
}

void UGridCtrl::OnMouseDragged(const UPoint& pt, UINT uFlags)
{

	OnMouseMove(pt, uFlags);
}

void UGridCtrl::OnMouseEnter(const UPoint& pt, UINT uFlags)
{
	UPoint LocalPt = ScreenToWindow(pt);
	LocalPt.x -= m_HeadSize.x; LocalPt.y -= m_HeadSize.y;

	//get the cell
	UPoint cell((LocalPt.x < 0 ? -1 : LocalPt.x / m_GridSize.x), (LocalPt.y < 0 ? -1 : LocalPt.y / m_GridSize.y));
	if (cell.x >= 0 && cell.x < m_GridCount.x && cell.y >= 0 && cell.y < m_GridCount.y)
	{
		m_HoverGrid = cell;
		OnCellHighlighted(m_HoverGrid);
	}
}

void UGridCtrl::OnMouseLeave(const UPoint& pt, UINT uFlags)
{
	m_HoverGrid.Set(-1,-1);
	OnCellHighlighted(m_HoverGrid);
}

BOOL UGridCtrl::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	if ((! m_bCreated) || (!m_Active) || (! m_Visible)) 
		return TRUE;

	//get the parent
	int pageSize = 1;
	/*UControl *parent = getParent();
	if (parent && mCellSize.y > 0)
	{
		pageSize = getMax(1, (parent->getHeight() / mCellSize.y) - 1);
	}*/

	UPoint delta(0,0);
	switch (nKeyCode)
	{
	case LK_LEFT:
		delta.Set(-1, 0);
		break;
	case LK_RIGHT:
		delta.Set(1, 0);
		break;
	case LK_UP:
		delta.Set(0, -1);
		break;
	case LK_DOWN:
		delta.Set(0, 1);
		break;
	case LK_PGUP:
		delta.Set(0, -pageSize);
		break;
	case LK_PGDN:
		delta.Set(0, pageSize);
		break;
	case LK_HOME:
		SetSel(0,0);
		return  TRUE ;
	case LK_END:
		SetSel(0, m_GridCount.y - 1 );
		return TRUE;
	default:
		return UControl::OnKeyDown(nKeyCode, nRepCnt, nFlags);
	}

	if (m_GridCount.x < 1 || m_GridCount.y < 1)
		return TRUE;

	//select the first cell if no previous cell was selected
	if (m_SelGrid.x == -1 || m_SelGrid.y == -1)
	{
		SetSel(0,0);
		return TRUE;
	}

	//select the cell
	UPoint cell = m_SelGrid;
	cell.x = __max(0, __min(m_GridCount.x - 1, cell.x + delta.x));
	cell.y = __max(0, __min(m_GridCount.y - 1, cell.y + delta.y));
	SetSel(cell.y, cell.x);
	return TRUE ;
}

void UGridCtrl::ResizeControl(const UPoint &NewPos, const UPoint &newExtent)
{
	UControl::ResizeControl(NewPos, newExtent);
}