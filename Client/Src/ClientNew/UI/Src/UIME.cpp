#include "StdAfx.h"
#include "UBase.h"
#include "uime.h"
#include "USystem.h"
#include "UDesktop.h"

UIMEControl::UIMEControl()
{
	m_Active = TRUE;
	m_Visible = FALSE;
	m_Position.Set(0,0);
	m_fTime = 0.0f;
	m_bCaretOn = TRUE;
}

UIMEControl::~UIMEControl()
{

}

BOOL UIMEControl::OnCreate()
{
	m_Style = g_USystem->FindControlStyle("IME");
	if (m_Style == NULL)
	{
		UTRACE("can't find the IME style");
		m_Style = new UControlStyle;

		return FALSE;
	}

	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	m_spBackgroundSkin = g_USystem->LoadSkin("Core\\ImeBackground.skin");
	return TRUE;
}

void UIMEControl::OnDestroy()
{
	m_spBackgroundSkin = NULL;
	UControl::OnDestroy();
}

void UIMEControl::UpdateRect()
{
	assert(m_bCreated && m_Visible);
	assert(UIME::sm_bInited);
	WCHAR* pCompStr = UIME::sm_wszCompBuffer;
	const UIME::CCandList& CandList = UIME::sm_CandList;
	IUFont* pFont = m_Style->m_spFont;
	assert(pFont);

	int nMaxWidth = 0;
	INT nStrLen = 0;
	if (pCompStr && pCompStr[0] != 0)
	{
		nStrLen = wcslen(pCompStr);
		int nCompWidth = pFont->GetStrNWidth(pCompStr, nStrLen);
		nMaxWidth = __max(nMaxWidth, nCompWidth);
	}

	int nNumCand = CandList.dwCount;
	int nCandWidth;
	for (int i = 0; i < nNumCand; i++)
	{
		nStrLen = wcslen(CandList.awszCandidate[i]);
		nCandWidth = pFont->GetStrNWidth(CandList.awszCandidate[i], nStrLen);
		nMaxWidth = __max(nMaxWidth, nCandWidth);
	}

	int nFontHeight = pFont->GetHeight();
	int nMaxHeight = 3*COMP_PADDING;
	nMaxHeight += nFontHeight*(nNumCand + 1);	// 1 for compstr.
	if (nNumCand > 0)
	{
		nMaxHeight += (nNumCand -1)*CAND_PADDING;
	}

	nMaxWidth += (2*VERT_PADDING + 32);
	if (nMaxWidth < 80)
	{
		nMaxWidth = 80;
	}

	// Calc Position.
	UControl* Target = g_Desktop->GetFocus();
	
	if (Target)
	{
		UPoint NewPos = Target->WindowToScreen(UPoint(0,0));
		NewPos.y += Target->GetHeight();	// 放置在底部
		UEdit* pEdit = UDynamicCast(UEdit, Target);
		if (pEdit)
		{
			NewPos.x = pEdit->GetCareteXPos();
		}
		
		const UPoint& DesktopSize = g_Desktop->GetWindowSize();
		if ((NewPos.x + nMaxWidth) > DesktopSize.x)
		{
			NewPos.x = DesktopSize.x - nMaxWidth;
		}

		if((NewPos.y + nMaxHeight) > DesktopSize.y)
		{
			NewPos.y = Target->GetWindowPos().y - nMaxHeight;
		}
		
		ResizeControl(NewPos, UPoint(nMaxWidth, nMaxHeight));
	}else
	{
		SetSize(UPoint(nMaxWidth, nMaxHeight));
	}
}

void UIMEControl::OnTimer(float fDeltaTime)
{
	if (!IsVisible())
	{
		return; 
	}
	m_fTime += fDeltaTime;
	
		if (m_fTime > 0.3f)
		{
			m_fTime = 0.0f;
			m_bCaretOn = !m_bCaretOn;
		}
	
}

void UIMEControl::OnRender(const UPoint& offset, const URect &updateRect)
{
	if (UIME::sm_wszCompBuffer[0] == 0)
	{
		return;
	}

	// first , draw background
	if (m_spBackgroundSkin)
	{
		IUTexture* pTexture = m_spBackgroundSkin->GetTexture();
		assert(pTexture);

		URect ScreenRect = GetControlRect();
		int Bottom = ScreenRect.bottom;
		INT FontHeight = m_Style->m_spFont->GetHeight();
		// compstr:
		ScreenRect.bottom = ScreenRect.top + FontHeight + 2 * COMP_PADDING;
		const URect* pSkinRect = m_spBackgroundSkin->GetSkinItemRect(IME_COMP);
		sm_UiRender->DrawImage(pTexture, ScreenRect, *pSkinRect);

		// candidate list
		ScreenRect.top = ScreenRect.bottom;
		ScreenRect.bottom = Bottom ;
		pSkinRect = m_spBackgroundSkin->GetSkinItemRect(IME_CANDIDATE);
		sm_UiRender->DrawImage(pTexture, ScreenRect, *pSkinRect);
	}

	// Draw Text

	WCHAR* pCompStr = UIME::sm_wszCompBuffer;
	const UIME::CCandList& CandList = UIME::sm_CandList;
	IUFont* pFont = m_Style->m_spFont;
	int nFontHeight = pFont->GetHeight();

	const UColor& CompColor = m_Style->m_FontColorHL;
	const UColor& SelCandColor = m_Style->m_FontColorSEL;
	const UColor& CandColor = m_Style->m_FontColor;
	// comp str
	int nCompStrLen = wcslen(pCompStr);
	UPoint ptText = m_Position;
	ptText.x += VERT_PADDING;
	ptText.y += COMP_PADDING;
	sm_UiRender->DrawTextN(pFont, ptText, pCompStr, nCompStrLen, /*m_Style->m_FontColors*/NULL, CompColor);

	ptText.y += (COMP_PADDING + nFontHeight);
	int VertStep = nFontHeight + CAND_PADDING;
	WCHAR* pCandStr;
	for (int i =0; i < (int)CandList.dwCount; i++)
	{
		pCandStr = (WCHAR*)CandList.awszCandidate[i];
		if (i != CandList.dwSelection)
		{
			sm_UiRender->DrawTextN(pFont, ptText, pCandStr, wcslen(pCandStr), /*m_Style->m_FontColors*/NULL, CandColor);
		}else
		{
			sm_UiRender->DrawTextN(pFont, ptText, pCandStr, wcslen(pCandStr), /*m_Style->m_FontColors*/NULL, SelCandColor);

		}
		ptText.y += VertStep;
	}

	// Draw Caret.
	if (m_bCaretOn)
	{
		int nCaretCP = UIME::sm_nCompCaret;
		nCaretCP = LClamp(nCaretCP, 0, nCompStrLen);
		INT nCaretX;
		if (nCaretCP > 0)
		{
			nCaretX = pFont->GetStrNWidth(pCompStr, nCaretCP);
		}else
		{
			nCaretX = 0;
		}

		nCaretX += (VERT_PADDING + m_Position.x);
		UPoint ptCaret0(nCaretX, m_Position.y + COMP_PADDING);
		UPoint ptCaret1(nCaretX, m_Position.y + COMP_PADDING + nFontHeight);
		sm_UiRender->DrawLine(ptCaret0, ptCaret1, m_Style->mCursorColor);
	}
	
}

void UIMEControl::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
}

void UIMEControl::OnMouseUp(const UPoint& pt, UINT uFlags)
{
}

void UIMEControl::OnMouseDragged(const UPoint& position, UINT flags)
{

}

INPUTCONTEXT* (WINAPI* UIME::ImmLockIMC)( HIMC ) = NULL;
BOOL (WINAPI* UIME::ImmUnlockIMC)( HIMC )  = NULL;
LPVOID (WINAPI* UIME::ImmLockIMCC)( HIMCC ) = NULL;
BOOL (WINAPI* UIME::ImmUnlockIMCC)( HIMCC ) = NULL;
BOOL (WINAPI* UIME::ImmDisableTextFrameService)( DWORD ) = NULL;
LONG (WINAPI* UIME::ImmGetCompositionStringW)( HIMC, DWORD, LPVOID, DWORD ) = NULL;
DWORD (WINAPI* UIME::ImmGetCandidateListW)( HIMC, DWORD, LPCANDIDATELIST, DWORD ) = NULL;
HIMC (WINAPI* UIME::ImmGetContext)( HWND ) = NULL;
BOOL (WINAPI* UIME::ImmReleaseContext)( HWND, HIMC ) = NULL;
HIMC (WINAPI* UIME::ImmAssociateContext)( HWND, HIMC ) = NULL;
BOOL (WINAPI* UIME::ImmGetOpenStatus)( HIMC ) = NULL;
BOOL (WINAPI* UIME::ImmSetOpenStatus)( HIMC, BOOL ) = NULL;
BOOL (WINAPI* UIME::ImmGetConversionStatus)( HIMC, LPDWORD, LPDWORD ) = NULL;
HWND (WINAPI* UIME::ImmGetDefaultIMEWnd)( HWND ) = NULL;
UINT (WINAPI* UIME::ImmGetIMEFileNameA)( HKL, LPSTR, UINT ) = NULL;
UINT (WINAPI* UIME::ImmGetVirtualKey)( HWND ) = NULL;
BOOL (WINAPI* UIME::ImmNotifyIME)( HIMC, DWORD, DWORD, DWORD ) = NULL;
BOOL (WINAPI* UIME::ImmSetConversionStatus)( HIMC, DWORD, DWORD ) = NULL;
BOOL (WINAPI* UIME::ImmSimulateHotKey)( HWND, DWORD ) = NULL;
BOOL (WINAPI* UIME::ImmIsIME)( HKL ) = NULL;
UINT (WINAPI* UIME::ImmGetDescriptionW)(HKL, LPWSTR, UINT uBufLen) = NULL;

UINT (WINAPI * UIME::GetReadingString)( HIMC, UINT, LPWSTR, PINT, BOOL*, PUINT ) = UIME::_GetReadingString; // Traditional Chinese IME
BOOL (WINAPI * UIME::ShowReadingWindow)( HIMC, BOOL ) = UIME::_ShowReadingWindow; // Traditional Chinese IME

HMODULE UIME::sm_hIMM32 = NULL;
BOOL UIME::sm_bInited = FALSE;
UIME::IMEContext* UIME::sm_ImeContext = NULL;
HKL UIME::sm_hCurKL = NULL;
HMODULE UIME::sm_hCurIME = NULL;
UIMEControl* UIME::sm_ImeCtrl = NULL;
int UIME::sm_nCompCaret = 0;
WCHAR UIME::sm_wszCompBuffer[MAX_COMP_BUFFER];
BYTE UIME::sm_byCompStringAttr[MAX_COMP_BUFFER];
UIME::CCandList UIME::sm_CandList;
BOOL UIME::sm_bVertCandList = TRUE;
BOOL UIME::sm_bEnabled = FALSE;
HWND UIME::sm_bHwnd = NULL;
HIMC UIME::sm_himcorg = NULL;

// 对紫光拼音支持不好! 因为紫光并没有 IMN_CHANGECANDIDATE 等通知.

BOOL UIME::Initialize()
{
	if (sm_bInited)
	{
		return TRUE;
	}
	if (!InitImmApi())
	{
		return FALSE;
	}
	sm_ImeContext = new IMEContext;
	assert(sm_ImeContext);

	InitKeyboardLayer();

	sm_ImeCtrl = new UIMEControl;
	assert(sm_ImeCtrl);
	if (!sm_ImeCtrl->Create())
	{
		return FALSE;
	}
	g_Desktop->SetImeControl(sm_ImeCtrl);
	sm_bInited = TRUE;
	return TRUE;
}
void UIME::SetIMEWnd(HWND hWnd)
{
	if (sm_bHwnd == NULL)
	{
		sm_bHwnd = hWnd;
	}
	if (sm_bHwnd && sm_himcorg == NULL)
	{
		sm_himcorg = ImmGetContext(sm_bHwnd);
		ImmReleaseContext( sm_bHwnd, sm_himcorg );
	}
}

void UIME::Shutdown()
{
	g_Desktop->SetImeControl(NULL);
	if (sm_ImeCtrl)
	{
		sm_ImeCtrl->Destroy();
		delete sm_ImeCtrl;
	}

	if (sm_ImeContext)
	{
		delete sm_ImeContext; sm_ImeContext = NULL;
	}

	if (sm_hIMM32)
	{
		FreeLibrary(sm_hIMM32);
		sm_hIMM32 = NULL;
	}
	if (sm_bHwnd)
	{
		ImmAssociateContext(sm_bHwnd,sm_himcorg);
	}
	sm_bHwnd = NULL;
	sm_himcorg = NULL;

	sm_bInited = FALSE;

}

BOOL UIME::EnableIME()
{
	if (!sm_bInited || !sm_ImeCtrl)
	{
		sm_bEnabled = FALSE;
		return FALSE;
	}
	ImmAssociateContext( sm_bHwnd, sm_himcorg );
	sm_bEnabled = TRUE;

	return TRUE;
}

void UIME::DisableIME()
{
	sm_bEnabled = FALSE;
	if (sm_ImeCtrl)
	{
		sm_ImeCtrl->SetVisible(FALSE);
	}
	HIMC himcdbg;
	himcdbg = ImmAssociateContext( sm_bHwnd , NULL);
	if (himcdbg)
		sm_himcorg = himcdbg;
}

void UIME::StartComposition()
{
	if (!sm_bEnabled)
	{
		return;
	}
	sm_nCompCaret = 0;
	sm_wszCompBuffer[0] = 0;
	sm_ImeCtrl->SetVisible(TRUE);
}

void UIME::EndComposition()
{
	if (!sm_bEnabled)
	{
		return;
	}
	sm_nCompCaret = 0;
	sm_wszCompBuffer[0] = 0;
	sm_ImeCtrl->SetVisible(FALSE);
	sm_CandList.dwCount = 0;
}

void UIME::SendCompoStr()
{
	if (g_Desktop)
	{
		int nNumChar = wcslen(sm_wszCompBuffer);
		for (int c = 0; c < nNumChar; c++)
		{
			g_Desktop->Char(sm_wszCompBuffer[c], 1, 0);
		}
	//	UpdateControlRect();
	}
}

BOOL UIME::InitImmApi()
{
	if (sm_hIMM32)
	{
		return TRUE;
	}
	sm_hIMM32 = LoadLibrary("imm32.dll");
	if( sm_hIMM32 == NULL )
	{
		UTRACE("can't load imm32.dll");
		return FALSE;
	}

	{
		FARPROC Temp;

#define GETPROCADDRESS( Module, FnName, Temp ) \
	Temp = GetProcAddress( Module, #FnName ); \
	if( Temp ){ \
	*(FARPROC*)(&FnName) = Temp;\
	}else{	\
	UTRACE("ime : can't install func %s ", #FnName);\
	return FALSE;\
	}\


		GETPROCADDRESS( sm_hIMM32, ImmLockIMC, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmUnlockIMC, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmLockIMCC, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmUnlockIMCC, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmDisableTextFrameService, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmGetCompositionStringW, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmGetCandidateListW, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmGetContext, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmReleaseContext, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmAssociateContext, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmGetOpenStatus, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmSetOpenStatus, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmGetConversionStatus, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmGetDefaultIMEWnd, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmGetIMEFileNameA, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmGetVirtualKey, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmNotifyIME, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmSetConversionStatus, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmSimulateHotKey, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmIsIME, Temp );
		GETPROCADDRESS( sm_hIMM32, ImmGetDescriptionW, Temp );
#undef GETPROCADDRESS
	}

	return TRUE;
}


BOOL UIME::ProcessIMEMessage(UINT nMsg, UINT wParam, UINT lParam)
{
	return FALSE;
	if (!sm_bEnabled)
	{
		return FALSE;
	}
	BOOL bRet = FALSE;
	switch(nMsg)
	{
	case WM_INPUTLANGCHANGE:
		{
			//UTRACE("WM_INPUTLANGCHANGE");
			HKL hKL = (HKL)lParam;
			if (sm_hCurKL != hKL)
			{
				InstallInputLayer(hKL);
				sm_hCurKL = hKL;
			}

			if (ShowReadingWindow)
			{
				HIMC hImc = ImmGetContext(GetMainWnd());
				if (hImc != NULL)
				{
					ShowReadingWindow(hImc, FALSE);
					ImmReleaseContext(GetMainWnd(), hImc);
				}
			}
			bRet = TRUE;
		}
		break;
	case WM_IME_SETCONTEXT:
		{
			//UTRACE("WM_IME_SETCONTEXT");
			bRet = TRUE;
		}
		break;
	case WM_IME_STARTCOMPOSITION:
		{
			//UTRACE("WM_IME_STARTCOMPOSITION");
			StartComposition();
		}
		break;
	case WM_IME_ENDCOMPOSITION:
		{
			//UTRACE("WM_IME_ENDCOMPOSITION");
			EndComposition();
		}
		break;
	case WM_IME_COMPOSITION:
		{
			//UTRACE("WM_IME_COMPOSITION");
			bRet = TRUE;
			LONG lNumRes;
			HIMC hImc = ImmGetContext(GetMainWnd());
			if (hImc == NULL)
			{
				break;
			}
			if ( lParam & GCS_CURSORPOS )
			{
				//UTRACE("\tGCS_CURSORPOS");
				sm_nCompCaret = ImmGetCompositionStringW( hImc, GCS_CURSORPOS, NULL, 0 );
				if( sm_nCompCaret < 0 )
					sm_nCompCaret = 0; 
			}

			if ( lParam & GCS_RESULTSTR )
			{
				//UTRACE("\tGCS_RESULTSTR" );
				lNumRes = ImmGetCompositionStringW( hImc, GCS_RESULTSTR, sm_wszCompBuffer, MAX_COMP_BUFFER*sizeof(WCHAR) );
				if( lNumRes > 0 )
				{
					lNumRes /= sizeof(WCHAR);		// lNumRes in bytes.
					sm_wszCompBuffer[lNumRes] = 0; 

					SendCompoStr();
					sm_wszCompBuffer[0] = 0;
					//SendCompString();
					//ResetCompositionString();
				}
			}

			if ( lParam & GCS_COMPSTR )
			{
				//UTRACE("  GCS_COMPSTR");
				// Retrieve the latest user-selected IME candidates
				lNumRes = ImmGetCompositionStringW( hImc, GCS_COMPSTR, sm_wszCompBuffer, MAX_COMP_BUFFER*sizeof(WCHAR) );
				if( lNumRes > 0 )
				{
					lNumRes /= sizeof(WCHAR);
					sm_wszCompBuffer[lNumRes] = 0;
				}
			}

			if( lParam & GCS_COMPATTR )
			{
				//UTRACE("  GCS_COMPATTR");
				lNumRes = ImmGetCompositionStringW( hImc, GCS_COMPATTR, sm_byCompStringAttr, MAX_COMP_BUFFER);
				if( lNumRes > 0 )
					sm_byCompStringAttr[lNumRes] = 0; 
			}

			// Retrieve clause information
			//if( lParam & GCS_COMPCLAUSE )
			//{
				//UTRACE(" GCS_COMPCLAUSE (japenese???)");
			//}

			ImmReleaseContext( GetMainWnd(), hImc );
			UpdateControlRect();
		}
		break;
	case WM_IME_NOTIFY:
		{
			bRet = ProcessIMENotify(wParam, lParam);
		}
		break;
	}

	return bRet;
}

BOOL UIME::ProcessIMENotify(UINT wParam, UINT lParam )
{
	//return FALSE;
	BOOL bRet = FALSE;
	HIMC hImc;
	//UTRACE(" WM_IME_NOTIFY");
	switch(wParam)
	{
	case IMN_OPENCANDIDATE:
	case IMN_CHANGECANDIDATE:
		{
			//UTRACE( wParam == IMN_CHANGECANDIDATE ? "  IMN_CHANGECANDIDATE" : "  IMN_OPENCANDIDATE" );

			sm_CandList.bShowWindow = TRUE;
			bRet = TRUE;
			if( NULL == ( hImc = ImmGetContext( GetMainWnd() ) ) )
				break;

			LPCANDIDATELIST lpCandList = NULL;
			DWORD dwLenRequired;

			// s_bShowReadingWindow = FALSE;
			// Retrieve the candidate list
			dwLenRequired = ImmGetCandidateListW( hImc, 0, NULL, 0 );
			if( dwLenRequired )
			{
				lpCandList = (LPCANDIDATELIST)HeapAlloc( GetProcessHeap(), 0, dwLenRequired );
				dwLenRequired = ImmGetCandidateListW( hImc, 0, lpCandList, dwLenRequired );
			}

			if( lpCandList )
			{
				// Update candidate list data
				sm_CandList.dwSelection = lpCandList->dwSelection;		// 选择的字符串
				sm_CandList.dwCount = lpCandList->dwCount;				// 字符串个数	

				int nPageTopIndex = 0;
				sm_CandList.dwPageSize = __min( lpCandList->dwPageSize, MAX_CANDLIST );

				nPageTopIndex = lpCandList->dwPageStart;

				// Make selection index relative to first entry of page
				sm_CandList.dwSelection = sm_CandList.dwSelection - nPageTopIndex;

				memset( sm_CandList.awszCandidate, 0, sizeof(sm_CandList.awszCandidate) );
				for( UINT i = nPageTopIndex, j = 0;
					(DWORD)i < lpCandList->dwCount && j < sm_CandList.dwPageSize;
					i++, j++ )
				{
					// Initialize the candidate list strings
					LPWSTR pwsz = sm_CandList.awszCandidate[j];
					// For every candidate string entry,
					// write [index] + Space + [String] if vertical,
					// write [index] + [String] + Space if horizontal.
					*pwsz++ = (WCHAR)( L'0' + ( (j + 1) % 10 ) );  // Index displayed is 1 based
					if( sm_bVertCandList )
						*pwsz++ = L' ';
					WCHAR *pwszNewCand = (LPWSTR)( (LPBYTE)lpCandList + lpCandList->dwOffset[i] );
					while ( *pwszNewCand )
						*pwsz++ = *pwszNewCand++;
					if( !sm_bVertCandList )
						*pwsz++ = L' ';
					*pwsz = 0;  // Terminate
				}

				// Make dwCount in s_CandList be number of valid entries in the page.
				sm_CandList.dwCount = lpCandList->dwCount - lpCandList->dwPageStart;
				if( sm_CandList.dwCount > lpCandList->dwPageSize )
					sm_CandList.dwCount = lpCandList->dwPageSize;

				HeapFree( GetProcessHeap(), 0, lpCandList );
				ImmReleaseContext( GetMainWnd(), hImc );

				UpdateControlRect();
			}
		}
		break;

	case IMN_CLOSECANDIDATE:
		{
			//UTRACE("  IMN_CLOSECANDIDATE" );
			sm_CandList.bShowWindow = FALSE;
			/*  if( !s_bShowReadingWindow )
			{
			s_CandList.dwCount = 0;
			ZeroMemory( s_CandList.awszCandidate, sizeof(s_CandList.awszCandidate) );
			}*/
			bRet = TRUE;
			break;
		}
	case IMN_PRIVATE:
		{
			//UTRACE("  IMN_PRIVATE");
		}
		break;
	}

	return bRet;
}

void UIME::UpdateControlRect()
{
	// Width
	if (sm_ImeCtrl && sm_ImeCtrl->IsVisible())
	{
		sm_ImeCtrl->UpdateRect();
	}
}

void UIME::UpdateCurrentKL()
{

}

BOOL UIME::InitKeyboardLayer()
{
	return FALSE;
	assert(sm_ImeContext);
	UTRACE("IME::InitKeyboardLayer");

	sm_ImeContext->KeyboardLayers.clear();
	UINT NumKL = GetKeyboardLayoutList( 0, NULL );
	HKL* phKL = (HKL*)UMalloc(sizeof(HKL)*NumKL);
	if( phKL )
	{
		GetKeyboardLayoutList( NumKL, phKL );
		KeyboardLayer OurKL;
		OurKL.IconTexture = NULL;
		WCHAR wszDesc[128] = L"";
		WCHAR Desc[256];
		for( UINT i = 0; i < NumKL; ++i )
		{
			// TODO how to get the ime name ???? shit! eg : 紫光拼音输入法
			GetLocaleInfoW( MAKELCID( LOWORD( phKL[i] ), SORT_DEFAULT ), LOCALE_SLANGUAGE, wszDesc, 128 );
			ImmGetDescriptionW(phKL[i], Desc, 256);
			//如果主语言ID为亚洲语言但又不是IME,抛弃.
			if( (	PRIMARYLANGID( LOWORD( phKL[i] ) ) == LANG_CHINESE ||
				PRIMARYLANGID( LOWORD( phKL[i] ) ) == LANG_JAPANESE ||
				PRIMARYLANGID( LOWORD( phKL[i] ) ) == LANG_KOREAN ) &&
				!ImmIsIME( phKL[i] ) 
				)
				continue;

			// TODO , 这个函数可能会失败.
			ImmGetIMEFileNameA(phKL[i], OurKL.szImeFile, 256 );
			sm_ImeContext->KeyboardLayers.push_back(OurKL);

		}
		UFree(phKL);
	}

	return TRUE;
}

void UIME::InstallInputLayer(HKL hKL)
{
	return;
	CHAR szImeFile[256];
	GetReadingString = NULL;
	ShowReadingWindow = NULL;
	if(ImmGetIMEFileNameA(hKL, szImeFile, 256) == 0 )
	{
		return;
	}

	if (sm_hCurIME)
	{
		FreeLibrary(sm_hCurIME);
	}

	sm_hCurIME = LoadLibraryA( szImeFile );
	if (sm_hCurIME == NULL)
	{
		return;
	}

	GetReadingString = (UINT (WINAPI*)(HIMC, UINT, LPWSTR, PINT, BOOL*, PUINT))
		( GetProcAddress( sm_hCurIME, "GetReadingString" ) );
	ShowReadingWindow =(BOOL (WINAPI*)(HIMC, BOOL))
		( GetProcAddress( sm_hCurIME, "ShowReadingWindow" ) );


}

