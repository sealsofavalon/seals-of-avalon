#include "stdafx.h"
#include "UBase.h"	// for UIAPI
#include "UInput.h"
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>


#if(_WIN32_WINNT < 0x0500)
#define VK_BROWSER_BACK        0xA6
#define VK_BROWSER_FORWARD     0xA7
#define VK_BROWSER_REFRESH     0xA8
#define VK_BROWSER_STOP        0xA9
#define VK_BROWSER_SEARCH      0xAA
#define VK_BROWSER_FAVORITES   0xAB
#define VK_BROWSER_HOME        0xAC

#define VK_VOLUME_MUTE         0xAD
#define VK_VOLUME_DOWN         0xAE
#define VK_VOLUME_UP           0xAF
#define VK_MEDIA_NEXT_TRACK    0xB0
#define VK_MEDIA_PREV_TRACK    0xB1
#define VK_MEDIA_STOP          0xB2
#define VK_MEDIA_PLAY_PAUSE    0xB3
#define VK_LAUNCH_MAIL         0xB4
#define VK_LAUNCH_MEDIA_SELECT 0xB5
#define VK_LAUNCH_APP1         0xB6
#define VK_LAUNCH_APP2         0xB7

#endif /* _WIN32_WINNT >= 0x0500 */

static BYTE _dik2lk[256];
static BYTE _lk2vk[256];
static BYTE _vk2lk[256];
static BYTE _lk2dik[256];

struct AsciiData
{
	WORD upper;
	WORD lower;
	WORD goofy;
	WORD unused;
};

// 就目前而言, dik = lk . 值都是一样的, 但是不能保证以后没有变化.

inline UINT MapDIK2LK( UINT dikCode )
{
#define MAP_KEY(x)   case DIK_##x : return LK_##x;
	switch ( dikCode )
	{
		MAP_KEY(ESCAPE);
		MAP_KEY(1);
		MAP_KEY(2);
		MAP_KEY(3);
		MAP_KEY(4);
		MAP_KEY(5);
		MAP_KEY(6);
		MAP_KEY(7);
		MAP_KEY(8);
		MAP_KEY(9);
		MAP_KEY(0);
		MAP_KEY(MINUS);
		MAP_KEY(EQUALS);
		MAP_KEY(BACK);
		MAP_KEY(TAB);
		MAP_KEY(Q);
		MAP_KEY(W);
		MAP_KEY(E);
		MAP_KEY(R);
		MAP_KEY(T);
		MAP_KEY(Y);
		MAP_KEY(U);
		MAP_KEY(I);
		MAP_KEY(O);
		MAP_KEY(P);
		MAP_KEY(LBRACKET);
		MAP_KEY(RBRACKET);
		MAP_KEY(RETURN);
		MAP_KEY(LCONTROL);
		MAP_KEY(A);
		MAP_KEY(S);
		MAP_KEY(D);
		MAP_KEY(F);
		MAP_KEY(G);
		MAP_KEY(H);
		MAP_KEY(J);
		MAP_KEY(K);
		MAP_KEY(L);
		MAP_KEY(SEMICOLON);
		MAP_KEY(APOSTROPHE);
		MAP_KEY(GRAVE);
		MAP_KEY(LSHIFT);
		MAP_KEY(BACKSLASH);
		MAP_KEY(Z);
		MAP_KEY(X);
		MAP_KEY(C);
		MAP_KEY(V);
		MAP_KEY(B);
		MAP_KEY(N);
		MAP_KEY(M);
		MAP_KEY(COMMA);
		MAP_KEY(PERIOD);
		MAP_KEY(SLASH);
		MAP_KEY(RSHIFT);
		MAP_KEY(MULTIPLY);
		MAP_KEY(LALT);
		MAP_KEY(SPACE);
		MAP_KEY(CAPSLOCK);
		MAP_KEY(F1);
		MAP_KEY(F2);
		MAP_KEY(F3);
		MAP_KEY(F4);
		MAP_KEY(F5);
		MAP_KEY(F6);
		MAP_KEY(F7);
		MAP_KEY(F8);
		MAP_KEY(F9);
		MAP_KEY(F10);
		MAP_KEY(NUMLOCK);
		MAP_KEY(SCROLL);
		MAP_KEY(NUMPAD7);
		MAP_KEY(NUMPAD8);
		MAP_KEY(NUMPAD9);
		MAP_KEY(SUBTRACT);
		MAP_KEY(NUMPAD4);
		MAP_KEY(NUMPAD5);
		MAP_KEY(NUMPAD6);
		MAP_KEY(ADD);
		MAP_KEY(NUMPAD1);
		MAP_KEY(NUMPAD2);
		MAP_KEY(NUMPAD3);
		MAP_KEY(NUMPAD0);
		MAP_KEY(DECIMAL);
		MAP_KEY(OEM_102);
		MAP_KEY(F11);
		MAP_KEY(F12);
		MAP_KEY(F13);
		MAP_KEY(F14);
		MAP_KEY(F15);
		MAP_KEY(KANA);
		MAP_KEY(ABNT_C1);
		MAP_KEY(CONVERT);
		MAP_KEY(NOCONVERT);
		MAP_KEY(YEN);
		MAP_KEY(ABNT_C2);
		MAP_KEY(NUMPADEQUALS);
		MAP_KEY(PREVTRACK);
		MAP_KEY(AT);
		MAP_KEY(COLON);
		MAP_KEY(UNDERLINE);
		MAP_KEY(KANJI);
		MAP_KEY(STOP);
		MAP_KEY(AX);
		MAP_KEY(UNLABELED);
		MAP_KEY(NEXTTRACK);
		MAP_KEY(NUMPADENTER);
		MAP_KEY(RCONTROL);
		MAP_KEY(MUTE);
		MAP_KEY(CALCULATOR);
		MAP_KEY(PLAYPAUSE);
		MAP_KEY(MEDIASTOP);
		MAP_KEY(VOLUMEDOWN);
		MAP_KEY(VOLUMEUP);
		MAP_KEY(WEBHOME);
		MAP_KEY(NUMPADCOMMA);
		MAP_KEY(DIVIDE);
		MAP_KEY(SYSRQ);
		MAP_KEY(RALT);
		MAP_KEY(PAUSE);
		MAP_KEY(HOME);
		MAP_KEY(UP);
		MAP_KEY(PGUP);
		MAP_KEY(LEFT);
		MAP_KEY(RIGHT);
		MAP_KEY(END);
		MAP_KEY(DOWN);
		MAP_KEY(PGDN);
		MAP_KEY(INSERT);
		MAP_KEY(DELETE);
		MAP_KEY(LWIN);
		MAP_KEY(RWIN);
		MAP_KEY(APPS);
		MAP_KEY(POWER);
		MAP_KEY(SLEEP);
		MAP_KEY(WAKE);
		MAP_KEY(WEBSEARCH);
		MAP_KEY(WEBFAVORITES); 
		MAP_KEY(WEBREFRESH);
		MAP_KEY(WEBSTOP);
		MAP_KEY(WEBFORWARD);
		MAP_KEY(WEBBACK);
		MAP_KEY(MYCOMPUTER);
		MAP_KEY(MAIL); 
		MAP_KEY(MEDIASELECT);  
	}
#undef MAP_KEY
	return LK_NULL;
}


inline UINT MapLK2DIK( UINT keyCode )
{
#define MAP_KEY(x)   case LK_##x : return DIK_##x;
	switch ( keyCode )
	{
		MAP_KEY(ESCAPE);
		MAP_KEY(1);
		MAP_KEY(2);
		MAP_KEY(3);
		MAP_KEY(4);
		MAP_KEY(5);
		MAP_KEY(6);
		MAP_KEY(7);
		MAP_KEY(8);
		MAP_KEY(9);
		MAP_KEY(0);
		MAP_KEY(MINUS);
		MAP_KEY(EQUALS);
		MAP_KEY(BACK);
		MAP_KEY(TAB);
		MAP_KEY(Q);
		MAP_KEY(W);
		MAP_KEY(E);
		MAP_KEY(R);
		MAP_KEY(T);
		MAP_KEY(Y);
		MAP_KEY(U);
		MAP_KEY(I);
		MAP_KEY(O);
		MAP_KEY(P);
		MAP_KEY(LBRACKET);
		MAP_KEY(RBRACKET);
		MAP_KEY(RETURN);
		MAP_KEY(LCONTROL);
		MAP_KEY(A);
		MAP_KEY(S);
		MAP_KEY(D);
		MAP_KEY(F);
		MAP_KEY(G);
		MAP_KEY(H);
		MAP_KEY(J);
		MAP_KEY(K);
		MAP_KEY(L);
		MAP_KEY(SEMICOLON);
		MAP_KEY(APOSTROPHE);
		MAP_KEY(GRAVE);
		MAP_KEY(LSHIFT);
		MAP_KEY(BACKSLASH);
		MAP_KEY(Z);
		MAP_KEY(X);
		MAP_KEY(C);
		MAP_KEY(V);
		MAP_KEY(B);
		MAP_KEY(N);
		MAP_KEY(M);
		MAP_KEY(COMMA);
		MAP_KEY(PERIOD);
		MAP_KEY(SLASH);
		MAP_KEY(RSHIFT);
		MAP_KEY(MULTIPLY);
		MAP_KEY(LALT);
		MAP_KEY(SPACE);
		MAP_KEY(CAPSLOCK);
		MAP_KEY(F1);
		MAP_KEY(F2);
		MAP_KEY(F3);
		MAP_KEY(F4);
		MAP_KEY(F5);
		MAP_KEY(F6);
		MAP_KEY(F7);
		MAP_KEY(F8);
		MAP_KEY(F9);
		MAP_KEY(F10);
		MAP_KEY(NUMLOCK);
		MAP_KEY(SCROLL);
		MAP_KEY(NUMPAD7);
		MAP_KEY(NUMPAD8);
		MAP_KEY(NUMPAD9);
		MAP_KEY(SUBTRACT);
		MAP_KEY(NUMPAD4);
		MAP_KEY(NUMPAD5);
		MAP_KEY(NUMPAD6);
		MAP_KEY(ADD);
		MAP_KEY(NUMPAD1);
		MAP_KEY(NUMPAD2);
		MAP_KEY(NUMPAD3);
		MAP_KEY(NUMPAD0);
		MAP_KEY(DECIMAL);
		MAP_KEY(OEM_102);
		MAP_KEY(F11);
		MAP_KEY(F12);
		MAP_KEY(F13);
		MAP_KEY(F14);
		MAP_KEY(F15);
		MAP_KEY(KANA);
		MAP_KEY(ABNT_C1);
		MAP_KEY(CONVERT);
		MAP_KEY(NOCONVERT);
		MAP_KEY(YEN);
		MAP_KEY(ABNT_C2);
		MAP_KEY(NUMPADEQUALS);
		MAP_KEY(PREVTRACK);
		MAP_KEY(AT);
		MAP_KEY(COLON);
		MAP_KEY(UNDERLINE);
		MAP_KEY(KANJI);
		MAP_KEY(STOP);
		MAP_KEY(AX);
		MAP_KEY(UNLABELED);
		MAP_KEY(NEXTTRACK);
		MAP_KEY(NUMPADENTER);
		MAP_KEY(RCONTROL);
		MAP_KEY(MUTE);
		MAP_KEY(CALCULATOR);
		MAP_KEY(PLAYPAUSE);
		MAP_KEY(MEDIASTOP);
		MAP_KEY(VOLUMEDOWN);
		MAP_KEY(VOLUMEUP);
		MAP_KEY(WEBHOME);
		MAP_KEY(NUMPADCOMMA);
		MAP_KEY(DIVIDE);
		MAP_KEY(SYSRQ);
		MAP_KEY(RALT);
		MAP_KEY(PAUSE);
		MAP_KEY(HOME);
		MAP_KEY(UP);
		MAP_KEY(PGUP);
		MAP_KEY(LEFT);
		MAP_KEY(RIGHT);
		MAP_KEY(END);
		MAP_KEY(DOWN);
		MAP_KEY(PGDN);
		MAP_KEY(INSERT);
		MAP_KEY(DELETE);
		MAP_KEY(LWIN);
		MAP_KEY(RWIN);
		MAP_KEY(APPS);
		MAP_KEY(POWER);
		MAP_KEY(SLEEP);
		MAP_KEY(WAKE);
		MAP_KEY(WEBSEARCH);
		MAP_KEY(WEBFAVORITES); 
		MAP_KEY(WEBREFRESH);
		MAP_KEY(WEBSTOP);
		MAP_KEY(WEBFORWARD);
		MAP_KEY(WEBBACK);
		MAP_KEY(MYCOMPUTER);
		MAP_KEY(MAIL); 
		MAP_KEY(MEDIASELECT);  
	}
#undef MAP_KEY
	return 0;
}
inline UINT MapLK2VK(UINT lk)
{
	KeyCodes elk = (KeyCodes)lk;
	UINT dik = MapLK2DIK(lk);
	if (dik = 0)
	{
		return 0;
	}
	// MapVirtualKey 的scan code 和 vk code 相当模糊, 但是, 如果scan code 是DIK 的话, 则大部分都能正常工作.

	UINT vk = MapVirtualKey( dik, 1 );
	//VK_ADD
	return vk;
}
inline const char* LK2StringKey(UINT lk)
{
#define MAP_KEY(x,c) case LK_##c : return x;
	switch( lk )
	{
		MAP_KEY("NULL",NULL);
		MAP_KEY("ESCAPE",ESCAPE);
		MAP_KEY("1",1);
		MAP_KEY("2",2);
		MAP_KEY("3",3);
		MAP_KEY("4",4);
		MAP_KEY("5",5);
		MAP_KEY("6",6);
		MAP_KEY("7",7);
		MAP_KEY("8",8);
		MAP_KEY("9",9);
		MAP_KEY("0",0);
		//MAP_KEY("MINUS",MINUS);
		//MAP_KEY("EQUALS",EQUALS);
		MAP_KEY("-",MINUS);
		MAP_KEY("=",EQUALS);
		MAP_KEY("BACK",BACK);
		MAP_KEY("TAB",TAB);
		MAP_KEY("Q",Q);
		MAP_KEY("W",W);
		MAP_KEY("E",E);
		MAP_KEY("R",R);
		MAP_KEY("T",T);
		MAP_KEY("Y",Y);
		MAP_KEY("U",U);
		MAP_KEY("I",I);
		MAP_KEY("O",O);
		MAP_KEY("P",P);
		MAP_KEY("LBRACKET",LBRACKET);
		MAP_KEY("RBRACKET",RBRACKET);
		MAP_KEY("RETURN",RETURN);
		MAP_KEY("LCONTROL",LCONTROL);
		MAP_KEY("A",A);
		MAP_KEY("S",S);
		MAP_KEY("D",D);
		MAP_KEY("F",F);
		MAP_KEY("G",G);
		MAP_KEY("H",H);
		MAP_KEY("J",J);
		MAP_KEY("K",K);
		MAP_KEY("L",L);
		MAP_KEY("SEMICOLON",SEMICOLON);
		MAP_KEY("APOSTROPHE",APOSTROPHE);
		MAP_KEY("GRAVE",GRAVE);
		MAP_KEY("LSHIFT",LSHIFT);
		MAP_KEY("BACKSLASH",BACKSLASH);
		MAP_KEY("Z",Z);
		MAP_KEY("X",X);
		MAP_KEY("C",C);
		MAP_KEY("V",V);
		MAP_KEY("B",B);
		MAP_KEY("N",N);
		MAP_KEY("M",M);
		MAP_KEY("COMMA",COMMA);
		MAP_KEY("PERIOD",PERIOD);
		MAP_KEY("SLASH",SLASH);
		MAP_KEY("RSHIFT",RSHIFT);
		MAP_KEY("MULTIPLY",MULTIPLY);
		MAP_KEY("LALT",LALT);
		MAP_KEY("SPACE",SPACE);
		MAP_KEY("CAPSLOCK",CAPSLOCK);
		MAP_KEY("F1",F1);
		MAP_KEY("F2",F2);
		MAP_KEY("F3",F3);
		MAP_KEY("F4",F4);
		MAP_KEY("F5",F5);
		MAP_KEY("F6",F6);
		MAP_KEY("F7",F7);
		MAP_KEY("F8",F8);
		MAP_KEY("F9",F9);
		MAP_KEY("F10",F10);
		MAP_KEY("NUMLOCK",NUMLOCK);
		MAP_KEY("SCROLL",SCROLL);
		MAP_KEY("NUMPAD7",NUMPAD7);
		MAP_KEY("NUMPAD8",NUMPAD8);
		MAP_KEY("NUMPAD9",NUMPAD9);
		MAP_KEY("SUBTRACT",SUBTRACT);
		MAP_KEY("NUMPAD4",NUMPAD4);
		MAP_KEY("NUMPAD5",NUMPAD5);
		MAP_KEY("NUMPAD6",NUMPAD6);
		MAP_KEY("ADD",ADD);
		MAP_KEY("NUMPAD1",NUMPAD1);
		MAP_KEY("NUMPAD2",NUMPAD2);
		MAP_KEY("NUMPAD3",NUMPAD3);
		MAP_KEY("NUMPAD0",NUMPAD0);
		MAP_KEY("DECIMAL",DECIMAL);
		MAP_KEY("OEM_102",OEM_102);
		MAP_KEY("F11",F11);
		MAP_KEY("F12",F12);
		MAP_KEY("F13",F13);
		MAP_KEY("F14",F14);
		MAP_KEY("F15",F15);
		MAP_KEY("KANA",KANA);
		MAP_KEY("ABNT_C1",ABNT_C1);
		MAP_KEY("CONVERT",CONVERT);
		MAP_KEY("NOCONVERT",NOCONVERT);
		MAP_KEY("YEN",YEN);
		MAP_KEY("ABNT_C2",ABNT_C2);
		MAP_KEY("NUMPADEQUALS",NUMPADEQUALS);
		MAP_KEY("PREVTRACK",PREVTRACK);
		MAP_KEY("AT",AT);
		MAP_KEY("COLON",COLON);
		MAP_KEY("UNDERLINE",UNDERLINE);
		MAP_KEY("KANJI",KANJI);
		MAP_KEY("STOP",STOP);
		MAP_KEY("AX",AX);
		MAP_KEY("UNLABELED",UNLABELED);
		MAP_KEY("NEXTTRACK",NEXTTRACK);
		MAP_KEY("NUMPADENTER",NUMPADENTER);
		MAP_KEY("RCONTROL",RCONTROL);
		MAP_KEY("MUTE",MUTE);
		MAP_KEY("CALCULATOR",CALCULATOR);
		MAP_KEY("PLAYPAUSE",PLAYPAUSE);
		MAP_KEY("MEDIASTOP",MEDIASTOP);
		MAP_KEY("VOLUMEDOWN",VOLUMEDOWN);
		MAP_KEY("VOLUMEUP",VOLUMEUP);
		MAP_KEY("WEBHOME",WEBHOME);
		MAP_KEY("NUMPADCOMMA",NUMPADCOMMA);
		MAP_KEY("DIVIDE",DIVIDE);
		MAP_KEY("SYSRQ",SYSRQ);
		MAP_KEY("RALT",RALT);
		MAP_KEY("PAUSE",PAUSE);
		MAP_KEY("HOME",HOME);
		MAP_KEY("UP",UP);
		MAP_KEY("PGUP",PGUP);
		MAP_KEY("LEFT",LEFT);
		MAP_KEY("RIGHT",RIGHT);
		MAP_KEY("END",END);
		MAP_KEY("DOWN",DOWN);
		MAP_KEY("PGDN",PGDN);
		MAP_KEY("INSERT",INSERT);
		MAP_KEY("DELETE",DELETE);
		MAP_KEY("LWIN",LWIN);
		MAP_KEY("RWIN",RWIN);
		MAP_KEY("APPS",APPS);
		MAP_KEY("POWER",POWER);
		MAP_KEY("SLEEP",SLEEP);
		MAP_KEY("WAKE",WAKE);
		MAP_KEY("WEBSEARCH",WEBSEARCH);
		MAP_KEY("WEBFAVORITES",WEBFAVORITES); 
		MAP_KEY("WEBREFRESH",WEBREFRESH);
		MAP_KEY("WEBSTOP",WEBSTOP);
		MAP_KEY("WEBFORWARD",WEBFORWARD);
		MAP_KEY("WEBBACK",WEBBACK);
		MAP_KEY("MYCOMPUTER",MYCOMPUTER);
		MAP_KEY("MAIL",MAIL); 
		MAP_KEY("MEDIASELECT",MEDIASELECT);
	}
#undef MAP_KEY
	return NULL;
}
inline UINT StringKey2LK( const char * key )
{
#define MAP_KEY(key,x,c) if(stricmp(key,x) == 0) return LK_##c;
	{
		MAP_KEY(key,"ESCAPE",ESCAPE);
		MAP_KEY(key,"1",1);
		MAP_KEY(key,"2",2);
		MAP_KEY(key,"3",3);
		MAP_KEY(key,"4",4);
		MAP_KEY(key,"5",5);
		MAP_KEY(key,"6",6);
		MAP_KEY(key,"7",7);
		MAP_KEY(key,"8",8);
		MAP_KEY(key,"9",9);
		MAP_KEY(key,"0",0);
		//MAP_KEY(key,"MINUS",MINUS);
		//MAP_KEY(key,"EQUALS",EQUALS);		
		MAP_KEY(key,"-",MINUS);
		MAP_KEY(key,"=",EQUALS);
		MAP_KEY(key,"BACK",BACK);
		MAP_KEY(key,"TAB",TAB);
		MAP_KEY(key,"Q",Q);
		MAP_KEY(key,"W",W);
		MAP_KEY(key,"E",E);
		MAP_KEY(key,"R",R);
		MAP_KEY(key,"T",T);
		MAP_KEY(key,"Y",Y);
		MAP_KEY(key,"U",U);
		MAP_KEY(key,"I",I);
		MAP_KEY(key,"O",O);
		MAP_KEY(key,"P",P);
		MAP_KEY(key,"LBRACKET",LBRACKET);
		MAP_KEY(key,"RBRACKET",RBRACKET);
		MAP_KEY(key,"RETURN",RETURN);
		MAP_KEY(key,"LCONTROL",LCONTROL);
		MAP_KEY(key,"A",A);
		MAP_KEY(key,"S",S);
		MAP_KEY(key,"D",D);
		MAP_KEY(key,"F",F);
		MAP_KEY(key,"G",G);
		MAP_KEY(key,"H",H);
		MAP_KEY(key,"J",J);
		MAP_KEY(key,"K",K);
		MAP_KEY(key,"L",L);
		MAP_KEY(key,"SEMICOLON",SEMICOLON);
		MAP_KEY(key,"APOSTROPHE",APOSTROPHE);
		MAP_KEY(key,"GRAVE",GRAVE);
		MAP_KEY(key,"LSHIFT",LSHIFT);
		MAP_KEY(key,"BACKSLASH",BACKSLASH);
		MAP_KEY(key,"Z",Z);
		MAP_KEY(key,"X",X);
		MAP_KEY(key,"C",C);
		MAP_KEY(key,"V",V);
		MAP_KEY(key,"B",B);
		MAP_KEY(key,"N",N);
		MAP_KEY(key,"M",M);
		MAP_KEY(key,"COMMA",COMMA);
		MAP_KEY(key,"PERIOD",PERIOD);
		MAP_KEY(key,"SLASH",SLASH);
		MAP_KEY(key,"RSHIFT",RSHIFT);
		MAP_KEY(key,"MULTIPLY",MULTIPLY);
		MAP_KEY(key,"LALT",LALT);
		MAP_KEY(key,"SPACE",SPACE);
		MAP_KEY(key,"CAPSLOCK",CAPSLOCK);
		MAP_KEY(key,"F1",F1);
		MAP_KEY(key,"F2",F2);
		MAP_KEY(key,"F3",F3);
		MAP_KEY(key,"F4",F4);
		MAP_KEY(key,"F5",F5);
		MAP_KEY(key,"F6",F6);
		MAP_KEY(key,"F7",F7);
		MAP_KEY(key,"F8",F8);
		MAP_KEY(key,"F9",F9);
		MAP_KEY(key,"F10",F10);
		MAP_KEY(key,"NUMLOCK",NUMLOCK);
		MAP_KEY(key,"SCROLL",SCROLL);
		MAP_KEY(key,"NUMPAD7",NUMPAD7);
		MAP_KEY(key,"NUMPAD8",NUMPAD8);
		MAP_KEY(key,"NUMPAD9",NUMPAD9);
		MAP_KEY(key,"SUBTRACT",SUBTRACT);
		MAP_KEY(key,"NUMPAD4",NUMPAD4);
		MAP_KEY(key,"NUMPAD5",NUMPAD5);
		MAP_KEY(key,"NUMPAD6",NUMPAD6);
		MAP_KEY(key,"ADD",ADD);
		MAP_KEY(key,"NUMPAD1",NUMPAD1);
		MAP_KEY(key,"NUMPAD2",NUMPAD2);
		MAP_KEY(key,"NUMPAD3",NUMPAD3);
		MAP_KEY(key,"NUMPAD0",NUMPAD0);
		MAP_KEY(key,"DECIMAL",DECIMAL);
		MAP_KEY(key,"OEM_102",OEM_102);
		MAP_KEY(key,"F11",F11);
		MAP_KEY(key,"F12",F12);
		MAP_KEY(key,"F13",F13);
		MAP_KEY(key,"F14",F14);
		MAP_KEY(key,"F15",F15);
		MAP_KEY(key,"KANA",KANA);
		MAP_KEY(key,"ABNT_C1",ABNT_C1);
		MAP_KEY(key,"CONVERT",CONVERT);
		MAP_KEY(key,"NOCONVERT",NOCONVERT);
		MAP_KEY(key,"YEN",YEN);
		MAP_KEY(key,"ABNT_C2",ABNT_C2);
		MAP_KEY(key,"NUMPADEQUALS",NUMPADEQUALS);
		MAP_KEY(key,"PREVTRACK",PREVTRACK);
		MAP_KEY(key,"AT",AT);
		MAP_KEY(key,"COLON",COLON);
		MAP_KEY(key,"UNDERLINE",UNDERLINE);
		MAP_KEY(key,"KANJI",KANJI);
		MAP_KEY(key,"STOP",STOP);
		MAP_KEY(key,"AX",AX);
		MAP_KEY(key,"UNLABELED",UNLABELED);
		MAP_KEY(key,"NEXTTRACK",NEXTTRACK);
		MAP_KEY(key,"NUMPADENTER",NUMPADENTER);
		MAP_KEY(key,"RCONTROL",RCONTROL);
		MAP_KEY(key,"MUTE",MUTE);
		MAP_KEY(key,"CALCULATOR",CALCULATOR);
		MAP_KEY(key,"PLAYPAUSE",PLAYPAUSE);
		MAP_KEY(key,"MEDIASTOP",MEDIASTOP);
		MAP_KEY(key,"VOLUMEDOWN",VOLUMEDOWN);
		MAP_KEY(key,"VOLUMEUP",VOLUMEUP);
		MAP_KEY(key,"WEBHOME",WEBHOME);
		MAP_KEY(key,"NUMPADCOMMA",NUMPADCOMMA);
		MAP_KEY(key,"DIVIDE",DIVIDE);
		MAP_KEY(key,"SYSRQ",SYSRQ);
		MAP_KEY(key,"RALT",RALT);
		MAP_KEY(key,"PAUSE",PAUSE);
		MAP_KEY(key,"HOME",HOME);
		MAP_KEY(key,"UP",UP);
		MAP_KEY(key,"PGUP",PGUP);
		MAP_KEY(key,"LEFT",LEFT);
		MAP_KEY(key,"RIGHT",RIGHT);
		MAP_KEY(key,"END",END);
		MAP_KEY(key,"DOWN",DOWN);
		MAP_KEY(key,"PGDN",PGDN);
		MAP_KEY(key,"INSERT",INSERT);
		MAP_KEY(key,"DELETE",DELETE);
		MAP_KEY(key,"LWIN",LWIN);
		MAP_KEY(key,"RWIN",RWIN);
		MAP_KEY(key,"APPS",APPS);
		MAP_KEY(key,"POWER",POWER);
		MAP_KEY(key,"SLEEP",SLEEP);
		MAP_KEY(key,"WAKE",WAKE);
		MAP_KEY(key,"WEBSEARCH",WEBSEARCH);
		MAP_KEY(key,"WEBFAVORITES",WEBFAVORITES); 
		MAP_KEY(key,"WEBREFRESH",WEBREFRESH);
		MAP_KEY(key,"WEBSTOP",WEBSTOP);
		MAP_KEY(key,"WEBFORWARD",WEBFORWARD);
		MAP_KEY(key,"WEBBACK",WEBBACK);
		MAP_KEY(key,"MYCOMPUTER",MYCOMPUTER);
		MAP_KEY(key,"MAIL",MAIL); 
		MAP_KEY(key,"MEDIASELECT",MEDIASELECT);
	}
#undef MAP_KEY
	return LK_NULL;
}
void InitKeyMapTable()
{
	memset(_dik2lk, 0, sizeof(BYTE)*256);
	memset(_lk2dik, 0, sizeof(BYTE)*256);
	memset(_vk2lk, 0, sizeof(BYTE)*256);
	memset(_lk2vk, 0, sizeof(BYTE)*256);
	for (int dik  =0; dik < 256;++dik)
	{
		UINT lk = MapDIK2LK(dik);
		_dik2lk[dik] = lk;
		_lk2dik[lk] = dik;
	}
	// 当DIK 大于 DIK_NUMPAD7, 的时候,小键盘的转换就出现问题. 因此我们手工映射.
	for (int dik  =1; dik < DIK_NUMPAD7; ++dik)
	{
		KeyCodes elk = (KeyCodes)MapDIK2LK(dik);
		if (elk == LK_NULL)
		{
			continue;
		}
		BYTE vk = (BYTE)MapVirtualKey(dik, 1);
		_lk2vk[elk] = vk;
		_vk2lk[vk] = elk;
#if 0 	
		// debug , check code.
		if (vk == 0)
		{
			elk = LK_NULL;
		}else
		{
			char buf[16];
			if (!isascii(vk))
			{
				sprintf(buf,"%2x", vk);
			}else
			{
				sprintf(buf, "%c", vk);
			}
			buf[15] = '0';
		}
#endif 
	}

	struct dik2vk
	{
		BYTE dik;
		BYTE vk;
	};
	
	dik2vk dvtable[] = {
		{DIK_NUMPAD7, VK_NUMPAD7},
		{DIK_NUMPAD8, VK_NUMPAD8},
		{DIK_NUMPAD9, VK_NUMPAD9},
		{DIK_SUBTRACT, VK_SUBTRACT},		/* - on numeric keypad */
		{DIK_NUMPAD4, VK_NUMPAD4},
		{DIK_NUMPAD5, VK_NUMPAD5},
		{DIK_NUMPAD6, VK_NUMPAD6},
		{DIK_ADD, VK_ADD},
		{DIK_NUMPAD1,VK_NUMPAD1},
		{DIK_NUMPAD2,VK_NUMPAD2},
		{DIK_NUMPAD3,VK_NUMPAD3},
		{DIK_NUMPAD0,VK_NUMPAD0},
		{DIK_DECIMAL,VK_DECIMAL},			/* . on numeric keypad */
		{DIK_OEM_102,VK_OEM_102},			/* <> or \| on RT 102-key keyboard (Non-U.S.) */
		{DIK_F11, VK_F11},
		{DIK_F12, VK_F12},
		{DIK_F13, VK_F13},					/*                     (NEC PC98) */
		{DIK_F14, VK_F14},					/*                     (NEC PC98) */
		{DIK_F15, VK_F15},					/*                     (NEC PC98) */
		{DIK_KANA, VK_KANA},				/* (Japanese keyboard)            */
		{DIK_ABNT_C1, 0x00},				/* /? on Brazilian keyboard */
		{DIK_CONVERT, VK_CONVERT},			/* (Japanese keyboard)            */
		{DIK_NOCONVERT, VK_NONCONVERT},    /* (Japanese keyboard)            */
		{DIK_YEN, 0x00},					/* (Japanese keyboard)            */
		{DIK_ABNT_C2, 0x00},				/* Numpad . on Brazilian keyboard */
		{DIK_NUMPADEQUALS, 0x00},			/* = on numeric keypad (NEC PC98) */
		{DIK_PREVTRACK, 0x00},				/* Previous Track (DIK_CIRCUMFLEX on Japanese keyboard) */
		{DIK_AT, 0x00},						/*                     (NEC PC98) */
		{DIK_COLON, 0x00},					/*                     (NEC PC98) */
		{DIK_UNDERLINE, 0x00},				/*                     (NEC PC98) */
		{DIK_KANJI, 0x00},					/* (Japanese keyboard)            */
		{DIK_STOP, 0x00},					/*                     (NEC PC98) */
		{DIK_AX, VK_OEM_AX},				/*                     (Japan AX) */
		{DIK_UNLABELED, 0x00},				/*                        (J3100) */
		{DIK_NEXTTRACK, 0x00},				/* Next Track */
		{DIK_NUMPADENTER, VK_RETURN},		/* Enter on numeric keypad */
		{DIK_RCONTROL, VK_RCONTROL},
		{DIK_MUTE, 0x00},					/* Mute */
		{DIK_CALCULATOR, 0x00},					/* Calculator */
		{DIK_PLAYPAUSE, VK_MEDIA_PLAY_PAUSE},	/* Play / Pause */
		{DIK_MEDIASTOP, VK_MEDIA_STOP},			/* Media Stop */
		{DIK_VOLUMEDOWN, VK_VOLUME_DOWN},		/* Volume - */
		{DIK_VOLUMEUP, VK_VOLUME_UP},			/* Volume + */
		{DIK_WEBHOME, VK_BROWSER_HOME},			/* Web home */
		{DIK_NUMPADCOMMA, 0x00},				/* , on numeric keypad (NEC PC98) */
		{DIK_DIVIDE, VK_DIVIDE},				/* / on numeric keypad */
		{DIK_SYSRQ, VK_SNAPSHOT},
		{DIK_RALT, VK_RMENU},					/* right Alt */
		{DIK_PAUSE, VK_PAUSE},					/* Pause */
		{DIK_HOME, VK_HOME},					/* Home on arrow keypad */
		{DIK_UP, VK_UP},						/* SBR_UP on arrow keypad */
		{DIK_PRIOR, VK_PRIOR},					/* PgUp on arrow keypad */
		{DIK_LEFT, VK_LEFT},					/* SBR_LEFT on arrow keypad */
		{DIK_RIGHT, VK_RIGHT},					/* SBR_RIGHT on arrow keypad */
		{DIK_END, VK_END},						/* End on arrow keypad */
		{DIK_DOWN, VK_DOWN},					/* SBR_DOWN on arrow keypad */
		{DIK_NEXT, VK_NEXT},					/* PgDn on arrow keypad */
		{DIK_INSERT, VK_INSERT},				/* Insert on arrow keypad */
		{DIK_DELETE, VK_DELETE},				/* Delete on arrow keypad */
		{DIK_LWIN, VK_LWIN},					/* Left Windows key */
		{DIK_RWIN, VK_RWIN},					/* Right Windows key */
		{DIK_APPS, VK_APPS},					/* AppMenu key */
		{DIK_POWER, 0x00},						/* System Power */
		{DIK_SLEEP, VK_SLEEP},					/* System Sleep */
		{DIK_WAKE, 0x00},						/* System Wake */
		{DIK_WEBSEARCH, VK_BROWSER_SEARCH},		/* Web Search */
		{DIK_WEBFAVORITES, VK_BROWSER_FAVORITES},   /* Web Favorites */
		{DIK_WEBREFRESH, VK_BROWSER_REFRESH},		/* Web Refresh */
		{DIK_WEBSTOP, VK_BROWSER_STOP},				/* Web Stop */
		{DIK_WEBFORWARD, VK_BROWSER_FORWARD},		/* Web Forward */
		{DIK_WEBBACK, VK_BROWSER_BACK},				/* Web Back */
		{DIK_MYCOMPUTER, 0x00},						/* My Computer */
		{DIK_MAIL, VK_LAUNCH_MAIL},					/* Mail */
		{DIK_MEDIASELECT, VK_LAUNCH_MEDIA_SELECT},	/* Media Select */
	};

	int NumDV = sizeof(dvtable)/sizeof(dik2vk);
	for (int dik = DIK_NUMPAD7; dik < 256; dik++)
	{
		KeyCodes elk = (KeyCodes)MapDIK2LK(dik);
		if (elk == LK_NULL)
		{
			continue;
		}

		BYTE vk = 0;
		for (int i = 0; i < NumDV; i++)
		{
			if (dvtable[i].dik == dik)
			{
				vk = dvtable[i].vk;
				break;
			}
		}
		_lk2vk[elk] = vk;
		_vk2lk[vk] = elk;
	}
}

KeyCodes UDikToLKey(BYTE dik)
{
	return (KeyCodes)_dik2lk[dik];
}

BYTE  LKeyToDik(KeyCodes lk)
{
	return (UINT)_lk2dik[lk];
}

KeyCodes VKToLKey(BYTE vk)
{
	return (KeyCodes)_vk2lk[vk];
}

BYTE LKeyToVK(BYTE lk)
{
	return (UINT)_lk2vk[lk];
}