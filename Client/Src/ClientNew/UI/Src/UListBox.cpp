#include "StdAfx.h"
#include "UListBox.h"
#include "UScrollBar.h"

struct ListBoxItem 
{
	ListBoxItem():UserData(NULL)
	{
		text[0] = 0;
		bEnable = true;
	}
	WCHAR text[UListBox::ITEM_TEXT_MAX + 4];
	void* UserData;

	bool bEnable;
	static bool Sort(const ListBoxItem& A, const ListBoxItem& B)
	{
		if (!B.bEnable)
		{
			return true ;
		}

		if (!A.bEnable)
		{
			return false;
		}
		
		if (wcslen(A.text) >= wcslen(B.text))
		{
			return true ;
		}

		return false;
	}
};

struct ListBoxContext
{
	std::vector<ListBoxItem> Items ;

	void Sort()
	{
		std::sort(Items.begin(), Items.end(), ListBoxItem::Sort);
	}
	inline int GetSize() const 
	{
		return (int)Items.size();
	}

	ListBoxItem* GetListItem(int index){
		return &Items[index];
	}
};


UIMP_CLASS(UListBox,UGridCtrl);

static int sortColumn;
static bool sIncreasing;

static const char *GetColumn(const char*text)
{
	int ct = sortColumn;
	while(ct--)
	{
		text = strchr(text ,'\t');
		if (!text)
		{
			return"";
			text++;
		}
	}
	return text;
}

void UListBox::StaticInit()
{
	UREG_PROPERTY("EnumeRate", UPT_BOOL, UFIELD_OFFSET(UListBox,m_Enumerate));
	UREG_PROPERTY("ResizeCell",UPT_BOOL, UFIELD_OFFSET(UListBox,m_ResizeCell));
	UREG_PROPERTY("FitParentWidth",UPT_BOOL, UFIELD_OFFSET(UListBox,m_FitParentWidth));
	UREG_PROPERTY("ClipColumnText",UPT_BOOL, UFIELD_OFFSET(UListBox,m_ClipColumnText));
	UREG_PROPERTY("ChooseBkg",UPT_STRING, UFIELD_OFFSET(UListBox, m_ChooseBkgSkinFileName));
}

UListBox::UListBox(void)
{
	m_Active = TRUE;
	m_Enumerate = TRUE;
	m_ResizeCell = TRUE;
	m_GridCount.Set(1,0);
	m_HightLightSize.Set(0,0);
	m_FitParentWidth = TRUE;
	m_ClipColumnText = FALSE;
	m_pListBoxCtx = new ListBoxContext;
	m_ChooseBkg = NULL;
	assert(m_pListBoxCtx);
}

UListBox::~UListBox(void)
{
	if (m_pListBoxCtx)
	{
		delete m_pListBoxCtx;
	}
}

int UListBox::GetCurSel() const
{
	return m_SelGrid.y;
}

void UListBox::SetCurSel(int index)
{
	SetSel(index, 0);
}

void UListBox::SetItemEnable(int index, bool bEnable)
{
	ListBoxItem* pItem = m_pListBoxCtx->GetListItem(index);
	if (pItem == NULL)
		return;

	pItem->bEnable = bEnable;
}

bool UListBox::IsItemEnable(int index)
{
	ListBoxItem* pItem = m_pListBoxCtx->GetListItem(index);
	if (pItem == NULL)
		return false;

	return pItem->bEnable;
}

UINT UListBox::GetSelectedRow()
{
	return m_SelGrid.y;
}
void UListBox::OnSelChange(int row, int col)
{
	UGridCtrl::OnSelChange(row, col);
}
//
//UINT UListBox::GetRowWidth(ListBoxItem *row)
//{
	//UINT width = 1; 
	//const char* text = row->text;
	//for (UINT index = 0 ; index < m_pListBoxCtx->ColumnOffsets.size(); index++)
	//{
	//	//
	//	const char* nextCol = strchr(text,'\t');
	//	UINT textWidth ;
	//	if (nextCol)
	//	{
	//		;//textWidth = m_Style->m_spFont->GetStrNWidth(text,nextCol-text);
	//	}else
	//	{
	//		textWidth = m_Style->m_spFont->GetStrWidth(text);
	//	}

	//	if (m_pListBoxCtx->ColumnOffsets[index] >= 0 && (m_pListBoxCtx->ColumnOffsets[index] + textWidth) > width)
	//	{
	//		width = m_pListBoxCtx->ColumnOffsets[index] + textWidth;
	//	}

	//	if (!nextCol)
	//	{
	//		break;
	//	}
	//	text = nextCol +1;
	//}
	//return width;
//}

void UListBox::InsertItem(int index, const char *text, void* pUserData)
{
	if (text == NULL || text[0] == '\0')
	{
		return;
	}
	ListBoxItem e;
	int Ret = MultiByteToWideChar(CP_UTF8, 0, text, strlen(text), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
	e.UserData = pUserData;

	if (m_pListBoxCtx->Items.empty())
	{
		m_pListBoxCtx->Items.push_back(e);
	}
	else
	{
		if (index > m_pListBoxCtx->Items.size() )
		{
			index = m_pListBoxCtx->Items.size();
		}
		m_pListBoxCtx->Items.insert(m_pListBoxCtx->Items.begin()+ index,e);
		m_pListBoxCtx->Items[index] = e;
	}

	SetItemCount(m_pListBoxCtx->GetSize());
}

void UListBox::AddItem(const char *text, void* pUserData)
{
	if (text == NULL || text[0] == '\0')
	{
		return;
	}
	ListBoxItem e ;
	e.UserData = pUserData;
	int Ret = MultiByteToWideChar(CP_UTF8, 0, text, strlen(text), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
	m_pListBoxCtx->Items.push_back(e);
	SetItemCount(m_pListBoxCtx->GetSize());
}

void UListBox::AddItem(const WCHAR* text, void* pUserData)
{
	if (text == NULL || text[0] == 0)
	{
		return;
	}
	ListBoxItem e ;
	e.UserData = pUserData;
	wcscpy(e.text, text);
	m_pListBoxCtx->Items.push_back(e);
	SetItemCount(m_pListBoxCtx->GetSize());
}

void UListBox::SetItemText(int index, const char *text)
{
	if (index < 0 || index >= m_pListBoxCtx->GetSize())
	{
		UTRACE("UListBox::SetItemText(index) index 越界");
		return;
	}
	
	if (text == NULL || text[0] == '\0')
	{
		UTRACE("text is null.");
		return;
	}
	ListBoxItem& e = m_pListBoxCtx->Items[index];
	int Ret = MultiByteToWideChar(CP_UTF8, 0, text, strlen(text), e.text, ITEM_TEXT_MAX);
	e.text[Ret] = 0;
	//SetGridCount(m_pListBoxCtx->GetSize(), 1);
}

INT UListBox::FindItemByData(void* pUserData)
{
	for (UINT i = 0 ; i < m_pListBoxCtx->GetSize(); i++)
	{
		if (m_pListBoxCtx->Items[i].UserData == pUserData)
		{
			return i ;
		}
	}
	return -1 ;
}

INT UListBox::FindItem(const char *text)
{
	assert(text);
	WCHAR wszBuf[ITEM_TEXT_MAX];
	int nRet = MB2WC(text, strlen(text), wszBuf, ITEM_TEXT_MAX -1);
	wszBuf[nRet] = 0;
	return FindItem(wszBuf);
}

INT UListBox::FindItem(const WCHAR *text)
{
	assert(text);
	for (UINT i = 0 ; i < m_pListBoxCtx->GetSize(); i++)
	{
		if (wcscmp(m_pListBoxCtx->Items[i].text, text) == 0)
		{
			return i ;
		}
	}
	return -1;
}

void UListBox::SetItemCount(int nNumItems)
{
	m_GridCount.x = 1;
	m_GridCount.y = nNumItems;
	
	if (m_Style && m_Style->m_spFont)
	{
		//if ( mSize.x == 1 && mFitParentWidth )
		//{
		//	UScrollBar* parent = UDynamicCast(UScrollBar ,GetParent());
		//	if ( parent )
		//		mCellSize.x = parent->GetContentSize().x;
		//}
		//else
		//{
		//	// Find the maximum width cell:
		//	INT maxWidth = 1;
		//	for ( UINT i = 0; i < mList.size(); i++ )
		//	{
		//		UINT rWidth = getRowWidth( &mList[i] );
		//		if ( rWidth > maxWidth )
		//			maxWidth = rWidth;
		//	}

		//	mCellSize.x = maxWidth + 8;
		//}

      m_GridSize.y = m_Style->m_spFont->GetHeight() + 2;
   }

   // 计算新控件大小, 只放大, 注意这个地方只适合于父窗口是滚动条的情况, 否则, 控件行为有些怪异
	UPoint NewSize(m_GridCount.x * m_GridSize.x + m_HeadSize.x, m_GridCount.y * m_GridSize.y + m_HeadSize.y);
	NewSize.x = __max(m_GridCount.x * m_GridSize.x + m_HeadSize.x , m_Size.x);
	NewSize.y = __max(m_GridCount.y * m_GridSize.y + m_HeadSize.y, m_Size.y);

	m_HightLightSize = m_GridSize;
	UScrollBar* pSB = UDynamicCast(UScrollBar,GetParent());
	if (pSB)
	{
		SetHeight(m_GridCount.y * m_GridSize.y + m_HeadSize.y);
		pSB->ReCalLayout();
		pSB->scrollTo(0,m_GridCount.y * m_GridSize.y + m_HeadSize.y);

		UINT ParentWidth = m_Parent->GetWidth();
		m_HightLightSize.x  = (float)pSB->GetContentSize().x;
		m_HightLightSize.y = m_GridSize.y;

	}
	SetSize(NewSize);
}

BOOL UListBox::OnCreate()
{
	if (!UGridCtrl::OnCreate())
	{
		return FALSE;
	}

	m_HightLightSize = m_GridSize ;
	if (m_ChooseBkgSkinFileName.GetBuffer())
	{
		if (m_ChooseBkg == NULL)
		{
			m_ChooseBkg = sm_UiRender->LoadTexture(m_ChooseBkgSkinFileName.GetBuffer());
		}
	}
	return TRUE;
}

void UListBox::OnDestroy()
{
	UGridCtrl::OnDestroy();
}

BOOL UListBox::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	return TRUE;
}

UINT UListBox::GetItemCount()
{
	return (UINT)m_pListBoxCtx->Items.size();
}

void* UListBox::GetItemData(int index)
{
	assert(index >= 0 && index < GetItemCount());
	return m_pListBoxCtx->Items[index].UserData;
}

void UListBox::Clear()
{
	m_pListBoxCtx->Items.clear();
	SetItemCount(0);
	m_HoverGrid.Set(-1,-1);
	ClearSel();
}

void UListBox::Sort(UINT column, bool increasing /* = TRUE */)
{
	m_pListBoxCtx->Sort();
}
void UListBox::SortNumerical(UINT column, bool increasing /* = TRUE */)
{

}
void UListBox::OnRemove()
{
	Clear();
	UGridCtrl::OnRemove();
}

void UListBox::DeleteItem(INT index)
{
	if (index < 0 && index > m_pListBoxCtx->Items.size())
	{
		return;
	}
	m_pListBoxCtx->Items.erase(m_pListBoxCtx->Items.begin()+index);
	SetItemCount(m_pListBoxCtx->GetSize());
	ClearSel();
}

int UListBox::GetSelectedText(char* pText, int nMaxLen)
{
	if (m_SelGrid.y == -1 || m_SelGrid.y >= GetItemCount())
	{
		return 0;
	}
	
	if (pText == NULL || nMaxLen <= 0)
	{
		return wcslen(m_pListBoxCtx->Items[m_SelGrid.y].text);
	}
	
	int nRet = WideCharToMultiByte(CP_UTF8, 0, m_pListBoxCtx->Items[m_SelGrid.y].text, -1, pText, nMaxLen, NULL, NULL);
	return nRet;
}

void UListBox::OnRender(const UPoint& offset, const URect &updateRect)
{
	UGridCtrl::OnRender(offset,updateRect);
}

void UListBox::DrawGridItem(UPoint offset, UPoint cell, BOOL selected, BOOL mouseOver)
{
	if (m_Style->mBorder)
	{
		offset.x += 1;
	}

	bool bEnable = m_pListBoxCtx->Items[cell.y].bEnable;

	if (bEnable) 
	{
		URect hightLightRect = URect(offset,m_HightLightSize);
		
		if (selected)
		{
			if (m_ChooseBkg)
				sm_UiRender->DrawImage(m_ChooseBkg, hightLightRect);
			else
				sm_UiRender->DrawRectFill(hightLightRect,m_Style->mFillColorHL);
		}else if (mouseOver)
		{
			if (m_ChooseBkg)
				sm_UiRender->DrawImage(m_ChooseBkg, hightLightRect);
			else
				sm_UiRender->DrawRectFill(hightLightRect,m_Style->mFillColor);
		}
	}

	UColor fontColor = selected ? m_Style->m_FontColorHL : m_Style->m_FontColor;
	fontColor = bEnable ? fontColor : m_Style->m_FontColorNA;
	const WCHAR* text = m_pListBoxCtx->Items[cell.y].text;
	//sm_UiRender->DrawTextN(m_Style->m_spFont,offset,text, wcslen(text), NULL, fontColor);
	USetTextEdge(selected);
	UDrawText(sm_UiRender, m_Style->m_spFont, offset, m_HightLightSize, fontColor, text, m_Style->mAlignment);
	USetTextEdge(FALSE);
}

