#include "StdAfx.h"
#include "UListCtrl.h"

struct ListCtrlContex
{
	std::vector<ULCITEM*> Items;
	std::vector<ULCITEM*> SelectedItems;
};

UIMP_CLASS(UListCtrl,UControl);
void UListCtrl::StaticInit()
{
	UREG_PROPERTY("AllowMultipleSelections",UPT_BOOL,UFIELD_OFFSET(UListCtrl,m_MultipeSelections));
	UREG_PROPERTY("FitParentWidth",UPT_BOOL,UFIELD_OFFSET(UListCtrl,m_FitParentWidth));
}

INT UListCtrl::getStringElementCount( const char *inString )
{
	// Non-whitespace chars.
	static const char* set = " \t\n";

	// End of string.
	if ( *inString == 0 )
		return 0;

	UINT wordCount = 0;
	UINT search = 0;

	// Search String.
	while( *inString )
	{
		// Get string element.
		search = *inString;

		// End of string?
		if ( search == 0 )
			break;

		// Move to next element.
		inString++;

		// Search for seperators.
		for( UINT i = 0; set[i]; i++ )
		{
			// Found one?
			if( search == set[i] )
			{
				// Yes...
				search = 0;
				break;
			}   
		}

		// Found a seperator?
		if ( search == 0 )
			continue;

		// We've found a non-seperator.
		wordCount++;

		// Search for end of non-seperator.
		while( 1 )
		{
			// Get string element.
			search = *inString;

			// End of string?
			if ( search == 0 )
				break;

			// Move to next element.
			inString++;

			// Search for seperators.
			for( UINT i = 0; set[i]; i++ )
			{
				// Found one?
				if( search == set[i] )
				{
					// Yes...
					search = 0;
					break;
				}   
			}

			// Found Seperator?
			if ( search == 0 )
				break;
		}

		// End of string?
		if ( *inString == 0 )
		{
			// Bah!
			break;
		}
	}

	// We've finished.
	return wordCount;

}
const char* UListCtrl::getStringElement( const char* inString, const INT index )
{
	static const char* set = " \t\n";

	UINT wordCount = 0;
	UINT search = 0;
	const char* pWordStart = NULL;

	// End of string?
	if ( *inString != 0 )
	{
		// No, so search string.
		while( *inString )
		{
			// Get string element.
			search = *inString;

			// End of string?
			if ( search == 0 )
				break;

			// Move to next element.
			inString++;

			// Search for seperators.
			for( UINT i = 0; set[i]; i++ )
			{
				// Found one?
				if( search == set[i] )
				{
					// Yes...
					search = 0;
					break;
				}   
			}

			// Found a seperator?
			if ( search == 0 )
				continue;

			// Found are word?
			if ( wordCount == index )
			{
				// Yes, so mark it.
				pWordStart = inString-1;
			}

			// We've found a non-seperator.
			wordCount++;

			// Search for end of non-seperator.
			while( 1 )
			{
				// Get string element.
				search = *inString;

				// End of string?
				if ( search == 0 )
					break;

				// Move to next element.
				inString++;

				// Search for seperators.
				for( UINT i = 0; set[i]; i++ )
				{
					// Found one?
					if( search == set[i] )
					{
						// Yes...
						search = 0;
						break;
					}   
				}

				// Found Seperator?
				if ( search == 0 )
					break;
			}

			// Have we found our word?
			if ( pWordStart )
			{
				// Yes, so we've got our word...

				// Result Buffer.
				static char buffer[4096];

				// Calculate word length.
				const UINT length = inString - pWordStart - ((*inString)?1:0);

				// Copy Word.
				strncpy( buffer, pWordStart, length);
				buffer[length] = '\0';

				// Return Word.
				return buffer;
			}

			// End of string?
			if ( *inString == 0 )
			{
				// Bah!
				break;
			}
		}
	}

	// Didn't find it
	return " ";
}
UListCtrl::UListCtrl(void)
{
	m_MultipeSelections = TRUE;
	m_FitParentWidth = TRUE;
	m_ItemSize = UPoint(10,20);
	m_LastClickItem = NULL;
}
UListCtrl::~UListCtrl(void)
{
	ClearItems();
}

INT UListCtrl::GetItemCount()
{
	if (m_pListCtx)
	{
		return m_pListCtx->Items.size();
	}	
	return 0;
}

INT UListCtrl::GetSelectCount()
{
	return m_pListCtx ? m_pListCtx->SelectedItems.size() : 0;
}

INT UListCtrl::GetSelectedItem()
{
	if (m_pListCtx == NULL)
	{
		return -1;
	}
	if (m_pListCtx->SelectedItems.empty() || m_pListCtx->Items.empty())
	{
		return -1 ;
	}

	for(INT i = 0 ; i < m_pListCtx->Items.size(); i++)
	{
		if (m_pListCtx->Items[i]->isSelected)
		{
			return i ;
		}
	}

	return -1 ;
}
void UListCtrl::GetSelectedItems(vInt& Items)
{
	Items.clear();
	if (m_pListCtx->SelectedItems.empty())
	{
		return;
	}

	for(INT i=0 ; i < m_pListCtx->Items.size() ; i++)
	{
		if (m_pListCtx->Items[i]->isSelected)
		{
			Items.push_back(i);
		}
	}
}
INT UListCtrl::GetItemIndex(ULCITEM* item)
{
	if (m_pListCtx->Items.empty())
	{
		return -1 ;
	}
	for(INT i=0 ; i < m_pListCtx->Items.size() ; i++)
	{
		if (m_pListCtx->Items[i] == item)
		{
			return i;
		}
	}
	return -1 ;
}
const char* UListCtrl::GetItemText(INT index)
{
	if (index >= m_pListCtx->Items.size() || index < 0)
	{
		return NULL ;
	}
	return m_pListCtx->Items[index]->tableEntryText;
}

void UListCtrl::SetCurSelected(INT index)
{
	if (index >= m_pListCtx->Items.size() || index < 0 )
	{
		return;
	}
	if (index == -1)
	{
		m_pListCtx->SelectedItems.clear();
		return;
	}

	AddSeletion(m_pListCtx->Items[index],index);
}

void UListCtrl::AddSeletion(ULCITEM *item, INT index )
{
	if (!m_MultipeSelections)
	{
		if (!m_pListCtx->SelectedItems.empty())
		{
			ULCITEM* selItem = m_pListCtx->SelectedItems.front();
			if (selItem != item)
			{
				ClearSelection();
			}
			else
			{
				return;
			}
		}
	}else
	{
		if (!m_pListCtx->SelectedItems.empty())
		{
			for(INT i = 0 ; i < m_pListCtx->SelectedItems.size() ; i++)
			{
				if (m_pListCtx->SelectedItems[i] = item)
				{
					return;
				}
			}
		}
	}

	item->isSelected = TRUE;
	m_pListCtx->SelectedItems.insert(m_pListCtx->SelectedItems.begin(),item);
}
void UListCtrl::SetCurSelectedRang(INT start , INT stop)
{
	if (start < 0)
	{
		start = 0 ;
	}else if (start > m_pListCtx->Items.size())
	{
		start = m_pListCtx->Items.size();
	}

	if (stop < 0 )
	{
		stop = 0;
	}else if (stop > m_pListCtx->Items.size())
	{
		stop = m_pListCtx->Items.size();
	}

	INT iterStart = (start < stop) ? start : stop ;
	INT iterStop = (stop < start) ? stop :  start ;

	for(; iterStart <= iterStop ;iterStart++)
	{
		AddSeletion(m_pListCtx->Items[iterStart],iterStart);
	}


}
void UListCtrl::SetItemText(INT index,const char* text)
{
	if (index > m_pListCtx->Items.size() || index < 0)
	{
		return;
	}
	if (!text)
	{
		return;
	}
	m_pListCtx->Items[index]->tableEntryText = text ;
}

INT UListCtrl::AddItem(const char* text, void*itemDate)
{
	return InsertItem(m_pListCtx->Items.size(),text,itemDate);
}
INT UListCtrl::AddItemWithColour(const char* text,UColor color , void*itemDate)
{
	return InsertItemWithColour(m_pListCtx->Items.size(),text,color,itemDate);
}
INT UListCtrl::InsertItem(INT index, const char* text,void*itemDate)
{
	if (index >= m_pListCtx->Items.size())
	{
		index = m_pListCtx->Items.size();
	}
	if (!text )
	{
		return -1 ;
	}

	ULCITEM*  newItem = new ULCITEM;
	if (!newItem)
	{
		return -1; 
	}
	newItem->tableEntryText = text ;
	newItem->itemDate = itemDate;
	newItem->isSelected = FALSE;
	newItem->hasColor = FALSE;

	m_pListCtx->Items.insert(m_pListCtx->Items.begin()+index,newItem);

	return index ;
}
INT UListCtrl::InsertItemWithColour(INT index, const char* text,UColor color, void*itemDate)
{
	if (index >= m_pListCtx->Items.size())
	{
		index = m_pListCtx->Items.size();
	}
	if (!text )
	{
		return -1 ;
	}

	ULCITEM*  newItem = new ULCITEM;
	if (!newItem)
	{
		return -1; 
	}
	newItem->tableEntryText = text ;
	newItem->itemDate = itemDate;
	newItem->isSelected = FALSE;
	newItem->hasColor = TRUE;
	newItem->color = color;

	m_pListCtx->Items.insert(m_pListCtx->Items.begin()+index,newItem);

	return index ;
}
INT UListCtrl::FindItemByText(const char* text,BOOL caseSensitive )
{
	if (!text || !text[0] || text == "")
	{
		return -1;
	}
	if (m_pListCtx->Items.empty())
	{
		return -1;
	}

	for (INT i =0; i < m_pListCtx->Items.size() ; i++)
	{
		if (caseSensitive && (strcmp(m_pListCtx->Items[i]->tableEntryText,text) == 0))
		{
			return i;
		}
	    else if (!caseSensitive && (strcmp(m_pListCtx->Items[i]->tableEntryText,text) == 0))
	    {
			return i ;
	    }
	}

	return -1; 
}

void UListCtrl::SetItemColor(INT index,UColor color)
{
	if (index > m_pListCtx->Items.size() || index < 0)
	{
		return;
	}
	ULCITEM * item = m_pListCtx->Items[index];
	item->hasColor = TRUE ;
	item->color = color ;
}
void UListCtrl::ClearItemColor(INT index)
{
	if (index > m_pListCtx->Items.size() || index < 0)
	{
		return;
	}
	ULCITEM* item = m_pListCtx->Items[index];
	item->hasColor = FALSE;
}

void UListCtrl::DeleteItem(INT index)
{
	if (index >= m_pListCtx->Items.size() || index < 0)
	{
		return;
	}
	ULCITEM* item = m_pListCtx->Items[index];
	if (!item)
	{
		return ;
	}

	if (item->isSelected)
	{
		for( std::vector<ULCITEM*>::iterator i = m_pListCtx->SelectedItems.begin(); i != m_pListCtx->SelectedItems.end(); i++)
		{
			if (item == *i)
			{
				m_pListCtx->SelectedItems.erase(i);
				break;
			}
		}
	}

	m_pListCtx->Items.erase(m_pListCtx->Items.begin() +index);
	delete item;
}
void UListCtrl::ClearItems()
{
	while(m_pListCtx->Items.size())
		DeleteItem(0);
	m_pListCtx->Items.clear();
	m_pListCtx->SelectedItems.clear();
}
void UListCtrl::ClearSelection()
{
	if (!m_pListCtx->SelectedItems.size())
	{
		return;
	}
	std::vector<ULCITEM*>::iterator i = m_pListCtx->SelectedItems.begin();
	for (; i != m_pListCtx->SelectedItems.end(); i++)
	{
		(*i)->isSelected = FALSE;
	}
	m_pListCtx->SelectedItems.clear();
}
void UListCtrl::RemoveSelection(ULCITEM* item, INT index)
{
	if( !m_pListCtx->SelectedItems.size() )
		return;

	if( !item )
		return;

	for( INT i = 0 ; i < m_pListCtx->SelectedItems.size(); i++ )
	{
		if( m_pListCtx->SelectedItems[i] == item )
		{
			m_pListCtx->SelectedItems.erase( m_pListCtx->SelectedItems.begin()+i);
			item->isSelected = FALSE;
			return;
		}
	}
}
void UListCtrl::RemoveSelection(INT index)
{
	if( index >= m_pListCtx->Items.size() || index < 0 )
	{
		return;
	}

	RemoveSelection(m_pListCtx->Items[index], index );
}

void UListCtrl::UpdateSize()
{
	
}
void UListCtrl::OnRenderItem( URect itemRect, ULCITEM *item )
{
	if (item->isSelected)
	{
		sm_UiRender->DrawRectFill(itemRect,m_Style->mBorderColor);
	}
	UDrawText(sm_UiRender,m_Style->m_spFont,itemRect.GetPosition() + UPoint(2,0),m_Size,m_Style->m_FontColor,item->tableEntryText);
}
void UListCtrl::DrawBox(const UPoint &box, INT size, UColor &outlineColor, UColor &boxColor)
{
	URect r(box.x - size,box.y - size, 2*size + 1, 2*size + 1);
	r.InflateRect(1,1);
	sm_UiRender->DrawRectFill(r,boxColor);
	r.InflateRect(-1,-1);
	sm_UiRender->DrawRect(r,outlineColor);
}

BOOL UListCtrl::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	UpdateSize();

	return TRUE;
}
void UListCtrl::OnDestroy()
{
	UControl::OnDestroy();
}
void UListCtrl::OnRender(const UPoint& offset, const URect &updateRect)
{
	URect clipRect = updateRect ;
	URect oldClipRect = clipRect;

	for (INT i = 0; i < m_pListCtx->Items.size(); i++)
	{
		INT colorBoxSize = 0 ;
		UColor boxColor = UColor(0,0,0);
		if ((i+1)*(m_ItemSize.y) + offset.y >= updateRect.bottom)
		{
			break;
		}
		if (m_pListCtx->Items[i]->hasColor)
		{
			colorBoxSize = 3 ;
			boxColor = UColor(m_pListCtx->Items[i]->color);
			UColor black = UColor(0,0,0);

			DrawBox(UPoint(offset.x + m_Style->mTextOffset.x + colorBoxSize, offset.y+(i* m_ItemSize.y + 8)),colorBoxSize,black,boxColor);
		}

		URect itemRect = URect(offset.x +m_Style->mTextOffset.x + colorBoxSize*2,offset.y +i* m_ItemSize.y,m_ItemSize.x, m_ItemSize.y);
		OnRenderItem(itemRect,m_pListCtx->Items[i]);
	}
	//sm_UiRender->SetClipRect(oldClipRect);
}
void UListCtrl::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{

}
void UListCtrl::OnMouseDragged(const UPoint& position, UINT flags)
{

}