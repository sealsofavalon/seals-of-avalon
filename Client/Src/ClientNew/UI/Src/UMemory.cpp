#include "StdAfx.h"
#include "UMemory.h"


#if defined(UI_MEMDBG)
KMemoryManagerTrack g_MemMgr;

void* LMallocDbg(size_t stSizeInBytes, const char* pcSourceFile, int iSourceLine, const char* pcFunction)
{
	// 必须使内存分配形式通过参数传入进来
	return g_MemMgr.Allocate(stSizeInBytes, 4, LMEMOP_MALLOC,FALSE,pcSourceFile,iSourceLine,pcFunction);
}

void* LReallocDbg(void *pMem, size_t stSizeInBytes,const char* pcSourceFile, int iSourceLine, const char* pcFunction)
{
	return g_MemMgr.Reallocate(pMem,stSizeInBytes, 4, LMEMOP_REALLOC,FALSE,-1,pcSourceFile,iSourceLine,pcFunction);
}

void LFreeDbg(void* pMem,LMemOp op)
{
	g_MemMgr.Deallocate(pMem,op);
}

// 我们不能用 new(__FILE__,__LINE__,__FUNCTION__) 来重定义new . 带来一堆警告.
// 因此只好还是用new(size_t) 形式了. 但是.在基于对象方法的new delete 中,上面的形式还是可以的.

BOOL LNewMemTrack(const char* sFile,DWORD dwLine, const char* sFunc)
{
	g_MemMgr.BeginNewOpTrack(sFile,dwLine,sFunc);
	return FALSE;
}

void* LNewMemDbg(size_t sz,LMemOp op)
{
	return g_MemMgr.Allocate(sz,4,op,FALSE);
}

#else

#endif


#ifdef UI_MEMDBG
KMemoryManagerTrack::KMemoryManagerTrack()
{
	m_stReservoirGrowBy = 4096;
	m_bAlwaysValidateAll = TRUE;
}

KMemoryManagerTrack::~KMemoryManagerTrack()
{
	if (m_ppkReservoirBuffer)
	{
		for (unsigned int i = 0; i < m_stReservoirBufferSize; i++)
		{
			free(m_ppkReservoirBuffer[i]);
		}
		free(m_ppkReservoirBuffer);
		m_ppkReservoirBuffer = NULL;
	}
}

BOOL KMemoryManagerTrack::Initialize()
{
	m_stCurrentAllocID = 0;
	m_NewOpFLF.Reset();
	ResetSummaryStats();
	return TRUE;
}

void KMemoryManagerTrack::Shutdown()
{

	ReportLeakMemory();

	if (m_ppkReservoirBuffer)
	{
		for (unsigned int i = 0; i < m_stReservoirBufferSize; i++)
		{
			free(m_ppkReservoirBuffer[i]);
		}
		free(m_ppkReservoirBuffer);
		m_ppkReservoirBuffer = NULL;
	}
}

void* KMemoryManagerTrack::Allocate(size_t stSizeinBytes,
									size_t Alignment,
									LMemOp eEventType,
									BOOL bProvideAccurateSizeOnDeallocate,
									const char *pcSourceFile,
									int iSourceLine,
									const char* pcFunction)
{
	m_CriticalSec.Lock();
	size_t stSizeOriginal = stSizeinBytes; 
	void* pvMem = malloc(stSizeinBytes);   

	// update summary statistics
	m_stActiveAllocationCount++;
	m_stAccumulatedAllocationCount++;
	if (m_stActiveAllocationCount > m_stPeakAllocationCount)
	{
		m_stPeakAllocationCount = m_stActiveAllocationCount;
	}

	m_stActiveMemory += stSizeinBytes;
	m_stAccumulatedMemory += stSizeinBytes;
	if (m_stActiveMemory > m_stPeakMemory)
	{
		m_stPeakMemory = m_stActiveMemory;
	}

	if (pvMem == NULL)
	{
		m_CriticalSec.Unlock();
		return NULL;
	}

	assert(stSizeinBytes >= stSizeOriginal);

	// Fill the originally requested memory size with the pad character.
	// This will allow us to see how much of the allocation was 
	// actually used.
	MemoryFillWithPattern(pvMem, stSizeOriginal);

#ifdef LMEMORY_ENABLE_EXCEPTIONS
	try
	{
#endif

		assert(FindAllocUnit(pvMem) == NULL);

		// Grow the tracking unit reservoir if necessary
		if (!m_pkReservoir)
			GrowReservoir();

		assert(m_pkReservoir != NULL);
		KAllocUnit* pkUnit = m_pkReservoir;
		m_pkReservoir = pkUnit->m_pkNext;

		// fill in the known information
		pkUnit->Reset();
		pkUnit->m_stAllocationID = m_stCurrentAllocID;
		pkUnit->m_ulAllocThreadId = GetCurrentThreadId();
		pkUnit->m_eAllocType = eEventType;
		pkUnit->m_stAlignment = Alignment;
		//		pkUnit->m_fAllocTime = fTime;
		pkUnit->m_pvMem = pvMem;
		pkUnit->m_stSizeRequested = stSizeOriginal;
		pkUnit->m_stSizeAllocated = stSizeinBytes;

		if (eEventType == LMEMOP_NEW || eEventType == LMEMOP_NEWARR)
		{
			assert(m_NewOpFLF.m_pcFile);
			pkUnit->m_kFLF.Set(m_NewOpFLF.m_pcFile, m_NewOpFLF.m_uiLine, m_NewOpFLF.m_pcFunc);
		}else
		{
			pkUnit->m_kFLF.Set(pcSourceFile, iSourceLine, pcFunction);
		}


		assert(eEventType != LMEMOP_UNKNOWN);

		// Insert the new allocation into the hash table
		InsertAllocUnit(pkUnit);

		// Validate every single allocated unit in memory
		if (m_bAlwaysValidateAll)
		{
			BOOL bValidateAllAllocUnits = ValidateAllAllocUnits();
			assert(bValidateAllAllocUnits);
		}

#ifdef LMEMORY_ENABLE_EXCEPTIONS
	}
	catch(const char *err)
	{

	}
#endif
	++m_stCurrentAllocID;
	m_NewOpFLF.Reset();
	m_CriticalSec.Unlock();
	return pvMem;
}

void* KMemoryManagerTrack::Reallocate(
									  void* pvMemory, 
									  size_t stSizeinBytes, 
									  size_t Alignment,
									  LMemOp eEventType, 
									  BOOL bProvideAccurateSizeOnDeallocate,
									  size_t stSizeCurrent,
									  const char *pcSourceFile,
									  int iSourceLine,
									  const char* pcFunction)
{
	size_t stSizeOriginal = stSizeinBytes;

	if (pvMemory == NULL)
	{
		return Allocate(stSizeinBytes, Alignment, eEventType, bProvideAccurateSizeOnDeallocate, 
			pcSourceFile, iSourceLine, pcFunction);
	}

	if (stSizeinBytes== 0)
	{
		Deallocate(pvMemory, eEventType);
		return NULL;
	}

	m_CriticalSec.Lock();

#ifdef LMEMORY_ENABLE_EXCEPTIONS
	try
	{
#endif
		KAllocUnit* pkUnit = FindAllocUnit(pvMemory);

		if (pkUnit == NULL)
		{

			assert(pkUnit != NULL);
#ifdef LMEMORY_ENABLE_EXCEPTIONS
			throw "非法的内存地址";
#endif
			m_CriticalSec.Unlock();
			return NULL;
		}

		assert(ValidateAllocUnit(pkUnit));

		// Determine how much memory was actually set
		size_t stSizeUnused = MemoryBytesWithPattern(pkUnit->m_pvMem,
			pkUnit->m_stSizeRequested);
		m_stUnusedButAllocatedMemory += stSizeUnused;

		// Save the thread id that freed the memory
		//unsigned long ulFreeThreadId = GetCurrentThreadId();

		void* pvNewMemory = realloc(pvMemory, stSizeinBytes);

		assert(pvNewMemory != NULL);

		// update summary statistics
		size_t stPreReallocSize = pkUnit->m_stSizeRequested;

		m_stAccumulatedAllocationCount++;
		int iDeltaSize = stSizeinBytes - pkUnit->m_stSizeAllocated;
		m_stActiveMemory += iDeltaSize;

		if (iDeltaSize > 0)
			m_stAccumulatedMemory += iDeltaSize;

		if (m_stActiveMemory > m_stPeakMemory)
		{
			m_stPeakMemory = m_stActiveMemory;
			//			m_fPeakMemoryTime = fTime;
		}


		// Fill the originally requested memory size with the pad character.
		// This will allow us to see how much of the allocation was 
		// actually used.
		// For reallocation, we only want to fill the unused section
		// that was just allocated.
		if (iDeltaSize > 0)
		{
			MemoryFillWithPattern((char*)pvNewMemory + stPreReallocSize, 
				iDeltaSize);
		}

		//LMEMASSERT(m_stCurrentAllocID != ms_stBreakOnAllocID);

		// If you hit this LMEMASSERT, you requested a breakpoint on a specific 
		// allocation request size.
		//LMEMASSERT(ms_stBreakOnSizeRequested != stSizeOriginal);

		// If you hit this LMEMASSERT, then this reallocation was made from a  
		// that isn't setup to use this memory tracking software, use the stack
		// source frame to locate the source and include KMemManager.h.
		assert(eEventType != LMEMOP_UNKNOWN);

		// If you hit this LMEMASSERT, you were trying to reallocate RAM that was 
		// not allocated in a way that is compatible with realloc. In other 
		// words, you have a allocation/reallocation mismatch.
		assert(pkUnit->m_eAllocType == LMEMOP_MALLOC 
			|| pkUnit->m_eAllocType == LMEMOP_REALLOC
			|| pkUnit->m_eAllocType == LMEMOP_ALIGNEDMALLOC 
			|| pkUnit->m_eAllocType == LMEMOP_ALIGNEDREALLOC);

		// update allocation unit
		//		LMemOp eDeallocType = eEventType;

		RemoveAllocUnit(pkUnit);   
		--m_stActiveAllocationCount;

		if (m_bAlwaysValidateAll)
		{
			assert(ValidateAllAllocUnits());
		}

		// Recycle the allocation unit
		// Add it to the front of the reservoir 
		pkUnit->m_kFLF.Reset();
		pkUnit->m_pkNext = m_pkReservoir;
		m_pkReservoir = pkUnit;
		pkUnit = NULL;

		// Grow the tracking unit reservoir if necessary
		if (!m_pkReservoir)
			GrowReservoir();

		// If you hit this LMEMASSERT, the free store for allocation units
		// does not exist. This should only happen if the reservoir 
		// needed to grow and was unable to satisfy the request. In other words,
		// you may be out of memory.
		assert(m_pkReservoir != NULL);

		// Get an allocation unit from the reservoir
		KAllocUnit* pkNewUnit = m_pkReservoir;
		m_pkReservoir = pkNewUnit->m_pkNext;

		// fill in the known information
		pkNewUnit->Reset();
		pkNewUnit->m_stAllocationID = m_stCurrentAllocID; 
		pkNewUnit->m_stAlignment = Alignment;
		pkNewUnit->m_ulAllocThreadId = GetCurrentThreadId();
		pkNewUnit->m_eAllocType = eEventType;
		//pkNewUnit->m_fAllocTime = fTime;
		pkNewUnit->m_pvMem = pvNewMemory;
		pkNewUnit->m_stSizeAllocated = stSizeinBytes;
		pkNewUnit->m_stSizeRequested = stSizeOriginal;
		pkNewUnit->m_kFLF.Set(pcSourceFile, iSourceLine, pcFunction);

		// If you hit this LMEMASSERT, then this allocation was made from a source
		// that isn't setup to use this memory tracking software, use the 
		// stack frame to locate the source and include KMemManager.h.
		assert(eEventType != LMEMOP_UNKNOWN);

		// Insert the new allocation into the hash table
		InsertAllocUnit(pkNewUnit);
		++m_stActiveAllocationCount;
		++m_stCurrentAllocID;

		if (m_bAlwaysValidateAll)
		{
			assert(ValidateAllAllocUnits());
		}


#ifdef LMEMORY_ENABLE_EXCEPTIONS
	}
	catch(const char *err)
	{
	}
#endif
	m_CriticalSec.Unlock();

	return pvNewMemory;
}


void KMemoryManagerTrack::Deallocate(void* pvMemory, LMemOp eEventType)
{
	if (pvMemory)
	{

		m_CriticalSec.Lock();

#ifdef LMEMORY_ENABLE_EXCEPTIONS
		try {
#endif
			KAllocUnit* pkUnit = FindAllocUnit(pvMemory);

			//size_t stSizeinBytes;
			//if (eEventType == LMEMOP_DELETE)
			//{
			//	stSizeinBytes = pkUnit->m_stSizeRequested;
			//}

			if (pkUnit == NULL)
			{
				assert(pkUnit != NULL);
#ifdef LMEMORY_ENABLE_EXCEPTIONS
				throw "无效的内存地址";
#endif
				m_CriticalSec.Unlock();
				return;
			}

			// If you hit this LMEMASSERT, then the allocation unit that is about 
			// to be deleted requested an initial size that doesn't match
			// what is currently the 'size' argument for deallocation.
			// This is most commonly caused by the lack of a virtual destructor
			// for a class that is used polymorphically in an array.
			//if (stSizeinBytes!= L_MEM_DEALLOC_SIZE_DEFAULT)
			//	LMEMASSERTULT(stSizeinBytes== pkUnit->m_stSizeRequested, pkUnit);

			// If you hit this LMEMASSERT, then the allocation unit that is about 
			// to be deallocated is damaged.
			BOOL bValidateAllocUnit = ValidateAllocUnit(pkUnit);
			assert(bValidateAllocUnit);

			// If you hit this LMEMASSERT, then this deallocation was made from a 
			// source that isn't setup to use this memory tracking software, 
			// use the stack frame to locate the source and include
			// KMemManager.h
			assert(eEventType != LMEMOP_UNKNOWN);

			// If you hit this LMEMASSERT, you were trying to deallocate RAM that 
			// was not allocated in a way that is compatible with the 
			// deallocation method requested. In other words, you have a 
			// allocation/deallocation mismatch.
			assert((eEventType == LMEMOP_DELETE && pkUnit->m_eAllocType == LMEMOP_NEW) 
				|| (eEventType == LMEMOP_DELETEARR && pkUnit->m_eAllocType == LMEMOP_NEWARR) 
				|| (eEventType == LMEMOP_FREE && pkUnit->m_eAllocType == LMEMOP_MALLOC) 
				|| (eEventType == LMEMOP_FREE && pkUnit->m_eAllocType == LMEMOP_REALLOC) 
				|| (eEventType == LMEMOP_REALLOC && pkUnit->m_eAllocType == LMEMOP_MALLOC) 
				|| (eEventType == LMEMOP_REALLOC && pkUnit->m_eAllocType == LMEMOP_REALLOC)
				|| (eEventType ==  LMEMOP_ALIGNEDFREE && pkUnit->m_eAllocType ==  LMEMOP_ALIGNEDMALLOC) 
				|| (eEventType ==  LMEMOP_ALIGNEDFREE && pkUnit->m_eAllocType ==  LMEMOP_ALIGNEDREALLOC) 
				|| (eEventType ==  LMEMOP_ALIGNEDREALLOC &&  pkUnit->m_eAllocType ==  LMEMOP_ALIGNEDMALLOC) 
				|| (eEventType ==  LMEMOP_ALIGNEDREALLOC && pkUnit->m_eAllocType ==  LMEMOP_ALIGNEDREALLOC)
				);


			// update allocation unit
			//			LMemOp eDeallocType = eEventType;
			//float fDeallocTime = fTime;

			// Determine how much memory was actually set
			size_t stSizeUnused = MemoryBytesWithPattern(pvMemory,
				pkUnit->m_stSizeRequested);
			m_stUnusedButAllocatedMemory += stSizeUnused;

			// Save the thread id that freed the memory
			//			unsigned long ulFreeThreadId = GetCurrentThreadId();

			//if (m_bCheckArrayOverruns)
			//{
			//	// If you hit this LMEMASSERT, you have code that overwrites
			//	// either before or after the range of an allocation.
			//	// Check the pkUnit for information about which allocation
			//	// is being overwritten. 
			//	bool bCheckForArrayOverrun = CheckForArrayOverrun(pvMemory,
			//		pkUnit->m_stAlignment, pkUnit->m_stSizeRequested);
			//	LMEMASSERTULT(!bCheckForArrayOverrun, pkUnit);

			//	if (stSizeinBytes!= L_MEM_DEALLOC_SIZE_DEFAULT)
			//	{
			//		stSizeinBytes= PadForArrayOverrun(pkUnit->m_stAlignment,
			//			stSizeinBytes);
			//	}
			//}

			// Perform the actual deallocation
			//m_pkActualAllocator->Deallocate(pvMemory,
			//	eEventType, stSizeinBytes);
			free(pvMemory);
			// Remove this allocation unit from the hash table
			RemoveAllocUnit(pkUnit);

			// update summary statistics
			--m_stActiveAllocationCount;
			m_stActiveMemory -= pkUnit->m_stSizeAllocated;

			// Validate every single allocated unit in memory
			//	if (m_bAlwaysValidateAll)
			//	{
			//		BOOL bValidateAllAllocUnits = ValidateAllAllocUnits();
			//		assert(bValidateAllAllocUnits);
			//	}

			// Write out the freed memory to the memory log
			//LogAllocUnit(pkUnit, MEM_LOG_COMPLETE, "\t", eDeallocType,
			//	fDeallocTime, ulFreeThreadId, stSizeUnused);

			// Recycle the allocation unit
			// Add it to the front of the reservoir 
			pkUnit->m_kFLF.Reset();
			pkUnit->m_pkNext = m_pkReservoir;
			m_pkReservoir = pkUnit;

			// Validate every single allocated unit in memory
			if (m_bAlwaysValidateAll)
			{
				BOOL bValidateAllAllocUnits = ValidateAllAllocUnits();
				assert(bValidateAllAllocUnits);
			}
			m_CriticalSec.Unlock();

#ifdef LMEMORY_ENABLE_EXCEPTIONS
		}
		catch(const char *err)
		{
			// Deal with the errors

		}
#endif
	}
}

void KMemoryManagerTrack::ResetSummaryStats()
{
	//	m_fPeakMemoryTime = 0.0f;
	//	m_fPeakAllocationCountTime = 0.0f;
	m_stActiveMemory = 0;
	m_stPeakMemory = 0;
	m_stAccumulatedMemory = 0;
	m_stUnusedButAllocatedMemory = 0;

	m_stActiveAllocationCount = 0;
	m_stPeakAllocationCount = 0;
	m_stAccumulatedAllocationCount = 0;
}

void KMemoryManagerTrack::MemoryFillWithPattern(void* pvMemory, size_t stSizeinBytes)
{
	BYTE* pcMemArray = (BYTE*)pvMemory;
	for (unsigned int ui = 0; ui < stSizeinBytes; ui++)
	{
		pcMemArray[ui] = (BYTE)FILL_BYTE;
	}
}

size_t KMemoryManagerTrack::MemoryBytesWithPattern(void* pvMemory, size_t stSizeinBytes) const
{
	unsigned char* pcMemArray = (unsigned char*) pvMemory;
	size_t numBytes = 0;
	for (unsigned int ui = 0; ui < stSizeinBytes; ui++)
	{
		if (pcMemArray[ui] == (BYTE)FILL_BYTE)
		{
			numBytes++;
		}
	}

	return numBytes;
}

KMemoryManagerTrack::KAllocUnit* KMemoryManagerTrack::FindAllocUnit(const void* pvMem) const
{
	assert(pvMem != NULL);
	UINT uiHashIndex = AddressToHashIndex(pvMem);
	KAllocUnit* pkUnit = m_pkActiveMem[uiHashIndex];
	while(pkUnit)
	{
		if (pkUnit->m_pvMem == pvMem) 
			return pkUnit;
		pkUnit = pkUnit->m_pkNext;
	}
	return NULL;
}

void KMemoryManagerTrack::InsertAllocUnit(KMemoryManagerTrack::KAllocUnit* pkUnit)
{
	assert(pkUnit != NULL && pkUnit->m_pvMem != NULL);
	UINT uiHashIndex = AddressToHashIndex(pkUnit->m_pvMem);

	// Remap the new allocation unit to the head of the hash entry
	if (m_pkActiveMem[uiHashIndex])
	{
		assert(m_pkActiveMem[uiHashIndex]->m_pkPrev == NULL);
		m_pkActiveMem[uiHashIndex]->m_pkPrev = pkUnit;
	}

	assert(pkUnit->m_pkNext == NULL);
	pkUnit->m_pkNext = m_pkActiveMem[uiHashIndex];
	pkUnit->m_pkPrev = NULL;
	m_pkActiveMem[uiHashIndex] = pkUnit;
}
//---------------------------------------------------------------------------
void KMemoryManagerTrack::RemoveAllocUnit(KMemoryManagerTrack::KAllocUnit* pkUnit)
{
	assert(pkUnit != NULL && pkUnit->m_pvMem != NULL);
	UINT uiHashIndex = AddressToHashIndex(pkUnit->m_pvMem);

	// If you hit this LMEMASSERT, somehow we have emptied the
	// hash table for this bucket. This should not happen
	// and is indicative of a serious error in the memory
	// tracking infrastructure.
	assert(m_pkActiveMem[uiHashIndex] != NULL);

	if (m_pkActiveMem[uiHashIndex] == pkUnit)
	{
		assert(pkUnit->m_pkPrev == NULL);
		m_pkActiveMem[uiHashIndex] = pkUnit->m_pkNext;

		if (m_pkActiveMem[uiHashIndex])
			m_pkActiveMem[uiHashIndex]->m_pkPrev = NULL;
	}
	else
	{      
		if (pkUnit->m_pkPrev)
		{
			assert(pkUnit->m_pkPrev->m_pkNext == pkUnit);
			pkUnit->m_pkPrev->m_pkNext = pkUnit->m_pkNext;
		}
		if (pkUnit->m_pkNext)
		{
			assert(pkUnit->m_pkNext->m_pkPrev == pkUnit);
			pkUnit->m_pkNext->m_pkPrev = pkUnit->m_pkPrev;
		}
	}
}

BOOL KMemoryManagerTrack::ValidateAllocUnit(const KMemoryManagerTrack::KAllocUnit* pkUnit) const
{
	if (pkUnit->m_stAllocationID > m_stCurrentAllocID)
		return FALSE;

	if (pkUnit->m_stSizeAllocated < pkUnit->m_stSizeRequested)
		return FALSE;

	if (pkUnit->m_stSizeAllocated == 0 ||  pkUnit->m_stSizeRequested == 0)
		return FALSE;

	if (pkUnit->m_pvMem == NULL)
		return FALSE;

	if (pkUnit->m_pkNext != NULL && pkUnit->m_pkNext->m_pkPrev != pkUnit)
		return FALSE;

	return TRUE;
}

BOOL KMemoryManagerTrack::ValidateAllAllocUnits() const
{
	//UINT uiActiveCount = 0;
	//for (UINT uiHashIndex = 0; uiHashIndex < HASH_SIZE; uiHashIndex++)
	//{
	//	KAllocUnit* pkUnit = m_pkActiveMem[uiHashIndex];
	//	KAllocUnit* pkPrev = NULL;

	//	while(pkUnit)
	//	{
	//		if (!ValidateAllocUnit(pkUnit))
	//			return FALSE;

	//		if (pkUnit->m_pkPrev != pkPrev)
	//			return FALSE;

	//		// continue to the next unit
	//		pkPrev = pkUnit;
	//		pkUnit = pkUnit->m_pkNext;
	//		uiActiveCount++;
	//	}
	//}   

	//if (uiActiveCount != m_stActiveAllocationCount)
	//	return FALSE;

	return TRUE;
}

void KMemoryManagerTrack::GrowReservoir()
{
	assert(!m_pkReservoir);
	m_pkReservoir = (KAllocUnit*)malloc(sizeof(KAllocUnit)*m_stReservoirGrowBy);

	// If you hit this LMEMASSERT, then the memory manager failed to allocate 
	// internal memory for tracking the allocations
	assert(m_pkReservoir != NULL);

	if (m_pkReservoir == NULL) 
	{
		throw "内存跟踪系统创建内部跟踪数据失败.";
	}

	// sizeof(KAllocUnit*) 为自身跟踪单元内存地址大小																				
	m_stActiveTrackerOverhead += sizeof(KAllocUnit) * m_stReservoirGrowBy + sizeof(KAllocUnit*);

	if (m_stActiveTrackerOverhead > m_stPeakTrackerOverhead)
	{
		m_stPeakTrackerOverhead = m_stActiveTrackerOverhead;
	}
	m_stAccumulatedTrackerOverhead += sizeof(KAllocUnit) * m_stReservoirGrowBy + sizeof(KAllocUnit*);

	// Build a linked-list of the elements in the reservoir
	// Initialize the allocation units
	for (unsigned int i = 0; i < m_stReservoirGrowBy-1; i++)
	{
		m_pkReservoir[i].m_kFLF.Reset();
		m_pkReservoir[i].m_pkNext = &m_pkReservoir[i+1];
	}
	m_pkReservoir[m_stReservoirGrowBy-1].m_kFLF.Reset();
	m_pkReservoir[m_stReservoirGrowBy-1].m_pkNext = NULL;

	// Add this address to the reservoir buffer so it can be freed later
	KAllocUnit **pkTemp = (KAllocUnit**) realloc(
		m_ppkReservoirBuffer, 
		(m_stReservoirBufferSize + 1) * sizeof(KAllocUnit*));

	assert(pkTemp != NULL);
	if (pkTemp)
	{
		m_ppkReservoirBuffer = pkTemp;
		m_ppkReservoirBuffer[m_stReservoirBufferSize] = m_pkReservoir;
		m_stReservoirBufferSize++;
	}
	else
	{
		throw "内存跟踪系统创建内部跟踪数据失败.";
	}
}

void KMemoryManagerTrack::ReportLeakMemory() const
{
	FILE *fp = fopen("uimemdbg.log", "wt+");
	assert(fp);
	if( !fp )
		return;

	// Any leaks?

	// Header
	SYSTEMTIME Time;
	GetLocalTime(&Time);
	fprintf(fp, " ---------------------------------------------------------------------------------------------------------------------------------- \r\n");
	fprintf(fp, "|                            User Interface Leak Memory Dump:  %04d/%02d/%02d %02d:%02d:%02d                                            |\r\n", Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond);
	fprintf(fp, " ---------------------------------------------------------------------------------------------------------------------------------- \r\n");
	fprintf(fp, "\r\n");
	fprintf(fp, "\r\n");

	if( m_stActiveAllocationCount )
	{
		fprintf(fp, "%d Memory leak found:\r\n", m_stActiveAllocationCount);
	}
	else
	{
		fprintf(fp, "Congratulations! No memory leaks found!\r\n");
	}
	fprintf(fp, "\r\n");

	if( m_stActiveAllocationCount )
	{
		fprintf(fp, "Alloc  Thread   Addr       Size                                      \r\n");
		fprintf(fp, " ID     ID     Actual     Actual     Unused    Method   Allocated by \r\n");
		fprintf(fp, "------ ------ ---------- ---------- ---------- -------- ----------------------------------------------------------------------- \r\n");

		for( UINT i = 0; i < HASH_SIZE; i++ )
		{
			KAllocUnit* pUnit = m_pkActiveMem[i];
			while( pUnit )
			{
				fprintf(fp, "%06d %06d 0x%08X 0x%08X 0x%08X %-8s   %s(%d) %s\r\n",
					pUnit->m_stAllocationID, pUnit->m_ulAllocThreadId,
					(size_t) pUnit->m_pvMem, pUnit->m_stSizeAllocated,
					/*gpDbgMemMgr->calcUnused(ptr)*/0,
					MemEventTypeToString(pUnit->m_eAllocType),
					pUnit->m_kFLF.m_pcFile, pUnit->m_kFLF.m_uiLine, pUnit->m_kFLF.m_pcFunc);

				pUnit = pUnit->m_pkNext;
			}
		}
	}

	fclose(fp);
}

#endif 