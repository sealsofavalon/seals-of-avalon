#ifndef __UMEMORY_H__
#define __UMEMORY_H__

#ifdef _DEBUG
#define UI_MEMDBG
#endif 


#if defined(UI_MEMDBG) 	// 跟踪内存分配情况
enum LMemOp
{
	LMEMOP_UNKNOWN = 0,
	LMEMOP_MALLOC,
	LMEMOP_REALLOC,
	LMEMOP_FREE,
	LMEMOP_NEW,
	LMEMOP_DELETE,
	LMEMOP_NEWARR,
	LMEMOP_DELETEARR,
	LMEMOP_ALIGNEDMALLOC,
	LMEMOP_ALIGNEDREALLOC,
	LMEMOP_ALIGNEDFREE,
};
void* LMallocDbg(size_t size, const char* sFile, int iLine, const char* sFunction);
void* LReallocDbg(void* pMem, size_t size, const char* sFile, int iLine, const char* sFunction);
void	LFreeDbg(void* pMem, LMemOp op);
BOOL	LNewMemTrack(const char* sFile,DWORD dwLine, const char* sFunc);
void* LNewMemDbg(size_t sz,LMemOp op);
#define UMalloc(size)		LMallocDbg(size,__FILE__, __LINE__, __FUNCTION__)
#define URealloc(pMem,size)	LReallocDbg(pMem,size,__FILE__, __LINE__, __FUNCTION__) 
#define UFree(pMem)			LFreeDbg(pMem, LMEMOP_FREE)
inline void *operator new(size_t size)		{ return LNewMemDbg(size,LMEMOP_NEW);	}
inline void *operator new[](size_t size)	{ return LNewMemDbg(size, LMEMOP_NEWARR); }
inline void operator delete(void *pvMem)	{ LFreeDbg(pvMem, LMEMOP_DELETE);	}
inline void operator delete[](void *pvMem)	{ LFreeDbg(pvMem, LMEMOP_DELETEARR); }
#define new LNewMemTrack(__FILE__, __LINE__, __FUNCTION__)?NULL:new
#else	// 
#define UMalloc malloc
#define URealloc realloc
#define	UFree free

inline void *operator new(size_t size) { return UMalloc(size); }
inline void *operator new[](size_t size) { return UMalloc(size); }
inline void operator delete(void *pvMem)	{ UFree(pvMem);	}
inline void operator delete[](void *pvMem) { UFree(pvMem); }
#endif 


#if defined(UI_MEMDBG)




//ftp://ftp.GraphicsPapers.com/pub/ProgrammingTools/MemoryManagers/

class KMemoryManagerTrack
{
	enum
	{
		HASH_SIZE = 1<<12,
		FILL_BYTE = 0xBD,
	};
	enum
	{
		ALIGNMENT = 4,
	};
	// For storing File, Line, and Function of allocation event.   
	struct  KFLF
	{
		KFLF()
		{
			Reset();
		}
		KFLF(const char* pcFile, unsigned int uiLine, const char* pcFunc):
		m_pcFile(pcFile), m_pcFunc(pcFunc), m_uiLine(uiLine)
		{
		}
		void Set(const char* pcFile, unsigned int uiLine, const char* pcFunc)
		{
			m_pcFile = pcFile;
			m_uiLine = uiLine;
			m_pcFunc = pcFunc;
		}
		void Reset() 
		{
			m_pcFile = "";
			m_pcFunc = "";
			m_uiLine = 0;
		}
		BOOL IsValid() const
		{
			return (m_uiLine != 0);
		}

		// 考虑到DLL 动态卸载的问题, 一旦卸载后, 文件名,函数名地址将不再有效,在这种情况下,
		// 更为安全的方法是直接保存字符串内容,但是所带来的开销就大了,因此我们暂时不想考虑
		// 动态卸载DLL 带来的问题,将来如果必要支持这种情况的时候,再来.
#if  LTH_MEM_FLF_SUPPORT_DLL_DETACH
		char m_pcFile[256];
		char m_pcFunc[256];
#else 
		const char* m_pcFile;
		const char* m_pcFunc;
#endif
		unsigned int m_uiLine;
	};

	struct  KAllocUnit
	{
		size_t m_stAllocationID;
		void* m_pvMem;
		size_t m_stAlignment;
		size_t m_stSizeRequested;
		size_t m_stSizeAllocated;

		// hash table support
		KAllocUnit* m_pkPrev;
		KAllocUnit* m_pkNext;

		unsigned long m_ulAllocThreadId;
		LMemOp m_eAllocType;
		KFLF m_kFLF;
		void Reset()
		{
			m_stAllocationID = (size_t) -1;
			m_stAlignment = 0;
			m_ulAllocThreadId = 0;
			m_pvMem = NULL;
			m_stSizeRequested = 0;
			m_stSizeAllocated = 0;
			m_kFLF.Reset();
			m_eAllocType = LMEMOP_UNKNOWN;
			//				m_fAllocTime = -FLT_MAX;
			m_pkPrev = NULL;
			m_pkNext = NULL;

		}
	};
public:
	KMemoryManagerTrack();
	~KMemoryManagerTrack();
	void BeginNewOpTrack(const char *sFile , int iLine, const char* sFunction )
	{
		// TODO stl xmemory 31 line:
		/*
		template<class _Ty> inline
		_Ty _FARQ *_Allocate(_SIZT _Count, _Ty _FARQ *)
		{	// allocate storage for _Count elements of type _Ty
		return ((_Ty _FARQ *)operator new(_Count * sizeof (_Ty)));
		}
		这个调用会导致 new 操作不经过 BeginNewOpTrack 而直接Allocate ,因此,Lock 操作不能放在这里.
		*/
		//m_CriticalSec.Lock();
		m_NewOpFLF.Set(sFile,iLine,sFunction);
	}
public:
	virtual BOOL Initialize();
	virtual void Shutdown();
	virtual void* Allocate(size_t stSize, size_t Alignment, LMemOp eEventType, BOOL bProvideAccurateSizeOnDeallocate = FALSE,
		const char *pcSourceFile = NULL, int iSourceLine = -1, const char* pcFunction = NULL);

	virtual void* Reallocate(void* pvMem, size_t stSize, size_t Alignment, LMemOp eEventType, BOOL bProvideAccurateSizeOnDeallocate = FALSE,
		size_t stSizeCurrent = -1, const char *pcSourceFile = NULL, int iSourceLine = -1, const char* pcFunction = NULL);

	// TODO 需要重新设计这个接口,我们并不想依赖一个stSizeinBytes 参数,形同多余
	virtual void Deallocate(void* pvMem, LMemOp eEventType);
private:
	void ResetSummaryStats();
	void MemoryFillWithPattern(void* pvMemory, size_t stSizeinBytes);
	size_t MemoryBytesWithPattern(void* pvMemory, size_t stSizeinBytes) const;
	void InsertAllocUnit(KAllocUnit* pkUnit);
	void RemoveAllocUnit(KAllocUnit* pkUnit);
	KAllocUnit* FindAllocUnit(const void* pvMem) const;
	BOOL ValidateAllocUnit(const KAllocUnit* pkUnit) const;
	BOOL ValidateAllAllocUnits() const;
	inline UINT AddressToHashIndex(const void* pvAddress) const
	{
		// Use the address to locate the hash index. Note that we shift off the 
		// lower four bits. This is because most allocated addresses will be on 
		// four-, eight- or even sixteen-byte boundaries. If we didn't do this, 
		// the hash index would not have very good coverage.

		// The size_t may be cast to an unsigned int as long as ms_uiHashSize
		// is less than the largest unsigned int.
		return (UINT)(((size_t)pvAddress) >> 4) & (HASH_SIZE - 1);
	}

	void GrowReservoir();
	inline const char* MemEventTypeToString(LMemOp eEventType) const
	{
		switch (eEventType)
		{
		case LMEMOP_UNKNOWN:
			return "UNKNOWN";
		case LMEMOP_NEW:
			return "new";
		case LMEMOP_NEWARR:
			return "new []";
		case LMEMOP_DELETE:
			return "delete";
		case LMEMOP_DELETEARR:
			return "delete []";
		case LMEMOP_MALLOC:
			return "malloc";
		case LMEMOP_REALLOC:
			return "realloc";
		case LMEMOP_FREE:
			return "free";
		case LMEMOP_ALIGNEDMALLOC:
			return "AlignedMalloc";
		case LMEMOP_ALIGNEDREALLOC:
			return "AlignedRealloc";
		case LMEMOP_ALIGNEDFREE:
			return "AlignedFree";
		default:
			assert(!"Unknown Memory operator value!");
		};

		return NULL;
	}
	virtual void ReportLeakMemory() const;
private:
	size_t m_stActiveMemory;				// 有效内存单元字节数目
	size_t m_stPeakMemory;					// 总共内存字节
	size_t m_stAccumulatedMemory;			// in bytes
	size_t m_stUnusedButAllocatedMemory;	// 空闲的内存字节数目

	size_t m_stActiveAllocationCount;		// 有效内存单元数目
	size_t m_stPeakAllocationCount;			
	size_t m_stAccumulatedAllocationCount;	// 总共分配的单元数目

	size_t m_stActiveTrackerOverhead;
	size_t m_stPeakTrackerOverhead;
	size_t m_stAccumulatedTrackerOverhead;

	BOOL m_bAlwaysValidateAll;
	KAllocUnit* m_pkReservoir;
	size_t m_stReservoirGrowBy;
	KAllocUnit** m_ppkReservoirBuffer;
	size_t m_stReservoirBufferSize;
	KAllocUnit* m_pkActiveMem[HASH_SIZE];

	size_t m_stCurrentAllocID;	
	LCriticalSection m_CriticalSec;
	KFLF m_NewOpFLF;		// for new delete.
};

extern KMemoryManagerTrack g_MemMgr;
#endif // UI_MEMDBG


// small memory pool
class UChunkAllocator
{
	struct Block
   {
      Block *next;
      BYTE *data;
      int pos;
      Block(int size)
	  {
		  data = (BYTE*)UMalloc(size);
		  assert(data);
	  }
      ~Block()
	  {
		  if (data)
		  {
			  UFree(data);
		  }
	  }
   };

public:
	UChunkAllocator(int nGrowSize = 1024*32)
	{
		m_nGrowSize = nGrowSize;
		curBlock = NULL;
	}
	~UChunkAllocator()
	{
		Free();
	}
	void* Alloc(int size)
	{
		assert(size <= m_nGrowSize);
		if(!curBlock || (size + curBlock->pos) > m_nGrowSize)
		{
			Block *temp = new Block(m_nGrowSize);
			temp->next = curBlock;
			temp->pos = 0;
			curBlock = temp;
		}
		void *ret = curBlock->data + curBlock->pos;
		curBlock->pos += (size + 3) & ~3; // dword align
		return ret;
	}
	void Free()
	{
		while(curBlock)
		{
			Block *temp = curBlock->next;
			delete curBlock;
			curBlock = temp;
		}
	}

private:
	int m_nGrowSize;
	Block *curBlock;
 
};

#endif 