#ifndef __UPRIVATE_H__
#define __UPRIVATE_H__

class UDesktop;
class USystem;
extern USystem* g_USystem;
extern UDesktop* g_Desktop;
extern HWND g_hWnd;
extern HWND GetMainWnd();



//! 临界
class LCriticalSection
{
public:
	inline LCriticalSection()
	{ 

		InitializeCriticalSection(&m_CriticalSection); 
	}
	~LCriticalSection()		{ DeleteCriticalSection(&m_CriticalSection); }
	void Lock()		
	{
		EnterCriticalSection(&m_CriticalSection);

	}
	void Unlock()
	{
		LeaveCriticalSection(&m_CriticalSection);

	}
protected:
	CRITICAL_SECTION m_CriticalSection;

};



// UFont 现在的实现是完全面向UNICODE字符集的, 因此, 我们需要自行实现一个快速的ANSI-UNICODE
// 的转换方法. 当然你也可以完全不考虑性能.....

// FrameAllocator 不能长期维护内存, 引用FrameAllocator的Lock Unlock 必须按照先进后出的方式来操作.
class FrameAllocator
{
public:
	FrameAllocator()
	{
		m_CurBufPos = 0;
	}
	~FrameAllocator()
	{
	}
	void Resize(int newSize)
	{
		m_Buffer.resize(newSize);
	}
	// for safety. 字符转换不见得都只有渲染线程才会调用.
	BYTE* Lock(int size, int& header)			
	{
		m_cs.Lock();
		int bufsize = m_CurBufPos + size;

		if (bufsize > (int)m_Buffer.size() )
		{
			m_Buffer.resize(int(bufsize*1.6f));
		}
		header = m_CurBufPos;
		m_CurBufPos+= size;
		return &m_Buffer[m_CurBufPos];
	}
	void Unlock(int header)
	{
		m_CurBufPos = header;
		m_cs.Unlock();
	}

private:
	LCriticalSection m_cs;	// for thread safe
	INT m_CurBufPos;
	std::vector<BYTE> m_Buffer;
};
extern FrameAllocator* g_pFrameAllocator;

template<class T>
class UFrameMarker
{
	int m_Marker;
	T* m_Buffer;
public:
	UFrameMarker( int size ) 
	{
		assert(g_pFrameAllocator && size);
		m_Buffer = (T*)g_pFrameAllocator->Lock(size*sizeof(T), m_Marker);
	}
	~UFrameMarker()
	{
		assert(g_pFrameAllocator);
		g_pFrameAllocator->Unlock(m_Marker);
	}
	const T& operator[]( int n ) const { return m_Buffer[n]; }
	T& operator[]( int n )	{ return m_Buffer[n];}
	operator T*() { return m_Buffer; }
	operator const T*() const { return m_Buffer; }
};


inline void RevisionPath(char* pPath, int nMaxLen = -1)
{
	if(nMaxLen == -1)
	{
		nMaxLen = strlen(pPath);
	}else
	{
		assert(nMaxLen <= (int)strlen(pPath));
	}
	for (int i = 0; i < nMaxLen; i++)
	{
		if (pPath[i] == '/')
		{
			pPath[i] = '\\';
		}
	}
}

inline int MB2WC(const char* pszMbText, int nMBChars, WCHAR* pwszBuf, int nWideChars)
{
	return MultiByteToWideChar(CP_ACP, 0, pszMbText, nMBChars, pwszBuf, nWideChars);
}

inline int WC2MB(const WCHAR* pszWideText, int nWideChars, char* pszBuf, int nChars)
{
	return WideCharToMultiByte(CP_ACP, 0, pszWideText, nWideChars, pszBuf, nChars, NULL, NULL);
}


#endif 