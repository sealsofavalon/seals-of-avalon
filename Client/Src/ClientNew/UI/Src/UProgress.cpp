#include "StdAfx.h"
#include "UProgress.h"
#include "UControl.h"
#include "USystem.h"

UIMP_CLASS(UProgressCtrl, UControl);

void UProgressCtrl::StaticInit()
{
	UREG_PROPERTY("Skin", UPT_STRING, UFIELD_OFFSET(UProgressCtrl, m_strBitmapFile));
	UREG_PROPERTY("Vertical", UPT_BOOL, UFIELD_OFFSET(UProgressCtrl, m_bVertical));	
	UREG_PROPERTY("BarPosition", UPT_FLOAT, UFIELD_OFFSET(UProgressCtrl, m_Progress));
	UREG_PROPERTY("showProgess", UPT_BOOL, UFIELD_OFFSET(UProgressCtrl, m_bShowtext));
}
UProgressCtrl::UProgressCtrl(void)
{
	m_bVertical = FALSE ;//默认水平
	m_bShowtext = TRUE ;
	//m_text = NULL;
}

UProgressCtrl::~UProgressCtrl(void)
{
	m_ProgressSkin = NULL;
}

BOOL UProgressCtrl::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (!m_strBitmapFile.Empty())
	{
		m_ProgressSkin = sm_System->LoadSkin(m_strBitmapFile.GetBuffer());
	}
  
	return  TRUE ;
}
void UProgressCtrl::OnDestroy()
{
	m_ProgressSkin = NULL;
	UControl::OnDestroy();
}
void UProgressCtrl::OnRender(const UPoint& offset, const URect &updateRect)
{
	URect LT_Rect;    // 左边或者顶部边
	URect RB_Rect;    // 右边或者底部边
	URect Center_Rect = updateRect; //中间可以高亮部分
	URect LbgkRect ;  //显示高亮区域

	UINT LT_wOrH;
	UINT RB_wOrH;

	if (m_ProgressSkin != NULL && m_ProgressSkin->GetItemNum() >=4)
	{
		if (m_ProgressSkin->GetSkinItem(LeftOrTopSkin) != NULL || m_ProgressSkin->GetSkinItem(RightOrButtomSkin) != NULL)
		{
			if (m_bVertical)
			{
				LT_wOrH = m_ProgressSkin->GetSkinItem(LeftOrTopSkin)->SubSec.GetHeight();
				RB_wOrH = m_ProgressSkin->GetSkinItem(RightOrButtomSkin)->SubSec.GetHeight();

				LT_Rect = URect(updateRect.left,updateRect.top,updateRect.right, LT_wOrH);
				RB_Rect = URect(updateRect.left, updateRect.bottom - RB_wOrH,updateRect.right,updateRect.bottom);

				Center_Rect = URect(Center_Rect.left, Center_Rect.top +LT_wOrH, Center_Rect.right, Center_Rect.bottom - RB_wOrH);
				
			}else
			{
				LT_wOrH = m_ProgressSkin->GetSkinItem(LeftOrTopSkin)->SubSec.GetWidth();
				RB_wOrH = m_ProgressSkin->GetSkinItem(RightOrButtomSkin)->SubSec.GetWidth();

				LT_Rect = URect(updateRect.left,updateRect.top,LT_wOrH,updateRect.bottom);
				RB_Rect = URect(updateRect.right - RB_wOrH, updateRect.top,updateRect.right,updateRect.bottom);

				Center_Rect = URect(Center_Rect.left + LT_wOrH,Center_Rect.top,Center_Rect.right - RB_wOrH,Center_Rect.bottom);
			}

			sm_UiRender->DrawSkin(m_ProgressSkin,LeftOrTopSkin,LT_Rect);
			sm_UiRender->DrawSkin(m_ProgressSkin,RightOrButtomSkin,RB_Rect);
		}
	}

	if (m_bVertical)
	{
		LbgkRect = URect(Center_Rect.left,Center_Rect.top,Center_Rect.right, Center_Rect.GetSize().y * m_Progress+Center_Rect.top);
	}else
	{	
		LbgkRect = URect(Center_Rect.left,Center_Rect.top,Center_Rect.left + Center_Rect.GetSize().x * m_Progress,Center_Rect.bottom);	
	}

	if (m_ProgressSkin == NULL)
	{
		sm_UiRender->DrawSkin(m_Style->m_spSkin,NormalCenterSkin,Center_Rect);
		sm_UiRender->DrawSkin(m_Style->m_spSkin,LightCenterSkin,LbgkRect);
	}else
	{
		sm_UiRender->DrawSkin(m_ProgressSkin,NormalCenterSkin,Center_Rect);
		sm_UiRender->DrawSkin(m_ProgressSkin,LightCenterSkin,LbgkRect);
	}

	
	if (m_Style->mBorder)
	{
		sm_UiRender->DrawRect(updateRect,m_Style->mBorderColor); //边框
	}
	if (m_bShowtext)
	{
		if (m_text.GetLength() == 0 && m_Progress >= 0.0f)
		{
			char ProgressText[4];
			sprintf(ProgressText,"%d%%",(int)(m_Progress*100.0f));
			UDrawText(sm_UiRender,m_Style->m_spFont,offset,m_Size,m_Style->m_FontColor,ProgressText,UT_CENTER);
		}else
		{
			if (m_text.GetLength() == 0)
			{
				return;
			}
			UDrawText(sm_UiRender,m_Style->m_spFont,offset,m_Size,m_Style->m_FontColor,m_text.GetBuffer(),UT_CENTER);
		}
	}
}
void  UProgressCtrl::SetText(int a, int b)
{
	char buf[_MAX_PATH];
	sprintf(buf,"%d / %d",a,b);
	m_text.Set(buf);
	SetPos((float)a / (float)b);
}
void  UProgressCtrl::SetPos(float pos)
{
	if (pos > 1.0f)
	{
		pos = 1.0f;
	}
	if (pos < 0.0f)
	{
		pos = 0.0f ;
	}
	m_Progress = pos;
}
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UProgressBar_Skin, UControl);
void UProgressBar_Skin::StaticInit()
{
	UREG_PROPERTY("BarSkin", UPT_STRING, UFIELD_OFFSET(UProgressBar_Skin, m_strSkinFile));
	UREG_PROPERTY("BarHeadSkin", UPT_STRING, UFIELD_OFFSET(UProgressBar_Skin, m_strBarHSkinFile));
}
UProgressBar_Skin::UProgressBar_Skin()
{
	m_fProgressPos = 0.f;
	m_agePos = 0;
	m_lifePos = 1;
	m_bShowText = true;
	mAlpha = 255;

	m_BarSkin = NULL;
	m_BarHeadSkin = NULL;
	mbBadEnding = false;
}
UProgressBar_Skin::~UProgressBar_Skin()
{}
BOOL UProgressBar_Skin::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_strSkinFile.GetBuffer())
	{
		if (m_BarSkin == NULL)
		{
			m_BarSkin = sm_System->LoadSkin(m_strSkinFile.GetBuffer());
		}
	}

	if (m_strBarHSkinFile.GetBuffer())
	{
		if (m_BarHeadSkin == NULL)
		{
			m_BarHeadSkin = sm_System->LoadSkin(m_strBarHSkinFile.GetBuffer());
		}
	}
	return TRUE;
}
void UProgressBar_Skin::OnDestroy()
{
	m_fProgressPos = 0.f;
	m_agePos = 0;
	m_lifePos = 1;
	UControl::OnDestroy();
}
void UProgressBar_Skin::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_BarSkin == NULL)
	{
		return UControl::OnRender(offset,updateRect);
	}

	if (m_BarSkin->GetItemNum() > 1)
		sm_UiRender->DrawSkin(m_BarSkin, 1, updateRect, LCOLOR_ARGB(mAlpha, 255, 255, 255));


	URect rect = m_BarSkin->GetSkinItem(0)->SubSec;
	UPoint off;
	int Width = min(m_Size.x , rect.GetWidth());
	int Height = min(m_Size.y , rect.GetHeight());
	off.x = (m_Size.x - Width)/2;
	off.y = (m_Size.y - Height)/2;
	URect DrawBarRect(offset + off, UPoint( Width, Height ));
	DrawBarRect.SetWidth( Width * m_fProgressPos );

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	g_pRenderInterface->SetScissorRect(DrawBarRect);
	rect.SetPosition(offset + off);
	rect.SetSize(Width, Height);

	if ( mbBadEnding )
	{
		if (m_BarSkin->GetItemNum() > 3)
			sm_UiRender->DrawSkin(m_BarSkin, 3, rect, LCOLOR_ARGB(mAlpha, 255, 255, 255));
		else
			sm_UiRender->DrawSkin(m_BarSkin, 0, rect, LCOLOR_ARGB(mAlpha, 255, 255, 255));
	}
	else
		sm_UiRender->DrawSkin(m_BarSkin, 0, rect, LCOLOR_ARGB(mAlpha, 255, 255, 255));

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);

	if (m_BarHeadSkin && m_fProgressPos < 1.0f)
	{
		UPoint HeadPos = offset;
		HeadPos.x += off.x + Width * m_fProgressPos - m_BarHeadSkin->GetSkinItem(0)->SubSec.GetWidth() / 2;
		HeadPos.y += (m_Size.y - m_BarHeadSkin->GetSkinItem(0)->SubSec.GetHeight())/2;
		sm_UiRender->DrawSkin(m_BarHeadSkin, 0, HeadPos);
	}

	if (m_BarSkin->GetItemNum() > 2 && m_fProgressPos >= 1.0f)
	{
		rect = m_BarSkin->GetSkinItem(2)->SubSec;
		int Width = rect.GetWidth();
		int Height = rect.GetHeight();
		off.x = (m_Size.x - Width)/2;
		off.y = (m_Size.y - Height)/2;
		if (mbBadEnding)
		{
			if (m_BarSkin->GetItemNum() > 4)
				sm_UiRender->DrawSkin(m_BarSkin, 4, offset + off, LCOLOR_ARGB(mAlpha, 255, 255, 255));
			else
				sm_UiRender->DrawSkin(m_BarSkin, 2, offset + off, LCOLOR_ARGB(mAlpha, 255, 255, 255));
		}
		else
			sm_UiRender->DrawSkin(m_BarSkin, 2, offset + off, LCOLOR_ARGB(mAlpha, 255, 255, 255));
	}


	if (m_Style->m_spFont && m_bShowText)
	{
		UStringW rendertext(mShowText.GetBuffer());
		int height = m_Style->m_spFont->GetHeight();
		URect DrawRect;
		DrawRect.pt0.x = offset.x;
		DrawRect.pt0.y = offset.y + (m_Size.y - height) / 2;
		DrawRect.pt1.x = offset.x + m_Size.x;
		DrawRect.pt1.y = offset.y + m_Size.y;
		USetTextEdge(TRUE);
		UDrawText(sm_UiRender, m_Style->m_spFont, DrawRect.GetPosition(), DrawRect.GetSize(), LCOLOR_WHITE, rendertext, UT_CENTER);
		USetTextEdge(FALSE);
	}
}
void UProgressBar_Skin::OnMouseEnter(const UPoint& position, UINT flags)
{
	UControl::OnMouseEnter(position, flags);
}
void UProgressBar_Skin::OnMouseLeave(const UPoint& position, UINT flags)
{
	UControl::OnMouseLeave(position, flags);
}
void UProgressBar_Skin::SetProgressPos( unsigned int agePos,  unsigned int lifePos)
{
	m_agePos = agePos;
	m_lifePos = lifePos;
	UpdateText();
}
void UProgressBar_Skin::SetProgressAge( unsigned int age)
{
	m_agePos = age;
	UpdateText();
}
void UProgressBar_Skin::SetProgressLife( unsigned int life)
{
	m_lifePos = life;
	UpdateText();
}
void UProgressBar_Skin::SetProgressPos(float pos)
{
	m_agePos =  unsigned int(m_lifePos * pos);
	UpdateText();
}
void UProgressBar_Skin::SetSkin(const char* fileName)
{
	if (fileName)
	{
		m_BarSkin = sm_System->LoadSkin(fileName);
	}
}
void UProgressBar_Skin::UpdateText()
{
	char buf[256];	
	if(!m_lifePos)
		return;
	m_fProgressPos = (float)m_agePos/(float)m_lifePos;
	if(m_lifePos < 1000000)
		sprintf(buf, "%u/%u", m_agePos, m_lifePos);
	else{
		sprintf(buf, "%5.2f%%", m_fProgressPos * 100);
	}
	if(m_fProgressPos > 1.f) m_fProgressPos = 1.f;
	if(m_fProgressPos < 0.f) m_fProgressPos = 0.f;
	mShowText.Set(buf);
}

void UProgressBar_Skin::SetBadEnding()
{
	mbBadEnding = true;
}
void UProgressBar_Skin::SetNorEnding()
{
	mbBadEnding = false;
}