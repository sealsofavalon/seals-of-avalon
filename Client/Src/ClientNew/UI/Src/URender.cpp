#include "StdAfx.h"
#include "URender.h"
#include "UControl.h"
#include "URichAtom.h"
#include "USystem.h"
#include "d3duirender.h"

BOOL g_bEdge = FALSE;
UColor g_cEdgeColor = UColor(0,0,0,255);
void UIAPI USetTextEdge(BOOL bEdge, UColor& EdgeColor)
{
	g_bEdge = bEdge;
	g_cEdgeColor = EdgeColor;
}
void UIAPI UDrawText(URender* plugin, IUFont* pFont, const UPoint& offset, const UPoint& size, const UColor& color, const WCHAR* text, EUTextAlignment ta, float depth, float scale, bool bHori )
{
	assert(plugin && pFont);
	if (pFont == NULL || text == NULL)
	{
		return;
	}

	if (bHori)
	{
		int textWidth = pFont->GetStrWidth(text);
		UPoint start(0,0);

		if (ta == UT_CENTER)
		{
			start.Set( ( size.x - textWidth) / 2, 0 );
		}else if (ta == UT_RIGHT)
		{
			start.Set( size.x - textWidth, 0 );
		}

		// If the text is longer then the box size, (it'll get clipped) so
		// force Left Justify
		if( textWidth > size.x )
			start.Set( 0, 0 );

		// center the vertical
		int FontHeight = pFont->GetHeight();
		if(FontHeight > size.y)
			start.y = 0 - ((FontHeight - size.y) / 2) ;
		else
			start.y = ( size.y - FontHeight ) / 2;


		start += offset;
		int len = pFont->GetBreakPos(text, wcslen(text), size.x, true);
		bool bClip = len < wcslen(text);
		if (bClip) --len;

		//int len = wcslen(text);
		if(g_bEdge){
			int off = 1;//max(pFont->GetHeight()/10, 1);
			plugin->DrawTextN(pFont,start - UPoint(0, off),text ,len,NULL,g_cEdgeColor, depth, scale);
			plugin->DrawTextN(pFont,start - UPoint(0, -off),text ,len,NULL,g_cEdgeColor, depth, scale);
			plugin->DrawTextN(pFont,start - UPoint(off, 0),text ,len,NULL,g_cEdgeColor, depth, scale);
			plugin->DrawTextN(pFont,start - UPoint(-off, 0),text ,len,NULL,g_cEdgeColor, depth, scale);
		}
		plugin->DrawTextN(pFont, start, text, len, NULL,  color, depth, scale);
		if (bClip)
		{
			start.x += pFont->GetStrNWidth(text, len);
			plugin->DrawTextN(pFont, start, L"...", 3, NULL, color);
		}
	}
	else
	{
		UPoint start(0,0);

		int textwidth = pFont->GetStrNWidth(text, 1);
		int textHeight = (pFont->GetHeight() + 2) * wcslen(text);

		if (ta == UT_CENTER)
		{
			start.Set( ( size.x - textwidth) / 2, 0 );
		}else if (ta == UT_RIGHT)
		{
			start.Set( size.x - textwidth, 0 );
		}
		if( textwidth > size.x )
			start.Set( 0, 0 );

		if(textHeight > size.y)
			start.y = 0 - ((textHeight - size.y) / 2) ;
		else
			start.y = ( size.y - textHeight ) / 2;

		UDrawStaticText(plugin, pFont,start + offset, textwidth, color, text, UT_LEFT, 2);
	}
	
}

// TODO Move TO UControl
void UDrawText(URender* plugin, IUFont* pFont, 
			   const UPoint& offset, const UPoint& size,const UColor& color,
			   const char *text, EUTextAlignment ta, float depth, float scale, bool bHori )
{
	assert(plugin && pFont);
	if (pFont == NULL || text == NULL)
	{
		return;
	}
	int nLen = strlen(text);
	if (nLen == 0)
	{
		return;
	}
	UFrameMarker<WCHAR> Marker(nLen + 1);	// max size to fit.
	int nRet = MultiByteToWideChar(CP_UTF8, 0, text, nLen, Marker, nLen);
	Marker[nRet] =0;
	UDrawText(plugin, pFont, offset, size, color, Marker, ta, depth, scale, bHori);
}
void UIAPI UDrawTextEx(URender* plugin, IUFont* pFont, const UPoint& offset, const UPoint& size, const UColor& color_left,const UColor& color_right, const char* text_Left,const char *text_Right)
{
	UDrawText(plugin,pFont,offset,size,color_left,text_Left,UT_LEFT);
	UDrawText(plugin,pFont,offset,size,color_right,text_Right,UT_RIGHT);
}
void UIAPI UDrawTextEx(URender* plugin, IUFont* pFont, const UPoint& offset, const UPoint& size, const UColor& color_left,const UColor& color_right, const WCHAR* text_Left,const WCHAR *text_Right)
{
	UDrawText(plugin,pFont,offset,size,color_left,text_Left,UT_LEFT);
	UDrawText(plugin,pFont,offset,size,color_right,text_Right,UT_RIGHT);
}

void DrawTextAtom(URender* pRender, UControlStyle* pCtrlStl, const WCHAR* pStr, float fAlpha, BOOL sel, 
				  UINT start, UINT end, Atom *atom, 
				  Line *line, const UPoint& offset, float depth)
{
	IUFont* font = atom->style->font->fontRes;
	int xOff = 0;
	if(start != atom->textStart)
	{
		const WCHAR* buff = pStr + atom->textStart;
		xOff += font->GetStrNWidth(buff, start - atom->textStart);
	}

	UPoint drawPoint(offset.x + atom->xStart + xOff, offset.y + atom->yStart);

	UColor color;
	if(atom->url)
	{ 
		if(atom->url->mouseDown)
			color = atom->style->linkColorHL;
		else
			color = atom->style->linkColor;
	}
	else
		color = atom->style->color;

	const WCHAR* tmp = pStr + start;
	UINT tmpLen = end - start;

	if(!sel)
	{
		if(atom->style->shadowOffset.x || atom->style->shadowOffset.y)
		{
			UColor shadowColor = atom->style->shadowColor;
			shadowColor.a = (INT)(fAlpha * shadowColor.a);
			UPoint drawpos = drawPoint;
			for(int i = 0 ; i < 2 ; i++)
			{
				drawpos += atom->style->shadowOffset;
				pRender->DrawTextN(font, drawpos ,tmp, tmpLen, NULL, shadowColor, depth);
			}
		}

		color.a = (INT)(fAlpha * color.a);
		if(g_bEdge){
			int off = max(font->GetHeight()/10, 1);
			pRender->DrawTextN(font,drawPoint - UPoint(0, 1),tmp ,end-start,NULL,g_cEdgeColor, depth);
			pRender->DrawTextN(font,drawPoint - UPoint(0, -1),tmp ,end-start,NULL,g_cEdgeColor, depth);
			pRender->DrawTextN(font,drawPoint - UPoint(1, 0),tmp ,end-start,NULL,g_cEdgeColor, depth);
			pRender->DrawTextN(font,drawPoint - UPoint(-1, 0),tmp ,end-start,NULL,g_cEdgeColor, depth);
		}
		pRender->DrawTextN(font, drawPoint, tmp, end-start, NULL, color, depth);

		//if the atom was "clipped", see if we need to draw a "..." at the end
		if (atom->isClipped)
		{
			UPoint p2 = drawPoint;
			p2.x += font->GetStrNWidthPrecise( tmp, tmpLen);
			pRender->DrawTextN(font, p2, "...", 3, NULL, color, depth);
		}
	}
	else
	{
		URect rect;
		rect.left = drawPoint.x;
		rect.top = line->y + offset.y;
		rect.right =  font->GetStrNWidth(tmp, tmpLen) + 1 + rect.left;
		rect.bottom = line->height + 1 + rect.top;

		pRender->DrawRectFill(rect, pCtrlStl->mFillColorHL);
		pRender->DrawTextN(font, drawPoint, tmp, tmpLen, NULL, pCtrlStl->m_FontColorHL, depth);

		//if the atom was "clipped", see if we need to draw a "..." at the end
		if (atom->isClipped)
		{
			UPoint p2 = drawPoint;
			p2.x +=  font->GetStrNWidthPrecise( tmp, end - atom->textStart);
			pRender->DrawTextN(font, p2, "...", 3, NULL, pCtrlStl->m_FontColorHL, depth);
		}
	}

	if(atom->url && !atom->url->noUnderline)
	{
		drawPoint.y += atom->baseLine + 2;
		UPoint p2 = drawPoint;
		p2.x +=  font->GetStrNWidthPrecise( tmp, end - atom->textStart);
		pRender->DrawLine(drawPoint, p2, color);
	}
}

int UIAPI UDrawStaticText(URender * plugin,IUFont * pFont,const UPoint &offset,const int& width,const UColor& color,const WCHAR* wstr, EUTextAlignment ta, UINT pCharoffset, float depth)
{
	assert(plugin && pFont && wstr);
	if (pFont == NULL || wcslen(wstr) == 0)
		return 0;
	UControlStyle* pStyle = UControl::sm_System->FindControlStyle("default");

	RichEdit tRichEdit;
	char buf[256];std::string texthead;
	sprintf( buf, "<color:%0*X",2, color.r);texthead += buf;
	sprintf( buf, "%0*X",2, color.g);texthead += buf;
	sprintf( buf, "%0*X",2, color.b);texthead += buf;
	sprintf( buf, "%0*X>",2, color.a);texthead += buf;
	switch(ta)
	{
	case UT_CENTER:
		texthead += "<center>";
		break;
	case UT_LEFT:
		texthead += "<left>";
		break;
	case UT_RIGHT:
		texthead += "<right>";
		break;
	}
	D3D9Font* pd3d9Font = (D3D9Font*)pFont;
	sprintf(buf, "<font:%s:%d>", pd3d9Font->m_FaceName.c_str(), pd3d9Font->m_FontSize);
	texthead += buf;
	UStringW WStr(texthead.c_str());
	WStr.Append(wstr);
	tRichEdit.SetTextLineSpace(pCharoffset);
	tRichEdit.Parse(pStyle, width, WStr, NULL, NULL);
	{
		BitmapRef* pBitmapRefList = tRichEdit.mBitmapRefList;
		BitmapRef* pBitmapAtom = pBitmapRefList;
		URect AtomRect;
		while (pBitmapAtom)
		{
			AtomRect.left = pBitmapAtom->left;
			AtomRect.top = pBitmapAtom->top;
			AtomRect.right = pBitmapAtom->right;
			AtomRect.bottom = pBitmapAtom->bottom;
			AtomRect.Offset(offset);

			plugin->DrawImage(pBitmapAtom->bitmap->bitmapObject, AtomRect, pBitmapAtom->uvRect);

			pBitmapAtom = pBitmapAtom->next;
		}
	}
	{
		Line* pLine = tRichEdit.mLineList;
		while (pLine)
		{
			for(Atom* pAtom = pLine->atomList; pAtom; pAtom = pAtom->next)
			{
				//pAtom->style->font->fontRes = pFont;
				//pAtom->style->color = color;
				DrawTextAtom(plugin, pStyle, WStr, 1.f, 
					FALSE, pAtom->textStart, pAtom->textStart + pAtom->len, pAtom, pLine, offset);
			}
			pLine = pLine->next;
		}
	}
	return tRichEdit.mMaxY;

	//int textlen = wcslen(wstr);
	//if(pFont->GetHeight() > width && textlen > 1)
	//	return 0;

	//int line = 0, p  = 0, textstart = 0;
	//while(p < textlen)
	//{
	//	int textpos = 0, w = 0;
	//	while(TRUE)
	//	{
	//		int l = pFont->GetStrNWidth(wstr+p,1);
	//		if (p++ == textlen || textpos + l  > width) break;
	//		textpos += l;w++;
	//	}
	//	UPoint TextPos = offset;
	//	TextPos.y += line * (pFont->GetHeight() + pCharoffset);
	//	if (ta == UT_CENTER) TextPos.x = ( width - textpos ) / 2;
	//	else if (ta == UT_RIGHT) TextPos.x =  width - textpos;


	//	if(g_bEdge){
	//		int off = max(pFont->GetHeight()/10, 1);
	//		plugin->DrawTextN(pFont,TextPos - UPoint(0, off),wstr + textstart ,w,NULL,g_cEdgeColor, depth);
	//		plugin->DrawTextN(pFont,TextPos - UPoint(0, -off),wstr + textstart ,w,NULL,g_cEdgeColor, depth);
	//		plugin->DrawTextN(pFont,TextPos - UPoint(off, 0),wstr + textstart ,w,NULL,g_cEdgeColor, depth);
	//		plugin->DrawTextN(pFont,TextPos - UPoint(-off, 0),wstr + textstart ,w,NULL,g_cEdgeColor, depth);
	//	}
	//	plugin->DrawTextN(pFont, TextPos, wstr + textstart, w, NULL, color, depth);
	//	line++;
	//	textstart = p;
	//}
	//return line * (pFont->GetHeight() + pCharoffset);
}
int UIAPI UDrawStaticText(URender * plugin,IUFont * pFont,const UPoint &offset,const int& width,const UColor& color,UStringW &wstr, EUTextAlignment ta, UINT pCharoffset, float depth)
{
	assert(plugin && pFont && wstr);
	if (pFont == NULL || wstr.GetLength() == 0){
		return 0;
	}
	return UDrawStaticText(plugin, pFont, offset, width, color, wstr.GetBuffer(), ta, pCharoffset, depth);
}

URect UIAPI UDrawResizeBitmap(URender * plugin , USkinPtr skin, const UPoint & pos , const UPoint & size, float depth)
{		
	URect renderrect;
	URect rect[9];
	UPoint _pos,_size;

	if (skin && skin->GetItemNum() == 9)
	{	
		renderrect.SetPosition(pos - skin->GetSkinItemRect(0)->GetSize());
		renderrect.SetWidth(size.x + skin->GetSkinItemRect(0)->GetWidth() + skin->GetSkinItemRect(1)->GetWidth());
		renderrect.SetHeight(size.y + skin->GetSkinItemRect(0)->GetHeight() + skin->GetSkinItemRect(2)->GetHeight());
		//TOP LEFT
		{
			_pos = renderrect.GetPosition();
			_size = skin->GetSkinItemRect(0)->GetSize();
			rect[0] = URect(_pos,_size);
		}
		//TOP RIGHT
		{
			_size = skin->GetSkinItemRect(1)->GetSize();
			_pos = renderrect.GetPosition();
			rect[1] = URect(_pos,_size);
			rect[1].Offset( renderrect.GetSize().x - _size.x , 0 );
		}
		//BOTTOM LEFT
		{
			_size = skin->GetSkinItemRect(2)->GetSize();
			_pos = renderrect.GetPosition();
			rect[2] = URect(_pos,_size);
			rect[2].Offset( 0 , renderrect.GetSize().y - _size.y );
		}
		//BOTTOM RIGHT
		{
			_size = skin->GetSkinItemRect(3)->GetSize();
			_pos = renderrect.GetPosition();
			rect[3] = URect(_pos,_size);
			rect[3].Offset( renderrect.GetSize().x - _size.x , renderrect.GetSize().y - _size.y );
		}
		//TOP
		{
			_size = skin->GetSkinItemRect(4)->GetSize();
			_pos = renderrect.GetPosition();
			rect[4] = URect(_pos,_size);
			rect[4].Offset( skin->GetSkinItemRect(0)->GetSize().x , 0);
			rect[4].SetWidth(renderrect.GetWidth() - skin->GetSkinItemRect(0)->GetSize().x - skin->GetSkinItemRect(1)->GetSize().x);
		}
		//RIGHT
		{
			_size = skin->GetSkinItemRect(5)->GetSize();
			_pos = renderrect.GetPosition();
			rect[5] = URect(_pos,_size);
			rect[5].Offset( renderrect.GetSize().x - _size.x , skin->GetSkinItemRect(1)->GetSize().y );
			rect[5].SetHeight(renderrect.GetHeight() - skin->GetSkinItemRect(0)->GetSize().y - skin->GetSkinItemRect(2)->GetSize().y);
		}
		//BOTTOM
		{
			_size = skin->GetSkinItemRect(6)->GetSize();
			_pos = renderrect.GetPosition();
			rect[6] = URect(_pos,_size);
			rect[6].Offset( skin->GetSkinItemRect(2)->GetSize().x , renderrect.GetSize().y - _size.y );
			rect[6].SetWidth(renderrect.GetWidth() - skin->GetSkinItemRect(2)->GetSize().x - skin->GetSkinItemRect(3)->GetSize().x);
		}
		//LEFT
		{
			_size = skin->GetSkinItemRect(7)->GetSize();
			_pos = renderrect.GetPosition();
			rect[7] = URect(_pos,_size);
			rect[7].Offset( 0 , skin->GetSkinItemRect(0)->GetSize().y );
			rect[7].SetHeight(renderrect.GetHeight() - skin->GetSkinItemRect(1)->GetSize().y - skin->GetSkinItemRect(3)->GetSize().y);
		}
		//MIDDLE
		{
			_size = skin->GetSkinItemRect(8)->GetSize();
			_pos = renderrect.GetPosition();
			rect[8] = URect(_pos,_size);
			rect[8].Offset( skin->GetSkinItemRect(0)->GetSize() );
			rect[8].SetSize(renderrect.GetWidth() - rect[2].GetSize().x - rect[3].GetSize().x,renderrect.GetHeight() - rect[1].GetSize().y - rect[3].GetSize().y);
		}
	}
	for (int i = 0 ; i < 9 ; i++)
	{
		plugin->DrawSkin(skin, i, rect[i], -1, 0, depth);
	}
	return rect[8];
}
void UIAPI UDrawResizeImage(EUResizeImageType type,URender * plugin,USkin * skin , const UPoint & pos,const UPoint &size, float depth)
{
	if (plugin == NULL || skin == NULL)
	{
		return;
	}
	switch (type)
	{
	case RESIZE_HEIGHT:
		{
			if (skin->GetItemNum() < 3)
			{
				return;
			}
			URect renderRectTop(pos,skin->GetSkinItemRect(0)->GetSize());
			renderRectTop.SetWidth(size.x);
			plugin->DrawSkin(skin,0,renderRectTop,-1, 0 , depth);

			URect renderRectBottom(pos,skin->GetSkinItemRect(2)->GetSize());		
			renderRectBottom.SetWidth(size.x);
			renderRectBottom.Offset(0,size.y - renderRectBottom.GetHeight());
			plugin->DrawSkin(skin,2,renderRectBottom,-1, 0 , depth);

			URect renderRectMiddle(pos,skin->GetSkinItemRect(1)->GetSize());
			UINT height = size.y - renderRectTop.GetHeight() - renderRectBottom.GetHeight();
			renderRectMiddle.SetHeight(height);
			renderRectMiddle.SetWidth(size.x);
			renderRectMiddle.Offset(0,renderRectTop.GetHeight());
			plugin->DrawSkin(skin,1,renderRectMiddle,-1, 0 , depth);
		}
		break;
	case RESIZE_WIDTH:
		{
			if (skin->GetItemNum() < 3)
			{
				return;
			}
			URect renderRectLeft(pos ,skin->GetSkinItemRect(0)->GetSize());
			renderRectLeft.SetHeight(size.y);
			plugin->DrawSkin(skin,0,renderRectLeft,-1, 0 , depth);

			URect renderRectRight(pos ,skin->GetSkinItemRect(2)->GetSize());		
			renderRectRight.SetHeight(size.y);
			renderRectRight.Offset(size.x - renderRectRight.GetWidth(),0);
			plugin->DrawSkin(skin,2,renderRectRight,-1, 0 , depth);

			URect renderRectMiddle(pos ,skin->GetSkinItemRect(1)->GetSize());
			UINT width = size.x - renderRectLeft.GetWidth() - renderRectRight.GetWidth();
			renderRectMiddle.SetWidth(width);
			renderRectMiddle.SetHeight(size.y);
			renderRectMiddle.Offset(renderRectLeft.GetWidth(),0);
			plugin->DrawSkin(skin,1,renderRectMiddle,-1, 0 , depth);
		}
		break;
	case RESIZE_WANDH:
		{
			if (skin->GetItemNum() < 9)
			{
				return;
			}	
			URect renderrect(pos,size);
			URect rect[9];
			UPoint _pos,_size;

			//TOP LEFT
			{
				_pos = renderrect.GetPosition();
				_size = skin->GetSkinItemRect(0)->GetSize();
				rect[0] = URect(_pos,_size);
			}
			//TOP RIGHT
			{
				_size = skin->GetSkinItemRect(1)->GetSize();
				_pos = renderrect.GetPosition();
				rect[1] = URect(_pos,_size);
				rect[1].Offset( renderrect.GetSize().x - _size.x , 0 );
			}
			//BOTTOM LEFT
			{
				_size = skin->GetSkinItemRect(2)->GetSize();
				_pos = renderrect.GetPosition();
				rect[2] = URect(_pos,_size);
				rect[2].Offset( 0 , renderrect.GetSize().y - _size.y );
			}
			//BOTTOM RIGHT
			{
				_size = skin->GetSkinItemRect(3)->GetSize();
				_pos = renderrect.GetPosition();
				rect[3] = URect(_pos,_size);
				rect[3].Offset( renderrect.GetSize().x - _size.x , renderrect.GetSize().y - _size.y );
			}
			//TOP
			{
				_size = skin->GetSkinItemRect(4)->GetSize();
				_pos = renderrect.GetPosition();
				rect[4] = URect(_pos,_size);
				rect[4].Offset( skin->GetSkinItemRect(0)->GetSize().x , 0);
				rect[4].SetWidth(renderrect.GetWidth() - skin->GetSkinItemRect(0)->GetSize().x - skin->GetSkinItemRect(1)->GetSize().x);
			}
			//RIGHT
			{
				_size = skin->GetSkinItemRect(5)->GetSize();
				_pos = renderrect.GetPosition();
				rect[5] = URect(_pos,_size);
				rect[5].Offset( renderrect.GetSize().x - _size.x , skin->GetSkinItemRect(1)->GetSize().y );
				rect[5].SetHeight(renderrect.GetHeight() - skin->GetSkinItemRect(0)->GetSize().y - skin->GetSkinItemRect(2)->GetSize().y);
			}
			//BOTTOM
			{
				_size = skin->GetSkinItemRect(6)->GetSize();
				_pos = renderrect.GetPosition();
				rect[6] = URect(_pos,_size);
				rect[6].Offset( skin->GetSkinItemRect(2)->GetSize().x , renderrect.GetSize().y - _size.y );
				rect[6].SetWidth(renderrect.GetWidth() - skin->GetSkinItemRect(2)->GetSize().x - skin->GetSkinItemRect(3)->GetSize().x);
			}
			//LEFT
			{
				_size = skin->GetSkinItemRect(7)->GetSize();
				_pos = renderrect.GetPosition();
				rect[7] = URect(_pos,_size);
				rect[7].Offset( 0 , skin->GetSkinItemRect(0)->GetSize().y );
				rect[7].SetHeight(renderrect.GetHeight() - skin->GetSkinItemRect(1)->GetSize().y - skin->GetSkinItemRect(3)->GetSize().y);
			}
			//MIDDLE
			{
				_size = skin->GetSkinItemRect(8)->GetSize();
				_pos = renderrect.GetPosition();
				rect[8] = URect(_pos,_size);
				rect[8].Offset( skin->GetSkinItemRect(0)->GetSize() );
				rect[8].SetSize(renderrect.GetWidth() - rect[2].GetSize().x - rect[3].GetSize().x,renderrect.GetHeight() - rect[1].GetSize().y - rect[3].GetSize().y);
			}	
			for (int i = 0 ; i < 9 ; i++)
			{
				plugin->DrawSkin(skin,i,rect[i], -1, 0 , depth);
			}
		}
		break;
	}
}