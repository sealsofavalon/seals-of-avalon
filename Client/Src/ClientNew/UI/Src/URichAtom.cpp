#include "StdAfx.h"
#include "UBase.h"
#include "URichAtom.h"
#include "UFont.h"
#include "USystem.h"
#include "URender.h"
// unicode 
void RichEditTrace(const WCHAR* pfmt, ...)
{
	WCHAR Buf[1024];
	
	va_list args;
	va_start( args, pfmt );
	int len = _vsnwprintf(Buf, 1024, pfmt, args);
	va_end(args);
	CHAR szBuf[1024];
	len = WideCharToMultiByte(CP_UTF8, 0, Buf, len,szBuf, 1024, NULL, NULL);
	szBuf[len] = '\0';
	UTRACE(szBuf);
}

#ifdef _DEBUG
#define RICHTRACE RichEditTrace
#else
#define RICHTRACE __noop
#endif 


int GetLineBreakCP(IUFont* pFont, const WCHAR* wstr, int len, int width, BOOL breakOnWhitespace)
{
	// Some early out cases.
	if(len==0)
		return 0;

	int ret = 0;
	int lastws = 0;
	WCHAR c;
	int charCount = 0;

	for( charCount=0; charCount < len; charCount++)
	{
		c = wstr[charCount];
		if(c == 0)
			break;

		if(c == L'\t')
			c = L' ';

		if(!pFont->IsValidChar(c))
		{
			ret++;
			continue;
		}

		if(c == L' ')
			lastws = ret+1;

		int nCharWidth = pFont->GetCharWidth(c);
		int nCharXInc = pFont->GetCharXIncrement(c);
		if(nCharWidth > width || nCharXInc > width)
		{
			if(lastws && breakOnWhitespace)
				return lastws;
			return ret;
		}

		width -= nCharXInc;

		ret++;
	}
	return ret;
}


int GetStrNWidthPrecise(IUFont* pFont, const WCHAR *str, int n)
{
	assert(pFont);
	if (str == NULL || str[0] == NULL || n == 0)   
		return 0;

	int totWidth = 0;
	WCHAR curChar;
	int charCount = 0;

	for(charCount = 0; charCount < n; charCount++)
	{
		curChar = str[charCount];
		if(curChar == 0)
			break;

		if(pFont->IsValidChar(curChar))
		{
			totWidth += pFont->GetCharXIncrement(curChar);
		}
		else if (curChar == L'\t')
		{
			int wsxinc = pFont->GetCharXIncrement(L' ');
			totWidth += wsxinc * UTAB_WIDTH;
		}
	}

	WCHAR endChar = str[__min(charCount,n-1)];

	if (pFont->IsValidChar(endChar))
	{
		int eWidth = pFont->GetCharWidth(endChar);
		int eInc = pFont->GetCharXIncrement(endChar);
		if (eWidth > eInc)
			totWidth += (eWidth - eInc);
	}

	return totWidth;
}

RichEdit::RichEdit()
{
	mBitmapList = 0;
	mBitmapRefList = 0;
	mFontList = 0;
	mDirty = TRUE;
	mLineList = 0;
	mTagList = 0;
	mHitURL = 0;
	mOnMoveURL = 0;
	mMaxY = 0;
	mLineSpace = 4;

	m_ContentBitMapURLFun = NULL ;
}


void RichEdit::freeLineBuffers()
{
	mViewChunker.Free();
	mLineList = NULL;
	mBitmapRefList = NULL;
	mTagList = NULL;
	mHitURL = 0;
	mOnMoveURL = 0;
	mDirty = TRUE;
}

void RichEdit::freeResource()
{
	for(Font* walk = mFontList; walk; walk = walk->next)
	{
		walk->fontRes = NULL;
	}

	for(Bitmap* bwalk = mBitmapList; bwalk; bwalk = bwalk->next)
		bwalk->bitmapObject = NULL;

	mFontList = NULL;
	mBitmapList = NULL;
	mResourceChunker.Free();
	freeLineBuffers();
}



static BOOL scanforchar(const WCHAR *pSubStr, int& idx, WCHAR c)
{
	UINT startidx = idx;
	while(pSubStr[idx] != c && pSubStr[idx] && pSubStr[idx] != L':' && pSubStr[idx] != L'>' && pSubStr[idx] != L'\n')
		idx++;
	return pSubStr[idx] == c && startidx != idx;
}


static INT getHexVal(WCHAR c)
{
	if(c >= L'0' && c <= L'9')
		return c - L'0';
	else if(c >= L'A' && c <= L'Z')
		return c - L'A' + 10;
	else if(c >= L'a' && c <= L'z')
		return c - L'a' + 10;
	return -1;
}

void RichEdit::Parse(const UControlStyle* pCtrlStyle, int nWidth, const UStringW& strbuf, const char* pRtfFilePath, ContentBitMapURLFun BitMapURLFun)
{
	assert(pCtrlStyle);
	// 重置缓存.
	freeLineBuffers();
	mDirty = FALSE;
	mScanPos = 0;

	m_ContentBitMapURLFun = BitMapURLFun ;

	mLineList = NULL;
	mLineInsert = &mLineList;

	mCurStyle = allocStyle(NULL);
	mCurStyle->font = allocFont(pCtrlStyle->m_FontFace.GetBuffer(),pCtrlStyle->m_FontSize);
	if(!mCurStyle->font)
	{
		UTRACE("RichEdit create font failed.");
		return;
	}
	mCurStyle->color = pCtrlStyle->m_FontColor;
	mCurStyle->shadowColor = pCtrlStyle->m_FontColor;
	mCurStyle->shadowOffset.Set(0,0);
	mCurStyle->linkColor = pCtrlStyle->m_FontColorUser0;
	mCurStyle->linkColorHL = pCtrlStyle->m_FontColorUser1;

	UINT width = nWidth;

	mCurLMargin = 0;
	mCurRMargin = width;
	mCurJustify = UT_LEFT;
	mCurDiv = 0;
	mCurY = 0;
	mCurX = 0;
	mCurClipX = 0;
	mLineAtoms = NULL;
	mLineAtomPtr = &mLineAtoms;

	mSentinel.left = width;
	mSentinel.top = 0;
	mSentinel.right = width;
	mSentinel.bottom = 0x7FFFFF;
	mSentinel.nextBlocker = NULL;
	mLineStart = 0;
	mEmitAtoms = 0;
	mMaxY = 0;
	mEmitAtomPtr = &mEmitAtoms;

	mBlockList = &mSentinel;

	mTabStops = 0;
	mCurTabStop = 0;
	mTabStopCount = 0;
	mCurURL = 0;

	m_pwstrBuffer = &strbuf;
	// 解析文本.
	CHAR szRootPath[MAX_PATH];
	szRootPath[0] = '\0';
	if (pRtfFilePath && pRtfFilePath[0] != 0)
	{
		strcpy(szRootPath, pRtfFilePath);
		char* pSlash = strrchr(szRootPath, '\\');
		if (pSlash == NULL)
		{
			pSlash = strrchr(szRootPath, '/');
		}
		if (pSlash)
		{
			*++pSlash = '\0';
		}else
		{
			szRootPath[0] = '\0';
		}
	}
	Font *nextFont;
	LineTag *nextTag;
	//Style *newStyle;

	UINT textStart;
	UINT len;
	int idx;
	int sizidx;

	const int nStrLen = strbuf.GetLength();
	for(;;)
	{
		const WCHAR curChar = strbuf[mScanPos];

		if(!curChar)
			break;

		if(curChar == L'\n')
		{
			textStart = mScanPos;
			len = 1;
			mScanPos++;
			processEmitAtoms();
			emitNewLine(textStart);
			mCurDiv = 0;
			continue;
		}

		if(curChar == L'\t')
		{
			textStart = mScanPos;
			len = 1;
			mScanPos++;
			processEmitAtoms();

			int nWhiteSpaceWidth = mCurStyle->font->fontRes->GetCharXIncrement(L' ');
			mCurX += nWhiteSpaceWidth*UTAB_WIDTH;
			/*if(mTabStopCount)
			{
				if(mCurTabStop < mTabStopCount)
				{
					if(mCurX < mTabStops[mCurTabStop])
						mCurX = mTabStops[mCurTabStop];
				}
				mCurTabStop++;
			}*/
			continue;
		}

		if(curChar == L'<')
		{
			// it's probably some kind of tag:
			const WCHAR* pSubStr = &strbuf[mScanPos];
			if(wcsnicmp(pSubStr +1, L"br>", 3) == 0)
			{
				mScanPos += 4;
				len = 4;
				textStart = mScanPos + 4;
				processEmitAtoms();
				emitNewLine(textStart);
				mCurDiv = 0;
				continue;
			}

			if(!wcsnicmp(pSubStr+1, L"font:", 5))
			{
				// scan for the second colon...
				// at each level it should drop out to the text case below...
				idx = 6;
				if(!scanforchar(pSubStr, idx, L':'))
					goto textemit;

				sizidx = idx + 1;
				if(!scanforchar(pSubStr, sizidx, L'>'))
					goto textemit;

				UINT size = _wtoi(pSubStr + idx + 1);
				if(!size || size > 64)
				{
					WCHAR Error[64];
					wcsncpy(Error, pSubStr, 64);
					RICHTRACE(L"%s size is invalidate", Error);
					goto textemit;
				}
				textStart = mScanPos + 6;
				len = idx - 6;

				mScanPos += sizidx + 1;

				{
					UFrameMarker<CHAR> szFontFace(len*2 +1);
					int Ret = WideCharToMultiByte(CP_UTF8, 0, pSubStr + 6, len, szFontFace, len*2, NULL, NULL);
					szFontFace[Ret] = '\0';
					nextFont = allocFont(szFontFace, size);
				}
				if(nextFont)
				{
					if(mCurStyle->used)
						mCurStyle = allocStyle(mCurStyle);
					mCurStyle->font = nextFont;
				}
				continue;
			}

			if(!wcsnicmp( pSubStr + 1, L"tag:", 4 ) )
			{
				idx = 5;
				if ( !scanforchar( pSubStr, idx, L'>' ) )
					goto textemit;
				UINT tagId = _wtoi( pSubStr + 5 );
				nextTag = allocLineTag( tagId );

				mScanPos += idx + 1;
				continue;
			}

			if(!wcsnicmp(pSubStr +1, L"color:", 6))
			{
				idx = 7;
				if(!scanforchar(pSubStr, idx, L'>'))
					goto textemit;
				if(idx != 13 && idx != 15)
					goto textemit;
				UColor color;

				color.r = getHexVal(pSubStr[7]) * 16 + getHexVal(pSubStr[8]);
				color.g = getHexVal(pSubStr[9]) * 16 + getHexVal(pSubStr[10]);
				color.b = getHexVal(pSubStr[11]) * 16 + getHexVal(pSubStr[12]);
				if(idx == 15)
					color.a = getHexVal(pSubStr[13]) * 16 + getHexVal(pSubStr[14]);
				else
					color.a = 255;
				mScanPos += idx + 1;

				if(mCurStyle->used)		// 如果当前样式已经使用, 分配新的样式, 否则重写当前样式.
					mCurStyle = allocStyle(mCurStyle);
				mCurStyle->color = color;

				continue;
			}

			if(!wcsnicmp(pSubStr +1, L"shadowcolor:", 12))
			{
				idx = 13;
				if(!scanforchar(pSubStr, idx, '>'))
					goto textemit;
				if(idx != 19 && idx != 21)
					goto textemit;
				UColor color;

				color.r   = getHexVal(pSubStr[13]) * 16 + getHexVal(pSubStr[14]);
				color.g = getHexVal(pSubStr[15]) * 16 + getHexVal(pSubStr[16]);
				color.b  = getHexVal(pSubStr[17]) * 16 + getHexVal(pSubStr[18]);
				if(idx == 21)
					color.a = getHexVal(pSubStr[19]) * 16 + getHexVal(pSubStr[20]);
				else
					color.a = 255;
				mScanPos += idx + 1;

				if(mCurStyle->used)
					mCurStyle = allocStyle(mCurStyle);
				mCurStyle->shadowColor = color;

				continue;
			}

			if(!wcsnicmp(pSubStr +1, L"linkcolor:", 10))
			{
				idx = 11;
				if(!scanforchar(pSubStr, idx, L'>'))
					goto textemit;
				if(idx != 17 && idx != 19)
					goto textemit;
				UColor color;

				color.r   = getHexVal(pSubStr[11]) * 16 + getHexVal(pSubStr[12]);
				color.g = getHexVal(pSubStr[13]) * 16 + getHexVal(pSubStr[14]);
				color.b  = getHexVal(pSubStr[15]) * 16 + getHexVal(pSubStr[16]);
				if(idx == 19)
					color.a = getHexVal(pSubStr[17]) * 16 + getHexVal(pSubStr[18]);
				else
					color.a = 255;
				mScanPos += idx + 1;

				if(mCurStyle->used)
					mCurStyle = allocStyle(mCurStyle);
				mCurStyle->linkColor = color;

				continue;
			}

			if(!wcsnicmp(pSubStr +1, L"linkcolorhl:", 12))
			{
				idx = 13;
				if(!scanforchar(pSubStr, idx, L'>'))
					goto textemit;
				if(idx != 19 && idx != 21)
					goto textemit;
				UColor color;

				color.r = getHexVal(pSubStr[13]) * 16 + getHexVal(pSubStr[14]);
				color.g = getHexVal(pSubStr[15]) * 16 + getHexVal(pSubStr[16]);
				color.b = getHexVal(pSubStr[17]) * 16 + getHexVal(pSubStr[18]);
				if(idx == 21)
					color.a = getHexVal(pSubStr[19]) * 16 + getHexVal(pSubStr[20]);
				else
					color.a = 255;
				mScanPos += idx + 1;

				if(mCurStyle->used)
					mCurStyle = allocStyle(mCurStyle);
				mCurStyle->linkColorHL = color;

				continue;
			}
			if(!wcsnicmp(pSubStr +1, L"shadow:", 7))
			{
				idx = 8;
				if(!scanforchar(pSubStr, idx, L':'))
					goto textemit;
				int yidx = idx + 1;
				if(!scanforchar(pSubStr, yidx, L'>'))
					goto textemit;
				mScanPos += yidx + 1;
				UPoint offset;
				offset.x = _wtoi(pSubStr + 8);
				offset.y = _wtoi(pSubStr + idx + 1);
				if(mCurStyle->used)
					mCurStyle = allocStyle(mCurStyle);
				mCurStyle->shadowOffset = offset;
				continue;
			}
			if(!wcsnicmp(pSubStr +1, L"img", 3))
			{
				INT start = 5;
				if(pSubStr[4] != L':')
					goto textemit;

				idx = start;
				if(!scanforchar(pSubStr, idx, L'>'))
					goto textemit;

				textStart = mScanPos + start;
				len = idx - start;

				// 检查是否有提供UV坐标
				URect uv(0, 0, 0, 0);
				int uvidx = start;
				/*bool bHasUV = false;
				if(scanforchar(pSubStr, uvidx, L' '))
				{
					bHasUV = true;
					len = uvidx - start;

					int uvx = start + len + 1;
					int uvy = start + len + 1;
					scanforchar(pSubStr, uvx, L'Xpos');
					scanforchar(pSubStr, uvy, L'Ypos');
					wchar_t sz[4];
					memset(sz, 0, sizeof(sz));
					unsigned int uiIndex = 0;
					for(unsigned int ui = uvx + 4; ui < uvy; ui++)
					{
						sz[uiIndex] = *(pSubStr + ui);
						uiIndex++;
					}
					int x = _wtoi(sz);

					uiIndex = 0;
					memset(sz, 0, sizeof(sz));
					for(unsigned int ui = uvy + 4; ui < idx; ui++)
					{
						sz[uiIndex] = *(pSubStr + ui);
						uiIndex++;
					}
					int y = _wtoi(sz);
					uv.SetPosition(x, y);
					uv.SetSize(37, 37);
				}*/

				mScanPos += idx + 1;

				processEmitAtoms();
				Bitmap *bmp;
				{
					UFrameMarker<CHAR> szBitmapName(2*len + 1);
					int nRet = WideCharToMultiByte(CP_UTF8, 0, pSubStr + 5, len, szBitmapName, 2*len, NULL, NULL);
					szBitmapName[nRet] = '\0';
					bmp = allocBitmap(szBitmapName, szRootPath);
					if (bmp == NULL)
					{
						UTRACE("RICHEDIT: can't load bitmap %s ", szBitmapName);
					}
				}
				
				if(bmp)
				{
					//if(!bHasUV)
					{
						uv.SetSize(bmp->bitmapObject->GetWidth(), bmp->bitmapObject->GetHeight());
					}
					emitBitmapToken(bmp, textStart, FALSE, uv);
				}

				continue;
			}

			if(!wcsnicmp(pSubStr +1, L"fac", 3))
			{
				INT start = 5;
				if(pSubStr[4] != L':')
					goto textemit;

				idx = start;
				if(!scanforchar(pSubStr, idx, L'>'))
					goto textemit;

				textStart = mScanPos + start;
				len = idx - start;

				// 检查是否有提供UV坐标
				URect uv(0, 0, 0, 0);
				int uvidx = start;

				mScanPos += idx + 1;

				processEmitAtoms();
				Bitmap *bmp;
				{
					UFrameMarker<CHAR> szBitmapName(2*len + 1);
					int nRet = WideCharToMultiByte(CP_UTF8, 0, pSubStr + 5, len, szBitmapName, 2*len, NULL, NULL);
					szBitmapName[nRet] = '\0';
					bmp = allocBitmap(szBitmapName, szRootPath);
					if (bmp == NULL)
					{
						UTRACE("RICHEDIT: can't load bitmap %s ", szBitmapName);
					}
				}

				if(bmp)
				{
					uv.SetSize(UTX ,UTY);
					emitBitmapToken(bmp, textStart, FALSE, uv,TRUE);
				}

				continue;
			}
			if (!wcsnicmp(pSubStr +1, L"skin", 4))
			{
				INT start = 6;
				if(pSubStr[5] != L':')
					goto textemit;

				idx = start;
				//后面是SKIN的INDEX
				if (!scanforchar(pSubStr,idx,L'>'))
					goto textemit;


				textStart = mScanPos + start;
				len = idx - start ;

				// 检查是否有提供UV坐标
				URect uv(0, 0, 0, 0);
				int uvidx = start;

				mScanPos += idx + 1;

				processEmitAtoms();
				Bitmap *bmp;
				{
					UFrameMarker<CHAR> szBitmapName(2*len + 1);
					int nRet = WideCharToMultiByte(CP_UTF8, 0, pSubStr + 6, len, szBitmapName, 2*len, NULL, NULL);
					szBitmapName[nRet] = '\0';

					
					std::string str = szBitmapName ;
					int pos = str.find(";");
					std::string skinstr = str.substr(0,pos);
					std::string strIndex = str.substr(pos+1, str.length() -1);

					USkinPtr skin =  g_USystem->LoadSkin(skinstr.c_str());
					

					bmp = allocBitmap(skin, szRootPath);
					
					int skinindex = atoi(strIndex.c_str());
					if (bmp == NULL)
					{
						UTRACE("RICHEDIT: can't load SKIN %s ", szBitmapName);
					}

					if(bmp)
					{
						const URect* skinrec = skin->GetSkinItemRect(skinindex);
						uv.SetSize(skinrec->GetSize());
						uv.SetPosition(skinrec->GetPosition());

						emitBitmapToken(bmp, textStart, FALSE, uv, FALSE, TRUE);
					}
				}

				

				continue ;
			}

			// alignment
			if(!wcsnicmp(pSubStr +1, L"left>", 5))
			{
				processEmitAtoms();
				mCurJustify = UT_LEFT;
				mScanPos += 6;
				continue;
			}

			if(!wcsnicmp(pSubStr +1, L"right>", 6))
			{
				processEmitAtoms();
				mCurJustify = UT_RIGHT;
				mScanPos += 7;
				continue;
			}

			if(!wcsnicmp(pSubStr +1, L"center>", 7))
			{
				processEmitAtoms();
				mCurJustify = UT_CENTER;
				mScanPos += 8;
				continue;
			}

			if(!wcsnicmp(pSubStr + 1, L"div:", 4))
			{
				idx = 5;
				if(!scanforchar(pSubStr, idx, L'>'))
					goto textemit;
				mScanPos += idx + 1;
				mCurDiv = _wtoi(pSubStr + 5);
				continue;
			}

			if(!wcsnicmp(pSubStr + 1, L"tab>", 4))
			{
				len = 5;
				textStart = mScanPos;
				mScanPos += 5;
				processEmitAtoms();

				int nWhiteSpaceWidth = mCurStyle->font->fontRes->GetCharXIncrement(L' ');
				mCurX += nWhiteSpaceWidth*UTAB_WIDTH;
				continue;
			}

			if(!wcsnicmp(pSubStr +1, L"a:", 2))
			{
				idx = 3;
				if(!scanforchar(pSubStr, idx, L'>'))
					goto textemit;

				mCurURL = (URL *) mViewChunker.Alloc(sizeof(URL));
				mCurURL->mouseDown = FALSE;
				mCurURL->textStart = mScanPos + 3;
				mCurURL->len = idx - 3;
				mCurURL->noUnderline = FALSE;

				//if the URL is a "#", don't underline...
				if (pSubStr[3] == L'#')
				{
					mCurURL->noUnderline = TRUE;
				}
				mScanPos += idx + 1;
				continue;
			}

			if(!wcsnicmp(pSubStr+1, L"/a>", 3))
			{
				mCurURL = NULL;
				mScanPos += 4;
				continue;
			}

#if 0
			if(!wcsnicmp(pSubStr +1, "spush>", 6))
			{
				mScanPos += 7;
				newStyle = allocStyle(mCurStyle); // copy out all the attributes...
				newStyle->next = mCurStyle;
				mCurStyle = newStyle;
				continue;
			}

			if(!wcsnicmp(pSubStr +1, "spop>", 5))
			{
				mScanPos += 6;
				if(mCurStyle->next)
					mCurStyle = mCurStyle->next;
				continue;
			}

			if(!wcsnicmp(pSubStr +1, "sbreak>", 7))
			{
				mScanPos += 8;
				processEmitAtoms();
				while(mBlockList != &mSentinel)
					emitNewLine(mScanPos);
				continue;
			}

			
			
			UINT margin;

			if(!wcsnicmp(pSubStr + 1, "lmargin%:", 9))
			{
				idx = 10;
				if(!scanforchar(pSubStr, idx, '>'))
					goto textemit;
				margin = (getWidth() * dAtoi(pSubStr + 10)) / 100;
				mScanPos += idx + 1;
				goto setleftmargin;
			}

			if(!wcsnicmp(pSubStr + 1, "lmargin:", 8))
			{
				idx = 9;
				if(!scanforchar(pSubStr, idx, '>'))
					goto textemit;
				margin = dAtoi(pSubStr + 9);
				mScanPos += idx + 1;
setleftmargin:
				processEmitAtoms();
				UINT oldLMargin;
				oldLMargin = mCurLMargin;
				mCurLMargin = margin;
				if(mCurLMargin >= width)
					mCurLMargin = width - 1;
				if(mCurX == oldLMargin)
					mCurX = mCurLMargin;
				if(mCurX < mCurLMargin)
					mCurX = mCurLMargin;
				continue;
			}

			if(!wcsnicmp(pSubStr + 1, "rmargin%:", 9))
			{
				idx = 10;
				if(!scanforchar(pSubStr, idx, '>'))
					goto textemit;
				margin = (getWidth() * dAtoi(pSubStr + 10)) / 100;
				mScanPos += idx + 1;
				goto setrightmargin;
			}

			if(!wcsnicmp(pSubStr + 1, "rmargin:", 8))
			{
				idx = 9;
				if(!scanforchar(pSubStr, idx, '>'))
					goto textemit;
				margin = dAtoi(pSubStr + 9);
				mScanPos += idx + 1;
setrightmargin:
				processEmitAtoms();
				mCurRMargin = margin;
				if(mCurLMargin >= width)
					mCurLMargin = width;
				if (mCurClipX > mCurRMargin)
					mCurClipX = mCurRMargin;
				continue;
			}

			if(!wcsnicmp(pSubStr + 1, "clip:", 5))
			{
				UINT clipWidth = 0;
				idx = 6;
				if(!scanforchar(pSubStr, idx, '>'))
					goto textemit;
				clipWidth = dAtoi(pSubStr + 6);
				mScanPos += idx + 1;
				processEmitAtoms();
				if (clipWidth > 0)
					mCurClipX = mCurX + clipWidth;
				else
					mCurClipX = 0;
				if(mCurClipX > mCurRMargin)
					mCurClipX = mCurRMargin;
				continue;
			}

			if(!wcsnicmp(pSubStr + 1, "/clip>", 6))
			{
				processEmitAtoms();
				mCurClipX = 0;
				mScanPos += 7;
				continue;
			}

			
#endif 
		}

		// default case:
textemit:
		textStart = mScanPos;
		idx = 1;
		while( strbuf[mScanPos+idx] != L'\t' 
			&& strbuf[mScanPos+idx] != L'<' 
			&& strbuf[mScanPos+idx] != L'\n' 
			&& strbuf[mScanPos+idx] )
		{
			idx++;
		}
		len = idx;
		mScanPos += idx;
		emitTextToken(textStart, len);
	}
	processEmitAtoms();
	emitNewLine(mScanPos);
	//setHeight( mMaxY );
	//Con::executef( this,  "onResize", Con::getIntArg( getWidth() ), Con::getIntArg( mMaxY ) );

	//make sure the cursor is still visible - this handles if we're a child of a scroll ctrl...
	//ScrollCaretVisible();
}

Style* RichEdit::allocStyle(Style *style)
{
	Style *ret = (Style *) mViewChunker.Alloc(sizeof(Style));
	ret->used = FALSE;
	if(style)
	{
		ret->font = style->font;
		ret->color = style->color;
		ret->linkColor = style->linkColor;
		ret->linkColorHL = style->linkColorHL;
		ret->shadowColor = style->shadowColor;
		ret->shadowOffset = style->shadowOffset;
		ret->next = style->next;
	}
	else
	{
		ret->font = 0;
		ret->next = 0;
	}
	return ret;
}

Font* RichEdit::allocFont(const char *faceName,UINT size)
{
	assert(g_USystem);
	// 检查字体是否有重复
	for(Font *walk = mFontList; walk; walk = walk->next)
	{
		if (size == walk->size && (stricmp(walk->faceName, faceName) == 0))
		{
			return walk;
		}
	}

	// Create!
	Font *ret;
	ret = (Font*)mResourceChunker.Alloc(sizeof(Font));
	ret = new(ret) Font;
	strcpy(ret->faceName, faceName);
	ret->size = size;
	ret->fontRes = g_USystem->GetPlugin()->CreateFont(faceName, size, GB2312_CHARSET);
	if(ret->fontRes)
	{
		ret->next = mFontList;
		mFontList = ret;
		return ret;
	}
	return NULL;
}

void RichEdit::UpdataBitmapRecIndex()
{
	BitmapRef *Temp  = mBitmapRefList;
	while(Temp)
	{
		if (Temp->bNeed)
		{
			Temp->index += 1;
			UPoint TextureSize;
			
			//纹理的大小
			TextureSize.y= Temp->bitmap->bitmapObject->GetHeight();
			TextureSize.x= Temp->bitmap->bitmapObject->GetWidth();

			if (TextureSize.x > UTX && TextureSize.y > UTY)
			{
				UINT x, y;
				x = TextureSize.x / UTX;
				y = TextureSize.y / UTX;

				if (Temp->index >= x * y)
				{
					Temp->index = 0;
				}

				UINT XIndex = Temp->index % x;
				UINT YIndex = Temp->index / x;
				Temp->uvRect.SetPosition(XIndex * UTX, YIndex * UTY);
			}
		}
		Temp = Temp->next;
	}
}
Bitmap *RichEdit::allocBitmap(const USkin* skin, const char* pRelPath)
{
	if (!skin)
	{
		return NULL;
	}
	const char* bitmapName = skin->GetSkinName();

	for(Bitmap *walk = mBitmapList; walk; walk = walk->next)
	{
		if(!stricmp(walk->bitmapName, bitmapName))
			return walk;
	}
	Bitmap* ret = (Bitmap*)mResourceChunker.Alloc(sizeof(Bitmap));

	ret = new(ret) Bitmap;
	strcpy(ret->bitmapName, bitmapName);
	ret->bitmapObject = skin->GetTexture();

	if( ret->bitmapObject)
	{
		ret->next = mBitmapList;
		mBitmapList = ret;
		return ret;
	}
	return NULL;
}
Bitmap *RichEdit::allocBitmap(const char *bitmapName, const char* pRelPath)
{
	for(Bitmap *walk = mBitmapList; walk; walk = walk->next)
	{
		if(!stricmp(walk->bitmapName, bitmapName))
			return walk;
	}
	Bitmap* ret = (Bitmap*)mResourceChunker.Alloc(sizeof(Bitmap));
	ret = new(ret) Bitmap;
	strcpy(ret->bitmapName, bitmapName);
	if (strlen(pRelPath))
	{
		ret->bitmapObject = g_USystem->GetPlugin()->LoadTexture(ret->bitmapName, pRelPath);
	}
	else
	{
		if (strstr(strupr(ret->bitmapName), "DATA\\UI") == NULL && strstr(strupr(ret->bitmapName), "DATA/UI") == NULL)
		{
			ret->bitmapObject = g_USystem->GetPlugin()->LoadTexture(ret->bitmapName, "DATA\\ui\\");
		}
		else
		{
			ret->bitmapObject = g_USystem->GetPlugin()->LoadTexture(ret->bitmapName, NULL);
		}
	}
	if( ret->bitmapObject)
	{
		ret->next = mBitmapList;
		mBitmapList = ret;
		return ret;
	}
	return NULL;
}

LineTag *RichEdit::allocLineTag(UINT id)
{
	for ( LineTag* walk = mTagList; walk; walk = walk->next )
	{
		if ( walk->id == id )
		{
			UTRACE("line tag %d alread exist.", id);
			return  NULL;
		}
	}
	LineTag* newTag = (LineTag*) mViewChunker.Alloc(sizeof( LineTag ));
	newTag->id = id;
	newTag->y = mCurY;
	newTag->next = mTagList;
	mTagList = newTag;
	return newTag;
}

void RichEdit::SetTextLineSpace(int linespace)
{
	mLineSpace = linespace;
}

void RichEdit::emitNewLine(UINT textStart)
{
	//clear any clipping
	mCurClipX = 0;

	Line *l = (Line *)mViewChunker.Alloc(sizeof(Line));
	l->height = mCurStyle->font->fontRes->GetHeight();
	l->y = mCurY;
	l->textStart = mLineStart;
	l->len = textStart - l->textStart;
	mLineStart = textStart;
	l->atomList = mLineAtoms;
	l->next = 0;
	l->divStyle = mCurDiv;
	*mLineInsert = l;
	mLineInsert = &(l->next);
	mCurX = mCurLMargin;
	mCurTabStop = 0;

	if(mLineAtoms)
	{
		// scan through the atoms in the line, get the largest height
		UINT maxBaseLine = 0;
		UINT maxDescent = 0;
		Atom* walk;

		for(walk = mLineAtoms; walk; walk = walk->next)
		{
			if(walk->baseLine > maxBaseLine)
				maxBaseLine = walk->baseLine;
			if(walk->descent > maxDescent)
				maxDescent = walk->descent;
			if(!walk->next)
			{
				l->len = walk->textStart + walk->len - l->textStart;
				mLineStart = walk->textStart + walk->len;
			}
		}
		l->height = maxBaseLine + maxDescent;

		for(walk = mLineAtoms; walk; walk = walk->next)
		{
			walk->yStart = mCurY + maxBaseLine - walk->baseLine;
			
			//排版设置啊！
			BitmapRef *Temp  = mBitmapRefList;
			while(Temp)
			{
				int h = mCurStyle->font->fontRes->GetHeight();
				if (walk->yStart >= Temp->top && walk->yStart < Temp->bottom)
				{
					if (Temp->GetHeight() >= h)
					{
						walk->yStart = Temp->bottom - h;
						if (l->height < Temp->GetHeight())
						{
							l->height = Temp->GetHeight() ;
						}
					}
				}
				Temp = Temp->next;
			}
		}

		mCurY += l->height + mLineSpace;
	}
	else
	{
		int maxheight = l->height;
		if (mCurX)
		{
			BitmapRef *Temp  = mBitmapRefList;
			while(Temp)
			{
				if (Temp->GetHeight() >= maxheight)
				{
					maxheight = Temp->GetHeight();
				}
				Temp = Temp->next;
			}
		}
		mCurY += maxheight + mLineSpace;
	}
	mLineAtoms = NULL;
	mLineAtomPtr = &mLineAtoms;

	// clear out the blocker list
	BitmapRef **blockList = &mBlockList;
	while(*blockList)
	{
		BitmapRef *blk = *blockList;
		if(blk->bottom <= mCurY)
		{
			*blockList = blk->nextBlocker;
		}else
		{
			blockList = &(blk->nextBlocker);
		}
	}
	if(mCurY > mMaxY)
		mMaxY = mCurY;
}

//--------------------------------------------------------------------------

void RichEdit::emitBitmapToken(Bitmap *bmp, UINT textStart, BOOL bitmapBreak, URect uv, BOOL bNeed, BOOL isSkin)
{
	if(mCurRMargin <= mCurLMargin)		
		return;
	if(uv.GetSize().x == 0)
	{
		if(mCurRMargin - mCurLMargin < bmp->bitmapObject->GetWidth())
			return;
	}

	BitmapRef *ref = (BitmapRef *) mViewChunker.Alloc(sizeof(BitmapRef));
	ref->bitmap = bmp;
	ref->uvRect = uv;
	ref->next = mBitmapRefList;
	ref->url = mCurURL;
	ref->index = 0;
	ref->bNeed = bNeed;
	ref->isSkin = isSkin;
	if (ref->url && m_ContentBitMapURLFun)
	{
		std::string str;
		
		URL*pkURL = ref->url ;
		char* pLinkURL = (char*)UMalloc(pkURL->len*2 + 1);
		assert(pLinkURL);
		WCHAR* pwszLinkURL = *m_pwstrBuffer + pkURL->textStart;
		int nRet = WideCharToMultiByte(CP_UTF8, 0, pwszLinkURL, pkURL->len, pLinkURL, pkURL->len*2, NULL, NULL);
		pLinkURL[nRet] = '\0';
		
		bool isNeed = (*m_ContentBitMapURLFun)(pLinkURL, str);
		
		if (isNeed)
		{
			ref->textContent = (char*)mViewChunker.Alloc(str.length());
			strcpy(ref->textContent, str.c_str());
		}else
		{
			ref->textContent = NULL ;
		}
		
		UFree(pLinkURL);
	}else
	{
		ref->textContent = NULL ;
	}
	mBitmapRefList = ref;

	// now we gotta insert it into the blocker list and figure out where it's spos to go...
	if(uv.GetSize().x != 0)
	{
		ref->left = 0;
		ref->top = 0;
		ref->right = uv.right - uv.left;
		ref->bottom = uv.bottom - uv.top;
		if (ref->GetWidth() > mCurRMargin)
		{
			ref->SetWidth(mCurRMargin - 1);
		}
	}
	else
	{
		ref->left = 0;
		ref->top = 0;
		ref->right = bmp->bitmapObject->GetWidth();
		ref->bottom = bmp->bitmapObject->GetHeight();
		if (ref->GetWidth() > mCurRMargin)
		{
			ref->SetWidth(mCurRMargin - 1);
		}
	}

	// find the first space in the blocker list that will fit this thats > curLMargin

	while(bitmapBreak && mBlockList != &mSentinel)
		emitNewLine(textStart);

	for(;;)
	{
		// loop til we find a line that fits...
		// we'll have to emitLine repeatedly to clear out the block lists...

		BitmapRef **walk = &mBlockList;
		UINT minx = mCurX;
		UINT maxx = mCurRMargin;

		while(*walk)
		{
			BitmapRef *blk = *walk;

			if(blk->left > minx)
			{
				UINT right = maxx;
				if(blk->left < right)
					right = blk->left;
				UINT width = right - minx;

				if(right > minx && width >= ref->right) // we've found the spot...
				{
					// insert it:
					UINT x = minx;
					if(mCurJustify == UT_CENTER)
						x += (width - ref->GetWidth()) >> 1;
					else if(mCurJustify == UT_RIGHT)
						x += width - ref->GetWidth();
				
					ref->Offset(x,mCurY);
					ref->nextBlocker = blk;
					*walk = ref;
					if(ref->bottom > mMaxY)
					{
						mMaxY = ref->bottom;
						//mCurY = mMaxY;  //排版是否需要字围绕图片。 这只这个值后字体将无法围绕图片
					}
					return;
				}
			}
			if(minx < blk->right)
				minx = blk->right;
			// move on to the next blocker...
			walk = &(blk->nextBlocker);
		}
		// go to the next line...
		emitNewLine(textStart);
	}
}

//--------------------------------------------------------------------------
void RichEdit::emitTextToken(UINT textStart, UINT len)
{
	if(mCurRMargin <= mCurLMargin)
		return;

	IUFont *font = mCurStyle->font->fontRes;
	Atom *a = (Atom *) mViewChunker.Alloc(sizeof(Atom));
	a->url = mCurURL;

	a->style = mCurStyle;
	mCurStyle->used = TRUE;

	a->baseLine = font->GetBaseline();
	a->descent = font->GetDescent();
	a->textStart = textStart;
	a->xStart = mCurX;
	a->yStart = mCurY + a->baseLine;
	a->len = len;
	a->isClipped = FALSE;
	a->next = NULL;
	*mEmitAtomPtr = a;
	mEmitAtomPtr = &(a->next);
}

//--------------------------------------------------------------------------
void RichEdit::processEmitAtoms()
{
	Atom *atomList = mEmitAtoms;
	mEmitAtoms = NULL;
	mEmitAtomPtr = &mEmitAtoms;

	BOOL bailout = FALSE;

	while(atomList)
	{
		// split the tokenlist by space
		// first find the first space that the text can go into:
		BitmapRef *br = mBlockList;
		//BOOL bailout = FALSE; // Scoping error here? Moved up one scope. -pw
		Atom *list = atomList;

		while(br && atomList)
		{
			// if the blocker is before the current x, ignore it.
			if(br->right <= mCurX)
			{
				br = br->nextBlocker;
				continue;
			}
		
			// if cur x is in the middle of the blocker
			// advance cur x to right edge of blocker.
			if(mCurX >= br->left)
			{
				mCurX = br->right;
				br = br->nextBlocker;
				continue;
			}

			
			// get the remaining width
			UINT right = br->left;
			if(right > mCurRMargin)
				right = mCurRMargin;

			//if we're clipping text, readjust
			if (mCurClipX > 0 && right > mCurClipX)
				right = mCurClipX;

			// if there's no room, break to the next line...
			if(right <= mCurX)
				break;

			// we've got some space:
			UINT width = right - mCurX;
			
			//UINT tH = mCurStyle->font->fontRes->GetHeight();
			//atomList->yStart = br->bottom - tH;
			atomList = splitAtomListEmit(atomList, width);
			if(atomList) // there's more, so advance cur x
			{
				mCurX = br->right;
				br = br->nextBlocker;
			}
		}
		if(mBlockList == &mSentinel && atomList == list)
		{
			if(bailout)
				return;
			else
				bailout = TRUE;
		}
		// is there more to process for the next line?
		if(atomList)
			emitNewLine(mScanPos);
	}
}

//--------------------------------------------------------------------------
Atom *RichEdit::splitAtomListEmit(Atom *list, UINT width)
{
	UINT totalWidth = 0;
	Atom *emitList = 0;
	Atom **emitPtr = &emitList;

	BOOL adjustClipAtom = FALSE;
	Atom *clipAtom = NULL;
	BOOL emitted = FALSE;

	while(list)
	{
		IUFont *font = list->style->font->fontRes;
		UINT breakPos = 0;

		const WCHAR *tmp16 = m_pwstrBuffer->GetBuffer() + list->textStart;

		//if we're clipping the text, we don't break within an atom, we adjust the atom to only render
		//the portion of text that does fit, and to ignore the rest...
		if (mCurClipX > 0)
		{
#if 0 
			//find out how many character's fit within the given width
			breakPos = font->getBreakPos(tmp16, list->len, width - totalWidth, FALSE);

			//if there isn't room for even the first character...
			if (breakPos == 0)
			{
				//set the atom's len and width to prevent it from being drawn
				list->len = 0;
				list->width = 0;
				adjustClipAtom = TRUE;
			}

			//if our text doesn't fit within the clip region, add a "..."
			else if (breakPos != list->len)
			{
				UINT etcWidth = font->getStrNWidthPrecise("...", 3);
				breakPos = font->getBreakPos(tmp16, list->len, width - totalWidth - etcWidth, FALSE);

				//again, if there isn't even room for a single character before the "...."
				if (breakPos == 0)
				{
					//set the atom's len and width to prevent it from being drawn
					list->len = 0;
					list->width = 0;
					adjustClipAtom = TRUE;
				}
				else
				{
					//set the char len to the break pos, and the rest of the characters in this atom will be ignored
					list->len = breakPos;
					list->width = width - totalWidth;

					//mark this one as clipped
					list->isClipped = TRUE;
					clipAtom = NULL;
				}
			}

			//otherwise no need to treat this atom any differently..
			else
			{
				//set the atom width == to the string length
				list->width = font->getStrNWidthPrecise(tmp16, breakPos);

				//set the pointer to the last atom that fit within the clip region
				clipAtom = list;
			}
#endif 
		}
		else
		{
			breakPos = GetLineBreakCP(font, tmp16, list->len, width - totalWidth, TRUE);
			if(breakPos == 0 
				|| (breakPos < list->len 
					&& m_pwstrBuffer->GetChar(list->textStart + breakPos - 1)!= ' ' 
					&& emitted
					)
				)
			{
				// Todo. Modify by WJ don't break at this point.
				//break;
			}
			else
			{
				//set the atom width == to the string length
				list->width =  GetStrNWidthPrecise(font, tmp16, breakPos);
			}
		}

		//update the total width
		totalWidth += list->width;

		// see if this is the last atom that will fit:
		Atom *emit = list;

		*emitPtr = emit;
		emitPtr = &(emit->next);
		emitted = TRUE;

		//if we're clipping, don't split the atom, otherwise, see if it needs to be split
		if(!list->isClipped && breakPos != list->len)
		{
			Atom *a = (Atom *) mViewChunker.Alloc(sizeof(Atom));
			a->url = list->url;
			a->textStart = list->textStart + breakPos;
			a->len = list->len - breakPos;
			a->next = list->next;
			a->baseLine = list->baseLine;
			a->descent = list->descent;
			a->style = list->style;
			a->isClipped = FALSE;

			list = a;
			emit->len = breakPos;
			break;
		}
		list = list->next;
		if(totalWidth > width)
			break;
	}

	//if we had to completely clip an atom(s), the last (partially) visible atom should be modified to include a "..."
	if (adjustClipAtom && clipAtom)
	{
		IUFont *font = clipAtom->style->font->fontRes;
		UINT breakPos;

		UINT etcWidth = GetStrNWidthPrecise(font, L"...", 3);

		const WCHAR *tmp16 = m_pwstrBuffer->GetBuffer() + clipAtom->textStart;

		breakPos = GetLineBreakCP(font, tmp16, clipAtom->len, clipAtom->width - etcWidth, FALSE);
		if (breakPos != 0)
		{
			clipAtom->isClipped = TRUE;
			clipAtom->len = breakPos;
		}
	}

	// terminate the emit list:
	*emitPtr = 0;
	// now emit it:
	// going from mCurX to mCurX + width:
	if(mCurJustify == UT_CENTER)
	{
		if ( width > totalWidth )
			mCurX += (width - totalWidth) >> 1;
	}
	else if(mCurJustify == UT_RIGHT)
	{
		if ( width > totalWidth )
			mCurX += width - totalWidth;
	}


	while(emitList)
	{
		emitList->xStart = mCurX;
		mCurX += emitList->width;
		Atom *temp = emitList->next;
		*mLineAtomPtr = emitList;
		emitList->next = 0;
		mLineAtomPtr = &(emitList->next);
		emitList = temp;
	}

	return list;
}

BitmapRef* RichEdit::findHitBitMapRec(const UPoint& localCoords)
{

	BitmapRef* pBitmapAtom = mBitmapRefList;
	while (pBitmapAtom)
	{
		if (pBitmapAtom->PointInRect(localCoords))
		{
			return pBitmapAtom;
		}
		pBitmapAtom = pBitmapAtom->next ;
	}
	

	return NULL;
}

Atom *RichEdit::findHitAtom(const UPoint& localCoords)
{
	//if(mDirty)
	//{
		//Parse();
	//}
	for(Line *walk = mLineList; walk; walk = walk->next)
	{
		if(localCoords.y < walk->y)
			return NULL;

		if(localCoords.y >= walk->y && localCoords.y < walk->y + walk->height)
		{
			for(Atom *awalk = walk->atomList; awalk; awalk = awalk->next)
			{
				if(localCoords.x < awalk->xStart)
					return NULL;
				if(localCoords.x >= awalk->xStart + awalk->width)
					continue;
				return awalk;
			}
		}
	}
	return NULL;
}

IUTexture* RichEdit::FindTextureAtom(const char* Name) const
{
	BitmapRef *walk = mBlockList;

	while(walk)
	{
		if(walk->bitmap)
		{
			if (stricmp(Name, walk->bitmap->bitmapName) == 0)
			{
				return walk->bitmap->bitmapObject;
			}
		}
		walk = walk->nextBlocker;
	}
	return NULL;
}
