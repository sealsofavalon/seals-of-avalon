#ifndef __URICHATOM_H__
#define __URICHATOM_H__
#include "URichEdit.h"

struct Font {
	char faceName[MAX_PATH];// 字体名称	
	UINT size;				// 字体大小
	UFontPtr fontRes;		// 字体资源引用
	Font *next;
};

struct Bitmap 
{
	char bitmapName[MAX_PATH];
	UTexturePtr bitmapObject;
	Bitmap *next;
};

struct URL
{
	BOOL mouseDown;
	UINT textStart;
	UINT len;
	BOOL noUnderline;
};

struct Style
{
	UColor color;
	UColor shadowColor;
	UColor linkColor;
	UColor linkColorHL;
	UPoint shadowOffset;
	Font *font;
	BOOL used;
	Style *next;
};

struct Atom
{
	UINT textStart;
	UINT len;
	UINT xStart;
	UINT yStart;
	UINT width;
	UINT baseLine;
	UINT descent;
	Style *style;
	bool isClipped;	// 是否被裁减了. 末尾显示"..." 

	URL *url;
	Atom *next;
};

struct Line {
	UINT y;
	UINT height;
	UINT divStyle;
	UINT textStart; // 行首字符位置 char pos
	UINT len;
	Atom *atomList;
	Line *next;
};

//这里已经定义需要查找索引的图的大小。由于暂时的需求，这里定义死了大小为32 * 32 。
//以后如果需要拓展功能的话,可以将数据写入BitmapRef里进行在定义.
#define  UTX 32
#define  UTY 32

struct BitmapRef : public URect
{
	BitmapRef *nextBlocker;
	UINT textStart;
	UINT len;
	URect uvRect;
	URL *url;
	Bitmap *bitmap;
	BitmapRef *next;
	int index;//这需要渲染的索引
	BOOL bNeed;//是否需要动态计算 //
	BOOL isSkin;
	char* textContent ;
};

struct LineTag {
	UINT id;
	INT y;
	LineTag *next;
};


struct RichEdit 
{
	bool mIsEditCtrl;

	const UStringW* m_pwstrBuffer;
	UINT *mTabStops;
	UINT mTabStopCount;
	UINT mCurTabStop;

	UChunkAllocator mViewChunker;
	UChunkAllocator mResourceChunker;
	Line *mLineList;
	Bitmap *mBitmapList;
	BitmapRef *mBitmapRefList;
	Font *mFontList;
	LineTag *mTagList;
	BOOL mDirty;
	Style* mCurStyle;

	UINT mCurLMargin;
	UINT mCurRMargin;
	UINT mCurJustify;
	UINT mCurDiv;
	UINT mCurY;
	UINT mCurClipX;
	UINT   mLineStart;		// 当前行的起点X坐标
	UINT mLineSpace;
	Atom* mLineAtoms;
	Atom** mLineAtomPtr;

	Atom* mEmitAtoms;
	Atom** mEmitAtomPtr;

	BitmapRef mSentinel;
	Line** mLineInsert;
	BitmapRef* mBlockList;
	int mScanPos;
	UINT mCurX;
	UINT mMaxY;
	URL *mCurURL;
	URL *mHitURL;
	URL *mOnMoveURL;

	ContentBitMapURLFun  m_ContentBitMapURLFun ;

	RichEdit();
	void Parse(const UControlStyle* pCtrlStyle, int nWidth, const UStringW& strbuf, const char* pRtfFilePath , ContentBitMapURLFun BitMapURLFun = NULL);
	void freeLineBuffers();
	void freeResource();
	BitmapRef* findHitBitMapRec(const UPoint& localCoords);
	Atom *findHitAtom(const UPoint& localCoords);
	IUTexture* FindTextureAtom(const char* Name) const;
	void UpdataBitmapRecIndex();
	void SetTextLineSpace(int linespace);
private:
	Bitmap *allocBitmap(const char *bitmapName, const char* pRelPath);
	Bitmap *allocBitmap(const USkin* skin, const char* pRelPath);
	Font *allocFont(const char *faceName, UINT size);
	LineTag *allocLineTag(UINT id);
	void emitNewLine(UINT textStart);
	Atom *buildTextAtom(UINT start, UINT len, UINT left, UINT right, URL *url);
	void emitTextToken(UINT textStart, UINT len);
	void emitBitmapToken(Bitmap *bmp, UINT textStart, BOOL bitmapBreak, URect uv, BOOL bNeed = FALSE,BOOL isSkin = FALSE);
	void processEmitAtoms();
	Atom *splitAtomListEmit(Atom *list, UINT width );
	
	
	Style *allocStyle(Style *style);
	
};

class URender;
class UControlStyle;
extern int GetLineBreakCP(IUFont* pFont, const WCHAR* wstr, int len, int width, BOOL breakOnWhitespace);

extern void DrawTextAtom(URender* pRender, UControlStyle* pCtrlStl, const WCHAR* pStr, float fAlpha, BOOL sel, 
				  UINT start, UINT end, Atom *atom, 
				  Line *line, const UPoint& offset, float depth = 0.f);



#endif 