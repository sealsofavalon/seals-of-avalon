#include "StdAfx.h"
#include "URichEdit.h"
#include "URichAtom.h"
#include "UScrollBar.h"

UIMP_CLASS(URichTextCtrl, UControl);
//--------------------------------------------------------------------------
void URichTextCtrl::StaticInit()
{
	UREG_PROPERTY("rtffile", UPT_STRING, UFIELD_OFFSET(URichTextCtrl, m_strRtfFile));
	UREG_PROPERTY("MouseOverShowBGk", UPT_BOOL, UFIELD_OFFSET(URichTextCtrl, m_bDrawBgk));
	UREG_PROPERTY("ActiveMouseOverURL", UPT_BOOL, UFIELD_OFFSET(URichTextCtrl, m_ActiveOnMouseURL));
	UREG_PROPERTY("ActiveBigMapURL", UPT_BOOL, UFIELD_OFFSET(URichTextCtrl, m_ActiveBigMapURL));
	/*Parent::initPersistFields();
	addField("lineSpacing",       TypeS32,    Offset(mLineSpacingPixels, URichTextCtrl));
	addField("allowColorChars",   TypeBool,   Offset(mAllowColorChars,   URichTextCtrl));
	addField("maxChars",          TypeS32,    Offset(m_nMaxSize,     URichTextCtrl));
	addField("deniedSound",       TypeSFXProfilePtr, Offset(mDeniedSound, URichTextCtrl));
	addField("text",              TypeCaseString,  Offset( mInitialText, URichTextCtrl ) );*/
}

URichTextCtrl::URichTextCtrl()
{
	//  mIsEditCtrl = FALSE;
	m_nCaretCP     = 0;
	m_nMaxSize = -1;
	m_bSelActive = FALSE;
	m_nSelStart  = 0;
	m_nSelEnd    = 0;
	mLineSpacingPixels = 2;
	mAllowColorChars = FALSE;
	m_pRichEdit = new RichEdit;
	assert(m_pRichEdit);
	m_Active = TRUE;
	m_bDirty = TRUE;
	mAlpha = 1.0;
	m_uiLineOffset = 1;
	m_FaceTime = 0;
	m_bDrag = FALSE;
	m_bDrawBgk = FALSE;
	m_ActiveOnMouseURL= FALSE;
	m_ActiveBigMapURL = TRUE;

	m_pkBitMapURLFun = NULL;
}

URichTextCtrl::~URichTextCtrl()
{
	m_nCaretCP     = 0;
	if (m_pRichEdit)
	{
		m_pRichEdit->freeResource();
		delete m_pRichEdit;
	}
	m_pkBitMapURLFun = NULL;
}


//BOOL URichTextCtrl::onAdd()
//{
//   if(!Parent::onAdd())
//      return FALSE;
//
//   if (!m_wstrBuffer.length() && mInitialText[0] != 0)
//      SetText(mInitialText, dStrlen(mInitialText)+1);
//   return TRUE;
//}

BOOL URichTextCtrl::OnCreate()
{
	if (!UControl::OnCreate())
		return FALSE;

	if (!m_strRtfFile.Empty())
	{
		unsigned int size = 0;
		CFileStream kFile;
		if(!kFile.Open(m_strRtfFile.GetBuffer()))
		{
			UTRACE("can't open RTF file from %s", m_strRtfFile.GetBuffer());
			return FALSE;
		}

		size = kFile.GetSize();
		char* pRtfContent = (char*)UMalloc(size + 1);
		assert(pRtfContent);
		size = kFile.Read(pRtfContent, size);
		pRtfContent[size] = '\0';

		int nWideChar = MultiByteToWideChar(CP_UTF8, 0, pRtfContent, size, NULL, 0);

		// append 128 chars for safety when parsing the RTF text.
		if(!m_wstrBuffer.Resize(nWideChar+128))
		{
			UFree(pRtfContent);
			UTRACE("memory overflow.");
			return FALSE;
		}

		nWideChar = MultiByteToWideChar(CP_UTF8, 0, pRtfContent, size, m_wstrBuffer, nWideChar);
		UFree(pRtfContent);
		for (int i = 0; i < 128; i++)
		{
			m_wstrBuffer[i + nWideChar] = 0;
		}
		m_wstrBuffer.SetLength(nWideChar);
	}

	if (m_wstrBuffer.GetLength())
	{
		m_bDirty = TRUE;
		FormatContent();
		scrollToTop();
	}else
	{
		m_bDirty = FALSE;
	}
	return TRUE;
}

void URichTextCtrl::OnDestroy()
{
	if (m_pRichEdit)
	{
		m_pRichEdit->freeResource();
	}
	UControl::OnDestroy();
}
void URichTextCtrl::UpDataFaceIndex(float time)
{
	if (!IsVisible())
	{
		return ;
	}
	UINT UTIME = time * 1000;
	m_FaceTime += UTIME;
	if (m_FaceTime >= UFACEFRAMETIME)
	{
		m_FaceTime = 0;
		m_pRichEdit->UpdataBitmapRecIndex();
	}
}
void URichTextCtrl::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);
	UpDataFaceIndex(fDeltaTime);
}

void URichTextCtrl::FormatContent()
{
	if (m_bDirty)
	{
		assert(m_pRichEdit);
		if(m_wstrBuffer.Empty())
		{
			m_pRichEdit->freeResource();

			if (GetParent())
			{
				SetHeight(0);
				UScrollBar* pSB = UDynamicCast(UScrollBar, GetParent());
				if (pSB)
				{
					pSB->ReCalLayout();
					pSB->scrollTo(0,0);
				}
			}
			return ;
		}

		m_pRichEdit->Parse(m_Style, m_Size.x, m_wstrBuffer, m_strRtfFile.GetBuffer(),m_pkBitMapURLFun);
		m_bDirty = FALSE;




		if (GetParent())
		{
			SetHeight(m_pRichEdit->mMaxY);
			UScrollBar* pSB = UDynamicCast(UScrollBar, GetParent());
			if (pSB)
			{
				pSB->ReCalLayout();
				pSB->scrollTo(0,m_pRichEdit->mMaxY);
			}
		}
	}
}

IUTexture* URichTextCtrl::FindTextureAtom(const char* Name) const
{
	assert(Name && Name[0]);
	if (m_pRichEdit)
	{
		return m_pRichEdit->FindTextureAtom(Name);
	}
	return NULL;
}

void URichTextCtrl::OnRender(const UPoint& offset, const URect& updateRect)
{
	UControl::OnRender(offset, updateRect);

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	g_pRenderInterface->SetScissorRect(sm_UiRender->GetClipRect());

	if (m_bDrawBgk && m_MouseHover)
	{
		sm_UiRender->DrawRectFill(updateRect, UColor(1, 1 ,1, 120));
	}
	FormatContent();
	// 渲染图像
	{
		BitmapRef* pBitmapRefList = m_pRichEdit->mBitmapRefList;
		BitmapRef* pBitmapAtom = pBitmapRefList;
		URect AtomRect;
		while (pBitmapAtom)
		{
			AtomRect.left = pBitmapAtom->left;
			AtomRect.top = pBitmapAtom->top;
			AtomRect.right = pBitmapAtom->right;
			AtomRect.bottom = pBitmapAtom->bottom;
			AtomRect.Offset(offset);
			if ( AtomRect.top > updateRect.bottom || AtomRect.top < updateRect.top || AtomRect.bottom > updateRect.bottom)
			{
				pBitmapAtom = pBitmapAtom->next;
				continue;
			}
			if (AtomRect.Intersect(updateRect))
			{
				if (m_pRichEdit->mHitURL && pBitmapAtom->url && m_pRichEdit->mHitURL == pBitmapAtom->url && m_pRichEdit->mHitURL->mouseDown)
				{
					sm_UiRender->DrawImage(pBitmapAtom->bitmap->bitmapObject, AtomRect, pBitmapAtom->uvRect,UColor(242,181,53));
				}else if (m_pRichEdit->mOnMoveURL && pBitmapAtom->url &&  m_pRichEdit->mOnMoveURL ==  pBitmapAtom->url)
				{
					sm_UiRender->DrawImage(pBitmapAtom->bitmap->bitmapObject, AtomRect, pBitmapAtom->uvRect, UColor(72,219,223));
				}else
				{
					sm_UiRender->DrawImage(pBitmapAtom->bitmap->bitmapObject, AtomRect, pBitmapAtom->uvRect);
				}

				if (pBitmapAtom->textContent)
				{
					int textH = m_Style->m_spFont->GetHeight();
					UPoint TextPos( AtomRect.pt0.x, AtomRect.pt1.y - textH - 1); 
					UPoint TextSize( AtomRect.GetWidth() - 1, textH);

					//要求字渲染在右下角
					UDrawText(sm_UiRender,m_Style->m_spFont,TextPos,TextSize,m_Style->m_FontColor,pBitmapAtom->textContent,UT_RIGHT);
				}
			}
			pBitmapAtom = pBitmapAtom->next;
		}
	}

	// 渲染字符
	{
		Line* pLine = m_pRichEdit->mLineList;

		URect LineRect;
		while (pLine)
		{
			LineRect.left = offset.x;
			LineRect.top = offset.y;
			LineRect.right = offset.x + m_Size.x;
			LineRect.bottom = offset.y + pLine->height;

			if (LineRect.Intersect(updateRect))
			{
				if(pLine->divStyle)
				{
					sm_UiRender->DrawRectFill(LineRect, m_Style->mFillColorHL);
				}
			}
			if (pLine->y + offset.y > updateRect.bottom || 
				pLine->y + offset.y < updateRect.top || 
				pLine->y + pLine->height + offset.y > updateRect.bottom)
			{
				pLine = pLine->next;
				continue;
			}
			for(Atom* pAtom = pLine->atomList; pAtom; pAtom = pAtom->next)
			{
				if(!m_bSelActive 
					|| m_nSelEnd < pAtom->textStart 
					|| m_nSelStart >= (pAtom->textStart + pAtom->len))
				{
					DrawTextAtom(sm_UiRender, m_Style, m_wstrBuffer.GetBuffer(), mAlpha, 
						FALSE, pAtom->textStart, pAtom->textStart + pAtom->len, pAtom, pLine, offset);
				}
				else
				{
					UINT selectionStart = __max(pAtom->textStart, m_nSelStart);
					UINT selectionEnd = __min(pAtom->textStart + pAtom->len, m_nSelEnd + 1);

					// draw some unselected text
					if(selectionStart > pAtom->textStart)
					{
						DrawTextAtom(sm_UiRender, m_Style, m_wstrBuffer.GetBuffer(), mAlpha,
							FALSE, pAtom->textStart, selectionStart, pAtom, pLine, offset);
					}

					// draw the selection
					DrawTextAtom(sm_UiRender, m_Style, m_wstrBuffer.GetBuffer(), mAlpha,
						TRUE, selectionStart, selectionEnd, pAtom, pLine, offset);

					if(selectionEnd < pAtom->textStart + pAtom->len)
					{
						DrawTextAtom(sm_UiRender, m_Style, m_wstrBuffer.GetBuffer(), mAlpha,
							FALSE, selectionEnd, pAtom->textStart + pAtom->len, pAtom, pLine, offset);
					}
				}
			}

			// 处理下一个
			pLine = pLine->next;
		}
	}

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
}

void URichTextCtrl::OnSize(const UPoint& NewSize)
{
	UControl::OnSize(NewSize);
	m_bDirty = TRUE;
	FormatContent();
}

int URichTextCtrl::GetTextLength() const
{
	return (int)m_wstrBuffer.GetLength();
}
void URichTextCtrl::SetBitMapURLFun(ContentBitMapURLFun Fun)
{
	m_pkBitMapURLFun = Fun ;
}
int URichTextCtrl::GetText(int nStartCP, char* pDestBuf, int nMaxLen) const
{
	if (nMaxLen <= 0 || pDestBuf == NULL)
	{
		nMaxLen = m_wstrBuffer.GetLength() - nStartCP;
		if (nMaxLen <= 0)
		{
			return 0;
		}
		WCHAR* pwStr = m_wstrBuffer;
		int nRet = WideCharToMultiByte(CP_UTF8, 0, pwStr + nStartCP, nMaxLen, NULL, 0, NULL, NULL);
		return nRet;
	}else
	{
		int nWideChar = m_wstrBuffer.GetLength() - nStartCP;
		if (nWideChar <= 0)
		{
			return 0;
		}
		WCHAR* pwStr = m_wstrBuffer;
		pwStr += nStartCP;
		nWideChar = __min(nMaxLen, nWideChar);
		int nRet = WideCharToMultiByte(CP_UTF8, 0, pwStr, nWideChar, pDestBuf, nMaxLen, NULL, NULL);
		return nRet;
	}
}

int URichTextCtrl::GetText(int nStartCP, WCHAR* pBuffer, int nMaxLen) const
{
	if (nMaxLen <= 0 || pBuffer == NULL)
	{
		nMaxLen = m_wstrBuffer.GetLength() - nStartCP;
		if (nMaxLen <0)
		{
			nMaxLen = 0;
		}
		return nMaxLen;
	}else
	{
		nMaxLen = __min(m_wstrBuffer.GetLength() - nStartCP, nMaxLen);
	}

	if (nMaxLen <= 0)
	{
		return 0;
	}

	for (int i =0; i < nMaxLen; i++)
	{
		pBuffer[i] = m_wstrBuffer[nStartCP + i];
	}
	return nMaxLen;
}

void URichTextCtrl::SetText(const WCHAR* textBuffer, UINT numChars)
{
	if (textBuffer == NULL || textBuffer[0] == 0 || numChars == 0)
	{
		m_wstrBuffer.SetEmpty();
	}else
	{
		UINT chars = numChars;
		if(numChars >= m_nMaxSize)
			chars = m_nMaxSize;

		m_wstrBuffer.Resize(chars + 128);
		wcsncpy(m_wstrBuffer, textBuffer, chars);
		m_wstrBuffer.SetLength(chars);
		for (int i = 0; i < 128; i++)
		{
			m_wstrBuffer.operator [](i + chars) = 0;
		}
	}

	//after setting text, always set the cursor to the beginning
	SetCaretCP(0);
	ClearSel();
	m_bDirty = TRUE;
	FormatContent();
	scrollToTop();
}

void URichTextCtrl::SetText(const char* textBuffer, UINT numChars)
{
	if (textBuffer == NULL || textBuffer[0] == 0 || numChars == 0)
	{
		m_wstrBuffer.SetEmpty();
	}else
	{
		UINT chars = numChars;
		if(numChars >= m_nMaxSize)
			chars = m_nMaxSize;

		//UFrameMarker<WCHAR> wsz(chars + 1);
		WCHAR* wsz = new WCHAR[chars + 1];
		int nRet = MultiByteToWideChar(CP_UTF8, 0, textBuffer, chars, wsz, chars);
		wsz[nRet] = 0;
		m_wstrBuffer.Set(wsz);
	}

	//after setting text, always set the cursor to the beginning
	SetCaretCP(0);
	ClearSel();
	m_bDirty = TRUE;
	FormatContent();
	scrollToTop();
}

void URichTextCtrl::AppendText(const WCHAR* text, UINT numChars, BOOL bReformat)
{
	if(m_nMaxSize > 0 && numChars >= m_nMaxSize)
		return;

	m_wstrBuffer.Append(text, numChars);
	if (bReformat)
	{
		SetCaretCP(0);
		ClearSel();
		m_bDirty = TRUE;
		FormatContent();
		scrollToTop();
	}
}

void URichTextCtrl::AppendText(const char* textBuffer, UINT numChars, BOOL reformat)
{
	if(m_nMaxSize > 0 && numChars >= m_nMaxSize)
		return;

	{
		//UFrameMarker<WCHAR>  MemMark(numChars +1);
		WCHAR* MemMark = new WCHAR[numChars + 1];
		int Ret = MultiByteToWideChar(CP_UTF8, 0, textBuffer, numChars, MemMark, numChars);
		m_wstrBuffer.Append(MemMark, Ret);
		delete[] MemMark;
	}

	//after setting text, always set the cursor to the beginning
	if (reformat)
	{
		SetCaretCP(0);
		ClearSel();
		m_bDirty = TRUE;
		FormatContent();
		scrollToTop();
	}
}

int URichTextCtrl::GetContentHeight() const
{
	if (m_pRichEdit == NULL)
	{
		return 0;
	}
	return m_pRichEdit->mMaxY;
}

int URichTextCtrl::GetLineHeight() const
{
	if (m_pRichEdit == NULL || !m_pRichEdit->mLineList)
	{
		return 0;
	}
	return m_pRichEdit->mLineList->height;
}
BOOL URichTextCtrl::SetCaretCP(INT newPosition)
{
	if (newPosition < 0) 
	{
		m_nCaretCP = 0;
		return TRUE;
	}
	else if (newPosition >= m_wstrBuffer.GetLength()) 
	{
		m_nCaretCP = m_wstrBuffer.GetLength();
		return TRUE;
	}
	else 
	{
		m_nCaretCP = newPosition;
		return FALSE;
	}
}

void URichTextCtrl::ScrollCaretVisible()
{
	// If our parent isn't a scroll control, or we're not the only control
	//  in the content region, bail...
	UControl* pParent = GetParent();
	UScrollBar* pSB = UDynamicCast(UScrollBar, pParent);
	if(!pSB)
		return;

	// Ok.  Now we know that our parent is a scroll control.  Let's find the
	//  top of the cursor, and it's bottom.  We can then scroll the parent control
	//  if appropriate...

	UPoint cursorTopP, cursorBottomP;
	UColor color;
	GetCaretInfo(cursorTopP, cursorBottomP, color);

	pSB->scrollRectVisible(URect(cursorTopP.x, cursorTopP.y, 1, cursorBottomP.y - cursorTopP.y));
}

void URichTextCtrl::GetCaretInfo(UPoint &cursorTop, UPoint &cursorBottom, UColor &color)
{
	INT x = 0;
	INT y = 0;
	INT height = m_Style->m_spFont->GetHeight();
	color = m_Style->mCursorColor;
	assert(m_pRichEdit);
	FormatContent();
	for(Line *walk = m_pRichEdit->mLineList; walk; walk = walk->next)
	{
		if ((m_nCaretCP <= walk->textStart + walk->len) || (walk->next == NULL))
		{
			// it's in the atoms on this line...
			y = walk->y;
			height = walk->height;

			for(Atom *awalk = walk->atomList; awalk; awalk = awalk->next)
			{
				if(m_nCaretCP < awalk->textStart)
				{
					x = awalk->xStart;
					goto done;
				}

				if(m_nCaretCP > awalk->textStart + awalk->len)
				{
					x = awalk->xStart + awalk->width;
					continue;
				}

				// it's in the text block...
				x = awalk->xStart;
				IUFont *font = awalk->style->font->fontRes;

				const WCHAR* buff = m_wstrBuffer.GetBuffer() + awalk->textStart;
				x += font->GetStrNWidth(buff, m_nCaretCP - awalk->textStart - 1);

				color = awalk->style->color;
				goto done;
			}

			//if it's within this walk's width, but we didn't find an atom, leave the cursor at the beginning of the line...
			goto done;
		}
	}
done:
	cursorTop.Set(x, y);
	cursorBottom.Set(x, y + height);
}

int URichTextCtrl::InsertText(int nCharPos, const WCHAR* text, int nLen)
{
	assert(!IsSelActive());
	assert(nCharPos <= m_wstrBuffer.GetLength());
	assert(nLen > 0);

	INT nCharToIns = nLen;
	if (m_nMaxSize > 0 && (m_wstrBuffer.GetLength() + nLen + 1 > m_nMaxSize))
		nCharToIns = m_nMaxSize - m_wstrBuffer.GetLength() - 1;


	if (nCharToIns <= 0)
	{
		// Play the "Denied" sound:
		// if ( numInputChars > 0 && mDeniedSound )
		//    SFX->playOnce(mDeniedSound);
		UTRACE("rich edit buffer overflow.");
		return 0;
	}

	m_wstrBuffer.Insert(nCharPos, text, nCharToIns);

	if (m_nCaretCP >= nCharPos) 
	{
		m_nCaretCP += nCharToIns;
	}

	assert(m_nCaretCP <= m_wstrBuffer.GetLength());
	m_bDirty = TRUE;
	return nCharToIns;
}

int URichTextCtrl::InsertText(int nCharPos, const char* text, int nLen)
{
	assert(!IsSelActive());
	assert(nCharPos <= m_wstrBuffer.GetLength());
	assert(nLen > 0);

	INT nCharToIns = nLen;
	if (m_nMaxSize > 0 && (m_wstrBuffer.GetLength() + nLen + 1 > m_nMaxSize))
		nCharToIns = m_nMaxSize - m_wstrBuffer.GetLength() - 1;


	if (nCharToIns <= 0)
	{
		// Play the "Denied" sound:
		// if ( numInputChars > 0 && mDeniedSound )
		//    SFX->playOnce(mDeniedSound);
		UTRACE("rich edit buffer overflow.");
		return 0;
	}

	{
		UFrameMarker<WCHAR> wszBuf(nCharToIns+1);
		int nWC = MultiByteToWideChar(CP_UTF8,0, text, nCharToIns, wszBuf, nCharToIns);
		wszBuf[nWC] = 0;
		m_wstrBuffer.Insert(nCharPos,wszBuf, nWC);
		nCharToIns = nWC;
	}
	if (m_nCaretCP >= nCharPos) 
	{
		// Cursor was at or after the inserted text, move forward...
		m_nCaretCP += nCharToIns;
	}
	assert(m_nCaretCP <= m_wstrBuffer.GetLength());
	m_bDirty = TRUE;
	return nCharToIns;
}

void URichTextCtrl::DeleteText(int rangeStart, int rangeEnd)
{
	assert(!IsSelActive());
	assert(rangeStart < m_wstrBuffer.GetLength() 
		&& rangeEnd < m_wstrBuffer.GetLength()
		&& rangeStart < rangeEnd);

	m_wstrBuffer.Cut(rangeStart, rangeEnd);

	if (m_nCaretCP <= rangeStart) 
	{
		// Cursor placed before deleted text, ignore
	}
	else if (m_nCaretCP > rangeStart && m_nCaretCP <= rangeEnd) 
	{
		// Cursor inside deleted text, set to start of range
		m_nCaretCP = rangeStart;
	}
	else 
	{
		// Cursor after deleted text, decrement by number of chars deleted
		m_nCaretCP -= (rangeEnd - rangeStart) + 1;
	}

	assert(m_nCaretCP <= m_wstrBuffer.GetLength());
	m_bDirty = TRUE;
	//FormatContent();
}
void URichTextCtrl::ClearText()
{

	SetText("",0);
}
void URichTextCtrl::Copy()
{
	if (m_bSelActive)
	{
		Copy(m_nSelStart, m_nSelEnd);
	}
}

void URichTextCtrl::Copy(int rangeStart, int rangeEnd)
{
	assert(rangeStart < m_wstrBuffer.GetLength() 
		&& rangeEnd < m_wstrBuffer.GetLength()
		&& rangeStart < rangeEnd);
	if (!OpenClipboard(NULL))
	{
		return;
	}
	if(!EmptyClipboard())
	{
		return;
	}
	HGLOBAL hBlock = GlobalAlloc( GMEM_MOVEABLE, (rangeEnd - rangeStart + 1 )*sizeof(WCHAR) );
	if( hBlock )
	{
		WCHAR *pszText = (WCHAR*)GlobalLock( hBlock );
		if( pszText )
		{
			m_wstrBuffer.SubStr(pszText, rangeStart, rangeEnd - rangeStart);
			GlobalUnlock( hBlock );
		}
		HANDLE hcbData = SetClipboardData(CF_UNICODETEXT, hBlock );
	}
	CloseClipboard();
	if( hBlock )
		GlobalFree( hBlock );
}

BOOL URichTextCtrl::IsSelActive() const
{
	return m_bSelActive;
}

void URichTextCtrl::ClearSel()
{
	m_bSelActive = FALSE;
	m_nSelStart  = 0;
	m_nSelEnd    = 0;
}

void URichTextCtrl::SetSel(int nStart, int nEnd)
{
	ClearSel();
	if (nStart != nEnd)
	{
		nStart = __min(nStart, nEnd);
		nEnd = __max(nStart, nEnd);

		int nMaxCP = m_wstrBuffer.GetLength();
		nStart = LClamp(nStart, 0, nMaxCP);
		nEnd = LClamp(nEnd, 0, nMaxCP);
		m_nSelStart = nStart;
		m_nSelEnd = nEnd;
	}
}

BOOL URichTextCtrl::GetSel(int* nStart, int* nEnd)
{
	if (nStart)
	{
		*nStart = m_nSelStart;
	}
	if (nEnd)
	{
		*nEnd = m_nSelEnd;
	}
	return m_bSelActive;
}

void URichTextCtrl::scrollToTag( UINT id )
{
	// If the parent control is not a GuiScrollContentCtrl, then this call is invalid:
	//  UScrollBar *pappy = dynamic_cast<UScrollBar*>(getParent());
	//  if ( !pappy )
	//     return;

	//  // Find the indicated tag:
	//  LineTag* tag = NULL;
	//  for ( tag = mTagList; tag; tag = tag->next )
	//  {
	//     if ( tag->id == id )
	//        break;
	//  }

	//  if ( !tag )
	//  {
	//     Con::warnf( ConsoleLogEntry::General, "URichTextCtrl::scrollToTag - tag id %d not found!", id );
	//     return;
	//  }
	//pappy->scrollRectVisible(URect(0, tag->y, 1, 1));
}

void URichTextCtrl::scrollToTop()
{
	// If the parent control is not a GuiScrollContentCtrl, then this call is invalid:
	UScrollBar *pappy = UDynamicCast(UScrollBar, GetParent());
	if ( !pappy )
		return;
	pappy->scrollRectVisible(URect(0,0,0,0));
}

void URichTextCtrl::scrollToBottom()
{
	// If the parent control is not a GuiScrollContentCtrl, then this call is invalid:
	UScrollBar *pappy = UDynamicCast(UScrollBar, GetParent());
	if ( !pappy )
		return;

	// Figure bounds for the bottom left corner
	URect cornerBounds (0, GetWindowPos().y + GetWindowSize().y, 1, 1);
	pappy->scrollRectVisible(cornerBounds);
}

int URichTextCtrl::GetCharPos(const UPoint& localCoords)
{
	assert(m_bCreated);
	FormatContent();
	for(Line *walk = m_pRichEdit->mLineList; walk; walk = walk->next)
	{
		if((INT)localCoords.y < (INT)walk->y)
			return walk->textStart;

		if(localCoords.y >= walk->y && localCoords.y < walk->y + walk->height)
		{
			for(Atom *awalk = walk->atomList; awalk; awalk = awalk->next)
			{
				if(localCoords.x < awalk->xStart)
					return awalk->textStart;
				if(localCoords.x >= awalk->xStart + awalk->width)
					continue;
				// it's in the text block...
				IUFont *font = awalk->style->font->fontRes;

				const WCHAR *tmp16 = m_wstrBuffer.GetBuffer() + awalk->textStart;
				UINT bp = GetLineBreakCP(font, tmp16, awalk->len, localCoords.x - awalk->xStart, FALSE);
				return awalk->textStart + bp;
			}
			return walk->textStart + walk->len;
		}
	}
	return m_wstrBuffer.GetLength() - 1;
}


BOOL URichTextCtrl::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	//only copy work with this control...
	if ((nFlags & LKM_CTRL)
		&& nKeyCode == LK_C)
	{
		if (m_bSelActive)
		{
			Copy(m_nSelStart, m_nSelEnd);
		}
		return TRUE;
	}

	// dispatch to parent.
	return UControl::OnKeyDown(nKeyCode, nRepCnt, nFlags);
}
void URichTextCtrl::ClearHitURL()
{
	if (m_pRichEdit)
	{
		if (m_pRichEdit->mHitURL)
		{
			m_pRichEdit->mHitURL->mouseDown = FALSE;
			m_pRichEdit->mHitURL = NULL;
		}
	}
}

void URichTextCtrl::OnMouseDown(const UPoint& pt, UINT nClickCnt, UINT uFlags)
{
	if(!m_Active || !m_bCreated)
		return;

	UPoint ptLocal = ScreenToWindow(pt);
	FormatContent();


	Atom* pHitAtom = m_pRichEdit->findHitAtom(ptLocal);
	URL* pURL = NULL;
	if (pHitAtom)
	{
		CaptureControl();
		pURL = pHitAtom->url;
	}else
	{
		if (m_ActiveBigMapURL)
		{
			BitmapRef* pHitBitMap = m_pRichEdit->findHitBitMapRec(ptLocal);
			if (pHitBitMap)
			{
				CaptureControl();
				pURL = pHitBitMap->url;
			}
		}
	}

	if (pURL)
	{
		if (m_pRichEdit->mHitURL)
		{
			m_pRichEdit->mHitURL->mouseDown = FALSE;
			m_pRichEdit->mHitURL = NULL;
		}

		m_pRichEdit->mHitURL = pURL;
		pURL->mouseDown = TRUE;
	}

	SetFocusControl();
	CaptureControl();

	m_bSelActive = FALSE;
	mSelectionAnchor = GetCharPos(ptLocal);
	mSelectionAnchorDropped = pt;
	if (mSelectionAnchor < 0)
		mSelectionAnchor = 0;
	else if (mSelectionAnchor >= m_wstrBuffer.GetLength())
		mSelectionAnchor = m_wstrBuffer.GetLength();
	mVertMoveAnchorValid = FALSE;
}

void URichTextCtrl::SetVisible(BOOL value)
{
	if (m_pRichEdit)
	{
		if (m_pRichEdit->mOnMoveURL)
		{
			DispatchNotify(URED_OUTMOUSEMOVEURL);
			m_pRichEdit->mOnMoveURL = NULL;
		}
	}

	UControl::SetVisible(value);
}
void URichTextCtrl::OnMouseDragged(const UPoint& pt, UINT uFlags)
{
	if (!m_Active|| !m_bCreated || !IsCaptured() || !m_bDrag)
	{
		return;
	}
	FormatContent();
	UPoint ptLocal = ScreenToWindow(pt);
	Atom* pHitAtom = m_pRichEdit->findHitAtom(ptLocal);

	//BOOL down = FALSE;
	URL* pHitURL = NULL;
	if (pHitAtom)
	{
		pHitURL = pHitAtom->url ;
	}
	pHitAtom->url ;//m_pRichEdit->mHitURL;
	////note mHitURL can't be set unless this is (!mIsEditCtrl)
	//if(pHitAtom && pHitAtom->url == pHitURL)
	//   down = TRUE;

	//if(pHitURL && down != pHitURL->mouseDown)
	//   m_pRichEdit->mHitURL->mouseDown = down;

	if (!pHitURL)
	{
		INT newSelection = 0;
		newSelection = GetCharPos(ptLocal);
		newSelection = LClamp<int>(newSelection, 0, m_wstrBuffer.GetLength());

		if (newSelection == mSelectionAnchor) 
		{
			m_bSelActive = FALSE;
		}
		else if (newSelection > mSelectionAnchor) 
		{
			m_bSelActive = TRUE;
			m_nSelStart  = mSelectionAnchor;
			m_nSelEnd    = newSelection - 1;
		}
		else 
		{
			m_nSelStart  = newSelection;
			m_nSelEnd    = mSelectionAnchor - 1;
			m_bSelActive = TRUE;
		}
		SetCaretCP(newSelection);
		//   mDirty = TRUE;
	}
}
void URichTextCtrl::OnMouseMove(const UPoint& position, UINT flags)
{
	if (!m_Active || !m_bCreated || !m_ActiveOnMouseURL)
	{
		return ;
	}
	ReleaseCapture();

	FormatContent();
	UPoint ptLocal = ScreenToWindow(position);
	Atom* pHitAtom = m_pRichEdit->findHitAtom(ptLocal);

	URL* pURL = NULL;
	if (pHitAtom)
	{
		CaptureControl();
		pURL = pHitAtom->url;
	}else
	{
		BitmapRef* pHitBitMap = m_pRichEdit->findHitBitMapRec(ptLocal);
		if (pHitBitMap)
		{
			CaptureControl();
			pURL = pHitBitMap->url;
		}
	}
	if (m_pRichEdit->mOnMoveURL && !pURL)
	{
		m_pRichEdit->mOnMoveURL = NULL;
		DispatchNotify(URED_OUTMOUSEMOVEURL);
		return ;
	}

	if (m_pRichEdit->mOnMoveURL  == pURL && pURL)
	{
		return ;
	}
	m_pRichEdit->mOnMoveURL = pURL ;

	if (pURL)
	{
		char* pLinkURL = (char*)UMalloc(pURL->len*2 + 1);
		assert(pLinkURL);
		WCHAR* pwszLinkURL = m_wstrBuffer + pURL->textStart;
		int nRet = WideCharToMultiByte(CP_UTF8, 0, pwszLinkURL, pURL->len, pLinkURL, pURL->len*2, NULL, NULL);
		pLinkURL[nRet] = '\0';

		URichEditURLClickNM NM;
		NM.pszURL = pLinkURL;
		DispatchNotify(URED_ONMOUSEMOVEURL, &NM);

		UFree(pLinkURL);
		return;
	}
}
void URichTextCtrl::OnMouseUp(const UPoint& pt, UINT uFlags)
{
	if (!m_Active|| !m_bCreated || !IsCaptured())
	{
		return;
	}

	ReleaseCapture();

	FormatContent();
	UPoint ptLocal = ScreenToWindow(pt);
	Atom* pHitAtom = m_pRichEdit->findHitAtom(ptLocal);
	URL* pHitURL = m_pRichEdit->mHitURL;
	BitmapRef* pHitBitMap  = NULL ;

	if (m_ActiveBigMapURL)
	{
		pHitBitMap = m_pRichEdit->findHitBitMapRec(ptLocal);
	}

	if (pHitURL && ((pHitAtom && pHitAtom->url == pHitURL) || (pHitBitMap && pHitBitMap->url == pHitURL) )&& pHitURL->mouseDown)
	{
		//pHitURL->mouseDown = FALSE;

		// 不要使用UFrameMarker, 因为这里最后会调用用户的相应函数
		// 如果用户挂起线程, 则其它任何引起UFrameMarker操作的线程都将被挂起.
		char* pLinkURL = (char*)UMalloc(pHitURL->len*2 + 1);
		assert(pLinkURL);
		WCHAR* pwszLinkURL = m_wstrBuffer + pHitURL->textStart;
		int nRet = WideCharToMultiByte(CP_UTF8, 0, pwszLinkURL, pHitURL->len, pLinkURL, pHitURL->len*2, NULL, NULL);
		pLinkURL[nRet] = '\0';
		if (uFlags == LKM_SHIFT)
		{
			URichEditURLClickNM NM;
			NM.pszURL = pLinkURL;
			DispatchNotify(URED_SHIFTCLICKURL, &NM);
		}else if(uFlags == LKM_CTRL)
		{
			URichEditURLClickNM NM;
			NM.pszURL = pLinkURL;
			DispatchNotify(URED_CTRLCLICKURL, &NM);
		}
		else
		{
			OnClickedLinkTag(pLinkURL);
		}
		UFree(pLinkURL);
		if(!m_ActiveBigMapURL)
			ClearHitURL();
		//m_pRichEdit->mHitURL = NULL;	
		return;
	}
	else
	{
		UPoint diff = pt - mSelectionAnchorDropped;
		if ((diff.x*diff.x + diff.y * diff.y) < 9)
		{
			m_bSelActive = FALSE;
		}
		int nCharPos = GetCharPos(ptLocal);
		SetCaretCP(nCharPos);
		mVertMoveAnchorValid = FALSE;
	}
}
void URichTextCtrl::OnClickedLinkTag(const char* pCmd)
{
	URichEditURLClickNM NM;
	NM.pszURL = pCmd;
	DispatchNotify(URED_CLICKURL, &NM);
	//	DispatchCmdMsg
}
void URichTextCtrl::OnRightClickedLinkTag(const char *pCmd)
{
	URichEditURLClickNM NM;
	NM.pszURL = pCmd;
	DispatchNotify(URED_RIGHTCLICKURL, &NM);
}
void URichTextCtrl::OnRightMouseDown(const UPoint& position, UINT nRepCnt, UINT flags)
{
	OnMouseDown(position,nRepCnt,flags);
}
void URichTextCtrl::OnRightMouseUp(const UPoint& pt, UINT flags)
{
	if (!m_Active|| !m_bCreated || !IsCaptured())
	{
		return;
	}

	ReleaseCapture();

	FormatContent();
	UPoint ptLocal = ScreenToWindow(pt);
	Atom* pHitAtom = m_pRichEdit->findHitAtom(ptLocal);
	URL* pHitURL = m_pRichEdit->mHitURL;
	BitmapRef* pHitBitMap  = m_pRichEdit->findHitBitMapRec(ptLocal);
	if (pHitURL && ((pHitAtom && pHitAtom->url == pHitURL) || (pHitBitMap && pHitBitMap->url)) && pHitURL->mouseDown)
	{
		//pHitURL->mouseDown = FALSE;

		// 不要使用UFrameMarker, 因为这里最后会调用用户的相应函数
		// 如果用户挂起线程, 则其它任何引起UFrameMarker操作的线程都将被挂起.
		char* pLinkURL = (char*)UMalloc(pHitURL->len*2 + 1);
		assert(pLinkURL);
		WCHAR* pwszLinkURL = m_wstrBuffer + pHitURL->textStart;
		int nRet = WideCharToMultiByte(CP_UTF8, 0, pwszLinkURL, pHitURL->len, pLinkURL, pHitURL->len*2, NULL, NULL);
		pLinkURL[nRet] = '\0';
		OnRightClickedLinkTag(pLinkURL);
		UFree(pLinkURL);
		if(!m_ActiveBigMapURL)
			ClearHitURL();
		//m_pRichEdit->mHitURL = NULL;	
		return;
	}
	else
	{
		UPoint diff = pt - mSelectionAnchorDropped;
		if ((diff.x*diff.x + diff.y * diff.y) < 9)
		{
			m_bSelActive = FALSE;
		}
		int nCharPos = GetCharPos(ptLocal);
		SetCaretCP(nCharPos);
		mVertMoveAnchorValid = FALSE;
	}
}
int URichTextCtrl::TrimRichFormat(const WCHAR* pInRFT, char* pOutFFT, int nMaxSize)
{
#if 0 
	if (! BOOL(inString))
		return NULL;
	UINT maxBufLength = 64;
	char *strippedBuffer = Con::getReturnBuffer(maxBufLength);
	char *stripBufPtr = &strippedBuffer[0];
	const char *bufPtr = (char *) inString;
	UINT idx, sizidx;

	for(;;)
	{
		//if we've reached the end of the string, or run out of room in the stripped Buffer...
		if(*bufPtr == '\0' || (UINT(stripBufPtr - strippedBuffer) >= maxBufLength - 1))
			break;

		if (*bufPtr == '\n')
		{
			UINT walked;
			oneUTF8toUTF32(bufPtr,&walked);
			bufPtr += walked; 
			continue;
		}
		if(*bufPtr == '\t')
		{
			UINT walked;
			oneUTF8toUTF32(bufPtr,&walked);
			bufPtr += walked;
			continue;
		}
		if(*bufPtr < 0x20 && *bufPtr >= 0)
		{
			UINT walked;
			oneUTF8toUTF32(bufPtr,&walked);
			bufPtr += walked;
			continue;
		}

		if(*bufPtr == '<')
		{
			// it's probably some kind of tag:
			if(!dStrnicmp(bufPtr + 1, "font:", 5))
			{
				// scan for the second colon...
				// at each level it should drop out to the text case below...
				idx = 6;
				if(!scanforchar((char*)bufPtr, idx, ':'))
					goto textemit;

				sizidx = idx + 1;
				if(!scanforchar((char*)bufPtr, sizidx, '>'))
					goto textemit;

				bufPtr += sizidx + 1;
				continue;
			}

			if (!dStrnicmp(bufPtr + 1, "tag:", 4 ))
			{
				idx = 5;
				if ( !scanforchar((char*)bufPtr, idx, '>' ))
					goto textemit;

				bufPtr += idx + 1;
				continue;
			}

			if(!dStrnicmp(bufPtr + 1, "color:", 6))
			{
				idx = 7;
				if(!scanforchar((char*)bufPtr, idx, '>'))
					goto textemit;
				if(idx != 13)
					goto textemit;

				bufPtr += 14;
				continue;
			}

			if(!dStrnicmp(bufPtr +1, "bitmap:", 7))
			{
				idx = 8;
				if(!scanforchar((char*)bufPtr, idx, '>'))
					goto textemit;

				bufPtr += idx + 1;
				continue;
			}

			if(!dStrnicmp(bufPtr +1, "spush>", 6))
			{
				bufPtr += 7;
				continue;
			}

			if(!dStrnicmp(bufPtr +1, "spop>", 5))
			{
				bufPtr += 6;
				continue;
			}

			if(!dStrnicmp(bufPtr +1, "sbreak>", 7))
			{
				bufPtr += 8;
				continue;
			}

			if(!dStrnicmp(bufPtr +1, "just:left>", 10))
			{
				bufPtr += 11;
				continue;
			}

			if(!dStrnicmp(bufPtr +1, "just:right>", 11))
			{
				bufPtr += 12;
				continue;
			}

			if(!dStrnicmp(bufPtr +1, "just:center>", 12))
			{
				bufPtr += 13;
				continue;
			}

			if(!dStrnicmp(bufPtr +1, "a:", 2))
			{
				idx = 3;
				if(!scanforchar((char*)bufPtr, idx, '>'))
					goto textemit;

				bufPtr += idx + 1;
				continue;
			}

			if(!dStrnicmp(bufPtr+1, "/a>", 3))
			{
				bufPtr += 4;
				continue;
			}

			if(!dStrnicmp(bufPtr + 1, "lmargin%:", 9))
			{
				idx = 10;
				if(!scanforchar((char*)bufPtr, idx, '>'))
					goto textemit;
				bufPtr += idx + 1;
				goto setleftmargin;
			}

			if(!dStrnicmp(bufPtr + 1, "lmargin:", 8))
			{
				idx = 9;
				if(!scanforchar((char*)bufPtr, idx, '>'))
					goto textemit;
				bufPtr += idx + 1;
setleftmargin:
				continue;
			}

			if(!dStrnicmp(bufPtr + 1, "rmargin%:", 9))
			{
				idx = 10;
				if(!scanforchar((char*)bufPtr, idx, '>'))
					goto textemit;
				bufPtr += idx + 1;
				goto setrightmargin;
			}

			if(!dStrnicmp(bufPtr + 1, "rmargin:", 8))
			{
				idx = 9;
				if(!scanforchar((char*)bufPtr, idx, '>'))
					goto textemit;
				bufPtr += idx + 1;
setrightmargin:
				continue;
			}

			if(!dStrnicmp(bufPtr + 1, "clip:", 5))
			{
				idx = 6;
				if(!scanforchar((char*)bufPtr, idx, '>'))
					goto textemit;
				bufPtr += idx + 1;
				continue;
			}

			if(!dStrnicmp(bufPtr + 1, "/clip>", 6))
			{
				bufPtr += 7;
				continue;
			}

			if(!dStrnicmp(bufPtr + 1, "div:", 4))
			{
				idx = 5;
				if(!scanforchar((char*)bufPtr, idx, '>'))
					goto textemit;
				bufPtr += idx + 1;
				continue;
			}

			if(!dStrnicmp(bufPtr + 1, "tab:", 4))
			{
				idx = 5;
				if(!scanforchar((char*)bufPtr, idx, '>'))
					goto textemit;
				bufPtr += idx + 1;
				continue;
			}
		}

		// default case:
textemit:
		*stripBufPtr++ = *bufPtr++;
		while(*bufPtr != '\t' && *bufPtr != '<' && *bufPtr != '\n' && (*bufPtr >= 0x20 || *bufPtr < 0))
			*stripBufPtr++ = *bufPtr++;
	}

	//we're finished - terminate the string
	*stripBufPtr = '\0';
	return strippedBuffer;
#endif 
	return NULL;
}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(URichEdit, URichTextCtrl)

void URichEdit::StaticInit()
{
}

URichEdit::URichEdit(void)
{
	//	mIsEditCtrl = TRUE;

	m_Active = TRUE;

	mVertMoveAnchorValid = FALSE;
}

URichEdit::~URichEdit(void)
{
}

void URichEdit::Cut()
{
	Copy();
	m_bSelActive = FALSE;
	DeleteText(m_nSelStart, m_nSelEnd);
	m_nCaretCP = m_nSelStart;
}

void URichEdit::Paste()
{
	//const char *clipBuf = Platform::getClipboard();
	//if (dStrlen(clipBuf) > 0)
	//{
	//	// Normal ascii keypress.  Go ahead and add the chars...
	//	if (m_bSelActive == TRUE)
	//	{
	//		m_bSelActive = FALSE;
	//		DeleteText(m_nSelStart, m_nSelEnd);
	//		m_nCaretCP = m_nSelStart;
	//	}

	//	InsertText(clipBuf, dStrlen(clipBuf), m_nCaretCP);
	//}
}

void URichEdit::OnSize(const UPoint& NewSize)
{
	UControl::OnSize(NewSize);
	//// We don't want to get any smaller than our parent:
	//UPoint newExt = newExtent;
	//UControl* parent = getParent();
	//if ( parent )
	//	newExt.y = getMax( parent->getHeight(), newExt.y );
}

BOOL URichEdit::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	if (nFlags & LKM_CTRL)
	{
		switch(nKeyCode)
		{
		case LK_C:
			{
				Copy();
				return TRUE;
			}
		case LK_X:
			{
				Cut();
				return TRUE;
			}
		case LK_V:
			{
				Paste();
				return TRUE;
			}
		}
	}
	else if ( nFlags & LKM_SHIFT )
	{
		switch ( nKeyCode )
		{
		case LK_TAB:
			return UControl::OnKeyDown(nKeyCode, nRepCnt, nFlags);
		}
	}
	else if ( nFlags == 0 )
	{
		switch (nKeyCode)
		{
			//	// Escape:
			//case KEY_ESCAPE:
			//	if ( mEscapeCommand[0] )
			//	{
			//		Con::evaluate( mEscapeCommand );
			//		return( TRUE );
			//	}
			//	return( Parent::onKeyDown( event ) );

			// Deletion
		case LK_DELETE:
		case LK_BACKSPACE:
			{
				if ( IsSelActive() )
				{
					m_bSelActive = FALSE;
					DeleteText(m_nSelStart, m_nSelEnd+1);
					m_nCaretCP = m_nSelStart;
				}
				else
				{
					if (nKeyCode == LK_BACKSPACE)
					{
						if (m_nCaretCP != 0)
						{
							DeleteText(m_nCaretCP-1, m_nCaretCP);
						}
					}else
					{
						if (m_nCaretCP != m_wstrBuffer.GetLength())
						{
							// delete one character right
							DeleteText(m_nCaretCP, m_nCaretCP+1);
						}
					}
				}
			}
			return TRUE;

			// Cursor movement
		case LK_LEFT:
		case LK_RIGHT:
		case LK_UP:
		case LK_DOWN:
		case LK_HOME:
		case LK_END:
			{

			}
			return TRUE;

			// Special chars...
		case LK_TAB:
			// insert 3 spaces
			if (m_bSelActive == TRUE)
			{
				m_bSelActive = FALSE;
				DeleteText(m_nSelStart, m_nSelEnd);
				m_nCaretCP = m_nSelStart;
			}
			InsertText(m_nCaretCP, L"\t", 1  );
			return TRUE;

		case LK_RETURN:
			// insert carriage return
			if (m_bSelActive == TRUE)
			{
				m_bSelActive = FALSE;
				DeleteText(m_nSelStart, m_nSelEnd);
				m_nCaretCP = m_nSelStart;
			}
			InsertText(m_nCaretCP, L"\n", 1);
			return TRUE;
		}
	}
	//
	//	if (event.ascii != 0)
	//	{
	//		// Normal ascii keypress.  Go ahead and add the chars...
	//		if (m_bSelActive == TRUE)
	//		{
	//			m_bSelActive = FALSE;
	//			DeleteText(m_nSelStart, m_nSelEnd);
	//			m_nCaretCP = m_nSelStart;
	//		}
	//
	//		UTF8 *outString = NULL;
	//		UINT outStringLen = 0;
	//
	//#ifdef TORQUE_UNICODE
	//
	//		UTF16 inData[2] = { event.ascii, 0 };
	//		StringBuffer inBuff(inData);
	//
	//		FrameTemp<UTF8> outBuff(4);
	//		inBuff.getCopy8(outBuff, 4);
	//
	//		outString = outBuff;
	//		outStringLen = dStrlen(outBuff);
	//#else
	//		char ascii = char(event.ascii);
	//		outString = &ascii;
	//		outStringLen = 1;
	//#endif
	//
	//		InsertText(outString, outStringLen, m_nCaretCP);
	//		mVertMoveAnchorValid = FALSE;
	//		return TRUE;
	//	}

	// Otherwise, let the parent have the event...
	return UControl::OnKeyDown(nKeyCode, nRepCnt, nFlags);
}
//
////--------------------------------------
//void URichEdit::handleMoveKeys(const GuiEvent& event)
//{
//	if ( event.modifier & SI_SHIFT )
//		return;
//
//	m_bSelActive = FALSE;
//
//	switch ( event.keyCode )
//	{
//	case KEY_LEFT:
//		mVertMoveAnchorValid = FALSE;
//		// move one left
//		if ( m_nCaretCP != 0 )
//		{
//			m_nCaretCP--;
//			setUpdate();
//		}
//		break;
//
//	case KEY_RIGHT:
//		mVertMoveAnchorValid = FALSE;
//		// move one right
//		if ( m_nCaretCP != m_wstrBuffer.length() )
//		{
//			m_nCaretCP++;
//			setUpdate();
//		}
//		break;
//
//	case KEY_UP:
//	case KEY_DOWN:
//		{
//			Line* walk;
//			for ( walk = mLineList; walk->next; walk = walk->next )
//			{
//				if ( m_nCaretCP <= ( walk->textStart + walk->len ) )
//					break;
//			}
//
//			if ( !walk )
//				return;
//
//			if ( event.keyCode == KEY_UP )
//			{
//				if ( walk == mLineList )
//					return;
//			}
//			else if ( walk->next == NULL )
//				return;
//
//			UPoint newPos;
//			newPos.set( 0, walk->y );
//
//			// Find the x-position:
//			if ( !mVertMoveAnchorValid )
//			{
//				UPoint cursorTopP, cursorBottomP;
//				UColor color;
//				GetCaretInfo(cursorTopP, cursorBottomP, color);
//				mVertMoveAnchor = cursorTopP.x;
//				mVertMoveAnchorValid = TRUE;
//			}
//
//			newPos.x = mVertMoveAnchor;
//
//			// Set the new y-position:
//			if (event.keyCode == KEY_UP)
//				newPos.y--;
//			else
//				newPos.y += (walk->height + 1);
//
//			if (SetCaretCP(GetCharPos(newPos)))
//				mVertMoveAnchorValid = FALSE;
//			break;
//		}
//
//	case KEY_HOME:
//	case KEY_END:
//		{
//			mVertMoveAnchorValid = FALSE;
//			Line* walk;
//			for (walk = mLineList; walk->next; walk = walk->next)
//			{
//				if (m_nCaretCP <= (walk->textStart + walk->len))
//					break;
//			}
//
//			if (walk)
//			{
//				if (event.keyCode == KEY_HOME)
//				{
//					//place the cursor at the beginning of the first atom if there is one
//					if (walk->atomList)
//						m_nCaretCP = walk->atomList->textStart;
//					else
//						m_nCaretCP = walk->textStart;
//				}
//				else
//				{
//					m_nCaretCP = walk->textStart;
//					m_nCaretCP += walk->len;
//				}
//				setUpdate();
//			}
//			break;
//		}
//
//	default:
//		assert(FALSE, "Unknown move key code was received!");
//	}
//
//	ScrollCaretVisible();
//}

//--------------------------------------------------------------------------
void URichEdit::OnRender(const UPoint& offset, const URect& updateRect)
{
	URichTextCtrl::OnRender(offset, updateRect);

	// We are the first responder, draw our cursor in the appropriate position...
	if (IsFocused()) 
	{
		UPoint top, bottom;
		UColor color;
		GetCaretInfo(top, bottom, color);
		sm_UiRender->DrawLine(top + offset, bottom + offset, m_Style->mCursorColor);
	}
}

