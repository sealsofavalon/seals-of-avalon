#include "StdAfx.h"
#include "UScrollBar.h"

// 拖动滚动条的时候, 鼠标难免会超出滚动条的范围, 加上这个修正允许鼠标拖动时候能够超出的范围.
#define SCROLLBAR_DRAG_TOLERANCE 60  // pixel. 

// 隐藏起来, 避免派生类错误修改.
struct ScrollContext 
{
	ScrollContext()
	{
		nArrowWidth = 16;
		nArrowHeight = 16;
		ChildRelPos.Set(0,0);
		ChildRelPosAnchor.Set(0,0);
	}
	URect	rcUpArrow;
	URect	rcDownArrow;
	URect	rcVTrack;
	URect	rcLeftArrow;
	URect	rcRightArrow;
	URect	rcHTrack;
	
	int		nFixedThumbSize;	// 固定长度大小

	int nLastUpdated;
	int nHThumbSize;
	int nHThumbPos;
	int nVThumbSize;
	int nVThumbPos;
	int nArrowWidth;	// 箭头宽度,
	int nArrowHeight;	// 箭头高度 由skin 中的低一个 uparrow normal 状态决定.

	UPoint ContentPos;  // the position of the content region in the control's coord system
	UPoint ContentSize;  // the extent of the content region
	UPoint ChildPos;   // the position of the upper left corner of the child control(s)
	UPoint ChildSize;

	UPoint ChildRelPos; // the relative position of the upper left content corner in
						// the child's coordinate system - 0,0 if scrolled all the way to upper left.
	UPoint ChildRelPosAnchor; // the original childRelPos when scrolling started
	int nThumbMouseDragPos;		

	UINT bHBarEnabled:1;
	UINT bVBarEnabled:1;
	UINT bHasHScrollBar:1;
	UINT bHasVScrollBar:1;
	UINT bFixThumbLength:1;	// 是否固定滚动条长度 如果固定,则滚动条长度不会因为内容大小发生变化.

	BOOL ResetBarRect(USkin* pSkin, UPoint& Pos, UPoint& Size, int thickness)
	{
		
		int nSkinItem = pSkin->GetItemNum();
		if (nSkinItem <= UScrollBar::SBS_VERT_THUMBTOPCAP_N)
		{
			bFixThumbLength = TRUE;
			const URect* pThumbRect = pSkin->GetSkinItemRect(UScrollBar::SBS_VERT_THUMB_N);
			assert(pThumbRect);
			nFixedThumbSize = pThumbRect->GetHeight();
		}else if(nSkinItem == UScrollBar::SBS_MAX)
		{
			bFixThumbLength = FALSE;
		}

		const URect* pUpArrowRect = pSkin->GetSkinItemRect(UScrollBar::SBS_UP_ARROW_N);
		nArrowWidth = pUpArrowRect->GetWidth();
		nArrowHeight = pUpArrowRect->GetHeight();


		if (bHasHScrollBar)
		{
			rcLeftArrow.Set(thickness, Size.y - thickness - nArrowHeight - 1,
				nArrowHeight,
				nArrowWidth);

			rcRightArrow.Set(Size.x - thickness - (bHasVScrollBar ? nArrowHeight : 0) - nArrowHeight,
				Size.y - thickness - nArrowWidth - 1,
				nArrowHeight,
				nArrowWidth);
			rcHTrack.Set(rcLeftArrow.right,
				rcLeftArrow.top,
				rcRightArrow.left - rcLeftArrow.right,
				nArrowWidth);
		}
		if (bHasVScrollBar)
		{
			rcUpArrow.Set(Size.x - thickness - nArrowWidth,
				thickness,
				nArrowWidth,
				nArrowHeight);
			rcDownArrow.Set(Size.x - thickness - nArrowWidth,
				Size.y - thickness - nArrowHeight - (bHasHScrollBar ? ( nArrowWidth + 1 ) : 0),
				nArrowWidth,
				nArrowHeight);
			rcVTrack.Set(rcUpArrow.left,
				rcUpArrow.bottom,
				nArrowWidth,
				rcDownArrow.top - rcUpArrow.bottom);
		}
		return TRUE;
	}

	void UpdateThumb()
	{
		if (bHBarEnabled && ChildSize.x > 0)
		{
			UINT trackSize = rcHTrack.GetWidth();

			if (bFixThumbLength)
				nHThumbSize = nFixedThumbSize;
			else
				nHThumbSize = __max(nFixedThumbSize, int((ContentSize.x * trackSize) / ChildSize.x));

			nHThumbPos = rcHTrack.left + (ChildRelPos.x * (trackSize - nHThumbSize)) / (ChildSize.x - ContentSize.x);
		}
		if (bVBarEnabled && ChildSize.y > 0)
		{
			UINT trackSize = rcVTrack.GetHeight();

			if (bFixThumbLength)
				nVThumbSize = nFixedThumbSize;
			else
				nVThumbSize = __max(nFixedThumbSize, int((ContentSize.y * trackSize) / ChildSize.y));

			nVThumbPos = rcVTrack.top + (ChildRelPos.y * (trackSize - nVThumbSize)) / (ChildSize.y - ContentSize.y);
		}
	}

	UScrollBar::EUSBRegion HitTest(const UPoint &pt) const
	{
		if (bHasVScrollBar)
		{
			if (rcUpArrow.PointInRect(pt))
				return UScrollBar::SBR_UP;
			else if (rcDownArrow.PointInRect(pt))
				return UScrollBar::SBR_DOWN;
			else if (bVBarEnabled && rcVTrack.PointInRect(pt))
			{
				if (pt.y < nVThumbPos)
					return UScrollBar::SBR_PGUP;
				else if (pt.y < nVThumbPos + nVThumbSize)
					return UScrollBar::SBR_VTHUMB;
				else
					return UScrollBar::SBR_PGDN;
			}
		}
		if (bHasHScrollBar)
		{
			if (rcLeftArrow.PointInRect(pt))
				return UScrollBar::SBR_LEFT;
			else if (rcRightArrow.PointInRect(pt))
				return UScrollBar::SBR_RIGHT;
			else if (bHBarEnabled && rcHTrack.PointInRect(pt))
			{
				
				if (pt.x < nHThumbPos)
					return UScrollBar::SBR_PGLEFT;
				else if (pt.x < nHThumbPos + nHThumbSize)
					return UScrollBar::SBR_HTHUMB;
				else
					return UScrollBar::SBR_PGRIGHT;
			}
		}
		return UScrollBar::SBR_UNKNOWN;
	}

	void UpdateSize(const UPoint& SBPos, const UPoint& SBSize, 
		const UPoint* pChildPos, const UPoint* pChildSize,
		INT nVBarMode, int nHBarMode, UPoint& delta)
	{
		ContentPos = SBPos;
		ContentSize = SBSize;

		UPoint childLowerRight;

		bHBarEnabled = FALSE;
		bVBarEnabled = FALSE;
		bHasVScrollBar = (nVBarMode == USBSM_FORCEON);
		bHasHScrollBar = (nHBarMode == USBSM_FORCEON);
		delta.x = 0;
		delta.y = 0;
		if (pChildSize && pChildPos)
		{
			ChildPos = *pChildPos;
			ChildSize = *pChildSize;
			childLowerRight = ChildPos + ChildSize;

			if (bHasVScrollBar)
				ContentSize.x -= nArrowWidth;

			if (bHasHScrollBar)
				ContentSize.y -= nArrowWidth;

			if (ChildSize.x > ContentSize.x && (nHBarMode == USBSM_DYNAMIC))
			{
				bHasHScrollBar = TRUE;
				ContentSize.y -= nArrowWidth;
			}
			if (ChildSize.y > ContentSize.y && (nVBarMode == USBSM_DYNAMIC))
			{
				bHasVScrollBar = TRUE;
				ContentSize.x -= nArrowWidth;

				// If Extent X Changed, check Horiz Scrollbar.
				if (ChildSize.x > ContentSize.x && !bHasHScrollBar && (nHBarMode == USBSM_DYNAMIC))
				{
					bHasHScrollBar = TRUE;
					ContentSize.y -= nArrowWidth;
				}
			}
			UPoint contentLowerRight = ContentPos + ContentSize;

			if (ChildPos.x > ContentPos.x)
				delta.x = ContentPos.x - ChildPos.x;
			else if (contentLowerRight.x > childLowerRight.x)
			{
				int diff = contentLowerRight.x - childLowerRight.x;
				delta.x = __min(ContentPos.x - ChildPos.x, diff);
			}

			//reposition the children if the child extent > the scroll content extent
			if (ChildPos.y > ContentPos.y)
				delta.y = ContentPos.y - ChildPos.y;
			else if (contentLowerRight.y > childLowerRight.y)
			{
				int diff = contentLowerRight.y - childLowerRight.y;
				delta.y = __min(ContentPos.y - ChildPos.y, diff);
			}

			// apply the deltas to the children...
			if (delta.x || delta.y)
			{
				ChildPos += delta;
				childLowerRight += delta;
			}
			// enable needed scroll bars
			if (ChildSize.x > ContentSize.x)
				bHBarEnabled = TRUE;
			if (ChildSize.y > ContentSize.y)
				bVBarEnabled = TRUE;
			ChildRelPos = ContentPos - ChildPos;
		}
	}

	void BeginDrag(int pos, BOOL bVert)
	{
		if (bVert)
		{
			ChildRelPosAnchor = ChildRelPos;
			nThumbMouseDragPos = pos - nVThumbPos;
		}
		else
		{
			ChildRelPosAnchor = ChildRelPos;
			nThumbMouseDragPos = pos - nHThumbPos;
		}
	}

	BOOL Drag(const UPoint& pt, BOOL bVert, UPoint& NewOffset)
	{
		if (bVert)
		{
			URect ToleranceRect;
			ToleranceRect = rcVTrack;
			ToleranceRect.InflateRect(-SCROLLBAR_DRAG_TOLERANCE, -SCROLLBAR_DRAG_TOLERANCE);
			if (ToleranceRect.PointInRect(pt))
			//if (rcVTrack.PointInRect(pt))
			{
				int newVThumbPos = pt.y - nThumbMouseDragPos;
				if(newVThumbPos != nVThumbPos)
				{
					int newVPos = (newVThumbPos - rcVTrack.top) *
						(ChildSize.y - ContentSize.y) /(rcVTrack.GetHeight() - nVThumbSize);
					
					NewOffset.Set(ChildRelPosAnchor.x, newVPos);
					return TRUE;
				}
			}
			else
			{
				NewOffset.Set(ChildRelPosAnchor.x, ChildRelPosAnchor.y);
				return TRUE;
			}
		}
		else
		{
			URect ToleranceRect;
			ToleranceRect = rcHTrack;
			ToleranceRect.InflateRect(-SCROLLBAR_DRAG_TOLERANCE, -SCROLLBAR_DRAG_TOLERANCE);
			if (ToleranceRect.PointInRect(pt))
			//if (rcHTrack.PointInRect(pt))
			{
				int newHThumbPos = pt.x - nThumbMouseDragPos;
				if(newHThumbPos != nHThumbPos)
				{
					int newHPos = (newHThumbPos - rcHTrack.left) *
						(ChildSize.x - ContentSize.x) /
						(rcHTrack.GetWidth() - nHThumbSize);

					NewOffset.Set(newHPos, ChildRelPosAnchor.y);
					return TRUE;
				}
			}
			else
			{
				NewOffset.Set(ChildRelPosAnchor.x, ChildRelPosAnchor.y);
				return TRUE;
			}
		}
		return FALSE;
	}

	BOOL ScrollTo(int x, int y, UPoint& Delta)
	{
		if (x > ChildSize.x - ContentSize.x)
			x = ChildSize.x - ContentSize.x;
		if (x < 0)
			x = 0;
		if (y > ChildSize.y - ContentSize.y)
			y = ChildSize.y - ContentSize.y;
		if (y < 0)
			y = 0;

		Delta.x = x - ChildRelPos.x;
		Delta.y = y - ChildRelPos.y;
		if (Delta.x == 0 && Delta.y == 0)
		{
			return FALSE;
		}
		ChildRelPos += Delta;
		ChildPos -= Delta;
		return TRUE;
	}
};


UIMP_CLASS(UScrollBar, UControl);

void UScrollBar::StaticInit()
{
	UREG_PROPERTY("ChildMargin", UPT_POINT, UFIELD_OFFSET(UScrollBar, mChildMargin));
	UREG_PROPERTY("ShowScrollBkg", UPT_BOOL, UFIELD_OFFSET(UScrollBar, mbShowScrollBkg));
	UREG_PROPERTY("AllShowBar", UPT_BOOL, UFIELD_OFFSET(UScrollBar, mbAllShowScrollBar));
	//addField("willFirstRespond",     TypeBool,    Offset(mWillFirstRespond, UScrollBar));
	//addField("hScrollBar",           TypeEnum,    Offset(mForceHScrollBar, UScrollBar), 1, &gScrollBarTable);
	//addField("vScrollBar",           TypeEnum,    Offset(mForceVScrollBar, UScrollBar), 1, &gScrollBarTable);
	//addField("constantThumbHeight",  TypeBool,    Offset(m_bFixThumbLength, UScrollBar));
	//addField("childMargin",          TypePoint2I, Offset(mChildMargin, UScrollBar));

}

UScrollBar::UScrollBar()
{
	mChildMargin.Set(0,0);
	mbShowScrollBkg = true;
	mbAllShowScrollBar = TRUE;
	mBorderThickness = 1;

	mScrollBarDragTolerance = 130;
	m_bBarDepressed = FALSE;
	m_eCurActiveRegion = SBR_UNKNOWN;

	mWillFirstRespond = TRUE;
	//	m_bFixThumbLength = FALSE;
	//	mIsContainer = TRUE;

	mForceVScrollBar = USBSM_FORCEON;
	mForceHScrollBar = USBSM_FORCEON;
	m_Size.Set(200,200);

	m_pScrollCtx = new ScrollContext;
	assert(m_pScrollCtx);
}

UScrollBar::~UScrollBar()
{
	if (m_pScrollCtx)
	{
		delete m_pScrollCtx;
	}
}

UPoint UScrollBar::GetChildPos() 
{ 
	assert(m_pScrollCtx);
	return m_pScrollCtx->ChildPos;

}
UPoint UScrollBar::GetChildRelPos() 
{ 
	assert(m_pScrollCtx);
	return m_pScrollCtx->ChildRelPos; 
};
UPoint UScrollBar::GetChildSize() 
{ 
	assert(m_pScrollCtx);
	return m_pScrollCtx->ChildSize; 
}
UPoint UScrollBar::GetContentSize() 
{ 
	assert(m_pScrollCtx);
	return m_pScrollCtx->ContentSize; 
}

int UScrollBar::GetBarSize() const
{
	return m_pScrollCtx->nArrowWidth;
}

BOOL UScrollBar::HasHBar() const  
{ 
	return m_pScrollCtx->bHasHScrollBar; 
}

BOOL UScrollBar::HasVBar() const                            
{ 
	return m_pScrollCtx->bHasVScrollBar ; 
}

BOOL UScrollBar::IsHBarEnable() const                        
{
	return m_pScrollCtx->bHBarEnabled; 
}

BOOL UScrollBar::IsVBarEnable() const                        
{ 
	return m_pScrollCtx->bVBarEnabled; 
}

void UScrollBar::OnSize(const UPoint& NewSize)
{
	UControl::OnSize(NewSize);
	ReCalLayout();
}

void UScrollBar::childResized(UControl *child)
{
	//Parent::childResized(child);
	ReCalLayout();
}

BOOL UScrollBar::OnCreate()
{
	if (!UControl::OnCreate())
		return FALSE;

	if (m_Style->m_spSkin)
	{
		ReCalLayout();
	}else
	{
		UTRACE("scrollbar without skin.");
		return FALSE;
	}
	//mTextureObject = m_Style->mTextureObject;
	//if (mTextureObject && (m_Style->constructBitmapArray() >= BmpStates * BmpCount))
	//{
	//	mBitmapBounds = m_Style->mBitmapArrayRects.address();

	//	//init
	//	mBaseThumbSize = mBitmapBounds[BmpStates * BmpVThumbTopCap].extent.y +
	//		mBitmapBounds[BmpStates * BmpVThumbBottomCap].extent.y;
	//	nArrowWidth      = mBitmapBounds[BmpStates * BmpVPage].extent.x;
	//	nArrowHeight = mBitmapBounds[BmpStates * BmpUp].extent.y;
	//	ReCalLayout();
	//} 
	//else
	//{
	//	Con::warnf("No texture loaded for scroll control named %s with profile %s", getName(), m_Style->getName());
	//}
	return TRUE;
}

void UScrollBar::OnDestroy()
{
	// Reset the mouse tracking state of this control
	//  when it is put to sleep
	m_bBarDepressed = FALSE;
	m_eCurActiveRegion = SBR_UNKNOWN;

	UControl::OnDestroy();
	//	mTextureObject = NULL;
}

BOOL UScrollBar::CalcChildSize(UPoint& Pos, UPoint& Size)
{
	if (m_Childs.empty())
	{
		UTRACE("Scrollbar must has a childctrl.");
		return FALSE;
	}
	UControl *pScollContent = m_Childs[0];
	Size = pScollContent->GetWindowSize();
	Pos = pScollContent->GetWindowPos();
	for(unsigned int ui = 1; ui < m_Childs.size(); ui++)
	{
		pScollContent = m_Childs[ui];
		if(!pScollContent->IsVisible())
			continue;
		//Size.x += pScollContent->GetWindowSize().x;
		Size.y += pScollContent->GetWindowSize().y;

		if(pScollContent->GetWindowPos().x < Pos.x)
			Pos.x = pScollContent->GetWindowPos().x;
		if(pScollContent->GetWindowPos().y < Pos.y)
			Pos.y = pScollContent->GetWindowPos().y;
	}
	return TRUE;
}

URect lastVisRect;

void UScrollBar::scrollRectVisible(const URect& rect)
{
	// rect is passed in virtual client space
	URect Rect = rect;
	if(Rect.GetSize().x > m_pScrollCtx->ContentSize.x)
		Rect.SetWidth(m_pScrollCtx->ContentSize.x);
	if(Rect.GetSize().y > m_pScrollCtx->ContentSize.y)
		Rect.SetHeight(m_pScrollCtx->ContentSize.y);

	// Determine the points bounding the requested rectangle
	ULog("%d,%d,%d,%d", Rect.pt0.x, Rect.pt0.y, Rect.pt1.y, Rect.pt1.y);
	UPoint rectUpperLeft  = Rect.GetPosition();
	UPoint rectLowerRight = Rect.GetPosition() + Rect.GetSize();

	lastVisRect = Rect;

	// Determine the points bounding the actual visible area...
	UPoint visUpperLeft = m_pScrollCtx->ChildRelPos;
	UPoint visLowerRight = m_pScrollCtx->ChildRelPos + m_pScrollCtx->ContentSize;
	UPoint delta(0,0);

	// We basically try to make sure that first the top left of the given
	// rect is visible, and if it is, then that the bottom right is visible.

	// Make sure the rectangle is visible along the X axis...
	if(rectUpperLeft.x < visUpperLeft.x)
		delta.x = rectUpperLeft.x - visUpperLeft.x;
	else if(rectLowerRight.x > visLowerRight.x)
		delta.x = rectLowerRight.x - visLowerRight.x;

	// Make sure the rectangle is visible along the Y axis...
	if(rectUpperLeft.y < visUpperLeft.y)
		delta.y = rectUpperLeft.y - visUpperLeft.y;
	else if(rectLowerRight.y > visLowerRight.y)
		delta.y = rectLowerRight.y - visLowerRight.y;

	// If we had any changes, scroll, otherwise don't.
	ULog("%d,%d,%d,%d,%d", rectUpperLeft.y, visUpperLeft.y, rectLowerRight.y, visLowerRight.y, delta.y);
	if(delta.x || delta.y)
		scrollDelta(delta.x, delta.y);
}


//void UScrollBar::addObject(SimObject *object)
//{
//	Parent::addObject(object);
//	ReCalLayout();
//}

UControl* UScrollBar::FindHitWindow(const UPoint &pt, int initialLayer)
{
	if(pt.x < m_Style->mBorderThickness || pt.y < m_Style->mBorderThickness)
		return this;
	if(pt.x >= m_Size.x - m_Style->mBorderThickness - (m_pScrollCtx->bHasVScrollBar ? m_pScrollCtx->nArrowWidth : 0) ||
		pt.y >= m_Size.y - m_Style->mBorderThickness - (m_pScrollCtx->bHasHScrollBar ? m_pScrollCtx->nArrowWidth : 0))
		return this;
	return UControl::FindHitWindow(pt, initialLayer);
}

void UScrollBar::SetBarShowMode(EUScrollBarShowMode eVertShowMode, EUScrollBarShowMode eHoriShowMode)
{
	mForceVScrollBar = eVertShowMode;
	mForceHScrollBar = eHoriShowMode;
	ReCalLayout();
}

void UScrollBar::ReCalLayout()
{
	int thickness = (m_Style ? m_Style->mBorderThickness : 1);
	UPoint borderExtent(thickness, thickness);

	UPoint SBPos = borderExtent + mChildMargin;
	UPoint SBSize = m_Size - SBPos * 2;
	UPoint ChildPos;
	UPoint ChildSize;
	UPoint MoveChild;
	if (CalcChildSize(ChildPos, ChildSize))
	{
		m_pScrollCtx->UpdateSize(SBPos, SBSize, &ChildPos, &ChildSize, mForceVScrollBar, mForceHScrollBar,MoveChild);
	}else
	{
		m_pScrollCtx->UpdateSize(SBPos, SBSize, NULL, NULL, mForceVScrollBar, mForceHScrollBar,MoveChild);
	}
	
	if (MoveChild.x != 0 || MoveChild.y != 0)
	{
		UPoint NewPos;
		for (int i = 0; i < m_Childs.size(); i++)
		{
			UControl* pChild = m_Childs[i];
			NewPos = pChild->GetWindowPos() + MoveChild;
			pChild->SetPosition(NewPos);
		}
	}

	m_pScrollCtx->ResetBarRect(m_Style->m_spSkin, m_Position, m_Size, thickness);

	UpdateThumb();
}


void UScrollBar::UpdateThumb()
{
	assert(m_pScrollCtx);
	m_pScrollCtx->UpdateThumb();
}


void UScrollBar::scrollDelta(int deltaX, int deltaY)
{
	scrollTo(m_pScrollCtx->ChildRelPos.x + deltaX, m_pScrollCtx->ChildRelPos.y + deltaY);
}

void UScrollBar::scrollTo(int x, int y)
{
	if(m_Childs.empty())
		return;

	UPoint Delta;
	if(m_pScrollCtx->ScrollTo(x, y, Delta))
	{
		UPoint CtrlPos;
		for (int i = 0; i < m_Childs.size(); i++)
		{
			UControl* pChild = m_Childs[i];
			CtrlPos = pChild->GetWindowPos() - Delta;
			pChild->SetPosition(CtrlPos);
		}
	}
	UpdateThumb();
}

UScrollBar::EUSBRegion UScrollBar::HitTestScrollBar(const UPoint &pt) const
{
	if (m_pScrollCtx)
	{
		return m_pScrollCtx->HitTest(pt);
	}
	return SBR_UNKNOWN;
}

BOOL UScrollBar::wantsTabListMembership()
{
	return TRUE;
}

BOOL UScrollBar::loseFirstResponder()
{
	return TRUE;
}

BOOL UScrollBar::becomeFirstResponder()
{
	return mWillFirstRespond;
}

BOOL UScrollBar::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	if (mWillFirstRespond)
	{
		switch (nKeyCode)
		{
		case LK_RIGHT:
			scrollByRegion(SBR_RIGHT);
			return TRUE;

		case LK_LEFT:
			scrollByRegion(SBR_LEFT);
			return TRUE;

		case LK_DOWN:
			scrollByRegion(SBR_DOWN);
			return TRUE;

		case LK_UP:
			scrollByRegion(SBR_UP);
			return TRUE;

		case LK_PGUP:
			scrollByRegion(SBR_PGUP);
			return TRUE;

		case LK_PGDN:
			scrollByRegion(SBR_PGDN);
			return TRUE;
		}
	}
	return UControl::OnKeyDown(nKeyCode, nRepCnt, nFlags);
}

void UScrollBar::OnMouseDown(const UPoint& pt, UINT nRepCnt, UINT uFlags)
{
	CaptureControl();
	UPoint curMousePos = ScreenToWindow(pt);
	m_eCurActiveRegion = HitTestScrollBar(curMousePos);
	m_bBarDepressed = TRUE;

	// Set a 0.5 second delay before we start scrolling
//	mLastUpdated = timeGetTime() + 500;

	scrollByRegion(m_eCurActiveRegion);

	if (m_eCurActiveRegion == SBR_VTHUMB)
	{
		m_pScrollCtx->BeginDrag(curMousePos.y, TRUE);
	}
	else if (m_eCurActiveRegion == SBR_HTHUMB)
	{
		m_pScrollCtx->BeginDrag(curMousePos.x, FALSE);
	}
}

void UScrollBar::OnMouseUp(const UPoint& position, UINT flags)
{
	ReleaseCapture();
	UPoint curMousePos = ScreenToWindow(position);
	m_eCurActiveRegion = HitTestScrollBar(curMousePos);
	m_bBarDepressed = FALSE;
}

void UScrollBar::OnMouseMove(const UPoint& position, UINT flags)
{
	UPoint curMousePos = ScreenToWindow(position);
	m_eCurActiveRegion = HitTestScrollBar(curMousePos);
	UControl::OnMouseMove(position, flags);
}

void UScrollBar::OnMouseDragged(const UPoint& position, UINT flags)
{
	UPoint curMousePos = ScreenToWindow(position);

	if ( (m_eCurActiveRegion != SBR_VTHUMB) && (m_eCurActiveRegion != SBR_HTHUMB) )
	{
		EUSBRegion hit = HitTestScrollBar(curMousePos);
		if (hit != m_eCurActiveRegion)
			m_bBarDepressed = FALSE;
		else
			m_bBarDepressed = TRUE;
		return;
	}

	// ok... if the mouse is 'near' the scroll bar, scroll with it
	// otherwise, snap back to the previous position.



	if (m_eCurActiveRegion == SBR_VTHUMB)
	{
		UPoint NewOffset;
		if(m_pScrollCtx->Drag(curMousePos, TRUE, NewOffset))
		{
			scrollTo(NewOffset.x, NewOffset.y);
		}
		
	}
	else if (m_eCurActiveRegion == SBR_HTHUMB)
	{
		UPoint NewOffset;
		if(m_pScrollCtx->Drag(curMousePos, FALSE, NewOffset))
		{
		scrollTo(NewOffset.x, NewOffset.y);
		}
	}
}

BOOL UScrollBar::OnMouseWheel(const UPoint& position, int delta , UINT flags)
{
	if ( !m_bCreated || !m_Visible )
		return FALSE;

	UPoint previousPos = m_pScrollCtx->ChildPos;
	scrollByRegion((flags & LKM_CTRL) ? (delta > 0?SBR_PGUP : SBR_PGDN) : (delta > 0 ? SBR_UP : SBR_DOWN));

	//// Tell the kids that the mouse moved (relatively):
	//iterator itr;
	//for ( itr = begin(); itr != end(); itr++ )
	//{
	//	UControl* grandKid = static_cast<UControl*>( *itr );
	//	grandKid->onMouseMove( event );
	//}

	//// If no scrolling happened (already at the top), pass it on to the parent.
	//UControl* parent = getParent();
	//if (parent && (previousPos == m_pScrollCtx->ChildPos))
	//	return parent->onMouseWheelUp(event);

	return TRUE;
}
//
//BOOL UScrollBar::onMouseWheelDown(const GuiEvent &event)
//{
//	if ( !mAwake || !m_Visible )
//		return( FALSE );
//
//	UPoint previousPos = m_pScrollCtx->ChildPos;
//	scrollByRegion((event.modifier & SI_CTRL) ? SBR_PGDN : SBR_DOWN);
//
//	// Tell the kids that the mouse moved (relatively):
//	iterator itr;
//	for ( itr = begin(); itr != end(); itr++ )
//	{
//		UControl* grandKid = static_cast<UControl *>( *itr );
//		grandKid->onMouseMove( event );
//	}
//
//	// If no scrolling happened (already at the bottom), pass it on to the parent.
//	UControl* parent = getParent();
//	if (parent && (previousPos == m_pScrollCtx->ChildPos))
//		return parent->onMouseWheelDown(event);
//
//	return TRUE;
//}

void UScrollBar::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);

	// Short circuit if not depressed to save cycles
	if( m_bBarDepressed != TRUE )
		return;

	//default to one second, though it shouldn't be necessary
	UINT timeThreshold = 1.0f;

	// We don't want to scroll by pages at an interval the same as when we're scrolling
	// using the arrow buttons, so adjust accordingly.
	switch( m_eCurActiveRegion )
	{
	case SBR_PGUP:
	case SBR_PGDN:
	case SBR_PGLEFT:
	case SBR_PGRIGHT:
		timeThreshold = 0.2f;
		break;
	case SBR_UP:
	case SBR_DOWN:
	case SBR_LEFT:
	case SBR_RIGHT:
		timeThreshold = 0.2f;
		break;
	default:
		// Neither a button or a page, don't scroll (shouldn't get here)
		return;
		break;
	};


	if ( ( fDeltaTime > 0.0f ) && ( fDeltaTime > timeThreshold ) )
	{

	//	mLastUpdated = timeGetTime();
		scrollByRegion(m_eCurActiveRegion);
	}

}

void UScrollBar::scrollByRegion(EUSBRegion reg)
{
	//if(!size())
	//	return;
	UControl *content = (UControl *) m_Childs[0];
	UINT rowHeight, columnWidth;
	//UINT pageHeight, pageWidth;

	content->GetScrollLineSizes(&rowHeight, &columnWidth);

	//UINT rowHeight = 10;
	//UINT columnWidth = 10;
	UINT pageHeight, pageWidth;

	if(rowHeight >= m_pScrollCtx->ContentSize.y)
		pageHeight = 1;
	else
		pageHeight = m_pScrollCtx->ContentSize.y - rowHeight;

	if(columnWidth >= m_pScrollCtx->ContentSize.x)
		pageWidth = 1;
	else
		pageWidth = m_pScrollCtx->ContentSize.x - columnWidth;

	if (m_pScrollCtx->bVBarEnabled)
	{
		switch(reg)
		{
		case SBR_PGUP:
			scrollDelta(0, -(int)pageHeight);
			break;
		case SBR_PGDN:
			scrollDelta(0, pageHeight);
			break;
		case SBR_UP:
			scrollDelta(0, -(int)rowHeight);
			break;
		case SBR_DOWN:
			scrollDelta(0, rowHeight);
			break;
		}
	}

	if (m_pScrollCtx->bHBarEnabled)
	{
		switch(reg)
		{
		case SBR_PGLEFT:
			scrollDelta(-(int)pageWidth, 0);
			break;
		case SBR_PGRIGHT:
			scrollDelta(pageWidth, 0);
			break;
		case SBR_LEFT:
			scrollDelta(-(int)columnWidth, 0);
			break;
		case SBR_RIGHT:
			scrollDelta(columnWidth, 0);
			break;
		}
	}
}


void UScrollBar::OnRender(const UPoint& offset, const URect &updateRect)
{
	URect r(offset, m_Size);

	UPoint ContentPos = m_pScrollCtx->ContentPos + offset;
	URect ContentRect(ContentPos, m_pScrollCtx->ContentSize);
	ContentRect = ContentRect.GetIntersection(updateRect);

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
	g_pRenderInterface->SetScissorRect(ContentRect);
	UControl::OnRender(offset, ContentRect);

	//sm_UiRender->SetClipRect(r);
	g_pRenderInterface->SetScissorRect(r);

	// draw scroll bars
	if (m_pScrollCtx->bHasVScrollBar)
		drawVScrollBar(offset);

	if (m_pScrollCtx->bHasHScrollBar)
		drawHScrollBar(offset);

	//draw the scroll corner
	if (m_pScrollCtx->bHasVScrollBar && m_pScrollCtx->bHasHScrollBar)
		drawScrollCorner(offset);

	g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
}

void UScrollBar::drawBorder( const UPoint &offset, BOOL /*isFirstResponder*/ )
{

}

void UScrollBar::drawVScrollBar(const UPoint &offset)
{
	USkin* pSkin = m_Style->m_spSkin;
	if (pSkin == NULL)
	{
		return;
	}
	if (mbAllShowScrollBar || m_pScrollCtx->bVBarEnabled)
	{
		UPoint pos = offset;
		pos.x += m_pScrollCtx->rcUpArrow.left;
		pos.y += m_pScrollCtx->rcUpArrow.top;
		// up arrow
		int nSkinIndex = SBS_UP_ARROW_N;
		if (m_eCurActiveRegion == SBR_UP)
		{
			nSkinIndex = m_bBarDepressed ? SBS_UP_ARROW_D : SBS_UP_ARROW_H;
		}
		sm_UiRender->DrawSkin(pSkin, nSkinIndex,pos);

		URect DrawRect;
		// bkg ground
		{
			pos.y += m_pScrollCtx->nArrowHeight;

			DrawRect.left = pos.x;
			DrawRect.top = pos.y;
			DrawRect.right = (DrawRect.left + m_pScrollCtx->nArrowWidth);
			DrawRect.bottom = m_pScrollCtx->rcDownArrow.top + offset.y;
			nSkinIndex = SBS_VERT_BG;
			if(mbShowScrollBkg)
			{
				sm_UiRender->DrawSkin(pSkin, nSkinIndex,DrawRect);
			}
		}

		// DOWN Arrow
		{
			pos.y = DrawRect.bottom;
			nSkinIndex = SBS_DOWN_ARROW_N;
			if (m_eCurActiveRegion == SBR_DOWN)
			{
				nSkinIndex = m_bBarDepressed ? SBS_DOWN_ARROW_D : SBS_DOWN_ARROW_H;
			}

			sm_UiRender->DrawSkin(pSkin, nSkinIndex,pos);
		}

		if (m_pScrollCtx->bVBarEnabled)
		{
			// Draw Thumb
			BOOL thumbSelected = (m_eCurActiveRegion == SBR_VTHUMB && m_bBarDepressed);
			if (m_pScrollCtx->bFixThumbLength)
			{
				pos.y = m_pScrollCtx->nVThumbPos + offset.y;
				nSkinIndex = thumbSelected ? SBS_VERT_THUMB_D:SBS_VERT_THUMB_N;
				int _width = pSkin->GetSkinItemRect(nSkinIndex)->GetWidth();
				pos.x = (DrawRect.left + DrawRect.right)/2 - _width/2;
				sm_UiRender->DrawSkin(pSkin, nSkinIndex,pos);
			}else{

			}
	}
	

		//	int ttop = (thumbSelected ? BmpStates * BmpVThumbTopCap + BmpHilite : BmpStates * BmpVThumbTopCap);
		//	int tmid = (thumbSelected ? BmpStates * BmpVThumb + BmpHilite : BmpStates * BmpVThumb);
		//	int tbot = (thumbSelected ? BmpStates * BmpVThumbBottomCap + BmpHilite : BmpStates * BmpVThumbBottomCap);

		//	// draw the thumb
		//	GFX->getDrawUtil()->clearBitmapModulation();
		//	GFX->getDrawUtil()->drawBitmapSR(mTextureObject, pos, mBitmapBounds[ttop]);
		//	pos.y += mBitmapBounds[ttop].extent.y;
		//	end = mVThumbPos + mVThumbSize - mBitmapBounds[tbot].extent.y + offset.y;

		//	if (end > pos.y)
		//	{
		//		GFX->getDrawUtil()->clearBitmapModulation();
		//		GFX->getDrawUtil()->drawBitmapStretchSR(mTextureObject, URect(pos, UPoint(mBitmapBounds[tmid].extent.x, end - pos.y)), mBitmapBounds[tmid]);
		//	}

		//	pos.y = end;
		//	GFX->getDrawUtil()->clearBitmapModulation();
		//	GFX->getDrawUtil()->drawBitmapSR(mTextureObject, pos, mBitmapBounds[tbot]);
		//	pos.y += mBitmapBounds[tbot].extent.y;
		//	end = rcVTrack.top + rcVTrack.extent.y - 1 + offset.y;

		//	bitmap = (m_eCurActiveRegion == SBR_PGDN && m_bBarDepressed) ? BmpStates * BmpVPage + BmpHilite : BmpStates * BmpVPage;
		//	if (end > pos.y)
		//	{
		//		GFX->getDrawUtil()->clearBitmapModulation();
		//		GFX->getDrawUtil()->drawBitmapStretchSR(mTextureObject, URect(pos, UPoint(mBitmapBounds[bitmap].extent.x, end - pos.y)), mBitmapBounds[bitmap]);
		//	}

		//	pos.y = end;
	}
}

void UScrollBar::drawHScrollBar(const UPoint &offset)
{
	USkin* pSkin = m_Style->m_spSkin;
	if (pSkin == NULL)
	{
		return;
	}

	if (mbAllShowScrollBar || m_pScrollCtx->bHBarEnabled )
	{
		UPoint pos = offset;
		pos.x += m_pScrollCtx->rcLeftArrow.left;
		pos.y += m_pScrollCtx->rcLeftArrow.top;
		// left arrow
		int nSkinIndex = SBS_LEFT_ARROW_N;
		if (m_eCurActiveRegion == SBR_LEFT)
		{
			nSkinIndex = m_bBarDepressed ? SBS_LEFT_ARROW_D : SBS_LEFT_ARROW_H;
		}
		sm_UiRender->DrawSkin(pSkin, nSkinIndex,pos);

		// bkg
		URect DrawRect;
		// bkg ground
		{
			pos.x += m_pScrollCtx->nArrowWidth;

			DrawRect.left = pos.x;
			DrawRect.top = pos.y;
			DrawRect.right = m_pScrollCtx->rcRightArrow.left + offset.x;
			DrawRect.bottom = (DrawRect.top + m_pScrollCtx->nArrowHeight);
			nSkinIndex = SBS_HORI_BG;
			if(mbShowScrollBkg)
			{
				sm_UiRender->DrawSkin(pSkin, nSkinIndex,DrawRect);
			}
		}

		// Right Arrow
		{
			pos.x = DrawRect.right;
			nSkinIndex = SBS_RIGHT_ARROW_N;
			if (m_eCurActiveRegion == SBR_RIGHT)
			{
				nSkinIndex = m_bBarDepressed ? SBS_RIGHT_ARROW_D : SBS_RIGHT_ARROW_H;
			}
			sm_UiRender->DrawSkin(pSkin, nSkinIndex,pos);
		}

		if (m_pScrollCtx->bHBarEnabled)
		{
			BOOL thumbSelected = (m_eCurActiveRegion == SBR_HTHUMB && m_bBarDepressed);
			if (m_pScrollCtx->bFixThumbLength)
			{
				pos.x = m_pScrollCtx->nHThumbPos + offset.x;
				nSkinIndex = thumbSelected ? SBS_HORI_THUMB_D:SBS_HORI_THUMB_N;
				sm_UiRender->DrawSkin(pSkin, nSkinIndex,pos);
			}else{

			}
	}
	
	//	int ttop = (thumbSelected ? BmpStates * BmpHThumbLeftCap + BmpHilite : BmpStates * BmpHThumbLeftCap );
	//	int tmid = (thumbSelected ? BmpStates * BmpHThumb + BmpHilite : BmpStates * BmpHThumb);
	//	int tbot = (thumbSelected ? BmpStates * BmpHThumbRightCap + BmpHilite : BmpStates * BmpHThumbRightCap);

	//	// draw the thumb
	//	GFX->getDrawUtil()->clearBitmapModulation();
	//	GFX->getDrawUtil()->drawBitmapSR(mTextureObject, pos, mBitmapBounds[ttop]);
	//	pos.x += mBitmapBounds[ttop].extent.x;
	//	end = mHThumbPos + mHThumbSize - mBitmapBounds[tbot].extent.x + offset.x;
	//	if (end > pos.x)
	//	{
	//		GFX->getDrawUtil()->clearBitmapModulation();
	//		GFX->getDrawUtil()->drawBitmapStretchSR(mTextureObject, URect(pos, UPoint(end - pos.x, mBitmapBounds[tmid].extent.y)), mBitmapBounds[tmid]);
	//	}

	//	pos.x = end;
	//	GFX->getDrawUtil()->clearBitmapModulation();
	//	GFX->getDrawUtil()->drawBitmapSR(mTextureObject, pos, mBitmapBounds[tbot]);
	//	pos.x += mBitmapBounds[tbot].extent.x;
	//	end = rcHTrack.left + rcHTrack.extent.x - 1 + offset.x;

	//	bitmap = ((m_eCurActiveRegion == SBR_PGRIGHT && m_bBarDepressed) ? BmpStates * BmpHPage + BmpHilite : BmpStates * BmpHPage);

	//	if (end > pos.x)
	//	{
	//		GFX->getDrawUtil()->clearBitmapModulation();
	//		GFX->getDrawUtil()->drawBitmapStretchSR(mTextureObject, URect(pos, UPoint(end - pos.x, mBitmapBounds[bitmap].extent.y)), mBitmapBounds[bitmap]);
	//	}

	//	pos.x = end;
	}
}

void UScrollBar::drawScrollCorner(const UPoint &offset)
{
	/*UPoint pos = offset;
	pos.x += rcRightArrow.left + rcRightArrow.extent.x;
	pos.y += rcRightArrow.top;
	GFX->getDrawUtil()->clearBitmapModulation();
	GFX->getDrawUtil()->drawBitmapSR(mTextureObject, pos, mBitmapBounds[BmpStates * BmpResize]);*/
}

void UScrollBar::autoScroll(EUSBRegion reg)
{
	scrollByRegion(reg);
}
void UScrollBar::DispatchChildNotify(int mess)
{
	DispatchNotify(mess);
}