#include "StdAfx.h"
#include "UScrollText.h"

#define MAXSCROLLTEXTLEN 2048
class ScrollText//走马灯渲染类
{
	struct ScrollTextElement
	{
		UINT	ID;
		WCHAR Text[MAXSCROLLTEXTLEN];//走马灯文本
		UINT    TextWidth;
		UColor  TextColor;//走马灯文本颜色
		UPoint  ScrollToPos;
	};
public:
	ScrollText();
	~ScrollText();
	void Create(const UPoint &pos , const UINT Width,IUFont *pFont);
	void Destroy();

	void SetDeltaTime(UINT Time);
	void ADDScrollText(UINT ID, const char * text , const UColor &color);
	void ADDScrollText(UINT ID, const WCHAR * text , const UColor &color);
	void RemoveScrollText(const char * text);
	void RemoveScrollText(const WCHAR *text);
	void RemoveScrollText(UINT ID);

	void UpdataText(float fDeltaTime);
	void DrawScrollText(URender * pRender,const UPoint &offset, const UPoint &Size);
private:
	UINT TextIndex;
	float mdtime;
	float mDeltaTime;
	UPoint mTextPos;
	UINT mWidth;
	BOOL m_bCreate;
	UFontPtr mFont;
	std::vector<ScrollTextElement> mScrollTextCon;
};
ScrollText::ScrollText()
{
	mTextPos.Set(0,0);
	mWidth = 0;
	mFont = NULL;
	mdtime = 0.0f;
	TextIndex = 0;
	mDeltaTime = 0.1f;
	m_bCreate = FALSE;
}
ScrollText::~ScrollText()
{

}
void ScrollText::Create(const UPoint &pos , const UINT Width , IUFont *pFont)
{
	mTextPos = pos;
	mWidth = Width;
	mFont = pFont;
	m_bCreate = TRUE;
}
void ScrollText::Destroy()
{
	mTextPos.Set(0,0);
	mWidth = 0;
	mdtime = 0.0f;
	TextIndex = 0;
	m_bCreate = FALSE;
	mDeltaTime = 0.1f;
	mScrollTextCon.clear();
}
void ScrollText::SetDeltaTime(UINT Time)
{
	mDeltaTime = Time/1000.0f;
}
void ScrollText::ADDScrollText(UINT ID, const char * text , const UColor &color)
{
	if (m_bCreate)
	{
		ScrollTextElement newElement;
		newElement.ID = ID;
		int nRet = MultiByteToWideChar(CP_UTF8, 0, text, strlen(text), newElement.Text, MAXSCROLLTEXTLEN - 1);
		newElement.Text[nRet] = 0;
		//newElement.Text.Set(text);
		newElement.TextWidth = mFont->GetStrWidth(newElement.Text);
		newElement.TextColor = color;
		newElement.ScrollToPos = /*mTextPos +*/ UPoint(mWidth,0);
		mScrollTextCon.push_back(newElement);
	}
}
void ScrollText::ADDScrollText(UINT ID, const WCHAR * text , const UColor &color)
{
	if (m_bCreate)
	{
		ScrollTextElement newElement;
		newElement.ID = ID;
		wcsncpy(newElement.Text, text, MAXSCROLLTEXTLEN);
		newElement.Text[MAXSCROLLTEXTLEN - 1] = 0;
		newElement.TextWidth = mFont->GetStrWidth(text);
		newElement.TextColor = color;
		newElement.ScrollToPos = /*mTextPos +*/ UPoint(mWidth,0);
		mScrollTextCon.push_back(newElement);
	}
}

void ScrollText::RemoveScrollText(const char * text)
{
	//有问题啊 暂时不要使用
	int nLen = strlen(text);
	if (nLen == 0)
	{
		return;
	}
	UFrameMarker<WCHAR> Marker(nLen + 1);
	int nRet = MultiByteToWideChar(CP_UTF8, 0, text, nLen, Marker, nLen);
	Marker[nRet] =0;
	RemoveScrollText(Marker);
}
void ScrollText::RemoveScrollText(const WCHAR *text)
{
	//有问题啊 暂时不要使用 莫名的内存混乱 难道是erase的用法的问题？
	std::vector<ScrollTextElement>::iterator it = mScrollTextCon.begin();
	while (it != mScrollTextCon.end())
	{
		ScrollTextElement &TElement = *it;
		if(wcscmp(text,TElement.Text) == 0)
		{
			it = mScrollTextCon.erase(it);
			TextIndex = 0;
			for (size_t i = 0 ; i < mScrollTextCon.size() ;i++ )
			{
				mScrollTextCon[i].ScrollToPos = UPoint(mWidth,0);
			}
			break;
		}
		++it;
	}	
}
void ScrollText::UpdataText(float fDeltaTime)
{
	if (!m_bCreate || !mScrollTextCon.size())
	{
		return;
	}
	mdtime += fDeltaTime;
	if (mdtime > mDeltaTime)
	{
		if (TextIndex >= 0 && TextIndex < mScrollTextCon.size())
		{
			mScrollTextCon[TextIndex].ScrollToPos -= UPoint(1,0);
		}
		if (mScrollTextCon[TextIndex].ScrollToPos.x + mScrollTextCon[TextIndex].TextWidth <= 0)
		{
			mScrollTextCon[TextIndex].ScrollToPos =  /*mTextPos +*/ UPoint(mWidth,0);
			TextIndex++;
			if (TextIndex >= mScrollTextCon.size())
			{
				TextIndex = 0;
			}
		}
		mdtime = 0.0f;
	}
}
void ScrollText::DrawScrollText(URender * pRender,const UPoint &offset, const UPoint &Size)
{
	if (!m_bCreate)
	{
		return;
	}
	if (TextIndex >= 0 && TextIndex < mScrollTextCon.size())
	{
		//URect oldclip = pRender->GetClipRect();
		UPoint size;
		size.Set(mScrollTextCon[TextIndex].TextWidth , Size.y/*mFont->GetHeight()*/);
		pRender->SetClipRect(URect(offset,UPoint(mWidth,Size.y/*mFont->GetHeight()*/)));
		UDrawText(pRender , mFont ,offset + mScrollTextCon[TextIndex].ScrollToPos , size , mScrollTextCon[TextIndex].TextColor ,
			mScrollTextCon[TextIndex].Text,UT_CENTER);
		mTextPos = offset;
		//pRender->SetClipRect(oldclip);
	}
}
void ScrollText::RemoveScrollText(UINT ID)
{
	std::vector<ScrollTextElement>::iterator it = mScrollTextCon.begin();
	while (it != mScrollTextCon.end())
	{
		if (it->ID == ID)
		{
			mScrollTextCon.erase(it);
			TextIndex = 0;
			return;
		}
		++it;
	}	
}
////////////////////////////////////////////////////////////////////////////////////////////
UIMP_CLASS(UScrollText,UControl);
void UScrollText::StaticInit()
{

}
UScrollText::UScrollText()
{
	mScrollText = new ScrollText;
}
UScrollText::~UScrollText()
{
	if (mScrollText)
	{
		delete mScrollText;
	}
}
BOOL UScrollText::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (mScrollText)
	{
		mScrollText->Create(m_Position,m_Size.x,m_Style->m_spFont);
	}
	return TRUE;
}
void UScrollText::OnDestroy()
{
	if (mScrollText)
	{
		mScrollText->Destroy();
	}
	UControl::OnDestroy();
}
void UScrollText::OnSize(const UPoint& NewSize)
{
	if (mScrollText)
	{
		mScrollText->Create(m_Position, m_Size.x, m_Style->m_spFont);
	}
	UControl::OnSize(NewSize);
}
void UScrollText::OnTimer(float fDeltaTime)
{
	if (mScrollText)
	{
		mScrollText->UpdataText(fDeltaTime);
	}
	UControl::OnTimer(fDeltaTime);
}
void UScrollText::OnRender(const UPoint& offset,const URect &updateRect)
{
	//UControl::OnRender(offset,updateRect);
	if (mScrollText)
	{
        g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
        g_pRenderInterface->SetScissorRect(sm_UiRender->GetClipRect());

		mScrollText->DrawScrollText(sm_UiRender, offset, updateRect.GetSize());

        g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
	}
}

void UScrollText::SetScrollSpeed(UINT Time)
{
	if (mScrollText)
	{
		mScrollText->SetDeltaTime(Time);
	}
}

void UScrollText::AddScrollText(UINT Id, const char * text , UColor color)
{
	if (mScrollText)
	{
		mScrollText->ADDScrollText(Id, text, color);
	}
}
void UScrollText::AddScrollText(UINT Id, const WCHAR * text , UColor color)
{
	if (mScrollText)
	{
		mScrollText->ADDScrollText(Id, text, color);
	}
}
void UScrollText::RemoveScrollText(const char * text)
{
	if (mScrollText)
	{
		mScrollText->RemoveScrollText(text);
	}
}
void UScrollText::RemoveScrollText(const WCHAR * text)
{
	if (mScrollText)
	{
		mScrollText->RemoveScrollText(text);
	}
}
void UScrollText::RemoveScrollText(UINT ID)
{
	if (mScrollText)
	{
		mScrollText->RemoveScrollText(ID);
	}
}