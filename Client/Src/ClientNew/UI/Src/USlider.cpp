#include "StdAfx.h"
#include "USlider.h"
#include "USystem.h"
UIMP_CLASS(USlider, UControl);
void USlider::StaticInit()
{
	UREG_PROPERTY("SliderButtonSkin", UPT_STRING, UFIELD_OFFSET(USlider, m_ButtonSkinName));
	UREG_PROPERTY("SliderBarSkin", UPT_STRING, UFIELD_OFFSET(USlider, m_BarSkinName));
}

USlider::USlider()
{
	m_nMin = 0;
	m_nMax = 100;
	m_nValue = 0;

	m_bPressed = false;

	m_ButtonSkin = NULL;
	m_BarSkin = NULL;
	m_rcButtonState = SLIDERBUTTON_NORMAL;
}

USlider::~USlider()
{

}

BOOL USlider::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_ButtonSkin == NULL)
	{
		if (m_ButtonSkinName.GetBuffer())
		{
			m_ButtonSkin = sm_System->LoadSkin(m_ButtonSkinName.GetBuffer());
			if (!m_ButtonSkin)
			{
				return FALSE;
			}
		}
	}	
	if (m_BarSkin == NULL)
	{
		if (m_BarSkinName.GetBuffer())
		{
			m_BarSkin = sm_System->LoadSkin(m_BarSkinName.GetBuffer());
			if (!m_BarSkin)
			{
				return FALSE;
			}
		}
	}
	UpdateRects();
	return TRUE;
}

void USlider::OnDestroy()
{
	m_bPressed = false;
	m_rcButtonState = SLIDERBUTTON_NORMAL;
	UControl::OnDestroy();
}

void USlider::OnChange(bool bResized)
{
	UControl::OnChange(bResized);
	UpdateRects();
}

void USlider::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	CaptureControl();
	URect BoundingBox( WindowToScreen( UPoint(0, 0) ), m_Size );
	if ( m_rcButton.PointInRect(point) )
	{
		m_bPressed = true;
		m_nDragX = point.x;

		m_nDragOffset = m_nButtonX - m_nDragX;

		m_rcButtonState = SLIDERBUTTON_PRESS;
		return;
	}

	if ( BoundingBox.PointInRect(point) )
	{
		//m_bPressed = true;
		m_nDragOffset = 0;
		if( point.x > m_nButtonX + BoundingBox.left )
		{
			SetValueInternal( m_nValue + 1, true );
			return;
		}

		if( point.x < m_nButtonX + BoundingBox.left )
		{
			SetValueInternal( m_nValue - 1, true );
			return;
		}

	}
}

void USlider::OnMouseUp(const UPoint& position, UINT flags)
{
	m_bPressed = false;
	ReleaseCapture();
}

void USlider::OnMouseMove(const UPoint& position, UINT flags)
{
	if( !m_bPressed )
	{
		if ( m_rcButton.PointInRect(position) )
		{
			m_rcButtonState = SLIDERBUTTON_HOVER;
		}
		else
		{
			m_rcButtonState = SLIDERBUTTON_NORMAL;
		}
	}
}

void USlider::OnMouseDragged(const UPoint& position, UINT flags)
{
	if( m_bPressed )
	{
		SetValueInternal( ValueFromPos( WindowToScreen( UPoint(0 ,0) ).x + position.x + m_nDragOffset ), true );
		return;
	}
}

BOOL USlider::HitTest(const UPoint& parentCoordPoint)
{
	if ( m_rcButton.PointInRect(GetParent()->WindowToScreen(parentCoordPoint)) )
	{
		return TRUE;
	}
	return UControl::HitTest(parentCoordPoint);
}

void USlider::OnMouseLeave(const UPoint& position, UINT flags)
{
	m_rcButtonState = SLIDERBUTTON_NORMAL;
}

void USlider::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_ButtonSkin/* && m_BarSkin*/)
	{
		if (m_Active)
		{
			if(m_BarSkin)
			{
				UPoint barpos = offset;
				URect SliderBar_SkinRect = *m_BarSkin->GetSkinItemRect(SLIDERBAR_NORMAL);
				barpos.y += (updateRect.GetHeight()>>1) - (SliderBar_SkinRect.GetHeight()>>1);
				if (barpos.y < 0)
					barpos.y = 0;

				URect DrawBarRect(barpos, SliderBar_SkinRect.GetSize());
				float fValue = updateRect.GetWidth() / SliderBar_SkinRect.GetWidth();
				int numItem = int(fValue);
				//float = fValue - numItem;
				for (int i = 0 ; i < numItem ; i++)
				{
					sm_UiRender->DrawSkin(m_BarSkin, SLIDERBAR_NORMAL, DrawBarRect);
					DrawBarRect.Offset(DrawBarRect.GetWidth(), 0);
				}
			}
			else
			{
				UPoint size = updateRect.GetSize();
				size.y *= 0.174f;
				UPoint pos(offset.x, offset.y + (GetHeight() - size.y) * 0.5f);
				URect DrawBarRect(pos, size);
				sm_UiRender->DrawRectFill(DrawBarRect, m_Style->mFillColor);
			}


			//DrawBarRect.SetWidth(updateRect.GetWidth());
			//sm_UiRender->DrawSkin(m_BarSkin, SLIDERBAR_NORMAL, DrawBarRect);

			//URect oldClip = sm_UiRender->GetClipRect();
			//sm_UiRender->SetClipRect( m_rcButton );
			sm_UiRender->DrawSkin(m_ButtonSkin, m_rcButtonState, m_rcButton);
			//sm_UiRender->SetClipRect( oldClip );
		}
		else
		{
			if(m_BarSkin)
			{
				UPoint barpos = offset;
				barpos.y += (updateRect.GetHeight()>>1) - (m_BarSkin->GetSkinItemRect(0)->GetHeight()>>1);
				if (barpos.y < 0)
				{
					barpos.y = 0;
				}
				URect DrawBarRect(barpos, m_BarSkin->GetSkinItemRect(SLIDERBAR_INACTIVE)->GetSize());
				float fValue = updateRect.GetWidth() / m_BarSkin->GetSkinItemRect(SLIDERBAR_INACTIVE)->GetWidth();
				int numItem = int(fValue);
				//float = fValue - numItem;
				for (int i = 0 ; i < numItem ; i++)
				{
					sm_UiRender->DrawSkin(m_BarSkin, SLIDERBAR_INACTIVE, DrawBarRect);
					DrawBarRect.Offset(DrawBarRect.GetWidth(), 0);
				}
			}
			else
			{
				UPoint size = updateRect.GetSize();
				size.y *= 0.174f;
				UPoint pos(offset.x, offset.y + (GetHeight() - size.y) * 0.5f);
				URect DrawBarRect(pos, size);
				sm_UiRender->DrawRectFill(DrawBarRect, m_Style->mFillColor);
			}
			//DrawBarRect.SetWidth(updateRect.GetWidth());
			//sm_UiRender->DrawSkin(m_BarSkin, SLIDERBAR_INACTIVE, DrawBarRect);
			//URect oldClip = sm_UiRender->GetClipRect();
			//sm_UiRender->SetClipRect( m_rcButton );
			sm_UiRender->DrawSkin(m_ButtonSkin, SLIDERBUTTON_INACTIVE, m_rcButton);
			//sm_UiRender->SetClipRect( oldClip );
		}
	}
	else
	{
		sm_UiRender->DrawRectFill(updateRect, m_Style->mFillColor);
		sm_UiRender->DrawRectFill(m_rcButton, m_Style->mCursorColor);
	}
}

void USlider::SetRange( int nMin, int nMax )
{
	m_nMin = nMin;
	m_nMax = nMax;

	SetValueInternal( m_nValue, false );
}
void USlider::SetValueInternal( int nValue, bool bFromInput )
{
	// Clamp to range
	nValue = __max( m_nMin, nValue );
	nValue = __min( m_nMax, nValue );

	if( nValue == m_nValue )
		return;

	m_nValue = nValue;
	UpdateRects();

	//DispathNotify
	DispatchNotify(USLIDER_CHANGE);
}

int USlider::ValueFromPos( int x )
{
	URect BoundingBox( WindowToScreen( UPoint(0, 0) ), m_Size );
	float fValuePerPixel = (float)(m_nMax - m_nMin) / BoundingBox.GetWidth() ;
	return (int) (0.5f + m_nMin + fValuePerPixel * (x - BoundingBox.left)) ; 
}

void USlider::UpdateRects()
{
	URect BoundingBox( WindowToScreen( UPoint(0, 0) ), m_Size );
	m_rcButton = BoundingBox;

	m_rcButton.right = m_rcButton.left + m_rcButton.GetHeight() ;
	m_rcButton.Offset( -m_rcButton.GetWidth() / 2, 0 );

	m_nButtonX = (int) ( (m_nValue - m_nMin) * (float)BoundingBox.GetWidth() / (m_nMax - m_nMin) );
	m_rcButton.Offset( m_nButtonX, 0 );
}