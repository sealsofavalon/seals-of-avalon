#include "stdafx.h"
#include "USoftKeyboard.h"
#include "UDesktop.h"

UIMP_CLASS(USoftKeyBoard::UKeyLock, UControl);
void USoftKeyBoard::UKeyLock::StaticInit()
{

}

USoftKeyBoard::UKeyLock::UKeyLock()
{
	m_IsLock = TRUE ;
	m_LockSkin = NULL;
}

USoftKeyBoard::UKeyLock::~UKeyLock()
{

}

BOOL USoftKeyBoard::UKeyLock::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE ;
	}
	if (m_LockSkin == NULL)
	{
		m_LockSkin = sm_System->LoadSkin("core\\SoftKeyBoard\\Lock.skin");
		if (!m_LockSkin)
		{
			return FALSE ;
		}
	}

	return TRUE;
}

void USoftKeyBoard::UKeyLock::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_LockSkin)
	{
		if (m_IsLock)
		{
			sm_UiRender->DrawSkin(m_LockSkin, 1, updateRect);
			
		}else
		{
			sm_UiRender->DrawSkin(m_LockSkin, 0, updateRect);
		}
	}
}

void USoftKeyBoard::UKeyLock::OnMouseUp(const UPoint& position, UINT flags)
{
	UControl::OnMouseUp( position,flags);
}
void USoftKeyBoard::UKeyLock::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	if (m_Active)
	{
		m_IsLock = !m_IsLock;
	}
	UControl::OnMouseDown(point,nRepCnt,uFlags);
}

//////////////////////////////////////////////////////////////////////////
UIMP_CLASS(USoftKeyBoard::UKeyCtrl, UControl);
void USoftKeyBoard::UKeyCtrl::StaticInit()
{
	UREG_PROPERTY("bgkSkin", UPT_STRING, UFIELD_OFFSET(USoftKeyBoard::UKeyCtrl, m_strBitmapFile));
}

USoftKeyBoard::UKeyCtrl::UKeyCtrl()
{
	m_Key = LK_NULL;
	m_MouseDown  = FALSE ;
	m_isFunKey = FALSE ;

	m_skinBGK = NULL;

	
}
USoftKeyBoard::UKeyCtrl::~UKeyCtrl()
{

}
BOOL USoftKeyBoard::UKeyCtrl::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE ;
	}


	if (m_skinBGK == NULL)
	{
		if (m_strBitmapFile.Empty())
		{
			m_skinBGK = sm_System->LoadSkin("Core\\SoftKeyBoard\\KeyBgk.skin");
		}else
		{

			m_skinBGK = sm_System->LoadSkin(m_strBitmapFile.GetBuffer());
		}
		if (!m_skinBGK)
		{
			return FALSE ;
		}
	}
	return TRUE ;
}
BOOL USoftKeyBoard::UKeyCtrl::SetKey(KeyCodes key, BOOL isFunKey , std::string c)
{
	m_Key = key;
	m_isFunKey = isFunKey;
	m_MouseDown = FALSE ;
	m_str = c ;

	return TRUE ;
}
BOOL USoftKeyBoard::UKeyCtrl::SetMouseUp()
{
	m_MouseDown = FALSE ;
	return TRUE ;
}

void USoftKeyBoard::UKeyCtrl::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_skinBGK)
	{
		sm_UiRender->DrawSkin(m_skinBGK, 0,updateRect);
	}

	if ((!m_MouseHover || m_isFunKey)&& m_Key)
	{
		if (m_MouseDown)
		{	
			if (m_MouseHover)
			{
				UDrawText(sm_UiRender,m_Style->m_spFont,offset,updateRect.GetSize(), m_Style->m_FontColorNA, m_str.c_str(), UT_CENTER);
			}else
			{
				UDrawText(sm_UiRender,m_Style->m_spFont,offset,updateRect.GetSize(), m_Style->m_FontColorHL, m_str.c_str(), UT_CENTER);
			}
			
		}else if (m_isFunKey)
		{
			if (m_MouseHover)
			{
				UDrawText(sm_UiRender,m_Style->m_spFont,offset,updateRect.GetSize(), m_Style->m_FontColorHL, m_str.c_str(), UT_CENTER);
			}else
			{
				UDrawText(sm_UiRender,m_Style->m_spFont,offset,updateRect.GetSize(), m_Style->m_FontColorNA, m_str.c_str(), UT_CENTER);
			}
			
		}else
		{
			UDrawText(sm_UiRender,m_Style->m_spFont,offset,updateRect.GetSize(), m_Style->m_FontColor, m_str.c_str(), UT_CENTER);
		}
	}

}
void USoftKeyBoard::UKeyCtrl::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	if (!m_isFunKey)
	{
		// send key msg 
	}else
	{
		// send flag msg 
		if (m_Key == LK_CAPSLOCK)
		{
			m_MouseDown = !m_MouseDown ;
		}
	}

	UControl* pkP = GetParent();
	USoftKeyBoard* pkSoftKey = UDynamicCast(USoftKeyBoard, pkP);
	if (pkSoftKey)
	{
		pkSoftKey->KeyMsg(m_Key);
	}
}
void USoftKeyBoard::UKeyCtrl::OnMouseUp(const UPoint& position, UINT flags)
{

	UControl::OnMouseUp(position,flags);

	//if (!m_isFunKey)
	//{
	//	// send key msg 
	//}else
	//{
	//	// send flag msg 
	//	m_MouseDown = !m_MouseDown ;
	//}

	//UControl* pkP = GetParent();
	//USoftKeyBoard* pkSoftKey = UDynamicCast(USoftKeyBoard, pkP);
	//if (pkSoftKey)
	//{
	//	pkSoftKey->KeyMsg(m_Key);
	//}
	
}

//////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////


UIMP_CLASS(USoftKeyBoard, UControl);
void USoftKeyBoard::StaticInit()
{

}

USoftKeyBoard::USoftKeyBoard()
{
	m_Lock = NULL;
	m_LastKey = LK_NULL;
	m_UseControl = NULL ;

	m_flag = FALSE ;
	InitKeyTable();

	for (int i = 0; i < 47; i++)
	{
		m_KeyCtrl[i] = NULL ;
	}
	m_FunKeyCtrl[0] = NULL;
	m_FunKeyCtrl[1] = NULL;
	m_FunKeyCtrl[2] = NULL;

	m_Btn[0] = NULL;
	m_Btn[1] = NULL;

	m_bgk = NULL;
}
USoftKeyBoard::~USoftKeyBoard()
{

}
void USoftKeyBoard::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	
}
void USoftKeyBoard::OnMouseUp(const UPoint& position, UINT flags)
{
	
}
void USoftKeyBoard::OnMouseDragged(const UPoint& position, UINT flags)
{

}
void USoftKeyBoard::OnRender(const UPoint& offset,const URect &updateRect)
{
	if (m_bgk)
	{
		sm_UiRender->DrawImage(m_bgk, updateRect);
	}
	RenderChildWindow(offset,updateRect);
}
void USoftKeyBoard::KeyMsg(KeyCodes key)
{
	if (m_UseControl)
	{
		switch(key)
		{
		case LK_RETURN:
		case LK_BACK:
			{
				m_UseControl->SoftKeyBoardKeyMsg(key, 1, 0);
				return ;
			}
		case LK_CAPSLOCK:
			{
				m_flag = !m_flag;
				if (!m_flag)
				{
					m_FunKeyCtrl[0]->SetMouseUp();
				}
				
				ResetKeyString();
				return ;
			}

		default:
			{

				UINT Code = GetKeyCodesChar(key, m_flag);

				m_UseControl->SoftKeyBoardChar(Code, 1, 0);
				
				if (m_Lock->GetLock())
				{
					SortKeyTable();
				}
			}
			
		}
	}else
	{
		Hiden();
	}
}
BOOL USoftKeyBoard::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE ;
	}
	if (!m_bgk)
	{
		m_bgk = sm_UiRender->LoadTexture("data\\ui\\core\\SoftKeyBoard\\KeyBoard.png");
		if (!m_bgk)
		{
			return FALSE ;
		}
	}
	int top = 14;
	int left = 15;
	for (int i = 0; i < 47; i ++)
	{
		UControl* pkCtrl = sm_System->CreateDialogFromFile("core\\SoftKeyBoard\\key.udg");
		m_KeyCtrl[i] = UDynamicCast(USoftKeyBoard::UKeyCtrl, pkCtrl);
		if (m_KeyCtrl[i])
		{
			AddChild(m_KeyCtrl[i]);
			m_KeyCtrl[i]->SetId(0xff + i );
			m_KeyCtrl[i]->SetVisible(TRUE);

			int w = m_KeyCtrl[i]->GetWidth();
			int h = m_KeyCtrl[i]->GetHeight();
			if (i < 10)
			{
				m_KeyCtrl[i]->SetPosition(UPoint(i * w + left, top));
			}else if ( i >= 10 && i < 21)
			{
				m_KeyCtrl[i]->SetPosition(UPoint((i - 10) * w + left, top + h));
			}else
			{
				m_KeyCtrl[i]->SetPosition(UPoint( (i - 21) % 11 * w + left, (i - 21) / 11  * h + 2 * h + top + 5));
			}
			
		}else
		{
			return FALSE ;
		}
	}


	if (m_FunKeyCtrl[0] == NULL)
	{
		m_FunKeyCtrl[0] = UDynamicCast(USoftKeyBoard::UKeyCtrl, GetChildByID(0));
	}
	if (m_FunKeyCtrl[1] == NULL)
	{
		m_FunKeyCtrl[1] = UDynamicCast(USoftKeyBoard::UKeyCtrl, GetChildByID(1));
	}
	if (m_FunKeyCtrl[2] == NULL)
	{
		m_FunKeyCtrl[2] = UDynamicCast(USoftKeyBoard::UKeyCtrl, GetChildByID(2));
	}

	if (!m_FunKeyCtrl[0] || !m_FunKeyCtrl[1] || !m_FunKeyCtrl[2])
	{
		return FALSE ;
	}

	// ���ܼ�����
	m_FunKeyCtrl[0]->SetKey(LK_CAPSLOCK, TRUE, GetKeyString(LK_CAPSLOCK));
	m_FunKeyCtrl[2]->SetKey(LK_RETURN, TRUE, GetKeyString(LK_RETURN));
	m_FunKeyCtrl[1]->SetKey(LK_BACK, TRUE, GetKeyString(LK_BACK));

	m_Lock = UDynamicCast(USoftKeyBoard::UKeyLock, GetChildByID(3));
	if (!m_Lock)
	{
		return FALSE ;
	}


	return TRUE ;
	
}
void USoftKeyBoard::Hiden()
{
	SetVisible(FALSE);
	m_UseControl = NULL;
	m_flag= FALSE; ;
	if (m_FunKeyCtrl[0])
	{
		m_FunKeyCtrl[0]->SetMouseUp();
	}
}
void USoftKeyBoard::SetUseControl(UEdit* use)
{
	if (use)
	{
		m_UseControl = use ;
	}else
	{
		Hiden();
	}

}
void USoftKeyBoard::Show(UEdit* use)
{
	if (use)
	{
		SetVisible(TRUE);
		m_flag = FALSE ;
		if (m_FunKeyCtrl[0])
		{
			m_FunKeyCtrl[0]->SetMouseUp();
		}
		m_UseControl = use ;

		UPoint NewPos = m_UseControl->WindowToScreen(UPoint(0, 0));
		NewPos.y += m_UseControl->GetHeight() + 4;

		int gx = g_Desktop->GetWidth() - (NewPos.x  + GetWidth());
		int gy = g_Desktop->GetHeight() - (NewPos.y + GetHeight());

		if (gx < 0 )
		{
			NewPos.y -= GetHeight() ;
		}

		if (gx < 0)
		{
			NewPos.x -= GetWidth() ; 
		}
		SetPosition(NewPos);

		
		SortKeyTable();	
	}
}
std::string USoftKeyBoard::GetKeyString(KeyCodes key)
{
	switch(key)
	{
	case LK_RETURN:
		return "ENTER";
	case LK_BACK:
		return "<-";
	case LK_CAPSLOCK:
		return "SHIFT";
	default:
		{
			char Tem0 = (char) GetKeyCodesChar(key, FALSE);
			char Tem1 = (char) GetKeyCodesChar(key, TRUE);
			static char buf[128];
			if (!m_flag)
			{
				buf[0] = Tem0;
				buf[1] = Tem1;
			}else
			{
				buf[0] = Tem1;
				buf[1] = Tem0;
			}
			
			buf[2] = 0;
			
			std::string str ;
			str = buf;
			buf[0] = '*';
			buf[1] = '*';
			//UString str;
			//str.Set(buf);
			return str;
		}
	}
}
UINT USoftKeyBoard::GetKeyCodesChar(KeyCodes key, BOOL Flag)
{
	for (int i = 0; i < 47; i++)
	{
		if (m_InputKey[i] == key)
		{
			return  GetChar(i, Flag);
		}
	}

	return 0 ;
}
UINT USoftKeyBoard::GetChar(UINT index, BOOL Flag)
{
	if (index >= 47)
	{
		return 0 ;
	}
	
	switch(m_InputKey[index])
	{
	case LK_GRAVE:
		{
			if (Flag)
			{
				return '~';
			}else
			{
				return '`';
			}
		}
	case LK_MINUS:
		{
			if (Flag)
			{
				return '_';
			}else
			{
				return '-';
			}
		}
	case LK_EQUALS:
		{
			if (Flag)
			{
				return '+';
			}else
			{
				return '=';
			}
		}
	case LK_LBRACKET:
		{
			if (Flag)
			{	
				return '{';
			}else
			{
				return '[';
			}
		}
	case LK_RBRACKET:
		{
			if (Flag)
			{
				return '}';
			}else
			{
				return ']';
			}
		}
	case LK_SEMICOLON:
		{
			if (Flag)
			{
				return ':';
			}else
			{
				return ';';
			}
		}
	case LK_APOSTROPHE:
		{
			if (Flag)
			{
				return '"';
			}else
			{
				return '\'';
			}
		}
	case LK_BACKSLASH:
		{
			if (Flag)
			{
				return '|';
			}else
			{
				return '\\';
			}
		}
	case LK_COMMA:
		{
			if (Flag)
			{
				return '<';
			}else
			{
				return ',';
			}
		}
	case LK_PERIOD:
		{
			if (Flag)
			{
				return '>';
			}else
			{
				return '.';
			}
		}
	case LK_SLASH:
		{
			if (Flag)
			{
				return '?';
			}else
			{
				return '/';
			}
		}
		
	}
	
	const char* str = 	LK2StringKey(m_InputKey[index]);
	if (Flag)
	{
		switch(m_InputKey[index])
		{
		case LK_0:
			{
				return ')';
			}
		case LK_1:
			{
				return '!';
			}
		case LK_2:
			{
				return '@';
			}
		case LK_3:
			{
				return '#';
			}
		case LK_4:
			{
				return '$';
			}
		case LK_5:
			{
				return '%';
			}
		case LK_6:
			{
				return '^';
			}
		case LK_7:
			{
				return '&';
			}
		case LK_8:
			{
				return '*';

			}
		case LK_9:
			{
				return '(';
			}
		default:
			return str[0];
			
		}
	}else
	{
		switch(m_InputKey[index])
		{
		case LK_0:
		case LK_1:
		case LK_2:
		case LK_3:
		case LK_4:
		case LK_5:
		case LK_6:
		case LK_7:
		case LK_8:
		case LK_9:
			{
				return str[0];
			}
		default:
			return str[0] + 32;

		}
	}
}
void USoftKeyBoard::InitKeyTable()
{
	m_InputKey[0] = LK_A;
	m_InputKey[1] = LK_B;
	m_InputKey[2] = LK_C;
	m_InputKey[3] = LK_D;
	m_InputKey[4] = LK_E;
	m_InputKey[5] = LK_F;
	m_InputKey[6] = LK_G;
	m_InputKey[7] = LK_H;
	m_InputKey[8] = LK_I;
	m_InputKey[9] = LK_J;
	m_InputKey[10] = LK_K;
	m_InputKey[11] = LK_L;
	m_InputKey[12] = LK_M;
	m_InputKey[13] = LK_N;
	m_InputKey[14] = LK_O;
	m_InputKey[15] = LK_P;
	m_InputKey[16] = LK_Q;
	m_InputKey[17] = LK_R;
	m_InputKey[18] = LK_S;
	m_InputKey[19] = LK_T;
	m_InputKey[20] = LK_U;
	m_InputKey[21] = LK_V;
	m_InputKey[22] = LK_W;
	m_InputKey[23] = LK_X;
	m_InputKey[24] = LK_Y;
	m_InputKey[25] = LK_Z;

	m_InputKey[26] = LK_0;
	m_InputKey[27] = LK_1;
	m_InputKey[28] = LK_2;
	m_InputKey[29] = LK_3;
	m_InputKey[30] = LK_4;
	m_InputKey[31] = LK_5;
	m_InputKey[32] = LK_6;
	m_InputKey[33] = LK_7;
	m_InputKey[34] = LK_8;
	m_InputKey[35] = LK_9;

	m_InputKey[36] = LK_GRAVE; // ~`
	m_InputKey[37] = LK_MINUS; // - _
	m_InputKey[38] = LK_EQUALS; // + =
	m_InputKey[39] = LK_LBRACKET; // {[
	m_InputKey[40] = LK_RBRACKET; // }]
	m_InputKey[41] = LK_SEMICOLON; // ; :
	m_InputKey[42] = LK_APOSTROPHE; // ' "
	m_InputKey[43] = LK_BACKSLASH; // \| 
	m_InputKey[44] = LK_COMMA;// <,
	m_InputKey[45] = LK_PERIOD;// >.
	m_InputKey[46] = LK_SLASH;// ?/

	m_FunctionKey[0]= LK_BACK;
	m_FunctionKey[1]= LK_RETURN ;
	m_FunctionKey[2]= LK_CAPSLOCK;
}
void USoftKeyBoard::ResetKeyString()
{
	for ( int i =0 ; i < 47; i++)
	{
		KeyCodes key = m_KeyCtrl[i]->GetKey();
		m_KeyCtrl[i]->SetKey(key, FALSE,GetKeyString(key));
	}
}
void USoftKeyBoard::SortKeyTable()
{
	std::vector<KeyCodes> vKey1 ;
	std::vector<KeyCodes> vKey2 ;
	vKey1.reserve(27);
	vKey2.reserve(22);

	for (int i = 0 ; i < 47; i++)
	{
		if (i < 26)
		{
			vKey1.push_back(m_InputKey[i]);
		}else
		{
			vKey2.push_back(m_InputKey[i]);
		}
	}

	/*if (m_Lock->GetLock())
	{
		*///srand(time(NULL));
	std::random_shuffle(vKey1.begin(), vKey1.end());
	std::random_shuffle(vKey2.begin(), vKey2.end());
	//}

	for ( int i =0 ; i < 47; i++)
	{
		if (i < 21)
		{
			if (i < vKey2.size())
			{
				m_KeyCtrl[i]->SetKey(vKey2[i], FALSE, GetKeyString(vKey2[i]));
			}
		}else
		{
			if ((i - 21) < vKey1.size())
			{
				m_KeyCtrl[i]->SetKey(vKey1[i - 21], FALSE, GetKeyString(vKey1[i- 21]));
			}
		}
	}
}