#ifndef __USOFTKEYBOARD_H__
#define __USOFTKEYBOARD_H__
#include "UInc.h"


class UIAPI USoftKeyBoard : public UControl
{
	UDEC_CLASS(USoftKeyBoard);
public:
	USoftKeyBoard();
	~USoftKeyBoard();

	//////////////////////////////////////////////////////////////////////////
	class UIAPI UKeyCtrl :public UControl
	{
		UDEC_CLASS(USoftKeyBoard::UKeyCtrl)
	public:
		UKeyCtrl();
		~UKeyCtrl();
		BOOL SetKey(KeyCodes key, BOOL isFunKey,  std::string c);
		KeyCodes GetKey() {return m_Key;}
		BOOL SetMouseUp();
		BOOL virtual OnCreate();
	protected:
		virtual void OnRender(const UPoint& offset,const URect &updateRect);
		virtual void OnMouseUp(const UPoint& position, UINT flags);
		virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	private:
		KeyCodes m_Key ;
		BOOL m_MouseDown ;
		BOOL m_isFunKey ; // 是否是功能键
		USkinPtr m_skinBGK;
		UString		m_strBitmapFile;
		std::string m_str;
	};

	//////////////////////////////////////////////////////////////////////////
	
	class UIAPI UKeyLock : public UControl
	{
		UDEC_CLASS(USoftKeyBoard::UKeyLock)
	public:
		UKeyLock();
		~UKeyLock();
		BOOL virtual OnCreate();

	public:
		inline BOOL GetLock(){return m_IsLock;}
	protected:
		virtual void OnRender(const UPoint& offset,const URect &updateRect);
		virtual void OnMouseUp(const UPoint& position, UINT flags);
		virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	private:
		BOOL m_IsLock ;
		USkinPtr m_LockSkin ;

	};
	//////////////////////////////////////////////////////////////////////////

public:
	virtual BOOL OnCreate();
	void KeyMsg(KeyCodes key);
public:
	void Hiden();
	void Show(UEdit* use);
	void SetUseControl(UEdit* use);
	void InitKeyTable();
	std::string GetKeyString(KeyCodes key);
	UINT GetKeyCodesChar(KeyCodes key, BOOL Flag);
	UINT GetChar(UINT index, BOOL Flag);
protected:
	void SortKeyTable();
	void ResetKeyString();
	virtual void OnRender(const UPoint& offset,const URect &updateRect);
	virtual void OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags);
	virtual void OnMouseUp(const UPoint& position, UINT flags);
	virtual void OnMouseDragged(const UPoint& position, UINT flags);

private:
	KeyCodes m_InputKey[47]; // 输入键
	KeyCodes m_FunctionKey[4]; //功能键 。。 删除 回车 大小写锁定 SHIFT
	UButton* m_Btn[2]; // 关闭 锁定
	KeyCodes m_LastKey;
	UEdit* m_UseControl;  // 使用软键盘的窗口
	USoftKeyBoard::UKeyCtrl* m_KeyCtrl[47];
	USoftKeyBoard::UKeyCtrl* m_FunKeyCtrl[3];
	USoftKeyBoard::UKeyLock* m_Lock ;
	BOOL m_flag;
	UTexturePtr m_bgk ;
};
#endif