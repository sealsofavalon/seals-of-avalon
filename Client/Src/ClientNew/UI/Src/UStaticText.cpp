#include "stdafx.h"
#include "UStaticText.h"


UIMP_CLASS(UStaticText,UControl);

void UStaticText::StaticInit()
{
	UREG_PROPERTY("text",UPT_STRINGW,UFIELD_OFFSET(UStaticText,m_WStrBuffer));
	UREG_PROPERTY("TextEdge", UPT_BOOL, UFIELD_OFFSET(UStaticText, m_bFontBoard));
}
UStaticText::UStaticText()
{
	m_Font = NULL;
	m_TextAlign = UT_LEFT;
	m_bFontBoard = false;
	m_FontEdgeColor = LCOLOR_BLACK;
	m_bShadow = false;
}
UStaticText::~UStaticText()
{

}
void UStaticText::SetText(const char * str)
{
	if(str && str[0] != 0)
	{
		m_WStrBuffer.Set( str );
	}
	else
	{
		m_WStrBuffer.SetEmpty();
	}
}
void UStaticText::SetText(const WCHAR * wstr)
{
	if(wstr && wstr[0] != 0)
	{
		m_WStrBuffer.Set( wstr );
	}
	else
	{
		m_WStrBuffer.SetEmpty();
	}
}
void UStaticText::Clear()
{
	m_WStrBuffer.Clear();
}
int UStaticText::GetText(WCHAR *dest,int nMaxLen,int npos /* = 0 */)
{
	if(npos >= m_WStrBuffer.GetLength() || nMaxLen == 0)
	{
		return 0;
	}
	assert(nMaxLen > 0);

	nMaxLen = __min(nMaxLen, m_WStrBuffer.GetLength() - npos );
	for(INT i=0; i<nMaxLen; i++)
	{
		dest[i] = m_WStrBuffer[npos + i];
	}
	return nMaxLen;
}
int UStaticText::GetText(char *dest,int nMaxLen,int npos /* = 0 */)
{
	return m_WStrBuffer.GetAnsi(dest,nMaxLen,npos);
}
INT getVal(char c)
{
	if(c >= '0' && c <= '9')
		return c - '0';
	else if(c >= 'A' && c <= 'Z')
		return c - 'A' + 10;
	else if(c >= 'a' && c <= 'z')
		return c - 'a' + 10;
	return -1;
}
void UStaticText::SetTextColor(const char *color)
{
	int num = 0;
	while (color[num++] != '\0');
	num--;
	if (num != 6 && num != 8)
		return;
	m_FontColor.r   = getVal(color[0]) * 16 + getVal(color[1]);
	m_FontColor.g = getVal(color[2]) * 16 + getVal(color[3]);
	m_FontColor.b  = getVal(color[4]) * 16 + getVal(color[5]);
	if (num == 8)
		m_FontColor.a = getVal(color[6]) * 16 + getVal(color[7]);
	else
		m_FontColor.a = 255;
}
void UStaticText::SetTextColor(UColor &color)
{
	m_FontColor = color;
}
void UStaticText::SetTextAlign(int align)
{
	m_TextAlign = align;
}
void UStaticText::SetTextEdge(bool bedge, UColor& color)
{
	m_bFontBoard = bedge;
	m_FontEdgeColor = color;
}
BOOL UStaticText::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}
	if (m_Font == NULL)
	{
		m_Font = m_Style->m_spFont;
	}
	m_FontColor = m_Style->m_FontColor;
	m_TextAlign = m_Style->mAlignment;
	if (m_Font == NULL)
	{
		return FALSE;
	}
	return TRUE;
}
void UStaticText::OnDestroy()
{
	m_bShadow = false;
	return UControl::OnDestroy();
}
void UStaticText::OnRender(const UPoint& offset,const URect &updateRect)
{
	UControl::OnRender(offset,updateRect);
	URect DrawRect(offset, m_Size);
	UColor color = m_Active? m_FontColor : m_Style->m_FontColorNA;
	if (m_Font && sm_UiRender && m_WStrBuffer.GetLength())
	{
		USetTextEdge(m_bFontBoard, m_FontEdgeColor);
		if (m_bShadow)
			UDrawText(sm_UiRender, m_Font, DrawRect.GetPosition() + UPoint(1, 1), DrawRect.GetSize(), LCOLOR_BLACK, m_WStrBuffer, (EUTextAlignment)m_TextAlign);

		UDrawText(sm_UiRender, m_Font, DrawRect.GetPosition(), DrawRect.GetSize(), color, m_WStrBuffer, (EUTextAlignment)m_TextAlign);
		USetTextEdge(false);
	}
}
void UStaticText::OnTimer(float fDeltaTime)
{
	UControl::OnTimer(fDeltaTime);
}