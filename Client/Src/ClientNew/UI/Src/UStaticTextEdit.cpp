#include "StdAfx.h"
#include "UStaticTextEdit.h"
#include "UInput.h"
#include "UIME.h"
#include "UDesktop.h"
#include "UScrollBar.h"
#include "USystem.h"


struct LineText           //需要渲染的单行字符串.
{
	INT StartPos;        //这一行字符串在整个字符串起始位置
	INT LineTextlen;     //这一行字符串的长度
	INT HeightToTop;     //这一行距离顶部的长度
	INT WidthToRight;    //这一行距离右边的长度
	LineText*  NextLine;  //下一行
	LineText()
	{
		NextLine = NULL;
	}
};
struct StaticText 
{
	StaticText()
	{
		CaretLeftStrWidth = CaretTopStrHeight = UndoCursorPos = LineTextHeight = LineDistance =0;
		LineList = LastLineList = curLineList = NULL;
		TextFont = NULL;
	}

	INT			UndoCursorPos;      //光标的位置
	INT         CaretLeftStrWidth;	//光标左边的字符宽度
	INT         CaretTopStrHeight;  //距离顶部的高度 
	INT         LineTextHeight;     //每行字的高度
	INT         LineTextWidth;      //每行字的最大宽度
	INT         LineDistance;		// 行与行的间隙
	INT         LineToLeftAndRight; //每行距离左边和右边的长度
	INT         TextMaxYHeight;     //字符串的总高度
	UFontPtr    TextFont;           //字体
	LineText*   LineList;           //整个链表
	LineText*   LastLineList;       //木尾部分
	LineText*   curLineList;    //当前光标操作的行.

	//创建链表
	void ArrcoLineText(LineText* nLine,  UStringW* nText, UINT pStart)
	{	
		UStringW Text; 
		Text.Set(nText);

		if (TextFont == NULL  || Text.GetLength() == 0)
		{
			nLine = NULL;
			return ;
		}else
		{
			nLine->StartPos = pStart;
			nLine->HeightToTop = TextMaxYHeight; //新的一行的高度等于原来的文本的高度.//
			TextMaxYHeight = TextMaxYHeight + LineTextHeight + LineDistance;  //文本的高度需要重新的从当前操作的行计算

			INT len = Text.GetLength();   // 获得字符串的长度

			// 如果整个文本的宽度小于或者等于最大文本的宽度. 则为最后一行. 可以返回链表了.
			if (TextFont->GetStrWidth(Text.GetBuffer()) <= LineTextWidth)
			{
				nLine->NextLine = NULL;
				nLine->LineTextlen = len;
				nLine->WidthToRight = LineTextWidth - TextFont->GetStrWidth(Text.GetBuffer());
				LastLineList = nLine;	
				return ;
			}else
			{
				for (size_t i = 0; i < len - 1; i++)
				{	

					WCHAR str_i[100];
					WCHAR Addstri[100] ;
					Text.SubStr(str_i,0,i+1);
					// 如果是最后一个字符, 并且宽度刚好等于或者小于
					INT iWidth = TextFont->GetStrWidth(str_i);
					if (iWidth <= LineTextWidth && (i == len -2 ||i == len -1))
					{
						nLine->LineTextlen = i+1;
						nLine->WidthToRight = 0;
						nLine->NextLine = NULL;
						return ;
					}
					Text.SubStr(Addstri,0,i+2);


					INT pWidth = TextFont->GetStrWidth(Addstri);

					if (iWidth <= LineTextWidth  && pWidth >= LineTextWidth)
					{	
						nLine->LineTextlen = i+1;
						nLine->WidthToRight = 0;
						nLine->NextLine = NULL;
						return ;
					}
				}	
			}	
		}
	}

	~StaticText()
	{
		while(LineList)
		{
			LineText* pkNext = LineList->NextLine;
			delete LineList;LineList = NULL;
			LineList = pkNext;
		}

		LineList = NULL;
		LastLineList = NULL;
		curLineList = NULL;
	}
	//根据需要初始化链表
	void ArrcoLine(UStringW* nUndo, UINT pos, LineText* pLine)
	{

		UINT nLen = nUndo->GetLength();

		WCHAR pNext[1000] ;
		nUndo->SubStr(pNext,pos, nLen - pos);

		UStringW strNext ;
		strNext.Set(pNext);
		LineText* temp ; temp = pLine;

		ArrcoLineText(temp,&strNext,pos);
		UINT pStart = temp->StartPos + temp->LineTextlen;
		if (pStart < nUndo->GetLength())
		{
			if (temp->NextLine == NULL)
			{
				temp->NextLine = new LineText;
			}

			ArrcoLine(nUndo,pStart,temp->NextLine);
		}else
		{
			pLine->NextLine = NULL;
		}
	}

	//第一次设置
	void InitUndo(UStringW* nUndo)
	{
		UndoCursorPos = nUndo->GetLength();
		TextMaxYHeight = LineDistance;

		CaretLeftStrWidth = LineToLeftAndRight ;
		CaretTopStrHeight = TextMaxYHeight;

		while(LineList)
		{
			LineText* pkNext = LineList->NextLine;
			delete LineList;LineList = NULL;
			LineList = pkNext;
		}

		if (LineList == NULL)
		{
			LineList = new LineText;
		}	
		ArrcoLine(nUndo,0,LineList);
		curLineList = LastLineList;
	}

	// 设置尾行的属性
	void SetLastLine()
	{
		if (LineList == NULL)
		{
			LastLineList = NULL;
			return ;
		}else
		{
			LineText* pLineText ;
			pLineText = LineList;
			while(pLineText)
			{
				if (pLineText->NextLine == NULL)
				{
					LastLineList = pLineText;
					return ;	
				}
				pLineText = pLineText->NextLine ;
			}
		}
	}

	//执行删除操作
	void DelUndo(UStringW* nUndo)
	{
		if (LineList == NULL)
		{
			return;
		}

		size_t strLen = nUndo->GetLength();
		size_t pos = LastLineList->StartPos;
		size_t len = LastLineList->LineTextlen;

		if (strLen == 0)
		{
			while(LineList)
			{
				LineText* pkNext = LineList->NextLine;
				delete LineList;LineList = NULL;
				LineList = pkNext;
			}
			UndoCursorPos = 0;
			curLineList = NULL;
			LastLineList = NULL;
			return ;
		}

		if (strLen <= pos)
		{
			while(LineList)
			{
				LineText* pkNext = LineList->NextLine;
				delete LineList; LineList = NULL;
				LineList = pkNext;
			}
			curLineList = NULL;
			LastLineList = NULL;
		}
		OnUpdateUndo(nUndo);
	}

	//根据光标位置设置当前操作的行
	void SetCurLine()
	{
		if (LineList == NULL)
		{
			curLineList = NULL;
			return ;
		}

		LineText* pLine = LineList ;
		while(pLine)
		{
			if (UndoCursorPos >= pLine->StartPos && UndoCursorPos -  pLine->StartPos <= pLine->LineTextlen)
			{
				curLineList = pLine;
				return ;
			}
			pLine = pLine->NextLine;
		}
	}

	//根据光标位置设置光标的属性
	void SetCursor(UStringW* nUndo)
	{
		if (curLineList == NULL)
		{
			CaretLeftStrWidth = 0;
			CaretTopStrHeight = LineDistance;
			return ;
		}
		UINT pStart = curLineList->StartPos ;
		if (UndoCursorPos - pStart >=0 && UndoCursorPos- pStart <= curLineList->LineTextlen)
		{
			if (pStart == 0 && UndoCursorPos == 0)
			{
				CaretLeftStrWidth = 0;
				CaretTopStrHeight = LineDistance;
			}else if (UndoCursorPos == LastLineList->LineTextlen)
			{
				CaretLeftStrWidth = LineTextWidth - LastLineList->WidthToRight ;
				CaretTopStrHeight = LastLineList->HeightToTop ;
			}else
			{
				WCHAR pDest[255];
				nUndo->SubStr(pDest,pStart,UndoCursorPos - pStart);

				//
				CaretLeftStrWidth = TextFont->GetStrWidth(pDest);
				CaretTopStrHeight = curLineList->HeightToTop;
			}
		}
	}

	//更新
	void OnUpdateUndo(UStringW* nUndo)
	{
		if (nUndo->GetLength() == 0)
		{
			while(LineList)
			{
				LineText* pkNext = LineList->NextLine;
				delete LineList;LineList = NULL;
				LineList = pkNext;
			}
			LastLineList = NULL;
			curLineList = NULL;

			return ;
		}
		if (LineList == NULL)
		{
			InitUndo(nUndo);
		}else
		{
			LineText* nLine = curLineList;
			size_t pStart = curLineList->StartPos;
			size_t pLen =curLineList->LineTextlen;
			TextMaxYHeight = curLineList->HeightToTop;	

			ArrcoLine(nUndo,pStart,nLine);
		}
	}
};

UIMP_CLASS(UStaticTextEdit,UControl)

void UStaticTextEdit::StaticInit()
{

}

UStaticTextEdit::UStaticTextEdit()
{
	m_Active = TRUE;
	m_pTextContent = NULL;
	m_nMaxLength = 10000;
	m_nCaretPos = 0;
}
UStaticTextEdit::~UStaticTextEdit()
{

}

BOOL UStaticTextEdit::OnCreate()
{
	if (!UControl::OnCreate())
	{
		return FALSE;
	}

	if (m_Style->m_spFont == NULL)
	{
		return FALSE;
	}
	m_pTextContent = new StaticText;
	m_pTextContent->TextFont = m_Style->m_spFont;
	m_pTextContent->LineToLeftAndRight = 18;
	m_pTextContent->LineTextWidth = m_Size.x - 2* m_pTextContent->LineToLeftAndRight;
	m_pTextContent->LineTextHeight = m_pTextContent->TextFont->GetHeight();
	m_pTextContent->LineDistance = 2; //暂时设置字符间距为2;

	return TRUE;
}

void UStaticTextEdit::OnDestroy()
{
	if (m_pTextContent)
	{
		delete m_pTextContent;

		m_pTextContent = NULL;
	}
	UControl::OnDestroy();
}

void UStaticTextEdit::OnSize(const UPoint& NewSize)
{
	m_pTextContent->LineTextWidth = NewSize.x - 2 * m_pTextContent->LineToLeftAndRight;
}

void UStaticTextEdit::OnGetFocus()
{
	UControl::OnGetFocus();

	sm_System->SetIMEEnable(TRUE);
}
void UStaticTextEdit::OnLostFocus()
{
	sm_System->SetIMEEnable(FALSE);
	//UIME::DisableIME();
}

int UStaticTextEdit::GetText(char* dest, int nMaxLen, int nPos)
{
	return m_wstrBuffer.GetAnsi(dest, nMaxLen, nPos);
}
int UStaticTextEdit::GetText(WCHAR* dest, int nMaxLen, int nPos)
{
	if(nPos >= m_wstrBuffer.GetLength() || nMaxLen == 0)
	{
		return 0;
	}
	assert(nMaxLen > 0);

	nMaxLen = __min(nMaxLen, m_wstrBuffer.GetLength() - nPos );
	for(INT i=0; i<nMaxLen; i++)
	{
		dest[i] = m_wstrBuffer[nPos + i];
	}
	return nMaxLen;
}
void UStaticTextEdit::SetText(const char* txt)
{
	if (!m_pTextContent)
		return;
	if(txt && txt[0] != 0)
	{
		m_wstrBuffer.Set( txt );
	}
	else
	{
		m_wstrBuffer.SetEmpty();
	}

	m_nCaretPos = m_wstrBuffer.GetLength();
	m_pTextContent->OnUpdateUndo(&m_wstrBuffer);
	OnSetCaretPos();
}
 void UStaticTextEdit::SetText(const WCHAR* txt)
 {
	 if (!m_pTextContent)
		 return;
	 if(txt && txt[0] != 0)
	 {
		 m_wstrBuffer.Set(txt);
	 }
	 else
	 {
		 m_wstrBuffer.SetEmpty();
	 }

	 m_nCaretPos = m_wstrBuffer.GetLength();
	 m_pTextContent->OnUpdateUndo(&m_wstrBuffer);
	 OnSetCaretPos();

 }


void UStaticTextEdit::OnRender(const UPoint& offset, const URect &updateRect)
{
    g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
    g_pRenderInterface->SetScissorRect(sm_UiRender->GetClipRect());

	if ( m_Style->mOpaque )
	{
		if(IsFocused())
		{
			sm_UiRender->DrawRectFill(updateRect, m_Style->mFillColorHL);
		}
		else
		{
			sm_UiRender->DrawRectFill(updateRect, m_Style->mFillColor);
		}
	}

	DrawContent(offset);

    g_pRenderInterface->SetRenderState(D3DRS_SCISSORTESTENABLE,FALSE);
}

void UStaticTextEdit::DrawContent(UPoint offset)
{
	UPoint curOffset;
	curOffset.Set(offset.x,offset.y);
	if (m_pTextContent->LineList)
	{
		for (LineText* linelist = m_pTextContent->LineList; linelist; linelist = linelist->NextLine)
		{
			UPoint lineOffset =  UPoint(offset.x + m_pTextContent->LineToLeftAndRight, offset.y + linelist->HeightToTop);
			INT len = linelist->LineTextlen;
			INT pStart = linelist->StartPos;

			WCHAR pDest[255] ; 
			m_wstrBuffer.SubStr(pDest,pStart,len);
			sm_UiRender->DrawTextN(m_pTextContent->TextFont,lineOffset,pDest,len,NULL,m_Style->m_FontColor);
		}
	}

	// 光标
	if (IsFocused())
	{
		UPoint cursorPos ;
		UPoint cursorPosT ;
		cursorPos =  UPoint(curOffset.x + m_pTextContent->LineToLeftAndRight + m_pTextContent->CaretLeftStrWidth, curOffset.y + m_pTextContent->CaretTopStrHeight);
		cursorPosT = UPoint(cursorPos.x, cursorPos.y + m_pTextContent->LineTextHeight);
		//sm_UiRender->DrawLine(cursorPos,cursorPosT,m_Style->m_FontColor);
		sm_UiRender->DrawRectFill(URect(cursorPos, UPoint(2, m_pTextContent->LineTextHeight)), m_Style->mCursorColor);
	}	
}
void UStaticTextEdit::OnMouseDown(const UPoint& point, UINT nRepCnt, UINT uFlags)
{
	CaptureControl();

	SetFocusControl();

}
void UStaticTextEdit::OnMouseUp(const UPoint& position, UINT flags)
{
	ReleaseCapture();
}
BOOL UStaticTextEdit::OnKeyUp(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{
	return TRUE;
}

void UStaticTextEdit::OnSetCaretPos()
{
	m_pTextContent->UndoCursorPos = m_nCaretPos;
	m_pTextContent->SetCurLine();
	m_pTextContent->SetCursor(&m_wstrBuffer);

	if (GetParent())
	{
		UScrollBar* pSB = UDynamicCast(UScrollBar,GetParent());
		if (pSB)
		{
			//SetHeight(m_pTextContent->TextMaxYHeight);
			//pSB->ReCalLayout();
			if (m_pTextContent->curLineList && m_pTextContent->curLineList->HeightToTop /*> m_Size.y*/)
			{
				pSB->scrollTo(0,m_pTextContent->curLineList->HeightToTop);
			}else
			{
				pSB->scrollTo(0,0);
			}

		}
	}
}
BOOL UStaticTextEdit::OnKeyDown(UINT nKeyCode, UINT nRepCnt, UINT nFlags)
{

	if(!m_Active )
		return FALSE;

	assert(m_pTextContent);
	UINT KeyCode = nKeyCode;
	UINT Flags = nFlags;

	INT stringLen = m_wstrBuffer.GetLength();
	// 优先处理退格操作.
	if(KeyCode == LK_BACKSPACE)
	{
		if (stringLen > 0 && m_nCaretPos > 0)
		{
			m_wstrBuffer.Cut(m_nCaretPos-1,1);
			m_pTextContent->DelUndo(&m_wstrBuffer);
			m_nCaretPos--;
			OnSetCaretPos();

			

		}
		return TRUE;
	}else  if (KeyCode == LK_LEFT)
	{
		if (m_pTextContent->LineList)
		{
			if (m_nCaretPos > 0)
			{
				m_nCaretPos-- ;
			}
			OnSetCaretPos();
		}
		return TRUE;
	}else  if (KeyCode == LK_RIGHT)
	{
		if (m_pTextContent->LineList)
		{
			int len = m_wstrBuffer.GetLength();
			if (m_nCaretPos < len)
			{
				m_nCaretPos++ ;
			}
			OnSetCaretPos();
		}
		return TRUE;
	}else if (KeyCode == LK_RETURN || KeyCode == LK_NUMPADENTER)
	{
		return TRUE;
	}
	return TRUE;
}
BOOL UStaticTextEdit::OnChar(UINT nCharCode, UINT nRepCnt, UINT nFlags)
 {
	 assert(m_pTextContent);
	 if(!m_Active )
		 return FALSE;

	 // 处理字符输入
	 if ( m_pTextContent->TextFont->IsValidChar((WCHAR)nCharCode) )
	 {
		 //save the current state
		
		 INT TextLen = m_wstrBuffer.GetLength();

		 if ( (TextLen < m_nMaxLength))
		 { 	
			 
			m_wstrBuffer.Insert(m_nCaretPos,nCharCode);
			m_pTextContent->OnUpdateUndo(&m_wstrBuffer);
			m_nCaretPos++ ;
			OnSetCaretPos();
				 

			if (m_pTextContent->TextMaxYHeight >= m_Size.y - 10)
			{
				if (GetParent())
				{
					UScrollBar* pSB = UDynamicCast(UScrollBar,GetParent());
					if (pSB)
					{
						SetHeight(m_pTextContent->TextMaxYHeight);
						pSB->ReCalLayout();
						pSB->scrollTo(0,m_pTextContent->TextMaxYHeight);
					}
				}
			}
		 }
		 return TRUE;
	 }
	 return TRUE;
 }