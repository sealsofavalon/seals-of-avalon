#include "StdAfx.h"
#include "USystem.h"
#include "USystemImp.h"
#include "UMemory.h"
#include "UIME.h"

static BOOL g_bSystemInited = FALSE;
FrameAllocator* g_pFrameAllocator = NULL;
USystem* g_USystem = NULL;
USystem* USystem::Create()
{
	InitKeyMapTable();
	if (g_USystem != NULL)
	{
		return g_USystem;
	}
#ifdef UI_MEMDBG
	g_MemMgr.Initialize();
#endif 
	USystem* pSystem = new USystem;
	g_pFrameAllocator = new FrameAllocator;
	assert(g_pFrameAllocator);
	g_pFrameAllocator->Resize(1024);
	UControl::sm_System = pSystem;
	g_USystem = pSystem;
	return pSystem;

}

void USystem::SetSoundInterface(IUSoundInterface* iSI)
{
	if (m_pSystem)
	{
		m_pSystem->SetSoundInterface(iSI);
	}
}

void USystem::__PlaySound(const char* pszSnd)
{
	if (m_pSystem)
	{
		m_pSystem->PlaySound(pszSnd);
	}
}

void USystem::__PlaySound(int Index)
{
	if (m_pSystem)
	{
		m_pSystem->PlaySound(Index);
	}
}

void USystem::SetMouseControl(UControl* pWnd)
{
	if(m_pSystem)
	{
		m_pSystem->SetMouseControl(pWnd);
	}
}

USystem::USystem(void)
{
	m_pSystem = new USystemImp;
	assert(m_pSystem);
}

USystem::~USystem(void)
{
	if (m_pSystem)
	{
		delete m_pSystem;
	}
}

URender* USystem::GetPlugin()
{
	return UControl::sm_UiRender;
}

BOOL USystem::Initialize(HWND hWnd)
{
	if (g_bSystemInited)
	{
		return TRUE;
	}
	if (m_pSystem->Initialize(hWnd))
	{
		g_bSystemInited = TRUE;
		return TRUE;
	}else
	{
		return FALSE;
	}
}

void USystem::Shutdown()
{
	if (!g_bSystemInited)
	{
		return;
	}
	EnableIME(FALSE);
	m_pSystem->Destroy();
	if (UControl::sm_UiRender)
	{
		UControl::sm_UiRender->Shutdown();
		UControl::sm_UiRender = NULL;
	}
	UControl::sm_System = NULL;
	g_bSystemInited = FALSE;
}

void USystem::SetDragCursorSize(const UPoint& CursorSize)
{
	if (g_Desktop)
	{
		g_Desktop->SetDragCursorSize(CursorSize);
	}
}

BOOL USystem::EnableIME(BOOL Enable)
{
	BOOL bRet = FALSE;
	if (Enable)
	{
		bRet = UIME::Initialize();
		if (!bRet)
		{
			UTRACE("IME Initialize failed.");
			UIME::Shutdown();
		}
	}else
	{
		UIME::Shutdown();
		bRet = TRUE;
	}
	m_IsIMEEable = FALSE;
	return bRet;
}
void USystem::SetIMEEnable(BOOL Enable)
{
	m_IsIMEEable = Enable ;
	m_pSystem->WindowKillFouce();
	if (Enable)
	{
		UIME::EnableIME();
	}
	else
	{
		UIME::DisableIME();
	}
}
BOOL USystem::IsIMEEable()
{
	return m_IsIMEEable ;
}
void USystem::IniIMEWnd(HWND hWnd)
{
	UIME::SetIMEWnd(hWnd);
}
void USystem::Release()
{
	Shutdown();
	if (g_pFrameAllocator)
	{
		delete g_pFrameAllocator;
	}
	UControl::sm_System = NULL;
	delete this;
#ifdef UI_MEMDBG 
	g_MemMgr.Shutdown();
#endif 
}

void USystem::Update(float fDeltaTime)
{
	if (m_pSystem)
	{
		m_pSystem->Update(fDeltaTime);
	}
}

void USystem::Draw()
{
	if (m_pSystem)
	{
		m_pSystem->Draw();
	}
}

void USystem::Resize(int x, int y)
{
	if (m_pSystem)
	{
		m_pSystem->Resize(x,y);
	}
}

void USystem::ReszieWindow(int x, int y)
{
	if (m_pSystem)
	{
		m_pSystem->WindowResize(x, y);
	}
}
	const char* USystem::GetRootPath() const
	{
		if (m_pSystem)
		{
			return m_pSystem->m_szRootPath;
		}
		return NULL;
	}
	

BOOL USystem::LoadControlStyle(const char* StyleFileName)
{
	if (StyleFileName == NULL)
	{
		return FALSE;
	}
	if (m_pSystem)
	{
		return m_pSystem->LoadControlStyle(StyleFileName);
	}
	return FALSE;
}

UControlStyle* USystem::FindControlStyle(const char* StyleName)
{
	assert(StyleName);
	if (m_pSystem)
	{
		return m_pSystem->FindStyle(StyleName);
	}
	return NULL;
}
BOOL USystem::LoadAcceleratorKey(const char * keyFileName, bool bCover)
{
	if (keyFileName == NULL)
	{
		return FALSE;
	}
	if (m_pSystem)
	{
		return m_pSystem->LoadAccelarKeyMap(keyFileName, bCover);
	}
	return FALSE;
}

BOOL USystem::SaveAcceleratorKey(const char * keyFileName)
{
	BOOL BFine = FALSE;
	if (keyFileName == NULL)
	{
		return FALSE;
	}

	if (m_pSystem)
	{
		BFine = m_pSystem->SaveAccelarKeyMap(keyFileName);
		if (g_Desktop)
		{
			g_Desktop->ReRigestAcceleratorKey();
		}
	}
	return BFine;
}
void USystem::UnLoadAccelertorKey()
{
	if (m_pSystem)
	{
		return m_pSystem->UnRegisterAllKeys();
	}
}
UAccelarKey * USystem::FindAcceleratorKey(const char * AcceleratorKeyName)
{
	assert(AcceleratorKeyName);
	if (m_pSystem)
	{
		return m_pSystem->FindKey(AcceleratorKeyName);
	}
	return NULL;
}
BOOL USystem::SetAcceleratorKey(const char* AcceleratorKeyName, UINT nKeycode, UINT nFlag)
{
	assert(AcceleratorKeyName);
	if (m_pSystem)
	{
		return m_pSystem->SetKey(AcceleratorKeyName, nKeycode, nFlag);
	}
	return FALSE;
}
BOOL USystem::LoadColorTable(const char* ColorFileName)
{
	if (ColorFileName == NULL)
	{
		return FALSE;
	}
	if (m_pSystem)
	{
		return m_pSystem->LoadColorTable(ColorFileName);
	}
	return FALSE;
}
UColorTable* USystem::FindColor(const char * ColorName)
{
	assert(ColorName);
	if (m_pSystem)
	{
		return m_pSystem->FindColor(ColorName);
	}
	return NULL;
}
USkinPtr USystem::LoadSkin(const char* filename)
{
	assert(filename);
	if (filename == NULL || filename[0] == 0)
	{
		return NULL;
	}
	if (m_pSystem)
	{
		return m_pSystem->LoadSkin(filename);
	}
	return NULL;
}

void USystem::DestorySkin(USkin* pSkin)
{
	assert(pSkin);
	if (m_pSystem)
	{
		m_pSystem->DestroySkin(pSkin);
	}

}
UControl* USystem::CreateDialogFromFile(const char* TemplateName)
{
	assert(TemplateName && TemplateName[0] );
	char Buf[MAX_PATH];
	LSprintf(Buf, MAX_PATH, "%s%s", m_pSystem->m_szRootPath, TemplateName);

    CFileStream kFile;
    if( !kFile.Open(Buf) )
    {
		UTRACE("can't open dialog template file %s.", TemplateName);
		return NULL;
    }

    char* acBuffer = (char*)malloc( 256*1024 );
    unsigned int uiSize = kFile.Read(acBuffer, 256*1024);
    assert(uiSize < 256*1024 - 1);
    acBuffer[uiSize] = '\0';

	UControl* p = CreateDialogFromMemory(acBuffer, uiSize);
	free( acBuffer );
	return p;
}

UControl* USystem::CreateDialogFromMemory(const char* pTemplate, int nSize)
{
	assert(m_pSystem && pTemplate && nSize > 0);
	UControl* pControl = m_pSystem->LoadDialog(pTemplate, nSize);
	return pControl;
}

void USystem::DestoryControl(UControl* pControl)
{
	if (pControl && m_pSystem)
	{
		m_pSystem->DestoryControl(pControl);
	}
}

const UPoint& USystem::GetCursorPos() const
{
	assert(m_pSystem);
	return m_pSystem->GetCursorPos();
}

UControl* USystem::GetCapturedCtrl()
{
	assert(m_pSystem);
	return m_pSystem->GetCapturedCtrl();
}
void USystem::CaptureControl(UControl* pTargetCtrl)
{
	assert(m_pSystem);
	m_pSystem->CaptureControl(pTargetCtrl);
}
void USystem::ReleaseCapture(UControl* pCapturedCtrl)
{
	assert(m_pSystem);
	m_pSystem->ReleaseCapture(pCapturedCtrl);
}
UControl* USystem::GetFocus()
{
	assert(m_pSystem);
	return m_pSystem->GetFocus();
}

BOOL USystem::SetFrameControl(UControl* pFrameCtrl)
{
	assert(m_pSystem);
	return m_pSystem->SetFrameControl(pFrameCtrl);
}

UControl* USystem::GetCurFrame()
{
	assert(m_pSystem);
	return m_pSystem->GetCurFrame();
}

void USystem::PushDialog(UControl* pDialog)
{
	if (m_pSystem)
	{
		m_pSystem->PushDialog(pDialog);
	}
}

void USystem::PopDialog(UControl* pDialog)
{

	if (m_pSystem)
	{
		m_pSystem->PopDialog(pDialog);
	}
}

BOOL USystem::MouseMove(int x, int y)
{
	if (m_pSystem)
	{
		return	m_pSystem->MouseMove(x,y);
	}
	return FALSE;
}

BOOL USystem::MouseBtnDown(UINT Btn)
{
	if (m_pSystem)
	{
		return	m_pSystem->MouseBtnDown(Btn);
	}
	return FALSE;
}

BOOL USystem::MouseBtnUp(UINT Btn)
{
	if (m_pSystem)
	{
		return	m_pSystem->MouseBtnUp(Btn);
	}
	return FALSE;
}

BOOL USystem::MouseWheel(int Delta)
{
	if (m_pSystem)
	{
		return	m_pSystem->MouseWheel(Delta);
	}
	return FALSE;
}

BOOL USystem::KeyDown(UINT Key)
{
	if (m_pSystem )
	{
		return	m_pSystem->KeyDown(Key);
	}
	return FALSE;
}

BOOL USystem::KeyUp(UINT Key)
{
	if (m_pSystem )
	{
		return	m_pSystem->KeyUp(Key);
	}
	return FALSE;
}
BOOL USystem::KeyPress(UINT Key)
{
	if (m_pSystem )
	{
		return m_pSystem->KeyPress(Key);
	}

	return FALSE ;
}
inline BOOL USystem::IsDraging() const
{
	assert(m_pSystem);
	return m_pSystem->IsDraging();
}

void USystem::KeyMessageW(UINT nMsg, UINT wParam, UINT lParam)
{
	if (m_pSystem)
	{
		m_pSystem->KeyMessageW(nMsg, wParam, lParam);
	}
}
BOOL USystem::InitSoftKeyBoardControl()
{
	if (m_pSystem)
	{
		return m_pSystem->InitSoftKeyBoard();
	}
	return FALSE;
}
void USystem::KeyMessageA(UINT nMsg, UINT wParam, UINT lParam)
{
	if (m_pSystem)
	{
		m_pSystem->KeyMessageA(nMsg, wParam, lParam);
	}
}

BOOL USystem::ProcessIMEMessage(UINT nMsg, UINT wParam, UINT lParam)
{
	if (m_pSystem == NULL || !UIME::sm_bInited)
	{
		return FALSE;
	}
	return UIME::ProcessIMEMessage(nMsg, wParam, lParam);
}
BOOL USystem::WindowKillFouce()
{
	if (m_pSystem == NULL)
	{
		return FALSE;
	}
	return m_pSystem->WindowKillFouce();
}