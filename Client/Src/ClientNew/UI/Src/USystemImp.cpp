#include "StdAfx.h"
#include "USystemImp.h"
#include "USystem.h"

HWND g_hWnd = NULL;
HWND GetMainWnd()
{
	return g_hWnd;
}

USkinImp::USkinImp()
{
}
USkinImp::~USkinImp()
{
	m_spTexSurface = NULL;
	m_Items.clear();
}

BOOL USkinImp::Create()
{
	if (m_strTextureFile.empty())
	{
		return FALSE;
	}
	if (UControl::sm_UiRender == NULL)
	{
		return FALSE;
	}
	m_spTexSurface = UControl::sm_UiRender->LoadTexture(m_strTextureFile.c_str());
	if (m_spTexSurface == NULL)
	{
		return FALSE;
	}
	return TRUE;
}

const USKINITEM* USkinImp::GetSkinItem(int index) const
{
	assert(index >= 0 && index < m_Items.size());
	if (index >= 0 && index < m_Items.size())
	{
		return &m_Items[index];
	}
	return NULL;
}
int USkinImp::GetItemNum() const
{
	return (int)m_Items.size();
}
const char* USkinImp::GetSkinName() const
{
	return m_strName.c_str();
}
IUTexture* USkinImp::GetTexture() const
{
	return m_spTexSurface;
}

void USkinImp::Release()
{
	if (--m_RefCounter == 0)
	{
		// also remove this from system.
		g_USystem->DestorySkin(this);
	}
}

USystemImp::USystemImp(void):m_DesktopWindow(NULL)
{
	strcpy(m_szRootPath, "Data\\UI\\");
	m_bEnableIME = TRUE;
	m_hMainWnd = NULL;
	m_pSI = NULL;
}

USystemImp::~USystemImp(void)
{
}


BOOL USystemImp::Initialize(HWND hWnd)
{
	UTRACE("Initialize...");
	LinkUiClasses();

	m_hMainWnd = hWnd;
	if (m_hMainWnd == NULL)
	{
		return FALSE;
	}
	g_hWnd = m_hMainWnd;

	LoadFontResource();

	if (m_DesktopWindow == NULL)
	{
		m_DesktopWindow = new UDesktop;
	}
	return m_DesktopWindow != NULL;
}

void USystemImp::Destroy()
{
	if (m_DesktopWindow)
	{
		delete m_DesktopWindow; m_DesktopWindow = NULL;
	}
	UnLinkClasses();
	UnRegisterAllStyles();
	UnRegisterAllKeys();
	UnRegisterAllColor();
	RemoveAllFont();
	UTRACE("Destory.");
}

extern UClass* g_ClassHead;
void USystemImp::LinkUiClasses()
{
	UClass* pClass = g_ClassHead;
	while (pClass)
	{
		pClass->StaticInit();
		std::string className = pClass->GetClassName();
		m_ClassMap.insert(UiClassMap::value_type(className, pClass));
		pClass = pClass->GetNextClass();
	}

}

void USystemImp::UnLinkClasses()
{
	UClass* pClass = g_ClassHead;
	while (pClass)
	{
		pClass->UnRegister();
		pClass = pClass->GetNextClass();
	}
}


void USystemImp::UnRegisterAllStyles()
{
	UiStyleMap::const_iterator it = m_StyleMap.begin();
	while( it != m_StyleMap.end())
	{
		UControlStyle* pStyle = it->second;
		if (pStyle->GetRefCnt() != 1)
		{
			// warn
		}
		pStyle->Release();

		delete pStyle;
		++it;
	}
	m_StyleMap.clear();
}
void USystemImp::UnRegisterAllKeys()
{
	UAccelarkeyMap::const_iterator it = m_AccelarKeyMap.begin();
	while( it != m_AccelarKeyMap.end())
	{
		UAccelarKey* pAccelarKey = it->second;
		//if (pAccelarKey->GetRefCnt() != 1)
		//{
		//	// warn
		//}
		//pAccelarKey->Release();

		delete pAccelarKey;
		++it;
	}
	m_AccelarKeyMap.clear();
}
void USystemImp::UnRegisterAllColor()
{
	ColorTable::const_iterator it = m_ColorTable.begin();
	while( it != m_ColorTable.end())
	{
		UColorTable * pColor = it->second;
		//if (pAccelarKey->GetRefCnt() != 1)
		//{
		//	// warn
		//}
		//pAccelarKey->Release();

		delete pColor;
		it++;
	}
	m_ColorTable.clear();
}
void USystemImp::DestoryControl(UControl* pControl)
{
	if (m_DesktopWindow)
	{
		m_DesktopWindow->DestroyControl(pControl);
	}
	pControl->DeleteThis();
}

void USystemImp::DestroySkin(USkin* pSkin)
{
	USkinImp* pSkinImp = (USkinImp*)pSkin;
	assert(pSkinImp);
	const std::string& strSkinName = pSkinImp->m_strName;
	USkinMap::iterator it = m_SkinMap.find(strSkinName);
	if (it != m_SkinMap.end())
	{
		m_SkinMap.erase(it);
	}
	
	delete pSkinImp;
}

/*
1. 是否总是根据 CP_ACP 来作为CODEPAGE? 还是根据键盘布局来判断转换编码?
2. 
*/

void USystemImp::DispatchCharMessageA(UINT wParam, UINT nRepCnt, UINT nFlags)
{
	//	BOOL bUnicode = IsWindowUnicode(hWnd);
	//NOTE: 字符集的UTF8编码 有可能会包含多个字节, 而不总是2. 可知的 GB18030, GBK 
	// 会有4字节编码, 而所有的字符集编码具体有多少个字节, 现在亦未可只, 先给予一个
	// 宏定义, 来包含足够的缓存. MAX_ANSICHAR_BUF

	static CHAR sAnsiCharBuf[MAX_ANSICHAR_BUF];
	static CHAR sUTF8CharBuf[MAX_ANSICHAR_BUF];

	static WCHAR sUniCharBuf[MAX_ANSICHAR_BUF];
	
	static int  sAnsiCharCnt = 0;
	static BOOL bDBCS = FALSE;
	//static MSG sAppendCharMsg; 


	if (bDBCS)
	{
		sAnsiCharBuf[sAnsiCharCnt++] = (CHAR)wParam;
		bDBCS =  FALSE;
	}else if(IsDBCSLeadByteEx(CP_ACP, wParam))
	{
		bDBCS = TRUE;
		sAnsiCharBuf[sAnsiCharCnt++] = (CHAR)wParam;
	}else
	{
		sAnsiCharBuf[sAnsiCharCnt++] = (CHAR)wParam;
	}

	if (bDBCS == FALSE)
	{
		// flush the buffer

		MultiByteToWideChar(CP_ACP, 0, sAnsiCharBuf, -1, sUniCharBuf, MAX_ANSICHAR_BUF);

		WideCharToMultiByte( CP_UTF8,0, sUniCharBuf, -1, sUTF8CharBuf, MAX_ANSICHAR_BUF, NULL,NULL );

		INT nUniChar = MultiByteToWideChar(CP_UTF8, 0, sUTF8CharBuf, -1, sUniCharBuf, MAX_ANSICHAR_BUF);

		if (nUniChar > 0 && sUniCharBuf[0] >= 0x20)		// 0x20 以下全部为非打印字符
		{
			if (m_DesktopWindow)
			{
				m_DesktopWindow->Char(sUniCharBuf[0], nRepCnt, nFlags);
			}
		}

		sAnsiCharCnt = 0;
	}


	//	char* pEnd = sAnsiCharBuf + sAnsiCharCnt;
	//	char* pNextStr = CharNextExA(CP_ACP, sAnsiCharBuf, 0);
	//	if(*pNextStr == '\0' || pNextStr != sAnsiCharBuf)
	//	{
	//
	//	}

	// 一次UNICODE 字符输入会由多次 WM_CHAR 消息传入. 因此, 我们可以从第一次接受开始,
	// 将窗口消息队列中的所有字符全部取出.
	//while (!PeekMessageA(&sAppendCharMsg, m_hMainWnd, WM_CHAR, WM_CHAR, PM_NOREMOVE/*|PM_NOYIELD*/))
	//{
	//	TranslateMessage(&sAppendCharMsg);	
	//}

}

void USystemImp::DispatchCharMessageW(UINT wParam, UINT nRepCnt, UINT nFlags)
{
	//	BOOL bUnicode = IsWindowUnicode(hWnd);

	//if (nMsg == WM_CHAR)
	//{
	//	// Char
	//	WCHAR Str[256];
	//	WCHAR CharCode = (WCHAR)wParam;
	//	Str[0] = CharCode;
	//	Str[1] = 0;
	//	if (Str[0] != 0)
	//	{
	//		return;
	//	}else
	//	{
	//		Str[2] = 0;
	//	}
	//}
}

void USystemImp::KeyMessageA(UINT nMsg, UINT wParam, UINT lParam)
{
	assert(m_DesktopWindow);
	UINT ScanCode    = (lParam >> 16) & 0xFF;	// DIK 
	BOOL Extended   =  lParam & (1 << 24);       // Enhanced keyboard key
	BOOL Previous   = lParam & (1 << 30);       // Previously down
	BOOL Press       = (nMsg == WM_KEYDOWN || nMsg == WM_SYSKEYDOWN);
	KeyCodes lk    =  VKToLKey(wParam);
	UINT Modifier = 0;
#ifdef _DEBUG
	if (lk != ScanCode)
	{
		//UTRACE("DIK Scancode != LK");
	}
#endif 

	if (nMsg == WM_CHAR || nMsg == WM_IME_CHAR)
	{		
		DispatchCharMessageA(wParam, Previous ? 1: 0, m_DesktopWindow->m_uModifierFlags);
		return;
	}

	// 按键不可识别
	if (lk != LK_NULL )
	{
		KeyCodes Exlk = lk;
		if (Extended)
		{
			switch(lk)
			{
			case LK_LALT:
				Exlk = LK_RALT;
				break;
			case LK_LCONTROL:
				Exlk = LK_RCONTROL;
				break;
			case LK_LSHIFT:
				Exlk = LK_RSHIFT;
				break;
			}
		}

		switch (Exlk) 
		{
			//case LK_LALT:     Modifier = LKM_LALT;   break;
			//case LK_RALT:     Modifier = LKM_RALT;   break;
			//case LK_LSHIFT:   Modifier = LKM_LSHIFT; break;
			//case LK_RSHIFT:   Modifier = LKM_RSHIFT; break;
			//case LK_LCONTROL: Modifier = LKM_LCTRL;  break;
			//case LK_RCONTROL: Modifier = LKM_RCTRL;  break;
			case LK_LALT:     Modifier = LKM_ALT;   break;
			case LK_RALT:     Modifier = LKM_ALT;   break;
			case LK_LSHIFT:   Modifier = LKM_SHIFT; break;
			case LK_RSHIFT:   Modifier = LKM_SHIFT; break;
			case LK_LCONTROL: Modifier = LKM_CTRL;  break;
			case LK_RCONTROL: Modifier = LKM_CTRL;  break;
		}

		static BYTE KeyStates[256];
		if (m_DesktopWindow)
		{
			if (Press)
			{
				m_DesktopWindow->m_uModifierFlags |= Modifier;
				KeyStates[wParam] |= 0x80;
			}else
			{
				m_DesktopWindow->m_uModifierFlags &= ~Modifier;
				KeyStates[wParam] &= 0x7f;
			}
		}

		if (Press)
		{
			m_DesktopWindow->KeyDown(Exlk, Previous ? 1 : 0, m_DesktopWindow->m_uModifierFlags);
		}else
		{
			m_DesktopWindow->KeyUp(Exlk, Previous ? 1 : 0, m_DesktopWindow->m_uModifierFlags);
		}
	}

	
	
}


void USystemImp::KeyMessageW(UINT nMsg, UINT wParam, UINT lParam)
{
	assert(m_DesktopWindow);

	UINT ScanCode    = (lParam >> 16) & 0xFF;	// DIK 
	BOOL Extended   =  lParam & (1 << 24);       // Enhanced keyboard key
	BOOL Previous   = lParam & (1 << 30);       // Previously down
	BOOL Press       = (nMsg == WM_KEYDOWN || nMsg == WM_SYSKEYDOWN);
	KeyCodes lk    =  VKToLKey(wParam);
	UINT Modifier = 0;
#ifdef _DEBUG
	if (lk != ScanCode)
	{
		//UTRACE("DIK Scancode != LK");
	}
#endif 

	// 按键不可识别
	if (lk == LK_NULL)
	{
		return;
	}

	KeyCodes Exlk = lk;
	if (Extended)
	{
		switch(lk)
		{
		case LK_LALT:
			Exlk = LK_RALT;
			break;
		case LK_LCONTROL:
			Exlk = LK_RCONTROL;
			break;
		case LK_LSHIFT:
			Exlk = LK_RSHIFT;
			break;
		}
	}

	switch (Exlk) 
	{
	//case LK_LALT:     Modifier = LKM_LALT;   break;
	//case LK_RALT:     Modifier = LKM_RALT;   break;
	//case LK_LSHIFT:   Modifier = LKM_LSHIFT; break;
	//case LK_RSHIFT:   Modifier = LKM_RSHIFT; break;
	//case LK_LCONTROL: Modifier = LKM_LCTRL;  break;
	//case LK_RCONTROL: Modifier = LKM_RCTRL;  break;
	case LK_LALT:     Modifier = LKM_ALT;   break;
	case LK_RALT:     Modifier = LKM_ALT;   break;
	case LK_LSHIFT:   Modifier = LKM_SHIFT; break;
	case LK_RSHIFT:   Modifier = LKM_SHIFT; break;
	case LK_LCONTROL: Modifier = LKM_CTRL;  break;
	case LK_RCONTROL: Modifier = LKM_CTRL;  break;
	}

	static BYTE KeyStates[256];
	if (m_DesktopWindow)
	{
		if (Press)
		{
			m_DesktopWindow->m_uModifierFlags |= Modifier;
			KeyStates[wParam] |= 0x80;
		}else
		{
			m_DesktopWindow->m_uModifierFlags &= ~Modifier;
			KeyStates[wParam] &= 0x7f;
		}
	}

	if (nMsg == WM_CHAR)
	{
		DispatchCharMessageW(wParam, Previous ? 1: 0, m_DesktopWindow->m_uModifierFlags);
		return;
	}

	if (Press)
	{
		m_DesktopWindow->KeyDown(Exlk, Previous ? 1 : 0, m_DesktopWindow->m_uModifierFlags);
	}else
	{
		m_DesktopWindow->KeyUp(Exlk, Previous ? 1 : 0, m_DesktopWindow->m_uModifierFlags);
	}
}
