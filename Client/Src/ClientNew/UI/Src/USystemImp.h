#pragma once
#include "UDesktop.h"

class USkinImp : public USkin
{
public:
	USkinImp();
	virtual ~USkinImp();
	BOOL Create();
	std::string m_strName;
	std::string m_strTextureFile;
	std::vector<USKINITEM> m_Items;
	UTexturePtr m_spTexSurface;
private:
	virtual void Release();
	virtual const URect* GetSkinItemRect(int index) const
	{
		if (index >= 0 && index < (int)m_Items.size())
		{
			return &m_Items[index].SubSec;
		}
		return NULL;
	}
	virtual const USKINITEM* GetSkinItem(int index) const;
	virtual int GetItemNum() const;
	virtual const char* GetSkinName() const;
	virtual IUTexture* GetTexture() const;
	virtual BOOL IsAniSkin() const { return FALSE;}
};

class USystemImp
{
	friend class USystem;
public:
	enum
	{
		MAX_ANSICHAR_BUF = 8,	// 一次字符输入, 所引起的ANSI CHAR 的最大缓存个数.
	};
	USystemImp(void);
	~USystemImp(void);
	BOOL Initialize(HWND hWnd);
	void Destroy();
	BOOL InitSoftKeyBoard()
	{
		if (m_DesktopWindow)
		{
			return m_DesktopWindow->InitSoftKeyBoardControl();
		}
		return FALSE;
	}
	BOOL MouseMove(int x, int y);
	BOOL MouseBtnDown(UINT Btn);
	BOOL MouseBtnUp(UINT Btn);
	BOOL MouseWheel(int Delta);
	BOOL KeyDown(UINT Key);
	BOOL KeyUp(UINT Key);
	BOOL KeyPress(UINT Key);
	void KeyMessageA(UINT nMsg, UINT wParam, UINT lParam);
	void KeyMessageW(UINT nMsg, UINT wParam, UINT lParam);
	inline BOOL IsDraging() const
	{
		assert(m_DesktopWindow);
		return m_DesktopWindow->IsDraging();
	}
	
	const UPoint& GetCursorPos() const
	{
		assert(m_DesktopWindow);
		return m_DesktopWindow->GetCursorPos();
	}
	UControl* GetCapturedCtrl()
	{
		assert(m_DesktopWindow);
		return m_DesktopWindow->GetCapturedCtrl();
	}
	void CaptureControl(UControl* pTargetCtrl)
	{
		assert(m_DesktopWindow);
		m_DesktopWindow->CaptureControl(pTargetCtrl);
	}
	void ReleaseCapture(UControl* pCapturedCtrl)
	{
		assert(m_DesktopWindow);
		m_DesktopWindow->ReleaseCapture(pCapturedCtrl);
	}
	UControl* GetFocus()
	{
		assert(m_DesktopWindow);
		return m_DesktopWindow->GetFocus();
	}
	void LoadFontResource();
	void RemoveAllFont();
	BOOL LoadControlStyle(const char* pFileName);
	inline UControlStyle* FindStyle(const std::string& name);
	inline BOOL RegisterStyle(UControlStyle* pStyle);
	void UnRegisterAllStyles();

	BOOL LoadAccelarKeyMap(const char* pFileName, bool bCover);
	BOOL SaveAccelarKeyMap(const char* pFileName);
	inline UAccelarKey * FindKey(const std::string & name);
	inline BOOL SetKey(const std::string & name, UINT nKeyCode, UINT nFlags);
	inline BOOL RegisterKeyMap(UAccelarKey* pAccelarKey);
	void UnRegisterAllKeys();

	BOOL LoadColorTable(const char* pFileName);
	inline UColorTable * FindColor(const std::string& name);
	inline BOOL RegisterColor(UColorTable* color);
	void UnRegisterAllColor();

	USkinPtr LoadSkin(const char* filename);
	USkinImp* FindSkin(const std::string& skinName);
	void DestroySkin(USkin* pSkin);

	UControl* LoadDialog(const char* pBuffer, int size);
	void DestoryControl(UControl* pControl);

	inline const UClass* FindClass(const std::string& clsName);
	inline UObject* CreateObject(const std::string& clsName);
	BOOL WindowKillFouce()
	{
		if (m_DesktopWindow)
		{
			m_DesktopWindow->WindowKillFouce();
		}
		return TRUE;
	};
	BOOL SetFrameControl(UControl* pFrameCtrl)
	{
		if (m_DesktopWindow)
		{
			m_DesktopWindow->SetFrameWindow(pFrameCtrl);
			return TRUE;
		}
		return FALSE;
	}
	inline UControl* GetCurFrame() 
	{
		if (m_DesktopWindow)
		{
			return m_DesktopWindow->GetCurFrame();
		}
		return NULL;
	}
	void PushDialog(UControl* pDialog)
	{
		if (m_DesktopWindow)
		{
			m_DesktopWindow->PushDialogControl(pDialog);
		}
	}
	void PopDialog(UControl* pDialog)
	{
		if (m_DesktopWindow)
		{
			m_DesktopWindow->PopDialog(pDialog);
		}
	}
	void Draw()
	{
		if(m_DesktopWindow && UControl::sm_UiRender)
		{
			m_DesktopWindow->Render(0);
		}
	}
	inline void Update(float dt) 
	{
		if (m_DesktopWindow)
		{
			m_DesktopWindow->Update(dt);
		}
	}
	void Resize(int x, int y)
	{
		if (x <= 0 || y <= 0)
		{
			return;
		}
		if (m_DesktopWindow)
		{
			m_DesktopWindow->SetSize(UPoint(x,y));
		}
	}
	void WindowResize(int x, int y)
	{
		if (x <= 0 || y <= 0)
		{
			return;
		}
		if (m_DesktopWindow)
		{
			m_DesktopWindow->SetWindowSize(x, y);
		}
	}

	void SetSoundInterface(IUSoundInterface* iSI) { m_pSI = iSI; }
	void PlaySound(const char* pszSnd) 
	{
		if (m_pSI)
		{
			m_pSI->PlaySound(pszSnd);
		}
	}
	void PlaySound(int index)
	{
		if (m_pSI)
		{
			m_pSI->PlaySound(index);
		}
	}
	void SetMouseControl(UControl* pWnd)
	{
		if(m_DesktopWindow)
		{
			m_DesktopWindow->SetMouseControl(pWnd);
		}
	}
private:
	void DispatchCharMessageA(UINT wParam, UINT nRepCnt, UINT nFlags);
	void DispatchCharMessageW(UINT wParam, UINT nRepCnt, UINT nFlags);
	void LinkUiClasses();
	void UnLinkClasses();
protected:
	typedef stdext::hash_map<std::string, const UClass*> UiClassMap;
	typedef std::map<std::string, UControlStyle*> UiStyleMap;
	typedef std::map<std::string, USkinImp*> USkinMap;
	typedef std::map<std::string, UAccelarKey*> UAccelarkeyMap;
	typedef std::map<std::string, UColorTable*> ColorTable;
	HWND m_hMainWnd;
	BOOL m_bEnableIME;
	char m_szRootPath[MAX_PATH];
	UiClassMap m_ClassMap;
	UiStyleMap m_StyleMap;
	USkinMap m_SkinMap;
	UAccelarkeyMap m_AccelarKeyMap;
	ColorTable m_ColorTable;
	UDesktop* m_DesktopWindow;

	IUSoundInterface* m_pSI;
};

inline BOOL USystemImp::RegisterStyle(UControlStyle* pStyle)
{
	assert(pStyle);
	std::string stlname = pStyle->m_Name.GetBuffer();
	UControlStyle* pExist = FindStyle(stlname);
	if (pExist && pExist != pStyle)
	{
		return FALSE;
	}else if (pExist == pStyle)
	{
		return TRUE;
	}
	pStyle->AddRef();
	m_StyleMap.insert(UiStyleMap::value_type(stlname, pStyle));
	return TRUE;
}

inline UControlStyle* USystemImp::FindStyle(const std::string& name)
{
	UiStyleMap::const_iterator it = m_StyleMap.find(name);
	if (it != m_StyleMap.end())
	{
		return it->second;
	}
	return NULL;
}
inline UAccelarKey * USystemImp::FindKey(const std::string & name)
{
	UAccelarkeyMap::const_iterator it = m_AccelarKeyMap.find(name);
	if (it != m_AccelarKeyMap.end())
	{
		return it->second;
	}
	return NULL;
}
inline BOOL USystemImp::SetKey(const std::string & name, UINT nKeyCode, UINT nFlags)
{
	UAccelarkeyMap::const_iterator it = m_AccelarKeyMap.find(name);
	if (it != m_AccelarKeyMap.end())
	{
		std::string KeyStr;	
		if (nFlags & LKM_CTRL)
		{
			KeyStr += "LCONTROL+";
		}
		if (nFlags & LKM_ALT)
		{
			KeyStr += "LALT+";
		}
		if (nFlags & LKM_SHIFT)
		{
			KeyStr += "LSHIFT+";
		}
		const char* buf = LK2StringKey(nKeyCode);
		if (buf)
		{
			it->second->mAccelarKeyCode.Set(buf);
		}
		else
		{
			return FALSE;
		}
		it->second->mAccelarKeyModify.Set(KeyStr.c_str());
		return TRUE;
	}
	return FALSE;
}
inline BOOL USystemImp::RegisterKeyMap(UAccelarKey* pAccelarKey)
{
	assert(pAccelarKey);
	std::string akname = pAccelarKey->mName.GetBuffer();
	UAccelarKey* pExist = FindKey(akname);
	if (pExist && pExist != pAccelarKey)
	{
		return FALSE;
	}else if (pExist == pAccelarKey)
	{
		return TRUE;
	}
	//pAccelarKey->AddRef();
	m_AccelarKeyMap.insert(UAccelarkeyMap::value_type(akname, pAccelarKey));
	return TRUE;
}
inline BOOL USystemImp::RegisterColor(UColorTable* color)
{
	assert(color);
	std::string Colordesc = color->mColorDesc.GetBuffer();
	if(!Colordesc.length())
		return FALSE;
	UColorTable* pExist = FindColor(Colordesc);
	if (pExist && pExist != color)
	{
		return FALSE;
	}
	else if (pExist == color)
	{
		return TRUE;
	}
	//color->AddRef();
	ULOG("Color:RegisterColor %s Success",Colordesc.c_str());
	m_ColorTable.insert(ColorTable::value_type(Colordesc,color));
	return TRUE;
}
inline UColorTable* USystemImp::FindColor(const std::string& name)
{
	ColorTable::const_iterator it = m_ColorTable.find(name);
	if (it != m_ColorTable.end())
	{
		return it->second;
	}
	return NULL;
}
inline const UClass* USystemImp::FindClass(const std::string& clsName)
{
	UiClassMap::const_iterator it = m_ClassMap.find(clsName);
	if (it != m_ClassMap.end())
	{
		return it->second;
	}
	return NULL;
}

inline USkinImp* USystemImp::FindSkin(const std::string& skinName)
{
	USkinMap::const_iterator it = m_SkinMap.find(skinName);
	if (it != m_SkinMap.end())
	{
		return it->second;
	}
	return NULL;
}

inline UObject* USystemImp::CreateObject(const std::string& clsName)
{
	const UClass* pCls = FindClass(clsName);
	if (pCls)
	{
		return pCls->CreateObject();
	}
	return NULL;
}
inline BOOL USystemImp::MouseMove(int x, int y)
{
	if (m_DesktopWindow)
	{
		m_DesktopWindow->MouseMove(x,y);
	}
	return TRUE;
}

inline BOOL USystemImp::MouseBtnDown(UINT Btn)
{
	if (m_DesktopWindow)
	{
		if (Btn == LK_LEFTBTN)
		{
			m_DesktopWindow->LeftBtnDown();
		}else if (Btn == LK_RIGHTBTN)
		{
			m_DesktopWindow->RightBtnDown();
		}else if (Btn == LK_MIDBTN)
		{
			//m_DesktopWindow->;
		}
		
		return TRUE;
	}
	return FALSE;
}

inline BOOL USystemImp::MouseBtnUp(UINT Btn)
{
	if (m_DesktopWindow)
	{
		if (Btn == LK_LEFTBTN)
		{
			m_DesktopWindow->LeftMouseUp();
		}else if (Btn == LK_RIGHTBTN)
		{
			m_DesktopWindow->RightMouseUp();
		}else if (Btn == LK_MIDBTN)
		{
			//m_DesktopWindow->;
		}
		return TRUE;
	}
	return FALSE;
}

inline BOOL USystemImp::MouseWheel(int Delta)
{
	if (m_DesktopWindow)
	{
		m_DesktopWindow->MouseWheel(Delta);
	}
	return TRUE;
}

inline BOOL USystemImp::KeyDown(UINT Key)
{
	KeyCodes Exlk = (KeyCodes)Key;
	if (KeyPress(Key))
	{
		return TRUE;
	}
	if (m_DesktopWindow)
	{
		m_DesktopWindow->KeyDown(Exlk, 0, m_DesktopWindow->m_uModifierFlags);
	}
	return TRUE;
}
inline BOOL USystemImp::KeyPress(UINT Key)
{
	BOOL ModifierFlag = FALSE ;
	KeyCodes Exlk = (KeyCodes)Key;
	UINT Modifier = 0;
	if (Key != LK_NULL )
	{
		ModifierFlag = TRUE;

			switch(Key)
			{
			case LK_LALT:
				Exlk = LK_RALT;
				break;
			case LK_LCONTROL:
				Exlk = LK_RCONTROL;
				break;
			case LK_LSHIFT:
				Exlk = LK_RSHIFT;
				break;
			}
		
			

		switch (Exlk) 
		{
		case LK_LALT:     Modifier = LKM_ALT;   break;
		case LK_RALT:     Modifier = LKM_ALT;   break;
		case LK_LSHIFT:   Modifier = LKM_SHIFT; break;
		case LK_RSHIFT:   Modifier = LKM_SHIFT; break;
		case LK_LCONTROL: Modifier = LKM_CTRL;  break;
		case LK_RCONTROL: Modifier = LKM_CTRL;  break;
		default:
			ModifierFlag = FALSE ;
			break;
		}
	}

	if (m_DesktopWindow && ModifierFlag)
	{
		m_DesktopWindow->m_uModifierFlags |= Modifier;

		return TRUE;
	}

	return FALSE;
}
inline BOOL USystemImp::KeyUp(UINT Key)
{
	BOOL ModifierFlag = FALSE ;
	UINT Modifier = 0;
	KeyCodes Exlk = (KeyCodes)Key;
	if (Key != LK_NULL )
	{
		ModifierFlag = TRUE;
		switch(Key)
		{
		case LK_LALT:
			Exlk = LK_RALT;
			break;
		case LK_LCONTROL:
			Exlk = LK_RCONTROL;
			break;
		case LK_LSHIFT:
			Exlk = LK_RSHIFT;
			break;
		}


		switch (Exlk) 
		{
		case LK_LALT:     Modifier = LKM_ALT;   break;
		case LK_RALT:     Modifier = LKM_ALT;   break;
		case LK_LSHIFT:   Modifier = LKM_SHIFT; break;
		case LK_RSHIFT:   Modifier = LKM_SHIFT; break;
		case LK_LCONTROL: Modifier = LKM_CTRL;  break;
		case LK_RCONTROL: Modifier = LKM_CTRL;  break;
		default:
			ModifierFlag = FALSE;
			break;
		}
	}

	if (m_DesktopWindow && ModifierFlag)
	{
		m_DesktopWindow->m_uModifierFlags  &= ~Modifier;
		return  TRUE;
	}

	if (m_DesktopWindow)
	{
		m_DesktopWindow->KeyUp(Key, 0, m_DesktopWindow->m_uModifierFlags);
	}
	return TRUE;
}

