#include "StdAfx.h"
#include "../../../TinyXML/tinyxml.h"

#include "USystemImp.h"
#include "USystem.h"

namespace
{
	
class UDialogParser
{
public:
	UControl* Parse(USystemImp* Sys, const char* pBuffer, int size)
	{
		TiXmlDocument xdoc;
		xdoc.Parse(pBuffer);
		if( xdoc.Error() )
		{
			const char* Error = xdoc.ErrorDesc();
			UTRACE("parse udg file failed. error : %s", Error);
			return NULL;
		}
		TiXmlElement* pElement = xdoc.RootElement();
		// 我们不支持在一个文档里定义多个顶级对象.
		//TiXmlElement* pElement2 = xdoc.FirstChildElement();
		if (pElement == NULL)
		{
			return NULL;
		}
		UControl* pControl = NULL;
		ProcessElement(Sys,pControl, pElement);
		return pControl;
	}
private:
	enum ElementType
	{
		ET_UNKNOWN = 0,
		ET_PROP,
		ET_WINDOW,
	};
	BOOL ProcessElement(USystemImp* Sys, UControl*& pControl, TiXmlElement* pElement)
	{
		static const char* class_str = "class";

		const char* pType = pElement->Value();
		ElementType eType = ET_UNKNOWN;
		if (stricmp(pType, "property") == 0)
		{
			eType = ET_PROP;
		}else if (stricmp(pType, "window") == 0)
		{
			eType = ET_WINDOW;
		}else
		{
			//warning.
			UTRACE("found unknown element %s", pType);
		}

		if (eType == ET_WINDOW)
		{
			const char* clsName = pElement->Attribute(class_str);
			if (clsName == NULL)
			{
				UTRACE("the control class is undefined");
				return FALSE;
			}
			UControl* pNewControl = (UControl*)Sys->CreateObject(clsName);	// make by class

			if (pNewControl == NULL)
			{
				UTRACE("control class %s is undefined", clsName);
				return FALSE;
			}
			if (pControl)
			{
				pControl->AddChild(pNewControl);
			}else
			{
				pControl = pNewControl;
			}
			const char* ID = pElement->Attribute("id");
			if (ID)
			{
				int nid = atoi(ID);
				pNewControl->SetId(nid);
			}

			TiXmlElement* pChildElement = pElement->FirstChildElement();
			BOOL bSuccess = TRUE;
			while (pChildElement /*&& bSuccess*/ )
			{
				bSuccess = ProcessElement(Sys,pNewControl, pChildElement);
				pChildElement = pChildElement->NextSiblingElement();
			}
			
			// 我们忽略未处理成功的,但是不取消整个进程.

		}else if (eType == ET_PROP)
		{
			if (pControl == NULL)
			{
				return FALSE;
			}
			
			TiXmlAttribute* pAttr = pElement->FirstAttribute();
			const char* pField;
			const char* pValue;
			while (pAttr)
			{
				pField = pAttr->Name();
				pValue = pAttr->Value();
				pControl->SetField(pField, pValue);
				pAttr = pAttr->Next();
			}
		}
		
		return TRUE;
	}
};
class UAccelarkeyParser
{
public:
	BOOL Parse(USystemImp* Sys, const char* pBuffer, int size, bool bCover)
	{
		TiXmlDocument xdoc;
		xdoc.Parse(pBuffer);
		if (xdoc.Error())
		{
			const char* Error = xdoc.ErrorDesc();
			UTRACE("parse Accelarkey failed, error : %s", Error);
			return FALSE;
		}
		TiXmlElement* pStyles = xdoc.RootElement();
		if (pStyles == NULL)
		{
			return FALSE;
		}
		if(stricmp(pStyles->Value(), "AccelerKeys") != 0)
		{
			UTRACE("unknown Accelarkey element");
			return FALSE;
		}

		TiXmlElement* pChildElem = pStyles->FirstChildElement();
		while (pChildElem)
		{
			LoadKeyMap(Sys, pChildElem, bCover);
			pChildElem = pChildElem->NextSiblingElement();
		}

		return TRUE;
	}
private:
	BOOL LoadKeyMap(USystemImp* Sys, TiXmlElement* pElement, bool bCover)
	{
		static const char* Key_name_str = "ActionCode";
		const char* pKeyName = pElement->Attribute(Key_name_str);
		if (pKeyName == NULL)
		{
			UTRACE("Key must with a Action defined.");
			return FALSE;
		}
		UAccelarKey* pAccelarKey = Sys->FindKey(pKeyName);
		if (pAccelarKey)
		{
			// 已经存在同名样式.
			UTRACE("AccelerKey with name %s already exist, change it.", pKeyName);
			TiXmlAttribute* pAttr = pElement->FirstAttribute();
			const char* pField;
			const char* pValue;
			while (pAttr)
			{
				pField = pAttr->Name();
				pValue = pAttr->Value();
				pAccelarKey->SetField(pField,pValue);
				pAttr = pAttr->Next();
			}
			return TRUE;
		}else
		{
			ULOG("load AccelerKey %s", pKeyName);
		}
		if (!bCover)
		{
			UAccelarKey * pNewAccelKey = new UAccelarKey;
			assert(pNewAccelKey);
			pNewAccelKey->mName.Set(pKeyName);

			TiXmlAttribute* pAttr = pElement->FirstAttribute();
			const char* pField;
			const char* pValue;
			while (pAttr)
			{
				pField = pAttr->Name();
				pValue = pAttr->Value();
				pNewAccelKey->SetField(pField,pValue);
				pAttr = pAttr->Next();
			}
			if(!Sys->RegisterKeyMap(pNewAccelKey))
			{
				UTRACE("register style %s failed.", pKeyName);
				delete pNewAccelKey;
				return FALSE;
			}
		}
		return TRUE;
	}
};
class UStyleParser
{
public:
	BOOL Parse(USystemImp* Sys, const char* pBuffer, int size)
	{
		TiXmlDocument xdoc;
		xdoc.Parse(pBuffer);
		if (xdoc.Error())
		{
			const char* Error = xdoc.ErrorDesc();
			UTRACE("parse style failed, error : %s", Error);
			return FALSE;
		}
		TiXmlElement* pStyles = xdoc.RootElement();
		if (pStyles == NULL)
		{
			return FALSE;
		}
		if(stricmp(pStyles->Value(), "styles") != 0)
		{
			UTRACE("unknown styles element");
			return FALSE;
		}
		
		TiXmlElement* pChildElem = pStyles->FirstChildElement();
		while (pChildElem)
		{
			LoadStyle(Sys, pChildElem);
			pChildElem = pChildElem->NextSiblingElement();
		}
		
		return TRUE;
	}
private:
	BOOL LoadStyle(USystemImp* Sys, TiXmlElement* pElement)
	{
		static const char* style_name_str = "name";
		const const char* pStyleName = pElement->Attribute(style_name_str);
		if (pStyleName == NULL)
		{
			UTRACE("style must with a name defined.");
			return FALSE;
		}
		UControlStyle* pStyle = Sys->FindStyle(pStyleName);
		if (pStyle)
		{
			// 已经存在同名样式.
			UTRACE("style with name %s already exist, ignore.", pStyleName);
			return FALSE;
		}else
		{
			ULOG("load control style %s", pStyleName);
		}
		UControlStyle* pNewStyle = new UControlStyle;
		assert(pNewStyle);
		pNewStyle->m_Name.Set(pStyleName);
		
		const char* pPropElementName;
		TiXmlElement* pPropElement = pElement->FirstChildElement();
		while (pPropElement)
		{
			pPropElementName = pPropElement->Value();
			if (stricmp(pPropElementName, "property") == 0)
			{
				TiXmlAttribute* pAttr = pPropElement->FirstAttribute();
				const char* pField;
				const char* pValue;
				while (pAttr)
				{
					pField = pAttr->Name();
					pValue = pAttr->Value();
					pNewStyle->SetField(pField, pValue);
					pAttr = pAttr->Next();
				}
			}
			pPropElement = pPropElement->NextSiblingElement();
		}

		if(!Sys->RegisterStyle(pNewStyle))
		{
			UTRACE("register style %s failed.", pStyleName);
			delete pNewStyle;
			return FALSE;
		}
		return TRUE;
	}

};
class UColorTableParser
{
public:
	BOOL Parse(USystemImp* Sys, const char* pBuffer, int size)
	{
		TiXmlDocument xDoc;
		xDoc.Parse(pBuffer);	
		if (xDoc.Error())
		{
			const char* Error = xDoc.ErrorDesc();
			UTRACE("parse ColorTable failed, error : %s", Error);
			return FALSE;
		}		
		TiXmlElement* pColors = xDoc.RootElement();
		if (pColors == NULL)
		{
			return FALSE;
		}
		if(stricmp(pColors->Value(), "Colors") != 0)
		{
			UTRACE("unknown Color element");
			return FALSE;
		}
		TiXmlElement* pChildElem = pColors->FirstChildElement();
		while (pChildElem)
		{
			const char* pItemName;
			UColorTable * NewColorItem = new UColorTable;
			assert(NewColorItem);
			pItemName = pChildElem->Value();
			if (stricmp(pItemName,"ColorItem") == 0)
			{
				TiXmlAttribute* pAttr = pChildElem->FirstAttribute();
				const char* pField;
				const char* pValue;
				while (pAttr)
				{
					pField = pAttr->Name();
					pValue = pAttr->Value();
					NewColorItem->SetField(pField, pValue);
					pAttr = pAttr->Next();
				}
			}
			if (!Sys->RegisterColor(NewColorItem))
			{
				UTRACE("RegisterColor %s failed.", NewColorItem->mColorDesc.GetBuffer());
				delete NewColorItem;
				return FALSE;
			}
			pChildElem = pChildElem->NextSiblingElement();
		}	
		return TRUE;
	}
};
class USkinParser
{
public:
	static BOOL Parse(USkinImp** pSkin, const char* pBuffer, int size, const char* skinname)
	{
		TiXmlDocument xdoc;
		xdoc.Parse(pBuffer);
		if (xdoc.Error())
		{
			const char* Error = xdoc.ErrorDesc();
			UTRACE("parse skin file %s failed. error: %s", skinname, Error);
			return FALSE;
		}
		TiXmlElement* pElement = xdoc.RootElement();
		// 我们不支持在一个文档里定义多个顶级对象.
		//TiXmlElement* pElement2 = xdoc.FirstChildElement();
		if (pElement == NULL)
		{
			UTRACE("can't find the \"skin\" element in %s" , skinname);
			return FALSE;
		}
		
		const char* ElementStr = pElement->Value();
		assert(ElementStr);
		if (stricmp(ElementStr, "skin") != 0)
		{
			UTRACE("invalid skin element %s.", ElementStr);
			return FALSE;
		}
		
		const char* texfile = pElement->Attribute("texture");
		if (texfile == NULL)
		{
			UTRACE("skin without \"texture\" attribute");
			return FALSE;
		}
		
		USkinImp* pNewSkin = new USkinImp;
		char Buf[MAX_PATH];
		strcpy(Buf, skinname);
		char* slash = strrchr(Buf, '\\');
		if (slash)
		{
			strcpy(slash + 1, texfile);
		}else
		{
			// root.
			strcpy(Buf, texfile);
		}
		pNewSkin->m_strTextureFile = Buf;
		
		const char* pItemName;
		const char* pAttrValue;
		USKINITEM NewItem;
		int x,y,w,h;
		pElement = pElement->FirstChildElement();
		while (pElement)
		{
			pItemName = pElement->Value();
			if (stricmp(pItemName, "item") == 0)
			{
				pAttrValue = pElement->Attribute("x");
				if (pAttrValue)
				{
					x = atoi(pAttrValue); 
				}else
				{
					x = 0;
				}
				pAttrValue = pElement->Attribute("y");
				if (pAttrValue)
				{
					y = atoi(pAttrValue); 
				}else
				{
					y = 0;
				}
				pAttrValue = pElement->Attribute("w");
				if (pAttrValue)
				{
					w = atoi(pAttrValue); 
				}else
				{
					w = 0;
				}

				pAttrValue = pElement->Attribute("h");
				if (pAttrValue)
				{
					h = atoi(pAttrValue); 
				}else
				{
					h = 0;
				}
			
				NewItem.SubSec.left = x;
				NewItem.SubSec.top = y;
				NewItem.SubSec.right = x + w;
				NewItem.SubSec.bottom = y + h;
				pNewSkin->m_Items.push_back(NewItem);
			}
			pElement = pElement->NextSiblingElement();
		}
		
		*pSkin = pNewSkin;
		return TRUE;
	}
};
} // namespace


BOOL USystemImp::LoadAccelarKeyMap(const char *pFileName, bool bCover)
{
	assert(pFileName && pFileName[0] != 0);

    CFileStream kFile;
    if( !kFile.Open(pFileName) )
    {
        UTRACE("Can't Load KeyMap File %s", pFileName);
        return FALSE;
    }

    char* acBuffer = (char*)malloc(64*1024);
    unsigned int uiSize = kFile.Read(acBuffer, 64*1024);
    assert(uiSize < 64*1024 - 1);
    acBuffer[uiSize] = '\0';

	UAccelarkeyParser Parser;
	Parser.Parse(this, acBuffer, uiSize, bCover);

	free( acBuffer );
	return TRUE;
}

BOOL USystemImp::SaveAccelarKeyMap(const char* pFileName)
{
	TiXmlDocument doc;
	TiXmlElement * root = new TiXmlElement("AccelerKeys");
	if (!root)
		return FALSE;
	doc.LinkEndChild(root);
	UAccelarkeyMap::iterator it = m_AccelarKeyMap.begin();
	while(it != m_AccelarKeyMap.end())
	{
		TiXmlElement * element = new TiXmlElement("AccelerKey");
		if (element)
		{
			root->LinkEndChild(element);
			element->SetAttribute("ActionCode", it->second->mActionName.GetBuffer());
			element->SetAttribute("KeyCode", it->second->mAccelarKeyCode.GetBuffer());
			const char* str = it->second->mAccelarKeyModify.GetBuffer();
			if (str)
			{
				element->SetAttribute("KeyModify", str);
			}
			else
			{
				element->SetAttribute("KeyModify", "");
			}
		}
		else
		{
			return FALSE;
		}
		++it;
	}
	return doc.SaveFile(pFileName);
}
BOOL USystemImp::LoadColorTable(const char* pFileName)
{
	assert(pFileName && pFileName[0] != 0);
	char Buf[MAX_PATH];
	LSprintf(Buf, MAX_PATH, "%s%s", m_szRootPath, pFileName);

    CFileStream kFile;
    if( !kFile.Open(Buf) )
    {
        UTRACE("Can't Load KeyMap File %s", pFileName);
        return FALSE;
    }

    char* acBuffer = (char*)malloc(64*1024);
    unsigned int uiSize = kFile.Read(acBuffer, 64*1024);
    assert(uiSize < 64*1024 - 1);
    acBuffer[uiSize] = '\0';

	UColorTableParser Parser;
	Parser.Parse(this, acBuffer, uiSize);
	free( acBuffer );
	return TRUE;
}

UControl* USystemImp::LoadDialog(const char* pBuffer, int size)
{
	UDialogParser Parser;
	UControl* pDialog = Parser.Parse(this,pBuffer, size);
	return pDialog;
}

static char* s_pFonts[] = 
{
    "UData\\Fonts\\ZYKai.ttf", 
	"UData\\Fonts\\方正北魏楷书简体.ttf",
	"UData\\Fonts\\FZMWFont.ttf",
	"UData\\Fonts\\文泉驿微米黑.ttf",
	"UData\\Fonts\\字幕黑体M.ttf",
    ""
};

void USystemImp::LoadFontResource()
{
    for(int i = 0; s_pFonts[i] && s_pFonts[i][0]; ++i)
    {
        if (!AddFontResourceEx(s_pFonts[i], FR_PRIVATE, 0))
            ULOG("Load Font Failed!");
    }

    SendMessage(m_hMainWnd,  WM_FONTCHANGE,  0,  0);
    ULOG("Load Font Succeed!");
}

void USystemImp::RemoveAllFont()
{
    for(int i = 0; s_pFonts[i] && s_pFonts[i][0]; ++i)
    {
        if (!RemoveFontResourceEx(s_pFonts[i], FR_PRIVATE, 0))
            ULOG("Remove Font Failed!");
    }

    SendMessage(m_hMainWnd,   WM_FONTCHANGE,   0,   0);
    ULOG("Remove Font Succeed!");
}

BOOL USystemImp::LoadControlStyle(const char* pFileName)
{
	assert(pFileName && pFileName[0] != 0);
	char Buf[MAX_PATH];
	LSprintf(Buf, MAX_PATH, "%s%s", m_szRootPath, pFileName);

    CFileStream kFile;
    if( !kFile.Open(Buf) )
    {
        UTRACE("can't open Control Style file %s.", Buf);
        return NULL;
    }

    char* acBuffer = (char*)malloc( 64*1024 );
    unsigned int uiSize = kFile.Read(acBuffer, 64*1024);
    assert(uiSize < 64*1024 - 1);
    acBuffer[uiSize] = '\0';

	UStyleParser Parser;
	Parser.Parse(this, acBuffer, uiSize);
	free( acBuffer );

	return TRUE;
}

USkinPtr USystemImp::LoadSkin(const char* filename)
{
	if (filename == NULL || filename[0] == 0)
	{
		return NULL;
	}
	std::string strFileName = filename;
	strupr(&strFileName[0]);
	for (int i = 0; i < (int)strFileName.size(); i++)
	{
		if (strFileName[i] == '/')	
		{
			strFileName[i] = '\\';
		}
	}
	USkinImp* pSkin = FindSkin(strFileName);
	if (pSkin)
	{
		return (USkin*)pSkin;
	}
	
	// load new skin.
	CHAR Buf[MAX_PATH];
	LSprintf(Buf,MAX_PATH,"%s%s", g_USystem->GetRootPath(), strFileName.c_str());

    CFileStream kFile;
    if( !kFile.Open(Buf) )
    {
        UTRACE("can't open the skin file %s", strFileName.c_str());
		return NULL;    
    }

    static char acBuffer[64*1024];
    unsigned int uiSize = kFile.Read(acBuffer, sizeof(acBuffer));
    assert(uiSize < sizeof(acBuffer) - 1);
    acBuffer[uiSize] = '\0';

	USkinImp* pRetSkin = NULL;
	BOOL bRet = USkinParser::Parse(&pRetSkin, acBuffer, uiSize, Buf);

	if (bRet)
	{
		assert(pRetSkin);
		if(!pRetSkin->Create())
		{
			UTRACE("create skin failed.");
			return NULL;
		}
		pRetSkin->m_strName = strFileName;
		m_SkinMap.insert(USkinMap::value_type(strFileName.c_str(), pRetSkin));
		return (USkin*)pRetSkin;
	}else
	{
		UTRACE("LoadSkin(%s) failed.", strFileName.c_str());
		return NULL;
	}
}