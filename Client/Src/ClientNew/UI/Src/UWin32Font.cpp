#include "StdAfx.h"
#include "UFont.h"

#define SHAPE_FONT 0

HDC UWin32FontInterface::sm_hFontDC = NULL;
HBITMAP UWin32FontInterface::sm_hFontBMP = NULL;
int UWin32FontInterface::sm_nRefCnt = 0;
//void CopyCharToBitmap(GBitmap *pDstBMP, HDC hSrcHDC, const URect &r);

void UWin32FontInterface::InitFontDC()
{
	if (sm_nRefCnt++ == 0)
	{
		sm_hFontDC = CreateCompatibleDC(NULL);
		sm_hFontBMP = CreateCompatibleBitmap(sm_hFontDC, 256, 256);
	}
}

void UWin32FontInterface::ReleaseFontDC()
{
	if (--sm_nRefCnt == 0)
	{
		DeleteObject(sm_hFontBMP);
		DeleteObject(sm_hFontDC);
	}
}
//
//void CopyCharToBitmap(GBitmap *pDstBMP, HDC hSrcHDC, const URect &r)
//{
//   for (int i = r.point.y; i < r.point.y + r.extent.y; i++)
//   {
//      for (int j = r.point.x; j < r.point.x + r.extent.x; j++)
//      {
//         COLORREF color = GetPixel(hSrcHDC, j, i);
//         if (color)
//            *pDstBMP->getAddress(j, i) = 255;
//         else
//            *pDstBMP->getAddress(j, i) = 0;
//      }
//   }
//}


UFontInterface *createPlatformFont(const char *name, UINT size, UINT charset /* = TGE_ANSI_CHARSET */)
{
	UFontInterface *retFont = new UWin32FontInterface;
	if(retFont->Create(name, size, charset))
		return retFont;
	delete retFont;
	return NULL;
}

UWin32FontInterface::UWin32FontInterface() : m_hFont(NULL)
{
}

UWin32FontInterface::~UWin32FontInterface()
{
	if(m_hFont)
	{
		DeleteObject(m_hFont);
	}
	ReleaseFontDC();
}

BOOL UWin32FontInterface::Create(const char *name, UINT size, UINT charset /* = TGE_ANSI_CHARSET */)
{
	InitFontDC();
	if(name == NULL || size < 1)
		return FALSE;

	char FontName[1024];
	strcpy(FontName,name);
	UINT weight = FW_MEDIUM;
	UINT doItalic = 0;

	const char* bold = strstr(name, "Bold");
	const char* italic = strstr(name, "Italic");

	if (bold != NULL && bold != name)
	{
		int _w = strlen(name);
		FontName[_w - 4] = '\0';
		weight = FW_SEMIBOLD;
	}
	else if (italic != NULL && italic != name)
	{
		int _w = strlen(name);
		FontName[_w - 6] = '\0';
		doItalic = 1;
	}

	// Just in case...
	bold = strstr(name, "bold");
	italic = strstr(name, "italic");

	if (bold != NULL && bold != name)
	{
		int _w = strlen(name);
		FontName[_w - 4] = '\0';
		weight = FW_SEMIBOLD;
	}
	else if (italic != NULL && italic != name)
	{		
		int _w = strlen(name);
		FontName[_w - 6] = '\0';
		doItalic = 1;
	}

#ifdef UNICODE
	WCHAR n[512];
	convertUTF8toUTF16((CHAR *)name, n, sizeof(n));
	m_hFont = CreateFont(size,0,0,0,weight,doItalic,0,0,DEFAULT_CHARSET,OUT_TT_PRECIS,0,PROOF_QUALITY,0,n);
#else
	//m_hFont = CreateFont(size,
	//	0,
	//	0,
	//	0,
	//	weight,
	//	doItalic,
	//	0,
	//	0,
	//	charset,
	//	OUT_TT_PRECIS,	//OUT_DEFAULT_PRECIS
	//	0,				//CLIP_DEFAULT_PRECIS
	//	PROOF_QUALITY,	//CLEARTYPE_QUALITY   ANTIALIASED_QUALITY  DEFAULT_QUALITY
	//	FF_DONTCARE,
	//	name
	//	);
	m_hFont = CreateFont(size ,
		0,
		0,
		0,
		weight,
		doItalic,
		0,
		0,
		GB2312_CHARSET, //134
		OUT_DEFAULT_PRECIS, //OUT_DEFAULT_PRECIS
		CLIP_DEFAULT_PRECIS,    //CLIP_DEFAULT_PRECIS
		DEFAULT_QUALITY, //CLEARTYPE_QUALITY   ANTIALIASED_QUALITY  DEFAULT_QUALITY
		DEFAULT_PITCH|FF_DONTCARE,
		"����"//FontName ΢���ź�Bold
		);

#endif
	if(m_hFont == NULL)
		return FALSE;

	SelectObject(sm_hFontDC, sm_hFontBMP);
	SelectObject(sm_hFontDC, m_hFont);
	GetTextMetrics(sm_hFontDC, &mTextMetric);

	return TRUE;
}

BOOL UWin32FontInterface::IsValidChar(WCHAR ch) const
{
	return ch != 0 /* && (ch >= mTextMetric.tmFirstChar && ch <= mTextMetric.tmLastChar)*/;
}

//BOOL UWin32FontInterface::IsValidChar(const CHAR *str) const
//{
//
//    return IsValidChar(oneUTF8toUTF32(str));
//}

inline FIXED FixedFromDouble(double d)
{
	long l;
	l = (long) (d * 65536L);
	return *(FIXED *)&l;
}
BOOL UWin32FontInterface::GetCharDesc(WCHAR ch, CharInfo& cinfo, UFontGlyphData& GlyphData) const
{
	memset(&cinfo, 0, sizeof(cinfo));
	cinfo.bitmapIndex = -1;

	static BYTE scratchPad[65536];

	COLORREF backgroundColorRef = RGB(  0,   0,   0);
	COLORREF foregroundColorRef = RGB(255, 255, 255);
	SelectObject(sm_hFontDC, sm_hFontBMP);
	SelectObject(sm_hFontDC, m_hFont);
	SetBkColor(sm_hFontDC, backgroundColorRef);
	SetTextColor(sm_hFontDC, foregroundColorRef);

	MAT2 matrix;
	GLYPHMETRICS metrics;

	matrix.eM11 = FixedFromDouble(1);
	matrix.eM12 = FixedFromDouble(0);
	matrix.eM21 = FixedFromDouble(0);
	matrix.eM22 = FixedFromDouble(1);

	DWORD Ret = GetGlyphOutlineW(
		sm_hFontDC,	// handle of device context 
		ch,	// character to query 
		GGO_GRAY8_BITMAP,	// format of data to return 
		&metrics,	// address of structure for metrics 
		0,	// size of buffer for data 
		NULL,	// address of buffer for data 
		&matrix 	// address of transformation matrix structure  
		);

	if( Ret != GDI_ERROR)
	{
		if(GetGlyphOutlineW(sm_hFontDC, ch, GGO_GRAY8_BITMAP, &metrics, sizeof(scratchPad), scratchPad, &matrix) == GDI_ERROR)
		{
			SIZE size;
			GetTextExtentPoint32W(sm_hFontDC, &ch, 1, &size);
			if(size.cx)
			{
				cinfo.xIncrement = (BYTE)size.cx;
				cinfo.bitmapIndex = 0;
			}
			return FALSE;
		}

		UINT rowStride = (metrics.gmBlackBoxX + 3) & ~3; // DWORD aligned
		UINT size = rowStride * metrics.gmBlackBoxY;


		cinfo.xOffset = 0;
		cinfo.yOffset = 0;
		cinfo.width = metrics.gmBlackBoxX;
		cinfo.height = metrics.gmBlackBoxY;
		cinfo.xOrigin = (char)metrics.gmptGlyphOrigin.x;
		cinfo.yOrigin = (char)metrics.gmptGlyphOrigin.y;
		cinfo.xIncrement = (BYTE)metrics.gmCellIncX;

		assert(cinfo.width < 256);
		assert(cinfo.height < 256);

#if SHAPE_FONT
		rowStride = ((rowStride-1)>>3) + 1;	// bit -> byte
		rowStride = (rowStride+3) & ~3;
		BYTE* pSrc = scratchPad;
		for(int y = 0; y < (int)cinfo.height; y++)
		{
			UINT x;
			for(x = 0; x < cinfo.width; x++)
			{           
				// ǿ��ȥ�������Ե����Ӱ.
				GlyphData.pPixels[y][x] = GetBit(pSrc + y * rowStride, x)? 255 : 0;
			}
		}
#if 0
		// for debug 
		{
			CHAR buf[MAX_PATH];
			int ret = WideCharToMultiByte(CP_UTF8,0,&ch, 1, buf, MAX_PATH, NULL, NULL);
			strcpy(buf + ret, ".raw");
			FILE* fp = fopen(buf,"wb+");
			if (fp)
			{
				fwrite(GlyphData.pPixels, 65536, 1, fp);
				fclose(fp);
			}
		}
#endif 
#else 
		for(UINT j = 0; j < size && j < sizeof(scratchPad); j++)
		{
			UINT pad = UINT(scratchPad[j]) << 2;
			if(pad > 255)
				pad = 255;
			scratchPad[j] = pad;
		}

		for(int y = 0; y < cinfo.height ; y++)
		{
			UINT x;
			for(x = 0; x < cinfo.width ; x++)
			{
				// [neo, 5/7/2007 - #3055] 
				// See comments above about scratchPad overrun
				int spi = y * rowStride + x;

				if( spi >= sizeof(scratchPad) )
				{
					return TRUE;
				}

				GlyphData.pPixels[y][x] = scratchPad[spi];
			}
		}
#endif 
		return TRUE;
	}
	else
	{
		SIZE size;
		GetTextExtentPoint32W(sm_hFontDC, &ch, 1, &size);
		if(size.cx)
		{
			cinfo.xIncrement = (BYTE)size.cx;
			cinfo.bitmapIndex = 0;
		}
		return FALSE;
	}
	return FALSE;
}

//CharInfo &UWin32FontInterface::GetCharDesc(const CHAR *str) const
//{
//    return GetCharDesc(oneUTF8toUTF32(str));
//}
