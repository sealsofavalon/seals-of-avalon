// stdafx.h : 标准系统包含文件的包含文件，
// 或是常用但不常更改的项目特定的包含文件
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// 从 Windows 头中排除极少使用的资料
// Windows 头文件:
#include <windows.h>
#include <assert.h>
#include <hash_map>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include "UPrivate.h"
//#include "ILInput.h"
#include <MMSystem.h>
#include <D3D9.h>
#include <d3dx9.h>
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "winmm.lib")
// TODO: 在此处引用程序要求的附加头文件
#include "UMemory.h"

#define PACK_IMPORT
#include "..\\..\\..\\PackTool\\PackLib\\PackInterface.h"


#pragma warning(disable: 4018)
#pragma warning(disable: 4244)