#include "NiMilesAudioPCH.h"
#include "mss.h"

#define MSS_FUNC(Return, Name, Args,Entry) Return (WINAPI *Name)Args;
#include "MSS_Func.inl"
#undef MSS_FUNC

static HMODULE g_hMSS32 = NULL;

static struct AutoClear
{
	AutoClear()
	{
		g_hMSS32 = NULL;
	}
	~AutoClear()
	{
		if (g_hMSS32)
		{
		//	CloseHandle(g_hMSS32);
		}
	}
} _AutoClearMSSHandle;

BOOL AttachMSSFunction(void *&Func , const char *FunName)
{
	Func = (void*)(GetProcAddress( g_hMSS32, FunName ));
	if( Func == NULL )
	{
		char ErrorBuffer[256];
		NiSprintf(ErrorBuffer,256,"mss32.dll ȱ�ٺ���%s",FunName);
		MessageBox(NULL,ErrorBuffer,"Audio System",MB_OK);
		return FALSE;
	}
	return TRUE;
}

BOOL AttachMSSFunctions()
{
	BOOL result = TRUE;

	char Buf[256];

#define MSS_FUNC(Return, Name, Args,Entry)	\
	NiSprintf(Buf,256,"_%s@%d",#Name,Entry);		\
	result = AttachMSSFunction(*(void**)&Name, Buf);	\
				if(!result)				\
					return FALSE;		\
			
#include "MSS_Func.inl"
#undef  MSS_FUNC

	return result;
}

BOOL ImportMSS()
{
	if (g_hMSS32==NULL)
	{
		if((g_hMSS32 = LoadLibrary("mss32.dll")) == 0)
		{
			char ErrorBuffer[256];
			NiSprintf(ErrorBuffer,256,"�Ҳ���mss32.dll");
			MessageBox(NULL,ErrorBuffer,"Audio System",MB_OK);
			return FALSE;
		}
	} else
	{
		return TRUE;
	}
   
	assert(g_hMSS32);

	return AttachMSSFunctions();
}