#ifndef __SYMSS_H__
#define __SYMSS_H__

#ifndef FAR
#define FAR
#endif 

#ifndef C8
#define C8 char
#endif

#ifndef U8
#define U8 unsigned char
#endif

#ifndef S8
#define S8 signed char
#endif

#ifndef U16
#define U16 unsigned short
#endif

#ifndef S16
#define S16 signed short
#endif

#ifndef U32
#define U32 unsigned long
#endif

#ifndef S32
#define S32 signed long
#endif

#ifndef F32
#define F32 float
#endif

#ifndef F64
#define F64 double
#endif


#ifndef REALFAR
#define REALFAR unsigned long
#endif

#ifndef FILE_ERRS
#define FILE_ERRS

#define AIL_NO_ERROR        0
#define AIL_IO_ERROR        1
#define AIL_OUT_OF_MEMORY   2
#define AIL_FILE_NOT_FOUND  3
#define AIL_CANT_WRITE_FILE 4
#define AIL_CANT_READ_FILE  5
#define AIL_DISK_FULL       6

#endif

#define MIN_VAL 0
#define NOM_VAL 1
#define MAX_VAL 2

#ifndef YES
#define YES 1
#endif

#ifndef NO
#define NO  0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE  0
#endif

//  [12/4/2007 Administrator]
#define AILFILETYPE_UNKNOWN     0
#define AILFILETYPE_PCM_WAV     1
#define AILFILETYPE_ADPCM_WAV   2
#define AILFILETYPE_OTHER_WAV   3
#define AILFILETYPE_VOC         4
#define AILFILETYPE_MIDI        5
#define AILFILETYPE_XMIDI       6
#define AILFILETYPE_XMIDI_DLS   7
#define AILFILETYPE_XMIDI_MLS   8
#define AILFILETYPE_DLS         9
#define AILFILETYPE_MLS        10
#define AILFILETYPE_MPEG_L1_AUDIO 11
#define AILFILETYPE_MPEG_L2_AUDIO 12
#define AILFILETYPE_MPEG_L3_AUDIO 13
#define AILFILETYPE_OTHER_ASI_WAV 14


#define AIL_LENGTHY_INIT           0
#define AIL_LENGTHY_SET_PREFERENCE 1
#define AIL_LENGTHY_UPDATE         2
#define AIL_LENGTHY_DONE           3



#define SMP_FREE          0x0001    // Sample is available for allocation

#define SMP_DONE          0x0002    // Sample has finished playing, or has
                                    // never been started

#define SMP_PLAYING       0x0004    // Sample is playing

#define SMP_STOPPED       0x0008    // Sample has been stopped

#define SMP_PLAYINGBUTRELEASED 0x0010 // Sample is playing, but digital handle
                                      // has been temporarily released

typedef S32 (__cdecl* AILLENGTHYCB)(U32 state,U32 user);
typedef S32 (__cdecl* AILCODECSETPREF)(char const* preference,U32 value);

//  [12/4/2007 Administrator]
typedef struct _STREAM FAR* HSTREAM;				// Handle to stream
typedef struct _DIG_DRIVER FAR * HDIGDRIVER;		// Handle to digital driver
typedef struct _MDI_DRIVER FAR * HMDIDRIVER;		// Handle to XMIDI driver
typedef struct _SAMPLE FAR * HSAMPLE;				// Handle to sample
typedef struct _SEQUENCE FAR * HSEQUENCE;			// Handle to sequence
typedef S32 HTIMER;                             // Handle to timer
typedef struct _DLSDEVICE FAR* HDLSDEVICE;
typedef U32 HPROVIDER;
typedef struct _AILSOUNDINFO {
  S32 format;
  void const FAR* data_ptr;
  U32 data_len;
  U32 rate;
  S32 bits;
  S32 channels;
  U32 samples;
  U32 block_size;
  void const FAR* initial_ptr;
} AILSOUNDINFO;

typedef enum
{
   DP_ASI_DECODER=0,   // Must be "ASI codec stream" provider
   SP_OUTPUT =0,		// 可能有问题...
   DP_FILTER,        // Must be "MSS pipeline filter" provider
   DP_MERGE,         // Must be "MSS mixer" provider
   N_SAMPLE_STAGES,  // Placeholder for end of list (= # of valid stages)
   SAMPLE_ALL_STAGES // Used to signify all pipeline stages, for shutdown
}
SAMPLESTAGE;

typedef enum
{
   DP_FLUSH = 0,     // Must be "MSS mixer" provider
   DP_DEFAULT_FILTER, // Must be "MSS pipeline filter" provider (sets the default)
   DP_DEFAULT_MERGE,  // Must be "MSS mixer" provider (sets the default)
   DP_COPY,          // Must be "MSS mixer" provider
   N_DIGDRV_STAGES,  // Placeholder for end of list (= # of valid stages)
   DIGDRV_ALL_STAGES // Used to signify all pipeline stages, for shutdown
}
DIGDRVSTAGE;

// 需要调整！！！！
#define MSS_MC_USE_SYSTEM_CONFIG 0

#define MSS_FUNC(Return, Name, Args,Entry) extern Return (WINAPI *Name)Args;
//#define MSS_FUNC(Return, Name, Args,Entry) Return Name##Args;
#include "MSS_Func.inl"
#undef MSS_FUNC

BOOL ImportMSS();
#endif 