#include "StormLib.h"

//����ͷ���� ��ֹ�ƽ�

DWORD ConvertDWord(DWORD dwValue)
{
	dwValue ^= 0xFFFFFFFF;

	char* v1 = (char*)&dwValue;
	char v2[4];

	v2[0] = v1[2];
	v2[2] = v1[0];

	v2[1] = v1[3];
	v2[3] = v1[1];

	return *((DWORD*)v2);
}

TMPQHeader2 ConvertHeader(const TMPQHeader2& Header)
{
	TMPQHeader2 Ret;
	Ret.dwID = Header.dwID;
	Ret.dwArchiveSize = ConvertDWord(Header.dwArchiveSize);

	//
	Ret.dwBlockSize = ConvertDWord(Header.dwBlockTablePos);
	Ret.dwBlockTablePos = ConvertDWord(Header.dwBlockSize);
	//
	
	Ret.dwBlockTableSize = ConvertDWord(Header.dwBlockTableSize);
	
	//
	Ret.dwFormatVersion = ConvertDWord(Header.dwHashTablePos);
	Ret.dwHashTablePos = ConvertDWord(Header.dwFormatVersion);
	//

	//
	Ret.dwHashTableSize = ConvertDWord(Header.dwHashTableSize);
	Ret.dwHeaderSize = ConvertDWord(Header.dwHeaderSize);
	//

	Ret.ExtBlockTablePos.HighPart = ConvertDWord(Header.ExtBlockTablePos.LowPart);
	Ret.ExtBlockTablePos.LowPart = ConvertDWord(Header.ExtBlockTablePos.HighPart);

	Ret.dwBlockTablePosHigh = Header.dwBlockTablePosHigh;

	//
	Ret.dwHashTablePosHigh = ConvertDWord(Header.dwHashTablePosHigh);

	return Ret;
}

inline void ConvertBuffer(void* buf1, void* buf2)
{
	DWORD dwValue1 = *(DWORD*) buf1;
	DWORD dwValue2 = *(DWORD*) buf2;

	*(DWORD*) buf2 = ConvertDWord(dwValue1);
	*(DWORD*) buf1 = ConvertDWord(dwValue2);
}

void ConvertData(char* buf, DWORD len)
{
	if(len >= 32)
		ConvertBuffer(buf + 12, buf + len - 16);

	if(len >= 24)
		ConvertBuffer(buf + 8, buf + len - 12);

	if(len >= 16)
		ConvertBuffer(buf + 4, buf + len - 8);

	if(len >= 8)
		ConvertBuffer(buf, buf + len - 4);
}