
#define __STORMLIB_SELF__
#include "StormLib.h"
#include "SCommon.h"
#include "FilePos.h"

void CFilePosArray::AddPos(const LONGLONG& pos, const LONGLONG& size)
{
	m_PosArray.push_back(FilePos(pos, size));
}

void CFilePosArray::Sort()
{
	std::sort(m_PosArray.begin(), m_PosArray.end());
	
	assert(m_PosArray.size() > 0);
	assert(m_PosArray[0].pos == 0);

	for(size_t i = 0; i + 1 < m_PosArray.size(); ++i)
	{
		assert(m_PosArray[i].pos >= 0);
		assert(m_PosArray[i].size >= 0);
		assert(m_PosArray[i].pos +  m_PosArray[i].size <= m_PosArray[i + 1].pos);
	}
}

LONGLONG CFilePosArray::FindBestPos(const LONGLONG& size)
{
	LONGLONG diff;
	LONGLONG min_diff;

	LONGLONG pos;
	assert(m_PosArray.size() > 0);
	size_t idx = m_PosArray.size() - 1;

	for(size_t i = 0; i + 1 < m_PosArray.size(); ++i)
	{
		pos = m_PosArray[i].pos + m_PosArray[i].size + size;
		if(m_PosArray[i + 1].pos >= pos)
		{
			diff = m_PosArray[i + 1].pos - pos;
			if(idx != m_PosArray.size() - 1)
			{
				if(diff < min_diff)
				{
					min_diff = diff;
					idx = i;
				}
			}
			else
			{
				min_diff = diff;
				idx = i;
			}
		}
	}

	return m_PosArray[idx].pos + m_PosArray[idx].size;
}

LONGLONG CFilePosArray::GetEndPos() const
{
	assert(m_PosArray.size() > 0);
	return m_PosArray[m_PosArray.size() - 1].pos + m_PosArray[m_PosArray.size() - 1].size;
}