#ifndef __FILE_POS_H__
#define __FILE_POS_H__

#include <vector>
#include <algorithm>

struct FilePos
{
	LONGLONG pos;
	LONGLONG size;

	FilePos(LONGLONG pos, LONGLONG size) 
	{
		this->pos = pos;
		this->size = size;
	}

	bool operator<(const FilePos& other)
	{
		if( pos < other.pos )
            return true;

        if( pos > other.pos )
            return false;

        return size < other.size; //0 字节的文件会共用同一个Pos
	}
};

class CFilePosArray
{
public:
	void AddPos(const LONGLONG& pos, const LONGLONG& size);
	void Sort();

	LONGLONG FindBestPos(const LONGLONG& size);
	LONGLONG GetEndPos() const;

private:
	std::vector<FilePos> m_PosArray;
};

#endif //__FILE_POS_H__