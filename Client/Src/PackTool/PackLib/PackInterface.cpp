
#include "PackInterface.h"
#include "SCommon.h"

void MakeStandardName(const char* src, char* dest)
{
	unsigned int i = 0;
	unsigned int j = 0;
	bool bSlash = false; //连续的斜杠只保留一个
	for(; src[i] != '\0'; ++i)
	{
		if(src[i] == '\\' || src[i] == '/')
		{
			if(!bSlash)
			{
				dest[j++] = '\\';
				bSlash = true;
			}
			continue;
		}

		bSlash = false;

		//转换成大写
		if(src[i] >= 'a' && src[i] <= 'z')
			dest[j++] = src[i] + 'A' - 'a';
		else
			dest[j++] = src[i];
	}

	dest[j] = '\0';
}

HANDLE CPackInterface::Open(const char* pFileName, DWORD dwCreationDisposition, DWORD dwHashTableSize)
{
	HANDLE hArchive;
	SFileCreateArchiveEx(pFileName, dwCreationDisposition, dwHashTableSize, &hArchive);
	return hArchive;
}

void CPackInterface::Close(HANDLE hArchive)
{
	SFileCloseArchive(hArchive);
}

HANDLE CPackInterface::OpenFile(HANDLE hArchive, const char* pFileName, DWORD dwSearchScope)
{
	HANDLE hFile;
	SFileOpenFileEx(hArchive, pFileName, dwSearchScope, &hFile);
	return hFile;
}

DWORD CPackInterface::ReadFile(HANDLE hFile, VOID * lpBuffer, DWORD dwToRead)
{
	DWORD dwRead;
	if(SFileReadFile(hFile, lpBuffer, dwToRead, &dwRead, NULL))
		return dwRead;

	return 0;
}

void CPackInterface::CloseFile(HANDLE hFile)
{
	SFileCloseFile(hFile);
}

DWORD CPackInterface::GetFileSize(HANDLE hFile)
{
	return SFileGetFileSize(hFile, NULL);
}

DWORD CPackInterface::GetFilePosition(HANDLE hFile)
{
	return SFileGetFilePos(hFile, NULL);
}

DWORD CPackInterface::SetFilePointer(HANDLE hFile, LONG lFilePos, DWORD dwMethod)
{
	return SFileSetFilePointer(hFile, lFilePos, NULL, dwMethod);
}

bool CPackInterface::HasFile(HANDLE hArchive, const char* pFileName)
{
	return SFileHasFile(hArchive, pFileName) ? true : false;
}

HANDLE CPackInterface::FindFirstFile(HANDLE hArchive, const char* pMask, SFILE_FIND_DATA* lpFindFileData)
{
	return SListFileFindFirstFile(hArchive, NULL, pMask, lpFindFileData);
}

BOOL CPackInterface::FindNextFile(HANDLE hFind, SFILE_FIND_DATA* lpFindFileData)
{
	return SListFileFindNextFile(hFind, lpFindFileData);
}

void CPackInterface::FindClose(HANDLE hFind)
{
	SListFileFindClose(hFind);
}

void CPackInterface::ExtractFile(HANDLE hArchive, const char * pToExtract, const char * pExtracted)
{
	SFileExtractFile(hArchive, pToExtract, pExtracted);
}

bool CPackInterface::AddFile(HANDLE hArchive, const char* pFileName, const char* pArchivedName)
{
	return SFileAddFile(hArchive, pFileName, pArchivedName, MPQ_FILE_COMPRESS|MPQ_FILE_REPLACEEXISTING) ? true : false;
}

void CPackInterface::RemoveFile(HANDLE hArchive, const char* pFileName)
{
	SFileRemoveFile(hArchive, pFileName, 0);
}

DWORD CPackInterface::GetFileInfo(HANDLE hFile, DWORD dwInfoType)
{
	return SFileGetFileInfo(hFile, dwInfoType);
}

DWORD CPackInterface::CopyFile(HANDLE hDest, HANDLE hSrc, const char * szArchivedName)
{
    return SFileCopyFile((TMPQArchive*)hDest, (TMPQArchive*)hSrc, szArchivedName);
}

#define MAX_ARCHIVE_COUNT 16

#define THREAD __declspec(thread)
THREAD static HANDLE s_hArchives[MAX_ARCHIVE_COUNT];
THREAD static unsigned int s_uiArchCount = 0;

THREAD static HANDLE audio_hArchives[16];
THREAD static unsigned int audio_uiArchCount = 0;

bool CPackManager::Init()
{
	if(s_uiArchCount != 0)
		return true;

	memset(s_hArchives, 0, sizeof(s_hArchives));

	//Patch
	char aPatchName[256];
    HANDLE hArchive;
	for(int i = 9; i >= 0; --i)
	{
		sprintf(aPatchName, "Data\\Patch%d.sypak", i);
        if( SFileOpenArchiveEx(aPatchName, 0, 0, &hArchive, GENERIC_READ) )
        {
            s_hArchives[s_uiArchCount] = hArchive;
            ++s_uiArchCount;
        }
	}

    if( SFileOpenArchiveEx("Data\\Data.sypak", 0, 0, &hArchive, GENERIC_READ) )
    {
        s_hArchives[s_uiArchCount] = hArchive;
        ++s_uiArchCount;
    }

	return true;
}

void CPackManager::Shutdown()
{
	for(unsigned int i = 0; i < s_uiArchCount; ++i)
	{
        SFileCloseArchive(s_hArchives[i]);
	}

    s_uiArchCount = 0;
}

HANDLE CPackManager::OpenFile(const char* pFileName)
{
    HANDLE hFile;

    char filename[1024];
    MakeStandardName(pFileName, filename);

	//先从打包文件里面打开
	for(unsigned int i = 0; i < s_uiArchCount; ++i)
	{
		hFile = g_pPackInterface->OpenFile(s_hArchives[i], filename, SFILE_OPEN_FROM_MPQ);
		if(hFile)
			return hFile;
	}

    //再尝试打开本地文件
    hFile = g_pPackInterface->OpenFile(NULL, filename, SFILE_OPEN_LOCAL_FILE);
    if(hFile != NULL)
        return hFile;

	return NULL;
}

bool CPackManager::HasFile(const char* pFileName)
{
    char filename[1024];
    MakeStandardName(pFileName, filename);

	//先查找打包文件
	for(unsigned int i = 0; i < s_uiArchCount; ++i)
	{
		if(g_pPackInterface->HasFile(s_hArchives[i], filename))
			return true;
	}

    //再尝试查找本地文件
    if(GetFileAttributes(filename) != INVALID_FILE_ATTRIBUTES)
        return true;

	return false;
}



bool CPackAudioManager::Init()
{
	if(audio_uiArchCount != 0)
		return true;

	memset(audio_hArchives, 0, sizeof(audio_hArchives));

	//Patch
	char aPatchName[256];
	HANDLE hArchive;
	for(int i = 9; i >= 0; --i)
	{
		sprintf(aPatchName, "Data\\Patch%d.sypak", i);
		if( SFileOpenArchiveEx(aPatchName, 0, 0, &hArchive, GENERIC_READ) )
		{
			audio_hArchives[audio_uiArchCount] = hArchive;
			++audio_uiArchCount;
		}
	}

	if( SFileOpenArchiveEx("Data\\Data.sypak", 0, 0, &hArchive, GENERIC_READ) )
	{
		audio_hArchives[audio_uiArchCount] = hArchive;
		++audio_uiArchCount;
	}

	return true;
}

void CPackAudioManager::Shutdown()
{
	for(unsigned int i = 0; i < audio_uiArchCount; ++i)
	{
		SFileCloseArchive(audio_hArchives[i]);
	}

	audio_uiArchCount = 0;
}



HANDLE CPackAudioManager::OpenFile(const char* pFileName)
{
	HANDLE hFile;

	char filename[1024];
	MakeStandardName(pFileName, filename);

	//先从打包文件里面打开
	for(unsigned int i = 0; i < audio_uiArchCount; ++i)
	{
		hFile = g_pPackInterface->OpenFile(audio_hArchives[i], filename, SFILE_OPEN_FROM_MPQ);
		if(hFile)
			return hFile;
	}

	//再尝试打开本地文件
	hFile = g_pPackInterface->OpenFile(NULL, filename, SFILE_OPEN_LOCAL_FILE);
	if(hFile != NULL)
		return hFile;

	return NULL;
}

bool CPackAudioManager::HasFile(const char* pFileName)
{
	char filename[1024];
	MakeStandardName(pFileName, filename);

	//先查找打包文件
	for(unsigned int i = 0; i < audio_uiArchCount; ++i)
	{
		if(g_pPackInterface->HasFile(audio_hArchives[i], filename))
			return true;
	}

	//再尝试查找本地文件
	if(GetFileAttributes(filename) != INVALID_FILE_ATTRIBUTES)
		return true;

	return false;
}

CPackInterface g_kPackInterface;
CPackInterface* g_pPackInterface = &g_kPackInterface;

CPackManager g_kPackManager;
CPackManager* g_pPackManager = &g_kPackManager;

CPackAudioManager g_kPackAudioManager;
CPackAudioManager* g_pPackAudioManager = &g_kPackAudioManager;