#ifndef PACK_INTERFACE_H
#define PACK_INTERFACE_H

#include "StormLib.h"

#ifdef PACK_EXPORT
    // DLL library project uses this
    #define PACK_ENTRY extern "C" __declspec(dllexport)
#else

#ifdef PACK_IMPORT

    #define PACK_ENTRY extern "C" __declspec(dllimport)

#else
    // static library project uses this
    #define PACK_ENTRY extern "C"
#endif

#endif

class CPackInterface
{
public:
	virtual HANDLE Open(const char* pFileName, DWORD dwCreationDisposition, DWORD dwHashTableSize);
	virtual void Close(HANDLE hArchive);

	virtual HANDLE OpenFile(HANDLE hArchive, const char* pFileName, DWORD dwSearchScope);
	virtual DWORD ReadFile(HANDLE hFile, VOID * lpBuffer, DWORD dwToRead);
	virtual void CloseFile(HANDLE hFile);
	virtual DWORD GetFileSize(HANDLE hFile);
	virtual DWORD GetFilePosition(HANDLE hFile);
	virtual DWORD SetFilePointer(HANDLE hFile, LONG lFilePos, DWORD dwMethod);
	virtual bool HasFile(HANDLE hArchive, const char* pFileName);
	virtual HANDLE FindFirstFile(HANDLE hArchive, const char* pMask, SFILE_FIND_DATA* lpFindFileData);
	virtual BOOL FindNextFile(HANDLE hFind, SFILE_FIND_DATA* lpFindFileData);
	virtual void FindClose(HANDLE hFind);
	virtual void ExtractFile(HANDLE hArchive, const char * pToExtract, const char * pExtracted);
	virtual bool AddFile(HANDLE hArchive, const char* pFileName, const char* pArchivedName);
	virtual void RemoveFile(HANDLE hArchive, const char* pFileName);
	virtual DWORD GetFileInfo(HANDLE hFile, DWORD dwInfoType);

    //在两个Archive 之间Copy
    virtual DWORD CopyFile(HANDLE hDest, HANDLE hSrc, const char * szArchivedName);
};

class CPackManager
{
public:
	virtual bool Init();
	virtual void Shutdown();
	virtual HANDLE OpenFile(const char* pFileName);
	virtual bool HasFile(const char* pFileName);
};

class CPackAudioManager
{
public:
	virtual bool Init();
	virtual void Shutdown();
	virtual HANDLE OpenFile(const char* pFileName);
	virtual bool HasFile(const char* pFileName);
};

PACK_ENTRY CPackManager* g_pPackManager;
PACK_ENTRY CPackAudioManager* g_pPackAudioManager;
PACK_ENTRY CPackInterface* g_pPackInterface;

class CFileStream
{
public:
	CFileStream()
		:m_hFile(NULL)
	{
	}

	CFileStream(const char* pFileName)
	{
		Open(pFileName);
	}

	~CFileStream()
	{
		Close();
	}

	bool Open(const char* pFileName)
	{
		m_hFile = g_pPackManager->OpenFile(pFileName);
		return m_hFile != NULL;
	}

	void Close()
	{
		if(m_hFile)
		{
			g_pPackInterface->CloseFile(m_hFile);
			m_hFile = NULL;
		}
	}

	unsigned int Read(void* pBuffer, unsigned int uiLen)
	{
		assert(m_hFile != NULL);
		return g_pPackInterface->ReadFile(m_hFile, pBuffer, uiLen);
	}

	void Seek(int iOffset, int iOrigin = FILE_CURRENT)
	{
		assert(m_hFile != NULL);
		g_pPackInterface->SetFilePointer(m_hFile, iOffset, iOrigin);
	}

	unsigned int GetSize()
	{
		assert(m_hFile != NULL);
		return g_pPackInterface->GetFileSize(m_hFile);
	}

	unsigned int GetPosition()
	{
		assert(m_hFile != NULL);
		return g_pPackInterface->GetFilePosition(m_hFile);
	}

	template <class T>
	unsigned int Read(T* pBuffer)
	{
		return Read(pBuffer, sizeof(T));
	}

private:
	HANDLE m_hFile;
};

#endif //PACK_INTERFACE_H