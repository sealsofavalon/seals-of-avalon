/*****************************************************************************/
/* SFileExtractFile.cpp                   Copyright (c) Ladislav Zezula 2003 */
/*---------------------------------------------------------------------------*/
/* Simple extracting utility                                                 */
/*---------------------------------------------------------------------------*/
/*   Date    Ver   Who  Comment                                              */
/* --------  ----  ---  -------                                              */
/* 20.06.03  1.00  Lad  The first version of SFileExtractFile.cpp            */
/*****************************************************************************/

#define __STORMLIB_SELF__
#include "StormLib.h"
#include "SCommon.h"

BOOL WINAPI SFileExtractFile(HANDLE hMpq, const char * szToExtract, const char * szExtracted)
{
    HANDLE hLocalFile = INVALID_HANDLE_VALUE;
    HANDLE hMpqFile = NULL;
    DWORD dwSearchScope = 0;
    int nError = ERROR_SUCCESS;


    // Open the MPQ file
    if(nError == ERROR_SUCCESS)
    {
        if((DWORD_PTR)szToExtract <= 0x10000)
            dwSearchScope = SFILE_OPEN_BY_INDEX;
        if(!SFileOpenFileEx(hMpq, szToExtract, dwSearchScope, &hMpqFile))
            nError = GetLastError();
    }

    // Create the local file
    if(nError == ERROR_SUCCESS)
    {
        hLocalFile = CreateFile(szExtracted, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, 0, NULL);
        if(hLocalFile == INVALID_HANDLE_VALUE)
            nError = GetLastError();
    }

    // Copy the file's content
    if(nError == ERROR_SUCCESS)
    {
        char  szBuffer[64*1024];
        DWORD dwTransferred;

        for(;;)
        {
            // dwTransferred is only set to nonzero if something has been read.
            // nError can be ERROR_SUCCESS or ERROR_HANDLE_EOF
            if(!SFileReadFile(hMpqFile, szBuffer, sizeof(szBuffer), &dwTransferred, NULL))
                nError = GetLastError();
            if(nError == ERROR_HANDLE_EOF)
                nError = ERROR_SUCCESS;
            if(dwTransferred == 0)
                break;

            // If something has been actually read, write it
            WriteFile(hLocalFile, szBuffer, dwTransferred, &dwTransferred, NULL);
            if(dwTransferred == 0)
                nError = ERROR_DISK_FULL;
        }
    }

    if(nError == ERROR_SUCCESS)
    {
        FILETIME ftWrite;
        SYSTEMTIME stUTC, stLocal;
        
        DWORD dwFileTime = SFileGetFileInfo(hMpqFile, SFILE_INFO_FILE_TIME);

        stLocal.wYear = dwFileTime >> 25;
        dwFileTime -= stLocal.wYear << 25;
        stLocal.wYear += 1980;

        stLocal.wMonth = dwFileTime >> 21;
        dwFileTime -= stLocal.wMonth << 21;

        stLocal.wDay = dwFileTime >> 16;
        dwFileTime -= stLocal.wDay << 16;

        stLocal.wHour = (WORD)(dwFileTime >> 11);
        dwFileTime -= stLocal.wHour << 11;

        stLocal.wMinute = (WORD)(dwFileTime >> 5);
        dwFileTime -= stLocal.wMinute << 5;

        stLocal.wSecond = (WORD)(dwFileTime << 1);

        TzSpecificLocalTimeToSystemTime(NULL, &stLocal, &stUTC);
        SystemTimeToFileTime(&stUTC, &ftWrite);

        SetFileTime(hLocalFile, NULL, NULL, &ftWrite);
    }

    // Close the files
    if(hMpqFile != NULL)
        SFileCloseFile(hMpqFile);
    if(hLocalFile != INVALID_HANDLE_VALUE)
        CloseHandle(hLocalFile);
    if(nError != ERROR_SUCCESS)
        SetLastError(nError);
    return (BOOL)(nError == ERROR_SUCCESS);
}
