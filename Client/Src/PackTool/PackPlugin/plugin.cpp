
#include <windows.h>
#include <stdio.h>
#include <direct.h>
#include <sys/types.h>
#include <sys/utime.h>
#include <time.h>

#include <vector>
#include <string>
#include <algorithm>   
#include <functional>

#include "wcxhead.h"
#include "../PackLib/PackInterface.h"

static tProcessDataProc s_pfnProcessDataProc = NULL;

#define BUF_SIZE 128*1024 // 128 KB buffer
static char data[BUF_SIZE];

struct ArcData
{
	HANDLE hArchive;
	HANDLE hFind;
	SFILE_FIND_DATA FindFileData;
};

void CreatePath(char *path)
{
	char *ofs;

	if(!path) 
		return;

	for(ofs=path+1; *ofs; ofs++)
	{
		if(*ofs=='\\' || *ofs=='/')
		{
			// create the directory
			*ofs=0;
			_mkdir(path);
			*ofs='\\';
		}
	}
}

int IsDir(const char *name)
{
	if(!name || !name[0]) 
		return 0;

	while(name[1]) 
		name++;

	if(name[0]=='\\')
		return 1;
	else
		return 0;
}

void Unix2WinName(char *name)
{
	if(!name) 
		return;

	while(*name)
	{
		if(name[0]=='/') 
			name[0]='\\';

		name++;
	}
}

void Win2UnixName(char *name)
{
	if(!name) 
		return;

	while(*name)
	{
		if(name[0]=='\\') 
			name[0]='/';

		name++;
	}
}

// get file name from full name
const char *GetName(const char *name)
{
	const char *n=name;

	if(!name) 
		return NULL;

	while(*name)
	{
		if(name[0]=='\\')
			n=name+1;

		name++;
	}

	return n;
}

void PrepareName(char *out, const char *path, const char *name, int sp)
{
	char n[MAX_PATH], *p;

	if(path)
		sprintf(n, "%s\\%s", path, sp ? name:GetName(name));
	else
		strcpy(n, sp ? name : GetName(name));

	Unix2WinName(n);

	// remove leading slash
	p= (*n=='\\') ? n + 1 : n;
	strcpy(out, p);
}


// ------------------------- * !DLL Exports! * -------------------------

/*
** Plugin Interface!
*/

// OpenArchive should perform all necessary operations when an archive is to be opened
HANDLE __stdcall OpenArchive(tOpenArchiveData *ArchiveData)
{
	HANDLE hArchive = g_pPackInterface->Open(ArchiveData->ArcName, OPEN_EXISTING, 0);
	if(hArchive)
	{
		ArcData* hArcData = new ArcData;
		memset(hArcData, 0, sizeof(ArcData));

		hArcData->hArchive = hArchive;
		ArchiveData->OpenResult = 0;

		return hArcData;
	}

	ArchiveData->OpenResult = E_BAD_ARCHIVE;
	return 0;
}

// CloseArchive should perform all necessary operations when an archive is about to be closed
int __stdcall CloseArchive(HANDLE *hArcData)
{
	ArcData* pArcData = (ArcData*)hArcData;
	if(pArcData->hFind)
	{
		g_pPackInterface->FindClose(pArcData->hFind);
		pArcData->hFind = NULL;
	}

	g_pPackInterface->Close(pArcData->hArchive);
	pArcData->hArchive = NULL;

	delete pArcData;

	return 0;
}


// WinCmd calls ReadHeader to find out what files are in the archive
int __stdcall ReadHeader(HANDLE hArcData, tHeaderData *HeaderData)
{
	ArcData* pArcData = (ArcData*)hArcData;

	if(pArcData->hFind == NULL)
	{
		pArcData->hFind = g_pPackInterface->FindFirstFile(pArcData->hArchive, "*", &pArcData->FindFileData);
		if(pArcData->hFind == NULL)
			return E_END_ARCHIVE;
	}
	else
	{
		if(!g_pPackInterface->FindNextFile(pArcData->hFind, &pArcData->FindFileData))
		{
			g_pPackInterface->FindClose(pArcData->hFind);
			pArcData->hFind = NULL;
			return E_END_ARCHIVE;
		}
	}

	Unix2WinName(pArcData->FindFileData.cFileName);

	strcpy(HeaderData->ArcName, ((TMPQArchive*)pArcData->hArchive)->szFileName);
	strcpy(HeaderData->FileName, pArcData->FindFileData.cFileName);
	HeaderData->FileCRC = 0;

	HANDLE hFile = g_pPackInterface->OpenFile(pArcData->hArchive, pArcData->FindFileData.cFileName, SFILE_OPEN_FROM_MPQ);

	HeaderData->UnpSize = g_pPackInterface->GetFileInfo(hFile, SFILE_INFO_FILE_SIZE);
	HeaderData->PackSize = g_pPackInterface->GetFileInfo(hFile, SFILE_INFO_COMPRESSED_SIZE);
	HeaderData->FileTime = g_pPackInterface->GetFileInfo(hFile, SFILE_INFO_FILE_TIME);
	HeaderData->FileAttr = 0x21; //Read-only file, Archive file

	g_pPackInterface->CloseFile(hFile);
	if(s_pfnProcessDataProc(HeaderData->FileName, 0) == 0)
		return E_EABORTED;

	return 0;
}

// ProcessFile should unpack the specified file or test the integrity of the archive
int __stdcall ProcessFile(HANDLE hArcData, int Operation, char *DestPath, char *DestName)
{
	if ( Operation != PK_EXTRACT )
		return 0;

	ArcData* pArcData = (ArcData*)hArcData;
	if(pArcData->hFind == NULL)
		return E_BAD_DATA;

	char n[MAX_PATH], *name;
	FILE *fp;

	if(DestPath==NULL)
		name=DestName;
	else
	{
		name=n;
		sprintf(name, "%s\\%s", DestPath, DestName);
	}

	CreatePath(name);

	fp=fopen(name, "wb");
	if(!fp) 
		return E_EWRITE;

	HANDLE hFile = g_pPackInterface->OpenFile(pArcData->hArchive, pArcData->FindFileData.cFileName, SFILE_OPEN_FROM_MPQ);
	if(hFile == NULL)
		return E_BAD_DATA;

	DWORD dwFileTime = g_pPackInterface->GetFileInfo(hFile, SFILE_INFO_FILE_TIME);
	DWORD dwSize = g_pPackInterface->GetFileInfo(hFile, SFILE_INFO_FILE_SIZE);
	DWORD dwToRead;
	DWORD dwRead;
	while(dwSize > 0)
	{
		dwToRead = (dwSize < BUF_SIZE) ? dwSize : BUF_SIZE;
		dwRead = g_pPackInterface->ReadFile(hFile, data, dwToRead);

		fwrite(data, dwRead, 1, fp);

		s_pfnProcessDataProc(name, dwRead);

		if(dwRead != dwToRead)
			break;

		dwSize -= dwRead;
	}

	g_pPackInterface->CloseFile(hFile);

	//设置文件时间
	struct tm tmm = {0};

	tmm.tm_year = dwFileTime >> 25;
	dwFileTime -= tmm.tm_year << 25;
	tmm.tm_year += (1980 - 1900);

	tmm.tm_mon = dwFileTime >> 21;
	dwFileTime -= tmm.tm_mon << 21;
	tmm.tm_mon -= 1;

	tmm.tm_mday = dwFileTime >> 16;
	dwFileTime -= tmm.tm_mday << 16;

	tmm.tm_hour = dwFileTime >> 11;
	dwFileTime -= tmm.tm_hour << 11;

	tmm.tm_min = dwFileTime >> 5;
	dwFileTime -= tmm.tm_min << 5;

	tmm.tm_sec = dwFileTime << 1;

	struct __utimbuf64 ut;
	ut.modtime = mktime(&tmm);
	ut.actime = ut.modtime;

	_futime64(fp->_file, &ut);

	fclose(fp);


	return 0;
}

// This function allows you to notify user about changing a volume when packing files
void __stdcall SetChangeVolProc(HANDLE hArcData, tChangeVolProc pChangeVolProc1)
{
	
}

// This function allows you to notify user about the progress when you un/pack files
void __stdcall SetProcessDataProc(HANDLE hArcData, tProcessDataProc pProcessDataProc)
{
	s_pfnProcessDataProc = pProcessDataProc;
}

// ------------------------- * optional * -------------------------

// GetPackerCaps tells WinCmd what features your packer plugin supports
int __stdcall GetPackerCaps(void)
{
	return PK_CAPS_NEW|PK_CAPS_MODIFY|PK_CAPS_MULTIPLE|PK_CAPS_DELETE/*|PK_CAPS_OPTIONS*/;
}

class StringCompare   
{   
public:  
    bool operator () (const char* a, const char* b) const  
    {
		return _stricmp(a, b) < 0;
    };
};   

std::vector<const char*> SortFiles(const char* files)
{
	std::vector<const char*> fileList;

	DWORD dwCount = 0;
	while(*files)
	{
		if(!IsDir(files))
		{
			fileList.push_back(files);
			dwCount++;
		}

		while(*files++);
	}

	std::sort(fileList.begin(), fileList.end(), StringCompare());
	return fileList;
}

// PackFiles specifies what should happen when a user creates, or adds files to the archive
int __stdcall PackFiles(char *PackedFile, char *SubPath, char *SrcPath, char *AddList, int Flags)
{
	std::vector<const char*>& fileList = SortFiles(AddList);
	DWORD dwCount = fileList.size();

	HANDLE hArchive = g_pPackInterface->Open(PackedFile, OPEN_ALWAYS, dwCount + 128); //留有128个文件的空位
	if(hArchive == NULL)
		return E_EOPEN;


	char SrcName[MAX_PATH];
	char DestName[MAX_PATH];

	int ret = 0;
	for(DWORD i = 0; i < dwCount; ++i)
	{
		PrepareName(DestName, SubPath, fileList[i], Flags&PK_PACK_SAVE_PATHS);
		if(DestName[0] == '\0')
			continue;

		if(SrcPath)
			sprintf(SrcName, "%s%s", SrcPath, fileList[i]);
		else
			strcpy(SrcName, fileList[i]);

		if(s_pfnProcessDataProc(SrcName, -int(float(i)/float(dwCount)*100.f) - 1) == 0)
		{
			ret = E_EABORTED;
			break;
		}

		if(!g_pPackInterface->AddFile(hArchive, SrcName, DestName))
		{
			ret = E_TOO_MANY_FILES;
			break;
		}
	}
	
	g_pPackInterface->Close(hArchive);
	return ret;
}

bool HasMask(const char* str)
{
	for( ; *str; ++str)
	{
		if(*str == '*' || *str == '?')
			return true;
	}

	return false;
}

// DeleteFiles should delete the specified files from the archive
int __stdcall DeleteFiles(char *PackedFile, char *DeleteList)
{
	HANDLE hArchive = g_pPackInterface->Open(PackedFile, OPEN_EXISTING, 0);
	if(hArchive == NULL)
		return E_EOPEN;

	HANDLE hFind;
	SFILE_FIND_DATA FindFileData;
	std::vector<std::string> files;
	while(*DeleteList)
	{
		if(HasMask(DeleteList))
		{
			hFind = g_pPackInterface->FindFirstFile(hArchive, DeleteList, &FindFileData);
			while(hFind)
			{
				files.push_back(FindFileData.cFileName);
				if(!g_pPackInterface->FindNextFile(hFind, &FindFileData))
				{
					g_pPackInterface->FindClose(hFind);
					hFind = NULL;
				}
			}
		}
		else
		{
			files.push_back(DeleteList);
		}

		while(*DeleteList++);
	}

	DWORD dwCount = files.size();
	DWORD dwCurrent = 0;
	int ret = 0;
	for(DWORD dwIndex = 0; dwIndex < dwCount; ++dwIndex)
	{
		if(s_pfnProcessDataProc((char*)files[dwIndex].c_str(), -int(float(dwIndex)/float(dwCount)*100.f)-1) == 0)
		{
			ret = E_EABORTED;
			break;
		}

		g_pPackInterface->RemoveFile(hArchive, files[dwIndex].c_str());
	}

	g_pPackInterface->Close(hArchive);
	return ret;
}

// ConfigurePacker gets called when the user clicks the Configure
// button from within "Pack files..." dialog box in WinCmd
void __stdcall ConfigurePacker(HWND Parent, HINSTANCE DllInstance)
{
}
