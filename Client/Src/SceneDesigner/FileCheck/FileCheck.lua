
dofile "Utils.lua"
dofile "FileMap.lua"
dofile "Stream.lua"
dofile "Map.lua"
dofile "Tile.lua"


lfs.chdir("..\\..")

--需要忽略的文件
RegisterIgnorePattern("%.svn$")
RegisterIgnorePattern("Thumbs%.db$")
RegisterIgnorePattern("%.settings$")
RegisterIgnorePattern("^Data\\Textures\\Water\\.+")

--所有的文件
RegisterDir("Data\\Effects")
RegisterDir("Data\\Models")
RegisterDir("Data\\Sky")
RegisterDir("Data\\Textures")
RegisterDir("Data\\World")

--检查所有的Map需要的文件
CheckMapFiles("Data\\World\\Maps")

WriteUnusedFiles("Data\\FileCheck\\Unused.txt")
WriteMissFiles("Data\\FileCheck\\Miss.txt")
