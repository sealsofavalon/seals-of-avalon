
FileMap = {}
MissFiles = {}
IgnorePatterns = {}

function MakeStandard(filename)
    filename = filename:upper() --转成大写
    filename = filename:gsub("[\\/]+", "\\") --多个斜杠反斜杠替换成单个斜杠
    
    return filename
end

function RegisterFile(filename)
    FileMap[filename] = 0
end

function RegisterUsedFile(filename, opt)
    filename = MakeStandard(filename)
    if FileMap[filename] == nil then
        if opt then --此文件是可选的
            return false
        end
        
        if MissFiles[filename] == nil then
           MissFiles[filename] = 0
        end
           
        MissFiles[filename] = MissFiles[filename] + 1
        return false
    end
    
    FileMap[filename] = FileMap[filename] + 1
    return true
end

function RegisterIgnorePattern(pattern)
    pattern = pattern:upper() --转成大写
    IgnorePatterns[#IgnorePatterns + 1] = pattern
end

--检查是否需要忽略
function CheckIgnore(filename)
    for i, pattern in ipairs(IgnorePatterns) do 
        if filename:find(pattern) then 
            return true
        end
    end
        
    return false
end

function RegisterDir (path)
	for file in lfs.dir(path) do
		if file ~= "." and file ~= ".." then
			local f = path.."\\"..file
			f = MakeStandard(f)
			if not CheckIgnore(f) then
			    local attr = lfs.attributes (f)
			    if attr.mode == "directory" then
				    RegisterDir (f)
			    else
			        RegisterFile(f)
			    end
			end
		end
	end
end

function array_iter (array, i)
    i = i + 1
    local v = array[i]
    if v then
       return i, v
    end
end

function SortTable(tab)
    local array = {}
    for k in pairs(tab) do
        array[#array + 1] = k
    end
    
    table.sort(array)
    
    return array_iter, array, 0
end

function WriteUnusedFiles(path_name)
    local f = assert(io.open(path_name, "wb"))
    if not f then
        return
    end
    
    f:setvbuf("full", 256*1024)
    
    for _, file in SortTable(FileMap) do
        if FileMap[file] == 0 then 
            f:write("del /f \"..\\..\\", file, "\"\r\n")
        end
    end
    
    f:close()
end

function WriteMissFiles(path_name)
    local f = assert(io.open(path_name, "wb"))
    if not f then
        return
    end
    
    f:setvbuf("full", 256*1024)
        
    for _, file in SortTable(MissFiles) do
        f:write(file, "\r\n")
    end

    f:close()
end