
function MakeVersion(major, minor)
    return major * 256 + minor
end

maps = {}
function FindMaps(path)
    for file in lfs.dir(path) do
		if file ~= "." and file ~= ".." then
			local f = path.."\\"..file
			f = MakeStandard(f)
			if not CheckIgnore(f) then
			    local attr = lfs.attributes (f)
			    if attr.mode == "directory" then
				    FindMaps (f)
			    else
			        --if f:find("^.+%.MAP$") then			            
			        if f:find("^.+\\(.+)\\%1%.MAP$") then
			            maps[#maps + 1] = f
			        end
			    end
			end
		end
	end
end

function CheckMapFiles(root_path)
    maps = {}
    FindMaps(root_path)
    
    for i = 1, #maps do
        LoadMap(maps[i])
    end
end

function LoadMap(map)
    print("Load "..map)
    
    if not RegisterUsedFile(map) then
        print("map "..map.." not found")
        return
    end
    
    local path, name = map:match("^(.+)\\(.+)%.MAP$")
    
    --light
    RegisterUsedFile(path.."\\"..name..".lit", true)
    --路点
    RegisterUsedFile(path.."\\"..name..".pth", true)
    --大地图
    RegisterUsedFile("Data\\World\\MiniMaps\\"..name..".jpg", true)
    
    local f = io.open(map, "rb")
    if not f then
        printf("open map %s error", map)
        return
    end
    
    local s
    local tag
    local size
    local tile_flags
    while true do
        s = CreateTagStream(f)
        
        tag = s:GetTag()
        if tag == 0 then
            break
        elseif tag == string_to_tag("MAIN") then
            tile_flags = s:GetData()
        elseif tag == string_to_tag("SKY") then
            RegisterUsedFile(s:ReadString())
        elseif tag == string_to_tag("GRTX") then
            RegisterUsedFile(s:ReadString())
        else
        end
    end
    
    f:close()
    
    local flags
    local tile_path_name
    local tile_minimap_name
    for i = 0, 63 do
        for j = 0, 63 do
            flags = tile_flags:sub((i*64 + j)*4 + 1, (i*64 + j + 1)*4)
            if string_to_value(flags) > 0 then
                --
                tile_path_name = string.format("%s\\%s_%d_%d.tle", path, name, i, j)
                RegisterUsedFile(tile_path_name, true)
                LoadTile(tile_path_name)
                
                
                --小地图
                tile_minimap_name = string.format("Data\\World\\Minimaps\\%s\\%s_%d_%d.jpg", name, name, i, j)
                RegisterUsedFile(tile_minimap_name, true)
            end
        end
    end
end