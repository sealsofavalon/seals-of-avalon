
StreamMetaTable = {}
StreamMetaTable.__index = StreamMetaTable

function string_to_value(str, len)
    str = str or ""
    len = len or #str
    
    local value = 0
    for i = 1, len do
        value = value * 256 + str:byte(-i)
    end
    
    return value
end

function string_to_tag(str)
    return string_to_value(str:reverse())
end

function CreateTagStream(s)
    stream = {}
    
    local size
    if s.ReadUInt then
        --是一个Stream
        stream.tag = s:ReadUInt()
        size = s:ReadUInt()
        stream.data = s:Read(size) or ""
    else
        --是一个文件
        stream.tag = string_to_value(s:read(4))
        size = string_to_value(s:read(4))
        stream.data = s:read(size) or ""
    end
    
    stream.pos = 1
    stream.size = #stream.data
    setmetatable(stream, StreamMetaTable)
    
    return stream
end

function CreateStream(s)
    stream = {}
    
    stream.pos = 1
    stream.data = s or ""
    stream.size = #s
    setmetatable(stream, StreamMetaTable)
    
    return stream
end

function StreamMetaTable:GetTag()
    return self.tag
end

function StreamMetaTable:GetSize()
    return self.size
end

function StreamMetaTable:GetData()
    return self.data
end

function StreamMetaTable:Seek(offset)
    local new_pos = self.pos + offset
    if new_pos > self.size then
        self.pos = self.size + 1
    elseif new_pos < 1 then
        self.pos = 1
    else
        self.pos = new_pos
    end
end

function StreamMetaTable:Read(size)
    local to_read = size
    if self.pos + to_read > self.size + 1 then
        to_read = self.size - self.pos + 1
    end
    
    local pos = self.pos
    self.pos = self.pos + to_read
    return self.data:sub(pos, self.pos - 1) or "", to_read
end

function StreamMetaTable:ReadUChar()
    return string_to_value(self:Read(1))
end

function StreamMetaTable:ReadUShort()
    return string_to_value(self:Read(2))
end

function StreamMetaTable:ReadUInt()
    return string_to_value(self:Read(4))
end

function StreamMetaTable:ReadString()
    local str = self.data:match("(.-)%z", self.pos) or ""
    if #str > 0 then 
        self.pos = self.pos + #str + 1
    end
    
    return str
end
