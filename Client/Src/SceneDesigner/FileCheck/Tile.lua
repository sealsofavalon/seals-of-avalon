
EntityType = { ["ET_MODEL"] = 0, ["ET_TRANSPORT"] = 1 }

function LoadTile(path_name)
    printf("load tile %s\n", path_name)
    local f = io.open(path_name, "rb")
    if not f then
        printf("open tile %s error\n", path_name)
        return
    end
    
    local tag
    local name
    local version = 0
    while true do
        s = CreateTagStream(f)
        
        tag = s:GetTag()        
        if tag == 0 then
            break
        elseif tag == string_to_tag("VERS") then
            version = string_to_value(s:Read(4))
        elseif tag == string_to_tag("TEXS") then
            while true do
                name = s:ReadString()
                if #name == 0 then
                    break
                end
                
                RegisterUsedFile(name)
                s:Seek(4) --fUScale
                s:Seek(4) --fVScale
            end
        elseif tag == string_to_tag("MODL") then
            while true do
                name = s:ReadString()
                if #name == 0 then
                    break
                end
                
                local uiFlags
                local uiType = EntityType.ET_MODEL
                if version < MakeVersion(1, 0) then
                    uiFlags = s:ReadUChar()
                else
                    uiFlags = s:ReadUInt()
                    uiType = s:ReadUInt()
                end
                
                if uiType == EntityType.ET_TRANSPORT then
                    s:Seek(4) --transport id
                end
                
                local uiComponentCount = s:ReadUInt()
                local kComponentName
                local kNifFilePath
                for i = 1, uiComponentCount do
                    kComponentName = s:ReadString()
                    
                    if kComponentName == "Scene Graph" then
                        kNifFilePath = s:ReadString()
                        RegisterUsedFile(kNifFilePath)
                    elseif kComponentName == "Transformation" then
                        s:Seek(3*4 + 3*3*4 + 1*4) --translate, rotation, scale
                    end
                end --for                
            end --while
        end --if
    end --while
    
    f:close()
end