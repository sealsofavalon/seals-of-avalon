@rem Script to build Lua under "Visual Studio .NET Command Prompt".
@rem (contributed by David Manura and Mike Pall)

@setlocal

@call "%VS90COMNTOOLS%vsvars32.bat"
@call "%VCINSTALLDIR%\vcvarsall.bat"

@set MYCOMPILE=cl /nologo /MT /O2 /W3 /c /D_CRT_SECURE_NO_DEPRECATE /wd 4996 /wd 4244
@set MYLINK=link /nologo /DEBUG
@set MYMT=mt /nologo

%MYCOMPILE% l*.c
del luac.obj
%MYLINK% /out:lua.exe l*.obj
del *.obj lua.ilk
