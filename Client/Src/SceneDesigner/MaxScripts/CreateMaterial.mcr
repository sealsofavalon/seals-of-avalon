macroScript CreateMaterial
category: "GameBryo Helper"
buttontext: "Create Material"
tooltip: "Create Material"
(
	for i = 1 to meditMaterials.count do
	(
		meditMaterials[i].shaderType = 2
		meditMaterials[i].Diffuse = color 128 128 128
		meditMaterials[i].ambient = color 128 128 128
		meditMaterials[i].Specular = color 64 64 64
		meditMaterials[i].SpecularEnable = off
		meditMaterials[i].emittance = color 128 128 128
		meditMaterials[i].alphaMode = 1
		meditMaterials[i].VertexColorsEnable = off
		meditMaterials[i].TestRef = 128
		meditMaterials[i].customshader = "Default Shader"
		showTextureMap meditMaterials[i] on
	)

	meditMaterials[1].name = "Hair"
	meditMaterials[1].diffuseMapEnable = on
	meditMaterials[1].diffuseMap = Bitmaptexture fileName:"Hair.dds"
	meditMaterials[1].diffuseMap.name = "Hair"

	meditMaterials[2].name = "Face"
	meditMaterials[2].diffuseMapEnable = on
	meditMaterials[2].diffuseMap = Bitmaptexture fileName:"Face.dds"
	meditMaterials[2].diffuseMap.name = "Face"
)
