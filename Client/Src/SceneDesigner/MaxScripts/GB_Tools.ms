if nc_tools != undefined then closeRolloutFloater nc_tools
nc_tools=newrolloutfloater "GB_Tools" 160 820 1120 5

fn bone_scale idx=   ---缩放骨骼的函数
(
	select $Bone*
	for i in selection do 
		(
			if i.parent == undefined then (i.scale = i.scale * idx) else
				(
					if classof i.parent == Biped_Object then 
						(
							i.pos = (i.pos - i.parent.transform.pos)*idx + i.parent.transform.pos;
							i.scale = i.scale * idx
						)
				)
		)
)
fn addmap mapfile =
(
	local mapfileN=mapfile as name
	local index=finditem mapfiles mapfileN
	if index == 0 do append mapfiles mapfileN
)

fn copybitmap old_files new_patch =
(
	for f in old_files do 
	(
		filename = filenameFromPath f
		newfile = (new_patch + "\\" + filename)
		copyFile f newfile 
	)
)
global tmparrayforbipsel = #() --全局变量
fn SelChildren sel=
(
	append tmparrayforbipsel sel --把自己加进选择集
	for i = 1 to sel.children.count do
	( 
		tempsel = sel.children[i]
		append tmparrayforbipsel tempsel --把子物体加进选择集
		if tempsel != undefined do SelChildren tempsel
	)
	return tmparrayforbipsel
)
function  guiling trans =
	(
		
	)
function savedate = --这个脚本里没有用这个函数
(
   f=createFile ((getFilenamePath (GetExportValue "NiFilename"))+file_name+".txt")
   format "%\n"  (file_name + "  ParentName:  " )  to: f
   close f
)
fn SetTwoKey =
(
	sliderTime = animationrange.end
	max set key keys
	sliderTime = animationrange.start
	max set key keys
)
-----------------第1个卷展栏------------------------------------------------------------
rollout Tools_modle "建模工具" width:148 height:130
(
	button hit1 "重置Xform" pos:[5,5] width:136 height:21 toolTip:"重置xform，位移旋转归零，缩放归100%"
	button hit2 "翻转法线" pos:[5,30] width:136 height:21 toolTip:"如发现法线不正确，可以试用本工具翻转法线"
	button hit3 "渲染小图标" pos:[5,55] width:136 height:21 toolTip:"渲染64*64的小图标" --如果想渲染其他大小图标，可以搜索64，修改成其他数值。
	button hit4 "转为GB材质(场景)" pos:[5,80] width:136 height:21 toolTip:"把场景中全部材质转换为GB材质"
	button hit6 "转为GB材质(角色)" pos:[5,105] width:136 height:21 toolTip:"把场景中全部材质转换为GB材质"
	button hit5 "转为普通材质" pos:[5,130] width:136 height:21 toolTip:"把场景中全部材质转换为普通材质"
	on hit1 pressed do
	(
		if selection.count ==0 then 
			(
				messagebox "请选择一个要处理的物体" beep:false
			)
		else
			(
				holdMaxFile()
				try ( ResetXForm $ ) catch ()
				try ( convertToPoly $ ) catch ()
				try ( ResetTransform $ ) catch ()
				try ( $.Pivot = [0,0,0] ) catch ()
			)
	)--end on
	on hit2 pressed do
	(
		if selection.count ==0 then 
			(
				messagebox "请选择一个要处理的物体" beep:false
			)
		else
			(
				holdMaxFile()
				try ( addmodifier $ (normalmodifier flip:true)  ) catch ()
				try ( convertToMesh $ ) catch ()
			)
	)--end on
	on hit3 pressed do
	(
		renderWidth = 64
		renderHeight = 64
		max quick render
	)--end on
	on hit4 pressed do
	(
		for i in 1 to sceneMaterials.count do
		(
			m = sceneMaterials [i]
			if classof m == Multimaterial then 
			(
				for n in 1 to m.materialList.count do
				(
					m.materialList[n].shaderType = 2
					m.materialList[n].adTextureLock = off
					m.materialList[n].ambient = color 0 0 0
					m.materialList[n].Diffuse = color 255 255 255
					m.materialList[n].emittance = color 200 200 200 
					m.materialList[n].SrcVertexMode = 0
					m.materialList[n].LightingMode = 1
					for a = 1 to m.materialList[n].maps.count do
					(
						if m.materialList[n].maps[a] != undefined then 
						(
							m.materialList[n].maps[a].filtering = 2
						)
					)
				)
			)
			else if classof m == Standardmaterial then 
			(
				m.shaderType = 2
				m.adTextureLock = off
				m.ambient = color 0 0 0
				m.Diffuse = color 255 255 255
				m.emittance = color 200 200 200 
				m.SrcVertexMode = 0
				m.LightingMode = 1
				for a = 1 to m.maps.count do
				(
					if m.maps[a] != undefined then 
					(
						m.maps[a].filtering = 2
					)
				)
			)
		)	
	)--end on
	on hit5 pressed do
	(
		for i in 1 to sceneMaterials.count do
		(
			m = sceneMaterials [i]
			if classof m == Multimaterial then 
			(
				for n in 1 to m.materialList.count do
				(
					m.materialList[n].shaderType = 1
					m.materialList[n].adTextureLock = on
					m.materialList[n].ambient = color 150 150 150
					m.materialList[n].Diffuse = color 150 150 150
				)
			)
			else if classof m == Standardmaterial then 
			(
				m.shaderType = 1
				m.adTextureLock = on
				m.ambient = color 150 150 150
				m.Diffuse = color 150 150 150
		
			)
		)
	)--end on
	on hit6 pressed do
	(
		for i in 1 to sceneMaterials.count do
		(
			m = sceneMaterials [i]
			if classof m == Multimaterial then 
			(
				for n in 1 to m.materialList.count do
				(
					m.materialList[n].shaderType = 2
					m.materialList[n].twoSided = on
					m.materialList[n].alphaMode = 1
					m.materialList[n].adTextureLock = off
					m.materialList[n].ambient = color 255 255 255
					m.materialList[n].Diffuse = color 255 255 255
					m.materialList[n].Specular = color 255 255 255
					m.materialList[n].emittance = color 255 255 255
					m.materialList[n].SrcVertexMode = 1
					m.materialList[n].LightingMode = 0
					m.materialList[n].ApplyMode = 0
					m.materialList[n].AlphaTestEnable = on
					m.materialList[n].NoSorter = on
					m.materialList[n].TestRef = 128
					for a = 1 to m.materialList[n].maps.count do
					(
						if m.materialList[n].maps[a] != undefined then 
						(
							m.materialList[n].maps[a].filtering = 2
						)
					)
				)
			)
			else if classof m == Standardmaterial then 
			(
				m.shaderType = 2
				m.twoSided = on
				m.alphaMode = 1
				m.adTextureLock = off
				m.ambient = color 255 255 255
				m.Diffuse = color 255 255 255
				m.Specular = color 255 255 255
				m.emittance = color 255 255 255
				m.SrcVertexMode = 1
				m.LightingMode = 0
				m.ApplyMode = 0
				m.AlphaTestEnable = on
				m.NoSorter = on
				m.TestRef = 128
				for a = 1 to m.maps.count do
				(
					if m.maps[a] != undefined then 
					(
						m.maps[a].filtering = 2
					)
				)
			)
		)	
	)--end on
)
-----------------第2个卷展栏------------------------------------------------------------
rollout Tools0 "材质工具" width:148 height:130
(
	button hit10 "整理材质球/ 翻到1页" pos:[6,7] width:136 height:21 toolTip:"材质编辑器整理，翻页"
	button hit11 "初始化为GBshader"  width:136 height:21  toolTip:"特效用GB材质"
	button hit5 "双面所有材质球"  width:136 height:21 toolTip:"打开材质球里的双面显示"
	button hit6 "双面显示所选物体"  width:136 height:21 toolTip:"取消背面透明显示"
	button hit1 "清空透明通道" width:136 height:21 toolTip:"清空场景中所有材质透明通道"
	button btn3 "设置收集bitmap路径" width:138 height:21 toolTip:""
	editText edt1 "" width:116 height:21 toolTip:"请点击设置收集bitmap路径按钮设置路径"
	button btn4 "收集Bitmap"width:136 height:21 toolTip:""
	local mapfiles=#()
	local k
	local savefilepath = ""
	local path = ""
	on hit1 pressed do
	(
		holdMaxFile()
		for i in 1 to sceneMaterials.count do
		(
			m = sceneMaterials [i]
			if classof m == Multimaterial then 
			(
				for n in 1 to m.materialList.count do
				(
					m.materialList[n].opacityMap = undefined
				)
			)
			else if classof m == Standardmaterial then 
			(
				m.opacityMap = undefined
			)
		)
	)--end on
	on hit10 pressed do
	(
		disableSceneRedraw()
		if scenematerials.count>0 and geometry.count>0 do
		(
			s = scenematerials.count
			x=int(s/24)---计算取整
			y=mod s 24 ---计算余数
			if k== undefined then k=1
			if k > x+2 do k=1
			if y>0 and k == x+1 then
			(
				for j=1 to 24 do
				(
					new_mat_name = (j+24*(k-1)) as string + " - Default"
					meditMaterials[j] = standard name:new_mat_name
				)
				for i = 1 to y do
				(
					meditMaterials[i] = sceneMaterials[i+24*(k-1)]
				)
				
			)else
			(
				for j=1 to 24 do
				(
					meditMaterials[j]=sceneMaterials[j+24*(k-1)]
				)
			)
			k=k+1
			pushPrompt ("第 "as string+k as string+" 批材质成功运行!! "as string +s as string+"个场景材质。")
			if k==x+1 and y==0 do k=1
			if k==x+2 and y>0 do k=1
			hit10.caption = "整理材质球/ 翻到第" + k as string + "页"
		)
		if scenematerials.count==0 do (pushPrompt ("本场景没有任何材质"))
		enableSceneRedraw()
	)
	on hit11 pressed do 
	(
		TempState = animButtonState
		animButtonState = off
		for i in selection do 
		(
		i.Material.shaderByName = "Gamebryo Shader"
		i.Material.adTextureLock = off
		i.Material.emittance = color 255 255 255
		i.Material.alphaMode = 5
		i.Material.srcBlend = 6
		i.Material.destBlend = 0
		i.Material.alpha = 100
		i.Material.SrcVertexMode = 1
		i.Material.LightingMode = 0
		i.Material.ApplyMode = 2
		try (i.Material.twoSided = on) catch()
		)
		animButtonState = TempState
	)--end on
	on btn3 pressed do
	(	
		path = getSavePath caption:"选择保存文件路径:"  initialDir:savefilepath 
		if path != undefined then 
		try(		
				edt1.text = path;
				savefilepath = path;
			)catch()	
	)--end on
	on btn4 pressed do
	(
		enumeratefiles addmap
		sort mapfiles
		copybitmap mapfiles savefilepath
	)--end on
	on hit5 pressed do 
	(
		for i in sceneMaterials do 
		try (i.twoSided = on) catch()
	)--end on
	on hit6 pressed do 
	(
		for i in selection do 
		try (i.backfacecull = off) catch()
	)--end on
)			
  ----------------开始第2个卷展栏---------------------------------
rollout tools1 "特效工具" width:150 height:555
(
	
	button hit3 "公告板" pos:[5,7] width:65 height:18 toolTip:"创建公告板"
	button btn8 "point" pos:[75,7] width:65 height:18 toolTip:"创建虚拟点"
	button btn11 "Dummy1归零" pos:[5,71] width:66 height:18 toolTip:"可以用来检查dummy点是否真的归零了"
	button btn12 "调整方向" pos:[73,71] width:67 height:18 
	button crt_ptc1 "SuperSpray" pos:[5,28] width:65 height:18 
	button crt_ptc2 "PCloud" pos:[75,28] width:65 height:18 
	button hit_ptc "初始化粒子发射器" pos:[5,49] width:136 height:18 
	button btn_exprot_eff "导出特效" pos:[54,194] width:87 height:18 toolTip:"适合导出绑在身体部位的特效"
	checkbutton ckb1 "max" pos:[6,194] width:44 height:18 enabled:true toolTip:"是否保存max文件" checked:false
	radiobuttons rdo1 "" pos:[14,93] width:158 height:80 labels:#("ehit_b", "ehit_d", "Lhand", "Lfoot", "Rhand", "Rfoot", "Dummy", "body", "contrail", "碰球" , "A_dummy" , "A_skill") default:7 columns:2
	local ex_path = getFilenamePath (GetExportValue "NiFilename")
	local nifname = ex_path + "dummy01.nif"
	
	
	on hit3 pressed do
	( 
		TempState = animButtonState
		animButtonState = off
		local tempos = [0,0,0]
		if selection.count == 1 then tempos = $.transform.pos else tempos = [0,0,0]
		myplane = Plane length:1 width:1 lengthsegs:1 widthsegs:1 transform:(matrix3 [0,1,0] [0,0,1] [1,0,0] [0,0,0]) isSelected:on;$.pos = tempos ;
		convertToMesh $
		setUserPropBuffer $ "billboard\r\nzMode10"
		$.backfacecull = off
		animButtonState = TempState
	)
	on btn11 pressed do
	(
		try ($Dummy01.transform=(matrix3 [1,0,0] [0,1,0] [0,0,1] [0,0,0])) catch()
	)
	on btn12 pressed do
	(
	try ($Dummy01.transform=(matrix3 [-1,0,0] [0,-1,0] [0,0,1] [0,0,0]);) catch()
	try ($Bip01.parent = $Dummy01;) catch()
	try ($Dummy01.transform=(matrix3 [1,0,0] [0,1,0] [0,0,1] [0,0,0])) catch()
	)(
		tmparrayforbipsel = #() --清空变量。
		if selection.count != 1 then 
		(
			messagebox "您现在选择的物体数不为1，请选择要导出物体的跟物体。" beep:false
		)
		else
		(
			disableSceneRedraw()
			holdMaxFile()
			animButtonState = off
			sliderTime = animationrange.start
			$.transform=(matrix3 [-1,0,0] [0,-1,0] [0,0,1] [0,0,0]) --pos归零，z旋转180度。
			SelChildren $  --选择自己和所有子物体。
			select tmparrayforbipsel
			sliderTime = animationrange.end
			max set key keys
			sliderTime = animationrange.start
			max set key keys
			enableSceneRedraw()
			redrawViews()
			if ckb1.state == true then (max file save selected)
		    strFilename = GetExportValue "NiFilename"
			GamebryoExportSelected strFilename
			fetchMaxFile quiet:true	
		)
	)
	on crt_ptc1 pressed do
	(
		TempState = animButtonState
		animButtonState = off
		local tempos = [0,0,0]
		if selection.count == 1 then tempos = $.transform.pos else tempos = [0,0,0]
		SuperSpray speed:0.23 Speed_Variation:10 Off_Axis:0 Axis_Spread:0 Off_Plane:0 Plane_Spread:0 Birth_Rate:1 Total_Number:100 quantityMethod:0 viewPercent:100 Emitter_Start:0f Emitter_Stop:30f Display_Until:100f life:30f Life_Variation:0f subsampleEmitterTranslation:on subsampleCreationTime:on subsampleEmitterRotation:off size:0.15 Size_Variation:10 Growth_Time:10f Fade_Time:10f seed:12345 standardParticle:3 Metaparticle_Tension:1 Metaparticle_Tension_Variation:0 metaballAutoCoarsness:on metaballRenderCoarsness:0.5 viewType:2 mappingType:0 Mapping_Time_Base:30f Mapping_Distance_Base:100 Spin_Time:30f Spin_Time_Variation:0 Spin_Phase:0 Spin_Phase_Variation:0 spinAxisType:0 X_Spin_Vector:1 Y_Spin_Vector:0 Z_Spin_Vector:0 Spin_Axis_Variation:0 motionInfluence:0 motionMultiplier:0 motionVariation:0 Bubble_Amplitude:0 Bubble_Amplitude_Variation:0 Bubble_Period:100000f Bubble_Period_Variation:0 Bubble_Phase:0 Bubble_Phase_Variation:0 iconSize:0 iconHidden:off metaballViewCoarsness:1 Spawn_Affects:100 Spawn_Multiplier_Variation:0 Die__X_frames_after_collision:0f Interparticle_Collisions_On:0 Interparticle_Collision_Steps:2 Interparticle_Collision_Bounce:100 Interparticle_Collision_Bounce_Variation:0 iconSize:0.5 pos:tempos isSelected:on
		animButtonState = TempState
	)
	on crt_ptc2 pressed do
	(
		TempState = animButtonState
		animButtonState = off
		local tempos = [0,0,0]
		if selection.count == 1 then tempos = $.transform.pos else tempos = [0,0,0]
		PCloud speed:0 Speed_Variation:0 Birth_Rate:10 Total_Number:100 quantityMethod:0 viewPercent:100 Emitter_Start:0f Emitter_Stop:0f Display_Until:100f life:101f Life_Variation:0f size:1 Size_Variation:0 Growth_Time:0f Fade_Time:0f seed:12345 standardParticle:3 Metaparticle_Tension:1 Metaparticle_Tension_Variation:0 metaballAutoCoarsness:on metaballRenderCoarsness:0.5 metaballViewCoarsness:1 viewType:2 mappingType:0 Mapping_Time_Base:30f Mapping_Distance_Base:100 Spin_Time:10000f Spin_Time_Variation:0 Spin_Phase:0 Spin_Phase_Variation:0 spinAxisType:0 X_Spin_Vector:1 Y_Spin_Vector:0 Z_Spin_Vector:0 Spin_Axis_Variation:0 Direction_Vector_X:1 Direction_Vector_Y:0 Direction_Vector_Z:0 motionInfluence:100 motionMultiplier:1 motionVariation:0 Bubble_Amplitude:0 Bubble_Amplitude_Variation:0 Bubble_Period:100000f Bubble_Period_Variation:0 Bubble_Phase:0 Bubble_Phase_Variation:0 Emitter_Rad_Len:0.5 emitterHidden:off Spawn_Generations:1 Spawn_Multiplier:1 formation:0 Die__X_frames_after_collision:0f Die__X_frames_after_collision_variation:0 Interparticle_Collisions_On:0 Interparticle_Collision_Steps:2 Interparticle_Collision_Bounce:100 Interparticle_Collision_Bounce_Variation:0 NiPSysLoopStart:0 NiPSysLoopEnd:52 NiPSysMaxAlloc:100 Emitter_Width:0.5 Emitter_Height:0.5 pos:tempos isSelected:on
		animButtonState = TempState
	)
	on hit_ptc pressed do
	(
		particletype = #( Blizzard, Snow, PCloud, Spray, SuperSpray, PArray )
		for i in particletype do 
		(
			if classof $ == i then 
			(
				try( $.speed = 0.23 ) catch()
				try( $.Speed_Variation = 10 ) catch()
				try( $.Emitter_Start = 0f ) catch()
				try( $.Emitter_Stop = 30f ) catch()
				try( $.viewType = 2 ) catch()
				try( $.standardParticle = 3 ) catch()
				try( $.size = 0.15 ) catch()
				try( $.Size_Variation = 10 ) catch()
				try( $.Fade_Time = 11f ) catch()
				try( $.Growth_Time = 11f ) catch()
				try($.motionInfluence = 0 ) catch()
				try($.motionMultiplier = 0 ) catch()	
				try( $.baseObject.ParticleSystemSettings.NiPSysLoop = off ) catch() 
			)
		)
	)--end on
	on btn_exprot_eff pressed do
	(
		if selection.count == 0 then 
		(
			messagebox "您现在没有选择物体,不能进行操作！" beep:false
		)
		else
		(
			holdMaxFile()
			disableSceneRedraw()
			animButtonState = off
			tmparrayforbipsel = #()
			case rdo1.state of
			(
				1:(
						sliderTime = 0
						$.transform=(matrix3 [-1,0,0] [0,-1,0] [0,0,1] [0,0,0]) --pos归零，z旋转180度。
						SelChildren $  --选择自己和所有子物体。
						select tmparrayforbipsel
						nifname = ex_path + "ehit_body.nif"
					)
				2:(
						sliderTime = 0
						$.transform=(matrix3 [-1,0,0] [0,-1,0] [0,0,1] [0,0,0]) --pos归零，z旋转180度。
						SelChildren $  --选择自己和所有子物体。
						select tmparrayforbipsel
						nifname = ex_path + "ehit_dummy01.nif"
					)
				3:(
						sliderTime = 0
						for i in selection do 
						(
							a = i.parent
							if a == undefined then
							(
								messagebox "您现在选择的物体没有父物体（中心点）,不能做归零操作！" beep:false
							)
							else
							(
								b = Point pos:[0,0,0] 
								b.transform = a.transform
								i.parent = b
								b.transform = (matrix3 [1,0,0] [0,1,0] [0,0,1] [0,0,0])  --transform归零 
								delete b
								i.parent = undefined   --设置父物体为未定义
								SelChildren i
							) 
						)
						select tmparrayforbipsel
						nifname = ex_path + "lefthand.nif"
						SetTwoKey ()
					)
				4:(
						sliderTime = 0
						for i in selection do 
						(
							a = i.parent
							if a == undefined then
							(
								messagebox "您现在选择的物体没有父物体（中心点）,不能做归零操作！" beep:false
							)
							else
							(
								b = Point pos:[0,0,0] 
								b.transform = a.transform
								i.parent = b
								b.transform = (matrix3 [1,0,0] [0,1,0] [0,0,1] [0,0,0]) --transform归零 
								delete b
								i.parent = undefined   --设置父物体为未定义
								SelChildren i
							) 
						)
						select tmparrayforbipsel
						nifname = ex_path + "leftfoot.nif"
						SetTwoKey ()
					)
				5:(
						sliderTime = 0
						for i in selection do 
						(
							a = i.parent
							if a == undefined then
							(
								messagebox "您现在选择的物体没有父物体（中心点）,不能做归零操作！" beep:false
							)
							else
							(
								b = Point pos:[0,0,0] 
								b.transform = a.transform
								i.parent = b
								b.transform = (matrix3 [1,0,0] [0,1,0] [0,0,1] [0,0,0])  --transform归零 
								delete b
								i.parent = undefined   --设置父物体为未定义
								SelChildren i
							) 
						)
						select tmparrayforbipsel
						nifname = ex_path + "righthand.nif"
						SetTwoKey ()
					)
				6:(
						sliderTime = 0
						for i in selection do 
						(
							a = i.parent
							if a == undefined then
							(
								messagebox "您现在选择的物体没有父物体（中心点）,不能做归零操作！" beep:false
							)
							else
							(
								b = Point pos:[0,0,0] 
								b.transform = a.transform
								i.parent = b
								b.transform = (matrix3 [1,0,0] [0,1,0] [0,0,1] [0,0,0])  --transform归零 
								delete b
								i.parent = undefined   --设置父物体为未定义
								SelChildren i
							) 
						)
						select tmparrayforbipsel
						nifname = ex_path + "rightfoot.nif"
						SetTwoKey ()
					)		
				7:(
						sliderTime = 0
						for i in selection do 
						(
							i.parent = undefined   --设置父物体为未定义
						)
						nifname = ex_path + "dummy01.nif"
						SetTwoKey ()
					)		
				8:(
						sliderTime = 0
						for i in selection do 
						(
							a = i.parent
							if a == undefined then
							(
								messagebox "您现在选择的物体没有父物体（中心点）,不能做归零操作！" beep:false
							)
							else
							(
								b = Point pos:[0,0,0] 
								b.transform = a.transform
								i.parent = b
								b.transform = (matrix3 [1,0,0] [0,1,0] [0,0,1] [0,0,0]) --transform归零 
								delete b
								i.parent = undefined   --设置父物体为未定义
								SelChildren i
							) 
						)
						select tmparrayforbipsel
						nifname = ex_path + "body.nif"  --绑在胸部骨骼的导出这个名字
						SetTwoKey ()
					)
				9:(
						sliderTime = 0
						$.pos=[0,0,0] --pos归零
						SelChildren $  --选择自己和所有子物体。
						select tmparrayforbipsel
						nifname = ex_path + "contrail.nif"
						---SetTwoKey ()    空中拖尾，要从第一帧开始做吧！
					)
				10:(
						$.name = "Sphere"
						sliderTime = 0
						max file save
						tStart = animationrange.start
						tEnd = animationrange.end
						animButtonState = on
						sliderTime = animationrange.end
						$.visibility = true
						sliderTime = animationrange.start
						$.visibility = true
						animButtonState = off
						$.parent = undefined   --设置父物体为未定义
						nifname = ex_path + (getFilenameFile maxfilename) + ".nif"
						SetTwoKey ()
				   )
				11:(
						sliderTime = 0
						for i in selection do 
						(
							i.parent = undefined   --设置父物体为未定义
							i.pos = [0,0,0]
						)
						nifname = ex_path + "A_dummy.nif"
						SetTwoKey ()
					)	
				12:(
						sliderTime = 0
						for i in selection do 
						(
							i.parent = undefined   --设置父物体为未定义
							i.pos = [0,0,0]
						)
						nifname = ex_path + "A_skill.nif"
						SetTwoKey ()
					)	
			)
			enableSceneRedraw();redrawViews()
			if ckb1.state == true then (max file save selected) -- 保存选择的为max文件。
			GamebryoExportSelected  nifname
			fetchMaxFile quiet:true	
		)
	)
	on btn8 pressed do
	( 
		TempState = animButtonState
		animButtonState = off
		local tempos = [0,0,0]
		if selection.count == 1 then tempos = $.transform.pos else tempos = [0,0,0]
		mypoint =  Point pos:tempos size:5 box: on isSelected:on;mypoint.transform=(matrix3 [1,0,0] [0,1,0] [0,0,1] tempos)
		animButtonState = TempState
	)
)



rollout tools2 "动画工具" width:165 height:298
(
	button hit1 "子物体动画转关键帧" pos:[5,7] width:136 height:21
	button hit2 "路径动画转关键帧" pos:[5,32] width:136 height:21
	button hit3 "反转关键帧(非CS)" pos:[5,56] width:136 height:21
	spinner scl "骨骼缩放系数:" pos:[5,81] width:127 height:16 range:[0,10,1] type:#float fieldwidth:40
	button scale_them "缩放BONE骨骼※CS骨骼" pos:[5,102] width:136 height:21 
	on hit1 pressed do
	(
		if selection.count ==0 then 
			(
				messagebox "请选择一个要处理的物体" beep:false
			)
			else
				(
					holdMaxFile()
					mybox = $
					mypoint = Point ()
					--ResetTransform mybox
					s = animationrange.end
					for i in 0 to s do
						(
							sliderTime = i 
							with animate on
								(
									mypoint.transform = mybox.transform
								)
						)
					setInheritanceFlags mybox #all
					mybox.parent = undefined
					for i in 0 to s do
						(
							sliderTime = i 
							with animate on
								(
									mybox.transform = mypoint.transform
								)
						)
					delete mypoint
		)
	)--end on
	on hit2 pressed do
	(
		if selection.count ==0 then 
			(
				messagebox "请选择一个要处理的物体" beep:false
			)
			else
				(
					holdMaxFile()
					mybox = $
					mypoint = Point ()
					s = animationrange.end
					for i in 0 to s do
						(
							sliderTime = i 
							with animate on
								(
									mypoint.transform = mybox.transform
								)
						)
					eee = mybox.pos.controller
					try ( eee.delete 2 ) catch ()
					for i in 0 to s do
						(
							sliderTime = i 
							with animate on
								(
									mybox.transform = mypoint.transform
								)
						)
					delete mypoint
				)
	)--end on
	on hit3 pressed do
	(
		holdMaxFile()
		for i in selection do
			(
				EndT = animationrange.end + 1
				if classOf i == Biped_Object then
					( 
						messagebox "本工具不支持CS骨骼" beep:false 
						fetchmaxfile quiet:true
						exit
					)
				else
					(
						reverseTime i.transform.controller 0 EndT  #incleft
						deleteTime i.transform.controller 0 1  #incleft
					)
			)
	)--end on
	on scale_them pressed do
	(
		
		sc = scl.value 
	
		try ($bip01.controller.figureMode = true)catch()
		try ($bip01.controller.height=$bip01.controller.height*sc)catch()
		bone_scale sc
		try ($bip01.controller.figureMode = false)catch()
		
	)--end on
)
----------------------------------------
rollout bout "关于本工具" width:162 height:60
(
	button ha "wando 制作" pos:[26,22] width:94 height:21
	HyperLink lnk1 "-----网上帮助及软件更新-----" pos:[5,5] width:140 height:15 color:(color 0 0 255) hovercolor:(color 0 0 255) visitedcolor:(color 0 0 192) address:"http://www.cgjoy.net/bbs/thread-598-1-1.html"
	label lbl2 "Copyright 2007-2008" pos:[21,46] width:102 height:17
	on ha pressed do
		(
			messagebox "作者：铁豌豆\n\nQQ:32431334"\ 
			title:"关于本工具"
		)
)----结束about
		
		
		
addrollout tools_modle nc_tools	
addrollout tools0 nc_tools	
addrollout tools1 nc_tools
addrollout tools2 nc_tools
addrollout bout nc_tools





