if laobian_tools != undefined then closeRolloutFloater laobian_tools
laobian_tools=newrolloutfloater "laobian_tools" 168 700 1120 5

rollout LaobianHelper "老边的帮助工具" width:168 height:700
(
	button btn7 "解决透明物体的交错问题" pos:[6,14] width:151 height:20
	button btn9 "解决不透明物体穿透问题" pos:[6,38] width:151 height:22
	button btn3 "物体lookat相机" pos:[6,66] width:151 height:21
	GroupBox grp1 "动画转换" pos:[6,96] width:150 height:91
	button btn4 "转换选择物体的动画" pos:[20,157] width:124 height:19
	edittext edt1 "" pos:[12,116] width:39 height:16
	label lbl1 "设置动画间隔桢数" pos:[55,118] width:90 height:15
	edittext edt2 "" pos:[12,136] width:39 height:16
	label lbl2 "设置动画间长度" pos:[56,137] width:90 height:14
	button btn1 "设置带有被击盒的虚拟体" pos:[6,194] width:151 height:21
	button btn2 "设置带有攻击盒的虚拟体" pos:[6,218] width:151 height:21 toolTip:""
	button btn11 "选择要重命名的物体" pos:[15,347] width:132 height:20
	GroupBox grp2 "物体批量重命名" pos:[6,301] width:153 height:81
	edittext base_name "" pos:[10,320] width:141 height:20 enabled:true
	GroupBox grp3 "自定义材质组" pos:[6,393] width:153 height:122
	edittext edt4 "材质球序号" pos:[14,419] width:132 height:16
	button btn8 "自定义材质01" pos:[14,443] width:130 height:19
	button btn19 "自定义材质02" pos:[14,468] width:132 height:19
	button btn222 "多向渲染" pos:[6,521] width:69 height:21
	button btn001 "碰撞盒自身EACH GROUP" pos:[6,244] width:151 height:21
	button btn002 "B" pos:[5,272] width:17 height:17
	button btn003 "S" pos:[24,272] width:17 height:17
	button btn004 "C" pos:[44,272] width:17 height:17
	button btn005 "H" pos:[64,272] width:17 height:17
	button btn007 "T" pos:[84,272] width:17 height:17
	button btn008 "O" pos:[104,272] width:17 height:17
	button btn009 "N" pos:[142,272] width:17 height:17
	button btn010 "U" pos:[123,272] width:17 height:17
	button btn28 "自身群组" pos:[86,521] width:69 height:21
	button btn101 "box on" pos:[13,577] width:61 height:20
	groupBox grp17 "Boxmode显示模式" pos:[7,552] width:151 height:57
	button btn102 "Box off" pos:[84,577] width:61 height:20
	--end on

	
	on btn7 pressed do
	(
			try
			(
				Setuserpropbuffer $ "zMode10"
				)
				catch
			(
			messagebox("请选择出问题的透明物体")
			)
		)
	on btn9 pressed do
	(
			try
			(
			Setuserpropbuffer $ "NiSortAdjustNode = SORTING_OFF"
			)
			catch
			(
			messagebox("请选择不透明物体的透明穿透问题")
			)
		)
	on btn3 pressed do
	try
				(
				Setuserpropbuffer $ "billboardRigidCenter"
				)
				catch
				(
				messagebox("请选择需要与相机对齐的物体")
				)
	on btn4 pressed do
	try
	(
	(     
		         modPanel.addModToSelection (Morpher ()) ui:on
				num=edt1.text as integer
				timenum=edt2.text as integer
				for t = 0 to timenum by num do
			        (
				    sliderTime=t 
			                   (
			                   ctime=t / num
				              WM3_MC_BuildFromNode $.morpher ctime $
				                                                )
				                         )
	set animate on
	(
	for m =0 to timenum by num do
	(
	sliderTime=m
		for i = 0 to 100 do
			(
				WM3_MC_SetValue $.Morpher i 0.0
			 )
		ctimet=m / num
		WM3_MC_SetValue $.Morpher ctimet 100.0
	)
	)
		)
	)
	catch
	(
	messagebox("请选择需要转换动画的物品")
	)
	on btn1 pressed do
	(
	try
					(
						Setuserpropbuffer $ "collidee"
						messagebox("请确认被攻击包围盒已经更名为：NDLCD BN beji")
	)
						catch
					(
					messagebox("请选择需要修改的虚拟体")
			)
		)
	on btn2 pressed do
	(
	try
					(
						Setuserpropbuffer $ "collider"
						messagebox("请确认攻击包围盒已经更名为：NDLCD BN tx")
	)
						catch
					(
					messagebox("请选择需要修改的虚拟体")
			)
		)
	on btn11 pressed do
	try
	             (
	
	             if base_name.text != "" then
	
	             for i in selection do i.name = uniquename base_name.text
	
	              )
	catch
				(
				messagebox("请选择需要更名的物体")
				)
	on btn8 pressed do
	(
	 try
			(
	          activeMeditSlot=edt4.text as integer
			m=meditMaterials[activeMeditSlot]
			m.shaderType = 2
			
			m.ambient = color 0 0 0
			m.adTextureLock = off
			m.Diffuse = color 0 0 0
			m.emittance = color 255 255 255
			m.Specular = color 255 255 255
			m.SrcVertexMode = 1
			m.LightingMode = 0
	         m.twoSided = on
		)
	         catch
	                     (
						messagebox("请输入ID为1~24的取值范围")
				)
			)
	on btn19 pressed do
	(
	 try
			(
	          activeMeditSlot=edt4.text as integer
			m=meditMaterials[activeMeditSlot]
	
	         m.shaderType = 2
	         m.Diffuse = color 255 255 255
	         m.ambient = color 0 0 0
	         m.adTextureLock = off
	         m.emittance = color 255 255 255
	         m.Specular = color 255 255 255
	         m.Diffuse = color 0 0 0
	         m.alphaMode = 3
	         m.srcBlend = 0
	         m.destBlend = 0
	         m.twoSided = on
	         )
	         catch
	                     (
						messagebox("请输入ID为1~24的取值范围")
				)
			)
	on btn222 pressed do
	(
		dirl =#("ss")	
			function RenderObj path ActionName prefix dire=
																			(
																					--打开文件
																					
			
																					bOK = true
																					if doesFileExist (path+ActionName+".max") != true then return false
																					if doesFileExist (path+ActionName) != true then
																					(
																						--路径不存在,建立路径
																						bOK = makeDir (path+ActionName)
																						if bOK == false then
																						(
																							--messageBox "路径["+path+ActionName+"]无法创建"
																							return false
																						)
																					)
																					--resetMaxFile(#noPrompt)
																					bOK = loadMaxFile (path+ActionName+".max")
																					if bOK == false then
																					(
																						messageBox "无法打开文件["+path+ActionName+"]"
																						return false
																					)
																					dir = #("00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15")
																					setProperty autobackup "enabled" false																												
																					progressStart "rendering"
																					rol= 22.5
																					trol =16
																					if  dire ==false then 
																					(
																						rol =rol*2
																						trol =trol/2
																					)
																					try(
																					for d = 1 to trol do
																					(
																						if (getProgressCancel())==true then return false
																						td = d*12
																						progressUpdate td
																						outfilename = (path+ActionName+"\\"+prefix+dir[d]+".tga")
																						render outputfile:outfilename camera:$Camera01 fromframe:rendStart toframe:rendEnd nthframe:rendNThFrame outputwidth:renderwidth outputheight:renderheight renderer:#production
																						--变换镜头																	
																						animate off
																						(															
																							rotate $灯光 rol [0,0,1]	
																						)															
																					)
																					)catch(messagebox  "渲染出错了可能是文件有问题，或是没有Camera01\n 建立一个Camera并改名为Camera01")
																					progressEnd ()	
																					setProperty autobackup "enabled" true	
																																	
																					--按照8方向进行渲染			
																			)
																				
																				rollout eight "八方向渲染工具" width:182 height:384
																				(
																					button btAll "渲染文件夹内所有max文件" pos:[9,20] width:161 height:20 toolTip:"对指定文件夹内的max文件进行渲染"
																					label lbl1 "前缀：" pos:[15,46] width:50 height:17
																					edittext etDir "" pos:[49,44] width:98 height:17
																					button bCurrent3 "当前max档8/16方向渲染" pos:[9,108] width:164 height:18
																					checkbox chk2 "使用自定义渲染路径" pos:[11,65] width:155 height:17 enabled:true
																					edittext edt2 "" pos:[7,86] width:131 height:18 enabled:false															
																					button btn13 "..." pos:[141,85] width:26 height:18 toolTip:"自定义渲染图的路径"															
																						
																					button btn36 "256X256" pos:[47,194] width:56 height:20
																					label lbl26 "大小:" pos:[10,196] width:38 height:16
																					button btn37 "512X512" pos:[109,194] width:54 height:20 toolTip:""
																					spinner spn1 "开始:" pos:[17,263] width:69 height:16 range:[-1000,1000,0] type:#integer
																					spinner spn2 "结束:" pos:[101,263] width:61 height:16 range:[0,1000,0] type:#integer
																					spinner spn3 "跳帧数:" pos:[39,284] width:92 height:16 range:[1,1000,1] type:#integer
																					spinner spn9 "宽度:" pos:[17,219] width:146 height:16 range:[0,10000,0] type:#integer
																					spinner spn10 "高度:" pos:[17,240] width:146 height:16 range:[0,10000,0] type:#integer
																					button btn11 "渲染出错，改用" pos:[10,304] width:154 height:17
																					checkbox chk6 "Checkbox" pos:[27,270] width:0 height:0
																					checkbox chk7 "渲染过程显示" pos:[30,323] width:120 height:18
																					Timer tmr2 "Timer" pos:[148,320] width:24 height:24
																					button npcrend "Npc主角专用分层渲染" pos:[10,350] width:157 height:18 toolTip:"头发名字定为Rhair  身体名字定为Rbody"
																					button btn14 "当前max档单方向渲染" pos:[10,129] width:161 height:18
																					GroupBox grp1 "设置" pos:[3,174] width:173 height:168
																					GroupBox grp2 "渲染" pos:[3,5] width:173 height:169
																					checkbox chk14 "16方向吗" pos:[14,149] width:139 height:18
																					on eight open do
																					(
																						spn1.range =[-1000,1000,rendstart]		
																						spn2.range =[0,1000,rendend]
																						spn3.range =[1,1000,rendNThFrame]
																						spn9.range =[0,10000,renderwidth]
																						spn10.range = [0,10000,renderheight]	
																					)
																					on eight moved pos do
																					(
																						spn1.range =[-1000,1000,rendstart]		
																						spn2.range =[0,1000,rendend]
																						spn3.range =[1,1000,rendNThFrame]
																						spn9.range =[0,10000,renderwidth]
																						spn10.range = [0,10000,renderheight]	
																					)
																					on btAll pressed do
																					(
																						path = getsavepath()
																						if path == undefined then
																							(
																									--messageBox "操作取消"
																							)
																							else
																							(		
																									--path = getFilenamePath objname
																									--etDir.text = path
																									--dirs = getDirectories (etDir.text+"\\*.*")
																								dirs = getDirectories (path +"*.*")
																								for d in dirs do
																								(
																									files = getFiles (d+"*.max")
																									for f in files do
																									(
																										name = getFilenameFile f
																									)
																							)
																					messageBox "渲染完成"
																					)	
																						
																					)
																					on bCurrent3 pressed do
																					(							
																						temppath = maxfilepath
																						tempname = getfilenamefile maxfilename																
																						setProperty autobackup "enabled" false																
																					dir = #("00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15")
																					if chk2.checked then
																					(
																						pathname = edt2.text
																					)
																					else
																					(
																						pathname = temppath + tempname
																					)
																					bOK = makeDir ( pathname)
																					if bOK == false then
																					(
																						--messageBox "路径无法创建"																	
																					)
																					if (rendend - rendstart	) > 80 then
																						(
																							bb = querybox "你渲染的帧数超过80帧，是否设定好！"
																							if bb == false then
																							return false	
																						)														
																					progressStart "rendering"
																					dirrend =8
																					dirrol =45
																					if chk14.checked == true then
																					(
																						dirrend =16
																						dirrol =22.5
																					)
																					try(
																						for d = 1 to dirrend do
																						(								
																							td =100/dirrend*d
																							progressUpdate td
																							outfilename = (pathname+"\\"+etDir.text + dir[d]+".tga")
																							render outputfile:outfilename camera:$Camera01 fromframe:rendStart toframe:rendEnd nthframe:rendNThFrame outputwidth:renderwidth outputheight:renderheight renderer:#production
																							-- progressbar:true
																							--变换镜头  vfb:false
																							animate off
																							(
																								rotate $灯光 dirrol [0,0,1]
																							)																		
																						)
																					)
																					catch(messagebox "渲染出错了可能是文件有问题，或是没有Camera01\n 建立一个Camera并改名为Camera01")
																					progressEnd ()																		
																					setProperty autobackup "enabled" true																
																						)
																					on chk2 changed state do
																					(
																						if chk2.checked then
																						(
																							edt2.enabled =true																																			
																						)
																						else
																						(
																							edt2.enabled =false
																						)
																					)
																					on btn13 pressed do
																					(
																							
																							tempsavefilename = getMAXSaveFileName()
																							if tempsavefilename == undefined then
																							(																		
																								chk2.checked = false
																					              chk2.enabled = true
																								return false
																							)
																							else
																							(
																								chk2.enabled = true
																								chk2.checked = true
																								edt2.enabled = true
																								tempsavefilename = getFilenamePath tempsavefilename
																								edt2.text =tempsavefilename	
																							)
																					
																					)
																					on btn36 pressed do
																					(
																						scanlineRender.antiAliasFilter =catmull_rom()
																						renderWidth = 256
																						renderHeight= 256
																						spn9.range =[0,10000,renderwidth]
																						spn10.range = [0,10000,renderheight]																	
																																				)
																					on btn37 pressed do
																					(
																							scanlineRender.antiAliasFilter =catmull_rom()
																							renderWidth = 512
																							renderHeight= 512
																							spn9.range =[0,10000,renderwidth]
																							spn10.range = [0,10000,renderheight]	
																					)
																					on spn1 changed val do
																					(
																						rendStart=val																	
																					)
																					on spn2 changed val do
																					(
																						rendEnd =val																	
																					)
																					on spn3 changed val do
																					(
																						rendNThFrame=val	
																					)
																					on spn9 changed val do
																					(
																						renderwidth=val
																					)
																					on spn10 changed val do
																					(
																						renderheight=val
																					)
																					on btn11 pressed do
																					(
																						
																						temppath1 = maxfilepath
																						tempname1 = getfilenamefile maxfilename
																						setProperty autobackup "enabled" false																
																						dir = #("0","1","2","3","4","5","6","7")
																						if chk2.checked then
																						(
																							pathname1 = edt2.text
																						)
																						else
																						(
																							pathname1 = temppath1 + tempname1
																						)
																							bOK = makeDir ( pathname1)
																						if bOK == false then
																						(
																							--messageBox "路径无法创建"																	
																						)
																						hhh = (rendend - rendstart)/rendNThFrame as integer
																						if  hhh > 80 then
																						(
																							bb = querybox "你渲染的帧数超过80帧，是否设定好！"
																							if bb == false then
																							return false	
																						)														
																						progressStart "rendering"
																						--try(
																							for d = 1 to 8 do
																							(								
																								td =d*12																		
																								for i = 0 to hhh do 
																								(																		
																									dd = rendstart + i * rendNThFrame
																									at time dd
																									(
																										kk = i as string
																										outfilename = (pathname1+"\\"+etDir.text + dir[d]+ "00"+kk +".tga")
																										render outputfile:outfilename camera:$Camera01 outputwidth:renderwidth outputheight:renderheight renderer:#production vfb:chk7.checked
																										-- progressbar:true
																										--变换镜头  vfb:false
																									)
																									td = td + i
																									progressUpdate td
																								)
																								animate off
																								(
																									rotate $灯光 45 [0,0,1]
																								)									
																							)
																						 --)catch( messagebox "渲染出错了可能是文件有问题，或是没有Camera01\n 建立一个Camera并改名为Camera01")
																						progressEnd ()																		
																						setProperty autobackup "enabled" true															
																					  )
																					on tmr2 tick do
																					(
																						spn1.range =[-1000,1000,rendstart]		
																						spn2.range =[0,1000,rendend]
																						spn3.range =[1,1000,rendNThFrame]
																						spn9.range =[0,10000,renderwidth]
																						spn10.range = [0,10000,renderheight]	
																					)
																					on btn14 pressed do
																					(
																						temppath1 = maxfilepath
																						tempname1 = getfilenamefile maxfilename
																						
																						setProperty autobackup "enabled" false													
																						if chk2.checked then
																						(
																							pathname1 = edt2.text
																						)
																						else
																						(
																							pathname1 = temppath1 + tempname1
																						)
																						bOK = makeDir ( pathname1)
																						if bOK == false then
																						(
																							--messageBox "路径无法创建"																	
																						)																					
																							outfilename = (pathname1+"\\"+etDir.text + "0"+".tga")
																							render outputfile:outfilename camera:$Camera01 fromframe:rendStart toframe:rendEnd nthframe:rendNThFrame outputwidth:renderwidth outputheight:renderheight renderer:#production progressbar:true
																							--变换镜头  vfb:false											
																							setProperty autobackup "enabled" true
																					)
																				)
																					 createdialog eight width:182 height:384
																					 registerViewWindow eight
			
				)
	on btn001 pressed do
	try
			(
	
	for b in $'NDLCD BN*' do group b name:"GROUP Box"
	for s in $'NDLCD SN*' do group s name:"GROUP Sphere"
	for c in $'NDLCD CN*' do group c name:"GROUP Capsule"
	for h in $'NDLCD HN*' do group h name:"GROUP Halfspace"
	for u in $'NDLCD UN*' do group u name:"GROUP Union"
	for t in $'NDLCD TN*' do group t name:"GROUP Triangle"
	for o in $'NDLCD ON*' do group o name:"GROUP OBB"
	for n in $'NDLCD NN*' do group n name:"GROUP No Test"
	
	)
	         catch
	                     (
						messagebox("没有要群组的文件")
				)
	on btn002 pressed do
	try
					(
						select $'GROUP Box*'
	
	)
						catch
					(
					messagebox("没找到您要的组")
			)
	on btn003 pressed do
	try
					(
						select $'GROUP Sphere*'
	
	)
						catch
					(
					messagebox("没找到您要的组")
			)
	on btn004 pressed do
	try
					(
						select $'GROUP Capsule*'
	
	)
						catch
					(
					messagebox("没找到您要的组")
			)
	on btn005 pressed do
	try
					(
						select $'GROUP Halfspace*'
	
	)
						catch
					(
					messagebox("没找到您要的组")
			)
	on btn007 pressed do
	try
					(
						select $'GROUP Triangle*'
	
	)
						catch
					(
					messagebox("没找到您要的组")
			)
	on btn008 pressed do
	try
					(
						select $'GROUP OBB*'
	
	)
						catch
					(
					messagebox("没找到您要的组")
			)
	on btn009 pressed do
	try
					(
						select $'GROUP No Test*'
	
	)
						catch
					(
					messagebox("没找到您要的组")
			)
	on btn010 pressed do
	try
					(
						select $'GROUP Union*'
	
	)
						catch
					(
					messagebox("没找到您要的组")
			)
	on btn28 pressed do
		FOR I IN SELECTION DO GROUP I NAME:"EACH GROUP"
	on btn101 pressed do
	(
			try
			(
				$.boxmode = on
				)
				catch
			(
			messagebox("请选择物体")
			)
		)
	on btn102 pressed  do
	(
			try
			(
				$.boxmode = off
				)
				catch
			(
			messagebox("请选择物体")
			)
		)
)

addrollout LaobianHelper laobian_tools