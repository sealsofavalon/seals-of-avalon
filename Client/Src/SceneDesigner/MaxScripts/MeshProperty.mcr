macroScript MeshProperty
category: "GameBryo Helper"
buttontext: "Mesh Property"
tooltip: "Mesh Property"
(
	for obj in $* where SuperClassOf obj == GeometryClass do
	(
		userpropbuffer = getUserPropBuffer obj
		if findString userpropbuffer "NiUnifyNormals" == undefined then
		(
				userpropbuffer = userpropbuffer + "\r\x00A" + "NiUnifyNormals" + "\r\x00A"
				setUserPropBuffer obj userpropbuffer
		)
	)	
)