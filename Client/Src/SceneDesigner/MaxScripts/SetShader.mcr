macroScript SetShader
category: "GameBryo Helper"
buttontext: "Set GameBryo Shader"
tooltip: "Set GameBryo Shader"
(
	for i = 1 to meditMaterials.count do
	(
		meditMaterials[i].shaderType = 2
		meditMaterials[i].Diffuse = color 128 128 128
		meditMaterials[i].ambient = color 128 128 128
		meditMaterials[i].Specular = color 0 0 0
		meditMaterials[i].SpecularEnable = off
		meditMaterials[i].emittance = color 128 128 128
	)
)
