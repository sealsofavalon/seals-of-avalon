macroScript TerrainEditor
category: "TE"
buttontext: "Terrain Editor"
tooltip: "Terrain Editor"
(
		fn CreateTerrain =
		(
				if( $Terrain != undefined ) then
				(
				    MessageBox "Terrain Already Exist"
				    return 0
				)
				
				terrain=plane length:512 width:512 pos:[0, 0, 0] lengthsegs:128 widthsegs:128
				terrain.name="terrain"
				convertto terrain editable_mesh
				settransformlockflags terrain #all
				
				select terrain
				max modify mode
				addModifier terrain (Displace ())
				terrain.modifiers[#Displace].strength = 50.0
		)

		fn ExportTerrain =
		(
				local terrain = $Terrain
				if( terrain == undefined ) then
				(
						MessageBox "Terrain not found"
						return 0
				)
				
   			local filename = getSaveFileName caption:"Export Terrain" types:"Terrain File(*.ter)|*.ter|"
   			if( filename == undefinded ) then
   			(
   					return 0
   			)
   			
   			local file=fopen filename	"wb"
   			if( file == undefined ) then
   			(
   					MessageBox "Can't open the file"
   					return 0
   			)
   			
   			WriteString file "TER"  --sign
   			WriteLong file 1        --version
   			WriteLong file 129	    --x count
   			WriteLong file 129			--y count
   			
   			local vertpos
   			for vertnum = 1 to (129 * 129) do
   			(
   					vertpos = meshOP.getvert terrain vertnum
   					--print (vertpos.x as string)
   					--print (vertpos.y as string)
   					--print (vertpos.z as string)
   					WriteFloat file vertpos.z --height
   			)
   			
   			fclose file
   			
   			MessageBox "Export terrain OK"
		)
		
		rollout TerrainEditorRol "Terrain Editor" width:213 height:300
		(
				button CreateTerrainBtn "Create Terrain" pos:[65,93] width:83 height:36
				button ExportTerrainBtn "Export Terrain" pos:[65,170] width:83 height:36
				on CreateTerrainBtn pressed  do
				(
				   CreateTerrain()
				)
				on ExportTerrainBtn pressed  do
				(
					 ExportTerrain()
				)
		)
			
		CreateDialog TerrainEditorRol style:#(#style_sysmenu,#style_titlebar,#style_minimizebox) modal:false
)