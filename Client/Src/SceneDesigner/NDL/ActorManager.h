#pragma unmanaged

#ifndef ACTOR_MANAGER_H
#define ACTOR_MANAGER_H

#include "../../AssetViewer/ModelFile.h"
class CActorManager : public NiActorManager
{
protected:
    CActorManager(NiKFMTool* pkKFMTool)
        :NiActorManager(pkKFMTool, true)
    {
    }

public:
    static NiActorManager* Create(NiKFMTool* pkKFMTool, const char* pcKFMFilePath)
    {
        NIASSERT(pkKFMTool && pcKFMFilePath);

        // Build the KFM path.
        NiFilename kFilename(pcKFMFilePath);
        char acKFMPath[NI_MAX_PATH];
        NiSprintf(acKFMPath, NI_MAX_PATH, "%s%s", kFilename.GetDrive(), 
        kFilename.GetDir());
        pkKFMTool->SetBaseKFMPath(acKFMPath);

        // Create actor manager.
        CActorManager* pkActorManager = NiNew CActorManager(pkKFMTool);
        if(!pkActorManager->Load())
        {
            NiDelete pkActorManager;
            return NULL;
        }

        return pkActorManager;
    }

    bool Load()
    {
        NiStream kStream;

        const char* pcNIFFilename = m_spKFMTool->GetFullModelPath();
        NIASSERT(pcNIFFilename);

        NiAVObjectPtr spNIFRoot;
        if(IsMFFile(pcNIFFilename))
        {
            CModelFile kModelFile;
            spNIFRoot = kModelFile.Load(pcNIFFilename);
        }
        else
        {
            if(!kStream.Load(pcNIFFilename))
                return false;

            spNIFRoot = NiDynamicCast(NiAVObject, kStream.GetObjectAt(0));
        }

        if(!spNIFRoot)
            return false;

        ChangeNIFRoot(spNIFRoot);
        LoadSequences(&kStream);

        return true;
    }
};

#endif //ACTOR_MANAGER_H

#pragma managed