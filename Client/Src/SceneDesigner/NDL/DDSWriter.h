#ifndef DDS_WRITER_H
#define DDS_WRITER_H

#include <ddraw.h>

static bool WriteDDS(NiPixelData* pkPixelData, const char* pcFilename)
{
    NiFile* pkFile = NiFile::GetFile(pcFilename, NiFile::WRITE_ONLY);
    if (!pkFile || !*(pkFile))
    {
        NiDelete pkFile;
        return false;
    }
    
    bool bDeletePixelData = false;
    NiPixelFormat kPixelFormat = pkPixelData->GetPixelFormat();
    if( kPixelFormat.GetFormat() != NiPixelFormat::FORMAT_DXT1
     && kPixelFormat.GetFormat() != NiPixelFormat::FORMAT_DXT3
     && kPixelFormat.GetFormat() != NiPixelFormat::FORMAT_DXT5 )
    {
        pkPixelData = NiImageConverter::GetImageConverter()->ConvertPixelData(*pkPixelData, NiPixelFormat::RGBA32, NULL, true);
        if(pkPixelData == NULL)
        {
            NiDelete pkFile;
            return false;
        }

        kPixelFormat = pkPixelData->GetPixelFormat();
        bDeletePixelData = true;
    }

    unsigned int uiWidth = pkPixelData->GetWidth();
    unsigned int uiHeight = pkPixelData->GetHeight();
    unsigned int uiMipmapLevels = pkPixelData->GetNumMipmapLevels();
    if(uiMipmapLevels == 0)
        uiMipmapLevels = 1;

    unsigned int uiNumFaces = pkPixelData->GetNumFaces();
    if(uiNumFaces == 0)
        uiNumFaces = 1;

    unsigned int uiMagicNumber = ' SDD';    // "DDS " is the header for all DDS files
    pkFile->Write(&uiMagicNumber, sizeof(uiMagicNumber));

    unsigned int uiHeaderSize = 124; // surface description header
    pkFile->Write(&uiHeaderSize, sizeof(uiHeaderSize));

    //flags
    unsigned int uiFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT | DDSD_MIPMAPCOUNT;
    if(kPixelFormat.GetCompressed())
        uiFlags |= DDSD_LINEARSIZE;
    else
        uiFlags |= DDSD_PITCH;
    pkFile->Write(&uiFlags, sizeof(uiFlags));

    pkFile->Write(&uiHeight, sizeof(uiHeight));
    pkFile->Write(&uiWidth, sizeof(uiWidth));
    
    unsigned int uiPitchOrLinearSize;
    if(kPixelFormat.GetCompressed())
        uiPitchOrLinearSize = pkPixelData->GetSizeInBytes();
    else
        uiPitchOrLinearSize = pkPixelData->GetSizeInBytes() / uiHeight;
    pkFile->Write(&uiPitchOrLinearSize, sizeof(uiPitchOrLinearSize));

    unsigned int uiDepth = 0;
    pkFile->Write(&uiDepth, sizeof(uiDepth)); //depth

    pkFile->Write(&uiMipmapLevels, sizeof(uiMipmapLevels));

    char acReserved[4*11] = { '\0' };
    pkFile->Write(acReserved, sizeof(acReserved));

    DDPIXELFORMAT kDDPixelFormat;
    memset(&kDDPixelFormat, 0, sizeof(kDDPixelFormat));
    kDDPixelFormat.dwSize = sizeof(kDDPixelFormat);
    
    if(kPixelFormat.GetCompressed())
    {
        kDDPixelFormat.dwFlags = DDPF_FOURCC;
        switch(kPixelFormat.GetFormat())
        {
            case NiPixelFormat::FORMAT_DXT1:
                kDDPixelFormat.dwFourCC = FOURCC_DXT1;
                break;

            case NiPixelFormat::FORMAT_DXT3:
                kDDPixelFormat.dwFourCC = FOURCC_DXT3;
                break;

            case NiPixelFormat::FORMAT_DXT5:
                kDDPixelFormat.dwFourCC = FOURCC_DXT5;
                break;
        }
    }
    else
    {
        kDDPixelFormat.dwFlags = DDPF_RGB;
        if(kPixelFormat.GetMask(NiPixelFormat::COMP_ALPHA))
            kDDPixelFormat.dwFlags |= DDPF_ALPHAPIXELS;
        
        kDDPixelFormat.dwRGBBitCount = kPixelFormat.GetBitsPerPixel();
        kDDPixelFormat.dwRBitMask = kPixelFormat.GetMask(NiPixelFormat::COMP_RED);
        kDDPixelFormat.dwGBitMask = kPixelFormat.GetMask(NiPixelFormat::COMP_GREEN);
        kDDPixelFormat.dwBBitMask = kPixelFormat.GetMask(NiPixelFormat::COMP_BLUE);
        kDDPixelFormat.dwRGBAlphaBitMask = kPixelFormat.GetMask(NiPixelFormat::COMP_ALPHA);
    }
    pkFile->Write(&kDDPixelFormat, sizeof(kDDPixelFormat));

    unsigned int uiCaps1 = DDSCAPS_COMPLEX | DDSCAPS_TEXTURE | DDSCAPS_MIPMAP;
    pkFile->Write(&uiCaps1, sizeof(uiCaps1));

    unsigned int uiCaps2 = 0;
    if( uiNumFaces == 6 )
        uiCaps2 = DDSCAPS2_CUBEMAP;
    pkFile->Write(&uiCaps2, sizeof(uiCaps2));

    char acReserved2[4*3] = {'\0'};
    pkFile->Write(acReserved2, sizeof(acReserved2));

    //data
    if(kPixelFormat.GetCompressed())
    {
        const unsigned char* pucPixel;
        unsigned int uiSize;
        for (unsigned int uiFaceIdx = 0; uiFaceIdx < uiNumFaces; uiFaceIdx++)
        {
            for (unsigned int i = 0; i < uiMipmapLevels; i++)
            {
                pucPixel = pkPixelData->GetPixels(i, uiFaceIdx);
                uiSize = pkPixelData->GetSizeInBytes(i, uiFaceIdx);
                pkFile->Write(pucPixel, uiSize);
            }
        }
    }
    else
    {
        const unsigned char* pucPixel;
        unsigned int uiSize;
        for (unsigned int uiFaceIdx = 0; uiFaceIdx < uiNumFaces; uiFaceIdx++)
        {
            for (unsigned int i = 0; i < uiMipmapLevels; i++)
            {
                pucPixel = pkPixelData->GetPixels(i, uiFaceIdx);
                uiSize = pkPixelData->GetSizeInBytes(i, uiFaceIdx);
                pkFile->Write(pucPixel, uiSize);
            }
        }
    }

    NiDelete pkFile;

    if(bDeletePixelData)
        NiDelete pkPixelData;

    return true;
}

#endif //DDS_WRITER_H
