#ifndef EXPORT_TEXTURES_H
#define EXPORT_TEXTURES_H

#include "DDSWriter.h"
void SaveMap(const NiTexturingProperty::Map* pkMap, const char* pcFolder)
{
    if(pkMap == NULL)
        return;

    NiSourceTexture* pkTexture = NiDynamicCast(NiSourceTexture, pkMap->GetTexture());
    if(pkTexture == NULL)
        return;

    NiPixelData* pkPixelData = pkTexture->GetSourcePixelData();
    if(pkPixelData == NULL)
        return;
    
    char acFilename[1024];
    if(pkTexture->GetFilename())
    {
        const char* pcName = strrchr(pkTexture->GetFilename(), '\\');
        if(pcName == NULL)
            pcName = strrchr(pkTexture->GetFilename(), '/');

        if(pcName)
            ++pcName;
        else
            pcName = pkTexture->GetFilename();

        NiSprintf(acFilename, sizeof(acFilename), "%s\\%s", pcFolder, pcName);
    }
    else
    {
        NiSprintf(acFilename, sizeof(acFilename), "%s\\0x%.8x", pcFolder, long(pkTexture));
    }

    const char* pcExt = strrchr(acFilename, '.');
    if(!pcExt || _stricmp(pcExt, ".dds") != 0)
        strcat_s(acFilename, sizeof(acFilename), ".dds");

    WriteDDS(pkPixelData, acFilename);
}

static void ExportTextures(NiAVObject* pkAVObject, const char* pcFolder)
{
    if(pkAVObject == NULL)
        return;

    NiTexturingProperty* pkTexturingProp = (NiTexturingProperty*)pkAVObject->GetProperty(NiTexturingProperty::GetType());
    if(pkTexturingProp)
    {
        const NiTexturingProperty::NiMapArray& kMaps = pkTexturingProp->GetMaps();
        for(unsigned int ui = 0; ui < kMaps.GetSize(); ++ui)
        {
            SaveMap(kMaps.GetAt(ui), pcFolder);
        }

        for(unsigned int ui = 0; ui < pkTexturingProp->GetShaderMapCount(); ++ui)
        {
            SaveMap(pkTexturingProp->GetShaderMap(ui), pcFolder);
        }
    }

    if(pkAVObject->IsNode())
    {
        NiNode* pkNode = (NiNode*)pkAVObject;
        for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ++ui)
        {
            ExportTextures(pkNode->GetAt(ui), pcFolder);
        }
    }
}

#endif