#ifndef MODEL_FILE_H
#define MODEL_FILE_H

#include <NiFilename.h>
static bool IsMFFile(const char* pcFilename)
{
    const char* pcExt = strrchr(pcFilename, '.');
    if(pcExt == NULL)
        return false;

    if ( toupper(pcExt[1]) == 'M' && 
         toupper(pcExt[2]) == 'F' && 
         toupper(pcExt[3]) == '\0' )
        return true;

    return false;
}

static char* GetToken(char*& pcLine)
{
    //忽略前面的空格
    while(*pcLine == ' ' || *pcLine == '\t')
        ++pcLine;

    char* pcToken = pcLine;

    int i = 0;
    while(*pcLine && *pcLine != ' ' && *pcLine != '\t')
    {
        if(*pcLine == '=')  //字符串里面用等号标示空格
            *pcLine = ' ';

        ++pcLine;
    }

    if(*pcLine != '\0')
    {
        *pcLine = '\0';
        pcLine++;
    }

    return pcToken;
}

static char* GetObjectName(char*& pcMesh)
{
    char* name = pcMesh;
    while(*pcMesh && *pcMesh != '\\' && *pcMesh != '/')
        ++pcMesh;

    if(*pcMesh != '\0')
    {
        *pcMesh = '\0';
        ++pcMesh;
    }

    return name;
}

static NiAVObject* GetSubObjectByName(NiNode* pkNode, const NiFixedString& kName)
{
    NiAVObject* pkAVObject;
    for(unsigned int i = 0; i < pkNode->GetArrayCount(); ++i)
    {
        pkAVObject = pkNode->GetAt(i);
        if(pkAVObject)
        {
            if(pkAVObject->GetName() == kName)
                return pkAVObject;

            if(pkAVObject->IsNode())
            {
                pkAVObject = GetSubObjectByName((NiNode*)pkAVObject, kName);
                if(pkAVObject)
                    return pkAVObject;
            }
        }
    }

    return NULL;
}

static NiAVObject* GetAVObject(NiAVObject* pkRoot, char* pcMesh)
{
    NiAVObject* pkAVObject = pkRoot;

    char* name;
    while(true)
    {
        name = GetObjectName(pcMesh);
        if(name[0])
        {
            if(pkAVObject->IsLeaf())
                return NULL;

            pkAVObject = GetSubObjectByName((NiNode*)pkAVObject, name);
            if(pkAVObject == NULL)
                return NULL;
        }
        else
            break;
    }

    if(pkAVObject == pkRoot)
        return NULL;

    return pkAVObject;
}

class CModelFile : public NiMemObject
{
public:
    void ProcessTexture(char* pcLine)
    {
        char* pcMesh = GetToken(pcLine);
        NiAVObject* pkAVObject = GetAVObject(m_spRoot, pcMesh);

        if(pkAVObject == NULL)
        {
            char strMsg[1024];
            NiSprintf(strMsg, sizeof(strMsg), "Unable to find NiAVObject: %s.\n (%s:%d)", pcMesh, (const char*)m_kFileName, m_uiLine);
            ::MessageBox(NULL, strMsg, "Error Loading MF File", MB_OK | MB_ICONERROR);
            return;
        }

        char* pcType = GetToken(pcLine);
        char* pcTextureName = GetToken(pcLine);

        NiTexturingPropertyPtr spTexturingProp = (NiTexturingProperty*)pkAVObject->GetProperty(NiTexturingProperty::GetType());
        if(spTexturingProp == NULL && pcTextureName[0] == '\0')
            return;

        if(spTexturingProp == NULL)
        {
            spTexturingProp = NiTCreate<NiTexturingProperty>();
            pkAVObject->AttachProperty(spTexturingProp);
        }

        NiTexturePtr spTexture = NULL;
        if(pcTextureName[0] != '\0')
        {
            NiFixedString kPlatformSubdirectory = NiImageConverter::GetPlatformSpecificSubdirectory();
            NiImageConverter::SetPlatformSpecificSubdirectory(NULL);

            char acTextureName[512];
            NiSprintf(acTextureName, sizeof(acTextureName), "%s%s", (const char*)m_kFilePath, pcTextureName);
            spTexture = NiSourceTexture::Create(acTextureName);

            NiImageConverter::SetPlatformSpecificSubdirectory(kPlatformSubdirectory);
        }

        if(_stricmp(pcType, "base") == 0)
        {
            if(spTexture)
            {
                NiTexturingProperty::Map* pkMap = spTexturingProp->GetBaseMap();
                if(pkMap == NULL)
                {
                    pkMap = NiTCreate<NiTexturingProperty::Map>();
                    spTexturingProp->SetBaseMap(pkMap);
                }

                pkMap->SetTexture(spTexture);
            }
            else
            {
                spTexturingProp->SetBaseMap(NULL);
            }
        }
        else if(_stricmp(pcType, "glow") == 0)
        {
            if(spTexture)
            {
                NiTexturingProperty::Map* pkMap = spTexturingProp->GetGlowMap();
                if(pkMap == NULL)
                {
                    pkMap = NiTCreate<NiTexturingProperty::Map>();
                    spTexturingProp->SetGlowMap(pkMap);
                }

                pkMap->SetTexture(spTexture);
            }
            else
            {
                spTexturingProp->SetGlowMap(NULL);
            }
        }
        else if(_stricmp(pcType, "dark") == 0)
        {
            if(spTexture)
            {
                NiTexturingProperty::Map* pkMap = spTexturingProp->GetDarkMap();
                if(pkMap == NULL)
                {
                    pkMap = NiTCreate<NiTexturingProperty::Map>();
                    spTexturingProp->SetDarkMap(pkMap);
                }

                pkMap->SetTexture(spTexture);
            }
            else
            {
                spTexturingProp->SetDarkMap(NULL);
            }
        }
        else if(_stricmp(pcType, "detail") == 0)
        {
            if(spTexture)
            {
                NiTexturingProperty::Map* pkMap = spTexturingProp->GetDetailMap();
                if(pkMap == NULL)
                {
                    pkMap = NiTCreate<NiTexturingProperty::Map>();
                    spTexturingProp->SetDetailMap(pkMap);
                }

                pkMap->SetTexture(spTexture);
            }
            else
            {
                spTexturingProp->SetDetailMap(NULL);
            }
        }
        else
        {
            char strMsg[1024];
            NiSprintf(strMsg, sizeof(strMsg), "Unknown texture type : %s.\n (%s:%d)", pcType, (const char*)m_kFileName, m_uiLine);
            ::MessageBox(NULL, strMsg, "Error Loading MF File", MB_OK | MB_ICONERROR);
            return;
        }
    }

    void ProcessShader(char* pcLine)
    {
        char* pcMesh = GetToken(pcLine);
        NiGeometry* pkGeometry = NiDynamicCast(NiGeometry, GetAVObject(m_spRoot, pcMesh));
        if(pkGeometry == NULL)
        {
            char strMsg[1024];
            NiSprintf(strMsg, sizeof(strMsg), "Unable to find NiGeometry: %s.\n (%s:%d)", pcMesh, (const char*)m_kFileName, m_uiLine);
            ::MessageBox(NULL, strMsg, "Error Loading MF File", MB_OK | MB_ICONERROR);
            return;
        }

        char* pcShader = GetToken(pcLine);
        if(pcShader[0] == '\0')
        {
            if(pkGeometry->GetSkinInstance())
            {
                pkGeometry->ApplyAndSetActiveMaterial("FXSkinningSY");
            }
            else
            {
                pkGeometry->ApplyAndSetActiveMaterial("FXSY");
            }
        }
        else if(_stricmp(pcShader, "default") == 0)
            pkGeometry->ApplyAndSetActiveMaterial("NiStandardMaterial");
        else
            pkGeometry->ApplyAndSetActiveMaterial(pcShader);
    }

    void ProcessHide(char* pcLine)
    {
        char* pcMesh = GetToken(pcLine);
        NiAVObject* pkAVObject = GetAVObject(m_spRoot, pcMesh);
        if(pkAVObject == NULL)
        {
            char strMsg[1024];
            NiSprintf(strMsg, sizeof(strMsg), "Unable to find NiAVObject: %s.\n (%s:%d)", pcMesh, (const char*)m_kFileName, m_uiLine);
            ::MessageBox(NULL, strMsg, "Error Loading MF File", MB_OK | MB_ICONERROR);
            return;
        }

        char* pcHide = GetToken(pcLine);
        bool bHide = true;
        if(pcHide[0] != '\0')
        {
            if(atoi(pcHide) > 0 || _stricmp(pcHide, "true") == 0)
                bHide = true;
            else
                bHide = false;
        }

        pkAVObject->SetAppCulled(bHide);
    }

    void ProcessMount(char* pcLine)
    {
        char* pcNode = GetToken(pcLine);
        NiNode* pkNode = NiDynamicCast(NiNode, GetAVObject(m_spRoot, pcNode));
        if(pkNode == NULL)
        {
            char strMsg[1024];
            NiSprintf(strMsg, sizeof(strMsg), "Unable to find NiNode: %s.\n (%s:%d)", pcNode, (const char*)m_kFileName, m_uiLine);
            ::MessageBox(NULL, strMsg, "Error Loading MF File", MB_OK | MB_ICONERROR);
            return;
        }

        char* pcMount = GetToken(pcLine);
        if(pcMount[0] == '\0')
        {
            char strMsg[1024];
            NiSprintf(strMsg, sizeof(strMsg), "Mount name error: %s.\n (%s:%d)", pcMount, (const char*)m_kFileName, m_uiLine);
            ::MessageBox(NULL, strMsg, "Error Loading MF File", MB_OK | MB_ICONERROR);
            return;
        }

        NiStream kStream;
        if(!kStream.Load(pcMount))
        {
            char strMsg[1024];
            NiSprintf(strMsg, sizeof(strMsg), "Unable to load mount: %s.\n (%s:%d)", pcMount, (const char*)m_kFileName, m_uiLine);
            ::MessageBox(NULL, strMsg, "Error Loading MF File", MB_OK | MB_ICONERROR);
            return;
        }

        NiAVObject* pkAVObject = NiDynamicCast(NiAVObject, kStream.GetObjectAt(0));
        if(pkAVObject == NULL)
        {
            char strMsg[1024];
            NiSprintf(strMsg, sizeof(strMsg), "Not a NiAVObject: %s.\n (%s:%d)", pcMount, (const char*)m_kFileName, m_uiLine);
            ::MessageBox(NULL, strMsg, "Error Loading MF File", MB_OK | MB_ICONERROR);
            return;
        }

        pkNode->AttachChild(pkAVObject);
    }

    void ProcessLine(char* pcLine)
    {
        //第一个单词是命令
        char* pcCmd = GetToken(pcLine);
        if(_stricmp(pcCmd, "texture") == 0)
        {
            ProcessTexture(pcLine);
        }
        else if(_stricmp(pcCmd, "shader") == 0)
        {
            ProcessShader(pcLine);
        }
        else if(_stricmp(pcCmd, "hide") == 0)
        {
            ProcessHide(pcLine);
        }
        else if(_stricmp(pcCmd, "mount") == 0)
        {
            ProcessMount(pcLine);
        }
        else
        {
            char strMsg[1024];
            NiSprintf(strMsg, sizeof(strMsg), "Unknown cmd: %s.\n (%s:%d)", pcCmd, (const char*)m_kFileName, m_uiLine);
            ::MessageBox(NULL, strMsg, "Error Loading MF File", MB_OK | MB_ICONERROR);
            return;
        }
    }

    void Process()
    {
        char acLine[512];
        while(m_pkFile->GetLine(acLine, sizeof(acLine)))
        {
            ProcessLine(acLine);
            m_uiLine++;
        }

        m_spRoot->UpdateProperties();
        m_spRoot->Update(0.f);
    }

    NiAVObject* Load(const NiFixedString& kFileName)
    {
        if ( !IsMFFile(kFileName) )
            return NULL;

        m_kFileName = kFileName;
        m_pkFile = NULL;
        m_uiLine = 0;
        m_spRoot = NULL;

        m_pkFile = NiFile::GetFile(kFileName, NiFile::READ_ONLY);
        if(!m_pkFile || !*m_pkFile)
        {
            NiDelete m_pkFile;
            m_pkFile = NULL;
            return NULL;
        }

        char acLine[256];
        m_pkFile->GetLine(acLine, sizeof(acLine));
        m_uiLine = 1;

        char acBuffer[NI_MAX_PATH];
        NiFilename kFilename(kFileName);
        kFilename.SetFilename("");
        kFilename.SetExt("");
        kFilename.GetFullPath(acBuffer, NI_MAX_PATH);
        NiPath::Standardize(acBuffer);

        m_kFilePath = acBuffer;

        strcat_s(acBuffer, acLine);
        NiStream kStream;
        if( kStream.Load(acBuffer) && kStream.GetObjectCount() > 0) //装载Nif
        {
            m_spRoot = NiDynamicCast(NiAVObject, kStream.GetObjectAt(0));
            if(m_spRoot)
                Process();
        }

        NiDelete m_pkFile;
        m_pkFile = NULL;

        return m_spRoot;
    }

private:
    NiFixedString m_kFileName;
    NiFixedString m_kFilePath;
    unsigned int m_uiLine;
    NiFile* m_pkFile;
    NiAVObjectPtr m_spRoot;
};

#endif
