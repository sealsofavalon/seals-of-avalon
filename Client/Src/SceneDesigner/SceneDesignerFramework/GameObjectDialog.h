
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner {

	namespace Framework 
	{

		public __gc class GameObjectDialog : public System::Windows::Forms::Form
		{
		private: System::Windows::Forms::Label*  m_NameLabel;
		private: System::Windows::Forms::Button*  m_NameButton;
		private: System::Windows::Forms::Label*  m_IDLabel;
		private: System::Windows::Forms::Button*  m_IDButton;
		private: System::Windows::Forms::TextBox*  m_IDTextBox;
		private: System::Windows::Forms::TextBox*  m_NameTextBox;
        private: NiTPrimitiveArray<GameObjectInfo*>* m_pkGameObjects;
        private: NiTObjectArray<NiFixedString>* m_pkNifNames;

		private:
		public:
			GameObjectDialog()
			{
                m_pkGameObjects = NiNew NiTPrimitiveArray<GameObjectInfo*>;
                m_pkNifNames = NiNew NiTObjectArray<NiFixedString>;
				this->InitializeComponent();
			}

			void Do_Dispose(bool bDisposing)
			{
                if(m_pkGameObjects)
                {
                    NiDelete m_pkGameObjects;
                    NiDelete m_pkNifNames;
                    m_pkGameObjects = NULL;
                }
			}

            GameObjectInfo** GetGameObjects() { return m_pkGameObjects->GetBase(); }
            NiFixedString* GetNifNames() { return m_pkNifNames->GetBase(); }
            unsigned int  GetSize() { return m_pkGameObjects->GetSize(); }

		private: 
			System::Void InitializeComponent() 
			{
                this->m_NameLabel = (new System::Windows::Forms::Label());
                this->m_NameButton = (new System::Windows::Forms::Button());
                this->m_NameTextBox = (new System::Windows::Forms::TextBox());
                this->m_IDLabel = (new System::Windows::Forms::Label());
                this->m_IDButton = (new System::Windows::Forms::Button());
                this->m_IDTextBox = (new System::Windows::Forms::TextBox());
                this->SuspendLayout();
                // 
                // m_NameLabel
                // 
                this->m_NameLabel->AutoSize = true;
                this->m_NameLabel->Location = System::Drawing::Point(14, 28);
                this->m_NameLabel->Name = S"m_NameLabel";
                this->m_NameLabel->Size = System::Drawing::Size(29, 12);
                this->m_NameLabel->TabIndex = 0;
                this->m_NameLabel->Text = S"名字";
                // 
                // m_NameButton
                // 
                this->m_NameButton->Location = System::Drawing::Point(243, 23);
                this->m_NameButton->Name = S"m_NameButton";
                this->m_NameButton->Size = System::Drawing::Size(94, 23);
                this->m_NameButton->TabIndex = 1;
                this->m_NameButton->Text = S"通过名字选择";
                this->m_NameButton->UseVisualStyleBackColor = true;
                this->m_NameButton->Click += new System::EventHandler(this, &GameObjectDialog::OnNameButtonClick);
                // 
                // m_NameTextBox
                // 
                this->m_NameTextBox->Location = System::Drawing::Point(57, 24);
                this->m_NameTextBox->Name = S"m_NameTextBox";
                this->m_NameTextBox->Size = System::Drawing::Size(172, 21);
                this->m_NameTextBox->TabIndex = 2;
                // 
                // m_IDLabel
                // 
                this->m_IDLabel->AutoSize = true;
                this->m_IDLabel->Location = System::Drawing::Point(14, 70);
                this->m_IDLabel->Name = S"m_IDLabel";
                this->m_IDLabel->Size = System::Drawing::Size(17, 12);
                this->m_IDLabel->TabIndex = 0;
                this->m_IDLabel->Text = S"ID";
                // 
                // m_IDButton
                // 
                this->m_IDButton->Location = System::Drawing::Point(243, 65);
                this->m_IDButton->Name = S"m_IDButton";
                this->m_IDButton->Size = System::Drawing::Size(94, 23);
                this->m_IDButton->TabIndex = 1;
                this->m_IDButton->Text = S"通过ID选择";
                this->m_IDButton->UseVisualStyleBackColor = true;
                this->m_IDButton->Click += new System::EventHandler(this, &GameObjectDialog::OnIDButtonClick);
                // 
                // m_IDTextBox
                // 
                this->m_IDTextBox->Location = System::Drawing::Point(57, 66);
                this->m_IDTextBox->Name = S"m_IDTextBox";
                this->m_IDTextBox->Size = System::Drawing::Size(172, 21);
                this->m_IDTextBox->TabIndex = 2;
                // 
                // NpcSelectDialog
                // 
                this->ClientSize = System::Drawing::Size(350, 114);
                this->Controls->Add(this->m_IDTextBox);
                this->Controls->Add(this->m_IDButton);
                this->Controls->Add(this->m_NameTextBox);
                this->Controls->Add(this->m_IDLabel);
                this->Controls->Add(this->m_NameButton);
                this->Controls->Add(this->m_NameLabel);
                this->MaximizeBox = false;
                this->MinimizeBox = false;
                this->Name = S"Game Object Dialog";
                this->Text = S"选择Game Object";
                this->ResumeLayout(false);
                this->PerformLayout();
            }

		protected: 
			void Dispose(Boolean disposing)
			{
				__super::Dispose(disposing);
			}

		private:
			System::Void OnIDButtonClick(System::Object*  sender, System::EventArgs*  e) 
			{
                CTokenString kTokenString;
				const char* pcString = MStringToCharPointer(m_IDTextBox->Text);
                kTokenString.ParseTokens(pcString);
				MFreeCharPointer(pcString);

				if( kTokenString.GetSize() == 0 )
					return;

                m_pkGameObjects->RemoveAll();
                NiDefaultErrorHandlerPtr spErrors = NiNew NiDefaultErrorHandler();
                
                unsigned int uiEntryID;
                char acMsg[1024];
                char acModelName[1024];
                NiFixedString* pkModelName;
                GameObjectInfo* pkGameObjectInfo;
                for(unsigned int ui = 0; ui < kTokenString.GetSize(); ++ui)
                {
                    uiEntryID = atoi(kTokenString.GetToken(ui));
                    if(uiEntryID == 0)
                        continue;

                    pkGameObjectInfo = g_pkGameObjectNamesDB->GetGameObjectInfo(uiEntryID);
                    if(pkGameObjectInfo == NULL)
                    {
                        NiSprintf(acMsg, sizeof(acMsg), "GameObject %d not found", uiEntryID);
                        spErrors->ReportError(acMsg, NULL, NULL, NULL);
                        continue;
                    }

                    if( !g_pkDisplayInfoDB->GetModelName(pkGameObjectInfo->m_uiDisplayID, pkModelName) )
                    {
                        NiSprintf(acMsg, sizeof(acMsg), "没有找到GameObject对应的模型名称 Display ID %d", pkGameObjectInfo->m_uiDisplayID);
                        spErrors->ReportError(acMsg, NULL, NULL, NULL);
                        continue;
                    }

                    NiSprintf(acModelName, sizeof(acModelName), "%s\\%s", TerrainHelper::GetClientPath(), (const char*)*pkModelName);
                    if( !NiFile::Access(acModelName, NiFile::READ_ONLY) )
                    {
                        NiSprintf(acMsg, sizeof(acMsg), "没有找到GameObject 模型文件 %s", acModelName);
                        spErrors->ReportError(acMsg, NULL, NULL, NULL);
                        continue;
                    }

                    m_pkNifNames->Add(acModelName);
                    m_pkGameObjects->Add(pkGameObjectInfo);
                }

                MUtility::AddErrorInterfaceMessages(MessageChannelType::Errors, spErrors);
                if(m_pkGameObjects->GetSize() > 0)
				    this->DialogResult = DialogResult::OK;
			}

		private: 
			System::Void OnNameButtonClick(System::Object*  sender, System::EventArgs*  e) 
			{
                CTokenString kTokenString;
                const char* pcString = MStringToCharPointer(m_NameTextBox->Text);
                kTokenString.ParseTokens(pcString);
                MFreeCharPointer(pcString);

                if( kTokenString.GetSize() == 0 )
                    return;

                m_pkGameObjects->RemoveAll();
                NiDefaultErrorHandlerPtr spErrors = NiNew NiDefaultErrorHandler();

                NiFixedString kGameObjectName;
                GameObjectInfo* pkGameObjectInfo;
                char acMsg[1024];
                char acModelName[1024];
                NiFixedString* pkModelName;
                for(unsigned int ui = 0; ui < kTokenString.GetSize(); ++ui)
                {
                    kGameObjectName = kTokenString.GetToken(ui);
                    if(kGameObjectName.GetLength() == 0)
                        continue;

                    pkGameObjectInfo = g_pkGameObjectNamesDB->GetGameObjectInfo(kGameObjectName);
                    if(pkGameObjectInfo == NULL)
                    {
                        NiSprintf(acMsg, sizeof(acMsg), "GameObject %s not found", (const char*)kGameObjectName);
                        spErrors->ReportError(acMsg, NULL, NULL, NULL);
                        continue;
                    }

                    if( !g_pkDisplayInfoDB->GetModelName(pkGameObjectInfo->m_uiDisplayID, pkModelName) )
                    {
                        NiSprintf(acMsg, sizeof(acMsg), "没有找到GameObject对应的模型名称 Display ID %d", pkGameObjectInfo->m_uiDisplayID);
                        spErrors->ReportError(acMsg, NULL, NULL, NULL);
                        continue;
                    }

                    NiSprintf(acModelName, sizeof(acModelName), "%s\\%s", TerrainHelper::GetClientPath(), (const char*)*pkModelName);
                    if( !NiFile::Access(acModelName, NiFile::READ_ONLY) )
                    {
                        NiSprintf(acMsg, sizeof(acMsg), "没有找到GameObject 模型文件 %s", acModelName);
                        spErrors->ReportError(acMsg, NULL, NULL, NULL);
                        continue;
                    }

                    m_pkNifNames->Add(acModelName);
                    m_pkGameObjects->Add(pkGameObjectInfo);
                }

                MUtility::AddErrorInterfaceMessages(MessageChannelType::Errors, spErrors);
                if(m_pkGameObjects->GetSize() > 0)
                    this->DialogResult = DialogResult::OK;
            }
	};
 }
}}}