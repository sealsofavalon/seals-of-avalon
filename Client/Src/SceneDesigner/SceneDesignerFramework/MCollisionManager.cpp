// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "SceneDesignerFrameworkPCH.h"

#include "MFramework.h"
#include "MCollisionManager.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//---------------------------------------------------------------------------
void MCollisionManager::Init()
{
	if (ms_pmThis == NULL)
	{
		ms_pmThis = new MCollisionManager();
	}
}
//---------------------------------------------------------------------------
void MCollisionManager::Shutdown()
{
	if (ms_pmThis != NULL)
	{
		ms_pmThis->Dispose();
		ms_pmThis = NULL;
	}
}
//---------------------------------------------------------------------------
bool MCollisionManager::InstanceIsValid()
{
	return (ms_pmThis != NULL);
}
//---------------------------------------------------------------------------
MCollisionManager* MCollisionManager::get_Instance()
{
	return ms_pmThis;
}
//---------------------------------------------------------------------------
MCollisionManager::MCollisionManager()
{
	m_pkObjectNode = NULL;
}
//---------------------------------------------------------------------------
void MCollisionManager::Do_Dispose(bool bDisposing)
{
}
//---------------------------------------------------------------------------
void MCollisionManager::Startup()
{
	MVerifyValidInstance;

}
//---------------------------------------------------------------------------
void MCollisionManager::OnSettingsChanged(Object* pmSender,
									  SettingChangedEventArgs* pmEventArgs)
{
	MVerifyValidInstance;

}
//---------------------------------------------------------------------------
void MCollisionManager::AddCollider(NiAVObject* pkObject, bool bCreateOBB)
{
	//NiAVObject* pkABV = pkObject->GetObjectByName("Dummy02");
	NiCollisionData* pkData = NiGetCollisionData(pkObject);
	if(pkData)
	{
		m_pkObjectNode = pkObject;
	}
}
//---------------------------------------------------------------------------
void MCollisionManager::AddCollidee(NiAVObject* pkCollidee, bool bCreateOBB)
{

}
//---------------------------------------------------------------------------
void MCollisionManager::RemoveCollider(NiAVObject* pkObject)
{

}
//---------------------------------------------------------------------------
void MCollisionManager::RemoveCollidee(NiAVObject* pkObject)
{

}
//---------------------------------------------------------------------------
void MCollisionManager::Remove(NiAVObject* pkObject)
{

}
//---------------------------------------------------------------------------
void MCollisionManager::RemoveAll()
{

}
//---------------------------------------------------------------------------
void MCollisionManager::Update(float fTime)
{
	MVerifyValidInstance;
}