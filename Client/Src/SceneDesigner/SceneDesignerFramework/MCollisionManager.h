// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#pragma once

#include "MDisposable.h"
#include "ISettingsService.h"
#include "IOptionsService.h"

using namespace System::Collections;
using namespace System::Runtime::InteropServices;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI
::StandardServices;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		public __gc class MCollisionManager : public MDisposable
		{
		private public:
			void Startup();

		public:
			void Update(float fTime);

			void OnSettingsChanged(Object* pmSender, 
				SettingChangedEventArgs* pmEventArgs);

			void AddCollider(NiAVObject* pkObject, bool bCreateOBB);
			void AddCollidee(NiAVObject* pkCollidee, bool bCreateOBB);
			
			void RemoveCollider(NiAVObject* pkObject);
			void RemoveCollidee(NiAVObject* pkObject);

			void Remove(NiAVObject* pkObject);
			void RemoveAll();

		protected:
			NiAVObject* m_pkObjectNode;

		private:

			// MDisposable members.
		protected:
			virtual void Do_Dispose(bool bDisposing);

			// Singleton members.
		private public:
			static void Init();
			static void Shutdown();
			static bool InstanceIsValid();
			__property static MCollisionManager* get_Instance();
		private:
			static MCollisionManager* ms_pmThis = NULL;
			MCollisionManager();
		};
	}}}}

