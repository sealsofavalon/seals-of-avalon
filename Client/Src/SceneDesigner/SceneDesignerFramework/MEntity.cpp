// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "SceneDesignerFrameworkPCH.h"

#include "MEntity.h"
#include "MEventManager.h"
#include "MEntityFactory.h"
#include "MComponentFactory.h"
#include "MUtility.h"
#include "MAddRemoveComponentCommand.h"
#include "MRenameEntityCommand.h"
#include "MChangeHiddenStateCommand.h"
#include "MChangeFrozenStateCommand.h"
#include "ServiceProvider.h"
#include "MEntityPropertyDescriptor.h"
#include "MExpandablePropertyConverter.h"
#include "MFramework.h"
#include "IEntityPathService.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace System::Drawing;

static const char* gs_pcSceneRootPointerName = "Scene Root Pointer";

//---------------------------------------------------------------------------
MEntity::MEntity(NiEntityInterface* pkEntity) : m_pkEntity(pkEntity)
{
    MInitInterfaceReference(m_pkEntity);
}
//---------------------------------------------------------------------------
void MEntity::_SDMInit()
{
    m_pkPropertyNames = NiNew NiTObjectSet<NiFixedString>(64);
}
//---------------------------------------------------------------------------
void MEntity::_SDMShutdown()
{
    NiDelete m_pkPropertyNames;
}
//---------------------------------------------------------------------------
NiEntityPropertyInterface* MEntity::get_PropertyInterface()
{
    return m_pkEntity;
}
//---------------------------------------------------------------------------
void MEntity::Do_Dispose(bool bDisposing)
{
    __super::Do_Dispose(bDisposing);

    if (bDisposing)
    {
        const unsigned int uiArrayCount = m_pkEntity->GetComponentCount();
        for (unsigned int ui = 0; ui < uiArrayCount; ui++)
        {
            MComponentFactory::Instance->Remove(m_pkEntity->GetComponentAt(
                ui));
        }

        MDisposeInterfaceReference(m_pkEntity);
    }
}
//---------------------------------------------------------------------------
String* MEntity::ToString()
{
    MVerifyValidInstance;

    return this->Name;
}
//---------------------------------------------------------------------------
String* MEntity::get_Name()
{
    MVerifyValidInstance;

    return m_pkEntity->GetName();
}
//---------------------------------------------------------------------------
void MEntity::set_Name(String* strName)
{
    MVerifyValidInstance;

    m_bDirtyBit = true;

    if (!strName->Equals(this->Name))
    {
        // Check for unique names here.
        MScene* pmScene = MScene::FindSceneContainingEntity(this);
        if (pmScene != NULL && pmScene->GetEntityByName(strName) != NULL)
        {
            throw new ArgumentException(String::Format("An entity "
                "already exists in the \"{0}\" scene with the name "
                "\"{1}\"; new name cannot be set.", pmScene->Name, strName));
        }

        const char* pcName = MStringToCharPointer(strName);
        CommandService->ExecuteCommand(new MRenameEntityCommand(
            NiNew NiRenameEntityCommand(m_pkEntity, pcName)), true);
        MFreeCharPointer(pcName);
    }
}
//---------------------------------------------------------------------------
Guid MEntity::get_TemplateID()
{
    MVerifyValidInstance;

    return MUtility::IDToGuid(m_pkEntity->GetTemplateID());
}
//---------------------------------------------------------------------------
void MEntity::set_TemplateID(Guid mGuid)
{
    MVerifyValidInstance;
    NiUniqueID kUniqueID;
    MUtility::GuidToID(mGuid, kUniqueID);
    m_pkEntity->SetTemplateID(kUniqueID);
}
//---------------------------------------------------------------------------
MEntity* MEntity::CreateGeneralEntity(String* strName)
{
    const char* pcName = MStringToCharPointer(strName);
    NiUniqueID kTemplateID;
    MUtility::GuidToID(Guid::NewGuid(), kTemplateID);
    NiGeneralEntity* pkEntity = NiNew NiGeneralEntity(pcName, kTemplateID);
    MFreeCharPointer(pcName);
    return MEntityFactory::Instance->Get(pkEntity);
}
//---------------------------------------------------------------------------
MEntity* MEntity::Clone(String* strNewName, bool bInheritProperties)
{
    MVerifyValidInstance;

    const char* pcNewName = MStringToCharPointer(strNewName);
    MEntity* pmClone = MEntityFactory::Instance->Get(m_pkEntity->Clone(
        pcNewName, bInheritProperties));
    MFreeCharPointer(pcNewName);
    pmClone->Update(0, MFramework::Instance->ExternalAssetManager);

    return pmClone;
}
//---------------------------------------------------------------------------
MEntity* MEntity::get_MasterEntity()
{
    MVerifyValidInstance;

    NiEntityInterface* pkMasterEntity = m_pkEntity->GetMasterEntity();
    if (pkMasterEntity)
    {
        return MEntityFactory::Instance->Get(pkMasterEntity);
    }

    return NULL;
}
//---------------------------------------------------------------------------
void MEntity::set_MasterEntity(MEntity* pmEntity)
{
    MVerifyValidInstance;

    m_bDirtyBit = true;

    
    MAssert(pmEntity != this, "Master Entity set to itself");

    if (pmEntity)
    {
        m_pkEntity->ReplaceMasterEntity(pmEntity->m_pkEntity);
    }
    else
    {
        m_pkEntity->ReplaceMasterEntity(NULL);
    }
}
//---------------------------------------------------------------------------
void MEntity::MakeEntityUnique()
{
    MVerifyValidInstance;
    m_bDirtyBit = true;

    String* astrPropertyNames[] = GetPropertyNames();

    CommandService->BeginUndoFrame(String::Format("Make \"{0}\" entity "
        "unique", this->Name));
    for (int i = 0; i < astrPropertyNames->Length; i++)
    {
        MakePropertyUnique(astrPropertyNames[i]);
    }
    CommandService->EndUndoFrame(true);
}
//---------------------------------------------------------------------------
bool MEntity::get_SupportsComponents()
{
    MVerifyValidInstance;

    return NIBOOL_IS_TRUE(m_pkEntity->SupportsComponents());
}
//---------------------------------------------------------------------------
unsigned int MEntity::get_ComponentCount()
{
    MVerifyValidInstance;

    return m_pkEntity->GetComponentCount();
}
//---------------------------------------------------------------------------
MComponent* MEntity::GetComponents()[]
{
    MVerifyValidInstance;

    unsigned int uiComponentCount = m_pkEntity->GetComponentCount();
    MComponent* amComponents[] = new MComponent*[uiComponentCount];
    for (unsigned int ui = 0; ui < uiComponentCount; ui++)
    {
        amComponents[ui] = MComponentFactory::Instance->Get(
            m_pkEntity->GetComponentAt(ui));
    }

    return amComponents;
}
//---------------------------------------------------------------------------
MComponent* MEntity::GetComponentByTemplateID(Guid mTemplateID)
{
    MVerifyValidInstance;

    NiUniqueID kTemplateID;
    MUtility::GuidToID(mTemplateID, kTemplateID);
    return MComponentFactory::Instance->Get(
        m_pkEntity->GetComponentByTemplateID(kTemplateID));
}
//---------------------------------------------------------------------------
bool MEntity::CanAddComponent(MComponent* pmComponent)
{
    MVerifyValidInstance;

    MAssert(pmComponent != NULL, "Null component provided to function!");

    return NIBOOL_IS_TRUE(pmComponent->GetNiEntityComponentInterface()
        ->CanAttachToEntity(m_pkEntity));
}
//---------------------------------------------------------------------------
bool MEntity::CanRemoveComponent(MComponent* pmComponent)
{
    MVerifyValidInstance;

    MAssert(pmComponent != NULL, "Null component provided to function!");

    return NIBOOL_IS_TRUE(pmComponent->GetNiEntityComponentInterface()
        ->CanDetachFromEntity(m_pkEntity));

}
//---------------------------------------------------------------------------
void MEntity::AddComponent(MComponent* pmComponent,
    bool bPerformErrorChecking, bool bUndoable)
{
    MVerifyValidInstance;
    m_bDirtyBit = true;

    MAssert(pmComponent != NULL, "Null component provided to function!");

    CommandService->ExecuteCommand(new MAddRemoveComponentCommand(
        NiNew NiAddRemoveComponentCommand(m_pkEntity, 
        pmComponent->GetNiEntityComponentInterface(), true,
        bPerformErrorChecking)), bUndoable);

}
//---------------------------------------------------------------------------
void MEntity::RemoveComponent(MComponent* pmComponent,
    bool bPerformErrorChecking, bool bUndoable)
{
    MVerifyValidInstance;
    m_bDirtyBit = true;

    MAssert(pmComponent != NULL, "Null component provided to function!");

    CommandService->ExecuteCommand(new MAddRemoveComponentCommand(
        NiNew NiAddRemoveComponentCommand(m_pkEntity, 
        pmComponent->GetNiEntityComponentInterface(), false,
        bPerformErrorChecking)), bUndoable);

}
//---------------------------------------------------------------------------
String* MEntity::GetPropertyNames()[]
{
    MVerifyValidInstance;

    NIASSERT(m_pkPropertyNames->GetSize() == 0);
    m_pkEntity->GetPropertyNames(*m_pkPropertyNames);

    String* astrPropertyNames[] = new String*[m_pkPropertyNames->GetSize()];
    for (unsigned int ui = 0; ui < m_pkPropertyNames->GetSize(); ui++)
    {
        astrPropertyNames[ui] = m_pkPropertyNames->GetAt(ui);
    }

    m_pkPropertyNames->RemoveAll();

    return astrPropertyNames;
}
//---------------------------------------------------------------------------
NiAVObject* MEntity::GetSceneRootPointer(unsigned int uiIndex)
{
    MVerifyValidInstance;

    if (uiIndex >= GetSceneRootPointerCount())
    {
        return NULL;
    }

    NiObject* pkSceneRootPointer = NULL;
    NiAVObject* pkSceneRoot = NULL;
    if (m_pkEntity->GetPropertyData(gs_pcSceneRootPointerName,
        pkSceneRootPointer, uiIndex))
    {
        pkSceneRoot = NiDynamicCast(NiAVObject, pkSceneRootPointer);
    }

    return pkSceneRoot;
}
//---------------------------------------------------------------------------
unsigned int MEntity::GetSceneRootPointerCount()
{
    MVerifyValidInstance;

    unsigned int uiCount;
    if (!m_pkEntity->GetElementCount(gs_pcSceneRootPointerName, uiCount))
    {
        uiCount = 0;
    }

    return uiCount;
}
//---------------------------------------------------------------------------
bool MEntity::get_Hidden()
{
    MVerifyValidInstance;

    return NIBOOL_IS_TRUE(m_pkEntity->GetHidden());
}
//---------------------------------------------------------------------------
void MEntity::set_Hidden(bool bHidden)
{
    MVerifyValidInstance;

    SetHidden(bHidden, true);
}
//---------------------------------------------------------------------------
void MEntity::SetHidden(bool bHidden, bool bUndoable)
{
    MVerifyValidInstance;

    m_bDirtyBit = true;

    if (this->Hidden != bHidden)
    {
        CommandService->ExecuteCommand(new MChangeHiddenStateCommand(
            NiNew NiChangeHiddenStateCommand(m_pkEntity, bHidden)),
            bUndoable);
    }
}
//---------------------------------------------------------------------------
bool MEntity::get_Frozen()
{
    MVerifyValidInstance;

    return m_bFrozen;
}
//---------------------------------------------------------------------------
void MEntity::set_Frozen(bool bFrozen)
{
    MVerifyValidInstance;


    SetFrozen(bFrozen, true);
}
//---------------------------------------------------------------------------
void MEntity::SetFrozen(bool bFrozen, bool bUndoable)
{
    MVerifyValidInstance;

    m_bDirtyBit = true;

    if (m_bFrozen != bFrozen)
    {
        CommandService->ExecuteCommand(new MChangeFrozenStateCommand(this,
            bFrozen), bUndoable);
    }
}
//---------------------------------------------------------------------------
bool MEntity::get_Dirty()
{
    MVerifyValidInstance;

    bool bDirty = __super::get_Dirty();
    //This may not be needed...
    if (!bDirty && MasterEntity != NULL)
    {
        bDirty = bDirty || MasterEntity->Dirty;
    }

    return bDirty;
}
//---------------------------------------------------------------------------
void MEntity::set_Dirty(bool bDirty)
{
    MVerifyValidInstance;

    m_bDirtyBit = bDirty;
}
//---------------------------------------------------------------------------
NiEntityInterface* MEntity::GetNiEntityInterface()
{
    MVerifyValidInstance;

    return m_pkEntity;
}
//---------------------------------------------------------------------------
void MEntity::Update(float fTime, NiExternalAssetManager* pkAssetManager)
{
    MVerifyValidInstance;

    // Create error handler.
    NiDefaultErrorHandlerPtr spErrors = NiNew NiDefaultErrorHandler();
    pkAssetManager->SetErrorHandler(spErrors);

    // Update the entity.
    m_pkEntity->Update(NULL, fTime, spErrors, pkAssetManager);

    // Report errors.
    MUtility::AddErrorInterfaceMessages(MessageChannelType::Errors, spErrors);
}
//---------------------------------------------------------------------------
IMessageService* MEntity::get_MessageService()
{
    if (ms_pmMessageService == NULL)
    {
        ms_pmMessageService = MGetService(IMessageService);
        MAssert(ms_pmMessageService != NULL, "Message service not found!");
    }
    return ms_pmMessageService;
}
//---------------------------------------------------------------------------
PropertyDescriptorCollection* MEntity::GetProperties()
{
    MVerifyValidInstance;

    return GetProperties(new Attribute*[0]);
}
//---------------------------------------------------------------------------
PropertyDescriptorCollection* MEntity::GetProperties(
    Attribute* amAttributes[])
{
    MVerifyValidInstance;

    ArrayList* pmPropertyDescs = new ArrayList();

    String* strGeneralCategory = "General";

    // Name.
    Attribute* amEntityAttributes[] = new Attribute*[3];
    amEntityAttributes[0] = new CategoryAttribute(strGeneralCategory);
    amEntityAttributes[1] = new DescriptionAttribute(
        "The name of the entity.");
    amEntityAttributes[2] = new MergablePropertyAttribute(false);
    pmPropertyDescs->Add(new EntityDescriptor(this,
        EntityDescriptor::ValueType::Name, "Name", amEntityAttributes));

    // Entity template.
    amEntityAttributes = new Attribute*[2];
    amEntityAttributes[0] = new CategoryAttribute(strGeneralCategory);
    amEntityAttributes[1] = new DescriptionAttribute("The entity template "
        "from which this entity inherits its properties.");
    pmPropertyDescs->Add(new EntityDescriptor(this,
        EntityDescriptor::ValueType::MasterEntity, "Entity Template",
        amEntityAttributes));

    // Hidden.
    amEntityAttributes = new Attribute* [2];
    amEntityAttributes[0] = new CategoryAttribute(strGeneralCategory);
    amEntityAttributes[1] = new DescriptionAttribute("Indicates whether or "
        "not the entity is hidden. Hidden entities are not displayed in the "
        "tool.");
    pmPropertyDescs->Add(new EntityDescriptor(this,
        EntityDescriptor::ValueType::Hidden, "Hidden", amEntityAttributes));

    // Frozen.
    amEntityAttributes = new Attribute* [2];
    amEntityAttributes[0] = new CategoryAttribute(strGeneralCategory);
    amEntityAttributes[1] = new DescriptionAttribute("Indicates whether or "
        "not the entity is frozen. Frozen entities cannot be edited in the "
        "tool.");
    pmPropertyDescs->Add(new EntityDescriptor(this,
        EntityDescriptor::ValueType::Frozen, "Frozen", amEntityAttributes));

    PropertyDescriptorCollection* pmBaseProperties =
        __super::GetProperties(amAttributes);

    pmPropertyDescs->AddRange(pmBaseProperties);

    PropertyDescriptor* amPropertyDescArray[] =
        dynamic_cast<PropertyDescriptor*[]>(pmPropertyDescs->ToArray(
        __typeof(PropertyDescriptor)));
    return new PropertyDescriptorCollection(amPropertyDescArray);
}
//---------------------------------------------------------------------------
// EntityDescriptor implementation.
//---------------------------------------------------------------------------
MEntity::EntityDescriptor::EntityDescriptor(MEntity* pmEntity,
    ValueType eValueType, String* strName, Attribute* amAttributes[]) :
    PropertyDescriptor(strName, amAttributes), m_pmEntity(pmEntity),
    m_eValueType(eValueType)
{
    MAssert(m_pmEntity != NULL, "MEntity::EntityDescriptor Error: "
        "Null entity provided to constructor.");
}
//---------------------------------------------------------------------------
Type* MEntity::EntityDescriptor::get_ComponentType()
{
    return m_pmEntity->GetType();
}
//---------------------------------------------------------------------------
bool MEntity::EntityDescriptor::get_IsReadOnly()
{
    switch (m_eValueType)
    {
        case ValueType::Name:
        case ValueType::Hidden:
        case ValueType::Frozen:
            if (MScene::FindSceneContainingEntity(m_pmEntity) ==
                MFramework::Instance->Scene)
            {
                return false;
            }
            else
            {
                return true;
            }
        case ValueType::MasterEntity:
            return true;
        default:
            return true;
    }
}
//---------------------------------------------------------------------------
Type* MEntity::EntityDescriptor::get_PropertyType()
{
    switch (m_eValueType)
    {
        case ValueType::Name:
            return m_pmEntity->Name->GetType();
        case ValueType::MasterEntity:
            return __typeof(String);
        case ValueType::Hidden:
            return __box(m_pmEntity->Hidden)->GetType();
        case ValueType::Frozen:
            return __box(m_pmEntity->Frozen)->GetType();
        default:
            return NULL;
    }
}
//---------------------------------------------------------------------------
bool MEntity::EntityDescriptor::CanResetValue(Object* pmComponent)
{
    return false;
}
//---------------------------------------------------------------------------
Object* MEntity::EntityDescriptor::GetValue(Object* pmComponent)
{
    switch (m_eValueType)
    {
        case ValueType::Name:
            return m_pmEntity->Name;
        case ValueType::MasterEntity:
            if (m_pmEntity->MasterEntity != NULL)
            {
                IEntityPathService* pmPathService = 
                    MGetService(IEntityPathService);
                String* strFullPath = 
                    pmPathService->FindFullPath(m_pmEntity->MasterEntity);

                return strFullPath;
            }
            else
            {
                return NULL;
            }
        case ValueType::Hidden:
            return __box(m_pmEntity->Hidden);
        case ValueType::Frozen:
            return __box(m_pmEntity->Frozen);
        default:
            return NULL;
    }
}
//---------------------------------------------------------------------------
void MEntity::EntityDescriptor::ResetValue(Object* pmComponent)
{
}
//---------------------------------------------------------------------------
void MEntity::EntityDescriptor::SetValue(Object* pmComponent, Object* pmValue)
{
    switch (m_eValueType)
    {
        case ValueType::Name:
            m_pmEntity->Name = dynamic_cast<String*>(pmValue);
            break;
        case ValueType::Hidden:
            m_pmEntity->Hidden = *dynamic_cast<__box bool*>(pmValue);
            break;
        case ValueType::Frozen:
            m_pmEntity->Frozen = *dynamic_cast<__box bool*>(pmValue);
            break;
        default:
            break;
    }
}
//---------------------------------------------------------------------------
bool MEntity::EntityDescriptor::ShouldSerializeValue(Object* pmComponent)
{
    return true;
}
//---------------------------------------------------------------------------
