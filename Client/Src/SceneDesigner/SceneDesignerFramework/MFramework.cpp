// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "SceneDesignerFrameworkPCH.h"

#include "MFramework.h"
#include "NiLightProxyComponent.h"
#include "ServiceProvider.h"
#include "MPoint3.h"
#include "MUtility.h"
#include "MFolderLocation.h"
#include <NiMemTracker.h>
#include <NiStandardAllocator.h>
#include "Map.h"

//The following is related to a bug in VC7.1/.Net CLR 1.1
// see http://support.microsoft.com/?id=814472
#if _MSC_VER == 1310
#pragma unmanaged
#include <windows.h>
#include <_vcclrit.h>
#pragma managed
#endif

using namespace System::Windows::Forms;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
//---------------------------------------------------------------------------
void MFramework::Init()
{
#if _MSC_VER == 1310
    __crt_dll_initialize();
#endif
    if (ms_pmThis == NULL)
    {
        ms_pmThis = new MFramework();
    }
}
//---------------------------------------------------------------------------
void MFramework::Shutdown()
{
    if (ms_pmThis != NULL)
    {
        ms_pmThis->Dispose();
        ms_pmThis = NULL;
    }
#if _MSC_VER == 1310
    __crt_dll_terminate();
#endif
}
//---------------------------------------------------------------------------
bool MFramework::InstanceIsValid()
{
    return (ms_pmThis != NULL);
}
//---------------------------------------------------------------------------
MFramework* MFramework::get_Instance()
{
    return ms_pmThis;
}
//---------------------------------------------------------------------------
MFramework::MFramework() : m_strImageSubfolder(String::Empty),
    m_bDoNotUpdate(false), m_bInitSuccessful(false), m_uiLongOperationCount(0),
    m_strCurrentFilename(String::Empty)
{
    NiInitOptions* pkInitOptions = NiExternalNew NiInitOptions(
#ifdef NI_MEMORY_DEBUGGER
        NiExternalNew NiMemTracker(NiExternalNew NiStandardAllocator(), false)
#else
        NiExternalNew NiStandardAllocator()
#endif
        );

    NiInit(pkInitOptions);
    m_bInitSuccessful = true;

    // Initialize the shadow manager. It is initially set to be inactive.
    // Rendering modes that draw shadows are responsible for activating it
    // and then deactivating when done. Rendering modes that do not draw
    // shadows need not take any action.
    NiShadowManager::Initialize();
    NiShadowManager::SetActive(false);
    NiShadowManager::SetCullingProcess(NiNew NiCullingProcess(NULL));

    NiProxyComponent::_SDMInit();
    NiLightProxyComponent::_SDMInit();
    MViewport::_SDMInit();
    MEntity::_SDMInit();

    MSceneFactory::Init();
    MEntityFactory::Init();
    MSelectionSetFactory::Init();
    MComponentFactory::Init();

    MEventManager::Init();
    MRenderer::Init();
    MTimeManager::Init();
    MCameraManager::Init();
    MViewportManager::Init();
    MPaletteManager::Init();
    MProxyManager::Init();
    MLightManager::Init();
    MBoundManager::Init();
	//
	MCollisionManager::Init();

    // Ensure that all registered assets clone from a pristine copy
    NiTFactory<NiExternalAssetHandler*>* pkAssetFactory = 
        NiFactories::GetAssetFactory();
    NiTMapIterator kIter = pkAssetFactory->GetFirstPos();
    while (kIter)
    {
        const char* pcKey = NULL;
        pkAssetFactory->GetNext(kIter, pcKey);

        NiExternalAssetHandler* pkHandler = 
            pkAssetFactory->GetPersistent(pcKey);

        NIVERIFY(pkHandler->SetCloneFromPristine(true));
    }

    m_pmPickUtility = new MPickUtility();

    m_pkAssetManager = NiNew NiExternalAssetManager(
        NiFactories::GetAssetFactory());

	CMap::SetAssetManager(m_pkAssetManager);

    // To make clear that error handling, if used, will be set explicitly.
    m_pkAssetManager->SetErrorHandler(NULL);

    MInitRefObject(m_pkAssetManager);

    MScene* pmScene = NewScene(10);
    InitNewScene(pmScene, false);

    this->TimeManager->Enabled = true;
}
//---------------------------------------------------------------------------
void MFramework::Do_Dispose(bool bDisposing)
{
    if (!m_bInitSuccessful)
    {
        return;
    }

    MDisposeRefObject(m_pkAssetManager);

    if (bDisposing)
    {
        MProxyManager::Shutdown();

        if (m_pmPickUtility != NULL)
        {
            m_pmPickUtility->Dispose();
            m_pmPickUtility = NULL;
        }

        MLightManager::Shutdown();
        MPaletteManager::Shutdown();
        MViewportManager::Shutdown();
        MCameraManager::Shutdown();
        MTimeManager::Shutdown();
        MBoundManager::Shutdown();
		//
		MCollisionManager::Shutdown();

        // The factories are shut down after the managers because some of the
        // managers access them in their Dispose functions. The factories must
        // also be shutdown in the order listed here because some factories
        // depend on other factories.
        MSceneFactory::Shutdown();
        MEntityFactory::Shutdown();
        MSelectionSetFactory::Shutdown();
        MComponentFactory::Shutdown();

        // The shadow manager must be shut down prior to the renderer being
        // destroyed.
        NiShadowManager::Shutdown();

        MRenderer::Shutdown();
        MEventManager::Shutdown();
    }

    NiLightProxyComponent::_SDMShutdown();
    NiProxyComponent::_SDMShutdown();
    MEntity::_SDMShutdown();
    MViewport::_SDMShutdown();

    const NiInitOptions* pkInitOptions = NiStaticDataManager
        ::GetInitOptions();
    NiShutdown();
    NiAllocator* pkAllocator = pkInitOptions->GetAllocator();
    NiExternalDelete pkInitOptions;
    NiExternalDelete pkAllocator;
}
//---------------------------------------------------------------------------
void MFramework::Startup()
{
    MVerifyValidInstance;

    IUICommandService* pmCommand = MGetService(IUICommandService);
    pmCommand->BindCommands(MViewportManager::Instance);
    pmCommand->BindCommands(MCameraManager::Instance);

    this->ProxyManager->Startup();
    this->LightManager->Startup();
    this->CameraManager->Startup();
    this->ViewportManager->Startup();
    this->BoundManager->Startup();

    RegisterSettingsAndOptions();
}
//---------------------------------------------------------------------------
void MFramework::RegisterSettingsAndOptions()
{
    MVerifyValidInstance;

    SettingsService->RegisterSettingsObject(ms_strImageSubfolderSettingName,
        new MFolderLocation(m_strImageSubfolder), SettingsCategory::PerScene);
    SettingsService->SetChangedSettingHandler(ms_strImageSubfolderSettingName,
        SettingsCategory::PerScene, new SettingChangedHandler(this,
        &MFramework::OnImageSubfolderSettingChanged));
    OnImageSubfolderSettingChanged(NULL, NULL);

    OptionsService->AddOption(ms_strImageSubfolderOptionName,
        SettingsCategory::PerScene, ms_strImageSubfolderSettingName);
    OptionsService->SetHelpDescription(ms_strImageSubfolderOptionName,
        "The directory to use when loading external images referenced by NIF "
        "files. A blank path indicates that the same directory as the NIF "
        "file should be used.\n\nNote: this option is stored per scene and "
        "does not take effect until the scene is saved and reloaded.");
}
//---------------------------------------------------------------------------
void MFramework::OnImageSubfolderSettingChanged(Object* pmSender,
    SettingChangedEventArgs* pmEventArgs)
{
    MVerifyValidInstance;

    MFolderLocation* pmImageSubfolder = dynamic_cast<MFolderLocation*>(
        SettingsService->GetSettingsObject(ms_strImageSubfolderSettingName,
        SettingsCategory::PerScene));
    if (pmImageSubfolder != NULL)
    {
        // Store new image subfolder.
        m_strImageSubfolder = pmImageSubfolder->Path;

        // Set platform-specific subdirectory with new image subfolder.
        const char* pcImageSubfolder = MStringToCharPointer(
            m_strImageSubfolder);
        NiDevImageConverter::SetPlatformSpecificSubdirectory(
            pcImageSubfolder);
        MFreeCharPointer(pcImageSubfolder);
    }
}
//---------------------------------------------------------------------------
MSceneFactory* MFramework::get_SceneFactory()
{
    MVerifyValidInstance;

    return MSceneFactory::Instance;
}
//---------------------------------------------------------------------------
MEntityFactory* MFramework::get_EntityFactory()
{
    MVerifyValidInstance;

    return MEntityFactory::Instance;
}
//---------------------------------------------------------------------------
MSelectionSetFactory* MFramework::get_SelectionSetFactory()
{
    MVerifyValidInstance;

    return MSelectionSetFactory::Instance;
}
//---------------------------------------------------------------------------
MComponentFactory* MFramework::get_ComponentFactory()
{
    MVerifyValidInstance;

    return MComponentFactory::Instance;
}
//---------------------------------------------------------------------------
MRenderer* MFramework::get_Renderer()
{
    MVerifyValidInstance;

    return MRenderer::Instance;
}
//---------------------------------------------------------------------------
MEventManager* MFramework::get_EventManager()
{
    MVerifyValidInstance;

    return MEventManager::Instance;
}
//---------------------------------------------------------------------------
MTimeManager* MFramework::get_TimeManager()
{
    MVerifyValidInstance;

    return MTimeManager::Instance;
}
//---------------------------------------------------------------------------
MCameraManager* MFramework::get_CameraManager()
{
    MVerifyValidInstance;

    return MCameraManager::Instance;
}
//---------------------------------------------------------------------------
MViewportManager* MFramework::get_ViewportManager()
{
    MVerifyValidInstance;

    return MViewportManager::Instance;
}
//---------------------------------------------------------------------------
MPaletteManager* MFramework::get_PaletteManager()
{
    MVerifyValidInstance;

    return MPaletteManager::Instance;
}
//---------------------------------------------------------------------------
MProxyManager* MFramework::get_ProxyManager()
{
    MVerifyValidInstance;

    return MProxyManager::Instance;
}
//---------------------------------------------------------------------------
MLightManager* MFramework::get_LightManager()
{
    MVerifyValidInstance;

    return MLightManager::Instance;
}
//---------------------------------------------------------------------------
MBoundManager* MFramework::get_BoundManager()
{
    MVerifyValidInstance;

    return MBoundManager::Instance;
}
//
//---------------------------------------------------------------------------
MCollisionManager* MFramework::get_CollisionManager()
{
	MVerifyValidInstance;

	return MCollisionManager::Instance;
}
//---------------------------------------------------------------------------
String* MFramework::get_AppStartupPath()
{
    MVerifyValidInstance;

    return String::Format(S"{0}{1}",Application::StartupPath, S"\\");
}
//---------------------------------------------------------------------------
MScene* MFramework::get_Scene()
{
    MVerifyValidInstance;

    return m_pmScene;
}
//---------------------------------------------------------------------------
MPickUtility* MFramework::get_PickUtility()
{
    MVerifyValidInstance;

    return m_pmPickUtility;
}
//---------------------------------------------------------------------------
NiExternalAssetManager* MFramework::get_ExternalAssetManager()
{
    MVerifyValidInstance;

    return m_pkAssetManager;
}
//---------------------------------------------------------------------------
String* MFramework::get_ApplicationName()
{
    MVerifyValidInstance;

    return Application::ProductName;
}
//---------------------------------------------------------------------------
String* MFramework::get_ImageSubfolder()
{
    MVerifyValidInstance;

    return m_strImageSubfolder;
}
//---------------------------------------------------------------------------
MScene* MFramework::NewScene(unsigned int uiEntityArraySize)
{
    MVerifyValidInstance;

    return this->SceneFactory->Get(NiNew NiScene("Main Scene",
        uiEntityArraySize));
}
//---------------------------------------------------------------------------
MScene* MFramework::LoadScene(String* strFilename, String* strFormat)
{
    MVerifyValidInstance;

    // Create the appropriate file format handler based on strFormat
    const char* pcFormat = MStringToCharPointer(strFormat);
    NiEntityStreaming* pkEntityStreaming =
        NiFactories::GetStreamingFactory()->GetPersistent(pcFormat);
    MFreeCharPointer(pcFormat);

    // Create error handler.
    NiDefaultErrorHandlerPtr spErrors = NiNew NiDefaultErrorHandler();
    pkEntityStreaming->SetErrorHandler(spErrors);

    // Load the scene
    const char* pcFilename = MStringToCharPointer(strFilename);
    NiBool bSuccess = pkEntityStreaming->Load(pcFilename);
    MFreeCharPointer(pcFilename);

    // Report errors.
    MUtility::AddErrorInterfaceMessages(MessageChannelType::Errors, spErrors);
    pkEntityStreaming->SetErrorHandler(NULL);

    if (!bSuccess)
    {
        return NULL;
    }
    
    NIASSERT(pkEntityStreaming->GetSceneCount() == 1);
    MScene* pmScene = this->SceneFactory->Get(pkEntityStreaming->GetSceneAt(
        0));

    pkEntityStreaming->RemoveAllScenes(); // Because static instance

    return pmScene;
}
//---------------------------------------------------------------------------
void MFramework::InitNewScene(MScene* pmNewScene)
{
    MVerifyValidInstance;

    InitNewScene(pmNewScene, true);
}
//---------------------------------------------------------------------------
void MFramework::InitNewScene(MScene* pmNewScene, bool bRaiseEvent)
{
    MVerifyValidInstance;

    MAssert(pmNewScene != NULL, "Null scene provided to function!");

    m_bDoNotUpdate = true;
    BeginLongOperation();

    if (m_pmScene != NULL)
    {
        this->EventManager->RaiseSceneClosing(m_pmScene);
        MSceneFactory::Instance->Remove(m_pmScene);
        m_pmScene = NULL;
    }
    m_pkAssetManager->RemoveAll();
    this->TimeManager->ResetTime(0.0f);

    m_pmScene = pmNewScene;
    m_pmScene->Update(TimeManager->CurrentTime, m_pkAssetManager);

    if (bRaiseEvent)
    {
        this->EventManager->RaiseNewSceneLoaded(m_pmScene);
    }

    EndLongOperation();
    m_bDoNotUpdate = false;
}
//---------------------------------------------------------------------------
bool MFramework::SaveScene(String* strFilename, String* strFormat)
{
    MVerifyValidInstance;

    if (!m_pmScene)
        return false;

    if (!m_pmScene->GetNiScene())
        return false;

    m_bDoNotUpdate = true;

    // Create the appropriate file format handler based on strFormat
    const char* pcFormat = MStringToCharPointer(strFormat);
    NiEntityStreaming* pkEntityStreaming =
        NiFactories::GetStreamingFactory()->GetPersistent(pcFormat);
    MFreeCharPointer(pcFormat);

    // Create error handler.
    NiDefaultErrorHandlerPtr spErrors = NiNew NiDefaultErrorHandler();
    pkEntityStreaming->SetErrorHandler(spErrors);

    // Insert desired scenes
    pkEntityStreaming->InsertScene(m_pmScene->GetNiScene());

    // Save the scene
    const char* pcFilename = MStringToCharPointer(strFilename);
    bool bSuccess = NIBOOL_IS_TRUE(pkEntityStreaming->Save(pcFilename));
    MFreeCharPointer(pcFilename);
    pkEntityStreaming->RemoveAllScenes(); // Because static instance

    // Report errors.
    MUtility::AddErrorInterfaceMessages(MessageChannelType::Errors, spErrors);
    pkEntityStreaming->SetErrorHandler(NULL);

    m_bDoNotUpdate = false;

    return bSuccess;
}
//---------------------------------------------------------------------------
void MFramework::Update()
{
    MVerifyValidInstance;

    if (m_bDoNotUpdate)
    {
        return;
    }

    m_bDoNotUpdate = true;

    this->TimeManager->UpdateTime();
    PerformUpdate();
    this->Renderer->Render();

    m_bDoNotUpdate = false;
}
//---------------------------------------------------------------------------
void MFramework::PerformUpdate()
{
    MVerifyValidInstance;

    // Continuous time never pauses and is used to update UI elements. Current
    // time is used to update the main scene.
    float fContinuousTime = this->TimeManager->ContinuousTime;
    float fCurrentTime = this->TimeManager->CurrentTime;

	CollisionManager->Update(fCurrentTime);
    InteractionModeService->Update(fContinuousTime);
    LightManager->Update(fContinuousTime);
    CameraManager->Update(fContinuousTime);

    if (m_pmScene != NULL)
    {
        m_pmScene->Update(fCurrentTime, m_pkAssetManager);
    }
    if (this->ProxyManager->ProxyScene != NULL)
    {
        this->ProxyManager->ProxyScene->Update(fContinuousTime,
            m_pkAssetManager);
    }
    for (unsigned int ui = 0; ui < this->ViewportManager->ViewportCount; ui++)
    {
        this->ViewportManager->GetViewport(ui)->ToolScene->Update(
            fContinuousTime, m_pkAssetManager);
    }

    BoundManager->Update(fContinuousTime);
}
//---------------------------------------------------------------------------
bool MFramework::get_PerformingLongOperation()
{
    MVerifyValidInstance;

    return (m_uiLongOperationCount > 0);
}
//---------------------------------------------------------------------------
void MFramework::BeginLongOperation()
{
    MVerifyValidInstance;

    if (m_uiLongOperationCount == 0)
    {
        this->EventManager->RaiseLongOperationStarted();
    }
    m_uiLongOperationCount++;
}
//---------------------------------------------------------------------------
void MFramework::EndLongOperation()
{
    MVerifyValidInstance;

    if (m_uiLongOperationCount > 0)
    {
        m_uiLongOperationCount--;

        if (m_uiLongOperationCount == 0)
        {
            this->EventManager->RaiseLongOperationCompleted();
        }
    }
}
//---------------------------------------------------------------------------
void MFramework::RestartAnimation()
{
    MEntity* pmEntities[] = m_pmScene->GetEntities();
    int iEntityCount = pmEntities->Count;
    NiFixedString kActorClassName = "NiActorComponent";
    for (int iEntityIndex = 0; iEntityIndex < iEntityCount; iEntityIndex++)
    {
        MComponent* pmComponents[] = pmEntities[iEntityIndex]->GetComponents();
        int iComponentCount = pmComponents->Count;
        for (int iComponentIndex = 0; iComponentIndex < iComponentCount; 
            iComponentIndex++)
        {
            MComponent* pmComponent = pmComponents[iComponentIndex];
            NiEntityComponentInterface* pkComponent = 
                pmComponent->GetNiEntityComponentInterface();
            NiFixedString kClassName = pkComponent->GetClassName();
            if (kClassName == kActorClassName)
            {
                NiActorComponent* pkActor = static_cast<NiActorComponent*>
                    (pkComponent);
                pkActor->ResetAnimation();
            }            
        }
    }
}
//---------------------------------------------------------------------------
String* MFramework::get_CurrentFilename()
{
    return m_strCurrentFilename;
}
//---------------------------------------------------------------------------
void MFramework::set_CurrentFilename(String* strCurrentFilename)
{
    if (strCurrentFilename == NULL)
    {
        strCurrentFilename = String::Empty;
    }
    m_strCurrentFilename = strCurrentFilename;
}
//---------------------------------------------------------------------------
IInteractionModeService* MFramework::get_InteractionModeService()
{
    if (ms_pmInteractionModeService == NULL)
    {
        ms_pmInteractionModeService = MGetService(IInteractionModeService);
        MAssert(ms_pmInteractionModeService != NULL, "Interaction mode "
            "service not found!");
    }
    return ms_pmInteractionModeService;
}
//---------------------------------------------------------------------------
ISelectionService* MFramework::get_SelectionService()
{
    if (ms_pmSelectionService == NULL)
    {
        ms_pmSelectionService = MGetService(ISelectionService);
        MAssert(ms_pmSelectionService != NULL, "Selection service not "
            "found!");
    }
    return ms_pmSelectionService;
}
//---------------------------------------------------------------------------
ISettingsService* MFramework::get_SettingsService()
{
    if (ms_pmSettingsService == NULL)
    {
        ms_pmSettingsService = MGetService(ISettingsService);
        MAssert(ms_pmSettingsService != NULL, "Settings service not "
            "found!");
    }
    return ms_pmSettingsService;
}
//---------------------------------------------------------------------------
IOptionsService* MFramework::get_OptionsService()
{
    if (ms_pmOptionsService == NULL)
    {
        ms_pmOptionsService = MGetService(IOptionsService);
        MAssert(ms_pmOptionsService != NULL, "Options service not "
            "found!");
    }
    return ms_pmOptionsService;
}
//---------------------------------------------------------------------------
bool MFramework::get_SceneIsDirty()
{
	CMap* pkMap = CMap::Get();
	if( pkMap )
		return pkMap->IsDirty();

	return false;
}