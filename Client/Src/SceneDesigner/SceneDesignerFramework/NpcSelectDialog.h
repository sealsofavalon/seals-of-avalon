
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner {

	namespace Framework 
	{

		public __gc class NpcSelectDialog : public System::Windows::Forms::Form
		{
		private: System::Windows::Forms::Label*  m_NameLabel;
		private: System::Windows::Forms::Button*  m_NameButton;
		private: System::Windows::Forms::Label*  m_IDLabel;
		private: System::Windows::Forms::Button*  m_IDButton;
		private: System::Windows::Forms::TextBox*  m_IDTextBox;
		private: System::Windows::Forms::TextBox*  m_NameTextBox;

		private:
			NameInfo* m_pkNameInfo;
			NiFixedString* m_pkModelName;

		public:
			NpcSelectDialog()
			{
				m_pkNameInfo = NULL;
				m_pkModelName = NULL;
				this->InitializeComponent();
			}

			void Do_Dispose(bool bDisposing)
			{
			}

			NameInfo* GetNameInfo() { return m_pkNameInfo; }
			NiFixedString* GetModelName() { return m_pkModelName; }

		private: 
			System::Void InitializeComponent() 
			{
                this->m_NameLabel = (new System::Windows::Forms::Label());
                this->m_NameButton = (new System::Windows::Forms::Button());
                this->m_NameTextBox = (new System::Windows::Forms::TextBox());
                this->m_IDLabel = (new System::Windows::Forms::Label());
                this->m_IDButton = (new System::Windows::Forms::Button());
                this->m_IDTextBox = (new System::Windows::Forms::TextBox());
                this->SuspendLayout();
                // 
                // m_NameLabel
                // 
                this->m_NameLabel->AutoSize = true;
                this->m_NameLabel->Location = System::Drawing::Point(14, 28);
                this->m_NameLabel->Name = S"m_NameLabel";
                this->m_NameLabel->Size = System::Drawing::Size(29, 12);
                this->m_NameLabel->TabIndex = 0;
                this->m_NameLabel->Text = S"名字";
                // 
                // m_NameButton
                // 
                this->m_NameButton->Location = System::Drawing::Point(243, 23);
                this->m_NameButton->Name = S"m_NameButton";
                this->m_NameButton->Size = System::Drawing::Size(94, 23);
                this->m_NameButton->TabIndex = 1;
                this->m_NameButton->Text = S"通过名字选择";
                this->m_NameButton->UseVisualStyleBackColor = true;
                this->m_NameButton->Click += new System::EventHandler(this, &NpcSelectDialog::OnNameButtonClick);
                // 
                // m_NameTextBox
                // 
                this->m_NameTextBox->Location = System::Drawing::Point(57, 24);
                this->m_NameTextBox->Name = S"m_NameTextBox";
                this->m_NameTextBox->Size = System::Drawing::Size(172, 21);
                this->m_NameTextBox->TabIndex = 2;
                // 
                // m_IDLabel
                // 
                this->m_IDLabel->AutoSize = true;
                this->m_IDLabel->Location = System::Drawing::Point(14, 70);
                this->m_IDLabel->Name = S"m_IDLabel";
                this->m_IDLabel->Size = System::Drawing::Size(17, 12);
                this->m_IDLabel->TabIndex = 0;
                this->m_IDLabel->Text = S"ID";
                // 
                // m_IDButton
                // 
                this->m_IDButton->Location = System::Drawing::Point(243, 65);
                this->m_IDButton->Name = S"m_IDButton";
                this->m_IDButton->Size = System::Drawing::Size(94, 23);
                this->m_IDButton->TabIndex = 1;
                this->m_IDButton->Text = S"通过ID选择";
                this->m_IDButton->UseVisualStyleBackColor = true;
                this->m_IDButton->Click += new System::EventHandler(this, &NpcSelectDialog::OnIDButtonClick);
                // 
                // m_IDTextBox
                // 
                this->m_IDTextBox->Location = System::Drawing::Point(57, 66);
                this->m_IDTextBox->Name = S"m_IDTextBox";
                this->m_IDTextBox->Size = System::Drawing::Size(172, 21);
                this->m_IDTextBox->TabIndex = 2;
                // 
                // NpcSelectDialog
                // 
                this->ClientSize = System::Drawing::Size(350, 114);
                this->Controls->Add(this->m_IDTextBox);
                this->Controls->Add(this->m_IDButton);
                this->Controls->Add(this->m_NameTextBox);
                this->Controls->Add(this->m_IDLabel);
                this->Controls->Add(this->m_NameButton);
                this->Controls->Add(this->m_NameLabel);
                this->MaximizeBox = false;
                this->MinimizeBox = false;
                this->Name = S"NpcSelectDialog";
                this->Text = S"选择NPC";
                this->ResumeLayout(false);
                this->PerformLayout();

            }

		protected: 
			void Dispose(Boolean disposing)
			{
				__super::Dispose(disposing);
			}

		private: 
			System::Void OnIDButtonClick(System::Object*  sender, System::EventArgs*  e) 
			{
				const char* pcString = MStringToCharPointer(m_IDTextBox->Text);
				unsigned int uiEntryID = atoi(pcString);
				MFreeCharPointer(pcString);

				if( uiEntryID == 0 )
					return;

				NameInfo* pkNameInfo;
				NiFixedString* pkModelName;

				if( !g_pkCreatureNamesDB->GetNameInfo(uiEntryID, pkNameInfo) )
				{
					MessageBox::Show("没有找到NPC", "错误", MessageBoxButtons::OK, MessageBoxIcon::Error);
					return;
				}

				char acMessage[1024];
				if( !g_pkDisplayInfoDB->GetModelName(pkNameInfo->m_uiDisplayID, pkModelName) )
				{
					NiSprintf(acMessage, sizeof(acMessage), "没有找到NPC 模型名称\nDisplay ID %d", pkNameInfo->m_uiDisplayID);
					MessageBox::Show(acMessage, "错误", MessageBoxButtons::OK, MessageBoxIcon::Error);
					return;
				}

				char acModelName[1024];
				NiSprintf(acModelName, sizeof(acModelName), "%s\\%s", TerrainHelper::GetClientPath(), (const char*)*pkModelName);
				if( !NiFile::Access(acModelName, NiFile::READ_ONLY) )
				{
					NiSprintf(acMessage, sizeof(acMessage), "没有找到NPC 模型文件\n%s", acModelName);
					MessageBox::Show(acMessage, "错误", MessageBoxButtons::OK, MessageBoxIcon::Error);
					return;
				}

				NiSprintf(acMessage, sizeof(acMessage), "名称 %s\nEntry ID %d\nDisplay ID %d\n路径 %s", 
					(const char*)pkNameInfo->m_kName, pkNameInfo->m_uiEntryID, pkNameInfo->m_uiDisplayID, (const char*)*pkModelName);
				if( MessageBox::Show(acMessage,  "添加NPC 模板", MessageBoxButtons::OKCancel) == DialogResult::Cancel )
					return;

				m_pkNameInfo = pkNameInfo;
				m_pkModelName = pkModelName;
				this->DialogResult = DialogResult::OK;
			}

		private: 
			System::Void OnNameButtonClick(System::Object*  sender, System::EventArgs*  e) 
			{
				const char* pcString = MStringToCharPointer(m_NameTextBox->Text);
				NiFixedString kString = pcString;
				MFreeCharPointer(pcString);

				if( kString.GetLength() == 0 )
					return;

				NameInfo* pkNameInfo;
				NiFixedString* pkModelName;

				if( !g_pkCreatureNamesDB->GetNameInfo(kString, pkNameInfo) )
				{
					MessageBox::Show("没有找到NPC", "错误", MessageBoxButtons::OK, MessageBoxIcon::Error);
					return;
				}

				char acMessage[1024];
				if( !g_pkDisplayInfoDB->GetModelName(pkNameInfo->m_uiDisplayID, pkModelName) )
				{
					NiSprintf(acMessage, sizeof(acMessage), "没有找到NPC对应的模型名称 \nDisplay ID %d", pkNameInfo->m_uiDisplayID);
					MessageBox::Show(acMessage, "错误", MessageBoxButtons::OK, MessageBoxIcon::Error);
					return;
				}

				char acModelName[1024];
				NiSprintf(acModelName, sizeof(acModelName), "%s\\%s", TerrainHelper::GetClientPath(), (const char*)*pkModelName);
				if( !NiFile::Access(acModelName, NiFile::READ_ONLY) )
				{
					NiSprintf(acMessage, sizeof(acMessage), "没有找到NPC 模型文件\n%s", acModelName);
					MessageBox::Show(acMessage, "错误", MessageBoxButtons::OK, MessageBoxIcon::Error);
					return;
				}

				NiSprintf(acMessage, sizeof(acMessage), "名称 %s\nEntry ID %d\nDisplay ID %d\n路径 %s", 
					(const char*)pkNameInfo->m_kName, pkNameInfo->m_uiEntryID, pkNameInfo->m_uiDisplayID, (const char*)*pkModelName);
				if( MessageBox::Show(acMessage,  "添加NPC 模板", MessageBoxButtons::OKCancel) == DialogResult::Cancel )
					return;

				m_pkNameInfo = pkNameInfo;
				m_pkModelName = pkModelName;
				this->DialogResult = DialogResult::OK;
			}
	};
 }
}}}