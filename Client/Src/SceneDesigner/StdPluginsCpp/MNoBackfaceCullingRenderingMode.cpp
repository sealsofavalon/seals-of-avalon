// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "StdPluginsCppPCH.h"

#include <NiD3DShaderFactory.h>
#include "MNoBackfaceCullingRenderingMode.h"

using namespace Emergent::Gamebryo::SceneDesigner::StdPluginsCpp;

//---------------------------------------------------------------------------
MNoBackfaceCullingRenderingMode::MNoBackfaceCullingRenderingMode() : 
    m_pkAlphaAccumulator(NULL),
    m_pkErrors(NULL),
    m_pkStencilProperty(NULL)
{
    m_pkAlphaAccumulator = NiNew NiAlphaAccumulator();
    MInitRefObject(m_pkAlphaAccumulator);

    m_pkErrors = NiNew NiDefaultErrorHandler();
    MInitInterfaceReference(m_pkErrors);

    m_pkStencilProperty = NiNew NiStencilProperty();
    m_pkStencilProperty->SetDrawMode(NiStencilProperty::DRAW_BOTH);
    MInitRefObject(m_pkStencilProperty);

    m_pkShadowRenderStep = NiNew NiDefaultClickRenderStep;
    MInitRefObject(m_pkShadowRenderStep);
}
//---------------------------------------------------------------------------
void MNoBackfaceCullingRenderingMode::Do_Dispose(bool bDisposing)
{
    MDisposeRefObject(m_pkShadowRenderStep);
    MDisposeRefObject(m_pkStencilProperty);
    MDisposeInterfaceReference(m_pkErrors);
    MDisposeRefObject(m_pkAlphaAccumulator);
}
//---------------------------------------------------------------------------
String* MNoBackfaceCullingRenderingMode::get_Name()
{
    MVerifyValidInstance;

    return "No Backface Culling";
}
//---------------------------------------------------------------------------
bool MNoBackfaceCullingRenderingMode::get_DisplayToUser()
{
    MVerifyValidInstance;

    return true;
}
//---------------------------------------------------------------------------
void MNoBackfaceCullingRenderingMode::Update(float fTime)
{
    MVerifyValidInstance;
}
//---------------------------------------------------------------------------
void MNoBackfaceCullingRenderingMode::Begin(
    MRenderingContext* pmRenderingContext)
{
    MVerifyValidInstance;

    MAssert(pmRenderingContext != NULL, "Null rendering context provided to "
        "function!");

    NiEntityRenderingContext* pkRenderingContext =
        pmRenderingContext->GetRenderingContext();

    // Clear out error handler.
    m_pkErrors->ClearErrors();

    // Clear out visible array.
    pkRenderingContext->m_pkCullingProcess->GetVisibleSet()->RemoveAll();
}
//---------------------------------------------------------------------------
void MNoBackfaceCullingRenderingMode::Render(MEntity* pmEntity,
    MRenderingContext* pmRenderingContext)
{
    MVerifyValidInstance;

    MAssert(pmEntity != NULL, "Null entity provided to function!");

    MEntity* amEntities[] = {pmEntity};
    Render(amEntities, pmRenderingContext);
}
//---------------------------------------------------------------------------
void MNoBackfaceCullingRenderingMode::Render(MEntity* amEntities[],
    MRenderingContext* pmRenderingContext)
{
    MVerifyValidInstance;

    MAssert(amEntities != NULL, "Null entity array provided to function!");
    MAssert(pmRenderingContext != NULL, "Null rendering context provided to "
        "function!");

    NiEntityRenderingContext* pkRenderingContext =
        pmRenderingContext->GetRenderingContext();

    for (int i = 0; i < amEntities->Length; i++)
    {
        MEntity* pmEntity = amEntities[i];
        MAssert(pmEntity != NULL, "Null entity in array!");

        pmEntity->GetNiEntityInterface()->BuildVisibleSet(pkRenderingContext,
            m_pkErrors);
    }
}
//---------------------------------------------------------------------------
void MNoBackfaceCullingRenderingMode::End(
    MRenderingContext* pmRenderingContext)
{
    MVerifyValidInstance;

    MAssert(pmRenderingContext != NULL, "Null rendering context provided to "
        "function!");

    NiEntityRenderingContext* pkRenderingContext =
        pmRenderingContext->GetRenderingContext();

    // Activate the shadow manager and inform it to retain its current
    // shadow maps.
    NiShadowManager::SetActive(true, true);

    // Backup current render target group.
    const NiRenderTargetGroup* pkCurRenderTarget = pkRenderingContext
        ->m_pkRenderer->GetCurrentRenderTargetGroup();
    MAssert(pkCurRenderTarget != NULL, "No active render target group!");
    pkRenderingContext->m_pkRenderer->EndUsingRenderTargetGroup();

    // Generate list of shadow render clicks.
    NiShadowManager::SetSceneCamera(pkRenderingContext->m_pkCamera);
    const NiTPointerList<NiRenderClick*>& kShadowClicks =
        NiShadowManager::GenerateRenderClicks();
    m_pkShadowRenderStep->GetRenderClickList().RemoveAll();
    NiTListIterator kIter = kShadowClicks.GetHeadPos();
    while (kIter)
    {
        m_pkShadowRenderStep->AppendRenderClick(kShadowClicks.GetNext(kIter));
    }

    // Render shadow maps.
    m_pkShadowRenderStep->Render();

    // Restore previous render target group.
    if (pkRenderingContext->m_pkRenderer->IsRenderTargetGroupActive())
    {
        pkRenderingContext->m_pkRenderer->EndUsingRenderTargetGroup();
    }
    pkRenderingContext->m_pkRenderer->BeginUsingRenderTargetGroup(
        (NiRenderTargetGroup*) pkCurRenderTarget, NiRenderer::CLEAR_NONE);

    // Set up the renderer's camera data.
    pkRenderingContext->m_pkRenderer->SetCameraData(
        pkRenderingContext->m_pkCamera);

    // Get visible set.
    NiVisibleArray* pkVisibleSet = pkRenderingContext->m_pkCullingProcess
        ->GetVisibleSet();
    NIASSERT(pkVisibleSet);

    // Set accumulator.
    NiAccumulatorPtr spOldAccumulator = pkRenderingContext->m_pkRenderer
        ->GetSorter();
    pkRenderingContext->m_pkRenderer->SetSorter(m_pkAlphaAccumulator);

    // Attach properties.
    NiTObjectArray<NiPropertyPtr> kOldProperties(pkVisibleSet->GetCount());
    for (unsigned int ui = 0; ui < pkVisibleSet->GetCount(); ui++)
    {
        NiPropertyState* pkPropertyState =
            pkVisibleSet->GetAt(ui).GetPropertyState();
        assert(pkPropertyState);
        kOldProperties.SetAt(ui, pkPropertyState->GetStencil());
        pkPropertyState->SetProperty(m_pkStencilProperty);
    }

    // Draw objects in the visible set.
    NiDrawVisibleArray(pkRenderingContext->m_pkCamera, *pkVisibleSet);

    // Detach properties.
    for (unsigned int ui = 0; ui < pkVisibleSet->GetCount(); ui++)
    {
        NiPropertyState* pkPropertyState =
            pkVisibleSet->GetAt(ui).GetPropertyState();
        assert(pkPropertyState);
        pkPropertyState->SetProperty(kOldProperties.GetAt(ui));
    }

    // Restore accumulator.
    pkRenderingContext->m_pkRenderer->SetSorter(spOldAccumulator);

    // Deactivate the shadow manager and inform it to retain its current
    // shadow maps.
    NiShadowManager::SetActive(false, true);

    // Report errors.
    MUtility::AddErrorInterfaceMessages(MessageChannelType::Errors,
        m_pkErrors);
}
//---------------------------------------------------------------------------
