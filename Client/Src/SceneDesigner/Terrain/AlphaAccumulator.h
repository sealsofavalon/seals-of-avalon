#ifndef ALPHA_ACCUMULATOR
#define ALPHA_ACCUMULATOR

class CAlphaAccumulator : public NiAlphaAccumulator
{
public:
    virtual void FinishAccumulating() {}
    void RealFinishAccumulating() { NiAlphaAccumulator::FinishAccumulating(); }
};

NiSmartPointer(CAlphaAccumulator);

#endif //ALPHA_ACCUMULATOR