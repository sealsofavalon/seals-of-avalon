#ifndef BIT_ARRAY_H
#define BIT_ARRAY_H

template<unsigned int TBitCount>
class CBitArray : public NiMemObject
{
public:
    CBitArray()
    {
        memset(m_uiBits, 0, sizeof(m_uiBits));
    }

    bool Test(unsigned int uiBit) const
    {
        NIASSERT(uiBit < TBitCount);
        return (m_uiBits[uiBit/32] & (1 << (uiBit%32))) ? true : false;
    }

    void Set(unsigned int uiBit) 
    {
        NIASSERT(uiBit < TBitCount);
        m_uiBits[uiBit/32] |= (1 << (uiBit%32));
    }

    void Clear(unsigned int uiBit) 
    {
        NIASSERT(uiBit < TBitCount);
        m_uiBits[uiBit/32] &= ~(1 << (uiBit%32));
    }

    unsigned int GetBitCount() const { return TBitCount; }
    unsigned int* GetBits() { return m_uiBits; }
    const unsigned int* GetBits() const { return m_uiBits; }
    unsigned int GetEffectiveSize() const
    {
        unsigned int uiCount = 0;
        unsigned int uiBits;
        for(unsigned int i = 0; i < TBitCount/32; ++i)
        {
            uiBits = m_uiBits[i];
            for(int j = 0; j < 32; ++j)
            {
                if(uiBits & (1 << j))
                    ++uiCount;
            }
        }

        return uiCount;
    }

private:
    unsigned int m_uiBits[TBitCount/32];
};

#endif