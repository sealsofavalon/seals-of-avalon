
#include "TerrainPCH.h"
#include "Box.h"

void CBox::Reset()
{
	m_kMax.x = -FLOAT_MAX;
	m_kMax.y = -FLOAT_MAX;
	m_kMax.z = -FLOAT_MAX;

	m_kMin.x = FLOAT_MAX;
	m_kMin.y = FLOAT_MAX;
	m_kMin.z = FLOAT_MAX;
}

void CBox::Intersect(const CBox& kBox)
{
	m_kMin.x = NiMin(m_kMin.x, kBox.m_kMin.x);
	m_kMin.y = NiMin(m_kMin.y, kBox.m_kMin.y);
	m_kMin.z = NiMin(m_kMin.z, kBox.m_kMin.z);

	m_kMax.x = NiMax(m_kMax.x, kBox.m_kMax.x);
	m_kMax.y = NiMax(m_kMax.y, kBox.m_kMax.y);
	m_kMax.z = NiMax(m_kMax.z, kBox.m_kMax.z);
}

void CBox::Intersect(const NiPoint3& kPoint)
{
	m_kMin.x = NiMin(m_kMin.x, kPoint.x);
	m_kMin.y = NiMin(m_kMin.y, kPoint.y);
	m_kMin.z = NiMin(m_kMin.z, kPoint.z);

	m_kMax.x = NiMax(m_kMax.x, kPoint.x);
	m_kMax.y = NiMax(m_kMax.y, kPoint.y);
	m_kMax.z = NiMax(m_kMax.z, kPoint.z);
}


/// Collide a line against the box.
///
/// Returns true on collision.
bool CBox::CollideLine(const NiPoint3& start, const NiPoint3& end) const
{
	float st,et;
	float fst = 0.f;
	float fet = 1.f;
	const float* bmin = &m_kMin.x;
	const float* bmax = &m_kMax.x;
	const float* si   = &start.x;
	const float* ei   = &end.x;

	for(int i = 0; i < 3; i++) 
	{
		if(si[i] < ei[i])
		{
			if (si[i] > bmax[i] || ei[i] < bmin[i])
				return false;

			float di = ei[i] - si[i];
			st = (si[i] < bmin[i]) ? (bmin[i] - si[i]) / di : 0.f;
			et = (ei[i] > bmax[i]) ? (bmax[i] - si[i]) / di : 1.f;
		}
		else 
		{
			if (ei[i] > bmax[i] || si[i] < bmin[i])
				return false;

			float di = ei[i] - si[i];
			st = (si[i] > bmax[i]) ? (bmax[i] - si[i]) / di : 0.f;
			et = (ei[i] < bmin[i]) ? (bmin[i] - si[i]) / di : 1.f;
		}
		if (st > fst) 
		{
			fst = st;
		}
		if (et < fet)
			fet = et;

		if (fet < fst)
			return false;
	}

	return true;
}

bool CBox::CollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent) const
{
   float st,et;
   float fst = 0;
   float fet = 1;
   
   NiPoint3 newmin = m_kMin - extent;
   NiPoint3 newmax = m_kMax + extent;

   const float* bmin = &newmin.x;
   const float* bmax = &newmax.x;
   const float* si   = &start.x;
   const float* ei   = &end.x;

   for (int i = 0; i < 3; i++) {
      if (si[i] < ei[i]) {
         if (si[i] > bmax[i] || ei[i] < bmin[i])
            return false;
         float di = ei[i] - si[i];
         st = (si[i] < bmin[i]) ? (bmin[i] - si[i]) / di : 0;
         et = (ei[i] > bmax[i]) ? (bmax[i] - si[i]) / di : 1;
      }
      else {
         if (ei[i] > bmax[i] || si[i] < bmin[i])
            return false;
         float di = ei[i] - si[i];
         st = (si[i] > bmax[i]) ? (bmax[i] - si[i]) / di : 0;
         et = (ei[i] < bmin[i]) ? (bmin[i] - si[i]) / di : 1;
      }
      if (st > fst) {
         fst = st;
      }
      if (et < fet)
         fet = et;

      if (fet < fst)
         return false;
   }

   return true;
}

bool CBox::IsOverlapped(const CBox& box) const
{
   if (box.m_kMin.x > m_kMax.x ||
       box.m_kMin.y > m_kMax.y ||
       box.m_kMin.z > m_kMax.z)
      return false;

   if (box.m_kMax.x < m_kMin.x ||
       box.m_kMax.y < m_kMin.y ||
       box.m_kMax.z < m_kMin.z)
      return false;

   return true;
}

int CBox::WhichSide(const NiPlane& kPlane) const
{
    NiPoint3 kPoint;

    unsigned int uiNegativeSide = 0;
    unsigned int uiPositiveSide = 0;

    if( kPlane.Distance(m_kMin) >= 0 )
        ++uiPositiveSide;
    else
        ++uiNegativeSide;

    if( kPlane.Distance(m_kMax) >= 0 )
        ++uiPositiveSide;
    else
        ++uiNegativeSide;

    if( uiPositiveSide && uiNegativeSide )
        return NiPlane::NO_SIDE;

    if( kPlane.Distance(NiPoint3(m_kMin.x, m_kMin.y, m_kMax.z)) >= 0 )
        ++uiPositiveSide;
    else
        ++uiNegativeSide;

    if( kPlane.Distance(NiPoint3(m_kMax.x, m_kMax.y, m_kMin.z)) >= 0 )
        ++uiPositiveSide;
    else
        ++uiNegativeSide;

    if( uiPositiveSide && uiNegativeSide )
        return NiPlane::NO_SIDE;

    if( kPlane.Distance(NiPoint3(m_kMax.x, m_kMin.y, m_kMin.z)) >= 0 )
        ++uiPositiveSide;
    else
        ++uiNegativeSide;

    if( kPlane.Distance(NiPoint3(m_kMin.x, m_kMax.y, m_kMax.z)) >= 0 )
        ++uiPositiveSide;
    else
        ++uiNegativeSide;

    if( uiPositiveSide && uiNegativeSide )
        return NiPlane::NO_SIDE;

    if( kPlane.Distance(NiPoint3(m_kMax.x, m_kMin.y, m_kMax.z)) >= 0 )
        ++uiPositiveSide;
    else
        ++uiNegativeSide;

    if( kPlane.Distance(NiPoint3(m_kMin.x, m_kMax.y, m_kMin.z)) >= 0 )
        ++uiPositiveSide;
    else
        ++uiNegativeSide;

    if( uiPositiveSide && uiNegativeSide )
        return NiPlane::NO_SIDE;

    if( uiPositiveSide )
        return NiPlane::POSITIVE_SIDE;
    else
        return NiPlane::NEGATIVE_SIDE;
}

bool CBox::TestVisible(const NiFrustumPlanes& kPlanes)
{
	unsigned int i;
	for (i = 0; i < NiFrustumPlanes::MAX_PLANES; i++)
	{
		int iSide = WhichSide(kPlanes.GetPlane(i));
		if (iSide == NiPlane::NEGATIVE_SIDE)
		{
			// The object is not visible since it is on the negative
			// side of the plane.
			break;
		}
	}

	if(i == NiFrustumPlanes::MAX_PLANES)
		return true;

	return false;
}