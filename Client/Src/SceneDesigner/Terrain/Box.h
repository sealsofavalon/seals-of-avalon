
#ifndef BOX_H
#define BOX_H

#include "TerrainLibType.h"

#define FLOAT_MIN (1.175494351e-38F)
#define FLOAT_MAX (3.402823466e+38F)

class TERRAIN_ENTRY CBox
{
public:
   NiPoint3 m_kMin;
   NiPoint3 m_kMax;

public:
   void Reset();
   /// Perform an intersection operation with another box
   /// and store the results in this box.
   void Intersect(const CBox& kBox);
   void Intersect(const NiPoint3& kPoint);

   /// Collide a line against the box.
   ///
   /// Returns true on collision.
   bool CollideLine(const NiPoint3& start, const NiPoint3& end) const;
   bool CollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent) const;

   bool IsOverlapped(const CBox& box) const;
   int WhichSide(const NiPlane& kPlane) const;
   bool TestVisible(const NiFrustumPlanes& kPlanes);
   NiPoint3 GetCenter() const { return (m_kMin + m_kMax)/2.f; }
};


#endif //BOX_H