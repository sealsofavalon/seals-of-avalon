
#include "TerrainPCH.h"
#include "Chunk.h"
#include "Tile.h"
#include "Map.h"
#include "FreeImage.h"
#include "Water.h"
#include "TerrainHelper.h"
#include "EditorInfoComponent.h"
#include "EntityInfoComponent.h"
#include "SoundEmitterComponent.h"
#include "TerrainShader.h"
#include "Player.h"

NiFixedString CChunk::ms_kSceneRootPointerName;
CTerrainShader* CChunk::ms_pkTerrainShader = NULL;

unsigned short*	CChunk::ms_pusTriList = NULL;

void CQuad::GetBoundBox(CBox& kBox) const
{
    kBox.m_kMin.x = x;
    kBox.m_kMin.y = y;
    kBox.m_kMin.z = FLOAT_MAX;
    kBox.m_kMin.z = z0 < kBox.m_kMin.z ? z0 : kBox.m_kMin.z;
    kBox.m_kMin.z = z1 < kBox.m_kMin.z ? z1 : kBox.m_kMin.z; 
    kBox.m_kMin.z = z2 < kBox.m_kMin.z ? z2 : kBox.m_kMin.z;
    kBox.m_kMin.z = z3 < kBox.m_kMin.z ? z3 : kBox.m_kMin.z;

    kBox.m_kMax.x = x + UNIT_SIZE;
    kBox.m_kMax.y = y + UNIT_SIZE;
    kBox.m_kMax.z = -FLOAT_MAX;
    kBox.m_kMax.z = z0 > kBox.m_kMax.z ? z0 : kBox.m_kMax.z;
    kBox.m_kMax.z = z1 > kBox.m_kMax.z ? z1 : kBox.m_kMax.z;
    kBox.m_kMax.z = z2 > kBox.m_kMax.z ? z2 : kBox.m_kMax.z;
    kBox.m_kMax.z = z3 > kBox.m_kMax.z ? z3 : kBox.m_kMax.z;
}

bool CQuad::CollideLine(const NiPoint3 &start, const NiPoint3 &end, float& fTime) const
{
    CBox BoundBox;
    GetBoundBox(BoundBox);

    if( !BoundBox.CollideLine(start, end) )
        return false;

    NiPoint3 v0(x,             y,		        z0);
    NiPoint3 v1(x + UNIT_SIZE, y,		        z1);
    NiPoint3 v2(x + UNIT_SIZE, y + UNIT_SIZE,	z2);
    NiPoint3 v3(x,             y + UNIT_SIZE,	z3);

    float t1 = LineCheckWithTriangle(start, end, v0, v1, v3);
    float t2 = LineCheckWithTriangle(start, end, v1, v2, v3);

    if( t1 > 1.0f && t2 > 1.0f )
        return false;

    fTime = NiMin( t1, t2 );

    return true;
}

bool CQuad::CollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, Result& result) const
{ 
    //先和Box碰撞
    CBox BoundBox;
    GetBoundBox(BoundBox);
    if( !BoundBox.CollideBox(start, end, extent) )
        return false;

    NiPoint3 v0(x,             y,		        z0);
    NiPoint3 v1(x + UNIT_SIZE, y,		        z1);
    NiPoint3 v2(x + UNIT_SIZE, y + UNIT_SIZE,	z2);
    NiPoint3 v3(x,             y + UNIT_SIZE,	z3);

    float t1 = FindSeparatingAxis(v0, v1, v3, start, end, extent, NiPoint3(1.f, 0.f, 0.f), NiPoint3(0.f, 1.f, 0.f), NiPoint3(0.f, 0.f, 1.f));
    float t2 = FindSeparatingAxis(v1, v2, v3, start, end, extent, NiPoint3(1.f, 0.f, 0.f), NiPoint3(0.f, 1.f, 0.f), NiPoint3(0.f, 0.f, 1.f));

    if( t1 > 1.0f && t2 > 1.0f )
        return false;

    NiPoint3 n;
    NiPoint3 dir = end - start;
    if( t1 < t2 )
    {
        n = NiPoint3(v0.z - v1.z, v0.z - v3.z, UNIT_SIZE);
        if( n.Dot(dir) < 0.f )
        {
            result.fTime = t1;
            result.kNormal = n;
            return true;
        }
        else if( t2 <= 1.f )
        {
            n = NiPoint3(v3.z - v2.z, v1.z - v2.z, UNIT_SIZE);
            if( n.Dot(dir) < 0.f )
            {
                result.fTime = t2;
                result.kNormal = n;
                return true;
            }
        }
    }
    else
    {
        n = NiPoint3(v3.z - v2.z, v1.z - v2.z, UNIT_SIZE);
        if( n.Dot(dir) < 0.f )
        {
            result.fTime = t2;
            result.kNormal = n;
            return true;
        }
        else if( t1 <= 1.f )
        {
            n = NiPoint3(v0.z - v1.z, v0.z - v3.z, UNIT_SIZE);
            if( n.Dot(dir) < 0.f )
            {
                result.fTime = t1;
                result.kNormal = n;
                return true;
            }
        }
    }

    return false;
}

void CQuad::MultiCollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList) const
{
    //先和Box碰撞
    CBox BoundBox;
    GetBoundBox(BoundBox);
    if( !BoundBox.CollideBox(start, end, extent) )
        return;

    NiPoint3 v0(x,             y,		        z0);
    NiPoint3 v1(x + UNIT_SIZE, y,		        z1);
    NiPoint3 v2(x + UNIT_SIZE, y + UNIT_SIZE,	z2);
    NiPoint3 v3(x,             y + UNIT_SIZE,	z3);

    float t1 = FindSeparatingAxis(v0, v1, v3, start, end, extent, NiPoint3(1.f, 0.f, 0.f), NiPoint3(0.f, 1.f, 0.f), NiPoint3(0.f, 0.f, 1.f));
    float t2 = FindSeparatingAxis(v1, v2, v3, start, end, extent, NiPoint3(1.f, 0.f, 0.f), NiPoint3(0.f, 1.f, 0.f), NiPoint3(0.f, 0.f, 1.f));

    NiPoint3 n;
    NiPoint3 dir = end - start;
    if( t1 < 1.0f )
    {
        //n = (v1 - v0).Cross(v3 - v1);
        n = NiPoint3(v0.z - v1.z, v0.z - v3.z, UNIT_SIZE);
        if( n.Dot(dir) < 0.f )
        {
            resultList.push_back(Result(t1, n));
        }
    }

    if( t2 < 1.0f )
    {
        //n = (v2 - v1).Cross(v3 - v2);
        n = NiPoint3(v3.z - v2.z, v1.z - v2.z, UNIT_SIZE);
        if( n.Dot(dir) < 0.f )
        {
            resultList.push_back(Result(t2, n));
        }
    }
}


CChunk::CChunk()
{
    memset(&m_kHeader, 0, sizeof(m_kHeader));

    m_pkPositionList = NiNew NiPoint3[VERTEX_COUNT*VERTEX_COUNT];
    memset(m_pkPositionList, 0, sizeof(NiPoint3)*VERTEX_COUNT*VERTEX_COUNT);

    m_pkNormalList = NiNew NiPoint3[VERTEX_COUNT*VERTEX_COUNT];
    memset(m_pkNormalList, 0, sizeof(NiPoint3)*VERTEX_COUNT*VERTEX_COUNT);

    m_pusTriList = NULL;

    m_uiNumLayers = 0;
    memset(m_auiLayerTextures, 0, sizeof(m_auiLayerTextures));

    memset(m_apkAlphaMapDatas, 0, sizeof(m_apkAlphaMapDatas));
    memset(m_apkAlphaTextures, 0, sizeof(m_apkAlphaTextures));
    memset(m_AlphaSum, 0, sizeof(m_AlphaSum));

	for (int i=0; i<MaxGrid; i++)
	{
		m_pkGridPixelData[i] = NiNew NiPixelData(GRID_COUNT, GRID_COUNT, NiPixelFormat::A8, 1, 1);
		m_pkGridPixelData[i]->IncRefCount();
		unsigned int* puiPixels = (unsigned int*)m_pkGridPixelData[i]->GetPixels();
		memset(m_pkGridPixelData[i]->GetPixels(), 0, m_pkGridPixelData[i]->GetTotalSizeInBytes());
		m_pkGridTexture[i] = NULL;
	}

    m_pkShadowMapData = NULL;
    m_pkShadowTexture = NULL;

	m_apkWaterEdgeMapDatas = NULL;
	m_apkWaterEdgeMapTextures = NULL;

    m_pkWater = NULL;
    m_kBasePoint = NiPoint3(0.f, 0.f, 0.f);
}

CChunk::~CChunk()
{
    for( int i = 0; i < MAX_LAYERS - 1; i++ )
    {
        if(m_apkAlphaTextures[i])
            NiDelete (NiTexture*)m_apkAlphaTextures[i];

        if( m_apkAlphaMapDatas[i] )
            m_apkAlphaMapDatas[i]->DecRefCount();
    }

    if(m_pkShadowTexture)
        NiDelete (NiTexture*)m_pkShadowTexture;

    if( m_pkShadowMapData )
        m_pkShadowMapData->DecRefCount();

	for (int i= 0; i<MaxGrid; i++)
	{
		if( m_pkGridTexture[i] )
			NiDelete (NiTexture*)m_pkGridTexture[i];

		m_pkGridPixelData[i]->DecRefCount();

	}

    if( !m_spGeometry )
    {
        NiDelete[] m_pkPositionList;
        NiDelete[] m_pkNormalList;
    }

    if( m_pkWater )
        NiDelete m_pkWater;

    if( m_pusTriList )
        NiFree(m_pusTriList);

    for( unsigned int i = 0; i < m_pkSoundEmitterEntities.GetSize(); i++ )
    {
        if(m_pkSoundEmitterEntities.GetAt(i))
            m_pkSoundEmitterEntities.GetAt(i)->RemoveReference();
    }
}

void CChunk::Set(int x, int y, CTile* pkTile)
{
    m_iX = x;
    m_iY = y;
    m_pkTile = pkTile;
}

//4Bit to Byte
unsigned char ToByte(unsigned char* pucPtr, bool bLowByte)
{
    float fValue;
    if(bLowByte)
        fValue = float(*pucPtr & 0x0f);
    else
        fValue = float((*pucPtr & 0xf0) >> 4);

    //变换到8bit
    fValue = fValue * (255.f / 15.f);
    return fValue >= 255.f ? 255 : unsigned char(fValue); 
}

bool CChunk::Load(NiBinaryStream* pkStream)
{
    m_kBoundBox.Reset();

    m_kBasePoint.x = float(m_pkTile->GetX() * TILE_SIZE + m_iX * CHUNK_SIZE);
    m_kBasePoint.y = float(m_pkTile->GetY() * TILE_SIZE + m_iY * CHUNK_SIZE);
    m_kBasePoint.z = 0.f;

    CTagStream kTagStream;
    while( kTagStream.ReadFromStream( pkStream ) )
    {
        switch( kTagStream.GetTag() )
        {
        case 'HEAD': //Header
            {
				if(m_pkTile->GetVersion() < MakeVersion(1, 0))
				{
					kTagStream.Read(&m_kHeader.m_uiTurnEdgeFlag1, sizeof(unsigned int));
					kTagStream.Read(&m_kHeader.m_uiTurnEdgeFlag2, sizeof(unsigned int));
					kTagStream.Read(&m_kHeader.m_uiHoleFlag1, sizeof(unsigned int));
					kTagStream.Read(&m_kHeader.m_uiHoleFlag2, sizeof(unsigned int));
					m_kHeader.m_uiAreaId = 0;
				}
				else
				{
					kTagStream.Read(&m_kHeader, sizeof(m_kHeader));
				}
            }
            break;

        case 'HGHT': //height
            {
                NIASSERT( kTagStream.GetSize() == VERTEX_COUNT*VERTEX_COUNT*sizeof(float) );

                float h;

                NiPoint3* pkPosition = m_pkPositionList;
                for(int  x = 0; x < VERTEX_COUNT; x++)
                {
                    for( int y = 0; y < VERTEX_COUNT; y++ )
                    {
                        kTagStream.Read(&h, sizeof(float));
                        pkPosition->x = float(x*UNIT_SIZE);
                        pkPosition->y = float(y*UNIT_SIZE);
                        pkPosition->z = h;

                        m_kBoundBox.Intersect(*pkPosition + m_kBasePoint);

                        pkPosition++;
                    }
                }
            }
            break;

        case 'NRML': //法线
            {
                NIASSERT( kTagStream.GetSize() == VERTEX_COUNT*VERTEX_COUNT*sizeof(char)*3 );

                char acNormal[3];
                NiPoint3* pkNormal = m_pkNormalList;
                for( int  x = 0; x < VERTEX_COUNT; x++ )
                {
                    for( int y = 0; y < VERTEX_COUNT; y++ )
                    {
                        kTagStream.Read(acNormal, sizeof(acNormal));

                        pkNormal->x = float(acNormal[0])/127.f;
                        pkNormal->y = float(acNormal[1])/127.f;
                        pkNormal->z = float(acNormal[2])/127.f;
                        //*pkNormal *= 10.f;

                        pkNormal++;
                    }
                }
            }
            break;

        case 'LMAP': //层贴图
            {
                m_uiNumLayers = kTagStream.GetSize() / sizeof(unsigned int);
                NIASSERT( m_uiNumLayers <= MAX_LAYERS );

                kTagStream.Read(m_auiLayerTextures, kTagStream.GetSize());
            }
            break;

        case 'ALPH':
            {
                NiBool bHasShadow;
                kTagStream.Read(&bHasShadow, sizeof(bHasShadow));

                int iChannels = 0;
                unsigned char* pucChannels[MAX_LAYERS]; //加上阴影
                for( unsigned int uiLayer = 1; uiLayer < m_uiNumLayers; uiLayer++ ) //第0层没有Alpha值, 层索引小于m_uiNumLayers
                {
                    m_apkAlphaMapDatas[iChannels] = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, NiPixelFormat::A8, 1, 1);
                    m_apkAlphaMapDatas[iChannels]->IncRefCount();
                    pucChannels[iChannels] = m_apkAlphaMapDatas[iChannels]->GetPixels(0, 0);
                    iChannels++;
                }

                if( bHasShadow )
                {
                    m_pkShadowMapData = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, NiPixelFormat::A8, 1, 1);
                    m_pkShadowMapData->IncRefCount();
                    pucChannels[iChannels] = m_pkShadowMapData->GetPixels(0, 0);
                    iChannels++;
                }

                int iChannel1 = 0;
                switch( iChannels )
                {
                case 1:
                    //pkFormat1 = &NiPixelFormat::A8;
                    iChannel1 = 1;
                    break;
                case 2:
                    //pkFormat1 = &NiPixelFormat::A8;
                    //pkFormat2 = &NiPixelFormat::A8;
                    iChannel1 = 1;
                    break;
                case 3:
                    //pkFormat1 = &NiPixelFormat::RGB24;
                    iChannel1 = 3;
                    break;

                case 4:
                    //pkFormat1 = &NiPixelFormat::RGBA32;
                    iChannel1 = 4;
                    break;
                case 5:
                    //pkFormat1 = &NiPixelFormat::RGBA32;
                    //pkFormat2 = &NiPixelFormat::A8;
                    iChannel1 = 4;
                    break;
                case 6:
                    //pkFormat1 = &NiPixelFormat::RGB24;
                    //pkFormat2 = &NiPixelFormat::RGB24;
                    iChannel1 = 3;
                    break;
                }

                int iBufferSize = iChannels*ALPHA_MAP_SIZE*ALPHA_MAP_SIZE/2;
                unsigned char* pucBuffer = NiAlloc(unsigned char, iBufferSize);
                kTagStream.Read(pucBuffer, iBufferSize);
                unsigned char* pucPtr = pucBuffer;

                bool bLowBits = true;
                unsigned char ucPixel;
                for( int i = 0; i < ALPHA_MAP_SIZE; i++ )
                {
                    for( int j = 0; j < ALPHA_MAP_SIZE; j++ )
                    {
                        for( int k = 0; k < iChannel1; k++ )
                        {
                            *pucChannels[k] = ToByte(pucPtr, bLowBits);
                            bLowBits = !bLowBits;
                            if(bLowBits)
                                pucPtr++;

                            if( k < int(m_uiNumLayers - 1) )
                            {
                                m_AlphaSum[k] += *pucChannels[k];
                            }

                            pucChannels[k]++;
                        }
                    }
                }

                for( int i = 0; i < ALPHA_MAP_SIZE; i++ )
                {
                    for( int j = 0; j < ALPHA_MAP_SIZE; j++ )
                    {
                        for( int k = iChannel1; k < iChannels; k++ )
                        {
                            *pucChannels[k] = ToByte(pucPtr, bLowBits);
                            bLowBits = !bLowBits;
                            if(bLowBits)
                                pucPtr++;

                            if( k < int(m_uiNumLayers - 1) )
                            {
                                m_AlphaSum[k] += *pucChannels[k];
                            }

                            pucChannels[k]++;
                        }
                    }
                }

                NiFree(pucBuffer);
            }
            break;


		case 'WDMP': //水的深度图
				{
					m_apkWaterEdgeMapDatas = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, NiPixelFormat::ARGB8888, 1, 1);
					DWORD* pdwChannels = (DWORD*)m_apkWaterEdgeMapDatas->GetPixels(0, 0);

					int iBufferSize = sizeof(DWORD)*ALPHA_MAP_SIZE*ALPHA_MAP_SIZE; // 4 bits per pixel
					DWORD* pdwBuffer = NiAlloc(DWORD, iBufferSize);
					kTagStream.Read(pdwBuffer, iBufferSize);

					for( int i = 0; i < ALPHA_MAP_SIZE; i++ )
					{
						for( int j = 0; j < ALPHA_MAP_SIZE; j++ )
						{
							pdwChannels[i*ALPHA_MAP_SIZE+j] = pdwBuffer[i*ALPHA_MAP_SIZE+j];
						}
					}

					if ( !m_apkWaterEdgeMapTextures )
					{
						m_apkWaterEdgeMapTextures = CAlphaTexture::Create( m_apkWaterEdgeMapDatas );
					}
				}
				break;

        case 'LQUD': //水
            {
                m_pkWater = NiNew CWater(m_kBasePoint.x, m_kBasePoint.y, m_pkTile);
                m_pkWater->Load(&kTagStream);
            }
            break;

        case 'GRID':
            {
                ReadGrid(kTagStream, WalkGrid);
            }
            break;

		case 'FYGD':
			{
				ReadGrid(kTagStream, FlyGrid);
			}
			break;

            //所引用的模型
        case 'MODL':
            {
                int iCurrentTileX = 0;//m_pkTile->GetX();
                int iCurrentTileY = 0;//m_pkTile->GetY();
                int iTileX;
                int iTileY;

                ModelRef kRef;
                unsigned int uiCount;
                kTagStream.Read(&uiCount, sizeof(uiCount));
                for(unsigned int ui = 0; ui < uiCount; ++ui)
                {
                    kTagStream.Read(&iTileX, sizeof(iTileX));
                    kTagStream.Read(&iTileY, sizeof(iTileY));
                    kTagStream.Read(&kRef.uiIndex, sizeof(kRef.uiIndex));

                    kRef.iTileX = iTileX + iCurrentTileX;
                    kRef.iTileY = iTileY + iCurrentTileY;

                    m_kModelList.push_back(kRef);
                }
            }
            break;

        case 'GRSS':
            {
                m_kGrass.Load(&kTagStream);
            }
            break;

        case 'SNDE': //Sound Emitter
            {
                NiExternalAssetManager* pkAssetManager = CMap::GetAssetManager();
				NiEntityErrorInterface* pkErrors = const_cast<NiEntityErrorInterface *>(CMap::Get()->GetErrorHandler());

                unsigned int uiSoundEmitterCount;
                
                NiFixedString kEntityName;
                NiFixedString kSoundName;
                NiPoint3 kTranslate;
                unsigned int uiTimeInterval;
                unsigned int uiLoopCount;
                float fMinDistance;
                float fMaxDistance;
                
                NiGeneralEntity* pkEntity;
                NiTransformationComponent *pkTransformationComponent;
                CSoundEmitterComponent *pkSoundEmitterComponent;
                kTagStream.Read(&uiSoundEmitterCount, sizeof(uiSoundEmitterCount));
                m_pkSoundEmitterEntities.SetSize(uiSoundEmitterCount);
                for(unsigned int i = 0; i < uiSoundEmitterCount; ++i)
                {
                    kTagStream.ReadFixedString(kEntityName);
                    kTagStream.ReadFixedString(kSoundName);
                    kTagStream.Read(&kTranslate, sizeof(kTranslate));
                    kTagStream.Read(&uiTimeInterval, sizeof(uiTimeInterval));
                    kTagStream.Read(&uiLoopCount, sizeof(uiLoopCount));
                    kTagStream.Read(&fMinDistance, sizeof(fMinDistance));
                    kTagStream.Read(&fMaxDistance, sizeof(fMaxDistance));

                    NiGeneralEntity* pkEntity = NiNew NiGeneralEntity;
                    pkEntity->SetName(kEntityName);
                    pkEntity->SetTypeMask(NiGeneralEntity::SoundEmitter);

                    pkTransformationComponent = NiNew NiTransformationComponent;
                    pkTransformationComponent->SetTranslation(kTranslate + m_kBasePoint);
                    pkEntity->AddComponent(pkTransformationComponent, false);
                    
                    pkSoundEmitterComponent = NiNew CSoundEmitterComponent;
                    pkSoundEmitterComponent->SetSoundName(kSoundName);
                    pkSoundEmitterComponent->SetTimeInterval(uiTimeInterval);
                    pkSoundEmitterComponent->SetLoopCount(uiLoopCount);
                    pkSoundEmitterComponent->SetMinDistance(fMinDistance);
                    pkSoundEmitterComponent->SetMaxDistance(fMaxDistance);
                    pkEntity->AddComponent(pkSoundEmitterComponent, false);
                    m_pkSoundEmitterEntities.SetAt(i, pkEntity);
                    pkEntity->AddReference();

                    pkEntity->Update(NULL, 0.f, pkErrors, pkAssetManager);
                }
            }
            break;

        default:
            {
                NIASSERT( false );
            }
            break;
        }
    }

    if( m_kHeader.m_uiHoleFlag1 || m_kHeader.m_uiHoleFlag2 )
    {
        for( int X = 0; X < VERTEX_COUNT - 1; X++ )
        {
            for( int Y = 0; Y < VERTEX_COUNT - 1; Y++ )
            {
                if( IsHole(X*UNIT_SIZE + int(m_kBasePoint.x), Y*UNIT_SIZE + int(m_kBasePoint.y)) )
                {
                    ExcavateHole(X*UNIT_SIZE + int(m_kBasePoint.x), Y*UNIT_SIZE + int(m_kBasePoint.y), true, true); 
                }
            }
        }
    }

    if( m_kHeader.m_uiTurnEdgeFlag1 || m_kHeader.m_uiTurnEdgeFlag2 )
    {
        for( int X = 0; X < VERTEX_COUNT - 1; X++ )
        {
            for( int Y = 0; Y < VERTEX_COUNT - 1; Y++ )
            {
                if( IsTurnEdge(X*UNIT_SIZE + int(m_kBasePoint.x), Y*UNIT_SIZE + int(m_kBasePoint.y)) )
                {
                    TurnEdge(X*UNIT_SIZE + int(m_kBasePoint.x), Y*UNIT_SIZE + int(m_kBasePoint.y), true, true); 
                }
            }
        }
    }

    m_bTextureChanged = true;

    return true;
}

bool CChunk::Save(NiBinaryStream* pkStream)
{
    PureLayers();

    CTagStream kTagStream;

    //Header
    {
        kTagStream.Reset();
        kTagStream.SetTag('HEAD');

        kTagStream.Write(&m_kHeader, sizeof(m_kHeader));

        kTagStream.WriteToStream( pkStream );
    }

    //hight
    {
        kTagStream.Reset();
        kTagStream.SetTag('HGHT');

        NiPoint3* pkPosition = m_pkPositionList;
        for(int  x = 0; x < VERTEX_COUNT; x++)
        {
            for( int y = 0; y < VERTEX_COUNT; y++ )
            {
                kTagStream.Write(&pkPosition->z, sizeof(float));
                pkPosition++;
            }
        }

        kTagStream.WriteToStream( pkStream );
    }

    //normal
    {
        kTagStream.Reset();
        kTagStream.SetTag('NRML');

        char acNormal[3];
        NiPoint3* pkNormal = m_pkNormalList;
        for( int  x = 0; x < VERTEX_COUNT; x++ )
        {
            for( int y = 0; y < VERTEX_COUNT; y++ )
            {
                acNormal[0] = (char)(pkNormal->x*127.f);
                acNormal[1] = (char)(pkNormal->y*127.f);
                acNormal[2] = (char)(pkNormal->z*127.f);
                pkNormal++;

                kTagStream.Write(acNormal, sizeof(acNormal));
            }
        }

        kTagStream.WriteToStream( pkStream );
    }

    //层贴图
    if( m_uiNumLayers > 0 )
    {
        kTagStream.Reset();
        kTagStream.SetTag('LMAP');

        kTagStream.Write(m_auiLayerTextures, m_uiNumLayers*sizeof(unsigned int));

        kTagStream.WriteToStream( pkStream );
    }

    //alpha贴图
    if( m_uiNumLayers > 1 || m_pkShadowMapData )
    {
        kTagStream.Reset();
        kTagStream.SetTag('ALPH');

        int iChannels = 0;
        unsigned char* pucChannels[MAX_LAYERS]; //加上阴影
        for( unsigned int uiLayer = 1; uiLayer < m_uiNumLayers; uiLayer++ )
        {
            pucChannels[iChannels] = m_apkAlphaMapDatas[iChannels]->GetPixels(0, 0);
            iChannels++;
        }

        NiBool bHasShadow;
        if( m_pkShadowMapData )
        {
            bHasShadow = true;
            pucChannels[iChannels] = m_pkShadowMapData->GetPixels(0, 0);
            iChannels++;
        }
        else
        {
            bHasShadow = false;
        }

        int iChannel1 = 0;
        switch( iChannels )
        {
        case 1:
            //pkFormat1 = &NiPixelFormat::A8;
            iChannel1 = 1;
            break;
        case 2:
            //pkFormat1 = &NiPixelFormat::A8;
            //pkFormat2 = &NiPixelFormat::A8;
            iChannel1 = 1;
            break;
        case 3:
            //pkFormat1 = &NiPixelFormat::RGB24;
            iChannel1 = 3;
            break;
        case 4:
            //pkFormat1 = &NiPixelFormat::RGBA32;
            iChannel1 = 4;
            break;
        case 5:
            //pkFormat1 = &NiPixelFormat::RGBA32;
            //pkFormat2 = &NiPixelFormat::A8;
            iChannel1 = 4;
            break;
        case 6:
            //pkFormat1 = &NiPixelFormat::RGB24;
            //pkFormat2 = &NiPixelFormat::RGB24;
            iChannel1 = 3;
            break;
        }

        int iBufferSize = iChannels*ALPHA_MAP_SIZE*ALPHA_MAP_SIZE/2; // 4 bits per pixel
        unsigned char* pucBuffer = NiAlloc(unsigned char, iBufferSize);
        unsigned char* pucPtr = pucBuffer;

        bool bLowBits = true;
        unsigned char ucPixel;
        for( int i = 0; i < ALPHA_MAP_SIZE; i++ )
        {
            for( int j = 0; j < ALPHA_MAP_SIZE; j++ )
            {
                for( int k = 0; k < iChannel1; k++ )
                {
                    ucPixel = *pucChannels[k]++;
                    if( bLowBits )
                    {
                        *pucPtr = ((ucPixel >> 4)&0x0f);
                        bLowBits = false;
                    }
                    else
                    {
                        *pucPtr |= (ucPixel&0xf0);
                        pucPtr++;
                        bLowBits = true;
                    }
                }
            }
        }

        for( int i = 0; i < ALPHA_MAP_SIZE; i++ )
        {
            for( int j = 0; j < ALPHA_MAP_SIZE; j++ )
            {
                for( int k = iChannel1; k < iChannels; k++ )
                {
                    ucPixel = *pucChannels[k]++;
                    if( bLowBits )
                    {
                        *pucPtr = ((ucPixel >> 4)&0x0f);
                        bLowBits = false;
                    }
                    else
                    {
                        *pucPtr |= (ucPixel&0xf0);
                        pucPtr++;
                        bLowBits = true;
                    }
                }
            }
        }

        kTagStream.Write(&bHasShadow, sizeof(bHasShadow));
        kTagStream.Write(pucBuffer, iBufferSize);
        kTagStream.WriteToStream( pkStream );

        NiFree(pucBuffer);
    }

    if( m_pkWater && m_pkWater->IsValid() )
    {
        kTagStream.Reset();
        kTagStream.SetTag('LQUD');

        m_pkWater->Save(&kTagStream);

        kTagStream.WriteToStream( pkStream );
    }

	if ( m_apkWaterEdgeMapDatas )
	{
		kTagStream.Reset();
		kTagStream.SetTag('WDMP');//water depth map

		DWORD* pdwChannels = (DWORD*)m_apkWaterEdgeMapDatas->GetPixels(0, 0);

		int iBufferSize = sizeof(DWORD)*ALPHA_MAP_SIZE*ALPHA_MAP_SIZE; // 4 bits per pixel
		DWORD* pucBuffer = NiAlloc(DWORD, iBufferSize);
		DWORD* pucPtr = pucBuffer;

		for( int i = 0; i < ALPHA_MAP_SIZE; i++ )
		{
			for( int j = 0; j < ALPHA_MAP_SIZE; j++ )
			{
				pucPtr[i*ALPHA_MAP_SIZE+j] = pdwChannels[i*ALPHA_MAP_SIZE+j];
			}
		}

		kTagStream.Write(pucBuffer, iBufferSize);

		kTagStream.WriteToStream( pkStream );
	}

    {
		for (int gridLevel = WalkGrid; gridLevel < MaxGrid; gridLevel++)
		{
			//检查是否有格子信息，有的话就写
			unsigned char* pucPtr = m_pkGridPixelData[gridLevel]->GetPixels(0, 0);
			unsigned int uiAlphaSum = 0;
			for( unsigned int i = 0; i < GRID_COUNT*GRID_COUNT; i++ )
			{
				uiAlphaSum += pucPtr[i];
			}

			if( uiAlphaSum > 0 )
			{
				kTagStream.Reset();

				if ( gridLevel == WalkGrid )
					kTagStream.SetTag('GRID');
				else if ( gridLevel == FlyGrid )
					kTagStream.SetTag('FYGD');

				WriteGrid(kTagStream, gridLevel);

				kTagStream.WriteToStream( pkStream );
			}
		}

    }

    if( !m_kModelList.empty() )
    {
        //todo... 检测是否实际引用

        kTagStream.Reset();
        kTagStream.SetTag('MODL');

        int iCurrentTileX = 0;//m_pkTile->GetX();
        int iCurrentTileY = 0;//m_pkTile->GetY();
        int iTileX;
        int iTileY;

        unsigned int uiCount = m_kModelList.size();
        kTagStream.Write(&uiCount, sizeof(uiCount));
        for(ModelList::const_iterator itr = m_kModelList.begin(); itr != m_kModelList.end(); itr++)
        {
            iTileX = itr->iTileX - iCurrentTileX; //存储相对的Tile坐标
            iTileY = itr->iTileY - iCurrentTileY;
            kTagStream.Write(&iTileX, sizeof(iTileX));
            kTagStream.Write(&iTileY, sizeof(iTileY));
            kTagStream.Write(&itr->uiIndex, sizeof(itr->uiIndex));
        }

        kTagStream.WriteToStream( pkStream );
    }

    if( m_pkSoundEmitterEntities.GetSize() > 0 )
    {
        kTagStream.Reset();
        kTagStream.SetTag('SNDE');

        unsigned int uiSoundEmitterCount = m_pkSoundEmitterEntities.GetSize();
        kTagStream.Write(&uiSoundEmitterCount, sizeof(uiSoundEmitterCount));

        NiEntityInterface* pkEntity;

        NiFixedString kEntityName;
        NiFixedString kSoundName;
        NiPoint3 kTranslate;
        unsigned int uiTimeInterval;
        unsigned int uiLoopCount;
        float fMinDistance;
        float fMaxDistance;
        char acRelFilePath[1024];
        for(unsigned int i = 0; i < uiSoundEmitterCount; ++i)
        {
            pkEntity = m_pkSoundEmitterEntities.GetAt(i);
            kEntityName = pkEntity->GetName();
            pkEntity->GetPropertyData(CSoundEmitterComponent::ms_kTranslationName, kTranslate);
            pkEntity->GetPropertyData(CSoundEmitterComponent::ms_kSoundName, kSoundName);
            pkEntity->GetPropertyData(CSoundEmitterComponent::ms_kTimeIntervalName, uiTimeInterval);
            
            pkEntity->GetPropertyData(CSoundEmitterComponent::ms_kLoopCountName, uiLoopCount);
            pkEntity->GetPropertyData(CSoundEmitterComponent::ms_kMinDistanceName, fMinDistance);
            pkEntity->GetPropertyData(CSoundEmitterComponent::ms_kMaxDistanceName, fMaxDistance);
            
            kTranslate -= m_kBasePoint; //存储相对位置

            //保存相对路径
            TerrainHelper::ConvertToRelative(acRelFilePath, 1024, kSoundName);
            kTagStream.WriteFixedString(kEntityName);
            kTagStream.WriteFixedString(acRelFilePath);
            kTagStream.Write(&kTranslate, sizeof(kTranslate));
            kTagStream.Write(&uiTimeInterval, sizeof(uiTimeInterval));
            kTagStream.Write(&uiLoopCount, sizeof(uiLoopCount));
            kTagStream.Write(&fMinDistance, sizeof(fMinDistance));
            kTagStream.Write(&fMaxDistance, sizeof(fMaxDistance));
        }

        kTagStream.WriteToStream( pkStream );
    }

    if( m_kGrass.GetGrassCount() > 0 )
    {
        kTagStream.Reset();
        kTagStream.SetTag('GRSS');

        m_kGrass.Save(&kTagStream);

        kTagStream.WriteToStream( pkStream );
    }

    return true;
}

void CChunk::ReadGrid(CTagStream& kTagStream, int nLv)
{
    int iX, iY;
    kTagStream.Read(&iX, sizeof(int));
    kTagStream.Read(&iY, sizeof(int));
    NIASSERT(m_iX == iX && m_iY == iY);

    NiBool bUniformGrid;
    kTagStream.Read(&bUniformGrid, sizeof(NiBool));

    unsigned char* pucAlpha = m_pkGridPixelData[nLv]->GetPixels();

    unsigned char ucAlpha;
    if( bUniformGrid )
    {
        kTagStream.Read(&ucAlpha, sizeof(unsigned char));

        if( ucAlpha > 0 )
            ucAlpha = 0;
        else
            ucAlpha = 85;

        memset(pucAlpha, ucAlpha, GRID_COUNT*GRID_COUNT);
    }
    else
    {
        unsigned int uiGridInfo;
        for( int x = 0; x < GRID_COUNT; x++ )
        {
            kTagStream.Read(&uiGridInfo, sizeof(unsigned int));

            for( int y = 0; y < GRID_COUNT; y++ )
            {
                if( !(uiGridInfo & (1 << y)) )
                    ucAlpha = 85;
                else
                    ucAlpha = 0;

                pucAlpha[y*GRID_COUNT + x] = ucAlpha;
            }
        }
    }
}

void CChunk::WriteGrid(CTagStream& kTagStream, int nLv)
{
    kTagStream.Write(&m_iX, sizeof(int));
    kTagStream.Write(&m_iY, sizeof(int));

    NiBool bUniformGrid = true;

    unsigned char* pucAlpha = m_pkGridPixelData[nLv]->GetPixels();
    unsigned char ucAlpha = pucAlpha[0];

    //判断是否所有属性都是相同的
    for( int i = 0; i < GRID_COUNT*GRID_COUNT; i++ )
    {
        if( ucAlpha != pucAlpha[i] )
        {
            bUniformGrid = false;
            break;
        }
    }

    kTagStream.Write(&bUniformGrid, sizeof(NiBool));
    if( bUniformGrid )
    {
        if( ucAlpha > 0 )
            ucAlpha = 0;
        else
            ucAlpha = 1;

        kTagStream.Write(&ucAlpha, sizeof(unsigned char));
    }
    else
    {
        unsigned int uiGridInfo;
        for( int x = 0; x < GRID_COUNT; x++ )
        {
            uiGridInfo = 0;

            for( int y = 0; y < GRID_COUNT; y++ )
            { 
                ucAlpha = pucAlpha[y*GRID_COUNT + x];

                if( ucAlpha > 0 )
                {
                    uiGridInfo |= (0 << y); //阻挡, 不可行走
                }
                else
                {
                    uiGridInfo |= (1 << y); //可行走
                }
            }

            kTagStream.Write(&uiGridInfo, sizeof(unsigned int));
        }
    }
}

void CChunk::OnCreate()
{
    NiPoint3* pkPosition = m_pkPositionList;

    m_kBasePoint.x = float(m_pkTile->GetX() * TILE_SIZE + m_iX * CHUNK_SIZE);
    m_kBasePoint.y = float(m_pkTile->GetY() * TILE_SIZE + m_iY * CHUNK_SIZE);
    m_kBasePoint.z = 0.f;

    float h = 0.f;

    m_kBoundBox.Reset();
    for(int  x = 0; x < VERTEX_COUNT; x++)
    {
        for( int y = 0; y < VERTEX_COUNT; y++ )
        {
            pkPosition->x = float(x*UNIT_SIZE);
            pkPosition->y = float(y*UNIT_SIZE);
            pkPosition->z = h;

            m_kBoundBox.Intersect(*pkPosition + m_kBasePoint);

            pkPosition++;
        }
    }

    NiPoint3* pkNormal = m_pkNormalList;
    for( int  x = 0; x < VERTEX_COUNT; x++ )
    {
        for( int y = 0; y < VERTEX_COUNT; y++ )
        {
            pkNormal->x = 0.f;
            pkNormal->y = 0.f;
            pkNormal->z = 1.f;

            pkNormal++;
        }
    }

    m_bTextureChanged = true;
}

bool CChunk::GetVertexIndex(float inX, float inY, int& iIndex) const
{
    float x = inX - m_kBasePoint.x;
    float y = inY - m_kBasePoint.y;

    unsigned int X = (unsigned int)(x/UNIT_SIZE);
    unsigned int Y = (unsigned int)(y/UNIT_SIZE);

    if( X >= VERTEX_COUNT || Y >= VERTEX_COUNT )
    {
        return false;
    }

    iIndex = X * VERTEX_COUNT + Y;
    return true;
}


float CChunk::GetHeight(float x, float y) const
{
    float h;
    if( GetHeight(x, y, h) )
        return h;

    return 10.f;
}

bool CChunk::GetHeight(float x, float y, float& h) const
{
    int iIndex;
    if( !GetVertexIndex( x, y, iIndex ) )
        return false;


    //  v3	          v2  
    //   *------------*
    //   |            |
    //   |            |
    //   |            |
    //   |            |
    //   |            |
    //   *------------*
    //  v0            v1

    float h0 = m_pkPositionList[iIndex].z;
    float h1 = m_pkPositionList[iIndex + VERTEX_COUNT].z;
    float h2 = m_pkPositionList[iIndex + VERTEX_COUNT + 1].z;
    float h3 = m_pkPositionList[iIndex + 1].z;

    float deltaX = NiFmod(x - m_kBasePoint.x, UNIT_SIZE)/float(UNIT_SIZE);
    float deltaY = NiFmod(y - m_kBasePoint.y, UNIT_SIZE)/float(UNIT_SIZE);

    if( deltaX + deltaY < 1.f )
    {
        //v3v0v1
        h = h0 + deltaX*(h1 - h0) + deltaY*(h3 - h0);
    }
    else
    {
        //v1v2v3
        h = h2 + (1 - deltaX)*(h3 - h2) + (1 - deltaY)*(h1 - h2);
    }

    return true;
}

bool CChunk::GetHeightAndNormal(float x, float y, float&h, NiPoint3& kNormal) const
{
    int iIndex;
    if( !GetVertexIndex( x, y, iIndex ) )
        return false;


    //  v3	          v2  
    //   *------------*
    //   |            |
    //   |            |
    //   |            |
    //   |            |
    //   |            |
    //   *------------*
    //  v0            v1

    float h0 = m_pkPositionList[iIndex].z;
    float h1 = m_pkPositionList[iIndex + VERTEX_COUNT].z;
    float h2 = m_pkPositionList[iIndex + VERTEX_COUNT + 1].z;
    float h3 = m_pkPositionList[iIndex + 1].z;

    float deltaX = NiFmod(x - m_kBasePoint.x, UNIT_SIZE)/float(UNIT_SIZE);
    float deltaY = NiFmod(y - m_kBasePoint.y, UNIT_SIZE)/float(UNIT_SIZE);

    if( deltaX + deltaY < 1.f )
    {
        //v3v0v1
        h = h0 + deltaX*(h1 - h0) + deltaY*(h3 - h0); 
        kNormal = (m_pkPositionList[iIndex + VERTEX_COUNT] - m_pkPositionList[iIndex]).UnitCross(m_pkPositionList[iIndex + 1] - m_pkPositionList[iIndex]);
    }
    else
    {
        //v1v2v3
        h = h2 + (1 - deltaX)*(h3 - h2) + (1 - deltaY)*(h1 - h2);
        kNormal = (m_pkPositionList[iIndex + 1] - m_pkPositionList[iIndex + VERTEX_COUNT + 1]).UnitCross(m_pkPositionList[iIndex + VERTEX_COUNT] - m_pkPositionList[iIndex + VERTEX_COUNT + 1]);
    }

    return true;
}

void CChunk::SetHeight(float x, float y, float h)
{
    int iIndex;
    if( !GetVertexIndex( float(x), float(y), iIndex ) )
        return;

    if( m_pkPositionList[iIndex].z == h )
        return;

    m_pkPositionList[iIndex].z = h;

    m_pkTile->SetDirty(true);
    m_kBoundBox.Intersect(m_pkPositionList[iIndex] + m_kBasePoint);

    //更新显存
	if(m_spGeometry)
		m_spGeometry->GetModelData()->MarkAsChanged(NiGeometryData::VERTEX_MASK);
}

void CChunk::UpdateNormal(float x, float y)
{
    int iIndex;
    if( !GetVertexIndex(float(x), float(y), iIndex) )
        return;

    m_pkTile->SetDirty(true);

    NiPoint3 v0 = NiPoint3(float(x), float(y), CMap::Get()->GetHeight(float(x), float(y)));
    NiPoint3 v1 = NiPoint3(float(x - UNIT_SIZE), float(y), CMap::Get()->GetHeight(float(x - UNIT_SIZE), float(y)));
    NiPoint3 v2 = NiPoint3(float(x), float(y - UNIT_SIZE), CMap::Get()->GetHeight(float(x), float(y - UNIT_SIZE)));
    NiPoint3 v3 = NiPoint3(float(x + UNIT_SIZE), float(y), CMap::Get()->GetHeight(float(x + UNIT_SIZE), float(y)));
    NiPoint3 v4 = NiPoint3(float(x), float(y + UNIT_SIZE), CMap::Get()->GetHeight(float(x), float(y + UNIT_SIZE)));

    NiPoint3 n1 = (v1 - v0).UnitCross(v2 - v0);
    NiPoint3 n2 = (v2 - v0).UnitCross(v3 - v0);
    NiPoint3 n3 = (v3 - v0).UnitCross(v4 - v0);
    NiPoint3 n4 = (v4 - v0).UnitCross(v1 - v0);

    NiPoint3 n = (n1 + n2 + n3 + n4)/4.0f;
    n.Unitize();

    m_pkNormalList[iIndex] = n;

    //更新显存
	if(m_spGeometry)
		m_spGeometry->GetModelData()->MarkAsChanged(NiGeometryData::NORMAL_MASK);
}

float CChunk::GetWaterHeight(float x, float y)
{
    float h;
    if( GetWaterHeight(x, y, h) )
        return h;

    return 10.f;
}

bool CChunk::GetWaterHeight(float x, float y, float& h)
{
    if( m_pkWater )
        return m_pkWater->GetHeight(x, y, h);

    return false;
}

void CChunk::SetWaterHeight(float x, float y, float h)
{
    if( m_pkWater )
        m_pkWater->SetHeight(x, y, h);
}

void CChunk::SetWaterFlag(float x, float y, bool bHasWater, unsigned int uiWaterIndex)
{
    if( !bHasWater && m_pkWater == NULL )
        return;

    if( m_pkWater == NULL )
    {
        float fStartX = float(m_pkTile->GetX() * TILE_SIZE + m_iX * CHUNK_SIZE);
        float fStartY = float(m_pkTile->GetY() * TILE_SIZE + m_iY * CHUNK_SIZE);
        m_pkWater = NiNew CWater(fStartX, fStartY, m_pkTile);
    }

    m_pkWater->SetFlag( x, y, bHasWater);
    if(bHasWater)
        m_pkWater->SetShaderIndex(uiWaterIndex);

	//BuidWaterEdgeMap();
}

bool CChunk::GetWaterFlag(float x, float y)
{
    if( m_pkWater )
        return m_pkWater->GetFlag(x, y);

    return false;
}

void CChunk::OnWaterChanged(unsigned int uiWaterIndex)
{
    if(m_pkWater && m_pkWater->GetShaderIndex() == uiWaterIndex)
    {
        CMap* pkMap = CMap::Get();
        if(pkMap->GetWaterShader(uiWaterIndex) == NULL)
        {
            //此Shader被删除了
            NiDelete m_pkWater;
            m_pkWater = NULL;

            m_pkTile->SetDirty(true);
        }
        else
        {
            m_pkWater->OnShaderChanged();
        }

    }
}

bool CChunk::IsTurnEdge(int x, int y) const
{
    unsigned short X = (unsigned short)((x - int(m_kBasePoint.x))/UNIT_SIZE);
    unsigned short Y = (unsigned short)((y - int(m_kBasePoint.y))/UNIT_SIZE);

    if( X >= VERTEX_COUNT - 1 || Y >= VERTEX_COUNT - 1 )
    {
        return false;
    }

    if( Y < 4 )
    {
        //0, 1, 2, 3
        return TerrainHelper::GetBit(m_kHeader.m_uiTurnEdgeFlag1, Y * 8 + X);
    }
    else
    {
        //4, 5, 6, 7
        return TerrainHelper::GetBit(m_kHeader.m_uiTurnEdgeFlag2, (Y - 4) * 8 + X);
    }
}

bool CChunk::SetTurnEdgeFlag(int x, int y, bool bTurn)
{
    unsigned short X = (unsigned short)((x - int(m_kBasePoint.x))/UNIT_SIZE);
    unsigned short Y = (unsigned short)((y - int(m_kBasePoint.y))/UNIT_SIZE);

    if( X >= VERTEX_COUNT - 1 || Y >= VERTEX_COUNT - 1 )
    {
        return false;
    }

    bool bChange;
    if( Y < 4 )
    {
        //0, 1, 2, 3
        bChange = TerrainHelper::SetBit(m_kHeader.m_uiTurnEdgeFlag1, Y * 8 + X, bTurn);
    }
    else
    {
        //4, 5, 6, 7
        bChange = TerrainHelper::SetBit(m_kHeader.m_uiTurnEdgeFlag2, (Y - 4) * 8 + X, bTurn);
    }

    if( bChange )
        m_pkTile->SetDirty(true);

    return bChange;
}

bool CChunk::TurnEdge(int x, int y, bool bTurn, bool bForce)
{
    unsigned short X = (unsigned short)((x - int(m_kBasePoint.x))/UNIT_SIZE);
    unsigned short Y = (unsigned short)((y - int(m_kBasePoint.y))/UNIT_SIZE);

    if( X >= VERTEX_COUNT - 1 || Y >= VERTEX_COUNT - 1 )
    {
        return false;
    }

    bool bChange = SetTurnEdgeFlag(x, y, bTurn);
    if(!bChange && !bForce)
        return false;

    //Update the render data
    if( m_pusTriList == NULL )
    {
        m_pusTriList = NiAlloc(unsigned short, (VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2*3);
        memcpy(m_pusTriList, ms_pusTriList, (VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2*3*sizeof(unsigned short));
    }

    unsigned short idx = (X*(VERTEX_COUNT - 1) + Y)*6;

    if( bTurn )
    {
        m_pusTriList[idx + 2] = m_pusTriList[idx + 3];
        m_pusTriList[idx + 5] = m_pusTriList[idx + 0];
    }
    else
    {
        m_pusTriList[idx + 2] = m_pusTriList[idx + 4];
        m_pusTriList[idx + 5] = m_pusTriList[idx + 1];
    }

    if( m_spGeometry )
    {
        ((CTerrainGeomData*)m_spGeometry->GetModelData())->Replace((VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2, m_pusTriList);
        m_spGeometry->GetModelData()->MarkAsChanged(NiTriBasedGeomData::TRIANGLE_INDEX_MASK);
    }

    return true;
}

bool CChunk::IsHole(int x, int y) const
{
    unsigned short X = (unsigned short)((x - int(m_kBasePoint.x))/UNIT_SIZE);
    unsigned short Y = (unsigned short)((y - int(m_kBasePoint.y))/UNIT_SIZE);

    if(X >= VERTEX_COUNT - 1 || Y >= VERTEX_COUNT - 1)
        return false;

    if( Y < 4 )
    {
        //0, 1, 2, 3
        return TerrainHelper::GetBit(m_kHeader.m_uiHoleFlag1, Y * 8 + X);
    }
    else
    {
        //4, 5, 6, 7
        return TerrainHelper::GetBit(m_kHeader.m_uiHoleFlag2, (Y - 4) * 8 + X);
    }
}

bool CChunk::SetHoleFlag(int x, int y, bool bHole)
{
    unsigned short X = (unsigned short)((x - int(m_kBasePoint.x))/UNIT_SIZE);
    unsigned short Y = (unsigned short)((y - int(m_kBasePoint.y))/UNIT_SIZE);

    if(X >= VERTEX_COUNT - 1 || Y >= VERTEX_COUNT - 1)
        return false;

    bool bChange;
    if( Y < 4 )
    {
        //0, 1, 2, 3
        bChange = TerrainHelper::SetBit(m_kHeader.m_uiHoleFlag1, Y * 8 + X, bHole);
    }
    else
    {
        //4, 5, 6, 7
        bChange = TerrainHelper::SetBit(m_kHeader.m_uiHoleFlag2, (Y - 4) * 8 + X, bHole);
    }

    if( bChange )
        m_pkTile->SetDirty(true);

    return bChange;
}

bool CChunk::ExcavateHole(int x, int y, bool bHole, bool bForce)
{
    unsigned short X = (unsigned short)((x - int(m_kBasePoint.x))/UNIT_SIZE);
    unsigned short Y = (unsigned short)((y - int(m_kBasePoint.y))/UNIT_SIZE);

    if(X >= VERTEX_COUNT - 1 || Y >= VERTEX_COUNT - 1)
        return false;

    bool bChange = SetHoleFlag(x, y, bHole);
    if( !bChange && !bForce )
        return false;

    //Update the render data
    bool bTurnEdge = IsTurnEdge(x, y);

    if( m_pusTriList == NULL )
    {
        m_pusTriList = NiAlloc(unsigned short, (VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2*3);
        memcpy(m_pusTriList, ms_pusTriList, (VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2*3*sizeof(unsigned short));
    }

    unsigned short idx = (X*(VERTEX_COUNT - 1) + Y)*6;

    if( bHole )
    {
        //显示成退化的一点
        m_pusTriList[idx + 0] = X*VERTEX_COUNT + Y;
        m_pusTriList[idx + 1] = X*VERTEX_COUNT + Y;
        m_pusTriList[idx + 2] = X*VERTEX_COUNT + Y;

        m_pusTriList[idx + 3] = X*VERTEX_COUNT + Y;
        m_pusTriList[idx + 4] = X*VERTEX_COUNT + Y;
        m_pusTriList[idx + 5] = X*VERTEX_COUNT + Y;
    }
    else
    {
        m_pusTriList[idx + 0] = (X + 0)*VERTEX_COUNT + (Y + 0);
        m_pusTriList[idx + 1] = (X + 1)*VERTEX_COUNT + (Y + 0);
        m_pusTriList[idx + 2] = (X + 0)*VERTEX_COUNT + (Y + 1);

        m_pusTriList[idx + 3] = (X + 1)*VERTEX_COUNT + (Y + 1);
        m_pusTriList[idx + 4] = (X + 0)*VERTEX_COUNT + (Y + 1);
        m_pusTriList[idx + 5] = (X + 1)*VERTEX_COUNT + (Y + 0);

        if( bTurnEdge )
        {
            m_pusTriList[idx + 2] = m_pusTriList[idx + 3];
            m_pusTriList[idx + 5] = m_pusTriList[idx + 0];
        }
    }

    if( m_spGeometry )
    {
        //更新显存
        ((CTerrainGeomData*)m_spGeometry->GetModelData())->Replace((VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2, m_pusTriList);
        m_spGeometry->GetModelData()->MarkAsChanged(NiTriBasedGeomData::TRIANGLE_INDEX_MASK);
    }

    return true;
}


unsigned int CChunk::AddLayer(const char* pcTextureName)
{
    unsigned int uiTexture = m_pkTile->AddTexture(pcTextureName, 0.f, 0.f);
    if( uiTexture == INVALID_TEXTURE )
        return INVALID_LAYER;

    for( unsigned int uiLayer = 0; uiLayer < m_uiNumLayers; uiLayer++ )
    {
        if( m_auiLayerTextures[uiLayer] == uiTexture )
            return uiLayer;
    }

    if( m_uiNumLayers >= MAX_LAYERS )
        return INVALID_LAYER;

    m_pkTile->SetDirty(true);

    //增加一层
    m_auiLayerTextures[m_uiNumLayers] = uiTexture;
    m_uiNumLayers++;

    if( m_uiNumLayers > 1 && m_apkAlphaMapDatas[m_uiNumLayers - 2] == NULL )
    {
        m_apkAlphaMapDatas[m_uiNumLayers - 2] = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, NiPixelFormat::A8, 1, 1);
        m_apkAlphaMapDatas[m_uiNumLayers - 2]->IncRefCount();

        unsigned char* pucPixels = m_apkAlphaMapDatas[m_uiNumLayers - 2]->GetPixels();
        memset(pucPixels, 0, ALPHA_MAP_SIZE*ALPHA_MAP_SIZE);
        m_AlphaSum[m_uiNumLayers - 2] = 0;
    }

    m_bTextureChanged = true;

    return m_uiNumLayers - 1;
}

void CChunk::RemoveLayer(unsigned int uiLayer)
{
    NIASSERT( uiLayer < m_uiNumLayers );

    m_pkTile->SetDirty(true);

    for( unsigned int i = uiLayer; i < m_uiNumLayers - 1; i++ )
    {
        if( i >= 1 )
        {
            m_AlphaSum[i - 1] = m_AlphaSum[i];
            NiMemcpy(m_apkAlphaMapDatas[i - 1]->GetPixels(0, 0), ALPHA_MAP_SIZE*ALPHA_MAP_SIZE, m_apkAlphaMapDatas[i]->GetPixels(0, 0), ALPHA_MAP_SIZE*ALPHA_MAP_SIZE);
            m_apkAlphaMapDatas[i - 1]->MarkAsChanged();
        }

        m_auiLayerTextures[i] = m_auiLayerTextures[i + 1];
    }

    if( m_uiNumLayers >= 2 )
    {
        m_AlphaSum[m_uiNumLayers - 2] = 0;
        memset(m_apkAlphaMapDatas[m_uiNumLayers - 2]->GetPixels(0, 0), 0, ALPHA_MAP_SIZE*ALPHA_MAP_SIZE);
        m_apkAlphaMapDatas[m_uiNumLayers - 2]->MarkAsChanged();
    }

    m_auiLayerTextures[m_uiNumLayers - 1] = 0;
    m_bTextureChanged = true;
    m_uiNumLayers--;
}

//删除没有用的层
void CChunk::PureLayers()
{
    //没有Alpha 层
    if( m_uiNumLayers <= 1 )
        return;

    //对每个像素
    //上层的Alpha 如果到了255, 则把下层的Alpha值设置成0
    for( unsigned int i = m_uiNumLayers - 1; i > 1 ; --i )
    {
        unsigned char* pucPixel = m_apkAlphaMapDatas[i - 1]->GetPixels();
        for( unsigned int j = 0; j < ALPHA_MAP_SIZE*ALPHA_MAP_SIZE; ++j )
        {
            if( pucPixel[j] == 255 )
            {
                for( unsigned int k = i - 1; k > 0; --k )
                {
                    m_AlphaSum[k - 1] -= m_apkAlphaMapDatas[k - 1]->GetPixels()[j];
                    m_apkAlphaMapDatas[k - 1]->GetPixels()[j] = 0;
                    m_apkAlphaMapDatas[k - 1]->MarkAsChanged();
                }
            }
        }
    }

    //删除完全透明的层
    for( unsigned int i = 1; i < m_uiNumLayers; )
    {
        if( m_AlphaSum[i - 1] == 0 )//完全透明
        {
            RemoveLayer(i); //会修改m_uiNumLayers
        }
        else
        {
            i++;
        }
    }

    //如果有一层完全不透明, 删除所有下面的层
    for( unsigned int i = m_uiNumLayers; i >= 2; i-- )
    {
        if( m_AlphaSum[i - 2] >= ALPHA_MAP_SIZE*ALPHA_MAP_SIZE*255 )
        {
            unsigned int uiNewLayers = m_uiNumLayers - (i - 1);

            while( uiNewLayers < m_uiNumLayers )
            {
                RemoveLayer(0);
            }

            break;
        }
    }

    //Shadow
    if( m_pkShadowMapData )
    {
        unsigned char* pucPtr = m_pkShadowMapData->GetPixels(0, 0);
        unsigned int uiAlphaSum = 0;
        for( unsigned int i = 0; i < ALPHA_MAP_SIZE*ALPHA_MAP_SIZE; i++ )
        {
            uiAlphaSum += pucPtr[i];
        }

        if( uiAlphaSum == 0 )
        {
            m_pkShadowMapData->DecRefCount();
            m_pkShadowMapData = NULL;
            m_bTextureChanged = true;
        }
    }
}

void CChunk::PaintMaterial(int x, int y, FIBITMAP* pkAlphaMap, const char* pcTextureName, float fScale)
{
    int iHalfWidth = FreeImage_GetWidth(pkAlphaMap) / 2;
    int iHalfHeight = FreeImage_GetHeight(pkAlphaMap) / 2;

    //X,Y为象素空间的坐标
    int X = (x - int(m_kBasePoint.x))*(ALPHA_MAP_SIZE/CHUNK_SIZE);//每个单位2个Alpha象素
    int Y = (y - int(m_kBasePoint.y))*(ALPHA_MAP_SIZE/CHUNK_SIZE);

    int iXBegin = NiMax( X - iHalfWidth, 0 );
    int iYBegin = NiMax( Y - iHalfHeight, 0 );
    int iXEnd = NiMin( X + iHalfWidth - 1,  ALPHA_MAP_SIZE - 1);
    int iYEnd = NiMin( Y + iHalfHeight - 1,  ALPHA_MAP_SIZE - 1);

    if( iXBegin > iXEnd || iYBegin > iYEnd )
        return;

    unsigned int uiLayer = AddLayer( pcTextureName );
    if( uiLayer >= MAX_LAYERS )
    {
        //PureLayers();

        uiLayer = AddLayer( pcTextureName );

        if( uiLayer >= MAX_LAYERS )
        {
            //NiOutputDebugString("add layer error\n");
            return;
        }
    }

    m_pkTile->SetDirty(true);

    bool bShiftKeyDown = ((GetAsyncKeyState(VK_SHIFT) & 0x8000) != 0);
    bool bCtrlKeyDown = ((GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0);

    RGBQUAD kColor;
    float fAlpha;
    float fAddAlpha;
    unsigned char* pucPixel;

    //计算出画刷经过的重叠顶点的个数
    //每经过一个Chunk就会有一个顶点重叠
    //此时Alpha值也必须重叠
    int iXOffset = m_pkTile->GetX() * CHUNK_COUNT + m_iX - NiMax((x - iHalfWidth/2), 0)/CHUNK_SIZE;
    int iYOffset = m_pkTile->GetY() * CHUNK_COUNT + m_iY - NiMax((y - iHalfHeight/2), 0)/CHUNK_SIZE;

    for( int i = iXBegin; i <= iXEnd; i++ )
    {
        for( int j = iYBegin; j <= iYEnd; j++ )
        {
            FreeImage_GetPixelColor(pkAlphaMap, i - X + iHalfWidth - iXOffset, j - Y + iHalfHeight - iYOffset, &kColor);//获取象素值
            fAddAlpha = (float)(kColor.rgbReserved) * fScale;

            if( fAddAlpha == 0.f )
                continue;

            if( uiLayer > 0 )
            {
                pucPixel = m_apkAlphaMapDatas[uiLayer - 1]->GetPixels();
                fAlpha = float(pucPixel[j*ALPHA_MAP_SIZE + i]);
                if( bShiftKeyDown )
                {
                    //按住Shift键减少当前层的Alpha值
                    fAlpha -= fAddAlpha;
                }
                else
                {
                    fAlpha += fAddAlpha;
                }

                m_AlphaSum[uiLayer - 1] -= pucPixel[j*ALPHA_MAP_SIZE + i];
                pucPixel[j*ALPHA_MAP_SIZE + i] = (unsigned char)NiClamp( fAlpha, 0.f, 255.f );
                m_AlphaSum[uiLayer - 1] += pucPixel[j*ALPHA_MAP_SIZE + i];

                m_apkAlphaMapDatas[uiLayer - 1]->MarkAsChanged();
            }

            //减少上面层的Alpha值
            if( !bCtrlKeyDown && !bShiftKeyDown )
            {
                for( unsigned int k = uiLayer + 1; k < m_uiNumLayers; k++ )
                {
                    pucPixel = m_apkAlphaMapDatas[k - 1]->GetPixels();
                    fAlpha = float(pucPixel[j*ALPHA_MAP_SIZE + i]);
                    fAlpha -= fAddAlpha;

                    m_AlphaSum[k - 1] -= pucPixel[j*ALPHA_MAP_SIZE + i];
                    pucPixel[j*ALPHA_MAP_SIZE + i] = (unsigned char)NiClamp( fAlpha, 0.f, 255.f );
                    m_AlphaSum[k - 1] += pucPixel[j*ALPHA_MAP_SIZE + i];

                    m_apkAlphaMapDatas[k - 1]->MarkAsChanged();
                }
            }
        }
    }

    //PureLayers();
}

void CChunk::RemoveMaterial(const char* pcTextureName)
{
    bool bShiftKeyDown = ((GetAsyncKeyState(VK_SHIFT) & 0x8000) != 0);
    bool bCtrlKeyDown = ((GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0);

    if( bShiftKeyDown )
    {
        //按住Shift键, 只删除最上层
        if( m_uiNumLayers > 0 )
        {
            RemoveLayer(m_uiNumLayers - 1);
        }
    }
    else if( bCtrlKeyDown )
    {
        //按住Ctrl键, 删除所有层
        while( m_uiNumLayers > 0 )
        {
            RemoveLayer(m_uiNumLayers - 1);
        }
    }
    else if(pcTextureName)
    {
        //删除指定的层
        unsigned int uiIndex = m_pkTile->GetTextureIndex(pcTextureName);
        if(uiIndex != INVALID_INDEX)
        {
            for(unsigned int uiLayer = 0; uiLayer < m_uiNumLayers; ++uiLayer)
            {
                if(m_auiLayerTextures[uiLayer] == uiIndex)
                {
                    RemoveLayer(uiLayer);
                    break;
                }
            }
        }
    }
}

void CChunk::SetShadow(unsigned char* pucChunkShadow)
{
    if( m_pkShadowMapData == NULL )
    {
        m_pkShadowMapData = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, NiPixelFormat::A8, 1, 1);
        m_pkShadowMapData->IncRefCount();
        m_bTextureChanged = true;
    }

    memcpy(m_pkShadowMapData->GetPixels(), pucChunkShadow, ALPHA_MAP_SIZE*ALPHA_MAP_SIZE);
    m_pkShadowMapData->MarkAsChanged();

    m_pkTile->SetDirty(true);
}

void CChunk::SetMoveable(float x, float y, bool bMoveable, int nLv)
{
    unsigned int X = unsigned int((x - m_kBasePoint.x)/GRID_SIZE);
    unsigned int Y = unsigned int((y - m_kBasePoint.y)/GRID_SIZE);

    if( X >= GRID_COUNT || Y >= GRID_COUNT )
        return;

    unsigned char* pucAlpha = m_pkGridPixelData[nLv]->GetPixels();

    if( bMoveable )
        pucAlpha[Y*GRID_COUNT + X] = 0;
    else
        pucAlpha[Y*GRID_COUNT + X] = 85;

    m_pkGridPixelData[nLv]->MarkAsChanged();

    m_pkTile->SetDirty(true);
}

bool CChunk::GetMoveable(float x, float y, int nLv)
{
    unsigned int X = unsigned int((x - m_kBasePoint.x)/GRID_SIZE);
    unsigned int Y = unsigned int((y - m_kBasePoint.y)/GRID_SIZE);

    if( X >= GRID_COUNT || Y >= GRID_COUNT )
        return false;

    unsigned char* pucAlpha = m_pkGridPixelData[nLv]->GetPixels();

    return (pucAlpha[Y*GRID_COUNT + X] == 0);
}

void CChunk::SetGridInfo(const unsigned char* pucGridInfo, int nLv)
{
    unsigned char* pucAlpha = m_pkGridPixelData[nLv]->GetPixels();
    for( int x = 0; x < GRID_COUNT; x++ )
    {
        for( int y = 0; y < GRID_COUNT; y++ )
        {
            if( pucGridInfo[y*GRID_COUNT + x] )
            {
                //可行走
                pucAlpha[y*GRID_COUNT + x] = 0;
            }
            else
            {
                //不可行走
                pucAlpha[y*GRID_COUNT + x] = 85;
            }
        }
    }
    m_pkGridPixelData[nLv]->MarkAsChanged();

    m_pkTile->SetDirty(true);
}

void CChunk::SetAreaId(unsigned int uiAreaId)
{
	if(m_kHeader.m_uiAreaId != uiAreaId)
	{
		m_kHeader.m_uiAreaId = uiAreaId;
		m_pkTile->SetDirty(true);
	}
}

bool CChunk::PaintGrass(int x, int y, unsigned short usGrass)
{
    unsigned short X = (unsigned short)(x - int(m_kBasePoint.x));
    unsigned short Y = (unsigned short)(y - int(m_kBasePoint.y));

    if( usGrass == INVALID_GRASS )
    {
        if( m_kGrass.ClearGrass(X, Y) )
        {
            m_pkTile->SetDirty(true);
            return true;
        }

        return false;
    }

    if( m_kGrass.SetGrass(X, Y, usGrass) )
    {
        m_pkTile->SetDirty(true);
        return true;
    }

    return false;
}

//Tile的某个Texture删除了
void CChunk::OnDeleteTexture(unsigned int uiTexture)
{
    //先删除这一层
    for( unsigned int i = 0; i < m_uiNumLayers; i++ )
    {
        if( m_auiLayerTextures[i] == uiTexture )
        {
            RemoveLayer(i);
            break;
        }
    }

    //其他大于此索引的层减1
    for( unsigned int i = 0; i < m_uiNumLayers; i++ )
    {
        NIASSERT( uiTexture != m_auiLayerTextures[i] ); 
        if( m_auiLayerTextures[i] > uiTexture )
        {
            m_auiLayerTextures[i]--;
        }
    }
}


bool CChunk::GetQuad(float x, float y, CQuad& quad) const
{
    int iIndex;
    if( !GetVertexIndex(x, y, iIndex) )
        return false;


    //  v3	          v2  
    //   *------------*
    //   |            |
    //   |            |
    //   |            |
    //   |            |
    //   |            |
    //   *------------*
    //  v0            v1

    quad.z0 = m_pkPositionList[iIndex].z;
    quad.z1 = m_pkPositionList[iIndex + VERTEX_COUNT].z;
    quad.z2 = m_pkPositionList[iIndex + VERTEX_COUNT + 1].z;
    quad.z3 = m_pkPositionList[iIndex + 1].z;

    quad.x  = m_pkPositionList[iIndex].x + m_kBasePoint.x;
    quad.y  = m_pkPositionList[iIndex].y + m_kBasePoint.y;

    return true;
}

bool CChunk::CollideLine(const NiPoint3 &start, const NiPoint3 &end, float& fTime)
{
    if( !m_kBoundBox.CollideLine(start, end) )
        return false;

    float deltaX = end.x - start.x;
    float deltaY = end.y - start.y;
    int XDir = deltaX < 0.0f ? -1:1;
    int YDir = deltaY < 0.0f ? -1:1;

    int XStart, XEnd, YStart, YEnd;

    int XMin = (int)m_kBoundBox.m_kMin.x;
    int XMax = (int)m_kBoundBox.m_kMax.x;

    int YMin = (int)m_kBoundBox.m_kMin.y;
    int YMax = (int)m_kBoundBox.m_kMax.y;

    XStart = XDir > 0 ? ALIGN(start.x,           UNIT_SIZE) : ALIGN(start.x + UNIT_SIZE, UNIT_SIZE);
    XEnd   = XDir > 0 ? ALIGN(end.x + UNIT_SIZE, UNIT_SIZE) : ALIGN(end.x,               UNIT_SIZE);

    XStart = XDir > 0 ? NiMax(XStart, XMin) : NiMin(XStart, XMax);
    XEnd   = XDir > 0 ? NiMin(XEnd,   XMax) : NiMax(XEnd,   XMin);

    if( XStart*XDir >= XEnd*XDir )
        return false;

    int x, y;
    CQuad quad;
    if(XEnd == XStart + UNIT_SIZE*XDir)//此时DeltaX可能为0
    {
        YStart = YDir > 0 ? ALIGN(start.y,           UNIT_SIZE) : ALIGN(start.y + UNIT_SIZE, UNIT_SIZE);
        YEnd   = YDir > 0 ? ALIGN(end.y + UNIT_SIZE, UNIT_SIZE) : ALIGN(end.y,               UNIT_SIZE);

        YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
        YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);


        for( y = YStart; y*YDir < YEnd*YDir; y += UNIT_SIZE*YDir )
        {			
            if( GetQuad(float(XStart + XDir), float(y + YDir), quad) && 
                quad.CollideLine(start, end, fTime) )
            {
                return true;
            }
        }

        return false;
    }

    //Y = mX + c;
    float m = deltaY / deltaX;
    float c = end.y - end.x*m;
    float y1, y2;
    for( x = XStart; x*XDir < XEnd*XDir; x += UNIT_SIZE*XDir )
    {
        y1 = m*x + c;
        y2 = y1 + UNIT_SIZE*XDir*m;

        YStart = YDir > 0 ? ALIGN(y1,             UNIT_SIZE) : ALIGN(y1 + UNIT_SIZE, UNIT_SIZE);
        YEnd   = YDir > 0 ? ALIGN(y2 + UNIT_SIZE, UNIT_SIZE) : ALIGN(y2,             UNIT_SIZE);

        YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
        YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);

        for( y = YStart; y*YDir < YEnd*YDir; y += UNIT_SIZE*YDir )
        {
            if( GetQuad(float(x + XDir), float(y + YDir), quad) && 
                quad.CollideLine(start, end, fTime) )
            {
                return true;
            }
        }
    }

    return false;
}

bool CChunk::CollideBox(const NiPoint3 &start, const NiPoint3 &end, const NiPoint3& extent, Result& result)
{
    if( !m_kBoundBox.CollideBox(start, end, extent) )
        return false;

    float deltaX = end.x - start.x;
    float deltaY = end.y - start.y;
    int XDir = deltaX < 0.0f ? -1:1;
    int YDir = deltaY < 0.0f ? -1:1;

    int XStart, XEnd, YStart, YEnd;

    int XMin = (int)m_kBoundBox.m_kMin.x;
    int XMax = (int)m_kBoundBox.m_kMax.x;

    int YMin = (int)m_kBoundBox.m_kMin.y;
    int YMax = (int)m_kBoundBox.m_kMax.y;

    XStart = XDir > 0 ? ALIGN(start.x - extent.x,             UNIT_SIZE) : ALIGN(start.x + extent.x + UNIT_SIZE, UNIT_SIZE);
    XEnd   = XDir > 0 ? ALIGN(end.x   + extent.x + UNIT_SIZE, UNIT_SIZE) : ALIGN(end.x   - extent.x,             UNIT_SIZE);

    XStart = XDir > 0 ? NiMax(XStart, XMin) : NiMin(XStart, XMax);
    XEnd   = XDir > 0 ? NiMin(XEnd,   XMax) : NiMax(XEnd,   XMin);

    if( XStart*XDir >= XEnd*XDir )
        return false;

    int x, y;
    CQuad quad;
    if(NiAbs( deltaX ) < 0.01f || XEnd == XStart + UNIT_SIZE*XDir)//此时DeltaX可能为0
    {
        YStart = YDir > 0 ? ALIGN(start.y - extent.y,             UNIT_SIZE) : ALIGN(start.y + extent.y + UNIT_SIZE, UNIT_SIZE);
        YEnd   = YDir > 0 ? ALIGN(end.y   + extent.y + UNIT_SIZE, UNIT_SIZE) : ALIGN(end.y   - extent.y,             UNIT_SIZE);

        YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
        YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);

        for( x = XStart; x*XDir < XEnd*XDir; x += UNIT_SIZE*XDir )
        {
            for( y = YStart; y*YDir < YEnd*YDir; y += UNIT_SIZE*YDir)
            {			
                if( GetQuad(float(x + XDir), float(y + YDir), quad) && 
                    quad.CollideBox(start, end, extent, result) )
                {
                    return true;
                }
            }
        }

        return false;
    }

    //Y = mX + c;
    float m = deltaY / deltaX;
    float c = end.y - end.x*m;
    float y1, y2;
    for( x = XStart; x*XDir < XEnd*XDir; x += UNIT_SIZE*XDir )
    {
        y1 = m*x + c;
        y2 = y1 + UNIT_SIZE*XDir*m;

        YStart = YDir > 0 ? ALIGN(y1 - extent.y,             UNIT_SIZE) : ALIGN(y1 + extent.y + UNIT_SIZE, UNIT_SIZE);
        YEnd   = YDir > 0 ? ALIGN(y2 + extent.y + UNIT_SIZE, UNIT_SIZE) : ALIGN(y2 - extent.y,             UNIT_SIZE);

        YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
        YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);

        for( y = YStart; y*YDir < YEnd*YDir; y += UNIT_SIZE*YDir )
        {
            if( GetQuad(float(x + XDir), float(y + YDir), quad) && 
                quad.CollideBox(start, end, extent, result) )
            {
                return true;
            }
        }
    }

    return false;
}

void CChunk::MultiCollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList)
{
    MultiCollideBoxModels(start, end, extent, resultList);

    //bound box 只包含地形
    if( !m_kBoundBox.CollideBox(start, end, extent) )
        return;

    MultiCollideBoxTerrain(start, end, extent, resultList);
}

void CChunk::MultiCollideBoxTerrain(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList)
{
    float deltaX = end.x - start.x;
    float deltaY = end.y - start.y;
    int XDir = deltaX < 0.0f ? -1:1;
    int YDir = deltaY < 0.0f ? -1:1;

    int XStart, XEnd, YStart, YEnd;

    int XMin = (int)m_kBoundBox.m_kMin.x;
    int XMax = (int)m_kBoundBox.m_kMax.x;

    int YMin = (int)m_kBoundBox.m_kMin.y;
    int YMax = (int)m_kBoundBox.m_kMax.y;

    XStart = XDir > 0 ? ALIGN(start.x - extent.x,             UNIT_SIZE) : ALIGN(start.x + extent.x + UNIT_SIZE, UNIT_SIZE);
    XEnd   = XDir > 0 ? ALIGN(end.x   + extent.x + UNIT_SIZE, UNIT_SIZE) : ALIGN(end.x   - extent.x,             UNIT_SIZE);

    XStart = XDir > 0 ? NiMax(XStart, XMin) : NiMin(XStart, XMax);
    XEnd   = XDir > 0 ? NiMin(XEnd,   XMax) : NiMax(XEnd,   XMin);

    if( XStart*XDir >= XEnd*XDir )
        return;

    int x, y;
    CQuad quad;
    if(NiAbs( deltaX ) < 0.01 || XEnd == XStart + UNIT_SIZE*XDir)//此时DeltaX可能为0
    {
        YStart = YDir > 0 ? ALIGN(start.y - extent.y,             UNIT_SIZE) : ALIGN(start.y + extent.y + UNIT_SIZE, UNIT_SIZE);
        YEnd   = YDir > 0 ? ALIGN(end.y   + extent.y + UNIT_SIZE, UNIT_SIZE) : ALIGN(end.y   - extent.y,             UNIT_SIZE);

        YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
        YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);

        for( x = XStart; x*XDir < XEnd*XDir; x += UNIT_SIZE*XDir )
        {
            for( y = YStart; y*YDir < YEnd*YDir; y += UNIT_SIZE*YDir)
            {			
                if( GetQuad(float(x + XDir), float(y + YDir), quad) )
                    quad.MultiCollideBox(start, end, extent, resultList);
            }
        }
    }
    else
    {
        //Y = mX + c;
        float m = deltaY / deltaX;
        float c = end.y - end.x*m;
        float y1, y2;
        for( x = XStart; x*XDir < XEnd*XDir; x += UNIT_SIZE*XDir )
        {
            y1 = m*x + c;
            y2 = y1 + UNIT_SIZE*XDir*m;

            YStart = YDir > 0 ? ALIGN(y1 - extent.y,             UNIT_SIZE) : ALIGN(y1 + extent.y + UNIT_SIZE, UNIT_SIZE);
            YEnd   = YDir > 0 ? ALIGN(y2 + extent.y + UNIT_SIZE, UNIT_SIZE) : ALIGN(y2 - extent.y,             UNIT_SIZE);

            YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
            YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);

            for( y = YStart; y*YDir < YEnd*YDir; y += UNIT_SIZE*YDir )
            {
                if( GetQuad(float(x + XDir), float(y + YDir), quad) )
                    quad.MultiCollideBox(start, end, extent, resultList);
            }
        }
    }
}

void CChunk::MultiCollideBoxModels(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList)
{
    CMap* pkMap = CMap::Get();
    NiEntityInterface* pkEntity;
    CEntityInfoComponent* pkEntityInfoComponent;
    NiObject* pkSceneRootPointer;
    NiAVObject* pkSceneRoot;
    for( ModelList::const_iterator itr = m_kModelList.begin(); itr != m_kModelList.end(); ++itr )
    {
        pkEntity = pkMap->GetEntity(itr->iTileX, itr->iTileY, itr->uiIndex);
        if( !pkEntity || pkEntity->GetHidden() )
            continue;

        pkEntityInfoComponent = (CEntityInfoComponent*)pkEntity->GetComponentByTemplateID(CEntityInfoComponent::ms_kTemplateID);
        if( pkEntityInfoComponent == NULL )
            continue;

        if( pkEntityInfoComponent->GetCollisionCount() == pkMap->GetCollisionCount() )
            continue;

        pkEntityInfoComponent->SetCollisionCount(pkMap->GetCollisionCount());


        pkSceneRootPointer = NULL;
        pkSceneRoot = NULL;
        if (pkEntity->GetPropertyData(ms_kSceneRootPointerName, pkSceneRootPointer))
        {
            pkSceneRoot = NiDynamicCast(NiAVObject, pkSceneRootPointer);
        }

        if( pkSceneRoot )
        {
            MultiCollide(pkSceneRoot, start, end, extent, resultList);
        }
    }
}

void CChunk::CalcShadow(const NiPoint3 &start, const NiPoint3 &end, float& fShadow)
{
    CMap* pkMap = CMap::Get();
    NiEntityInterface* pkEntity;
    CEntityInfoComponent* pkEntityInfoComponent;
    NiObject* pkSceneRootPointer;
    NiAVObject* pkSceneRoot;
    NiPoint3 kDir = end - start;
    kDir.Unitize();

    for( ModelList::const_iterator itr = m_kModelList.begin(); itr != m_kModelList.end(); ++itr )
    {
        pkEntity = pkMap->GetEntity(itr->iTileX, itr->iTileY, itr->uiIndex);
        if( !pkEntity || pkEntity->GetHidden() )
            continue;

        pkEntityInfoComponent = (CEntityInfoComponent*)pkEntity->GetComponentByTemplateID(CEntityInfoComponent::ms_kTemplateID);
        if( pkEntityInfoComponent == NULL )
            continue;

        if(!pkEntityInfoComponent->GetCastShadow() || pkEntityInfoComponent->GetCollisionCount() == pkMap->GetCollisionCount() )
            continue;

        pkEntityInfoComponent->SetCollisionCount(pkMap->GetCollisionCount());


        pkSceneRootPointer = NULL;
        pkSceneRoot = NULL;
        if (pkEntity->GetPropertyData(ms_kSceneRootPointerName, pkSceneRootPointer))
        {
            pkSceneRoot = NiDynamicCast(NiAVObject, pkSceneRootPointer);
        }

        if( pkSceneRoot )
        {
            CalcModelShadow(pkSceneRoot, start, kDir, fShadow);
            if(fShadow >= 1.f)
                return;
        }
    }
}

void CChunk::BuildTextureProperty()
{
    if( !m_bTextureChanged )
        return;

    m_bTextureChanged = false;

    for( unsigned int ui = 1; ui < m_uiNumLayers; ui++ )
    {
        if(m_apkAlphaMapDatas[ui - 1])
        {
            if( m_apkAlphaTextures[ui - 1] == NULL
                || m_apkAlphaTextures[ui - 1]->GetSourcePixelData() != m_apkAlphaMapDatas[ui - 1] )
            {
                NiDelete (NiTexture*)m_apkAlphaTextures[ui - 1];
                m_apkAlphaTextures[ui - 1] = CAlphaTexture::Create(m_apkAlphaMapDatas[ui - 1]);
            }
        }
        else
        {
            NiDelete (NiTexture*)m_apkAlphaTextures[ui - 1];
            m_apkAlphaTextures[ui - 1] = NULL;
        }
    }

    if( m_pkShadowMapData )
    {
        if( m_pkShadowTexture == NULL
            || m_pkShadowTexture->GetSourcePixelData() != m_pkShadowMapData )
        {
            m_pkShadowTexture = CAlphaTexture::Create(m_pkShadowMapData);
        }
    }
    else
    {
        NiDelete (NiTexture*)m_pkShadowTexture;
        m_pkShadowTexture = NULL;
    }

	for (int nLv=WalkGrid; nLv < MaxGrid; nLv++)
	{
		if( m_pkGridPixelData[nLv] )
		{
			if( m_pkGridTexture[nLv] == NULL
				|| m_pkGridTexture[nLv]->GetSourcePixelData() != m_pkGridPixelData[nLv] )
			{
				m_pkGridTexture[nLv] = CAlphaTexture::Create(m_pkGridPixelData[nLv]);
			}
		}
		else
		{
			NiDelete (NiTexture*)m_pkGridTexture[nLv];
			m_pkGridTexture[nLv] = NULL;
		}
	}

}

void CChunk::CreateGeometry()
{
    if( m_spGeometry )
        return;

    //
    //todo... 扩充CTerrainShader使得在好的显卡上面可以一个Pass渲染完
    //
    NiTriShapeData* pkModelData = NiNew CTerrainGeomData(VERTEX_COUNT * VERTEX_COUNT, 
        m_pkPositionList, 
        m_pkNormalList, 
        NULL, 
        NULL,
        0,
        NiGeometryData::NBT_METHOD_NONE,
        (VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2,
        m_pusTriList ? m_pusTriList : ms_pusTriList);
    m_spGeometry = NiNew NiTriShape(pkModelData);
    m_spGeometry->SetTranslate(m_kBasePoint);

    if( ms_pkTerrainShader == NULL )
    {
        ms_pkTerrainShader = NiNew CTerrainShader();
        ms_pkTerrainShader->IncRefCount();
    }

    m_spGeometry->SetShader(ms_pkTerrainShader);

    m_spGeometry->UpdateProperties();
    m_spGeometry->Update(0.f, false);
}

void CChunk::BuildVisibleSet(CCullingProcess& kCuller)
{
    CBox kBoundBox = m_kBoundBox;
    kBoundBox.m_kMax.z += 200.f;

    const NiFrustumPlanes& kPlanes = kCuller.GetFrustumPlanes();

    unsigned int i;
    for (i = 0; i < NiFrustumPlanes::MAX_PLANES; ++i)
    {
        int iSide = kBoundBox.WhichSide(kPlanes.GetPlane(i));

        if (iSide == NiPlane::NEGATIVE_SIDE)
        {
            // The object is not visible since it is on the negative
            // side of the plane.
            break;
        }
    }

    if(i < NiFrustumPlanes::MAX_PLANES)
        return;

    //计算地形本身是否可见
    CreateGeometry();
    BuildTextureProperty();

    NiGeometryData* pkGeometryData = m_spGeometry->GetModelData();
    if( pkGeometryData->GetRevisionID() )
    {
        pkGeometryData->GetBound().ComputeFromData(VERTEX_COUNT * VERTEX_COUNT, m_pkPositionList);
        m_spGeometry->Update(0.f, false);
    }

    for (i = 0; i < NiFrustumPlanes::MAX_PLANES; ++i)
    {
        int iSide = m_kBoundBox.WhichSide(kPlanes.GetPlane(i));

        if (iSide == NiPlane::NEGATIVE_SIDE)
        {
            // The object is not visible since it is on the negative
            // side of the plane.
            break;
        }
    }

    if( i == NiFrustumPlanes::MAX_PLANES )
        kCuller.OnChunkVisible(this);

    if( m_pkWater )
        m_pkWater->BuildVisibleSet(kCuller);

    if((CMap::GetVisibleMask() & NiGeneralEntity::SceneObject) == 0)
        return;

    // Scene Object
    NiEntityInterface* pkEntity;
    NiObject* pkSceneRootPointer;
    NiAVObject* pkSceneRoot;

    NiExternalAssetManager* pkAssetManager = CMap::GetAssetManager();

    CMap* pkMap = CMap::Get();
    unsigned int uiFrameCount = pkMap->GetFrameCount();
    CEntityInfoComponent* pkEntityInfoComponent;
    for( ModelList::const_iterator itr = m_kModelList.begin(); itr != m_kModelList.end(); ++itr )
    {
        pkEntity = pkMap->GetEntity(itr->iTileX, itr->iTileY, itr->uiIndex);
        if( !pkEntity || pkEntity->GetHidden() )
            continue;

        pkEntityInfoComponent = (CEntityInfoComponent*)pkEntity->GetComponentByTemplateID(CEntityInfoComponent::ms_kTemplateID);
        if( pkEntityInfoComponent == NULL )
            continue;

        //已经加入了VisibleArray
        if( pkEntityInfoComponent->GetFrameCount() == uiFrameCount )
            continue;

        pkEntityInfoComponent->SetFrameCount(uiFrameCount);
        pkEntity->Update(NULL, pkMap->GetTime(), pkMap->GetErrorHandler(), pkAssetManager);

        pkSceneRootPointer = NULL;
        pkSceneRoot = NULL;
        if (pkEntity->GetPropertyData(ms_kSceneRootPointerName, pkSceneRootPointer))
        {
            pkSceneRoot = NiDynamicCast(NiAVObject, pkSceneRootPointer);
            if(pkSceneRoot)
            {
                kCuller.CullModel(pkSceneRoot);
            }
        }
    }

    //sound emitter
    for(unsigned int i = 0; i < m_pkSoundEmitterEntities.GetSize(); ++i)
    {
        pkSceneRootPointer = NULL;
        pkSceneRoot = NULL;
        pkEntity = m_pkSoundEmitterEntities.GetAt(i);
        if (pkEntity && pkEntity->GetPropertyData(ms_kSceneRootPointerName, pkSceneRootPointer))
        {
            pkEntity->Update(NULL, pkMap->GetTime(), pkMap->GetErrorHandler(), pkAssetManager);
            pkSceneRoot = NiDynamicCast(NiAVObject, pkSceneRootPointer);
            if(pkSceneRoot)
            {
                pkSceneRoot->Cull(kCuller);
            }
        }
    }
}

void CChunk::BuildReflectVisibleSet(CCullingProcess& kCuller)
{
    CBox kBoundBox = m_kBoundBox;
    kBoundBox.m_kMax.z += 200.f;

    const NiFrustumPlanes& kPlanes = kCuller.GetReflectFrustumPlanes();

    unsigned int i;
    for (i = 0; i < NiFrustumPlanes::MAX_PLANES; ++i)
    {
        int iSide = kBoundBox.WhichSide(kPlanes.GetPlane(i));

        if (iSide == NiPlane::NEGATIVE_SIDE)
        {
            // The object is not visible since it is on the negative
            // side of the plane.
            break;
        }
    }

    if(i < NiFrustumPlanes::MAX_PLANES)
        return;

    //计算地形本身是否可见
    CreateGeometry();
    BuildTextureProperty();

    NiGeometryData* pkGeometryData = m_spGeometry->GetModelData();
    if( pkGeometryData->GetRevisionID() )
    {
        pkGeometryData->GetBound().ComputeFromData(VERTEX_COUNT * VERTEX_COUNT, m_pkPositionList);
        m_spGeometry->Update(0.f, false);
    }

    for (i = 0; i < NiFrustumPlanes::MAX_PLANES; ++i)
    {
        int iSide = m_kBoundBox.WhichSide(kPlanes.GetPlane(i));

        if (iSide == NiPlane::NEGATIVE_SIDE)
        {
            // The object is not visible since it is on the negative
            // side of the plane.
            break;
        }
    }

    if( i == NiFrustumPlanes::MAX_PLANES )
        kCuller.OnReflectChunkVisible(this);

    if((CMap::GetVisibleMask() & NiGeneralEntity::SceneObject) == 0)
        return;

    //处理反射模型
    NiEntityInterface* pkEntity;
    NiObject* pkSceneRootPointer;
    NiAVObject* pkSceneRoot;

    CMap* pkMap = CMap::Get();
    unsigned int uiFrameCount = pkMap->GetFrameCount();
    CEntityInfoComponent* pkEntityInfoComponent;
    for( ModelList::const_iterator itr = m_kModelList.begin(); itr != m_kModelList.end(); ++itr )
    {
        pkEntity = pkMap->GetEntity(itr->iTileX, itr->iTileY, itr->uiIndex);
        if( !pkEntity || pkEntity->GetHidden() )
            continue;

        pkEntityInfoComponent = (CEntityInfoComponent*)pkEntity->GetComponentByTemplateID(CEntityInfoComponent::ms_kTemplateID);
        if( pkEntityInfoComponent == NULL )
            continue;

        //已经加入了VisibleArray
        if( pkEntityInfoComponent->GetFrameCount() == uiFrameCount )
            continue;

        pkEntityInfoComponent->SetFrameCount(uiFrameCount);

        //查看是否反射
        if(!pkEntityInfoComponent->GetReflect())
            continue;

        pkSceneRootPointer = NULL;
        pkSceneRoot = NULL;
        if (pkEntity->GetPropertyData(ms_kSceneRootPointerName, pkSceneRootPointer))
        {
            pkSceneRoot = NiDynamicCast(NiAVObject, pkSceneRootPointer);
            if(pkSceneRoot)
                kCuller.CullReflectModel(pkSceneRoot);
        }
    }
}

void CChunk::DrawGrass(CGrassMesh& kGrassMesh) const
{
    if( m_kGrass.GetGrassCount() == 0 )
        return;

    NiPoint3 kCenter;
    const unsigned int* puiBits = m_kGrass.GetMaskBits();
    const unsigned short* pusGrass = m_kGrass.GetGrassInstances();
    for(unsigned int i = 0; i < 32; ++i)
    {
        unsigned int uiBit = puiBits[i];
        for(unsigned int j = 0; j < 32; ++j)
        {
            if(uiBit & (1 << j))
            {
                kCenter.x = m_kBasePoint.x + i;
                kCenter.y = m_kBasePoint.y + j;
                kCenter.z = GetHeight(kCenter.x, kCenter.y);
                kGrassMesh.AddGrass(kCenter, pusGrass[i*32 +j]);
            }
        }
    }
}

void CChunk::BuidWaterEdgeMap()
{
	if ( !m_pkWater)
		return;

	//bool bBuidDepthMap = false;

	////水面高度和地形相交 那么就生成深度图
	//for( int i = 0; i < ALPHA_MAP_SIZE; i++ )
	//{
	//	for( int j = 0; j < ALPHA_MAP_SIZE; j++ )
	//	{
	//		float fRealX = m_kBasePoint.x + float(j) * 0.5f;
	//		float fRealY = m_kBasePoint.y + float(i) * 0.5f;

	//		float fWH = GetWaterHeight(fRealX, fRealY); //获得当前水面的高度

	//		if ( fWH > m_kBoundBox.m_kMin.z && fWH < m_kBoundBox.m_kMax.z )
	//		{
	//			bBuidDepthMap = true;
	//			break;
	//		}
	//		
	//	}
	//}

	//if ( !bBuidDepthMap )
	//	return;


	//先创建贴图
	//然后根据高度求出贴图对应的点的颜色。
	if ( !m_apkWaterEdgeMapDatas )
	{
		m_apkWaterEdgeMapDatas = NiNew NiPixelData(ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, NiPixelFormat::ARGB8888, 1, 1);
		m_apkWaterEdgeMapDatas->IncRefCount();
	}

	if ( !m_apkWaterEdgeMapTextures )
	{
		m_apkWaterEdgeMapTextures = CAlphaTexture::Create( m_apkWaterEdgeMapDatas );
	}

	DWORD* pucChannels = (DWORD*)m_apkWaterEdgeMapDatas->GetPixels(0, 0);

	for( int i = 0; i < ALPHA_MAP_SIZE; i++ )
	{
		for( int j = 0; j < ALPHA_MAP_SIZE; j++ )
		{
			float fRealX = m_kBasePoint.x + float(j) * 0.5f;
			float fRealY = m_kBasePoint.y + float(i) * 0.5f;

			float fTH = GetHeight(fRealX, fRealY); //获得当前点地形的高度
			float fWH = GetWaterHeight(fRealX, fRealY); //获得当前水面的高度

			float fDepth = fWH - fTH; //水面高度 - 地形高度 得到深度值

			//if ( fDepth > 5.0f )
			//	fDepth = 5.0f;
			float fCent = 255.0f / 1.0f;

			fDepth = NiClamp(fDepth, 0.0f, 1.0);//把深度控制在这个区间内

			fDepth =  ::abs( fDepth );

			fDepth = fDepth * fCent;

			if ( fDepth > 255.0f )
				fDepth = 255.0f;

			unsigned char ucAlpha = (unsigned char)fDepth;

			DWORD dwColor = 0xffff0000;
			dwColor += ucAlpha;

			pucChannels[ i * ALPHA_MAP_SIZE + j] = dwColor;
		}
	}

	if ( m_apkWaterEdgeMapDatas )
		m_apkWaterEdgeMapDatas->MarkAsChanged();

}

void CChunk::AddEntity(int x, int y, unsigned int idx)
{
    //检查重复添加
    for( ModelList::const_iterator itr = m_kModelList.begin(); itr != m_kModelList.end(); ++itr )
    {
        if( itr->iTileX == x
            && itr->iTileY == y
            && itr->uiIndex == idx )
            return;
    }

    m_kModelList.push_back(ModelRef(x, y, idx));
    m_pkTile->SetDirty(true);
}

void CChunk::RemoveEntity(int x, int y, unsigned int idx)
{
    for( ModelList::iterator itr = m_kModelList.begin(); itr != m_kModelList.end(); ++itr )
    {
        if( itr->iTileX == x
         && itr->iTileY == y
         && itr->uiIndex == idx )
        {
            m_kModelList.erase(itr);
            m_pkTile->SetDirty(true);
            break;
        }
    }
}

void CChunk::ChangeEntityIndex(int x, int y, unsigned int uiOldIndex, unsigned int uiNewIndex)
{
    for( ModelList::iterator itr = m_kModelList.begin(); itr != m_kModelList.end(); ++itr )
    {
        if( itr->iTileX == x
         && itr->iTileY == y
         && itr->uiIndex == uiOldIndex )
        {
            itr->uiIndex = uiNewIndex;
            m_pkTile->SetDirty(true);
        }
    }
}

void CChunk::UpdateEditorInfo()
{
    CMap* pkMap = CMap::Get();
    NiEntityInterface* pkEntity;
    NiEntityComponentInterface* pkEditorInfoComponent;
    unsigned int uiCount;
    NiPoint2 kData;
    bool bExist;
    for( ModelList::iterator itr = m_kModelList.begin(); itr != m_kModelList.end(); ++itr )
    {
        pkEntity = pkMap->GetEntity(itr->iTileX, itr->iTileY, itr->uiIndex);
        if( pkEntity == NULL )
            continue;

        pkEditorInfoComponent = pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
        if( !pkEditorInfoComponent )
        {
            pkEditorInfoComponent = NiNew CEditorInfoComponent;
            pkEntity->AddComponent(pkEditorInfoComponent);
        }

        bExist = false;
        uiCount = 0;
        pkEditorInfoComponent->GetElementCount(CEditorInfoComponent::ms_kQuadTreeChunksName, uiCount);
        for( unsigned int ui = 0; ui < uiCount; ++ui )
        {
            kData.x = -1.f;
            kData.y = -1.f;
            pkEditorInfoComponent->GetPropertyData(CEditorInfoComponent::ms_kQuadTreeChunksName, kData, ui);

            if( int(kData.x) == int(m_kBoundBox.m_kMin.x)
             && int(kData.y) == int(m_kBoundBox.m_kMin.y) )
            {
                bExist = true;
                break;
            }
        }

        if( !bExist )
        {
            kData.x = m_kBoundBox.m_kMin.x;
            kData.y = m_kBoundBox.m_kMin.y;
            pkEditorInfoComponent->SetPropertyData(CEditorInfoComponent::ms_kQuadTreeChunksName, kData, uiCount);
        }
    }

    kData.x = m_kBoundBox.m_kMin.x;
    kData.y = m_kBoundBox.m_kMin.y;
    for( unsigned int i = 0; i < m_pkSoundEmitterEntities.GetSize(); ++i )
    {
        pkEntity = m_pkSoundEmitterEntities.GetAt(i);
        pkEditorInfoComponent = pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
        if( !pkEditorInfoComponent )
        {
            pkEditorInfoComponent = NiNew CEditorInfoComponent;
            pkEntity->AddComponent(pkEditorInfoComponent);
        }

        pkEditorInfoComponent->SetPropertyData(CEditorInfoComponent::ms_kChunkPropertyName, kData, 0);
    }
}

void CChunk::Precache()
{
	CreateGeometry();
	BuildTextureProperty();

	NIASSERT(m_spGeometry);
	NiGeometryData* pkData = m_spGeometry->GetModelData();

	NiGeometryGroup* pkGroup = NiUnsharedGeometryGroup::Create();
	NiGeometryGroupManager* pkGeometryGroupManager = NiGeometryGroupManager::GetManager();
	pkGeometryGroupManager->AddObjectToGroup(pkGroup, pkData);

	ms_pkTerrainShader->PrepareGeometryForRendering(m_spGeometry, NULL, (NiGeometryBufferData*)pkData->GetRendererData(), NULL);

	if(m_pkWater)
		m_pkWater->Precache();
}

void CChunk::CalcMoveable(int nLv)
{
    unsigned char* pucAlpha = m_pkGridPixelData[nLv]->GetPixels();
	bool bModify = false;
	float x, y;
	float h;
	NiPoint3 n;
	for(int X = 0; X < GRID_COUNT; X++)
	{
		for(int Y = 0; Y < GRID_COUNT; Y++)
		{
			if(pucAlpha[Y*GRID_COUNT + X])
				continue;

			x = m_kBasePoint.x + float(X*GRID_SIZE + GRID_SIZE/2);
			y = m_kBasePoint.y + float(Y*GRID_SIZE + GRID_SIZE/2);
			if(!GetHeightAndNormal(x, y, h, n))
				continue;

			if(n.z < MIN_WALK_NORMAL)
			{
				pucAlpha[Y*GRID_COUNT + X] = 85;
				bModify = true;
			}
		}
	}

	if(bModify)
	{
		m_pkGridPixelData[nLv]->MarkAsChanged();
		m_pkTile->SetDirty(true);
	}
}

void CChunk::ClearMoveable(int nLv)
{
    unsigned char* pucAlpha = m_pkGridPixelData[nLv]->GetPixels();
	memset(pucAlpha, 0, m_pkGridPixelData[nLv]->GetSizeInBytes());

	m_pkGridPixelData[nLv]->MarkAsChanged();
	m_pkTile->SetDirty(true);
}

void CChunk::AddSoundEmitter(NiEntityInterface* pkEntity)
{
    //检查重复添加
	for( unsigned int i = 0; i < m_pkSoundEmitterEntities.GetSize(); ++i )
	{
		if( pkEntity == m_pkSoundEmitterEntities.GetAt(i) )
			return;
	}

	pkEntity->AddReference();
	m_pkSoundEmitterEntities.Add(pkEntity);

    m_pkTile->SetDirty(true);
}

void CChunk::RemoveSoundEmitter(NiEntityInterface* pkEntity)
{
    bool bFind = false;
	for( unsigned int i = 0; i < m_pkSoundEmitterEntities.GetSize(); ++i )
	{
        if( m_pkSoundEmitterEntities.GetAt(i) == pkEntity )
        {
            pkEntity->RemoveReference();
            m_pkSoundEmitterEntities.SetAt(i, NULL);
            bFind = true;
        }
	}

    if(bFind)
    {
        m_pkTile->SetDirty(true);
        m_pkSoundEmitterEntities.Compact();
    }
}

void CChunk::GetEntities(NiScene *pkScene)
{
    NiEntityInterface* pkEntity;

    //Sound Emitter
    for(unsigned int i = 0; i < m_pkSoundEmitterEntities.GetSize(); i++)
    {
        pkEntity = m_pkSoundEmitterEntities.GetAt(i);
        if( TerrainHelper::MakeUniqueName(pkScene, pkEntity) )
        {
            pkScene->AddEntity(pkEntity);
        }
    }
}


//静态初始化
void CChunk::_SDMInit()
{
    ms_kSceneRootPointerName = "Scene Root Pointer";

    //阻止纹理数据被删除
    NiSourceTexture::SetDestroyAppDataFlag(false);

    ms_pusTriList = NiAlloc(unsigned short, (VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2*3);
    memset(ms_pusTriList, 0, (VERTEX_COUNT - 1)*(VERTEX_COUNT - 1)*2*3*sizeof(unsigned short));

    //generate triangle indices
    unsigned short idx = 0;
    for( unsigned short i = 0; i < VERTEX_COUNT - 1; i++ )
    {
        for( unsigned short j = 0; j < VERTEX_COUNT - 1; j++ )
        {
            // 1   3
            // *---*
            // |   |
            // *---*
            // 0   2

            //triangle 012
            ms_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 0);
            ms_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 0);
            ms_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 1);

            //triangle 321
            ms_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 1);
            ms_pusTriList[idx++] = (i + 0)*VERTEX_COUNT + (j + 1);
            ms_pusTriList[idx++] = (i + 1)*VERTEX_COUNT + (j + 0);
        }
    }
}

void CChunk::_SDMShutdown()
{
    ms_kSceneRootPointerName = NULL;

    NiFree(ms_pusTriList);

    if( ms_pkTerrainShader )
        ms_pkTerrainShader->DecRefCount();
}
