
#ifndef CHUNK_H
#define CHUNK_H

#include "TerrainLibType.h"
#include "TagStream.h"
#include "Box.h"
#include "Collision.h"
#include "CullingProcess.h"
#include "GrassInfo.h"
#include "GrassMesh.h"
#include <list>

#define INVALID_INDEX (0xFFFFFFFF)

#define MAX_LAYERS			(6)
#define ALPHA_MAP_SIZE		(64)

#define UNIT_SIZE			(4)
#define UNIT_COUNT			(8)
#define VERTEX_COUNT		(UNIT_COUNT + 1) //9

#define CHUNK_SIZE			(UNIT_COUNT * UNIT_SIZE) //32
#define CHUNK_COUNT			(16)

#define TILE_SIZE			(CHUNK_COUNT * CHUNK_SIZE) //512
#define TILE_COUNT			(64)

#define MAP_SIZE			(TILE_COUNT * TILE_SIZE) //32768

#define STRIP_SIZE		    ((VERTEX_COUNT - 1)/2*(VERTEX_COUNT*2*2 + 4) - 2) //158

#define ALIGN( X, ALIGNMENT ) ( int(int(X) & ~(ALIGNMENT-1)) )

#define INVALID_LAYER		(0xFFFFFFFF)

#define GRID_SIZE		(1)
#define GRID_COUNT      (CHUNK_SIZE/GRID_SIZE) //32
#define LIGHT_DIR		NiPoint3(0.577350f, 0.577350f, -0.577350f) //NiPoint3(1.f, 1.f, -1.f).Unitize()

#define MakeVersion(major, minor) ((unsigned int(major) << 8) + unsigned int(minor))

enum
{
	WalkGrid = 0,
	FlyGrid,
	MaxGrid
};

struct CChunkHeader
{
	unsigned int m_uiTurnEdgeFlag1;
	unsigned int m_uiTurnEdgeFlag2;
	unsigned int m_uiHoleFlag1;
	unsigned int m_uiHoleFlag2;
	unsigned int m_uiAreaId;
};

struct ModelRef
{
	int iTileX;
	int iTileY;
	unsigned int uiIndex;

	ModelRef(int iX, int iY, unsigned int uiIdx)
		:iTileX(iX)
		,iTileY(iY)
		,uiIndex(uiIdx)
	{
	}

	ModelRef()
		:iTileX(INVALID_INDEX)
		,iTileY(INVALID_INDEX)
		,uiIndex(INVALID_INDEX)
	{
	}

	ModelRef(const ModelRef& other)
		:iTileX(other.iTileX)
		,iTileY(other.iTileY)
		,uiIndex(other.uiIndex)
	{
	}
};

typedef std::list<ModelRef> ModelList; 

//(x,y+1,z3)	(x+1,y+1,z2)  
//   *------------*
//   |            |
//   |            |
//   |            |
//   |            |
//   |            |
//   *------------*
//(x, y, z0)  (x+1, y, z1)
class CQuad
{
public:
	float x;
	float y;
	float z0;
	float z1;
	float z2;
	float z3;

	void GetBoundBox(CBox& box) const;
	bool CollideLine(const NiPoint3 &start, const NiPoint3 &end, float& fTime) const;
	bool CollideBox(const NiPoint3 &start, const NiPoint3 &end, const NiPoint3& extent, Result& result) const;
	void MultiCollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList) const;
};

class CTile;
class CWater;
class CTerrainShader;
struct FIBITMAP;

class TERRAIN_ENTRY CChunk
{
public:
	CChunk();
	virtual ~CChunk();

	void Set(int x, int y, CTile* pkTile);
	int GetX() const { return m_iX; }
	int GetY() const { return m_iY; }

	bool Load(NiBinaryStream* pkStream);
	bool Save(NiBinaryStream* pkStream);

	//保存服务器场景
	void ReadGrid(CTagStream& kTagStream, int nLv);
	void WriteGrid(CTagStream& kTagStream, int nLv);

	void OnCreate();

	bool GetVertexIndex(float x, float y, int& iIndex) const;

	float GetHeight(float x, float y) const;
	bool GetHeight(float x, float y, float& h) const;
	bool GetHeightAndNormal(float x, float y, float&h, NiPoint3& kNormal) const;
	void SetHeight(float x, float y, float h);
	void UpdateNormal(float x, float y);

	float GetWaterHeight(float x, float y);
	bool GetWaterHeight(float x, float y, float& h);
	void SetWaterHeight(float x, float y, float h);
	void SetWaterFlag(float x, float y, bool bHasWater, unsigned int uiWaterIndex);
	bool GetWaterFlag(float x, float y);
	void OnWaterChanged(unsigned int uiWaterIndex);
    CWater* GetWater() const { return m_pkWater; }

	bool IsTurnEdge(int x, int y) const;
	bool TurnEdge(int x, int y, bool bTurn, bool bForce = false);
	bool SetTurnEdgeFlag(int x, int y, bool bTurnEdge);

	bool IsHole(int x, int y) const;
	bool ExcavateHole(int x, int y, bool bHole, bool bForce = false);
	bool SetHoleFlag(int x, int y, bool bHole);

	unsigned int AddLayer(const char* pcTextureName);
	void RemoveLayer(unsigned int uiLayer);
	void PureLayers(); //删除没有用的层
    unsigned int GetNumLayers() const { return m_uiNumLayers; }
    unsigned int GetTextureId(unsigned int uiLayer) const { NIASSERT(uiLayer <  m_uiNumLayers); return m_auiLayerTextures[uiLayer]; }
    NiTexture* GetAlphaTexture(unsigned int uiLayer) const { NIASSERT(uiLayer <  m_uiNumLayers && uiLayer > 0); return m_apkAlphaTextures[uiLayer - 1]; }

	void PaintMaterial(int x, int y, FIBITMAP* pkAlphaMap, const char* pcTextureName, float fScale);
	void RemoveMaterial(const char* pcTextureName);

	void SetShadow(unsigned char* pucChunkShadow);
    NiTexture* GetShadowTexture() const { return m_pkShadowTexture; }

	NiTexture*	GetWaterEdgeMapTexture() const { return m_apkWaterEdgeMapTextures; }

	void SetMoveable(float x, float y, bool bMoveable, int nLv);
    bool GetMoveable(float x, float y, int nLv);
	void SetGridInfo(const unsigned char* pucGridInfo, int nLv);
    NiTexture* GetGridTexture(int nLv) const { return m_pkGridTexture[nLv]; }

	//Tile的某个Texture删除了
	void OnDeleteTexture(unsigned int uiTexture);

	bool GetQuad(float inX, float inY, CQuad& quad) const;
	bool CollideLine(const NiPoint3 &start, const NiPoint3 &end, float& fTime);
	bool CollideBox(const NiPoint3 &start, const NiPoint3 &end, const NiPoint3& extent, Result& result);
	void MultiCollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList);
	void MultiCollideBoxTerrain(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList);
	void MultiCollideBoxModels(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList);
	void CalcShadow(const NiPoint3 &start, const NiPoint3 &end, float& fShadow);

	NiTexture* CreateAlphaTexture(NiPixelData* pkPixelData);
	void BuildTextureProperty();
	void CreateGeometry();
	NiGeometry* GetGeometry() const { return m_spGeometry; }
	void BuildVisibleSet(CCullingProcess& kCuller);
    void BuildReflectVisibleSet(CCullingProcess& kCuller);
    void DrawGrass(CGrassMesh& kGrassMesh) const;

	//Add By Hays
	void BuidWaterEdgeMap(); //生成水的深度图虚化

	void AddEntity(int x, int y, unsigned int idx);
	void RemoveEntity(int x, int y, unsigned int idx);
	void ChangeEntityIndex(int x, int y, unsigned int uiOldIndex, unsigned int uiNewIndex);
	const ModelList& GetModelList() const { return m_kModelList; }

	void UpdateEditorInfo();

	const CBox& GetBoundBox() const { return m_kBoundBox; }
    CTile* GetTile() { return m_pkTile; }

	void Precache();
	void CalcMoveable(int nLv);
	void ClearMoveable(int nLv);

	unsigned int GetAreaId() const { return m_kHeader.m_uiAreaId; }
	void SetAreaId(unsigned int uiAreaId);

    bool PaintGrass(int x, int y, unsigned short usGrass);

    void AddSoundEmitter(NiEntityInterface* pkEntity);
    void RemoveSoundEmitter(NiEntityInterface* pkEntity);

    void GetEntities(NiScene *pkScene);

	static void _SDMInit();
	static void _SDMShutdown();

private:
	int			m_iX;
	int			m_iY;

	CChunkHeader m_kHeader;

	CBox		m_kBoundBox;

	CTile*		m_pkTile;

	bool m_bTextureChanged;


	NiPoint3 m_kBasePoint;
	NiPoint3* m_pkPositionList; //顶点和法线
	NiPoint3* m_pkNormalList;
	unsigned short* m_pusTriList;

	unsigned int m_uiNumLayers;
	unsigned int m_auiLayerTextures[MAX_LAYERS];
	NiPixelData* m_apkAlphaMapDatas[MAX_LAYERS - 1];
    NiSourceTexture* m_apkAlphaTextures[MAX_LAYERS - 1];
	unsigned int m_AlphaSum[MAX_LAYERS - 1];

	NiPixelData* m_pkShadowMapData;
    NiSourceTexture* m_pkShadowTexture;

	NiPixelData* m_apkWaterEdgeMapDatas;
	NiSourceTexture* m_apkWaterEdgeMapTextures;

	NiTriShapePtr	m_spGeometry;

	CWater*			m_pkWater;

	ModelList		m_kModelList;

	//格点信息
	NiPixelData* m_pkGridPixelData[MaxGrid];
    NiSourceTexture* m_pkGridTexture[MaxGrid];

    //草地
    CGrassInfo m_kGrass;

    NiTPrimitiveArray<NiEntityInterface*> m_pkSoundEmitterEntities;

	static NiFixedString ms_kSceneRootPointerName;
	static CTerrainShader*  ms_pkTerrainShader;

	static unsigned short*	ms_pusTriList;
};

#endif