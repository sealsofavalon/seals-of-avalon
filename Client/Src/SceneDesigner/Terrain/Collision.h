#ifndef COLLISION_H
#define COLLISION_H

#include <vector>

#define DELTA (0.00001)

struct Result
{
	float fTime;
	NiPoint3 kNormal;

	Result(float t, const NiPoint3& n)
		:fTime(t)
		,kNormal(n)
	{
	}

	Result()
		:fTime(0.f)
		,kNormal(0.f, 0.f, 0.f)
	{
	}

	Result(const Result& other)
		:fTime(other.fTime)
		,kNormal(other.kNormal)
	{
	}
};

struct ResultList : public std::vector<Result>
{
    bool Compare(const Result& r1, const Result& r2, bool bMinZ) const
    {
        if( r1.fTime < r2.fTime )
            return true;

        if( r1.fTime > r2.fTime )
            return false;

        if(bMinZ)
            return NiAbs(r1.kNormal.z) <= NiAbs(r2.kNormal.z);
        else
            return r1.kNormal.z >= r2.kNormal.z;
    }

	void Sort(bool bMinZ)
	{
		unsigned int uiSize = size();
		for( unsigned int i = 0; i < uiSize; ++i )
		{
			at(i).kNormal.Unitize();
		}

		Result t;
		for(unsigned int i = 0; i < uiSize; i++)
		{
			for(unsigned int j = i + 1; j < uiSize; j++)
			{
				if( Compare(at(j), at(i), bMinZ) )
				{
					t = at(i);
					at(i) = at(j);
					at(j) = t;
				}
			}
		}
	}
};

float LineCheckWithTriangle(const NiPoint3& start,const NiPoint3& end, const NiPoint3& v0,const NiPoint3& v1,const NiPoint3& v2);

float FindSeparatingAxis(
	const NiPoint3& V0,
	const NiPoint3& V1,
	const NiPoint3& V2,
	const NiPoint3& Start,
	const NiPoint3& End,
	const NiPoint3& BoxExtent,
	const NiPoint3& BoxX,
	const NiPoint3& BoxY,
	const NiPoint3& BoxZ
	);


void MultiCollide(NiAVObject* pkAVObject, const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList);

void CreateCollisionData(NiAVObject* pkAVObject);
void CreateCollisionData(NiEntityInterface* pkEntity);
void CollectCollisionNode(NiAVObject* pkAVObject, std::vector<NiAVObject*>& vList);

enum AlphaMode
{
	ALPHA_NONE, //没有alpha
	ALPHA_TEST, //alpha test
	ALPHA_BLEND, //alpha blend
	ALPHA_IGNORE, //忽略掉这个 Geometry 的阴影
};
AlphaMode GetAlphaMode(NiGeometry* pkGeometry);
void CalcModelShadow(NiAVObject* pkAVObject, const NiPoint3& kOrigin, const NiPoint3& kDir, float& fShadow);


#endif //COLLISION_H