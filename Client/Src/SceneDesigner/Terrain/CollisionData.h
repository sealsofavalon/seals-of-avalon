#ifndef C0LLISION_DATA_H
#define C0LLISION_DATA_H

#include "TerrainLibType.h"
#include "Box.h"
#include "Collision.h"

#define GetCollisionData(pkAVObject) ((CCollisionData*)(pkAVObject->GetCollisionObject()))

class TERRAIN_ENTRY CCollisionData : public NiCollisionObject
{
    NiDeclareRTTI;
    NiDeclareClone(CCollisionData);

public:
    CCollisionData(NiAVObject* pkSceneObject);
    virtual ~CCollisionData();

    virtual void SetSceneGraphObject(NiAVObject* pkSceneObject);

	virtual void UpdateWorldData() {}
	virtual void RecreateWorldData() {}

	void CreateWorldVertices();
	void UpdateWorldVertices();
	void UpdateBoundBox();

	CBox& GetBoundBox() { return m_BoundBox; }
	void MultiCollide(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList);

protected:
    // To prevent public access to a constructor with no scene association
    CCollisionData();

	CBox m_BoundBox;
	
	// world geometry data
    NiPoint3* m_pkWorldVertex;
	bool m_bWorldVerticesNeedUpdate;

	unsigned int m_usNumVertices;
	unsigned int m_usNumTriangles;
};

#endif //C0LLISION_DATA_H

