#include "TerrainPCH.h"
#include "CollisionShader.h"
#include "TerrainHelper.h"

bool CCollisionShader::ms_bShowCollision = false;
bool CCollisionShader::ms_bShowWireframe = true;
NiPropertyState* CCollisionShader::ms_pkPropertyState = NULL;
CCollisionShader* CCollisionShader::ms_pkThis = NULL;

CCollisionShader::CCollisionShader()
{
    SetName("CollisionShader");
    m_bIsBestImplementation = true;
}

unsigned int CCollisionShader::PreProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    // Safety catch - fail if the shader hasn't been initialized
    if (!m_bInitialized)
    {
        return 0xFFFFFFFF;
    }

    m_pkD3DRenderState->UpdateRenderState(pkState);
    if (m_pkD3DRenderer->GetLightManager())
    {
        m_pkD3DRenderer->GetLightManager()->SetState(pkEffects, 
            pkState->GetTexturing(), pkState->GetVertexColor());
    }
    m_pkD3DRenderState->SetTexture(0, NULL);
    m_pkD3DRenderState->SetRenderState(D3DRS_DEPTHBIAS, F2DW(-0.0001f));

    m_pkD3DRenderState->SetVertexShader(NULL);
    m_pkD3DRenderState->SetPixelShader(NULL);

    return 0;
}

unsigned int CCollisionShader::PostProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    m_pkD3DRenderState->SetRenderState(D3DRS_DEPTHBIAS, F2DW(0.f));
    return 0;
}

unsigned int CCollisionShader::SetupTransformations(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        const NiSkinPartition::Partition* pkPartition, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects, 
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    m_pkD3DRenderer->SetModelTransform(kWorld);
    return 0;
}

NiGeometryData::RendererData* CCollisionShader::PrepareGeometryForRendering(
        NiGeometry* pkGeometry, const NiSkinPartition::Partition* pkPartition,
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState)
{
    NiGeometryBufferData* pkBuffData = (NiGeometryBufferData*)pkRendererData;
    NIASSERT(pkBuffData);
    // On the first pass, the geometry should be packed. This call is a 
    // safety net to catch geometry that was not precached.
    // If the geometry has already been packed, this part of the function 
    // will quick-out.
    // 
    // Special case: volatile HW-skinned geometry must be repacked each pass
    //
    // Each pass, the stream sources and index buffers (if they apply) should
    // be set in this function.
    if (pkGeometry != NULL)
    {
        NiGeometryData* pkData = pkGeometry->GetModelData();
        NiSkinInstance* pkSkinInstance = pkGeometry->GetSkinInstance();

        NiD3DShaderDeclaration* pkDecl = 
            (NiD3DShaderDeclaration*)(pkGeometry->GetShaderDecl());
        if (!pkDecl)
        {
            NIVERIFY(NiShaderDeclaration::CreateDeclForGeometry(pkGeometry));
            pkDecl = (NiD3DShaderDeclaration*)(pkGeometry->GetShaderDecl());
        }

        if (pkPartition)
        {
            NiSkinPartition::Partition* pkSkinPartition = 
                (NiSkinPartition::Partition*)pkPartition;

            pkBuffData = (NiGeometryBufferData*)pkPartition->m_pkBuffData;

            m_pkD3DRenderer->PackSkinnedGeometryBuffer(pkBuffData,
                pkData, pkSkinInstance, pkSkinPartition,
                pkDecl);
        }
        else
        {
            pkBuffData = (NiGeometryBufferData*)pkData->GetRendererData();

            m_pkD3DRenderer->PackGeometryBuffer(pkBuffData, pkData, 
                pkSkinInstance, pkDecl);
        }
    }

    if (pkBuffData)
    {
        // Set the streams
        for (unsigned int i = 0; i < pkBuffData->GetStreamCount(); i++)
        {
            m_pkD3DDevice->SetStreamSource(i, 
                pkBuffData->GetVBChip(i)->GetVB(), 0, 
                pkBuffData->GetVertexStride(i));
        }
        m_pkD3DDevice->SetIndices(pkBuffData->GetIB());
        NIMETRICS_DX9RENDERER_AGGREGATEVALUE(VERTEX_BUFFER_CHANGES, 
            pkBuffData->GetStreamCount());
    }

    return pkBuffData;
}

    // Advance to the next pass.
unsigned int CCollisionShader::FirstPass()
{
    if(ms_bShowCollision)
        return 1;

    return 0;
}

unsigned int CCollisionShader::NextPass()
{
    return 0;
}

bool CCollisionShader::SetShowCollision(bool bShowCollision)
{
    if(BOOL_EQUATION(ms_bShowCollision, bShowCollision))
        return false;

    ms_bShowCollision = bShowCollision;
    return true;
}

bool CCollisionShader::SetShowWireframe(bool bShowWireframe)
{
    if(BOOL_EQUATION(ms_bShowWireframe, bShowWireframe))
        return false;

    ms_bShowWireframe = bShowWireframe;
    if(ms_pkPropertyState)
        ms_pkPropertyState->GetWireframe()->SetWireframe(ms_bShowWireframe);

    return true;
}

void CCollisionShader::_SDMInit()
{
    ms_pkThis = NiNew CCollisionShader;
    ms_pkThis->IncRefCount();

    ms_pkPropertyState = NiNew NiPropertyState;
    ms_pkPropertyState->IncRefCount();

    ms_pkPropertyState->SetProperty(NiNew NiMaterialProperty);
    ms_pkPropertyState->SetProperty(NiNew NiVertexColorProperty);
    ms_pkPropertyState->SetProperty(NiNew NiWireframeProperty);
    ms_pkPropertyState->SetProperty(NiNew NiStencilProperty);

    ms_pkPropertyState->GetMaterial()->SetEmittance(NiColor(106.f/255.f, 124.f/255.f, 73.f/255.f));
    ms_pkPropertyState->GetVertexColor()->SetSourceMode(NiVertexColorProperty::SOURCE_IGNORE);
    ms_pkPropertyState->GetVertexColor()->SetLightingMode(NiVertexColorProperty::LIGHTING_E);
    ms_pkPropertyState->GetWireframe()->SetWireframe(ms_bShowWireframe);
    ms_pkPropertyState->GetStencil()->SetDrawMode(NiStencilProperty::DRAW_BOTH);
}

void CCollisionShader::_SDMShutdown()
{
    ms_pkThis->DecRefCount();
    ms_pkThis = NULL;

    ms_pkPropertyState->DecRefCount();
    ms_pkPropertyState = NULL;
}
