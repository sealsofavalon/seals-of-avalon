#ifndef COLLISION_SHADER_H
#define COLLISION_SHADER_H

#include "TerrainLibType.h"

class TERRAIN_ENTRY CCollisionShader : public NiD3DShaderInterface
{
public:
    CCollisionShader();
    virtual ~CCollisionShader() {}
	
	virtual unsigned int PreProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound);

	virtual unsigned int PostProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound);

    virtual unsigned int SetupTransformations(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        const NiSkinPartition::Partition* pkPartition, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects, 
        const NiTransform& kWorld, const NiBound& kWorldBound);

    virtual NiGeometryData::RendererData* PrepareGeometryForRendering(
        NiGeometry* pkGeometry, const NiSkinPartition::Partition* pkPartition,
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState);

    // Advance to the next pass.
    virtual unsigned int FirstPass();
    virtual unsigned int NextPass();

	static bool SetShowCollision(bool bShowCollision);
    static bool GetShowCollision() { return ms_bShowCollision; }

	static bool SetShowWireframe(bool bShowWireframe);
    static bool GetShowWireframe() { return ms_bShowWireframe; }

    static NiPropertyState* GetPropertyState() { return ms_pkPropertyState; }

    static CCollisionShader* Get() { return ms_pkThis; } 
 	static void _SDMInit();
	static void _SDMShutdown();

private:
	static bool	ms_bShowCollision;
	static bool	ms_bShowWireframe;

    static CCollisionShader* ms_pkThis;
    static NiPropertyState* ms_pkPropertyState;
};

#endif