#include "TerrainPCH.h"
#include "CullingProcess.h"
#include "Water.h"
#include "WaterShader.h"
#include "Chunk.h"
#include "Player.h"
#include "Map.h"
#include "TerrainHelper.h"
#include "CollisionShader.h"
#include "CollisionData.h"

CCullingProcess::CCullingProcess(NiVisibleArray* pkVisibleSet)
    :NiCullingProcess(pkVisibleSet)
    ,m_pfDepths(NULL)
    ,m_uiAllocatedDepths(0)
{
    m_spReflectCamera = NiNew NiCamera;
}

void CCullingProcess::Reset(NiCamera* pkCamera)
{
	NIASSERT(pkCamera);
	m_pkCamera = pkCamera;
	SetFrustum(m_pkCamera->GetViewFrustum());

    m_kWaterArray.RemoveAll();
    m_bHasRRWater = false;

    m_kCollisionModels.RemoveAll();
    m_kVisibleChunks.RemoveAll();

    m_kReflectModels.RemoveAll();
    m_kReflectChunks.RemoveAll();

    m_spCamera = pkCamera;
}

void CCullingProcess::OnWaterVisible(CWater* pkWater)
{
    CWaterShader* pkShader = pkWater->GetShader();
    if(!pkShader->GetRR())
    {
        //如果没有反射折射效果
        m_pkVisibleSet->Add(*(pkWater->GetGeometry()));
        return;
    }

	m_kWaterArray.Add(*(pkWater->GetGeometry()));
	if(!m_bHasRRWater)
	{
	m_bHasRRWater = true;
	m_fRRWaterHeight = pkWater->GetAVGHeight();
	m_kRRWaterBase = pkWater->GetBasePoint();
	m_kRRWaterBase.z = m_fRRWaterHeight;
	return;
	}

	//寻找更近的反射水面
	NiPoint3 kPos = m_pkCamera->GetWorldLocation();
	NiPoint3 kWaterBase = pkWater->GetBasePoint();
	kWaterBase.z = pkWater->GetAVGHeight();
	if( (kPos - kWaterBase).SqrLength() < (kPos - m_kRRWaterBase).SqrLength() )
	{
		m_kRRWaterBase = kWaterBase;
		m_fRRWaterHeight = kWaterBase.z;
	}

   /* m_kWaterArray.Add(*(pkWater->GetGeometry()));
    if(!m_bHasRRWater)
    {
        m_bHasRRWater = true;
        m_fRRWaterHeight = pkWater->GetValidHeight();
        m_kRRWaterBase = pkWater->GetBasePoint();
        return;
    }

    //寻找更近的反射水面
    NiPoint3 kPos = m_spCamera->GetWorldLocation();
    if( (kPos - pkWater->GetBasePoint()).SqrLength() < (kPos - m_kRRWaterBase).SqrLength() )
    {
        m_fRRWaterHeight = pkWater->GetValidHeight();
        m_kRRWaterBase = pkWater->GetBasePoint();
    }*/

}

void CCullingProcess::SortWater()
{
    const unsigned int uiCount = m_kWaterArray.GetCount();

    // Initialize size of object depths array.
    if (m_uiAllocatedDepths < uiCount)
    {
        NiFree(m_pfDepths);
        m_pfDepths = NiAlloc(float, uiCount);
        m_uiAllocatedDepths = uiCount;
    }

    // Iterate over geometry array.
    NiPoint3 kWorldDir = m_spCamera->GetWorldDirection();
    for (unsigned int ui = 0; ui < uiCount; ui++)
    {
        NiGeometry& kGeometry = m_kWaterArray.GetAt(ui);

        const NiBound& kWorldBound = kGeometry.GetWorldBound();
        m_pfDepths[ui] = kWorldBound.GetCenter() * kWorldDir;
    }

    // Sort output array by depth.
    SortObjectsByDepth(m_kWaterArray, 0, m_kWaterArray.GetCount() - 1);
}

//---------------------------------------------------------------------------
void CCullingProcess::SortObjectsByDepth(NiVisibleArray& kArrayToSort, int l, int r)
{
    // This recursive function implements a quick sort of kArrayToSort. It is
    // taken directly from the function of the same name in
    // NiBackToFrontAccumulator except that it sorts back to front instead of
    // front to back.

    if (r > l)
    {
        int i = l - 1;
        int j = r + 1;
        float fPivot = ChoosePivot(l, r);

        while (true)
        {
            do 
            {
                j--;
            } while (fPivot > m_pfDepths[j]);

            do
            {
                i++;
            } while (m_pfDepths[i] > fPivot);

            if (i < j)
            {
                // Swap array elements.
                NiGeometry* pkTempObj = &kArrayToSort.GetAt(i);
                kArrayToSort.SetAt(i, kArrayToSort.GetAt(j));
                kArrayToSort.SetAt(j, *pkTempObj);

                float fTempDepth = m_pfDepths[i];
                m_pfDepths[i] = m_pfDepths[j];
                m_pfDepths[j] = fTempDepth;
            }
            else
            {
                break;
            }
        }

        if (j == r)
        {
            SortObjectsByDepth(kArrayToSort, l, j - 1);
        }
        else
        {
            SortObjectsByDepth(kArrayToSort, l, j);
            SortObjectsByDepth(kArrayToSort, j + 1, r);
        }
    }
}

//---------------------------------------------------------------------------
float CCullingProcess::ChoosePivot(int l, int r) const
{
    // Check the first, middle, and last element. Choose the one which falls
    // between the other two. This has a good chance of discouraging 
    // quadratic behavior from qsort.
    // In the case when all three are equal, this code chooses the middle
    // element, which will prevent quadratic behavior for a list with 
    // all elements equal.

    int m = (l + r) >> 1;

    const float fDepth_l = m_pfDepths[l];
    const float fDepth_r = m_pfDepths[r];
    const float fDepth_m = m_pfDepths[m];

    if (fDepth_l > fDepth_m)
    {
        if (fDepth_m > fDepth_r)
        {
            return fDepth_m;
        }
        else
        {
            if (fDepth_l > fDepth_r)
            {
                return fDepth_r;
            }
            else
            {
                return fDepth_l;
            }
        }
    }
    else
    {
        if (fDepth_l > fDepth_r)
        {
            return fDepth_l;
        }
        else
        {
            if (fDepth_m > fDepth_r)
            {
                return fDepth_r;
            }
            else
            {
                return fDepth_m;
            }
        }
    }
}

void CCullingProcess::UpdateReflectCamera()
{
    NiCamera* pkCamera = m_spCamera;
    pkCamera->Update(0.f, false);

    NiPoint3 kWorldLoc = pkCamera->GetWorldLocation();
    NiPoint3 kWorldDir = pkCamera->GetWorldDirection();
    NiPoint3 kWorldUp = pkCamera->GetWorldUpVector();

	float fHeightOff = kWorldLoc.z - GetRRWaterHeight();
	float fHeight = GetRRWaterHeight() - fHeightOff;

    kWorldLoc.z = kWorldLoc.z = GetRRWaterHeight()*2 - kWorldLoc.z;
    kWorldDir.z = -kWorldDir.z;
    kWorldUp.z = -kWorldUp.z;

    m_spReflectCamera->SetTranslate(kWorldLoc);
    m_spReflectCamera->Update(0.f, false);
    m_spReflectCamera->LookAtWorldPoint(kWorldLoc + kWorldDir, kWorldUp);

    m_spReflectCamera->SetViewFrustum(pkCamera->GetViewFrustum());
    m_spReflectCamera->SetViewPort(pkCamera->GetViewPort());
    m_spReflectCamera->Update(0.f, false);

    m_kReflectPlanes.Set(pkCamera->GetViewFrustum(), m_spReflectCamera->GetWorldTransform());
    m_kReflectPlanes.EnableAllPlanes();
}

void CCullingProcess::CullModel(NiAVObject* pkSceneRoot)
{
    if(pkSceneRoot == NULL)
        return;

    //判断是否是需要显示的碰撞盒
    CCollisionData* pkColData = NULL;
    if(pkSceneRoot->GetAppCulled())
    {
        pkColData = (CCollisionData*)GetCollisionData(pkSceneRoot);
        if(pkColData == NULL || !CCollisionShader::GetShowCollision())
            return;
    }

    unsigned int i;
    int iSide;
    unsigned int uiSaveActive = m_kPlanes.GetActivePlaneState();

    for (i = 0; i < NiFrustumPlanes::MAX_PLANES; i++)
    {
        if (m_kPlanes.IsPlaneActive(i))
        {
            iSide = pkSceneRoot->GetWorldBound().WhichSide(m_kPlanes.GetPlane(i));

            if (iSide == NiPlane::NEGATIVE_SIDE)
            {
                // The object is not visible since it is on the negative
                // side of the plane.
                break;
            }

            if (iSide == NiPlane::POSITIVE_SIDE)
            {
                // The object is fully on the positive side of the plane,
                // so there is no need to compare child objects to this
                // plane.
                m_kPlanes.DisablePlane(i);
            }
        }
    }

    if(i == NiFrustumPlanes::MAX_PLANES)
    {
        if(NiIsKindOf(NiBillboardNode, pkSceneRoot))
            pkSceneRoot->OnVisible(*this);
        else if(NiIsKindOf(NiNode, pkSceneRoot))
        {
            for(unsigned int ui = 0; ui < ((NiNode*)pkSceneRoot)->GetArrayCount(); ++ui)
            {
                CullModel(((NiNode*)pkSceneRoot)->GetAt(ui));
            }
        }
        else if(NiIsKindOf(NiGeometry, pkSceneRoot))
        {
            if( pkColData ) //碰撞盒单独显示
                m_kCollisionModels.Add(*(NiGeometry*)pkSceneRoot);
            else
                pkSceneRoot->OnVisible(*this);
        }
    }

    m_kPlanes.SetActivePlaneState(uiSaveActive);
}

void CCullingProcess::OnChunkVisible(CChunk* pkChunk)
{
    m_kVisibleChunks.Add(pkChunk);
    pkChunk->GetGeometry()->OnVisible(*this);
}

void CCullingProcess::OnReflectChunkVisible(CChunk* pkChunk)
{
    m_kReflectChunks.Add(*pkChunk->GetGeometry());
}

void CCullingProcess::CullReflectModel(NiAVObject* pkSceneRoot)
{
    if(pkSceneRoot == NULL || pkSceneRoot->GetAppCulled())
        return;

    unsigned int i;
    int iSide;
    unsigned int uiSaveActive = m_kReflectPlanes.GetActivePlaneState();

    for (i = 0; i < NiFrustumPlanes::MAX_PLANES; i++)
    {
        if (m_kReflectPlanes.IsPlaneActive(i))
        {
            iSide = pkSceneRoot->GetWorldBound().WhichSide(m_kReflectPlanes.GetPlane(i));

            if (iSide == NiPlane::NEGATIVE_SIDE)
            {
                // The object is not visible since it is on the negative
                // side of the plane.
                break;
            }

            if (iSide == NiPlane::POSITIVE_SIDE)
            {
                // The object is fully on the positive side of the plane,
                // so there is no need to compare child objects to this
                // plane.
                m_kReflectPlanes.DisablePlane(i);
            }
        }
    }

    if(i == NiFrustumPlanes::MAX_PLANES)
    {
        if(NiIsKindOf(NiNode, pkSceneRoot))
        {
            for(unsigned int ui = 0; ui < ((NiNode*)pkSceneRoot)->GetArrayCount(); ++ui)
            {
                CullReflectModel(((NiNode*)pkSceneRoot)->GetAt(ui));
            }
        }
        else if(NiIsKindOf(NiGeometry, pkSceneRoot))
        {
            m_kReflectModels.Add(*(NiGeometry*)pkSceneRoot);
        }
    }

    m_kReflectPlanes.SetActivePlaneState(uiSaveActive);
}

void CCullingProcess::BuildReflectVisibleSet(NiEntityRenderingContext* pkRenderingContext)
{
    if(!HasRRWater())
        return;

    UpdateReflectCamera();

    //水面以下
    if(m_spCamera->GetWorldLocation().z < m_fRRWaterHeight)
        return;

    CMap::Get()->BuildReflectVisibleSet(pkRenderingContext);

    if(g_pkPlayer)
    {
        NiAVObject* pkAVObject = g_pkPlayer->GetSceneRoot();
        if(!g_pkPlayer->IsFreeCameraMode() && pkAVObject)
        {
            TerrainHelper::SetFrameID(pkAVObject, 0);
            CullReflectModel(pkAVObject);
        }
    }
}