#ifndef CULLINGPROCESS_H
#define CULLINGPROCESS_H

#include "TerrainLibType.h"

class CWater;
class CChunk;
class TERRAIN_ENTRY CCullingProcess : public NiCullingProcess
{
private:
    NiVisibleArray m_kWaterArray; //可见的水面

    bool m_bHasRRWater; //是否有需要反射的水面可见
    float m_fRRWaterHeight; //需要反射的水面高度
    NiPoint3 m_kRRWaterBase;

    NiTPrimitiveArray<CChunk*> m_kVisibleChunks;
    NiVisibleArray m_kCollisionModels; //需要显示的碰撞盒

    NiVisibleArray m_kReflectChunks; //需要反射的地形
    NiVisibleArray m_kReflectModels; //需要反射的模型

    NiCameraPtr m_spCamera; //
    NiCameraPtr m_spReflectCamera; //反射Camera
    NiFrustumPlanes m_kReflectPlanes; //反射Camera组成的Frustum Planes

    //用来排序水面
    float* m_pfDepths;
    unsigned int m_uiAllocatedDepths;


public:
	CCullingProcess(NiVisibleArray* pkVisibleSet);
    virtual ~CCullingProcess()
    {
        if(m_pfDepths)
            NiFree(m_pfDepths);
    }

	CCullingProcess & operator=( const CCullingProcess & ) { return *this; }

	void Reset(NiCamera* pkCamera);
    void OnWaterVisible(CWater* pkWater);

    NiVisibleArray* GetWaterSet() { return &m_kWaterArray; }
    bool HasRRWater() const { return m_bHasRRWater; }
    float GetRRWaterHeight() const { return m_fRRWaterHeight; }
    const NiPoint3& GetRRWaterBase() const { return m_kRRWaterBase; }

    void SortWater();

    // Sorts objects in the specified array using the depth values stored in
    // m_kObjectDepths. Requires m_kObjectDepths to be properly populated.
    void SortObjectsByDepth(NiVisibleArray& kArrayToSort, int l, int r);
    float ChoosePivot(int l, int r) const;

    NiVisibleArray* GetReflectModelSet() { return &m_kReflectModels; }
    NiVisibleArray* GetReflectChunkSet() { return &m_kReflectChunks; }

    NiCamera* GetCamera() { return m_spCamera; }
    NiCamera* GetReflectCamera() { return m_spReflectCamera; }
    void UpdateReflectCamera();
    const NiFrustumPlanes& GetReflectFrustumPlanes() const { return m_kReflectPlanes; }

    void CullModel(NiAVObject* pkSceneRoot);
    void OnChunkVisible(CChunk* pkChunk);
    NiVisibleArray* GetCollisionSet() { return &m_kCollisionModels; }

    void OnReflectChunkVisible(CChunk* pkChunk);
    void CullReflectModel(NiAVObject* pkSceneRoot);

    void BuildReflectVisibleSet(NiEntityRenderingContext* pkRenderingContext);

    const NiTPrimitiveArray<CChunk*>& GetVisibleChunks() const { return m_kVisibleChunks; }
};

NiSmartPointer(CCullingProcess);

#endif