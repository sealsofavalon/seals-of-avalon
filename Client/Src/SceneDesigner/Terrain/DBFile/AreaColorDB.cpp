#include "TerrainPCH.h"
#include "TerrainHelper.h"
#include "AreaColorDB.h"

CAreaColorDB* CAreaColorDB::ms_pkThis = NULL;

CAreaColorDB::CAreaColorDB()
{
	ms_pkThis = this;
}

CAreaColorDB::~CAreaColorDB()
{
	ms_pkThis = NULL;
}


bool CAreaColorDB::ReadTopLine()
{
	char acTempToken[1024];
	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken, true) )
		return false;

	return true;
}

void CAreaColorDB::WriteTopLine()
{
	WriteToken("area_id");
	WriteToken("color", true);
}

bool CAreaColorDB::Load()
{
	//合成绝对路径
	char acFilename[NI_MAX_PATH];
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\Maps\\area_color.csv", TerrainHelper::GetServerPath());

	if( !CCSVFile::Load(acFilename) )
		return false;

	if( !ReadTopLine() )
		return false;

	m_kColorMap.RemoveAll();
	unsigned int uiAreaId;
	unsigned int uiColor;
	while(true)
	{
		if( !ReadUInt(uiAreaId) ) break; //area id
		if( !ReadUInt(uiColor, true) ) break; //color id

		m_kColorMap.SetAt(uiAreaId, uiColor);
	}

	return true;
}

bool CAreaColorDB::Save()
{
	Clear();
	WriteTopLine();

	unsigned int uiAreaID;
	unsigned int uiColor;
	NiTMapIterator kPos = m_kColorMap.GetFirstPos();
	while(kPos)
	{
		m_kColorMap.GetNext(kPos, uiAreaID, uiColor);
		WriteUInt(uiAreaID);
		WriteUInt(uiColor, true);
	}

	//创建目录
	char acPath[NI_MAX_PATH];
	NiSprintf(acPath, NI_MAX_PATH, "%s\\Data\\Maps\\",  TerrainHelper::GetServerPath()); 
	NiFile::CreateDirectoryRecursive(acPath);

	//合成绝对路径
	char acFilename[NI_MAX_PATH];
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\Maps\\area_color.csv", TerrainHelper::GetServerPath());

	return CCSVFile::Save(acFilename);
}

void CAreaColorDB::SetColor(unsigned int uiAreaID, unsigned int uiColor)
{
	m_kColorMap.SetAt(uiAreaID, uiColor);
}

unsigned int CAreaColorDB::GetColor(unsigned int uiAreaID)
{
	unsigned int uiColor;
	if(!m_kColorMap.GetAt(uiAreaID, uiColor))
	{
		uiColor = D3DCOLOR_COLORVALUE(NiUnitRandom(), NiUnitRandom(), NiUnitRandom(), 1.f);
		SetColor(uiAreaID, uiColor);
	}

	return uiColor;
}

CAreaColorDB* CAreaColorDB::Get()
{
	return ms_pkThis;
}
