#pragma once

#include "TerrainLibType.h"
#include "CSVFile.h"
class TERRAIN_ENTRY CAreaColorDB : public CCSVFile
{
public:
	CAreaColorDB();
	virtual ~CAreaColorDB();

	bool ReadTopLine();
	void WriteTopLine();

	bool Load();
    bool Save();

	void SetColor(unsigned int uiAreaID, unsigned int uiColor);
	unsigned int GetColor(unsigned int uiAreaID);

	static CAreaColorDB* Get();

private:
	NiTMap<unsigned int, unsigned int> m_kColorMap;
	static CAreaColorDB* ms_pkThis;
};

#define g_pkAreaColorDB (CAreaColorDB::Get())
