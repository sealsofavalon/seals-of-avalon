#include "TerrainPCH.h"
#include "AreaDB.h"

CAreaDB* CAreaDB::ms_pkThis = NULL;

CAreaDB::CAreaDB()
{
	NIASSERT(ms_pkThis == NULL);
	ms_pkThis = this;
}

CAreaDB::~CAreaDB()
{
	ms_pkThis = NULL;
}

bool CAreaDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
		AreaInfo* pkAreaInfo = NiNew AreaInfo;
        pkAreaInfo->m_uiEntryID = GetUInt(ui, CAreaDB::EntryID);
        GetString(ui, CAreaDB::Name, pkAreaInfo->m_kName);

		m_kNameMap.SetAt(pkAreaInfo->m_kName, pkAreaInfo);
		m_kIDMap.SetAt(pkAreaInfo->m_uiEntryID, pkAreaInfo);
	}

	return true;
}

void CAreaDB::Unload()
{
	unsigned int uiEntryID;
	AreaInfo* pkAreaInfo;
    NiTMapIterator pos = m_kIDMap.GetFirstPos();
    while (pos)
    {
        m_kIDMap.GetNext(pos, uiEntryID, pkAreaInfo);
        NiDelete pkAreaInfo;
    }

	CDBFile::Unload();
}

bool CAreaDB::GetAreaInfo(unsigned int uiEntryID, AreaInfo*& pkAreaInfo) const
{
	return m_kIDMap.GetAt(uiEntryID, pkAreaInfo);
}

bool CAreaDB::GetAreaInfo(const NiFixedString& kName, AreaInfo*& pkAreaInfo) const
{
	return m_kNameMap.GetAt(kName, pkAreaInfo);
}

CAreaDB* CAreaDB::Get()
{ 
	return ms_pkThis;
}