#pragma once

#include "TerrainLibType.h"
#include "DBFile.h"

struct TERRAIN_ENTRY AreaInfo : public NiMemObject
{
    unsigned int m_uiEntryID;
    NiFixedString m_kName;
};

class TERRAIN_ENTRY CAreaDB : public CDBFile
{
private:
    NiTFixedStringMap<AreaInfo*> m_kNameMap;
	NiTPointerMap<unsigned int, AreaInfo*> m_kIDMap;
	
	static CAreaDB* ms_pkThis;

public:
    enum
	{
		EntryID = 0, //unsigned int
        Name = 7, //string
	};

	CAreaDB();
	virtual ~CAreaDB();

    virtual bool Load(const char* pcName);
	virtual void Unload();

    bool GetAreaInfo(unsigned int uiEntryID, AreaInfo*& pkAreaInfo) const;
	bool GetAreaInfo(const NiFixedString& kName, AreaInfo*& pkAreaInfo) const;

	static CAreaDB* Get();
};

#define g_pkAreaDB (CAreaDB::Get())