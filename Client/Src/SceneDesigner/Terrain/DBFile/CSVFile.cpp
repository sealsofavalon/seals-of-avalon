#include "TerrainPCH.h"
#include "CSVFile.h"

bool CCSVFile::Load(const char* pcFilename)
{
	NiFile* pkFile = NiFile::GetFile(pcFilename, NiFile::READ_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return false;
	}

    unsigned int uiSize = pkFile->GetFileSize();
	if( uiSize == 0 )
	{
		NiDelete pkFile;
		return false;
	}

	char* pcData = NiAlloc(char, uiSize + 1);
	pkFile->Read(pcData, uiSize);
	NiDelete pkFile;
	pcData[uiSize] = '\0';

    m_kString = pcData;

    NiFree(pcData);
    return true;
}

bool CCSVFile::Save(const char* pcFilename)
{
	NiFile* pkFile = NiFile::GetFile(pcFilename, NiFile::WRITE_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return false;
	}

	if( m_kString.Length() )
		pkFile->Write((const char*)m_kString, m_kString.Length());

    NiDelete pkFile;
    return true;
}

bool CCSVFile::ReadToken(char* pcToken, bool bLastField)
{
    int i = 0;
	for( ; (pcToken[i] = m_kString.GetAt(i)); ++i )
    {
        if( bLastField )
        {
            if( pcToken[i] == '\r' || pcToken[i] == '\n' )
            {
                pcToken[i] = '\0';
                i += 2;
                break;
            }
        }
        else
        {
            if( pcToken[i] == ',' )
            {
                pcToken[i] = '\0';
                ++i;
                break;
            }
        }
    }

    if(i == 0)
        return false;

	m_kString = m_kString.Right(m_kString.Length() - i);
    return true;
}

bool CCSVFile::CheckLastField()
{
    for(int i = 0; m_kString.GetAt(i); ++i )
    {
        if( m_kString.GetAt(i) == '\r' || m_kString.GetAt(i) == '\n' )
            return true;

       if( m_kString.GetAt(i) == ',' )
           return false;
    }

    return true;
}

bool CCSVFile::ReadInt(int& iValue, bool bLastField)
{
    char acToken[1024];
    if( ReadToken( acToken, bLastField ) )
    {
        iValue = atoi(acToken);
        return true;
    }

    return false;
}

bool CCSVFile::ReadUInt(unsigned int& uiValue, bool bLastField)
{
    char acToken[1024];
    if( ReadToken( acToken, bLastField ) )
    {
        __int64 i64Value = _atoi64(acToken);
        uiValue = (unsigned int)i64Value;
        return true;
    }

    return false;
}

bool CCSVFile::ReadFloat(float& fValue, bool bLastField)
{
    char acToken[1024];
    if( ReadToken( acToken, bLastField ) )
    {
        fValue = (float)atof(acToken);
        return true;
    }

    return false;
}

bool CCSVFile::ReadString(NiFixedString& kString, bool bLastField)
{
    char acToken[1024];
    if( ReadToken(acToken, bLastField) )
    {
        kString = acToken;
        return true;
    }

    return false;
}

bool CCSVFile::WriteToken(const char* pcToken, bool bLastField)
{
    m_kString += pcToken;
    if( bLastField )
    {
        m_kString += '\r';
        m_kString += '\n';
    }
    else
    {
        m_kString += ',';
    }

    return true;
}

bool CCSVFile::WriteInt(int iValue, bool bLastField)
{
    char acToken[64];
    NiSprintf(acToken, sizeof(acToken), "%d", iValue);

    return WriteToken(acToken, bLastField);
}

bool CCSVFile::WriteUInt(unsigned int uiValue, bool bLastField)
{
    char acToken[64];
    NiSprintf(acToken, sizeof(acToken), "%u", uiValue);

    return WriteToken(acToken, bLastField);
}

bool CCSVFile::WriteFloat(float fValue, bool bLastField)
{
    char acToken[64];
    NiSprintf(acToken, sizeof(acToken), "%f", fValue);

    return WriteToken(acToken, bLastField);
}

bool CCSVFile::WriteString(const NiFixedString& kStr, bool bLastField)
{
    return WriteToken(kStr, bLastField);
}