#pragma once
#include "TerrainLibType.h"

class TERRAIN_ENTRY CCSVFile : public NiMemObject
{
private:
    NiString m_kString;

public:
    CCSVFile() {}
    virtual ~CCSVFile() {}

    virtual bool Load(const char* pcFilename);
    virtual bool Save(const char* pcFilename);

    void Clear() { m_kString.Empty(); }

    unsigned int GetLength() const { return m_kString.Length(); }
    bool CheckLastField();

    bool ReadToken(char* pcToken, bool bLastField = false);
    bool ReadInt(int& iValue, bool bLastField = false);
    bool ReadUInt(unsigned int& uiValue, bool bLastField = false);
    bool ReadFloat(float& fValue, bool bLastField = false);
    bool ReadString(NiFixedString& kString, bool bLastField = false);

    bool WriteToken(const char* pcToken, bool bLastField = false);
    bool WriteInt(int iValue, bool bLastField = false);
    bool WriteUInt(unsigned int uiValue, bool bLastField = false);
    bool WriteFloat(float fValue, bool bLastField = false);
    bool WriteString(const NiFixedString& kStr, bool bLastField = false);
};
