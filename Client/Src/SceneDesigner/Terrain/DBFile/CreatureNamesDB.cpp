#include "TerrainPCH.h"
#include "CreatureNamesDB.h"

CCreatureNamesDB* CCreatureNamesDB::ms_pkThis = NULL;

CCreatureNamesDB::CCreatureNamesDB()
{
	NIASSERT(ms_pkThis == NULL);
	ms_pkThis = this;
}

CCreatureNamesDB::~CCreatureNamesDB()
{
	ms_pkThis = NULL;
}

bool CCreatureNamesDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
		NameInfo* pkNameInfo = NiNew NameInfo;
        pkNameInfo->m_uiEntryID = GetUInt(ui, CCreatureNamesDB::EntryID);
        pkNameInfo->m_uiDisplayID = GetUInt(ui, CCreatureNamesDB::DisplayID); 
        GetString(ui, CCreatureNamesDB::Name, pkNameInfo->m_kName);

		m_kNameMap.SetAt(pkNameInfo->m_kName, pkNameInfo);
		m_kIDMap.SetAt(pkNameInfo->m_uiEntryID, pkNameInfo);
	}

	return true;
}

void CCreatureNamesDB::Unload()
{
	unsigned int uiEntryID;
	NameInfo* pkNameInfo;
    NiTMapIterator pos = m_kIDMap.GetFirstPos();
    while (pos)
    {
        m_kIDMap.GetNext(pos, uiEntryID, pkNameInfo);
        NiDelete pkNameInfo;
    }

	CDBFile::Unload();
}

bool CCreatureNamesDB::GetNameInfo(unsigned int uiEntryID, NameInfo*& pkNameInfo) const
{
	return m_kIDMap.GetAt(uiEntryID, pkNameInfo);
}

bool CCreatureNamesDB::GetNameInfo(const NiFixedString& kName, NameInfo*& pkNameInfo) const
{
	return m_kNameMap.GetAt(kName, pkNameInfo);
}

CCreatureNamesDB* CCreatureNamesDB::Get() 
{ 
	return ms_pkThis;
}