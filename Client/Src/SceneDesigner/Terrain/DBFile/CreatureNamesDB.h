#pragma once

#include "TerrainLibType.h"
#include "DBFile.h"

struct TERRAIN_ENTRY NameInfo : public NiMemObject
{
    unsigned int m_uiEntryID;
    unsigned int m_uiDisplayID;
    NiFixedString m_kName;
};

class TERRAIN_ENTRY CCreatureNamesDB : public CDBFile
{
private:
    NiTFixedStringMap<NameInfo*> m_kNameMap;
	NiTPointerMap<unsigned int, NameInfo*> m_kIDMap;
	
	static CCreatureNamesDB* ms_pkThis;

public:
    enum
	{
		EntryID = 0, //unsigned int
        Name = 1, //string
        DisplayID = 7, //unsigned int
	};

	CCreatureNamesDB();
	virtual ~CCreatureNamesDB();

    virtual bool Load(const char* pcName);
	virtual void Unload();

    bool GetNameInfo(unsigned int uiEntryID, NameInfo*& pkNameInfo) const;
	bool GetNameInfo(const NiFixedString& kName, NameInfo*& pkNameInfo) const;

	static CCreatureNamesDB* Get();
};

#define g_pkCreatureNamesDB (CCreatureNamesDB::Get())