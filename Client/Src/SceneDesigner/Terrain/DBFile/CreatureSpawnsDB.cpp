#include "TerrainPCH.h"
#include "CreatureSpawnsDB.h"
#include "CreatureNamesDB.h"
#include "DisplayInfoDB.h"
#include "TerrainHelper.h"
#include "NPCComponent.h"
#include "Map.h"

bool CCreatureSpawnsDB::ReadTopLine()
{
	char acTempToken[1024];

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken, true) )
		return false;

	return true;
}



void CCreatureSpawnsDB::WriteTopLine()
{
	WriteToken("id");
	WriteToken("entry");
	WriteToken("map");
	WriteToken("position_x");
	WriteToken("position_y");
	WriteToken("position_z");
	WriteToken("orientation");
	WriteToken("movetype");
	WriteToken("displayid");
	WriteToken("faction");
	WriteToken("flags");
	WriteToken("bytes");
	WriteToken("bytes2");
	WriteToken("emote_state");
	WriteToken("npc_respawn_link");
	WriteToken("channel_spell");
	WriteToken("channel_target_sqlid");
	WriteToken("channel_target_sqlid_creat");
	WriteToken("standstate", true);
}

bool CCreatureSpawnsDB::Load(CTile* pkTile)
{
	//合成绝对路径
	const NiFixedString& kMapName = CMap::Get()->GetMapName();
	char acFilename[NI_MAX_PATH];
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\Maps\\%s\\NPC\\npc_%d_%d.csv", TerrainHelper::GetServerPath(), (const char*)kMapName, pkTile->GetX(), pkTile->GetY());

    if( !CCSVFile::Load(acFilename) )
        return false;

	if( !ReadTopLine() )
		return false;

	unsigned int uiId;
	unsigned int uiEntryID;
	NiPoint3 kPosition;
	float fRot;
	NiMatrix3 kRotMat;
    unsigned int uiMoveType;

	NameInfo* pkNameInfo;
	NiFixedString* pkModelName;
	NiGeneralEntity* pkEntity;
	NiTransformationComponent* pkTransformation;
	char acModelName[1024];
	char acTempToken[1024];
	CNpcComponent* pkNpcComponent;
    unsigned int uiFlag;

	NiTPrimitiveArray<NiEntityInterface*>& kEntities = pkTile->GetNpcEntities();
    while( true )
    {
		if( !ReadUInt(uiId) ) break; //id

        if( !ReadUInt(uiEntryID) ) break; //entry id 

		if( !ReadToken(acTempToken) ) break; //map

        if( !ReadFloat(kPosition.x) ) break; //x
        if( !ReadFloat(kPosition.y) ) break; //y
        if( !ReadFloat(kPosition.z) ) break; //z
        if( !ReadFloat(fRot) ) break; //orientation
        if( !ReadUInt(uiMoveType) ) break; //movetype

		if( !ReadToken(acTempToken) ) break; //displayid
		if( !ReadToken(acTempToken) ) break; //faction

        if( !ReadUInt(uiFlag) ) break;
		
		if( !ReadToken(acTempToken) ) break; //bytes
		if( !ReadToken(acTempToken) ) break; //bytes2
		if( !ReadToken(acTempToken) ) break; //emote_state
		if( !ReadToken(acTempToken) ) break; //npc_respawn_link
		if( !ReadToken(acTempToken) ) break; //channel_spell
		if( !ReadToken(acTempToken) ) break; //channel_target_sqlid
		if( !ReadToken(acTempToken) ) break; //channel_target_sqlid_creature
		if( !ReadToken(acTempToken, true) ) break; //standstate

		if( uiEntryID == 0 )
			continue;

		if( !g_pkCreatureNamesDB->GetNameInfo(uiEntryID, pkNameInfo) )
			continue;

		if( !g_pkDisplayInfoDB->GetModelName(pkNameInfo->m_uiDisplayID, pkModelName) )
			continue;

		//使位置为相对位置
		//kPosition.x = NiFmod(kPosition.x, TILE_SIZE) + pkTile->GetX()*TILE_SIZE;
		//kPosition.y = NiFmod(kPosition.y, TILE_SIZE) + pkTile->GetY()*TILE_SIZE;

		pkEntity = NiNew NiGeneralEntity;
		pkEntity->SetName(pkNameInfo->m_kName);
        pkEntity->SetTypeMask(NiGeneralEntity::Npc);

		kRotMat.FromEulerAnglesXYZ(0.f, 0.f, fRot);
		pkTransformation = NiNew NiTransformationComponent();
		pkTransformation->SetTranslation(kPosition);
		pkTransformation->SetRotation(kRotMat);
		pkEntity->AddComponent(pkTransformation, false);

		// Add Actor component.
		NiSprintf(acModelName, sizeof(acModelName), "%s\\%s", TerrainHelper::GetClientPath(), (const char*)*pkModelName);
		NiPath::Standardize(acModelName);
		NiActorComponent* pkActorComponent = NiNew NiActorComponent();
		pkActorComponent->SetKfmFilePath(acModelName);
		pkEntity->AddComponent(pkActorComponent, false);

		// Add Npc Component
		pkNpcComponent = NiNew CNpcComponent(pkNameInfo->m_kName, pkNameInfo->m_uiEntryID);
		pkNpcComponent->SetId(uiId);
        pkNpcComponent->SetMoveType(uiMoveType);
        pkNpcComponent->SetFlag(uiFlag);
		pkEntity->AddComponent(pkNpcComponent, false);

		pkEntity->AddReference();
		kEntities.Add(pkEntity);
    }

    return true;
}

bool CCreatureSpawnsDB::Save(CTile* pkTile)
{
    Clear();

	WriteTopLine();

    NiFixedString kTranslationName = "Translation";
    NiFixedString kRotationName = "Rotation";
	NiFixedString kEntryIDName = "Entry ID";

	NiPoint3 kPosition;
	NiMatrix3 kRotMat;
	float fRotX, fRotY, fRotZ;
	unsigned int uiEntryID;
    bool bMoveType;
	const NiEntityInterface* pkEntity;
    const CNpcComponent* pkNpcComponent;

	NiTPrimitiveArray<NiEntityInterface*>& kEntities = pkTile->GetNpcEntities();
	unsigned int uiTileId = pkTile->GetX()*100 + pkTile->GetY();
	unsigned int uiMapId = 0;
	MapInfo* pkMapInfo;
	const NiFixedString& kMapName = CMap::Get()->GetMapName();
	if(g_pkWorldMapDB->GetMapInfo(kMapName, pkMapInfo))
	{
		uiMapId = pkMapInfo->m_uiID;
	}

	for( unsigned int ui = 0; ui < kEntities.GetSize(); ++ui )
	{
		pkEntity = kEntities.GetAt(ui);

		if( !pkEntity->GetPropertyData(kTranslationName, kPosition, 0) )
			continue;

		if( !pkEntity->GetPropertyData(kRotationName, kRotMat, 0) )
			continue;
		kRotMat.ToEulerAnglesXYZ(fRotX, fRotY, fRotZ);

		if( !pkEntity->GetPropertyData(kEntryIDName, uiEntryID, 0) )
			continue;

        pkNpcComponent = (CNpcComponent*)pkEntity->GetComponentByTemplateID(CNpcComponent::ms_kTemplateID);
        if(pkNpcComponent == NULL)
            continue;

		NIASSERT(uiMapId <= 428); //地图id最大是428

        //xxx       yyyy     zzz
        //地图ID   TileID   顺序ID

		WriteUInt((uiMapId*10000 + uiTileId) * 1000 + ui); //id

		WriteUInt(uiEntryID); //EntryIDk

		WriteUInt(uiMapId);//map id

		WriteFloat(kPosition.x);
		WriteFloat(kPosition.y);
		WriteFloat(kPosition.z);
		WriteFloat(fRotZ);
        
        WriteUInt(pkNpcComponent->GetMoveType()); //movetype

		WriteToken("0"); //displayid
		WriteToken("1"); //faction
        WriteUInt(pkNpcComponent->GetFlag()); //flags
		WriteToken("0"); //bytes
		WriteToken("0"); //bytes2
		WriteToken("0"); //emote_state
		WriteToken("0"); //npc_respawn_link
		WriteToken("0"); //channel_spell
		WriteToken("0"); //channel_target_sqlid
		WriteToken("0"); //channel_target_sqlid_creature
		WriteToken("0", true); //standstate
	}
    
	//创建目录
	char acPath[NI_MAX_PATH];
	NiSprintf(acPath, NI_MAX_PATH, "%s\\Data\\Maps\\%s\\NPC",  TerrainHelper::GetServerPath(), (const char*)kMapName); 
	NiFile::CreateDirectoryRecursive(acPath);

	//合成绝对路径
	char acFilename[NI_MAX_PATH];
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\Maps\\%s\\NPC\\npc_%d_%d.csv", TerrainHelper::GetServerPath(), (const char*)kMapName, pkTile->GetX(), pkTile->GetY());
    return CCSVFile::Save(acFilename);
}
