#include "TerrainPCH.h"
#include "DBManager.h"
#include "TerrainHelper.h"

bool CDBManager::LoadDB()
{
	char acFilename[NI_MAX_PATH];
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\DBFiles\\creature_names.db",  TerrainHelper::GetClientPath());
	m_kCreatureNamsDB.Load(acFilename);

	NiSprintf(acFilename, NI_MAX_PATH, "%s\\DBFiles\\displayinfo.db",  TerrainHelper::GetClientPath());
	m_kDisplayInfoDB.Load(acFilename);

	NiSprintf(acFilename, NI_MAX_PATH, "%s\\DBFiles\\worldmap_info.db",  TerrainHelper::GetClientPath());
	m_kWorldMapDB.Load(acFilename);

    NiSprintf(acFilename, NI_MAX_PATH, "%s\\DBFiles\\gameobject_names.DB",  TerrainHelper::GetClientPath());
    m_kGameObjectNamesDB.Load(acFilename);

	NiSprintf(acFilename, NI_MAX_PATH, "%s\\DBFiles\\area.db",  TerrainHelper::GetClientPath());
	m_kAreaDB.Load(acFilename);

	m_kAreaColorDB.Load();

	return true;
}

void CDBManager::UnloadDB()
{
	m_kAreaColorDB.Save();

	m_kAreaDB.Unload();
	m_kDisplayInfoDB.Unload();
    m_kGameObjectNamesDB.Unload();
	m_kCreatureNamsDB.Unload();
	m_kWorldMapDB.Unload();
}