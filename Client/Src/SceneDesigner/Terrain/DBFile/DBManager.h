#pragma once

#include "CreatureNamesDB.h"
#include "DisplayInfoDB.h"
#include "WorldMapDB.h"
#include "GameObjectNamesDB.h"
#include "AreaDB.h"
#include "AreaColorDB.h"


class CDBManager : public NiMemObject
{
private:
	CCreatureNamesDB m_kCreatureNamsDB;
	CDisplayInfoDB m_kDisplayInfoDB;
	CWorldMapDB m_kWorldMapDB;
    CGameObjectNamesDB m_kGameObjectNamesDB;
	CAreaDB m_kAreaDB;
	CAreaColorDB m_kAreaColorDB;

public:
	bool LoadDB();
	void UnloadDB();
};