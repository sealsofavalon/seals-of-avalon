#include "TerrainPCH.h"
#include "DisplayInfoDB.h"

CDisplayInfoDB* CDisplayInfoDB::ms_pkThis = NULL;

CDisplayInfoDB::CDisplayInfoDB()
{
	NIASSERT(ms_pkThis == NULL);
	ms_pkThis = this;
}

CDisplayInfoDB::~CDisplayInfoDB()
{
	ms_pkThis = NULL;
}

bool CDisplayInfoDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

    unsigned int uiDisplayID;
	NiFixedString* pkModelName;
    NiFixedString* pkDupName;
    char filename[1024];
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
		pkModelName = NiNew NiFixedString;
		uiDisplayID = GetUInt(ui, CDisplayInfoDB::DisplayID); 
		GetString(ui, CDisplayInfoDB::ModelName, *pkModelName);

        NiStrcpy(filename, sizeof(filename), *pkModelName);
        NiPath::Standardize(filename);
        *pkModelName = filename;

        if(m_kModelNameMap.GetAt(uiDisplayID, pkDupName))
        {
            char acMsg[512];
            NiSprintf(acMsg, sizeof(acMsg), "%d %s dup\n", uiDisplayID, (const char*)(*pkDupName));
            OutputDebugStringA(acMsg);
            NiDelete pkDupName;
        }

		m_kModelNameMap.SetAt(uiDisplayID, pkModelName); 
	}

	return true;
}

void CDisplayInfoDB::Unload()
{
	unsigned int uiDisplayID;
	NiFixedString* pkModelName;
    NiTMapIterator pos = m_kModelNameMap.GetFirstPos();
    while (pos)
    {
        m_kModelNameMap.GetNext(pos, uiDisplayID, pkModelName);
        NiDelete pkModelName;
    }

	CDBFile::Unload();
}

bool CDisplayInfoDB::GetModelName(unsigned int uiDisplayID, NiFixedString*& pkModelName) const
{
	return m_kModelNameMap.GetAt(uiDisplayID, pkModelName);
}

CDisplayInfoDB* CDisplayInfoDB::Get() 
{
	return ms_pkThis; 
}