#pragma once

#include "TerrainLibType.h"
#include "DBFile.h"
class TERRAIN_ENTRY CDisplayInfoDB : public CDBFile
{
private:
     NiTPointerMap<unsigned int, NiFixedString*> m_kModelNameMap;
	
	 static CDisplayInfoDB* ms_pkThis;

public:
    enum
	{
		DisplayID = 0, //unsigned int
        ModelName = 1, //string
	};

	CDisplayInfoDB();
	virtual ~CDisplayInfoDB();

    virtual bool Load(const char* pcName);
	virtual void Unload();
    bool GetModelName(unsigned int uiDisplayID, NiFixedString*& pkModelName) const;

	static CDisplayInfoDB* Get();
};

#define g_pkDisplayInfoDB (CDisplayInfoDB::Get())