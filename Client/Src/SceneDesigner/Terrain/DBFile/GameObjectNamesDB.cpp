#include "TerrainPCH.h"
#include "GameObjectNamesDB.h"

CGameObjectNamesDB* CGameObjectNamesDB::ms_pkThis = NULL;

CGameObjectNamesDB::CGameObjectNamesDB()
{
	NIASSERT(ms_pkThis == NULL);
	ms_pkThis = this;
}

CGameObjectNamesDB::~CGameObjectNamesDB()
{
	ms_pkThis = NULL;
}

bool CGameObjectNamesDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	GameObjectInfo* pkGameObjectInfo;
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
		pkGameObjectInfo = NiNew GameObjectInfo;
        pkGameObjectInfo->m_uiEntryID = GetUInt(ui, CGameObjectNamesDB::EntryID); 
        pkGameObjectInfo->m_uiDisplayID = GetUInt(ui, CGameObjectNamesDB::DisplayID);
        GetString(ui, CGameObjectNamesDB::Name, pkGameObjectInfo->m_kName);

        m_kIDMap.SetAt(pkGameObjectInfo->m_uiEntryID, pkGameObjectInfo);
        m_kNameMap.SetAt(pkGameObjectInfo->m_kName, pkGameObjectInfo);
	}

	return true;
}

void CGameObjectNamesDB::Unload()
{
	unsigned int uiEntryID;
	GameObjectInfo* pkGameObjectInfo;
    NiTMapIterator pos = m_kIDMap.GetFirstPos();
    while (pos)
    {
        m_kIDMap.GetNext(pos, uiEntryID, pkGameObjectInfo);
        NiDelete pkGameObjectInfo;
    }

	CDBFile::Unload();
}

GameObjectInfo* CGameObjectNamesDB::GetGameObjectInfo(unsigned int uiID)
{
    GameObjectInfo* pkGameObjectInfo;
    if(m_kIDMap.GetAt(uiID, pkGameObjectInfo))
        return pkGameObjectInfo;

    return NULL;
}

GameObjectInfo* CGameObjectNamesDB::GetGameObjectInfo(const NiFixedString& kName)
{
    GameObjectInfo* pkGameObjectInfo;
    if(m_kNameMap.GetAt(kName, pkGameObjectInfo))
        return pkGameObjectInfo;

    return NULL;
}

CGameObjectNamesDB* CGameObjectNamesDB::Get() 
{ 
	return ms_pkThis; 
}