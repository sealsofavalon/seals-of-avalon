#pragma once

#include "TerrainLibType.h"
#include "DBFile.h"

struct TERRAIN_ENTRY GameObjectInfo : public NiMemObject
{
    unsigned int m_uiEntryID;
    unsigned int m_uiDisplayID;
    NiFixedString m_kName;
};

class TERRAIN_ENTRY CGameObjectNamesDB : public CDBFile
{
private:
     NiTPointerMap<unsigned int, GameObjectInfo*> m_kIDMap;
     NiTFixedStringMap<GameObjectInfo*> m_kNameMap;
	
	 static CGameObjectNamesDB* ms_pkThis;

public:
    enum
	{
		EntryID = 0, //unsigned int
        DisplayID = 2,
        Name = 3, //string
	};

	CGameObjectNamesDB();
	virtual ~CGameObjectNamesDB();

    virtual bool Load(const char* pcName);
	virtual void Unload();

    GameObjectInfo* GetGameObjectInfo(unsigned int uiID);
    GameObjectInfo* GetGameObjectInfo(const NiFixedString& kName);

	static CGameObjectNamesDB* Get();
};

#define g_pkGameObjectNamesDB (CGameObjectNamesDB::Get())