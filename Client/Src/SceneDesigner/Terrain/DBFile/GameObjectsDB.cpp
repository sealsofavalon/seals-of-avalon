#include "TerrainPCH.h"
#include "GameObjectsDB.h"
#include "GameObjectComponent.h"
#include "Map.h"
#include "TerrainHelper.h"

bool CGameObjectsDB::ReadTopLine()
{
	char acTempToken[1024];

	if( !ReadToken(acTempToken) ) //id
		return false;

	if( !ReadToken(acTempToken) ) //Entry
		return false;

	if( !ReadToken(acTempToken) ) //map
		return false;

	if( !ReadToken(acTempToken) ) //position_x
		return false;

	if( !ReadToken(acTempToken) ) //position_y
		return false;

	if( !ReadToken(acTempToken) ) //position_z
		return false;

	if( !ReadToken(acTempToken) ) //Facing
		return false;

	if( !ReadToken(acTempToken) ) //orientation1
		return false;

	if( !ReadToken(acTempToken) ) //orientation2
		return false;

	if( !ReadToken(acTempToken) ) //orientation3
		return false;

	if( !ReadToken(acTempToken) ) //orientation4
		return false;

	if( !ReadToken(acTempToken) ) //State
		return false;

	if( !ReadToken(acTempToken) ) //Flags
		return false;

    if( !ReadToken(acTempToken) ) //Faction
        return false;

    if( !ReadToken(acTempToken) ) //Scale
        return false;

    if( !ReadToken(acTempToken, true) ) //stateNpcLink
        return false;

	return true;
}

void CGameObjectsDB::WriteTopLine()
{
	WriteToken("id");
	WriteToken("entry");
	WriteToken("map");
	WriteToken("position_x");
	WriteToken("position_y");
	WriteToken("position_z");
	WriteToken("facing");
	WriteToken("orientation1");
	WriteToken("orientation2");
	WriteToken("orientation3");
	WriteToken("orientation4");
	WriteToken("state");
	WriteToken("flags");
    WriteToken("faction");
    WriteToken("scale");
    WriteToken("stateNpcLink", true);
}

bool CGameObjectsDB::Load(CTile* pkTile)
{
	//合成绝对路径
	const NiFixedString& kMapName = CMap::Get()->GetMapName();
	char acFilename[NI_MAX_PATH];
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\Maps\\%s\\GameObjects\\GameObjects_%d_%d.csv", TerrainHelper::GetServerPath(), (const char*)kMapName, pkTile->GetX(), pkTile->GetY());

	if( !CCSVFile::Load(acFilename) )
		return false;

	if( !ReadTopLine() )
		return false;

    unsigned int uiId;
    unsigned int uiEntryID;
    NiPoint3 kPosition;
    float fRot;
    NiMatrix3 kRotMat;

    GameObjectInfo* pkGameObjectInfo;
    NiFixedString* pkModelName;
    NiGeneralEntity* pkEntity;
    NiTransformationComponent* pkTransformation;
    char acModelName[1024];
    char acTempToken[1024];
    CGameObjectComponent* pkGameObjectComponent;

    NiTPrimitiveArray<NiEntityInterface*>& kEntities = pkTile->GetGameObjectEntities();

    while( true )
    {
		if( !ReadUInt(uiId) ) break; //id

        if( !ReadUInt(uiEntryID) ) break; //entry id 

		if( !ReadToken(acTempToken) ) break; //map

        if( !ReadFloat(kPosition.x) ) break; //x
        if( !ReadFloat(kPosition.y) ) break; //y
        if( !ReadFloat(kPosition.z) ) break; //z
        if( !ReadFloat(fRot) ) break; //facing

		if( !ReadToken(acTempToken) ) break; //orientation1
		if( !ReadToken(acTempToken) ) break; //orientation2
		if( !ReadToken(acTempToken) ) break; //orientation3
		if( !ReadToken(acTempToken) ) break; //orientation4
		if( !ReadToken(acTempToken) ) break; //state
		if( !ReadToken(acTempToken) ) break; //flags
		if( !ReadToken(acTempToken) ) break; //faction
		if( !ReadToken(acTempToken) ) break; //scale
		if( !ReadToken(acTempToken, true) ) break; //stateNpcLink

		if( uiEntryID == 0 )
			continue;

        pkGameObjectInfo = g_pkGameObjectNamesDB->GetGameObjectInfo(uiEntryID);
		if( !pkGameObjectInfo )
			continue;

		if( !g_pkDisplayInfoDB->GetModelName(pkGameObjectInfo->m_uiDisplayID, pkModelName) )
			continue;

		//使位置为相对位置
		kPosition.x = NiFmod(kPosition.x, TILE_SIZE) + pkTile->GetX()*TILE_SIZE;
		kPosition.y = NiFmod(kPosition.y, TILE_SIZE) + pkTile->GetY()*TILE_SIZE;

		pkEntity = NiNew NiGeneralEntity;
		pkEntity->SetName(pkGameObjectInfo->m_kName);

		kRotMat.FromEulerAnglesXYZ(0.f, 0.f, fRot);
		pkTransformation = NiNew NiTransformationComponent();
		pkTransformation->SetTranslation(kPosition);
		pkTransformation->SetRotation(kRotMat);
		pkEntity->AddComponent(pkTransformation, false);

		// Add Nif component.
		NiSprintf(acModelName, sizeof(acModelName), "%s\\%s", TerrainHelper::GetClientPath(), (const char*)*pkModelName);
		NiPath::Standardize(acModelName);
		NiSceneGraphComponent* pkSceneGraphComponent = NiNew NiSceneGraphComponent();
		pkSceneGraphComponent->SetNifFilePath(acModelName);
		pkEntity->AddComponent(pkSceneGraphComponent, false);

		// Add GameObject Component
		pkGameObjectComponent = NiNew CGameObjectComponent(pkGameObjectInfo->m_kName, pkGameObjectInfo->m_uiEntryID);
		pkEntity->AddComponent(pkGameObjectComponent, false);

		pkEntity->AddReference();
		kEntities.Add(pkEntity);
    }

    return true;
}

bool CGameObjectsDB::Save(CTile* pkTile)
{
	Clear();

	WriteTopLine();

    NiFixedString kTranslationName = "Translation";
    NiFixedString kRotationName = "Rotation";
    
    NiPoint3 kPosition;
	NiMatrix3 kRotMat;
	float fRotX, fRotY, fRotZ;
	unsigned int uiEntryID;
	const NiEntityInterface* pkEntity;

	NiTPrimitiveArray<NiEntityInterface*>& kEntities = pkTile->GetGameObjectEntities();
	unsigned int uiTileId = pkTile->GetX()*100 + pkTile->GetY();
	unsigned int uiMapId = 0;
	MapInfo* pkMapInfo;
	const NiFixedString& kMapName = CMap::Get()->GetMapName();
	if(g_pkWorldMapDB->GetMapInfo(kMapName, pkMapInfo))
	{
		uiMapId = pkMapInfo->m_uiID;
	}

	for( unsigned int ui = 0; ui < kEntities.GetSize(); ++ui )
	{
		pkEntity = kEntities.GetAt(ui);

		if( !pkEntity->GetPropertyData(kTranslationName, kPosition, 0) )
			continue;

		if( !pkEntity->GetPropertyData(kRotationName, kRotMat, 0) )
			continue;
		kRotMat.ToEulerAnglesXYZ(fRotX, fRotY, fRotZ);

        if( !pkEntity->GetPropertyData(CGameObjectComponent::ms_kEntryIDName, uiEntryID, 0) )
			continue;

		NIASSERT(uiMapId <= 428); //地图id最大是428

		WriteUInt((uiMapId*10000 + uiTileId) * 1000 + ui); //id

		WriteUInt(uiEntryID); //EntryID

		WriteUInt(uiMapId);//map id

		WriteFloat(kPosition.x);
		WriteFloat(kPosition.y);
		WriteFloat(kPosition.z);
		WriteFloat(fRotZ);

		WriteToken("0"); //orientation1
		WriteToken("0"); //orientation2
		WriteToken("0"); //orientation3
		WriteToken("0"); //orientation4
		WriteToken("0"); //state
		WriteToken("0"); //flags
		WriteToken("0"); //faction
		WriteToken("1"); //scale
		WriteToken("0", true); //stateNpcLink
	}

	//创建目录
	char acPath[NI_MAX_PATH];
	NiSprintf(acPath, NI_MAX_PATH, "%s\\Data\\Maps\\%s\\GameObjects",  TerrainHelper::GetServerPath(), (const char*)kMapName); 
	NiFile::CreateDirectoryRecursive(acPath);

	//合成绝对路径
	char acFilename[NI_MAX_PATH];
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\Maps\\%s\\GameObjects\\GameObjects_%d_%d.csv", TerrainHelper::GetServerPath(), (const char*)kMapName, pkTile->GetX(), pkTile->GetY());

	return CCSVFile::Save(acFilename);
}