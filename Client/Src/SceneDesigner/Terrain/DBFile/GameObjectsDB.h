#include "CSVFile.h"

class CTile;
class CGameObjectsDB : public CCSVFile
{
public:
	bool ReadTopLine();
	void WriteTopLine();

	bool Load(CTile* pkTile);
    bool Save(CTile* pkTile);
};
