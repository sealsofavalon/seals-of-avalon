#include "TerrainPCH.h"
#include "WayPointsDB.h"
#include "NpcComponent.h"
#include "WayPointComponent.h"
#include "Map.h"
#include "TerrainHelper.h"

bool CWayPointsDB::ReadTopLine()
{
	char acTempToken[1024];

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken) )
		return false;

	if( !ReadToken(acTempToken, true) )
		return false;

	return true;
}

void CWayPointsDB::WriteTopLine()
{
	WriteToken("spawnid");
	WriteToken("waypointid");
	WriteToken("position_x");
	WriteToken("position_y");
	WriteToken("position_z");
	WriteToken("waittime");
	WriteToken("flags");
	WriteToken("forwardemoteoneshot");
	WriteToken("forwardemoteid");
	WriteToken("backwardemoteoneshot");
	WriteToken("backwardemoteid");
	WriteToken("forwardskinid");
	WriteToken("backwardskinid", true);
}

bool CWayPointsDB::Load(CTile* pkTile)
{
	//合成绝对路径
	const NiFixedString& kMapName = CMap::Get()->GetMapName();
	char acFilename[NI_MAX_PATH];
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\Maps\\%s\\NPC\\waypoints_%d_%d.csv", TerrainHelper::GetServerPath(), (const char*)kMapName, pkTile->GetX(), pkTile->GetY());

	if( !CCSVFile::Load(acFilename) )
		return false;

	if( !ReadTopLine() )
		return false;

	NiTPrimitiveArray<NiEntityInterface*>& kEntities = pkTile->GetNpcEntities();


	NiGeneralEntity* pkEntity = NULL;
	NiEntityInterface* pkNpcEntity = NULL;
	CNpcComponent* pkNpcComponent = NULL;
	CWayPointComponent* pkWayPointComponent;
	NiTransformationComponent* pkTransformationComponent;

	unsigned int uiSpawnId;
	unsigned int uiWayPointId;
	NiPoint3 kPosition;
	float fWaitTime;
	char acName[256];
	char acTempToken[1024];
	while(true)
	{
		if( !ReadUInt(uiSpawnId) ) break; //spawn id
		if( !ReadUInt(uiWayPointId) ) break; //way point id
		if( !ReadFloat(kPosition.x) ) break; //x
		if( !ReadFloat(kPosition.y) ) break; //y
		if( !ReadFloat(kPosition.z) ) break; //z
		if( !ReadFloat(fWaitTime) ) break; //wait time

		if( !ReadToken(acTempToken) ) break; //flags
		if( !ReadToken(acTempToken) ) break; //forwardemoteoneshot
		if( !ReadToken(acTempToken) ) break; //forwardemoteid
		if( !ReadToken(acTempToken) ) break; //backwardemoteoneshot
		if( !ReadToken(acTempToken) ) break; //backwardemoteid
		if( !ReadToken(acTempToken) ) break; //forwardskinid
		if( !ReadToken(acTempToken, true) ) break; //backwardskinid

		if(uiWayPointId == 0)
		{
			//第一个点是NPC的位置, 编辑器忽略这一点
			continue;
		}

		uiWayPointId--;

		if(pkNpcComponent == NULL || pkNpcComponent->GetId() != uiSpawnId)
		{
			for(unsigned int ui = 0; ui < kEntities.GetSize(); ++ui)
			{
				pkNpcEntity = kEntities.GetAt(ui);
				pkNpcComponent = (CNpcComponent*)pkNpcEntity->GetComponentByTemplateID(CNpcComponent::ms_kTemplateID);
				if(pkNpcComponent->GetId() == uiSpawnId)
					break;
			}
		}

		if(pkNpcComponent == NULL || pkNpcComponent->GetId() != uiSpawnId)
			continue;

		//使位置为相对位置
		//kPosition.x = NiFmod(kPosition.x, TILE_SIZE) + pkTile->GetX()*TILE_SIZE;
		//kPosition.y = NiFmod(kPosition.y, TILE_SIZE) + pkTile->GetY()*TILE_SIZE;

		NiSprintf(acName, sizeof(acName), "WayPoint_%d_%d", uiSpawnId, uiWayPointId);
		pkEntity = NiNew NiGeneralEntity;
		pkEntity->SetName(acName);
        pkEntity->SetTypeMask(NiGeneralEntity::WayPoint);

		pkTransformationComponent = NiNew NiTransformationComponent();
		pkTransformationComponent->SetTranslation(kPosition);
		pkEntity->AddComponent(pkTransformationComponent, false);

		pkWayPointComponent = NiNew CWayPointComponent();
		pkWayPointComponent->SetNpcEntity(pkNpcEntity);
		pkWayPointComponent->SetWaitTime(fWaitTime);
		pkEntity->AddComponent(pkWayPointComponent, false);

		pkNpcComponent->SetWayPoint(uiWayPointId, pkEntity);
	}

	return true;
}

bool CWayPointsDB::Save(CTile* pkTile)
{
	Clear();

	WriteTopLine();

	NiEntityInterface* pkEntity;
	CNpcComponent* pkNpcComponent;
	NiFixedString kTranslationName = "Translation";
	NiFixedString kWaitTimeName = "等待时间";
	NiPoint3 kPosition;
	float fWaitTime;

	unsigned int uiTileId = pkTile->GetX()*100 + pkTile->GetY();
	unsigned int uiMapId = 0;
	MapInfo* pkMapInfo;
	const NiFixedString& kMapName = CMap::Get()->GetMapName();
	if(g_pkWorldMapDB->GetMapInfo(kMapName, pkMapInfo))
	{
		uiMapId = pkMapInfo->m_uiID;
	}

	NiTPrimitiveArray<NiEntityInterface*>& kEntities = pkTile->GetNpcEntities();
	for(unsigned int ui = 0; ui < kEntities.GetSize(); ++ui)
	{
		pkEntity = kEntities.GetAt(ui);
		pkNpcComponent = (CNpcComponent*)pkEntity->GetComponentByTemplateID(CNpcComponent::ms_kTemplateID);
		if(pkNpcComponent->GetWayPointCount())
		{
			pkEntity->GetPropertyData(kTranslationName, kPosition, 0);

			//把NPC的位置当作第一点加进去
			WriteUInt((uiMapId*10000 + uiTileId) * 1000 + ui); //spawn id
			WriteUInt(0); //way point id
			WriteFloat(kPosition.x); //x
			WriteFloat(kPosition.y); //y
			WriteFloat(kPosition.z); //z
			WriteFloat(0.f); //wait time
			WriteUInt(0);
			WriteToken("0"); //forwardemoteoneshot
			WriteToken("0"); //forwardemoteid
			WriteToken("0"); //backwardemoteoneshot
			WriteToken("0"); //backwardemoteid
			WriteToken("0"); //forwardskinid
			WriteToken("0", true); //backwardskinid
		}

		for(unsigned int uj = 0; uj < pkNpcComponent->GetWayPointCount(); ++uj)
		{
			pkEntity = pkNpcComponent->GetWayPoint(uj);
			pkEntity->GetPropertyData(kTranslationName, kPosition, 0);
			pkEntity->GetPropertyData(kWaitTimeName, fWaitTime, 0);

			WriteUInt((uiMapId*10000 + uiTileId) * 1000 + ui); //spawn id
			WriteUInt(uj + 1); //way point id
			WriteFloat(kPosition.x); //x
			WriteFloat(kPosition.y); //y
			WriteFloat(kPosition.z); //z
			WriteFloat(fWaitTime); //wait time
			WriteUInt(0); //flags
			WriteToken("0"); //forwardemoteoneshot
			WriteToken("0"); //forwardemoteid
			WriteToken("0"); //backwardemoteoneshot
			WriteToken("0"); //backwardemoteid
			WriteToken("0"); //forwardskinid
			WriteToken("0", true); //backwardskinid
		}
	}

	//创建目录
	char acPath[NI_MAX_PATH];
	NiSprintf(acPath, NI_MAX_PATH, "%s\\Data\\Maps\\%s\\NPC",  TerrainHelper::GetServerPath(), (const char*)kMapName); 
	NiFile::CreateDirectoryRecursive(acPath);

	//合成绝对路径
	char acFilename[NI_MAX_PATH];
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\Maps\\%s\\NPC\\waypoints_%d_%d.csv", TerrainHelper::GetServerPath(), (const char*)kMapName, pkTile->GetX(), pkTile->GetY());

	return CCSVFile::Save(acFilename);
}