#pragma once

#include "CSVFile.h"
class CTile;
class CWayPointsDB : public CCSVFile
{
public:
	bool ReadTopLine();
	void WriteTopLine();

	bool Load(CTile* pkTile);
    bool Save(CTile* pkTile);
};
