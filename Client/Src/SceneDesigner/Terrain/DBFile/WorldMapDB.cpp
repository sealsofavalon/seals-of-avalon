#include "TerrainPCH.h"
#include "WorldMapDB.h"

CWorldMapDB* CWorldMapDB::ms_pkThis = NULL;

CWorldMapDB::CWorldMapDB()
{
	NIASSERT(ms_pkThis == NULL);
	ms_pkThis = this;
}

CWorldMapDB::~CWorldMapDB()
{
	ms_pkThis = NULL;
}

bool CWorldMapDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

    NiString kString;
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
		MapInfo* pkMapInfo = NiNew MapInfo;
        pkMapInfo->m_uiID = GetUInt(ui, CWorldMapDB::EntryID); 
        GetString(ui, CWorldMapDB::Name, pkMapInfo->m_kName);

        kString = pkMapInfo->m_kName;
        kString.ToUpper();
        pkMapInfo->m_kName = kString;

        MapInfo* pkT;
        if(!m_kMapInfos.GetAt(pkMapInfo->m_kName, pkT))
		    m_kMapInfos.SetAt(pkMapInfo->m_kName, pkMapInfo);
        else
            NiDelete pkMapInfo;
	}

	return true;
}

void CWorldMapDB::Unload()
{
	NiFixedString kName;
	MapInfo* pkMapInfo;
    NiTMapIterator pos = m_kMapInfos.GetFirstPos();
    while (pos)
    {
        m_kMapInfos.GetNext(pos, kName, pkMapInfo);
        NiDelete pkMapInfo;
    }

	CDBFile::Unload();
}

bool CWorldMapDB::GetMapInfo(const NiFixedString& kName, MapInfo*& pkMapInfo) const
{
    NiString kString = kName;
    kString.ToUpper();

	return m_kMapInfos.GetAt((const char*)kString, pkMapInfo);
}

CWorldMapDB* CWorldMapDB::Get() 
{ 
	return ms_pkThis;
}