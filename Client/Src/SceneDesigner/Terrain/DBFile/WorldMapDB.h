#pragma once

#include "TerrainLibType.h"
#include "DBFile.h"

struct TERRAIN_ENTRY MapInfo : public NiMemObject
{
    unsigned int m_uiID;
    NiFixedString m_kName;
};

class TERRAIN_ENTRY CWorldMapDB : public CDBFile
{
private:
    NiTFixedStringMap<MapInfo*> m_kMapInfos;
	static CWorldMapDB* ms_pkThis;

public:
    enum
	{
		EntryID = 0, //unsigned int
        Name = 19, //string
	};

	CWorldMapDB();
	virtual ~CWorldMapDB();

    virtual bool Load(const char* pcName);
	virtual void Unload();

	bool GetMapInfo(const NiFixedString& kName, MapInfo*& pkMapInfo) const;

	static CWorldMapDB* Get();
};

#define g_pkWorldMapDB (CWorldMapDB::Get())