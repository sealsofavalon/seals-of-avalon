#include "TerrainPCH.h"
#include "DepthStencilBufferF32.h"

CDepthStencilBufferF32* CDepthStencilBufferF32::Create(unsigned int uiWidth, unsigned int uiHeight, Ni2DBuffer::MultiSamplePreference eMSAAPref)
{
	CDepthStencilBufferF32* pkDSBuffer = NiNew CDepthStencilBufferF32();
	pkDSBuffer->m_uiWidth = uiWidth;
	pkDSBuffer->m_uiHeight = uiHeight;

	if( CDepthStencilBufferDataF32::Create(pkDSBuffer, eMSAAPref) == NULL )
	{
		NiDelete pkDSBuffer;
		return NULL;
	}
	return pkDSBuffer;
}

//---------------------------------------------------------------------------
CDepthStencilBufferDataF32* CDepthStencilBufferDataF32::Create(CDepthStencilBufferF32*& pkBuffer, Ni2DBuffer::MultiSamplePreference eMSAAPref)
{
    // Determine if the D3D format is compatible with the device.
    NiDX9Renderer* pkRenderer = (NiDX9Renderer*)NiDX9Renderer::GetRenderer();
    const NiRenderTargetGroup* pkDefaultRenderTargetGroup = pkRenderer->GetDefaultRenderTargetGroup();
    NIASSERT(pkDefaultRenderTargetGroup);

    HRESULT eD3DRet = pkRenderer->GetDirect3D()->CheckDeviceFormat( pkRenderer->GetAdapter(), pkRenderer->GetDevType(), pkRenderer->GetAdapterFormat(), D3DUSAGE_DEPTHSTENCIL, D3DRTYPE_SURFACE, D3DFMT_D32F_LOCKABLE);
    
	// The format is not compatible, the renderer data failed to create.
    if (FAILED(eD3DRet))
    {
        NiDX9Renderer::Warning("CDepthStencilBufferDataF32::Create> FAILED");
        return NULL;
    }

    CDepthStencilBufferDataF32* pkThis = NiNew CDepthStencilBufferDataF32();

    NIASSERT(pkBuffer != NULL);

    unsigned int uiMultiSampleQuality = 0;
    D3DMULTISAMPLE_TYPE eMultisampleType = D3DMULTISAMPLE_NONE;
    GetMSAAD3DTypeAndQualityFromPref(eMSAAPref, eMultisampleType, uiMultiSampleQuality);

    BOOL bDiscard = false;

    // Create the depth/stencil buffer.
    eD3DRet = pkRenderer->GetD3DDevice()->CreateDepthStencilSurface(pkBuffer->GetWidth(), pkBuffer->GetHeight(),  D3DFMT_D32F_LOCKABLE, eMultisampleType, uiMultiSampleQuality, bDiscard, &pkThis->m_pkSurface, NULL);

    if (FAILED(eD3DRet))
    {
        NiDX9Renderer::Warning("CDepthStencilBufferDataF32::Create> FAILED");
        
        NiDelete pkThis;
        return NULL;
    }

    //  Get the surface desc...
    D3DSURFACE_DESC kSurfDesc;
    HRESULT eD3dRet = pkThis->m_pkSurface->GetDesc(&kSurfDesc);
    if (FAILED(eD3dRet))
    {
        NiDelete pkThis;
        return NULL;
    }

    // Compute the NiRenderer representation of the D3D pixel format.
    pkThis->m_pkPixelFormat = NiNew NiPixelFormat(NiPixelFormat::FORMAT_DEPTH_STENCIL, NiPixelFormat::COMP_DEPTH, NiPixelFormat::REP_NORM_INT, 32);

    pkBuffer->SetRendererData(pkThis);
    pkThis->m_pkBuffer = pkBuffer;
    pkThis->m_eMSAAPref = GetMSAAPrefFromD3DTypeAndQuality(kSurfDesc.MultiSampleType, kSurfDesc.MultiSampleQuality);

    return pkThis;
}
