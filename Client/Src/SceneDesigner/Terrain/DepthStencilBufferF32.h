
class CDepthStencilBufferF32 : public NiDepthStencilBuffer
{
public:
	static CDepthStencilBufferF32* Create(unsigned int uiWidth, unsigned int uiHeight, Ni2DBuffer::MultiSamplePreference eMSAAPref = Ni2DBuffer::MULTISAMPLE_NONE);
};

//---------------------------------------------------------------------------
// Class representing a depth/stencil buffer
class CDepthStencilBufferDataF32 : public NiDX9DepthStencilBufferData
{
public:

    // IMPORTANT NOTE:The create method passes in a Ni2DBuffer* reference.
    // Since DX9 requires that you create the device prior to creating
    // the buffer, the buffer and its renderer data are populated by 
    // the Create call. Note that if the pkBuffer is NULL, a new buffer
    // is created. If it is not stored in a smart pointer, it will be 
    // LEAKED by the application.
    static CDepthStencilBufferDataF32* Create(CDepthStencilBufferF32*& pkBuffer, Ni2DBuffer::MultiSamplePreference eMSAAPref);
};