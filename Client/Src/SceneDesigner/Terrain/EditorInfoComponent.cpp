#include "TerrainPCH.h"
#include "EditorInfoComponent.h"

NiFixedString CEditorInfoComponent::ms_kName;
NiFixedString CEditorInfoComponent::ms_kClassName;
NiUniqueID CEditorInfoComponent::ms_kTemplateID;

NiFixedString CEditorInfoComponent::ms_kTileIndexPropertyName;
NiFixedString CEditorInfoComponent::ms_kChunkPropertyName;
NiFixedString CEditorInfoComponent::ms_kQuadTreeChunksName;

CEditorInfoComponent::CEditorInfoComponent()
	:NiGeneralComponent(ms_kName, ms_kTemplateID)
{
	AddProperty(ms_kTileIndexPropertyName, ms_kTileIndexPropertyName, PT_POINT2, PT_POINT2, ms_kTileIndexPropertyName);
    AddProperty(ms_kChunkPropertyName, ms_kChunkPropertyName, PT_POINT2, PT_POINT2, ms_kChunkPropertyName);
	AddProperty(ms_kQuadTreeChunksName, ms_kQuadTreeChunksName, PT_POINT2, PT_POINT2, ms_kQuadTreeChunksName);
	MakeCollection(ms_kQuadTreeChunksName, true);

	Reset();
}

void CEditorInfoComponent::Reset()
{
	SetPropertyData(ms_kTileIndexPropertyName, NiPoint2(-1.f, -1.f), 0);
    SetPropertyData(ms_kChunkPropertyName, NiPoint2(-1.f, -1.f), 0);
	bool bCountSet;
	SetElementCount(ms_kQuadTreeChunksName, 0, bCountSet);
}

NiEntityComponentInterface* CEditorInfoComponent::Clone(bool bInheritProperties)
{
	CEditorInfoComponent* pkClone = NiNew CEditorInfoComponent();

	unsigned int uiSize = m_kProperties.GetSize();
	for (unsigned int ui = 0; ui < uiSize; ui++)
	{
		pkClone->m_kProperties.Add(m_kProperties.GetAt(ui)->Clone());
	}

	return pkClone;
}

NiBool CEditorInfoComponent::SetTemplateID(const NiUniqueID& kTemplateID)
{
    return false;
}

NiFixedString CEditorInfoComponent::GetClassName() const
{
    return ms_kClassName; 
}

NiFixedString CEditorInfoComponent::GetName() const
{
    return ms_kName;
}

NiBool CEditorInfoComponent::SetName(const NiFixedString& kName)
{
    return false;
}

NiBool CEditorInfoComponent::CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const
{
	if( kPropertyName == ms_kTileIndexPropertyName
     || kPropertyName == ms_kChunkPropertyName
	 || kPropertyName == ms_kQuadTreeChunksName)
	{
		bCanReset = false;
		return true;
	}

	return NiGeneralComponent::CanResetProperty(kPropertyName, bCanReset);
}

NiBool CEditorInfoComponent::MakeCollection(const NiFixedString& kPropertyName, bool bCollection)
{
	if( kPropertyName == ms_kTileIndexPropertyName 
     || kPropertyName == ms_kChunkPropertyName ) 
	{
		return true;
	}

	return NiGeneralComponent::MakeCollection(kPropertyName, bCollection);
}

NiBool CEditorInfoComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly)
{
	if( kPropertyName == ms_kTileIndexPropertyName
     || kPropertyName == ms_kChunkPropertyName
	 || kPropertyName == ms_kQuadTreeChunksName )
	{
		bIsReadOnly = true;
		return true;
	}

	return NiGeneralComponent::IsPropertyReadOnly(kPropertyName, bIsReadOnly);
}

void CEditorInfoComponent::_SDMInit()
{
	ms_kName = "Editor Info";
	ms_kClassName = "CEditorInfoComponent";
	ms_kTemplateID = NiUniqueID(0x37,0x8B,0x71,0x24,0x60,0xAA,0xBB,0x44, 0xAB,0x18,0x27,0x2,0xA0,0x2A,0x4B,0xED);

	ms_kTileIndexPropertyName = "Tile Index";
    ms_kChunkPropertyName = "Chunk Pos";
	ms_kQuadTreeChunksName = "QuadTree Chunks";
}

void CEditorInfoComponent::_SDMShutdown()
{
	ms_kName = NULL;
	ms_kClassName = NULL;
	ms_kTileIndexPropertyName = NULL;
    ms_kChunkPropertyName = NULL;
	ms_kQuadTreeChunksName = NULL;
}


