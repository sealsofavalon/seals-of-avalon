#pragma once

#include "TerrainLibType.h"

class TERRAIN_ENTRY CEditorInfoComponent : public NiGeneralComponent
{
public:
	CEditorInfoComponent();

	NiEntityComponentInterface* Clone(bool bInheritProperties);
	NiBool SetTemplateID(const NiUniqueID& kTemplateID);
	NiFixedString GetClassName() const;
	NiFixedString GetName() const;
	NiBool SetName(const NiFixedString& kName);
	NiBool CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const;
	NiBool MakeCollection(const NiFixedString& kPropertyName, bool bCollection);
	NiBool IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly);

	void Reset();

    static void _SDMInit();
    static void _SDMShutdown();

	static NiFixedString ms_kName;
	static NiFixedString ms_kClassName;
	static NiUniqueID ms_kTemplateID;

	static NiFixedString ms_kTileIndexPropertyName;
    static NiFixedString ms_kChunkPropertyName;
	static NiFixedString ms_kQuadTreeChunksName;
};