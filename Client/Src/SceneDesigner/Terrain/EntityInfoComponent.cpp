#include "TerrainPCH.h"

#include "EntityInfoComponent.h"

NiFixedString CEntityInfoComponent::ms_kClassName;
NiFixedString CEntityInfoComponent::ms_kComponentName;

NiFixedString CEntityInfoComponent::ms_kReflectName;
NiFixedString CEntityInfoComponent::ms_kCastShadowName;
NiFixedString CEntityInfoComponent::ms_kCollideCameraName;

NiFixedString CEntityInfoComponent::ms_kEntityTypeName;
NiFixedString CEntityInfoComponent::ms_kTransportIDName;

NiFixedString CEntityInfoComponent::ms_kModelName;
NiFixedString CEntityInfoComponent::ms_kTransportName;

NiUniqueID CEntityInfoComponent::ms_kTemplateID;


//---------------------------------------------------------------------------
void CEntityInfoComponent::_SDMInit()
{
    ms_kClassName = "CRenderInfoComponent";
    ms_kComponentName = "Entity Info";

    ms_kReflectName = "反射";
    ms_kCastShadowName = "产生阴影";
    ms_kCollideCameraName = "碰撞镜头";

	ms_kEntityTypeName = "类型";
	ms_kTransportIDName = "传送门ID";

	ms_kModelName = "模型";
	ms_kTransportName = "传送门";

    ms_kTemplateID = NiUniqueID(0xBA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAD,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA);
}

//---------------------------------------------------------------------------
void CEntityInfoComponent::_SDMShutdown()
{
    ms_kClassName = NULL;
    ms_kComponentName = NULL;

    ms_kReflectName = NULL;
    ms_kCastShadowName = NULL;
    ms_kCollideCameraName = NULL;

	ms_kEntityTypeName = NULL;
	ms_kTransportIDName = NULL;
	ms_kModelName = NULL;
	ms_kTransportName = NULL;
}

CEntityInfoComponent::CEntityInfoComponent()
	:m_bReflect(false)
    ,m_bCastShadow(true)
    ,m_bCollideCamera(true)
	,m_uiEntityType(ET_MODEL)
	,m_uiTransportID(0)
    ,m_uiFrameCount(0xFFFFFFFF)
	,m_uiCollisionCount(0xFFFFFFFF)
{
}

//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* CEntityInfoComponent::Clone(bool bInheritProperties)
{
	CEntityInfoComponent* pkEntityInfoComponent = NiNew CEntityInfoComponent;
    pkEntityInfoComponent->SetReflect(m_bReflect);
    pkEntityInfoComponent->SetCastShadow(m_bCastShadow);
    pkEntityInfoComponent->SetCollideCamera(m_bCollideCamera);
	pkEntityInfoComponent->SetEntityType(m_uiEntityType);
	pkEntityInfoComponent->SetTransportID(m_uiTransportID);

    return pkEntityInfoComponent;
}
//---------------------------------------------------------------------------
NiEntityComponentInterface* CEntityInfoComponent::GetMasterComponent()
{
    return NULL;
}
//---------------------------------------------------------------------------
void CEntityInfoComponent::SetMasterComponent(NiEntityComponentInterface* pkMasterComponent)
{
}
//---------------------------------------------------------------------------
void CEntityInfoComponent::GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
}
//---------------------------------------------------------------------------
// NiEntityPropertyInterface overrides.
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::SetTemplateID(const NiUniqueID& kID)
{
    //Not supported for custom components
    return false;
}
//---------------------------------------------------------------------------
NiUniqueID  CEntityInfoComponent::GetTemplateID()
{
    return ms_kTemplateID;
}
//---------------------------------------------------------------------------
void CEntityInfoComponent::AddReference()
{
    this->IncRefCount();
}
//---------------------------------------------------------------------------
void CEntityInfoComponent::RemoveReference()
{
    this->DecRefCount();
}
//---------------------------------------------------------------------------
NiFixedString CEntityInfoComponent::GetClassName() const
{
    return ms_kClassName;
}
//---------------------------------------------------------------------------
NiFixedString CEntityInfoComponent::GetName() const
{
    return ms_kComponentName;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::SetName(const NiFixedString& kName)
{
    // This component does not allow its name to be set.
    return false;
}
//---------------------------------------------------------------------------
void CEntityInfoComponent::BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors)
{
}
//---------------------------------------------------------------------------
void CEntityInfoComponent::Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager)
{
}
//---------------------------------------------------------------------------
void CEntityInfoComponent::GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const
{
    kPropertyNames.Add(ms_kReflectName);
	kPropertyNames.Add(ms_kEntityTypeName);
	kPropertyNames.Add(ms_kTransportIDName);
    kPropertyNames.Add(ms_kCastShadowName);
    kPropertyNames.Add(ms_kCollideCameraName);
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const
{
    if( kPropertyName == ms_kReflectName 
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        bCanReset = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::ResetProperty(const NiFixedString& kPropertyName)
{
    return false;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique)
{
    return false;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const
{
    if( kPropertyName == ms_kReflectName 
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        kDisplayName = kPropertyName;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::SetDisplayName(const NiFixedString& kPropertyName, const NiFixedString& kDisplayName)
{
    // This component does not allow the display name to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName)
    {
        kPrimitiveType = PT_BOOL;
    }
	else if(kPropertyName == ms_kEntityTypeName)
	{
		kPrimitiveType = PT_STRING;
	}
	else if(kPropertyName == ms_kTransportIDName)
	{
		kPrimitiveType = PT_UINT;
	}
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::SetPrimitiveType(const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType)
{
    // This component does not allow the primitive type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName)
    {
        kSemanticType = PT_BOOL;
    }
	else if(kPropertyName == ms_kEntityTypeName)
	{
		kSemanticType = "Entity Type";
	}
	else if(kPropertyName == ms_kTransportIDName)
	{
		kSemanticType = PT_UINT;
	}
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::SetSemanticType(const NiFixedString& kPropertyName, const NiFixedString& kSemanticType)
{
    // This component does not allow the semantic type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        kDescription = kPropertyName;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::SetDescription(const NiFixedString& kPropertyName, const NiFixedString& kDescription)
{
    // This component does not allow the description to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        kCategory = ms_kComponentName;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly)
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName 
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        bIsReadOnly = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique)
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName 
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        bIsUnique = true;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable)
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName 
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        bIsSerializable = true;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable)
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName 
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        bIsInheritable = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex,bool& bIsExternalAssetPath) const
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName 
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        bIsExternalAssetPath = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        uiCount = 1;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet)
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName 
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        bCountSet = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CEntityInfoComponent::IsCollection(const NiFixedString& kPropertyName,bool& bIsCollection) const
{
    if( kPropertyName == ms_kReflectName
     || kPropertyName == ms_kCastShadowName
     || kPropertyName == ms_kCollideCameraName 
	 || kPropertyName == ms_kEntityTypeName 
	 || kPropertyName == ms_kTransportIDName )
    {
        bIsCollection = false;
        return true;
    }

    return false;
}

NiBool CEntityInfoComponent::GetPropertyData(const NiFixedString& kPropertyName, bool& bData, unsigned int uiIndex) const
{
    if (uiIndex != 0)
	{
		return false;
	}
    else if( kPropertyName == ms_kReflectName )
    {
        bData = m_bReflect;
    }
    else if( kPropertyName == ms_kCastShadowName )
    {
        bData = m_bCastShadow;
    }
    else if( kPropertyName == ms_kCollideCameraName )
    {
        bData = m_bCollideCamera;
    }
    else
    {
        return false;
    }

    return true;
}

NiBool CEntityInfoComponent::SetPropertyData(const NiFixedString& kPropertyName, bool bData, unsigned int uiIndex)
{
    if (uiIndex != 0)
	{
		return false;
	}
    else if( kPropertyName == ms_kReflectName )
    {
        m_bReflect = bData;
    }
    else if( kPropertyName == ms_kCastShadowName )
    {
        m_bCastShadow = bData;
    }
    else if( kPropertyName == ms_kCollideCameraName )
    {
        m_bCollideCamera = bData;
    }
    else
    {
        return false;
    }

    return true;
}

NiBool CEntityInfoComponent::GetPropertyData(const NiFixedString& kPropertyName, unsigned int& uiData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
    else if( kPropertyName == ms_kTransportIDName )
    {
        uiData = m_uiTransportID;
    }
    else
    {
        return false;
    }

    return true;
}

NiBool CEntityInfoComponent::SetPropertyData(const NiFixedString& kPropertyName, unsigned int uiData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
    else if( kPropertyName == ms_kTransportIDName )
    {
        m_uiTransportID = uiData;
    }
    else
    {
        return false;
    }

    return true;
}

NiBool CEntityInfoComponent::GetPropertyData(const NiFixedString& kPropertyName, NiFixedString& kData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
    else if( kPropertyName == ms_kEntityTypeName )
    {
		if(GetEntityType() == ET_TRANSPORT)
		{
			kData = ms_kTransportName;
		}
		else
		{
			kData = ms_kModelName;
		}
    }
    else
    {
        return false;
    }

    return true;
}

NiBool CEntityInfoComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiFixedString& kData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
    else if( kPropertyName == ms_kEntityTypeName )
    {
		if(	kData == ms_kModelName )
		{
			SetEntityType(ET_MODEL);
		}
		else if( kData == ms_kTransportName )
		{
			SetEntityType(ET_TRANSPORT);
		}
    }
    else
    {
        return false;
    }

    return true;
}
