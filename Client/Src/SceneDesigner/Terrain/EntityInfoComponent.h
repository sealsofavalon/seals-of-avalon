#pragma once

#include "TerrainLibType.h"
class TERRAIN_ENTRY CEntityInfoComponent : public NiRefObject,
                                    public NiEntityComponentInterface
{
public:
    enum EntityFlag
    {
        EF_HIDDEN = 0x01, //是否隐藏
        EF_REFLECT = 0x02, //是否反射
        EF_NO_SHDOW = 0x04, //是否产生阴影
		EF_NO_COLLIDE_CAMERA = 0x08, //是否和摄像机碰撞
    };

	enum EntityType
	{
		ET_MODEL = 0, //场景模型
		ET_TRANSPORT, //传送门
		ET_TYPE_MAX,
	};

    static void _SDMInit();
    static void _SDMShutdown();

    // Class name.
    static NiFixedString ms_kClassName;

    // Component name.
    static NiFixedString ms_kComponentName;

    // Property names.
    static NiFixedString ms_kReflectName;
    static NiFixedString ms_kCastShadowName;
    static NiFixedString ms_kCollideCameraName;

	static NiFixedString ms_kEntityTypeName;
	static NiFixedString ms_kTransportIDName;

	static NiFixedString ms_kModelName;
	static NiFixedString ms_kTransportName;

    // Dependent property names.
    static NiUniqueID ms_kTemplateID;

    bool GetReflect() const { return m_bReflect; }
    void SetReflect(bool bReflect) { m_bReflect = bReflect; }

    bool GetCastShadow() const { return m_bCastShadow; }
    void SetCastShadow(bool bCastShadow) { m_bCastShadow = bCastShadow; }

    bool GetCollideCamera() const { return m_bCollideCamera; }
    void SetCollideCamera(bool bCollideCamera) { m_bCollideCamera = bCollideCamera; }

	unsigned int GetEntityType() const { return m_uiEntityType; }
	void SetEntityType(unsigned int uiEntityType) 
	{
		NIASSERT(uiEntityType < ET_TYPE_MAX);

		m_uiEntityType = uiEntityType;
	}

	unsigned int GetTransportID() const { return m_uiTransportID; }
	void SetTransportID(unsigned int uiTransportID) { m_uiTransportID = uiTransportID; }

    unsigned int GetFrameCount() const { return m_uiFrameCount; }
    void SetFrameCount(unsigned int uiFrameCount) { m_uiFrameCount = uiFrameCount; }

    unsigned int GetCollisionCount() const { return m_uiCollisionCount; };
    void SetCollisionCount(unsigned int uiCollisionCount) { m_uiCollisionCount = uiCollisionCount; }
	
private:
    bool m_bReflect;
    bool m_bCastShadow;
    bool m_bCollideCamera;

	unsigned int m_uiEntityType;
	unsigned int m_uiTransportID;

    unsigned int m_uiFrameCount;
    unsigned int m_uiCollisionCount;

public:
    CEntityInfoComponent();

    // NiEntityComponentInterface overrides.
    virtual NiEntityComponentInterface* Clone(bool bInheritProperties);
    virtual NiEntityComponentInterface* GetMasterComponent();
    virtual void SetMasterComponent(NiEntityComponentInterface* pkMasterComponent);
    virtual void GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames);

    // NiEntityPropertyInterface overrides.
    virtual NiBool SetTemplateID(const NiUniqueID& kID);
    virtual NiUniqueID GetTemplateID();
    virtual void AddReference();
    virtual void RemoveReference();
    virtual NiFixedString GetClassName() const;
    virtual NiFixedString GetName() const;
    virtual NiBool SetName(const NiFixedString& kName);
    virtual void Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager);
    virtual void BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors);
    virtual void GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const;
    virtual NiBool CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const;
    virtual NiBool ResetProperty(const NiFixedString& kPropertyName);
    virtual NiBool MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique);

    virtual NiBool GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const;
    virtual NiBool SetDisplayName(const NiFixedString& kPropertyName, const NiFixedString& kDisplayName);
    virtual NiBool GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const;
    virtual NiBool SetPrimitiveType(const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType);
    virtual NiBool GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const;
    virtual NiBool SetSemanticType(const NiFixedString& kPropertyName, const NiFixedString& kSemanticType);
    virtual NiBool GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const;
    virtual NiBool SetDescription(const NiFixedString& kPropertyName, const NiFixedString& kDescription);
    virtual NiBool GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const;
    virtual NiBool IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly);
    virtual NiBool IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique);
    virtual NiBool IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable);
    virtual NiBool IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable);
    virtual NiBool IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex, bool& bIsExternalAssetPath) const;
    virtual NiBool GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const;
    virtual NiBool SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet);
    virtual NiBool IsCollection(const NiFixedString& kPropertyName, bool& bIsCollection) const;

    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, bool& bData, unsigned int uiIndex = 0) const;
    virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, bool bData, unsigned int uiIndex = 0);

	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, unsigned int& uiData, unsigned int uiIndex = 0) const;
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, unsigned int uiData, unsigned int uiIndex = 0);

	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiFixedString& kData, unsigned int uiIndex = 0) const;
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiFixedString& kData, unsigned int uiIndex = 0);
};