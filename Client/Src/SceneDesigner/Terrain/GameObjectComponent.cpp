#include "TerrainPCH.h"

#include "GameObjectComponent.h"

NiFixedString CGameObjectComponent::ms_kClassName;
NiFixedString CGameObjectComponent::ms_kComponentName;

NiFixedString CGameObjectComponent::ms_kGameObjectName;
NiFixedString CGameObjectComponent::ms_kEntryIDName;

NiFixedString CGameObjectComponent::ms_kTranslationName;
NiFixedString CGameObjectComponent::ms_kRotationName;
NiFixedString CGameObjectComponent::ms_kScaleName;

NiUniqueID CGameObjectComponent::ms_kTemplateID;

NiFactoryDeclarePropIntf(CGameObjectComponent);

//---------------------------------------------------------------------------
void CGameObjectComponent::_SDMInit()
{
    ms_kClassName = "CGameObjectComponent";
    ms_kComponentName = "GameObject";

    ms_kGameObjectName = "Game Object Name";
    ms_kEntryIDName = "Entry ID";

    ms_kTranslationName = "Translation";
    ms_kRotationName = "Rotation";
    ms_kScaleName = "Scale";

    ms_kTemplateID = NiUniqueID(0xAF, 0x60, 0xBE, 0xFA, 0xA2, 0x03, 0x42, 0x9e, 0x8D, 0x1D, 0xB0, 0x2B, 0xDC, 0x8E, 0x91, 0x4C);
	NiFactoryRegisterPropIntf(CGameObjectComponent);
}

//---------------------------------------------------------------------------
void CGameObjectComponent::_SDMShutdown()
{
    ms_kClassName = NULL;
    ms_kComponentName = NULL;

    ms_kTranslationName = NULL;
    ms_kRotationName = NULL;
    ms_kScaleName = NULL;

    ms_kGameObjectName = NULL;
    ms_kEntryIDName = NULL;
}

CGameObjectComponent::CGameObjectComponent(const NiFixedString& kGameObjectName, unsigned int uiEntryID)
    :m_kGameObjectName(kGameObjectName)
    ,m_uiEntryID(uiEntryID)
{
}

//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* CGameObjectComponent::Clone(bool bInheritProperties)
{
    return NiNew CGameObjectComponent(m_kGameObjectName, m_uiEntryID);
}
//---------------------------------------------------------------------------
NiEntityComponentInterface* CGameObjectComponent::GetMasterComponent()
{
    return NULL;
}
//---------------------------------------------------------------------------
void CGameObjectComponent::SetMasterComponent(NiEntityComponentInterface* pkMasterComponent)
{
}
//---------------------------------------------------------------------------
void CGameObjectComponent::GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
    kDependentPropertyNames.Add(ms_kTranslationName);
    kDependentPropertyNames.Add(ms_kRotationName);
    kDependentPropertyNames.Add(ms_kScaleName);
}
//---------------------------------------------------------------------------
// NiEntityPropertyInterface overrides.
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::SetTemplateID(const NiUniqueID& kID)
{
    //Not supported for custom components
    return false;
}
//---------------------------------------------------------------------------
NiUniqueID  CGameObjectComponent::GetTemplateID()
{
    return ms_kTemplateID;
}
//---------------------------------------------------------------------------
void CGameObjectComponent::AddReference()
{
    this->IncRefCount();
}
//---------------------------------------------------------------------------
void CGameObjectComponent::RemoveReference()
{
    this->DecRefCount();
}
//---------------------------------------------------------------------------
NiFixedString CGameObjectComponent::GetClassName() const
{
    return ms_kClassName;
}
//---------------------------------------------------------------------------
NiFixedString CGameObjectComponent::GetName() const
{
    return ms_kComponentName;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::SetName(const NiFixedString& kName)
{
    // This component does not allow its name to be set.
    return false;
}
//---------------------------------------------------------------------------
void CGameObjectComponent::BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors)
{
}
//---------------------------------------------------------------------------
void CGameObjectComponent::Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager)
{
}
//---------------------------------------------------------------------------
void CGameObjectComponent::GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const
{
    kPropertyNames.Add(ms_kGameObjectName);
    kPropertyNames.Add(ms_kEntryIDName);
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const
{
    if( kPropertyName == ms_kGameObjectName
     || kPropertyName == ms_kEntryIDName )
    {
        bCanReset = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::ResetProperty(const NiFixedString& kPropertyName)
{
    return false;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique)
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName )
	{
		bMadeUnique = false;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName )
	{
        kDisplayName = kPropertyName;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::SetDisplayName(const NiFixedString& kPropertyName, const NiFixedString& kDisplayName)
{
    // This component does not allow the display name to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const
{
    if (kPropertyName == ms_kGameObjectName)
    {
        kPrimitiveType = PT_STRING;
    }
    else if (kPropertyName == ms_kEntryIDName)
    {
        kPrimitiveType = PT_UINT;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::SetPrimitiveType(const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType)
{
    // This component does not allow the primitive type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const
{
    if (kPropertyName == ms_kGameObjectName)
    {
        kSemanticType = PT_STRING;
    }
    else if (kPropertyName == ms_kEntryIDName)
    {
        kSemanticType = PT_UINT;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::SetSemanticType(const NiFixedString& kPropertyName, const NiFixedString& kSemanticType)
{
    // This component does not allow the semantic type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName )
    {
        kDescription = kPropertyName;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::SetDescription(const NiFixedString& kPropertyName, const NiFixedString& kDescription)
{
    // This component does not allow the description to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName )
    {
        kCategory = ms_kComponentName;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly)
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName )
    {
        bIsReadOnly = true;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique)
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName )
    {
        bIsUnique = true;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable)
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName )
    {
        bIsSerializable = true;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable)
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName )
    {
        bIsInheritable = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex,bool& bIsExternalAssetPath) const
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName )
	{
        bIsExternalAssetPath = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName)
    {
        uiCount = 1;
    }
	else
	{
		return false;
	}

    return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet)
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName )
	{
        bCountSet = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::IsCollection(const NiFixedString& kPropertyName,bool& bIsCollection) const
{
	if( kPropertyName == ms_kGameObjectName
	 || kPropertyName == ms_kEntryIDName )
    {
        bIsCollection = false;
    }
	else
	{
		return false;
	}

    return true;
}

//---------------------------------------------------------------------------
NiBool CGameObjectComponent::GetPropertyData(const NiFixedString& kPropertyName, unsigned int& uiData, unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kEntryIDName)
    {
        uiData = GetEntryID();
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::SetPropertyData(const NiFixedString& kPropertyName, unsigned int uiData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kEntryIDName)
	{
		SetEntryID(uiData);
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CGameObjectComponent::GetPropertyData(const NiFixedString& kPropertyName, NiFixedString& kData,unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kGameObjectName)
    {
        kData = GetGameObjectName();
    }
    else
    {
        return false;
    }

    return true;
}


//---------------------------------------------------------------------------
NiBool CGameObjectComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiFixedString& kData,unsigned int uiIndex)
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kGameObjectName)
    {
        SetGameObjectName(kData);
    }
    else
    {
        return false;
    }

    return true;
}