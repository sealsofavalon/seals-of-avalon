#pragma once

#include "TerrainLibType.h"

class TERRAIN_ENTRY CGameObjectComponent : public NiRefObject,
                                    public NiEntityComponentInterface
{
public:
    static void _SDMInit();
    static void _SDMShutdown();

    // Class name.
    static NiFixedString ms_kClassName;

    // Component name.
    static NiFixedString ms_kComponentName;

    // Property names.
    static NiFixedString ms_kGameObjectName;
    static NiFixedString ms_kEntryIDName;

    // Dependent property names.
    static NiFixedString ms_kTranslationName;
    static NiFixedString ms_kRotationName;
    static NiFixedString ms_kScaleName;

    static NiUniqueID ms_kTemplateID;

	void SetEntryID(unsigned int uiEntryID) { m_uiEntryID = uiEntryID; }
	unsigned int GetEntryID() const { return m_uiEntryID; }
	void SetGameObjectName(const NiFixedString& kGameObjectName) { m_kGameObjectName = kGameObjectName; }
	const NiFixedString& GetGameObjectName() const { return m_kGameObjectName; }

private:
    unsigned int m_uiEntryID;
	NiFixedString m_kGameObjectName;

public:
    CGameObjectComponent(const NiFixedString& kName, unsigned int uiEntryID);
	CGameObjectComponent() {}

    // NiEntityComponentInterface overrides.
    virtual NiEntityComponentInterface* Clone(bool bInheritProperties);
    virtual NiEntityComponentInterface* GetMasterComponent();
    virtual void SetMasterComponent(NiEntityComponentInterface* pkMasterComponent);
    virtual void GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames);

    // NiEntityPropertyInterface overrides.
    virtual NiBool SetTemplateID(const NiUniqueID& kID);
    virtual NiUniqueID GetTemplateID();
    virtual void AddReference();
    virtual void RemoveReference();
    virtual NiFixedString GetClassName() const;
    virtual NiFixedString GetName() const;
    virtual NiBool SetName(const NiFixedString& kName);
    virtual void Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager);
    virtual void BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors);
    virtual void GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const;
    virtual NiBool CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const;
    virtual NiBool ResetProperty(const NiFixedString& kPropertyName);
    virtual NiBool MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique);
    virtual NiBool GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const;
    virtual NiBool SetDisplayName(const NiFixedString& kPropertyName, const NiFixedString& kDisplayName);
    virtual NiBool GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const;
    virtual NiBool SetPrimitiveType(const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType);
    virtual NiBool GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const;
    virtual NiBool SetSemanticType(const NiFixedString& kPropertyName, const NiFixedString& kSemanticType);
    virtual NiBool GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const;
    virtual NiBool SetDescription(const NiFixedString& kPropertyName, const NiFixedString& kDescription);
    virtual NiBool GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const;
    virtual NiBool IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly);
    virtual NiBool IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique);
    virtual NiBool IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable);
    virtual NiBool IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable);
    virtual NiBool IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex, bool& bIsExternalAssetPath) const;
    virtual NiBool GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const;
    virtual NiBool SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet);
    virtual NiBool IsCollection(const NiFixedString& kPropertyName, bool& bIsCollection) const;

    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, unsigned int& uiData, unsigned int uiIndex) const;
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, unsigned int uiData, unsigned int uiIndex);
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiFixedString& kData, unsigned int uiIndex) const;
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiFixedString& kData,unsigned int uiIndex);
};