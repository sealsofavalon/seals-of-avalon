#include "TerrainPCH.h"
#include "GrassInfo.h"

void CGrassInfo::Load(NiBinaryStream* pkStream)
{
    pkStream->Read(&m_uiGrassCount, sizeof(m_uiGrassCount));
    if(m_uiGrassCount == 0)
        return;

    unsigned int* puiBits = m_kGrassMask.GetBits();
    unsigned int uiCount = m_kGrassMask.GetBitCount() / 32;
    pkStream->Read(puiBits, uiCount * sizeof(unsigned int));


    unsigned int uiBits;
    for(unsigned int i = 0; i < 32; ++i)
    {
        uiBits = *puiBits++;
        for(unsigned int j = 0; j < 32; ++j)
        {
            if(uiBits & (1 << j))
                pkStream->Read(&m_kGrassInstances[i][j], sizeof(unsigned short));
            else
                m_kGrassInstances[i][j] = 0;
        }
    }

    m_uiGrassCount = m_kGrassMask.GetEffectiveSize();
}

void CGrassInfo::Save(NiBinaryStream* pkStream)
{
    m_uiGrassCount = m_kGrassMask.GetEffectiveSize();
    pkStream->Write(&m_uiGrassCount, sizeof(m_uiGrassCount));
    if(m_uiGrassCount == 0)
        return;

    const unsigned int* puiBits = m_kGrassMask.GetBits();
    unsigned int uiCount = m_kGrassMask.GetBitCount() / 32;
    pkStream->Write(puiBits, uiCount * sizeof(unsigned int));
    unsigned int uiBits;
    
    for(unsigned int i = 0; i < 32; ++i)
    {
        uiBits = *puiBits++;
        for(unsigned int j = 0; j < 32; ++j)
        {
            if(uiBits & (1 << j))
                pkStream->Write(&m_kGrassInstances[i][j], sizeof(unsigned short));
        }
    }
}

bool CGrassInfo::SetGrass(unsigned int i, unsigned int j, unsigned short grass)
{
    if(m_kGrassMask.Test(i*32 + j) && m_kGrassInstances[i][j] == grass)
        return false;

    m_kGrassMask.Set(i*32 + j);
    m_kGrassInstances[i][j] = grass;
    m_uiGrassCount++;
    return true;
}

bool CGrassInfo::ClearGrass(unsigned int i, unsigned int j)
{
    if( !m_kGrassMask.Test(i*32 + j) )
        return false;

    m_kGrassMask.Clear(i*32 + j);
    m_uiGrassCount--;
    return true;
}
