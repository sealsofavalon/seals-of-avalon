#ifndef GRASS_INFO_H
#define GRASS_INFO_H
#include "BitArray.h"

//大小
//4bit 0.2米--16米
static float s_fSizeTable[] = {0.2f, 0.4f, 0.6f, 0.8f, 1.f, 1.5f, 2.f, 2.5f, 3.f, 3.5f, 4.f, 5.f, 6.f, 8.f, 12.f, 16.f};
inline float Id2Size(unsigned int i)
{
    return s_fSizeTable[i&0xF];
}

//方向
//3bit
static float s_fRotTable[] = {0.f, 10.f, 20.f, 30.f, 45.f, 60.f, 70.f, 80.f};
inline float id2rot(unsigned int i)
{
    return s_fRotTable[i&0x7];
}

inline float id2rad(int i)
{
    return id2rot(i)/180.f*NI_PI;
}

#define INVALID_GRASS (0xFFFF)
inline unsigned short MakeGrass(unsigned int idx, unsigned int size, unsigned int rot, unsigned int offset)
{
    NIASSERT(idx < 16);
    NIASSERT(size < 16);
    NIASSERT(rot < 8);
    NIASSERT(offset < 8);

    //-----------------------3bit------------3bit----------4bit-------4bit
    return unsigned short((offset << 11) + (rot << 8) + (size << 4) + idx);
}

inline unsigned int GrassIdx(unsigned short grass)
{
    return (grass & 0xF);
}

inline unsigned int GrassSizeId(unsigned short grass)
{
    return ((grass >> 4) & 0xF);
}

inline float GrassSize(unsigned short grass)
{
    return Id2Size(GrassSizeId(grass));
}

inline unsigned int GrassRotId(unsigned short grass)
{
    return ((grass >> 8) & 0x07);
}

inline float GrassRot(unsigned short grass)
{
    return id2rad(GrassRotId(grass));
}

inline unsigned int GrassOffset(unsigned short grass)
{
    return ((grass >> 11) & 0x07);
}

class CGrassInfo : public NiMemObject
{
public:
    CGrassInfo()
        :m_uiGrassCount(0)
    {}

    void Load(NiBinaryStream* pkStream);
    void Save(NiBinaryStream* pkStream);
    unsigned int GetGrassCount() const { return m_uiGrassCount; }

    bool SetGrass(unsigned int i, unsigned int j, unsigned short grass);
    bool ClearGrass(unsigned int i, unsigned int j);

    const unsigned int* GetMaskBits() const { return m_kGrassMask.GetBits(); }
    const unsigned short* GetGrassInstances() const { return &m_kGrassInstances[0][0]; }

private:
    CBitArray<32*32> m_kGrassMask; //1024 bits
    unsigned short m_kGrassInstances[32][32];
    unsigned int m_uiGrassCount;
};

#endif