#include "TerrainPCH.h"
#include "GrassMesh.h"
#include "Map.h"
#include "TerrainHelper.h"

CGrassMesh::CGrassMesh()
:m_uiXCount(1)
,m_uiYCount(1)
{
    m_spVertexShader = TerrainHelper::CreateVertexShader("Grass.hlsl", "GrassVS");
    m_spPixelShader = TerrainHelper::CreatePixelShader("Grass.hlsl", "GrassPS");
}

void CGrassMesh::AddGrass(const NiPoint3& kCenter, unsigned short grass)
{
    if(m_uiCurrentMesh == MAX_MESH_COUNT)
        return;
    
    unsigned short& usGrassCount = m_usGrassCount[m_uiCurrentMesh];
    //vertices
    float fSize = GrassSize(grass);

    //增加一个固定的小角度偏移, 防止相邻的两个面完全重叠在一起
    float fRot = GrassRot(grass) + NiFmod(10.f*kCenter.x + kCenter.y, 100.f)/5000.f;
    unsigned int idx = GrassIdx(grass)*4; //贴图坐标起始位置
    unsigned int offset = GrassOffset(grass);

    GrassVertex* pkGrassVertex = &m_kGrassVertices[m_uiCurrentMesh][usGrassCount*12];
    NiPoint3 kPos = kCenter - m_kOrigin;
    
    //triangle 0 ---------------------------------------------------------------------
    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 0, idx + 0, offset);  //0
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 1, idx + 1, offset);  //1
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 2, idx + 2, offset);  //2
    ++pkGrassVertex;


    //triangle 1 ---------------------------------------------------------------------
    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 2, idx + 2, offset);  //重复2
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 3, idx + 3, offset);  //3
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 0, idx + 0, offset); //重复0
    ++pkGrassVertex;

    //triangle 2 ---------------------------------------------------------------------
    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 4, idx + 0, offset);  //4
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 5, idx + 1, offset);  //5
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 6, idx + 2, offset);  //6
    ++pkGrassVertex;

    //triangle 3 ---------------------------------------------------------------------
    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 6, idx + 2, offset);  //重复6
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 7, idx + 3, offset);  //3
    ++pkGrassVertex;

    *pkGrassVertex = GrassVertex(kPos, fSize, fRot, 4, idx + 0, offset); //重复4
    ++pkGrassVertex;

    ++usGrassCount;
    if(usGrassCount == MAX_GRASS_COUNT)
        ++m_uiCurrentMesh;
}

void CGrassMesh::SetXYCount(unsigned int uiXCount, unsigned int uiYCount) 
{
    if( m_uiXCount != uiXCount || m_uiYCount != uiYCount )
    {
        m_uiXCount = uiXCount;
        m_uiYCount = uiYCount;
        CMap::Get()->SetDirty(true);
    }

    float fX = 1.f / m_uiXCount;
    float fY = 1.f / m_uiYCount;
    for(unsigned int i = 0; i < m_uiXCount; ++i)
    {
        for(unsigned int j = 0; j < m_uiYCount; ++j)
        {
            m_kUV[i*m_uiYCount + j][0].x = i*fX;
            m_kUV[i*m_uiYCount + j][0].y = j*fY + fY;

            m_kUV[i*m_uiYCount + j][1].x = i*fX;
            m_kUV[i*m_uiYCount + j][1].y = j*fY;

            m_kUV[i*m_uiYCount + j][2].x = i*fX + fX;
            m_kUV[i*m_uiYCount + j][2].y = j*fY;

            m_kUV[i*m_uiYCount + j][3].x = i*fX + fX;
            m_kUV[i*m_uiYCount + j][3].y = j*fY + fY;
        }
    }
}


void CGrassMesh::SetTextureName(const NiFixedString& kTextureName)
{
    if( m_kTextureName != kTextureName )
        CMap::Get()->SetDirty(true);

    m_kTextureName = kTextureName;
    if(m_kTextureName)
    {
        char acAbsPathName[1024];
		NiSprintf(acAbsPathName, 1024, "%s\\%s", TerrainHelper::GetClientPath(), (const char*)kTextureName);
		m_spTexture = NiSourceTexture::Create(acAbsPathName);
    }
    else
        m_spTexture = NULL;
}

void CGrassMesh::BeginRender(const NiPoint3& kOrigin)
{
    m_kOrigin = kOrigin;
    m_uiCurrentMesh = 0;
    for(int i = 0; i < MAX_MESH_COUNT; ++i)
        m_usGrassCount[i] = 0;
}

void CGrassMesh::EndRender()
{
    if(m_spTexture == NULL)
        return;

    NiDX9Renderer* pkRenderer = (NiDX9Renderer*)NiRenderer::GetRenderer();
    if(pkRenderer == NULL)
        return;

    LPDIRECT3DDEVICE9 pkD3DDevice = pkRenderer->GetD3DDevice();
    NiDX9RenderState* pkRenderState = pkRenderer->GetRenderState();

    bool bS0Mipmap;
    bool bNonPow2;
    bool bChanged;
    D3DBaseTexturePtr pkD3DTexture = pkRenderer->GetTextureManager()->PrepareTextureForRendering(m_spTexture, bChanged, bS0Mipmap, bNonPow2);
    pkRenderState->SetTexture(0, pkD3DTexture);

    pkRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
    //pkRenderState->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
    //pkRenderState->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
    pkRenderState->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
    pkRenderState->SetRenderState(D3DRS_ALPHAREF, 64);
    pkRenderState->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);
    pkRenderState->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
    pkRenderState->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
    pkRenderState->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);
    pkRenderState->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    pkRenderState->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    pkRenderState->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

    pkRenderState->SetVertexShader(m_spVertexShader->GetShaderHandle());
    pkRenderState->SetPixelShader(m_spPixelShader->GetShaderHandle());

    D3DXMATRIX kTempMat;
    D3DXMATRIX kD3DView = pkRenderer->GetD3DView();
    D3DXMATRIX kD3DProj = pkRenderer->GetD3DProj();

    float fValues[11 + 64][4];

    D3DXMatrixTranspose(&kTempMat, &kD3DView);
    memcpy(fValues[0], &kTempMat, sizeof(kTempMat));

    kTempMat = kD3DView * kD3DProj;
    D3DXMatrixTranspose(&kTempMat, &kTempMat);
    memcpy(fValues[4], &kTempMat, sizeof(kTempMat));

    CMap* pkMap = CMap::Get();
    NiLight* pkLight = pkMap->GetLight();

    const NiColor& kDiffuseColor = pkLight->GetDiffuseColor();

    fValues[8][0] = kDiffuseColor.r;
    fValues[8][1] = kDiffuseColor.g;
    fValues[8][2] = kDiffuseColor.b;
    fValues[8][3] = 1.f;

    const NiColor& kAmbientColor = pkLight->GetAmbientColor();
    fValues[9][0] = kAmbientColor.r;
    fValues[9][1] = kAmbientColor.g;
    fValues[9][2] = kAmbientColor.b;
    fValues[9][3] = 1.f;

    fValues[10][0] = CTerrainLightManager::Get()->GetFogDensity();
    fValues[10][1] = CMap::GetTime()*0.4f;//获取当前时间

    memcpy(fValues[11], m_kUV, sizeof(m_kUV));

    pkD3DDevice->SetVertexShaderConstantF(0, &fValues[0][0], sizeof(fValues)/(4*sizeof(float)));

    BOOL bValues[1];
    if(CTerrainLightManager::GetFogEnable())
    {
        bValues[0] = TRUE;

        //FogColor
        NiColor kFogColor = CTerrainLightManager::Get()->GetFogColor();
        fValues[0][0] = kFogColor.r;
        fValues[0][1] = kFogColor.g;
        fValues[0][2] = kFogColor.b;
        fValues[0][3] = 1.f;
    }
    else
    {
        bValues[0] = FALSE;
    }

    pkD3DDevice->SetPixelShaderConstantF(0, &fValues[0][0], 4);
    pkD3DDevice->SetPixelShaderConstantB(0, bValues, 1);

    pkD3DDevice->SetFVF(D3DFVF_XYZ|D3DFVF_TEX2|D3DFVF_TEXCOORDSIZE4(0)|D3DFVF_TEXCOORDSIZE1(1));

    unsigned short usGrassCount;
    for(unsigned int i = 0; i < MAX_MESH_COUNT; ++i)
    {
        usGrassCount = m_usGrassCount[i];
        if(usGrassCount > 0)
        {
            pkD3DDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST,
                usGrassCount*4,
                m_kGrassVertices[i],
                sizeof(GrassVertex));
        }
        else
            break;
    }
}

