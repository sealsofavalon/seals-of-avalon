#ifndef GRASS_MESH_H
#define GRASS_MESH_H

#include "TerrainLibType.h"

class NiPoint4 : public NiMemObject
{
public:
    float x, y, z, w;
};

struct GrassVertex : public NiMemObject
{
    NiPoint3 kPos;
    float fSize;
    float fRot;
    float fCorner;
    float fTex;
    float fOffset;

    GrassVertex()
    {
    }

    GrassVertex(const NiPoint3& pos, float size, float rot, int corner, int tex, int offset)
        :kPos(pos)
        ,fOffset(float(offset))
        ,fSize(size)
        ,fRot(rot)
        ,fCorner(float(corner))
        ,fTex(float(tex))
    {}
};

#define MAX_MESH_COUNT (8)
#define MAX_GRASS_COUNT (5400)
class TERRAIN_ENTRY CGrassMesh : public NiMemObject
{
public:
    CGrassMesh();

    void AddGrass(const NiPoint3& kCenter, unsigned short grass);
    void BeginRender(const NiPoint3& kOrigin);
    void EndRender();

    void GetXYCount(unsigned int& uiXCount, unsigned int& uiYCount) { uiXCount = m_uiXCount; uiYCount = m_uiYCount; }

    void SetXYCount(unsigned int uiXCount, unsigned int uiYCount);
    const NiFixedString& GetTextureName() const { return m_kTextureName; }
    void SetTextureName(const NiFixedString& kTextureName);

private:
    unsigned short m_usGrassCount[MAX_MESH_COUNT];
    GrassVertex m_kGrassVertices[MAX_MESH_COUNT][MAX_GRASS_COUNT*12]; //ÿ�Ų�8������
    NiTexturePtr m_spTexture;

    NiPoint4 m_kUV[16][4];
    NiFixedString m_kTextureName;
    unsigned int m_uiXCount;
    unsigned int m_uiYCount;

    NiD3DVertexShaderPtr m_spVertexShader;
    NiD3DPixelShaderPtr m_spPixelShader;

    unsigned int m_uiCurrentMesh;
    NiPoint3 m_kOrigin;
};

#endif