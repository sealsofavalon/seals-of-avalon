
#include "TerrainPCH.h"
#include "Map.h"
#include "TagStream.h"
#include "TerrainHelper.h"
#include "FreeImage.h"
#include "Water.h"
#include "TextureManager.h"
#include "EditorInfoComponent.h"
#include "CullingProcess.h"
#include "SpeedTree/SPTComponent.h"
#include "SpeedTree/ExternalAssetSPTHandler.h"
#include "SpeedTree/SpeedTreeShader.h"
#include "NpcComponent.h"
#include "WayPointComponent.h"
#include "GameObjectComponent.h"
#include "EntityInfoComponent.h"
#include "PathNodeComponent.h"
#include "TerrainShader.h"
#include "Player.h"
#include "ShadowGeometry.h"
#include "WaterEffect.h"
#include "SoundEmitterComponent.h"
#include "CollisionShader.h"

#define TILE_INDEX(x) (int(x) >> 9)
#define DEFAULT_SKY ("Data\\Sky\\Skydome.nif")

NiFixedString CMap::ms_kClassName;
NiFixedString CMap::ms_kName;
NiFixedString CMap::ms_kSceneRootPointerName;
NiFixedString CMap::ms_kNifFilePathName;
NiFixedString CMap::ms_kSPTFilePathName;
NiFixedString CMap::ms_kTranslationName;


CMap* CMap::ms_pkMap = NULL;
NiExternalAssetManager* CMap::ms_pkAssetManager = NULL;
CDBManager* CMap::ms_DBManager = NULL;
float CMap::ms_fTime = 0.f;

unsigned int CMap::ms_uiVisibleMask = 0xFFFFFFFF;
unsigned int CMap::ms_uiSelectMask = 0xFFFFFFFF;


bool IsValidTileIndex(int x, int y)
{
	if( x < 0
		|| x >= TILE_COUNT
		|| y < 0
		|| y >= TILE_COUNT )
	{
		return false;
	}

	return true;
}

CMap::CMap()
{
	NIASSERT( ms_pkMap == NULL );
	ms_pkMap = this;
	ms_pkMap->IncRefCount();

	memset(m_pkWaterShaders, 0, sizeof(m_pkWaterShaders));

	Reset();

	m_pkSceneRoot = NiNew NiNode;
	SetHidden(true);

	m_pkLight = NiNew NiDirectionalLight;
	m_pkLight->IncRefCount();

	m_pkTerrainLightManager = NiNew CTerrainLightManager;

	NiMatrix3 kMat;
	kMat.MakeIdentity();
	NiPoint3 kDir(1, 1, -1);
	kDir.Unitize();
	kMat.SetCol(0, kDir);
	m_pkLight->SetRotate(kMat);

	m_pkLight->SetAmbientColor(m_pkTerrainLightManager->GetAmbientColor());
	m_pkLight->SetDiffuseColor(m_pkTerrainLightManager->GetDiffuseColor());

	m_pkLight->Update(0.f, false);

	m_pkSceneRoot->AttachEffect(m_pkLight);
	m_pkSceneRoot->UpdateEffects();

	m_bDirty = false;

	m_spScene = NiNew NiScene("Main Scene");
    m_spErrorHandler = NiNew NiDefaultErrorHandler();

	m_uiFrameID = 0;
	m_uiCollisionCount = 0;

    m_spAccumulator = NiNew CAlphaAccumulator;

	m_pkVMapCover = new VMapCover;
}

CMap::~CMap()
{
	for(int i = 0; i < CACHE_SIZE; i++)
	{
		if( m_pkCacheTiles[i] )
		{
			m_pkCacheTiles[i]->DecRefCount();
			m_pkCacheTiles[i] = NULL;
		}
	}

	m_pkLight->DecRefCount();

	NiDelete m_pkTerrainLightManager;

	m_pkSceneRoot = 0;
	ms_pkMap = NULL;

	Reset();
}

void CMap::Reset()
{
	m_iCurrentX		= INVALID_INDEX;
	m_iCurrentY		= INVALID_INDEX;

	memset(m_iTileFlags, 0, sizeof(m_iTileFlags));
	memset(m_pkTiles, 0, sizeof(m_pkTiles));
	memset(m_pkCacheTiles, 0, sizeof(m_pkCacheTiles));

	for(int i = 0; i <MAX_WATER_COUNT; ++i)
	{
		if(m_pkWaterShaders[i])
		{
			m_pkWaterShaders[i]->DecRefCount();
			m_pkWaterShaders[i] = NULL;
		}
	}
}

int CMap::GetTileFlag(int x, int y) const
{
	if( IsValidTileIndex(x, y) )
	{
		return m_iTileFlags[x][y];
	}

	return 0;
}

void CMap::SetTileFlag(int x, int y, int flag)
{
	if( IsValidTileIndex(x, y) && m_iTileFlags[x][y] != flag )
	{
		m_bDirty = true;
		m_iTileFlags[x][y] = flag;
	}
}

const int* CMap::GetTileFlags() const
{
	return &m_iTileFlags[0][0];
}

float CMap::GetHeight(float x, float y)
{
	float h;
	if( GetHeight(x, y, h) )
		return h;

	return 10.f;
}

bool CMap::GetHeight(float x, float y, float& h)
{
	CChunk* chunk = GetChunk(x, y);
	if( chunk )
		return chunk->GetHeight(x, y, h);

	//边界点
	if( (int(x) & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(x - 1.f, y);
		if( chunk )
			return chunk->GetHeight(x, y, h);
	}

	//边界点
	if( (int(y) & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(x, y - 1.f);
		if( chunk )
			return chunk->GetHeight(x, y, h);
	}

	//顶点
	if( (int(x) & (CHUNK_SIZE - 1)) == 0 && (int(y) & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(x - 1.f, y - 1.f);
		if( chunk )
			return chunk->GetHeight(x, y, h);
	}

	return false;
}

bool CMap::GetHeightAndNormal(float x, float y, float&h, NiPoint3& kNormal)
{
	CChunk* pkChunk = GetChunk(x, y);
	if( pkChunk )
		return pkChunk->GetHeightAndNormal(x, y, h, kNormal);

	return false;
}

//------------------------------------------------------------------------------
void CMap::SetHeight(float x, float y, float h)
{
	if( h == GetHeight(float(x), float(y)) )
		return;

	CChunk* chunk;
	chunk = GetChunk(float(x), float(y));
	if( chunk )
		chunk->SetHeight(x, y, h);

	int nX = int(x);
	int nY = int(y);

	//边界点
	if( (nX & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x - 1), float(y));
		if( chunk )
			chunk->SetHeight(x, y, h);
	}

	//边界点
	if( (nY & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x), float(y - 1));
		if( chunk )
			chunk->SetHeight(x, y, h);
	}

	//顶点
	if( (nX & (CHUNK_SIZE - 1)) == 0 && (nY & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x - 1), float(y - 1));
		if( chunk )
			chunk->SetHeight(x, y, h);
	}

	//更新法线, 包括周围的4个点
	UpdateNormal(x, y);
	UpdateNormal(x - UNIT_SIZE, y);
	UpdateNormal(x, y - UNIT_SIZE);
	UpdateNormal(x + UNIT_SIZE, y);
	UpdateNormal(x, y + UNIT_SIZE);
}

void CMap::UpdateNormal(float x, float y)
{
	CChunk* chunk;
	chunk = GetChunk(float(x), float(y));
	if( chunk )
		chunk->UpdateNormal(x, y);

	int nX = int(x);
	int nY = int(y);

	//边界点
	if( (nX & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x - 1), float(y));
		if( chunk )
			chunk->UpdateNormal(x, y);
	}

	//边界点
	if( (nY & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x), float(y - 1));
		if( chunk )
			chunk->UpdateNormal(x, y);
	}

	//顶点
	if( (nX & (CHUNK_SIZE - 1)) == 0 && (nY & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x - 1), float(y - 1));
		if( chunk )
			chunk->UpdateNormal(x, y);
	}
}

float CMap::GetWaterHeight(float x, float y)
{
	float h;
	if( GetWaterHeight(x, y, h) )
		return h;

	return 10.f;
}

bool CMap::GetWaterHeight(float x, float y, float& h)
{
	CChunk* chunk = GetChunk(float(x), float(y));
	if( chunk )
		return chunk->GetWaterHeight(x, y, h);

	int nX = int(x);
	int nY = int(y);

	//边界点
	if( (nX & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x - 1), float(y));
		if( chunk )
			return chunk->GetWaterHeight(x, y, h);
	}

	//边界点
	if( (nY & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x), float(y - 1));
		if( chunk )
			return chunk->GetWaterHeight(x, y, h);
	}

	//顶点
	if( (nX & (CHUNK_SIZE - 1)) == 0 && (nY & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x - 1), float(y - 1));
		if( chunk )
			return chunk->GetWaterHeight(x, y, h);
	}

	return false;
}

void CMap::SetWaterHeight(float x, float y, float h)
{
	CChunk* chunk;
	chunk = GetChunk(float(x), float(y));
	if( chunk )
		chunk->SetWaterHeight(x, y, h);

	int nX = int(x);
	int nY = int(y);

	//边界点
	if( (nX & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x - 1), float(y));
		if( chunk )
			chunk->SetWaterHeight(x, y, h);
	}

	//边界点
	if( (nY & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x), float(y - 1));
		if( chunk )
			chunk->SetWaterHeight(x, y, h);
	}

	//顶点
	if( (nX & (CHUNK_SIZE - 1)) == 0 && (nY & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(float(x - 1), float(y - 1));
		if( chunk )
			chunk->SetWaterHeight(x, y, h);
	}

}

void CMap::SetWaterFlag(float x, float y, bool bHasWater, unsigned int uiWaterIndex)
{
	CChunk* chunk;
	chunk = GetChunk(float(x), float(y));
	if( chunk )
		chunk->SetWaterFlag(x, y, bHasWater, uiWaterIndex);

	//Water Flag 没有和其他的Chunk连接
	//不需要考虑边界点
}

bool CMap::GetWaterFlag(float x, float y)
{
	CChunk* chunk;
	chunk = GetChunk(float(x), float(y));
	if( chunk )
		return chunk->GetWaterFlag(x, y);

	//Water Flag 没有和其他的Chunk连接
	//不需要考虑边界点

	return false;
}

//单个水面是否在地形上面
bool CMap::WaterAboveTerrain(float x, float y, float h)
{
	if( h < GetHeight(float(x), float(y)) - 0.01f 
		&& h < GetHeight(float(x + UNIT_SIZE), float(y)) - 0.01f
		&& h < GetHeight(float(x + UNIT_SIZE), float(y + UNIT_SIZE)) - 0.01f
		&& h < GetHeight(float(x), float(y + UNIT_SIZE)) - 0.01f )
	{
		return false;
	}

	return true;
}

CWaterShader* CMap::GetWaterShader(unsigned int uiIndex)
{
	NIASSERT(uiIndex < MAX_WATER_COUNT);
	return m_pkWaterShaders[uiIndex];
}

bool CMap::ChangeWaterShader(const NiFixedString& kTexturePath, unsigned int uiWaterIndex)
{
	NIASSERT(uiWaterIndex < MAX_WATER_COUNT);

	if(m_pkWaterShaders[uiWaterIndex] == NULL)
	{
		m_pkWaterShaders[uiWaterIndex] = NiNew CWaterShader(uiWaterIndex);
		m_pkWaterShaders[uiWaterIndex]->IncRefCount();
		SetDirty(true);
	}

	if(m_pkWaterShaders[uiWaterIndex]->SetTexturePathName(kTexturePath))
	{
		SetDirty(true);
		OnWaterChanged(uiWaterIndex);//通知所有的水， Shader已经改变了
	}

	return true;
}

bool CMap::DeleteWaterShader(unsigned int uiWaterIndex)
{
	NIASSERT(uiWaterIndex < MAX_WATER_COUNT);

	if(m_pkWaterShaders[uiWaterIndex] != NULL)
	{
		m_pkWaterShaders[uiWaterIndex]->DecRefCount();
		m_pkWaterShaders[uiWaterIndex] = NULL;

		SetDirty(true);

		//如果当前没有装载的Tile， 使用了这个Water
		//以后装载的时候将会使用默认的Water

		OnWaterChanged(uiWaterIndex);//通知所有的水， Shader已经改变了
	}

	return true;
}

void CMap::BuidWaterEdgeMap()
{
	CTile* tile;
	for( int x = m_iCurrentX - 2; x < m_iCurrentX + 2; x++ )
	{
		for( int y = m_iCurrentY - 2; y < m_iCurrentY + 2; y++ )
		{
			tile = GetTileByIndex(x, y);
			if( tile )
			{
				tile->BuidWaterEdgeMap();
			}
		}
	}

	m_bDirty = true;
}

void CMap::SetWaterAlpha(float fAlpha, unsigned int uiWaterIndex)
{
	NIASSERT(uiWaterIndex < MAX_WATER_COUNT);
	if(m_pkWaterShaders[uiWaterIndex] != NULL)
	{
		if(m_pkWaterShaders[uiWaterIndex]->SetAlpha(fAlpha))
            SetDirty(true);
	}
}

void CMap::SetWaterAlphaBlend(bool bAlphaBlend, unsigned int uiWaterIndex)
{
	NIASSERT(uiWaterIndex < MAX_WATER_COUNT);
	if(m_pkWaterShaders[uiWaterIndex] != NULL)
	{
		if(m_pkWaterShaders[uiWaterIndex]->SetAlphaBlend(bAlphaBlend))
		    SetDirty(true);
	}
}

void CMap::SetWaterAdditiveLight(bool bAdditive, unsigned int uiWaterIndex)
{
	NIASSERT(uiWaterIndex < MAX_WATER_COUNT);
	if(m_pkWaterShaders[uiWaterIndex] != NULL)
	{
		if(m_pkWaterShaders[uiWaterIndex]->SetAdditiveLight(bAdditive))
		    SetDirty(true);
	}
}

void CMap::SetWaterRR(bool bRR, unsigned int uiWaterIndex)
{
	NIASSERT(uiWaterIndex < MAX_WATER_COUNT);
	if(m_pkWaterShaders[uiWaterIndex] != NULL)
	{
		if(m_pkWaterShaders[uiWaterIndex]->SetRR(bRR))
		    SetDirty(true);
	}
}

void CMap::SetWaterUVScale(float fUVScale, unsigned int uiWaterIndex)
{
	NIASSERT(uiWaterIndex < MAX_WATER_COUNT);
	if(m_pkWaterShaders[uiWaterIndex] != NULL)
	{
		if(m_pkWaterShaders[uiWaterIndex]->SetUVScale(fUVScale))
		    SetDirty(true);
	}
}

void CMap::SetWaterSpeed(float fSpeed, unsigned int uiWaterIndex)
{
	NIASSERT(uiWaterIndex < MAX_WATER_COUNT);
	if(m_pkWaterShaders[uiWaterIndex] != NULL && m_pkWaterShaders[uiWaterIndex]->GetSpeed() != fSpeed)
	{
		if(m_pkWaterShaders[uiWaterIndex]->SetSpeed(fSpeed))
		    SetDirty(true);
	}
}

void CMap::OnWaterChanged(unsigned int uiWaterIndex)
{
	CTile* pkTile;
	for( int x = m_iCurrentX - 2; x < m_iCurrentX + 2; x++ )
	{
		for( int y = m_iCurrentY - 2; y < m_iCurrentY + 2; y++ )
		{
			pkTile = GetTileByIndex(x, y);
			if( pkTile )
			{
				pkTile->OnWaterChanged(uiWaterIndex);
			}
		}
	}
}

void CMap::UpdateWaterShaders()
{
	for(int i = 0; i < MAX_WATER_COUNT; ++i)
	{
		if(m_pkWaterShaders[i])
		{
			m_pkWaterShaders[i]->Update(m_pkTerrainLightManager->GetWaterAmbientColor(), m_pkTerrainLightManager->GetWaterDiffuseColor());
		}
	}

    CWaterShader* pkDefaultWaterShader = CWater::GetDefaultWaterShader();
    if(pkDefaultWaterShader)
        pkDefaultWaterShader->Update(m_pkTerrainLightManager->GetWaterAmbientColor(), m_pkTerrainLightManager->GetWaterDiffuseColor());
}

bool CMap::TurnEdge(int x, int y, bool bTurnEdge)
{
	CChunk* chunk;
	chunk = GetChunk(float(x), float(y));
	if( chunk )
		return chunk->TurnEdge(x, y, bTurnEdge);

	return false;
}

bool CMap::IsTurnEdge(int x, int y)
{
	CChunk* chunk;
	chunk = GetChunk(float(x), float(y));
	if( chunk )
		return chunk->IsTurnEdge(x, y);

	return false;
}

bool CMap::ExcavateHole(int x, int y, bool bHole)
{
	CChunk* chunk;
	chunk = GetChunk(float(x), float(y));
	if( chunk )
		return chunk->ExcavateHole(x, y, bHole);

	return false;
}

bool CMap::IsHole(int x, int y)
{
	CChunk* chunk;
	chunk = GetChunk(float(x), float(y));
	if( chunk )
		return chunk->IsHole(x, y);

	return false;
}


void CMap::PaintMaterial(int x, int y, FIBITMAP* pkAlphaMap, const char* pcTextureName, float fScale)
{
	NIASSERT( pkAlphaMap );
	NIASSERT( pcTextureName );

	int iHalfWidth = FreeImage_GetWidth(pkAlphaMap)/(ALPHA_MAP_SIZE/CHUNK_SIZE)/2;//每个单位2个Alpha象素
	int iHalfHeight = FreeImage_GetHeight(pkAlphaMap)/(ALPHA_MAP_SIZE/CHUNK_SIZE)/2;

	CChunk* pkChunk;
	for( int X = x - iHalfWidth; X <= x + iHalfWidth - 1 + CHUNK_SIZE; X += CHUNK_SIZE )
	{
		for( int Y = y - iHalfHeight; Y <= y + iHalfHeight - 1 + CHUNK_SIZE; Y += CHUNK_SIZE )
		{
			pkChunk = GetChunk(float(X), float(Y));
			if( pkChunk )
				pkChunk->PaintMaterial( x, y, pkAlphaMap, pcTextureName, fScale );
		}
	}
}

void CMap::RemoveMaterial(int x, int y, const char* pcTextureName)
{
	CChunk* pkChunk = GetChunk(float(x), float(y));
	if( pkChunk )
		pkChunk->RemoveMaterial(pcTextureName);
}

void CMap::ChangeTexture(const char* pcSrcTextureName, const char* pcDestTextureName)
{
	CTile* pkTile = GetCurrentTile();
	if( pkTile )
	{
		if( pcDestTextureName )
			pkTile->ChangeTexture(pcSrcTextureName, pcDestTextureName);
		else
			pkTile->DeleteTexture(pcSrcTextureName);
	}
}

void CMap::GetCurrentTextureNames(NiTObjectArray<NiFixedString>& akTextureNames)
{
	akTextureNames.SetSize(0);
	CTile* pkTile = GetCurrentTile();
	if( pkTile )
		pkTile->GetTextureNames(akTextureNames);
}

void CMap::SetMoveable(float x, float y, bool bMoveable, int nLv)
{
	CChunk* pkChunk = GetChunk(x, y);
	if( pkChunk )
		pkChunk->SetMoveable(x, y, bMoveable, nLv);
}

bool CMap::GetMoveable(float x, float y)
{
    CChunk* pkChunk = GetChunk(x, y);
    if( pkChunk )
        return pkChunk->GetMoveable(x, y, WalkGrid);

    return false;
}

void CMap::CalcMoveable()
{
	CTile* pkTile;
	for( int x = m_iCurrentX - 2; x < m_iCurrentX + 2; x++ )
	{
		for( int y = m_iCurrentY - 2; y < m_iCurrentY + 2; y++ )
		{
			pkTile = GetTileByIndex(x, y);
			if(pkTile)
				pkTile->CalcMoveable();
		}
	}
}

void CMap::ClearMoveable()
{
	CTile* pkTile;
	for( int x = m_iCurrentX - 2; x < m_iCurrentX + 2; x++ )
	{
		for( int y = m_iCurrentY - 2; y < m_iCurrentY + 2; y++ )
		{
			pkTile = GetTileByIndex(x, y);
			if(pkTile)
				pkTile->ClearMoveable();
		}
	}
}

void CMap::SetAreaId(float x, float y, unsigned int uiAreaId)
{
	CChunk* chunk;
	chunk = GetChunk(x, y);
	if( chunk )
		chunk->SetAreaId(uiAreaId);
}

unsigned int CMap::GetAreaId(float x, float y)
{
	CChunk* chunk;
	chunk = GetChunk(x, y);
	if( chunk )
		return chunk->GetAreaId();

	return 0;
}

//从一个其他的地图传送过来
void CMap::Transport(const NiFixedString& kMapName)
{
	if( m_kMapName == kMapName )
		return;

	Unload();//释放之前的地图
	Reset();

	m_kMapName = kMapName;
	Load();//装载新的地图
}

bool CMap::Load()
{
	char acFilename[NI_MAX_PATH];
	//合成绝对路径
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\World\\Maps\\%s\\%s.map",  TerrainHelper::GetClientPath(), (const char*)m_kMapName, (const char*)m_kMapName);

	NiFile* pkFile = NiFile::GetFile(acFilename, NiFile::READ_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return false;
	}

	CTagStream kTagStream;
	NiFixedString kFilename;

	while( kTagStream.ReadFromStream( pkFile ) )
	{
		switch( kTagStream.GetTag() )
		{
		case 'MAIN':
			{
				kTagStream.Read(m_iTileFlags, sizeof(m_iTileFlags));
			}
			break;

		case 'SKY':
			{
				kTagStream.ReadFixedString(kFilename);
			}
			break;

		case 'WTMT'://water material
			{
				NiBool bHasWater;
				for(int i = 0; i < MAX_WATER_COUNT; ++i)
				{
					kTagStream.Read(&bHasWater, sizeof(bHasWater));
					if(bHasWater)
					{
						m_pkWaterShaders[i] = NiNew CWaterShader(i);
						m_pkWaterShaders[i]->IncRefCount();
						m_pkWaterShaders[i]->Load(kTagStream);
					}
					else
					{
						m_pkWaterShaders[i] = NULL;
					}
				}
			}
			break;

        case 'GRTX': //grass texture
            {
                unsigned int uiXCount;
                unsigned int uiYCount;
                NiFixedString kGrassTexture;
                kTagStream.ReadFixedString(kGrassTexture);
                kTagStream.Read(&uiXCount, sizeof(uiXCount));
                kTagStream.Read(&uiYCount, sizeof(uiYCount));

                m_kGrassMesh.SetTextureName(kGrassTexture);
                m_kGrassMesh.SetXYCount(uiXCount, uiYCount);
            }
            break;

		default:
			{
				//NIASSERT( false );
			}
			break;
		}
	}

	NiDelete pkFile;

	//Sky
	{
		if(!kFilename.Exists())
			kFilename = DEFAULT_SKY;

		m_kSky.Load(kFilename);
	}

    m_kPathNodeManager.Load(m_kMapName);
	m_pkTerrainLightManager->Load(m_kMapName);

	m_bDirty = false;
	return true;
}

void CMap::Unload()
{
	for(int i = 0; i < CACHE_SIZE; i++)
	{
		if( m_pkCacheTiles[i] )
		{
			NiDelete m_pkCacheTiles[i];
			m_pkCacheTiles[i] = NULL;
		}
	}

    //m_kGrassMesh.SetTextureName(NULL);
}

bool CMap::Save()
{
	TerrainHelper::ResetCurrentPath();

	char acPath[NI_MAX_PATH];
	NiSprintf(acPath, NI_MAX_PATH, "%s\\Data\\World\\Maps\\%s",  TerrainHelper::GetClientPath(), (const char*)m_kMapName); 
	NiFile::CreateDirectoryRecursive(acPath);

	SaveMap();

	SaveTiles();

	//SaveServerMap();

	return true;
}

//保存地图信息
bool CMap::SaveMap()
{
	if( m_bDirty )
	{
        NiMemStream kMemStream; //先写到内存
		CTagStream kTagStream;

		//标记
		{
			kTagStream.Reset();
			kTagStream.SetTag('MAIN');

			kTagStream.Write(m_iTileFlags, sizeof(m_iTileFlags));

			kTagStream.WriteToStream(&kMemStream);
		}

		//Sky
		{
			kTagStream.Reset();
			kTagStream.SetTag('SKY');

			kTagStream.WriteFixedString(m_kSky.GetFilename());
			kTagStream.WriteToStream(&kMemStream);
		}

		//water material
		{
			NiBool bHasWater = false;
			for(int i = 0; i < MAX_WATER_COUNT; ++i)
			{
				if(m_pkWaterShaders[i])
				{
					bHasWater = true;
					break;
				}
			}

			if(bHasWater)
			{
				kTagStream.Reset();
				kTagStream.SetTag('WTMT');

				for(int i = 0; i < MAX_WATER_COUNT; ++i)
				{
					if(m_pkWaterShaders[i])
					{
						bHasWater = 1;
						kTagStream.Write(&bHasWater, sizeof(bHasWater));
						m_pkWaterShaders[i]->Save(kTagStream);
					}
					else
					{
						bHasWater = 0;
						kTagStream.Write(&bHasWater, sizeof(bHasWater));
					}
				}

				kTagStream.WriteToStream(&kMemStream);
			}
		}

        if( m_kGrassMesh.GetTextureName() )
        {
            kTagStream.Reset();
            kTagStream.SetTag('GRTX');

            kTagStream.WriteFixedString(m_kGrassMesh.GetTextureName());

            unsigned int uiXCount;
            unsigned int uiYCount;
            m_kGrassMesh.GetXYCount(uiXCount, uiYCount);
            kTagStream.Write(&uiXCount, sizeof(uiXCount));
            kTagStream.Write(&uiYCount, sizeof(uiYCount));

            kTagStream.WriteToStream(&kMemStream);
        }

        char acFilename[NI_MAX_PATH];
        NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\World\\Maps\\%s\\%s.map", TerrainHelper::GetClientPath(), (const char*)m_kMapName, (const char*)m_kMapName);
        NiFile* pkFile = NiFile::GetFile(acFilename, NiFile::WRITE_ONLY);
        if( !pkFile || !(*pkFile) )
        {
            NiDelete pkFile;
            return false;
        }

        pkFile->Write(kMemStream.Str(), kMemStream.GetSize());
        kMemStream.Freeze(false);

        NiDelete pkFile;
        m_bDirty = false;
	}
    
    m_kPathNodeManager.Save(m_kMapName);
	m_pkTerrainLightManager->Save(m_kMapName);

	return true;
}

//保存中区
bool CMap::SaveTiles()
{
	//保存中区, 每个中区保存成一个文件
	CTile* pkTile;
	for( int x = m_iCurrentX - 2; x < m_iCurrentX + 2; x++ )
	{
		for( int y = m_iCurrentY - 2; y < m_iCurrentY + 2; y++ )
		{
			pkTile = GetTileByIndex(x, y);
			if( pkTile )
			{
				pkTile->Save(m_kMapName);
			}
		}
	}

	return true;
}

//保存服务器端的地图
bool CMap::SaveServerMap()
{
	char acFilename[NI_MAX_PATH];
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Envir\\Maps\\%s\\%s.smp", TerrainHelper::GetServerPath(), (const char*)m_kMapName, (const char*)m_kMapName);
	NiFile* pkFile = NiFile::GetFile(acFilename, NiFile::WRITE_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return false;
	}

	//标识, 版本号
	int iSign = SERVER_MAP_SIGN;
	int iVersion = SERVER_MAP_VERSION;
	pkFile->Write(&iSign, sizeof(int));
	pkFile->Write(&iVersion, sizeof(int));

	//TileFlags
	CTagStream kTagStream;

	kTagStream.Reset();
	kTagStream.SetTag('MAIN');

	kTagStream.Write(m_iTileFlags, sizeof(m_iTileFlags));

	kTagStream.WriteToStream(pkFile);

	NiDelete pkFile;

	return true;
}

CTile* CMap::GetTile(float x, float y)
{
	return GetTileByIndex(TILE_INDEX(x), TILE_INDEX(y));
}

CTile* CMap::GetTileByIndex(int x, int y)
{
	//越界或者不存在
	if( GetTileFlag(x, y) == 0 )
		return NULL;

	return m_pkTiles[x][y];
}

CTile* CMap::GetCurrentTile()
{
	return GetTileByIndex(m_iCurrentX, m_iCurrentY);
}

CChunk* CMap::GetChunk(float x, float y)
{
	CTile* tile = GetTile(x, y);
	if( tile )
		return tile->GetChunk(x, y);

	return NULL;
}

bool CMap::EnterTile(float x, float y)
{
	int X = TILE_INDEX(x);
	int Y = TILE_INDEX(y);

	if( m_iCurrentX == X 
		&& m_iCurrentY == Y )
		return false;

	m_iCurrentX = X;
	m_iCurrentY = Y;

	//先Load当前的
	LoadTile(m_iCurrentX    , m_iCurrentY    );

	//旁边中间的
	LoadTile(m_iCurrentX    , m_iCurrentY + 1);
	LoadTile(m_iCurrentX    , m_iCurrentY - 1);
	LoadTile(m_iCurrentX + 1, m_iCurrentY    );
	LoadTile(m_iCurrentX - 1, m_iCurrentY    );

	//四角的
	LoadTile(m_iCurrentX + 1, m_iCurrentY + 1);
	LoadTile(m_iCurrentX - 1, m_iCurrentY + 1);
	LoadTile(m_iCurrentX + 1, m_iCurrentY - 1);
	LoadTile(m_iCurrentX - 1, m_iCurrentY - 1);

	UpdateWorldBound();

	UpdateScene();

	return true;
}

//找一个最合适的缓存位置
int CMap::GetBestCache(int x, int y)
{
	int iBestIdx = -1;
	int iBest = 0;

	for(int i = 0; i < CACHE_SIZE; i++)
	{
		if( !m_pkCacheTiles[i] )
		{
			return i;
		}


		if( NiAbs( float(m_pkCacheTiles[i]->GetX() - m_iCurrentX) ) + NiAbs( float(m_pkCacheTiles[i]->GetY() - m_iCurrentY) ) > (float)iBest )
		{
			iBest = (int)NiAbs( float(m_pkCacheTiles[i]->GetX() - m_iCurrentX) ) + (int)NiAbs( float(m_pkCacheTiles[i]->GetY() - m_iCurrentY) );
			iBestIdx = i;
		}
	}

	NIASSERT(iBestIdx >= 0 && iBestIdx < CACHE_SIZE);

	if( m_pkCacheTiles[iBestIdx] )
	{
		m_pkCacheTiles[iBestIdx]->Save(m_kMapName);

		m_pkSceneRoot->DetachChild(m_pkCacheTiles[iBestIdx]);

		m_pkTiles[m_pkCacheTiles[iBestIdx]->GetX()][m_pkCacheTiles[iBestIdx]->GetY()] = NULL;
		m_pkCacheTiles[iBestIdx]->DecRefCount();
		m_pkCacheTiles[iBestIdx] = NULL;
	}

	return iBestIdx;
}

//装载中区
void CMap::LoadTile(int x, int y)
{
	//越界或者不存在Local
	if( GetTileFlag(x, y) == 0 )
		return;

	//已经存在
	if( m_pkTiles[x][y] )
		return;

	for( int i = 0; i < CACHE_SIZE; i++ )
	{
		if( m_pkCacheTiles[i]
		&&	m_pkCacheTiles[i]->GetX() == x
			&& m_pkCacheTiles[i]->GetY() == y)   //已经在缓存里面了
		{
			m_pkTiles[x][y] = m_pkCacheTiles[i];
			return;
		}
	}

	int iBestIdx = GetBestCache(x, y);
	m_pkCacheTiles[iBestIdx] = m_pkTiles[x][y] = NiNew CTile(x, y);
	m_pkTiles[x][y]->IncRefCount();
	m_pkTiles[x][y]->Load(m_kMapName);
}

//创建中区
CTile* CMap::CreateTile(int x, int y)
{
	if( GetTileFlag(x, y) != 0 )
		return m_pkTiles[x][y];

	int iBestIdx = GetBestCache(x, y);
	m_pkCacheTiles[iBestIdx] = m_pkTiles[x][y] = NiNew CTile(x, y);
	m_pkTiles[x][y]->OnCreate();
	m_pkTiles[x][y]->IncRefCount();

	SetTileFlag(x, y, 1);
	return m_pkTiles[x][y];
}

//删除中区
bool CMap::DeleteTile(int x, int y)
{
	if( GetTileFlag(x, y) == 0 )
		return false;

	//已经装载的, 先保存, 后删除
	for( int i = 0; i < CACHE_SIZE; i++ )
	{
		if( m_pkCacheTiles[i]
		&&	m_pkCacheTiles[i]->GetX() == x
			&& m_pkCacheTiles[i]->GetY() == y)
		{
			m_pkCacheTiles[i]->Save(m_kMapName);
			m_pkSceneRoot->DetachChild(m_pkCacheTiles[i]);
			NiDelete m_pkCacheTiles[i];
			m_pkCacheTiles[i] = m_pkTiles[x][y] = NULL;
			break;
		}
	}

	SetTileFlag(x, y, 0);

	//更新场景
	UpdateScene();

	return true;
}

bool CMap::HasTile() const
{
	for( int x = 0; x < TILE_COUNT; x++ )
	{
		for( int y = 0; y < TILE_COUNT; y++ )
		{
			if( GetTileFlag(x, y) )
				return true;
		}
	}

	return false;
}

//更新Scene里面的Entities
void CMap::UpdateScene()
{
	//创建一个新的Scene
	m_spScene = NiNew NiScene("Main Scene");
	m_spScene->AddEntity(this);

	CTile* tile;
	for( int x = m_iCurrentX - 2; x < m_iCurrentX + 2; x++ )
	{
		for( int y = m_iCurrentY - 2; y < m_iCurrentY + 2; y++ )
		{
			tile = GetTileByIndex(x, y);
			if( tile )
			{
				tile->GetEntities(m_spScene);
				tile->UpdateEditorInfo();
			}
		}
	}

	CTerrainLightManager::Get()->GetEntities(m_spScene);

    m_kPathNodeManager.GetEntities(m_spScene);

	CTextureManager::Get()->UnloadAllUnused();
}

void CMap::HideCollision(NiAVObject* pkObject)
{
	if(NiIsKindOf(NiGeometry, pkObject))
	{
		const char* pcName = pkObject->GetName();
		if( NiStrnicmp(pcName, "Col", 3) == 0 )
			pkObject->SetAppCulled(true);

		return;
	}

	if(NiIsKindOf(NiNode, pkObject))
	{
		NiAVObject* pkChild;
		for (unsigned int i = 0; i < ((NiNode*)pkObject)->GetArrayCount(); i++)
		{
			pkChild = ((NiNode*)pkObject)->GetAt(i);
			if (pkChild)
				HideCollision(pkChild);
		}
	}
}

void CMap::OnEntityAdded(NiEntityInterface* pkEntity)
{
	if( TerrainHelper::IsNpcEntity(pkEntity) )
	{
		OnNpcEntityAdded(pkEntity);
	}
	else if( TerrainHelper::IsWayPointEntity(pkEntity) )
	{
		OnWayPointAdded(pkEntity);
	}
    else if( TerrainHelper::IsGameObject(pkEntity) )
    {
        OnGameObjectAdded(pkEntity);
    }
    else if( TerrainHelper::IsModelEntity(pkEntity) )
	{
		OnModelEntityAdded(pkEntity);
	}
    else if( TerrainHelper::IsPathNodeEntity(pkEntity) )
    {
        m_kPathNodeManager.OnPathNodeAdded(pkEntity);
    }
    else if( TerrainHelper::IsSoundEmitter(pkEntity) )
    {
        OnSoundEmitterAdded(pkEntity);
    }
}

void CMap::OnEntityRemoved(NiEntityInterface* pkEntity)
{
	if( TerrainHelper::IsNpcEntity(pkEntity) )
	{
		OnNpcEntityRemoved(pkEntity);
	}
	else if( TerrainHelper::IsWayPointEntity(pkEntity) )
	{
		OnWayPointRemoved(pkEntity);
	}
    else if( TerrainHelper::IsGameObject(pkEntity) )
    {
        OnGameObjectRemoved(pkEntity);
    }
    else if( TerrainHelper::IsModelEntity(pkEntity) )
	{
		RemoveFromQuadTree(pkEntity);
	}
    else if( TerrainHelper::IsPathNodeEntity(pkEntity) )
    {
        m_kPathNodeManager.OnPathNodeRemoved(pkEntity);
    }
    else if( TerrainHelper::IsSoundEmitter(pkEntity) )
    {
        OnSoundEmitterRemoved(pkEntity);
    }
}

void CMap::OnEntityPropertyChanged(NiEntityInterface* pkEntity)
{
	if( TerrainHelper::IsNpcEntity(pkEntity) )
	{
		OnNpcEntityPropertyChanged(pkEntity);
	}
	else if( TerrainHelper::IsWayPointEntity(pkEntity) )
	{
		OnWayPointPropertyChanged(pkEntity);
	}
    else if( TerrainHelper::IsGameObject(pkEntity) )
    {
        OnGameObjectPropertyChanged(pkEntity);
    }
    else if( TerrainHelper::IsSoundEmitter(pkEntity) )
    {
        OnSoundEmitterChanged(pkEntity);
    }
    else if( TerrainHelper::IsModelEntity(pkEntity) )
	{
		UpdateQuadTree(pkEntity);//更新Entity所属的Quad Tree
	}
}

void CMap::OnModelEntityAdded(NiEntityInterface* pkEntity)
{
	CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
	if( !pkEditorInfoComponent )
	{
		pkEditorInfoComponent = NiNew CEditorInfoComponent;
		pkEntity->AddComponent(pkEditorInfoComponent);
	}
	else
	{
		//当恢复一个已经删除了的Entity 的时候可能存在这种情况
		pkEditorInfoComponent->Reset();
	}

    CEntityInfoComponent* pkEntityInfoComponent = (CEntityInfoComponent *)pkEntity->GetComponentByTemplateID(CEntityInfoComponent::ms_kTemplateID);
    if(!pkEntityInfoComponent)
    {
        pkEntityInfoComponent = NiNew CEntityInfoComponent;
        pkEntity->AddComponent(pkEntityInfoComponent);
    }

	UpdateQuadTree(pkEntity);

    //Attach Fog Property
    TerrainHelper::AttachFogProperty(pkEntity);
}

void CMap::UpdateQuadTree(NiEntityInterface* pkEntity)
{
	CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
	if( !pkEditorInfoComponent )
		return;

    pkEntity->Update(NULL, ms_fTime, m_spErrorHandler, ms_pkAssetManager);

	NiObject* pkObject = NULL;
	if( !pkEntity->GetPropertyData(ms_kSceneRootPointerName, pkObject) )
		return;

	if( pkObject == NULL )
		return;

	NiAVObject* pkAVObject = (NiAVObject*)pkObject;
	pkAVObject->Update(0, false);

	NiPoint2 kData;
	CChunk* pkChunk;
	const NiPoint3& kPos = pkAVObject->GetWorldTranslate();

	CTile* pkDestTile = GetTile(kPos.x, kPos.y);
	if( pkDestTile == NULL )
		return;

	CTile* pkSrcTile = NULL;
	if( pkEntity->GetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData) )
		pkSrcTile = GetTileByIndex(int(kData.x), int(kData.y));

    bool bCountSet;
	unsigned int uiEntityIndex = INVALID_INDEX;
	if( pkSrcTile != NULL )
	{
		uiEntityIndex = pkSrcTile->GetEntityIndex(pkEntity);
        if(uiEntityIndex == INVALID_INDEX)
        {
            pkSrcTile->AddEntity(pkEntity);
            uiEntityIndex = pkSrcTile->GetEntityIndex(pkEntity);
        }

		unsigned int uiCount = 0;
		pkEditorInfoComponent->GetElementCount(CEditorInfoComponent::ms_kQuadTreeChunksName, uiCount);
		for( unsigned int ui = 0; ui < uiCount; ++ui )
		{
			pkEditorInfoComponent->GetPropertyData(CEditorInfoComponent::ms_kQuadTreeChunksName, kData, ui);
			pkChunk = GetChunk(kData.x, kData.y);
			if( pkChunk )
				pkChunk->RemoveEntity(pkSrcTile->GetX(), pkSrcTile->GetY(), uiEntityIndex);
		}

		pkSrcTile->SetDirty(true);
		pkEditorInfoComponent->SetElementCount(CEditorInfoComponent::ms_kQuadTreeChunksName, 0, bCountSet);
	}

	if( pkDestTile != pkSrcTile )
	{
		//add to dest tile
		pkDestTile->AddEntity(pkEntity);
		uiEntityIndex = pkDestTile->GetEntityIndex(pkEntity);
		kData.x = (float)pkDestTile->GetX();
		kData.y = (float)pkDestTile->GetY();
		pkEditorInfoComponent->SetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData, 0);

		//delete from src tile
		if( pkSrcTile )
			pkSrcTile->RemoveEntity(pkEntity);
	}

	//分配四叉树
    unsigned int uiCount = 0;
    if( uiEntityIndex != INVALID_INDEX )
    {
        NiBound kBound = pkAVObject->GetWorldBound();
        int XStart = ALIGN(kBound.GetCenter().x - kBound.GetRadius(), CHUNK_SIZE);
        int XEnd   = ALIGN(kBound.GetCenter().x + kBound.GetRadius(), CHUNK_SIZE);

        int YStart =  ALIGN(kBound.GetCenter().y - kBound.GetRadius(), CHUNK_SIZE);
        int YEnd   =  ALIGN(kBound.GetCenter().y + kBound.GetRadius(), CHUNK_SIZE);

        for( int X = XStart; X <= XEnd; X += CHUNK_SIZE )
        {
            for( int Y = YStart; Y <= YEnd; Y += CHUNK_SIZE )
            {
                pkChunk = GetChunk(float(X), float(Y));
                if( pkChunk )
                {
                    pkChunk->AddEntity(pkDestTile->GetX(), pkDestTile->GetY(), uiEntityIndex);
                    kData.x = float(X);
                    kData.y = float(Y);
                    pkEditorInfoComponent->SetPropertyData(CEditorInfoComponent::ms_kQuadTreeChunksName, kData, uiCount++);
                }
            }
        }
    }

	pkEditorInfoComponent->SetElementCount(CEditorInfoComponent::ms_kQuadTreeChunksName, uiCount, bCountSet);
}

void CMap::RemoveFromQuadTree(NiEntityInterface* pkEntity)
{
	CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
	if( pkEditorInfoComponent == NULL )
		return;

	NiPoint2 kData;
	CTile* pkSrcTile = NULL;
	if( pkEntity->GetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData) )
	{
		pkSrcTile = GetTileByIndex(int(kData.x), int(kData.y));
	}

	if( pkSrcTile != NULL )
	{
		//delete from src tile
		pkSrcTile->RemoveEntity(pkEntity);
	}
}

void CMap::OnNpcEntityAdded(NiEntityInterface* pkEntity)
{
	CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
	if( !pkEditorInfoComponent )
	{
		pkEditorInfoComponent = NiNew CEditorInfoComponent;
		pkEntity->AddComponent(pkEditorInfoComponent);
	}
	else
	{
		//当恢复一个已经删除了的Entity 的时候可能存在这种情况
		pkEditorInfoComponent->Reset();
	}

	OnNpcEntityPropertyChanged(pkEntity);
}

void CMap::OnNpcEntityPropertyChanged(NiEntityInterface* pkEntity)
{
	CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
	if( !pkEditorInfoComponent )
		return;

	NiPoint3 kPos;
	if( !pkEntity->GetPropertyData(ms_kTranslationName, kPos) )
		return;

	CTile* pkDestTile = GetTile(kPos.x, kPos.y);
	if( pkDestTile == NULL )
		return;

	CTile* pkSrcTile = NULL;
	NiPoint2 kData;
	if( pkEntity->GetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData) )
		pkSrcTile = GetTileByIndex(int(kData.x), int(kData.y));

	if( pkDestTile != pkSrcTile )
	{
		//add to dest tile
		pkDestTile->AddNpcEntity(pkEntity);
		kData.x = (float)pkDestTile->GetX();
		kData.y = (float)pkDestTile->GetY();
		pkEditorInfoComponent->SetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData, 0);

		//delete from src tile
		if( pkSrcTile )
			pkSrcTile->RemoveNpcEntity(pkEntity);
	}

	pkDestTile->SetDirty(true);
}

void CMap::OnNpcEntityRemoved(NiEntityInterface* pkEntity)
{
	CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
	if( pkEditorInfoComponent == NULL )
		return;

	NiPoint2 kData;
	CTile* pkSrcTile = NULL;
	if( pkEntity->GetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData) )
	{
		pkSrcTile = GetTileByIndex(int(kData.x), int(kData.y));
	}

	if( pkSrcTile != NULL )
	{
		//delete from src tile
		pkSrcTile->RemoveNpcEntity(pkEntity);
	}
}

void CMap::OnWayPointAdded(NiEntityInterface* pkEntity)
{
	//重新分配给NPC,　避免删除后再撤销时产生的问题
	CWayPointComponent* pkWayPointComponent = (CWayPointComponent*)pkEntity->GetComponentByTemplateID(CWayPointComponent::ms_kTemplateID);
	CNpcComponent* pkNpcCompoent = (CNpcComponent*)pkWayPointComponent->GetNpcEntity()->GetComponentByTemplateID(CNpcComponent::ms_kTemplateID);
	pkNpcCompoent->SetWayPoint(pkWayPointComponent->GetIndex(), pkEntity);

	NiPoint3 kPos;
	if( !pkEntity->GetPropertyData(ms_kTranslationName, kPos) )
		return;

	CTile* pkTile = GetTile(kPos.x, kPos.y);
	if(pkTile)
		pkTile->SetDirty(true);
}

void CMap::OnWayPointRemoved(NiEntityInterface* pkEntity)
{
	NiPoint3 kPos;
	if( !pkEntity->GetPropertyData(ms_kTranslationName, kPos) )
		return;

	CTile* pkTile = GetTile(kPos.x, kPos.y);
	if(pkTile)
		pkTile->SetDirty(true);

	CWayPointComponent* pkWayPointComponent = (CWayPointComponent*)pkEntity->GetComponentByTemplateID(CWayPointComponent::ms_kTemplateID);
	CNpcComponent* pkNpcComponent = (CNpcComponent*)pkWayPointComponent->GetNpcEntity()->GetComponentByTemplateID(CNpcComponent::ms_kTemplateID);
	pkNpcComponent->RemoveWayPoint(pkEntity);
}

void CMap::OnWayPointPropertyChanged(NiEntityInterface* pkEntity)
{
	NiPoint3 kPos;
	if( !pkEntity->GetPropertyData(ms_kTranslationName, kPos) )
		return;

	CTile* pkTile = GetTile(kPos.x, kPos.y);
	if(pkTile)
		pkTile->SetDirty(true);
	
	//pkEntity->Update(NULL, 0.f, NULL, NULL);
}

void CMap::OnGameObjectAdded(NiEntityInterface* pkEntity)
{
    CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
    if( !pkEditorInfoComponent )
    {
        pkEditorInfoComponent = NiNew CEditorInfoComponent;
        pkEntity->AddComponent(pkEditorInfoComponent);
    }
    else
    {
        //当恢复一个已经删除了的Entity 的时候可能存在这种情况
        pkEditorInfoComponent->Reset();
    }

    OnGameObjectPropertyChanged(pkEntity);
}

void CMap::OnGameObjectRemoved(NiEntityInterface* pkEntity)
{
    CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
    if( pkEditorInfoComponent == NULL )
        return;

    NiPoint2 kData;
    CTile* pkSrcTile = NULL;
    if( pkEntity->GetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData) )
    {
        pkSrcTile = GetTileByIndex(int(kData.x), int(kData.y));
    }

    if( pkSrcTile != NULL )
    {
        //delete from src tile
        pkSrcTile->RemoveGameObject(pkEntity);
    }
}

void CMap::OnGameObjectPropertyChanged(NiEntityInterface* pkEntity)
{
    CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
    if( !pkEditorInfoComponent )
        return;

    NiPoint3 kPos;
    if( !pkEntity->GetPropertyData(ms_kTranslationName, kPos) )
        return;

    CTile* pkDestTile = GetTile(kPos.x, kPos.y);
    if( pkDestTile == NULL )
        return;

    CTile* pkSrcTile = NULL;
    NiPoint2 kData;
    if( pkEntity->GetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData) )
        pkSrcTile = GetTileByIndex(int(kData.x), int(kData.y));

    if( pkDestTile != pkSrcTile )
    {
        //add to dest tile
        pkDestTile->AddGameObject(pkEntity);
        kData.x = (float)pkDestTile->GetX();
        kData.y = (float)pkDestTile->GetY();
        pkEditorInfoComponent->SetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData, 0);

        //delete from src tile
        if( pkSrcTile )
            pkSrcTile->RemoveGameObject(pkEntity);
    }

    pkDestTile->SetDirty(true);
}

void CMap::OnSoundEmitterAdded(NiEntityInterface* pkEntity)
{
    CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
    if( !pkEditorInfoComponent )
    {
        pkEditorInfoComponent = NiNew CEditorInfoComponent;
        pkEntity->AddComponent(pkEditorInfoComponent);
    }
    else
    {
        //当恢复一个已经删除了的Entity 的时候可能存在这种情况
        pkEditorInfoComponent->Reset();
    }

    OnSoundEmitterChanged(pkEntity);
}

void CMap::OnSoundEmitterRemoved(NiEntityInterface* pkEntity)
{
    CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
    if( pkEditorInfoComponent == NULL )
        return;

    NiPoint2 kData;
    CChunk* pkSrcChunk = NULL;
    if( pkEntity->GetPropertyData(CEditorInfoComponent::ms_kChunkPropertyName, kData) )
    {
        pkSrcChunk = GetChunk(kData.x, kData.y);
    }

    if( pkSrcChunk != NULL )
    {
        //delete from src chunk
        pkSrcChunk->RemoveSoundEmitter(pkEntity);
    }
}

void CMap::OnSoundEmitterChanged(NiEntityInterface* pkEntity)
{
    CEditorInfoComponent* pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
    if( !pkEditorInfoComponent )
        return;

    NiPoint3 kPos;
    if( !pkEntity->GetPropertyData(ms_kTranslationName, kPos) )
        return;

    CChunk* pkDestChunk = GetChunk(kPos.x, kPos.y);
    if( pkDestChunk == NULL )
        return;

    CChunk* pkSrcChunk = NULL;
    NiPoint2 kData;
    if( pkEntity->GetPropertyData(CEditorInfoComponent::ms_kChunkPropertyName, kData) )
        pkSrcChunk = GetChunk(kData.x, kData.y);

    if( pkDestChunk != pkSrcChunk )
    {
        //add to dest chunk
        pkDestChunk->AddSoundEmitter(pkEntity);
        kData.x = (float)pkDestChunk->GetBoundBox().m_kMin.x;
        kData.y = (float)pkDestChunk->GetBoundBox().m_kMin.y;
        pkEditorInfoComponent->SetPropertyData(CEditorInfoComponent::ms_kChunkPropertyName, kData, 0);

        //delete from src chunk
        if( pkSrcChunk )
            pkSrcChunk->RemoveSoundEmitter(pkEntity);
    }

    pkDestChunk->GetTile()->SetDirty(true);
}


NiEntityInterface* CMap::GetEntity(int iTileX, int iTileY, unsigned int uiIndex)
{
	CTile* pkTile = GetTileByIndex(iTileX, iTileY);
	if( pkTile )
		return pkTile->GetEntity(uiIndex);

	return NULL;
}

void CMap::UpdateWorldBound()
{
	NiPoint3 kCenter;
	kCenter.x = float(m_iCurrentX * TILE_SIZE + TILE_SIZE / 2);
	kCenter.y = float(m_iCurrentY * TILE_SIZE + TILE_SIZE / 2);
	kCenter.z = GetHeight(kCenter.x, kCenter.y);

	NiBound	kBound;
	kBound.SetCenter(kCenter);
	kBound.SetRadius(TILE_SIZE*1.5f*1.5f);

	m_pkSceneRoot->SetWorldBound(kBound);
}

void CMap::GetBound(NiBound& kBound)
{
	NiPoint3 kCenter;
	kCenter.x = float(m_iCurrentX * TILE_SIZE + TILE_SIZE / 2);
	kCenter.y = float(m_iCurrentY * TILE_SIZE + TILE_SIZE / 2);
	kCenter.z = GetHeight(kCenter.x, kCenter.y);

	kBound.SetCenter(kCenter);
	kBound.SetRadius(TILE_SIZE*1.5f*1.5f);
}

bool CMap::IsDirty() const
{
	if( m_bDirty )
		return true;

	for( int i = 0; i < CACHE_SIZE; i++ )
	{
		if( m_pkCacheTiles[i] )   //已经在缓存里面了
		{
			if( m_pkCacheTiles[i]->IsDirty() )
				return true;
		}
	}

    if(m_kPathNodeManager.IsDirty())
        return true;

	return false;
}

NiBool CMap::GetPropertyData(const NiFixedString& kPropertyName, NiObject*& pkData, unsigned int uiIndex) const
{
	if( kPropertyName == ms_kSceneRootPointerName )
	{
		pkData = m_pkSceneRoot;
		return true;
	}

	return NiGeneralEntity::GetPropertyData( kPropertyName, pkData, uiIndex );
}

NiAVObject* CMap::GetSceneRootPointer(unsigned int uiIndex)
{
	return m_pkSceneRoot;
}

NiFixedString CMap::GetClassName() const
{
	return ms_kClassName;
}

NiFixedString CMap::GetName() const
{
	return ms_kName;
}

void CMap::ChangeSky(const char* pcSkyFilename)
{
	if(m_kSky.Load(pcSkyFilename))
		m_bDirty = true;
}

void CMap::Update(NiEntityPropertyInterface* pkParentEntity, float fTime)
{
    ms_fTime = fTime;
	NiGeneralEntity::Update(NULL, fTime, NULL, NULL);
	m_pkTerrainLightManager->UpdateLightEntities(NULL, fTime);

	m_kSky.Update(fTime);
}

void CMap::BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext)
{
    m_spErrorHandler->ClearErrors();

    m_uiFrameID++;

    const NiCamera* pkCamera = pkRenderingContext->m_pkCamera;
    const NiPoint3& kPos = pkCamera->GetWorldLocation();
    const NiPoint3& kDir = pkCamera->GetWorldDirection();
    const NiPoint3& kRight = pkCamera->GetWorldRightVector();
    const NiPoint3& kUp = pkCamera->GetWorldUpVector();
    const NiFrustum& kFrustum = pkCamera->GetViewFrustum();
    float fNearFarRatio = kFrustum.m_fFar / kFrustum.m_fNear;

	//更新光照
	m_pkTerrainLightManager->Update(kPos);

	m_pkLight->SetAmbientColor(m_pkTerrainLightManager->GetAmbientColor());
	m_pkLight->SetDiffuseColor(m_pkTerrainLightManager->GetDiffuseColor());

	//water shader
	UpdateWaterShaders();

	CSpeedTreeShader::UpdateCamera(pkRenderingContext->m_pkCamera);

	m_pkSceneRoot->UpdateEffects();
    CCullingProcess& kCuller = *(CCullingProcess*)pkRenderingContext->m_pkCullingProcess;
	kCuller.Reset(pkRenderingContext->m_pkCamera);

    CBox kBox;
    kBox.Reset();
    if(kFrustum.m_bOrtho)
    {
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fBottom);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fBottom);

        kBox.Intersect(kPos + kDir*kFrustum.m_fNear*fNearFarRatio + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear*fNearFarRatio + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fBottom);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear*fNearFarRatio + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear*fNearFarRatio + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fBottom);
    }
    else
    {
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fBottom);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fBottom);

        kBox.Intersect(kPos + (kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fTop) * fNearFarRatio);
        kBox.Intersect(kPos + (kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fBottom) * fNearFarRatio);
        kBox.Intersect(kPos + (kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fTop) * fNearFarRatio);
        kBox.Intersect(kPos + (kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fBottom) * fNearFarRatio);
    }

    NiTPrimitiveArray<CChunk*> kVisibleChunks(256, 32);

    //
	//地形和水面
    CChunk* pkChunk;
    CBox kChunkBox;
	for( float x = kBox.m_kMin.x; x < kBox.m_kMax.x + CHUNK_SIZE; x += CHUNK_SIZE )
	{
		for( float y = kBox.m_kMin.y; y < kBox.m_kMax.y + CHUNK_SIZE; y += CHUNK_SIZE )		
        {
			pkChunk = GetChunk(x, y);
			if(pkChunk)
            {
                kChunkBox = pkChunk->GetBoundBox();
                kChunkBox.m_kMax.z += 100.f; //假设建筑物最高是100米
                if(kBox.IsOverlapped(kChunkBox) && kChunkBox.TestVisible(kCuller.GetFrustumPlanes()))
                    kVisibleChunks.Add(pkChunk);
            }
		}
	}

    //按照离摄像机的距离排序
    for( unsigned int i = 0; i < kVisibleChunks.GetSize(); i++ )
    {
        for( unsigned int j = i + 1; j < kVisibleChunks.GetSize(); j++ )
        {
            if( (kVisibleChunks.GetAt(j)->GetBoundBox().GetCenter() - kPos).SqrLength()
                < (kVisibleChunks.GetAt(i)->GetBoundBox().GetCenter() - kPos).SqrLength() )
            {
                pkChunk = kVisibleChunks.GetAt(i);
                kVisibleChunks.SetAt(i, kVisibleChunks.GetAt(j));
                kVisibleChunks.SetAt(j, pkChunk);
            }
        }
    }

    for( unsigned int i = 0; i < kVisibleChunks.GetSize(); ++i )
	{
		kVisibleChunks.GetAt(i)->BuildVisibleSet(kCuller);
	}

	//npc 和 way point
    if( CTile::IsEditNpc() && (ms_uiVisibleMask & NiGeneralEntity::Npc))
	{
		CTile* pkTile;
        for( float x = kBox.m_kMin.x; x < kBox.m_kMax.x + TILE_SIZE; x += TILE_SIZE )
	    {
		    for( float y = kBox.m_kMin.y; y < kBox.m_kMax.y + TILE_SIZE; y += TILE_SIZE )
			{
				pkTile = GetTile(x, y);
				if( pkTile )
                {
					pkTile->BuildNpcVisibleSet(pkRenderingContext);
                    pkTile->BuildGameObjectVisibleSet(pkRenderingContext);
                }
			}
		}
	}

    //path node
    if( CTile::IsEditNpc() && (ms_uiVisibleMask & NiGeneralEntity::PathNode) )
        m_kPathNodeManager.BuildVisibleSet(pkRenderingContext, kBox);

	//阴影
	CPlayer* pkPlayer = CPlayer::Get();
	if(pkPlayer && !pkPlayer->IsFreeCameraMode())
	{
		pkPlayer->DrawShadow(kCuller);
		pkPlayer->DrawWaterEffects(kCuller);
	}
}

void CMap::BuildReflectVisibleSet(NiEntityRenderingContext* pkRenderingContext)
{
    m_uiFrameID++;

    CCullingProcess& kCuller = *(CCullingProcess*)pkRenderingContext->m_pkCullingProcess;
    NiCamera* pkCamera = kCuller.GetReflectCamera();
    const NiPoint3& kPos = pkCamera->GetWorldLocation();
    const NiPoint3& kDir = pkCamera->GetWorldDirection();
    const NiPoint3& kRight = pkCamera->GetWorldRightVector();
    const NiPoint3& kUp = pkCamera->GetWorldUpVector();
    const NiFrustum& kFrustum = pkCamera->GetViewFrustum();
    float fNearFarRatio = (kFrustum.m_fFar < 500.f ? kFrustum.m_fFar : 500.f) / kFrustum.m_fNear;

    CBox kBox;
    kBox.Reset();
    if(kFrustum.m_bOrtho)
    {
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fBottom);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fBottom);

        kBox.Intersect(kPos + kDir*kFrustum.m_fNear*fNearFarRatio + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear*fNearFarRatio + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fBottom);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear*fNearFarRatio + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear*fNearFarRatio + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fBottom);
    }
    else
    {
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fBottom);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fTop);
        kBox.Intersect(kPos + kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fBottom);

        kBox.Intersect(kPos + (kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fTop) * fNearFarRatio);
        kBox.Intersect(kPos + (kDir*kFrustum.m_fNear + kRight*kFrustum.m_fRight + kUp*kFrustum.m_fBottom) * fNearFarRatio);
        kBox.Intersect(kPos + (kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fTop) * fNearFarRatio);
        kBox.Intersect(kPos + (kDir*kFrustum.m_fNear + kRight*kFrustum.m_fLeft + kUp*kFrustum.m_fBottom) * fNearFarRatio);
    }

    //
    //地形和水面
    CChunk* pkChunk;
    for( float x = kBox.m_kMin.x; x < kBox.m_kMax.x + CHUNK_SIZE; x += CHUNK_SIZE )
    {
        for( float y = kBox.m_kMin.y; y < kBox.m_kMax.y + CHUNK_SIZE; y += CHUNK_SIZE )
        {
            pkChunk = GetChunk(x, y);
            if(pkChunk)
                pkChunk->BuildReflectVisibleSet(kCuller);
        }
    }
}

void CMap::_SDMInit()
{
	NiSrand( (unsigned int)time( NULL ) );

	ms_kClassName = MAP_CALSS_NAME;
	ms_kName = MAP_NAME;
	ms_kSceneRootPointerName = "Scene Root Pointer";
	ms_kNifFilePathName = "NIF File Path";
	ms_kSPTFilePathName = "SPT File Path";
	ms_kTranslationName = "Translation";

	NiImageConverter::SetImageConverter(NiNew NiDevImageConverter);
	CTextureManager::_SDMInit();

	CChunk::_SDMInit();
	CTile::_SDMInit();
	CTerrainLightComponent::_SDMInit();
	CSPTComponent::_SDMInit();
	CEditorInfoComponent::_SDMInit();
	CExternalAssetSPTHandler::_SDMInit();
	CTerrainShader::_SDMInit();

	CWater::_SDMInit();
	CWaterShader::_SDMShutdown();

	CSpeedTreeShader::_SDMInit();

	ms_DBManager = NiNew CDBManager();
	ms_DBManager->LoadDB();
	CNpcComponent::_SDMInit();
	CWayPointComponent::_SDMInit();
    CGameObjectComponent::_SDMInit();
    CEntityInfoComponent::_SDMInit();
    CPathNodeComponent::_SDMInit();
	CShadowGeometry::_SDMInit();
	CWaterEffect::_SDMInit();
    CSoundEmitterComponent::_SDMInit();
    CCollisionShader::_SDMInit();
}

void CMap::_SDMShutdown()
{
	if( ms_pkMap )
	{
		//CMap里面包含Scene
		//CMap又当成一个Scene的一个Node
		//造成循环引用, 必须手工解除
		NiScene* kScene =  ms_pkMap->GetScene();
		kScene->RemoveAllEntities();
		ms_pkMap->DecRefCount();
	}

    CCollisionShader::_SDMShutdown();
    CSoundEmitterComponent::_SDMShutdown();
	CWaterEffect::_SDMShutdown();
	CShadowGeometry::_SDMShutdown();
	CSpeedTreeShader::_SDMShutdown();
	CWaterShader::_SDMShutdown();
	CWater::_SDMShutdown();

	CTerrainShader::_SDMShutdown();
	CExternalAssetSPTHandler::_SDMShutdown();
	CSPTComponent::_SDMShutdown();
	CEditorInfoComponent::_SDMShutdown();
	CTerrainLightComponent::_SDMShutdown();
	CTile::_SDMShutdown();
	CChunk::_SDMShutdown();

    CPathNodeComponent::_SDMShutdown();
    CGameObjectComponent::_SDMShutdown();
	CNpcComponent::_SDMShutdown();
	CWayPointComponent::_SDMShutdown();
    CEntityInfoComponent::_SDMShutdown();

	CTextureManager::_SDMShutdown();
	NiImageConverter::SetImageConverter(NiNew NiImageConverter);

	ms_kClassName = NULL;
	ms_kName = NULL;
	ms_kTranslationName = NULL;
	ms_kSPTFilePathName = NULL;
	ms_kNifFilePathName = NULL;
	ms_kSceneRootPointerName = NULL;

	ms_DBManager->UnloadDB();
	NiDelete ms_DBManager;
}

CMap* CMap::Get()
{
	return ms_pkMap;
}

void CMap::SetAssetManager(NiExternalAssetManager* pkAssetManager) { ms_pkAssetManager = pkAssetManager; } 
NiExternalAssetManager* CMap::GetAssetManager() { return ms_pkAssetManager; }
