#ifndef MAP_H
#define MAP_H

#include "TerrainLibType.h"
#include "Chunk.h"
#include "Tile.h"
#include "TerrainLight.h"
#include "Sky.h"
#include "DBFile/DBManager.h"
#include "WaterShader.h"
#include "Water.h"
#include "PathNode.h"
#include "AlphaAccumulator.h"
#include "GrassMesh.h"
#include "VMapCover.h"

#define CACHE_SIZE (16)

#define MAP_CALSS_NAME "CMap"
#define MAP_NAME "Map"

#define SERVER_MAP_SIGN 'SMAP'
#define SERVER_MAP_VERSION 1

class CTile;
class CChunk;
struct FIBITMAP;

class TERRAIN_ENTRY CMap : public NiGeneralEntity
{
public:
    // construction
    CMap();
    virtual ~CMap();
	void Reset();

	int			GetTileFlag(int x, int y) const;
	void		SetTileFlag(int x, int y, int flag);

	const int*	GetTileFlags() const;

	CTile*		GetTile(float x, float y);
	CTile*		GetTileByIndex(int x, int y);
	CTile*		GetCurrentTile();
	CChunk*		GetChunk(float x, float y);

	float		GetHeight(float x, float y);
	bool		GetHeight(float x, float y, float& h);
	bool		GetHeightAndNormal(float x, float y, float&h, NiPoint3& kNormal);
	void		SetHeight(float x, float y, float h);
	void		UpdateNormal(float x, float y);

	float		GetWaterHeight(float x, float y);
	bool		GetWaterHeight(float x, float y, float& h);
	void		SetWaterHeight(float x, float y, float h);
	void		SetWaterFlag(float x, float y, bool bHasWater, unsigned int uiWaterIndex);
	bool		GetWaterFlag(float x, float y);
	bool		WaterAboveTerrain(float x, float y, float h);

	CWaterShader* GetWaterShader(unsigned int uiIndex);
	bool		ChangeWaterShader(const NiFixedString& kTexturePath, unsigned int uiWaterIndex);
	bool		DeleteWaterShader(unsigned int uiWaterIndex);

	void		BuidWaterEdgeMap(); //创建水的深度图
	void		SetWaterAlpha(float fAlpha, unsigned int uiWaterIndex);
	void		SetWaterAlphaBlend(bool bAlphaBlend, unsigned int uiWaterIndex);
	void		SetWaterAdditiveLight(bool bAdditive, unsigned int uiWaterIndex);
	void		SetWaterRR(bool bRR, unsigned int uiWaterIndex);
	void		SetWaterUVScale(float fUVScale, unsigned int uiWaterIndex);
	void		SetWaterSpeed(float fSpeed, unsigned int uiWaterIndex);
	void		OnWaterChanged(unsigned int uiWaterIndex);
	void        UpdateWaterShaders();
    void        DrawWater(NiEntityRenderingContext* pkRenderingContext);

	//VMapCover
	void		StartVMapCover();

	bool		TurnEdge(int x, int y, bool bTurnEdge); //把Edge处于Turn状态
	bool		IsTurnEdge(int x, int y); //是否处于翻转状态
	bool		ExcavateHole(int x, int y, bool bHole); //挖洞
	bool		IsHole(int x, int y);

	void		PaintMaterial(int x, int y, FIBITMAP* pkAlphaMap, const char* pcTextureName, float fScale);
	void		RemoveMaterial(int x, int y, const char* pcTextureName);

	void		SetMoveable(float x, float y, bool bMoveable, int nLv);
    bool        GetMoveable(float x, float y);
	void		CalcMoveable();
	void		ClearMoveable();

	void		SetAreaId(float x, float y, unsigned int uiAreaId);
	unsigned int GetAreaId(float x, float y);

	void		ChangeTexture(const char* pcSrcTextureName, const char* pcDestTextureName);
	void		GetCurrentTextureNames(NiTObjectArray<NiFixedString>& akTextureNames);

	//在本地图内传送
	bool		EnterTile(float x, float y);

	//从一个其他的地图传送过来
	void		Transport(const NiFixedString& kMapName);

	const		NiFixedString& GetMapName() const { return m_kMapName; }
	void		SetMapName(NiFixedString& kMapName) { m_kMapName = kMapName; }

	bool		Load();
	void		Unload();

	bool		Save();
	bool		SaveMap();
	bool		SaveTiles();
	bool		SaveServerMap();

	//找一个最合适的缓存位置
	int			GetBestCache(int x, int y);

	void		LoadTile(int x, int y);
	bool		HasTile() const;

	CTile*		CreateTile(int x, int y);
	bool		DeleteTile(int x, int y);

	void		UpdateScene();
	void		HideCollision(NiAVObject* pkObject);
	
	NiEntityInterface* GetEntity(int iTileX, int iTileY, unsigned int uiIndex);

	void		OnEntityAdded(NiEntityInterface* pkEntity);
	void		OnEntityRemoved(NiEntityInterface* pkEntity);
	void		OnEntityPropertyChanged(NiEntityInterface* pkEntity);

	void		OnModelEntityAdded(NiEntityInterface* pkEntity);
	void		UpdateQuadTree(NiEntityInterface* pkEntity);
	void		RemoveFromQuadTree(NiEntityInterface* pkEntity);

	void		OnNpcEntityAdded(NiEntityInterface* pkEntity);
	void		OnNpcEntityRemoved(NiEntityInterface* pkEntity);
	void		OnNpcEntityPropertyChanged(NiEntityInterface* pkEntity);

	void		OnWayPointAdded(NiEntityInterface* pkEntity);
	void		OnWayPointRemoved(NiEntityInterface* pkEntity);
	void		OnWayPointPropertyChanged(NiEntityInterface* pkEntity);

    void        OnGameObjectAdded(NiEntityInterface* pkEntity);
    void        OnGameObjectRemoved(NiEntityInterface* pkEntity);
    void		OnGameObjectPropertyChanged(NiEntityInterface* pkEntity);

    void		OnSoundEmitterAdded(NiEntityInterface* pkEntity);
    void		OnSoundEmitterRemoved(NiEntityInterface* pkEntity);
    void		OnSoundEmitterChanged(NiEntityInterface* pkEntity);

	void		UpdateWorldBound();
	void		GetBound(NiBound& kBound);

	bool		CollideLine(const NiPoint3 &start, const NiPoint3 &end, float& fTime);
	bool		CollideBox(const NiPoint3 &start, const NiPoint3 &end, const NiPoint3& extent, Result& result);
	bool		MultiCollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList);
	bool		MoveObject(NiPoint3& start, const NiPoint3& delta, const NiPoint3& extent, Result& result);
	void		CalcShadow(const NiPoint3 &start, const NiPoint3 &end, float& fShadow);

	bool		IsDirty() const;
	void		SetDirty(bool bDirty) { m_bDirty = bDirty; }
	
	unsigned int GetFrameCount() const { return m_uiFrameID; }
	void		 IncFrameCount() { m_uiFrameID++; }

	unsigned int GetCollisionCount() const { return m_uiCollisionCount; }
	void	     IncCollisionCount() { m_uiCollisionCount++; }

	NiScene*	GetScene() { return m_spScene; }
	NiLight*	GetLight() { return m_pkLight; }

	virtual NiFixedString GetClassName() const;
	virtual NiFixedString GetName() const;

	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiObject*& pkData, unsigned int uiIndex = 0) const;
	virtual NiAVObject* GetSceneRootPointer(unsigned int uiIndex);

    CSky* GetSky() { return &m_kSky; }
	void ChangeSky(const char* pcSkyFilename);
	void DrawSky(NiCamera* pkCamera, bool bReflectPass);

    void Draw(NiEntityRenderingContext* pkRenderingContext);
    void DrawAlpha(NiEntityRenderingContext* pkRenderingContext);
    void DrawGrass(NiEntityRenderingContext* pkRenderingContext);
    virtual void BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors) {}
	virtual void Update(NiEntityPropertyInterface* pkParentEntity, float fTime);

    void BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext);
    void BuildReflectVisibleSet(NiEntityRenderingContext* pkRenderingContext);
    void RenderReflectTexture(NiEntityRenderingContext* pkRenderingContext);
    void RenderRefractTexture(NiEntityRenderingContext* pkRenderingContext);

	void Precache();

    NiEntityErrorInterface* GetErrorHandler() { return m_spErrorHandler; }

    CGrassMesh& GetGrassMesh() { return m_kGrassMesh; }

    static unsigned int GetVisibleMask() { return ms_uiVisibleMask; }
    static void SetVisibleMask(unsigned int uiVisibleMask) { ms_uiVisibleMask = uiVisibleMask; }

    static unsigned int GetSelectMask() { return ms_uiSelectMask; }
    static void SetSelectMask(unsigned int uiSelectMask) { ms_uiSelectMask = uiSelectMask; }

    static void _SDMInit();
    static void _SDMShutdown();
	static CMap* Get();

	static void SetAssetManager(NiExternalAssetManager* pkAssetManager);
	static NiExternalAssetManager* GetAssetManager();
    static float GetTime() { return ms_fTime; }

protected:
	NiFixedString m_kMapName;//地图名称

	VMapCover*	m_pkVMapCover;

	//当前的中区索引
	int		m_iCurrentX;
	int		m_iCurrentY;

	int		m_iTileFlags[TILE_COUNT][TILE_COUNT];
    CTile*  m_pkTiles[TILE_COUNT][TILE_COUNT];
	CTile*  m_pkCacheTiles[CACHE_SIZE];

	NiNodePtr m_pkSceneRoot;
	CTerrainLightManager* m_pkTerrainLightManager;
	NiDirectionalLight* m_pkLight;
	NiScenePtr m_spScene;

	CSky m_kSky;
    CPathNodeManager m_kPathNodeManager;

	//是否需要保存
	bool m_bDirty;

	unsigned int m_uiFrameID;
	unsigned int m_uiCollisionCount;

	CWaterShader* m_pkWaterShaders[MAX_WATER_COUNT];

    NiDefaultErrorHandlerPtr m_spErrorHandler;
    CAlphaAccumulatorPtr m_spAccumulator;

    CGrassMesh m_kGrassMesh;

    static unsigned int ms_uiVisibleMask;
    static unsigned int ms_uiSelectMask;

	static CMap* ms_pkMap;
	static NiExternalAssetManager* ms_pkAssetManager;
	static CDBManager* ms_DBManager;
    static float ms_fTime; //当前的时间

public:
	static NiFixedString ms_kClassName;
	static NiFixedString ms_kName;
	static NiFixedString ms_kSceneRootPointerName;
	static NiFixedString ms_kNifFilePathName;
	static NiFixedString ms_kSPTFilePathName;
	static NiFixedString ms_kTranslationName;
};

#endif