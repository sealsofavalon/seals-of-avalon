
#include "TerrainPCH.h"
#include "Map.h"

bool CMap::CollideLine(const NiPoint3 &start, const NiPoint3 &end, float& fTime)
{
	IncCollisionCount();

	float deltaX = end.x - start.x;
	float deltaY = end.y - start.y;
	int XDir = deltaX < 0.0f ? -1:1;
	int YDir = deltaY < 0.0f ? -1:1;

	int XStart, XEnd, YStart, YEnd;

	int XMin = (m_iCurrentX - 2) * TILE_SIZE;
	int XMax = (m_iCurrentX + 2) * TILE_SIZE;

	int YMin = (m_iCurrentY - 2) * TILE_SIZE;
	int YMax = (m_iCurrentY + 2) * TILE_SIZE;

	XStart = XDir > 0 ? ALIGN(start.x,             CHUNK_SIZE) : ALIGN(start.x + CHUNK_SIZE, CHUNK_SIZE);
	XEnd   = XDir > 0 ? ALIGN(end.x + CHUNK_SIZE,  CHUNK_SIZE) : ALIGN(end.x,                CHUNK_SIZE);

	XStart = XDir > 0 ? NiMax(XStart, XMin) : NiMin(XStart, XMax);
	XEnd   = XDir > 0 ? NiMin(XEnd,   XMax) : NiMax(XEnd,   XMin);

	if( XStart*XDir >= XEnd*XDir )
		return false;

	int x, y;
	CChunk *chunk;
	if(XEnd == XStart + XDir*CHUNK_SIZE)//此时DeltaX可能为0
	{
		YStart = YDir > 0 ? ALIGN(start.y,            CHUNK_SIZE) : ALIGN(start.y + CHUNK_SIZE, CHUNK_SIZE);
		YEnd   = YDir > 0 ? ALIGN(end.y + CHUNK_SIZE, CHUNK_SIZE) : ALIGN(end.y,                CHUNK_SIZE);

		YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
		YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);


		for( y = YStart; y*YDir < YEnd*YDir; y += CHUNK_SIZE * YDir)
		{
			chunk = GetChunk(float(XStart + XDir), float(y + YDir));
			if( chunk && chunk->CollideLine(start, end, fTime) )
			{
				return true;
			}
		}

		return false;
	}

	//Y = mX + c;
	float m = deltaY / deltaX;
	float c = end.y - end.x*m;
	float y1, y2;
	for( x = XStart; x*XDir < XEnd*XDir; x += CHUNK_SIZE*XDir )
	{
		y1 = m*x + c;
		y2 = y1 + CHUNK_SIZE*XDir*m;

		YStart = YDir > 0 ? ALIGN(y1,              CHUNK_SIZE) : ALIGN(y1 + CHUNK_SIZE, CHUNK_SIZE);
		YEnd   = YDir > 0 ? ALIGN(y2 + CHUNK_SIZE, CHUNK_SIZE) : ALIGN(y2,              CHUNK_SIZE);

		YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
		YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);


		for( y = YStart; y*YDir < YEnd*YDir; y += CHUNK_SIZE*YDir )
		{
			chunk = GetChunk(float(x + XDir), float(y + YDir));
			if( chunk && chunk->CollideLine(start, end, fTime) )
			{
				return true;
			}
		}
	}

	return false;
}


bool CMap::CollideBox(const NiPoint3 &start, const NiPoint3 &end, const NiPoint3& extent, Result& result)
{
	IncCollisionCount();

	float deltaX = end.x - start.x;
	float deltaY = end.y - start.y;
	int XDir = deltaX < 0.0f ? -1:1;
	int YDir = deltaY < 0.0f ? -1:1;

	int XStart, XEnd, YStart, YEnd;

	int XMin = (m_iCurrentX - 2) * TILE_SIZE;
	int XMax = (m_iCurrentX + 2) * TILE_SIZE;

	int YMin = (m_iCurrentY - 2) * TILE_SIZE;
	int YMax = (m_iCurrentY + 2) * TILE_SIZE;

	XStart = XDir > 0 ? ALIGN(start.x - extent.x,              CHUNK_SIZE) : ALIGN(start.x + extent.x + CHUNK_SIZE, CHUNK_SIZE);
	XEnd   = XDir > 0 ? ALIGN(end.x   + extent.x + CHUNK_SIZE, CHUNK_SIZE) : ALIGN(end.x   - extent.x,              CHUNK_SIZE);

	XStart = XDir > 0 ? NiMax(XStart, XMin) : NiMin(XStart, XMax);
	XEnd   = XDir > 0 ? NiMin(XEnd,   XMax) : NiMax(XEnd,   XMin);

	if( XStart*XDir >= XEnd*XDir )
		return false;

	int x, y;
	CChunk *chunk;
	if(NiAbs(deltaX) < 0.01f || XEnd == XStart + XDir*CHUNK_SIZE)//此时DeltaX可能为0
	{
		YStart = YDir > 0 ? ALIGN(start.y - extent.y,              CHUNK_SIZE) : ALIGN(start.y + extent.y + CHUNK_SIZE, CHUNK_SIZE);
		YEnd   = YDir > 0 ? ALIGN(end.y   + extent.y + CHUNK_SIZE, CHUNK_SIZE) : ALIGN(end.y   - extent.y,              CHUNK_SIZE);

		YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
		YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);
		for( x = XStart; x*XDir < XEnd*XDir; x += CHUNK_SIZE * XDir )
		{
			for( y = YStart; y*YDir < YEnd*YDir; y += CHUNK_SIZE * YDir)
			{
				chunk = GetChunk(float(x + XDir), float(y + YDir));
				if( chunk && chunk->CollideBox(start, end, extent, result) )
				{
					//info->Normal.normalizeSafe();
					return true;
				}
			}
		}

		return false;
	}

	//Y = mX + c;
	float m = deltaY / deltaX;
	float c = end.y - end.x*m;
	float y1, y2;
	for( x = XStart; x*XDir < XEnd*XDir; x += CHUNK_SIZE*XDir )
	{
		y1 = m*x + c;
		y2 = y1 + CHUNK_SIZE*XDir*m;
		
		YStart = YDir > 0 ? ALIGN(y1 - extent.y,              CHUNK_SIZE) : ALIGN(y1 + extent.y + CHUNK_SIZE, CHUNK_SIZE);
		YEnd   = YDir > 0 ? ALIGN(y2 + extent.y + CHUNK_SIZE, CHUNK_SIZE) : ALIGN(y2 - extent.y,              CHUNK_SIZE);

		YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
		YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);

		for( y = YStart; y*YDir < YEnd*YDir; y += CHUNK_SIZE*YDir )
		{
			chunk = GetChunk(float(x + XDir), float(y + YDir));
			if( chunk && chunk->CollideBox(start, end, extent, result) )
			{
				//info->Normal.normalizeSafe();
				return true;
			}
		}
   }

   return false;
}

//返回所有碰撞的结果
bool CMap::MultiCollideBox(const NiPoint3& start, const NiPoint3& end, const NiPoint3& extent, ResultList& resultList)
{
	IncCollisionCount();

	float deltaX = end.x - start.x;
	float deltaY = end.y - start.y;
	int XDir = deltaX < 0.0f ? -1:1;
	int YDir = deltaY < 0.0f ? -1:1;

	int XStart, XEnd, YStart, YEnd;

	int XMin = (m_iCurrentX - 2) * TILE_SIZE;
	int XMax = (m_iCurrentX + 2) * TILE_SIZE;

	int YMin = (m_iCurrentY - 2) * TILE_SIZE;
	int YMax = (m_iCurrentY + 2) * TILE_SIZE;

	XStart = XDir > 0 ? ALIGN(start.x - extent.x,              CHUNK_SIZE) : ALIGN(start.x + extent.x + CHUNK_SIZE, CHUNK_SIZE);
	XEnd   = XDir > 0 ? ALIGN(end.x   + extent.x + CHUNK_SIZE, CHUNK_SIZE) : ALIGN(end.x   - extent.x,              CHUNK_SIZE);

	XStart = XDir > 0 ? NiMax(XStart, XMin) : NiMin(XStart, XMax);
	XEnd   = XDir > 0 ? NiMin(XEnd,   XMax) : NiMax(XEnd,   XMin);

	if( XStart*XDir >= XEnd*XDir )
		return false;

	int x, y;
	CChunk *chunk;
	if(NiAbs(deltaX) < 0.01f || XEnd == XStart + XDir*CHUNK_SIZE)//此时DeltaX可能为0
	{
		YStart = YDir > 0 ? ALIGN(start.y - extent.y,              CHUNK_SIZE) : ALIGN(start.y + extent.y + CHUNK_SIZE, CHUNK_SIZE);
		YEnd   = YDir > 0 ? ALIGN(end.y   + extent.y + CHUNK_SIZE, CHUNK_SIZE) : ALIGN(end.y   - extent.y,              CHUNK_SIZE);

		YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
		YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);
		for( x = XStart; x*XDir < XEnd*XDir; x += CHUNK_SIZE * XDir )
		{
			for( y = YStart; y*YDir < YEnd*YDir; y += CHUNK_SIZE * YDir)
			{
				chunk = GetChunk(float(x + XDir), float(y + YDir));
				if( chunk )
					chunk->MultiCollideBox(start, end, extent, resultList);
			}
		}
	}
	else
	{
		//Y = mX + c;
		float m = deltaY / deltaX;
		float c = end.y - end.x*m;
		float y1, y2;
		for( x = XStart; x*XDir < XEnd*XDir; x += CHUNK_SIZE*XDir )
		{
			y1 = m*x + c;
			y2 = y1 + CHUNK_SIZE*XDir*m;

			YStart = YDir > 0 ? ALIGN(y1 - extent.y,              CHUNK_SIZE) : ALIGN(y1 + extent.y + CHUNK_SIZE, CHUNK_SIZE);
			YEnd   = YDir > 0 ? ALIGN(y2 + extent.y + CHUNK_SIZE, CHUNK_SIZE) : ALIGN(y2 - extent.y,              CHUNK_SIZE);

			YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
			YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);

			for( y = YStart; y*YDir < YEnd*YDir; y += CHUNK_SIZE*YDir )
			{
				chunk = GetChunk(float(x + XDir), float(y + YDir));
				if( chunk )
					chunk->MultiCollideBox(start, end, extent, resultList);
			}
		}
	}

	if( resultList.size() > 0 )
	{
		resultList.Sort(false);
		return true;
	}

	return false;
}

//Returns 1 if some movement occured, 0 if no movement occured.
bool CMap::MoveObject(NiPoint3& start, const NiPoint3& delta, const NiPoint3& extent, Result& result)
{
	if( delta.SqrLength() == 0.f )
	{
		result.fTime = 0.f;
		result.kNormal = NiPoint3(0.f, 0.f, 0.f);
		return false;
	}

	NiPoint3 kDelta = delta;
	kDelta.Unitize();
	NiPoint3 kStart = start + kDelta*0.01f;

	ResultList resultList;
	if( MultiCollideBox(kStart, kStart + delta, extent, resultList) )
	{
		result = resultList[0];
	}
	else
	{
		result.fTime = 1.0f;
		result.kNormal = NiPoint3(0.f, 0.f, 0.f);
	}

	start += delta * result.fTime;
	return result.fTime > 0.f;
}

void CMap::CalcShadow(const NiPoint3 &start, const NiPoint3 &end, float& fShadow)
{
	IncCollisionCount();

	float deltaX = end.x - start.x;
	float deltaY = end.y - start.y;
	int XDir = deltaX < 0.0f ? -1:1;
	int YDir = deltaY < 0.0f ? -1:1;

	int XStart, XEnd, YStart, YEnd;

	int XMin = (m_iCurrentX - 2) * TILE_SIZE;
	int XMax = (m_iCurrentX + 2) * TILE_SIZE;

	int YMin = (m_iCurrentY - 2) * TILE_SIZE;
	int YMax = (m_iCurrentY + 2) * TILE_SIZE;

	XStart = XDir > 0 ? ALIGN(start.x,             CHUNK_SIZE) : ALIGN(start.x + CHUNK_SIZE, CHUNK_SIZE);
	XEnd   = XDir > 0 ? ALIGN(end.x + CHUNK_SIZE,  CHUNK_SIZE) : ALIGN(end.x,                CHUNK_SIZE);

	XStart = XDir > 0 ? NiMax(XStart, XMin) : NiMin(XStart, XMax);
	XEnd   = XDir > 0 ? NiMin(XEnd,   XMax) : NiMax(XEnd,   XMin);

	if( XStart*XDir >= XEnd*XDir )
		return;

	int x, y;
	CChunk *chunk;
	if(XEnd == XStart + XDir*CHUNK_SIZE)//此时DeltaX可能为0
	{
		YStart = YDir > 0 ? ALIGN(start.y,            CHUNK_SIZE) : ALIGN(start.y + CHUNK_SIZE, CHUNK_SIZE);
		YEnd   = YDir > 0 ? ALIGN(end.y + CHUNK_SIZE, CHUNK_SIZE) : ALIGN(end.y,                CHUNK_SIZE);

		YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
		YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);


		for( y = YStart; y*YDir < YEnd*YDir; y += CHUNK_SIZE * YDir)
		{
			chunk = GetChunk(float(XStart + XDir), float(y + YDir));
			if(chunk)
			{
				chunk->CalcShadow(start, end, fShadow);
				if(fShadow >= 1.f)
					return;
			}
		}

		return;
	}

	//Y = mX + c;
	float m = deltaY / deltaX;
	float c = end.y - end.x*m;
	float y1, y2;
	for( x = XStart; x*XDir < XEnd*XDir; x += CHUNK_SIZE*XDir )
	{
		y1 = m*x + c;
		y2 = y1 + CHUNK_SIZE*XDir*m;

		YStart = YDir > 0 ? ALIGN(y1,              CHUNK_SIZE) : ALIGN(y1 + CHUNK_SIZE, CHUNK_SIZE);
		YEnd   = YDir > 0 ? ALIGN(y2 + CHUNK_SIZE, CHUNK_SIZE) : ALIGN(y2,              CHUNK_SIZE);

		YStart = YDir > 0 ? NiMax(YStart, YMin) : NiMin(YStart, YMax);
		YEnd   = YDir > 0 ? NiMin(YEnd,   YMax) : NiMax(YEnd,   YMin);


		for( y = YStart; y*YDir < YEnd*YDir; y += CHUNK_SIZE*YDir )
		{
			chunk = GetChunk(float(x + XDir), float(y + YDir));
			if(chunk)
			{
				chunk->CalcShadow(start, end, fShadow);
				if(fShadow >= 1.f)
					return;
			}
		}
	}
}