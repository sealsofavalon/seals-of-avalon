#include "TerrainPCH.h"
#include "Map.h"
#include "TerrainShader.h"
#include "CollisionShader.h"
#include "TerrainHelper.h"

//准备反射贴图
void CMap::RenderReflectTexture(NiEntityRenderingContext* pkRenderingContext)
{
    CCullingProcess& kCuller = *(CCullingProcess*)pkRenderingContext->m_pkCullingProcess;
    if(!kCuller.HasRRWater())
        return;

    NiD3DRenderer* pkRenderer = (NiD3DRenderer*)pkRenderingContext->m_pkRenderer;

    // Backup current render target group.
    const NiRenderTargetGroup* pkCurRenderTarget = pkRenderer->GetCurrentRenderTargetGroup();
    NIASSERT(pkCurRenderTarget != NULL);
    pkRenderer->EndUsingRenderTargetGroup();

    //-------------------------------------------------------------------------------------------------------------------------
    pkRenderer->BeginUsingRenderTargetGroup(CWater::GetReflectTarget(), NiRenderer::CLEAR_ALL);
    pkRenderer->SetCameraData(kCuller.GetReflectCamera());
    
    //天空体
    DrawSky(kCuller.GetReflectCamera(), true);

    pkRenderer->GetD3DDevice()->SetRenderState(D3DRS_CLIPPLANEENABLE, D3DCLIPPLANE0);
    
    ////Todo 使用Oblique Frustum Clipping 把水面以下的东西Clip掉
    //D3DXMATRIX kView = pkRenderer->GetD3DView();
    //D3DXMATRIX kProj = pkRenderer->GetD3DProj();
    //D3DXMATRIX kViewProj = kView*kProj;
    //D3DXMatrixTranspose(&kViewProj, &kViewProj);
    //D3DXMatrixInverse(&kViewProj, NULL, &kViewProj);
    //D3DXPLANE kPlane = D3DXPLANE(0.f, 0.f, 1.f, kCuller.GetReflectCamera()->GetWorldLocation().z - kCuller.GetRRWaterHeight() + 0.5f);
    //D3DXPlaneTransform(&kPlane, &kPlane, &kViewProj);
    //pkRenderer->GetD3DDevice()->SetClipPlane(0, &kPlane.a);

    //地形
    NiDrawVisibleArray(kCuller.GetReflectCamera(), *kCuller.GetReflectChunkSet());

	float fCameraH = kCuller.GetReflectCamera()->GetWorldLocation().z;
	float fRRWaterH = kCuller.GetRRWaterHeight();

	float d = fCameraH - fRRWaterH;
    ////注意摄像机在原点
    //kPlane = D3DXPLANE(0.f, 0.f, 1.f, d);
    //D3DXPlaneTransform(&kPlane, &kPlane, &kViewProj);
    //pkRenderer->GetD3DDevice()->SetClipPlane(0, &kPlane.a);

    NiDrawVisibleArray(kCuller.GetReflectCamera(), *kCuller.GetReflectModelSet());

    pkRenderer->EndUsingRenderTargetGroup();
    //test
    //static int i = 0;
    //if( i++ > 200 )
    //{
    //    i = 0;
    //    NiDX92DBufferData* pkBuffData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)CWater::GetReflectTarget()->GetBufferRendererData(0));
    //    D3DXSaveSurfaceToFileA("E:\\1.jpg", D3DXIFF_JPG, pkBuffData->GetSurface(), NULL, NULL);
    //}

    pkRenderer->GetD3DDevice()->SetRenderState(D3DRS_CLIPPLANEENABLE, 0);
    pkRenderer->BeginUsingRenderTargetGroup((NiRenderTargetGroup *)pkCurRenderTarget, NiRenderer::CLEAR_NONE);

    // Set up the renderer's camera data.
    pkRenderer->SetCameraData(kCuller.GetCamera());
}

//
void CMap::RenderRefractTexture(NiEntityRenderingContext* pkRenderingContext)
{
    CCullingProcess& kCuller = *(CCullingProcess*)pkRenderingContext->m_pkCullingProcess;
    if(!kCuller.HasRRWater())
        return;

    NiD3DRenderer* pkRenderer = (NiD3DRenderer*)pkRenderingContext->m_pkRenderer;
	NiD3DRenderState* pkRenderState = pkRenderer->GetRenderState();

    NiRenderedTexture* pkBackTexture = (NiRenderedTexture*)CWater::GetBackTexture();
    NiRenderedTexture* pkRefractTexture = (NiRenderedTexture*)CWater::GetRefractTexture();
    NIASSERT(pkBackTexture && pkRefractTexture);

    CWaterShader::SetRefractPass(true);
    pkRenderState->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_ALPHA);
    pkRenderState->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	pkRenderState->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	pkRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
    
    NiDrawVisibleArray(pkRenderingContext->m_pkCamera, *kCuller.GetWaterSet());
    
    pkRenderState->SetRenderState(D3DRS_COLORWRITEENABLE, 0x0000000F);
    pkRenderState->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
    CWaterShader::SetRefractPass(false);

    pkRenderer->FastCopy(pkBackTexture->GetBuffer(), pkRefractTexture->GetBuffer());
}

void CMap::DrawWater(NiEntityRenderingContext* pkRenderingContext)
{
    CCullingProcess& kCuller = *(CCullingProcess*)pkRenderingContext->m_pkCullingProcess;

    //反射模型和地面
    kCuller.BuildReflectVisibleSet(pkRenderingContext);
    kCuller.SortWater();

    if(kCuller.GetWaterSet()->GetCount() == 0)
        return;

	if(pkRenderingContext->m_pkRenderer->GetCurrentRenderTargetGroup() == NULL)
		return;

    RenderReflectTexture(pkRenderingContext);


    RenderRefractTexture(pkRenderingContext);

    NiD3DRenderState* pkD3DRenderState = ((NiD3DRenderer*)pkRenderingContext->m_pkRenderer)->GetRenderState();

    pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_ADDRESSU, D3DTADDRESS_WRAP);
    pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_ADDRESSV, D3DTADDRESS_WRAP);
    pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_MAGFILTER, D3DTEXF_LINEAR);
    pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_MINFILTER, D3DTEXF_LINEAR);
    pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_MIPFILTER, D3DTEXF_LINEAR);

    pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_ADDRESSU, D3DTADDRESS_WRAP);
    pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_ADDRESSV, D3DTADDRESS_WRAP);
    pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_MAGFILTER, D3DTEXF_LINEAR);
    pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_MINFILTER, D3DTEXF_LINEAR);
    pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_MIPFILTER, D3DTEXF_LINEAR);

    pkD3DRenderState->SetSamplerState(2, NiD3DRenderState::NISAMP_ADDRESSU, D3DTADDRESS_WRAP);
    pkD3DRenderState->SetSamplerState(2, NiD3DRenderState::NISAMP_ADDRESSV, D3DTADDRESS_WRAP);
    pkD3DRenderState->SetSamplerState(2, NiD3DRenderState::NISAMP_MAGFILTER, D3DTEXF_LINEAR);
    pkD3DRenderState->SetSamplerState(2, NiD3DRenderState::NISAMP_MINFILTER, D3DTEXF_LINEAR);
    pkD3DRenderState->SetSamplerState(2, NiD3DRenderState::NISAMP_MIPFILTER, D3DTEXF_LINEAR);

    pkD3DRenderState->SetSamplerState(3, NiD3DRenderState::NISAMP_ADDRESSU, D3DTADDRESS_WRAP);
    pkD3DRenderState->SetSamplerState(3, NiD3DRenderState::NISAMP_ADDRESSV, D3DTADDRESS_WRAP);
    pkD3DRenderState->SetSamplerState(3, NiD3DRenderState::NISAMP_MAGFILTER, D3DTEXF_LINEAR);
    pkD3DRenderState->SetSamplerState(3, NiD3DRenderState::NISAMP_MINFILTER, D3DTEXF_LINEAR);
    pkD3DRenderState->SetSamplerState(3, NiD3DRenderState::NISAMP_MIPFILTER, D3DTEXF_LINEAR);

	pkD3DRenderState->SetSamplerState(4, NiD3DRenderState::NISAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	pkD3DRenderState->SetSamplerState(4, NiD3DRenderState::NISAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	pkD3DRenderState->SetSamplerState(4, NiD3DRenderState::NISAMP_MAGFILTER, D3DTEXF_LINEAR);
	pkD3DRenderState->SetSamplerState(4, NiD3DRenderState::NISAMP_MINFILTER, D3DTEXF_LINEAR);
	pkD3DRenderState->SetSamplerState(4, NiD3DRenderState::NISAMP_MIPFILTER, D3DTEXF_LINEAR);
    
	pkD3DRenderState->SetRenderState(D3DRS_ZENABLE, TRUE);
    pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

    NiDrawVisibleArray(pkRenderingContext->m_pkCamera, *kCuller.GetWaterSet());

    pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
}

void CMap::StartVMapCover()
{
	if ( m_pkVMapCover )
		m_pkVMapCover->StartCoverVMaps("..\\..\\..\\NewClient\\Data\\World\\TestMaps", "");
}

void CMap::DrawSky(NiCamera* pkCamera, bool bReflectPass)
{
	m_kSky.Draw(pkCamera, bReflectPass);
}

void CMap::Precache()
{
	CTile* pkTile;
	for( int x = m_iCurrentX - 2; x < m_iCurrentX + 2; x++ )
	{
		for( int y = m_iCurrentY - 2; y < m_iCurrentY + 2; y++ )
		{
			pkTile = GetTileByIndex(x, y);
			if( pkTile )
				pkTile->Precache();
		}
	}
}

void CMap::Draw(NiEntityRenderingContext* pkRenderingContext)
{
    BuildVisibleSet(pkRenderingContext);

    NiRenderer* pkRender = pkRenderingContext->m_pkRenderer;
    NiAccumulatorPtr spOldAccumulator = pkRender->GetSorter();
    pkRender->SetSorter(m_spAccumulator);

    CCullingProcess* pkCullingProcess = (CCullingProcess*)pkRenderingContext->m_pkCullingProcess;

    NiDrawVisibleArray(pkRenderingContext->m_pkCamera, *pkCullingProcess->GetVisibleSet());
    pkCullingProcess->GetVisibleSet()->RemoveAll();

    pkRender->SetSorter(spOldAccumulator);

    NiVisibleArray& kCollisionSet = *pkCullingProcess->GetCollisionSet();
    

    //碰撞盒
    //NiDrawVisibleArray(pkRenderingContext->m_pkCamera, kCollisionSet);
    if( CCollisionShader::GetShowCollision() && kCollisionSet.GetCount() )
    {
        pkRender->BeginBatch(CCollisionShader::GetPropertyState(), NULL);

        for (unsigned int i = 0; i < kCollisionSet.GetCount(); i++)
        {
            NiGeometry* pkGeom = &kCollisionSet.GetAt(i);
            if (NiIsKindOf(NiTriStrips, pkGeom))
            {
                pkRender->BatchRenderStrips((NiTriStrips*)pkGeom);
            }
            else if (NiIsKindOf(NiTriShape, pkGeom))
            {
                pkRender->BatchRenderShape((NiTriShape*)pkGeom);
            }
        }

        pkRender->EndBatch();
    }
}

void CMap::DrawAlpha(NiEntityRenderingContext* pkRenderingContext)
{
    m_spAccumulator->RealFinishAccumulating();
}

void CMap::DrawGrass(NiEntityRenderingContext* pkRenderingContext)
{
    CCullingProcess* pkCullingProcess = (CCullingProcess*)pkRenderingContext->m_pkCullingProcess;
    NiPoint3 kCameraPos = pkCullingProcess->GetCamera()->GetWorldLocation();

    m_kGrassMesh.BeginRender(kCameraPos);

    const NiTPrimitiveArray<CChunk*>& kVisibleChunks = pkCullingProcess->GetVisibleChunks();
    for(unsigned int i = 0; i < kVisibleChunks.GetSize(); ++i)
    {
        kVisibleChunks.GetAt(i)->DrawGrass(m_kGrassMesh);
    }
    
    m_kGrassMesh.EndRender();
}