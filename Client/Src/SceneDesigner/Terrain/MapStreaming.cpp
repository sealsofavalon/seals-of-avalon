
#include "TerrainPCH.h"
#include "Map.h"
#include "MapStreaming.h"

NiFixedString CMapStreaming::STREAMING_EXTENSION;
NiFixedString CMapStreaming::STREAMING_DESCRIPTION;

NiFactoryDeclareStreaming(CMapStreaming);

CMapStreaming::CMapStreaming()
{
}

CMapStreaming::~CMapStreaming()
{
}

NiBool CMapStreaming::Load(const char* pcFileName)
{
	char acStandardFilename[NI_MAX_PATH];
    NiStrcpy(acStandardFilename, NI_MAX_PATH, pcFileName);
	NiPath::Standardize(acStandardFilename);
    NiFilename kFileName(acStandardFilename);

	NiFixedString kMapName = kFileName.GetFilename();

	CMap* pkMap = CMap::Get();
	if( pkMap == NULL )
		pkMap = NiNew CMap;

	pkMap->Transport(kMapName);

    InsertScene(pkMap->GetScene());

	return true;
}

NiBool CMapStreaming::Save(const char* pcFileName)
{
	char acStandardFilename[NI_MAX_PATH];
	NiStrcpy(acStandardFilename, NI_MAX_PATH, pcFileName);
	NiPath::Standardize(acStandardFilename);
	NiFilename kFileName(acStandardFilename);

	NiFixedString kMapName = kFileName.GetFilename();

	CMap* pkMap = CMap::Get();
	if( pkMap == NULL )
		return false;

	if( pkMap->GetMapName() == "New Map" )
	{
		pkMap->SetMapName(kMapName);
	}
	
	return pkMap->Save();
}

//---------------------------------------------------------------------------
NiFixedString CMapStreaming::GetFileExtension() const
{
    return STREAMING_EXTENSION;
}

//---------------------------------------------------------------------------
NiFixedString CMapStreaming::GetFileDescription() const
{
    return STREAMING_DESCRIPTION;
}


//---------------------------------------------------------------------------
void CMapStreaming::_SDMInit()
{
    STREAMING_EXTENSION = "MAP";
    STREAMING_DESCRIPTION = "Map files (*.map)|*.map";

	NiFactoryRegisterStreaming(CMapStreaming);
}

//---------------------------------------------------------------------------
void CMapStreaming::_SDMShutdown()
{
    STREAMING_EXTENSION = NULL;
    STREAMING_DESCRIPTION = NULL;
}