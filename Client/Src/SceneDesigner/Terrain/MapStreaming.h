
#ifndef MAP_STREAMING_H
#define MAP_STREAMING_H

#include "TerrainLibType.h"

class TERRAIN_ENTRY CMapStreaming : public NiEntityStreaming
{
public:
    static NiFixedString STREAMING_EXTENSION;
    static NiFixedString STREAMING_DESCRIPTION;

    CMapStreaming();
    virtual ~CMapStreaming();

    // I/O Functions.
    virtual NiBool Load(const char* pcFileName);
    virtual NiBool Save(const char* pcFileName);

    // File Extension of format that can be load/saved.
    // Also used for registering factory so that different files
    // can be read and saved appropriately.
    virtual NiFixedString GetFileExtension() const;
    virtual NiFixedString GetFileDescription() const;

    // *** begin Emergent internal use only ***
    static void _SDMInit();
    static void _SDMShutdown();
    // *** end Emergent internal use only ***
};

#endif //MAP_STREAMING_H