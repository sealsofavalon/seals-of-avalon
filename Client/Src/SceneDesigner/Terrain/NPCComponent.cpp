#include "TerrainPCH.h"

#include "NPCComponent.h"
#include "WayPointComponent.h"
#include "SplinePath.h"
#include "Map.h"

NiFixedString CNpcComponent::ms_kClassName;
NiFixedString CNpcComponent::ms_kComponentName;

NiFixedString CNpcComponent::ms_kNPCName;
NiFixedString CNpcComponent::ms_kEntryIDName;
NiFixedString CNpcComponent::ms_kWayPointsName;
NiFixedString CNpcComponent::ms_kMoveTypeName;
NiFixedString CNpcComponent::ms_kSpawnFlagName;

NiFixedString CNpcComponent::ms_kTranslationName;
NiFixedString CNpcComponent::ms_kRotationName;
NiFixedString CNpcComponent::ms_kScaleName;

NiUniqueID CNpcComponent::ms_kTemplateID;

NiVertexColorProperty* CNpcComponent::ms_pkVertexColorProperty;
NiWireframeProperty* CNpcComponent::ms_pkWireframeProperty;
NiStencilProperty* CNpcComponent::ms_pkStencilProperty;

NiFactoryDeclarePropIntf(CNpcComponent);

//---------------------------------------------------------------------------
void CNpcComponent::_SDMInit()
{
    ms_kClassName = "CNpcComponent";
    ms_kComponentName = "NPC";

    ms_kNPCName = "NPC Name";
    ms_kEntryIDName = "Entry ID";
	ms_kWayPointsName = "路点";
    ms_kMoveTypeName = "Move Type";
    ms_kSpawnFlagName = "Spawn Flag";

    ms_kTranslationName = "Translation";
    ms_kRotationName = "Rotation";
    ms_kScaleName = "Scale";

    ms_kTemplateID = NiUniqueID(0xA2,0x33,0x23,0x23,0x12,0x26,0xA8,0x4A,0xAA,0x79,0x2,0xC4,0x3E,0x7E,0xCD,0xBD);
	NiFactoryRegisterPropIntf(CNpcComponent);

	ms_pkVertexColorProperty = NiNew NiVertexColorProperty;
	ms_pkVertexColorProperty->SetSourceMode(NiVertexColorProperty::SOURCE_EMISSIVE);
	ms_pkVertexColorProperty->SetLightingMode(NiVertexColorProperty::LIGHTING_E_A_D);
	ms_pkVertexColorProperty->IncRefCount();

	ms_pkWireframeProperty = NiNew NiWireframeProperty;
	ms_pkWireframeProperty->SetWireframe(true);
	ms_pkWireframeProperty->IncRefCount();

	ms_pkStencilProperty = NiNew NiStencilProperty;
	ms_pkStencilProperty->SetDrawMode(NiStencilProperty::DRAW_BOTH);
	ms_pkStencilProperty->IncRefCount();
}

//---------------------------------------------------------------------------
void CNpcComponent::_SDMShutdown()
{
    ms_kClassName = NULL;
    ms_kComponentName = NULL;

    ms_kTranslationName = NULL;
    ms_kRotationName = NULL;
    ms_kScaleName = NULL;

    ms_kNPCName = NULL;
    ms_kEntryIDName = NULL;
	ms_kWayPointsName = NULL;
    ms_kMoveTypeName = NULL;
    ms_kSpawnFlagName = NULL;

	ms_pkVertexColorProperty->DecRefCount();
	ms_pkVertexColorProperty = NULL;

	ms_pkWireframeProperty->DecRefCount();
	ms_pkWireframeProperty = NULL;

	ms_pkStencilProperty->DecRefCount();
	ms_pkStencilProperty = NULL;
}

CNpcComponent::CNpcComponent()
    :m_uiEntryID(0)
    ,m_uiSpawnId(0)
    ,m_uiMoveType(0)
    ,m_uiFlag(0)
{
}

CNpcComponent::CNpcComponent(const NiFixedString& kNpcName, unsigned int uiEntryID)
    :m_kNpcName(kNpcName)
    ,m_uiEntryID(uiEntryID)
	,m_uiSpawnId(0)
	,m_kTranslation(0.f, 0.f, 0.f)
    ,m_uiMoveType(0)
    ,m_uiFlag(0)
{
}

unsigned int CNpcComponent::GetWayPointIndex(NiEntityInterface* pkWayPoint)
{
	for(unsigned int ui = 0; ui < m_kWayPoints.GetSize(); ++ui)
	{
		if(m_kWayPoints.GetAt(ui) == pkWayPoint)
			return ui;
	}

	return m_kWayPoints.GetSize();
}

void CNpcComponent::SetWayPoint(unsigned int uiIndex, NiEntityInterface* pkWayPoint)
{
	if(uiIndex >= m_kWayPoints.GetSize())
	{
		m_kWayPoints.SetAtGrow(uiIndex, pkWayPoint);
	}
	else if(pkWayPoint == NULL)
	{
		//删除一个WayPoint
        if(m_kWayPoints.GetSize() >= 1)
        {
            for(unsigned int ui = uiIndex; ui + 1 < m_kWayPoints.GetSize(); ++ui)
                m_kWayPoints.SetAt(ui, m_kWayPoints.GetAt(ui + 1));

            m_kWayPoints.SetAt(m_kWayPoints.GetSize() - 1,  NULL);
        }
	}
	else if(GetWayPointIndex(pkWayPoint) >= GetWayPointCount())
	{
		//如果此WayPoint不存在
		if( m_kWayPoints.GetAt(uiIndex) != NULL )
		{
			//插入一个WayPoint
			for(unsigned int ui = m_kWayPoints.GetSize(); ui > uiIndex; --ui)
				m_kWayPoints.SetAtGrow(ui, m_kWayPoints.GetAt(ui - 1));
		}

		m_kWayPoints.SetAt(uiIndex, pkWayPoint);
	}
	else
	{
		//此WayPoint 已经存在
		return;
	}

	m_kWayPoints.Compact();

	//更新WayPoint 的Index
	CWayPointComponent* pkWayPointComponent;
	for(unsigned int ui = 0; ui < m_kWayPoints.GetSize(); ++ui)
	{
		if(m_kWayPoints.GetAt(ui) == NULL)
			continue;

		pkWayPointComponent = (CWayPointComponent*)m_kWayPoints.GetAt(ui)->GetComponentByTemplateID(CWayPointComponent::ms_kTemplateID);
		pkWayPointComponent->SetIndex(ui);
	}
}

NiEntityInterface* CNpcComponent::GetWayPoint(unsigned int uiIndex)
{
	if(uiIndex < m_kWayPoints.GetSize())
		return m_kWayPoints.GetAt(uiIndex);

	return NULL;
}

void CNpcComponent::RemoveWayPoint(NiEntityInterface* pkWayPoint)
{
	SetWayPoint(GetWayPointIndex(pkWayPoint), NULL);
	BuildPathGeomerty();
}

void CNpcComponent::BuildPathGeomerty()
{
	m_spPathGeometry = NULL;
	if( m_kWayPoints.GetSize() == 0 )
		return;

	NiPoint3 kPosition;
	NiEntityInterface* pkEntity;

	CSplinePath kSplinePath;

	//把NPC的位置加到路点
	kSplinePath.PushBack(NiNew CSplinePath::Knot(m_kTranslation, NiQuaternion(1.f, 0.f, 0.f, 0.f), 1.f, CSplinePath::Knot::POSITION_ONLY, CSplinePath::Knot::LINEAR));

	for(unsigned int ui = 0; ui < m_kWayPoints.GetSize(); ++ui)
	{
		pkEntity = m_kWayPoints.GetAt(ui);
		pkEntity->GetPropertyData(ms_kTranslationName, kPosition, 0);

		kSplinePath.PushBack(NiNew CSplinePath::Knot(kPosition, NiQuaternion(1.f, 0.f, 0.f, 0.f), 1.f, CSplinePath::Knot::POSITION_ONLY, CSplinePath::Knot::LINEAR)); 
	}

	unsigned int uiSize = kSplinePath.GetSize();
	if(uiSize <= 1)
		return;

	kSplinePath.PushBack(NiNew CSplinePath::Knot(*kSplinePath.Front()));
	kSplinePath.PushFront(NiNew CSplinePath::Knot(*kSplinePath.Back()));

	uiSize += 2;
	float fTime = 1.f;
	NiTPrimitiveArray<float> kPointList(70, 35);
	while(fTime < uiSize - 1)
	{
		CSplinePath::Knot k;
		kSplinePath.Value(fTime, &k, true);
		fTime = kSplinePath.AdvanceDist(fTime, 2.0f);

		kPointList.Add(k.m_kPosition.x);
		kPointList.Add(k.m_kPosition.y);
		kPointList.Add(k.m_kPosition.z);
	}

	unsigned short usVertices = (unsigned short)kPointList.GetSize();
	if(usVertices == 0)
		return;
	
	NiPoint3* pkVertex = NiNew NiPoint3[usVertices];
	NiColorA* pkColor = NiNew NiColorA[usVertices];
	unsigned short* pusTriList = NiAlloc(unsigned short, usVertices);

	NiColorA kColor(0.f, 1.f, 0.f, 1.f);
	NiPoint3 kA(-0.1f, -0.3f, 0.3f);
	NiPoint3 kB( 0.0f,  0.3f, 0.3f);
	NiPoint3 kC( 0.1f, -0.3f, 0.3f);

	NiMatrix3 kRotMat;
	NiPoint3 kNextPosition;
	for(unsigned short us = 0; us < usVertices/3; ++us)
	{
		kPosition = NiPoint3(kPointList.GetAt(us*3 + 0), kPointList.GetAt(us*3 + 1), kPointList.GetAt(us*3 + 2));
		if( us + 1 < usVertices/3 )
			kNextPosition = NiPoint3(kPointList.GetAt(us*3 + 3), kPointList.GetAt(us*3 + 4), kPointList.GetAt(us*3 + 5));
		else
			kNextPosition = NiPoint3(kPointList.GetAt(0), kPointList.GetAt(1), kPointList.GetAt(2));

		NiPoint3 kDir = kNextPosition - kPosition;
		float fZ = NiATan2(kDir.y, kDir.x) + NI_PI/2;
		kRotMat.MakeZRotation(-fZ);

		pkVertex[us*3 + 0] = kPosition + kRotMat*kA;
		pkColor[us*3 + 0] = kColor;
		pusTriList[us*3 + 0] = us*3 + 0;

		pkVertex[us*3 + 1] = kPosition + kRotMat*kB;
		pkColor[us*3 + 1] = kColor;
		pusTriList[us*3 + 1] = us*3 + 1;

		pkVertex[us*3 + 2] = kPosition + kRotMat*kC;
		pkColor[us*3 + 2] = kColor;
		pusTriList[us*3 + 2] = us*3 + 2;
	}
	
	m_spPathGeometry = NiNew NiTriShape(usVertices, pkVertex, NULL, pkColor, NULL, 0, NiGeometryData::NBT_METHOD_NONE, usVertices/3, pusTriList);

	m_spPathGeometry->AttachProperty(ms_pkVertexColorProperty);
	m_spPathGeometry->AttachProperty(ms_pkWireframeProperty);
	m_spPathGeometry->AttachProperty(ms_pkStencilProperty);
	m_spPathGeometry->UpdateProperties();
	m_spPathGeometry->Update(0.0f);
}


//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* CNpcComponent::Clone(bool bInheritProperties)
{
    CNpcComponent* pkComponent = NiNew CNpcComponent(m_kNpcName, m_uiEntryID);
    pkComponent->SetMoveType(GetMoveType());
    pkComponent->SetFlag(GetFlag());
    return pkComponent;
}
//---------------------------------------------------------------------------
NiEntityComponentInterface* CNpcComponent::GetMasterComponent()
{
    return NULL;
}
//---------------------------------------------------------------------------
void CNpcComponent::SetMasterComponent(NiEntityComponentInterface* pkMasterComponent)
{
}
//---------------------------------------------------------------------------
void CNpcComponent::GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
    kDependentPropertyNames.Add(ms_kTranslationName);
    kDependentPropertyNames.Add(ms_kRotationName);
    kDependentPropertyNames.Add(ms_kScaleName);
}
//---------------------------------------------------------------------------
// NiEntityPropertyInterface overrides.
//---------------------------------------------------------------------------
NiBool CNpcComponent::SetTemplateID(const NiUniqueID& kID)
{
    //Not supported for custom components
    return false;
}
//---------------------------------------------------------------------------
NiUniqueID  CNpcComponent::GetTemplateID()
{
    return ms_kTemplateID;
}
//---------------------------------------------------------------------------
void CNpcComponent::AddReference()
{
    this->IncRefCount();
}
//---------------------------------------------------------------------------
void CNpcComponent::RemoveReference()
{
    this->DecRefCount();
}
//---------------------------------------------------------------------------
NiFixedString CNpcComponent::GetClassName() const
{
    return ms_kClassName;
}
//---------------------------------------------------------------------------
NiFixedString CNpcComponent::GetName() const
{
    return ms_kComponentName;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::SetName(const NiFixedString& kName)
{
    // This component does not allow its name to be set.
    return false;
}
//---------------------------------------------------------------------------
void CNpcComponent::BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors)
{
	if(m_spPathGeometry)
	{
		pkRenderingContext->m_pkCullingProcess->Process(
			pkRenderingContext->m_pkCamera, 
			m_spPathGeometry,
			pkRenderingContext->m_pkCullingProcess->GetVisibleSet());
	}

    if( (CMap::GetVisibleMask() & NiGeneralEntity::WayPoint) == 0 )
        return;

	for(unsigned int ui = 0; ui < m_kWayPoints.GetSize(); ++ui)
	{
		m_kWayPoints.GetAt(ui)->BuildVisibleSet(pkRenderingContext, pkErrors);
	}
}
//---------------------------------------------------------------------------
void CNpcComponent::Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager)
{
	NiPoint3 kTranslation;
	if( pkParentEntity->GetPropertyData(ms_kTranslationName, kTranslation)
	 && m_kTranslation != kTranslation )
	{
		m_kTranslation = kTranslation;
		BuildPathGeomerty();
	}

	for(unsigned int ui = 0; ui < m_kWayPoints.GetSize(); ++ui)
	{
		m_kWayPoints.GetAt(ui)->Update(NULL, fTime, pkErrors, pkAssetManager);
	}
}
//---------------------------------------------------------------------------
void CNpcComponent::GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const
{
    kPropertyNames.Add(ms_kNPCName);
    kPropertyNames.Add(ms_kEntryIDName);
	kPropertyNames.Add(ms_kMoveTypeName);
    kPropertyNames.Add(ms_kWayPointsName);
    kPropertyNames.Add(ms_kSpawnFlagName);
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const
{
    if( kPropertyName == ms_kNPCName
     || kPropertyName == ms_kEntryIDName
     || kPropertyName == ms_kMoveTypeName
	 || kPropertyName == ms_kWayPointsName
     || kPropertyName == ms_kSpawnFlagName )
    {
        bCanReset = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::ResetProperty(const NiFixedString& kPropertyName)
{
    return false;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique)
{
	if( kPropertyName == ms_kNPCName
	 || kPropertyName == ms_kEntryIDName
     || kPropertyName == ms_kMoveTypeName
	 || kPropertyName == ms_kWayPointsName 
     || kPropertyName == ms_kSpawnFlagName )
	{
		bMadeUnique = false;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const
{
	if( kPropertyName == ms_kNPCName
	 || kPropertyName == ms_kEntryIDName
	 || kPropertyName == ms_kWayPointsName )
	{
        kDisplayName = kPropertyName;
    }
    else if( kPropertyName == ms_kMoveTypeName )
    {
        kDisplayName = "移动类型是否是护送";
    }
    else if( kPropertyName == ms_kSpawnFlagName )
    {
        kDisplayName = "需要直接刷新";
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::SetDisplayName(const NiFixedString& kPropertyName, const NiFixedString& kDisplayName)
{
    // This component does not allow the display name to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const
{
    if(kPropertyName == ms_kNPCName)
    {
        kPrimitiveType = PT_STRING;
    }
    else if(kPropertyName == ms_kEntryIDName)
    {
        kPrimitiveType = PT_UINT;
    }
    else if(kPropertyName == ms_kMoveTypeName)
    {
        kPrimitiveType = PT_BOOL;
    }
    else if(kPropertyName == ms_kSpawnFlagName)
    {
        kPrimitiveType = PT_BOOL;
    }
	else if(kPropertyName == ms_kWayPointsName)
	{
		kPrimitiveType = PT_ENTITYPOINTER;
	}
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::SetPrimitiveType(const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType)
{
    // This component does not allow the primitive type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const
{
    if (kPropertyName == ms_kNPCName)
    {
        kSemanticType = PT_STRING;
    }
    else if (kPropertyName == ms_kEntryIDName)
    {
        kSemanticType = PT_UINT;
    }
    else if(kPropertyName == ms_kMoveTypeName)
    {
        kSemanticType = PT_BOOL;
    }
    else if(kPropertyName == ms_kSpawnFlagName)
    {
        kSemanticType = PT_BOOL;
    }
	else if(kPropertyName == ms_kWayPointsName)
	{
		kSemanticType = PT_ENTITYPOINTER;
	}
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::SetSemanticType(const NiFixedString& kPropertyName, const NiFixedString& kSemanticType)
{
    // This component does not allow the semantic type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const
{
	if( kPropertyName == ms_kNPCName
	 || kPropertyName == ms_kEntryIDName
     || kPropertyName == ms_kMoveTypeName
	 || kPropertyName == ms_kWayPointsName
     || kPropertyName == ms_kSpawnFlagName )
    {
        kDescription = kPropertyName;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::SetDescription(const NiFixedString& kPropertyName, const NiFixedString& kDescription)
{
    // This component does not allow the description to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const
{
	if( kPropertyName == ms_kNPCName
	 || kPropertyName == ms_kEntryIDName
     || kPropertyName == ms_kMoveTypeName
	 || kPropertyName == ms_kWayPointsName
     || kPropertyName == ms_kSpawnFlagName )
    {
        kCategory = ms_kComponentName;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly)
{
	if( kPropertyName == ms_kNPCName
	 || kPropertyName == ms_kEntryIDName
	 || kPropertyName == ms_kWayPointsName )
    {
        bIsReadOnly = true;
        return true;
    }
    else if( kPropertyName == ms_kMoveTypeName
          || kPropertyName == ms_kSpawnFlagName )
    {
        bIsReadOnly = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique)
{
	if( kPropertyName == ms_kNPCName
	 || kPropertyName == ms_kEntryIDName
     || kPropertyName == ms_kMoveTypeName
	 || kPropertyName == ms_kWayPointsName 
     || kPropertyName == ms_kSpawnFlagName)
    {
        bIsUnique = true;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable)
{
	if( kPropertyName == ms_kNPCName
	 || kPropertyName == ms_kEntryIDName )
    {
        bIsSerializable = true;
    }
	else if( kPropertyName == ms_kWayPointsName 
          || kPropertyName == ms_kMoveTypeName //暂时不保存MoveType
          || kPropertyName == ms_kSpawnFlagName) //暂时不保存SpawnFlag
	{
		bIsSerializable = false;
	}
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable)
{
	if( kPropertyName == ms_kNPCName
	 || kPropertyName == ms_kEntryIDName
     || kPropertyName == ms_kMoveTypeName
	 || kPropertyName == ms_kWayPointsName
     || kPropertyName == ms_kSpawnFlagName )
    {
        bIsInheritable = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex,bool& bIsExternalAssetPath) const
{
	if( kPropertyName == ms_kNPCName
	 || kPropertyName == ms_kEntryIDName
     || kPropertyName == ms_kMoveTypeName
	 || kPropertyName == ms_kWayPointsName
     || kPropertyName == ms_kSpawnFlagName )
	{
        bIsExternalAssetPath = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const
{
	if( kPropertyName == ms_kNPCName
     || kPropertyName == ms_kMoveTypeName
	 || kPropertyName == ms_kEntryIDName
     || kPropertyName == ms_kSpawnFlagName)
    {
        uiCount = 1;
    }
	else if(kPropertyName == ms_kWayPointsName)
	{
		uiCount = m_kWayPoints.GetSize();
	}
	else
	{
		return false;
	}

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet)
{
	if( kPropertyName == ms_kNPCName
	 || kPropertyName == ms_kEntryIDName
     || kPropertyName == ms_kMoveTypeName
	 || kPropertyName == ms_kWayPointsName
     || kPropertyName == ms_kSpawnFlagName )
	{
        bCountSet = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::IsCollection(const NiFixedString& kPropertyName,bool& bIsCollection) const
{
	if( kPropertyName == ms_kNPCName
     || kPropertyName == ms_kMoveTypeName
	 || kPropertyName == ms_kEntryIDName
     || kPropertyName == ms_kSpawnFlagName )
    {
        bIsCollection = false;
    }
	else if(kPropertyName == ms_kWayPointsName)
	{
		bIsCollection = true;
	}
	else
	{
		return false;
	}

    return true;
}

//---------------------------------------------------------------------------
NiBool CNpcComponent::GetPropertyData(const NiFixedString& kPropertyName, unsigned int& uiData, unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kEntryIDName)
    {
        uiData = GetEntryID();
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::SetPropertyData(const NiFixedString& kPropertyName, unsigned int uiData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kEntryIDName)
	{
		SetEntryID(uiData);
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::GetPropertyData(const NiFixedString& kPropertyName, bool& bData, unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kMoveTypeName)
    {
        bData = (GetMoveType() != 0);
    }
    else if (kPropertyName == ms_kSpawnFlagName)
    {
        bData = ((GetFlag() & 0x00000001) == 0);
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::SetPropertyData(const NiFixedString& kPropertyName, bool bData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kMoveTypeName)
	{
        if(bData)
            SetMoveType(10);
        else
            SetMoveType(0);
	}
    else if (kPropertyName == ms_kSpawnFlagName)
    {
        unsigned int uiFlag = GetFlag();
        if(bData)
        {
            uiFlag &= ~0x00000001;
        }
        else
        {
            uiFlag |= 0x00000001;
        }
        SetFlag(uiFlag);
    }
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CNpcComponent::GetPropertyData(const NiFixedString& kPropertyName, NiFixedString& kData,unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kNPCName)
    {
        kData = GetNpcName();
    }
    else
    {
        return false;
    }

    return true;
}


//---------------------------------------------------------------------------
NiBool CNpcComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiFixedString& kData,unsigned int uiIndex)
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kNPCName)
    {
        SetNpcName(kData);
    }
    else
    {
        return false;
    }

    return true;
}

//---------------------------------------------------------------------------
NiBool CNpcComponent::GetPropertyData(const NiFixedString& kPropertyName, NiEntityInterface*& pkData, unsigned int uiIndex) const
{
	if(kPropertyName == ms_kWayPointsName)
	{
		if(uiIndex < m_kWayPoints.GetSize())
		{
			pkData = m_kWayPoints.GetAt(uiIndex);
		}
		else
		{
			pkData = NULL;
		}
	}
	else
	{
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------
NiBool CNpcComponent::SetPropertyData(const NiFixedString& kPropertyName, NiEntityInterface* pkData, unsigned int uiIndex)
{
	if(kPropertyName == ms_kWayPointsName)
	{
		SetWayPoint(uiIndex, pkData);
	}
	else
	{
		return false;
	}

	return true;
}
