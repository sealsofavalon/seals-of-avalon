#pragma once

#include "TerrainLibType.h"

class TERRAIN_ENTRY CNpcComponent : public NiRefObject,
                                    public NiEntityComponentInterface
{
public:
    static void _SDMInit();
    static void _SDMShutdown();

    // Class name.
    static NiFixedString ms_kClassName;

    // Component name.
    static NiFixedString ms_kComponentName;

    // Property names.
    static NiFixedString ms_kNPCName;
    static NiFixedString ms_kEntryIDName;
	static NiFixedString ms_kWayPointsName;
    static NiFixedString ms_kMoveTypeName;
    static NiFixedString ms_kSpawnFlagName;

    // Dependent property names.
    static NiFixedString ms_kTranslationName;
    static NiFixedString ms_kRotationName;
    static NiFixedString ms_kScaleName;

    static NiUniqueID ms_kTemplateID;

	unsigned int GetId() const { return m_uiSpawnId; }
	void SetId(unsigned int uiId) { m_uiSpawnId = uiId; }
	void SetEntryID(unsigned int uiEntryID) { m_uiEntryID = uiEntryID; }
	unsigned int GetEntryID() const { return m_uiEntryID; }
	void SetNpcName(const NiFixedString& kNpcName) { m_kNpcName = kNpcName; }
	const NiFixedString& GetNpcName() const { return m_kNpcName; }

	//way point
	unsigned int GetWayPointCount() const { return m_kWayPoints.GetSize(); }
	unsigned int GetWayPointIndex(NiEntityInterface* pkWayPoint);
	void SetWayPoint(unsigned int uiIndex, NiEntityInterface* pkWayPoint);
	NiEntityInterface* GetWayPoint(unsigned int uiIndex);
	void RemoveWayPoint(NiEntityInterface* pkWayPoint);

	void BuildPathGeomerty();
    unsigned int GetMoveType() const { return m_uiMoveType; }
    void SetMoveType(unsigned int uiMoveType) { m_uiMoveType = uiMoveType; }

    unsigned int GetFlag() const { return m_uiFlag; }
    void SetFlag(unsigned int uiFlag) { m_uiFlag = uiFlag; }

private:
	unsigned int m_uiSpawnId;
    unsigned int m_uiEntryID;
	NiFixedString m_kNpcName;
	NiTPrimitiveArray<NiEntityInterface*> m_kWayPoints;
	NiTriShapePtr m_spPathGeometry;
	NiPoint3 m_kTranslation;
    unsigned int m_uiMoveType;
    unsigned int m_uiFlag;

	static NiVertexColorProperty* ms_pkVertexColorProperty;
	static NiWireframeProperty* ms_pkWireframeProperty;
	static NiStencilProperty* ms_pkStencilProperty;

public:
    CNpcComponent(const NiFixedString& kName, unsigned int uiEntryID);
	CNpcComponent();

    // NiEntityComponentInterface overrides.
    virtual NiEntityComponentInterface* Clone(bool bInheritProperties);
    virtual NiEntityComponentInterface* GetMasterComponent();
    virtual void SetMasterComponent(NiEntityComponentInterface* pkMasterComponent);
    virtual void GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames);

    // NiEntityPropertyInterface overrides.
    virtual NiBool SetTemplateID(const NiUniqueID& kID);
    virtual NiUniqueID GetTemplateID();
    virtual void AddReference();
    virtual void RemoveReference();
    virtual NiFixedString GetClassName() const;
    virtual NiFixedString GetName() const;
    virtual NiBool SetName(const NiFixedString& kName);
    virtual void Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager);
    virtual void BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors);
    virtual void GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const;
    virtual NiBool CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const;
    virtual NiBool ResetProperty(const NiFixedString& kPropertyName);
    virtual NiBool MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique);
    virtual NiBool GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const;
    virtual NiBool SetDisplayName(const NiFixedString& kPropertyName, const NiFixedString& kDisplayName);
    virtual NiBool GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const;
    virtual NiBool SetPrimitiveType(const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType);
    virtual NiBool GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const;
    virtual NiBool SetSemanticType(const NiFixedString& kPropertyName, const NiFixedString& kSemanticType);
    virtual NiBool GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const;
    virtual NiBool SetDescription(const NiFixedString& kPropertyName, const NiFixedString& kDescription);
    virtual NiBool GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const;
    virtual NiBool IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly);
    virtual NiBool IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique);
    virtual NiBool IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable);
    virtual NiBool IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable);
    virtual NiBool IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex, bool& bIsExternalAssetPath) const;
    virtual NiBool GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const;
    virtual NiBool SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet);
    virtual NiBool IsCollection(const NiFixedString& kPropertyName, bool& bIsCollection) const;

    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, unsigned int& uiData, unsigned int uiIndex) const;
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, unsigned int uiData, unsigned int uiIndex);
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, bool& bData, unsigned int uiIndex) const;
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, bool bData, unsigned int uiIndex);
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiFixedString& kData, unsigned int uiIndex) const;
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiFixedString& kData,unsigned int uiIndex);
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiEntityInterface*& pkData, unsigned int uiIndex) const;
    virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, NiEntityInterface* pkData, unsigned int uiIndex);
};