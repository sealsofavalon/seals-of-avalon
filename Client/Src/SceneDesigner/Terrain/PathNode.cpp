#include "TerrainPCH.h"
#include "PathNode.h"
#include "Chunk.h"
#include "TerrainHelper.h"
#include "Tile.h"
#include "PathNodeComponent.h"
#include "Map.h"

void CPathNode::Sort()
{
    float fLen1, fLen2;
    CPathNode* pkPathNode1;
    CPathNode* pkPathNode2;
    for(unsigned int i = 0; i < m_kLinkList.GetSize(); ++i)
    {
        pkPathNode1 = m_kLinkList.GetAt(i);
        fLen1 = (pkPathNode1->m_kTranslate - m_kTranslate).Length();
        for(unsigned int j = i + 1; j < m_kLinkList.GetSize(); ++j)
        {
            pkPathNode2 = m_kLinkList.GetAt(j);
            fLen2 = (pkPathNode2->m_kTranslate - m_kTranslate).Length();
            if(fLen1 > fLen2)
            {
                m_kLinkList.SetAt(i, pkPathNode2);
                m_kLinkList.SetAt(j, pkPathNode1);
                pkPathNode1 = pkPathNode2;
                fLen1 = fLen2;
            }
        }
    }
}

CPathNodeManager* CPathNodeManager::ms_pkThis = NULL;
CPathNodeManager::CPathNodeManager()
    :m_kPathNodeList(16, 16)
    ,m_bDirty(false)
{
    NIASSERT(ms_pkThis == NULL);
    ms_pkThis = this;

    m_kPathRender.SetColor(NiColorA(1.f, 0.f, 0.f, 1.f));
}

CPathNodeManager::~CPathNodeManager()
{
    for(unsigned int ui = 0; ui < m_kPathNodeList.GetSize(); ++ui)
    {
        NiDelete m_kPathNodeList.GetAt(ui);
    }

    ms_pkThis = NULL;
}

void CPathNodeManager::Load(const NiFixedString& kMapName)
{
    if(!CTile::IsEditNpc())
        return;

    char acFilename[NI_MAX_PATH];
    NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\World\\Maps\\%s\\%s.pth", TerrainHelper::GetClientPath(), (const char*)kMapName, (const char*)kMapName);
    NiFile* pkFile = NiFile::GetFile(acFilename, NiFile::READ_ONLY);
    if( !pkFile || !(*pkFile) )
    {
        NiDelete pkFile;
        return;
    }

    CTagStream kStream;
	while( kStream.ReadFromStream( pkFile ) )
    {
        switch( kStream.GetTag() )
        {
        case 'NODE':
            {
                CPathNode* pkPathNode;
                unsigned int uiLinkCount;
                unsigned int uiFirstLink;

                //nodes
                unsigned int uiNodeCount;
                kStream.Read(&uiNodeCount, sizeof(unsigned int));
                m_kPathNodeList.SetSize(uiNodeCount);
                for(unsigned int i = 0; i < uiNodeCount; ++i)
                {
                    pkPathNode = NiNew CPathNode(NiPoint3::ZERO);
                    kStream.Read(&pkPathNode->m_kTranslate, sizeof(NiPoint3));
                    kStream.Read(&uiLinkCount, sizeof(unsigned int));
                    kStream.Read(&uiFirstLink, sizeof(unsigned int));
                    
                    pkPathNode->m_kLinkList.SetSize(uiLinkCount);
                    for(unsigned int j = 0; j < uiLinkCount; ++j)
                    {
                        pkPathNode->m_kLinkList.Add(0);
                    }

                    pkPathNode->m_uiId = i;
                    m_kPathNodeList.SetAt(i, pkPathNode);
                }

                //links
                unsigned int uiLink;
                unsigned int uiTotalLinkCount;
                kStream.Read(&uiTotalLinkCount, sizeof(unsigned int));
                for(unsigned int i = 0; i < uiNodeCount; ++i)
                {
                    pkPathNode = m_kPathNodeList.GetAt(i);
                    for(unsigned int j = 0; j < pkPathNode->m_kLinkList.GetSize(); ++j)
                    {
                        kStream.Read(&uiLink, sizeof(unsigned int));
                        NIASSERT(uiLink < m_kPathNodeList.GetSize());
                        pkPathNode->m_kLinkList.SetAt(j, m_kPathNodeList.GetAt(uiLink));
                    }
                }
            }
            break;
        }
    }

    NiDelete pkFile;
    m_bDirty = false;
}

void CPathNodeManager::Save(const NiFixedString& kMapName)
{
    if(!CTile::IsEditNpc())
        return;

    if(!m_bDirty)
        return;

    //节点重新排序, 返回非孤立点的数量, 不保存孤立点
    unsigned int uiNodeCount = Sort();

    CTagStream kStream;
    kStream.Reset();
    kStream.SetTag('NODE');

    kStream.Write(&uiNodeCount, sizeof(unsigned int));

    unsigned int uiFirstLink = 0;
    unsigned int uiLinkCount;
    for(unsigned int i = 0; i < uiNodeCount; ++i)
    {
        kStream.Write(&m_kPathNodeList.GetAt(i)->m_kTranslate, sizeof(NiPoint3));
        
        uiLinkCount = m_kPathNodeList.GetAt(i)->m_kLinkList.GetSize();
        kStream.Write(&uiLinkCount, sizeof(unsigned int)); 
        kStream.Write(&uiFirstLink, sizeof(unsigned int));

        uiFirstLink += uiLinkCount;
    }

    //Links
    kStream.Write(&uiFirstLink, sizeof(unsigned int)); //总共Link数量
    CPathNode* pkPathNode;
    CPathNode* pkLinkNode;
    unsigned int uiSize;
    for(unsigned int i = 0; i < uiNodeCount; ++i)
    {
        pkPathNode = m_kPathNodeList.GetAt(i);
        uiSize = pkPathNode->m_kLinkList.GetSize();
        NIASSERT(uiSize > 0);
        for(unsigned int j = 0; j < uiSize; ++j)
        {
            pkLinkNode = pkPathNode->m_kLinkList.GetAt(j);
            kStream.Write(&pkLinkNode->m_uiId, sizeof(unsigned int));
        }
    }

    char acFilename[NI_MAX_PATH];
    NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\World\\Maps\\%s\\%s.pth", TerrainHelper::GetClientPath(), (const char*)kMapName, (const char*)kMapName);
    NiFile* pkFile = NiFile::GetFile(acFilename, NiFile::WRITE_ONLY);
    if( !pkFile || !(*pkFile) )
    {
        NiDelete pkFile;
        return;
    }

    kStream.WriteToStream(pkFile);

    NiDelete pkFile;

    m_bDirty = false;
}

//按照距离进行排序, 返回有效点的个数
unsigned int CPathNodeManager::Sort()
{
    float fLen1, fLen2;
    CPathNode* pkPathNode1;
    CPathNode* pkPathNode2;
    bool bSwap;

    for(unsigned int i = 0; i < m_kPathNodeList.GetSize(); ++i)
    {
        pkPathNode1 = m_kPathNodeList.GetAt(i);
        fLen1 = pkPathNode1->m_kTranslate.Length();

        for(unsigned int j = i + 1; j < m_kPathNodeList.GetSize(); ++j)
        {
            pkPathNode2 = m_kPathNodeList.GetAt(j);
            fLen2 = pkPathNode2->m_kTranslate.Length();
            bSwap = false;

            if( pkPathNode2->m_kLinkList.GetSize() == 0 )
            {
                bSwap = false;
            }
            else if( pkPathNode1->m_kLinkList.GetSize() == 0 )
            {
                bSwap = true;
            }
            else if(fLen1 > fLen2)
            {
                bSwap = true;
            }

            if(bSwap)
            {
                m_kPathNodeList.SetAt(i, pkPathNode2);
                m_kPathNodeList.SetAt(j, pkPathNode1);
                fLen1 = fLen2;
                pkPathNode1 = pkPathNode2;
            }
        }
    }

    unsigned int uiValidNodeCount = 0;
    CPathNode* pkPathNode;
    for(unsigned int k = 0; k < m_kPathNodeList.GetSize(); ++k)
    {
        pkPathNode = m_kPathNodeList.GetAt(k);
        pkPathNode->m_uiId = k;

        if(pkPathNode->m_kLinkList.GetEffectiveSize() > 0)
        {
            uiValidNodeCount = k + 1;
        }

        pkPathNode->Sort();
    }

    return uiValidNodeCount;
}

bool CPathNodeManager::FindPathNode(const CPathNode* pkPathNode)
{
    for(unsigned int i = 0; i < m_kPathNodeList.GetSize(); ++i)
    {
        if(m_kPathNodeList.GetAt(i) == pkPathNode)
            return true;
    }

    return false;
}

void CPathNodeManager::OnPathNodeAdded(NiEntityInterface* pkEntity)
{
    CPathNodeComponent* pkPathNodeComponent = (CPathNodeComponent*)pkEntity->GetComponentByTemplateID(CPathNodeComponent::ms_kTemplateID);
    NIASSERT(pkPathNodeComponent);

    CPathNode* pkPathNode = pkPathNodeComponent->GetPathNode();
    if(pkPathNode == NULL)
    {
        NiPoint3 kTranslate;
        pkEntity->GetPropertyData("Translation", kTranslate);
        pkPathNode = NiNew CPathNode(kTranslate);
        pkPathNodeComponent->SetPathNode(pkPathNode);
    }
    else
    {
        for (unsigned int i = 0; i < m_kPathNodeList.GetSize(); ++i)
        {
            if(m_kPathNodeList.GetAt(i) == pkPathNode)
                return;
        }
    }

    pkPathNode->m_kLinkList.RemoveAll();
    pkPathNode->SetEntity(pkEntity);

    m_kPathNodeList.Add(pkPathNode);
    OnPathNodeChanged(pkPathNode, pkPathNode->GetTranslate());

    m_bDirty = true;
}

void CPathNodeManager::OnPathNodeRemoved(NiEntityInterface* pkEntity)
{
    CPathNodeComponent* pkPathNodeComponent = (CPathNodeComponent*)pkEntity->GetComponentByTemplateID(CPathNodeComponent::ms_kTemplateID);
    NIASSERT(pkPathNodeComponent);

    CPathNode* pkPathNode = pkPathNodeComponent->GetPathNode();
    CPathNode* pkNode;
    for(unsigned int i = 0; i < m_kPathNodeList.GetSize(); ++i)
    {
        pkNode = m_kPathNodeList.GetAt(i);
        if(pkNode == pkPathNode)
        {
            m_kPathNodeList.SetAt(i, NULL);
        }
        else
        {
            //移除到这个的连接
            for(unsigned int j = 0; j < pkNode->m_kLinkList.GetSize(); ++j)
            {
                if(pkNode->m_kLinkList.GetAt(j) == pkPathNode)
                    pkNode->m_kLinkList.SetAt(j, NULL);
            }

            pkNode->m_kLinkList.Compact();
        }
    }

    NiDelete pkPathNode;
    m_kPathNodeList.Compact();

    pkPathNodeComponent->SetPathNode(NULL);

    m_bDirty = true;
}

void CPathNodeManager::OnPathNodeChanged(CPathNode* pkPathNode, const NiPoint3& kTranslate)
{
    NiPoint3 kPos;
    kPos.x = NiClamp(kTranslate.x, m_kBoundBox.m_kMin.x, m_kBoundBox.m_kMax.x);
    kPos.y = NiClamp(kTranslate.y, m_kBoundBox.m_kMin.y, m_kBoundBox.m_kMax.y);
    kPos.z = kTranslate.z;

    if(pkPathNode->GetTranslate() != kPos)
    {
        pkPathNode->SetTranslate(kPos);
        m_bDirty = true;
    }
}

void CPathNodeManager::LinkPathNode(CPathNode* pkPathNode1, CPathNode* pkPathNode2)
{
    NIASSERT(pkPathNode1 && pkPathNode2);
    NIASSERT(pkPathNode1 != pkPathNode2);
    if(pkPathNode1 == pkPathNode2 || !pkPathNode1 || !pkPathNode2)
        return;

    NIASSERT(FindPathNode(pkPathNode1) && FindPathNode(pkPathNode2));

    bool bFound = false;
    for(unsigned int i = 0; i < pkPathNode1->m_kLinkList.GetSize(); ++i)
    {
        if(pkPathNode1->m_kLinkList.GetAt(i) == pkPathNode2)
        {
            bFound = true;
            break;
        }
    }

    if(!bFound)
    {
        pkPathNode1->m_kLinkList.Add(pkPathNode2);
        m_bDirty = true;
    }

    bFound = false;
    for(unsigned int i = 0; i < pkPathNode2->m_kLinkList.GetSize(); ++i)
    {
        if(pkPathNode2->m_kLinkList.GetAt(i) == pkPathNode1)
        {
            bFound = true;
            break;
        }
    }

    if(!bFound)
    {
        pkPathNode2->m_kLinkList.Add(pkPathNode1);
        m_bDirty = true;
    }
}

void CPathNodeManager::LinkPathNodes(NiTPrimitiveArray<CPathNode*>& kPathNodes, bool bRing)
{
    unsigned int uiSize = kPathNodes.GetSize();
    if(uiSize < 2)
        return;

    for(unsigned int i = 0; i + 1 < uiSize; ++i)
    {
        LinkPathNode(kPathNodes.GetAt(i), kPathNodes.GetAt(i + 1));
    }

    if(bRing && uiSize > 2)
    {
        LinkPathNode(kPathNodes.GetAt(uiSize - 1), kPathNodes.GetAt(0));
    }
}

void CPathNodeManager::UnlinkPathNode(CPathNode* pkPathNode1, CPathNode* pkPathNode2)
{
    NIASSERT(pkPathNode1 && pkPathNode2);

    for(unsigned int i = 0; i < pkPathNode1->m_kLinkList.GetSize(); ++i)
    {
        if(pkPathNode1->m_kLinkList.GetAt(i) == pkPathNode2)
        {
            pkPathNode1->m_kLinkList.SetAt(i, NULL);
            m_bDirty = true;
        }
    }
    pkPathNode1->m_kLinkList.Compact();

    for(unsigned int i = 0; i < pkPathNode2->m_kLinkList.GetSize(); ++i)
    {
        if(pkPathNode2->m_kLinkList.GetAt(i) == pkPathNode1)
        {
            pkPathNode2->m_kLinkList.SetAt(i, NULL);
            m_bDirty = true;
        }
    }
    pkPathNode2->m_kLinkList.Compact();
}

void CPathNodeManager::UnlinkPathNodes(NiTPrimitiveArray<CPathNode*>& kPathNodes, bool bRing)
{
    unsigned int uiSize = kPathNodes.GetSize();
    if(uiSize < 2)
        return;

    for(unsigned int i = 0; i + 1 < uiSize; ++i)
    {
        UnlinkPathNode(kPathNodes.GetAt(i), kPathNodes.GetAt(i + 1));
    }

    if(bRing && uiSize > 2)
    {
        UnlinkPathNode(kPathNodes.GetAt(uiSize - 1), kPathNodes.GetAt(0));
    }
}

void CPathNodeManager::GetEntities(NiScene* pkScene)
{
    CMap* pkMap = CMap::Get();
    if(pkMap == NULL)
        return;

    CTile* pkTile = pkMap->GetCurrentTile();
    if(pkTile == NULL)
        return;

    m_kBoundBox.m_kMin = NiPoint3(float(pkTile->GetX()*TILE_SIZE - TILE_SIZE), float(pkTile->GetY()*TILE_SIZE - TILE_SIZE), 0.f);
    m_kBoundBox.m_kMax = NiPoint3(float(pkTile->GetX()*TILE_SIZE + TILE_SIZE*2), float(pkTile->GetY()*TILE_SIZE + TILE_SIZE*2), 0.f);

	CPathNode* pkPathNode;

	NiGeneralEntity* pkEntity;

	NiTransformationComponent* pkTransformationComponent;
	CPathNodeComponent* pkPathNodeComponent;
    char aName[32];
	for (unsigned int i = 0; i < m_kPathNodeList.GetSize(); ++i)
	{
		pkPathNode = m_kPathNodeList.GetAt(i);
        if( pkPathNode->GetTranslate().x <= m_kBoundBox.m_kMax.x
         && pkPathNode->GetTranslate().x >= m_kBoundBox.m_kMin.x
         && pkPathNode->GetTranslate().y <= m_kBoundBox.m_kMax.y
         && pkPathNode->GetTranslate().y >= m_kBoundBox.m_kMin.y )
        {
            NiSprintf(aName, sizeof(aName), "PathNode %d", i);

            pkEntity = NiNew NiGeneralEntity;
            pkEntity->SetName(aName);
            pkEntity->SetTypeMask(NiGeneralEntity::PathNode);

            pkTransformationComponent = NiNew NiTransformationComponent;
            pkTransformationComponent->SetTranslation(pkPathNode->GetTranslate());
            pkEntity->AddComponent(pkTransformationComponent, false);

            pkPathNodeComponent = NiNew CPathNodeComponent();
            pkPathNodeComponent->SetPathNode(pkPathNode);
            pkEntity->AddComponent(pkPathNodeComponent, false);

            pkScene->AddEntity(pkEntity);

            pkPathNode->SetEntity(pkEntity);
        }
	}
}

void CPathNodeManager::BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, const CBox& kBox)
{
    m_kPathRender.Clear();

    CPathNode* pkPathNode;
    NiEntityPropertyInterface* pkEntity;
    NiVisibleArray* pkVisibleSet = pkRenderingContext->m_pkCullingProcess->GetVisibleSet();
    unsigned int uiOldCount;
	for (unsigned int i = 0; i < m_kPathNodeList.GetSize(); ++i)
	{
		pkPathNode = m_kPathNodeList.GetAt(i);
        if( pkPathNode->GetTranslate().x <= kBox.m_kMax.x
         && pkPathNode->GetTranslate().x >= kBox.m_kMin.x
         && pkPathNode->GetTranslate().y <= kBox.m_kMax.y
         && pkPathNode->GetTranslate().y >= kBox.m_kMin.y )
        {
            pkEntity = pkPathNode->GetEntity();
            if(pkEntity == NULL)
                continue;

            uiOldCount = pkVisibleSet->GetCount();
            pkEntity->BuildVisibleSet(pkRenderingContext, NULL);
            if(uiOldCount < pkVisibleSet->GetCount())
            {
                pkEntity->Update(NULL, 0.f, NULL, NULL);
            }
        }

        BuildPathLink(pkPathNode, kBox);
	}
}

void CPathNodeManager::BuildPathLink(CPathNode* pkPathNode, const CBox& kBox)
{
    CMap* pkMap = CMap::Get();
    NIASSERT(pkMap);
    unsigned int uiFrameCount = pkMap->GetFrameCount();
    pkPathNode->SetFrameCount(uiFrameCount);

    CPathNode* pkLink;
    for(unsigned int i = 0; i < pkPathNode->m_kLinkList.GetSize(); ++i)
    {
        pkLink = pkPathNode->m_kLinkList.GetAt(i);
        if(pkLink->GetFrameCount() == uiFrameCount) //这条连接已经显示过
            continue;

        if(kBox.CollideLine(pkPathNode->GetTranslate(), pkLink->GetTranslate()))
        {
            m_kPathRender.AddVertex(pkPathNode->GetTranslate(), true);
            m_kPathRender.AddVertex(pkLink->GetTranslate(), false);
        }
    }
}

bool CPathNodeManager::CheckConnected()
{
    CMap* pkMap = CMap::Get();
    NiEntityErrorInterface* pkErrorHandler = pkMap->GetErrorHandler();
    char acMsg[128];
    NiSprintf(acMsg, sizeof(acMsg), "Total path node count %d",  m_kPathNodeList.GetSize());
    pkErrorHandler->ReportError(acMsg, NULL, NULL, NULL);


    unsigned int uiPathNodeCount = Sort();
    if(uiPathNodeCount <= 2) //少于2个有效的点
        return true;

    NiTPrimitiveSet<CPathNode*> kPathNodeSet(uiPathNodeCount);

    kPathNodeSet.Add(m_kPathNodeList.GetAt(0));
    unsigned int uiOldIndex = 0;
    unsigned int uiNewIndex = 0;
    CPathNode* pkPathNode;

    while(true)
    {
        uiOldIndex = uiNewIndex;
        uiNewIndex = kPathNodeSet.GetSize();

        if(uiOldIndex == uiNewIndex)
            break;

        for(unsigned int uiIndex = uiOldIndex; uiIndex < uiNewIndex; ++uiIndex)
        {
            pkPathNode = kPathNodeSet.GetAt(uiIndex);
            for(unsigned int i = 0; i < pkPathNode->m_kLinkList.GetSize(); ++i)
                kPathNodeSet.AddUnique(pkPathNode->m_kLinkList.GetAt(i));
        }
    }

    if( kPathNodeSet.GetSize() < uiPathNodeCount )
    {
        pkErrorHandler->ReportError("path node not connected", NULL, NULL, NULL);

        for(unsigned int i = 1; i < uiPathNodeCount; ++i)
        {
            pkPathNode = m_kPathNodeList.GetAt(i);
            if(!pkPathNode->m_pkEntity)
                continue;

            if(kPathNodeSet.Find(pkPathNode) == -1)
            {
                pkErrorHandler->ReportError(pkPathNode->m_pkEntity->GetName(), "not connected", NULL, NULL);
                break;
            }
        }

        for(unsigned int i = 0; i < uiPathNodeCount; ++i)
        {
            pkPathNode = m_kPathNodeList.GetAt(i);
            if(!pkPathNode->m_pkEntity)
                continue;

            pkErrorHandler->ReportError(pkPathNode->m_pkEntity->GetName(), "not connected", NULL, NULL);
            break;
        }

        return false;
    }

    return true;
}

CPathNodeManager* CPathNodeManager::Get()
{
    return ms_pkThis;
}