#pragma once
#include "TerrainLibType.h"
#include "Box.h"
#include "PathRender.h"

class TERRAIN_ENTRY CPathNode : public NiMemObject
{
private:
    NiPoint3 m_kTranslate;
    unsigned int m_uiId; //只有在刚刚排序完成后是正确的
    NiTPrimitiveArray<CPathNode*> m_kLinkList;
    NiEntityInterface* m_pkEntity; //所属的Entity, 可以为NULL
    unsigned int m_uiFrameCount;
    
public:
    CPathNode(const NiPoint3& kTranslate)
        :m_kLinkList(8, 8)
        ,m_kTranslate(kTranslate)
        ,m_pkEntity(NULL)
        ,m_uiFrameCount(0)
    {
    }

    virtual ~CPathNode()
    {
        if(m_pkEntity)
            m_pkEntity->RemoveReference();
    }

    void SetEntity(NiEntityInterface* pkEntity)
    {
        NIASSERT(pkEntity && !m_pkEntity);
        m_pkEntity = pkEntity;
        pkEntity->AddReference();
    }

    NiEntityInterface* GetEntity() { return m_pkEntity; }

    void SetTranslate(const NiPoint3& kTranslate) { m_kTranslate = kTranslate; }
    const NiPoint3& GetTranslate() const { return m_kTranslate; }

    void Sort();

    unsigned int GetFrameCount() const { return m_uiFrameCount; }
    void SetFrameCount(unsigned int uiFrameCount) { m_uiFrameCount = uiFrameCount; }

    friend class CPathNodeManager;
};

class TERRAIN_ENTRY CPathNodeManager : public NiMemObject
{
public:
    CPathNodeManager();
    virtual ~CPathNodeManager();

    void Load(const NiFixedString& kMapName);
    void Save(const NiFixedString& kMapName);

    //按照距离进行排序, 返回有效点的个数
    unsigned int Sort();

    bool FindPathNode(const CPathNode* pkPathNode);

    void OnPathNodeAdded(NiEntityInterface* pkEntity);
    void OnPathNodeRemoved(NiEntityInterface* pkEntity);
    void OnPathNodeChanged(CPathNode* pkPathNode, const NiPoint3& kTranslate);

    void LinkPathNode(CPathNode* pkPathNode1, CPathNode* pkPathNode2);
    void LinkPathNodes(NiTPrimitiveArray<CPathNode*>& kPathNodes, bool bRing);

    void UnlinkPathNode(CPathNode* pkPathNode1, CPathNode* pkPathNode2);
    void UnlinkPathNodes(NiTPrimitiveArray<CPathNode*>& kPathNodes, bool bRing);

    bool IsDirty() const { return m_bDirty; }
    void SetDirty(bool bDirty) { m_bDirty = bDirty; }

    void GetEntities(NiScene* pkScene);
    void BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, const CBox& kBox);
    void BuildPathLink(CPathNode* pkPathNode, const CBox& kBox);
    void DrawPath(NiCamera* pkCamera) { m_kPathRender.Render(pkCamera); }

    //检测是否连通
    bool CheckConnected();

    static CPathNodeManager* Get();

private:
    NiTPrimitiveArray<CPathNode*> m_kPathNodeList;
    bool m_bDirty;
    CBox m_kBoundBox;
    CPathRender m_kPathRender;
    static CPathNodeManager* ms_pkThis;
};

#define g_pkPathNodeManager (CPathNodeManager::Get())