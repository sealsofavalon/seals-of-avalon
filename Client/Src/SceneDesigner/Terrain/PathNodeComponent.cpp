#include "TerrainPCH.h"

#include "PathNodeComponent.h"

NiFixedString CPathNodeComponent::ms_kClassName;
NiFixedString CPathNodeComponent::ms_kComponentName;
NiFixedString CPathNodeComponent::ms_kTranslationName;
NiFixedString CPathNodeComponent::ms_kSceneRootPointerName;
NiAVObject* CPathNodeComponent::ms_pkSceneRootTemplate;

NiUniqueID CPathNodeComponent::ms_kTemplateID;

//---------------------------------------------------------------------------
void CPathNodeComponent::_SDMInit()
{
    ms_kClassName = "CPathNodeComponent";
    ms_kComponentName = "Path Node";
    ms_kTranslationName = "Translation";
    ms_kSceneRootPointerName = "Scene Root Pointer";

    ms_kTemplateID = NiUniqueID(0x61,0x3A,0x72,0x02,0x6B,0x84,0x43,0xD8,0x88,0xD3,0x00,0xED,0xAA,0x6E,0x6E,0xA8); 

	LoadSceneRootTemplate();
}

//---------------------------------------------------------------------------
void CPathNodeComponent::_SDMShutdown()
{
    ms_kClassName = NULL;
    ms_kComponentName = NULL;

 	ms_kSceneRootPointerName = NULL;
	
	ms_kTranslationName = NULL;

	DestroySceneRootTemplate();
}

void CPathNodeComponent::LoadSceneRootTemplate()
{
	NiStream kStream;
	kStream.Load("..\\..\\Data\\pathnode.nif");
	NIASSERT( kStream.GetObjectCount() == 1 );
	NIASSERT(NiIsKindOf(NiNode, kStream.GetObjectAt(0)));

	ms_pkSceneRootTemplate = (NiAVObject*)kStream.GetObjectAt(0);
	ms_pkSceneRootTemplate->IncRefCount();
}

void CPathNodeComponent::DestroySceneRootTemplate()
{
	ms_pkSceneRootTemplate->DecRefCount();
	ms_pkSceneRootTemplate = NULL;
}

CPathNodeComponent::CPathNodeComponent()
    :m_pkPathNode(NULL)
{
	m_spSceneRoot = (NiAVObject*)ms_pkSceneRootTemplate->Clone();
	m_spSceneRoot->UpdateProperties();
}

//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* CPathNodeComponent::Clone(bool bInheritProperties)
{
    CPathNodeComponent* pkPathNodeComponent = NiNew CPathNodeComponent();
    return pkPathNodeComponent;
}
//---------------------------------------------------------------------------
NiEntityComponentInterface* CPathNodeComponent::GetMasterComponent()
{
    return NULL;
}
//---------------------------------------------------------------------------
void CPathNodeComponent::SetMasterComponent(NiEntityComponentInterface* pkMasterComponent)
{
}
//---------------------------------------------------------------------------
void CPathNodeComponent::GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
    kDependentPropertyNames.Add(ms_kTranslationName);
}
//---------------------------------------------------------------------------
// NiEntityPropertyInterface overrides.
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::SetTemplateID(const NiUniqueID& kID)
{
    //Not supported for custom components
    return false;
}
//---------------------------------------------------------------------------
NiUniqueID  CPathNodeComponent::GetTemplateID()
{
    return ms_kTemplateID;
}
//---------------------------------------------------------------------------
void CPathNodeComponent::AddReference()
{
    this->IncRefCount();
}
//---------------------------------------------------------------------------
void CPathNodeComponent::RemoveReference()
{
    this->DecRefCount();
}
//---------------------------------------------------------------------------
NiFixedString CPathNodeComponent::GetClassName() const
{
    return ms_kClassName;
}
//---------------------------------------------------------------------------
NiFixedString CPathNodeComponent::GetName() const
{
    return ms_kComponentName;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::SetName(const NiFixedString& kName)
{
    // This component does not allow its name to be set.
    return false;
}
//---------------------------------------------------------------------------
void CPathNodeComponent::BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors)
{
	pkRenderingContext->m_pkCullingProcess->Process(
		pkRenderingContext->m_pkCamera, 
		m_spSceneRoot,
	    pkRenderingContext->m_pkCullingProcess->GetVisibleSet());
}
//---------------------------------------------------------------------------
void CPathNodeComponent::Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager)
{
	NiPoint3 kTranslation;
	if( pkParentEntity->GetPropertyData(ms_kTranslationName, kTranslation)
	 && m_spSceneRoot->GetTranslate() != kTranslation )
	{
		m_spSceneRoot->SetTranslate(kTranslation);
		m_spSceneRoot->Update(fTime);

        if(m_pkPathNode && g_pkPathNodeManager)
            g_pkPathNodeManager->OnPathNodeChanged(m_pkPathNode, kTranslation);
	}
}
//---------------------------------------------------------------------------
void CPathNodeComponent::GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const
{
	kPropertyNames.Add(ms_kSceneRootPointerName);
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const
{
    if( kPropertyName == ms_kSceneRootPointerName )
    {
        bCanReset = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::ResetProperty(const NiFixedString& kPropertyName)
{
    return false;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique)
{
    return false;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const
{
    if (kPropertyName == ms_kSceneRootPointerName)
    {
        // Scene Root Pointer property should not be displayed.
        kDisplayName = NULL;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::SetDisplayName(const NiFixedString& kPropertyName, const NiFixedString& kDisplayName)
{
    // This component does not allow the display name to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const
{
    if (kPropertyName == ms_kSceneRootPointerName)
    {
        kPrimitiveType = PT_NIOBJECTPOINTER;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::SetPrimitiveType(const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType)
{
    // This component does not allow the primitive type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const
{
    if (kPropertyName == ms_kSceneRootPointerName)
    {
        kSemanticType = PT_NIOBJECTPOINTER;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::SetSemanticType(const NiFixedString& kPropertyName, const NiFixedString& kSemanticType)
{
    // This component does not allow the semantic type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const
{
    if (kPropertyName == ms_kSceneRootPointerName)
    {
        // This is a hidden property, so no description is provided.
        kDescription = NULL;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::SetDescription(const NiFixedString& kPropertyName, const NiFixedString& kDescription)
{
    // This component does not allow the description to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const
{
    if( kPropertyName == ms_kSceneRootPointerName )
    {
        kCategory = ms_kComponentName;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly)
{
    if (kPropertyName == ms_kSceneRootPointerName)
    {
        bIsReadOnly = true;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique)
{
    if( kPropertyName == ms_kSceneRootPointerName )
    {
        bIsUnique = true;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable)
{
    if (kPropertyName == ms_kSceneRootPointerName)
    {
        bIsSerializable = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable)
{
    if( kPropertyName == ms_kSceneRootPointerName )
    {
        bIsInheritable = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex,bool& bIsExternalAssetPath) const
{
    if( kPropertyName == ms_kSceneRootPointerName )
    {
        bIsExternalAssetPath = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const
{
    if( kPropertyName == ms_kSceneRootPointerName )
    {
        uiCount = 1;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet)
{
    if( kPropertyName == ms_kSceneRootPointerName )
    {
        bCountSet = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CPathNodeComponent::IsCollection(const NiFixedString& kPropertyName,bool& bIsCollection) const
{
    if( kPropertyName == ms_kSceneRootPointerName )
    {
        bIsCollection = false;
        return true;
    }

    return false;
}

NiBool CPathNodeComponent::GetPropertyData(const NiFixedString& kPropertyName, NiObject*& pkData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		pkData = m_spSceneRoot;
	}
	else
	{
		return false;
	}

	return true;
}
