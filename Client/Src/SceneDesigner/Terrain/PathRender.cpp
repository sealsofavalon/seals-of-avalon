#include "TerrainPCH.h"
#include "PathRender.h"
#include "Map.h"

#define PATH_TRI_DISTANCE (4.f)

#define VERTEX_A NiPoint3(-0.4f, -1.2f, 1.f)
#define VERTEX_B NiPoint3( 0.0f,  1.2f, 1.f)
#define VERTEX_C NiPoint3( 0.4f, -1.2f, 1.f)

CPathRender::CPathRender()
    :m_pkVertex(NULL)
    ,m_kLastVertex(0.f, 0.f, 0.f)
    ,m_uiTriCount(0)
    ,m_uiMaxTriCount(0)
{
}

CPathRender::~CPathRender()
{
    NiFree(m_pkVertex);
}

void CPathRender::Clear()
{
    m_uiTriCount = 0;
}

void CPathRender::AddVertex(const NiPoint3& kVertex, bool bNewPath)
{
    if(bNewPath)
    {
        m_kLastVertex = kVertex;
        return;
    }

    NiMatrix3 kRotMat;
    NiPoint3 kVector = kVertex - m_kLastVertex;
    float fZ = NiATan2(kVector.y, kVector.x) + NI_PI/2;
    kRotMat.MakeZRotation(-fZ);

    NiPoint3 kOffsetA = kRotMat*VERTEX_A;
    NiPoint3 kOffsetB = kRotMat*VERTEX_B;
    NiPoint3 kOffsetC = kRotMat*VERTEX_C;

    float fLen = kVector.Length();
    unsigned int uiCount = unsigned int(fLen / PATH_TRI_DISTANCE);
    if(uiCount < 1)
        uiCount = 1;

    kVector /= float(uiCount + 1);

    if(m_uiTriCount + uiCount > m_uiMaxTriCount)
    {
        m_uiMaxTriCount = m_uiTriCount + uiCount + 1024; //每次多分配1024个
        m_pkVertex = (NiPoint3*)NiRealloc(m_pkVertex, m_uiMaxTriCount*sizeof(NiPoint3)*3);
    }

    NiPoint3 kPos = m_kLastVertex;
    NiPoint3* pkVertex = &m_pkVertex[m_uiTriCount*3];
    for(unsigned int i = 0; i < uiCount; ++i)
    {
        kPos += kVector;

        *pkVertex++ = kPos + kOffsetA;
        *pkVertex++ = kPos + kOffsetB;
        *pkVertex++ = kPos + kOffsetC;
    }

    m_uiTriCount += uiCount;
    m_kLastVertex = kVertex;
}

void CPathRender::Render(NiCamera* pkCamera)
{
    if( (CMap::GetVisibleMask() & NiGeneralEntity::PathNode) == 0 )
        return;

    if(m_uiTriCount == 0)
        return;

    NiDX9Renderer* pkRenderer = (NiDX9Renderer*)NiRenderer::GetRenderer();
    NiDX9RenderState* pkRenderState = pkRenderer->GetRenderState();

    pkRenderState->SetRenderState(D3DRS_LIGHTING, TRUE);
    pkRenderState->SetRenderState(D3DRS_EMISSIVEMATERIALSOURCE, D3DMCS_MATERIAL);
    pkRenderState->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
    pkRenderState->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
    pkRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
    //pkRenderState->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
    pkRenderState->SetVertexShader(NULL);
    pkRenderState->SetPixelShader(NULL);

    pkRenderState->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_DISABLE);
    pkRenderState->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);

    LPDIRECT3DDEVICE9 pkD3D = pkRenderer->GetD3DDevice();

    D3DMATERIAL9 kMaterial;
    kMaterial.Emissive.r = m_kColor.r;
    kMaterial.Emissive.g = m_kColor.g;
    kMaterial.Emissive.b = m_kColor.b;
    kMaterial.Emissive.a = m_kColor.a;
    kMaterial.Ambient = kMaterial.Emissive;
    kMaterial.Diffuse = kMaterial.Emissive;

    pkD3D->SetMaterial(&kMaterial);

    D3DXMATRIX kD3DWorld;
    kD3DWorld._11 = 1.f;
    kD3DWorld._12 = 0.f;
    kD3DWorld._13 = 0.f;
    kD3DWorld._14 = 0.f;

    kD3DWorld._21 = 0.f;
    kD3DWorld._22 = 1.f;
    kD3DWorld._23 = 0.f;
    kD3DWorld._24 = 0.f;

    kD3DWorld._31 = 0.f;
    kD3DWorld._32 = 0.f;
    kD3DWorld._33 = 1.f;
    kD3DWorld._34 = 0.f;

    kD3DWorld._41 = 0.f;
    kD3DWorld._42 = 0.f;
    kD3DWorld._43 = 0.f;
    kD3DWorld._44 = 1.f;

    pkD3D->SetTransform(D3DTS_WORLD, &kD3DWorld);

    const NiPoint3& kLoc = pkCamera->GetWorldLocation();
    NiPoint3 kDir = pkCamera->GetWorldDirection();
    NiPoint3 kRight = pkCamera->GetWorldRightVector();
    NiPoint3 kUp = pkCamera->GetWorldUpVector();

    D3DXMATRIX kD3DView;
    kD3DView._11 = kRight.x;
    kD3DView._12 = kUp.x;
    kD3DView._13 = kDir.x;
    kD3DView._14 = 0.0f;
    kD3DView._21 = kRight.y; 
    kD3DView._22 = kUp.y;
    kD3DView._23 = kDir.y;
    kD3DView._24 = 0.0f;
    kD3DView._31 = kRight.z; 
    kD3DView._32 = kUp.z;
    kD3DView._33 = kDir.z;
    kD3DView._34 = 0.0f;
    kD3DView._41 = -(kRight * kLoc);
    kD3DView._42 = -(kUp * kLoc);
    kD3DView._43 = -(kDir * kLoc);
    kD3DView._44 = 1.0f;

    pkD3D->SetTransform(D3DTS_VIEW, &kD3DView);

    const NiFrustum& kFrustum = pkCamera->GetViewFrustum();

    float fNearDepth = kFrustum.m_fNear;
    float fDepthRange = kFrustum.m_fFar - kFrustum.m_fNear;

    // Projection matrix update
    float fRmL = kFrustum.m_fRight - kFrustum.m_fLeft;
    float fRpL = kFrustum.m_fRight + kFrustum.m_fLeft;
    float fTmB = kFrustum.m_fTop - kFrustum.m_fBottom;
    float fTpB = kFrustum.m_fTop + kFrustum.m_fBottom;
    float fInvFmN = 1.0f / fDepthRange;

    D3DXMATRIX kD3DProj;
    if (kFrustum.m_bOrtho)
    {
        kD3DProj._11 = 2.0f / fRmL;
        kD3DProj._21 = 0.0f;
        kD3DProj._31 = 0.0f; 
        kD3DProj._41 = -fRpL / fRmL; 

        kD3DProj._12 = 0.0f;
        kD3DProj._22 = 2.0f / fTmB;
        kD3DProj._32 = 0.0f; 
        kD3DProj._42 = -fTpB / fTmB;

        kD3DProj._13 = 0.0f;
        kD3DProj._23 = 0.0f;
        kD3DProj._33 = fInvFmN; 
        kD3DProj._43 = -(kFrustum.m_fNear * fInvFmN);
    }
    else
    {
        kD3DProj._11 = 2.0f / fRmL;
        kD3DProj._21 = 0.0f;
        kD3DProj._31 = -fRpL / fRmL;
        kD3DProj._41 = 0.0f;

        kD3DProj._12 = 0.0f;
        kD3DProj._22 = 2.0f / fTmB;
        kD3DProj._32 = -fTpB / fTmB;
        kD3DProj._42 = 0.0f;

        kD3DProj._13 = 0.0f;
        kD3DProj._23 = 0.0f;
        kD3DProj._33 = kFrustum.m_fFar * fInvFmN;
        kD3DProj._43 = -(kFrustum.m_fNear * kFrustum.m_fFar * fInvFmN);
    }

    // A "w-friendly" projection matrix to make fog, w-buffering work
    //kD3DProj._14 = 0.0f;
    //kD3DProj._24 = 0.0f;
    //kD3DProj._34 = 1.0f;
    //kD3DProj._44 = 0.0f;
    //pkD3D->SetTransform(D3DTS_PROJECTION, &kD3DProj);


    pkD3D->SetFVF(D3DFVF_XYZ);
    pkD3D->SetTexture(0, NULL);
    pkD3D->SetTexture(1, NULL);

    pkD3D->DrawPrimitiveUP(D3DPT_TRIANGLELIST, m_uiTriCount, m_pkVertex, sizeof(NiPoint3));
}