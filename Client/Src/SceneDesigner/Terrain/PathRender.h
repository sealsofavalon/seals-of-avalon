#ifndef PATH_RENDER_H
#define PATH_RENDER_H

#include "TerrainLibType.h"

class TERRAIN_ENTRY CPathRender : public NiRefObject
{
private:
    NiPoint3 m_kLastVertex;
    NiPoint3* m_pkVertex;
    unsigned int m_uiTriCount;
    unsigned int m_uiMaxTriCount;
    NiColorA m_kColor;

public:
    CPathRender();
    virtual ~CPathRender();

    void Clear();
    void AddVertex(const NiPoint3& kVertex, bool bNewPath);

    void SetColor(const NiColorA& kColor) { m_kColor = kColor; }
    void Render(NiCamera* pkCamera);
};

#endif