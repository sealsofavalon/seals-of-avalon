
#include "TerrainPCH.h"
#include "Collision.h"
#include "Map.h"
#include "Player.h"
#include "ShadowGeometry.h"

#define MAX_ITERATIONS 4

static const NiUniqueID kPlayerUniqueID = NiUniqueID(0xFF,0x83,0x43,0x23,0xFD,0x24,0xA6,0x47,0xA8,0x78,0x2,0xC4,0x3E,0x7E,0xCD,0xBD);

CPlayer* CPlayer::ms_pkThis = NULL;
CPlayer::CPlayer()
	:NiGeneralEntity("Proxy Player", kPlayerUniqueID, 2)
	,m_ePhysics(PHYS_Walking)
	,m_fSpeed(DEFAULT_BASE_SPEED)
	,m_fBaseSpeed(DEFAULT_BASE_SPEED)
	,m_fLastTime(0.f)
	,m_eCameraMode(FREE_MODE)
	,m_pkActor(NULL)
	,m_fCameraDistance(8.f)
	,m_bAutoMove(false)
	,m_bWalkMode(false)
	,m_bFollowCameraDirection(true)
	,m_bNeedUpdateActor(false)
	,m_fStandTime(0.f)
	,m_kStandSequenceId(NiActorManager::INVALID_SEQUENCE_ID)
	,m_kRunSequenceId(NiActorManager::INVALID_SEQUENCE_ID)
	,m_kWaitSequenceId(NiActorManager::INVALID_SEQUENCE_ID)
	,m_kWalkSequenceId(NiActorManager::INVALID_SEQUENCE_ID)
	,m_kJumpStartSequenceId(NiActorManager::INVALID_SEQUENCE_ID)
	,m_kJumpFallSequenceId(NiActorManager::INVALID_SEQUENCE_ID)
	,m_kJumpEndSequenceId(NiActorManager::INVALID_SEQUENCE_ID)
	,m_kJumpEndRunSequenceId(NiActorManager::INVALID_SEQUENCE_ID)
	,m_pkShadowGeometry(NULL)
    ,m_kDir(NiPoint3::ZERO)
	,m_bFlying(false)
	,m_bMoveBackward(false)
{
	m_pkTransformationComponent = NiNew NiTransformationComponent;
	m_pkTransformationComponent->AddReference();
	AddComponent(m_pkTransformationComponent, false);

	char acPath[NI_MAX_PATH];
	char acFilePath[NI_MAX_PATH];
	NiPath::GetExecutableDirectory(acPath, NI_MAX_PATH);
	NiSprintf(acFilePath, NI_MAX_PATH, "%s\\..\\..\\Data\\Player\\player.kfm", acPath);
	NiPath::RemoveDotDots(acFilePath);

	m_pkActorComponent = NiNew NiActorComponent;
	m_pkActorComponent->AddReference();
	SetKfmFilePath(acFilePath);
	AddComponent(m_pkActorComponent, false);

	m_kSceneRootPointerName = NiFixedString("Scene Root Pointer");

	m_fLastWaterEffectTime = 0.f;

	NIASSERT( ms_pkThis == NULL );
	ms_pkThis = this;
}

CPlayer::~CPlayer()
{
	m_pkActorComponent->RemoveReference();
	m_pkTransformationComponent->RemoveReference();

	if(m_pkShadowGeometry)
		NiDelete m_pkShadowGeometry;

	ms_pkThis = NULL;
}

void CPlayer::SetKfmFilePath(const char* pcKfmFilePath)
{
	m_pkActorComponent->SetKfmFilePath(pcKfmFilePath);
	m_bNeedUpdateActor = true;
}

const char* CPlayer::GetKfmFilePath() const
{
	return m_pkActorComponent->GetKfmFilePath();
}

void CPlayer::SetSpeed(float fSpeed)
{
	m_fSpeed = fSpeed;

	if(m_pkActor)
		PlayAnimation(m_pkActor->GetCurAnimation());
}

void CPlayer::SetBaseSpeed(float fSpeed)
{
	m_fBaseSpeed = fSpeed;
	if(m_pkActor)
		PlayAnimation(m_pkActor->GetCurAnimation());
}

void CPlayer::Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager)
{
	NiGeneralEntity::Update(pkParentEntity, fTime, pkErrors, pkAssetManager);

	if( m_bNeedUpdateActor )
	{
		m_pkActor = m_pkActorComponent->GetActorManager();

		m_kWaitSequenceId = m_pkActor->FindSequenceID("wait");

		m_kRunSequenceId = m_pkActor->FindSequenceID("run");
		if(m_kRunSequenceId == NiActorManager::INVALID_SEQUENCE_ID)
			m_kRunSequenceId = m_kWaitSequenceId;

		m_kStandSequenceId = m_pkActor->FindSequenceID("stand");
		if(m_kStandSequenceId == NiActorManager::INVALID_SEQUENCE_ID)
			m_kStandSequenceId = m_kWaitSequenceId;

		m_kWalkSequenceId = m_pkActor->FindSequenceID("walk");
		if(m_kWalkSequenceId == NiActorManager::INVALID_SEQUENCE_ID)
			m_kWalkSequenceId = m_kWaitSequenceId;

		m_kJumpStartSequenceId = m_pkActor->FindSequenceID("jump_start");
		if(m_kJumpStartSequenceId == NiActorManager::INVALID_SEQUENCE_ID)
			m_kJumpStartSequenceId = m_kWaitSequenceId;

		m_kJumpFallSequenceId = m_pkActor->FindSequenceID("jump_fly");
		if(m_kJumpFallSequenceId == NiActorManager::INVALID_SEQUENCE_ID)
			m_kJumpFallSequenceId = m_kWaitSequenceId;

		m_kJumpEndSequenceId = m_pkActor->FindSequenceID("jump_fall");
		if(m_kJumpEndSequenceId == NiActorManager::INVALID_SEQUENCE_ID)
			m_kJumpEndSequenceId = m_kWaitSequenceId;

		m_kJumpEndRunSequenceId = m_pkActor->FindSequenceID("jump_fall_run");
		if(m_kJumpEndRunSequenceId == NiActorManager::INVALID_SEQUENCE_ID)
			m_kJumpEndRunSequenceId = m_kWaitSequenceId;

		m_kWalkBackSequenceId = m_pkActor->FindSequenceID("Backward01");
		if(m_kWalkBackSequenceId == NiActorManager::INVALID_SEQUENCE_ID)
			m_kWalkBackSequenceId = m_kWaitSequenceId;
		
		m_pkActor->SetCallbackObject(this);

		m_kSequenceList.RemoveAll();
		NiKFMTool* pkKFMTool = m_pkActor->GetKFMTool();
		if(pkKFMTool)
		{
			unsigned int* puiSequenceIDs;
			unsigned int uiNumIDs;
			pkKFMTool->GetSequenceIDs(puiSequenceIDs, uiNumIDs);

			for(unsigned int ui = 0; ui < uiNumIDs; ++ui)
			{
				if(m_pkActor->GetSequence(puiSequenceIDs[ui]))
				{
					m_pkActor->RegisterCallback(NiActorManager::END_OF_SEQUENCE, puiSequenceIDs[ui]);
					m_kSequenceList.Add(m_pkActor->GetSequence(puiSequenceIDs[ui]));
				}
			}

			NiFree(puiSequenceIDs);
		}

		m_kStateMachine.GoToState(STATE_STAND);
		CMap* pkMap = CMap::Get();
		if( pkMap )
		{
			((NiNode*)m_pkActor->GetActorRoot())->AttachEffect(pkMap->GetLight());
			((NiNode*)m_pkActor->GetActorRoot())->UpdateEffects();
		}

		if(m_pkShadowGeometry == NULL)
			m_pkShadowGeometry = CShadowGeometry::Create();

		if(m_pkShadowGeometry)
			m_pkShadowGeometry->ResetCaster(m_pkActor->GetNIFRoot());

		m_bNeedUpdateActor = false;
	}

	float fTimeDelta = fTime - m_fLastTime;
	m_fLastTime = fTime;

	if( fTimeDelta > 0.1f )
		fTimeDelta = 0.1f;
    else if( fTimeDelta < 0.f )
        fTimeDelta = 0.f;

	UpdateDirection(fTimeDelta);
	PerformPhysics(fTimeDelta);
	PerformAnimation(fTimeDelta);

	if(m_pkShadowGeometry)
		m_pkShadowGeometry->Click();

	UpdateWaterLevel();
	m_kWaterEffectMgr.UpdateEffects();
}

void CPlayer::UpdateDirection(float fTimeDelta)
{
	m_kDir.z += m_kMove.yaw*fTimeDelta;
	while( m_kDir.z > float(NI_PI*2) )
		m_kDir.z -= float(NI_PI*2);
	while( m_kDir.z < 0.f )
		m_kDir.z += float(NI_PI*2);
}

#define	OVERCLIP		1.1f
void ClipPlane( const NiPoint3& in, const NiPoint3& normal, NiPoint3& out) 
{
	float backoff = in.Dot(normal);
	if( backoff < 0.f )
	{
		backoff *= OVERCLIP;
	}
	else
	{
		backoff *= (1 / OVERCLIP);
	}

	out = in - backoff*normal;
}

void CPlayer::CalcVelocity()
{
	NiMatrix3 kMat;
	kMat.FromEulerAnglesXYZ(0.f, 0.f, m_kDir.z);

	NiPoint3 kDVector, kUVector, kRVector;
	kMat.GetCol(1, kDVector);
	kMat.GetCol(2, kUVector);
	kMat.GetCol(0, kRVector);

	kDVector.Unitize();
	kRVector.Unitize();

	m_kVelocity = kDVector*m_kMove.y + kRVector*m_kMove.x;
	m_kVelocity.Unitize();
	m_kVelocity *= m_fSpeed;
}

void CPlayer::PerformPhysics(float fTimeDelta)
{
	StartNewPhysics(fTimeDelta, 0);
}

void CPlayer::StartNewPhysics(float fTimeDelta, int iIterations)
{
	if( fTimeDelta < 0.0003f )
		return;

	switch(m_ePhysics)
	{
	case PHYS_Walking: PhysWalking(fTimeDelta, iIterations);break;
	case PHYS_Falling: PhysFalling(fTimeDelta, iIterations);break;
	case PHYS_Flying:  PhysFlying(fTimeDelta, iIterations);break;
	}
}

void CPlayer::StepUp(NiPoint3& start, const NiPoint3& kDesireDir, NiPoint3 delta, Result& result)
{

	CMap* pkMap = CMap::Get();

	NiPoint3 extent(EXTENT);
	NiPoint3 down(0.f, 0.f, -MAX_STEP);
	
	if( NiAbs( result.kNormal.z ) < MIN_WALK_NORMAL )
	{
		NiPoint3 oldstart = start;
		Result oldResult = result;

		// step up - treat as vertical wall
		start.z += MAX_STEP;
		pkMap->MoveObject(start, delta, extent, result);
        if(result.fTime > 0.f)
        {
		    pkMap->MoveObject(start, down, extent, result);
            if( result.fTime < 1.f && NiAbs( result.kNormal.z ) < MIN_WALK_NORMAL )
            {
                start = oldstart;
                result = oldResult;
            }
        }
        else
        {
            start = oldstart;
            result = oldResult;
        }
	}
	else if( result.kNormal.z >= MIN_WALK_NORMAL )
	{

		// walkable slope - slide up it
		float dist = delta.Length() + 0.05f;
		//需要保持xy平面方向不变
		pkMap->MoveObject(start, delta + NiPoint3(0.f, 0.f, dist*result.kNormal.z), extent, result);

		if( result.fTime < 1.f && NiAbs( result.kNormal.z ) < MIN_WALK_NORMAL )
		{
			NiPoint3 oldstart = start;
			Result oldresult = result;

			start.z += MAX_STEP;
			pkMap->MoveObject(start, delta, extent, result);
			pkMap->MoveObject(start, down, extent, result);
			if( result.fTime < 1.f && NiAbs( result.kNormal.z ) < MIN_WALK_NORMAL )
			{
				start = oldstart;
				result = oldresult;
			}
		}
	}

	NiPoint3 n1, n2, n3, n4;

	if( result.fTime < 1.f )
	{
		n1 = result.kNormal;

		//adjust and try again
		result.kNormal.z = 0.f; // treat barrier as vertical;
		result.kNormal.Unitize();
		NiPoint3 kOriginalDelta = delta;
		NiPoint3 kOriginalNormal = result.kNormal;

		ClipPlane(delta, result.kNormal, delta);
		delta *= (1.f - result.fTime);
		if( delta.Dot(kOriginalDelta) >= 0.f )
		{
			pkMap->MoveObject(start, delta, extent, result);
			n2 = result.kNormal;
			if( result.fTime < 1.f )
			{
				//two wall adjust
				if( kOriginalNormal.Dot(result.kNormal) <= 0.f ) //90 or less corner, so use cross product for dir
				{
					NiPoint3 newDir = result.kNormal.UnitCross(kOriginalNormal);
					delta = delta.Dot(newDir) * newDir * (1.f - result.fTime);
					if( kDesireDir.Dot(delta) < 0.f )
						delta = -delta;
				}
				else //adjust to new wall
				{
					ClipPlane(delta, result.kNormal, delta);
					delta *= (1.f - result.fTime);

					if( kDesireDir.Dot(delta) < 0.f )
						delta = NiPoint3(0.f, 0.f, 0.f);
				}

				pkMap->MoveObject(start, delta, extent, result);
			}
		}
	}
}

//Walking
void CPlayer::PhysWalking(float fTimeDelta, int iIterations)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	CalcVelocity();

	NiPoint3 extent(EXTENT);
	NiPoint3 kOldLocation = m_pkTransformationComponent->GetTranslation();
	kOldLocation.z += extent.z;
	NiPoint3 start = kOldLocation;

	float fTimeRemaining = fTimeDelta;
	NiPoint3 kDesireMove = m_kVelocity;
	NiPoint3 kDesireDir = m_kVelocity;
	kDesireDir.Unitize();

	NiPoint3 delta;
	Result result;
	while( fTimeRemaining > 0.f && iIterations < MAX_ITERATIONS )
	{

		if ( IsFlying()  )
		{
			SetPhysics(PHYS_Flying );
			StartNewPhysics(fTimeRemaining, iIterations);

			return;
		}

		++iIterations;

		float fTimeTick = (fTimeRemaining > 0.05f) ? NiMin(0.05f, fTimeRemaining * 0.5f) : fTimeRemaining;
		fTimeRemaining -= fTimeTick;

		delta = fTimeTick * kDesireMove;

		if( NiAbs(delta.x) < 0.0001
		 && NiAbs(delta.y) < 0.0001
		 && NiAbs(delta.z) < 0.0001 )
		{
			fTimeRemaining = 0.f;
		}
		else
		{
			pkMap->MoveObject(start, delta, extent, result);
			if( result.fTime < 1.f )
			{
				StepUp(start, kDesireDir, delta * (1.f - result.fTime), result); 
			}
		}

		//drop to floor
		NiPoint3 down(0.f, 0.f, -MAX_STEP);
		pkMap->MoveObject(start, down, extent, result);

		if( result.fTime < 1.f )
		{
			if( result.kNormal.z < MIN_WALK_NORMAL )
			{
				// slide down slope
				NiPoint3 slide(0.f, 0.f, -MAX_STEP*0.2f);
				ClipPlane(slide, result.kNormal, slide);
				pkMap->MoveObject(start, slide, extent, result);
			}
		}
		else
		{
			//Falling
			m_kVelocity = (start - kOldLocation)/fTimeDelta;
			m_kVelocity.z = 0.f;

			start = CheckMoveable(kOldLocation, start, false);
			start.z -= extent.z;
			SetPosition(start, m_kDir);

			SetPhysics(PHYS_Falling);
			StartNewPhysics(fTimeRemaining, iIterations);
			return;
		}
	}

	start = CheckMoveable(kOldLocation, start, false);
	start.z -= extent.z; 
	SetPosition(start, m_kDir);

	m_kVelocity = (start - kOldLocation)/fTimeDelta;
}

NiPoint3 CPlayer::NewFallVelocity(const NiPoint3& kVelocity, const NiPoint3& kAcceleration, float fTimeTick) const
{
	return kVelocity + fTimeTick * kAcceleration * 2.f;
}


//Flying
void CPlayer::PhysFlying(float fTimeDelta, int iIterations)
{
	CMap* pkMap = CMap::Get();
	
	CalcVelocity();

	NiPoint3 extent(EXTENT);
	NiPoint3 kOldLocation = m_pkTransformationComponent->GetTranslation();
	NiPoint3 start = kOldLocation;

	float fTimeRemaining = fTimeDelta;
	//	NiPoint3 kDesireMove = m_kInternelVel;
	NiPoint3 kDesireDir = m_kVelocity;
	kDesireDir.Unitize();

	NiPoint3 delta;
	Result result;
	while( fTimeRemaining > 0.f && iIterations < MAX_ITERATIONS )
	{

		if ( !IsFlying() )
		{
			SetPhysics( PHYS_Walking );
			StartNewPhysics(fTimeRemaining, iIterations);
			return;
		}
		++iIterations;

		float fTimeTick = (fTimeRemaining > 0.05f) ? NiMin(0.05f, fTimeRemaining * 0.5f) : fTimeRemaining;
		fTimeRemaining -= fTimeTick;

		delta = fTimeTick * m_kVelocity;

		if( NiAbs(delta.x) < 0.0001
			&& NiAbs(delta.y) < 0.0001
			&& NiAbs(delta.z) < 0.0001 )
		{
			fTimeRemaining = 0.f;
		}
		else
		{
			pkMap->MoveObject(start, delta, extent, result);
			if( result.fTime < 1.f )
			{
				// collision horizon
				/*if (result.pkCollObject != 0)
				{
				}
				*/
				StepUp(start, kDesireDir, delta * (1.f - result.fTime), result); 
				//SetPhyFly(false);
			}
		}

		//drop to floor
		/*NiPoint3 down(0.f, 0.f, 0.f );
		pkMap->MoveObject(start, down, extent, result);*/

	}

	start = CheckMoveable(kOldLocation, start, false);

	//start.z -= extent.z;


	if ( start.z > 750 )
	{
     		start.z = 750;
	}

	SetPosition(start, m_kDir);
 
	m_kVelocity = (start - kOldLocation)/fTimeDelta;
}

//Falling
void CPlayer::PhysFalling(float fTimeDelta, int iIterations)
{
	CMap* pkMap = CMap::Get();
	float fTimeRemaining = fTimeDelta;

	NiPoint3 start = m_pkTransformationComponent->GetTranslation();
	const NiPoint3 extent(EXTENT);
	start.z += extent.z;

	NiPoint3 kOldLocation;
	NiPoint3 kAdjusted;
	Result result;
	while( fTimeRemaining > 0.f && iIterations < MAX_ITERATIONS )
	{

		if ( IsFlying()  )
		{
			SetPhysics(PHYS_Flying );
			StartNewPhysics(fTimeRemaining, iIterations);
			return;
		}

		iIterations++;

        float fTimeTick = (fTimeRemaining > 0.05f) ? NiMin(0.05f, fTimeRemaining * 0.5f) : fTimeRemaining;
		fTimeRemaining -= fTimeTick;

		m_kVelocity = NewFallVelocity(m_kVelocity, GRAVITY, fTimeTick);
		
        kAdjusted = m_kVelocity*fTimeTick;
        kOldLocation = start;

		pkMap->MoveObject(start, kAdjusted, extent, result);
		if( result.fTime < 1.f )
		{
			if( result.kNormal.z >= MIN_WALK_NORMAL )
			{
				fTimeRemaining = fTimeTick * (1.f - result.fTime);
				//if( result.fTime > 0.1f && result.fTime * fTimeTick > 0.003f )
				//	m_kVelocity = (start - kOldLocation)/(result.fTime * fTimeTick);
				SetPhysics(PHYS_Walking);
				m_kStateMachine.FireEvent(EVENT_JUMP_END);

				start = CheckMoveable(kOldLocation, start, true);
				start.z -= extent.z;
				SetPosition(start, m_kDir);

				StartNewPhysics(fTimeRemaining, iIterations);
				return;
			}
			else
			{
				NiPoint3 kOldNormal = result.kNormal;
				NiPoint3 delta;
				ClipPlane(kAdjusted, result.kNormal, delta);
				delta *= (1.f - result.fTime);

				if( delta.Dot(kAdjusted) >= 0.f )
				{
					pkMap->MoveObject(start, delta, extent, result);
					if( result.fTime < 1.f ) //hit second wall
					{
						if( result.kNormal.z >= MIN_WALK_NORMAL )
						{
							SetPhysics(PHYS_Walking);
							m_kStateMachine.FireEvent(EVENT_JUMP_END);

							start = CheckMoveable(kOldLocation, start, true);
							start.z -= extent.z;
							SetPosition(start, m_kDir);
							return;
						}

						NiPoint3 kDesiredDir = kAdjusted;
						kDesiredDir.Unitize();

						if( kOldNormal.Dot(result.kNormal) <= 0.f ) //90 or less corner, so use cross product for dir
						{
							NiPoint3 kNewDir = result.kNormal.UnitCross(kOldNormal);

							delta = delta.Dot(kNewDir)*(1.f - result.fTime)*kNewDir;
							if( kDesiredDir.Dot(delta) < 0.f )
								delta = -delta;
						}
						else
						{
							ClipPlane(delta, result.kNormal, delta);
							delta *= (1.f - result.fTime);
							if( delta.Dot(kDesiredDir) <= 0.f )
								delta = NiPoint3(0.f, 0.f, 0.f);
						}

						pkMap->MoveObject(start, delta, extent, result);
						if(result.kNormal.z >= MIN_WALK_NORMAL )
						{
							SetPhysics(PHYS_Walking);
							m_kStateMachine.FireEvent(EVENT_JUMP_END);

							start = CheckMoveable(kOldLocation, start, true);
							start.z -= extent.z;
							SetPosition(start, m_kDir);
							return;
						}

						if( result.fTime < 1.f )
						{
							ClipPlane(delta, result.kNormal, delta);
							delta *= (1.f - result.fTime);
							pkMap->MoveObject(start, delta, extent, result);

							if( result.kNormal.z >= MIN_WALK_NORMAL )
							{
								SetPhysics(PHYS_Walking);
								m_kStateMachine.FireEvent(EVENT_JUMP_END);

								start = CheckMoveable(kOldLocation, start, true);
								start.z -= extent.z;
								SetPosition(start, m_kDir);
								return;
							}
						}
					}
				}
			}
		}

		float fOldZ = m_kVelocity.z;
		m_kVelocity = (start - kOldLocation)/fTimeTick;
		m_kVelocity.z = fOldZ;
	}

	start = CheckMoveable(m_pkTransformationComponent->GetTranslation(), start, true);
	start.z -= extent.z;
	SetPosition(start, m_kDir);
}

void CPlayer::SetPosition(const NiPoint3& pos, const NiPoint3& rot)
{
    CMap* pkMap = CMap::Get();
    NIASSERT(pkMap);

	m_kDir = rot;

	NiMatrix3 kMat;
	kMat.FromEulerAnglesXYZ(0.f, 0.f, m_kDir.z);

	m_pkTransformationComponent->SetTranslation(pos);
	m_pkTransformationComponent->SetRotation(kMat);
}

void CPlayer::SetScale(const float fScale)
{
	m_pkTransformationComponent->SetScale(fScale);
}

//检查可移动性
NiPoint3 CPlayer::CheckMoveable(const NiPoint3& kFrom, const NiPoint3& kTo, bool bJump) const
{
	CMap* pkMap = CMap::Get();
	NIASSERT(pkMap);

	//是否可以行走
	if(pkMap->GetMoveable(kTo.x, kTo.y))
		return kTo;

	if(!bJump)
		return kFrom; //退回原地

	NiPoint3 kStart = kFrom;
	NiPoint3 kEnd = kFrom;
	kEnd.z = kTo.z;

	Result kResult;
	pkMap->MoveObject(kStart, kEnd - kStart, EXTENT, kResult);
	kStart.x = kFrom.x;
	kStart.y = kFrom.y;
	return kStart;
}

void CPlayer::PerformAnimation(float fTimeDelta)
{
	m_kStateMachine.Update(fTimeDelta);
}

void CPlayer::PlayAnimation(NiActorManager::SequenceID eSequenceId)
{
	m_pkActorComponent->SetActiveSequenceID(eSequenceId);
	if(m_pkActor && m_pkActor->GetSequence(eSequenceId))
		m_pkActor->GetSequence(eSequenceId)->SetFrequency(m_fSpeed/m_fBaseSpeed);
}

void CPlayer::ResetAnimation(NiActorManager::SequenceID eSequenceId)
{
	NiControllerSequence* pkControllerSequence = m_pkActor->GetSequence(eSequenceId);
	if(pkControllerSequence && pkControllerSequence->GetCycleType() != NiTimeController::LOOP)
		pkControllerSequence->ResetSequence();
}

void CPlayer::StartForward()
{
    m_kMove.y = -1.f;
	m_bMoveBackward = false;
	m_kStateMachine.FireEvent(EVENT_MOVE_BEGIN);
}

void CPlayer::StartBackward()
{
    m_kMove.y = 1.f;
	m_bMoveBackward = true;
	m_kStateMachine.FireEvent(EVENT_MOVE_BEGIN);
}

void CPlayer::Stop()
{
	if( !m_bAutoMove )
		m_kMove.y = 0.f;

	if(!IsMoving())
		m_kStateMachine.FireEvent(EVENT_MOVE_END);
}

void CPlayer::StartStrafeLeft()
{
    m_kMove.x = 1.f;
	m_kStateMachine.FireEvent(EVENT_MOVE_BEGIN);
}

void CPlayer::StartStrafeRight()
{
    m_kMove.x = -1.f;
	m_kStateMachine.FireEvent(EVENT_MOVE_BEGIN);
}

void CPlayer::StopStrafe()
{
    m_kMove.x = 0.f;
	if(!IsMoving())
		m_kStateMachine.FireEvent(EVENT_MOVE_END);
}

void CPlayer::StartTurnLeft()
{
    m_kMove.yaw = -4.f;
}

void CPlayer::StartTurnRight()
{
    m_kMove.yaw = 4.f;
}

void CPlayer::StopTurn()
{
    m_kMove.yaw = 0.f;
}

void CPlayer::SetFacing(float rot)
{
	SetPosition(m_pkTransformationComponent->GetTranslation(), NiPoint3(0.f, 0.f, rot));
}

float CPlayer::GetFacing() const
{
	return m_kDir.z;
}

void CPlayer::Jump()
{
	if( !IsJumping() )
	{
		SetPhysics(PHYS_Falling);
		m_kVelocity.z = 8.f;
		m_kStateMachine.FireEvent(EVENT_JUMP_START);
	}
}

void CPlayer::SetCameraMode(ECameraMode eCameraMode)
{
	m_eCameraMode = eCameraMode;
	if( m_eCameraMode == FREE_MODE )
	{
		m_pkActor->GetNIFRoot()->SetAppCulled(true);
		SetPhysics(PHYS_None);
	}
	else
	{
		m_pkActor->GetNIFRoot()->SetAppCulled(false);
		SetPhysics(PHYS_Walking);
	}
}

void CPlayer::SetCameraDistance(float fDistance)
{
	m_fCameraDistance = NiClamp(fDistance, 2.f, 36.f);
}

void CPlayer::StartAutoMove()
{
	m_bAutoMove = true;
	StartForward();
}

void CPlayer::StopAutoMove()
{
	m_bAutoMove = false;
	Stop();
}

void CPlayer::SetCustomSequence(const char* pcSequenceName)
{
	m_kStateMachine.FireEvent(EVENT_CUSTOM_ANIMATION);
	m_kStateMachine.FireEvent(EVENT_PLAY_ANIMATION, m_pkActor->FindSequenceID(pcSequenceName));
}

void CPlayer::EndOfSequence(NiActorManager* pkManager, NiActorManager::SequenceID eSequenceID, float fCurrentTime, float fEventTime)
{
	m_kStateMachine.FireEvent(EVENT_ANIMATION_END, eSequenceID);
}

NiAVObject* CPlayer::GetSceneRoot()
{
    if(m_pkActor)
        return m_pkActor->GetNIFRoot();

    return NULL;
}

void CPlayer::DrawShadow(CCullingProcess& kCuller)
{
	if(m_pkShadowGeometry)
		m_pkShadowGeometry->GetShadowGeometry()->Cull(kCuller);
}

void CPlayer::DrawWaterEffects(CCullingProcess& kCuller)
{
	m_kWaterEffectMgr.DrawEffects(kCuller);
}

void CPlayer::UpdateWaterLevel()
{
	CMap* pkMap = CMap::Get();
	if(pkMap == NULL)
		return;

	float fTime = pkMap->GetTime();

	NiPoint3 kPos = GetPosition();

	float fWaterHeight;
	if(!pkMap->GetWaterHeight(kPos.x, kPos.y, fWaterHeight))
		return;

	if( fWaterHeight < kPos.z )
		return;

	kPos.z = fWaterHeight;
	if(IsMoving())
	{ 
		if(m_fLastWaterEffectTime + MAX_WAKE_LIFE/8.f < fTime)
		{
			m_fLastWaterEffectTime = fTime;

			float fRot = m_kDir.z;
			if(m_kMove.y < 0.f)
			{
				if(m_kMove.x < 0.f)
					fRot += NI_PI / 4.f;
				else if(m_kMove.x > 0.f)
					fRot -= NI_PI / 4.f;
			}
			else if(m_kMove.y > 0.f)
			{
				fRot += NI_PI;
				if(m_kMove.x < 0.f)
					fRot += NI_PI / 4.f;
				else if(m_kMove.x > 0.f)
					fRot -= NI_PI / 4.f;
			}
			else
			{
				if(m_kMove.x < 0.f)
					fRot += NI_PI / 2.f;
				else if(m_kMove.x > 0.f)
					fRot -= NI_PI / 2.f;
			}
			
			NiMatrix3 kRot;
			kRot.MakeZRotation(fRot);
			m_kWaterEffectMgr.CreateEffect(kPos, kRot, true);
		}
	}
	else
	{
		if(m_fLastWaterEffectTime + MAX_SPLASH_LIFE < fTime)
		{
			m_fLastWaterEffectTime = fTime;
			m_kWaterEffectMgr.CreateEffect(kPos, m_pkTransformationComponent->GetRotation(), false);
		}
	}
}

CPlayer* CPlayer::Get() { return ms_pkThis; }