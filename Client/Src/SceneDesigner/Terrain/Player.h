#ifndef PLAYER_H
#define PLAYER_H

#include "TerrainLibType.h"
#include "Collision.h"
#include "StateMachine.h"
#include "CullingProcess.h"
#include "WaterEffect.h"

#define MAX_STEP (0.5f)
#define MIN_WALK_NORMAL (0.7f)
#define GRAVITY (NiPoint3(0.f, 0.f, -10.f))
#define EXTENT (NiPoint3(0.3f, 0.3f, 0.8f))
#define DEFAULT_BASE_SPEED (8.f)

struct Move
{
	Move()
		:x(0.f), y(0.f), z(0.f)
		,yaw(0.f), pitch(0.f), roll(0.f)
	{}

	float x, y, z;          // float -1 to 1
	float yaw, pitch, roll; // 0-2PI
};

enum EPhysics
{
    PHYS_None               =0,
    PHYS_Walking            =1,
    PHYS_Falling            =2,
    PHYS_Swimming           =3,
    PHYS_Flying             =4,
    PHYS_Rotating           =5,
    PHYS_Projectile         =6,
    PHYS_Interpolating      =7,
    PHYS_MovingBrush        =8,
    PHYS_Spider             =9,
    PHYS_Trailer            =10,
    PHYS_Ladder             =11,
    PHYS_RootMotion         =12,
    PHYS_MAX                =13,
};

enum ECameraMode
{
	PLAYER_MODE,
	FREE_MODE,
};

class CShadowGeometry;
class TERRAIN_ENTRY CPlayer : public NiGeneralEntity
	                        , public NiActorManager::CallbackObject
{
private:
	NiActorComponent* m_pkActorComponent;
	NiTransformationComponent* m_pkTransformationComponent;
	
	NiActorManager* m_pkActor;
	NiActorManager::SequenceID m_kStandSequenceId;
	NiActorManager::SequenceID m_kRunSequenceId;
	NiActorManager::SequenceID m_kWaitSequenceId;
	NiActorManager::SequenceID m_kWalkSequenceId;
	NiActorManager::SequenceID m_kWalkBackSequenceId;
	NiActorManager::SequenceID m_kJumpStartSequenceId;
	NiActorManager::SequenceID m_kJumpFallSequenceId;
	NiActorManager::SequenceID m_kJumpEndSequenceId;
	NiActorManager::SequenceID m_kJumpEndRunSequenceId;

	NiTPrimitiveArray<NiControllerSequence*> m_kSequenceList;

	float m_fStandTime;

	Move m_kMove;
	NiPoint3 m_kVelocity;
	float m_fSpeed;
	float m_fBaseSpeed;
	EPhysics m_ePhysics;
	NiPoint3 m_kDir;
	float m_fLastTime;
	float m_fCameraDistance;
	bool m_bAutoMove;
	bool m_bWalkMode;
	bool m_bFollowCameraDirection;

	bool m_bFlying;
	bool  m_bPhyFly;
	bool m_bMoveBackward;

	bool m_bNeedUpdateActor;

	ECameraMode m_eCameraMode;
	NiFixedString m_kSceneRootPointerName;
	static CPlayer* ms_pkThis;

	CStateMachine m_kStateMachine;

	//��Ӱ
	CShadowGeometry* m_pkShadowGeometry;

	//
	CWaterEffectMgr m_kWaterEffectMgr;
	float m_fLastWaterEffectTime;

public:
	CPlayer();
	virtual ~CPlayer();

	void SetKfmFilePath(const char* pcKfmFilePath);
	const char* GetKfmFilePath() const;
	void SetSpeed(float fSpeed);
	float GetSpeed() const { return m_fSpeed; }
	void SetBaseSpeed(float fSpeed);
	float GetBaseSpeed() const { return m_fBaseSpeed; } 
	virtual void Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager);

	void UpdateDirection(float fTime);
	void CalcVelocity();
	void PerformPhysics(float fTimeDelta);
	void PerformAnimation(float fTimeDelta);
	void PlayAnimation(NiActorManager::SequenceID eSequenceId);
	void ResetAnimation(NiActorManager::SequenceID eSequenceId);
	void PlayStandAnimation() { PlayAnimation(m_kStandSequenceId); }
	void PlayRunAnimation() { PlayAnimation(m_kRunSequenceId); }
	void PlayWaitAnimation() { PlayAnimation(m_kWaitSequenceId); }
	void PlayWalkAnimation() { PlayAnimation(m_kWalkSequenceId); }
	void PlayBackwardAnimation() { PlayAnimation(m_kWalkBackSequenceId); }
	void PlayJumpStartAnimation() { PlayAnimation(m_kJumpStartSequenceId); }
	void PlayJumpFallAnimation() { PlayAnimation(m_kJumpFallSequenceId); }
	void PlayJumpEndAnimation() { PlayAnimation(m_kJumpEndSequenceId); }
	void PlayJumpEndRunAnimation() { PlayAnimation(m_kJumpEndRunSequenceId); }

	void StartNewPhysics(float fTimeDelta, int iIterations);
	void StepUp(NiPoint3& start, const NiPoint3& kDesireDir, NiPoint3 delta, Result& result);
	void PhysWalking(float fTimeDelta, int iIterations);
	NiPoint3 NewFallVelocity(const NiPoint3& kVelocity, const NiPoint3& kAcceleration, float fTimeTick) const;
	void PhysFalling(float fTimeDelta, int iIterations);
	void PhysFlying(float fTimeDelta, int iIterations);

	NiPoint3 CheckMoveable(const NiPoint3& kFrom, const NiPoint3& kTo, bool bJump) const;
	void SetPosition(const NiPoint3& pos, const NiPoint3& rot);
	const NiPoint3& GetPosition() const { return m_pkTransformationComponent->GetTranslation(); }
	void SetScale(const float fScale);
	const float GetScale()	{ return m_pkTransformationComponent->GetScale(); }
	bool IsFlying()	{ return m_bFlying; }
	void SetPhysics(EPhysics phys) { m_ePhysics = phys; }
	void SetFly( bool bflying ) {
		m_bFlying = bflying;
	}

	void SetPhyFly( bool phyFly )
	{
		m_bPhyFly = phyFly;
	}

	void StartForward();
	void StartBackward();
	void Stop();
	void StartStrafeLeft();
	void StartStrafeRight();
	void StopStrafe();
	void StartTurnLeft();
	void StartTurnRight();
	void StopTurn();
	bool IsMoving() const { return m_kMove.x != 0.f || m_kMove.y != 0.f; }

	void SetWalkMode(bool bWalk) { m_bWalkMode = bWalk; }
	bool GetWalkMode() const { return m_bWalkMode; } 
	bool IsBackward()	const { return m_bMoveBackward; }

	void SetFacing(float rot);
	float GetFacing() const;

	void Jump();
	bool IsJumping() const { return m_ePhysics == PHYS_Falling; }

	void StartAutoMove();
	void StopAutoMove();
	bool IsAutoMove() const { return m_bAutoMove; }
	void SetCustomSequence(const char* pcSequenceName);

	void SetCameraMode(ECameraMode eCameraMode);
	ECameraMode GetCameraMode() const { return m_eCameraMode; }
	bool IsFreeCameraMode() const { return m_eCameraMode == FREE_MODE; }

	float GetCameraDistance() const { return m_fCameraDistance; }
	void SetCameraDistance(float fDistance);

	void ToggleFollowCameraDirection() { m_bFollowCameraDirection = !m_bFollowCameraDirection; }
	bool GetFollowCameraDirection() const { return m_bFollowCameraDirection; }

	const NiTPrimitiveArray<NiControllerSequence*>& GetSequences() const { return m_kSequenceList; }

	// Animation callbacks
    virtual void AnimActivated(NiActorManager* pkManager, NiActorManager::SequenceID eSequenceID, float fCurrentTime, float fEventTime){}
    virtual void AnimDeactivated(NiActorManager* pkManager, NiActorManager::SequenceID eSequenceID, float fCurrentTime, float fEventTime){}
	virtual void TextKeyEvent(NiActorManager* pkManager, NiActorManager::SequenceID eSequenceID, const NiFixedString& kTextKey, const NiTextKeyMatch* pkMatchObject, float fCurrentTime, float fEventTime) {}
    virtual void EndOfSequence(NiActorManager* pkManager, NiActorManager::SequenceID eSequenceID, float fCurrentTime, float fEventTime);

    NiAVObject* GetSceneRoot();

	void DrawShadow(CCullingProcess& kCuller);
	void DrawWaterEffects(CCullingProcess& kCuller);
	void UpdateWaterLevel();

	const Move& GetMove() const { return m_kMove; }

	static CPlayer* Get();
};

#define g_pkPlayer (CPlayer::Get())

#endif //PLAYER_H