#include "TerrainPCH.h"
#include "TerrainHelper.h"
#include "TerrainLight.h"
#include "Sky.h"

CSky::CSky()
	:m_fMinTime(0.f)
	,m_fMaxTime(0.f)
	,m_fTime(0.f)
{
    m_spZBuffer = NiNew NiZBufferProperty;
    m_spZBuffer->SetZBufferTest(false);
    m_spZBuffer->SetZBufferWrite(false);

    m_spMaterialProperty = NiNew NiMaterialProperty;
    m_spMaterial = NiSingleShaderMaterial::Create("FogMountain");

    m_spCuller = NiNew NiCullingProcess(NULL);
}

void CSky::Process(NiObjectNET* pkRoot)
{
	NiTimeController* pkController = pkRoot->GetControllers();
	// Gather all time controllers from this object
	while(pkController != NULL)
	{
		if(pkController->GetBeginKeyTime() < m_fMinTime)
			m_fMinTime = pkController->GetBeginKeyTime();
		if(pkController->GetEndKeyTime() > m_fMaxTime)
			m_fMaxTime = pkController->GetEndKeyTime();

		pkController = pkController->GetNext();
	}

	if(NiIsKindOf(NiAVObject, pkRoot))
	{
		// NiProperties can have time controllers, so search them too
		NiAVObject* pkObj = (NiAVObject*) pkRoot;
		NiPropertyList* pkPropList = &(pkObj->GetPropertyList());
		NiTListIterator kIter = pkPropList->GetHeadPos();
		while(pkPropList != NULL && !pkPropList->IsEmpty()&& kIter)
		{
			NiProperty* pkProperty = pkPropList->GetNext(kIter);
			if(pkProperty)
				Process(pkProperty);
		}
	}

	if(NiIsKindOf(NiNode, pkRoot))
	{
		NiNode* pkNode = (NiNode*) pkRoot;
		// Search all of the children
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ui++)
		{
			NiAVObject* pkObj = pkNode->GetAt(ui);
			Process(pkObj);
		}
		// NiDynamicEffects can have time controllers, so search them too
		const NiDynamicEffectList* pkEffectList= &(pkNode->GetEffectList());

		NiTListIterator kIter = pkEffectList->GetHeadPos();
		while(pkEffectList != NULL && !pkEffectList->IsEmpty() && kIter)
		{
			NiDynamicEffect* pkEffect = pkEffectList->GetNext(kIter);
			if(pkEffect)
				Process(pkEffect);
		}
	}
    else if(!GetEnableZBuffer() && NiIsKindOf(NiGeometry, pkRoot))
    {
        NiGeometry* pkGeometry = (NiGeometry*)pkRoot;
        NiZBufferProperty* pkZBufferProperty = (NiZBufferProperty*)pkGeometry->GetProperty(NiZBufferProperty::GetType());
        if(pkZBufferProperty)
        {
            pkZBufferProperty->SetZBufferTest(false);
            pkZBufferProperty->SetZBufferWrite(false);
        }
        else
        {
            pkGeometry->AttachProperty(m_spZBuffer);
        }
    }
}

void CSky::ProcessMountain(NiAVObject* pkAVObject)
{
	if(pkAVObject == NULL)
		return;

	if(NiIsKindOf(NiGeometry, pkAVObject))
	{
		NiMaterialProperty* pkMaterialProperty = (NiMaterialProperty*)pkAVObject->GetProperty(NiMaterialProperty::GetType());
		if(pkMaterialProperty)
			pkAVObject->DetachProperty(pkMaterialProperty);

		pkAVObject->AttachProperty(m_spMaterialProperty);
		((NiGeometry*)pkAVObject)->ApplyAndSetActiveMaterial(m_spMaterial);
	}
	else if(NiIsKindOf(NiNode, pkAVObject))
	{
		NiNode* pkNode = (NiNode*)pkAVObject;
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ++ui)
			ProcessMountain(pkNode->GetAt(ui));
	}
}

bool CSky::Load(const NiFixedString& kFilename)
{
	NiStream kStream;
	char acFilename[1024];
	NiSprintf(acFilename, sizeof(acFilename), "%s\\%s", TerrainHelper::GetClientPath(), (const char*)kFilename);
	kStream.Load(acFilename);
	if(kStream.GetObjectCount() == 0)
		return false;

	m_kFilename = kFilename;
	
	NIASSERT(NiIsKindOf(NiAVObject, kStream.GetObjectAt(0)));
	m_spSceneRoot = (NiAVObject*)kStream.GetObjectAt(0);
    m_fMinTime = m_fMaxTime = 0.f;
    Process(m_spSceneRoot);

    m_spMountain = m_spSceneRoot->GetObjectByName("Mountain");
    if(m_spMountain == NULL)
        m_spMountain = m_spSceneRoot->GetObjectByName("Crater");

    ProcessMountain(m_spMountain);

	m_spSceneRoot->SetScale(0.1f);
	
	m_spSceneRoot->Update(0.0f);
	m_spSceneRoot->UpdateProperties();
	m_spSceneRoot->UpdateEffects();

	return true;
}

void CSky::Draw(NiCamera* pkCamera, bool bReflectPass)
{
	if(m_spSceneRoot)
	{
        m_spSceneRoot->SetTranslate(pkCamera->GetWorldTranslate());
        if(!bReflectPass)
        {
            CTerrainLightManager* pkLightManager = CTerrainLightManager::Get();
            if(pkLightManager)
                m_spMaterialProperty->SetEmittance(pkLightManager->GetFogColor());

            if(m_spMountain)
                m_spMountain->SetAppCulled(false);

            m_spSceneRoot->Update(m_fTime, true);
        }
        else
        {
            if(m_spMountain)
                m_spMountain->SetAppCulled(true);

            m_spSceneRoot->Update(m_fTime, false);
        }

		m_kVisibleSet.RemoveAll();
		NiDrawScene(pkCamera, m_spSceneRoot, *m_spCuller, &m_kVisibleSet);

        if(GetEnableZBuffer())
        {
            //清除天空盒留下来的ZBuffer
            NiRenderer* pkRenderer = NiRenderer::GetRenderer();
            pkRenderer->ClearBuffer(NULL, NiRenderer::CLEAR_ZBUFFER|NiRenderer::CLEAR_STENCIL);
        }
	}
}

void CSky::Update(float fDeltaTime)
{
	if(CTerrainLightManager::Get())
	{
		unsigned int uiHour;
		unsigned int uiMinute;
		CTerrainLightManager::Get()->GetTime(uiHour, uiMinute);

		float r = float(uiHour*60 + uiMinute)/float(24*60);
		m_fTime = m_fMinTime*r + m_fMaxTime*(1.f - r);
	}
}

void CSky::SetEnableZBuffer(bool bEnableZBuffer)
{ 
    if(bEnableZBuffer) 
        m_uiFlags = SF_ENABLE_ZBUFFER;
    else
        m_uiFlags = 0;

    if(m_spSceneRoot)
        Load(m_kFilename);
}
