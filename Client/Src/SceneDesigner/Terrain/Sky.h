#ifndef SKY_H
#define SKY_H

#include "TerrainLibType.h"

class TERRAIN_ENTRY CSky : public NiMemObject
{
public:
    enum SkyFlags
    {
        SF_ENABLE_ZBUFFER = 0x01,
    };

	CSky();
    virtual ~CSky() {};

	bool Load(const NiFixedString& kFilename);

	const NiFixedString& GetFilename() const { return m_kFilename; }
	void Draw(NiCamera* pkCamera, bool bReflectPass);
	void Update(float fDeltaTime);
    bool GetEnableZBuffer() const { return (m_uiFlags & SF_ENABLE_ZBUFFER); }
    void SetEnableZBuffer(bool bEnableZBuffer);

private:
    void RunUpScene(); //没有实现, 请参考AssetViewer的实现
    void Process(NiObjectNET* pkRoot);
    void ProcessMountain(NiAVObject* pkAVObject);

private:
    NiAVObjectPtr m_spSceneRoot;
    NiAVObjectPtr m_spMountain;
    NiCullingProcessPtr m_spCuller;
    NiVisibleArray m_kVisibleSet;
    NiZBufferPropertyPtr m_spZBuffer;
    NiMaterialPropertyPtr m_spMaterialProperty;
    NiMaterialPtr m_spMaterial;
    unsigned int m_uiFlags;

    NiFixedString m_kFilename;
    float m_fMinTime;
    float m_fMaxTime;
    float m_fTime;
};

#endif //SKY_H