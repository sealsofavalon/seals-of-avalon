#include "TerrainPCH.h"

#include "SoundEmitterComponent.h"
#include "Map.h"

NiFixedString CSoundEmitterComponent::ms_kClassName;
NiFixedString CSoundEmitterComponent::ms_kComponentName;

NiFixedString CSoundEmitterComponent::ms_kTimeIntervalName;
NiFixedString CSoundEmitterComponent::ms_kSoundName;

NiFixedString CSoundEmitterComponent::ms_kLoopCountName;
NiFixedString CSoundEmitterComponent::ms_kMinDistanceName;
NiFixedString CSoundEmitterComponent::ms_kMaxDistanceName;


NiFixedString CSoundEmitterComponent::ms_kSceneRootPointerName;

NiFixedString CSoundEmitterComponent::ms_kTranslationName;

NiUniqueID CSoundEmitterComponent::ms_kTemplateID;
NiAVObject* CSoundEmitterComponent::ms_pkSceneRootTemplate;

NiFactoryDeclarePropIntf(CSoundEmitterComponent);

//---------------------------------------------------------------------------
void CSoundEmitterComponent::_SDMInit()
{
    ms_kClassName = "CSoundEmitterComponent";
    ms_kComponentName = "Sound Emitter";

    ms_kTimeIntervalName = "Time Interval";
    ms_kSoundName = "Sound Name";

    ms_kLoopCountName = "Loop Count";
    ms_kMinDistanceName = "Min Distance";
    ms_kMaxDistanceName = "Max Distance";

    ms_kSceneRootPointerName = "Scene Root Pointer";

    ms_kTranslationName = "Translation";

    ms_kTemplateID = NiUniqueID(0x9A,0xF8,0x4B,0xA6,0x6C,0x39,0x46,0x19,0xA2,0x7A,0xA6,0xAD,0x31,0xF8,0x00,0x8A);
	NiFactoryRegisterPropIntf(CSoundEmitterComponent);

    NiStream kStream;
	kStream.Load("..\\..\\Data\\sound_emitter.nif");
	NIASSERT( kStream.GetObjectCount() == 1 );
	NIASSERT(NiIsKindOf(NiNode, kStream.GetObjectAt(0)));

	ms_pkSceneRootTemplate = (NiAVObject*)kStream.GetObjectAt(0);
	ms_pkSceneRootTemplate->IncRefCount();
}

//---------------------------------------------------------------------------
void CSoundEmitterComponent::_SDMShutdown()
{
    ms_kClassName = NULL;
    ms_kComponentName = NULL;

    ms_kTimeIntervalName = NULL;
    ms_kSoundName = NULL;
    ms_kLoopCountName = NULL;
    ms_kMinDistanceName = NULL;
    ms_kMaxDistanceName = NULL;


    ms_kSceneRootPointerName = NULL;

    ms_kTranslationName = NULL;

	ms_pkSceneRootTemplate->DecRefCount();
}

CSoundEmitterComponent::CSoundEmitterComponent()
{
    m_uiTimeInterval = 5;

    m_uiLoopCount = 10000; //����ѭ��
    m_fMinDistance = 1.f;
    m_fMaxDistance = 128.f;


    m_spSceneRoot = (NiAVObject*)ms_pkSceneRootTemplate->Clone();
    m_spSceneRoot->UpdateProperties();
}


//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* CSoundEmitterComponent::Clone(bool bInheritProperties)
{
    CSoundEmitterComponent* pkSoundEmitterComponent = NiNew CSoundEmitterComponent();
    pkSoundEmitterComponent->SetTimeInterval(m_uiTimeInterval);
    pkSoundEmitterComponent->SetSoundName(m_kSoundName);
    pkSoundEmitterComponent->SetLoopCount(m_uiLoopCount);
    pkSoundEmitterComponent->SetMinDistance(m_fMinDistance);
    pkSoundEmitterComponent->SetMaxDistance(m_fMaxDistance);

    return pkSoundEmitterComponent;
}
//---------------------------------------------------------------------------
NiEntityComponentInterface* CSoundEmitterComponent::GetMasterComponent()
{
    return NULL;
}
//---------------------------------------------------------------------------
void CSoundEmitterComponent::SetMasterComponent(NiEntityComponentInterface* pkMasterComponent)
{
}
//---------------------------------------------------------------------------
void CSoundEmitterComponent::GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
    kDependentPropertyNames.Add(ms_kTranslationName);
}
//---------------------------------------------------------------------------
// NiEntityPropertyInterface overrides.
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::SetTemplateID(const NiUniqueID& kID)
{
    //Not supported for custom components
    return false;
}
//---------------------------------------------------------------------------
NiUniqueID  CSoundEmitterComponent::GetTemplateID()
{
    return ms_kTemplateID;
}
//---------------------------------------------------------------------------
void CSoundEmitterComponent::AddReference()
{
    this->IncRefCount();
}
//---------------------------------------------------------------------------
void CSoundEmitterComponent::RemoveReference()
{
    this->DecRefCount();
}
//---------------------------------------------------------------------------
NiFixedString CSoundEmitterComponent::GetClassName() const
{
    return ms_kClassName;
}
//---------------------------------------------------------------------------
NiFixedString CSoundEmitterComponent::GetName() const
{
    return ms_kComponentName;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::SetName(const NiFixedString& kName)
{
    // This component does not allow its name to be set.
    return false;
}
//---------------------------------------------------------------------------
void CSoundEmitterComponent::BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors)
{
	pkRenderingContext->m_pkCullingProcess->Process(
		pkRenderingContext->m_pkCamera, 
		m_spSceneRoot,
	    pkRenderingContext->m_pkCullingProcess->GetVisibleSet());
}

//---------------------------------------------------------------------------
void CSoundEmitterComponent::Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager)
{
	NiPoint3 kTranslation;
	if( pkParentEntity->GetPropertyData(ms_kTranslationName, kTranslation)
	&& m_spSceneRoot->GetTranslate() != kTranslation )
	{
		m_spSceneRoot->SetTranslate(kTranslation);
		m_spSceneRoot->Update(fTime);
	}
}
//---------------------------------------------------------------------------
void CSoundEmitterComponent::GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const
{
    kPropertyNames.Add(ms_kTimeIntervalName);
    kPropertyNames.Add(ms_kSoundName);
    kPropertyNames.Add(ms_kSceneRootPointerName);
    kPropertyNames.Add(ms_kLoopCountName);
    kPropertyNames.Add(ms_kMinDistanceName);
    kPropertyNames.Add(ms_kMaxDistanceName);
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName
     || kPropertyName == ms_kSceneRootPointerName
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )
    {
        bCanReset = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::ResetProperty(const NiFixedString& kPropertyName)
{
    return false;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique)
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName
     || kPropertyName == ms_kSceneRootPointerName
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )
	{
		bMadeUnique = false;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName
     || kPropertyName == ms_kSceneRootPointerName
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )	
    {
        kDisplayName = kPropertyName;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        // Scene Root Pointer property should not be displayed.
        kDisplayName = NULL;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::SetDisplayName(const NiFixedString& kPropertyName, const NiFixedString& kDisplayName)
{
    // This component does not allow the display name to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const
{
    if (kPropertyName == ms_kTimeIntervalName || kPropertyName == ms_kLoopCountName)
    {
        kPrimitiveType = PT_UINT;
    }
    else if (kPropertyName == ms_kSoundName)
    {
        kPrimitiveType = PT_STRING;
    }
    else if(kPropertyName == ms_kMinDistanceName || kPropertyName == ms_kMaxDistanceName)
    {
        kPrimitiveType = PT_FLOAT;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        kPrimitiveType = PT_NIOBJECTPOINTER;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::SetPrimitiveType(const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType)
{
    // This component does not allow the primitive type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const
{
    if (kPropertyName == ms_kTimeIntervalName || kPropertyName == ms_kLoopCountName)
    {
        kSemanticType = PT_UINT;
    }
    else if (kPropertyName == ms_kSoundName)
    {
        kSemanticType = "Sound Filename";
    }
    else if(kPropertyName == ms_kMinDistanceName || kPropertyName == ms_kMaxDistanceName)
    {
        kSemanticType = PT_FLOAT;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        kSemanticType = PT_NIOBJECTPOINTER;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::SetSemanticType(const NiFixedString& kPropertyName, const NiFixedString& kSemanticType)
{
    // This component does not allow the semantic type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName 
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )
    {
        kDescription = kPropertyName;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        // This is a hidden property, so no description is provided.
        kDescription = NULL;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::SetDescription(const NiFixedString& kPropertyName, const NiFixedString& kDescription)
{
    // This component does not allow the description to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName 
     || kPropertyName == ms_kSceneRootPointerName
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )    
    {
        kCategory = ms_kComponentName;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly)
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName 
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )
    {
        bIsReadOnly = false;
        return true;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        bIsReadOnly = true;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique)
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName 
     || kPropertyName == ms_kSceneRootPointerName 
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )
    {
        bIsUnique = true;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable)
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName 
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )
    {
        bIsSerializable = true;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        bIsSerializable = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable)
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName
     || kPropertyName == ms_kSceneRootPointerName 
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )
    {
        bIsInheritable = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex,bool& bIsExternalAssetPath) const
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName
     || kPropertyName == ms_kSceneRootPointerName 
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )
    {
        bIsExternalAssetPath = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName
     || kPropertyName == ms_kSceneRootPointerName 
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )
    {
        uiCount = 1;
    }
	else
	{
		return false;
	}

    return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet)
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName
     || kPropertyName == ms_kSceneRootPointerName
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )
    {
        bCountSet = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::IsCollection(const NiFixedString& kPropertyName,bool& bIsCollection) const
{
    if( kPropertyName == ms_kTimeIntervalName
     || kPropertyName == ms_kSoundName
     || kPropertyName == ms_kSceneRootPointerName
     || kPropertyName == ms_kLoopCountName
     || kPropertyName == ms_kMinDistanceName
     || kPropertyName == ms_kMaxDistanceName )
    {
        bIsCollection = false;
    }
	else
	{
		return false;
	}

    return true;
}

//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::GetPropertyData(const NiFixedString& kPropertyName, unsigned int& uiData, unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kTimeIntervalName)
    {
        uiData = GetTimeInterval();
    }
    else if (kPropertyName == ms_kLoopCountName)
    {
        uiData = GetLoopCount();
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::SetPropertyData(const NiFixedString& kPropertyName, unsigned int uiData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kTimeIntervalName)
	{
		SetTimeInterval(uiData);
	}
    else if (kPropertyName == ms_kLoopCountName)
    {
        SetLoopCount(uiData);
    }
	else
	{
		return false;
	}

	return true;
}

NiBool CSoundEmitterComponent::GetPropertyData(const NiFixedString& kPropertyName, float& fData, unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kMinDistanceName)
    {
        fData = GetMinDistance();
    }
    else if (kPropertyName == ms_kMaxDistanceName)
    {
        fData = GetMaxDistance();
    }
    else
    {
        return false;
    }

    return true;
}

NiBool CSoundEmitterComponent::SetPropertyData(const NiFixedString& kPropertyName, float fData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kMinDistanceName)
	{
		SetMinDistance(fData);
	}
    else if (kPropertyName == ms_kMaxDistanceName)
    {
        SetMaxDistance(fData);
    }
	else
	{
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::GetPropertyData(const NiFixedString& kPropertyName, NiFixedString& kData,unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kSoundName)
    {
        kData = GetSoundName();
    }
    else
    {
        return false;
    }

    return true;
}


//---------------------------------------------------------------------------
NiBool CSoundEmitterComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiFixedString& kData,unsigned int uiIndex)
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kSoundName)
    {
        SetSoundName(kData);
    }
    else
    {
        return false;
    }

    return true;
}

NiBool CSoundEmitterComponent::GetPropertyData(const NiFixedString& kPropertyName, NiObject*& pkData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		pkData = m_spSceneRoot;
	}
	else
	{
		return false;
	}

	return true;
}
