#pragma once

#include "TerrainLibType.h"

class TERRAIN_ENTRY CSoundEmitterComponent : public NiRefObject,
                                    public NiEntityComponentInterface
{
public:
    static void _SDMInit();
    static void _SDMShutdown();

    // Class name.
    static NiFixedString ms_kClassName;

    // Component name.
    static NiFixedString ms_kComponentName;

    // Property names.
    static NiFixedString ms_kTimeIntervalName; //时间间隔
    static NiFixedString ms_kSoundName; //声音文件
    static NiFixedString ms_kLoopCountName; //循环次数
    static NiFixedString ms_kMinDistanceName; //最近距离
    static NiFixedString ms_kMaxDistanceName; //最远距离

    static NiFixedString ms_kSceneRootPointerName;

    // Dependent property names.
    static NiFixedString ms_kTranslationName;

    static NiUniqueID ms_kTemplateID;
    static NiAVObject* ms_pkSceneRootTemplate;

	unsigned int GetTimeInterval() const { return m_uiTimeInterval; }
	void SetTimeInterval(unsigned int uiTimeInterval) { m_uiTimeInterval = uiTimeInterval; }
	const NiFixedString& GetSoundName() const { return m_kSoundName; }
    void SetSoundName(const NiFixedString& kSoundName) { m_kSoundName = kSoundName; }
	
    unsigned int GetLoopCount() const { return m_uiLoopCount; }
    void SetLoopCount(unsigned int uiLoopCount) { m_uiLoopCount = uiLoopCount; }

    float GetMinDistance() const { return m_fMinDistance; }
    void SetMinDistance(float fMinDistance) { m_fMinDistance = fMinDistance; }

    float GetMaxDistance() const { return m_fMaxDistance; }
    void SetMaxDistance(float fMaxDistance) { m_fMaxDistance = fMaxDistance; }

private:
	unsigned int m_uiTimeInterval;
    unsigned int m_uiLoopCount;
    float m_fMinDistance;
    float m_fMaxDistance;
    NiFixedString m_kSoundName;
    NiAVObjectPtr m_spSceneRoot;

public:
	CSoundEmitterComponent();

    // NiEntityComponentInterface overrides.
    virtual NiEntityComponentInterface* Clone(bool bInheritProperties);
    virtual NiEntityComponentInterface* GetMasterComponent();
    virtual void SetMasterComponent(NiEntityComponentInterface* pkMasterComponent);
    virtual void GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames);

    // NiEntityPropertyInterface overrides.
    virtual NiBool SetTemplateID(const NiUniqueID& kID);
    virtual NiUniqueID GetTemplateID();
    virtual void AddReference();
    virtual void RemoveReference();
    virtual NiFixedString GetClassName() const;
    virtual NiFixedString GetName() const;
    virtual NiBool SetName(const NiFixedString& kName);
    virtual void Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager);
    virtual void BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors);
    virtual void GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const;
    virtual NiBool CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const;
    virtual NiBool ResetProperty(const NiFixedString& kPropertyName);
    virtual NiBool MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique);
    virtual NiBool GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const;
    virtual NiBool SetDisplayName(const NiFixedString& kPropertyName, const NiFixedString& kDisplayName);
    virtual NiBool GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const;
    virtual NiBool SetPrimitiveType(const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType);
    virtual NiBool GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const;
    virtual NiBool SetSemanticType(const NiFixedString& kPropertyName, const NiFixedString& kSemanticType);
    virtual NiBool GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const;
    virtual NiBool SetDescription(const NiFixedString& kPropertyName, const NiFixedString& kDescription);
    virtual NiBool GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const;
    virtual NiBool IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly);
    virtual NiBool IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique);
    virtual NiBool IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable);
    virtual NiBool IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable);
    virtual NiBool IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex, bool& bIsExternalAssetPath) const;
    virtual NiBool GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const;
    virtual NiBool SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet);
    virtual NiBool IsCollection(const NiFixedString& kPropertyName, bool& bIsCollection) const;

    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, unsigned int& uiData, unsigned int uiIndex) const;
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, unsigned int uiData, unsigned int uiIndex);
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, float& fData, unsigned int uiIndex = 0) const;
    virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, float fData, unsigned int uiIndex = 0);
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiFixedString& kData, unsigned int uiIndex) const;
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiFixedString& kData,unsigned int uiIndex);
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiObject*& pkData, unsigned int uiIndex) const;
};