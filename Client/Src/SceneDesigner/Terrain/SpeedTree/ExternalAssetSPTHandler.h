#pragma once

#include "TerrainLibType.h"

class NiExternalAssetParams;

class TERRAIN_ENTRY CExternalAssetSPTHandler :public NiExternalAssetHandler
{
public:
    CExternalAssetSPTHandler(bool bCloneFromPristine = false);
    virtual ~CExternalAssetSPTHandler();

    static NiFixedString T_ASSETTYPE;

    // Errors
    static NiFixedString ERR_FILE_LOAD_FAILED;

    virtual NiBool CanHandleParams(NiExternalAssetParams* pkParams);
    virtual NiFixedString GetAssetType();

    virtual unsigned int GetNumAssetsRegistered(NiExternalAssetParams* pkParams);
    virtual NiBool Register(NiExternalAssetParams* pkParams);

    virtual NiBool LoadAll(NiExternalAssetParams* pkParams = 0, NiEntityErrorInterface* pkErrorHandler = 0, NiExternalAssetManager* pkAssetManager = 0);
    virtual NiBool Load(NiExternalAssetParams* pkParams, NiEntityErrorInterface* pkErrorHandler = 0, NiExternalAssetManager* pkAssetManager = 0);
    virtual void Unload(NiExternalAssetParams* pkParams, NiExternalAssetManager* pkAssetManager = 0);
    virtual void UnloadAll(NiExternalAssetParams* pkParams = 0, NiExternalAssetManager* pkAssetManager = 0);
    virtual void UnloadAllUnusedAssets(NiExternalAssetParams* pkParams = 0, NiExternalAssetManager* pkAssetManager = 0);

    virtual NiBool GetCloneFromPristine(NiExternalAssetParams* pkParams = 0);
    virtual NiBool SetCloneFromPristine(bool bCloneFromPristine, NiExternalAssetParams* pkParams = 0);

    virtual NiBool Retrieve(NiExternalAssetParams* pkParams);

    // *** begin Emergent internal use only ***
    static void _SDMInit();
    static void _SDMShutdown();
    // *** end Emergent internal use only ***

protected:
    void Unload(const char* pcAssetName);

    NiBool LoadSPTFile(const char* pcAssetName, NiEntityErrorInterface* pkErrorHandler, NiExternalAssetManager* pkAssetManager);

    // Store mapping from Asset path name to Asset
    NiTStringMap<NiAVObjectPtr> m_kAVObjectMap;
    NiTStringMap<NiTObjectSet<NiAVObjectPtr>*> m_kMapToCloneSet;

	bool m_bCloneFromPristine;
};
