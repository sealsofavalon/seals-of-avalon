
#include "TerrainPCH.h"
#include "ParamsSPT.h"

//---------------------------------------------------------------------------
NiImplementRTTI(CParamsSPT, NiExternalAssetParams);
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
CParamsSPT::CParamsSPT()
{
    m_spSceneRoot = 0;
}
//---------------------------------------------------------------------------
CParamsSPT::~CParamsSPT()
{
    m_spSceneRoot = 0;
}
//---------------------------------------------------------------------------
NiBool CParamsSPT::GetSceneRoot(NiAVObject*& pkAVObject)
{
    if (!m_spSceneRoot)
        return false;

    pkAVObject = m_spSceneRoot;
    return true;
}
//---------------------------------------------------------------------------
void CParamsSPT::SetSceneRoot(NiAVObject* pkAVObject)
{
    m_spSceneRoot = pkAVObject;
}
//---------------------------------------------------------------------------
