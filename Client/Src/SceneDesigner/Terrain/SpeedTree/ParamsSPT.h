#ifndef PARAMS_SPT_H
#define PARAMS_SPT_H

#include "TerrainLibType.h"


class TERRAIN_ENTRY CParamsSPT : public NiExternalAssetParams
{
    NiDeclareRTTI;

public:
    CParamsSPT();
    virtual ~CParamsSPT();

    NiBool GetSceneRoot(NiAVObject*& pkAVObject);
    void SetSceneRoot(NiAVObject* pkAVObject);

protected:
    NiAVObjectPtr m_spSceneRoot;
};

#endif // NIPARAMSNIF_H
