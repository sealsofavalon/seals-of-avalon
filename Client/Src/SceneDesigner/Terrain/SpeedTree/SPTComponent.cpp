#include "TerrainPCH.h"

#include "SPTComponent.h"
#include "ParamsSPT.h"

NiFactoryDeclarePropIntf(CSPTComponent);

NiFixedString CSPTComponent::ERR_TRANSLATION_NOT_FOUND;
NiFixedString CSPTComponent::ERR_ROTATION_NOT_FOUND;
NiFixedString CSPTComponent::ERR_SCALE_NOT_FOUND;
NiFixedString CSPTComponent::ERR_FILE_LOAD_FAILED;

NiFixedString CSPTComponent::ms_kClassName;
NiFixedString CSPTComponent::ms_kComponentName;

NiFixedString CSPTComponent::ms_kSPTFilePathName;
NiFixedString CSPTComponent::ms_kSceneRootPointerName;

NiFixedString CSPTComponent::ms_kSPTFilePathDescription;

NiFixedString CSPTComponent::ms_kTranslationName;
NiFixedString CSPTComponent::ms_kRotationName;
NiFixedString CSPTComponent::ms_kScaleName;

//---------------------------------------------------------------------------
void CSPTComponent::_SDMInit()
{
    ERR_TRANSLATION_NOT_FOUND = "Translation property not found.";
    ERR_ROTATION_NOT_FOUND = "Rotation property not found.";
    ERR_SCALE_NOT_FOUND = "Scale property not found.";
    ERR_FILE_LOAD_FAILED = "SPT file load failed.";

    ms_kClassName = "CSPTComponent";
    ms_kComponentName = "Speed Tree";

    ms_kSPTFilePathName = "SPT File Path";
    ms_kSceneRootPointerName = "Scene Root Pointer";

    ms_kSPTFilePathDescription = "The path to a spt file to use for the entity.";

    ms_kTranslationName = "Translation";
    ms_kRotationName = "Rotation";
    ms_kScaleName = "Scale";

	NiFactoryRegisterPropIntf(CSPTComponent);
}
//---------------------------------------------------------------------------
void CSPTComponent::_SDMShutdown()
{
    ERR_TRANSLATION_NOT_FOUND = NULL;
    ERR_ROTATION_NOT_FOUND = NULL;
    ERR_SCALE_NOT_FOUND = NULL;
    ERR_FILE_LOAD_FAILED = NULL; 

    ms_kClassName = NULL;
    ms_kComponentName = NULL;

    ms_kSPTFilePathName = NULL;
    ms_kSceneRootPointerName = NULL;

    ms_kSPTFilePathDescription = NULL;

    ms_kTranslationName = NULL;
    ms_kRotationName = NULL;
    ms_kScaleName = NULL;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::RecursiveFindAnimations(NiAVObject* pkObject)
{
    NIASSERT(pkObject);

    NiTimeController* pkCtlrs = pkObject->GetControllers();
    if (pkCtlrs)
    {
        return true;
    }

    NiNode* pkNode = NiDynamicCast(NiNode, pkObject);
    if (pkNode)
    {
        for (unsigned int ui = 0; ui < pkNode->GetArrayCount(); ui++)
        {
            NiAVObject* pkChild = pkNode->GetAt(ui);
            if (pkChild)
            {
                if (RecursiveFindAnimations(pkChild))
                {
                    return true;
                }
            }
        }
    }

    return false;
}
//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* CSPTComponent::Clone(bool bInheritProperties)
{
    if (bInheritProperties)
    {
        return NiNew CSPTComponent(this);
    }
    else
    {
        CSPTComponent* pkClone = NiNew CSPTComponent(m_spMasterComponent);

        pkClone->m_uFlags = m_uFlags;
        pkClone->m_kSPTFilePath = m_kSPTFilePath;

        return pkClone;
    }
}
//---------------------------------------------------------------------------
NiEntityComponentInterface* CSPTComponent::GetMasterComponent()
{
    return m_spMasterComponent;
}
//---------------------------------------------------------------------------
void CSPTComponent::SetMasterComponent(NiEntityComponentInterface* pkMasterComponent)
{
    if (pkMasterComponent)
    {
        NIASSERT(pkMasterComponent->GetClassName() == GetClassName());
        m_spMasterComponent = (CSPTComponent*) pkMasterComponent;
    }
    else
    {
        NiTObjectSet<NiFixedString> kPropertyNames(10);
        GetPropertyNames(kPropertyNames);
        for (unsigned int ui = 0; ui < kPropertyNames.GetSize(); ui++)
        {
            bool bMadeUnique;
            NIVERIFY(MakePropertyUnique(kPropertyNames.GetAt(ui), bMadeUnique));
        }
        m_spMasterComponent = NULL;
    }
}
//---------------------------------------------------------------------------
void CSPTComponent::GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
    kDependentPropertyNames.Add(ms_kTranslationName);
    kDependentPropertyNames.Add(ms_kRotationName);
    kDependentPropertyNames.Add(ms_kScaleName);
}
//---------------------------------------------------------------------------
// NiEntityPropertyInterface overrides.
//---------------------------------------------------------------------------
NiBool CSPTComponent::SetTemplateID(const NiUniqueID& kID)
{
    //Not supported for custom components
    return false;
}
//---------------------------------------------------------------------------
NiUniqueID  CSPTComponent::GetTemplateID()
{
    static const NiUniqueID kUniqueID = NiUniqueID(0xA3,0x83,0x43,0x23,0xFD,0x24,0xA6,0x47,0xA8,0x78,0x2,0xC4,0x3E,0x7E,0xCD,0xBD);
    return kUniqueID;
}
//---------------------------------------------------------------------------
void CSPTComponent::AddReference()
{
    this->IncRefCount();
}
//---------------------------------------------------------------------------
void CSPTComponent::RemoveReference()
{
    this->DecRefCount();
}
//---------------------------------------------------------------------------
NiFixedString CSPTComponent::GetClassName() const
{
    return ms_kClassName;
}
//---------------------------------------------------------------------------
NiFixedString CSPTComponent::GetName() const
{
    return ms_kComponentName;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::SetName(const NiFixedString& kName)
{
    // This component does not allow its name to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::IsAnimated() const
{
    return GetShouldUpdateSceneRoot();
}
//---------------------------------------------------------------------------
void CSPTComponent::Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager)
{
    if (ShouldReloadScene())
    {
        // Re-synchronize default and local data.
        m_kSPTFilePath = GetSPTFilePath();

        // Clear out existing scene root.
        m_spSceneRoot = NULL;

        // If the scene root does not exist, create it using the property
        // data.
        if (GetSPTFilePath().Exists() && pkAssetManager && pkAssetManager->GetAssetFactory())
        {
            NIASSERT(pkAssetManager);
            CParamsSPT kSPTParams;
            if (!kSPTParams.SetAssetPath(GetSPTFilePath()) || !pkAssetManager->RegisterAndResolve(&kSPTParams))
            {
                // Failing to resolve amounts to failed to load file.
                SetLoadErrorHit(true);
                pkErrors->ReportError(ERR_FILE_LOAD_FAILED, GetSPTFilePath(), pkParentEntity->GetName(), ms_kSPTFilePathName);
                return;
            }

            NiBool bSuccess = pkAssetManager->Retrieve(&kSPTParams);
            NIASSERT(bSuccess);

            NiAVObject* pkAVObject; 
            bSuccess = kSPTParams.GetSceneRoot(pkAVObject);

            if (bSuccess && pkAVObject != NULL)
            {
                m_spSceneRoot = pkAVObject;

                SetShouldUpdateSceneRoot(NIBOOL_IS_TRUE(RecursiveFindAnimations(m_spSceneRoot)));

                // Perform initial update.
                m_spSceneRoot->Update(0.0f);
                m_spSceneRoot->UpdateNodeBound();
                m_spSceneRoot->UpdateProperties();
                m_spSceneRoot->UpdateEffects();
            }
            else
            {
                NIASSERT(!"Failed to Retrieve file!");
                // Failing to retrieve amounts to failed to load file.
                SetLoadErrorHit(true);
                pkErrors->ReportError(ERR_FILE_LOAD_FAILED, GetSPTFilePath(), pkParentEntity->GetName(), ms_kSPTFilePathName);
                return;
            }
        }
    }

    if (m_spSceneRoot)
    {
        NiBool bDependentPropertiesFound = true;

        // Find dependent properties.
        NiPoint3 kTranslation;
        if (!pkParentEntity->GetPropertyData(ms_kTranslationName, kTranslation))
        {
            bDependentPropertiesFound = false;
            pkErrors->ReportError(ERR_TRANSLATION_NOT_FOUND, NULL, pkParentEntity->GetName(), ms_kTranslationName);
        }

        NiMatrix3 kRotation;
        if (!pkParentEntity->GetPropertyData(ms_kRotationName, kRotation))
        {
            bDependentPropertiesFound = false; 
			pkErrors->ReportError(ERR_ROTATION_NOT_FOUND, NULL, pkParentEntity->GetName(), ms_kRotationName);
        }

        float fScale;
        if (!pkParentEntity->GetPropertyData(ms_kScaleName, fScale))
        {
            bDependentPropertiesFound = false;
			pkErrors->ReportError(ERR_SCALE_NOT_FOUND, NULL, pkParentEntity->GetName(), ms_kScaleName);
        }

        // Use dependent properties to update transform of scene root.
        bool bUpdatedTransforms = false;
        if (bDependentPropertiesFound)
        {
            if (m_spSceneRoot->GetTranslate() != kTranslation)
            {
                m_spSceneRoot->SetTranslate(kTranslation);
                bUpdatedTransforms = true;
            }
            if (m_spSceneRoot->GetRotate() != kRotation)
            {
                m_spSceneRoot->SetRotate(kRotation);
                bUpdatedTransforms = true;
            }
            if (m_spSceneRoot->GetScale() != fScale)
            {
                m_spSceneRoot->SetScale(fScale);
                bUpdatedTransforms = true;
            }
        }

        // Update scene root with the provided time.
        if (bUpdatedTransforms)
        {
            m_spSceneRoot->Update(fTime);
        }
        else if (GetShouldUpdateSceneRoot())
        {
            m_spSceneRoot->UpdateSelected(fTime);
        }
    }
}
//---------------------------------------------------------------------------
void CSPTComponent::BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors)
{
    if (m_spSceneRoot != NULL)
    {
        pkRenderingContext->m_pkCullingProcess->Process(pkRenderingContext->m_pkCamera, m_spSceneRoot, pkRenderingContext->m_pkCullingProcess->GetVisibleSet());
    }
}
//---------------------------------------------------------------------------
void CSPTComponent::GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const
{
    kPropertyNames.Add(ms_kSPTFilePathName);
    kPropertyNames.Add(ms_kSceneRootPointerName);
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const
{
    if (kPropertyName == ms_kSPTFilePathName)
    {
        bCanReset = (m_spMasterComponent && GetSPTFilePathChanged());
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        bCanReset = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::ResetProperty(const NiFixedString& kPropertyName)
{
    bool bCanReset;
    if (!CanResetProperty(kPropertyName, bCanReset) || !bCanReset)
    {
        return false;
    }

    NIASSERT(m_spMasterComponent);

    if (kPropertyName == ms_kSPTFilePathName)
    {
        SetSPTFilePathChanged(false);
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::MakePropertyUnique(
    const NiFixedString& kPropertyName, bool& bMadeUnique)
{
    bool bCanReset;
    if (!CanResetProperty(kPropertyName, bCanReset))
    {
        return false;
    }

    if (!bCanReset && m_spMasterComponent)
    {
        if (kPropertyName == ms_kSPTFilePathName)
        {
            SetSPTFilePathChanged(true);
            bMadeUnique = true;
        }
        else
        {
            bMadeUnique = false;
        }
    }
    else
    {
        bMadeUnique = false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const
{
    if (kPropertyName == ms_kSPTFilePathName)
    {
        kDisplayName = ms_kSPTFilePathName;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        // Scene Root Pointer property should not be displayed.
        kDisplayName = NULL;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::SetDisplayName(const NiFixedString& kPropertyName, const NiFixedString& kDisplayName)
{
    // This component does not allow the display name to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const
{
    if (kPropertyName == ms_kSPTFilePathName)
    {
        kPrimitiveType = PT_STRING;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        kPrimitiveType = PT_NIOBJECTPOINTER;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::SetPrimitiveType(const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType)
{
    // This component does not allow the primitive type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const
{
    if (kPropertyName == ms_kSPTFilePathName)
    {
        kSemanticType = "SPT Filename";
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        kSemanticType = PT_NIOBJECTPOINTER;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::SetSemanticType(const NiFixedString& kPropertyName, const NiFixedString& kSemanticType)
{
    // This component does not allow the semantic type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const
{
    if (kPropertyName == ms_kSPTFilePathName)
    {
        kDescription = ms_kSPTFilePathDescription;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        // This is a hidden property, so no description is provided.
        kDescription = NULL;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::SetDescription(const NiFixedString& kPropertyName, const NiFixedString& kDescription)
{
    // This component does not allow the description to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const
{
    if (kPropertyName == ms_kSPTFilePathName ||
        kPropertyName == ms_kSceneRootPointerName)
    {
        kCategory = ms_kComponentName;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly)
{
    if (kPropertyName == ms_kSPTFilePathName)
    {
        bIsReadOnly = false;
        return true;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        bIsReadOnly = true;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique)
{
    if (kPropertyName == ms_kSPTFilePathName)
    {
        if (m_spMasterComponent)
        {
            NIVERIFY(CanResetProperty(kPropertyName, bIsUnique));
        }
        else
        {
            bIsUnique = true;
        }
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        bIsUnique = true;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable)
{
    if (kPropertyName == ms_kSPTFilePathName)
    {
        bool bIsUnique;
        NIVERIFY(IsPropertyUnique(kPropertyName, bIsUnique));

        bool bIsReadOnly;
        NIVERIFY(IsPropertyReadOnly(kPropertyName, bIsReadOnly));

        bIsSerializable = bIsUnique && !bIsReadOnly;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        bIsSerializable = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable)
{
    if (kPropertyName == ms_kSPTFilePathName)
    {
        bIsInheritable = true;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        bIsInheritable = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex,bool& bIsExternalAssetPath) const
{
    if (kPropertyName == ms_kSPTFilePathName)
    {
        bIsExternalAssetPath = true;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        bIsExternalAssetPath = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const
{
    if (kPropertyName == ms_kSPTFilePathName ||
        kPropertyName == ms_kSceneRootPointerName)
    {
        uiCount = 1;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet)
{
    if (kPropertyName == ms_kSPTFilePathName ||
        kPropertyName == ms_kSceneRootPointerName)
    {
        bCountSet = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::IsCollection(const NiFixedString& kPropertyName,bool& bIsCollection) const
{
    if (kPropertyName == ms_kSPTFilePathName ||
        kPropertyName == ms_kSceneRootPointerName)
    {
        bIsCollection = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::GetPropertyData(const NiFixedString& kPropertyName, NiFixedString& kData,unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kSPTFilePathName)
    {
        kData = GetSPTFilePath();
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiFixedString& kData,unsigned int uiIndex)
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kSPTFilePathName)
    {
        SetSPTFilePath(kData);
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CSPTComponent::GetPropertyData(const NiFixedString& kPropertyName, NiObject*& pkData,unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kSceneRootPointerName)
    {
        pkData = m_spSceneRoot;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
