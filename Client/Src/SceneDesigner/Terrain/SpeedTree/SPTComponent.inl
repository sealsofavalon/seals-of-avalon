
//---------------------------------------------------------------------------
inline CSPTComponent::CSPTComponent() 
	:m_uFlags(0)
{
}
//---------------------------------------------------------------------------
inline CSPTComponent::CSPTComponent(const NiFixedString& kSPTFilePath) 
	:m_uFlags(0)
	,m_kSPTFilePath(kSPTFilePath)
{
}
//---------------------------------------------------------------------------
inline CSPTComponent::CSPTComponent(CSPTComponent* pkMasterComponent) 
	:m_uFlags(0)
	,m_spMasterComponent(pkMasterComponent)
{
}
//---------------------------------------------------------------------------
inline const NiFixedString& CSPTComponent::GetSPTFilePath() const
{
    if (!GetSPTFilePathChanged() && m_spMasterComponent)
    {
        return m_spMasterComponent->GetSPTFilePath();
    }
    else
    {
        return m_kSPTFilePath;
    }
}
//---------------------------------------------------------------------------
inline void CSPTComponent::SetSPTFilePath(const NiFixedString& kSPTFilePath)
{
    if (GetSPTFilePath() != kSPTFilePath)
    {
        m_kSPTFilePath = kSPTFilePath;
        m_spSceneRoot = NULL;
        SetLoadErrorHit(false);
        SetSPTFilePathChanged(true);
    }
}
//---------------------------------------------------------------------------
inline NiAVObject* CSPTComponent::GetSceneRoot() const
{
    return m_spSceneRoot;
}
//---------------------------------------------------------------------------
inline NiBool CSPTComponent::ShouldReloadScene() const
{
    if (GetSPTFilePath() != m_kSPTFilePath)
    {
        return true;
    }
    else if (!m_spSceneRoot)
    {
        return !GetLoadErrorHit();
    }
    else
    {
        return false;
    }
}
//---------------------------------------------------------------------------
inline NiBool CSPTComponent::GetLoadErrorHit() const
{
    return GetBit(LOAD_ERROR_HIT_MASK);
}
//---------------------------------------------------------------------------
inline void CSPTComponent::SetLoadErrorHit(bool bLoadErrorHit)
{
    SetBit(bLoadErrorHit, LOAD_ERROR_HIT_MASK);
}
//---------------------------------------------------------------------------
inline NiBool CSPTComponent::GetSPTFilePathChanged() const
{
    return GetBit(SPT_FILE_PATH_CHANGED_MASK);
}
//---------------------------------------------------------------------------
inline void CSPTComponent::SetSPTFilePathChanged(bool bSPTFilePathChanged)
{
    SetBit(bSPTFilePathChanged, SPT_FILE_PATH_CHANGED_MASK);
}
//---------------------------------------------------------------------------
inline NiBool CSPTComponent::GetShouldUpdateSceneRoot() const
{
    return GetBit(SHOULD_UPDATE_SCENE_ROOT_MASK);
}
//---------------------------------------------------------------------------
inline void CSPTComponent::SetShouldUpdateSceneRoot(bool bShouldUpdateSceneRoot)
{
    SetBit(bShouldUpdateSceneRoot, SHOULD_UPDATE_SCENE_ROOT_MASK);
}
//---------------------------------------------------------------------------
