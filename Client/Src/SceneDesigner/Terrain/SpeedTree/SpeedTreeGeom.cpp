
#include "TerrainPCH.h"

#include "TextureManager.h"

#include "SpeedTreeGeom.h"
#include "SpeedTreeRT.h"
#include "SpeedTreeShader.h"

NiNode* CSpeedTreeNode::Create(const char* pcAssetName, NiEntityErrorInterface* pkErrorHandler)
{
	CSpeedTreeRT kSpeedTree;
	if( !kSpeedTree.LoadTree(pcAssetName) )
		return NULL;

	NiAVObjectPtr spAVObject;

	// 设置光照方式
	kSpeedTree.SetBranchLightingMethod(CSpeedTreeRT::LIGHT_DYNAMIC);
	kSpeedTree.SetLeafLightingMethod(CSpeedTreeRT::LIGHT_DYNAMIC);
	kSpeedTree.SetFrondLightingMethod(CSpeedTreeRT::LIGHT_DYNAMIC);

	// 设置风力计算方式
	kSpeedTree.SetBranchWindMethod(CSpeedTreeRT::WIND_NONE);
	kSpeedTree.SetLeafWindMethod(CSpeedTreeRT::WIND_NONE);
	kSpeedTree.SetFrondWindMethod(CSpeedTreeRT::WIND_NONE);

	if( !kSpeedTree.Compute() )
	{
		return NULL;
	}

	kSpeedTree.SetLeafRockingState(true);

	NiNode* pkNode = NiNew NiNode;
	NiAVObject* pkAVObject;

	NiFilename kFilename(pcAssetName);
	char acPath[1024];
	NiSprintf(acPath, sizeof(acPath), "%s%s", kFilename.GetDrive(), kFilename.GetDir());
	NiPath::Standardize(acPath);

	pkAVObject = CBranchGeometry::Create(kSpeedTree, acPath, pkErrorHandler);
	if( pkAVObject )
		pkNode->AttachChild(pkAVObject);

	pkAVObject = CFrondGeometry::Create(kSpeedTree, acPath, pkErrorHandler);
	if( pkAVObject )
		pkNode->AttachChild(pkAVObject);

	pkAVObject = CLeafCardGeometry::Create(kSpeedTree, acPath, pkErrorHandler);
	if( pkAVObject )
		pkNode->AttachChild(pkAVObject);

	pkAVObject = CLeafMeshGeometry::Create(kSpeedTree, acPath, pkErrorHandler);
	if( pkAVObject )
		pkNode->AttachChild(pkAVObject);

	return pkNode;
}

NiAVObject* CBranchGeometry::Create(CSpeedTreeRT& kSpeedTree, const char* pcPath, NiEntityErrorInterface* pkErrorHandler)
{
	CSpeedTreeRT::SGeometry sGeometry;
	kSpeedTree.GetGeometry(sGeometry, SpeedTree_BranchGeometry);

	CSpeedTreeRT::SMapBank sMapBank;
	kSpeedTree.GetMapBank(sMapBank);

	const CSpeedTreeRT::SGeometry::SIndexed& sBranches = sGeometry.m_sBranches;

	if (sBranches.m_nNumVertices <= 0 || sBranches.m_nNumLods <= 0)
	{
		return NULL;
	}

	if( sBranches.m_pNumStrips[0] <= 0 )
		return NULL;

	//vertex
	NiPoint3* pkVertex = NiNew NiPoint3[sBranches.m_nNumVertices];
	memcpy(pkVertex, sBranches.m_pCoords, sizeof(NiPoint3)*sBranches.m_nNumVertices);

	//normal
	NiPoint3* pkNormal = NiNew NiPoint3[sBranches.m_nNumVertices];
	memcpy(pkNormal, sBranches.m_pNormals, sizeof(NiPoint3)*sBranches.m_nNumVertices);

	//texture coords
	NiPoint2* pkTexCoords;
	unsigned short usNumTextureSets;
	if( sBranches.m_pTexCoords[CSpeedTreeRT::TL_DETAIL] && sMapBank.m_pBranchMaps[CSpeedTreeRT::TL_DETAIL] && sMapBank.m_pBranchMaps[CSpeedTreeRT::TL_DETAIL][0])
	{
		pkTexCoords	= NiNew NiPoint2[sBranches.m_nNumVertices*2];
		memcpy(pkTexCoords, sBranches.m_pTexCoords[CSpeedTreeRT::TL_DIFFUSE], sizeof(NiPoint2)*sBranches.m_nNumVertices);
		memcpy(pkTexCoords + sBranches.m_nNumVertices, sBranches.m_pTexCoords[CSpeedTreeRT::TL_DIFFUSE], sizeof(NiPoint2)*sBranches.m_nNumVertices);

		usNumTextureSets = 2;
	}
	else
	{
		pkTexCoords	= NiNew NiPoint2[sBranches.m_nNumVertices];
		memcpy(pkTexCoords, sBranches.m_pTexCoords[CSpeedTreeRT::TL_DIFFUSE], sizeof(NiPoint2)*sBranches.m_nNumVertices);

		usNumTextureSets = 1;
	}

	//strips index
	unsigned short usStrips = (unsigned short)sBranches.m_pNumStrips[0];
	unsigned short* pusStripLengths = NiAlloc(unsigned short, usStrips);

	int iTotalStripLengths = 0;
	unsigned short usTriangleCount = 0;
	for( int i = 0; i < usStrips; i++ )
	{
		pusStripLengths[i] = (unsigned short)sBranches.m_pStripLengths[0][i];
		iTotalStripLengths += sBranches.m_pStripLengths[0][i];

		usTriangleCount += pusStripLengths[i] - 2;
	}

	unsigned short* pusStripLists = NiAlloc(unsigned short, iTotalStripLengths);
	unsigned short* pusPtr = pusStripLists;
	for( int i = 0; i < usStrips; i++ )
	{
		for( int j = 0; j < sBranches.m_pStripLengths[0][i]; j++ )
		{
			//int to short
			*pusPtr++ = (unsigned short)sBranches.m_pStrips[0][i][j]; 
		}
	}

	//Geometry
	NiTriStrips* pkGeometry = NiNew NiTriStrips(unsigned short(sBranches.m_nNumVertices), pkVertex, pkNormal, NULL, pkTexCoords, usNumTextureSets, NiGeometryData::NBT_METHOD_NONE, usTriangleCount, usStrips, pusStripLengths, pusStripLists);

	//base map
	NiTexturingProperty::Map* pkBaseMap = NULL;
	if(sMapBank.m_pBranchMaps[CSpeedTreeRT::TL_DIFFUSE] && sMapBank.m_pBranchMaps[CSpeedTreeRT::TL_DIFFUSE][0])
	{
		NiFilename kTexName(sMapBank.m_pBranchMaps[CSpeedTreeRT::TL_DIFFUSE]);
		char acFilename[1024];
		NiSprintf(acFilename, sizeof(acFilename), "%s%s%s", pcPath, kTexName.GetFilename(), kTexName.GetExt());
		NiPixelData* pkPixelData = CTextureManager::Get()->Load(acFilename);
		if( pkPixelData )
		{
			NiTexture* pkTexture = NiSourceTexture::Create(pkPixelData);
			if( pkTexture )
			{
				pkBaseMap = NiNew NiTexturingProperty::Map;
				pkBaseMap->SetTexture(pkTexture);
				pkBaseMap->SetClampMode(NiTexturingProperty::WRAP_S_WRAP_T);
				pkBaseMap->SetTextureIndex(0);
			}
		}
		else
		{
			pkErrorHandler->ReportError("Branch diffuse texture load failed.", acFilename, "", ""); 
		}
	}
	else
	{
		pkErrorHandler->ReportError("No branch diffuse texture", pcPath, "", "");
	}

	//Decal Map
	NiTexturingProperty::Map* pkDecalMap = NULL;
	if(sMapBank.m_pBranchMaps[CSpeedTreeRT::TL_DETAIL] && sMapBank.m_pBranchMaps[CSpeedTreeRT::TL_DETAIL][0])
	{
		NiFilename kTexName(sMapBank.m_pBranchMaps[CSpeedTreeRT::TL_DETAIL]);
		char acFilename[1024];
		NiSprintf(acFilename, sizeof(acFilename), "%s%s%s", pcPath, kTexName.GetFilename(), kTexName.GetExt());
		NiPixelData* pkPixelData = CTextureManager::Get()->Load(acFilename);
		if( pkPixelData )
		{
			NiTexture* pkTexture = NiSourceTexture::Create(pkPixelData);
			if( pkTexture )
			{
				pkDecalMap = NiNew NiTexturingProperty::Map;
				pkDecalMap->SetTexture(pkTexture);
				pkDecalMap->SetClampMode(NiTexturingProperty::WRAP_S_WRAP_T);
				pkDecalMap->SetTextureIndex(1);
			}
		}
		else
		{
			pkErrorHandler->ReportError("Branch detail texture load failed.", acFilename, "", "");
		}
	}

	//Texture property
	if( pkBaseMap || pkDecalMap )
	{
		NiTexturingProperty* pkTexturingProp = NiNew NiTexturingProperty;
		
		if( pkBaseMap )
			pkTexturingProp->SetBaseMap(pkBaseMap);
		
		if( pkDecalMap )
			pkTexturingProp->SetDecalMap(0, pkDecalMap);

		pkGeometry->AttachProperty(pkTexturingProp);
	}

	return pkGeometry;
}

NiAVObject* CFrondGeometry::Create(CSpeedTreeRT& kSpeedTree, const char* pcPath, NiEntityErrorInterface* pkErrorHandler)
{
	// query vertex attribute data
	CSpeedTreeRT::SGeometry sGeometry;
	kSpeedTree.GetGeometry(sGeometry, SpeedTree_FrondGeometry);

	CSpeedTreeRT::SMapBank sMapBank;
	kSpeedTree.GetMapBank(sMapBank);

	const CSpeedTreeRT::SGeometry::SIndexed& sFronds = sGeometry.m_sFronds;

	if (sFronds.m_nNumVertices <= 0 || sFronds.m_nNumLods <= 0)
	{
		return NULL;
	}

	if( sFronds.m_pNumStrips[0] <= 0 )
		return NULL;

	//vertex
	NiPoint3* pkVertex = NiNew NiPoint3[sFronds.m_nNumVertices];
	memcpy(pkVertex, sFronds.m_pCoords, sizeof(NiPoint3)*sFronds.m_nNumVertices);

	//normal
	NiPoint3* pkNormal = NiNew NiPoint3[sFronds.m_nNumVertices];
	memcpy(pkNormal, sFronds.m_pNormals, sizeof(NiPoint3)*sFronds.m_nNumVertices);

	NIASSERT(sFronds.m_pTexCoords[CSpeedTreeRT::TL_DIFFUSE]);

	//texture coords
	NiPoint2* pkTexCoords = NiNew NiPoint2[sFronds.m_nNumVertices];
	memcpy(pkTexCoords, sFronds.m_pTexCoords[CSpeedTreeRT::TL_DIFFUSE], sizeof(NiPoint2)*sFronds.m_nNumVertices);

	//strips index
	unsigned short usStrips = (unsigned short)sFronds.m_pNumStrips[0];
	unsigned short* pusStripLengths = NiAlloc(unsigned short, usStrips);

	int iTotalStripLengths = 0;
	unsigned short usTriangleCount = 0;
	for( int i = 0; i < usStrips; i++ )
	{
		pusStripLengths[i] = (unsigned short)sFronds.m_pStripLengths[0][i];
		iTotalStripLengths += sFronds.m_pStripLengths[0][i];

		usTriangleCount += pusStripLengths[i] - 2;
	}

	unsigned short* pusStripLists = NiAlloc(unsigned short, iTotalStripLengths);
	unsigned short* pusPtr = pusStripLists;
	for( int i = 0; i < usStrips; i++ )
	{
		for( int j = 0; j < sFronds.m_pStripLengths[0][i]; j++ )
		{
			//int to short
			*pusPtr++ = (unsigned short)sFronds.m_pStrips[0][i][j]; 
		}
	}

	//Geometry
	NiTriStrips* pkGeometry = NiNew NiTriStrips(unsigned short(sFronds.m_nNumVertices), pkVertex, pkNormal, NULL, pkTexCoords, 1, NiGeometryData::NBT_METHOD_NONE, usTriangleCount, usStrips, pusStripLengths, pusStripLists);

	//base map
	NiTexturingProperty::Map* pkBaseMap = NULL;
	if(sMapBank.m_pCompositeMaps[CSpeedTreeRT::TL_DIFFUSE] && sMapBank.m_pCompositeMaps[CSpeedTreeRT::TL_DIFFUSE][0])
	{
		NiFilename kTexName(sMapBank.m_pCompositeMaps[CSpeedTreeRT::TL_DIFFUSE]);
		char acFilename[1024];
		NiSprintf(acFilename, sizeof(acFilename), "%s%s%s", pcPath, kTexName.GetFilename(), kTexName.GetExt());
		NiPixelData* pkPixelData = CTextureManager::Get()->Load(acFilename);
		if( pkPixelData )
		{
			NiTexture* pkTexture = NiSourceTexture::Create(pkPixelData);
			if( pkTexture )
			{
				pkBaseMap = NiNew NiTexturingProperty::Map;
				pkBaseMap->SetTexture(pkTexture);
				pkBaseMap->SetClampMode(NiTexturingProperty::WRAP_S_WRAP_T);
				pkBaseMap->SetTextureIndex(0);
			}
		}
		else
		{
			pkErrorHandler->ReportError("Frond diffuse texture load failed.", acFilename, "", "");
		}
	}
	else
	{
		pkErrorHandler->ReportError("No frond diffuse texture", pcPath, "", "");
	}


	//Texture property
	if( pkBaseMap )
	{
		NiTexturingProperty* pkTexturingProp = NiNew NiTexturingProperty;
		pkTexturingProp->SetBaseMap(pkBaseMap);
		pkGeometry->AttachProperty(pkTexturingProp);
	}

	CSpeedTreeRT::SLodValues sLodValues;
	kSpeedTree.GetLodValues(sLodValues, 1.f);

	//Alpha property
	NiAlphaProperty* pkAlphaProperty = NiNew NiAlphaProperty;//todo... use static NiAlphaProperty
	pkAlphaProperty->SetAlphaTesting(true);
	pkAlphaProperty->SetTestMode(NiAlphaProperty::TEST_GREATER);
	pkAlphaProperty->SetTestRef((unsigned char)sLodValues.m_fFrondAlphaTestValue);
	pkGeometry->AttachProperty(pkAlphaProperty);

	NiStencilProperty* pkStencilProperty = NiNew NiStencilProperty;
	pkStencilProperty->SetDrawMode(NiStencilProperty::DRAW_BOTH);
	pkGeometry->AttachProperty(pkStencilProperty);

	return pkGeometry;
}

//----------------------------------------------------------------------------------------------------------------------------
//CLeafCardGeometry
//
CLeafCardGeometry::CLeafCardGeometry(
		unsigned short usVertices, NiPoint3* pkVertex,
        NiPoint3* pkNormal, NiColorA* pkColor, NiPoint2* pkTexture, 
        unsigned short usNumTextureSets, NiGeometryData::DataFlags eNBTMethod,
        unsigned short usTriangles, unsigned short* pusTriList, 
		NiPoint3* pkCenter, NiPoint3* pkCorner, NiPoint2* pkAngle) :
		NiTriShape(NiNew CLeafCardGeometryData(usVertices, pkVertex,
			       pkNormal, pkColor, pkTexture,
				   usNumTextureSets, eNBTMethod,
				   usTriangles, pusTriList)),
       m_usVertices(usVertices),
	   m_pkVertex(pkVertex),
	   m_pkNormal(pkNormal),
	   m_pkColor(pkColor),
	   m_pkTexture(pkTexture),
	   m_pusTriList(pusTriList),
	   m_pkCenter(pkCenter),
	   m_pkCorner(pkCorner),
	   m_pkAngle(pkAngle),
	   m_uiCameraUpdate(0),
	   m_bOwner(true)
{
}

CLeafCardGeometry::CLeafCardGeometry()
	 :m_usVertices(0)
	 ,m_pkVertex(NULL)
	 ,m_pkNormal(NULL)
	 ,m_pkColor(NULL)
	 ,m_pkTexture(NULL)
	 ,m_pusTriList(NULL)
	 ,m_pkCenter(NULL)
	 ,m_pkCorner(NULL)
	 ,m_pkAngle(NULL)
	 ,m_uiCameraUpdate(0)
	 ,m_bOwner(false)
{
}

CLeafCardGeometry::~CLeafCardGeometry()
{
	NiDelete[] m_pkVertex;
	if( m_bOwner )
	{
		NiDelete[] m_pkNormal;
		NiDelete[] m_pkColor;
		NiDelete[] m_pkTexture;
		NiFree(m_pusTriList);

		NiDelete[] m_pkCenter;
		NiDelete[] m_pkCorner;
		NiDelete[] m_pkAngle;
	}
}

void CLeafCardGeometry::OnVisible(NiCullingProcess& kCuller)
{
	if( m_uiCameraUpdate != CSpeedTreeShader::GetCameraUpdate() )
	{
		m_uiCameraUpdate = CSpeedTreeShader::GetCameraUpdate();

		NiMatrix3 kInvRot;
		GetWorldRotate().Inverse(kInvRot);

		for( unsigned short i = 0; i < m_usVertices; i++ )
		{
			CSpeedTreeShader::LeafCardVS(kInvRot, m_pkCenter[i/4], m_pkAngle[i/4], m_pkCorner[i], m_pkVertex[i]);
		}

		if( m_usVertices )
			GetModelData()->MarkAsChanged(NiGeometryData::VERTEX_MASK);
	}

	NiTriShape::OnVisible(kCuller);
}

void CLeafCardGeometry::UpdateWorldData()
{
	NiMatrix3 kRot = GetWorldRotate();
	NiTriShape::UpdateWorldData();
	if( kRot != GetWorldRotate() )
	{
		m_uiCameraUpdate = 0;
	}
}

//---------------------------------------------------------------------------
// cloning
//---------------------------------------------------------------------------
NiImplementCreateClone(CLeafCardGeometry);
//---------------------------------------------------------------------------
void CLeafCardGeometry::CopyMembers(CLeafCardGeometry* pkDest, NiCloningProcess& kCloning)
{
    NiTriShape::CopyMembers(pkDest, kCloning);

	NiPoint3* pkVertex = NiNew NiPoint3[m_usVertices];
	memcpy(pkVertex, m_pkVertex, sizeof(NiPoint3)*m_usVertices);

	pkDest->m_usVertices = m_usVertices;
	pkDest->m_pkVertex = pkVertex;
	pkDest->m_pkCenter = m_pkCenter;
	pkDest->m_pkCorner = m_pkCorner;
	pkDest->m_pkAngle = m_pkAngle;
	pkDest->m_uiCameraUpdate = m_uiCameraUpdate;

	CLeafCardGeometryData* pkGeometryData = 
		NiNew CLeafCardGeometryData(m_usVertices, pkVertex,
			       m_pkNormal, m_pkColor, m_pkTexture,
				   1, NiGeometryData::NBT_METHOD_NONE,
				   m_usVertices/3, m_pusTriList);

	//not share geometry data
	pkDest->SetModelData(pkGeometryData);
}

// unit leaf card that's turned towards the camera and wind-rocked/rustled by the
// vertex shader.  card is aligned on YZ plane and centered at (0.0f, 0.0f, 0.0f)
const NiPoint3  g_mLeafUnitSquare[4] =                  
{
	NiPoint3(0.0f,  0.5f,  0.5f), 
    NiPoint3(0.0f, -0.5f,  0.5f), 
    NiPoint3(0.0f, -0.5f, -0.5f), 
    NiPoint3(0.0f,  0.5f, -0.5f)
};

NiAVObject* CLeafCardGeometry::Create(CSpeedTreeRT& kSpeedTree, const char* pcPath, NiEntityErrorInterface* pkErrorHandler)
{
	// query vertex attribute data
	CSpeedTreeRT::SGeometry sGeometry;
	kSpeedTree.GetGeometry(sGeometry, SpeedTree_LeafGeometry);

	CSpeedTreeRT::SMapBank sMapBank;
	kSpeedTree.GetMapBank(sMapBank);

	if( sGeometry.m_nNumLeafLods <= 0 )
		return NULL;

	const CSpeedTreeRT::SGeometry::SLeaf& sLeaves = sGeometry.m_pLeaves[0];

	if (sLeaves.m_nNumLeaves <= 0)
	{
		return NULL;
	}

	const CSpeedTreeRT::SGeometry::SLeaf::SCard* pCard;
	unsigned short usNumCard = 0;
	for (int iLeaf = 0; iLeaf < sLeaves.m_nNumLeaves; ++iLeaf)
	{
		pCard = sLeaves.m_pCards + sLeaves.m_pLeafCardIndices[iLeaf];
		if(pCard->m_pMesh) //LeafMesh
			continue;

		usNumCard++;
	}

	if( usNumCard == 0 )
		return NULL;

	//vertex
	NiPoint3* pkVertex = NiNew NiPoint3[usNumCard*4];
	NiPoint3* pkVertexPtr = pkVertex;

	//normal
	NiPoint3* pkNormal = NiNew NiPoint3[usNumCard*4];
	NiPoint3* pkNormalPtr = pkNormal;

	//texture coords
	NiPoint2* pkTexCoords = NiNew NiPoint2[usNumCard*4];
	NiPoint2* pkTexCoordsPtr = pkTexCoords;

	//Center
	NiPoint3* pkCenter = NiNew NiPoint3[usNumCard];
	NiPoint3* pkCenterPtr = pkCenter;

	//Corner
	NiPoint3* pkCorner = NiNew NiPoint3[usNumCard*4];
	NiPoint3* pkCornerPtr = pkCorner;

	//Angle
	NiPoint2* pkAngle = NiNew NiPoint2[usNumCard];
	NiPoint2* pkAnglePtr = pkAngle;

	NiPoint3 vPivotPoint;
	NiPoint3 vPivotedPoint;
	for (int iLeaf = 0; iLeaf < sLeaves.m_nNumLeaves; ++iLeaf)
	{
		pCard = sLeaves.m_pCards + sLeaves.m_pLeafCardIndices[iLeaf];
		if(pCard->m_pMesh) //LeafMesh
			continue;

		NiMatrix3 kRot;
		kRot.MakeIdentity();
		*pkAnglePtr = NiPoint2(pCard->m_afAngleOffsets[0], pCard->m_afAngleOffsets[1])*(NI_PI / 180.0f);
		*pkCenterPtr = *(NiPoint3*)(sLeaves.m_pCenterCoords + iLeaf * 3);
		for( int iCorner = 0; iCorner < 4; ++iCorner )
		{
			*pkNormalPtr = *(NiPoint3*)(sLeaves.m_pNormals + iLeaf * 12 + iCorner * 3);
			*pkTexCoordsPtr = *(NiPoint2*)(pCard->m_pTexCoords + iCorner * 2);

			vPivotPoint = NiPoint3(0.f, pCard->m_afPivotPoint[0] - 0.5f, pCard->m_afPivotPoint[1] - 0.5f);
			vPivotedPoint = g_mLeafUnitSquare[iCorner] + vPivotPoint;
			*pkCornerPtr = NiPoint3(vPivotedPoint.x*pCard->m_fWidth, vPivotedPoint.y*pCard->m_fWidth, vPivotedPoint.z*pCard->m_fHeight);

			CSpeedTreeShader::LeafCardVS(kRot, *pkCenterPtr, *pkAnglePtr, *pkCornerPtr, *pkVertexPtr);

			++pkVertexPtr;
			++pkNormalPtr;
			++pkTexCoordsPtr;
			++pkCornerPtr;
		}
		++pkCenterPtr;
		++pkAnglePtr;
	}

	NIASSERT(pkVertexPtr == pkVertex + usNumCard*4);

	//Indices
	const unsigned short ausVertIndices[6] = { 0, 2, 1, 0, 3, 2 };
	unsigned short* pusTriList = NiAlloc(unsigned short, usNumCard*6);
	unsigned short* pusTriListPtr = pusTriList;
	for( unsigned short i = 0; i < usNumCard; i++ )
	{
		*pusTriListPtr++ = i*4 + ausVertIndices[0];
		*pusTriListPtr++ = i*4 + ausVertIndices[1];
		*pusTriListPtr++ = i*4 + ausVertIndices[2];
		*pusTriListPtr++ = i*4 + ausVertIndices[3];
		*pusTriListPtr++ = i*4 + ausVertIndices[4];
		*pusTriListPtr++ = i*4 + ausVertIndices[5];
	}

	//Geometry
	NiTriShape* pkGeometry = NiNew CLeafCardGeometry(unsigned short(usNumCard*4), pkVertex, pkNormal, NULL, pkTexCoords, 1, NiGeometryData::NBT_METHOD_NONE, usNumCard*2, pusTriList, pkCenter, pkCorner, pkAngle);

	//base map
	NiTexturingProperty::Map* pkBaseMap = NULL;
	if(sMapBank.m_pCompositeMaps[CSpeedTreeRT::TL_DIFFUSE] && sMapBank.m_pCompositeMaps[CSpeedTreeRT::TL_DIFFUSE][0])
	{
		NiFilename kTexName(sMapBank.m_pCompositeMaps[CSpeedTreeRT::TL_DIFFUSE]);
		char acFilename[1024];
		NiSprintf(acFilename, sizeof(acFilename), "%s%s%s", pcPath, kTexName.GetFilename(), kTexName.GetExt());
		NiPixelData* pkPixelData = CTextureManager::Get()->Load(acFilename);
		if( pkPixelData )
		{
			NiTexture* pkTexture = NiSourceTexture::Create(pkPixelData);
			if( pkTexture )
			{
				pkBaseMap = NiNew NiTexturingProperty::Map;
				pkBaseMap->SetTexture(pkTexture);
				pkBaseMap->SetClampMode(NiTexturingProperty::WRAP_S_WRAP_T);
				pkBaseMap->SetTextureIndex(0);
			}
		}
		else
		{
			pkErrorHandler->ReportError("LeafCard diffuse texture load failed.", acFilename, "", "");
		}
	}
	else
	{
		pkErrorHandler->ReportError("No LeafCard diffuse texture", pcPath, "", "");
	}


	//Texture property
	if( pkBaseMap )
	{
		NiTexturingProperty* pkTexturingProp = NiNew NiTexturingProperty;
		pkTexturingProp->SetBaseMap(pkBaseMap);
		pkGeometry->AttachProperty(pkTexturingProp);
	}

	CSpeedTreeRT::SLodValues sLodValues;
	kSpeedTree.GetLodValues(sLodValues, 1.f);

	//Alpha property
	NiAlphaProperty* pkAlphaProperty = NiNew NiAlphaProperty;//todo... use static NiAlphaProperty
	pkAlphaProperty->SetAlphaTesting(true);
	pkAlphaProperty->SetTestMode(NiAlphaProperty::TEST_GREATER);
	pkAlphaProperty->SetTestRef((unsigned char)sLodValues.m_afLeafAlphaTestValues[0]);
	pkGeometry->AttachProperty(pkAlphaProperty);

	return pkGeometry;
}

NiAVObject* CLeafMeshGeometry::Create(CSpeedTreeRT& kSpeedTree, const char* pcPath, NiEntityErrorInterface* pkErrorHandler)
{
	// query vertex attribute data
	CSpeedTreeRT::SGeometry sGeometry;
	kSpeedTree.GetGeometry(sGeometry, SpeedTree_LeafGeometry);

	CSpeedTreeRT::SMapBank sMapBank;
	kSpeedTree.GetMapBank(sMapBank);

	if( sGeometry.m_nNumLeafLods <= 0 )
		return NULL;

	const CSpeedTreeRT::SGeometry::SLeaf& sLeaves = sGeometry.m_pLeaves[0];

	if (sLeaves.m_nNumLeaves <= 0)
	{
		return NULL;
	}

	const CSpeedTreeRT::SGeometry::SLeaf::SCard* pCard;
	const CSpeedTreeRT::SGeometry::SLeaf::SMesh* pMesh;
	unsigned short usTotalVertices = 0;
	unsigned short usTotalIndices = 0;
	for (int iLeaf = 0; iLeaf < sLeaves.m_nNumLeaves; ++iLeaf)
	{
		pCard = sLeaves.m_pCards + sLeaves.m_pLeafCardIndices[iLeaf];
		if(pCard->m_pMesh)
		{
			usTotalVertices = usTotalVertices + (unsigned short)(pCard->m_pMesh->m_nNumVertices);
			usTotalIndices = usTotalIndices + (unsigned short)(pCard->m_pMesh->m_nNumIndices);
		}
	}

	if( usTotalVertices == 0 )
		return NULL;

	//vertex
	NiPoint3* pkVertex = NiNew NiPoint3[usTotalVertices];
	NiPoint3* pkVertexPtr = pkVertex;

	//normal
	NiPoint3* pkNormal = NiNew NiPoint3[usTotalVertices];
	NiPoint3* pkNormalPtr = pkNormal;

	//texture coords
	NiPoint2* pkTexCoords;
	pkTexCoords	= NiNew NiPoint2[usTotalVertices];
	NiPoint2* pkTexCoordsPtr = pkTexCoords;

	//Indices
	unsigned short* pusTriList = NiAlloc(unsigned short, usTotalIndices);
	unsigned short* pusTriListPtr = pusTriList; 

	unsigned short usVertexStart;
	NiPoint3 kCenter;
	NiMatrix3 kRot;
	for (int iLeaf = 0; iLeaf < sLeaves.m_nNumLeaves; ++iLeaf)
	{
		pCard = sLeaves.m_pCards + sLeaves.m_pLeafCardIndices[iLeaf];
		pMesh = pCard->m_pMesh;
		if(pMesh)
		{
			kCenter = *(NiPoint3*)(sLeaves.m_pCenterCoords + iLeaf*3);
			usVertexStart = unsigned short(pkVertexPtr - pkVertex);
			kRot.SetCol(0, sLeaves.m_pTangents[iLeaf*12 + 0], sLeaves.m_pBinormals[iLeaf*12 + 0], sLeaves.m_pNormals[iLeaf*12 + 0]);//X
			kRot.SetCol(1, sLeaves.m_pTangents[iLeaf*12 + 1], sLeaves.m_pBinormals[iLeaf*12 + 1], sLeaves.m_pNormals[iLeaf*12 + 1]);//Y
			kRot.SetCol(2, sLeaves.m_pTangents[iLeaf*12 + 2], sLeaves.m_pBinormals[iLeaf*12 + 2], sLeaves.m_pNormals[iLeaf*12 + 2]);//Z

			for( int i = 0; i < pMesh->m_nNumVertices; ++i )
			{
				//vertex
				*pkVertexPtr = *(NiPoint3*)(pMesh->m_pCoords + i*3);
				*pkNormalPtr = *(NiPoint3*)(pMesh->m_pNormals + i*3);
				*pkTexCoordsPtr = *(NiPoint2*)(pMesh->m_pTexCoords + i*2);

				*pkVertexPtr = kRot * (*pkVertexPtr) + kCenter;
				*pkNormalPtr = kRot * (*pkNormalPtr);
				

				++pkVertexPtr;
				++pkNormalPtr;
				++pkTexCoordsPtr;
			}

			for( unsigned short i = 0; i < pMesh->m_nNumIndices; i++ )
			{
				*pusTriListPtr++ = (unsigned short)pMesh->m_pIndices[i] + usVertexStart;
			}
		}
	}

	//Geometry
	NiTriShape* pkGeometry = NiNew NiTriShape(usTotalVertices, pkVertex, pkNormal, NULL, pkTexCoords, 1, NiGeometryData::NBT_METHOD_NONE, usTotalIndices/3, pusTriList);

	//base map
	NiTexturingProperty::Map* pkBaseMap = NULL;
	if(sMapBank.m_pCompositeMaps[CSpeedTreeRT::TL_DIFFUSE] && sMapBank.m_pCompositeMaps[CSpeedTreeRT::TL_DIFFUSE][0])
	{
		NiFilename kTexName(sMapBank.m_pCompositeMaps[CSpeedTreeRT::TL_DIFFUSE]);
		char acFilename[1024];
		NiSprintf(acFilename, sizeof(acFilename), "%s%s%s", pcPath, kTexName.GetFilename(), kTexName.GetExt());
		NiPixelData* pkPixelData = CTextureManager::Get()->Load(acFilename);
		if( pkPixelData )
		{
			NiTexture* pkTexture = NiSourceTexture::Create(pkPixelData);
			if( pkTexture )
			{
				pkBaseMap = NiNew NiTexturingProperty::Map;
				pkBaseMap->SetTexture(pkTexture);
				pkBaseMap->SetClampMode(NiTexturingProperty::WRAP_S_WRAP_T);
				pkBaseMap->SetTextureIndex(0);
			}
		}
		else
		{
			pkErrorHandler->ReportError("LeafMesh diffuse texture load failed.", acFilename, "", "");
		}
	}
	else
	{
		pkErrorHandler->ReportError("No LeafMesh diffuse texture", pcPath, "", "");
	}


	//Texture property
	if( pkBaseMap )
	{
		NiTexturingProperty* pkTexturingProp = NiNew NiTexturingProperty;
		pkTexturingProp->SetBaseMap(pkBaseMap);
		pkGeometry->AttachProperty(pkTexturingProp);
	}

	CSpeedTreeRT::SLodValues sLodValues;
	kSpeedTree.GetLodValues(sLodValues, 1.f);

	//Alpha property
	NiAlphaProperty* pkAlphaProperty = NiNew NiAlphaProperty;//todo... use static NiAlphaProperty
	pkAlphaProperty->SetAlphaTesting(true);
	pkAlphaProperty->SetTestMode(NiAlphaProperty::TEST_GREATER);
	pkAlphaProperty->SetTestRef((unsigned char)sLodValues.m_afLeafAlphaTestValues[0]);
	pkGeometry->AttachProperty(pkAlphaProperty);

	NiStencilProperty* pkStencilProperty = NiNew NiStencilProperty;
	pkStencilProperty->SetDrawMode(NiStencilProperty::DRAW_BOTH);
	pkGeometry->AttachProperty(pkStencilProperty);

	return pkGeometry;
}