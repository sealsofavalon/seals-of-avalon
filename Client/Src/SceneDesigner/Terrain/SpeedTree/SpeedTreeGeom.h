#pragma once

//todo... suport LOD
class CSpeedTreeRT;
class CSpeedTreeNode : public NiNode
{
public:
	static NiNode* Create(const char* pcAssetName, NiEntityErrorInterface* pkErrorHandler);
};

class CBranchGeometry : public NiTriStrips
{
public:
	static NiAVObject* Create(CSpeedTreeRT& pkSpeedTree, const char* pcPath, NiEntityErrorInterface* pkErrorHandler);
};

class CFrondGeometry : public NiTriStrips
{
public:
	static NiAVObject* Create(CSpeedTreeRT& pkSpeedTree, const char* pcPath, NiEntityErrorInterface* pkErrorHandler);
};

class CLeafCardGeometryData : public NiTriShapeData
{
public:
	 CLeafCardGeometryData(unsigned short usVertices, NiPoint3* pkVertex,
        NiPoint3* pkNormal, NiColorA* pkColor, NiPoint2* pkTexture,
        unsigned short usNumTextureSets, NiGeometryData::DataFlags eNBTMethod,
		unsigned short usTriangles, unsigned short* pusTriList) :
		NiTriShapeData(usVertices, pkVertex, pkNormal, pkColor, pkTexture, 
			usNumTextureSets, eNBTMethod, 
			usTriangles, pusTriList)
	 {
	 }

	virtual ~CLeafCardGeometryData()
	{
		m_pkVertex = NULL;
		m_pkNormal = NULL;
		m_pkColor = NULL;
		m_pkTexture = NULL;
		m_pusTriList = NULL;
	}
};

class CLeafCardGeometry : public NiTriShape
{
	NiDeclareClone(CLeafCardGeometry);

	unsigned short m_usVertices;
	NiPoint3* m_pkVertex;
	NiPoint3* m_pkNormal;
	NiColorA* m_pkColor;
	NiPoint2* m_pkTexture;
	unsigned short* m_pusTriList;

	NiPoint3* m_pkCenter;
	NiPoint3* m_pkCorner;
	NiPoint2* m_pkAngle;
	unsigned int m_uiCameraUpdate;
	NiMatrix3 m_kRotate;
	bool m_bOwner;

public:
	CLeafCardGeometry(unsigned short usVertices, NiPoint3* pkVertex,
        NiPoint3* pkNormal, NiColorA* pkColor, NiPoint2* pkTexture, 
        unsigned short usNumTextureSets, NiGeometryData::DataFlags eNBTMethod,
        unsigned short usTriangles, unsigned short* pusTriList, 
		NiPoint3* pkCenter, NiPoint3* pkCorner, NiPoint2* pkAngle);

	virtual ~CLeafCardGeometry();

	virtual void OnVisible(NiCullingProcess& kCuller);
	virtual void UpdateWorldData();

	//todo... suport clone

	static NiAVObject* Create(CSpeedTreeRT& pkSpeedTree, const char* pcPath, NiEntityErrorInterface* pkErrorHandler);

protected:
	CLeafCardGeometry();
};

class CLeafMeshGeometry : public NiTriShape
{
public:
	static NiAVObject* Create(CSpeedTreeRT& pkSpeedTree, const char* pcPath, NiEntityErrorInterface* pkErrorHandler);
};