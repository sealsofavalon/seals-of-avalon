#include "TerrainPCH.h"

#include "SpeedTreeShader.h"

//Converted from speedtree.fx


NiPoint3 CSpeedTreeShader::ms_kCameraAngle;
unsigned int CSpeedTreeShader::ms_uiCameraUpdate;

void CSpeedTreeShader::UpdateCamera(const NiCamera* pkCamera)
{
	float x, y, z;
	pkCamera->GetWorldRotate().ToEulerAnglesXYZ(x, y, z);
	if( x < 0.f )
		x += NI_TWO_PI;
	if( y < 0.f )
		y += NI_TWO_PI;
	if( z < 0.f )
		z += NI_TWO_PI;

	if( fabs(x - ms_kCameraAngle.x) < 0.01f 
	 && fabs(y - ms_kCameraAngle.y) < 0.01f
	 && fabs(z - ms_kCameraAngle.z) < 0.01f )
		return;

	ms_kCameraAngle.x = x;
	ms_kCameraAngle.y = y;
	ms_kCameraAngle.z = z;

	ms_uiCameraUpdate++;
}

unsigned int CSpeedTreeShader::GetCameraUpdate()
{
	return ms_uiCameraUpdate;
}

void CSpeedTreeShader::LeafCardVS(const NiMatrix3& kInvRot, const NiPoint3& kCenter, const NiPoint2& kAngle, const NiPoint3& kCorner, NiPoint3& kPos)
{
	NiMatrix3 kMat;
	kMat.FromEulerAnglesXYZ(-ms_kCameraAngle.x, kAngle.y - ms_kCameraAngle.y, kAngle.x - ms_kCameraAngle.z);
	kPos = kCenter + kInvRot*(kMat*kCorner);
}

void CSpeedTreeShader::LeafMeshVS(const NiPoint3& kOrientX, const NiPoint3& kOrientZ, const NiPoint3& kOffset, NiPoint3& kPos, NiPoint3& kNormal)
{
	NiPoint3 kOrientY = kOrientX.Cross(kOrientZ);
	NiMatrix3 kMat(kOrientY, kOrientZ, kOrientX);

	kPos = kMat*kPos;
	kNormal = kMat*kPos;
	kPos += kOffset;
}


void CSpeedTreeShader::_SDMInit()
{
	ms_kCameraAngle = NiPoint3(0.f, 0.f, 0.f);
	ms_uiCameraUpdate = 0x123456;
}

void CSpeedTreeShader::_SDMShutdown()
{
}


