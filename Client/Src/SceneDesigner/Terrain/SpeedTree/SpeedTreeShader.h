
#pragma once

class CSpeedTreeShader : public NiMemObject
{
public:
	static void UpdateCamera(const NiCamera* pkCamera);
	static unsigned int GetCameraUpdate();

	static void LeafCardVS(const NiMatrix3& kInvRot, const NiPoint3& kCenter, const NiPoint2& kAngle, const NiPoint3& kCorner, NiPoint3& kPos);
	static void LeafMeshVS(const NiPoint3& kOrientX, const NiPoint3& kOrientY, const NiPoint3& kOffset, NiPoint3& kPos, NiPoint3& kNormal);

	static void _SDMInit();
	static void _SDMShutdown();

private:
	static NiPoint3 ms_kCameraAngle;
	static unsigned int ms_uiCameraUpdate;
};