#include "TerrainPCH.h"
#include "TerrainHelper.h"
#include "SplinePath.h"

//-----------------------------------------------------------------------------

CSplinePath::Knot::Knot(const Knot &k)
{
   m_kPosition = k.m_kPosition;
   m_kRotation = k.m_kRotation;
   m_fSpeed    = k.m_fSpeed;
   m_eType = k.m_eType;
   m_ePath = k.m_ePath;
   m_pkPrev = NULL; 
   m_pkNext = NULL;
}

CSplinePath::Knot::Knot(const NiPoint3 &p, const NiQuaternion &r, float s, Knot::Type eType, Knot::Path ePath)
{
   m_kPosition = p;
   m_kRotation = r;
   m_fSpeed    = s;
   m_eType = eType;
   m_ePath = ePath;
   m_pkPrev = NULL; 
   m_pkNext = NULL;
}


//-----------------------------------------------------------------------------

CSplinePath::CSplinePath()
{
   m_pkFront = NULL;
   m_uiSize = 0;
   m_bMapDirty = true;
}


CSplinePath::~CSplinePath()
{
   RemoveAll();
}

void CSplinePath::PushBack(Knot *w)
{
   if (!m_pkFront)
   {
      m_pkFront = w;
      w->m_pkNext = w;
      w->m_pkPrev = w;
   }
   else
   {
      Knot *pkBefore = Back();
      Knot *pkAfter  = pkBefore->m_pkNext;

      w->m_pkNext = pkBefore->m_pkNext;
      w->m_pkPrev = pkBefore;
      pkAfter->m_pkPrev = w;
      pkBefore->m_pkNext = w;
   }
   ++m_uiSize;
   m_bMapDirty = true;
}

CSplinePath::Knot* CSplinePath::GetKnot(unsigned int i)
{
   Knot *k = m_pkFront;
   while(i--)
      k = k->m_pkNext;
   return k;
}

CSplinePath::Knot* CSplinePath::Remove(Knot *w)
{
   if (w->m_pkNext == m_pkFront && w->m_pkPrev == m_pkFront)
      m_pkFront = NULL;
   else
   {
      w->m_pkPrev->m_pkNext = w->m_pkNext;
      w->m_pkNext->m_pkPrev = w->m_pkPrev;
      if (m_pkFront == w)
         m_pkFront = w->m_pkNext;
   }
   --m_uiSize;
   m_bMapDirty = true;
   return w;
}


void CSplinePath::RemoveAll()
{
   while(Front())
      NiDelete Remove(Front());
  m_uiSize = 0;
}


//-----------------------------------------------------------------------------
void CSplinePath::BuildTimeMap()
{
   if (!m_bMapDirty)
      return;

   m_kTimeMaps.clear();
   m_kTimeMaps.reserve(GetSize()*3);      // preallocate

   // Initial node and knot value..
   TimeMap kTimeMap;
   kTimeMap.m_fTime = 0.f;
   kTimeMap.m_fDistance = 0.f;
   m_kTimeMaps.push_back(kTimeMap);

   Knot ka,kj,ki;
   Value(0, &kj, true);
   float fLength = 0.f;
   ka = kj;

   // Loop through the knots and add nodes. Nodes are added for every knot and
   // whenever the spline length and segment length deviate by epsilon.
   float fEpsilon = 0.90f;
   const float fStep = 0.05f;
   float lt = 0.f,fTime = 0.f;
   do  
   {
      if ((fTime += fStep) + 1 > m_uiSize)
         fTime = float(m_uiSize - 1);

      Value(fTime, &ki, true);
	  fLength += (ki.m_kPosition - kj.m_kPosition).Length();
      float fSegment = (ki.m_kPosition - ka.m_kPosition).Length();

      if ((fSegment / fLength) < fEpsilon || fTime == (m_uiSize - 1) || NiFloor(lt) != NiFloor(fTime)) 
	  {
         kTimeMap.m_fTime = fTime;
         kTimeMap.m_fDistance = fLength;
         m_kTimeMaps.push_back(kTimeMap);
         ka = ki;
      }
      kj = ki;
      lt = fTime;
   }
   while (fTime + 1.f<m_uiSize);

   m_bMapDirty = false;
}

//-----------------------------------------------------------------------------

float CSplinePath::AdvanceTime(float fTime, float fDeltaTime)
{
   BuildTimeMap();
   Knot k;
   Value(fTime, &k, false);
   float fDist = GetDistance(fTime) + k.m_fSpeed * fDeltaTime;
   return GetTime(fDist);
}


float CSplinePath::AdvanceDist(float fTime, float fMeters)
{
   BuildTimeMap();
   float fDist = GetDistance(fTime) + fMeters;
   return GetTime(fDist);
}


float CSplinePath::GetDistance(float fTime)
{
   if (m_uiSize <= 1)
      return 0.f;

   // Find the nodes spanning the time
   std::vector<TimeMap>::iterator end = m_kTimeMaps.begin() + 1, start;
   for (; end < (m_kTimeMaps.end() - 1) && (end->m_fTime < fTime); end++)
   {
   }
   start = end - 1;

   // Interpolate between the two nodes
   float i = (fTime - start->m_fTime) / (end->m_fTime - start->m_fTime);
   return start->m_fDistance + (end->m_fDistance - start->m_fDistance) * i;
}


float CSplinePath::GetTime(float d)
{
   if (m_uiSize <= 1)
      return 0.f;

   // Find the nodes spanning the time
   std::vector<TimeMap>::iterator end = m_kTimeMaps.begin() + 1, start;
   for (; end < (m_kTimeMaps.end() - 1) && (end->m_fDistance < d); end++)
   {
   }
   start = end - 1;

   // Check for duplicate points..
   float fSeg = end->m_fDistance - start->m_fDistance;
   if (!fSeg)
      return end->m_fTime;

   // Interpolate between the two nodes
   float i = (d - start->m_fDistance) / (end->m_fDistance - start->m_fDistance);
   return start->m_fTime + (end->m_fTime - start->m_fTime) * i;
}


//-----------------------------------------------------------------------------

void CSplinePath::Value(float fTime, CSplinePath::Knot *pkResult, bool bSkipRotation)
{
   // Verify that t is in range [0 >= t > size]
   NIASSERT(fTime >= 0.f && fTime < GetSize() && "t out of range");
   Knot *p1 = GetKnot((unsigned int)NiFloor(fTime));
   Knot *p2 = Next(p1);

   float i = fTime - NiFloor(fTime);  // adjust t to 0 to 1 on p1-p2 interval
   if (p1->m_ePath == Knot::SPLINE)
   {
      Knot *p0 = (p1->m_eType == Knot::KINK) ? p1 : Prev(p1);
      Knot *p3 = (p2->m_eType == Knot::KINK) ? p2 : Next(p2);
      pkResult->m_kPosition.x = TerrainHelper::CatmullRom(i, p0->m_kPosition.x, p1->m_kPosition.x, p2->m_kPosition.x, p3->m_kPosition.x);
      pkResult->m_kPosition.y = TerrainHelper::CatmullRom(i, p0->m_kPosition.y, p1->m_kPosition.y, p2->m_kPosition.y, p3->m_kPosition.y);
      pkResult->m_kPosition.z = TerrainHelper::CatmullRom(i, p0->m_kPosition.z, p1->m_kPosition.z, p2->m_kPosition.z, p3->m_kPosition.z);
   }
   else
   {   // Linear
      pkResult->m_kPosition = p1->m_kPosition * (1 - i) + p2->m_kPosition * i;
   }

   if(bSkipRotation)
      return;

   BuildTimeMap();

   // find the two knots to interpolate rotation and velocity through since some
   // knots are only positional
   unsigned int uiStart = (unsigned int)NiFloor(fTime);
   unsigned int uiEnd   = (p2 == p1) ? uiStart : (uiStart + 1);
   while (p1->m_eType == Knot::POSITION_ONLY && p1 != Front())
   {
      p1 = Prev(p1);
      uiStart--;
   }

   while (p2->m_eType == Knot::POSITION_ONLY && p2 != Back())
   {
      p2 = Next(p2);
      uiEnd++;
   }

   if (uiStart == uiEnd) 
   {
      pkResult->m_kRotation = p1->m_kRotation;
      pkResult->m_fSpeed = p1->m_fSpeed;
   }
   else 
   {
      float c = GetDistance(fTime);
      float d1 = GetDistance((float)uiStart);
      float d2 = GetDistance((float)uiEnd);
      if (d1 == d2) 
	  {
         pkResult->m_kRotation = p2->m_kRotation;
         pkResult->m_fSpeed = p2->m_fSpeed;
      }
      else 
	  {
         i  = (c-d1)/(d2-d1);
		 pkResult->m_kRotation = NiQuaternion::Slerp(i, p1->m_kRotation, p2->m_kRotation);
         pkResult->m_fSpeed = p1->m_fSpeed * (1.0f-i) + (p2->m_fSpeed * i);
      }
   }
}



