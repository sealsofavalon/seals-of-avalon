
//----------------------------------------------------------------------------
class CSplinePath
{
public:
	struct Knot :  public NiMemObject
	{
	public:
		NiPoint3 m_kPosition;
		NiQuaternion m_kRotation;
		float m_fSpeed;    /// in meters per second
		enum Type
		{
			NORMAL,
			POSITION_ONLY,
			KINK,
			NUM_TYPE_BITS = 2
		}m_eType;

		enum Path 
		{
			LINEAR,
			SPLINE,
			NUM_PATH_BITS = 1
		}m_ePath;

		float m_fDistance;
		Knot *m_pkPrev;
		Knot *m_pkNext;

		Knot::Knot() {};
		Knot::Knot(const Knot &k);
		Knot::Knot(const NiPoint3 &p, const NiQuaternion &r, float s, Knot::Type eType = NORMAL, Knot::Path ePath = SPLINE);
	};

   CSplinePath();
   ~CSplinePath();

   bool IsEmpty() { return (m_pkFront == NULL); }
   unsigned int GetSize() { return m_uiSize; }
   Knot* Remove(Knot *w);
   void RemoveAll();

   Knot* Front()  { return m_pkFront; }
   Knot* Back()   { return (m_pkFront == NULL) ? NULL : m_pkFront->m_pkPrev; }

   void PushBack(Knot *w);
   void PushFront(Knot *w) { PushBack(w); m_pkFront = w; m_bMapDirty = true; }

   Knot* GetKnot(unsigned int i);
   Knot* Next(Knot *k) { return (k->m_pkNext == m_pkFront) ? k : k->m_pkNext; }
   Knot* Prev(Knot *k) { return (k == m_pkFront) ? k : k->m_pkPrev; }

   float AdvanceTime(float fTime, float fDeltaTime);
   float AdvanceDist(float fTime, float fMeters);
   void Value(float t, Knot *pkResult, bool bSkipRotation);

   float GetDistance(float t);
   float GetTime(float d);

private:
   Knot *m_pkFront;
   unsigned int m_uiSize;
   bool m_bMapDirty;

   struct TimeMap 
   {
      float m_fTime;
      float m_fDistance;
   };

   std::vector<TimeMap> m_kTimeMaps;
   void BuildTimeMap();
};