#include "TerrainPCH.h"
#include "StateMachine.h"
#include "Player.h"

//----------------------------------------------------------------------------------------------
CStateMachine::CStateMachine()
	:m_pkCurrentState(NULL)
{
	m_apkStates[STATE_JUMP] = NiNew CJumpState(this);
	m_apkStates[STATE_MOVE] = NiNew CMoveState(this);
	m_apkStates[STATE_STAND] = NiNew CStandState(this);
	m_apkStates[STATE_WAIT] = NiNew CWaitState(this);
	m_apkStates[STATE_CUSTOM_ANIMATION] = NiNew CCustomAnimationState(this);
}

CStateMachine::~CStateMachine()
{
	if(m_pkCurrentState)
	{
		m_pkCurrentState->OnEnd();
		m_pkCurrentState = NULL;
	}

	NiDelete m_apkStates[STATE_JUMP];
	NiDelete m_apkStates[STATE_MOVE];
	NiDelete m_apkStates[STATE_STAND];
	NiDelete m_apkStates[STATE_WAIT];
	NiDelete m_apkStates[STATE_CUSTOM_ANIMATION];
}

void CStateMachine::FireEvent(StateEvent eEvent, ...)
{
	NIASSERT(m_pkCurrentState != NULL);

	va_list kArgList;
	va_start(kArgList, eEvent);

	m_pkCurrentState->OnEvent(eEvent, kArgList);

	va_end(kArgList);
}

void CStateMachine::GoToState(StateType eType)
{
	NIASSERT(eType >= 0 && eType < STATE_COUNT);

	if(m_pkCurrentState)
		m_pkCurrentState->OnEnd();

	m_pkCurrentState = m_apkStates[eType];
	m_pkCurrentState->OnStart();
}

void CStateMachine::Update(float fDeltaTime)
{
	if(m_pkCurrentState)
		m_pkCurrentState->OnUpdate(fDeltaTime);
}


//----------------------------------------------------------------------------------------------
CJumpState::CJumpState(CStateMachine* pkStateMachine)
	:CState(pkStateMachine)
{
}

void CJumpState::OnStart()
{
	m_eJumpState = JS_START;
	g_pkPlayer->PlayJumpStartAnimation();
}

void CJumpState::OnEnd()
{
}

void CJumpState::OnEvent(StateEvent eEvent, va_list kArgList)
{
	switch(eEvent)
	{
	case EVENT_ANIMATION_END:
		if(m_eJumpState == JS_START)
		{
			// 由起跳动作转到下落动作
			m_eJumpState = JS_FALL;
			g_pkPlayer->PlayJumpFallAnimation();
		}
		else if(m_eJumpState == JS_END)
		{
			if( g_pkPlayer->IsMoving() )
			{
				m_pkStateMachine->GoToState(STATE_MOVE);
			}
			else
			{
				m_pkStateMachine->GoToState(STATE_STAND);
			}
		}
		break;

	case EVENT_JUMP_END:
		m_eJumpState = JS_END;
		if( g_pkPlayer->IsMoving() )
		{
			g_pkPlayer->PlayJumpEndRunAnimation();
		}
		else
		{
			g_pkPlayer->PlayJumpEndAnimation();
		}
		break;

	case EVENT_CUSTOM_ANIMATION:
		m_pkStateMachine->GoToState(STATE_CUSTOM_ANIMATION);
		break;
	}
}

void CJumpState::OnUpdate(float fDeltaTime)
{
}


//----------------------------------------------------------------------------------------------
CMoveState::CMoveState(CStateMachine* pkStateMachine)
	:CState(pkStateMachine)
{
}

void CMoveState::OnStart()
{
	if ( g_pkPlayer->IsBackward() )
	{
		g_pkPlayer->PlayBackwardAnimation();
	}
	else
	{
		if( g_pkPlayer->GetWalkMode() )
			g_pkPlayer->PlayWalkAnimation();
		else
			g_pkPlayer->PlayRunAnimation();
	}
		
}

void CMoveState::OnEnd()
{
}

void CMoveState::OnEvent(StateEvent eEvent, va_list kArgList)
{
	switch(eEvent)
	{
	case EVENT_MOVE_END:
		m_pkStateMachine->GoToState(STATE_WAIT);
		break;

	case EVENT_JUMP_START:
		m_pkStateMachine->GoToState(STATE_JUMP);
		break;

	case EVENT_CUSTOM_ANIMATION:
		m_pkStateMachine->GoToState(STATE_CUSTOM_ANIMATION);
		break;
	};
}

void CMoveState::OnUpdate(float fDeltaTime)
{
}


//----------------------------------------------------------------------------------------------
CStandState::CStandState(CStateMachine* pkStateMachine)
	:CState(pkStateMachine)
{
}

void CStandState::OnStart()
{
	m_fStandTime = 0.f;
	g_pkPlayer->PlayStandAnimation();
}

void CStandState::OnEnd()
{
}

void CStandState::OnEvent(StateEvent eEvent, va_list kArgList)
{
	switch(eEvent)
	{
	case EVENT_MOVE_BEGIN:
		m_pkStateMachine->GoToState(STATE_MOVE);
		break;

	case EVENT_JUMP_START:
		m_pkStateMachine->GoToState(STATE_JUMP);
		break;

	case EVENT_CUSTOM_ANIMATION:
		m_pkStateMachine->GoToState(STATE_CUSTOM_ANIMATION);
		break;
	}
}

void CStandState::OnUpdate(float fDeltaTime)
{
	m_fStandTime += fDeltaTime;
	if(m_fStandTime > 10.f)
		m_pkStateMachine->GoToState(STATE_WAIT);
}


//----------------------------------------------------------------------------------------------
CWaitState::CWaitState(CStateMachine* pkStateMachine)
	:CState(pkStateMachine)
{
}

void CWaitState::OnStart()
{
	g_pkPlayer->PlayWaitAnimation();
}

void CWaitState::OnEnd()
{
}

void CWaitState::OnEvent(StateEvent eEvent, va_list kArgList)
{
	switch(eEvent)
	{
	case EVENT_ANIMATION_END:
		m_pkStateMachine->GoToState(STATE_STAND);
		break;

	case EVENT_MOVE_BEGIN:
		m_pkStateMachine->GoToState(STATE_MOVE);
		break;

	case EVENT_JUMP_START:
		m_pkStateMachine->GoToState(STATE_JUMP);
		break;

	case EVENT_CUSTOM_ANIMATION:
		m_pkStateMachine->GoToState(STATE_CUSTOM_ANIMATION);
		break;
	}
}

void CWaitState::OnUpdate(float fDeltaTime)
{
}


//----------------------------------------------------------------------------------------------
CCustomAnimationState::CCustomAnimationState(CStateMachine* pkStateMachine)
	:CState(pkStateMachine)
{
}

void CCustomAnimationState::OnStart()
{
}

void CCustomAnimationState::OnEnd()
{
}

void CCustomAnimationState::OnEvent(StateEvent eEvent, va_list kArgList)
{
	switch(eEvent)
	{
	case EVENT_PLAY_ANIMATION:
		{
			NiActorManager::SequenceID kSequenceID = va_arg(kArgList, NiActorManager::SequenceID);
			if(kSequenceID == NiActorManager::INVALID_SEQUENCE_ID)
			{
				if( g_pkPlayer->IsMoving() )
				{
					m_pkStateMachine->GoToState(STATE_MOVE);
				}
				else
				{
					m_pkStateMachine->GoToState(STATE_STAND);
				}
			}
			else
			{
				g_pkPlayer->PlayAnimation(kSequenceID);
			}
		}
		break;

	case EVENT_ANIMATION_END:
		{
			NiActorManager::SequenceID kSequenceID = va_arg(kArgList, NiActorManager::SequenceID);
			g_pkPlayer->ResetAnimation(kSequenceID);
			g_pkPlayer->PlayAnimation(kSequenceID);
		}
		break;
	}
}

void CCustomAnimationState::OnUpdate(float fDeltaTime)
{
}