#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

enum StateEvent
{
	EVENT_MOVE_BEGIN,
	EVENT_MOVE_END,
	EVENT_JUMP_START,
	EVENT_JUMP_FALL,
	EVENT_JUMP_END,
	EVENT_CUSTOM_ANIMATION,
	EVENT_PLAY_ANIMATION,
	EVENT_ANIMATION_END,
};

enum StateType
{
	STATE_JUMP = 0,
	STATE_MOVE,
	STATE_STAND,
	STATE_WAIT,
	STATE_CUSTOM_ANIMATION,
	STATE_COUNT,
};

class CPlayer;
class CState;
class CStateMachine : public NiMemObject
{
private:
	CState* m_pkCurrentState;
	CState* m_apkStates[STATE_COUNT];

public:
	CStateMachine();
	virtual ~CStateMachine();

	void FireEvent(StateEvent eEvent, ...);
	void GoToState(StateType eType);
	void Update(float fDeltaTime);
};

class CState : public NiMemObject
{
protected:
	CStateMachine* m_pkStateMachine;

public:
	CState(CStateMachine* pkStateMachine)
		:m_pkStateMachine(pkStateMachine)
	{
	}

	virtual ~CState() {}

	virtual void OnStart() {}
	virtual void OnEnd() {}
	virtual void OnEvent(StateEvent eEvent, va_list kArgList) {}
	virtual void OnUpdate(float fDeltaTime) {}
};

class CJumpState : public CState
{
private:
	enum JumpState
	{
		JS_START,
		JS_FALL,
		JS_END,
	};

	JumpState m_eJumpState;

public:
	CJumpState(CStateMachine* pkStateMachine);

	virtual void OnStart();
	virtual void OnEnd();
	virtual void OnEvent(StateEvent eEvent, va_list kArgList);
	virtual void OnUpdate(float fDeltaTime);
};

class CMoveState : public CState
{
public:
	CMoveState(CStateMachine* pkStateMachine);

	virtual void OnStart();
	virtual void OnEnd();
	virtual void OnEvent(StateEvent eEvent, va_list kArgList);
	virtual void OnUpdate(float fDeltaTime);
};

class CStandState : public CState
{
private:
	float m_fStandTime;

public:
	CStandState(CStateMachine* pkStateMachine);

	virtual void OnStart();
	virtual void OnEnd();
	virtual void OnEvent(StateEvent eEvent, va_list kArgList);
	virtual void OnUpdate(float fDeltaTime);
};

class CWaitState : public CState
{
public:
	CWaitState(CStateMachine* pkStateMachine);

	virtual void OnStart();
	virtual void OnEnd();
	virtual void OnEvent(StateEvent eEvent, va_list kArgList);
	virtual void OnUpdate(float fDeltaTime);
};

class CCustomAnimationState : public CState
{
public:
	CCustomAnimationState(CStateMachine* pkStateMachine);

	virtual void OnStart();
	virtual void OnEnd();
	virtual void OnEvent(StateEvent eEvent, va_list kArgList);
	virtual void OnUpdate(float fDeltaTime);
};

#endif //STATE_MACHINE_H