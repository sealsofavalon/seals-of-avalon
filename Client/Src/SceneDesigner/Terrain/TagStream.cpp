
#include "TerrainPCH.h"
#include "TagStream.h"
#include "TerrainHelper.h"
#include "EditorInfoComponent.h"
#include "EntityInfoComponent.h"
#include "Map.h"

bool CTagStream::ReadFromStream(NiBinaryStream* pkStream)
{
	NIASSERT(pkStream);

	Reset();
	
	if( pkStream->Read(&m_uiTag, sizeof(unsigned int)) == 0 )
		return false;

	pkStream->Read(&m_uiEnd, sizeof(unsigned int));


	if( m_uiEnd > m_uiAllocSize )
	{
		NiFree(m_pBuffer);

		m_uiAllocSize = m_uiEnd;
		m_pBuffer = NiAlloc(char, m_uiAllocSize);
	}

	unsigned int uiRead = pkStream->Read(m_pBuffer, m_uiEnd);
	NIASSERT( uiRead == m_uiEnd );

	return true;
}

bool CTagStream::WriteToStream(NiBinaryStream* pkStream)
{
	NIASSERT(pkStream);

	pkStream->Write(&m_uiTag, sizeof(unsigned int));
	pkStream->Write(&m_uiEnd, sizeof(unsigned int));
	pkStream->Write(m_pBuffer, m_uiEnd);

	return true;
}

void CTagStream::Reset()
{
	m_uiPos = 0;
	m_uiEnd = 0;
}

bool CTagStream::ReadFixedString(NiFixedString& kString)
{
	kString = 0;

	if( m_uiPos >= m_uiEnd )
		return false;

	kString = &m_pBuffer[m_uiPos];
	m_uiPos += kString.GetLength() + 1;

	return true;
}

bool CTagStream::WriteFixedString(const NiFixedString& kString)
{ 
	Write((const char*)kString, kString.GetLength());
	
	char c = '\0';
	Write(&c, 1);

	return true;
}

NiActorComponent* CTagStream::ReadActorComponent()
{
	NiFixedString kKfmFilePath;
	unsigned int uiActiveSequenceID;
	NiBool bAccumulateTransforms;

	ReadFixedString(kKfmFilePath);
	Read(&uiActiveSequenceID, sizeof(unsigned int));
	Read(&bAccumulateTransforms, sizeof(NiBool));


	NiActorComponent *pkComponent = NiNew NiActorComponent;

	pkComponent->SetKfmFilePath(kKfmFilePath);
	pkComponent->SetActiveSequenceID(uiActiveSequenceID);
	pkComponent->SetAccumulateTransforms(NIBOOL_IS_TRUE(bAccumulateTransforms));

	return pkComponent;
}

void CTagStream::WriteActorComponent(NiActorComponent* pkComponent)
{
	NiFixedString kKfmFilePath;
	unsigned int uiActiveSequenceID;
	NiBool bAccumulateTransforms;

	kKfmFilePath = pkComponent->GetKfmFilePath();
	uiActiveSequenceID = pkComponent->GetActiveSequenceID();
	bAccumulateTransforms = pkComponent->GetAccumulateTransforms();

	WriteFixedString(kKfmFilePath);
	Write(&uiActiveSequenceID, sizeof(unsigned int));
	Write(&bAccumulateTransforms, sizeof(NiBool));
}

NiCameraComponent* CTagStream::ReadCameraComponent()
{
	float fFOV;
	float fAspect;
	float fOrthoWidth;
	float fNear;
	float fFar;
	NiBool bOrtho;

	Read(&fFOV, sizeof(float));
	Read(&fAspect, sizeof(float));
	Read(&fOrthoWidth, sizeof(float));
	Read(&fNear, sizeof(float));
	Read(&fFar, sizeof(float));
	Read(&bOrtho, sizeof(NiBool));

	NiCameraComponent *pkComponent = NiNew NiCameraComponent;

	pkComponent->SetPropertyData("Field of View", fFOV, 0);
	pkComponent->SetPropertyData("Aspect Ratio", fAspect, 0);
	pkComponent->SetPropertyData("Orthographic Frustum Width", fOrthoWidth, 0);
	pkComponent->SetPropertyData("Near Clipping Plane", fNear, 0);
	pkComponent->SetPropertyData("Far Clipping Plane", fFar, 0);
	pkComponent->SetPropertyData("Orthographic", NIBOOL_IS_TRUE(bOrtho), 0);

	return pkComponent;
}

void CTagStream::WriteCameraComponent(NiCameraComponent* pkComponent)
{
	float fFOV;
	float fAspect;
	float fOrthoWidth;
	float fNear;
	float fFar;

	bool bOrtho;

	pkComponent->GetPropertyData("Field of View", fFOV, 0);
	pkComponent->GetPropertyData("Aspect Ratio", fAspect, 0);
	pkComponent->GetPropertyData("Orthographic Frustum Width", fOrthoWidth, 0);
	pkComponent->GetPropertyData("Near Clipping Plane", fNear, 0);
	pkComponent->GetPropertyData("Far Clipping Plane", fFar, 0);
	pkComponent->GetPropertyData("Orthographic", bOrtho, 0);

	Write(&fFOV, sizeof(float));
	Write(&fAspect, sizeof(float));
	Write(&fOrthoWidth, sizeof(float));
	Write(&fNear, sizeof(float));
	Write(&fFar, sizeof(float));

	NiBool bTemp = bOrtho;
	Write(&bTemp, sizeof(NiBool));
}

NiInheritedTransformationComponent* CTagStream::ReadInheritedTransformationComponent()
{
	//NiInheritedTransformationComponent *pkComponent = NiNew NiInheritedTransformationComponent;
	//return pkComponent;
	return NULL;
}

void CTagStream::WriteInheritedTransformationComponent(NiInheritedTransformationComponent* pkComponent)
{
}

NiLightComponent* CTagStream::ReadLightComponent()
{
	NiFixedString kLightType;
	float fDimmer;
	float fConstantAttenuation;
	float fLinearAttenuation;
	float fQuadraticAttenuation;
	float fOuterSpotAngle;
	float fInnerSpotAngle;
	float fSpotExponent;
	NiColor kAmbientColor;
	NiColor kDiffuseColor;
	NiColor kSpecularColor;

	ReadFixedString(kLightType);
	Read(&fDimmer, sizeof(float));
	Read(&fConstantAttenuation, sizeof(float));
	Read(&fLinearAttenuation, sizeof(float));
	Read(&fQuadraticAttenuation, sizeof(float));
	Read(&fOuterSpotAngle, sizeof(float));
	Read(&fInnerSpotAngle, sizeof(float));
	Read(&fSpotExponent, sizeof(float));
	Read(&kAmbientColor, sizeof(NiColor));
	Read(&kDiffuseColor, sizeof(NiColor));
	Read(&kSpecularColor, sizeof(NiColor));

	NiLightComponent *pkComponent = NiNew NiLightComponent;

	pkComponent->SetLightType(kLightType);
	pkComponent->SetDimmer(fDimmer);
	pkComponent->SetConstantAttenuation(fConstantAttenuation);
	pkComponent->SetLinearAttenuation(fLinearAttenuation);
	pkComponent->SetQuadraticAttenuation(fQuadraticAttenuation);
	pkComponent->SetOuterSpotAngle(fOuterSpotAngle);
	pkComponent->SetInnerSpotAngle(fInnerSpotAngle);
	pkComponent->SetSpotExponent(fSpotExponent);
	pkComponent->SetAmbientColor(kAmbientColor);
	pkComponent->SetDiffuseColor(kDiffuseColor);
	pkComponent->SetSpecularColor(kSpecularColor);

	return pkComponent;
}

void CTagStream::WriteLightComponent(NiLightComponent* pkComponent)
{
	NiFixedString kLightType;
	float fDimmer;
	float fConstantAttenuation;
	float fLinearAttenuation;
	float fQuadraticAttenuation;
	float fOuterSpotAngle;
	float fInnerSpotAngle;
	float fSpotExponent;
	NiColor kAmbientColor;
	NiColor kDiffuseColor;
	NiColor kSpecularColor;

	kLightType = pkComponent->GetLightType();
	fDimmer = pkComponent->GetDimmer();
	fConstantAttenuation = pkComponent->GetConstantAttenuation();
	fLinearAttenuation = pkComponent->GetLinearAttenuation();
	fQuadraticAttenuation = pkComponent->GetQuadraticAttenuation();
	fOuterSpotAngle = pkComponent->GetOuterSpotAngle();
	fInnerSpotAngle = pkComponent->GetInnerSpotAngle();
	fSpotExponent = pkComponent->GetSpotExponent();
	kAmbientColor = pkComponent->GetAmbientColor();
	kDiffuseColor = pkComponent->GetDiffuseColor();
	kSpecularColor = pkComponent->GetSpecularColor();

	WriteFixedString(kLightType);
	Write(&fDimmer, sizeof(float));
	Write(&fConstantAttenuation, sizeof(float));
	Write(&fLinearAttenuation, sizeof(float));
	Write(&fQuadraticAttenuation, sizeof(float));
	Write(&fOuterSpotAngle, sizeof(float));
	Write(&fInnerSpotAngle, sizeof(float));
	Write(&fSpotExponent, sizeof(float));
	Write(&kAmbientColor, sizeof(NiColor));
	Write(&kDiffuseColor, sizeof(NiColor));
	Write(&kSpecularColor, sizeof(NiColor));
}

NiSceneGraphComponent* CTagStream::ReadSceneGraphComponent()
{
	NiFixedString kFilePath;
	char acAbsFilePath[1024];

	ReadFixedString(kFilePath);
	NiSprintf(acAbsFilePath, 1024, "%s\\%s", TerrainHelper::GetClientPath(), (const char*)kFilePath);

	NiSceneGraphComponent *pkComponent = NiNew NiSceneGraphComponent;

	//装载绝对路径
	pkComponent->SetNifFilePath(acAbsFilePath);

	return pkComponent;
}

void CTagStream::WriteSceneGraphComponent(NiSceneGraphComponent* pkComponent)
{
	NiFixedString kFilePath;
	char acRelFilePath[1024];

	kFilePath = pkComponent->GetNifFilePath();
	TerrainHelper::ConvertToRelative(acRelFilePath, 1024, kFilePath);

	//保存相对路径
	WriteFixedString(acRelFilePath);
}

NiShadowGeneratorComponent* CTagStream::ReadShadowGeneratorComponent()
{
	float fDepthBias;
	NiBool bCastShadows;
	NiBool bStaticShadows;
	NiBool bStrictlyObserveSizeHint;
	NiBool bRenderBackfaces;
	NiBool bUseDefaultDepthBias;
	unsigned short usSizeHint;
	NiFixedString kShadowTechnique;

	Read(&fDepthBias, sizeof(float));
	Read(&bCastShadows, sizeof(NiBool));
	Read(&bStaticShadows, sizeof(NiBool));
	Read(&bStrictlyObserveSizeHint, sizeof(NiBool));
	Read(&bRenderBackfaces, sizeof(NiBool));
	Read(&bUseDefaultDepthBias, sizeof(NiBool));
	Read(&usSizeHint, sizeof(unsigned short));
	ReadFixedString(kShadowTechnique);

	NiShadowGeneratorComponent *pkComponent = NiNew NiShadowGeneratorComponent;

	pkComponent->SetDepthBias(fDepthBias);
	pkComponent->SetCastShadows(NIBOOL_IS_TRUE(bCastShadows));
	pkComponent->SetStaticShadows(NIBOOL_IS_TRUE(bStaticShadows));
	pkComponent->SetStrictlyObserveSizeHint(NIBOOL_IS_TRUE(bStrictlyObserveSizeHint));
	pkComponent->SetRenderBackfaces(NIBOOL_IS_TRUE(bRenderBackfaces));
	pkComponent->SetUseDefaultDepthBias(NIBOOL_IS_TRUE(bUseDefaultDepthBias));
	pkComponent->SetSizeHint(usSizeHint);
	pkComponent->SetShadowTechnique(kShadowTechnique);

	return pkComponent;
}

void CTagStream::WriteShadowGeneratorComponent(NiShadowGeneratorComponent* pkComponent)
{
	float fDepthBias;
	bool bCastShadows;
	bool bStaticShadows;
	bool bStrictlyObserveSizeHint;
	bool bRenderBackfaces;
	bool bUseDefaultDepthBias;
	unsigned short usSizeHint;
	NiFixedString kShadowTechnique;

	fDepthBias = pkComponent->GetDepthBias();	
	bCastShadows = pkComponent->GetCastShadows();
	bStaticShadows = pkComponent->GetStaticShadows();
	bStrictlyObserveSizeHint = pkComponent->GetStrictlyObserveSizeHint();
	bRenderBackfaces = pkComponent->GetRenderBackfaces();
	bUseDefaultDepthBias = pkComponent->GetUseDefaultDepthBias();
	usSizeHint = pkComponent->GetSizeHint();
	kShadowTechnique = pkComponent->GetShadowTechnique();

	NiBool bTemp;
	Write(&fDepthBias, sizeof(float));
	bTemp = bCastShadows;
	Write(&bTemp, sizeof(NiBool));
	bTemp = bStaticShadows;
	Write(&bTemp, sizeof(NiBool));
	bTemp = bStrictlyObserveSizeHint;
	Write(&bTemp, sizeof(NiBool));
	bTemp = bRenderBackfaces;
	Write(&bTemp, sizeof(NiBool));
	bTemp = bUseDefaultDepthBias;
	Write(&bTemp, sizeof(NiBool));
	Write(&usSizeHint, sizeof(unsigned short));
	WriteFixedString(kShadowTechnique);
}

NiTransformationComponent* CTagStream::ReadTransformationComponent()
{
	NiPoint3 kTranslation;
	NiMatrix3 kRotation;
	float fScale;

	Read(&kTranslation, sizeof(kTranslation));
	Read(&kRotation, sizeof(kRotation));
	Read(&fScale, sizeof(fScale));

	NiTransformationComponent *pkComponent = NiNew NiTransformationComponent;

	//使位置为相对位置
	kTranslation.x = NiFmod(kTranslation.x, TILE_SIZE) + m_fBaseX;
	kTranslation.y = NiFmod(kTranslation.y, TILE_SIZE) + m_fBaseY;
	
	pkComponent->SetTranslation(kTranslation);
	pkComponent->SetRotation(kRotation);
	pkComponent->SetScale(fScale);

	return pkComponent;
}

void CTagStream::WriteTransformationComponent(NiTransformationComponent* pkComponent)
{
	NiPoint3 kTranslation;
	NiMatrix3 kRotation;
	float fScale;

	kTranslation = pkComponent->GetTranslation();
	kRotation = pkComponent->GetRotation();
	fScale = pkComponent->GetScale();

	kTranslation.x -= m_fBaseX;
	kTranslation.y -= m_fBaseY;

	Write(&kTranslation, sizeof(kTranslation));
	Write(&kRotation, sizeof(kRotation));
	Write(&fScale, sizeof(fScale));
}

CSPTComponent* CTagStream::ReadSPTComponent()
{
	NiFixedString kFilePath;
	char acAbsFilePath[1024];

	ReadFixedString(kFilePath);
	NiSprintf(acAbsFilePath, 1024, "%s\\%s", TerrainHelper::GetClientPath(), (const char*)kFilePath);

	CSPTComponent *pkComponent = NiNew CSPTComponent;

	//装载绝对路径
	pkComponent->SetSPTFilePath(acAbsFilePath);

	return pkComponent;
}

void CTagStream::WriteSPTComponent(CSPTComponent* pkComponent)
{
	NiFixedString kFilePath;
	char acRelFilePath[1024];

	kFilePath = pkComponent->GetSPTFilePath();
	TerrainHelper::ConvertToRelative(acRelFilePath, 1024, kFilePath);

	//保存相对路径
	WriteFixedString(acRelFilePath);
}

NiEntityComponentInterface* CTagStream::ReadComponent()
{
	NiFixedString kName;
	ReadFixedString(kName); //名字

	if( kName == "Actor" )
	{
		return ReadActorComponent();
	}
	else if( kName == "Camera" )
	{
		return ReadCameraComponent();
	}
	else if( kName == "Inherited Transformation" )
	{
		return ReadInheritedTransformationComponent();
	}
	else if( kName == "Light" )
	{
		return ReadLightComponent();
	}
	else if( kName == "Scene Graph" )
	{
		return ReadSceneGraphComponent();
	}
	else if( kName == "Shadow Generator" )
	{
		return ReadShadowGeneratorComponent();
	}
	else if( kName == "Transformation" )
	{
		return ReadTransformationComponent();
	}
	else if( kName == "Speed Tree" )
	{
		return ReadSPTComponent();
	}
	else
	{
	}

	return NULL;
}

void CTagStream::WriteComponent(NiEntityComponentInterface* pkComponent)
{
	WriteFixedString(pkComponent->GetName()); //名字

	if( pkComponent->GetName() == "Actor" )
	{
		WriteActorComponent((NiActorComponent*)pkComponent);
	}
	else if( pkComponent->GetName() == "Camera" )
	{
		WriteCameraComponent((NiCameraComponent*)pkComponent);
	}
	else if( pkComponent->GetName() == "Inherited Transformation" )
	{
		WriteInheritedTransformationComponent((NiInheritedTransformationComponent*)pkComponent);
	}
	else if( pkComponent->GetName() == "Light" )
	{
		WriteLightComponent((NiLightComponent*)pkComponent);
	}
	else if( pkComponent->GetName() == "Scene Graph" )
	{
		WriteSceneGraphComponent((NiSceneGraphComponent*)pkComponent);
	}
	else if( pkComponent->GetName() == "Shadow Generator" )
	{
		WriteShadowGeneratorComponent((NiShadowGeneratorComponent*)pkComponent);
	}
	else if( pkComponent->GetName() == "Transformation" )
	{
		WriteTransformationComponent((NiTransformationComponent*)pkComponent);
	}
	else if( pkComponent->GetName() == "Speed Tree" )
	{
		WriteSPTComponent((CSPTComponent*)pkComponent);
	}
}

NiEntityInterface* CTagStream::ReadEntity()
{
	NiFixedString kName;
	if( !ReadFixedString(kName) )
		return NULL;

	NiGeneralEntity* pkEntity = NiNew NiGeneralEntity;
	pkEntity->SetName(kName);
    pkEntity->SetTypeMask(NiGeneralEntity::SceneObject);

	unsigned int uiFlag = 0;
	unsigned int uiEntityType = CEntityInfoComponent::ET_MODEL;
	if( GetVersion() < MakeVersion(1, 0) )
	{
		//处理标记
		unsigned char ucFlag;
		Read(&ucFlag, sizeof(ucFlag));
		uiFlag = ucFlag;
	}
	else
	{
		Read(&uiFlag, sizeof(uiFlag));
		Read(&uiEntityType, sizeof(uiEntityType));//物件类型
	}

	unsigned int uiTransportID = 0;
	if(uiEntityType == CEntityInfoComponent::ET_TRANSPORT)
	{
		Read(&uiTransportID, sizeof(uiTransportID));
	}


	pkEntity->SetHidden((uiFlag & CEntityInfoComponent::EF_HIDDEN) != 0);

	unsigned int uiComponentCount;
	Read(&uiComponentCount, sizeof(unsigned int));
	NiEntityComponentInterface* pkComponent;
	for( unsigned int i = 0; i < uiComponentCount; i++ )
	{
		pkComponent = ReadComponent();
		if( pkComponent )
			pkEntity->AddComponent(pkComponent, false);
	}

    CEntityInfoComponent* pkEntityInfo = NiNew CEntityInfoComponent;
	pkEntityInfo->SetReflect((uiFlag & CEntityInfoComponent::EF_REFLECT) != 0);
    pkEntityInfo->SetCastShadow((uiFlag & CEntityInfoComponent::EF_NO_SHDOW) == 0);
    pkEntityInfo->SetCollideCamera((uiFlag & CEntityInfoComponent::EF_NO_COLLIDE_CAMERA) == 0);
	pkEntityInfo->SetEntityType(uiEntityType);
	pkEntityInfo->SetTransportID(uiTransportID);
    pkEntity->AddComponent(pkEntityInfo, false);

	return pkEntity;
}

void CTagStream::WriteEntity(NiEntityInterface* pkEntity)
{
	NIASSERT( pkEntity );

	//这里不保存光照
	if( EntityIsLight(pkEntity) )
		return;

	NiFixedString kName;
	kName = pkEntity->GetName();
	WriteFixedString(kName);

	bool bHidden = NIBOOL_IS_TRUE(pkEntity->GetHidden());
    bool bReflect = false;
    bool bCastShadow = true;
    bool bCollideCamera = true;
	unsigned int uiEntityType = CEntityInfoComponent::ET_MODEL;
	unsigned int uiTransportID = 0;

    NiEntityComponentInterface* pkComponent;
    unsigned int uiComponentCount = 0;
    for( unsigned int i = 0; i < pkEntity->GetComponentCount(); i++ )
    {
        pkComponent = pkEntity->GetComponentAt(i);
        if(pkComponent == NULL)
            continue;

        if(pkComponent->GetTemplateID() == CEditorInfoComponent::ms_kTemplateID)
            continue;

        if(pkComponent->GetTemplateID() == CEntityInfoComponent::ms_kTemplateID)
        {
            bReflect = ((CEntityInfoComponent*)pkComponent)->GetReflect();
            bCastShadow = ((CEntityInfoComponent*)pkComponent)->GetCastShadow();
            bCollideCamera = ((CEntityInfoComponent*)pkComponent)->GetCollideCamera();
			uiEntityType = ((CEntityInfoComponent*)pkComponent)->GetEntityType();
			uiTransportID = ((CEntityInfoComponent*)pkComponent)->GetTransportID();
            continue;
        }

        uiComponentCount++;
    }

	unsigned int uiFlag = 0;
    if(bHidden)
		uiFlag |= CEntityInfoComponent::EF_HIDDEN;

    if(bReflect)
		uiFlag |= CEntityInfoComponent::EF_REFLECT;

    if(!bCastShadow)
        uiFlag |= CEntityInfoComponent::EF_NO_SHDOW;

    if(!bCollideCamera)
        uiFlag |= CEntityInfoComponent::EF_NO_COLLIDE_CAMERA;

	Write(&uiFlag, sizeof(uiFlag));
	Write(&uiEntityType, sizeof(uiEntityType));
	if(uiEntityType == CEntityInfoComponent::ET_TRANSPORT)
		Write(&uiTransportID, sizeof(uiTransportID));

	Write(&uiComponentCount, sizeof(unsigned int));
	for( unsigned int i = 0; i < uiComponentCount; i++ )
	{
		pkComponent = pkEntity->GetComponentAt(i);
		if(pkComponent == NULL)
            continue;

        if(pkComponent->GetTemplateID() == CEditorInfoComponent::ms_kTemplateID)
            continue;

        if(pkComponent->GetTemplateID() == CEntityInfoComponent::ms_kTemplateID)
            continue;
        
        WriteComponent(pkComponent);
	}
}

bool CTagStream::EntityIsLight(NiEntityInterface* pkEntity)
{
    NiObject* pkSceneRootPointer = NULL;
    if(pkEntity->GetPropertyData("Scene Root Pointer", pkSceneRootPointer, 0))
    {
		if( pkSceneRootPointer && NiIsKindOf(NiLight, pkSceneRootPointer) )
			return true;
    }

	return false;
}



