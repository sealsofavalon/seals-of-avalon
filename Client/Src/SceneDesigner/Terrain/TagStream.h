
#ifndef TAG_STREAM_H
#define TAG_STREAM_H

#include "TerrainLibType.h"
#include "SpeedTree/SPTComponent.h"

class TERRAIN_ENTRY CTagStream : public NiMemStream
{
public:
	CTagStream()
		:m_uiVersion(0)
	{
	}

	bool ReadFromStream(NiBinaryStream* pkStream);
	bool WriteToStream(NiBinaryStream* pkStream);


	bool ReadFixedString(NiFixedString& kString);
	bool WriteFixedString(const NiFixedString& kString);

	NiActorComponent* ReadActorComponent();
	void WriteActorComponent(NiActorComponent* pkComponent);

	NiCameraComponent* ReadCameraComponent();
	void WriteCameraComponent(NiCameraComponent* pkComponent);

	NiInheritedTransformationComponent* ReadInheritedTransformationComponent();
	void WriteInheritedTransformationComponent(NiInheritedTransformationComponent* pkComponent);

	NiLightComponent* ReadLightComponent();
	void WriteLightComponent(NiLightComponent* pkComponent);

	NiSceneGraphComponent* ReadSceneGraphComponent();
	void WriteSceneGraphComponent(NiSceneGraphComponent* pkComponent);

	NiShadowGeneratorComponent* ReadShadowGeneratorComponent();
	void WriteShadowGeneratorComponent(NiShadowGeneratorComponent* pkComponent);

	NiTransformationComponent* ReadTransformationComponent();
	void WriteTransformationComponent(NiTransformationComponent* pkComponent);

	CSPTComponent* ReadSPTComponent();
	void WriteSPTComponent(CSPTComponent* pkComponent);

	NiEntityComponentInterface* ReadComponent();
	void WriteComponent(NiEntityComponentInterface* pkComponent);

	NiEntityInterface* ReadEntity();
	void WriteEntity(NiEntityInterface* pkEntity);

	bool EntityIsLight(NiEntityInterface* pkEntity);

	unsigned int GetTag() const { return m_uiTag; }
	void SetTag(unsigned int tag) { m_uiTag = tag; }

	void SetBase(float x, float y) { m_fBaseX = x; m_fBaseY = y; }

	void Reset();

	unsigned int GetVersion() const { return m_uiVersion; }
	void SetVersion(unsigned int uiVersion) { m_uiVersion = uiVersion; }

private:
	int	m_uiTag;
	float m_fBaseX;
	float m_fBaseY;
	unsigned int m_uiVersion;
};

#endif //TAG_STREAM_H