
#include "TerrainPCH.h"
#include "TerrainHelper.h"
#include <direct.h>
#include <stdio.h>

#define NID3DXEFFECTSHADER_IMPORT

#include <NiShader.h>
#include <NiShaderFactory.h>
#include <NiShaderLibrary.h>
#include <NiShaderLibraryDesc.h>

#include <NiD3DShaderProgramFactory.h>
#include <NiD3DRendererHeaders.h>
#include "NiD3DXEffectShaderLibrary.h"
#include "NPCComponent.h"
#include "WayPointComponent.h"
#include "GameObjectComponent.h"
#include "PathNodeComponent.h"
#include "SoundEmitterComponent.h"
#include "Map.h"

#ifdef NIDEBUG
	#pragma comment(lib, "NiD3DXEffectShaderLibDX923VC80D.lib")
#else
	#pragma comment(lib, "NiD3DXEffectShaderLibDX923VC80R.lib")
#endif

namespace TerrainHelper
{

//恢复当前路径
void ResetCurrentPath()
{
	char acExePath[NI_MAX_PATH];
	NiPath::GetExecutableDirectory(acExePath, NI_MAX_PATH);
	_chdir(acExePath);
}

const char* GetExePath()
{
	static char s_acExePath[NI_MAX_PATH] = {'\0'};
	if( s_acExePath[0] == '\0' )
	{
		NiPath::GetExecutableDirectory(s_acExePath, NI_MAX_PATH);
	}

	return s_acExePath;
}

//获取客户端路径
const char* GetClientPath()
{
	FILE* fp = NULL;
	fopen_s(&fp, "config.txt", "rb");
	char buff[MAX_PATH];
	fgets(buff, MAX_PATH, fp);
	fclose(fp);

	static char s_acClientPath[NI_MAX_PATH] = {'\0'};
	if( s_acClientPath[0] == '\0' )
	{
		char acExePath[NI_MAX_PATH];
		NiPath::GetExecutableDirectory(acExePath, NI_MAX_PATH);
		NiSprintf(s_acClientPath, NI_MAX_PATH, "%s\\..\\..\\..\\%s", acExePath, buff);
		NiPath::RemoveDotDots(s_acClientPath);
	}

	return s_acClientPath;
}


//获取服务器端路径
const char* GetServerPath()
{
	static char s_acServerPath[NI_MAX_PATH] = {'\0'};
	if( s_acServerPath[0] == '\0' )
	{
		char acExePath[NI_MAX_PATH];
		NiPath::GetExecutableDirectory(acExePath, NI_MAX_PATH);
		NiSprintf(s_acServerPath, NI_MAX_PATH, "%s\\..\\..\\..\\Server\\GameServer", acExePath);
		NiPath::RemoveDotDots(s_acServerPath);
	}

	return s_acServerPath;
}

bool ConvertToRelative(char* pcRelativePath, size_t stRelBytes, const char* pcAbsolutePath)
{
    if(NiPath::IsRelative(pcAbsolutePath))
    {
        NiStrcpy(pcRelativePath, stRelBytes, pcAbsolutePath);
        return true;
    }

	size_t stLen = NiPath::ConvertToRelative(pcRelativePath, stRelBytes, pcAbsolutePath, GetClientPath());
	if( stLen > 0 )
	{
		NIASSERT( stLen > 2 );

		//去掉开始的 \.
		NiStrcpy(pcRelativePath, stRelBytes, &pcRelativePath[2]);
		stLen -= 2;
	}

	//不在客户端目录下面
	if( stLen > 2 )
	{
		if(pcRelativePath[0] == '.' && pcRelativePath[1] == '.')
			return false;

		return true;
	}

	return false;
}

bool SetBit(unsigned int& value, unsigned int bit, bool bSet)
{
	NIASSERT( bit < 32 );

	if( GetBit(value, bit) == bSet )
		return false;

	value ^= (1 << bit);

	NIASSERT( GetBit(value, bit) == bSet );

	return true;
}

bool GetBit(unsigned int value, unsigned int bit)
{
	NIASSERT( bit < 32 );

	return ((value & (1 << bit)) != 0) ? true : false;
}

//save to tga file
bool SaveDepthSurfaceToFile(const char* pcDestFile, LPDIRECT3DSURFACE9 pkSrcSurface)
{
	NIASSERT( pkSrcSurface );

	D3DSURFACE_DESC kSurfDesc;
    HRESULT eD3DRet = pkSrcSurface->GetDesc(&kSurfDesc);
    if (FAILED(eD3DRet))
    {
        return false;
    } 

	D3DLOCKED_RECT kRect;
	eD3DRet = pkSrcSurface->LockRect(&kRect, NULL, D3DLOCK_READONLY);
    if (FAILED(eD3DRet))
    {
        NiDX9Renderer::Warning("SaveDepthSurfaceToFile FAILED");
        return false;
    }

	NiFile* pkFile = NiFile::GetFile(pcDestFile, NiFile::WRITE_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		pkSrcSurface->UnlockRect();
		return false;
	}

	unsigned char ucIDLength = 0;
	unsigned char ucColorMapType = 0;
	unsigned char ucImageType = 2;

	pkFile->Write(&ucIDLength, sizeof(ucIDLength));
	pkFile->Write(&ucColorMapType, sizeof(ucColorMapType));
	pkFile->Write(&ucImageType, sizeof(ucImageType));

	//Color Map Specification
	unsigned short usFirstEntryIndex = 0;
	unsigned short usColorMapLength = 0;
	unsigned char ucColorMapEntrySize = 0;
	pkFile->Write(&usFirstEntryIndex, sizeof(usFirstEntryIndex));
	pkFile->Write(&usColorMapLength, sizeof(usColorMapLength)); 
	pkFile->Write(&ucColorMapEntrySize, sizeof(ucColorMapEntrySize)); 

	//Image Specification
	unsigned short usXOrigin = 0;
	unsigned short usYOrigin = 0;
	unsigned short usWidth = (unsigned short)kSurfDesc.Width;
	unsigned short usHeight = (unsigned short)kSurfDesc.Height;
	unsigned char ucPixelDepth = 32;
	unsigned char ucImageDescriptor = 0x28; //8 bit alpha and top left
	pkFile->Write(&usXOrigin, sizeof(usXOrigin));
	pkFile->Write(&usYOrigin, sizeof(usYOrigin));
	pkFile->Write(&usWidth, sizeof(usWidth));
	pkFile->Write(&usHeight, sizeof(usHeight));
	pkFile->Write(&ucPixelDepth, sizeof(ucPixelDepth));
	pkFile->Write(&ucImageDescriptor, sizeof(ucImageDescriptor));

	//Image Data
	unsigned char ucDepth;
	for( unsigned int h = 0; h < kSurfDesc.Height; ++h )
	{
		float* pfDepth = (float*)((unsigned char*)(kRect.pBits) + kRect.Pitch*h);
		for( unsigned int w = 0; w < kSurfDesc.Width; ++w, ++pfDepth )
		{
			ucDepth = (unsigned char)NiClamp(*pfDepth*255.f, 0.f, 255.f);
			pkFile->Write(&ucDepth, sizeof(ucDepth));
			pkFile->Write(&ucDepth, sizeof(ucDepth));
			pkFile->Write(&ucDepth, sizeof(ucDepth));
			pkFile->Write(&ucDepth, sizeof(ucDepth));
		}
	}

	pkSrcSurface->UnlockRect();
	NiDelete pkFile;

	return true;
}

//---------------------------------------------------------------------------
NiMatrix3 LookAt(const NiPoint3& kFocus, const NiPoint3& kSource, const NiPoint3& kUp)
{
    NiPoint3 kLook = kFocus - kSource;
    kLook.Unitize();
    NiPoint3 kLookTangent = kLook.Cross(kUp);
    kLookTangent.Unitize();
    NiPoint3 kLookBiTangent = kLookTangent.Cross(kLook);

    return NiMatrix3(kLook, kLookBiTangent, kLookTangent);
}

bool IsModelEntity(NiEntityInterface* pkEntity)
{
    if(pkEntity->GetTypeMask() == NiGeneralEntity::SceneObject)
        return true;

    if(pkEntity->GetTypeMask() == 0)
    {
        NiFixedString kFilePath;
        if( pkEntity->GetPropertyData(CMap::ms_kNifFilePathName, kFilePath) )
        {
            ((NiGeneralEntity*)pkEntity)->SetTypeMask(NiGeneralEntity::SceneObject);
            return true;
        }

        if( pkEntity->GetPropertyData(CMap::ms_kSPTFilePathName, kFilePath) )
        {
            ((NiGeneralEntity*)pkEntity)->SetTypeMask(NiGeneralEntity::SceneObject);
            return true;
        }
    }

	return false;
}

bool IsNpcEntity(NiEntityInterface* pkEntity)
{
    if(pkEntity->GetTypeMask() == NiGeneralEntity::Npc)
        return true;

    if(pkEntity->GetTypeMask() == 0)
    {
        //palette 里面没有保存TypeMask
        //刚创建的时候类型可能还没有设置，这时GetTypeMask不正确
        if( pkEntity->GetComponentByTemplateID(CNpcComponent::ms_kTemplateID) )
        {
            ((NiGeneralEntity*)pkEntity)->SetTypeMask(NiGeneralEntity::Npc);
            return true;
        }
    }

	return false;
}

bool IsWayPointEntity(NiEntityInterface* pkEntity)
{
    if( pkEntity->GetTypeMask() == NiGeneralEntity::WayPoint )
        return true;

	return false;
}

bool IsGameObject(NiEntityInterface* pkEntity)
{
    if( pkEntity->GetTypeMask() == NiGeneralEntity::GameObject )
        return true;

    if( pkEntity->GetTypeMask() == 0 )
    {
        if( pkEntity->GetComponentByTemplateID(CGameObjectComponent::ms_kTemplateID) )
        {
            ((NiGeneralEntity*)pkEntity)->SetTypeMask(NiGeneralEntity::GameObject);
            return true;
        }
    }

    return false;
}

bool IsPathNodeEntity(NiEntityInterface* pkEntity)
{
    if( pkEntity->GetTypeMask() == NiGeneralEntity::PathNode )
        return true;

    return false;
}

TERRAIN_ENTRY bool IsSoundEmitter(NiEntityInterface* pkEntity)
{
    if(pkEntity->GetTypeMask() == NiGeneralEntity::SoundEmitter)
        return true;

    if( pkEntity->GetTypeMask() == 0 )
    {
        //palette 里面没有保存TypeMask
        //刚创建的时候类型可能还没有设置，这时GetTypeMask不正确
        if( pkEntity->GetComponentByTemplateID(CSoundEmitterComponent::ms_kTemplateID) )
        {
            ((NiGeneralEntity*)pkEntity)->SetTypeMask(NiGeneralEntity::SoundEmitter);
            return true;
        }
    }

    return false;
}


float CatmullRom(float t, float p0, float p1, float p2, float p3)
{
	return 0.5f * ((3.0f*p1 - 3.0f*p2 + p3 - p0)*t*t*t
		+  (2.0f*p0 - 5.0f*p1 + 4.0f*p2 - p3)*t*t
		+  (p2-p0)*t
		+  2.0f*p1);
}

NiD3DVertexShader* CreateVertexShader(const char* pcFilename, const char* pcShaderName)
{
    char acFileName[1024];
    NiSprintf(acFileName, sizeof(acFileName), "%s..\\..\\Data\\Shaders\\%s", TerrainHelper::GetExePath(), pcFilename);
    NiPath::RemoveDotDots(acFileName);
    return NiD3DShaderProgramFactory::CreateVertexShaderFromFile(
                acFileName,
                pcShaderName, 
                pcShaderName, 
                NULL,
                NULL, 
                0, 
                false);
}

NiD3DPixelShader* CreatePixelShader(const char* pcFilename, const char* pcShaderName)
{
    char acFileName[1024];
    NiSprintf(acFileName, sizeof(acFileName), "%s..\\..\\Data\\Shaders\\%s", TerrainHelper::GetExePath(), pcFilename);
    NiPath::RemoveDotDots(acFileName);
    return NiD3DShaderProgramFactory::CreatePixelShaderFromFile(
                acFileName,
                pcShaderName, 
                pcShaderName, 
                NULL,
                false);
}

static bool FXLibraryClassCreate(
								 const char* pcLibFile,
								 NiRenderer* pkRenderer, 
								 int iDirectoryCount, 
								 char* apcDirectories[], 
								 bool bRecurseSubFolders, 
								 NiShaderLibrary** ppkLibrary)
{
	*ppkLibrary = NiD3DXEffectShaderLibrary::Create((NiD3DRenderer*)pkRenderer, iDirectoryCount, apcDirectories,  bRecurseSubFolders);
	return *ppkLibrary != NULL;
}

void CreateShaderSystem()
{
	char acShaderPath[2][NI_MAX_PATH];
	acShaderPath[0][0] = '\0';

	size_t stSize = 0;
	getenv_s(&stSize, acShaderPath[0], NI_MAX_PATH, "EGB_SHADER_LIBRARY_PATH");
	strcat_s(acShaderPath[0], NI_MAX_PATH, "\\Data");

	NiSprintf(acShaderPath[1], NI_MAX_PATH, "%s..\\..\\Data\\Shaders", GetExePath());
	NiPath::RemoveDotDots(acShaderPath[1]);

	//NiShaderFactory::AddShaderProgramFileDirectory(acShaderPath[0]);
	//NiShaderFactory::AddShaderProgramFileDirectory(acShaderPath[1]);

	char* pacDirectories[2] = {acShaderPath[0], acShaderPath[1]};
	NiShaderFactory::RegisterClassCreationCallback(FXLibraryClassCreate);
	NiShaderFactory::LoadAndRegisterShaderLibrary(NULL, 2, pacDirectories, true);
}

void AttachFogProperty(NiEntityInterface* pkEntity)
{
    NiObject* pkSceneRootPointer = NULL;
    NiAVObject* pkSceneRoot = NULL;

    if (pkEntity->GetPropertyData("Scene Root Pointer", pkSceneRootPointer))
    {
        pkSceneRoot = NiDynamicCast(NiAVObject, pkSceneRootPointer);
    }

    if(pkSceneRoot)
    {
        AttachFogProperty(pkSceneRoot);
		pkSceneRoot->Update(0.0f);
        pkSceneRoot->UpdateProperties();
    }
}

void AttachFogProperty(NiAVObject* pkAVObject)
{
    if(pkAVObject == NULL)
        return;

    if(NiIsKindOf(NiGeometry, pkAVObject))
    {
        NiAlphaProperty* pkAlphaProperty = (NiAlphaProperty*)pkAVObject->GetProperty(NiAlphaProperty::GetType());
        if( !pkAlphaProperty 
         || (pkAlphaProperty->GetSrcBlendMode() != NiAlphaProperty::ALPHA_ONE && pkAlphaProperty->GetDestBlendMode() != NiAlphaProperty::ALPHA_ONE) )
        {
            NiFogProperty* pkFogProperty = (NiFogProperty*)pkAVObject->GetProperty(NiFogProperty::GetType());
            if(pkFogProperty)
                pkAVObject->DetachProperty(pkFogProperty);

            pkAVObject->AttachProperty(CTerrainLightManager::Get()->GetFogProperty());
			pkAVObject->UpdateProperties();
        }
    }
    else if(NiIsKindOf(NiNode, pkAVObject))
    {
        NiNode* pkNode = (NiNode*)pkAVObject;
        for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ++ui)
            AttachFogProperty(pkNode->GetAt(ui));
    }
}

unsigned int CountBits(unsigned int uiBits)
{
    unsigned int uiCount = 0;
    for(unsigned int ui = 0; ui < 32; ui++)
    {
        if(uiBits & (1 << ui))
            ++uiCount;
    }

    return uiCount;
}

void SetFrameID(NiAVObject* pkAVObject, unsigned int uiID)
{
    if(NiIsKindOf(NiGeometry, pkAVObject))
    {
        NiGeometry* pkGeometry = (NiGeometry*)pkAVObject;
        if( pkGeometry->GetSkinInstance() )
            pkGeometry->GetSkinInstance()->SetFrameID(uiID);
    }
    else if(NiIsKindOf(NiNode, pkAVObject))
    {
        NiNode* pkNode = (NiNode*)pkAVObject;
        for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ++ui)
            SetFrameID(pkNode->GetAt(ui), uiID);
    }
}

//保证读到SceneDesiger里面的名字是唯一的
bool MakeUniqueName(NiScene *pkScene, NiEntityInterface* pkEntity)
{
	NiFixedString kName = pkEntity->GetName();

	if( pkScene->GetEntityByName(kName) == NULL )
		return true;

	char acBaseName[1024];
	NIASSERT(kName.GetLength() < sizeof(acBaseName));
	NiStrcpy(acBaseName, sizeof(acBaseName), (const char*)kName);

	int iIndex = -1;

	//去掉字符串尾部的空格
	for( int i = 0; i < (int)kName.GetLength(); i++ )
	{
		if( acBaseName[i] != ' ' )
		{
			iIndex = i; 
		}
	}

	int iLen = iIndex + 1;
	acBaseName[iLen] = '\0';

	//寻找最后一个空格
	iIndex = -1;
	for( int i = 0; i < iLen; i++ )
	{
		if( acBaseName[i] == ' ' )
			iIndex = i;
	}

	if( iIndex != -1 )
	{
		bool bNum = true;
		for( int i = iIndex + 1; i < iLen; i++ )
		{
			if( acBaseName[i] >= '0' && acBaseName[i] <= '9' )
				continue;

			bNum = false;
			break;
		}

		if( bNum )
		{
			acBaseName[iIndex] = '\0';//去掉空格和空格后面的所有数字
		}
	}

	//todo...
	//优化这里, 否则装载速度太慢
	char acName[sizeof(acBaseName) + 8];
	for( int i = 0; i < 1000; i++ )
	{
		NiSprintf(acName, sizeof(acName), "%s %03d", acBaseName, i);
		if( pkScene->GetEntityByName(acName) == NULL )
		{
			pkEntity->SetName(acName);
			return true;
		}
	}

	return false;
}

}