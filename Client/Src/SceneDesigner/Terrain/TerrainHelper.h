
#ifndef TERRAIN_HELPER
#define TERRAIN_HELPER

#include "TerrainLibType.h"

#define BOOL_EQUATION(x, y) ( ((x)&&(y)) || !((x) || (y)) )

namespace TerrainHelper
{

//重设当前路径
TERRAIN_ENTRY void ResetCurrentPath();

//获取客户端路径
TERRAIN_ENTRY const char* GetClientPath();

//获取服务器端路径
TERRAIN_ENTRY const char* GetServerPath();

TERRAIN_ENTRY const char* GetExePath();

//绝对路径转相对客户端的路径
//绝对路径不在客户端路径下面的时候返回失败
TERRAIN_ENTRY bool ConvertToRelative(char* pcRelativePath, size_t stRelBytes, const char* pcAbsolutePath);

TERRAIN_ENTRY bool SetBit(unsigned int& value, unsigned int bit, bool bSet);
TERRAIN_ENTRY bool GetBit(unsigned int value, unsigned int bit);

TERRAIN_ENTRY bool SaveDepthSurfaceToFile(const char* pcDestFile, LPDIRECT3DSURFACE9 pSrcSurface);

TERRAIN_ENTRY NiMatrix3 LookAt(const NiPoint3& kFocus, const NiPoint3& kSource, const NiPoint3& kUp);

TERRAIN_ENTRY bool IsModelEntity(NiEntityInterface* pkEntity);
TERRAIN_ENTRY bool IsNpcEntity(NiEntityInterface* pkEntity);
TERRAIN_ENTRY bool IsWayPointEntity(NiEntityInterface* pkEntity);
TERRAIN_ENTRY bool IsGameObject(NiEntityInterface* pkEntity);
TERRAIN_ENTRY bool IsPathNodeEntity(NiEntityInterface* pkEntity);
TERRAIN_ENTRY bool IsSoundEmitter(NiEntityInterface* pkEntity);

TERRAIN_ENTRY float CatmullRom(float t, float p0, float p1, float p2, float p3);

TERRAIN_ENTRY NiD3DVertexShader* CreateVertexShader(const char* pcFilename, const char* pcShaderName);
TERRAIN_ENTRY NiD3DPixelShader* CreatePixelShader(const char* pcFilename, const char* pcShaderName);
TERRAIN_ENTRY void CreateShaderSystem();

void TERRAIN_ENTRY AttachFogProperty(NiEntityInterface* pkEntity);
void TERRAIN_ENTRY AttachFogProperty(NiAVObject* pkAVObject);

unsigned int TERRAIN_ENTRY CountBits(unsigned int uiBits);

void TERRAIN_ENTRY SetFrameID(NiAVObject* pkAVObject, unsigned int uiID);
bool MakeUniqueName(NiScene *pkScene, NiEntityInterface* pkEntity);

}

#endif