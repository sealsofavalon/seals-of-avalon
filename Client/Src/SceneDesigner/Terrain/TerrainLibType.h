
#ifndef TERRAIN_LIB_TYPE_H
#define TERRAIN_LIB_TYPE_H

#if defined(TERRAIN_EXPORT)
    // DLL library project uses this
    #define TERRAIN_ENTRY __declspec(dllexport)
#elif defined(TERRAIN_IMPORT)
    // client of DLL uses this
    #define TERRAIN_ENTRY __declspec(dllimport)
#else
    // static library project uses this
    #define TERRAIN_ENTRY
#endif

#endif // TERRAIN_LIB_TYPE_H