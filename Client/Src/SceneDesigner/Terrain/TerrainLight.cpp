
#include "TerrainPCH.h"
#include "TerrainHelper.h"
#include "TerrainLight.h"
#include "Map.h"

#define TERRAIN_LIGHT_SIGN    'LGHT'

CDayNightColor::CDayNightColor()
{
	SetBit(false, 0xFFFFFFFF);
	CalcColors();
}

NiColor CDayNightColor::GetColor(unsigned int uiHour, unsigned int uiMinute) const
{
	NIASSERT( uiHour < NUM_DAY_NIGHT_COLOR );

	float r = uiMinute / 60.f;
	NiColor kColor1 = m_kColors[uiHour];
	NiColor kColor2 = m_kColors[uiHour == NUM_DAY_NIGHT_COLOR - 1 ? 0 : uiHour + 1];

	return kColor1 * (1.f - r) + kColor2 * r;
}

void CDayNightColor::SetColor(unsigned int uiHour, const NiColor& kColor)
{
	NIASSERT( uiHour < NUM_DAY_NIGHT_COLOR );
	m_kColors[uiHour] = kColor;

	CalcColors();
}

void CDayNightColor::Enable(unsigned int uiHour, bool bEnable)
{
	NIASSERT( uiHour < NUM_DAY_NIGHT_COLOR );

	SetBit( bEnable, 2 << uiHour );

	CalcColors();
}

bool CDayNightColor::IsEnable(unsigned int uiHour) const
{
	NIASSERT( uiHour < NUM_DAY_NIGHT_COLOR );

	return GetBit( 2 << uiHour );
}

void CDayNightColor::CalcColors()
{
	for( int i = 0; i < NUM_DAY_NIGHT_COLOR; ++i )
	{
		if( IsEnable( i ) )
			continue;

		NiColor kColor1 = DEFAULT_COLOR;
		float r1 = 0.f;
		for( int j = i + 1; ; ++j )
		{
			if( j == NUM_DAY_NIGHT_COLOR )
				j = 0;

			if( j == i )
				break;

			r1 += 1.f;
			if( IsEnable( j ) )
			{
				kColor1 = m_kColors[j];
				break;
			}
		}

		NiColor kColor2 = DEFAULT_COLOR;
		float r2 = 0.f;
		for( int j = i - 1; ; --j )
		{
			if( j == -1 )
				j = NUM_DAY_NIGHT_COLOR - 1;

			if( j == i )
				break;

			r2 += 1.f;
			if( IsEnable( j ) )
			{
				kColor2 = m_kColors[j];
				break;
			}
		}

		m_kColors[i] = kColor1 * (r2 / (r1 + r2)) + kColor2 * (r1 / (r1 + r2));
	}
}

NiFixedString CDayNightColor::ToString() const
{
	NiString kString;
	char acBuffer[256];
	NiSprintf(acBuffer, sizeof(acBuffer), "%u ", m_uFlags);
	kString = acBuffer;

	for( int i = 0; i < NUM_DAY_NIGHT_COLOR; ++i )
	{
		if( !IsEnable( i ) )
			continue;

		NiSprintf(acBuffer, sizeof(acBuffer), "%f %f %f ", m_kColors[i].r, m_kColors[i].g, m_kColors[i].b);
		kString += acBuffer;
	}

	return (const char*)kString;
}

bool CDayNightColor::FromString(const NiFixedString& kData)
{
	const char* ptr = kData;
	sscanf_s(ptr, "%u ", &m_uFlags);

	for( int i = 0; i < NUM_DAY_NIGHT_COLOR; ++i )
	{
		if( !IsEnable( i ) )
			continue;

		ptr = strstr(ptr, " ");
		if( ptr == NULL )
			return false;

		ptr++;
		if( sscanf_s(ptr, "%f", &m_kColors[i].r) != 1 )
			return false;

		ptr = strstr(ptr, " ");
		if( ptr == NULL )
			return false;

		ptr++;
		if( sscanf_s(ptr, "%f", &m_kColors[i].g) != 1 )
			return false;

		ptr = strstr(ptr, " ");
		if( ptr == NULL )
			return false;

		ptr++;
		if( sscanf_s(ptr, "%f", &m_kColors[i].b) != 1 )
			return false;
	}

	CalcColors();
	return true;
}

bool CDayNightColor::Load(NiBinaryStream* pkStream)
{
	pkStream->Read(&m_uFlags, sizeof(int));
	pkStream->Read(m_kColors, sizeof(m_kColors));

	return true;
}

bool CDayNightColor::Save(NiBinaryStream* pkStream)
{
	pkStream->Write(&m_uFlags, sizeof(int));
	pkStream->Write(m_kColors, sizeof(m_kColors));

	return true;
}


bool CTerrainLight::Load(NiBinaryStream* pkStream)
{
	NiPoint3 kPos;
	pkStream->Read(&kPos, sizeof(NiPoint3));
	SetTranslate(kPos);

	pkStream->Read(&m_fInnerRadius, sizeof(float));
	pkStream->Read(&m_fOuterRadius, sizeof(float));

	NiBool bGlobal;
	pkStream->Read(&bGlobal, sizeof(NiBool));
	m_bGlobal = NIBOOL_IS_TRUE(bGlobal);

    CTerrainLightManager* pkLightManager = CTerrainLightManager::Get();
    NIASSERT(pkLightManager);

    if(pkLightManager->GetVersion() < MakeVersion(1, 0))
    {
        unsigned int uiNumLightColor = 4;
        pkStream->Read(m_akColors, sizeof(CDayNightColor)*uiNumLightColor);
        m_akColors[FOG_COLOR] = m_akColors[AMBIENT_COLOR];
    }
    else
        pkStream->Read(m_akColors, sizeof(m_akColors));

	unsigned int uiNameLen;
	NiFixedString kName;
	pkStream->Read(&uiNameLen, sizeof(unsigned int));
	NIASSERT(uiNameLen < 1024);

	if( uiNameLen > 0 )
	{
		char acString[1024];
		pkStream->Read(acString, uiNameLen);
		acString[uiNameLen] = '\0';
		kName = acString;
	}
	else
	{
		kName = "";
	}
	SetName(kName);

	return true;
}

bool CTerrainLight::Save(NiBinaryStream* pkStream)
{
	pkStream->Write(&GetTranslate(), sizeof(NiPoint3));
	pkStream->Write(&m_fInnerRadius, sizeof(float));
	pkStream->Write(&m_fOuterRadius, sizeof(float));

	NiBool bGlobal = m_bGlobal;
	pkStream->Write(&bGlobal, sizeof(NiBool));

	pkStream->Write(m_akColors, sizeof(m_akColors));

	NiFixedString kName = GetName();
	unsigned int uiNameLen = kName.GetLength();
	pkStream->Write(&uiNameLen, sizeof(unsigned int));
	NIASSERT(uiNameLen < 1024);

	if( uiNameLen > 0 )
	{
		pkStream->Write((const char*)kName, uiNameLen);
	}

	return true;
}

NiColor CTerrainLight::GetColor(unsigned int uiColor, unsigned int uiHour, unsigned int uiMinute) const
{
	NIASSERT( uiColor < NUM_LIGHT_COLOR );

	return m_akColors[uiColor].GetColor(uiHour, uiMinute);
}

void CTerrainLight::SetColor( unsigned int uiColor, unsigned int uiHour, const NiColor& kColor ) 
{ 
	NIASSERT( uiColor < NUM_LIGHT_COLOR );
	m_akColors[uiColor].SetColor(uiHour, kColor);
}

CDayNightColor& CTerrainLight::GetDayNightColor(unsigned int uiColor)
{
	NIASSERT( uiColor < NUM_LIGHT_COLOR );

	return m_akColors[uiColor];
}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
//

NiFactoryDeclarePropIntf(CTerrainLightComponent);

NiFixedString CTerrainLightComponent::ms_kInnerRadiusName;
NiFixedString CTerrainLightComponent::ms_kOuterRadiusName;
NiFixedString CTerrainLightComponent::ms_kGlobalName;

NiFixedString CTerrainLightComponent::ms_kAmbientColorName;
NiFixedString CTerrainLightComponent::ms_kDiffuseColorName;
NiFixedString CTerrainLightComponent::ms_kWaterAmbientColorName;
NiFixedString CTerrainLightComponent::ms_kWaterDiffuseColorName;
NiFixedString CTerrainLightComponent::ms_kFogColorName;

NiFixedString CTerrainLightComponent::ms_kClassName;
NiFixedString CTerrainLightComponent::ms_kComponentName;

NiUniqueID CTerrainLightComponent::ms_kTemplateID;

void CTerrainLightComponent::_SDMInit()
{
	ms_kInnerRadiusName = "Inner Radius";
	ms_kOuterRadiusName = "Outer Radius";
	ms_kGlobalName = "Global Light";

	ms_kAmbientColorName = "Color (Ambient)";
	ms_kDiffuseColorName = "Color (Diffuse)";
	ms_kWaterAmbientColorName = "Water Color (Ambient)";
	ms_kWaterDiffuseColorName = "Water Color (Diffuse)";
    ms_kFogColorName = "Fog Color";

	ms_kClassName = "CTerrainLightComponent";
	ms_kComponentName = "Terrain Light";

	ms_kTemplateID = NiUniqueID(0x19,0x25,0xE,0xD8,0xF1,0xE2,0x9A,0x5D,0x92,0xDF,0x37,0xE7,0x21,0xE7,0x6E,0xEE);

	NiFactoryRegisterPropIntf(CTerrainLightComponent);
}

void CTerrainLightComponent::_SDMShutdown()
{
	ms_kInnerRadiusName = NULL;
	ms_kOuterRadiusName = NULL;
	ms_kGlobalName = NULL;

	ms_kAmbientColorName = NULL;
	ms_kDiffuseColorName = NULL;
	ms_kWaterAmbientColorName = NULL;
	ms_kWaterDiffuseColorName = NULL;
    ms_kFogColorName = NULL;

	ms_kClassName = NULL;
	ms_kComponentName = NULL;
}

const NiUniqueID& CTerrainLightComponent::GetStaticTemplateID()
{
	return ms_kTemplateID;
}

CTerrainLightComponent::CTerrainLightComponent()
{
	SetLightType(LT_DIRECTIONAL);

	SetOuterRadius(128.f);
	SetInnerRadius(64.f);
	SetGlobal(false);
}

NiEntityComponentInterface* CTerrainLightComponent::Clone(bool bInheritProperties)
{
	CTerrainLightComponent* pkClone = NiNew CTerrainLightComponent();
	pkClone->SetOuterRadius(GetOuterRadius());
	pkClone->SetInnerRadius(GetInnerRadius());
	pkClone->SetLightType(LT_DIRECTIONAL);
	pkClone->SetGlobal(false);

	NiFixedString kData;
	if( GetPropertyData(ms_kWaterAmbientColorName, kData, 0) )
	{
		pkClone->SetPropertyData(ms_kWaterAmbientColorName, kData, 0);
	}

	if( GetPropertyData(ms_kWaterDiffuseColorName, kData, 0) )
	{
		pkClone->SetPropertyData(ms_kWaterDiffuseColorName, kData, 0);
	}

	if( GetPropertyData(ms_kAmbientColorName, kData, 0) )
	{
		pkClone->SetPropertyData(ms_kAmbientColorName, kData, 0);
	}

	if( GetPropertyData(ms_kDiffuseColorName, kData, 0) )
	{
		pkClone->SetPropertyData(ms_kDiffuseColorName, kData, 0);
	}

    if( GetPropertyData(ms_kFogColorName, kData, 0) )
    {
        pkClone->SetPropertyData(ms_kFogColorName, kData, 0);
    }

	return pkClone;
}

NiUniqueID  CTerrainLightComponent::GetTemplateID()
{
	return ms_kTemplateID;
}

NiFixedString CTerrainLightComponent::GetClassName() const
{
	return ms_kClassName;
}

NiFixedString CTerrainLightComponent::GetName() const
{
	return ms_kComponentName;
}

void CTerrainLightComponent::Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager)
{
	if( !m_spLight )
	{
		m_spLight = CTerrainLightManager::Get()->CreateLight(pkParentEntity);
		NIASSERT( m_spLight );

		// Enable setting of light properties.
		SetLightPropertiesChanged(true);
		SetAffectedEntitiesChanged(true);

		// Perform initial update.
		m_spLight->Update(0.0f);
	}

	if( m_spLight && GetLightPropertiesChanged() )
	{
		CTerrainLight* pkLight = (CTerrainLight*)(NiLight*)m_spLight;
		pkLight->SetInnerRadius(GetInnerRadius());
		pkLight->SetOuterRadius(GetOuterRadius());
		pkLight->SetGlobal(IsGlobal());

		for( unsigned int i = 0; i < NUM_LIGHT_COLOR; i++ )
		{
			CDayNightColor& kDayNightColor = pkLight->GetDayNightColor(i);
			kDayNightColor.FromString( m_akDayNightColors[i].ToString() );
		}
	}

	NiLightComponent::Update(pkParentEntity, fTime, pkErrors, pkAssetManager);
}

void CTerrainLightComponent::GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const
{
	kPropertyNames.Add(ms_kInnerRadiusName);
	kPropertyNames.Add(ms_kOuterRadiusName);
	kPropertyNames.Add(ms_kGlobalName);

	kPropertyNames.Add(ms_kDiffuseColorName);
	kPropertyNames.Add(ms_kAmbientColorName);

	kPropertyNames.Add(ms_kWaterDiffuseColorName);
	kPropertyNames.Add(ms_kWaterAmbientColorName);

    kPropertyNames.Add(ms_kFogColorName);
}

NiBool CTerrainLightComponent::CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const
{
	if( NiLightComponent::CanResetProperty(kPropertyName, bCanReset) )
		return true;

	if( kPropertyName == ms_kInnerRadiusName
	 || kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName
	 || kPropertyName == ms_kAmbientColorName
	 || kPropertyName == ms_kDiffuseColorName
	 || kPropertyName == ms_kWaterAmbientColorName
	 || kPropertyName == ms_kWaterDiffuseColorName
     || kPropertyName == ms_kFogColorName )
	{
		bCanReset = false;
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::ResetProperty(const NiFixedString& kPropertyName)
{
	if( NiLightComponent::ResetProperty(kPropertyName) )
		return true;

	return false;
}

NiBool CTerrainLightComponent::MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique)
{
	if( NiLightComponent::MakePropertyUnique(kPropertyName, bMadeUnique) )
		return true;

	if( kPropertyName == ms_kInnerRadiusName
	 || kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName
	 || kPropertyName == ms_kAmbientColorName
	 || kPropertyName == ms_kDiffuseColorName
	 || kPropertyName == ms_kWaterAmbientColorName
	 || kPropertyName == ms_kWaterDiffuseColorName 
     || kPropertyName == ms_kFogColorName )
	{
		bMadeUnique = false;
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const
{
	if( kPropertyName == ms_kInnerRadiusName
	 || kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName )
	{
		kDisplayName = kPropertyName;
	}
	else if( kPropertyName == ms_kDiffuseColorName )
	{
		kDisplayName = "1: 太阳光";
	}
	else if( kPropertyName == ms_kAmbientColorName )
	{
		kDisplayName = "2: 环境光";
	}
	else if( kPropertyName == ms_kWaterDiffuseColorName )
	{
		kDisplayName = "3: 水面接收的太阳光";
	}
	else if( kPropertyName == ms_kWaterAmbientColorName )
	{
		kDisplayName = "4: 水面接收的环境光";
	}
    else if( kPropertyName == ms_kFogColorName )
    {
        kDisplayName = "5: 雾颜色";
    }
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const
{
	if( kPropertyName == ms_kInnerRadiusName ||
		kPropertyName == ms_kOuterRadiusName)
	{
		kPrimitiveType = PT_FLOAT;
	}
	else if( kPropertyName == ms_kGlobalName )
	{
		kPrimitiveType = PT_BOOL;
	}
	else if( kPropertyName == ms_kWaterAmbientColorName
		  || kPropertyName == ms_kWaterDiffuseColorName
		  || kPropertyName == ms_kAmbientColorName
		  || kPropertyName == ms_kDiffuseColorName 
          || kPropertyName == ms_kFogColorName )
	{
		kPrimitiveType = PT_STRING;
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const
{
	if( kPropertyName == ms_kInnerRadiusName ||
		kPropertyName == ms_kOuterRadiusName)
	{
		kSemanticType = PT_FLOAT;
	}
	else if( kPropertyName == ms_kGlobalName )
	{
		kSemanticType = PT_BOOL;
	}
	else if( kPropertyName == ms_kWaterAmbientColorName
		  || kPropertyName == ms_kWaterDiffuseColorName
		  || kPropertyName == ms_kAmbientColorName
		  || kPropertyName == ms_kDiffuseColorName 
          || kPropertyName == ms_kFogColorName )
	{
		kSemanticType = "Day Night Color";
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const
{
	if( kPropertyName == ms_kInnerRadiusName
	 ||	kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName
	 || kPropertyName == ms_kWaterAmbientColorName
	 || kPropertyName == ms_kWaterDiffuseColorName
	 || kPropertyName == ms_kAmbientColorName
	 || kPropertyName == ms_kDiffuseColorName 
     || kPropertyName == ms_kFogColorName )
	{
		kDescription = kPropertyName;
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const
{
	if( kPropertyName == ms_kInnerRadiusName
	 ||	kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName
	 || kPropertyName == ms_kWaterAmbientColorName
	 || kPropertyName == ms_kWaterDiffuseColorName
	 || kPropertyName == ms_kAmbientColorName
	 || kPropertyName == ms_kDiffuseColorName 
     || kPropertyName == ms_kFogColorName )
	{
		kCategory = ms_kComponentName;
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly)
{
	if( kPropertyName == ms_kInnerRadiusName
	 ||	kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName )
	{
		bIsReadOnly = false;
	}
	else if( kPropertyName == ms_kWaterAmbientColorName
		  || kPropertyName == ms_kWaterDiffuseColorName
		  || kPropertyName == ms_kAmbientColorName
		  || kPropertyName == ms_kDiffuseColorName 
          || kPropertyName == ms_kFogColorName )
	{
		bIsReadOnly = false;
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique)
{
	if( NiLightComponent::IsPropertyUnique(kPropertyName, bIsUnique) )
		return true;

	if( kPropertyName == ms_kInnerRadiusName
	 ||	kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName
	 || kPropertyName == ms_kWaterAmbientColorName
	 || kPropertyName == ms_kWaterDiffuseColorName
	 || kPropertyName == ms_kAmbientColorName
	 || kPropertyName == ms_kDiffuseColorName 
     || kPropertyName == ms_kFogColorName )
	{
		bIsUnique = true;
	}
	else
	{
		return false;
	}


	return true;
}

NiBool CTerrainLightComponent::IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable)
{
	if( NiLightComponent::IsPropertySerializable(kPropertyName, bIsSerializable) )
		return true;

	if( kPropertyName == ms_kInnerRadiusName
	 ||	kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName
	 || kPropertyName == ms_kWaterAmbientColorName
	 || kPropertyName == ms_kWaterDiffuseColorName
	 || kPropertyName == ms_kAmbientColorName
	 || kPropertyName == ms_kDiffuseColorName
     || kPropertyName == ms_kFogColorName )
	{
		bIsSerializable = true;
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable)
{
	if( NiLightComponent::IsPropertyInheritable(kPropertyName, bIsInheritable) )
		return true;

	if( kPropertyName == ms_kInnerRadiusName
	 ||	kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName
	 || kPropertyName == ms_kWaterAmbientColorName
	 || kPropertyName == ms_kWaterDiffuseColorName
	 || kPropertyName == ms_kAmbientColorName
	 || kPropertyName == ms_kDiffuseColorName 
     || kPropertyName == ms_kFogColorName )
	{
		bIsInheritable = false;
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex, bool& bIsExternalAssetPath) const
{
	if( NiLightComponent::IsExternalAssetPath(kPropertyName, uiIndex, bIsExternalAssetPath) )
		return true;

	if( kPropertyName == ms_kInnerRadiusName
	 ||	kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName
	 || kPropertyName == ms_kWaterAmbientColorName
	 || kPropertyName == ms_kWaterDiffuseColorName
	 || kPropertyName == ms_kAmbientColorName
	 || kPropertyName == ms_kDiffuseColorName 
     || kPropertyName == ms_kFogColorName )
	{
		bIsExternalAssetPath = false;
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const
{
	if( NiLightComponent::GetElementCount(kPropertyName, uiCount) )
		return true;

	if( kPropertyName == ms_kInnerRadiusName
	 ||	kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName
	 || kPropertyName == ms_kWaterAmbientColorName
	 || kPropertyName == ms_kWaterDiffuseColorName
	 || kPropertyName == ms_kAmbientColorName
	 || kPropertyName == ms_kDiffuseColorName 
     || kPropertyName == ms_kFogColorName )
	{
		uiCount = 1;
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet)
{
	if( NiLightComponent::SetElementCount(kPropertyName, uiCount, bCountSet) )
		return true;

	if( kPropertyName == ms_kInnerRadiusName
	 ||	kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName
	 || kPropertyName == ms_kWaterAmbientColorName
	 || kPropertyName == ms_kWaterDiffuseColorName
	 || kPropertyName == ms_kAmbientColorName
	 || kPropertyName == ms_kDiffuseColorName 
     || kPropertyName == ms_kFogColorName )
	{
		bCountSet = false;
	}
	else
	{
		return false;
	}


	return true;
}

NiBool CTerrainLightComponent::IsCollection(const NiFixedString& kPropertyName, bool& bIsCollection) const
{
	if( NiLightComponent::IsCollection(kPropertyName, bIsCollection) )
		return true;

	if( kPropertyName == ms_kInnerRadiusName
	 ||	kPropertyName == ms_kOuterRadiusName
	 || kPropertyName == ms_kGlobalName
	 || kPropertyName == ms_kWaterAmbientColorName
	 || kPropertyName == ms_kWaterDiffuseColorName
	 || kPropertyName == ms_kAmbientColorName
	 || kPropertyName == ms_kDiffuseColorName 
     || kPropertyName == ms_kFogColorName )
	{
		bIsCollection = false;
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::GetPropertyData(const NiFixedString& kPropertyName, float& fData, unsigned int uiIndex) const
{
	if( NiLightComponent::GetPropertyData(kPropertyName, fData, uiIndex) )
		return true;

	if( uiIndex != 0 )
	{
		return false;
	}

	if( kPropertyName == ms_kInnerRadiusName )
	{
		fData = GetInnerRadius();
	}
	else if( kPropertyName == ms_kOuterRadiusName )
	{
		fData = GetOuterRadius();
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::SetPropertyData(const NiFixedString& kPropertyName, float fData, unsigned int uiIndex)
{
	if( NiLightComponent::SetPropertyData(kPropertyName, fData, uiIndex) )
		return true;

	if( uiIndex != 0 )
	{
		return false;
	}

	if( kPropertyName == ms_kInnerRadiusName )
	{
		SetInnerRadius(fData);
	}
	else if( kPropertyName == ms_kOuterRadiusName )
	{
		SetOuterRadius(fData);
	}
	else
	{
		return false;
	}


	return true;
}

NiBool CTerrainLightComponent::GetPropertyData(const NiFixedString& kPropertyName, NiFixedString& kData, unsigned int uiIndex) const
{
	if( uiIndex != 0 )
	{
		return false;
	}

	if( kPropertyName == ms_kWaterAmbientColorName )
	{
		kData = m_akDayNightColors[WATER_AMBIENT_COLOR].ToString();
	}
	else if( kPropertyName == ms_kWaterDiffuseColorName )
	{
		kData = m_akDayNightColors[WATER_DIFFUSE_COLOR].ToString();
	}
	else if( kPropertyName == ms_kAmbientColorName )
	{
		kData = m_akDayNightColors[AMBIENT_COLOR].ToString();
	}
	else if( kPropertyName == ms_kDiffuseColorName )
	{
		kData = m_akDayNightColors[DIFFUSE_COLOR].ToString();
	}
    else if( kPropertyName == ms_kFogColorName )
    {
        kData = m_akDayNightColors[FOG_COLOR].ToString();
    }
	else
	{
		if( NiLightComponent::GetPropertyData(kPropertyName, kData, uiIndex) )
			return true;

		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiFixedString& kData, unsigned int uiIndex)
{
	if( uiIndex != 0 )
	{
		return false;
	}

	if( kPropertyName == ms_kWaterAmbientColorName )
	{
		m_akDayNightColors[WATER_AMBIENT_COLOR].FromString(kData);
	}
	else if( kPropertyName == ms_kWaterDiffuseColorName )
	{
		m_akDayNightColors[WATER_DIFFUSE_COLOR].FromString(kData);
	}
	else if( kPropertyName == ms_kAmbientColorName )
	{
		m_akDayNightColors[AMBIENT_COLOR].FromString(kData);
	}
	else if( kPropertyName == ms_kDiffuseColorName )
	{
		m_akDayNightColors[DIFFUSE_COLOR].FromString(kData);
	}
    else if( kPropertyName == ms_kFogColorName )
    {
        m_akDayNightColors[FOG_COLOR].FromString(kData);
    }
	else
	{
		if( NiLightComponent::SetPropertyData(kPropertyName, kData, uiIndex) )
			return true;

		return false;
	}

	SetLightPropertiesChanged(true);
	return true;
}

NiBool CTerrainLightComponent::GetPropertyData(const NiFixedString& kPropertyName, NiColor& kData, unsigned int uiIndex) const
{
	if( NiLightComponent::GetPropertyData(kPropertyName, kData, uiIndex) )
		return true;

	return false;
}

NiBool CTerrainLightComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiColor& kData, unsigned int uiIndex)
{
	if( NiLightComponent::SetPropertyData(kPropertyName, kData, uiIndex) )
		return true;

	return false;
}

NiBool CTerrainLightComponent::GetPropertyData(const NiFixedString& kPropertyName, NiPoint3& kData, unsigned int uiIndex) const
{
	if( NiLightComponent::GetPropertyData(kPropertyName, kData, uiIndex) )
		return true;

	return false;
}

NiBool CTerrainLightComponent::GetPropertyData(const NiFixedString& kPropertyName, bool& bData, unsigned int uiIndex) const
{
	if( uiIndex != 0 )
	{
		return false;
	}

	if( kPropertyName == ms_kGlobalName )
	{
		bData = IsGlobal();
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::SetPropertyData(const NiFixedString& kPropertyName, bool bData, unsigned int uiIndex)
{
	if( uiIndex != 0 )
	{
		return false;
	}

	if( kPropertyName == ms_kGlobalName )
	{
		SetGlobal(bData);
	}
	else
	{
		return false;
	}

	return true;
}

NiBool CTerrainLightComponent::GetPropertyData(const NiFixedString& kPropertyName, NiObject*& pkData, unsigned int uiIndex) const
{
	if( NiLightComponent::GetPropertyData(kPropertyName, pkData, uiIndex) )
		return true;

	return false;
}

NiBool CTerrainLightComponent::GetPropertyData(const NiFixedString& kPropertyName, NiEntityInterface*& pkData, unsigned int uiIndex) const
{
	if( NiLightComponent::GetPropertyData(kPropertyName, pkData, uiIndex) )
		return true;

	return false;
}

NiBool CTerrainLightComponent::SetPropertyData(const NiFixedString& kPropertyName, NiEntityInterface* pkData, unsigned int uiIndex)
{
	if( NiLightComponent::SetPropertyData(kPropertyName, pkData, uiIndex) )
		return true;

	return false;
}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
//

#define FOG_NEAR 0.1f
#define FOG_FAR   470.0f

CTerrainLightManager* CTerrainLightManager::ms_TerrainLightManager = NULL;
bool CTerrainLightManager::ms_bFogEnable = true;

CTerrainLightManager::CTerrainLightManager()
:m_bDirty(false)
,m_bStopTime(true)
,m_uiCycleType(0)
,m_uiHour(12)
,m_uiMinute(0)
,m_fFogNear(FOG_NEAR)
,m_fFogFar(FOG_FAR)
{
	NIASSERT(ms_TerrainLightManager == NULL );
	ms_TerrainLightManager = this;

    m_spFogProperty = NiNew NiFogProperty;
    
    m_spFogProperty->SetFogFunction(NiFogProperty::FOG_Z_LINEAR);
    m_spFogProperty->SetFog(true);

	m_spFogProperty->SetNear( m_fFogNear );
	m_spFogProperty->SetFar( m_fFogFar );
    m_fFogDensity = 0.04f;
    m_spFogProperty->SetDepth(m_fFogDensity); //修改后的引擎用Depth 来表现Density

	ResetColors();
}

CTerrainLightManager::~CTerrainLightManager()
{
	for( unsigned int i = 0; i < m_akLightList.GetSize(); i++ )
	{
		m_akLightList.GetAt(i)->DecRefCount();
	}

	for( unsigned int i = 0; i < m_akLightEntityList.GetSize(); i++ )
	{
		m_akLightEntityList.GetAt(i)->RemoveReference();
	}
}

CTerrainLightManager* CTerrainLightManager::Get() 
{ 
	return ms_TerrainLightManager; 
} 

bool CTerrainLightManager::Load(const NiFixedString& kMapName)
{
	char acFilename[NI_MAX_PATH];
	//合成绝对路径
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\World\\Maps\\%s\\%s.lit",  TerrainHelper::GetClientPath(), (const char*)kMapName, (const char*)kMapName);

	NiFile* pkFile = NiFile::GetFile(acFilename, NiFile::READ_ONLY);
	if( !pkFile || !(*pkFile) )
	{ 
		NiDelete pkFile;
		return false;
	}

	for( unsigned int i = 0; i < m_akLightList.GetSize(); i++ )
	{
		NiDelete m_akLightList.GetAt(i);
	}
	m_akLightList.RemoveAll();

	m_uiCurrentLight = 0xFFFFFF;

	int iSign;
	pkFile->Read(&iSign, sizeof(int));
	pkFile->Read(&m_uiVersion, sizeof(int));
	NIASSERT(iSign == TERRAIN_LIGHT_SIGN);

	unsigned int uiSize;
	pkFile->Read(&uiSize, sizeof(unsigned int));
	m_akLightList.SetSize( uiSize );

	CTerrainLight* pkLight;
	for( unsigned int i = 0; i < uiSize; i++ )
	{
		pkLight = NiNew CTerrainLight();
		pkLight->IncRefCount();

		pkLight->Load( pkFile );
		m_akLightList.SetAt(i, pkLight);
	}

	m_bDirty = false;

	NiDelete pkFile;

	return true;
}

bool CTerrainLightManager::Save(const NiFixedString& kMapName)
{
	NiMemStream kMemStream;

	int iSign = TERRAIN_LIGHT_SIGN;
	kMemStream.Write(&iSign, sizeof(int));

    unsigned int uiVersion = MakeVersion(1, 0);
	kMemStream.Write(&uiVersion, sizeof(uiVersion));

	unsigned int uiSize = m_akLightList.GetSize();
	kMemStream.Write(&uiSize, sizeof(unsigned int));

	for( unsigned int i = 0; i < uiSize; i++ )
	{
		m_akLightList.GetAt(i)->Save(&kMemStream);
	}

	{
		char acFilename[NI_MAX_PATH];
		//合成绝对路径
		NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\World\\Maps\\%s\\%s.lit",  TerrainHelper::GetClientPath(), (const char*)kMapName, (const char*)kMapName);

		NiFile* pkFile = NiFile::GetFile(acFilename, NiFile::WRITE_ONLY);
		if( !pkFile || !(*pkFile) )
		{ 
			NiDelete pkFile;
			return false;		
		}

		pkFile->Write(kMemStream.Str(), kMemStream.GetSize());
		kMemStream.Freeze(false);

		NiDelete pkFile;
	}

	m_bDirty = false;
	return true;
}

void CTerrainLightManager::ApplyFog(NiDX9RenderState* pkRenderState,const NiColor& kFogColor, float fDepth, float fStart /* = 0.0f */, float fEnd /* = 0.0f */)
{
	pkRenderState->SetRenderState(D3DRS_FOGENABLE, TRUE);
	float fFogNear = fStart;
	float fFogFar = fEnd;

	pkRenderState->SetRenderState(D3DRS_FOGDENSITY, F2DW(fDepth));
	pkRenderState->SetRenderState(D3DRS_FOGSTART, F2DW(fFogNear));
	pkRenderState->SetRenderState(D3DRS_FOGEND, F2DW(fFogFar));       
	pkRenderState->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_LINEAR);
	pkRenderState->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);
	pkRenderState->SetRenderState(D3DRS_RANGEFOGENABLE, FALSE);

	unsigned int uiFogColor = D3DCOLOR_XRGB((unsigned char)(255.0f*kFogColor.r), (unsigned char)(255.0f*kFogColor.g), (unsigned char)(255.0f*kFogColor.b));
	pkRenderState->SetRenderState(D3DRS_FOGCOLOR, uiFogColor);

	m_spFogProperty->SetFogColor(kFogColor);
	m_spFogProperty->SetDepth(fDepth);
	m_spFogProperty->SetNear(fFogNear);
	m_spFogProperty->SetFar(fFogFar);

}

void CTerrainLightManager::ResetColors()
{
	for( int i = 0; i < NUM_LIGHT_COLOR; i++ )
	{
		m_akColors[i] = DEFAULT_COLOR;
	}

	m_akColors[AMBIENT_COLOR] *= 0.5f;
}

CTerrainLight* CTerrainLightManager::CreateLight(NiEntityPropertyInterface* pkEntity)
{
	CTerrainLight* pkLight = GetLight(pkEntity->GetName());
	if( !pkLight )
	{
		m_bDirty = true;

		pkLight = NiNew CTerrainLight();
		pkLight->IncRefCount();

		pkLight->SetName(pkEntity->GetName());
		m_akLightList.Add(pkLight);


		AddLightEntity(pkEntity);
	}

	return pkLight;
}

void CTerrainLightManager::DeleteLight(NiEntityPropertyInterface* pkEntity)
{
	CTerrainLight* pkLight;
	for( unsigned int i = 0; i < m_akLightList.GetSize(); i++ )
	{
		pkLight = m_akLightList.GetAt(i);
		if( pkLight->GetName().EqualsNoCase(pkEntity->GetName()) )
		{
			pkLight->DecRefCount();

			//m_akLightList.RemoveAt(i);
			//从数组里面删除, 后面的向前移动
			for( unsigned int j = i; j + 1 < m_akLightList.GetSize(); j++ )
			{
				m_akLightList.SetAt(j, m_akLightList.GetAt(j + 1));
			}
			m_akLightList.SetSize(m_akLightList.GetSize() - 1);
			break;
		}
	}

	RemoveLightEntity(pkEntity);

	if( m_akLightList.GetSize() == 0 )
	{
		ResetColors();
	}
}

bool CTerrainLightManager::AddLightEntity(NiEntityPropertyInterface* pkEntity)
{
	for( unsigned int i = 0; i < m_akLightEntityList.GetSize(); i++ )
	{
		if( m_akLightEntityList.GetAt(i) == pkEntity )
			return false;
	}

	pkEntity->AddReference();
	m_akLightEntityList.Add(pkEntity);

	return true;
}

bool CTerrainLightManager::RemoveLightEntity(NiEntityPropertyInterface* pkEntity)
{
	for( unsigned int i = 0; i < m_akLightEntityList.GetSize(); i++ )
	{
		if( m_akLightEntityList.GetAt(i) == pkEntity )
		{
			//m_akLightList.RemoveAt(i);
			//从数组里面删除, 后面的向前移动
			for( unsigned int j = i; j + 1 < m_akLightEntityList.GetSize(); j++ )
			{
				m_akLightEntityList.SetAt(j, m_akLightEntityList.GetAt(j + 1));
			}
			m_akLightEntityList.SetSize(m_akLightEntityList.GetSize() - 1);

			pkEntity->RemoveReference();
			return true;
		}
	}

	return false;
}

CTerrainLight* CTerrainLightManager::GetLight(const NiFixedString& kName)
{
	for( unsigned int i = 0; i < m_akLightList.GetSize(); i++ )
	{
		if( m_akLightList.GetAt(i)->GetName().EqualsNoCase(kName) )
			return m_akLightList.GetAt(i);
	}

	return NULL;
}

void CTerrainLightManager::ChangeLightName(const NiFixedString& kNewName, const NiFixedString& kOldName)
{
	if( kNewName == kOldName )
		return;

	CTerrainLight* pkLight = GetLight(kOldName);
	if( pkLight )
		pkLight->SetName(kNewName);
}


void CTerrainLightManager::CalcLightWeights(const NiPoint3& pos)
{
	NIASSERT(m_akLightList.GetSize() > 0);

	int iMaxLight = int(m_akLightList.GetSize()) - 1;
	m_akLightList.GetAt(iMaxLight)->SetWeight(1.f);
	m_uiCurrentLight = (unsigned int)iMaxLight;

	CTerrainLight* pkLight;
	for(int i = iMaxLight - 1; i >= 0; i--) 
	{
		pkLight = m_akLightList.GetAt(i);
		NiPoint3 kDiff = pos - pkLight->GetTranslate();

		float fDist = kDiff.Length();
		if( fDist <= pkLight->GetInnerRadius() )
		{
			// we're in a sky, zero out the rest
			pkLight->SetWeight(1.f);
			m_uiCurrentLight = unsigned int(i);

			for(int j = i + 1; j < (int)m_akLightList.GetSize(); j++)
				m_akLightList.GetAt(j)->SetWeight(0.f);
		}
		else if(fDist < pkLight->GetOuterRadius())
		{
			// we're in an outer area, scale down the other weights
			float r = (fDist - pkLight->GetInnerRadius())/(pkLight->GetOuterRadius() - pkLight->GetInnerRadius());
			pkLight->SetWeight(1.0f - r);

			for(int j = i + 1; j < (int)m_akLightList.GetSize(); j++) 
				m_akLightList.GetAt(j)->SetWeight(m_akLightList.GetAt(j)->GetWeight() * r);
		}
		else
		{
			pkLight->SetWeight(0.f);
		}
	}
}

void CTerrainLightManager::SortLights()
{
	CTerrainLight* pkTerrainLight1;
	CTerrainLight* pkTerrainLight2;
	for( unsigned int ui = 0; ui < m_akLightList.GetSize(); ui++ )
	{
		for( unsigned int uj = ui + 1; uj < m_akLightList.GetSize(); uj++ )
		{
			pkTerrainLight1 = m_akLightList.GetAt(ui);
			pkTerrainLight2 = m_akLightList.GetAt(uj);

			if( *pkTerrainLight2 < *pkTerrainLight1 )
			{
				m_akLightList.SetAt(uj, pkTerrainLight1);
				m_akLightList.SetAt(ui, pkTerrainLight2);
			}
		}
	}
}

void CTerrainLightManager::UpdateTime()
{
	if( m_bStopTime )
	{
		//当前时间
	}
	else if( m_uiCycleType == 0 )
	{
		//真实时间

		SYSTEMTIME st;
		GetLocalTime(&st);

		m_uiHour = st.wHour;
		m_uiMinute = st.wMinute;
	}
	else
	{
		//快速循环的时间

		m_uiMinute += m_uiCycleType;
		if(m_uiMinute >= 60)
		{
			++m_uiHour;
			m_uiMinute = 0;
		}

		if(m_uiHour >= 24)
			m_uiHour = 0;
	}
}

void CTerrainLightManager::Update(const NiPoint3& pos)
{
	UpdateTime();

	if(m_akLightList.GetSize())
    {
        SortLights();

        CalcLightWeights(pos);

        memset(&m_akColors, 0, sizeof(m_akColors));

        CTerrainLight* pkTerrainLight;
        for( unsigned int i = 0; i < m_akLightList.GetSize(); i++ ) 
        {
            pkTerrainLight = m_akLightList.GetAt(i);
            if( pkTerrainLight->GetWeight() > 0.f )
            {
                // now calculate the color rows
                for( int j = 0; j < NUM_LIGHT_COLOR; j++ )
                {
                    m_akColors[j] += pkTerrainLight->GetColor(j, m_uiHour, m_uiMinute) * pkTerrainLight->GetWeight();
                }
            }
        }
    }

    //
    m_spFogProperty->SetFogColor(GetFogColor());
    m_spFogProperty->SetFog(ms_bFogEnable);

	NiD3DRenderState* pkRenderState = ((NiD3DRenderer*)NiRenderer::GetRenderer())->GetRenderState();

	if ( ms_bFogEnable )
	{
		float fDepth = m_spFogProperty->GetDepth();

		const NiColor& kFogColor = GetFogColor();
		ApplyFog(pkRenderState, kFogColor, fDepth, FOG_NEAR, FOG_FAR);
	}
	else
	{
		pkRenderState->SetRenderState(D3DRS_FOGENABLE, FALSE);
		pkRenderState->SetRenderState(D3DRS_FOGCOLOR, 0);
	}
}

void CTerrainLightManager::UpdateLightEntities(NiEntityPropertyInterface* pkParentEntity, float fTime)
{
	NiExternalAssetManager* pkAssetManager = CMap::GetAssetManager();
	NiEntityErrorInterface* pkErrors = const_cast<NiEntityErrorInterface *>(CMap::Get()->GetErrorHandler());

	NiEntityPropertyInterface* pkEntity;
	for( unsigned int i = 0; i < m_akLightEntityList.GetSize(); i++ )
	{
		pkEntity = m_akLightEntityList.GetAt(i);
		if( pkEntity )
			pkEntity->Update(pkParentEntity, fTime, pkErrors, pkAssetManager);
	}
}

void CTerrainLightManager::GetEntities(NiScene* pkScene)
{
	CTerrainLight* pkLight;

	NiGeneralEntity* pkEntity;

	NiTransformationComponent* pkTransformationComponent;
	CTerrainLightComponent* pkTerrainLightComponent;
	for( unsigned int i = 0; i < m_akLightList.GetSize(); i++ ) 
	{
		pkLight = m_akLightList.GetAt(i);

		pkEntity = NiNew NiGeneralEntity;
		pkEntity->SetName(pkLight->GetName());

		pkTransformationComponent = NiNew NiTransformationComponent;
		pkTransformationComponent->SetTranslation(pkLight->GetTranslate());
		pkTransformationComponent->SetRotation(pkLight->GetRotate());
		pkTransformationComponent->SetScale(pkLight->GetScale());
		pkEntity->AddComponent(pkTransformationComponent, false);

		pkTerrainLightComponent = NiNew CTerrainLightComponent;
		pkTerrainLightComponent->SetAmbientColor(pkLight->GetAmbientColor());
		pkTerrainLightComponent->SetDiffuseColor(pkLight->GetDiffuseColor());
		pkTerrainLightComponent->SetInnerRadius(pkLight->GetInnerRadius());
		pkTerrainLightComponent->SetOuterRadius(pkLight->GetOuterRadius());
		pkTerrainLightComponent->SetGlobal(pkLight->IsGlobal());

		for( unsigned int i = 0; i < NUM_LIGHT_COLOR; i++ )
		{
			CDayNightColor& kDayNightColor = pkLight->GetDayNightColor(i);
			pkTerrainLightComponent->SetDayNightColor(i, kDayNightColor);
		}

		pkEntity->AddComponent(pkTerrainLightComponent, false);

		pkScene->AddEntity(pkEntity);

		AddLightEntity(pkEntity);
	}
}

bool CTerrainLightManager::SetFogEnable(bool bFogEnable) 
{ 
    if(ms_bFogEnable && bFogEnable)
        return false;

    if(!ms_bFogEnable && !bFogEnable)
        return false;

    ms_bFogEnable = bFogEnable;
    return true;
}

bool CTerrainLightManager::GetFogEnable()
{ 
    return ms_bFogEnable; 
}
