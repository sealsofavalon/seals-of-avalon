#ifndef TERRAIN_LIGHT_H
#define TERRAIN_LIGHT_H

#include "TerrainLibType.h"
#include "Tile.h"

#define DEFAULT_COLOR NiColor(1.0f, 1.0f, 1.0f)
#define CAMERA_FAR (512.f)

enum LightColorNames 
{
	DIFFUSE_COLOR = 0,
	AMBIENT_COLOR,
	WATER_DIFFUSE_COLOR,
	WATER_AMBIENT_COLOR,
    FOG_COLOR,
	NUM_LIGHT_COLOR = 16, //保留一些方便以后扩充 
};

#define NUM_DAY_NIGHT_COLOR  24
class TERRAIN_ENTRY CDayNightColor : public NiMemObject
{
private:
	NiDeclareFlags(unsigned int);
	NiColor m_kColors[NUM_DAY_NIGHT_COLOR];

public:
	CDayNightColor();
	NiColor GetColor(unsigned int uiHour, unsigned int uiMinute) const;
	void SetColor(unsigned int iHour, const NiColor& kColor);
	void Enable(unsigned int iHour, bool bEnable);
	bool IsEnable(unsigned int iHour) const;
	void CalcColors();
	NiFixedString ToString() const;
	bool FromString(const NiFixedString& kData);

	bool Load(NiBinaryStream* pkStream);
	bool Save(NiBinaryStream* pkStream);
};

class TERRAIN_ENTRY CTerrainLight : public NiDirectionalLight
{
private:
	float			m_fInnerRadius;//内半径
	float			m_fOuterRadius;//内外半径
	bool			m_bGlobal;
	CDayNightColor 	m_akColors[NUM_LIGHT_COLOR];

	float			m_fWeight;

public:
	CTerrainLight()
		:m_fInnerRadius(float(TILE_SIZE/2))
		,m_fOuterRadius(float(TILE_SIZE))
		,m_bGlobal(false)
		,m_fWeight(0.f)
	{
		NiMatrix3 kMat;
		kMat.MakeIdentity();
		NiPoint3 kDir(1, 1, -1);
		kDir.Unitize();
		kMat.SetCol(0, kDir);
		SetRotate(kMat);
	}

	bool operator<(const CTerrainLight& l) const
	{
		//全局的最大
		if (m_bGlobal)
			return false;

		if (l.m_bGlobal)
			return true;

		if( NiAbs(m_fOuterRadius - l.m_fOuterRadius) > 0.1f )
			return m_fOuterRadius < l.m_fOuterRadius; 

		return GetTranslate().x < l.GetTranslate().x;
	}

	float GetInnerRadius() const { return m_fInnerRadius; }
	void SetInnerRadius( float fInnerRadius ) {  m_fInnerRadius = fInnerRadius; }

	float GetOuterRadius() const { return m_fOuterRadius; }
	void SetOuterRadius( float fOuterRadius ) { m_fOuterRadius = fOuterRadius; }

	float GetWeight() const { return m_fWeight; }
	void SetWeight( float fWeight ) { m_fWeight = fWeight; }

	bool IsGlobal() const { return m_bGlobal; }
	void SetGlobal( bool bGlobal ) { m_bGlobal = bGlobal; }

	NiColor GetColor(unsigned int uiColor, unsigned int uiHour, unsigned int uiMinute) const;
	void SetColor( unsigned int uiColor, unsigned int uiHour, const NiColor& kColor );

	CDayNightColor& GetDayNightColor(unsigned int uiColor);

	bool Load(NiBinaryStream* pkStream);
	bool Save(NiBinaryStream* pkStream);
};

//-------------------------------------------------------------------------------------------------------------------------------------------------------
//
//

class TERRAIN_ENTRY CTerrainLightComponent : public NiLightComponent
{
private:
	static NiFixedString ms_kInnerRadiusName;
	static NiFixedString ms_kOuterRadiusName;
	static NiFixedString ms_kGlobalName;

	static NiFixedString ms_kAmbientColorName;
	static NiFixedString ms_kDiffuseColorName;
	static NiFixedString ms_kWaterAmbientColorName;
	static NiFixedString ms_kWaterDiffuseColorName;
    static NiFixedString ms_kFogColorName;

	static NiFixedString ms_kClassName;
	static NiFixedString ms_kComponentName;

	float	m_fInnerRadius;//内半径
	float	m_fOuterRadius;//内外半径
	bool	m_bGlobal;
	CDayNightColor m_akDayNightColors[NUM_LIGHT_COLOR];

	static NiUniqueID ms_kTemplateID;


public:
    static void _SDMInit();
    static void _SDMShutdown();

	CTerrainLightComponent();

	virtual NiEntityComponentInterface* Clone(bool bInheritProperties);
	virtual NiUniqueID GetTemplateID();
    virtual NiFixedString GetClassName() const;
    virtual NiFixedString GetName() const;
    virtual void Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager);
    virtual void GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const;
    virtual NiBool CanResetProperty(const NiFixedString& kPropertyName,bool& bCanReset) const;
    virtual NiBool ResetProperty(const NiFixedString& kPropertyName);
    virtual NiBool MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique);
    virtual NiBool GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const;
    virtual NiBool GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const;
    virtual NiBool GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const;
    virtual NiBool GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const;
    virtual NiBool GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const;
    virtual NiBool IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly);
    virtual NiBool IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique);
    virtual NiBool IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable);
    virtual NiBool IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable);
    virtual NiBool IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex, bool& bIsExternalAssetPath) const;
    virtual NiBool GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const;
    virtual NiBool SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet);
    virtual NiBool IsCollection(const NiFixedString& kPropertyName, bool& bIsCollection) const;
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, float& fData, unsigned int uiIndex) const;
    virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, float fData, unsigned int uiIndex);
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiFixedString& kData, unsigned int uiIndex) const;
    virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiFixedString& kData, unsigned int uiIndex);
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiColor& kData, unsigned int uiIndex) const;
    virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiColor& kData, unsigned int uiIndex);
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiPoint3& kData, unsigned int uiIndex) const;
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, bool& bData, unsigned int uiIndex) const;
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, bool bData, unsigned int uiIndex);
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiObject*& pkData, unsigned int uiIndex) const;
    virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiEntityInterface*& pkData, unsigned int uiIndex) const;
    virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, NiEntityInterface* pkData, unsigned int uiIndex);
	
	float GetInnerRadius() const { return m_fInnerRadius; }
	void SetInnerRadius( float fInnerRadius ) 
	{  
		if( GetInnerRadius() != fInnerRadius )
		{
			m_fInnerRadius = fInnerRadius;
			SetLightPropertiesChanged(true);
		}
	}

	float GetOuterRadius() const { return m_fOuterRadius; }
	void SetOuterRadius( float fOuterRadius ) 
	{
		if( GetOuterRadius() != fOuterRadius )
		{
			m_fOuterRadius = fOuterRadius; 
			SetLightPropertiesChanged(true);
		}
	}

	bool IsGlobal() const { return m_bGlobal; }
	void SetGlobal( bool bGlobal ) 
	{
		if( IsGlobal() != bGlobal )
		{
			m_bGlobal = bGlobal;
			SetLightPropertiesChanged(true);
		}
	}

	void SetDayNightColor(unsigned int uiColor, const CDayNightColor& kDayNightColor)
	{
		NIASSERT( uiColor < NUM_LIGHT_COLOR );

		m_akDayNightColors[uiColor].FromString( kDayNightColor.ToString() );
	}

	static const NiUniqueID& GetStaticTemplateID();
};

//---------------------------------------------------------------------------------------------------------------------------------------------------------
//
//

class TERRAIN_ENTRY CTerrainLightManager : public NiMemObject
{
private:
	NiTPrimitiveArray<CTerrainLight*>	m_akLightList;
	unsigned int						m_uiCurrentLight;
	NiColor								m_akColors[NUM_LIGHT_COLOR];

	bool								m_bDirty;

	NiTPrimitiveArray<NiEntityPropertyInterface*> m_akLightEntityList;

	bool m_bStopTime; //是否停止时间循环
	unsigned int m_uiHour; //当前时间
	unsigned int m_uiMinute; //当前时间

	unsigned int m_uiCycleType; //循环类型, 默认是1天

    unsigned int m_uiVersion;

    NiFogPropertyPtr m_spFogProperty;
    float m_fFogDensity;
	float m_fFogNear;
	float m_fFogFar;

    static bool ms_bFogEnable;
	static CTerrainLightManager* ms_TerrainLightManager;

public:
	CTerrainLightManager();
	~CTerrainLightManager();

	bool Load(const NiFixedString& kMapName);
	bool Save(const NiFixedString& kMapName);

	void ApplyFog(NiDX9RenderState* pkRenderState,const NiColor& kFogColor, float fDepth, float fStart = 0.0f, float fEnd = 0.0f);

	void ResetColors();

	bool HasLight() const { return m_akLightList.GetSize() > 0; }

	CTerrainLight* CreateLight(NiEntityPropertyInterface* pkEntity);
	void DeleteLight(NiEntityPropertyInterface* pkEntity);
	CTerrainLight* GetLight(const NiFixedString& kName);
	void ChangeLightName(const NiFixedString& kNewName, const NiFixedString& kOldName);

	bool AddLightEntity(NiEntityPropertyInterface* pkEntity);
	bool RemoveLightEntity(NiEntityPropertyInterface* pkEntity);

	const NiColor& GetAmbientColor() const { return m_akColors[AMBIENT_COLOR]; }
	const NiColor& GetDiffuseColor() const { return m_akColors[DIFFUSE_COLOR]; }
	const NiColor& GetWaterAmbientColor() const { return m_akColors[WATER_AMBIENT_COLOR]; }
	const NiColor& GetWaterDiffuseColor() const { return m_akColors[WATER_DIFFUSE_COLOR]; }
    const NiColor& GetFogColor() const { return m_akColors[FOG_COLOR]; }

	void SortLights();
	void CalcLightWeights(const NiPoint3& pos);
	void UpdateTime();
	void Update(const NiPoint3& pos);
	void UpdateLightEntities(NiEntityPropertyInterface* pkParentEntity, float fTime);

	bool IsDirty() const { return m_bDirty; }
	void SetDirty( bool bDirty ) { m_bDirty = bDirty; }

	void GetEntities(NiScene* pkScene);

	bool IsStopTime() const { return m_bStopTime; }
	void SetStopTime( bool bStop ) { m_bStopTime = bStop; }

	void SetTime(unsigned int uiHour, unsigned int uiMinute)
	{
		m_uiHour = uiHour;
		m_uiMinute = uiMinute;

		m_uiHour = NiClamp(m_uiHour, 0, 23);
		m_uiMinute = NiClamp(m_uiHour, 0, 59);
	}

	void GetTime(unsigned int &uiHour, unsigned int &uiMinute) const 
	{
		uiHour = m_uiHour;
		uiMinute = m_uiMinute;
	}

	void SetCycleType( unsigned int uiType ) { m_uiCycleType = uiType; }
	unsigned int GetCycleType() const { return m_uiCycleType; }

    unsigned int GetVersion() const { return m_uiVersion; }
    NiFogProperty* GetFogProperty() const { return m_spFogProperty; }

    float GetFogDensity() const { return m_fFogDensity; }

	void GetFogNearFar(float& fNear, float& fFar) 
	{
		fNear = m_fFogNear;
		fFar = m_fFogFar;
	}

    static bool SetFogEnable(bool bFogEnable);
    static bool GetFogEnable();
	static CTerrainLightManager* Get();
};

#endif //TERRAIN_LIGHT_H