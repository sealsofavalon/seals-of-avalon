
#pragma warning( push, 3 )

#include "NiMain.h"
#include "NiSystem.h"
#include "NiAccumulator.h"
#include "NiCamera.h"
#include "NiPath.h"
#include "NiNode.h"
#include "NiDX9Renderer.h"
#include "NiD3DShader.h"
#include "NiEntityStreaming.h"
#include "NiGeneralEntity.h"
#include "NiActorComponent.h"
#include "NiCameraComponent.h"
#include "NiInheritedTransformationComponent.h"
#include "NiLightComponent.h"
#include "NiSceneGraphComponent.h"
#include "NiShadowGeneratorComponent.h"
#include "NiTransformationComponent.h"
#include "NiGeneralComponent.h"
#include "NiFactories.h"

#include "NiDX9TextureData.h"
#include "NiPick.h"

#include "NiMemStream.h"

#include "NiFilename.h"
#include "NiExternalAssetParams.h"
#include "NiViewMath.h"
#include "NiD3DShaderProgramFactory.h"

#include "NiDefaultErrorHandler.h"
#include "NiDynamicGeometryGroup.h"
#include "NiUnsharedGeometryGroup.h"
#include "NiVBChip.h"


#include <vector>
#include <list>

#pragma warning( pop )

#pragma warning(disable : 4127) //conditional expression is constant
#pragma warning(disable : 4100) //unreferenced formal parameter
#pragma warning(disable : 4189) //local variable is initialized but not referenced
#pragma warning(disable : 4101)
#pragma warning(disable : 4251)
#pragma warning(disable : 4706)

