
#include "TerrainPCH.h"
#include "TerrainShader.h"
#include "TerrainHelper.h"
#include "TerrainLight.h"
#include "Map.h"
#include "DBFile\\DBManager.h"

CTerrainGeomData::CTerrainGeomData(unsigned short usVertices, NiPoint3* pkVertex,
        NiPoint3* pkNormal, NiColorA* pkColor, NiPoint2* pkTexture, 
        unsigned short usNumTextureSets, DataFlags eNBTMethod,
        unsigned short usTriangles, unsigned short* pusTriList)
		:NiTriShapeData(usVertices, pkVertex, pkNormal, pkColor, pkTexture, 
		usNumTextureSets, eNBTMethod, usTriangles, pusTriList)
{
}

CTerrainGeomData::~CTerrainGeomData()
{
	//地形共用以下数据, 防止被删除
	m_pusTriList = NULL;
}

void CTerrainGeomData::Replace(unsigned short usTriangles, unsigned short* pusTriList)
{
	if( m_pusTriList != pusTriList )
		m_pusTriList = NULL;

	NiTriShapeData::Replace(usTriangles, pusTriList);
}

NiSourceTexture* CAlphaTexture::Create(NiPixelData* pkPixelData)
{
	NIASSERT(pkPixelData);

	CAlphaTexture* pkTexture = NiNew CAlphaTexture;
    NIASSERT(pkTexture);

	pkTexture->SetStatic(false);

	pkTexture->m_kFormatPrefs.m_ePixelLayout = NiTexture::FormatPrefs::SINGLE_COLOR_8;
	pkTexture->m_kFormatPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::SMOOTH;
	pkTexture->m_kFormatPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
	pkTexture->m_spSrcPixelData = pkPixelData;
	
	pkTexture->CreateRendererData();

	return pkTexture;
}

bool CTerrainShader::ms_bShowShadow;
bool CTerrainShader::ms_bShowGridInfo;
int CTerrainShader::ms_iGridLevel;
bool CTerrainShader::ms_bShowWireframe;
bool CTerrainShader::ms_bShowAreaInfo;
NiShaderDeclaration* CTerrainShader::ms_pkDecl;

CTerrainShader::CTerrainShader()
{
	SetName("TerrainShader");

	// This is the best (and only) implementation of this shader
	m_bIsBestImplementation = true;

    m_spVertexShader = TerrainHelper::CreateVertexShader("Terrain.hlsl", "TerrainVS");
    
    m_pkPixelShaders[0] = TerrainHelper::CreatePixelShader("Terrain.hlsl", "TerrainPS0");
    m_pkPixelShaders[1] = TerrainHelper::CreatePixelShader("Terrain.hlsl", "TerrainPS1");
    m_pkPixelShaders[2] = TerrainHelper::CreatePixelShader("Terrain.hlsl", "TerrainPS2");
    m_pkPixelShaders[3] = TerrainHelper::CreatePixelShader("Terrain.hlsl", "TerrainPS3");
    m_pkPixelShaders[4] = TerrainHelper::CreatePixelShader("Terrain.hlsl", "TerrainPS4");
    m_pkPixelShaders[5] = TerrainHelper::CreatePixelShader("Terrain.hlsl", "TerrainPS5");

    m_spWireframePixelShader = TerrainHelper::CreatePixelShader("Terrain.hlsl", "WireframePS");
	m_uiCurrentPass = 0;
}

CTerrainShader::~CTerrainShader()
{
    NiDelete m_pkPixelShaders[0];
    NiDelete m_pkPixelShaders[1];
    NiDelete m_pkPixelShaders[2];
    NiDelete m_pkPixelShaders[3];
    NiDelete m_pkPixelShaders[4];
    NiDelete m_pkPixelShaders[5];
}

unsigned int CTerrainShader::PreProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    m_pkChunk = CMap::Get()->GetChunk(kWorld.m_Translate.x, kWorld.m_Translate.y);
    m_pkD3DRenderState->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	return 0;
}

unsigned int CTerrainShader::PostProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    if(m_bWireframe)
    {
        m_pkD3DRenderState->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
        m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
        m_pkD3DRenderState->SetRenderState(D3DRS_DEPTHBIAS, F2DW(0.f));
    }

	m_uiCurrentPass = 0;
    m_pkChunk = NULL;
    return 0;
}

unsigned int CTerrainShader::SetupTransformations(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        const NiSkinPartition::Partition* pkPartition, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects, 
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    if(m_uiCurrentPass == 0)
    {
        m_pkD3DRenderState->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
        m_pkD3DRenderState->SetRenderState(D3DRS_ZENABLE, TRUE);

        m_pkD3DRenderState->SetVertexShader(m_spVertexShader->GetShaderHandle());
        m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
        m_pkD3DRenderState->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
        m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
        m_pkD3DRenderState->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);

        m_pkD3DRenderer->SetModelTransform(kWorld);

        //Vertex Shader
        float fValues[12][4];
        D3DXMATRIX kD3DWorld = m_pkD3DRenderer->GetD3DMat();
        fValues[0][0] = kD3DWorld._41;
        fValues[0][1] = kD3DWorld._42;
        fValues[0][2] = kD3DWorld._43;
        fValues[0][3] = 0.f;

        D3DXMATRIX kTempMat;
        D3DXMATRIX kD3DView = m_pkD3DRenderer->GetD3DView();
        D3DXMATRIX kD3DProj = m_pkD3DRenderer->GetD3DProj();

        D3DXMatrixTranspose(&kTempMat, &kD3DView);
        memcpy(fValues[1], &kTempMat, sizeof(kTempMat));

        kTempMat = kD3DView * kD3DProj;
        D3DXMatrixTranspose(&kTempMat, &kTempMat);
        memcpy(fValues[5], &kTempMat, sizeof(kTempMat));

        CMap* pkMap = CMap::Get();
        NiLight* pkLight = pkMap->GetLight();

        const NiColor& kDiffuseColor = pkLight->GetDiffuseColor();

        fValues[9][0] = kDiffuseColor.r;
        fValues[9][1] = kDiffuseColor.g;
        fValues[9][2] = kDiffuseColor.b;
        fValues[9][3] = 1.f;

        const NiColor& kAmbientColor = pkLight->GetAmbientColor();
        fValues[10][0] = kAmbientColor.r;
        fValues[10][1] = kAmbientColor.g;
        fValues[10][2] = kAmbientColor.b;
        fValues[10][3] = 1.f;

		float fNear,fFar = 0.0f;

		CTerrainLightManager::Get()->GetFogNearFar(fNear, fFar);

        fValues[11][0] = CTerrainLightManager::Get()->GetFogDensity();
		fValues[11][1] = fNear;
		fValues[11][2] = fFar;
		fValues[11][3] = 1.f;

        m_pkD3DRenderState->SetVertexShaderConstantF(0, &fValues[0][0], 12);
    }

    if(m_bWireframe)
    {
        m_pkD3DRenderState->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
        m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
        m_pkD3DRenderState->SetRenderState(D3DRS_DEPTHBIAS, F2DW(-0.0001f));
        m_pkD3DRenderState->SetPixelShader(m_spWireframePixelShader->GetShaderHandle());

        float fValues[4];
        fValues[0] = m_kWireframeColor.r;
        fValues[1] = m_kWireframeColor.g;
        fValues[2] = m_kWireframeColor.b;
        fValues[3] = 1.f;

        m_pkD3DRenderState->SetPixelShaderConstantF(0, &fValues[0], 1);
    }
    else
    {
        unsigned int uiNumLayers = m_pkChunk->GetNumLayers();
        NIASSERT(uiNumLayers <= MAX_LAYERS && uiNumLayers > 0);
        m_pkD3DRenderState->SetPixelShader(m_pkPixelShaders[uiNumLayers - 1]->GetShaderHandle());

        float fValues[8][4];
        CTile* pkTile = m_pkChunk->GetTile();
        CTileTexture* pkTileTexture = pkTile->GetTexture(m_pkChunk->GetTextureId(0));

        NiDX9TextureManager* pkDX9TextureManager = m_pkD3DRenderer->GetTextureManager();
        bool bS0Mipmap;
        bool bNonPow2;
        bool bChanged; 
        D3DBaseTexturePtr pkD3DTexture = pkDX9TextureManager->PrepareTextureForRendering(pkTileTexture->pkTexture, bChanged, bS0Mipmap, bNonPow2);
        m_pkD3DRenderState->SetTexture(2, pkD3DTexture);
        m_pkD3DRenderState->SetSamplerState(2, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
        m_pkD3DRenderState->SetSamplerState(2, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
        m_pkD3DRenderState->SetSamplerState(2, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
        m_pkD3DRenderState->SetSamplerState(2, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
        m_pkD3DRenderState->SetSamplerState(2, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

        fValues[0][0] = pkTileTexture->fUScale;
        fValues[0][1] = pkTileTexture->fVScale;

        for(unsigned int ui = 1; ui < uiNumLayers; ++ui)
        {
            pkTileTexture = pkTile->GetTexture(m_pkChunk->GetTextureId(ui));
            pkD3DTexture = pkDX9TextureManager->PrepareTextureForRendering(pkTileTexture->pkTexture, bChanged, bS0Mipmap, bNonPow2);
            m_pkD3DRenderState->SetTexture(2*ui + 1, pkD3DTexture);
            fValues[ui][0] = pkTileTexture->fUScale;
            fValues[ui][1] = pkTileTexture->fVScale;
            m_pkD3DRenderState->SetSamplerState(2*ui + 1, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
            m_pkD3DRenderState->SetSamplerState(2*ui + 1, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
            m_pkD3DRenderState->SetSamplerState(2*ui + 1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
            m_pkD3DRenderState->SetSamplerState(2*ui + 1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
            m_pkD3DRenderState->SetSamplerState(2*ui + 1, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

            pkD3DTexture = pkDX9TextureManager->PrepareTextureForRendering(m_pkChunk->GetAlphaTexture(ui), bChanged, bS0Mipmap, bNonPow2);
            m_pkD3DRenderState->SetTexture(2*ui + 2, pkD3DTexture);
            m_pkD3DRenderState->SetSamplerState(2*ui + 2, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
            m_pkD3DRenderState->SetSamplerState(2*ui + 2, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
            m_pkD3DRenderState->SetSamplerState(2*ui + 2, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
            m_pkD3DRenderState->SetSamplerState(2*ui + 2, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
            m_pkD3DRenderState->SetSamplerState(2*ui + 2, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
        }

        BOOL bValues[4];
        if(ms_bShowShadow && m_pkChunk->GetShadowTexture())
        {
            pkD3DTexture = pkDX9TextureManager->PrepareTextureForRendering(m_pkChunk->GetShadowTexture(), bChanged, bS0Mipmap, bNonPow2);
            m_pkD3DRenderState->SetTexture(0, pkD3DTexture);
            m_pkD3DRenderState->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
            m_pkD3DRenderState->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
            m_pkD3DRenderState->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
            m_pkD3DRenderState->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
            m_pkD3DRenderState->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

            bValues[0] = TRUE;
        }
        else
        {
            bValues[0] = FALSE;
        }

        if(ms_bShowGridInfo && m_pkChunk->GetGridTexture(ms_iGridLevel))
        {
            pkD3DTexture = pkDX9TextureManager->PrepareTextureForRendering(m_pkChunk->GetGridTexture(ms_iGridLevel), bChanged, bS0Mipmap, bNonPow2);
            m_pkD3DRenderState->SetTexture(1, pkD3DTexture);
            m_pkD3DRenderState->SetSamplerState(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
            m_pkD3DRenderState->SetSamplerState(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
            m_pkD3DRenderState->SetSamplerState(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
            m_pkD3DRenderState->SetSamplerState(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
            m_pkD3DRenderState->SetSamplerState(1, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

            bValues[1] = TRUE;
        }
        else
        {
            bValues[1] = FALSE;
        }

		if(ms_bShowAreaInfo && m_pkChunk->GetAreaId() != 0)
		{
			bValues[2] = TRUE;

			unsigned int uiColor = g_pkAreaColorDB->GetColor(m_pkChunk->GetAreaId());
			fValues[6][0] = ((uiColor >> 16)&0xFF)/255.f;
			fValues[6][1] = ((uiColor >> 8)&0xFF)/255.f;
			fValues[6][2] = ((uiColor)&0xFF)/255.f;
			fValues[6][3] = 1.f;
		}
		else
		{
			bValues[2] = FALSE;
		}

        if(CTerrainLightManager::GetFogEnable())
        {
            bValues[3] = TRUE;

            //FogColor
            NiColor kFogColor = CTerrainLightManager::Get()->GetFogColor();
            fValues[7][0] = kFogColor.r;
            fValues[7][1] = kFogColor.g;
            fValues[7][2] = kFogColor.b;
            fValues[7][3] = 1.f;
        }
        else
        {
            bValues[3] = FALSE;
        }

        m_pkD3DRenderState->SetPixelShaderConstantF(0, &fValues[0][0], 8);
        m_pkD3DRenderState->SetPixelShaderConstantB(0, bValues, 4);
    }

    return 0;
}

NiGeometryData::RendererData* CTerrainShader::PrepareGeometryForRendering(
    NiGeometry* pkGeometry, const NiSkinPartition::Partition* pkPartition,
    NiGeometryData::RendererData* pkRendererData, 
    const NiPropertyState* pkState)
{
    NiGeometryData* pkData = pkGeometry->GetModelData();
    NiGeometryBufferData* pkBuffData = (NiGeometryBufferData*)pkData->GetRendererData();

	if(m_uiCurrentPass == 0)
    {
        if(ms_pkDecl == NULL)
        {
            ms_pkDecl = m_pkD3DRenderer->CreateShaderDeclaration(1, 2);
            ms_pkDecl->IncRefCount();

            ms_pkDecl->SetEntry(0, NiShaderDeclaration::SHADERPARAM_NI_POSITION, NiShaderDeclaration::SPTYPE_FLOAT3, 0);
            ms_pkDecl->SetEntry(0, NiShaderDeclaration::SHADERPARAM_NI_NORMAL, NiShaderDeclaration::SPTYPE_FLOAT3, 1);
        }

        m_pkD3DRenderer->PackGeometryBuffer(pkBuffData, pkData, NULL, (NiD3DShaderDeclaration*)ms_pkDecl);
    }

    NIASSERT(pkBuffData->GetStreamCount() == 2);

    m_pkD3DDevice->SetStreamSource(0, pkBuffData->GetVBChip(0)->GetVB(), 0, sizeof(NiPoint3));
    m_pkD3DDevice->SetStreamSource(1, pkBuffData->GetVBChip(1)->GetVB(), 0, sizeof(NiPoint3));

    m_pkD3DDevice->SetIndices(pkBuffData->GetIB());
    m_pkD3DRenderState->SetDeclaration(((NiD3DShaderDeclaration*)ms_pkDecl)->GetD3DDeclaration());

    return pkBuffData;
}

unsigned int CTerrainShader::FirstPass()
{
    m_uiCurrentPass = 0;

    if(m_pkChunk->GetNumLayers() == 0)
    {
        m_bWireframe = true;
        m_uiPassCount = 1;

        m_kWireframeColor = NiColor::WHITE;
    }
    else if(ms_bShowWireframe)
    {
        m_bWireframe = false;
        m_uiPassCount = 2;

        if( (m_pkChunk->GetX() + m_pkChunk->GetY()) & 0x01 )
        {
            m_kWireframeColor.r = 0.f;
            m_kWireframeColor.g = 1.f;
            m_kWireframeColor.b = 0.f;
        }
        else
        {
            m_kWireframeColor.r = 0.f;
            m_kWireframeColor.g = 0.f;
            m_kWireframeColor.b = 1.f;        
        }
    }
    else
    {
        m_bWireframe = false;
        m_uiPassCount = 1;
    }

    return m_uiPassCount;
}

unsigned int CTerrainShader::NextPass()
{
    m_uiCurrentPass++;
    unsigned int uiRemainingPasses = m_uiPassCount - m_uiCurrentPass;

    //第二个Pass是Wireframe
    if(uiRemainingPasses > 0)
        m_bWireframe = true;

    return uiRemainingPasses;
}

bool CTerrainShader::ShowShadow(bool bShowShadow)
{
	bool bChange = (ms_bShowShadow != bShowShadow);
	ms_bShowShadow = bShowShadow;
	return bChange;
}

bool CTerrainShader::ShowGridInfo(bool bShowGridInfo)
{
	bool bChange = (ms_bShowGridInfo != bShowGridInfo);
	ms_bShowGridInfo = bShowGridInfo;
	return bChange;
}

bool CTerrainShader::SetGridLevel(int nLv)
{
	bool bChange = (ms_iGridLevel != nLv);
	ms_iGridLevel = nLv;
	return bChange;
}

bool CTerrainShader::ShowWireframe(bool bShowWireframe)
{
	bool bChange = (ms_bShowWireframe != bShowWireframe);
	ms_bShowWireframe = bShowWireframe;
	return bChange;
}

bool CTerrainShader::ShowAreaInfo(bool bShowAreaInfo)
{
	if(BOOL_EQUATION(ms_bShowAreaInfo, bShowAreaInfo))
		return false;

	ms_bShowAreaInfo = bShowAreaInfo;
	return true;
}

void CTerrainShader::_SDMInit()
{
	ms_bShowShadow = true;
	ms_bShowGridInfo = false;
	ms_bShowWireframe = false;
	ms_bShowAreaInfo = false;

    ms_pkDecl = NULL;
}

void CTerrainShader::_SDMShutdown()
{
    if(ms_pkDecl)
    {
        ms_pkDecl->DecRefCount();
        ms_pkDecl = NULL;
    }
}
