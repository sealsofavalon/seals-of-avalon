#ifndef TERRAIN_SHADER_H
#define TERRAIN_SHADER_H

#include "TerrainLibType.h"
#include "Chunk.h"

class TERRAIN_ENTRY CTerrainGeomData : public NiTriShapeData
{
public:
	CTerrainGeomData(unsigned short usVertices, NiPoint3* pkVertex,
        NiPoint3* pkNormal, NiColorA* pkColor, NiPoint2* pkTexture, 
        unsigned short usNumTextureSets, DataFlags eNBTMethod,
        unsigned short usTriangles, unsigned short* pusTriList);

	virtual ~CTerrainGeomData();

	void Replace(unsigned short usTriangles, unsigned short* pusTriList);
};

class TERRAIN_ENTRY CAlphaTexture : public NiSourceTexture
{
public:
	static NiSourceTexture* Create(NiPixelData* pkPixelData);
};

class TERRAIN_ENTRY CTerrainShader : public NiD3DShaderInterface
{
public:
	CTerrainShader();
	virtual ~CTerrainShader();
	
	virtual unsigned int PreProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound);

	virtual unsigned int PostProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound);

    virtual unsigned int SetupTransformations(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        const NiSkinPartition::Partition* pkPartition, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects, 
        const NiTransform& kWorld, const NiBound& kWorldBound);

    virtual NiGeometryData::RendererData* PrepareGeometryForRendering(
        NiGeometry* pkGeometry, const NiSkinPartition::Partition* pkPartition,
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState);

    // Advance to the next pass.
    virtual unsigned int FirstPass();
    virtual unsigned int NextPass();

	static bool ShowShadow(bool bShowShadow);
	static bool ShowGridInfo(bool bShowGridInfo);
	static bool SetGridLevel(int nLv);
	static bool ShowWireframe(bool bShowWireframe);
	static bool GetShowWireframe() { return ms_bShowWireframe; }
	static bool ShowAreaInfo(bool bShaowAreaInfo);

 	static void _SDMInit();
	static void _SDMShutdown();

private:
	static bool	ms_bShowShadow;
	static bool	ms_bShowGridInfo;
	static bool	ms_bShowWireframe;
	static bool ms_bShowAreaInfo;
	static int  ms_iGridLevel;

    static NiShaderDeclaration* ms_pkDecl;

    NiD3DVertexShaderPtr m_spVertexShader;
    NiD3DPixelShader* m_pkPixelShaders[MAX_LAYERS];
    NiD3DPixelShaderPtr m_spWireframePixelShader;
    CChunk* m_pkChunk; //������Chunk

    bool m_bWireframe;
    NiColor m_kWireframeColor;
    unsigned int m_uiPassCount;
    unsigned int m_uiCurrentPass;
};

#endif //TERRAIN_SHADER_H

