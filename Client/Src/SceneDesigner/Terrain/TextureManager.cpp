
#include "TerrainPCH.h"
#include "TextureManager.h"

CTextureManager* CTextureManager::ms_This;

NiPixelData* CTextureManager::Load(const char* pcFilename)
{
	char acFilename[1024];
	NiStrcpy(acFilename, sizeof(acFilename), pcFilename);
	_strupr_s(acFilename, sizeof(acFilename));
	NiPath::Standardize(acFilename);

	NiPixelData* pkPixelData;
	if (m_kTextureMap.GetAt(acFilename, pkPixelData))
	{
		if (pkPixelData)
		{
			return pkPixelData;
		}
	}

	pkPixelData = NiImageConverter::GetImageConverter()->ReadImageFile(acFilename, NULL);
	if( pkPixelData )
	{
		pkPixelData->IncRefCount();
		m_kTextureMap.SetAt(acFilename, pkPixelData);
	}

	return pkPixelData;
}

void CTextureManager::UnloadAll()
{
	NiTMapIterator kIter = m_kTextureMap.GetFirstPos();
    while (kIter)
    {
        NiPixelData* pkPixelData;
		const char* pcFilename;
        m_kTextureMap.GetNext(kIter, pcFilename, pkPixelData);

		if( pkPixelData )
			pkPixelData->DecRefCount();
    }

	m_kTextureMap.RemoveAll();
}

void CTextureManager::UnloadAllUnused()
{
	NiTMapIterator kIter = m_kTextureMap.GetFirstPos();
    while (kIter)
    {
        NiPixelData* pkPixelData;
		const char* pcFilename;
        m_kTextureMap.GetNext(kIter, pcFilename, pkPixelData);

		if( pkPixelData && pkPixelData->GetRefCount() == 1 )
		{
			pkPixelData->DecRefCount();
			m_kTextureMap.RemoveAt(pcFilename);
		}
    }
}

void CTextureManager::_SDMInit()
{
	ms_This = NiNew CTextureManager;
}

void CTextureManager::_SDMShutdown()
{
	ms_This->UnloadAll();
	NiDelete ms_This;
	ms_This = NULL;
}
