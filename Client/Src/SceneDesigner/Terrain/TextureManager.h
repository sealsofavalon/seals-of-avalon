#pragma once

#include "TerrainLibType.h"

class TERRAIN_ENTRY CTextureManager : public NiMemObject
{
public:
	NiPixelData* Load(const char* pcFilename);
	void UnloadAllUnused();
	void UnloadAll();

	static CTextureManager* Get() { return ms_This; }

	static void _SDMInit();
	static void _SDMShutdown();

private:
	NiTStringMap<NiPixelData*> m_kTextureMap;
	static CTextureManager* ms_This;
};