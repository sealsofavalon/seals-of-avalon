#include "TerrainPCH.h"
#include "TagStream.h"
#include "TerrainHelper.h"

#include "Chunk.h"
#include "Tile.h"
#include "Map.h"
#include "EditorInfoComponent.h"
#include "DepthStencilBufferF32.h"
#include "DBFile/CreatureSpawnsDB.h"
#include "DBFile/WayPointsDb.h"
#include "DBFile/GameObjectsDB.h"
#include "NpcComponent.h"
#include "Collision.h"
#include "TerrainShader.h"
#include "EntityInfoComponent.h"


#include <string.h>
#include "crc32.h"


CTileTexture::CTileTexture()
:pkTexture(NULL)
,fUScale(1.f)
,fVScale(1.f)
{
}

CTileTexture::~CTileTexture()
{
	if( pkTexture )
		pkTexture->DecRefCount();
}

NiStencilPropertyPtr CTile::m_spStencilProperty;
NiFixedString CTile::ms_kSceneRootPointerName;
bool CTile::ms_bEditNpc;

NiImplementRTTI(CTile, NiNode);

CTile::CTile(int x, int y)
:m_iX(x)
,m_iY(y)
,m_bDirty(false)
,m_uiVersion(0)
{
	for( int x = 0; x < CHUNK_COUNT; x++ )
	{
		for( int y = 0; y < CHUNK_COUNT; y++ )
		{
			m_kChunks[x][y].Set(x, y, this);
		}
	}
}

CTile::~CTile()
{
	for( unsigned int i = 0; i < m_pkTextures.GetSize(); i++ )
	{
		NiDelete m_pkTextures.GetAt(i);
	}

	for( unsigned int i = 0; i < m_pkModels.GetSize(); i++ )
	{
		if( m_pkModels.GetAt(i) )
			m_pkModels.GetAt(i)->RemoveReference();
	}

	for( unsigned int i = 0; i < m_pkNpcEntities.GetSize(); i++ )
	{
		if( m_pkNpcEntities.GetAt(i) )
			m_pkNpcEntities.GetAt(i)->RemoveReference();
	}

    for( unsigned int i = 0; i < m_pkGameObjectEntities.GetSize(); i++ )
    {
        if(m_pkGameObjectEntities.GetAt(i))
            m_pkGameObjectEntities.GetAt(i)->RemoveReference();
    }
}

bool CTile::Load(const NiFixedString& kMapName)
{
	char acFilename[NI_MAX_PATH];
	//合成绝对路径
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\World\\Maps\\%s\\%s_%d_%d.tle",  TerrainHelper::GetClientPath(), (const char*)kMapName, (const char*)kMapName, m_iX, m_iY);

	m_strTitlePath = acFilename;
	m_strMapName = kMapName;

	NiFile* pkFile = NiFile::GetFile(acFilename, NiFile::READ_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;

		//创建一个空的
		OnCreate();

		return false;
	}

	CTagStream kTagStream;

	while( kTagStream.ReadFromStream( pkFile ) )
	{
		switch( kTagStream.GetTag() )
		{
		case 'VERS':
			kTagStream.Read(&m_uiVersion, sizeof(m_uiVersion));
			kTagStream.SetVersion(m_uiVersion);
			break;

		case 'TEXS'://贴图列表
			{
				NiFixedString kTex;
				while( kTagStream.ReadFixedString( kTex ) )
				{
					float fUScale = 1.f;
					float fVScale = 1.f;
					kTagStream.Read(&fUScale, sizeof(float));
					kTagStream.Read(&fVScale, sizeof(float));
					AddTexture( kTex, fUScale, fVScale );
				}
			}
			break;

		case 'CHNK'://chunks
			{
				int x, y;
				kTagStream.Read(&x, sizeof(int));
				kTagStream.Read(&y, sizeof(int));

				NIASSERT( x >= 0 && x < CHUNK_COUNT );
				NIASSERT( y >= 0 && y < CHUNK_COUNT );

				m_kChunks[x][y].Load( &kTagStream );
			}
			break;

		case 'MODL': //物件模型
			{
				NiExternalAssetManager* pkAssetManager = CMap::GetAssetManager();
				NiEntityErrorInterface* pkErrors = const_cast<NiEntityErrorInterface *>(CMap::Get()->GetErrorHandler());

				NiEntityInterface* pkEntity;
				NiPoint3 kPos;
				CChunk* pkChunk;
				kTagStream.SetBase(float(m_iX*TILE_SIZE), float(m_iY*TILE_SIZE));
				while( (pkEntity = kTagStream.ReadEntity()) != NULL )
				{
					pkEntity->Update(NULL, 0.f, pkErrors, pkAssetManager);
					CreateCollisionData(pkEntity);
                    TerrainHelper::AttachFogProperty(pkEntity);
					AddEntity(pkEntity);
				}
			}
			break;

		default:
			{
				NIASSERT( false );
			}
			break;
		}
	}

	NiDelete pkFile;

	LoadNpc();
    LoadGameObjects();

	m_bDirty = false;
	return true;
}

bool CTile::Save(const NiFixedString& kMapName)
{
	if( !m_bDirty )
		return true;

	NiMemStream kMemStream;
	CTagStream kTagStream;

	//版本号
	{
		kTagStream.Reset();
		kTagStream.SetTag('VERS');

		unsigned int uiVersion = MakeVersion(1, 0);
		kTagStream.Write(&uiVersion, sizeof(uiVersion));
		kTagStream.WriteToStream( &kMemStream );
	}

	//地表贴图列表
	if( m_pkTextures.GetSize() > 0 )
	{
		kTagStream.Reset();
		kTagStream.SetTag('TEXS');

		for( unsigned int i = 0; i < m_pkTextures.GetSize(); i++ )
		{
			kTagStream.WriteFixedString(m_pkTextures.GetAt(i)->kName);
			kTagStream.Write(&m_pkTextures.GetAt(i)->fUScale, sizeof(float));
			kTagStream.Write(&m_pkTextures.GetAt(i)->fVScale, sizeof(float));
		}

		kTagStream.WriteToStream( &kMemStream );
	}

	//地块
	{
		//地形是按照坐标Y优先存储的
		//所以内循环是Y坐标
		for( int x = 0; x < CHUNK_COUNT; x++ )
		{
			for( int y = 0; y < CHUNK_COUNT; y++ )
			{
				kTagStream.Reset();
				kTagStream.SetTag('CHNK');

				kTagStream.Write(&x, sizeof(int));
				kTagStream.Write(&y, sizeof(int));

				m_kChunks[x][y].Save( &kTagStream );

				kTagStream.WriteToStream( &kMemStream );
			}
		}
	}


	//模型列表
	if( m_pkModels.GetSize() > 0 )
	{
		kTagStream.Reset();
		kTagStream.SetTag('MODL');

		kTagStream.SetBase(float(m_iX*TILE_SIZE), float(m_iY*TILE_SIZE));

		for( unsigned int i = 0; i < m_pkModels.GetSize(); i++ )
		{
			kTagStream.WriteEntity(m_pkModels.GetAt(i));
		}

		kTagStream.WriteToStream( &kMemStream );
	}

	//为了防止保存文件的时候出错, 先把文件写到内存, 然后再把内存写到文件

	char acFilename[NI_MAX_PATH];
	//合成绝对路径
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Data\\World\\Maps\\%s\\%s_%d_%d.tle",  TerrainHelper::GetClientPath(), (const char*)kMapName, (const char*)kMapName, m_iX, m_iY); 

	NiFile* pkFile = NiFile::GetFile(acFilename, NiFile::WRITE_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return false;
	}

	m_bDirty = false;

	pkFile->Write(kMemStream.Str(), kMemStream.GetSize());
	kMemStream.Freeze(false);


	NiDelete pkFile;

	//SaveServerTile(kMapName);
	SaveNpc();
    SaveGameObjects();

	SaveVMap();

	return true;
}

bool CTile::SaveServerTile(const NiFixedString& kMapName)
{
	char acFilename[NI_MAX_PATH];
	//合成绝对路径
	NiSprintf(acFilename, NI_MAX_PATH, "%s\\Envir\\Map\\%s\\%s_%d_%d.sle",  TerrainHelper::GetServerPath(), (const char*)kMapName, (const char*)kMapName, m_iX, m_iY); 

	NiFile* pkFile = NiFile::GetFile(acFilename, NiFile::WRITE_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return false;
	}

	CTagStream kTagStream;
	kTagStream.Reset();
	kTagStream.SetTag('STLE');
	kTagStream.Write(&m_iX, sizeof(int));
	kTagStream.Write(&m_iY, sizeof(int));
	kTagStream.WriteToStream( pkFile );

	//存储小区块
	{
		//地形是按照坐标Y优先存储的
		//所以内循环是Y坐标
		for( int x = 0; x < CHUNK_COUNT; x++ )
		{
			for( int y = 0; y < CHUNK_COUNT; y++ )
			{
				kTagStream.Reset();
				kTagStream.SetTag('GRID');
				m_kChunks[x][y].WriteGrid( kTagStream , WalkGrid);
				kTagStream.WriteToStream( pkFile );


				kTagStream.Reset();
				kTagStream.SetTag('FYGD');
				m_kChunks[x][y].WriteGrid( kTagStream , FlyGrid);
				kTagStream.WriteToStream( pkFile );
			}
		}
	}

	NiDelete pkFile;
	return true;
}

bool CTile::LoadNpc()
{
	if( !IsEditNpc() )
		return false;

	//npc
	CCreatureSpawnsDB kCreatureSpawnsDB;
	kCreatureSpawnsDB.Load(this);

	//way points
	CWayPointsDB kWayPointsDB;
	kWayPointsDB.Load(this);

	return true;
}


void CTile::SaveVMap()
{
	//ModelContainer *modelContainer = 0;
	//CMap* pkMap = CMap::Get();
	//unsigned int uiFrameCount = pkMap->GetFrameCount();
	//uiFrameCount++;

 //   Vector3 basepos = Vector3(0,0,0);
 //   AABSPTree<SubModel *>* mainTree = new AABSPTree<SubModel *>();


	//for( int x = 0; x < CHUNK_COUNT; x++ )
	//{
	//	for( int y = 0; y < CHUNK_COUNT; y++ )
	//	{
	//		ModelList kModleList = m_kChunks[x][y].GetModelList();

	//		// Scene Object
	//		NiEntityInterface* pkEntity;
	//		NiObject* pkSceneRootPointer;
	//		NiAVObject* pkSceneRoot;
	//		NiExternalAssetManager* pkAssetManager = CMap::GetAssetManager();


	//		CEntityInfoComponent* pkEntityInfoComponent;
	//		for( ModelList::const_iterator itr = kModleList.begin(); itr != kModleList.end(); ++itr )
	//		{
	//			pkEntity = GetEntity(itr->uiIndex);
	//			if( !pkEntity || pkEntity->GetHidden() )
	//				continue;

	//			pkEntityInfoComponent = (CEntityInfoComponent*)pkEntity->GetComponentByTemplateID(CEntityInfoComponent::ms_kTemplateID);
	//			if( pkEntityInfoComponent == NULL )
	//				continue;

	//			//已经加入了VisibleArray
	//			if( pkEntityInfoComponent->GetFrameCount() == uiFrameCount )
	//				continue;

	//			pkEntityInfoComponent->SetFrameCount(uiFrameCount);
	//			pkEntity->Update(NULL, pkMap->GetTime(), pkMap->GetErrorHandler(), pkAssetManager);

	//			pkSceneRootPointer = NULL;
	//			pkSceneRoot = NULL;
	//			if (pkEntity->GetPropertyData(ms_kSceneRootPointerName, pkSceneRootPointer))
	//			{
	//				pkSceneRoot = NiDynamicCast(NiAVObject, pkSceneRootPointer);
	//				if(pkSceneRoot)
	//				{
	//					fillModelIntoTree(mainTree, basepos, pkSceneRoot);
	//				}
	//			}
	//		}//
	//	}//for
	//}//for

	//if( mainTree->size() > 0)
 //   {
 //       mainTree->balance();
 //       modelContainer = new ModelContainer(mainTree);



	//	char cVMapFileName[NI_MAX_PATH];
	//	char cVdirFileName[NI_MAX_PATH];
	//	char cVMapName[NI_MAX_PATH];
	//	unsigned int n = crc32( (const unsigned char*)m_strMapName.c_str(), m_strMapName.length());

	//	NiSprintf(cVMapFileName, NI_MAX_PATH, "E:\\ServerVmaps\\Map\\%u_%d_%d.vmap", n, m_iX, m_iY); 
	//	NiSprintf(cVMapName, NI_MAX_PATH, "%u_%d_%d.vmap", n, m_iX, m_iY); 
	//	NiSprintf(cVdirFileName, NI_MAX_PATH, "E:\\ServerVmaps\\Map\\%u_%d_%d.vmdir", n, m_iX, m_iY); 


	//	//std::string strVMapName = "E:\\G3D-7.00-src\\G3D\\data-files\\vmaps\\000_26_32.vmap";

	//	//int pos = strVMapName.find(".tle");
	//	//strVMapName.erase(pos);
	//	//strVMapName += ".vmp";

 //       modelContainer->writeFile( cVMapFileName );


	//	FILE *wf =fopen(cVdirFileName,"w");

	//	fprintf(wf, "%s", cVMapName);

	//	fclose(wf);
 //   }
}

void CTile::SaveNpc()
{
	if( !IsEditNpc() )
		return;

	CCreatureSpawnsDB kCreatureSpawnsDB;
	kCreatureSpawnsDB.Save(this);

	CWayPointsDB kWayPointsDB;
	kWayPointsDB.Save(this);
}

bool CTile::LoadGameObjects()
{
	if( !IsEditNpc() )
		return false;

    CGameObjectsDB kGameObjectsDB;
    kGameObjectsDB.Load(this);

    return true;
}

void CTile::SaveGameObjects()
{
	if( !IsEditNpc() )
		return;

    CGameObjectsDB kGameObjectsDB;
    kGameObjectsDB.Save(this);
}

//被创建的时候调用, 用来初始化数据
void CTile::OnCreate()
{
	for( int x = 0; x < CHUNK_COUNT; x++ )
	{
		for( int y = 0; y < CHUNK_COUNT; y++ )
		{
			m_kChunks[x][y].Set(x, y, this);
			m_kChunks[x][y].OnCreate();
		}
	}

	m_bDirty = true;
}

CChunk* CTile::GetChunk(float x, float y)
{
	int iX = int(x / CHUNK_SIZE) - m_iX*CHUNK_COUNT;
	int iY = int(y / CHUNK_SIZE) - m_iY*CHUNK_COUNT;

	if( iX < 0  
		|| iX >= CHUNK_COUNT 
		|| iY < 0  
		|| iY >= CHUNK_COUNT)
	{
		return NULL;
	}

	return &m_kChunks[iX][iY];
}

CChunk* CTile::GetChunkByIndex(int iX, int iY)
{
	if( iX < 0  
		|| iX >= CHUNK_COUNT 
		|| iY < 0  
		|| iY >= CHUNK_COUNT)
	{
		return NULL;
	}

	return &m_kChunks[iX][iY];
}

float Length2Scale( int iLen )
{
	if( iLen >= 2048 )
		return 0.125f;
	if( iLen >= 1024 )
		return 0.25f;
	if( iLen >= 512 )
		return 0.5f;
	if( iLen >= 256 )
		return 1.f;
	if( iLen >= 128 )
		return 2.f;
	if( iLen >= 64 )
		return 4.f;
	if( iLen >= 32 )
		return 8.f;

	return 1.f;
}

//获取Texture, 不存在就用路径名生成一个
CTileTexture* CTile::GetTexture(unsigned int uiId)
{
	NIASSERT( uiId < m_pkTextures.GetSize() );

	CTileTexture* pkTileTexture = m_pkTextures.GetAt(uiId);
	NIASSERT(pkTileTexture);

	if( pkTileTexture->pkTexture == NULL ) //不存在, 创建一个
	{
		//todo... add to resource manager
		char acAbsPathName[1024];
		NiSprintf(acAbsPathName, 1024, "%s\\%s", TerrainHelper::GetClientPath(), (const char*)pkTileTexture->kName);
		pkTileTexture->pkTexture = NiSourceTexture::Create(acAbsPathName);
		pkTileTexture->pkTexture->IncRefCount();

		if(pkTileTexture->fUScale == 0.f || pkTileTexture->fVScale == 0.f)
		{
			pkTileTexture->fUScale = Length2Scale( pkTileTexture->pkTexture->GetWidth() );  
			pkTileTexture->fVScale = Length2Scale( pkTileTexture->pkTexture->GetHeight() );
		}
	}

	return pkTileTexture;
}

CTileTexture* CTile::GetTexture(const char* pcTextureName)
{
	for( unsigned int i = 0; i < m_pkTextures.GetSize(); i++ )
	{
		if( m_pkTextures.GetAt(i)->kName.EqualsNoCase(pcTextureName) )
		{
			return m_pkTextures.GetAt(i);
		}
	}

	return NULL;
}

unsigned int CTile::GetTextureIndex(const char* pcTextureName)
{
    for( unsigned int i = 0; i < m_pkTextures.GetSize(); i++ )
    {
        if( m_pkTextures.GetAt(i)->kName.EqualsNoCase(pcTextureName) )
            return i;
    }

    return INVALID_INDEX;
}

//增加Texture
unsigned int CTile::AddTexture(const char* pcTextureName, float fUScale, float fVScale)
{
	for( unsigned int i = 0; i < m_pkTextures.GetSize(); i++ )
	{
		if( m_pkTextures.GetAt(i)->kName.EqualsNoCase(pcTextureName) )
			return i;
	}

	m_bDirty = true;

	CTileTexture* pkTileTexture = NiNew CTileTexture;
	pkTileTexture->kName = pcTextureName;
	pkTileTexture->fUScale = fUScale;
	pkTileTexture->fVScale = fVScale;

	m_pkTextures.Add(pkTileTexture);
	return m_pkTextures.GetSize() - 1;
}

//删除Texture, 路径名也一起删除
//通知Chunk更新Texture索引
void CTile::DeleteTexture(const char* pcTextureName)
{
	for( unsigned int i = 0; i < m_pkTextures.GetSize(); i++ )
	{
		if( m_pkTextures.GetAt(i)->kName.EqualsNoCase(pcTextureName) )
		{
			m_bDirty = true;

			NiDelete m_pkTextures.GetAt(i);

			//m_pkTextures.RemoveAt(i);
			//从数组里面删除, 后面的向前移动
			for( unsigned int j = i; j + 1 < m_pkTextures.GetSize(); j++ )
			{
				m_pkTextures.SetAt(j, m_pkTextures.GetAt(j + 1));
			}
			m_pkTextures.SetSize(m_pkTextures.GetSize() - 1);

			for( int x = 0; x < CHUNK_COUNT; x++ )
			{
				for( int y = 0; y < CHUNK_COUNT; y++ )
				{
					m_kChunks[x][y].OnDeleteTexture(i);
				}
			}

			break;
		}
	}
}

//替换贴图
void CTile::ChangeTexture(const char* pcSrcTextureName, const char* pcDestTextureName)
{
	for( unsigned int i = 0; i < m_pkTextures.GetSize(); i++ )
	{
		if( m_pkTextures.GetAt(i)->kName.EqualsNoCase(pcSrcTextureName) )
		{
			m_bDirty = true;

			if( m_pkTextures.GetAt(i)->pkTexture )
			{
				m_pkTextures.GetAt(i)->pkTexture->DecRefCount();
				m_pkTextures.GetAt(i)->pkTexture = NULL;
			}

			m_pkTextures.GetAt(i)->kName = pcDestTextureName;
			break;
		}
	}
}

void CTile::SetTextureUVScale(const char* pcTextureName, float fUScale, float fVScale)
{
	CTileTexture* pkTileTexture = GetTexture(pcTextureName);
	for( unsigned int i = 0; i < m_pkTextures.GetSize(); i++ )
	{
		if( m_pkTextures.GetAt(i)->kName.EqualsNoCase(pcTextureName) )
		{
			m_bDirty = true;
			m_pkTextures.GetAt(i)->fUScale = fUScale;
			m_pkTextures.GetAt(i)->fVScale = fVScale;
			return;
		}
	}
}

bool CTile::GetTextureUVScale(const char* pcTextureName, float& fUScale, float& fVScale)
{
	CTileTexture* pkTileTexture = GetTexture(pcTextureName);
	if( pkTileTexture )
	{
		m_bDirty = true;
		fUScale = pkTileTexture->fUScale;
		fVScale = pkTileTexture->fVScale;

		return true;
	}

	return false;
}

void CTile::GetTextureNames(NiTObjectArray<NiFixedString>& akTextureNames) const
{
	akTextureNames.SetSize( m_pkTextures.GetSize() );
	for( unsigned int i = 0; i < m_pkTextures.GetSize(); i++ )
		akTextureNames.SetAt(i, m_pkTextures.GetAt(i)->kName);
}

void CTile::BuidWaterEdgeMap()
{
	m_bDirty = true;
	//chunks
	for( int x = 0; x < CHUNK_COUNT; x++ )
	{
		for( int y = 0; y < CHUNK_COUNT; y++ )
		{
			m_kChunks[x][y].BuidWaterEdgeMap();
		}
	}
}

void CTile::GetEntities(NiScene *pkScene)
{
	for( unsigned int i = 0; i < m_pkModels.GetSize(); i++ )
	{
        if( TerrainHelper::MakeUniqueName(pkScene, m_pkModels.GetAt(i)) )
		{
			pkScene->AddEntity(m_pkModels.GetAt(i));
		}
	}

	NiEntityInterface* pkEntity;
	CNpcComponent* pkNpcComponent;
	for( unsigned int i = 0; i < m_pkNpcEntities.GetSize(); i++ )
	{
		pkEntity = m_pkNpcEntities.GetAt(i);
		if( TerrainHelper::MakeUniqueName(pkScene, pkEntity) )
		{
			//增加NPC
			pkScene->AddEntity(pkEntity);

			pkNpcComponent = (CNpcComponent*)pkEntity->GetComponentByTemplateID(CNpcComponent::ms_kTemplateID);

			//增加NPC的路点
			for(unsigned int j = 0; j < pkNpcComponent->GetWayPointCount(); ++j)
			{
				pkEntity = pkNpcComponent->GetWayPoint(j);
				if(pkEntity)
					pkScene->AddEntity(pkEntity);
			}
		}
	}

    //Game Objects
    for(unsigned int i = 0; i < m_pkGameObjectEntities.GetSize(); i++)
    {
        pkEntity = m_pkGameObjectEntities.GetAt(i);
        if( TerrainHelper::MakeUniqueName(pkScene, pkEntity) )
        {
            pkScene->AddEntity(pkEntity);
        }
    }

    //chunks
    for( int x = 0; x < CHUNK_COUNT; x++ )
    {
        for( int y = 0; y < CHUNK_COUNT; y++ )
        {
            m_kChunks[x][y].GetEntities(pkScene);
        }
    }
}

void CTile::AddEntity(NiEntityInterface* pkEntity)
{
	//检查重复添加
	for( unsigned int i = 0; i < m_pkModels.GetSize(); i++ )
	{
		if( pkEntity == m_pkModels.GetAt(i) )
			return;
	}

	pkEntity->AddReference();
	m_pkModels.Add(pkEntity);

	m_bDirty = true;
}

void CTile::RemoveEntity(NiEntityInterface* pkInEntity)
{
	CMap* pkMap = CMap::Get();
	NiPoint2 kData;
	NiEntityInterface* pkEntity;
	bool bFind = false;
	CEditorInfoComponent* pkEditorInfoComponent;
	CChunk* pkChunk;
	unsigned int uiCount;

	for( unsigned int i = 0; i < m_pkModels.GetSize(); i++ )
	{
		if( !bFind && m_pkModels.GetAt(i) == pkInEntity )
		{
			//Remove Chunk Entity
			uiCount = 0;
			pkEditorInfoComponent = (CEditorInfoComponent *)pkInEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
			pkEditorInfoComponent->GetElementCount(CEditorInfoComponent::ms_kQuadTreeChunksName, uiCount);
			for( unsigned int ui = 0; ui < uiCount; ++ui )
			{
				pkEditorInfoComponent->GetPropertyData(CEditorInfoComponent::ms_kQuadTreeChunksName, kData, ui);
				pkChunk = pkMap->GetChunk(kData.x, kData.y);
				if( pkChunk )
					pkChunk->RemoveEntity(GetX(), GetY(), i);
			}

			NIASSERT( bFind == false );
			bFind = true;
			continue;
		}

		if( bFind )
		{
			//Change Chunk Entity
			pkEntity = m_pkModels.GetAt(i);
			if( pkEntity )
			{
				uiCount = 0;
				pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
				pkEditorInfoComponent->GetElementCount(CEditorInfoComponent::ms_kQuadTreeChunksName, uiCount);
				for( unsigned int ui = 0; ui < uiCount; ++ui )
				{
					pkEditorInfoComponent->GetPropertyData(CEditorInfoComponent::ms_kQuadTreeChunksName, kData, ui);
					pkChunk = pkMap->GetChunk(kData.x, kData.y);
					if( pkChunk )
						pkChunk->ChangeEntityIndex(GetX(), GetY(), i, i - 1);
				}
			}
			//向前移动Entity
			m_pkModels.SetAt(i - 1, pkEntity);
		}
	}

	if( bFind )
	{
		m_pkModels.SetAt(m_pkModels.GetSize() - 1, NULL);
		m_pkModels.SetSize(m_pkModels.GetSize() - 1);
		pkInEntity->RemoveReference();
		m_bDirty = true;
	}
}

NiEntityInterface* CTile::GetEntity(unsigned int uiIndex)
{
	if( uiIndex < m_pkModels.GetSize() )
	{
		return m_pkModels.GetAt(uiIndex);
	}

	return NULL;
}

unsigned int CTile::GetEntityIndex(NiEntityInterface* pkEntity)
{
	for( unsigned int i = 0; i < m_pkModels.GetSize(); i++ )
	{
		if( m_pkModels.GetAt(i) == pkEntity )
			return i;
	}

	return INVALID_INDEX;
}

void CTile::UpdateEditorInfo()
{
	NiPoint2 kData;
	kData.x = (float)GetX();
	kData.y = (float)GetY();

	NiEntityInterface* pkEntity;
	CEditorInfoComponent* pkEditorInfoComponent;
	for(unsigned int i = 0; i < m_pkModels.GetSize(); ++i)
	{
		pkEntity = m_pkModels.GetAt(i);

		pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
		if( !pkEditorInfoComponent )
		{
			pkEditorInfoComponent = NiNew CEditorInfoComponent;
			pkEntity->AddComponent(pkEditorInfoComponent);
		}

		pkEditorInfoComponent->SetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData, 0);
	}

	for(unsigned int i = 0; i < m_pkNpcEntities.GetSize(); ++i)
	{
		pkEntity = m_pkNpcEntities.GetAt(i);

		pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
		if( !pkEditorInfoComponent )
		{
			pkEditorInfoComponent = NiNew CEditorInfoComponent;
			pkEntity->AddComponent(pkEditorInfoComponent);
		}

		pkEditorInfoComponent->SetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData, 0);
	}

    for(unsigned int i = 0; i < m_pkGameObjectEntities.GetSize(); ++i)
    {
        pkEntity = m_pkGameObjectEntities.GetAt(i);

        pkEditorInfoComponent = (CEditorInfoComponent *)pkEntity->GetComponentByTemplateID(CEditorInfoComponent::ms_kTemplateID);
        if( !pkEditorInfoComponent )
        {
            pkEditorInfoComponent = NiNew CEditorInfoComponent;
            pkEntity->AddComponent(pkEditorInfoComponent);
        }

        pkEditorInfoComponent->SetPropertyData(CEditorInfoComponent::ms_kTileIndexPropertyName, kData, 0);
    }

	for( int x = 0; x < CHUNK_COUNT; x++ )
	{
		for( int y = 0; y < CHUNK_COUNT; y++ )
		{
			m_kChunks[x][y].UpdateEditorInfo();
		}
	} 
}

bool CTile::IsDirty() const
{
	return m_bDirty;
}

void CTile::SetDirty( bool bDirty )
{
	m_bDirty = bDirty;
}

void CTile::GetBound(CBox& kBox)
{
	kBox.Reset();

	kBox.m_kMin.x = float(m_iX*TILE_SIZE);
	kBox.m_kMin.y = float(m_iY*TILE_SIZE);
	kBox.m_kMax.x = kBox.m_kMin.x + float(TILE_SIZE);
	kBox.m_kMax.y = kBox.m_kMin.y + float(TILE_SIZE);

	for( int x = 0; x < CHUNK_COUNT; x++ )
	{
		for( int y = 0; y < CHUNK_COUNT; y++ )
		{
			if( kBox.m_kMax.z < m_kChunks[x][y].GetBoundBox().m_kMax.z )
				kBox.m_kMax.z = m_kChunks[x][y].GetBoundBox().m_kMax.z;

			if( kBox.m_kMin.z > m_kChunks[x][y].GetBoundBox().m_kMin.z )
				kBox.m_kMin.z = m_kChunks[x][y].GetBoundBox().m_kMin.z;
		}
	}

}

//-------------------------------------------------------------------------------------------------------------------------------------------------
//静态阴影计算
//

class CSourceTexture : public NiSourceTexture
{
public:
	void SetPixelData(NiPixelData* pkPixelData)
	{
		m_spSrcPixelData = pkPixelData;
	}
};

//把贴图都转换成RGBA格式
void ConvertTextureFormat(NiAVObject* pkAVObject)
{
	if(NiIsKindOf(NiGeometry, pkAVObject))
	{
		AlphaMode eAlphaMode = GetAlphaMode((NiGeometry*)pkAVObject);
		if(eAlphaMode == ALPHA_IGNORE) //不计算这个模型的阴影
			return;

		if(eAlphaMode != ALPHA_NONE)
		{
			NiPropertyState* pkPropertyState = ((NiGeometry*)pkAVObject)->GetPropertyState();

			//查找Texture Alpha
			NiTexturingProperty* pkTexturingProperty = pkPropertyState->GetTexturing();	
			NiTexturingProperty::Map* pkBaseMap = pkTexturingProperty->GetBaseMap();

			CSourceTexture* pkSourceTexture = (CSourceTexture*)pkBaseMap->GetTexture();
			NiPixelData* pkPixelData = pkSourceTexture->GetSourcePixelData();
			const NiPixelFormat& kPixelFormal = pkPixelData->GetPixelFormat();

			//如果是DTX1 DTX3 or DTX5我们需要转换
			switch(kPixelFormal.GetFormat())
			{
			case NiPixelFormat::FORMAT_DXT3:
			case NiPixelFormat::FORMAT_DXT5:
			case NiPixelFormat::FORMAT_DXT1:
				{
					pkPixelData = NiImageConverter::GetImageConverter()->ConvertPixelData(*pkPixelData, NiPixelFormat::RGBA32, NULL, false);
					pkSourceTexture->SetPixelData(pkPixelData);
				}
				break;
			}
		}
	}
	else if( NiIsKindOf(NiNode, pkAVObject) )
	{
		for(unsigned int ui = 0; ui < ((NiNode*)pkAVObject)->GetArrayCount(); ++ui)
		{
			ConvertTextureFormat(((NiNode*)pkAVObject)->GetAt(ui));
		}
	}
}

#define BLUR_SIZE 6
#define SHADOW_WIDTH		(CHUNK_COUNT * (ALPHA_MAP_SIZE -  1) + 1 + BLUR_SIZE*2) //1009 + 12 = 1021
#define SHADOW_HEIGHT		(CHUNK_COUNT * (ALPHA_MAP_SIZE -  1) + 1 + BLUR_SIZE*2) //1009 + 12 = 1021
#define ALPHA_PIXEL_LENGTH  (float( CHUNK_SIZE) / float(ALPHA_MAP_SIZE -  1)) // 32.f / 63.f

#define TRACE_DISTANCE		1000.f

//收集当前场景里面有阴影的模型
void CollectShadowedModel(NiTPrimitiveArray<NiAVObject*>& kModelList, bool bConvertTexture)
{
	CMap* pkMap = CMap::Get();
	NIASSERT( pkMap );

	NiScene* pkScene = pkMap->GetScene();

	NiEntityInterface* pkEntity;
	NiObject* pkSceneRootPointer;
	NiAVObject* pkSceneRoot;

	for( unsigned int i = 0; i < pkScene->GetEntityCount(); i++ )
	{
		pkEntity = pkScene->GetEntityAt(i);

		if( pkEntity == pkMap )
			continue;

		if( pkEntity->GetHidden() )
			continue;


		pkSceneRootPointer = NULL;
		pkSceneRoot = NULL;
		if (pkEntity->GetPropertyData("Scene Root Pointer", pkSceneRootPointer, 0))
		{
			pkSceneRoot = NiDynamicCast(NiAVObject, pkSceneRootPointer);
		}

		if( pkSceneRoot )
		{
			if( bConvertTexture )
				ConvertTextureFormat(pkSceneRoot);

			kModelList.Add(pkSceneRoot);
		}
	}
}

void CTile::BlurShadow(float* pfShadow)
{
	// gaussian blur
	int iPixelKernel[13] =
	{
		{ -6 },
		{ -5 },
		{ -4 },
		{ -3 },
		{ -2 },
		{ -1 },
		{  0 },
		{  1 },
		{  2 },
		{  3 },
		{  4 },
		{  5 },
		{  6 },
	};

	float fBlurWeights[13] = 
	{
		0.002216f,
		0.008764f,
		0.026995f,
		0.064759f,
		0.120985f,
		0.176033f,
		0.199471f,
		0.176033f,
		0.120985f,
		0.064759f,
		0.026995f,
		0.008764f,
		0.002216f,
	};

	float* pfShadowSave = NiAlloc(float, SHADOW_WIDTH*SHADOW_HEIGHT);
	float* pfBase;
	float* pfBaseSave;

	memcpy(pfShadowSave, pfShadow, SHADOW_WIDTH*SHADOW_HEIGHT*sizeof(float));
	for(int h = BLUR_SIZE; h < SHADOW_HEIGHT - BLUR_SIZE; ++h)
	{
		for(int w = BLUR_SIZE; w < SHADOW_WIDTH - BLUR_SIZE; ++w)
		{
			pfBase = pfShadow + h*SHADOW_WIDTH + w;
			pfBaseSave = pfShadowSave + + h*SHADOW_WIDTH + w;

			*pfBase = 0.f;
			for(unsigned int i = 0; i < 13; ++i)
			{
				*pfBase += pfBaseSave[iPixelKernel[i]]*fBlurWeights[i]; 
			}
		}
	}

	memcpy(pfShadowSave, pfShadow, SHADOW_WIDTH*SHADOW_HEIGHT*sizeof(float));
	for(int h = BLUR_SIZE; h < SHADOW_HEIGHT - BLUR_SIZE; ++h)
	{
		for(int w = BLUR_SIZE; w < SHADOW_WIDTH - BLUR_SIZE; ++w)
		{
			pfBase = pfShadow + h*SHADOW_WIDTH + w;
			pfBaseSave = pfShadowSave + + h*SHADOW_WIDTH + w;

			*pfBase = 0.f;
			for(unsigned int i = 0; i < 13; ++i)
			{
				*pfBase += pfBaseSave[iPixelKernel[i]*SHADOW_WIDTH]*fBlurWeights[i];
			}
		}
	}

	NiFree(pfShadowSave);
}

void CTile::SplitShadow(float* pfTileShadow, float fSoftness)
{
	unsigned char* pucChunkShadow = NiAlloc(unsigned char, ALPHA_MAP_SIZE*ALPHA_MAP_SIZE);
	unsigned char* pucPtr = pucChunkShadow;
	float* pfBase;

	char buff[MAX_PATH];

	for( int i = 0; i < CHUNK_COUNT; i++ )
	{
		for( int j = 0; j < CHUNK_COUNT; j++ ) 
		{
			pfBase = pfTileShadow + (BLUR_SIZE + i * (ALPHA_MAP_SIZE - 1)) * SHADOW_WIDTH + ( BLUR_SIZE + j * (ALPHA_MAP_SIZE - 1));

			pucPtr = pucChunkShadow;
			for( int y = 0; y < ALPHA_MAP_SIZE; y++ )
			{
				for( int x = 0; x < ALPHA_MAP_SIZE; x++, pucPtr++)
				{ 
					//*pucPtr = (unsigned char)(pfBase[y*SHADOW_WIDTH + x]*fSoftness * 255.f);

					float fShadow = pfBase[y*SHADOW_WIDTH + x];//*fSoftness * 255.f;

					if ( fShadow >= 1.0f )
						*pucPtr = (unsigned char)80;
					else
						*pucPtr = 0;
				}
			}

			m_kChunks[j][i].SetShadow(pucChunkShadow);
		}
	}

	NiFree(pucChunkShadow);
}

//计算静态阴影
void CTile::CalcShadow(bool bModelShadow, bool bTerrainShadow, float fSoftness)
{
	NiLogger::SetOutputToDebugWindow(NIMESSAGE_GENERAL_0, true);
	NiLogger("Begin calc shadow %d %d\n", m_iX, m_iY);
	NiSystemClockTimer kTimer;
	kTimer.Start();


	NiTPrimitiveArray<NiAVObject*> kModelList;
	CollectShadowedModel(kModelList, true); //现在主要作用是用来转换贴图格式

	NiLogger("Model Count %d\n", kModelList.GetSize());


	CMap* pkMap = CMap::Get();
	NIASSERT( pkMap )

	float* pfShadow = NiAlloc(float, SHADOW_WIDTH*SHADOW_HEIGHT);
	memset(pfShadow, 0, SHADOW_WIDTH*SHADOW_HEIGHT*sizeof(float));

	NiPoint3 kDir = -LIGHT_DIR;
	kDir.Unitize();

	float fTime;

	float fBaseX = float(m_iX * TILE_SIZE) - float(BLUR_SIZE) * ALPHA_PIXEL_LENGTH;
	float fBaseY = float(m_iY * TILE_SIZE) - float(BLUR_SIZE) * ALPHA_PIXEL_LENGTH;

	for( int h = 0; h < SHADOW_HEIGHT; h++)
	{
		NiLogger("Current %4d %4d\n", h, SHADOW_HEIGHT);

		NiPoint3 kOrigin;
		NiPoint3 kBegin;
		NiPoint3 kEnd;
		float* pfPtr;

		kOrigin.y = fBaseY + ALPHA_PIXEL_LENGTH*h;

		for( int w = 0; w < SHADOW_WIDTH; w++)
		{
			kOrigin.x = fBaseX + ALPHA_PIXEL_LENGTH*w;
			kOrigin.z = pkMap->GetHeight(kOrigin.x, kOrigin.y);

			pfPtr = pfShadow + h*SHADOW_WIDTH + w;

			kBegin = kOrigin + kDir*0.1f;
			kEnd = kOrigin + kDir*TRACE_DISTANCE;

			//地形阴影
			if( bTerrainShadow && pkMap->CollideLine(kBegin, kEnd, fTime) )
			{
				*pfPtr = 1.f;
				continue;
			}

			pkMap->CalcShadow(kBegin, kEnd, *pfPtr);
		}
	}

	//BlurShadow(pfShadow);
	SplitShadow(pfShadow, fSoftness);

	NiFree(pfShadow);

	kTimer.Stop();
	NiLogger("Calc shadow finish %d %d\n", m_iX, m_iY);
	NiLogger("time %f minite\n", kTimer.GetElapsed()/60.f);
	NiLogger::SetOutputToDebugWindow(NIMESSAGE_GENERAL_0, false);
}

//---------------------------------------------------------------------------------------------------------------------------------------------------------
//Fast Calc Shadow
//
//
void CTile::DrawAVObject(NiAVObject* pkAVObject, NiMaterial* pkMaterial)
{
	if(NiIsKindOf(NiGeometry, pkAVObject))
	{
		NiStencilProperty* pkStencilProp;
		NiGeometry* pkGeometry = (NiGeometry*)pkAVObject;
		const NiMaterial* pkOldMaterial = NULL;

		if( pkMaterial )
		{
			pkOldMaterial = pkGeometry->GetActiveMaterial();
			pkGeometry->ApplyAndSetActiveMaterial(pkMaterial);
		}

		pkStencilProp = pkGeometry->GetPropertyState()->GetStencil();
		if( pkStencilProp )
		{
			pkStencilProp->SetDrawMode(NiStencilProperty::DRAW_BOTH);
		}
		else
		{
			pkGeometry->GetPropertyState()->SetProperty(m_spStencilProperty);
		}

		pkGeometry->RenderImmediate(NiRenderer::GetRenderer());

		if( pkMaterial )
		{
			pkGeometry->RemoveMaterial(pkMaterial);
			pkGeometry->SetActiveMaterial(pkOldMaterial);
		}
	}
	else if( NiIsKindOf(NiNode, pkAVObject) )
	{
		for(unsigned int ui = 0; ui < ((NiNode*)pkAVObject)->GetArrayCount(); ++ui)
		{
			DrawAVObject( ((NiNode*)pkAVObject)->GetAt(ui), pkMaterial );
		}
	}
}

//todo...
//禁止back face culling
bool CTile::FastCalcShadow(bool bModelShadow, bool bTerrainShadow, float fSoftness)
{
	NiTPrimitiveArray<NiAVObject*> kModelList;
	CollectShadowedModel(kModelList, false);

	CMap* pkMap = CMap::Get();
	NIASSERT( pkMap );

	NiRenderer* pkRenderer = NiRenderer::GetRenderer();
	if( !pkRenderer )
		return false;

	NiColorA kOldColor;
	pkRenderer->GetBackgroundColor(kOldColor);
	pkRenderer->SetBackgroundColor(NiColor());

	CBox kBoundBox;
	GetBound(kBoundBox);
	float fHeight = (kBoundBox.m_kMax.z - kBoundBox.m_kMin.z) * 2.f;
	if( fHeight < 200.f )
		fHeight = 200.f;

	NiPoint3 kCenter((float)(TILE_SIZE * m_iX + TILE_SIZE * 0.5f), (float)(TILE_SIZE * m_iY + TILE_SIZE * 0.5f), (kBoundBox.m_kMax.z + kBoundBox.m_kMin.z) * 0.5f);

	NiCamera* pkCamera = NiNew NiCamera;
	NiFrustum kFrustum((float)(-TILE_SIZE / 2), (float)(TILE_SIZE / 2), (float)(-TILE_SIZE / 2), (float)(TILE_SIZE / 2), 1.0f, fHeight, true);

	pkCamera->SetViewFrustum(kFrustum);
	pkCamera->SetViewPort(NiRect<float>(0.0f, 1.0f, 1.0f, 0.0f));


	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	//1. get min model depth
	//

	//todo... 支持倾斜方向
	NiMatrix3 kRotation = TerrainHelper::LookAt(kCenter, kCenter + NiPoint3(0.f, 0.f, fHeight*0.5f), NiPoint3(0.f, 1.f, 0.f));
	pkCamera->SetWorldRotate(kRotation);
	pkCamera->SetWorldTranslate(kCenter + NiPoint3(0.f, 0.f, fHeight*0.5f));

	// Create Texture & RenderTarget
	NiTexture::FormatPrefs kPrefs;
	kPrefs.m_ePixelLayout = NiTexture::FormatPrefs::FLOAT_COLOR_128;
	kPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
	kPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::SMOOTH;

	NiRenderedTexture* pkTexture = NiRenderedTexture::Create(SHADOW_WIDTH, SHADOW_HEIGHT, pkRenderer, kPrefs);
	NiDepthStencilBuffer* pkDSBuffer = CDepthStencilBufferF32::Create(SHADOW_WIDTH, SHADOW_HEIGHT);
	NiRenderTargetGroup* pkRenderTarget = NiRenderTargetGroup::Create(pkTexture->GetBuffer(), pkRenderer, pkDSBuffer);

	pkRenderer->BeginFrame();
	pkRenderer->BeginUsingRenderTargetGroup(pkRenderTarget, NiRenderer::CLEAR_ALL);

	pkRenderer->SetCameraData(pkCamera);

	//Render Models
	for( unsigned int i = 0; i < kModelList.GetSize(); i++ )
	{
		DrawAVObject(kModelList.GetAt(i), NULL);
	}

	pkRenderer->EndUsingRenderTargetGroup();

	pkRenderer->EndFrame();
	pkRenderer->DisplayFrame();

	//NiDX92DBufferData* pkBufferData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)pkRenderTarget->GetBufferRendererData(0));
	//D3DXSaveSurfaceToFileA("c:\\test1.tga", D3DXIFF_TGA, pkBufferData->GetSurface(), NULL, NULL);

	NiDX92DBufferData* pkDepthData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)pkRenderTarget->GetDepthStencilBufferRendererData());
	//TerrainHelper::SaveDepthSurfaceToFile("c:\\test2.tga", pkDepthData->GetSurface());

	D3DLOCKED_RECT kRect;
	pkDepthData->GetSurface()->LockRect(&kRect, NULL, D3DLOCK_READONLY);
	int iPitch = kRect.Pitch/4;
	float* pfMinModelDepth = NiAlloc(float, iPitch*SHADOW_HEIGHT);
	memcpy(pfMinModelDepth, kRect.pBits, iPitch*SHADOW_HEIGHT*sizeof(float));
	pkDepthData->GetSurface()->UnlockRect();

	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	//2. get terrain depth
	//
	pkRenderer->BeginFrame();
	pkRenderer->BeginUsingRenderTargetGroup(pkRenderTarget, NiRenderer::CLEAR_ALL);

	pkRenderer->SetCameraData(pkCamera);
	for( int i = 0; i < CHUNK_COUNT; i++ )
	{
		for( int j = 0; j < CHUNK_COUNT; j++ )
		{
			DrawAVObject(m_kChunks[j][i].GetGeometry(), NULL);
		}
	}

	pkRenderer->EndUsingRenderTargetGroup();

	pkRenderer->EndFrame();
	pkRenderer->DisplayFrame();

	//pkBufferData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)pkRenderTarget->GetBufferRendererData(0));
	//D3DXSaveSurfaceToFileA("c:\\test3.tga", D3DXIFF_TGA, pkBufferData->GetSurface(), NULL, NULL);

	pkDepthData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)pkRenderTarget->GetDepthStencilBufferRendererData());
	//TerrainHelper::SaveDepthSurfaceToFile("c:\\test4.tga", pkDepthData->GetSurface());

	pkDepthData->GetSurface()->LockRect(&kRect, NULL, D3DLOCK_READONLY);
	float* pfTerrainDepth = NiAlloc(float, iPitch*SHADOW_HEIGHT);
	memcpy(pfTerrainDepth, kRect.pBits, iPitch*SHADOW_HEIGHT*sizeof(float));
	pkDepthData->GetSurface()->UnlockRect();


	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	//3. calc model shadow
	//
	pkRenderer->SetBackgroundColor(kOldColor);

	float fMinModelDepth;
	float fTerrainDepth;

	float* pfShadow = NiAlloc(float, SHADOW_WIDTH*SHADOW_HEIGHT);
	memset(pfShadow, 0, SHADOW_WIDTH*SHADOW_HEIGHT*sizeof(float));
	float* pfPtr;

	for( int h = 0; h < SHADOW_HEIGHT; h++)
	{
		for( int w = 0; w < SHADOW_WIDTH; w++)
		{
			pfPtr = pfShadow + h*SHADOW_WIDTH + w;

			fMinModelDepth = *(pfMinModelDepth + h*iPitch + w);
			fTerrainDepth = *(pfTerrainDepth + h*iPitch + w);

			if( fMinModelDepth < fTerrainDepth ) 
				*pfPtr = 1.f;
			else
				*pfPtr = 0.f;
		}
	}

	//BlurShadow(pfShadow);
	SplitShadow(pfShadow, fSoftness);

	NiFree(pfShadow);

	NiDelete pkTexture;
	NiDelete pkRenderTarget;

	NiDelete pkCamera;

	NiFree(pfMinModelDepth);
	NiFree(pfTerrainDepth);

	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------
//ClearShadow
//
//
void CTile::ClearShadow()
{
	float* pfShadow = NiAlloc(float, SHADOW_WIDTH*SHADOW_HEIGHT);
	memset(pfShadow, 0, SHADOW_WIDTH*SHADOW_HEIGHT*sizeof(float));

	SplitShadow(pfShadow, 0.f);

	NiFree(pfShadow);
}

//---------------------------------------------------------------------------------------------------------------------------------------------------------
//计算服务器端用的地形信息
//
//
#define TILE_GRID_COUNT (CHUNK_COUNT * GRID_COUNT) //512

bool IsModelMoveable(NiPick& kPick, NiAVObject* pkAVObject, NiPoint3& kOrigin, NiPoint3& kDir)
{
	if(NiIsKindOf(NiGeometry, pkAVObject))
	{
		NiPropertyState* pkPropertyState = ((NiGeometry*)pkAVObject)->GetPropertyState();
		NiAlphaProperty* pkAlphaProperty = pkPropertyState->GetAlpha();
		if( pkAlphaProperty )
		{
			if( pkAlphaProperty->GetAlphaBlending() )
			{
				return true;
			}
		}

		kPick.SetPickType(NiPick::FIND_ALL);
		kPick.SetSortType(NiPick::NO_SORT);
		kPick.SetIntersectType(NiPick::TRIANGLE_INTERSECT);
		kPick.SetTarget(pkAVObject);

		if( kPick.PickObjects(kOrigin, kDir) )
		{
			float fMinHeight = 3.f;
			float fMaxHeight = 0.f;
			float fHeight;

			const NiPick::Results& kResults = kPick.GetResults();
			for(unsigned int ui = 0; ui < kResults.GetSize(); ++ui)
			{
				fHeight = kResults.GetAt(ui)->GetDistance();
				if( fHeight < fMinHeight )
					fMinHeight = fHeight;
				if( fHeight > fMaxHeight )
					fMaxHeight = fHeight;
			}

			if( fMinHeight > 2.5f )//此物件离地面很高, 玩家可以从物件下面行走
				return true;

			//if( fMaxHeight < 0.5f )//此物件离地面很低, 玩家可以从物件上面行走
			//	return true;

			return false;
		}

		return true;
	}
	else if( NiIsKindOf(NiNode, pkAVObject) )
	{
		kPick.SetPickType(NiPick::FIND_FIRST);
		kPick.SetSortType(NiPick::NO_SORT);
		kPick.SetIntersectType(NiPick::BOUND_INTERSECT);
		kPick.SetTarget(pkAVObject);
		if( !kPick.PickObjects(kOrigin, kDir) )
			return true;

		for(unsigned int ui = 0; ui < ((NiNode*)pkAVObject)->GetArrayCount(); ++ui)
		{
			if( !IsModelMoveable(kPick, ((NiNode*)pkAVObject)->GetAt(ui), kOrigin, kDir) )
				return false;
		}

		return true;
	}

	return true;
}

//收集当前场景里面碰撞体的模型
void CTile::CollectCollisionModel(NiTPrimitiveArray<NiAVObject*>& kModelList)
{
	CMap* pkMap = CMap::Get();
	NIASSERT( pkMap );

	NiScene* pkScene = pkMap->GetScene();

	NiEntityInterface* pkEntity;
	NiObject* pkSceneRootPointer;
	NiAVObject* pkSceneRoot;

	for( unsigned int i = 0; i < pkScene->GetEntityCount(); i++ )
	{
		pkEntity = pkScene->GetEntityAt(i);

		if( pkEntity == pkMap )
			continue;

		pkSceneRootPointer = NULL;
		pkSceneRoot = NULL;
		if (pkEntity->GetPropertyData(ms_kSceneRootPointerName, pkSceneRootPointer, 0))
		{
			pkSceneRoot = NiDynamicCast(NiAVObject, pkSceneRootPointer);
		}

		if( pkSceneRoot )
		{
			kModelList.Add(pkSceneRoot);
		}
	}
}

//
void CTile::SplitGridInfo(unsigned char* pucGridInfo)
{
	unsigned char* pucChunkGrid = NiAlloc(unsigned char, GRID_COUNT*GRID_COUNT);
	unsigned char* pucPtr = pucChunkGrid;
	unsigned char* pucBase;

	for( int i = 0; i < CHUNK_COUNT; i++ )
	{
		for( int j = 0; j < CHUNK_COUNT; j++ )
		{
			pucBase = pucGridInfo + i * GRID_COUNT * TILE_GRID_COUNT + j * GRID_COUNT;

			pucPtr = pucChunkGrid;
			for( int y = 0; y < GRID_COUNT; y++ )
			{
				for( int x = 0; x < GRID_COUNT; x++, pucPtr++)
				{
					*pucPtr = pucBase[y*TILE_GRID_COUNT + x];
				}
			}

			m_kChunks[j][i].SetGridInfo(pucChunkGrid, WalkGrid);
		}
	}

	NiFree(pucChunkGrid);
}

//计算网格信息
void CTile::CalcGridInfo()
{
	NiTPrimitiveArray<NiAVObject*> kModelList;
	CollectCollisionModel(kModelList);

	CMap* pkMap = CMap::Get();
	NIASSERT( pkMap )

		unsigned char* pucGridInfo = NiAlloc(unsigned char, TILE_GRID_COUNT*TILE_GRID_COUNT);
	memset(pucGridInfo, 0, TILE_GRID_COUNT*TILE_GRID_COUNT*sizeof(unsigned char));//所有的都设置成不可行走

	NiPoint3 kDir(0.f, 0.f, 1.f);
	NiPoint3 kNormal;

	NiPoint3 kOrigin;

	bool bMoveable;

	float fTime;

	NiPick kPick;
	kPick.SetFrontOnly(false);

	float fBaseX = float(m_iX * TILE_SIZE);
	float fBaseY = float(m_iY * TILE_SIZE);

	unsigned char* pucPtr = pucGridInfo;
	kOrigin.y = fBaseY + GRID_SIZE/2.f;
	for( int h = 0; h < TILE_GRID_COUNT; h++, kOrigin.y += GRID_SIZE )
	{
		kOrigin.x = fBaseX + GRID_SIZE/2.f;
		for( int w = 0; w < TILE_GRID_COUNT; w++, kOrigin.x += GRID_SIZE, pucPtr++)
		{
			if( !pkMap->GetHeightAndNormal(kOrigin.x, kOrigin.y, kOrigin.z, kNormal) )
			{
				//不可行走
				continue;
			}

			if( kNormal.z < 0.7f )
			{
				//地面太陡峭, 不可行走
				continue;
			}

			//计算和模型的碰撞
			bMoveable = true;

			for( unsigned int i = 0; i < kModelList.GetSize(); i++ )
			{
				if( !IsModelMoveable(kPick, kModelList.GetAt(i), kOrigin, kDir) )
				{
					bMoveable = false;
					break;
				}
			}

			if( bMoveable )
				*pucPtr = 1; 
		}
	}

	SplitGridInfo(pucGridInfo);

	NiFree(pucGridInfo);
}

//快速计算网格信息
bool CTile::FastCalcGridInfo()
{
	NiTPrimitiveArray<NiAVObject*> kModelList;
	CollectCollisionModel(kModelList);

	CMap* pkMap = CMap::Get();
	NIASSERT( pkMap );

	NiRenderer* pkRenderer = NiRenderer::GetRenderer();
	if( !pkRenderer )
		return false;

	NiColorA kOldColor;
	pkRenderer->GetBackgroundColor(kOldColor);
	pkRenderer->SetBackgroundColor(NiColor());

	CBox kBoundBox;
	GetBound(kBoundBox);
	float fHeight = (kBoundBox.m_kMax.z - kBoundBox.m_kMin.z) * 2.f;
	if( fHeight < 200.f )
		fHeight = 200.f;

	NiPoint3 kCenter((float)(TILE_SIZE * m_iX + TILE_SIZE * 0.5f), (float)(TILE_SIZE * m_iY + TILE_SIZE * 0.5f), (kBoundBox.m_kMax.z + kBoundBox.m_kMin.z) * 0.5f);

	NiCamera* pkCamera = NiNew NiCamera;
	NiFrustum kFrustum((float)(-TILE_SIZE / 2), (float)(TILE_SIZE / 2), (float)(-TILE_SIZE / 2), (float)(TILE_SIZE / 2), 1.0f, fHeight, true);

	pkCamera->SetViewFrustum(kFrustum);
	pkCamera->SetViewPort(NiRect<float>(0.0f, 1.0f, 1.0f, 0.0f));


	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	//1. get min model depth
	//
	NiMatrix3 kRotation = TerrainHelper::LookAt(kCenter, kCenter + NiPoint3(0.f, 0.f, fHeight/2.f), NiPoint3(0.f, 1.f, 0.f));
	pkCamera->SetWorldRotate(kRotation);
	pkCamera->SetWorldTranslate(kCenter + NiPoint3(0.f, 0.f, fHeight/2.f));

	// Create Texture & RenderTarget
	NiTexture::FormatPrefs kPrefs;
	kPrefs.m_ePixelLayout = NiTexture::FormatPrefs::FLOAT_COLOR_128;
	kPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
	kPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::SMOOTH;

	NiRenderedTexture* pkTexture = NiRenderedTexture::Create(TILE_GRID_COUNT, TILE_GRID_COUNT, pkRenderer, kPrefs);
	NiDepthStencilBuffer* pkDSBuffer = CDepthStencilBufferF32::Create(TILE_GRID_COUNT, TILE_GRID_COUNT);
	NiRenderTargetGroup* pkRenderTarget = NiRenderTargetGroup::Create(pkTexture->GetBuffer(), pkRenderer, pkDSBuffer);

	pkRenderer->BeginFrame();
	pkRenderer->BeginUsingRenderTargetGroup(pkRenderTarget, NiRenderer::CLEAR_ALL);

	pkRenderer->SetCameraData(pkCamera);

	//Render Models
	for( unsigned int i = 0; i < kModelList.GetSize(); i++ )
	{
		DrawAVObject(kModelList.GetAt(i), NULL);
	}

	pkRenderer->EndUsingRenderTargetGroup();

	pkRenderer->EndFrame();
	pkRenderer->DisplayFrame();

	//NiDX92DBufferData* pkBufferData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)pkRenderTarget->GetBufferRendererData(0));
	//D3DXSaveSurfaceToFileA("c:\\test1.tga", D3DXIFF_TGA, pkBufferData->GetSurface(), NULL, NULL);

	NiDX92DBufferData* pkDepthData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)pkRenderTarget->GetDepthStencilBufferRendererData());
	//TerrainHelper::SaveDepthSurfaceToFile("c:\\test2.tga", pkDepthData->GetSurface());

	D3DLOCKED_RECT kRect;
	pkDepthData->GetSurface()->LockRect(&kRect, NULL, D3DLOCK_READONLY);
	float* pfMinModelDepth = NiAlloc(float, TILE_GRID_COUNT*TILE_GRID_COUNT);
	memcpy(pfMinModelDepth, kRect.pBits, TILE_GRID_COUNT*TILE_GRID_COUNT*sizeof(float));
	pkDepthData->GetSurface()->UnlockRect();

	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	//2. get terrain depth
	//
	pkRenderer->BeginFrame();
	pkRenderer->BeginUsingRenderTargetGroup(pkRenderTarget, NiRenderer::CLEAR_ALL);

	pkRenderer->SetCameraData(pkCamera);
	for( int i = 0; i < CHUNK_COUNT; i++ )
	{
		for( int j = 0; j < CHUNK_COUNT; j++ )
		{
			DrawAVObject(m_kChunks[j][i].GetGeometry(), NULL);
		}
	}

	pkRenderer->EndUsingRenderTargetGroup();

	pkRenderer->EndFrame();
	pkRenderer->DisplayFrame();

	//pkBufferData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)pkRenderTarget->GetBufferRendererData(0));
	//D3DXSaveSurfaceToFileA("c:\\test3.tga", D3DXIFF_TGA, pkBufferData->GetSurface(), NULL, NULL);

	pkDepthData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)pkRenderTarget->GetDepthStencilBufferRendererData());
	//TerrainHelper::SaveDepthSurfaceToFile("c:\\test4.tga", pkDepthData->GetSurface());

	pkDepthData->GetSurface()->LockRect(&kRect, NULL, D3DLOCK_READONLY);
	float* pfTerrainDepth = NiAlloc(float, TILE_GRID_COUNT*TILE_GRID_COUNT);
	memcpy(pfTerrainDepth, kRect.pBits, TILE_GRID_COUNT*TILE_GRID_COUNT*sizeof(float));
	pkDepthData->GetSurface()->UnlockRect();

	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	//3. get max model depth
	//
	kFrustum = NiFrustum((float)(TILE_SIZE / 2), (float)(-TILE_SIZE / 2), (float)(-TILE_SIZE / 2), (float)(TILE_SIZE / 2), 1.0f, fHeight, true);

	kRotation = TerrainHelper::LookAt(kCenter, kCenter - NiPoint3(0.f, 0.f, fHeight/2.f), NiPoint3(0.f, 1.f, 0.f));
	pkCamera->SetWorldRotate(kRotation);
	pkCamera->SetWorldTranslate(kCenter - NiPoint3(0.f, 0.f, fHeight/2.f));
	pkCamera->SetViewFrustum(kFrustum);

	pkRenderer->BeginFrame();
	pkRenderer->BeginUsingRenderTargetGroup(pkRenderTarget, NiRenderer::CLEAR_ALL);

	pkRenderer->SetCameraData(pkCamera);

	//Render Models
	for( unsigned int i = 0; i < kModelList.GetSize(); i++ )
	{
		DrawAVObject(kModelList.GetAt(i), NULL);
	}

	pkRenderer->EndUsingRenderTargetGroup();

	pkRenderer->EndFrame();
	pkRenderer->DisplayFrame();

	//pkBufferData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)pkRenderTarget->GetBufferRendererData(0));
	//D3DXSaveSurfaceToFileA("c:\\test5.tga", D3DXIFF_TGA, pkBufferData->GetSurface(), NULL, NULL);

	pkDepthData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)pkRenderTarget->GetDepthStencilBufferRendererData());
	//TerrainHelper::SaveDepthSurfaceToFile("c:\\test6.tga", pkDepthData->GetSurface());

	pkDepthData->GetSurface()->LockRect(&kRect, NULL, D3DLOCK_READONLY);
	float* pfMaxModelDepth = NiAlloc(float, TILE_GRID_COUNT*TILE_GRID_COUNT);
	memcpy(pfMaxModelDepth, kRect.pBits, TILE_GRID_COUNT*TILE_GRID_COUNT*sizeof(float));
	pkDepthData->GetSurface()->UnlockRect();


	//-------------------------------------------------------------------------------------------------------------------------------------------------------
	//4. calc grid info
	//
	pkRenderer->SetBackgroundColor(kOldColor);

	unsigned char* pucGridInfo = NiAlloc(unsigned char, TILE_GRID_COUNT*TILE_GRID_COUNT);
	memset(pucGridInfo, 0, TILE_GRID_COUNT*TILE_GRID_COUNT*sizeof(unsigned char));//所有的都设置成不可行走
	unsigned char* pucPtr = pucGridInfo;
	NiPoint3 kOrigin;
	NiPoint3 kNormal;

	float fBaseX = float(m_iX * TILE_SIZE);
	float fBaseY = float(m_iY * TILE_SIZE);
	bool bMoveable;

	float fMaxModelDepth;
	float fMinModelDepth;
	float fTerrainDepth;

	kOrigin.y = fBaseY + GRID_SIZE/2.f;
	for( int h = 0; h < TILE_GRID_COUNT; h++, kOrigin.y += GRID_SIZE )
	{
		kOrigin.x = fBaseX + GRID_SIZE/2.f;
		for( int w = 0; w < TILE_GRID_COUNT; w++, kOrigin.x += GRID_SIZE, pucPtr++)
		{
			if( !pkMap->GetHeightAndNormal(kOrigin.x, kOrigin.y, kOrigin.z, kNormal) )
			{
				//不可行走
				continue;
			}

			if( kNormal.z < 0.7f )
			{
				//地面太陡峭, 不可行走
				continue;
			}

			fMinModelDepth = *(pfMinModelDepth + h*TILE_GRID_COUNT + w);
			fMaxModelDepth = *(pfMaxModelDepth + h*TILE_GRID_COUNT + w);
			if( fMaxModelDepth < 1.f )
				fMaxModelDepth = 1.f - fMaxModelDepth;

			fTerrainDepth = *(pfTerrainDepth + h*TILE_GRID_COUNT + w);

			if( fMinModelDepth >= fTerrainDepth )
				bMoveable = true;
			else if( fMaxModelDepth <= fTerrainDepth )
			{
				if( (fTerrainDepth - fMaxModelDepth)*fHeight > 2.f )
					bMoveable = true;
				else
					bMoveable = false;
			}
			else
			{
				if( (fTerrainDepth - fMinModelDepth)*fHeight < 0.5f )
					bMoveable = true;//可以在模型上面行走
				else
					bMoveable = false;
			}

			if( bMoveable )
				*pucPtr = 1; 
		}
	}

	SplitGridInfo(pucGridInfo);

	NiDelete pkTexture;
	NiDelete pkRenderTarget;

	NiDelete pkCamera;
	NiFree(pucGridInfo);

	NiFree(pfMaxModelDepth);
	NiFree(pfMinModelDepth);
	NiFree(pfTerrainDepth);

	return true;
}

void CTile::ClearGridInfo()
{
	unsigned char* pucGridInfo = NiAlloc(unsigned char, TILE_GRID_COUNT*TILE_GRID_COUNT);
	memset(pucGridInfo, 255, TILE_GRID_COUNT*TILE_GRID_COUNT*sizeof(unsigned char));//所有的都设置成可行走

	SplitGridInfo(pucGridInfo);

	NiFree(pucGridInfo);
}

void CTile::CalcMoveable()
{
	for( int i = 0; i < CHUNK_COUNT; i++ )
	{
		for( int j = 0; j < CHUNK_COUNT; j++ )
		{
			m_kChunks[j][i].CalcMoveable(WalkGrid);
			m_kChunks[j][i].CalcMoveable(FlyGrid);
		}
	}
}

void CTile::ClearMoveable()
{
	for( int i = 0; i < CHUNK_COUNT; i++ )
	{
		for( int j = 0; j < CHUNK_COUNT; j++ )
		{
			m_kChunks[j][i].ClearMoveable(WalkGrid);
			m_kChunks[j][i].ClearMoveable(FlyGrid);
		}
	}
}

void CTile::OnWaterChanged(unsigned int uiWaterIndex)
{
	for( int i = 0; i < CHUNK_COUNT; i++ )
	{
		for( int j = 0; j < CHUNK_COUNT; j++ )
		{
			m_kChunks[j][i].OnWaterChanged(uiWaterIndex);
		}
	}
}

void CTile::Precache()
{
	for( int i = 0; i < CHUNK_COUNT; i++ )
	{
		for( int j = 0; j < CHUNK_COUNT; j++ )
		{
			m_kChunks[j][i].Precache();
		}
	}

	bool bS0Mipmap;
    bool bNonPow2;
    bool bChanged;
	NiD3DRenderer* pkRenderer = (NiD3DRenderer*)NiRenderer::GetRenderer();
	for(unsigned int ui = 0; ui < m_pkTextures.GetSize(); ++ui)
		pkRenderer->GetTextureManager()->PrepareTextureForRendering(GetTexture(ui)->pkTexture, bChanged, bS0Mipmap, bNonPow2);
}

void CTile::AddNpcEntity(NiEntityInterface* pkEntity)
{
	//检查重复添加
	for( unsigned int i = 0; i < m_pkNpcEntities.GetSize(); ++i )
	{
		if( pkEntity == m_pkNpcEntities.GetAt(i) )
			return;
	}

	pkEntity->AddReference();
	m_pkNpcEntities.Add(pkEntity);

	m_bDirty = true;
}

void CTile::RemoveNpcEntity(NiEntityInterface* pkEntity)
{
	bool bFind = false;
	for( unsigned int i = 0; i < m_pkNpcEntities.GetSize(); ++i )
	{
		if(m_pkNpcEntities.GetAt(i) == pkEntity)
		{
            pkEntity->RemoveReference();
            m_pkNpcEntities.SetAt(i, NULL);
			bFind = true;
		}
	}

	if( bFind )
	{
        m_pkNpcEntities.Compact();
		m_bDirty = true;
	}
}

void CTile::BuildNpcVisibleSet(NiEntityRenderingContext* pkRenderingContext)
{
	NiExternalAssetManager* pkAssetManager = CMap::GetAssetManager();
	NiEntityInterface* pkEntity;
	for(unsigned int ui = 0; ui < m_pkNpcEntities.GetSize(); ++ui)
	{
		pkEntity = m_pkNpcEntities.GetAt(ui);
		if( pkEntity )
		{
            pkEntity->Update(NULL, CMap::GetTime(), const_cast<NiEntityErrorInterface *>(CMap::Get()->GetErrorHandler()), pkAssetManager);
			pkEntity->BuildVisibleSet(pkRenderingContext, NULL);
		}
	}
}

void CTile::AddGameObject(NiEntityInterface* pkEntity)
{
	//检查重复添加
	for( unsigned int i = 0; i < m_pkGameObjectEntities.GetSize(); ++i )
	{
		if( pkEntity == m_pkGameObjectEntities.GetAt(i) )
			return;
	}

	pkEntity->AddReference();
	m_pkGameObjectEntities.Add(pkEntity);

	m_bDirty = true;
}

void CTile::RemoveGameObject(NiEntityInterface* pkEntity)
{
	for( unsigned int i = 0; i < m_pkGameObjectEntities.GetSize(); ++i )
	{
		if( m_pkGameObjectEntities.GetAt(i) == pkEntity )
		{
            m_pkGameObjectEntities.SetAt(i, NULL);
            m_pkGameObjectEntities.Compact();
            pkEntity->RemoveReference();
            m_bDirty = true;
			break;
		}
	}
}

void CTile::BuildGameObjectVisibleSet(NiEntityRenderingContext* pkRenderingContext)
{
	NiExternalAssetManager* pkAssetManager = CMap::GetAssetManager();
	NiEntityInterface* pkEntity;
	for(unsigned int ui = 0; ui < m_pkGameObjectEntities.GetSize(); ++ui)
	{
		pkEntity = m_pkGameObjectEntities.GetAt(ui);
		if( pkEntity )
		{
            pkEntity->Update(NULL, CMap::GetTime(), const_cast<NiEntityErrorInterface *>(CMap::Get()->GetErrorHandler()), pkAssetManager);
			pkEntity->BuildVisibleSet(pkRenderingContext, NULL);
		}
	}
}

// 计算小地图
void CTile::CalcMiniMap(NiEntityRenderingContext* pkRenderingContext, unsigned int uiSize)
{
    CMap* pkMap = CMap::Get();
    if(pkMap == NULL)
        return;

    //禁止水面反射,折射
    CWater::SetRREnable(false);

	// Turn Off Terrain Shadow
	bool bShowShadowChanged = CTerrainShader::ShowShadow(false);
	bool bShowGridInfoChanged = CTerrainShader::ShowGridInfo(false);
	bool bShowWireframeChanged = CTerrainShader::ShowWireframe(false);
	bool bShowAreaInfoChanged = CTerrainShader::ShowAreaInfo(false);
    bool bFogEnableChanged = CTerrainLightManager::SetFogEnable(false);

	// Get Renderer
	NiRenderer* pkRenderer = pkRenderingContext->m_pkRenderer;

	// Create Texture & RenderTarget
	NiTexture::FormatPrefs kPrefs;
	kPrefs.m_ePixelLayout = NiTexture::FormatPrefs::TRUE_COLOR_32;
	kPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
	kPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::SMOOTH;
	NiRenderedTexture* pkTexture;
	pkTexture = NiRenderedTexture::Create(uiSize, uiSize, pkRenderer, kPrefs);
    NiDepthStencilBufferPtr spDSB = NiDepthStencilBuffer::Create(uiSize, uiSize, pkRenderer, NiPixelFormat::STENCILDEPTH824);
	NiRenderTargetGroupPtr spTarget = NiRenderTargetGroup::Create(pkTexture->GetBuffer(), pkRenderer, spDSB);

	// Build A New Camera
	NiCameraPtr spCamera = NiNew NiCamera;
	NiFrustum kFrustum((float)(-TILE_SIZE / 2), (float)(TILE_SIZE / 2), (float)(TILE_SIZE / 2), (float)(-TILE_SIZE / 2), 1.0f, 1200.0f, true);
	NiMatrix3 kRotation(NiPoint3(0.0f, 0.0f, -1.0f), NiPoint3(0.0f, 1.0f, 0.0f), NiPoint3(1.0f, 0.0f, 0.0f));
	spCamera->SetViewFrustum(kFrustum);
	spCamera->SetViewPort(NiRect<float>(0.0f, 1.0f, 1.0f, 0.0f));
	spCamera->SetWorldRotate(kRotation);
	spCamera->SetWorldTranslate(NiPoint3((float)(TILE_SIZE * GetX() + TILE_SIZE * 0.5), (float)(TILE_SIZE * GetY() + TILE_SIZE * 0.5), 1000.0f));

	// Reset RenderContext
	pkRenderingContext->m_pkCamera = spCamera;
	pkRenderingContext->m_pkCullingProcess->GetVisibleSet()->RemoveAll();

	pkMap->BuildVisibleSet(pkRenderingContext);

	pkRenderer->BeginFrame();
	pkRenderer->BeginUsingRenderTargetGroup(spTarget, NiRenderer::CLEAR_ALL);

	pkRenderer->SetCameraData(spCamera);

	// Draw objects in the visible array.
	NiDrawVisibleArray(spCamera, *pkRenderingContext->m_pkCullingProcess->GetVisibleSet());

    g_pkPathNodeManager->DrawPath(spCamera);

    pkMap->DrawWater(pkRenderingContext);

	pkRenderer->EndUsingRenderTargetGroup();

	NiDX92DBufferData* pkBuffData = NiDynamicCast(NiDX92DBufferData, (NiDX92DBufferData*)spTarget->GetBufferRendererData(0));

	char acPath[NI_MAX_PATH];
	char acPathName[NI_MAX_PATH];
	NiSprintf(acPath, NI_MAX_PATH, "%s\\Data\\World\\MiniMaps\\%s",  TerrainHelper::GetClientPath(), (const char*)pkMap->GetMapName()); 
	NiFile::CreateDirectoryRecursive(acPath);

	NiSprintf(acPathName, NI_MAX_PATH, "%s\\Data\\World\\MiniMaps\\%s\\%s_%d_%d.jpg", TerrainHelper::GetClientPath(), (const char*)pkMap->GetMapName(), (const char*)pkMap->GetMapName(), GetX(), GetY());
	D3DXSaveSurfaceToFileA(acPathName, D3DXIFF_JPG, pkBuffData->GetSurface(), NULL, NULL);

	pkRenderer->EndFrame();
	pkRenderer->DisplayFrame();

	pkRenderingContext->m_pkCamera = NULL;

	// Turn On Terrain Shadow
	if( bShowShadowChanged )
		CTerrainShader::ShowShadow(true);

	if( bShowGridInfoChanged )
		CTerrainShader::ShowGridInfo(true);

	if( bShowWireframeChanged )
		CTerrainShader::ShowWireframe(true);

	if( bShowAreaInfoChanged )
		CTerrainShader::ShowAreaInfo(true);

    if( bFogEnableChanged )
        CTerrainLightManager::SetFogEnable(true);

    //允许反射和折射
    CWater::SetRREnable(true);

    //todo... Save ZoneInfo
}


void CTile::_SDMInit()
{
	m_spStencilProperty = NiNew NiStencilProperty;
	m_spStencilProperty->SetDrawMode(NiStencilProperty::DRAW_BOTH);

	ms_kSceneRootPointerName = "Scene Root Pointer";
	ms_bEditNpc = false;
}

void CTile::_SDMShutdown()
{
	m_spStencilProperty = NULL;
	ms_kSceneRootPointerName = NULL;
}

