
#ifndef TILE_H
#define TILE_H

#include "TerrainLibType.h"
#include "TagStream.h"
#include "Chunk.h"

#define INVALID_TEXTURE 0xFFFFFFFF


struct CTileTexture : public NiMemObject
{
	NiFixedString kName;
	NiTexture* pkTexture;
	float fUScale;
	float fVScale;

	CTileTexture();
	virtual ~CTileTexture();
};

class TERRAIN_ENTRY CTile : public NiNode
{
public:
	NiDeclareRTTI;

public:
	CTile(int x, int y);
	virtual ~CTile();

	int GetX() const { return m_iX; }
	int GetY() const { return m_iY; }

	bool	Load(const NiFixedString& kMapName);
	bool    Save(const NiFixedString& kMapName);
	
	bool	SaveServerTile(const NiFixedString& kMapName);

	//被创建的时候调用, 用来初始化数据
	void    OnCreate();

	CChunk* GetChunk(float x, float y);
	CChunk* GetChunkByIndex(int iX, int iY);


	CTileTexture* GetTexture(unsigned int uiId);
	CTileTexture* GetTexture(const char* pcTextureName);
    unsigned int GetTextureIndex(const char* pcTextureName);
	unsigned int AddTexture(const char* pcTextureName, float fUScale = 0.f, float fVScale = 0.f);
	void DeleteTexture(const char* pcTextureName);
	void ChangeTexture(const char* pcSrcTextureName, const char* pcDestTextureName);
	void GetTextureNames(NiTObjectArray<NiFixedString>& akTextureNames) const;
	void BuidWaterEdgeMap();

	void SetTextureUVScale(const char* pcTextureName, float fUScale, float fVScale);
	bool GetTextureUVScale(const char* pcTextureName, float& fUScale, float& fVScale);
	bool GetTextureSize(const char* pcTextureName, unsigned int& uiWidth, unsigned int& uiHeight);

	void GetEntities(NiScene *pkScene);
	void AddEntity(NiEntityInterface* pkEntity);
	void RemoveEntity(NiEntityInterface* pkEntity);
	NiEntityInterface* GetEntity(unsigned int uiIndex);
	unsigned int GetEntitiesCount() const { return m_pkModels.GetSize(); }

	unsigned int GetEntityIndex(NiEntityInterface* pkEntity);
	void UpdateEditorInfo();

	void GetBound(CBox& kBox);

	//--------------------------------------------------------------------------------
	//静态阴影计算
	//
	void BlurShadow(float* pfShadow);
	void SplitShadow(float* pfShadow, float fSoftness);
	void CalcShadow(bool bModelShadow, bool bTerrainShadow, float fSoftness);
	
	//--------------------------------------------------------------------------------
	//FastCalcShadow
	//
	bool FastCalcShadow(bool bModelShadow, bool bTerrainShadow, float fSoftness);
	void DrawAVObject(NiAVObject* pkAVObject, NiMaterial* pkMaterial);
	
	//清除阴影
	void ClearShadow();


	//-----------------------------------------------------------------------------------
	//计算服务器端用的地形信息
	//
	void CollectCollisionModel(NiTPrimitiveArray<NiAVObject*>& kModelList);
	void SplitGridInfo(unsigned char* pucGridInfo);
	void CalcGridInfo();
	bool FastCalcGridInfo();
	void ClearGridInfo();
	
	void CalcMoveable();
	void ClearMoveable();

    void AddNpcEntity(NiEntityInterface* pkEntity);
	void RemoveNpcEntity(NiEntityInterface* pkEntity);
	NiTPrimitiveArray<NiEntityInterface*>& GetNpcEntities() { return m_pkNpcEntities; }
	void BuildNpcVisibleSet(NiEntityRenderingContext* pkRenderingContext);
    
    void AddGameObject(NiEntityInterface* pkEntity);
    void RemoveGameObject(NiEntityInterface* pkEntity);
    NiTPrimitiveArray<NiEntityInterface*>& GetGameObjectEntities() { return m_pkGameObjectEntities; }
    void BuildGameObjectVisibleSet(NiEntityRenderingContext* pkRenderingContext);

    void CalcMiniMap(NiEntityRenderingContext* pkRenderingContext, unsigned int uiSize);

	bool IsDirty() const;
	void SetDirty( bool bDirty );

	static void SetEditNpc(bool bEditNpc) { ms_bEditNpc = bEditNpc; }
	static bool IsEditNpc() { return ms_bEditNpc; }
	bool LoadNpc();
	void SaveNpc();
    bool LoadGameObjects();
    void SaveGameObjects();

	void OnWaterChanged(unsigned int uiWaterIndex);
	void Precache();

	unsigned int GetVersion() const { return m_uiVersion; }

	//VMap Funtion
	void SaveVMap();

	static void _SDMInit();
	static void _SDMShutdown();

private:
	int	m_iX;
	int	m_iY;
	bool m_bDirty;
	unsigned int m_uiVersion;

	std::string m_strTitlePath;
	std::string m_strMapName;

	NiTPrimitiveArray<CTileTexture*> m_pkTextures;
	NiTPrimitiveArray<NiEntityInterface*> m_pkModels;

	NiTPrimitiveArray<NiEntityInterface*> m_pkNpcEntities;
    NiTPrimitiveArray<NiEntityInterface*> m_pkGameObjectEntities;

	CChunk	m_kChunks[CHUNK_COUNT][CHUNK_COUNT];

	static NiStencilPropertyPtr m_spStencilProperty;
	static NiFixedString ms_kSceneRootPointerName;

	static bool ms_bEditNpc;
};

#endif