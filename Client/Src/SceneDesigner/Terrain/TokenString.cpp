#include "TerrainPCH.h"
#include "TokenString.h"

void CTokenString::ParseTokens(const char* pcString)
{
    m_kTokens.RemoveAll();
    char acToken[1024];
    const char* ptr = pcString;
    unsigned int ui = 0;
    for(; *ptr; ++ptr)
    {
        if(*ptr == ' ') //�ո���Ϊ�ָ��
        {
            if(ui != 0)
            {
                acToken[ui] = '\0';
                m_kTokens.Add(acToken);
                ui = 0;
            }
        }
        else
        {
            acToken[ui++] = *ptr;
        }
    }

    if(ui != 0)
    {
        acToken[ui] = '\0';
        m_kTokens.Add(acToken);
    }
}

unsigned int CTokenString::GetSize()
{
    return m_kTokens.GetSize();
}

NiFixedString CTokenString::GetToken(unsigned int uiIndex)
{
    NIASSERT(uiIndex < GetSize());

    return m_kTokens.GetAt(uiIndex);
}