#pragma once
#include "TerrainLibType.h"

class TERRAIN_ENTRY CTokenString : public NiMemObject
{
public:
    void ParseTokens(const char* pcString);
    unsigned int GetSize();
    NiFixedString GetToken(unsigned int uiIndex);

private:
    NiTObjectArray<NiFixedString> m_kTokens;
};