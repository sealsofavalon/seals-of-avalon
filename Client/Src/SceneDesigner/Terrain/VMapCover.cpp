// Class Map
// Holder for all instances of each mapmgr, handles transferring
// players between, and template holding.

//#include "Stream/FileStream.h"

#include "TerrainPCH.h"
#include "TagStream.h"
#include "VMapCover.h"
#include <Windows.h>
#include <NiExternalAssetManager.h>
#include <NiEntityInterface.h>
#include "Map.h"
#include "Tile.h"
#include "EntityInfoComponent.h"
#include <G3D/Vector3.h>
#include <G3D/Triangle.h>
#include "VMap/AABSPTree.h"
#include "VMap/SubModel.h"
#include "VMap/TileAssembler.h"

#include <string.h>
#include "crc32.h"

using namespace G3D;
using namespace VMAP;

bool readNifData(NiAVObject* pRootMesh,  VMAP::ModelPosition& pModelPosition, AABSPTree<VMAP::SubModel *> *pMainTree)
{

	G3D::uint32 groups;
	char blockId[5];
	blockId[4] = 0;
	int blocksize;

	int nSizeMatrix = sizeof( NiMatrix3 );
	int nSizePoint3 = sizeof( NiPoint3 );
	int nChunkHeaderSize = sizeof( CChunkHeader );


	int trianglecount = 0;
	int startgroup = 0;
	int endgroup = INT_MAX;

	std::vector<NiAVObject*> vMeshList;

	CollectCollisionNode(pRootMesh, vMeshList);

	int nMeshSize = vMeshList.size();

	for(int g=0;g<(int)nMeshSize;++g)
	{
		//// group MUST NOT have more then 65536 indexes !! Array will have a problem with that !! (strange ...)
		Array<Vector3> tempVertexArray;

		AABSPTree<Triangle> *gtree = new AABSPTree<Triangle>();
		NiAVObject* pObj = vMeshList[g];

		NiTriBasedGeom* pMeshData = (NiTriBasedGeom*)pObj;


		//// ---- vectors
		unsigned int nVCount = pMeshData->GetVertexCount();
		NiPoint3* pVertices = pMeshData->GetVertices();

		NiTransform kTranse = pMeshData->GetWorldTransform();
		NiMatrix3 kMatRotX;
		kMatRotX.MakeXRotation(NI_PI * 0.5f);

		NiMatrix3 kMatRotZ;
		kMatRotZ.MakeZRotation(NI_PI * 0.5f);

		G3D::Vector3 iWorldPos(kTranse.m_Translate.y, kTranse.m_Translate.z, kTranse.m_Translate.x);

		NiMatrix3 kFinalMatrix =  kMatRotX * kMatRotZ * kTranse.m_Rotate;

		for(unsigned int i=0; i<nVCount; i++)
		{
			NiPoint3 kPos = pVertices[i]; 

			kPos = kTranse.m_fScale * (kFinalMatrix * kPos);

			Vector3 v = Vector3(kPos.x, kPos.y, kPos.z);
			////v = pModelPosition.transform(v);

			//float swapy = v.z;
			//v.z = v.y;
			//v.y = swapy;

			tempVertexArray.append(v);
		}


		//TriangleCount
		unsigned short nCount = pMeshData->GetTriangleCount();

		for(unsigned int i=0;i<nCount; ++i)
		{
			unsigned short i0,i1,i2;
			pMeshData->GetTriangleIndices(i, i0, i1, i2);
			Triangle t = Triangle(tempVertexArray[i2], tempVertexArray[i1], tempVertexArray[i0] );

			++trianglecount;
			if(g>= startgroup && g <= endgroup)
			{
				gtree->insert(t);
			}
		}

		if(gtree->size() >0)
		{
			gtree->balance();
			SubModel *sm = new SubModel(gtree);

			sm->setBasePosition( iWorldPos );
			pMainTree->insert(sm);
		}

		tempVertexArray.clear();
		delete gtree;
	}

	return true;
}

bool fillModelIntoTree( AABSPTree<VMAP::SubModel*> *pMainTree, const G3D::Vector3& pBasePos, NiAVObject* pMeshData)
{
	ModelPosition modelPosition;

	float fScale = pMeshData->GetWorldScale();
	NiPoint3 kPos = pMeshData->GetWorldTranslate();
	NiMatrix3 kRot = pMeshData->GetWorldRotate();
	NiPoint3 kDir(0.0f, 0.0f, 0.0f);
	kRot.GetCol(0, kDir);

	modelPosition.iPos = Vector3(kPos.y, kPos.z, kPos.x);
	//modelPosition.iDir = Vector3(kDir.x, kDir.z, kDir.y);
	modelPosition.iScale = fScale;
	// all should be relative to object base position
	modelPosition.moveToBasePos(pBasePos);

	modelPosition.init();

	return readNifData(pMeshData,  modelPosition, pMainTree);
}


VMapChunk::VMapChunk()
{

}

void VMapChunk::Set( int x, int y, VMapTile* pTile )
{
	m_pTile = pTile;

	m_iX = x;
	m_iY = y;
}

bool VMapChunk::Load(NiBinaryStream* pkStream)
{
	CTagStream kTagStream;
	while( kTagStream.ReadFromStream( pkStream ) )
	{
		switch( kTagStream.GetTag() )
		{
		case 'MODL':
			{
				int iCurrentTileX = 0;//m_pkTile->GetX();
				int iCurrentTileY = 0;//m_pkTile->GetY();
				int iTileX;
				int iTileY;

				ModelRef kRef;
				unsigned int uiCount;
				kTagStream.Read(&uiCount, sizeof(uiCount));
				for(unsigned int ui = 0; ui < uiCount; ++ui)
				{
					kTagStream.Read(&iTileX, sizeof(iTileX));
					kTagStream.Read(&iTileY, sizeof(iTileY));
					kTagStream.Read(&kRef.uiIndex, sizeof(kRef.uiIndex));

					kRef.iTileX = iTileX + iCurrentTileX;
					kRef.iTileY = iTileY + iCurrentTileY;

					m_kModelList.push_back(kRef);
				}
			}
			break;
		}
	}

	return true;
}

extern char* g_mapLoadBuff;

VMapTile::~VMapTile()
{
	for( unsigned int i = 0; i < m_pkModels.GetSize(); i++ )
	{
		if( m_pkModels.GetAt(i) )
			m_pkModels.GetAt(i)->RemoveReference();
	}
}

void VMapTile::AddEntity(NiEntityInterface* pkEntity)
{
	for( unsigned int i = 0; i < m_pkModels.GetSize(); i++ )
	{
		if( pkEntity == m_pkModels.GetAt(i) )
			return;
	}

	pkEntity->AddReference();
	m_pkModels.Add(pkEntity);

}

NiEntityInterface* VMapTile::GetEntity(unsigned int uiIndex)
{
	if( uiIndex < m_pkModels.GetSize() )
	{
		return m_pkModels.GetAt(uiIndex);
	}

	return NULL;
}

void VMapTile::SaveToVMap(std::string& strMapName)
{
	ModelContainer *modelContainer = 0;
	CMap* pkMap = CMap::Get();
	unsigned int uiFrameCount = pkMap->GetFrameCount();
	uiFrameCount++;

	Vector3 basepos = Vector3(0,0,0);
	AABSPTree<SubModel *>* mainTree = new AABSPTree<SubModel *>();


	for( int x = 0; x < CHUNK_COUNT; x++ )
	{
		for( int y = 0; y < CHUNK_COUNT; y++ )
		{
			ModelList kModleList = m_Chunks[x][y].GetModelList();

			// Scene Object
			NiEntityInterface* pkEntity;
			NiObject* pkSceneRootPointer;
			NiAVObject* pkSceneRoot;
			NiExternalAssetManager* pkAssetManager = CMap::GetAssetManager();


			CEntityInfoComponent* pkEntityInfoComponent;
			for( ModelList::const_iterator itr = kModleList.begin(); itr != kModleList.end(); ++itr )
			{
				pkEntity = GetEntity(itr->uiIndex);
				if( !pkEntity || pkEntity->GetHidden() )
					continue;

				pkEntityInfoComponent = (CEntityInfoComponent*)pkEntity->GetComponentByTemplateID(CEntityInfoComponent::ms_kTemplateID);
				if( pkEntityInfoComponent == NULL )
					continue;

				//已经加入了VisibleArray
				if( pkEntityInfoComponent->GetFrameCount() == uiFrameCount )
					continue;

				pkEntityInfoComponent->SetFrameCount(uiFrameCount);
				pkEntity->Update(NULL, pkMap->GetTime(), pkMap->GetErrorHandler(), pkAssetManager);

				pkSceneRootPointer = NULL;
				pkSceneRoot = NULL;
				if (pkEntity->GetPropertyData("Scene Root Pointer", pkSceneRootPointer))
				{
					NiFixedString kPath;
					pkEntity->GetPropertyData("NIF File Path", kPath);
					pkSceneRoot = NiDynamicCast(NiAVObject, pkSceneRootPointer);
					if(pkSceneRoot)
					{
						fillModelIntoTree(mainTree, basepos, pkSceneRoot);
					}
				}
			}//
		}//for
	}//for

	if( mainTree->size() > 0)
	{
		mainTree->balance();
		modelContainer = new ModelContainer(mainTree);



		char cVMapFileName[NI_MAX_PATH];
		char cVdirFileName[NI_MAX_PATH];
		char cVMapName[NI_MAX_PATH];
		unsigned int n = crc32( (const unsigned char*)strMapName.c_str(), strMapName.length());

		NiSprintf(cVMapFileName, NI_MAX_PATH, "VMap\\%u_%d_%d.vmap", n, m_iX, m_iY); 
		NiSprintf(cVMapName, NI_MAX_PATH, "%u_%d_%d.vmap", n, m_iX, m_iY); 
		NiSprintf(cVdirFileName, NI_MAX_PATH, "VMap\\%u_%d_%d.vmdir", n, m_iX, m_iY); 


		//std::string strVMapName = "E:\\G3D-7.00-src\\G3D\\data-files\\vmaps\\000_26_32.vmap";

		//int pos = strVMapName.find(".tle");
		//strVMapName.erase(pos);
		//strVMapName += ".vmp";

		modelContainer->writeFile( cVMapFileName );


		FILE *wf =fopen(cVdirFileName,"w");

		fprintf(wf, "%s", cVMapName);

		fclose(wf);
	}
}

bool VMapTile::Load(const char* pcMapName)
{
	//char filename[1024];
	//sprintf(filename, "Data/Maps/%s/%s_%d_%d.tle", pcMapName, pcMapName, m_iX, m_iY);


	NiFile* pkFile = NiFile::GetFile(pcMapName, NiFile::READ_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return false;
	}

	CTagStream kTagStream;

	while( kTagStream.ReadFromStream( pkFile ) )
	{
		switch( kTagStream.GetTag() )
		{
		case 'VERS':
			kTagStream.Read(&m_uiVersion, sizeof(m_uiVersion));
			kTagStream.SetVersion(m_uiVersion);
			break;

		case 'MODL': //物件模型
			{
				NiExternalAssetManager* pkAssetManager = CMap::GetAssetManager();
				NiEntityErrorInterface* pkErrors = const_cast<NiEntityErrorInterface *>(CMap::Get()->GetErrorHandler());

				NiEntityInterface* pkEntity;
				NiPoint3 kPos;
				VMapChunk* pkChunk;
				kTagStream.SetBase(float(m_iX*TILE_SIZE), float(m_iY*TILE_SIZE));
				while( (pkEntity = kTagStream.ReadEntity()) != NULL )
				{
					pkEntity->Update(NULL, 0.f, pkErrors, pkAssetManager);
					AddEntity(pkEntity);
				}
			}
			break;

		case 'CHNK':
			{
				int x, y;
				kTagStream.Read(&x, sizeof(int));
				kTagStream.Read(&y, sizeof(int));

				assert( x >= 0 && x < CHUNK_COUNT );
				assert( y >= 0 && y < CHUNK_COUNT );

				m_Chunks[x][y].Set(x, y, this);
				m_Chunks[x][y].Load( &kTagStream );
			}
			break;
		}
	}

	NiDelete pkFile;

	return true;
}


VMapCover::VMapCover()
{
}

void VMapCover::StartCoverVMaps(const char* szSrc, const char* szDest)
{
	std::vector<std::string> vFileList;

	SearchFile(szSrc, vFileList);


	int nSize = vFileList.size();

	for (int i=0; i<nSize; i++)
	{
		std::string strFileName = vFileList[i];
		coverTitleToVMaps(strFileName.c_str()); 
	}
}

void GetNameAndXY(const char* szTitleName, std::string& strMapName, unsigned int& x, unsigned int& y)
{
	std::string strTempName = szTitleName;

	int nPos = strTempName.find(".tle");
	strTempName.erase(nPos);

	nPos = strTempName.find_last_of("_");
	std::string strY = strTempName.substr( nPos + 1);
	strTempName.erase(nPos);
	y = ::atoi( strY.c_str() );
	
	nPos = strTempName.find_last_of("_");
	std::string strX = strTempName.substr(nPos + 1);
	strTempName.erase(nPos);
	x = ::atoi( strX.c_str() );

	nPos = strTempName.find_last_of("\\");
	strMapName = strTempName.substr(nPos + 1);
}

void VMapCover::coverTitleToVMaps(const char* szTitleName)
{
	std::string strMapName;
	unsigned int x, y;
	GetNameAndXY(szTitleName, strMapName, x, y);

	VMapTile* pNewTitle = new VMapTile(x, y);
	pNewTitle->Load(szTitleName);
	pNewTitle->SaveToVMap(strMapName);

	delete pNewTitle;
}

void VMapCover::SearchFile(const char* rootDir, std::vector<std::string>& vFileList)
{
	char fname[MAX_PATH];
	ZeroMemory(fname, MAX_PATH);
	WIN32_FIND_DATA fd;
	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));
	HANDLE hSearch;
	char filePathName[256];
	char tmpPath[256];
	ZeroMemory(filePathName, 256);
	ZeroMemory(tmpPath, 256); 

	strcpy_s(filePathName, rootDir);

	BOOL bSearchFinished = FALSE;
	if( filePathName[strlen(filePathName) -1] != '\\' )
	{
		strcat_s(filePathName, "\\");
	}
	strcat_s(filePathName, "*.*");
	hSearch = FindFirstFile(filePathName, &fd);
	//Is directory
	if( (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		&& strcmp(fd.cFileName, ".") && strcmp(fd.cFileName, "..") )  
	{
		strcpy_s(tmpPath, rootDir);
		strcat_s(tmpPath,"\\");
		strcat_s(tmpPath, fd.cFileName);
		SearchFile(tmpPath, vFileList);
	}
	else if( strcmp(fd.cFileName, ".") && strcmp(fd.cFileName, "..") )
	{
		sprintf_s(fname, "%-50.50s", fd.cFileName);
		printf("%s\n" , fname);
	}

	while( !bSearchFinished )
	{
		if( FindNextFile(hSearch, &fd) )
		{
			if( (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				&& strcmp(fd.cFileName, ".") && strcmp(fd.cFileName, "..") )  
			{
				strcpy_s(tmpPath, rootDir);
				strcat_s(tmpPath,"\\");
				strcat_s(tmpPath, fd.cFileName);
				SearchFile(tmpPath, vFileList);
			}
			else if( strcmp(fd.cFileName, ".") && strcmp(fd.cFileName, "..") )
			{
				std::string strFile = rootDir;
				strFile += "\\";
				strFile += fd.cFileName;
				int pos = strFile.find(".tle");
				
				if ( pos != -1 && (pos + 4 == strFile.length()) )
				{
					sprintf_s(fname, "%s", fd.cFileName);
					printf("%s\n" , fname);
					vFileList.push_back( strFile );
				}
			}
		}
		else
		{
			if( GetLastError() == ERROR_NO_MORE_FILES )       //Normal Finished
			{
				bSearchFinished = TRUE;
			}
			else
				bSearchFinished = TRUE;     //Terminate Search
		}
	}
	FindClose(hSearch);
}