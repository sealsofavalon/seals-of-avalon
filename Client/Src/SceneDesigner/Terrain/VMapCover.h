#ifndef __MAP_H
#define __MAP_H

class CStream;
class CTile;

#include <vector>
#include <assert.h>
#include <list>
#include <NiExternalAssetManager.h>
#include <NiDefaultErrorHandler.h>
#include <NiTArray.h>
#include <NiEntityInterface.h>
#include "Chunk.h"

class VMapTile;

class VMapChunk
{
public:
	VMapChunk();

	bool Load(NiBinaryStream* pkStream);
	void Set( int x, int y, VMapTile* pTile );

	int GetX() const { return m_iX; }
	int GetY() const { return m_iY; }

	bool GetVertexIndex(float x, float y, int& iIndex) const;
	const ModelList& GetModelList() const { return m_kModelList; }


private:
	int m_iX;
	int m_iY;
	VMapTile* m_pTile;
	ModelList		m_kModelList;
};

class VMapTile
{
public:
	VMapTile( int x, int y )
		:m_iX(x)
		,m_iY(y)
	{}

	~VMapTile();

	int GetX() const { return m_iX; }
	int GetY() const { return m_iY; }

	bool Load(const char* pcMapName);
	unsigned int GetVersion() { return m_uiVersion; }

	void AddEntity(NiEntityInterface* pkEntity);
	NiEntityInterface* GetEntity(unsigned int uiIndex);

	void SaveToVMap(std::string& strMapName);

private:
	int m_iX;
	int m_iY;
	unsigned int m_uiVersion;
	NiTPrimitiveArray<NiEntityInterface*> m_pkModels;
	VMapChunk	m_Chunks[CHUNK_COUNT][CHUNK_COUNT];
};

class VMapCover
{
public:
	VMapCover();

	void StartCoverVMaps(const char* szSrc, const char* szDest);

private:
	void SearchFile(const char* rootDir, std::vector<std::string>& vFileList);
	void coverTitleToVMaps(const char* szTitleName);
};

#endif
