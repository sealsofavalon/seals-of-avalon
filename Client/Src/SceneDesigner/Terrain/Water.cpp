#include "TerrainPCH.h"

#include "Tile.h"
#include "WaterShader.h"
#include "Water.h"
#include "TerrainLight.h"
#include "TerrainHelper.h"
#include "Map.h"

CWaterShader* CWater::ms_pkDefaultWaterShader = NULL;

NiTexture* CWater::ms_pkRefractTexture = NULL;

NiTexture* CWater::ms_pkReflectTexture = NULL;
NiRenderTargetGroup* CWater::ms_pkReflectTarget = NULL;

NiTexture* CWater::ms_pkBackTexture = NULL;
NiTexture* CWater::ms_pkNormalTexture = NULL;
NiTexture* CWater::ms_pkDuDvTexture = NULL;

bool CWater::ms_bRREnable;

void CWater::_SDMInit()
{
    ms_pkDefaultWaterShader = NULL;
    ms_bRREnable = true;
}

void CWater::_SDMShutdown()
{
    if(ms_pkDefaultWaterShader)
	    ms_pkDefaultWaterShader->DecRefCount();

	if(ms_pkReflectTexture)
		ms_pkReflectTexture->DecRefCount();

    if(ms_pkReflectTarget)
        ms_pkReflectTarget->DecRefCount();

    if(ms_pkRefractTexture)
        ms_pkRefractTexture->DecRefCount();

    if(ms_pkBackTexture)
        ms_pkBackTexture->DecRefCount();

    if(ms_pkNormalTexture)
        ms_pkNormalTexture->DecRefCount();

    if(ms_pkDuDvTexture)
        ms_pkDuDvTexture->DecRefCount();
}

CWater::CWater(float x, float y, CTile* pkTile)
	:m_kBasePoint(x, y, 0.f)
	,m_bIndicesDirty(true)
	,m_pkTile(pkTile)
	,m_uiShaderIndex(0)
{
	m_pkPositionList = NiNew NiPoint3[WATER_VERTEX_COUNT*WATER_VERTEX_COUNT];
	m_pusTriList = NiAlloc(unsigned short, (WATER_VERTEX_COUNT - 1)*(WATER_VERTEX_COUNT-1)*6);
	
	memset(m_pusTriList, 0, (WATER_VERTEX_COUNT - 1)*(WATER_VERTEX_COUNT-1)*6*sizeof(unsigned short));
	memset(m_acFlags, 0, sizeof(m_acFlags));

	for( int x = 0; x < WATER_VERTEX_COUNT; x++ )
	{
		for( int y = 0; y < WATER_VERTEX_COUNT; y++ )
		{
			m_pkPositionList[x*WATER_VERTEX_COUNT + y] = NiPoint3(float(x * UNIT_SIZE), float(y * UNIT_SIZE), 0.f);
		}
	}
}

CWater::~CWater()
{
	if(m_spGeometry == NULL)
	{
		NiDelete[] m_pkPositionList;
		NiFree(m_pusTriList);
	}
}

void CWater::Load(NiBinaryStream* pkStream)
{
	pkStream->Read(m_acFlags, sizeof(m_acFlags));

	float afHeights[WATER_VERTEX_COUNT][WATER_VERTEX_COUNT];
	pkStream->Read(afHeights, sizeof(afHeights));

	int iIndex;
	for( int x = 0; x < WATER_VERTEX_COUNT; x++ )
	{
		for( int y = 0; y < WATER_VERTEX_COUNT; y++ )
		{
			iIndex = x*WATER_VERTEX_COUNT + y;
			m_pkPositionList[iIndex].z = afHeights[x][y];			
		}
	}

	//兼容之前版本的水面
	if( pkStream->Read(&m_uiShaderIndex, sizeof(m_uiShaderIndex)) != sizeof(m_uiShaderIndex) )
	{
		m_uiShaderIndex = 0;
	}
}

void CWater::Save(NiBinaryStream* pkStream)
{
	pkStream->Write(m_acFlags, sizeof(m_acFlags));

	float afHeights[WATER_VERTEX_COUNT][WATER_VERTEX_COUNT];
	for( int x = 0; x < WATER_VERTEX_COUNT; x++ )
	{
		for( int y = 0; y < WATER_VERTEX_COUNT; y++ )
		{
			afHeights[x][y] = m_pkPositionList[x*WATER_VERTEX_COUNT + y].z;
		}
	}

	pkStream->Write(afHeights, sizeof(afHeights));
	pkStream->Write(&m_uiShaderIndex, sizeof(m_uiShaderIndex));
}

float CWater::GetHeight(float x, float y)
{
	float h;
	if( GetHeight(x, y, h) )
		return h;

	return 10.f;
}

bool CWater::GetHeight(float x, float y, float& h)
{
	x -= int(m_kBasePoint.x);
	y -= int(m_kBasePoint.y);

	unsigned int X = unsigned int(x)/UNIT_SIZE;
	unsigned int Y = unsigned int(y)/UNIT_SIZE;

	if( X >= WATER_VERTEX_COUNT || Y >= WATER_VERTEX_COUNT )
		return false;

    if( !NIBOOL_IS_TRUE(m_acFlags[X][Y]) )
        return false;

	h = m_pkPositionList[X * VERTEX_COUNT + Y].z;

	return true;
}

void CWater::SetHeight(float x, float y, float h)
{
	x -= int(m_kBasePoint.x);
	y -= int(m_kBasePoint.y);

	unsigned int X = unsigned int(x)/UNIT_SIZE;
	unsigned int Y = unsigned int(y)/UNIT_SIZE;

	if( X >= WATER_VERTEX_COUNT || Y >= WATER_VERTEX_COUNT )
		return;

	if( m_pkPositionList[X * VERTEX_COUNT + Y].z == h )
		return;

	m_pkPositionList[X * VERTEX_COUNT + Y].z = h;

	if( m_spGeometry )
		m_spGeometry->GetModelData()->MarkAsChanged(NiGeometryData::VERTEX_MASK);

	m_pkTile->SetDirty(true);
}

void CWater::SetFlag(float x, float y, bool bHasWater)
{
	x -= int(m_kBasePoint.x);
	y -= int(m_kBasePoint.y);

	unsigned int X = unsigned int(x)/UNIT_SIZE;
	unsigned int Y = unsigned int(y)/UNIT_SIZE;

	if( X >= WATER_VERTEX_COUNT - 1 || Y >= WATER_VERTEX_COUNT - 1 )
		return;

	if( NIBOOL_IS_TRUE(m_acFlags[X][Y]) && bHasWater )
		return;

	if( !NIBOOL_IS_TRUE(m_acFlags[X][Y]) && !bHasWater )
		return;

	m_acFlags[X][Y] = bHasWater;

	m_bIndicesDirty = true;

	m_pkTile->SetDirty(true);
}

bool CWater::GetFlag(float x, float y)
{
	x -= int(m_kBasePoint.x);
	y -= int(m_kBasePoint.y);

	unsigned int X = unsigned int(x)/UNIT_SIZE;
	unsigned int Y = unsigned int(y)/UNIT_SIZE;

	if( X >= WATER_VERTEX_COUNT - 1 || Y >= WATER_VERTEX_COUNT - 1 )
		return false;

	return NIBOOL_IS_TRUE(m_acFlags[X][Y]);
}

void CWater::SetShaderIndex(unsigned int uiShaderIndex)
{
	if(m_uiShaderIndex != uiShaderIndex)
	{
		m_uiShaderIndex = uiShaderIndex;
		m_pkTile->SetDirty(true);

		OnShaderChanged();
	}
}

void CWater::OnShaderChanged()
{
	if(m_spGeometry)
	{
		CMap* pkMap = CMap::Get();
		NIASSERT(pkMap);

		CWaterShader* pkShader = pkMap->GetWaterShader(m_uiShaderIndex);
		NIASSERT(pkShader);
        m_spGeometry->SetShader(pkShader);
        m_spGeometry->DetachProperty(m_spGeometry->GetProperty(NiProperty::ALPHA));
        m_spGeometry->AttachProperty(pkShader->GetAlphaProperty());
	}
}

//重新建立 Indices
void CWater::BuildIndices()
{
	if( !m_bIndicesDirty )
		return;

	m_fAVGHeight = 0.f;

	m_bIndicesDirty = false;

	m_usTriangles = 0;

	unsigned short usIdx;
	unsigned short* pusTriList = m_pusTriList;
	for( int x = 0; x < WATER_VERTEX_COUNT - 1; x++ )
	{
		for( int y = 0; y < WATER_VERTEX_COUNT - 1; y++ )
		{
			if( NIBOOL_IS_TRUE(m_acFlags[x][y]) )
			{
				m_usTriangles += 2;

				usIdx = unsigned short(x *  WATER_VERTEX_COUNT + y);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;

				usIdx = unsigned short((x + 1) *  WATER_VERTEX_COUNT + y);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;

				usIdx = unsigned short(x *  WATER_VERTEX_COUNT + y + 1);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;

				usIdx = unsigned short(x *  WATER_VERTEX_COUNT + y + 1);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;

				usIdx = unsigned short((x + 1) *  WATER_VERTEX_COUNT + y);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;

				usIdx = unsigned short((x + 1) *  WATER_VERTEX_COUNT + y + 1);
				m_fAVGHeight += m_pkPositionList[usIdx].z;
				*pusTriList++ = usIdx;
			}
		}
	}

	if(m_usTriangles > 0)
		m_fAVGHeight /= (m_usTriangles*3);

	if( m_spGeometry )
	{
		CWaterTriData* pkModelData = (CWaterTriData*)m_spGeometry->GetModelData();
		pkModelData->Replace(m_usTriangles, m_pusTriList);
		pkModelData->MarkAsChanged(NiTriBasedGeomData::TRIANGLE_INDEX_MASK|NiTriBasedGeomData::TRIANGLE_COUNT_MASK);
	}
}


void CWater::CreateGeometry()
{
	if( m_spGeometry )
		return;

	CMap* pkMap = CMap::Get();
	NIASSERT(pkMap);

	CWaterShader* pkShader = GetShader();
	CWaterTriData* pkWaterTriData = NiNew CWaterTriData(
		WATER_VERTEX_COUNT * WATER_VERTEX_COUNT,
		m_pkPositionList, 
		NULL, 
		NULL,
		NULL,
		0,
		NiGeometryData::NBT_METHOD_NONE,
		m_usTriangles,
		m_pusTriList);

	m_spGeometry = NiNew NiTriShape(pkWaterTriData);
	m_spGeometry->SetTranslate(m_kBasePoint);
    m_spGeometry->SetShader(pkShader);
    m_spGeometry->AttachProperty(pkShader->GetAlphaProperty());

	m_spGeometry->UpdateProperties();
	m_spGeometry->Update(0.f, false);
}

//是不是有水
bool CWater::IsValid() const
{
	for( int x = 0; x < WATER_VERTEX_COUNT - 1; x++ )
	{
		for( int y = 0; y < WATER_VERTEX_COUNT - 1; y++ )
		{
			if( m_acFlags[x][y] )
				return true;
		}
	}

	return false;
}

float CWater::GetValidHeight() const
{
    for(int x = 0; x < WATER_VERTEX_COUNT - 1; x++)
    {
        for(int y = 0; y < WATER_VERTEX_COUNT - 1; y++)
        {
            if( m_acFlags[x][y] )
                return m_pkPositionList[x * VERTEX_COUNT + y].z;
        }
    }

	return 0.f;
}

CWaterShader* CWater::GetShader() const
{
    CMap* pkMap = CMap::Get();
    NIASSERT(pkMap);

    CWaterShader* pkWaterShader = pkMap->GetWaterShader(m_uiShaderIndex);
    if(pkWaterShader)
        return pkWaterShader;

    //Shader 有可能被删除了, 返回默认Shader
    if(ms_pkDefaultWaterShader == NULL)
    {
        ms_pkDefaultWaterShader = NiNew CWaterShader(MAX_WATER_COUNT);
        ms_pkDefaultWaterShader->SetTexturePathName("Data\\Textures\\Water\\river\\lake_a.");
        ms_pkDefaultWaterShader->IncRefCount();
    }
    return ms_pkDefaultWaterShader;
}

void CWater::BuildVisibleSet(CCullingProcess& kCuller)
{
	BuildIndices();
	CreateGeometry();
    if(m_spGeometry == NULL)
        return;

    NiGeometryData* pkGeometryData = m_spGeometry->GetModelData();
    if( pkGeometryData->GetRevisionID() )
    {
        pkGeometryData->GetBound().ComputeFromData(VERTEX_COUNT * VERTEX_COUNT, m_pkPositionList);
        m_spGeometry->Update(0.f, false);
    }

    const NiFrustumPlanes& kPlanes = kCuller.GetFrustumPlanes();

    unsigned int i;
    for (i = 0; i < NiFrustumPlanes::MAX_PLANES; ++i)
    {
        int iSide = m_spGeometry->GetWorldBound().WhichSide(kPlanes.GetPlane(i));

        if (iSide == NiPlane::NEGATIVE_SIDE)
        {
            // The object is not visible since it is on the negative
            // side of the plane.
            break;
        }
    }

    if( i == NiFrustumPlanes::MAX_PLANES )
        kCuller.OnWaterVisible(this);
}

void CWater::Precache()
{
	BuildIndices();
	CreateGeometry();
    if(m_spGeometry == NULL)
        return;

	NiGeometryData* pkData = m_spGeometry->GetModelData();

	NiGeometryGroup* pkGroup = NiUnsharedGeometryGroup::Create();
	NiGeometryGroupManager* pkGeometryGroupManager = NiGeometryGroupManager::GetManager();
	pkGeometryGroupManager->AddObjectToGroup(pkGroup, pkData);

	GetShader()->PrepareGeometryForRendering(m_spGeometry, NULL, (NiGeometryBufferData*)pkData->GetRendererData(), NULL);
}


void CWater::SetRefractTexture(NiTexture* pkTexture)
{
    if(ms_pkRefractTexture)
        ms_pkRefractTexture->DecRefCount();

    ms_pkRefractTexture = pkTexture;

    if(ms_pkRefractTexture)
        ms_pkRefractTexture->IncRefCount();
}

NiTexture* CWater::GetRefractTexture()
{
    return ms_pkRefractTexture;
}

void CWater::SetReflectTexture(NiTexture* pkTexture)
{
	if(ms_pkReflectTexture)
		ms_pkReflectTexture->DecRefCount();

	ms_pkReflectTexture = pkTexture;

	if(ms_pkReflectTexture)
		ms_pkReflectTexture->IncRefCount();
}

NiTexture* CWater::GetReflectTexture()
{
    return ms_pkReflectTexture;
}

void CWater::SetReflectTarget(NiRenderTargetGroup* pkTargetGroup)
{
    if(ms_pkReflectTarget)
        ms_pkReflectTarget->DecRefCount();

    ms_pkReflectTarget = pkTargetGroup;

    if(ms_pkReflectTarget)
        ms_pkReflectTarget->IncRefCount();
}

NiRenderTargetGroup* CWater::GetReflectTarget()
{
    return ms_pkReflectTarget; 
}

void CWater::SetBackTexture(NiTexture* pkTexture)
{
    if(ms_pkBackTexture)
        ms_pkBackTexture->DecRefCount();

    ms_pkBackTexture = pkTexture;

    if(ms_pkBackTexture)
        ms_pkBackTexture->IncRefCount();
}

NiTexture* CWater::GetBackTexture()
{
    return ms_pkBackTexture;
}

NiTexture* CWater::GetNormalTexture()
{
    if(ms_pkNormalTexture == NULL)
    {
        char acFileName[1024];
        NiSprintf(acFileName, sizeof(acFileName), "%s..\\..\\Data\\normal.dds", TerrainHelper::GetExePath());
        NiPath::RemoveDotDots(acFileName);

        ms_pkNormalTexture = NiSourceTexture::Create(acFileName);
        ms_pkNormalTexture->IncRefCount();
    }

    return ms_pkNormalTexture;
}

NiTexture* CWater::GetDuDvTexture()
{
    if(ms_pkDuDvTexture == NULL)
    {
        char acFileName[1024];
        NiSprintf(acFileName, sizeof(acFileName), "%s..\\..\\Data\\dudv.dds", TerrainHelper::GetExePath());
        NiPath::RemoveDotDots(acFileName);

        ms_pkDuDvTexture = NiSourceTexture::Create(acFileName);
        ms_pkDuDvTexture->IncRefCount();
    }

    return ms_pkDuDvTexture;
}