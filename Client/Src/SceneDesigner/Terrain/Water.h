#ifndef WATER_H
#define WATER_H

#include "TerrainLibType.h"

#define WATER_VERTEX_COUNT 9

class TERRAIN_ENTRY CWaterTriData : public NiTriShapeData
{
public:
    CWaterTriData(unsigned short usVertices, NiPoint3* pkVertex,
        NiPoint3* pkNormal, NiColorA* pkColor, NiPoint2* pkTexture,
        unsigned short usNumTextureSets, NiGeometryData::DataFlags eNBTMethod,
        unsigned short usTriangles, unsigned short* pusTriList)
		:NiTriShapeData(usVertices, pkVertex, pkNormal, pkColor, pkTexture,
		usNumTextureSets, eNBTMethod, usTriangles, pusTriList)
	{
	}

	virtual ~CWaterTriData()
	{
		m_pkTexture = NULL;
		m_pkNormal = NULL;
	}
};

class CTile;
class CWaterShader;
class TERRAIN_ENTRY CWater : public NiMemObject
{
private:
	NiPoint3 m_kBasePoint;

	CTile* m_pkTile;

	NiTriShapePtr m_spGeometry;
	
	//顶点
	NiPoint3* m_pkPositionList;

	//顶点索引
	unsigned short* m_pusTriList;
	bool m_bIndicesDirty;

	//3角型的个数
	unsigned short	m_usTriangles;

	float m_fAVGHeight; //水面的平均高度

	//是否有水
	//todo...
	//改成每个Flag用一位来表示
	NiBool m_acFlags[WATER_VERTEX_COUNT - 1][WATER_VERTEX_COUNT - 1];

	unsigned int m_uiShaderIndex; //使用哪个Shader来渲染

	static CWaterShader* ms_pkDefaultWaterShader;

    static NiTexture* ms_pkRefractTexture;

	static NiTexture* ms_pkReflectTexture;
    static NiRenderTargetGroup* ms_pkReflectTarget;
    
    static NiTexture* ms_pkBackTexture;
    static NiTexture* ms_pkNormalTexture;
    static NiTexture* ms_pkDuDvTexture;

    static bool ms_bRREnable;

private:
	void BuildIndices();
	void CreateGeometry();

public:
	CWater(float x, float y, CTile* pkTile);
	virtual ~CWater();

    const NiPoint3& GetBasePoint() const { return m_kBasePoint; }

	void Load(NiBinaryStream* pkStream);
	void Save(NiBinaryStream* pkStream);

	float GetHeight(float x, float y);
	bool GetHeight(float x, float y, float& h);
	void SetHeight(float x, float y, float h);
	void SetFlag(float x, float y, bool bHasWater);
	bool GetFlag(float x, float y);
	void SetShaderIndex(unsigned int uiShaderIndex);
	unsigned int GetShaderIndex() const { return m_uiShaderIndex; }
    CWaterShader* GetShader() const;
    NiTriShape* GetGeometry() const { return m_spGeometry; }

	float GetAVGHeight() const { return m_fAVGHeight; }

	void OnShaderChanged();

	//是不是有水
	bool IsValid() const;

    //第一个有水地方网格的高度
    float GetValidHeight() const;
    
	void BuildVisibleSet(CCullingProcess& kCuller);
	void Precache();

	static void _SDMInit();
	static void _SDMShutdown();
	static CWaterShader* GetDefaultWaterShader() { return ms_pkDefaultWaterShader; }

    static void SetRefractTexture(NiTexture* pkTexture);
    static NiTexture* GetRefractTexture();

    static void SetReflectTexture(NiTexture* pkTexture);
    static NiTexture* GetReflectTexture();

    static void SetReflectTarget(NiRenderTargetGroup* pkTargetGroup);
    static NiRenderTargetGroup* GetReflectTarget();

    static void SetBackTexture(NiTexture* pkTexture);
    static NiTexture* GetBackTexture();

    static NiTexture* GetNormalTexture();
    static NiTexture* GetDuDvTexture();

    static bool GetRREnable() { return ms_bRREnable; }
    static void SetRREnable(bool bEnable) { ms_bRREnable = bEnable; }
};

#endif