
#include "TerrainPCH.h"
#include "WaterEffect.h"
#include "Map.h"
#include "TerrainHelper.h"

NiZBufferProperty* CWaterEffect::ms_pkZBufferProperty;
NiAlphaProperty* CWaterEffect::ms_pkAlpha;
NiTexturingProperty* CWaterEffect::ms_pkWakeTextringProp;
NiTexturingProperty* CWaterEffect::ms_pkSplashTextringProp;

void CWaterEffect::_SDMInit()
{
	ms_pkZBufferProperty = NiNew NiZBufferProperty;
	ms_pkZBufferProperty->IncRefCount();
	ms_pkZBufferProperty->SetZBufferWrite(false);
	ms_pkZBufferProperty->SetZBufferTest(true);

	ms_pkAlpha = NiNew NiAlphaProperty;
	ms_pkAlpha->IncRefCount();
	ms_pkAlpha->SetAlphaBlending(true);
	ms_pkAlpha->SetSrcBlendMode(NiAlphaProperty::ALPHA_SRCALPHA);
	ms_pkAlpha->SetDestBlendMode(NiAlphaProperty::ALPHA_ONE);

	char acFileName[1024];
	NiSprintf(acFileName, sizeof(acFileName), "%s..\\..\\Data\\wake.dds", TerrainHelper::GetExePath());
	NiPath::RemoveDotDots(acFileName);

	ms_pkWakeTextringProp = NiNew NiTexturingProperty;
	ms_pkWakeTextringProp->IncRefCount();
	ms_pkWakeTextringProp->SetBaseTexture(NiSourceTexture::Create(acFileName));

	NiSprintf(acFileName, sizeof(acFileName), "%s..\\..\\Data\\splash.dds", TerrainHelper::GetExePath());
	NiPath::RemoveDotDots(acFileName);

	ms_pkSplashTextringProp = NiNew NiTexturingProperty;
	ms_pkSplashTextringProp->IncRefCount();
	ms_pkSplashTextringProp->SetBaseTexture(NiSourceTexture::Create(acFileName));
}

void CWaterEffect::_SDMShutdown()
{
	ms_pkZBufferProperty->DecRefCount();
	ms_pkAlpha->DecRefCount();
	ms_pkWakeTextringProp->DecRefCount();
	ms_pkSplashTextringProp->DecRefCount();
}

CWaterEffect::CWaterEffect(const NiPoint3& kPos, const NiMatrix3& kRotate, EffectType eType)
	:m_eType(eType)
{
	NiPoint3* pkVertex = NiNew NiPoint3[4];
    NiPoint2* pkTexC = NiNew NiPoint2[4];
    unsigned short* pusConnect = NiAlloc(unsigned short, 2 * 3);
    NiTriShapeDynamicData* pkTriData = NiNew NiTriShapeDynamicData(4, pkVertex, 0, 0, pkTexC, 1, 
        NiGeometryData::NBT_METHOD_NONE, 2, pusConnect);

    m_spGeometry = NiNew NiTriShape(pkTriData);
    m_spGeometry->SetActiveVertexCount(4);
    m_spGeometry->SetActiveTriangleCount(2);

	pkTexC[0] = NiPoint2(0.f, 1.f);
	pkTexC[1] = NiPoint2(1.f, 1.f);
	pkTexC[2] = NiPoint2(1.f, 0.f);
	pkTexC[3] = NiPoint2(0.f, 0.f);

	pusConnect[0] = 0;
	pusConnect[1] = 1;
	pusConnect[2] = 2;

	pusConnect[3] = 2;
	pusConnect[4] = 3;
	pusConnect[5] = 0;

	m_spGeometry->AttachProperty(ms_pkZBufferProperty);
	m_spGeometry->AttachProperty(ms_pkAlpha);
	if (eType == ET_WAKE)
		m_spGeometry->AttachProperty(ms_pkWakeTextringProp);
	else
		m_spGeometry->AttachProperty(ms_pkSplashTextringProp);

	NiMaterialProperty* pkMaterial = NiNew NiMaterialProperty;
	pkMaterial->SetAlpha(1.0f);
	pkMaterial->SetDiffuseColor(NiColor::BLACK);
	pkMaterial->SetEmittance(NiColor::WHITE);
	m_spGeometry->AttachProperty(pkMaterial);

	m_spGeometry->SetTranslate(kPos);
	m_spGeometry->SetRotate(kRotate);

	m_spGeometry->UpdateProperties();
	m_spGeometry->Update(0.f, false);

	m_fBaseTime = 0.f;
}

bool CWaterEffect::Update(float fTime)
{
	float fMaxTime;
	if(m_eType == ET_WAKE)
		fMaxTime = MAX_WAKE_LIFE;
	else
		fMaxTime = MAX_SPLASH_LIFE;

	if(m_fBaseTime == 0.f)
		m_fBaseTime = fTime;

	float fElapsedTime = fTime - m_fBaseTime;
	float r = fElapsedTime / fMaxTime;
	if( r > 1.f || r < 0.f ) //��ֹ��Ч��
		return false;

	float fSize;
	NiPoint3* pkVertices = m_spGeometry->GetVertices();
	if( m_eType == ET_WAKE )
	{
		fSize = 0.5f + 1.3f * r;
		pkVertices[0] = NiPoint3(-fSize, 0.f, 0.f);
		pkVertices[1] = NiPoint3(fSize, 0.f, 0.f);
		pkVertices[2] = NiPoint3(fSize, fSize*2.0f, 0.f);
		pkVertices[3] = NiPoint3(-fSize, fSize*2.0f, 0.f);
	}
	else
	{
		fSize = 0.2f + 0.5f * r;
		pkVertices[0] = NiPoint3(-fSize, -fSize, 0.f);
		pkVertices[1] = NiPoint3( fSize, -fSize, 0.f);
		pkVertices[2] = NiPoint3( fSize,  fSize, 0.f);
		pkVertices[3] = NiPoint3(-fSize,  fSize, 0.f);
	}
	
	NiMaterialProperty* pkMaterialProp = (NiMaterialProperty*)m_spGeometry->GetProperty(NiProperty::MATERIAL);
	NIASSERT(pkMaterialProp);

	float fAlpha = 1.f - r;
	fAlpha *= 2.f;
	fAlpha = NiClamp(fAlpha, 0.f, 1.f);

	pkMaterialProp->SetAlpha(fAlpha);
	m_spGeometry->GetModelData()->MarkAsChanged(NiGeometryData::VERTEX_MASK);
	
	return true;
}

void CWaterEffect::DrawEffect(CCullingProcess& kCuller)
{
	m_spGeometry->Cull(kCuller);
}

CWaterEffectMgr::CWaterEffectMgr()
	:m_kWaterEffects(8, 8)
{
}

CWaterEffectMgr::~CWaterEffectMgr()
{
	RemoveAll();
}

void CWaterEffectMgr::CreateEffect(const NiPoint3& kPos, const NiMatrix3& kRotate, bool bMoving)
{
	m_kWaterEffects.Add(NiNew CWaterEffect(kPos, kRotate, bMoving ? CWaterEffect::ET_WAKE : CWaterEffect::ET_SPLASH));
}

void CWaterEffectMgr::UpdateEffects()
{
	CMap* pkMap = CMap::Get();
	if(pkMap == NULL)
		return;

	float fTime = pkMap->GetTime();
	for(unsigned int ui = 0; ui < m_kWaterEffects.GetSize(); ++ui)
	{
		CWaterEffect* pkWaterEffect = m_kWaterEffects.GetAt(ui);
		if(!pkWaterEffect->Update(fTime))
		{
			m_kWaterEffects.RemoveAt(ui);
			NiDelete pkWaterEffect;
		}
	}

	m_kWaterEffects.Compact();
}

void CWaterEffectMgr::DrawEffects(CCullingProcess& kCuller)
{
	for(unsigned int ui = 0; ui < m_kWaterEffects.GetSize(); ++ui)
	{
		CWaterEffect* pkWaterEffect = m_kWaterEffects.GetAt(ui);
		pkWaterEffect->DrawEffect(kCuller);
	}
}

void CWaterEffectMgr::RemoveAll()
{
	for(unsigned int ui = 0; ui < m_kWaterEffects.GetSize(); ++ui)
	{
		CWaterEffect* pkWaterEffect = m_kWaterEffects.GetAt(ui);
		NiDelete pkWaterEffect;
	}

	m_kWaterEffects.RemoveAll();
}