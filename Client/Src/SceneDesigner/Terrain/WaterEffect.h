#pragma once

#include "WaterEffect.h"
#include "CullingProcess.h"

#define MAX_WAKE_LIFE (2.f)
#define MAX_SPLASH_LIFE (2.f)

class CWaterEffect : public NiRefObject
{
public:
	enum EffectType
	{
		ET_WAKE, //ˮ���ƶ��켣
		ET_SPLASH, //ˮ��ͣ���켣
	};

	CWaterEffect(const NiPoint3& kPos, const NiMatrix3& kRotate, EffectType eType);
	bool Update(float fTime);
	void DrawEffect(CCullingProcess& kCuller);
	

	static void _SDMInit();
	static void _SDMShutdown();

private:
	EffectType m_eType;
	NiTriShapePtr m_spGeometry;
	float m_fBaseTime;

	static NiZBufferProperty* ms_pkZBufferProperty;
	static NiAlphaProperty* ms_pkAlpha;
	static NiTexturingProperty* ms_pkWakeTextringProp;
	static NiTexturingProperty* ms_pkSplashTextringProp;
};

class CWaterEffectMgr : public NiMemObject
{
public:
	CWaterEffectMgr();
	virtual ~CWaterEffectMgr();

	void CreateEffect(const NiPoint3& kPos, const NiMatrix3& kRotate, bool bMoving);
	void UpdateEffects();
	void DrawEffects(CCullingProcess& kCuller);
	void RemoveAll();

private:
	NiTPrimitiveArray<CWaterEffect*> m_kWaterEffects;
};