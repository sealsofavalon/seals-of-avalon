#include "TerrainPCH.h"
#include "TagStream.h"
#include "WaterShader.h"
#include "TerrainHelper.h"
#include "Water.h"
#include "Map.h"

NiShaderDeclaration* CWaterShader::ms_pkDecl;
bool CWaterShader::ms_bRefractPass;
CWaterShader::CWaterShader(int id)
	:m_uiTexureCount(0)
    ,m_pkCurrentTexture(NULL)
	,m_pkChunk(NULL)
{
	Init(id);
}

CWaterShader::~CWaterShader()
{
	Destroy();
}

unsigned int CWaterShader::PreProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
	 m_pkChunk = CMap::Get()->GetChunk(kWorld.m_Translate.x, kWorld.m_Translate.y);

	return 0;
}

unsigned int CWaterShader::PostProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    if(!GetRR())
    {
        m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
        m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
    }

	 m_pkChunk = NULL;
    return 0;
}

unsigned int CWaterShader::SetupTransformations(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        const NiSkinPartition::Partition* pkPartition, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects, 
        const NiTransform& kWorld, const NiBound& kWorldBound)
{
    m_pkD3DRenderer->SetModelTransform(kWorld);
    if(GetRR())
    {
        if(ms_bRefractPass)
        {
            SetupRefractRender();
        }
        else
        {
            SetupRRRender(kWorld.m_Translate);
        }
    }
    else
    {
        SetupRender();
    }

    m_pkD3DRenderState->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

    m_pkD3DRenderState->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
    m_pkD3DRenderState->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);

    return 0;
}

NiGeometryData::RendererData* CWaterShader::PrepareGeometryForRendering(
    NiGeometry* pkGeometry, const NiSkinPartition::Partition* pkPartition,
    NiGeometryData::RendererData* pkRendererData, 
    const NiPropertyState* pkState)
{
   NiGeometryData* pkData = pkGeometry->GetModelData();
   NiGeometryBufferData* pkBuffData = (NiGeometryBufferData*)pkData->GetRendererData();

   if(ms_pkDecl == NULL)
   {
	   ms_pkDecl = m_pkD3DRenderer->CreateShaderDeclaration(1, 1);
	   ms_pkDecl->IncRefCount();

	   ms_pkDecl->SetEntry(0, NiShaderDeclaration::SHADERPARAM_NI_POSITION, NiShaderDeclaration::SPTYPE_FLOAT3, 0);
   }
   m_pkD3DRenderer->PackGeometryBuffer(pkBuffData, pkData, NULL, (NiD3DShaderDeclaration*)ms_pkDecl);

   // Set the streams
   m_pkD3DDevice->SetStreamSource(0, pkBuffData->GetVBChip(0)->GetVB(), 0, pkBuffData->GetVertexStride(0));
   m_pkD3DDevice->SetIndices(pkBuffData->GetIB());
   m_pkD3DRenderState->SetDeclaration(((NiD3DShaderDeclaration*)ms_pkDecl)->GetD3DDeclaration());

    return pkBuffData;
}

bool CWaterShader::Init(int id)
{
	char acName[128];
	NiSprintf(acName, sizeof(acName), "water shader %d", id);
	SetName(acName);

	// This is the best (and only) implementation of this shader
	m_bIsBestImplementation = true;

	//textures
	for(int i = 0; i < MAX_TEXTURE_COUNT; i++)
	{
		m_pkTextures[i] = NULL;
	}

    m_spVertexShader = TerrainHelper::CreateVertexShader("Water.hlsl", "WaterVS");
    m_spPixelShader = TerrainHelper::CreatePixelShader("Water.hlsl", "WaterPS");
    m_spAdditivePixelShader = TerrainHelper::CreatePixelShader("Water.hlsl", "AdditiveWaterPS");

    m_spRefractVertexShader = TerrainHelper::CreateVertexShader("Water.hlsl", "RefractWaterVS");
    m_spRefractPixelShader = TerrainHelper::CreatePixelShader("Water.hlsl", "RefractWaterPS");

    m_spRRVertexShader = TerrainHelper::CreateVertexShader("Water.hlsl", "RRWaterVS");
    m_spRRPixelShader = TerrainHelper::CreatePixelShader("Water.hlsl", "RRWaterPS");

    m_spAlphaProperty = NiNew NiAlphaProperty;
    
	//默认值
	SetAlpha(1.f);
	SetAlphaBlend(true);
	SetAdditiveLight(true);
	SetRR(false);
	SetUVScale(1.f);
	SetSpeed(1.f);

	return true;
}

void CWaterShader::Destroy()
{
	for(int i = 0; i < MAX_TEXTURE_COUNT; i++)
	{
		if(m_pkTextures[i])
		{
			m_pkTextures[i]->DecRefCount();
			m_pkTextures[i] = NULL;;
		}
	}
}

bool CWaterShader::SetAlpha(float fAlpha)
{
    if(m_fAlpha != fAlpha)
    {
	    m_fAlpha = fAlpha;
        return true;
    }

    return false;
}

bool CWaterShader::SetAlphaBlend(bool bAlphaBlend)
{
	if(m_bAlphaBlend != bAlphaBlend)
    {
        m_bAlphaBlend = bAlphaBlend;
        m_spAlphaProperty->SetAlphaBlending(bAlphaBlend);
        return true;
    }

    return false;
}

bool CWaterShader::SetAdditiveLight(bool bAdditive)
{
    if(m_bAdditiveLight != bAdditive)
    {
	    m_bAdditiveLight = bAdditive;
        return true;
    }

    return false;
}

bool CWaterShader::GetRR() const
{ 
    return m_bRR && CWater::GetRREnable(); 
}

bool CWaterShader::SetRR(bool bRR)
{
    if(m_bRR != bRR)
    {
	    m_bRR = bRR;
        return true;
    }

    return false;
}

bool CWaterShader::SetUVScale(float fUVScale)
{
    if(m_fUVScale != fUVScale)
    {
	    m_fUVScale = fUVScale;
        return true;
    }

    return false;
}

bool CWaterShader::SetSpeed(float fSpeed)
{
	if(m_fSpeed != fSpeed)
    {
       m_fSpeed = fSpeed;
       return true;
    }

    return false;
}

bool CWaterShader::SetTexturePathName(const NiFixedString& kTexturePathName)
{
	if(m_kTexturePathName.EqualsNoCase(kTexturePathName))
		return false;

	for(int i = 0; i < MAX_TEXTURE_COUNT; i++)
	{
		if(m_pkTextures[i])
		{
			m_pkTextures[i]->DecRefCount();
			m_pkTextures[i] = NULL;
		}
	}

	m_kTexturePathName = kTexturePathName;
	char acFilename[1024];
	m_uiTexureCount = MAX_TEXTURE_COUNT;
	for(int i = 0; i < MAX_TEXTURE_COUNT; i++)
	{
		NiSprintf(acFilename, sizeof(acFilename), "%s\\%s%d.dds", TerrainHelper::GetClientPath(), (const char*)kTexturePathName, i + 1);
		if(!NiFile::Access(acFilename, NiFile::READ_ONLY))
		{
			m_uiTexureCount = i;
			break;
		}

		m_pkTextures[i] = NiSourceTexture::Create(acFilename);
		m_pkTextures[i]->IncRefCount();
	}

	SetCurrentTexture(m_pkTextures[0]);
    return true;
}

void CWaterShader::SetCurrentTexture(NiTexture* pkTexture)
{
    m_pkCurrentTexture = pkTexture;
}

bool CWaterShader::Load(CTagStream& kStream)
{
	float fAlpha;
	kStream.Read(&fAlpha, sizeof(fAlpha));
	SetAlpha(fAlpha);

	NiBool bTemp;
	kStream.Read(&bTemp, sizeof(bTemp));
	SetAlphaBlend(NIBOOL_IS_TRUE(bTemp));

	kStream.Read(&bTemp, sizeof(bTemp));
	SetAdditiveLight(NIBOOL_IS_TRUE(bTemp));

	kStream.Read(&bTemp, sizeof(bTemp));
	SetRR(NIBOOL_IS_TRUE(bTemp));

	float fUVScale;
	kStream.Read(&fUVScale, sizeof(fUVScale));
	SetUVScale(fUVScale);

	float fSpeed;
	kStream.Read(&fSpeed, sizeof(fSpeed));
	SetSpeed(fSpeed);

	NiFixedString kTexturePathName;
	kStream.ReadFixedString(kTexturePathName);
	SetTexturePathName(kTexturePathName);

	return true;
}

bool CWaterShader::Save(CTagStream& kStream)
{
	kStream.Write(&m_fAlpha, sizeof(m_fAlpha));

	NiBool bTemp = m_bAlphaBlend;
	kStream.Write(&bTemp, sizeof(bTemp));

	bTemp = m_bAdditiveLight;
	kStream.Write(&bTemp, sizeof(bTemp));

	bTemp = m_bRR;
	kStream.Write(&bTemp, sizeof(bTemp));

	kStream.Write(&m_fUVScale, sizeof(m_fUVScale));
	kStream.Write(&m_fSpeed, sizeof(m_fSpeed));
	kStream.WriteFixedString(m_kTexturePathName);

	return true;
}

void CWaterShader::Update(const NiColor& kAmbientColor, const NiColor& kDiffuseColor)
{
	int iIndex;
	if(m_uiTexureCount > 1)
	{
		iIndex = int( NiGetCurrentTimeInSec() * ((1000.f * m_fSpeed) / m_uiTexureCount) );
		iIndex = iIndex % m_uiTexureCount;
	}
	else
		iIndex = 0;

	SetCurrentTexture(m_pkTextures[iIndex]);

    m_kAmbientColor = kAmbientColor;
    m_kDiffuseColor = kDiffuseColor;
}

void CWaterShader::SetupRender()
{
    m_pkD3DRenderState->SetVertexShader(m_spVertexShader->GetShaderHandle());

    if(m_bAdditiveLight)
        m_pkD3DRenderState->SetPixelShader(m_spAdditivePixelShader->GetShaderHandle());
    else
        m_pkD3DRenderState->SetPixelShader(m_spPixelShader->GetShaderHandle());

    float fValues[16][4];
    D3DXMATRIX kD3DWorld = m_pkD3DRenderer->GetD3DMat();
    fValues[0][0] = kD3DWorld._41;
    fValues[0][1] = kD3DWorld._42;
    fValues[0][2] = kD3DWorld._43;
    fValues[0][3] = 0.f;

    D3DXMATRIX kD3DView = m_pkD3DRenderer->GetD3DView();
    D3DXMATRIX kD3DProj = m_pkD3DRenderer->GetD3DProj();
    D3DXMATRIX kTempMat = kD3DView * kD3DProj;
    D3DXMatrixTranspose(&kTempMat, &kTempMat);

    memcpy(fValues[1], &kTempMat, sizeof(kTempMat));

    fValues[5][0] = m_kDiffuseColor.r;
    fValues[5][1] = m_kDiffuseColor.g;
    fValues[5][2] = m_kDiffuseColor.b;
    fValues[5][3] = m_fAlpha;

    fValues[6][0] = m_kAmbientColor.r;
    fValues[6][1] = m_kAmbientColor.g;
    fValues[6][2] = m_kAmbientColor.b;
    fValues[6][3] = m_fAlpha;

    fValues[7][0] = m_fUVScale;

    //view
    D3DXMatrixTranspose(&kTempMat, &kD3DView);
    memcpy(fValues[11], &kTempMat, sizeof(kTempMat));

    //fog density

	float fNear,fFar = 0.0f;

	CTerrainLightManager::Get()->GetFogNearFar(fNear, fFar);

	fValues[15][0] = CTerrainLightManager::Get()->GetFogDensity();
	fValues[15][1] = fNear;
	fValues[15][2] = fFar;
	fValues[15][3] = 1.f;

    m_pkD3DRenderState->SetVertexShaderConstantF(0, (float*)fValues, 16);

    bool bS0Mipmap;
    bool bNonPow2;
    bool bChanged;

	NiTexture* pChunkDepthText = m_pkChunk->GetWaterEdgeMapTexture();

	D3DBaseTexturePtr pkD3DTexture = NULL;


	pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(m_pkCurrentTexture, bChanged, bS0Mipmap, bNonPow2);

    m_pkD3DRenderState->SetTexture(0, pkD3DTexture);

	if ( pChunkDepthText )
	{
		pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(pChunkDepthText, bChanged, bS0Mipmap, bNonPow2);
		m_pkD3DRenderState->SetTexture(1, pkD3DTexture);
	}
	
    m_pkD3DRenderState->SetTexture(2, NULL);

	m_pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_ADDRESSU, D3DTADDRESS_WRAP);
	m_pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_ADDRESSV, D3DTADDRESS_WRAP);
	m_pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_MAGFILTER, D3DTEXF_LINEAR);
	m_pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_MINFILTER, D3DTEXF_LINEAR);
	m_pkD3DRenderState->SetSamplerState(0, NiD3DRenderState::NISAMP_MIPFILTER, D3DTEXF_LINEAR);

	if ( pChunkDepthText )
	{
		m_pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_ADDRESSU, D3DTADDRESS_CLAMP);
		m_pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_ADDRESSV, D3DTADDRESS_CLAMP);
		m_pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_MAGFILTER, D3DTEXF_LINEAR);
		m_pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_MINFILTER, D3DTEXF_LINEAR);
		m_pkD3DRenderState->SetSamplerState(1, NiD3DRenderState::NISAMP_MIPFILTER, D3DTEXF_LINEAR);
	}



    if(m_bAlphaBlend)
    {
        m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
        m_pkD3DRenderState->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
        m_pkD3DRenderState->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
        m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
    }
    else
    {
        m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
        m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
    }

    //pixel
    BOOL bValues[2];
    if(CTerrainLightManager::GetFogEnable())
    {
        bValues[0] = TRUE;

        //FogColor
        NiColor kFogColor = CTerrainLightManager::Get()->GetFogColor();
        fValues[0][0] = kFogColor.r;
        fValues[0][1] = kFogColor.g;
        fValues[0][2] = kFogColor.b;
        fValues[0][3] = 1.f;
    }
    else
    {
        bValues[0] = FALSE;
    }

	if ( pChunkDepthText )
	{
		bValues[1] = TRUE;
	}
	else
	{
		bValues[1] = FALSE;
	}

    m_pkD3DRenderState->SetPixelShaderConstantF(0, &fValues[0][0], 1);
	m_pkD3DRenderState->SetPixelShaderConstantB(0, bValues, 2);
}

void CWaterShader::SetupRefractRender()
{
    m_pkD3DRenderState->SetVertexShader(m_spRefractVertexShader->GetShaderHandle());
    m_pkD3DRenderState->SetPixelShader(m_spRefractPixelShader->GetShaderHandle());

    float fValues[5][4];
    D3DXMATRIX kD3DWorld = m_pkD3DRenderer->GetD3DMat();
    fValues[0][0] = kD3DWorld._41;
    fValues[0][1] = kD3DWorld._42;
    fValues[0][2] = kD3DWorld._43;
    fValues[0][3] = 0.f;

    D3DXMATRIX kD3DView = m_pkD3DRenderer->GetD3DView();
    D3DXMATRIX kD3DProj = m_pkD3DRenderer->GetD3DProj();
    D3DXMATRIX kTempMat = kD3DView * kD3DProj;
    D3DXMatrixTranspose(&kTempMat, &kTempMat);

    memcpy(fValues[1], &kTempMat, sizeof(kTempMat));
    m_pkD3DRenderState->SetVertexShaderConstantF(0, (float*)fValues, 5);
}

void CWaterShader::SetupRRRender(const NiPoint3& kWaterBase)
{
    m_pkD3DRenderState->SetVertexShader(m_spRRVertexShader->GetShaderHandle());
    m_pkD3DRenderState->SetPixelShader(m_spRRPixelShader->GetShaderHandle());

    float fValues[16][4];
    D3DXMATRIX kD3DWorld = m_pkD3DRenderer->GetD3DMat();
    fValues[0][0] = kD3DWorld._41;
    fValues[0][1] = kD3DWorld._42;
    fValues[0][2] = kD3DWorld._43;
    fValues[0][3] = 0.f;

    D3DXMATRIX kD3DView = m_pkD3DRenderer->GetD3DView();
    D3DXMATRIX kD3DProj = m_pkD3DRenderer->GetD3DProj();
    D3DXMATRIX kTempMat = kD3DView * kD3DProj;
    D3DXMatrixTranspose(&kTempMat, &kTempMat);

    memcpy(fValues[1], &kTempMat, sizeof(kTempMat));

    fValues[5][0] = m_kDiffuseColor.r;
    fValues[5][1] = m_kDiffuseColor.g;
    fValues[5][2] = m_kDiffuseColor.b;
    fValues[5][3] = m_fAlpha;

    fValues[6][0] = m_kAmbientColor.r;
    fValues[6][1] = m_kAmbientColor.g;
    fValues[6][2] = m_kAmbientColor.b;
    fValues[6][3] = m_fAlpha;

    fValues[7][0] = m_fUVScale;
    fValues[8][0] = CMap::GetTime();//获取当前时间

    fValues[9][0] = NiFmod(kWaterBase.x, 512.f);
    fValues[9][1] = NiFmod(kWaterBase.y, 512.f);

    D3DMATRIX kViewMat = m_pkD3DRenderer->GetInvView();
    fValues[10][0] = kViewMat._31;
    fValues[10][1] = kViewMat._32;
    fValues[10][2] = kViewMat._33;
    fValues[10][3] = kViewMat._34;

    //view
    D3DXMatrixTranspose(&kTempMat, &kD3DView);
    memcpy(fValues[11], &kTempMat, sizeof(kTempMat));

    //fog density
	float fNear,fFar = 0.0f;

	CTerrainLightManager::Get()->GetFogNearFar(fNear, fFar);

	fValues[15][0] = CTerrainLightManager::Get()->GetFogDensity();
	fValues[15][1] = fNear;
	fValues[15][2] = fFar;
	fValues[15][3] = 1.f;

    m_pkD3DRenderState->SetVertexShaderConstantF(0, (float*)fValues, 16);

    bool bS0Mipmap;
    bool bNonPow2;
    bool bChanged; 
    D3DBaseTexturePtr pkD3DTexture;
    pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(CWater::GetRefractTexture(), bChanged, bS0Mipmap, bNonPow2);
    m_pkD3DRenderState->SetTexture(0, pkD3DTexture);

    pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(CWater::GetReflectTexture(), bChanged, bS0Mipmap, bNonPow2);
    m_pkD3DRenderState->SetTexture(1, pkD3DTexture);

    pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(CWater::GetNormalTexture(), bChanged, bS0Mipmap, bNonPow2);
    m_pkD3DRenderState->SetTexture(2, pkD3DTexture);

    pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(CWater::GetDuDvTexture(), bChanged, bS0Mipmap, bNonPow2);
    m_pkD3DRenderState->SetTexture(3, pkD3DTexture);


	NiTexture* pChunkDepthText = m_pkChunk->GetWaterEdgeMapTexture();

	if ( pChunkDepthText )
	{
		pkD3DTexture = m_pkD3DRenderer->GetTextureManager()->PrepareTextureForRendering(pChunkDepthText, bChanged, bS0Mipmap, bNonPow2);
		m_pkD3DRenderState->SetTexture(4, pkD3DTexture);
	}
    //pixel
    BOOL bValues[2];
    if(CTerrainLightManager::GetFogEnable())
    {
        bValues[0] = TRUE;

        //FogColor
        NiColor kFogColor = CTerrainLightManager::Get()->GetFogColor();
        fValues[0][0] = kFogColor.r;
        fValues[0][1] = kFogColor.g;
        fValues[0][2] = kFogColor.b;
        fValues[0][3] = 1.f;
    }
    else
    {
        bValues[0] = FALSE;
    }
	if ( pChunkDepthText )
	{
		bValues[1] = TRUE;
	}
	else
	{
		bValues[1] = FALSE;
	}

	if(m_bAlphaBlend)
	{
		m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		m_pkD3DRenderState->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		m_pkD3DRenderState->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	}
	else
	{
		m_pkD3DRenderState->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		m_pkD3DRenderState->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	}

    m_pkD3DRenderState->SetPixelShaderConstantF(0, &fValues[0][0], 1);
    m_pkD3DRenderState->SetPixelShaderConstantB(0, bValues, 2);
}

void CWaterShader::_SDMInit()
{
	ms_pkDecl = NULL;
	ms_bRefractPass = false;
}

void CWaterShader::_SDMShutdown()
{
	if(ms_pkDecl)
	{
		ms_pkDecl->DecRefCount();
		ms_pkDecl = NULL;
	}
}
