#ifndef WATER_SHADER_H
#define WATER_SHADER_H

#include "TerrainLibType.h"
#include "Chunk.h"

#define MAX_TEXTURE_COUNT 30 //最多有多少张贴图
#define MAX_WATER_COUNT 4 //一张地图最多是4种水面

class TERRAIN_ENTRY CWaterShader : public NiD3DShaderInterface
{
public:
	CWaterShader(int id);
	virtual ~CWaterShader();

	virtual unsigned int PreProcessPipeline(NiGeometry* pkGeometry, 
		const NiSkinInstance* pkSkin, 
		NiGeometryData::RendererData* pkRendererData, 
		const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
		const NiTransform& kWorld, const NiBound& kWorldBound);

	virtual unsigned int PostProcessPipeline(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects,
        const NiTransform& kWorld, const NiBound& kWorldBound);

    virtual unsigned int SetupTransformations(NiGeometry* pkGeometry, 
        const NiSkinInstance* pkSkin, 
        const NiSkinPartition::Partition* pkPartition, 
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState, const NiDynamicEffectState* pkEffects, 
        const NiTransform& kWorld, const NiBound& kWorldBound);

    virtual NiGeometryData::RendererData* PrepareGeometryForRendering(
        NiGeometry* pkGeometry, const NiSkinPartition::Partition* pkPartition,
        NiGeometryData::RendererData* pkRendererData, 
        const NiPropertyState* pkState);

    // Advance to the next pass.
    virtual unsigned int FirstPass() { return 1; }
    virtual unsigned int NextPass() { return 0; }

	void SetCurrentTexture(NiTexture* pkTexture);

	float GetAlpha() const { return m_fAlpha; }
	bool SetAlpha(float fAlpha);
	
	bool GetAlphaBlend() const { return m_bAlphaBlend; }
	bool SetAlphaBlend(bool bAlphaBlend);

	bool GetAdditiveLight() const { return m_bAdditiveLight; }
	bool SetAdditiveLight(bool bAdditive);

    bool GetRR() const;
	bool SetRR(bool bRR);

	float GetUVScale() const { return m_fUVScale; }
	bool SetUVScale(float fUVScale);

	float GetSpeed() const { return m_fSpeed; }
	bool SetSpeed(float fSpeed);

	const NiFixedString& GetTexturePathName() const { return m_kTexturePathName; }
	bool SetTexturePathName(const NiFixedString& kTexturePathName);

	bool Load(CTagStream& kStream);
	bool Save(CTagStream& kStream);
	bool Init(int id);
	void Destroy();

	void Update(const NiColor& kAmbientColor, const NiColor& kDiffuseColor);
    
    void SetupRender();
    void SetupRefractRender();
    void SetupRRRender(const NiPoint3& kWaterBase);

    NiAlphaProperty* GetAlphaProperty() { return m_spAlphaProperty; }

    static void SetRefractPass(bool bRefrPass) { ms_bRefractPass = bRefrPass; }

	static void _SDMInit();
	static void _SDMShutdown();

private:
	float m_fAlpha; //透明度
	bool m_bAlphaBlend; //是否混合
	bool m_bAdditiveLight; //贴图和光照相加
	bool m_bRR; //ReflectRefract 反射折射
	float m_fUVScale; //贴图坐标缩放
	float m_fSpeed; //贴图变化速度, 如果为1， 则每秒变化m_uiTexureCount张贴图
	NiFixedString m_kTexturePathName; //相对路径 + 文件名， 没有扩展名

	unsigned int m_uiTexureCount;
	NiTexture* m_pkTextures[MAX_TEXTURE_COUNT];

	CChunk*	 m_pkChunk;

    NiColor m_kAmbientColor;
    NiColor m_kDiffuseColor;

    NiD3DVertexShaderPtr m_spVertexShader;
    NiD3DPixelShaderPtr m_spPixelShader;
    NiD3DPixelShaderPtr m_spAdditivePixelShader;
    
    NiD3DVertexShaderPtr m_spRefractVertexShader;
    NiD3DPixelShaderPtr m_spRefractPixelShader;

    NiD3DVertexShaderPtr m_spRRVertexShader;
    NiD3DPixelShaderPtr m_spRRPixelShader;
    
    NiTexture* m_pkCurrentTexture;

    //排序用
    NiAlphaPropertyPtr m_spAlphaProperty;

    static bool	ms_bRefractPass;
	static NiShaderDeclaration* ms_pkDecl;
};

#endif WATER_SHADER_H