#include "TerrainPCH.h"

#include "WayPointComponent.h"
#include "NpcComponent.h"

NiFixedString CWayPointComponent::ms_kClassName;
NiFixedString CWayPointComponent::ms_kComponentName;

NiFixedString CWayPointComponent::ms_kWaitTimeName;
NiFixedString CWayPointComponent::ms_kSceneRootPointerName;

NiFixedString CWayPointComponent::ms_kTranslationName;

NiUniqueID CWayPointComponent::ms_kTemplateID;
NiAVObject* CWayPointComponent::ms_pkSceneRootTemplate;

NiFactoryDeclarePropIntf(CWayPointComponent);

//---------------------------------------------------------------------------
void CWayPointComponent::_SDMInit()
{
    ms_kClassName = "CWayPointComponent";
    ms_kComponentName = "Way Point";

    ms_kWaitTimeName = "�ȴ�ʱ��";
	ms_kSceneRootPointerName = "Scene Root Pointer";

    ms_kTranslationName = "Translation";

    ms_kTemplateID = NiUniqueID(0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA);
	NiFactoryRegisterPropIntf(CWayPointComponent);

	LoadSceneRootTemplate();
}

//---------------------------------------------------------------------------
void CWayPointComponent::_SDMShutdown()
{
    ms_kClassName = NULL;
    ms_kComponentName = NULL;

    ms_kWaitTimeName = NULL;
	ms_kSceneRootPointerName = NULL;
	
	ms_kTranslationName = NULL;

	DestroySceneRootTemplate();
}

void CWayPointComponent::LoadSceneRootTemplate()
{
	NiStream kStream;
	kStream.Load("..\\..\\Data\\waypoint.nif");
	NIASSERT( kStream.GetObjectCount() == 1 );
	NIASSERT(NiIsKindOf(NiNode, kStream.GetObjectAt(0)));

	ms_pkSceneRootTemplate = (NiAVObject*)kStream.GetObjectAt(0);
	ms_pkSceneRootTemplate->IncRefCount();
}

void CWayPointComponent::DestroySceneRootTemplate()
{
	ms_pkSceneRootTemplate->DecRefCount();
	ms_pkSceneRootTemplate = NULL;
}

CWayPointComponent::CWayPointComponent()
	:m_fWaitTime(0.f)
	,m_uiIndex(0)
{
	m_spSceneRoot = (NiAVObject*)ms_pkSceneRootTemplate->Clone();
	m_spSceneRoot->UpdateProperties();
}

//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* CWayPointComponent::Clone(bool bInheritProperties)
{
	CWayPointComponent* pkWayPointComponent = NiNew CWayPointComponent;
	pkWayPointComponent->SetIndex(m_uiIndex + 1);
	pkWayPointComponent->SetWaitTime(m_fWaitTime);

    return pkWayPointComponent;
}
//---------------------------------------------------------------------------
NiEntityComponentInterface* CWayPointComponent::GetMasterComponent()
{
    return NULL;
}
//---------------------------------------------------------------------------
void CWayPointComponent::SetMasterComponent(NiEntityComponentInterface* pkMasterComponent)
{
}
//---------------------------------------------------------------------------
void CWayPointComponent::GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
    kDependentPropertyNames.Add(ms_kTranslationName);
}
//---------------------------------------------------------------------------
// NiEntityPropertyInterface overrides.
//---------------------------------------------------------------------------
NiBool CWayPointComponent::SetTemplateID(const NiUniqueID& kID)
{
    //Not supported for custom components
    return false;
}
//---------------------------------------------------------------------------
NiUniqueID  CWayPointComponent::GetTemplateID()
{
    return ms_kTemplateID;
}
//---------------------------------------------------------------------------
void CWayPointComponent::AddReference()
{
    this->IncRefCount();
}
//---------------------------------------------------------------------------
void CWayPointComponent::RemoveReference()
{
    this->DecRefCount();
}
//---------------------------------------------------------------------------
NiFixedString CWayPointComponent::GetClassName() const
{
    return ms_kClassName;
}
//---------------------------------------------------------------------------
NiFixedString CWayPointComponent::GetName() const
{
    return ms_kComponentName;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::SetName(const NiFixedString& kName)
{
    // This component does not allow its name to be set.
    return false;
}
//---------------------------------------------------------------------------
void CWayPointComponent::BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext, NiEntityErrorInterface* pkErrors)
{
	pkRenderingContext->m_pkCullingProcess->Process(
		pkRenderingContext->m_pkCamera, 
		m_spSceneRoot,
	    pkRenderingContext->m_pkCullingProcess->GetVisibleSet());
}
//---------------------------------------------------------------------------
void CWayPointComponent::Update(NiEntityPropertyInterface* pkParentEntity, float fTime, NiEntityErrorInterface* pkErrors, NiExternalAssetManager* pkAssetManager)
{
	NiPoint3 kTranslation;
	if( pkParentEntity->GetPropertyData(ms_kTranslationName, kTranslation)
	 && m_spSceneRoot->GetTranslate() != kTranslation )
	{
		m_spSceneRoot->SetTranslate(kTranslation);
		m_spSceneRoot->Update(fTime);

		CNpcComponent* pkNpcComponent = (CNpcComponent*)m_pkNpcEntity->GetComponentByTemplateID(CNpcComponent::ms_kTemplateID);
		pkNpcComponent->BuildPathGeomerty();
	}
}
//---------------------------------------------------------------------------
void CWayPointComponent::GetPropertyNames(NiTObjectSet<NiFixedString>& kPropertyNames) const
{
    kPropertyNames.Add(ms_kWaitTimeName);
	kPropertyNames.Add(ms_kSceneRootPointerName);
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::CanResetProperty(const NiFixedString& kPropertyName, bool& bCanReset) const
{
    if( kPropertyName == ms_kWaitTimeName
	 || kPropertyName == ms_kSceneRootPointerName )
    {
        bCanReset = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::ResetProperty(const NiFixedString& kPropertyName)
{
    return false;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::MakePropertyUnique(const NiFixedString& kPropertyName, bool& bMadeUnique)
{
    return false;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::GetDisplayName(const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const
{
    if (kPropertyName == ms_kWaitTimeName)
    {
        kDisplayName = kPropertyName;
    }
	else if (kPropertyName == ms_kSceneRootPointerName)
    {
        // Scene Root Pointer property should not be displayed.
        kDisplayName = NULL;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::SetDisplayName(const NiFixedString& kPropertyName, const NiFixedString& kDisplayName)
{
    // This component does not allow the display name to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::GetPrimitiveType(const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const
{
    if (kPropertyName == ms_kWaitTimeName)
    {
        kPrimitiveType = PT_FLOAT;
    }
	else if (kPropertyName == ms_kSceneRootPointerName)
    {
        kPrimitiveType = PT_NIOBJECTPOINTER;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::SetPrimitiveType(const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType)
{
    // This component does not allow the primitive type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::GetSemanticType(const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const
{
    if (kPropertyName == ms_kWaitTimeName)
    {
        kSemanticType = PT_FLOAT;
    }
	else if (kPropertyName == ms_kSceneRootPointerName)
    {
        kSemanticType = PT_NIOBJECTPOINTER;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::SetSemanticType(const NiFixedString& kPropertyName, const NiFixedString& kSemanticType)
{
    // This component does not allow the semantic type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::GetDescription(const NiFixedString& kPropertyName, NiFixedString& kDescription) const
{
    if (kPropertyName == ms_kWaitTimeName)
    {
        kDescription = kPropertyName;
    }
	else if (kPropertyName == ms_kSceneRootPointerName)
    {
        // This is a hidden property, so no description is provided.
        kDescription = NULL;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::SetDescription(const NiFixedString& kPropertyName, const NiFixedString& kDescription)
{
    // This component does not allow the description to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::GetCategory(const NiFixedString& kPropertyName, NiFixedString& kCategory) const
{
    if( kPropertyName == ms_kWaitTimeName
	 || kPropertyName == ms_kSceneRootPointerName )
    {
        kCategory = ms_kComponentName;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName, bool& bIsReadOnly)
{
    if (kPropertyName == ms_kWaitTimeName)
    {
        bIsReadOnly = false;
        return true;
    }
	else if (kPropertyName == ms_kSceneRootPointerName)
    {
        bIsReadOnly = true;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::IsPropertyUnique(const NiFixedString& kPropertyName, bool& bIsUnique)
{
    if( kPropertyName == ms_kWaitTimeName
	 || kPropertyName == ms_kSceneRootPointerName )
    {
        bIsUnique = true;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::IsPropertySerializable(const NiFixedString& kPropertyName, bool& bIsSerializable)
{
    if (kPropertyName == ms_kWaitTimeName)
    {
        bIsSerializable = true;
    }
	else if (kPropertyName == ms_kSceneRootPointerName)
    {
        bIsSerializable = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::IsPropertyInheritable(const NiFixedString& kPropertyName, bool& bIsInheritable)
{
    if( kPropertyName == ms_kWaitTimeName
	 || kPropertyName == ms_kSceneRootPointerName )
    {
        bIsInheritable = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::IsExternalAssetPath(const NiFixedString& kPropertyName, unsigned int uiIndex,bool& bIsExternalAssetPath) const
{
    if( kPropertyName == ms_kWaitTimeName
	 || kPropertyName == ms_kSceneRootPointerName )
    {
        bIsExternalAssetPath = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::GetElementCount(const NiFixedString& kPropertyName, unsigned int& uiCount) const
{
    if( kPropertyName == ms_kWaitTimeName
	 || kPropertyName == ms_kSceneRootPointerName )
    {
        uiCount = 1;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::SetElementCount(const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet)
{
    if( kPropertyName == ms_kWaitTimeName
	 || kPropertyName == ms_kSceneRootPointerName )
    {
        bCountSet = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool CWayPointComponent::IsCollection(const NiFixedString& kPropertyName,bool& bIsCollection) const
{
    if( kPropertyName == ms_kWaitTimeName
	 || kPropertyName == ms_kSceneRootPointerName )
    {
        bIsCollection = false;
        return true;
    }

    return false;
}

//---------------------------------------------------------------------------
NiBool CWayPointComponent::GetPropertyData(const NiFixedString& kPropertyName, float& fData, unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kWaitTimeName)
    {
        fData = GetWaitTime();
    }
    else
    {
        return false;
    }

    return true;
}

//---------------------------------------------------------------------------
NiBool CWayPointComponent::SetPropertyData(const NiFixedString& kPropertyName, float fData, unsigned int uiIndex)
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kWaitTimeName)
    {
        SetWaitTime(fData);
    }
    else
    {
        return false;
    }

    return true;
}

NiBool CWayPointComponent::GetPropertyData(const NiFixedString& kPropertyName, NiObject*& pkData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		pkData = m_spSceneRoot;
	}
	else
	{
		return false;
	}

	return true;
}