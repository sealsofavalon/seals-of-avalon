#include "TerrainPluginPCH.h"
#include "FindArea.h"
#include "AreaDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void AreaControl::Init(AreaDialog* dlg, int id, int x, int y)
{
	m_iID = id;
	m_ParentDlg = dlg;
	this->m_AreaGroupBox = (new System::Windows::Forms::GroupBox());
	this->m_ColorTextBox = (new System::Windows::Forms::TextBox());
	this->m_AreaTextBox = (new System::Windows::Forms::TextBox());
	this->m_ChangeColor = (new System::Windows::Forms::Button());
	this->m_ChangeArea = (new System::Windows::Forms::Button());
	this->m_AreaGroupBox->SuspendLayout();
	// 
	// m_AreaGroupBox
	// 
	this->m_AreaGroupBox->Controls->Add(this->m_ColorTextBox);
	this->m_AreaGroupBox->Controls->Add(this->m_AreaTextBox);
	this->m_AreaGroupBox->Controls->Add(this->m_ChangeColor);
	this->m_AreaGroupBox->Controls->Add(this->m_ChangeArea);
	this->m_AreaGroupBox->Location = System::Drawing::Point(x + 26, y + 12);
	this->m_AreaGroupBox->Name = S"m_AreaGroupBox";
	this->m_AreaGroupBox->Size = System::Drawing::Size(286, 127);
	this->m_AreaGroupBox->TabIndex = 7;
	this->m_AreaGroupBox->TabStop = false;
	this->m_AreaGroupBox->Text = String::Format("Area {0}", __box(m_iID + 1));
	this->m_AreaGroupBox->Paint += new System::Windows::Forms::PaintEventHandler(this, &AreaControl::OnPaint);
	this->m_AreaGroupBox->Click += new System::EventHandler(this, &AreaControl::OnGroupBoxClick);
	// 
	// m_ColorTextBox
	// 
	this->m_ColorTextBox->Location = System::Drawing::Point(19, 82);
	this->m_ColorTextBox->Name = S"m_ColorTextBox";
	this->m_ColorTextBox->ReadOnly = true;
	this->m_ColorTextBox->Size = System::Drawing::Size(169, 21);
	this->m_ColorTextBox->TabIndex = 5;
	// 
	// m_AreaTextBox
	// 
	this->m_AreaTextBox->Location = System::Drawing::Point(19, 32);
	this->m_AreaTextBox->Name = S"m_AreaTextBox";
	this->m_AreaTextBox->ReadOnly = true;
	this->m_AreaTextBox->Size = System::Drawing::Size(169, 21);
	this->m_AreaTextBox->TabIndex = 4;
	// 
	// m_ChangeColor
	// 
	this->m_ChangeColor->Location = System::Drawing::Point(194, 82);
	this->m_ChangeColor->Name = S"m_ChangeColor";
	this->m_ChangeColor->Size = System::Drawing::Size(75, 23);
	this->m_ChangeColor->TabIndex = 3;
	this->m_ChangeColor->Text = S"更改色彩";
	this->m_ChangeColor->UseVisualStyleBackColor = true;
	this->m_ChangeColor->Click += new System::EventHandler(this, &AreaControl::OnChangeColor);
	// 
	// m_ChangeArea
	// 
	this->m_ChangeArea->Location = System::Drawing::Point(194, 30);
	this->m_ChangeArea->Name = S"m_ChangeArea";
	this->m_ChangeArea->Size = System::Drawing::Size(75, 23);
	this->m_ChangeArea->TabIndex = 0;
	this->m_ChangeArea->Text = S"更改区域";
	this->m_ChangeArea->UseVisualStyleBackColor = true;
	this->m_ChangeArea->Click += new System::EventHandler(this, &AreaControl::OnChangeArea);

	this->m_AreaGroupBox->ResumeLayout(false);
	this->m_AreaGroupBox->PerformLayout();
}

System::Void AreaControl::OnPaint(System::Object* sender, System::Windows::Forms::PaintEventArgs* e)
{
	if(m_ParentDlg->GetActiveControlId() == m_iID)
	{
		Pen* RedPen = new Pen(Color::FromArgb(255, 255, 0, 0), 4);
		e->Graphics->DrawRectangle(RedPen, 15, 15, m_AreaGroupBox->Width - 30, m_AreaGroupBox->Height - 30);
	}
}

System::Void AreaControl::OnGroupBoxClick(System::Object* sender, System::EventArgs* e)
{
	if(m_pkAreaInfo && m_pkAreaInfo->m_uiEntryID != 0)
	{
		m_ParentDlg->SetActiveControlId(m_iID);
		m_ParentDlg->Invalidate(true);
	}
}

System::Void AreaControl::OnChangeColor(System::Object* sender, System::EventArgs* e)
{
	if(m_pkAreaInfo == NULL || m_pkAreaInfo->m_uiEntryID == 0)
		return;

	ColorDialog* pmColorDialog = new ColorDialog();
	pmColorDialog->Color = m_ColorTextBox->BackColor;
	pmColorDialog->FullOpen = true;

	if( pmColorDialog->ShowDialog() == DialogResult::OK )
	{
		m_ColorTextBox->BackColor = pmColorDialog->Color;
		unsigned int uiColor = pmColorDialog->Color.ToArgb();
		g_pkAreaColorDB->SetColor(m_pkAreaInfo->m_uiEntryID, uiColor);
		m_AreaGroupBox->Invalidate(true);
	}
}

System::Void AreaControl::OnChangeArea(System::Object* sender, System::EventArgs* e)
{
	FindAreaDialog* dlg = new FindAreaDialog;
	if(dlg->ShowDialog() == DialogResult::OK)
	{
		m_pkAreaInfo = dlg->GetAreaInfo();
		m_AreaTextBox->Text = (const char*)m_pkAreaInfo->m_kName;

		unsigned int uiColor = g_pkAreaColorDB->GetColor(m_pkAreaInfo->m_uiEntryID);
		m_ColorTextBox->BackColor = Color::FromArgb(uiColor);

		m_AreaGroupBox->Invalidate(true);
	}
}

AreaControl* AreaDialog::GetActiveAreaControl()
{
	if(m_iActiveControlId < 0 || m_iActiveControlId >= MAX_AREA_CONTROLS)
		return NULL;

	return m_pmAreaControls[m_iActiveControlId];
}

AreaInfo* AreaDialog::GetActiveAreaInfo()
{
	if(ms_AreaDialog == NULL)
		return NULL;

	AreaControl* pmAreaControl = ms_AreaDialog->GetActiveAreaControl();
	if(pmAreaControl)
		return pmAreaControl->GetAreaInfo();

	return NULL;
}
