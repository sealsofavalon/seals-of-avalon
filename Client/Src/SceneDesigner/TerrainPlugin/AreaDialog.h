
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#define MAX_AREA_CONTROLS 8
namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
	namespace TerrainPlugin 
{
	public __gc class AreaDialog;
	public __gc class AreaControl : public MDisposable
	{
	private:
		System::Windows::Forms::Button*  m_ChangeColor;
		System::Windows::Forms::Button*  m_ChangeArea;
		System::Windows::Forms::TextBox*  m_ColorTextBox;
		System::Windows::Forms::TextBox*  m_AreaTextBox;
		System::Windows::Forms::GroupBox*  m_AreaGroupBox;

		unsigned int m_uiAreaID;
		unsigned int m_uiColor;

		AreaInfo* m_pkAreaInfo;
		AreaDialog* m_ParentDlg;
		int m_iID;

	public:
		void Init(AreaDialog* dlg, int id, int x, int y);
		__property System::Windows::Forms::Control* get_GroupBox()
		{
			return m_AreaGroupBox;
		}

		System::Void OnPaint(System::Object* sender, System::Windows::Forms::PaintEventArgs* e);
		System::Void OnGroupBoxClick(System::Object* sender, System::EventArgs* e);
		System::Void OnChangeColor(System::Object* sender, System::EventArgs* e);
		System::Void OnChangeArea(System::Object* sender, System::EventArgs* e);

		virtual void Do_Dispose(bool bDisposing)
		{
		}

		AreaInfo* GetAreaInfo() { return m_pkAreaInfo; }
	};

	public __gc class AreaDialog : public System::Windows::Forms::Form
	{
	public:
		AreaDialog(void)
		{
			InitializeComponent();
		}

		void Do_Dispose(bool bDisposing)
		{
			for(int i = 0; i < MAX_AREA_CONTROLS; ++i)
			{
				if(m_pmAreaControls[i])
				{
					m_pmAreaControls[i]->Do_Dispose(bDisposing);
					m_pmAreaControls[i] = NULL;
				}
			}
		}

	protected:
		void Dispose(Boolean disposing)
		{
			__super::Dispose(disposing);
		}

	private:
		AreaControl* m_pmAreaControls[];
		System::Windows::Forms::Panel* m_Panel;
		int m_iActiveControlId;
		static AreaDialog* ms_AreaDialog;

		void InitializeComponent(void)
		{
			ms_AreaDialog = this;

			m_iActiveControlId = -1;

			this->SuspendLayout();

			this->m_Panel = (new System::Windows::Forms::Panel());
			this->m_Panel->AutoScroll = true;
			this->m_Panel->Location = System::Drawing::Point(3, 3);
			this->m_Panel->Name = S"m_Panel";
			this->m_Panel->Size = System::Drawing::Size(345, 601);
			this->m_Panel->TabIndex = 2;

			m_pmAreaControls = new AreaControl*[MAX_AREA_CONTROLS];
			for(int i = 0; i < MAX_AREA_CONTROLS; ++i)
			{
				m_pmAreaControls[i] = new AreaControl;
				m_pmAreaControls[i]->Init(this, i, 10, i*130);

				this->m_Panel->Controls->Add(m_pmAreaControls[i]->GroupBox);
			}

			// 
			// AreaDialog
			//
			this->ClientSize = System::Drawing::Size(345, 612);
			this->Controls->Add(this->m_Panel);
			this->Name = S"AreaDialog";
			this->Text = S"Area Dialog";
			this->ResumeLayout(false);
			this->PerformLayout();
		}

	public:
		int GetActiveControlId() { return m_iActiveControlId; }
		void SetActiveControlId(int id) { m_iActiveControlId = id; }
		AreaControl* GetActiveAreaControl();
		static AreaDialog* Get() { return ms_AreaDialog; }
		static AreaInfo* GetActiveAreaInfo();
	};
} }}}