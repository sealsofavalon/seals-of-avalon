
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
	namespace TerrainPlugin {
		public __gc class CalcDialog : public System::Windows::Forms::Form
		{
		public:
			CalcDialog()
			{
				this->InitializeComponent();
			}

			void Do_Dispose(bool bDisposing)
			{
			}

		private: System::Void InitializeComponent() {
                     this->CalLightMapBtn = (new System::Windows::Forms::Button());
                     this->CalTerrainShadowChk = (new System::Windows::Forms::CheckBox());
                     this->groupBox1 = (new System::Windows::Forms::GroupBox());
                     this->m_FastCalcShadowBtn = (new System::Windows::Forms::Button());
                     this->button1 = (new System::Windows::Forms::Button());
                     this->groupBox2 = (new System::Windows::Forms::GroupBox());
                     this->m_BatchCalcMiniMapBtn = (new System::Windows::Forms::Button());
                     this->m_SizeLable = (new System::Windows::Forms::Label());
                     this->m_SizeBox = (new System::Windows::Forms::ComboBox());
                     this->m_CalcMiniMapBtn = (new System::Windows::Forms::Button());
                     this->groupBox3 = (new System::Windows::Forms::GroupBox());
                     this->m_ClearGridInfoBtn = (new System::Windows::Forms::Button());
                     this->m_CalcServerMapBtn = (new System::Windows::Forms::Button());
                     this->m_PathNodeGroupBox = (new System::Windows::Forms::GroupBox());
                     this->m_CheckConnectedBtn = (new System::Windows::Forms::Button());
                     this->BlurBox = (new System::Windows::Forms::CheckBox());
                     this->groupBox1->SuspendLayout();
                     this->groupBox2->SuspendLayout();
                     this->groupBox3->SuspendLayout();
                     this->m_PathNodeGroupBox->SuspendLayout();
                     this->SuspendLayout();
                     // 
                     // CalLightMapBtn
                     // 
                     this->CalLightMapBtn->Location = System::Drawing::Point(51, 90);
                     this->CalLightMapBtn->Name = S"CalLightMapBtn";
                     this->CalLightMapBtn->Size = System::Drawing::Size(117, 23);
                     this->CalLightMapBtn->TabIndex = 0;
                     this->CalLightMapBtn->Text = S"Calc Shadow ";
                     this->CalLightMapBtn->UseVisualStyleBackColor = true;
                     this->CalLightMapBtn->Click += new System::EventHandler(this, &CalcDialog::OnCalcShadow);
                     // 
                     // CalTerrainShadowChk
                     // 
                     this->CalTerrainShadowChk->AutoSize = true;
                     this->CalTerrainShadowChk->Enabled = false;
                     this->CalTerrainShadowChk->Location = System::Drawing::Point(57, 16);
                     this->CalTerrainShadowChk->Name = S"CalTerrainShadowChk";
                     this->CalTerrainShadowChk->Size = System::Drawing::Size(108, 16);
                     this->CalTerrainShadowChk->TabIndex = 2;
                     this->CalTerrainShadowChk->Text = S"Terrain Shadow";
                     this->CalTerrainShadowChk->UseVisualStyleBackColor = true;
                     // 
                     // groupBox1
                     // 
                     this->groupBox1->Controls->Add(this->BlurBox);
                     this->groupBox1->Controls->Add(this->m_FastCalcShadowBtn);
                     this->groupBox1->Controls->Add(this->button1);
                     this->groupBox1->Controls->Add(this->CalTerrainShadowChk);
                     this->groupBox1->Controls->Add(this->CalLightMapBtn);
                     this->groupBox1->Location = System::Drawing::Point(38, 12);
                     this->groupBox1->Name = S"groupBox1";
                     this->groupBox1->Size = System::Drawing::Size(219, 196);
                     this->groupBox1->TabIndex = 3;
                     this->groupBox1->TabStop = false;
                     this->groupBox1->Text = S"Shadow";
                     // 
                     // m_FastCalcShadowBtn
                     // 
                     this->m_FastCalcShadowBtn->Location = System::Drawing::Point(51, 126);
                     this->m_FastCalcShadowBtn->Name = S"m_FastCalcShadowBtn";
                     this->m_FastCalcShadowBtn->Size = System::Drawing::Size(117, 23);
                     this->m_FastCalcShadowBtn->TabIndex = 8;
                     this->m_FastCalcShadowBtn->Text = S"Batch Calc Shadow";
                     this->m_FastCalcShadowBtn->UseVisualStyleBackColor = true;
                     this->m_FastCalcShadowBtn->Click += new System::EventHandler(this, &CalcDialog::OnFastCalcShadow);
                     // 
                     // button1
                     // 
                     this->button1->Location = System::Drawing::Point(51, 165);
                     this->button1->Name = S"button1";
                     this->button1->Size = System::Drawing::Size(117, 23);
                     this->button1->TabIndex = 7;
                     this->button1->Text = S"Clear Shadow";
                     this->button1->UseVisualStyleBackColor = true;
                     this->button1->Click += new System::EventHandler(this, &CalcDialog::OnClearShadow);
                     // 
                     // groupBox2
                     // 
                     this->groupBox2->Controls->Add(this->m_BatchCalcMiniMapBtn);
                     this->groupBox2->Controls->Add(this->m_SizeLable);
                     this->groupBox2->Controls->Add(this->m_SizeBox);
                     this->groupBox2->Controls->Add(this->m_CalcMiniMapBtn);
                     this->groupBox2->Location = System::Drawing::Point(38, 223);
                     this->groupBox2->Name = S"groupBox2";
                     this->groupBox2->Size = System::Drawing::Size(219, 176);
                     this->groupBox2->TabIndex = 4;
                     this->groupBox2->TabStop = false;
                     this->groupBox2->Text = S"MiniMap";
                     // 
                     // m_BatchCalcMiniMapBtn
                     // 
                     this->m_BatchCalcMiniMapBtn->Location = System::Drawing::Point(49, 130);
                     this->m_BatchCalcMiniMapBtn->Name = S"m_BatchCalcMiniMapBtn";
                     this->m_BatchCalcMiniMapBtn->Size = System::Drawing::Size(121, 20);
                     this->m_BatchCalcMiniMapBtn->TabIndex = 3;
                     this->m_BatchCalcMiniMapBtn->Text = S"Batch Calc MiniMap";
                     this->m_BatchCalcMiniMapBtn->UseVisualStyleBackColor = true;
                     this->m_BatchCalcMiniMapBtn->Click += new System::EventHandler(this, &CalcDialog::OnBatchCalcMinimap);
                     // 
                     // m_SizeLable
                     // 
                     this->m_SizeLable->AutoSize = true;
                     this->m_SizeLable->Location = System::Drawing::Point(49, 26);
                     this->m_SizeLable->Name = S"m_SizeLable";
                     this->m_SizeLable->Size = System::Drawing::Size(29, 12);
                     this->m_SizeLable->TabIndex = 2;
                     this->m_SizeLable->Text = S"Size";
                     // 
                     // m_SizeBox
                     // 
                     this->m_SizeBox->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
                     this->m_SizeBox->FormattingEnabled = true;
                     this->m_SizeBox->Location = System::Drawing::Point(51, 50);
                     this->m_SizeBox->Name = S"m_SizeBox";
                     this->m_SizeBox->Size = System::Drawing::Size(121, 20);
                     this->m_SizeBox->TabIndex = 1;
                     this->m_SizeBox->Items->Add(S"128 x 128");
                     this->m_SizeBox->Items->Add(S"256 x 256");
                     this->m_SizeBox->Items->Add(S"512 x 512");
                     this->m_SizeBox->Items->Add(S"1024 x 1024");
                     this->m_SizeBox->SelectedIndex = 1;
                     // 
                     // m_CalcMiniMapBtn
                     // 
                     this->m_CalcMiniMapBtn->Location = System::Drawing::Point(51, 91);
                     this->m_CalcMiniMapBtn->Name = S"m_CalcMiniMapBtn";
                     this->m_CalcMiniMapBtn->Size = System::Drawing::Size(121, 20);
                     this->m_CalcMiniMapBtn->TabIndex = 0;
                     this->m_CalcMiniMapBtn->Text = S"Calc MiniMap";
                     this->m_CalcMiniMapBtn->UseVisualStyleBackColor = true;
                     this->m_CalcMiniMapBtn->Click += new System::EventHandler(this, &CalcDialog::OnCalcMiniMap);
                     // 
                     // groupBox3
                     // 
                     this->groupBox3->Controls->Add(this->m_ClearGridInfoBtn);
                     this->groupBox3->Controls->Add(this->m_CalcServerMapBtn);
                     this->groupBox3->Location = System::Drawing::Point(38, 506);
                     this->groupBox3->Name = S"groupBox3";
                     this->groupBox3->Size = System::Drawing::Size(219, 85);
                     this->groupBox3->TabIndex = 5;
                     this->groupBox3->TabStop = false;
                     this->groupBox3->Text = S"Moveable Info";
                     // 
                     // m_ClearGridInfoBtn
                     // 
                     this->m_ClearGridInfoBtn->Enabled = false;
                     this->m_ClearGridInfoBtn->Location = System::Drawing::Point(49, 51);
                     this->m_ClearGridInfoBtn->Name = S"m_ClearGridInfoBtn";
                     this->m_ClearGridInfoBtn->Size = System::Drawing::Size(121, 20);
                     this->m_ClearGridInfoBtn->TabIndex = 1;
                     this->m_ClearGridInfoBtn->Text = S"Clear Moveable";
                     this->m_ClearGridInfoBtn->UseVisualStyleBackColor = true;
                     this->m_ClearGridInfoBtn->Click += new System::EventHandler(this, &CalcDialog::OnClearMoveable);
                     // 
                     // m_CalcServerMapBtn
                     // 
                     this->m_CalcServerMapBtn->Location = System::Drawing::Point(49, 22);
                     this->m_CalcServerMapBtn->Name = S"m_CalcServerMapBtn";
                     this->m_CalcServerMapBtn->Size = System::Drawing::Size(121, 20);
                     this->m_CalcServerMapBtn->TabIndex = 0;
                     this->m_CalcServerMapBtn->Text = S"Calc Moveable";
                     this->m_CalcServerMapBtn->UseVisualStyleBackColor = true;
                     this->m_CalcServerMapBtn->Click += new System::EventHandler(this, &CalcDialog::OnCalcMoveable);
                     // 
                     // m_PathNodeGroupBox
                     // 
                     this->m_PathNodeGroupBox->Controls->Add(this->m_CheckConnectedBtn);
                     this->m_PathNodeGroupBox->Location = System::Drawing::Point(39, 405);
                     this->m_PathNodeGroupBox->Name = S"m_PathNodeGroupBox";
                     this->m_PathNodeGroupBox->Size = System::Drawing::Size(217, 92);
                     this->m_PathNodeGroupBox->TabIndex = 6;
                     this->m_PathNodeGroupBox->TabStop = false;
                     this->m_PathNodeGroupBox->Text = S"PathNode";
                     // 
                     // m_CheckConnectedBtn
                     // 
                     this->m_CheckConnectedBtn->Location = System::Drawing::Point(50, 38);
                     this->m_CheckConnectedBtn->Name = S"m_CheckConnectedBtn";
                     this->m_CheckConnectedBtn->Size = System::Drawing::Size(121, 23);
                     this->m_CheckConnectedBtn->TabIndex = 0;
                     this->m_CheckConnectedBtn->Text = S"Check Connected";
                     this->m_CheckConnectedBtn->UseVisualStyleBackColor = true;
                     this->m_CheckConnectedBtn->Click += new System::EventHandler(this, &CalcDialog::OnCheckConnected);
                     // 
                     // BlurBox
                     // 
                     this->BlurBox->AutoSize = true;
                     this->BlurBox->Location = System::Drawing::Point(57, 48);
                     this->BlurBox->Name = S"BlurBox";
                     this->BlurBox->Size = System::Drawing::Size(72, 16);
                     this->BlurBox->TabIndex = 9;
                     this->BlurBox->Text = S"模糊处理";
                     this->BlurBox->UseVisualStyleBackColor = true;
                     // 
                     // CalcDialog
                     // 
                     this->ClientSize = System::Drawing::Size(292, 603);
                     this->Controls->Add(this->m_PathNodeGroupBox);
                     this->Controls->Add(this->groupBox3);
                     this->Controls->Add(this->groupBox2);
                     this->Controls->Add(this->groupBox1);
                     this->Name = S"CalcDialog";
                     this->Text = S"Calc...";
                     this->groupBox1->ResumeLayout(false);
                     this->groupBox1->PerformLayout();
                     this->groupBox2->ResumeLayout(false);
                     this->groupBox2->PerformLayout();
                     this->groupBox3->ResumeLayout(false);
                     this->m_PathNodeGroupBox->ResumeLayout(false);
                     this->ResumeLayout(false);

                 }

		protected: 
			void Dispose(Boolean disposing)
			{
				if (disposing && components)
				{
					components->Dispose();
				}

				__super::Dispose(disposing);
			}
		
		private: System::ComponentModel::IContainer*  components;
		private: System::Windows::Forms::CheckBox*  CalTerrainShadowChk;
		private: System::Windows::Forms::GroupBox*  groupBox1;

		private: System::Windows::Forms::Button*  button1;
		private: System::Windows::Forms::GroupBox*  groupBox2;
		private: System::Windows::Forms::Button*  m_CalcMiniMapBtn;

		private: System::Windows::Forms::GroupBox*  groupBox3;
		private: System::Windows::Forms::Button*  m_CalcServerMapBtn;
        private: System::Windows::Forms::Button*  m_FastCalcShadowBtn;
        private: System::Windows::Forms::Button*  m_ClearGridInfoBtn;
        
        private: System::Windows::Forms::Label*  m_SizeLable;
        private: System::Windows::Forms::ComboBox*  m_SizeBox;
        private: System::Windows::Forms::GroupBox*  m_PathNodeGroupBox;
        private: System::Windows::Forms::Button*  m_CheckConnectedBtn;
        private: System::Windows::Forms::Button*  m_BatchCalcMiniMapBtn;
private: System::Windows::Forms::CheckBox*  BlurBox;


		protected:
			private: System::Windows::Forms::Button*  CalLightMapBtn;
		private:
			System::Void OnCalcShadow(System::Object*  sender, System::EventArgs*  e) 
			{
				CMap* pkMap = CMap::Get();
				if(!pkMap)
					return;

				CTile* pkTile = pkMap->GetCurrentTile();
				if(!pkTile)
					return;
				
				bool bCalTerrain = this->CalTerrainShadowChk->Checked;
				if(::MessageBox((HWND)this->Handle.ToInt32(), "More than 10 minutes on this task\n Are you continue?", "Warning", MB_OKCANCEL | MB_ICONWARNING) != IDOK)
					return;

                bool bBlur = BlurBox->Checked;
				pkTile->CalcShadow(true, false, bBlur);
			}

			//此函数功能改成批量计算了
			System::Void OnFastCalcShadow(System::Object*  sender, System::EventArgs*  e) {
				CMap* pkMap = CMap::Get();
				if(!pkMap)
					return;

				CTile* pkTile = pkMap->GetCurrentTile();
				if(!pkTile)
					return;

				int iX = pkTile->GetX();
				int iY = pkTile->GetY();

                bool bBlur = BlurBox->Checked;

				pkTile = pkMap->GetTileByIndex(iX + 1, iY + 1);
				if(pkTile)
					pkTile->CalcShadow(true, false, bBlur);

				pkTile = pkMap->GetTileByIndex(iX + 1, iY);
				if(pkTile)
					pkTile->CalcShadow(true, false, bBlur);

				pkTile = pkMap->GetTileByIndex(iX + 1, iY - 1);
				if(pkTile)
					pkTile->CalcShadow(true, false, bBlur);

				pkTile = pkMap->GetTileByIndex(iX, iY + 1);
				if(pkTile)
					pkTile->CalcShadow(true, false, bBlur);

				pkTile = pkMap->GetTileByIndex(iX, iY);
				if(pkTile)
					pkTile->CalcShadow(true, false, bBlur);

				pkTile = pkMap->GetTileByIndex(iX, iY - 1);
				if(pkTile)
					pkTile->CalcShadow(true, false, bBlur);

				pkTile = pkMap->GetTileByIndex(iX - 1, iY + 1);
				if(pkTile)
					pkTile->CalcShadow(true, false, bBlur);

				pkTile = pkMap->GetTileByIndex(iX - 1, iY);
				if(pkTile)
					pkTile->CalcShadow(true, false, bBlur);

				pkTile = pkMap->GetTileByIndex(iX - 1, iY - 1);
				if(pkTile)
					pkTile->CalcShadow(true, false, bBlur);
			}
			
			System::Void OnClearShadow(System::Object*  sender, System::EventArgs*  e) 
			{
				CMap* pkMap = CMap::Get();
				if(!pkMap)
					return;

				CTile* pkTile = pkMap->GetCurrentTile();
				if(!pkTile)
					return;

				pkTile->ClearShadow();
			}

			System::Void OnCalcMiniMap(System::Object*  sender, System::EventArgs*  e) 
			{
				CMap* pkMap = CMap::Get();
				if( !pkMap )
                    return;

                unsigned int uiSize = 256;
                switch(this->m_SizeBox->SelectedIndex)
                {
                case 0:
                    uiSize = 128;
                    break;

                case 1:
                    uiSize = 256;
                    break;

                case 2:
                    uiSize = 512;
                    break;

                case 3:
                    uiSize = 1024;
                    break;
                }

                CTile* pkTile = pkMap->GetCurrentTile();
				pkTile->CalcMiniMap(MFramework::Instance->Renderer->RenderingContext->GetRenderingContext(), uiSize);
			}

            System::Void OnBatchCalcMinimap(System::Object*  sender, System::EventArgs*  e) 
            {
                unsigned int uiSize = 256;
                switch(this->m_SizeBox->SelectedIndex)
                {
                case 0:
                    uiSize = 128;
                    break;

                case 1:
                    uiSize = 256;
                    break;

                case 2:
                    uiSize = 512;
                    break;

                case 3:
                    uiSize = 1024;
                    break;
                }

                CMap* pkMap = CMap::Get();
                if( !pkMap )
                    return;

                CTile* pkTile = pkMap->GetCurrentTile();
                if(!pkTile)
                    return;

                int iX = pkTile->GetX();
                int iY = pkTile->GetY();

                pkTile = pkMap->GetTileByIndex(iX + 1, iY + 1);
                if(pkTile)
                    pkTile->CalcMiniMap(MFramework::Instance->Renderer->RenderingContext->GetRenderingContext(), uiSize);

                pkTile = pkMap->GetTileByIndex(iX + 1, iY);
                if(pkTile)
                    pkTile->CalcMiniMap(MFramework::Instance->Renderer->RenderingContext->GetRenderingContext(), uiSize);

                pkTile = pkMap->GetTileByIndex(iX + 1, iY - 1);
                if(pkTile)
                    pkTile->CalcMiniMap(MFramework::Instance->Renderer->RenderingContext->GetRenderingContext(), uiSize);

                pkTile = pkMap->GetTileByIndex(iX, iY + 1);
                if(pkTile)
                    pkTile->CalcMiniMap(MFramework::Instance->Renderer->RenderingContext->GetRenderingContext(), uiSize);

                pkTile = pkMap->GetTileByIndex(iX, iY);
                if(pkTile)
                    pkTile->CalcMiniMap(MFramework::Instance->Renderer->RenderingContext->GetRenderingContext(), uiSize);

                pkTile = pkMap->GetTileByIndex(iX, iY - 1);
                if(pkTile)
                    pkTile->CalcMiniMap(MFramework::Instance->Renderer->RenderingContext->GetRenderingContext(), uiSize);

                pkTile = pkMap->GetTileByIndex(iX - 1, iY + 1);
                if(pkTile)
                    pkTile->CalcMiniMap(MFramework::Instance->Renderer->RenderingContext->GetRenderingContext(), uiSize);

                pkTile = pkMap->GetTileByIndex(iX - 1, iY);
                if(pkTile)
                    pkTile->CalcMiniMap(MFramework::Instance->Renderer->RenderingContext->GetRenderingContext(), uiSize);

                pkTile = pkMap->GetTileByIndex(iX - 1, iY - 1);
                if(pkTile)
                    pkTile->CalcMiniMap(MFramework::Instance->Renderer->RenderingContext->GetRenderingContext(), uiSize);
            }


			System::Void OnCalcMoveable(System::Object* sender, System::EventArgs* e) 
			{
				CMap* pkMap = CMap::Get();
				if(!pkMap)
					return;

				pkMap->CalcMoveable();
			}

			System::Void OnClearMoveable(System::Object* sender, System::EventArgs* e) 
			{
				CMap* pkMap = CMap::Get();
				if(!pkMap)
					return;

				pkMap->ClearMoveable();
			}
            
            System::Void OnCheckConnected(System::Object*  sender, System::EventArgs*  e)
            {
				CMap* pkMap = CMap::Get();
				if(!pkMap)
					return;

                g_pkPathNodeManager->CheckConnected();

                MUtility::AddErrorInterfaceMessages(MessageChannelType::Errors, pkMap->GetErrorHandler());
                pkMap->GetErrorHandler()->ClearErrors();
            }
};
} }}}