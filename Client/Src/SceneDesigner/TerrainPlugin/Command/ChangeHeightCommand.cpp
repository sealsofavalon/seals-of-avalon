#include "TerrainPluginPCH.h"
#include "ChangeHeightCommand.h"

//---------------------------------------------------------------------------
ChangeHeightCommand::ChangeHeightCommand(NiPoint3* pkUndoPoints, NiPoint3* pkRedoPoints, unsigned int uiCount)
	:m_pkUndoPoints(pkUndoPoints)
	,m_pkRedoPoints(pkRedoPoints)
	,m_uiCount(uiCount)
{
	char acCommandName[256];
	NiSprintf(acCommandName, sizeof(acCommandName), "Change terrain height %8.3f %8.3f count %4d", pkUndoPoints[0].x, pkRedoPoints[0].y, uiCount); 
	m_kCommandName = acCommandName;
}

//---------------------------------------------------------------------------
ChangeHeightCommand::~ChangeHeightCommand()
{
	NiDelete[] m_pkUndoPoints;
	NiDelete[] m_pkRedoPoints;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// NiEntityCommandInterface overrides.
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void ChangeHeightCommand::DoCommand(NiEntityErrorInterface* pkErrors, bool bUndoable)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	for( unsigned int ui = 0; ui < m_uiCount; ui++ )
		pkMap->SetHeight(m_pkRedoPoints[ui].x, m_pkRedoPoints[ui].y, m_pkRedoPoints[ui].z);
}

//---------------------------------------------------------------------------
void ChangeHeightCommand::UndoCommand(NiEntityErrorInterface* pkErrors)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	for( unsigned int ui = 0; ui < m_uiCount; ui++ )
		pkMap->SetHeight(m_pkUndoPoints[ui].x, m_pkUndoPoints[ui].y, m_pkUndoPoints[ui].z);
}