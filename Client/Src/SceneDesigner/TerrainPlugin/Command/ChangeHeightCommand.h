#pragma once

#include "TerrainEditCommand.h"
class ChangeHeightCommand : public TerrainEditCommand
{
private:
	NiPoint3* m_pkUndoPoints;
	NiPoint3* m_pkRedoPoints;
	unsigned int m_uiCount;

public:
	ChangeHeightCommand(NiPoint3* pkUndoPoints, NiPoint3* pkRedoPoints, unsigned int uiCount);
	virtual ~ChangeHeightCommand();

	virtual void DoCommand(NiEntityErrorInterface* pkErrors, bool bUndoable);
	virtual void UndoCommand(NiEntityErrorInterface* pkErrors);
};
