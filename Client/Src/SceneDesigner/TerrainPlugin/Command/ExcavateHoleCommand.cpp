#include "TerrainPluginPCH.h"
#include "ExcavateHoleCommand.h"

//---------------------------------------------------------------------------
ExcavateHoleCommand::ExcavateHoleCommand()
{
	m_kCommandName = "Excavate Hole";
}

//---------------------------------------------------------------------------
ExcavateHoleCommand::~ExcavateHoleCommand()
{
}

void ExcavateHoleCommand::AddHole(int x, int y, bool bHole)
{
	HoleInfo kHole;
	kHole.m_iX = x;
	kHole.m_iY = y;
	kHole.m_bHole = bHole;

	m_HoleList.push_back(kHole);
}

void ExcavateHoleCommand::Clear()
{
	m_HoleList.clear();
}

void ExcavateHoleCommand::DoCommand(NiEntityErrorInterface* pkErrors, bool bUndoable)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	for( size_t i = 0; i < m_HoleList.size(); i++ )
	{
		pkMap->ExcavateHole(m_HoleList[i].m_iX, m_HoleList[i].m_iY, m_HoleList[i].m_bHole);
	}
}

//---------------------------------------------------------------------------
void ExcavateHoleCommand::UndoCommand(NiEntityErrorInterface* pkErrors)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	for( size_t i = 0; i < m_HoleList.size(); i++ )
	{
		pkMap->ExcavateHole(m_HoleList[i].m_iX, m_HoleList[i].m_iY, !m_HoleList[i].m_bHole);
	}
}