#pragma once

#include "TerrainEditCommand.h"
#include <vector>

struct HoleInfo
{
	int m_iX;
	int m_iY;
	bool m_bHole;
};

class ExcavateHoleCommand : public TerrainEditCommand
{
private:
	std::vector<HoleInfo> m_HoleList;

public:
	ExcavateHoleCommand();
	virtual ~ExcavateHoleCommand();

	void AddHole(int x, int y, bool bHole);
	void Clear();

	virtual void DoCommand(NiEntityErrorInterface* pkErrors, bool bUndoable);
	virtual void UndoCommand(NiEntityErrorInterface* pkErrors);
};
