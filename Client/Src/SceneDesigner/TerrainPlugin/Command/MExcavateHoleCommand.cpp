#include "TerrainPluginPCH.h"
#include "MExcavateHoleCommand.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

//---------------------------------------------------------------------------
MExcavateHoleCommand::MExcavateHoleCommand(ExcavateHoleCommand* pkCommand) 
	: m_pkCommand(pkCommand)
{
	MInitRefObject(m_pkCommand);
}
//---------------------------------------------------------------------------
void MExcavateHoleCommand::Do_Dispose(bool bDisposing)
{
	MDisposeRefObject(m_pkCommand);
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// ICommand members.
//---------------------------------------------------------------------------
String* MExcavateHoleCommand::get_Name()
{
	MVerifyValidInstance;

	return m_pkCommand->GetName();
}
//---------------------------------------------------------------------------
NiEntityCommandInterface* MExcavateHoleCommand::GetNiEntityCommandInterface()
{
	MVerifyValidInstance;

	return m_pkCommand;
}
//---------------------------------------------------------------------------
void MExcavateHoleCommand::DoCommand(bool bInBatch, bool bUndoable)
{
	MVerifyValidInstance;

	m_pkCommand->DoCommand(NULL, bUndoable);
}
//---------------------------------------------------------------------------
void MExcavateHoleCommand::UndoCommand(bool bInBatch)
{
	MVerifyValidInstance;
	m_pkCommand->UndoCommand(NULL);
}