#include "TerrainPluginPCH.h"
#include "MTerrainEditCommand.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

//---------------------------------------------------------------------------
MTerrainEditCommand::MTerrainEditCommand(TerrainEditCommand* pkCommand) 
	: m_pkCommand(pkCommand)
{
	MInitRefObject(m_pkCommand);
}
//---------------------------------------------------------------------------
void MTerrainEditCommand::Do_Dispose(bool bDisposing)
{
	MDisposeRefObject(m_pkCommand);
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// ICommand members.
//---------------------------------------------------------------------------
String* MTerrainEditCommand::get_Name()
{
	MVerifyValidInstance;

	return m_pkCommand->GetName();
}
//---------------------------------------------------------------------------
NiEntityCommandInterface* MTerrainEditCommand::GetNiEntityCommandInterface()
{
	MVerifyValidInstance;

	return m_pkCommand;
}
//---------------------------------------------------------------------------
void MTerrainEditCommand::DoCommand(bool bInBatch, bool bUndoable)
{
	MVerifyValidInstance;

	m_pkCommand->DoCommand(NULL, bUndoable);
}
//---------------------------------------------------------------------------
void MTerrainEditCommand::UndoCommand(bool bInBatch)
{
	MVerifyValidInstance;
	m_pkCommand->UndoCommand(NULL);
}