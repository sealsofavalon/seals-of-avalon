#pragma once

#include "TerrainEditCommand.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace TerrainPlugin
	{
		using namespace System;
		using namespace System::Collections;
		using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;
		using namespace Emergent::Gamebryo::SceneDesigner::Framework;

		public __gc class MTerrainEditCommand : public MDisposable,
			public ICommand
		{
		public:
			MTerrainEditCommand(TerrainEditCommand* pkCommand);

		private:
			TerrainEditCommand* m_pkCommand;

			// MDisposable members.
		protected:
			virtual void Do_Dispose(bool bDisposing);

			// ICommand members.
		public:
			__property String* get_Name();
			NiEntityCommandInterface* GetNiEntityCommandInterface();
			void DoCommand(bool bInBatch, bool bUndoable);
			void UndoCommand(bool bInBatch);
		};
	}}}}