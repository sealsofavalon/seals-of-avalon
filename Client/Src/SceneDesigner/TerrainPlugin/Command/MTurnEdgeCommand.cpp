#include "TerrainPluginPCH.h"
#include "MTurnEdgeCommand.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

//---------------------------------------------------------------------------
MTurnEdgeCommand::MTurnEdgeCommand(TurnEdgeCommand* pkCommand) 
	: m_pkCommand(pkCommand)
{
	MInitRefObject(m_pkCommand);
}
//---------------------------------------------------------------------------
void MTurnEdgeCommand::Do_Dispose(bool bDisposing)
{
	MDisposeRefObject(m_pkCommand);
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// ICommand members.
//---------------------------------------------------------------------------
String* MTurnEdgeCommand::get_Name()
{
	MVerifyValidInstance;

	return m_pkCommand->GetName();
}
//---------------------------------------------------------------------------
NiEntityCommandInterface* MTurnEdgeCommand::GetNiEntityCommandInterface()
{
	MVerifyValidInstance;

	return m_pkCommand;
}
//---------------------------------------------------------------------------
void MTurnEdgeCommand::DoCommand(bool bInBatch, bool bUndoable)
{
	MVerifyValidInstance;

	m_pkCommand->DoCommand(NULL, bUndoable);
}
//---------------------------------------------------------------------------
void MTurnEdgeCommand::UndoCommand(bool bInBatch)
{
	MVerifyValidInstance;
	m_pkCommand->UndoCommand(NULL);
}