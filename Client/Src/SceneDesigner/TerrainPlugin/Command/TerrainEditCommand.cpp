#include "TerrainPluginPCH.h"
#include "TerrainEditCommand.h"

//---------------------------------------------------------------------------
TerrainEditCommand::TerrainEditCommand()
{
}

//---------------------------------------------------------------------------
TerrainEditCommand::~TerrainEditCommand()
{
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// NiEntityCommandInterface overrides.
//---------------------------------------------------------------------------
void TerrainEditCommand::AddReference()
{
	this->IncRefCount();
}
//---------------------------------------------------------------------------
void TerrainEditCommand::RemoveReference()
{
	this->DecRefCount();
}
//---------------------------------------------------------------------------
NiFixedString TerrainEditCommand::GetName()
{
	return m_kCommandName;
}