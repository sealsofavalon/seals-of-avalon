#pragma once

class TerrainEditCommand : public NiRefObject,
	public NiEntityCommandInterface
{
protected:
	NiFixedString m_kCommandName;

public:
	TerrainEditCommand();
	virtual ~TerrainEditCommand();

	// NiEntityCommandInterface overrides.
	virtual void AddReference();
	virtual void RemoveReference();
	virtual NiFixedString GetName();
};