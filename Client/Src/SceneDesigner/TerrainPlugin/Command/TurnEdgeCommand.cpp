#include "TerrainPluginPCH.h"
#include "TurnEdgeCommand.h"

//---------------------------------------------------------------------------
TurnEdgeCommand::TurnEdgeCommand()
{
	m_kCommandName = "Excavate Hole";
}

//---------------------------------------------------------------------------
TurnEdgeCommand::~TurnEdgeCommand()
{
}

void TurnEdgeCommand::AddTurnEdge(int x, int y, bool bTurnEdge)
{
	TurnEdge kTurnEdge;
	kTurnEdge.m_iX = x;
	kTurnEdge.m_iY = y;
	kTurnEdge.m_bTurnEdge = bTurnEdge;

	m_TurnEdgeList.push_back(kTurnEdge);
}

void TurnEdgeCommand::Clear()
{
	m_TurnEdgeList.clear();
}

void TurnEdgeCommand::DoCommand(NiEntityErrorInterface* pkErrors, bool bUndoable)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	for( size_t i = 0; i < m_TurnEdgeList.size(); i++ )
	{
		pkMap->TurnEdge(m_TurnEdgeList[i].m_iX, m_TurnEdgeList[i].m_iY, m_TurnEdgeList[i].m_bTurnEdge);
	}
}

//---------------------------------------------------------------------------
void TurnEdgeCommand::UndoCommand(NiEntityErrorInterface* pkErrors)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	for( size_t i = 0; i < m_TurnEdgeList.size(); i++ )
	{
		pkMap->TurnEdge(m_TurnEdgeList[i].m_iX, m_TurnEdgeList[i].m_iY, !m_TurnEdgeList[i].m_bTurnEdge);
	}
}