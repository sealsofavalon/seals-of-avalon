#pragma once

#include "TerrainEditCommand.h"
#include <vector>

struct TurnEdge
{
	int m_iX;
	int m_iY;
	bool m_bTurnEdge;
};

class TurnEdgeCommand : public TerrainEditCommand
{
private:
	std::vector<TurnEdge> m_TurnEdgeList;

public:
	TurnEdgeCommand();
	virtual ~TurnEdgeCommand();

	void AddTurnEdge(int x, int y, bool bTurnEdge);
	void Clear();

	virtual void DoCommand(NiEntityErrorInterface* pkErrors, bool bUndoable);
	virtual void UndoCommand(NiEntityErrorInterface* pkErrors);
};