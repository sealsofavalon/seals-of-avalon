
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
	namespace TerrainPlugin {

		public __gc class DayNightControl : public System::Windows::Forms::Form
		{
		private: System::Windows::Forms::CheckBox*  m_bStopTime;
		private: System::Windows::Forms::ListBox*  m_CycleType;

		private: System::Windows::Forms::Label*  m_TimeCycleLable;
		private: System::Windows::Forms::Label*  m_CurrentTime;


		private: System::Windows::Forms::TrackBar*  m_TimeTrackBar;



		public:
			DayNightControl()
			{
				this->InitializeComponent();
			}

			void Do_Dispose(bool bDisposing)
			{
			}

		private: 
			System::Void InitializeComponent() 
			{
				this->m_TimeTrackBar = (new System::Windows::Forms::TrackBar());
				this->m_bStopTime = (new System::Windows::Forms::CheckBox());
				this->m_CycleType = (new System::Windows::Forms::ListBox());
				this->m_TimeCycleLable = (new System::Windows::Forms::Label());
				this->m_CurrentTime = (new System::Windows::Forms::Label());
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_TimeTrackBar))->BeginInit();
				this->SuspendLayout();
				// 
				// m_TimeTrackBar
				// 
				this->m_TimeTrackBar->Location = System::Drawing::Point(12, 12);
				this->m_TimeTrackBar->Maximum = 23;
				this->m_TimeTrackBar->Name = S"m_TimeTrackBar";
				this->m_TimeTrackBar->Size = System::Drawing::Size(326, 45);
				this->m_TimeTrackBar->TabIndex = 0;
				this->m_TimeTrackBar->Value = 12;
				this->m_TimeTrackBar->ValueChanged += new System::EventHandler(this, &DayNightControl::OnValueChanged);
				// 
				// m_bStopTime
				// 
				this->m_bStopTime->AutoSize = true;
				this->m_bStopTime->Checked = true;
				this->m_bStopTime->CheckState = System::Windows::Forms::CheckState::Checked;
				this->m_bStopTime->Location = System::Drawing::Point(23, 95);
				this->m_bStopTime->Name = S"m_bStopTime";
				this->m_bStopTime->Size = System::Drawing::Size(72, 16);
				this->m_bStopTime->TabIndex = 1;
				this->m_bStopTime->Text = S"时间停止";
				this->m_bStopTime->UseVisualStyleBackColor = true;
				this->m_bStopTime->CheckedChanged += new System::EventHandler(this, &DayNightControl::OnCheckedChanged);
				// 
				// m_CycleType
				// 
				this->m_CycleType->Enabled = false;
				this->m_CycleType->ItemHeight = 12;
				System::Object* __mcTemp__1[] = new System::Object*[7];
				__mcTemp__1[0] = S"天";
				__mcTemp__1[1] = S"快速1";
				__mcTemp__1[2] = S"快速2";
				__mcTemp__1[3] = S"快速3";
				__mcTemp__1[4] = S"快速4";
				__mcTemp__1[5] = S"快速5";
				__mcTemp__1[6] = S"快速6";
				this->m_CycleType->Items->AddRange(__mcTemp__1);
				this->m_CycleType->Location = System::Drawing::Point(196, 91);
				this->m_CycleType->Name = S"m_CycleType";
				this->m_CycleType->Size = System::Drawing::Size(121, 100);
				this->m_CycleType->TabIndex = 2;
				this->m_CycleType->SelectedValueChanged += new System::EventHandler(this, &DayNightControl::OnSelectedValueChanged);
				// 
				// m_TimeCycleLable
				// 
				this->m_TimeCycleLable->AutoSize = true;
				this->m_TimeCycleLable->Location = System::Drawing::Point(127, 96);
				this->m_TimeCycleLable->Name = S"m_TimeCycleLable";
				this->m_TimeCycleLable->Size = System::Drawing::Size(53, 12);
				this->m_TimeCycleLable->TabIndex = 3;
				this->m_TimeCycleLable->Text = S"循环时间";
				// 
				// m_CurrentTime
				// 
				this->m_CurrentTime->AutoSize = true;
				this->m_CurrentTime->Location = System::Drawing::Point(21, 60);
				this->m_CurrentTime->Name = S"m_CurrentTime";
				this->m_CurrentTime->Size = System::Drawing::Size(71, 12);
				this->m_CurrentTime->TabIndex = 4;
				this->m_CurrentTime->Text = S"当前时间 12";
				// 
				// DayNightControl
				// 
				this->ClientSize = System::Drawing::Size(343, 242);
				this->Controls->Add(this->m_CurrentTime);
				this->Controls->Add(this->m_TimeCycleLable);
				this->Controls->Add(this->m_CycleType);
				this->Controls->Add(this->m_bStopTime);
				this->Controls->Add(this->m_TimeTrackBar);
				this->Name = S"DayNightControl";
				this->Text = S"时间控制";
				this->VisibleChanged += new System::EventHandler(this, &DayNightControl::OnVisibleChanged);
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_TimeTrackBar))->EndInit();
				this->ResumeLayout(false);
				this->PerformLayout();

			}

		protected: 
			void Dispose(Boolean disposing)
			{
				__super::Dispose(disposing);
			}
		private: 
			System::Void OnCheckedChanged(System::Object*  sender, System::EventArgs*  e) 
			{
				if( m_bStopTime->Checked )
				{
					m_CycleType->Enabled = false;
					m_TimeTrackBar->Enabled = true;
				}
				else
				{
					m_CycleType->Enabled = true;
					m_TimeTrackBar->Enabled = false;
				}

				CTerrainLightManager* pkLightTerrainManager = CTerrainLightManager::Get();
				if( pkLightTerrainManager )
					pkLightTerrainManager->SetStopTime(m_bStopTime->Checked);
			}
			
			System::Void OnValueChanged(System::Object*  sender, System::EventArgs*  e)
			{
				m_CurrentTime->Text = String::Format("当前时间 {0}", m_TimeTrackBar->Value.ToString());

				CTerrainLightManager* pkLightTerrainManager = CTerrainLightManager::Get();
				if( pkLightTerrainManager )
					pkLightTerrainManager->SetTime(m_TimeTrackBar->Value, 0);
			}
			
			System::Void OnVisibleChanged(System::Object*  sender, System::EventArgs*  e)
			{
				CTerrainLightManager* pkLightTerrainManager = CTerrainLightManager::Get();
				if( pkLightTerrainManager )
				{
					unsigned int uiHour;
					unsigned int uiMinute;
					pkLightTerrainManager->GetTime(uiHour, uiMinute);
					m_TimeTrackBar->Value = uiHour;
					switch( pkLightTerrainManager->GetCycleType() )
					{
					case 1:
						this->m_CycleType->SetSelected(1, true);
						break;

					case 2:
						this->m_CycleType->SetSelected(2, true);
						break;

					case 4:
						this->m_CycleType->SetSelected(3, true);
						break;

					case 6:
						this->m_CycleType->SetSelected(4, true);
						break;

					case 10:
						this->m_CycleType->SetSelected(5, true);
						break;

					case 12:
						this->m_CycleType->SetSelected(6, true);
						break;

					default:
						this->m_CycleType->SetSelected(0, true);
						break;
					}
				}
			}
			
			System::Void OnSelectedValueChanged(System::Object*  sender, System::EventArgs*  e) 
			{
				unsigned int uiCycleType = 0;
				switch( this->m_CycleType->SelectedIndex )
				{
				case 1:
					uiCycleType = 1;
					break;

				case 2:
					uiCycleType = 2;
					break;

				case 3:
					uiCycleType = 4;
					break;

				case 4:
					uiCycleType = 6;
					break;

				case 5:
					uiCycleType = 10;
					break;

				case 6:
					uiCycleType = 12;
					break;
				}

				CTerrainLightManager* pkLightTerrainManager = CTerrainLightManager::Get();
				if( pkLightTerrainManager )
					pkLightTerrainManager->SetCycleType(uiCycleType);
			}
};
} }}}