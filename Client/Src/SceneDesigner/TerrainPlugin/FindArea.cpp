#include "TerrainPluginPCH.h"
#include "FindArea.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

System::Void FindAreaDialog::OnFindName(System::Object* sender, System::EventArgs* e)
{
	const char* pcString = MStringToCharPointer(m_NameTextBox->Text);
	NiFixedString kString = pcString;
	MFreeCharPointer(pcString);

	if( kString.GetLength() == 0 )
	{
		MessageBox::Show("������д��������", "����", MessageBoxButtons::OK, MessageBoxIcon::Error);
		return;
	}

	AreaInfo* pkAreaInfo;
	if(g_pkAreaDB->GetAreaInfo(kString, pkAreaInfo))
	{
		m_pkAreaInfo = pkAreaInfo;
		this->DialogResult = DialogResult::OK;
	}
	else
	{
		MessageBox::Show("û���ҵ�����ID", "����", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
}

System::Void FindAreaDialog::OnFindID(System::Object* sender, System::EventArgs* e)
{
	const char* pcString = MStringToCharPointer(m_IDTextBox->Text);
	unsigned int uiAreaID = atoi(pcString);
	MFreeCharPointer(pcString);

	if( uiAreaID == 0 )
	{
		MessageBox::Show("����ID ����", "����", MessageBoxButtons::OK, MessageBoxIcon::Error);
		return;
	}

	AreaInfo* pkAreaInfo;
	if(g_pkAreaDB->GetAreaInfo(uiAreaID, pkAreaInfo))
	{
		m_pkAreaInfo = pkAreaInfo;
		this->DialogResult = DialogResult::OK;
	}
	else
	{
		MessageBox::Show("û���ҵ�����ID", "����", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
}