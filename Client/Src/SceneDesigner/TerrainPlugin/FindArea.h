
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
	namespace TerrainPlugin {

		public __gc class FindAreaDialog : public System::Windows::Forms::Form
		{
		private: System::Windows::Forms::TextBox*  m_NameTextBox;
		private: System::Windows::Forms::Button*  m_NameBtn;
		private: System::Windows::Forms::TextBox*  m_IDTextBox;
		private: System::Windows::Forms::Button*  m_IDBtn;
		private: AreaInfo* m_pkAreaInfo;
		public:
			FindAreaDialog()
			{
				this->InitializeComponent();
			}

			void Do_Dispose(bool bDisposing)
			{
			}

			AreaInfo* GetAreaInfo() { return m_pkAreaInfo; }

		private: 
			System::Void InitializeComponent() 
			{
				this->m_NameTextBox = (new System::Windows::Forms::TextBox());
				this->m_NameBtn = (new System::Windows::Forms::Button());
				this->m_IDTextBox = (new System::Windows::Forms::TextBox());
				this->m_IDBtn = (new System::Windows::Forms::Button());
				this->SuspendLayout();
				// 
				// m_NameTextBox
				// 
				this->m_NameTextBox->Location = System::Drawing::Point(12, 23);
				this->m_NameTextBox->Name = S"m_NameTextBox";
				this->m_NameTextBox->Size = System::Drawing::Size(226, 21);
				this->m_NameTextBox->TabIndex = 0;
				// 
				// m_NameBtn
				// 
				this->m_NameBtn->Location = System::Drawing::Point(244, 21);
				this->m_NameBtn->Name = S"m_NameBtn";
				this->m_NameBtn->Size = System::Drawing::Size(89, 23);
				this->m_NameBtn->TabIndex = 1;
				this->m_NameBtn->Text = S"通过名字查找";
				this->m_NameBtn->UseVisualStyleBackColor = true;
				this->m_NameBtn->Click += new System::EventHandler(this, &FindAreaDialog::OnFindName);
				// 
				// m_IDTextBox
				// 
				this->m_IDTextBox->Location = System::Drawing::Point(12, 71);
				this->m_IDTextBox->Name = S"m_IDTextBox";
				this->m_IDTextBox->Size = System::Drawing::Size(226, 21);
				this->m_IDTextBox->TabIndex = 2;
				// 
				// m_IDBtn
				// 
				this->m_IDBtn->Location = System::Drawing::Point(244, 71);
				this->m_IDBtn->Name = S"m_IDBtn";
				this->m_IDBtn->Size = System::Drawing::Size(89, 23);
				this->m_IDBtn->TabIndex = 3;
				this->m_IDBtn->Text = S"通过ID查找";
				this->m_IDBtn->UseVisualStyleBackColor = true;
				this->m_IDBtn->Click += new System::EventHandler(this, &FindAreaDialog::OnFindID);
				// 
				// FindAreaDialog
				// 
				this->ClientSize = System::Drawing::Size(345, 111);
				this->Controls->Add(this->m_IDBtn);
				this->Controls->Add(this->m_IDTextBox);
				this->Controls->Add(this->m_NameBtn);
				this->Controls->Add(this->m_NameTextBox);
				this->Name = S"FindAreaDialog";
				this->Text = S"Find Area";
				this->ResumeLayout(false);
				this->PerformLayout();

			}

		protected: 
			void Dispose(Boolean disposing)
			{
				__super::Dispose(disposing);
			}

			System::Void OnFindName(System::Object* sender, System::EventArgs* e);
			System::Void OnFindID(System::Object* sender, System::EventArgs* e);
		};
} }}}