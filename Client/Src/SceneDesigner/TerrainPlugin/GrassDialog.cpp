#include "TerrainPluginPCH.h"
#include "TerrainPluginHelper.h"
#include "TerrainEditDialog.h"
#include "GrassDialog.h"


using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void GrassDialog::Dispose(Boolean disposing)
{
    if (disposing && components)
    {
        components->Dispose();
    }

    if(m_TexturePathName)
    {
        NiDelete m_TexturePathName;
        m_TexturePathName = NULL;
    }

    if( m_Bitmap )
    {
        TerrainPluginHelper::UnloadImage(m_Bitmap);
        TerrainPluginHelper::UnloadImage(m_DisplayBitmap);

        m_Bitmap = NULL;
        m_DisplayBitmap = NULL;
    }

    __super::Dispose(disposing);
}

System::Void GrassDialog::OnPictureBoxPaint(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e) 
{
    if( !m_DisplayBitmap )
        return;

    IntPtr hDC = e->Graphics->GetHdc();
    TerrainPluginHelper::DrawImage((HDC)hDC.ToInt32(), 0, 0, m_PictureBox->Width, m_PictureBox->Height, m_DisplayBitmap); 
    e->Graphics->ReleaseHdc(hDC);

    Pen* RedPen = new Pen(Color::FromArgb(255, 255, 0, 0), 4);
    for(int idx = 0; idx < 16; ++idx)
    {
        if(m_bSelectedGrass[idx])
        {
            int iX = idx / m_uiXCount;
            int iY = idx % m_uiXCount;
            int iWidth = m_PictureBox->Width / m_uiXCount;
            int iHeight = m_PictureBox->Height / m_uiYCount;

            e->Graphics->DrawRectangle(RedPen, iX*iWidth + 4, iY*iHeight + 4, iWidth - 10, iHeight - 10);
        }
    }
}

System::Void GrassDialog::OnPictureBoxMouseDown(System::Object*  sender, System::Windows::Forms::MouseEventArgs* e)
{
	if( !m_Bitmap )
		return;

    int iWidth = m_PictureBox->Width / m_uiXCount;
    int iHeight = m_PictureBox->Height / m_uiYCount;

    unsigned int uiIdx = e->X / iWidth * m_uiXCount + e->Y / iHeight;
    NIASSERT(uiIdx < 16);
    m_bSelectedGrass[uiIdx] = !m_bSelectedGrass[uiIdx];
    m_uiGrassCount = 0;
    for(int i = 0; i < 16; ++i)
    {
        if(m_bSelectedGrass[i])
            ++m_uiGrassCount;
    }

	m_PictureBox->Invalidate();
}

System::Void GrassDialog::OnPictureBoxMouseHover(System::Object*  sender, System::EventArgs*  e)
{
	if( !m_TexturePathName )
		return;

	this->m_ToolTip->RemoveAll();
	this->m_ToolTip->Show(*m_TexturePathName, this->m_PictureBox, 1000 * 20);
}

System::Void GrassDialog::OnChangeGrass(System::Object*  sender, System::EventArgs*  e)
{
    char AbsPathName[1024];
    if( !TerrainPluginHelper::GetLoadPathName(AbsPathName, "grass texture file (*.DDS)|*.DDS") )
        return;

    //转到相对路径
    char PathName[1024];
    if( !TerrainHelper::ConvertToRelative(PathName, 1024, AbsPathName) )
        return;

    if( m_Bitmap )
    {
        TerrainPluginHelper::UnloadImage(m_Bitmap);
        TerrainPluginHelper::UnloadImage(m_DisplayBitmap);
    }

    m_Bitmap = TerrainPluginHelper::LoadImage(AbsPathName);
    m_DisplayBitmap = TerrainPluginHelper::RescaleImage(m_Bitmap, m_PictureBox->Width, m_PictureBox->Height);
    m_PictureBox->Invalidate();

    *m_TexturePathName = PathName;

    CMap* pkMap = CMap::Get();
    if(pkMap)
    {
        CGrassMesh& kGrassMesh = pkMap->GetGrassMesh();
        kGrassMesh.SetTextureName(PathName);
    }
}

System::Void GrassDialog::OnDeleteGrass(System::Object*  sender, System::EventArgs*  e)
{
    //if(::MessageBox((HWND)this->Handle.ToInt32(), "Delete grass\n Are you continue?", "Warning", MB_OKCANCEL | MB_ICONWARNING) != IDOK)
    //    return;

    //if( m_Bitmap )
    //{
    //    TerrainPluginHelper::UnloadImage(m_Bitmap);
    //    TerrainPluginHelper::UnloadImage(m_DisplayBitmap);

    //    m_Bitmap = NULL;
    //    m_DisplayBitmap = NULL;
    //}

}

System::Void GrassDialog::OnSetXCount(System::Object*  sender, System::EventArgs*  e)
{
    int uiXCount = 1;
    if(sender == m_XRadioButton2)
        uiXCount = 2;
    else if(sender == m_XRadioButton3)
        uiXCount = 3;
    else if(sender == m_XRadioButton4)
        uiXCount = 4;

    m_uiXCount = uiXCount;
    m_PictureBox->Invalidate();

    CMap* pkMap = CMap::Get();
    if(pkMap)
    {
        CGrassMesh& kGrassMesh = pkMap->GetGrassMesh();
        kGrassMesh.SetXYCount(m_uiXCount, m_uiYCount);
    }
}

System::Void GrassDialog::OnSetYCount(System::Object*  sender, System::EventArgs*  e)
{
    int uiYCount = 1;
    if(sender == m_YRadioButton2)
        uiYCount = 2;
    else if(sender == m_YRadioButton3)
        uiYCount = 3;
    else if(sender == m_YRadioButton4)
        uiYCount = 4;

    m_uiYCount = uiYCount;
    m_PictureBox->Invalidate();

    CMap* pkMap = CMap::Get();
    if(pkMap)
    {
        CGrassMesh& kGrassMesh = pkMap->GetGrassMesh();
        kGrassMesh.SetXYCount(m_uiXCount, m_uiYCount);
    }
}

void GrassDialog::UpdateGrassControls()
{
    CMap* pkMap = CMap::Get();
    if(pkMap)
    {
        CGrassMesh& kGrassMesh = pkMap->GetGrassMesh();
        unsigned int uiXCount, uiYCount;
        kGrassMesh.GetXYCount(uiXCount, uiYCount);
        m_uiXCount = uiXCount;
        m_uiYCount = uiYCount;
        switch(m_uiXCount)
        {
        case 1:
            m_XRadioButton1->Checked = true;
            break;

        case 2:
            m_XRadioButton2->Checked = true;
            break;

        case 3:
            m_XRadioButton3->Checked = true;
            break;

        case 4:
            m_XRadioButton4->Checked = true;
            break;
        }

        switch(m_uiYCount)
        {
        case 1:
            m_YRadioButton1->Checked = true;
            break;

        case 2:
            m_YRadioButton2->Checked = true;
            break;

        case 3:
            m_YRadioButton3->Checked = true;
            break;

        case 4:
            m_YRadioButton4->Checked = true;
            break;
        }


        //更新贴图
        const NiFixedString& PathName = kGrassMesh.GetTextureName();
        char AbsPathName[1024];
        NiSprintf(AbsPathName, 1024, "%s\\%s", TerrainHelper::GetClientPath(), PathName);

        if( m_Bitmap )
        {
            TerrainPluginHelper::UnloadImage(m_Bitmap);
            TerrainPluginHelper::UnloadImage(m_DisplayBitmap);
        }

        m_Bitmap = TerrainPluginHelper::LoadImage(AbsPathName);
        m_DisplayBitmap = TerrainPluginHelper::RescaleImage(m_Bitmap, m_PictureBox->Width, m_PictureBox->Height);
        *m_TexturePathName = PathName;

        m_PictureBox->Invalidate();
    }
}

unsigned short GrassDialog::GenerateGrass()
{
    unsigned int uiSize = m_MinSizeTrackBar->Value;
    if( m_MaxSizeTrackBar->Value > m_MinSizeTrackBar->Value )
        uiSize += (unsigned int)(NiUnitRandom()*float(m_MaxSizeTrackBar->Value - m_MinSizeTrackBar->Value) + 0.5f);

    unsigned int uiRot = unsigned int(NiUnitRandom()*7 + 0.5f);
    unsigned int uiIdx = 0;
    if(m_uiGrassCount > 1)
    {
        int n = int(NiUnitRandom()*(m_uiGrassCount - 1) + 0.5f);
        for(int i = 0; i < 16; ++i)
        {
            if(m_bSelectedGrass[i])
            {
                if(n-- == 0)
                {
                    uiIdx = i;
                    break;
                }
            }
        }
    }
    else
    {
        for(int i = 0; i < 16; ++i)
        {
            if(m_bSelectedGrass[i])
            {
                uiIdx = i;
                break;
            }
        }
    }

    unsigned int offset = (unsigned int)(NiUnitRandom()*7);
    return MakeGrass(uiIdx, uiSize, uiRot, offset);
}