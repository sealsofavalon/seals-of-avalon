
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
    namespace TerrainPlugin {

    public __gc class GrassDialog : public System::Windows::Forms::Form
    {
    private:
        FIBITMAP* m_Bitmap; //原始的Bitmap
		FIBITMAP* m_DisplayBitmap; //经过缩放后用来显示的Bitmap
		NiFixedString* m_TexturePathName;
        unsigned int m_uiXCount;
        unsigned int m_uiYCount;
        bool m_bSelectedGrass __gc [];
        unsigned int m_uiGrassCount;
        unsigned int m_uiGrassInterval; //间距1 到 8
        unsigned int m_uiGrassDensity; //密度10% - 100%

        static GrassDialog* ms_pmThis;

    private: System::Windows::Forms::GroupBox*  m_GrassSizeBox;
    private: System::Windows::Forms::Label*  m_MinSizeLabel;
    private: System::Windows::Forms::TrackBar*  m_MinSizeTrackBar;
    private: System::Windows::Forms::TrackBar*  m_MaxSizeTrackBar;
    private: System::Windows::Forms::Label*  m_MaxSizeLabel;
    
    private: System::Windows::Forms::Label*  m_DensityLable;
    private: System::Windows::Forms::TrackBar*  m_DensityTrackBar;

    private: System::Windows::Forms::Label*  m_IntervalLable;
    private: System::Windows::Forms::TrackBar*  m_IntervalTrackBar;
    public:
        GrassDialog(void)
        {
            ms_pmThis = this;
            m_uiXCount = 4;
            m_uiYCount = 4;
            m_uiGrassInterval = 1;
            m_uiGrassDensity = 10; //10%
            m_TexturePathName = NiNew NiFixedString;

            m_bSelectedGrass = new bool __gc[16];
            m_uiGrassCount = 0;
            for(int i = 0; i < 16; ++i)
            {
                m_bSelectedGrass[i] = false;
            }

            InitializeComponent();
        }

        void Do_Dispose(Boolean disposing)
        {
            Dispose(disposing);
        }

    protected:
        void Dispose(Boolean disposing);
        
        System::Void OnPictureBoxPaint(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e);
        System::Void OnPictureBoxMouseDown(System::Object*  sender, System::Windows::Forms::MouseEventArgs* e);
        System::Void OnPictureBoxMouseHover(System::Object*  sender, System::EventArgs*  e);
        System::Void OnChangeGrass(System::Object*  sender, System::EventArgs*  e);
        System::Void OnDeleteGrass(System::Object*  sender, System::EventArgs*  e);

        System::Void OnSetXCount(System::Object*  sender, System::EventArgs*  e);
        System::Void OnSetYCount(System::Object*  sender, System::EventArgs*  e);

    private: System::Windows::Forms::PictureBox* m_PictureBox;
    private: System::Windows::Forms::Button*  m_ChangeGrassBtn;
    private: System::Windows::Forms::Button*  m_DeleteGrassBtn;

    private: System::Windows::Forms::GroupBox*  m_XGroupBox;
    private: System::Windows::Forms::RadioButton*  m_XRadioButton1;
    private: System::Windows::Forms::RadioButton*  m_XRadioButton2;
    private: System::Windows::Forms::RadioButton*  m_XRadioButton3;
    private: System::Windows::Forms::RadioButton*  m_XRadioButton4;

    private: System::Windows::Forms::GroupBox*  m_YGroupBox;
    private: System::Windows::Forms::RadioButton*  m_YRadioButton1;
    private: System::Windows::Forms::RadioButton*  m_YRadioButton2;
    private: System::Windows::Forms::RadioButton*  m_YRadioButton3;
    private: System::Windows::Forms::RadioButton*  m_YRadioButton4;
    private: System::Windows::Forms::ToolTip* m_ToolTip;
    private: System::ComponentModel::IContainer*  components;

    private:
        void InitializeComponent(void)
        {
            this->components = (new System::ComponentModel::Container());
            this->m_PictureBox = (new System::Windows::Forms::PictureBox());
            this->m_ChangeGrassBtn = (new System::Windows::Forms::Button());
            this->m_XGroupBox = (new System::Windows::Forms::GroupBox());
            this->m_XRadioButton4 = (new System::Windows::Forms::RadioButton());
            this->m_XRadioButton3 = (new System::Windows::Forms::RadioButton());
            this->m_XRadioButton2 = (new System::Windows::Forms::RadioButton());
            this->m_XRadioButton1 = (new System::Windows::Forms::RadioButton());
            this->m_YGroupBox = (new System::Windows::Forms::GroupBox());
            this->m_YRadioButton4 = (new System::Windows::Forms::RadioButton());
            this->m_YRadioButton3 = (new System::Windows::Forms::RadioButton());
            this->m_YRadioButton2 = (new System::Windows::Forms::RadioButton());
            this->m_YRadioButton1 = (new System::Windows::Forms::RadioButton());
            this->m_ToolTip = (new System::Windows::Forms::ToolTip(this->components));
            this->m_DeleteGrassBtn = (new System::Windows::Forms::Button());
            this->m_GrassSizeBox = (new System::Windows::Forms::GroupBox());
            this->m_MaxSizeLabel = (new System::Windows::Forms::Label());
            this->m_MinSizeLabel = (new System::Windows::Forms::Label());
            this->m_MaxSizeTrackBar = (new System::Windows::Forms::TrackBar());
            this->m_MinSizeTrackBar = (new System::Windows::Forms::TrackBar());
            this->m_IntervalLable = (new System::Windows::Forms::Label());
            this->m_IntervalTrackBar = (new System::Windows::Forms::TrackBar());
            this->m_DensityLable = (new System::Windows::Forms::Label());
            this->m_DensityTrackBar = (new System::Windows::Forms::TrackBar());
            (__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_PictureBox))->BeginInit();
            this->m_XGroupBox->SuspendLayout();
            this->m_YGroupBox->SuspendLayout();
            this->m_GrassSizeBox->SuspendLayout();
            (__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_MaxSizeTrackBar))->BeginInit();
            (__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_MinSizeTrackBar))->BeginInit();
            (__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_IntervalTrackBar))->BeginInit();
            (__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_DensityTrackBar))->BeginInit();
            this->SuspendLayout();
            // 
            // m_PictureBox
            // 
            this->m_PictureBox->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
            this->m_PictureBox->Location = System::Drawing::Point(22, 22);
            this->m_PictureBox->Name = S"m_PictureBox";
            this->m_PictureBox->Size = System::Drawing::Size(256, 256);
            this->m_PictureBox->TabIndex = 0;
            this->m_PictureBox->TabStop = false;
            this->m_PictureBox->MouseDown += new System::Windows::Forms::MouseEventHandler(this, &GrassDialog::OnPictureBoxMouseDown);
            this->m_PictureBox->MouseHover += new System::EventHandler(this, &GrassDialog::OnPictureBoxMouseHover);
            this->m_PictureBox->Paint += new System::Windows::Forms::PaintEventHandler(this, &GrassDialog::OnPictureBoxPaint);
            // 
            // m_ChangeGrassBtn
            // 
            this->m_ChangeGrassBtn->Location = System::Drawing::Point(36, 281);
            this->m_ChangeGrassBtn->Name = S"m_ChangeGrassBtn";
            this->m_ChangeGrassBtn->Size = System::Drawing::Size(103, 23);
            this->m_ChangeGrassBtn->TabIndex = 1;
            this->m_ChangeGrassBtn->Text = S"Change Grass";
            this->m_ChangeGrassBtn->UseVisualStyleBackColor = true;
            this->m_ChangeGrassBtn->Click += new System::EventHandler(this, &GrassDialog::OnChangeGrass);
            // 
            // m_XGroupBox
            // 
            this->m_XGroupBox->Controls->Add(this->m_XRadioButton4);
            this->m_XGroupBox->Controls->Add(this->m_XRadioButton3);
            this->m_XGroupBox->Controls->Add(this->m_XRadioButton2);
            this->m_XGroupBox->Controls->Add(this->m_XRadioButton1);
            this->m_XGroupBox->Location = System::Drawing::Point(5, 310);
            this->m_XGroupBox->Name = S"m_XGroupBox";
            this->m_XGroupBox->Size = System::Drawing::Size(142, 47);
            this->m_XGroupBox->TabIndex = 2;
            this->m_XGroupBox->TabStop = false;
            this->m_XGroupBox->Text = S"X";
            // 
            // m_XRadioButton4
            // 
            this->m_XRadioButton4->AutoSize = true;
            this->m_XRadioButton4->Checked = true;
            this->m_XRadioButton4->Location = System::Drawing::Point(109, 19);
            this->m_XRadioButton4->Name = S"m_XRadioButton4";
            this->m_XRadioButton4->Size = System::Drawing::Size(29, 16);
            this->m_XRadioButton4->TabIndex = 3;
            this->m_XRadioButton4->TabStop = true;
            this->m_XRadioButton4->Text = S"4";
            this->m_XRadioButton4->UseVisualStyleBackColor = true;
            this->m_XRadioButton4->Click += new System::EventHandler(this, &GrassDialog::OnSetXCount);
            // 
            // m_XRadioButton3
            // 
            this->m_XRadioButton3->AutoSize = true;
            this->m_XRadioButton3->Location = System::Drawing::Point(74, 19);
            this->m_XRadioButton3->Name = S"m_XRadioButton3";
            this->m_XRadioButton3->Size = System::Drawing::Size(29, 16);
            this->m_XRadioButton3->TabIndex = 2;
            this->m_XRadioButton3->TabStop = true;
            this->m_XRadioButton3->Text = S"3";
            this->m_XRadioButton3->UseVisualStyleBackColor = true;
            this->m_XRadioButton3->Click += new System::EventHandler(this, &GrassDialog::OnSetXCount);
            // 
            // m_XRadioButton2
            // 
            this->m_XRadioButton2->AutoSize = true;
            this->m_XRadioButton2->Location = System::Drawing::Point(39, 19);
            this->m_XRadioButton2->Name = S"m_XRadioButton2";
            this->m_XRadioButton2->Size = System::Drawing::Size(29, 16);
            this->m_XRadioButton2->TabIndex = 1;
            this->m_XRadioButton2->TabStop = true;
            this->m_XRadioButton2->Text = S"2";
            this->m_XRadioButton2->UseVisualStyleBackColor = true;
            this->m_XRadioButton2->Click += new System::EventHandler(this, &GrassDialog::OnSetXCount);
            // 
            // m_XRadioButton1
            // 
            this->m_XRadioButton1->AutoSize = true;
            this->m_XRadioButton1->Location = System::Drawing::Point(4, 19);
            this->m_XRadioButton1->Name = S"m_XRadioButton1";
            this->m_XRadioButton1->Size = System::Drawing::Size(29, 16);
            this->m_XRadioButton1->TabIndex = 0;
            this->m_XRadioButton1->TabStop = true;
            this->m_XRadioButton1->Text = S"1";
            this->m_XRadioButton1->UseVisualStyleBackColor = true;
            this->m_XRadioButton1->Click += new System::EventHandler(this, &GrassDialog::OnSetXCount);
            // 
            // m_YGroupBox
            // 
            this->m_YGroupBox->Controls->Add(this->m_YRadioButton4);
            this->m_YGroupBox->Controls->Add(this->m_YRadioButton3);
            this->m_YGroupBox->Controls->Add(this->m_YRadioButton2);
            this->m_YGroupBox->Controls->Add(this->m_YRadioButton1);
            this->m_YGroupBox->Location = System::Drawing::Point(153, 310);
            this->m_YGroupBox->Name = S"m_YGroupBox";
            this->m_YGroupBox->Size = System::Drawing::Size(142, 47);
            this->m_YGroupBox->TabIndex = 3;
            this->m_YGroupBox->TabStop = false;
            this->m_YGroupBox->Text = S"Y";
            // 
            // m_YRadioButton4
            // 
            this->m_YRadioButton4->AutoSize = true;
            this->m_YRadioButton4->Checked = true;
            this->m_YRadioButton4->Location = System::Drawing::Point(109, 20);
            this->m_YRadioButton4->Name = S"m_YRadioButton4";
            this->m_YRadioButton4->Size = System::Drawing::Size(29, 16);
            this->m_YRadioButton4->TabIndex = 3;
            this->m_YRadioButton4->TabStop = true;
            this->m_YRadioButton4->Text = S"4";
            this->m_YRadioButton4->UseVisualStyleBackColor = true;
            this->m_YRadioButton4->Click += new System::EventHandler(this, &GrassDialog::OnSetYCount);
            // 
            // m_YRadioButton3
            // 
            this->m_YRadioButton3->AutoSize = true;
            this->m_YRadioButton3->Location = System::Drawing::Point(74, 20);
            this->m_YRadioButton3->Name = S"m_YRadioButton3";
            this->m_YRadioButton3->Size = System::Drawing::Size(29, 16);
            this->m_YRadioButton3->TabIndex = 2;
            this->m_YRadioButton3->TabStop = true;
            this->m_YRadioButton3->Text = S"3";
            this->m_YRadioButton3->UseVisualStyleBackColor = true;
            this->m_YRadioButton3->Click += new System::EventHandler(this, &GrassDialog::OnSetYCount);
            // 
            // m_YRadioButton2
            // 
            this->m_YRadioButton2->AutoSize = true;
            this->m_YRadioButton2->Location = System::Drawing::Point(39, 20);
            this->m_YRadioButton2->Name = S"m_YRadioButton2";
            this->m_YRadioButton2->Size = System::Drawing::Size(29, 16);
            this->m_YRadioButton2->TabIndex = 1;
            this->m_YRadioButton2->TabStop = true;
            this->m_YRadioButton2->Text = S"2";
            this->m_YRadioButton2->UseVisualStyleBackColor = true;
            this->m_YRadioButton2->Click += new System::EventHandler(this, &GrassDialog::OnSetYCount);
            // 
            // m_YRadioButton1
            // 
            this->m_YRadioButton1->AutoSize = true;
            this->m_YRadioButton1->Location = System::Drawing::Point(4, 20);
            this->m_YRadioButton1->Name = S"m_YRadioButton1";
            this->m_YRadioButton1->Size = System::Drawing::Size(29, 16);
            this->m_YRadioButton1->TabIndex = 0;
            this->m_YRadioButton1->TabStop = true;
            this->m_YRadioButton1->Text = S"1";
            this->m_YRadioButton1->UseVisualStyleBackColor = true;
            this->m_YRadioButton1->Click += new System::EventHandler(this, &GrassDialog::OnSetYCount);
            // 
            // m_ToolTip
            // 
            this->m_ToolTip->AutoPopDelay = 5000;
            this->m_ToolTip->InitialDelay = 200;
            this->m_ToolTip->ReshowDelay = 100;
            this->m_ToolTip->ShowAlways = true;
            // 
            // m_DeleteGrassBtn
            // 
            this->m_DeleteGrassBtn->Enabled = false;
            this->m_DeleteGrassBtn->Location = System::Drawing::Point(162, 281);
            this->m_DeleteGrassBtn->Name = S"m_DeleteGrassBtn";
            this->m_DeleteGrassBtn->Size = System::Drawing::Size(103, 23);
            this->m_DeleteGrassBtn->TabIndex = 4;
            this->m_DeleteGrassBtn->Text = S"Delete Grass";
            this->m_DeleteGrassBtn->UseVisualStyleBackColor = true;
            // 
            // m_GrassSizeBox
            // 
            this->m_GrassSizeBox->Controls->Add(this->m_MaxSizeLabel);
            this->m_GrassSizeBox->Controls->Add(this->m_MinSizeLabel);
            this->m_GrassSizeBox->Controls->Add(this->m_MaxSizeTrackBar);
            this->m_GrassSizeBox->Controls->Add(this->m_MinSizeTrackBar);
            this->m_GrassSizeBox->Location = System::Drawing::Point(5, 362);
            this->m_GrassSizeBox->Name = S"m_GrassSizeBox";
            this->m_GrassSizeBox->Size = System::Drawing::Size(290, 105);
            this->m_GrassSizeBox->TabIndex = 5;
            this->m_GrassSizeBox->TabStop = false;
            this->m_GrassSizeBox->Text = S"大小范围";
            // 
            // m_MaxSizeLabel
            // 
            this->m_MaxSizeLabel->AutoSize = true;
            this->m_MaxSizeLabel->Location = System::Drawing::Point(7, 66);
            this->m_MaxSizeLabel->Name = S"m_MaxSizeLabel";
            this->m_MaxSizeLabel->Size = System::Drawing::Size(35, 12);
            this->m_MaxSizeLabel->TabIndex = 3;
            this->m_MaxSizeLabel->Text = S"最大1";
            // 
            // m_MinSizeLabel
            // 
            this->m_MinSizeLabel->AutoSize = true;
            this->m_MinSizeLabel->Location = System::Drawing::Point(7, 26);
            this->m_MinSizeLabel->Name = S"m_MinSizeLabel";
            this->m_MinSizeLabel->Size = System::Drawing::Size(47, 12);
            this->m_MinSizeLabel->TabIndex = 2;
            this->m_MinSizeLabel->Text = S"最小0.4";
            // 
            // m_MaxSizeTrackBar
            // 
            this->m_MaxSizeTrackBar->LargeChange = 1;
            this->m_MaxSizeTrackBar->Location = System::Drawing::Point(60, 56);
            this->m_MaxSizeTrackBar->Maximum = 15;
            this->m_MaxSizeTrackBar->Name = S"m_MaxSizeTrackBar";
            this->m_MaxSizeTrackBar->Size = System::Drawing::Size(226, 45);
            this->m_MaxSizeTrackBar->TabIndex = 1;
            this->m_MaxSizeTrackBar->Value = 5;
            this->m_MaxSizeTrackBar->ValueChanged += new System::EventHandler(this, &GrassDialog::OnMaxSizeValueChanged);
            // 
            // m_MinSizeTrackBar
            // 
            this->m_MinSizeTrackBar->LargeChange = 1;
            this->m_MinSizeTrackBar->Location = System::Drawing::Point(60, 16);
            this->m_MinSizeTrackBar->Maximum = 15;
            this->m_MinSizeTrackBar->Name = S"m_MinSizeTrackBar";
            this->m_MinSizeTrackBar->Size = System::Drawing::Size(226, 45);
            this->m_MinSizeTrackBar->TabIndex = 0;
            this->m_MinSizeTrackBar->Value = 2;
            this->m_MinSizeTrackBar->ValueChanged += new System::EventHandler(this, &GrassDialog::OnMinSizeValueChanged);
            // 
            // m_IntervalLable
            // 
            this->m_IntervalLable->AutoSize = true;
            this->m_IntervalLable->Location = System::Drawing::Point(7, 495);
            this->m_IntervalLable->Name = S"m_IntervalLable";
            this->m_IntervalLable->Size = System::Drawing::Size(41, 12);
            this->m_IntervalLable->TabIndex = 7;
            this->m_IntervalLable->Text = S"间距 1";
            // 
            // m_IntervalTrackBar
            // 
            this->m_IntervalTrackBar->LargeChange = 1;
            this->m_IntervalTrackBar->Location = System::Drawing::Point(65, 485);
            this->m_IntervalTrackBar->Maximum = 8;
            this->m_IntervalTrackBar->Minimum = 1;
            this->m_IntervalTrackBar->Name = S"m_IntervalTrackBar";
            this->m_IntervalTrackBar->Size = System::Drawing::Size(218, 45);
            this->m_IntervalTrackBar->TabIndex = 5;
            this->m_IntervalTrackBar->Value = 1;
            this->m_IntervalTrackBar->ValueChanged += new System::EventHandler(this, &GrassDialog::OnIntervalValueChanged);
            // 
            // m_DensityLable
            // 
            this->m_DensityLable->AutoSize = true;
            this->m_DensityLable->Location = System::Drawing::Point(7, 536);
            this->m_DensityLable->Name = S"m_DensityLable";
            this->m_DensityLable->Size = System::Drawing::Size(59, 12);
            this->m_DensityLable->TabIndex = 7;
            this->m_DensityLable->Text = S"密度 10%";
            // 
            // m_DensityTrackBar
            // 
            this->m_DensityTrackBar->LargeChange = 1;
            this->m_DensityTrackBar->Location = System::Drawing::Point(65, 529);
            this->m_DensityTrackBar->Maximum = 50;
            this->m_DensityTrackBar->Minimum = 1;
            this->m_DensityTrackBar->Name = S"m_DensityTrackBar";
            this->m_DensityTrackBar->Size = System::Drawing::Size(218, 45);
            this->m_DensityTrackBar->TabIndex = 8;
            this->m_DensityTrackBar->Value = 5;
            this->m_DensityTrackBar->ValueChanged += new System::EventHandler(this, &GrassDialog::OnDensityValueChanged);
            // 
            // GrassDialog
            // 
            this->ClientSize = System::Drawing::Size(300, 628);
            this->Controls->Add(this->m_IntervalTrackBar);
            this->Controls->Add(this->m_IntervalLable);
            this->Controls->Add(this->m_DensityTrackBar);
            this->Controls->Add(this->m_DensityLable);
            this->Controls->Add(this->m_GrassSizeBox);
            this->Controls->Add(this->m_YGroupBox);
            this->Controls->Add(this->m_XGroupBox);
            this->Controls->Add(this->m_ChangeGrassBtn);
            this->Controls->Add(this->m_DeleteGrassBtn);
            this->Controls->Add(this->m_PictureBox);
            this->Name = S"GrassDialog";
            this->Text = S"Grass Dialog";
            (__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_PictureBox))->EndInit();
            this->m_XGroupBox->ResumeLayout(false);
            this->m_XGroupBox->PerformLayout();
            this->m_YGroupBox->ResumeLayout(false);
            this->m_YGroupBox->PerformLayout();
            this->m_GrassSizeBox->ResumeLayout(false);
            this->m_GrassSizeBox->PerformLayout();
            (__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_MaxSizeTrackBar))->EndInit();
            (__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_MinSizeTrackBar))->EndInit();
            (__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_IntervalTrackBar))->EndInit();
            (__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_DensityTrackBar))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
    private: System::Void OnMinSizeValueChanged(System::Object*  sender, System::EventArgs*  e)
             {
                 float fSize = Id2Size(m_MinSizeTrackBar->Value);
                 m_MinSizeLabel->Text = String::Format("最小{0:#0.#}", __box(fSize));
             }

    private: System::Void OnMaxSizeValueChanged(System::Object*  sender, System::EventArgs*  e) 
             {
                 float fSize = Id2Size(m_MaxSizeTrackBar->Value);
                 m_MaxSizeLabel->Text = String::Format("最大{0:#0.#}", __box(fSize));
             }

    private: System::Void OnIntervalValueChanged(System::Object*  sender, System::EventArgs*  e) 
             {
                 m_uiGrassInterval = (unsigned int)m_IntervalTrackBar->Value;
                 m_IntervalLable->Text = String::Format("间距 {0}", __box(m_uiGrassInterval));
             }

    private: System::Void OnDensityValueChanged(System::Object*  sender, System::EventArgs*  e) 
             {
                 m_uiGrassDensity = (unsigned int)m_DensityTrackBar->Value*2;
                 m_DensityLable->Text = String::Format("密度 {0}%", __box(m_uiGrassDensity));
             }

    public:
        void UpdateGrassControls();
        unsigned short GenerateGrass();
        unsigned int GetGrassInterval() { return m_uiGrassInterval; }
        unsigned int GetGrassDensity() { return m_uiGrassDensity; }
        static GrassDialog* Get() { return ms_pmThis; }
    };
} }}}