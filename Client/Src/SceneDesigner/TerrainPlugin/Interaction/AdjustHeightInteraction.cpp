
#include "TerrainPluginPCH.h"
#include "AdjustHeightInteraction.h"
#include "TerrainEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MAdjustHeightInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );

	NiDelete m_pkIntersectionPosition;
	NiDelete m_pkIntersectionPlane;
}

MAdjustHeightInteractionMode::MAdjustHeightInteractionMode()
{
	m_bAlreadyBegin = false;

	m_pkIntersectionPosition = NiNew NiPoint3;
	m_pkIntersectionPlane =	NiNew NiPlane;
}

String* MAdjustHeightInteractionMode::get_Name()
{
    return "Adjust Height";
}

void MAdjustHeightInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MAdjustHeightInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndAdjustHeight(iX, iY);
	}
}

void MAdjustHeightInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			AdjustHeight(iX, iY);
		}
		else
		{
			BeginAdjustHeight(iX, iY);
		}

		return;
	}

	__super::MouseMove(iX, iY);
}

void MAdjustHeightInteractionMode::BeginAdjustHeight(int iX, int iY)
{	
	if( !m_pkMouseBrush )
		return;

	if( !CollideMouse(iX, iY, *m_pkIntersectionPosition) )
		return;

	m_bAlreadyBegin = true;

	//计算摄像机的碰撞面
	NiPoint3 kOrigin, kDir;
	NiViewMath::MouseToRay((float) iX, (float) iY, 
		MFramework::Instance->ViewportManager->ActiveViewport->Width,
		MFramework::Instance->ViewportManager->ActiveViewport->Height,
		MFramework::Instance->ViewportManager->ActiveViewport->
		GetNiCamera(), kOrigin, kDir);

	NiPoint3 kCross = kDir.UnitCross( NiPoint3(0.f, 0.f, 1.f) );
	NiPoint3 kNormal = NiPoint3(0.f, 0.f, 1.f).UnitCross( kCross );

	*m_pkIntersectionPlane = NiPlane(kNormal, *m_pkIntersectionPosition);

	//保存原始值
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		amSelectedPoints[i]->m_fStartHeight = amSelectedPoints[i]->m_fHeight;
	}

	BeginChangeHeightCommand();
}

void MAdjustHeightInteractionMode::AdjustHeight(int iX, int iY)
{
	NiPoint3 kOrigin, kDir;
	NiViewMath::MouseToRay((float) iX, (float) iY, 
		MFramework::Instance->ViewportManager->ActiveViewport->Width,
		MFramework::Instance->ViewportManager->ActiveViewport->Height,
		MFramework::Instance->ViewportManager->ActiveViewport->
		GetNiCamera(), kOrigin, kDir);

	float fTotal = m_pkIntersectionPlane->GetNormal().Dot(kDir * MAX_DISTANCE);
	if( fTotal == 0.f )
		return;

	float fDistance = m_pkIntersectionPlane->Distance(kOrigin);

	float t = -fDistance / fTotal;
	t = NiClamp(t, 0.f, 1.f);

	NiPoint3 kPos = kOrigin + kDir * (MAX_DISTANCE * t);

	float fDiff = kPos.z - m_pkIntersectionPosition->z;

	//修改高度值
	CMap* pkMap = CMap::Get();
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		amSelectedPoints[i]->m_fHeight = amSelectedPoints[i]->m_fStartHeight + amSelectedPoints[i]->m_fWeight * fDiff;
		pkMap->SetHeight(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), amSelectedPoints[i]->m_fHeight);
	}

	//更新Brush
	m_pkMouseBrush->UpdateBrush();
}

void MAdjustHeightInteractionMode::EndAdjustHeight(int iX, int iY)
{
	EndChangeHeightCommand();

	m_bAlreadyBegin = false;
}

void MAdjustHeightInteractionMode::Active()
{
	__super::Active();

	m_ParamDlg->ParamTextBox->Text = "";
	m_ParamDlg->ParamTrackBar->Value = 0;
	m_ParamDlg->ParamLable->Text = "";
	m_ParamDlg->ParamGroupBox->Enabled = false;
}

void MAdjustHeightInteractionMode::Deactive()
{
	m_ParamDlg->ParamGroupBox->Enabled = true;

	__super::Deactive();
}
