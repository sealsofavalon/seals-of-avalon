#pragma once

#include "TerrainEditInteraction.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{
    public __gc class MAdjustHeightInteractionMode : public MTerrainEditInteractionMode
    {
    protected:
        virtual void Do_Dispose(bool bDisposing);

	public:
		MAdjustHeightInteractionMode();

		__property String* get_Name();

		void MouseDown(MouseButtonType eType, int iX, int iY);
        void MouseUp(MouseButtonType eType, int iX, int iY);
		void MouseMove(int iX, int iY);

		void BeginAdjustHeight(int iX, int iY);
		void AdjustHeight(int iX, int iY);
		void EndAdjustHeight(int iX, int iY);

		virtual void Active();
		virtual void Deactive();

	private:
		bool m_bAlreadyBegin;

		NiPoint3* m_pkIntersectionPosition;
		NiPlane* m_pkIntersectionPlane;
    };

}}}}