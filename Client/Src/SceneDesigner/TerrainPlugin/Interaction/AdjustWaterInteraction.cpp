
#include "TerrainPluginPCH.h"
#include "AdjustWaterInteraction.h"
#include "TerrainEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MAdjustWaterInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );

	NiDelete m_pkIntersectionPosition;
	NiDelete m_pkIntersectionPlane;
}

MAdjustWaterInteractionMode::MAdjustWaterInteractionMode()
{
	m_bAlreadyBegin = false;

	m_pkIntersectionPosition = NiNew NiPoint3;
	m_pkIntersectionPlane =	NiNew NiPlane;
}

String* MAdjustWaterInteractionMode::get_Name()
{
    return "Adjust Water";
}

void MAdjustWaterInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MAdjustWaterInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndAdjustWater(iX, iY);
	}
}

void MAdjustWaterInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			AdjustWater(iX, iY);
		}
		else
		{
			BeginAdjustWater(iX, iY);
		}

		return;
	}

	__super::MouseMove(iX, iY);
}

void MAdjustWaterInteractionMode::BeginAdjustWater(int iX, int iY)
{	
	if( !m_pkMouseBrush )
		return;

	if( !CollideMouse(iX, iY, *m_pkIntersectionPosition) )
		return;

	m_bAlreadyBegin = true;

	//计算摄像机的碰撞面
	NiPoint3 kOrigin, kDir;
	NiViewMath::MouseToRay((float) iX, (float) iY, 
		MFramework::Instance->ViewportManager->ActiveViewport->Width,
		MFramework::Instance->ViewportManager->ActiveViewport->Height,
		MFramework::Instance->ViewportManager->ActiveViewport->
		GetNiCamera(), kOrigin, kDir);

	NiPoint3 kCross = kDir.UnitCross( NiPoint3(0.f, 0.f, 1.f) );
	NiPoint3 kNormal = NiPoint3(0.f, 0.f, 1.f).UnitCross( kCross );

	*m_pkIntersectionPlane = NiPlane(kNormal, *m_pkIntersectionPosition);

	//保存原始值
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		amSelectedPoints[i]->m_fStartWaterHeight = amSelectedPoints[i]->m_fWaterHeight;
	}
}

void MAdjustWaterInteractionMode::AdjustWater(int iX, int iY)
{
	NiPoint3 kOrigin, kDir;
	NiViewMath::MouseToRay((float) iX, (float) iY, 
		MFramework::Instance->ViewportManager->ActiveViewport->Width,
		MFramework::Instance->ViewportManager->ActiveViewport->Height,
		MFramework::Instance->ViewportManager->ActiveViewport->
		GetNiCamera(), kOrigin, kDir);

	float fTotal = m_pkIntersectionPlane->GetNormal().Dot(kDir * MAX_DISTANCE);
	if( fTotal == 0.f )
		return;

	bool bCtrlKeyDown = ((GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0);

	float fDistance = m_pkIntersectionPlane->Distance(kOrigin);

	float t = -fDistance / fTotal;
	t = NiClamp(t, 0.f, 1.f);

	NiPoint3 kPos = kOrigin + kDir * (MAX_DISTANCE * t);

	float fDiff = kPos.z - m_pkIntersectionPosition->z;

	//修改水面高度值
	CMap* pkMap = CMap::Get();
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		if( amSelectedPoints[i]->m_bHasWater )
		{
			if( bCtrlKeyDown )//硬刷
				amSelectedPoints[i]->m_fWaterHeight = amSelectedPoints[i]->m_fStartWaterHeight + fDiff;
			else
				amSelectedPoints[i]->m_fWaterHeight = amSelectedPoints[i]->m_fStartWaterHeight + amSelectedPoints[i]->m_fWeight * fDiff;

			pkMap->SetWaterHeight(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), amSelectedPoints[i]->m_fWaterHeight);
		}
	}

	//更新Brush
	m_pkMouseBrush->UpdateBrush();
}

void MAdjustWaterInteractionMode::EndAdjustWater(int iX, int iY)
{
	m_bAlreadyBegin = false;
}

void MAdjustWaterInteractionMode::Active()
{
	__super::Active();

	m_ParamDlg->ParamTextBox->Text = "";
	m_ParamDlg->ParamTrackBar->Value = 0;
	m_ParamDlg->ParamLable->Text = "";
	m_ParamDlg->ParamGroupBox->Enabled = false;

	m_pkMouseBrush->SetWaterBrush(true);
}

void MAdjustWaterInteractionMode::Deactive()
{
	m_ParamDlg->ParamGroupBox->Enabled = true;

	m_pkMouseBrush->SetWaterBrush(false);

	__super::Deactive();
}
