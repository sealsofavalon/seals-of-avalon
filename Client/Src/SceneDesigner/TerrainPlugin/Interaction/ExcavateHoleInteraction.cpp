#include "TerrainPluginPCH.h"
#include "ExcavateHoleInteraction.h"
#include "TerrainEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MExcavateHoleInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MExcavateHoleInteractionMode::MExcavateHoleInteractionMode()
{
	m_bAlreadyBegin = false;
	m_pkExcavateHoleCommand = NULL;
}

String* MExcavateHoleInteractionMode::get_Name()
{
    return "Excavate Hole";
}

void MExcavateHoleInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);

	if( eType == MouseButtonType::LeftButton )
	{
		BeginExcavateHole(iX, iY);
		ExcavateHole(iX, iY);
	}
}

void MExcavateHoleInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndExcavateHole(iX, iY);
	}
}

void MExcavateHoleInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{			
			ExcavateHole(iX, iY);
		}
	}

	__super::MouseMove(iX, iY);
}

void MExcavateHoleInteractionMode::BeginExcavateHole(int iX, int iY)
{	
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;
	if( m_pkExcavateHoleCommand )
		m_pkExcavateHoleCommand->Clear();
	else
		m_pkExcavateHoleCommand = NiNew ExcavateHoleCommand;
}

void MExcavateHoleInteractionMode::ExcavateHole(int iX, int iY)
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	bool bShiftKeyDown = (GetAsyncKeyState(VK_SHIFT) & 0x8000) != 0;
	bool bCtrlKeyDown = (GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0;
	bool bHole;

	CMap* pkMap = CMap::Get();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		if(bShiftKeyDown)
			bHole = true;
		else if(bCtrlKeyDown)
			bHole = false;
		else
			bHole = !pkMap->IsHole(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY));

		if( pkMap->ExcavateHole(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), bHole) )
		{
			m_pkExcavateHoleCommand->AddHole(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), bHole);
		}
	}
}

void MExcavateHoleInteractionMode::EndExcavateHole(int iX, int iY)
{
	m_bAlreadyBegin = false;

	CommandService->ExecuteCommand(new MTerrainEditCommand(m_pkExcavateHoleCommand), true);
	m_pkExcavateHoleCommand = NULL;
}

void MExcavateHoleInteractionMode::Active()
{
	__super::Active();

	m_pkMouseBrush->SetCenterGrid(true);
	m_pkMouseBrush->SetSize(1, 1);

	m_ParamDlg->ParamTextBox->Text = "";
	m_ParamDlg->ParamTrackBar->Value = 0;
	m_ParamDlg->ParamLable->Text = "";
	m_ParamDlg->ParamGroupBox->Enabled = false;
}

void MExcavateHoleInteractionMode::Deactive()
{
	m_ParamDlg->ParamGroupBox->Enabled = true;

	m_pkMouseBrush->SetCenterGrid(false);

	__super::Deactive();
}