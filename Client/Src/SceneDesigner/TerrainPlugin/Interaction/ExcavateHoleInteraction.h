#pragma once

#include "TerrainEditInteraction.h"
#include "Command/ExcavateHoleCommand.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{
    public __gc class MExcavateHoleInteractionMode : public MTerrainEditInteractionMode
    {
    protected:
        virtual void Do_Dispose(bool bDisposing);

	public:
		MExcavateHoleInteractionMode();

		__property String* get_Name();

		void MouseDown(MouseButtonType eType, int iX, int iY);
        void MouseUp(MouseButtonType eType, int iX, int iY);
		void MouseMove(int iX, int iY);

		void BeginExcavateHole(int iX, int iY);
		void ExcavateHole(int iX, int iY);
		void EndExcavateHole(int iX, int iY);

		virtual void Active();
		virtual void Deactive();

	private:
		bool m_bAlreadyBegin;
		ExcavateHoleCommand* m_pkExcavateHoleCommand;
    };

}}}}