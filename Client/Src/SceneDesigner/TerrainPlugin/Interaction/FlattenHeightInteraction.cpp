#include "TerrainPluginPCH.h"
#include "FlattenHeightInteraction.h"
#include "TerrainEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MFlattenHeightInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MFlattenHeightInteractionMode::MFlattenHeightInteractionMode()
{
	m_bAlreadyBegin = false;
}

String* MFlattenHeightInteractionMode::get_Name()
{
    return "Flatten Height";
}

void MFlattenHeightInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MFlattenHeightInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndFlattenHeight(iX, iY);
	}
}

void MFlattenHeightInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			//记录Undo数据
			BeginChangeHeightCommand();
			
			FlattenHeight(iX, iY);
			
			//记录Redo数据
			EndChangeHeightCommand();
		}
		else
		{
			BeginFlattenHeight(iX, iY);
		}
	}

	__super::MouseMove(iX, iY);
}

void MFlattenHeightInteractionMode::BeginFlattenHeight(int iX, int iY)
{	
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;
}

void MFlattenHeightInteractionMode::FlattenHeight(int iX, int iY)
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	//计算平均值
	float fAvgHeight = 0.f;
	for(int i = 0; i < amSelectedPoints->Count; i++)
	{
		fAvgHeight += amSelectedPoints[i]->m_fHeight;
	}

	fAvgHeight /= amSelectedPoints->Count;

	//修改高度值
	CMap* pkMap = CMap::Get();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		amSelectedPoints[i]->m_fHeight = fAvgHeight;
		pkMap->SetHeight(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), amSelectedPoints[i]->m_fHeight);
	}
}

void MFlattenHeightInteractionMode::EndFlattenHeight(int iX, int iY)
{
	m_bAlreadyBegin = false;
}

void MFlattenHeightInteractionMode::Active()
{
	__super::Active();

	m_ParamDlg->ParamTextBox->Text = "";
	m_ParamDlg->ParamTrackBar->Value = 0;
	m_ParamDlg->ParamLable->Text = "";
	m_ParamDlg->ParamGroupBox->Enabled = false;
}

void MFlattenHeightInteractionMode::Deactive()
{
	m_ParamDlg->ParamGroupBox->Enabled = true;

	__super::Deactive();
}