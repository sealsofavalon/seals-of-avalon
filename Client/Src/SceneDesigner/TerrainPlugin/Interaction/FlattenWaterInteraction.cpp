
#include "TerrainPluginPCH.h"
#include "FlattenWaterInteraction.h"

#include "TerrainEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MFlattenWaterInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MFlattenWaterInteractionMode::MFlattenWaterInteractionMode()
{
	m_bAlreadyBegin = false;
}

String* MFlattenWaterInteractionMode::get_Name()
{
	return "Flatten Water";
}

void MFlattenWaterInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MFlattenWaterInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndFlattenWater(iX, iY);
	}
}

void MFlattenWaterInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			FlattenWater(iX, iY);
		}
		else
		{
			BeginFlattenWater(iX, iY);
		}
	}

	__super::MouseMove(iX, iY);
}

void MFlattenWaterInteractionMode::BeginFlattenWater(int iX, int iY)
{
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;
}

void MFlattenWaterInteractionMode::FlattenWater(int iX, int iY)
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();

	int iWaterCount = 0;

	//计算平均值
	float fAvgHeight = 0.f;
	for(int i = 0; i < amSelectedPoints->Count; i++)
	{
		if( amSelectedPoints[i]->m_bHasWater )
		{
			fAvgHeight += amSelectedPoints[i]->m_fWaterHeight;
			iWaterCount++;
		}
	}

	if( iWaterCount == 0 )
		return;

	fAvgHeight /= iWaterCount;

	//修改高度值
	CMap* pkMap = CMap::Get();
	bool bWaterAboveTerrain;
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		if( amSelectedPoints[i]->m_bHasWater )
		{
			bWaterAboveTerrain = pkMap->WaterAboveTerrain(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), fAvgHeight);

			if( bWaterAboveTerrain )			
			{
				amSelectedPoints[i]->m_fWaterHeight = fAvgHeight;

				pkMap->SetWaterHeight(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), fAvgHeight);
				pkMap->SetWaterHeight(int(amSelectedPoints[i]->m_fX) + UNIT_SIZE, int(amSelectedPoints[i]->m_fY), fAvgHeight);
				pkMap->SetWaterHeight(int(amSelectedPoints[i]->m_fX) + UNIT_SIZE, int(amSelectedPoints[i]->m_fY)  + UNIT_SIZE, fAvgHeight);
				pkMap->SetWaterHeight(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY)  + UNIT_SIZE, fAvgHeight);
			}
			else
			{
				amSelectedPoints[i]->m_bHasWater = false;
				pkMap->SetWaterFlag(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), false, 0);
			}
		}
	}
}

void MFlattenWaterInteractionMode::EndFlattenWater(int iX, int iY)
{
	m_bAlreadyBegin = false;
}

void MFlattenWaterInteractionMode::Active()
{
	__super::Active();

	m_pkMouseBrush->SetWaterBrush(true);

	m_ParamDlg->ParamTextBox->Text = "";
	m_ParamDlg->ParamTrackBar->Value = 0;
	m_ParamDlg->ParamLable->Text = "";
	m_ParamDlg->ParamGroupBox->Enabled = false;
}

void MFlattenWaterInteractionMode::Deactive()
{
	m_ParamDlg->ParamGroupBox->Enabled = true;

	m_pkMouseBrush->SetWaterBrush(false);

	__super::Deactive();
}