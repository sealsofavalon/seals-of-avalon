#include "TerrainPluginPCH.h"
#include "IncHeightInteraction.h"
#include "TerrainEditDialog.h"
#include "Command/MTerrainEditCommand.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MIncHeightInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MIncHeightInteractionMode::MIncHeightInteractionMode()
{
	m_bAlreadyBegin = false;
	
	m_fValueMax = 100.f;
	m_fValueMin = 0.f;
	m_fValue = m_fDefaultValue = 30.f;
}

String* MIncHeightInteractionMode::get_Name()
{
    return "Inc Height";
}

void MIncHeightInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MIncHeightInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndIncHeight(iX, iY);
	}
}

void MIncHeightInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			//记录Undo数据
			BeginChangeHeightCommand();
			
			IncHeight(iX, iY);
			
			//记录Redo数据
			EndChangeHeightCommand();
		}
		else
		{
			BeginIncHeight(iX, iY);
		}
	}

	__super::MouseMove(iX, iY);
}

void MIncHeightInteractionMode::BeginIncHeight(int iX, int iY)
{	
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;
}

void MIncHeightInteractionMode::IncHeight(int iX, int iY)
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	float fAdjustHeight = m_fValue / 30.f; 
	float fMaxHeight = m_pkMouseBrush->GetCenterPoint().z + fAdjustHeight;

	//修改高度值
	CMap* pkMap = CMap::Get();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		if( amSelectedPoints[i]->m_fHeight < fMaxHeight )
		{
			amSelectedPoints[i]->m_fHeight += amSelectedPoints[i]->m_fWeight * fAdjustHeight;
			if( amSelectedPoints[i]->m_fHeight > fMaxHeight )
				amSelectedPoints[i]->m_fHeight = fMaxHeight;

			pkMap->SetHeight(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), amSelectedPoints[i]->m_fHeight);
		}
	}
}

void MIncHeightInteractionMode::EndIncHeight(int iX, int iY)
{
	m_bAlreadyBegin = false;
}

void MIncHeightInteractionMode::Active()
{
	__super::Active();

	m_ParamDlg->ParamLable->Text = "Hardness:";
	UpdateParamDlg();
}

void MIncHeightInteractionMode::Deactive()
{
	__super::Deactive();
}