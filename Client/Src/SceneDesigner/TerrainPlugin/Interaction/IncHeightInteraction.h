#pragma once

#include "TerrainEditInteraction.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{
    public __gc class MIncHeightInteractionMode : public MTerrainEditInteractionMode
    {
    protected:
        virtual void Do_Dispose(bool bDisposing);

	public:
		MIncHeightInteractionMode();

		__property String* get_Name();

		void MouseDown(MouseButtonType eType, int iX, int iY);
        void MouseUp(MouseButtonType eType, int iX, int iY);
		void MouseMove(int iX, int iY);

		void BeginIncHeight(int iX, int iY);
		void IncHeight(int iX, int iY);
		void EndIncHeight(int iX, int iY);

		virtual void Active();
		virtual void Deactive();

	private:
		bool m_bAlreadyBegin;
    };

}}}}