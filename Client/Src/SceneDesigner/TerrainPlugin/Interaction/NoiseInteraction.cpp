#include "TerrainPluginPCH.h"
#include "NoiseInteraction.h"
#include "TerrainEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MNoiseInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MNoiseInteractionMode::MNoiseInteractionMode()
{
	m_bAlreadyBegin = false;
	
	m_fValue = m_fDefaultValue = 3.f;
	m_fValueMax = 10.f;
	m_fValueMin = 0.f;
}

String* MNoiseInteractionMode::get_Name()
{
    return "Noise Height";
}

void MNoiseInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);

	if( eType == MouseButtonType::LeftButton )
	{
		BeginNoiseHeight(iX, iY);

		//记录Undo数据
		BeginChangeHeightCommand();

		NoiseHeight(iX, iY);

		//记录Redo数据
		EndChangeHeightCommand();
	}
}

void MNoiseInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndNoiseHeight(iX, iY);
	}
}

void MNoiseInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			//记录Undo数据
			BeginChangeHeightCommand();

			NoiseHeight(iX, iY);

			//记录Redo数据
			EndChangeHeightCommand();
		}

		return;
	}

	__super::MouseMove(iX, iY);
}

void MNoiseInteractionMode::BeginNoiseHeight(int iX, int iY)
{	
	if( !m_pkMouseBrush )
		return;

	//保存原始值
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		amSelectedPoints[i]->m_fStartHeight = amSelectedPoints[i]->m_fHeight;
	}


	m_bAlreadyBegin = true;
}

void MNoiseInteractionMode::NoiseHeight(int iX, int iY)
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	//shift 键按下是硬刷
	bool bShiftKeyDown = (GetAsyncKeyState(VK_SHIFT) & 0x8000) ? true : false;

	//计算平均值
	float fAvgHeight = 0.f;
	for(int i = 0; i < amSelectedPoints->Count; i++)
	{
		fAvgHeight += amSelectedPoints[i]->m_fHeight;
	}

	fAvgHeight /= amSelectedPoints->Count;

	//修改高度值
	CMap* pkMap = CMap::Get();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		if( bShiftKeyDown )
		{
			amSelectedPoints[i]->m_fHeight = amSelectedPoints[i]->m_fStartHeight + m_fValue*NiSymmetricRandom();
		}
		else
		{
			amSelectedPoints[i]->m_fHeight = amSelectedPoints[i]->m_fStartHeight + amSelectedPoints[i]->m_fWeight*m_fValue*NiSymmetricRandom();
		}

		pkMap->SetHeight(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), amSelectedPoints[i]->m_fHeight);
	}

	//更新Brush
	m_pkMouseBrush->UpdateBrush();
}

void MNoiseInteractionMode::EndNoiseHeight(int iX, int iY)
{
	m_bAlreadyBegin = false;
}

void MNoiseInteractionMode::Active()
{
	__super::Active();

	m_ParamDlg->ParamLable->Text = "Max Noise Height:";
	UpdateParamDlg();
}

void MNoiseInteractionMode::Deactive()
{
	__super::Deactive();
}