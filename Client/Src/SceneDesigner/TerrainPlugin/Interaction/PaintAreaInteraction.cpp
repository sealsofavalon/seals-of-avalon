
#include "TerrainPluginPCH.h"
#include "TerrainEditDialog.h"
#include "PaintAreaInteraction.h"
#include "AreaDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MPaintAreaInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MPaintAreaInteractionMode::MPaintAreaInteractionMode()
{
	m_bAlreadyBegin = false;
}

String* MPaintAreaInteractionMode::get_Name()
{
    return "Paint Area";
}

void MPaintAreaInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MPaintAreaInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndPaintArea(iX, iY);
	}
}

void MPaintAreaInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			PaintArea(iX, iY);
		}
		else
		{
			BeginPaintArea(iX, iY);
			PaintArea(iX, iY);
		}
	}

	__super::MouseMove(iX, iY);
}

void MPaintAreaInteractionMode::BeginPaintArea(int iX, int iY)
{
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;
}

void MPaintAreaInteractionMode::PaintArea(int iX, int iY)
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	CMap* pkMap = CMap::Get();
	if(pkMap == NULL)
		return;

	AreaInfo* pkAreaInfo = AreaDialog::GetActiveAreaInfo();
	if(pkAreaInfo == NULL)
		return;

	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		pkMap->SetAreaId(amSelectedPoints[i]->m_fX, amSelectedPoints[i]->m_fY, pkAreaInfo->m_uiEntryID);
	}
}

void MPaintAreaInteractionMode::EndPaintArea(int iX, int iY)
{
	m_bAlreadyBegin = false;
}

void MPaintAreaInteractionMode::Active()
{
	__super::Active();

	m_pkMouseBrush->SetGridSize(8);

	m_ParamDlg->ParamTextBox->Text = "";
	m_ParamDlg->ParamTrackBar->Value = 0;
	m_ParamDlg->ParamLable->Text = "";
	m_ParamDlg->ParamGroupBox->Enabled = false;

	CTerrainShader::ShowAreaInfo(true);
}

void MPaintAreaInteractionMode::Deactive()
{
	CTerrainShader::ShowAreaInfo(false);

	m_pkMouseBrush->SetGridSize(UNIT_SIZE);

	m_ParamDlg->ParamGroupBox->Enabled = true;
	__super::Deactive();
}
