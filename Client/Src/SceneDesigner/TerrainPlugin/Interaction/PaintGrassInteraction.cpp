#include "TerrainPluginPCH.h"
#include "PaintGrassInteraction.h"
#include "TerrainEditDialog.h"
#include "GrassDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MPaintGrassInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MPaintGrassInteractionMode::MPaintGrassInteractionMode()
{
	m_bAlreadyBegin = false;
}

String* MPaintGrassInteractionMode::get_Name()
{
    return "Paint Grass";
}

void MPaintGrassInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);

    if( eType == MouseButtonType::LeftButton )
	{
        BeginPaintGrass();
        PaintGrass();
	}
}

void MPaintGrassInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndPaintGrass();
	}
}

void MPaintGrassInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			PaintGrass();
		}
		else
		{
			BeginPaintGrass();
		}
	}

	__super::MouseMove(iX, iY);
}

void MPaintGrassInteractionMode::BeginPaintGrass()
{
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;
}

void MPaintGrassInteractionMode::PaintGrass()
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

    CMap* pkMap = CMap::Get();
    if(pkMap == NULL)
        return;

    GrassDialog* pmGrassDialog = GrassDialog::Get();
    if(pmGrassDialog == NULL)
        return;

    unsigned int uiDensity = pmGrassDialog->GetGrassDensity();
    unsigned int uiInterval = pmGrassDialog->GetGrassInterval();
    int iX, iY;
    unsigned short usGrass;
    CChunk* pkChunk;

    bool bShiftKeyDown = ((GetAsyncKeyState(VK_SHIFT) & 0x8000) != 0);
    if(bShiftKeyDown)
        uiInterval = 1;

	//Paint Grass
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
        iX = (int)amSelectedPoints[i]->m_fX; 
        iY = (int)amSelectedPoints[i]->m_fY;
        if(uiInterval > 1)
        {
            if((iX % uiInterval) != 0 || (iY % uiInterval) != 0)
                continue;
        }

        pkChunk = pkMap->GetChunk(amSelectedPoints[i]->m_fX, amSelectedPoints[i]->m_fY);
        if(pkChunk == NULL)
            continue;

        if( bShiftKeyDown )
            usGrass = INVALID_GRASS; //ɾ��
        else if(unsigned int(NiUnitRandom()*100) > uiDensity)
            usGrass = INVALID_GRASS; //ɾ��
        else
            usGrass = pmGrassDialog->GenerateGrass();

        pkChunk->PaintGrass(iX, iY, usGrass);
	}
}

void MPaintGrassInteractionMode::EndPaintGrass()
{
	m_bAlreadyBegin = false;
}

void MPaintGrassInteractionMode::Active()
{
	__super::Active();

    m_pkMouseBrush->SetGridSize(1.f);
	m_ParamDlg->ParamLable->Text = "Paint Grass";
	UpdateParamDlg();
}

void MPaintGrassInteractionMode::Deactive()
{
    m_pkMouseBrush->SetGridSize(UNIT_SIZE);
	__super::Deactive();
}