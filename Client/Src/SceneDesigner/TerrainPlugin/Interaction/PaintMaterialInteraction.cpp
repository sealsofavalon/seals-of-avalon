#include "TerrainPluginPCH.h"
#include "PaintMaterialInteraction.h"
#include "TerrainEditDialog.h"
#include "PaintMaterialDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MPaintMaterialInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MPaintMaterialInteractionMode::MPaintMaterialInteractionMode()
{
	m_bAlreadyBegin = false;

	m_fValueMax = 1.f;
	m_fValueMin = 0.f;
	m_fValue = m_fDefaultValue = 0.5f;
}

String* MPaintMaterialInteractionMode::get_Name()
{
    return "Paint Material";
}

void MPaintMaterialInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		BeginPaintMaterial(iX, iY);
		PaintMaterial(iX, iY);
	}
}

void MPaintMaterialInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndPaintMaterial(iX, iY);
	}
}

void MPaintMaterialInteractionMode::MouseMove(int iX, int iY)
{
	__super::MouseMove(iX, iY);
	if( m_bLeftDown && m_bAlreadyBegin )
	{
		PaintMaterial(iX, iY);
	}
}

void MPaintMaterialInteractionMode::BeginPaintMaterial(int iX, int iY)
{	
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;
}

void MPaintMaterialInteractionMode::PaintMaterial(int iX, int iY)
{
	if( PaintMaterialDialog::Get() == NULL 
	 || PaintMaterialDialog::Get()->ActiveBrushControl == NULL
	 || PaintMaterialDialog::Get()->ActiveTextureControl == NULL)
		return;

	if( CMap::Get() == NULL )
		return;

	//�޸ĸ߶�ֵ
	NiPoint3 kPos;
	if( !CollideMouse(iX, iY, kPos) )
	{
		return;
	}
	
	CMap::Get()->PaintMaterial(	int(NiFloor(kPos.x)),
								int(NiFloor(kPos.y)),
								PaintMaterialDialog::Get()->ActiveBrushControl->Bitmap,
								PaintMaterialDialog::Get()->ActiveTextureControl->PathName,
								m_fValue*0.5f);
}

void MPaintMaterialInteractionMode::EndPaintMaterial(int iX, int iY)
{
	m_bAlreadyBegin = false;
}

void MPaintMaterialInteractionMode::Active()
{
	__super::Active();

	m_pkMouseBrush->SetMaterialBrush(true);
	m_pkMouseBrush->SetGridSize(1.f);

	m_ParamDlg->ParamLable->Text = "Alpha Scale:";
	UpdateParamDlg();
}

void MPaintMaterialInteractionMode::Deactive()
{
	m_pkMouseBrush->SetMaterialBrush(false);
	m_pkMouseBrush->SetGridSize(UNIT_SIZE);

	__super::Deactive();
}