#pragma once

#include "TerrainEditInteraction.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{
    public __gc class MPaintMaterialInteractionMode : public MTerrainEditInteractionMode
    {
    protected:
        virtual void Do_Dispose(bool bDisposing);

	public:
		MPaintMaterialInteractionMode();

		__property String* get_Name();

		void MouseDown(MouseButtonType eType, int iX, int iY);
        void MouseUp(MouseButtonType eType, int iX, int iY);
		void MouseMove(int iX, int iY);

		void BeginPaintMaterial(int iX, int iY);
		void PaintMaterial(int iX, int iY);
		void EndPaintMaterial(int iX, int iY);

		virtual void Active();
		virtual void Deactive();

	private:
		bool m_bAlreadyBegin;
    };

}}}}