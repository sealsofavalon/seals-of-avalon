
#include "TerrainPluginPCH.h"
#include "PaintTerrainInfoInteraction.h"
#include "TerrainEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MPaintTerrainInfoInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MPaintTerrainInfoInteractionMode::MPaintTerrainInfoInteractionMode()
{
	m_bAlreadyBegin = false;
}

String* MPaintTerrainInfoInteractionMode::get_Name()
{
    return "Paint Terrain Info";
}

void MPaintTerrainInfoInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MPaintTerrainInfoInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndPaintTerrainInfo(iX, iY);
	}
}

void MPaintTerrainInfoInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			PaintTerrainInfo(iX, iY);
		}
		else
		{
			BeginPaintTerrainInfo(iX, iY);
		}
	}

	__super::MouseMove(iX, iY);
}

void MPaintTerrainInfoInteractionMode::BeginPaintTerrainInfo(int iX, int iY)
{
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;
}

void MPaintTerrainInfoInteractionMode::PaintTerrainInfo(int iX, int iY)
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	bool bCheckNormal = false;
	if(GetAsyncKeyState(VK_CONTROL))
		bCheckNormal = true;

	bool bMoveable = false;
	if( GetAsyncKeyState(VK_SHIFT) & 0x8000 )
		bMoveable = true;

	float h;
	NiPoint3 n;

	//�޸ĸ߶�ֵ
	CMap* pkMap = CMap::Get();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		if(bCheckNormal)
		{
			pkMap->GetHeightAndNormal(amSelectedPoints[i]->m_fX + GRID_SIZE/2, amSelectedPoints[i]->m_fY + GRID_SIZE/2, h, n);
			if(n.z < MIN_WALK_NORMAL)
				bMoveable = false;
			else
				bMoveable = true;
		}

		pkMap->SetMoveable(amSelectedPoints[i]->m_fX, amSelectedPoints[i]->m_fY, bMoveable, WalkGrid);
	}
}

void MPaintTerrainInfoInteractionMode::EndPaintTerrainInfo(int iX, int iY)
{
	m_bAlreadyBegin = false;
}

void MPaintTerrainInfoInteractionMode::Active()
{
	__super::Active();

	m_pkMouseBrush->SetGridSize(GRID_SIZE);

	m_ParamDlg->ParamTextBox->Text = "";
	m_ParamDlg->ParamTrackBar->Value = 0;
	m_ParamDlg->ParamLable->Text = "";
	m_ParamDlg->ParamGroupBox->Enabled = false;

	CTerrainShader::ShowGridInfo(true);
	CTerrainShader::SetGridLevel(WalkGrid);
}

void MPaintTerrainInfoInteractionMode::Deactive()
{
	CTerrainShader::ShowGridInfo(false);

	m_pkMouseBrush->SetGridSize(UNIT_SIZE);

	m_ParamDlg->ParamGroupBox->Enabled = true;
	__super::Deactive();
}
