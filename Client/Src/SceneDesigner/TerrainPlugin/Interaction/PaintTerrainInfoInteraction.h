#pragma once

#include "TerrainEditInteraction.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{
    public __gc class MPaintTerrainInfoInteractionMode : public MTerrainEditInteractionMode
    {
    protected:
        virtual void Do_Dispose(bool bDisposing);

	public:
		MPaintTerrainInfoInteractionMode();

		__property String* get_Name();

		void MouseDown(MouseButtonType eType, int iX, int iY);
        void MouseUp(MouseButtonType eType, int iX, int iY);
		void MouseMove(int iX, int iY);

		void BeginPaintTerrainInfo(int iX, int iY);
		void PaintTerrainInfo(int iX, int iY);
		void EndPaintTerrainInfo(int iX, int iY);

		virtual void Active();
		virtual void Deactive();

	private:
		bool m_bAlreadyBegin;
    };

}}}}