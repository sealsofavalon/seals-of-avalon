
#include "TerrainPluginPCH.h"
#include "PaintWaterInteraction.h"

#include "TerrainEditDialog.h"
#include "WaterDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MPaintWaterInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MPaintWaterInteractionMode::MPaintWaterInteractionMode()
{
	m_bAlreadyBegin = false;

	m_fValueMax = 800.f;
	m_fValueMin = -200.f;
	m_fValue = m_fDefaultValue = 20.f;
}

String* MPaintWaterInteractionMode::get_Name()
{
	return "Paint Water";
}

void MPaintWaterInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MPaintWaterInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndPaintWater(iX, iY);
	}
}

void MPaintWaterInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			PaintWater(iX, iY);
		}
		else
		{
			BeginPaintWater(iX, iY);
		}
	}

	__super::MouseMove(iX, iY);
}

void MPaintWaterInteractionMode::BeginPaintWater(int iX, int iY)
{
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;
}

void MPaintWaterInteractionMode::PaintWater(int iX, int iY)
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	int id = -1;
	WaterDialog* pmWaterDialog = WaterDialog::Get();
	if(pmWaterDialog)
		id = pmWaterDialog->GetActiveWaterId();

	if(id == -1)
		return;

	float fWaterHeight;

	bool bShiftKeyDown = ((GetAsyncKeyState(VK_SHIFT) & 0x8000) != 0);
	bool bCtrlKeyDown = ((GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0);
	
	//修改高度值
	CMap* pkMap = CMap::Get();
	bool bWaterAboveTerrain;
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		if( bCtrlKeyDown )
		{
			//按下Ctrl键, 高度值是相对地面的
			fWaterHeight = m_fValue + pkMap->GetHeight(amSelectedPoints[i]->m_fX, amSelectedPoints[i]->m_fY);
		}
		else
			fWaterHeight = m_fValue;


		bWaterAboveTerrain = pkMap->WaterAboveTerrain(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), fWaterHeight);
		if( bShiftKeyDown || !bWaterAboveTerrain )
		{
			//擦除水面
			amSelectedPoints[i]->m_bHasWater = false;
			pkMap->SetWaterFlag(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), false, id);
		}
		else
		{
			amSelectedPoints[i]->m_bHasWater = true;
			pkMap->SetWaterFlag(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), true, id);

			amSelectedPoints[i]->m_fWaterHeight = fWaterHeight;

			pkMap->SetWaterHeight(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), fWaterHeight);
			pkMap->SetWaterHeight(int(amSelectedPoints[i]->m_fX) + UNIT_SIZE, int(amSelectedPoints[i]->m_fY), fWaterHeight);
			pkMap->SetWaterHeight(int(amSelectedPoints[i]->m_fX) + UNIT_SIZE, int(amSelectedPoints[i]->m_fY)  + UNIT_SIZE, fWaterHeight);
			pkMap->SetWaterHeight(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY)  + UNIT_SIZE, fWaterHeight);
		}
	}
}

void MPaintWaterInteractionMode::EndPaintWater(int iX, int iY)
{
	m_bAlreadyBegin = false;
}

void MPaintWaterInteractionMode::Active()
{
	__super::Active();

	m_pkMouseBrush->SetWaterBrush(true);

	m_ParamDlg->ParamLable->Text = "Water Height:";
	UpdateParamDlg();
}

void MPaintWaterInteractionMode::Deactive()
{
	m_pkMouseBrush->SetWaterBrush(false);

	__super::Deactive();
}