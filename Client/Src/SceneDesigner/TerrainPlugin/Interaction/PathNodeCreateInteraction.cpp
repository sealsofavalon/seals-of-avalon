#include "TerrainPluginPCH.h"
#include "PathNodeCreateInteraction.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

MPathNodeCreateInteractionMode::MPathNodeCreateInteractionMode()
{
}

void MPathNodeCreateInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose(bDisposing);
}

String* MPathNodeCreateInteractionMode::get_Name()
{
	return "Create Path Node";
}

void MPathNodeCreateInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	if (eType != MouseButtonType::LeftButton)
	{
		__super::MouseDown(eType, iX, iY);
		return;
	}

	// make sure we are placing it on the ideal plane relative to the view
	NiPoint3 kLook;
	NiCamera* pkCamera;
	pkCamera = MFramework::Instance->ViewportManager->ActiveViewport->GetNiCamera();
	(pkCamera->GetRotate()).GetCol(0, kLook);
	GetBestPlane(kLook.x, kLook.y, kLook.z);

	// create a path node object
	NiUniqueID kTemplateID;
	MUtility::GuidToID(Guid::NewGuid(), kTemplateID);

	String* strName = MFramework::Instance->Scene->GetUniqueEntityName("PathNode 01");	
	const char* pcName = MStringToCharPointer(strName);
	NiGeneralEntityPtr spEntity = NiNew NiGeneralEntity(pcName, kTemplateID, 2);
    MFreeCharPointer(pcName);
    spEntity->SetTypeMask(NiGeneralEntity::PathNode);

	// Add transformation component.
	spEntity->AddComponent(NiNew NiTransformationComponent(), false);

	// Add path node component.
    CPathNode* pkPathNode = NiNew CPathNode(NiPoint3::ZERO);
	CPathNodeComponent* pkPathNodeComponent = NiNew CPathNodeComponent;
	spEntity->AddComponent(pkPathNodeComponent, false);

	m_pmNewEntity = MFramework::Instance->EntityFactory->Get(spEntity);
	m_pmNewEntity->Update(MFramework::Instance->TimeManager->CurrentTime, MFramework::Instance->ExternalAssetManager);

	// place the entity based off of the mouse location
	CreateHelper((float)iX, (float)iY);

	MFramework::Instance->Scene->AddEntity(m_pmNewEntity, true);
    SelectionService->AddEntityToSelection(m_pmNewEntity);
}
