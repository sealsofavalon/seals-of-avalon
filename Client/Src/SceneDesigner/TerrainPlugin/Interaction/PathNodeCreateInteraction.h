#pragma once

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{

	public __gc class MPathNodeCreateInteractionMode : public MCreateInteractionMode
	{
	// MDisposable members.
    protected:
        virtual void Do_Dispose(bool bDisposing);

	public:
		MPathNodeCreateInteractionMode();

		__property String* get_Name();
		void MouseDown(MouseButtonType eType, int iX, int iY);
	};

}}}}