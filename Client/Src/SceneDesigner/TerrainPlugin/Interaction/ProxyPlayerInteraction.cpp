#include "TerrainPluginPCH.h"
#include "ProxyPlayerInteraction.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

MProxyPlayerInteractionMode::MProxyPlayerInteractionMode()
{
	m_kProxyState = Idle;

	m_fCamDist = 12.0f;

	m_fCamHeight = 2.0f;

	m_fZoomSpeed = 0.1f;

	m_fMaxCameraDistance = 50.0f;

	m_fMinCameraDistance = 5.0f;

	m_pmPrevInteractionMode = NULL;
}

void MProxyPlayerInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

String* MProxyPlayerInteractionMode::get_Name()
{
	return "Proxy Player Interaction";
}

void MProxyPlayerInteractionMode::SetInteractionMode(Object* pmObject, 
											   EventArgs* mArgs)
{
	MVerifyValidInstance;

	if( InteractionModeService->ActiveMode != this )
	{
		m_pmPrevInteractionMode = InteractionModeService->ActiveMode;
		InteractionModeService->ActiveMode = this;
	}
	else
	{
		InteractionModeService->ActiveMode = m_pmPrevInteractionMode;
	}
}

void MProxyPlayerInteractionMode::ValidateInteractionMode(Object* pmSender,
													UIState* pmState)
{
	MVerifyValidInstance;

	pmState->Checked = (InteractionModeService->ActiveMode == this);
}

//判断鼠标和地面的交点
bool MProxyPlayerInteractionMode::CollideMouse(int iX, int iY, NiPoint3& kPos)
{
	return false;
}

void MProxyPlayerInteractionMode::Update(float fTime)
{
	if(!LoadProxyPlayer())
		return;

	// Process WASD navigation mode. WASD does not function in orthographic
	// views. It also does not function if the left or middle buttons are down.
	NiCamera* pkCamera = MFramework::Instance->ViewportManager->ActiveViewport->GetNiCamera();
	MEntity* pmCamera = MFramework::Instance->ViewportManager->ActiveViewport->CameraEntity;

	MEntity* pkProxyEntity = MFramework::Instance->ProxyManager->ProxyScene->GetEntityByName("Proxy Player");
	CPlayer* pkPlayer = (CPlayer*)pkProxyEntity->GetNiEntityInterface();

	// Proxy Player
	NiEntityInterface* pkProxyEntityInterface = MFramework::Instance->ProxyManager->ProxyScene->GetNiScene()->GetEntityByName("Proxy Player");
	NiEntityInterface* pkCameraEntity = pmCamera->GetNiEntityInterface();
	// todo 键盘输入或者鼠标输入同时更新Camera Position的话，会产生抖动的现象
	if (pkCamera && pmCamera)
	{
		// Capture key states.
		short siWKeyDown = (GetAsyncKeyState(0x57) & 0x8000); //W Key
		short siAKeyDown = (GetAsyncKeyState(0x41) & 0x8000); //A Key
		short siSKeyDown = (GetAsyncKeyState(0x53) & 0x8000); //S Key
		short siDKeyDown = (GetAsyncKeyState(0x44) & 0x8000); //D Key

		short siQKeyDown = (GetAsyncKeyState(0x51) & 0x8000); //Q Key
		short siEKeyDown = (GetAsyncKeyState(0x45) & 0x8000); //E Key

		short siSpaceKeyDown = (GetAsyncKeyState(VK_SPACE) & 0x8000); //Space Key 

		if (siWKeyDown)
		{
			pkPlayer->StartForward();
		}
		else if (siSKeyDown)
		{
			pkPlayer->StartBackward();
		}
		else
		{
			pkPlayer->Stop();
		}

		if (siAKeyDown)
		{
			pkPlayer->StartStrafeLeft();
		}
		else if (siDKeyDown)
		{
			pkPlayer->StartStrafeRight();
		}
		else
		{
			pkPlayer->StopStrafe();
		}

		if( siQKeyDown )
		{
			pkPlayer->StartTurnLeft();
		}
		else if( siEKeyDown )
		{
			pkPlayer->StartTurnRight();
		}
		else
		{
			pkPlayer->StopTurn();
		}

		if( siSpaceKeyDown )
		{
			pkPlayer->Jump();
		}


		NiPoint3 kData;
		pkPlayer->GetPropertyData("Translation", kData, 0);
		kData += NiPoint3(-2.f, -2.f, 4.f); 
		pkCameraEntity->SetPropertyData("Translation", kData, 0);
	}
}

void MProxyPlayerInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MProxyPlayerInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
}

void MProxyPlayerInteractionMode::MouseMove(int iX, int iY)
{
	int iDX;
	int iDY;

	iDX = iX - m_iLastX;
	iDY = iY - m_iLastY;

	if(m_kProxyState == Idle)
	{
		if (m_bLeftDown || m_bRightDown)
		{
			if ((!m_bRotating) && ((m_iStartX != iX) || (m_iStartY != iY)))
			{
				m_bRotating = true;
			}
			else if (m_bRotating)
			{
				if ((m_bMiddleDown) || (m_bOrthographic))
				{
					;
				}
				else
				{
					MEntity* pkProxyEntity = MFramework::Instance->ProxyManager->ProxyScene->GetEntityByName("Proxy Player");
					NiEntityInterface* pkProxyEntityInterface = MFramework::Instance->ProxyManager->ProxyScene->GetNiScene()->GetEntityByName("Proxy Player");

					NiPoint3 kProxyPosition = pkProxyEntity->GetSceneRootPointer(0)->GetWorldTranslate();

					// Camera
					NiCamera* pkCamera = MFramework::Instance->ViewportManager->ActiveViewport->GetNiCamera();
					MEntity* pmCamera = MFramework::Instance->ViewportManager->ActiveViewport->CameraEntity;
					NiEntityInterface* pkCameraEntity = pmCamera->GetNiEntityInterface();

					NiPoint3 kReturnPoint;
					NiMatrix3 kReturnRotation;
					kProxyPosition.z += 2.0f;
					NiViewMath::Orbit((float)iDX * m_fMouseLookScalar, (float)iDY * m_fMouseLookScalar, pkCamera->GetTranslate(), pkCamera->GetRotate(), kProxyPosition, NiPoint3::UNIT_Z, kReturnPoint, kReturnRotation);
					// Position
					float fHeight = CMap::Get()->GetHeight(kReturnPoint.x, kReturnPoint.y);
					if(kReturnPoint.z < fHeight)
					{
						kReturnPoint.z = fHeight;
					}
					else
					{
						pkCameraEntity->SetPropertyData("Rotation", kReturnRotation);
					}
				}
			}
		}
	}

	__super::MouseMove(iX, iY);
}

void MProxyPlayerInteractionMode::MouseWheel(int iDelta)
{
	m_fCamDist -= (float)iDelta * 0.1f * m_fZoomSpeed;
	if(m_fCamDist < m_fMinCameraDistance)
		m_fCamDist = m_fMinCameraDistance;
	if(m_fCamDist > m_fMaxCameraDistance)
		m_fCamDist = m_fMaxCameraDistance;
}

bool MProxyPlayerInteractionMode::LoadProxyPlayer()
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return false;

	MEntity* pkProxyEntity = MFramework::Instance->ProxyManager->ProxyScene->GetEntityByName("Proxy Player");
	if( pkProxyEntity )
	{
		return true;
	}

	NiEntityInterfaceIPtr spEntity = NiNew CPlayer;
	spEntity->Update(NULL, 0.f, NULL, MFramework::Instance->ExternalAssetManager);

	NiCamera* pkCamera = MFramework::Instance->ViewportManager->ActiveViewport->GetNiCamera();

	pkProxyEntity = MFramework::Instance->EntityFactory->Get(spEntity);
	MFramework::Instance->ProxyManager->ProxyScene->AddEntity(pkProxyEntity, true);

	NiAVObject* pkAVObject = pkProxyEntity->GetSceneRootPointer(0);
	NIASSERT( pkAVObject != NULL );


	// Set Show
	pkAVObject->SetAppCulled(false);
	
	pkAVObject->SetDisplayObject(true);

	NiPoint3 kData = pkCamera->GetWorldLocation();
	kData += NiPoint3(1.f, 1.f, -1.f);
	spEntity->SetPropertyData("Translation", kData, 0);
	spEntity->Update(NULL, 0.f, NULL, MFramework::Instance->ExternalAssetManager);

	SetupCamera();
	SetupExtraData();

	return true;
}

void MProxyPlayerInteractionMode::SetupCamera()
{
	MEntity* pkProxyEntity = MFramework::Instance->ProxyManager->ProxyScene->GetEntityByName("Proxy Player");

	NiAVObject* pkAVObject = pkProxyEntity->GetSceneRootPointer(0);
	NIASSERT( pkAVObject != NULL );

	MEntity* pmCamera = MFramework::Instance->ViewportManager->ActiveViewport->CameraEntity;

	NiMatrix3 kView = NiViewMath::LookAt(NiPoint3::ZERO, NiPoint3(-1.0, -1.0f, 1.0f + 2.0f), NiPoint3::UNIT_Z);
	pmCamera->SetPropertyData("Rotation", new MMatrix3(kView), false);
	kView.MakeIdentity();
	kView.MakeZRotation(-NI_PI * 0.75f);
	pkAVObject->SetRotate(kView);

	NiEntityInterface* pkProxyEntityInterface = MFramework::Instance->ProxyManager->ProxyScene->GetNiScene()->GetEntityByName("Proxy Player");
	NiEntityComponentInterface* pkEntityComponent = pkProxyEntityInterface->GetComponentAt(0);
	NiTransformationComponent* pkTransformComponent = (NiTransformationComponent*)pkEntityComponent;
	pkTransformComponent->SetRotation(kView);
}

void MProxyPlayerInteractionMode::SetupExtraData()
{
	MEntity* pkProxyEntity = MFramework::Instance->ProxyManager->ProxyScene->GetEntityByName("Proxy Player");
	NIASSERT( pkProxyEntity != NULL );

	NiAVObject* pkAVObject = pkProxyEntity->GetSceneRootPointer(0);
	NIASSERT( pkAVObject != NULL );

	NiCamera* pkCamera = MFramework::Instance->ViewportManager->ActiveViewport->GetNiCamera();

	// Extra Data Add
	NiFloatsExtraData* pkFloatsExtraData = (NiFloatsExtraData*)pkAVObject->GetExtraData("New Translation");
	float ExtraData[3] = { pkCamera->GetTranslate().x, pkCamera->GetTranslate().y, CMap::Get()->GetHeight(pkCamera->GetTranslate().x, pkCamera->GetTranslate().y) };
	if(!pkFloatsExtraData)
	{
		pkFloatsExtraData = NiNew NiFloatsExtraData(3, ExtraData);
		pkFloatsExtraData->SetName("New Translation");
		pkAVObject->AddExtraData(pkFloatsExtraData);
	}
	else
	{
		pkFloatsExtraData->SetArray(3, ExtraData);
	}
	
	pkFloatsExtraData = (NiFloatsExtraData*)pkAVObject->GetExtraData("Old Translation");
	if(!pkFloatsExtraData)
	{
		pkFloatsExtraData = NiNew NiFloatsExtraData(3, ExtraData);
		pkFloatsExtraData->SetName("Old Translation");
		pkAVObject->AddExtraData(pkFloatsExtraData);
	}
	else
	{
		pkFloatsExtraData->SetArray(3, &ExtraData[0]);
	}

	pkFloatsExtraData = (NiFloatsExtraData*)pkProxyEntity->GetSceneRootPointer(0)->GetExtraData("Velocity");
	memset(ExtraData, 0, sizeof(ExtraData));
	if(pkFloatsExtraData)
	{
		pkFloatsExtraData->SetValue(0, ExtraData[0]);
		pkFloatsExtraData->SetValue(1, ExtraData[1]);
		pkFloatsExtraData->SetValue(2, ExtraData[2]);
	}
	else
	{
		pkFloatsExtraData = NiNew NiFloatsExtraData(3, ExtraData);
		pkFloatsExtraData->SetName("Velocity");
		pkAVObject->AddExtraData(pkFloatsExtraData);
	}
}

void MProxyPlayerInteractionMode::Active()
{
	//显示Player
	MEntity* pkProxyEntity = MFramework::Instance->ProxyManager->ProxyScene->GetEntityByName("Proxy Player");
	if( pkProxyEntity != NULL )
	{
		NiAVObject* pkAVObject = pkProxyEntity->GetSceneRootPointer(0);
		if( pkAVObject != NULL )
		{
			pkAVObject->SetAppCulled(false);

			SetupExtraData();
		}
	}
}

void MProxyPlayerInteractionMode::Deactive()
{
	//隐藏Player
	MEntity* pkProxyEntity = MFramework::Instance->ProxyManager->ProxyScene->GetEntityByName("Proxy Player");
	if( pkProxyEntity != NULL )
	{
		NiAVObject* pkAVObject = pkProxyEntity->GetSceneRootPointer(0);
		if( pkAVObject != NULL )
		{
			pkAVObject->SetAppCulled(true);
		}
	}
}