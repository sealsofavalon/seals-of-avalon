#pragma once


using namespace System::Runtime::InteropServices;
using namespace System::Drawing;
using namespace System::Windows::Forms;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace TerrainPlugin
	{
		public __gc class MProxyPlayerInteractionMode : public MViewInteractionMode
		{
		protected:
			virtual void Do_Dispose(bool bDisposing);

		public:
			MProxyPlayerInteractionMode();

			[UICommandHandlerAttribute("Proxy Mode")]
			void SetInteractionMode(Object* pmObject, EventArgs* mArgs);

			[UICommandValidatorAttribute("Proxy Mode")]
			void ValidateInteractionMode(Object* pmSender, UIState* pmState);

			__property String* get_Name();

			bool CollideMouse(int iX, int iY, NiPoint3& kPos);

			void Update(float fTime);

			void MouseDown(MouseButtonType eType, int iX, int iY);
			void MouseUp(MouseButtonType eType, int iX, int iY);
			void MouseMove(int iX, int iY);
			void MouseWheel(int iDelta);

			virtual void Active();
			virtual void Deactive();

			void SetupCamera();
			void SetupExtraData();
			bool LoadProxyPlayer();

			__value enum ProxyState
			{
				Idle = 0,
				Run
			};

		protected:
			float m_fCamDist;  // follow-distance of the camera from the proxy

			float m_fCamHeight; // height of camera look-at point

			float m_fZoomSpeed;

			float m_fMaxCameraDistance;

			float m_fMinCameraDistance;

			ProxyState m_kProxyState;

			IInteractionMode* m_pmPrevInteractionMode;
		};

	}}}}