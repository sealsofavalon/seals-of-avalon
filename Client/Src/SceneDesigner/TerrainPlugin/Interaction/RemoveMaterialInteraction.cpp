#include "TerrainPluginPCH.h"
#include "RemoveMaterialInteraction.h"
#include "TerrainEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MRemoveMaterialInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MRemoveMaterialInteractionMode::MRemoveMaterialInteractionMode()
{
}

String* MRemoveMaterialInteractionMode::get_Name()
{
    return "Remove Material";
}

void MRemoveMaterialInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);

    const char* pcTextureName = NULL;
    if( PaintMaterialDialog::Get() && PaintMaterialDialog::Get()->ActiveTextureControl)
    {
        pcTextureName = PaintMaterialDialog::Get()->ActiveTextureControl->PathName;
    }


	if( CMap::Get() == NULL )
		return;

	//�޸ĸ߶�ֵ
	NiPoint3 kPos;
	if( !CollideMouse(iX, iY, kPos) )
	{
		return;
	}
	
	CMap::Get()->RemoveMaterial(int(NiFloor(kPos.x)),
								int(NiFloor(kPos.y)),
                                pcTextureName);
}

void MRemoveMaterialInteractionMode::Active()
{
	__super::Active();

	m_ParamDlg->ParamTextBox->Text = "";
	m_ParamDlg->ParamTrackBar->Value = 0;
	m_ParamDlg->ParamLable->Text = "";
	m_ParamDlg->ParamGroupBox->Enabled = false;
}

void MRemoveMaterialInteractionMode::Deactive()
{
	m_ParamDlg->ParamGroupBox->Enabled = true;

	__super::Deactive();
}