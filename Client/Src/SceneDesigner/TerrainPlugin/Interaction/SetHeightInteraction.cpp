#include "TerrainPluginPCH.h"
#include "SetHeightInteraction.h"
#include "TerrainEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MSetHeightInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MSetHeightInteractionMode::MSetHeightInteractionMode()
{
	m_bAlreadyBegin = false;
	
	m_fValueMax = 800.f;
	m_fValueMin = -200.f;
	m_fValue = m_fDefaultValue = 20.f;
}

String* MSetHeightInteractionMode::get_Name()
{
    return "Set Height";
}

void MSetHeightInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MSetHeightInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndSetHeight(iX, iY);
	}
}

void MSetHeightInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			//记录Undo数据
			BeginChangeHeightCommand();
			
			SetHeight(iX, iY);
			
			//记录Redo数据
			EndChangeHeightCommand();
		}
		else
		{
			BeginSetHeight(iX, iY);
		}
	}

	__super::MouseMove(iX, iY);
}

void MSetHeightInteractionMode::BeginSetHeight(int iX, int iY)
{	
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;
}

void MSetHeightInteractionMode::SetHeight(int iX, int iY)
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	float fHeight = m_fValue; 
	

	//修改高度值
	CMap* pkMap = CMap::Get();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		amSelectedPoints[i]->m_fHeight = fHeight;
		pkMap->SetHeight(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), amSelectedPoints[i]->m_fHeight);
	}
}

void MSetHeightInteractionMode::EndSetHeight(int iX, int iY)
{
	m_bAlreadyBegin = false;
}

void MSetHeightInteractionMode::Active()
{
	__super::Active();

	m_ParamDlg->ParamLable->Text = "Terrain Height:";
	UpdateParamDlg();
}

void MSetHeightInteractionMode::Deactive()
{
	__super::Deactive();
}