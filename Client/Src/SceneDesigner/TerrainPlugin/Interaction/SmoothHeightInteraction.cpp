#include "TerrainPluginPCH.h"
#include "SmoothHeightInteraction.h"
#include "TerrainEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MSmoothHeightInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );
}

MSmoothHeightInteractionMode::MSmoothHeightInteractionMode()
{
	m_bAlreadyBegin = false;
	
	m_fValue = m_fDefaultValue = 0.1f;
	m_fValueMax = 1.f;
	m_fValueMin = 0.f;
}

String* MSmoothHeightInteractionMode::get_Name()
{
    return "Smooth Height";
}

void MSmoothHeightInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MSmoothHeightInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndSmoothHeight(iX, iY);
	}
}

void MSmoothHeightInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{
			//记录Undo数据
			BeginChangeHeightCommand();
			
			SmoothHeight(iX, iY);
			
			//记录Redo数据
			EndChangeHeightCommand();
		}
		else
		{
			BeginSmoothHeight(iX, iY);
		}
	}

	__super::MouseMove(iX, iY);
}

void MSmoothHeightInteractionMode::BeginSmoothHeight(int iX, int iY)
{	
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;
}

void MSmoothHeightInteractionMode::SmoothHeight(int iX, int iY)
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	//计算平均值
	float fAvgHeight = 0.f;
	for(int i = 0; i < amSelectedPoints->Count; i++)
	{
		fAvgHeight += amSelectedPoints[i]->m_fHeight;
	}

	fAvgHeight /= amSelectedPoints->Count;

	//修改高度值
	CMap* pkMap = CMap::Get();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		amSelectedPoints[i]->m_fHeight += amSelectedPoints[i]->m_fWeight*m_fValue*(fAvgHeight - amSelectedPoints[i]->m_fHeight);
		pkMap->SetHeight(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), amSelectedPoints[i]->m_fHeight);
	}
}

void MSmoothHeightInteractionMode::EndSmoothHeight(int iX, int iY)
{
	m_bAlreadyBegin = false;
}

void MSmoothHeightInteractionMode::Active()
{
	__super::Active();

	m_ParamDlg->ParamLable->Text = "Smooth Coefficient:";
	UpdateParamDlg();
}

void MSmoothHeightInteractionMode::Deactive()
{
	__super::Deactive();
}