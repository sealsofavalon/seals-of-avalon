
#include "TerrainPluginPCH.h"
#include "TerrainEditInteraction.h"
#include "TerrainEditDialog.h"
#include "TileEditDialog.h"
#include "Command/MTerrainEditCommand.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MTerrainEditInteractionMode::Do_Dispose(bool bDisposing)
{
	__super::Do_Dispose( bDisposing );

	if( m_pkBorderGeometry )
	{
		NiDelete m_pkBorderGeometry;
		m_pkBorderGeometry = NULL;
	}

	if( m_pkUndoPoints )
	{
		NiDelete[] m_pkUndoPoints;
		m_pkUndoPoints = NULL;
	}
}

MTerrainEditInteractionMode::MTerrainEditInteractionMode()
{
	m_fValueMax = 1.f;
	m_fValueMin = 0.f;

	m_fValue = m_fDefaultValue = 0.f;

	m_pkBorderGeometry = NULL;
	m_pkBorderVerts = NULL;

	m_pkUndoPoints = NULL;
}

//判断鼠标和地面的交点
bool MTerrainEditInteractionMode::CollideMouse(int iX, int iY, NiPoint3& kPos)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return false;

	NiPoint3 kOrigin, kDir;
	NiViewMath::MouseToRay((float) iX, (float) iY, 
		MFramework::Instance->ViewportManager->ActiveViewport->Width,
		MFramework::Instance->ViewportManager->ActiveViewport->Height,
		MFramework::Instance->ViewportManager->ActiveViewport->
		GetNiCamera(), kOrigin, kDir);

	NiPoint3 kEnd;
	kEnd = kOrigin + kDir * MAX_DISTANCE;

	float fTime;
	if( pkMap->CollideLine(kOrigin, kEnd, fTime) )
	{
		kPos = kOrigin + kDir*(fTime*MAX_DISTANCE);
		return true;
	}

	return false;
}

void MTerrainEditInteractionMode::Update(float fTime)
{
	__super::Update(fTime);

	if( m_pkMouseBrush )
	{
		bool bLeftSquareBracketsDown = ((GetAsyncKeyState(VK_OEM_4) & 0x8000) != 0); //[ Key
		bool bRightSquareBracketsDown = ((GetAsyncKeyState(VK_OEM_6) & 0x8000) != 0); //] Key

		if( bLeftSquareBracketsDown )
		{
			//增加顶点刷的大小
			int x, y;
			m_pkMouseBrush->GetSize(x, y);
			m_pkMouseBrush->SetSize(x - 2, y - 2);
		}
		else if( bRightSquareBracketsDown )
		{
			//减少顶点刷的大小
			int x, y;
			m_pkMouseBrush->GetSize(x, y);
			m_pkMouseBrush->SetSize(x + 2, y + 2);
		}
	}	
}

void MTerrainEditInteractionMode::RenderGizmo(MRenderingContext* pmRenderingContext)
{
	if( m_pkMouseBrush )
		m_pkMouseBrush->Render(pmRenderingContext);

	RenderCurrentTileBorder(pmRenderingContext);
}

//显示当前中区的边框
void MTerrainEditInteractionMode::RenderCurrentTileBorder(MRenderingContext* pmRenderingContext)
{
	CMap* pkMap = CMap::Get(); 
	if( !pkMap )
		return;

	CTile* pkTile = CMap::Get()->GetCurrentTile();
	if( !pkTile )
		return;

	if( m_pkBorderGeometry == NULL )
	{
		int iVertCount = TILE_SIZE / UNIT_SIZE * 4 + 1;
		m_pkBorderVerts = NiNew NiPoint3[iVertCount];

		NiColorA* pkColors = NiNew NiColorA[iVertCount];
		NiBool* pbConnections = NiAlloc(NiBool, iVertCount);

		for( int i = 0; i < iVertCount; i++ )
		{
			pkColors[i].r = 0.8f;
			pkColors[i].g = 0.f;
			pkColors[i].b = 0.f;
			pkColors[i].a = 1.f;

			pbConnections[i] = true;
		}

		pbConnections[iVertCount - 1] = false;

		m_pkBorderGeometry = NiNew NiLines(iVertCount, m_pkBorderVerts, pkColors, NULL, 0, 
        NiGeometryData::NBT_METHOD_NONE, pbConnections);

		NiVertexColorProperty* pkVertexColorProp = NiNew NiVertexColorProperty();

		pkVertexColorProp->SetSourceMode(NiVertexColorProperty::SOURCE_EMISSIVE);
		pkVertexColorProp->SetLightingMode(NiVertexColorProperty::LIGHTING_E_A_D);
		m_pkBorderGeometry->AttachProperty(pkVertexColorProp);

		//NiZBufferProperty* pkZBufferProperty = NiNew NiZBufferProperty();
		//pkZBufferProperty->SetZBufferTest(true);
		//m_pkBorderGeometry->AttachProperty(pkZBufferProperty);

		m_pkBorderGeometry->UpdateProperties();
	}

	float fZOffset = 1.f;
	int i = 0;
	for( float x = 0; x < float(TILE_SIZE); x += float(UNIT_SIZE) )
	{
		m_pkBorderVerts[i].x = float(pkTile->GetX()*TILE_SIZE) + x;
		m_pkBorderVerts[i].y = float(pkTile->GetY()*TILE_SIZE);

		m_pkBorderVerts[i].z = pkMap->GetHeight(m_pkBorderVerts[i].x, m_pkBorderVerts[i].y) + fZOffset;
		i++;
	}

	for( float y = 0; y < float(TILE_SIZE); y += float(UNIT_SIZE) )
	{
		m_pkBorderVerts[i].x = float(pkTile->GetX()*TILE_SIZE + TILE_SIZE);
		m_pkBorderVerts[i].y = float(pkTile->GetY()*TILE_SIZE) + y;

		m_pkBorderVerts[i].z = pkMap->GetHeight(m_pkBorderVerts[i].x, m_pkBorderVerts[i].y) + fZOffset;
		i++;
	}

	for( float x = 0; x < float(TILE_SIZE); x += float(UNIT_SIZE) )
	{
		m_pkBorderVerts[i].x = float(pkTile->GetX()*TILE_SIZE + TILE_SIZE) - x;
		m_pkBorderVerts[i].y = float(pkTile->GetY()*TILE_SIZE + TILE_SIZE);

		m_pkBorderVerts[i].z = pkMap->GetHeight(m_pkBorderVerts[i].x, m_pkBorderVerts[i].y) + fZOffset;
		i++;
	}

	for( float y = 0; y < float(TILE_SIZE); y += float(UNIT_SIZE) )
	{
		m_pkBorderVerts[i].x = float(pkTile->GetX()*TILE_SIZE);
		m_pkBorderVerts[i].y = float(pkTile->GetY()*TILE_SIZE + TILE_SIZE) - y;

		m_pkBorderVerts[i].z = pkMap->GetHeight(m_pkBorderVerts[i].x, m_pkBorderVerts[i].y) + fZOffset;
		i++;
	}

	//最后一点和第一点重合
	m_pkBorderVerts[i] = m_pkBorderVerts[0];

	m_pkBorderGeometry->GetModelData()->MarkAsChanged(NiGeometryData::VERTEX_MASK);
	m_pkBorderGeometry->Update(0.0f);

	NiEntityRenderingContext* pkContext = pmRenderingContext->GetRenderingContext();
	NiVisibleArray kVisibleArray;
	kVisibleArray.Add(*m_pkBorderGeometry);
	NiDrawVisibleArray(pkContext->m_pkCamera, kVisibleArray);
}

void MTerrainEditInteractionMode::DoubleClick(MouseButtonType eType, int iX, int iY)
{
	NiPoint3 kPos;
	if( eType == MouseButtonType::RightButton && CollideMouse(iX, iY, kPos) )
	{
		bool bCtrlKeyDown = ((GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0);
		if( bCtrlKeyDown )
		{
			CMap* pkMap = CMap::Get();

			CTile* pkCurrentTile = pkMap->GetCurrentTile();
			CTile* pkTile = pkMap->GetTile(kPos.x, kPos.y);

			if( pkTile != pkCurrentTile )
			{
				TileEditDialog::Goto(kPos.x, kPos.y);
				TileEditDialog::ChangeLastTileSettings(kPos.x, kPos.y);
			}
		}
	}
}

void MTerrainEditInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
}

void MTerrainEditInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
}

void MTerrainEditInteractionMode::MouseMove(int iX, int iY)
{
	if( m_iMouseX == iX && m_iMouseY == iY )
		return;

	if( !m_pkMouseBrush )
		return;

	m_iMouseX = iX;
	m_iMouseY = iY;

	NiPoint3 kPos;
	if( CollideMouse(iX, iY, kPos) )
	{
		m_pkMouseBrush->Update(kPos);
		//todo... 隐藏鼠标
	}
	else
	{
		m_pkMouseBrush->Clear();
		//todo... 显示鼠标
	}

	__super::MouseMove(iX, iY);
}

//显示当前坐标
String* MTerrainEditInteractionMode::GetHoverData([Out]int* piX, [Out]int* piY)
{
	*piX = 150;
	*piY = 0;

	char acBuffer[1024];
	if( g_pkPlayer && !g_pkPlayer->IsFreeCameraMode() )
	{
		NiSprintf(acBuffer, sizeof(acBuffer), "Player %f %f %f %f",
			g_pkPlayer->GetPosition().x,
			g_pkPlayer->GetPosition().y,
			g_pkPlayer->GetPosition().z,
			g_pkPlayer->GetFacing());
	}
	else
	{
		NiSprintf(acBuffer, sizeof(acBuffer), "Pos %f %f %f",
			m_pkMouseBrush->GetCenterPoint().x,
			m_pkMouseBrush->GetCenterPoint().y,
			m_pkMouseBrush->GetCenterPoint().z);
	}

	return acBuffer;
}

void MTerrainEditInteractionMode::UpdateParamDlg()
{
	m_ParamDlg->ParamTextBox->Text = m_fValue.ToString();
	m_ParamDlg->ParamTrackBar->Value = int( (m_fValue - m_fValueMin) / (m_fValueMax - m_fValueMin) * 1000.f);
}

bool MTerrainEditInteractionMode::OnParamTextBoxValidating()
{
	for( int i = 0; i < m_ParamDlg->ParamTextBox->Text->Length; i++ )
	{
		if( i == 0 && m_ParamDlg->ParamTextBox->Text->Chars[i] == '-' )
			continue;

		if( m_ParamDlg->ParamTextBox->Text->Chars[i] == '.'  )
			continue;

		if( m_ParamDlg->ParamTextBox->Text->Chars[i] <= '9' && m_ParamDlg->ParamTextBox->Text->Chars[i] >= '0' )
			continue;

		m_fValue = m_fDefaultValue;
		UpdateParamDlg();

		return false;
	}

	return true;
}

void MTerrainEditInteractionMode::OnParamTrackBarChanged()
{
	m_fValue = m_ParamDlg->ParamTrackBar->Value / 1000.f * (m_fValueMax - m_fValueMin) + m_fValueMin;
	m_fValue = NiClamp( m_fValue, m_fValueMin, m_fValueMax );
	UpdateParamDlg();
}

void MTerrainEditInteractionMode::OnParamTextBoxChanged()
{
	try
	{
		//输入非数字的时候会出现异常
		m_fValue = (float)System::Convert::ToDouble(m_ParamDlg->ParamTextBox->Text);
	}
	catch(...)
	{
		m_fValue = 0.f;
	}
	m_fValue = NiClamp( m_fValue, m_fValueMin, m_fValueMax );
	UpdateParamDlg();
}

void MTerrainEditInteractionMode::Active()
{
}

void MTerrainEditInteractionMode::Deactive()
{
}

//记录Undo数据
void MTerrainEditInteractionMode::BeginChangeHeightCommand()
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	if( m_pkUndoPoints != NULL )
		NiDelete[] m_pkUndoPoints;

	m_pkUndoPoints = NiNew NiPoint3[amSelectedPoints->Count];

	CMap* pkMap = CMap::Get();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		m_pkUndoPoints[i].x = amSelectedPoints[i]->m_fX;
		m_pkUndoPoints[i].y = amSelectedPoints[i]->m_fY;
		m_pkUndoPoints[i].z = pkMap->GetHeight(m_pkUndoPoints[i].x, m_pkUndoPoints[i].y);
	}
}

//记录Redo数据
void MTerrainEditInteractionMode::EndChangeHeightCommand()
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	if( m_pkUndoPoints == NULL )
		return;

	//注意Undo和Redo的大小必须一样
	NiPoint3* pkRedoPoints = NiNew NiPoint3[amSelectedPoints->Count];

	CMap* pkMap = CMap::Get();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		pkRedoPoints[i].x = m_pkUndoPoints[i].x;
		pkRedoPoints[i].y = m_pkUndoPoints[i].y;
		pkRedoPoints[i].z = pkMap->GetHeight(m_pkUndoPoints[i].x, m_pkUndoPoints[i].y);
	}

	CommandService->ExecuteCommand(new MTerrainEditCommand(NiNew ChangeHeightCommand(m_pkUndoPoints, pkRedoPoints, amSelectedPoints->Count)), true);

	m_pkUndoPoints = NULL;
}
