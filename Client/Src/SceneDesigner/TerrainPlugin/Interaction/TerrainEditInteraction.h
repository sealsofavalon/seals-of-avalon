#pragma once

#include "TerrainEditBrush.h"
#include <list>
using namespace System::Runtime::InteropServices;
using namespace System::Drawing;
using namespace System::Windows::Forms;
using namespace std;

#define MAX_DISTANCE 2000.f  

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{
	public __gc class TerrainEditDialog;

    public __gc class MTerrainEditInteractionMode : public MViewInteractionMode
    {
	protected:
        virtual void Do_Dispose(bool bDisposing);

	public:
		MTerrainEditInteractionMode();

		bool CollideMouse(int iX, int iY, NiPoint3& kPos);

		void Update(float fTime);
		void RenderGizmo(MRenderingContext* pmRenderingContext);
		void RenderCurrentTileBorder(MRenderingContext* pmRenderingContext);

		virtual void DoubleClick(MouseButtonType eType, int iX, int iY);
		virtual void MouseDown(MouseButtonType eType, int iX, int iY);
        virtual void MouseUp(MouseButtonType eType, int iX, int iY);
        virtual void MouseMove(int iX, int iY);
		virtual String* GetHoverData([Out]int* piX, [Out]int* piY);

		virtual void Active();
		virtual void Deactive();

		void UpdateParamDlg();
		virtual bool OnParamTextBoxValidating();
		virtual void OnParamTrackBarChanged();
		virtual void OnParamTextBoxChanged();

		__property void set_ParamDlg(TerrainEditDialog* ParamDlg)
		{
			m_ParamDlg = ParamDlg;
		}

		__property void set_MouseBrush(MTerrainEditBrush* pMouseBrush)
		{
			m_pkMouseBrush = pMouseBrush;
		}

		//��¼Undo, Redo����
		void BeginChangeHeightCommand();
		void EndChangeHeightCommand();

	protected:
		int m_iMouseX;
		int m_iMouseY;

		float m_fValueMax;
		float m_fValueMin;
		float m_fValue;
		float m_fDefaultValue;

		TerrainEditDialog* m_ParamDlg;
		MTerrainEditBrush* m_pkMouseBrush;

		NiLines* m_pkBorderGeometry;
		NiPoint3* m_pkBorderVerts;

		NiPoint3* m_pkUndoPoints;
   };

}}}}