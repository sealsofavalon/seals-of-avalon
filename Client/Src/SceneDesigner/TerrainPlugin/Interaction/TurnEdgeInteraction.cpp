#include "TerrainPluginPCH.h"
#include "TurnEdgeInteraction.h"
#include "TerrainEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void MTurnEdgeInteractionMode::Do_Dispose(bool bDisposing)
{
	if( m_pkTurnEdgeCommand )
		NiDelete m_pkTurnEdgeCommand;

	__super::Do_Dispose( bDisposing );
}

MTurnEdgeInteractionMode::MTurnEdgeInteractionMode()
{
	m_bAlreadyBegin = false;
	m_pkTurnEdgeCommand = NULL;
}

String* MTurnEdgeInteractionMode::get_Name()
{
    return "Turn Edge";
}

void MTurnEdgeInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	__super::MouseDown(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		BeginTurnEdge(iX, iY);
		TurnEdge(iX, iY);
	}
}

void MTurnEdgeInteractionMode::MouseUp(MouseButtonType eType, int iX, int iY)
{
	__super::MouseUp(eType, iX, iY);
	if( eType == MouseButtonType::LeftButton )
	{
		EndTurnEdge(iX, iY);
	}
}

void MTurnEdgeInteractionMode::MouseMove(int iX, int iY)
{
	if( m_bLeftDown )
	{
		if( m_bAlreadyBegin )
		{	
			TurnEdge(iX, iY);
		}
	}

	__super::MouseMove(iX, iY);
}

void MTurnEdgeInteractionMode::BeginTurnEdge(int iX, int iY)
{	
	if( !m_pkMouseBrush )
		return;

	m_bAlreadyBegin = true;

	if( m_pkTurnEdgeCommand )
		m_pkTurnEdgeCommand->Clear();
	else
		m_pkTurnEdgeCommand = NiNew TurnEdgeCommand;
}

void MTurnEdgeInteractionMode::TurnEdge(int iX, int iY)
{
	MPointInfo* amSelectedPoints[] = m_pkMouseBrush->GetSelectedPoints();
	if( amSelectedPoints->Count == 0 )
		return;

	bool bShiftKeyDown = (GetAsyncKeyState(VK_SHIFT) & 0x8000) != 0;
	bool bCtrlKeyDown = (GetAsyncKeyState(VK_CONTROL) & 0x8000) != 0;
	bool bTurnEdge;

	CMap* pkMap = CMap::Get();
	for( int i = 0; i < amSelectedPoints->Count; i++ )
	{
		if(bShiftKeyDown)
			bTurnEdge = true;
		else if(bCtrlKeyDown)
			bTurnEdge = false;
		else
			bTurnEdge = !pkMap->IsTurnEdge(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY));

		if( pkMap->TurnEdge(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), bTurnEdge) )
		{
			m_pkTurnEdgeCommand->AddTurnEdge(int(amSelectedPoints[i]->m_fX), int(amSelectedPoints[i]->m_fY), bTurnEdge);
		}
	}
}

void MTurnEdgeInteractionMode::EndTurnEdge(int iX, int iY)
{
	m_bAlreadyBegin = false;

	CommandService->ExecuteCommand(new MTerrainEditCommand(m_pkTurnEdgeCommand), true);
	m_pkTurnEdgeCommand = NULL;
}

void MTurnEdgeInteractionMode::Active()
{
	__super::Active();

	m_pkMouseBrush->SetCenterGrid(true);
	m_pkMouseBrush->SetSize(1, 1);

	m_ParamDlg->ParamTextBox->Text = "";
	m_ParamDlg->ParamTrackBar->Value = 0;
	m_ParamDlg->ParamLable->Text = "";
	m_ParamDlg->ParamGroupBox->Enabled = false;
}

void MTurnEdgeInteractionMode::Deactive()
{
	m_ParamDlg->ParamGroupBox->Enabled = true;

	m_pkMouseBrush->SetCenterGrid(false);

	__super::Deactive();
}