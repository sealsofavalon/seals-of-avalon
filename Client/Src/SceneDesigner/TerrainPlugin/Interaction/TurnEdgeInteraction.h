#pragma once

#include "TerrainEditInteraction.h"
#include "Command/TurnEdgeCommand.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{
    public __gc class MTurnEdgeInteractionMode : public MTerrainEditInteractionMode
    {
    protected:
        virtual void Do_Dispose(bool bDisposing);

	public:
		MTurnEdgeInteractionMode();

		__property String* get_Name();

		void MouseDown(MouseButtonType eType, int iX, int iY);
        void MouseUp(MouseButtonType eType, int iX, int iY);
		void MouseMove(int iX, int iY);

		void BeginTurnEdge(int iX, int iY);
		void TurnEdge(int iX, int iY);
		void EndTurnEdge(int iX, int iY);

		virtual void Active();
		virtual void Deactive();

	private:
		bool m_bAlreadyBegin;
		TurnEdgeCommand* m_pkTurnEdgeCommand;
    };

}}}}