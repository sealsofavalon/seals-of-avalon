#include "TerrainPluginPCH.h"
#include "WayPointCreateInteraction.h"
#include "NPCComponent.h"
#include "TerrainHelper.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

MWayPointCreateInteractionMode::MWayPointCreateInteractionMode()
{
	m_pkPickRecord = NiNew NiPick::Record(NULL);
	m_pkPickRecord->SetDistance(NI_INFINITY);
	m_pkPickRecord->SetIntersection(NiPoint3::ZERO);
	m_pkPickRecord->SetNormal(NiPoint3::ZERO);
}

void MWayPointCreateInteractionMode::Do_Dispose(bool bDisposing)
{
	NiDelete m_pkPickRecord;
	__super::Do_Dispose(bDisposing);
}

String* MWayPointCreateInteractionMode::get_Name()
{
	return "Create Way Point";
}

void MWayPointCreateInteractionMode::MouseDown(MouseButtonType eType, int iX, int iY)
{
	if (eType != MouseButtonType::LeftButton)
	{
		__super::MouseDown(eType, iX, iY);
		return;
	}

	if(SelectionService->SelectedEntity == NULL)
	{
		SelectEntity(eType, iX, iY);
		return;
	}

	NiEntityInterface* pkSelectedEntity = SelectionService->SelectedEntity->GetNiEntityInterface();
	NiEntityInterface* pkNpcEntity = NULL;
	CNpcComponent* pkNpcComponent = NULL;
 	unsigned int uiIndex = 0;

	if( TerrainHelper::IsNpcEntity(pkSelectedEntity) )
	{
		//当前选择的是一个NPC, 把新创建的WayPoint添加到NPC的最后一个路点
		pkNpcEntity = pkSelectedEntity;
		pkNpcComponent = (CNpcComponent*)pkSelectedEntity->GetComponentByTemplateID(CNpcComponent::ms_kTemplateID);
		uiIndex = pkNpcComponent->GetWayPointCount();
	}
	else if( TerrainHelper::IsWayPointEntity(pkSelectedEntity) )
	{
		//当前选择的是一个WayPoint, 把新创建的WayPoint添加到当前WayPoint后面
		CWayPointComponent* pkWayPointComponent = (CWayPointComponent*)pkSelectedEntity->GetComponentByTemplateID(CWayPointComponent::ms_kTemplateID);
		pkNpcEntity = pkWayPointComponent->GetNpcEntity();
		pkNpcComponent = (CNpcComponent*)pkNpcEntity->GetComponentByTemplateID(CNpcComponent::ms_kTemplateID);
		uiIndex = pkNpcComponent->GetWayPointIndex(pkSelectedEntity) + 1;
	}
	else
	{
		SelectEntity(eType, iX, iY);
		return;
	}

	// make sure we are placing it on the ideal plane relative to the view
	NiPoint3 kLook;
	NiCamera* pkCamera;
	pkCamera = MFramework::Instance->ViewportManager->ActiveViewport->GetNiCamera();
	(pkCamera->GetRotate()).GetCol(0, kLook);
	GetBestPlane(kLook.x, kLook.y, kLook.z);

	// create a way point object
	NiUniqueID kTemplateID;
	MUtility::GuidToID(Guid::NewGuid(), kTemplateID);

	String* strName = MFramework::Instance->Scene->GetUniqueEntityName("WayPoint 01");	
	const char* pcName = MStringToCharPointer(strName);
	NiGeneralEntityPtr spEntity = NiNew NiGeneralEntity(pcName, kTemplateID, 2);
    MFreeCharPointer(pcName);
    spEntity->SetTypeMask(NiGeneralEntity::WayPoint);

	// Add transformation component.
	spEntity->AddComponent(NiNew NiTransformationComponent(), false);

	// Add way point component.
	CWayPointComponent* pkWayPointComponent = NiNew CWayPointComponent();
	pkWayPointComponent->SetNpcEntity(pkNpcEntity);
	spEntity->AddComponent(pkWayPointComponent, false);
	pkNpcComponent->SetWayPoint(uiIndex, spEntity);

	m_pmNewEntity = MFramework::Instance->EntityFactory->Get(spEntity);
	m_pmNewEntity->Update(MFramework::Instance->TimeManager->CurrentTime, MFramework::Instance->ExternalAssetManager);

	// place the entity based off of the mouse location
	CreateHelper((float)iX, (float)iY);

	MFramework::Instance->Scene->AddEntity(m_pmNewEntity, true);
	SelectionService->SelectedEntity = m_pmNewEntity;
}

void MWayPointCreateInteractionMode::SelectEntity(MouseButtonType eType, int iX, int iY)
{
	MViewInteractionMode::MouseDown(eType, iX, iY);
	NiPoint3 kOrigin, kDir;
	NiViewMath::MouseToRay((float) iX, (float) iY, 
		MFramework::Instance->ViewportManager->ActiveViewport->Width,
		MFramework::Instance->ViewportManager->ActiveViewport->Height,
		MFramework::Instance->ViewportManager->ActiveViewport->
		GetNiCamera(), kOrigin, kDir);

	MEntity* pmPickedEntity;
	float fPickDistance = NI_INFINITY;
	if (MFramework::Instance->PickUtility->PerformPick(
		MFramework::Instance->Scene, kOrigin, kDir, false))
	{
		const NiPick* pkPick =
			MFramework::Instance->PickUtility->GetNiPick();
		const NiPick::Results& kPickResults = pkPick->GetResults();
		NiPick::Record* pkPickRecord = kPickResults.GetAt(0);
		if (pkPickRecord)
		{
			if (pkPickRecord->GetDistance() < fPickDistance)
			{
				m_pkPickRecord->SetDistance(pkPickRecord->GetDistance());
				m_pkPickRecord->SetIntersection(
					pkPickRecord->GetIntersection());
				m_pkPickRecord->SetNormal(pkPickRecord->GetNormal());
				fPickDistance = pkPickRecord->GetDistance();
				NiAVObject* pkPickedObject = pkPickRecord->GetAVObject();
				pmPickedEntity = MFramework::Instance->PickUtility
					->GetEntityFromPickedObject(pkPickedObject);
			}
		}
	}

	MFramework::Instance->ProxyManager->UpdateProxyScales(
		MFramework::Instance->ViewportManager->ActiveViewport);

	if (MFramework::Instance->PickUtility->PerformPick(
		MFramework::Instance->ProxyManager->ProxyScene, kOrigin, kDir,
		false))
	{
		const NiPick* pkPick =
			MFramework::Instance->PickUtility->GetNiPick();
		const NiPick::Results& kPickResults = pkPick->GetResults();
		NiPick::Record* pkPickRecord = kPickResults.GetAt(0);
		if (pkPickRecord)
		{
			if (pkPickRecord->GetDistance() < fPickDistance)
			{
				m_pkPickRecord->SetDistance(pkPickRecord->GetDistance());
				m_pkPickRecord->SetIntersection(
					pkPickRecord->GetIntersection());
				m_pkPickRecord->SetNormal(pkPickRecord->GetNormal());
				fPickDistance = pkPickRecord->GetDistance();
				NiAVObject* pkPickedProxy = pkPickRecord->GetAVObject();
				MEntity* pmPickedProxy = MFramework::Instance->PickUtility
					->GetEntityFromPickedObject(pkPickedProxy);
				if (pmPickedProxy != NULL &&
					pmPickedProxy->HasProperty("Source Entity"))
				{
					MEntity* pmEntity = dynamic_cast<MEntity*>(
						pmPickedProxy->GetPropertyData("Source Entity"));
					if (pmEntity != NULL)
					{
						if (MFramework::Instance->Scene->IsEntityInScene(
							pmEntity))
						{
							pmPickedEntity = pmEntity;
						}
					}
				}
			}
		}
	}

	if (pmPickedEntity != NULL)
	{
		if (GetAsyncKeyState(VK_CONTROL) & 0x8000)
		{
			// first, check if the picked entity is already selected
			bool bAddSelection = true;
			MEntity* amSelection[] = 
				SelectionService->GetSelectedEntities();
			for (int i = 0; i < amSelection->Count; i++)
			{
				if (amSelection[i] == pmPickedEntity)
				{
					bAddSelection = false;
					break;
				}
			}
			if (bAddSelection)
			{
				SelectionService->AddEntityToSelection(pmPickedEntity);
			}
		}
		else if (GetAsyncKeyState(VK_MENU) & 0x8000)
		{
			// first check that the entity is in the selection
			bool bRemove = false;
			MEntity* amSelection[] = 
				SelectionService->GetSelectedEntities();
			for (int i = 0; i < amSelection->Count; i++)
			{
				if (amSelection[i] == pmPickedEntity)
				{
					bRemove = true;
					break;
				}
			}
			if (bRemove)
			{
				SelectionService->RemoveEntityFromSelection(
					pmPickedEntity);
			}
		}
		else
		{
			// check that the selection doesn't match the entity
			if ((SelectionService->NumSelectedEntities != 1) || 
				(SelectionService->SelectedEntity != pmPickedEntity))
			{
				SelectionService->SelectedEntity = pmPickedEntity;
			}
		}
	}
	else
	{
		SelectionService->SelectedEntity = NULL;
	}
}
