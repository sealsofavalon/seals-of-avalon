#pragma once

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{

	public __gc class MWayPointCreateInteractionMode : public MCreateInteractionMode
	{
	protected:
		NiPick::Record* m_pkPickRecord;
		
	// MDisposable members.
    protected:
        virtual void Do_Dispose(bool bDisposing);

	public:
		MWayPointCreateInteractionMode();

		__property String* get_Name();
		void MouseDown(MouseButtonType eType, int iX, int iY);
		void SelectEntity(MouseButtonType eType, int iX, int iY);
	};

}}}}