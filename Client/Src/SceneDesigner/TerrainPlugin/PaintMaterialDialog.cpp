
#include "TerrainPluginPCH.h"
#include "PaintMaterialDialog.h"
#include "TerrainEditDialog.h"
#include "TextureProperty.h"
#include "FreeImage.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

//===========================================================================================================
//Brush Control
//

void BrushControl::Init(PaintMaterialDialog* dlg, int x, int y, int id)
{
	m_ParentDlg = dlg;
	m_BrushName = NiNew NiFixedString;

	// 
	//m_PictureBox
	// 
	this->m_PictureBox = (new System::Windows::Forms::PictureBox());
	this->m_PictureBox->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
	this->m_PictureBox->Location = System::Drawing::Point(x + 19, y + 20);
	this->m_PictureBox->Name = S"m_PictureBox";
	this->m_PictureBox->Size = System::Drawing::Size(112, 73);
	this->m_PictureBox->TabIndex = 0;
	this->m_PictureBox->TabStop = false;
	this->m_PictureBox->Paint += new System::Windows::Forms::PaintEventHandler(this, &BrushControl::OnPictureBoxPaint);
	this->m_PictureBox->Click += new System::EventHandler(this, &BrushControl::OnPictureBoxClick);
	this->m_PictureBox->MouseHover += new System::EventHandler(this, &BrushControl::OnPictureBoxMouseHover);
	// 
	//m_ChangeBtn
	//
	this->m_ChangeButton = (new System::Windows::Forms::Button);
	this->m_ChangeButton->Location = System::Drawing::Point(x + 45, y + 105);
	this->m_ChangeButton->Name = S"m_ChangeBtn";
	this->m_ChangeButton->Size = System::Drawing::Size(60, 23);
	this->m_ChangeButton->TabIndex = 1;
	this->m_ChangeButton->Text = S"Change";
	this->m_ChangeButton->UseVisualStyleBackColor = true;
	this->m_ChangeButton->Click += new System::EventHandler(this, &BrushControl::OnChangeButtonClick);
	//
	//m_ToolTip
	//
	this->m_ToolTip = (new System::Windows::Forms::ToolTip);
	this->m_ToolTip->InitialDelay = 200;
	this->m_ToolTip->ReshowDelay = 100;
	this->m_ToolTip->AutoPopDelay = 5000;
	this->m_ToolTip->ShowAlways = true;
	//
	//m_Bitmap
	//
	this->m_Bitmap = NULL;
	this->m_DisplayBitmap = NULL;
	this->m_pkTexture = NULL;

	m_iID = id;
}

void BrushControl::LoadBrush(const char* TextureName)
{
	if( TextureName )
	{
		if( m_Bitmap )
		{
			TerrainPluginHelper::UnloadImage(m_Bitmap);
			TerrainPluginHelper::UnloadImage(m_DisplayBitmap);
		}

		m_Bitmap = TerrainPluginHelper::LoadImage(TextureName);
		m_DisplayBitmap = TerrainPluginHelper::RescaleImage(m_Bitmap, m_PictureBox->Width, m_PictureBox->Height);
		m_PictureBox->Invalidate();

		if( m_pkTexture )
		{
			m_pkTexture->DecRefCount();
			m_pkTexture = NULL;
		}

		*m_BrushName = TextureName;
	}
}


System::Void BrushControl::OnPictureBoxPaint(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e) 
{
    if( !m_DisplayBitmap )
        return;

	IntPtr hDC = e->Graphics->GetHdc();
	TerrainPluginHelper::DrawImage((HDC)hDC.ToInt32(), 0, 0, m_PictureBox->Width, m_PictureBox->Height, m_DisplayBitmap); 
	e->Graphics->ReleaseHdc(hDC);

	if( m_ParentDlg->ActiveBrushControl == this )
	{
		Pen* RedPen = new Pen(Color::FromArgb(255, 255, 0, 0), 4);
		e->Graphics->DrawRectangle(RedPen, 4, 4, m_PictureBox->Width - 10, m_PictureBox->Height - 10);
	}
}

System::Void BrushControl::OnPictureBoxClick(System::Object*  sender, System::EventArgs*  e)
{
	if( !m_Bitmap )
		return;

	if( m_ParentDlg->ActiveBrushControl )
		m_ParentDlg->ActiveBrushControl->PictureBox->Invalidate();

	m_ParentDlg->ActiveBrushControl = this;
	m_PictureBox->Invalidate();

	TerrainEditDialog::Get()->SetInteractionMode("Paint Material");
}

System::Void BrushControl::OnPictureBoxMouseHover(System::Object*  sender, System::EventArgs*  e)
{
	if( !m_BrushName )
		return;

	this->m_ToolTip->RemoveAll();
	this->m_ToolTip->Show(*m_BrushName, this->m_PictureBox, 1000 * 20);
}

System::Void BrushControl::OnChangeButtonClick(System::Object*  sender, System::EventArgs*  e) 
{
	m_ParentDlg->ChangeBrush(m_iID);
}

//===========================================================================================================
//Texture Control
//

void TextureControl::Init(PaintMaterialDialog* dlg, int x, int y)
{
	m_ParentDlg = dlg;

	m_PathName = new char[1024];
	m_PathName[0] = '\0';

	// 
	// m_PictureBox
	// 
	this->m_PictureBox = (new System::Windows::Forms::PictureBox());
	this->m_PictureBox->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
	this->m_PictureBox->Location = System::Drawing::Point(x + 19, y + 20);
	this->m_PictureBox->Name = S"m_PictureBox";
	this->m_PictureBox->Size = System::Drawing::Size(112, 73);
	this->m_PictureBox->TabIndex = 0;
	this->m_PictureBox->TabStop = false;
	this->m_PictureBox->Paint += new System::Windows::Forms::PaintEventHandler(this, &TextureControl::OnPictureBoxPaint);
	this->m_PictureBox->Click += new System::EventHandler(this, &TextureControl::OnPictureBoxClick);
	this->m_PictureBox->MouseHover += new System::EventHandler(this, &TextureControl::OnPictureBoxMouseHover);
	// 
	// m_AddButton
	// 
	this->m_AddButton = (new System::Windows::Forms::Button);
	this->m_AddButton->Location = System::Drawing::Point(x + 19, y + 105);
	this->m_AddButton->Name = S"m_AddButton";
	this->m_AddButton->Size = System::Drawing::Size(55, 23);
	this->m_AddButton->TabIndex = 1;
	this->m_AddButton->Text = S"Add";
	this->m_AddButton->UseVisualStyleBackColor = true;
	this->m_AddButton->Click += new System::EventHandler(this, &TextureControl::OnAddButtonClick);
	// 
	// m_DeleteButton
	// 
	this->m_DeleteButton = (new System::Windows::Forms::Button);
	this->m_DeleteButton->Location = System::Drawing::Point(x + 80, y + 105);
	this->m_DeleteButton->Name = S"m_DeleteButton";
	this->m_DeleteButton->Size = System::Drawing::Size(55, 23);
	this->m_DeleteButton->TabIndex = 2;
	this->m_DeleteButton->Text = S"Delete";
	this->m_DeleteButton->UseVisualStyleBackColor = true;
	this->m_DeleteButton->Click += new System::EventHandler(this, &TextureControl::OnDeleteButtonClick);
	//
	//m_ToolTip
	//
	this->m_ToolTip = (new System::Windows::Forms::ToolTip);
	this->m_ToolTip->InitialDelay = 200;
	this->m_ToolTip->ReshowDelay = 100;
	this->m_ToolTip->AutoPopDelay = 5000;
	this->m_ToolTip->ShowAlways = true;
	//
	// m_Bitmap
	//
	this->m_Bitmap = NULL;
}

System::Void TextureControl::OnPictureBoxPaint(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e)
{
	IntPtr hDC = e->Graphics->GetHdc();
	TerrainPluginHelper::DrawImage((HDC)hDC.ToInt32(), 0, 0, m_PictureBox->Width, m_PictureBox->Height, m_Bitmap); 
	e->Graphics->ReleaseHdc(hDC);

	if( m_ParentDlg->ActiveTextureControl == this )
	{
		Pen* RedPen = new Pen(Color::FromArgb(255, 255, 0, 0), 4);
		e->Graphics->DrawRectangle(RedPen, 4, 4, m_PictureBox->Width - 10, m_PictureBox->Height - 10);
	}
}

System::Void TextureControl::OnPictureBoxClick(System::Object*  sender, System::EventArgs*  e)
{
	if( !m_Bitmap )
		return;

	if( m_ParentDlg->ActiveTextureControl )
		m_ParentDlg->ActiveTextureControl->PictureBox->Invalidate();

	m_ParentDlg->ActiveTextureControl = this;
	m_PictureBox->Invalidate();

	TexturePropertyDialog::Get()->UpdateTextureProperty();

	TerrainEditDialog::Get()->SetInteractionMode("Paint Material");
}

System::Void TextureControl::OnPictureBoxMouseHover(System::Object*  sender, System::EventArgs*  e)
{
	if( !m_PathName )
		return;

	this->m_ToolTip->RemoveAll();
	this->m_ToolTip->Show(m_PathName, this->m_PictureBox, 1000 * 20);
}

void TextureControl::SetTexture(const char* PathName)
{
	if( m_Bitmap )
		TerrainPluginHelper::UnloadImage(m_Bitmap);

	if( PathName )
	{
		char AbsPathName[1024];
		NiSprintf(AbsPathName, 1024, "%s\\%s", TerrainHelper::GetClientPath(), PathName);
		m_Bitmap = TerrainPluginHelper::LoadImage(AbsPathName);
		NiStrcpy(m_PathName, 1024, PathName);
		m_AddButton->Text = "Change";
	}
	else
	{
		m_Bitmap = NULL;
		m_PathName[0] = '\0';
		m_AddButton->Text = "Add";

		if( m_ParentDlg->ActiveTextureControl == this )
			m_ParentDlg->ActiveTextureControl = NULL;
	}

	m_PictureBox->Invalidate();
	m_AddButton->Invalidate();
}

System::Void TextureControl::OnAddButtonClick(System::Object*  sender, System::EventArgs*  e)
{
	char AbsPathName[1024];
	if( TerrainPluginHelper::GetLoadPathName(AbsPathName, TEXTURE_IMAGE_FILTER) )
	{
		//转到相对路径
		char PathName[1024];
		if( !TerrainHelper::ConvertToRelative(PathName, 1024, AbsPathName) )
			return;

		//检查是否重复的贴图
		if( m_ParentDlg->IsDuplicatedTexture(PathName, this) )
			return;

		if( CMap::Get() )
		{
			CMap::Get()->ChangeTexture(m_PathName, PathName);
		}

		SetTexture(PathName);
		OnPictureBoxClick(sender, e);
	}
}

System::Void TextureControl::OnDeleteButtonClick(System::Object*  sender, System::EventArgs*  e)
{
	if( CMap::Get() )
	{
		CMap::Get()->ChangeTexture(m_PathName, NULL);
	}

	SetTexture(NULL);
}

void TextureControl::GetTextureSize(unsigned int& x, unsigned int& y)
{
	NIASSERT(m_Bitmap);

	x = FreeImage_GetWidth(m_Bitmap);
	y = FreeImage_GetHeight(m_Bitmap);
}

//===========================================================================================================
//PaintMaterialDialog
//

void PaintMaterialDialog::UpdateTextureControls()
{
	if( CMap::Get() )
	{
		NiTObjectArray<NiFixedString> akTextureNames;
		CMap::Get()->GetCurrentTextureNames(akTextureNames);

		for( int i = 0; i < m_TextureControls->Count; i++ )
		{
			if( unsigned int(i) < akTextureNames.GetSize() )
			{
				m_TextureControls[i]->SetTexture(akTextureNames.GetAt(i));
			}
			else
			{
				m_TextureControls[i]->SetTexture(NULL);
			}
		}
	}
}

bool PaintMaterialDialog::IsDuplicatedTexture(const char* PathName, TextureControl* pkTextureControl)
{
	NiFilename kPathName(PathName);

	for( int i = 0; i < m_TextureControls->Count; i++ )
	{
		if( pkTextureControl == m_TextureControls[i] )
			continue;

		NiFilename kTexturePathName(m_TextureControls[i]->PathName);

		if( NiStricmp( kPathName.GetFilename(), kTexturePathName.GetFilename() ) == 0 )
			return true;
	}

	return false;
}

void PaintMaterialDialog::ChangeBrush(int iBrushID)
{
	MAssert( iBrushID >= 0 && iBrushID < NUM_MATERIAL_BRUSH, "Invalid brush id" );

	char PathName[1024];
	if( TerrainPluginHelper::GetLoadPathName(PathName, BRUSH_IMAGE_FILTER) )
	{
		m_BrushControls[iBrushID]->LoadBrush(PathName);
		TerrainEditDialog::Get()->SetInteractionMode("Paint Material");

		// Register brush control settings.
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		String* pmString = String::Format("Brush {0}", __box(iBrushID));
		String* pmFilename = new String(PathName);
		pmSettingsService->SetSettingsObject(pmString, pmFilename, SettingsCategory::PerUser);
	}
}

void PaintMaterialDialog::RegisterSettings()
{
	// Register brush control settings.
	ISettingsService* pmSettingsService = MGetService(ISettingsService);
	MAssert(pmSettingsService != NULL, "Settings service not found!");

	for( int iBrushID = 0; iBrushID < NUM_MATERIAL_BRUSH; iBrushID++ )
	{
		String* pmString = String::Format("Brush {0}", __box(iBrushID));
		String* pmFilename = new String("");
		pmSettingsService->RegisterSettingsObject(pmString, pmFilename, SettingsCategory::PerUser);
		pmSettingsService->SetChangedSettingHandler(pmString, SettingsCategory::PerUser, new SettingChangedHandler(this, &PaintMaterialDialog::OnSettingChanged));

		//Init value
		pmFilename = dynamic_cast<String*> (pmSettingsService->GetSettingsObject(pmString, SettingsCategory::PerUser));
		const char* pcFilename = MStringToCharPointer(pmFilename);
		m_BrushControls[iBrushID]->LoadBrush(pcFilename);
		MFreeCharPointer(pcFilename);
	}
}

void PaintMaterialDialog::OnSettingChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	ISettingsService* pmSettingsService = MGetService(ISettingsService);
	MAssert(pmSettingsService != NULL, "Settings service not found!");

	for( int iBrushID = 0; iBrushID < NUM_MATERIAL_BRUSH; iBrushID++ )
	{
		String* pmString = String::Format("Brush {0}", __box(iBrushID));
		if( pmEventArgs->Name->Equals(pmString) )
		{
			String* pmFilename = dynamic_cast<String*>(pmSettingsService->GetSettingsObject(pmString, SettingsCategory::PerUser));
			MAssert(pmFilename != NULL, "Settings value is empty!");

			const char* pcFilename = MStringToCharPointer(pmFilename);
			m_BrushControls[iBrushID]->LoadBrush(pcFilename);
			MFreeCharPointer(pcFilename);

			break;
		}
	}
}
