
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#define NUM_MATERIAL_BRUSH 8
#define BRUSH_IMAGE_FILTER "Imgage file (*.JPG;*.PNG;*.BMP;*.GIF;*.DDS;*.TGA)|*.JPG;*.PNG;*.BMP;*.GIF;*.DDS;*.TGA|All files (*.*)|*.*"

#define NUM_TEXTURE 24
#define TEXTURE_IMAGE_FILTER "DDS file (*.DDS)|*.DDS"

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
    namespace TerrainPlugin {

	public __gc class PaintMaterialDialog;
	public __gc class BrushControl : public MDisposable
	{
	private:
		System::Windows::Forms::PictureBox* m_PictureBox;
		System::Windows::Forms::Button* m_ChangeButton;
		System::Windows::Forms::ToolTip* m_ToolTip;
		FIBITMAP* m_Bitmap; //原始的Bitmap
		FIBITMAP* m_DisplayBitmap; //经过缩放后用来显示的Bitmap
		PaintMaterialDialog* m_ParentDlg;
		NiFixedString* m_BrushName;
		NiTexture* m_pkTexture;
		int m_iID;

	public:
		BrushControl()
		{
			m_Bitmap = NULL;
			m_DisplayBitmap = NULL;
			m_ParentDlg = NULL;
			m_BrushName = NULL;
			m_pkTexture = NULL;
			m_iID = 0;
		}

		__property System::Windows::Forms::PictureBox* get_PictureBox()
		{
			return m_PictureBox;
		}

		__property System::Windows::Forms::Button* get_ChangeButton()
		{
			return m_ChangeButton;
		}

		__property FIBITMAP* get_Bitmap()
		{
			return m_Bitmap;
		}

		__property NiTexture* get_Texture()
		{
			if( m_pkTexture == NULL )
			{
				if( !NiRenderer::GetRenderer() )
					return NULL;

				TerrainHelper::ResetCurrentPath();

				m_pkTexture = NiSourceTexture::Create(*m_BrushName);
				m_pkTexture->IncRefCount();
			}

			return m_pkTexture;
		}

		void Init(PaintMaterialDialog* dlg, int x, int y, int id);
		void LoadBrush(const char* TextureName);

		System::Void OnPictureBoxPaint(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e);
		System::Void OnPictureBoxClick(System::Object*  sender, System::EventArgs*  e);
		System::Void OnPictureBoxMouseHover(System::Object*  sender, System::EventArgs*  e);
		System::Void OnChangeButtonClick(System::Object*  sender, System::EventArgs*  e);

		virtual void Do_Dispose(bool bDisposing)
		{
			if( m_pkTexture )
			{
				m_pkTexture->DecRefCount();
				m_pkTexture = NULL;
			}

			if( m_BrushName )
			{
				NiDelete m_BrushName;
				m_BrushName = NULL;
			}
		}
	};

	public __gc class TextureControl : public MDisposable
	{
	private:
		System::Windows::Forms::PictureBox* m_PictureBox;
		System::Windows::Forms::Button* m_AddButton;
		System::Windows::Forms::Button* m_DeleteButton;
		System::Windows::Forms::ToolTip* m_ToolTip;

		FIBITMAP* m_Bitmap;
		PaintMaterialDialog* m_ParentDlg;
		char* m_PathName;

	public:
		__property System::Windows::Forms::PictureBox* get_PictureBox()
		{
			return m_PictureBox;
		}

		__property System::Windows::Forms::Button* get_AddButton()
		{
			return m_AddButton;
		}

		__property System::Windows::Forms::Button* get_DeleteButton()
		{
			return m_DeleteButton;
		}

		__property FIBITMAP* get_Bitmap()
		{
			return m_Bitmap;
		}

		__property const char* get_PathName()
		{
			return m_PathName;
		}


		void Init(PaintMaterialDialog* dlg, int x, int y);
		void SetTexture(const char* PathName);

		System::Void OnPictureBoxPaint(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e);
		System::Void OnPictureBoxClick(System::Object*  sender, System::EventArgs*  e);		
		System::Void OnPictureBoxMouseHover(System::Object*  sender, System::EventArgs*  e);
		System::Void OnAddButtonClick(System::Object*  sender, System::EventArgs*  e);
		System::Void OnDeleteButtonClick(System::Object*  sender, System::EventArgs*  e);

		void GetTextureSize(unsigned int& x, unsigned int& y);

		virtual void Do_Dispose(bool bDisposing)
		{
		}
	};

    public __gc class PaintMaterialDialog : public System::Windows::Forms::Form
    {
    public: 
        PaintMaterialDialog(void)
        {
			ms_PaintMaterialDialog = this;
            InitializeComponent();			
        }

		__property BrushControl* get_ActiveBrushControl()
		{
			return m_ActiveBrushControl;
		}

		__property void set_ActiveBrushControl(BrushControl* pBrushControl)
		{
			m_ActiveBrushControl = pBrushControl;
		}

		__property TextureControl* get_ActiveTextureControl()
		{
			return m_ActiveTextureControl;
		}

		__property void set_ActiveTextureControl(TextureControl* pTextureControl)
		{
			m_ActiveTextureControl = pTextureControl;
		}

		void UpdateTextureControls();
		bool IsDuplicatedTexture(const char* PathName, TextureControl* pkTextureControl);
		void ChangeBrush(int iBrushID);

		void RegisterSettings();
		void OnSettingChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs);

		static PaintMaterialDialog* Get() { return ms_PaintMaterialDialog; }

		void Do_Dispose(bool bDisposing)
		{
			ms_PaintMaterialDialog = NULL;

			for( int i = 0; i < m_BrushControls->Count; i++ )
			{
				delete m_BrushControls[i];
			}
			m_BrushControls->Clear();

			m_TextureControls->Clear();
		}

    protected:
        void Dispose(Boolean disposing)
        {
			if (disposing && components)
			{
				components->Dispose();
			}
			__super::Dispose(disposing);
        }



	private: static PaintMaterialDialog* ms_PaintMaterialDialog;
	private: System::Windows::Forms::Panel*  m_Panel;

    private: System::Windows::Forms::GroupBox*  m_BrushGroupBox;
	private: BrushControl* m_ActiveBrushControl;
	private: BrushControl* m_BrushControls[];

	private: System::Windows::Forms::GroupBox* m_TextureGroupBox;
	private: TextureControl* m_ActiveTextureControl;
	private: TextureControl* m_TextureControls[];


    private:
        System::ComponentModel::Container* components;
        void InitializeComponent(void)
        {
			this->m_Panel = (new System::Windows::Forms::Panel());
			this->m_BrushGroupBox = (new System::Windows::Forms::GroupBox());
			this->m_ActiveBrushControl = NULL;
			this->m_BrushControls = new BrushControl*[NUM_MATERIAL_BRUSH];
			for( int i = 0; i < m_BrushControls->Count; i++ )
			{
				this->m_BrushControls[i] = new BrushControl();
				this->m_BrushControls[i]->Init(this, 0, i*120, i);

				this->m_BrushGroupBox->Controls->Add(this->m_BrushControls[i]->PictureBox);
				this->m_BrushGroupBox->Controls->Add(this->m_BrushControls[i]->ChangeButton);
			}

			this->m_TextureGroupBox = (new System::Windows::Forms::GroupBox());
			this->ActiveTextureControl = NULL;
			this->m_TextureControls = new TextureControl*[NUM_TEXTURE];
			for( int i = 0; i < m_TextureControls->Count; i++ )
			{
				this->m_TextureControls[i] = new TextureControl();
				this->m_TextureControls[i]->Init(this, 0, i*120);

				this->m_TextureGroupBox->Controls->Add(this->m_TextureControls[i]->PictureBox);
				this->m_TextureGroupBox->Controls->Add(this->m_TextureControls[i]->AddButton);
				this->m_TextureGroupBox->Controls->Add(this->m_TextureControls[i]->DeleteButton);
			}

			// 
			// m_Panel
			// 
			this->m_Panel->AutoScroll = true;
			this->m_Panel->Controls->Add(this->m_BrushGroupBox);
			this->m_Panel->Controls->Add(this->m_TextureGroupBox);
			this->m_Panel->Location = System::Drawing::Point(3, 3);
			this->m_Panel->Name = S"m_Panel";
			this->m_Panel->Size = System::Drawing::Size(345, 601);
			this->m_Panel->TabIndex = 2;
			// 
			// m_BrushGroupBox
			// 
			this->m_BrushGroupBox->Location = System::Drawing::Point(7, 14);
			this->m_BrushGroupBox->Name = S"m_BrushGroupBox";
			this->m_BrushGroupBox->Size = System::Drawing::Size(151, NUM_MATERIAL_BRUSH*120 + 50);
			this->m_BrushGroupBox->TabIndex = 0;
			this->m_BrushGroupBox->TabStop = false;
			this->m_BrushGroupBox->Text = S"Material Brush";
			// 
			// m_TextureGroupBox
			// 
			this->m_TextureGroupBox->Location = System::Drawing::Point(170, 14);
			this->m_TextureGroupBox->Name = S"m_TextureGroupBox";
			this->m_TextureGroupBox->Size = System::Drawing::Size(151, NUM_TEXTURE*120 + 50);
			this->m_TextureGroupBox->TabIndex = 1;
			this->m_TextureGroupBox->TabStop = false;
			this->m_TextureGroupBox->Text = S"Terrain Texture";
			// 
			// PaintMaterialDialog
			// 
			this->AutoScaleBaseSize = System::Drawing::Size(6, 14);
			this->ClientSize = System::Drawing::Size(352, 608);
			this->ControlBox = false;
			this->Controls->Add(this->m_Panel);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->Name = S"PaintMaterialDialog";
			this->ShowInTaskbar = false;
			this->Text = S"Paint Material";
		}
};
} }}}