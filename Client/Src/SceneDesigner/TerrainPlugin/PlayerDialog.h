
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
	namespace TerrainPlugin 
{
		public __gc class PlayerDialog : public System::Windows::Forms::Form
		{
		private: System::Windows::Forms::Button*  m_ChangeFileNameButton;

		private: System::Windows::Forms::Label*  m_FileNameLable;
		private: System::Windows::Forms::Label*  m_SpeedLable;
		private: System::Windows::Forms::TrackBar*  m_SpeedTrackBar;
		private: System::Windows::Forms::TextBox*  m_SpeedTextBox;
		private: System::Windows::Forms::CheckBox*  m_WalkCheckBox;
		private: System::Windows::Forms::ComboBox*  m_SequenceComboBox;
		private: System::Windows::Forms::Label*  m_CustomLabel;
		private: System::Windows::Forms::TrackBar*  m_BaseSpeedTrackBar;


		private: System::Windows::Forms::Label*  m_BaseSpeedLabel;
		private: System::Windows::Forms::TextBox*  m_BaseSpeedTextBox;
		private: System::Windows::Forms::CheckBox*  m_FlyCheckBox;
		private: System::Windows::Forms::TextBox*  m_PlayerSize;

		private: System::Windows::Forms::Label*  label1;
		private: System::Windows::Forms::Button*  button1;

		private: System::Windows::Forms::TextBox*  m_FileNameTextBox;

		public: 
			PlayerDialog(void)
			{
				InitializeComponent();
			}

			void Do_Dispose(bool bDisposing)
			{
			}

		protected:
			void Dispose(Boolean disposing)
			{
				__super::Dispose(disposing);
			}

		private:
			void InitializeComponent(void)
			{
				this->m_FileNameTextBox = (new System::Windows::Forms::TextBox());
				this->m_ChangeFileNameButton = (new System::Windows::Forms::Button());
				this->m_FileNameLable = (new System::Windows::Forms::Label());
				this->m_SpeedLable = (new System::Windows::Forms::Label());
				this->m_SpeedTrackBar = (new System::Windows::Forms::TrackBar());
				this->m_SpeedTextBox = (new System::Windows::Forms::TextBox());
				this->m_WalkCheckBox = (new System::Windows::Forms::CheckBox());
				this->m_SequenceComboBox = (new System::Windows::Forms::ComboBox());
				this->m_CustomLabel = (new System::Windows::Forms::Label());
				this->m_BaseSpeedTrackBar = (new System::Windows::Forms::TrackBar());
				this->m_BaseSpeedLabel = (new System::Windows::Forms::Label());
				this->m_BaseSpeedTextBox = (new System::Windows::Forms::TextBox());
				this->m_FlyCheckBox = (new System::Windows::Forms::CheckBox());
				this->m_PlayerSize = (new System::Windows::Forms::TextBox());
				this->label1 = (new System::Windows::Forms::Label());
				this->button1 = (new System::Windows::Forms::Button());
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_SpeedTrackBar))->BeginInit();
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_BaseSpeedTrackBar))->BeginInit();
				this->SuspendLayout();
				// 
				// m_FileNameTextBox
				// 
				this->m_FileNameTextBox->Location = System::Drawing::Point(12, 34);
				this->m_FileNameTextBox->Name = S"m_FileNameTextBox";
				this->m_FileNameTextBox->ReadOnly = true;
				this->m_FileNameTextBox->Size = System::Drawing::Size(334, 21);
				this->m_FileNameTextBox->TabIndex = 0;
				// 
				// m_ChangeFileNameButton
				// 
				this->m_ChangeFileNameButton->Location = System::Drawing::Point(352, 33);
				this->m_ChangeFileNameButton->Name = S"m_ChangeFileNameButton";
				this->m_ChangeFileNameButton->Size = System::Drawing::Size(40, 23);
				this->m_ChangeFileNameButton->TabIndex = 1;
				this->m_ChangeFileNameButton->Text = S"...";
				this->m_ChangeFileNameButton->UseVisualStyleBackColor = true;
				this->m_ChangeFileNameButton->Click += new System::EventHandler(this, &PlayerDialog::OnChangeFileName);
				// 
				// m_FileNameLable
				// 
				this->m_FileNameLable->AutoSize = true;
				this->m_FileNameLable->Location = System::Drawing::Point(12, 9);
				this->m_FileNameLable->Name = S"m_FileNameLable";
				this->m_FileNameLable->Size = System::Drawing::Size(41, 12);
				this->m_FileNameLable->TabIndex = 2;
				this->m_FileNameLable->Text = S"文件名";
				// 
				// m_SpeedLable
				// 
				this->m_SpeedLable->AutoSize = true;
				this->m_SpeedLable->Location = System::Drawing::Point(12, 68);
				this->m_SpeedLable->Name = S"m_SpeedLable";
				this->m_SpeedLable->Size = System::Drawing::Size(83, 12);
				this->m_SpeedLable->TabIndex = 3;
				this->m_SpeedLable->Text = S"加成后的速度:";
				// 
				// m_SpeedTrackBar
				// 
				this->m_SpeedTrackBar->LargeChange = 10;
				this->m_SpeedTrackBar->Location = System::Drawing::Point(7, 80);
				this->m_SpeedTrackBar->Maximum = 200;
				this->m_SpeedTrackBar->Name = S"m_SpeedTrackBar";
				this->m_SpeedTrackBar->Size = System::Drawing::Size(334, 42);
				this->m_SpeedTrackBar->TabIndex = 1;
				this->m_SpeedTrackBar->TickFrequency = 10;
				this->m_SpeedTrackBar->Value = 80;
				this->m_SpeedTrackBar->ValueChanged += new System::EventHandler(this, &PlayerDialog::OnSpeedChanged);
				// 
				// m_SpeedTextBox
				// 
				this->m_SpeedTextBox->Location = System::Drawing::Point(347, 82);
				this->m_SpeedTextBox->Name = S"m_SpeedTextBox";
				this->m_SpeedTextBox->ReadOnly = true;
				this->m_SpeedTextBox->Size = System::Drawing::Size(40, 21);
				this->m_SpeedTextBox->TabIndex = 4;
				this->m_SpeedTextBox->Text = S"8.0";
				// 
				// m_WalkCheckBox
				// 
				this->m_WalkCheckBox->AutoSize = true;
				this->m_WalkCheckBox->Location = System::Drawing::Point(14, 247);
				this->m_WalkCheckBox->Name = S"m_WalkCheckBox";
				this->m_WalkCheckBox->Size = System::Drawing::Size(48, 16);
				this->m_WalkCheckBox->TabIndex = 5;
				this->m_WalkCheckBox->Text = S"行走";
				this->m_WalkCheckBox->UseVisualStyleBackColor = true;
				this->m_WalkCheckBox->CheckedChanged += new System::EventHandler(this, &PlayerDialog::WalkCheckBoxChanged);
				// 
				// m_SequenceComboBox
				// 
				this->m_SequenceComboBox->DropDownHeight = 400;
				this->m_SequenceComboBox->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
				this->m_SequenceComboBox->FormattingEnabled = true;
				this->m_SequenceComboBox->IntegralHeight = false;
				this->m_SequenceComboBox->Location = System::Drawing::Point(12, 299);
				this->m_SequenceComboBox->Name = S"m_SequenceComboBox";
				this->m_SequenceComboBox->Size = System::Drawing::Size(121, 20);
				this->m_SequenceComboBox->Sorted = true;
				this->m_SequenceComboBox->TabIndex = 6;
				this->m_SequenceComboBox->SelectedValueChanged += new System::EventHandler(this, &PlayerDialog::OnSelectedSequenceChanged);
				// 
				// m_CustomLabel
				// 
				this->m_CustomLabel->AutoSize = true;
				this->m_CustomLabel->Location = System::Drawing::Point(10, 275);
				this->m_CustomLabel->Name = S"m_CustomLabel";
				this->m_CustomLabel->Size = System::Drawing::Size(65, 12);
				this->m_CustomLabel->TabIndex = 7;
				this->m_CustomLabel->Text = S"自定义动作";
				// 
				// m_BaseSpeedTrackBar
				// 
				this->m_BaseSpeedTrackBar->LargeChange = 10;
				this->m_BaseSpeedTrackBar->Location = System::Drawing::Point(6, 135);
				this->m_BaseSpeedTrackBar->Maximum = 200;
				this->m_BaseSpeedTrackBar->Name = S"m_BaseSpeedTrackBar";
				this->m_BaseSpeedTrackBar->Size = System::Drawing::Size(334, 42);
				this->m_BaseSpeedTrackBar->TabIndex = 8;
				this->m_BaseSpeedTrackBar->TickFrequency = 10;
				this->m_BaseSpeedTrackBar->Value = 80;
				this->m_BaseSpeedTrackBar->ValueChanged += new System::EventHandler(this, &PlayerDialog::OnBaseSpeedValueChanged);
				// 
				// m_BaseSpeedLabel
				// 
				this->m_BaseSpeedLabel->AutoSize = true;
				this->m_BaseSpeedLabel->Location = System::Drawing::Point(10, 124);
				this->m_BaseSpeedLabel->Name = S"m_BaseSpeedLabel";
				this->m_BaseSpeedLabel->Size = System::Drawing::Size(59, 12);
				this->m_BaseSpeedLabel->TabIndex = 9;
				this->m_BaseSpeedLabel->Text = S"基本速度:";
				// 
				// m_BaseSpeedTextBox
				// 
				this->m_BaseSpeedTextBox->Location = System::Drawing::Point(346, 135);
				this->m_BaseSpeedTextBox->Name = S"m_BaseSpeedTextBox";
				this->m_BaseSpeedTextBox->ReadOnly = true;
				this->m_BaseSpeedTextBox->Size = System::Drawing::Size(40, 21);
				this->m_BaseSpeedTextBox->TabIndex = 10;
				this->m_BaseSpeedTextBox->Text = S"8.0";
				// 
				// m_FlyCheckBox
				// 
				this->m_FlyCheckBox->AutoSize = true;
				this->m_FlyCheckBox->Location = System::Drawing::Point(68, 248);
				this->m_FlyCheckBox->Name = S"m_FlyCheckBox";
				this->m_FlyCheckBox->Size = System::Drawing::Size(48, 16);
				this->m_FlyCheckBox->TabIndex = 11;
				this->m_FlyCheckBox->Text = S"飞行";
				this->m_FlyCheckBox->UseVisualStyleBackColor = true;
				this->m_FlyCheckBox->CheckedChanged += new System::EventHandler(this, &PlayerDialog::FlyCheckBoxChanged);
				// 
				// m_PlayerSize
				// 
				this->m_PlayerSize->Location = System::Drawing::Point(16, 195);
				this->m_PlayerSize->Name = S"m_PlayerSize";
				this->m_PlayerSize->Size = System::Drawing::Size(100, 21);
				this->m_PlayerSize->TabIndex = 12;
				// 
				// label1
				// 
				this->label1->AutoSize = true;
				this->label1->Location = System::Drawing::Point(16, 180);
				this->label1->Name = S"label1";
				this->label1->Size = System::Drawing::Size(59, 12);
				this->label1->TabIndex = 13;
				this->label1->Text = S"人物大小:";
				// 
				// button1
				// 
				this->button1->Location = System::Drawing::Point(122, 195);
				this->button1->Name = S"button1";
				this->button1->Size = System::Drawing::Size(39, 23);
				this->button1->TabIndex = 14;
				this->button1->Text = S"设置";
				this->button1->UseVisualStyleBackColor = true;
				this->button1->Click += new System::EventHandler(this, &PlayerDialog::button1_Click);
				// 
				// PlayerDialog
				// 
				this->ClientSize = System::Drawing::Size(404, 351);
				this->Controls->Add(this->button1);
				this->Controls->Add(this->label1);
				this->Controls->Add(this->m_PlayerSize);
				this->Controls->Add(this->m_FlyCheckBox);
				this->Controls->Add(this->m_BaseSpeedTextBox);
				this->Controls->Add(this->m_BaseSpeedLabel);
				this->Controls->Add(this->m_BaseSpeedTrackBar);
				this->Controls->Add(this->m_CustomLabel);
				this->Controls->Add(this->m_SequenceComboBox);
				this->Controls->Add(this->m_WalkCheckBox);
				this->Controls->Add(this->m_SpeedTextBox);
				this->Controls->Add(this->m_SpeedTrackBar);
				this->Controls->Add(this->m_SpeedLable);
				this->Controls->Add(this->m_FileNameLable);
				this->Controls->Add(this->m_ChangeFileNameButton);
				this->Controls->Add(this->m_FileNameTextBox);
				this->Name = S"PlayerDialog";
				this->Text = S"Player Edit";
				this->VisibleChanged += new System::EventHandler(this, &PlayerDialog::OnVisibleChanged);
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_SpeedTrackBar))->EndInit();
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_BaseSpeedTrackBar))->EndInit();
				this->ResumeLayout(false);
				this->PerformLayout();

			}

		void UpdateSequenceNames()
		{
			m_SequenceComboBox->Items->Clear();
			m_SequenceComboBox->Items->Add(new String("(None)"));
			if(g_pkPlayer == NULL)
				return;

			const NiTPrimitiveArray<NiControllerSequence*>& kSequences = g_pkPlayer->GetSequences();
			for(unsigned int ui = 0; ui < kSequences.GetSize(); ++ui)
			{
				m_SequenceComboBox->Items->Add(new String((const char*)kSequences.GetAt(ui)->GetName()));
			}
		}

private: System::Void OnChangeFileName(System::Object*  sender, System::EventArgs*  e) 
		 {
			 if(g_pkPlayer == NULL)
				 return;

			 char acPathName[1024];
			 if( TerrainPluginHelper::GetLoadPathName(acPathName, "Kfm file (*.KFM)|*.KFM") )
			 {
				 m_FileNameTextBox->Text = acPathName;
				 g_pkPlayer->SetKfmFilePath(acPathName);

				 g_pkPlayer->Update(NULL, MFramework::Instance->TimeManager->CurrentTime, NULL, MFramework::Instance->ExternalAssetManager);
				 UpdateSequenceNames();
			 }
		 }

private: System::Void OnSpeedChanged(System::Object*  sender, System::EventArgs*  e) 
		 {
			 if(g_pkPlayer == NULL)
				 return;

			 g_pkPlayer->SetSpeed(float(m_SpeedTrackBar->Value)/10.f);
			 m_SpeedTextBox->Text = String::Format("{0:#0.#}", __box(g_pkPlayer->GetSpeed()));
		 }

private: System::Void OnVisibleChanged(System::Object*  sender, System::EventArgs*  e) 
		 {
			 if(g_pkPlayer == NULL)
				 return;

			 m_FileNameTextBox->Text = g_pkPlayer->GetKfmFilePath();
			 m_SpeedTrackBar->Value = (int)(g_pkPlayer->GetSpeed()*10.f);
			 m_SpeedTextBox->Text = String::Format("{0:#0.#}", __box(g_pkPlayer->GetSpeed()));
			 m_BaseSpeedTrackBar->Value = (int)(g_pkPlayer->GetBaseSpeed()*10.f);
			 m_BaseSpeedTextBox->Text = String::Format("{0:#0.#}", __box(g_pkPlayer->GetBaseSpeed()));
			 m_WalkCheckBox->Checked = g_pkPlayer->GetWalkMode();

			 UpdateSequenceNames();
		 }

private: System::Void WalkCheckBoxChanged(System::Object*  sender, System::EventArgs*  e) 
		 {
			 if(g_pkPlayer != NULL)
				 g_pkPlayer->SetWalkMode(m_WalkCheckBox->Checked);
		 }

private: System::Void OnSelectedSequenceChanged(System::Object*  sender, System::EventArgs*  e) 
		 {
			 if(g_pkPlayer == NULL)
				 return;

			 const char* pcString = MStringToCharPointer(m_SequenceComboBox->SelectedItem->ToString());
			 g_pkPlayer->SetCustomSequence(pcString);
			 MFreeCharPointer(pcString);
		 }

private: System::Void OnBaseSpeedValueChanged(System::Object*  sender, System::EventArgs*  e) 
		 {
			 if(g_pkPlayer == NULL)
				 return;

			 g_pkPlayer->SetBaseSpeed(float(m_BaseSpeedTrackBar->Value)/10.f);
			 m_BaseSpeedTextBox->Text = String::Format("{0:#0.#}", __box(g_pkPlayer->GetBaseSpeed()));

			 m_SpeedTrackBar->Value = m_BaseSpeedTrackBar->Value;
		 }
private: System::Void FlyCheckBoxChanged(System::Object*  sender, System::EventArgs*  e) 
		 {
			 if(g_pkPlayer != NULL)
				 g_pkPlayer->SetFly(m_FlyCheckBox->Checked);
		 }
private: System::Void button1_Click(System::Object*  sender, System::EventArgs*  e) {
			 float fValue = (float)System::Convert::ToDouble(m_PlayerSize->Text);
			 //g_pkPlayer->SetScale( fValue );
		 }
};
} }}}