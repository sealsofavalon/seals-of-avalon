// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27514
// http://www.emergentgametech.com

#include "TerrainPluginPCH.h"

#if _MSC_VER == 1310
#pragma unmanaged
#include <windows.h>
#include <_vcclrit.h>
#pragma managed
#endif

#include "Plugin.h"

#include "TerrainController.h"
#include "Interaction/AdjustHeightInteraction.h"
#include "Interaction/SmoothHeightInteraction.h"
#include "Interaction/IncHeightInteraction.h"
#include "Interaction/DecHeightInteraction.h"
#include "Interaction/FlattenHeightInteraction.h"
#include "Interaction/SetHeightInteraction.h"
#include "Interaction/TurnEdgeInteraction.h"
#include "Interaction/ExcavateHoleInteraction.h"
#include "Interaction/PaintMaterialInteraction.h"
#include "Interaction/RemoveMaterialInteraction.h"
#include "Interaction/PaintWaterInteraction.h"
#include "Interaction/FlattenWaterInteraction.h"
#include "Interaction/AdjustWaterInteraction.h"
#include "Interaction/PaintTerrainInfoInteraction.h"
#include "Interaction/NoiseInteraction.h"
#include "Interaction/WayPointCreateInteraction.h"
#include "Interaction/PaintAreaInteraction.h"
#include "Interaction/PathNodeCreateInteraction.h"
#include "Interaction/PaintGrassInteraction.h"
#include "Interaction/PaintFlyGridInfoInteraction.h"

#include "SpeedTree/SPTComponent.h"

#include "TypeEditor/MSPTFilenameEditor.h"
#include "TypeEditor/MDayNightColorEditor.h"
#include "TypeEditor/MEntityTypeConverter.h"
#include "TypeEditor/MSoundFilenameEditor.h"

#include "NPCComponent.h"
#include "PlayerDialog.h"


using namespace System;
using namespace System::IO;
using namespace System::Drawing;
using namespace System::Reflection;
using namespace System::Windows::Forms;
using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;

//---------------------------------------------------------------------------
Plugin::Plugin(void)
{
}

//---------------------------------------------------------------------------
Plugin::~Plugin(void)
{
}

//---------------------------------------------------------------------------
void Plugin::Do_Dispose(bool bDisposing)
{
    if (bDisposing)
    {
		m_pmTerrainEditDialog->Do_Dispose(bDisposing);
		m_pmPaintMaterialDialog->Do_Dispose(bDisposing);
		m_pmCalcDialog->Do_Dispose(bDisposing);
		m_pmTexturePropertyDialog->Do_Dispose(bDisposing);
		m_pmSkyEditDialog->Do_Dispose(bDisposing);
		m_pmDayNightControl->Do_Dispose(bDisposing);
		m_pmWaterDialog->Do_Dispose(bDisposing);
		m_pmAreaDialog->Do_Dispose(bDisposing);
        m_pmGrassDialog->Do_Dispose(bDisposing);

		CMapStreaming::_SDMShutdown();
		CMap::_SDMShutdown();
    }
}

//---------------------------------------------------------------------------
String* Plugin::get_Name()
{
    return "Terrain Edit Plug-in";
}

//---------------------------------------------------------------------------
System::Version* Plugin::get_Version()
{
    return Assembly::GetCallingAssembly()->GetName()->Version;
}

//---------------------------------------------------------------------------
System::Version* Plugin::get_ExpectedVersion()
{
    return new System::Version(2, 2);
}

//---------------------------------------------------------------------------
void Plugin::Load(int iToolMajorVersion, int iToolMinorVersion)
{
}

//---------------------------------------------------------------------------
IService* Plugin::GetProvidedServices()[]
{
    return new IService*[];
}

//---------------------------------------------------------------------------
void Plugin::Start()
{
	CMap::_SDMInit();
	CMapStreaming::_SDMInit();

	AddPropertyTypes();
    //AddToolBarButtons();
    SetupController();
    RegisterComponents();
    AddCustomOption();
	AddInteractionModes();
	RegisterCommandPanes();
	RegisterSettings();
}

void Plugin::AddPropertyTypes()
{
	IPropertyTypeService* pmPropertyTypeService = MGetService(IPropertyTypeService);
	MAssert(pmPropertyTypeService != NULL, "Service not found!");
	
	pmPropertyTypeService->RegisterType(new PropertyType("SPT Filename", NiEntityPropertyInterface::PT_STRING, __typeof(String), __typeof(MSPTFilenameEditor), NULL));
	pmPropertyTypeService->RegisterType(new PropertyType("Day Night Color", NiEntityPropertyInterface::PT_STRING, __typeof(String), __typeof(MDayNightColorEditor), __typeof(MDayNightColorEditorConverter)));
    pmPropertyTypeService->RegisterType(new PropertyType("Sound Filename", NiEntityPropertyInterface::PT_STRING, __typeof(String), __typeof(MSoundFilenameEditor), NULL));

	pmPropertyTypeService->RegisterType(new PropertyType("Entity Type", NiEntityPropertyInterface::PT_STRING, __typeof(String), NULL, __typeof(MEntityTypeConverter)), false);
}

//---------------------------------------------------------------------------
void Plugin::AddToolBarButtons()
{
}

//---------------------------------------------------------------------------
void Plugin::SetupController()
{
	TerrainController* pmController = new TerrainController();
    pmController->Init();
}

//---------------------------------------------------------------------------
void Plugin::RegisterComponents()
{
	// Get component service.
    IComponentService* pmComponentService = MGetService(IComponentService);
    MAssert(pmComponentService != NULL, "Component service not found!");

    // Get component factory.
    MComponentFactory* pmFactory = MFramework::Instance->ComponentFactory;
    MAssert(pmFactory != NULL, "Component factory not found!");

    // Register standard component types.
    pmComponentService->RegisterComponent(pmFactory->Get(NiNew CTerrainLightComponent()));
	pmComponentService->RegisterComponent(pmFactory->Get(NiNew CSPTComponent()));
	pmComponentService->RegisterComponent(pmFactory->Get(NiNew CNpcComponent()));
}

//---------------------------------------------------------------------------
void Plugin::AddCustomOption()
{
}

void Plugin::RegisterCommandPanes()
{
	ICommandPanelService* pmCommandPanelService = MGetService(ICommandPanelService);

	m_pmTerrainEditDialog = new TerrainEditDialog();
	pmCommandPanelService->RegisterPanel(m_pmTerrainEditDialog);

	m_pmPaintMaterialDialog = new PaintMaterialDialog();
    pmCommandPanelService->RegisterPanel(m_pmPaintMaterialDialog);

	m_pmCalcDialog = new CalcDialog();
	pmCommandPanelService->RegisterPanel(m_pmCalcDialog);

	m_pmTexturePropertyDialog = new TexturePropertyDialog();
	pmCommandPanelService->RegisterPanel(m_pmTexturePropertyDialog);

	m_pmSkyEditDialog = new SkyEditDialog();
	pmCommandPanelService->RegisterPanel(m_pmSkyEditDialog);

	m_pmDayNightControl = new DayNightControl();
	pmCommandPanelService->RegisterPanel(m_pmDayNightControl);

	pmCommandPanelService->RegisterPanel(new PlayerDialog);

	m_pmWaterDialog = new WaterDialog;
	pmCommandPanelService->RegisterPanel(m_pmWaterDialog);

	m_pmAreaDialog = new AreaDialog;
	pmCommandPanelService->RegisterPanel(m_pmAreaDialog);

    m_pmGrassDialog = new GrassDialog;
    pmCommandPanelService->RegisterPanel(m_pmGrassDialog);
}

void Plugin::AddInteractionModes()
{
	// Add interaction modes.
    IInteractionModeService* pmInteractionModeService = MGetService(IInteractionModeService);
    MAssert(pmInteractionModeService != NULL, "Service not found!");

	IInteractionMode* pmInteractionMode;

    pmInteractionMode = new MAdjustHeightInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MSmoothHeightInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MIncHeightInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MDecHeightInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MFlattenHeightInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MSetHeightInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MNoiseInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);


	pmInteractionMode = new MTurnEdgeInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MExcavateHoleInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MPaintMaterialInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MRemoveMaterialInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MPaintWaterInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MFlattenWaterInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MAdjustWaterInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);
	
	pmInteractionMode = new MPaintTerrainInfoInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MWayPointCreateInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MPaintAreaInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);

    pmInteractionMode = new MPathNodeCreateInteractionMode();
    pmInteractionModeService->AddInteractionMode(pmInteractionMode);

    pmInteractionMode = new MPaintGrassInteractionMode();
    pmInteractionModeService->AddInteractionMode(pmInteractionMode);

	pmInteractionMode = new MPaintFlyGridInfoInteractionMode();
	pmInteractionModeService->AddInteractionMode(pmInteractionMode);
}

void Plugin::RegisterSettings()
{
	TileEditDialog::RegisterSettings();
	TerrainController::RegisterSettings();

	if( PaintMaterialDialog::Get() )
		PaintMaterialDialog::Get()->RegisterSettings();
}

//---------------------------------------------------------------------------

//The following is related to a bug in VC7.1/.Net CLR 1.1
// see http://support.microsoft.com/?id=814472

void Plugin::InitStatics()
{
#if _MSC_VER == 1310
    __crt_dll_initialize();
#endif
}
//---------------------------------------------------------------------------
void Plugin::ShutdownStatics()
{
#if _MSC_VER == 1310
    //__crt_dll_terminate();
#endif
}
//---------------------------------------------------------------------------
