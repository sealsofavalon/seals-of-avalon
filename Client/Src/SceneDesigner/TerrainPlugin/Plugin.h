// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27514
// http://www.emergentgametech.com

#pragma once
using namespace System;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;
#include "PaintMaterialDialog.h"
#include "TerrainEditDialog.h"
#include "CalcDialog.h"
#include "TextureProperty.h"
#include "SkyEdit.h"
#include "DayNightControl.h"
#include "WaterDialog.h"
#include "AreaDialog.h"
#include "GrassDialog.h"

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
    namespace TerrainPlugin 
{

    public __gc class Plugin : public MDisposable, public IPlugin
    {
    private:
		TerrainEditDialog* m_pmTerrainEditDialog;
		PaintMaterialDialog* m_pmPaintMaterialDialog;
		CalcDialog* m_pmCalcDialog;
		TexturePropertyDialog* m_pmTexturePropertyDialog;
		SkyEditDialog* m_pmSkyEditDialog;
		DayNightControl* m_pmDayNightControl;
		WaterDialog* m_pmWaterDialog;
		AreaDialog* m_pmAreaDialog;
        GrassDialog* m_pmGrassDialog;
        
    public:
        Plugin();
        ~Plugin();

        //IPlugin implementation
        __property String* get_Name();
        __property System::Version* get_Version();
        __property System::Version* get_ExpectedVersion();
        void Load(int iToolMajorVersion, int iToolMinorVersion);
        IService* GetProvidedServices()[];
        void Start();

        [DllInit]
        static void InitStatics();

        [DllShutdown]
        static void ShutdownStatics();

    private:
		void AddPropertyTypes();
        void AddToolBarButtons();
        void SetupController();
        void RegisterComponents();
        void AddCustomOption();
		void RegisterCommandPanes();
		void AddInteractionModes();
		void RegisterSettings();

    // MDisposable member
    protected:
        virtual void Do_Dispose(bool bDisposing);
    };

}}}}
