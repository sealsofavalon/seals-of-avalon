#include "TerrainPluginPCH.h"
#include "SkyEdit.h"
#include "TerrainPluginHelper.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;


System::Void SkyEditDialog::OnChangeSky(System::Object*  sender, System::EventArgs*  e)
{
	char AbsPathName[1024];
	char PathName[1024];
	if( TerrainPluginHelper::GetLoadPathName(AbsPathName, "Nif File(*.nif)|*.nif") 
	 && TerrainHelper::ConvertToRelative(PathName, sizeof(PathName), AbsPathName))
	{
		CMap* pkMap = CMap::Get();
		if( pkMap )
		{
			pkMap->ChangeSky(PathName);
            UpdateCurrentSky();
		}
	}
}

System::Void SkyEditDialog::OnClick(System::Object*  sender, System::EventArgs*  e)
{
    UpdateCurrentSky();
}

System::Void SkyEditDialog::OnVisibleChanged(System::Object*  sender, System::EventArgs*  e)
{
    UpdateCurrentSky();
}

System::Void SkyEditDialog::OnCheckedChanged(System::Object*  sender, System::EventArgs*  e)
{
    CMap* pkMap = CMap::Get();
    if( pkMap )
    {
        pkMap->GetSky()->SetEnableZBuffer(this->m_EnableZBuffer->Checked);
    }
}

void SkyEditDialog::UpdateCurrentSky()
{
    CMap* pkMap = CMap::Get();
    if( pkMap )
    {
        this->m_EnableZBuffer->Checked = pkMap->GetSky()->GetEnableZBuffer();
        m_CurrentSky->Text = (const char*)pkMap->GetSky()->GetFilename();
    }
}
