
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
	namespace TerrainPlugin 
{
		public __gc class SkyEditDialog : public System::Windows::Forms::Form
		{
		public: 
			SkyEditDialog(void)
			{
				InitializeComponent();
			}

			void Do_Dispose(bool bDisposing)
			{
			}

		protected:
			void Dispose(Boolean disposing)
			{
				__super::Dispose(disposing);
			}
        private: System::Windows::Forms::TextBox*  m_CurrentSky;
        private: System::Windows::Forms::CheckBox*  m_EnableZBuffer;
        protected: 

        protected: 

		private: System::Windows::Forms::Button*  m_ChangeSkyBtn;
		private:
			void InitializeComponent(void)
			{
                this->m_ChangeSkyBtn = (new System::Windows::Forms::Button());
                this->m_CurrentSky = (new System::Windows::Forms::TextBox());
                this->m_EnableZBuffer = (new System::Windows::Forms::CheckBox());
                this->SuspendLayout();
                // 
                // m_ChangeSkyBtn
                // 
                this->m_ChangeSkyBtn->Location = System::Drawing::Point(235, 27);
                this->m_ChangeSkyBtn->Name = S"m_ChangeSkyBtn";
                this->m_ChangeSkyBtn->Size = System::Drawing::Size(65, 23);
                this->m_ChangeSkyBtn->TabIndex = 0;
                this->m_ChangeSkyBtn->Text = S"Change";
                this->m_ChangeSkyBtn->UseVisualStyleBackColor = true;
                this->m_ChangeSkyBtn->Click += new System::EventHandler(this, &SkyEditDialog::OnChangeSky);
                // 
                // m_CurrentSky
                // 
                this->m_CurrentSky->Location = System::Drawing::Point(12, 29);
                this->m_CurrentSky->Name = S"m_CurrentSky";
                this->m_CurrentSky->ReadOnly = true;
                this->m_CurrentSky->Size = System::Drawing::Size(217, 21);
                this->m_CurrentSky->TabIndex = 1;
                // 
                // m_EnableZBuffer
                // 
                this->m_EnableZBuffer->AutoSize = true;
                this->m_EnableZBuffer->Location = System::Drawing::Point(12, 68);
                this->m_EnableZBuffer->Name = S"m_EnableZBuffer";
                this->m_EnableZBuffer->Size = System::Drawing::Size(102, 16);
                this->m_EnableZBuffer->TabIndex = 2;
                this->m_EnableZBuffer->Text = S"���� Z Buffer";
                this->m_EnableZBuffer->UseVisualStyleBackColor = true;
                this->m_EnableZBuffer->CheckedChanged += new System::EventHandler(this, &SkyEditDialog::OnCheckedChanged);
                // 
                // SkyEditDialog
                // 
                this->ClientSize = System::Drawing::Size(312, 114);
                this->Controls->Add(this->m_EnableZBuffer);
                this->Controls->Add(this->m_CurrentSky);
                this->Controls->Add(this->m_ChangeSkyBtn);
                this->Name = S"SkyEditDialog";
                this->Text = S"Sky Edit";
                this->VisibleChanged += new System::EventHandler(this, &SkyEditDialog::OnVisibleChanged);
                this->Click += new System::EventHandler(this, &SkyEditDialog::OnClick);
                this->ResumeLayout(false);
                this->PerformLayout();

            }
		private: System::Void OnChangeSky(System::Object*  sender, System::EventArgs*  e);
        private: System::Void OnClick(System::Object*  sender, System::EventArgs*  e);
        private: System::Void OnVisibleChanged(System::Object*  sender, System::EventArgs*  e);
        private: System::Void OnCheckedChanged(System::Object*  sender, System::EventArgs*  e);

        void UpdateCurrentSky();
        };
} }}}