
#include "TerrainPluginPCH.h"
#include "TextureManager.h"
#include "TerrainController.h"
#include "Interaction/TerrainEditInteraction.h"
#include "Interaction/WayPointCreateInteraction.h"
#include "Interaction/PathNodeCreateInteraction.h"
#include "NPCComponent.h"
#include "PaintMaterialDialog.h"
#include "WaterDialog.h"
#include "GrassDialog.h"



#include <list>
#using <mscorlib.dll>

using namespace System::Windows::Forms;

using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

using namespace std;

//---------------------------------------------------------------------------
TerrainController::TerrainController()
{
	__hook(&MEventManager::EntityAddedToScene, MFramework::Instance->EventManager, &TerrainController::OnEntityAddedToScene);
    __hook(&MEventManager::EntityRemovedFromScene, MFramework::Instance->EventManager, &TerrainController::OnEntityRemovedFromScene);
    __hook(&MEventManager::EntityPropertyChanged, MFramework::Instance->EventManager, &TerrainController::OnEntityPropertyChanged);
	__hook(&MEventManager::NewSceneLoaded, MFramework::Instance->EventManager, &TerrainController::OnNewSceneLoaded);
}

//---------------------------------------------------------------------------
void TerrainController::Do_Dispose(bool bDisposing)
{
    if (bDisposing)
    {
        // For some reason, __unhook causes bogus compilation errors here.
        // Thus, some events are unhooked manually.
        MFramework::Instance->EventManager->remove_EntityAddedToScene(new
            MEventManager::__Delegate_EntityAddedToScene(this,
            &TerrainController::OnEntityAddedToScene));
        MFramework::Instance->EventManager->remove_EntityRemovedFromScene(new
            MEventManager::__Delegate_EntityRemovedFromScene(this,
            &TerrainController::OnEntityRemovedFromScene));
        __unhook(&MEventManager::EntityPropertyChanged,
            MFramework::Instance->EventManager, &TerrainController::OnEntityPropertyChanged);
    }
}

//---------------------------------------------------------------------------
void TerrainController::Init()
{
    IUICommandService* pmUICommandService = MGetService(IUICommandService);
    pmUICommandService->BindCommands(this);
}

void TerrainController::OnToggleCameraMode(System::Object* pmSender, EventArgs* pmArgs)
{
	LoadPlayer();
	if( g_pkPlayer == NULL )
		return;

	if( g_pkPlayer->IsFreeCameraMode() )
	{
		g_pkPlayer->SetCameraMode(PLAYER_MODE);

		NiCamera* pkCamera = MFramework::Instance->ViewportManager->ActiveViewport->GetNiCamera(); 
		NiPoint3 kPos = pkCamera->GetTranslate() + pkCamera->GetWorldDirection()*g_pkPlayer->GetCameraDistance();
		g_pkPlayer->SetPosition(kPos, NiPoint3(0.f, 0.f, 0.f));
	}
	else
	{
		g_pkPlayer->SetCameraMode(FREE_MODE);
	}
}

void TerrainController::OnValidateCameraMode(System::Object* pmSender, UIState* pmState)
{
	if( CMap::Get() )
		pmState->Enabled = true;
	else
		pmState->Enabled = false;
}

void TerrainController::OnToggleWayPointInteractionMode(System::Object* pmSender, EventArgs* pmArgs)
{
	IInteractionModeService* pmInteractionModeService = MGetService(IInteractionModeService);
	MWayPointCreateInteractionMode* pmWayPointCreateInteractionMode = 
		dynamic_cast<MWayPointCreateInteractionMode*>(pmInteractionModeService->GetInteractionModeByName("Create Way Point"));

	if( pmWayPointCreateInteractionMode )
		pmWayPointCreateInteractionMode->SetInteractionMode(pmSender, pmArgs);
}

void TerrainController::OnValidateWayPointInteractionMode(System::Object* pmSender, UIState* pmState)
{
	if( ms_bEditNpc && CMap::Get() )
		pmState->Enabled = true;
	else
		pmState->Enabled = false;
}

void TerrainController::OnTogglePathNodeInteractionMode(System::Object* pmSender, EventArgs* pmArgs)
{
	IInteractionModeService* pmInteractionModeService = MGetService(IInteractionModeService);
	MPathNodeCreateInteractionMode* pmPathNodeCreateInteractionMode = 
		dynamic_cast<MPathNodeCreateInteractionMode*>(pmInteractionModeService->GetInteractionModeByName("Create Path Node"));

	if( pmPathNodeCreateInteractionMode )
		pmPathNodeCreateInteractionMode->SetInteractionMode(pmSender, pmArgs);
}

void TerrainController::OnValidatePathNodeInteractionMode(System::Object* pmSender, UIState* pmState)
{
	if( ms_bEditNpc && CMap::Get() )
		pmState->Enabled = true;
	else
		pmState->Enabled = false;
}

void TerrainController::OnLinkPathNode(System::Object* pmSender, EventArgs* pmArgs)
{
    NiTPrimitiveArray<CPathNode*> kPathNodes(8, 8);

    CPathNodeComponent* pkPathNodeComponent;
    NiEntityInterface* pkEntity;
	MEntity* amEntities[];
	amEntities = SelectionService->GetSelectedEntities();
	for (int i = 0; i < amEntities->Count; i++)
	{
		if (amEntities[i] != NULL)
		{
            pkEntity = amEntities[i]->GetNiEntityInterface();
            pkPathNodeComponent = (CPathNodeComponent*)pkEntity->GetComponentByTemplateID(CPathNodeComponent::ms_kTemplateID);
            if(pkPathNodeComponent == NULL)
                continue;

            kPathNodes.Add(pkPathNodeComponent->GetPathNode());
		}
	}

    if(kPathNodes.GetSize() > 1)
        g_pkPathNodeManager->LinkPathNodes(kPathNodes, false);
}

void TerrainController::OnValidateLinkPathNode(System::Object* pmSender, UIState* pmState)
{
    pmState->Enabled = (SelectionService->NumSelectedEntities > 1);
}

void TerrainController::OnUnlinkPathNode(System::Object* pmSender, EventArgs* pmArgs)
{
    NiTPrimitiveArray<CPathNode*> kPathNodes(8, 8);

    CPathNodeComponent* pkPathNodeComponent;
    NiEntityInterface* pkEntity;
	MEntity* amEntities[];
	amEntities = SelectionService->GetSelectedEntities();
	for (int i = 0; i < amEntities->Count; i++)
	{
		if (amEntities[i] != NULL)
		{
            pkEntity = amEntities[i]->GetNiEntityInterface();
            pkPathNodeComponent = (CPathNodeComponent*)pkEntity->GetComponentByTemplateID(CPathNodeComponent::ms_kTemplateID);
            if(pkPathNodeComponent == NULL)
                continue;

            kPathNodes.Add(pkPathNodeComponent->GetPathNode());
		}
	}

    if(kPathNodes.GetSize() > 1)
        g_pkPathNodeManager->UnlinkPathNodes(kPathNodes, false);
}

void TerrainController::OnValidateUnlinkPathNode(System::Object* pmSender, UIState* pmState)
{
    pmState->Enabled = (SelectionService->NumSelectedEntities > 1);
}


void TerrainController::DropToTerrainHandler(Object* pmObject, EventArgs* mArgs)
{
	if (SelectionService->NumSelectedEntities == 0)
		return;

	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	NiPoint3 kTranslation;
	NiFixedString kTranslationName = "Translation";

	MEntity* amEntities[];
	amEntities = SelectionService->GetSelectedEntities();
	for (int i = 0; i < amEntities->Count; i++)
	{
		if (amEntities[i] != NULL)
		{
			if( !amEntities[i]->GetNiEntityInterface()->GetPropertyData(kTranslationName, kTranslation) )
				continue;

			kTranslation.z = pkMap->GetHeight(kTranslation.x, kTranslation.y);
			amEntities[i]->SetPropertyData(kTranslationName, new MPoint3(kTranslation), true);
		}
	}
}

void TerrainController::DropToTerrainValidator(Object* pmSender, UIState* pmState)
{
	pmState->Enabled = (SelectionService->NumSelectedEntities > 0);
}

void TerrainController::DragToPlayerHandler(Object* pmObject, EventArgs* mArgs)
{
	if (SelectionService->NumSelectedEntities == 0)
		return;

	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	NiPoint3 kNewCenter;
	if( !g_pkPlayer || g_pkPlayer->IsFreeCameraMode() )
	{
		NiCamera* pkCamera = MFramework::Instance->ViewportManager->ActiveViewport->GetNiCamera(); 
		kNewCenter = pkCamera->GetTranslate() + pkCamera->GetWorldDirection()*8.f;
	}
	else
	{
		kNewCenter = g_pkPlayer->GetPosition();
	}

	NiPoint3 kOldCenter;
	SelectionService->SelectionCenter->ToNiPoint3(kOldCenter);
	NiPoint3 kDeltaTranslation = kNewCenter - kOldCenter;
	NiPoint3 kTranslation;
	NiFixedString kTranslationName = "Translation";

	MEntity* amEntities[];
	amEntities = SelectionService->GetSelectedEntities();
	for (int i = 0; i < amEntities->Count; i++)
	{
		if (amEntities[i] != NULL)
		{
			if( !amEntities[i]->GetNiEntityInterface()->GetPropertyData(kTranslationName, kTranslation) )
				continue;

			amEntities[i]->SetPropertyData(kTranslationName, new MPoint3(kTranslation + kDeltaTranslation), true);
		}
	}
}

void TerrainController::DragToPlayerValidator(Object* pmSender, UIState* pmState)
{
	pmState->Enabled = (SelectionService->NumSelectedEntities > 0);
}

void TerrainController::LoadPlayer()
{
	if( g_pkPlayer )
		return;

	CPlayer* pkPlayer = NiNew CPlayer;
	pkPlayer->Update(NULL, 0.f, NULL, MFramework::Instance->ExternalAssetManager);

	NiCamera* pkCamera = MFramework::Instance->ViewportManager->ActiveViewport->GetNiCamera();

	MEntity* pmProxyEntity = MFramework::Instance->EntityFactory->Get(pkPlayer);
	MFramework::Instance->ProxyManager->ProxyScene->AddEntity(pmProxyEntity, true);

	NiPoint3 kData = pkCamera->GetWorldLocation();
	kData += NiPoint3(1.f, 1.f, -1.f);
	pkPlayer->SetPropertyData("Translation", kData, 0);
	pkPlayer->Update(NULL, 0.f, NULL, MFramework::Instance->ExternalAssetManager);
}

void TerrainController::WireframeModeHandler(Object* pmObject, EventArgs* mArgs)
{
	CTerrainShader::ShowWireframe(!CTerrainShader::GetShowWireframe());
}

void TerrainController::WireframeModeValidator(Object* pmSender, UIState* pmState)
{
	if(CMap::Get())
		pmState->Enabled = true;
	else
		pmState->Enabled = false;

	pmState->Checked = CTerrainShader::GetShowWireframe();
}


void TerrainController::OnEntityAddedToScene(MScene* pmScene, MEntity* pmEntity)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	if( pmScene->GetNiScene() ==  pkMap->GetScene() )
		pkMap->OnEntityAdded((NiEntityInterface*)pmEntity->PropertyInterface);
}

void TerrainController::OnEntityRemovedFromScene(MScene* pmScene, MEntity* pmEntity)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	if( pmScene->GetNiScene() != pkMap->GetScene() )
		return;

	NiEntityInterface* pkEntity = pmEntity->GetNiEntityInterface();
	if(TerrainHelper::IsNpcEntity(pkEntity))
	{
		//删除NPC所有的路点
		CNpcComponent* pkNpcComponent = (CNpcComponent*)pkEntity->GetComponentByTemplateID(CNpcComponent::ms_kTemplateID);
		NiEntityInterface* pkWayPointEntity;
		while(pkNpcComponent->GetWayPointCount() > 0) //从场景删除路点时, WayPointCount 会减少
		{
			pkWayPointEntity = pkNpcComponent->GetWayPoint(pkNpcComponent->GetWayPointCount() - 1);
			MFramework::Instance->Scene->RemoveEntity(MFramework::Instance->EntityFactory->Get(pkWayPointEntity), true);
		}
	}

	pkMap->OnEntityRemoved(pkEntity);
}

void TerrainController::OnEntityPropertyChanged(MEntity* pmEntity, String* strPropertyName, unsigned int uiPropertyIndex, bool bInBatch)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	pkMap->OnEntityPropertyChanged((NiEntityInterface*)pmEntity->PropertyInterface);
}

void TerrainController::OnNewSceneLoaded(MScene* pmScene)
{
	CTextureManager::Get()->UnloadAllUnused();
	MFramework::Instance->ExternalAssetManager->RemoveAllUnusedAssets();

    WaterDialog::Get()->UpdateWaterControls();
    GrassDialog::Get()->UpdateGrassControls();
    PaintMaterialDialog::Get()->UpdateTextureControls();
}

//
void TerrainController::RegisterSettings()
{
	ISettingsService* pmSettingsService = MGetService(ISettingsService);
	MAssert(pmSettingsService != NULL, "Settings service not found!");

	pmSettingsService->RegisterSettingsObject("Show Grid", __box(ms_bShowGrid), SettingsCategory::PerUser);
	pmSettingsService->SetChangedSettingHandler("Show Grid", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::ShowGridSettingsChangedHandler));

    pmSettingsService->RegisterSettingsObject("Show Collision", __box(ms_bShowCollision), SettingsCategory::PerUser);
    pmSettingsService->SetChangedSettingHandler("Show Collision", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::ShowCollisionSettingsChangedHandler));

    pmSettingsService->RegisterSettingsObject("Show Collision Wireframe", __box(ms_bShowCollisionWireframe), SettingsCategory::PerUser);
	pmSettingsService->SetChangedSettingHandler("Show Collision Wireframe", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::ShowCollisionWireframeSettingsChangedHandler));

	pmSettingsService->RegisterSettingsObject("Edit Npc", __box(ms_bEditNpc), SettingsCategory::PerUser);
	pmSettingsService->SetChangedSettingHandler("Edit Npc", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::EditNpcSettingsChangedHandler));

    pmSettingsService->RegisterSettingsObject("Fog Enable", __box(ms_bFogEnable), SettingsCategory::PerUser);
    pmSettingsService->SetChangedSettingHandler("Fog Enable", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::FogEnableSettingsChanged));

    pmSettingsService->RegisterSettingsObject("Scene Object Visible", __box(ms_bSceneObjectVisible), SettingsCategory::PerUser);
    pmSettingsService->SetChangedSettingHandler("Scene Object Visible", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::SceneObjectVisibleSettingsChanged));

    pmSettingsService->RegisterSettingsObject("Scene Object Select", __box(ms_bSceneObjectSelect), SettingsCategory::PerUser);
    pmSettingsService->SetChangedSettingHandler("Scene Object Select", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::SceneObjectSelectSettingsChanged));

    pmSettingsService->RegisterSettingsObject("Npc Visible", __box(ms_bNpcVisible), SettingsCategory::PerUser);
    pmSettingsService->SetChangedSettingHandler("Npc Visible", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::NpcVisibleSettingsChanged));

    pmSettingsService->RegisterSettingsObject("Npc Select", __box(ms_bNpcSelect), SettingsCategory::PerUser);
    pmSettingsService->SetChangedSettingHandler("Npc Select", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::NpcSelectSettingsChanged));

    pmSettingsService->RegisterSettingsObject("Way Point Visible", __box(ms_bWayPointVisible), SettingsCategory::PerUser);
    pmSettingsService->SetChangedSettingHandler("Way Point Visible", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::WayPointVisibleSettingsChanged));

    pmSettingsService->RegisterSettingsObject("Way Point Select", __box(ms_bWayPointSelect), SettingsCategory::PerUser);
    pmSettingsService->SetChangedSettingHandler("Way Point Select", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::WayPointSelectSettingsChanged));

    pmSettingsService->RegisterSettingsObject("Path Node Visible", __box(ms_bPathNodeVisible), SettingsCategory::PerUser);
    pmSettingsService->SetChangedSettingHandler("Path Node Visible", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::PathNodeVisibleSettingsChanged));

    pmSettingsService->RegisterSettingsObject("Path Node Select", __box(ms_bPathNodeSelect), SettingsCategory::PerUser);
    pmSettingsService->SetChangedSettingHandler("Path Node Select", SettingsCategory::PerUser, new SettingChangedHandler(NULL, &TerrainController::PathNodeSelectSettingsChanged));


	IOptionsService* pmOptionsService = MGetService(IOptionsService);
	MAssert(pmOptionsService != NULL, "Optins service not found!");
	pmOptionsService->AddOption("选项.显示地面阻挡", SettingsCategory::PerUser, "Show Grid");
    pmOptionsService->AddOption("选项.显示碰撞盒", SettingsCategory::PerUser, "Show Collision");
    pmOptionsService->AddOption("选项.用线框显示", SettingsCategory::PerUser, "Show Collision Wireframe");
    pmOptionsService->AddOption("选项.雾效果", SettingsCategory::PerUser, "Fog Enable");

    pmOptionsService->AddOption("选项.可编辑NPC和PathNode", SettingsCategory::PerUser, "Edit Npc");

    pmOptionsService->AddOption("选项.场景模型.可见", SettingsCategory::PerUser, "Scene Object Visible");
    pmOptionsService->AddOption("选项.场景模型.可选择", SettingsCategory::PerUser, "Scene Object Select");

    pmOptionsService->AddOption("选项.NPC.可见", SettingsCategory::PerUser, "Npc Visible");
    pmOptionsService->AddOption("选项.NPC.可选择", SettingsCategory::PerUser, "Npc Select");

    pmOptionsService->AddOption("选项.Way Point.可见", SettingsCategory::PerUser, "Way Point Visible");
    pmOptionsService->AddOption("选项.Way Point.可选择", SettingsCategory::PerUser, "Way Point Select");

    pmOptionsService->AddOption("选项.PathNode.可见", SettingsCategory::PerUser, "Path Node Visible");
    pmOptionsService->AddOption("选项.PathNode.可选择", SettingsCategory::PerUser, "Path Node Select");


    ShowGridSettingsChangedHandler(NULL, new SettingChangedEventArgs("Show Grid", SettingsCategory::PerUser));
    ShowCollisionSettingsChangedHandler(NULL, new SettingChangedEventArgs("Show Collision", SettingsCategory::PerUser));
    ShowCollisionWireframeSettingsChangedHandler(NULL, new SettingChangedEventArgs("Show Collision Wireframe", SettingsCategory::PerUser));
	EditNpcSettingsChangedHandler(NULL, new SettingChangedEventArgs("Edit Npc", SettingsCategory::PerUser));
    FogEnableSettingsChanged(NULL, new SettingChangedEventArgs("Fog Enable", SettingsCategory::PerUser));

    SceneObjectVisibleSettingsChanged(NULL, new SettingChangedEventArgs("Scene Object Visible", SettingsCategory::PerUser));
    SceneObjectSelectSettingsChanged(NULL, new SettingChangedEventArgs("Scene Object Select", SettingsCategory::PerUser));
    NpcVisibleSettingsChanged(NULL, new SettingChangedEventArgs("Npc Visible", SettingsCategory::PerUser));
    NpcSelectSettingsChanged(NULL, new SettingChangedEventArgs("Npc Select", SettingsCategory::PerUser));
    WayPointVisibleSettingsChanged(NULL, new SettingChangedEventArgs("Way Point Visible", SettingsCategory::PerUser));
    WayPointSelectSettingsChanged(NULL, new SettingChangedEventArgs("Way Point Select", SettingsCategory::PerUser));
    PathNodeVisibleSettingsChanged(NULL, new SettingChangedEventArgs("Path Node Visible", SettingsCategory::PerUser));
    PathNodeSelectSettingsChanged(NULL, new SettingChangedEventArgs("Path Node Select", SettingsCategory::PerUser));
}

void TerrainController::ShowGridSettingsChangedHandler(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Show Grid") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmShowGrid = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Show Grid", SettingsCategory::PerUser));
		MAssert(pmShowGrid != NULL, "Settings value is empty!");

		CTerrainShader::ShowGridInfo(*pmShowGrid);
	}
}

void TerrainController::ShowCollisionSettingsChangedHandler(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
    if (pmEventArgs->Name->Equals("Show Collision") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmShowCollision = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Show Collision", SettingsCategory::PerUser));
		MAssert(pmShowCollision != NULL, "Settings value is empty!");

		CCollisionShader::SetShowCollision(*pmShowCollision);
	}
}

void TerrainController::ShowCollisionWireframeSettingsChangedHandler(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
    if (pmEventArgs->Name->Equals("Show Collision Wireframe") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmShowCollisionWireframe = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Show Collision Wireframe", SettingsCategory::PerUser));
		MAssert(pmShowCollisionWireframe != NULL, "Settings value is empty!");

		CCollisionShader::SetShowWireframe(*pmShowCollisionWireframe);
	}
}

void TerrainController::EditNpcSettingsChangedHandler(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Edit Npc") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmEditNpc = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Edit Npc", SettingsCategory::PerUser));
		MAssert(pmEditNpc != NULL, "Settings value is empty!");

		ms_bEditNpc = *pmEditNpc;
		CTile::SetEditNpc(ms_bEditNpc);
	}
}

void TerrainController::FogEnableSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Fog Enable") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmFogEnable = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Fog Enable", SettingsCategory::PerUser));
		MAssert(pmFogEnable != NULL, "Settings value is empty!");
        if(pmFogEnable == NULL)
            return;

		ms_bFogEnable = *pmFogEnable;
        CTerrainLightManager::SetFogEnable(ms_bFogEnable);
	}
}

void TerrainController::SceneObjectVisibleSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Scene Object Visible") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmValue = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Scene Object Visible", SettingsCategory::PerUser));
		MAssert(pmValue != NULL, "Settings value is empty!");
        if(pmValue == NULL)
            return;

        unsigned int uiVisibleMask = CMap::GetVisibleMask();
        if(*pmValue)
            uiVisibleMask |= NiGeneralEntity::SceneObject;
        else
            uiVisibleMask &= ~NiGeneralEntity::SceneObject;
        CMap::SetVisibleMask(uiVisibleMask);
	}
}

void TerrainController::SceneObjectSelectSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Scene Object Select") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmValue = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Scene Object Select", SettingsCategory::PerUser));
		MAssert(pmValue != NULL, "Settings value is empty!");
        if(pmValue == NULL)
            return;

        unsigned int uiSelectMask = CMap::GetSelectMask();
        if(*pmValue)
            uiSelectMask |= NiGeneralEntity::SceneObject;
        else
            uiSelectMask &= ~NiGeneralEntity::SceneObject;
        CMap::SetSelectMask(uiSelectMask);
	}
}

void TerrainController::NpcVisibleSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Npc Visible") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmValue = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Npc Visible", SettingsCategory::PerUser));
		MAssert(pmValue != NULL, "Settings value is empty!");
        if(pmValue == NULL)
            return;

        unsigned int uiVisibleMask = CMap::GetVisibleMask();
        if(*pmValue)
            uiVisibleMask |= NiGeneralEntity::Npc;
        else
            uiVisibleMask &= ~NiGeneralEntity::Npc;
        CMap::SetVisibleMask(uiVisibleMask);
	}
}

void TerrainController::NpcSelectSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Npc Select") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmValue = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Npc Select", SettingsCategory::PerUser));
		MAssert(pmValue != NULL, "Settings value is empty!");
        if(pmValue == NULL)
            return;

        unsigned int uiSelectMask = CMap::GetSelectMask();
        if(*pmValue)
            uiSelectMask |= NiGeneralEntity::Npc;
        else
            uiSelectMask &= ~NiGeneralEntity::Npc;
        CMap::SetSelectMask(uiSelectMask);
	}
}

void TerrainController::WayPointVisibleSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Way Point Visible") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmValue = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Way Point Visible", SettingsCategory::PerUser));
		MAssert(pmValue != NULL, "Settings value is empty!");
        if(pmValue == NULL)
            return;

        unsigned int uiVisibleMask = CMap::GetVisibleMask();
        if(*pmValue)
            uiVisibleMask |= NiGeneralEntity::WayPoint;
        else
            uiVisibleMask &= ~NiGeneralEntity::WayPoint;
        CMap::SetVisibleMask(uiVisibleMask);

	}
}

void TerrainController::WayPointSelectSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Way Point Select") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmValue = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Way Point Select", SettingsCategory::PerUser));
		MAssert(pmValue != NULL, "Settings value is empty!");
        if(pmValue == NULL)
            return;

        unsigned int uiSelectMask = CMap::GetSelectMask();
        if(*pmValue)
            uiSelectMask |= NiGeneralEntity::WayPoint;
        else
            uiSelectMask &= ~NiGeneralEntity::WayPoint;
        CMap::SetSelectMask(uiSelectMask);
	}
}

void TerrainController::PathNodeVisibleSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Path Node Visible") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmValue = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Path Node Visible", SettingsCategory::PerUser));
		MAssert(pmValue != NULL, "Settings value is empty!");
        if(pmValue == NULL)
            return;

        unsigned int uiVisibleMask = CMap::GetVisibleMask();
        if(*pmValue)
            uiVisibleMask |= NiGeneralEntity::PathNode;
        else
            uiVisibleMask &= ~NiGeneralEntity::PathNode;
        CMap::SetVisibleMask(uiVisibleMask);
	}
}

void TerrainController::PathNodeSelectSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Path Node Select") && pmEventArgs->Category == SettingsCategory::PerUser)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		__box bool* pmValue = dynamic_cast<__box bool*>(pmSettingsService->GetSettingsObject("Path Node Select", SettingsCategory::PerUser));
		MAssert(pmValue != NULL, "Settings value is empty!");
        if(pmValue == NULL)
            return;

        unsigned int uiSelectMask = CMap::GetSelectMask();
        if(*pmValue)
            uiSelectMask |= NiGeneralEntity::PathNode;
        else
            uiSelectMask &= ~NiGeneralEntity::PathNode;
        CMap::SetSelectMask(uiSelectMask);
	}
}

//---------------------------------------------------------------------------
ISelectionService* TerrainController::get_SelectionService()
{
    if (ms_pmSelectionService == NULL)
    {
        ms_pmSelectionService = MGetService(ISelectionService);
        MAssert(ms_pmSelectionService != NULL, "Selection service not "
            "found!");
    }
    return ms_pmSelectionService;
}