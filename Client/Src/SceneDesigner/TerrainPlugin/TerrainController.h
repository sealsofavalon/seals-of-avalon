#pragma once

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
    namespace TerrainPlugin
{
    using namespace System;
    using namespace System::Collections;
    using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;
    using namespace Emergent::Gamebryo::SceneDesigner::Framework;

	public __gc class TerrainController : public MDisposable
    {
    public:
        TerrainController();
        void Init();

	public:
		static void RegisterSettings();
		static void ShowGridSettingsChangedHandler(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
        static void ShowCollisionSettingsChangedHandler(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
        static void ShowCollisionWireframeSettingsChangedHandler(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
		static void EditNpcSettingsChangedHandler(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
        static void FogEnableSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs);

        static void SceneObjectVisibleSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
        static void SceneObjectSelectSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
        static void NpcVisibleSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
        static void NpcSelectSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
        static void WayPointVisibleSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
        static void WayPointSelectSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
        static void PathNodeVisibleSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
        static void PathNodeSelectSettingsChanged(Object* pmSender, SettingChangedEventArgs* pmEventArgs);

	private:
		static bool ms_bShowGrid = false;
		static bool ms_bEditNpc = false;
        static bool ms_bFogEnable = true;

        static bool ms_bSceneObjectVisible = true;
        static bool ms_bSceneObjectSelect = true;
        static bool ms_bNpcVisible = true;
        static bool ms_bNpcSelect = true;
        static bool ms_bWayPointVisible = true;
        static bool ms_bWayPointSelect = true;
        static bool ms_bPathNodeVisible = true;
        static bool ms_bPathNodeSelect = true;

        static bool ms_bShowCollision = false;
        static bool ms_bShowCollisionWireframe = true;

    private:
		[UICommandHandlerAttribute("CameraMode")]
        void OnToggleCameraMode(System::Object* pmSender, EventArgs* pmArgs);

        [UICommandValidatorAttribute("CameraMode")]
        void OnValidateCameraMode(System::Object* pmSender, UIState* pmState);

		[UICommandHandlerAttribute("WayPointInteractionMode")]
		void OnToggleWayPointInteractionMode(System::Object* pmSender, EventArgs* pmArgs);

		[UICommandValidatorAttribute("WayPointInteractionMode")]
		void OnValidateWayPointInteractionMode(System::Object* pmSender, UIState* pmState);

		[UICommandHandlerAttribute("DropToTerrain")]
		void DropToTerrainHandler(Object* pmObject, EventArgs* mArgs);
		[UICommandValidatorAttribute("DropToTerrain")]
		void DropToTerrainValidator(Object* pmSender, UIState* pmState);

		[UICommandHandlerAttribute("DragToPlayer")]
		void DragToPlayerHandler(Object* pmObject, EventArgs* mArgs);
		[UICommandValidatorAttribute("DragToPlayer")]
		void DragToPlayerValidator(Object* pmSender, UIState* pmState);

		[UICommandHandlerAttribute("WireframeMode")]
		void WireframeModeHandler(Object* pmObject, EventArgs* mArgs);
		[UICommandValidatorAttribute("WireframeMode")]
		void WireframeModeValidator(Object* pmSender, UIState* pmState);

        [UICommandHandlerAttribute("PathNodeInteractionMode")]
        void OnTogglePathNodeInteractionMode(System::Object* pmSender, EventArgs* pmArgs);

        [UICommandValidatorAttribute("PathNodeInteractionMode")]
        void OnValidatePathNodeInteractionMode(System::Object* pmSender, UIState* pmState);

        [UICommandHandlerAttribute("LinkPathNode")]
        void OnLinkPathNode(System::Object* pmSender, EventArgs* pmArgs);

        [UICommandValidatorAttribute("LinkPathNode")]
        void OnValidateLinkPathNode(System::Object* pmSender, UIState* pmState);

        [UICommandHandlerAttribute("UnlinkPathNode")]
        void OnUnlinkPathNode(System::Object* pmSender, EventArgs* pmArgs);

        [UICommandValidatorAttribute("UnlinkPathNode")]
        void OnValidateUnlinkPathNode(System::Object* pmSender, UIState* pmState);

		void OnEntityAddedToScene(MScene* pmScene, MEntity* pmEntity);
		void OnEntityRemovedFromScene(MScene* pmScene, MEntity* pmEntity);
		void OnEntityPropertyChanged(MEntity* pmEntity, String* strPropertyName, unsigned int uiPropertyIndex, bool bInBatch);
		void OnNewSceneLoaded(MScene* pmScene);

		void LoadPlayer();

		__property static ISelectionService* get_SelectionService();
		static ISelectionService* ms_pmSelectionService;

	// MDisposable members.
    protected:
        virtual void Do_Dispose(bool bDisposing);
    };

}}}}