
#include "TerrainPluginPCH.h"
#include "TerrainEditBrush.h"
#include "PaintMaterialDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;


void MPointInfo::Do_Dispose(bool bDisposing)
{
}

void MFilter::Do_Dispose(bool bDisposing)
{
}

void MFilter::Init(int iSize)
{
	if( iSize < 2 )
		iSize = 2;

	m_Values = new float __gc [iSize + 1];
	for( int i = 0; i <= iSize; i++ )
	{
		m_Values[i] = 1.f - float(i)/float(iSize);
	}
}

float MFilter::GetValue(float v)
{
	v = NiClamp(v, 0.f, 1.f);
	v *= float(m_Values->Length) - 1.f;

	float p0, p1, p2, p3;
	int i1 = (int)NiFloor(v);
	int i2 = i1 + 1;
	float t = v - float(i1);

	p1 = m_Values[i1];
	p2 = m_Values[i2];

	if( i1 == 0 )
		p0 = p1 + (p1 - p2);
	else
		p0 = m_Values[i1 - 1];

	if( i2 == m_Values->Length - 1 )
		p3 = p2 + (p2 - p1);
	else
		p3 = m_Values[i2 + 1];

	return NiClamp( TerrainHelper::CatmullRom(t, p0, p1, p2, p3), 0.f, 1.f );
}

void MTerrainEditBrush::Do_Dispose(bool bDisposing)
{
	NiDelete m_pkGeometry;
	m_pkGeometry = NULL;

	NiDelete m_pkCenterPoint;
	m_pkCenterPoint = NULL;
}

MTerrainEditBrush::MTerrainEditBrush()
{
	m_pmSelectedPoints = new ArrayList();
	m_pkCenterPoint = NiNew NiPoint3(0.f, 0.f, 0.f);
	
	m_pmFilter = new MFilter;
	m_pmFilter->Init(6);

	m_pkGeometry = NULL;

	m_bCenterGrid = false;
	m_fGridSize = UNIT_SIZE;

	m_bMaterialBrush = false;
	m_bWaterBrush = false;

	m_iWidth = 9;
	m_iHeight = 9;
}

MPointInfo* MTerrainEditBrush::GetSelectedPoints()[]
{
	return static_cast<MPointInfo*[]>(m_pmSelectedPoints->ToArray(__typeof(MPointInfo)));
}

void MTerrainEditBrush::Update(const NiPoint3& kPos)
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	m_pkCenterPoint->x = int(kPos.x / m_fGridSize) * m_fGridSize;
	m_pkCenterPoint->y = int(kPos.y / m_fGridSize) * m_fGridSize;
	if( m_bCenterGrid )
	{
		m_pkCenterPoint->x += m_fGridSize / 2;
		m_pkCenterPoint->y += m_fGridSize / 2;
	}

	m_pkCenterPoint->z = pkMap->GetHeight(m_pkCenterPoint->x, m_pkCenterPoint->y);

	UpdateSelectedPoints();
	UpdateBrush();
}

void MTerrainEditBrush::Clear()
{
	m_pmSelectedPoints->Clear();

	UpdateBrush();
}

void MTerrainEditBrush::UpdateSelectedPoints()
{
	m_pmSelectedPoints->Clear();

	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	// a point is in a circle if:
	// x^2 + y^2 <= r^2
	// a point is in an ellipse if:
	// (ax)^2 + (by)^2 <= 1
	// where a = 1/halfEllipseWidth and b = 1/halfEllipseHeight

	// for a soft-selected ellipse,
	// the factor is simply the filtered: ((ax)^2 + (by)^2)
	int iCenterX = (m_iWidth - 1)/2;
	int iCenterY = (m_iHeight - 1)/2;

	float a = 1 / (float(m_iWidth) * 0.5f);
	float b = 1 / (float(m_iHeight) * 0.5f);

	float h;

	for( int x = 0; x < m_iWidth; x++ )
	{
		for( int y = 0; y < m_iHeight; y++ )
		{
			float xp = float(iCenterX - x);
			float yp = float(iCenterY - y);

			float factor = a * a * xp * xp + b * b * yp * yp;
			if( factor > 1.f )
				continue;

			MPointInfo* pmInfo = new MPointInfo;

			pmInfo->m_fX = m_pkCenterPoint->x + (x - iCenterX)*m_fGridSize;
			pmInfo->m_fY = m_pkCenterPoint->y + (y - iCenterY)*m_fGridSize;

			//地形高度
			if( pkMap->GetHeight( pmInfo->m_fX, pmInfo->m_fY, h ) )
				pmInfo->m_fHeight = h;
			else
				pmInfo->m_fHeight = m_pkCenterPoint->z;

			//水面高度
			if( pkMap->GetWaterHeight( pmInfo->m_fX, pmInfo->m_fY, h ) )
			{
				pmInfo->m_bHasWater = true;
				pmInfo->m_fWaterHeight = h;
			}
			else
			{
				pmInfo->m_bHasWater = false;
				pmInfo->m_fWaterHeight = pmInfo->m_fHeight;
			}

			//权重
			pmInfo->m_fWeight = m_pmFilter->GetValue(factor);

			m_pmSelectedPoints->Add(pmInfo);
		}
	}
}

void MTerrainEditBrush::UpdateBrush()
{
	NiDelete m_pkGeometry;
	m_pkGeometry = NULL;

	if( NiRenderer::GetRenderer() == NULL )
		return;

	if( m_bMaterialBrush )
	{
		UpdateMaterialBrush();
	}
	else
	{
		UpdatePointBrush();
	}
}

void MTerrainEditBrush::UpdatePointBrush()
{
	if( m_pmSelectedPoints->Count == 0 )
		return;

	int iVertCount = m_pmSelectedPoints->Count*5;
	
	NiPoint3* pkVerts = NiNew NiPoint3[iVertCount];
	NiColorA* pkColors = NiNew NiColorA[iVertCount];
    NiBool* pbConnections = NiAlloc(NiBool, iVertCount);

	NiColorA color;
	color.a = 1.f;
	color.b = 0.f;

	float r;
	r = m_fGridSize / 4.f;

	NiPoint3 pos;
	MPointInfo* pmInfo;

	for( int i = 0; i < m_pmSelectedPoints->Count; i++ )
	{
		pmInfo = static_cast<MPointInfo*>(m_pmSelectedPoints->Item[i]);

		if( m_bWaterBrush )
		{
			pos = NiPoint3(pmInfo->m_fX, pmInfo->m_fY, pmInfo->m_fWaterHeight);
		}
		else
		{
			pos = NiPoint3(pmInfo->m_fX, pmInfo->m_fY, pmInfo->m_fHeight);
		}
		
		pkVerts[5*i    ] = pos + NiPoint3(-r, -r, 0.f);
		pkVerts[5*i + 1] = pos + NiPoint3( r, -r, 0.f);
		pkVerts[5*i + 2] = pos + NiPoint3( r,  r, 0.f);
		pkVerts[5*i + 3] = pos + NiPoint3(-r,  r, 0.f);
		pkVerts[5*i + 4] = pos + NiPoint3(-r, -r, 0.f);

		if( pmInfo->m_fWeight < 0.f || pmInfo->m_fWeight > 1.f )
		{
			color.r = 1.f;
			color.g = 0.f;
		}
		else
		{
			color.r = pmInfo->m_fWeight;
			color.g = 1.f - pmInfo->m_fWeight;
		}
		pkColors[5*i    ] = color;
		pkColors[5*i + 1] = color;
		pkColors[5*i + 2] = color;
		pkColors[5*i + 3] = color;
		pkColors[5*i + 4] = color;

		pbConnections[5*i    ] = true;
		pbConnections[5*i + 1] = true;
		pbConnections[5*i + 2] = true;
		pbConnections[5*i + 3] = true;
		pbConnections[5*i + 4] = false;
	}
	
	m_pkGeometry = NiNew NiLines(iVertCount, pkVerts, pkColors, NULL, 0, 
        NiGeometryData::NBT_METHOD_NONE, pbConnections);

	NiVertexColorProperty* pkVertexColorProp = NiNew NiVertexColorProperty();

	pkVertexColorProp->SetSourceMode(NiVertexColorProperty::SOURCE_EMISSIVE);
	pkVertexColorProp->SetLightingMode(NiVertexColorProperty::LIGHTING_E_A_D);

	m_pkGeometry->AttachProperty(pkVertexColorProp);

	m_pkGeometry->UpdateProperties();
	m_pkGeometry->Update(0.0f);
}

void MTerrainEditBrush::UpdateMaterialBrush()
{
	CMap* pkMap = CMap::Get();
	if( !pkMap )
		return;

	if( PaintMaterialDialog::Get()->ActiveBrushControl == NULL )
		return;

	NiTexture* pkTexture = PaintMaterialDialog::Get()->ActiveBrushControl->Texture;
	if( pkTexture == NULL )
		return;

	unsigned int uiWidth = pkTexture->GetWidth();
	unsigned int uiHeight = pkTexture->GetHeight();

	if( uiWidth > 128 || uiHeight > 128 )
		return;
	
	unsigned int uiVertCount = (uiWidth + 1) * (uiHeight + 1);


	NiPoint3* pkVerts = NiNew NiPoint3[uiVertCount];
	NiPoint2* pkTexCoords = NiNew NiPoint2[uiVertCount];

	float h;
	NiPoint3* pkV = pkVerts;
	NiPoint2* pkT = pkTexCoords; 
	for( unsigned int x = 0; x <= uiWidth; x++ )
	{
		for( unsigned int y  = 0; y <= uiHeight; y++ )
		{
			//顶点0
			pkV->x = m_pkCenterPoint->x + float(x) - float(uiWidth / 2);
			pkV->y = m_pkCenterPoint->y + float(y) - float(uiHeight / 2);
			if( pkMap->GetHeight( pkV->x, pkV->y, h ) )
			{
				pkV->z = h;
			}
			else
			{
				pkV->z = m_pkCenterPoint->z;
			}
			pkV++;

			pkT->x = (float(x) / float(uiWidth));
			pkT->y = (float(y) / float(uiHeight));
			pkT++;
		}
	}

	unsigned short* pusStripsLength = NiAlloc(unsigned short, 1);
	*pusStripsLength = uiWidth/2*((uiHeight+1)*4 + 4) - 2;
	unsigned short* pusStrips = NiAlloc(unsigned short, *pusStripsLength);
	
	//生成strip
	int k = 0;
	for( unsigned int i = 0; i < uiWidth; i += 2 )
	{
		if( i > 0 )
		{
			pusStrips[k++] = unsigned short( i * (uiWidth + 1) );
			pusStrips[k++] = unsigned short( i * (uiWidth + 1) );
		}

		for( unsigned int j = 0; j < (uiHeight + 1); j++ )
		{
			pusStrips[k++] = unsigned short(i * (uiWidth + 1) + j);
			pusStrips[k++] = unsigned short((i + 1) * (uiWidth + 1) + j);
		}

		pusStrips[k++] = unsigned short((i + 1) * (uiWidth + 1) + uiWidth);
		pusStrips[k++] = unsigned short((i + 1) * (uiWidth + 1) + uiWidth);

		for( unsigned int j = 0; j < (uiHeight + 1); j++ )
		{
			pusStrips[k++] = unsigned short((i + 2) * (uiWidth + 1) + uiWidth - j);
			pusStrips[k++] = unsigned short((i + 1) * (uiWidth + 1) + uiWidth - j);
		}
	}
	NIASSERT( *pusStripsLength == k );

	m_pkGeometry = NiNew NiTriStrips(uiVertCount, pkVerts, NULL, NULL, pkTexCoords, 1, NiGeometryData::NBT_METHOD_NONE, k - 2, 1, pusStripsLength, pusStrips);

	NiTexturingProperty* pkTexturingProp = NiNew NiTexturingProperty();
	pkTexturingProp->SetBaseTexture(pkTexture);
	pkTexturingProp->SetBaseTextureIndex(0);
	pkTexturingProp->SetApplyMode(NiTexturingProperty::APPLY_REPLACE);
	pkTexturingProp->SetBaseClampMode(NiTexturingProperty::CLAMP_S_CLAMP_T);
	m_pkGeometry->AttachProperty(pkTexturingProp);

	NiAlphaProperty* pkAlphaProperty = NiNew NiAlphaProperty;
	pkAlphaProperty->SetAlphaBlending(true);
	pkAlphaProperty->SetSrcBlendMode(NiAlphaProperty::ALPHA_SRCALPHA);
	pkAlphaProperty->SetDestBlendMode(NiAlphaProperty::ALPHA_INVSRCALPHA);
	m_pkGeometry->AttachProperty(pkAlphaProperty);

	//NiWireframeProperty* pkWireframeProperty = NiNew NiWireframeProperty;
	//pkWireframeProperty->SetWireframe(true);
	//m_pkGeometry->AttachProperty(pkWireframeProperty);

	m_pkGeometry->UpdateProperties();
	m_pkGeometry->Update(0.0f);
}

void MTerrainEditBrush::Render(MRenderingContext* pmRenderingContext)
{
	//显示地形刷
	if( m_pkGeometry )
	{
		NiEntityRenderingContext* pkContext = pmRenderingContext->GetRenderingContext();
		NiDrawScene(pkContext->m_pkCamera, m_pkGeometry, *pkContext->m_pkCullingProcess);
	}
}

void MTerrainEditBrush::GetSize(int& x, int& y)
{
	x = m_iWidth;
	y = m_iHeight;
}

void MTerrainEditBrush::SetSize(int x, int y)
{
	m_iWidth = NiClamp(x, 1, 49);
	m_iHeight = NiClamp(y, 1, 49);

	Update(*m_pkCenterPoint);
}

const NiPoint3& MTerrainEditBrush::GetCenterPoint()
{
	return *m_pkCenterPoint;
}