#pragma once

using namespace System::Collections;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{

	public __gc class MPointInfo : public MDisposable
	{
	public:
		float m_fX;
		float m_fY;

		float m_fHeight;
		float m_fStartHeight;

		float m_fWaterHeight;
		float m_fStartWaterHeight;
		bool  m_bHasWater;

		float m_fWeight;//Ȩ��

	protected:
        virtual void Do_Dispose(bool bDisposing);
	};

	public __gc class MFilter : public MDisposable
    {
		float m_Values __gc [];

	public:
		void Init(int iSize);
		float GetValue(float v);

	protected:
        virtual void Do_Dispose(bool bDisposing);
    };

	public __gc class MTerrainEditBrush : public MDisposable
	{
	protected:
        virtual void Do_Dispose(bool bDisposing);

	public:
		MTerrainEditBrush();
		
		MPointInfo* GetSelectedPoints()[];

		void Clear();
		void Update(const NiPoint3& kPos);
		void UpdateSelectedPoints();
		void UpdateBrush();
		void UpdatePointBrush();
		void UpdateMaterialBrush();
		void Render(MRenderingContext* pmRenderingContext);

		void GetSize(int& x, int& y);
		void SetSize(int x, int y);
		

		const NiPoint3& GetCenterPoint();

		void SetGridSize(float fSize) { m_fGridSize = fSize; }

		void SetCenterGrid( bool bCenterGrid ) { m_bCenterGrid = bCenterGrid; }
		void SetWaterBrush( bool bWaterBrush ) { m_bWaterBrush = bWaterBrush; }
		void SetMaterialBrush( bool bMaterialBrush ) { m_bMaterialBrush = bMaterialBrush; } 

	private:
		ArrayList* m_pmSelectedPoints;
		NiPoint3* m_pkCenterPoint;
		MFilter* m_pmFilter;

		bool m_bCenterGrid;
		float m_fGridSize;

		bool m_bMaterialBrush;
		bool m_bWaterBrush;

		int m_iWidth;
		int m_iHeight;

		NiGeometry* m_pkGeometry;
	};

}}}}