
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "TileEditDialog.h"
#include "TerrainEditBrush.h"
#include "Interaction/TerrainEditInteraction.h"

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
    namespace TerrainPlugin {
    public __gc class TerrainEditDialog : public System::Windows::Forms::Form
    {
    public: 
        TerrainEditDialog(void)
        {
			m_pmTerrainEditBrush = new MTerrainEditBrush;
			ms_TerrainEditDialog = this;
			m_pmCurrentInteractionMode = NULL;

            InitializeComponent();

			m_btnBuidWaterEdgeMap->Hide( );
        }

		void SetInteractionMode(String* pmModeName)
		{
			if ( pmModeName->Equals("Paint Water") )
				m_btnBuidWaterEdgeMap->Show( );
			else
				m_btnBuidWaterEdgeMap->Hide( );

			IInteractionModeService* pmInteractionModeService = MGetService(IInteractionModeService);
			m_pmCurrentInteractionMode = dynamic_cast<MTerrainEditInteractionMode*>(pmInteractionModeService->GetInteractionModeByName(pmModeName));

			if( m_pmCurrentInteractionMode )
			{
				m_pmCurrentInteractionMode->MouseBrush = m_pmTerrainEditBrush;
				m_pmCurrentInteractionMode->ParamDlg = this;
			}
			
			pmInteractionModeService->ActiveMode = m_pmCurrentInteractionMode;
		}

		__property System::Windows::Forms::GroupBox* get_ParamGroupBox()
		{
			return m_ParamGroupBox;
		}

		__property System::Windows::Forms::TrackBar* get_ParamTrackBar()
		{
			return m_ParamTrackBar;
		}

		__property System::Windows::Forms::Label* get_ParamLable()
		{
			return m_ParamLable;
		}

		__property System::Windows::Forms::TextBox* get_ParamTextBox()
		{
			return m_ParamTextBox;
		}

		static TerrainEditDialog* Get() { return ms_TerrainEditDialog; }

		void Do_Dispose(bool bDisposing)
		{
			delete m_pmTerrainEditBrush;
			ms_TerrainEditDialog = NULL;
		}
        
    protected: 
        void Dispose(Boolean disposing)
        {
            if (disposing && components)
            {
                components->Dispose();
            }

            __super::Dispose(disposing);
        }
	private: System::ComponentModel::IContainer*  components;
	protected: 

	private: MTerrainEditBrush* m_pmTerrainEditBrush;
	private: MTerrainEditInteractionMode* m_pmCurrentInteractionMode;
	private: static TerrainEditDialog* ms_TerrainEditDialog;

    private: System::Windows::Forms::GroupBox*  m_ActionGroupBox;
	private: System::Windows::Forms::RadioButton*  m_AdjustHeightBtn;
	private: System::Windows::Forms::RadioButton*  m_SmoothHeightBtn;
	private: System::Windows::Forms::RadioButton*  m_IncHeightBtn;
	private: System::Windows::Forms::RadioButton*  m_DecHeightBtn;
	private: System::Windows::Forms::RadioButton*  m_FlattenHeightBtn;
	private: System::Windows::Forms::RadioButton*  m_SetHeightBtn;
	private: System::Windows::Forms::RadioButton*  m_CaveBtn;
	private: System::Windows::Forms::RadioButton*  m_PaintWaterBtn;
	private: System::Windows::Forms::RadioButton*  m_FlattenWaterBtn;
	private: System::Windows::Forms::RadioButton*  m_AddjustWaterBtn;

	private: System::Windows::Forms::GroupBox*  m_BrushGroupBox;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn1;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn3;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn5;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn7;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn9;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn11;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn15;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn21;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn25;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn29;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn33;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn37;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn41;
	private: System::Windows::Forms::RadioButton*  m_SizeBtn45;

	private: System::Windows::Forms::GroupBox*  m_ParamGroupBox;
	private: System::Windows::Forms::TrackBar*  m_ParamTrackBar;
	private: System::Windows::Forms::Label*  m_ParamLable;
	private: System::Windows::Forms::TextBox*  m_ParamTextBox;

    private: System::Windows::Forms::Button*  m_TileEditBtn;

	private: System::Windows::Forms::RadioButton*  m_RemoveMaterialBtn;
	private: System::Windows::Forms::RadioButton*  m_TerrainInfoBtn;
	private: System::Windows::Forms::RadioButton*  m_FlyGridInfoBtn;
	private: System::Windows::Forms::RadioButton*  m_TurnEdgeBtn;
	private: System::Windows::Forms::RadioButton*  m_NoiseHeightBtn;
	private: System::Windows::Forms::RadioButton*  m_AreaInfoBtn;
private: System::Windows::Forms::RadioButton*  m_PaintGrassBtn;
private: System::Windows::Forms::RadioButton*  m_FlyGridBtn;
private: System::Windows::Forms::Button*  m_btnBuidWaterEdgeMap;
private: System::Windows::Forms::Button*  VMapCover;



	private: System::Windows::Forms::RadioButton*  m_PaintMaterialBtn;

        void InitializeComponent(void)
        {
			this->m_ActionGroupBox = (new System::Windows::Forms::GroupBox());
			this->m_FlyGridBtn = (new System::Windows::Forms::RadioButton());
			this->m_PaintGrassBtn = (new System::Windows::Forms::RadioButton());
			this->m_AreaInfoBtn = (new System::Windows::Forms::RadioButton());
			this->m_NoiseHeightBtn = (new System::Windows::Forms::RadioButton());
			this->m_TurnEdgeBtn = (new System::Windows::Forms::RadioButton());
			this->m_TerrainInfoBtn = (new System::Windows::Forms::RadioButton());
			this->m_RemoveMaterialBtn = (new System::Windows::Forms::RadioButton());
			this->m_PaintMaterialBtn = (new System::Windows::Forms::RadioButton());
			this->m_AdjustHeightBtn = (new System::Windows::Forms::RadioButton());
			this->m_SmoothHeightBtn = (new System::Windows::Forms::RadioButton());
			this->m_IncHeightBtn = (new System::Windows::Forms::RadioButton());
			this->m_DecHeightBtn = (new System::Windows::Forms::RadioButton());
			this->m_FlattenHeightBtn = (new System::Windows::Forms::RadioButton());
			this->m_SetHeightBtn = (new System::Windows::Forms::RadioButton());
			this->m_CaveBtn = (new System::Windows::Forms::RadioButton());
			this->m_PaintWaterBtn = (new System::Windows::Forms::RadioButton());
			this->m_FlattenWaterBtn = (new System::Windows::Forms::RadioButton());
			this->m_AddjustWaterBtn = (new System::Windows::Forms::RadioButton());
			this->m_BrushGroupBox = (new System::Windows::Forms::GroupBox());
			this->m_SizeBtn1 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn3 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn5 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn7 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn9 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn11 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn15 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn21 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn25 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn29 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn33 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn37 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn41 = (new System::Windows::Forms::RadioButton());
			this->m_SizeBtn45 = (new System::Windows::Forms::RadioButton());
			this->m_ParamGroupBox = (new System::Windows::Forms::GroupBox());
			this->m_btnBuidWaterEdgeMap = (new System::Windows::Forms::Button());
			this->m_ParamLable = (new System::Windows::Forms::Label());
			this->m_ParamTextBox = (new System::Windows::Forms::TextBox());
			this->m_ParamTrackBar = (new System::Windows::Forms::TrackBar());
			this->m_TileEditBtn = (new System::Windows::Forms::Button());
			this->VMapCover = (new System::Windows::Forms::Button());
			this->m_ActionGroupBox->SuspendLayout();
			this->m_BrushGroupBox->SuspendLayout();
			this->m_ParamGroupBox->SuspendLayout();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_ParamTrackBar))->BeginInit();
			this->SuspendLayout();
			// 
			// m_ActionGroupBox
			// 
			this->m_ActionGroupBox->Controls->Add(this->m_FlyGridBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_PaintGrassBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_AreaInfoBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_NoiseHeightBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_TurnEdgeBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_TerrainInfoBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_RemoveMaterialBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_PaintMaterialBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_AdjustHeightBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_SmoothHeightBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_IncHeightBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_DecHeightBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_FlattenHeightBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_SetHeightBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_CaveBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_PaintWaterBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_FlattenWaterBtn);
			this->m_ActionGroupBox->Controls->Add(this->m_AddjustWaterBtn);
			this->m_ActionGroupBox->Location = System::Drawing::Point(17, 34);
			this->m_ActionGroupBox->Name = S"m_ActionGroupBox";
			this->m_ActionGroupBox->Size = System::Drawing::Size(146, 433);
			this->m_ActionGroupBox->TabIndex = 0;
			this->m_ActionGroupBox->TabStop = false;
			this->m_ActionGroupBox->Text = S"Action";
			// 
			// m_FlyGridBtn
			// 
			this->m_FlyGridBtn->AutoSize = true;
			this->m_FlyGridBtn->Location = System::Drawing::Point(13, 410);
			this->m_FlyGridBtn->Name = S"m_FlyGridBtn";
			this->m_FlyGridBtn->Size = System::Drawing::Size(95, 16);
			this->m_FlyGridBtn->TabIndex = 16;
			this->m_FlyGridBtn->TabStop = true;
			this->m_FlyGridBtn->Text = S"FlyGrid Info";
			this->m_FlyGridBtn->UseVisualStyleBackColor = true;
			this->m_FlyGridBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnFlyGridInfoBtn);
			// 
			// m_PaintGrassBtn
			// 
			this->m_PaintGrassBtn->AutoSize = true;
			this->m_PaintGrassBtn->Location = System::Drawing::Point(13, 387);
			this->m_PaintGrassBtn->Name = S"m_PaintGrassBtn";
			this->m_PaintGrassBtn->Size = System::Drawing::Size(89, 16);
			this->m_PaintGrassBtn->TabIndex = 15;
			this->m_PaintGrassBtn->TabStop = true;
			this->m_PaintGrassBtn->Text = S"Paint Grass";
			this->m_PaintGrassBtn->UseVisualStyleBackColor = true;
			this->m_PaintGrassBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnPaintGrassBtn);
			// 
			// m_AreaInfoBtn
			// 
			this->m_AreaInfoBtn->AutoSize = true;
			this->m_AreaInfoBtn->Location = System::Drawing::Point(13, 365);
			this->m_AreaInfoBtn->Name = S"m_AreaInfoBtn";
			this->m_AreaInfoBtn->Size = System::Drawing::Size(77, 16);
			this->m_AreaInfoBtn->TabIndex = 15;
			this->m_AreaInfoBtn->TabStop = true;
			this->m_AreaInfoBtn->Text = S"Area Info";
			this->m_AreaInfoBtn->UseVisualStyleBackColor = true;
			this->m_AreaInfoBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnPaintAreaBtn);
			// 
			// m_NoiseHeightBtn
			// 
			this->m_NoiseHeightBtn->AutoSize = true;
			this->m_NoiseHeightBtn->Location = System::Drawing::Point(13, 158);
			this->m_NoiseHeightBtn->Name = S"m_NoiseHeightBtn";
			this->m_NoiseHeightBtn->Size = System::Drawing::Size(101, 16);
			this->m_NoiseHeightBtn->TabIndex = 14;
			this->m_NoiseHeightBtn->TabStop = true;
			this->m_NoiseHeightBtn->Text = S"Noise  Height";
			this->m_NoiseHeightBtn->UseVisualStyleBackColor = true;
			this->m_NoiseHeightBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnNoiseHeight);
			// 
			// m_TurnEdgeBtn
			// 
			this->m_TurnEdgeBtn->AutoSize = true;
			this->m_TurnEdgeBtn->Location = System::Drawing::Point(13, 204);
			this->m_TurnEdgeBtn->Name = S"m_TurnEdgeBtn";
			this->m_TurnEdgeBtn->Size = System::Drawing::Size(77, 16);
			this->m_TurnEdgeBtn->TabIndex = 12;
			this->m_TurnEdgeBtn->TabStop = true;
			this->m_TurnEdgeBtn->Text = S"Turn Edge";
			this->m_TurnEdgeBtn->UseVisualStyleBackColor = true;
			this->m_TurnEdgeBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnTurnEdge);
			// 
			// m_TerrainInfoBtn
			// 
			this->m_TerrainInfoBtn->AutoSize = true;
			this->m_TerrainInfoBtn->Location = System::Drawing::Point(13, 342);
			this->m_TerrainInfoBtn->Name = S"m_TerrainInfoBtn";
			this->m_TerrainInfoBtn->Size = System::Drawing::Size(77, 16);
			this->m_TerrainInfoBtn->TabIndex = 11;
			this->m_TerrainInfoBtn->TabStop = true;
			this->m_TerrainInfoBtn->Text = S"Grid Info";
			this->m_TerrainInfoBtn->UseVisualStyleBackColor = true;
			this->m_TerrainInfoBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnTerrainInfoBtn);
			// 
			// m_RemoveMaterialBtn
			// 
			this->m_RemoveMaterialBtn->AutoSize = true;
			this->m_RemoveMaterialBtn->Location = System::Drawing::Point(13, 319);
			this->m_RemoveMaterialBtn->Name = S"m_RemoveMaterialBtn";
			this->m_RemoveMaterialBtn->Size = System::Drawing::Size(113, 16);
			this->m_RemoveMaterialBtn->TabIndex = 10;
			this->m_RemoveMaterialBtn->TabStop = true;
			this->m_RemoveMaterialBtn->Text = S"Delete Material";
			this->m_RemoveMaterialBtn->UseVisualStyleBackColor = true;
			this->m_RemoveMaterialBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnRemoveMaterialBtn);
			// 
			// m_PaintMaterialBtn
			// 
			this->m_PaintMaterialBtn->AutoSize = true;
			this->m_PaintMaterialBtn->Location = System::Drawing::Point(13, 296);
			this->m_PaintMaterialBtn->Name = S"m_PaintMaterialBtn";
			this->m_PaintMaterialBtn->Size = System::Drawing::Size(107, 16);
			this->m_PaintMaterialBtn->TabIndex = 9;
			this->m_PaintMaterialBtn->TabStop = true;
			this->m_PaintMaterialBtn->Text = S"Paint Material";
			this->m_PaintMaterialBtn->UseVisualStyleBackColor = true;
			this->m_PaintMaterialBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnPaintMaterialBtn);
			// 
			// m_AdjustHeightBtn
			// 
			this->m_AdjustHeightBtn->AutoSize = true;
			this->m_AdjustHeightBtn->Location = System::Drawing::Point(13, 20);
			this->m_AdjustHeightBtn->Name = S"m_AdjustHeightBtn";
			this->m_AdjustHeightBtn->Size = System::Drawing::Size(107, 16);
			this->m_AdjustHeightBtn->TabIndex = 0;
			this->m_AdjustHeightBtn->TabStop = true;
			this->m_AdjustHeightBtn->Text = S"Addjust Height";
			this->m_AdjustHeightBtn->UseVisualStyleBackColor = true;
			this->m_AdjustHeightBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnAdjustHeightBtn);
			// 
			// m_SmoothHeightBtn
			// 
			this->m_SmoothHeightBtn->AutoSize = true;
			this->m_SmoothHeightBtn->Location = System::Drawing::Point(13, 43);
			this->m_SmoothHeightBtn->Name = S"m_SmoothHeightBtn";
			this->m_SmoothHeightBtn->Size = System::Drawing::Size(101, 16);
			this->m_SmoothHeightBtn->TabIndex = 1;
			this->m_SmoothHeightBtn->TabStop = true;
			this->m_SmoothHeightBtn->Text = S"Smooth Height";
			this->m_SmoothHeightBtn->UseVisualStyleBackColor = true;
			this->m_SmoothHeightBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnSmoothHeightBtn);
			// 
			// m_IncHeightBtn
			// 
			this->m_IncHeightBtn->AutoSize = true;
			this->m_IncHeightBtn->BackColor = System::Drawing::SystemColors::Control;
			this->m_IncHeightBtn->Location = System::Drawing::Point(13, 66);
			this->m_IncHeightBtn->Name = S"m_IncHeightBtn";
			this->m_IncHeightBtn->Size = System::Drawing::Size(125, 16);
			this->m_IncHeightBtn->TabIndex = 2;
			this->m_IncHeightBtn->TabStop = true;
			this->m_IncHeightBtn->Text = S"Increament Height";
			this->m_IncHeightBtn->UseVisualStyleBackColor = false;
			this->m_IncHeightBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnIncHeightBtn);
			// 
			// m_DecHeightBtn
			// 
			this->m_DecHeightBtn->AutoSize = true;
			this->m_DecHeightBtn->Location = System::Drawing::Point(13, 89);
			this->m_DecHeightBtn->Name = S"m_DecHeightBtn";
			this->m_DecHeightBtn->Size = System::Drawing::Size(125, 16);
			this->m_DecHeightBtn->TabIndex = 3;
			this->m_DecHeightBtn->TabStop = true;
			this->m_DecHeightBtn->Text = S"Decreament Height";
			this->m_DecHeightBtn->UseVisualStyleBackColor = true;
			this->m_DecHeightBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnDecHeightBtn);
			// 
			// m_FlattenHeightBtn
			// 
			this->m_FlattenHeightBtn->AutoSize = true;
			this->m_FlattenHeightBtn->Location = System::Drawing::Point(13, 112);
			this->m_FlattenHeightBtn->Name = S"m_FlattenHeightBtn";
			this->m_FlattenHeightBtn->Size = System::Drawing::Size(107, 16);
			this->m_FlattenHeightBtn->TabIndex = 4;
			this->m_FlattenHeightBtn->TabStop = true;
			this->m_FlattenHeightBtn->Text = S"Flatten Height";
			this->m_FlattenHeightBtn->UseVisualStyleBackColor = true;
			this->m_FlattenHeightBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnFlattenHeightBtn);
			// 
			// m_SetHeightBtn
			// 
			this->m_SetHeightBtn->AutoSize = true;
			this->m_SetHeightBtn->Location = System::Drawing::Point(13, 135);
			this->m_SetHeightBtn->Name = S"m_SetHeightBtn";
			this->m_SetHeightBtn->Size = System::Drawing::Size(83, 16);
			this->m_SetHeightBtn->TabIndex = 5;
			this->m_SetHeightBtn->TabStop = true;
			this->m_SetHeightBtn->Text = S"Set Height";
			this->m_SetHeightBtn->UseVisualStyleBackColor = true;
			this->m_SetHeightBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnSetHeightBtn);
			// 
			// m_CaveBtn
			// 
			this->m_CaveBtn->AutoSize = true;
			this->m_CaveBtn->Location = System::Drawing::Point(13, 181);
			this->m_CaveBtn->Name = S"m_CaveBtn";
			this->m_CaveBtn->Size = System::Drawing::Size(101, 16);
			this->m_CaveBtn->TabIndex = 6;
			this->m_CaveBtn->TabStop = true;
			this->m_CaveBtn->Text = S"Excavate Hole";
			this->m_CaveBtn->UseVisualStyleBackColor = true;
			this->m_CaveBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnExcavateHole);
			// 
			// m_PaintWaterBtn
			// 
			this->m_PaintWaterBtn->AutoSize = true;
			this->m_PaintWaterBtn->Location = System::Drawing::Point(13, 227);
			this->m_PaintWaterBtn->Name = S"m_PaintWaterBtn";
			this->m_PaintWaterBtn->Size = System::Drawing::Size(89, 16);
			this->m_PaintWaterBtn->TabIndex = 7;
			this->m_PaintWaterBtn->TabStop = true;
			this->m_PaintWaterBtn->Text = S"Paint Water";
			this->m_PaintWaterBtn->UseVisualStyleBackColor = true;
			this->m_PaintWaterBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnPaintWaterBtn);
			// 
			// m_FlattenWaterBtn
			// 
			this->m_FlattenWaterBtn->AutoSize = true;
			this->m_FlattenWaterBtn->Location = System::Drawing::Point(13, 273);
			this->m_FlattenWaterBtn->Name = S"m_FlattenWaterBtn";
			this->m_FlattenWaterBtn->Size = System::Drawing::Size(101, 16);
			this->m_FlattenWaterBtn->TabIndex = 8;
			this->m_FlattenWaterBtn->TabStop = true;
			this->m_FlattenWaterBtn->Text = S"Flatten Water";
			this->m_FlattenWaterBtn->UseVisualStyleBackColor = true;
			this->m_FlattenWaterBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnFlattenWaterBtn);
			// 
			// m_AddjustWaterBtn
			// 
			this->m_AddjustWaterBtn->AutoSize = true;
			this->m_AddjustWaterBtn->Location = System::Drawing::Point(13, 250);
			this->m_AddjustWaterBtn->Name = S"m_AddjustWaterBtn";
			this->m_AddjustWaterBtn->Size = System::Drawing::Size(101, 16);
			this->m_AddjustWaterBtn->TabIndex = 13;
			this->m_AddjustWaterBtn->TabStop = true;
			this->m_AddjustWaterBtn->Text = S"Addjust Water";
			this->m_AddjustWaterBtn->UseVisualStyleBackColor = true;
			this->m_AddjustWaterBtn->Click += new System::EventHandler(this, &TerrainEditDialog::OnAddjustWater);
			// 
			// m_BrushGroupBox
			// 
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn1);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn3);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn5);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn7);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn9);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn11);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn15);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn21);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn25);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn29);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn33);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn37);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn41);
			this->m_BrushGroupBox->Controls->Add(this->m_SizeBtn45);
			this->m_BrushGroupBox->Location = System::Drawing::Point(181, 34);
			this->m_BrushGroupBox->Name = S"m_BrushGroupBox";
			this->m_BrushGroupBox->Size = System::Drawing::Size(146, 433);
			this->m_BrushGroupBox->TabIndex = 1;
			this->m_BrushGroupBox->TabStop = false;
			this->m_BrushGroupBox->Text = S"Brush Size";
			// 
			// m_SizeBtn1
			// 
			this->m_SizeBtn1->AutoSize = true;
			this->m_SizeBtn1->Location = System::Drawing::Point(15, 23);
			this->m_SizeBtn1->Name = S"m_SizeBtn1";
			this->m_SizeBtn1->Size = System::Drawing::Size(53, 16);
			this->m_SizeBtn1->TabIndex = 0;
			this->m_SizeBtn1->TabStop = true;
			this->m_SizeBtn1->Text = S"1 x 1";
			this->m_SizeBtn1->UseVisualStyleBackColor = true;
			this->m_SizeBtn1->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize1);
			// 
			// m_SizeBtn3
			// 
			this->m_SizeBtn3->AutoSize = true;
			this->m_SizeBtn3->Location = System::Drawing::Point(15, 50);
			this->m_SizeBtn3->Name = S"m_SizeBtn3";
			this->m_SizeBtn3->Size = System::Drawing::Size(53, 16);
			this->m_SizeBtn3->TabIndex = 1;
			this->m_SizeBtn3->TabStop = true;
			this->m_SizeBtn3->Text = S"3 x 3";
			this->m_SizeBtn3->UseVisualStyleBackColor = true;
			this->m_SizeBtn3->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize3);
			// 
			// m_SizeBtn5
			// 
			this->m_SizeBtn5->AutoSize = true;
			this->m_SizeBtn5->Location = System::Drawing::Point(15, 77);
			this->m_SizeBtn5->Name = S"m_SizeBtn5";
			this->m_SizeBtn5->Size = System::Drawing::Size(53, 16);
			this->m_SizeBtn5->TabIndex = 2;
			this->m_SizeBtn5->TabStop = true;
			this->m_SizeBtn5->Text = S"5 x 5";
			this->m_SizeBtn5->UseVisualStyleBackColor = true;
			this->m_SizeBtn5->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize5);
			// 
			// m_SizeBtn7
			// 
			this->m_SizeBtn7->AutoSize = true;
			this->m_SizeBtn7->Location = System::Drawing::Point(15, 104);
			this->m_SizeBtn7->Name = S"m_SizeBtn7";
			this->m_SizeBtn7->Size = System::Drawing::Size(53, 16);
			this->m_SizeBtn7->TabIndex = 3;
			this->m_SizeBtn7->TabStop = true;
			this->m_SizeBtn7->Text = S"7 x 7";
			this->m_SizeBtn7->UseVisualStyleBackColor = true;
			this->m_SizeBtn7->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize7);
			// 
			// m_SizeBtn9
			// 
			this->m_SizeBtn9->AutoSize = true;
			this->m_SizeBtn9->Location = System::Drawing::Point(15, 131);
			this->m_SizeBtn9->Name = S"m_SizeBtn9";
			this->m_SizeBtn9->Size = System::Drawing::Size(53, 16);
			this->m_SizeBtn9->TabIndex = 4;
			this->m_SizeBtn9->TabStop = true;
			this->m_SizeBtn9->Text = S"9 x 9";
			this->m_SizeBtn9->UseVisualStyleBackColor = true;
			this->m_SizeBtn9->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize9);
			// 
			// m_SizeBtn11
			// 
			this->m_SizeBtn11->AutoSize = true;
			this->m_SizeBtn11->Location = System::Drawing::Point(15, 158);
			this->m_SizeBtn11->Name = S"m_SizeBtn11";
			this->m_SizeBtn11->Size = System::Drawing::Size(65, 16);
			this->m_SizeBtn11->TabIndex = 5;
			this->m_SizeBtn11->TabStop = true;
			this->m_SizeBtn11->Text = S"11 x 11";
			this->m_SizeBtn11->UseVisualStyleBackColor = true;
			this->m_SizeBtn11->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize11);
			// 
			// m_SizeBtn15
			// 
			this->m_SizeBtn15->AutoSize = true;
			this->m_SizeBtn15->Location = System::Drawing::Point(15, 185);
			this->m_SizeBtn15->Name = S"m_SizeBtn15";
			this->m_SizeBtn15->Size = System::Drawing::Size(65, 16);
			this->m_SizeBtn15->TabIndex = 6;
			this->m_SizeBtn15->TabStop = true;
			this->m_SizeBtn15->Text = S"15 x 15";
			this->m_SizeBtn15->UseVisualStyleBackColor = true;
			this->m_SizeBtn15->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize15);
			// 
			// m_SizeBtn21
			// 
			this->m_SizeBtn21->AutoSize = true;
			this->m_SizeBtn21->Location = System::Drawing::Point(15, 212);
			this->m_SizeBtn21->Name = S"m_SizeBtn21";
			this->m_SizeBtn21->Size = System::Drawing::Size(65, 16);
			this->m_SizeBtn21->TabIndex = 7;
			this->m_SizeBtn21->TabStop = true;
			this->m_SizeBtn21->Text = S"21 x 21";
			this->m_SizeBtn21->UseVisualStyleBackColor = true;
			this->m_SizeBtn21->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize21);
			// 
			// m_SizeBtn25
			// 
			this->m_SizeBtn25->AutoSize = true;
			this->m_SizeBtn25->Location = System::Drawing::Point(15, 239);
			this->m_SizeBtn25->Name = S"m_SizeBtn25";
			this->m_SizeBtn25->Size = System::Drawing::Size(65, 16);
			this->m_SizeBtn25->TabIndex = 8;
			this->m_SizeBtn25->TabStop = true;
			this->m_SizeBtn25->Text = S"25 x 25";
			this->m_SizeBtn25->UseVisualStyleBackColor = true;
			this->m_SizeBtn25->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize25);
			// 
			// m_SizeBtn29
			// 
			this->m_SizeBtn29->AutoSize = true;
			this->m_SizeBtn29->Location = System::Drawing::Point(15, 266);
			this->m_SizeBtn29->Name = S"m_SizeBtn29";
			this->m_SizeBtn29->Size = System::Drawing::Size(65, 16);
			this->m_SizeBtn29->TabIndex = 9;
			this->m_SizeBtn29->TabStop = true;
			this->m_SizeBtn29->Text = S"29 x 29";
			this->m_SizeBtn29->UseVisualStyleBackColor = true;
			this->m_SizeBtn29->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize29);
			// 
			// m_SizeBtn33
			// 
			this->m_SizeBtn33->AutoSize = true;
			this->m_SizeBtn33->Location = System::Drawing::Point(15, 293);
			this->m_SizeBtn33->Name = S"m_SizeBtn33";
			this->m_SizeBtn33->Size = System::Drawing::Size(65, 16);
			this->m_SizeBtn33->TabIndex = 10;
			this->m_SizeBtn33->TabStop = true;
			this->m_SizeBtn33->Text = S"33 x 33";
			this->m_SizeBtn33->UseVisualStyleBackColor = true;
			this->m_SizeBtn33->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize33);
			// 
			// m_SizeBtn37
			// 
			this->m_SizeBtn37->AutoSize = true;
			this->m_SizeBtn37->Location = System::Drawing::Point(15, 320);
			this->m_SizeBtn37->Name = S"m_SizeBtn37";
			this->m_SizeBtn37->Size = System::Drawing::Size(65, 16);
			this->m_SizeBtn37->TabIndex = 11;
			this->m_SizeBtn37->TabStop = true;
			this->m_SizeBtn37->Text = S"37 x 37";
			this->m_SizeBtn37->UseVisualStyleBackColor = true;
			this->m_SizeBtn37->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize37);
			// 
			// m_SizeBtn41
			// 
			this->m_SizeBtn41->AutoSize = true;
			this->m_SizeBtn41->Location = System::Drawing::Point(15, 347);
			this->m_SizeBtn41->Name = S"m_SizeBtn41";
			this->m_SizeBtn41->Size = System::Drawing::Size(65, 16);
			this->m_SizeBtn41->TabIndex = 12;
			this->m_SizeBtn41->TabStop = true;
			this->m_SizeBtn41->Text = S"41 x 41";
			this->m_SizeBtn41->UseVisualStyleBackColor = true;
			this->m_SizeBtn41->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize41);
			// 
			// m_SizeBtn45
			// 
			this->m_SizeBtn45->AutoSize = true;
			this->m_SizeBtn45->Location = System::Drawing::Point(15, 374);
			this->m_SizeBtn45->Name = S"m_SizeBtn45";
			this->m_SizeBtn45->Size = System::Drawing::Size(65, 16);
			this->m_SizeBtn45->TabIndex = 13;
			this->m_SizeBtn45->TabStop = true;
			this->m_SizeBtn45->Text = S"45 x 45";
			this->m_SizeBtn45->UseVisualStyleBackColor = true;
			this->m_SizeBtn45->Click += new System::EventHandler(this, &TerrainEditDialog::OnBrushSize45);
			// 
			// m_ParamGroupBox
			// 
			this->m_ParamGroupBox->Controls->Add(this->m_btnBuidWaterEdgeMap);
			this->m_ParamGroupBox->Controls->Add(this->m_ParamLable);
			this->m_ParamGroupBox->Controls->Add(this->m_ParamTextBox);
			this->m_ParamGroupBox->Controls->Add(this->m_ParamTrackBar);
			this->m_ParamGroupBox->Location = System::Drawing::Point(30, 482);
			this->m_ParamGroupBox->Name = S"m_ParamGroupBox";
			this->m_ParamGroupBox->Size = System::Drawing::Size(280, 106);
			this->m_ParamGroupBox->TabIndex = 3;
			this->m_ParamGroupBox->TabStop = false;
			this->m_ParamGroupBox->Text = S"Parameter";
			// 
			// m_btnBuidWaterEdgeMap
			// 
			this->m_btnBuidWaterEdgeMap->Location = System::Drawing::Point(142, 41);
			this->m_btnBuidWaterEdgeMap->Name = S"m_btnBuidWaterEdgeMap";
			this->m_btnBuidWaterEdgeMap->Size = System::Drawing::Size(107, 22);
			this->m_btnBuidWaterEdgeMap->TabIndex = 17;
			this->m_btnBuidWaterEdgeMap->Text = S"��Ե�黯";
			this->m_btnBuidWaterEdgeMap->UseVisualStyleBackColor = true;
			this->m_btnBuidWaterEdgeMap->Click += new System::EventHandler(this, &TerrainEditDialog::WaterEdgeMapBtn_Click);
			// 
			// m_ParamLable
			// 
			this->m_ParamLable->AutoSize = true;
			this->m_ParamLable->Location = System::Drawing::Point(31, 69);
			this->m_ParamLable->Name = S"m_ParamLable";
			this->m_ParamLable->Size = System::Drawing::Size(35, 12);
			this->m_ParamLable->TabIndex = 2;
			this->m_ParamLable->Text = S"Size:";
			// 
			// m_ParamTextBox
			// 
			this->m_ParamTextBox->Location = System::Drawing::Point(142, 66);
			this->m_ParamTextBox->Name = S"m_ParamTextBox";
			this->m_ParamTextBox->Size = System::Drawing::Size(107, 21);
			this->m_ParamTextBox->TabIndex = 1;
			this->m_ParamTextBox->Leave += new System::EventHandler(this, &TerrainEditDialog::OnParamTextBoxChanged);
			this->m_ParamTextBox->Validating += new System::ComponentModel::CancelEventHandler(this, &TerrainEditDialog::OnParamTextBoxValidating);
			// 
			// m_ParamTrackBar
			// 
			this->m_ParamTrackBar->Location = System::Drawing::Point(6, 21);
			this->m_ParamTrackBar->Maximum = 1000;
			this->m_ParamTrackBar->Name = S"m_ParamTrackBar";
			this->m_ParamTrackBar->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->m_ParamTrackBar->Size = System::Drawing::Size(268, 42);
			this->m_ParamTrackBar->TabIndex = 0;
			this->m_ParamTrackBar->TickStyle = System::Windows::Forms::TickStyle::None;
			this->m_ParamTrackBar->ValueChanged += new System::EventHandler(this, &TerrainEditDialog::OnParamTrackBarChanged);
			// 
			// m_TileEditBtn
			// 
			this->m_TileEditBtn->Location = System::Drawing::Point(63, 605);
			this->m_TileEditBtn->Name = S"m_TileEditBtn";
			this->m_TileEditBtn->Size = System::Drawing::Size(75, 23);
			this->m_TileEditBtn->TabIndex = 2;
			this->m_TileEditBtn->Text = S"Edit Tile";
			this->m_TileEditBtn->UseVisualStyleBackColor = true;
			this->m_TileEditBtn->Click += new System::EventHandler(this, &TerrainEditDialog::TileEditBtn_Click);
			// 
			// VMapCover
			// 
			this->VMapCover->Location = System::Drawing::Point(190, 603);
			this->VMapCover->Name = S"VMapCover";
			this->VMapCover->Size = System::Drawing::Size(75, 23);
			this->VMapCover->TabIndex = 4;
			this->VMapCover->Text = S"VMapCover";
			this->VMapCover->UseVisualStyleBackColor = true;
			this->VMapCover->Click += new System::EventHandler(this, &TerrainEditDialog::VMapCoverBtn_Click);
			// 
			// TerrainEditDialog
			// 
			this->ClientSize = System::Drawing::Size(340, 644);
			this->Controls->Add(this->VMapCover);
			this->Controls->Add(this->m_ActionGroupBox);
			this->Controls->Add(this->m_BrushGroupBox);
			this->Controls->Add(this->m_ParamGroupBox);
			this->Controls->Add(this->m_TileEditBtn);
			this->Name = S"TerrainEditDialog";
			this->Text = S"Terrain Edit";
			this->m_ActionGroupBox->ResumeLayout(false);
			this->m_ActionGroupBox->PerformLayout();
			this->m_BrushGroupBox->ResumeLayout(false);
			this->m_BrushGroupBox->PerformLayout();
			this->m_ParamGroupBox->ResumeLayout(false);
			this->m_ParamGroupBox->PerformLayout();
			(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_ParamTrackBar))->EndInit();
			this->ResumeLayout(false);

		}
	private: System::Void OnBrushSize1(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(1, 1);
			 }
	private: System::Void OnBrushSize3(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(3, 3);
			 }
	private: System::Void OnBrushSize5(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(5, 5);
			 }
	private: System::Void OnBrushSize7(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(7, 7);
			 }
	private: System::Void OnBrushSize9(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(9, 9);
			 }
	private: System::Void OnBrushSize11(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(11, 11);
			 }
	private: System::Void OnBrushSize15(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(15, 15);
			 }
	private: System::Void OnBrushSize21(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(21, 21);
			 }
	private: System::Void OnBrushSize25(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(25, 25);
			 }
	private: System::Void OnBrushSize29(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(29, 29);
			 }
	private: System::Void OnBrushSize33(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(33, 33);
		     }
	private: System::Void OnBrushSize37(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(37, 37);
		     }
	private: System::Void OnBrushSize41(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(41, 41);
		     }
	private: System::Void OnBrushSize45(System::Object*  sender, System::EventArgs*  e) {
				 m_pmTerrainEditBrush->SetSize(45, 45);
		     }

	private: System::Void OnParamTrackBarChanged(System::Object*  sender, System::EventArgs*  e) {
				 if( m_pmCurrentInteractionMode )
					 m_pmCurrentInteractionMode->OnParamTrackBarChanged();
			 }
	private: System::Void OnParamTextBoxValidating(System::Object*  sender, System::ComponentModel::CancelEventArgs*  e) {
				 e->Cancel = false;
				 if( m_pmCurrentInteractionMode && !m_pmCurrentInteractionMode->OnParamTextBoxValidating() )
					 ::MessageBox((HWND)this->Handle.ToInt32(), "Invalid Value", "Error",MB_OK | MB_ICONERROR);
			 }
	private: System::Void OnParamTextBoxChanged(System::Object*  sender, System::EventArgs*  e) {
				 if( m_pmCurrentInteractionMode )
					 m_pmCurrentInteractionMode->OnParamTextBoxChanged();
			 }
	private: System::Void TileEditBtn_Click(System::Object*  sender, System::EventArgs*  e) {
				 TileEditDialog* dlg = new TileEditDialog;

				 CMap* pkMap = CMap::Get();

				 if( pkMap )
				 {
					 const int* piFlags = pkMap->GetTileFlags();
					 dlg->SetTileFlags(piFlags);
				 }

				 dlg->ShowDialog();
			 }
	private: System::Void OnAdjustHeightBtn(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Adjust Height");
			 }
	private: System::Void OnSmoothHeightBtn(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Smooth Height");
			 }
 	private: System::Void OnIncHeightBtn(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Inc Height");
			 }
	private: System::Void OnDecHeightBtn(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Dec Height");
			 }
	private: System::Void OnFlattenHeightBtn(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Flatten Height");
			 }
	private: System::Void OnSetHeightBtn(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Set Height");
			 }
	private: System::Void OnNoiseHeight(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Noise Height");
			 }
	private: System::Void OnPaintMaterialBtn(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Paint Material");
			 }
	private: System::Void OnRemoveMaterialBtn(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Remove Material");
			 }
	private: System::Void OnPaintWaterBtn(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Paint Water");
			 }
	private: System::Void OnFlattenWaterBtn(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Flatten Water");
			 }
    private: System::Void OnAddjustWater(System::Object*  sender, System::EventArgs*  e) {
			 SetInteractionMode("Adjust Water");
		     }
	private: System::Void OnTerrainInfoBtn(System::Object*  sender, System::EventArgs*  e) {
			 SetInteractionMode("Paint Terrain Info");
			 }
	private: System::Void OnExcavateHole(System::Object*  sender, System::EventArgs*  e) {
			 SetInteractionMode("Excavate Hole");
			 }
	private: System::Void OnTurnEdge(System::Object*  sender, System::EventArgs*  e) {
			 SetInteractionMode("Turn Edge");
			 }
	private: System::Void OnPaintAreaBtn(System::Object*  sender, System::EventArgs*  e) {
				 SetInteractionMode("Paint Area");
			 }
    private: System::Void OnPaintGrassBtn(System::Object*  sender, System::EventArgs*  e) {
                 SetInteractionMode("Paint Grass");
             }

private: System::Void OnFlyGridInfoBtn(System::Object*  sender, System::EventArgs*  e) {
				SetInteractionMode("Paint FlyGrid Info");
		 }
private: System::Void WaterEdgeMapBtn_Click(System::Object*  sender, System::EventArgs*  e) 
		 {
			 CMap* pkMap = CMap::Get();
			 if( !pkMap )
				 return;

			 pkMap->BuidWaterEdgeMap();
		 }
private: System::Void VMapCoverBtn_Click(System::Object*  sender, System::EventArgs*  e) 
		 {
			 CMap* pkMap = CMap::Get();
			 if( !pkMap )
				 return;
//#ifdef USE_VMAP
			 pkMap->StartVMapCover();
//#endif
		 }
};
} }}}