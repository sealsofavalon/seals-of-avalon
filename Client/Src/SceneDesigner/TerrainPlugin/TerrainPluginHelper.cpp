#include "TerrainPluginPCH.h"
#include "TerrainPluginHelper.h"
#include "FreeImage.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;


namespace Emergent { namespace Gamebryo { namespace SceneDesigner { namespace TerrainPlugin {

namespace TerrainPluginHelper
{
	bool GetLoadPathName(char* lpszPathName, const char* pcFilter)
	{
		OpenFileDialog* pmDialog = new OpenFileDialog();
		pmDialog->Title = "Open file";
		pmDialog->Filter = pcFilter;
		pmDialog->RestoreDirectory = true;
		if (pmDialog->ShowDialog() == DialogResult::OK)
		{
			const char* pcTemp = MStringToCharPointer(pmDialog->FileName);
			NiStrcpy(lpszPathName, 1024, pcTemp);
			MFreeCharPointer(pcTemp);

			return true;
		}

		return false;
	}

	//===========================================================================================================
	//Free Image
	//

	FIBITMAP* LoadImage(const char* lpszPathName)
	{
		FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(lpszPathName);
		if( fif == FIF_UNKNOWN )
		{
			fif = FreeImage_GetFIFFromFilename(lpszPathName);
		}

		if((fif != FIF_UNKNOWN) && FreeImage_FIFSupportsReading(fif)) 
		{
			return FreeImage_Load(fif, lpszPathName, 0);
		}

		return NULL;
	}

	void UnloadImage(FIBITMAP* dib)
	{
		FreeImage_Unload(dib);
	}

	void DrawImage(HDC hDC, int x, int y, int Width, int Height, FIBITMAP* fi)
	{
		if( !fi )
			return;

		SetStretchBltMode(hDC, COLORONCOLOR);
		StretchDIBits(hDC, x, y, Width, Height, 0, 0, 
		FreeImage_GetWidth(fi), 
		FreeImage_GetHeight(fi), 
		FreeImage_GetBits(fi), 
		FreeImage_GetInfo(fi), DIB_RGB_COLORS, SRCCOPY);
	}

	FIBITMAP* RescaleImage(FIBITMAP *dib, int dst_width, int dst_height)
	{
		return FreeImage_Rescale(dib, dst_width, dst_height, FILTER_BILINEAR);
	}
}

}}}}