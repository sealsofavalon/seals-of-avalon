#pragma once

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
    namespace TerrainPlugin 
{
	namespace TerrainPluginHelper
	{
		bool GetLoadPathName(char* lpszPathName, const char* pcFilter);
		FIBITMAP* LoadImage(const char* lpszPathName);
		void UnloadImage(FIBITMAP* dib);
		void DrawImage(HDC hDC, int x, int y, int Width, int Height, FIBITMAP* fi);
		FIBITMAP* RescaleImage(FIBITMAP *dib, int dst_width, int dst_height);
	};

}}}}