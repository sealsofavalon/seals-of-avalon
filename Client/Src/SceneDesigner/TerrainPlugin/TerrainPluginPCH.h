// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27514
// http://www.emergentgametech.com

#pragma once

#pragma unmanaged

#include <vector>
#include <list>

#include <NiSystem.h>
#include <NiMain.h>
#include <NiAnimation.h>
#include <NiParticle.h>
#include <NiCollision.h>
#include <NiPortal.h>
#include <NiDX9Renderer.h>
#include <NiEntity.h>
#include <NiViewMath.h>
#include <NiMemStream.h>
#include <NiFilename.h>

#include <Map.h>
#include <Tile.h>
#include <Chunk.h>
#include <MapStreaming.h>
#include <TerrainHelper.h>
#include <Player.h>
#include <WayPointComponent.h>
#include <TerrainShader.h>
#include <EntityInfoComponent.h>
#include <PathNodeComponent.h>
#include <GrassInfo.h>
#include <CollisionShader.h>
#include <DBFile/DBManager.h>


#pragma managed

#using <SceneDesignerFramework.dll>
#using <StdPluginsCpp.dll>

using namespace System;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::StdPluginsCpp;

#include "ManagedMacros.h"
