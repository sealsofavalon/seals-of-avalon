#include "TerrainPluginPCH.h"
#include "TextureProperty.h"
#include "PaintMaterialDialog.h"
#include "TerrainPluginHelper.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

int Scale2Value(float fScale)
{
	if( fScale <= 0.125f )
		return 11;

	if( fScale <= 0.25f )
		return 10;

	if( fScale <= 0.5f )
		return 9;

	if( fScale <= 1.f )
		return 8;

	if( fScale <= 2.f )
		return 7;

	if( fScale <= 4.f )
		return 6;

	if( fScale <= 8.f )
		return 5;

	//������
	return 8;
}

float Value2Scale(int iValue)
{
	switch( iValue )
	{
	case 5:
		return 8.f;
	case 6:
		return 4.f;
	case 7:
		return 2.f;
	case 8:
		return 1.f;
	case 9:
		return 0.5f;
	case 10:
		return 0.25f;
	case 11:
		return 0.125f;
	default:
		return 1.f;
	}
}


System::Void TexturePropertyDialog::OnPictureBoxPaint(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e)
{
	Pen* RedPen = new Pen(Color::FromArgb(255, 255, 0, 0), 4);
	e->Graphics->DrawRectangle(RedPen, 0, 0, m_PictureBox->Width, m_PictureBox->Height);

	if( PaintMaterialDialog::Get() == NULL || PaintMaterialDialog::Get()->ActiveTextureControl == NULL )
		return;

	FIBITMAP* fi = PaintMaterialDialog::Get()->ActiveTextureControl->Bitmap;
	if( fi == NULL )
		return;

	Pen* GreenPen = new Pen(Color::FromArgb(255, 0, 255, 0), 1);

	float fUScale = Value2Scale(m_TrackBarScaleU->Value);
	float fVScale = Value2Scale(m_TrackBarScaleV->Value);

	if( fUScale <= 1.f && fVScale <= 1.f )
	{
		IntPtr hDC = e->Graphics->GetHdc();

		for( float u = 0.f; u < 1.f; u += fUScale )
		{
			for( float v = 0.f; v < 1.f; v += fVScale )
			{
				TerrainPluginHelper::DrawImage((HDC)hDC.ToInt32(), int(u*m_PictureBox->Width), int(v*m_PictureBox->Height), int(m_PictureBox->Width*fUScale), int(m_PictureBox->Height*fVScale), fi);
			}
		}

		e->Graphics->ReleaseHdc(hDC);

		//�߿�
		for( float u = 0.f; u < 1.f; u += fUScale )
		{
			for( float v = 0.f; v < 1.f; v += fVScale )
			{
				e->Graphics->DrawRectangle(GreenPen, int(u*m_PictureBox->Width) + 1, int(v*m_PictureBox->Height) + 1, int(m_PictureBox->Width*fUScale) - 2, int(m_PictureBox->Height*fVScale) - 2);
			}
		}
	}
	else
	{
		fUScale = 1.f/fUScale;
		fVScale = 1.f/fVScale;

		IntPtr hDC = e->Graphics->GetHdc();

		TerrainPluginHelper::DrawImage((HDC)hDC.ToInt32(), 0, 0, int(m_PictureBox->Width*fUScale), int(m_PictureBox->Height*fVScale), fi);

		e->Graphics->ReleaseHdc(hDC);
	}

}

System::Void TexturePropertyDialog::OnTexturePropertyChanged(System::Object*  sender, System::EventArgs*  e) 
{
	if( PaintMaterialDialog::Get() == NULL || PaintMaterialDialog::Get()->ActiveTextureControl == NULL )
		return;

	CMap* pkMap = CMap::Get();

	if(  pkMap == NULL )
		return;

	if( pkMap->GetCurrentTile() == NULL )
		return;

	float fUScale = Value2Scale(m_TrackBarScaleU->Value);
	float fVScale = Value2Scale(m_TrackBarScaleV->Value);
	pkMap->GetCurrentTile()->SetTextureUVScale(PaintMaterialDialog::Get()->ActiveTextureControl->PathName, fUScale, fVScale);

	UpdateTextureProperty();
}

void TexturePropertyDialog::UpdateTextureProperty()
{
	if( PaintMaterialDialog::Get() == NULL || PaintMaterialDialog::Get()->ActiveTextureControl == NULL )
		return;

	if( CMap::Get() == NULL )
		return;

	if( CMap::Get()->GetCurrentTile() == NULL )
		return;

	float fUScale;
	float fVScale;
	CMap::Get()->GetCurrentTile()->GetTextureUVScale(PaintMaterialDialog::Get()->ActiveTextureControl->PathName, fUScale, fVScale);

	m_TrackBarScaleU->Value = Scale2Value(fUScale);
	m_TrackBarScaleV->Value = Scale2Value(fVScale);

	unsigned int uiWidth;
	unsigned int uiHeight;
	PaintMaterialDialog::Get()->ActiveTextureControl->GetTextureSize(uiWidth, uiHeight);

	this->m_TextureInfo->Text = String::Format("Texture Size {0}X{1}", __box(uiWidth), __box(uiHeight));
	
	if( fUScale > 1.f )
		m_LableScaleU->Text = String::Format("U: 1/{0} grid", __box(int(fUScale)));
	else
		m_LableScaleU->Text = String::Format("U: {0} grid", __box(int(1/fUScale)));

	if( fVScale > 1.f )
		m_LableScaleV->Text = String::Format("V: 1/{0} grid", __box(int(fVScale)));
	else
		m_LableScaleV->Text = String::Format("V: {0} grid", __box(int(1/fVScale)));

	m_PictureBox->Invalidate();
}