
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
	namespace TerrainPlugin {

		public __gc class TexturePropertyDialog : public System::Windows::Forms::Form
		{
		public: 
			TexturePropertyDialog(void)
			{
				ms_This = this;
				InitializeComponent();
			}

			void Do_Dispose(bool bDisposing)
			{
			}

		protected:
			void Dispose(Boolean disposing)
			{
				ms_This = NULL;
				if (disposing && components)
				{
					components->Dispose();
				}
				__super::Dispose(disposing);
			}

		private: System::Windows::Forms::Label*  m_TextureInfo;

	    private: System::Windows::Forms::PictureBox*  m_PictureBox;

		private: System::Windows::Forms::Label*  m_LableScaleU;
		private: System::Windows::Forms::TrackBar*  m_TrackBarScaleU;

		private: System::Windows::Forms::Label*  m_LableScaleV;
		private: System::Windows::Forms::TrackBar*  m_TrackBarScaleV;

		private: System::ComponentModel::IContainer*  components;

		private: static TexturePropertyDialog* ms_This = NULL;

		private:
			void InitializeComponent(void)
			{
				this->m_LableScaleU = (new System::Windows::Forms::Label());
				this->m_TrackBarScaleU = (new System::Windows::Forms::TrackBar());
				this->m_LableScaleV = (new System::Windows::Forms::Label());
				this->m_TrackBarScaleV = (new System::Windows::Forms::TrackBar());
				this->m_PictureBox = (new System::Windows::Forms::PictureBox());
				this->m_TextureInfo = (new System::Windows::Forms::Label());
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_TrackBarScaleU))->BeginInit();
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_TrackBarScaleV))->BeginInit();
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_PictureBox))->BeginInit();
				this->SuspendLayout();
				// 
				// m_LableScaleU
				// 
				this->m_LableScaleU->AutoSize = true;
				this->m_LableScaleU->Location = System::Drawing::Point(51, 340);
				this->m_LableScaleU->Name = S"m_LableScaleU";
				this->m_LableScaleU->Size = System::Drawing::Size(17, 12);
				this->m_LableScaleU->TabIndex = 1;
				this->m_LableScaleU->Text = S"U:";
				// 
				// m_TrackBarScaleU
				// 
				this->m_TrackBarScaleU->LargeChange = 1;
				this->m_TrackBarScaleU->Location = System::Drawing::Point(141, 330);
				this->m_TrackBarScaleU->Maximum = 11;
				this->m_TrackBarScaleU->Minimum = 5;
				this->m_TrackBarScaleU->Name = S"m_TrackBarScaleU";
				this->m_TrackBarScaleU->Size = System::Drawing::Size(168, 45);
				this->m_TrackBarScaleU->TabIndex = 0;
				this->m_TrackBarScaleU->Value = 8;
				this->m_TrackBarScaleU->ValueChanged += new System::EventHandler(this, &TexturePropertyDialog::OnTexturePropertyChanged);
				// 
				// m_LableScaleV
				// 
				this->m_LableScaleV->AutoSize = true;
				this->m_LableScaleV->Location = System::Drawing::Point(51, 391);
				this->m_LableScaleV->Name = S"m_LableScaleV";
				this->m_LableScaleV->Size = System::Drawing::Size(17, 12);
				this->m_LableScaleV->TabIndex = 1;
				this->m_LableScaleV->Text = S"V:";
				// 
				// m_TrackBarScaleV
				// 
				this->m_TrackBarScaleV->LargeChange = 1;
				this->m_TrackBarScaleV->Location = System::Drawing::Point(141, 381);
				this->m_TrackBarScaleV->Maximum = 11;
				this->m_TrackBarScaleV->Minimum = 5;
				this->m_TrackBarScaleV->Name = S"m_TrackBarScaleV";
				this->m_TrackBarScaleV->Size = System::Drawing::Size(168, 45);
				this->m_TrackBarScaleV->TabIndex = 0;
				this->m_TrackBarScaleV->Value = 8;
				this->m_TrackBarScaleV->ValueChanged += new System::EventHandler(this, &TexturePropertyDialog::OnTexturePropertyChanged);
				// 
				// m_PictureBox
				// 
				this->m_PictureBox->Location = System::Drawing::Point(53, 54);
				this->m_PictureBox->MaximumSize = System::Drawing::Size(256, 256);
				this->m_PictureBox->MinimumSize = System::Drawing::Size(256, 256);
				this->m_PictureBox->Name = S"m_PictureBox";
				this->m_PictureBox->Size = System::Drawing::Size(256, 256);
				this->m_PictureBox->TabIndex = 2;
				this->m_PictureBox->TabStop = false;
				this->m_PictureBox->Paint += new System::Windows::Forms::PaintEventHandler(this, &TexturePropertyDialog::OnPictureBoxPaint);
				// 
				// m_TextureInfo
				// 
				this->m_TextureInfo->AutoSize = true;
				this->m_TextureInfo->Location = System::Drawing::Point(51, 20);
				this->m_TextureInfo->Name = S"m_TextureInfo";
				this->m_TextureInfo->Size = System::Drawing::Size(65, 12);
				this->m_TextureInfo->TabIndex = 3;
				this->m_TextureInfo->Text = S"TexureSize";
				// 
				// TexturePropertyDialog
				// 
				this->ClientSize = System::Drawing::Size(362, 435);
				this->Controls->Add(this->m_TextureInfo);
				this->Controls->Add(this->m_PictureBox);
				this->Controls->Add(this->m_LableScaleU);
				this->Controls->Add(this->m_TrackBarScaleU);
				this->Controls->Add(this->m_LableScaleV);
				this->Controls->Add(this->m_TrackBarScaleV);
				this->Name = S"TexturePropertyDialog";
				this->Text = S"TextureProperty";
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_TrackBarScaleU))->EndInit();
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_TrackBarScaleV))->EndInit();
				(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_PictureBox))->EndInit();
				this->ResumeLayout(false);
				this->PerformLayout();

			}

		public:
			static TexturePropertyDialog* Get()
			{
				return ms_This;
			}

			System::Void OnPictureBoxPaint(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e);
			System::Void OnTexturePropertyChanged(System::Object*  sender, System::EventArgs*  e);
			void UpdateTextureProperty();
		};
	} }}}