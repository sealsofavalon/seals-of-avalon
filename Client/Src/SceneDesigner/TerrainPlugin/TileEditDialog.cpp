
#include "TerrainPluginPCH.h"
#include "TileEditDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;


TileEditDialog::TileEditDialog(void)
{
	m_TileFlags = new int[TILE_COUNT*TILE_COUNT];
	memset(m_TileFlags, 0, TILE_COUNT*TILE_COUNT*sizeof(int));

	m_CurrentSelectedX = -1;
	m_CurrentSelectedY = -1;

	InitializeComponent();
}

void TileEditDialog::SetTileFlags(const int* piFlags)
{
	memcpy(m_TileFlags, piFlags, TILE_COUNT*TILE_COUNT*sizeof(int));
}

void TileEditDialog::SetTileFlag(int x, int y, int iFlag)
{
	if( x >= 0
		&& x <= TILE_COUNT
		&& y >= 0
		&& y <= TILE_COUNT )
	{
		m_TileFlags[x*TILE_COUNT + y] = iFlag;
		this->m_TilePanel->Invalidate();
	}
}

int TileEditDialog::GetTileFlag(int x, int y)
{
	if( x >= 0
		&& x <= TILE_COUNT
		&& y >= 0
		&& y <= TILE_COUNT )
	{
		return m_TileFlags[x*TILE_COUNT + y];
	}

	return 0;
}

void TileEditDialog::Dispose(Boolean disposing)
{
	if (disposing && components)
	{
		components->Dispose();
	}
	__super::Dispose(disposing);
}

void TileEditDialog::InitializeComponent(void)
{
	this->m_ScrollPanel = (new System::Windows::Forms::Panel());
	this->m_TilePanel = (new System::Windows::Forms::Panel());
	this->m_MousePosLable = (new System::Windows::Forms::Label());
	this->m_SelectedPosLable = (new System::Windows::Forms::Label());
	this->m_NewTileBtn = (new System::Windows::Forms::Button());
	this->m_DeleteTileBtn = (new System::Windows::Forms::Button());
	this->m_ImportBtn = (new System::Windows::Forms::Button());
	this->m_GotoTileBtn = (new System::Windows::Forms::Button());
	this->m_BtnSave = (new System::Windows::Forms::Button());
	this->m_ScrollPanel->SuspendLayout();
	this->SuspendLayout();
	// 
	// m_ScrollPanel
	// 
	this->m_ScrollPanel->AutoScroll = true;
	this->m_ScrollPanel->Controls->Add(this->m_TilePanel);
	this->m_ScrollPanel->Location = System::Drawing::Point(30, 21);
	this->m_ScrollPanel->Name = S"m_ScrollPanel";
	this->m_ScrollPanel->Size = System::Drawing::Size(621, 409);
	this->m_ScrollPanel->TabIndex = 0;
	// 
	// m_TilePanel
	// 
	this->m_TilePanel->Location = System::Drawing::Point(0, 0);
	this->m_TilePanel->Name = S"m_TilePanel";
	this->m_TilePanel->Size = System::Drawing::Size(2052, 2052);
	this->m_TilePanel->TabIndex = 0;
	this->m_TilePanel->Paint += new System::Windows::Forms::PaintEventHandler(this, &TileEditDialog::PaintTiles);
	this->m_TilePanel->MouseMove += new System::Windows::Forms::MouseEventHandler(this, &TileEditDialog::TilePanelMouseMove);
	this->m_TilePanel->MouseDown += new System::Windows::Forms::MouseEventHandler(this, &TileEditDialog::TilePanlMouseDown);
	// 
	// m_MousePosLable
	// 
	this->m_MousePosLable->Location = System::Drawing::Point(162, 531);
	this->m_MousePosLable->Name = S"m_MousePosLable";
	this->m_MousePosLable->Size = System::Drawing::Size(129, 15);
	this->m_MousePosLable->TabIndex = 1;
	this->m_MousePosLable->Text = S"Mouse";
	// 
	// m_SelectedPosLable
	// 
	this->m_SelectedPosLable->ForeColor = System::Drawing::SystemColors::ControlText;
	this->m_SelectedPosLable->Location = System::Drawing::Point(390, 531);
	this->m_SelectedPosLable->Name = S"m_SelectedPosLable";
	this->m_SelectedPosLable->Size = System::Drawing::Size(142, 15);
	this->m_SelectedPosLable->TabIndex = 2;
	this->m_SelectedPosLable->Text = S"Current";
	// 
	// m_NewTileBtn
	// 
	this->m_NewTileBtn->Location = System::Drawing::Point(54, 473);
	this->m_NewTileBtn->Name = S"m_NewTileBtn";
	this->m_NewTileBtn->Size = System::Drawing::Size(75, 23);
	this->m_NewTileBtn->TabIndex = 3;
	this->m_NewTileBtn->Text = S"&New ";
	this->m_NewTileBtn->UseVisualStyleBackColor = true;
	this->m_NewTileBtn->Click += new System::EventHandler(this, &TileEditDialog::OnCreateTile);
	// 
	// m_DeleteTileBtn
	// 
	this->m_DeleteTileBtn->Location = System::Drawing::Point(178, 473);
	this->m_DeleteTileBtn->Name = S"m_DeleteTileBtn";
	this->m_DeleteTileBtn->Size = System::Drawing::Size(75, 23);
	this->m_DeleteTileBtn->TabIndex = 4;
	this->m_DeleteTileBtn->Text = S"&Delete";
	this->m_DeleteTileBtn->UseVisualStyleBackColor = true;
	this->m_DeleteTileBtn->Click += new System::EventHandler(this, &TileEditDialog::OnDeleteTile);
	// 
	// m_ImportBtn
	// 
	this->m_ImportBtn->Location = System::Drawing::Point(303, 473);
	this->m_ImportBtn->Name = S"m_ImportBtn";
	this->m_ImportBtn->Size = System::Drawing::Size(75, 23);
	this->m_ImportBtn->TabIndex = 5;
	this->m_ImportBtn->Text = S"&Import";
	this->m_ImportBtn->UseVisualStyleBackColor = true;
	this->m_ImportBtn->Click += new System::EventHandler(this, &TileEditDialog::OnImport);
	// 
	// m_GotoTileBtn
	// 
	this->m_GotoTileBtn->Location = System::Drawing::Point(428, 473);
	this->m_GotoTileBtn->Name = S"m_GotoTileBtn";
	this->m_GotoTileBtn->Size = System::Drawing::Size(75, 23);
	this->m_GotoTileBtn->TabIndex = 6;
	this->m_GotoTileBtn->Text = S"&Goto";
	this->m_GotoTileBtn->UseVisualStyleBackColor = true;
	this->m_GotoTileBtn->Click += new System::EventHandler(this, &TileEditDialog::OnGoto);
	// 
	// m_BtnSave
	// 
	this->m_BtnSave->Location = System::Drawing::Point(553, 473);
	this->m_BtnSave->Name = S"m_BtnSave";
	this->m_BtnSave->Size = System::Drawing::Size(75, 23);
	this->m_BtnSave->TabIndex = 7;
	this->m_BtnSave->Text = S"&Save";
	this->m_BtnSave->UseVisualStyleBackColor = true;
	this->m_BtnSave->Click += new System::EventHandler(this, &TileEditDialog::OnSaveTile);
	// 
	// TileEditDialog
	// 
	this->ClientSize = System::Drawing::Size(680, 555);
	this->Controls->Add(this->m_BtnSave);
	this->Controls->Add(this->m_GotoTileBtn);
	this->Controls->Add(this->m_ImportBtn);
	this->Controls->Add(this->m_DeleteTileBtn);
	this->Controls->Add(this->m_NewTileBtn);
	this->Controls->Add(this->m_SelectedPosLable);
	this->Controls->Add(this->m_MousePosLable);
	this->Controls->Add(this->m_ScrollPanel);
	this->Name = S"TileEditDialog";
	this->Text = S"Tile Edit";
	this->VisibleChanged += new System::EventHandler(this, &TileEditDialog::OnVisible);
	this->m_ScrollPanel->ResumeLayout(false);
	this->ResumeLayout(false);

}

//-----------------------------------------------------------------------------------------------------------------
//显示
//
System::Void TileEditDialog::PaintTiles(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e) 
{

	SolidBrush* GreenBrush = new SolidBrush(Color::FromArgb(255, 0, 200, 0));

	//有中区的地方显示为绿色
	for( int i = 0; i < 64; i++ )
	{
		for( int j = 0; j < 64; j++ )
		{
			if( m_TileFlags[i*TILE_COUNT + j] )
				e->Graphics->FillRectangle(GreenBrush, i*32, j*32, 32, 32);
		}
	}

	//当前区用蓝色
	if( CMap::Get() && CMap::Get()->GetCurrentTile() )
	{
		SolidBrush* BlueBrush = new SolidBrush(Color::FromArgb(255, 0, 0, 200));
		e->Graphics->FillRectangle(BlueBrush, CMap::Get()->GetCurrentTile()->GetX()*32, CMap::Get()->GetCurrentTile()->GetY()*32, 32, 32);
	}

	//当前选中的显示为红色
	if( this->m_CurrentSelectedX >= 0
		&& this->m_CurrentSelectedX < 64
		&& this->m_CurrentSelectedY >= 0
		&& this->m_CurrentSelectedY < 64 )
	{
		SolidBrush* RedBrush = new SolidBrush(Color::FromArgb(255, 200, 0, 0));
		e->Graphics->FillRectangle(RedBrush, this->m_CurrentSelectedX*32 + 5, this->m_CurrentSelectedY*32 + 5, 24, 24);
	}


	//网格线
	Pen* BlackPen = new Pen(Color::FromArgb(255, 0, 0, 0), 2);
	for( int i = 0; i <= 64; i++ )
	{
		e->Graphics->DrawLine(BlackPen, 1, i*32 + 1, 32*64 + 1, i*32 + 1);
		e->Graphics->DrawLine(BlackPen, i*32 + 1, 1, i*32 + 1, 32*64 + 1);
	}
}

//-----------------------------------------------------------------------------------------------------------------
//选择某一个中区
//
System::Void TileEditDialog::TilePanlMouseDown(System::Object*  sender, System::Windows::Forms::MouseEventArgs*  e) 
{
	this->m_CurrentSelectedX = e->X / 32;
	this->m_CurrentSelectedY = e->Y / 32;

	char Buffer[64];
	NiSprintf(Buffer, sizeof(Buffer), "Current Selected %d %d", this->m_CurrentSelectedX, this->m_CurrentSelectedY);

	this->m_SelectedPosLable->Text = Buffer;
	this->m_TilePanel->Invalidate();
}

//-----------------------------------------------------------------------------------------------------------------
//创建中区
//
System::Void TileEditDialog::OnCreateTile(System::Object*  sender, System::EventArgs*  e) 
{
	if( this->m_CurrentSelectedX < 0 
		|| this->m_CurrentSelectedX >= TILE_COUNT
		|| this->m_CurrentSelectedY < 0
		|| this->m_CurrentSelectedY >= TILE_COUNT)
	{
		::MessageBox((HWND)this->Handle.ToInt32(), "Please select a empty tile", "Error",MB_OK | MB_ICONERROR);
		return;
	}

	if( GetTileFlag(this->m_CurrentSelectedX, this->m_CurrentSelectedY) )
	{
		::MessageBox((HWND)this->Handle.ToInt32(), "Tile already exist", "Error",MB_OK | MB_ICONERROR);
		return;
	}

	CMap* pkMap = CMap::Get();

	//当前场景中没有Map, 增加一个
	if( pkMap == NULL )
	{
		pkMap = NiNew CMap;
		pkMap->Transport("New Map");
	}

	NIASSERT(pkMap);

	if( pkMap->CreateTile(this->m_CurrentSelectedX, this->m_CurrentSelectedY) )
	{
		SetTileFlag(this->m_CurrentSelectedX, this->m_CurrentSelectedY, 1);

		//跳转到当前区
		OnGoto(sender, e); 
	}
}

//-----------------------------------------------------------------------------------------------------------------
//删除中区
//
System::Void TileEditDialog::OnDeleteTile(System::Object*  sender, System::EventArgs*  e) 
{
	if( this->m_CurrentSelectedX < 0 
		|| this->m_CurrentSelectedX >= TILE_COUNT
		|| this->m_CurrentSelectedY < 0
		|| this->m_CurrentSelectedY >= TILE_COUNT)
	{
		::MessageBox((HWND)this->Handle.ToInt32(), "Please select a tile", "Error",MB_OK | MB_ICONERROR);
		return;
	}

	if( GetTileFlag(this->m_CurrentSelectedX, this->m_CurrentSelectedY) == 0 )
	{
		::MessageBox((HWND)this->Handle.ToInt32(), "Tile not exist", "Error",MB_OK | MB_ICONERROR);
		return;
	}

	CMap* pkMap = CMap::Get();
	NIASSERT(pkMap);

	if( pkMap->GetCurrentTile()->GetX() == this->m_CurrentSelectedX
		&& pkMap->GetCurrentTile()->GetY() == this->m_CurrentSelectedY )
	{
		::MessageBox((HWND)this->Handle.ToInt32(), "Can not delete the current tile", "Error",MB_OK | MB_ICONERROR);
		return;
	}

	if( pkMap->DeleteTile(this->m_CurrentSelectedX, this->m_CurrentSelectedY) )
	{
		MEntity* pmCamera = NULL;

		if( MFramework::Instance->ViewportManager->ActiveViewport )
			pmCamera = MFramework::Instance->ViewportManager->ActiveViewport->CameraEntity;

		MPoint3* pmPos;
		if( pmCamera )
			pmPos = static_cast<MPoint3*>(pmCamera->GetPropertyData("Translation", 0));

		MScene* pmScene = MFramework::Instance->SceneFactory->Get(pkMap->GetScene());
		MFramework::Instance->InitNewScene(pmScene);

		UpdateCamera(NiPoint3(pmPos->X, pmPos->Y, pmPos->Z));

		SetTileFlag(this->m_CurrentSelectedX, this->m_CurrentSelectedY, 0);
	}
}

//-----------------------------------------------------------------------------------------------------------------
//导入地形数据
//
System::Void TileEditDialog::OnImport(System::Object*  sender, System::EventArgs*  e) 
{
	if( GetTileFlag(this->m_CurrentSelectedX, this->m_CurrentSelectedY) == 0 )
	{
		::MessageBox((HWND)this->Handle.ToInt32(), "Please select a tile", "Error",MB_OK | MB_ICONERROR);
		return;
	}

	char acPathName[1024];
	if( !TerrainPluginHelper::GetLoadPathName(acPathName, "Terrain File(*.ter)|*.ter") )
		return;

	NiFile* pkFile = NiFile::GetFile(acPathName, NiFile::READ_ONLY);
	if( !pkFile || !(*pkFile) )
	{
		NiDelete pkFile;
		return;
	}

	//Check the terrain file format
	unsigned char acSign[4];
	unsigned int uiVersion;
	pkFile->Read(acSign, sizeof(acSign));
	pkFile->Read(&uiVersion, sizeof(uiVersion));

	if( acSign[0] != 'T' 
		|| acSign[1] != 'E' 
		|| acSign[2] != 'R' 
		|| acSign[3] != '\0' 
		|| uiVersion != 1 )
	{
		::MessageBox((HWND)this->Handle.ToInt32(), "Error terrain file format", "ERROR", MB_OK | MB_ICONERROR);
		return;
	}

	CMap* pkMap = CMap::Get();
	NIASSERT(pkMap);

	//Import the terrain data

	int iXCount;
	int iYCount;
	pkFile->Read(&iXCount, sizeof(iXCount));
	pkFile->Read(&iYCount, sizeof(iYCount));
	int iXStart = this->m_CurrentSelectedX*TILE_SIZE;
	int iYStart = this->m_CurrentSelectedY*TILE_SIZE;
	float fHeight;

	NiPoint3* pkUndoPoints = NiNew NiPoint3[iXCount*iYCount];
	NiPoint3* pkRedoPoints = NiNew NiPoint3[iXCount*iYCount];
	NiPoint3 kPoint;

	for( int iY = 0; iY < iYCount; iY++ )
	{
		for( int iX = 0; iX < iXCount; iX++ )
		{
			pkFile->Read(&fHeight, sizeof(fHeight));

			kPoint.x = float(iXStart + iX*UNIT_SIZE);
			kPoint.y = float(iYStart + iY*UNIT_SIZE);
			kPoint.z = pkMap->GetHeight(kPoint.x, kPoint.y);
			pkUndoPoints[iY*iXCount + iX] = kPoint;

			kPoint.z = fHeight;
			pkRedoPoints[iY*iXCount + iX] = kPoint;

			//pkMap->SetHeight(kPoint.x, kPoint.y, fHeight);
		}
	}

	NiDelete pkFile;

	//Execute Command
	ICommandService* CommandService = MGetService(ICommandService);
	CommandService->ExecuteCommand((new MTerrainEditCommand(NiNew ChangeHeightCommand(pkUndoPoints, pkRedoPoints, iXCount*iYCount))), true);
}

//-----------------------------------------------------------------------------------------------------------------
//跳转到某一中区
//
System::Void TileEditDialog::OnGoto(System::Object*  sender, System::EventArgs*  e) 
{
	if( GetTileFlag(this->m_CurrentSelectedX, this->m_CurrentSelectedY) == 0 )
	{
		::MessageBox((HWND)this->Handle.ToInt32(), "Please select a tile", "Error",MB_OK | MB_ICONERROR);
		return;
	}

	float x = float(this->m_CurrentSelectedX*TILE_SIZE + TILE_SIZE / 2);
	float y = float(this->m_CurrentSelectedY*TILE_SIZE + TILE_SIZE / 2); 
	Goto(x, y);
	ChangeLastTileSettings(x, y);

	this->m_TilePanel->Invalidate();
}

void TileEditDialog::Goto(float x, float y)
{
	CMap* pkMap = CMap::Get();
	if( pkMap )
	{
		if( pkMap->EnterTile(x, y) )
		{
			MScene* pmScene = MFramework::Instance->SceneFactory->Get(pkMap->GetScene());
			MFramework::Instance->InitNewScene(pmScene);

			//更新当前的贴图
			PaintMaterialDialog::Get()->UpdateTextureControls();

			NiPoint3 kCameraPos;
			kCameraPos.x = x;
			kCameraPos.y = y;
			kCameraPos.z = pkMap->GetHeight(x, y) + 50.f;

			//更新摄像机的位置
			UpdateCamera(kCameraPos);
		}

        MUtility::AddErrorInterfaceMessages(MessageChannelType::Errors, pkMap->GetErrorHandler());
        pkMap->GetErrorHandler()->ClearErrors();
	}
}

System::Void TileEditDialog::OnSaveTile(System::Object*  sender, System::EventArgs*  e)
{
	if( this->m_CurrentSelectedX < 0 
		|| this->m_CurrentSelectedX >= TILE_COUNT
		|| this->m_CurrentSelectedY < 0
		|| this->m_CurrentSelectedY >= TILE_COUNT)
	{
		::MessageBox((HWND)this->Handle.ToInt32(), "Please select a tile", "Error",MB_OK | MB_ICONERROR);
		return;
	}

	if( !GetTileFlag(this->m_CurrentSelectedX, this->m_CurrentSelectedY) )
	{
		::MessageBox((HWND)this->Handle.ToInt32(), "Tile dos't exist", "Error",MB_OK | MB_ICONERROR);
		return;
	}

	CMap* pkMap = CMap::Get();
	if(pkMap == NULL)
		return;

	CTile* pkTile = pkMap->GetTileByIndex(this->m_CurrentSelectedX, this->m_CurrentSelectedY);
	if(pkTile == NULL)
		return;

	pkTile->SetDirty(true);
	pkTile->Save(pkMap->GetMapName());
}

void TileEditDialog::UpdateCamera(const NiPoint3& kCameraPos)
{
	MViewportManager* pmViewportManager = MFramework::Instance->ViewportManager;

	MViewport* pmViewport;
	MEntity* pmCamera;
	for( unsigned int i = 0; i < pmViewportManager->ViewportCount; i++ )
	{
		pmViewport = pmViewportManager->GetViewport(i);
		pmCamera = pmViewport->CameraEntity;
		if( pmCamera != NULL )
		{
			pmCamera->SetPropertyData("Translation", new MPoint3(kCameraPos), false);
		}
	}
}		

//----------------------------------------------------------------------------------------------
//记录场景编辑器最后的位置
//
void TileEditDialog::RegisterSettings()
{
	// Register setting.
	ISettingsService* pmSettingsService = MGetService(ISettingsService);
	MAssert(pmSettingsService != NULL, "Settings service not found!");

	pmSettingsService->RegisterSettingsObject("Last Tile", new MPoint2(0, 0), SettingsCategory::PerScene);
	pmSettingsService->SetChangedSettingHandler("Last Tile", SettingsCategory::PerScene,
		new SettingChangedHandler(NULL, &TileEditDialog::LastTileSettingsChangedHandler));
}

void TileEditDialog::LastTileSettingsChangedHandler(Object* pmSender, SettingChangedEventArgs* pmEventArgs)
{
	if (pmEventArgs->Name->Equals("Last Tile") && pmEventArgs->Category == SettingsCategory::PerScene)
	{
		ISettingsService* pmSettingsService = MGetService(ISettingsService);
		MAssert(pmSettingsService != NULL, "Settings service not found!");

		MPoint2* pmLastTile = dynamic_cast<MPoint2*>(pmSettingsService->GetSettingsObject("Last Tile", SettingsCategory::PerScene));
		MAssert(pmLastTile != NULL, "Settings value is empty!");

		Goto(pmLastTile->X, pmLastTile->Y);
	}
}

//改变设置
void TileEditDialog::ChangeLastTileSettings(float x, float y)
{
	ISettingsService* pmSettingsService = MGetService(ISettingsService);
	MAssert(pmSettingsService != NULL, "Settings service not found!");

	MPoint2* pmLastTile = dynamic_cast<MPoint2*>(pmSettingsService->GetSettingsObject("Last Tile", SettingsCategory::PerScene));
	if( pmLastTile->X == x && pmLastTile->Y == y )
		return;

	pmLastTile = new MPoint2(x, y);
	pmSettingsService->SetSettingsObject("Last Tile", pmLastTile, SettingsCategory::PerScene);
}

System::Void TileEditDialog::OnVisible(System::Object*  sender, System::EventArgs*  e) 
{
	CTile* pkTile = CMap::Get() ? CMap::Get()->GetCurrentTile() : NULL;

	if( pkTile && this->Visible )
	{
		this->m_ScrollPanel->AutoScroll = true;
		this->m_ScrollPanel->AutoScrollPosition = System::Drawing::Point((pkTile->GetX() - 6)*32, (pkTile->GetY() - 6)*32);
	}
}

System::Void TileEditDialog::TilePanelMouseMove(System::Object*  sender, System::Windows::Forms::MouseEventArgs*  e) 
{
	char Buffer[64];
	NiSprintf(Buffer, sizeof(Buffer), "Mouse %d %d", e->X / 32, e->Y / 32);

	this->m_MousePosLable->Text = Buffer;
}