
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "PaintMaterialDialog.h"
#include "Command/ChangeHeightCommand.h"
#include "Command/MTerrainEditCommand.h"
#include "TerrainPluginHelper.h"

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
    namespace TerrainPlugin {
    public __gc class TileEditDialog : public System::Windows::Forms::Form
    {
    public: 
        TileEditDialog(void);
		void SetTileFlags(const int* piFlags);
		void SetTileFlag(int x, int y, int iFlag);
		int GetTileFlag(int x, int y);
        
    protected: 
        void Dispose(Boolean disposing);

	private: System::Windows::Forms::Panel*  m_ScrollPanel;
	private: System::Windows::Forms::Panel*  m_TilePanel;
	private: int* m_TileFlags;
	private: System::Windows::Forms::Label*  m_MousePosLable;
	private: System::Windows::Forms::Label*  m_SelectedPosLable;


	private: System::Windows::Forms::Button*  m_NewTileBtn;
	private: System::Windows::Forms::Button*  m_DeleteTileBtn;
	private: System::Windows::Forms::Button*  m_ImportBtn;

    
	private: int m_CurrentSelectedX;
    private: int m_CurrentSelectedY;
	private: System::Windows::Forms::Button*  m_GotoTileBtn;
	private: System::Windows::Forms::Button*  m_BtnSave;




    private:
        System::ComponentModel::Container* components;
        void InitializeComponent(void);

		//-----------------------------------------------------------------------------------------------------------------
		//显示
		//
		System::Void PaintTiles(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e);

		//-----------------------------------------------------------------------------------------------------------------
		//
		//
		System::Void TilePanelMouseMove(System::Object*  sender, System::Windows::Forms::MouseEventArgs*  e);

		//-----------------------------------------------------------------------------------------------------------------
		//选择某一个中区
		//
		System::Void TilePanlMouseDown(System::Object*  sender, System::Windows::Forms::MouseEventArgs*  e);

		//-----------------------------------------------------------------------------------------------------------------
		//创建中区
		//
		System::Void OnCreateTile(System::Object*  sender, System::EventArgs*  e);
		
		//-----------------------------------------------------------------------------------------------------------------
		//删除中区
		//
		System::Void OnDeleteTile(System::Object*  sender, System::EventArgs*  e);

		//-----------------------------------------------------------------------------------------------------------------
		//导入地形数据
		//
		System::Void OnImport(System::Object*  sender, System::EventArgs*  e);

		//-----------------------------------------------------------------------------------------------------------------
		//跳转到某一中区
		//
		System::Void OnGoto(System::Object*  sender, System::EventArgs*  e);

		//------------------------------------------------------------------------------------------------------------------
		//重新保存当前中区
		//
		System::Void OnSaveTile(System::Object*  sender, System::EventArgs*  e);

		static void UpdateCamera(const NiPoint3& kCameraPos);
		
public:
		//----------------------------------------------------------------------------------------------
		//记录场景编辑器最后的位置
		//
		static void RegisterSettings();

		static void LastTileSettingsChangedHandler(Object* pmSender, SettingChangedEventArgs* pmEventArgs);
		static void Goto(float x, float y);

		//改变设置
		static void ChangeLastTileSettings(float x, float y);
		private: System::Void OnVisible(System::Object*  sender, System::EventArgs*  e);
	};
} }}}