#include "TerrainPluginPCH.h"

#include "MDayNightColorEditor.h"
#include "MDayNightColorEditorDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

//---------------------------------------------------------------------------
UITypeEditorEditStyle MDayNightColorEditor::GetEditStyle(ITypeDescriptorContext* pmContext)
{
    return UITypeEditorEditStyle::Modal;
}

//---------------------------------------------------------------------------
Object* MDayNightColorEditor::EditValue(ITypeDescriptorContext* pmContext, System::IServiceProvider* pmProvider, Object* pmValue)
{
	Object* pmResult = pmValue;
    if (pmValue->GetType() == __typeof(String))
    {
        String* strValue = static_cast<String*>(pmValue);

        DayNightColorEditorDialog* pmDialog = new DayNightColorEditorDialog();
		pmDialog->Text = pmContext->PropertyDescriptor->Description;
		
		pmDialog->InitDayNightColor(strValue);

        if ( pmDialog->ShowDialog() == DialogResult::OK )
        {
			pmResult = pmDialog->GetDayNightColor();
        }

		pmDialog->DestroyDayNightColor();
    }

    return pmResult;
}

bool MDayNightColorEditor::GetPaintValueSupported(ITypeDescriptorContext* pmContext)
{
	return true;
}

void MDayNightColorEditor::PaintValue(PaintValueEventArgs* pe)
{
	String* strValue = static_cast<String*>(pe->Value);

	NiColor kColor;
	{
		const char* pcColorValue = MStringToCharPointer(strValue);

		CDayNightColor kDayNightColor;
		kDayNightColor.FromString(pcColorValue);
		kColor = kDayNightColor.GetColor(12, 0);

		MFreeCharPointer(pcColorValue);
	}

	SolidBrush* pmBrush = new SolidBrush(Color::FromArgb(255, int(kColor.r*255), int(kColor.g*255), int(kColor.b*255)));
	pe->Graphics->FillRectangle(pmBrush, pe->Bounds);
}