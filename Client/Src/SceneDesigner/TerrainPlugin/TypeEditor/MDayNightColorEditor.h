
using namespace System::Drawing::Design;
using namespace System::ComponentModel;
using namespace System::Drawing;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{
    public __gc class MDayNightColorEditor : public UITypeEditor
    {
    public:
        virtual UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext* pmContext);
        virtual Object* EditValue(ITypeDescriptorContext* pmContext, System::IServiceProvider* pmProvider, Object* pmValue);
		
		virtual bool GetPaintValueSupported(ITypeDescriptorContext* pmContext);
		virtual void PaintValue(PaintValueEventArgs* pe);
    };

	public __gc class MDayNightColorEditorConverter : public StringConverter
    {
    public:
		virtual bool GetStandardValuesExclusive(ITypeDescriptorContext* pmContext) { return true; }
    };

}}}}
