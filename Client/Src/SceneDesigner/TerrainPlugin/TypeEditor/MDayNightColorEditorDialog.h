
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
	namespace TerrainPlugin {

		public __gc class DayNightColorEditorDialog : public System::Windows::Forms::Form
		{
		private: System::Windows::Forms::Button*  m_OKButton;
		private: System::Windows::Forms::Button*  m_CancelButton;

		private:
			CDayNightColor* m_pkDayNightColor;

			Label* m_TimeLabels __gc [];
			Button* m_ColorButtons __gc [];
			CheckBox* m_EnableCheckBoxs __gc [];

			ToolTip* m_ColorToolTip;

		public:
			DayNightColorEditorDialog()
			{
				m_pkDayNightColor = NULL;
				this->InitializeComponent();
			}

			void Do_Dispose(bool bDisposing)
			{
			}

			void InitDayNightColor(String* strColorValue)
			{
				const char* pcColorValue = MStringToCharPointer(strColorValue);

				m_pkDayNightColor = NiNew CDayNightColor;
				m_pkDayNightColor->FromString(pcColorValue);

				MFreeCharPointer(pcColorValue);

				m_ColorToolTip = new ToolTip();
				m_ColorToolTip->InitialDelay = 200;
				m_ColorToolTip->ReshowDelay = 100;
				m_ColorToolTip->AutoPopDelay = 5000;
				m_ColorToolTip->ShowAlways = true;

				InitTimeLables();
				InitColorButtons();
				InitEnableCheckBoxs();

				ResetControls();
			}

			void DestroyDayNightColor()
			{
				NiDelete m_pkDayNightColor;

				DestroyEnableCheckBoxs();
				DestroyColorButtons();
				DestroyTimeLables();
			}

			String* GetDayNightColor()
			{
				NIASSERT(m_pkDayNightColor);
				return new String((const char*)m_pkDayNightColor->ToString());
			}

			void InitTimeLables()
			{
				this->SuspendLayout();

				m_TimeLabels = new Label* __gc [NUM_DAY_NIGHT_COLOR];
				for( int i = 0; i < m_TimeLabels->Length; ++i )
				{
					m_TimeLabels[i] = new Label();

					m_TimeLabels[i]->AutoSize = true;
					m_TimeLabels[i]->Location = System::Drawing::Point(14 + i * 25, 17);
					m_TimeLabels[i]->Size = System::Drawing::Size(11, 12);
					m_TimeLabels[i]->Text = i.ToString();

					this->Controls->Add(m_TimeLabels[i]);
				}

				this->ResumeLayout(false);
				this->PerformLayout();
			}

			void DestroyTimeLables()
			{
				for( int i = 0; i < m_TimeLabels->Length; ++i )
				{
					this->Controls->Remove(m_TimeLabels[i]);
				}
			}

			void InitColorButtons()
			{
				this->SuspendLayout();

				m_ColorButtons = new Button* __gc [NUM_DAY_NIGHT_COLOR];
				for( int i = 0; i < m_ColorButtons->Length; ++i )
				{
					m_ColorButtons[i] = new Button();

					m_ColorButtons[i]->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
					m_ColorButtons[i]->Location = System::Drawing::Point(10 + i * 25, 39);
					m_ColorButtons[i]->Size = System::Drawing::Size(19, 23);
					m_ColorButtons[i]->Tag = i.ToString();
					m_ColorButtons[i]->UseVisualStyleBackColor = true;
					m_ColorButtons[i]->Click += new System::EventHandler(this, &DayNightColorEditorDialog::OnColorButtonClick);
					m_ColorButtons[i]->MouseHover += new System::EventHandler(this, &DayNightColorEditorDialog::OnColorButtonMouseHover);

					this->Controls->Add(m_ColorButtons[i]);
				}

				this->ResumeLayout(false);
				this->PerformLayout();
			}

			void DestroyColorButtons()
			{
				for( int i = 0; i < m_ColorButtons->Length; ++i )
				{
					this->Controls->Remove(m_ColorButtons[i]);
				}
			}

			void InitEnableCheckBoxs()
			{
				this->SuspendLayout();

				m_EnableCheckBoxs = new CheckBox* __gc [NUM_DAY_NIGHT_COLOR];
				for( int i = 0; i < m_EnableCheckBoxs->Length; ++i )
				{
					m_EnableCheckBoxs[i] = new CheckBox();

					m_EnableCheckBoxs[i]->AutoSize = true;
					m_EnableCheckBoxs[i]->Location = System::Drawing::Point(12 + i * 25, 72);
					m_EnableCheckBoxs[i]->Size = System::Drawing::Size(15, 14);
					m_EnableCheckBoxs[i]->Tag = i.ToString();
					m_EnableCheckBoxs[i]->UseVisualStyleBackColor = true;
					m_EnableCheckBoxs[i]->Click += new System::EventHandler(this, &DayNightColorEditorDialog::OnEnableCheckBoxClick);

					this->Controls->Add(m_EnableCheckBoxs[i]);
				}

				this->ResumeLayout(false);
				this->PerformLayout();
			}

			void DestroyEnableCheckBoxs()
			{
				for( int i = 0; i < m_EnableCheckBoxs->Length; ++i )
				{
					this->Controls->Remove(m_EnableCheckBoxs[i]);
				}
			}

		private: 
			System::Void InitializeComponent() 
			{
				this->m_OKButton = (new System::Windows::Forms::Button());
				this->m_CancelButton = (new System::Windows::Forms::Button());
				this->SuspendLayout();
				// 
				// m_OKButton
				// 
				this->m_OKButton->DialogResult = System::Windows::Forms::DialogResult::OK;
				this->m_OKButton->Location = System::Drawing::Point(176, 123);
				this->m_OKButton->Name = S"m_OKButton";
				this->m_OKButton->Size = System::Drawing::Size(75, 23);
				this->m_OKButton->TabIndex = 0;
				this->m_OKButton->Text = S"ȷ��";
				this->m_OKButton->UseVisualStyleBackColor = true;
				// 
				// m_CancelButton
				// 
				this->m_CancelButton->DialogResult = System::Windows::Forms::DialogResult::Cancel;
				this->m_CancelButton->Location = System::Drawing::Point(366, 123);
				this->m_CancelButton->Name = S"m_CancelButton";
				this->m_CancelButton->Size = System::Drawing::Size(75, 23);
				this->m_CancelButton->TabIndex = 1;
				this->m_CancelButton->Text = S"ȡ��";
				this->m_CancelButton->UseVisualStyleBackColor = true;
				// 
				// DayNightColorEditorDialog
				// 
				this->ClientSize = System::Drawing::Size(629, 175);
				this->Controls->Add(this->m_CancelButton);
				this->Controls->Add(this->m_OKButton);
				this->Name = S"DayNightColorEditorDialog";
				this->ResumeLayout(false);

			}

			System::Void DayNightColorEditorDialog::OnColorButtonClick(System::Object*  sender, System::EventArgs*  e)
			{
				Button* pmButton = static_cast<Button*>(sender);
				if( pmButton == NULL )
					return;

				int iHour = pmButton->Tag->ToString()->ToInt32(NULL);
				NIASSERT( iHour >= 0 && iHour <= NUM_DAY_NIGHT_COLOR );
				NiColor kColor = m_pkDayNightColor->GetColor(iHour, 0);

				ColorDialog* pmColorDialog = new ColorDialog();
				pmColorDialog->Color = Color::FromArgb(255, int(kColor.r*255), int(kColor.g*255), int(kColor.b*255));
				pmColorDialog->FullOpen = true;

				if( pmColorDialog->ShowDialog() == DialogResult::OK )
				{
					kColor.r = pmColorDialog->Color.R/255.f;
					kColor.g = pmColorDialog->Color.G/255.f;
					kColor.b = pmColorDialog->Color.B/255.f;

					m_pkDayNightColor->Enable( iHour, true );
					m_pkDayNightColor->SetColor(iHour, kColor);

					ResetControls();
					this->Invalidate();
				}
			}

			System::Void DayNightColorEditorDialog::OnEnableCheckBoxClick(System::Object*  sender, System::EventArgs*  e)
			{
				CheckBox* pmCheckBox = static_cast<CheckBox*>(sender);
				if( pmCheckBox == NULL )
					return;

				int iHour = pmCheckBox->Tag->ToString()->ToInt32(NULL);
				NIASSERT( iHour >= 0 && iHour <= NUM_DAY_NIGHT_COLOR );

				m_pkDayNightColor->Enable( iHour, pmCheckBox->Checked );

				ResetControls();
				this->Invalidate();
			}

			System::Void DayNightColorEditorDialog::OnColorButtonMouseHover(System::Object*  sender, System::EventArgs*  e)
			{
				Button* pmButton = static_cast<Button*>(sender);
				if( pmButton == NULL )
					return;

				m_ColorToolTip->RemoveAll();
				m_ColorToolTip->Show(pmButton->BackColor.ToString(), pmButton, 1000 * 20);
			}

			void ResetControls()
			{
				for( int i = 0; i < m_ColorButtons->Length; ++i )
				{
					NiColor kColor = m_pkDayNightColor->GetColor(i, 0);
					m_ColorButtons[i]->BackColor = Color::FromArgb(255, int(kColor.r*255), int(kColor.g*255), int(kColor.b*255));
					m_EnableCheckBoxs[i]->Checked = m_pkDayNightColor->IsEnable(i);
				}
			}
		};
   }
}}}