
// Precompiled Header
#include "TerrainPluginPCH.h"
#include "MEntityTypeConverter.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;
using namespace System::Collections;

//---------------------------------------------------------------------------
bool MEntityTypeConverter::GetStandardValuesSupported(ITypeDescriptorContext* pmContext)
{
    return true;
}
//---------------------------------------------------------------------------
bool MEntityTypeConverter::GetStandardValuesExclusive(ITypeDescriptorContext* pmContext)
{
    return true;
}
//---------------------------------------------------------------------------
TypeConverter::StandardValuesCollection* MEntityTypeConverter::GetStandardValues(ITypeDescriptorContext* pmContext)
{
    ArrayList* pmValues = new ArrayList();
    pmValues->Add(new String(CEntityInfoComponent::ms_kModelName));
    pmValues->Add(new String(CEntityInfoComponent::ms_kTransportName));

    return new StandardValuesCollection(pmValues);
}
//---------------------------------------------------------------------------
