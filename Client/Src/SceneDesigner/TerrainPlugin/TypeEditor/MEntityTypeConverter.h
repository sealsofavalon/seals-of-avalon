
#pragma once

using namespace System::ComponentModel;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{
    public __gc class MEntityTypeConverter : public StringConverter
    {
    public:
        virtual bool GetStandardValuesSupported(ITypeDescriptorContext* pmContext);
        virtual bool GetStandardValuesExclusive(ITypeDescriptorContext* pmContext);
        virtual StandardValuesCollection* GetStandardValues(ITypeDescriptorContext* pmContext);
    };
}}}}
