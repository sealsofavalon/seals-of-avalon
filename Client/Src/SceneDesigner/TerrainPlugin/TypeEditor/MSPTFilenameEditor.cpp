#include "TerrainPluginPCH.h"

#include "MSPTFilenameEditor.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

//---------------------------------------------------------------------------
String* MSPTFilenameEditor::GetDialogTitle()
{
    return "Choose SPT File";
}
//---------------------------------------------------------------------------
String* MSPTFilenameEditor::GetDialogFilter()
{
    return "SPT Files (*.spt)|*.spt";
}
//---------------------------------------------------------------------------
