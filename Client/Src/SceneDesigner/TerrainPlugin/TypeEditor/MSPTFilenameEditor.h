#pragma once

using namespace Emergent::Gamebryo::SceneDesigner::StdPluginsCpp;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{
    public __gc class MSPTFilenameEditor : public MFilenameEditor
    {
    protected:
        virtual String* GetDialogTitle();
        virtual String* GetDialogFilter();
    };
}}}}
