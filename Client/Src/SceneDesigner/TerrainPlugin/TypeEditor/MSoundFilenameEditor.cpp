#include "TerrainPluginPCH.h"

#include "MSoundFilenameEditor.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

//---------------------------------------------------------------------------
String* MSoundFilenameEditor::GetDialogTitle()
{
    return "Choose Sound File";
}
//---------------------------------------------------------------------------
String* MSoundFilenameEditor::GetDialogFilter()
{
    return "Sound Files (*.wav)|*.wav";
}
//---------------------------------------------------------------------------
