#pragma once

using namespace Emergent::Gamebryo::SceneDesigner::StdPluginsCpp;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace TerrainPlugin
{
    public __gc class MSoundFilenameEditor : public MFilenameEditor
    {
    protected:
        virtual String* GetDialogTitle();
        virtual String* GetDialogFilter();
    };
}}}}
