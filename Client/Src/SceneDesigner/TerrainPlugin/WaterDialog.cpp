
#include "TerrainPluginPCH.h"
#include "WaterDialog.h"
#include "TerrainPluginHelper.h"

using namespace Emergent::Gamebryo::SceneDesigner::TerrainPlugin;

void WaterControl::Init(WaterDialog* dlg, int id, int x, int y)
{
	m_ParentDlg = dlg;
	m_iID = id;
	m_pkPathName = NiNew NiFixedString;
	m_Bitmap = NULL;

	this->m_WaterPictureBox = (new System::Windows::Forms::PictureBox());
	this->m_ChangeBtn = (new System::Windows::Forms::Button());
	this->m_DeleteBtn = (new System::Windows::Forms::Button());
	this->m_TransCheckBox = (new System::Windows::Forms::CheckBox());
	this->m_AdditiveCheckBox = (new System::Windows::Forms::CheckBox());
	this->m_RRCheckBox = (new System::Windows::Forms::CheckBox());
	this->m_WaterGroupBox = (new System::Windows::Forms::GroupBox());
	this->m_TransTrackBar = (new System::Windows::Forms::TrackBar());
	this->m_SpeedTrackBar = (new System::Windows::Forms::TrackBar());
	this->m_UVScaleTrackBar = (new System::Windows::Forms::TrackBar());
	this->m_TransLable = (new System::Windows::Forms::Label());
	this->m_SpeedLable = (new System::Windows::Forms::Label());
	this->m_UVScaleLabel = (new System::Windows::Forms::Label());
	(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_WaterPictureBox))->BeginInit();
	(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_TransTrackBar))->BeginInit();
	(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_SpeedTrackBar))->BeginInit();
	(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_UVScaleTrackBar))->BeginInit();
	this->m_WaterGroupBox->SuspendLayout();
	// 
	// m_WaterPictureBox
	// 
	this->m_WaterPictureBox->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
	this->m_WaterPictureBox->Location = System::Drawing::Point(13, 157);
	this->m_WaterPictureBox->Name = S"m_WaterPictureBox";
	this->m_WaterPictureBox->Size = System::Drawing::Size(128, 128);
	this->m_WaterPictureBox->TabIndex = 0;
	this->m_WaterPictureBox->TabStop = false;
	this->m_WaterPictureBox->Paint += new System::Windows::Forms::PaintEventHandler(this, &WaterControl::OnPictureBoxPaint);
	this->m_WaterPictureBox->Click += new System::EventHandler(this, &WaterControl::OnPictureBoxClick);
	this->m_WaterPictureBox->MouseHover += new System::EventHandler(this, &WaterControl::OnPictureBoxMouseHover);
	// 
	// m_ChangeBtn
	// 
	this->m_ChangeBtn->Location = System::Drawing::Point(155, 234);
	this->m_ChangeBtn->Name = S"m_ChangeBtn";
	this->m_ChangeBtn->Size = System::Drawing::Size(51, 23);
	this->m_ChangeBtn->TabIndex = 1;
	this->m_ChangeBtn->Text = S"更改";
	this->m_ChangeBtn->UseVisualStyleBackColor = true;
	this->m_ChangeBtn->Click += new System::EventHandler(this, &WaterControl::OnChangeBtnClick);
	// 
	// m_DeleteBtn
	// 
	this->m_DeleteBtn->Location = System::Drawing::Point(155, 263);
	this->m_DeleteBtn->Name = S"m_DeleteBtn";
	this->m_DeleteBtn->Size = System::Drawing::Size(54, 23);
	this->m_DeleteBtn->TabIndex = 2;
	this->m_DeleteBtn->Text = S"删除";
	this->m_DeleteBtn->UseVisualStyleBackColor = true;
	this->m_DeleteBtn->Click += new System::EventHandler(this, &WaterControl::OnDeleteBtnClick);
	// 
	// m_TransCheckBox
	// 
	this->m_TransCheckBox->AutoSize = true;
	this->m_TransCheckBox->Location = System::Drawing::Point(155, 163);
	this->m_TransCheckBox->Name = S"m_TransCheckBox";
	this->m_TransCheckBox->Size = System::Drawing::Size(60, 16);
	this->m_TransCheckBox->TabIndex = 4;
	this->m_TransCheckBox->Text = S"半透明";
	this->m_TransCheckBox->UseVisualStyleBackColor = true;
	this->m_TransCheckBox->CheckedChanged += new System::EventHandler(this, &WaterControl::OnTransCheckedChanged);
	// 
	// m_AdditiveCheckBox
	// 
	this->m_AdditiveCheckBox->AutoSize = true;
	this->m_AdditiveCheckBox->Location = System::Drawing::Point(155, 188);
	this->m_AdditiveCheckBox->Name = S"m_AdditiveCheckBox";
	this->m_AdditiveCheckBox->Size = System::Drawing::Size(108, 16);
	this->m_AdditiveCheckBox->TabIndex = 5;
	this->m_AdditiveCheckBox->Text = S"光照和贴图相加";
	this->m_AdditiveCheckBox->UseVisualStyleBackColor = true;
	this->m_AdditiveCheckBox->CheckedChanged += new System::EventHandler(this, &WaterControl::OnAdditiveCheckedChanged);
	// 
	// m_RRCheckBox
	// 
	this->m_RRCheckBox->AutoSize = true;
	this->m_RRCheckBox->Location = System::Drawing::Point(155, 212);
	this->m_RRCheckBox->Name = S"m_RRCheckBox";
	this->m_RRCheckBox->Size = System::Drawing::Size(108, 16);
	this->m_RRCheckBox->TabIndex = 6;
	this->m_RRCheckBox->Text = S"允许反射，折射";
	this->m_RRCheckBox->UseVisualStyleBackColor = true;
	this->m_RRCheckBox->CheckedChanged += new System::EventHandler(this, &WaterControl::OnRRCheckedChanged);
	// 
	// m_WaterGroupBox
	// 
	this->m_WaterGroupBox->Controls->Add(this->m_UVScaleLabel);
	this->m_WaterGroupBox->Controls->Add(this->m_SpeedLable);
	this->m_WaterGroupBox->Controls->Add(this->m_TransLable);
	this->m_WaterGroupBox->Controls->Add(this->m_UVScaleTrackBar);
	this->m_WaterGroupBox->Controls->Add(this->m_SpeedTrackBar);
	this->m_WaterGroupBox->Controls->Add(this->m_TransTrackBar);
	this->m_WaterGroupBox->Controls->Add(this->m_WaterPictureBox);
	this->m_WaterGroupBox->Controls->Add(this->m_RRCheckBox);
	this->m_WaterGroupBox->Controls->Add(this->m_ChangeBtn);
	this->m_WaterGroupBox->Controls->Add(this->m_AdditiveCheckBox);
	this->m_WaterGroupBox->Controls->Add(this->m_DeleteBtn);
	this->m_WaterGroupBox->Controls->Add(this->m_TransCheckBox);
	this->m_WaterGroupBox->Location = System::Drawing::Point(x, y);
	this->m_WaterGroupBox->Name = S"m_WaterGroupBox";
	this->m_WaterGroupBox->Size = System::Drawing::Size(286, 306);
	this->m_WaterGroupBox->TabIndex = 7;
	this->m_WaterGroupBox->TabStop = false;
	this->m_WaterGroupBox->Text = String::Format("Water {0}", __box(m_iID + 1));
	// 
	// m_TransTrackBar
	// 
	this->m_TransTrackBar->Location = System::Drawing::Point(74, 10);
	this->m_TransTrackBar->Maximum = 20;
	this->m_TransTrackBar->Minimum = 1;
	this->m_TransTrackBar->Name = S"m_TransTrackBar";
	this->m_TransTrackBar->Size = System::Drawing::Size(209, 45);
	this->m_TransTrackBar->TabIndex = 7;
	this->m_TransTrackBar->Value = 20;
	this->m_TransTrackBar->LargeChange = 1;
	this->m_TransTrackBar->ValueChanged += new System::EventHandler(this, &WaterControl::OnTransValueChanged);
	// 
	// m_SpeedTrackBar
	// 
	this->m_SpeedTrackBar->Location = System::Drawing::Point(74, 55);
	this->m_SpeedTrackBar->Maximum = 20;
	this->m_SpeedTrackBar->Minimum = 1;
	this->m_SpeedTrackBar->Name = S"m_SpeedTrackBar";
	this->m_SpeedTrackBar->Size = System::Drawing::Size(209, 45);
	this->m_SpeedTrackBar->TabIndex = 7;
	this->m_SpeedTrackBar->Value = 20;
	this->m_SpeedTrackBar->LargeChange = 1;
	this->m_SpeedTrackBar->ValueChanged += new System::EventHandler(this, &WaterControl::OnSpeedValueChanged);
	// 
	// m_UVScaleTrackBar
	// 
	this->m_UVScaleTrackBar->Location = System::Drawing::Point(74, 100);
	this->m_UVScaleTrackBar->Maximum = 10;
	this->m_UVScaleTrackBar->Minimum = 0;
	this->m_UVScaleTrackBar->Name = S"m_UVScaleTrackBar";
	this->m_UVScaleTrackBar->Size = System::Drawing::Size(209, 45);
	this->m_UVScaleTrackBar->TabIndex = 7;
	this->m_UVScaleTrackBar->Value = 5;
	this->m_UVScaleTrackBar->LargeChange = 1;
	this->m_UVScaleTrackBar->ValueChanged += new System::EventHandler(this, &WaterControl::OnUVScaleValueChanged);
	// 
	// m_TransLable
	// 
	this->m_TransLable->AutoSize = true;
	this->m_TransLable->Location = System::Drawing::Point(14, 18);
	this->m_TransLable->Name = S"m_TransLable";
	this->m_TransLable->Size = System::Drawing::Size(41, 12);
	this->m_TransLable->TabIndex = 8;
	this->m_TransLable->Text = S"透明度";
	// 
	// m_SpeedLable
	// 
	this->m_SpeedLable->AutoSize = true;
	this->m_SpeedLable->Location = System::Drawing::Point(14, 63);
	this->m_SpeedLable->Name = S"m_SpeedLable";
	this->m_SpeedLable->Size = System::Drawing::Size(29, 12);
	this->m_SpeedLable->TabIndex = 8;
	this->m_SpeedLable->Text = S"速度";
	// 
	// m_UVScaleLabel
	// 
	this->m_UVScaleLabel->AutoSize = true;
	this->m_UVScaleLabel->Location = System::Drawing::Point(14, 108);
	this->m_UVScaleLabel->Name = S"m_UVScaleLabel";
	this->m_UVScaleLabel->Size = System::Drawing::Size(41, 12);
	this->m_UVScaleLabel->TabIndex = 8;
	this->m_UVScaleLabel->Text = S"UV缩放";
	//
	//m_ToolTip
	//
	this->m_ToolTip = (new System::Windows::Forms::ToolTip);
	this->m_ToolTip->InitialDelay = 200;
	this->m_ToolTip->ReshowDelay = 100;
	this->m_ToolTip->AutoPopDelay = 5000;
	this->m_ToolTip->ShowAlways = true;


	(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_WaterPictureBox))->EndInit();
	this->m_WaterGroupBox->ResumeLayout(false);
	this->m_WaterGroupBox->PerformLayout();
	(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_TransTrackBar))->EndInit();
	(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_SpeedTrackBar))->EndInit();
	(__try_cast<System::ComponentModel::ISupportInitialize*  >(this->m_UVScaleTrackBar))->EndInit();
}

System::Void WaterControl::OnPictureBoxPaint(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e)
{
	IntPtr hDC = e->Graphics->GetHdc();
	TerrainPluginHelper::DrawImage((HDC)hDC.ToInt32(), 0, 0, m_WaterPictureBox->Width, m_WaterPictureBox->Height, m_Bitmap); 
	e->Graphics->ReleaseHdc(hDC);

	if(m_ParentDlg->GetActiveWaterId() == m_iID)
	{
		Pen* RedPen = new Pen(Color::FromArgb(255, 255, 0, 0), 4);
		e->Graphics->DrawRectangle(RedPen, 4, 4, m_WaterPictureBox->Width - 10, m_WaterPictureBox->Height - 10);
	}
}

System::Void WaterControl::OnPictureBoxClick(System::Object*  sender, System::EventArgs*  e)
{
	if(m_Bitmap)
	{
		m_ParentDlg->SetActiveWaterId(m_iID);
		m_ParentDlg->Invalidate(true);
	}
}

System::Void WaterControl::OnPictureBoxMouseHover(System::Object*  sender, System::EventArgs*  e)
{
	if(m_Bitmap)
	{
		this->m_ToolTip->RemoveAll();
		this->m_ToolTip->Show((const char*)*m_pkPathName, this->m_WaterPictureBox, 1000 * 20);
	}
}

System::Void WaterControl::OnChangeBtnClick(System::Object*  sender, System::EventArgs*  e)
{
	CMap* pkMap = CMap::Get();
	if(!pkMap)
		return;

	char acAbsPathName[1024];
	if( !TerrainPluginHelper::GetLoadPathName(acAbsPathName, "DDS file (*.DDS)|*.DDS") )
		return;

	//转到相对路径
	char acPathName[1024];
	if( !TerrainHelper::ConvertToRelative(acPathName, 1024, acAbsPathName) )
		return;

	//去掉文件名最后面的2个数字
	NiFilename kFilename(acPathName);
	char acName[256];
	NiStrcpy(acName, sizeof(acName), kFilename.GetFilename());
	int iLen = strlen(acName);
	if(iLen < 2)
		return;

	if(acName[iLen - 1] >= '0' && acName[iLen - 1] <= '9')
	{
		acName[iLen - 1] = '\0';
		if(acName[iLen - 2] >= '0' && acName[iLen - 2] <= '9')
			acName[iLen - 2] = '\0';
	}

	//去掉扩展名
	NiSprintf(acPathName, sizeof(acPathName), "%s%s", kFilename.GetDir(), acName);

	pkMap->ChangeWaterShader(acPathName, m_iID);
	Update();
}

System::Void WaterControl::OnDeleteBtnClick(System::Object*  sender, System::EventArgs*  e)
{
	CMap* pkMap = CMap::Get();
	if(!pkMap)
		return;

	pkMap->DeleteWaterShader(m_iID);
	Update();
}

System::Void WaterControl::OnTransCheckedChanged(System::Object*  sender, System::EventArgs*  e)
{
	CMap* pkMap = CMap::Get();
	if(!pkMap)
		return;

	pkMap->SetWaterAlphaBlend(m_TransCheckBox->Checked, m_iID);
}

System::Void WaterControl::OnAdditiveCheckedChanged(System::Object*  sender, System::EventArgs*  e)
{
	CMap* pkMap = CMap::Get();
	if(!pkMap)
		return;

	pkMap->SetWaterAdditiveLight(m_AdditiveCheckBox->Checked, m_iID);
}

System::Void WaterControl::OnRRCheckedChanged(System::Object*  sender, System::EventArgs*  e)
{
	CMap* pkMap = CMap::Get();
	if(!pkMap)
		return;

	pkMap->SetWaterRR(m_RRCheckBox->Checked, m_iID);
}

System::Void WaterControl::OnTransValueChanged(System::Object*  sender, System::EventArgs* e)
{
	CMap* pkMap = CMap::Get();
	if(!pkMap)
		return;

	pkMap->SetWaterAlpha(m_TransTrackBar->Value / 20.f, m_iID);

	Update();
}

System::Void WaterControl::OnSpeedValueChanged(System::Object*  sender, System::EventArgs* e)
{
	CMap* pkMap = CMap::Get();
	if(!pkMap)
		return;

	pkMap->SetWaterSpeed(m_SpeedTrackBar->Value / 20.f, m_iID);

	Update();
}

System::Void WaterControl::OnUVScaleValueChanged(System::Object*  sender, System::EventArgs* e)
{
	CMap* pkMap = CMap::Get();
	if(!pkMap)
		return;

	float fUVScale = (1 << m_UVScaleTrackBar->Value) / 32.f;
	pkMap->SetWaterUVScale(fUVScale, m_iID);

	Update();
}

void WaterControl::Update()
{
	CWaterShader* pkWaterShader = NULL;
	CMap* pkMap = CMap::Get();
	if(pkMap)
		pkWaterShader = pkMap->GetWaterShader(m_iID);

	if(pkWaterShader)
	{
		//path name
		NiFixedString kPathName = pkWaterShader->GetTexturePathName();
		if(!m_pkPathName->EqualsNoCase(kPathName))
		{
			*m_pkPathName = kPathName;
			LoadImage();
		}

		m_TransCheckBox->Checked = pkWaterShader->GetAlphaBlend();
		m_AdditiveCheckBox->Checked = pkWaterShader->GetAdditiveLight();
		m_RRCheckBox->Checked = pkWaterShader->GetRR();

		m_TransTrackBar->Value = int((pkWaterShader->GetAlpha() + 0.01f)*20);
		m_TransLable->Text = String::Format("透明度 {0:#0.##}", __box(pkWaterShader->GetAlpha()));

		m_SpeedTrackBar->Value = int((pkWaterShader->GetSpeed() + 0.01f)*20);
		m_SpeedLable->Text = String::Format("速度 {0:#0.##}", __box(pkWaterShader->GetSpeed()));

		int iValue;
		String* strDesc;
		float fUVScale = pkWaterShader->GetUVScale();
		if(fUVScale <= 1.f/32.f)
		{
			iValue = 0;
			strDesc = new String("1/32");
		}
		else if(fUVScale <= 1.f/16.f)
		{
			iValue = 1;
			strDesc = new String("1/16");
		}
		else if(fUVScale <= 1.f/8.f)
		{
			iValue = 2;
			strDesc = new String("1/8");
		}
		else if(fUVScale <= 1.f/4.f)
		{
			iValue = 3;
			strDesc = new String("1/4");
		}
		else if(fUVScale <= 1.f/2.f)
		{
			iValue = 4;
			strDesc = new String("1/2");
		}
		else if(fUVScale <= 1.f)
		{
			iValue = 5;
			strDesc = new String("1");
		}
		else if(fUVScale <= 2.f)
		{
			iValue = 6;
			strDesc = new String("2");
		}
		else if(fUVScale <= 4.f)
		{
			iValue = 7;
			strDesc = new String("4");
		}
		else if(fUVScale <= 8.f)
		{
			iValue = 8;
			strDesc = new String("8");
		}
		else if(fUVScale <= 16.f)
		{
			iValue = 9;
			strDesc = new String("16");
		}
		else
		{
			iValue = 10;
			strDesc = new String("32");
		}

		m_UVScaleTrackBar->Value = iValue;
		m_UVScaleLabel->Text = String::Format("UV缩放 {0}", strDesc);
	}
	else
	{
		*m_pkPathName = NULL;
		UnloadImage();

		if(m_ParentDlg->GetActiveWaterId() == m_iID)
		{
			m_ParentDlg->SetActiveWaterId(-1);
		}
	}
}

void WaterControl::LoadImage()
{
	if( m_Bitmap )
		TerrainPluginHelper::UnloadImage(m_Bitmap);

	//用第15个作为代表
	char acFileName[1024];
	NiSprintf(acFileName, sizeof(acFileName), "%s\\%s1.dds", TerrainHelper::GetClientPath(), (const char*)*m_pkPathName);

	m_Bitmap = TerrainPluginHelper::LoadImage(acFileName);
	m_WaterPictureBox->Invalidate();
}

void WaterControl::UnloadImage()
{
	if(m_Bitmap)
	{
		TerrainPluginHelper::UnloadImage(m_Bitmap);
		m_Bitmap = NULL;
	}

	m_WaterPictureBox->Invalidate();
}

void WaterControl::Do_Dispose(bool bDisposing)
{
	if(m_pkPathName)
	{
		NiDelete m_pkPathName;
		m_pkPathName = NULL;
	}

	if(m_Bitmap)
	{
		TerrainPluginHelper::UnloadImage(m_Bitmap);
		m_Bitmap = NULL;
	}
}

void WaterDialog::UpdateWaterControls()
{
	for(unsigned int ui = 0; ui < MAX_WATER_COUNT; ++ui)
	{
		m_pmWaterControls[ui]->Update();
	}
}
