
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
	namespace TerrainPlugin 
{
	public __gc class WaterDialog;
	public __gc class WaterControl : public MDisposable
	{
	private:
		System::Windows::Forms::Button* m_ChangeBtn;
		System::Windows::Forms::Button* m_DeleteBtn;
		System::Windows::Forms::CheckBox* m_TransCheckBox;
		System::Windows::Forms::CheckBox* m_AdditiveCheckBox;
		System::Windows::Forms::CheckBox* m_RRCheckBox;
		System::Windows::Forms::PictureBox* m_WaterPictureBox;	
		System::Windows::Forms::TrackBar*  m_UVScaleTrackBar;
		System::Windows::Forms::TrackBar*  m_SpeedTrackBar;
		System::Windows::Forms::TrackBar*  m_TransTrackBar;
		System::Windows::Forms::Label*  m_UVScaleLabel;
		System::Windows::Forms::Label*  m_SpeedLable;
		System::Windows::Forms::Label*  m_TransLable;
		System::Windows::Forms::GroupBox* m_WaterGroupBox;

		System::Windows::Forms::ToolTip* m_ToolTip;

		FIBITMAP* m_Bitmap;
		WaterDialog* m_ParentDlg;
		NiFixedString* m_pkPathName;
		int m_iID;

	public:
		void Init(WaterDialog* dlg, int id, int x, int y);
		__property System::Windows::Forms::Control* get_GroupBox()
		{
			return m_WaterGroupBox;
		}

		System::Void OnPictureBoxPaint(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e);
		System::Void OnPictureBoxClick(System::Object*  sender, System::EventArgs*  e);		
		System::Void OnPictureBoxMouseHover(System::Object*  sender, System::EventArgs*  e);

		System::Void OnChangeBtnClick(System::Object*  sender, System::EventArgs*  e);
		System::Void OnDeleteBtnClick(System::Object*  sender, System::EventArgs*  e);

		System::Void OnTransCheckedChanged(System::Object*  sender, System::EventArgs*  e);
		System::Void OnAdditiveCheckedChanged(System::Object*  sender, System::EventArgs*  e);
		System::Void OnRRCheckedChanged(System::Object*  sender, System::EventArgs*  e);

		System::Void OnTransValueChanged(System::Object*  sender, System::EventArgs* e);
		System::Void OnSpeedValueChanged(System::Object*  sender, System::EventArgs* e);
		System::Void OnUVScaleValueChanged(System::Object*  sender, System::EventArgs* e);

		void Update();
		void LoadImage();
		void UnloadImage();

		virtual void Do_Dispose(bool bDisposing);
	};

	public __gc class WaterDialog : public System::Windows::Forms::Form
	{
	public:
		WaterDialog(void)
		{
			InitializeComponent();
		}

		void Do_Dispose(bool bDisposing)
		{
			for(int i = 0; i < MAX_WATER_COUNT; ++i)
			{
				if(m_pmWaterControls[i])
				{
					m_pmWaterControls[i]->Do_Dispose(bDisposing);
					m_pmWaterControls[i] = NULL;
				}
			}
		}

		void UpdateWaterControls();

	protected:
		void Dispose(Boolean disposing)
		{
			__super::Dispose(disposing);
		}

	private:
		WaterControl* m_pmWaterControls[];
		System::Windows::Forms::Panel* m_Panel;
		int m_iActiveWaterId;
		static WaterDialog* ms_WaterDialog;

		void InitializeComponent(void)
		{
			ms_WaterDialog = this;

			m_iActiveWaterId = -1;

			this->SuspendLayout();

			this->m_Panel = (new System::Windows::Forms::Panel());
			this->m_Panel->AutoScroll = true;
			this->m_Panel->Location = System::Drawing::Point(3, 3);
			this->m_Panel->Name = S"m_Panel";
			this->m_Panel->Size = System::Drawing::Size(345, 601);
			this->m_Panel->TabIndex = 2;

			m_pmWaterControls = new WaterControl*[MAX_WATER_COUNT];
			for(int i = 0; i < MAX_WATER_COUNT; ++i)
			{
				m_pmWaterControls[i] = new WaterControl;
				m_pmWaterControls[i]->Init(this, i, 10, i*320);

				this->m_Panel->Controls->Add(m_pmWaterControls[i]->GroupBox);
			}

			// 
			// WaterDialog
			//
			this->ClientSize = System::Drawing::Size(345, 612);
			this->Controls->Add(this->m_Panel);
			this->Name = S"WaterDialog";
			this->Text = S"Water Dialog";
			this->ResumeLayout(false);
			this->PerformLayout();
		}

	public:
		int GetActiveWaterId() { return m_iActiveWaterId; }
		void SetActiveWaterId(int id) { m_iActiveWaterId = id; }
		static WaterDialog* Get() { return ms_WaterDialog; }
	};
} }}}