
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
	namespace TerrainPlugin {

		public __gc class TemplateControl : public System::Windows::Forms::Form
		{






		public:
			Template()
			{
				this->InitializeComponent();
			}

			void Do_Dispose(bool bDisposing)
			{
			}

		private: 
			System::Void InitializeComponent() 
			{
				this->m_Area0GroupBox = (new System::Windows::Forms::GroupBox());
				this->m_ColorTextBox = (new System::Windows::Forms::TextBox());
				this->m_AreaTextBox = (new System::Windows::Forms::TextBox());
				this->m_ChangeColor = (new System::Windows::Forms::Button());
				this->m_ChangeArea = (new System::Windows::Forms::Button());
				this->m_Area0GroupBox->SuspendLayout();
				this->SuspendLayout();
				// 
				// m_Area0GroupBox
				// 
				this->m_Area0GroupBox->Controls->Add(this->m_ColorTextBox);
				this->m_Area0GroupBox->Controls->Add(this->m_AreaTextBox);
				this->m_Area0GroupBox->Controls->Add(this->m_ChangeColor);
				this->m_Area0GroupBox->Controls->Add(this->m_ChangeArea);
				this->m_Area0GroupBox->Location = System::Drawing::Point(26, 12);
				this->m_Area0GroupBox->Name = S"m_Area0GroupBox";
				this->m_Area0GroupBox->Size = System::Drawing::Size(286, 127);
				this->m_Area0GroupBox->TabIndex = 7;
				this->m_Area0GroupBox->TabStop = false;
				this->m_Area0GroupBox->Text = S"Area0";
				this->m_Area0GroupBox->Paint += new System::Windows::Forms::PaintEventHandler(this, &TemplateControl::OnPaint);
				// 
				// m_ColorTextBox
				// 
				this->m_ColorTextBox->Location = System::Drawing::Point(19, 82);
				this->m_ColorTextBox->Name = S"m_ColorTextBox";
				this->m_ColorTextBox->ReadOnly = true;
				this->m_ColorTextBox->Size = System::Drawing::Size(169, 21);
				this->m_ColorTextBox->TabIndex = 5;
				// 
				// m_AreaTextBox
				// 
				this->m_AreaTextBox->Location = System::Drawing::Point(19, 32);
				this->m_AreaTextBox->Name = S"m_AreaTextBox";
				this->m_AreaTextBox->ReadOnly = true;
				this->m_AreaTextBox->Size = System::Drawing::Size(169, 21);
				this->m_AreaTextBox->TabIndex = 4;
				// 
				// m_ChangeColor
				// 
				this->m_ChangeColor->Location = System::Drawing::Point(194, 82);
				this->m_ChangeColor->Name = S"m_ChangeColor";
				this->m_ChangeColor->Size = System::Drawing::Size(75, 23);
				this->m_ChangeColor->TabIndex = 3;
				this->m_ChangeColor->Text = S"更改色彩";
				this->m_ChangeColor->UseVisualStyleBackColor = true;
				// 
				// m_ChangeArea
				// 
				this->m_ChangeArea->Location = System::Drawing::Point(194, 30);
				this->m_ChangeArea->Name = S"m_ChangeArea";
				this->m_ChangeArea->Size = System::Drawing::Size(75, 23);
				this->m_ChangeArea->TabIndex = 0;
				this->m_ChangeArea->Text = S"更改区域";
				this->m_ChangeArea->UseVisualStyleBackColor = true;
				// 
				// TemplateControl
				// 
				this->ClientSize = System::Drawing::Size(345, 612);
				this->Controls->Add(this->m_Area0GroupBox);
				this->Name = S"TemplateControl";
				this->Text = S"Template Dialog";
				this->m_Area0GroupBox->ResumeLayout(false);
				this->m_Area0GroupBox->PerformLayout();
				this->ResumeLayout(false);

			}

		protected: 
			void Dispose(Boolean disposing)
			{
				__super::Dispose(disposing);
			}

	System::Void OnPaint(System::Object*  sender, System::Windows::Forms::PaintEventArgs*  e) 
			{
			}
};
} }}}