// AnimationViewDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "AssetViewer.h"
#include "AnimationViewDialog.h"


// AnimationViewDialog 对话框

IMPLEMENT_DYNAMIC(AnimationViewDialog, CDialog)

AnimationViewDialog::AnimationViewDialog(CWnd* pParent /*=NULL*/)
	: CDialog(AnimationViewDialog::IDD, pParent)
{

}

AnimationViewDialog::~AnimationViewDialog()
{
}

void AnimationViewDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(AnimationViewDialog, CDialog)
END_MESSAGE_MAP()


// AnimationViewDialog 消息处理程序

void AnimationViewDialog::OnCancel()
{
	// TODO: 在此添加专用代码和/或调用基类

	CDialog::OnCancel();
}

void AnimationViewDialog::OnOK()
{
	// TODO: 在此添加专用代码和/或调用基类

	CDialog::OnOK();
}

BOOL AnimationViewDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
