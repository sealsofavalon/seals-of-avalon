#pragma once


// AnimationViewDialog 对话框

class AnimationViewDialog : public CDialog
{
	DECLARE_DYNAMIC(AnimationViewDialog)

public:
	AnimationViewDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~AnimationViewDialog();

// 对话框数据
	enum { IDD = IDD_ANIMATION_VIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
	virtual void OnCancel();
	virtual void OnOK();
public:
	virtual BOOL OnInitDialog();
};
