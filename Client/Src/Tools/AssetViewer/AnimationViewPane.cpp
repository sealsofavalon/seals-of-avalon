// AnimationViewPane.cpp : 实现文件
//

#include "stdafx.h"
#include "AssetViewer.h"
#include "AnimationViewPane.h"
#include "NifDoc.h"

// AnimationViewPane

IMPLEMENT_DYNCREATE(AnimationViewPane, CView)

AnimationViewPane::AnimationViewPane()
{

}

AnimationViewPane::~AnimationViewPane()
{
}

BEGIN_MESSAGE_MAP(AnimationViewPane, CView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// AnimationViewPane 绘图

void AnimationViewPane::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: 在此添加绘制代码
}


// AnimationViewPane 诊断

#ifdef _DEBUG
void AnimationViewPane::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void AnimationViewPane::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// AnimationViewPane 消息处理程序

int AnimationViewPane::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_wndActionDialog.Create(IDD_ANIMATION_VIEW_PANE, this);
	return 0;
}

void AnimationViewPane::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);
	RECT rcClient;
	rcClient.left = rcClient.top = 0;
	rcClient.bottom = cy;
	rcClient.right = cx;
	m_wndActionDialog.MoveWindow(&rcClient);
}

void AnimationViewPane::OnDestroy()
{
	CView::OnDestroy();
	m_wndActionDialog.DestroyWindow();
}

void AnimationViewPane::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	CNifDoc* Document = (CNifDoc*)GetDocument();
	if (m_wndActionDialog.m_hWnd)
	{
		m_wndActionDialog.OnUpdate(pSender,lHint,pHint);
	}

	NiAVObject* SelObject = Document->GetSelectedObject();
	if (SelObject)
	{
		NiNode* SelNode = NiDynamicCast(NiNode,SelObject);
		NiBooleanExtraData* bKFMData = NULL;
		NiAVObject* pChild;
		if (SelNode)
		{
			pChild = SelNode->GetAt(0);
			if (pChild)
			{
				bKFMData = (NiBooleanExtraData*)pChild->GetExtraData("bKFM");
			}
		}
	
		if (bKFMData)
		{
			SelectKFMObject(pChild);
			return;
		}
	}
	SelectKFMObject(NULL);
}

void AnimationViewPane::SelectKFMObject(NiAVObject* Object)
{
	if (::IsWindow(m_wndActionDialog))
	{
		m_wndActionDialog.SelectKFMObject(Object);
	}
}