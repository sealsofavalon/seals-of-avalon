#pragma once
#include "AnimationViewPaneDialog.h"

// AnimationViewPane 视图

class AnimationViewPane : public CView
{
	DECLARE_DYNCREATE(AnimationViewPane)

protected:
	AnimationViewPane();           // 动态创建所使用的受保护的构造函数
	virtual ~AnimationViewPane();

public:
	virtual void OnDraw(CDC* pDC);      // 重写以绘制该视图
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	AnimationViewPaneDialog m_wndActionDialog;
	void SelectKFMObject(NiAVObject* Object);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
protected:
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
};


