// AnimationViewPaneDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "AssetViewer.h"
#include "AnimationViewPaneDialog.h"
#include "NifDoc.h"
#include "NifTimeManager.h"
#include "NifUserPreferences.h"
// AnimationViewPaneDialog 对话框

IMPLEMENT_DYNAMIC(AnimationViewPaneDialog, CDialog)

AnimationViewPaneDialog::AnimationViewPaneDialog(CWnd* pParent /*=NULL*/)
	: CDialog(AnimationViewPaneDialog::IDD, pParent)
{
	m_Actor = NULL;
	m_KFM = NULL;
	m_ActorMgr = NULL;
	m_CurSeqEnd = 0.0f;
	m_CurSeqStart = 0.0f;
	m_CurSeqLength = 0.0f;
	m_TimeRate = 1.0f;
	m_ShowWithFrame = FALSE;
	m_FrameRate = 30.0f;
}

AnimationViewPaneDialog::~AnimationViewPaneDialog()
{
}

void AnimationViewPaneDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SEQUENCE_LIST, m_cmbSequenceList);
	DDX_Control(pDX, IDC_ANIM_LOOP, m_btnLoop);
	DDX_Control(pDX, IDC_CURRENT_TIME, m_wndCurTime);
	DDX_Control(pDX, IDC_END_TIME, m_wndEndTime);
	DDX_Control(pDX, IDC_PLAY, m_btnPlay);
	DDX_Control(pDX, IDC_START_TIME, m_wndStartTime);
	DDX_Control(pDX, IDC_STOP, m_btnStop);
	DDX_Control(pDX, IDC_TIME_DURATION, m_wndTimeDuration);
	DDX_Control(pDX, IDC_TIME_SLIDER, m_wndTimeSlider);
	DDX_Control(pDX, IDC_EDIT_TIMERATE, m_wndTimeRate);
	DDX_Control(pDX, IDC_SPIN_TIMERATE, m_spinTimeRate);
	DDX_Control(pDX, IDC_SLIDER_TIMERATE, m_sliderTimeRate);
	DDX_Control(pDX, IDC_CHECK_FRAME, m_chkFrame);
}


BEGIN_MESSAGE_MAP(AnimationViewPaneDialog, CDialog)
	ON_WM_CREATE()
	ON_CBN_SELCHANGE(IDC_SEQUENCE_LIST, &AnimationViewPaneDialog::OnSequenceChange)
	ON_BN_CLICKED(IDC_PLAY, &AnimationViewPaneDialog::OnPlay)
	ON_BN_CLICKED(IDC_STOP, &AnimationViewPaneDialog::OnStop)
	ON_WM_HSCROLL()
	ON_EN_UPDATE(IDC_EDIT_TIMERATE, &AnimationViewPaneDialog::OnUpdateTimeRate)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_TIMERATE, &AnimationViewPaneDialog::OnDeltePosTimeRate)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_TIMERATE, &AnimationViewPaneDialog::OnNMReleasedcaptureSliderTimerate)
	ON_BN_CLICKED(IDC_CHECK_FRAME, &AnimationViewPaneDialog::OnBnClickedCheckFrame)
END_MESSAGE_MAP()


// AnimationViewPaneDialog 消息处理程序

int AnimationViewPaneDialog::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	UINT Resource[] = {IDB_PLAY,IDB_PAUSE,IDB_STOP,IDB_ANIM_LOOPU,IDB_ANIM_LOOPD};


	for(UINT ui = PLAY; ui < NUM_IMAGES; ui++)
	{
		m_bmButtons[ui].LoadBitmap(Resource[ui]);
	}

	return 0;
}

BOOL AnimationViewPaneDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_btnPlay.SetBitmap(m_bmButtons[PLAY]);
	m_btnStop.SetBitmap(m_bmButtons[STOP]);
	m_btnLoop.SetBitmap(m_bmButtons[ANIMATIONS_LOOP]);
	m_Playing = FALSE;
	
	m_wndTimeRate.SetWindowText("1.0");
	m_spinTimeRate.SetRange(1,100);
	m_spinTimeRate.SetPos(10);
	m_sliderTimeRate.SetRange(1,10);
	m_sliderTimeRate.SetPos(1);
	m_chkFrame.SetCheck(m_ShowWithFrame);
	//INT Pos = m_spinTimeRate.GetPos();
	return TRUE; 
}

BOOL AnimationViewPaneDialog::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult)
{
	// TODO: 在此添加专用代码和/或调用基类

	return CDialog::OnChildNotify(message, wParam, lParam, pLResult);
}

void AnimationViewPaneDialog::OnOK()
{
}
void AnimationViewPaneDialog::OnCancel()
{
}

void AnimationViewPaneDialog::SelectKFMObject(NiAVObject* Object)
{
	if (Object == m_Actor)
	{
		return;
	}
	if (Object == NULL)
	{
		GetDlgItem(IDC_KFM_NAME)->SetWindowText("无效的KFM对象");
		m_cmbSequenceList.ResetContent();
		m_Actor = NULL;
		m_KFM = NULL;
		m_ActorMgr = NULL;
		m_ActorRoot = NULL;
		return;
	}
	NiNode* Parent = Object->GetParent();
	if (Parent != NULL)
	{
		const NiFixedString& ObjName = Parent->GetName();
		GetDlgItem(IDC_KFM_NAME)->SetWindowText((const TCHAR*)ObjName);
		NiNode* Node = NiDynamicCast(NiNode,Object);
		NIASSERT(Node);
		m_Actor = Object;
		Object = Node->GetAt(0);
		NiIntegerExtraData* IPtr =(NiIntegerExtraData*)Object->GetExtraData("KFM_Actor");
		NIASSERT(IPtr);
		
		NiActorManager* ActorMgr =  (NiActorManager*)IPtr->GetValue();
		NiKFMTool* KFM = ActorMgr->GetKFMTool();
		UINT* SequenceID;
		UINT NumSeqIDs;
		KFM->GetSequenceIDs(SequenceID,NumSeqIDs);
		
		// 填充动作列表
		m_cmbSequenceList.ResetContent();
		NiControllerSequence* Seq;
		for (UINT S = 0; S < NumSeqIDs; ++S)
		{
			Seq = ActorMgr->GetSequence(SequenceID[S]);
			NIASSERT(Seq);
			const NiFixedString& SeqName = Seq->GetName();
			INT Item = m_cmbSequenceList.AddString((const char*)SeqName);
			m_cmbSequenceList.SetItemData(Item,SequenceID[S]);
		}

		if (SequenceID)
		{
			NiFree(SequenceID);
		}
		
		m_ActorRoot = Object;
		m_KFM = KFM;
		m_ActorMgr = ActorMgr;
	}
	
}
void AnimationViewPaneDialog::OnSequenceChange()
{
	if (m_KFM == NULL || m_ActorMgr == NULL)
	{
		return;
	}
	INT Sel = m_cmbSequenceList.GetCurSel();
	if (Sel != -1)
	{
		UINT SeqID = (UINT)m_cmbSequenceList.GetItemData(Sel);
		m_ActorMgr->SetTargetAnimation(SeqID);
		NiControllerSequence* Seq = m_ActorMgr->GetSequence(SeqID);
		NIASSERT(Seq);
		m_CurSeqStart = Seq->GetBeginKeyTime();
		m_CurSeqEnd = Seq->GetEndKeyTime();
		m_CurSeqLength = Seq->GetLength();
		
		CNifDoc* pkDoc = CNifDoc::GetDocument();
		if(!pkDoc)
			return;

		pkDoc->Lock();
		CNifTimeManager* pkTimeManager = pkDoc->GetTimeManager();
		pkTimeManager->SetStartTime(m_CurSeqStart);
		pkTimeManager->SetEndTime(m_CurSeqEnd);
		pkTimeManager->SetCurrentTime(0);
		pkDoc->UnLock();
		Play(FALSE);
		RefreshValue();
	}
}

void AnimationViewPaneDialog::Play(BOOL bPlay)
{
	m_Playing = bPlay;
	CNifDoc* pkDoc = CNifDoc::GetDocument();
	if(!pkDoc)
		return;

	pkDoc->Lock();
	CNifTimeManager* pkTimeManager = pkDoc->GetTimeManager();

	if(!m_Playing && pkTimeManager)
	{
		pkTimeManager->Disable();
		m_btnPlay.SetBitmap(m_bmButtons[PLAY]);
	}
	else if(pkTimeManager)
	{
		pkTimeManager->Enable();
		m_btnPlay.SetBitmap(m_bmButtons[PAUSE]);
	}
	pkDoc->UnLock();
}

void AnimationViewPaneDialog::OnPlay()
{
	Play(!m_Playing);
}

void AnimationViewPaneDialog::OnStop()
{
	Play(FALSE);
	CNifDoc* pkDoc = CNifDoc::GetDocument();
	if(!pkDoc)
		return;
	pkDoc->Lock();
	CNifTimeManager* pkTimeManager = pkDoc->GetTimeManager();
	pkTimeManager->SetCurrentTime(pkTimeManager->GetStartTime());
	pkDoc->UnLock();
}

void AnimationViewPaneDialog::RefreshValue()
{
	CNifDoc* pkDoc = CNifDoc::GetDocument();
	if(!pkDoc) 
		return;

	pkDoc->Lock();
	CNifTimeManager* pkTimeManager = pkDoc->GetTimeManager();

	CNifUserPreferences::Lock();
	float fTicksPerSec = CNifUserPreferences::
		AccessUserPreferences()->GetAnimationSliderSecondsPerTick();
	CNifUserPreferences::UnLock();

	if(!pkTimeManager)
	{
		pkDoc->UnLock();
		return;
	}
	if (m_CurSeqStart >= m_CurSeqEnd)
	{
		pkDoc->UnLock();
		return;
	}
	


	if (m_wndTimeSlider.m_hWnd)
	{
		INT Range = TimeToRange(m_CurSeqLength);
		m_wndTimeSlider.SetRange(0,Range,TRUE);
		//int iLastTime = TimeToRange(pkDoc->GetLastTimeUpdated());
		m_wndTimeSlider.SetPos(0);
	}
	
	TCHAR Buffer[256];
	if (m_wndStartTime.m_hWnd)
	{
		NiSprintf(Buffer, 256, "%.2f", m_CurSeqStart);
		m_wndStartTime.SetWindowText(Buffer);
	}
	
	if (m_wndEndTime.m_hWnd)
	{
		NiSprintf(Buffer, 256, "%.2f", m_CurSeqEnd);
		m_wndEndTime.SetWindowText(Buffer);
	}

	if (m_wndTimeDuration.m_hWnd)
	{
		NiSprintf(Buffer, 256, "%.2f", m_CurSeqLength);
		m_wndTimeDuration.SetWindowText(Buffer);
	}
	
	if (m_btnLoop.m_hWnd)
	{
		if(pkTimeManager->GetTimeMode() == CNifTimeManager::CONTINUOUS)
		{
			//m_Playing = FALSE;
			m_btnLoop.SetBitmap(m_bmButtons[ANIMATIONS_CONTINUOUS]);
		}
		else if (pkTimeManager->GetTimeMode() == CNifTimeManager::LOOP)
		{
			//m_bIsLooping = true;
			m_btnLoop.SetBitmap(m_bmButtons[ANIMATIONS_LOOP]);
		}
	}
	
	if (m_btnPlay.m_hWnd)
	{
		if(pkTimeManager->IsEnabled())
		{
			m_Playing = TRUE;
			m_btnPlay.SetBitmap(m_bmButtons[PAUSE]);
		}
		else 
		{
			m_Playing = FALSE;
			m_btnPlay.SetBitmap(m_bmButtons[PLAY]);
		}
	}

	
	pkDoc->UnLock();
}
void AnimationViewPaneDialog::OnUpdate(CView* pSender, LPARAM lHint,CObject* pHint)
{

	WORD wLoWord = LOWORD(lHint);
	CNifDoc* pkDoc = CNifDoc::GetDocument();

	if (wLoWord == NIF_TIMINGINFOCHANGED)
	{
		RefreshValue();
	}
	else if(wLoWord == NIF_TIMECHANGED && pkDoc)
	{
		float fTime = pkDoc->GetLastTimeUpdated();
		

		if(m_wndCurTime.m_hWnd != NULL)
		{
			char acValue [256];
			
			if (m_ShowWithFrame)
			{
				INT Frame = fTime*m_FrameRate;
				NiSprintf(acValue, 256,"%d", Frame);
			}else
			{
				NiSprintf(acValue, 256,"%f", fTime);
			}
			m_wndCurTime.SetWindowText(acValue);
		}
		int iCurrentTime = TimeToRange(fTime);
		if(m_wndTimeSlider.m_hWnd != NULL)
			m_wndTimeSlider.SetPos(iCurrentTime);
	}
}

void AnimationViewPaneDialog::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (pScrollBar == (CScrollBar*)&m_wndTimeSlider)
	{
		if(nSBCode != TB_ENDTRACK)
		{
			int iPos = m_wndTimeSlider.GetPos();
			float Time = RangeToTime(iPos);
			CNifDoc* pkDoc = CNifDoc::GetDocument();
			if(!pkDoc)
				return;

			pkDoc->Lock();
			CNifTimeManager* pkTimeManager = pkDoc->GetTimeManager();

			float fLastTime = pkDoc->GetLastTimeUpdated();

			if(fLastTime != Time)
			{
				pkTimeManager->SetCurrentTime(Time);
				pkDoc->UpdateScene();
			}
			pkDoc->UnLock();
		}
	}
	
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void AnimationViewPaneDialog::OnUpdateTimeRate()
{
	//UpdateData(FALSE);
	//CString TimeStr;
	//m_wndTimeRate.GetWindowText(TimeStr);
	//float Factor = (float)atof(TimeStr.GetBuffer(0));
	//if (Factor < 0.0001f) // ERROR. atof failed.
	//{
	//	m_wndTimeRate.SetWindowText("1.0");
	//	m_spinTimeRate.SetPos(10);
	//	return;
	//}else if (Factor > 10.0f)
	//{
	//	Factor = 10.0f;
	//}

	//INT Pos = (INT)(Factor*10);
	//m_spinTimeRate.SetPos(Pos);
}

void AnimationViewPaneDialog::OnDeltePosTimeRate(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	INT Pos = m_spinTimeRate.GetPos();
	Pos = LOWORD(Pos);
//	m_spinTimeRate.SetPos(Pos);
	TCHAR Buffer[8];
	NiSprintf(Buffer,8,"%.1f",Pos*0.1f);
	m_wndTimeRate.SetWindowText(Buffer);
	m_sliderTimeRate.SetPos(Pos*0.1f);
	CNifDoc* pkDoc = CNifDoc::GetDocument();
	if(!pkDoc)
		return;

	pkDoc->Lock();
	CNifTimeManager* pkTimeManager = pkDoc->GetTimeManager();
	pkTimeManager->SetScaleFactor(Pos*0.1f);
	pkDoc->UnLock();
	*pResult = 0;
}

void AnimationViewPaneDialog::OnNMReleasedcaptureSliderTimerate(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	float Pos = (float)m_sliderTimeRate.GetPos();
	TCHAR Buffer[8];
	NiSprintf(Buffer,8,"%.1f",Pos);
	m_wndTimeRate.SetWindowText(Buffer);
	m_spinTimeRate.SetPos(Pos*10.0f);
	CNifDoc* pkDoc = CNifDoc::GetDocument();
	if(!pkDoc)
		return;

	pkDoc->Lock();
	CNifTimeManager* pkTimeManager = pkDoc->GetTimeManager();
	pkTimeManager->SetScaleFactor(Pos*1.0f);
	pkDoc->UnLock();
	*pResult = 0;
}

void AnimationViewPaneDialog::OnBnClickedCheckFrame()
{
	m_ShowWithFrame = m_chkFrame.GetCheck();
	if (m_ShowWithFrame)
	{
		GetDlgItem(IDC_STATIC_TIME_UNIT)->SetWindowText("Frame");
	}else
	{
		GetDlgItem(IDC_STATIC_TIME_UNIT)->SetWindowText("Sec");
	}
	CNifDoc* pkDoc = CNifDoc::GetDocument();
	float fTime = pkDoc->GetLastTimeUpdated();
	if(m_wndCurTime.m_hWnd != NULL)
	{
		char acValue [256];

		if (m_ShowWithFrame)
		{
			INT Frame = fTime*m_FrameRate;
			NiSprintf(acValue, 256,"%d", Frame);
		}else
		{
			NiSprintf(acValue, 256,"%f", fTime);
		}
		m_wndCurTime.SetWindowText(acValue);
	}
}
