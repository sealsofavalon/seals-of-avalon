#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// AnimationViewPaneDialog 对话框
// 该对话框作为子窗口嵌入在AnimationViewPane视图中,以便于利用既有框架进行停靠.
class AnimationViewPaneDialog : public CDialog
{
	DECLARE_DYNAMIC(AnimationViewPaneDialog)

public:
	AnimationViewPaneDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~AnimationViewPaneDialog();

// 对话框数据
	enum { IDD = IDD_ANIMATION_VIEW_PANE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	enum 
	{
		PLAY = 0,
		PAUSE,
		STOP,
		ANIMATIONS_LOOP,
		ANIMATIONS_CONTINUOUS,
		NUM_IMAGES
	};
	CBitmap m_bmButtons[NUM_IMAGES];
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual BOOL OnInitDialog();
	virtual BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult);
	void SelectKFMObject(NiAVObject* Object);
	NiActorManager*	m_ActorMgr;
	NiKFMTool*		m_KFM;
	NiAVObject*		m_Actor;
	NiAVObject*		m_ActorRoot;
protected:
	void Play(BOOL Paly = TRUE);
	virtual void OnOK();
	virtual void OnCancel();
	CComboBox m_cmbSequenceList;
	afx_msg void OnSequenceChange();
	CButton m_btnLoop;
	CEdit m_wndCurTime;
	CEdit m_wndEndTime;
	CButton m_btnPlay;
	CEdit m_wndStartTime;
	CButton m_btnStop;
	CStatic m_wndTimeDuration;
	CSliderCtrl m_wndTimeSlider;
	UINT m_Playing :1;
	float m_CurSeqStart;
	float m_CurSeqEnd;				//  当前动作停止时间
	float m_CurSeqLength;			//	当前动作时间长度
	float m_TimeRate;				// 时钟缩放
	float m_FrameRate;
public:
	afx_msg void OnPlay();
	afx_msg void OnStop();
	inline INT TimeToRange(float tm)
	{
		float fInSpan = tm - m_CurSeqStart;
		fInSpan *= 60.0f;
		return (int)fInSpan;
	}
	inline float RangeToTime(INT Pos)
	{
		float Time = m_CurSeqStart + (Pos/60.0f);
		if (Time > m_CurSeqEnd)
		{
			Time = m_CurSeqEnd;
		}
		return Time;
	}
public:
	void RefreshValue();
	void OnUpdate(CView* pSender, LPARAM lHint,CObject* pHint); 
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
public:
	CEdit m_wndTimeRate;
	CSpinButtonCtrl m_spinTimeRate;
	afx_msg void OnUpdateTimeRate();
	afx_msg void OnDeltePosTimeRate(NMHDR *pNMHDR, LRESULT *pResult);
public:
	CSliderCtrl m_sliderTimeRate;
public:
	afx_msg void OnNMReleasedcaptureSliderTimerate(NMHDR *pNMHDR, LRESULT *pResult);
public:
	CButton m_chkFrame;
	BOOL m_ShowWithFrame;
public:
	afx_msg void OnBnClickedCheckFrame();
};
