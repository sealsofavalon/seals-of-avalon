// AvatarDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AssetViewer.h"
#include "AvatarDlg.h"
#include "AvatarNode.h"
#include "AvatarItemDataBase.h"
#include "NifDoc.h"
#include <string>
#include <fstream>

#define _EditVersion_

// CAvatarDlg 对话框

IMPLEMENT_DYNAMIC(CAvatarDlg, CDialog)

CAvatarDlg::CAvatarDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAvatarDlg::IDD, pParent)
{
	m_pAvatarItemListDlg = NULL;
}

CAvatarDlg::~CAvatarDlg()
{
	if(m_pAvatarItemListDlg)
		delete m_pAvatarItemListDlg;
}

void CAvatarDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAvatarDlg, CDialog)
	ON_BN_CLICKED(IDC_AVATAR_HAIR, &CAvatarDlg::OnBnClickedAvatarHair)
	ON_BN_CLICKED(IDC_AVATAR_HEAD, &CAvatarDlg::OnBnClickedAvatarHead)
	ON_BN_CLICKED(IDC_AVATAR_SHOULDER_LEFT, &CAvatarDlg::OnBnClickedAvatarShoulderLeft)
	ON_BN_CLICKED(IDC_AVATAR_SHOULDER_RIGHT, &CAvatarDlg::OnBnClickedAvatarShoulderRight)
	ON_BN_CLICKED(IDC_AVATAR_BRACERS_LEFT, &CAvatarDlg::OnBnClickedAvatarBracersLeft)
	ON_BN_CLICKED(IDC_AVATAR_BRACERS_RIGHT, &CAvatarDlg::OnBnClickedAvatarBracersRight)
	ON_BN_CLICKED(IDC_AVATAR_CHEST, &CAvatarDlg::OnBnClickedAvatarChest)
	ON_BN_CLICKED(IDC_AVATAR_GLOVES_LEFT, &CAvatarDlg::OnBnClickedAvatarGlovesLeft)
	ON_BN_CLICKED(IDC_AVATAR_GLOVES_RIGHT, &CAvatarDlg::OnBnClickedAvatarGlovesRight)
	ON_BN_CLICKED(IDC_AVATAR_LEFTHAND, &CAvatarDlg::OnBnClickedAvatarLefthand)
	ON_BN_CLICKED(IDC_AVATAR_RIGHTHAND, &CAvatarDlg::OnBnClickedAvatarRighthand)
	//ON_BN_CLICKED(IDC_AVATAR_BELT, &CAvatarDlg::OnBnClickedAvatarBelt)
	ON_BN_CLICKED(IDC_AVATAR_TROUSERS_LEFT, &CAvatarDlg::OnBnClickedAvatarTrousersLeft)
	ON_BN_CLICKED(IDC_AVATAR_TROUSERS_RIGHT, &CAvatarDlg::OnBnClickedAvatarTrousersRight)
	ON_BN_CLICKED(IDC_AVATAR_SHOES_LEFT, &CAvatarDlg::OnBnClickedAvatarShoesLeft)
	ON_BN_CLICKED(IDC_AVATAR_SHOES_RIGHT, &CAvatarDlg::OnBnClickedAvatarShoesRight)
	ON_BN_CLICKED(ID_AVATAR_SAVE, &CAvatarDlg::OnBnClickedAvatarSave)
	ON_BN_CLICKED(IDC_AVATAR_LOAD, &CAvatarDlg::OnBnClickedAvatarLoad)
	ON_BN_CLICKED(IDC_AVATAR_ARM_LETF, &CAvatarDlg::OnBnClickedAvatarArmLetf)
	ON_BN_CLICKED(IDC_AVATAR_ARM_RIGHT, &CAvatarDlg::OnBnClickedAvatarArmRight)
END_MESSAGE_MAP()


void CAvatarDlg::ChangeAvatar(Avatar_Part eAvatarPart)
{
	// TODO: 在此添加控件通知处理程序代码
	// New
	if(!m_pAvatarItemListDlg)
		m_pAvatarItemListDlg = new CAvatarItemListDlg;
	m_pAvatarItemListDlg->InitDlg(eAvatarPart);
}

// CAvatarDlg 消息处理程序
// 发型
void CAvatarDlg::OnBnClickedAvatarHair()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Hair);
}

// 头
void CAvatarDlg::OnBnClickedAvatarHead()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Head);
}

// 肩部
void CAvatarDlg::OnBnClickedAvatarShoulderLeft()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Shoulder);
}

// 肩部
void CAvatarDlg::OnBnClickedAvatarShoulderRight()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Shoulder);
}

// 护腕
void CAvatarDlg::OnBnClickedAvatarBracersLeft()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Bracers);
}

// 护腕
void CAvatarDlg::OnBnClickedAvatarBracersRight()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Bracers);
}

// 胸部
void CAvatarDlg::OnBnClickedAvatarChest()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Chest);
}

void CAvatarDlg::OnBnClickedAvatarArmLetf()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Arm);
}

void CAvatarDlg::OnBnClickedAvatarArmRight()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Arm);
}

// 手套
void CAvatarDlg::OnBnClickedAvatarGlovesLeft()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Gloves);
}

// 手套
void CAvatarDlg::OnBnClickedAvatarGlovesRight()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Gloves);
}

// 左手武器
void CAvatarDlg::OnBnClickedAvatarLefthand()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_LeftHand);
}

// 右手武器
void CAvatarDlg::OnBnClickedAvatarRighthand()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_RightHand);
}

//// 腰带
//void CAvatarDlg::OnBnClickedAvatarBelt()
//{
//	// TODO: 在此添加控件通知处理程序代码
//	ChangeAvatar(Avatar_Belt);
//}

// 裤子
void CAvatarDlg::OnBnClickedAvatarTrousersLeft()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Legs);
}

// 裤子
void CAvatarDlg::OnBnClickedAvatarTrousersRight()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Legs);
}

// 鞋子
void CAvatarDlg::OnBnClickedAvatarShoesLeft()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Boots);
}

// 鞋子
void CAvatarDlg::OnBnClickedAvatarShoesRight()
{
	// TODO: 在此添加控件通知处理程序代码
	ChangeAvatar(Avatar_Boots);
}

void CAvatarDlg::RecursiveGetGeometry(NiNode* pNode, std::vector<NiGeometry*>& vGeometry)
{
	if(NiIsKindOf(NiGeometry, pNode))
	{
		vGeometry.push_back((NiGeometry*)pNode);
	}
	else
	{
		for(unsigned int ui = 0; ui < pNode->GetArrayCount(); ++ui)
		{
			NiNode* pChildNode = (NiNode*)pNode->GetAt(ui);
			if(pChildNode && pChildNode->GetName() != "Dummy01")
				RecursiveGetGeometry(pChildNode, vGeometry);
		}
	}
}

void CAvatarDlg::OnBnClickedAvatarSave()
{
	// TODO: 在此添加控件通知处理程序代码
	static char szFileName[] = "Avatar Info(*.csv)|*.csv;||";

	CFileDialog dlg(TRUE, "csv", NULL, OFN_HIDEREADONLY | OFN_NONETWORKBUTTON
		| OFN_LONGNAMES | OFN_OVERWRITEPROMPT | OFN_ENABLESIZING, szFileName);
	
	if(dlg.DoModal() != IDOK)
	{
		return;
	}

	CString str = dlg.GetPathName();

	if(str.Find(".") == -1)
	{
		str.Append(".csv");
	}

	FILE* SaveFiles = NULL;

	SaveFiles = fopen(str, "wb");

	long startPos = ftell(SaveFiles);

	if(!SaveFiles)
	{
		::MessageBox(NULL, "Couldn't open file for writing.", NULL, MB_OK);
		return;
	}

	rewind(SaveFiles);

	// Write
	CNifDoc* pkDoc = CNifDoc::GetDocument();
	pkDoc->Lock();

	NiNode* pkScene = pkDoc->GetSceneGraph();
	if(pkScene)
	{
		//
#ifdef _EditVersion_
		NiNode* pRootNode = (NiNode*)pkScene->GetObjectByName("Scene Root");
		if(!pRootNode)
			goto end;
#endif
		// Write
		char str[_MAX_PATH];
		sprintf(str, "角色编号,Avatar名称,装备编号,贴图编号,描述\n");
		std::string strSize;
		strSize = str;
		fwrite(&str, sizeof(char), strSize.size(), SaveFiles);

		std::vector<NiGeometry*> vGeometry;
		RecursiveGetGeometry(pRootNode, vGeometry);
		std::string strAvatarName;
		unsigned int ItemId;
		unsigned int TextureId;
		//std::string strAvatarDescribe;
		for(unsigned int ui = 0; ui < vGeometry.size(); ++ui)
		{
			NiAVObject* pAVObject = NULL;
			// Get Role Type, 0
			;
			// Get AvatarName
			NiGeometry* pGeometry = vGeometry[ui];
			pAVObject = pGeometry->GetParent();
			strAvatarName = pAVObject->GetName();
			// Get ItemID
			if(pAVObject->GetExtraDataSize() == 0)
			{
				ItemId = 0;
			}
			else
			{
				for(unsigned short usExtraDataIndex = 0; usExtraDataIndex < pAVObject->GetExtraDataSize(); ++usExtraDataIndex)
				{
					NiExtraData* pExtraData = pAVObject->GetExtraDataAt(usExtraDataIndex);
					NiFixedString strItemIdName = pExtraData->GetName();
					if(strItemIdName.Contains(".nif"))
					{
						char sItemId[_MAX_PATH];
						for(size_t index = 0; index < strItemIdName.GetLength() - 4; ++index)
						{
							sItemId[index] = strItemIdName[index];
						}
						ItemId = atoi(sItemId);
						//// Describe
						//strAvatarDescribe = ((NiStringExtraData*)pExtraData)->GetValue();
					}
				}
			}
			// Get TextureID
			NiTexturingProperty* pTexProperty = (NiTexturingProperty*)pGeometry->GetProperty(NiProperty::TEXTURING);
			if(pTexProperty)
			{
				NiTexturingProperty::Map* pBaseMap = pTexProperty->GetBaseMap();
				if(pBaseMap)
				{
					NiSourceTexture* pTexture = (NiSourceTexture*)pBaseMap->GetTexture();
					if(pTexture)
					{
						std::string strFileName = pTexture->GetFilename();
						int index = strFileName.find_last_of("\\");
						int lastDot = strFileName.find_last_of(".");
						std::string strTextureName;
						if(index == -1)
						{
							strTextureName = strFileName.substr(0, lastDot);
						}
						else
						{
							strTextureName = strFileName.substr(index + 1, lastDot - index -1);
						}
						TextureId = atoi(strTextureName.c_str());
					}
				}
			}
			else
			{
				TextureId = 0;
			}
			// Write
			sprintf(str, "0,%s,%d,%d\n", strAvatarName.c_str(), ItemId, TextureId);
			strSize = str;
			fwrite(&str, sizeof(char), strSize.size(), SaveFiles);
		}
	}

end:
	pkDoc->UnLock();

	if(SaveFiles) 
		fclose(SaveFiles);
}

void CAvatarDlg::OnBnClickedAvatarLoad()
{
	// TODO: 在此添加控件通知处理程序代码
	static char szFileName[] = "Avatar Info(*.csv)|*.csv;||";

	CFileDialog dlg(TRUE, "csv", NULL, OFN_HIDEREADONLY | OFN_NONETWORKBUTTON
		| OFN_LONGNAMES | OFN_OVERWRITEPROMPT | OFN_ENABLESIZING, szFileName);

	if(dlg.DoModal() != IDOK)
	{
		return;
	}

	CString str = dlg.GetPathName();

	NiImageConverter::SetPlatformSpecificSubdirectory(NULL);

	CNifDoc* pkDoc = CNifDoc::GetDocument();
	pkDoc->Lock();
	NiNode* pkScene = pkDoc->GetSceneGraph();
	if(pkScene)
	{
		CAvatarNode pAvatarNode;
		// Open .csv file
		std::ifstream fin(str.GetBuffer());
		char line[512];
		// skip 1 line
		fin.getline(line, 512);
		while(fin.getline(line, 512)) 
		{
			SItemRecord rec(line);

			// ChangeAvatar
			char strFullPathName[_MAX_PATH];
			sprintf(strFullPathName, "%s..\\%d.nif", theApp.GetApplicationDirectory(), rec.itemId);
			char strFileName[128];
			sprintf(strFileName, "%d.nif", rec.itemId);
			NiStream kStream;
			bool bSuccess = kStream.Load(strFullPathName);
			if(bSuccess)
			{
				NiAVObject* pNode = (NiAVObject*)kStream.GetObjectAt(0);
				if(pNode)
				{
					CAvatarNode::SAvatarInfo sAvatarInfo;
					sAvatarInfo.Type = Avatar_Model;
					sAvatarInfo.Part = GetAvatarPartId((char*)rec.avatarType.GetBuffer());
					sAvatarInfo.strMeshName = strFileName;
					pAvatarNode.ChangeAvatar(pkScene, (NiObject*)pNode, sAvatarInfo);
				}
			}
			//sprintf(strFullPathName, "%s..\\%d.tga", theApp.GetApplicationDirectory(), rec.textureId);
			sprintf(strFullPathName, "%s..\\%s.tga", theApp.GetApplicationDirectory(), rec.texture);
			NiTexture* pTex = NiSourceTexture::Create(strFullPathName);
			NiTexturingPropertyPtr pTexProperty = NiNew NiTexturingProperty();
			if(pTex && pTexProperty)
			{
				pTexProperty->SetBaseTexture(pTex);	

				CAvatarNode::SAvatarInfo sAvatarInfo;
				sAvatarInfo.Type = Avatar_Textrue;
				sAvatarInfo.Part = GetAvatarPartId((char*)rec.avatarType.GetBuffer());
				pAvatarNode.ChangeAvatar(pkScene, (NiObject*)pTexProperty, sAvatarInfo);
			}
		}
		fin.close();
	}
	pkDoc->UnLock();

	// ChangeMesh Need Update View
	// May case some bugs*
	pkDoc->UpdateAllViews(NULL, MAKELPARAM(NIF_SCENECHANGED, 0), NULL); 
}
