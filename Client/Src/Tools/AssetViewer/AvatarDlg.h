#ifndef AVATARDLG_H
#define AVATARDLG_H

#include "AvatarItemListDlg.h"

class CAvatarNode;
// CAvatarDlg 对话框
class CAvatarDlg : public CDialog
{
	DECLARE_DYNAMIC(CAvatarDlg)

public:
	CAvatarDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CAvatarDlg();

// 对话框数据
	enum { IDD = IDD_AVATAR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

	void ChangeAvatar(Avatar_Part eAvatarPart);
	void RecursiveGetGeometry(NiNode* pNode, std::vector<NiGeometry*>& vGeometry);

	CAvatarItemListDlg* m_pAvatarItemListDlg;
public:
	afx_msg void OnBnClickedAvatarHair();
	afx_msg void OnBnClickedAvatarHead();
	afx_msg void OnBnClickedAvatarShoulderLeft();
	afx_msg void OnBnClickedAvatarShoulderRight();
	afx_msg void OnBnClickedAvatarBracersLeft();
	afx_msg void OnBnClickedAvatarBracersRight();
	afx_msg void OnBnClickedAvatarChest();
	afx_msg void OnBnClickedAvatarGlovesLeft();
	afx_msg void OnBnClickedAvatarGlovesRight();
	afx_msg void OnBnClickedAvatarLefthand();
	afx_msg void OnBnClickedAvatarRighthand();
	//afx_msg void OnBnClickedAvatarBelt();
	afx_msg void OnBnClickedAvatarTrousersLeft();
	afx_msg void OnBnClickedAvatarTrousersRight();
	afx_msg void OnBnClickedAvatarShoesLeft();
	afx_msg void OnBnClickedAvatarShoesRight();
	afx_msg void OnBnClickedAvatarSave();
	afx_msg void OnBnClickedAvatarLoad();
public:
	afx_msg void OnBnClickedAvatarArmLetf();
public:
	afx_msg void OnBnClickedAvatarArmRight();
};

#endif