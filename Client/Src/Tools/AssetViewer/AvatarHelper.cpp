#include "StdAfx.h"
#include "AvatarHelper.h"

bool IsMeshLoaded(NiNode* pRootNode, Avatar_Part eAvatarPart, NiFixedString& strModelName)
{
	//
	// Get Object
	// Get Extra Data
	NiFixedString strPartName =	AvatarNodeName[eAvatarPart];

	NiAVObject* pAVObj = pRootNode->GetObjectByName(strPartName);
	if(!pAVObj)
		return false;

	NiStringExtraData* strExtraData;
	strExtraData = (NiStringExtraData*)pAVObj->GetExtraData(strPartName);
	if(!strExtraData)
		return false;
	if(strcmp(strExtraData->GetValue(), strModelName) != 0)
		return false;

	return true;
}

bool IsTextureLoaded(NiNode* pRootNode, Avatar_Part eAvatarPart, NiFixedString& strTextureName)
{
	//
	// Get Object
	// Get Texture Name
	NiFixedString strPartName = AvatarNodeName[eAvatarPart];

	NiAVObject* pAVObj = pRootNode->GetObjectByName(strPartName);
	if(!pAVObj)
		return false;

	NiGeometry* pGemo = NULL;
	RecursiveGetGeometry(pAVObj, &pGemo);
	if(!pGemo)
		return false;

	NiTexturingProperty* pTexProperty = (NiTexturingProperty*)pGemo->GetProperty(NiProperty::TEXTURING);
	if(!pTexProperty)
		return false;

	NiTexturingProperty::Map* pBaseMap = pTexProperty->GetBaseMap();
	if(!pBaseMap)
		return false;

	NiSourceTexture* pTexture = (NiSourceTexture*)pBaseMap->GetTexture();
	if(!pTexture)
		return false;

	if(strcmp(strTextureName, pTexture->GetFilename()) != 0)
		return false;

	return true;
}

void RecursiveGetGeometry(NiAVObject* pObject, NiGeometry** pGeometry)
{
	if(NiIsKindOf(NiGeometry, pObject))
	{
		*pGeometry = (NiGeometry*)pObject;
	}
	else if(NiIsKindOf(NiNode, pObject))
	{
		NiNode* pkNode = (NiNode*)pObject;
		for(unsigned int ui = 0; ui < pkNode->GetArrayCount(); ++ui)
		{
			if(*pGeometry == NULL)
			{
				NiAVObject* pkChild = pkNode->GetAt(ui);
				if(pkChild)
					RecursiveGetGeometry(pkChild, pGeometry);
			}
			else
			{
				return;
			}
		}
	}
}

Avatar_Part GetAvatarPartId(char* str)
{
	if(strcmp(str, "Hair") == 0)
		return Avatar_Hair;
	else if(strcmp(str, "Head") == 0)
		return Avatar_Head;
	else if(strcmp(str, "Shoulder") == 0)
		return Avatar_Shoulder;
	else if(strcmp(str, "Chest") == 0)
		return Avatar_Chest;
	else if(strcmp(str, "Arm") == 0)
		return Avatar_Arm;
	else if(strcmp(str, "Bracers") == 0)
		return Avatar_Bracers;
	else if(strcmp(str, "Gloves") == 0)
		return Avatar_Gloves;
	//else if(strcmp(str, "Belt") == 0)
	//	return Avatar_Belt;
	else if(strcmp(str, "Legs") == 0)
		return Avatar_Legs;
	else if(strcmp(str, "Boots") == 0)
		return Avatar_Boots;
	else if(strcmp(str, "Cape") == 0)
		return Avatar_Cape;
	else if(strcmp(str, "Earbob") == 0)
		return Avatar_Earbob;
	else if(strcmp(str, "Necklace") == 0)
		return Avatar_Necklace;
	else if(strcmp(str, "Quiver") == 0)
		return Avatar_Quiver;
	else if(strcmp(str, "Accouterment") == 0)
		return Avatar_Accouterment;
	else if(strcmp(str, "Talisman") == 0)
		return Avatar_Talisman;
	else if(strcmp(str, "LeftHand") == 0)
		return Avatar_LeftHand;
	else if(strcmp(str, "RightHand") == 0)
		return Avatar_RightHand;
	else
		return Avatar_Max;
}