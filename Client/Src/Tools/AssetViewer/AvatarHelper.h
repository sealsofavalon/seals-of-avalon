#ifndef AVATARHELP_H
#define AVATARHELP_H

enum Avatar_Part
{
	Avatar_Hair,		// 发型
	Avatar_Head,		// 头
	Avatar_Shoulder,	// 肩部
	Avatar_Chest,		// 胸部
	Avatar_Arm,			//上臂
	Avatar_Bracers,		// 护腕
	Avatar_Gloves,		// 手套
	//Avatar_Belt,		// 腰带
	Avatar_Legs,		// 裤子
	Avatar_Boots,		// 鞋子

	Avatar_Cape,		// 披风
	Avatar_Earbob,		// 耳环
	Avatar_Necklace,	// 项链
	Avatar_Quiver,		// 箭桶
	Avatar_Accouterment,// 饰品
	Avatar_Talisman,	// 法宝
	Avatar_LeftHand,	// 左手武器
	Avatar_RightHand,	// 右手武器
	Avatar_Max
};

enum Avatar_Type
{
	Avatar_Model,
	Avatar_Textrue,
	Avatar_Invalid
};

static char RoleType[8][16] = {
	{""},
	{""},
	{""},
	{""},
	{""},
	{""},
	{""},
	{""}
};

static char AvatarNodeName[Avatar_Max][16] = { 
	{"Hair"},
	{"Head"},
	{"Shoulder"},
	{"Chest"},
	{"Arm"},
	{"Bracers"},
	{"Gloves"},
	//{"Belt"},
	{"Legs"},
	{"Boots"},
	{"Cape"},
	{"Earbob"},
	{"Necklace"},
	{"Quiver"},
	{"Accouterment"},
	{"Talisman"},
	{"LeftHand"},
	{"RightHand"}
};

bool IsMeshLoaded(NiNode* pRootNode, Avatar_Part eAvatarPart, NiFixedString& strModelName);
bool IsTextureLoaded(NiNode* pRootNode, Avatar_Part eAvatarPart, NiFixedString& strTextureName);
void RecursiveGetGeometry(NiAVObject* pObject, NiGeometry** pGeometry);
Avatar_Part GetAvatarPartId(char* str);

#endif