#include "stdafx.h"
#include "AvatarItemDataBase.h"
#include "AssetViewer.h"
#include <fstream>
#include <string>

SItemRecord::SItemRecord(const char* line)
{
	std::string str = line;
	int offset = 0;
	int n = 0;
	std::string subStr;
	
	//
	offset = str.find_first_of(",");
	subStr = str.substr(0, offset);
	roleType = atoi(subStr.c_str());
	//
	n = offset + 1;
	offset = str.find_first_of(",", n);
	subStr = str.substr(n, offset - n);
	avatarType = subStr.c_str();
	//
	n = offset + 1;
	offset = str.find_first_of(",", n);
	subStr = str.substr(n, offset - n);
	itemId = atoi(subStr.c_str());
	//
	n = offset + 1;
	offset = str.find_first_of(",", n);
	subStr = str.substr(n, offset - n);
	//textureId = atoi(subStr.c_str());
	texture = subStr.c_str();
	//
	n = offset + 1;
	offset = str.find_first_of(",", n);
	subStr = str.substr(n);
	itemDescribe = subStr.c_str();
}

CAvatarItemDataBase::CAvatarItemDataBase(const char *filename)
{
	// Open .csv file
	std::ifstream fin(theApp.GetApplicationDirectory() + "..\\Avatar.csv");
	char line[512];
	// skip 1 line
	fin.getline(line, 512);
	while(fin.getline(line, 512)) 
	{
		SItemRecord rec(line);
		m_stItems.push_back(rec);
	}
	fin.close();
}