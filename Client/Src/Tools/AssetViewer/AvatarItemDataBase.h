#pragma once

#include <vector>
#include <map>

struct SItemRecord
{
	int roleType;
	int itemId;
	//int textureId;
	CString texture;
	CString avatarType;
	CString itemDescribe;

	SItemRecord(const char* line);
};

class CAvatarItemDataBase
{
public:
	CAvatarItemDataBase(const char* filename);
	CAvatarItemDataBase() {};

public:
	std::vector<SItemRecord> m_stItems;
	std::map<int, int> m_ItemLoopup;
};

