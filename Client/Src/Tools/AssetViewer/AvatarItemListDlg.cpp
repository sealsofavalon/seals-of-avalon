// AvatarItemListDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AssetViewer.h"
#include "AvatarItemListDlg.h"
#include "NifDoc.h"
#include <fstream>
#include "AvatarItemDataBase.h"
#include "AvatarNode.h"
#include "BackLoadQueue.h"
#include "NifRenderView.h"

#define _BackLoad_
// CAvatarItemListDlg 对话框

IMPLEMENT_DYNAMIC(CAvatarItemListDlg, CDialog)

CAvatarItemListDlg::CAvatarItemListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAvatarItemListDlg::IDD, pParent)
{
	Create(IDD_AVATAR_ITEM_LIST);
}

CAvatarItemListDlg::~CAvatarItemListDlg()
{
}

void CAvatarItemListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_AVATAR_ITEM_LIST, m_ItemList);
}


BEGIN_MESSAGE_MAP(CAvatarItemListDlg, CDialog)
	ON_LBN_DBLCLK(IDC_AVATAR_ITEM_LIST, &CAvatarItemListDlg::OnLbnSelchangeAvatarItemList)
END_MESSAGE_MAP()


void CAvatarItemListDlg::InitDlg(Avatar_Part eAvatarPart /* = CAvatarNode::Avatar_Head */)
{
	m_ItemIndex.clear();
	m_ItemList.ResetContent();

	m_eAvatarPart = eAvatarPart;

	CAvatarItemDataBase* pBase = CNifDoc::GetDocument()->GetAvatarItemDataBasePtr();
	for(unsigned int ui = 0; ui < pBase->m_stItems.size(); ++ui)
	{
		if(pBase->m_stItems[ui].avatarType == AvatarNodeName[eAvatarPart])
		{
			m_ItemIndex.push_back(ui);
			m_ItemList.InsertString(m_ItemIndex.size() -  1, CNifDoc::GetDocument()->GetAvatarItemDataBasePtr()->m_stItems[ui].itemDescribe);
		}
	}

	ShowWindow(SW_SHOW);
	UpdateWindow();
	m_reIndex = -1;
}

// CAvatarItemListDlg 消息处理程序

void CAvatarItemListDlg::OnLbnSelchangeAvatarItemList()
{
	// TODO: 在此添加控件通知处理程序代码

	// Directory
	//const char* Directory = NiImageConverter::GetPlatformSpecificSubdirectory();
	NiImageConverter::SetPlatformSpecificSubdirectory(NULL);
	
	m_reIndex = m_ItemList.GetCurSel();

	char strModelName[128];
	sprintf(strModelName, "%d.nif", CNifDoc::GetDocument()->GetAvatarItemDataBasePtr()->m_stItems[m_reIndex].itemId);

	ChangeAvatar((CString)strModelName);

	// Directory
	//NiImageConverter::SetPlatformSpecificSubdirectory(Directory);
}

void CAvatarItemListDlg::ChangeAvatar(CString& strModelName)
{
	CNifDoc* pkDoc = CNifDoc::GetDocument();
	pkDoc->Lock();

	NiNode* pkScene = pkDoc->GetSceneGraph();
	if(pkScene)
	{
		CAvatarNode pAvatarNode;
		// Check Mesh
		if(IsMeshLoaded(pkScene, m_eAvatarPart, (NiFixedString)strModelName))
		{
			// Only Change Texture
			ChangeTexture(pkScene, &pAvatarNode);
		}
		else
		{
			// Change Mesh & Texture
			ChangeMesh(pkScene, &pAvatarNode);
			ChangeTexture(pkScene, &pAvatarNode);
		}
	}

	pkDoc->UnLock();
}

void CAvatarItemListDlg::ChangeMesh(NiNode* pRootNode, CAvatarNode* pAvatarNode)
{
	char strFullPathName[_MAX_PATH];
	sprintf(strFullPathName, "%s..\\%s\\%s\\%d.nif", 
		theApp.GetApplicationDirectory(), 
		RoleType[CNifDoc::GetDocument()->GetAvatarItemDataBasePtr()->m_stItems[m_reIndex].roleType],
		AvatarNodeName[(int)(GetAvatarPartId(CNifDoc::GetDocument()->GetAvatarItemDataBasePtr()->m_stItems[m_reIndex].avatarType.GetBuffer()))],
		CNifDoc::GetDocument()->GetAvatarItemDataBasePtr()->m_stItems[m_reIndex].itemId);

	char strFileName[_MAX_PATH];
	sprintf(strFileName, "%d.nif", CNifDoc::GetDocument()->GetAvatarItemDataBasePtr()->m_stItems[m_reIndex].itemId);

#ifndef _BackLoad_
	NiStream kStream;
	bool bSuccess = kStream.Load(strFullPathName);
	if(bSuccess)
	{
		NiObject* pObject = kStream.GetObjectAt(0);

		ChangeAvatarMesh(pRootNode, pAvatarNode, pObject, strFileName, m_eAvatarPart);

		// ChangeMesh Need Update View
		// May case some bugs
		MainThreadUpdateAllViews(MAKELPARAM(NIF_SCENECHANGED, 0));
	}
#else
	// BackLoad
	CBackLoadEntry* entry = CNifRenderView::ms_pBackLoadManager->OpenBackLoadQueue();
	entry->m_iPart = m_eAvatarPart;
	entry->m_sType = CBackLoadEntry::NIF_Entry;
	entry->m_strFileName = strFileName;
	entry->m_strPathName = strFullPathName;
	entry->m_pDlg = (DWORD)this;
	CNifRenderView::ms_pBackLoadManager->CloseBackLoadQueue(entry);
#endif
}

void CAvatarItemListDlg::ChangeAvatarMesh(NiNode* pRootNode, CAvatarNode* pAvatarNode, NiObject* pObject, char* fileName, Avatar_Part eAvatarPart)
{
	if(pObject)
	{
		CAvatarNode::SAvatarInfo sAvatarInfo;
		sAvatarInfo.Type = Avatar_Model;
		sAvatarInfo.Part = eAvatarPart;
		sAvatarInfo.strMeshName = fileName;
		pAvatarNode->ChangeAvatar(pRootNode, pObject, sAvatarInfo);
	}
}

void CAvatarItemListDlg::ChangeTexture(NiNode* pRootNode, CAvatarNode* pAvatarNode)
{
	char strFullPathName[_MAX_PATH];
	//sprintf(strFullPathName, "%s..\\%d.tga", theApp.GetApplicationDirectory(), CNifDoc::GetDocument()->GetAvatarItemDataBasePtr()->m_stItems[m_reIndex].textureId);
	sprintf(strFullPathName, "%s..\\%s\\%s\\%s.tga", 
		theApp.GetApplicationDirectory(), 
		RoleType[CNifDoc::GetDocument()->GetAvatarItemDataBasePtr()->m_stItems[m_reIndex].roleType],
		AvatarNodeName[(int)(GetAvatarPartId(CNifDoc::GetDocument()->GetAvatarItemDataBasePtr()->m_stItems[m_reIndex].avatarType.GetBuffer()))],
		CNifDoc::GetDocument()->GetAvatarItemDataBasePtr()->m_stItems[m_reIndex].texture);

#ifndef _BackLoad_
	if(IsTextureLoaded(pRootNode, m_eAvatarPart, (NiFixedString)strFullPathName))
		return;

	NiTexture* pTexture = NiSourceTexture::Create(strFullPathName);
	if(pTexture)
		ChangeAvatarTexture(pRootNode, pAvatarNode, pTexture, m_eAvatarPart);
#else
	// BackLoad
	CBackLoadEntry* entry = CNifRenderView::ms_pBackLoadManager->OpenBackLoadQueue();
	entry->m_iPart = m_eAvatarPart;
	entry->m_sType = CBackLoadEntry::Texture_Entry;
	entry->m_strFileName = "";
	entry->m_strPathName = strFullPathName;
	entry->m_pDlg = (DWORD)this;
	CNifRenderView::ms_pBackLoadManager->CloseBackLoadQueue(entry);
#endif
}

void CAvatarItemListDlg::ChangeAvatarTexture(NiNode* pRootNode, CAvatarNode* pAvatarNode, NiTexture* pTexture, Avatar_Part eAvatarPart)
{
	NiTexturingPropertyPtr pTexProperty = NiNew NiTexturingProperty();
	if(pTexture && pTexProperty)
	{
		pTexProperty->SetBaseTexture(pTexture);	

		CAvatarNode::SAvatarInfo sAvatarInfo;
		sAvatarInfo.Type = Avatar_Textrue;
		sAvatarInfo.Part = eAvatarPart;
		pAvatarNode->ChangeAvatar(pRootNode, (NiObject*)pTexProperty, sAvatarInfo);
	}
}

void CAvatarItemListDlg::OnLoadFinish(NiObject* pObject, CBackLoadEntry* entry)
{
	CNifDoc* pkDoc = CNifDoc::GetDocument();

	NiNode* pkScene = pkDoc->GetSceneGraph();
	if(pkScene)
	{
		CAvatarNode pAvatarNode;

		switch(entry->m_sType)
		{
		case CBackLoadEntry::NIF_Entry:
			{
				ChangeAvatarMesh(pkScene, &pAvatarNode, pObject, entry->m_strFileName.GetBuffer(), (Avatar_Part)entry->m_iPart);
				MainThreadUpdateAllViews(MAKELPARAM(NIF_SCENECHANGED, 0));
			}
			break;

		case CBackLoadEntry::Texture_Entry:
			{
				ChangeAvatarTexture(pkScene, &pAvatarNode, (NiTexture*)pObject, (Avatar_Part)entry->m_iPart);
			}
			break;
		
		default:
			break;
		}
	}
}

#ifdef _EditVersion_
void Update()
{
	CBackLoadEntry* entry = CNifRenderView::ms_pBackLoadManager->OpenBackLoadQueue();
	if(entry->InFinish())
	{
		OnLoadFinish(entry->GetNiObject(), entry);
	}
}
#endif