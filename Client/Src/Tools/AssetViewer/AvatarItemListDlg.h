#ifndef AVATARITEMDLG_H
#define AVATARITEMDLG_H

#include "afxwin.h"
#include <vector>
#include "AvatarHelper.h"

class CAvatarNode;
class CBackLoadEntry;
// CAvatarItemListDlg 对话框

class CAvatarItemListDlg : public CDialog
{
	DECLARE_DYNAMIC(CAvatarItemListDlg)

public:
	CAvatarItemListDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CAvatarItemListDlg();

// 对话框数据
	enum { IDD = IDD_AVATAR_ITEM_LIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	void InitDlg(Avatar_Part eAvatarPart = Avatar_Head);

	void ChangeAvatar(CString& strModelName);

	void ChangeMesh(NiNode* pRootNode, CAvatarNode* pAvatarNode);
	void ChangeAvatarMesh(NiNode* pRootNode, CAvatarNode* pAvatarNode, NiObject* pObject, char* fileName, Avatar_Part eAvatarPart);

	void ChangeTexture(NiNode* pRootNode, CAvatarNode* pAvatarNode);
	void ChangeAvatarTexture(NiNode* pRootNode, CAvatarNode* pAvatarNode, NiTexture* pTexture, Avatar_Part eAvatarPart);

	void OnLoadFinish(NiObject* pObject, CBackLoadEntry* entry);
	//void GetMeshFileName(char* meshFileName);
	//void GetTextureFileName(char* texFileName);

	CListBox m_ItemList;

	int	m_reIndex;
public:
	afx_msg void OnLbnSelchangeAvatarItemList();

private:
	Avatar_Part m_eAvatarPart;
	std::vector<int> m_ItemIndex;
};

#endif