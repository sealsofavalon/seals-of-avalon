#include "stdafx.h"
#include "AvatarNode.h"

#define _EditVersion_

CAvatarNode::CAvatarNode()
{
}

CAvatarNode::~CAvatarNode()
{
}

bool CAvatarNode::ChangeAvatar(NiActorManager* pActorManager, NiObject* pAvatarObject, SAvatarInfo sAvatarInfo)
{
	if(!pActorManager || !pAvatarObject)
		return false;

	// 解析Avatar_Type
	m_strAvatarNodeName = AvatarNodeName[sAvatarInfo.Part];
	m_strAvatarMeshName = sAvatarInfo.strMeshName;

	// ChangeMesh or ChangeTexture
	NiNode* pRootNode = (NiNode*)pActorManager->GetNIFRoot();
	if(sAvatarInfo.Type == Avatar_Model)
		ChangeMesh(pRootNode, (NiNode*)pAvatarObject);
	else
		ChangeTexture(pRootNode, (NiProperty*)pAvatarObject);

	pRootNode->UpdateProperties();
	pRootNode->Update(0.0f);

	return true;
}

bool CAvatarNode::ChangeAvatar(NiNode* pRootNode, NiObject* pAvatarObject, SAvatarInfo sAvatarInfo)
{
	if(!pRootNode || !pAvatarObject)
		return false;

	// 解析Avatar_Type
	m_strAvatarNodeName = AvatarNodeName[sAvatarInfo.Part];
	m_strAvatarMeshName = sAvatarInfo.strMeshName;

	// ChangeMesh or ChangeTexture
	if(sAvatarInfo.Type == Avatar_Model)
		ChangeMesh(pRootNode, (NiNode*)pAvatarObject);
	else
		ChangeTexture(pRootNode, (NiProperty*)pAvatarObject);

	pRootNode->UpdateProperties();
	pRootNode->Update(0.0f);

	return true;
}

bool CAvatarNode::ChangeMesh(NiNode* pRootNode, NiNode* pAvatarNode)
{
#ifdef _EditVersion_
	pRootNode = (NiNode*)pRootNode->GetObjectByName("Scene Root");
	if(!pRootNode)
		return false;
#endif
	NiAVObject* pAVObj = pRootNode->GetObjectByName(m_strAvatarNodeName);
	NiAVObject* pAvatarObj = pAvatarNode->GetObjectByName(m_strAvatarNodeName);
	if(!pAVObj || !pAvatarObj)
		return false;

	NiGeometry* pGemo = NULL;
	NiGeometry* pAvatarGemo = NULL;
	RecursiveGetGeometry(pAVObj, &pGemo);
	RecursiveGetGeometry(pAvatarObj, &pAvatarGemo);
	if(!pGemo || !pAvatarGemo)
		return false;

	NiSkinInstance* pSkinIns = pGemo->GetSkinInstance();
	NiSkinInstance* pAvatarSkinIns = pAvatarGemo->GetSkinInstance();
	if(!pSkinIns || !pAvatarSkinIns)
		return false;

	NiSkinData* pSkinData = pSkinIns->GetSkinData();
	if(!pSkinData)
		return false;

	for(unsigned int ui = 0; ui < pSkinData->GetBoneCount(); ++ui)
	{
		//const NiFixedString strBoneName = pSkinIns->GetBones()[ui]->GetName();
		pAvatarSkinIns->SetBone(ui, pSkinIns->GetBones()[ui]);
	}

	pAvatarSkinIns->SetRootParent(pSkinIns->GetRootParent());

	// Add ExtraData
	NiStringExtraData* pExtraData = NiNew NiStringExtraData(m_strAvatarMeshName);
	pExtraData->SetName(m_strAvatarNodeName);
	pAvatarObj->AddExtraData(pExtraData);

	// Detach/Attach之前是否需要clone
	//NiAVObjectPtr pAvatarClone = (NiAVObject*)pAvatarGemo->Clone();
	pRootNode->DetachChild(pAVObj);
	pRootNode->AttachChild(pAvatarObj);

	return true;
}

bool CAvatarNode::ChangeTexture(NiNode* pRootNode, NiProperty* pNewProperty)
{
	NiNode* pNode = (NiNode*)pRootNode->GetObjectByName(m_strAvatarNodeName);
	if(!pNode)
		return false;

	NiGeometry* pGemo = NULL;
	RecursiveGetGeometry(pNode, &pGemo);
	NiTexturingProperty* pOldProperty = (NiTexturingProperty*)pGemo->GetProperty(NiProperty::TEXTURING);
	if(pOldProperty)
		pGemo->RemoveProperty(NiProperty::TEXTURING);

	pGemo->AttachProperty(pNewProperty);

	return true;
}

#ifdef _ComposeTexture_
bool CAvatarNode::ComposeTexture(NiTexturingProperty* pDestProperty, NiTexturingProperty* pSrcProperty)
{
	NiTexturingProperty::Map* pDestBaseMap = pDestProperty->GetBaseMap();
	NiTexturingProperty::Map* pSrcBaseMap = pSrcProperty->GetBaseMap();
	if(!pDestBaseMap || !pSrcBaseMap)
		return false;

	NiTexture* pDestTexture = (NiTexture*)pDestBaseMap->GetTexture();
	NiTexture* pSrcTexture = (NiTexture*)pSrcBaseMap->GetTexture();
	if(!pDestTexture || !pSrcTexture)
		return false;

	NiTexture::RendererData* pDestRendererData = pDestTexture->GetRendererData();
	NiTexture::RendererData* pSrcRendererData = pSrcTexture->GetRendererData();
	if(pDestRendererData && NiIsKindOf(NiDX9TextureData, pDestRendererData) &&
		pSrcRendererData && NiIsKindOf(NiDX9TextureData, pSrcRendererData))
	{
		NiDX9TextureData* pDestDX9TextureData = (NiDX9TextureData*)pDestRendererData;
		NiDX9TextureData* pSrcDX9TextureData = (NiDX9TextureData*)pSrcRendererData;

		LPDIRECT3DTEXTURE9 pDestTexture = (LPDIRECT3DTEXTURE9)pDestDX9TextureData->GetD3DTexture();
		LPDIRECT3DTEXTURE9 pSrcTexture = (LPDIRECT3DTEXTURE9)pSrcDX9TextureData->GetD3DTexture();

		LPDIRECT3DSURFACE9 pDestSurface;
		LPDIRECT3DSURFACE9 pSrcSurface;

		RECT pDestRect = {0, 0, 64, 64};

		int total = pDestTexture->GetLevelCount();
		for(int i = 0; i < total; ++i)
		{
			pDestRect.right = max(64 / pow(2.0f, i), 1.0);
			pDestRect.bottom = max(64 / pow(2.0f, i), 1.0);

			pDestTexture->GetSurfaceLevel(i, &pDestSurface);
			pSrcTexture->GetSurfaceLevel(0, &pSrcSurface);

			D3DXLoadSurfaceFromSurface(pDestSurface,
				NULL,
				&pDestRect,
				pSrcSurface,
				NULL,
				NULL,
				D3DX_FILTER_LINEAR,
				0xFF000000);

			pDestSurface->Release();
			pDestSurface = NULL;

			pSrcSurface->Release();
			pSrcSurface = NULL;
		}
	}
}
#endif