#pragma once
#include <vector>
#include "AvatarHelper.h"

class CAvatarNode// : public NiRefObject
{
public:
	CAvatarNode();
	~CAvatarNode();

public:
	struct SAvatarInfo
	{
		Avatar_Part Part;
		Avatar_Type Type;
		NiFixedString strMeshName;
	};
public:
	//bool ChangeAvatar(NiActorManager* pActorManager, NiRefObject* pAvatarObject, std::vector<SAvatarInfo>& sAvatarInfo);
	//bool ChangeAvatar(NiNode* pRootNode, NiRefObject* pAvatarObject, std::vector<SAvatarInfo>& sAvatarInfo);
	bool ChangeAvatar(NiActorManager* pActorManager, NiObject* pAvatarObject, SAvatarInfo sAvatarInfo);
	bool ChangeAvatar(NiNode* pRootNode, NiObject* pAvatarObject, SAvatarInfo sAvatarInfo);

	// Help Func...
	//bool IsMeshLoaded(NiNode* pRootNode, Avatar_Part eAvatarPart, NiFixedString& strModelName);
	//bool IsTextureLoaded(NiNode* pRootNode, Avatar_Part eAvatarPart, NiFixedString& strModelName);
protected:
	bool ChangeMesh(NiNode* pRootNode, NiNode* pAvatarNode);
	bool ChangeTexture(NiNode* pRootNode, NiProperty* pNewProperty);

#ifdef _ComposeTexture_
	bool ComposeTexture(NiTexturingProperty* pDestProperty, NiTexturingProperty* pSrcProperty);
#endif

	// Help func...
	//void RecursiveGetGeometry(NiAVObject* pObject, NiGeometry** pGeometry);

	//void SetAvatarPartName(Avatar_Part eAvatarPart);
	//void GetAvatarPartName(Avatar_Part eAvatarPart, NiFixedString& strPartName);

	// Local member
	NiFixedString m_strAvatarNodeName;
	NiFixedString m_strAvatarMeshName;
};
