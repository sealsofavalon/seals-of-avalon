#include "stdafx.h"
#include "BackLoadManager.h"

CBackLoadManager::CBackLoadManager()
:m_bWaiting(false)
{
}

CBackLoadManager::~CBackLoadManager()
{
}

void CBackLoadManager::Init()
{
/*
	NiParallelUpdateTaskManager::Initialize(32);
	NiParticleSystem::SetParallelUpdateEnabled(true);
	NiGeomMorpherController::SetParallelUpdateEnabled(true);
*/
}

void CBackLoadManager::Destroy()
{
	m_Stream.BackgroundLoadFinish(); // in case of exit while still loading.
	m_Stream.RemoveAllObjects();

	m_Texture.BackgroundLoadFinish();
/*
	NiParallelUpdateTaskManager::Shutdown();
*/
}

void CBackLoadManager::BeginUpdate()
{
/*
	NiParallelUpdateTaskManager::BeginUpdate();
*/
}

void CBackLoadManager::EndUpdate()
{
/*
	NiParallelUpdateTaskManager::EndUpdate();
*/
}

void CBackLoadManager::WaitRenderSema()
{
/*
	NiParallelUpdateTaskManager::WaitRenderSema();
*/
}

void CBackLoadManager::SignalRenderSema()
{
/*
	NiParallelUpdateTaskManager::SignalRenderSema();
*/
}

void CBackLoadManager::LoadTexture(CBackLoadEntry* entry)
{
	CallBackTexture::LoadState kLoadState;
	CallBackTexture::ThreadStatus eStatus = m_Texture.BackgroundLoadPoll(
		&kLoadState);

	if(m_bWaiting)
	{
		NiSleep(1);
		if(eStatus == CallBackTexture::IDLE)
		{
			m_bWaiting = false;
			m_BackLoadQueue.Execute((NiObject*)m_Texture.GetTexture());
			OutputDebugString("Texture\n");
		}
	}
	else
	{
		m_Texture.BackgroundLoadBegin(entry->m_strPathName);
		m_bWaiting = true;
	}
}

void CBackLoadManager::LoadNifStream(CBackLoadEntry* entry)
{
	NiStream::LoadState kLoadState;
	NiStream::ThreadStatus eStatus = m_Stream.BackgroundLoadPoll(
		&kLoadState);

	if(m_bWaiting)
	{
		NiSleep(1);
		if(eStatus == NiStream::IDLE)
		{
			m_bWaiting = false;
			NiObject* pObject = m_Stream.GetObjectAt(0);
			m_BackLoadQueue.Execute(pObject);
			OutputDebugString("Mesh\n");
		}
	}
	else
	{
		m_Stream.BackgroundLoadBegin(entry->m_strPathName);
		m_bWaiting = true;
	}
}

void CBackLoadManager::OnIdle()
{
	CBackLoadEntry* entry = m_BackLoadQueue.GetEntry();
	if(entry)
	{
		switch(entry->m_sType)
		{
		case CBackLoadEntry::NIF_Entry:
			LoadNifStream(entry);
			break;

		case CBackLoadEntry::Texture_Entry:
			LoadTexture(entry);
			break;

		default:
			break;
		}
	}
}

CBackLoadEntry* CBackLoadManager::OpenBackLoadQueue()
{
	return m_BackLoadQueue.LockBackLoadEntry();
}

void CBackLoadManager::CloseBackLoadQueue(CBackLoadEntry* entry)
{
	m_BackLoadQueue.UnlockBackLoadEntry(entry);
}