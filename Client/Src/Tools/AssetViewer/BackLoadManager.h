#ifndef BACKLOADMANAGER_H
#define BACKLOADMANAGER_H

#include "BackLoadQueue.h"
#include "CallBackResource.h"

class CBackLoadManager// : public Singleton
{
public:
	CBackLoadManager();
	~CBackLoadManager();

	void Init();

	void Destroy();

	void BeginUpdate();
	void EndUpdate();

	void WaitRenderSema();
	void SignalRenderSema();

	void OnIdle();

	CBackLoadEntry* OpenBackLoadQueue();
	void CloseBackLoadQueue(CBackLoadEntry* entry);

protected:
	void LoadNifStream(CBackLoadEntry* entry);
	void LoadTexture(CBackLoadEntry* entry);

private:
	CBackLoadQueue m_BackLoadQueue;
	CallbackStream m_Stream;
	CallBackTexture m_Texture;
	bool m_bWaiting;
};

#endif