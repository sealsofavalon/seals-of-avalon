#include "stdafx.h"
#include "BackLoadQueue.h"
#include "NifDoc.h"
#include "AvatarNode.h"
#include "AvatarItemListDlg.h"

void CBackLoadEntry::Clear()
{
	m_pObject = NULL;
	m_iPart = 0;
	m_sType = Invalid_Entry;
	m_bFinish = false;
	m_pDlg = 0;
}

void CBackLoadEntry::OnLoadFinish(NiObject* pObject)
{
	if(pObject)
	{
		m_pObject = pObject;
		m_bFinish = true;
	}
}

CBackLoadQueue::CBackLoadQueue()
{
	m_activeEntries = 0;
}

CBackLoadQueue::~CBackLoadQueue()
{
}

CBackLoadEntry* CBackLoadQueue::LockBackLoadEntry()
{
	if(m_activeEntries + 1 == k_maxBackLoadEntries)
	{
		// we are out of space.
		// Execute (and Reset) 
		// the current queue
		;
	}

	CBackLoadEntry* entry = &m_entryPool[m_activeEntries];
	entry->Clear();

	return entry;
}

void CBackLoadQueue::UnlockBackLoadEntry(CBackLoadEntry* entry)
{
	//m_entryList[m_activeEntries] = entry;
	m_entryQueue.push(entry);
	++m_activeEntries;
}

void CBackLoadQueue::Reset()
{
}

void CBackLoadQueue::Execute(NiObject* pObject)
{
	CBackLoadEntry* entry = m_entryQueue.front();
	if(pObject)
	{
#ifndef _GemaVersion_
		((CAvatarItemListDlg*)entry->m_pDlg)->OnLoadFinish(pObject, entry);
#else
		entry->OnLoadFinish(pObject);
#endif
	}
	
	m_entryQueue.pop();
	m_activeEntries--;
}

CBackLoadEntry* CBackLoadQueue::GetEntry()
{
	if(m_entryQueue.size())
		return m_entryQueue.front();
	else
		return NULL;
}