#ifndef BACKLOADQUEUE_H
#define BACKLOADQUEUE_H

#include <NiStream.h>
#include <queue>

class NiAVObject;

class CBackLoadEntry
{
friend class CBackLoadQueue;
public:
	CBackLoadEntry() {};
	~CBackLoadEntry() {};

	enum EntryType
	{
		NIF_Entry,
		Texture_Entry,
		Sound_Entry,
		Invalid_Entry
	};

	void Clear();
	bool InFinish() { return m_bFinish; }
	NiObject* GetNiObject() { return m_pObject; }

public:
	CString m_strPathName;
	CString m_strFileName;
	int m_iPart;
	EntryType m_sType;
	DWORD m_pDlg;

protected:
	void OnLoadFinish(NiObject* pObject);
	NiObject* m_pObject;
	bool m_bFinish;
};

class CBackLoadQueue
{
friend class CBackLoadManager;
public:
	CBackLoadQueue();
	~CBackLoadQueue();

	enum eConstants
	{
		k_maxBackLoadEntries = 1024 
	};

	CBackLoadEntry* LockBackLoadEntry();
	void UnlockBackLoadEntry(CBackLoadEntry* entry);

	CBackLoadEntry* GetEntry();

protected:
	void Reset();
	void Execute(NiObject* pObject);

private:
	CBackLoadEntry m_entryPool[k_maxBackLoadEntries];

	unsigned int m_activeEntries;
	std::queue<CBackLoadEntry*> m_entryQueue;
};

#endif