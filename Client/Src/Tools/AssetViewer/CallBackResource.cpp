#include "stdafx.h"
#include "CallBackResource.h"

// Nif Stream
void CallbackStream::BackgroundLoadOnExit()
{
	;
}

void CallbackStream::Precache(NiAVObject* pkObject)
{
	;
}


// Texture
CallBackTexture::CallBackTexture()
:m_pkThread(0)
,m_pkBGLoadProc(0)
,m_ePriority(NiThread::BELOW_NORMAL)
{
	m_eBackgroundLoadStatus = IDLE;
}

CallBackTexture::CallBackTexture(NiFixedString strTextureName)
:m_pkThread(0)
,m_pkBGLoadProc(0)
,m_ePriority(NiThread::BELOW_NORMAL)
,m_strTextureName(strTextureName)
{
	m_eBackgroundLoadStatus = IDLE;
}

CallBackTexture::~CallBackTexture()
{
	NiDelete m_pkBGLoadProc;
}

unsigned int CallBackTexture::BackgroundLoadProcedure::ThreadProcedure(void *pvArg)
{
	m_pCallBackTexture->BackgroundLoad();

	return 0;
}

void CallBackTexture::BackgroundLoad()
{
	m_pTexture = NiSourceTexture::Create(m_strTextureName);
	if(m_pTexture)
		m_bBackgroundLoadExitStatus = true;

	// If a pause or cancel has been requested, we should do so at this point
	// since BackgroundLoadOnExit may do a non-trivial amount of work in 
	// a derived class.
	if(m_eBackgroundLoadStatus == CANCELLING)
	{
		// Cancel out and don't run BackgroundLoadOnExit
		m_bBackgroundLoadExitStatus = false;
		m_kSemaphore.Signal();
		return;
	}
	else if(m_eBackgroundLoadStatus == PAUSING)
	{
		DoThreadPause();
	}

	BackgroundLoadOnExit();
	m_kSemaphore.Signal();
}

void CallBackTexture::BackgroundLoadBegin(const char* pcFileName)
{
	m_strTextureName = pcFileName;
	BackgroundLoadBegin();
}

CallBackTexture::ThreadStatus CallBackTexture::BackgroundLoadPoll(LoadState* pkLoadState)
{
	if (m_eBackgroundLoadStatus == LOADING ||
		m_eBackgroundLoadStatus == CANCELLING ||
		m_eBackgroundLoadStatus == PAUSING)
	{
		int iPoll = m_kSemaphore.GetCount();

		// If the semaphore has been signalled, the thread is done processing.
		// As such, CANCELLING or PAUSING are irrelevant. The appropriate
		// values have been set to m_bBackgroundLoadExitStatus. Cleanup should
		// occur to set the status to IDLE.
		if (iPoll > 0)
		{
			m_kSemaphore.Wait();
			BackgroundLoadCleanup();
		}
	}

	if (pkLoadState != NULL && (m_eBackgroundLoadStatus == LOADING ||
		m_eBackgroundLoadStatus == PAUSED))
	{
		//BackgroundLoadEstimateProgress(*pkLoadState);
		;
	}

	return m_eBackgroundLoadStatus;
}

void CallBackTexture::BackgroundLoadBegin()
{
	NIASSERT(m_eBackgroundLoadStatus == IDLE);

	if(m_pkBGLoadProc == 0)
	{
		m_pkBGLoadProc = NiNew BackgroundLoadProcedure(this);
	}

	NIASSERT(m_pkThread == 0);
	m_pkThread = NiThread::Create(m_pkBGLoadProc);
	NIASSERT(m_pkThread);

	m_pkThread->SetThreadAffinity(m_kAffinity);

	m_pkThread->SetPriority(m_ePriority);
	m_pkThread->Resume();

	m_eBackgroundLoadStatus = LOADING;
}

void CallBackTexture::BackgroundLoadPause()
{
	if(m_eBackgroundLoadStatus == LOADING)
	{
		m_eBackgroundLoadStatus = PAUSING;
	}
}

void CallBackTexture::BackgroundLoadResume()
{
	if(m_eBackgroundLoadStatus == PAUSED)
	{
		m_pkThread->Resume();
		m_eBackgroundLoadStatus = LOADING;
	}
}

void CallBackTexture::BackgroundLoadCancel()
{
	if(m_eBackgroundLoadStatus == PAUSED)
	{
		BackgroundLoadResume();
	}

	if(m_eBackgroundLoadStatus == LOADING)
	{
		m_eBackgroundLoadStatus = CANCELLING;
	}
}

bool CallBackTexture::BackgroundLoadFinish()
{
	if(m_eBackgroundLoadStatus == PAUSED)
	{
		BackgroundLoadResume();
	}

	if(m_eBackgroundLoadStatus == LOADING ||
		m_eBackgroundLoadStatus == CANCELLING)
	{
		m_kSemaphore.Wait();
		BackgroundLoadCleanup();
	}

	return m_bBackgroundLoadExitStatus;
}

void CallBackTexture::BackgroundLoadCleanup()
{
	NiDelete m_pkThread;
	m_pkThread = 0;
	m_eBackgroundLoadStatus = IDLE;
}

bool CallBackTexture::BackgroundLoadGetExitStatus() const
{
	return m_bBackgroundLoadExitStatus;
}

void CallBackTexture::BackgroundLoadOnExit()
{
	;
}

void CallBackTexture::DoThreadPause()
{
	m_eBackgroundLoadStatus = PAUSED;
	m_pkThread->Suspend();
}

NiSourceTexture* CallBackTexture::GetTexture()
{
	return m_pTexture;
}