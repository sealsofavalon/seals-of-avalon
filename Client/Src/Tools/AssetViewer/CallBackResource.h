#ifndef CALLBACKRESOURCE_H
#define CALLBACKRESOURCE_H


class CallbackStream : public NiStream
{
public:
	virtual void BackgroundLoadOnExit();

protected:
	void Precache(NiAVObject* pkObject);
};


class CallBackTexture
{
public:
	CallBackTexture();
	CallBackTexture(NiFixedString strTextureName);
	~CallBackTexture();

	class BackgroundLoadProcedure : public NiThreadProcedure
	{
	public:
		BackgroundLoadProcedure(CallBackTexture* pCallBackTexture)
		{
			m_pCallBackTexture = pCallBackTexture;
		}

		virtual unsigned int ThreadProcedure(void* pvArg);
		CallBackTexture* m_pCallBackTexture;
	};

	// background loading
	enum ThreadStatus
	{
		IDLE,
		LOADING,
		CANCELLING,
		PAUSING,
		PAUSED
	};

	class LoadState : public NiMemObject
	{
	public:
		float m_fReadProgress; // [0.0f..1.0f]
		float m_fLinkProgress; // [0.0f..1.0f]
	};

	void BackgroundLoad();
	void BackgroundLoadBegin(const char* pcFileName);
	ThreadStatus BackgroundLoadPoll(LoadState* pkState);
	void BackgroundLoadPause();
	void BackgroundLoadResume();
	void BackgroundLoadCancel();
	bool BackgroundLoadFinish();
	void BackgroundLoadCleanup();
	bool BackgroundLoadGetExitStatus() const;
	void BackgroundLoadOnExit();

	NiSourceTexture* GetTexture();
protected:
	NiFixedString m_strTextureName;
	NiSourceTexture* m_pTexture;

	void DoThreadPause();

	// background loading
	void BackgroundLoadBegin();

	ThreadStatus m_eBackgroundLoadStatus;
	bool m_bBackgroundLoadExitStatus;

	// NiThread implementation
	NiSemaphore m_kSemaphore;
	NiThread* m_pkThread;
	BackgroundLoadProcedure* m_pkBGLoadProc;
	NiThread::Priority m_ePriority;
	NiProcessorAffinity m_kAffinity;
};

#endif