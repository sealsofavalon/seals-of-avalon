// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#include "EventMap.h"
#include <NiFile.h>
#include <NiStream.h>

const char* g_pcCurrentMAPVersion = "1.0";

//---------------------------------------------------------------------------
EventMap::Mapping::Mapping() :
    m_uiInputCode(EventMap::INPUT_CODE_NONE),
    m_uiModifier(EventMap::INPUT_CODE_NONE)
{
}
//---------------------------------------------------------------------------
EventMap::EventMap()
{
}
//---------------------------------------------------------------------------
EventMap::~EventMap()
{
    Mapping* pkMapping;
    unsigned int uiEventCode;
    NiTMapIterator pos = m_mapMappings.GetFirstPos();
    while (pos)
    {
        m_mapMappings.GetNext(pos, uiEventCode, pkMapping);
        m_mapMappings.RemoveAt(uiEventCode);
        delete pkMapping;
    }
}
//---------------------------------------------------------------------------
bool EventMap::AddMapping(unsigned int uiEventCode,
    unsigned int uiInputCode, unsigned int uiModifier)
{
    // Returns: true if the mapping was added successfully.
    //          false if uiInputCode is already listed as a modifier key or
    //              uiModifier is already listed as an input code.

    // This function replaces any mapping that already exists at uiEventCode
    // and removes any other mapping that uses the the combination of
    // uiInputCode and uiModifier.
    //
    // You may want to run GetEventCode and/or GetInputCodes first
    // to determine if any mappings will be replaced or removed before
    // calling this function.

    bool bTemp;
    if (m_mapModifiers.GetAt(uiInputCode, bTemp))
    {
        return false;
    }
    if (m_mapInputCodes.GetAt(uiModifier, bTemp))
    {
        return false;
    }

    Mapping* pkMapping;
    if (m_mapMappings.GetAt(uiEventCode, pkMapping))
    {
        RemoveMapping(uiEventCode);
        RemoveMapping(uiInputCode, uiModifier);
    }

    pkMapping = new Mapping;
    pkMapping->m_uiInputCode = uiInputCode;
    pkMapping->m_uiModifier = uiModifier;
    m_mapMappings.SetAt(uiEventCode, pkMapping);
    if (uiInputCode != INPUT_CODE_NONE)
    {
        m_mapInputCodes.SetAt(uiInputCode, true);
    }
    if (uiModifier != INPUT_CODE_NONE && uiModifier != INPUT_CODE_ANY)
    {
        m_mapModifiers.SetAt(uiModifier, true);
    }

    return true;
}
//---------------------------------------------------------------------------
void EventMap::RemoveMapping(unsigned int uiEventCode)
{
    Mapping* pkMapping;
    if (m_mapMappings.GetAt(uiEventCode, pkMapping))
    {
        m_mapMappings.RemoveAt(uiEventCode);
        m_mapInputCodes.RemoveAt(pkMapping->m_uiInputCode);
        m_mapModifiers.RemoveAt(pkMapping->m_uiModifier);

        delete pkMapping;
    }
}
//---------------------------------------------------------------------------
void EventMap::RemoveMapping(unsigned int uiInputCode,
    unsigned int uiModifier)
{
    unsigned int uiTempEC;
    Mapping* pkMapping;
    NiTMapIterator pos = m_mapMappings.GetFirstPos();
    while (pos)
    {
        m_mapMappings.GetNext(pos, uiTempEC, pkMapping);

        if (pkMapping->m_uiInputCode == uiInputCode &&
            pkMapping->m_uiModifier == uiModifier)
        {
            m_mapMappings.RemoveAt(uiTempEC);
            m_mapInputCodes.RemoveAt(pkMapping->m_uiInputCode);
            m_mapModifiers.RemoveAt(pkMapping->m_uiModifier);

            delete pkMapping;
        }
    }
}
//---------------------------------------------------------------------------
bool EventMap::IsInputCode(unsigned int uiCode)
{
    // Returns: true if uiCode is currently being used as an input code.
    //          false if uiCode is not currently being used as an input code.

    if (uiCode == INPUT_CODE_NONE)
    {
        return false;
    }

    bool bTemp;
    if (m_mapInputCodes.GetAt(uiCode, bTemp))
    {
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
bool EventMap::IsModifier(unsigned int uiCode)
{
    // Returns: true if uiCode is currently being used as a modifier.
    //          false if uiCode is not currently being used as a modifier.

    if (uiCode == INPUT_CODE_NONE)
    {
        return false;
    }

    bool bTemp;
    if (m_mapModifiers.GetAt(uiCode, bTemp))
    {
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
bool EventMap::DoesMappingExist(unsigned int uiInputCode,
    unsigned int uiModifier)
{
    // Returns: true if a mapping to the specified input code and modifier
    //              exists or, in the case of a modifier being all keys,
    //              if a mapping to the input code exists.
    //          false if no mapping exists to the specified input code and
    //              modifier.

    unsigned int uiEventCode;
    Mapping* pkMapping;
    NiTMapIterator pos = m_mapMappings.GetFirstPos();
    while (pos)
    {
        m_mapMappings.GetNext(pos, uiEventCode, pkMapping);

        if (uiModifier == INPUT_CODE_ANY ||
            pkMapping->m_uiModifier == INPUT_CODE_ANY)
        {
            if (pkMapping->m_uiInputCode == uiInputCode)
            {
                return true;
            }
        }
        else
        {
            if (pkMapping->m_uiInputCode == uiInputCode &&
                pkMapping->m_uiModifier == uiModifier)
            {
                return true;
            }
        }
    }

    return false;
}
//---------------------------------------------------------------------------
bool EventMap::GetEventCode(unsigned int& uiEventCode,
    unsigned int uiInputCode, unsigned int uiModifier)
{
    // Returns: true if an event code exists for the specified uiInputCode
    //              and uiModifier. In this case, the event code is stored in
    //              uiEventCode.
    //          false if no event code exists for the specified uiInputCode
    //              and uiModifier. In this case, uiEventCode does not
    //              contain a valid event code.

    Mapping* pkMapping;
    NiTMapIterator pos = m_mapMappings.GetFirstPos();
    while (pos)
    {
        m_mapMappings.GetNext(pos, uiEventCode, pkMapping);

        if (pkMapping->m_uiInputCode == uiInputCode &&
            (pkMapping->m_uiModifier == uiModifier ||
            pkMapping->m_uiModifier == INPUT_CODE_ANY ||
            uiModifier == INPUT_CODE_ANY))
        {
            return true;
        }
    }

    return false;
}
//---------------------------------------------------------------------------
bool EventMap::GetInputCodes(unsigned int uiEventCode,
    unsigned int& uiInputCode, unsigned int& uiModifier)
{
    // Returns: true if input codes exist for the specified uiEventCode.
    //              In this case, the input codes are stored in uiInputCode
    //              and uiModifier.
    //          false if no input codes exist for the specified uiEventCode.
    //              In this case, uiInputCode and uiModifier do not contain
    //              valid input codes.

    Mapping* pkMapping;
    if (!m_mapMappings.GetAt(uiEventCode, pkMapping))
    {
        return false;
    }

    uiInputCode = pkMapping->m_uiInputCode;
    uiModifier = pkMapping->m_uiModifier;

    return true;
}
//---------------------------------------------------------------------------
void EventMap::UpdateEventCode(unsigned int uiOldEC, unsigned int uiNewEC)
{
    Mapping* pkMapping;
    if (m_mapMappings.GetAt(uiOldEC, pkMapping))
    {
        m_mapMappings.RemoveAt(uiOldEC);
        m_mapMappings.SetAt(uiNewEC, pkMapping);
    }
}
//---------------------------------------------------------------------------
EventMap::EM_RC EventMap::StreamFile(bool bWriteFile,
    const char* pcFilename)
{
    // Returns: true if the stream was successful.
    //          false if an error occurred during the stream and it was
    //              unsuccessful.

    if (bWriteFile)
    {
        // Write file.
        NiFile kFile(pcFilename, NiFile::WRITE_ONLY);
        if (!kFile)
        {
            return EM_ERR_FILE_IO;
        }

        char acBuf[255];

        NiSprintf(acBuf, 255, ";Gamebryo MAP File Version %s\n\n",
            g_pcCurrentMAPVersion);
        kFile.Write(acBuf, strlen(acBuf));

        unsigned int uiEventCode;
        Mapping* pkMapping;
        NiTMapIterator pos = m_mapMappings.GetFirstPos();
        while (pos)
        {
            m_mapMappings.GetNext(pos, uiEventCode, pkMapping);

            NiSprintf(acBuf, 255,
                "EVENTCODE %d#INPUTCODE %d#MODIFIER %d#\n",
                uiEventCode, pkMapping->m_uiInputCode,
                pkMapping->m_uiModifier);
            kFile.Write(acBuf, strlen(acBuf));
        }

        NiStrcpy(acBuf, 255, "\n\nEND_MAP_FILE\n");
        kFile.Write(acBuf, strlen(acBuf));
    }
    else    // Read file
    {
        NiFile kFile(pcFilename, NiFile::READ_ONLY);
        if (!kFile)
        {
            return EM_ERR_FILE_NOT_FOUND;
        }

        char acBuf[256];
        char acSeps[] = " #\n";
        char* pcToken;

        // Get file version.
        kFile.GetLine(acBuf, 256);
        char acVersion[4];
        if (strncmp(acBuf, ";Gamebryo MAP File Version ", 27) == 0)
        {
            NiStrncpy(acVersion, 4, &acBuf[27], 3);
        }
        else
        {
            return EM_ERR_FILE_VERSION;
        }

        unsigned int uiVersion = NiStream::GetVersionFromString(acVersion);
        if (uiVersion > NiStream::GetVersionFromString(g_pcCurrentMAPVersion))
        {
            return EM_ERR_FILE_VERSION;
        }

        while (kFile.GetLine(acBuf, 256) > 0)
        {
            if (acBuf[0] && acBuf[0] != ';')
            {
                char* pcContext;
                pcToken = NiStrtok(acBuf, acSeps, &pcContext);

                if (pcToken && strcmp(pcToken, "END_MAP_FILE") == 0)
                {
                    break;
                }

                if (strcmp(pcToken, "EVENTCODE") == 0)
                {
                    char* pcEventCode = NiStrtok(NULL, acSeps, &pcContext);
                    if (!pcEventCode)
                    {
                        return EM_ERR_FILE_FORMAT;
                    }
                    unsigned int uiEventCode = atoi(pcEventCode);

                    pcToken = NiStrtok(NULL, acSeps, &pcContext);
                    if (!pcToken || strcmp(pcToken, "INPUTCODE") != 0)
                    {
                        return EM_ERR_FILE_FORMAT;
                    }

                    char* pcInputCode = NiStrtok(NULL, acSeps, &pcContext);
                    if (!pcInputCode)
                    {
                        return EM_ERR_FILE_FORMAT;
                    }
                    unsigned int uiInputCode = atoi(pcInputCode);

                    pcToken = NiStrtok(NULL, acSeps, &pcContext);
                    if (!pcToken || strcmp(pcToken, "MODIFIER") != 0)
                    {
                        return EM_ERR_FILE_FORMAT;
                    }

                    char* pcModifier = NiStrtok(NULL, acSeps, &pcContext);
                    if (!pcModifier)
                    {
                        return EM_ERR_FILE_FORMAT;
                    }
                    unsigned int uiModifier = atoi(pcModifier);

                    AddMapping(uiEventCode, uiInputCode, uiModifier);
                }
            }
        }
    }

    return EM_SUCCESS;
}
//---------------------------------------------------------------------------