// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#ifndef EVENTMAP_H
#define EVENTMAP_H

#include <NiTMap.h>

class EventMap
{
public:
    enum
    {
        INPUT_CODE_NONE = 0x00,
        INPUT_CODE_ANY = 0x05,
        INPUT_CODE_UNASSIGNED = 0x06
    };

    enum EM_RC
    {
        EM_SUCCESS,
        EM_ERR_FILE_IO,
        EM_ERR_FILE_NOT_FOUND,
        EM_ERR_FILE_FORMAT,
        EM_ERR_FILE_VERSION
    };

    EventMap();
    ~EventMap();

    bool AddMapping(unsigned int uiEventCode, unsigned int uiInputCode,
        unsigned int uiModifier = INPUT_CODE_NONE);
    void RemoveMapping(unsigned int uiEventCode);
    void RemoveMapping(unsigned int uiInputCode, unsigned int uiModifier);

    bool IsInputCode(unsigned int uiCode);
    bool IsModifier(unsigned int uiCode);
    bool DoesMappingExist(unsigned int uiInputCode, unsigned int uiModifier);

    bool GetEventCode(unsigned int& uiEventCode, unsigned int uiInputCode,
        unsigned int uiModifier = INPUT_CODE_NONE);
    bool GetInputCodes(unsigned int uiEventCode, unsigned int& uiInputCode,
        unsigned int& uiModifier);

    void UpdateEventCode(unsigned int uiOldEC, unsigned int uiNewEC);

    EM_RC StreamFile(bool bWriteFile, const char* pcFilename);

    const char* LookupReturnCode(EM_RC eReturnCode);

protected:
    class Mapping
    {
    public:
        Mapping();

        unsigned int m_uiInputCode;
        unsigned int m_uiModifier;
    };

    // Mapping from event code to input codes.
    NiTMap<unsigned int, Mapping*> m_mapMappings;

    // Mapping from input code to bool (for quick lookup of non-modifier
    // input codes).
    NiTMap<unsigned int, bool> m_mapInputCodes;

    // Mapping from modifier to bool (for quick lookup of modifier codes).
    NiTMap<unsigned int, bool> m_mapModifiers;
};

#include "EventMap.inl"

#endif  // #ifndef EVENTMAP_H
