// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

//---------------------------------------------------------------------------
inline const char* EventMap::LookupReturnCode(
    EventMap::EM_RC eReturnCode)
{
    switch (eReturnCode)
    {
        case EM_SUCCESS:
            return "The operation completed successfully.";
            break;
        case EM_ERR_FILE_IO:
            return "A file I/O error occured when writing specified file.";
            break;
        case EM_ERR_FILE_NOT_FOUND:
            return "The specified file does not exist or cannot be opened.";
            break;
        case EM_ERR_FILE_FORMAT:
            return "The specified file is not in the correct format.";
            break;
        case EM_ERR_FILE_VERSION:
            return ("The specified file is from an incompatible version "
                "of the Gamebryo Animation Tool.");
            break;
        default:
            return "";
            break;
    }
}
//---------------------------------------------------------------------------
