// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// NiParticleSystemPropertyWindowCreator.cpp

#include "stdafx.h"
#include "AssetViewer.h"
#include "NiParticleSystemPropertyWindowCreator.h"
#include "NifPropertyWindow.h"
#include "NiParticlesDlg.h"

//---------------------------------------------------------------------------
void CNiParticleSystemPropertyWindowCreator::AddPages(NiObject* pkObj,
    CNifPropertyWindow* pkWindow)
{
    ASSERT(pkObj != NULL && pkWindow != NULL);
    ASSERT(NiIsKindOf(NiParticleSystem, pkObj));
    AddBasicInfoDialog(pkObj, pkWindow, true);
    AddLocalTransformsDialog(pkObj, pkWindow, true);
    AddWorldTransformsDialog(pkObj, pkWindow);
    AddNiBoundDialog(pkObj, pkWindow);
    AddParticleSystemDialog(pkObj, pkWindow, true);
    AddTimeControllerDialog(pkObj, pkWindow, true);
    AddPropertiesDialog(pkObj, pkWindow);
    AddSelectiveUpdateDialog(pkObj, pkWindow);
    AddCollisionDialog(pkObj, pkWindow);
    AddExtraDataDialog(pkObj, pkWindow);
    AddViewerStringsDialog(pkObj, pkWindow);
    pkWindow->ScrollToPage(0);
}
//---------------------------------------------------------------------------
NiRTTI* CNiParticleSystemPropertyWindowCreator::GetTargetRTTI()
{
    return (NiRTTI*) (&NiParticleSystem::ms_RTTI);
}
//---------------------------------------------------------------------------
int CNiParticleSystemPropertyWindowCreator::AddParticleSystemDialog(
    NiObject* pkObj, CNifPropertyWindow* pkWindow, bool bExpanded, 
    int iIndex)
{
    CNiParticlesDlg* pwndDlg = new CNiParticlesDlg;
    pwndDlg->SetNiObject(pkObj);
    
    pwndDlg->Create(MAKEINTRESOURCE(pwndDlg->IDD), 
        pkWindow->GetParentForPages());

    return pkWindow->InsertPage("Particle System", pwndDlg, bExpanded, 
        iIndex);
}
//---------------------------------------------------------------------------
