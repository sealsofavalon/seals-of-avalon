// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// NiParticlesDlg.cpp

#include "stdafx.h"
#include "AssetViewer.h"
#include "NiParticlesDlg.h"
#include "NifPropertyWindowManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//---------------------------------------------------------------------------
// CNiParticlesDlg dialog
//---------------------------------------------------------------------------
CNiParticlesDlg::CNiParticlesDlg(CWnd* pParent)
    : CNiObjectDlg(CNiParticlesDlg::IDD, pParent)
{
    //{{AFX_DATA_INIT(CNiParticlesDlg)
        // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
}
//---------------------------------------------------------------------------
void CNiParticlesDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CNiParticlesDlg)
    DDX_Control(pDX, IDC_MODIFIERS_LIST, m_wndModifiersList);
    DDX_Control(pDX, IDC_NIPARTICLESYSTEM_MAXCOUNT_EDIT, m_wndMaxCountEdit);
    DDX_Control(pDX, IDC_NIPARTICLESYSTEM_COUNT_EDIT, m_wndCurrentCountEdit);
    DDX_Control(pDX, IDC_NIPARTICLESYSTEM_WORLDSPACE_EDIT, 
        m_wndWorldSpaceEdit);
    //}}AFX_DATA_MAP
}
//---------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(CNiParticlesDlg, CDialog)
    //{{AFX_MSG_MAP(CNiParticlesDlg)
    ON_NOTIFY(NM_DBLCLK, IDC_MODIFIERS_LIST, OnDblclkModifiersList)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()
//---------------------------------------------------------------------------
// CNiParticlesDlg message handlers
//---------------------------------------------------------------------------
void CNiParticlesDlg::DoUpdate()
{
    int nItem = 0;
    LPCTSTR lpszItem = NULL;
    NiParticleSystem* pkObj = (NiParticleSystem*) m_pkObj;
    m_wndModifiersList.DeleteAllItems();

    for (unsigned int ui = 0; ui < pkObj->GetModifierCount(); ui++)
    {
        NiPSysModifier* pkMod = pkObj->GetModifierAt(ui);
        lpszItem = pkMod->GetName();
        m_wndModifiersList.InsertItem( nItem++, lpszItem );
    }
    
    if (pkObj->GetWorldSpace())
        m_wndWorldSpaceEdit.SetWindowText("TRUE");
    else
        m_wndWorldSpaceEdit.SetWindowText("FALSE");

    char acString[256];
    NiSprintf(acString, 256, "%d", pkObj->GetMaxNumParticles());
    m_wndMaxCountEdit.SetWindowText(acString);

    NiSprintf(acString, 256, "%d", pkObj->GetNumParticles());
    m_wndCurrentCountEdit.SetWindowText(acString);

}
//---------------------------------------------------------------------------
BOOL CNiParticlesDlg::OnInitDialog() 
{
    CDialog::OnInitDialog();
    
    ASSERT(this->m_pkObj != NULL && NiIsKindOf(NiObjectNET, m_pkObj));
    m_wndModifiersList.InsertColumn(0, "Name");
    CRect rect;
    m_wndModifiersList.GetWindowRect(&rect);
    int cx = rect.Width();
    m_wndModifiersList.SetColumnWidth(0, cx-4);
    DoUpdate();
    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}
//---------------------------------------------------------------------------
void CNiParticlesDlg::OnDblclkModifiersList(
    NMHDR* pNMHDR, LRESULT* pResult)
{
    if(m_wndModifiersList.GetSelectedCount() == 0)
        return;

    int iIndex = m_wndModifiersList.GetNextItem(-1, LVIS_SELECTED);

    if(iIndex == -1)
        return;

    int iMatchIndex = 0;
    NiParticleSystem* pkObj = (NiParticleSystem*) m_pkObj;
    
    if ((unsigned int )iIndex < pkObj->GetModifierCount())
    {
        NiPSysModifier* pkMod = pkObj->GetModifierAt(iIndex);
        if(pkMod)
        {
            CNifPropertyWindowManager* pkManager = 
                CNifPropertyWindowManager::GetPropertyWindowManager();

            pkManager->CreatePropertyWindow(pkMod);
        }
    }

    *pResult = 0;
}
//---------------------------------------------------------------------------
