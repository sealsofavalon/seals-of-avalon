// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// NifGlobals.cpp

#include "stdafx.h"

const int g_iParamNotFound = INT_MIN;
const float g_fParamNotFound = -NI_INFINITY;
const CString g_strParamNotFound = "None";
const CString g_strBoundShapePath = "BoundShape.nif";
const CString g_strBoundShapeRootName = "BoundShapeRoot";

bool g_bNeedPromptOnDiscard = false;
unsigned int g_uiMaxFrameRate = 100;
float g_fMinFrameTime = 1.0f / g_uiMaxFrameRate;

bool g_bD3D10 = false;

//---------------------------------------------------------------------------
void MainThreadUpdateAllViews(LPARAM params)
{
    CWinApp* pkApp = AfxGetApp();
    if(!pkApp)
    {
        NiOutputDebugString("No app found in "
            "MainThreadUpdateAllViews(LPARAM params)");
        return;
    }
    CWnd* pkWnd = AfxGetApp()->GetMainWnd();
    if(!pkWnd)
    {
        NiOutputDebugString("No main CWnd* found in "
            "MainThreadUpdateAllViews(LPARAM params)");
        return;
    }
    HWND hWnd = pkWnd->m_hWnd;
    ::PostMessage(hWnd, NIF_BROADCASTUPDATE, 0, params);
}
//---------------------------------------------------------------------------
CString TranslateInputCode(unsigned int uiInputCode)
{
    CString strName;

    switch (uiInputCode)
    {
        case EventMap::INPUT_CODE_NONE:
            strName = "None";
            break;
        case EventMap::INPUT_CODE_ANY:
            strName = "Any Key";
            break;
        case VK_LEFT:
            strName = "Left Arrow";
            break;
        case VK_UP:
            strName = "Up Arrow";
            break;
        case VK_RIGHT:
            strName = "Right Arrow";
            break;
        case VK_DOWN:
            strName = "Down Arrow";
            break;
        case VK_SPACE:
            strName = "Space";
            break;
        case VK_PRIOR:
            strName = "Page Up";
            break;
        case VK_NEXT:
            strName = "Page Down";
            break;
        case VK_END:
            strName = "End";
            break;
        case VK_HOME:
            strName = "Home";
            break;
        case '0':
            strName = "0";
            break;
        case '1':
            strName = "1";
            break;
        case '2':
            strName = "2";
            break;
        case '3':
            strName = "3";
            break;
        case '4':
            strName = "4";
            break;
        case '5':
            strName = "5";
            break;
        case '6':
            strName = "6";
            break;
        case '7':
            strName = "7";
            break;
        case '8':
            strName = "8";
            break;
        case '9':
            strName = "9";
            break;
        case 'A':
            strName = "A";
            break;
        case 'B':
            strName = "B";
            break;
        case 'C':
            strName = "C";
            break;
        case 'D':
            strName = "D";
            break;
        case 'E':
            strName = "E";
            break;
        case 'F':
            strName = "F";
            break;
        case 'G':
            strName = "G";
            break;
        case 'H':
            strName = "H";
            break;
        case 'I':
            strName = "I";
            break;
        case 'J':
            strName = "J";
            break;
        case 'K':
            strName = "K";
            break;
        case 'L':
            strName = "L";
            break;
        case 'M':
            strName = "M";
            break;
        case 'N':
            strName = "N";
            break;
        case 'O':
            strName = "O";
            break;
        case 'P':
            strName = "P";
            break;
        case 'Q':
            strName = "Q";
            break;
        case 'R':
            strName = "R";
            break;
        case 'S':
            strName = "S";
            break;
        case 'T':
            strName = "T";
            break;
        case 'U':
            strName = "U";
            break;
        case 'V':
            strName = "V";
            break;
        case 'W':
            strName = "W";
            break;
        case 'X':
            strName = "X";
            break;
        case 'Y':
            strName = "Y";
            break;
        case 'Z':
            strName = "Z";
            break;
        case 0xc0:
            strName = "`";
            break;
        case 0xbd:
            strName = "-";
            break;
        case 0xbb:
            strName = "=";
            break;
        case 0xdb:
            strName = "[";
            break;
        case 0xdd:
            strName = "]";
            break;
        case 0xdc:
            strName = "\\";
            break;
        case 0xba:
            strName = ";";
            break;
        case 0xde:
            strName = "\'";
            break;
        case 0xbc:
            strName = ",";
            break;
        case 0xbe:
            strName = ".";
            break;
        case 0xbf:
            strName = "/";
            break;
        case VK_LWIN:
            strName = "Left Windows";
            break;
        case VK_RWIN:
            strName = "Right Windows";
            break;
        case VK_APPS:
            strName = "Apps";
            break;
        case VK_NUMPAD0:
            strName = "Num Pad 0";
            break;
        case VK_NUMPAD1:
            strName = "Num Pad 1";
            break;
        case VK_NUMPAD2:
            strName = "Num Pad 2";
            break;
        case VK_NUMPAD3:
            strName = "Num Pad 3";
            break;
        case VK_NUMPAD4:
            strName = "Num Pad 4";
            break;
        case VK_NUMPAD5:
            strName = "Num Pad 5";
            break;
        case VK_NUMPAD6:
            strName = "Num Pad 6";
            break;
        case VK_NUMPAD7:
            strName = "Num Pad 7";
            break;
        case VK_NUMPAD8:
            strName = "Num Pad 8";
            break;
        case VK_NUMPAD9:
            strName = "Num Pad 9";
            break;
        case VK_MULTIPLY:
            strName = "Num Pad *";
            break;
        case VK_ADD:
            strName = "Num Pad +";
            break;
        case VK_SEPARATOR:
            strName = "SEPARATOR";
            break;
        case VK_SUBTRACT:
            strName = "Num Pad -";
            break;
        case VK_DECIMAL:
            strName = "Num Pad .";
            break;
        case VK_DIVIDE:
            strName = "Num Pad /";
            break;
        case VK_F1:
            strName = "F1";
            break;
        case VK_F2:
            strName = "F2";
            break;
        case VK_F3:
            strName = "F3";
            break;
        case VK_F4:
            strName = "F4";
            break;
        case VK_F5:
            strName = "F5";
            break;
        case VK_F6:
            strName = "F6";
            break;
        case VK_F7:
            strName = "F7";
            break;
        case VK_F8:
            strName = "F8";
            break;
        case VK_F9:
            strName = "F9";
            break;
        case VK_F10:
            strName = "F10";
            break;
        case VK_F11:
            strName = "F11";
            break;
        case VK_F12:
            strName = "F12";
            break;
        case VK_F13:
            strName = "F13";
            break;
        case VK_F14:
            strName = "F14";
            break;
        case VK_F15:
            strName = "F15";
            break;
        case VK_F16:
            strName = "F16";
            break;
        case VK_F17:
            strName = "F17";
            break;
        case VK_F18:
            strName = "F18";
            break;
        case VK_F19:
            strName = "F19";
            break;
        case VK_F20:
            strName = "F20";
            break;
        case VK_F21:
            strName = "F21";
            break;
        case VK_F22:
            strName = "F22";
            break;
        case VK_F23:
            strName = "F23";
            break;
        case VK_F24:
            strName = "F24";
            break;
        case VK_BACK:
            strName = "Backspace";
            break;
        case VK_TAB:
            strName = "Tab";
            break;
        case VK_CLEAR:
            strName = "Clear";
            break;
        case VK_RETURN:
            strName = "Enter";
            break;
        case VK_SHIFT:
            strName = "Shift";
            break;
        case VK_CONTROL:
            strName = "Control";
            break;
        case VK_MENU:
            strName = "Alt";
            break;
        case VK_PAUSE:
            strName = "Pause";
            break;
        case VK_CAPITAL:
            strName = "Caps Lock";
            break;
        case VK_NUMLOCK:
            strName = "Num Lock";
            break;
        case VK_SCROLL:
            strName = "Scroll Lock";
            break;
        case VK_ESCAPE:
            strName = "Esc";
            break;
        case VK_INSERT:
            strName = "Insert";
            break;
        case VK_DELETE:
            strName = "Delete";
            break;
        case VK_HANGUL:
            strName = "HANGUL";
            break;
        case VK_JUNJA:
            strName = "JUNJA";
            break;
        case VK_FINAL:
            strName = "FINAL";
            break;
        case VK_KANJI:
            strName = "KANJI";
            break;
        case VK_CONVERT:
            strName = "CONVERT";
            break;
        case VK_NONCONVERT:
            strName = "NONCONVERT";
            break;
        case VK_ACCEPT:
            strName = "ACCEPT";
            break;
        case VK_MODECHANGE:
            strName = "MODECHANGE";
            break;
        case VK_SELECT:
            strName = "SELECT";
            break;
        case VK_PRINT:
            strName = "PRINT";
            break;
        case VK_EXECUTE:
            strName = "EXECUTE";
            break;
        case VK_SNAPSHOT:
            strName = "SNAPSHOT";
            break;
        case VK_HELP:
            strName = "HELP";
            break;
        default:
            strName = "Unknown";
            break;
    }

    return strName;
}
//---------------------------------------------------------------------------
