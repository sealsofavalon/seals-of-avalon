// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// NifRenderViewLODAdjustCommand.cpp

#include "stdafx.h"
#include "NifRenderViewLODAdjustCommand.h"
#include "NifRenderView.h"
#include "NifDoc.h"
#include "NifRenderViewUIManager.h"
#include "NifRenderViewUIState.h"

bool CNifRenderViewLODAdjustCommand::m_bAllowAdjust = false;
//---------------------------------------------------------------------------
bool CNifRenderViewLODAdjustCommand::Execute(CNifRenderView* pkView)
{
    if(pkView)
    {
        CNifDoc* pkDoc = pkView->GetDocument();
        if(pkDoc)
        {
            int iLOD = NiLODNode::GetGlobalLOD();
            if (iLOD < 0)
                m_iAdjust += -iLOD;
            iLOD += m_iAdjust;
            if (iLOD < 0)
                iLOD = 0;
            NiLODNode::SetGlobalLOD(iLOD);

            iLOD = NiBoneLODController::GetGlobalLOD();
            if (iLOD < 0)
                m_iAdjust += -iLOD;
            iLOD += m_iAdjust;
            if (iLOD < 0)
                iLOD = 0;
            NiBoneLODController::SetGlobalLOD(iLOD);

            pkDoc->UpdateScene(true);
        }
        // No document == error
        else 
            return false;
    }
    // No view == error
    else 
        return false;

    return true;
}
//---------------------------------------------------------------------------
CNifRenderViewLODAdjustCommand::CNifRenderViewLODAdjustCommand(
    int iAdjust)
{
    m_iAdjust = iAdjust;
}
//---------------------------------------------------------------------------
CNifRenderViewLODAdjustCommand::~CNifRenderViewLODAdjustCommand()
{
}
//---------------------------------------------------------------------------
bool CNifRenderViewLODAdjustCommand::IsAdjustEnabled()
{
    return m_bAllowAdjust;
}
//---------------------------------------------------------------------------
void CNifRenderViewLODAdjustCommand::ToggleAdjust(bool bAdjustOn)
{
    m_bAllowAdjust = bAdjustOn;

    if (bAdjustOn)
    {
        NiLODNode::SetGlobalLOD(0);
        NiBoneLODController::SetGlobalLOD(0);
    }
    else
    {
        NiLODNode::SetGlobalLOD(-1);
        NiBoneLODController::SetGlobalLOD(-1);
    }
}
//---------------------------------------------------------------------------
bool CNifRenderViewLODAdjustCommand::IsDecrementEnabled()
{
    if (!IsAdjustEnabled())
        return false;

    int iLOD = NiLODNode::GetGlobalLOD();
    int iBoneLOD = NiBoneLODController::GetGlobalLOD();
            
    if (iLOD < 1 || iBoneLOD < 1)
        return false;

    return true;
}
//---------------------------------------------------------------------------
bool CNifRenderViewLODAdjustCommand::IsIncrementEnabled()
{
    if (!IsAdjustEnabled())
        return false;
    return true;
}
//---------------------------------------------------------------------------
