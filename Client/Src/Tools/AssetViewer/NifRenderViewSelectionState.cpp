// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// NifRenderViewSelectionState.cpp

#include "stdafx.h"
#include "AssetViewer.h"
#include "NifRenderViewSelectionState.h"
#include "NifRenderView.h"
#include "NifDoc.h"

//---------------------------------------------------------------------------
CNifRenderViewSelectionState::CNifRenderViewSelectionState(
    CNifRenderView* pkView) : CNifRenderViewUIState(pkView)
{
}
//---------------------------------------------------------------------------
CNifRenderViewSelectionState::~CNifRenderViewSelectionState()
{
}
//---------------------------------------------------------------------------
bool CNifRenderViewSelectionState::CanExecuteCommand(char* pcCommandID)
{
    return true;
}
//---------------------------------------------------------------------------
void CNifRenderViewSelectionState::OnLButtonDown(UINT nFlags, CPoint point)
{
    m_pkView->ScreenToClient(&point);
    PerformPick(point);
}
//---------------------------------------------------------------------------
void CNifRenderViewSelectionState::PerformPick(CPoint point)
{
    CNifDoc* pkDoc = m_pkView->GetDocument();
    NIASSERT(pkDoc);

    pkDoc->Lock();

    NiCamera* pkCamera = pkDoc->GetCameraInfo(
        m_pkView->GetCurrentCameraIndices())->m_spCam;

    NiPoint3 kOrigin, kDir;
    pkCamera->WindowPointToRay(point.x, point.y, kOrigin, kDir);

    NiPick kPick;
    kPick.SetTarget(pkDoc->GetSceneGraph());

    bool bHighlightedObject = false;
    bool bHit = kPick.PickObjects(kOrigin, kDir);
    if (bHit)
    {
        NiPick::Results& kPickResults = kPick.GetResults();

        for (unsigned int ui = 0; ui < kPickResults.GetSize(); ui++)
        {
            NiPick::Record* pkPickRecord = kPickResults.GetAt(ui);
            if (pkPickRecord)
            {
                NiAVObject* pkObject = pkPickRecord->GetAVObject();
                NiNode* pkParent = pkObject->GetParent();
                if (pkParent && pkParent->GetName() &&
                    g_strBoundShapeRootName != (const char*)
                    pkParent->GetName())
                {
                    pkDoc->HighlightObject(pkObject);
                    bHighlightedObject = true;
                    break;
                }
            }
        }
    }

    if (!bHighlightedObject)
    {
        pkDoc->HighlightObject(NULL);
    }

    pkDoc->UnLock();
}
//---------------------------------------------------------------------------
