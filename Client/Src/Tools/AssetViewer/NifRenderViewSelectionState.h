// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// NifRenderViewSelectionState.h

#ifndef NIFRENDERVIEWSELECTIONSTATE_H
#define NIFRENDERVIEWSELECTIONSTATE_H

#include "NifRenderViewUIState.h"

class CNifRenderViewSelectionState : public CNifRenderViewUIState
{
public:
    CNifRenderViewSelectionState(CNifRenderView* pkView);
    virtual ~CNifRenderViewSelectionState();

    virtual bool CanExecuteCommand(char* pcCommandID);
    virtual void Update() {}
    virtual void OnLButtonDown(UINT nFlags, CPoint point);

protected:
    void PerformPick(CPoint point);
};

#endif  // #ifndef NIFRENDERVIEWSELECTIONSTATE_H
