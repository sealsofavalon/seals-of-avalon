// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// NifPropertyWindow.h

#ifndef UTILITY_H
#define UTILITY_H

#include <NiMemoryDefines.h>

// In some cases, it may be necessary to use these templated create
// functions to allocate an object instead of calling NiNew directly.
// This is especially true in MFC, where "new" is overridden
// by a macro to mean DEBUG_NEW. Since NiNew is a substitution for
// new, we run into a problem. Calling NiTCreate<object>() will
// allow a client to allocate through Gamebryo in such a circumstance.
template<class TYPE, class ARG1> 
inline TYPE* NiTCreateExt(ARG1 a)
{
	return NiNew TYPE[a]();
}

template<class TYPE, class ARG1, class ARG2, class ARG3, class ARG4, class ARG5, class ARG6, class ARG7> 
inline TYPE* NiTCreate7(ARG1 a, ARG2 b, ARG3 c, ARG4 d, ARG5 e, ARG6 f, ARG7 g)
{
	return NiNew TYPE(a, b, c, NULL, e, f, NULL);
}
#endif  // #ifndef UTILITY_H