#include "StdAfx.h"
#include "CoverInstance.h"
#include <Windows.h>

CoverInstance::CoverInstance(void) :
m_nIDCounter(0)
{
}

CoverInstance::~CoverInstance(void)
{
}


void CoverInstance::StartCover(const char* szRootDir)
{
	VEC_STRING vFileList;
	SearchFile(szRootDir, vFileList);

	int nFileCount = vFileList.size();
	for (int i = 0; i<nFileCount; i++)
	{
		printf("Parse...>> %s\n", vFileList[i].c_str());
		ParseFile(vFileList[i].c_str());
	}
	printf("\n");

	FILE* fp = NULL;
	fp = fopen("不带参数的.txt", "w+");
	if ( fp )
	{
		int nStringCount = m_vUnContainArg.size();
		for (int i=0; i<nStringCount; i++)
			fprintf(fp, "%s\n", m_vUnContainArg[i].c_str());
	}
	fclose(fp);


	fp = NULL;
	fp = fopen("带参数的.txt", "w+");
	if ( fp )
	{
		int nStringCount = m_vContainArg.size();
		for (int i=0; i<nStringCount; i++)
			fprintf(fp, "%s\n", m_vContainArg[i].c_str());
	}
	fclose(fp);

	fp = NULL;
	fp = fopen("凡子要的.txt", "w+");
	if ( fp )
	{
		int nStringCount = m_vFanZiUse.size();
		for (int i=0; i<nStringCount; i++)
			fprintf(fp, "%s\n", m_vFanZiUse[i].c_str());
	}
	fclose(fp);
}

void CoverInstance::ParseFile(const char* szFileName)
{
	FILE* fp = fopen(szFileName, "r");
	if ( !fp )
		return;

	int nFileSize = 0;

	fseek(fp, 0, SEEK_END);
	nFileSize = ftell(fp);
	fseek(fp, 0, SEEK_SET);


	STRING vFileBuff;
	char c;
	for (int i=0; i<nFileSize; i++)
	{
		fread(&c, 1, 1, fp );
		vFileBuff += c;
	}
	fclose(fp);

	ParseFileBuff( vFileBuff.c_str() , nFileSize, szFileName);
}

bool CoverInstance::IsContainArg(STRING& vString)
{
	int pos = vString.find("%");
	if ( pos != -1 ) //找到了 说明是带参数的
		return true;

	return false;
}

STRING CoverInstance::GenerateFanZi(STRING& vString)
{
	STRING strRet = vString;

	char buff[MAX_PATH];
	
	//先搜寻%的个数
	SIZE size;
	
	std::vector<SIZE> vSize;

	int nStrCount = strRet.length();
	int i = 0;
	bool b = false;
	int pos = 0;
	int nOffCount = 0;


	while ( i < nStrCount )
	{
		char c = strRet[i];
		if ( c == '%' )
		{
			b = true;
			pos = i+1;
			nOffCount = 0;
		}
		else if ( c == 's' || c == 'f' || c == 'd' || c == 'u' )
		{
			if ( b )//说明之前
			{
				b  =false;
				SIZE size;
				size.cx = pos-1;
				size.cy = nOffCount+1;
				vSize.push_back(size);
			}
		}

		if ( b )
		{
			nOffCount++;
		}

		i++;
	}

	int nSizeCount = vSize.size();
	int nDeltaSize = 0;

	for (int i=0; i<nSizeCount; i++)
	{
		SIZE size = vSize[i];

		STRING strAdd;
		char buff[MAX_PATH];
		sprintf(buff, "<LANGUAGE_FLAG_STRING_%d>", i);
		strAdd += buff;

		strRet.erase(size.cx + i*strAdd.length() - nDeltaSize, size.cy);
		strRet.insert( size.cx + i*strAdd.length() - nDeltaSize, strAdd);

		nDeltaSize += size.cy;
	}

	return strRet;
}

bool CoverInstance::IsBContainStr(const char* szBuff, int nReadPos, STRING strArg)
{
	STRING str;
	int nLenth = strArg.length();
	szBuff -= nLenth;


	for (int i=nReadPos; i<nReadPos + nLenth; i++)
	{
		str += szBuff[i];
	}

	if ( str.find( strArg.c_str() ) != -1 )
		return true;

	return false;
}

void CoverInstance::ParseFileBuff(const char* szBuff, int nFileSize, const char* szFileName)
{
	int nReadPos = 0;
	int nMarkCount = 0;
	STRING vViewString;
	bool bFirstCon = true;
	bool bFirstUnCon = true;

	while( nReadPos != nFileSize )
	{
		char c = szBuff[nReadPos];
		vViewString = szBuff + nReadPos;
		switch( c )
		{
		case '"':
			{
				bool bIsTransed = false;

				if ( IsBContainStr(szBuff, nReadPos, "TRAN(") )
					bIsTransed = true;
				else if ( IsBContainStr(szBuff, nReadPos, "OutputDebugString(") )
					bIsTransed = true;
				else if ( IsBContainStr(szBuff, nReadPos, "ULOG(") )
					bIsTransed = true;

				STRING vString = GetLine(szBuff, nReadPos, nFileSize);

				if (IsRightString(vString) ) //分析字符串是否合法
				{
					if ( !bIsTransed )
					{
						if ( IsContainArg( vString ) ) //是否带参数的
						{
							m_nIDCounter++;


							//写入第一个文件
							STRING strContainArg = vString;
							char buff[MAX_PATH];
							sprintf(buff, "  [%d]:", m_nIDCounter);
							strContainArg.insert(0, buff);

							if ( bFirstCon ) //写入文件路径
							{
								bFirstCon = false;
								STRING strFileName = szFileName;
								strFileName.insert(0, "\n");
								m_vContainArg.push_back( strFileName );
							}
							m_vContainArg.push_back( strContainArg );


							//写给凡子的文件 
							STRING strFanZi = vString;
							STRING strFanziUse = GenerateFanZi(strFanZi);
							sprintf(buff, "%d ", m_nIDCounter);
							strFanziUse.insert(0, buff);
							m_vFanZiUse.push_back( strFanziUse );

						}
						else
						{
							if ( bFirstUnCon )
							{
								bFirstUnCon = false;
								STRING strFileName = szFileName;
								strFileName.insert(0, "\n");
								m_vUnContainArg.push_back( strFileName );
							}
							m_vUnContainArg.push_back( vString );
						}
					}

				}
			}
			break;

		case '/':
			{
				nReadPos++;
				STRING vTemp;
				vTemp += c;
				vTemp += szBuff[nReadPos];
				if ( szBuff[nReadPos] == '/' )
				{
					nReadPos++;
					
					for ( ; nReadPos < nFileSize; ++nReadPos)
					{
						char cc = szBuff[nReadPos];
						vTemp += cc;
						if ( cc == '\r' || cc == '\n' )
							break;
					}
				}
				else if ( szBuff[nReadPos] == '*' )
				{
					nReadPos++;
					for ( ; nReadPos < nFileSize; ++nReadPos)
					{
						char c0 = szBuff[nReadPos];
						char c1 = szBuff[nReadPos+1];
						vTemp += c0;
						if ( c0 == '*' && c1 == '/' )
						{
							++nReadPos;
							++nReadPos;
							vTemp += c1;
							char c = szBuff[nReadPos];
							break;
						}
					}
				}
				else
					nReadPos++;
			}
			break;

		default:
			nReadPos++;
			break;
		}
	}
}

STRING CoverInstance::GetLine(const char* szBuff, int& nReadPos, int nFileSize)
{
	nReadPos++;
	STRING vRet;

	while( nReadPos != nFileSize )
	{
		char c = szBuff[nReadPos];
		nReadPos++;
		if ( c == '"' ) //
		{
			break;
		}
		
		vRet += c;
	}

	return vRet;
}

bool CoverInstance::IsRightString(STRING& vString)
{
	int pos = vString.find(".h");
	if ( pos != -1 )
		return false;

	int nSize = vString.size();

	for (int i=0; i<nSize; i++)
	{
		int n = vString[i];
		if ( n < 0 ) //找到中文就返回true
			return true;
	}

	return false;
}

void CoverInstance::SearchFile(const char* rootDir, VEC_STRING& vFileList)
{
	char fname[MAX_PATH];
	ZeroMemory(fname, MAX_PATH);
	WIN32_FIND_DATA fd;
	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));
	HANDLE hSearch;
	char filePathName[256];
	char tmpPath[256];
	ZeroMemory(filePathName, 256);
	ZeroMemory(tmpPath, 256); 

	strcpy_s(filePathName, rootDir);

	BOOL bSearchFinished = FALSE;
	if( filePathName[strlen(filePathName) -1] != '\\' )
	{
		strcat_s(filePathName, "\\");
	}
	strcat_s(filePathName, "*.*");
	hSearch = FindFirstFile(filePathName, &fd);
	//Is directory
	if( (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		&& strcmp(fd.cFileName, ".") && strcmp(fd.cFileName, "..") )  
	{
		strcpy_s(tmpPath, rootDir);
		strcat_s(tmpPath,"\\");
		strcat_s(tmpPath, fd.cFileName);
		SearchFile(tmpPath, vFileList);
	}
	else if( strcmp(fd.cFileName, ".") && strcmp(fd.cFileName, "..") )
	{

	}

	while( !bSearchFinished )
	{
		if( FindNextFile(hSearch, &fd) )
		{
			if( (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				&& strcmp(fd.cFileName, ".") && strcmp(fd.cFileName, "..") )  
			{
				strcpy_s(tmpPath, rootDir);
				strcat_s(tmpPath,"\\");
				strcat_s(tmpPath, fd.cFileName);
				SearchFile(tmpPath, vFileList);
			}
			else if( strcmp(fd.cFileName, ".") && strcmp(fd.cFileName, "..") )
			{
				std::string strFile = rootDir;
				strFile += "\\";
				strFile += fd.cFileName;
				int pos = strFile.find(".cpp");

				if ( pos != -1 && (pos + 4 == strFile.length()) )
				{
					vFileList.push_back( strFile );
				}
				//else 
				//{
				//	pos = strFile.find(".h");
				//	if ( pos != -1 && (pos + 2 == strFile.length()) )
				//	{
				//		sprintf_s(fname, "%s", fd.cFileName);
				//		printf("%s\n" , fname);
				//		vFileList.push_back( strFile );
				//	}
				//}
			}
		}
		else
		{
			if( GetLastError() == ERROR_NO_MORE_FILES )       //Normal Finished
			{
				bSearchFinished = TRUE;
			}
			else
				bSearchFinished = TRUE;     //Terminate Search
		}
	}
	FindClose(hSearch);
}