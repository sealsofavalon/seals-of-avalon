#pragma once
#include <string.h>
#include <vector>

typedef std::string STRING;
typedef std::vector<STRING> VEC_STRING;


class CoverInstance
{
public:
	CoverInstance(void);
	~CoverInstance(void);

	void StartCover(const char* szRootDir);
	void ParseFile(const char* szFileName);

protected:
	void ParseFileBuff(const char* szBuff, int nFileSize, const char* szFileName);
	void SearchFile(const char* rootDir, VEC_STRING& vFileList); 
	STRING GetLine(const char* szBuff, int& nReadPos, int nFileSize);
	bool IsRightString(STRING& vString);
	bool IsContainArg(STRING& vString); //是否带有参数的中文字符串
	bool IsBContainStr(const char* szBuff, int nReadPos,  STRING strArg);

	STRING GenerateFanZi(STRING& vString);

private:
	VEC_STRING m_vContainArg;
	VEC_STRING m_vUnContainArg;
	VEC_STRING m_vFanZiUse;
	int	m_nIDCounter;
};
