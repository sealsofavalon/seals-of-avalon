// FireFox.h : FireFox DLL 的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// 主符号


// CFireFoxApp
// 有关此类实现的信息，请参阅 FireFox.cpp
//
#if !defined( __AFXCTL_H__ )
#error include 'afxctl.h' before including this file
#endif
class CFireFoxApp : public COleControlModule
{
	DECLARE_DYNAMIC(CFireFoxApp)
public:
	CFireFoxApp();

// 重写
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
	virtual int ExitInstance();
};
