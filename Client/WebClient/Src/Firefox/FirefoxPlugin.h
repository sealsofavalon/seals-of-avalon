#pragma once

#include "ILWPlugin.h"

class NPApplication;

class FirefoxPlugin : public ILWPlugin
{
public:
	FirefoxPlugin(NPApplication* App);
	~FirefoxPlugin(void);
	virtual void SetStatusText(const char *txt);
	virtual void ComputeBaseUrl(TCHAR* BaseUrl, INT MaxChar);
	NPApplication* m_App;
};
