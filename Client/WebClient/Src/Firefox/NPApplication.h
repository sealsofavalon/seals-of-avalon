#pragma once

#include "NPCtrlWnd.h"
#include "ILWStreamSystem.h"
#include "IGame.h"

class FirefoxPlugin;
class NPViewport;

class NPApplication
{
public:
	NPApplication(NPP pNPInstance, uint16 mode, int16 argc, char* argn[], char* argv[]);
	virtual ~NPApplication();
	NPBool Initialize(NPWindow* pNPWindow);
	void Destroy();
	BOOL IsInitialized() const		{ return m_bInitialized; }
protected:
	void OnSize(INT Width, INT Height);
	void OnLBDown(INT X, INT Y);
	void OnLBUp(INT X, INT Y);
	void OnMBDown(INT X, INT Y);
	void OnMBUp(INT X, INT Y);
	void OnRBDown(INT X, INT Y);
	void OnRBUp(INT X, INT Y);
	void OnMouseMove(INT X, INT Y);
	void OnMouseWheel(INT ZDelta);
	void OnKey(WORD Key);
	void OnChar(WORD Char);
	void OnSetFocus();
	void OnTimer(UINT nIDEvent);
private:
	BOOL InitClientApp();
	void UpdateClientApp();
	static LRESULT CALLBACK PluginWinProc(HWND, UINT, WPARAM, LPARAM);
	NPP			m_pNPInstance;
	NPWindow*	m_Window;
	NPStream*	m_pNPStream;
	HWND		m_hWnd;
	CNPCtrlWnd*	m_pCtrlWnd;
	FirefoxPlugin* m_PluginInterface;
	NPViewport* m_Viewport;
	ILWStreamSystem* m_StreamSystem;
	IGame*		m_iGame;
	HMODULE m_hStreamSystem;
	HMODULE m_hGameInterface;
	CriticalSection m_MainCS;
	char m_InstallPath[MAX_PATH];
	char m_BaseUrl[1024];
	BOOL m_Loading;
	BOOL m_bInitialized;
	BOOL m_bFailed;

};
