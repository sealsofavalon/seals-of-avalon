#pragma once

class NPApplication;
// NPViewport

class NPViewport : public CWnd
{
	DECLARE_DYNAMIC(NPViewport)

	NPApplication* m_App;
public:
	NPViewport(NPApplication* App);
	virtual ~NPViewport();

protected:
	DECLARE_MESSAGE_MAP()
};


