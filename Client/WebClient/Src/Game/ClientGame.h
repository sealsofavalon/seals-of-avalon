#pragma once
#include "IGame.h"
#include "SceneLoadingSink.h"

class ClientGame : public NiApplication, public IGame 
{
	friend class SceneLoadingSink;
public:
	ClientGame(void);
	virtual ~ClientGame(void);
protected:
	// 重写
	virtual bool CreateRenderer();
	virtual void UpdateFrame();
private:
	// IGame实现
	virtual BOOL InitializeForCtrl(HWND hCtrlWnd,ILWStreamSystem* iStreamSys);
	virtual void Destroy();
	virtual void Update();
	// 加载WEB场景.
	virtual BOOL LoadScene(const char* RefPath,const char* SceneFile, void* Reserve);
	virtual ISceneLoadingSink* GetSceneLoadingSink();
	SceneLoadingSink m_LoadingSink;
	ILWStreamSystem* m_StreamSystem;
};
