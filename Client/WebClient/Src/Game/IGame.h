#pragma once

struct ILWStreamSystem;
// 场景加载监听. 当前只需要能在场景文件未加载完成时显示加载进度或者LOGO等.
// 本想将其封装在LWEngine模块中,但对于不同的游戏,甚至同一游戏中不同场景或都有不同的
// 装载方式.因此交由IGame 实现模块来处理.
struct ISceneLoadingSink 
{
	// NOTE, 不需要进行多线程同步处理.
	virtual void OnLoadingNotify(const char* NotifyMsg, float Ratio) = 0;
	virtual void OnRender() = 0;
};


// 通过这个接口在尽可能不对客户端做大量修改的前提下保持和WEB客户端的联系.
struct IGame
{
	// 为浏览器控件初始化程序环境,注意不要再创建窗口. hCtrlWnd 为浏览器控件的渲染窗口.
	// 而实际的浏览器控件窗口为渲染窗口的父窗口.
	virtual BOOL InitializeForCtrl(HWND hCtrlWnd, ILWStreamSystem* iStreamSys) = 0;
	// 销毁客户端. 注意同时删除this.
	virtual void Destroy() = 0;
	// 更新. 由windows timer 驱动, 准确时间幅度由客户端计算.
	virtual void Update() = 0;
	//virtual void DeleteThis() = 0;
	// 载入场景.
	virtual BOOL LoadScene(const char* RefPath,const char* SceneFile, void* Reserve) = 0;

	// 设置场景加载时通知接收对象.
	virtual ISceneLoadingSink* GetSceneLoadingSink() =0;
	virtual BOOL IsLoadingScene() = 0;

	virtual void Lock() = 0;
	virtual void Unlock() = 0;
};


#ifdef IGAME_EXPORT
#		define IGameApi __declspec(dllexport)
#	else 
#		define IGameApi 
#endif 


// 创建客户端游戏接口. 
// hAppInst : 浏览器实例.
IGameApi IGame* CreateGameInterface(HINSTANCE hAppInst);

typedef IGame* (*_CreateGameInterfaceFunc)(HINSTANCE hInst);
typedef _CreateGameInterfaceFunc CreateGameInterfaceFunc;