#ifndef __LWBASE_H__
#define __LWBASE_H__


#ifndef LWCoreApi 
#define LWCoreApi 
#endif 



class LWCoreApi LRefTarget
{
public:
	UINT m_RefCnt;
	LRefTarget(void):m_RefCnt(0)
	{
	}

	virtual ~LRefTarget(void)
	{
	}
	
	inline void AddRef()
	{
		m_RefCnt++;
	}

	inline void Release()
	{
		if (--m_RefCnt == 0)
		{
			delete this;
		}
	}
};

class LWCoreApi LWSmartPtr
{

};

class LWCoreApi LWCriticalSection
{
public:

};

class LWCoreApi LWObject : public LRefTarget
{
public:

};

#endif 