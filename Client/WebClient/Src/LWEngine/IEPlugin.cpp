#include "StdAfx.h"
#include "IEPlugin.h"
#include "LWEngineCtrl.h"
#include "URLUtility.h"
CIEPlugin::CIEPlugin(CLWEngineCtrl* OcxCtrl):m_OcxCtrl(OcxCtrl)
{
}

CIEPlugin::~CIEPlugin(void)
{
}

void CIEPlugin::SetStatusText(const char* txt)
{
	m_OcxCtrl->SetStatusText(txt);
}

void CIEPlugin::ComputeBaseUrl(TCHAR* BaseUrl, INT MaxChar)
{
	ASSERT(m_OcxCtrl);
	LPOLECLIENTSITE pClientSite = m_OcxCtrl->GetClientSite();
	if (pClientSite)
	{
		HRESULT hr;
		IMoniker *pMoniker = NULL;
		IBindCtx *pBC = NULL;
		LPOLESTR wRootPath = NULL;

		hr = pClientSite->GetMoniker(OLEGETMONIKER_FORCEASSIGN, OLEWHICHMK_CONTAINER, &pMoniker);
		if (SUCCEEDED(hr))
		{
			hr = CreateBindCtx(0, &pBC);
			if (SUCCEEDED(hr))
			{
				hr = pMoniker->GetDisplayName(pBC, NULL, &wRootPath);
				if (SUCCEEDED(hr))
				{
					ASSERT(wRootPath);

					wcstombs(BaseUrl, wRootPath, wcslen(wRootPath) + 1);

					URLUtility::ReviseURL(BaseUrl,BaseUrl,1024);

					LPMALLOC pMalloc;
					hr = CoGetMalloc(1, &pMalloc);
					if (SUCCEEDED(hr))
					{
						pMalloc->Free(wRootPath);
					}
					else
					{
						// N.B.: panic!
					}
				}
			}
		}
	}
}