#include "StdAfx.h"
#include "LWApplication.h"
#include "LWViewport.h"
#include "ILWPlugin.h"
#include "LWEngine.h"


LWApplication::LWApplication(IGame* iGame, HINSTANCE hInst)
	:m_hInstance(hInst), m_iGame(iGame),m_Viewport(NULL)
{
	ASSERT(m_iGame);
	m_StreamSystem = NULL;
	m_StreamClient = NULL;
	m_Loading = TRUE;
	m_TrackMenu = NULL;
	m_TrackMenu0 = NULL;
}

LWApplication::~LWApplication(void)
{
	Destroy();
}

void LWApplication::AttachViewport(LWViewport* Viewport)
{
	ASSERT(m_Viewport == NULL);
	m_Viewport = Viewport;
	ASSERT(m_Viewport != NULL);
}

void LWApplication::Destroy()
{
	Lock();
	if (m_TrackMenu)
	{
		DestroyMenu(m_TrackMenu);
		m_TrackMenu = NULL;
	}
	if (m_iGame)
	{
		m_iGame->Destroy();	// Must delete this.
		m_iGame = NULL;
	}
	if (m_StreamSystem)
	{
		m_StreamSystem->Release();
		m_StreamSystem = NULL;
	}
	if (m_StreamClient)
	{
		delete m_StreamClient;
		m_StreamClient = NULL;
	}
	if (m_Plugin)
	{
		delete m_Plugin;
		m_Plugin = NULL;
	}
	Unlock();
	STREAM_LOG("LWAppication Destroyed\n");
}

void LWApplication::Update()
{
	Lock();
	
	if (m_iGame)
	{
		m_Loading = m_iGame->IsLoadingScene();
		if (m_Loading)
		{
			m_iGame->GetSceneLoadingSink()->OnRender();
		}else
		{
			m_iGame->Update();
		}
	}
	Unlock();

	m_StreamSystem->Stream();
}

// 初始化WEB客户程序.
BOOL LWApplication::Initialise()
{
	// 初始化Stream System
	//m_hStreamSystem = LoadLibrary("LWStream.dll");
	m_StreamSystem = CreateStreamSystem();
	if (m_StreamSystem == NULL)
	{
		return FALSE;
	}

	m_StreamClient = new LWStreamClient(this);
	if (m_StreamClient == NULL)
	{
		return FALSE;
	}
	m_StreamSystem->SetClient(m_StreamClient);
	if (!m_StreamSystem->Intialise())
	{
		return FALSE;
	}
	
	// 务必确保客户端程序对象和视口有效.
	if (m_iGame == NULL || m_Viewport == NULL)
	{
		return FALSE;
	}
	ASSERT(m_Plugin);
	m_Plugin->ComputeBaseUrl(m_BaseUrl,1024);
	m_StreamSystem->SetBaseUrl(m_BaseUrl);
	m_TrackMenu = LoadMenu(m_hInstance, MAKEINTRESOURCE(IDR_MENU1));
	ASSERT(m_TrackMenu);
	m_TrackMenu0 = GetSubMenu(m_TrackMenu,0);

	m_iGame->InitializeForCtrl(m_Viewport->m_hWnd,m_StreamSystem);
	// 移动到客户端.
	//SetSpawnSceneURL("Scenes/test5.nif");
	//EnterScene();
	return TRUE;
}

void LWApplication::SetLoadingScene(BOOL bLoading)
{
	m_Loading = bLoading;
}

void LWApplication::SetSpawnSceneURL(const TCHAR* Path)
{
	m_ScenePath = Path;
}

ILWResource* LWApplication::SceneLoadedCB(String* RequestURL, String* CacheFileName, void* UserData)
{
	
	LWApplication* pThis = (LWApplication*)UserData;
	ASSERT(pThis);
	pThis->Lock();
	if (pThis->m_iGame)
	{
		pThis->m_iGame->LoadScene(RequestURL->c_str(),CacheFileName->c_str(),UserData);

		pThis->SetLoadingScene(FALSE);
	}
	pThis->Unlock();
	return NULL;
}

void LWApplication::SceneLoadProgressCallback(float Ratio, void *UserData)
{
	
}

void LWApplication::EnterScene()
{
	Lock();
	if (m_ScenePath.GetLength() > 0)
	{
		// 启动装载界面
		SetLoadingScene(TRUE);
		ResourceCallback CB;
		memset(&CB,0,sizeof(ResourceCallback));
		CB.LoadedCB = SceneLoadedCB;
		CB.ProgressCB = SceneLoadProgressCallback;

		m_StreamSystem->StreamResource(m_ScenePath,&CB,this);
	}else
	{
		SetLoadingScene(TRUE);	
	}
	Unlock();
}