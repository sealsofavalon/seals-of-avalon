#pragma once
#include "LWStreamClient.h"

class LWViewport;
struct ILWPlugin;
struct IGame;
// Client Application wrap
class LWApplication //: public NiCriticalSection
{
public:
	LWApplication(IGame* iGame, HINSTANCE hInst);
	~LWApplication(void);
	// 初始化客户端
	BOOL Initialise();
	// 销毁
	void Destroy();
	// 附加视口. 
	// 不在LWViewport::OnCreate中处理渲染设备,输入设备的创建,而是将视口指针传入到这里,统一处理.
	// @see LWApplication::Initialise
	void AttachViewport(LWViewport* Viewport);
	void SetPlugin(ILWPlugin* Plugin)	{m_Plugin = Plugin;}
	void Update();

	// 显示加载界面.
	void SetLoadingScene(BOOL bLoading = TRUE);
	// 设置出生场景地址
	void SetSpawnSceneURL(const TCHAR* Path);
	// 进入场景,将停止装载界面显示.
	void EnterScene();
	
	//TODO 调用 iGame->Lock 
	void Lock() { m_CirticalSec.Lock();}
	void Unlock() { m_CirticalSec.Unlock(); }

	static ILWResource* SceneLoadedCB(String* RequestURL, String* CacheFileName, void* UserData);
	static void SceneLoadProgressCallback(float Ratio, void *UserData);
	
	ILWPlugin* GetPlugin()
	{
		return m_Plugin;
	}
private:
	HINSTANCE m_hInstance;
	ILWPlugin* m_Plugin;
	//NiApplication* m_ClientApp;		//< 客户端应用程序
	IGame*	m_iGame;
	LWViewport* m_Viewport;			//< 视口.
	ILWStreamSystem* m_StreamSystem;
	LWStreamClient* m_StreamClient;
	HMENU m_TrackMenu;
	HMENU m_TrackMenu0;
	HMODULE m_hStreamSystem;
	BOOL m_Loading;
	TCHAR m_BaseUrl[1024];
	CString m_ScenePath;
	CriticalSection m_CirticalSec;
};
