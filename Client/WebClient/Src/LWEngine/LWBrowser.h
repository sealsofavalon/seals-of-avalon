// LWBrowser.h : CLWBrowser ������

#pragma once
#include "resource.h"       // ������

#include "LWEngine.h"


// CLWBrowser

class ATL_NO_VTABLE CLWBrowser : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CLWBrowser, &CLSID_LWBrowser>,
	public IDispatchImpl<ILWBrowser, &IID_ILWBrowser, &LIBID_LWEngineLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CLWBrowser()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_LWBROWSER)


BEGIN_COM_MAP(CLWBrowser)
	COM_INTERFACE_ENTRY(ILWBrowser)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:

};

OBJECT_ENTRY_AUTO(__uuidof(LWBrowser), CLWBrowser)
