// LWEngine.cpp : CLWEngineApp 和 DLL 注册的实现。

#include "stdafx.h"
#include "LWEngine.h"
#include <initguid.h>
#include "LWEngine_i.c"
#include "LWApplication.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



class CLWEngineModule :
	public CAtlMfcModule
{
public:
	DECLARE_LIBID(LIBID_LWEngineLib);
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_LWENGINE, "{B6697DF8-2861-457A-967D-73FB6C8C9F65}");};

	CLWEngineModule _AtlModule;

	CLWEngineApp NEAR theApp;

	const GUID CDECL BASED_CODE _tlid =
	{ 0xD4EAA65F, 0x6E93, 0x42E0, { 0x84, 0xD6, 0xC2, 0x2, 0x55, 0x9A, 0x60, 0xBE } };
	const WORD _wVerMajor = 1;
	const WORD _wVerMinor = 0;



	// CLWEngineApp::InitInstance - DLL 初始化

	BOOL CLWEngineApp::InitInstance()
	{
		BOOL bInit = COleControlModule::InitInstance();

		if (bInit)
		{
			//return TRUE;
#if 0 
			// LoadLibrary 总是失败, 目前原因不明, 很可能是客户端静态变量引起的.
			// NOTE 对NDL 的其它几个工程显示载入都能成功.
			char ModulePath[MAX_PATH];

			HMODULE hThisModule = GetModuleHandle("LWEngine.ocx");
			GetModuleFileName(hThisModule,ModulePath,MAX_PATH);
			char* pSlash = strrchr(ModulePath,'\\');
			if (pSlash == NULL)
			{
				pSlash = strrchr(ModulePath,'/');
			}
			ASSERT(pSlash);
			*++pSlash = '\0';
			strcpy(pSlash,"SyGame.dll");

			HMODULE m_hGameInterface = LoadLibrary(ModulePath);
			if (m_hGameInterface == NULL)
			{
				DWORD error = GetLastError();
				error;
				return FALSE;
			}
			CreateGameInterfaceFunc CreateFunc = (CreateGameInterfaceFunc)GetProcAddress(m_hGameInterface,"CreateGameInterface");
			if (CreateFunc == NULL)
			{
				return FALSE;
			}
			IGame* GI = CreateFunc(m_hInstance);
#else
			IGame* GI = CreateGameInterface(m_hInstance);
#endif 
			if (!GI)
			{
				return FALSE;
			}
			

			m_AppInterface = new LWApplication(GI,m_hInstance);
		}

		return bInit;
	}



	// CLWEngineApp::ExitInstance - DLL 终止

	int CLWEngineApp::ExitInstance()
	{
		// TODO: 在此添加您自己的模块终止代码。
		if (m_AppInterface)
		{
			m_AppInterface->Destroy();
			delete m_AppInterface;
			m_AppInterface = NULL;
		}
		STREAM_LOG("OLE APP Exit\n");
		int ret = COleControlModule::ExitInstance();
		STREAM_LOG("COleControlModule Exit\n");
		return ret;
	}


	const CATID CLSID_SafeItem = {0x5c271d51, 0xa5ff, 0x4a6a, {0xbd, 0x5b, 0x6c, 0x65, 0x80, 0xab, 0x3a, 0xfd}};
	// 安全载入控件代码. 
	// HRESULT CreateComponentCategory - Used to register ActiveX control as safe
#include "comcat.h"
#include "strsafe.h"
#include "objsafe.h"

	HRESULT CreateComponentCategory(CATID catid, WCHAR *catDescription)
	{
		ICatRegister *pcr = NULL ;
		HRESULT hr = S_OK ;

		hr = CoCreateInstance(CLSID_StdComponentCategoriesMgr, 
			NULL, CLSCTX_INPROC_SERVER, IID_ICatRegister, (void**)&pcr);
		if (FAILED(hr))
			return hr;

		// Make sure the HKCR\Component Categories\{..catid...}
		// key is registered.
		CATEGORYINFO catinfo;
		catinfo.catid = catid;
		catinfo.lcid = 0x0409 ; // english
		size_t len;
		// Make sure the provided description is not too long.
		// Only copy the first 127 characters if it is.
		// The second parameter of StringCchLength is the maximum
		// number of characters that may be read into catDescription.
		// There must be room for a NULL-terminator. The third parameter
		// contains the number of characters excluding the NULL-terminator.
		len = wcslen(catDescription);
		if (len>127)
			len = 127;
		wcsncpy(catinfo.szDescription, catDescription, len);
		// Make sure the description is null terminated.
		catinfo.szDescription[len] = '\0';

		hr = pcr->RegisterCategories(1, &catinfo);
		pcr->Release();

		return hr;
	}

	// HRESULT RegisterCLSIDInCategory - Register your component categories information

	HRESULT RegisterCLSIDInCategory(REFCLSID clsid, CATID catid)
	{
		// Register your component categories information.
		ICatRegister *pcr = NULL ;
		HRESULT hr = S_OK ;
		hr = CoCreateInstance(CLSID_StdComponentCategoriesMgr, 
			NULL, CLSCTX_INPROC_SERVER, IID_ICatRegister, (void**)&pcr);
		if (SUCCEEDED(hr))
		{
			// Register this category as being "implemented" by the class.
			CATID rgcatid[1] ;
			rgcatid[0] = catid;
			hr = pcr->RegisterClassImplCategories(clsid, 1, rgcatid);
		}

		if (pcr != NULL)
			pcr->Release();

		return hr;
	}


	HRESULT UnRegisterCLSIDInCategory(REFCLSID clsid, CATID catid)
	{
		ICatRegister *pcr = NULL ;
		HRESULT hr = S_OK ;

		hr = CoCreateInstance(CLSID_StdComponentCategoriesMgr, 
			NULL, CLSCTX_INPROC_SERVER, IID_ICatRegister, (void**)&pcr);
		if (SUCCEEDED(hr))
		{
			// Unregister this category as being "implemented" by the class.
			CATID rgcatid[1] ;
			rgcatid[0] = catid;
			hr = pcr->UnRegisterClassImplCategories(clsid, 1, rgcatid);
		}

		if (pcr != NULL)
			pcr->Release();

		return hr;
	}
	// DllRegisterServer - 将项添加到系统注册表

	STDAPI DllRegisterServer(void)
	{
		//_AtlModule.UpdateRegistryAppId(TRUE);
		HRESULT hRes;
		//	HRESULT hRes = _AtlModule.RegisterServer(TRUE);
		//	if (hRes != S_OK)
		//	return hRes;
		AFX_MANAGE_STATE(_afxModuleAddrThis);

		if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
			return ResultFromScode(SELFREG_E_TYPELIB);

		if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
			return ResultFromScode(SELFREG_E_CLASS);

		// Mark the control as safe for initializing.
		hRes = CreateComponentCategory(CATID_SafeForInitializing, L"Controls safely initializable from persistent data!");
		if (FAILED(hRes))
			return hRes;

		hRes = RegisterCLSIDInCategory(CLSID_SafeItem, CATID_SafeForInitializing);
		if (FAILED(hRes))
			return hRes;

		// Mark the control as safe for scripting.

		hRes = CreateComponentCategory(CATID_SafeForScripting, L"Controls safely scriptable!");
		if (FAILED(hRes))
			return hRes;

		hRes = RegisterCLSIDInCategory(CLSID_SafeItem, CATID_SafeForScripting);
		if (FAILED(hRes))
			return hRes;

		return NOERROR;
	}



	// DllUnregisterServer - 将项从系统注册表中移除

	STDAPI DllUnregisterServer(void)
	{
		HRESULT hRes;
		//	_AtlModule.UpdateRegistryAppId(FALSE);
		//	HRESULT hRes = _AtlModule.UnregisterServer(TRUE);
		//	if (hRes != S_OK)
		//		return hRes;
		AFX_MANAGE_STATE(_afxModuleAddrThis);

		if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
			return ResultFromScode(SELFREG_E_TYPELIB);

		if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
			return ResultFromScode(SELFREG_E_CLASS);

		hRes=UnRegisterCLSIDInCategory(CLSID_SafeItem, CATID_SafeForInitializing);
		if (FAILED(hRes))
			return hRes;

		hRes=UnRegisterCLSIDInCategory(CLSID_SafeItem, CATID_SafeForScripting);
		if (FAILED(hRes))
			return hRes;

		return NOERROR;
	}

	// DllCanUnloadNow - Allows COM to unload DLL
#pragma comment(linker, "/EXPORT:DllCanUnloadNow=_DllCanUnloadNow@0,PRIVATE")
#pragma comment(linker, "/EXPORT:DllGetClassObject=_DllGetClassObject@12,PRIVATE")
#pragma comment(linker, "/EXPORT:DllRegisterServer=_DllRegisterServer@0,PRIVATE")
#pragma comment(linker, "/EXPORT:DllUnregisterServer=_DllUnregisterServer@0,PRIVATE")

	STDAPI DllCanUnloadNow(void)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState());
		if (_AtlModule.GetLockCount() > 0)
			return S_FALSE;
		return S_OK;
	}

	// DllGetClassObject - Returns class factory
	STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState());
		if (S_OK == _AtlModule.GetClassObject(rclsid, riid, ppv))
			return S_OK;
		return AfxDllGetClassObject(rclsid, riid, ppv);
	}
