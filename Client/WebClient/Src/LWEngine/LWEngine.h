#pragma once

// LWEngine.h : LWEngine.DLL 的主头文件

#if !defined( __AFXCTL_H__ )
#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // 主符号
#include "LWEngine_i.h"


// CLWEngineApp : 有关实现的信息，请参阅 LWEngine.cpp。

class CLWEngineApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
	class LWApplication* m_AppInterface;
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

