// LWEngineCtrl.cpp : CLWEngineCtrl ActiveX 控件类的实现。

#include "stdafx.h"
#include "LWEngine.h"
#include "LWEngineCtrl.h"
#include "LWEnginePropPage.h"
#include "LWApplication.h"
#include "LWViewport.h"
#include "IEPlugin.h"
#include "OptionDialog.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CLWEngineCtrl, COleControl)



// 消息映射

BEGIN_MESSAGE_MAP(CLWEngineCtrl, COleControl)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
	ON_WM_SETFOCUS()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
	ON_WM_DESTROY()
END_MESSAGE_MAP()



// 调度映射

BEGIN_DISPATCH_MAP(CLWEngineCtrl, COleControl)
	DISP_FUNCTION_ID(CLWEngineCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
	DISP_PROPERTY_NOTIFY_ID(CLWEngineCtrl, "Src", dispidSrc, m_Src, OnSrcChanged, VT_BSTR)
END_DISPATCH_MAP()



// 事件映射

BEGIN_EVENT_MAP(CLWEngineCtrl, COleControl)
END_EVENT_MAP()



// 属性页

// TODO: 按需要添加更多属性页。请记住增加计数！
BEGIN_PROPPAGEIDS(CLWEngineCtrl, 1)
	PROPPAGEID(CLWEnginePropPage::guid)
END_PROPPAGEIDS(CLWEngineCtrl)



// 初始化类工厂和 guid

IMPLEMENT_OLECREATE_EX(CLWEngineCtrl, "LWENGINE.LWEngineCtrl.1",
	0x5c271d51, 0xa5ff, 0x4a6a, 0xbd, 0x5b, 0x6c, 0x65, 0x80, 0xab, 0x3a, 0xfd)



// 键入库 ID 和版本

IMPLEMENT_OLETYPELIB(CLWEngineCtrl, _tlid, _wVerMajor, _wVerMinor)



// 接口 ID

const IID BASED_CODE IID_DLWEngine =
		{ 0x94D8B2A6, 0xA564, 0x41A4, { 0xB0, 0x7D, 0xE6, 0x3, 0xBB, 0x24, 0xEF, 0xCF } };
const IID BASED_CODE IID_DLWEngineEvents =
		{ 0x248D4EF5, 0xD95, 0x468D, { 0x86, 0x76, 0x74, 0xF6, 0x12, 0x4A, 0x60, 0xED } };



// 控件类型信息

static const DWORD BASED_CODE _dwLWEngineOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CLWEngineCtrl, IDS_LWENGINE, _dwLWEngineOleMisc)



// CLWEngineCtrl::CLWEngineCtrlFactory::UpdateRegistry -
// 添加或移除 CLWEngineCtrl 的系统注册表项

BOOL CLWEngineCtrl::CLWEngineCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: 验证您的控件是否符合单元模型线程处理规则。
	// 有关更多信息，请参考 MFC 技术说明 64。
	// 如果您的控件不符合单元模型规则，则
	// 必须修改如下代码，将第六个参数从
	// afxRegApartmentThreading 改为 0。

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_LWENGINE,
			IDB_LWENGINE,
			afxRegApartmentThreading,
			_dwLWEngineOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}



// CLWEngineCtrl::CLWEngineCtrl - 构造函数

CLWEngineCtrl::CLWEngineCtrl()
{
	InitializeIIDs(&IID_DLWEngine, &IID_DLWEngineEvents);
	// TODO: 在此初始化控件的实例数据。
	m_ClientApp = NULL;
	m_Viewport = NULL;

	m_StatusDirty = FALSE;
}



// CLWEngineCtrl::~CLWEngineCtrl - 析构函数

CLWEngineCtrl::~CLWEngineCtrl()
{
	DestroyFrame();
	STREAM_LOG("OCX CTRL DESCONSTRUCTED.\n");
}



// CLWEngineCtrl::OnDraw - 绘图函数

void CLWEngineCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	if (!pdc)
		return;

	// TODO: 用您自己的绘图代码替换下面的代码。
	//pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	//pdc->Ellipse(rcBounds);
	//pdc->TextOut(10,10,"Sunyou WEB Client Viewport.");
}



// CLWEngineCtrl::DoPropExchange - 持久性支持

void CLWEngineCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: 为每个持久的自定义属性调用 PX_ 函数。
	
}



// CLWEngineCtrl::OnResetState - 将控件重置为默认状态

void CLWEngineCtrl::OnResetState()
{
	COleControl::OnResetState();  // 重置 DoPropExchange 中找到的默认值

	// TODO: 在此重置任意其他控件状态。
}



// CLWEngineCtrl::AboutBox - 向用户显示“关于”框

void CLWEngineCtrl::AboutBox()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CDialog dlgAbout(IDD_ABOUTBOX_LWENGINE);
	dlgAbout.DoModal();
}


// CLWEngineCtrl 消息处理程序

void CLWEngineCtrl::OnSrcChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 在此添加属性处理程序代码

	SetModifiedFlag();
}

void CLWEngineCtrl::OnSetFocus(CWnd* pOldWnd)
{
	COleControl::OnSetFocus(pOldWnd);
	if (m_Viewport)
	{
		m_Viewport->SetFocus();
	}
}

BOOL CLWEngineCtrl::CreateFrame()
{
	ASSERT(m_ClientApp == NULL);
	ASSERT(m_Viewport == NULL);

	try
	{
		//m_ClientApp = new LWApplication;
		CIEPlugin* Plugin = new CIEPlugin(this);
		ASSERT(Plugin);
		CLWEngineApp* OleApp = (CLWEngineApp*)AfxGetApp();
		m_ClientApp = OleApp->m_AppInterface;
		ASSERT(m_ClientApp != NULL);
		m_ClientApp->SetPlugin(Plugin);

		m_Viewport = new LWViewport(this);
		RECT ClientRect;
		GetClientRect(&ClientRect);
		
		// 创建视口
		if (!m_Viewport->Create(NULL, "Viewport", WS_CHILD | WS_VISIBLE, ClientRect, this, 2))
		{
			return FALSE;
		}

		m_ClientApp->AttachViewport(m_Viewport);
		
		if (!m_ClientApp->Initialise())
		{
			return FALSE;
		}


	}
	catch (CException* e)
	{
		
	}
	return TRUE;
}

void CLWEngineCtrl::DestroyFrame()
{
	if (m_Viewport)
	{
	//	m_Viewport->Restore();
		delete m_Viewport; m_Viewport = NULL;
	}

	m_ClientApp = NULL;
}

void CLWEngineCtrl::SetStatusText(const char* Text)
{
	m_StatusCS.Lock();
	m_StatusStr = Text;
	m_StatusDirty = TRUE;
	m_StatusCS.Unlock();
}

void CLWEngineCtrl::UpdateStatusBar()
{
	if( m_pInPlaceFrame && m_StatusDirty ) 
	{
		static wchar_t buf[1000];
		m_StatusCS.Lock();
		INT Ret = MultiByteToWideChar(CP_ACP,0,m_StatusStr.GetBuffer(0),m_StatusStr.GetLength(),buf,1000);
		buf[Ret] = 0;
		m_StatusCS.Unlock();
		m_pInPlaceFrame->SetStatusText(buf);
		m_StatusDirty = FALSE;
	}
}

int CLWEngineCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (COleControl::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码
	if (!CreateFrame())
	{
		return -1;
	}
	SetTimer(0xE0F,40,NULL);
	return 0;
}

void CLWEngineCtrl::OnSize(UINT nType, int cx, int cy)
{
	COleControl::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
	if (m_ClientApp != NULL)
	{
		m_Viewport->SetWindowPos(NULL, 0, 0, cx, cy, 0);
	}
}

BOOL CLWEngineCtrl::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	//return COleControl::OnEraseBkgnd(pDC);
	return TRUE;
}

void CLWEngineCtrl::OnTimer(UINT nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (nIDEvent == 0xE0F)
	{
		if (m_ClientApp)
		{
			m_ClientApp->Update();
		}
		UpdateStatusBar();
	}
	
	COleControl::OnTimer(nIDEvent);
}

void CLWEngineCtrl::OnDestroy()
{
if (m_Viewport)
{
	m_Viewport->Restore();
}

	COleControl::OnDestroy();

	// TODO: 在此处添加消息处理程序代码
}
