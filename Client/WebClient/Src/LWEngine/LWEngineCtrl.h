#pragma once

// LWEngineCtrl.h : CLWEngineCtrl ActiveX 控件类的声明。


// CLWEngineCtrl : 有关实现的信息，请参阅 LWEngineCtrl.cpp。



class CLWEngineCtrl : public COleControl
{
	DECLARE_DYNCREATE(CLWEngineCtrl)

// 构造函数
public:
	CLWEngineCtrl();

// 重写
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();

	void Lock()		{ m_CritticalSec.Lock(); }
	void Unlock()	{ m_CritticalSec.Unlock(); }
	void SetStatusText(const char* Text);
// 实现
protected:
	CriticalSection m_CritticalSec; 
	CriticalSection m_StatusCS;
	CString m_StatusStr;
	BOOL m_StatusDirty;

	class LWApplication* m_ClientApp;
	class LWViewport* m_Viewport;

	BOOL CreateFrame();
	void DestroyFrame();
	void UpdateStatusBar();
protected:
	~CLWEngineCtrl();

	DECLARE_OLECREATE_EX(CLWEngineCtrl)    // 类工厂和 guid
	DECLARE_OLETYPELIB(CLWEngineCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CLWEngineCtrl)     // 属性页 ID
	DECLARE_OLECTLTYPE(CLWEngineCtrl)		// 类型名称和杂项状态

// 消息映射
	DECLARE_MESSAGE_MAP()

// 调度映射
	DECLARE_DISPATCH_MAP()

	

// 事件映射
	DECLARE_EVENT_MAP()

// 调度和事件 ID
public:
	enum {
		dispidSrc = 1
	};
protected:
	void OnSrcChanged(void);
	CString m_Src;
public:
	afx_msg void AboutBox();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnTimer(UINT nIDEvent);
public:
	afx_msg void OnDestroy();
};

