// LWEnginePropPage.cpp : CLWEnginePropPage 属性页类的实现。

#include "stdafx.h"
#include "LWEngine.h"
#include "LWEnginePropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CLWEnginePropPage, COlePropertyPage)



// 消息映射

BEGIN_MESSAGE_MAP(CLWEnginePropPage, COlePropertyPage)
END_MESSAGE_MAP()



// 初始化类工厂和 guid

IMPLEMENT_OLECREATE_EX(CLWEnginePropPage, "LWENGINE.LWEnginePropPage.1",
	0xc45ff7a5, 0xe4e, 0x40b9, 0xb2, 0x97, 0xe, 0xc1, 0x51, 0x9b, 0x25, 0xc8)



// CLWEnginePropPage::CLWEnginePropPageFactory::UpdateRegistry -
// 添加或移除 CLWEnginePropPage 的系统注册表项

BOOL CLWEnginePropPage::CLWEnginePropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_LWENGINE_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}



// CLWEnginePropPage::CLWEnginePropPage - 构造函数

CLWEnginePropPage::CLWEnginePropPage() :
	COlePropertyPage(IDD, IDS_LWENGINE_PPG_CAPTION)
{
}



// CLWEnginePropPage::DoDataExchange - 在页和属性间移动数据

void CLWEnginePropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}



// CLWEnginePropPage 消息处理程序
