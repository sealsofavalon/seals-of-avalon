#pragma once

// LWEnginePropPage.h : CLWEnginePropPage 属性页类的声明。


// CLWEnginePropPage : 有关实现的信息，请参阅 LWEnginePropPage.cpp。

class CLWEnginePropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CLWEnginePropPage)
	DECLARE_OLECREATE_EX(CLWEnginePropPage)

// 构造函数
public:
	CLWEnginePropPage();

// 对话框数据
	enum { IDD = IDD_PROPPAGE_LWENGINE };

// 实现
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 消息映射
protected:
	DECLARE_MESSAGE_MAP()
};

