

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Tue Jan 15 11:50:12 2008
 */
/* Compiler settings for .\LWEngine.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __LWEngineidl_h__
#define __LWEngineidl_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef ___DLWEngine_FWD_DEFINED__
#define ___DLWEngine_FWD_DEFINED__
typedef interface _DLWEngine _DLWEngine;
#endif 	/* ___DLWEngine_FWD_DEFINED__ */


#ifndef ___DLWEngineEvents_FWD_DEFINED__
#define ___DLWEngineEvents_FWD_DEFINED__
typedef interface _DLWEngineEvents _DLWEngineEvents;
#endif 	/* ___DLWEngineEvents_FWD_DEFINED__ */


#ifndef __LWEngine_FWD_DEFINED__
#define __LWEngine_FWD_DEFINED__

#ifdef __cplusplus
typedef class LWEngine LWEngine;
#else
typedef struct LWEngine LWEngine;
#endif /* __cplusplus */

#endif 	/* __LWEngine_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 


#ifndef __LWEngineLib_LIBRARY_DEFINED__
#define __LWEngineLib_LIBRARY_DEFINED__

/* library LWEngineLib */
/* [control][helpstring][helpfile][version][uuid] */ 


EXTERN_C const IID LIBID_LWEngineLib;

#ifndef ___DLWEngine_DISPINTERFACE_DEFINED__
#define ___DLWEngine_DISPINTERFACE_DEFINED__

/* dispinterface _DLWEngine */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__DLWEngine;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("94D8B2A6-A564-41A4-B07D-E603BB24EFCF")
    _DLWEngine : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DLWEngineVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DLWEngine * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DLWEngine * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DLWEngine * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DLWEngine * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DLWEngine * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DLWEngine * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DLWEngine * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DLWEngineVtbl;

    interface _DLWEngine
    {
        CONST_VTBL struct _DLWEngineVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DLWEngine_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _DLWEngine_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _DLWEngine_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _DLWEngine_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _DLWEngine_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _DLWEngine_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _DLWEngine_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DLWEngine_DISPINTERFACE_DEFINED__ */


#ifndef ___DLWEngineEvents_DISPINTERFACE_DEFINED__
#define ___DLWEngineEvents_DISPINTERFACE_DEFINED__

/* dispinterface _DLWEngineEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__DLWEngineEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("248D4EF5-0D95-468D-8676-74F6124A60ED")
    _DLWEngineEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DLWEngineEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DLWEngineEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DLWEngineEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DLWEngineEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DLWEngineEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DLWEngineEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DLWEngineEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DLWEngineEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DLWEngineEventsVtbl;

    interface _DLWEngineEvents
    {
        CONST_VTBL struct _DLWEngineEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DLWEngineEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _DLWEngineEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _DLWEngineEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _DLWEngineEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _DLWEngineEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _DLWEngineEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _DLWEngineEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DLWEngineEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_LWEngine;

#ifdef __cplusplus

class DECLSPEC_UUID("5C271D51-A5FF-4A6A-BD5B-6C6580AB3AFD")
LWEngine;
#endif
#endif /* __LWEngineLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


