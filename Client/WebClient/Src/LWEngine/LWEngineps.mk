
LWEngineps.dll: dlldata.obj LWEngine_p.obj LWEngine_i.obj
	link /dll /out:LWEngineps.dll /def:LWEngineps.def /entry:DllMain dlldata.obj LWEngine_p.obj LWEngine_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \
.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0500 /DREGISTER_PROXY_DLL \
		$<
# _WIN32_WINNT=0x0500 is for Win2000, change it to 0x0400 for NT4 or Win95 with DCOM

clean:
	@del LWEngineps.dll
	@del LWEngineps.lib
	@del LWEngineps.exp
	@del dlldata.obj
	@del LWEngine_p.obj
	@del LWEngine_i.obj
