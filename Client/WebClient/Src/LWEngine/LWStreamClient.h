#pragma once

class LWApplication;

class NIStreamResource
{
   
};


class LWStreamClient : public ILWStreamClient
{
public:
	LWStreamClient(LWApplication* App);
	~LWStreamClient(void);
	BOOL Initialize();
	// 状态信息显示.
	virtual void OnSetStatusText(const char* Status);

	// 通过下面2个接口来实现资源管理.
	virtual ILWResource* FindResource(const char* szKey);
	virtual void AddResource(const char* szKey, ILWResource* Res);

	LWApplication* m_Application;
	ResourceCallback m_NifCB;
	ResourceCallback m_TextureCB;
};

