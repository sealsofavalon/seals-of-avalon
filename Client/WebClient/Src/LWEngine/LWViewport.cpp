// LWViewport.cpp : 实现文件
//

#include "stdafx.h"
#include "LWEngine.h"
#include "LWViewport.h"
#include "LWEngineCtrl.h"
#include ".\lwviewport.h"

// CLWViewport

IMPLEMENT_DYNAMIC(LWViewport, CWnd)
LWViewport::LWViewport(CLWEngineCtrl* Ctrl)
{
	m_Ctrl = Ctrl;
	m_FullScreen = FALSE;
}

LWViewport::~LWViewport()
{
	
}

void LWViewport::Restore()
{
	WNDPROC Proc = (WNDPROC)::GetWindowLong(m_hWnd, GWL_WNDPROC);
	if (Proc != m_DefaultProc)
	{
		::SetWindowLong(m_hWnd,GWL_WNDPROC, (long)m_DefaultProc);
	}
}

BEGIN_MESSAGE_MAP(LWViewport, CWnd)
	ON_WM_CHAR()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_WM_SETCURSOR()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_ABOUTBOX, OnAboutbox)
	ON_COMMAND(ID_CTXMENU_FULLSCREEN, OnCtxmenuFullscreen)
	ON_UPDATE_COMMAND_UI(ID_CTXMENU_FULLSCREEN, OnUpdateCtxmenuFullscreen)
END_MESSAGE_MAP()



// CLWViewport 消息处理程序


void LWViewport::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CWnd::OnChar(nChar, nRepCnt, nFlags);
}

void LWViewport::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

void LWViewport::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

void LWViewport::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CWnd::OnLButtonDown(nFlags, point);
}

void LWViewport::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CWnd::OnLButtonUp(nFlags, point);
}

void LWViewport::OnMButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CWnd::OnMButtonDown(nFlags, point);
}

void LWViewport::OnMButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CWnd::OnMButtonUp(nFlags, point);
}

void LWViewport::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CMenu menu;
	menu.LoadMenu(IDR_MENU1);
	CMenu* Popup = menu.GetSubMenu(0);
	CPoint ptScreen = point;
	ClientToScreen(&ptScreen);
	TrackPopupMenu(Popup->GetSafeHmenu(), TPM_RIGHTBUTTON, ptScreen.x, ptScreen.y, 0, GetSafeHwnd(), 0);

	CWnd::OnRButtonDown(nFlags, point);
}

void LWViewport::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CWnd::OnRButtonUp(nFlags, point);
}

void LWViewport::OnSetFocus(CWnd* pOldWnd)
{
	CWnd::OnSetFocus(pOldWnd);

	// TODO: 在此处添加消息处理程序代码
}

void LWViewport::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
}

BOOL LWViewport::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

int LWViewport::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_DefaultProc = (WNDPROC)::GetWindowLong(m_hWnd, GWL_WNDPROC);
	// TODO:  在此添加您专用的创建代码
	return 0;
}

void LWViewport::OnDestroy()
{
	CWnd::OnDestroy();

	// TODO: 在此处添加消息处理程序代码
}


BOOL LWViewport::CreateRender()
{
	return TRUE;
}
void LWViewport::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CWnd::OnMouseMove(nFlags, point);
}

void LWViewport::OnAboutbox()
{
	if (m_Ctrl)
	{
		m_Ctrl->AboutBox();
	}
}

void LWViewport::OnCtxmenuFullscreen()
{
	// 这里我们不必关心质量,进行复杂地设备重构.
	if (!m_FullScreen)
	{
		m_FullScreen = TRUE;
		CWnd* Desktop = GetDesktopWindow();
		SetParent(Desktop);
		CRect rect;
		Desktop->GetWindowRect(&rect);
		SetWindowPos(&wndTopMost,rect.left,rect.top,rect.right,rect.bottom,SWP_SHOWWINDOW);
	}else
	{
		m_FullScreen = FALSE;
		SetParent(m_Ctrl);
		CRect rect;
		m_Ctrl->GetWindowRect(&rect);
		SetWindowPos(NULL,0,0,rect.Width(),rect.Height(),0);
	}
}

void LWViewport::OnUpdateCtxmenuFullscreen(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_FullScreen);
}
