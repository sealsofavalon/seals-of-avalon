#pragma once

class LWApplication;
class CLWEngineCtrl;
// CLWViewport
// 嵌入OLE窗口中,用来作为消息交互和渲染窗口.
class LWViewport : public CWnd
{
	DECLARE_DYNAMIC(LWViewport)

public:
	LWViewport(CLWEngineCtrl* Ctrl);
	virtual ~LWViewport();
	void Restore();
protected:
	LWApplication* m_Client;
	CLWEngineCtrl* m_Ctrl;
	BOOL m_FullScreen;
private:
	BOOL CreateRender();
	WNDPROC m_DefaultProc;
protected:
	DECLARE_MESSAGE_MAP()

	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnAboutbox();
	afx_msg void OnCtxmenuFullscreen();
	afx_msg void OnUpdateCtxmenuFullscreen(CCmdUI *pCmdUI);
};


