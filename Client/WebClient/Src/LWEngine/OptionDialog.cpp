// OptionDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "LWEngine.h"
#include "OptionDialog.h"


// COptionDialog 对话框

IMPLEMENT_DYNAMIC(COptionDialog, CDialog)
COptionDialog::COptionDialog(CWnd* pParent /*=NULL*/)
	: CDialog(COptionDialog::IDD, pParent)
{
}

COptionDialog::~COptionDialog()
{
}

void COptionDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(COptionDialog, CDialog)
END_MESSAGE_MAP()


// COptionDialog 消息处理程序
