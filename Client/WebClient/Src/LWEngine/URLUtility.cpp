#include "StdAfx.h"
#include "URLUtility.h"

void URLUtility::ReviseURL(const TCHAR* SrcURL, TCHAR* OutBuf, INT MaxChar)
{
	assert(MaxChar > 0);
	char *cp;
	char tmp[1024];
	int len = strlen(SrcURL);
	for (int i = 0; i < len + 1; i++)
	{
		if (SrcURL[i] == '\\')
			tmp[i] = '/';
		else
			tmp[i] = SrcURL[i];
	}

	cp = strrchr(tmp, '/');

	if (!cp)
	{
		OutBuf[0] = '\0';
		return;
	}

	len = cp - tmp + 1;

	if (len > MaxChar - 1)
		len = MaxChar - 1;

	strncpy(OutBuf, tmp, len);
	OutBuf[len] = '\0';
}
