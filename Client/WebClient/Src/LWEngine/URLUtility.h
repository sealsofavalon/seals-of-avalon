#pragma once

class URLUtility
{
public:
	URLUtility(void);
	~URLUtility(void);
	static void ReviseURL(const TCHAR* SrcURL, TCHAR* OutBuf, INT MaxChar);

	static BOOL IsLocalFile(const char *fname)
	{
		// file://
		if (!strnicmp(fname, "file:", 5))
			return TRUE;

		// D:// 
		if (fname[1] == ':')
			return TRUE;

		return FALSE;
	}

	static BOOL IsRootPath(const char *fname)
	{
		if ( fname[0] == '/' || fname[0] == '\\'  )
			return TRUE;

		return FALSE;
	}
	static BOOL IsWebPath(const char *fname)
	{
		if (!strnicmp(fname, "http://", 7))
			return TRUE;

		if (!strnicmp(fname, "ftp://", 6))
			return TRUE;

		return FALSE;
	}
	static BOOL IsRelativePath(const char *fname)
	{
		if( IsWebPath(fname) ) {
			return FALSE;
		}
		return !IsLocalFile(fname);
	}
	
	static void ComputeFullName(const char *url, const char *baseUrl, char *fullname)
	{
		if (baseUrl && strlen(baseUrl))
		{
			if (IsRootPath(url) &&IsWebPath( baseUrl ) ) 
			{
					char RootUrl[2048];
					ComputeRootUrl( baseUrl, RootUrl, 2048 );
					strcpy(fullname, RootUrl);
					strcat(fullname, url);
			}
			else if (IsRelativePath(url))
			{
				strcpy(fullname, baseUrl);
				strcat(fullname, url);
			}
			else
				strcpy(fullname, url);
		}
		else
		{
			strcpy(fullname, url);
		}
		// Look for '#' url silliness and remove it before trying to load
		char *hashchr = strchr(fullname, '#');

		if (hashchr)
			*hashchr = '\0';

	}

	
static void ComputeRootUrl(const char *url, char *outBuf, int maxlen)
{
	assert(maxlen > 0);
	outBuf[0] = '\0';
	int len = strlen(url);
	for (int i = 0; i < len + 1; i++)
	{
		outBuf[i] = url[i];
		if( i>0 &&
			( outBuf[i] == '/' || outBuf[i] == '\\' ) &&
			outBuf[i-1] != '/' &&
			outBuf[i-1] != '\\' &&
			outBuf[i-1] != ':' ) 
		{
			outBuf[i] = '\0';
			break;
		}
	}
}
};
