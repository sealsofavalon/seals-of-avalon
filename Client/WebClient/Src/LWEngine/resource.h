//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by LWEngine.rc
//
#define IDS_LWENGINE                    1
#define IDD_ABOUTBOX_LWENGINE           1
#define IDB_LWENGINE                    1
#define IDI_ABOUTDLL                    1
#define IDS_LWENGINE_PPG                2
#define IDR_LWENGINE                    103
#define IDR_LWBROWSER                   104
#define IDS_LWENGINE_PPG_CAPTION        200
#define IDD_PROPPAGE_LWENGINE           200
#define IDC_RADIO1                      201
#define IDC_EDIT1                       202
#define IDR_MENU1                       203
#define IDC_CHECK1                      203
#define IDD_OPTION                      204
#define ID_CTXMENU_32770                32770
#define ID_Menu                         32772
#define ID_ABOUTBOX                     32773
#define ID_CTXMENU_FULLSCREEN           32774
#define ID_CTXMENU_OPTION               32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        205
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         204
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
