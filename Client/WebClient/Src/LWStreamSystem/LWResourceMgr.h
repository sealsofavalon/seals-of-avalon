#pragma once

class LWResource
{

};

class LWResourceMgr
{
public:
	LWResourceMgr(void);
	~LWResourceMgr(void);

private:
	typedef HashMap<LWString, LWResource*> ResourceMap;
	ResourceMap m_Resource;
};
