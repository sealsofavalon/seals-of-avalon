#pragma once

class StreamThread;

class LWResource
{
public:
	LWResource()
	{

	}

	std::string m_Name;
	LWStreamUserData m_UserData;
};

struct StreamRequest 
{
	ILWStreamClient* RequestClient;
	String* RequestUrl;
	const char* RequestBaseUrl;
	FileLoadedCB RequestLoadedCallback;
	//CAnmFileAssetLoadedCallback requestAssetLoadedCallback;
	ResourceReferenceCB RefCallback;
	FileActiveCallback RequestActiveCallback;
	FileProgressCallback RequestProgressCallback;
	void *UserData;
	BOOL AllowStreaming;

	StreamRequest(ILWStreamClient* Client, String* pUrls, const char *baseUrl,
		FileLoadedCB lcb, /*CAnmFileAssetLoadedCallback alcb,*/ FileActiveCallback acb,
		FileProgressCallback pcb, void *userData, BOOL bAllowStreaming = false )
	{
		RequestClient = Client;
		RequestUrl = pUrls;
		RequestBaseUrl = baseUrl;
		RequestLoadedCallback = lcb;
		//requestAssetLoadedCallback = alcb;
		RequestActiveCallback = acb;
		RequestProgressCallback = pcb;
		UserData = userData;
		AllowStreaming = bAllowStreaming;
	}
	~StreamRequest()
	{
		if (RequestUrl)
		{
			delete RequestUrl;
		}
	}
};

class LWStreamSystem : public ILWStreamSystem
{
	friend class LWBindStatusCallback;
	friend class StreamThread;
	struct ResourceInfo
	{
		char Ext[32];
		ResourceCallback CBInfo;
	};
	enum
	{
		LW_BAD = 0,
	};
public:
	LWStreamSystem(void);
	virtual ~LWStreamSystem(void);
	void Lock()
	{
		EnterCriticalSection(&m_CriticalSec);
	}
	void Unlock()
	{
		LeaveCriticalSection(&m_CriticalSec);
	}
private:
	// 释放对象
	virtual void Release();
	// 初始化
	virtual BOOL Intialise();
	virtual BOOL NeedStream();
	virtual BOOL Stream();
	virtual void SetBaseUrl(const char* BaseUrl);
	// 设置/返回Stream系统监听者
	virtual void SetClient(ILWStreamClient* Clinet);
	virtual ILWStreamClient* GetClient() const;

	// 以默认注册的回调结构创建资源
	virtual void StreamResource(const char* FileName);
	// 以特定的回调结构创建资源.
	virtual void StreamResource(const char* FileName, const ResourceCallback* ResCB, void* UserData);
	// 注册资源对象的回调
	// @param ResExt	资源扩展名
	// @param ResCB		资源回调结构
	virtual BOOL RegisterResource(const char* ResExt, const ResourceCallback* ResCB);
private:
	BOOL ProcessAllRequests();
	BOOL ProcessRequest(StreamRequest* Req);
	BOOL LoadCachedFile(String* url, const char *baseUrl,
									String** pCacheFileName, String** pCacheUrl, 
									ILWResource **ppAsset,
									StreamRequest *pRequest);
	// URL 路径是否已经存储在本地(硬盘或者缓存目录)
	BOOL UrlLoaded(const char* url, String** pCacheFileName, String** pCacheUrl);

	// URL 是否已经下载到缓存目录.
	BOOL UrlInCache(const char *url, String** pCacheFileName);
	BOOL LockUrlCache(String* url);
	BOOL UnlockUrlCache(String* url);
	void NotifyProgress(float progress, void *userData);
	ILWStreamClient* m_Client;
	StreamThread* m_StreamThread;
	CRITICAL_SECTION m_CriticalSec;
	char m_BaseUrl[1024];
	std::deque<StreamRequest*> m_LoadRequests;
	//std::deque<StreamRequest*> m_StreamLoadRequests;
	
};
