#pragma once

// ���紫���߳�
class StreamThread
{
public:
	StreamThread(ILWStreamSystem* System);
	~StreamThread(void);
	void Begin(UINT Priority = THREAD_PRIORITY_NORMAL, BOOL Suspend = FALSE);
	void End(BOOL Waitflag = FALSE);
	BOOL IsRunning();
	DWORD ThreadProc();
	void Suspend();
	void Resume();

	void SetPriority(UINT Priority);
	UINT GetPriority();
	HANDLE GetEvent() { return m_hEvent;}
	ILWStreamSystem* m_StreamSystem;
private:
	HANDLE m_hThread;
	DWORD m_ThreadID;
	HANDLE m_hEvent;
	CRITICAL_SECTION m_CS;
	BOOL m_Runing;
	
};
