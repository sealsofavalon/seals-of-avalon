#ifndef __ILWSTREAMSYSTEM_H__
#define __ILWSTREAMSYSTEM_H__

// 这里不想和NDL耦合太多,不光导致体积倍增而且也不便于重用.
// 因此在设计上用了很多回调, 注意在初始化阶段设置好.

#define STREAM_LOADING_MSG "下载 %s ..."


#include <string>
typedef std::string String;

struct ILWStreaming;
struct ILWResource;

typedef void* LWStreamUserData;

// 文件下载完成的回调
typedef ILWResource* (*FileLoadedCB)(String* RequestURL, String* CacheFileName, void* UserData);

// 文件请求时如果已经存在同名资源,那么会调用这个来判断是否引用既有资源
typedef void (*ResourceReferenceCB)(String* RequestUrl, String* CacheFileName, ILWResource* RefResource, void* userData);

typedef void (*FileActiveCallback) (String* pRequestUrl, String* pCacheFileName, void *UserData);

// 文件下载进程回调.
typedef void (*FileProgressCallback)(float Ratio, void *UserData);


struct ResourceCallback
{
	FileLoadedCB					LoadedCB;
	ResourceReferenceCB				RefCB;
	FileActiveCallback				ActiveCB;
	FileProgressCallback			ProgressCB;
	BOOL							Cache;		// 是否缓存.
};

// 描述文件操作,由用户实现. 简单地说,NiFile的接口. 
struct IFileStream
{

};

// 文件流客户
struct ILWStreamClient 
{
	// 状态信息显示.
	virtual void OnSetStatusText(const char* Status) = 0;

	// 通过下面2个接口来实现资源管理.
	virtual ILWResource* FindResource(const char* szKey) = 0;
	virtual void AddResource(const char* szKey, ILWResource* Res) = 0;
};

// 数据流, 可以看作是一个于存储位置无关的文件/Socket
struct ILWStreaming
{
};

// 资源
struct ILWResource
{
	virtual BOOL OnStreamFinish(void* Handle) = 0;
	LWStreamUserData* m_UserData;		//< 
};

// 文件流系统
struct ILWStreamSystem
{
public:
	// 释放对象
	virtual void Release() = 0;
	// 初始化
	virtual BOOL Intialise() = 0;

	virtual BOOL NeedStream() = 0;
	//virtual BOOL BegineStream() = 0;
	virtual BOOL Stream() =0;
	//virtual void PauseStream() =0;
	//virtual void EndStream() = 0;
	//virtual void Update() = 0;
	// 设置/返回Stream系统监听者
	virtual void SetBaseUrl(const char* BaseUrl) = 0;
	virtual void SetClient(ILWStreamClient* Clinet) = 0;
	virtual ILWStreamClient* GetClient() const = 0;

	// 以默认注册的回调结构创建资源
	virtual void StreamResource(const char* FileName) = 0;
	// 以特定的回调结构创建资源.
	virtual void StreamResource(const char* FileName, const ResourceCallback* ResCB, void* UserData) = 0;
	// 注册资源对象的回调
	// @param ResExt	资源扩展名
	// @param ResCB		资源回调结构
	virtual BOOL RegisterResource(const char* ResExt, const ResourceCallback* ResCB) = 0;

};

//
#ifndef LWStreamApi 
#define LWStreamApi __declspec(dllimport)
#endif

#ifndef __cplusplus
extern "C" {
#endif 
LWStreamApi ILWStreamSystem* CreateStreamSystem();

#ifndef __cplusplus
}
#endif 

// for dynamic link.
typedef ILWStreamSystem* (*_CreateSteamSystemFn)();
typedef _CreateSteamSystemFn CreateSteamSystemFunc;

#endif 