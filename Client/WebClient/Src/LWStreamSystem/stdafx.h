// stdafx.h : 标准系统包含文件的包含文件，
// 或是常用但不常更改的项目特定的包含文件
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// 从 Windows 头中排除极少使用的资料
// Windows 头文件:
#include <windows.h>
#include <Wininet.h>
#pragma comment(lib, "Wininet.lib")
#pragma comment(lib, "Urlmon.lib")
#include <hash_map>
#include <vector>
#include <deque>
#include <string>
#include <assert.h>
#define  HashMap stdext::hash_map
#include "ILWStreamSystem.h"
#include <Objbase.h>
//typedef std::vector<CString> StringArray;
#include  <io.h>
#include "URLUtility.h"

// TODO: 在此处引用程序要求的附加头文件
#ifdef _DEBUG
#define STREAM_LOG(x) OutputDebugString(x)
#else
#define STREAM_LOG(x) 
#endif 

#define DBG_MEMORY 0

#if DBG_MEMORY
#pragma comment(lib,"LthCore_D.lib")
#include <LBase.h>
#include "LCore.h"
#include "LMemory.h"
#endif 