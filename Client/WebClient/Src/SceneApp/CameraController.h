// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#ifndef CAMERACONTROLLER_H
#define CAMERACONTROLLER_H

#include <NiSmartPointer.h>
#include <NiCamera.h>
#include <NiNode.h>
#include <NiScreenElements.h>
#include <NiScreenTexture.h>
#include <NiOldString2D.h>
#include <NiEntityInterface.h>

#include "PickController.h"

class NiFrameRate;
class NiRenderer;
class NiScene;

NiSmartPointer(NiMaterialProperty);

/*
 * CameraController implements a form of �smart camera�.  It adds numerous 
 * features beyond a standard Gamebryo camera and hides implementation 
 * details from other parts of the demo.  The CameraController supports 
 * first and third person control.  It manages user interaction to the
 * demo and updates camera response appropriately.  It also provides
 * management for the 2D elements in the demo (HUD).
*/

class CameraController : public NiMemObject
{
public:
    CameraController(NiScene* pkEntityScene);
    ~CameraController();

    NiCamera* GetCamera() const; // The main, "3D" camera
    void GetAllCameras(NiTPrimitiveArray<NiCamera*>& kCameras) const; 

    void Reset();   
    void UpdateAllCameras(float fTime,
        NiEntityErrorInterface* pkErrorInterface,
        NiExternalAssetManager* pkAssetManager);

    void SetFly(const bool bFly);
    bool GetFly();

    // Retrieves the heading (yaw angle) of the camera from the previous frame
    float GetCameraHeading() const;

    // Retrieves the pitch of the camera.
    float GetCameraPitch() const;

    // Returns yaw and pitch values that are related to the current rotational
    // velocities - these can be used to make the character turn his head or
    // pivot his shoulders when the camera is moved quickly.
    float GetCameraFrameYaw() const;
    float GetCameraFramePitch() const;

    // Per-camera data, designed for expansion to a multi-camera application
    class CameraData : public NiMemObject
    {
    public:
        CameraData();

        NiEntityInterfaceIPtr m_spCameraEntity;

        float m_fCamDist;  // follow-distance of the camera 
        float m_fCamHeight; // height of camera look-at point
        float m_fCamPitch; // pitch of the camera mounting
        float m_fHeading; // current absolute yaw angle
        float m_fFrameYaw; // angular velocity-related outputs
        float m_fFramePitch;
        float m_fLastWorldHeight; // cached terrain height
        float m_fForward;
        float m_fStrafe;
        float m_fVertical;

        float m_fMaxCameraRaise; // Maximum allowed to 'pop' up
        float m_fMaxCameraDrop; // Maximum allowed to drop down
        float m_fWalkVelocity;
        float m_fRunVelocity;

        NiPoint3 m_kTrans;

        bool m_bInvert;

        enum EMoveState
        {
            FLYING,
            LANDING,
            WALKING
        };
        EMoveState m_eMoveState;
    };

    NiEntityInterface* GetEntity() const;

protected:
    float GetInitialHeight(NiPoint3& kWorldTrans);
    void UpdateCameraTransforms();
    void UpdateMap();

    static bool TryPick(
        const CameraController::CameraData &kCamera,
        PickController &kPickController,
        NiPoint3 &kNewPosition, 
        const int iTry = 0);

    static NiPoint3 CalculateNewHeading(
        const CameraController::CameraData &kCamera,
        const float fOffsetAngleRadians,
        const float fMagnitude
        );

    static bool CameraController::IsHeightInWalkingRange(
        const float fHeight, 
        const CameraData &kCamera);

    // Looks for the first camera component in the scene
    // and returns it's camera object.
    NiEntityInterface* FindFirstCamera(NiScene* pkEntityScene);
    void FindAllSceneCameras(
        NiScene* pkEntityScene,
        NiTObjectArray<NiEntityInterfaceIPtr>& kCameras);

    PickController m_kPickController;

    CameraData m_kCamera; // currently, the app has a single camera
    CameraData m_kCameraDefaults; // Camera's Defaults
    NiTObjectArray<NiEntityInterfaceIPtr> m_kSceneCameras;
    PickController* m_pkPickController;
    
    // Cached time to compute frame-to-frame deltas
    float m_fLastUpdateTime;

    // Interested Properties
    NiFixedString m_kRotationProperty;
    NiFixedString m_kTranslationProperty;
};

#include "CameraController.inl"

#endif // CAMERACONTROLLER_H
