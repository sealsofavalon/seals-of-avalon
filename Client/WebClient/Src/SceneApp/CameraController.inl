// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

//---------------------------------------------------------------------------
inline NiCamera* CameraController::GetCamera() const
{
    NiObject* pkObject;
    if (m_kCamera.m_spCameraEntity && 
        m_kCamera.m_spCameraEntity->GetPropertyData("Scene Root Pointer",
        pkObject))
    {
        return (NiCamera*)pkObject;
    }
    return NULL;
}
//---------------------------------------------------------------------------
inline float CameraController::GetCameraHeading() const
{
    return m_kCamera.m_fHeading;
}
//---------------------------------------------------------------------------
inline float CameraController::GetCameraFrameYaw() const
{
    return m_kCamera.m_fFrameYaw;
}
//---------------------------------------------------------------------------
inline float CameraController::GetCameraFramePitch() const
{
    return m_kCamera.m_fFramePitch;
}
//---------------------------------------------------------------------------
inline float CameraController::GetCameraPitch() const
{
    return m_kCamera.m_fCamPitch;
}
//---------------------------------------------------------------------------
inline void CameraController::SetFly(const bool bFly)
{
    if (bFly)
    {
        m_kCamera.m_eMoveState = CameraData::FLYING;
    }
    else
    {
        if (m_kCamera.m_eMoveState == CameraData::FLYING)
            m_kCamera.m_eMoveState = CameraData::LANDING;
    }
}
//---------------------------------------------------------------------------
inline bool CameraController::GetFly()
{
    return m_kCamera.m_eMoveState == CameraData::FLYING;
}
//---------------------------------------------------------------------------
inline bool CameraController::IsHeightInWalkingRange(
    const float fHeight, 
    const CameraData &kCamera)
{
    float fDiff = fHeight - kCamera.m_kTrans.z;

    bool bResult = fDiff >= -kCamera.m_fMaxCameraDrop &&
        fDiff <= kCamera.m_fMaxCameraRaise;

    return bResult;
}
