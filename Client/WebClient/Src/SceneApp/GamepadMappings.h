// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#ifndef GAMEPADMAPPINGS_H
#define GAMEPADMAPPINGS_H

#include <NiApplication.h>

#define SCENEAPP_LSTICK_VERT    NiInputGamePad::NIGP_DEFAULT_LEFT_VERT
#define SCENEAPP_LSTICK_HORZ    NiInputGamePad::NIGP_DEFAULT_LEFT_HORZ
#define SCENEAPP_RSTICK_VERT    NiInputGamePad::NIGP_DEFAULT_RIGHT_VERT
#define SCENEAPP_RSTICK_HORZ    NiInputGamePad::NIGP_DEFAULT_RIGHT_HORZ
#define SCENEAPP_TOGGLE_PIPELINE_BUTTON     NiInputGamePad::NIGP_L1
#define SCENEAPP_TOGGLE_FULLSCREEN_BUTTON     NiInputGamePad::NIGP_R1
#define SCENEAPP_TOGGLE_CAMERA_BUTTON       NiInputGamePad::NIGP_RDOWN
#define SCENEAPP_TOGGLE_WIREFRAME_BUTTON    NiInputGamePad::NIGP_RUP
#define SCENEAPP_TOGGLE_FLY_BUTTON          NiInputGamePad::NIGP_RRIGHT
#define SCENEAPP_TOGGLE_SHADOWS_BUTTON      NiInputGamePad::NIGP_RLEFT
#define SCENEAPP_TOGGLE_OCCLUDERS_BUTTON    NiInputGamePad::NIGP_A
#define SCENEAPP_QUIT_BUTTON    NiInputGamePad::NIGP_SELECT
#define SCENEAPP_RESET_BUTTON NiInputGamePad::NIGP_START

#endif // GAMEPADMAPPINGS_H
