// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net
#include "stdafx.h"
#include <NiScreenTexture.h>
#include <NiTexturingProperty.h>

#include "NiScreenText.h"
#include <NiApplication.h>

NiTexturePtr NiScreenText::ms_spTextTexture = 0;

#if defined(_XENON)
const char* NiScreenText::ms_pcTextImage = "ASCICON.TGA";
unsigned int NiScreenText::ms_uiCharWidth = 8;
unsigned int NiScreenText::ms_uiCharHeight = 12;
unsigned int NiScreenText::ms_uiCharSpacingX = 9;
unsigned int NiScreenText::ms_uiCharSpacingY = 13;
const unsigned int NiScreenText::ms_uiCharBaseU = 0;
const unsigned int NiScreenText::ms_uiCharBaseV = 0;
#else   //#if defined(_XENON)
const char* NiScreenText::ms_pcTextImage = "ASCITINY.TGA";
unsigned int NiScreenText::ms_uiCharWidth = 5;
unsigned int NiScreenText::ms_uiCharHeight = 8;
unsigned int NiScreenText::ms_uiCharSpacingX = 6;
unsigned int NiScreenText::ms_uiCharSpacingY = 9;
const unsigned int NiScreenText::ms_uiCharBaseU = 0;
const unsigned int NiScreenText::ms_uiCharBaseV = 0;
#endif  //#if defined(_XENON)

const unsigned int NiScreenText::ms_uiASCIIMin = 33;
const unsigned int NiScreenText::ms_uiASCIIMax = 122;
unsigned int NiScreenText::ms_uiASCIICols = 12;
int NiScreenText::ms_iCount = 0;

//---------------------------------------------------------------------------
NiScreenText::NiScreenText(unsigned int uiMaxChars,
    NiTPointerList<NiScreenTexturePtr>* pkScreenTextures,
    const NiColorA& kColor)
{
    Init(uiMaxChars, pkScreenTextures, kColor);
}
//---------------------------------------------------------------------------
NiScreenText::NiScreenText(unsigned int uiMaxChars,
    NiTPointerList<NiScreenTexturePtr>* pkScreenTextures,
    const NiColorA& kColor, const char* pcName, unsigned int uiWidth,
    unsigned int uiHeight, unsigned int uiColumns)
{
    ms_uiCharWidth = ms_uiCharSpacingX = uiWidth;
    ms_uiCharHeight = ms_uiCharSpacingY = uiHeight;
    ms_uiASCIICols = uiColumns;
    ms_pcTextImage = pcName;
    Init(uiMaxChars, pkScreenTextures, kColor);
}
//---------------------------------------------------------------------------
NiScreenText::~NiScreenText()
{
    m_pkScreenTextures->Remove(m_spScreenTexture);

    if (--ms_iCount < 1)
        ms_spTextTexture = 0;

    NiFree(m_pcString);
}
//---------------------------------------------------------------------------
void NiScreenText::Init(unsigned int uiMaxChars,
    NiTPointerList<NiScreenTexturePtr>* pkScreenTextures,
    const NiColorA& kColor)
{
    ms_iCount++;

    if (!ms_spTextTexture)
    {
        ms_spTextTexture = NiSourceTexture::Create(
            NiApplication::ConvertMediaFilename(ms_pcTextImage));
    }

    m_uiMaxChars = uiMaxChars;
    m_kColor = kColor;
    
    m_spScreenTexture = NiNew NiScreenTexture(ms_spTextTexture);
    m_spScreenTexture->SetApplyMode(NiTexturingProperty::APPLY_MODULATE);
    m_pkScreenTextures = pkScreenTextures;

    m_uiTextOriginX = 0;
    m_uiTextOriginY = 0;
    m_bScrollDown = true;

    // String is _not_ NULL terminated
    m_pcString = NiAlloc(char, m_uiMaxChars);
    m_uiNumChars = 0;

    m_uiMaxCols = 40;

    m_uiNumRects = 0;
    m_uiNumCurrentRows = 0;
    m_uiCurrentColumn = 0;

    AppendCharacter('T');
    AppendCharacter('E');
    AppendCharacter('S');
    AppendCharacter('T');
}
//---------------------------------------------------------------------------
void NiScreenText::RecreateText()
{
    m_spScreenTexture->RemoveAllScreenRects();

    m_uiNumCurrentRows = 0;
    m_uiCurrentColumn = 0;

    for (unsigned int i = 0; i < m_uiNumChars; i++)
    {
        char cChar = m_pcString[i];

        // if we are at the end of a line or if the char is '\n' then move
        // to the start of the next line
        if ((m_uiCurrentColumn >= m_uiMaxCols) || (cChar == '\n'))
        {
            m_uiNumCurrentRows++;
            m_uiCurrentColumn = 0;

            if (!m_bScrollDown)
            {
                // Move all characters up one row
                unsigned int uiNumRects = 
                    m_spScreenTexture->GetNumScreenRects();
                for (unsigned int j = 0; j < uiNumRects; j++)
                {
                    NiScreenTexture::ScreenRect& kRect = 
                        m_spScreenTexture->GetScreenRect(j);
                    kRect.m_sPixTop -= ms_uiCharSpacingY;
                }
            }

            // if the extra char is a '\n', skip it
            if (cChar == '\n')
                continue;
        }

        unsigned int uiChar = (unsigned int)cChar;

        // skip whitespace or unprintable character
        if ((uiChar >= ms_uiASCIIMin) && (uiChar <= ms_uiASCIIMax))
        {
            uiChar -= ms_uiASCIIMin;
    
            unsigned short usPixTop = m_uiTextOriginY;
            if (m_bScrollDown)
                usPixTop += (m_uiNumCurrentRows) * ms_uiCharSpacingY;
            unsigned short usPixLeft = m_uiTextOriginX + 
                m_uiCurrentColumn * ms_uiCharSpacingX;

            unsigned short usTexTop = ms_uiCharBaseV + 
                (uiChar / ms_uiASCIICols) * ms_uiCharSpacingY;
            unsigned short usTexLeft = ms_uiCharBaseU + 
                (uiChar % ms_uiASCIICols) * ms_uiCharSpacingX;

            m_spScreenTexture->AddNewScreenRect(usPixTop, usPixLeft, 
                ms_uiCharWidth, ms_uiCharHeight, usTexTop, usTexLeft, 
                m_kColor);
        }

        m_uiCurrentColumn++;
    }
    m_spScreenTexture->MarkAsChanged(NiScreenTexture::EVERYTHING_MASK);
}
//---------------------------------------------------------------------------
void NiScreenText::AppendCharacter(char cChar)
{
    if (m_uiNumChars >= (m_uiMaxChars - 1))
        return;
    
    m_pcString[m_uiNumChars] = cChar;

    m_uiNumChars++;
    m_pcString[m_uiNumChars] = '\0';

    m_spScreenTexture->MarkAsChanged(NiScreenTexture::EVERYTHING_MASK);

    // if we are at the end of a line or if the char is '\n' then move
    // to the start of the next line
    if ((m_uiCurrentColumn >= m_uiMaxCols) || (cChar == '\n'))
    {
        m_uiNumCurrentRows++;
        m_uiCurrentColumn = 0;

        if (!m_bScrollDown)
        {
            // Move all characters up one row
            unsigned int uiNumRects = m_spScreenTexture->GetNumScreenRects();
            for (unsigned int i = 0; i < uiNumRects; i++)
            {
                NiScreenTexture::ScreenRect& kRect = 
                    m_spScreenTexture->GetScreenRect(i);
                kRect.m_sPixTop -= ms_uiCharSpacingY;
            }
        }

        // if the extra char is a '\n', skip it
        if (cChar == '\n')
            return;
    }

    unsigned int uiChar = (unsigned int)cChar;

    // skip whitespace or unprintable character
    if ((uiChar >= ms_uiASCIIMin) && (uiChar <= ms_uiASCIIMax))
    {
        uiChar -= ms_uiASCIIMin;
    
        unsigned short usPixTop = m_uiTextOriginY;
        if (m_bScrollDown)
            usPixTop += (m_uiNumCurrentRows + 1) * ms_uiCharSpacingY;
        unsigned short usPixLeft = m_uiTextOriginX + 
            m_uiCurrentColumn * ms_uiCharSpacingX;

        unsigned short usTexTop = ms_uiCharBaseV + 
            (uiChar / ms_uiASCIICols) * ms_uiCharSpacingY;
        unsigned short usTexLeft = ms_uiCharBaseU + 
            (uiChar % ms_uiASCIICols) * ms_uiCharSpacingX;

        m_spScreenTexture->AddNewScreenRect(usPixTop, usPixLeft, 
            ms_uiCharWidth, ms_uiCharHeight, usTexTop, usTexLeft, m_kColor);
    }

    m_uiCurrentColumn++;
}
//---------------------------------------------------------------------------
void NiScreenText::DeleteLastCharacter()
{
    if (!m_uiNumChars)
        return;

    m_uiNumChars--;
    char cChar = m_pcString[m_uiNumChars];
    m_pcString[m_uiNumChars] = '\0';

    // if we are at the beginning of a line then return to end of the 
    // previous line - easiest way to do this is to recreate the text
    if (m_uiCurrentColumn == 0)
    {
        RecreateText();
    }
    else
    {
        m_spScreenTexture->MarkAsChanged(NiScreenTexture::EVERYTHING_MASK);

        unsigned int uiChar = (unsigned int)cChar;

        // skip whitespace or unprintable character
        if ((uiChar >= ms_uiASCIIMin) && (uiChar <= ms_uiASCIIMax))
        {
            // Remove character
            m_spScreenTexture->RemoveScreenRect(
                m_spScreenTexture->GetNumScreenRects() - 1);
        }
        m_uiCurrentColumn--;
    }
}
//---------------------------------------------------------------------------
void NiScreenText::SetColor(NiColorA& kNewColor)
{
    if (kNewColor == m_kColor)
        return;

    m_kColor = kNewColor;

    // Update all of the Screen Rectangles
    const unsigned int uiQuantity = m_spScreenTexture->GetNumScreenRects();
    for (unsigned int uiLoop = 0; uiLoop < uiQuantity; uiLoop++)
    {
        NiScreenTexture::ScreenRect& kTemp = 
            m_spScreenTexture->GetScreenRect(uiLoop);

        kTemp.m_kColor = m_kColor;
    }

    m_spScreenTexture->MarkAsChanged(NiScreenTexture::COLOR_MASK);
}
//---------------------------------------------------------------------------
void NiScreenText::SetVisible(bool bVisible)
{
    if (bVisible)
        m_pkScreenTextures->AddTail(m_spScreenTexture);
    else
        m_pkScreenTextures->Remove(m_spScreenTexture);
}
//---------------------------------------------------------------------------
