// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#ifndef PICKCONTROLLER_H
#define PICKCONTROLLER_H

#include <NiPick.h>
#include <NiSmartPointer.h>

class NiScene;

NiSmartPointer(NiAVObject);

class PickController : public NiMemObject
{
public:
    PickController();
    ~PickController();

    bool Initialize(NiScene* pkSceneRoot);

    // Given world x, y, return a Z
    bool GetHeight(NiPoint3& kLoc, float& fHeight);

    // Given a starting position and direction, determine the point of 
    // intersection with the terrain mesh in world space
    bool GetIntersection(const NiPoint3& kStart, const NiPoint3& kDir, 
        NiPoint3& kLoc);

    bool HasWalkables();
    void ToggleDisplayWalkables();

protected:
    // Methods to find walkables and non-walkables
    void FindAllWalkables(NiScene* pkSceneRoot,
        NiTObjectArray<NiAVObjectPtr>& kWalkables);
    void FindWalkables(NiTObjectArray<NiAVObjectPtr>& kWalkables,
        NiAVObject* pkObject);

    NiTObjectArray<NiAVObjectPtr> m_kWalkables;
    NiPick m_kGroundPick;
};

#endif // #ifdef PICKCONTROLLER_H
