// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#ifndef SCENEAPP_H
#define SCENEAPP_H

#include <NiApplication.h>
#include <NiTArray.h>
#include <NiEntity.h>
#include <NiScreenTexture.h>
#include <NiTPtrSet.h>
#include <NiSmartPointer.h>
#include <NiFrustum.h>
#include <NiRect.h>

#include <NiCollision.h>
#include <NiAnimation.h>
#include <NiParticle.h>

#include "NiOcclusionCuller.h"
#include "SceneLoadingSink.h"
NiSmartPointer(SceneAppAccum);
class CameraController;
class PickController;
class NiAVObject;
class NiMaterial;
class ShaderHelper;
class NiScreenText;

class SceneApp : public NiApplication, public IGame
{
	friend class SceneLoadingSink;
	friend class WStreamResource;
public:
    SceneApp();
    ~SceneApp();

    // NiApplication overrides
    virtual void Terminate(); 
    virtual bool Initialize();
    virtual bool CreateScene();
	virtual bool CreateRenderer();
    virtual bool CreateFrame();
    virtual void UpdateFrame();
    virtual void ProcessInput();
//    virtual void CullFrame();
//    virtual void RenderFrame();
	 virtual NiInputSystem::CreateParams* GetInputSystemCreateParams();
	 virtual bool CreateInputSystem();
protected:
    void CreateSplashScreen();
    bool CreateShaders();
    bool CreateCameraController();
    bool ProcessCommandLine();

    // Methods for applying an NiMaterial to all geometry
    // (except for NiParticleSystem and NiLines)
    void ApplyMaterialToAllGeometry(
        NiMaterial* pkLegacyPipelineMaterial);
    void ApplyMaterial(NiAVObject* pkObject,
        NiMaterial* pkLegacyPipelineMaterial);

    NiScreenElements* CreateScreenElements(const char* pcFilename);
   
    void PrepareScene(NiScene* pkScene);
    void RecursivePrecacheEntity(NiEntityInterface* pkEntity);
    void RecursivePrecacheScene(NiAVObject* pkObject);
    void RecursiveApplyMaterial(NiEntityInterface* pkEntity,
        NiMaterial* pkLegacyPipelineMaterial);
    void RecursiveToggleWireframe(NiEntityInterface* pkEntity);

    // Methods for determining and setting fullscreen/splitscreen modes
    bool GetUseSplitScreen();
    void SetUseSplitScreen(bool bUseSplitScreen);

    void RenderSplashScreen(const char* pcMsg = NULL);

    // Frame rendering system callbacks.
    static bool RenderFramePre(NiRenderFrame* pkCurrentFrame,
        void* pvCallbackData);
    static bool LeftClickPre(NiRenderClick* pkCurrentRenderClick,
        void* pvCallbackData);
    static bool RightClickPre(NiRenderClick* pkCurrentRenderClick,
        void* pvCallbackData);
    static bool RightClickPost(NiRenderClick* pkCurrentRenderClick,
        void* pvCallbackData);
    
    void AdvanceCamera();

    // For the splash screen
    NiScreenElementsPtr  m_spScreenQuad;
    NiScreenText* m_pkText;

    // Error reporting handler
    NiDefaultErrorHandlerPtr m_spError;
    
    // Asset manager
    NiExternalAssetManagerPtr m_spAssetManager;

    // The first scene and the only one this app honors
    NiScenePtr m_spEntityScene;

    // The array of entities that should be updated.
    NiTPrimitiveSet<NiEntityInterface*> m_kEntitiesToUpdate;
  
    // Pointer to the camera controllers
    CameraController* m_pkCameraController;
    NiTPrimitiveArray<NiCamera*> m_kAllCameras;
    unsigned int m_uiCurrentCamera;
    unsigned int m_uiWalkableCamera;

    // File name to load (passed in by command line)
    char m_acFilename[NI_MAX_PATH];

    // Shader file paths (passed in by command line)
    NiTPrimitiveArray<char*> m_kShaderPaths;
    NiTPrimitiveArray<char*> m_kShaderProgramPaths;

    // The apps special accumulator to allow toggling between
    // shader systems.
    SceneAppAccumPtr m_spAccum;

    // Shows off old and new system
    bool m_bUseSplitScreen;

    // Using visual objects
    bool m_bVisualTrackersCreated;

    // Force Shadows off
    bool m_bForceShadowsOff;

    // Art for showing fixed-function pipeline and new system
    NiScreenElementsPtr m_spNewPipelineScreenElements;
    NiScreenElementsPtr m_spOldPipelineScreenElements;
    NiScreenElementsPtr m_spLogo;

    // pointer to a shader helper.
    ShaderHelper* m_pkShaderHelper;

//    NiOcclusionCuller m_kOcclusionCuller;

    NiFixedString m_kSceneRootPointerName;

    // Used for split screen
    NiFrustum m_kOrigFrustum;
    NiFrustum m_kLeftFrustum;
    NiFrustum m_kRightFrustum;
    NiRect<float> m_kOrigPort;
    NiRect<float> m_kLeftPort;
    NiRect<float> m_kRightPort;
    NiSceneRenderView* m_pkSceneView;
    NiSceneRenderView* m_pkDuplicateView;
    NiViewRenderClickPtr m_spFullClick;
    NiViewRenderClickPtr m_spLeftClick;
    NiViewRenderClickPtr m_spRightClick;
public:
	void SetLoading(BOOL Loading) {m_Loading = Loading;}
	BOOL PostLoadScene();
	void ReCacheScene();
	virtual void Lock() {m_MainThreadCS.Lock();}
	virtual void Unlock() {m_MainThreadCS.Unlock();}
private:
	// IGame ʵ��
	virtual BOOL InitializeForCtrl(HWND hCtrlWnd,ILWStreamSystem* iStreamSys);
	virtual void Destroy();
	virtual void Update();

	virtual BOOL LoadScene(const char* RefPath,const char* SceneFile, void* Reserve);
	virtual ISceneLoadingSink* GetSceneLoadingSink();
	virtual BOOL IsLoadingScene() { return m_Loading;}
	
	NiCriticalSection m_MainThreadCS;
	SceneLoadingSink m_LoadingSink;
	ILWStreamSystem* m_StreamSystem;
	BOOL m_bWebMode;
	BOOL m_Loading;
};

#endif // SCENEAPP_H
