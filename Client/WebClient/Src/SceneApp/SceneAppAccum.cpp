// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net
#include "stdafx.h"
#include "SceneAppAccum.h"
#include <NiParticleSystem.h>

//---------------------------------------------------------------------------
SceneAppAccum::SceneAppAccum(NiMaterial* pkLegacyPipelineMaterial,
    NiMaterial* pkCurrentPipelineMaterial) : m_bUseNewSystem(true),
    m_bUseSplitScreen(false),
    m_spLegacyPipelineMaterial(pkLegacyPipelineMaterial),
    m_spCurrentPipelineMaterial(pkCurrentPipelineMaterial),
    m_kOpaqueAccum()
{
}
//---------------------------------------------------------------------------
void SceneAppAccum::RegisterObjectArray(NiVisibleArray& kArray)
{
    const unsigned int uiQuantity = kArray.GetCount();
    for (unsigned int i = 0; i < uiQuantity; i++)
    {
        NiGeometry& kObject = kArray.GetAt(i);

        // Custom materials are not displayed in split-screen mode in
        // SceneApp, since split screen mode (unfortunately) assumes a
        // NiStandardMaterial vs. legacy-pipeline comparison.
        if (GetUseSplitScreen())
        {
            if (!NiIsKindOf(NiParticleSystem, &kObject))
            {
                if (kObject.IsMaterialApplied(m_spCurrentPipelineMaterial) || 
                    kObject.IsMaterialApplied(m_spLegacyPipelineMaterial))
                {
                    if (GetUseNewSystem())
                        kObject.SetActiveMaterial(m_spCurrentPipelineMaterial);
                    else
                        kObject.SetActiveMaterial(m_spLegacyPipelineMaterial);
                }
            }
        }

        const NiPropertyState* pkState = kObject.GetPropertyState();

        NIASSERT(pkState);
        const NiAlphaProperty *pkAlpha = pkState->GetAlpha();
        // Every property state should have a valid alpha property
        NIASSERT(pkAlpha);

        if (pkAlpha->GetAlphaBlending() && 
            !(m_bObserveNoSortHint && pkAlpha->GetNoSorter()) && 
            kObject.GetSortObject())
        {
            m_kItems.AddTail(&kObject);
        }
        else
        {
            //kObject.RenderImmediate(NiRenderer::GetRenderer());
            m_kOpaqueAccum.RegisterObject(&kObject);
        }
    }
}
//---------------------------------------------------------------------------
void SceneAppAccum::StartAccumulating(const NiCamera* pkCamera)
{
    m_kOpaqueAccum.StartAccumulating(pkCamera);
    NiAlphaAccumulator::StartAccumulating(pkCamera);
}
//---------------------------------------------------------------------------
void SceneAppAccum::FinishAccumulating()
{
    m_kOpaqueAccum.FinishAccumulating();
    NiAlphaAccumulator::FinishAccumulating();
}
//---------------------------------------------------------------------------
bool SceneAppAccum::GetUseNewSystem()
{
    return m_bUseNewSystem;
}
//---------------------------------------------------------------------------
void SceneAppAccum::SetUseNewSystem(bool bValue)
{
    m_bUseNewSystem = bValue;
}
//---------------------------------------------------------------------------
bool SceneAppAccum::GetUseSplitScreen()
{
    return m_bUseSplitScreen;
}
//---------------------------------------------------------------------------
void SceneAppAccum::SetUseSplitScreen(bool bUseSplitScreen)
{
    if (bUseSplitScreen == m_bUseSplitScreen)
        return;

    m_bUseSplitScreen = bUseSplitScreen;
}
//---------------------------------------------------------------------------
