// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#ifndef SCENEAPPACCUM_H
#define SCENEAPPACCUM_H

#include <NiAlphaAccumulator.h>
#include "NiFrontToBackAccumulator.h"
#include <NiMaterial.h>

class SceneAppAccum : public NiAlphaAccumulator
{
public:
    SceneAppAccum(NiMaterial* pkLegacyPipelineMaterial,
        NiMaterial* pkCurrentPipelineMaterial);

    virtual void RegisterObjectArray(NiVisibleArray& kArray); 

    virtual void StartAccumulating(const NiCamera* pkCamera);
    virtual void FinishAccumulating();

    bool GetUseNewSystem();
    void SetUseNewSystem(bool bValue);

    bool GetUseSplitScreen();
    void SetUseSplitScreen(bool bUseSplitScreen);

protected:
    bool m_bUseNewSystem;
    bool m_bUseSplitScreen;

    NiMaterialPtr m_spLegacyPipelineMaterial;
    NiMaterialPtr m_spCurrentPipelineMaterial;
    NiFrontToBackAccumulator m_kOpaqueAccum;
};

#endif // SCENEAPPACCUM_H
