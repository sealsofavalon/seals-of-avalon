#include "StdAfx.h"
#include "SceneLoadingSink.h"
#include "SceneApp.h"
SceneLoadingSink::SceneLoadingSink(void):m_Render(NULL)
{
}

SceneLoadingSink::~SceneLoadingSink(void)
{
	Destroy();
}

void SceneLoadingSink::OnLoadingNotify(const char* NotifyMsg, float Ratio)
{
}

void SceneLoadingSink::OnRender()
{
	m_Render->BeginFrame();
	m_Render->BeginUsingDefaultRenderTargetGroup(NiRenderer::CLEAR_ALL);
	m_Render->SetScreenSpaceCameraData();
	m_OverlayQuad->Draw(m_Render);
	const NiTPointerList<NiScreenTexturePtr>& kScreenTextures = g_Game->GetScreenTextures();
	NiTListIterator kIter = kScreenTextures.GetHeadPos();
	while (kIter)
	{
		NiScreenTexture* pkTexture = kScreenTextures.GetNext(kIter);
		NIASSERT(pkTexture);
		pkTexture->Draw(m_Render);
	}
	m_Render->EndUsingRenderTargetGroup();
	m_Render->EndFrame();
	m_Render->DisplayFrame();
}

BOOL SceneLoadingSink::Create(SceneApp* Game)
{
	m_Render = NiRenderer::GetRenderer();
	NIASSERT(m_Render);
	m_OverlayQuad = NiNew NiScreenElements(NiNew NiScreenElementsData(false, false, 1));
    int iPolygon = m_OverlayQuad->Insert(4);
    m_OverlayQuad->SetRectangle(iPolygon, 0.0f, 0.0f, 1.0f, 1.0f);
    m_OverlayQuad->UpdateBound();
    m_OverlayQuad->SetTextures(iPolygon, 0, 0.0f, 0.0f, 1.0f, 1.0f);
	
	 NiTexturingProperty* pkTexProp = NiNew NiTexturingProperty();
    pkTexProp->SetApplyMode(NiTexturingProperty::APPLY_REPLACE);
    pkTexProp->SetBaseMap(NiNew NiTexturingProperty::Map());
    pkTexProp->SetBaseClampMode(NiTexturingProperty::CLAMP_S_CLAMP_T);
    m_OverlayQuad->AttachProperty(pkTexProp);

    // Create and add z-buffer property.
    NiZBufferProperty* pkZBufferProp = NiNew NiZBufferProperty();
    pkZBufferProp->SetZBufferTest(false);
    pkZBufferProp->SetZBufferWrite(false);
    m_OverlayQuad->AttachProperty(pkZBufferProp);

    // Perform initial update.
    m_OverlayQuad->Update(0.0f);
    m_OverlayQuad->UpdateNodeBound();
    m_OverlayQuad->UpdateProperties();
    m_OverlayQuad->UpdateEffects();
	
    // Load splash screen texture.
    NiTexturePtr spSplashScreenTex = NiSourceTexture::Create("resource/background.tga");
    NIASSERT(spSplashScreenTex);
    pkTexProp->SetBaseTexture(spSplashScreenTex);

   // unsigned int uiX, uiY;
   // m_spRenderer->GetOnScreenCoord(0.0f, 0.0f, 0, 0, uiX, uiY, NiRenderer::CORNER_TOP_LEFT);
   // m_pkText->SetTextOrigin(uiX, uiY);
   // m_pkText->SetVisible(true);

	//Game->GetScreenElements().AddTail(m_OverlayQuad);
	return TRUE;
}

void SceneLoadingSink::Destroy()
{
	m_OverlayQuad = NULL;
	m_Render = NULL;
}