#pragma once

class SceneLoadingSink : public ISceneLoadingSink
{
public:
	SceneLoadingSink(void);
	virtual ~SceneLoadingSink(void);
	BOOL Create(SceneApp* Game);
	void Destroy();
private:
	// ʵ��
	virtual void OnLoadingNotify(const char* NotifyMsg, float Ratio);
	virtual void OnRender();
private:
	NiRenderer* m_Render;
	NiScreenElementsPtr m_OverlayQuad;		// ����
};
