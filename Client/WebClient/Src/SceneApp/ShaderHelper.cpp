// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net
//---------------------------------------------------------------------------
#include "stdafx.h"
// Precompiled Header
//#include "SceneAppPCH.h"

#if defined WIN32
    #include <NiDirectXVersion.h>
#endif

#include "ShaderHelper.h"
#include <NiApplication.h>

#define PARSE_GAMEBRYO_NSF

#if defined(_PS3)
    #define _LIB
    #include <NSBShaderLib.h>
    #include <NSFParserLib.h>
    #include <NiShader.h>
    #include <NiShaderFactory.h>
    #include <NiShaderLibrary.h>
    #include <NiShaderLibraryDesc.h>
    #include <NiPS3ShaderProgramFactory.h>
#endif

// DLL shader libraries not supported
#if defined(WIN32) || defined(_XENON)
    #define _LIB
        #include <NSBShaderLib.h>
        #include <NSFParserLib.h>
        #include <NiD3DXEffectShaderLib.h>
        #include <NiD3DShaderProgramFactory.h>
    #if defined(_XENON)
        #include <NiFXLShaderLib.h>
    #else
        #include <NiD3D10Renderer.h>
        #include <NiD3D10ShaderProgramFactory.h>
    #endif
    #undef _LIB
#endif

#if defined(_XENON)
    #pragma comment(lib, "NiBinaryShaderLib.lib")
    #pragma comment(lib, "NSBShaderLib.lib")
    #pragma comment(lib, "NSFParserLib.lib")
    #pragma comment(lib, "NiD3DXEffectShaderLib.lib")
    #pragma comment(lib, "NiFXLShaderLib.lib")
#elif defined(WIN32)
    #pragma comment(lib, "NiBinaryShaderLibDX9.lib")
    #pragma comment(lib, "NiD3D10BinaryShaderLibD3D10.lib")
    #pragma comment(lib, "NSBShaderLibDX9.lib")
    #pragma comment(lib, "NSFParserLibDX9.lib")
    #pragma comment(lib, "NiD3DXEffectShaderLibDX9.lib")
#endif

//---------------------------------------------------------------------------
bool ShaderHelper::SetupShaderSystem(
    char* apcProgramDirectory[], unsigned int uiNumProgramDirectories, 
    char* apcShaderDirectories[], unsigned int uiNumShaderDirectories)
{
#if defined(WIN32)
    NIASSERT(NiIsKindOf(NiDX9Renderer, NiRenderer::GetRenderer()) == NULL ||
        NiD3DShaderProgramFactory::GetInstance());
    NIASSERT(NiIsKindOf(NiD3D10Renderer, NiRenderer::GetRenderer()) == NULL ||
        NiD3D10ShaderProgramFactory::GetInstance());
#elif defined(_XENON)
    NIASSERT(NiD3DShaderProgramFactory::GetInstance());
#elif defined(_PS3)
    NIASSERT(NiPS3ShaderProgramFactory::GetInstance());
#endif

    for (unsigned int i = 0; i < uiNumProgramDirectories; i++)
    {
        NiShaderFactory::AddShaderProgramFileDirectory(
            apcProgramDirectory[i]);
    }

    m_uiShaderDirectoryCount = uiNumShaderDirectories;
    m_ppcShaderDirectories = apcShaderDirectories;

#if defined(PARSE_GAMEBRYO_NSF)
    // First, we will run the NSF parser. This is done to make sure that
    // any text-based shader files that have been modified are re-compiled
    // to binary before loading all the binary representations.
    if (!RunShaderParsers())
    {
        NiMessageBox("Failed to run shader parsers!", "ERROR");
        return false;
    }
#endif

    if (!RegisterShaderLibraries())
    {
        NiMessageBox("Failed to register shader libraries!", "ERROR");
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
bool ShaderHelper::RunShaderParsers()
{
    NiShaderFactory::RegisterRunParserCallback(NSFRunParser);

    for (unsigned int i = 0; i < m_uiShaderDirectoryCount; i++)
    {
        unsigned int uiCount = NiShaderFactory::LoadAndRunParserLibrary(
            NULL, m_ppcShaderDirectories[i], true);

        if (uiCount == 0)
        {
            NiOutputDebugString("NSF parser library failed to parse "
                "any shaders!");
        }
    }

    return true;
}
//---------------------------------------------------------------------------
bool ShaderHelper::RegisterShaderLibraries()
{
#if defined(PARSE_GAMEBRYO_NSF)
    NiShaderFactory::RegisterClassCreationCallback(NSBLibraryClassCreate);

    if (!NiShaderFactory::LoadAndRegisterShaderLibrary(NULL, 
        m_uiShaderDirectoryCount, m_ppcShaderDirectories, true))
    {
        NiOutputDebugString("NSB shader library failed to load any shaders!");
    }
#endif

#if defined(WIN32) || defined(_XENON)
    NiShaderFactory::RegisterClassCreationCallback(FXLibraryClassCreate);

    if (!NiShaderFactory::LoadAndRegisterShaderLibrary(NULL, 
        m_uiShaderDirectoryCount, m_ppcShaderDirectories, true))
    {
        NiOutputDebugString("FX shader library failed to load any shaders!");
    }
#endif

#if defined(_XENON)
    NiShaderFactory::RegisterClassCreationCallback(FXLiteLibraryClassCreate);

    if (!NiShaderFactory::LoadAndRegisterShaderLibrary(NULL, 
        m_uiShaderDirectoryCount, m_ppcShaderDirectories, true))
    {
        NiOutputDebugString("FXL shader library failed to load any shaders!");
    }
#endif

    return true;
}
//---------------------------------------------------------------------------
bool ShaderHelper::NSBLibraryClassCreate(const char* pcLibFile, 
    NiRenderer* pkRenderer, int iDirectoryCount, char* apcDirectories[], 
    bool bRecurseSubFolders, NiShaderLibrary** ppkLibrary)
{
    *ppkLibrary = NULL;

    // Create the NSB Shader Library...
    return NSBShaderLib_LoadShaderLibrary(pkRenderer, iDirectoryCount, 
        apcDirectories, bRecurseSubFolders, ppkLibrary);
}
//---------------------------------------------------------------------------
bool ShaderHelper::FXLibraryClassCreate(const char* pcLibFile, 
    NiRenderer* pkRenderer, int iDirectoryCount, char* apcDirectories[], 
    bool bRecurseSubFolders, NiShaderLibrary** ppkLibrary)
{
    *ppkLibrary = NULL;

#if defined(WIN32) || defined(_XENON)
    // Create the FX Shader Library.
    return NiD3DXEffectShaderLib_LoadShaderLibrary(pkRenderer, 
        iDirectoryCount, apcDirectories, bRecurseSubFolders, ppkLibrary);
#else
    return false;
#endif
}
//---------------------------------------------------------------------------
bool ShaderHelper::FXLiteLibraryClassCreate(const char* pcLibFile, 
    NiRenderer* pkRenderer, int iDirectoryCount, char* apcDirectories[], 
    bool bRecurseSubFolders, NiShaderLibrary** ppkLibrary)
{
    *ppkLibrary = NULL;

#if defined(_XENON)
    // Create the FX Shader Library.
    return NiFXLShaderLib_LoadShaderLibrary(pkRenderer, 
        iDirectoryCount, apcDirectories, bRecurseSubFolders, ppkLibrary);
#else
    return false;
#endif
}
//---------------------------------------------------------------------------
unsigned int ShaderHelper::NSFRunParser(const char* pcLibFile, 
    NiRenderer* pkRenderer, const char* pcDirectory, 
    bool bRecurseSubFolders)
{
    return NSFParserLib_RunShaderParser(pcDirectory, bRecurseSubFolders);
}
//---------------------------------------------------------------------------
