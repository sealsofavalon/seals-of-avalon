#pragma once
// Web stream resource.
struct ILWStreamSystem;

class WStreamResource
{
	class _NiSourceTexture : public NiSourceTexture
	{
		friend class WStreamResource;
	};
	typedef NiTexture::RendererData* HRenderData;	// 实质上是一个D3DTEXTURE的包装对象.
public:
	static void HookNDLCreator();
	static void SetStreamSystem(ILWStreamSystem* StreamSystem);
	static BOOL Initialize(ILWStreamSystem* StreamSystem);
	static void Destroy();
	static void ReviseURL(char* WebPath);
	static void LoadScene(const char* Path, void* pScene);
	static void LoadNif(const char* Path, void* UserData);
private:
	static void Lock();
	static void Unlock();
	static int __cdecl StreamTextureRequest(NiSourceTexture* pThis, void* UserData);
	
	static ILWResource* TextureLoadedCB(String* RequestURL, String* CacheFileName, void* UserData);
	static void TextureReferenceCB(String* RequestUrl, String* CacheFileName, ILWResource* RefResource, void* userData);
	static void TextureLoadingProgressCallback(float Ratio, void *UserData);

	static ILWResource* SceneLoadedCB(String* RequestURL, String* CacheFileName, void* UserData);
	static void SceneReferenceCB(String* RequestUrl, String* CacheFileName, ILWResource* RefResource, void* userData);
	static void SceneLoadingProgressCallback(float Ratio, void *UserData);

	static ILWResource* NifLoadedCB(String* RequestURL, String* CacheFileName, void* UserData);
	static void NifReferenceCB(String* RequestUrl, String* CacheFileName, ILWResource* RefResource, void* userData);
	static void NifLoadingProgressCallback(float Ratio, void *UserData);
private:
	static NiTexturePtr sm_LoadingTex;
	static NiTexturePtr sm_ErrorTex;
	static ILWStreamSystem* sm_StreamSystem; 
	static const ResourceCallback m_TextureCB;
	static const ResourceCallback m_SceneCB;
	static const ResourceCallback m_NifCB;
	typedef std::list<NiSourceTexture*> LoadingTextureList;	// 用来正确释放引用Loading 纹理的纹理.
	static LoadingTextureList m_LoadingTexList;
};
