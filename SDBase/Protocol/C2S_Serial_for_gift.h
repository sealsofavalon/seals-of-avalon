#pragma once
#include "C2S.h"
namespace MSG_C2S
{
	struct stSerialCardForGift : public PakHead
	{
		stSerialCardForGift(){wProNO = SERIAL_CARD_FOR_GIFT;}
		std::string serial ;
		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << serial ; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> serial ; return cpu; }
	};
}