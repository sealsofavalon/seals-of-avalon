#pragma once

#include "PacketDef.h"
namespace MSG_GSP2GS
{
	enum
	{
		MSG_RETURN_2_MOTHER,
		MSG_JUMP_REQ,
	};

	struct stReturn2Mother : public PakHead
	{
		stReturn2Mother() { wProNO = MSG_RETURN_2_MOTHER; }
		uint32 transid;

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << transid; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> transid; return cpu; }
	};

	struct stJumpReq : public PakHead
	{
		stJumpReq() { wProNO = MSG_JUMP_REQ; }
		
		uint32 acct;
		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << acct; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> acct; return cpu; }
	};
}

namespace MSG_GS2GSP
{
	enum
	{
		MSG_GS_INIT,
		MSG_QUEUE_INSTANCE_REQ,
		MSG_LEAVE_QUEUE_INSTANCE_REQ,
		MSG_JUMP_ACK,
	};

	struct stGSInit : public PakHead
	{
		stGSInit() { wProNO = MSG_GS_INIT; }
		uint32 groupid;

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << groupid; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> groupid; return cpu; }
	};

	struct stQueueInstanceReq : public PakHead
	{
		stQueueInstanceReq() { wProNO = MSG_QUEUE_INSTANCE_REQ; }

		uint32 mapid;
		struct _group
		{
			uint32 guid;
			uint32 acct;
			uint8 race;
			CPacketSn& Sn(CPacketSn& cps) const {	cps << guid << acct << race; return cps; }
			CPacketUsn& Usn(CPacketUsn& cpu)	{	cpu >> guid >> acct >> race; return cpu; }
		};
		std::vector<_group> v;

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << mapid << v; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> mapid >> v; return cpu; }
	};

	struct stLeaveQueueInstanceReq : public PakHead
	{
		stLeaveQueueInstanceReq() { wProNO = MSG_LEAVE_QUEUE_INSTANCE_REQ; }
		uint32 guid;

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << guid; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> guid; return cpu; }
	};

	struct stJumpAck : public PakHead
	{
		stJumpAck() { wProNO = MSG_JUMP_ACK; }
		uint32 acct;
		uint32 transid;
		uint8 gateid;
		std::string gmflags;
		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << acct << transid << gateid << gmflags; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> acct >> transid >> gateid >> gmflags; return cpu; }
	};
}
