#pragma once
#include "S2C.h"
namespace MSG_S2C
{
	struct stSerial_for_gift_use : public PakHead
	{
		stSerial_for_gift_use() { wProNO = MSG_SERIALCARDFORGIFT_USE; }

		CPacketSn& Sn(CPacketSn& cps) const { PakHead::Sn(cps); return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)    { PakHead::Usn(cpu); return cpu; }
	};

	struct stSerial_for_gift_Result : public PakHead
	{
		stSerial_for_gift_Result() { wProNO = MSG_SERIALCARDFORGIFT_USE_RESULT; }
		enum{RESULT_ACCEPT, RESULT_NOT_FOUND, RESULT_IS_USER, RESULT_ERROR};
		ui8 result;
		CPacketSn& Sn(CPacketSn& cps) const { PakHead::Sn(cps); cps << result;return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)    { PakHead::Usn(cpu); cpu >> result;return cpu; }
	};
}