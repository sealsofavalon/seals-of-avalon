#include "stdafx.h"
#include "WebInterface.h"
CWebInterface gWebInterface;
void CWebInterface::Init()
{
	MyLog::log->notice( "Initialized Web Server ..." );
	ctx = mg_start();
	mg_set_option(ctx, "ssl_cert", "ssl_cert.pem");
	mg_set_option(ctx, "aliases", ALIAS_URI "=" ALIAS_DIR);
	mg_set_option(ctx, "ports", "8080,8081s");
	//mg_set_uri_callback(ctx, "/manage", &show_index, (void *) &data);
	//mg_set_auth_callback(ctx, "/*", &on_authorize, NULL);
	mg_set_uri_callback(ctx, "/account", &on_account_main, NULL);
}

void CWebInterface::Update()
{
	m_cb_mgr.poll();
}

void CWebInterface::Release()
{
	mg_stop(ctx);
}

static void
on_account_main(struct mg_connection *conn,
		   const struct mg_request_info *ri, void *data)
{
	char		*action,*account_name,*type;

	action = mg_get_var(conn, "action");
	account_name = mg_get_var(conn, "user");
	type = mg_get_var(conn, "type");

	if (!strcmp(action, "get"))
	{
		if(!strcmp(type, "info"))
		{

		}
		else if(!strcmp(type, "status"))
		{

		}
		else if(!strcmp(type, "password"))
		{

		}
		else if(!strcmp(type, "rolelist"))
		{

		}
		else if(!strcmp(type, "type"))
		{

		}
	}
	else if(!strcmp(action, "block"))
	{

	}
	else if(!strcmp(action, "deblock"))
	{

	}

	if (action != NULL)
		mg_free(action);
	if (account_name != NULL)
		mg_free(account_name);
	if (type != NULL)
		mg_free(type);
}

static void
on_authorize(struct mg_connection *conn,
		  const struct mg_request_info *ri, void *data)
{
	const char	*cookie, *domain;

	cookie = mg_get_header(conn, "Cookie");

	if (!strcmp(ri->uri, "/login")) {
		/* Always authorize accesses to the login page */
		mg_authorize(conn);
	} else if (cookie != NULL && strstr(cookie, "allow=yes") != NULL) {
		/* Valid cookie is present, authorize */
		mg_authorize(conn);
	} else {
		/* Not authorized. Redirect to the login page */
		mg_printf(conn, "HTTP/1.1 301 Moved Permanently\r\n"
			"Set-Cookie: uri=%s;\r\n"
			"Location: /login\r\n\r\n", ri->uri);
	}
}
