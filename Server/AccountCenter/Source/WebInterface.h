#ifndef _WEBINTERFACE_HEAD
#define _WEBINTERFACE_HEAD

#include "mongoose/mongoose.h"
#define ALIAS_URI "/my_etc"
#define ALIAS_DIR "/etc/"
#define DECLARE_ONREQ(x) static void onx(struct mg_connection *conn,const struct mg_request_info *ri,void *data);

static void on_account_main(struct mg_connection *conn,const struct mg_request_info *request_info,void *user_data);

//角色列表查询
static void on_account_info(struct mg_connection* conn, std::string acct_name);
//角色信息查询
static void on_account_status(struct mg_connection* conn, std::string acct_name);
//踢人
static void on_account_block(struct mg_connection* conn, std::string reason, std::string time_from, std::string time_to);
//禁言
static void on_account_deblock(struct mg_connection* conn, std::string acct_name);
//取消禁言
static void on_account_pw(struct mg_connection* conn, std::string acct_name, std::string newpass);

class CWebInterface
{
	struct mg_context	*ctx;
	int			data;
	call_back_mgr m_cb_mgr;
public:
	void Init();
	void Update();
	void Release();
	call_back_mgr* GetCallBackMgr(){ return &m_cb_mgr; }

	void account_info(struct mg_connection* conn, std::string acct_name);
	void account_status(struct mg_connection* conn, std::string acct_name);
	void account_block(struct mg_connection* conn, std::string reason, std::string time_from, std::string time_to);
	void account_deblock(struct mg_connection* conn, std::string acct_name);
	void account_pw(struct mg_connection* conn, std::string acct_name, std::string newpass);
};
extern CWebInterface gWebInterface;

#endif
