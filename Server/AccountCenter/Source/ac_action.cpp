#include "stdafx.h"
#include "ac_action.h"
#include "account.h"
#include "account_storage.h"
#include "ac_session.h"
#include "ac_srv.h"
#include "card_generator.h"

ac_action::ac_action() : session( NULL ), serial( 0xFFFFFFFF ), threadid( NORMAL_THREAD_INDEX ), acc( NULL )
{
}

void auth_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stAuthAck ack;
		
		if( acc->auth( session, md5pwd ) )
		{
			ack.ret = MSG_AC2CLIENT::stAuthAck::RET_SUCCESS;
			ack.acc = acc->get_info();
		}
		else
			ack.ret = MSG_AC2CLIENT::stAuthAck::RET_WRONG_PWD;
		ack.nTransID = serial;
		session->send_packet( ack );
	}
}

void auth_no_account_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stAuthAck ack;
		ack.ret = MSG_AC2CLIENT::stAuthAck::RET_WRONG_ACC;
		ack.nTransID = serial;
		session->send_packet( ack );

		g_ac_srv->log_event( session, "auth failed! account:[%s] does not exist", account_str.c_str() );
	}
}

void mod_pwd_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stModPwdAck ack;
		if( acc->mod_pwd( session, oldmd5pwd, newmd5pwd ) )
			ack.ret = MSG_AC2CLIENT::stModPwdAck::RET_SUCCESS;
		else
			ack.ret = MSG_AC2CLIENT::stModPwdAck::RET_WRONG_PWD;
		ack.nTransID = serial;
		ack.id = acc->get_id();
		session->send_packet( ack );
	}
}

void mod_pwd_no_account_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stModPwdAck ack;
		ack.id = 0;
		ack.ret = MSG_AC2CLIENT::stModPwdAck::RET_WRONG_ACC;
		ack.nTransID = serial;
		session->send_packet( ack );

		g_ac_srv->log_event( session, "modify password failed! account:[%s] does not exist", account_str.c_str() );
	}
}

void mod_information_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stModInformationAck ack;
		if( acc->mod_information( session, info, answer1, answer2 ) )
			ack.ret = MSG_AC2CLIENT::stModInformationAck::RET_SUCCESS;
		else
			ack.ret = MSG_AC2CLIENT::stModInformationAck::RET_DB_ERROR;
		ack.nTransID = serial;
		session->send_packet( ack );
	}
}

void mod_pwd_by_qa_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stModPwdByQaAck ack;
		if( acc->mod_pwd_by_qa( session, question, answer, newpwd ) )
			ack.ret = MSG_AC2CLIENT::stModPwdByQaAck::RET_SUCCESS;
		else
			ack.ret = MSG_AC2CLIENT::stModPwdByQaAck::RET_WRONG_ANSWER;
		ack.nTransID = serial;
		session->send_packet( ack );
	}
}

void safe_lock_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stSafeLockAck ack;
		if( acc->safe_lock( session, password, is_lock ) )
			ack.ret = MSG_AC2CLIENT::stSafeLockAck::RET_SUCCESS;
		else
			ack.ret = MSG_AC2CLIENT::stSafeLockAck::RET_DB_ERROR;
		ack.nTransID = serial;
		session->send_packet( ack );
	}
}

void create_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stCreateAccAck ack;
		ack.ret = MSG_AC2CLIENT::stCreateAccAck::RET_DB_ERROR;
		/*
		if( info.id_card.length() )
		{
			if( g_ac_storage->is_id_card_exist( info.id_card ) )
				ack.ret = MSG_AC2CLIENT::stCreateAccAck::RET_ID_CARD_ALREADY_EXIST;
			else
			{
				smart_ptr<QueryResult> qr = g_ac_srv->get_main_db()->Query( "select id from account where id_card = '%s'", info.id_card.c_str() );
				if( qr )
				{
					g_ac_storage->add_id_card( info.id_card );
					ack.ret = MSG_AC2CLIENT::stCreateAccAck::RET_ID_CARD_ALREADY_EXIST;
				}
			}
		}

		if( ack.ret != MSG_AC2CLIENT::stCreateAccAck::RET_ID_CARD_ALREADY_EXIST )
		*/
		{
			ack.id = g_ac_storage->create_new_account( session, info, md5pwd, answer1, answer2 );
			if( ack.id )
				ack.ret = MSG_AC2CLIENT::stCreateAccAck::RET_SUCCESS;
		}
		ack.nTransID = serial;
		session->send_packet( ack );
	}
}

void create_already_exist_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stCreateAccAck ack;
		ack.ret = MSG_AC2CLIENT::stCreateAccAck::RET_ALREADY_EXIST;
		ack.nTransID = serial;
		session->send_packet( ack );

		g_ac_srv->log_event( session, "create account failed! account:[%s] already exists", account_str.c_str() );
	}
}

void query_account_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stQueryAccAck ack;
		ack.ret = MSG_AC2CLIENT::stQueryAccAck::RET_SUCCESS;
		ack.nTransID = serial;
		ack.acc = acc->get_info();
		session->send_packet( ack );
	}
}

void query_no_account_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stQueryAccAck ack;
		ack.ret = MSG_AC2CLIENT::stQueryAccAck::RET_WRONG_ACC;
		ack.nTransID = serial;
		session->send_packet( ack );

		g_ac_srv->log_event( session, "query account failed! account:[%s] does not exist", account_str.c_str() );
	}
}

void fetch_pwd_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stFetchPwdAck ack;
		if( acc->fetch_pwd( session, question, answer, ack.newpwd ) )
			ack.ret = MSG_AC2CLIENT::stFetchPwdAck::RET_SUCCESS;
		else
			ack.ret = MSG_AC2CLIENT::stFetchPwdAck::RET_WRONG_ANSWER;
		ack.nTransID = serial;
		session->send_packet( ack );
	}
}

void fetch_pwd_no_account_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stFetchPwdAck ack;
		ack.ret = MSG_AC2CLIENT::stFetchPwdAck::RET_WRONG_ACC;
		ack.nTransID = serial;
		session->send_packet( ack );

		g_ac_srv->log_event( session, "fetch password failed! account:[%s] does not exist", account_str.c_str() );
	}
}

void top_up_card_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stTopUpCardAck ack;
		if( acc->top_up_card( session, card_serial, card_password, realm_id, description ) )
			ack.ret = MSG_AC2CLIENT::stTopUpCardAck::RET_SUCCESS;
		else
			ack.ret = MSG_AC2CLIENT::stTopUpCardAck::RET_WRONG_CARD;
		ack.nTransID = serial;
		session->send_packet( ack );
	}
}

void top_up_card_no_account_act::do_it()
{
	if( session && session->is_valid() )
	{
		MSG_AC2CLIENT::stTopUpCardAck ack;
		ack.ret = MSG_AC2CLIENT::stTopUpCardAck::RET_WRONG_ACC;
		ack.nTransID = serial;
		session->send_packet( ack );

		g_ac_srv->log_event( session, "top up card failed! account:[%s] does not exist", account_str.c_str() );
	}
}

void generate_card_act::do_it()
{
	if( session && session->is_valid() )
	{
		if( count > 1000 )
			count = 1000;
		MSG_AC2CLIENT::stGenerateCardAck ack;
		
		ack.ret = MSG_AC2CLIENT::stGenerateCardAck::RET_DB_ERROR;
		if( g_ac_srv->get_card_generator()->init_batch( session, batch_number, get_tick_count() % 10240, description ) )
		{
			MSG_AC2CLIENT::stGenerateCardAck::card_t c;
			scoped_sql_transaction_proc sstp( g_ac_srv->get_main_db() );
			for( int i = 0; i < count; ++i )
			{
				if( g_ac_srv->get_card_generator()->gen( session, type, points, c.serial, c.password ) )
					ack.cards.push_back( c );
				else
					break;
			}
			sstp.success();
			ack.ret = MSG_AC2CLIENT::stGenerateCardAck::RET_SUCCESS;
		}
		else
			ack.ret = MSG_AC2CLIENT::stGenerateCardAck::RET_NO_LICENSE;
		
		ack.nTransID = serial;
		session->send_packet( ack );

		if( ack.ret == MSG_AC2CLIENT::stGenerateCardAck::RET_NO_LICENSE )
			g_ac_srv->log_event( session, "generate card failed! no license!" );
		else if( ack.ret == MSG_AC2CLIENT::stGenerateCardAck::RET_DB_ERROR )
			g_ac_srv->log_event( session, "generate card failed! db error!" );
		else
			g_ac_srv->log_event( session, "generate card succeed! batch:[%s] type:[%d] points[%d] description:[%s]", batch_number.c_str(), type, points, description.c_str() );
	}
}

void query_top_up_card_history_act::do_it()
{
	if( session && session->is_valid() )
	{
		g_ac_srv->query_top_up_card_history( id, session, serial );
	}
}

void query_remain_points_act::do_it()
{
	if( session && session->is_valid() )
	{
		g_ac_srv->query_remain_points( id, realm, session, serial );
	}
}

void rmb_direct_charge_act::do_it()
{
	if( session && session->is_valid() )
	{
		g_ac_srv->rmb_direct_charge( acc, target_realm, card_type, session, serial );
	}
}
