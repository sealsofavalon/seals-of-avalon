#ifndef _ACCOUNT_CENTER_ACTION_HEAD
#define _ACCOUNT_CENTER_ACTION_HEAD

class ac_session;
class account;

struct ac_action
{
	ac_action();
	ac_session* session;
	uint32 serial;
	account* acc;
	int threadid;
	std::string account_str;
	virtual void do_it() = 0;
};

struct auth_act : public ac_action
{
	std::string md5pwd;

	virtual void do_it();
};

struct auth_no_account_act : public ac_action
{
	virtual void do_it();
};

struct mod_pwd_act : public ac_action
{
	std::string oldmd5pwd;
	std::string newmd5pwd;

	virtual void do_it();
};

struct mod_pwd_no_account_act : public ac_action
{
	virtual void do_it();
};

struct mod_information_act : public ac_action
{
	account_info_t info;
	std::string answer1;
	std::string answer2;
	virtual void do_it();
};

struct mod_information_no_account_act : public ac_action
{
	virtual void do_it();
};

struct mod_pwd_by_qa_act : public ac_action
{
	std::string newpwd;
	int question;
	std::string answer;
	virtual void do_it();
};

struct safe_lock_act : public ac_action
{
	uint8 is_lock;
	std::string password;
	virtual void do_it();
};

struct create_act : public ac_action
{
	account_info_t info;
	std::string md5pwd;
	std::string answer1;
	std::string answer2;
	virtual void do_it();
};

struct create_already_exist_act : public ac_action
{
	virtual void do_it();
};

struct query_account_act : public ac_action
{
	virtual void do_it();
};

struct query_no_account_act : public ac_action
{
	virtual void do_it();
};

struct fetch_pwd_act : public ac_action
{
	int question;
	std::string answer;
	virtual void do_it();
};

struct fetch_pwd_no_account_act : public ac_action
{
	virtual void do_it();
};

struct top_up_card_act : public ac_action
{
	std::string card_serial;
	std::string card_password;
	int realm_id;
	std::string description;
	virtual void do_it();
};

struct top_up_card_no_account_act : public ac_action
{
	virtual void do_it();
};

struct generate_card_act : public ac_action
{
	std::string batch_number;
	int type;
	int points;
	int count;
	std::string description;
	virtual void do_it();
};

struct query_top_up_card_history_act : public ac_action
{
	int id;
	virtual void do_it();
};

struct query_remain_points_act : public ac_action
{
	int id;
	int realm;
	virtual void do_it();
};

struct rmb_direct_charge_act : public ac_action
{
	int target_realm;
	int card_type;
	virtual void do_it();
};

#endif // _ACCOUNT_CENTER_ACTION_HEAD
