#include "stdafx.h"
#include "ac_session.h"
#include "ac_srv.h"
#include "ac_action.h"
#include "account.h"
#include "account_storage.h"
#include "card_generator.h"

ac_session::ac_session(void) : tcp_session( *net_global::get_io_service() ), m_vip( false ), m_viptime( 0 ), m_lasthb( 0 )
{
}

ac_session::~ac_session(void)
{
}

void ac_session::on_accept( tcp_server* p )
{
	tcp_session::on_accept( p );
	m_lasthb = g_ac_srv->get_unix_time();
	MyLog::log->info( "IP:[%s] joined", get_remote_address_string().c_str() );
	g_ac_srv->log_event( this, "join" );
}

void ac_session::on_close( const boost::system::error_code& error )
{
	MyLog::log->info( "IP:[%s] left", get_remote_address_string().c_str() );
	g_ac_srv->log_event( this, "left, error_message:[%s], error_code:[%d]", error.message().c_str(), error.value() );
	tcp_session::on_close( error );
}

void ac_session::proc_message( const message_t& msg )
{
	CPacketUsn pak( (char*)msg.data, msg.len );

	switch( pak.GetProNo() )
	{
	case MSG_CLIENT2AC::AUTH_REQ:
		{
			MSG_CLIENT2AC::stAuthReq msg;
			pak >> msg;

			uint32 crcname = 0;
			account* acc = g_ac_storage->find_account( msg.name, crcname );
			if( !acc && g_ac_storage->is_account_inexistent( crcname ) )
			{
				MSG_AC2CLIENT::stAuthAck ack;
				ack.ret = MSG_AC2CLIENT::stAuthAck::RET_WRONG_ACC;
				ack.nTransID = msg.nTransID;
				send_packet( ack );

				g_ac_srv->log_event( this, "auth failed! account:[%s] does not exist", msg.name.c_str() );
				return;
			}

			auth_act* act = new auth_act;
			act->session = this;
			act->acc = acc;
			act->serial = msg.nTransID;
			act->md5pwd = msg.md5pwd;
			if( !acc )
			{
				acc = new account;
				auth_no_account_act* act2 = new auth_no_account_act;
				act2->session = this;
				act2->acc = acc;
				act2->serial = msg.nTransID;
				acc->load( msg.name, act, act2 );
			}
			else
			{
				act->threadid = acc->get_thread_id();
				g_ac_srv->push_act( act );
			}
		}
		break;
	case MSG_CLIENT2AC::MOD_PWD_REQ:
		{
			MSG_CLIENT2AC::stModPwdReq msg;
			pak >> msg;
			uint32 crcname = 0;
			account* acc = g_ac_storage->find_account( msg.name, crcname );
			if( !acc && g_ac_storage->is_account_inexistent( crcname ) )
			{
				MSG_AC2CLIENT::stModPwdAck ack;
				ack.ret = MSG_AC2CLIENT::stModPwdAck::RET_WRONG_ACC;
				ack.nTransID = msg.nTransID;
				send_packet( ack );

				g_ac_srv->log_event( this, "modify password failed! account:[%s] does not exist", msg.name.c_str() );
				return;
			}
			mod_pwd_act* act = new mod_pwd_act;
			act->session = this;
			act->acc = acc;
			act->serial = msg.nTransID;
			act->oldmd5pwd = msg.oldmd5pwd;
			act->newmd5pwd = msg.newmd5pwd;
			if( acc )
			{
				act->threadid = acc->get_thread_id();
				g_ac_srv->push_act( act );
			}
			else
			{
				mod_pwd_no_account_act* act2 = new mod_pwd_no_account_act;
				act2->session = this;
				act2->serial = msg.nTransID;
				acc = new account;
				acc->load( msg.name, act, act2 ) ;
			}
		}
		break;
	case MSG_CLIENT2AC::CREATE_ACC_REQ:
		{
			MSG_CLIENT2AC::stCreateAccReq msg;
			pak >> msg;

			uint32 crcname = 0;
			account* acc = g_ac_storage->find_account( msg.acc.name, crcname );
			if( acc )
			{
				MSG_AC2CLIENT::stCreateAccAck ack;
				ack.ret = MSG_AC2CLIENT::stCreateAccAck::RET_ALREADY_EXIST;
				ack.id = 0;
				ack.nTransID = msg.nTransID;
				send_packet( ack );

				g_ac_srv->log_event( this, "create account failed! account:[%s] is already exists", msg.acc.name.c_str() );
			}
			else
			{
				/*
				MSG_AC2CLIENT::stCreateAccAck ack;
				ack.ret = MSG_AC2CLIENT::stCreateAccAck::RET_DB_ERROR;
				if( msg.acc.id_card.length() )
				{
					if( g_ac_storage->is_id_card_exist( msg.acc.id_card ) )
						ack.ret = MSG_AC2CLIENT::stCreateAccAck::RET_ID_CARD_ALREADY_EXIST;
					else
					{
						smart_ptr<QueryResult> qr = g_ac_srv->get_main_db()->Query( "select id from account where id_card = '%s'", msg.acc.id_card.c_str() );
						if( qr )
						{
							g_ac_storage->add_id_card( msg.acc.id_card );
							ack.ret = MSG_AC2CLIENT::stCreateAccAck::RET_ID_CARD_ALREADY_EXIST;
						}
					}
					if( ack.ret == MSG_AC2CLIENT::stCreateAccAck::RET_ID_CARD_ALREADY_EXIST )
					{
						ack.nTransID = msg.nTransID;
						send_packet( ack );
						break;
					}
				}
				*/

				acc = new account;
				create_already_exist_act* act = new create_already_exist_act;
				act->session = this;
				act->serial = msg.nTransID;
				act->acc = acc;

				create_act* act2 = new create_act;
				act2->session = this;
				act2->serial = msg.nTransID;
				act2->acc = acc;
				act2->info = msg.acc;
				act2->md5pwd = msg.md5pwd;
				act2->answer1 = msg.answer1;
				act2->answer2 = msg.answer2;

				acc->load( msg.acc.name, act, act2 );
			}
		}
		break;
	case MSG_CLIENT2AC::QUERY_ACC_REQ:
		{
			MSG_CLIENT2AC::stQueryAccReq msg;
			pak >> msg;

			uint32 crcname = 0;
			account* acc = g_ac_storage->find_account( msg.name, crcname );
			if( acc )
			{
				MSG_AC2CLIENT::stQueryAccAck ack;
				ack.ret = MSG_AC2CLIENT::stQueryAccAck::RET_SUCCESS;
				ack.acc = acc->get_info();
				ack.nTransID = msg.nTransID;
				send_packet( ack );

				g_ac_srv->log_event( this, "query account succeed! account:[%s]", msg.name.c_str() );
			}
			else if( g_ac_storage->is_account_inexistent( crcname ) )
			{
				MSG_AC2CLIENT::stQueryAccAck ack;
				ack.nTransID = msg.nTransID;
				ack.ret = MSG_AC2CLIENT::stQueryAccAck::RET_WRONG_ACC;
				send_packet( ack );

				g_ac_srv->log_event( this, "query account failed! account:[%s] does not exist", msg.name.c_str() );
			}
			else
			{
				acc = new account;

				query_account_act* act = new query_account_act;
				act->session = this;
				act->serial = msg.nTransID;
				act->acc = acc;

				query_no_account_act* act2 = new query_no_account_act;
				act2->session = this;
				act2->serial = msg.nTransID;
				act2->acc = acc;

				acc->load( msg.name, act, act2 );
			}
		}
		break;
	case MSG_CLIENT2AC::FETCH_PWD_REQ:
		{
			MSG_CLIENT2AC::stFetchPwdReq msg;
			pak >> msg;

			uint32 crcname = 0;
			account* acc = g_ac_storage->find_account( msg.name, crcname );
			if( !acc && g_ac_storage->is_account_inexistent( crcname ) )
			{
				MSG_AC2CLIENT::stFetchPwdAck ack;
				ack.ret = MSG_AC2CLIENT::stFetchPwdAck::RET_WRONG_ACC;
				ack.nTransID = msg.nTransID;
				send_packet( ack );

				g_ac_srv->log_event( this, "fetch password failed! account:[%s] does not exist", msg.name.c_str() );
				return;
			}

			fetch_pwd_act* act = new fetch_pwd_act;
			act->session = this;
			act->serial = msg.nTransID;
			act->question = msg.question;
			act->answer = msg.answer;

			if( acc )
			{
				act->acc = acc;
				g_ac_srv->push_act( act );
			}
			else
			{
				fetch_pwd_no_account_act* act2 = new fetch_pwd_no_account_act;
				act2->session = this;
				act2->serial = msg.nTransID;

				acc = new account;
				acc->load( msg.name, act, act2 );
			}
		}
		break;
	case MSG_CLIENT2AC::TOP_UP_CARD_REQ:
		{
			MSG_CLIENT2AC::stTopUpCardReq msg;
			pak >> msg;

			uint32 crcname = 0;
			account* acc = g_ac_storage->find_account( msg.name, crcname );
			if( !acc && g_ac_storage->is_account_inexistent( crcname ) )
			{
				MSG_AC2CLIENT::stTopUpCardAck ack;
				ack.ret = MSG_AC2CLIENT::stTopUpCardAck::RET_WRONG_ACC;
				ack.nTransID = msg.nTransID;
				send_packet( ack );
				g_ac_srv->log_event( this, "top up card failed! account:[%s] does not exist", msg.name.c_str() );
				return;
			}

			top_up_card_act* act = new top_up_card_act;
			act->session = this;
			act->serial = msg.nTransID;
			act->card_serial = msg.serial;
			act->card_password = msg.password;
			act->description = msg.description;
			act->realm_id = msg.realm_id;
			if( acc )
			{
				act->acc = acc;
				act->threadid = acc->get_thread_id();
				g_ac_srv->push_act( act );
			}
			else
			{
				top_up_card_no_account_act* act2 = new top_up_card_no_account_act;
				act2->session = this;
				act2->serial = msg.nTransID;

				acc = new account;
				acc->load( msg.name, act, act2 );
			}
		}
		break;
	case MSG_CLIENT2AC::VIP_ACCESS_REQ:
		{
			MSG_CLIENT2AC::stVipAccessReq msg;
			pak >> msg;

			MSG_AC2CLIENT::stVipAccessAck ack;
			if( g_ac_srv->get_card_generator()->check_access( this, msg.user.c_str(), msg.password.c_str() ) )
			{
				set_vip();
				ack.ret = MSG_AC2CLIENT::stVipAccessAck::RET_SUCCESS;
			}
			else
			{
				ack.ret = MSG_AC2CLIENT::stVipAccessAck::RET_REFUSE;
			}
			ack.nTransID = msg.nTransID;
			send_packet( ack );
		}
		break;
	case MSG_CLIENT2AC::GENERATE_CARD_REQ:
		{
			MSG_CLIENT2AC::stGenerateCardReq msg;
			pak >> msg;
			generate_card_act* act = new generate_card_act;
			act->threadid = GEN_CARDS_THREAD_INDEX;
			act->session = this;
			act->serial = msg.nTransID;
			act->batch_number = msg.batch_number;
			act->type = msg.type;
			act->points = msg.points;
			act->count = msg.count;
			act->description = msg.description;
			g_ac_srv->push_act( act );
		}
		break;
	case MSG_CLIENT2AC::QUERY_TOP_UP_CARD_HISTORY_REQ:
		{
			MSG_CLIENT2AC::stQueryTopUpCardHistoryReq msg;
			pak >> msg;
			query_top_up_card_history_act* act = new query_top_up_card_history_act;
			act->threadid = GEN_CARDS_THREAD_INDEX;
			act->session = this;
			act->serial = msg.nTransID;
			act->id = msg.id;
			g_ac_srv->push_act( act );
		}
		break;
	case MSG_CLIENT2AC::QUERY_REMAIN_POINTS_REQ:
		{
			MSG_CLIENT2AC::stQueryRemainPointsReq msg;
			pak >> msg;
			query_remain_points_act* act = new query_remain_points_act;
			act->threadid = GEN_CARDS_THREAD_INDEX;
			act->session = this;
			act->serial = msg.nTransID;
			act->id = msg.id;
			act->realm = msg.realm;
			g_ac_srv->push_act( act );
		}
		break;
	case MSG_CLIENT2AC::RMB_DIRECT_CHARGE_REQ:
		{
			MSG_CLIENT2AC::stRMBDirectChargeReq msg;
			pak >> msg;
			uint32 crcname = 0;
			account* acc = g_ac_storage->find_account( msg.user, crcname );
			if( acc )
			{
				rmb_direct_charge_act* act = new rmb_direct_charge_act;
				act->threadid = acc->get_thread_id();
				act->session = this;
				act->acc = acc;
				act->serial = msg.nTransID;
				act->target_realm = msg.target_realm;
				act->card_type = msg.card_type;
				g_ac_srv->push_act( act );
			}
			else
			{
				acc = new account;

				rmb_direct_charge_act* act = new rmb_direct_charge_act;
				act->threadid = acc->get_thread_id();
				act->session = this;
				act->acc = acc;
				act->serial = msg.nTransID;
				act->target_realm = msg.target_realm;
				act->card_type = msg.card_type;

				query_no_account_act* act2 = new query_no_account_act;
				act2->session = this;
				act2->serial = msg.nTransID;
				act2->acc = acc;
				acc->load( msg.user, act, act2 );
// 
// 				MSG_AC2CLIENT::stRMBDirectChargeAck ack;
// 				ack.ret = MSG_AC2CLIENT::stRMBDirectChargeAck::RET_WRONG_ACCOUNT;
// 				ack.points = 0;
// 				ack.nTransID = msg.nTransID;
// 				send_packet( ack );
			}
		}
		break;
	case MSG_CLIENT2AC::CLEANUP_ACCOUNT_REQ:
		{
			MSG_CLIENT2AC::stCleanupAccountReq msg;
			pak >> msg;
			g_ac_storage->reload_account( msg.account );
			MSG_AC2CLIENT::stCleanupAccountAck ack;
			ack.ret = MSG_AC2CLIENT::stCleanupAccountAck::RET_SUCCESS;
			ack.nTransID = msg.nTransID;
			send_packet( ack );
		}
		break;
	case MSG_CLIENT2AC::MOD_INFORMATION_REQ:
		{
			MSG_CLIENT2AC::stModInformationReq msg;
			pak >> msg;
			uint32 crcname = 0;
			account* acc = g_ac_storage->find_account( msg.info.name, crcname );
			if( !acc )
			{
				MSG_AC2CLIENT::stModInformationAck ack;
				ack.ret = MSG_AC2CLIENT::stModInformationAck::RET_WRONG_ACC;
				ack.nTransID = msg.nTransID;
				send_packet( ack );

				g_ac_srv->log_event( this, "modify description failed! account:[%s] does not exist", msg.info.name.c_str() );
				return;
			}
			mod_information_act* act = new mod_information_act;
			act->session = this;
			act->acc = acc;
			act->serial = msg.nTransID;
			act->info = msg.info;
			act->answer1 = msg.answer1;
			act->answer2 = msg.answer2;
			act->threadid = acc->get_thread_id();
			g_ac_srv->push_act( act );
		}
		break;
	case MSG_CLIENT2AC::MOD_PWD_BY_QA_REQ:
		{
			MSG_CLIENT2AC::stModPwdByQaReq msg;
			pak >> msg;
			uint32 crcname = 0;
			account* acc = g_ac_storage->find_account( msg.account, crcname );
			if( !acc )
			{
				MSG_AC2CLIENT::stModPwdByQaAck ack;
				ack.ret = MSG_AC2CLIENT::stModPwdByQaAck::RET_WRONG_ACC;
				ack.nTransID = msg.nTransID;
				send_packet( ack );

				g_ac_srv->log_event( this, "modify password by question and answer failed! account:[%s] does not exist", msg.account.c_str() );
				return;
			}
			mod_pwd_by_qa_act* act = new mod_pwd_by_qa_act;
			act->session = this;
			act->acc = acc;
			act->serial = msg.nTransID;
			act->newpwd = msg.newpwd;
			act->question = msg.question;
			act->answer = msg.answer;
			act->threadid = acc->get_thread_id();
			g_ac_srv->push_act( act );
		}
		break;
	case MSG_CLIENT2AC::SAFE_LOCK_REQ:
		{
			MSG_CLIENT2AC::stSafeLockReq msg;
			pak >> msg;
			uint32 crcname = 0;
			account* acc = g_ac_storage->find_account( msg.account, crcname );
			if( !acc )
			{
				MSG_AC2CLIENT::stSafeLockAck ack;
				ack.ret = MSG_AC2CLIENT::stSafeLockAck::RET_WRONG_ACC;
				ack.nTransID = msg.nTransID;
				send_packet( ack );

				if( msg.is_lock )
					g_ac_srv->log_event( this, "safe lock failed! account:[%s] does not exist", msg.account.c_str() );
				else
					g_ac_srv->log_event( this, "safe unlock failed! account:[%s] does not exist", msg.account.c_str() );
				return;
			}
			safe_lock_act* act = new safe_lock_act;
			act->session = this;
			act->acc = acc;
			act->serial = msg.nTransID;
			act->password = msg.password;
			act->is_lock = msg.is_lock;
			act->threadid = acc->get_thread_id();
			g_ac_srv->push_act( act );
		}
		break;
	case MSG_CLIENT2AC::HEART_BEAT_REQ:
		{
			m_lasthb = g_ac_srv->get_unix_time();
			//MyLog::log->info( "IP:[%s] heart beat", get_remote_address_string().c_str() );
		}
		break;
	default:
		MyLog::log->error( "recv unknown message from client, message id : [%d]", pak.GetProNo() );
		g_ac_srv->log_event( this, "recv unknown message from client, message_id:[%d]", pak.GetProNo() );
#ifndef _DEBUG
		delay_close( 1 );
#endif
		break;
	}
}

void ac_session::reset()
{
	tcp_session::reset();
	m_lasthb = 0;
	m_vip = false;
	m_viptime = 0;
}

void ac_session::run()
{
	tcp_session::run();
	if( m_lasthb )
	{
		if( g_ac_srv->get_unix_time() - m_lasthb >= 61 )
		{
			m_lasthb = 0;
			MyLog::log->notice( "IP:[%s] cannot recv heart beat for a long time, force closesocket\n", get_remote_address_string().c_str() );
			g_ac_srv->log_event( this, "have not recv heart beat for a long time, force closesocket" );
			close();
		}
	}
	if( m_vip && m_viptime )
	{
		if( g_ac_srv->get_unix_time() - m_viptime >= 60 )
		{
			m_vip = false;
			m_viptime = 0;
		}
	}
}

void ac_session::send_packet( const PakHead& pak )
{
	CPacketSn cps;
	cps << pak;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}

void ac_session::set_vip()
{
	m_vip = true;
	m_viptime = g_ac_srv->get_unix_time();
}
