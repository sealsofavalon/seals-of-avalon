#ifndef _ACCOUNT_CENTER_SESSION_HEAD
#define _ACCOUNT_CENTER_SESSION_HEAD

class ac_session : public tcp_session
{
public:
	ac_session(void);
	~ac_session(void);

public:
	virtual void on_accept( tcp_server* p );
	virtual void on_close( const boost::system::error_code& error );
	virtual void proc_message( const message_t& msg );
	virtual void reset();
	virtual void run();
	void send_packet( const PakHead& pak );
	void set_vip();

public:
	inline bool is_vip() const { return m_vip; }

private:
	volatile uint32 m_lasthb;
	volatile uint32 m_viptime;
	volatile bool m_vip;
};


#endif // _ACCOUNT_CENTER_SESSION_HEAD