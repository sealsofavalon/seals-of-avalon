#include "stdafx.h"
#include "ac_srv.h"
#include "ac_session.h"
#include "ac_action.h"
#include "card_generator.h"
#include "account.h"

ac_srv::ac_srv(void) : tcp_server( 0 ), m_exit( false ), m_last_log_time( 0 )
{
	m_unix_time = (uint32)time( NULL );
	for( int i = 0; i < THREAD_COUNT; ++i )
		m_threads[i] = new boost::thread( boost::bind( &ac_srv::thread_run, this, i ) );

	m_log_evt_thread = new boost::thread( boost::bind( &ac_srv::log_evt_proc, this ) );
	m_card_generator = new card_generator;
}

ac_srv::~ac_srv(void)
{
	m_exit = true;
	for( int i = 0; i < THREAD_COUNT; ++i )
	{
		m_threads[i]->join();
		delete m_threads[i];
	}
	m_log_evt_thread->join();
	delete m_log_evt_thread;
	delete m_card_generator;
}

extern Database* g_db;
extern std::map<int, Database*> g_map_realm_db;

Database* ac_srv::get_main_db() const
{
	return g_db;
}

Database* ac_srv::get_realm_db( int realm_id ) const
{
	std::map<int, Database*>::iterator it = g_map_realm_db.find( realm_id );
	if( it != g_map_realm_db.end() )
		return it->second;
	else
		return NULL;
}

void ac_srv::query_top_up_card_history( int id, ac_session* s, int transid )
{
	MSG_AC2CLIENT::stQueryTopUpCardHistoryAck ack;
	MSG_AC2CLIENT::stQueryTopUpCardHistoryAck::history_t his;

	for( std::map<int, Database*>::iterator it = g_map_realm_db.begin(); it != g_map_realm_db.end(); ++it )
	{
		int realm = it->first;
		Database* pdb = it->second;
		smart_ptr<QueryResult> qr = pdb->Query( "select type, init_points, remain_points, date from acc_point_card where acc_id = %d", id );
		if( qr )
		{
			do
			{
				Field* f = qr->Fetch();
				his.type = f[0].GetUInt32();
				his.points = f[1].GetUInt32();
				his.remain_points = f[2].GetUInt32();
				his.time = f[3].GetUInt32();
				his.realm = realm;
				ack.historys.push_back( his );
			}
			while( qr->NextRow() );		
		}
	}

	if( ack.historys.size() == 0 )
		ack.ret = MSG_AC2CLIENT::stQueryTopUpCardHistoryAck::RET_NO_RECORD;
	else
		ack.ret = MSG_AC2CLIENT::stQueryTopUpCardHistoryAck::RET_SUCCESS;
	
	ack.nTransID = transid;
	s->send_packet( ack );
}

void ac_srv::query_remain_points( int id, int realm, ac_session* s, int transid )
{
	MSG_AC2CLIENT::stQueryRemainPointsAck ack;
	ack.remain_points = 0;

	Database* db = get_realm_db( realm );
	if( db )
	{
		smart_ptr<QueryResult> qr = db->Query( "select sum(remain_points) from acc_point_card where acc_id = %d", id );
		if( qr )
		{
			do
			{
				Field* f = qr->Fetch();
				ack.remain_points += f[0].GetUInt32();
			}
			while( qr->NextRow() );
		}
	}
	ack.ret = MSG_AC2CLIENT::stQueryRemainPointsAck::RET_SUCCESS;
	ack.nTransID = transid;
	s->send_packet( ack );
}

void ac_srv::rmb_direct_charge( account* acct, int target_realm, int card_type, ac_session* s, int transid )
{
	MSG_AC2CLIENT::stRMBDirectChargeAck ack;
	ack.nTransID = transid;
	ack.points = 0;
	smart_ptr<QueryResult> qr = g_db->Query( "select serial, password, points from cards where type = %d and valid = 1 limit 1", card_type );
	if( qr )
	{
		Field* f = qr->Fetch();
		std::string serial = f[0].GetString();
		std::string pwd = f[1].GetString();
		if( acct->top_up_card( s, serial, pwd, target_realm, "RMB Direct charge" ) )
		{
			ack.ret = MSG_AC2CLIENT::stRMBDirectChargeAck::RET_SUCCESS;
			ack.points = f[2].GetUInt32();
		}
		else
			ack.ret = MSG_AC2CLIENT::stRMBDirectChargeAck::RET_DB_ERROR;
	}
	else
		ack.ret = MSG_AC2CLIENT::stRMBDirectChargeAck::RET_NO_MATCH_CARD;

	s->send_packet( ack );
}

void ac_srv::log_event( ac_session* s, const char* evt, ... )
{
	if( !s )
		return;
	char sql[1224] = { 0 };

	char temp[1024];
	va_list vlist;
	va_start( vlist, evt );
	vsnprintf( temp, 1024, evt, vlist );
	va_end( vlist );

	time_t t = get_unix_time();
	tm* lt = localtime( &t );
	char local_time_str[32] = { 0 };
	sprintf( local_time_str, "%d-%02d-%02d %02d:%02d:%02d", lt->tm_year + 1900, lt->tm_mon + 1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec );

	int len = strlen( temp );
	for( int i = 0; i < len; ++i )
		if( temp[i] == '\'' )
			temp[i] = ' ';
	sprintf( sql, "insert into ac_log (date, ip, event) values('%s', '%s', '%s')", local_time_str, s->get_remote_address_string().c_str(), temp );
	boost::mutex::scoped_lock lock( m_log_mutex );
	m_queue_log_evt.push( std::string( sql ) );
}

tcp_session* ac_srv::create_session()
{
	return new ac_session();
}

void ac_srv::push_act( ac_action* act )
{
	boost::mutex::scoped_lock lock( m_act_mutex[act->threadid] );
	m_queue_acts[act->threadid].push( act );
	m_act_cond[act->threadid].notify_one();
}

bool ac_srv::_swap_queue( uint32 threadid, std::queue<ac_action*>& q )
{
	if( m_queue_acts[threadid].empty() )
		return false;
	else
	{
		std::swap( q, m_queue_acts[threadid] );
		return true;
	}
}

void ac_srv::_proc_queue( std::queue<ac_action*>& q )
{
	while( !q.empty() )
	{
		ac_action* act = q.front();
		act->do_it();
		delete act;
		q.pop();
	}
}

void ac_srv::log_evt_proc()
{
	while( !m_exit )
	{
		Sleep( 1000 );
		if( m_unix_time - m_last_log_time >= 60 )
		{
			m_last_log_time = m_unix_time;
			std::queue<std::string> q;
			{
				boost::mutex::scoped_lock lock( m_log_mutex );
				if( !m_queue_log_evt.empty() )
					std::swap( q, m_queue_log_evt );
				else
					continue;
			}

			scoped_sql_transaction_proc sstp( get_main_db() );
			while( !q.empty() )
			{
				const std::string& sql = q.front();
				get_main_db()->TransactionExecute( sql.c_str() );
				q.pop();
			}
			sstp.success();
		}
	}
}

void ac_srv::run()
{
	m_unix_time = (uint32)time( NULL );
	tcp_server::run();
}

void ac_srv::thread_run( int id )
{
	std::queue<ac_action*> q;
	while( !m_exit )
	{
		{
			boost::mutex::scoped_lock lock( m_act_mutex[id] );
			if( m_queue_acts[id].empty() )
				m_act_cond[id].wait( lock );
			_swap_queue( id, q );
		}
		_proc_queue( q );
	}
}
