#ifndef _ACCOUNT_CENTER_SERVER_HEAD
#define _ACCOUNT_CENTER_SERVER_HEAD

enum
{
	ACC_THREAD_INDEX = 30,
	NORMAL_THREAD_INDEX,
	GEN_CARDS_THREAD_INDEX,
	THREAD_COUNT,
};

struct ac_action;
class ac_session;
class card_generator;
class account;

class ac_srv : public tcp_server
{
public:
	ac_srv(void);
	~ac_srv(void);

public:
	inline uint32 get_unix_time() const { return m_unix_time; }
	inline card_generator* get_card_generator() const { return m_card_generator; }
	boost::mutex& get_top_up_card_mutex() { return m_top_up_card_mutex; }

public:
	virtual void run();

public:
	void push_act( ac_action* act );
	void thread_run( int id );
	Database* get_main_db() const;
	Database* get_realm_db( int realm_id ) const;
	void query_top_up_card_history( int id, ac_session* s, int transid );
	void query_remain_points( int id, int realm, ac_session* s, int transid );
	void rmb_direct_charge( account* acct, int target_realm, int card_type, ac_session* s, int transid );
	void log_event( ac_session* s, const char* evt, ... );
	void log_evt_proc();

public:
	virtual tcp_session* create_session();

private:
	bool _swap_queue( uint32 threadid, std::queue<ac_action*>& q );
	void _proc_queue( std::queue<ac_action*>& q );

private:
	volatile bool m_exit;
	boost::mutex m_top_up_card_mutex;
	boost::mutex m_act_mutex[THREAD_COUNT];
	boost::condition m_act_cond[THREAD_COUNT];
	boost::thread* m_threads[THREAD_COUNT];
	boost::thread* m_log_evt_thread;
	std::queue<ac_action*> m_queue_acts[THREAD_COUNT];
	volatile uint32 m_unix_time;
	card_generator* m_card_generator;
	std::queue<std::string> m_queue_log_evt;
	uint32 m_last_log_time;
	boost::mutex m_log_mutex;
};

extern ac_srv* g_ac_srv;

#endif // _ACCOUNT_CENTER_SERVER_HEAD
