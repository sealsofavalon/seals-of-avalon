#ifndef _ACCOUNT_HEAD
#define _ACCOUNT_HEAD

struct ac_action;
class ac_session;

class account
{
public:
	account(void);
	~account(void);

public:
	inline int get_id() const { return m_info.id; }
	inline const std::string& get_name() const { return m_info.name; }
	inline int get_thread_id() const { return m_threadid; }
	inline void set_thread_id( int id ) { m_threadid = id; }
	inline const account_info_t& get_info() const { return m_info; }

public:
	// actions
	bool create( ac_session* s, int id, const account_info_t& info, const std::string& md5pwd, const std::string& pwdanswer1, const std::string& pwdanswer2 );
	bool auth( ac_session* s, const std::string& md5pwd );
	bool mod_pwd( ac_session* s, const std::string& oldmd5pwd, const std::string& newmd5pwd, bool force = false );
	bool fetch_pwd( ac_session* s, int question, const std::string& answer, std::string& newpwd );
	bool top_up_card( ac_session* s, const std::string& serial, const std::string& password, int realm_id, const std::string& decription );
	bool mod_pwd_by_qa( ac_session* s, int question, const std::string& answer, const std::string& newpwd );
	bool mod_information( ac_session* s, const account_info_t& info, const std::string& answer1, const std::string& answer2 );

	bool safe_lock( ac_session* s, const std::string& safe_lock_pwd, uint8 is_lock );

public:
	void load( const std::string& name, ac_action* act_succ = NULL, ac_action* act_fail = NULL );
	void reload();
	void load_proc( QueryResultVector& results );
	void reload_proc( QueryResultVector& results );

private:
	void _auto_gen_pwd( std::string& pwd );
	account_info_t m_info;
	std::string m_md5pwd;
	std::string m_pwdanswer[2];
	int m_threadid;
	ac_action* m_load_succ_act;
	ac_action* m_load_fail_act;
};

#endif // _ACCOUNT_HEAD