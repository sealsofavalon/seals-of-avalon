// AccountCenter.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "account.h"
#include "account_storage.h"
#include "ac_srv.h"
#include "ac_session.h"
#include <mysql/mysql.h>
#include "card_generator.h"
#include "WebInterface.h"

zip_compress_strategy cs;
std::string mysqlip;
std::string mysqlname;
std::string mysqluser;
std::string mysqlpwd;
uint16 mysqlport;
uint16 listenport;
Database* g_db;
std::map<int, Database*> g_map_realm_db;
volatile bool g_exit = false;
account_storage* g_ac_storage = NULL;
ac_srv* g_ac_srv = NULL;

struct scoped_server_monitor
{
	scoped_server_monitor()
	{
		printf( "\n/************************************************************************/\n" );
		printf( "[server starting...]\n" );
		printf( "/************************************************************************/\n" );
	}
	~scoped_server_monitor()
	{
		printf( "/************************************************************************/\n" );
		printf( "[server was shut down, please input any words and press enter to exit]\n" );
		printf( "/************************************************************************/\n" );

		char temp[128];
		scanf( "%s", temp );
	}
};

bool load_card_admin()
{
	g_ac_srv->get_card_generator()->clear_access_user();
	QueryResult* qr = g_db->Query( "select * from card_generate_access" );
	if( qr )
	{
		do
		{
			Field* f = qr->Fetch();
			g_ac_srv->get_card_generator()->add_access_user( f[0].GetString(), f[1].GetString(), f[2].GetString() );
			MyLog::log->info( "get card generate access user:%s", f[0].GetString() );
		}
		while( qr->NextRow() );
		delete qr;
	}
	return true;
}

bool init()
{
	/*
	data_stream_verifier dsv;

 	uint32 tmp = dsv.make_checksum( "hahahert asdf asd wer qwerq werqw eqweq sdf xcvvxcv xcv sdf asd asd asd asd qwe ahaha", 100 );
 	bool bb = dsv.verify_data_stream( "hahahert asdf asd wer qwerq werqw eqweq sdf xcvvxcv xcv sdf asd asd asd asd qwe ahaha", 100, tmp );
// 	bool bb2 = dsv.verify_data_stream( "hahahahaha", 10, tmp );

	*/
	MyLog::Init();
	FILE* fp = fopen( "AccountCenter.conf", "r" );
	char key[32];
	char value[32];
	if( fp )
	{
		fscanf( fp, "%s %s\n", key, value );
		mysqlip = value;
		fscanf( fp, "%s %s\n", key, value );
		mysqlname = value;
		fscanf( fp, "%s %s\n", key, value );
		mysqluser = value;
		fscanf( fp, "%s %s\n", key, value );
		mysqlpwd = value;
		fscanf( fp, "%s %u\n", key, &mysqlport );
		fscanf( fp, "%s %u", key, &listenport );
		fclose( fp );

		unsigned int mysqlthreadsafe = mysql_thread_safe();
		if( !mysqlthreadsafe )
		{
			MyLog::log->error( "mysql lib is not a thread safe mode!!!!!!!!!" );
			return false;
		}

		Database::StartThread();

		g_ac_srv = new ac_srv;

		g_db = new MySQLDatabase();
		if( !g_db->Initialize( mysqlip.c_str(), mysqlport, mysqluser.c_str(), mysqlpwd.c_str(), mysqlname.c_str(), 5, 16384 ) )
		{
			MyLog::log->error( "connect account center db failed!" );
			return false;
		}
		MyLog::log->notice( "connect account center db succeed!" );
		QueryResult* qr = g_db->Query( "select * from realm_db_conf" );
		if( qr )
		{
			do
			{
				Field* f = qr->Fetch();
				int id = f[0].GetInt32();
				std::string name( f[1].GetString() );
				std::string ip( f[2].GetString() );
				uint16 port = f[3].GetUInt16();
				std::string dbname( f[4].GetString() );
				std::string user( f[5].GetString() );
				std::string pwd( f[6].GetString() );

				MyLog::log->notice( "read realm: id:%d name:%s ip:%s port:%d dbname:%s user:%s pwd:******", id, name.c_str(), ip.c_str(), port, dbname.c_str(), user.c_str() );

				Database* pdb = new MySQLDatabase();
				if( pdb->Initialize( ip.c_str(), port, user.c_str(), pwd.c_str(), dbname.c_str(), 5, 16384 ) )
				{
					g_map_realm_db[id] = pdb;
					MyLog::log->info( "connect realmdb:%s succeed!", name.c_str() );
				}
				else
				{
					MyLog::log->error( "connect realmdb:%s failed!", name.c_str() );
					return false;
				}
			}
			while( qr->NextRow() );
			delete qr;
		}
		else
		{
			MyLog::log->error( "read realm config failed" );
			return false;
		}

		load_card_admin();

#ifdef _WIN32
		net_global::init_net_service( 2, 1, &cs, true, 100 );
#else
		net_global::init_net_service( 16, 1, &cs, true, 1024 );
#endif
		MyLog::log->notice( "net service initialized!" );
		g_ac_storage = new account_storage;
		if( g_ac_srv->create( listenport, 30, 8 ) )
		{
			MyLog::log->notice( "listen at port:%d", listenport );
			return true;
		}
		else
		{
			MyLog::log->notice( "failed to listen at port:%d", listenport );
			return false;
		}

		gWebInterface.Init();
	}
	else
	{
		MyLog::log->error( "cannot open file [AccountCenter.conf]" );
		return false;
	}
}

void release()
{
	net_global::free_net_service();

	delete g_ac_srv;
	delete g_ac_storage;
	delete g_db;
	for( std::map<int, Database*>::iterator it = g_map_realm_db.begin(); it != g_map_realm_db.end(); ++it )
		delete it->second;
	g_map_realm_db.clear();
	gWebInterface.Release();
	MyLog::Release();
}

void main_loop()
{
	while( !g_exit )
	{
		g_ac_srv->run();
		g_db->QueryTaskRun();
		for( std::map<int, Database*>::iterator it = g_map_realm_db.begin(); it != g_map_realm_db.end(); ++it )
			it->second->QueryTaskRun();
	}
	MyLog::log->info( "main_loop thread exited" );
}

int main()
{
	scoped_server_monitor ssm;
	if( !init() )
		return -1;

	MyLog::log->info( "AccountCenter has been started successfully!\n");//type exit to close the service\ntype cache to watch count of accounts\n" );
	/*
	boost::thread t( &main_loop );
	std::string input;
	while( true )
	{
		std::cin >> input;
		if( stricmp( input.c_str(), "exit" ) == 0 )
			break;
		else if( stricmp( input.c_str(), "cache" ) == 0 )
		{
			printf( "storage valid account: %u, invalid accounts: %u\n", g_ac_storage->get_account_count(), g_ac_storage->get_inexsistent_account_count() );
		}
		else if( stricmp( input.c_str(), "gencards" ) == 0 )
		{
			std::string batch_number;
			int batch_count = 0;
			std::cin >> batch_number >> batch_count;
			g_ac_srv->get_card_generator()->init_batch( NULL, batch_number, g_ac_srv->get_unix_time(), "batch generate" );

			std::string serial, password;
			for( int i = 0; i < batch_count; ++i )
				g_ac_srv->get_card_generator()->gen( NULL, 1, 1000, serial, password );
		}
		Sleep( 100 );
	}

	g_exit = true;
	t.join();
	*/
	main_loop();
	release();
	return 0;
}

