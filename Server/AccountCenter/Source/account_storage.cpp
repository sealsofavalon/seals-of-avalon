#include "stdafx.h"
#include "account_storage.h"
#include "account.h"
#include "ac_srv.h"
#include "crc32.h"

account_storage::account_storage(void) : m_generate_threadid( -1 )
{
	QueryResult* result = g_ac_srv->get_main_db()->Query( "select max(id) from account" );
	if( result )
	{
		m_maxid = result->Fetch()[0].GetInt32();
		delete result;
		MyLog::log->notice( "max account id is:[%d]", m_maxid );
	}
	else
	{
		MyLog::log->error( "cannot get max id from table:account" );
		exit( -1 );
	}
}

account_storage::~account_storage(void)
{
	for( acc_map_t::iterator it = m_accounts.begin(); it != m_accounts.end(); ++it )
		delete it->second;
}

account* account_storage::find_account( const std::string& name, uint32& crcname )
{
	crcname = crc32( (const unsigned char*)name.c_str(), name.length() );

	boost::mutex::scoped_lock lock( m_mutex );
	acc_map_t::iterator it = m_accounts.find( crcname );
	if( it != m_accounts.end() )
	{
		account* p = it->second;
		if( p->get_name() == name )
			return p;
		//else
		//	assert( 0 && "fatal error : crc32 duplication" );
	}
	return NULL;
}

int account_storage::add_account( account* p, uint32& crcname )
{
	crcname = crc32( (const unsigned char*)p->get_name().c_str(), p->get_name().length() );

	boost::mutex::scoped_lock lock( m_mutex );
	m_accounts.insert( acc_map_t::value_type( crcname, p ) );
	m_inexistent_accounts.erase( crcname );
	m_existent_idcards.insert( p->get_info().id_card );
	if( ++m_generate_threadid > ACC_THREAD_INDEX )
		m_generate_threadid = 0;
	return m_generate_threadid;
}

void account_storage::add_inexistent_account( const std::string& name )
{
	uint32 len = name.length();
	if( len )
	{
		uint32 crcname = crc32( (const unsigned char*)name.c_str(), len );
		boost::mutex::scoped_lock lock( m_mutex );
		m_inexistent_accounts.insert( crcname );
		m_accounts.erase( crcname );
	}
}

bool account_storage::is_account_inexistent( uint32 crcname )
{
	boost::mutex::scoped_lock lock( m_mutex );
	if( m_inexistent_accounts.end() != m_inexistent_accounts.find( crcname ) )
		return true;
	else
		return false;
}

void account_storage::add_id_card( const std::string& id_card )
{
	boost::mutex::scoped_lock lock( m_mutex );
	m_existent_idcards.insert( id_card );
}

bool account_storage::is_id_card_exist( const std::string& id_card )
{
	boost::mutex::scoped_lock lock( m_mutex );
	if( m_existent_idcards.end() != m_existent_idcards.find( id_card ) )
		return true;
	else
		return false;
}

void account_storage::reload_account( const std::string& name )
{
	uint32 crcname = 0;
	account* acc = find_account( name, crcname );
	if( acc )
	{
		acc->reload();
	}
}

int account_storage::create_new_account( ac_session* s, const account_info_t& info, const std::string& md5pwd, const std::string& answer1, const std::string& answer2 )
{
	account* p = new account;
	if( !p->create( s, ++m_maxid, info, md5pwd, answer1, answer2 ) )
	{
		delete p;
		return 0;
	}
	uint32 crcname = 0;
	p->set_thread_id( add_account( p, crcname ) );
	return m_maxid;
}

/*
void account_storage::del_account( account* p )
{
	uint32 crcname = crc32( p->get_name().c_str(), p->get_name().length() );
	m_accounts.erase( crcname );
}
*/
