#ifndef ACCOUNT_STORAGE_HEAD
#define ACCOUNT_STORAGE_HEAD

class account;
class ac_session;

class account_storage
{
public:
	account_storage(void);
	~account_storage(void);

public:
	account* find_account( const std::string& name, uint32& crcname );
	int add_account( account* p, uint32& crcname );		// return generated threadid
	int create_new_account( ac_session* s, const account_info_t& info, const std::string& md5pwd, const std::string& answer1, const std::string& answer2 );
	void add_inexistent_account( const std::string& name );
	bool is_account_inexistent( uint32 crcname );
	void add_id_card( const std::string& id_card );
	bool is_id_card_exist( const std::string& id_card );
	void reload_account( const std::string& name );

public:
	inline size_t get_account_count() const { return m_accounts.size(); }
	inline size_t get_inexsistent_account_count() const { return m_inexistent_accounts.size(); }

private:
	typedef HM_NAMESPACE::hash_map<uint32, account*> acc_map_t;
	acc_map_t m_accounts;
	typedef HM_NAMESPACE::hash_set<uint32> acc_set_t;
	acc_set_t m_inexistent_accounts;
	typedef std::set<std::string> id_card_set_t;

	id_card_set_t m_existent_idcards;
	int m_generate_threadid;
	int m_maxid;
	boost::mutex m_mutex;
};

extern account_storage* g_ac_storage;

#endif // ACCOUNT_STORAGE_HEAD
