#include "stdafx.h"
#include "card_generator.h"
#include "crc32.h"
#include "../../../new_common/Source/ssl/MD5.h"
#include "ac_srv.h"
#include "ac_session.h"

card_generator::card_generator(void) : m_batch_serial( -1 )
{
	const char* p = "ABCDEFGHIJKLMNDPQRSTUVWXYZ0123456789";
	for( int i = 0; i < TABLE_SIZE; ++i )
		m_table[i] = p[i % 36];
	m_table[TABLE_SIZE] = 0;
}

card_generator::~card_generator(void)
{
}

void card_generator::add_access_user( const char* user, const char* password, const char* ip )
{
	card_generate_access_t cga = { user, password, ip };
	m_list_cga.push_back( cga );
}

void card_generator::clear_access_user()
{
	m_list_cga.clear();
}

bool card_generator::check_access( ac_session* s, const char* user, const char* password ) const
{
	for( std::list<card_generate_access_t>::const_iterator it = m_list_cga.begin(); it != m_list_cga.end(); ++it )
	{
		const card_generate_access_t& cga = *it;
		if( cga.user == user && cga.password == password && cga.ip == s->get_remote_address_string() )
		{
			g_ac_srv->log_event( s, "user:[%s] access to generate cards", user );
			return true;
		}
	}
	g_ac_srv->log_event( s, "user:[%s] generate cards license not pass", user );
	return false;
}

int Rand(int n)
{
	return rand() % n ;
}

bool card_generator::init_batch( ac_session* s, const std::string& batch_number, int seed, const std::string& batch_description )
{
	if( s && !s->is_vip() )
	{
		g_ac_srv->log_event( s, "connection does not have generate cards license!!!" );
		return false;
	}
	m_batch_serial = 0;
	m_batch_number = batch_number;
	while( m_batch_number.length() < 6 )
		m_batch_number += '0';
	m_batch_description = batch_description;
	srand( seed );
#ifdef _WIN32
	std::random_shuffle( &m_table[0], &m_table[TABLE_SIZE], pointer_to_unary_function<int, int>( Rand ) );
#else
	std::random_shuffle( &m_table[0], &m_table[TABLE_SIZE], Rand );
#endif
	g_ac_srv->log_event( s, "init batch_number:%s description:%s", batch_number.c_str(), batch_description.c_str() );
	return true;
}

bool card_generator::gen( ac_session* s, int type, int points, std::string& out_serial, std::string& out_password )
{
	if( m_batch_serial < 0 )
	{
		return false;
	}
	uint8 key = ( ++m_batch_serial % TABLE_SIZE );
	uint32 rd = rand() % ( TABLE_SIZE >> 2 );
	char serial[23] = { 0 };
	memcpy( &serial[0], m_batch_number.c_str(), 6 );
	char temp[1024] = { 0 };
	sprintf( temp, "%s@%d@%d@%d@%s", m_batch_number.c_str(), key, rd, m_batch_serial, &m_table[rd] );
	uint32 n = crc32( (const unsigned char*)temp, strlen( temp ) );
	std::string md5_str( MD5Hash::_make_md5_pwd( temp ) );
	memcpy( &serial[6], md5_str.c_str(), 12 );
	sprintf( &serial[18], "%04d", m_batch_serial );
	out_serial = serial;

	char password[17] = { 0 };
	sprintf( temp, "%s$%d$%d$%d", &m_table[n % (TABLE_SIZE >> 2)], n, m_batch_serial, g_ac_srv->get_unix_time() );
	md5_str = MD5Hash::_make_md5_pwd( temp );
	memcpy( password, md5_str.c_str(), 16 );
	for( int i = 0; i < 8; ++i )
	{
		password[i*2] = m_table[++n % TABLE_SIZE];
	}
	out_password = password;

	std::string ip( "local" );
	if( s )
		ip = s->get_remote_address_string();
	if( g_ac_srv->get_main_db()->TransactionExecute( "insert into cards (serial, password, points, description, type, valid, target_acc_id, target_realm_id, topup_date, generate_date, generate_ip) values('%s', '%s', %d, '%s', %d, 1, 0, 0, 0, %u, '%s')",
												serial, password, points, m_batch_description.c_str(), type, g_ac_srv->get_unix_time(), ip.c_str() ) )
	{
//		g_ac_srv->log_event( s, "generate card succeed! card serial:[%s] points:[%d] generate_ip:[%s]", serial, points, ip.c_str() );
		return true;
	}
	else
	{
		g_ac_srv->log_event( s, "generate card failed! database execute error! card serial:[%s] points:[%d] generate_ip:[%s]", serial, points, ip.c_str() );
		return false;
	}
}
