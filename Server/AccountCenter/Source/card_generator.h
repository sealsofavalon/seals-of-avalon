#ifndef _CARD_GENERATOR_HEAD_
#define _CARD_GENERATOR_HEAD_


class ac_session;

class card_generator
{
	enum { TABLE_SIZE = 360 };
public:
	card_generator(void);
	~card_generator(void);

public:
	bool init_batch( ac_session* s, const std::string& batch_number, int seed, const std::string& batch_description );
	void add_access_user( const char* user, const char* password, const char* ip );
	void clear_access_user();
	bool check_access( ac_session* s, const char* user, const char* password ) const;
	bool gen( ac_session* s, int type, int points, std::string& out_serial, std::string& out_password );

private:
	char m_table[TABLE_SIZE + 1];
	int m_batch_serial;
	std::string m_batch_number;
	std::string m_batch_description;
	struct card_generate_access_t
	{
		std::string user;
		std::string password;
		std::string ip;
	};
	std::list<card_generate_access_t> m_list_cga;
};

#endif // _CARD_GENERATOR_HEAD_