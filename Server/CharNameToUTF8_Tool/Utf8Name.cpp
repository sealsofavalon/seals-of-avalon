#include "Utf8Name.h"
#include "Singleton.h"
#include "../../../new_common/Source/new_common.h"
#include "../Common/share/Database/Database.h"
#include "../Common/share/Database/MySQLDatabase.h"
#include "../Common/share/mysql/mysql.h"
#include "../Common/share/Common.h"


#ifdef _WIN32
std::string ChAnisToUTF8(const std::string& str)
{
	char acUnicode[4096];
	char acUTF8[4096];

	//Ansi 转换为 Unicode
	ui16 usLen = (unsigned short)MultiByteToWideChar(936, 0, str.c_str(), str.size(), (LPWSTR)acUnicode, sizeof(acUnicode)/2);

	//Unicode 转换为 Utf8
	usLen = (unsigned short)WideCharToMultiByte(CP_UTF8, 0, (LPCWSTR)acUnicode, usLen, acUTF8, sizeof(acUTF8), NULL, false);

	return std::string(acUTF8, usLen);
}
#else
#include <iconv.h>
int code_convert(const char *from_charset, const char *to_charset,char *inbuf, size_t inlen, char *outbuf, size_t outlen) 
{ 
	iconv_t cd; 
	int rc; 
	char **pin = &inbuf; 
	char **pout = &outbuf; 

	cd = iconv_open(to_charset,from_charset); 
	if (cd==0) return -1; 
	memset(outbuf,0,outlen); 
	if (iconv(cd,pin,&inlen,pout,&outlen)==-1) return -1; 
	iconv_close(cd); 
	return 0; 
} 
//GB2312码 转为 UNICODE码
int g2u(char *inbuf, size_t inlen,char *outbuf,size_t outlen) 
{ 
	return code_convert("gb2312","utf-8",inbuf,inlen,outbuf,outlen); 
} 


std::string ChAnisToUTF8(const std::string& str)
{
	char to_str[1024] = { 0 };
	size_t n = 1024;
	g2u( (char*)str.c_str(), str.length(), (char*)to_str, n );

	return std::string( to_str );
}
#endif

Database* g_Character_DB = NULL;
Utf8NameMgr*  g_pkUtf8NameMgr = NULL ;

Utf8NameMgr::Utf8NameMgr()
{

}
Utf8NameMgr::~Utf8NameMgr()
{

}

bool Utf8NameMgr::InitMgr()
{
	FILE*fp = fopen("CharNameCode.conf", "r");
	if (!fp)
	{
		return false ;
	}
	char key[32];
	char value[32];
	std::string  ip, username, password, dataname;
	unsigned int port ;

	fscanf( fp, "%s %s\n", key, value );
	ip = value;
	fscanf( fp, "%s %s\n", key, value );
	dataname = value;
	fscanf( fp, "%s %s\n", key, value );
	username = value;
	fscanf( fp, "%s %s\n", key, value );
	password = value;
	fscanf( fp, "%s %u\n", key, &port );
	fclose( fp );

	MyLog::Init();
	unsigned int mysqlthreadsafe = mysql_thread_safe();
	if( !mysqlthreadsafe )
	{
		MyLog::log->error( "mysql lib is not a thread safe mode!!!!!!!!!" );
		return false;
	}
	Database::StartThread();
	g_Character_DB = new MySQLDatabase();

	//连接数据库 
	printf("connect database ip:%s\n", ip.c_str());
	if (!g_Character_DB->Initialize(ip.c_str(), port, username.c_str(), password.c_str(), dataname.c_str(), 5, 16384))
	{
		printf("connect failed\n");
		return FALSE ;
	}
	printf("connect success\n");

	return true ;
}
bool Utf8NameMgr::LoadGuild()
{
	printf("begin load guild  ....\n");
	QueryResult* qr = g_Character_DB->Query( "select guildId, guildName, guildInfo from guilds" );
	if (qr)
	{
		do 
		{
			Field* f = qr->Fetch();
			sGuild* guild = new sGuild;
			guild->guildId = f[0].GetUInt32();
			guild->guildName = f[1].GetString(); 
			guild->guildInfo = f[2].GetString();

			sGuildMap.insert(make_pair(guild->guildId, guild));

		} while (qr->NextRow());
		printf("load guild success....\n");
		delete qr;
	}

	return true ;
}
bool Utf8NameMgr::Load()
{
	LoadCharName();
	LoadGuild();

	return true ;
}
bool Utf8NameMgr::LoadCharName()
{
	printf("begin load char name ....\n");
	QueryResult* qr = g_Character_DB->Query( "select guid, name from characters" );
	if (qr)
	{
		do 
		{
			Field* f = qr->Fetch();
			ui64 guid = f[0].GetUInt64();
			std::string name = f[1].GetString(); 

			CharNameMap.insert(make_pair(guid, name));
			
		} while (qr->NextRow());
		printf("load char name table success....\n");
		delete qr;
	}
	return true ;
}

bool Utf8NameMgr::Change()
{
	printf("begin change char name code to uft8 ....\n");
	std::map<ui64, std::string>::iterator it = CharNameMap.begin();
	for (it; it != CharNameMap.end(); ++it)
	{
		if (it->first)
		{
			it->second = ChAnisToUTF8(it->second);
		}
	}

	std::map<ui32, sGuild*>::iterator it2 = sGuildMap.begin();
	for (it2; it2 != sGuildMap.end(); ++it2)
	{
		if (it2->first)
		{
			it2->second->guildName = ChAnisToUTF8(it2->second->guildName);
			it2->second->guildInfo = ChAnisToUTF8(it2->second->guildInfo);
		}
	}
	printf("change char name code to uft8 success ....\n");
	
	return true ;
}

bool Utf8NameMgr::SaveUtf8Code()
{
	printf("begin save data....\n");
	scoped_sql_transaction_proc sstp( g_Character_DB );

	std::map<ui64, std::string>::iterator it = CharNameMap.begin();
	for (it; it != CharNameMap.end(); ++it)
	{
		if (it->second.length())
		{
			g_Character_DB->TransactionExecute( "Update  characters SET name = '%s' WHERE guid = %u", it->second.c_str(),it->first);	
		}
	}
	

	std::map<ui32, sGuild*>::iterator it2 = sGuildMap.begin();
	for (it2; it2 != sGuildMap.end(); ++it2)
	{
		if (it2->first)
		{
			g_Character_DB->TransactionExecute( "Update  guilds SET guildName = '%s', guildInfo = '%s' WHERE guildId = %u", it2->second->guildName.c_str(),it2->second->guildInfo.c_str(),it2->first);	
		}
	}

	sstp.success();
	printf("save success....\n");

	return true ;
}

void  Utf8NameMgr::ShutMgr()
{
	CharNameMap.clear();
	std::map<ui32, sGuild*>::iterator it2 = sGuildMap.begin();
	for (it2; it2 != sGuildMap.end(); ++it2)
	{
		if (it2->second)
		{
			delete it2->second;
			it2->second = NULL;
		}
	}
	sGuildMap.clear();
	g_Character_DB->Shutdown();
	delete g_Character_DB;
	MyLog::Release();
}