#ifndef __utf8name_h__
#define __utf8name_h__
#include <map>
#include <string>
#include "../../SDBase/Public/TypeDef.h"

class Utf8NameMgr 
{
public:
	Utf8NameMgr();
	~Utf8NameMgr();
	bool InitMgr();
	bool LoadCharName();
	bool Load();
	bool LoadGuild();
	void ShutMgr();
	bool Change();
	bool SaveUtf8Code();
protected:
	struct sGuild
	{
		ui32 guildId;
		std::string guildName;
		std::string guildInfo;
	};
private:
	std::map<ui64, std::string> CharNameMap;
	std::map<ui32, sGuild*> sGuildMap;
};

extern Utf8NameMgr* g_pkUtf8NameMgr;
#endif