#ifndef _GT_2_LS_HEAD
#define _GT_2_LS_HEAD

#include "../../../SDBase/Protocol/PacketDef.h"

namespace MSG_GT2LS
{
	using namespace SDBase;
	enum
	{
		MSG_INIT_REQ,
		MSG_BAN_REQ,
		MSG_MUTE_REQ,
		MSG_USER_ONLINE,
		MSG_USER_OFFLINE,
		MSG_UPDATE_USER_INFO,
	};

	struct stInitReq : public PakHead
	{
		stInitReq() { wProNO = MSG_INIT_REQ; }
		uint8 gateid;
		std::string ip;
		uint16 port;

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << gateid << ip << port; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> gateid >> ip >> port; return cpu; }
	};

	struct stBanReq : public PakHead
	{
		stBanReq() { wProNO = MSG_BAN_REQ; }
		uint32 acct;
		uint32 banned; // unixtime

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << acct << banned; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> acct >> banned; return cpu; }
	};

	struct stMuteReq : public PakHead
	{
		stMuteReq() { wProNO = MSG_MUTE_REQ; }
		uint32 acct;
		uint32 muted; // unixtime

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << acct << muted; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> acct >> muted; return cpu; }
	};

	struct stUserOnline : public PakHead
	{
		stUserOnline() { wProNO = MSG_USER_ONLINE; }
		uint32 acct;

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << acct; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> acct; return cpu; }
	};

	struct stUserOffline : public PakHead
	{
		stUserOffline() { wProNO = MSG_USER_OFFLINE; }
		uint32 acct;

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << acct; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> acct; return cpu; }
	};

	struct stUpdateUserInfo : public PakHead
	{
		stUpdateUserInfo() { wProNO = MSG_UPDATE_USER_INFO; }
		struct user_info {
			uint32 acct;
			uint8 ischild;
			std::string gmflags;
			uint32 muted;
			uint32 banned;
			CPacketSn& Sn(CPacketSn& cps) const {	cps << acct << ischild << gmflags << muted << banned; return cps; }
			CPacketUsn& Usn(CPacketUsn& cpu)	{	cpu >> acct >> ischild >> gmflags >> muted >> banned; return cpu; }
		};
		std::vector<user_info> users;

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << users; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> users; return cpu; }
	};
};


#endif
