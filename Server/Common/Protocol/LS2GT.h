#ifndef _LS_2_GT_HEAD
#define _LS_2_GT_HEAD

#include "../../../SDBase/Protocol/PacketDef.h"

namespace MSG_LS2GT
{
	using namespace SDBase;
	enum
	{
		MSG_KICKOUT_USER,
		MSG_LOGIN_REQ,
	};

	struct stKickoutUser : public PakHead
	{
		stKickoutUser() { wProNO = MSG_KICKOUT_USER; }
		uint32 acct;

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << acct; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> acct; return cpu; }
	};

	struct stLoginReq : public PakHead
	{
		stLoginReq() { wProNO = MSG_LOGIN_REQ; }
		uint32 acct;
		uint32 tmp_password;
		uint8 ischild;
		uint32 lastlogout;
		uint32 chenmitime;
		std::string gmflags;
		uint32 banned;
		uint32 muted;

		CPacketSn& Sn(CPacketSn& cps) const {	PakHead::Sn(cps); cps << acct << tmp_password << ischild << lastlogout << chenmitime << gmflags; return cps; }
		CPacketUsn& Usn(CPacketUsn& cpu)	{	PakHead::Usn(cpu);cpu >> acct >> tmp_password >> ischild >> lastlogout >> chenmitime >> gmflags; return cpu; }
	};
};

#endif // _LS_2_GT_HEAD
