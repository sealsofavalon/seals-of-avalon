
#include <stdio.h>
#include <assert.h>
#include <memory.h>
#include <stdlib.h>
#include "TagStream.h"

bool CTagStream::Read(void* pBuf, unsigned int uiSize)
{
	if( m_uiPos + uiSize <= m_uiSize )
	{
		memcpy(pBuf, m_pBuffer + m_uiPos, uiSize);
		m_uiPos += uiSize;
		return true;
	}

	return false;
}

bool CTagStream::Seek(int offset, int origin)
{
	unsigned int uiNewPos;
	if( origin == SeekBegin )
	{
		uiNewPos = offset;
	}
	else if( origin == SeekCurrent )
	{
		uiNewPos = m_uiPos + offset;
	}
	else if( origin == SeekEnd )
	{
		uiNewPos = m_uiSize + offset;
	}
	else
	{
		assert( false );
	}

	assert( uiNewPos <= m_uiSize );

	m_uiPos = uiNewPos;
	return true;
}

bool CTagStream::ReadFromStream(CStream& stream)
{
	Close();

	if( !stream.Read(&m_uiTag, sizeof(unsigned int)) )
		return false;

	stream.Read(&m_uiSize, sizeof(unsigned int));

	if( !m_static )
	{
		if( m_pBuffer )
		{
			if( m_uiSize > BUFFER_SIZE )
			{
				m_pBuffer = (char*)realloc( m_pBuffer, m_uiSize );
			}
		}
		else
			m_pBuffer = (char*)malloc( BUFFER_SIZE );
	}

	stream.Read(m_pBuffer, m_uiSize);
	return true;
}

bool CTagStream::ReadFromFile( FILE* fp )
{
	Close();

	int ret = fread( &m_uiTag, sizeof( m_uiTag ), 1, fp );
	if( ret != 1 )
		return false;

	ret = fread( &m_uiSize, sizeof( m_uiSize ), 1, fp );
	if( ret != 1 )
		return false;

	if( !m_static )
	{
		if( m_pBuffer )
		{
			if( m_uiSize > BUFFER_SIZE )
			{
				m_pBuffer = (char*)realloc( m_pBuffer, m_uiSize );
			}
		}
		else
			m_pBuffer = (char*)malloc( BUFFER_SIZE );
	}

	ret = fread( m_pBuffer, m_uiSize, 1, fp );
	if( ret != 1 )
		return false;

	return true;
}

bool CTagStream::ReadString( std::string& str )
{
	if( m_uiPos < m_uiSize )
	{
		char* p = m_pBuffer + m_uiPos;
		int n = strlen( p ) + 1;
		if( n > 0 && m_uiPos + n <= m_uiSize )
		{
			str = p;
			m_uiPos += n;
			return true;
		}
	}
	return false;
}

bool CTagStream::ReadModelInstance()
{
	std::string kName;
	if( !ReadString( kName ) )
		return false;

	unsigned int uiFlags = 0;
	unsigned int uiType = 0;
	if(GetVersion() < MakeVersion(1, 0))
	{
		unsigned char ucFlags;
		Read(&ucFlags, sizeof(ucFlags));
		uiFlags = ucFlags;
	}
	else
	{
		Read(&uiFlags, sizeof(uiFlags));
		Read(&uiType, sizeof(uiType));//�������
	}

	unsigned int uiTransportID = 0;
	if(uiType == 1)
	{
		Read(&uiTransportID, sizeof(uiTransportID));
	}


	std::string kNifFilePath;

	unsigned int uiComponentCount;
	static const std::string ms_SceneGraphComponent = "Scene Graph";
	static const std::string ms_TransformationComponent = "Transformation";

	Read(&uiComponentCount, sizeof(unsigned int));

	for( unsigned int i = 0; i < uiComponentCount; i++ )
	{
		std::string kComponentName;
		ReadString(kComponentName);

		if( kComponentName == ms_SceneGraphComponent )
		{
			ReadString(kNifFilePath);
		}
		else if( kComponentName == ms_TransformationComponent )
		{
			Seek( sizeof( float ) + 36 + 12 );
		}
	}
	return true;
}

void CTagStream::Close()
{
	if( !m_static )
	{
		if( m_pBuffer )
		{
			free( m_pBuffer );
			m_pBuffer = NULL;
		}
	}

	m_uiSize = 0;
	m_uiPos = 0;
}

CTagStream::CTagStream( char* static_buff ) : m_pBuffer( static_buff ), m_static( true )
{

}

CTagStream::CTagStream() : m_static( false )
{
	m_pBuffer = (char*)malloc( BUFFER_SIZE );
	assert( m_pBuffer );
}

CTagStream::~CTagStream()
{
	Close();
}