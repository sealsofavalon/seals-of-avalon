
#include "Stream.h"
#include <string>

template<unsigned int TBitCount>
class CBitArray
{
public:
	CBitArray()
	{
		memset(m_uiBits, 0, sizeof(m_uiBits));
	}

	bool Test(unsigned int uiBit) const
	{
		assert(uiBit < TBitCount);
		return (m_uiBits[uiBit/32] & (1 << (uiBit%32))) ? true : false;
	}

	void Set(unsigned int uiBit)
	{
		assert(uiBit < TBitCount);
		m_uiBits[uiBit/32] |= (1 << (uiBit%32));
	}

	void Clear(unsigned int uiBit)
	{
		assert(uiBit < TBitCount);
		m_uiBits[uiBit/32] &= ~(1 << (uiBit%32));
	}

	unsigned int GetBitCount() const { return TBitCount; }
	unsigned int* GetBits() { return m_uiBits; }
	const unsigned int* GetBits() const { return m_uiBits; }
	unsigned int GetEffectiveSize() const
	{
		unsigned int uiCount = 0;
		unsigned int uiBits;
		for(unsigned int i = 0; i < TBitCount/32; ++i)
		{
			uiBits = m_uiBits[i];
			for(int j = 0; j < 32; ++j)
			{
				if(uiBits & (1 << j))
					++uiCount;
			}
		}

		return uiCount;
	}

private:
	unsigned int m_uiBits[TBitCount/32];
};

class CTagStream : public CStream
{
public:
	enum
	{
		BUFFER_SIZE = 32*1024,
	};

	CTagStream();
	CTagStream( char* static_buff );

	virtual ~CTagStream();

	virtual bool Read(void* pBuf, unsigned int uiSize);
	virtual bool Write(const void* pBuf, unsigned int uiSize) { return false; }
	virtual bool Seek(int offset, int origin = SeekCurrent);


	bool ReadString( std::string& str );
	bool ReadFromStream(CStream& stream);
	bool ReadFromFile( FILE* fp );
	bool ReadModelInstance();
	inline unsigned int GetTag() const { return m_uiTag; }
	inline void SetVersion( int v ) { m_uiVersion = v; }
	inline int GetVersion() const { return m_uiVersion; }
	inline int GetSize() const { return m_uiSize - m_uiPos; }

	void Close();

private:
	char* m_pBuffer;
	bool m_static;

	unsigned int m_uiTag;
	int m_uiVersion;
};

#define MakeVersion(major, minor) ( ((unsigned int)major << 8) + (unsigned int)minor )
