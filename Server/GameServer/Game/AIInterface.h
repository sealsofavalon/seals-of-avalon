#ifndef AIINTERFACE_H
#define AIINTERFACE_H
/* platforms that already define M_PI in math.h */
#ifdef M_PI
#undef M_PI
#endif

#define M_PI	   (float)3.14159265358979323846
#define M_H_PI		1.57079632679489661923
#define M_Q_PI		0.785398163397448309615

#define UNIT_MOVEMENT_INTERPOLATE_INTERVAL 200/*750*/ // ms smoother server/client side moving vs less cpu/ less b/w
#define TARGET_UPDATE_INTERVAL 600 // ms
#define oocr 50.0f // out of combat range
#define PLAYER_SIZE 1.2f
#define DISTANCE_TO_SMALL_TO_WALK 1.0f

struct PathResult;

enum AIType
{
	AITYPE_LONER,
	AITYPE_AGRO,
	AITYPE_SOCIAL,
	AITYPE_PET,
	AITYPE_TOTEM,
	AITYPE_GUARDIAN, //we got a master but he cannot control us, we follow and battle oposite factions
};

enum MovementType
{
	MOVEMENTTYPE_NONE,
	MOVEMENTTYPE_RANDOMWP,
	MOVEMENTTYPE_CIRCLEWP,
	MOVEMENTTYPE_WANTEDWP,
	MOVEMENTTYPE_DONTMOVEWP,
	MOVEMENTTYPE_QUEST = 10,
	MOVEMENTTYPE_FORWARDTHANSTOP = 11,
};


/*struct AI_Target
{
	Unit* target;
	int32 threat;
};*/


enum AI_Agent
{
	AGENT_NULL,
	AGENT_MELEE,
	AGENT_RANGED,
	AGENT_FLEE,
	AGENT_SPELL,
	AGENT_CALLFORHELP,
	AGENT_SPELL_ATTACK,
};

enum AI_SpellType
{
	STYPE_NULL,
	STYPE_ROOT,
	STYPE_HEAL,
	STYPE_STUN,
	STYPE_FEAR,
	STYPE_SILENCE,
	STYPE_CURSE,
	STYPE_AOEDAMAGE,
	STYPE_DAMAGE,
	STYPE_SUMMON,
	STYPE_BUFF,
	STYPE_DEBUFF
};

enum AI_SpellTargetType
{
	TTYPE_NULL,
	TTYPE_FRIENDSINGLETARGET,
	TTYPE_FRIENDDESTINATION,
	TTYPE_SOURCE,
	TTYPE_CASTER,
	TTYPE_OWNER,
	TTYPE_ENMYSINGLETARGERT,
	TTYPE_ENMYDESTINATION
};

enum AI_State
{
	STATE_IDLE,
	STATE_ATTACKING,
	STATE_CASTING,
	STATE_FLEEING,
	STATE_FOLLOWING,
	STATE_EVADE,
	STATE_MOVEWP,
	STATE_FEAR,
	STATE_WANDER,
	STATE_STOPPED,
	STATE_SCRIPTMOVE,
	STATE_SCRIPTIDLE
};

enum MovementState
{
	MOVEMENTSTATE_MOVE,
	MOVEMENTSTATE_FOLLOW,
	MOVEMENTSTATE_STOP,
	MOVEMENTSTATE_FOLLOW_OWNER
};

enum CreatureState
{
	STOPPED,
	MOVING,
	ATTACKING
};

/*
0 = 进入战斗
1 = 离开战斗
2 = 受到伤害
3 = 怪物的目标开始施法
4 = 怪物的目标出现招架
5 = 怪物的目标出现躲闪
6 = 怪物的目标出现格担
7 = 怪物的目标出现暴击
8 = 怪物的目标死亡
9 = 死亡
10 = 生物或者玩家出现招架
11 = 生物或者玩家出现躲闪
12 = 生物或者玩家出现格担
13 = 生物或者玩家出现暴击
14 = 生物或者玩家死亡
15 = 帮助的目标死亡
16 = 跟随主人
*/
enum AiEvents
{
	EVENT_NULL = 0,
	EVENT_ENTERCOMBAT,
	EVENT_LEAVECOMBAT,
	EVENT_DAMAGETAKEN,
	EVENT_TARGET_CAST_SPELL,
	EVENT_TARGET_PARRY,
	EVENT_TARGET_DODGE,
	EVENT_TARGET_BLOCK,
	EVENT_TARGET_CRI_ATTACK,
	EVENT_TARGET_SPELL_CRI_HIT,
	EVENT_TARGET_SPELL_HIT,
	EVENT_TARGET_DEAD,
	EVENT_PARRY,
	EVENT_DODGE,
	EVENT_BLOCK,
	EVENT_CRI_ATTACK,
	EVENT_SPELL_CRI_HIT,
	EVENT_SPELL_HIT,
	EVENT_DEAD,
	EVENT_SUPPORT_DEAD,
	EVENT_FEAR,
	EVENT_UNFEAR,
	EVENT_FOLLOWOWNER,
	EVENT_WANDER,
	EVENT_UNWANDER,
	EVENT_UNITDIED,
};

struct SpellEntry;
//enum MOD_TYPES;

struct AI_Spell
{
	~AI_Spell() { autocast_type=(uint32)-1; cooldowntime = 0;}
	uint32 entryId;
	uint16 agent;
	uint16 AIEvent;
	uint32 procChance;
	//int32 procCount;
	//uint32 procCountDB;
	SpellEntry * spell;
	uint8 spellType;
	uint8 spelltargetType;
	uint32 cooldown;
	uint32 cooldowntime;
	uint32 procCount;
	uint32 procCounter;
	float floatMisc1;
	uint32 Misc2;
	float minrange;
	float maxrange;
	uint32 autocast_type;
	bool custom_pointer;
};

bool isGuard(uint32 id);
uint32 getGuardId(uint32 id);

#if ENABLE_SHITTY_STL_HACKS == 1
typedef HM_NAMESPACE::hash_map<Unit*, int32> TargetMap;
#else
namespace HM_NAMESPACE
{
	template <>
	struct hash<Unit*>
	{
		union __vp {
			size_t s;
			Unit* p;
		};

		size_t operator()(Unit* __x) const
		{
			__vp vp;
			vp.p = __x;
			return vp.s;
		}
	};
};

typedef HM_NAMESPACE::hash_map<Unit*, int32, HM_NAMESPACE::hash<Unit*> > TargetMap;
#endif

typedef std::set<Unit*> AssistTargetSet;
typedef std::map<uint32, AI_Spell*> SpellMap;

class SERVER_DECL AIInterface
{
public:
	class ThreatChain;
	ThreatChain* m_threat_chain;

	class ThreatChain
	{
	public:
		static std::map<int, ThreatChain*> s_chains;
		static int s_index;

		static ThreatChain* Find( int id )
		{
			std::map<int, ThreatChain*>::iterator it = s_chains.find( id );
			if( it != s_chains.end() )
				return it->second;
			else
				return NULL;
		}

		static ThreatChain* Create()
		{
			ThreatChain* p = new ThreatChain;
			p->m_id = ++s_index;
			s_chains[p->m_id] = p;
			return p;
		}

		static void Release( int id )
		{
			ThreatChain* tc = Find( id );

			if( tc )
			{
				for( std::map<int, Creature*>::iterator it = tc->m_creatures.begin(); it != tc->m_creatures.end(); ++it )
				{
					Creature* c = it->second;
					if( c->IsValid() )
						c->GetAIInterface()->m_threat_chain = NULL;
				}
				
				delete tc;
			}
			s_chains.erase( id );
		}

		void AddCreature( Creature* c )
		{
			m_creatures[c->GetUniqueIDForLua()] = c;
			c->GetAIInterface()->m_threat_chain = this;
			for( std::map<int, Creature*>::iterator it = m_creatures.begin(); it != m_creatures.end(); ++it )
			{
				Creature* c = it->second;
				if( c->IsValid() )
				{
					for( TargetMap::iterator it2 = c->GetAIInterface()->m_aiTargets.begin(); it2 != c->GetAIInterface()->m_aiTargets.end(); ++it2 )
					{
						c->GetAIInterface()->AttackReaction( it2->first, 1 );
					}
				}
			}
		}
		void RemoveCreature( Creature* c )
		{
			m_creatures.erase( c->GetUniqueIDForLua() );
			c->GetAIInterface()->m_threat_chain = NULL;
		}
		Creature* GetMinHPPctCreature()
		{
			float min = 1.f;
			Creature* minCreature = NULL;
			for( std::map<int, Creature*>::iterator it = m_creatures.begin(); it != m_creatures.end(); ++it )
			{
				Creature* c = it->second;
				if( c->IsValid() && c->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) > 0 )
				{
					float pct = (float)c->GetUInt32Value( UNIT_FIELD_HEALTH ) / (float)c->GetUInt32Value( UNIT_FIELD_MAXHEALTH );
					if( pct <= min )
					{
						minCreature = c;
						min = pct;
					}
				}
			}
			return minCreature;
		}
		size_t GetCreatureCount() const
		{
			return m_creatures.size();
		}
		std::map<int, Creature*>::iterator GetCreatureBegin()
		{
			return m_creatures.begin();
		}
		std::map<int, Creature*>::iterator GetCreatureEnd()
		{
			return m_creatures.end();
		}
		int m_id;
	private:
		std::map<int, Creature*> m_creatures;
	};

	AIInterface();
	~AIInterface();

	// Misc
	void Init(Unit *un, AIType at, MovementType mt);
	void Init(Unit *un, AIType at, MovementType mt, Unit *owner); // used for pets
	void InitForCastle( bool canMove, bool canCombat );
	void InitAfterProtoLoaded();
	Unit *GetUnit() { return m_Unit; }
	Unit *GetPetOwner() { return m_PetOwner; }
	void DismissPet();
	void SetUnitToFollow(Unit* un) { UnitToFollow = un; };
	void SetUnitToFear(Unit* un)  { UnitToFear = un; };
	void SetFollowDistance(float dist) { FollowDistance = dist; };
	void SetUnitToFollowAngle(float angle) { m_fallowAngle = angle; }
	bool setInFront(Unit* target);
	SUNYOU_INLINE Unit* getUnitToFollow() { return UnitToFollow; }
	void setCreatureState(CreatureState state){ m_creatureState = state; }
	SUNYOU_INLINE uint8 getAIState() { return m_AIState; }
	SUNYOU_INLINE uint8 getAIType() { return m_AIType; }
	SUNYOU_INLINE uint8 getCurrentAgent() { return m_aiCurrentAgent; }
	void setCurrentAgent(AI_Agent agent) { m_aiCurrentAgent = agent; }
	uint32	getThreatByGUID(uint64 guid);
	uint32	getThreatByPtr(Unit* obj);
	Unit	*GetMostHated();
	Unit	*GetSecondHated();
	bool	modThreatByGUID(uint64 guid, int32 mod);
	bool	modThreatByPtr(Unit* obj, int32 mod);
	void	RemoveThreatByPtr(Unit* obj);
	SUNYOU_INLINE AssistTargetSet GetAssistTargets() { return m_assistTargets; }
	SUNYOU_INLINE TargetMap *GetAITargets() { return &m_aiTargets; }
	void addAssistTargets(Unit* Friends);
	void delAssistTarget(Unit* Friends);
	void ClearThreatListAndLeaveCombat();
	void ClearHateList();
	void WipeHateList();
	void WipeTargetList();
	bool taunt(Unit* caster, bool apply = true);
	Unit* getTauntedBy();
	bool GetIsTaunted();
	Unit* getSoullinkedWith();
	void SetSoulLinkedWith(Unit* target);
	bool GetIsSoulLinked();
	SUNYOU_INLINE size_t getAITargetsCount() { return m_aiTargets.size(); }
	SUNYOU_INLINE uint32 getOutOfCombatRange() { return m_outOfCombatRange; }
	void setOutOfCombatRange(uint32 val) { m_outOfCombatRange = val; }

	// Spell
	void CastSpell(Unit* caster, SpellEntry *spellInfo, SpellCastTargets targets);
	SpellEntry *getSpellEntry(uint32 spellId);
	SpellCastTargets setSpellTargets(SpellEntry *spellInfo, Unit* target);
	AI_Spell *getSpell( AiEvents = EVENT_NULL);
	bool checkSpell(AI_Spell* sp, bool bSpellAttack = false);

	void addSpellToList(AI_Spell *sp);
	AI_Spell* GetSpellById(uint32 idSpell);
	//don't use this until i finish it !!
//	void CancelSpellCast();

	// Event Handler
	void HandleEvent(uint32 event, Unit* pUnit, uint32 misc1);
	void HandleEventToAssist(uint32 event, Unit* pUnit, uint32 misc1);
	void OnDeath(Object* pKiller);
	void AttackReaction(Unit *pUnit, uint32 damage_dealt, uint32 spellId = 0);
	bool HealReaction(Unit* caster, Unit* victim, uint32 amount);
	void Event_Summon_EE_totem(uint32 summon_duration);
	void Event_Summon_FE_totem(uint32 summon_duration);

	// Update
	void Update(uint32 p_time);
	Unit* FindFriend(float fdis);

	// Movement
	void SendMoveToPacket(float toX, float toY, float toZ, float toO, uint32 time, uint32 MoveFlags);
	//void SendMoveToSplinesPacket(std::list<Waypoint> wp, bool run);
	void MoveTo(float x, float y, float z, float o);
	void MoveToScript(float x, float y, float z, float o);
	void SetReturnPos(float x, float y, float z);
	void ForceMoveTo( float x, float y, float z, float o, bool bRun );
	void ForceStopMove();
	uint32 getMoveFlags();
	bool UpdateMove();
	void SendCurrentMove(Player* plyr/*uint64 guid*/);
	bool StopMovement(uint32 time);
	uint32 getCurrentWaypoint() { return m_currentWaypoint; }
	void changeWayPointID(uint32 oldwpid, uint32 newwpid);
	bool addWayPoint(WayPoint* wp);
	bool saveWayPoints();
	bool showWayPoints(Player* pPlayer, bool Backwards);
	bool hideWayPoints(Player* pPlayer);
	WayPoint* getWayPoint(uint32 wpid);
	void deleteWayPoint(uint32 wpid);
	void deleteWaypoints();
	SUNYOU_INLINE bool hasWaypoints() { return m_waypoints!=NULL; }
	SUNYOU_INLINE void setMoveType(uint32 movetype) { m_moveType = movetype; }
	SUNYOU_INLINE uint32 getMoveType() { return m_moveType; }
	SUNYOU_INLINE void setMoveRunFlag(bool f) { m_moveRun = f; }
	SUNYOU_INLINE bool getMoveRunFlag() { return m_moveRun; }
	void setWaypointToMove(uint32 id) { m_currentWaypoint = id; }
	bool IsFlying();

	void OnPlayerEnterInRange(Player* plr);
	// Calculation
	float _CalcAggroRange(Unit* target);
	void _CalcDestinationAndMove(Unit *target, float dist, bool ignoreCollision = false);
	float _CalcCombatRange(Unit* target, bool ranged);
	float _CalcDistanceFromHome();
	uint32 _CalcThreat(uint32 damage, SpellEntry * sp, Unit* Attacker);

	void _MeleeAttack( AI_Agent agent );
	
	void SetAllowedToEnterCombat(bool val) { m_AllowedToEnterCombat = val; }
	SUNYOU_INLINE bool GetAllowedToEnterCombat(void) { return m_AllowedToEnterCombat; }

	void CheckTarget(Unit* target);
	SUNYOU_INLINE void SetAIState(AI_State newstate) { m_AIState = newstate; }

	// Movement
	bool m_canMove;
	bool m_WayPointsShowing;
	bool m_WayPointsShowBackwards;
	int m_currentWaypoint;
	uint32 m_lastReachWP;
	bool m_moveBackward;
	uint32 m_moveType;
	bool m_moveRun;
	bool m_moveFly;
	bool m_moveSprint;
	CreatureState m_creatureState;
	int GetWayPointsCount()
	{
		if(m_waypoints && !m_waypoints->empty())
			return (int)m_waypoints->size()-1;	/* ignore 0 */
		else
			return 0;
	}
	bool m_canFlee;
	bool m_canCallForHelp;
	bool m_canRangedAttack;
	bool m_canSpellAttack;
	SpellEntry* m_defaultAttackSpell;
	float m_FleeHealth;
	uint32 m_FleeDuration;
	float m_CallForHelpHealth;
	uint32 m_totemspelltimer;
	uint32 m_totemspelltime;
	SpellEntry * totemspell;

	float m_sourceX, m_sourceY, m_sourceZ;
	uint32 m_totalMoveTime;
	SUNYOU_INLINE void AddStopTime(uint32 Time) { m_moveTimer += Time; }
	SUNYOU_INLINE void SetNextSpell(AI_Spell * sp) { m_nextSpell = sp; }
	SUNYOU_INLINE Unit* GetNextTarget() { return m_nextTarget; }
	void SetNextTarget (Unit *nextTarget);
	void AddAIAlliance(uint32 entry);
	void DelAIAlliance(uint32 entry);
	bool IsAIAlliance(uint32 entry);
	void ClearAIAlliance( );

	/*SUNYOU_INLINE void ResetProcCounts()
	{
		AI_Spell * sp;
		for(list<AI_Spell*>::iterator itr = m_spells.begin(); itr != m_spells.end(); ++itr)
				{
					sp = *itr;
					sp->procCount =sp->procCountDB;
				}
	}*/

	Creature * m_formationLinkTarget;
	float m_formationFollowDistance;
	float m_formationFollowAngle;
	uint32 m_formationLinkSqlId;

	void WipeReferences();
	WayPointMap *m_waypoints;
	SUNYOU_INLINE void SetPetOwner(Unit * owner) { m_PetOwner = owner; }
 
	list<AI_Spell*> m_spells;
	AI_Spell* m_usualSpell;
	set<uint32> m_setAIAlliance;


	bool disable_combat;

	bool disable_melee;
	bool disable_ranged;
	bool disable_spell;
	bool disable_targeting;

	bool waiting_for_cooldown;

	uint32 next_spell_time;

	void CheckNextSpell(AI_Spell * sp)
	{
		if(m_nextSpell == sp)
			m_nextSpell = 0;
	}

	void ResetProcCounts();

	SUNYOU_INLINE void SetWaypointMap(WayPointMap * m) { m_waypoints = m; }
	bool m_isGuard;
//	bool m_fastMove;
	void setGuardTimer(uint32 timer) { m_guardTimer = timer; }
	void _UpdateCombat(uint32 p_time);
	uint32 GetThreatTargetCount() const { return m_aiTargets.size(); }
	bool IsInThreatList( Unit* p );
	Unit* GetTargetByThreatIndex( int idx );
	void SetFindPathResult( float x, float y, float z );

	float m_walkSpeed;
	float m_runSpeed;
	TargetMap m_aiTargets;
	bool m_ForceCasting;
	bool m_ForceMoveMode;
	AI_State m_AIState;
	float m_lastFearAngle;
	uint32 m_RespawnTime;

protected:
	bool m_bForceMoving;
	bool m_AllowedToEnterCombat;

	// Update
	void _UpdateTargets();
	void _UpdateMovement(uint32 p_time);
	void _UpdateTimer(uint32 p_time);
	int m_updateAssist;
	int m_updateTargets;
	uint32 m_updateAssistTimer;
	uint32 m_updateTargetsTimer;

	// Misc
	bool firstLeaveCombat;
	Unit* FindTarget();
	Unit* FindTargetForSpell(AI_Spell *sp);
	bool FindFriends(float dist);
	AI_Spell *m_nextSpell;
	Unit* m_nextTarget;
	uint32 m_fleeTimer;
	bool m_hasFleed;
	bool m_hasCalledForHelp;
	uint32 m_outOfCombatRange;

	Unit *m_Unit;
	Unit *m_PetOwner;
	float FollowDistance;
	float FollowDistance_backup;
	float m_fallowAngle;

	//std::set<AI_Target> m_aiTargets;
	AssistTargetSet m_assistTargets;
	AIType m_AIType;
	AI_Agent m_aiCurrentAgent;

	Unit* tauntedBy; //This mob will hit only tauntedBy mob.
	bool isTaunted;
	Unit* soullinkedWith; //This mob can be hitten only by soullinked unit
	bool isSoulLinked;


	// Movement

	float m_flySpeed;
	float m_destinationX;
	float m_destinationY;
	float m_destinationZ;
	
	float m_nextPosX;
	float m_nextPosY;
	float m_nextPosZ;

	//Return position after attacking a mob
	float m_returnX;
	float m_returnY;
	float m_returnZ;

	float m_lastFollowX;
	float m_lastFollowY;
	//typedef std::map<uint32, WayPoint*> WayPointMap;
	Unit *UnitToFollow;
	Unit *UnitToFollow_backup;//used unly when forcing creature to wander (blind spell) so when effect wears off we can follow our master again (guardian)
	Unit *UnitToFear;
	uint32 m_timeToMove;
	uint32 m_timeMoved;
	uint32 m_moveTimer;
	uint32 m_FearTimer;
	uint32 m_WanderTimer;

	MovementType m_MovementType;
	MovementState m_MovementState;
	uint32 m_guardTimer;
	int32 m_currentHighestThreat;

	float m_NextPathX;
	float m_NextPathY;
	float m_NextPathZ;
	Unit* m_FindPathTarget;
	float m_FindPathDistance;
	uint32 m_lastFindPathTime;
public:
	bool m_is_in_instance;
	bool skip_reset_hp;

	void WipeCurrentTarget();
};
#endif
