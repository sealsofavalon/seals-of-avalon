#include "StdAfx.h"
#ifdef COLLISION
#define LOS_CHECKS 1
//#define LOS_ONLY_IN_INSTANCE 1
#endif
initialiseSingleton(AIInterfaceManager);
#ifdef WIN32
#define HACKY_CRASH_FIXES 1		// SEH stuff
#endif

#include "ObjectMgr.h"
#include "ScriptMgr.h"


#include "LuaInterface.h"



struct tgHate
{
	tgHate()
	{
		guid = 0;
		hate = 0;
	}

	tgHate( const tgHate& t )
	{
		guid = t.guid;
		hate = t.hate;
	}

	tgHate& operator = ( const tgHate& t )
	{
		guid = t.guid;
		hate = t.hate;
		return *this;
	}

	int guid;
	int hate;
};

bool sortHateList( const tgHate& p1, const tgHate& p2 )
{
	if (p1.guid && p2.guid)
	{
		if (p1.hate >= p2.hate)
		{
			return true;
		}
	}



	return false;
}

AIInterfaceManager::AIInterfaceManager()
{



	sScriptMgr.GetLuaState()->GetGlobals().Register("AIHandleEvent", LUA_HandleEvent);
	sScriptMgr.GetLuaState()->GetGlobals().Register("SetNextSpel", LUA_SetNextSpell);
	sScriptMgr.GetLuaState()->GetGlobals().Register("SetNextTarget", LUA_SetNextTarget);
	sScriptMgr.GetLuaState()->GetGlobals().Register("ModifyModThreat", LUA_ModifyModThreat);
	sScriptMgr.GetLuaState()->GetGlobals().Register("GetThreatSize", LUA_GetThreatSize);
	sScriptMgr.GetLuaState()->GetGlobals().Register("GetThreatGuidByIndex", LUA_GetThreatGuidByIndex);
	sScriptMgr.GetLuaState()->GetGlobals().Register("ClearThreatLis", LUA_ClearThreatList);
	sScriptMgr.GetLuaState()->GetGlobals().Register("GetHp", LUA_GetHp);
	sScriptMgr.GetLuaState()->GetGlobals().Register("GetPercentageHp", LUA_GetPercentageHp);

	sScriptMgr.GetLuaState()->GetGlobals().Register("AddSpell", LUA_AddSpell);
	sScriptMgr.GetLuaState()->GetGlobals().Register("ModfySpellColddown", LUA_ModfySpellColddown);
	sScriptMgr.GetLuaState()->GetGlobals().Register("ModfyUsualSpell", LUA_ModfyUsualSpell);
	sScriptMgr.GetLuaState()->GetGlobals().Register("CreatureSay", LUA_CreatureSay);
	sScriptMgr.GetLuaState()->GetGlobals().Register("ModifyFleeHealth", LUA_ModifyFleeHealth);
	sScriptMgr.GetLuaState()->GetGlobals().Register("ModifyCallForHelpHealth", LUA_ModifyCallForHelpHealth);
	sScriptMgr.GetLuaState()->GetGlobals().Register("AddAlliance", LUA_AddAlliance);
	sScriptMgr.GetLuaState()->GetGlobals().Register("DelAlliance", LUA_DelAlliance);
	sScriptMgr.GetLuaState()->GetGlobals().Register("ClearAlliance", LUA_ClearAlliance);

	sScriptMgr.GetLuaState()->GetGlobals().Register("RegisterAIUpdateEvent", LUA_RegisterAIUpdateEvent);
	sScriptMgr.GetLuaState()->GetGlobals().Register("ModifyAIUpdateEvent", LUA_ModifyAIUpdateEvent);
	sScriptMgr.GetLuaState()->GetGlobals().Register("RemoveAIUpdateEvent", LUA_RemoveAIUpdateEvent);

	sScriptMgr.GetLuaState()->GetGlobals().Register("GetNumber", LUA_GetNumber);
	sScriptMgr.GetLuaState()->GetGlobals().Register("SetNumber", LUA_SetNumber);
	sScriptMgr.GetLuaState()->GetGlobals().Register("CreateNumber", LUA_CreateNumber);
	sScriptMgr.GetLuaState()->GetGlobals().Register("DelNumber", LUA_DelNumber);

	sScriptMgr.GetLuaState()->GetGlobals().Register("GetCreatureInRange", LUA_GetCreatureInRange);
	sScriptMgr.GetLuaState()->GetGlobals().Register("GetCurTime", LUA_GetCurTime);
	sScriptMgr.GetLuaState()->GetGlobals().Register("SetCreatureDead", LUA_SetCreatureDead);

	sScriptMgr.GetLuaState()->GetGlobals().Register("Reborn", LUA_Relife);

	sScriptMgr.GetLuaState()->GetGlobals().Register("FindUnitCountInRange", LUAFindUnitCountInRange);
	sScriptMgr.GetLuaState()->GetGlobals().Register("FindNearestUnitInRange", LUAFindNearestUnitInRange);
	sScriptMgr.GetLuaState()->GetGlobals().Register("LUAFindNearestUnitByEntryInRange", LUAFindNearestUnitByEntryInRange);
	sScriptMgr.GetLuaState()->GetGlobals().Register("FollowUnitByGuid", LUAFollowUnitByGuid);
	sScriptMgr.GetLuaState()->GetGlobals().Register("FollowUnitByEntry", LUAFollowUnitByEntry);

	sScriptMgr.GetLuaState()->GetGlobals().Register("CreatureMoveTo", LUACreatureMoveTo);
	sScriptMgr.GetLuaState()->GetGlobals().Register("CreatureMoveTo2", LUACreatureMoveTo2);
	sScriptMgr.GetLuaState()->GetGlobals().Register("GetPosX", LUAGetPosX);
	sScriptMgr.GetLuaState()->GetGlobals().Register("GetPosY", LUAGetPosY);
	sScriptMgr.GetLuaState()->GetGlobals().Register("GetPosZ", LUAGetPosZ);
	sScriptMgr.GetLuaState()->GetGlobals().Register("GetOrientation", LUAGetOrientation);
	sScriptMgr.GetLuaState()->GetGlobals().Register("SetReturnPos", LUASetReturnPos);
	sScriptMgr.GetLuaState()->GetGlobals().Register("SetAIState", LUASetAIState);
	/*LUASpawnCreature*/
	sScriptMgr.GetLuaState()->GetGlobals().Register("SpawnCreature", LUASpawnCreature);
	sScriptMgr.GetLuaState()->GetGlobals().Register("LoadScript", LUALoadScript);

	//m_ttp.startup( 8 );
}

AIInterfaceManager::~AIInterfaceManager()
{

}


void AIInterfaceManager::RelifeCreature(int guid)
{
	AIInterface* pAI = FindAIIterface(guid);
	if (pAI)
	{
		if (pAI->GetUnit() && pAI->GetUnit()->IsCreature())
		{

			Creature* p = (Creature*)(pAI->GetUnit());
				
			if (!p->isAlive())
			{
				LocationVector vc = p->GetPosition();
				vc.z += 0.1f;
				p->Resurrect(vc);
			}

		}
		
	}
}


bool AIInterfaceManager::DelNumber(int guid, const char* sz)
{	
	MAPGUID::iterator itrfind = m_mapNewObj.find(guid);
	if (itrfind == m_mapNewObj.end() )
	{
		return false;
	}

	if (!sz)
	{
		m_mapNewObj.erase(itrfind);
	}
	else
	{
		MAPDOUBLE::iterator it = itrfind->second.find(sz);
		if (it != itrfind->second.end())
		{
			itrfind->second.erase(it);
			
		}

		if (itrfind->second.empty())
		{
			m_mapNewObj.erase(itrfind);
		}
		return true;
	}
	
	return false;
}

bool AIInterfaceManager::SetNewObj(int guid, const char* sz, double nValue)
{
	int c = 0;
	if (nValue < -60)
	{
		c ++;
	}

	MAPDOUBLE::iterator itrfind;
	int n = 0;
	MAPGUID::iterator it = m_mapNewObj.find(guid);
	if (it != m_mapNewObj.end())
	{
		itrfind = it->second.find(sz);
		if (itrfind != it->second.end())
		{
			 itrfind->second = nValue;
			 return true;
		}

	}
	else
	{
		n = nValue;
	}


	
	return false;
}

bool AIInterfaceManager::CreateNewObj(int guid, const char* sz, double f)
{

	int n = 0;

	MAPDOUBLE::iterator itrfind;
	double temf = f;
	MAPGUID::iterator it = m_mapNewObj.find(guid);
	if (it != m_mapNewObj.end())
	{
		itrfind = it->second.find(sz);
		if (itrfind != it->second.end())
		{
			return false;
		}
		else
		{
			std::string str = sz;
			it->second.insert(MAPDOUBLE::value_type(str, temf));
		}
	}
	else 
	{
		MAPDOUBLE newMap;
		std::string str = sz;
		newMap.insert(MAPDOUBLE::value_type(str, temf));
		m_mapNewObj.insert(MAPGUID::value_type(guid, newMap));
	}


	return false;

}

double AIInterfaceManager::FindNewObj(int guid, const char* sz)
{

	double f = -99.f;
	MAPDOUBLE::iterator itrfind;
	MAPGUID::iterator it = m_mapNewObj.find(guid);
	if (it != m_mapNewObj.end())
	{
		itrfind = it->second.find(sz);
		if (itrfind != it->second.end())
		{
			f =  itrfind->second;
		}

	}
	return f;
}

void AIInterfaceManager::SetCreatureDead(int guid, int nDead)
{
	AIInterface* pAi = FindAIIterface(guid);
	if (pAi)
	{
		if (pAi->GetUnit())
		{
			Creature* p = (Creature*)(pAi->GetUnit());
			if (nDead == 1)
			{
				p->m_corpseEvent = true;
				p->SetUInt32Value(UNIT_FIELD_DEATH_STATE, DEAD);
			}

		}

	}
}

bool AIInterfaceManager::ClearHateList(int guid)
{
	AIInterface* pAI = FindAIIterface(guid);
	if(pAI)
	{
		pAI->ClearHateList();
		return true;
	}

	return false;
}

bool AIInterfaceManager::WipeHateList(int guid)
{
	AIInterface* pAI = FindAIIterface(guid);
	if(pAI)
	{
		pAI->WipeHateList();
		return true;
	}
	return false;
}

bool AIInterfaceManager::WipeTargetList(int guid)
{

	AIInterface* pAI = FindAIIterface(guid);
	if(pAI)
	{
		pAI->WipeTargetList();
		return true;
	}

	return false;

}

void AIInterfaceManager::Update()
{
	//m_cbm.poll();
}

void AIInterfaceManager::PushFindPathResult( int AIInterfaceKey, float x, float y, float z )
{
	//m_cbm.add_cb( &AIInterfaceManager::SetPathResult, this, AIInterfaceKey, x, y, z );
}

void AIInterfaceManager::SetPathResult( int AIInterfaceKey, float x, float y, float z )
{
	/*
	AIInterface* pAI = FindAIIterface( AIInterfaceKey );
	if( pAI )
	{
		pAI->SetFindPathResult( x, y, z );
	}
	*/
}

void AIInterfaceManager::PushTask( task* p )
{
	//m_ttp.push_task( p );
}

AIInterface* AIInterfaceManager::FindAIIterface(int guid)
{
	AIInterface* pAI = NULL;
	Creature*p = objmgr.FindCreatureForLua( guid);
	if (p)
	{
		pAI = p->GetAIInterface();
	}

	return pAI;
}

uint32 AIInterfaceManager::GetEntry(int guidSrc)
{
	AIInterface* pAi = FindAIIterface(guidSrc);
	if (pAi)
	{
		
		if (pAi->GetUnit())
		{
			return pAi->GetUnit()->GetEntry();
		}
	}

	return 0;
}

bool AIInterfaceManager::Say(int guid, const char* sz, int range)
{
	AIInterface* pAi = FindAIIterface(guid);
	if (pAi)
	{
		if(pAi->GetUnit()&&pAi->GetUnit()->IsCreature())
		{
			Creature* p = (Creature*)(pAi->GetUnit());
			if( p && p->IsValid() )
			{
				p->SendChatMessage( range? 14 : 12, 0, sz );
				return true;
			}
		}
		
	}
	return false;
}



float AIInterfaceManager::GetPercentageHp(int guid)
{
	AIInterface* pAi = FindAIIterface(guid);
	float healthPercent = -1;
	if (pAi)
	{
		if (pAi->GetUnit())
		{
			uint32 cur = pAi->GetUnit()->GetUInt32Value(UNIT_FIELD_HEALTH) + 1;
			uint32 max = pAi->GetUnit()->GetUInt32Value(UNIT_FIELD_MAXHEALTH) + 1;
			 healthPercent = float(cur) / float(max);
		}	
	}
	return healthPercent;
}

int AIInterfaceManager::GetHp(int guid)
{
	AIInterface* pAi = FindAIIterface(guid);
	int health = -1;
	if (pAi)
	{
		if (pAi->GetUnit())
		{
			health= pAi->GetUnit()->GetUInt32Value(UNIT_FIELD_HEALTH) + 1;
		}
	}
	return health;
}





bool AIInterfaceManager::SetNextSpell(int guidSrc, uint32 NextSpell)
{

	AIInterface* pAIIterface = FindAIIterface(guidSrc);
	if (pAIIterface)
	{
		AI_Spell* pNextSpell = NULL;
		list<AI_Spell*>::iterator it  = pAIIterface->m_spells.begin();
		for (; it != pAIIterface->m_spells.end(); it ++)
		{
			if((*it)->entryId == NextSpell)
			{
				pNextSpell = (*it);
			}
			
		}
		if (pNextSpell)
		{
			pAIIterface->SetNextSpell(pNextSpell);
			return true;
		}
	}


	return false;
}

bool AIInterfaceManager::SetNextTarget(int guidSrc, int guidTarget)
{
	AIInterface* pAi =FindAIIterface(guidSrc);
	if (!pAi)
	{
		return false;
	}
	Unit* pUnit =pAi->GetUnit()->GetMapMgr()->GetUnit(guidTarget);
	if (pUnit)
	{
		pAi->SetNextTarget(pUnit);
		return true;
	}

	return false;

}


bool AIInterfaceManager::ModifyModThreat(int guidSrc, int guidTarget, uint32 Modify)
{
	AIInterface* pAI = FindAIIterface(guidSrc);
	if (pAI)
	{
		pAI->modThreatByGUID(guidTarget, Modify);
	}

	return true;

}

bool AIInterfaceManager::HandleEvent(int guid, AiEvents en, int guidTarget, uint32 misc1)
{

	AIInterface* pAI = FindAIIterface(guid);

	if (pAI)
	{
		Unit* pUnit =pAI->GetUnit()->GetMapMgr()->GetUnit(guidTarget);
		
		if (pUnit)
		{
			pAI->HandleEvent(en, pUnit, misc1);
		}
		else
		{
			pAI->HandleEvent(en, 0, misc1);
		}
		return true;		
	}
	return false;
}

bool AIInterfaceManager::AddAi_Spell(int guid , uint32 entryId, uint16 agent,uint32 procChance, uint8 spellType,
				 uint8 SpellTargetType, uint32 cooldown, uint32 cooldowntime, uint32 proCount, uint32 proCounter, float floatMisc1, uint32 Misc2,
				 float minRange, float maxRange, uint32 autocast_type, bool custom_pointer)
{
	AIInterface* pAi = FindAIIterface(guid);
	if (pAi)
	{
		SpellEntry * spell = pAi->getSpellEntry(entryId);

		if (spell)
		{
			AI_Spell * pAiSpell = new AI_Spell;
			pAiSpell->entryId = entryId;
			pAiSpell->agent = agent;
			pAiSpell->procChance = procChance;
			pAiSpell->spell = spell;
			pAiSpell->spellType = spellType;
			pAiSpell->spelltargetType = SpellTargetType;
			pAiSpell->cooldown = cooldown;
			pAiSpell->cooldowntime = cooldowntime;
			pAiSpell->procCount = proCount;
			pAiSpell->procCounter = proCounter;
			pAiSpell->floatMisc1 = floatMisc1;
			pAiSpell->Misc2 = Misc2;
			pAiSpell->minrange = minRange;
			pAiSpell->maxrange = maxRange;
			pAiSpell->autocast_type = autocast_type;
			pAiSpell->custom_pointer = custom_pointer;

			pAi->addSpellToList(pAiSpell);
			return true;
		}
	}
	return false;
}

int AIInterfaceManager::GetThreatSize(int guid )
{
	AIInterface* pAI = FindAIIterface(guid);
	int nSize = 0;
	if (pAI)
	{
		nSize = pAI->GetThreatTargetCount();
	}
	return nSize;
}


bool AIInterfaceManager::SetUsualSpell(int guid, uint32 idSpell)
{
	AIInterface* pAi = FindAIIterface(guid);
	if (pAi)
	{
		AI_Spell* pSpell = pAi->GetSpellById(idSpell);
		if (pSpell)
		{
			pAi->m_usualSpell = pSpell; 
			return true;
		}
	}

	return false;
}

bool AIInterfaceManager::ModifySpellColdown(int guid, uint32 idSpell , uint32 cooldown)
{
	AIInterface* pAi = FindAIIterface(guid);
	if (pAi)
	{
		AI_Spell* pSpell = pAi->GetSpellById(idSpell);
		if (pSpell)
		{
			pSpell->cooldown = cooldown;
			return true;
		}
	}

	return false;
}


bool AIInterfaceManager::ModifyFleeHealth(int guid, float fhealth)
{
	AIInterface* pAi = FindAIIterface(guid);
	if (pAi)
	{
		pAi->m_FleeHealth = fhealth;
		if (fhealth > 0)
		{
			pAi->m_canFlee = true;
		}
		else
		{
			pAi->m_canFlee = false;
		}
		return true;
	}

	return false;
}

bool AIInterfaceManager::ModifyCallForHealpHealth(int guid, float fhealth)
{
	AIInterface* pAi = FindAIIterface(guid);
	if (pAi)
	{
		pAi->m_CallForHelpHealth = fhealth;
		return true;
	
	}

	return false;
}


bool AIInterfaceManager::AddAlliance(int guid, uint32 entry)
{
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		p->AddAIAlliance(entry);
	}

	return true;
}

bool AIInterfaceManager::DelAlliance(int guid, uint32 entry)
{

	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		p->DelAIAlliance(entry);
	}

	return true;
}

bool AIInterfaceManager::ClearAlliance(int guid)
{
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		p->ClearAIAlliance();
	}

	return true;
}

int AIInterfaceManager::GetCurTime()
{
	return UNIXTIME;
}

bool AIInterfaceManager::FollowUnitByGuid(int guid , float fDis, int life, int FollowGuid, float followDis)
{
	AIInterface* p = FindAIIterface(guid);
	AIInterface* pTarget = FindAIIterface(FollowGuid);
	if (p && pTarget)
	{
		if (p->GetUnit()->GetMapMgr() == pTarget->GetUnit()->GetMapMgr() && pTarget->GetUnit()->CalcDistance(p->GetUnit()) < fDis)
		{
			Unit* pUnit = pTarget->GetUnit();
			if (life == 0 ||( life == -1 && !pUnit->isAlive()) || (life > 1&& pUnit->isAlive()))
			{
				p->SetUnitToFollow(pUnit);
				p->SetFollowDistance(followDis);
				return true;
			}
		}

	}
	return false;

}
bool AIInterfaceManager::FollowUnitByEntry(int guid, float fDis, int life,  int nEntry, float followDis)
{
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		uint32 Target = 0;
		Target = FindNearestUnitByEntryInRange(guid, fDis, life, nEntry);
		if (Target)
		{
			AIInterface* pFollow = FindAIIterface(Target);
			if (pFollow)
			{
				pFollow->SetUnitToFollow(p->GetUnit());
				pFollow->SetFollowDistance(followDis);
				return true;
			}


		}
	}
	return false;
}

bool AIInterfaceManager::FollowPlayerByGuildName(int guid,const char* szGuildName, float fDis, int life, float followDis)
{
	int n = 0;
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		Unit* pNearestUnit = NULL;
		float NearestDistance = fDis;
		Object::InRangeSet::iterator itBegin = p->GetUnit()->GetInRangeSetBegin();
		Object::InRangeSet::iterator itEnd = p->GetUnit()->GetInRangeSetEnd();
		for (Object::InRangeSet::iterator  it = itBegin; it != itEnd; ++ it)
		{
			float CurrentDistance = 0.f;
			Player* pPlayer = NULL;
			if((*it)->IsPlayer())
			{
				pPlayer = (Player*)(*it);
			}
			if(pPlayer && pPlayer->GetGuild()->GetGuildName() == szGuildName)
			{
				Unit* pUnit = (Unit*)(*it);

				CurrentDistance = pUnit->GetDistanceSq(p->GetUnit());
				if ( CurrentDistance> fDis)
				{
					continue;
				}


				if (life == 0 ||( life == -1 && !pUnit->isAlive()) || (life > 1&& pUnit->isAlive()))
				{
					if (NearestDistance > CurrentDistance)
					{
						pNearestUnit = pUnit;
						NearestDistance = CurrentDistance;
					}
				}
			}
		}
		if (pNearestUnit)
		{
			return pNearestUnit->GetUniqueIDForLua();
		}
	}

	return 0;
}

uint32 AIInterfaceManager::FindNearestUnitByEntryInRange(int guid, uint32 nEntry , float fDis, int life)
{
	int n = 0;
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		Unit* pNearestUnit = NULL;
		float NearestDistance = fDis;
		Object::InRangeSet::iterator itBegin = p->GetUnit()->GetInRangeSetBegin();
		Object::InRangeSet::iterator itEnd = p->GetUnit()->GetInRangeSetEnd();
		for (Object::InRangeSet::iterator  it = itBegin; it != itEnd; ++ it)
		{
			float CurrentDistance = 0.f;
			if ((*it)->IsUnit() &&(*it)->GetEntry() == nEntry )
			{
				Unit* pUnit = (Unit*)(*it);
			
					CurrentDistance = pUnit->GetDistanceSq(p->GetUnit());
					if ( CurrentDistance> fDis)
					{
						continue;
					}


					if (life == 0 ||( life == -1 && !pUnit->isAlive()) || (life > 1&& pUnit->isAlive()))
					{
						if (NearestDistance > CurrentDistance)
						{
							pNearestUnit = pUnit;
							NearestDistance = CurrentDistance;
						}
					}
					

			}

		}
		if (pNearestUnit)
		{
			return pNearestUnit->GetUniqueIDForLua();
		}
	}

	return 0;
}

uint32 AIInterfaceManager::FindNearestUnitInRange(int guid, uint32 Type , float fDis, int life, int nfriend)
{
	int n = 0;
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		Unit* pNearestUnit = NULL;
		float NearestDistance = fDis;
		Object::InRangeSet::iterator itBegin = p->GetUnit()->GetInRangeSetBegin();
		Object::InRangeSet::iterator itEnd = p->GetUnit()->GetInRangeSetEnd();
		for (Object::InRangeSet::iterator  it = itBegin; it != itEnd; ++ it)
		{
			float CurrentDistance = 0.f;
			if ((*it)->IsUnit())
			{
				Unit* pUnit = (Unit*)(*it);
				if (Type)
				{
					if (Type == 1 && !pUnit->IsCreature())
					{
						continue;
					}
					else if(Type == 2 && !pUnit->IsPlayer())
					{
						continue;
					}
				}

				CurrentDistance = pUnit->GetDistanceSq(p->GetUnit());
				if ( CurrentDistance> fDis)
				{
					continue;
				}
				if((isAttackable(pUnit, p->GetUnit() ) && nfriend == 2) || (isFriendly(pUnit, p->GetUnit() )) && nfriend == 1)
				{

					if (life == 0 ||( life == -1 && !pUnit->isAlive()) || (life > 1&& pUnit->isAlive()))
					{
						if (NearestDistance > CurrentDistance)
						{
							pNearestUnit = pUnit;
							NearestDistance = CurrentDistance;
						}
					}
				}

			}

		}
		if (pNearestUnit)
		{
			return pNearestUnit->GetUniqueIDForLua();
		}
	}

	return 0;

}

int AIInterfaceManager::FindUnitCountInRange(int guid, uint32 Type , float fDis, int life, int nfriend)
{
	int n = 0;
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		Object::InRangeSet::iterator itBegin = p->GetUnit()->GetInRangeSetBegin();
		Object::InRangeSet::iterator itEnd = p->GetUnit()->GetInRangeSetEnd();
		for (Object::InRangeSet::iterator  it = itBegin; it != itEnd; ++ it)
		{
			if ((*it)->IsUnit())
			{
				Unit* pUnit = (Unit*)(*it);
				if (Type)
				{
					if (Type == 1 && !pUnit->IsCreature())
					{
						continue;
					}
					else if(Type == 2 && !pUnit->IsPlayer())
					{
						continue;
					}
				}
				if (pUnit->GetDistanceSq(p->GetUnit()) > fDis)
				{
					continue;
				}

				if((isAttackable(pUnit, p->GetUnit() ) && nfriend == 2) || (isFriendly(pUnit, p->GetUnit() )) && nfriend == 1)
				{

					if (life == 0 || (life == -1 && !pUnit->isAlive()) || (life > 0 && pUnit->isAlive()))
					{
						n ++;
					}
				}

			}

		}

	}

	return n;
}

void AIInterfaceManager::CreatureMoveTo(int guid, float MoveX, float MoveY, float MoveZ,  float o,int nRun, int forceMove)
{
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		bool bRun = false;
		if (nRun)
		{
			bRun = true;
		}

		Unit* pUnit = p->GetUnit();
		float fMoveX = pUnit->GetPositionX() + MoveX;
		float fMoveY = pUnit->GetPositionY() + MoveY;
		float fMoveZ = pUnit->GetPositionZ() + MoveZ;
		if (forceMove)
		{
			p->ForceMoveTo(fMoveX, fMoveY, fMoveZ, o, bRun);
		}
		else
		{
			p->MoveToScript(fMoveX, fMoveY, fMoveZ, o);
		}
	}

}

void AIInterfaceManager::CreatureMoveTo2(int guid, float DesX, float DesY, float DesZ,float o, int forceMove)
{
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{

		if (forceMove)
		{
			p->ForceMoveTo(DesX, DesY, DesZ, o, false);
		}
		else
		{
			p->MoveToScript(DesX, DesY, DesZ, o);
		}
	}
}
float AIInterfaceManager::GetPosX(int guid)
{
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		return p->GetUnit()->GetPositionX();
	}

	return 0;
}

float AIInterfaceManager::GetPosY(int guid)
{
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		return p->GetUnit()->GetPositionY();
	}

	return 0;
}

float AIInterfaceManager::GetPosZ(int guid)
{
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		return p->GetUnit()->GetPositionZ();
	}

	return 0;
}

void AIInterfaceManager::SetReturnPos(int guid, float x, float y, float z)
{
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		return p->SetReturnPos(x, y, z);
	}
}


void AIInterfaceManager::SetAIState(int guid, int State)
{
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		p->m_AIState = (AI_State)State;
	}

}

float AIInterfaceManager::GetOrientation(int guid)
{
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		return p->GetUnit()->GetOrientation();
	}

	return 0;
}

float AIInterfaceManager::calcRadAngle(float Position1X, float Position1Y, float Position2X, float Position2Y)
{
	double dx = double(Position2X-Position1X);
	double dy = double(Position2Y-Position1Y);
	double angle=0.0;

	// Calculate angle
	if (dx == 0.0)
	{
		if (dy == 0.0)
			angle = 0.0;
		else if (dy > 0.0)
			angle = M_PI * 0.5/*/ 2.0*/;
		else
			angle = M_PI * 3.0 * 0.5/*/ 2.0*/;
	}
	else if (dy == 0.0)
	{
		if (dx > 0.0)
			angle = 0.0;
		else
			angle = M_PI;
	}
	else
	{
		if (dx < 0.0)
			angle = atan(dy/dx) + M_PI;
		else if (dy < 0.0)
			angle = atan(dy/dx) + (2*M_PI);
		else
			angle = atan(dy/dx);
	}

	// Return
	return float(angle);
}
float AIInterfaceManager::calcDistance(float Position1X, float Position1Y, float Position2X, float Position2Y)
{

	LocationVector Pos(Position1X, Position1Y, 0);
	return Pos.Distance(Position2X,Position2Y, 0);
}

int AIInterfaceManager::SpawnCreature(int guid, uint32 entry, float fx, float fy, float fz, float o, int Respawn)
{

	int CreatureGuid = 0;
	AIInterface* pAi = FindAIIterface(guid);
	if (pAi)
	{
		CreatureInfo* info = CreatureNameStorage.LookupEntry(entry);
		if (info)
		{
			CreatureSpawn* sp = new CreatureSpawn;
			info->GenerateModelId(&sp->displayid);
			sp->entry = entry;
			sp->form = 0;
			sp->x = fx;
			sp->y = fy;
			sp->z = fz;
			sp->o = o;
			sp->emote_state =0;
			sp->flags = 911;
			sp->factionid = 0;//proto->Faction;
			sp->bytes=0;
			sp->bytes2=0;
			sp->stand_state = 0;
			sp->channel_spell=sp->channel_target_creature=sp->channel_target_go=0; 
			Creature* p =pAi->GetUnit()->GetMapMgr()->CreateCreature(entry);
			if( p->Load(sp, (uint32)NULL, NULL) )
			{

				CreatureGuid = p->GetUniqueIDForLua();
				p->m_noRespawn = !Respawn;
				p->PushToWorld( pAi->GetUnit()->GetMapMgr() );

			}
			else
			{
				delete p;
				delete sp;
			}
		}
	}

	return CreatureGuid;

}
int AIInterfaceManager::GetCreatureInRange(int guid,uint32 entry, float fDis, int life)
{
	int n = 0;
	AIInterface* p = FindAIIterface(guid);
	if (p)
	{
		Object::InRangeSet::iterator itBegin = p->GetUnit()->GetInRangeSetBegin();
		Object::InRangeSet::iterator itEnd = p->GetUnit()->GetInRangeSetEnd();
		for (Object::InRangeSet::iterator  it = itBegin; it != itEnd; ++ it)
		{
			if ((*it)->IsUnit())
			{
				Unit* pUnit = (Unit*)(*it);
				if (pUnit->GetDistanceSq(p->GetUnit()) > fDis)
				{
					continue;
				}

				if(pUnit->GetEntry() == entry)
				{
					if (life == 0 ||(life == -1 &&!pUnit->isAlive()) || ( life >0 && pUnit->isAlive()))
					{
						n ++;
					}
				}

			}

		}
		
	}

	return n;
}

void AIInterfaceManager::RegisterAIUpdateEvent(int guid, uint32 frequency)
{
	AIInterface* pAi = FindAIIterface(guid);
	if (pAi)
	{
		if (pAi->GetUnit()->IsCreature())
		{
			Creature* p = (Creature*)(pAi->GetUnit());
			p->GetScript()->RegisterAIUpdateEvent(frequency);
		}

	}
}

void AIInterfaceManager::ModifyAIUpdateEvent(int guid, uint32 newfrequency)
{
	AIInterface* pAi = FindAIIterface(guid);
	if (pAi)
	{
		if (pAi->GetUnit()->IsCreature())
		{
			Creature* p = (Creature*)(pAi->GetUnit());
			p->GetScript()->ModifyAIUpdateEvent(newfrequency);
		}
	}
}

void AIInterfaceManager::RemoveAIUpdateEvent(int guid)
{
	AIInterface* pAi = FindAIIterface(guid);
	if (pAi)
	{
		if (pAi->GetUnit()->IsCreature())
		{
			Creature* p = (Creature*)(pAi->GetUnit());
			p->GetScript()->RemoveAIUpdateEvent();
		}

	}
}

int AIInterfaceManager::GetThreatGuidByIndex(int guid, uint32 nIndex)
{	
	int nThread = 0;
	AIInterface* pAi = FindAIIterface(guid);
	if (nIndex > pAi->GetThreatTargetCount())
	{
		return -1;
	}

	if (pAi)
	{
		std::vector<tgHate> VCGUID;

		TargetMap* listAItarget = pAi->GetAITargets();
		TargetMap::iterator it = listAItarget->begin();

		for (; it != listAItarget->end();  ++ it)
		{
			int nTemp = it->second;
			Unit* p = (Unit*)(it->first);
			int guid = p->GetGUID();

			tgHate tg;
			tg.guid = guid;
			tg.hate = nTemp;
			VCGUID.push_back(tg);

		}

		if (VCGUID.size() > nIndex)
		{
			return false;
		}

		std::sort(VCGUID.begin(), VCGUID.end(), sortHateList);
		
		return VCGUID[nIndex].guid;

	}

	return 0;

}


int LUA_GetCurTime(LuaState* s)
{
	int n = AIInterfaceManager::getSingletonPtr()->GetCurTime();
	s->PushInteger(n);
	return 1;
}
int LUA_GetCreatureInRange(LuaState* s)
{
	LuaStack args(s);
	int n = 0;
	if (args[1].IsInteger() && args[2].IsInteger() && args[3].IsNumber() && args[4].IsInteger())
	{
		 n = AIInterfaceManager::getSingletonPtr()->GetCreatureInRange(args[1].GetInteger(), args[2].GetInteger(), args[3].GetNumber(), args[4].GetInteger());
	}

	s->PushInteger(n);
	return 1;
}

int LUA_RegisterAIUpdateEvent(LuaState* s)
{

	LuaStack args(s);
	if (args[1].IsInteger() && args[2].IsInteger())
	{
		AIInterfaceManager::getSingletonPtr()->RegisterAIUpdateEvent(args[1].GetInteger(), args[2].GetInteger());
	}
	return 0;
}

int LUA_ModifyAIUpdateEvent(LuaState* s)
{
	LuaStack args(s);

	if (args[1].IsInteger() && args[2].IsInteger())
	{
		AIInterfaceManager::getSingletonPtr()->ModifyAIUpdateEvent(args[1].GetInteger(), args[2].GetInteger());
	}

	return 0;
}

int LUA_RemoveAIUpdateEvent(LuaState* s)
{
	LuaStack args(s);

	if (args[1].IsInteger() )
	{
		AIInterfaceManager::getSingletonPtr()->RemoveAIUpdateEvent(args[1].GetInteger());
	}

	return 0;
}



int LUA_AddAlliance(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsInteger()&& args[2].IsInteger())
	{
		AIInterfaceManager::getSingletonPtr()->AddAlliance(args[1].GetInteger(), args[2].GetInteger());

	}

	return 0;
}

int LUA_DelAlliance(LuaState* s)
{
	LuaStack args(s);

	if (args[1].IsInteger()&& args[2].IsInteger())
	{
		AIInterfaceManager::getSingletonPtr()->DelAlliance(args[1].GetInteger(), args[2].GetInteger());

	}

	return 0;
}

int LUA_ClearAlliance(LuaState* s)
{
	LuaStack args(s);


	if (args[1].IsInteger())
	{
		AIInterfaceManager::getSingletonPtr()->ClearAlliance(args[1].GetInteger());

	}
	return 0;
}

int LUA_ClearThreatList(LuaState* s)
{
	LuaStack args( s );
	if (args[1].IsNumber())
	{
		AIInterfaceManager::getSingletonPtr()->WipeTargetList(args[1].GetNumber());
	}

	return 0;

}
int LUAFindUnitCountInRange(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber())
	{
		int nCount =AIInterfaceManager::getSingletonPtr()->FindUnitCountInRange(args[1].GetNumber(), args[2].GetNumber(), args[3].GetNumber(), 
			args[4].GetNumber(), args[5].IsNumber());
		s->PushInteger(nCount);

		return 1;
	}
	return 0;
}
int LUAFindNearestUnitInRange(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber())
	{
		uint32 guid = AIInterfaceManager::getSingletonPtr()->FindNearestUnitInRange(args[1].GetNumber(), args[2].GetNumber(), args[3].GetNumber(), 
			args[4].GetNumber(), args[5].IsNumber());

		s->PushInteger(guid);
		return 1;
	}

	return 0;
}

int LUAFindNearestUnitByEntryInRange(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber() )
	{
		uint32 guid = AIInterfaceManager::getSingletonPtr()->FindNearestUnitByEntryInRange(args[1].GetNumber(), 
			args[2].GetNumber(), args[3].GetNumber(), 
			args[4].GetNumber());

		s->PushInteger(guid);

		return 1;
	}

	return 0;
}

int LUAFollowUnitByGuid(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber() )
	{
		if (args[5].IsNumber())
		{
			AIInterfaceManager::getSingletonPtr()->FollowUnitByGuid(args[1].GetNumber(), 
				args[2].GetNumber(), args[3].GetNumber(), 
				args[4].GetNumber(), args[5].GetNumber());
		}
		else
		{
			AIInterfaceManager::getSingletonPtr()->FollowUnitByGuid(args[1].GetNumber(), 
				args[2].GetNumber(), args[3].GetNumber(), 
				args[4].GetNumber());
		}
	}

	return 0;
}

int LUAFollowPlayerByGuildName(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsString() && args[3].IsNumber() && args[4].IsNumber() )
	{
		if (args[5].IsNumber())
		{
			AIInterfaceManager::getSingletonPtr()->FollowPlayerByGuildName(args[1].GetNumber(), 
				args[2].GetString(), args[3].GetNumber(), 
				args[4].GetNumber(), args[5].GetNumber());
		}
		else
		{
			AIInterfaceManager::getSingletonPtr()->FollowPlayerByGuildName(args[1].GetNumber(), 
				args[2].GetString(), args[3].GetNumber(), 
				args[4].GetNumber());
		}

	}
	return 0;

}
int LUAFollowUnitByEntry(LuaState* s)
{

	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber() )
	{

		float fx = args[2].GetNumber();
		float fy = args[3].GetNumber();
		float fz = args[4].GetNumber();

		if (args[5].IsNumber())
		{
			AIInterfaceManager::getSingletonPtr()->FollowUnitByEntry(args[1].GetNumber(), 
				args[2].GetNumber(), args[3].GetNumber(), 
				args[4].GetNumber(), args[5].GetNumber());
		}
		else
		{
			AIInterfaceManager::getSingletonPtr()->FollowUnitByEntry(args[1].GetNumber(), 
				args[2].GetNumber(), args[3].GetNumber(), 
				args[4].GetNumber());
		}
		
	}
	return 0;
}
int LUA_SetNextSpell(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber())
	{
		AIInterfaceManager::getSingletonPtr()->SetNextSpell(args[1].GetNumber() , args[2].GetNumber());
	}

	return 0;
}

int LUA_SetNextTarget(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber())
	{
		AIInterfaceManager::getSingletonPtr()->SetNextTarget(args[1].GetNumber() , args[2].GetNumber());
	}

	return 0;
}

int LUA_ModifyModThreat(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber()&& args[3].IsNumber())
	{
		AIInterfaceManager::getSingletonPtr()->ModifyModThreat(args[1].GetNumber() , args[2].GetNumber(), args[3].GetNumber());
	}
	
	return 0;
}

int LUA_GetThreatSize(LuaState* s)
{
	LuaStack args(s);
	int n = -1;
	if (args[1].IsNumber())
	{
		 n = AIInterfaceManager::getSingletonPtr()->GetThreatSize(args[1].GetNumber() );
	}


	s->PushInteger(n);
	return 1;


}

int LUA_GetThreatGuidByIndex(LuaState* s)
{
	LuaStack args(s);
	int n = -1;

	if (args[1].IsNumber() && args[2].IsNumber())
	{
		 n = AIInterfaceManager::getSingletonPtr()->GetThreatGuidByIndex(args[1].GetNumber(), args[2].GetNumber() );

	}
	s->PushInteger(n);
	return 1;

}

int LUA_AddSpell(LuaState* s)
{


	LuaStack args(s);
	if (args[1].IsInteger() && args[2].IsInteger() && args[3].IsInteger() && args[4].IsInteger() && args[5].IsInteger() && args[6].IsInteger() && args[7].IsInteger()
		&& args[8].IsInteger() && args[9].IsInteger() && args[9].IsInteger() && 
		 args[10].IsInteger() && args[11].IsNumber() && 
		args[12].IsInteger() && args[13].IsNumber() &&args[14].IsNumber() && 
		args[15].IsInteger() && args[16].IsBoolean())
	{
		AIInterfaceManager::getSingletonPtr()->AddAi_Spell(args[1].GetInteger(), args[2].GetInteger(), args[3].GetInteger() , args[4].GetInteger() , args[5].GetInteger(),
			args[6].GetInteger(), args[7].GetInteger(), args[8].GetInteger(), args[9].GetInteger(), args[10].GetInteger(), args[11].GetNumber(),
			args[12].GetInteger() , args[13].GetNumber(), args[14].GetNumber(), args[15].GetInteger(), args[16].GetBoolean());
	}

	return 0;
}

int LUA_HandleEvent(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsInteger())
	{
		int guid = 0;
		uint32 misc1 = 0;
		if (args[3].IsNumber())
		{
			guid = args[3].GetNumber();
		}

		if (args[4].IsNumber())
		{
			misc1 = args[4].GetNumber();
		} 

		AIInterfaceManager::getSingletonPtr()->HandleEvent(args[1].GetNumber(), (AiEvents)args[2].GetInteger(), guid, misc1 );
	}


	return 0;
}

int LUA_GetHp(LuaState* s)
{
	LuaStack args(s);
	int n = -1;
	if (args[1].IsNumber())
	{
		n = AIInterfaceManager::getSingletonPtr()->GetHp(args[1].GetNumber());
	}
	s->PushInteger(n);
	return 1;
	
}


int LUA_ModfySpellColddown(LuaState* s)
{
	LuaStack args(s);

	int n = -1;
	if (args[1].IsNumber() && args[2].IsNumber() && args[3].IsNumber())
	{
		n = AIInterfaceManager::getSingletonPtr()->ModifySpellColdown(args[1].GetNumber(), args[2].GetNumber(), args[3].GetNumber());

	}

	s->PushInteger(n);
	return 1;

}

int LUA_ModfyUsualSpell(LuaState*s)
{	
	LuaStack args(s);



	if (args[1].IsNumber() && args[2].IsNumber())
	{
		 AIInterfaceManager::getSingletonPtr()->SetUsualSpell(args[1].GetNumber(), args[2].GetNumber());

	}

	return 0;

}

int LUA_ModifyFleeHealth(LuaState* s)
{

	LuaStack args(s);

	if (args[1].IsInteger() && args[2].IsNumber())
	{
		AIInterfaceManager::getSingletonPtr()->ModifyFleeHealth(args[1].GetInteger(), args[2].GetNumber());

	}

	return 0;
}

int LUA_Relife(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsInteger())
	{
		AIInterfaceManager::getSingletonPtr()->RelifeCreature(args[1].GetInteger());
	}

	return 0;
}

int LUA_ModifyCallForHelpHealth(LuaState* s)
{

	LuaStack args(s);

	if (args[1].IsNumber() && args[2].IsNumber())
	{
		AIInterfaceManager::getSingletonPtr()->ModifyCallForHealpHealth(args[1].GetNumber(), args[2].GetNumber());

	}

	return 0;
}


int LUA_CreatureSay(LuaState* s)
{
	LuaStack args( s );
	if( args[1].IsInteger() && args[2].IsString() && args[3].IsInteger() )
	{
		AIInterfaceManager::getSingletonPtr()->Say(args[1].GetInteger() , args[2].GetString() ,args[3].GetInteger());
	}
	return 0;
}

int LUA_GetEntry(LuaState* s)
{
	LuaStack args(s);
	int n = -1;
	if (args[1].IsInteger())
	{
		n = AIInterfaceManager::getSingletonPtr()->GetEntry(args[1].GetInteger());

	}
	s->PushInteger(n);
	return 1;
}


int LUA_GetNumber(LuaState* s)
{

	LuaStack args(s);
	double f = -1.f;
	if (args[1].IsInteger() && args[2].IsString() )
	{
		 f = AIInterfaceManager::getSingletonPtr()->FindNewObj(args[1].GetInteger(), args[2].GetString());
	}
	s->PushNumber( f );

	return 1;

}


int LUA_SetNumber(LuaState* s)
{

	LuaStack args(s);
	if (args[1].IsInteger() && args[2].IsString() && args[3].IsNumber())
	{
		 AIInterfaceManager::getSingletonPtr()->SetNewObj(args[1].GetInteger(), args[2].GetString(), args[3].GetNumber());
	}

	return 0;

}

int LUA_GetPercentageHp(LuaState* s)
{
	LuaStack args(s);
	float f = -1.f;
	if (args[1].IsNumber())
	{
		f = AIInterfaceManager::getSingletonPtr()->GetPercentageHp(args[1].GetNumber());
	}
	s->PushNumber(f);
	return 1;

}


int LUA_DelNumber(LuaState* s)
{

	const char* sz = NULL;
	LuaStack args(s);
	int n = 0;
	if (args[1].IsInteger())
	{
		if (args[2].IsString())
		{
			sz = args[2].GetString();
		}

		AIInterfaceManager::getSingletonPtr()->DelNumber(args[1].GetInteger(), sz);
	}

	return 0;
}

int LUA_CreateNumber(LuaState* s)
{

	LuaStack args(s);

	int n = -1;
	if (args[1].IsInteger() && args[2].IsString())
	{
		bool b = false;
		if (args[3].IsNumber())
		{
			if (args[2].GetString() == "SpawnEntry0")
			{
				long l = args[3].GetNumber();
			}
			b = AIInterfaceManager::getSingletonPtr()->CreateNewObj(args[1].GetInteger() , args[2].GetString(), args[3].GetNumber());
		}
		else
		{
			b = AIInterfaceManager::getSingletonPtr()->CreateNewObj(args[1].GetInteger() , args[2].GetString());
		}
		if (b)
		{
			n = 0;
		}
		
	}


	s->PushInteger(n);
	return 1;
}

int LUA_SetCreatureDead(LuaState* s)
{
	LuaStack args(s);
	
	if (args[1].IsInteger() && args[2].IsInteger())
	{
		AIInterfaceManager::getSingletonPtr()->SetCreatureDead(args[1].GetInteger() , args[2].GetInteger());

	}
	return 0;
	
}
/*
void CreatureMoveTo(int guid, float MoveX, float MoveY, float MoveZ, float o,int nRun int forceMove);
void CreatureMoveTo2(int guid, float DesX, float DesY, float DesZ, float o, int forceMove);
float GetPosX(int guid);
float GetPosY(int guid);
float GetPosZ(int guid);
float GetOrientation(int guid);
*/
int LUAGetPosX(LuaState* s)
{
	LuaStack args(s);
	float f = 0.f;
	if (args[1].IsNumber())
	{
		f = AIInterfaceManager::getSingletonPtr()->GetPosX(args[1].GetNumber());
	}

	s->PushNumber(f);
	return 1;
}

int LUAGetPosY(LuaState* s)
{
	LuaStack args(s);
	float f = 0.f;
	if (args[1].IsNumber())
	{
		f = AIInterfaceManager::getSingletonPtr()->GetPosY(args[1].GetNumber());
	}

	s->PushNumber(f);
	return 1;
}

int LUAGetPosZ(LuaState* s)
{
	LuaStack args(s);
	float f = 0.f;
	if (args[1].IsNumber())
	{
		f = AIInterfaceManager::getSingletonPtr()->GetPosZ(args[1].GetNumber());
	}

	s->PushNumber(f);
	return 1;
}

int LUASetReturnPos(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber() &&args[3].IsNumber() &&args[4].IsNumber())
	{
		AIInterfaceManager::getSingletonPtr()->SetReturnPos(args[1].GetNumber() ,args[2].GetNumber() ,args[3].GetNumber() ,args[4].GetNumber());
	}

	return 0;

}

int LUASetAIState(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber())
	{
		AIInterfaceManager::getSingletonPtr()->SetAIState(args[1].GetNumber(), args[2].GetNumber());
	}

	return 0;
}

int LUAGetOrientation(LuaState* s)
{
	LuaStack args(s);
	float f = 0.f;
	if (args[1].IsNumber())
	{
		f = AIInterfaceManager::getSingletonPtr()->GetOrientation(args[1].GetNumber());
	}

	s->PushNumber(f);

	return 1;
}

int LUASpawnCreature(LuaState* s)
{
	LuaStack args(s);
	int nId = 0;
	if( args[1].IsInteger() && args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() )
	{
		if (args[7].IsNumber())
		{
			nId = AIInterfaceManager::getSingletonPtr()->SpawnCreature(args[1].GetInteger(), args[2].GetNumber(), args[3].GetNumber(), args[4].GetNumber(),
				args[5].GetNumber(), args[6].GetNumber(), args[7].GetNumber());
		}	
		else
		{
			nId = AIInterfaceManager::getSingletonPtr()->SpawnCreature(args[1].GetInteger(), args[2].GetNumber(), args[3].GetNumber(), args[4].GetNumber(),
				args[5].GetNumber(), args[6].GetNumber());
		}
	}
	s->PushNumber(nId);
	return 1;
}

int LUACreatureMoveTo(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber()&& 
		args[5].IsNumber() && args[6].IsNumber())
	{
		if (args[7].IsNumber())
		{
			AIInterfaceManager::getSingletonPtr()->CreatureMoveTo(args[1].GetNumber(), 
				args[2].GetNumber(), args[3].GetNumber(), 
				args[4].GetNumber(), args[5].GetNumber(),
				args[6].GetNumber(), args[7].GetNumber());
		}
		else
		{
			AIInterfaceManager::getSingletonPtr()->CreatureMoveTo(args[1].GetNumber(), 
				args[2].GetNumber(), args[3].GetNumber(), 
				args[4].GetNumber(), args[5].GetNumber(),
				args[6].GetNumber());
		}
	}

	return 0;

}
int LUACreatureMoveTo2(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsNumber() && args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber()&& 
		args[5].IsNumber())
	{
		if (args[6].IsNumber())
		{
			AIInterfaceManager::getSingletonPtr()->CreatureMoveTo2(args[1].GetNumber(), 
				args[2].GetNumber(), args[3].GetNumber(), 
				args[4].GetNumber(), args[5].GetNumber(),
				args[6].GetNumber());
		}
		else
		{
			AIInterfaceManager::getSingletonPtr()->CreatureMoveTo2(args[1].GetNumber(), 
				args[2].GetNumber(), args[3].GetNumber(), 
				args[4].GetNumber(), args[5].GetNumber());
		}
	}

	return 0;
}



int LUACalcAngle(LuaState* s)
{
	LuaStack args(s);
	float f = 0;
	if (args[1].IsNumber() && args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber())
	{

		f = AIInterfaceManager::getSingletonPtr()->calcRadAngle(args[1].GetNumber(), 
			args[2].GetNumber(), args[3].GetNumber(), 
			args[4].GetNumber());

	}
	s->PushNumber(f);
	return 1;
}
int LUACalcDistance(LuaState* s)
{
	LuaStack args(s);
	float f = 0;
	if (args[1].IsNumber() && args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber())
	{

		f = AIInterfaceManager::getSingletonPtr()->calcDistance(args[1].GetNumber(), 
			args[2].GetNumber(), args[3].GetNumber(), args[4].GetNumber());

	}
	s->PushNumber(f);
	return 1;
}


int LUALoadScript(LuaState* s)
{
	LuaStack args(s);
	if (args[1].IsString())
	{
		ScriptMgr::getSingletonPtr()->LoadString(args[1].GetString());
	}
	return 0;

}