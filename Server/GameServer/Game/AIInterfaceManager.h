#ifndef AIINTERFACEMANAGER_H
#define AIINTERFACEMANAGER_H
#include "AIInterface.h"
#include "../../../new_common/Source/utilities/call_back.h"
#include "../../../new_common/Source/utilities/task_thread_pool.h"

typedef std::map<uint64, AIInterface*> MAPINTERFACE;
using namespace LuaPlus;

typedef std::vector<ui64> VCGUID; 
class AIInterfaceManager : public Singleton<AIInterfaceManager>
{

public:
	typedef std::map<std::string, double> MAPDOUBLE;
	typedef std::map<int, MAPDOUBLE> MAPGUID;
public:
	friend class AIInterface;
	AIInterfaceManager();
	~AIInterfaceManager();

	bool HandleEvent(int guid, AiEvents en, int guidTarget,  uint32 misc1);

	bool AddAi_Spell(int guid , uint32 entryId, uint16 agent, uint32 procChance, uint8 spellType,
		uint8 SpellTargetType, uint32 cooldown, uint32 cooldowntime, uint32 proCount, uint32 proCounter, float floatMisc1, uint32 Misc2,
		float minRange, float maxRange, uint32 autocast_type, bool custom_pointer);

	bool ModifySpellColdown(int guid, uint32 idSpell , uint32 cooldown);
	bool SetUsualSpell(int guid, uint32 idSpell);
	bool ClearHateList(int guid);
	bool WipeHateList(int guid);
	bool WipeTargetList(int guid);
	bool SetNextSpell(int guidSrc, uint32 NextSpell);
	bool SetNextTarget(int guidSrc, int guidTarget);
	bool ModifyModThreat(int guidSrc, int guidTarget, uint32 Modify);
	uint32 GetEntry(int guidSrc);
	int GetThreatSize(int guid );
	int GetThreatGuidByIndex(int guid, uint32 nIndex);
	bool AddAlliance(int guid, uint32 entry);
	bool DelAlliance(int guid, uint32 entry);
	bool ClearAlliance(int guid);
	int GetCreatureInRange(int guid,uint32 entry,  float fDis, int life );//life = 0 全部的 life = -1 死亡的 //life = 1活着的 
	int SpawnCreature(int guid, uint32 entry, float fx, float fy, float fz, float o, int Respawn = 0);
	float calcRadAngle(float fx, float fy, float fx1, float fy1);
	float calcDistance(float fx, float fy, float fx1, float fy1);
	void CreatureMoveTo(int guid, float MoveX, float MoveY, float MoveZ, float o,int nRun,int forceMove = 0);
	void CreatureMoveTo2(int guid, float DesX, float DesY, float DesZ, float o, int forceMove = 0);
	float GetPosX(int guid);
	float GetPosY(int guid);
	float GetPosZ(int guid);
	void SetReturnPos(int guid, float x, float y, float z);
	void SetAIState(int guid, int State);
	float GetOrientation(int guid);
	int FindUnitCountInRange(int guid, uint32 Type , float fDis, int life, int nfriend); //Type = 0 所有 1 怪物 2 Player // friend = 0 所有 firend = 1友好 firend = 2敌方
	uint32 FindNearestUnitInRange(int guid, uint32 Type , float fDis, int life, int nfriend);
	uint32 FindNearestUnitByEntryInRange(int guid, uint32 nEntry , float fDis, int life);
	bool FollowUnitByGuid(int guid , float fDis, int life, int FollowGuid, float followDis = 3.0f);
	bool FollowUnitByEntry(int guid,float fDis, int life,  int nEntry, float followDis = 3.0f);
	bool FollowPlayerByGuildName(int guid,const char* szGuildName, float fDis, int life, float followDis = 3.0f);
	int GetCurTime();
	void RegisterAIUpdateEvent(int guid, uint32 frequency);
	void ModifyAIUpdateEvent(int guid, uint32 newfrequency);
	void RemoveAIUpdateEvent(int guid);
	void Update();
	void PushFindPathResult( int AIInterfaceKey, float x, float y, float z );
	void SetPathResult( int AIInterfaceKey, float x, float y, float z );
	void PushTask( task* p );


	AIInterface* FindAIIterface(int guid);
	float GetPercentageHp(int guid);
	int GetHp(int guid);
	bool ModifyFleeHealth(int guid, float fhealth);
	bool ModifyCallForHealpHealth(int guid, float fhealth);
	bool Say(int guid, const char* sz, int range);
	double FindNewObj(int guid, const char* sz);
	bool CreateNewObj(int guid, const char* sz, double f = 0);
	bool SetNewObj(int guid, const char* sz, double nValue);


	bool DelNumber(int guid, const char* sz);
	void RelifeCreature(int guid);
	void SetCreatureDead(int guid, int nDead);

protected:
	call_back_mgr m_cbm;
	task_thread_pool m_ttp;
	MAPINTERFACE m_mapInterface;
	MAPGUID		 m_mapNewObj;

};

int LUA_ModifyFleeHealth(LuaState* s);
int LUA_ModifyCallForHelpHealth(LuaState* s);
int LUA_HandleEvent(LuaState* s);
int LUA_SetNextSpell(LuaState* s);
int LUA_SetNextTarget(LuaState* s);
int LUA_ModifyModThreat(LuaState* s);
int LUA_GetThreatSize(LuaState* s);
int LUA_GetThreatGuidByIndex(LuaState* s);
int LUA_ClearThreatList(LuaState* s);
int LUA_GetPercentageHp(LuaState* s);
int LUA_GetHp(LuaState* s);
int LUA_AddSpell(LuaState* s);
int LUA_ModfySpellColddown(LuaState* s);
int LUA_ModfyUsualSpell(LuaState*s);
int LUA_CreatureSay(LuaState* s);
int LUA_GetEntry(LuaState* s);
int LUA_AddAlliance(LuaState* s);
int LUA_DelAlliance(LuaState* s);
int LUA_ClearAlliance(LuaState* s);
int LUA_RegisterAIUpdateEvent(LuaState* s);
int LUA_ModifyAIUpdateEvent(LuaState* s);
int LUA_RemoveAIUpdateEvent(LuaState* s);
int LUA_GetNumber(LuaState* s);
int LUA_SetNumber(LuaState* s);
int LUA_CreateNumber(LuaState* s);
int LUA_DelNumber(LuaState* s);
int LUA_GetCreatureInRange(LuaState* s);
int LUA_GetCurTime(LuaState* s);
int LUA_Relife(LuaState* s);
int LUA_SetCreatureDead(LuaState* s);
int LUAFindUnitCountInRange(LuaState* s);
int LUAFindNearestUnitInRange(LuaState* s);
int LUAFindNearestUnitByEntryInRange(LuaState* s);
int LUAFollowUnitByGuid(LuaState* s);
int LUAFollowUnitByEntry(LuaState* s);
int LUAFollowPlayerByGuildName(LuaState* s);
int LUACreatureMoveTo(LuaState* s);
int LUACreatureMoveTo2(LuaState* s);
int LUAGetPosX(LuaState* s);
int LUAGetPosY(LuaState* s);
int LUAGetPosZ(LuaState* s);
int LUASetReturnPos(LuaState* s);
int LUASetAIState(LuaState* s);
int LUAGetOrientation(LuaState* s);
int LUASpawnCreature(LuaState* s);
int LUACalcAngle(LuaState* s);
int LUACalcDistance(LuaState* s);
int LUALoadScript(LuaState* s);

/*

*/





#define sAIInterfaceManager AIInterfaceManager::getSingleton()





























#endif