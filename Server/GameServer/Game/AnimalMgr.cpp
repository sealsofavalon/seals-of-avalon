#include "StdAfx.h"

int8 likeanimal[12][12] = 
{
	//      鼠   牛   虎   兔   龙   蛇   马   羊   猴   鸡   狗   猪
	/*鼠*/  0,   1,   0,  -1,   1,   0,  -1,  -1,   1,   0,   0,   0,
	/*牛*/  1,   0,   0,   0,   0,   1,  -1,  -1,   0,   1,  -1,   0,
	/*虎*/  0,   0,   0,   0,   0,  -1,   1,   0,  -1,   0,   1,   0,
	/*兔*/ -1,   0,   0,   0,   0,   0,   0,   1,   0,  -1,   1,   1,
	/*龙*/  1,   0,   0,   0,   0,   0,   0,   0,   1,   1,  -1,   0,
	/*蛇*/  0,   1,  -1,   0,   0,   0,   0,   0,   0,   1,   0,  -1,
	/*马*/ -1,  -1,   1,   0,   0,   0,   0,   1,   0,   0,   1,   0,
	/*羊*/  0,  -1,   0,   1,   0,   0,   1,   0,   0,   0,  -1,   1,
	/*猴*/  1,   0,  -1,   0,   1,   0,   0,   0,   0,   0,   0,  -1,
	/*鸡*/  0,   1,   0,  -1,   1,   1,   0,   0,   0,   0,   0,   0,
	/*狗*/  0,  -1,   1,   1,  -1,   0,   0,   0,   0,   0,   0,   1,
	/*猪*/  0,   0,   0,   1,   1,  -1,   0,   0,  -1,   0,   0,   1,
};

const uint32 animalspell_min = 5; 
uint32 animalspell[] =
{
	2012,/*-5*/ 
	2011,/*-4*/ 
	2010,/*-3*/ 
	2009,/*-2*/ 
	2008,/*-1*/ 
	0,/*0*/  
	2001,/*1*/  
	2002,/*2*/  
	2003,/*3*/  
	2004,/*4*/  
	2005,/*5*/  
	2006,/*6*/  
	2007,/*7*/  
};

initialiseSingleton( AnimalMgr );

int8 AnimalMgr::GetRelation(uint8 animal1, uint8 animal2)
{
	if(animal1 > 12 || animal2 > 12)
		return 0;
	return likeanimal[animal1-1][animal2-1];
}

uint32 AnimalMgr::GetRelationSpell(int32 relationvalue)
{
	if(relationvalue < -5 || relationvalue >= 7)
	{
		ASSERT(0);
		return 0;
	}
	return animalspell[relationvalue+5];
}

void AnimalMgr::OnAniamlRelation(SubGroup* sg)
{
	uint32 spell = 0;
	uint32 animalspell_index = 0;
	uint8 sameanimal[12] = {0};
	int8 nclass = -1;
	bool bsameclass = false;

	//4人同生肖，与第5人相生        value = 5

	//4人同生肖，与第5人相克        value = -5

	//5人同生肖：天罡队形			value = 6

	//5人同生肖同职业：地煞队形		value = 7

	//for(int i = 0; i < pGroup->GetSubGroupCount(); i++)
	{
		//SubGroup* sg = pGroup->GetSubGroup(i);

		for( GroupMembersSet::iterator itr = sg->GetGroupMembersBegin(); itr != sg->GetGroupMembersEnd(); ++itr )
		{
			PlayerInfo* plyinfo = *itr;
			if( plyinfo && plyinfo->m_loggedInPlayer )
			{
				plyinfo->m_loggedInPlayer->RemoveAura(plyinfo->m_loggedInPlayer->m_animalgroupspell);

				if(plyinfo->m_loggedInPlayer->GetSession()->_loggingOut)
				{
				 continue;
				}
				if (plyinfo->m_loggedInPlayer->GetAnimal() == 0)
				{
					continue;
				}
				uint32 nAnimalNum = 0;
				int32 nAnimalValue = 0;
				uint32 nSameClass = 0;
				PlayerInfo* pSameOne = NULL;
				PlayerInfo* pUnSameOne = NULL;

// 				if(nclass == -1)
// 					nclass = plyinfo->m_loggedInPlayer->getClass();
// 				if(nclass != plyinfo->m_loggedInPlayer->getClass())
// 					bsameclass = false;
// 				sameanimal[plyinfo->m_loggedInPlayer->GetAnimal()]++;
				for( GroupMembersSet::iterator itr2 = sg->GetGroupMembersBegin(); itr2 != sg->GetGroupMembersEnd(); ++itr2 )
				{
					PlayerInfo* plyinfo2 = *itr2;
					uint32 relation = 0;
					if( plyinfo2 && plyinfo2 != plyinfo && plyinfo2->m_loggedInPlayer && !plyinfo2->m_loggedInPlayer->GetSession()->_loggingOut )
					{
						if(plyinfo2->m_loggedInPlayer->GetAnimal() == 0)
							continue;
						if(plyinfo2->m_loggedInPlayer->GetAnimal() == plyinfo->m_loggedInPlayer->GetAnimal())
							nAnimalNum++;
						if(plyinfo2->cl == plyinfo->cl)
							nSameClass++;
						relation = GetRelation(plyinfo->m_loggedInPlayer->GetAnimal(), plyinfo2->m_loggedInPlayer->GetAnimal());
						if(relation > 0)
							pSameOne = plyinfo2;
						else if(relation < 0)
							pUnSameOne = plyinfo2;
						nAnimalValue += relation;
					}
				}

				if(nAnimalNum == 3)
				{
					if(nAnimalValue == 1)
						nAnimalValue = 5;
					else if(nAnimalValue == -1)
						nAnimalValue = -5;
				}
				if(nAnimalNum == 5)
				{
					if(nSameClass == 5)
						nAnimalValue = 7;
					else
						nAnimalValue = 6;
				}

				if(nAnimalValue < -5 || nAnimalValue > 12)
				{
					MyLog::log->error("错误的AnimalValue[%d]", nAnimalValue);
					return;
				}
				uint32 nspellid = GetRelationSpell(nAnimalValue);

				if(nspellid)
				{
					plyinfo->m_loggedInPlayer->m_animalgroupspell = nspellid;
					SpellCastTargets targets( plyinfo->m_loggedInPlayer->GetGUID() );
					SpellEntry* spellEntry = dbcSpell.LookupEntry( nspellid );
					if( spellEntry == NULL )
						continue;;
					Spell* spell;
					spell = new Spell( plyinfo->m_loggedInPlayer, spellEntry, true, 0 );
					spell->p_caster = plyinfo->m_loggedInPlayer;
					spell->u_caster = (Unit*)plyinfo->m_loggedInPlayer;
					spell->prepare( &targets );			
				}	
			}
		}
	}
// 	
// 	
// 
// 	
}