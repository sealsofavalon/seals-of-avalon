#ifndef __ANIMALMGR_H
#define __ANIMALMGR_H
class AnimalMgr : public Singleton <AnimalMgr>
{
public:
	AnimalMgr(){}
	~AnimalMgr(){}

	int8 GetRelation(uint8 animal1, uint8 animal2);
	uint32 GetRelationSpell(int32 relationvalue);

	void OnAniamlRelation(SubGroup* sg);
};

#define sAnimalMgr AnimalMgr::getSingleton()

#endif
