#include "StdAfx.h"

#include "../../SDBase/Protocol/C2S.h"
#include "../../SDBase/Protocol/S2C.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "QuestMgr.h"

void WorldSession::HandleAreaTriggerOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stArea_Trigger MsgRecv;
	packet >> MsgRecv;
	_HandleAreaTriggerOpcode(MsgRecv.id);
}

enum AreaTriggerFailures
{
	AREA_TRIGGER_FAILURE_OK				= 0,
	AREA_TRIGGER_FAILURE_UNAVAILABLE	= 1,
	AREA_TRIGGER_FAILURE_NO_BC			= 2,
	AREA_TRIGGER_FAILURE_NO_HEROIC		= 3,
	AREA_TRIGGER_FAILURE_NO_RAID		= 4,
	AREA_TRIGGER_FAILURE_NO_ATTUNE_Q	= 5,
	AREA_TRIGGER_FAILURE_NO_ATTUNE_I	= 6,
	AREA_TRIGGER_FAILURE_LEVEL			= 7,
	AREA_TRIGGER_FAILURE_NO_GROUP		= 8,
	AREA_TRIGGER_FAILURE_NO_KEY         = 9,
	AREA_TRIGGER_FAILURE_LEVEL_HEROIC	= 9,
	AREA_TRIGGER_FAILURE_NO_CHECK		= 10,
};

const char * AreaTriggerFailureMessages[] = {
	"-",
	"This instance is unavailable",
	"You must have The Burning Crusade Expansion to access this content.",
	"Heroic mode unavailable for this instance.",
	"You must be in a raid group to pass through here.",
	"You do not have the required attunement to pass through here.", //TODO: Replace attunment with real itemname
	"You do not have the required attunement to pass through here.", //TODO: Replace attunment with real itemname
	"You must be at least level %u to pass through here.",
	"You must be in a party to pass through here.",
	"You do not have the required attunement to pass through here.", //TODO: Replace attunment with real itemname
	"You must be level 70 to enter heroic mode.",
};

uint32 CheckTriggerPrerequsites(AreaTrigger * pAreaTrigger, WorldSession * pSession, Player * pPlayer, MapInfo * pMapInfo)
{
	if(pAreaTrigger->required_level && pPlayer->getLevel() < pAreaTrigger->required_level)
		return AREA_TRIGGER_FAILURE_LEVEL;

	if(!pMapInfo || !pMapInfo->HasFlag(WMI_INSTANCE_ENABLED))
		return AREA_TRIGGER_FAILURE_UNAVAILABLE;

	if(!pSession->HasFlag(ACCOUNT_FLAG_XPACK_01) && pMapInfo->HasFlag(WMI_INSTANCE_XPACK_01))
		return AREA_TRIGGER_FAILURE_NO_BC;

	// These can be overridden by cheats/GM
	if(pPlayer->triggerpass_cheat)
		return AREA_TRIGGER_FAILURE_OK;

	if(pPlayer->iInstanceType >= MODE_HEROIC && pMapInfo->type != INSTANCE_MULTIMODE && pMapInfo->type != INSTANCE_NULL)
		return AREA_TRIGGER_FAILURE_NO_HEROIC;

	if(pMapInfo->type == INSTANCE_RAID && (!pPlayer->GetGroup() || (pPlayer->GetGroup() && pPlayer->GetGroup()->GetGroupType() != GROUP_TYPE_RAID)))
		return AREA_TRIGGER_FAILURE_NO_RAID;

	if(pMapInfo->type == INSTANCE_MULTIMODE && !pPlayer->GetGroup())
		return AREA_TRIGGER_FAILURE_NO_GROUP;

	if(pMapInfo && pMapInfo->required_quest && !pPlayer->HasFinishedQuest(pMapInfo->required_quest))
		return AREA_TRIGGER_FAILURE_NO_ATTUNE_Q;

	if(pMapInfo && pMapInfo->required_item && !pPlayer->GetItemInterface()->GetItemCount(pMapInfo->required_item, true))
		return AREA_TRIGGER_FAILURE_NO_ATTUNE_I;

	if (pPlayer->iInstanceType >= MODE_HEROIC &&
		pMapInfo->type == INSTANCE_MULTIMODE &&
		!pPlayer->GetItemInterface()->GetItemCount(pMapInfo->heroic_key_1, false) &&
		!pPlayer->GetItemInterface()->GetItemCount(pMapInfo->heroic_key_2, false))
		return AREA_TRIGGER_FAILURE_NO_KEY;

	if(pPlayer->getLevel()<70 && pPlayer->iInstanceType>=MODE_HEROIC && pMapInfo->type != INSTANCE_NULL)
		return AREA_TRIGGER_FAILURE_LEVEL_HEROIC;

	return AREA_TRIGGER_FAILURE_OK;
}

void WorldSession::_HandleAreaTriggerOpcode(uint32 id)
{
//	MyLog::log->debug("AreaTrigger: %u", id);

	// Are we REALLY here?
	if( !_player->IsInWorld() )
		return;

    // Search quest log, find any exploration quests
	sQuestMgr.OnPlayerExploreArea(GetPlayer(),id);

	extern SERVER_DECL SQLStorage<AreaTrigger, HashMapStorageContainer<AreaTrigger> >				AreaTriggerStorage;
	AreaTrigger* pAreaTrigger = AreaTriggerStorage.LookupEntry( id );

	if( pAreaTrigger == NULL )
	{
		MyLog::log->debug("Missing AreaTrigger: %u", id);
		return;
	}

	if( _player->GetSession()->CanUseCommand('1') )
		sChatHandler.BlueSystemMessage( this, "[%sSystem%s] Entered areatrigger: %s%u (%s).", MSG_COLOR_WHITE, MSG_COLOR_LIGHTBLUE, MSG_COLOR_SUBWHITE, id, pAreaTrigger->Name );

	// Hook for Scripted Areatriggers
	_player->GetMapMgr()->HookOnAreaTrigger(_player, id);

	switch(pAreaTrigger->Type)
	{
	case ATTYPE_INSTANCE:
		{
			if(GetPlayer()->GetPlayerStatus() != TRANSFER_PENDING) //only ports if player is out of pendings
			{
				uint32 reason = CheckTriggerPrerequsites(pAreaTrigger, this, _player, WorldMapInfoStorage.LookupEntry(pAreaTrigger->Mapid));
				if(reason != AREA_TRIGGER_FAILURE_OK)
				{
					const char * pReason = AreaTriggerFailureMessages[reason];
					char msg[200];

					MSG_S2C::stArea_Trigger_Message Msg;

					switch (reason)
					{
					case AREA_TRIGGER_FAILURE_LEVEL:
						snprintf(msg,200,pReason,pAreaTrigger->required_level);
						break;
					case AREA_TRIGGER_FAILURE_NO_ATTUNE_I:
						{
							MapInfo * pMi = WorldMapInfoStorage.LookupEntry(pAreaTrigger->Mapid);
							ItemPrototype * pItem = ItemPrototypeStorage.LookupEntry(pMi->required_item);
							if(pItem)
								snprintf(msg,200,"You must have the item, `%s` to pass through here.",Item::BuildStringWithProto(pItem) );
							else
								snprintf(msg,200,"You must have the item, UNKNOWN to pass through here.");

						}break;
					case AREA_TRIGGER_FAILURE_NO_ATTUNE_Q:
						{
							MapInfo * pMi = WorldMapInfoStorage.LookupEntry(pAreaTrigger->Mapid);
							Quest * pQuest = QuestStorage.LookupEntry(pMi->required_quest);
							if(pQuest)
								snprintf(msg,200,"You must have finished the quest, `%s` to pass through here.",pQuest->title);
							else
								snprintf(msg,200,"You must have finished the quest, UNKNOWN to pass through here.");
						}break;
					default:
						snprintf(msg, 200, pReason);
						break;
					}
					Msg.message = msg;
					SendPacket(Msg);
					return;
				}

				GetPlayer()->SaveEntryPoint(pAreaTrigger->Mapid);
				GetPlayer()->SafeTeleport(pAreaTrigger->Mapid, 0, LocationVector(pAreaTrigger->x, pAreaTrigger->y, pAreaTrigger->z, pAreaTrigger->o));
			}
		}break;
	case ATTYPE_QUESTTRIGGER:
		{

		}break;
	case ATTYPE_INN:
		{
			// Inn
			if (!GetPlayer()->m_isResting) GetPlayer()->ApplyPlayerRestState(true);
		}break;
	case ATTYPE_TELEPORT:
		{
			if(GetPlayer()->GetPlayerStatus() != TRANSFER_PENDING) //only ports if player is out of pendings
			{
				GetPlayer()->SaveEntryPoint(pAreaTrigger->Mapid);

				if(pAreaTrigger->AreaTriggerID == 4)
				{//传送到各自的新手村
					//人族 map 2    4885.932617 2620.954102 17.929279 4.620225
					//	巫族 map 3    4252.862793 3175.292236 -3.328961 4.874038
					//	妖族 map 1    4733.054688 3963.412842 3.398214 1.327529
					if(GetPlayer()->getRace() == RACE_REN)
					{
						GetPlayer()->SafeTeleport(2, 0, 4885.932617, 2620.954102, 17.929279, 4.620225);
					}
					else if(GetPlayer()->getRace() == RACE_WU)
					{
						GetPlayer()->SafeTeleport(3, 0, 4252.862793, 3175.292236, -3.328961, 4.874038);
					}
					else
					{
						GetPlayer()->SafeTeleport(1, 0, 4733.054688, 3963.412842, 3.398214, 1.327529);
					}
				}
				else
					GetPlayer()->SafeTeleport(pAreaTrigger->Mapid, 0, LocationVector(pAreaTrigger->x, pAreaTrigger->y, pAreaTrigger->z, pAreaTrigger->o));
			}
		}break;
	default:break;
	}
}
