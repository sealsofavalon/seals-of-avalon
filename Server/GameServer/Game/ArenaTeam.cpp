#include "StdAfx.h"
#include "ArenaTeam.h"
#include "../../SDBase/Protocol/C2S_ArenaTeam.h"
#include "../../SDBase/Protocol/S2C_ArenaTeam.h"
#include "../../SDBase/Protocol/S2C_Chat.h"

static const uint32 TeamCountToId[6] = {
	0,						// 0
	0,						// 1
	0,						// 2
	ARENA_TEAM_TYPE_2V2,	// 3
	ARENA_TEAM_TYPE_3V3,	// 4
	ARENA_TEAM_TYPE_5V5,	// 5
};

static const uint32 IdToTeamCount[6] = {
	3,
	4,
	5,
	0,
	0,
	0,
};

ArenaTeam::ArenaTeam(uint32 Type, uint32 Id)
{
	m_id = Id;
	m_type = Type;
	AllocateSlots(Type);
	m_leader = 0;
	m_emblemStyle = 0;
	m_emblemColour = 0;
	m_borderColour = 0;
	m_borderStyle = 0;
	m_backgroundColour = 0;
	m_stat_rating = 1500;
	m_stat_gamesplayedweek = 0;
	m_stat_gamesplayedseason = 0;
	m_stat_gameswonseason = 0;
	m_stat_gameswonweek = 0;
	m_stat_ranking = 0;
}

ArenaTeam::ArenaTeam(Field * f)
{
	uint32 z = 0, i, guid;
	const char * data;
	m_id = f[z++].GetUInt32();
	m_type = f[z++].GetUInt32();
	m_leader = f[z++].GetUInt32();
	m_name = f[z++].GetString();
	m_emblemStyle = f[z++].GetUInt32();
	m_emblemColour = f[z++].GetUInt32();
	m_borderStyle = f[z++].GetUInt32();
	m_borderColour = f[z++].GetUInt32();
	m_backgroundColour = f[z++].GetUInt32();
	m_stat_rating = f[z++].GetUInt32();
	AllocateSlots(m_type);

	m_stat_gamesplayedweek = 0;
	m_stat_gamesplayedseason = 0;
	m_stat_gameswonseason = 0;
	m_stat_gameswonweek = 0;
	sscanf(f[z++].GetString(), "%u %u %u %u", &m_stat_gamesplayedweek, &m_stat_gameswonweek, &m_stat_gamesplayedseason, &m_stat_gameswonseason);

	m_stat_ranking = f[z++].GetUInt32();
	for(i = 0; i < m_slots; ++i)
	{
		data = f[z++].GetString();
		if(sscanf(data, "%u %u %u %u %u", &guid, &m_members[i].Played_ThisWeek, &m_members[i].Won_ThisWeek,
			&m_members[i].Played_ThisSeason, &m_members[i].Won_ThisSeason) == 5)
		{
			m_members[i].Info = objmgr.GetPlayerInfo(guid);
			if(m_members[i].Info)
				++m_memberCount;
		}
		else
			m_members[i].Info = NULL;
	}	
}

void ArenaTeam::SendPacket(PakHead* packet)
{
	PlayerInfo * info;
	for(uint32 i = 0; i < m_memberCount; ++i)
	{
		info = m_members[i].Info;
		if(info && info->m_loggedInPlayer)
			info->m_loggedInPlayer->GetSession()->SendPacket(*packet);
	}
}

void ArenaTeam::Destroy()
{
	char buffer[1024];
	MSG_S2C::stChat_Message Msg;
	vector<PlayerInfo*> tokill;
	uint32 i;
	tokill.reserve(m_memberCount);
	snprintf(buffer,1024, "The arena team, '%s', disbanded.", m_name.c_str());
	sChatHandler.FillSystemMessageData(buffer,&Msg);
	SendPacket(&Msg);

	for(i=0; i < m_memberCount; ++i)
	{
		if(m_members[i].Info)
			tokill.push_back(m_members[i].Info);
	}

	for(vector<PlayerInfo*>::iterator itr = tokill.begin(); itr != tokill.end(); ++itr)
	{
		RemoveMember(*itr);
	}

	objmgr.RemoveArenaTeam(this);
	delete this;
}

bool ArenaTeam::AddMember(PlayerInfo * info)
{
	uint32 base_field;
	Player * plr = info->m_loggedInPlayer;
	if(m_memberCount >= m_slots)
		return false;

	memset(&m_members[m_memberCount], 0, sizeof(ArenaTeamMember));
	m_members[m_memberCount++].Info = info;
	SaveToDB();

	if(plr)
	{
		//base_field = (m_type*6) + PLAYER_FIELD_ARENA_TEAM_INFO_1_1;
		plr->SetUInt32Value(base_field, m_id);
		plr->SetUInt32Value(base_field+1,m_leader);
        
        plr->m_arenaTeams[m_type]=this;
		plr->GetSession()->SystemMessage("You are now a member of the arena team, '%s'.", m_name.c_str());
	}
	return true;
}

bool ArenaTeam::RemoveMember(PlayerInfo * info)
{
	for(uint32 i = 0; i < m_memberCount; ++i)
	{
		if(m_members[i].Info == info)
		{
			/* memcpy all the blocks in front of him back (so we only loop O(members) instead of O(slots) */
			for(uint32 j = (i+1); j < m_memberCount; ++j)
				memcpy(&m_members[j-1], &m_members[j], sizeof(ArenaTeamMember));

			--m_memberCount;
			SaveToDB();

			if(info->m_loggedInPlayer)
			{
//				info->m_loggedInPlayer->SetUInt32Value(PLAYER_FIELD_ARENA_TEAM_INFO_1_1 + (m_type*6), 0);
				info->m_loggedInPlayer->m_arenaTeams[m_type]=0;
			}
			return true;
		}
	}

	return false;
}

void ArenaTeam::Stat(MSG_S2C::stArenaTeam_Stats* Msg)
{
	Msg->id		= m_id;
	Msg->stat_rating	= m_stat_rating;
	Msg->stat_gamesplayedweek	= m_stat_gamesplayedweek;
	Msg->stat_gameswonweek		= m_stat_gameswonweek;
	Msg->stat_gamesplayedseason	= m_stat_gamesplayedseason;
	Msg->stat_gameswonseason		= m_stat_gameswonseason;
	Msg->stat_ranking			= m_stat_ranking;
}

void ArenaTeam::Query(MSG_S2C::stArenaTeam_Query_Response* Msg)
{
	Msg->teamid = m_id;
	Msg->name = m_name;
	Msg->maxPlayer = GetPlayersPerTeam();
	Msg->emblemColour = m_emblemColour;
	Msg->emblemStyle  = m_emblemStyle;
	Msg->borderColour = m_borderColour;
	Msg->borderStyle  = m_borderStyle;
	Msg->backgroundColour = m_backgroundColour;
}

void ArenaTeam::Roster(MSG_S2C::stArenaTeam_Roster* Msg)
{
	Msg->teamid = m_id;
	Msg->maxplayer_count = GetPlayersPerTeam();

	for(uint32 i = 0; i < m_memberCount; ++i)
	{
		PlayerInfo* info = m_members[i].Info;
		// TODO : burlex figure out why this became null
		if( info != NULL )
		{
			Msg->vMembers.push_back(MSG_S2C::stArenaTeam_Roster::stPlayer(info->guid,uint8( (info->m_loggedInPlayer != NULL) ? 1 : 0 ),info->name,
				uint8( m_members[i].Info->guid == m_leader ? 0 : 1), info->lastLevel, info->cl,m_members[i].Played_ThisWeek,m_members[i].Won_ThisWeek,
				m_members[i].Played_ThisSeason, m_members[i].Won_ThisSeason,m_stat_rating ));
		}
	}
}

void ArenaTeam::SaveToDB()
{
	std::stringstream ss;
	uint32 i;
	ss << "REPLACE INTO arenateams VALUES("
		<< m_id << ","
		<< m_type << ","
		<< m_leader << ",'"
		<< m_name << "',"
		<< m_emblemStyle << ","
		<< m_emblemColour << ","
		<< m_borderStyle << ","
		<< m_borderColour << ","
		<< m_backgroundColour << ","
		<< m_stat_rating << ",'"
		<< m_stat_gamesplayedweek << " " << m_stat_gameswonweek << " "
		<< m_stat_gamesplayedseason << " " << m_stat_gameswonseason << "',"
		<< m_stat_ranking;
    
	for(i = 0; i < m_memberCount; ++i)
	{
		if(m_members[i].Info)
		{
			ss << ",'" << m_members[i].Info->guid << " " << m_members[i].Played_ThisWeek << " "
				<< m_members[i].Won_ThisWeek << " " << m_members[i].Played_ThisSeason << " "
				<< m_members[i].Won_ThisSeason << "'";
		}
		else
		{
			ss << ",'0 0 0 0 0'";
		}
	}

	for(; i < 10; ++i)
	{
		ss << ",'0 0 0 0 0'";
	}

	ss << ")";
	sWorld.ExecuteSqlToDBServer(ss.str().c_str());
}

bool ArenaTeam::HasMember(uint32 guid)
{
	for(uint32 i = 0; i < m_memberCount; ++i)
	{
		if(m_members[i].Info && m_members[i].Info->guid == guid)
			return true;
	}
	return false;
}

void ArenaTeam::SetLeader(PlayerInfo * info)
{
	uint32 old_leader = m_leader;
	char buffer[1024];
	MSG_S2C::stChat_Message Msg;
	snprintf(buffer, 1024,"%s is now the captain of the arena team, '%s'.", info->name, m_name.c_str());
	sChatHandler.FillSystemMessageData(buffer, &Msg);
	m_leader=info->guid;
    SendPacket(&Msg);

	/* set the fields */
	for(uint32 i = 0; i < m_memberCount; ++i)
	{
		if(m_members[i].Info == info)		/* new leader */
		{
//			if(m_members[i].Info->m_loggedInPlayer)
//				m_members[i].Info->m_loggedInPlayer->SetUInt32Value(PLAYER_FIELD_ARENA_TEAM_INFO_1_1 + (m_type*6) + 1, 0);
		}
		else if(m_members[i].Info->guid == old_leader)
		{
//			if(m_members[i].Info->m_loggedInPlayer)
//				m_members[i].Info->m_loggedInPlayer->SetUInt32Value(PLAYER_FIELD_ARENA_TEAM_INFO_1_1 + (m_type*6) + 1, 1);
		}
	}
}

ArenaTeamMember * ArenaTeam::GetMember(PlayerInfo * info)
{
	for(uint32 i = 0; i < m_memberCount; ++i)
	{
		if(m_members[i].Info == info)
			return &m_members[i];
	}
	return NULL;
}

ArenaTeamMember * ArenaTeam::GetMemberByGuid(uint32 guid)
{
	for(uint32 i = 0; i < m_memberCount; ++i)
	{
		if(m_members[i].Info && m_members[i].Info->guid == guid)
			return &m_members[i];
	}
	return NULL;
}

void WorldSession::HandleArenaTeamRosterOpcode(CPacketUsn& packet)
{
	MSG_C2S::stArenaTeam_Roster MsgRecv;packet >> MsgRecv;
	uint8 slot;
	ArenaTeam * team;
	team = objmgr.GetArenaTeamById(MsgRecv.teamId);
	if(team)
	{
		slot = TeamCountToId[team->m_type];
		MSG_S2C::stArenaTeam_Roster Msg;
		team->Roster(&Msg);
		SendPacket(Msg);
	}
}

void WorldSession::HandleArenaTeamQueryOpcode(CPacketUsn& packet)
{
	ArenaTeam * team;
	MSG_C2S::stArenaTeam_Query MsgRecv;packet >> MsgRecv;

	team = objmgr.GetArenaTeamById(MsgRecv.teamId);
	if(team != NULL)
	{
		MSG_S2C::stArenaTeam_Query_Response Msg;
		team->Query(&Msg);
		SendPacket(Msg);

		MSG_S2C::stArenaTeam_Stats MsgStat;
		team->Stat(&MsgStat);
		SendPacket(MsgStat);
	}
}

void WorldSession::HandleArenaTeamAddMemberOpcode(CPacketUsn& packet)
{
	MSG_C2S::stArenaTeam_AddMember MsgRecv;packet>>MsgRecv;

	ArenaTeam * pTeam = objmgr.GetArenaTeamById(MsgRecv.teamId);
	if( !pTeam )
		return;

	if(!pTeam->HasMember(GetPlayer()->GetLowGUID()))
		GetPlayer()->SoftDisconnect();

	Player * plr = objmgr.GetPlayer(MsgRecv.player_name.c_str(), false);
	if(plr == NULL)
	{
		SystemMessage("Player `%s` is non-existent or not online.", MsgRecv.player_name.c_str());
		return;
	}

	if(pTeam->m_leader != _player->GetLowGUID())
	{
		SystemMessage("You are not the captain of this arena team.");
		return;
	}

	if(plr->getLevel() < 70)
	{
		SystemMessage("Player must be level 70 to join an arena team.");
		return;
	}

	if(plr->m_arenaTeams[pTeam->m_type] != NULL)
	{
		SystemMessage("That player is already in an arena team of this type.");
		return;
	}

	if(plr->m_arenateaminviteguid != 0)
	{
		SystemMessage("That player is already invited to an arena team");
		return;
	}

	if(plr->GetTeam() != _player->GetTeam() && !HasGMPermissions())
	{
		SystemMessage("That player is a member of a different faction.");
		return;
	}

	plr->m_arenateaminviteguid = _player->m_arenaTeams[pTeam->m_type]->m_id;

	MSG_S2C::stArenaTeam_Invite Msg;
	Msg.player_name = _player->GetName();
	Msg.team_name   = _player->m_arenaTeams[pTeam->m_type]->m_name;
	plr->GetSession()->SendPacket(Msg);
}

void WorldSession::HandleArenaTeamRemoveMemberOpcode(CPacketUsn& packet)
{
	ArenaTeam * team;
	uint8 slot;
	PlayerInfo * inf;
	MSG_C2S::stArenaTeam_Remove_Player MsgRecv;packet >> MsgRecv;

	team = objmgr.GetArenaTeamById(MsgRecv.teamId);
	if(!team)
	{
		GetPlayer()->SoftDisconnect();
		return;
	}

	slot = team->m_type;

	if( (team = _player->m_arenaTeams[slot]) == NULL )
	{
		SystemMessage("You are not in an arena team of this type.");
		return;
	}

	if(team->m_leader != _player->GetLowGUID())
	{
		SystemMessage("You are not the leader of this team.");
		return;
	}

	if( (inf = objmgr.GetPlayerInfoByName(MsgRecv.name.c_str())) == NULL )
	{
		SystemMessage("That player cannot be found.");
		return;
	}

	if(!team->HasMember(inf->guid))
	{
		SystemMessage("That player is not in your arena team.");
		return;
	}

	if(team->RemoveMember(inf))
	{
		char buffer[1024];
		MSG_S2C::stChat_Message Msg;
		snprintf(buffer,1024,"%s was removed from the arena team '%s'.", inf->name, team->m_name.c_str());
		sChatHandler.FillSystemMessageData(buffer, &Msg);
		team->SendPacket(&Msg);
		SystemMessage("Removed %s from the arena team '%s'.", inf->name, team->m_name.c_str());
	}
}

void WorldSession::HandleArenaTeamInviteAcceptOpcode(CPacketUsn& packet)
{
	ArenaTeam * team;

	if(_player->m_arenateaminviteguid == 0)
	{
		SystemMessage("You have not been invited into another arena team.");
		return;
	}

	team = objmgr.GetArenaTeamById(_player->m_arenateaminviteguid);
	_player->m_arenateaminviteguid=0;
	if(team == 0)
	{
		SystemMessage("That arena team no longer exists.");
		return;
	}

	if(team->m_memberCount >= team->m_slots)
	{
		SystemMessage("That team is now full.");
		return;
	}

	if(_player->m_arenaTeams[team->m_type] != NULL)		/* shouldn't happen */
	{
		SystemMessage("You have already been in an arena team of that size.");
		return;
	}

	if(team->AddMember(_player->m_playerInfo))
	{
		char buffer[1024];
		MSG_S2C::stChat_Message Msg;
		snprintf(buffer,1024,"%s joined the arena team, '%s'.", _player->GetName(), team->m_name.c_str());
		sChatHandler.FillSystemMessageData(buffer, &Msg);
		team->SendPacket(&Msg);
	}
	else
	{
		SendNotification("Internal error.");
	}
}

void WorldSession::HandleArenaTeamInviteDenyOpcode(CPacketUsn& packet)
{
	ArenaTeam * team;
	if(_player->m_arenateaminviteguid == 0)
	{
		SystemMessage("You were not invited.");
		return;
	}

	team = objmgr.GetArenaTeamById(_player->m_arenateaminviteguid);
	_player->m_arenateaminviteguid=0;
	if(team == NULL)
		return;

	Player * plr = objmgr.GetPlayer(team->m_leader);
	if(plr != NULL)
		plr->GetSession()->SystemMessage("%s denied your arena team invitation for %s.", _player->GetName(), team->m_name.c_str());
}

void WorldSession::HandleArenaTeamLeaveOpcode(CPacketUsn& packet)
{
	ArenaTeam * team;
	MSG_C2S::stArenaTeam_Leave MsgRecv;packet >> MsgRecv;

	team = objmgr.GetArenaTeamById(MsgRecv.teamId);

	if(!team)
	{
		GetPlayer()->SoftDisconnect();
		return;
	}

	if( (team = _player->m_arenaTeams[team->m_type]) == NULL )
	{
		SystemMessage("You are not in an arena team of this type.");
		return;
	}

	if(team->m_leader == _player->GetLowGUID())
	{
		SystemMessage("You cannot leave the team yet, promote someone else to captain first.");
		return;
	}

	if(team->RemoveMember(_player->m_playerInfo))
	{
		char buffer[1024];
		MSG_S2C::stChat_Message Msg;
		snprintf(buffer,1024,"%s left the arena team, '%s'.", _player->GetName(), team->m_name.c_str());
		sChatHandler.FillSystemMessageData(buffer, &Msg);
		team->SendPacket(&Msg);
		SystemMessage("You have left the arena team, '%s'.", team->m_name.c_str());
	}
}

void WorldSession::HandleArenaTeamDisbandOpcode(CPacketUsn& packet)
{
	ArenaTeam * team;
	MSG_C2S::stArenaTeam_Disband MsgRecv;packet >> MsgRecv;

	team = objmgr.GetArenaTeamById(MsgRecv.teamId);
	if(!team)
	{
		GetPlayer()->SoftDisconnect();
		return;
	}

	if( (team = _player->m_arenaTeams[team->m_type]) == NULL )
	{
		SystemMessage("You are not in an arena team of this type.");
		return;
	}

	if(team->m_leader != _player->GetLowGUID())
	{
		SystemMessage("You aren't the captain of this team.");
		return;
	}

	team->Destroy();
}

void WorldSession::HandleArenaTeamPromoteOpcode(CPacketUsn& packet) 
{
	uint8 slot;
	ArenaTeam * team;
	PlayerInfo * inf;
	MSG_C2S::stArenaTeam_Promote MsgRecv;packet >> MsgRecv;

	team = objmgr.GetArenaTeamById(MsgRecv.teamId);
	if(!team)
	{
		GetPlayer()->SoftDisconnect();
		return;
	}

	slot = team->m_type;

	if( slot >= NUM_ARENA_TEAM_TYPES )
		return;

	if( (team = _player->m_arenaTeams[slot]) == NULL )
	{
		SystemMessage("You are not in an arena team of this type.");
		return;
	}

	if(team->m_leader != _player->GetLowGUID())
	{
		SystemMessage("You aren't the captain of this team.");
		return;
	}

	if( (inf = objmgr.GetPlayerInfoByName(MsgRecv.name.c_str())) == NULL )
	{
		SystemMessage("That player cannot be found.");
		return;
	}

	if(!team->HasMember(inf->guid))
	{
		SystemMessage("That player is not a member of your arena team.");
		return;
	}

	team->SetLeader(inf);
}
