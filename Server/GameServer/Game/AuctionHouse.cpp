#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Auction.h"
#include "../../SDBase/Protocol/C2S_Auction.h"
#include "ObjectMgr.h"
#include "DonationSystem.h"
#include "MultiLanguageStringMgr.h"

void Auction::DeleteFromDB()
{
	sWorld.ExecuteSqlToDBServer("DELETE FROM auctions WHERE auctionId = %u", Id);
}

void Auction::SaveToDB(uint32 AuctionHouseId)
{
	sWorld.ExecuteSqlToDBServer("INSERT INTO auctions VALUES(%u, %u, "I64FMTD", %u, %u, %u, %u, %u, %u, %d, %u, '%s', %u, '%s')",
		Id, AuctionHouseId, pItem->GetGUID(), Owner, BuyoutPrice, ExpiryTime, HighestBidder, HighestBid,
		DepositAmount, yp, owner_acct, owner_name.c_str(), (int)is_yuanbao, highestbidder_name.c_str() );
}

void Auction::UpdateInDB()
{
	sWorld.ExecuteSqlToDBServer( "UPDATE auctions SET bidder = %u, bid = %u, highest_bidder_name = '%s' WHERE auctionId = %u", HighestBidder, HighestBid, highestbidder_name.c_str(), Id );
}

AuctionHouse::AuctionHouse(uint32 ID)
{
	dbc = dbcAuctionHouse.LookupEntry(ID);
	ASSERT(dbc);

	cut_percent = float( float(dbc->tax) / 100.0f );
	deposit_percent = float( float(dbc->fee ) / 100.0f );
}

AuctionHouse::~AuctionHouse()
{
	HM_NAMESPACE::hash_map<uint32, Auction*>::iterator itr = auctions.begin();
	for(; itr != auctions.end(); ++itr)
		delete itr->second;
}

void AuctionHouse::QueueDeletion(Auction * auct, uint32 Reason)
{
	if(auct->Deleted)
		return;

	auct->Deleted = true;
	auct->DeletedReason = Reason;
	
	removalList.push_back(auct);
	UpdateDeletionQueue();
}

void AuctionHouse::UpdateDeletionQueue()
{
	
	Auction * auct;

	list<Auction*>::iterator it = removalList.begin();
	for(; it != removalList.end(); ++it)
	{
		auct = *it;
		ASSERT(auct->Deleted);
		RemoveAuction(auct);
	}

	removalList.clear();
	
}

void AuctionHouse::UpdateAuctions()
{
	
	

	uint32 t = (uint32)UNIXTIME;
	HM_NAMESPACE::hash_map<uint32, Auction*>::iterator itr = auctions.begin();
	Auction * auct;
	for(; itr != auctions.end();)
	{
		auct = itr->second;
		++itr;

		if(t >= auct->ExpiryTime)
		{
			if(auct->HighestBidder == 0)
				auct->DeletedReason = AUCTION_REMOVE_EXPIRED;
			else
				auct->DeletedReason = AUCTION_REMOVE_WON;

			auct->Deleted = true;
			removalList.push_back(auct);
		}
	}

	
	
}

void AuctionHouse::AddAuction(Auction * auct)
{
	// add to the map
	
	auctions.insert( HM_NAMESPACE::hash_map<uint32, Auction*>::value_type( auct->Id , auct ) );
	

	// add the item
	
	auctionedItems.insert( HM_NAMESPACE::hash_map<uint64, Item*>::value_type( auct->pItem->GetGUID(), auct->pItem ) );
	

	MyLog::log->debug("AuctionHouse: %u: Add auction %u, expire@ %u.", dbc->id, auct->Id, auct->ExpiryTime);
}

Auction * AuctionHouse::GetAuction(uint32 Id)
{
	Auction * ret;
	HM_NAMESPACE::hash_map<uint32, Auction*>::iterator itr;
	
	itr = auctions.find(Id);
	ret = (itr == auctions.end()) ? 0 : itr->second;
	
	return ret;
}

void AuctionHouse::RemoveAuction(Auction * auct)
{
	MyLog::log->debug("AuctionHouse: %u: Removing auction %u, reason %u.", dbc->id, auct->Id, auct->DeletedReason);

	char subject[100];
	char body[1024];
	switch(auct->DeletedReason)
	{
	case AUCTION_REMOVE_EXPIRED:
		{
			//snprintf( subject, 100, "[%s]拍卖已到期", auct->pItem->BuildString() );
			strcpy( subject, build_language_string( BuildString_AuctionMailSubjectExpired ) );

			// Auction expired, resend item, no money to owner.
			sMailSystem.SendAutomatedMessage(AUCTION, dbc->id, auct->Owner, subject, "", 0, 0, auct->pItem->GetGUID(), 62, auct->yp, auct->is_yuanbao);
		}break;

	case AUCTION_REMOVE_WON:
		{
			// ItemEntry:0:1
			//snprintf(subject, 100, "%u:0:1", (unsigned int)auct->pItem->GetEntry());
			if( !auct->yp )
			{
				//snprintf( subject, 100, "拍卖[%s]成功", auct->pItem->GetProto()->lowercase_name.c_str() );
				strcpy( subject, build_language_string( BuildString_AuctionMailSubjectSucceed ) );
			}
			else
			{
				//snprintf( subject, 100, "义拍[%s]成功", auct->pItem->GetProto()->lowercase_name.c_str() );
				strcpy( subject, build_language_string( BuildString_AuctionMailSubjectDonationSucceed ) );
			}

			// <owner player guid>:bid:buyout
			//snprintf(body, 200, "%X:%u:%u", (unsigned int)auct->Owner, (unsigned int)auct->HighestBid, (unsigned int)auct->BuyoutPrice);
			//PlayerInfo* bidder = objmgr.GetPlayerInfo( auct->HighestBidder );
			//std::string biddername;
			//if( bidder )
			//	biddername = bidder->name;
			
			// Send a mail to the owner with his cut of the price.
			uint32 auction_cut = (ui32)(float(cut_percent * float(auct->HighestBid)));
			int32 amount = auct->HighestBid - auction_cut + auct->DepositAmount;
			if(amount < 0)
				amount = 0;

			int BuyoutPrice = auct->BuyoutPrice;
			if( auct->BuyoutPrice == 0x7FFFFFFF )
				BuyoutPrice = 0;

			if( auct->yp)
			{
				//snprintf( body, 200, "感谢您对慈善事业做出的贡献! <br>金额:%d元宝", auct->HighestBid );
				strcpy( body, build_language_string( BuildString_AuctionMailBodyDonationSucceed, auct->pItem->BuildString(), quick_itoa(auct->HighestBid) ) );
			}
			else if( auct->is_yuanbao )
			{
				//snprintf( body, 200, "拍卖者:%s, 最高竞标者:%s<br>最高竞标价格:%d元宝<br>一口价:%d元宝<br>已扣除所得税:%d元宝", auct->owner_name.c_str(), auct->highestbidder_name.c_str(), auct->HighestBid, auct->BuyoutPrice, auction_cut);
				strcpy( body, build_language_string( BuildString_AuctionMailBodySucceedCashCurrency, auct->pItem->BuildString(), auct->owner_name.c_str(),
														auct->highestbidder_name.c_str(), quick_itoa(auct->HighestBid),
														quick_itoa(BuyoutPrice), quick_itoa(auction_cut) ) );
			}
			else
			{
				//snprintf( body, 200, "拍卖者:%s, 最高竞标者:%s<br>最高竞标价格:%d金%d银%d铜<br>一口价:%d金%d银%d铜<br>已扣除所得税:%d金%d银%d铜", auct->owner_name.c_str(), auct->highestbidder_name.c_str(), auct->HighestBid, auct->BuyoutPrice, auction_cut );
				strcpy( body, build_language_string( BuildString_AuctionMailBodySucceedGold, auct->pItem->BuildString(), auct->owner_name.c_str(),
														auct->highestbidder_name.c_str(), quick_gold_s(auct->HighestBid),
														quick_gold_s(BuyoutPrice), quick_gold_s(auction_cut) ) );
			}
			// Auction won by highest bidder. He gets the item.
			sMailSystem.SendAutomatedMessage(AUCTION, dbc->id, auct->HighestBidder, subject, body, 0, 0, auct->pItem->GetGUID(), 62, auct->yp, auct->is_yuanbao);

			if (auct->HighestBidder)
			{
				Player* plBidder = objmgr.GetPlayer(auct->HighestBidder);
				if (plBidder)
				{
					//plBidder->GetSession()->SystemMessage("恭喜您成功购买[%s],请尽快查收！", auct->pItem->GetProto()->lowercase_name.c_str());
					plBidder->GetSession()->SystemMessage( build_language_string( BuildString_AuctionSystemMessageToBidder, auct->pItem->BuildString() ) );
				}
			}

			// ItemEntry:0:2
			//snprintf(subject, 100, "%u:0:2", (unsigned int)auct->pItem->GetEntry());

			// <hex player guid>:bid:0:deposit:cut
			//if(auct->HighestBid == auct->BuyoutPrice)	   // Buyout
			//	snprintf(body, 200, "%X:%u:%u:%u:%u", (unsigned int)auct->HighestBidder, (unsigned int)auct->HighestBid, (unsigned int)auct->BuyoutPrice, (unsigned int)auct->DepositAmount, (unsigned int)auction_cut);
			//else
			//	snprintf(body, 200, "%X:%u:0:%u:%u", (unsigned int)auct->HighestBidder, (unsigned int)auct->HighestBid, (unsigned int)auct->DepositAmount, (unsigned int)auction_cut);

			// send message away.
			if( auct->yp )
			{
				sMailSystem.SendAutomatedMessage(AUCTION, dbc->id, auct->Owner, subject, body, 0, 0, 0, 62, auct->yp, auct->is_yuanbao);
			}
			else if( auct->is_yuanbao )
			{
				sMailSystem.SendAutomatedMessage(AUCTION, dbc->id, auct->Owner, subject, body, auct->HighestBid, 0, 0, 62, auct->yp, auct->is_yuanbao);
			}
			else
			{
				sMailSystem.SendAutomatedMessage(AUCTION, dbc->id, auct->Owner, subject, body, amount, 0, 0, 62, auct->yp, auct->is_yuanbao);
			}
			
			if (auct->Owner)
			{
				Player* plOwer = objmgr.GetPlayer(auct->Owner);
				if (plOwer)
				{
					//plOwer->GetSession()->SystemMessage("恭喜，您的[%s]在拍卖行被买家收购啦！", auct->pItem->GetProto()->lowercase_name.c_str());
					plOwer->GetSession()->SystemMessage( build_language_string( BuildString_AuctionSystemMessageToOwner, auct->pItem->BuildString() ) );
				}
			}
			
			if( auct->yp )
			{
				if( auct->is_yuanbao )
				{
					if (g_donation_system)
					{
						g_donation_system->DonateYiPai(auct->Owner,auct->HighestBid,""); //不在线
					}
				}
			}
		}break;
	case AUCTION_REMOVE_CANCELLED:
		{
			//snprintf(subject, 100, "%u:0:5", (unsigned int)auct->pItem->GetEntry());
			
			uint32 cut = 0;
			if( auct->HighestBidder )
				cut = uint32(cut_percent * (float)auct->HighestBid);

			Player * plr = objmgr.GetPlayer(auct->Owner);
			if(cut && plr)
			{
				if( !auct->is_yuanbao )
				{
					if(plr->GetUInt32Value(PLAYER_FIELD_COINAGE) < cut)
					{
						cut = plr->GetUInt32Value(PLAYER_FIELD_COINAGE);
					}

					plr->ModCoin(-((int32)cut));
					plr->m_playerInfo->total_coin = plr->GetUInt32Value(PLAYER_FIELD_COINAGE);
				
					MyLog::yunyinglog->info("gold-auctioncancel-player[%u][%s] item[%u] cnt[%u] cut:[%u]", plr->GetLowGUID(), plr->GetName(), auct->pItem->GetEntry(), auct->pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT), cut);
				}
				else
				{
					if( plr->GetCardPoints() < cut )
						cut = plr->GetCardPoints();
					
					plr->SpendPoints( cut, "cancel auction cut" );

					MyLog::yunyinglog->info("yuanbao-auctioncancel-player[%u][%s] item[%u] cnt[%u] cut:[%u]", plr->GetLowGUID(), plr->GetName(), auct->pItem->GetEntry(), auct->pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT), cut);
				}
			}
			char content1[1024];
			char content2[1024];
			
			//snprintf( subject, 100, "[%s]拍卖已取消", auct->pItem->BuildString() );
			strcpy( subject, build_language_string( BuildString_AuctionMailSubjectCancel ) );
			if( !auct->is_yuanbao )
			{
				//snprintf( content1, 100, "系统惩罚您: %u金 %u银 %u铜 已扣除", cut/10000, (cut%10000)/100, cut % 100 );
				strcpy( content1, build_language_string( BuildString_AuctionMailBodyCancelTaxGold, auct->pItem->BuildString(), quick_gold_s(cut) ) );

				// snprintf( content2, 100, "系统退回您: %u金, %u银 %u铜", auct->HighestBid/10000, (auct->HighestBid%10000)/100, auct->HighestBid % 100 );
				strcpy( content2, build_language_string( BuildString_AuctionMailBodyCancelReturnGold, auct->pItem->BuildString(), quick_gold_s(auct->HighestBid) ) );
			}
			else
			{
				//snprintf( content1, 100, "系统惩罚您: %u 元宝已扣除", cut );
				strcpy( content2, build_language_string( BuildString_AuctionMailBodyCancelTaxCashCurrency, auct->pItem->BuildString(), quick_itoa(cut) ) );

				//snprintf( content2, 100, "系统退回您: %u 元宝", auct->HighestBid );
				strcpy( content2, build_language_string( BuildString_AuctionMailBodyCancelReturnCashCurrency, auct->pItem->BuildString(), quick_itoa(auct->HighestBid) ) );
			}

			sMailSystem.SendAutomatedMessage(AUCTION, GetID(), auct->Owner, subject, content1, 0, 0, auct->pItem->GetGUID(), 62, auct->yp, auct->is_yuanbao);
			
			// return bidders money
			if(auct->HighestBidder)
			{
				sMailSystem.SendAutomatedMessage(AUCTION, GetID(), auct->HighestBidder, subject, content2, auct->HighestBid, 
					0, 0, 62, auct->yp, auct->is_yuanbao);
			}
			
		}break;
	}

	// Remove the auction from the hashmap.
	
	
	
	auctions.erase(auct->Id);
	auctionedItems.erase(auct->pItem->GetGUID());

	
	

	// Destroy the item from memory (it still remains in the db)
	delete auct->pItem;

	// Finally destroy the auction instance.
	auct->DeleteFromDB();
	delete auct;
}

void WorldSession::HandleAuctionListBidderItems( CPacketUsn& packet )
{
	if(!_player->IsInWorld())
		return;

	MSG_C2S::stAuction_List_Bidder_Items MsgRecv;packet>>MsgRecv;

	if( MsgRecv.vendorguid )
	{
		Creature * pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.vendorguid));
		if(!pCreature || !pCreature->auctionHouse)
			return;

		pCreature->auctionHouse->SendBidListPacket(_player);
	}
	else
	{
		AuctionHouse* AH = sAuctionMgr.GetAuctionHouse( 0 );
		if( AH )
			AH->SendBidListPacket(_player);
	}
}

void Auction::AddToPacket(MSG_S2C::stAuction_Item* AuctionItem)
{
	AuctionItem->auction_id = Id;
	AuctionItem->entryid = pItem->GetEntry();

	for( int i = 0; i < 6; ++i )
		AuctionItem->extra.enchants[i] = pItem->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + i * 3 );
	AuctionItem->extra.jinglian_level	= pItem->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
	/******************** ItemRandomSuffix***************************
	* For what I have seen ItemRandomSuffix is like RandomItemProperty
	* The only difference is has is that it has a modifier.
	* That is the result of jewelcrafting, the effect is that the
	* enchantment is variable. That means that a enchantment can be +1 and 
	* with more Jem's +12 or so.
	* Decription for lookup: You get the enchantmentSuffixID and search the
	* DBC for the last 1 - 3 value's(depending on the enchantment).
	* That value is what I call EnchantmentValue. You guys might find a 
	* better name but for now its good enough. The formula to calculate
	* The ingame "GAIN" is:
	* (Modifier / 10000) * enchantmentvalue = EnchantmentGain;	
	*/
	
	AuctionItem->stack_count = pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT); // Amount
	AuctionItem->owner_guid = uint64(Owner);			  // Owner guid
	AuctionItem->owner_name = owner_name;
	AuctionItem->HighestBid = HighestBid;				 // Current prize
	AuctionItem->HighestBidName = highestbidder_name;
	// hehe I know its evil, this creates a nice trough put of money
	if( HighestBidder == 0 )
		AuctionItem->Nextbidmodify = 0;
	else
	{
		AuctionItem->Nextbidmodify = HighestBid / 10;
		if( AuctionItem->Nextbidmodify == 0 )
			AuctionItem->Nextbidmodify = 1;
	}
	AuctionItem->BuyoutPrice = BuyoutPrice;				// Buyout
	AuctionItem->timeleft = uint32((ExpiryTime - UNIXTIME) * 1000); // Time left
	AuctionItem->Lastbidder = uint64(HighestBidder);	  // Last bidder
	AuctionItem->yp = yp;
	AuctionItem->is_yuanbao = is_yuanbao;
}

void AuctionHouse::SendBidListPacket(Player * plr)
{
	uint32 count = 0;

	MSG_S2C::stAuction_Bidder_List_Result Msg;
	Auction * auct;
	
	HM_NAMESPACE::hash_map<uint32, Auction*>::iterator itr = auctions.begin();
	for(; itr != auctions.end(); ++itr)
	{
		auct = itr->second;
		if(auct->HighestBidder == plr->GetGUID())
		{
			if(auct->Deleted) continue;

			MSG_S2C::stAuction_Item AuctionItem;
			auct->AddToPacket(&AuctionItem);
			Msg.vItems.push_back(AuctionItem);
		}
	}
	
	plr->GetSession()->SendPacket(Msg);
}

void AuctionHouse::SendOwnerListPacket(Player * plr)
{
	MSG_S2C::stAuction_Owner_List_Result Msg;
	Auction * auct;
	
	HM_NAMESPACE::hash_map<uint32, Auction*>::iterator itr = auctions.begin();
	for(; itr != auctions.end(); ++itr)
	{
		auct = itr->second;
		if(auct->Owner == plr->GetGUID())
		{
			if(auct->Deleted) continue;

			MSG_S2C::stAuction_Item AuctionItem;
			auct->AddToPacket(&AuctionItem);
			Msg.vItems.push_back(AuctionItem);
		}			
	}
	plr->GetSession()->SendPacket(Msg);
}

void AuctionHouse::SendAuctionNotificationPacket(Player * plr, Auction * auct)
{
	MSG_S2C::stAuction_Bidder_Notifycation Msg;
	Msg.auctionhouse_id	= GetID();
	Msg.aucion_id		= auct->Id;
	Msg.HighestBidderguid = auct->HighestBidder;
	Msg.itementry		= auct->pItem->GetEntry();
	plr->GetSession()->SendPacket(Msg);
}

void WorldSession::HandleAuctionPlaceBid( CPacketUsn& packet )
{
	if(!_player->IsInWorld())
		return;

	MSG_C2S::stAuction_Place_Bid MsgRecv;packet >> MsgRecv;
	//AUCTION_CREATE,
	//	AUCTION_CANCEL,
	//	AUCTION_BID,
	//	AUCTION_BUYOUT,
	MSG_S2C::AUCTIONRESULT en;
	en = MSG_S2C::AUCTION_CANCEL;

	AuctionHouse * ah = NULL;

	if( MsgRecv.guid )
	{
		Creature * pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
		if(!pCreature || !pCreature->auctionHouse)
			return;

		// Find Item
		ah = pCreature->auctionHouse;
	}
	else
		ah = sAuctionMgr.GetAuctionHouse( 0 );
	if( !ah )
		return;

	Auction * auct = ah->GetAuction(MsgRecv.auction_id);
	if(auct == 0 || !auct->Owner || !_player || auct->Owner == _player->GetGUID())
		return;

	if(auct->HighestBid > MsgRecv.price)
	{
		// SystemMessage("您的出价太低!");
		SystemMessage( build_language_string( BuildString_AuctionSystemMessageLowPrice ) );
		return;
	}

	en = MSG_S2C::AUCTION_BID;
	if(MsgRecv.price > auct->BuyoutPrice)
	{
		MsgRecv.price = auct->BuyoutPrice;
		en = MSG_S2C::AUCTION_BUYOUT;

		// SystemMessage("当前出价大于一口价,自动设置为一口价购买");
		SystemMessage( build_language_string( BuildString_AuctionSystemMessageBuyOut ) );
	}

	int spend = 0;
	if(auct->HighestBidder != 0)
	{
		if( auct->HighestBidder == _player->GetLowGUID() )
		{
			if(MsgRecv.price != auct->BuyoutPrice)
			{
				// SystemMessage("您当前已经出最高价!");
				SystemMessage( build_language_string( BuildString_AuctionSystemMessageAlreadyHighestPrice ) );
				return;
			}
			else
				en = MSG_S2C::AUCTION_BUYOUT;
		}
		else
		{
			// Return the money to the last highest bidder.
			char subject[100];

			// BuildString_AuctionMailSubjectBidFail
			// snprintf( subject, 100, "[%s]竞标失败", auct->pItem->BuildString() );
			strcpy( subject, build_language_string( BuildString_AuctionMailSubjectBidFail ) );

			char content[1024];
			if( auct->is_yuanbao )
			{
				// snprintf( content, 100, "您的竞标价被超过<br>系统退回您: %u元宝", auct->HighestBid );
				strcpy( content, build_language_string( BuildString_AuctionMailBodyBidFailCashCurrency, auct->pItem->BuildString(), quick_itoa( auct->HighestBid ) ) );
			}
			else
			{
				//snprintf( content, 100, "您的竞标价被超过<br>系统退回您: %u金, %u银 %u铜", auct->pItem->BuildString(), auct->HighestBid/10000, (auct->HighestBid%10000)/100, auct->HighestBid % 100 );
				strcpy( content, build_language_string( BuildString_AuctionMailBodyBidFailGold, auct->pItem->BuildString(), quick_gold_s( auct->HighestBid ) ) );
			}

			sMailSystem.SendAutomatedMessage(AUCTION, ah->GetID(), auct->HighestBidder, subject, content, auct->HighestBid,
				0, 0, 62, auct->yp, auct->is_yuanbao);


			if( MsgRecv.price == auct->BuyoutPrice )
				en = MSG_S2C::AUCTION_BUYOUT;

			Player* hb = objmgr.GetPlayer( auct->HighestBidder );
			if( hb && hb->GetSession() )
			{
				hb->GetSession()->SendNotification( subject );
				ah->SendBidListPacket(hb);
			}
		}
		spend = MsgRecv.price - auct->HighestBid;
	}
	else
		spend = MsgRecv.price;

	if( auct->is_yuanbao )
	{
		if(_player->GetUInt32Value(PLAYER_FIELD_YUANBAO) < spend)
			return;
		if( !_player->SpendPoints( spend, "auction bid" ) )
			return;
		MyLog::yunyinglog->info("yuanbao-auction-player[%u][%s] spend yuanbao[%u] for item["I64FMT"][%u]", _player->GetLowGUID(), _player->GetName(), spend, auct->pItem->GetGUID(), auct->pItem->GetProto()->ItemId);
	}
	else
	{
		if(_player->GetUInt32Value(PLAYER_FIELD_COINAGE) < spend)
			return;
		_player->ModCoin(-((int32)spend));
		MyLog::yunyinglog->info("gold-auctionbid-player[%u][%s] bid gold[%u] for item["I64FMT"][%u]", _player->GetLowGUID(), _player->GetName(), spend, auct->pItem->GetGUID(), auct->pItem->GetProto()->ItemId);
	}

	auct->HighestBidder = _player->GetLowGUID();
	auct->HighestBid = MsgRecv.price;
	auct->highestbidder_name = _player->GetName();

	if(auct->BuyoutPrice == MsgRecv.price)
	{
		// we used buyout on the item.
		ah->QueueDeletion(auct, AUCTION_REMOVE_WON);
	}
	else
	{
		// update most recent bid
		auct->UpdateInDB();
	}
	// send response packet
	MSG_S2C::stAuction_Command_Result Msg;
	Msg.auction_id = auct->Id;
	Msg.auction_result = en;
	SendPacket(Msg);

	ah->SendBidListPacket(_player);
}

void WorldSession::HandleCancelAuction( CPacketUsn& packet)
{
	if(!_player->IsInWorld())
		return;

	MSG_C2S::stAuction_Remove_Item MsgRecv;packet >> MsgRecv;

	AuctionHouse* ah = NULL;

	if( MsgRecv.guid )
	{
		Creature * pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
		if(!pCreature)
			return;
		ah = pCreature->auctionHouse;
	}
	else
		ah = sAuctionMgr.GetAuctionHouse( 0 );

	if( !ah )
		return;

	// Find Item
	Auction * auct = ah->GetAuction(MsgRecv.auction_id);
	if(auct == 0) return;

	ah->QueueDeletion(auct, AUCTION_REMOVE_CANCELLED);

	// Send response packet.
	MSG_S2C::stAuction_Command_Result Msg;
	Msg.auction_id = MsgRecv.auction_id;
	Msg.auction_result = MSG_S2C::AUCTION_CANCEL;
	SendPacket(Msg);

	// Re-send the owner list.
	ah->SendOwnerListPacket(_player);
}

void WorldSession::HandleAuctionSellItem( CPacketUsn& packet )
{
	if (!_player->IsInWorld())
		return;
	MSG_C2S::stAuction_Sell_Item MsgRecv;packet >> MsgRecv;

	if( MsgRecv.buyout < MsgRecv.bid )
	{
		// SystemMessage( "一口价不可以小于竞标价" );
		SystemMessage( build_language_string( BuildString_AuctionSystemMessageBuyOutLessThanBidPrice ) );
		return;
	}

	AuctionHouse * ah = NULL;

	if( MsgRecv.guid )
	{
		Creature * pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
		if(!pCreature || !pCreature->auctionHouse)
			return;

		// Find Item
		ah = pCreature->auctionHouse;
	}
	else
		ah = sAuctionMgr.GetAuctionHouse( 0 );
	if( !ah )
		return;

	// Get item
	Item * pItem = _player->GetItemInterface()->GetItemByGUID(MsgRecv.item);
	if( !pItem || pItem->IsSoulbound() || pItem->HasFlag(ITEM_FIELD_FLAGS, ITEM_FLAG_CONJURED ) )
	{
		MSG_S2C::stAuction_Command_Result Msg;
		Msg.auction_id = 0;
		Msg.auction_error = MSG_S2C::AUCTION_ERROR_ITEM;
		Msg.auction_result = MSG_S2C::AUCTION_CREATE;
		SendPacket(Msg);
		return;
	};

	if(pItem->GetProto()->Bonding == ITEM_BIND_NOEXSTRANGE)
	{
		_player->GetItemInterface()->BuildInventoryChangeError(
			pItem, NULL, IVN_ERR_CANT_SELL);
		return;
	}

	uint32 item_worth = pItem->GetProto()->SellPrice * pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT);
	uint32 item_deposit = float2int32(item_worth * ah->deposit_percent) * (uint32)(MsgRecv.etime / 240.0f); // deposit is per 4 hours

	if (_player->GetUInt32Value(PLAYER_FIELD_COINAGE) < item_deposit)	// player cannot afford deposit
	{
		MSG_S2C::stAuction_Command_Result Msg;
		Msg.auction_id = 0;
		Msg.auction_error = MSG_S2C::AUCTION_ERROR_MONEY;
		Msg.auction_result = MSG_S2C::AUCTION_CREATE;
		SendPacket(Msg);
		return;
	}

	pItem = _player->GetItemInterface()->SafeRemoveAndRetreiveItemByGuid(MsgRecv.item, true);
	if (!pItem){
		MSG_S2C::stAuction_Command_Result Msg;
		Msg.auction_id = 0;
		Msg.auction_error = MSG_S2C::AUCTION_ERROR_ITEM;
		Msg.auction_result = MSG_S2C::AUCTION_CREATE;
		SendPacket(Msg);
		return;
	};

	MyLog::yunyinglog->info("item-auctionsell-player[%u][%s] sell["I64FMT"][%u] count[%u]", _player->GetLowGUID(), _player->GetName(), pItem->GetGUID(), pItem->GetProto()->ItemId, pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT));
	pItem->SetOwner(NULL);
	pItem->m_isDirty = true;
	pItem->SaveToDB(INVENTORY_SLOT_NOT_SET, 0, true, NULL);

	// Create auction
	Auction * auct = new Auction;
	auct->BuyoutPrice = MsgRecv.buyout;
	auct->ExpiryTime = (uint32)UNIXTIME + (MsgRecv.etime * 60);
	auct->HighestBid = MsgRecv.bid;
	auct->HighestBidder = 0;	// hm
	auct->Id = sAuctionMgr.GenerateAuctionId();
	auct->Owner = _player->GetLowGUID();
	auct->pItem = pItem;
	auct->Deleted = false;
	auct->DeletedReason = 0;
	auct->DepositAmount = item_deposit;
	auct->yp = MsgRecv.yp;
	auct->owner_acct = _player->GetSession()->GetAccountID();
	auct->owner_name = _player->GetName();
	auct->is_yuanbao = MsgRecv.is_yuanbao;

	int BuyoutPrice = auct->BuyoutPrice;
	if( auct->BuyoutPrice == 0x7FFFFFFF )
		BuyoutPrice = 0;

	// remove deposit
	_player->ModCoin(-(int32)item_deposit);
	if(auct->is_yuanbao)
		MyLog::yunyinglog->info("yuanbao-auctiontosell-player[%u][%s] use yuanbao[%u] for item["I64FMT"][%u]", _player->GetLowGUID(), _player->GetName(), BuyoutPrice, auct->pItem->GetGUID(), auct->pItem->GetProto()->ItemId);
	else
		MyLog::yunyinglog->info("gold-auctiontosell-player[%u][%s] use gold[%u] for item["I64FMT"][%u]", _player->GetLowGUID(), _player->GetName(), BuyoutPrice, auct->pItem->GetGUID(), auct->pItem->GetProto()->ItemId);

	// Add and save auction to DB
	ah->AddAuction(auct);
	auct->SaveToDB(ah->GetID());

	// Send result packet
	MSG_S2C::stAuction_Command_Result Msg;
	Msg.auction_id	= auct->Id;
	Msg.auction_error = MSG_S2C::AUCTION_ERROR_NONE;
	Msg.auction_result = MSG_S2C::AUCTION_CREATE;
	SendPacket(Msg);
}

void WorldSession::HandleAuctionListOwnerItems( CPacketUsn& packet )
{
	if(!_player->IsInWorld())
		return;

	MSG_C2S::stAuction_List_Owner_Items MsgRecv;packet >> MsgRecv;

	if( MsgRecv.vendorguid )
	{
		Creature * pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.vendorguid));
		if(!pCreature || !pCreature->auctionHouse)
			return;

		pCreature->auctionHouse->SendOwnerListPacket(_player);
	}
	else
	{
		AuctionHouse* ah = sAuctionMgr.GetAuctionHouse( 0 );
		if( ah )
			ah->SendOwnerListPacket(_player);
	}
}

void AuctionHouse::SendAuctionList(Player * plr, MSG_C2S::stAuction_List_Items* MsgRecv)
{
	uint32 current_index = 0;
	uint32 counted_items = 0;

	// convert auction string to lowercase for faster parsing.
	if(MsgRecv->auctionString.length() > 0)
	{
		for(uint32 j = 0; j < MsgRecv->auctionString.length(); ++j)
			MsgRecv->auctionString[j] = tolower(MsgRecv->auctionString[j]);
	}
	MSG_S2C::stAuction_List_Result MsgList;
	
	HM_NAMESPACE::hash_map<uint32, Auction*>::iterator itr = auctions.begin();
	ItemPrototype * proto;
	Auction* au = NULL;

	std::map<uint32, std::multimap<uint32, Auction*> > mSort;	

	for(; itr != auctions.end(); ++itr)
	{
		au = itr->second;
		if(au->Deleted) continue;

		proto = au->pItem->GetProto();

		// Check the auction for parameters

		// inventory type
		if(MsgRecv->inventory_type != -1 && MsgRecv->inventory_type != (int32)proto->InventoryType)
			continue;

		// class
		if(MsgRecv->itemclass != -1)
		{
			if(MsgRecv->itemclass == ITEM_CLASS_MISCELLANEOUS && (int32)proto->Class == ITEM_CLASS_CONTAINER)
				;
			else if(MsgRecv->itemclass != (int32)proto->Class)
				continue;
		}

		// subclass
		if(MsgRecv->itemsubclass != -1 && MsgRecv->itemsubclass != (int32)proto->SubClass)
			continue;

		// this is going to hurt. - name
		if(MsgRecv->auctionString.length() > 0 && !FindXinYString(MsgRecv->auctionString, proto->lowercase_name))
			continue;

		// rarity
		if(MsgRecv->rarityCheck != -1 && MsgRecv->rarityCheck != (int32)proto->Quality)
			continue;

		// level range check - lower boundary
		if(MsgRecv->levelRange1 && proto->ItemLevel < MsgRecv->levelRange1)
			continue;

		// level range check - high boundary
		if(MsgRecv->levelRange2 && proto->ItemLevel > MsgRecv->levelRange2)
			continue;

		// usable check - this will hurt too :(
		if(MsgRecv->usableCheck)
		{
			// allowed class
			if(proto->AllowableClass && !(plr->getClassMask() & proto->AllowableClass))
				continue;

			if(proto->RequiredLevel && proto->RequiredLevel > plr->getLevel())
				continue;

			if(proto->AllowableRace && !(plr->getRaceMask() & proto->AllowableRace))
				continue;

			if(proto->Class == 4 && proto->SubClass && !(plr->GetArmorProficiency()&(((uint32)(1))<<proto->SubClass)))
				continue;
			
			if(proto->Class == 2 && proto->SubClass && !(plr->GetWeaponProficiency()&(((uint32)(1))<<proto->SubClass)))
				continue;
		}

		mSort[au->pItem->GetEntry()].insert( std::make_pair(au->HighestBid, au) );

	}

	for( std::map<uint32, std::multimap<uint32, Auction*> >::iterator it1 = mSort.begin(); it1 != mSort.end(); ++it1 )
	{
		std::multimap<uint32, Auction*>& m2( it1->second );
		for( std::multimap<uint32, Auction*>::iterator it2 = m2.begin(); it2 != m2.end(); ++it2 )
		{
			if(MsgRecv->start_index && current_index++ < MsgRecv->start_index) continue;

			MSG_S2C::stAuction_Item Item;
			it2->second->AddToPacket(&Item);
			MsgList.vItems.push_back(Item);

			// Page system.
			if( ++counted_items >= 7 )
				break;
		}
	}

	plr->GetSession()->SendPacket(MsgList);
}

void WorldSession::HandleAuctionListItems( CPacketUsn& packet )
{
	CHECK_INWORLD_RETURN;
	MSG_C2S::stAuction_List_Items MsgRecv;packet >> MsgRecv;

	if( MsgRecv.vendorguid )
	{
		Creature * pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.vendorguid));
		if(!pCreature || !pCreature->auctionHouse)
			return;

		pCreature->auctionHouse->SendAuctionList(_player, &MsgRecv);
	}
	else
	{
		AuctionHouse* ah = sAuctionMgr.GetAuctionHouse( 0 );
		if( ah )
			ah->SendAuctionList(_player, &MsgRecv);
	}
}

void AuctionHouse::LoadAuctions()
{
	QueryResult *result = CharacterDatabase.Query("SELECT * FROM auctions WHERE auctionhouse =%u", GetID());

	if( !result )
		return;

	Auction * auct;
	Field * fields;

	do
	{
		fields = result->Fetch();
		auct = new Auction;
		auct->Id = fields[0].GetUInt32();
		
		Item * pItem = objmgr.LoadItem(fields[2].GetUInt64());
		if(!pItem)
		{
			sWorld.ExecuteSqlToDBServer("DELETE FROM auctions WHERE auctionId=%u",auct->Id);
			delete auct;
			continue;
		}
		auct->pItem = pItem;
		auct->Owner = fields[3].GetUInt32();
		auct->BuyoutPrice = fields[4].GetUInt32();
		auct->ExpiryTime = fields[5].GetUInt32();
		auct->HighestBidder = fields[6].GetUInt32();
		auct->HighestBid = fields[7].GetUInt32();
		auct->DepositAmount = fields[8].GetUInt32();
		auct->DeletedReason = 0;
		auct->Deleted = false;
		auct->yp = fields[9].GetUInt32();
		auct->owner_acct = fields[10].GetUInt32();
		auct->owner_name = fields[11].GetString();
		auct->is_yuanbao = (bool)fields[12].GetUInt32();
		auct->highestbidder_name = fields[13].GetString();

		auctions.insert( HM_NAMESPACE::hash_map<uint32, Auction*>::value_type( auct->Id, auct ) );
	} while (result->NextRow());
	delete result;
}
