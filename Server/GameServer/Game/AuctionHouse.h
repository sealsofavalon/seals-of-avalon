#ifndef AUCTIONHOUSE_H
#define AUCTIONHOUSE_H

enum AuctionRemoveType
{
	AUCTION_REMOVE_EXPIRED,
	AUCTION_REMOVE_WON,
	AUCTION_REMOVE_CANCELLED,
};

enum AuctionMailResult
{
	AUCTION_OUTBID,
	AUCTION_WON,
	AUCTION_SOLD,
	AUCTION_EXPIRED,
	AUCTION_EXPIRED2,
	AUCTION_CANCELLED,
};
namespace MSG_S2C
{
	struct stAuction_Item;
}
namespace MSG_C2S
{
	struct stAuction_List_Items;
}
struct Auction
{
	uint32 Id;
	
	uint32 Owner;
	uint32 HighestBidder;
	uint32 HighestBid;
	
	uint32 BuyoutPrice;
	uint32 DepositAmount;
	
	uint32 ExpiryTime;
	Item * pItem;
	bool yp;
	uint32 owner_acct;
	std::string owner_name;
	std::string highestbidder_name;
	bool is_yuanbao;

	void DeleteFromDB();
	void SaveToDB(uint32 AuctionHouseId);
	void UpdateInDB();
	void AddToPacket(MSG_S2C::stAuction_Item* AuctionItem);
	bool Deleted;
	uint32 DeletedReason;
};
class AuctionHouse
{
public:
	AuctionHouse(uint32 ID);
	~AuctionHouse();

	SUNYOU_INLINE uint32 GetID() { return dbc->id; }
	void LoadAuctions();

	void UpdateAuctions();
	void UpdateDeletionQueue();

	void RemoveAuction(Auction * auct);
	void AddAuction(Auction * auct);
	Auction * GetAuction(uint32 Id);
	void QueueDeletion(Auction * auct, uint32 Reason);

	void SendOwnerListPacket(Player * plr);
	void SendBidListPacket(Player * plr);
	void SendAuctionNotificationPacket(Player * plr, Auction * auct);
	void SendAuctionList(Player * plr, MSG_C2S::stAuction_List_Items* MsgRecv);

private:
	HM_NAMESPACE::hash_map<uint64, Item*> auctionedItems;

	HM_NAMESPACE::hash_map<uint32, Auction*> auctions;

	list<Auction*> removalList;

	AuctionHouseDBC * dbc;

public:
	float cut_percent;
	float deposit_percent;
};
#endif

