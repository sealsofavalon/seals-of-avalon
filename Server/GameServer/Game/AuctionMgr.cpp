#include "StdAfx.h"

initialiseSingleton( AuctionMgr );

void AuctionMgr::LoadAuctionHouses()
{
	MyLog::log->notice("AuctionMgr: Loading Auction Houses...");

	QueryResult * res = CharacterDatabase.Query("SELECT MAX(auctionId) FROM auctions");
	if(res)
	{
		maxId = res->Fetch()[0].GetUInt32();
		delete res;
	}
	MakeGUIDByGroupID( sMaster.m_group_id, (uint32&)maxId );

	MyLog::log->notice( "maxId = %d", maxId );

	res = WorldDatabase.Query("SELECT DISTINCT `group` FROM auctionhouse");
	AuctionHouse * ah;
	map<uint32, AuctionHouse*> tempmap;
	if(res)
	{
		MyLog::log->notice( "select group from auctionhouse succeed!" );
		uint32 period = (res->GetRowCount() / 20) + 1;
		uint32 c = 0;
		do
		{
			ah = new AuctionHouse(res->Fetch()[0].GetUInt32());
			ah->LoadAuctions();
			auctionHouses.push_back(ah);
			tempmap.insert( make_pair( res->Fetch()[0].GetUInt32(), ah ) );
			if( !((++c) % period) )
				MyLog::log->notice("AuctionHouse: Done %u/%u, %u%% complete.", c, res->GetRowCount(), float2int32( (float(c) / float(res->GetRowCount()))*100.0f ));

		}while(res->NextRow());
		delete res;
	}

	res = WorldDatabase.Query("SELECT creature_entry, `group` FROM auctionhouse");
	if(res)
	{
		do 
		{
			auctionHouseEntryMap.insert( make_pair( res->Fetch()[0].GetUInt32(), tempmap[res->Fetch()[1].GetUInt32()] ) );
		} while(res->NextRow());
		delete res;
	}
}

AuctionHouse * AuctionMgr::GetAuctionHouse(uint32 Entry)
{
	if( Entry > 0 )
	{
		HM_NAMESPACE::hash_map<uint32, AuctionHouse*>::iterator itr = auctionHouseEntryMap.find(Entry);
		if(itr == auctionHouseEntryMap.end()) return NULL;
		return itr->second;
	}
	else if( auctionHouseEntryMap.size() )
		return auctionHouseEntryMap.begin()->second;
	else
		return NULL;
}

void AuctionMgr::Update()
{
	if((++loopcount % 100))
		return;
		
	vector<AuctionHouse*>::iterator itr = auctionHouses.begin();
	for(; itr != auctionHouses.end(); ++itr)
	{
		AuctionHouse* au = *itr;
		au->UpdateDeletionQueue();

		// Actual auction loop is on a seperate timer.
		if(!(loopcount % 1200))
			au->UpdateAuctions();
	}
}

AuctionMgr::~AuctionMgr()
{
	vector<AuctionHouse*>::iterator itr = auctionHouses.begin();
	for(; itr != auctionHouses.end(); ++itr)
		delete (*itr);
}

AuctionMgr::AuctionMgr()
{
	loopcount = 0;
	maxId = 1;
}

uint32 AuctionMgr::GenerateAuctionId()
{
	uint32 id=++maxId;
	return id;
}