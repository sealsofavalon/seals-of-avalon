#ifndef __AUCTIONMGR_H
#define __AUCTIONMGR_H
class AuctionMgr : public Singleton <AuctionMgr>
{
public:
	AuctionMgr();

	~AuctionMgr();

	void LoadAuctionHouses();
	void Update();

	AuctionHouse * GetAuctionHouse(uint32 Entry);

	uint32 GenerateAuctionId();

private:
	HM_NAMESPACE::hash_map<uint32, AuctionHouse*> auctionHouseEntryMap;
	vector<AuctionHouse*> auctionHouses;
	volatile uint32 maxId;
	uint32 loopcount;
};

#define sAuctionMgr AuctionMgr::getSingleton()

#endif
