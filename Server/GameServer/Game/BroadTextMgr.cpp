#include "StdAfx.h"
#include "BroadTextMgr.h"
#include "../../SDBase/Protocol/S2C_Chat.h"


BroadTextMgr* g_broadTextMgr_system = NULL ;

BroadTextMgr::BroadTextMgr()
{
	for (int i = 0; i < MaxBroadTextCount; i++)
	{
		m_vBroadText[i] = NULL ;
		
	}

	for (int i = 0; i < MaxActivityTextCount; i++)
	{
		m_vActivityText[i] = NULL ;
	}
}
BroadTextMgr::~BroadTextMgr()
{

}
bool BroadTextMgr::SaveToDB(BroadTextDate* BroadText) //
{
	if (BroadText )
	{
		return RealmDatabase.Execute( "insert into BroadText (channel, flag, starttime, endtime, second, BroadText) values(%u, %u, %u, %u, %u, '%s')", 
			BroadText->channel, BroadText->flag, BroadText->starttime, BroadText->endtime, BroadText->seconds, BroadText->text.c_str());
	}
	
	return false ;
}
bool BroadTextMgr::InitBroadTextMgr()
{
	return LoadBroadFromDB();
}
bool BroadTextMgr::LoadBroadFromDB() //
{
	time_t curTime = UNIXTIME;
	QueryResult* qr = RealmDatabase.Query( "select * from BroadText " );
	if( qr )
	{
		do
		{
			Field* f = qr->Fetch();
			ui32 starttime = f[3].GetUInt32();
			ui32 endtime = f[4].GetUInt32();
			ui32 second =  f[5].GetUInt32();
			
			if (second < TIME_MINUTE * 3)
			{
				second = TIME_MINUTE * 3;
			}
			
			if (starttime <= (curTime + TIME_DAY * 7)  && (endtime > (curTime + second)))
			{
				uInt index = 0 ;
				if (FindBroadPos(index))
				{
					BroadTextDate* pkDate = new BroadTextDate ;
					pkDate->text = f[6].GetString();
					pkDate->channel = f[1].GetUInt8();
					pkDate->flag = f[2].GetUInt8();
					pkDate->starttime = starttime;
					pkDate->endtime = endtime;
					srand(get_tick_count());
					pkDate->lasttime = UNIXTIME - (rand() % (second / 60)) * 60;
					pkDate->seconds = second ;
					m_vBroadText[index] = pkDate;
				}else
				{
					break ;
				}
			}
			
		}while( qr->NextRow() );

		delete qr;
	}
	return true ;
}
bool BroadTextMgr::FindBroadPos(ui32& index)
{
	for (int i = 0; i < MaxBroadTextCount; i++)
	{
		if (m_vBroadText[i] == NULL)
		{
			index = i;
			return true ;
		}
	}
	return false;
}

bool BroadTextMgr::AddBroadText(string text, ui8 ntype, ui8 flag, uint32 start, uint32 end, ui32 second)
{
	if (second < TIME_MINUTE * 3)
	{
		second = TIME_MINUTE * 3;
	}
	if (start >= end)
	{
		return false ;
	}else
	{
		if (end - start < second)
		{
			return false ;
		}
	}

	time_t curTime = UNIXTIME;

	//最多只能发送未来1年的公告
	if (start > curTime + TIME_YEAR)
	{
		return false ;
	}
	
	if (end > curTime + TIME_YEAR * 10 || end <= curTime + second)
	{
		return false ;
	}

	if (end <= (curTime + second))
	{
		return false ;
	}

	BroadTextDate* pkDate = new BroadTextDate ;
	pkDate->text = text;
	pkDate->channel = ntype;
	pkDate->flag = flag;
	pkDate->starttime = start;
	pkDate->endtime = end;
	srand(get_tick_count());
	pkDate->lasttime = UNIXTIME - (rand() % (second / 60)) * 60;
	pkDate->seconds = second ;
	
	if (!SaveToDB(pkDate))
	{
		delete pkDate ;
		return false ;
	}

	bool addtoBroad = false;
	if (start <= (curTime + TIME_DAY * 7)  && (end > (curTime + second)))
	{
		uInt index = 0 ;
		if (FindBroadPos(index))
		{
			m_vBroadText[index] = pkDate;
			addtoBroad = true ;
		}
	}

	if (!addtoBroad)
	{
		delete pkDate;
		pkDate = NULL ;
	}
	return true ;
}
bool BroadTextMgr::RemoveBroadText(const ui32 index)
{
	if (index >=0 && index < MaxBroadTextCount)
	{
		if (m_vBroadText[index])
		{
			delete m_vBroadText[index];
			m_vBroadText[index] = NULL ;


			return true;
		}
	}

	return false ;
}
void BroadTextMgr::Update()
{
	if( g_crosssrvmgr->m_isInstanceSrv )
		return;

	UpdateBroadText();
	UpdateActivityText();
}
void BroadTextMgr::UpdateBroadText()
{
	ui32 curTime = UNIXTIME;
	for (int i =0; i < MaxBroadTextCount; i++)
	{
		if (m_vBroadText[i] && m_vBroadText[i]->starttime <= curTime)
		{
			if ( m_vBroadText[i]->endtime > curTime)
			{
				ui32 tolast = curTime - m_vBroadText[i]->lasttime;
				if (tolast >= m_vBroadText[i]->seconds)
				{
					//发送
					MSG_S2C::stChat_Message Msg;
					sChatHandler.FillMessageData( m_vBroadText[i]->channel, m_vBroadText[i]->text.c_str(), 0, &Msg, m_vBroadText[i]->flag );
					sWorld.SendGlobalMessage(Msg);
					m_vBroadText[i]->lasttime = curTime;
				}
			}else
			{
				RemoveBroadText(i);
			}
		}
	}
}
void BroadTextMgr::ShutBroadTextMgr()
{
	for (int i = 0; i < MaxBroadTextCount; i++)
	{
		if (m_vBroadText[i])
		{
			delete m_vBroadText[i];
			m_vBroadText[i] = NULL ;
		}
		
	}

	for ( int i = 0; i < MaxActivityTextCount; i++)
	{
		if (m_vActivityText[i])
		{
			delete m_vActivityText[i];
			m_vActivityText[i] = NULL;
		}
	}
}
void BroadTextMgr::UpdateActivityText()
{
	ui32 curTime = (ui32)UNIXTIME;
	for (int i =0; i < MaxActivityTextCount; i++)
	{
		if (m_vActivityText[i])
		{
			if (isOnTimeUpdate(m_vActivityText[i]->activity))
			{
				ui32 tolast = curTime - m_vActivityText[i]->lasttime;
				if (tolast >= m_vActivityText[i]->activity->next_broadcast_interval)
				{
					//发送
					MSG_S2C::stChat_Message Msg;
					sChatHandler.FillMessageData( m_vActivityText[i]->channel, m_vActivityText[i]->activity->description[1].c_str(), 0, &Msg, m_vActivityText[i]->flag );
					sWorld.SendGlobalMessage(Msg);
					m_vActivityText[i]->lasttime = curTime;
				}
			}else
			{
				if (m_vActivityText[i]->activity )
				{
					if (m_vActivityText[i]->activity->expire > 0  && m_vActivityText[i]->activity->expire < curTime)
					{
						RemoveActivitybyIndex(i);
					}
				}else 
				{
					RemoveActivitybyIndex(i);
				}
			}
			
			
		}
	}
}
bool BroadTextMgr::RemoveActivitybyIndex(const ui32 index)
{
	if (index >=0 && index < MaxActivityTextCount)
	{
		if (m_vActivityText[index])
		{
			delete m_vActivityText[index];
			m_vActivityText[index] = NULL ;
			return true;
		}
	}
	return false ;
}
bool BroadTextMgr::isOnTimeUpdate(activity_mgr::activity_base_t* p)
{
	if (!p)
	{
		return false ;
	}
	if (p->category <= activity_mgr::ACTIVITY_CATEGORY_DONATION_EVENT)
	{
		if( p->starttime && p->expire )
		{
			if( UNIXTIME >= p->starttime && UNIXTIME <= p->expire )
				return true;
		}
		for( int i = 0; i < 7; ++i )
		{	if( p->repeat_dayofweek[i] && is_dayofweek( UNIXTIME, i ) /*&& in_duration( UNIXTIME, p->starthour, p->expirehour )*/ )
			{
				return true;
			}
		}

		return false;

	}else
	{
		if (p->category == activity_mgr::ACTIVITY_CATEGORY_TREASURE)
		{
			if (p->starttime  &&  p->expire)
			{
				if( UNIXTIME >= p->starttime && UNIXTIME <= p->expire )
				{
					uint8 start_h = p->starthour;
					if (start_h > 6)
					{
						start_h -= 3; 
					}
					for( int i = 0; i < 7; ++i )
					{	
						if(p->repeat_dayofweek[i] && is_dayofweek( UNIXTIME, i ) && in_duration( UNIXTIME,start_h, p->expirehour ))
						{
							return true;
						}
					}
					return false;
				}
			}
		}

		if (p->starttime  &&  p->expire)
		{
			if( UNIXTIME >= p->starttime && UNIXTIME <= p->expire )
			{
				for( int i = 0; i < 7; ++i )
				{	
					if(p->repeat_dayofweek[i] && is_dayofweek( UNIXTIME, i ) /*&& in_duration( UNIXTIME,p->starthour, p->expirehour )*/ )
					{
						return true;
					}
				}
				return false;
			}
		}
	}

	return false ;
}
bool BroadTextMgr::AddActivityText(activity_mgr::activity_base_t* p)
{
	if (p->description[1].length() <= 0)
	{
		return false ;
	}
	if (p->starttime >= p->expire)
	{
		return false ;
	}else
	{
		if (p->expire - p->starttime < p->next_broadcast_interval)
		{
			return false ;
		}
	}

	if (p->next_broadcast_interval < 60)
	{
		return false ;
	}
	ui32 curTime = UNIXTIME;

	if (p->expire <= (curTime + p->next_broadcast_interval))
	{
		return false ;
	}
	ui32 index = 0;

	if (FindActivityText(p->id,index))
	{
		m_vActivityText[index]->channel = CHAT_MSG_SYSTEM;
		m_vActivityText[index]->flag = 0;
		m_vActivityText[index]->activity = p;
		m_vActivityText[index]->Eventid = p->id;

		return true ;
	}else
	{
		
		ActivityTextDate* pkDate = new ActivityTextDate ;
		pkDate->channel = CHAT_MSG_SYSTEM;
		pkDate->flag = 0;
		pkDate->activity = p;
		pkDate->Eventid = p->id;

		bool bFind = false ;
		
		for (int i = 0 ; i < MaxActivityTextCount; i++)
		{
			if (m_vActivityText[i] == NULL)
			{
				srand(get_tick_count());
				pkDate->lasttime = UNIXTIME - (rand() % (p->next_broadcast_interval / 60)) * 60;
				m_vActivityText[i] = pkDate;
				bFind = true;
				break;
			}
		}

		if (!bFind)
		{
			delete pkDate ;
		}
	}
	return true ;
}


bool BroadTextMgr::FindActivityText(ui32 evtid, ui32& index)
{
	for (int i = 0 ; i < MaxActivityTextCount; i++)
	{
		if (m_vActivityText[i] && m_vActivityText[i] ->Eventid == evtid)
		{
			index = i ;
			return true ;
		}
	}

	return false ;
}
bool BroadTextMgr::RemoveActivityText(ui32 evtid)
{
	for (int i = 0 ; i < MaxActivityTextCount; i++)
	{
		if (m_vActivityText[i] && m_vActivityText[i] ->Eventid == evtid)
		{
			delete m_vActivityText[i] ;
			m_vActivityText[i] = NULL ;
			return true ;
		}
	}

	return false ;
}