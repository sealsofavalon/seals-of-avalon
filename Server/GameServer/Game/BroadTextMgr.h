#ifndef __BROAD_TEXT_MGR_H__
#define __BROAD_TEXT_MGR_H__
#include "activity.h"

enum
{
	DonationText = 0,  //捐助
	NormalText ,  // 普通

};
struct BroadTextDate
{
	string text ;
	ui8 channel; //channel
	uint32 starttime; //
	uint32 endtime; //
	uint32 seconds; //频率
	ui8 flag;
	ui32 lasttime; //上次广播时间
};
struct ActivityTextDate
{
	ui8 channel; //channel
	ui8 flag;
	ui32 lasttime; //上次广播时间
	ui32 Eventid ;
	activity_mgr::activity_base_t* activity;
};
const uInt MaxBroadTextCount =  2048;
const uInt MaxActivityTextCount = 512 ;
class BroadTextMgr 
{
public:
	BroadTextMgr();
	~BroadTextMgr();

	bool InitBroadTextMgr();
	void ShutBroadTextMgr();
	bool LoadBroadFromDB(); //
	bool SaveToDB(BroadTextDate* BroadText); //
	bool FindBroadPos(ui32& index);
	bool RemoveBroadText(const ui32 index);
	void UpdateBroadText();
	bool AddBroadText(string text, ui8 ntype, ui8 flag, uint32 start, uint32 end, ui32 second);
	

	//
	bool isOnTimeUpdate(activity_mgr::activity_base_t* p);
	bool AddActivityText(activity_mgr::activity_base_t* p);
	bool RemoveActivityText(ui32 evtid);
	bool FindActivityText(ui32 evtid, ui32& index);
	bool RemoveActivitybyIndex(const ui32 index);
	void UpdateActivityText();

	void Update();
	
private:
	BroadTextDate* m_vBroadText[MaxBroadTextCount];
	ActivityTextDate* m_vActivityText[MaxActivityTextCount];
};

extern BroadTextMgr* g_broadTextMgr_system;
#endif