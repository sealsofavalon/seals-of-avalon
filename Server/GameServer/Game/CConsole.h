#ifndef __CConsole_H
#define __CConsole_H

#include "Common.h"

class ConsoleThread : public ThreadBase
{
protected:
	bool m_killSwitch;
	bool m_isRunning;
public:
	bool run();
	void terminate();
};

#endif
