#include "StdAfx.h"
#include "CardSerialSystem.h"
#include "Player.h"
#include "../../SDBase/Protocol/C2S_Serial_for_gift.h"
#include "../../SDBase/Protocol/S2C_Serial_for_gift.h"
#include "MultiLanguageStringMgr.h"
#include "../Master.h"

CardSerialSystem* g_Serial_System = NULL ;

bool CardSerialSystem::Load()
{
	QueryResult* qr = RealmDatabase.Query( "select * from CardSerialprizelist" );
	if (qr)
	{
		do
		{
			Field* f = qr->Fetch();
			Prize*  pkPrize = new Prize;
			pkPrize->PrizeID = f[0].GetUInt32();
			pkPrize->Item[0] = f[1].GetUInt32();
			pkPrize->ItemCount[0] = f[2].GetUInt32();

			pkPrize->Item[1] = f[3].GetUInt32();
			pkPrize->ItemCount[1] = f[4].GetUInt32();

			pkPrize->Item[2] = f[5].GetUInt32();
			pkPrize->ItemCount[2] = f[6].GetUInt32();

			pkPrize->Item[3] = f[7].GetUInt32();
			pkPrize->ItemCount[3] = f[8].GetUInt32();

			pkPrize->des = f[9].GetString();
			m_PrizeMap.insert(std::map<ui32, Prize*>::value_type(pkPrize->PrizeID , pkPrize));
		}
		while( qr->NextRow() );

		delete qr;

	}else
	{
		return false ;
	}

	return true ;
}

void CardSerialSystem::UseSerial(Player* plr,const char* serial)
{
	int len = strlen(serial);
	MSG_S2C::stSerial_for_gift_Result msg ;
	if (len == 0)
	{
		msg.result = MSG_S2C::stSerial_for_gift_Result::RESULT_NOT_FOUND;
		plr->GetSession()->SendPacket( msg );
		return ;	
	}
	char s[1024];
	for (int i =0 ; i < len; i++)
	{
		if (serial[i] >= 'a' && serial[i] <= 'z')
		{
			s[i] = serial[i] + ('A' - 'a');
		}else
		{
			s[i] = serial[i];
		}
	}
	s[len] = 0;

	QueryResult* h_qr = RealmDatabase.Query( "select * from cardserialprize_history where character_id = %u ", plr->GetGUID());
	if (h_qr)
	{
		//已经使用过激活码
		msg.result = MSG_S2C::stSerial_for_gift_Result::RESULT_ERROR;
		plr->GetSession()->SendPacket( msg );
		delete h_qr;
		return ;
	}
	QueryResult* qr = RealmDatabase.Query( "select * from CardSerial where CardSerial = '%s' ", s);
	if (qr)
	{
		if (qr->GetRowCount() > 1)
		{
			//msg.result = MSG_S2C::stSerial_for_gift_Result::RESULT_ERROR;
			//plr->GetSession()->SendPacket( msg );
			delete qr;
			return ;	
		}
		Field* f = qr->Fetch();
		Serial* pkSerial = new Serial;
		pkSerial->PrizeID = f[1].GetUInt32();
		pkSerial->IsUsed = f[2].GetBool();
		pkSerial->serial = f[3].GetString();
		
		if(!pkSerial->IsUsed)
		{
			bool success = PrizeToPlr(plr,pkSerial);

			if (success)
			{
				msg.result = MSG_S2C::stSerial_for_gift_Result::RESULT_ACCEPT;
			}else
			{
				msg.result = MSG_S2C::stSerial_for_gift_Result::RESULT_ERROR;
			}
		}else
		{
			//已经被使用
			msg.result = MSG_S2C::stSerial_for_gift_Result::RESULT_IS_USER;
		}
		delete pkSerial ;
		delete qr;

	}else
	{
		//没有改序列号
		msg.result = MSG_S2C::stSerial_for_gift_Result::RESULT_NOT_FOUND;
	}
	
	plr->GetSession()->SendPacket( msg );
	
}

bool CardSerialSystem::PrizeToPlr(Player* plr, const Serial* pkSerial)
{
	if (!pkSerial)
	{
		return false ;
	}

	if (pkSerial->IsUsed)
	{
		return false ;
	}

	std::map<ui32, Prize*>::iterator it = m_PrizeMap.find(pkSerial->PrizeID);
	if (it != m_PrizeMap.end())
	{
		Prize* pkPrize = it->second ;
		if (SaveToDB(plr, pkSerial, pkPrize))
		{
			MailMessage msg ;
			msg.message_type = NORMAL;
			msg.sender_guid = 0;
			msg.subject = build_language_string( BuildString_CardSerialSystemMailSubject );//"激活码奖励";
			msg.player_guid = plr->GetGUID();

			//sprintf(buf, "您使用了激活码：%s 获得了以下奖励", pkSerial->serial.c_str());
			msg.body = build_language_string( BuildString_CardSerialSystemMailBody, pkSerial->serial.c_str() );
			msg.money = 0;
			msg.cod = 0;

			msg.stationary = 62;

			msg.delivery_time = (uint32)UNIXTIME;
			msg.expire_time = 0;
			msg.read_flag = false;
			msg.copy_made = false;
			msg.deleted_flag = false;
			msg.yp = false;
			msg.is_yuanbao = false;

			Item * item[4];
			for (int i =0 ; i < 4; i++)
			{
				item[i] = NULL;
				if (pkPrize->Item[i] == 0)
				{
					continue ;
				}
				item[i] = objmgr.CreateItem(pkPrize->Item[i], NULL);
				if(!item[i])
				{
					continue ;
				}
				item[i]->m_isDirty=true;
				item[i]->SetUInt32Value(ITEM_FIELD_STACK_COUNT, pkPrize->ItemCount[i]);
				item[i]->SetOwner(NULL);
				item[i]->SaveToDB( 0, 0, true, NULL );
				if( GUID_LOPART(item[i]->GetGUID()) != 0 )
					msg.items.push_back( GUID_LOPART(item[i]->GetGUID()) );
			}

			sMailSystem.SendAutomatedMessage(msg);

			for (int i =0 ; i < 4; i++)
			{
				if (item[i])
				{
					delete item[i];
					item[i] = NULL;
				}
			}

			return true ;
		}		
	}else
	{
		return false ;
	}

	return false ;
}

bool CardSerialSystem::SaveToDB(Player* plr, const Serial* pkSerial, const Prize* pkPrize)
{
	PlayerInfo* pi =  objmgr.GetPlayerInfo( plr->GetGUID() );
	
	if (pi)
	{
		string name = pi->name ;
		string serial = pkSerial->serial;
		ui32 time  = (ui32)UNIXTIME;
		scoped_sql_transaction_proc sstp( &RealmDatabase );
		if (!RealmDatabase.TransactionExecute("insert into CardSerialprize_history (accid, group_id, character_id, character_name, activetime, CardSerial) values(%u,%u,%u,'%s',%u,'%s')", 
			pi->acct, sMaster.m_group_id, pi->guid,name.c_str(),time, serial.c_str()) )
		{
			return false ;
		}
		if (!RealmDatabase.TransactionExecute("update CardSerial set IsUsed = 1 where CardSerial = '%s'",serial.c_str()))
		{
			return false ;
		}

		sstp.success();

		return true ;
	}

	return false ;
}

void CardSerialSystem::SerialToPlayer(Player* plr)
{
	if (plr && plr->IsInWorld())
	{
		MSG_S2C::stSerial_for_gift_use msg ;
		plr->GetSession()->SendPacket( msg );	
	}
}
void WorldSession::HandleSerialForGiftUseReq(CPacketUsn& packet)
{
	if( !_player || !_player->IsInWorld() )
		return;

	MSG_C2S::stSerialCardForGift msg;
	packet >> msg;

	if (g_Serial_System)
	{
		g_Serial_System->UseSerial(_player, msg.serial.c_str());
	}
}