#ifndef __CARDSERIALSYSYEM_H__
#define __CARDSERIALSYSYEM_H__

#include "CardSerialDate.h"
class CardSerialSystem
{
public:
	void UseSerial(Player* plr,const char* serial); 
	bool Load();

	void SerialToPlayer(Player* plr);
protected:
	bool PrizeToPlr(Player* plr, const Serial* pkSerial);
	bool SaveToDB(Player* plr, const Serial* pkSerial, const Prize* pkPrize);
private:
	std::map<ui32, Prize*>	m_PrizeMap;
};

extern CardSerialSystem* g_Serial_System;
#endif