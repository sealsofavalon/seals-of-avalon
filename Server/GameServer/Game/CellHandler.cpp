#include "StdAfx.h"
CellHandler::CellHandler(Map* map)
{
	_map = map;
	_Init();
}



void CellHandler::_Init()
{
	_cells = new MapCell**[_sizeX];

	ASSERT(_cells);
	for (uint32 i = 0; i < _sizeX; i++)
	{
		_cells[i]=NULL;
	}
}

CellHandler::~CellHandler()
{
	if(_cells)
	{
		for (uint32 i = 0; i < _sizeX; i++)
		{
			if(!_cells[i])
				continue;

			for (uint32 j = 0; j < _sizeY; j++)
			{
				if(_cells[i][j])
					delete _cells[i][j];
			}
			delete [] _cells[i];	
		}
		delete [] _cells;
	}
}

MapCell* CellHandler::Create(uint32 x, uint32 y)
{
	if(!_cells[x])
	{
		_cells[x] = new MapCell*[_sizeY];
		memset(_cells[x],0,sizeof(MapCell*)*_sizeY);
	}

	ASSERT(_cells[x][y] == NULL);

	MapCell *cls = new MapCell;
	_cells[x][y] = cls;

	return cls;
}

MapCell* CellHandler::CreateByCoords(float x, float y)
{
	return Create(GetPosX(x),GetPosY(y));
}

void CellHandler::Remove(uint32 x, uint32 y)
{
	if(!_cells[x]) return;
	ASSERT(_cells[x][y] != NULL);

	MapCell *cls = _cells[x][y];
	_cells[x][y] = NULL;

	delete cls;
}

MapCell* CellHandler::GetCellByCoords(float x, float y)
{
	return GetCell(GetPosX(x),GetPosY(y));
}

uint32 CellHandler::GetPosX(float x)
{
	ASSERT((x >= _minX) && (x <= _maxX));
	return (uint32)((_maxX-x)/_cellSize);
}

uint32 CellHandler::GetPosY(float y)
{
	ASSERT((y >= _minY) && (y <= _maxY));
	return (uint32)((_maxY-y)/_cellSize);

}