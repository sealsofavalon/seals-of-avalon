#ifndef __CELLHANDLER_H
#define __CELLHANDLER_H

class Map;

class SERVER_DECL CellHandler
{
public:
	CellHandler(Map *map);
	~CellHandler();

	inline MapCell *GetCell(uint32 x, uint32 y)
	{
		if(!_cells[x]) return NULL;
		return _cells[x][y];
	}

	MapCell *GetCellByCoords(float x, float y);
	MapCell *Create(uint32 x, uint32 y);
	MapCell *CreateByCoords(float x, float y);
	void Remove(uint32 x, uint32 y);

	SUNYOU_INLINE bool Allocated(uint32 x, uint32 y) { return _cells[x][y] != NULL; }

	static uint32 GetPosX(float x); 
	static uint32 GetPosY(float y);

	SUNYOU_INLINE Map *GetBaseMap() { return _map; }

protected:
	void _Init();


	MapCell ***_cells;

	Map* _map;
};

#endif
