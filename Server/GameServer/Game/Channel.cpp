#include "StdAfx.h"

#include "../../SDBase/Protocol/C2S_Channel.h"
#include "../../SDBase/Protocol/S2C_Channel.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "../../Common/share/Util.h"

vector<string> m_bannedChannels;
uint64 voicechannelhigh = 0;
initialiseSingleton( ChannelMgr );

void Channel::LoadConfSettings()
{
	string BannedChannels = Config.MainConfig.GetStringDefault("Channels", "BannedChannels", "");
	m_bannedChannels = StrSplit(BannedChannels, ";");
}

bool Channel::HasMember(Player * pPlayer)
{
	if( m_members.find(pPlayer) == m_members.end() )
	{
		return false;
	}
	else
	{
		return true;
	}
}

Channel::Channel(const char * name, uint32 team, uint32 type_id)
{
//	ChatChannelDBC * pDBC;
	m_flags = 0;
	m_announce = true;
	m_muted = false;
	m_general = false;
	m_name = string(name);
	m_team = team;
	m_id = type_id;
	m_minimumLevel = 1;
#ifdef VOICE_CHAT
	voice_enabled = sVoiceChatHandler.CanUseVoiceChat();
	i_voice_channel_id = -1;
#endif
	/*
	pDBC = dbcChatChannels.LookupEntry(type_id);//LookupEntryForced(type_id);
	if( pDBC != NULL )
	{
		m_general = true;
		m_announce = false;

#ifdef VOICE_CHAT
		voice_enabled = false;
#endif

		m_flags |= 0x10;			// general flag
		// flags (0x01 = custom?, 0x04 = trade?, 0x20 = city?, 0x40 = lfg?, , 0x80 = voice?,

		if( pDBC->flags & 0x08 )
			m_flags |= 0x08;		// trade

		if( pDBC->flags & 0x10 || pDBC->flags & 0x20 )
			m_flags |= 0x20;		// city flag

		if( pDBC->flags & 0x40000 )
			m_flags |= 0x40;		// lfg flag
	}
	else
		m_flags = 0x01;
	*/
	switch( type_id )
	{
	case CHAT_CHANNEL_TYPE_PRIVATE:
		m_flags = 0x01;
		break;
	case CHAT_CHANNEL_TYPE_CITY:
		//m_minimumLevel = 10;
		m_announce = false;
		m_general = true;
		m_flags = 0x20;
		break;
	case CHAT_CHANNEL_TYPE_TRADE:
		//m_minimumLevel = 10;
		m_announce = false;
		m_general = true;
		m_flags = 0x08;
		break;
	case CHAT_CHANNEL_TYPE_LFG:
		//m_minimumLevel = 10;
		m_announce = false;
		m_general = true;
		m_flags = 0x40;
		break;
	default:
		ASSERT( 0 );
		break;
	}

	// scape stuff
	if( !stricmp(name, "global") || !stricmp(name, "mall") || !stricmp(name, "lfg") )
	{
	}
}

void Channel::AttemptJoin(Player * plr, const char * password)
{
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	uint32 flags = CHANNEL_FLAG_NONE;

	if( !m_general && plr->GetSession()->CanUseCommand('1') )
		flags |= CHANNEL_FLAG_MODERATOR;

	if(!m_password.empty() && strcmp(m_password.c_str(), password) != 0)
	{
        Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_WRONGPASS);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(m_bannedMembers.find(plr->GetLowGUID()) != m_bannedMembers.end())
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_YOURBANNED);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(m_members.find(plr) != m_members.end())
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_ALREADY_ON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(m_members.empty() && !m_general)
		flags |= CHANNEL_FLAG_OWNER;

	plr->JoinedChannel(this);
	m_members.insert(make_pair(plr, flags));

	if(m_announce)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_JOINED);
		Msg.guid = plr->GetGUID();
		SendToAll(Msg, NULL);
	}

	if( m_flags & 0x40 && !plr->GetSession()->HasFlag( ACCOUNT_FLAG_NO_AUTOJOIN ) )
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_YOUJOINED)/*<< uint8(0x1A) << uint32(0) << uint32(0)*/;
	else
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_YOUJOINED)/* << m_flags << m_id << uint32(0)*/;

	plr->GetSession()->SendPacket(Msg);

#ifdef VOICE_CHAT
	if(voice_enabled)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_VOICE_ON);
		Msg.guid = plr->GetGUID();
		plr->GetSession()->SendPacket(Msg);

		//JoinVoiceChannel(plr);

		MSG_S2C::stChannel_Voice_Available Msg;
		Msg.type = uint8(0);		// 00=custom,03=party,04=raid
		Msg.channle_name = m_name;
		Msg.guid = plr->GetGUID();
		plr->GetSession()->SendPacket(Msg);
	}
#endif
}

void Channel::Part(Player * plr)
{
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	uint32 flags;
	MemberMap::iterator itr = m_members.find(plr);
	if(itr == m_members.end())
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	flags = itr->second;
	m_members.erase(itr);

#ifdef VOICE_CHAT
	itr = m_VoiceMembers.find(plr);
	if(itr != m_VoiceMembers.end())
	{
		m_VoiceMembers.erase(itr);
		if(i_voice_channel_id != (uint16)-1)
			SendVoiceUpdate();
	}
#endif

	plr->LeftChannel(this);

	if( m_general )
		return;

	if(flags & CHANNEL_FLAG_OWNER)
	{
		// we need to find a new owner
		SetOwner(NULL, NULL);
	}

	if(plr->GetSession() && plr->GetSession()->IsLoggingOut())
	{

	}
	else
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_YOULEFT);
		Msg.guid = m_id;
		plr->GetSession()->SendPacket(Msg);
	}

	if(m_announce)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_LEFT);
		Msg.guid = plr->GetGUID();
		SendToAll(Msg);

/*		data.Initialize(SMSG_PLAYER_LEFT_CHANNEL);
		data << plr->GetGUID() << m_flags << m_id << m_name;
		SendToAll(&data);*/
	}

    if( m_members.size() == 0 )
    {
		channelmgr.RemoveChannel(this);
    }
}

void Channel::SetOwner(Player * oldpl, Player * plr)
{
	Player * pOwner = NULL;
	uint32 oldflags, oldflags2;
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(oldpl != NULL)
	{
		MemberMap::iterator itr = m_members.find(oldpl);
		if(m_members.end() == itr)
		{
			Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
			plr->GetSession()->SendPacket(Msg);
			return;
		}

		if(!(itr->second & CHANNEL_FLAG_OWNER))
		{
			Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOT_OWNER);
			plr->GetSession()->SendPacket(Msg);
			return;
		}
	}

	if(plr == NULL)
	{
		for(MemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
		{
			if(itr->second & CHANNEL_FLAG_OWNER)
			{
				// remove the old owner
				oldflags2 = itr->second;
				itr->second &= ~CHANNEL_FLAG_OWNER;
				Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_MODE_CHG);
				Msg.guid = itr->first->GetGUID();
				Msg.old_member_flag = uint8(oldflags2);
				Msg.member_flag = uint8(itr->second);
				SendToAll(Msg);
			}
			else
			{
				if(pOwner == NULL)
				{
					pOwner = itr->first;
					oldflags = itr->second;
					itr->second |= CHANNEL_FLAG_OWNER;
				}
			}
		}
	}
	else
	{
		for(MemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
		{
			if(itr->second & CHANNEL_FLAG_OWNER)
			{
				// remove the old owner
				oldflags2 = itr->second;
				itr->second &= ~CHANNEL_FLAG_OWNER;
				Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_MODE_CHG);
				Msg.guid = itr->first->GetGUID();
				Msg.old_member_flag = uint8(oldflags2);
				Msg.member_flag = uint8(itr->second);
				SendToAll(Msg);
			}
			else
			{
				if(plr == itr->first)
				{
					pOwner = itr->first;
					oldflags = itr->second;
					itr->second |= CHANNEL_FLAG_OWNER;
				}
			}
		}
	}

	if(pOwner == NULL)
		return;		// obviously no members

	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_CHGOWNER);
	Msg.guid = pOwner->GetGUID();
	SendToAll(Msg);

	// send the mode changes
	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_MODE_CHG);
	Msg.guid = pOwner->GetGUID();
	Msg.old_member_flag = uint8(oldflags);
	Msg.member_flag = uint8(oldflags | CHANNEL_FLAG_OWNER);
	SendToAll(Msg);
}

void Channel::Invite(Player * plr, Player * new_player)
{
	if(m_members.find(plr) == m_members.end())
	{
		SendNotOn(plr);
		return;
	}

	if(m_members.find(new_player) != m_members.end())
	{
		SendAlreadyOn(plr, new_player);
		return;
	}

	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_INVITED);
	Msg.guid = plr->GetGUID();
	new_player->GetSession()->SendPacket(Msg);

	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_YOU_INVITED);
	plr->GetSession()->SendPacket(Msg);
}

void Channel::Moderate(Player * plr)
{
	MemberMap::iterator itr = m_members.find(plr);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(m_members.end() == itr)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(!(itr->second & CHANNEL_FLAG_OWNER || itr->second & CHANNEL_FLAG_MODERATOR) && !plr->GetSession()->CanUseCommand('1'))
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTMOD);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	m_muted = !m_muted;
	Msg.nofify_flag = uint8(m_muted ? CHANNEL_NOTIFY_FLAG_MODERATED : CHANNEL_NOTIFY_FLAG_UNMODERATED);
	Msg.guid = plr->GetGUID();
	SendToAll(Msg);
}

void Channel::Say(Player * plr, const char * message, Player * for_gm_client, bool forced)
{
	MemberMap::iterator itr = m_members.find(plr);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(!forced)
	{
		if(m_members.end() == itr)
		{
			Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
			plr->GetSession()->SendPacket(Msg);
			return;
		}

		if(itr->second & CHANNEL_FLAG_MUTED)
		{
			Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_YOUCANTSPEAK);
			plr->GetSession()->SendPacket(Msg);
			return;
		}

		if(m_muted && !(itr->second & CHANNEL_FLAG_VOICED) && !(itr->second & CHANNEL_FLAG_MODERATOR) && !(itr->second & CHANNEL_FLAG_OWNER))
		{
			Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_YOUCANTSPEAK);
			plr->GetSession()->SendPacket(Msg);
			return;
		}
	}

	/*
	// not blizzlike but meh
	if( plr->getLevel() < m_minimumLevel )
	{
		plr->BroadcastMessage("你必须等级达到%u才能在频道中说话, '%s'.", m_minimumLevel, m_name.c_str());
		return;
	}
	*/

	MSG_S2C::stChat_Message MsgChat;
	MsgChat.type	= CHAT_MSG_CHANNEL;
	MsgChat.guid	= plr->GetGUID();
	MsgChat.creature_name = plr->GetName();
	MsgChat.txt		= message;
	MsgChat.gm_tag	= (uint8)(plr->bGMTagOn ? 4 : 0);
	MsgChat.channel_name = m_name;
	MsgChat.channel_id = m_id;
	if(for_gm_client != NULL)
		for_gm_client->GetSession()->SendPacket(MsgChat);
	else
	{
		SendToAll(MsgChat);

		/*
		if(!stricmp("trade", m_name.c_str()))
		{
			switch(plr->getRace())
			{
			case RACE_YAO:
				MsgChat.txt = "[妖族]"+MsgChat.txt;
				break;
			case RACE_REN:
				MsgChat.txt = "[人族]"+MsgChat.txt;
				break;
			case RACE_WU:
				MsgChat.txt = "[巫族]"+MsgChat.txt;
				break;
			}
			sWorld.SendMsgToALLGM(MsgChat);
		}
		*/
	}
}

void Channel::SendNotOn(Player * plr)
{
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
	plr->GetSession()->SendPacket(Msg);
}

void Channel::SendAlreadyOn(Player * plr, Player* plr2)
{
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	Msg.guid = plr2->GetGUID();
	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_ALREADY_ON);
	plr->GetSession()->SendPacket(Msg);
}

void Channel::Kick(Player * plr, Player * die_player, bool ban)
{
	MemberMap::iterator itr = m_members.find(die_player);
	MemberMap::iterator me_itr = m_members.find(plr);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	uint32 flags;

	if(me_itr == m_members.end())
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(itr == m_members.end())
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOT_ON_2);
		Msg.guid = die_player->GetGUID();
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(!(me_itr->second & CHANNEL_FLAG_OWNER || me_itr->second & CHANNEL_FLAG_MODERATOR))
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTMOD);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

    flags = itr->second;
	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_KICKED);
	Msg.guid = die_player->GetGUID();
	SendToAll(Msg);

	if(ban)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_BANNED);
		Msg.guid = die_player->GetGUID();
		SendToAll(Msg);
	}

	m_members.erase(itr);
#ifdef VOICE_CHAT
	itr = m_VoiceMembers.find(plr);
	if(itr != m_VoiceMembers.end())
	{
		m_VoiceMembers.erase(itr);
		if(i_voice_channel_id != (uint16)-1)
			SendVoiceUpdate();
	}
#endif

	if(flags & CHANNEL_FLAG_OWNER)
		SetOwner(NULL, NULL);

	if(ban)
		m_bannedMembers.insert(die_player->GetLowGUID());

	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_YOULEFT);
	Msg.guid = m_id;
	Msg.old_member_flag = uint32(0);
	Msg.member_flag = uint8(0);
	die_player->GetSession()->SendPacket(Msg);
}

void Channel::Unban(Player * plr, PlayerInfo * bplr)
{
	MemberMap::iterator itr = m_members.find(plr);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(m_members.end() == itr)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(!(itr->second & CHANNEL_FLAG_OWNER || itr->second & CHANNEL_FLAG_MODERATOR))
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTMOD);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	set<uint32>::iterator it2 = m_bannedMembers.find(bplr->guid);
	if(it2 == m_bannedMembers.end())
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOT_ON_2);
		Msg.guid = uint64(bplr->guid);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_UNBANNED);
	Msg.guid = uint64(bplr->guid);
	SendToAll(Msg);
	m_bannedMembers.erase(it2);
}

void Channel::Voice(Player * plr, Player * v_player)
{
	MemberMap::iterator itr = m_members.find(plr);
	MemberMap::iterator itr2 = m_members.find(v_player);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(m_members.end() == itr)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(m_members.end() == itr2)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOT_ON_2);
		Msg.guid = v_player->GetGUID();
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(!(itr->second & CHANNEL_FLAG_OWNER || itr->second & CHANNEL_FLAG_MODERATOR))
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTMOD);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	uint32 oldflags = itr2->second;
    itr2->second |= CHANNEL_FLAG_VOICED;
	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_MODE_CHG);
	Msg.guid = v_player->GetGUID();
	Msg.old_member_flag = uint8(oldflags);
	Msg.member_flag = uint8(itr2->second);
	SendToAll(Msg);
}

void Channel::Devoice(Player * plr, Player * v_player)
{
	MemberMap::iterator itr = m_members.find(plr);
	MemberMap::iterator itr2 = m_members.find(v_player);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(m_members.end() == itr)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(m_members.end() == itr2)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOT_ON_2);
		Msg.guid = v_player->GetGUID();
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(!(itr->second & CHANNEL_FLAG_OWNER || itr->second & CHANNEL_FLAG_MODERATOR))
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTMOD);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	uint32 oldflags = itr2->second;
	itr2->second &= ~CHANNEL_FLAG_VOICED;
	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_MODE_CHG);
	Msg.guid = v_player->GetGUID();
	Msg.old_member_flag = uint8(oldflags);
	Msg.member_flag = uint8(itr2->second);
	SendToAll(Msg);
}

void Channel::Mute(Player * plr, Player * die_player)
{
	MemberMap::iterator itr = m_members.find(plr);
	MemberMap::iterator itr2 = m_members.find(die_player);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(m_members.end() == itr)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(m_members.end() == itr2)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOT_ON_2);
		Msg.guid = die_player->GetGUID();
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(!(itr->second & CHANNEL_FLAG_OWNER || itr->second & CHANNEL_FLAG_MODERATOR))
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTMOD);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	uint32 oldflags = itr2->second;
	itr2->second |= CHANNEL_FLAG_MUTED;
	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_MODE_CHG);
	Msg.guid = die_player->GetGUID();
	Msg.old_member_flag = uint8(oldflags);
	Msg.member_flag = uint8(itr2->second);
	SendToAll(Msg);
}

void Channel::Unmute(Player * plr, Player * die_player)
{
	MemberMap::iterator itr = m_members.find(plr);
	MemberMap::iterator itr2 = m_members.find(die_player);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(m_members.end() == itr)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(m_members.end() == itr2)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOT_ON_2);
		Msg.guid = die_player->GetGUID();
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(!(itr->second & CHANNEL_FLAG_OWNER || itr->second & CHANNEL_FLAG_MODERATOR))
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTMOD);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	uint32 oldflags = itr2->second;
	itr2->second &= ~CHANNEL_FLAG_MUTED;
	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_MODE_CHG);
	Msg.guid = die_player->GetGUID();
	Msg.old_member_flag = uint8(oldflags);
	Msg.member_flag = uint8(itr2->second);
	SendToAll(Msg);
}

void Channel::GiveModerator(Player * plr, Player * new_player)
{
	MemberMap::iterator itr = m_members.find(plr);
	MemberMap::iterator itr2 = m_members.find(new_player);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(m_members.end() == itr)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(m_members.end() == itr2)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOT_ON_2);
		Msg.guid = new_player->GetGUID();
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(!(itr->second & CHANNEL_FLAG_OWNER || itr->second & CHANNEL_FLAG_MODERATOR))
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTMOD);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	uint32 oldflags = itr2->second;
	itr2->second |= CHANNEL_FLAG_MODERATOR;
	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_MODE_CHG);
	Msg.guid = new_player->GetGUID();
	Msg.old_member_flag = uint8(oldflags);
	Msg.member_flag = uint8(itr2->second);
	SendToAll(Msg);
}

void Channel::TakeModerator(Player * plr, Player * new_player)
{
	MemberMap::iterator itr = m_members.find(plr);
	MemberMap::iterator itr2 = m_members.find(new_player);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(m_members.end() == itr)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(m_members.end() == itr2)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOT_ON_2);
		Msg.guid = new_player->GetGUID();
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(!(itr->second & CHANNEL_FLAG_OWNER || itr->second & CHANNEL_FLAG_MODERATOR))
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTMOD);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	uint32 oldflags = itr2->second;
	itr2->second &= ~CHANNEL_FLAG_MODERATOR;
	Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_MODE_CHG);
	Msg.guid = new_player->GetGUID();
	Msg.old_member_flag = uint8(oldflags);
	Msg.member_flag = uint8(itr2->second);
	SendToAll(Msg);
}

void Channel::Announce(Player * plr)
{
	MemberMap::iterator itr = m_members.find(plr);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(m_members.end() == itr)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(!(itr->second & CHANNEL_FLAG_OWNER || itr->second & CHANNEL_FLAG_MODERATOR))
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTMOD);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	m_announce = !m_announce;
	Msg.nofify_flag = uint8(m_announce ? CHANNEL_NOTIFY_FLAG_ENABLE_ANN : CHANNEL_NOTIFY_FLAG_DISABLE_ANN);
	Msg.guid = plr->GetGUID();
	SendToAll(Msg);
}

void Channel::Password(Player * plr, const char * pass)
{
	MemberMap::iterator itr = m_members.find(plr);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(m_members.end() == itr)
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTON);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	if(!(itr->second & CHANNEL_FLAG_OWNER || itr->second & CHANNEL_FLAG_MODERATOR))
	{
		Msg.nofify_flag = uint8(CHANNEL_NOTIFY_FLAG_NOTMOD);
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	m_password = string(pass);
	Msg.guid = plr->GetGUID();
	SendToAll(Msg);
}

void Channel::List(Player * plr)
{
	MSG_S2C::stChannel_List MsgList;
	MemberMap::iterator itr = m_members.find(plr);
	if(itr == m_members.end())
	{
		MSG_S2C::stChannel_Notify Msg;
		Msg.name = m_name;
		Msg.nofify_flag = CHANNEL_NOTIFY_FLAG_NOTON;
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	uint8 flags;
	MsgList.name = m_name;
	MsgList.flag = m_flags;

	for(MemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
	{
		MSG_S2C::stChannel_List::stChannel channel;
		channel.member_guid = itr->first->GetGUID();
		flags = 0;
		if(!(itr->second & CHANNEL_FLAG_MUTED))
			flags |= CHANNEL_FLAG_VOICED;		// voice flag

		if(itr->second & CHANNEL_FLAG_OWNER)
			flags |= CHANNEL_FLAG_OWNER;		// owner flag

		if(itr->second & CHANNEL_FLAG_MODERATOR)
			flags |= CHANNEL_FLAG_MODERATOR;		// moderator flag

		if(!m_general)
			flags |= CHANNEL_FLAG_CUSTOM;

		channel.member_flag = flags;
		MsgList.vChannels.push_back(channel);
	}

	plr->GetSession()->SendPacket(MsgList);
}

void Channel::GetOwner(Player * plr)
{
	MemberMap::iterator itr = m_members.find(plr);
	MSG_S2C::stChannel_Notify Msg;
	Msg.name = m_name;
	if(itr == m_members.end())
	{
		Msg.nofify_flag = CHANNEL_NOTIFY_FLAG_NOTON;
		plr->GetSession()->SendPacket(Msg);
		return;
	}

	for(itr = m_members.begin(); itr != m_members.end(); ++itr)
	{
		if(itr->second & CHANNEL_FLAG_OWNER)
		{
			Msg.nofify_flag = CHANNEL_NOTIFY_FLAG_WHO_OWNER;
			Msg.guid = itr->first->GetGUID();
			plr->GetSession()->SendPacket(Msg);
			return;
		}
	}
}
ChannelMgr::~ChannelMgr()
{
	for(int i = 0; i < 2; ++i)
	{
		ChannelList::iterator itr = this->Channels[i].begin();
		for(; itr != this->Channels[i].end(); ++itr)
		{
			delete itr->second;
		}
		Channels[i].clear();
	}
}

Channel::~Channel()
{
	for(MemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
		itr->first->LeftChannel(this);
}

void Channel::SendToAll(PakHead& packet)
{
	for(MemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
	{
		if(itr->first->GetSession()->GetPermissionCount() != 0 && !stricmp("trade", m_name.c_str()))
			;//itr->first->GetSession()->SendPacket(packet);
		else
			itr->first->GetSession()->SendPacket(packet);
	}
}

void Channel::SendToAll(PakHead& packet, Player * plr)
{
	for(MemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
	{
		if (itr->first != plr)
			itr->first->GetSession()->SendPacket(packet);
	}
}

Channel * ChannelMgr::GetCreateChannel(const char *name, Player * p, uint32 type_id)
{
	Channel * chn;
	int team = 0;
	//if( seperatechannels && p != NULL )
	if( p )
		team = p->getRace();

	ChannelList * cl = &Channels[team-1];
	for(ChannelList::iterator itr = cl->begin(); itr != cl->end(); ++itr)
	{
		if(!stricmp(name, itr->first.c_str()))
		{
			return itr->second;
		}
	}

	// make sure the name isn't banned
	for(vector<string>::iterator itr = m_bannedChannels.begin(); itr != m_bannedChannels.end(); ++itr)
	{
		if(!strnicmp( name, itr->c_str(), itr->size() ) )
		{
			return NULL;
		}
	}

	chn = new Channel(name, team, type_id);
	cl->insert(make_pair(chn->m_name, chn));
	return chn;
}

Channel* ChannelMgr::GetCreateChannel(const char *name, int team, uint32 type_id)
{
	ChannelList * cl = &Channels[team-1];
	Channel * chn;

	for( ChannelList::iterator itr = cl->begin(); itr != cl->end(); ++itr)
	{
		if(!stricmp(name, itr->first.c_str()))
		{
			return itr->second;
		}
	}

	// make sure the name isn't banned
	for(vector<string>::iterator itr = m_bannedChannels.begin(); itr != m_bannedChannels.end(); ++itr)
	{
		if(!strnicmp( name, itr->c_str(), itr->size() ) )
		{
			return NULL;
		}
	}

	chn = new Channel(name, team, type_id);
	cl->insert(make_pair(chn->m_name, chn));
	return chn;
}

Channel * ChannelMgr::GetChannel(const char *name, Player * p)
{
	ChannelList::iterator itr;
	int team = 0;
	if( p )
		team = p->GetTeam();
	if (team >= 3)
	{
		return NULL;
	}
	ChannelList * cl = &Channels[team-1];
	//if(seperatechannels)
	//	cl = &Channels[p->GetTeam()-1];

	for(itr = cl->begin(); itr != cl->end(); ++itr)
	{
		if(!stricmp(name, itr->first.c_str()))
		{
			return itr->second;
		}
	}

	return NULL;
}

Channel * ChannelMgr::GetChannel(const char *name, uint32 team)
{
	ChannelList::iterator itr;
	if (team >= 3)
	{
		return NULL;
	}
	ChannelList * cl = &Channels[team-1];

//	ChannelList * cl = &Channels[0];
//	if(seperatechannels)
//		cl = &Channels[team-1];

	for(itr = cl->begin(); itr != cl->end(); ++itr)
	{
		if(!stricmp(name, itr->first.c_str()))
		{
			return itr->second;
		}
	}

	return NULL;
}

void ChannelMgr::RemoveChannel(Channel * chn)
{
	ChannelList::iterator itr;
	if (chn->m_team >= 3)
	{
		return;
	}
	ChannelList * cl = &Channels[chn->m_team-1];
//	if(seperatechannels)
//		cl = &Channels[chn->m_team-1];

	for(itr = cl->begin(); itr != cl->end(); ++itr)
	{
		if(itr->second == chn)
		{
			delete chn;
			cl->erase( itr );
			return;
		}
	}

}

ChannelMgr::ChannelMgr()
{
	this->GetCreateChannel( "city", 1, CHAT_CHANNEL_TYPE_CITY );
	this->GetCreateChannel( "city", 2, CHAT_CHANNEL_TYPE_CITY );
	this->GetCreateChannel( "city", 3, CHAT_CHANNEL_TYPE_CITY );

	this->GetCreateChannel( "trade", 1, CHAT_CHANNEL_TYPE_TRADE );
	this->GetCreateChannel( "trade", 2, CHAT_CHANNEL_TYPE_TRADE );
	this->GetCreateChannel( "trade", 3, CHAT_CHANNEL_TYPE_TRADE );

	this->GetCreateChannel( "lfg", 1, CHAT_CHANNEL_TYPE_LFG );
	this->GetCreateChannel( "lfg", 2, CHAT_CHANNEL_TYPE_LFG );
	this->GetCreateChannel( "lfg", 3, CHAT_CHANNEL_TYPE_LFG );

	for( int i = 0; i < 1000; ++i )
	{
		char tmp[256];
		sprintf( tmp, "zone%d", i + i );
		this->GetCreateChannel( tmp, 1, CHAT_CHANNEL_TYPE_CITY );
		this->GetCreateChannel( tmp, 2, CHAT_CHANNEL_TYPE_CITY );
		this->GetCreateChannel( tmp, 3, CHAT_CHANNEL_TYPE_CITY );
	}
}

#ifdef VOICE_CHAT
void Channel::VoiceChannelCreated(uint16 id)
{
	MyLog::log->debug("VoiceChannelCreated: id %u", id);
	i_voice_channel_id = id;

	SendVoiceUpdate();
}

void Channel::JoinVoiceChannel(Player * plr)
{
	if(m_VoiceMembers.find(plr) == m_VoiceMembers.end())
	{
		m_VoiceMembers.insert(make_pair(plr, 0x06));
		if(m_VoiceMembers.size() == 1)		// create channel
			sVoiceChatHandler.CreateVoiceChannel(this);

		if(i_voice_channel_id != (uint16)-1)
			SendVoiceUpdate();
	}
}

void Channel::PartVoiceChannel(Player * plr)
{
	MemberMap::iterator itr = m_VoiceMembers.find(plr);
	if(itr != m_VoiceMembers.end())
	{
		m_VoiceMembers.erase(itr);
		if(m_VoiceMembers.size() == 0)
			sVoiceChatHandler.DestroyVoiceChannel(this);

		if(i_voice_channel_id != (uint16)-1)
			SendVoiceUpdate();
	}
}

void Channel::SendVoiceUpdate()
{
	/*
	0x039E - SMSG_VOICE_SESSION
	uint64 channel_id
	uint16 voice_channel_id
	uint8 channel_type (0=channel,2=party)
	string name
	16 bytes - unk, some sort of encryption key?
	uint32 ip
	uint16 port
	uint8 player_count
	<for each player>
		uint64 guid
		uint8 user_id
		uint8 flags
	uint8 unk
	*/

	MSG_S2C::stChannel_Voice_Session Msg;
	uint8 m_encryptionKey[16] = { 0xba, 0x4d, 0x45, 0x60, 0x63, 0xcc, 0x12, 0xBC, 0x73, 0x94, 0x90, 0x03, 0x18, 0x14, 0x45, 0x1F };
	uint8 counter=1;
	MemberMap::iterator itr;
	memcpy( Msg.encryptionKey, m_encryptionKey, sizeof( m_encryptionKey ) );

	Msg.channel_id = uint64(0xe0e10000000032abULL);
	//data << uint16(0x5e26);		// used in header of udp packets
	Msg.channel_name =  m_name;
	Msg.VoiceServerIP = uint32(htonl(sVoiceChatHandler.GetVoiceServerIP()));
	Msg.VoiceServerPort = uint16(htons(sVoiceChatHandler.GetVoiceServerPort()));

	for(itr = m_VoiceMembers.begin(); itr != m_VoiceMembers.end(); ++itr)
	{
		MSG_S2C::stChannel_Voice_Session::stMember Member;
		Member.guid = itr->first->GetGUID();
		Member.count = counter;
		Member.unk =uint8(itr->second);
		Msg.push_back( Member );
	}


	for(itr = m_VoiceMembers.begin(); itr != m_VoiceMembers.end(); ++itr)
		itr->first->GetSession()->SendPacket(Msg);

}

void Channel::VoiceDied()
{
	m_VoiceMembers.clear();
	i_voice_channel_id = (uint16)-1;
}

void ChannelMgr::VoiceDied()
{
	for(uint32 i = 0; i < 2; ++i)
	{
		for(ChannelList::iterator itr = Channels[i].begin(); itr != Channels[i].end(); ++itr)
			itr->second->VoiceDied();
	}
}

#endif
