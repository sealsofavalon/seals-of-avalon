#ifndef __CHANNEL_H
#define __CHANNEL_H


class Channel
{
	typedef map<Player*, uint32> MemberMap;
	MemberMap m_members;
	set<uint32> m_bannedMembers;
public:
	friend class ChannelIterator;
	static void LoadConfSettings();
	string m_name;
	string m_password;
	uint8 m_flags;
	uint32 m_id;
	bool m_general;
	bool m_muted;
	bool m_announce;
	uint32 m_team;
	bool m_common;
	SUNYOU_INLINE size_t GetNumMembers() { return m_members.size(); }
#ifdef VOICE_CHAT
	bool voice_enabled;
	uint16 i_voice_channel_id;
	MemberMap m_VoiceMembers;
#endif
	uint32 m_minimumLevel;
public:
	Channel(const char * name, uint32 team, uint32 type_id);
	~Channel();

	void AttemptJoin(Player * plr, const char * password);
	void Part(Player * plr);
	void Kick(Player * plr, Player * die_player, bool ban);
	void Invite(Player * plr, Player * new_player);
	void Moderate(Player * plr);
	void Mute(Player * plr, Player * die_player);
	void Voice(Player * plr, Player * v_player);
	void Unmute(Player * plr, Player * die_player);
	void Devoice(Player * plr, Player * v_player);
	void Say(Player * plr, const char * message, Player * for_gm_client, bool forced);
	void Unban(Player * plr, PlayerInfo * bplr);
	void GiveModerator(Player * plr, Player * new_player);
	void TakeModerator(Player * plr, Player * new_player);
	void Announce(Player * plr);
	void Password(Player * plr, const char * pass);
	void List(Player * plr);
	void GetOwner(Player * plr);

	void SetOwner(Player * oldpl, Player * plr);

	// Packet Forging
	void SendAlreadyOn(Player * plr, Player * plr2);
	void SendYouAreBanned(Player * plr);
	void SendNotOn(Player * plr);
	void SendNotOwner(Player * plr);
	void SendYouCantSpeak(Player * plr);
	void SendModeChange(Player * plr, uint8 old_flags, uint8 new_flags);

	void SendToAll(PakHead& packet);
	void SendToAll(PakHead& packet, Player * plr);

#ifdef VOICE_CHAT
	void VoiceChannelCreated(uint16 id);
	void JoinVoiceChannel(Player * plr);
	void PartVoiceChannel(Player * plr);
	void SendVoiceUpdate();
	void VoiceDied();

#endif

	bool HasMember(Player * pPlayer);
};

class ChannelIterator
{
	Channel::MemberMap::iterator m_itr;
	Channel::MemberMap::iterator m_endItr;
	bool m_searchInProgress;
	Channel * m_target;
public:
	ChannelIterator(Channel* target) : m_searchInProgress(false),m_target(target) {}
	~ChannelIterator() { if(m_searchInProgress) { EndSearch(); } }

	void BeginSearch()
	{
		// iteminterface doesn't use mutexes, maybe it should :P
		ASSERT(!m_searchInProgress);
		m_itr = m_target->m_members.begin();
		m_endItr = m_target->m_members.end();
		m_searchInProgress=true;
	}

	void EndSearch()
	{
		// nothing here either
		ASSERT(m_searchInProgress);
		m_searchInProgress=false;
	}

	Player* operator*() const
	{
		return m_itr->first;
	}

	Player* operator->() const
	{
		return m_itr->first;
	}

	void Increment()
	{
		if(!m_searchInProgress)
			BeginSearch();

		if(m_itr==m_endItr)
			return;

		++m_itr;
	}

	SUNYOU_INLINE Player* Grab() { return m_itr->first; }
	SUNYOU_INLINE bool End() { return (m_itr==m_endItr)?true:false; }
};

#endif
