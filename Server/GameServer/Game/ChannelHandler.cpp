#include "StdAfx.h"

#include "../../SDBase/Protocol/C2S_Channel.h"
#include "../../SDBase/Protocol/S2C_Channel.h"

void WorldSession::HandleChannelJoin(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Join MsgRecv;packet >> MsgRecv;
	uint32 i;
	Channel * chn;

	if(!stricmp(MsgRecv.channelname.c_str(), "LookingForGroup") && !sWorld.m_lfgForNonLfg)
	{
		// make sure we have lfg dungeons
		for(i = 0; i < 3; ++i)
		{
			if(_player->LfgDungeonId[i] != 0)
				break;
		}

		if(i == 3)
			return;		// don't join lfg
	}

	if( sWorld.GmClientChannel.size() && !stricmp(sWorld.GmClientChannel.c_str(), MsgRecv.channelname.c_str()) && !GetPermissionCount())
		return;
	
	chn = channelmgr.GetCreateChannel(MsgRecv.channelname.c_str(), _player, MsgRecv.dbc_id);
	if(chn == NULL)
		return;

	chn->AttemptJoin(_player, MsgRecv.pass.c_str());
	//MyLog::log->debug("player:%s ChannelJoin: %s", _player->GetName(), MsgRecv.channelname.c_str());
}

void WorldSession::HandleChannelLeave(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Leave MsgRecv;packet >> MsgRecv;
	Channel * chn;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	if(chn == NULL)
		return;

	chn->Part(_player);
	//MyLog::log->debug("player:%s ChannelLeave: %s", _player->GetName(), MsgRecv.channelname.c_str());
}

void WorldSession::HandleChannelList(CPacketUsn& packet)
{
	MSG_C2S::stChannel_List MsgRecv;packet >> MsgRecv;
	Channel * chn;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	if(chn != NULL)
		chn->List(_player);
}

void WorldSession::HandleChannelPassword(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Password MsgRecv;packet >> MsgRecv;
	Channel * chn;
	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(),_player);
	if(chn)
		chn->Password(_player, MsgRecv.pass.c_str());
}

void WorldSession::HandleChannelSetOwner(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Set_Owner MsgRecv;packet >> MsgRecv;
	Channel * chn;
	Player * plr;
	
	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	plr = objmgr.GetPlayer(MsgRecv.newp.c_str(), false);
	if(chn && plr)
		chn->SetOwner(_player, plr);
}

void WorldSession::HandleChannelOwner(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Owner MsgRecv;packet >> MsgRecv;
	Channel * chn;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(),_player);
	if(chn)
		chn->GetOwner(_player);
}

void WorldSession::HandleChannelModerator(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Moderator MsgRecv;packet >> MsgRecv;
	Channel * chn;
	Player * plr;
	
	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	plr = objmgr.GetPlayer(MsgRecv.newp.c_str(), false);
	if(chn && plr)
		chn->GiveModerator(_player, plr);
}

void WorldSession::HandleChannelUnmoderator(CPacketUsn& packet)
{
	MSG_C2S::stChannel_UnModerator MsgRecv;packet >> MsgRecv;
	Channel * chn;
	Player * plr;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	plr = objmgr.GetPlayer(MsgRecv.newp.c_str(), false);
	if(chn && plr)
		chn->TakeModerator(_player, plr);
}

void WorldSession::HandleChannelMute(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Mute MsgRecv; packet >> MsgRecv;
	Channel * chn;
	Player * plr;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	plr = objmgr.GetPlayer(MsgRecv.newp.c_str(), false);
	if(chn && plr)
		chn->Mute(_player, plr);
}

void WorldSession::HandleChannelUnmute(CPacketUsn& packet)
{
	MSG_C2S::stChannel_UnMute MsgRecv;packet >> MsgRecv;
	Channel * chn;
	Player * plr;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	plr = objmgr.GetPlayer(MsgRecv.newp.c_str(), false);
	if(chn && plr)
		chn->Unmute(_player, plr);
}

void WorldSession::HandleChannelInvite(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Invite MsgRecv;packet >> MsgRecv;
	Channel * chn;
	Player * plr;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	plr = objmgr.GetPlayer(MsgRecv.newp.c_str(), false);
	if(chn && plr)
		chn->Invite(_player, plr);
}
void WorldSession::HandleChannelKick(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Kick MsgRecv;packet >> MsgRecv;
	Channel * chn;
	Player * plr;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	plr = objmgr.GetPlayer(MsgRecv.newp.c_str(), false);
	if(chn && plr)
		chn->Kick(_player, plr, false);
}

void WorldSession::HandleChannelBan(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Ban MsgRecv;packet >> MsgRecv;
	Channel * chn;
	Player * plr;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	plr = objmgr.GetPlayer(MsgRecv.newp.c_str(), false);
	if(chn && plr)
		chn->Kick(_player, plr, true);
}

void WorldSession::HandleChannelUnban(CPacketUsn& packet)
{
	MSG_C2S::stChannel_UnBan MsgRecv;packet >> MsgRecv;
	Channel * chn;
	PlayerInfo * plr;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	plr = objmgr.GetPlayerInfoByName(MsgRecv.newp.c_str());
	if(chn && plr)
		chn->Unban(_player, plr);
}

void WorldSession::HandleChannelAnnounce(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Announcements MsgRecv;packet >> MsgRecv;
	Channel * chn;
	
	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	if(chn)
		chn->Announce(_player);
}

void WorldSession::HandleChannelModerate(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Moderate MsgRecv;packet>>MsgRecv;
	Channel * chn;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	if(chn)
		chn->Moderate(_player);
}

void WorldSession::HandleChannelRosterQuery(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Get_Roster_Info MsgRecv;packet >> MsgRecv;
	Channel * chn;

	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	if(chn)
		chn->List(_player);
}

void WorldSession::HandleChannelNumMembersQuery(CPacketUsn& packet)
{
	MSG_C2S::stChannel_Num_Member_Query MsgRecv;packet >> MsgRecv;
	MSG_S2C::stChannel_Num_Member_Query_Response Msg;
	Channel *chn;
	chn = channelmgr.GetChannel(MsgRecv.channelname.c_str(), _player);
	if(chn)
	{
		Msg.channle_name = MsgRecv.channelname;
		Msg.flag = uint8(chn->m_flags);
		Msg.player_count = uint32(chn->GetNumMembers());
		SendPacket(Msg);
	}
}
