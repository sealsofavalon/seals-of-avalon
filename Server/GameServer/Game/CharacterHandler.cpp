#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Guild.h"
#include "../../SDBase/Protocol/S2C_Group.h"
#include "../../SDBase/Protocol/C2S_Move.h"
#include "../Common/Protocol/GS2DB.h"
#include "../Net/DSSocket.h"
#include "DonationSystem.h"
#include "MultiLanguageStringMgr.h"

bool VerifyName(const char * name, size_t nlen)
{
	const char * p;
	size_t i;

	static const char * bannedCharacters = "\t\v\b\f\a\n\r\\\"\'\? <>[](){}_=+-|/!@#$%^&*~`.,0123456789\0";
	static const char * allowedCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	if(sWorld.m_limitedNames)
	{
		for(i = 0; i < nlen; ++i)
		{
			p = allowedCharacters;
			for(; *p != 0; ++p)
			{
				if(name[i] == *p)
					goto cont;
			}

			return false;
cont:
			continue;
		}
	}
	else
	{
		for(i = 0; i < nlen; ++i)
		{
			p = bannedCharacters;
			while(*p != 0 && name[i] != *p && name[i] != 0)
				++p;

			if(*p != 0)
				return false;
		}
	}
	
	return true;
}

void WorldSession::HandlePlayerLoadingOKOpcode(CPacketUsn&)
{
	if( !_player->m_bRecvLoadingOK )
	{
		_player->m_bRecvLoadingOK = true;
		FullLogin(_player);
	}
}

bool ChatHandler::HandleRenameAllCharacter(const char * args, WorldSession * m_session)
{
	if( g_crosssrvmgr->m_isInstanceSrv )
		return false;
	uint32 uCount = 0;
	uint32 ts = getMSTime();
	QueryResult * result = CharacterDatabase.Query("SELECT guid, name FROM characters");
	if( result )
	{
		do 
		{
			uint32 uGuid = result->Fetch()[0].GetUInt32();
			const char * pName = result->Fetch()[1].GetString();
			size_t szLen = strlen(pName);

			if( !VerifyName(pName, szLen) )
			{
				printf("renaming character %s, %u\n", pName,uGuid);
                Player * pPlayer = objmgr.GetPlayer(uGuid);
				if( pPlayer != NULL )
				{
					pPlayer->rename_pending = true;
					pPlayer->GetSession()->SystemMessage("Your character has had a force rename set, you will be prompted to rename your character at next login in conformance with server rules.");
				}

				sWorld.ExecuteSqlToDBServer("UPDATE characters SET forced_rename_pending = 1 WHERE guid = %u", uGuid);
				++uCount;
			}

		} while (result->NextRow());
		delete result;
	}

	SystemMessage(m_session, "Procedure completed in %u ms. %u character(s) forced to rename.", getMSTime() - ts, uCount);
	return true;
}

void CapitalizeString(string& arg)
{
	if(arg.length() == 0) return;
	arg[0] = toupper(arg[0]);
	for(uint32 x = 1; x < arg.size(); ++x)
		arg[x] = tolower(arg[x]);
}

void WorldSession::LoadAccountDataProc(QueryResult * result)
{
	size_t len;
	const char * data;
	char * d;

	if(!result)
	{
		sWorld.ExecuteSqlToDBServer("INSERT INTO account_data VALUES(%u, '', '', '', '', '', '', '', '', '')", _accountId);
		return;
	}

	for(uint32 i = 0; i < 7; ++i)
	{
		data = result->Fetch()[1+i].GetString();
		len = data ? strlen(data) : 0;
		if(len > 1)
		{
			d = new char[len+1];
			memcpy(d, data, len+1);
			SetAccountData(i, d, true, (uint32)len);
		}
	}
}

void WorldSession::FullLogin(Player * plr)
{
	MyLog::log->debug("WorldSession: Fully loading player %u", plr->GetLowGUID());
	SetPlayer(plr); 

	// send finished quests
	MSG_S2C::stFinishQuests MsgFinishQuests;
	MsgFinishQuests.quests = plr->m_finishedQuests;
	SendPacket(MsgFinishQuests);

	// copy to movement array
	m_Movement->guid = plr->GetGUID();

	/*
	MSG_S2C::stMsg_Dungeon_Difficulty MsgDifficulty;
	MsgDifficulty.instanceType = plr->iInstanceType;
// 	datab << uint32(0x01);
// 	datab << uint32(0x00);
	SendPacket( MsgDifficulty );
	*/

	// send voicechat state - active/inactive
	/*
	{SERVER} Packet: (0x03C7) UNKNOWN PacketSize = 2
	|------------------------------------------------|----------------|
	|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
	|------------------------------------------------|----------------|
	|02 01							               |..              |
	-------------------------------------------------------------------
	*/

	MSG_S2C::stVoice_System_Stats MsgVoice;
	plr->UpdateAttackSpeed();
	/*if(plr->getLevel()>70)
		plr->SetUInt32Value(UNIT_FIELD_LEVEL,70);*/

	// enable trigger cheat by default
	plr->triggerpass_cheat = HasGMPermissions();

	// Make sure our name exists (for premade system)
	PlayerInfo * info = objmgr.GetPlayerInfo(plr->GetLowGUID());
	assert( info );
	plr->m_playerInfo = info;
	if(plr->m_playerInfo->guild)
	{
		plr->m_uint32Values[PLAYER_GUILDID] = plr->m_playerInfo->guild->GetGuildId();
		plr->m_uint32Values[PLAYER_GUILDRANK] = plr->m_playerInfo->guildRank->iId;
		plr->m_uint32Values[PLAYER_FIELD_HOSTILE_GUILDID] = plr->m_playerInfo->guild->GetHostileGuildID();
		plr->m_uint32Values[PLAYER_FIELD_GUILD_SCORE] = plr->m_playerInfo->guild->GetScore();
		plr->UpdateGuildEmblem();
	}

	info->m_loggedInPlayer = plr;
	if( !g_crosssrvmgr->m_isInstanceSrv && plr->_db != &CharacterDatabase )
	{
		uint32 guid = plr->GetLowGUID();
		plr->_db->Execute("delete from characters where guid = %u", guid );
		plr->_db->Execute("delete FROM tutorials WHERE playerId=%u",guid);
		plr->_db->Execute("delete FROM playercooldowns WHERE player_guid=%u", guid);
		plr->_db->Execute("delete FROM questlog WHERE player_guid=%u",guid);
		plr->_db->Execute("delete FROM playeritems WHERE ownerguid=%u", guid);
		plr->_db->Execute("delete FROM playerpets WHERE ownerguid=%u", guid);
		plr->_db->Execute("delete FROM playersummonspells where ownerguid=%u", guid);
		plr->_db->Execute("delete FROM mailbox WHERE player_guid = %u", guid);
		plr->_db->Execute("delete FROM player_arena_history WHERE guid = %u", guid);

		// social
		plr->_db->Execute("delete FROM social_friends WHERE character_guid = %u", guid);
		plr->_db->Execute("delete FROM social_friends WHERE friend_guid = %u", guid);
		plr->_db->Execute("delete FROM social_ignores WHERE character_guid = %u", guid);
		
		//plr->_db->Execute("delete from refine_item_container where guid = %u", guid);
		//plr->_db->Execute("delete from playertitles where player_guid = %u", guid);
	}

	if( plr->m_FirstLogin )
	{
		PlayerCreateInfo * createinfo = objmgr.GetPlayerCreateInfo(plr->getRace(), plr->getClass());
		if(info != NULL)
		for(std::list<CreateInfo_ItemStruct>::iterator is = createinfo->items.begin(); is!=createinfo->items.end(); is++)
		{
			if ( (*is).protoid != 0)
			{
				Item* item=objmgr.CreateItem((*is).protoid,plr);
				if(item)
				{
					item->SetUInt32Value(ITEM_FIELD_STACK_COUNT,(*is).amount);
					if((*is).slot<INVENTORY_SLOT_BAG_END)
					{
						if( !plr->GetItemInterface()->SafeAddItem(item, INVENTORY_SLOT_NOT_SET, (*is).slot) )
							delete item;
					}
					else
					{
						if( !plr->GetItemInterface()->AddItemToFreeSlot(item) )
							delete item;
					}
				}
			}
		}

		Item* pReturnStone = _player->GetItemInterface()->FindItemLessMax(ITEM_ID_HEARTH_STONE, 0, false);
		if(pReturnStone)
		{
			pReturnStone->SetUInt32Value(ITEM_FIELD_RECORD_MAPID, _player->GetMapId());
			pReturnStone->SetFloatValue(ITEM_FIELD_RECORD_POSX,  _player->GetPositionX());
			pReturnStone->SetFloatValue(ITEM_FIELD_RECORD_POSY,  _player->GetPositionY());
			pReturnStone->SetFloatValue(ITEM_FIELD_RECORD_POSZ,  _player->GetPositionZ());
			pReturnStone->SetUInt32Value(ITEM_FIELD_RECORD_ZONEID,_player->GetZoneId());
			MSG_S2C::stOnItemFieldRecordChange msg;
			msg.entry = pReturnStone->GetEntry();
			msg.zoneid = _player->GetZoneId();
			SendPacket( msg );
			pReturnStone->m_isDirty = true;
		}
	}

	bool enter_world = true;
#ifndef CLUSTERING
	// Find our transporter and add us if we're on one.
	if(plr->m_TransporterGUID != 0)
	{
		Transporter * pTrans = objmgr.GetTransporter(GUID_LOPART(plr->m_TransporterGUID));
		if(pTrans)
		{
			if(plr->isDead())
			{
				plr->ResurrectPlayer();
				plr->SetUInt32Value(UNIT_FIELD_HEALTH, plr->GetUInt32Value(UNIT_FIELD_MAXHEALTH));
				plr->SetUInt32Value(UNIT_FIELD_POWER1, plr->GetUInt32Value(UNIT_FIELD_MAXPOWER1));
			}

			float c_tposx = pTrans->GetPositionX() + plr->m_TransporterX;
			float c_tposy = pTrans->GetPositionY() + plr->m_TransporterY;
			float c_tposz = pTrans->GetPositionZ() + plr->m_TransporterZ;
			if(plr->GetMapId() != pTrans->GetMapId())	   // loaded wrong map
			{
				plr->SetMapId(pTrans->GetMapId());
				MSG_S2C::stMove_New_World Msg;
				Msg.map_id	= pTrans->GetMapId();
				Msg.pos_x	= c_tposx;
				Msg.pos_y	= c_tposy;
				Msg.pos_z	= c_tposz;
				Msg.orientation	= plr->GetOrientation();
				SendPacket(Msg);

				// shit is sent in worldport ack.
				enter_world = false;
			}

			plr->SetPosition(c_tposx, c_tposy, c_tposz, plr->GetOrientation(), false);
			plr->m_CurrentTransporter = pTrans;
			pTrans->AddPlayer(plr);
		}
	}
#endif

	MyLog::log->debug("Login: Player %s logged in.", plr->GetName());

	if(plr->GetTeam() == 1)
		sWorld.HordePlayers++;
	else
		sWorld.AlliancePlayers++;

	if(plr->m_FirstLogin && !HasGMPermissions())
	{
		uint32 racecinematic = plr->myRace->cinematic_id;
#ifdef USING_BIG_ENDIAN
		swap32(&racecinematic);
#endif
		OutPacket(SMSG_TRIGGER_CINEMATIC, 4, &racecinematic);
	}

	MyLog::log->notice( "WORLD: Created new player for existing players (%s)", plr->GetName() );

	// Login time, will be used for played time calc
	plr->m_playedtime[2] = (uint32)UNIXTIME;

	//Issue a message telling all guild members that this player has signed on
	if(plr->IsInGuild())
	{
		Guild *pGuild = plr->m_playerInfo->guild;
		if(pGuild)
		{
			MSG_S2C::stGuild_Event Msg;
			Msg.guild_event = GUILD_EVENT_MOTD;
			if(pGuild->GetMOTD())
				Msg.vStrs.push_back(pGuild->GetMOTD());
			SendPacket(Msg);

			pGuild->LogGuildEvent(GUILD_EVENT_HASCOMEONLINE, 1, plr->GetName());
		}
	}

	// Send online status to people having this char in friendlist
	_player->Social_TellFriendsOnline();
	// send friend list (for ignores)
	_player->Social_SendFriendList(7);


	if(_player->GetSession()->HasGMPermissions())
	{
		/*
		// Send MOTD
		_player->BroadcastMessage(sWorld.GetMotd());
		
		// Send revision (if enabled)
#ifdef WIN32
		_player->BroadcastMessage("Server: %s %s r%u/%s-Win-%s %s()", MSG_COLOR_WHITE, BUILD_TAG,
			BUILD_REVISION, CONFIG, ARCH, MSG_COLOR_LIGHTBLUE);		
#else
		_player->BroadcastMessage("Server: %ss %s r%u/%s-%s %s()", MSG_COLOR_WHITE, BUILD_TAG,
			BUILD_REVISION, PLATFORM_TEXT, ARCH, MSG_COLOR_LIGHTBLUE);
#endif
		*/


		if(sWorld.SendStatsOnJoin)
		{
			_player->BroadcastMessage("在线玩家数量: %s%u \r\r",
				MSG_COLOR_WHITE, sWorld.GetSessionCount());
		}
	}

	//Set current RestState
	if( plr->m_isResting) 		// We are resting at an inn , turn on Zzz
		plr->ApplyPlayerRestState(true);

	//Calculate rest bonus if there is time between lastlogoff and now
	if( plr->m_timeLogoff > 0 && plr->GetUInt32Value(UNIT_FIELD_LEVEL) < plr->GetUInt32Value(PLAYER_FIELD_MAX_LEVEL))	// if timelogoff = 0 then it's the first login
	{
		uint32 currenttime = (uint32)UNIXTIME;
		uint32 timediff = currenttime - plr->m_timeLogoff;

		//Calculate rest bonus
		if( timediff > 0 ) 
			plr->AddCalculatedRestXP(timediff);
	}

#ifdef CLUSTERING
	plr->SetInstanceID(forced_instance_id);
	plr->SetMapId(forced_map_id);
#else
	sHookInterface.OnEnterWorld2(_player);
#endif


	
	if(enter_world && !_player->GetMapMgr())
	{
		plr->AddToWorld();
	}

	if (info->guild)
	{
		if (info->guild->GetGuildLeader() != info->guid)
		{
			if (_player->HasSpell(88))
			{
				_player->removeSpell(88, false, false, 0);
			}
		}
		else
		{
			if (!_player->HasSpell(88))
			{
				_player->addSpell(88);
			}
		}

	}

	if(info->m_Group)
	{

		MSG_S2C::stPartyMemberStat Msg;
		_player->GetGroup()->UpdateGroupListForPlayer(_player);	
		_player->GetGroup()->UpdateOutOfRangePlayer(_player, GROUP_UPDATE_TYPE_FULL_CREATE | GROUP_UPDATE_TYPE_FULL_REQUEST_REPLY, false, &Msg);
		SendPacket(Msg);
		MSG_S2C::stGroupSetPlayerIcon MsgIcon;
		MsgIcon.guid = 0;
		MsgIcon.icon = 0;
		for (int i = 0; i < 8; i  ++)
		{
			MsgIcon.m_targetIcons[i] = info->m_Group->m_targetIcons[i];

		}

		SendPacket(MsgIcon);
	}
	
	objmgr.AddPlayer(_player);

	// donation

	if (g_donation_system)
	{
		g_donation_system->SendDonationEventList2Player(_player);
		g_donation_system->SendDonationHistory2Player(_player);
	}


	//fangchenmi
	if(_player->GetSession()->m_bFangchenmiAccount)
	{
		//_player->GetSession()->SystemMessage("您的账号已经纳入防沉迷系统，请尽快去官方网站填写防沉迷资料！当前沉迷等级为%d, 当前累积游戏时间为%s", _player->GetSession()->m_ChenmiStat,  ConvertTimeStampToString(_player->GetSession()->m_ChenmiTime).c_str());
		_player->GetSession()->SystemMessage( build_language_string( BuildString_CharacterHandlerSystemMessageChemi, quick_itoa(_player->GetSession()->m_ChenmiStat),  quick_itoa(_player->GetSession()->m_ChenmiTime/60) ) );
	}

}

bool ChatHandler::HandleRenameCommand(const char * args, WorldSession * m_session)
{
	// prevent buffer overflow
	if(strlen(args) > 100)
		return false;

	char name1[100];
	char name2[100];

	if(sscanf(args, "%s %s", name1, name2) != 2)
		return false;

	if(VerifyName(name2, strlen(name2)) == false)
	{
		RedSystemMessage(m_session, "That name is invalid or contains invalid characters.");
		return true;
	}

	string new_name = name2;
	PlayerInfo * pi = objmgr.GetPlayerInfoByName(name1);
	if(pi == 0)
	{
		RedSystemMessage(m_session, "Player not found with this name.");
		return true;
	}

	if( objmgr.GetPlayerInfoByName(new_name.c_str()) != NULL )
	{
		RedSystemMessage(m_session, "Player found with this name in use already.");
		return true;
	}

	objmgr.RenamePlayerInfo(pi, pi->name, new_name.c_str());

	free(pi->name);
	pi->name = strdup(new_name.c_str());

	// look in world for him
	Player * plr = objmgr.GetPlayer(pi->guid);
	if(plr != 0)
	{
		plr->SetName(new_name);
		BlueSystemMessageToPlr(plr, "%s changed your name to '%s'.", m_session->GetPlayer()->GetName(), new_name.c_str());
		plr->SaveToDB(false);
	}
	else
	{
		sWorld.ExecuteSqlToDBServer("UPDATE characters SET name = '%s' WHERE guid = %u", CharacterDatabase.EscapeString(new_name).c_str(), (uint32)pi->guid);
	}

	GreenSystemMessage(m_session, "Changed name of '%s' to '%s'.", name1, name2);
	MyLog::gmlog->notice("renamed character %s (GUID: %u) to %s", name1, pi->guid, name2);
	MyLog::gmlog->notice("GM renamed character %s (GUID: %u) to %s", name1, pi->guid, name2);
	return true;
}

bool ChatHandler::HandleCopyCommand(const char * args, WorldSession * m_session)
{
	return true;
	// prevent buffer overflow
	if(strlen(args) > 100)
		return false;

	Player *m_plyr = getSelectedChar(m_session, false);

	if(!m_plyr)
	{
		RedSystemMessage(m_session, "No target found.");
		return false;
	}

	MSG_S2C::stCopyName Msg;
	Msg.name = m_plyr->GetName();
	m_session->SendPacket(Msg);
	return true;
}
