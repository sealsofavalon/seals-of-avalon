#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S.h"
#include "../../SDBase/Protocol/S2C_Chat.h"

initialiseSingleton( ChatHandler );
initialiseSingleton(CommandTableStorage);

ChatCommand * ChatHandler::getCommandTable()
{
	ASSERT(false);
	return 0;
}

ChatCommand * CommandTableStorage::GetSubCommandTable(const char * name)
{
	if(!strcmp(name, "modify"))
		return _modifyCommandTable;
	else if(!strcmp(name, "debug"))
		return _debugCommandTable;
	else if(!strcmp(name, "waypoint"))
		return _waypointCommandTable;
	else if(!strcmp(name, "gmTicket"))
		return _GMTicketCommandTable;
	else if(!strcmp(name, "gobject"))
		return _GameObjectCommandTable;
	else if(!strcmp(name, "battleground"))
		return _BattlegroundCommandTable;
	else if(!strcmp(name, "npc"))
		return _NPCCommandTable;
	else if(!strcmp(name, "cheat"))
		return _CheatCommandTable;
	else if(!strcmp(name, "account"))
		return _accountCommandTable;
	else if(!strcmp(name, "pet"))
		return _petCommandTable;
	else if(!strcmp(name, "recall"))
		return _recallCommandTable;
	else if(!strcmp(name, "honor"))
		return _honorCommandTable;
	else if(!strcmp(name, "guild"))
		return _GuildCommandTable;
	else if(!strcmp(name, "quest"))
		return _questCommandTable;
	else if(!strcmp(name, "character"))
		return _charCommandTable;
	return 0;
}

#define dupe_command_table(ct, dt) this->dt = (ChatCommand*)allocate_and_copy(sizeof(ct)/* / sizeof(ct[0])*/, ct)
SUNYOU_INLINE void* allocate_and_copy(uint32 len, void * pointer)
{
	void * data = (void*)malloc(len);
	memcpy(data, pointer, len);
	return data;
}

void CommandTableStorage::Load()
{
	QueryResult * result = WorldDatabase.Query("SELECT * FROM command_overrides");
	if(!result) return;

	do 
	{
		const char * name = result->Fetch()[0].GetString();
		const char * level = result->Fetch()[1].GetString();
		Override(name, level);
	} while(result->NextRow());
	delete result;
}

void CommandTableStorage::Override(const char * command, const char * level)
{
	ASSERT(level[0] != '\0');
	char * cmd = strdup(command);

	// find the command we're talking about
	char * sp = strchr(cmd, ' ');
	const char * command_name = cmd;
	const char * subcommand_name = 0;

	if(sp != 0)
	{
		// we're dealing with a subcommand.
		*sp = 0;
		subcommand_name = sp + 1;
	}

	size_t len1 = strlen(command_name);
	size_t len2 = subcommand_name ? strlen(subcommand_name) : 0;

	// look for the command.
	ChatCommand * p = &_commandTable[0];
	while(p->Name != 0)
	{
		if(!strnicmp(p->Name, command_name, len1))
		{
			// this is the one we wanna modify
			if(!subcommand_name)
			{
				// no subcommand, we can change it.
				p->CommandGroup = level[0];
				printf("Changing command level of command `%s` to %c.\n", p->Name, level[0]);
			}
			else
			{
				// assume this is a subcommand, loop the second set.
				ChatCommand * p2 = p->ChildCommands;
				if(!p2)
				{
					printf("Invalid command specified for override: %s\n", command_name);
				}
				else
				{
					while(p2->Name != 0)
					{
						if(!strnicmp("*",subcommand_name,1))
						{
								p2->CommandGroup = level[0];
								printf("Changing command level of command (wildcard) `%s`:`%s` to %c.\n", p->Name, p2->Name, level[0]);
						}else{
							if(!strnicmp(p2->Name, subcommand_name, len2))
							{
								// change the level
								p2->CommandGroup = level[0];
								printf("Changing command level of command `%s`:`%s` to %c.\n", p->Name, p2->Name, level[0]);
								break;
							}
						}
						p2++;
					}
					if(p2->Name == 0)
					{
						if(strnicmp("*",subcommand_name,1)) //Hacky.. meh.. -DGM
						{
							printf("Invalid subcommand referenced: `%s` under `%s`.\n", subcommand_name, p->Name);
						}
						break;
					}
				}
			}
			break;
		}
		++p;
	}

	if(p->Name == 0)
	{
		printf("Invalid command referenced: `%s`\n", command_name);
	}

	free(cmd);
}

void CommandTableStorage::Dealloc()
{
	free( _modifyCommandTable );
	free( _debugCommandTable );
	free( _waypointCommandTable );
	free( _GMTicketCommandTable );
	free( _GameObjectCommandTable );
	free( _BattlegroundCommandTable );
	free( _NPCCommandTable );
	free( _CheatCommandTable );
	free( _accountCommandTable );
	free( _petCommandTable );
	free( _recallCommandTable );
	free( _honorCommandTable );
	free( _GuildCommandTable);
	free( _questCommandTable );
	free( _charCommandTable );
	free( _commandTable );
}

void CommandTableStorage::Init()
{
	static ChatCommand modifyCommandTable[] =
	{
		{ "hp",		 '4', NULL,	"Health Points/HP",	NULL, UNIT_FIELD_HEALTH,	UNIT_FIELD_MAXHEALTH, 1 },
		{ "gender",	 '4', &ChatHandler::HandleGenderChanger,		 "修改性别 0=male / 1=female.",		NULL, 0, 0, 0},
		{ "race",	 '4', &ChatHandler::HandleRaceChanger,		 "修改种族 1=妖 / 2=人 /3=巫.",		NULL, 0, 0, 0},
		{ "class",	 '4', &ChatHandler::HandleClassChanger,		 "修改职业 1=武修 / 2=羽箭 /3=仙道 /4=真巫.",		NULL, 0, 0, 0},
		{ "mana",	   '4', NULL,	"Mana Points/MP",	  NULL, UNIT_FIELD_POWER1,	UNIT_FIELD_MAXPOWER1, 1 },
		{ "level",	  '4', &ChatHandler::HandleModifyLevelCommand,"等级", NULL, 0, 0, 0 },
		{ "armor",	  '4', NULL,	"Armor",			   NULL, UNIT_FIELD_RESISTANCES,			  0,		   1 },
		{ "holy",	   '4', NULL,	"Holy Resistance",	 NULL, UNIT_FIELD_RESISTANCES_01,	 0,		   1 },
		{ "fire",	   '4', NULL,	"Fire Resistance",	 NULL, UNIT_FIELD_RESISTANCES_02,	 0,		   1 },
		{ "nature",	 '4', NULL,	"Nature Resistance",   NULL, UNIT_FIELD_RESISTANCES_03,	 0,		   1 },
		{ "frost",	  '4', NULL,	"Frost Resistance",	NULL, UNIT_FIELD_RESISTANCES_04,	 0,		   1 },
		{ "shadow",	 '4', NULL,	"Shadow Resistance",   NULL, UNIT_FIELD_RESISTANCES_05,	 0,		   1 },
		{ "arcane",	 '4', NULL,	"Arcane Resistance",   NULL, UNIT_FIELD_RESISTANCES_06,	 0,		   1 },
		{ "damage",	 '4', NULL,	"伤害 最小/最大", NULL, UNIT_FIELD_MINDAMAGE,  UNIT_FIELD_MAXDAMAGE,2 },
		{ "scale",	  '4', NULL,	"Size/Scale",		  NULL, OBJECT_FIELD_SCALE_X, 0,					2 },
		{ "gold",	   '4', &ChatHandler::HandleModifyGoldCommand,  "Gold/Money/Copper",	  NULL,   0,  0,  0 },
		{ "speed",	  '4', &ChatHandler::HandleModifySpeedCommand, "移动速度",		 NULL,   0,  0,  0 },
		//{ "nativedisplayid", '3', NULL, "Native Display ID", NULL, UNIT_FIELD_NATIVEDISPLAYID, 0,			  1 },
		//{ "displayid" , '3', NULL,	"Display ID",		  NULL, UNIT_FIELD_DISPLAYID,	   0,			  1 },
		//{ "flags" ,	 '3', NULL,	"Unit Flags",		  NULL, UNIT_FIELD_FLAGS,		   0,			  1 },
		{ "faction",	'4', NULL,	"Faction Template",	NULL, UNIT_FIELD_FACTIONTEMPLATE, 0,			  1 },
		//{ "dynamicflags",'3',NULL,	"Dynamic Flags",	   NULL, UNIT_DYNAMIC_FLAGS,		 0,			  1 },
		//{ "talentpoints",'3',NULL,	"Talent Points",	   NULL, PLAYER_CHARACTER_POINTS1,   0,			  1 },
		{ "spirit",	 '4', NULL,	"Spirit",			  NULL, UNIT_FIELD_STAT0,		   0,			  1 },
		{ "boundingraidius",'4',NULL,  "Bounding Radius",	 NULL, UNIT_FIELD_BOUNDINGRADIUS,		 0,			  2 },
		{ "combatreach",'4',NULL,	 "Combat Reach",		NULL, UNIT_FIELD_COMBATREACH, 0, 2 },
		//{ "emotestate",'3', NULL,	 "NPC Emote State",	 NULL, UNIT_NPC_EMOTESTATE, 0, 1 },
		//{ "bytes",'3',NULL,"Bytes",NULL,UNIT_FIELD_BYTES_0,0,1},
		{ "damagetimes",'4',NULL,"damage times",NULL, UNIT_FIELD_ATTACK_POWER,0,1},
		{ "guildscore",	  '4', &ChatHandler::HandleModifyGuildScoreCommand, "公会积分",		 NULL,   0,  0,  0 },
		{ "guildwarscore",	  '4', &ChatHandler::HandleModifyGuildWarScoreCommand, "战斗积分",		 NULL,   0,  0,  0 },
		{ NULL,		  0, NULL,	 "",					NULL, 0, 0  },
	};
	dupe_command_table(modifyCommandTable, _modifyCommandTable);

	static ChatCommand debugCommandTable[] =
	{
		//{ "expratio",	'4', &ChatHandler::HandleAdjustWorldExpRatioCommand, "", 0, 0, 0},
		//{ "expratioall",	'4', &ChatHandler::HandleAdjustWorldAllExpRatioCommand, "", 0, 0, 0},
		{ "gametime",	'1', &ChatHandler::HandleDebugGameTimeCommand, "目标的沉迷时间", 0, 0, 0},
		{ "itemtime",	'4', &ChatHandler::HandleItemTimeCommand, "目标道具必须在道具包的第一个格子,显示它的剩余时间", 0, 0, 0},
		{ "infront",	 '1', &ChatHandler::HandleDebugInFrontCommand,  "",							   NULL, 0, 0, 0},
		//{ "showreact",   'd', &ChatHandler::HandleShowReactionCommand,  "",							   NULL, 0, 0, 0},
		{ "aimove",	  '4', &ChatHandler::HandleAIMoveCommand,		"",							   NULL, 0, 0, 0},
		{ "dist",		'4', &ChatHandler::HandleDistanceCommand,	  "",							   NULL, 0, 0, 0},
		{ "face",		'4', &ChatHandler::HandleFaceCommand,		  "",							   NULL, 0, 0, 0},
		{ "moveinfo",	'4', &ChatHandler::HandleMoveInfoCommand,	  "",							   NULL, 0, 0, 0},
		{ "setbytes",	'4', &ChatHandler::HandleSetBytesCommand,	  "",							   NULL, 0, 0, 0},
		//{ "getbytes",	'd', &ChatHandler::HandleGetBytesCommand,	  "",							   NULL, 0, 0, 0},
		//{ "unroot",	  'd', &ChatHandler::HandleDebugUnroot,		  "",							   NULL, 0, 0, 0},
		//{ "root",		'd', &ChatHandler::HandleDebugRoot,			"",							   NULL, 0, 0, 0},
		//{ "landwalk",	'd', &ChatHandler::HandleDebugLandWalk,		"",							   NULL, 0, 0, 0},
		//{ "waterwalk",   'd', &ChatHandler::HandleDebugWaterWalk,	   "",							   NULL, 0, 0, 0},
		//{ "castspellne", 'd', &ChatHandler::HandleCastSpellNECommand,   ".castspellne <spellid> - Casts spell on target (only plays animations, doesnt handle effects or range/facing/etc.", NULL, 0, 0, 0 },
		{ "aggrorange",  '4', &ChatHandler::HandleAggroRangeCommand,	".aggrorange - Shows aggro Range of the selected Creature.", NULL, 0, 0, 0 },
		{ "knockback ",  '4', &ChatHandler::HandleKnockBackCommand,	 ".knockback <value> - Knocks you back.", NULL, 0, 0, 0 },
		
		//{ "fade ",	   'd', &ChatHandler::HandleFadeCommand,		  ".fade <value> - calls ModThreatModifyer().", NULL, 0, 0, 0 },
		//{ "threatMod ",  'd', &ChatHandler::HandleThreatModCommand,	 ".threatMod <value> - calls ModGeneratedThreatModifyer().", NULL, 0, 0, 0 },
		//{ "calcThreat ", 'd', &ChatHandler::HandleCalcThreatCommand,	".calcThreat <dmg> <spellId> - calculates threat.", NULL, 0, 0, 0 },
		//{ "threatList ", 'd', &ChatHandler::HandleThreatListCommand,	".threatList  - returns all AI_Targets of the selected Creature.", NULL, 0, 0, 0 },
		//{ "gettptime",   'd', &ChatHandler::HandleGetTransporterTime,   "grabs transporter travel time",NULL, 0, 0, 0 },
		{ "itempushresult",'4',&ChatHandler::HandleSendItemPushResult,  "sends item push result", NULL, 0, 0, 0 },
		//{ "setbit",	  'd',  &ChatHandler::HandleModifyBitCommand,	"",							   NULL, 0, 0, 0},
		//{ "setvalue",	'd', &ChatHandler::HandleModifyValueCommand,   "",							   NULL, 0, 0, 0},
		//{ "aispelltestbegin", 'd', &ChatHandler::HandleAIAgentDebugBegin, "", NULL, 0, 0, 0 },
		//{ "aispelltestcontinue", 'd', &ChatHandler::HandleAIAgentDebugContinue, "", NULL, 0, 0, 0 },
		//{ "aispelltestskip", 'd', &ChatHandler::HandleAIAgentDebugSkip, "", NULL, 0, 0, 0 },
		//{ "dumpcoords", 'd', &ChatHandler::HandleDebugDumpCoordsCommmand, "", NULL, 0, 0, 0 },
        //{ "sendpacket", 'd', &ChatHandler::HandleSendpacket, "<opcode ID>, <data>", NULL, 0, 0, 0 },
		//{ "sqlquery", 'd', &ChatHandler::HandleSQLQueryCommand, "<sql query>", NULL, 0, 0, 0 },
		{ "rangecheck", '1', &ChatHandler::HandleRangeCheckCommand, "Checks the 'yard' range and internal range between the player and the target.", NULL, 0, 0, 0 },
		//{ "setallratings", 'd', &ChatHandler::HandleRatingsCommand, "Sets rating values to incremental numbers based on their index.", NULL, 0, 0, 0 },
		{ NULL,		   0, NULL,									  "",							   NULL, 0, 0  }
	};
	dupe_command_table(debugCommandTable, _debugCommandTable);

	static ChatCommand waypointCommandTable[] =
	{
		//{ "add",		 'w', &ChatHandler::HandleWPAddCommand,		 "Add wp at current pos",		  NULL, 0, 0, 0},
		{ "show",		'4', &ChatHandler::HandleWPShowCommand,		"Show wp's for creature",		 NULL, 0, 0, 0},
		{ "hide",		'4', &ChatHandler::HandleWPHideCommand,		"Hide wp's for creature",		 NULL, 0, 0, 0},
		//{ "delete",	  'w', &ChatHandler::HandleWPDeleteCommand,	  "Delete selected wp",			 NULL, 0, 0, 0},
		//{ "movehere",	'w', &ChatHandler::HandleWPMoveHereCommand,	"Move to this wp",				NULL, 0, 0, 0},
		//{ "flags",	   'w', &ChatHandler::HandleWPFlagsCommand,	   "Wp flags",					   NULL, 0, 0, 0},
		//{ "waittime",	'w', &ChatHandler::HandleWPWaitCommand,		"Wait time at this wp",		   NULL, 0, 0, 0},
		//{ "emote",	   'w', &ChatHandler::HandleWPEmoteCommand,	   "Emote at this wp",			   NULL, 0, 0, 0},
		//{ "skin",		'w', &ChatHandler::HandleWPSkinCommand,		"Skin at this wp",				NULL, 0, 0, 0},
		//{ "change",	  'w', &ChatHandler::HandleWPChangeNoCommand,	"Change at this wp",			  NULL, 0, 0, 0},
		//{ "info",		'w', &ChatHandler::HandleWPInfoCommand,		"Show info for wp",			   NULL, 0, 0, 0},
		//{ "movetype",	'w', &ChatHandler::HandleWPMoveTypeCommand,	"Movement type at wp",			NULL, 0, 0, 0},
		//{ "generate",	'w', &ChatHandler::HandleGenerateWaypoints,	"Randomly generate wps",		  NULL, 0, 0, 0},
		//{ "save",		 'w', &ChatHandler::HandleSaveWaypoints,		"Save all waypoints",			  NULL, 0, 0, 0},
		//{ "deleteall",	 'w', &ChatHandler::HandleDeleteWaypoints,	  "Delete all waypoints",			  NULL, 0, 0, 0},
		//{ "addfly", 'w', &ChatHandler::HandleWaypointAddFlyCommand, "Adds a flying waypoint", NULL, 0, 0, 0 },
		{ NULL,			0, NULL,									 "",							   NULL, 0, 0  }
	};
	dupe_command_table(waypointCommandTable, _waypointCommandTable);

	static ChatCommand GMTicketCommandTable[] =
	{
		//{ "get",		 'c', &ChatHandler::HandleGMTicketGetAllCommand,  "Gets GM Ticket",			   NULL, 0, 0, 0},
		//{ "getId",	   'c', &ChatHandler::HandleGMTicketGetByIdCommand, "Gets GM Ticket by ID",		 NULL, 0, 0, 0},
		//{ "delId",	   'c', &ChatHandler::HandleGMTicketDelByIdCommand, "Deletes GM Ticket by ID",	  NULL, 0, 0, 0},
		{ NULL,			0, NULL,									   "",							 NULL, 0, 0  }
	};
	dupe_command_table(GMTicketCommandTable, _GMTicketCommandTable);

	static ChatCommand GuildCommandTable[] =
	{
		{ "create",		 '4', &ChatHandler::CreateGuildCommand,  "Creates a guild.",			   NULL, 0, 0, 0},
		//{ "rename",	   'm', &ChatHandler::HandleRenameGuildCommand, "Renames a guild.",		 NULL, 0, 0, 0},
		{ "addscore",	   '4', &ChatHandler::HandleGuildAddScoreCommand, "Renames a guild.",		 NULL, 0, 0, 0},
		//{ "members",	   'm', &ChatHandler::HandleGuildMembersCommand, "Lists guildmembers and their ranks.",	  NULL, 0, 0, 0},
		//{ "removeplayer",	   'm', &ChatHandler::HandleGuildRemovePlayerCommand, "Removes a player from a guild.",		 NULL, 0, 0, 0},
		//{ "disband",	   'm', &ChatHandler::HandleGuildDisbandCommand, "Disbands the guild of your target.",		 NULL, 0, 0, 0},
		//{ "setcastletime",	   'm', &ChatHandler::HandleGuildSetCastleTime, "Set castle start fight time(day of week).",		 NULL, 0, 0, 0},
		{ NULL,			0, NULL,									   "",							 NULL, 0, 0  }
	};
	dupe_command_table(GuildCommandTable, _GuildCommandTable);

	static ChatCommand GameObjectCommandTable[] =
	{
		//{ "select",	  'o', &ChatHandler::HandleGOSelect,   "Selects the nearest GameObject to you",	NULL, 0, 0, 0},
		//{ "delete",	  'o', &ChatHandler::HandleGODelete,   "Deletes selected GameObject",			  NULL, 0, 0, 0},
		//{ "spawn",	   'o', &ChatHandler::HandleGOSpawn,	"Spawns a GameObject by ID",				NULL, 0, 0, 0},
		//{ "info",		'o', &ChatHandler::HandleGOInfo,	 "Gives you informations about selected GO", NULL, 0, 0, 0},
		//{ "activate",	'o', &ChatHandler::HandleGOActivate, "Activates/Opens the selected GO.",		 NULL, 0, 0, 0},
		//{ "enable",	  'o', &ChatHandler::HandleGOEnable,   "Enables the selected GO for use.",		 NULL, 0, 0, 0},
		//{ "scale",	   'o', &ChatHandler::HandleGOScale,	"Sets scale of selected GO",				NULL, 0, 0, 0},
		//{ "animprogress",'o', &ChatHandler::HandleGOAnimProgress, "Sets anim progress",				   NULL, 0, 0, 0 },
		//{ "export",	  'o', &ChatHandler::HandleGOExport,   "Exports the current GO selected",		  NULL, 0, 0, 0 },
		//{ "move", 'g', &ChatHandler::HandleGOMove, "Moves gameobject to player xyz", NULL, 0, 0, 0 },
		//{ "rotate", 'g', &ChatHandler::HandleGORotate, "Rotates gameobject x degrees", NULL, 0, 0, 0 },
		{ NULL,			0, NULL,						   "",										 NULL, 0, 0  }
	};
	dupe_command_table(GameObjectCommandTable, _GameObjectCommandTable);

	static ChatCommand BattlegroundCommandTable[] = 
	{
		//{ "setbgscore",  'e', &ChatHandler::HandleSetBGScoreCommand,	"<Teamid> <Score> - Sets battleground score. 2 Arguments. ", NULL, 0, 0, 0},
		//{ "startbg",	 'e', &ChatHandler::HandleStartBGCommand,	   "Starts current battleground match.",  NULL, 0, 0, 0},
		//{ "pausebg",	 'e', &ChatHandler::HandlePauseBGCommand,	   "Pauses current battleground match.",  NULL, 0, 0, 0},
		//{ "bginfo",	  'e', &ChatHandler::HandleBGInfoCommnad,		"Displays information about current battleground.", NULL, 0, 0, 0},
		//{ "battleground",'e', &ChatHandler::HandleBattlegroundCommand,  "Shows BG Menu",					   NULL, 0, 0, 0 },
		//{ "setworldstate",'e',&ChatHandler::HandleSetWorldStateCommand, "<var> <val> - Var can be in hex. WS Value.", NULL, 0, 0, 0 },
		//{ "playsound",   'e', &ChatHandler::HandlePlaySoundCommand,	 "<val>. Val can be in hex.",		   NULL, 0, 0, 0 },
		//{ "setbfstatus", 'e', &ChatHandler::HandleSetBattlefieldStatusCommand,".setbfstatus - NYI.",		   NULL, 0, 0, 0 },
		//{ "leave",	   'e', &ChatHandler::HandleBattlegroundExitCommand, "Leaves the current battleground.", NULL, 0, 0, 0 },
		{ NULL,			0, NULL,									 "",									NULL, 0, 0  }
	};
	dupe_command_table(BattlegroundCommandTable, _BattlegroundCommandTable);

	static ChatCommand NPCCommandTable[] =
	{
		{ "vendoradditem",   '4', &ChatHandler::HandleItemCommand,	  "Adds to vendor",				 NULL, 0, 0, 0},
		{ "vendorremoveitem",'4', &ChatHandler::HandleItemRemoveCommand,"Removes from vendor.",		   NULL, 0, 0, 0},
		{ "flags",	   '4', &ChatHandler::HandleNPCFlagCommand,	   "Changes NPC flags",			  NULL, 0, 0, 0},
		//{ "emote",	   'n', &ChatHandler::HandleEmoteCommand,		 ".emote - Sets emote state",	  NULL, 0, 0, 0 },
		//{ "remove",	  'n', &ChatHandler::HandleRemoveCommand,		"Deletes mob from world.", NULL, 0, 0, 0},
		//{ "delete",	  'n', &ChatHandler::HandleDeleteCommand,		"Deletes mob from db and world.", NULL, 0, 0, 0},
		{ "info",		'4', &ChatHandler::HandleNpcInfoCommand,	   "Displays NPC information",	   NULL, 0, 0, 0},
		//{ "addAgent",	'n', &ChatHandler::HandleAddAIAgentCommand,	".npc addAgent <agent> <procEvent> <procChance> <procCount> <spellId> <spellType> <spelltargetType> <spellCooldown> <floatMisc1> <Misc2>",NULL, 0, 0, 0},
		//{ "listAgent",   'n', &ChatHandler::HandleListAIAgentCommand,   ".npc listAgent",NULL, 0, 0, 0},
		{ "say",		 '4', &ChatHandler::HandleMonsterSayCommand,	".npc say <text> - Makes selected mob say text <text>.", NULL, 0, 0, 0 },
		//{ "yell",		'n', &ChatHandler::HandleMonsterYellCommand,   ".npc yell <Text> - Makes selected mob yell text <text>.", NULL, 0, 0, 0},
		{ "come",		'4', &ChatHandler::HandleNpcComeCommand,	   ".npc come - Makes npc move to your position", NULL, 0, 0, 0 },
		{ "return",	  '4', &ChatHandler::HandleNpcReturnCommand,	 ".npc return - Returns ncp to spawnpoint.", NULL, 0, 0, 0 },
		{ "spawn", '4', &ChatHandler::HandleCreatureSpawnCommand, ".npc spawn - Spawns npc of entry <id>", NULL, 0, 0, 0 },
		//{ "spawnline",	   'o', &ChatHandler::HandleCreatureSpawnLineCommand,	"Spawns a line creature by ID",				NULL, 0, 0, 0},
		//{ "spawnlink", 'n', &ChatHandler::HandleNpcSpawnLinkCommand, ".spawnlink sqlentry", NULL, 0, 0, 0 },
		//{ "possess", 'n', &ChatHandler::HandleNpcPossessCommand, ".npc possess - Possess an npc (mind control)", NULL, 0, 0, 0 },
		//{ "unpossess", 'n', &ChatHandler::HandleNpcUnPossessCommand, ".npc unpossess - Unposses any currently possessed npc.", NULL, 0, 0, 0 },
		//{ "select", 'n', &ChatHandler::HandleNpcSelectCommand, ".npc select - selects npc closest", NULL, 0, 0, 0 },
		{ NULL,		  2, NULL,						   "",										   NULL, 0, 0  }
	};
	dupe_command_table(NPCCommandTable, _NPCCommandTable);

	static ChatCommand CheatCommandTable[] =
	{
		{ "status",	 '4', &ChatHandler::HandleShowCheatsCommand, "Shows active cheats.",			 NULL, 0, 0, 0 },
		//{ "taxi",	   '3', &ChatHandler::HandleTaxiCheatCommand,	 "Enables all taxi nodes.",	   NULL, 0, 0, 0},
		{ "cooldown",   '4', &ChatHandler::HandleCooldownCheatCommand, "Enables no cooldown cheat.",	NULL, 0, 0, 0 },
		{ "casttime",   '4', &ChatHandler::HandleCastTimeCheatCommand, "Enables no cast time cheat.",   NULL, 0, 0, 0 },
		{ "power",	  '4', &ChatHandler::HandlePowerCheatCommand, "Disables mana consumption etc.",   NULL, 0, 0, 0 },
		{ "god",		'4', &ChatHandler::HandleGodModeCommand, "Sets god mode, prevents you from taking damage.", NULL, 0, 0, 0 },
		//{ "fly",		'3', &ChatHandler::HandleFlyCommand, "Sets fly mode",						   NULL, 0, 0, 0 },
		//{ "land",	   '3', &ChatHandler::HandleLandCommand, "Unsets fly mode",						NULL, 0, 0, 0 },
		//{ "explore",	'3', &ChatHandler::HandleExploreCheatCommand, "Reveals the unexplored parts of the map.", NULL, 0, 0, 0 },
		//{ "flyspeed", '3', &ChatHandler::HandleFlySpeedCheatCommand, "Modifies fly speed.", NULL, 0, 0, 0 },
		{ "stack",	  '4', &ChatHandler::HandleStackCheatCommand, "Enables aura stacking cheat.", NULL, 0, 0, 0 },
		{ "triggerpass", '4', &ChatHandler::HandleTriggerpassCheatCommand, "Ignores area trigger prerequisites.", NULL, 0, 0, 0 },
		{ NULL,		   0, NULL,							"",									   NULL, 0, 0, 0 },
	};
	dupe_command_table(CheatCommandTable, _CheatCommandTable);

	static ChatCommand accountCommandTable[] =
	{
		{ "ban",	  '2', &ChatHandler::HandleAccountBannedCommand,   "ban an account. .account ban name timeperiod", NULL, 0, 0, 0 },
		{ "unban",	  '3', &ChatHandler::HandleAccountUnbanCommand,		"unban an account x.", NULL, 0, 0, 0 },
		//{ "level",	  'z', &ChatHandler::HandleAccountLevelCommand,    "Sets gm level on account. Pass it username and 0,1,2,3,az, etc.", NULL, 0, 0, 0 },
		{ "mute",	  '2', &ChatHandler::HandleAccountMuteCommand,		"mute an account for a duration <timeperiod>.", NULL, 0, 0, 0 },
		{ "unmute",	  '3', &ChatHandler::HandleAccountUnmuteCommand,	"unmute an account <x>", NULL, 0, 0, 0 },

		{ NULL, 0, NULL, "", NULL, 0, 0, 0},
	};
	dupe_command_table(accountCommandTable, _accountCommandTable);

	static ChatCommand honorCommandTable[] =
	{
		{ "addpoints",   '4', &ChatHandler::HandleAddHonorCommand,	  "Adds x amount of honor points/currency",NULL,0,0,0},
		{ "addkills",	 '4', &ChatHandler::HandleAddKillCommand,	   "Adds x amount of honor kills", NULL, 0, 0, 0 },
		//{ "globaldailyupdate", 'm', &ChatHandler::HandleGlobalHonorDailyMaintenanceCommand, "Daily honor field moves", NULL, 0, 0, 0},
		//{ "singledailyupdate", 'm', &ChatHandler::HandleNextDayCommand, "Daily honor field moves for selected player only", NULL,0,0,0},
		//{ "pvpcredit", 'm', &ChatHandler::HandlePVPCreditCommand, "Sends PVP credit packet, with specified rank and points", NULL,0,0,0},
		{ NULL,0,NULL,"",NULL,0,0,0},
	};
	dupe_command_table(honorCommandTable, _honorCommandTable);

	static ChatCommand petCommandTable[] = 
	{
		//{ "createpet",'m',&ChatHandler::HandleCreatePetCommand, "Creates a pet with <entry>.", NULL, 0, 0, 0 },
		//{ "renamepet",'m',&ChatHandler::HandleRenamePetCommand, "Renames a pet to <name>.", NULL, 0, 0, 0 },
		//{ "addspell",'m',&ChatHandler::HandleAddPetSpellCommand, "Teaches pet <spell>.", NULL, 0, 0, 0 },
		//{ "removespell",'m',&ChatHandler::HandleRemovePetSpellCommand, "Removes pet spell <spell>.", NULL, 0, 0, 0 },
#ifdef USE_SPECIFIC_AIAGENTS
		{ "spawnbot", 'a', &ChatHandler::HandlePetSpawnAIBot, ".pet spawnbot <type> - spawn a helper bot for your aid", NULL, 0, 0, 0 },
#endif
		{ NULL,0,NULL,"",NULL,0,0,0},
	};
	dupe_command_table(petCommandTable, _petCommandTable);

	static ChatCommand recallCommandTable[] =
	{
		//{ "list",		'q', &ChatHandler::HandleRecallListCommand,	   "List recall locations",		  NULL, 0, 0, 0},
		//{ "port",		'q', &ChatHandler::HandleRecallGoCommand,		 "Port to recalled location",	  NULL, 0, 0, 0},
		//{ "add",		 'q', &ChatHandler::HandleRecallAddCommand,		"Add recall location",			NULL, 0, 0, 0},
		//{ "del",		 'q', &ChatHandler::HandleRecallDelCommand,		"Remove a recall location",	   NULL, 0, 0, 0},
		//{ "portplayer", 'm', &ChatHandler::HandleRecallPortPlayerCommand, "recall ports player", NULL, 0, 0, 0 },
		{ NULL,		   0,  NULL,										"",							   NULL, 0, 0, 0},
	};
	dupe_command_table(recallCommandTable, _recallCommandTable);

	static ChatCommand questCommandTable[] =
	{
		//{ "addboth",   'q', &ChatHandler::HandleQuestAddBothCommand,	"Add quest <id> to the targeted NPC as start & finish",	NULL, 0, 0, 0},
		//{ "addfinish", 'q', &ChatHandler::HandleQuestAddFinishCommand,	"Add quest <id> to the targeted NPC as finisher",		NULL, 0, 0, 0},
		//{ "addstart",  'q', &ChatHandler::HandleQuestAddStartCommand,	"Add quest <id> to the targeted NPC as starter",		NULL, 0, 0, 0},
		//{ "delboth",   'q', &ChatHandler::HandleQuestDelBothCommand,	"Delete quest <id> from the targeted NPC as start & finish",	NULL, 0, 0, 0},
		//{ "delfinish", 'q', &ChatHandler::HandleQuestDelFinishCommand,	"Delete quest <id> from the targeted NPC as finisher",	NULL, 0, 0, 0},
		//{ "delstart",  'q', &ChatHandler::HandleQuestDelStartCommand,	"Delete quest <id> from the targeted NPC as starter",	NULL, 0, 0, 0},
		{ "complete",  '4', &ChatHandler::HandleQuestFinishCommand,	    "Complete/Finish quest <id>",							NULL, 0, 0, 0},
		{ "finisher",  '4', &ChatHandler::HandleQuestFinisherCommand,	"Lookup quest finisher for quest <id>",					NULL, 0, 0, 0},
		//{ "item",	   '3', &ChatHandler::HandleQuestItemCommand,		"Lookup itemid necessary for quest <id>",				NULL, 0, 0, 0},
		{ "list",	   '4', &ChatHandler::HandleQuestListCommand,		"Lists the quests for the npc <id>",					NULL, 0, 0, 0},
		{ "load",	   '4', &ChatHandler::HandleQuestLoadCommand,		"Loads quests from database",							NULL, 0, 0, 0},
		{ "lookup",	   '4', &ChatHandler::HandleQuestLookupCommand,		"Looks up quest string x",								NULL, 0, 0, 0},
		{ "giver",	   '4', &ChatHandler::HandleQuestGiverCommand,		"Lookup quest giver for quest <id>",					NULL, 0, 0, 0},
		{ "remove",	   '4', &ChatHandler::HandleQuestRemoveCommand,		"Removes the quest <id> from the targeted player",		NULL, 0, 0, 0},
		//{ "reward",	   'q', &ChatHandler::HandleQuestRewardCommand,		"Shows reward for quest <id>",							NULL, 0, 0, 0},
		//{ "status",	   'q', &ChatHandler::HandleQuestStatusCommand,		"Lists the status of quest <id>",						NULL, 0, 0, 0},
		{ "spawn",	   '4', &ChatHandler::HandleQuestSpawnCommand,		"Port to spawn location for quest <id>",				NULL, 0, 0, 0},
		{ "start",	   '4', &ChatHandler::HandleQuestStartCommand,		"Starts quest <id>",									NULL, 0, 0, 0},
		{ NULL,		    0,  NULL,										"",														NULL, 0, 0, 0},
	};
	dupe_command_table(questCommandTable, _questCommandTable);

	static ChatCommand charCommandTable[] =
	{
		{ "levelup",	'4', &ChatHandler::HandleLevelUpCommand,		"Levelup x lvls",				   NULL, 0, 0, 0},
		{ "renamechar", '3', &ChatHandler::HandleRenameCommand, "Renames character x to y.", NULL, 0, 0, 0 },
		//{ "copychar", '1', &ChatHandler::HandleCopyCommand, "Copy character's name", NULL, 0, 0, 0 },
		//{ "forcerenamechar", '2', &ChatHandler::HandleForceRenameCommand, "Forces character x to rename his char next login", NULL, 0, 0, 0 },
		{ "removeitem",  '4', &ChatHandler::HandleRemoveItemCommand,	"Removes item %u count %u.", NULL, 0, 0, 0 },
		{ "resetspells", '4', &ChatHandler::HandleResetSpellsCommand,   ".resetspells - Resets all spells to starting spells of targeted player. DANGEROUS.", NULL, 0, 0, 0 },
		//{ "resettalents",'s', &ChatHandler::HandleResetTalentsCommand,  ".resettalents - Resets all talents of targeted player to that of their current level. DANGEROUS.", NULL, 0, 0, 0 },
		//{ "resetskills", 's', &ChatHandler::HandleResetSkillsCommand ,  ".resetskills - Resets all skills.", NULL, 0, 0, 0 },
		{ "learn",	   '4', &ChatHandler::HandleLearnCommand,		 "Learns spell",				   NULL, 0, 0, 0},
		{ "unlearn",	 '4', &ChatHandler::HandleUnlearnCommand,	   "Unlearns spell",				 NULL, 0, 0, 0},
		{ "learnclass",	   '4', &ChatHandler::HandleLearnClassCommand,		 "Learns class's spell",				   NULL, 0, 0, 0},
		//{ "getskilllevel", 's', &ChatHandler::HandleGetSkillLevelCommand, "Gets the current level of a skill",NULL,0,0,0}, //DGM (maybe add to playerinfo?)
        //{ "getskillinfo", 's', &ChatHandler::HandleGetSkillsInfoCommand, "Gets all the skills from a player",NULL,0,0,0},
		//{ "learnskill",  '2', &ChatHandler::HandleLearnSkillCommand,	".learnskill <skillid> (optional) <value> <maxvalue> - Learns skill id skillid.", NULL, 0, 0, 0},
		//{ "advanceskill",'s', &ChatHandler::HandleModifySkillCommand,   "advanceskill <skillid> <amount, optional, default = 1> - Advances skill line x times..", NULL, 0, 0, 0},
		//{ "removeskill", 's', &ChatHandler::HandleRemoveSkillCommand,   ".removeskill <skillid> - Removes skill",		 NULL, 0, 0, 0 },
		//{ "increaseweaponskill", 's', &ChatHandler::HandleIncreaseWeaponSkill, ".increaseweaponskill <count> - Increase eqipped weapon skill x times (defaults to 1).", NULL, 0, 0, 0},
		{ "additem",	 '4', &ChatHandler::HandleAddInvItemCommand,	"add an item .additem entry count ",							   NULL, 0, 0, 0},
		//{ "addmapitem",	 '4', &ChatHandler::HandleAddMapItemCommand,	"",							   NULL, 0, 0, 0},
		{ "additemwithtime",	 'i', &ChatHandler::HandleAddItemWithTimeCommand,	"",							   NULL, 0, 0, 0},
		//{ "additemset",	'4', &ChatHandler::HandleAddItemSetCommand,	"Adds item set to inv.",		  NULL, 0, 0, 0 },
		{ "addyuanbao", '4', &ChatHandler::HandleAddYuanbaoCommand, "Add Yuanbao.", NULL, 0, 0, 0 },
		{ "addfriend", '1', &ChatHandler::HandleAddFriendCommand, "Adds player x as friend to player y.", NULL, 0, 0, 0},
		{ "mute",	  '2', &ChatHandler::HandleCharMuteCommand,		"mute a character for a duration <timeperiod>.", NULL, 0, 0, 0 },
		{ "unmute",	  '3', &ChatHandler::HandleCharUnmuteCommand,	"unmute a character <x>", NULL, 0, 0, 0 },
		//{ "buycastlenpc", '2', &ChatHandler::HandleBuyCastleNpcCommand, "buy castle npcs.", NULL, 0, 0, 0 },
		{ NULL,			0, NULL,									   "",							 NULL, 0, 0  }
	};
	dupe_command_table(charCommandTable, _charCommandTable);

	static ChatCommand commandTable[] = {
		//{ "renameguild", 'a', &ChatHandler::HandleRenameGuildCommand, "Renames a guild.", NULL, 0, 0, 0 },
		//{ "masssummon", 'z', &ChatHandler::HandleMassSummonCommand, ".masssummon - Summons all players.", NULL, 0, 0, 0},
		{ "commands",	'1', &ChatHandler::HandleCommandsCommand,		"Shows Commands",				 NULL, 0, 0, 0},
		//{ "help",		'0', &ChatHandler::HandleHelpCommand,			"Shows help for command",		 NULL, 0, 0, 0},
		{ "announce",	'2', &ChatHandler::HandleAnnounceCommand,	  "Sends Msg To All",			   NULL, 0, 0, 0},
		//{ "wannounce",   '1', &ChatHandler::HandleWAnnounceCommand,	 "Sends Widescreen Msg To All",	NULL, 0, 0, 0},
		{ "gmannounce", '3', &ChatHandler::HandleGMAnnounceCommand, "Sends Msg to all online GMs", NULL, 0, 0, 0},
		{ "systemnotifyadd", '3', &ChatHandler::HandleSystemNotifyAddCommand, "add system notify", NULL, 0, 0, 0},
		{ "systemnotifyremove", '2', &ChatHandler::HandleSystemNotifyRemoveCommand, "remove system notify", NULL, 0, 0, 0},
		{ "appear",	  '1', &ChatHandler::HandleAppearCommand,		"Teleports to x's position.",	 NULL, 0, 0, 0},
		{ "summon",	  '1', &ChatHandler::HandleSummonCommand,		"Summons x to your position",	 NULL, 0, 0, 0},
		{ "banchar",	 '3', &ChatHandler::HandleBanCharacterCommand,  "Bans character x with or without reason",			  NULL, 0, 0, 0},
		{ "unbanchar",   '3', &ChatHandler::HandleUnBanCharacterCommand,"Unbans character x",			 NULL, 0, 0, 0},
		{ "kick",		'2', &ChatHandler::HandleKickCommand,		  "Kicks player from server",	   NULL, 0, 0, 0},
		{ "kill",		'4', &ChatHandler::HandleKillCommand,		  ".kill - Kills selected unit.",   NULL, 0, 0, 0},
		{ "killplr" ,   '4', &ChatHandler::HandleKillByPlrCommand,         ".killplr <name> - Kills specified player" , NULL , 0 , 0 , 0 },
		{ "revive",	  '4', &ChatHandler::HandleReviveCommand,		"Revives you.",				   NULL, 0, 0, 0},
		{ "reviveplr",   '4', &ChatHandler::HandleReviveStringcommand,  "Revives player specified.",	  NULL, 0, 0, 0},
		//{ "demorph",	 'm', &ChatHandler::HandleDeMorphCommand,	   "Demorphs from morphed model.",   NULL, 0, 0, 0},
		//{ "mount",	   'm', &ChatHandler::HandleMountCommand,		 "Mounts into modelid x.",		 NULL, 0, 0, 0},
		{ "dismount",	  '4', &ChatHandler::HandleDismountCommand,	  "Dismounts.",					 NULL, 0, 0, 0},
		{ "gmlist",		  '1', &ChatHandler::HandleGMListCommand,		"Shows active GM's",			  NULL, 0, 0, 0},
		//{ "gmoff",	   't', &ChatHandler::HandleGMOffCommand,		 "Sets GM tag off",				NULL, 0, 0, 0},
		//{ "gmon",		't', &ChatHandler::HandleGMOnCommand,		  "Sets GM tag on",				 NULL, 0, 0, 0},
		//{ "gps",		 '0', &ChatHandler::HandleGPSCommand,		   "Shows Position",				 NULL, 0, 0, 0},
		{ "info",		'4', &ChatHandler::HandleInfoCommand,		  "Server info",					NULL, 0, 0, 0},
		{ "worldport",   '1', &ChatHandler::HandleWorldPortCommand,	 "",							   NULL, 0, 0, 0},
		//{ "save",		's', &ChatHandler::HandleSaveCommand,		  "Save's your character",		  NULL, 0, 0, 0},
		{ "saveall",	 '4', &ChatHandler::HandleSaveAllCommand,	   "Save's all playing characters",  NULL, 0, 0, 0},
		{ "start",	   '4', &ChatHandler::HandleStartCommand,		 "Teleport's you to a starting location",							   NULL, 0, 0, 0},
		{ "additem",	 '4', &ChatHandler::HandleAddInvItemCommand,	"",							   NULL, 0, 0, 0},
		{ "addmapitem",	 '4', &ChatHandler::HandleAddMapItemCommand,	"",							   NULL, 0, 0, 0},
		{ "adddefaultitem",	 '4', &ChatHandler::HandleAddDefaultItemCommand,	"add a set of items .adddefaultitem id lv",							   NULL, 0, 0, 0},
		{ "addtitle",	 '4', &ChatHandler::HandleAddTitleCommand,	"add a title .addtitle id",							   NULL, 0, 0, 0},
		{ "invincible",  '1', &ChatHandler::HandleInvincibleCommand,	".invincible - Toggles INVINCIBILITY (mobs won't attack you)", NULL, 0, 0, 0},
		{ "invisible",   '1', &ChatHandler::HandleInvisibleCommand,	 ".invisible - Toggles INVINCIBILITY and INVISIBILITY (mobs won't attack you and nobody can see you, but they can see your chat messages)", NULL, 0, 0, 0},
		{ "playerinfo",  '1', &ChatHandler::HandlePlayerInfo,		   ".playerinfo - Displays informations about the selected character (account...)", NULL, 0, 0, 0 },
		{ "modify",		'4', NULL,									 "",				 modifyCommandTable, 0, 0, 0},
		{ "waypoint",	  '4', NULL,									 "",			   waypointCommandTable, 0, 0, 0},
		{ "debug",		 '1', NULL,									 "",				  debugCommandTable, 0, 0, 0},
		//{ "gmTicket",	  'c', NULL,									 "",			   GMTicketCommandTable, 0, 0, 0},
		//{ "gobject",	   'o', NULL,									 "",			 GameObjectCommandTable, 0, 0, 0},
		//{ "battleground",  'e', NULL,									 "",		   BattlegroundCommandTable, 0, 0, 0},
		{ "npc"		 ,  '4', NULL,									 "",					NPCCommandTable, 0, 0, 0},
		{ "cheat"	   ,  '4', NULL,									 "",				  CheatCommandTable, 0, 0, 0},
		{ "account"	   ,  '2', NULL,									 "",				  accountCommandTable, 0, 0, 0},
		{ "honor"	   ,  '4', NULL,									 "",				  honorCommandTable, 0, 0, 0},
		{ "quest",		'4', NULL,									 "",				 questCommandTable, 0, 0, 0},
		//{ "pet",		   'm', NULL,									 "",					petCommandTable, 0, 0, 0},
		//{ "recall",		'q', NULL,									 "",				 recallCommandTable, 0, 0, 0},
		{ "guild",		'4', NULL,									 "",				 GuildCommandTable, 0, 0, 0},
		{ "character", '1', NULL,									 "",				 charCommandTable, 0, 0, 0},
		//{ "getpos"	  ,  'd', &ChatHandler::HandleGetPosCommand,		"",							   NULL, 0, 0, 0},
		//{ "clearcooldowns", 'm', &ChatHandler::HandleClearCooldownsCommand, "Clears all cooldowns for your class.", NULL, 0, 0, 0 },
		{ "removeauras",   '4', &ChatHandler::HandleRemoveAurasCommand,   "Removes all auras from target",  NULL, 0, 0, 0},
		//{ "paralyze",	  'b', &ChatHandler::HandleParalyzeCommand,	  "Roots/Paralyzes the target.",	NULL, 0, 0, 0 },
		//{ "unparalyze",	'b', &ChatHandler::HandleUnParalyzeCommand,	"Unroots/Unparalyzes the target.",NULL, 0, 0, 0 },
		//{ "setmotd",	   'm', &ChatHandler::HandleSetMotdCommand,	   "Sets MOTD",					  NULL, 0, 0, 0 },
		//{ "gotrig",		'v', &ChatHandler::HandleTriggerCommand,	   "Warps to areatrigger <id>",	  NULL, 0, 0, 0 },
		{ "exitinstance",  '2', &ChatHandler::HandleExitInstanceCommand,  "Exits current instance, return to entry point.", NULL, 0, 0, 0 },
		{ "reloadtable",	  '4', &ChatHandler::HandleDBReloadCommand,	  "Reloads some of the database tables", NULL, 0, 0, 0 },
		{ "servershutdown", '4', &ChatHandler::HandleShutdownCommand, "Initiates server shutdown in <x> seconds.", NULL, 0, 0, 0 },
		//{ "serverrestart", 'z', &ChatHandler::HandleShutdownRestartCommand, "Initiates server restart in <x> seconds.", NULL, 0, 0, 0 },
		//{ "allowwhispers", 'c', &ChatHandler::HandleAllowWhispersCommand, "Allows whispers from player <s> while in gmon mode.", NULL, 0, 0, 0 },
		//{ "blockwhispers", 'c', &ChatHandler::HandleBlockWhispersCommand, "Blocks whispers from player <s> while in gmon mode.", NULL, 0, 0, 0 },
		//{ "advanceallskills", 'm', &ChatHandler::HandleAdvanceAllSkillsCommand, "Advances all skills <x> points.", NULL, 0, 0, 0 },
		//{ "killbyplayer", 'f', &ChatHandler::HandleKillByPlayerCommand, "Disconnects the player with name <s>.", NULL, 0, 0, 0 },
		//{ "killbyaccount", 'f', &ChatHandler::HandleKillBySessionCommand, "Disconnects the session with account name <s>.", NULL, 0, 0, 0 },
		//{ "killbyip", 'f', &ChatHandler::HandleKillByIPCommand, "Disconnects the session with the ip <s>.", NULL, 0, 0, 0 },
		//{ "castall", 'z', &ChatHandler::HandleCastAllCommand, "Makes all players online cast spell <x>.", NULL, 0, 0, 0},
		//{ "dispelall", 'z', &ChatHandler::HandleDispelAllCommand, "Dispels all negative (or positive w/ 1) auras on all players.",NULL,0,0,0},
		//{ "castspell",   'd', &ChatHandler::HandleCastSpellCommand,	 ".castspell <spellid> - Casts spell on target.",  NULL, 0, 0, 0 },
		//{ "modperiod" , 'm', &ChatHandler::HandleModPeriodCommand, "Changes period of current transporter.", NULL, 0, 0, 0 },
		{ "npcfollow", '4', &ChatHandler::HandleNpcFollowCommand, "Sets npc to follow you", NULL, 0, 0, 0 },
		{ "nullfollow", '4', &ChatHandler::HandleNullFollowCommand, "Sets npc to not follow anything", NULL, 0, 0, 0 },
		//{ "formationlink1", 'm', &ChatHandler::HandleFormationLink1Command, "Sets formation master.", NULL, 0, 0, 0 },
		//{ "formationlink2", 'm', &ChatHandler::HandleFormationLink2Command, "Sets formation slave with distance and angle", NULL, 0, 0, 0 },
		//{ "formationclear", 'm', &ChatHandler::HandleFormationClearCommand, "Removes formation from creature", NULL, 0, 0, 0 },
		//{ "playall", 'z', &ChatHandler::HandleGlobalPlaySoundCommand, "Plays a sound to the entire server.", NULL, 0, 0, 0 },
		//{ "addipban", 'm', &ChatHandler::HandleIPBanCommand, "Adds an address to the IP ban table: <address> [duration]\nDuration must be a number optionally followed by a character representing the calendar subdivision to use (h>hours, d>days, w>weeks, m>months, y>years, default minutes)\nLack of duration results in a permanent ban.", NULL, 0, 0, 0 },
		//{ "delipban", 'm', &ChatHandler::HandleIPUnBanCommand, "Deletes an address from the IP ban table: <address>", NULL, 0, 0, 0},
		{ "lookupitem", '1', &ChatHandler::HandleLookupItemCommand, "Looks up item string x.", NULL, 0, 0, 0 },
		{ "lookupquest", '1', &ChatHandler::HandleQuestLookupCommand, "Looks up quest string x.", NULL, 0, 0, 0 },
		{ "lookupcreature", '1', &ChatHandler::HandleLookupCreatureCommand, "Looks up item string x.", NULL, 0, 0, 0 },
		//{ "reloadscripts", 'w', &ChatHandler::HandleReloadScriptsCommand, "Reloads GM Scripts", NULL, 0, 0, 0 },
		//{ "rehash", 'z', &ChatHandler::HandleRehashCommand, "Reloads config file.", NULL, 0, 0, 0 },
		//{ "createarenateam", 'g', &ChatHandler::HandleCreateArenaTeamCommands, "Creates arena team", NULL, 0, 0, 0 },
		//{ "whisperblock", 'g', &ChatHandler::HandleWhisperBlockCommand, "Blocks like .gmon except without the <GM> tag", NULL, 0, 0, 0 },
		//{ "logcomment" , '1' , &ChatHandler::HandleGmLogCommentCommand, "Adds a comment to the GM log for the admins to read." , NULL , 0 , 0 , 0 },
		//{ "showitems", 'm', &ChatHandler::HandleShowItems, "test for ItemIterator", NULL, 0, 0, 0 },
		{ "testlos", '4', &ChatHandler::HandleCollisionTestLOS, "tests los", NULL, 0, 0, 0 },
		{ "testindoor", '4', &ChatHandler::HandleCollisionTestIndoor, "tests indoor", NULL, 0, 0, 0 },
		{ "getheight", '4', &ChatHandler::HandleCollisionGetHeight, "Gets height", NULL, 0, 0, 0 },
		{ "renameallinvalidchars", '4', &ChatHandler::HandleRenameAllCharacter, "Renames all invalid character names", NULL, 0,0, 0 },
		//{ "removesickness",   'm', &ChatHandler::HandleRemoveRessurectionSickessAuraCommand,   "Removes ressurrection sickness from the target",  NULL, 0, 0, 0},
		{ "fixscale", '4', &ChatHandler::HandleFixScaleCommand, "", NULL, 0, 0, 0 },
		//{ "addtrainerspell", 'm', &ChatHandler::HandleAddTrainerSpellCommand, "", NULL, 0, 0, 0 },
		{ "queryaccount", '1', &ChatHandler::HandleQueryAccountCommand, "query account information by a character name.", NULL, 0, 0, 0 },
	
		{ NULL,		  0, NULL,										 "",							   NULL, 0, 0  }
	};
	dupe_command_table(commandTable, _commandTable);

	/* set the correct pointers */
	ChatCommand * p = &_commandTable[0];
	while(p->Name != 0)
	{
		if(p->ChildCommands != 0)
		{
			// Set the correct pointer.
			ChatCommand * np = GetSubCommandTable(p->Name);
			ASSERT(np);
			p->ChildCommands = np;
		}
		++p;
	}
}

ChatHandler::ChatHandler()
{
	new CommandTableStorage;
	CommandTableStorage::getSingleton().Init();
	SkillNameManager = new SkillNameMgr;
}

ChatHandler::~ChatHandler()
{
	CommandTableStorage::getSingleton().Dealloc();
	delete CommandTableStorage::getSingletonPtr();
	delete SkillNameManager;
}

bool ChatHandler::hasStringAbbr(const char* s1, const char* s2)
{
	for(;;)
	{
		if( !*s2 )
			return true;
		else if( !*s1 )
			return false;
		else if( tolower( *s1 ) != tolower( *s2 ) )
			return false;
		s1++; s2++;
	}
}

void ChatHandler::SendMultilineMessage(WorldSession *m_session, const char *str)
{
	char * start = (char*)str, *end;
	for(;;)
	{
        end = strchr(start, '\n');
		if(!end)
			break;

		*end = '\0';
		SystemMessage(m_session, start);
		start = end + 1;
	}
	if(*start != '\0')
		SystemMessage(m_session, start);
}

bool ChatHandler::ExecuteCommandInTable(ChatCommand *table, const char* text, WorldSession *m_session)
{
	std::string cmd = "";

	// get command
	while (*text != ' ' && *text != '\0')
	{
		cmd += *text;
		text++;
	}

	while (*text == ' ') text++; // skip whitespace

	if(!cmd.length())
		return false;

	for(uint32 i = 0; table[i].Name != NULL; i++)
	{
		if(!hasStringAbbr(table[i].Name, cmd.c_str()))
			continue;

		if(table[i].CommandGroup != '0' && !m_session->CanUseCommand(table[i].CommandGroup))
			continue;

		if(table[i].ChildCommands != NULL)
		{
			if(!ExecuteCommandInTable(table[i].ChildCommands, text, m_session))
			{
				if(table[i].Help != "")
					SendMultilineMessage(m_session, table[i].Help.c_str());
				else
				{
					GreenSystemMessage(m_session, "sub-command can use:");
					for(uint32 k=0; table[i].ChildCommands[k].Name;k++)
					{
						if(table[i].ChildCommands[k].CommandGroup != '0' && m_session->CanUseCommand(table[i].ChildCommands[k].CommandGroup))
							SystemMessage(m_session, " <color:ffffff00>%s<br> <color:ffff0000><br> <color:ff00ccff>%s<br>", table[i].ChildCommands[k].Name, table[i].ChildCommands[k].Help.size() ? table[i].ChildCommands[k].Help.c_str() : "No Help Available");
					}
				}
			}

			return true;
		}
		
		// Check for field-based commands
		if(table[i].Handler == NULL && (table[i].MaxValueField || table[i].NormalValueField))
		{
			bool result = false;
			if(strlen(text) == 0)
			{
				RedSystemMessage(m_session, "No values specified.");
			}
			if(table[i].ValueType == 2)
				result = CmdSetFloatField(m_session, table[i].NormalValueField, table[i].MaxValueField, table[i].Name, text);
			else
				result = CmdSetValueField(m_session, table[i].NormalValueField, table[i].MaxValueField, table[i].Name, text);
			if(!result)
				RedSystemMessage(m_session, "Must be in the form of (command) <value>, or, (command) <value> <maxvalue>");
		}
		else
		{
			if(!(this->*(table[i].Handler))(text, m_session))
			{
				if(table[i].Help != "")
					SendMultilineMessage(m_session, table[i].Help.c_str());
				else
				{
					RedSystemMessage(m_session, "syntax error. please use .help %s to query how to use it.", table[i].Name);
				}
			}
		}

		return true;
	}
	return false;
}

int ChatHandler::ParseCommands(const char* text, WorldSession *session)
{
	if (!session)
		return 0;

	if(!*text)
		return 0;

	if(session->GetPermissionCount() == 0 && sWorld.m_reqGmForCommands)
		return 0;

	if(text[0] != '!' && text[0] != '.') // let's not confuse users
		return 0;

	/* skip '..' :P that pisses me off */
	if(text[1] == '.')
		return 0;

	text++;

	if(!ExecuteCommandInTable(CommandTableStorage::getSingleton().Get(), text, session))
	{
		SystemMessage(session, "no such command or permission is low level.");
	}

	return 1;
}

void ChatHandler::FillMessageData( uint32 type, const char *message,uint64 guid ,MSG_S2C::stChat_Message* Msg, uint8 flag) const
{
	ASSERT(type != CHAT_MSG_CHANNEL);
	   //channels are handled in channel handler and so on
	Msg->type = type;
	Msg->guid = guid;
	Msg->txt  = message;
	Msg->flag = flag;
}

void ChatHandler::FillSystemMessageData(const char *message, MSG_S2C::stChat_Message* Msg) const
{
	Msg->type = CHAT_MSG_SYSTEM;
	Msg->txt  = message;
}

Player * ChatHandler::getSelectedChar(WorldSession *m_session, bool showerror)
{
	uint64 guid;
	Player *chr;
	
	guid = m_session->GetPlayer()->GetSelection();
	
	if (guid == 0)
	{
		if(showerror) 
			GreenSystemMessage(m_session, "Auto-targeting self.");
		chr = m_session->GetPlayer(); // autoselect
	}
	else
		chr = m_session->GetPlayer()->GetMapMgr()->GetPlayer((uint32)guid);
	
	if(chr == NULL)
	{
		if(showerror) 
			RedSystemMessage(m_session, "you must select a player.");
		return NULL;
	}

	return chr;
}

Creature * ChatHandler::getSelectedCreature(WorldSession *m_session, bool showerror)
{
	uint64 guid;
	Creature *creature = NULL;

	guid = m_session->GetPlayer()->GetSelection();
	if(GET_TYPE_FROM_GUID(guid) == HIGHGUID_TYPE_PET)
		creature = m_session->GetPlayer()->GetMapMgr()->GetPet( GET_LOWGUID_PART(guid) );
	else if(GET_TYPE_FROM_GUID(guid) == HIGHGUID_TYPE_UNIT)
		creature = m_session->GetPlayer()->GetMapMgr()->GetCreature( GET_LOWGUID_PART(guid) );
	
	if(creature != NULL)
		return creature;
	else
	{
		if(showerror) 
			RedSystemMessage(m_session, "you must select a creature.");
		return NULL;
	}
}

void ChatHandler::SystemMessage(WorldSession *m_session, const char* message, ...)
{
	if( !message ) return;
	va_list ap;
	va_start(ap, message);
	char msg1[1024];
	vsnprintf(msg1,1024, message,ap);
	MSG_S2C::stChat_Message Msg;
	FillSystemMessageData(msg1,&Msg);
	if(m_session != NULL) 
		m_session->SendPacket(Msg);
}

void ChatHandler::ColorSystemMessage(WorldSession *m_session, const char* colorcode, const char *message, ...)
{
	if( !message ) return;
	va_list ap;
	va_start(ap, message);
	char msg1[1024];
	vsnprintf(msg1,1024, message,ap);
	char msg[1024];
	snprintf(msg, 1024, "%s%s\r", colorcode, msg1);
	MSG_S2C::stChat_Message Msg;
	FillSystemMessageData(msg, &Msg);
	if(m_session != NULL) 
		m_session->SendPacket(Msg);
}

void ChatHandler::RedSystemMessage(WorldSession *m_session, const char *message, ...)
{
	if( !message ) return;
	va_list ap;
	va_start(ap, message);
	char msg1[1024];
	vsnprintf(msg1,1024,message,ap);
	char msg[1024];
	snprintf(msg, 1024,"%s%s\r", MSG_COLOR_LIGHTRED/*MSG_COLOR_RED*/, msg1);
	MSG_S2C::stChat_Message Msg;
	FillSystemMessageData(msg, &Msg);
	if(m_session != NULL) 
		m_session->SendPacket(Msg);
}

void ChatHandler::GreenSystemMessage(WorldSession *m_session, const char *message, ...)
{
	return;

	if( !message ) return;
	va_list ap;
	va_start(ap, message);
	char msg1[1024];
	vsnprintf(msg1,1024, message,ap);
	char msg[1024];
	snprintf(msg, 1024, "%s%s\r", MSG_COLOR_LIGHTRED, msg1);
	MSG_S2C::stChat_Message Msg;
	FillSystemMessageData(msg, &Msg);
	if(m_session != NULL) 
		m_session->SendPacket(Msg);
}

void ChatHandler::BlueSystemMessage(WorldSession *m_session, const char *message, ...)
{
	if( !message ) return;
	va_list ap;
	va_start(ap, message);
	char msg1[1024];
	vsnprintf(msg1,1024, message,ap);
	char msg[1024];
	snprintf(msg, 1024,"%s%s\r", MSG_COLOR_LIGHTBLUE, msg1);
	MSG_S2C::stChat_Message Msg;
	FillSystemMessageData(msg, &Msg);
	if(m_session != NULL) 
		m_session->SendPacket(Msg);
}

void ChatHandler::RedSystemMessageToPlr(Player* plr, const char *message, ...)
{
	if( !message || !plr->GetSession() ) return;
	va_list ap;
	va_start(ap, message);
	char msg1[1024];
	vsnprintf(msg1,1024,message,ap);
	RedSystemMessage(plr->GetSession(), (const char*)msg1);
}

void ChatHandler::GreenSystemMessageToPlr(Player* plr, const char *message, ...)
{
	if( !message || !plr->GetSession() ) return;
	va_list ap;
	va_start(ap, message);
	char msg1[1024];
	vsnprintf(msg1,1024,message,ap);
	GreenSystemMessage(plr->GetSession(), (const char*)msg1);
}

void ChatHandler::BlueSystemMessageToPlr(Player* plr, const char *message, ...)
{
	if( !message || !plr->GetSession() ) return;
	va_list ap;
	va_start(ap, message);
	char msg1[1024];
	vsnprintf(msg1,1024,message,ap);
	BlueSystemMessage(plr->GetSession(), (const char*)msg1);
}

void ChatHandler::SystemMessageToPlr(Player *plr, const char* message, ...)
{
	if( !message || !plr->GetSession() ) return;
	va_list ap;
	va_start(ap, message);
	char msg1[1024];
	vsnprintf(msg1,1024,message,ap);
	SystemMessage(plr->GetSession(), msg1);
}

bool ChatHandler::CmdSetValueField(WorldSession *m_session, uint32 field, uint32 fieldmax, const char *fieldname, const char *args)
{
	if(!args) return false;
	char* pvalue = strtok((char*)args, " ");
	uint32 mv, av;

	if (!pvalue)
		return false;
	else
		av = atol(pvalue);

	if(fieldmax)
	{
		char* pvaluemax = strtok(NULL, " ");   
		if (!pvaluemax)
			return false;  
		else
			mv = atol(pvaluemax);
	}
	else
	{
		mv = 0;
	}

	if (av <= 0 && mv > 0)
	{  
		RedSystemMessage(m_session, "Values are invalid. Value must be < max (if max exists), and both must be > 0.");
		return true;   
	}
	if(fieldmax)
	{
		if(mv < av || mv <= 0)
		{
			RedSystemMessage(m_session, "Values are invalid. Value must be < max (if max exists), and both must be > 0.");
			return true;  
		}
	}

	Player *plr = getSelectedChar(m_session, false);
	if(plr)
	{  
		MyLog::gmlog->notice("Player[%d] Acct[%d] used modify field value: %s, %u on %s", m_session->GetPlayer()->GetLowGUID(), m_session->GetAccountId(), fieldname, av, plr->GetName());
		if(fieldmax)
		{
			BlueSystemMessage(m_session, "You set the %s of %s to %d/%d.", fieldname, plr->GetName(), av, mv);
			GreenSystemMessageToPlr(plr, "%s set your %s to %d/%d.", m_session->GetPlayer()->GetName(), fieldname, av, mv);
		}
		else
		{
			BlueSystemMessage(m_session, "You set the %s of %s to %d.", fieldname, plr->GetName(), av);
			GreenSystemMessageToPlr(plr, "%s set your %s to %d.", m_session->GetPlayer()->GetName(), fieldname, av);
		}

		if(field == UNIT_FIELD_STAT1) av /= 2;
		if(field == UNIT_FIELD_BASE_HEALTH) 
		{
			plr->SetUInt32Value(UNIT_FIELD_HEALTH, av);
		}

		plr->SetUInt32Value(field, av);

		if(fieldmax) {
			plr->SetUInt32Value(fieldmax, mv);
		}
	}
	else
	{
		Creature *cr = getSelectedCreature(m_session, false);
		if(cr)
		{
			if(!(field < UNIT_END && fieldmax < UNIT_END)) return false;
			std::string creaturename = "Unknown Being";
			if(cr->GetCreatureName())
				creaturename = cr->GetCreatureName()->Name;
			if(fieldmax)
				BlueSystemMessage(m_session, "Setting %s of %s to %d/%d.", fieldname, creaturename.c_str(), av, mv);
			else
				BlueSystemMessage(m_session, "Setting %s of %s to %d.", fieldname, creaturename.c_str(), av);
			MyLog::gmlog->notice("Player[%d] Acct[%d] used modify field value: [creature]%s, %u on %s", m_session->GetPlayer()->GetLowGUID(), m_session->GetAccountId(), fieldname, av, creaturename.c_str());
			if(field == UNIT_FIELD_STAT1) av /= 2;
			if(field == UNIT_FIELD_BASE_HEALTH) 
				cr->SetUInt32Value(UNIT_FIELD_HEALTH, av);

			/*
			switch(field)
			{
			case UNIT_FIELD_FACTIONTEMPLATE:
				{
					if(cr->m_spawn)
						WorldDatabase.WaitExecute("UPDATE creature_spawns SET faction = %u WHERE entry = %u", av, cr->m_spawn->entry);
				}break;
			case UNIT_NPC_FLAGS:
				{
					if(cr->proto)
						WorldDatabase.WaitExecute("UPDATE creature_proto SET npcflags = %u WHERE entry = %u", av, cr->proto->Id);
				}break;
			}
			*/

			cr->SetUInt32Value(field, av);

			if(fieldmax) {
				cr->SetUInt32Value(fieldmax, mv);
			}
			// reset faction
			if(field == UNIT_FIELD_FACTIONTEMPLATE)
				cr->_setFaction();

			//cr->SaveToDB();
		}
		else
		{
			RedSystemMessage(m_session, "Invalid Selection.");
		}
	}
	return true;
}

bool ChatHandler::CmdSetFloatField(WorldSession *m_session, uint32 field, uint32 fieldmax, const char *fieldname, const char *args)
{
	char* pvalue = strtok((char*)args, " ");
	float mv, av;

	if (!pvalue)
		return false;
	else
		av = (float)atof(pvalue);

	if(fieldmax)
	{
		char* pvaluemax = strtok(NULL, " ");   
		if (!pvaluemax)
			return false;  
		else
			mv = (float)atof(pvaluemax);
	}
	else
	{
		mv = 0;
	}

	if (av <= 0)
	{  
		RedSystemMessage(m_session, "Values are invalid. Value must be < max (if max exists), and both must be > 0.");
		return true;   
	}
	if(fieldmax)
	{
		if(mv < av || mv <= 0)
		{
			RedSystemMessage(m_session, "Values are invalid. Value must be < max (if max exists), and both must be > 0.");
			return true;  
		}
	}

	Player *plr = getSelectedChar(m_session, false);
	if(plr)
	{  
		MyLog::gmlog->notice("Player[%d] Acct[%d] used modify field value: %s, %f on %s", m_session->GetPlayer()->GetLowGUID(), m_session->GetAccountId(), fieldname, av, plr->GetName());
		if(fieldmax)
		{
			BlueSystemMessage(m_session, "You set the %s of %s to %.1f/%.1f.", fieldname, plr->GetName(), av, mv);
			GreenSystemMessageToPlr(plr, "%s set your %s to %.1f/%.1f.", m_session->GetPlayer()->GetName(), fieldname, av, mv);
		}
		else
		{
			BlueSystemMessage(m_session, "You set the %s of %s to %.1f.", fieldname, plr->GetName(), av);
			GreenSystemMessageToPlr(plr, "%s set your %s to %.1f.", m_session->GetPlayer()->GetName(), fieldname, av);
		}
		plr->SetFloatValue(field, av);
		if(fieldmax) plr->SetFloatValue(fieldmax, mv);
	}
	else
	{
		Creature *cr = getSelectedCreature(m_session, false);
		if(cr)
		{
			if(!(field < UNIT_END && fieldmax < UNIT_END)) return false;
			std::string creaturename = "Unknown Being";
			if(cr->GetCreatureName())
				creaturename = cr->GetCreatureName()->Name;
			if(fieldmax)
				BlueSystemMessage(m_session, "Setting %s of %s to %.1f/%.1f.", fieldname, creaturename.c_str(), av, mv);
			else
				BlueSystemMessage(m_session, "Setting %s of %s to %.1f.", fieldname, creaturename.c_str(), av);
			cr->SetFloatValue(field, av);
			MyLog::gmlog->notice("Player[%d] Acct[%d] used modify field value: creature[%s], %f on %s", m_session->GetPlayer()->GetLowGUID(), m_session->GetAccountId(), fieldname, av, creaturename.c_str());
			if(fieldmax) {
				cr->SetFloatValue(fieldmax, mv);
			}
			//cr->SaveToDB();
		}
		else
		{
			RedSystemMessage(m_session, "Invalid Selection.");
		}
	}
	return true;
}

bool ChatHandler::HandleGetPosCommand(const char* args, WorldSession *m_session)
{
	/*if(m_session->GetPlayer()->GetSelection() == 0) return false;
	Creature *creature = objmgr.GetCreature(m_session->GetPlayer()->GetSelection());

	if(!creature) return false;
	BlueSystemMessage(m_session, "Creature Position: \nX: %f\nY: %f\nZ: %f\n", creature->GetPositionX(), creature->GetPositionY(), creature->GetPositionZ());
	return true;*/

	/*
	uint32 spell = atol(args);
	SpellEntry *se = dbcSpell.LookupEntry(spell);
	if(se)
		BlueSystemMessage(m_session, "SpellIcon for %d is %d", se->Id, se->field114);
		*/
	return true;
}
