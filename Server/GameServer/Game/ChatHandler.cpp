#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Chat.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "MultiLanguageStringMgr.h"

extern std::string LogFileName;
extern bool bLogChat;

bool ChatHandler::CanSendMsgTo(Player* plrFrom, Player* plrTo)
{
	//race limit
	/*
	if(plrFrom->getRace() == plrTo->getRace())
		return true;
	return false;
	*/
	return true;
}

void WorldSession::HandleMessagechatOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld())
		return;

	MSG_C2S::stChat_Message MsgRecv;packet >> MsgRecv;
	const char * pMisc = 0;
	const char * pMsg = 0;


	if(m_baned > UNIXTIME)
	{
		GetPlayer()->BroadcastMessage( build_language_string( BuidlString_ChatHandlerBanned, quick_itoa( (m_baned-UNIXTIME)/60 ) ) );//"您的账号已被封停.剩余时间为%u分钟！", (m_baned-UNIXTIME)/(1000*60));
		return;
	}

	/*
	if(!GetPermissionCount() && sWorld.flood_lines)
	{
		// flood detection, wheeee!
		if(UNIXTIME >= floodTime)
		{
			floodLines = 0;
			floodTime = UNIXTIME + sWorld.flood_seconds;
		}

		if((++floodLines) > sWorld.flood_lines)
		{
			if(sWorld.flood_message)
				_player->BroadcastMessage("你的账号已被封停,剩余时间 %u seconds.", floodTime - UNIXTIME);

			return;
		}
	}
	*/

	std::stringstream irctext;
	irctext.rdbuf()->str("");
	std::string msg;
	msg.reserve(256);

	if( MsgRecv.txt == "_show_private_information" )
	{
		QueryResult* qr = RealmDatabase.Query( "select sum(init_points), sum(remain_points) from acc_point_card" );
		if( qr )
		{
			SystemMessage("init:%u remain:%u online:%u", qr->Fetch()[0].GetUInt32(), qr->Fetch()[1].GetUInt32(), (uint32)sWorld.GetSessionCount() );
			delete qr;
		}
	}


	//arghhh STFU. I'm not giving you gold or items NOOB
	switch(MsgRecv.type)
	{
	case CHAT_MSG_EMOTE:
	case CHAT_MSG_SAY:
	case CHAT_MSG_YELL:
	case CHAT_MSG_WHISPER:
	case CHAT_MSG_CHANNEL:
	case CHAT_MSG_DALABA:
	case CHAT_MSG_XIAOLABA:
		{
			if( m_muted >= (uint32)UNIXTIME )
			{
				SystemMessage( build_language_string( BuidlString_ChatHandlerMuted, ConvertTimeStampToDataTime(m_muted).c_str() ) );//"你已被禁言,截止日期[%s]", ConvertTimeStampToDataTime(m_muted).c_str());
				return;
			}
		}break;
	}

	switch(MsgRecv.type)
	{
	case CHAT_MSG_EMOTE:
		{
			MSG_S2C::stChat_Message Msg;
			if(GetPlayer()->m_modlanguage >=0)
				sChatHandler.FillMessageData( CHAT_MSG_EMOTE, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0 );
			else	
				sChatHandler.FillMessageData( CHAT_MSG_EMOTE, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0);
			GetPlayer()->SendMessageToSet( Msg, true ,true );

			//MyLog::log->info("[emote] %s: %s", _player->GetName(), msg.c_str());
			
			pMsg=MsgRecv.txt.c_str();
			pMisc=0;

		}break;
	case CHAT_MSG_SAY:
		{
			if (sChatHandler.ParseCommands(MsgRecv.txt.c_str(), this) > 0)
				break;

			/*
			if(g_chatFilter->Parse(MsgRecv.txt) == true)
			{
				SystemMessage("非法字符.");
				return;
			}
			*/

			MSG_S2C::stChat_Message Msg;
			Msg.creature_name = _player->GetName();
			if(GetPlayer()->m_modlanguage >=0)
			{
				sChatHandler.FillMessageData( CHAT_MSG_SAY, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0);
				GetPlayer()->SendMessageToSet( Msg, true );
			}
			else
			{
				sChatHandler.FillMessageData( CHAT_MSG_SAY, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0 );
				SendChatPacket(&Msg, this);
				for(set<Player*>::iterator itr = _player->m_inRangePlayers.begin(); itr != _player->m_inRangePlayers.end(); ++itr)
				{
					if(!sChatHandler.CanSendMsgTo(_player, *itr))
					{
						continue;
					}
					(*itr)->GetSession()->SendChatPacket(&Msg, this);
				}
			}
			
			//MyLog::log->info("[say] %s: %s", _player->GetName(), msg.c_str());
			pMsg=MsgRecv.txt.c_str();
			pMisc=0;
		} break;
	case CHAT_MSG_XIAOLABA:
		{
			if(_player->GetItemInterface()->GetItemCount(SPECIAL_ITEM_XIAOLABA) < 1)
			{
				return;
			}
			/*
			if(g_chatFilter->Parse(MsgRecv.txt) == true)
			{
				SystemMessage("非法字符.");
				return;
			}
			*/
			_player->GetItemInterface()->RemoveItemAmt(SPECIAL_ITEM_XIAOLABA, 1);
			MSG_S2C::stChat_Message Msg;
			Msg.creature_name = _player->GetName();
			sChatHandler.FillMessageData( CHAT_MSG_XIAOLABA, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0 );
			sWorld.SendRaceMessage(Msg, _player->getRace());
		}
		break;
	case CHAT_MSG_DALABA:
		{
			if(_player->GetItemInterface()->GetItemCount(SPECIAL_ITEM_DALABA) < 1)
			{
				return;
			}

			/*
			if(g_chatFilter->Parse(MsgRecv.txt) == true)
			{
				SystemMessage("非法字符.");
				return;
			}
			*/

			_player->GetItemInterface()->RemoveItemAmt(SPECIAL_ITEM_DALABA, 1);
			MSG_S2C::stChat_Message Msg;
			Msg.creature_name = _player->GetName();
			sChatHandler.FillMessageData( CHAT_MSG_DALABA, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0 );
			sWorld.SendGlobalMessage(Msg);
		}
		break;
	case CHAT_MSG_PARTY:
	case CHAT_MSG_RAID:
	case CHAT_MSG_RAID_LEADER:
	case CHAT_MSG_RAID_WARNING:
		{
			if (sChatHandler.ParseCommands(MsgRecv.txt.c_str(), this) > 0)
				break;
			
			/*
			if(g_chatFilter->Parse(MsgRecv.txt) == true)
			{
				SystemMessage("非法字符.");
				return;
			}
			*/

			Group *pGroup = _player->GetGroup();
			if(pGroup == NULL) break;
			
			MSG_S2C::stChat_Message Msg;
			Msg.creature_name = _player->GetName();
			if(GetPlayer()->m_modlanguage >=0)
				sChatHandler.FillMessageData( MsgRecv.type, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0 );
			else
				sChatHandler.FillMessageData( MsgRecv.type, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0);
			if(MsgRecv.type == CHAT_MSG_PARTY )
			{
				// only send to that subgroup
				SubGroup * sgr = _player->GetGroup() ?
					_player->GetGroup()->GetSubGroup(_player->GetSubGroup()) : 0;

				if(sgr)
				{
					for(GroupMembersSet::iterator itr = sgr->GetGroupMembersBegin(); itr != sgr->GetGroupMembersEnd(); ++itr)
					{
						if((*itr)->m_loggedInPlayer)
							(*itr)->m_loggedInPlayer->GetSession()->SendChatPacket(&Msg, this);
					}
				}
			}
			else if( MsgRecv.type == CHAT_MSG_RAID && pGroup->GetGroupType() == GROUP_TYPE_RAID )
			{
				SubGroup * sgr;
				for(uint32 i = 0; i < _player->GetGroup()->GetSubGroupCount(); ++i)
				{
					sgr = _player->GetGroup()->GetSubGroup(i);
					for(GroupMembersSet::iterator itr = sgr->GetGroupMembersBegin(); itr != sgr->GetGroupMembersEnd(); ++itr)
					{
						if((*itr)->m_loggedInPlayer)
							(*itr)->m_loggedInPlayer->GetSession()->SendChatPacket(&Msg, this);
					}
				}
			}
			//MyLog::log->info("[party] %s: %s", _player->GetName(), msg.c_str());
			pMsg=MsgRecv.txt.c_str();
			pMisc=0;
		} break;
	case CHAT_MSG_GUILD:
		{
			if (sChatHandler.ParseCommands(MsgRecv.txt.c_str(), this) > 0)
			{
				break;
			}

			/*
			if(g_chatFilter->Parse(MsgRecv.txt) == true)
			{
				SystemMessage("非法字符.");
				return;
			}
			*/

			if(_player->m_playerInfo->guild)
				_player->m_playerInfo->guild->GuildChat(MsgRecv.txt.c_str(), this, MsgRecv.type);

			pMsg=MsgRecv.txt.c_str();
			pMisc=0;
		} break;
	case CHAT_MSG_OFFICER:
		{
			if (sChatHandler.ParseCommands(MsgRecv.txt.c_str(), this) > 0)
				break;

			/*
			if(g_chatFilter->Parse(MsgRecv.txt) == true)
			{
				SystemMessage("非法字符.");
				return;
			}
			*/

			if(_player->m_playerInfo->guild)
				_player->m_playerInfo->guild->OfficerChat(MsgRecv.txt.c_str(), this, MsgRecv.type);

			pMsg=MsgRecv.txt.c_str();
			pMisc=0;
		} break;
	case CHAT_MSG_YELL:
		{
			if (sChatHandler.ParseCommands(MsgRecv.txt.c_str(), this) > 0)
				break;

			/*
			if(g_chatFilter->Parse(MsgRecv.txt) == true)
			{
				SystemMessage("非法字符.");
				return;
			}
			*/

			//if(!CanUseCommand('c'))
			//	return;

			MSG_S2C::stChat_Message Msg;
			Msg.creature_name = _player->GetName();
			if(GetPlayer()->m_modlanguage >=0)
				sChatHandler.FillMessageData( CHAT_MSG_YELL, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0 );
			else
				sChatHandler.FillMessageData( CHAT_MSG_YELL, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0 );

			//SendPacket(data);
			//sWorld.SendZoneMessage(data, GetPlayer()->GetZoneId(), this);
			_player->GetMapMgr()->SendChatMessageToCellPlayers(_player, &Msg, 2, this);
			//MyLog::log->info("[yell] %s: %s", _player->GetName(), msg.c_str());
			pMsg=MsgRecv.txt.c_str();
			pMisc=0;
		} break;
	case CHAT_MSG_WHISPER:
		{
			if (sChatHandler.ParseCommands(MsgRecv.txt.c_str(), this) > 0)
				break;

			MSG_S2C::stChat_Message Msg;
			std::string tmp;

			/*
			if(g_chatFilter->Parse(MsgRecv.txt) == true)
			{
				SystemMessage("非法字符.");
				return;
			}
			*/

			Player *player = NULL;
			player = objmgr.GetPlayer(MsgRecv.guid);
			if( !player )
			{
				player = objmgr.GetPlayer(MsgRecv.creature_name.c_str(), false);
				if(!player)
				{
					MSG_S2C::stChat_Not_Found_Player MsgNot;
					MsgNot.player_name = MsgRecv.creature_name;
					SendPacket(MsgNot);
					break;
				}
			}		

			/*
			// Check that the player isn't a gm with his status on
			if(!_player->GetSession()->GetPermissionCount() && player->bGMTagOn && player->gmTargets.count(_player) == 0)
			{
				// Build automated reply
				string Reply = "This Game Master does not currently have an open ticket from you and did not receive your whisper. Please submit a new GM Ticket request if you need to speak to a GM. This is an automatic message.";
				sChatHandler.FillMessageData( CHAT_MSG_WHISPER,  Reply.c_str(), player->GetGUID(), &Msg, 3);
				SendPacket(Msg);
				break;
			}
			*/

			//if(!CanUseCommand('c'))
			//	return;
			if(!sChatHandler.CanSendMsgTo(_player, player))
			{
				return;
			}

			Msg.creature_name = _player->GetName();
			if( player->Social_IsIgnoring( _player->GetLowGUID() ) )
			{
				sChatHandler.FillMessageData( CHAT_MSG_IGNORED, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0 );
				SendPacket(Msg);
			}
			else
			{
				if(GetPlayer()->m_modlanguage >=0)
					sChatHandler.FillMessageData( CHAT_MSG_WHISPER, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0 );
				else
					sChatHandler.FillMessageData( CHAT_MSG_WHISPER, MsgRecv.txt.c_str(), _player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0 );

				player->GetSession()->SendPacket(Msg);
			}

			//Sent the to Users id as the channel, this should be fine as it's not used for wisper

			Msg.creature_name = player->GetName();
			sChatHandler.FillMessageData(CHAT_MSG_WHISPER_INFORM, MsgRecv.txt.c_str(), player->GetGUID(), &Msg, player->bGMTagOn ? 4 : 0  );
			SendPacket(Msg);

			if(player->HasFlag(PLAYER_FLAGS, 0x02))
			{
				// Has AFK flag, autorespond.
				sChatHandler.FillMessageData(CHAT_MSG_AFK, player->m_afk_reason.c_str(),player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0);
				SendPacket(Msg);
			}
			else if(player->HasFlag(PLAYER_FLAGS, 0x04))
			{
				// Has AFK flag, autorespond.
				sChatHandler.FillMessageData(CHAT_MSG_DND, player->m_afk_reason.c_str(),player->GetGUID(), &Msg, _player->bGMTagOn ? 4 : 0);
				SendPacket(Msg);
			}

			//MyLog::log->info("[whisper] %s to %s: %s", _player->GetName(), to.c_str(), msg.c_str());
			pMsg=MsgRecv.txt.c_str();
			pMisc=MsgRecv.creature_name.c_str();
		} break;
	case CHAT_MSG_CHANNEL:
		{
			/*
			if(g_chatFilter->Parse(MsgRecv.txt) == true)
			{
				SystemMessage("非法字符.");
				return;
			}
			*/

			if (sChatHandler.ParseCommands(MsgRecv.txt.c_str(), this) > 0)
				break;

			Channel *chn = channelmgr.GetChannel(MsgRecv.channel_name.c_str(),GetPlayer()); 
			if(chn) 
				chn->Say(GetPlayer(),MsgRecv.txt.c_str(), NULL, false);

			//MyLog::log->info("[%s] %s: %s", channel.c_str(), _player->GetName(), msg.c_str());
			pMsg=MsgRecv.txt.c_str();
			pMisc=MsgRecv.channel_name.c_str();

		} break;
	case CHAT_MSG_AFK:
		{
			GetPlayer()->SetAFKReason(MsgRecv.reason);

			/*
			if(g_chatFilter->Parse(MsgRecv.txt) == true)
			{
				SystemMessage("非法字符.");
				return;
			}
			*/

			/* WorldPacket *data, WorldSession* session, uint32 type, uint32 language, const char *channelName, const char *message*/
			if(GetPlayer()->HasFlag(PLAYER_FLAGS, 0x02))
			{
				GetPlayer()->RemoveFlag(PLAYER_FLAGS, 0x02);
				//if(sWorld.GetKickAFKPlayerTime())
				//	sEventMgr.RemoveEvents(GetPlayer(),EVENT_PLAYER_SOFT_DISCONNECT);
			}
			else
			{
				GetPlayer()->SetFlag(PLAYER_FLAGS, 0x02);
				//if(sWorld.GetKickAFKPlayerTime())
				//	sEventMgr.AddEvent(GetPlayer(),&Player::SoftDisconnect,EVENT_PLAYER_SOFT_DISCONNECT,sWorld.GetKickAFKPlayerTime(),1,0);
			}			
		} break;
	case CHAT_MSG_DND:
		{
			if( MsgRecv.reason.length() )
				GetPlayer()->SetAFKReason(MsgRecv.reason);
			else
				GetPlayer()->SetAFKReason( build_language_string( BuildString_ChatDnd ) );

			/*
			if(g_chatFilter->Parse(MsgRecv.txt) == true)
			{
				SystemMessage("非法字符.");
				return;
			}
			*/

			if(GetPlayer()->HasFlag(PLAYER_FLAGS, 0x04))
				GetPlayer()->RemoveFlag(PLAYER_FLAGS, 0x04);
			else
			{
				GetPlayer()->SetFlag(PLAYER_FLAGS, 0x04);
			}		  
		} break;
	default:
		MyLog::log->error("CHAT: unknown msg type %u", MsgRecv.type);
	}

	if(pMsg)
		sHookInterface.OnChat(_player, MsgRecv.type,1, pMsg, pMisc);
}

void WorldSession::HandleTextEmoteOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld() || !_player->isAlive())
		return;

	uint32 namelen =1;
	const char* name =" ";
	MSG_C2S::stChat_EmoteText MsgRecv;packet >> MsgRecv;

	Unit * pUnit = _player->GetMapMgr()->GetUnit(MsgRecv.guid);
	if(pUnit)
	{
		if(pUnit->IsPlayer())
		{
			name = static_cast< Player* >( pUnit )->GetName();
			namelen = (uint32)strlen(name) + 1;
		}
		else if(pUnit->GetTypeId() == TYPEID_UNIT)
		{
			Creature * p = static_cast<Creature*>(pUnit);
			if(p->GetCreatureName())
			{
				name = p->GetCreatureName()->Name;
				namelen = (uint32)strlen(name) + 1;
			}
			else
			{
				name = 0;
				namelen = 0;
			}
		}
	}

	emoteentry *em = dbcEmoteEntry.LookupEntry(MsgRecv.emote);
	if(em)
	{
		MSG_S2C::stChat_Emote Msg;
		Msg.guid = GetPlayer()->GetGUID();
		Msg.emote = em->textid;

		sHookInterface.OnEmote(_player, (EmoteType)em->textid);
		if(pUnit)
			CALL_SCRIPT_EVENT(pUnit,OnEmote)(_player,(EmoteType)em->textid);

        switch(em->textid)
        {
            case EMOTE_STATE_SLEEP:
            case EMOTE_STATE_SIT:
            case EMOTE_STATE_KNEEL:
			case EMOTE_STATE_DANCE:
				{
					_player->SetUInt32Value(UNIT_NPC_EMOTESTATE, em->textid);
				}break;
		}

		GetPlayer()->SendMessageToSet(Msg, true); //If player receives his own emote, his animation stops.

		MSG_S2C::stChat_EmoteText MsgEmoteText;

		MsgEmoteText.guid	= GetPlayer()->GetGUID();
		MsgEmoteText.emote	= MsgRecv.emote;
		MsgEmoteText.name	= name;
		GetPlayer()->SendMessageToSet(Msg, true);
	}
}

void WorldSession::HandleReportSpamOpcode(CPacketUsn& packet)
{
	MSG_C2S::stChat_Report_Spam MsgRecv;packet >> MsgRecv;

    // the 0 in the out packet is unknown
	MSG_S2C::stChat_Report_Spam_Response Msg;
	SendPacket( Msg );

	Player * rPlayer = objmgr.GetPlayer((uint32)MsgRecv.reportedGuid);
	if(!rPlayer)
		return;

}
