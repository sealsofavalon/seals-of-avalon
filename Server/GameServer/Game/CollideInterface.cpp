/*
 * Ascent MMORPG Server
 * Copyright (C) 2005-2010 Ascent Team <http://www.ascentemulator.net/>
 *
 * This software is  under the terms of the EULA License
 * All title, including but not limited to copyrights, in and to the AscentNG Software
 * and any copies there of are owned by ZEDCLANS INC. or its suppliers. All title
 * and intellectual property rights in and to the content which may be accessed through
 * use of the AscentNG is the property of the respective content owner and may be protected
 * by applicable copyright or other intellectual property laws and treaties. This EULA grants
 * you no rights to use such content. All rights not expressly granted are reserved by ZEDCLANS INC.
 *
 */

#include "StdAfx.h"
#include "CollideInterface.h"
#include "../../Common/share/Collision/vmap/VMapFactory.h"
#include "../../Common/share/Collision/g3dlite/G3D/Vector3.h"

//#pragma comment(lib, "collision.lib")
#define MAX_MAP 700

//extern VMAP::IVMapManager* CollisionMgr;

struct CollisionMap
{
	uint32 m_loadCount;
	uint32 m_tileLoadCount[64][64];
	//RWLock m_lock;
};

inline G3D::Vector3 convertPositionToMangosRep(float x, float y, float z)
{
	float pos[3];
	pos[0] = z;
	pos[1] = x;
	pos[2] = y;
	double full = 64.0*533.33333333;
	double mid = full/2.0;
	pos[0] = -((mid+pos[0])-full);
	pos[1] = -((mid+pos[1])-full);

	return(G3D::Vector3(pos));
}

CCollideInterface CollideInterface;
VMAP::IVMapManager * CollisionMgr;
//std::map<uint32, CollisionMap*> m_mapLocks;
//Mutex m_mapCreateLock;

//extern void* collision_init();
//extern void collision_shutdown();

typedef uint64 MapIdentify;
std::set<MapIdentify> s_loaded;


void CCollideInterface::Init()
{
	//Log.Notice("CollideInterface", "Init");
	CollisionMgr = VMAP::VMapFactory::createOrGetVMapManager();//((VMAP::IVMapManager*)new VMAP::VMapManager);
}

/*
void CCollideInterface::ActivateMap(uint32 mapId)
{
	if( !CollisionMgr ) return;
//	m_mapCreateLock.Acquire();
	if( m_mapLocks.find( mapId ) == m_mapLocks.end() )
	{
		m_mapLocks[mapId] = new CollisionMap;
		m_mapLocks[mapId]->m_loadCount = 1;
		memset(m_mapLocks[mapId]->m_tileLoadCount, 0, sizeof(uint32)*64*64);
	}
	else
		m_mapLocks[mapId]->m_loadCount++;

//	m_mapCreateLock.Release();
}

void CCollideInterface::DeactivateMap(uint32 mapId)
{
	if( !CollisionMgr ) return;
//	m_mapCreateLock.Acquire();
	if( m_mapLocks.find( mapId ) == m_mapLocks.end() )
		return;

	--m_mapLocks[mapId]->m_loadCount;
	if( m_mapLocks[mapId]->m_loadCount == 0 )
	{
		// no instances using this anymore
		delete m_mapLocks[mapId];
		CollisionMgr->unloadMap(mapId);
		m_mapLocks[mapId] = NULL;
	}
//	m_mapCreateLock.Release();
}

void CCollideInterface::ActivateTile(uint32 mapId, uint32 tileX, uint32 tileY)
{
	if( m_mapLocks.find( mapId ) == m_mapLocks.end() )
		return;
	if( !CollisionMgr ) return;

	// acquire write lock
	//m_mapLocks[mapId]->m_lock.AcquireWriteLock();
	if( m_mapLocks[mapId]->m_tileLoadCount[tileX][tileY] == 0 )
		CollisionMgr->loadMap( "Data/vmap", mapId, tileX, tileY);

	// increment count
	m_mapLocks[mapId]->m_tileLoadCount[tileX][tileY]++;

	// release lock
	//m_mapLocks[mapId]->m_lock.ReleaseWriteLock();
}

void CCollideInterface::DeactivateTile(uint32 mapId, uint32 tileX, uint32 tileY)
{
	if( m_mapLocks.find( mapId ) == m_mapLocks.end() )
		return;

	if( !CollisionMgr ) return;

	// get write lock
	//m_mapLocks[mapId]->m_lock.AcquireWriteLock();
	if( (--m_mapLocks[mapId]->m_tileLoadCount[tileX][tileY]) == 0 )
		CollisionMgr->unloadMap(mapId, tileX, tileY);

	// release write lock
	//m_mapLocks[mapId]->m_lock.ReleaseWriteLock();
}
*/

void CCollideInterface::LoadVMap( uint32 mapId, uint32 tileX, uint32 tileY )
{
	MapIdentify mi = ( (uint64)mapId << 32 ) | ( tileX << 16 ) | tileY;
	if( s_loaded.find( mi ) != s_loaded.end() )
		return;

	CollisionMgr->loadMap( "Data/vmap", mapId, tileX, tileY);
	MyLog::log->notice( "load vmap, mapId:%u, tileX:%u, tileY:%u OK!", mapId, tileX, tileY );

	s_loaded.insert( mi );
}

bool CCollideInterface::CheckLOS(uint32 mapId, float x1, float y1, float z1, float x2, float y2, float z2)
{
	if( !CollisionMgr )
		return false;

	//if( m_mapLocks.find( mapId ) == m_mapLocks.end() )
	//	return false;

	// get read lock
	//m_mapLocks[mapId]->m_lock.AcquireReadLock();

	/*
	Map* p = sInstanceMgr.GetMap( mapId );
	if( p )
	{
		float dx = x2 - x1;
		float dy = y2 - y1;
		float dz = z2 - z1;
		for( int i = 0; i < 4; ++i )
		{
			float x = x1 + ( i + 1 ) * dx / 5.f;
			float y = y1 + ( i + 1 ) * dy / 5.f;
			float z = p->GetLandHeight( x, y );
			if( z > z1 + 2.5f && z > z2 + 2.8f )
				return false;
		}
	}
	*/

	G3D::Vector3 v1 = convertPositionToMangosRep( y1, z1, x1 );
	G3D::Vector3 v2 = convertPositionToMangosRep( y2, z2, x2 );


	// get data
	bool res = CollisionMgr->isInLineOfSight(mapId, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z);

	// release write lock
	//m_mapLocks[mapId]->m_lock.ReleaseReadLock();

	// return
	return res;
}

bool CCollideInterface::CheckLOS(uint32 mapId, const LocationVector& a, const LocationVector& b )
{
	return CheckLOS( mapId, a.x, a.y, a.z + 2.f, b.x, b.y, b.z + 2.f );
}

bool CCollideInterface::GetFirstPoint(uint32 mapId, float x1, float y1, float z1, float x2, float y2, float z2, float & outx, float & outy, float & outz, float distmod)
{
	//if( m_mapLocks.find( mapId ) == m_mapLocks.end() )
	//	return false;

	// get read lock
	//m_mapLocks[mapId]->m_lock.AcquireReadLock();

	// get data
	G3D::Vector3 v1 = convertPositionToMangosRep( y1, z1, x1 );
	G3D::Vector3 v2 = convertPositionToMangosRep( y2, z2, x2 );

	bool res = (CollisionMgr ? CollisionMgr->getObjectHitPos(mapId, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, outx, outy, outz, distmod) : false);

	// release write lock
	//m_mapLocks[mapId]->m_lock.ReleaseReadLock();

	// return
	return res;
}

float CCollideInterface::GetHeight( Map* pMap, float x, float y, float z, bool* onLand )
{
	if( !CollisionMgr )
		return NO_WMO_HEIGHT;

	G3D::Vector3 v = convertPositionToMangosRep( y, z, x );
	// get data
	float res = CollisionMgr->getHeight(pMap->GetMapIDForCollision(), v.x, v.y, v.z );

	float landHight = pMap->GetLandHeight( x, y );
	if( landHight > res )
	{
		if( onLand )
			*onLand = true;
		return landHight;
	}
	else
	{
		if( onLand )
			*onLand = false;
		return res;
	}
}
/*
bool CCollideInterface::IsIndoor(uint32 mapId, float x, float y, float z)
{
	ASSERT(m_mapLocks[mapId] != NULL);

	// get read lock
	//m_mapLocks[mapId]->m_lock.AcquireReadLock();

	// get data
	bool res = CollisionMgr ? CollisionMgr->isInDoors(mapId, x, y, z) : true;

	// release write lock
	//m_mapLocks[mapId]->m_lock.ReleaseReadLock();

	// return
	return res;
}

bool CCollideInterface::IsOutdoor(uint32 mapId, float x, float y, float z)
{
	ASSERT(m_mapLocks[mapId] != NULL);

	// get read lock
	//m_mapLocks[mapId]->m_lock.AcquireReadLock();

	// get data
	bool res = CollisionMgr ? CollisionMgr->isOutDoors(mapId, x, y, z) : true; 

	// release write lock
	//m_mapLocks[mapId]->m_lock.ReleaseReadLock();

	// return
	return res;
}
*/
void CCollideInterface::DeInit()
{
	// bleh.
	VMAP::VMapFactory::clear();
}
