/*
 * Ascent MMORPG Server
 * Copyright (C) 2005-2010 Ascent Team <http://www.ascentemulator.net/>
 *
 * This software is  under the terms of the EULA License
 * All title, including but not limited to copyrights, in and to the AscentNG Software
 * and any copies there of are owned by ZEDCLANS INC. or its suppliers. All title
 * and intellectual property rights in and to the content which may be accessed through
 * use of the AscentNG is the property of the respective content owner and may be protected
 * by applicable copyright or other intellectual property laws and treaties. This EULA grants
 * you no rights to use such content. All rights not expressly granted are reserved by ZEDCLANS INC.
 *
 */

#ifndef _COLLIDEINTERFACE_H
#define _COLLIDEINTERFACE_H

class Map;

/* imports */
#define NO_WMO_HEIGHT -200000.0f
static const LocationVector INVALID_LOCATION( NO_WMO_HEIGHT, NO_WMO_HEIGHT, NO_WMO_HEIGHT );

class CCollideInterface
{
public:
	void Init();
	void DeInit();

	/*
	void ActivateTile(uint32 mapId, uint32 tileX, uint32 tileY);
	void DeactivateTile(uint32 mapId, uint32 tileX, uint32 tileY);
	void ActivateMap(uint32 mapId);
	void DeactivateMap(uint32 mapId);
	*/

	void LoadVMap( uint32 mapId, uint32 tileX, uint32 tileY );

	bool CheckLOS(uint32 mapId, float x1, float y1, float z1, float x2, float y2, float z2);
	bool CheckLOS(uint32 mapId, const LocationVector& a, const LocationVector& b );
	bool GetFirstPoint(uint32 mapId, float x1, float y1, float z1, float x2, float y2, float z2, float & outx, float & outy, float & outz, float distmod);
	//bool IsIndoor(uint32 mapId, float x, float y, float z);
	//bool IsOutdoor(uint32 mapId, float x, float y, float z);
	float GetHeight( Map* pMap, float x, float y, float z, bool* onLand = NULL );
};

extern CCollideInterface CollideInterface;

#endif
