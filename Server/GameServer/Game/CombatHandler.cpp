#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Spell.h"
#include "../../SDBase/Protocol/S2C_Spell.h"

void WorldSession::HandleAttackSwingOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	
	MSG_C2S::stAttack_Swing MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;

	if(!guid)
	{
		// does this mean cancel combat?
		HandleAttackStopOpcode(packet);
		return;
	}

	// AttackSwing
	//MyLog::log->debug( "player:%s Recvd CMSG_ATTACKSWING Message", _player->GetName() );

	if(GetPlayer()->IsPacified() || GetPlayer()->IsStunned() || GetPlayer()->IsFeared() || GetPlayer()->IsAsleep())
		return;

	if(GetPlayer()->HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_SHAPE_CANTFIGHT))
		return;
	if(GetPlayer()->HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_MOUNT_CANTFIGHT))
		return;

	_player->m_lastShotTime = (uint32)UNIXTIME;

//	printf("Got ATTACK SWING: %08X %08X\n", GUID_HIPART(guid), GUID_LOPART(guid));
	Unit *pEnemy = _player->GetMapMgr()->GetUnit(guid);
	//printf("Pointer: %08X\n", pEnemy);

	if(!pEnemy)
	{
		MyLog::log->alert(""I64FMT" does not exist.", guid);
		return;
	}

	if(pEnemy->isDead() || _player->isDead())		// haxors :(
		return;

	if(isFriendly(_player, pEnemy) /*|| pEnemy->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_2 | UNIT_FLAG_NOT_SELECTABLE)*/)
	{
		//MyLog::log->debug( "Enemy %s %u is friendly", "creature", GUID_LOPART(guid));

		// stop attack state at client
		GetPlayer()->smsg_AttackStop(pEnemy);
		return;
	}

	bool badfacing = false;
	if(!_player->canReachWithAttack(pEnemy, badfacing))
	{
		//MyLog::log->debug( "Enemy %s %u far from attaker", "creature", GUID_LOPART(guid));
		// stop attack state at client
		GetPlayer()->smsg_AttackStop(pEnemy);

		if( !badfacing )
		{
			MSG_S2C::stAttack_Swing_Not_In_Range Msg;
			_player->GetSession()->SendPacket( Msg );
		}
		else
		{
			MSG_S2C::stAttack_Swing_Bad_Facing Msg;
			_player->GetSession()->SendPacket( Msg );
		}

		return;
	}

	GetPlayer()->smsg_AttackStart(pEnemy);
	GetPlayer()->EventAttackStart();

	if(GetPlayer()->GetSummon())
	{
		Pet* pPet = GetPlayer()->GetSummon();
		pPet->GetAIInterface()->SetAIState(STATE_ATTACKING);
		pPet->GetAIInterface()->AttackReaction(pEnemy, 1, 0);
	}

	if(pEnemy->IsPlayer() && ((Player*)pEnemy)->GetSummon())
	{
		Pet* pPet = ((Player*)pEnemy)->GetSummon();
		pPet->GetAIInterface()->SetAIState(STATE_ATTACKING);
		pPet->GetAIInterface()->AttackReaction(GetPlayer(), 1, 0);
	}
	// Set PVP Flag.
	/*if(pEnemy->IsPlayer() && isHostile(_player, pEnemy))
	{
		// don't in duel.. this should be done in dealdamage anyway :S
		if( static_cast< Player* >( pEnemy )->GetTeam() != _player->GetTeam() )
			_player->SetPvPFlag();
	}*/
}

void WorldSession::HandleAttackStopOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	uint64 guid = GetPlayer()->GetSelection();
	Unit *pEnemy = NULL;

	if(guid)
	{
		pEnemy = _player->GetMapMgr()->GetUnit(guid);
		if(pEnemy)
		{
			GetPlayer()->EventAttackStop();
			GetPlayer()->smsg_AttackStop(pEnemy);

			/*WorldPacket data(SMSG_ATTACKSTOP, 20);
			data << _player->GetNewGUID();
			data << uint8(0);
			data << uint32(0);
			SendPacket(&data);*/
		}
	}
}

