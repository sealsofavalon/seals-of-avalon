#include "StdAfx.h"
#include "CombineItem.h"
#include "Player.h"
#include "Item.h"
#include "ItemInterface.h"
#include "../../SDBase/Public/UpdateFields.h"

CombineItemInterface::CombineItemInterface()
{
	max_slot = MAX_COMBINE_SLOT;
	player_field_cost = PLAYER_FIELD_COMBINE_COST;
	player_field_begin = PLAYER_FIELD_COMBINE_SLOT_MAIN_ITEM,
	inventory_slot_begin = INVENTORY_SLOT_COMBINE_BEGIN;
	inventory_slot_save = INVENTORY_SLOT_COMBINE;
}

CombineItemInterface::~CombineItemInterface()
{
}

void CombineItemInterface::RefreshCost()
{
	if( m_slots[0] )
	{
		uint32 lv = m_slots[0]->GetUInt32Value( ITEM_FIELD_COMBINE_LEVEL ) + 1;
		ItemPrototype* proto = m_slots[0]->GetProto();
		int offitemslot = 0;
		if( proto->Quality <= 4 )
		{
			for( int i = 1; i < max_slot; ++i )
			{
				if( m_slots[i]->GetProto()->Quality == proto->Quality )
				{
					offitemslot = i;
					break;
				}
			}
		}
		if( lv <= 9 )
		{
			combine_need* cn = dbcCombineNeed.LookupEntry( offitemslot ? 10 + proto->Quality : 100 * lv + proto->Quality );
			if( cn )
			{
				m_owner->SetUInt32Value( player_field_cost, cn->cost );
				return;
			}
		}
	}
	m_owner->SetUInt32Value( player_field_cost, 0 );
}


combine_result_t CombineItemInterface::Combine( bool check /* = false */ )
{
	if( !m_slots[0] ) return COMBINE_RESULT_INVALID_ITEM;

	if( !m_slots[0]->CanRefine() ) return COMBINE_RESULT_INVALID_ITEM;

	uint32 lv = m_slots[0]->GetUInt32Value( ITEM_FIELD_COMBINE_LEVEL ) + 1;

	ItemPrototype* proto = m_slots[0]->GetProto();
	int offitemslot = 0;

	if( proto->Quality <= 4 )
	{
		for( int i = 1; i < max_slot; ++i )
		{
			if( m_slots[i]->GetProto()->Quality == proto->Quality )
			{
				offitemslot = i;
				break;
			}
		}
	}
	else if( lv > 9 ) return COMBINE_RESULT_ALREADY_MAX_LEVEL;

	combine_need* cn = dbcCombineNeed.LookupEntry( offitemslot ? 10 + proto->Quality : 100 * lv + proto->Quality );
	if( !cn ) return COMBINE_RESULT_ALREADY_MAX_LEVEL;
	if( m_owner->GetUInt32Value( PLAYER_FIELD_COINAGE ) < cn->cost ) return COMBINE_RESULT_NOT_ENOUGH_GOLD;

	std::map<int, int> mConsume;
	for( int i = 0; i < 4; ++i )
	{
		if( cn->need_array[i].material )
		{
			uint32 s = FindItem( cn->need_array[i].material, cn->need_array[i].count );
			if( s == 0 )
				return COMBINE_RESULT_NO_MATCH_MATERIAL;
			mConsume[s] = cn->need_array[i].count;
		}
	}
	if( check )
		return COMBINE_RESULT_CHECK_SUCCESS;

	if( offitemslot )
	{
		m_slots[offitemslot]->DeleteFromDB();
		m_slots[offitemslot]->Delete();
		m_slots[offitemslot] = NULL;;
	}
	for( std::map<int, int>::iterator it = mConsume.begin(); it != mConsume.end(); ++it )
	{
		uint32 i = it->first;
		uint32 count = m_slots[i]->GetUInt32Value( ITEM_FIELD_STACK_COUNT );
		MyLog::yunyinglog->info("item-combine-player[%u][%s] removeitem["I64FMT"][%u][%s] count[%u]", m_owner->GetLowGUID(), m_owner->GetName(), m_slots[i]->GetGUID(), m_slots[i]->GetProto()->ItemId, m_slots[i]->GetProto()->Name1, count);
		if( count == it->second )
		{
			m_slots[i]->DeleteFromDB();
			m_slots[i]->Delete();
			m_slots[i] = NULL;
		}
		else
		{
			m_slots[i]->SetCount( count - it->second );
			m_slots[i]->m_isDirty = true;
		}
		RefreshSlot( i );
	}

	m_owner->ModCoin(-cn->cost );
	MyLog::yunyinglog->info("gold-combine-player[%u][%s] take gold[%u]", m_owner->GetLowGUID(), m_owner->GetName(), cn->cost);

	float chance = 100 - lv * 10;
	float val = 100.f * (float)( rand() % 32767 ) / 32767.f;
	if( val <= chance )
	{
		if( offitemslot )
		{
			combine_special* cs = dbcCombineSpecialList.LookupEntry( m_slots[0]->GetEntry() );
			if( cs )
			{
				Item* newItem = new Item;
				newItem->Create( cs->new_entry, m_owner );
				m_slots[0]->DeleteFromDB();
				m_slots[0]->Delete();
				m_slots[0] = newItem;
				RefreshSlot( 0 );
			}
			else
			{
				MyLog::log->info( "cannot find item entry[%d] in combine special list", m_slots[0]->GetEntry() );
				assert( 0 );
				return COMBINE_RESULT_FAILED;
			}
		}
		else
		{
			m_slots[0]->SetUInt32Value( ITEM_FIELD_COMBINE_LEVEL, lv );
			m_slots[0]->RefreshCombine();
		}
		return COMBINE_RESULT_SUCCESS;
	}
	
	return COMBINE_RESULT_FAILED;
}
