#ifndef _COMBINE_ITEM_HEAD_
#define _COMBINE_ITEM_HEAD_

#include "MakeItemBase.h"

class CombineItemInterface : public MakeItemBase
{
public:
	CombineItemInterface();
	~CombineItemInterface();

	virtual void RefreshCost();
	combine_result_t Combine( bool check = false );

};
#endif