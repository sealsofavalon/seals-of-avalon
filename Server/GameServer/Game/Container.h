#ifndef _CONTAINER_H
#define _CONTAINER_H
class SERVER_DECL Container : public Item
{
public:
	friend class WorldSession;
	Container(uint32 high, uint32 low);
	~Container();

	void Create( uint32 itemid, Player *owner );
	void LoadFromDB( Field*fields);

	bool AddItem(ui8 slot, Item *item);
	bool AddItemToFreeSlot(Item *pItem, uint32 * r_slot);
	Item *GetItem(ui8 slot)
	{
		if((uint8)slot < GetProto()->ContainerSlots)
			return m_Slot[slot];
		else
			return 0;
	}
	
	ui8 FindFreeSlot();
	bool HasItems();
	
	void SwapItems(ui8 SrcSlot,ui8 DstSlot);
	Item *SafeRemoveAndRetreiveItemFromSlot(ui8 slot, bool destroy); //doesnt destroy item from memory
	bool SafeFullRemoveItemFromSlot(ui8 slot); //destroys item fully
   
	void SaveBagToDB(ui8 slot, bool first, QueryBuffer * buf);

protected:
	Item **m_Slot;
	uint32 __fields[CONTAINER_END];
};

#endif
