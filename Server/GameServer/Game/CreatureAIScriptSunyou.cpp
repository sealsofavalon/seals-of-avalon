#include "StdAfx.h"
#include "CreatureAIScriptSunyou.h"


CreatureAIScriptSunyou::CreatureAIScriptSunyou()
{

	//CreatureAIScript::CreatureAIScript();
}

CreatureAIScriptSunyou::CreatureAIScriptSunyou(Creature* creature) : CreatureAIScript(creature)
{






}

CreatureAIScriptSunyou::~CreatureAIScriptSunyou(void)
{

}

void CreatureAIScriptSunyou::Init()
{
	if (!_unit)
	{
		return;
	}
	if (_unit->proto->lua_script[0] != 0 && mLuaState)
	{

		int entry  = atoi(_unit->proto->lua_script);
		char sz[256];
		sprintf(sz, "%u", entry);
		//m_str = sz;
		//mLuaState->DoFile(_unit->proto->lua_script);
	}



}


void CreatureAIScriptSunyou::OnCombatStart(Unit* mTarget)
{


	CallLua("OnCombatStart", mTarget);
}

void CreatureAIScriptSunyou::OnReturnFromCombat()
{
	CallLua("OnReturnFromCombat");
}

void CreatureAIScriptSunyou::OnCombatStop(Unit* mTarget)
{


	CallLua("OnCombatStop", mTarget);
}

void CreatureAIScriptSunyou::OnDamageTaken(Unit* mAttacker, float fAmount)
{


	CallLua("OnDamageTaken", mAttacker, fAmount);
}

void CreatureAIScriptSunyou::OnCastSpell(uint32 iSpellId)
{
	std::string str;
	str = m_str + "OnCastSpell" ;
	if (!mLuaState)
	{
	}

	LuaObject fnObj = mLuaState->GetGlobal( str.c_str());

	if( !fnObj.IsNil() )
	{
		LuaFunction<int> luaFn( fnObj );
		luaFn( _unit->GetUniqueIDForLua(), iSpellId);
	}

}


//����
void CreatureAIScriptSunyou::OnTargetParried(Unit* mTarget)
{




	CallLua("OnTargetParried", mTarget);

}

void CreatureAIScriptSunyou::OnTargetDodged(Unit* mTarget)
{

	CallLua("OnTargetDodged", mTarget);

}

void CreatureAIScriptSunyou::OnTargetBlocked(Unit* mTarget, int32 iAmount)
{


	CallLua("OnTargetBlocked", mTarget, iAmount);
}

void CreatureAIScriptSunyou::OnTargetCritHit(Unit* mTarget, float fAmount)
{


	CallLua("OnTargetCritHit", mTarget, fAmount);
}

void CreatureAIScriptSunyou::OnTargetDied(Unit* mTarget)
{


	CallLua("OnTargetDied", mTarget);
}

void CreatureAIScriptSunyou::OnParried(Unit* mTarget)
{


	CallLua("OnParried", mTarget);
}

void CreatureAIScriptSunyou::OnDodged(Unit* mTarget)
{
	CallLua("OnDodged", mTarget);
}

void CreatureAIScriptSunyou::OnBlocked(Unit* mTarget, int32 iAmount)
{

	CallLua( "OnBlocked", mTarget, iAmount);
}

void CreatureAIScriptSunyou::OnCritHit(Unit* mTarget, float fAmount)
{

	CallLua( "OnCritHit", mTarget, fAmount);;
}

void CreatureAIScriptSunyou::OnHit(Unit* mTarget, float fAmount)
{

	CallLua("OnHit", mTarget, fAmount);
}

void CreatureAIScriptSunyou::OnDied(Unit *mKiller)
{


	CallLua("OnDied" , mKiller);

	
}

void CreatureAIScriptSunyou::OnDestroy()
{

	CallLua("OnDestroy");
}

void CreatureAIScriptSunyou::OnAssistTargetDied(Unit* mAssistTarget)
{

	CallLua( "OnAssistTargetDied", mAssistTarget);
}

void CreatureAIScriptSunyou::OnFear(Unit* mFeared, uint32 iSpellId)
{
	
	CallLua("OnFear", mFeared, iSpellId);
}

void CreatureAIScriptSunyou::OnFlee(Unit* mFlee)
{

	CallLua("OnFlee", mFlee);
}

void CreatureAIScriptSunyou::OnCallForHelp()
{

	CallLua("OnCallForHelp");
}

void CreatureAIScriptSunyou::OnLoad()
{
	CallLua("OnLoad");
}

void CreatureAIScriptSunyou::OnReachWP(uint32 iWaypointId, bool bForwards)
{

}

void CreatureAIScriptSunyou::OnLootTaken(Player* pPlayer, ItemPrototype *pItemPrototype)
{

}

void CreatureAIScriptSunyou::AIUpdate()
{
	
	CallLua( "AIUpdate");
}

void CreatureAIScriptSunyou::OnEmote(Player * pPlayer, EmoteType Emote)
{

}

void CreatureAIScriptSunyou::StringFunctionCall(const char * pFunc)
{

}


void CreatureAIScriptSunyou::CallLua(const char* sz, Unit* mTarget, float fAmount)
{

	std::string str =sz;
	str += m_str;
	if (!mLuaState)
	{
		return;
	}
	LuaObject fnObj = mLuaState->GetGlobal( str.c_str());
	int Id = 0;
	if (mTarget)
	{
		Id = mTarget->GetUniqueIDForLua();
	}
	if( !fnObj.IsNil() )
	{
		LuaFunction<int> luaFn( fnObj );
		luaFn(  _unit->GetUniqueIDForLua() , Id ,fAmount);
	}

}

void CreatureAIScriptSunyou::CallLua(const char* sz, Unit* mTarget, int32 fAmount)
{

	std::string str =sz;
	str += m_str;
	if (!mLuaState)
	{
		return;
	}
	LuaObject fnObj = mLuaState->GetGlobal( str.c_str());

	int Id = 0;
	if (mTarget)
	{
		Id = mTarget->GetUniqueIDForLua();
	}
	if( !fnObj.IsNil() )
	{
		LuaFunction<int> luaFn( fnObj );
		luaFn( _unit->GetUniqueIDForLua() , Id,fAmount);
	}

}

void CreatureAIScriptSunyou::CallLua(const char* sz, Unit* mTarget, uint32 fAmount)
{
	std::string str =sz;
	str += m_str;
	if (!mLuaState)
	{
		return;
	}
	LuaObject fnObj = mLuaState->GetGlobal( str.c_str());

	int Id = 0;
	if (mTarget)
	{
		Id = mTarget->GetUniqueIDForLua();
	}
	if( !fnObj.IsNil() )
	{
		LuaFunction<int> luaFn( fnObj );
		luaFn( _unit->GetUniqueIDForLua() ,Id,fAmount);
	}

}

void CreatureAIScriptSunyou::CallLua(const char* sz, Unit* mTarget )
{

	std::string str =sz;
	str += m_str;
	if (!mLuaState)
	{
		return;
	}
	LuaObject fnObj = mLuaState->GetGlobal( str.c_str());

	int Id = 0;
	if (mTarget)
	{
		Id = mTarget->GetUniqueIDForLua();
	}
	if( !fnObj.IsNil() )
	{
		LuaFunction<int> luaFn( fnObj );
		luaFn( _unit->GetUniqueIDForLua() , Id);
	}

}

void CreatureAIScriptSunyou::CallLua( const char* sz)
{
	std::string str =sz;
	str += m_str;
	if (!mLuaState)
	{
		return;
	}
	LuaObject fnObj = mLuaState->GetGlobal( str.c_str());


	if( !fnObj.IsNil() )
	{
		LuaFunction<int> luaFn( fnObj );
		luaFn( _unit->GetUniqueIDForLua());
	}
}