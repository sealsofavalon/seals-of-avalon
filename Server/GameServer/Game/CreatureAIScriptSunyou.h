#ifndef _CREATURE_AI_SCRIPT_HEAD
#define _CREATURE_AI_SCRIPT_HEAD

#include "ScriptMgr.h"
#include "../../LuaPlusForLinux/LuaPlus.h"

using namespace LuaPlus;



class SERVER_DECL CreatureAIScriptSunyou : public CreatureAIScript
{
public:
	CreatureAIScriptSunyou();
	 CreatureAIScriptSunyou(Creature* creature);
	virtual ~CreatureAIScriptSunyou(void);

public:
	virtual void Init();
	virtual void OnCombatStart(Unit* mTarget);
	virtual void OnCombatStop(Unit* mTarget);
	virtual void OnReturnFromCombat();
	virtual void OnDamageTaken(Unit* mAttacker, float fAmount);
	virtual void OnCastSpell(uint32 iSpellId);
	virtual void OnTargetParried(Unit* mTarget);
	virtual void OnTargetDodged(Unit* mTarget);
	virtual void OnTargetBlocked(Unit* mTarget, int32 iAmount);
	virtual void OnTargetCritHit(Unit* mTarget, float fAmount);
	virtual void OnTargetDied(Unit* mTarget);
	virtual void OnParried(Unit* mTarget);
	virtual void OnDodged(Unit* mTarget);
	virtual void OnBlocked(Unit* mTarget, int32 iAmount);
	virtual void OnCritHit(Unit* mTarget, float fAmount);
	virtual void OnHit(Unit* mTarget, float fAmount) ;
	virtual void OnDied(Unit *mKiller);
	virtual void OnAssistTargetDied(Unit* mAssistTarget);
	virtual void OnFear(Unit* mFeared, uint32 iSpellId);
	virtual void OnFlee(Unit* mFlee) ;
	virtual void OnCallForHelp();
	virtual void OnLoad();
	virtual void OnReachWP(uint32 iWaypointId, bool bForwards);
	virtual void OnLootTaken(Player* pPlayer, ItemPrototype *pItemPrototype);
	virtual void AIUpdate();
	virtual void OnDestroy();
	virtual void OnEmote(Player * pPlayer, EmoteType Emote);
	virtual void StringFunctionCall(const char * pFunc);

	virtual void CallLua(const char* sz, Unit* mtarget);
	virtual void CallLua(const char* sz, Unit* mtarget, float fAmount);
	virtual void CallLua(const char* sz, Unit* mtarget, int32 iAmount);
	virtual void CallLua(const char* sz, Unit* mtarget, uint32 iAmount);
	virtual void CallLua(const char* sz);

	std::string m_str;
	LuaPlus::LuaState* mLuaState;
protected:

	ui64 m_guid;
	uint32 m_Entry;

};


#endif // _CREATURE_AI_SCRIPT_HEAD