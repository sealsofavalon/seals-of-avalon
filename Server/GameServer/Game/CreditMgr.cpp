#include "StdAfx.h"
#include "CreditMgr.h"

initialiseSingleton( CreditMgr );

CreditMgr::CreditMgr() : m_lastFlushTime( 0 )
{

}

void CreditMgr::Process()
{
	if( (uint32)UNIXTIME - m_lastFlushTime > 15 )
	{
		Flush();
		m_lastFlushTime = (uint32)UNIXTIME;
	}
}

void CreditMgr::Flush()
{
	SQLCallbackBase* pCallBack = new SQLClassCallbackP0<CreditMgr>( this, &CreditMgr::GenerateBillProc );
	AsyncQuery * q = new AsyncQuery( pCallBack );
	q->AddQuery( "select * from credit_card where bill_date <= %u and banned = 0", (uint32)UNIXTIME );
	RealmDatabase.QueueAsyncQuery( q );

	SQLCallbackBase* pCallBack2 = new SQLClassCallbackP0<CreditMgr>( this, &CreditMgr::CheckBillExpire );
	AsyncQuery * q2 = new AsyncQuery( pCallBack2 );
	q2->AddQuery( "select * from credit_card where is_restored = 0 and restore_points > 0 and restore_maturity < %u and banned = 0", (uint32)UNIXTIME );
	RealmDatabase.QueueAsyncQuery( q2 );
}

void CreditMgr::GenerateBillProc( QueryResultVector& results )
{
	QueryResult *result = results[0].result;
	if( !result )
	{
		return;
	}

	do 
	{
		Field* f = result->Fetch();
		if( f )
		{
			uint32 acc_id = f[0].GetUInt32();
			uint32 av = f[1].GetUInt32();
			uint32 ma = f[2].GetUInt32();

			uint32 ad = f[4].GetUInt32();
			uint32 bd = f[5].GetUInt32();
			uint32 rm = f[6].GetUInt32();

			std::list<PlayerInfo*> lst;
			objmgr.GetPlayerInfoByAccountID( acc_id, lst );
			uint32 rp = ma > av ? ma - av : 0;

			if( RealmDatabase.WaitExecute( "update credit_card set is_restored = 0, restore_points = %u, bill_date = %u where acc_id = %u", rp, bd + TIME_DAY * 30, acc_id ) )
			{
				char txt[512] = { 0 };
				int y, m, d, h, min, s;
				convert_unix_time( rm, &y, &m, &d, &h, &min, &s );

				int y2, m2, d2, h2, min2, s2;
				convert_unix_time( (uint32)UNIXTIME, &y2, &m2, &d2, &h2, &min2, &s2 );
				sprintf( txt, "本次帐单需还款金额:[%u]个元宝<br>到期还款日:[%d-%d-%d %d:%d:%d]<br>帐单出单日:[%d-%d-%d %d:%d:%d]", rp, y, m, d, h, min, s, y2, m2, d2, h2, min2, s2 );

				MailMessage msg;
				msg.subject = "君·天下信用卡帐单";
				msg.body = txt;
				msg.stationary = 0;
				msg.delivery_time = (uint32)UNIXTIME;
				msg.cod = 0;
				msg.money = 0;
				
				msg.sender_guid = 0;
				msg.expire_time = 0;

				msg.copy_made = false;
				msg.read_flag = false;
				msg.deleted_flag = false;
				msg.message_type = 0;

				for( std::list<PlayerInfo*>::iterator it = lst.begin(); it != lst.end(); ++it )
				{
					PlayerInfo* pi = *it;
					msg.player_guid = pi->guid;
					// Great, all our info is filled in. Now we can add it to the other players mailbox.
					sMailSystem.DeliverMessage( pi->guid, &msg);

					if( pi->m_loggedInPlayer )
					{
						pi->m_loggedInPlayer->SetUInt32Value( PLAYER_FIELD_CREDIT_RESTORE, rp );
						pi->m_loggedInPlayer->SetUInt32Value( PLAYER_FIELD_CREDIT_BILL_DATE, bd + TIME_DAY * 30 );
						pi->m_loggedInPlayer->GetSession()->SendNotification( "本期信用卡帐单已寄出，请查收！" );
					}
				}
			}
		}

	} while ( result->NextRow() );

}

void CreditMgr::CheckBillExpire( QueryResultVector& results )
{
	QueryResult *result = results[0].result;
	if( !result )
	{
		return;
	}

	do 
	{
		Field* f = result->Fetch();
		if( f )
		{
			uint32 acc_id = f[0].GetUInt32();

			if( RealmDatabase.WaitExecute( "update credit_card set banned = %u where acc_id = %u", (uint32)UNIXTIME, acc_id ) )
			{
				std::list<PlayerInfo*> lst;
				objmgr.GetPlayerInfoByAccountID( acc_id, lst );

				char txt[512] = { 0 };
				int y, m, d, h, min, s;
				convert_unix_time( (uint32)UNIXTIME, &y, &m, &d, &h, &min, &s );
				sprintf( txt, "亲爱的信用卡用户,由于您在规定期限内并未足额归还帐单提示所欠金额,系统已自动将您的信用卡封停,特此通知<br>日期:[%d-%d-%d %d:%d:%d]", y, m, d, h, min, s );

				MailMessage msg;
				msg.subject = "君·天下信用卡封停通知";
				msg.body = txt;
				msg.stationary = 0;
				msg.delivery_time = (uint32)UNIXTIME;
				msg.cod = 0;
				msg.money = 0;

				msg.sender_guid = 0;
				msg.expire_time = 0;

				msg.copy_made = false;
				msg.read_flag = false;
				msg.deleted_flag = false;
				msg.message_type = 0;

				for( std::list<PlayerInfo*>::iterator it = lst.begin(); it != lst.end(); ++it )
				{
					PlayerInfo* pi = *it;
					msg.player_guid = pi->guid;
					// Great, all our info is filled in. Now we can add it to the other players mailbox.
					sMailSystem.DeliverMessage( pi->guid, &msg);

					if( pi->m_loggedInPlayer )
					{
						pi->m_loggedInPlayer->SetUInt32Value( PLAYER_FIELD_CREDIT_BANNED, (uint32)UNIXTIME );
						pi->m_loggedInPlayer->GetSession()->SendNotification( txt );
					}
				}
			}
		}

	} while ( result->NextRow() );
}
