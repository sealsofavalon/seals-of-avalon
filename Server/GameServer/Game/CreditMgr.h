#ifndef _CREDIT_MGR_HEAD
#define _CREDIT_MGR_HEAD


class CreditMgr : public Singleton<CreditMgr>
{
public:
	CreditMgr();
	void Process();

	void Flush();
	void GenerateBillProc( QueryResultVector& results );
	void CheckBillExpire( QueryResultVector& results );
private:
	uint32 m_lastFlushTime;
};

#define g_creditmgr (CreditMgr::getSingleton())

#endif // _CREDIT_MGR_HEAD