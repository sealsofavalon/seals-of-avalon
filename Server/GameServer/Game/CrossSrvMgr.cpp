#include "stdafx.h"
#include "../Net/GS_SP_ListenServer.h"
#include "../Net/GS_SP_Client.h"
#include "../Net/GS_SP_Socket.h"
#include "../../Common/Protocol/GS2GT.h"
#include "../Net/GTListenSocket.h"

bool CrossSrvMgr::Init()
{
	for( int i = 0; i < 16; ++i )
	{
		m_groups[i].id = 0;
		m_groups[i].db = NULL;
	}
	tempdb = NULL;
	m_isInstanceSrv = sMaster.m_group_id == 15;

	if( m_isInstanceSrv )
	{
		QueryResult* qr = RealmDatabase.Query( "select * from group_conf where groupid < 15" );
		if( qr )
		{
			do
			{
				Field* f = qr->Fetch();
				uint32 id = f[0].GetUInt32();
				m_groups[id].id = id;
				m_groups[id].db = Database::CreateDatabaseInterface( 1 );
				std::string ip = f[7].GetString();
				uint32 port = f[8].GetUInt32();
				std::string db_name = f[9].GetString();
				std::string user = f[10].GetString();
				std::string pwd = f[11].GetString();
				if( !m_groups[id].db->Initialize( ip.c_str(), port, user.c_str(), pwd.c_str(), db_name.c_str(), 1, 16384 ) )
				{
					MyLog::log->error("sql : sunyou_character database initialization failed. Exiting.");
					return false;
				}
			}
			while( qr->NextRow() );
			delete qr;
		}
	}
	else
	{
		//tempdb = Database::CreateDatabaseInterface( 1 );
		QueryResult* qr = RealmDatabase.Query( "select * from group_conf where groupid = 15" );
		if( qr )
		{
			Field* f = qr->Fetch();
			std::string ip = f[7].GetString();
			uint32 port = f[8].GetUInt32();
			std::string db_name = f[9].GetString();
			std::string user = f[10].GetString();
			std::string pwd = f[11].GetString();
			tempdb = Database::CreateDatabaseInterface( 1 );
			if( !tempdb->Initialize( ip.c_str(), port, user.c_str(), pwd.c_str(), db_name.c_str(), 1, 16384 ) )
			{
				MyLog::log->error("sql : sunyou_character_temp database initialization failed. Exiting.");
				return false;
			}
		}
	}
	return true;
}

void CrossSrvMgr::Run()
{
	if( !m_isInstanceSrv )
	{
		if (g_gs_sp_client)
			g_gs_sp_client->run_no_wait();
		if (tempdb)
			tempdb->QueryTaskRun();
	}
	else
	{
		for( int i = 0; i < 16; ++i )
		{
			if( m_groups[i].db )
				m_groups[i].db->QueryTaskRun();
		}

		g_gs_sp_listen_server->run_no_wait();
	}
}

Database* CrossSrvMgr::GetDBByGUID( uint32 guid )
{
	if( !m_isInstanceSrv )
		return &CharacterDatabase;
	else
		return m_groups[GetGroupIDByGUID(guid)].db;
}

void CrossSrvMgr::PushJump( user_jump_t* p )
{
	gs_sp_scocket* gs = g_gs_sp_listen_server->get_gs( p->mother_groupid );
	if(gs)
	{
		std::map<uint32, user_jump_t*>::iterator it = m_jumps.find( p->acct );
		if( it != m_jumps.end() )
		{
			delete it->second;
			it->second = p;
		}
		else
			m_jumps[p->acct] = p;

		MSG_GSP2GS::stJumpReq msg;
		msg.acct = p->acct;
		gs->PostSend( msg );
	}else
		assert(0);
}

void CrossSrvMgr::ApplyJump( uint32 acct, uint32 transid, uint8 gateid, const std::string& gmflags )
{
	std::map<uint32, user_jump_t*>::iterator it = m_jumps.find( acct );
	if( it != m_jumps.end() )
	{
		user_jump_t* p = it->second;
		if( !sInstanceMgr.HasInstance( p->mapid, p->instanceid ) )
		{
			delete p;
			m_jumps.erase( it );
			return;
		}

		CGTSocket* socket = sGTListenSocket.GetGate( gateid );
		if( !socket )
		{
			delete p;
			m_jumps.erase( it );
			return;
		}

		WorldSession *s = new WorldSession( p->acct, transid, p->guid, "", "", socket, "" );
		bool result = sWorld.AddSession(s);
		if( !result )
		{
			delete s;
			delete p;
			m_jumps.erase( it );
			return;
		}
		s->LoadSecurity( gmflags );
		sWorld.AddGlobalSession(s);
		if(s->HasGMPermissions() && s)
			sWorld.gmList.insert(s);
		s->m_muted = 0;
		s->m_baned = 0;
		s->m_bFangchenmiAccount = false;
		s->m_ChenmiTime = 0;
		s->m_LastLogout = 0;
		Player* plr = s->RealEnterGame();

		switch( p->category )
		{
		case JUMP_CATEGORY_TEAM_ARENA:
			{
				arena_jump_t* jump = static_cast<arena_jump_t*>( p );
				plr->m_team_arena_idx = jump->team_index;
			}
			break;
		default:
			assert( 0 );
			return;
		}

		plr->SetInstanceID( p->instanceid );
		plr->SetMapId( p->mapid );
	}
}

void CrossSrvMgr::OnPlayerReturn2Mother( Player* plr )
{
	if( !m_isInstanceSrv || plr->GetSession()->IsLoggingOut() )
		return;

	std::map<uint32, user_jump_t*>::iterator it = m_jumps.find( plr->m_playerInfo->acct );
	if( it != m_jumps.end() )
	{
		if( plr->m_bgEntryPointMap )
		{
			plr->GetPositionNC().x = plr->m_bgEntryPointX;
			plr->GetPositionNC().y = plr->m_bgEntryPointY;
			plr->GetPositionNC().z = plr->m_bgEntryPointZ;
			plr->GetPositionNC().o = plr->m_bgEntryPointO;
			plr->SetMapId( plr->m_bgEntryPointMap );
			plr->SetInstanceID( plr->m_bgEntryPointInstance );
		}

		uint32 transid = plr->GetSession()->_SessionID;
		user_jump_t* p = it->second;
		WorldSession* s = plr->GetSession();
		gs_sp_scocket* gs = g_gs_sp_listen_server->get_gs( p->mother_groupid );
		if( gs )
		{
			uint32 guid = plr->GetLowGUID();

			MSG_GS2GT::stJumpGS jumpmsg;
			jumpmsg.transid = s->_SessionID;
			jumpmsg.groupid = p->mother_groupid;
			s->SendPacket( jumpmsg );

			s->LogoutPlayer( true, false );

			MSG_GSP2GS::stReturn2Mother msg;
			msg.transid = transid;
			gs->PostSend( msg );
		}
		else
		{
			s->LogoutPlayer( true, true );
		}

		delete p;
		m_jumps.erase( it );
		sWorld.DeleteSession( s );
	}
}
