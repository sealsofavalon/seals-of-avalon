#ifndef _CROSS_SRV_MGR
#define _CROSS_SRV_MGR

#include "../Net/DSSocket.h"

enum jump_category_t
{
	JUMP_CATEGORY_TEAM_ARENA,
};
struct user_jump_t
{
	uint32 acct;
	uint32 category;
	uint32 guid;
	uint32 mapid;
	uint32 instanceid;
	uint8 mother_groupid;
};
struct arena_jump_t : public user_jump_t
{
	arena_jump_t() {
		category = JUMP_CATEGORY_TEAM_ARENA;
	}
	uint32 team_index;
};

class CrossSrvMgr
{
public:
	bool Init();
	void Run();
	Database* GetDBByGUID( uint32 guid );
	void PushJump( user_jump_t* p );
	void ApplyJump( uint32 acct, uint32 transid, uint8 gateid, const std::string& gmflags );
	void OnPlayerReturn2Mother( Player* plr );

	bool m_isInstanceSrv;
	Database* tempdb;

protected:
	struct group_t
	{
		uint8 id;
		Database* db;
	};
	group_t m_groups[16];

	std::map<uint32, user_jump_t*> m_jumps;
};

extern CrossSrvMgr* g_crosssrvmgr;

#endif // _CROSS_SRV_MGR
