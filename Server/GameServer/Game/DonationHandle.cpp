#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S.h"
#include "../../SDBase/Protocol/S2C.h"
#include "DonationSystem.h"

void WorldSession::HandleDonationDonateReq(CPacketUsn& packet)
{
	//return; // for beta

	if( !_player || !_player->IsInWorld() )
		return;

	MSG_C2S::stDonateReq msg;
	packet >> msg;
	
	if (g_donation_system)
	{
		g_donation_system->Donate(_player,msg.event_id, msg.yuanbao,msg.leave_words);
	}
}

void WorldSession::HandleQueryDonationLadder(CPacketUsn& packet)
{
	//return; // for beta

	if( !_player || !_player->IsInWorld() )
		return;
	MSG_C2S::stQueryDonationLadder msg;
	packet>>msg;

	if (g_donation_system)
	{
		g_donation_system->SendDonationLadder2Player(_player,msg.event_id, msg.is_total);
	}
}

void WorldSession::HandleDonationHistroy(CPacketUsn& packet)
{
	//return; // for beta

	if( !_player || !_player->IsInWorld() )
		return;

	if (g_donation_system)
	{
		g_donation_system->SendDonationHistory2Player(_player);
	}
}