#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "../../SDBase/Protocol/C2S.h"
#include "DonationSystem.h"
#include "Chat.h"
#include "World.h"
#include "Player.h"
#include "MultiLanguageStringMgr.h"
#include "../Master.h"

DonationSystem* g_donation_system = NULL;

void DonationSystem::Load()
{
	QueryResult* qr = RealmDatabase.Query( "select * from donation_history where group_id = %u", sMaster.m_group_id );
	if( qr )
	{
		do
		{
			Field* f = qr->Fetch();

			uint32 event_id = f[0].GetUInt32();
			DonationEvent* p = FindEvent( event_id );
			if( !p )
			{
				p = new DonationEvent;
				p->id = event_id;
				p->total_yuanbao = 0;
				p->start = 0;
				p->expire = 0;
				p->des_count = 0;
			}
			
			DonationLog* log = new DonationLog;
			log->event_id = event_id;
			log->character_id = f[2].GetUInt32();
			log->acct = f[3].GetUInt32();
			log->character_name = f[4].GetString();
			log->yuanbao = f[5].GetUInt32();
			log->leave_words = f[6].GetString();
			log->datetime = f[7].GetUInt32();

			p->total_yuanbao += log->yuanbao;
			p->logs.push_back( log );
		}
		while( qr->NextRow() );

		delete qr;
	}

	m_total_ladder.clear();

	int y, m, d, h, min, s;
	convert_unix_time((ui32)UNIXTIME, &y, &m, &d, &h, &min, &s );
	
	m_next_give_title_time =  (ui32)UNIXTIME - h * TIME_HOUR - min * TIME_MINUTE - s * TIME_SECOND + 3 * TIME_HOUR;
	
	while (m_next_give_title_time <= UNIXTIME)
	{
		m_next_give_title_time = UNIXTIME + TIME_DAY ;
	}
}

DonationEvent* DonationSystem::FindEvent( uint32 id )
{
	std::map<uint32, DonationEvent*>::iterator it = m_events.find( id );
	if( it != m_events.end() )
		return it->second;
	else
		return NULL;
}
DonationEvent* DonationSystem::FindDonateEvent()
{
	//优先查找ID = 1 的捐助事件
	DonationEvent* pFirst = FindEvent(1);
	if (pFirst)
	{
		if( (uint32)UNIXTIME >= pFirst->start && (uint32)UNIXTIME <= pFirst->expire )
		{
			return pFirst ;
		}
	}
	std::map<uint32, DonationEvent*>::iterator itend = m_events.end();
	for (std::map<uint32, DonationEvent*>::iterator it = m_events.begin();  it != itend; ++it)
	{
		DonationEvent* p = it->second;
		if (p)
		{
			if( (uint32)UNIXTIME >= p->start && (uint32)UNIXTIME <= p->expire )
			{
				return p ;
			}
		}
	}

	return NULL ;

}
void DonationSystem::DonateYiPai(uint32 guid, uint32 yuanbao, const std::string& leave_words)
{
	//如果玩家在线
	Player* ply = objmgr.GetPlayer( guid );
	DonationEvent* p = FindDonateEvent();
	if (!p)
	{
		return ;
	}
	if (ply)
	{
		return Donate(ply, p->id,yuanbao,leave_words,true);
	}

	DonationEvent* de = p;
	if( !de )
	{
		//ack.result = MSG_S2C::stDonateAck::DONATE_NOT_FOUND;
		//p->GetSession()->SendPacket( ack );
		return;
	}

	if( (uint32)UNIXTIME < de->start || (uint32)UNIXTIME > de->expire )
	{
		//ack.result = MSG_S2C::stDonateAck::DONATE_RESULT_EXPIRE;
		//p->GetSession()->SendPacket( ack );
		return;
	}

	PlayerInfo* playerInfo = objmgr.GetPlayerInfo( guid );
	if (playerInfo)
	{
		DonationLog* log = new DonationLog;
		log->event_id = p->id ;
		log->acct = playerInfo->acct;
		log->character_id = guid;//p->GetLowGUID();
		log->character_name = playerInfo->name;
		log->datetime = (uint32)UNIXTIME;
		log->yuanbao = yuanbao;
		log->leave_words = leave_words;



		scoped_sql_transaction_proc sstp( &RealmDatabase );
		if( RealmDatabase.TransactionExecute( "insert into donation_history values(%u, %u, %u, %u, '%s', %u, '%s', %u)",
			p->id, sMaster.m_group_id, log->character_id, log->acct, log->character_name.c_str(), log->yuanbao, log->leave_words.c_str(), log->datetime ) )
		{

			de->logs.push_back( log );

			//针对该事件重新计算排行
			uint32 total_yuanbao = yuanbao ;
			for( std::multimap<uint32, ui32>::iterator it = de->ladder.begin(); it != de->ladder.end(); ++it )
			{
				if (it->second == guid)
				{
					total_yuanbao += it->first ;
					de->ladder.erase(it);
					break;
				}	
			}
			de->ladder.insert(std::multimap<uint32, uint32>::value_type(total_yuanbao, guid));
			// 重新计算总排行

			total_yuanbao = yuanbao ;
			for( std::multimap<uint32, ui32>::iterator it = m_total_ladder.begin(); it != m_total_ladder.end(); ++it )
			{
				if (it->second == guid)
				{
					total_yuanbao += it->first ;
					m_total_ladder.erase(it);
					break;
				}
			}
			m_total_ladder.insert(std::multimap<uint32, uint32>::value_type(total_yuanbao,  guid));
			CheckGift(guid, total_yuanbao, yuanbao);
			sstp.success();

		}
		else
		{
			delete log;
		}
	}

	

	//如果不在线.

}
void DonationSystem::Donate( Player* p, uint32 event_id, uint32 yuanbao, const std::string& leave_words, bool bAuction )
{
	MSG_S2C::stDonateAck ack;
	ack.result = MSG_S2C::stDonateAck::DONATE_SYSTEM_ERROR;
	
	if (!bAuction)
	{
		if( p->GetCardPoints() < yuanbao )
		{
			ack.result = MSG_S2C::stDonateAck::DONATE_RESULT_NOT_ENOUGH_YUANBAO;
			p->GetSession()->SendPacket( ack );
			return;
		}
	}
	
	DonationEvent* de = FindEvent( event_id );
	if( !de )
	{
		ack.result = MSG_S2C::stDonateAck::DONATE_NOT_FOUND;
		p->GetSession()->SendPacket( ack );
		return;
	}

	if( (uint32)UNIXTIME < de->start || (uint32)UNIXTIME > de->expire )
	{
		ack.result = MSG_S2C::stDonateAck::DONATE_RESULT_EXPIRE;
		p->GetSession()->SendPacket( ack );
		return;
	}

	DonationLog* log = new DonationLog;
	log->event_id = event_id ;
	log->acct = p->m_playerInfo->acct;
	log->character_id = p->GetLowGUID();
	log->character_name = p->GetName();
	log->datetime = (uint32)UNIXTIME;
	log->yuanbao = yuanbao;
	log->leave_words = leave_words;

	scoped_sql_transaction_proc sstp( &RealmDatabase );
	if( RealmDatabase.TransactionExecute( "insert into donation_history values(%u, %u, %u, %u, '%s', %u, '%s', %u)",
		event_id, sMaster.m_group_id, log->character_id, log->acct, log->character_name.c_str(), log->yuanbao, log->leave_words.c_str(), log->datetime ) )
	{
		if( bAuction || p->SpendPointsNotUseLock( yuanbao, "donation" ) )
		{
			de->logs.push_back( log );

			//针对该事件重新计算排行
			uint32 total_yuanbao = yuanbao ;
			for( std::multimap<uint32, ui32>::iterator it = de->ladder.begin(); it != de->ladder.end(); ++it )
			{
				if (it->second == p->GetGUID())
				{
					total_yuanbao += it->first ;
					de->ladder.erase(it);
					break;
				}	
			}
			de->ladder.insert(std::multimap<uint32, uint32>::value_type(total_yuanbao,  p->GetGUID()));
			// 重新计算总排行

			total_yuanbao = yuanbao ;
			for( std::multimap<uint32, ui32>::iterator it = m_total_ladder.begin(); it != m_total_ladder.end(); ++it )
			{
				if (it->second == p->GetGUID())
				{
					total_yuanbao += it->first ;
					m_total_ladder.erase(it);
					break;
				}
			}
			m_total_ladder.insert(std::multimap<uint32, uint32>::value_type(total_yuanbao,  p->GetGUID()));
			
			CheckGift(p->GetGUID(), total_yuanbao, yuanbao);
			//
			sstp.success();
			ack.result = MSG_S2C::stDonateAck::DONATE_RESULT_OK;

			//添加新的捐赠事件
			MSG_S2C::stAddDonateHistroy msg;
			msg.datatime = log->datetime;
			msg.event_id = log->event_id;
			msg.yuanbao = yuanbao;

			p->GetSession()->SendPacket( msg );

		}
	}
	else
	{
		delete log;
	}
	p->GetSession()->SendPacket( ack );
}

void DonationSystem::SendDonationHistory2Player( Player* player )
{
	MSG_S2C::stDonationHistory msg;
	for( std::map<uint32, DonationEvent*>::iterator it = m_events.begin(); it != m_events.end(); ++it )
	{
		DonationEvent* p = it->second;
		
		for( std::vector<DonationLog*>::iterator it2 = p->logs.begin(); it2 != p->logs.end(); ++it2 )
		{
			DonationLog* log = *it2;
			//msg.
			if (log->character_id == player->GetGUID())
			{
				MSG_S2C::stDonationHistory::evt stEvt;
				stEvt.event_id = log->event_id;
				stEvt.datatime = log->datetime;
				stEvt.yuanbao = log->yuanbao ;

				msg.v.push_back(stEvt);
			}
			
		}
	}

	player->GetSession()->SendPacket( msg );	
}

void DonationSystem::SendDonationLadder2Player( Player* player, uint32 query_event_id , bool is_total)
{
	MSG_S2C::stDonationLadderAck msg ;
	msg.query_event_id = query_event_id ;
	msg.local_query = 0;

	//
	int player_ladder =  0 ;
	MSG_S2C::stDonationLadderAck::info player_data ;
	player_data.name = player->GetName();
	player_data.level = player->getLevel();
	player_data.yuanbao = 0;
	if (player->GetGuild())
	{
		player_data.guild_name = player->GetGuild()->GetGuildName();
	}
	
	
	if (!is_total)
	{
		// 单个事件的排行
		DonationEvent* p = FindEvent(query_event_id);
		if (p)
		{
			for( std::multimap<uint32, uint32>::reverse_iterator it = p->ladder.rbegin(); it != p->ladder.rend(); ++it )
			{
				MSG_S2C::stDonationLadderAck::info data ;
				PlayerInfo* pi = objmgr.GetPlayerInfo( it->second );
				if (pi)
				{
					if (it->second == player->GetGUID())
					{
						player_data.yuanbao = it->first;
						msg.local_query = player_ladder;
						if( msg.v.size() == 19 )
							break;
					}
					if (msg.v.size() < 19)
					{
						data.name =  pi->name;
						data.yuanbao = it->first;
						data.level = pi->lastLevel;

						if (pi->guild)
						{
							data.guild_name = pi->guild->GetGuildName();
						}
						msg.v.push_back(data);
					}
				}

				player_ladder ++;
				
			}
		}
	}else
	{
		
		for( std::multimap<uint32, uint32>::reverse_iterator it = m_total_ladder.rbegin(); it != m_total_ladder.rend(); ++it )
		{
			MSG_S2C::stDonationLadderAck::info data ;
			PlayerInfo* pi = objmgr.GetPlayerInfo( it->second );
			if (pi)
			{
				if (it->second == player->GetGUID())
				{
					player_data.yuanbao = it->first;
					msg.local_query = player_ladder;
					if( msg.v.size() == 19 )
						break;

				}
				if (msg.v.size() < 19)
				{
					data.name =  pi->name;
					data.yuanbao = it->first;
					data.level = pi->lastLevel;
					if (pi->guild)
					{
						data.guild_name = pi->guild->GetGuildName();
					}
					msg.v.push_back(data);
				}
			}

			player_ladder ++;
		}
	}

	msg.v.push_back(player_data);
	msg.total_query = is_total;
	
	player->GetSession()->SendPacket( msg );	
}
void DonationSystem::SendDonationEventList2Player( Player* player )
{
	MSG_S2C::stDonationEventList msg;
	for (std::map<ui32, DonationEvent*>::iterator it = m_events.begin(); it != m_events.end(); ++it)
	{
		DonationEvent* p = it->second;
		MSG_S2C::stDonationEventList::evt stEvt ;
		stEvt.id = p->id;
		stEvt.start = p->start;
		stEvt.expire = p->expire;
		stEvt.yuanbao = p->total_yuanbao;
		stEvt.des_count = p->des_count;
		msg.v.insert(msg.v.begin(),stEvt);
	}
	player->GetSession()->SendPacket( msg );	
}
void DonationSystem::UpdateDonationEvent(uint32 id, uint32 start,uint32 expire, uint32 des_count)
{
	DonationEvent* p = FindEvent( id );
	if (p)
	{
		p->id = id;
		p->start = start;
		p->expire = expire;
		p->des_count = des_count ;
	}

}

void DonationSystem::AddDonationEvent( uint32 id, uint32 start, uint32 expire,uint32 des_count )
{
	DonationEvent* p = FindEvent( id );
	if( !p )
	{
		p = new DonationEvent;
		p->id = id;
		p->total_yuanbao = 0;
		p->start = start;
		p->expire = expire;
		p->des_count = des_count;
		m_events.insert( std::map<uint32, DonationEvent*>::value_type( id, p ) );
	}
	else
	{
		p->start = start;
		p->expire = expire;
		p->des_count = des_count;
	}
}

void DonationSystem::RemoveDonationEvent( uint32 id )
{
	std::map<uint32, DonationEvent*>::iterator it = m_events.find( id );
	if( it != m_events.end() )
	{
		delete it->second;
		m_events.erase( it );
	}
}
void DonationSystem::GiveDonationTitle()
{
	ui32 count = 0;
	for( std::multimap<uint32, uint32>::reverse_iterator it = m_total_ladder.rbegin(); it != m_total_ladder.rend(); ++it )
	{
		if (count <= 2)
		{
			PlayerInfo* pInfo=objmgr.GetPlayerInfo( it->second);
			if (pInfo)
			{	
				ui32 title = 0;
				if (count == 0)
				{
					title = 55;
				}
				if (count == 1)
				{
					title = 56;
				}
				if (count == 2)
				{
					title = 57;
				}

				if (pInfo->m_loggedInPlayer)
				{
					MSG_S2C::stTitleAdd msg;
					msg.title.title = title;
					pInfo->m_loggedInPlayer->GetSession()->SendPacket(msg);
				}
				objmgr.InsertTitle(pInfo, title, m_next_give_title_time);
				count++;
			}
		}else
		{
			break ;
		}
	}
}
void DonationSystem::Update()
{
	if ((ui32)UNIXTIME >= m_next_give_title_time)
	{
		m_next_give_title_time = UNIXTIME + TIME_DAY;
		GiveDonationTitle();
	}
}
void DonationSystem::CheckGift( uint32 who, uint32 total, uint32 thistime )
{
	//Player* pkPlayer = objmgr.GetPlayer(who);

	int Gift = 0;
	int TotalGift = 0;
	StorageContainerIterator<contribution_gift>* it = dbcContributionGift.MakeIterator();
	std::multimap<uint32, contribution_gift*> multim;
	while(!it->AtEnd())
	{
		contribution_gift* cg = it->Get();
		if( cg->is_accumulation && total >= cg->condition && total - thistime < cg->condition )
		{
			//总捐助的奖励
		
			/*
			if (cg->condition > Gift)
			{
				TotalGift = cg->condition;
			}
			*/
			TotalGift = cg->condition;
			
			Item *itm = objmgr.CreateItem(cg->misc1, NULL);
			if(!itm)
			{
				return;
			}

			itm->m_isDirty=true;
			itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, cg->misc2);
			itm->SetOwner(NULL);
			itm->SaveToDB( 0, 0, true, NULL );

			/*
			string subject = "慈善捐助奖励";
			char buf[1024];

			sprintf(buf, "亲爱的玩家，感谢你的爱心，由于您累计捐助已经超过了%d元宝。系统奖励您如下道具。希望您再接再厉。", cg->condition);
			string body = buf;
			*/
			sMailSystem.SendAutomatedMessage(NORMAL,0,who, build_language_string( BuildString_DonationMailSubjectAccumulationGift ),
											build_language_string( BuildString_DonationMailBodyAccumulationGift, quick_itoa(cg->condition) ), 0, 0, itm->GetGUID(), 62, false, false);
			
			delete itm;		
		}
		else if( !cg->is_accumulation )
			multim.insert( std::make_pair( cg->condition, cg ) );

		if(!it->Inc())
			break;
	}
	it->Destruct();

	for( std::multimap<uint32, contribution_gift*>::reverse_iterator it = multim.rbegin(); it != multim.rend(); ++it )
	{
		contribution_gift* cg = it->second;
		if( thistime >= cg->condition )
		{	
			Item *itm = objmgr.CreateItem(cg->misc1, NULL);
			if(!itm)
			{
				return;
			}
			/*
			if (cg->condition > Gift)
			{
				Gift = cg->condition;
			}
			*/
			Gift = cg->condition;

			itm->m_isDirty=true;
			itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, cg->misc2);
			itm->SetOwner(NULL);
			itm->SaveToDB( 0, 0, true, NULL );

			// string subject = "慈善捐助奖励";
			//char buf[1024];

			//sprintf(buf, "亲爱的玩家，感谢你的爱心，由于您本次捐助已经超过了%d元宝。系统奖励您如下道具。希望您再接再厉。", cg->condition);


			sMailSystem.SendAutomatedMessage(NORMAL,0,who, build_language_string( BuildString_DonationMailSubjectOnceGift ),
												build_language_string( BuildString_DonationMailBodyOnceGift, quick_itoa(cg->condition) ), 0, 0, itm->GetGUID(), 62, false, false);

			delete itm;

			break;
		}
	}

	PlayerInfo* pInfo=objmgr.GetPlayerInfo(who);
	if (pInfo)
	{
		if (Gift && thistime >= 1000 )
		{
			// sprintf(szTxt, "爱心大使[%s]捐助了%d元宝(累计:%d)，获得了系统的奖励！", pInfo->name, thistime, total);
			MSG_S2C::stChat_Message Msg1;
			sChatHandler.FillMessageData( CHAT_MSG_SYSTEM_BROADCAST, build_language_string( BuildString_DonationSystemMessageOnce, pInfo->name, quick_itoa(thistime), quick_itoa(total) ), 0, &Msg1, 0 );
			sWorld.SendGlobalMessage(Msg1);
		}

		if (TotalGift)
		{
			// sprintf(szTxt, "爱心大使[%s]累计捐助了%d元宝，获得了丰厚的系统奖励！", pInfo->name, total);
			MSG_S2C::stChat_Message Msg2;
			sChatHandler.FillMessageData( CHAT_MSG_SYSTEM_BROADCAST, build_language_string( BuildString_DonationSystemMessageAccumulation, pInfo->name, quick_itoa(total) ), 0, &Msg2, 0 );
			sWorld.SendGlobalMessage(Msg2);
		}
	}
}
void DonationSystem::CalcLadder()
{
	//单个事件计算排行
	for( std::map<uint32, DonationEvent*>::iterator it = m_events.begin(); it != m_events.end(); ++it )
	{
		DonationEvent* p = it->second;
		std::map<uint32, uint32> m;
		for( std::vector<DonationLog*>::iterator it2 = p->logs.begin(); it2 != p->logs.end(); ++it2 )
		{
			DonationLog* log = *it2;
			std::map<uint32, uint32>::iterator it3 = m.find( log->character_id );
			if( it3 != m.end() )
				it3->second += log->yuanbao;
			else
				m[log->character_id] = log->yuanbao;
		}

		p->ladder.clear();
		for( std::map<uint32, uint32>::iterator it4 = m.begin(); it4 != m.end(); ++it4 )
		{
			PlayerInfo* pi = objmgr.GetPlayerInfo( it4->first );
			if( pi )
				p->ladder.insert( std::multimap<uint32, uint32>::value_type(it4->second, it4->first) );
		}
	}


	//计算总排行
	std::map<uint32, uint32> num;
	m_total_ladder.clear();
	for( std::map<uint32, DonationEvent*>::iterator it = m_events.begin(); it != m_events.end(); ++it )
	{
		for( std::multimap<uint32, uint32>::iterator it2 = it->second->ladder.begin(); it2 != it->second->ladder.end(); ++it2 )
		{
			std::map<uint32, uint32>::iterator it3 = num.find(it2->second);
			if (it3 != num.end())
			{
				it3->second += it2->first;
			}else
			{
				num[it2->second] = it2->first;
			}
		}
	}

	for( std::map<uint32, uint32>::iterator it4 = num.begin(); it4 != num.end(); ++it4 )
	{
		PlayerInfo* pi = objmgr.GetPlayerInfo( it4->first );
		if( pi )
			m_total_ladder.insert( std::multimap<uint32, uint32>::value_type(it4->second, it4->first) );
	}
}
