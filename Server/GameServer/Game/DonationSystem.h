#ifndef _DONATION_SYSTEM_HEAD
#define _DONATION_SYSTEM_HEAD

struct DonationLog
{
	uint32 event_id;
	uint32 acct;
	uint32 character_id;
	std::string character_name;
	uint32 datetime;
	uint32 yuanbao;
	std::string leave_words;
};

struct DonationEvent
{
	uint32 id;
	uint32 total_yuanbao;
	uint32 start;
	uint32 expire;
	uint32 des_count;

	std::vector<DonationLog*> logs;
	std::multimap<uint32, uint32> ladder;
};

class DonationSystem
{
public:
	void Load();
	DonationEvent* FindEvent( uint32 id );
	DonationEvent* FindDonateEvent();//获取可以捐助的事件
	void Donate(Player* p, uint32 event_id, uint32 yuanbao, const std::string& leave_words, bool bAuction = false );
	void DonateYiPai(uint32 guid, uint32 yuanbao, const std::string& leave_words);
	void SendDonationHistory2Player( Player* player );
	void SendDonationEventList2Player( Player* player );
	void SendDonationLadder2Player( Player* player , uint32 query_event_id, bool is_total);
	void AddDonationEvent( uint32 id, uint32 start, uint32 expire, uint32 des_count );
	void UpdateDonationEvent(uint32 id, uint32 start,uint32 expire, uint32 des_count);
	void RemoveDonationEvent( uint32 id );
	void CalcLadder();
	void CheckGift( uint32 who, uint32 total, uint32 thistime );
	void GiveDonationTitle();
	void Update();
private:
	std::map<uint32, DonationEvent*> m_events;
	std::multimap<uint32, uint32> m_total_ladder;
	ui32 m_next_give_title_time;
};

extern DonationSystem* g_donation_system;

#endif //_DONATION_SYSTEM_HEAD
