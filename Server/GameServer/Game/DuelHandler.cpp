#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Duel.h"
#include "../../SDBase/Protocol/S2C_Duel.h"

void WorldSession::HandleDuelAccepted(CPacketUsn& packet)
{
	if( !_player->IsInWorld() )
		return;

	if( _player->DuelingWith == NULL )
		return;

	if( _player->m_duelState != DUEL_STATE_FINISHED )
		return;
	
	if( _player->m_duelCountdownTimer > 0 )
		return;

	_player->m_duelStatus = DUEL_STATUS_INBOUNDS;
	_player->DuelingWith->m_duelStatus = DUEL_STATUS_INBOUNDS;

	_player->m_duelState = DUEL_STATE_STARTED;
	_player->DuelingWith->m_duelState = DUEL_STATE_STARTED;

	MSG_S2C::stDuel_CountDown Msg;
	Msg.duelCountdownTimer = 3000;

	SendPacket( Msg );
	_player->DuelingWith->m_session->SendPacket( Msg );

	_player->m_duelCountdownTimer = 3000;

	sEventMgr.AddEvent(_player, &Player::DuelCountdown, EVENT_PLAYER_DUEL_COUNTDOWN, 1000, 3,0);
}

void WorldSession::HandleDuelCancelled(CPacketUsn& packet)
{
	if( _player->DuelingWith ==  NULL )
		return;

	if( _player->m_duelState == DUEL_STATE_STARTED )
	{
		_player->DuelingWith->EndDuel( DUEL_WINNER_KNOCKOUT );
		return;
	}

	MSG_S2C::stDuel_Complete Msg;
	Msg.unk = 1;
	SendPacket( Msg );
	_player->DuelingWith->m_session->SendPacket( Msg );

	GameObject* arbiter = _player->GetMapMgr() ? _player->GetMapMgr()->GetGameObject( _player->GetUInt32Value( PLAYER_DUEL_ARBITER ) ) : NULL;
	if( arbiter != NULL )
	{
		arbiter->RemoveFromWorld( true );
		delete arbiter;
 	}

	_player->DuelingWith->SetUInt64Value( PLAYER_DUEL_ARBITER, 0 );
	_player->SetUInt64Value( PLAYER_DUEL_ARBITER, 0 );

	_player->DuelingWith->SetUInt32Value( PLAYER_DUEL_TEAM, 0 );
	_player->SetUInt32Value( PLAYER_DUEL_TEAM, 0);

	_player->DuelingWith->m_duelState = DUEL_STATE_FINISHED;
	_player->m_duelState = DUEL_STATE_FINISHED;

	_player->DuelingWith->m_duelCountdownTimer = 0;
	_player->m_duelCountdownTimer = 0;

	_player->DuelingWith->DuelingWith = NULL;
	_player->DuelingWith = NULL;

}
