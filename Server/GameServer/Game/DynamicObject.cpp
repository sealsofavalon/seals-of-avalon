#include "StdAfx.h"
#include "DynamicObject.h"
#include "SunyouRaid.h"

DynamicObject::DynamicObject(uint32 high, uint32 low)
{
	m_objectTypeId = TYPEID_DYNAMICOBJECT;
	m_valuesCount = DYNAMICOBJECT_END;
	m_uint32Values = _fields;
	memset(m_uint32Values, 0,(DYNAMICOBJECT_END)*sizeof(uint32));
	m_updateMask.SetCount(DYNAMICOBJECT_END);
	m_uint32Values[OBJECT_FIELD_TYPE] = TYPE_DYNAMICOBJECT|TYPE_OBJECT;
	m_uint32Values[OBJECT_FIELD_GUID] = low;
	m_uint32Values[OBJECT_FIELD_GUID+1] = high;
	m_floatValues[OBJECT_FIELD_SCALE_X] = 1;


	m_parentSpell=NULL;
	m_aliveDuration = 0;
	u_caster = 0;
	m_spellProto = 0;
	p_caster = 0;
	g_caster = 0;
	m_casterGUID = 0;

	m_timeToMove = 0;
	m_timeMoved = 0;
	m_destinationX = 0;
	m_destinationY = 0;
	m_destinationZ = 0;
	m_timeToUpdatePosition = 0;
}

DynamicObject::~DynamicObject()
{
	// remove aura from all targets
	DynamicObjectList::iterator jtr  = targets.begin();
	DynamicObjectList::iterator jend = targets.end();
	Unit * target;

	while(jtr != jend)
	{
		target = *jtr;
		++jtr;
		if( target->IsValid() )
			target->RemoveAura(m_spellProto->Id);
	}

	if(u_caster && u_caster->IsValid() && u_caster->dynObj == this)
		u_caster->dynObj = 0;

	if( p_caster && p_caster->IsValid() )
		p_caster->m_setDynamicObjs.erase(this);
}

void DynamicObject::Create(Unit * caster, Spell * pSpell, float x, float y, float z, uint32 duration, float radius)
{
	Object::_Create(caster->GetMapId(),x, y, z, caster->GetOrientation());

	if( caster )
		m_casterGUID = caster->GetGUID();
	else
		m_casterGUID = 0;

	//if(pSpell->g_caster)
	{
		m_parentSpell = pSpell;
	}
	p_caster = pSpell->p_caster;

	m_spellProto = pSpell->m_spellInfo;
	SetUInt64Value(DYNAMICOBJECT_CASTER, caster->GetGUID());

	m_uint32Values[OBJECT_FIELD_ENTRY] = m_spellProto->Id;
	m_uint32Values[DYNAMICOBJECT_BYTES] = 0x01eeeeee;
	m_uint32Values[DYNAMICOBJECT_SPELLID] = m_spellProto->Id;

	m_floatValues[DYNAMICOBJECT_RADIUS] = radius;

	m_aliveDuration = duration;
	u_caster = caster;
	m_faction = caster->m_faction;
	m_factionDBC = caster->m_factionDBC;
	if(caster->dynObj != 0)
	{
		// expire next update
		//caster->dynObj->m_aliveDuration = 1;
		//caster->dynObj->UpdateTargets();
	}
	caster->dynObj = this;
	if(pSpell->g_caster)
	{
	   PushToWorld(pSpell->g_caster->GetMapMgr());
	}else 
		PushToWorld(caster->GetMapMgr());
	
	if(p_caster)
	{
		p_caster->m_setDynamicObjs.insert(this);
	}
	if(duration)
	{
		sEventMgr.AddEvent(this, &DynamicObject::UpdateTargets, EVENT_DYNAMICOBJECT_UPDATE, 100, 0,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);

		SunyouRaid* raid = caster->GetSunyouRaid();
		if( raid )
			raid->OnDynamicObjectCreated( caster, this );
	}
}

void DynamicObject::AddInRangeObject( Object* pObj )
{
	if( pObj->IsUnit() )
	{
		bool attackable;
		if( p_caster != NULL)
			attackable = isAttackable( p_caster, pObj );
		else if (u_caster)
		{
			attackable = isAttackable( u_caster, pObj );
		}
		else
		{
			attackable = isAttackable(this , pObj);
		}
		
		if( attackable )
			m_inRangeOppFactions.insert( static_cast< Unit* >( pObj ) );
	}
	Object::AddInRangeObject( pObj );
}

void DynamicObject::OnRemoveInRangeObject( Object* pObj )
{
	if( pObj->IsUnit() )
	{
		m_inRangeOppFactions.erase( static_cast< Unit* >( pObj ) );
		targets.erase( static_cast< Unit* >( pObj ) );
	}
	Object::OnRemoveInRangeObject( pObj );
}

void DynamicObject::UpdateTargets_HealMP_NoAura_Area_Radiu(uint32 damage)
{
	std::set<Object*>::iterator itr = GetInRangeSetBegin(),itr2;
	std::set<Object*>::iterator iend = GetInRangeSetEnd();
	Unit * target;
	float radius = m_floatValues[DYNAMICOBJECT_RADIUS]*m_floatValues[DYNAMICOBJECT_RADIUS];

	while(itr != iend)
	{
		itr2 = itr;
		++itr;

		if( !( (*itr2)->IsUnit() ) || ! static_cast< Unit* >( *itr2 )->isAlive() || ( static_cast< Creature* >( *itr2 )->IsTotem() && !static_cast< Unit* >( *itr2 )->IsPlayer() ) )
			continue;

		target = static_cast< Unit* >( *itr2 );

		if( !isFriendly( p_caster, target) )
			continue;

		// skip units already hit, their range will be tested later
		if(targets.find(target) != targets.end())
			continue;

		if(GetDistanceSq(target) <= radius)
		{
			if(p_caster)
			{
				p_caster->HandleProc(PROC_ON_CAST_SPECIFIC_SPELL | PROC_ON_CAST_SPELL,target, m_spellProto);
				p_caster->m_procCounter = 0;
			}

			float time = 0.0f;
			if(m_spellProto->speed > 0.01f)
				time = (u_caster->CalcDistance(target) * 1000.0f) / m_spellProto->speed;
			sEventMgr.AddEvent(u_caster,&Unit::EventHealMPWithAbility,target->GetGUID(),
				m_spellProto, (uint32)damage, EVENT_SPELL_DAMAGE_HIT, (uint32)time, 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
		}
	}

	Remove();
}
void DynamicObject::UpdateTargets_HealHP_NoAura_Area_Radiu(uint32 damage)
{
	std::set<Object*>::iterator itr = GetInRangeSetBegin(),itr2;
	std::set<Object*>::iterator iend = GetInRangeSetEnd();
	Unit * target;
	float radius = m_floatValues[DYNAMICOBJECT_RADIUS]*m_floatValues[DYNAMICOBJECT_RADIUS];

	while(itr != iend)
	{
		itr2 = itr;
		++itr;

		if( !( (*itr2)->IsUnit() ) || ! static_cast< Unit* >( *itr2 )->isAlive() || ( static_cast< Creature* >( *itr2 )->IsTotem() && !static_cast< Unit* >( *itr2 )->IsPlayer() ) )
			continue;

		target = static_cast< Unit* >( *itr2 );

		if( !isFriendly( p_caster, target) )
			continue;

		// skip units already hit, their range will be tested later
		if(targets.find(target) != targets.end())
			continue;

		if(GetDistanceSq(target) <= radius)
		{
			if(p_caster)
			{
				p_caster->HandleProc(PROC_ON_CAST_SPECIFIC_SPELL | PROC_ON_CAST_SPELL,target, m_spellProto);
				p_caster->m_procCounter = 0;
			}

			float time = 0.0f;
			if(m_spellProto->speed > 0.01f)
				time = (u_caster->CalcDistance(target) * 1000.0f) / m_spellProto->speed;
			sEventMgr.AddEvent(u_caster,&Unit::EventHealHPWithAbility,target->GetGUID(),
				m_spellProto, (uint32)damage, EVENT_SPELL_DAMAGE_HIT, (uint32)time, 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
		}
	}

	Remove();
}
void DynamicObject::UpdateTargets_Damage_NoAura_Area_Radiu(uint32 damage)
{
	if( u_caster )
		if( !u_caster->IsValid() || u_caster->GetGUID() != m_casterGUID )
			return;

	if( g_caster )
		if( !g_caster->IsValid() )
			return;

	std::set<Object*>::iterator itr = GetInRangeSetBegin(),itr2;
	std::set<Object*>::iterator iend = GetInRangeSetEnd();
	Unit * target;
	float radius = m_floatValues[DYNAMICOBJECT_RADIUS]*m_floatValues[DYNAMICOBJECT_RADIUS];

	SunyouRaid* raid = NULL;
	if( u_caster )
		raid = u_caster->GetSunyouRaid();

	while(itr != iend)
	{
		itr2 = itr;
		Object* obj = *itr2;
		++itr;

		if( !( obj->IsUnit() ) || ! static_cast< Unit* >( obj )->isAlive() || ( static_cast< Creature* >( obj )->IsTotem() && !static_cast< Unit* >( obj )->IsPlayer() ) )
			continue;

		target = static_cast< Unit* >( obj );

		if( u_caster )
		{
			if( !isAttackable( u_caster, target, !(m_spellProto->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED) ) )
				continue;
		}
		else if (g_caster)
		{
			if( g_caster->m_summoner && g_caster->m_summoner->IsValid() )
			{
				if( !isAttackable( g_caster->m_summoner, target, !(m_spellProto->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED) ) )
					continue;
			}
			else if( !isAttackable( g_caster, target, !(m_spellProto->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED) ) )
				continue;

		}
		if( g_caster && target == g_caster->m_summoner )
			continue;

		// skip units already hit, their range will be tested later
		if(targets.find(target) != targets.end())
			continue;

		if(GetDistanceSq(target) <= radius)
		{
			if(p_caster)
			{
				p_caster->HandleProc(PROC_ON_CAST_SPECIFIC_SPELL | PROC_ON_CAST_SPELL,target, m_spellProto);
				p_caster->m_procCounter = 0;
			}

			//float time = 0.0f;
			//if(m_spellProto->speed > 0.01f)
			//	time = (u_caster->CalcDistance(target) * 1000.0f) / m_spellProto->speed;
			if( u_caster && !g_caster && m_spellProto->Spell_Dmg_Type != SPELL_DMG_TYPE_MAGIC)
			{

				
				int RealDamage = CalculateDamage(u_caster, target, m_spellProto->Spell_Dmg_Type, 0, m_spellProto, false, damage);
				//sEventMgr.AddEvent(u_caster,&Unit::EventStrikeWithAbility,target->GetGUID(),
				//	m_spellProto, (int32)damage, (int32)0, (uint32)0, (bool)false, (bool)false, (bool)false,true, false,  EVENT_SPELL_DAMAGE_HIT, (uint32)time, 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
				u_caster->EventStrikeWithAbility( target->GetGUID(), m_spellProto, (int)RealDamage, 0, (uint32)0, false, false, false, true, false );
			}
			else
			{

				int RealDamage = CalculateDamage(u_caster, target, m_spellProto->Spell_Dmg_Type, 0, m_spellProto, false, damage);
				if( g_caster )
				{
					//g_caster->DealDamage( target, damage, 0, 0, m_spellProto->Id );
					g_caster->SpellNonMeleeDamageLog( target, m_spellProto->Id, RealDamage, false );
				}
				else if (u_caster)
				{
					u_caster->SpellNonMeleeDamageLog( target, m_spellProto->Id, RealDamage, false );
				}
			}
		}
	}
	Remove();
}


void DynamicObject::UpdateTargets_Buf_Area_Radiu(uint32 i, uint32 damage)
{
	std::set<Object*>::iterator itr = GetInRangeSetBegin(),itr2;
	std::set<Object*>::iterator iend = GetInRangeSetEnd();
	Unit * target;
	float radius = m_parentSpell->GetRadius(i);

	while(itr != iend)
	{
		itr2 = itr;
		++itr;

		Object* obj = *itr2;
		if( !( obj->IsUnit() ) || ! static_cast< Unit* >( obj )->isAlive() || (obj->IsCreature() && static_cast< Creature* >( obj )->IsTotem()) )
			continue;

		target = static_cast< Unit* >( *itr2 );

		if( !isAttackable( u_caster, target, !(m_spellProto->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED) ) )
			continue;

		// skip units already hit, their range will be tested later
		if(targets.find(target) != targets.end())
			continue;

		if(GetDistanceSq(target) <= radius)
		{
			if(p_caster)
			{
				p_caster->HandleProc(PROC_ON_CAST_SPECIFIC_SPELL | PROC_ON_CAST_SPELL,target, m_spellProto);
				p_caster->m_procCounter = 0;
			}

			//check if we already have stronger aura
			Aura *pAura;

			std::map<uint32,Aura*>::iterator itr=target->tmpAura.find(m_spellProto->Id);
			//if we do not make a check to see if the aura owner is the same as the caster then we will stack the 2 auras and they will not be visible client sided
			if(itr==target->tmpAura.end())
			{
				//buf���ȼ�
				{
					if(m_spellProto->EffectApplyAuraName[i] >= TOTAL_SPELL_AURAS)
					{
						MyLog::log->error("find a auraname which is bigger than %u", TOTAL_SPELL_AURAS);
						return;
					}
					uint32 oldspellid = target->m_spellAuras[m_spellProto->EffectApplyAuraName[i]];
					if(oldspellid)
					{
						SpellEntry* pOldSpell = dbcSpell.LookupEntry(oldspellid);
						if(pOldSpell)
						{
							if(pOldSpell->SpellGroupType == m_spellProto->SpellGroupType && pOldSpell->spellLevel > m_spellProto->spellLevel)
							{
								return;
							}
						}
					}
				}
				uint32 Duration=m_parentSpell->GetDuration();

				// Handle diminishing returns, if it should be resisted, it'll make duration 0 here.
				if(!(m_spellProto->Attributes & ATTRIBUTES_PASSIVE)) // Passive
					::ApplyDiminishingReturnTimer(&Duration, target, m_spellProto);

				if(!Duration)
				{
					//maybe add some resist messege to client here ?
					return;
				}
				
				pAura=new Aura(m_spellProto, Duration, p_caster, target);
		
				pAura->pSpellId = m_spellProto->Id; //this is required for triggered spells

				target->tmpAura[m_spellProto->Id] = pAura;		
				target->m_spellAuras[m_spellProto->EffectApplyAuraName[i]] = m_spellProto->Id;
				target->m_spellAurasEffectPtr[m_spellProto->EffectApplyAuraName[i]] = pAura;
			}
			else
			{
				pAura=itr->second;
			} 
			pAura->AddMod(m_spellProto->EffectApplyAuraName[i], m_spellProto->EffectBasePoints[i],m_spellProto->EffectMiscValue[i],i);
		}
	}

	Remove();
}

void DynamicObject::UpdateTargets_Apply_Effect( uint32 i, bool friendly )
{
	if( u_caster )
		if( !u_caster->IsValid() || u_caster->GetGUID() != m_casterGUID )
			return;

	if( g_caster )
		if( !g_caster->IsValid() )
			return;

	std::set<Object*>::iterator itr = GetInRangeSetBegin(),itr2;
	std::set<Object*>::iterator iend = GetInRangeSetEnd();
	Unit * target;
	float radius = m_floatValues[DYNAMICOBJECT_RADIUS]*m_floatValues[DYNAMICOBJECT_RADIUS];

	SunyouRaid* raid = NULL;
	if( u_caster )
		raid = u_caster->GetSunyouRaid();

	while(itr != iend)
	{
		itr2 = itr;
		Object* obj = *itr2;
		++itr;

		if( !( obj->IsUnit() ) || ! static_cast< Unit* >( obj )->isAlive() || ( static_cast< Creature* >( obj )->IsTotem() && !static_cast< Unit* >( obj )->IsPlayer() ) )
			continue;

		target = static_cast< Unit* >( obj );

		if( !friendly )
		{
			if( u_caster )
			{
				if( !isAttackable( u_caster, target, !(m_spellProto->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED) ) )
					continue;
			}
			else if (g_caster)
			{
				if( g_caster->m_summoner && g_caster->m_summoner->IsValid() )
				{
					if( !isAttackable( g_caster->m_summoner, target, !(m_spellProto->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED) ) )
						continue;
				}
				else if( !isAttackable( g_caster, target, !(m_spellProto->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED) ) )
					continue;
			}
		}
		else
		{
			if( isHostile( u_caster, target ) )
				continue;
		}
		if( g_caster && target == g_caster->m_summoner )
			continue;

		// skip units already hit, their range will be tested later
		if(targets.find(target) != targets.end())
			continue;

		if(GetDistanceSq(target) <= radius)
		{
			if(p_caster)
			{
				p_caster->HandleProc(PROC_ON_CAST_SPECIFIC_SPELL | PROC_ON_CAST_SPELL,target, m_spellProto);
				p_caster->m_procCounter = 0;
			}
			
			Spell sp( u_caster, m_spellProto, false, NULL );
			sp.m_external = true;
			sp.u_caster = u_caster;
			sp.p_caster = p_caster;
			sp.HandleEffects( target->GetGUID(), i );
		}
	}
	Remove();
}

void DynamicObject::UpdateTargets()
{
	if(m_aliveDuration == 0)
		return;

	if(m_aliveDuration >= 100)
	{
//		FactionRangeList::iterator itr  = m_inRangeOppFactions.begin();
//		FactionRangeList::iterator iend = m_inRangeOppFactions.end();
		std::set<Object*>::iterator itr = GetInRangeSetBegin(),itr2;
		std::set<Object*>::iterator iend = GetInRangeSetEnd();
		Unit * target;
		Aura * pAura;
		float radius = m_floatValues[DYNAMICOBJECT_RADIUS]*m_floatValues[DYNAMICOBJECT_RADIUS];

		while(itr != iend)
		{
//			target = *itr;
//			++itr;

			itr2 = itr;
			++itr;

			if( !( (*itr2)->IsUnit() ) || ! static_cast< Unit* >( *itr2 )->isAlive() || ( static_cast< Creature* >( *itr2 )->IsTotem() && !static_cast< Unit* >( *itr2 )->IsPlayer() ) )
				continue;

			target = static_cast< Unit* >( *itr2 );

			if( !isAttackable( u_caster, target, !(m_spellProto->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED) ) )
				continue;

			// skip units already hit, their range will be tested later
			if(targets.find(target) != targets.end())
				continue;

			if(GetDistanceSq(target) <= radius)
			{
				pAura = new Aura(m_spellProto, m_aliveDuration, u_caster, target);
				for(uint32 i = 0; i < 3; ++i)
				{
					if(m_spellProto->Effect[i] == SPELL_EFFECT_PERSISTENT_AREA_AURA)
					{
						pAura->AddMod(m_spellProto->EffectApplyAuraName[i],
							m_spellProto->EffectBasePoints[i]+1, m_spellProto->EffectMiscValue[i], i);
					}
				}
				target->AddAura(pAura);
				if(p_caster)
				{
					p_caster->HandleProc(PROC_ON_CAST_SPECIFIC_SPELL | PROC_ON_CAST_SPELL,target, m_spellProto);
					p_caster->m_procCounter = 0;
				}

				// add to target list
				targets.insert(target);
			}
		}

		// loop the targets, check the range of all of them
		DynamicObjectList::iterator jtr  = targets.begin();
		DynamicObjectList::iterator jtr2;
		DynamicObjectList::iterator jend = targets.end();
		
		while(jtr != jend)
		{
			target = *jtr;
			jtr2 = jtr;
			++jtr;

			if(GetDistanceSq(target) > radius)
			{
				targets.erase(jtr2);
				target->RemoveAura(m_spellProto->Id);
			}
		}

		m_aliveDuration -= 100;
	}
	else
	{
		m_aliveDuration = 0;
	}

	if(m_aliveDuration == 0)
	{
		DynamicObjectList::iterator jtr  = targets.begin();
		DynamicObjectList::iterator jend = targets.end();
		Unit * target;

		while(jtr != jend)
		{
			target = *jtr;
			++jtr;
			target->RemoveAura(m_spellProto->Id);
		}

		Remove();
	}
}

void DynamicObject::Remove()
{
	SunyouRaid* raid = GetSunyouRaid();
	if( raid )
		raid->OnDynamicObjectDestroyed( this );

	if(IsInWorld())
		RemoveFromWorld(true);
		
	delete this;
}

void DynamicObject::Update( uint32 difftime )
{
	if( m_timeToMove == 0 )
		return;

	m_timeMoved += difftime;
	if( m_timeMoved >= m_timeToMove )
	{
		SetPosition( m_destinationX, m_destinationY, m_destinationZ, GetOrientation() );
		SendMonsterMove( m_destinationX, m_destinationY, m_destinationZ, GetOrientation(), MOVE_TYPE_STOP, 0, 0 );
		m_destinationX = 0.f;
		m_destinationY = 0.f;
		m_destinationZ = 0.f;
		m_timeToMove = 0;
		m_timeMoved = 0;
		m_timeToUpdatePosition = 0;

		SunyouRaid* raid = this->GetSunyouRaid();
		if( raid )
			raid->OnDynamicObjectMoveArrival( this );
		return;
	}
	if( m_timeMoved >= m_timeToUpdatePosition )
	{
		float q = (float)m_timeMoved / (float)m_timeToMove;
		float x = GetPositionX() + (m_destinationX - GetPositionX()) * q;
		float y = GetPositionY() + (m_destinationY - GetPositionY()) * q;
		float z = GetPositionZ() + (m_destinationZ - GetPositionZ()) * q;
		if( IsInWorld() )
			z = CollideInterface.GetHeight( GetMapMgr()->GetBaseMap(), x, y, z + 5.f );
		SetPosition( x, y, z, GetOrientation() );
		m_timeToUpdatePosition = m_timeMoved + UNIT_MOVEMENT_INTERPOLATE_INTERVAL;
	}
}

void DynamicObject::MoveTo( float x, float y, float z )
{
	float ux = x - GetPositionX();
	float uy = y - GetPositionY();
	float angle = atan2(ux,uy);
	SetOrientation(angle);
	uint32 distance = CalcDistance(x, y, z);
	m_timeToMove = (uint32) (distance / m_walkSpeed * 0.001);
	m_timeMoved = 0;
	m_timeToUpdatePosition = UNIT_MOVEMENT_INTERPOLATE_INTERVAL;
	SendMonsterMove( m_destinationX, m_destinationY, m_destinationZ, angle, MOVE_TYPE_NORMAL, 0, m_timeToMove );
}
