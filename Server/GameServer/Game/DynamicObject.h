#ifndef DYNAMICOBJECT_H
#define DYNAMICOBJECT_H
#include "Object.h"
typedef set<Unit*>  DynamicObjectList;
typedef set<Unit*>  FactionRangeList;

class SERVER_DECL DynamicObject : public Object
{
public:
	DynamicObject( uint32 high, uint32 low );
	~DynamicObject( );

	void Create(Unit * caster, Spell * pSpell, float x, float y, float z, uint32 duration, float radius);
	void UpdateTargets();
	void UpdateTargets_Damage_NoAura_Area_Radiu(uint32 damage);
	void UpdateTargets_HealHP_NoAura_Area_Radiu(uint32 damage);
	void UpdateTargets_HealMP_NoAura_Area_Radiu(uint32 damage);
	void UpdateTargets_Buf_Area_Radiu(uint32 i, uint32 damage);
	void UpdateTargets_Apply_Effect( uint32 i, bool friendly );

	void AddInRangeObject(Object* pObj);
	void OnRemoveInRangeObject(Object* pObj);
	void Remove();
	void Update( uint32 difftime );
	void MoveTo( float x, float y, float z );

	GameObject* g_caster;

protected:
	SpellEntry * m_spellProto;
	Unit * u_caster;
	Player * p_caster;
	Spell* m_parentSpell;
	DynamicObjectList targets;
	FactionRangeList  m_inRangeOppFactions;
	uint64 m_casterGUID;
	uint32 _i;
	uint32 m_aliveDuration;
	uint32 _fields[DYNAMICOBJECT_END];

	float m_destinationX;
	float m_destinationY;
	float m_destinationZ;
	uint32 m_timeMoved;
	uint32 m_timeToMove;
	uint32 m_timeToUpdatePosition;
};

#endif

