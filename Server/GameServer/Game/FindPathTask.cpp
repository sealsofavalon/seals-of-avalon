#include "StdAfx.h"
#include "FindPathTask.h"
#include "AIInterfaceManager.h"
#include "Map.h"

static const int aroundx[8] = { 1, 0, 0, -1, 1, 1, -1, -1 };
static const int aroundy[8] = { 0, 1, -1, 0, 1, -1, 1, -1 };
static const float grid_size = 2.0f;
static const float half_grid_size = grid_size / 2.f;
static const float grid_dist = sqrtf(grid_size * grid_size + grid_size * grid_size);
static const float half_grid_dist = sqrtf(half_grid_size * half_grid_size + half_grid_size * half_grid_size);
static const float grid_distsq = grid_size * grid_size + grid_size * grid_size;
static const int grid_count = 35;

struct grid;

struct path_node
{
	LocationVector point;
	int x;
	int y;
	bool bad;
	path_node* next;
	path_node* prev;

	bool check_los( grid* g, path_node* p );

	bool search( grid* g, path_node* next_step[], int& index, bool from_start );

	bool get_height( grid* g, path_node* source );
};

struct grid
{
	path_node* nodes;
	uint32 width;
	uint32 height;
	LocationVector destination;
	Map* pMap;
	uint32 idForCollision;
	path_node* tail;
	path_node* start;

	grid( void* buffer, uint32 w, uint32 h, Map* m, uint32 hashID, const LocationVector& from, const LocationVector& to )
		: width( w ), height( h ), pMap( m ), idForCollision( hashID ), start( NULL ), tail( NULL )
	{
		nodes = (path_node*)buffer;
		memset( nodes, 0, sizeof( path_node ) * width * height );

		float dx = to.x - from.x;
		float dy = to.y - from.y;

		int nx = dx / grid_size / 2;
		int ny = dy / grid_size / 2;
		float centerx = to.x - (float)nx * grid_size;
		float centery = to.y - (float)ny * grid_size;

		destination.x = to.x;
		destination.y = to.y;

		path_node* aroundstart[4] = { NULL };
		int n = 0;

		for( uint32 y = 0; y < height; ++y )
		{
			for( uint32 x = 0; x < width; ++x )
			{
				path_node* p = &nodes[y * width + x];

				p->point.x = centerx - ((float)width/2 - (float)x) * grid_size;
				p->point.y = centery - ((float)height/2 - (float)y) * grid_size;
				p->point.z = NO_WMO_HEIGHT;
				p->x = x;
				p->y = y;
				
				if( n < 4 )
				{
					float dist = p->point.Distance2DSq( from );
					if( dist <= grid_distsq )
					{
						aroundstart[n++] = p;
					}
				}
				if( !tail && fabs(p->point.x - destination.x) < 0.1f && fabs(p->point.y - destination.y) < 0.1f )
				{
					p->point.z = CollideInterface.GetHeight( m, p->point.x, p->point.y, destination.z + 1.f );
					tail = p;
				}
			}
		}

		float mindist = 999.f;
		for( int i = 0; i < n; ++i )
		{
			aroundstart[i]->point.z = CollideInterface.GetHeight( m, aroundstart[i]->point.x, aroundstart[i]->point.y, from.z + 4.f );

			float dist = aroundstart[i]->point.Distance( from );

			if( dist < mindist )
			{
				start = aroundstart[i];
				mindist = dist;
			}
		}

		if( start && tail )
		{
			path_node* step_start[2][512];
			path_node* step_tail[2][512];
			
			int index_start = 1;
			int index_tail = 1;

			step_start[0][0] = start;
			step_tail[0][0] = tail;

			int x_start = 0;
			int x_tail = 0;
			do
			{
				int count = index_start;

				index_start = 0;
				for( int i = 0; i < count; ++i )
				{
					path_node* p = step_start[x_start][i];
					if( p->search( this, step_start[!x_start], index_start, true ) )
						return;
				}
				x_start = !x_start;

				count = index_tail;

				index_tail = 0;
				for( int i = 0; i < count; ++i )
				{
					path_node* p = step_tail[x_tail][i];
					if( p->search( this, step_tail[!x_tail], index_tail, false ) )
						return;
				}
				x_tail = !x_tail;
			}
			while( index_start || index_tail );

			start = NULL;
		}
	}

	inline path_node* get( uint32 x, uint32 y )
	{
		if( x < width && y < height )
			return &nodes[y * width + x];
		else
			return NULL;
	}
};

bool path_node::search( grid* g, path_node* next_step[], int& index, bool from_start )
{
	for( int i = 0; i < 8; ++i )
	{
		path_node* p = g->get( x + aroundx[i], y + aroundy[i] );
		if( p )
		{
			if( (from_start && p->next) || (!from_start && p->prev) )
			{
				if( check_los( g, p ) )
				{
					if( from_start )
					{
						p->prev = this;
						this->next = p;
					}
					else
						p->next = this;

					for( path_node* pn = p; pn != g->start; pn = pn->prev )
						pn->prev->next = pn;
					return true;
				}
				else
					continue;
			}

			if( !p->prev && !p->next && p != g->start && p != g->tail )
			{
				if( check_los( g, p ) )
				{
					if( from_start )
						p->prev = this;
					else
						p->next = this;

					next_step[index++] = p;
				}
			}
		}
	}
	return false;
}

bool path_node::check_los( grid* g, path_node* p )
{
	if( bad || p->bad )
		return false;

	if( !p->get_height( g, this ) )
		return false;

	if( fabs( p->point.z - point.z ) > grid_size )
		return false;
	else
		return true;
}

bool path_node::get_height( grid* g, path_node* source )
{
	float outx, outy, outz;
	if( CollideInterface.GetFirstPoint( g->idForCollision, source->point.x, source->point.y, source->point.z + grid_size, point.x, point.y, source->point.z + grid_size, outx, outy, outz, 0.1f ) )
	{
		return false;
	}
	else
	{
		point.z = CollideInterface.GetHeight( g->pMap, point.x, point.y, source->point.z + 2.f );
		if( point.z == NO_WMO_HEIGHT )
		{
			bad = true;
			return false;
		}
	}
	return true;
}

FindPathTask::FindPathTask( const LocationVector& afrom, const LocationVector& ato, Map* m, int key )
 : from( afrom ), to( ato ), mapID( m->GetMapIDForCollision() ), pMap( m ), AIInterfaceKey( key ), ResultX( NO_WMO_HEIGHT ), ResultY( NO_WMO_HEIGHT ), ResultZ( NO_WMO_HEIGHT )
{

}

static char s_buffer[8 * grid_count * grid_count * sizeof( path_node )];

void FindPathTask::execute()
{
	uint32 start_time = getMSTime();

	grid g( s_buffer + grid_count * grid_count * sizeof( path_node ) * get_thread_index(), grid_count, grid_count, pMap, mapID, from, to );
	if( g.start )
	{
		path_node* next_node = g.start->next;
		if( next_node )
		{
			int i = 0;
			for( path_node* p = next_node; p != g.tail; p = p->next )
			{
				if( CollideInterface.CheckLOS( mapID, g.start->point.x, g.start->point.y, g.start->point.z + 0.6f, p->point.x, p->point.y, p->point.z + 0.5f ) )
				{
					next_node = p;
					++i;
				}
				else
				{
					if( g.start->point.Distance2D( next_node->point ) <= grid_dist )
						next_node = p->next;
					break;
				}
			}

			if( next_node )
			{
				if( next_node->point.Distance2D( g.start->point ) > 2.f )
				{
					ResultX = next_node->point.x;
					ResultY = next_node->point.y;
					ResultZ = next_node->point.z;
				}
			}
		}
	}

	if( getMSTime() - start_time >= 100 )
		MyLog::log->warn( "find path spend time:%u ms", getMSTime() - start_time );
}

void FindPathTask::end()
{
	sAIInterfaceManager.PushFindPathResult( AIInterfaceKey, ResultX, ResultY, ResultZ );
}

int FindPathTask::get_thread_index()
{
	return AIInterfaceKey % 8;
}
