#ifndef _FIND_PATH_TASK_HEAD
#define _FIND_PATH_TASK_HEAD

#include "../../../new_common/Source/utilities/task_thread_pool.h"
#include "LocationVector.h"

class Map;
class AIInterface;

/*
struct PathResult
{
	std::list<LocationVector*> lst;
	LocationVector* Pop()
	{
		if( !lst.empty() )
		{
			LocationVector* plv = lst.front();
			lst.pop_front();
			return plv;
		}
		else
			return NULL;
	}

	~PathResult()
	{
		for( std::list<LocationVector*>::iterator it = lst.begin(); it != lst.end(); ++it )
			delete *it;
	}
};
*/

class FindPathTask : public task
{
public:
	FindPathTask( const LocationVector& afrom, const LocationVector& ato, Map* m, int key );
	virtual void execute();
	virtual void end();
	virtual int get_thread_index();

public:
	LocationVector from;
	LocationVector to;
	uint32 mapID;

	int AIInterfaceKey;
	Map* pMap;
	float ResultX;
	float ResultY;
	float ResultZ;
};

#endif // _FIND_PATH_TASK_HEAD