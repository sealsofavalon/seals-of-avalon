#include "stdafx.h"
#include "GSC_InstanceQueue.h"
#include "../Net/GS_SP_Client.h"
#include "../Net/GS_SP_ListenServer.h"

GSC_InstanceQueue::GSC_InstanceQueue(const sunyou_instance_configuration &conf, InstanceQueueMgr *father)
: m_conf( conf ), m_father( father )
{
}
GSC_InstanceQueue::~GSC_InstanceQueue()
{

}
bool GSC_InstanceQueue::checkcanjoin(Player* p, Player* leader)
{
	/*if (m_conf.cost_type == 1 && m_conf.cost_value1 > 0 )
	{
		if (p->GetCardPoints() < m_conf.cost_value1)
		{
			leader->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_19 );
			return false;
		}
	}*/
	ui32 lv = p->getLevel();
	if (lv > m_conf.maxlevel || lv < m_conf.minlevel)
	{
		leader->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_20 );
		return false;
	}

	if (p->IsInstanceBusy())
	{
		leader->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_21 );
		return false;
	}

	return true;
}
void GSC_InstanceQueue::Leave(Player* p)
{
	MSG_GS2GSP::stLeaveQueueInstanceReq msg;
	msg.guid = p->GetGUID();
	if (g_gs_sp_client)
		g_gs_sp_client->PostSend(msg);

	p->RemoveQueueInstance( m_conf.mapid );
	p->SetInstanceBusy( false );
	if( p->GetSession() )
		p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_24 );
}
void GSC_InstanceQueue::Add(Player *p)
{
	Group* g = p->GetGroup();
	if( g && g->GetLeader() != p->m_playerInfo )
		return;

	MSG_GS2GSP::stQueueInstanceReq msg;
	msg.mapid = m_conf.mapid;
	MSG_GS2GSP::stQueueInstanceReq::_group mgroup;
	if (g)
	{
		if ( g->MemberCount() > m_conf.maxplayer)
		{
			p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_18 );
			return;
		}
		ui32 groups = g->GetSubGroupCount();
		for (int i = 0; i < groups; i++)
		{
			SubGroup*sg = g->GetSubGroup(i);
			if (!sg)
				continue;
			for (GroupMembersSet::iterator it = sg->GetGroupMembersBegin(); it != sg->GetGroupMembersEnd(); ++it)
			{
				PlayerInfo* pi = *it;
				if (pi->m_loggedInPlayer)
				{
					if (!checkcanjoin(pi->m_loggedInPlayer, p))
						return ;
				}else
				{
					p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_22 );
					return;
				}
			}
		}
		for (int i = 0; i < groups; i++)
		{
			SubGroup*sg = g->GetSubGroup(i);
			if (!sg)
				continue;
			for (GroupMembersSet::iterator it = sg->GetGroupMembersBegin(); it != sg->GetGroupMembersEnd(); ++it)
			{
				PlayerInfo* pi = *it;
				if (pi->m_loggedInPlayer)
				{
					pi->m_loggedInPlayer->AddQueueInstance( m_conf.mapid );
					pi->m_loggedInPlayer->SetInstanceBusy( true );
					mgroup.guid = pi->m_loggedInPlayer->GetGUID();
					mgroup.acct = pi->acct;
					mgroup.race = pi->m_loggedInPlayer->getRace();
					msg.v.push_back(mgroup);
					if( pi->m_loggedInPlayer->GetSession() )
						pi->m_loggedInPlayer->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_23 );
				}
			}
		}

	}else
	{
		if (!checkcanjoin(p, p))
		{
			return ;
		}
		if (p->GetSession())
			p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_23 );
		p->AddQueueInstance(m_conf.mapid);
		p->SetInstanceBusy(true);
		mgroup.guid = p->GetGUID();
		mgroup.acct = p->m_playerInfo->acct;
		mgroup.race = p->getRace();
		msg.v.push_back(mgroup);
	}

	if (g_gs_sp_client)
		g_gs_sp_client->PostSend(msg);
}