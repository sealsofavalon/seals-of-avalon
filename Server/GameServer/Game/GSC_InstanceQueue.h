#ifndef __gsc_instance_queue_h__
#define __gsc_instance_queue_h__

class InstanceQueueMgr;
class GSC_InstanceQueue	: public sunyou_instance_configuration
{
public:
	GSC_InstanceQueue(const sunyou_instance_configuration& conf, InstanceQueueMgr* father);
	~GSC_InstanceQueue();

	virtual void Add(Player* p);
	virtual void Leave(Player* p);
protected:
	bool checkcanjoin(Player* p, Player* leader);
	InstanceQueueMgr* m_father;
	const sunyou_instance_configuration& m_conf;
private:
};
	
#endif