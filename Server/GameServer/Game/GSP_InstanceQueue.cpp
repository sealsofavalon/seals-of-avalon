#include "stdafx.h"
#include "GSP_InstanceQueue.h"
#include "SunyouTeamArena.h"
#include "SunyouBattleGround.h"
#include "SunyouAdvancedBattleGround.h"

GSP_InstanceQueueBase::GSP_InstanceQueueBase( const sunyou_instance_configuration& conf, InstanceQueueMgr* father)
:m_conf( conf ), m_father( father )
{

}
GSP_InstanceQueueBase::~GSP_InstanceQueueBase()
{

}

//////////////////////////////////////////////////////////////////////////
GSP_InstanceQueue::GSP_InstanceQueue(const sunyou_instance_configuration &conf, InstanceQueueMgr *father)
: GSP_InstanceQueueBase(conf, father)
{
	m_fireteam[0] = NULL;
	m_fireteam[1] = NULL;
}
GSP_InstanceQueue::~GSP_InstanceQueue()
{

}
void GSP_InstanceQueue::AddJoin(vector<MSG_GS2GSP::stQueueInstanceReq::_group>& vgroup, ui32 gsid)
{
	int count = vgroup.size();
	if (count)
	{
		team_t* nteam = new team_t;
		for (int i = 0; i < count; i++)
		{
			if (i < TEAMMAXPLAYER)
			{
				nteam->players[i] = vgroup[i].guid;	
				nteam->accts[i] = vgroup[i].acct;
				nteam->race = vgroup[i].race;
			}
		}
		nteam->member_count = count;
		nteam->m_gsid = gsid;
		
		m_teams.push_back(nteam);
		CheckStart();
	}
}

void GSP_InstanceQueue::CheckStart()
{
	if (m_teams.size() > 1)
	{
		std::list<team_t*>::iterator it = m_teams.begin();
		for (it; it != m_teams.end(); ++it)
		{
			team_t* teamA = *it;
			std::list<team_t*>::iterator it2 = it;
			for (++it2; it2 != m_teams.end(); ++it2)
			{
				team_t* teamB = *it2;
				if (teamA->member_count == teamB->member_count)
				{
					m_fireteam[0] = teamA;
					m_fireteam[1] = teamB;

 					m_teams.erase( it );
 					m_teams.erase( it2 );

					Start();
					return;
				}
			}
		}
	}
}
void GSP_InstanceQueue::RemoveJoin(ui32 guid, ui32 gsid)
{
	std::list<team_t*>::iterator it = m_teams.begin();
	for (it; it != m_teams.end(); ++it)
	{
		team_t* team = *it;
		if (team->m_gsid == gsid)
		{
			for (int i = 0 ; i < TEAMMAXPLAYER; i++)
			{
				if (guid == team->players[i])
				{
					team->players[i] = 0;
					team->accts[i] = 0;
					team->member_count -= 1;
					// 如果人数为0. 删除该组
					if (team->member_count == 0)
					{
						delete team;
						team = NULL;
						m_teams.erase(it);
					}
					return ;
				}
			}
		}
	}
}
void GSP_InstanceQueue::JumpFromGSC( SunyouInstance* instance )
{
	uint32 playercount = 0;
	for( int i = 0; i < TEAMMAXPLAYER; ++i )
	{
		if( m_fireteam[0]->players[i] )
		{
			arena_jump_t* jump = new arena_jump_t;
			jump->guid = m_fireteam[0]->players[i];
			jump->acct = m_fireteam[0]->accts[i];
			jump->instanceid = instance->GetMapMgr()->GetInstanceID();
			jump->mapid = instance->GetMapMgr()->GetMapId();
			jump->mother_groupid = GetGroupIDByGUID( jump->guid );
			jump->team_index = 1;
			g_crosssrvmgr->PushJump( jump );
			++playercount;
		}
		if( m_fireteam[1]->players[i] )
		{
			arena_jump_t* jump = new arena_jump_t;
			jump->guid = m_fireteam[1]->players[i];
			jump->acct = m_fireteam[1]->accts[i];
			jump->instanceid = instance->GetMapMgr()->GetInstanceID();
			jump->mapid = instance->GetMapMgr()->GetMapId();
			jump->mother_groupid = GetGroupIDByGUID( jump->guid );
			jump->team_index = 2;
			g_crosssrvmgr->PushJump( jump );
			++playercount;
		}
	}
	if( playercount < 2 )
		static_cast<SunyouTeamArena*>( instance )->m_vs_type = 0;
	else
		static_cast<SunyouTeamArena*>( instance )->m_vs_type = (playercount / 2) - 1;

	delete m_fireteam[0];
	m_fireteam[0] = NULL;
	delete m_fireteam[1];
	m_fireteam[1] = NULL;
}
void GSP_InstanceQueue::Start()
{
	MapMgr* pMgr = sInstanceMgr.CreateBattlegroundInstance( m_conf.mapid );
	if( pMgr )
	{
		switch( m_conf.category )
		{
		case INSTANCE_CATEGORY_TEAM_ARENA:
			pMgr->m_sunyouinstance = new SunyouTeamArena;
			pMgr->m_sunyouinstance->Init( m_conf, pMgr );
			break;
		default:
			assert( 0 );
			return;
		}
		
		JumpFromGSC( pMgr->m_sunyouinstance );
	}
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
GSP_BattleGroundGroup::GSP_BattleGroundGroup(const sunyou_instance_configuration &conf, InstanceQueueMgr *father)
: GSP_InstanceQueueBase(conf, father)
{
	
}
void GSP_BattleGroundGroup::AddJoin(vector<MSG_GS2GSP::stQueueInstanceReq::_group>& vgroup, ui32 gsid)
{
	int count = vgroup.size();
	if (count)
	{
		team_t* nteam[3];
		nteam[0] = NULL;
		nteam[1] = NULL;
		nteam[2] = NULL;
		for (int i = 0; i < count; i++)
		{
			team_t* nteamTem = NULL;
			if (vgroup[i].race == RACE_YAO)
			{	
				if (!nteam[0])
				{	
					nteam[0] = new team_t;
					nteam[0]->race = RACE_YAO;
					nteam[0]->m_gsid = gsid;
				}
				nteamTem = nteam[0];
			}
			if (vgroup[i].race == RACE_REN)
			{	
				if (!nteam[1])
				{	
					nteam[1] = new team_t;
					nteam[1]->race = RACE_REN;
					nteam[1]->m_gsid = gsid;
				}
				nteamTem = nteam[1];
			}

			if (vgroup[i].race == RACE_WU)
			{	
				if (!nteam[2])
				{	
					nteam[2] = new team_t;
					nteam[2]->race = RACE_WU;
					nteam[2]->m_gsid = gsid;
				}
				nteamTem = nteam[2];
			}
			if (nteamTem)
			{
				if (nteamTem->member_count <= TEAMMAXPLAYER )
				{
					nteamTem->players[nteamTem->member_count] = vgroup[i].guid;
					nteamTem->accts[nteamTem->member_count] = vgroup[i].acct;
					nteamTem->member_count++;
				}
			}		
		}
		if( nteam[0] )
			m_teams[0].push_back(nteam[0]);
		if( nteam[1] )
			m_teams[1].push_back(nteam[1]);
		if( nteam[2] )
			m_teams[2].push_back(nteam[2]);
		CheckStart();
	}
}
void GSP_BattleGroundGroup::RemoveJoin(ui32 guid, ui32 gsid)
{
	ui32 index = 0;
	team_t* findteam = NULL;
	for (int i =0; i < 3; i++ )
	{
		std::list<team_t*>::iterator it = m_teams[i].begin();
		for (it; it != m_teams[i].end(); ++it)
		{
			team_t* team = *it;
			for (int j =0; j < TEAMMAXPLAYER; j++)
			{
				if (team->players[j] == guid)
				{
					findteam = team;
					team->players[j] = 0;
					team->accts[j] = 0;
					team->member_count--;
					index = i;
					break;
				}
			}
			if (findteam)
				break;
		}
		if (findteam)
			break;
	}

	if (!findteam)
	{
		return ;
	}

	bool bfind = false;
	for (std::list<team_t*>::iterator fire_it = m_fireteam[index].begin(); fire_it != m_fireteam[index].end(); ++ fire_it)
	{
		team_t* fire_team = *fire_it;
		if (fire_team == findteam)
		{
			bfind = true;
			break;
		}
	}
	if (bfind)
		m_fireteam[index].clear();

	if (findteam->member_count == 0)
		EareseTeam(index, findteam);
	else
		CheckStart();
}

void GSP_BattleGroundGroup::Start()
{
	MapMgr* pMgr = sInstanceMgr.CreateBattlegroundInstance( m_conf.mapid );
	if( pMgr )
	{
		switch( m_conf.category )
		{
		case INSTANCE_CATEGORY_BATTLE_GROUND:
			pMgr->m_sunyouinstance = new SunyouBattleGround;
			pMgr->m_sunyouinstance->Init( m_conf, pMgr );
			break;
		case INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND:
			pMgr->m_sunyouinstance = new SunyouAdvancedBattleGround;
			pMgr->m_sunyouinstance->Init( m_conf, pMgr );
			break;
		default:
			assert( 0 );
			return;
		}

		JumpFromGSC( pMgr->m_sunyouinstance );
	}
}
bool GSP_BattleGroundGroup::CheckFireTeam(std::list<team_t*>& mother, int count, std::list<team_t*>& reslut)
{
	std::list<team_t*> n_mother;
	std::list<team_t*> n_result;
	
	if (!mother.size())
	{
		return false;
	}
	team_t* team_head = *mother.begin();
	if (team_head->member_count == count)
	{
		reslut.push_back(team_head);
		return true;
	}else
	{
		std::list<team_t*>::iterator it = mother.begin();
		std::list<team_t*>::iterator end_it = mother.end();
		std::list<team_t*>::iterator it2 ;
		for (it; it != end_it; ++it)
		{
			n_mother.clear();
			n_result.clear();
			
			team_t* team = *it;
			if (team->member_count > count)
			{
				continue ;
			}else
			{
				if (team->member_count == count)
				{
					reslut.push_back(team);
					return true;
				}
			}
			for (it2 = it ; it2 != end_it; ++it2)
			{
				if (it2 != it && (*it2)->member_count <= count - team->member_count)
					n_mother.push_back(*it2);
			}

			if (CheckFireTeam(n_mother, count - team->member_count, n_result))
			{
				reslut.push_back(team);
				for (std::list<team_t*>::iterator it_result = n_result.begin(); it_result != n_result.end(); ++it_result)
					reslut.push_back( * it_result);
				return true;
			}
		}
	}
	return false;
}
bool GSP_BattleGroundGroup::CheckCanFind(const std::set<ui32>& checklist, ui32 number)
{
	return checklist.find( number ) == checklist.end();
}
void GSP_BattleGroundGroup::GetFireTeam(int index)
{
	if (index >= 0 && index < 3 )
	{
		if (m_teams[index].size() == 0)
			return;

		int max = m_conf.bg_faction_max_player;
		std::list<team_t*>::iterator it = m_teams[index].begin();
		std::list<team_t*>::iterator it2;
		std::list<team_t*>::iterator end_it = m_teams[index].end();

		std::list<team_t*> mother;
		std::list<team_t*> result;
		std::set<ui32> check;

		for (it; it != end_it; ++it)
		{
			team_t* team = *it;
			if (check.size() && !CheckCanFind(check, team->member_count))
				continue ;

			// 如果匹配
			if (team->member_count == max)
			{
				m_fireteam[index].push_back(team);
				return ;
			}else
			{
				if (team->member_count > max)
				{
					continue;
				}
			}

			//准备遍历
			mother.clear();
			result.clear();
			
			for (it2 = it ; it2 != end_it; ++it2)
			{
				if (it2 != it && (*it2)->member_count <= max - team->member_count)
					mother.push_back(*it2);
			}
			// 检测列表
			if (CheckFireTeam(mother, max - team->member_count, result))
			{
				m_fireteam[index].push_back(*it);
				for (std::list<team_t*>::iterator it_result = result.begin(); it_result != result.end(); ++ it_result)
					m_fireteam[index].push_back(*it_result);
				
				return;
			}else
				check.insert(team->member_count);
		}

		m_fireteam[index].clear();
	}
}
void GSP_BattleGroundGroup::CheckStart()
{
	ui32 checkteam = 0;
	for (int i =0; i < 3; i++)
	{
		if (!m_fireteam[i].size())
			GetFireTeam(i);

		if (m_fireteam[i].size())
		{
			checkteam++;
			if (checkteam >= m_conf.bg_faction_count)
			{
				Start();
				return;
			}
		}
	}

}
void GSP_BattleGroundGroup::EareseTeam(int index, team_t* p)
{
	if (index >= 0 && index < 3)
	{
		std::list<team_t*>::iterator it = m_teams[index].begin();
		for (it; it != m_teams[index].end(); ++it)
		{
			if (*it == p)
			{
				m_teams[index].erase(it);
				delete p ; 
				p = NULL;
				break;
			}
		}
	}
}
void GSP_BattleGroundGroup::JumpFromGSC( SunyouInstance* instance )
{
	ui32 faction = 3;
	ui32 team_index = 0;
	for (int i = 0; i < 3; i++)
	{
		if (m_fireteam[i].size())
		{
			team_index++;
			std::list<team_t*>::iterator it = m_fireteam[i].begin();
			for(it; it != m_fireteam[i].end(); ++it)
			{
				team_t* team = *it;
				for( int j = 0; j < TEAMMAXPLAYER; ++j )
				{
					if( team->players[j] )
					{
						arena_jump_t* jump = new arena_jump_t;
						jump->guid = team->players[j];
						jump->acct = team->accts[j];
						jump->instanceid = instance->GetMapMgr()->GetInstanceID();
						jump->mapid = instance->GetMapMgr()->GetMapId();
						jump->mother_groupid = GetGroupIDByGUID( jump->guid );
						jump->team_index = team_index;
						g_crosssrvmgr->PushJump( jump );
					}
				}

				EareseTeam(i,team);
			}
			m_fireteam[i].clear();
		}
		else
		{
			faction = i ;
		}
	}

	if( instance->GetCategory() == INSTANCE_CATEGORY_BATTLE_GROUND )
	{
		if (faction == 0)
		{
			static_cast<SunyouBattleGround*>( instance )->Set2Faction( 1, 2 );
		}else if (faction == 1)
		{
			static_cast<SunyouBattleGround*>( instance )->Set2Faction( 0, 2 );
		}else if (faction == 2)
		{
			static_cast<SunyouBattleGround*>( instance )->Set2Faction( 0, 1 );
		}
	}	
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
GSP_TeamBattleGroundGroup::GSP_TeamBattleGroundGroup(const sunyou_instance_configuration& conf, InstanceQueueMgr* father)
: GSP_InstanceQueueBase(conf, father)
{

}

void GSP_TeamBattleGroundGroup::AddJoin(vector<MSG_GS2GSP::stQueueInstanceReq::_group>& vgroup, ui32 gsid)
{
	int count = vgroup.size();
	if (count)
	{
		team_t* nteam = new team_t;
		nteam->m_gsid = gsid;
		for (int i = 0; i < count; i++)
		{			
			if (nteam)
			{
				if (nteam->member_count <= TEAMMAXPLAYER )
				{
					nteam->players[nteam->member_count] = vgroup[i].guid;
					nteam->accts[nteam->member_count] = vgroup[i].acct;
					nteam->member_count++;
				}
			}		
		}
		m_teams.push_back(nteam);
		CheckStart();
	}
}
void GSP_TeamBattleGroundGroup::RemoveJoin(ui32 guid, ui32 gsid)
{
	team_t* findteam = NULL;
	bool bfind = false;
	std::list<team_t*>::iterator it = m_teams.begin();
	for (it; it != m_teams.end(); ++it)
	{
		team_t* team = *it;
		for (int j =0; j < TEAMMAXPLAYER; j++)
		{
			if (team->players[j] == guid)
			{
				findteam = team;
				team->players[j] = 0;
				team->accts[j] = 0;
				team->member_count--;
				break;
			}
		}
		if (findteam)
			break;
	}
	if (!findteam)
		return ;

	for (int i = 0; i < 3; i++)
	{
		for (std::list<team_t*>::iterator fire_it = m_fireteam[i].begin(); fire_it != m_fireteam[i].end(); ++ fire_it)
		{
			team_t* fire_team = *fire_it;
			if (fire_team == findteam)
			{
				m_fireteam[i].clear();
				bfind = true;
				break;
			}
		}
		if (bfind)
			break;
	}
	if (findteam->member_count == 0)
		EareseTeam(findteam);
	else
		CheckStart();
}
void GSP_TeamBattleGroundGroup::Start()
{
	MapMgr* pMgr = sInstanceMgr.CreateBattlegroundInstance( m_conf.mapid );
	if( pMgr )
	{
		switch( m_conf.category )
		{
		case  INSTANCE_CATEGORY_NEW_BATTLE_GROUND :
			pMgr->m_sunyouinstance = new SunyouTeamGroupBattleGround;
			pMgr->m_sunyouinstance->Init( m_conf, pMgr );
			break;
		default:
			assert( 0 );
			return;
		}

		JumpFromGSC( pMgr->m_sunyouinstance );
	}
}
void GSP_TeamBattleGroundGroup::GetFireTeam(ui32 index)
{
	if (index >= 0 && index < 3 )
	{
		if (m_teams.size() == 0)
			return;

		int max = m_conf.bg_faction_max_player;
		std::list<team_t*>::iterator it = m_teams.begin();
		std::list<team_t*>::iterator it2;
		std::list<team_t*>::iterator end_it = m_teams.end();

		std::list<team_t*> mother;
		std::list<team_t*> result;
		std::set<ui32> check;

		for (it; it != end_it; ++it)
		{
			team_t* team = *it;
			if (check.size() && !CheckCanFind(check, team->member_count))
				continue ;
			if (!CheckUse(team))
				continue;

			// 如果匹配
			if (team->member_count == max)
			{
				m_fireteam[index].push_back(team);
				return ;
			}else
			{
				if (team->member_count > max)
				{
					continue;
				}
			}

			//准备遍历
			mother.clear();
			result.clear();

			for (it2 = it ; it2 != end_it; ++it2)
			{
				if (it2 != it && (*it2)->member_count <= max - team->member_count && CheckUse(*it2))
					mother.push_back(*it2);
			}
			// 检测列表
			if (CheckFireTeam(mother, max - team->member_count, result))
			{
				m_fireteam[index].push_back(*it);
				for (std::list<team_t*>::iterator it_result = result.begin(); it_result != result.end(); ++ it_result)
					m_fireteam[index].push_back(*it_result);

				return;
			}else
				check.insert(team->member_count);
		}

		m_fireteam[index].clear();
	}
}
bool GSP_TeamBattleGroundGroup::CheckCanFind(const std::set<ui32>& checklist, ui32 number)
{
	return checklist.find( number ) == checklist.end();
}
bool GSP_TeamBattleGroundGroup::CheckUse(team_t* p)
{
	for (int i = 0; i < 3; i++)
	{
		std::list<team_t*>::iterator it = m_fireteam[i].begin();
		for (it; it != m_fireteam[i].end(); ++it)
		{
			if (*it == p )
				return false;
		}
	}
	return true;
}
bool GSP_TeamBattleGroundGroup::CheckFireTeam(std::list<team_t*>& mother, int count, std::list<team_t*>& reslut)
{
	std::list<team_t*> n_mother;
	std::list<team_t*> n_result;

	if (!mother.size())
	{
		return false;
	}
	team_t* team_head = *mother.begin();
 
	if (team_head->member_count == count)
	{
		reslut.push_back(team_head);
		return true;
	}else
	{
		std::list<team_t*>::iterator it = mother.begin();
		std::list<team_t*>::iterator end_it = mother.end();
		std::list<team_t*>::iterator it2 ;
		for (it; it != end_it; ++it)
		{
			n_mother.clear();
			n_result.clear();

			team_t* team = *it;
			if (team->member_count > count)
			{
				continue ;
			}else
			{
				if (team->member_count == count)
				{
					reslut.push_back(team);
					return true;
				}
			}
			for (it2 = it ; it2 != end_it; ++it2)
			{
				if (it2 != it && (*it2)->member_count <= count - team->member_count /*&& CheckUse(*it2)*/)
					n_mother.push_back(*it2);
			}

			if (CheckFireTeam(n_mother, count - team->member_count, n_result))
			{
				reslut.push_back(team);
				for (std::list<team_t*>::iterator it_result = n_result.begin(); it_result != n_result.end(); ++it_result)
					reslut.push_back( * it_result);
				return true;
			}
		}
	}
	return false;
}
void GSP_TeamBattleGroundGroup::EareseTeam(team_t* p)
{
	std::list<team_t*>::iterator it = m_teams.begin();
	for (it; it != m_teams.end(); ++it)
	{
		if (*it == p)
		{
			m_teams.erase(it);
			delete p ; 
			p = NULL;
			break;
		}
	}
}
void GSP_TeamBattleGroundGroup::CheckStart()
{
	ui32 checkteam = 0;
	for (int i =0; i < 3; i++)
	{
		if (!m_fireteam[i].size())
			GetFireTeam(i);

		if (m_fireteam[i].size())
		{
			checkteam++;
			if (checkteam >= m_conf.bg_faction_count)
			{
				Start();
				return;
			}
		}
	}
}
void GSP_TeamBattleGroundGroup::JumpFromGSC( SunyouInstance* instance ) // 服务器跳转
{
	ui32 faction = 3;
	ui32 team_index = 0;
	for (int i = 0; i < 3; i++)
	{
		if (m_fireteam[i].size())
		{
			team_index++;
			std::list<team_t*>::iterator it = m_fireteam[i].begin();
			for(it; it != m_fireteam[i].end(); ++it)
			{
				team_t* team = *it;
				for( int j = 0; j < TEAMMAXPLAYER; ++j )
				{
					if( team->players[j] )
					{
						arena_jump_t* jump = new arena_jump_t;
						jump->guid = team->players[j];
						jump->acct = team->accts[j];
						jump->instanceid = instance->GetMapMgr()->GetInstanceID();
						jump->mapid = instance->GetMapMgr()->GetMapId();
						jump->mother_groupid = team->m_gsid;
						assert( jump->mother_groupid == GetGroupIDByGUID(jump->guid) );
						jump->team_index = team_index;
						g_crosssrvmgr->PushJump( jump );
					}
				}

				EareseTeam(team);
			}
			m_fireteam[i].clear();
		}
	}
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
GSP_ServerBattleGroundGroup::GSP_ServerBattleGroundGroup(const sunyou_instance_configuration &conf, InstanceQueueMgr *father)
: GSP_InstanceQueueBase(conf, father)
{
	
}
void GSP_ServerBattleGroundGroup::AddJoin(vector<MSG_GS2GSP::stQueueInstanceReq::_group>& vgroup, ui32 gsid)
{	
	if (gsid < 0 || gsid >= 16)
	{
		return ;
	}
	int count = vgroup.size();
	if (count)
	{
		team_t* nteam = new team_t;
		nteam->m_gsid = gsid;
		for (int i = 0; i < count; i++)
		{			
			if (nteam)
			{
				if (nteam->member_count <= TEAMMAXPLAYER )
				{
					nteam->players[nteam->member_count] = vgroup[i].guid;
					nteam->accts[nteam->member_count] = vgroup[i].acct;
					nteam->member_count++;
				}
			}		
		}
		m_teams[gsid].push_back(nteam);
		CheckStart();
	}
}

void GSP_ServerBattleGroundGroup::RemoveJoin(ui32 guid, ui32 gsid)
{
	if (gsid < 0 || gsid >= 16)
	{
		return ;
	}

	team_t* findteam = NULL;
	bool bfind = false;
	std::list<team_t*>::iterator it = m_teams[gsid].begin();
	for (it; it != m_teams[gsid].end(); ++it)
	{
		team_t* team = *it;
		for (int j =0; j < TEAMMAXPLAYER; j++)
		{
			if (team->players[j] == guid)
			{
				findteam = team;
				team->players[j] = 0;
				team->accts[j] = 0;
				team->member_count--;
				break;
			}
		}
		if (findteam)
			break;
	}
	if (!findteam)
		return ;

	for (std::list<team_t*>::iterator fire_it = m_fireteam[gsid].begin(); fire_it != m_fireteam[gsid].end(); ++ fire_it)
	{
		team_t* fire_team = *fire_it;
		if (fire_team == findteam)
		{
			m_fireteam[gsid].clear();
			bfind = true;
			break;
		}
	}
	

	if (findteam->member_count == 0)
		EareseTeam(gsid,findteam);
	else
		CheckStart();
}

void GSP_ServerBattleGroundGroup::Start()
{
	MapMgr* pMgr = sInstanceMgr.CreateBattlegroundInstance( m_conf.mapid );
	if( pMgr )
	{
		switch( m_conf.category )
		{
		//case  INSTANCE_CATEGORY_NEW_BATTLE_GROUND :
		//	pMgr->m_sunyouinstance = new SunyouNewGroupBattleGround;
		//	pMgr->m_sunyouinstance->Init( m_conf, pMgr );
		//	break;
	/*	case INSTANCE_CATEGORY_BATTLE_GROUND:
			pMgr->m_sunyouinstance = new SunyouBattleGround;
			pMgr->m_sunyouinstance->Init( m_conf, pMgr );
			break;
		case INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND:
			pMgr->m_sunyouinstance = new SunyouAdvancedBattleGround;
			pMgr->m_sunyouinstance->Init( m_conf, pMgr );
			break;*/
		default:
			assert( 0 );
			return;
		}

		JumpFromGSC( pMgr->m_sunyouinstance );
	}
}
void GSP_ServerBattleGroundGroup::JumpFromGSC( SunyouInstance* instance )
{
	ui32 m_tem[10];
	for (int i = 0; i < 10; i++)
	{
		m_tem[i] = 0;
	}
		
	int index = 0;
	for (ui32 gs = 0; gs < 16; gs++)
	{
		if (m_fireteam[gs].size())
		{
			m_tem[index++] = gs;
			if (index ==  m_conf.bg_faction_count)
				break ;
		}
	}
	for (int i = 0; i < m_conf.bg_faction_count; i++)
	{
		if (m_tem[i] == 0)
			return ;
	}
	for (int i = 0; i < m_conf.bg_faction_count; i++)
	{
		ui32 gsid = m_tem[i];
		if (m_fireteam[gsid].size())
		{
			std::list<team_t*>::iterator it = m_fireteam[gsid].begin();
			for(it; it != m_fireteam[gsid].end(); ++it)
			{
				team_t* team = *it;
				for( int j = 0; j < TEAMMAXPLAYER; ++j )
				{
					if( team->players[j] )
					{
						arena_jump_t* jump = new arena_jump_t;
						jump->guid = team->players[j];
						jump->acct = team->accts[j];
						jump->instanceid = instance->GetMapMgr()->GetInstanceID();
						jump->mapid = instance->GetMapMgr()->GetMapId();
						jump->mother_groupid = GetGroupIDByGUID( jump->guid );
						jump->team_index = i + 1;
						g_crosssrvmgr->PushJump( jump );
					}
				}
				EareseTeam(gsid,team);
			}
			m_fireteam[gsid].clear();
		}
	}
}
void GSP_ServerBattleGroundGroup::CheckStart()
{
 	ui32 FullCount = 0;
 	for (int i =0; i < 16; i++)
 	{
 		if (!m_fireteam[i].size())
 			GetFireTeam(i);
 
 		if (m_fireteam[i].size())
 		{
 			FullCount++;
 			if (FullCount >= m_conf.bg_faction_count)
 			{
 				Start();
 				return;
 			}
 		}
 	}
}
void GSP_ServerBattleGroundGroup::GetFireTeam(ui32  gsid)
{
	if (gsid >= 0 && gsid < 16 )
	{
		if (m_teams[gsid].size() == 0)
			return;

		int max = m_conf.bg_faction_max_player;
		std::list<team_t*>::iterator it = m_teams[gsid].begin();
		std::list<team_t*>::iterator it2;
		std::list<team_t*>::iterator end_it = m_teams[gsid].end();

		std::list<team_t*> mother;
		std::list<team_t*> result;
		std::set<ui32> check;

		for (it; it != end_it; ++it)
		{
			team_t* team = *it;
			if (check.size() && !CheckCanFind(check, team->member_count))
				continue ;

			// 如果匹配
			if (team->member_count == max)
			{
				m_fireteam[gsid].push_back(team);
				return ;
			}else
			{
				if (team->member_count > max)
				{
					continue;
				}
			}

			//准备遍历
			mother.clear();
			result.clear();

			for (it2 = it ; it2 != end_it; ++it2)
			{
				if (it2 != it && (*it2)->member_count <= max - team->member_count)
					mother.push_back(*it2);
			}
			// 检测列表
			if (CheckFireTeam(mother, max - team->member_count, result))
			{
				m_fireteam[gsid].push_back(*it);
				for (std::list<team_t*>::iterator it_result = result.begin(); it_result != result.end(); ++ it_result)
					m_fireteam[gsid].push_back(*it_result);

				return;
			}else
				check.insert(team->member_count);
		}

		m_fireteam[gsid].clear();
	}
}
bool GSP_ServerBattleGroundGroup::CheckCanFind(const std::set<ui32>& checklist, ui32 number)
{
	return checklist.find( number ) == checklist.end();
}
bool GSP_ServerBattleGroundGroup::CheckFireTeam(std::list<team_t*>& mother, int count, std::list<team_t*>& reslut)
{
	std::list<team_t*> n_mother;
	std::list<team_t*> n_result;

	if (!mother.size())
	{
		return false;
	}
	team_t* team_head = *mother.begin();
	if (team_head->member_count == count)
	{
		reslut.push_back(team_head);
		return true;
	}else
	{
		std::list<team_t*>::iterator it = mother.begin();
		std::list<team_t*>::iterator end_it = mother.end();
		std::list<team_t*>::iterator it2 ;
		for (it; it != end_it; ++it)
		{
			n_mother.clear();
			n_result.clear();

			team_t* team = *it;
			if (team->member_count > count)
			{
				continue ;
			}else
			{
				if (team->member_count == count)
				{
					reslut.push_back(team);
					return true;
				}
			}
			for (it2 = it ; it2 != end_it; ++it2)
			{
				if (it2 != it && (*it2)->member_count <= count - team->member_count)
					n_mother.push_back(*it2);
			}

			if (CheckFireTeam(n_mother, count - team->member_count, n_result))
			{
				reslut.push_back(team);
				for (std::list<team_t*>::iterator it_result = n_result.begin(); it_result != n_result.end(); ++it_result)
					reslut.push_back( * it_result);
				return true;
			}
		}
	}
	return false;
}
void GSP_ServerBattleGroundGroup::EareseTeam(ui32 gsid,team_t* p)
{
	if (gsid >= 0 && gsid < 16)
	{
		std::list<team_t*>::iterator it = m_teams[gsid].begin();
		for (it; it != m_teams[gsid].end(); ++it)
		{
			if (*it == p)
			{
				m_teams[gsid].erase(it);
				delete p ; 
				p = NULL;
				break;
			}
		}
	}
}