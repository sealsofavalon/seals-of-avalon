#ifndef __GSP_INSTANCEQUEUE_H__
#define __GSP_INSTANCEQUEUE_H__
class InstanceQueueMgr;

#define TEAMMAXPLAYER 30

class GSP_InstanceQueueBase : public sunyou_instance_configuration
{
public:
	GSP_InstanceQueueBase( const sunyou_instance_configuration& conf, InstanceQueueMgr* father);
	virtual ~GSP_InstanceQueueBase();
	virtual void AddJoin(vector<MSG_GS2GSP::stQueueInstanceReq::_group>& vgroup, ui32 gsid) = 0;
	virtual void RemoveJoin(ui32 guid, ui32 gsid) = 0;
	virtual void Start() = 0;
	virtual void CheckStart() = 0;
	virtual void JumpFromGSC( SunyouInstance* instance ) = 0; // 服务器跳转

protected:
	struct team_t{
		team_t()
		{
			memset( this, 0, sizeof( *this ) );
		}
		ui32 players[TEAMMAXPLAYER];
		ui32 accts[TEAMMAXPLAYER];
		ui32 race;
		uint32 member_count;
		ui32 m_gsid;
	};

	InstanceQueueMgr* m_father;
	const sunyou_instance_configuration& m_conf;
private:
};
class GSP_InstanceQueue	: public GSP_InstanceQueueBase
{
public:
	GSP_InstanceQueue( const sunyou_instance_configuration& conf, InstanceQueueMgr* father );
	virtual ~GSP_InstanceQueue();

	virtual void AddJoin(vector<MSG_GS2GSP::stQueueInstanceReq::_group>& vgroup, ui32 gsid);
	virtual void RemoveJoin(ui32 guid, ui32 gsid);
	virtual void Start();
	virtual void CheckStart();
	virtual void JumpFromGSC( SunyouInstance* instance ); // 服务器跳转
protected:
	std::list<team_t*> m_teams;
	team_t* m_fireteam[2];
	
};
//种族战场
class GSP_BattleGroundGroup : public GSP_InstanceQueueBase
{
public:
	GSP_BattleGroundGroup(const sunyou_instance_configuration& conf, InstanceQueueMgr* father);
	virtual ~GSP_BattleGroundGroup(){;}

	virtual void AddJoin(vector<MSG_GS2GSP::stQueueInstanceReq::_group>& vgroup, ui32 gsid);
	virtual void RemoveJoin(ui32 guid, ui32 gsid);
	virtual void Start();
	virtual void CheckStart();
	virtual void JumpFromGSC( SunyouInstance* instance ); // 服务器跳转
protected:
	void GetFireTeam(int index);
	bool CheckCanFind(const std::set<ui32>& checklist, ui32 number);
	bool CheckFireTeam(std::list<team_t*>& mother, int count, std::list<team_t*>& reslut);
	void EareseTeam(int index,team_t* p);
protected:
	std::list<team_t*> m_teams[3]; // yao ren wu
	std::list<team_t*> m_fireteam[3];
private:
};

//组队战场
class GSP_TeamBattleGroundGroup : public GSP_InstanceQueueBase
{
public:
	GSP_TeamBattleGroundGroup(const sunyou_instance_configuration& conf, InstanceQueueMgr* father);
	~GSP_TeamBattleGroundGroup(){;}
	
	virtual void AddJoin(vector<MSG_GS2GSP::stQueueInstanceReq::_group>& vgroup, ui32 gsid);
	virtual void RemoveJoin(ui32 guid, ui32 gsid);
	virtual void Start();
	virtual void CheckStart();
	virtual void JumpFromGSC( SunyouInstance* instance ); // 服务器跳转
	
protected:
	void GetFireTeam(ui32 index);
	bool CheckCanFind(const std::set<ui32>& checklist, ui32 number);
	bool CheckUse(team_t* p);
	bool CheckFireTeam(std::list<team_t*>& mother, int count, std::list<team_t*>& reslut);
	void EareseTeam(team_t* p);

	std::list<team_t*> m_teams; // 
	std::list<team_t*> m_fireteam[3];
};




//服务器战场 按照服务器进行跨服战场分组
class GSP_ServerBattleGroundGroup : public GSP_InstanceQueueBase
{
public:
	GSP_ServerBattleGroundGroup(const sunyou_instance_configuration& conf, InstanceQueueMgr* father);
	virtual ~GSP_ServerBattleGroundGroup(){;}

	virtual void AddJoin(vector<MSG_GS2GSP::stQueueInstanceReq::_group>& vgroup, ui32 gsid);
	virtual void RemoveJoin(ui32 guid, ui32 gsid);
	virtual void Start();
	virtual void CheckStart();
	virtual void JumpFromGSC( SunyouInstance* instance ); // 服务器跳转

protected:
	void GetFireTeam(ui32 gsid);
	bool CheckCanFind(const std::set<ui32>& checklist, ui32 number);
	bool CheckFireTeam(std::list<team_t*>& mother, int count, std::list<team_t*>& reslut);
	void EareseTeam(ui32 gsid,team_t* p);
protected:
	std::list<team_t*> m_teams[16]; // 
	std::list<team_t*> m_fireteam[16];
};
#endif