#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Group.h"
#include "../../SDBase/Protocol/C2S_Group.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "InstanceQueue.h"

Group::Group(bool Assign)
{
	m_GroupType = GROUP_TYPE_PARTY;	 // Always init as party
	m_unRaidReadyCheck = 0;
	// Create initial subgroup
    memset(m_SubGroups,0, sizeof(SubGroup*)*6);
	m_SubGroups[0] = new SubGroup(this, 0);

	m_Leader = NULL;
	m_Looter = NULL;
	m_LootMethod = PARTY_LOOT_GROUP;
	m_LootThreshold = 2;
	m_SubGroupCount = 1;
	m_MemberCount = 0;

	m_pNewPlayer = NULL;
	if( Assign )
	{
		m_Id = objmgr.GenerateGroupId();
		ObjectMgr::getSingleton().AddGroup(this);
	}

	m_dirty=false;
	m_updateblock=false;
	m_disbandOnNoMembers = true;
	memset(m_targetIcons, 0, sizeof(uint64) * 8);
	memset( m_targetUniqueID, 0, sizeof( m_targetUniqueID ) );
	m_isqueued=false;
	m_difficulty=0;
	m_assistantLeader=m_mainAssist=m_mainTank=NULL;
	m_bCanApplyForJoining = true;

#ifdef VOICE_CHAT
	m_voiceChannelRequested = false;
	m_voiceChannelId = 0;
	m_voiceMemberCount = 0;
	memset(m_voiceMembersList, 0, sizeof(Player*)*41);
#endif
}

Group::~Group()
{
	ObjectMgr::getSingleton().RemoveGroup(this);
}

SubGroup::~SubGroup()
{

}

void SubGroup::RemovePlayer(PlayerInfo * info)
{
	GroupMembersSet::iterator it = m_GroupMembers.find( info );
	if( it != m_GroupMembers.end() )
	{
		if(info->m_loggedInPlayer)
		{
			info->m_loggedInPlayer->RemoveAura(info->m_loggedInPlayer->m_animalgroupspell);

			if( info->m_loggedInPlayer->IsInstanceBusy() )
				g_instancequeuemgr.RemovePlayerByType( info->m_loggedInPlayer, INSTANCE_CATEGORY_TEAM_ARENA );
		}

		m_GroupMembers.erase(info);
		m_listMembers.remove( info );
		sAnimalMgr.OnAniamlRelation(this);
		info->subGroup=-1;
	}
}

Player* SubGroup::GetFirstPlayer()
{
	for( std::list<PlayerInfo*>::iterator it = m_listMembers.begin(); it != m_listMembers.end(); ++it )
	{
		PlayerInfo* info = *it;
		if( info && info->m_loggedInPlayer )
			return info->m_loggedInPlayer;
	}
	return NULL;
}

PlayerInfo* SubGroup::GetFirstPlayerInfo()
{
	for( std::list<PlayerInfo*>::iterator it = m_listMembers.begin(); it != m_listMembers.end(); ++it )
	{
		PlayerInfo* info = *it;
		if( info )
			return info;
	}
	return NULL;
}

bool SubGroup::AddPlayer(PlayerInfo * info)
{
	if(IsFull())
		return false;
	if( m_GroupMembers.end() != m_GroupMembers.find( info ) )
		return false;

	m_GroupMembers.insert(info);
	m_listMembers.push_back( info );
	sAnimalMgr.OnAniamlRelation(this);
	return true;
}

bool SubGroup::HasMember(uint32 guid)
{
	for( GroupMembersSet::iterator itr = m_GroupMembers.begin(); itr != m_GroupMembers.end(); ++itr )
		if( (*itr) != NULL )
			if( (*itr)->guid == guid )
				return true;

	return false;
}

SubGroup * Group::FindFreeSubGroup()
{
	for(uint32 i = 0; i < m_SubGroupCount; i++)
		if(!m_SubGroups[i]->IsFull())
			return m_SubGroups[i];

	return NULL;
}

bool Group::AddMember(PlayerInfo * info, int32 subgroupid/* =-1 */)
{
	Player * pPlayer = info->m_loggedInPlayer;

	if(!IsFull())
	{
		SubGroup* subgroup = (subgroupid>0) ? m_SubGroups[subgroupid] : FindFreeSubGroup();
		if(subgroup == NULL)
		{
			return false;
		}

		if(subgroup->AddPlayer(info))
		{
			if(pPlayer)
			{
				sEventMgr.AddEvent(pPlayer,&Player::EventGroupFullUpdate,EVENT_PLAYER_UPDATE,1500,1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
				pPlayer->SetUInt32Value( PLAYER_FIELD_GROUP_ID, GetID() );
			}
            
			m_dirty=true;
			++m_MemberCount;
			if (m_pNewPlayer && m_pNewPlayer->m_loggedInPlayer)
			{
				UpdateGroupListForPlayer(m_pNewPlayer->m_loggedInPlayer);
				MSG_S2C::stGroupAddMember MsgNewMember;

				MsgNewMember.SubTeam = subgroup->GetID();
				MsgNewMember.NewMember.name = m_pNewPlayer->name;
				MsgNewMember.NewMember.guid = m_pNewPlayer->guid;
				if( m_pNewPlayer->m_loggedInPlayer != NULL )
				{
					MsgNewMember.NewMember.equiphead[0] = m_pNewPlayer->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0);
					MsgNewMember.NewMember.equiphead[1] = m_pNewPlayer->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+15*12);
					MsgNewMember.NewMember.equiphead[2] = m_pNewPlayer->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+2*12);
					MsgNewMember.NewMember.equiphead[3] = m_pNewPlayer->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+17*12);
					MsgNewMember.NewMember.isloggedin = true;
				}
				else
				{
					MsgNewMember.NewMember.equiphead[0] = 0;
					MsgNewMember.NewMember.equiphead[1] = 0;
					MsgNewMember.NewMember.isloggedin = false;
				}

				MsgNewMember.NewMember.flags = 0;

				if( m_pNewPlayer == m_assistantLeader )
					MsgNewMember.NewMember.flags |= 1;
				if( m_pNewPlayer == m_mainTank )
					MsgNewMember.NewMember.flags |= 2;
				if( m_pNewPlayer == m_mainAssist )
					MsgNewMember.NewMember.flags |= 4;

				MsgNewMember.NewMember.Race = m_pNewPlayer->race;
				MsgNewMember.NewMember.Class = m_pNewPlayer->cl;
				MsgNewMember.NewMember.Gender = m_pNewPlayer->gender;
				MsgNewMember.NewMember.level = m_pNewPlayer->lastLevel;

				SendPacketToAllButOne(MsgNewMember, m_pNewPlayer->m_loggedInPlayer);
				m_pNewPlayer = NULL;

			}
			else
			{
				Update();	
			}

			if(info->m_Group && info->m_Group != this)
				info->m_Group->RemovePlayer(info);

			info->m_Group=this;
			info->subGroup = subgroup->GetID();

			if( pPlayer )
			{
				if( pPlayer->IsInstanceBusy() && !pPlayer->m_sunyou_instance )
					g_instancequeuemgr.RemovePlayerByType( pPlayer, INSTANCE_CATEGORY_TEAM_ARENA );
				else
				{
					for( GroupMembersSet::iterator it = subgroup->GetGroupMembersBegin(); it != subgroup->GetGroupMembersEnd(); ++it )
					{
						PlayerInfo* pi = *it;
						if( pi->m_loggedInPlayer && pi->m_loggedInPlayer->IsInstanceBusy() && !pi->m_loggedInPlayer->m_sunyou_instance )
						{
							g_instancequeuemgr.RemovePlayerByType(  pi->m_loggedInPlayer, INSTANCE_CATEGORY_TEAM_ARENA );
							break;
						}
					}
				}

				if( !pPlayer->m_sunyou_instance )
					g_instancequeuemgr.LeavePlayer( pPlayer );

				if( m_Leader == NULL )
					m_Leader = info;
			}				
			return true;
		}
		else
		{
			info->m_Group=NULL;
			info->subGroup=-1;
			return false;
		}

	}
	else
	{
		info->m_Group = NULL;
		info->subGroup = -1;
		return false;
	}
}

void Group::SetLeader(Player* pPlayer, bool silent)
{
	if( pPlayer != NULL )
	{
		m_Leader = pPlayer->m_playerInfo;
		m_dirty = true;
		if( !silent )
		{
			MSG_S2C::stGroupSetLeader Msg;
			Msg.name = pPlayer->GetName();
			SendPacketToAll( Msg );
		}
	}
	//Update();
}


void Group::UpdateGroupListForPlayer(Player *pPlayer)
{
	if( m_updateblock )
		return;

	Player* pNewLeader = NULL;

	if( m_Leader == NULL || ( m_Leader != NULL && m_Leader->m_loggedInPlayer == NULL ) )
	{
		pNewLeader = FindFirstPlayer();
		if( pNewLeader != NULL )
			m_Leader = pNewLeader->m_playerInfo;
	}

	if( m_Looter != NULL && m_Looter->m_loggedInPlayer == NULL )
	{
		if( pNewLeader == NULL )
			pNewLeader = FindFirstPlayer();
		if( pNewLeader != NULL )
			m_Looter = pNewLeader->m_playerInfo;
	}

	GroupMembersSet::iterator itr1, itr2;

	uint32 i = 0, j = 0;
//	uint8 flags;
	SubGroup *sg1 = NULL;
	SubGroup *sg2 = NULL;



	if (m_MemberCount >  1)
	{

		if (pPlayer )
		{
			MSG_S2C::stGroupList Msg;
			Msg.grouptype = m_GroupType;
			Msg.membercount = m_MemberCount;	// we don't include self
			Msg.subgroupcount = m_SubGroupCount;

			MSG_S2C::stGroupList::stMember tempmember;
			for( j = 0; j < m_SubGroupCount; j++ )
			{
				sg2 = m_SubGroups[j];

				if( sg2 != NULL)
				{
					MSG_S2C::stGroupList::stSubGroup subgroup;
					for( itr2 = sg2->GetGroupMembersBegin(); itr2 != sg2->GetGroupMembersEnd(); ++itr2 )
					{
						PlayerInfo* plyinfo = *itr2;
						/*if( (*itr1) == plyinfo )
							continue;*/

						// should never happen but just in case
						if( plyinfo == NULL )
							continue;

						tempmember.name = plyinfo->name;
						tempmember.guid = plyinfo->guid;
						if( plyinfo->m_loggedInPlayer != NULL )
						{
							tempmember.equiphead[0] = plyinfo->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0);
							tempmember.equiphead[1] = plyinfo->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+15*12);
							tempmember.equiphead[2] = plyinfo->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+2*12);
							tempmember.equiphead[3] = plyinfo->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+17*12);
							tempmember.isloggedin = true;
						}
						else
						{
							tempmember.equiphead[0] = 0;
							tempmember.equiphead[1] = 0;
							tempmember.isloggedin = false;
						}
						
						tempmember.flags = 0;

						if( plyinfo == m_assistantLeader )
							tempmember.flags |= 1;
						if( plyinfo == m_mainTank )
							tempmember.flags |= 2;
						if( plyinfo == m_mainAssist )
							tempmember.flags |= 4;

						tempmember.Race = plyinfo->race;
						tempmember.Class = plyinfo->cl;
						tempmember.Gender = plyinfo->gender;
						tempmember.level = plyinfo->lastLevel;


						subgroup.vMembers.push_back( tempmember );
					}
					subgroup.ID = uint8( sg2->GetID() );
					Msg.vSubGroups.push_back( subgroup );
				}
			}

			Msg.leaderguid = m_Leader->guid;
			Msg.lootmethod = m_LootMethod;
			Msg.looterguid = m_Looter ? m_Looter->guid : uint64(0);
			Msg.lootthreshold = m_LootMethod;
			Msg.difficulty = m_difficulty;

	
			if( !pPlayer->IsInWorld() )
				pPlayer->CopyAndSendDelayedPacket( Msg );
			else
				pPlayer->GetSession()->SendPacket( Msg );


		}



	}

	if( m_dirty )
	{
		m_dirty = false;
		SaveToDB();
	}



}
void Group::Update()
{
	if( m_updateblock )
		return;

	Player* pNewLeader = NULL;

	if( m_Leader == NULL || ( m_Leader != NULL && m_Leader->m_loggedInPlayer == NULL ) )
	{
		pNewLeader = FindFirstPlayer();
		if( pNewLeader != NULL )
			m_Leader = pNewLeader->m_playerInfo;
	}

	if( m_Looter != NULL && m_Looter->m_loggedInPlayer == NULL )
	{
		if( pNewLeader == NULL )
			pNewLeader = FindFirstPlayer();
		if( pNewLeader != NULL )
			m_Looter = pNewLeader->m_playerInfo;
	}

	GroupMembersSet::iterator itr1, itr2;

	uint32 i = 0, j = 0;
//	uint8 flags;
	SubGroup *sg1 = NULL;
	SubGroup *sg2 = NULL;
	MSG_S2C::stGroupAddMember MsgNewMember;
	MsgNewMember.SubTeam = -2;


	if (m_MemberCount >  1)
	{
		for( i = 0; i < m_SubGroupCount; i++ )
		{
			sg1 = m_SubGroups[i];

			if( sg1 != NULL)
			{
				for( itr1 = sg1->GetGroupMembersBegin(); itr1 != sg1->GetGroupMembersEnd(); ++itr1 )
				{
					// should never happen but just in case
					if( (*itr1) == NULL )
						continue;

					Player* ply = (*itr1)->m_loggedInPlayer;
					/* skip offline players */
					if( ply == NULL )
					{
	#ifdef VOICE_CHAT
						if( (*itr1)->groupVoiceId >= 0 )
						{
							// remove from voice members since that player is now offline
							RemoveVoiceMember( (*itr1) );
						}

						continue;
					}

					if( (*itr1)->groupVoiceId < 0 && sVoiceChatHandler.CanUseVoiceChat() )
					{
						(*itr1)->groupVoiceId = 0;
						AddVoiceMember( (*itr1) );
					}
	#else
						continue;
					}
	#endif

					MSG_S2C::stGroupList Msg;
					Msg.grouptype = m_GroupType;
					Msg.membercount = m_MemberCount;	// we don't include self
					Msg.subgroupcount = m_SubGroupCount;

					MSG_S2C::stGroupList::stMember tempmember;
					for( j = 0; j < m_SubGroupCount; j++ )
					{
						sg2 = m_SubGroups[j];

						if( sg2 != NULL)
						{
							MSG_S2C::stGroupList::stSubGroup subgroup;
							for( itr2 = sg2->GetGroupMembersBegin(); itr2 != sg2->GetGroupMembersEnd(); ++itr2 )
							{
								PlayerInfo* plyinfo = *itr2;
								/*if( (*itr1) == plyinfo )
									continue;*/

								// should never happen but just in case
								if( plyinfo == NULL )
									continue;

								tempmember.name = plyinfo->name;
								tempmember.guid = plyinfo->guid;
								if( plyinfo->m_loggedInPlayer != NULL )
								{
									tempmember.equiphead[0] = plyinfo->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0);
									tempmember.equiphead[1] = plyinfo->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+15*12);
									tempmember.equiphead[2] = plyinfo->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+2*12);
									tempmember.equiphead[3] = plyinfo->m_loggedInPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+17*12);
									tempmember.isloggedin = true;
								}
								else
								{
									tempmember.equiphead[0] = 0;
									tempmember.equiphead[1] = 0;
									tempmember.isloggedin = false;
								}
								
								tempmember.flags = 0;

								if( plyinfo == m_assistantLeader )
									tempmember.flags |= 1;
								if( plyinfo == m_mainTank )
									tempmember.flags |= 2;
								if( plyinfo == m_mainAssist )
									tempmember.flags |= 4;

								tempmember.Race = plyinfo->race;
								tempmember.Class = plyinfo->cl;
								tempmember.Gender = plyinfo->gender;
								tempmember.level = plyinfo->lastLevel;


								subgroup.vMembers.push_back( tempmember );
							}
							subgroup.ID = uint8( sg2->GetID() );
							Msg.vSubGroups.push_back( subgroup );
						}
					}

					Msg.leaderguid = m_Leader->guid;
					Msg.lootmethod = m_LootMethod;
					Msg.looterguid = m_Looter ? m_Looter->guid : uint64(0);
					Msg.lootthreshold = m_LootMethod;
					Msg.difficulty = m_difficulty;

			
					if( !ply->IsInWorld() )
						ply->CopyAndSendDelayedPacket( Msg );
					else
						ply->GetSession()->SendPacket( Msg );

				}

			}
		}

	}
	

	if( m_dirty )
	{
		m_dirty = false;
		SaveToDB();
	}

}

void Group::Disband()
{
	m_updateblock=true;

	if(m_isqueued)
	{
		m_isqueued=false;
		MSG_S2C::stChat_Message Msg;
		sChatHandler.FillSystemMessageData("A change was made to your group. Removing the arena queue.",&Msg);
		SendPacketToAll(Msg);
	}

	uint32 i = 0;
	for(i = 0; i < m_SubGroupCount; i++)
	{
		SubGroup *sg = m_SubGroups[i];
		sg->Disband();
	}

	sWorld.ExecuteSqlToDBServer("DELETE FROM groups WHERE group_id = %u", m_Id);
	//sInstanceMgr.OnGroupDestruction(this);
	delete this;	// destroy ourselves, the destructor removes from eventmgr and objectmgr.
}

void SubGroup::Disband()
{
	MSG_S2C::stGroupDestroyed MsgGroup;
	MSG_S2C::stParty_Command_Result MsgParty;

	GroupMembersSet::iterator itr = m_GroupMembers.begin();
	GroupMembersSet::iterator it2;
	for(; itr != m_GroupMembers.end();)
	{
		PlayerInfo* plyinfo = *itr;
		if( plyinfo != NULL )
		{
			if( plyinfo->m_loggedInPlayer )
			{
				if( plyinfo->m_loggedInPlayer->GetSession() != NULL )
				{
					plyinfo->m_loggedInPlayer->RemoveAura(plyinfo->m_loggedInPlayer->m_animalgroupspell);
					plyinfo->m_loggedInPlayer->GetSession()->SendPacket(MsgGroup);
					plyinfo->m_loggedInPlayer->GetSession()->SendPacket(MsgParty);
				}
			}

			plyinfo->m_Group = NULL;
			plyinfo->subGroup = -1;
			plyinfo->ResetAllInstance();
		}

		m_Parent->m_MemberCount--;
		it2 = itr;
		++itr;

		m_GroupMembers.erase(it2);
	}

	m_Parent->m_SubGroups[m_Id] = NULL;
	delete this;
}

Player* Group::FindFirstPlayer()
{
	GroupMembersSet::iterator itr;

	for( uint32 i = 0; i < m_SubGroupCount; i++ )
	{
		if( m_SubGroups[i] != NULL )
		{
			/*
			for( itr = m_SubGroups[i]->GetGroupMembersBegin(); itr != m_SubGroups[i]->GetGroupMembersEnd(); ++itr )
			{
				PlayerInfo* plyinfo = *itr;
				if( plyinfo != NULL )
				{
					if( plyinfo->m_loggedInPlayer != NULL )
					{
						return plyinfo->m_loggedInPlayer;
					}
				}
			}
			*/
			Player* ply = m_SubGroups[i]->GetFirstPlayer();
			if( ply )
				return ply;
		}
	}

	return NULL;
}

void Group::RemovePlayer(PlayerInfo * info)
{
	Player * pPlayer = info->m_loggedInPlayer;

	if(m_isqueued)
	{
		m_isqueued=false;
	}
	
	SubGroup *sg=NULL;
	if(info->subGroup >= 0 && info->subGroup <= 6)
		sg = m_SubGroups[info->subGroup];

	if(sg == NULL || sg->m_GroupMembers.find(info) == sg->m_GroupMembers.end())
	{
		for(uint32 i = 0; i < m_SubGroupCount; ++i)
		{
			if(m_SubGroups[i] != NULL)
			{
				if(m_SubGroups[i]->m_GroupMembers.find(info) != m_SubGroups[i]->m_GroupMembers.end())
				{
					sg = m_SubGroups[i];
					break;
				}
			}
		}
	}

	if( sg )
		sg->RemovePlayer(info);

	if( pPlayer )
		pPlayer->SetUInt32Value( PLAYER_FIELD_GROUP_ID, 0 );

	info->m_Group=NULL;
	info->subGroup=-1;
#ifdef VOICE_CHAT
	if( info->groupVoiceId <= 0 )
		RemoveVoiceMember(info);
#endif

	if(sg==NULL)
	{
		return;
	}

	m_dirty=true;
	
	--m_MemberCount;

	uint32 lastplayerid = 0;
	Player* ply = sg->GetFirstPlayer();
	if( !ply )
	{
		PlayerInfo* pinfo = sg->GetFirstPlayerInfo();
		if( pinfo )
			lastplayerid = pinfo->guid;
	}
	else
		lastplayerid = ply->GetLowGUID();
	Player *newPlayer = NULL;
	if(m_Looter == info)
	{
		newPlayer = FindFirstPlayer();
		if( newPlayer != NULL )
			m_Looter = newPlayer->m_playerInfo;
		else
			m_Looter = NULL;
	}
	if(m_Leader == info)
	{
		if( newPlayer==NULL )
			newPlayer=FindFirstPlayer();


		if( newPlayer != NULL )
		{
			if (m_MemberCount > 1)
			{
				SetLeader(newPlayer, false);
			}

		}
		else
			m_Leader = NULL;
	}

	if( info->m_loggedInPlayer != NULL )
		sInstanceMgr.PlayerLeftGroup( this, info->m_loggedInPlayer, lastplayerid );

	if( pPlayer != NULL )
	{
		if( pPlayer->GetSession() != NULL )
		{
			//SendNullUpdate( pPlayer );
			MSG_S2C::stGroupDestroyed MsgGroup;
			MSG_S2C::stParty_Command_Result MsgParty;
			pPlayer->GetSession()->SendPacket( MsgGroup );
			pPlayer->GetSession()->SendPacket( MsgParty );
		}

		//Remove some party auras.
		for (uint32 i=0;i<MAX_POSITIVE_AURAS;i++)
		{
			if (pPlayer->m_auras[i] && 
				pPlayer->m_auras[i]->m_areaAura && 
				pPlayer->m_auras[i]->GetUnitCaster() &&
				(!pPlayer->m_auras[i]->GetUnitCaster() ||(pPlayer->m_auras[i]->GetUnitCaster()->IsPlayer() && pPlayer!=pPlayer->m_auras[i]->GetUnitCaster())))
				pPlayer->m_auras[i]->Remove();
		}
	}

	if(m_MemberCount < 2)
	{
		if(m_disbandOnNoMembers)
		{
			Disband();
			return;
		}
	}

	/* eek! ;P */
	GroupMembersSet::iterator itr1, itr2;

	if (m_MemberCount > 1)
	{
		if (m_Leader)
		{
			if (m_Leader &&m_Leader->guid ==info->guid && m_MemberCount > 1)
			{
				Player* p = FindFirstPlayer();
				if (p)
				{
					SetLeader(p, false);
				}
			}
		}
		else
		{
			Player* p = FindFirstPlayer();
			if (p)
			{
				SetLeader(p, false);
			}
		}

	}




	uint32 i = 0, j = 0;
//	uint8 flags;
	SubGroup *sg1 = NULL;
	SubGroup *sg2 = NULL;
	MSG_S2C::stGroupRemoveMember MsgRemove;
	MsgRemove.guid = info->guid;


	if (m_MemberCount >  1)
	{
		for( i = 0; i < m_SubGroupCount; i++ )
		{
			sg1 = m_SubGroups[i];

			if( sg1 != NULL)
			{
				for( itr1 = sg1->GetGroupMembersBegin(); itr1 != sg1->GetGroupMembersEnd(); ++itr1 )
				{
					// should never happen but just in case
					if( (*itr1) == NULL )
						continue;

					Player* ply = (*itr1)->m_loggedInPlayer;
					/* skip offline players */
	
					if (!ply)
					{
						continue;
					}
					if( !ply->IsInWorld() )
						ply->CopyAndSendDelayedPacket( MsgRemove );
					else
						ply->GetSession()->SendPacket( MsgRemove );
		
					

				}

			}
		}

	if( m_dirty )
	{
		m_dirty = false;
		SaveToDB();
	}

	}
	
	//Update();
}

void Group::ExpandToRaid()
{
	if(m_isqueued)
	{
		m_isqueued=false;
		MSG_S2C::stChat_Message Msg;
		sChatHandler.FillSystemMessageData("A change was made to your group. Removing the arena queue.", &Msg);
		SendPacketToAll(Msg);
	}
	// Very simple ;)

	uint32 i = 1;
	m_SubGroupCount = 6;

	for(; i < m_SubGroupCount; i++)
		m_SubGroups[i] = new SubGroup(this, i);

	m_GroupType = GROUP_TYPE_RAID;
	m_dirty=true;
	Update();
}

void Group::SetLooter(Player *pPlayer, uint8 method, uint16 threshold)
{ 
	if( pPlayer != NULL )
	{
		m_LootMethod = method;
		m_Looter = pPlayer->m_playerInfo;
		m_LootThreshold  = threshold;
		m_dirty = true;
	}
	MSG_S2C::stGroupLootMethod Msg;
	Msg.lootmethod = method;
	Msg.lootthreshold = threshold;
	SendPacketToAll(Msg);
	//Update();
}

void Group::SendPacketToAllButOne(PakHead& pak, Player *pSkipTarget)
{
	GroupMembersSet::iterator itr;
	uint32 i = 0;
	for(; i < m_SubGroupCount; i++)
	{
		for(itr = m_SubGroups[i]->GetGroupMembersBegin(); itr != m_SubGroups[i]->GetGroupMembersEnd(); ++itr)
		{
			PlayerInfo* plyinfo = *itr;
			if(plyinfo->m_loggedInPlayer != NULL && plyinfo->m_loggedInPlayer != pSkipTarget)
				plyinfo->m_loggedInPlayer->GetSession()->SendPacket(pak);
		}
	}
	
}

void Group::OutPacketToAllButOne(uint16 op, uint16 len, const void* data, Player *pSkipTarget)
{
	GroupMembersSet::iterator itr;
	uint32 i = 0;
	for(; i < m_SubGroupCount; i++)
	{
		for(itr = m_SubGroups[i]->GetGroupMembersBegin(); itr != m_SubGroups[i]->GetGroupMembersEnd(); ++itr)
		{
			PlayerInfo* plyinfo = *itr;
			if(plyinfo->m_loggedInPlayer != NULL && plyinfo->m_loggedInPlayer != pSkipTarget)
				plyinfo->m_loggedInPlayer->GetSession()->OutPacket( op, len, data );
		}
	}
}

bool Group::HasMember(ui64 guid)
{
	GroupMembersSet::iterator itr;

	for( uint32 i = 0; i < m_SubGroupCount; i++ )
	{
		if( m_SubGroups[i] != NULL )
		{
			if( m_SubGroups[i]->HasMember(guid))
			{
				return true;
			}
		}
	}
	return false;
}

bool Group::HasMember(Player * pPlayer)
{
	if( !pPlayer )
		return false;

	GroupMembersSet::iterator itr;

	for( uint32 i = 0; i < m_SubGroupCount; i++ )
	{
		if( m_SubGroups[i] != NULL )
		{
			if( m_SubGroups[i]->m_GroupMembers.find( pPlayer->m_playerInfo ) != m_SubGroups[i]->m_GroupMembers.end() )
			{
				return true;
			}
		}
	}

	return false;
}

bool Group::HasMember(PlayerInfo * info)
{
	GroupMembersSet::iterator itr;
	uint32 i = 0;

	for(; i < m_SubGroupCount; i++)
	{
		if(m_SubGroups[i]->m_GroupMembers.find(info) != m_SubGroups[i]->m_GroupMembers.end())
		{
			return true;
		}
	}

	return false;
}

uint32 Group::GetNearbyMemberCount( Player* pPlayer )
{
	m_setNearbyPlayers.clear();

	for (int i = 0; i< m_SubGroupCount; i ++)
	{
		for( GroupMembersSet::iterator it = m_SubGroups[i]->m_GroupMembers.begin(); it != m_SubGroups[i]->m_GroupMembers.end(); ++it )
		{
			PlayerInfo* ply = *it;
			if( ply->m_loggedInPlayer == pPlayer )
			{
				m_setNearbyPlayers.insert( pPlayer );
				continue;
			}
			if( ply->m_loggedInPlayer )
			{
				if( ply->m_loggedInPlayer->GetMapId() == pPlayer->GetMapId() && ply->m_loggedInPlayer->GetInstanceID() )
				{
					if( ply->m_loggedInPlayer->IsInRangeSet( pPlayer ) )
					{
						m_setNearbyPlayers.insert( ply->m_loggedInPlayer );
					}
				}
			}
		}
	}

	return m_setNearbyPlayers.size();
}

void Group::InsertNearbyMember2Set( Unit* pUnit, std::set<Player*>& s )
{
	for (int i = 0; i< m_SubGroupCount; i ++)
	{
		for( GroupMembersSet::iterator it = m_SubGroups[i]->m_GroupMembers.begin(); it != m_SubGroups[i]->m_GroupMembers.end(); ++it )
		{
			PlayerInfo* ply = *it;
			if( ply->m_loggedInPlayer == pUnit )
			{
				continue;
			}
			if( ply->m_loggedInPlayer )
			{
				if( ply->m_loggedInPlayer->GetMapId() == pUnit->GetMapId() && ply->m_loggedInPlayer->GetInstanceID() )
				{
					if( ply->m_loggedInPlayer->IsInRangeSet( pUnit ) )
					{
						s.insert( ply->m_loggedInPlayer );
					}
				}
			}
		}
	}
}

/*
void Group::GiveHearbyHonorPoints( Player* pPlayer, uint32 pts )
{
	uint32 r = pts / m_setNearbyPlayers.size();
	for( std::set<Player*>::iterator it = m_setNearbyPlayers.begin(); it != m_setNearbyPlayers.end(); ++it )
	{
		Player* plr = *it;
		if( plr->IsValid() )
			plr->ModUnsigned32Value( PLAYER_FIELD_HONOR_CURRENCY, r > 0 ? r : 1 );
	}
}
*/

void Group::SendMessage2NearbyMembers( PakHead& msg )
{
	for( std::set<Player*>::iterator it = m_setNearbyPlayers.begin(); it != m_setNearbyPlayers.end(); ++it )
	{
		Player* p = *it;
		p->GetSession()->SendPacket( msg );
	}
}

uint32 Group::GetMaxMemberCount()
{
	if (m_GroupType == GROUP_TYPE_RAID)
	{
		return 30;
	}
	
	return 5;
}

uint32 Group::GetCurrentMemberCount()
{

	uint32 number = 0;
	for (int i = 0; i< m_SubGroupCount; i ++)
	{
		if (m_SubGroups[i])
		{
			for( GroupMembersSet::iterator it = m_SubGroups[i]->m_GroupMembers.begin(); it != m_SubGroups[i]->m_GroupMembers.end(); ++it )
			{
				number ++;
			}

		}
	}

	return number;
}
void Group::SwepPlayer(PlayerInfo* SrcInfo, PlayerInfo* DesInfo)
{

	SubGroup* srcGroup = NULL;
	SubGroup* desGroup = NULL;
	if(SrcInfo->subGroup > 0 && SrcInfo->subGroup <= m_SubGroupCount)
		srcGroup = m_SubGroups[SrcInfo->subGroup];

	if(DesInfo->subGroup > 0 && DesInfo->subGroup <= m_SubGroupCount)
		desGroup = m_SubGroups[DesInfo->subGroup];

	if (srcGroup == NULL || srcGroup->m_GroupMembers.find(SrcInfo) ==  srcGroup->m_GroupMembers.end())
	{
		for(uint32 i = 0; i < m_SubGroupCount; ++i)
		{
			if(m_SubGroups[i] != NULL)
			{
				if(m_SubGroups[i]->m_GroupMembers.find(SrcInfo) != m_SubGroups[i]->m_GroupMembers.end())
				{
					srcGroup = m_SubGroups[i];
					break;
				}
			}
		}
	}



	if (desGroup == NULL || desGroup->m_GroupMembers.find(DesInfo) ==  desGroup->m_GroupMembers.end())
	{
		for(uint32 i = 0; i < m_SubGroupCount; ++i)
		{
			if(m_SubGroups[i] != NULL)
			{
				if(m_SubGroups[i]->m_GroupMembers.find(DesInfo) != m_SubGroups[i]->m_GroupMembers.end())
				{
					desGroup = m_SubGroups[i];
					break;
				}
			}
		}
	}

	if(!desGroup || !srcGroup)
	{
		return;
	}


	desGroup->RemovePlayer(DesInfo);
	srcGroup->RemovePlayer(SrcInfo);

	if (desGroup->AddPlayer(SrcInfo) && srcGroup->AddPlayer(DesInfo))
	{
		DesInfo->subGroup = srcGroup->GetID();
		SrcInfo->subGroup = desGroup->GetID();
		SrcInfo->m_Group = this;
		DesInfo->m_Group = this;
		MSG_S2C::stGroupSwep Msg;
		Msg.guiddes = SrcInfo->guid;
		Msg.guidsrc = DesInfo->guid;
		SendPacketToAll(Msg);
		if( m_dirty )
		{
			m_dirty = false;
			SaveToDB();
		}



	}
	else
	{
		desGroup->RemovePlayer(SrcInfo);
		srcGroup->RemovePlayer(DesInfo);
		SrcInfo->m_Group = NULL;
		DesInfo->m_Group = NULL;
		Update();
	}





}

void Group::MovePlayer(PlayerInfo *info, uint8 subgroup)
{
	if( subgroup >= m_SubGroupCount )
		return;

	if(m_SubGroups[subgroup]->IsFull())
		return;

	SubGroup *sg=NULL;

	if(info->subGroup > 0 && info->subGroup <= m_SubGroupCount)
		sg = m_SubGroups[info->subGroup];

	if(sg == NULL || sg->m_GroupMembers.find(info) == sg->m_GroupMembers.end())
	{
		for(uint32 i = 0; i < m_SubGroupCount; ++i)
		{
			if(m_SubGroups[i] != NULL)
			{
				if(m_SubGroups[i]->m_GroupMembers.find(info) != m_SubGroups[i]->m_GroupMembers.end())
				{
					sg = m_SubGroups[i];
					break;
				}
			}
		}
	}

	if(!sg)
	{
		return;
	}
	
	sg->RemovePlayer(info);
    
	// Grab the new group, and insert
	sg = m_SubGroups[subgroup];
	if(!sg->AddPlayer(info))
	{
		RemovePlayer(info);
		info->m_Group=NULL;
		Update();
	}
	else
	{
		info->subGroup=(int8)sg->GetID();
		info->m_Group=this;
		MSG_S2C::stGroupChangeSubgroup Msg;
		Msg.guid = info->guid;
		Msg.team = subgroup;
		SendPacketToAll(Msg);
		if( m_dirty )
		{
			m_dirty = false;
			SaveToDB();
		}



	}


}

void Group::SendNullUpdate( Player *pPlayer )
{
	// this packet is 24 bytes long.		// AS OF 2.1.0
	MSG_S2C::stGroupList Msg;
	pPlayer->GetSession()->SendPacket( Msg );
}

// player is object class becouse its called from unit class
void Group::SendPartyKillLog( Object * player, Object * Unit )
{
	if( !player || !Unit || !HasMember( static_cast< Player* >( player ) ) )
		return;

	MSG_S2C::stParty_Kill_Log Msg;
	Msg.playerguid = player->GetGUID();
	Msg.unitguid = Unit->GetGUID();
	SendPacketToAll( Msg );
}

void Group::LoadFromDB(Field *fields)
{
#define LOAD_ASSISTANT(__i, __d) g = fields[__i].GetUInt32(); if(g != 0) { __d = objmgr.GetPlayerInfo(g); }

	uint32 g;
	m_updateblock=true;
	m_Id = fields[0].GetUInt32();

	ObjectMgr::getSingleton().AddGroup( this );

	m_GroupType = fields[1].GetUInt8();
	m_SubGroupCount = fields[2].GetUInt8();
	m_LootMethod = fields[3].GetUInt8();
	m_LootThreshold = fields[4].GetUInt8();
	m_difficulty = fields[5].GetUInt8();

	LOAD_ASSISTANT(6, m_assistantLeader);
	LOAD_ASSISTANT(7, m_mainTank);
	LOAD_ASSISTANT(8, m_mainAssist);

	// create groups
	for(int i = 1; i < m_SubGroupCount; ++i)
		m_SubGroups[i] = new SubGroup(this, i);

	// assign players into groups
	for(int i = 0; i < m_SubGroupCount; ++i)
	{
		for(int j = 0; j < 5; ++j)
		{
			uint32 guid = fields[9 + (i*5) + j].GetUInt32();
			if( guid == 0 )
				continue;

			PlayerInfo * inf = objmgr.GetPlayerInfo(guid);
			if(inf == NULL)
				continue;

			AddMember(inf);
			m_dirty=false;
		}
	}
	m_updateblock=false;
}

void Group::SaveToDB()
{
	if(!m_disbandOnNoMembers)	/* don't save bg groups */
		return;

	std::stringstream ss;
	//uint32 i = 0;
	uint32 fillers = 6 - m_SubGroupCount;

	ss << "REPLACE INTO groups VALUES("
		<< m_Id << ","
		<< uint32(m_GroupType) << ","
		<< uint32(m_SubGroupCount) << ","
		<< uint32(m_LootMethod) << ","
		<< uint32(m_LootThreshold) << ","
		<< uint32(m_difficulty) << ",";

	if(m_assistantLeader)
		ss << m_assistantLeader->guid << ",";
	else
		ss << "0,";
	
	if(m_mainTank)
		ss << m_mainTank->guid << ",";
	else
		ss << "0,";

	if(m_mainAssist)
		ss << m_mainAssist->guid << ",";
	else
		ss << "0,";

	for(uint32 i = 0; i < m_SubGroupCount; ++i)
	{
		uint32 j = 0;
		for(GroupMembersSet::iterator itr = m_SubGroups[i]->GetGroupMembersBegin(); j<5 && itr != m_SubGroups[i]->GetGroupMembersEnd(); ++j, ++itr)
		{
			ss << (*itr)->guid << ",";
		}

		for(; j < 5; ++j)
			ss << "0,";
	}

	for(uint32 i = 0; i < fillers; ++i)
		ss << "0,0,0,0,0,";

	ss << (uint32)UNIXTIME << ")";
	/*printf("==%s==\n", ss.str().c_str());*/
	sWorld.ExecuteSqlToDBServer(ss.str().c_str());
}



void Group::UpdateOutOfRangePlayer(Player * pPlayer, uint32 Flags, bool Distribute, MSG_S2C::stPartyMemberStat* Msg)
{
	uint8 member_flags = 0x01;

	if(pPlayer->GetPowerType() != POWER_TYPE_MANA)
		Flags |= GROUP_UPDATE_FLAG_POWER_TYPE;

	/*Flags |= GROUP_UPDATE_FLAG_PET_NAME;
	Flags |= GROUP_UPDATE_FLAG_PET_UNK_1;*/

	Msg->guid = pPlayer->GetNewGUID();
	Msg->mask = Flags;

	static ByteBuffer data;
	data.clear();

	if(Flags & GROUP_UPDATE_FLAG_ONLINE)
	{
		if(pPlayer->IsPvPFlagged())
			member_flags |= 0x02;
		if(pPlayer->getDeathState() == CORPSE)
			member_flags |= 0x08;
		else if(pPlayer->isDead())
			member_flags |= 0x10;

		data << member_flags;
	}

	if(Flags & GROUP_UPDATE_FLAG_HEALTH)
		data << pPlayer->GetUInt32Value(UNIT_FIELD_HEALTH);

	if(Flags & GROUP_UPDATE_FLAG_MAXHEALTH)
		data << pPlayer->GetUInt32Value(UNIT_FIELD_MAXHEALTH);

	if(Flags & GROUP_UPDATE_FLAG_POWER_TYPE)
		data << uint8(pPlayer->GetPowerType());

	if(Flags & GROUP_UPDATE_FLAG_POWER)
		data << pPlayer->GetUInt32Value(UNIT_FIELD_POWER1 + pPlayer->GetPowerType());

	if(Flags & GROUP_UPDATE_FLAG_MAXPOWER)
		data << pPlayer->GetUInt32Value(UNIT_FIELD_MAXPOWER1 + pPlayer->GetPowerType());

	if(Flags & GROUP_UPDATE_FLAG_LEVEL)
		data << uint16(pPlayer->getLevel());

	if(Flags & GROUP_UPDATE_FLAG_ZONEID) {
		data << uint16(pPlayer->GetAreaID());
    }

	if(Flags & GROUP_UPDATE_FLAG_POSITION)
	{
		data << int16(pPlayer->GetPositionX()) << int16(pPlayer->GetPositionY());			// wtf packed floats? O.o
		pPlayer->m_last_group_position = pPlayer->GetPosition();
	}

	if(Flags & GROUP_UPDATE_TYPE_FULL_REQUEST_REPLY)
	{
		data << uint64(0xFF00000000000000ULL);
		data << uint8(0);
		data << uint64(0xFF00000000000000ULL);
	}

	if (Flags & GROUP_UPDATE_FLAG_PLAYER_AURAS)
	{
// 		uint64 auramask = pPlayer->GetAuraUpdateMask();
// 		data << uint64(auramask);
		for(uint32 i = 0; i < MAX_AURAS; ++i)
		{
			if(true/*auramask & (uint64(1) << i)*/)
			{
				data << ui16(pPlayer->GetUInt32Value(UNIT_FIELD_AURA + i));
				data << uint8(1);
			}
		}
	}
	if( Flags & GROUP_UPDATE_FLAG_EQUIPMENT )
	{
		data << pPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0);
		data << pPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+15*12);
		data << pPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+2*12);
		data << pPlayer->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_0+17*12);
	}

	if( Flags & GROUP_UPDATE_FLAG_MAPID )
	{

		if (pPlayer->GetMapMgr())
		{
			data << pPlayer->GetMapMgr()->GetMapId();
		}
		else
		{
			data << 0;
		}

	}

	if (!data.empty() )
	{
		Msg->data.assign((const char*)data.contents(), data.size());

	}
	

	if(Distribute&&pPlayer->IsInWorld())
	{
		Player * plr;
		float dist = pPlayer->GetMapMgr()->m_UpdateDistance;
		for(uint32 i = 0; i < m_SubGroupCount; ++i)
		{
			if(m_SubGroups[i]==NULL)
				continue;

			for(GroupMembersSet::iterator itr = m_SubGroups[i]->GetGroupMembersBegin(); itr != m_SubGroups[i]->GetGroupMembersEnd();)
			{
				plr = (*itr)->m_loggedInPlayer;
				++itr;

				if(plr && plr != pPlayer)
				{
					if((plr->GetDistance2dSq(pPlayer) > dist) || plr->GetMapMgr() != pPlayer->GetMapMgr())
					plr->GetSession()->SendPacket(*Msg);
				}
			}
		}
	}


}

void Group::UpdateAllOutOfRangePlayersFor(Player * pPlayer)
{
	MSG_S2C::stPartyMemberStat MsgParty;
	static ByteBuffer data;
	data.clear();

	if(m_SubGroupCount>6)
		return;

	/* tell the other players about us */
	UpdateOutOfRangePlayer(pPlayer, GROUP_UPDATE_TYPE_FULL_CREATE, true, &MsgParty);

	/* tell us any other players we don't know about */
	Player * plr;
	bool u1, u2;
	UpdateMask myMask;
	myMask.SetCount(PLAYER_END);
	UpdateMask hisMask;
	hisMask.SetCount(PLAYER_END);

	for(uint32 i = 0; i < m_SubGroupCount; ++i)
	{
		if(m_SubGroups[i]==NULL)
			continue;

		for(GroupMembersSet::iterator itr = m_SubGroups[i]->GetGroupMembersBegin(); itr != m_SubGroups[i]->GetGroupMembersEnd(); ++itr)
		{
			plr = (*itr)->m_loggedInPlayer;
			if(!plr || plr == pPlayer) continue;

			if(!plr->IsVisible(pPlayer))
			{
				UpdateOutOfRangePlayer(plr, GROUP_UPDATE_TYPE_FULL_CREATE, false, &MsgParty);
				pPlayer->GetSession()->SendPacket(MsgParty);
			}
			else
			{
				if(pPlayer->GetSubGroup() == plr->GetSubGroup())
				{
					/* distribute quest fields to other players */
					hisMask.Clear();
					myMask.Clear();
					u1 = u2 = false;
					for(uint32 j = PLAYER_QUEST_LOG_1_1; j < PLAYER_QUEST_LOG_25_1; ++j)
					{
						if(plr->GetUInt32Value(j))
						{
							hisMask.SetBit(j);
							u1 = true;
						}

						if(pPlayer->GetUInt32Value(j))
						{
							u2 = true;
							myMask.SetBit(j);
						}
					}

					if(u1)
					{
						data.clear();
                        plr->BuildValuesUpdateBlockForPlayer(&data, &hisMask);
						pPlayer->PushUpdateData(&data, 1);
					}

					if(u2)
					{
						data.clear();
						pPlayer->BuildValuesUpdateBlockForPlayer(&data, &myMask);
						plr->PushUpdateData(&data, 1);
					}
				}
			}
		}
	}
}

void Group::HandlePetChangeType(uint32 Type, Player* pPlayer)
{
	uint32 Flags = 0;

	switch(Type)
	{
	case PARTY_UPDATE_FLAG_PET_NAME:
		Flags = GROUP_UPDATE_FLAG_PET_NAME;
		break;

	case PARTY_UPDATE_FLAG_PET_AURAS:
		Flags = GROUP_UPDATE_FLAG_PET_AURAS;
		break;
	}

	MSG_S2C::stPartyMemberStat Msg;

	if(Flags)
		UpdateOutOfRangePlayer(pPlayer, Flags, true, &Msg);
}

void Group::HandlePetChangeValue(uint32 Index, Player* pPlayer)
{
	uint32 Flags = 0;
	switch(Index)
	{
	case OBJECT_FIELD_GUID:
		Flags = GROUP_UPDATE_FLAG_PET_GUID;
		break;
	case UNIT_FIELD_DISPLAYID:
		Flags = GROUP_UPDATE_FLAG_PET_DISPLAYID;
		break;
	case UNIT_FIELD_HEALTH:
		Flags = GROUP_UPDATE_FLAG_PET_HEALTH;
		break;
	case UNIT_FIELD_MAXHEALTH:
		Flags = GROUP_UPDATE_FLAG_PET_MAXHEALTH;
		break;
	case UNIT_FIELD_POWER1:
		Flags = GROUP_UPDATE_FLAG_PET_POWER;
		break;
	case UNIT_FIELD_MAXPOWER1:
		Flags = GROUP_UPDATE_FLAG_PET_MAXPOWER;
		break;	
	}
	MSG_S2C::stPartyMemberStat Msg;
	if( Flags != 0 )
		UpdateOutOfRangePlayer( pPlayer, Flags, true, &Msg );
}

void Group::HandleUpdateFieldChange(uint32 Index, Player * pPlayer)
{
	uint32 Flags = 0;
	switch(Index)
	{
	case UNIT_FIELD_HEALTH:
		Flags = GROUP_UPDATE_FLAG_HEALTH;
		break;
		
	case UNIT_FIELD_MAXHEALTH:
		Flags = GROUP_UPDATE_FLAG_MAXHEALTH;
		break;

	case UNIT_FIELD_POWER1:
		Flags = GROUP_UPDATE_FLAG_POWER;
		break;

	case UNIT_FIELD_MAXPOWER1:
		Flags = GROUP_UPDATE_FLAG_MAXPOWER;
		break;

	case UNIT_FIELD_LEVEL:
		Flags = GROUP_UPDATE_FLAG_LEVEL;
		break;
	default:
		break;
	}

	if(Index == PLAYER_VISIBLE_ITEM_1_0 || Index == PLAYER_VISIBLE_ITEM_15_0 || Index == PLAYER_VISIBLE_ITEM_2_0 || Index == PLAYER_VISIBLE_ITEM_17_0)
		Flags = GROUP_UPDATE_FLAG_EQUIPMENT;

	MSG_S2C::stPartyMemberStat Msg;
	if( Flags != 0 )
		UpdateOutOfRangePlayer( pPlayer, Flags, true, &Msg );

}

void Group::HandlePartialChange(uint32 Type, Player * pPlayer)
{
	uint32 Flags = 0;

	switch(Type)
	{
	case PARTY_UPDATE_FLAG_POSITION:
		Flags = GROUP_UPDATE_FLAG_POSITION;
		break;

	case PARTY_UPDATE_FLAG_ZONEID:
		Flags = GROUP_UPDATE_FLAG_ZONEID;
		break;

	case PARTY_UPDATE_FLAG_MAPID:
		Flags = GROUP_UPDATE_FLAG_MAPID;
		break;
	}

	MSG_S2C::stPartyMemberStat Msg;

	if(Flags)
		UpdateOutOfRangePlayer(pPlayer, Flags, true, &Msg);

}

void WorldSession::HandlePartyMemberStatsOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld())
		return;

	MSG_C2S::stPartyMemberStat MsgRecv;packet >> MsgRecv;

	Player * plr = _player->GetMapMgr()->GetPlayer((uint32)MsgRecv.guid);

	if(!_player->GetGroup() || !plr)
		return;

	MSG_S2C::stPartyMemberStat Msg;
	if(!_player->GetGroup()->HasMember(plr))
		return;			// invalid player

	if(_player->IsVisible(plr))
		return;

	_player->GetGroup()->UpdateOutOfRangePlayer(plr, GROUP_UPDATE_TYPE_FULL_CREATE | GROUP_UPDATE_TYPE_FULL_REQUEST_REPLY, false, &Msg);
	SendPacket(Msg);
}

Group* Group::Create()
{
	return new Group(true);
}

void Group::SetMainAssist(PlayerInfo * pMember)
{
	if(m_mainAssist==pMember)
		return;

	m_mainAssist = pMember;
	m_dirty = true;
	Update();
}

void Group::SetMainTank(PlayerInfo * pMember)
{
	if(m_mainTank==pMember)
		return;

	m_mainTank=pMember;
	m_dirty = true;
	Update();
}

void Group::SetAssistantLeader(PlayerInfo * pMember)
{
	if(m_assistantLeader == pMember)
		return;

	m_assistantLeader = pMember;
	m_dirty = true;
	Update();
}


/************************************************************************/
/* Voicechat                                                            */
/************************************************************************/
#ifdef VOICE_CHAT

void Group::CreateVoiceSession()
{
	sVoiceChatHandler.CreateGroupChannel( this );
}

void Group::VoiceChannelCreated(uint16 id)
{
	MyLog::log->debug("Group: voicechannelcreated: id %u", (uint32)id);
	m_voiceChannelId = id;
	if( id != 0 )
	{
		// so we just got a channel. we better activate the slots that are in use so people can talk
		uint32 i;
		for( i = 0; i <= 40; ++i )
		{
			if( m_voiceMembersList[i] != NULL )
				sVoiceChatHandler.ActivateChannelSlot(id, (uint8)i );
		}

		SendVoiceUpdate();
	}
}

void Group::AddVoiceMember(PlayerInfo * pPlayer)
{
	MyLog::log->debug("Group: adding voice member %u to group %u", pPlayer->guid, GetID());
	uint32 i;

	// find him an id
	for( i = 1; i <= 40; ++i )
	{
		if( m_voiceMembersList[i] == NULL )
			break;
	}

	if( i == 41 )
	{
		// no free slots
		MyLog::log->error("Group: could not add voice member, no slots!");
		return;
	}

	pPlayer->groupVoiceId = i;
	m_voiceMembersList[i] = pPlayer;
	++m_voiceMemberCount;

	if( !m_voiceChannelRequested )
	{
		/*uint32 count = 0;
		for( i = 0; i < 41; ++i )
		{
			if( m_voiceMembersList[i] != NULL )
			{
				++count;
				if( count >= 2 )
					break;
			}
		}*/

		if( m_voiceMemberCount >= 2 )		// Don't create channels with only one member
		{
			CreateVoiceSession();
			m_voiceChannelRequested = true;
		}
	}
	else
	{
		if( m_voiceChannelId != 0 )
		{
			// activate his slot
			sVoiceChatHandler.ActivateChannelSlot(m_voiceChannelId, (uint8)i);
			SendVoiceUpdate();
		}
	}

}

void Group::RemoveVoiceMember(PlayerInfo * pPlayer)
{
	if( pPlayer->groupVoiceId <= 0 )
		return;

	MyLog::log->debug("Group: removing voice member %u from group %u", pPlayer->guid, GetID());

	if( m_voiceMembersList[pPlayer->groupVoiceId] == pPlayer )
	{
		--m_voiceMemberCount;
		m_voiceMembersList[pPlayer->groupVoiceId] = NULL;

		if( m_voiceChannelId != 0 )
		{
			// turn off the slot
			sVoiceChatHandler.DeactivateChannelSlot(m_voiceChannelId, pPlayer->groupVoiceId);
			SendVoiceUpdate();
		}

		if( m_voiceMemberCount < 2 )
		{
			// destroy the channel
			sVoiceChatHandler.DestroyGroupChannel(this);
			m_voiceChannelId = 0;
			m_voiceChannelRequested = false;
		}
	}

	pPlayer->groupVoiceId = -1;
}

void Group::SendVoiceUpdate()
{
	MyLog::log->debug("Group: sendgroupupdate id %u", (uint32)m_voiceChannelId);

	//static uint8 EncryptionKey[16] = { 0x14, 0x60, 0xcf, 0xaf, 0x9e, 0xa2, 0x78, 0x38, 0xce, 0xc7, 0xaf, 0x0b, 0x3a, 0x23, 0x61, 0x44 };
	static uint8 EncryptionKey[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	uint8 counter = 1;
	size_t pos;
	uint32 i,j;
	Player * pl;

	WorldPacket data(SMSG_VOICE_SESSION, 100);
	data << uint32( 0x00000E9D );
	data << uint32( 0xE2500000 );		// this appears to be constant :S

	data << uint16( m_voiceChannelId );		// voice channel id, used in udp packet
	data << uint8( 2 );						// party voice channel
	data << uint8( 0 );						// for channels this is name
	data.append( EncryptionKey, 16 );		// encryption key

	// IP
	// these dont appear to be in network byte order.. gg
	data << uint32(htonl(sVoiceChatHandler.GetVoiceServerIP()));
	data << uint16(sVoiceChatHandler.GetVoiceServerPort());
	
	data << uint8( m_voiceMemberCount );
	pos = data.wpos();

	for( i = 0; i <= 40; ++i )
	{
		if( m_voiceMembersList[i] == NULL )
			continue;

		if( m_voiceMembersList[i]->m_loggedInPlayer == NULL )
		{
			// shouldnt happen
			RemoveVoiceMember(m_voiceMembersList[i]);
			continue;
		}

		pl = m_voiceMembersList[i]->m_loggedInPlayer;

		// Append ourself first, always.
		data << uint64 ( pl->GetGUID() );
		data << uint8( i );

		if( pl->m_playerInfo == m_Leader )
		{
			data << uint8(0x06);
		}
		else
		{
			data << uint8(0x46);
		}

		for( j = 0; j <= 40; ++j )
		{
			if( i == j || m_voiceMembersList[j] == NULL )
				continue;

			if( m_voiceMembersList[j]->m_loggedInPlayer == NULL )
			{
				// shouldnt happen
				RemoveVoiceMember(m_voiceMembersList[j]);
				continue;
			}

			data << uint64( m_voiceMembersList[j]->guid );
			data << uint8( j );

			if( m_voiceMembersList[j] == m_Leader )
			{
				data << uint8(0x80) << uint8(0x47);
			}
			else
			{
				data << uint8(0xC8) << uint8(0x47);
			}
		}

		//data << uint8( 0x47 );

		pl->GetSession()->SendPacket( &data );
		data.wpos( pos );
	}

}

void Group::VoiceSessionDropped()
{
	MyLog::log->debug("Group: Voice session dropped");
	for(uint32 i = 0; i < 41; ++i)
	{
		if( m_voiceMembersList[i] != NULL )
		{
			m_voiceMembersList[i]->groupVoiceId = -1;
			m_voiceMembersList[i] = NULL;
		}
	}
	m_voiceChannelRequested = false;
	m_voiceChannelId = 0;
	m_voiceMemberCount = 0;
}

void Group::VoiceSessionReconnected()
{
	if( !sVoiceChatHandler.CanUseVoiceChat() )
		return;

	// try to recreate a group if one is needed
	GroupMembersSet::iterator itr1, itr2;
	MyLog::log->debug("Group: Attempting to recreate voice session for group %u", GetID());

	uint32 i = 0, j = 0;
	SubGroup *sg1 = NULL;
	SubGroup *sg2 = NULL;

	for( i = 0; i < m_SubGroupCount; i++ )
	{
		sg1 = m_SubGroups[i];

		if( sg1 != NULL)
		{
			for( itr1 = sg1->GetGroupMembersBegin(); itr1 != sg1->GetGroupMembersEnd(); ++itr1 )
			{
				// should never happen but just in case
				if( (*itr1) == NULL )
					continue;

				/* skip offline players */
				if( (*itr1)->m_loggedInPlayer == NULL )
				{
					if( (*itr1)->groupVoiceId >= 0 )
					{
						// remove from voice members since that player is now offline
						RemoveVoiceMember( (*itr1) );
					}

					continue;
				}

				if( (*itr1)->groupVoiceId < 0 )
				{
					(*itr1)->groupVoiceId = 0;
					AddVoiceMember( (*itr1) );
				}
			}
		}
	}

}
#endif
