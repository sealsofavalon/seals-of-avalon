#ifndef _GROUP_H_
#define _GROUP_H_

struct PlayerInfo;
class Player;

typedef struct
{
	PlayerInfo * player_info;
	Player * player;
}GroupMember;

namespace MSG_S2C
{
	struct stPartyMemberStat;
}

typedef std::set<PlayerInfo*> GroupMembersSet;
class Group;

class SERVER_DECL SubGroup	  // Most stuff will be done through here, not through the "Group" class.
{
public:
	friend class Group;

	SubGroup(Group* parent, uint32 id):m_Parent(parent),m_Id(id)
	{
	}

	~SubGroup();

	SUNYOU_INLINE GroupMembersSet::iterator GetGroupMembersBegin(void) { return m_GroupMembers.begin(); }
	SUNYOU_INLINE GroupMembersSet::iterator GetGroupMembersEnd(void)   { return m_GroupMembers.end();   }

	bool AddPlayer(PlayerInfo * info);
	void RemovePlayer(PlayerInfo * info);
	Player* GetFirstPlayer();
	PlayerInfo* GetFirstPlayerInfo();
	
	SUNYOU_INLINE bool IsFull(void)				{ return m_GroupMembers.size() >= MAX_GROUP_SIZE_PARTY; }
	SUNYOU_INLINE size_t GetMemberCount(void)		{ return m_GroupMembers.size(); }
	
	SUNYOU_INLINE uint32 GetID(void)			   { return m_Id; }
	SUNYOU_INLINE void SetID(uint32 newid)		 { m_Id = newid; }

	SUNYOU_INLINE void   SetParent(Group* parent)  { m_Parent = parent; }
	SUNYOU_INLINE Group* GetParent(void)		   { return m_Parent; }


	void   Disband();
	bool HasMember(uint32 guid);

protected:
	std::list<PlayerInfo*> m_listMembers;
	GroupMembersSet	 m_GroupMembers;
	Group*			  m_Parent;
	uint32			  m_Id;

};

class Arena;
class SERVER_DECL Group
{
public:
	friend class SubGroup;

	static Group* Create();

	Group(bool Assign);
	~Group();

	// Adding/Removal Management
	bool AddMember(PlayerInfo * info, int32 subgroupid=-1);
	void RemovePlayer(PlayerInfo * info);

	// Leaders and Looting
	void SetLeader(Player* pPlayer,bool silent);
	void SetLooter(Player *pPlayer, uint8 method, uint16 threshold);

	// Transferring data to clients
	void Update();
	void UpdateGroupListForPlayer(Player *pPlayer);


	SUNYOU_INLINE void SendPacketToAll(PakHead& pak) { SendPacketToAllButOne(pak, NULL); }
	void SendPacketToAllButOne(PakHead& pak, Player *pSkipTarget);

	SUNYOU_INLINE void OutPacketToAll(uint16 op, uint16 len, const void* data) { OutPacketToAllButOne(op, len, data, NULL); }
	void OutPacketToAllButOne(uint16 op, uint16 len, const void* data, Player *pSkipTarget);

	void SendNullUpdate(Player *pPlayer);

	// Group Combat
	void SendPartyKillLog(Object * player, Object * Unit);

	// Destroying/Converting
	void Disband();
	Player* FindFirstPlayer();
	
	// Accessing functions
	SUNYOU_INLINE SubGroup* GetSubGroup(uint32 Id)
	{
		if(Id >= 6)
			return 0;

		return m_SubGroups[Id];
	}

	SUNYOU_INLINE uint32 GetSubGroupCount(void) { return m_SubGroupCount; }

	SUNYOU_INLINE uint8 GetMethod(void) { return m_LootMethod; }
	SUNYOU_INLINE uint16 GetThreshold(void) { return m_LootThreshold; }
	SUNYOU_INLINE PlayerInfo* GetLeader(void) { return m_Leader; }
	SUNYOU_INLINE PlayerInfo* GetLooter(void) { return m_Looter; }

	void MovePlayer(PlayerInfo* info, uint8 subgroup);
	void SwepPlayer(PlayerInfo* SrcInfo, PlayerInfo* DesInfo);
	uint32 GetMaxMemberCount();
	uint32 GetCurrentMemberCount();

	bool HasMember(Player *pPlayer);
	bool HasMember(PlayerInfo * info);
	bool HasMember(ui64 guid);
	uint32 GetNearbyMemberCount( Player* pPlayer );
	void InsertNearbyMember2Set( Unit* pUnit, std::set<Player*>& s );
	void SendMessage2NearbyMembers( PakHead& msg );

	SUNYOU_INLINE uint32 MemberCount(void) { return m_MemberCount; }
	SUNYOU_INLINE bool IsFull() { return ((m_GroupType == GROUP_TYPE_PARTY && m_MemberCount >= MAX_GROUP_SIZE_PARTY) || (m_GroupType == GROUP_TYPE_RAID && m_MemberCount >= MAX_GROUP_SIZE_RAID)); }

	SubGroup* FindFreeSubGroup();

	void ExpandToRaid();

	void SaveToDB();
	void LoadFromDB(Field *fields);

	SUNYOU_INLINE uint8 GetGroupType() { return m_GroupType; }
	SUNYOU_INLINE uint32 GetID() { return m_Id; }

	void UpdateOutOfRangePlayer(Player * pPlayer, uint32 Flags, bool Distribute, MSG_S2C::stPartyMemberStat* Msg);
	void UpdateAllOutOfRangePlayersFor(Player * pPlayer);
	void HandleUpdateFieldChange(uint32 Index, Player * pPlayer);
	void HandlePartialChange(uint32 Type, Player * pPlayer);
	void HandlePetChangeValue(uint32 Index, Player* pPlayer);
	void HandlePetChangeType(uint32 Type, Player* pPlayer);

	uint64 m_targetIcons[8];
	int m_targetUniqueID[8];
	bool m_disbandOnNoMembers;
	bool m_isqueued;
	std::set<Player*> m_setNearbyPlayers;
	uint32 m_unRaidReadyCheck;
	bool m_bCanApplyForJoining;
	std::string m_GroupTitile;
	PlayerInfo* m_pNewPlayer;
	void SetAssistantLeader(PlayerInfo * pMember);
	void SetMainTank(PlayerInfo * pMember);
	void SetMainAssist(PlayerInfo * pMember);


	SUNYOU_INLINE PlayerInfo * GetAssistantLeader() { return m_assistantLeader; }
	SUNYOU_INLINE PlayerInfo * GetMainTank() { return m_mainTank; }
	SUNYOU_INLINE PlayerInfo * GetMainAssist() { return m_mainAssist; }

	void SetDifficulty(uint8 difficulty);
	
	/************************************************************************/
	/* Voicechat                                                            */
	/************************************************************************/
#ifdef VOICE_CHAT
	void AddVoiceMember(PlayerInfo * pPlayer);
	void RemoveVoiceMember(PlayerInfo * pPlayer);
	void SendVoiceUpdate();
	void CreateVoiceSession();
	void VoiceChannelCreated(uint16 id);
	void VoiceSessionDropped();
	void VoiceSessionReconnected();

public:
	bool m_voiceChannelRequested;
	int16 m_voiceChannelId;
	uint32 m_voiceMemberCount;
protected:
	PlayerInfo* m_voiceMembersList[41];
#endif	// VOICE_CHAT

protected:
	PlayerInfo * m_Leader;
	PlayerInfo * m_Looter;
	PlayerInfo * m_assistantLeader;
	PlayerInfo * m_mainTank;
	PlayerInfo * m_mainAssist;

	uint8 m_LootMethod;
	uint16 m_LootThreshold;
	uint8 m_GroupType;
	uint32 m_Id;

	SubGroup* m_SubGroups[6];
	uint8 m_SubGroupCount;
	uint32 m_MemberCount;
	bool m_dirty;
	bool m_updateblock;
	uint8 m_difficulty;



};

#endif  // _GROUP_H_
