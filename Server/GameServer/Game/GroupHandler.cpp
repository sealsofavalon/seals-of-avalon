#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Group.h"
#include "../../SDBase/Protocol/S2C_Group.h"

//////////////////////////////////////////////////////////////
/// This function handles CMSG_GROUP_INVITE
//////////////////////////////////////////////////////////////
void WorldSession::HandleGroupInviteOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;

	MSG_C2S::stGroupInvite MsgRecv;
	packet >> MsgRecv;
	Player * player = NULL;
	Group *group = NULL;

	if(_player->HasBeenInvited())return;

	player = objmgr.GetPlayer(MsgRecv.membername.c_str(), false);

	if ( player == NULL)
	{
		SendPartyCommandResult(_player, 0, MsgRecv.membername, ERR_PARTY_CANNOT_FIND);
		return;
	}

	if (player == _player)
	{
		return;
	}

	if ( _player->InGroup() && !_player->IsGroupLeader() )
	{
		SendPartyCommandResult(_player, 0, "", ERR_PARTY_YOU_ARE_NOT_LEADER);
		return;
	}

	group = _player->GetGroup();
	if ( group != NULL )
	{
		if( !group->m_disbandOnNoMembers )
		{
			SendPartyCommandResult(_player, 0, "", ERR_PARTY_IN_ARENA_OR_BG);
			return;
		}
		if (group->IsFull())
		{
			SendPartyCommandResult(_player, 0, "", ERR_PARTY_IS_FULL);
			return;
		}
	}
	

	if ( player->InGroup() )
	{
		Group* grp = player->GetGroup();
		if( grp->GetSubGroup( 0 )->GetMemberCount() == 1 )
			grp->Disband();
		else
		{
			SendPartyCommandResult(_player, grp->GetGroupType(), MsgRecv.membername, ERR_PARTY_ALREADY_IN_GROUP);
			return;
		}
	}
	/*
	if(player->GetTeam()!=_player->GetTeam() && _player->GetSession()->GetPermissionCount() == 0)
	{
		SendPartyCommandResult(_player, 0, MsgRecv.membername, ERR_PARTY_WRONG_FACTION);
		return;
	}
	*/

	if ( player->HasBeenInvited() )
	{
		SendPartyCommandResult(_player, 0, MsgRecv.membername, ERR_PARTY_ALREADY_IN_GROUP);
		return;
	}

	if( player->Social_IsIgnoring( _player->GetLowGUID() ) )
	{
		SendPartyCommandResult(_player, 0, MsgRecv.membername, ERR_PARTY_IS_IGNORING_YOU);
		return;
	}

	// 16/08/06 - change to guid to prevent very unlikely event of a crash in deny, etc
	_player->SetInviter(_player->GetLowGUID());//bugfix if player invtied 2 people-> he can be in 2 parties

	MSG_S2C::stGroupInvite Msg;
	Msg.name = GetPlayer()->GetName();

	player->GetSession()->SendPacket(Msg);

	uint32 gtype = 0;
	if(group)
		gtype = group->GetGroupType();

	SendPartyCommandResult(_player, gtype, MsgRecv.membername, ERR_PARTY_NO_ERROR);

	// 16/08/06 - change to guid to prevent very unlikely event of a crash in deny, etc
	player->SetInviter(_player->GetLowGUID());
}

///////////////////////////////////////////////////////////////
///This function handles CMSG_GROUP_CANCEL:
///////////////////////////////////////////////////////////////
void WorldSession::HandleGroupCancelOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MyLog::log->debug( "WORLD: got CMSG_GROUP_CANCEL." );
}

////////////////////////////////////////////////////////////////
///This function handles CMSG_GROUP_ACCEPT:
////////////////////////////////////////////////////////////////
void WorldSession::HandleGroupAcceptOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;

	Player *player = objmgr.GetPlayer(_player->GetInviter());
	if ( !player )
		return;
	
	player->SetInviter(0);
	_player->SetInviter(0);
	
	Group *grp = player->GetGroup();
	

	MSG_S2C::stMsg_Dungeon_Difficulty MsgDiffculty;
	if(grp)
	{
		grp->m_pNewPlayer = _player->m_playerInfo;
		grp->AddMember(_player->m_playerInfo);
		if(grp->GetLeader()->m_loggedInPlayer)
			_player->iInstanceType = grp->GetLeader()->m_loggedInPlayer->iInstanceType;

		MsgDiffculty.instanceType = _player->iInstanceType;
		_player->GetSession()->SendPacket( MsgDiffculty );
        //sInstanceSavingManager.ResetSavedInstancesForPlayer(_player);
		return;
	}
	
	// If we're this far, it means we have no existing group, and have to make one.
	grp = new Group(true);
	grp->m_pNewPlayer = NULL;
	grp->AddMember(player->m_playerInfo);		// add the inviter first, therefore he is the leader
	grp->AddMember(_player->m_playerInfo);	   // add us.
    _player->iInstanceType = player->iInstanceType;
	MsgDiffculty.instanceType = _player->iInstanceType;
	_player->GetSession()->SendPacket( MsgDiffculty );

	//player->m_playerInfo->ResetAllInstance();
    //sInstanceSavingManager.ResetSavedInstancesForPlayer(_player);
	sInstanceMgr.PlayerNewGroup( player->m_playerInfo->guid, grp->GetID() );

	// Currentgroup and all that shit are set by addmember.
}

///////////////////////////////////////////////////////////////////////////////////////
///This function handles CMSG_GROUP_DECLINE:
//////////////////////////////////////////////////////////////////////////////////////
void WorldSession::HandleGroupDeclineOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MSG_S2C::stGroupDecline Msg;

	Player *player = objmgr.GetPlayer(_player->GetInviter());
	if(!player) return;

	Msg.name = GetPlayer()->GetName();

	player->GetSession()->SendPacket( Msg );
	player->SetInviter(0);
	_player->SetInviter(0);
}

//////////////////////////////////////////////////////////////////////////////////////////
///This function handles CMSG_GROUP_UNINVITE:
//////////////////////////////////////////////////////////////////////////////////////////
void WorldSession::HandleGroupUninviteOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;

	MSG_C2S::stGroupUnviteName MsgRecv;
	packet >> MsgRecv;
	Group *group;
	Player * player;
	PlayerInfo * info;

	player = objmgr.GetPlayer(MsgRecv.membername.c_str(), false);
	info = objmgr.GetPlayerInfoByName(MsgRecv.membername.c_str());
	if ( player == NULL && info == NULL )
	{
		SendPartyCommandResult(_player, 0, MsgRecv.membername, ERR_PARTY_CANNOT_FIND);
		return;
	}

	if ( !_player->InGroup() || info->m_Group != _player->GetGroup() )
	{
		SendPartyCommandResult(_player, 0, MsgRecv.membername, ERR_PARTY_IS_NOT_IN_YOUR_PARTY);
		return;
	}

	if( !_player->GetGroup()->m_disbandOnNoMembers )
	{
		SendPartyCommandResult(_player, 0, "", ERR_PARTY_IN_ARENA_OR_BG);
		return;
	}

	if ( !_player->IsGroupLeader() )
	{
		if(_player != player)
		{
			SendPartyCommandResult(_player, 0, "", ERR_PARTY_YOU_ARE_NOT_LEADER);
			return;
		}
	}

	group = _player->GetGroup();

	if(group)
		group->RemovePlayer(info);
}

//////////////////////////////////////////////////////////////////////////////////////////
///This function handles CMSG_GROUP_UNINVITE_GUID:
//////////////////////////////////////////////////////////////////////////////////////////
void WorldSession::HandleGroupUninviteGuildOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;

	MSG_C2S::stGroupUnviteGuid MsgRecv;
	packet >> MsgRecv;
	Group *group;
	Player * player;
	PlayerInfo * info;

	player = objmgr.GetPlayer( MsgRecv.guid );
	info = objmgr.GetPlayerInfo( MsgRecv.guid );
	if ( player == NULL && info == NULL )
	{
		SendPartyCommandResult(_player, 0, "", ERR_PARTY_CANNOT_FIND);
		return;
	}

	if ( !_player->InGroup() || info->m_Group != _player->GetGroup() )
	{
		SendPartyCommandResult(_player, 0, info->name, ERR_PARTY_IS_NOT_IN_YOUR_PARTY);
		return;
	}

	if ( !_player->IsGroupLeader() )
	{
		if(_player != player)
		{
			SendPartyCommandResult(_player, 0, "", ERR_PARTY_YOU_ARE_NOT_LEADER);
			return;
		}
	}

	group = _player->GetGroup();

	if(group)
		group->RemovePlayer(info);
	//MyLog::log->debug( "WORLD: got CMSG_GROUP_UNINVITE_GUID." );
}

//////////////////////////////////////////////////////////////////////////////////////////
///This function handles CMSG_GROUP_SET_LEADER:
//////////////////////////////////////////////////////////////////////////////////////////
void WorldSession::HandleGroupSetLeaderOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	// important note _player->GetName() can be wrong.
	MSG_C2S::stGroupSetLeader MsgRecv;
	packet >> MsgRecv;
	Player * player;
	
	player = objmgr.GetPlayer((uint32)MsgRecv.guid);

	if ( player == NULL )
	{
		//SendPartyCommandResult(_player, 0, membername, ERR_PARTY_CANNOT_FIND);
		SendPartyCommandResult(_player, 0, _player->GetName(), ERR_PARTY_CANNOT_FIND);
		return;
	}

	if(!_player->IsGroupLeader())
	{
		SendPartyCommandResult(_player, 0, "", ERR_PARTY_YOU_ARE_NOT_LEADER);
		return;
	}
	
	if(player->GetGroup() != _player->GetGroup())
	{
		//SendPartyCommandResult(_player, 0, membername, ERR_PARTY_IS_NOT_IN_YOUR_PARTY);
		SendPartyCommandResult(_player, 0, _player->GetName(), ERR_PARTY_IS_NOT_IN_YOUR_PARTY);
		return;
	}

	Group *pGroup = _player->GetGroup();
	if(pGroup)
		pGroup->SetLeader(player,false);
}

//////////////////////////////////////////////////////////////////////////////////////////
///This function handles CMSG_GROUP_DISBAND:
//////////////////////////////////////////////////////////////////////////////////////////
void WorldSession::HandleGroupDisbandOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	Group* pGroup = _player->GetGroup();
	if(!pGroup) return;

	if( !pGroup->m_disbandOnNoMembers )
	{
		SendPartyCommandResult(_player, 0, "", ERR_PARTY_IN_ARENA_OR_BG);
		return;
	}

	//pGroup->Disband();
	pGroup->RemovePlayer(_player->m_playerInfo);
}

//////////////////////////////////////////////////////////////////////////////////////////
///This function handles CMSG_LOOT_METHOD:
//////////////////////////////////////////////////////////////////////////////////////////
void WorldSession::HandleLootMethodOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stGroupLootMethod MsgRecv;packet >> MsgRecv;
  
	if(!_player->IsGroupLeader())
	{
		SendPartyCommandResult(_player, 0, "", ERR_PARTY_YOU_ARE_NOT_LEADER);
		return;
	}
	
	// TODO: fix me burlex 
	//Player *plyr = objmgr.GetPlayer((uint32)lootMaster);
	//if(!plyr)return;
	Group* pGroup = _player->GetGroup();
	if( pGroup != NULL)
		pGroup->SetLooter( _player, MsgRecv.lootMethod, MsgRecv.threshold );
}

void WorldSession::HandleMinimapPingOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stMiniMapPing MsgRecv;packet >> MsgRecv;
	if(!_player->InGroup())
	return;
	Group * party= _player->GetGroup();
	if(!party)return;

	MSG_S2C::stMiniMapPing Msg;
	Msg.guid	= _player->GetGUID();
	Msg.x		= MsgRecv.x;
	Msg.y		= MsgRecv.y;
	party->SendPacketToAllButOne(Msg, _player);
}

void WorldSession::HandleGroupModifyTitleReqOpcode(CPacketUsn& packet)
{
	MSG_C2S::stGroupModifyTitleReq MsgRe;
	packet >> MsgRe;
	if (!_player)
	{
		return;
	}

	Group* pGroup = _player->GetGroup();
	MSG_S2C::stGroupModifyTitleAck MsgModifyTitleAck;
	if (!pGroup)
	{
		MsgModifyTitleAck.Type = 1;
	}
	else
	{
		if (pGroup->GetLeader()->guid  == _player->GetGUID())
		{
			MsgModifyTitleAck.Type = 0;
			pGroup->m_GroupTitile = MsgRe.title;
		}
		else
		{
			MsgModifyTitleAck.Type = 2;
		}
	}

	if (_player->GetSession())
	{
		_player->GetSession()->SendPacket(MsgModifyTitleAck);
	}
}

void WorldSession::HandleGroupListReqOpcode(CPacketUsn& packet)
{
	if (!_player)
	{
		return;
	}

	MSG_C2S::stGroupListReq MsgRec;
	packet >> MsgRec;
	if (_player->GetSession())
	{
		Group* pGroup = NULL;
		MSG_S2C::stGroupListAck MsgAck;

		ObjectMgr::GroupMap::iterator it = objmgr.GroupMapBegin();
		ObjectMgr::GroupMap::iterator itEnd = objmgr.GroupMapEnd();
		for (; it != itEnd; it ++)
		{
	
			MSG_S2C::stGroupListAck::GroupFindInfo info;
			pGroup = it->second;
			if (pGroup)
			{
				PlayerInfo* pLeader = pGroup->GetLeader();
				if (pLeader)
				{
					info.name = pLeader->name;
					info.guid = pLeader->guid;
					info.bOpen = pGroup->m_bCanApplyForJoining;
					info.title = pGroup->m_GroupTitile;
					info.Level = pLeader->lastLevel;
					if (pLeader->m_loggedInPlayer)
					{
						if (MsgRec.id)
						{
							if (pLeader->m_loggedInPlayer->GetMapMgr()&&
								pLeader->m_loggedInPlayer->GetMapMgr()->GetBaseMap()&&
								pLeader->m_loggedInPlayer->GetMapMgr()->GetBaseMap()->GetMapId() != MsgRec.id)
							{
								continue;
							}
							
						}

					}

					info.MaxCount = pGroup->GetMaxMemberCount();
					info.CurCount = pGroup->GetCurrentMemberCount();
					MsgAck.vcGroup.push_back(info);
				}
			}

		}
		_player->GetSession()->SendPacket(MsgAck);
	}

}

void WorldSession::HandleGroupModifyCanApplyForJoiningOpcode(CPacketUsn& packet)
{
	if (!_player)
	{
		return;
	}

	MSG_C2S::stGroupModifyCanApplyForJoining MsgRec;
	packet >> MsgRec;
	MSG_S2C::stGroupModifyCanApplyForJoiningAck MsgAck;
	Group* pGroup = _player->GetGroup();
	if (!pGroup)
	{
		MsgAck.Type = 2;
	}
	else
	{
		if (pGroup->GetLeader()->guid == _player->GetGUID())
		{
			pGroup->m_bCanApplyForJoining = !pGroup->m_bCanApplyForJoining;
			if (pGroup->m_bCanApplyForJoining)
			{
				MsgAck.Type = 0;
			}
			else
			{
				MsgAck.Type = 1;
			}

		}
		else
		{
			MsgAck.Type = 3;
		}
	}

	if (_player->GetSession())
	{
		_player->GetSession()->SendPacket(MsgAck);
	}

}

void WorldSession::HandleGroupApplyForJoiningAckOpcode(CPacketUsn& packet)
{
	MSG_C2S::stGroupApplyForJoiningAck Msg;
	packet >> Msg;
	if (_player)
	{
		PlayerInfo* pinfo = objmgr.GetPlayerInfoByName(Msg.name.c_str());
		Player *player = pinfo->m_loggedInPlayer;
		if (!player)
		{
			return;
		}
		MSG_S2C::stGroupApplyForJoiningResult MsgResult;
		MsgResult.name = player->GetName();
		if (player->GetGroup())
		{
			MsgResult.Type = 1;
			if (_player->GetSession())
			{
				_player->GetSession()->SendPacket(MsgResult);
			}
			return;
		}
		if (Msg.Type)
		{
			Group* pGroup = _player->GetGroup();
			if (pGroup)
			{
				if (pGroup->GetLeader() && _player->GetGUID() == pGroup->GetLeader()->guid)
				{
					if (!pGroup->IsFull())
					{
						MsgResult.Type = 0;
						pGroup->AddMember(player->m_playerInfo);
					}
					else
					{
						MsgResult.Type = 2;
					}
				}
				else
				{
					MsgResult.Type = 3;
				}
			}
			else
			{
				MsgResult.Type = 0;
				MSG_S2C::stMsg_Dungeon_Difficulty MsgDiffculty;
				pGroup = new Group(true);
				pGroup->m_pNewPlayer = NULL;
				pGroup->AddMember(_player->m_playerInfo);		// add the inviter first, therefore he is the leader
				pGroup->AddMember(player->m_playerInfo);	   // add us.
				_player->iInstanceType = player->iInstanceType;
				MsgDiffculty.instanceType = _player->iInstanceType;
				_player->GetSession()->SendPacket( MsgDiffculty );
				sInstanceMgr.PlayerNewGroup( player->m_playerInfo->guid, pGroup->GetID() );
			}

			_player->GetSession()->SendPacket(MsgResult);

		}
		else
		{
			MSG_S2C::stGroupApplyForJoiningAck MsgApplyForJoinAck;
			MsgApplyForJoinAck.name = _player->GetName();
			MsgApplyForJoinAck.type = 2;
			if (player->GetSession())
			{
				player->GetSession()->SendPacket(MsgApplyForJoinAck);
			}
		}
	}

}
void WorldSession::HandleGroupApplyForJoining(CPacketUsn& packet)
{
	MSG_C2S::stGroupApplyForJoining req;
	packet >> req;
	if (_player)
	{
		Player* pLeader = NULL;
		Group* pGroup =  _player->GetGroup();
		if (pGroup)
		{
			return;
		}

		Player *player = objmgr.GetPlayer(req.guid);
		if (!player)
		{
			return;
		}
		pGroup = player->GetGroup();

		if (pGroup)
		{
			if (pGroup->IsFull())
			{
				PlayerInfo* playInfo = pGroup->GetLeader();
				MSG_S2C::stGroupApplyForJoiningAck Msg;
				Msg.name = playInfo->name;
				Msg.type = 1;
				SendPacket(Msg);
				return;
			}

			if (!pGroup->m_bCanApplyForJoining)
			{
				PlayerInfo* playInfo = pGroup->GetLeader();
				MSG_S2C::stGroupApplyForJoiningAck Msg;
				Msg.name = playInfo->name;
				Msg.type = 2;
				SendPacket(Msg);
				return;
			}

		   pLeader = pGroup->GetLeader()->m_loggedInPlayer;
		}
		else
		{
			pLeader = player;
		}

		if (pLeader && pLeader->GetSession())
		{
			MSG_S2C::stGroupApplyForJoining MsgApplyForJoining;
			MsgApplyForJoining.name = _player->GetName();
			MsgApplyForJoining.Level = _player->getLevel();
			MsgApplyForJoining.Sex = _player->getGender();
			MsgApplyForJoining.classMask = _player->getClass();
			pLeader->GetSession()->SendPacket(MsgApplyForJoining);
		}
	}
}

void WorldSession::HandleGroupApplyForJoiningStateOpcode(CPacketUsn& packet)
{

	if (_player&&_player->GetGroup())
	{
		MSG_S2C::stGroupApplyForJoiningStateAck Msg;
		Msg.bOpen = _player->GetGroup()->m_bCanApplyForJoining;
		if (_player->GetSession())
		{
			_player->GetSession()->SendPacket(Msg);
		}
	}
}


void WorldSession::HandleSetPlayerIconOpcode(CPacketUsn& packet)
{
	Group * pGroup = _player->GetGroup();
	if(!_player->IsInWorld() || !pGroup) return;

	MSG_C2S::stGroupSetPlayerIcon MsgRecv;packet >> MsgRecv;
	MSG_S2C::stGroupSetPlayerIcon Msg;
	Msg.icon = MsgRecv.icon;
	Msg.guid = MsgRecv.guid;
	if(MsgRecv.icon == 0xFF)
	{
		// client request
		for(uint8 i = 0; i < 8; ++i)
			Msg.m_targetIcons[i] = pGroup->m_targetIcons[i];

		SendPacket(Msg);
	}
	else if(_player->IsGroupLeader())
	{

		if(MsgRecv.icon > 7)
		{
			;
		}
		else
		{
			for (int i = 0; i < 8; i ++)
			{
				if (pGroup->m_targetIcons[i] == MsgRecv.guid)
				{
					pGroup->m_targetIcons[i] = 0;
					Msg.m_targetIcons[i] = 0;
					Msg.m_targetUniqueID[i] = 0;
				}
				else
				{
					Msg.m_targetIcons[i] = pGroup->m_targetIcons[i];
					Msg.m_targetUniqueID[i] = pGroup->m_targetUniqueID[i];
				}
				
			}
			pGroup->m_targetIcons[MsgRecv.icon] = MsgRecv.guid;
			Object* p = _player->GetMapMgr()->_GetObject( MsgRecv.guid );
			if( p )
				pGroup->m_targetUniqueID[MsgRecv.icon] = p->GetUniqueIDForLua();
			else
				pGroup->m_targetUniqueID[MsgRecv.icon] = 0;

			Msg.m_targetIcons[MsgRecv.icon] = MsgRecv.guid;
			Msg.m_targetUniqueID[MsgRecv.icon] = pGroup->m_targetUniqueID[MsgRecv.icon];
		}// whhopes,buffer overflow :p
		// setting icon
		pGroup->SendPacketToAll(Msg);
	}
}

void WorldSession::SendPartyCommandResult(Player *pPlayer, uint32 p1, const std::string& name, uint32 err)
{
	if(!_player->IsInWorld()) return;
	// if error message do not work, please sniff it and leave me a message
	if(pPlayer)
	{
		MSG_S2C::stParty_Command_Result Msg;
		Msg.op = p1;
		Msg.member = name;
		Msg.res = err;
		pPlayer->GetSession()->SendPacket(Msg);
	}
}
