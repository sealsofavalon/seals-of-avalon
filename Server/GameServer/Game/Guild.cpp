#include "StdAfx.h"
#include "../../../SDBase/Protocol/S2C_Group.h"
#include "../../../SDBase/Protocol/S2C_Chat.h"
#include "MultiLanguageStringMgr.h"

std::set<Guild*> Guild::s_setGuilds;

Guild::Guild()
{
	m_commandLogging=true;
	m_guildId=0;
	m_guildLeader=0;
	m_guildName= NULL;//(char*)"Goose";
	m_guildInfo=NULL;
	m_motd=NULL;
	m_backgroundColor=0;
	m_emblemColor=0;
	m_emblemStyle=0;
	m_borderColor=0;
	m_borderStyle=0;
	m_creationTimeStamp=0;
	m_bankBalance =0;
	m_bankTabCount=0;
	creationDay=creationMonth=creationYear=0;
	m_hiLogId=1;
	m_level = 1;
	m_score = 0;
	m_hostileGuildid = 0;
	memset(m_ranks, 0, sizeof(GuildRank*)*MAX_GUILD_RANKS);
	m_allianceid = 0;
	m_ready_ally_guild_id = 0;
	s_setGuilds.insert( this );
	m_castle_own = 0;
	m_hiddenScore = 0;
	m_NoWarBeginTime = 0;

}

Guild::~Guild()
{
	for(GuildMemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
	{
		PlayerInfo* pi = itr->first;
		if(itr->second->szOfficerNote)
			free((void*)itr->second->szOfficerNote);
		if(itr->second->szPublicNote)
			free((void*)itr->second->szPublicNote);
		if( pi->m_loggedInPlayer )
			pi->m_loggedInPlayer->SetHostileGuild( 0 );
		delete itr->second;
	}

	for(uint32 i = 0; i < MAX_GUILD_RANKS; ++i)
	{
		if(m_ranks[i] != NULL)
		{
			free(m_ranks[i]->szRankName);
			delete m_ranks[i];
		}
	}

	for(GuildLogList::iterator itr = m_log.begin(); itr != m_log.end(); ++itr)
	{
		delete (*itr);
	}

	for(GuildBankTabVector::iterator itr = m_bankTabs.begin(); itr != m_bankTabs.end(); ++itr)
	{
		for(uint32 i = 0; i < MAX_GUILD_BANK_SLOTS; ++i)
			if((*itr)->pSlots[i] != NULL)
				delete (*itr)->pSlots[i];

		for(list<GuildBankEvent*>::iterator it2 = (*itr)->lLog.begin(); it2 != (*itr)->lLog.end(); ++it2)
			delete (*it2);

		delete (*itr);
	}

	for(list<GuildBankEvent*>::iterator it2 = m_moneyLog.begin(); it2 != m_moneyLog.end(); ++it2)
		delete (*it2);

	if( m_guildName )
		free(m_guildName);
	s_setGuilds.erase( this );
}

void Guild::SendGuildCommandResult(WorldSession * pClient, uint32 iCmd, const char * szMsg, uint32 iType)
{
	return;
	/* client does not process
	MSG_S2C::stGuild_Command_Result Msg;
	Msg.iCmd = iCmd;
	Msg.szMsg = szMsg;
	Msg.iType = iType;
	pClient->SendPacket(Msg);
	*/
}

GuildRank * Guild::FindLowestRank()
{
	for(uint32 i = MAX_GUILD_RANKS-1; i > 0; --i)
	{
		if(m_ranks[i] != NULL)
			return m_ranks[i];
	}

	return NULL;
}

GuildRank * Guild::FindHighestRank()
{
	for(uint32 i = 1; i < MAX_GUILD_RANKS; ++i)
	{
		if(m_ranks[i] != NULL)
			return m_ranks[i];
	}

	return NULL;
}

bool GuildRank::CanPerformCommand(uint32 t)
{
	return ((iRights & t) >0 ? true : false);
}

bool GuildRank::CanPerformBankCommand(uint32 t, uint32 tab)
{
	return ((iTabPermissions[tab].iFlags & t) >0 ? true : false);
}

void Guild::LogGuildEvent(uint8 iEvent, uint8 iStringCount, ...)
{
	if(!m_commandLogging)
		return;

	va_list ap;
	char * strs[4] = {NULL,NULL,NULL,NULL};

	va_start(ap, iStringCount);
	ASSERT(iStringCount <= 4);

	MSG_S2C::stGuild_Event Msg;
	Msg.guild_event = iEvent;

	for(ui8 i = 0; i < iStringCount; ++i)
	{
		strs[i] = va_arg(ap, char*);
		Msg.vStrs.push_back(strs[i]);
	}

	va_end(ap);
	SendPacket(Msg);
}

void Guild::AddGuildLogEntry(uint8 iEvent, uint8 iParamCount, ...)
{
	if(!m_commandLogging)
		return;

	va_list ap;
	uint32 i;
	GuildLogEvent * ev;

	va_start(ap, iParamCount);
	ASSERT(iParamCount<=3);

	ev = new GuildLogEvent;
	ev->iLogId = GenerateGuildLogEventId();
	ev->iEvent = iEvent;
	ev->iTimeStamp = (uint32)UNIXTIME;

	for(i = 0; i < iParamCount; ++i)
		ev->iEventData[i] = va_arg(ap, uint32);

	for(; i < 3; ++i)
		ev->iEventData[i] = 0;

	sWorld.ExecuteSqlToDBServer("INSERT INTO guild_logs VALUES(%u, %u, %u, %u, %u, %u, %u)",
		ev->iLogId, m_guildId, ev->iTimeStamp, ev->iEvent, ev->iEventData[0], ev->iEventData[1], ev->iEventData[2]);

	if(m_log.size() >= 25)
	{
		// limit it to 250 events.
		// delete the first (oldest) event.
		sWorld.ExecuteSqlToDBServer("DELETE FROM guild_logs WHERE log_id = %u AND guildid = %u", (*(m_log.begin()))->iLogId, m_guildId);
		delete *(m_log.begin());
		m_log.pop_front();
	}

	m_log.push_back(ev);
}

void Guild::SendPacket(PakHead& packet)
{
	for(GuildMemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
	{
		Player* p = itr->first->m_loggedInPlayer;
		if( p && p->GetSession())
			p->GetSession()->SendPacket(packet);
	}
}

GuildRank * Guild::CreateGuildRank(const char * szRankName, uint32 iPermissions, bool bFullGuildBankPermissions)
{
	// find a free id
	uint32 i, j;
	for(i = 0; i < MAX_GUILD_RANKS; ++i)
	{
		if(m_ranks[i] == NULL)
		{
			GuildRank * r = new GuildRank;
			memset(r, 0, sizeof(GuildRank));
			r->iId = i;
			r->iRights=iPermissions;
			r->szRankName = strdup(szRankName);
			r->iGoldLimitPerDay = bFullGuildBankPermissions ? -1 : 0;

			if(bFullGuildBankPermissions)
			{
				for(j = 0; j < MAX_GUILD_BANK_TABS; ++j)
				{
					r->iTabPermissions[j].iFlags = 3;			// this is both use tab and withdraw
					r->iTabPermissions[j].iStacksPerDay = -1;	// -1 = unlimited
				}
			}
			m_ranks[i] = r;

			// save the rank into the database
			sWorld.ExecuteSqlToDBServer("INSERT INTO guild_ranks VALUES(%u, %u, \"%s\", %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)",
				m_guildId, i, CharacterDatabase.EscapeString(string(szRankName)).c_str(),
				r->iRights, r->iGoldLimitPerDay,
				r->iTabPermissions[0].iFlags, r->iTabPermissions[0].iStacksPerDay,
				r->iTabPermissions[1].iFlags, r->iTabPermissions[1].iStacksPerDay,
				r->iTabPermissions[2].iFlags, r->iTabPermissions[2].iStacksPerDay,
				r->iTabPermissions[3].iFlags, r->iTabPermissions[3].iStacksPerDay,
				r->iTabPermissions[4].iFlags, r->iTabPermissions[4].iStacksPerDay,
				r->iTabPermissions[5].iFlags, r->iTabPermissions[5].iStacksPerDay);

			MyLog::log->debug("Guild: Created rank %u on guild %u (%s)", i, m_guildId, szRankName);

			return r;
		}
	}
	return NULL;
}

void Guild::CreateFromPlayer(PlayerInfo* pCreator, const char* guildname)
{
	m_guildId = objmgr.GenerateGuildId();
	m_guildName = strdup(guildname);
	m_guildLeader = pCreator->guid;
	m_creationTimeStamp = (uint32)UNIXTIME;

	// create the guild in the database
	CreateInDB();

	// rest of the fields have been nulled out, create some default ranks.
	GuildRank * leaderRank = CreateGuildRank( build_language_string( BuildString_GuildRankName0 ), GR_RIGHT_ALL, true);
	CreateGuildRank( build_language_string( BuildString_GuildRankName1 ), GR_RIGHT_ALL, true);
	CreateGuildRank( build_language_string( BuildString_GuildRankName2 ), GR_RIGHT_CAPTAIN, false);
	CreateGuildRank( build_language_string( BuildString_GuildRankName3 ), GR_RIGHT_DEFAULT, false);
	
	// turn off command logging, we don't wanna spam the logs
	m_commandLogging = false;

	// add the leader to the guilds
	AddGuildMember(pCreator, NULL, leaderRank->iId);

	// re-enable command logging
	m_commandLogging = true;

	objmgr.AddGuild( this );
}

// creating from a charter
void Guild::CreateFromCharter(Charter * pCharter, WorldSession * pTurnIn)
{
	uint32 i;

	m_guildId = objmgr.GenerateGuildId();
	m_guildName = strdup(pCharter->GuildName.c_str());
	m_guildLeader = pCharter->LeaderGuid;
	m_creationTimeStamp = (uint32)UNIXTIME;

	// create the guild in the database
	CreateInDB();

	// rest of the fields have been nulled out, create some default ranks.
	GuildRank * leaderRank = CreateGuildRank( build_language_string( BuildString_GuildRankName0 ), GR_RIGHT_ALL, true);
	CreateGuildRank( build_language_string( BuildString_GuildRankName1 ), GR_RIGHT_ALL, true);
	CreateGuildRank( build_language_string( BuildString_GuildRankName2 ), GR_RIGHT_CAPTAIN, false);
	CreateGuildRank( build_language_string( BuildString_GuildRankName3 ), GR_RIGHT_DEFAULT, false);
	GuildRank * defRank = CreateGuildRank( build_language_string( BuildString_GuildRankName3 ), GR_RIGHT_DEFAULT, false);

	// turn off command logging, we don't wanna spam the logs
	m_commandLogging = false;

	// add the leader to the guild
	AddGuildMember(pTurnIn->GetPlayer()->m_playerInfo, NULL, leaderRank->iId);

	// add all the other people
	for(i = 0; i < pCharter->SignatureCount; ++i)
	{
		PlayerInfo * pi = objmgr.GetPlayerInfo(pCharter->Signatures[i]);
		if(pi)
			AddGuildMember(pi, NULL, defRank->iId);
	}

	// re-enable command logging
	m_commandLogging = true;

}

void Guild::PromoteGuildMember(PlayerInfo * pMember, WorldSession * pClient)
{
	/*
	if(pClient->GetPlayer()->m_playerInfo->guild != this || pMember->guild != this)
		return;

	if(!pClient->GetPlayer()->m_playerInfo->guildRank->CanPerformCommand(GR_RIGHT_PROMOTE))
	{
		SendGuildCommandResult(pClient, GUILD_SET_RANK_S, "", GUILD_PERMISSIONS);
		return;
	}

	// need a proper command result for this
	if(pMember->guildRank->iId == 1)
	{
		pClient->SystemMessage("You cannot promote this member any further.");
		return;
	}

	// find the lowest rank that isnt his rank
	int32 nh = pMember->guildRank->iId - 1;
	GuildRank * newRank = NULL;

	while(nh > 0 && newRank == NULL)
	{
		newRank = m_ranks[nh];
		nh--;
	}

	if(newRank==NULL)
	{
		pClient->SystemMessage("Could not find a rank to promote this member to.");
		return;
	}

	GuildMemberMap::iterator itr = m_members.find(pMember);
	if(itr == m_members.end())
	{
		// shouldnt happen
		return;
	}

	itr->second->pRank = newRank;
	itr->second->pPlayer->guildRank = newRank;

	// log it
	LogGuildEvent(GUILD_EVENT_PROMOTION, 3, pClient->GetPlayer()->GetName(), pMember->name, newRank->szRankName);
	AddGuildLogEntry(GUILD_LOG_EVENT_PROMOTION, 2, pClient->GetPlayer()->GetLowGUID(), pMember->guid, newRank->iId);

	// update in the database
	sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET guildRank = %u WHERE playerid = %u AND guildid = %u", newRank->iId, pMember->guid, m_guildId);

	// if the player is online, update his guildrank
	if(pMember->m_loggedInPlayer)
		pMember->m_loggedInPlayer->SetGuildRank(newRank->iId);

	// release lock
	*/
}

void Guild::DemoteGuildMember(PlayerInfo * pMember, WorldSession * pClient)
{
	/*
	if(pClient->GetPlayer()->m_playerInfo->guild != this || pMember->guild != this)
		return;

	if(!pClient->GetPlayer()->m_playerInfo->guildRank->CanPerformCommand(GR_RIGHT_DEMOTE) ||
		pMember->guid == GetGuildLeader())
	{
		SendGuildCommandResult(pClient, GUILD_SET_RANK_S, "", GUILD_PERMISSIONS);
		return;
	}

	// find the next highest rank
	uint32 nh = pMember->guildRank->iId + 1;
	GuildRank * newRank = NULL;

	while(nh < 10 && newRank == NULL)
	{
		newRank = m_ranks[nh];
		++nh;
	}

	if(newRank==NULL)
	{
		pClient->SystemMessage("Could not find a rank to demote this member to.");
		return;
	}

	GuildMemberMap::iterator itr = m_members.find(pMember);
	if(itr == m_members.end())
	{
		// shouldnt happen
		return;
	}

	itr->second->pRank = newRank;
	itr->second->pPlayer->guildRank = newRank;

	// log it
	LogGuildEvent(GUILD_EVENT_DEMOTION, 3, pClient->GetPlayer()->GetName(), pMember->name, newRank->szRankName);
	AddGuildLogEntry(GUILD_LOG_EVENT_DEMOTION, 2, pClient->GetPlayer()->GetLowGUID(), pMember->guid, newRank->iId);

	// update in the database
	sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET guildRank = %u WHERE playerid = %u AND guildid = %u", newRank->iId, pMember->guid, m_guildId);

	// if the player is online, update his guildrank
	if(pMember->m_loggedInPlayer)
		pMember->m_loggedInPlayer->SetGuildRank(newRank->iId);

	// release lock
	*/
}

void Guild::SetGuildMemberRank( PlayerInfo* pMember, WorldSession* pClient, uint8 rank )
{
	if( rank > 4 || rank == 0 ) return;
	
	PlayerInfo* pi = pClient->GetPlayer()->m_playerInfo;
	if( !pi ) return;

	if( pi->guild != this || pMember->guild != this)
		return;

	if(!pi->guildRank->CanPerformCommand(GR_RIGHT_SET_RANK) ||
		pMember->guid == GetGuildLeader())
	{
		//SendGuildCommandResult(pClient, GUILD_SET_RANK_S, "", GUILD_PERMISSIONS);
		pClient->SendNotification( build_language_string( BuildString_GuildSetRankPermission, pMember->name ) );//"你没有权限设置[%s]的等级", pMember->name );
		return;
	}
	GuildRank* newRank = m_ranks[rank];
	if( !newRank )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildRankNotFound ) );//"没有找到该阶位" );
		return;
	}

	if( (int)rank - (int)pi->guildRank->iId < 1 || pMember->guildRank->iId <= pi->guildRank->iId )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildSetRankPermission, pMember->name ) );//"你没有权限设置[%s]的等级", pMember->name );
		return;
	}

	GuildMemberMap::iterator itr = m_members.find(pMember);
	if(itr == m_members.end())
	{
		// shouldnt happen
		return;
	}

	itr->second->pRank = newRank;
	itr->second->pPlayer->guildRank = newRank;

	// log it
	LogGuildEvent(GUILD_EVENT_SETRANK, 3, pClient->GetPlayer()->GetName(), pMember->name, newRank->szRankName);
	AddGuildLogEntry(GUILD_LOG_EVENT_SETRANK, 2, pClient->GetPlayer()->GetLowGUID(), pMember->guid, newRank->iId);

	// update in the database
	sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET guildRank = %u WHERE playerid = %u AND guildid = %u", newRank->iId, pMember->guid, m_guildId);

	// if the player is online, update his guildrank
	if(pMember->m_loggedInPlayer)
		pMember->m_loggedInPlayer->SetGuildRank(newRank->iId);

}

void Guild::AddMemberContributionPoints( PlayerInfo* pMember, uint32 points )
{
	GuildMemberMap::iterator itr = m_members.find(pMember);
	if(itr == m_members.end())
	{
		// shouldnt happen
		return;
	}
	itr->second->contribution_points += points;
	sWorld.ExecuteSqlToDBServer( "update guild_data set contribution_points = %u where playerid = %u and guildid = %u", itr->second->contribution_points, pMember->guid, m_guildId );

	Player* m_plr = pMember->m_loggedInPlayer;
	if(itr->second->contribution_points >= 1000)
	{
		if(m_plr)
		if(m_plr->m_Titles.find(16) != m_plr->m_Titles.end())
		{
			m_plr->InsertTitle(16);
			MSG_S2C::stTitleAdd msg;
			msg.title.title = 16;
			m_plr->GetSession()->SendPacket(msg);
		}
	}
	else if(itr->second->contribution_points >= 10000)
	{
		if(m_plr)
		if(m_plr->m_Titles.find(17) != m_plr->m_Titles.end())
		{
			m_plr->InsertTitle(17, 0);
			MSG_S2C::stTitleAdd msg;
			msg.title.title = 17;
			m_plr->GetSession()->SendPacket(msg);
		}
	}

	MSG_S2C::stGuildContributionPointsNotify notify;
	notify.player_id = pMember->guid;
	notify.points = itr->second->contribution_points;
	notify.guild_score = m_score;
	SendPacket( notify );
}

void Guild::SpendMemberContributionPoints( PlayerInfo* pMember, uint32 points )
{
	GuildMemberMap::iterator itr = m_members.find(pMember);
	if(itr == m_members.end())
	{
		// shouldnt happen
		return;
	}
	if(itr->second->contribution_points < points)
		itr->second->contribution_points = 0;
	else
		itr->second->contribution_points -= points;
	sWorld.ExecuteSqlToDBServer( "update guild_data set contribution_points = %u where playerid = %u and guildid = %u", itr->second->contribution_points, pMember->guid, m_guildId );

	MSG_S2C::stGuildContributionPointsNotify notify;
	notify.player_id = pMember->guid;
	notify.points = itr->second->contribution_points;
	notify.guild_score = m_score;
	SendPacket( notify );
}

uint32 Guild::GetMemberContributionPoints( PlayerInfo* pMember )
{
	GuildMemberMap::iterator itr = m_members.find(pMember);
	if(itr == m_members.end())
	{
		// shouldnt happen
		return 0;
	}
	return itr->second->contribution_points;
}

bool Guild::LoadFromDB(Field * f)
{
	m_guildId = f[0].GetUInt32();
	m_guildName = strdup(f[1].GetString());
	m_guildLeader = f[2].GetUInt32();
	m_emblemStyle = f[3].GetUInt32();
	m_emblemColor = f[4].GetUInt32();
	m_borderStyle = f[5].GetUInt32();
	m_borderColor = f[6].GetUInt32();
	m_backgroundColor = f[7].GetUInt32();
	m_guildInfo = strlen(f[8].GetString()) ? strdup(f[8].GetString()) : NULL;
	m_motd = strlen(f[9].GetString()) ? strdup(f[9].GetString()) : NULL;
	m_creationTimeStamp = f[10].GetUInt32();
	m_bankTabCount = f[11].GetUInt32();
	m_bankBalance = f[12].GetUInt32();
	m_level = f[13].GetUInt32();
	switch(m_level)
	{
	case 1:
		{
			m_MaxMemebrCount = 30;
		}
		break;
	case 2:
		{
			m_MaxMemebrCount = 100;
		}
		break;
	case 3:
		{
			m_MaxMemebrCount = 300;
		}
		break;
	}
	m_score = f[14].GetUInt32();
	m_hiddenScore = f[15].GetUInt32();
	m_NoWarBeginTime = f[16].GetUInt32();

	// load ranks
	uint32 j;
	QueryResult * result = CharacterDatabase.Query("SELECT * FROM guild_ranks WHERE guildId = %u ORDER BY rankId ASC", m_guildId);
	if(result==NULL)
		return false;

	uint32 sid = 0;

	do 
	{
		GuildRank * r = new GuildRank;
		Field * f2 = result->Fetch();
		r->iId = f2[1].GetUInt32();
		if(r->iId!=sid)
		{
			MyLog::log->notice("Guild: Renaming rank %u of guild %s to %u.", r->iId, m_guildName, sid);
			sWorld.ExecuteSqlToDBServer("UPDATE guild_ranks SET rankId = %u WHERE guildId = %u AND rankName = \"%s\"", r->iId,
				m_guildId, CharacterDatabase.EscapeString(string(f2[2].GetString())).c_str());

			r->iId=sid;
		}
		sid++;
		r->szRankName = strdup(f2[2].GetString());
		r->iRights = f2[3].GetUInt32();
		r->iGoldLimitPerDay = f2[4].GetUInt32();
		
		for(j = 0; j < MAX_GUILD_BANK_TABS; ++j)
		{
			r->iTabPermissions[j].iFlags = f2[5+(j*2)].GetUInt32();
			r->iTabPermissions[j].iStacksPerDay = f2[6+(j*2)].GetUInt32();
		}

		//m_ranks.push_back(r);
		ASSERT(m_ranks[r->iId] == NULL);
		m_ranks[r->iId] = r;

	} while(result->NextRow());
	delete result;

	// load members
	result = CharacterDatabase.Query("SELECT * FROM guild_data WHERE guildid = %u", m_guildId);
	if(result==NULL)
		return false;

	do 
	{
		Field * f3 = result->Fetch();
		GuildMember * gm = new GuildMember;
		gm->pPlayer = objmgr.GetPlayerInfo(f3[1].GetUInt32());
		if(gm->pPlayer == NULL)
		{
			delete gm;
			continue;
		}

		if(f3[2].GetUInt32() >= MAX_GUILD_RANKS || m_ranks[f3[2].GetUInt32()] == NULL) 
		{
			delete gm;
			continue;
		}

		gm->pRank = m_ranks[f3[2].GetUInt32()];
		if(gm->pRank==NULL)
			gm->pRank=FindLowestRank();
		gm->pPlayer->guild=this;
		gm->pPlayer->guildRank=gm->pRank;
		gm->pPlayer->guildMember=gm;

		if(strlen(f3[3].GetString()))
			gm->szPublicNote = strdup(f3[3].GetString());
		else
			gm->szPublicNote = NULL;

		if(strlen(f3[4].GetString()))
			gm->szOfficerNote = strdup(f3[4].GetString());
		else
			gm->szOfficerNote = NULL;

		gm->uLastWithdrawReset = f3[5].GetUInt32();
		gm->uWithdrawlsSinceLastReset = f3[6].GetUInt32();
		for(j = 0; j < MAX_GUILD_BANK_TABS; ++j)
		{
			gm->uLastItemWithdrawReset[j] = f3[7+(j*2)].GetUInt32();
			gm->uItemWithdrawlsSinceLastReset[j] = f3[8+(j*2)].GetUInt32();
		}
		gm->contribution_points = f3[19].GetUInt32();

		m_members.insert(make_pair(gm->pPlayer, gm));

	} while(result->NextRow());
	delete result;

	result = CharacterDatabase.Query("SELECT MAX(log_id) FROM guild_logs WHERE guildid = %u", m_guildId);
	m_hiLogId = 1;
	if(result != NULL)
	{
		m_hiLogId = result->Fetch()[0].GetUInt32() + 1;
		delete result;
	}

	result = CharacterDatabase.Query("SELECT MAX(log_id) FROM guild_banklogs WHERE guildid = %u", m_guildId);
	if(result)
	{
		if((result->Fetch()[0].GetUInt32() + 1) > m_hiLogId)
			m_hiLogId = result->Fetch()[0].GetUInt32() + 1;
		delete result;
	}

	// load log
	result = CharacterDatabase.Query("SELECT * FROM guild_logs WHERE guildid = %u ORDER BY timestamp ASC", m_guildId);
	if(result)
	{
		do 
		{
			GuildLogEvent * li = new GuildLogEvent;
			li->iLogId = result->Fetch()[0].GetUInt32();
			li->iEvent = result->Fetch()[3].GetUInt32();
			li->iTimeStamp = result->Fetch()[2].GetUInt32();
			li->iEventData[0] = result->Fetch()[4].GetUInt32();
			li->iEventData[1] = result->Fetch()[5].GetUInt32();
			li->iEventData[2] = result->Fetch()[6].GetUInt32();
			m_log.push_back(li);
		} while(result->NextRow());
		delete result;
	}

	result = CharacterDatabase.Query("SELECT * FROM guild_banktabs WHERE guildId = %u ORDER BY tabId ASC", m_guildId);
	sid = 0;
	if(result)
	{
		do 
		{
			if((sid++) != result->Fetch()[1].GetUInt32())
			{
#ifdef WIN32
				MessageBox(0, "Guild bank tabs are out of order!", "Internal error", MB_OK);
				TerminateProcess(GetCurrentProcess(), 0);
				return false;
#else
				printf("Guild bank tabs are out of order!\n");
				exit(0);
#endif
			}

			QueryResult * res2 = CharacterDatabase.Query("SELECT * FROM guild_bankitems WHERE guildId = %u AND tabId = %u", m_guildId, result->Fetch()[1].GetUInt32());
			GuildBankTab * pTab = new GuildBankTab;
			pTab->iTabId = (uint8)result->Fetch()[1].GetUInt32();
			pTab->szTabName = (strlen(result->Fetch()[2].GetString()) > 0) ? strdup(result->Fetch()[2].GetString()) : NULL;
			pTab->szTabIcon = (strlen(result->Fetch()[3].GetString()) > 0) ? strdup(result->Fetch()[3].GetString()) : NULL;
			
			memset(pTab->pSlots, 0, sizeof(Item*) * MAX_GUILD_BANK_SLOTS);

			if(res2)
			{
				do 
				{
					Item * pItem = objmgr.LoadItem(res2->Fetch()[3].GetUInt64());
					if(pItem == NULL)
					{
						sWorld.ExecuteSqlToDBServer("DELETE FROM guild_bankitems WHERE itemGuid = %u AND guildId = %u AND tabId = %u", res2->Fetch()[3].GetUInt32(), m_guildId, (uint32)pTab->iTabId);
						continue;
					}

					pTab->pSlots[res2->Fetch()[2].GetUInt32()] = pItem;

				} while (res2->NextRow());
				delete res2;
			}

			res2 = CharacterDatabase.Query("SELECT * FROM guild_banklogs WHERE guildid = %u AND tabid = %u ORDER BY timestamp ASC", m_guildId, result->Fetch()[1].GetUInt32());
			if(res2 != NULL)
			{
				do 
				{
					GuildBankEvent * ev= new GuildBankEvent;
					ev->iLogId=res2->Fetch()[0].GetUInt32();
					ev->iAction=res2->Fetch()[3].GetUInt8();
					ev->uPlayer=res2->Fetch()[4].GetUInt32();
					ev->uEntry=res2->Fetch()[5].GetUInt32();
					ev->iStack=res2->Fetch()[6].GetUInt8();
					ev->uTimeStamp=res2->Fetch()[7].GetUInt32();

					pTab->lLog.push_back(ev);

				} while (res2->NextRow());
				delete res2;
			}

			m_bankTabs.push_back(pTab);

		} while (result->NextRow());
		delete result;
	}

	result = CharacterDatabase.Query("SELECT * FROM guild_banklogs WHERE guildid = %u AND tabid = 6 ORDER BY timestamp ASC", m_guildId);
	if(result != NULL)
	{
		do 
		{
			GuildBankEvent * ev= new GuildBankEvent;
			ev->iLogId=result->Fetch()[0].GetUInt32();
			ev->iAction=result->Fetch()[3].GetUInt8();
			ev->uPlayer=result->Fetch()[4].GetUInt32();
			ev->uEntry=result->Fetch()[5].GetUInt32();
			ev->iStack=result->Fetch()[6].GetUInt8();
			ev->uTimeStamp=result->Fetch()[7].GetUInt32();

			m_moneyLog.push_back(ev);

		} while (result->NextRow());
		delete result;
	}

	result = CharacterDatabase.Query( "select alliance_id from guild_alliance where guild_id = %u", m_guildId );
	if( result )
	{
		Field* f = result->Fetch();
		m_allianceid = f[0].GetUInt32();
		delete result;

		result = CharacterDatabase.Query( "select guild_id, is_owner from guild_alliance where alliance_id = %u", m_allianceid );
		if( result )
		{
			do
			{
				f = result->Fetch();
				uint32 gid = f[0].GetUInt32();
				if( gid != m_guildId )
				{
					m_alliance_guilds[gid] = ( f[1].GetUInt32() != 0 );
				}
			}
			while( result->NextRow() );
			delete result;
		}
	}
	
	MyLog::log->debug("Guild: Loaded guild %s, %u members.", m_guildName, m_members.size());

	if (m_NoWarBeginTime)
	{
		uint64 time = UNIXTIME - m_NoWarBeginTime;
		

		

		if (time >= 3 * 24 * 3600 * 1000)
		{
			EndNoWar();
		}
		else
		{
			time = 3 * 24 * 3600 * 1000 - time;
			sEventMgr.AddEvent( this, &Guild::EndNoWar, EVENT_GUILD_REMOVE_NO_WAR, time, 1, 0 );
		}

	}


	return true;
}

void Guild::SetMOTD(const char * szNewMotd, WorldSession * pClient)
{
	if(pClient->GetPlayer()->m_playerInfo->guild != this)
		return;

	if(!pClient->GetPlayer()->m_playerInfo->guildRank->CanPerformCommand(GR_RIGHT_SETMOTD))
	{
		Guild::SendGuildCommandResult(pClient, GUILD_INVITE_S, "", GUILD_PERMISSIONS);
		return;
	}

	if(m_motd)
		free(m_motd);

	if(strlen(szNewMotd))
	{
		m_motd = strdup(szNewMotd);
		sWorld.ExecuteSqlToDBServer("UPDATE guilds SET motd = \"%s\" WHERE guildId = %u", CharacterDatabase.EscapeString(string(szNewMotd)).c_str(), m_guildId);
	}
	else
	{
		m_motd= NULL;
		sWorld.ExecuteSqlToDBServer("UPDATE guilds SET motd = \"\" WHERE guildId = %u", m_guildId);
	}

	LogGuildEvent(GUILD_EVENT_MOTD, 1, szNewMotd);
}

void Guild::SetGuildInformation(const char * szGuildInformation, WorldSession * pClient)
{
	if(pClient && pClient->GetPlayer()->m_playerInfo->guild != this)
		return;

	if(pClient && !pClient->GetPlayer()->m_playerInfo->guildRank->CanPerformCommand(GR_RIGHT_EGUILDINFO))
	{
		Guild::SendGuildCommandResult(pClient, GUILD_INVITE_S, "", GUILD_PERMISSIONS);
		return;
	}

	if(m_guildInfo)
		free(m_guildInfo);

	if(strlen(szGuildInformation))
	{
		m_guildInfo = strdup(szGuildInformation);
		sWorld.ExecuteSqlToDBServer("UPDATE guilds SET guildInfo = \"%s\" WHERE guildId = %u", CharacterDatabase.EscapeString(string(szGuildInformation)).c_str(), m_guildId);
	}
	else
	{
		m_guildInfo= NULL;
		sWorld.ExecuteSqlToDBServer("UPDATE guilds SET guildInfo = \"\" WHERE guildId = %u", m_guildId);
	}
	LogGuildEvent( GUILD_EVENT_SET_INFORMATION, 1, szGuildInformation );
}

// adding a member
void Guild::AddGuildMember(PlayerInfo * pMember, WorldSession * pClient, int32 ForcedRank /* = -1 */)
{
	if(pMember->guild != NULL)
		return;

	if(pClient && pClient->GetPlayer()->m_playerInfo->guild != this)
		return;

	if(pMember->m_loggedInPlayer && pClient && pClient->GetPlayer()->GetFactionId() != pMember->m_loggedInPlayer->GetFactionId() )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildCannotInviteOtherRace ) );//"不能添加非本族成员入部族" );
		return;
	}

	if(pClient && !pClient->GetPlayer()->m_playerInfo->guildRank->CanPerformCommand(GR_RIGHT_INVITE))
	{
		pClient->SendNotification( build_language_string( BuildString_GuildInvitePermission ) );//"你没有权利邀请别人加入部族" );
		return;
	}

	if(pClient && m_members.size() >= m_MaxMemebrCount )
	{
		if (GetLevel() >= 3)
		{

			pClient->SendNotification( build_language_string( BuildString_GuildFull ) );//"人数已满" );
		}
		else
		{

			pClient->SendNotification( build_language_string( BuildString_GuildFull_LevelUp ) );//"人数已满,请升级公会" );
		}
		return;
	}

	GuildRank * r;
	if(m_members.size())
		r = (ForcedRank<=0) ? FindLowestRank() : m_ranks[ForcedRank];
	else
		r = (ForcedRank<0) ? FindLowestRank() : m_ranks[ForcedRank];

	if(r==NULL)
		r=FindLowestRank();

	if(r==NULL)
	{
		// shouldnt happen
		return;
	}

	GuildMember * pm = new GuildMember;
	memset(pm, 0, sizeof(GuildMember));
	pm->pPlayer = pMember;
	pm->pRank = r;
	pm->szOfficerNote = pm->szPublicNote = NULL;
	m_members.insert(make_pair(pMember, pm));

	pMember->guild=this;
	pMember->guildRank=r;
	pMember->guildMember=pm;

	if(pMember->m_loggedInPlayer)
	{
		pMember->m_loggedInPlayer->SetGuildId(m_guildId);
		pMember->m_loggedInPlayer->SetGuildRank(r->iId);
	}

	sWorld.ExecuteSqlToDBServer("INSERT INTO guild_data VALUES(%u, %u, %u, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)", m_guildId, pMember->guid, r->iId);
	LogGuildEvent(GUILD_EVENT_JOINED, 1, pMember->name);
	AddGuildLogEntry(GUILD_LOG_EVENT_JOIN, 1, pMember->guid);
	LevelUpNoReq();
}

void Guild::RemoveGuildMember(PlayerInfo * pMember, WorldSession * pClient)
{
	if(pMember->guild != this)
		return;

	if(pClient && pClient->GetPlayer()->m_playerInfo->guild != this)
		return;

	GuildRankRights LevelMover = (GuildRankRights)pClient->_player->m_playerInfo->guildRank->iRights;
	GuildRankRights LevelBemover = (GuildRankRights)pMember->guildRank->iRights;
	
	if (pMember->guid == GetGuildLeader()&&GetNumMembers()>1 )
	{
		bool bzuzhang = false;
		bool bduizhang = false;
		pClient->SendNotification( build_language_string( BuildString_GuildPermission ) );//"你的权限不够" );
		return;
	}
	else
	if (pClient->_player->GetGUID() != GetGuildLeader())
	{
		if ((LevelMover == LevelBemover && pClient->_player->m_playerInfo != pMember) ||
			( LevelMover == GR_RIGHT_CAPTAIN && LevelBemover == GR_RIGHT_ALL))
		{
			pClient->SendNotification( build_language_string( BuildString_GuildPermission ) );//"你的权限不够" );
			return;
		}
	}

	if(pClient && !pClient->GetPlayer()->m_playerInfo->guildRank->CanPerformCommand(GR_RIGHT_REMOVE) && pClient->GetPlayer()->m_playerInfo != pMember)
	{
		//Guild::SendGuildCommandResult(pClient, GUILD_CREATE_S, "", GUILD_PERMISSIONS);
		pClient->SendNotification( build_language_string( BuildString_GuildPermission ) );//"你的权限不够" );
		return;
	}

	if(pMember->guildRank->iId==0)
	{
		if(pClient->GetPlayer()->GetGUID() == GetGuildLeader())
		{
			if( pMember->guid == GetGuildLeader() )
			{
				if(GetNumMembers()>1)
				{
					pClient->SystemMessage( build_language_string( BuildString_GuildMasterCannotQuit ) );//"族长在部族有成员的情况下无法退会.");
				}
				else
				{
					Disband();
					return;
				}
			}
		}
		else
			pClient->SystemMessage( build_language_string( BuildString_GuildPermission ) );//"你的权限不够" );

		return;
	}

	sWorld.ExecuteSqlToDBServer("DELETE FROM guild_data WHERE playerid = %u", pMember->guid);

	GuildMemberMap::iterator itr = m_members.find(pMember);
	if(itr != m_members.end())
	{
		// this should always exist.
		if(itr->second->szOfficerNote)
			free((void*)itr->second->szOfficerNote);
		if(itr->second->szPublicNote)
			free((void*)itr->second->szPublicNote);

		delete itr->second;
		m_members.erase(itr);
	}

	LogGuildEvent(GUILD_EVENT_LEFT, 1, pMember->name);
	if(pClient && pClient->GetPlayer()->m_playerInfo != pMember)
	{
		AddGuildLogEntry(GUILD_LOG_EVENT_REMOVAL, 2, pClient->GetPlayer()->GetLowGUID(), pMember->guid);
	}
	else
	{
		AddGuildLogEntry(GUILD_LOG_EVENT_LEFT, 1, pMember->guid);
	}


	pMember->guildRank=NULL;
	pMember->guild=NULL;
	pMember->guildMember=NULL;

	if(pMember->m_loggedInPlayer)
	{
		pMember->m_loggedInPlayer->SetGuildRank(0);
		pMember->m_loggedInPlayer->SetGuildId(0);
		pMember->m_loggedInPlayer->SetLeaveGuildDate();
	}
}

void Guild::SetPublicNote(PlayerInfo * pMember, const char * szNewNote, WorldSession * pClient)
{
	if(pMember->guild != this)
		return;

	if(pClient->GetPlayer()->m_playerInfo->guild != this)
		return;

	if(!pClient->GetPlayer()->m_playerInfo->guildRank->CanPerformCommand(GR_RIGHT_EPNOTE))
	{
		//Guild::SendGuildCommandResult(pClient, GUILD_MEMBER_S, "", GUILD_PERMISSIONS);
		pClient->SendNotification( build_language_string( BuildString_GuildPermission ) );//"你的权限不够" );
		return;
	}

	GuildMemberMap::iterator itr = m_members.find(pMember);
	if(itr != m_members.end())
	{
		if(itr->second->szPublicNote)
			free((void*)itr->second->szPublicNote);

		if(szNewNote && szNewNote[0] != '\0')
			itr->second->szPublicNote = strdup(szNewNote);
		else
			itr->second->szPublicNote = NULL;

			// Update the database
		if (itr->second->szPublicNote == NULL) 
			sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET publicNote=\"\" WHERE playerid=%u", pMember->guid);
		else
			sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET publicNote=\"%s\" WHERE playerid=%u", 
				CharacterDatabase.EscapeString(string(itr->second->szPublicNote)).c_str(),
				pMember->guid
			);
	}

	Guild::SendGuildCommandResult(pClient, GUILD_PUBLIC_NOTE_CHANGED_S, pMember->name, 0);
}

void Guild::SetOfficerNote(PlayerInfo * pMember, const char * szNewNote, WorldSession * pClient)
{
	if(pMember->guild != this)
		return;

	if(pClient->GetPlayer()->m_playerInfo->guild != this)
		return;

	if(!pClient->GetPlayer()->m_playerInfo->guildRank->CanPerformCommand(GR_RIGHT_EOFFNOTE))
	{
		Guild::SendGuildCommandResult(pClient, GUILD_MEMBER_S, "", GUILD_PERMISSIONS);
		return;
	}

	GuildMemberMap::iterator itr = m_members.find(pMember);
	if(itr != m_members.end())
	{
		if(itr->second->szOfficerNote)
			free((void*)itr->second->szOfficerNote);

		if(szNewNote && szNewNote[0] != '\0')
			itr->second->szOfficerNote = strdup(szNewNote);
		else
			itr->second->szOfficerNote = NULL;

		// Update the database
		if (itr->second->szOfficerNote == NULL) 
			sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET officerNote=\"\" WHERE playerid=%u", pMember->guid);
		else
			sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET officerNote=\"%s\" WHERE playerid=%u", 
				CharacterDatabase.EscapeString(string(itr->second->szOfficerNote)).c_str(),
				pMember->guid
			);
	}

	Guild::SendGuildCommandResult(pClient, GUILD_OFFICER_NOTE_CHANGED_S, pMember->name, 0);
}

void Guild::RemoveGuildRank(WorldSession * pClient)
{

	GuildRank * pLowestRank = FindLowestRank();
	if(pLowestRank == NULL || pLowestRank->iId < 5)		// cannot delete default ranks.
	{
		pClient->SystemMessage("Cannot find a rank to delete.");
		return;		
	}

	// check for players that need to be promoted
	GuildMemberMap::iterator itr = m_members.begin();
	for(; itr != m_members.end(); ++itr)
	{
		if(itr->second->pRank == pLowestRank)
		{
			pClient->SystemMessage("There are still members using this rank. You cannot delete it yet!");
			return;
		}
	}

	sWorld.ExecuteSqlToDBServer("DELETE FROM guild_ranks WHERE guildId = %u AND rankId = %u", m_guildId, pLowestRank->iId);
	m_ranks[pLowestRank->iId] = NULL;
	delete pLowestRank;
}

void Guild::Disband()
{
	Player*pLeader = objmgr.GetPlayer(m_guildLeader);
	if (pLeader)
	{
		if (pLeader->HasSpell(88))
		{
			pLeader->removeSpell(88, false, false, 0);
		}
	}
	LogGuildEvent(GUILD_EVENT_DISBANDED, 0);
	for(GuildMemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
	{
		itr->first->guild=NULL;
		itr->first->guildRank=NULL;
		itr->first->guildMember=NULL;
		if(itr->first->m_loggedInPlayer != NULL)
		{
			itr->first->m_loggedInPlayer->SetGuildId(0);
			itr->first->m_loggedInPlayer->SetGuildRank(0);
		}

		delete itr->second;
	}
	m_members.clear();
	objmgr.RemoveGuild(m_guildId);
	sWorld.ExecuteSqlToDBServer("DELETE FROM guilds WHERE guildId = %u", m_guildId);
	sWorld.ExecuteSqlToDBServer("DELETE FROM guild_logs WHERE guildid = %u", m_guildId);
	sWorld.ExecuteSqlToDBServer("DELETE FROM guild_ranks WHERE guildId = %u", m_guildId);
	sWorld.ExecuteSqlToDBServer("DELETE FROM guild_data WHERE guildid = %u", m_guildId);
	sWorld.ExecuteSqlToDBServer("DELETE FROM guild_bankitems WHERE guildId = %u", m_guildId);
	sWorld.ExecuteSqlToDBServer("DELETE FROM guild_banktabs WHERE guildId = %u", m_guildId);
	delete this;
}

bool Guild::ChangeGuildName(string newname)
{
	if(objmgr.GetGuildByGuildName(newname))
		return false;

	m_guildName = strdup(newname.c_str());
	sWorld.ExecuteSqlToDBServer("UPDATE guilds SET guildName = '%s' WHERE guildId = %u", newname.c_str(), m_guildId);
	return true;
}

void Guild::ChangeGuildMaster(PlayerInfo * pNewMaster, WorldSession * pClient)
{
	if(pClient->GetPlayer()->GetLowGUID() != m_guildLeader)
	{
		Guild::SendGuildCommandResult(pClient, GUILD_SET_RANK_S, "", GUILD_PERMISSIONS);
		return;
	}

	GuildRank * newRank = FindHighestRank();
	if(newRank==NULL)
	{
		return;
	}

	GuildMemberMap::iterator itr = m_members.find(pNewMaster);
	GuildMemberMap::iterator itr2 = m_members.find(pClient->GetPlayer()->m_playerInfo);
	ASSERT(m_ranks[0]!=NULL);
	if(itr==m_members.end() || itr2==m_members.end())
	{
		return;
	}

	itr->second->pRank = m_ranks[0];
	itr->first->guildRank = itr->second->pRank;
	itr2->second->pRank = newRank;
	itr2->first->guildRank = newRank;
	sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET guildRank = 0 WHERE playerid = %u AND guildid = %u", itr->first->guid, m_guildId);
	sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET guildRank = %u WHERE playerid = %u AND guildid = %u", newRank->iId, itr2->first->guid, m_guildId);
	sWorld.ExecuteSqlToDBServer("UPDATE guilds SET leaderGuid = %u WHERE guildId = %u", itr->first->guid, m_guildId);
	m_guildLeader = itr->first->guid;
	pClient->GetPlayer()->SetGuildRank(newRank->iId);
	if (pNewMaster->m_loggedInPlayer)
	{
		pNewMaster->m_loggedInPlayer->SetGuildRank(itr->second->pRank->iId);
	}

	LogGuildEvent(GUILD_EVENT_SETRANK, 3, pClient->GetPlayer()->GetName(), pNewMaster->name, itr->second->pRank->szRankName);
	LogGuildEvent(GUILD_EVENT_SETRANK, 3, pClient->GetPlayer()->GetName(), pClient->GetPlayer()->GetName(), newRank->szRankName);
	
}

uint32 Guild::GenerateGuildLogEventId()
{
	uint32 r;
	r = ++m_hiLogId;
	return r;
}

void Guild::GuildChat(const char * szMessage, WorldSession * pClient, uint32 iType)
{
	if(pClient->GetPlayer()->m_playerInfo->guild != this)
		return;

	if(!pClient->GetPlayer()->m_playerInfo->guildRank->CanPerformCommand(GR_RIGHT_GCHATSPEAK))
	{
		Guild::SendGuildCommandResult(pClient, GUILD_MEMBER_S, "", GUILD_PERMISSIONS);
		return;
	}

	MSG_S2C::stChat_Message Msg;
	Msg.creature_name = pClient->GetPlayer()->GetName();
	sChatHandler.FillMessageData( CHAT_MSG_GUILD, szMessage, pClient->GetPlayer()->GetGUID(), &Msg);

	for(GuildMemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
	{
		if(itr->second->pRank->CanPerformCommand(GR_RIGHT_GCHATLISTEN) && itr->first->m_loggedInPlayer)
			itr->first->m_loggedInPlayer->GetSession()->SendPacket(Msg);
	}
}

void Guild::OfficerChat(const char * szMessage, WorldSession * pClient, uint32 iType)
{
	if(pClient->GetPlayer()->m_playerInfo->guild != this)
		return;

	if(!pClient->GetPlayer()->m_playerInfo->guildRank->CanPerformCommand(GR_RIGHT_OFFCHATSPEAK))
	{
		Guild::SendGuildCommandResult(pClient, GUILD_MEMBER_S, "", GUILD_PERMISSIONS);
		return;
	}

	MSG_S2C::stChat_Message Msg;
	sChatHandler.FillMessageData( CHAT_MSG_OFFICER, szMessage, pClient->GetPlayer()->GetGUID(), &Msg );

	for(GuildMemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
	{
		if(itr->second->pRank->CanPerformCommand(GR_RIGHT_OFFCHATLISTEN) && itr->first->m_loggedInPlayer)
			itr->first->m_loggedInPlayer->GetSession()->SendPacket(Msg);
	}
}

void Guild::SendGuildLog(WorldSession * pClient)
{
	MSG_S2C::stGuild_Log Msg;
	GuildLogList::iterator itr;
	uint32 count = 0;

	for(itr = m_log.begin(); itr != m_log.end(); ++itr) {
		MSG_S2C::stGuild_Log::stLog guild_log;
		guild_log.iEvent = uint8((*itr)->iEvent);
		switch((*itr)->iEvent)
		{
		case GUILD_LOG_EVENT_SETRANK:
			{
				guild_log.Event1 = uint64((*itr)->iEventData[0]);
				guild_log.Event2 = uint64((*itr)->iEventData[1]);
				guild_log.Event3 = uint8((*itr)->iEventData[2]);
			}break;
			
		case GUILD_LOG_EVENT_INVITE:
		case GUILD_LOG_EVENT_REMOVAL:
			{
				guild_log.Event1 = uint64((*itr)->iEventData[0]);
				guild_log.Event2 = uint64((*itr)->iEventData[1]);
			}break;

		case GUILD_LOG_EVENT_JOIN:
		case GUILD_LOG_EVENT_LEFT:
			{
				guild_log.Event1 = uint64((*itr)->iEventData[0]);
			}break;
		}

		guild_log.TimeStamp = uint32(UNIXTIME - (*itr)->iTimeStamp);
		Msg.vLogs.push_back(guild_log);
		if( (++count) >= 25 )
			break;
	}

	pClient->SendPacket(Msg);
}

void Guild::SendGuildRoster(WorldSession * pClient)
{
	MSG_S2C::stGuild_Roster Msg;
	GuildMemberMap::iterator itr;
	GuildRank * r;
	Player * pPlayer;
	uint32 i;
	uint32 c =0;
	GuildRank * myRank;
	bool ofnote;
	if(pClient->GetPlayer()->m_playerInfo->guild != this)
		return;

	myRank = pClient->GetPlayer()->m_playerInfo->guildRank;
	ofnote = myRank->CanPerformCommand(GR_RIGHT_VIEWOFFNOTE);

	Msg.motd = m_motd ? m_motd : "";
	Msg.guildInfo = m_guildInfo ? m_guildInfo : "";

	Msg.maxbanks = uint32(MAX_GUILD_RANKS);

	for(i = 0; i < MAX_GUILD_RANKS; ++i)
	{
		r = m_ranks[i];
		if(r != NULL)
		{
			MSG_S2C::stGuild_Roster::stBank guild_bank;
			guild_bank.iRights = r->iRights;
			guild_bank.iGoldLimitPerDay = r->iGoldLimitPerDay;

			// i would do this with one big memcpy but grr grr struct alignment
			for( uint32 j = 0; j < 6; j++ )
			{
				guild_bank.Permissions[j].iFlags = r->iTabPermissions[j].iFlags;
				guild_bank.Permissions[j].iStacksPerDay =  r->iTabPermissions[j].iStacksPerDay;
			}
			Msg.vBanks.push_back(guild_bank);
			++c;
		}
	}

	for(itr = m_members.begin(); itr != m_members.end(); ++itr)
	{
		GuildMember* gm = itr->second;
		PlayerInfo* pi = itr->first;
		pPlayer = gm->pPlayer->m_loggedInPlayer;

		MSG_S2C::stGuild_Roster::stMember guild_member;
		guild_member.player_id = pi->guid;
		guild_member.high_guid = uint32(0);			// highguid
		guild_member.bOnline = uint8( (pPlayer!=NULL) ? 1 : 0 );
		guild_member.name = pi->name;
		guild_member.rank_id = gm->pRank->iId;
		guild_member.last_level = uint8(pi->lastLevel);
		guild_member.Class = uint8(pi->cl);
		guild_member.last_zone = pi->lastZone;
		guild_member.gender = gm->pPlayer->gender;
		guild_member.contribution_points = gm->contribution_points;

		if(!pPlayer)
			guild_member.lastOnline = float((UNIXTIME-pi->lastOnline)/86400.0);

		if( gm->szPublicNote )
			guild_member.szPublicNote = gm->szPublicNote;

		if(ofnote && gm->szOfficerNote != NULL)
			guild_member.szOfficerNote = gm->szOfficerNote;

		Msg.vMembers.push_back( guild_member );
	}

	Msg.guildInfo = m_guildInfo ? m_guildInfo : "";
	pClient->SendPacket(Msg);
}

void Guild::SendGuildQuery(WorldSession * pClient)
{
	uint32 i = 0;
	GuildRank * r;
	MSG_S2C::stGuild_Query_Response Msg;
	Msg.guild_id = m_guildId;
	Msg.guild_name = m_guildName;
	Msg.war_score = m_hiddenScore;
	for(i = 0; i < MAX_GUILD_RANKS; ++i)
	{
		r = m_ranks[i];
		if(r != NULL)
			Msg.vBanks.push_back(r->szRankName);
	}

	/*
	Msg.emblemStyle = m_emblemStyle;
	Msg.emblemColor = m_emblemColor;
	Msg.borderStyle = m_borderStyle;
	Msg.borderColor = m_borderColor;
	Msg.backgroundColor = m_backgroundColor;
	*/
	Msg.level = m_level;
	Msg.score = m_score;
	Msg.emblemStyle = m_emblemStyle;
	Msg.emblemColor = m_emblemColor;
	Msg.enroll_castles = m_enroll_castles;
	for( std::map<uint32, bool>::iterator it = m_alliance_guilds.begin(); it != m_alliance_guilds.end(); ++it )
	{
		uint32 id = it->first;
		Guild* p = objmgr.GetGuild( id );
		if( p )
		{
			Msg.alliance_guilds[id] = p->GetGuildName();
			if( it->second )
				Msg.lead_guild_id = id;
		}
	}


	if(pClient != NULL)
	{
		pClient->SendPacket(Msg);
	}
	else
	{
		for(GuildMemberMap::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
		{
			if(itr->first->m_loggedInPlayer)
				itr->first->m_loggedInPlayer->GetSession()->SendPacket(Msg);
		}
	}

}

void Guild::CreateInDB()
{
	sWorld.ExecuteSqlToDBServer("INSERT INTO guilds VALUES(%u, \"%s\", %u, %u, %u, %u, %u, %u, '', '', %u, 0, 0, 1, 0, %u, %u)",
		m_guildId, CharacterDatabase.EscapeString(string(m_guildName)).c_str(), m_guildLeader, m_emblemStyle, m_emblemColor, m_borderColor, m_borderStyle,
		m_backgroundColor, m_creationTimeStamp, m_hiddenScore, m_NoWarBeginTime);
}

Guild* Guild::Create()
{
	Guild* p = new Guild();
	return p;
}

void Guild::BuyBankTab(WorldSession * pClient)
{
	if(pClient && pClient->GetPlayer()->GetLowGUID() != m_guildLeader)
		return;

	if(m_bankTabCount>=MAX_GUILD_BANK_TABS)
		return;

	GuildBankTab * pTab = new GuildBankTab;
	pTab->iTabId = m_bankTabCount;
	memset(pTab->pSlots, 0, sizeof(Item*)*MAX_GUILD_BANK_SLOTS);
	pTab->szTabName=NULL;
	pTab->szTabIcon=NULL;

	m_bankTabs.push_back(pTab);
	m_bankTabCount++;

	sWorld.ExecuteSqlToDBServer("INSERT INTO guild_banktabs VALUES(%u, %u, '', '')", m_guildId, (uint32)pTab->iTabId);
	sWorld.ExecuteSqlToDBServer("UPDATE guilds SET bankTabCount = %u WHERE guildId = %u", m_bankTabCount, m_guildId);
}

uint32 GuildMember::CalculateAllowedItemWithdraws(uint32 tab)
{
	if(pRank->iTabPermissions[tab].iStacksPerDay == -1)		// Unlimited
		return 0xFFFFFFFF;
	if(pRank->iTabPermissions[tab].iStacksPerDay == 0)		// none
		return 0;

	if((UNIXTIME - uLastItemWithdrawReset[tab]) >= TIME_DAY)
		return pRank->iTabPermissions[tab].iStacksPerDay;
	else
		return (pRank->iTabPermissions[tab].iStacksPerDay - uItemWithdrawlsSinceLastReset[tab]);
}

void GuildMember::OnItemWithdraw(uint32 tab)
{
	if(pRank->iTabPermissions[tab].iStacksPerDay <= 0)		// Unlimited
		return;

	// reset the counter if a day has passed
	if(((uint32)UNIXTIME - uLastItemWithdrawReset[tab]) >= TIME_DAY)
	{
		uLastItemWithdrawReset[tab] = (uint32)UNIXTIME;
		uItemWithdrawlsSinceLastReset[tab] = 1;
		sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET lastItemWithdrawReset%u = %u, itemWithdrawlsSinceLastReset%u = 1 WHERE playerid = %u",
			tab, uLastItemWithdrawReset, tab, pPlayer->guid);
	}
	else
	{
		// increment counter
		uItemWithdrawlsSinceLastReset[tab]++;
		sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET itemWithdrawlsSinceLastReset%u = %u WHERE playerid = %u",
			tab, uItemWithdrawlsSinceLastReset, pPlayer->guid);
	}
}

uint32 GuildMember::CalculateAvailableAmount()
{
	if(pRank->iGoldLimitPerDay == -1)		// Unlimited
		return 0xFFFFFFFF;

	if(pRank->iGoldLimitPerDay == 0)
		return 0;

	if((UNIXTIME - uLastWithdrawReset) >= TIME_DAY)
		return pRank->iGoldLimitPerDay;
	else
		return (pRank->iGoldLimitPerDay - uWithdrawlsSinceLastReset);
}

void GuildMember::OnMoneyWithdraw(uint32 amt)
{
	if(pRank->iGoldLimitPerDay <= 0)		// Unlimited
		return;

	// reset the counter if a day has passed
	if(((uint32)UNIXTIME - uLastWithdrawReset) >= TIME_DAY)
	{
		uLastWithdrawReset = (uint32)UNIXTIME;
		uWithdrawlsSinceLastReset = amt;
		sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET lastWithdrawReset = %u, withdrawlsSinceLastReset = %u WHERE playerid = %u",
			uLastWithdrawReset, uWithdrawlsSinceLastReset, pPlayer->guid);
	}
	else
	{
		// increment counter
		uWithdrawlsSinceLastReset++;
		sWorld.ExecuteSqlToDBServer("UPDATE guild_data SET withdrawlsSinceLastReset = %u WHERE playerid = %u",
			uWithdrawlsSinceLastReset, pPlayer->guid);
	}
}

void Guild::DepositMoney(WorldSession * pClient, uint32 uAmount)
{
	if(pClient->GetPlayer()->GetUInt32Value(PLAYER_FIELD_COINAGE) < uAmount)
		return;

	// add to the bank balance
	m_bankBalance += uAmount;

	// update in db
	sWorld.ExecuteSqlToDBServer("UPDATE guilds SET bankBalance = %u WHERE guildId = %u", m_bankBalance, m_guildId);

	// take the money, oh noes gm pls gief gold mi hero poor
	pClient->GetPlayer()->ModCoin(-(int32)uAmount);

	// broadcast guild event telling everyone the new balance
	char buf[20];
	snprintf(buf, 20, I64FMT, (uint64)m_bankBalance);
	LogGuildEvent(GUILD_EVENT_SETNEWBALANCE, 1, buf);

	// log it!
	LogGuildBankActionMoney(GUILD_BANK_LOG_EVENT_DEPOSIT_MONEY, pClient->GetPlayer()->GetLowGUID(), uAmount);
}

void Guild::WithdrawMoney(WorldSession * pClient, uint32 uAmount)
{
	GuildMember * pMember = pClient->GetPlayer()->m_playerInfo->guildMember;
	if(pMember==NULL)
		return;

	// sanity checks
	if(pMember->pRank->iGoldLimitPerDay > 0)
	{
		if(pMember->CalculateAvailableAmount() < uAmount)
		{
			pClient->SystemMessage("You have already withdrawn too much today.");
			return;
		}
	}

	if(pMember->pRank->iGoldLimitPerDay == 0)
	{
		pClient->SystemMessage("You don't have permission to do that.");
		return;
	}

	if(m_bankBalance < uAmount)
		return;

	// update his bank state
	pMember->OnMoneyWithdraw(uAmount);

	// give the gold! GM PLS GOLD PLS 1 COIN
	pClient->GetPlayer()->ModCoin((uint32)uAmount);

	// subtract the balance
	m_bankBalance -= uAmount;

	// update in db
	sWorld.ExecuteSqlToDBServer("UPDATE guilds SET bankBalance = %u WHERE guildId = %u", (m_bankBalance>0)?m_bankBalance:0, m_guildId);

	// notify everyone with the new balance
	char buf[20];
	snprintf(buf, 20, I64FMT, (uint64)m_bankBalance);
	LogGuildEvent(GUILD_EVENT_SETNEWBALANCE, 1, buf);

	// log it!
	LogGuildBankActionMoney(GUILD_BANK_LOG_EVENT_WITHDRAW_MONEY, pClient->GetPlayer()->GetLowGUID(), uAmount);
}

void Guild::SendGuildBankLog(WorldSession * pClient, uint8 iSlot)
{
	uint32 count = 0;
	if(iSlot > 6)
		return;

	MSG_S2C::stGuild_Bank_Log Msg;
	Msg.slotid = iSlot;
	if(iSlot == 6)
	{
		// sending the money log
		uint32 lt = (uint32)UNIXTIME;
		list<GuildBankEvent*>::iterator itr = m_moneyLog.begin();
		for(; itr != m_moneyLog.end(); ++itr)
		{
			MSG_S2C::stGuild_Bank_Log::stBankLog guild_log;
			guild_log.iAction = (*itr)->iAction;
			guild_log.player_id = (*itr)->uPlayer;
			guild_log.high_guid = uint32(0);		// highguid
			guild_log.uEntry = (*itr)->uEntry;
			guild_log.TimeStamp =uint32(lt - (*itr)->uTimeStamp);
			Msg.vLogs.push_back(guild_log);
			if( (++count) >= 25 )
				break;
		}

		pClient->SendPacket(Msg);
	}
	else
	{
		if(iSlot >= m_bankTabCount)
		{
			return;
		}

		GuildBankTab * pTab = m_bankTabs[iSlot];
		if(pTab == NULL)
		{
			return;
		}

		uint32 lt = (uint32)UNIXTIME;
		list<GuildBankEvent*>::iterator itr = pTab->lLog.begin();
		for(; itr != pTab->lLog.end(); ++itr)
		{
			MSG_S2C::stGuild_Bank_Log::stBankLog guild_log;
			guild_log.iAction = (*itr)->iAction;
			guild_log.player_id = (*itr)->uPlayer;
			guild_log.high_guid = uint32(0);		// highguid
			guild_log.uEntry = (*itr)->uEntry;
			guild_log.TimeStamp =uint32(lt - (*itr)->uTimeStamp);
			Msg.vLogs.push_back(guild_log);

				break;
		}
		
		SendPacket(Msg);
	}
}

void Guild::LogGuildBankAction(uint8 iAction, uint32 uGuid, uint32 uEntry, uint8 iStack, GuildBankTab * pTab)
{
	GuildBankEvent * ev = new GuildBankEvent;
	uint32 timest = (uint32)UNIXTIME;
	ev->iAction = iAction;
	ev->iStack = iStack;
	ev->uEntry = uEntry;
	ev->uPlayer = uGuid;
	ev->uTimeStamp = timest;


	if(pTab->lLog.size() >= 25)
	{
		// pop one off the end
		GuildBankEvent * ev2 = *(pTab->lLog.begin());
		sWorld.ExecuteSqlToDBServer("DELETE FROM guild_banklogs WHERE guildid = %u AND log_id = %u",
			m_guildId, ev2->iLogId);

		pTab->lLog.pop_front();
		delete ev2;
	}

	ev->iLogId = GenerateGuildLogEventId();
	pTab->lLog.push_back(ev);

	sWorld.ExecuteSqlToDBServer("INSERT INTO guild_banklogs VALUES(%u, %u, %u, %u, %u, %u, %u, %u)",
		ev->iLogId, m_guildId, (uint32)pTab->iTabId, (uint32)iAction, uGuid, uEntry, (uint32)iStack, timest);
}

void Guild::LogGuildBankActionMoney(uint8 iAction, uint32 uGuid, uint32 uAmount)
{
	GuildBankEvent * ev = new GuildBankEvent;
	uint32 timest = (uint32)UNIXTIME;
	ev->iAction = iAction;
	ev->iStack = 0;
	ev->uEntry = uAmount;
	ev->uPlayer = uGuid;
	ev->uTimeStamp = timest;


	if(m_moneyLog.size() >= 25)
	{
		// pop one off the end
		GuildBankEvent * ev2 = *(m_moneyLog.begin());
		sWorld.ExecuteSqlToDBServer("DELETE FROM guild_banklogs WHERE guildid = %u AND log_id = %u",
			m_guildId, ev2->iLogId);

		m_moneyLog.pop_front();
		delete ev2;
	}

	ev->iLogId = GenerateGuildLogEventId();
	m_moneyLog.push_back(ev);

	sWorld.ExecuteSqlToDBServer("INSERT INTO guild_banklogs VALUES(%u, %u, 6, %u, %u, %u, 0, %u)",
		ev->iLogId, m_guildId, (uint32)iAction, uGuid, uAmount, timest);
}

void Guild::SetTabardInfo(uint32 EmblemStyle, uint32 EmblemColor, uint32 BorderStyle, uint32 BorderColor, uint32 BackgroundColor)
{
	m_emblemStyle = EmblemStyle;
	m_emblemColor = EmblemColor;
	m_borderStyle = BorderStyle;
	m_borderColor = BorderColor;
	m_backgroundColor = BackgroundColor;

	// update in db
	sWorld.ExecuteSqlToDBServer("UPDATE guilds SET emblemStyle = %u, emblemColor = %u, borderStyle = %u, borderColor = %u, backgroundColor = %u WHERE guildId = %u",
		EmblemStyle, EmblemColor, BorderStyle, BorderColor, BackgroundColor, m_guildId);
}

void Guild::SendGuildInfo(WorldSession * pClient)
{
	MSG_S2C::stGuild_Info Msg;
	time_t ct = (time_t)m_creationTimeStamp;
	tm * pTM = localtime(&ct);

	Msg.guildName = m_guildName;
	Msg.create_year = uint32(pTM->tm_year+1900);
	Msg.create_month = uint32(pTM->tm_mon);
	Msg.create_day = uint32(pTM->tm_mday);
	Msg.member_count = uint32(m_members.size());
	pClient->SendPacket(Msg);	
}


bool Guild::LevelUpNoReq()
{
	int nSpendScore = 0;
	guild_upgrade_list* gul = dbcGuildUpgradeList.LookupEntry( m_level  + 1);
	if (gul)
	{
		if (m_level > 0 )
		{
			if (m_score < gul->score_require)
			{
				return false;
			}

			if (m_members.size() < gul->MemberMin)
			{
				return false;
			}

			ItemPrototype* ipt = NULL;
			
			if (gul->item_require)
			{
				ipt = ItemPrototypeStorage.LookupEntry( gul->item_require );
			}

			if( ipt )
			{

				return false;
			}

			nSpendScore = gul->score_require;
			++m_level;
			m_MaxMemebrCount = gul->membercount;
			
			sWorld.ExecuteSqlToDBServer( "update guilds set level = %u where guildId = %u", m_level, m_guildId );
			MSG_S2C::stGuild_Level_Up Msg;
			Msg.Level = m_level;
			SendPacket(Msg);
			MSG_S2C::stNotify_Msg NotifyMsg;
			NotifyMsg.notify = build_language_string( BuildString_GuildLevelUp, m_level );
			SendPacket(NotifyMsg);
			SpendScore(nSpendScore);

			return true;
		}
	}
	return false;
}

bool Guild::LevelUp( WorldSession* pClient )
{
	int nSpendScore = 0;
	if( m_level > 3 )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildLevelCap ) );//"部族已经达到顶级" );
		return false;		
	}

	if( !pClient->GetPlayer() )
		return false;

	guild_upgrade_list* gul = dbcGuildUpgradeList.LookupEntry( m_level  + 1);
	if (gul)
	{
		if (m_level > 0 )
		{
			if (m_score < gul->score_require)
			{
				pClient->SendNotification( build_language_string( BuildString_GuildUpgrade, gul->score_require ) );
				return false;
			}


			ItemPrototype* ipt = ItemPrototypeStorage.LookupEntry( gul->item_require );
			if( ipt )
			{
				if( !pClient->GetPlayer()->GetItemInterface()->GetItemCount( gul->item_require, false ) )
				{
					pClient->SendNotification( build_language_string( BuildString_GuildUpgradeRequireItem, Item::BuildStringWithProto( ipt ) ) );//"部族升级缺少物品:[%s]", Item::BuildStringWithProto( ipt ) );
					return false;
				}
				else
				{
					pClient->GetPlayer()->GetItemInterface()->RemoveItemAmt( gul->item_require, 1 );
				}
			}
			else
			{
				//pClient->SendNotification( "部族升级道具属性未填写。 请通知开发人员" );
				return false;
			}
			nSpendScore = gul->score_require;

			SpendScore(nSpendScore);
			++m_level;

			m_MaxMemebrCount = gul->membercount;

			sWorld.ExecuteSqlToDBServer( "update guilds set level = %u where guildId = %u", m_level, m_guildId );

			MSG_S2C::stGuild_Level_Up Msg;
			Msg.Level = m_level;
			pClient->SendPacket(Msg);
			MSG_S2C::stNotify_Msg NotifyMsg;
			NotifyMsg.notify = build_language_string( BuildString_GuildLevelUp, m_level );
			pClient->SendPacket(NotifyMsg);
			return true;
		}
	}
	return false;
}

void Guild::SendGuildListInfo( WorldSession* pClient )
{
	MSG_S2C::stGuildListInfoAck msg;
	for( std::set<Guild*>::iterator it = s_setGuilds.begin(); it != s_setGuilds.end(); ++it )
	{	
		Guild* p = *it;
		if( !p->m_guildLeader ) continue;

		time_t ct = (time_t)p->m_creationTimeStamp;
		tm * pTM = localtime(&ct);

		MSG_S2C::stGuildListInfoAck::guild_info_t gi;
		gi.id = p->m_guildId;
		gi.guildName = p->m_guildName;
		gi.create_year = uint32(pTM->tm_year+1900);
		gi.create_month = uint32(pTM->tm_mon);
		gi.create_day = uint32(pTM->tm_mday);
		gi.member_count = uint32(p->m_members.size());
		gi.level = p->m_level;
		gi.war_score = p->GetHiddenScore();
		gi.NoWar = p->GetNoWarTime();
		PlayerInfo* pi = objmgr.GetPlayerInfo( p->m_guildLeader );
		if( pi )
			gi.master_name = pi->name;
		msg.vGuilds.push_back( gi );
	}
	pClient->SendPacket( msg );
}

void Guild::AddGuildScore( uint32 score )
{
	m_score += score;
	sWorld.ExecuteSqlToDBServer( "update guilds set score = %u where guildId = %u", m_score, m_guildId );

	for( GuildMemberMap::iterator it = m_members.begin(); it != m_members.end(); ++it )
	{
		PlayerInfo* p = it->first;
		if( p->m_loggedInPlayer )
			p->m_loggedInPlayer->UpdateGuildScore( m_score );
	}

	LevelUpNoReq();
}

void Guild::ModifyGuildScore(uint32 score)
{
	m_score = score;
	sWorld.ExecuteSqlToDBServer( "update guilds set score = %u where guildId = %u", m_score, m_guildId );

	for( GuildMemberMap::iterator it = m_members.begin(); it != m_members.end(); ++it )
	{
		PlayerInfo* p = it->first;
		if( p->m_loggedInPlayer )
			p->m_loggedInPlayer->UpdateGuildScore( m_score );
	}
}

void Guild::ModifyGuildHiddenScore(uint32 score)
{
	m_hiddenScore = score;
	sWorld.ExecuteSqlToDBServer( "update guilds set rating = %u where guildId = %u", m_hiddenScore, m_guildId );
}


void Guild::BeginNoWar()
{

	if (!m_NoWarBeginTime)
	{
		m_NoWarBeginTime = UNIXTIME;
		sEventMgr.AddEvent( this, &Guild::EndNoWar, EVENT_GUILD_REMOVE_NO_WAR, 3 * 24 * 3600 * 1000, 1, 0 );
		sWorld.ExecuteSqlToDBServer( "update guilds set noWar = %u where guildId = %u", m_NoWarBeginTime, m_guildId );

		MSG_S2C::stNotify_Msg Msg;
		Msg.notify = build_language_string( BuildString_Guild_Begin_No_War );
		SendPacket( Msg );
	}
}

void Guild::EndNoWar()
{
	m_NoWarBeginTime = 0;
	sWorld.ExecuteSqlToDBServer( "update guilds set noWar = %u where guildId = %u", m_NoWarBeginTime, m_guildId );

}

uint32 Guild::GetNoWarTime()
{
	return m_NoWarBeginTime;
}

void Guild::AddGuildHiddenScore(uint32 score)
{
	m_hiddenScore += score;
	sWorld.ExecuteSqlToDBServer( "update guilds set rating = %u where guildId = %u", m_hiddenScore, m_guildId );

}

bool Guild::SpendHiddenScore(uint32 score)
{
	if (m_hiddenScore < score)
	{
		return false;
	}
	
	m_hiddenScore -= score;
	sWorld.ExecuteSqlToDBServer( "update guilds set rating = %u where guildId = %u", m_hiddenScore, m_guildId );
	return true;
}

bool Guild::SpendScore( uint32 score )
{
	if( m_score < score ) return false;
	m_score -= score;
	sWorld.ExecuteSqlToDBServer( "update guilds set score = %u where guildId = %u", m_score, m_guildId );

	for( GuildMemberMap::iterator it = m_members.begin(); it != m_members.end(); ++it )
	{
		PlayerInfo* p = it->first;
		if( p->m_loggedInPlayer )
			p->m_loggedInPlayer->UpdateGuildScore( m_score );
	}


	LevelUpNoReq();
	return true;
}

void Guild::DeclareWar( WorldSession* pClient, uint32 hostileguild )
{
	if( hostileguild == 0 ) return;
	PlayerInfo* pi = pClient->GetPlayer()->m_playerInfo;
	if( !pi->guildRank->CanPerformCommand( GR_RIGHT_DECLARE_WAR ) )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildPermission ) );//"你没有权利对其他部族宣战" );
		return;
	}

	if (GetNoWarTime())
	{
		pClient->SendNotification( build_language_string( BuildString_Guild_In_No_War ) );//"免战状态" );
		return;
	}

	if( m_hostileGuildid )
	{
		Guild* pHostileGuild = objmgr.GetGuild( m_hostileGuildid );
		if( pHostileGuild )
			pClient->SendNotification( build_language_string( BuildString_GuildWarRunning, pHostileGuild->GetGuildName() ) );//"你的部族正在宣战中，请等待解除宣战后再次宣战" );
		return;
	}
	if( m_hostileGuildid == m_guildId )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildWarSelf ) );//"不能对自己的部族宣战" );
		return;
	}
	if( IsAlliance( hostileguild ) )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildWarAlliance ) );//"不能向同盟宣战" );
		return;
	}
	
	Guild * p = objmgr.GetGuild( hostileguild );
	if( p )
	{
		if (p->GetNoWarTime())
		{
			pClient->SendNotification( build_language_string( BuildString_Target_Guild_In_No_War ) );//"免战状态" );
			return;
		}

		for( GuildMemberMap::iterator it = m_members.begin(); it != m_members.end(); ++it )
		{
			PlayerInfo* p = it->first;
			if( p->m_loggedInPlayer )
				p->m_loggedInPlayer->SetHostileGuild( hostileguild );
		}
		m_hostileGuildid = hostileguild;
		sEventMgr.AddEvent( this, &Guild::RemoveWar, EVENT_GUILD_REMOVE_WAR, 24 * 3600 * 1000, 1, 0 );

		MSG_S2C::stGuildDeclareWarNotify notify;
		notify.active_guildname = m_guildName;
		notify.passive_guildname = p->m_guildName;
		p->SendPacket( notify );
		SendPacket( notify );

		MyLog::log->info( "guild:[%s] declare war to guild:[%s]", GetGuildName(), p->GetGuildName() );
		return;
	}
	pClient->SendNotification( build_language_string( BuildString_GuildNotFound ) );//"没有这个部族" );
}

void Guild::RemoveWar()
{
	for( GuildMemberMap::iterator it = m_members.begin(); it != m_members.end(); ++it )
	{
		PlayerInfo* p = it->first;
		if( p->m_loggedInPlayer )
			p->m_loggedInPlayer->SetHostileGuild( 0 );
	}
	Guild * p = objmgr.GetGuild( m_hostileGuildid );
	if( p )
		MyLog::log->info( "guild:[%s] and guild:[%s] war state removed", GetGuildName(), p->GetGuildName() );
	m_hostileGuildid = 0;
}

void Guild::AllyReq( WorldSession* pClient, uint32 allianceguild )
{
	if( m_ready_ally_guild_id )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildAllianceWaiting ) );//"正在请求结盟，请等待回馈" );
		return;
	}

	if( allianceguild == m_guildId )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildAllianceSelf ) );//"不能和自己的部族结盟" );
		return;
	}

	if( allianceguild == m_hostileGuildid )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildAllianceWarRunning ) );//"不能对与我方交战状态的部族提出结盟请求" );
		return;
	}
	if( IsAlliance( allianceguild ) )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildAllianceAlreadyIs ) );//"已经是同盟了" );
		return;
	}

	Guild * p = objmgr.GetGuild( allianceguild );
	if( !p )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildNotFound ) );//"没有这个部族" );
		return;
	}
	if( p->m_hostileGuildid == m_guildId )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildAllianceWarRunning ) );//"不能对与我方交战状态的部族提出结盟请求" );
		return;
	}
	if( m_level < 3 )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildAllianceLowLevel ) );//"部族等级未达到3级" );
		return;
	}
	if( this->m_guildLeader != pClient->GetPlayer()->GetLowGUID() )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildYouAreNotTheLeader ) );//"你不是部族领袖" );
		return;
	}
	Player* ply = objmgr.GetPlayer( p->m_guildLeader );
	if( !ply )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildAllianceTargetLeaderOffline ) );//"对方部族领袖不在线" );
		return;
	}
	MSG_S2C::stGuildAllyReqNotify notify;
	notify.guild_id = m_guildId;
	notify.guild_name = m_guildName;
	notify.master_id = pClient->GetPlayer()->GetLowGUID();
	notify.master = pClient->GetPlayer()->GetName();
	ply->GetSession()->SendPacket( notify );
	m_ready_ally_guild_id = allianceguild;
	sEventMgr.AddEvent(this, &Guild::AllyReqTimeout, EVENT_GUILD_ALLY_REQ_TIMEOUT, 20 * 1000, 0,0);
}

void Guild::AcceptAlliance( WorldSession* pClient, uint32 allianceguild, bool baccept )
{
	if( m_guildLeader != pClient->GetPlayer()->GetLowGUID() )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildYouAreNotTheLeader ) );//"你不是部族领袖" );
		return;
	}
	Guild* g = objmgr.GetGuild( allianceguild );
	if( !g )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildAllianceInviterDisband ) );//"邀请和你同盟的部族已经解散" );
		return;
	}
	if( g->m_ready_ally_guild_id == 0 )
	{
		pClient->SendNotification( build_language_string( BuildString_GuildAllianceExpire ) );//"邀请已经过期，请让对方重新邀请" );
		return;
	}
	if( g->m_ready_ally_guild_id != m_guildId )
	{
		//pClient->SendNotification( "Guild::AcceptAlliance 异常，已忽略" );
		return;
	}
	g->m_ready_ally_guild_id = 0;
	Player* p = objmgr.GetPlayer( g->m_guildLeader );
	if( p )
	{
		MSG_S2C::stGuildAllyAck ack;
		ack.baccept = baccept;
		ack.master_id = pClient->GetPlayer()->GetLowGUID();
		if( p->GetSession() )
			p->GetSession()->SendPacket( ack );
	}
	if( g->m_allianceid )
	{
		if( sWorld.ExecuteSqlToDBServer( "insert into guild_alliance (alliance_id, guild_id, is_owner) values(%u, %u, 0)", g->m_allianceid, m_guildId ) )
		{
			m_allianceid = g->m_allianceid;
			m_alliance_guilds[allianceguild] = true;
			g->m_alliance_guilds[m_guildId] = false;
		}
		else
			return;
	}
	else
	{
		uint32 alliance_id = 1;
		QueryResult* qr = CharacterDatabase.Query( "select max(alliance_id) from guild_alliance" );
		if( qr )
		{
			alliance_id = 1 + qr->Fetch()[0].GetUInt32();
		}
		MakeGUIDByGroupID( sMaster.m_group_id, alliance_id );
		if( sWorld.ExecuteSqlToDBServer( "insert into guild_alliance (alliance_id, guild_id, is_owner) values(%u, %u, 1)", alliance_id, allianceguild ) )
		{
			if( sWorld.ExecuteSqlToDBServer( "insert into guild_alliance (alliance_id, guild_id, is_owner) values(%u, %u, 0)", alliance_id, m_guildId ) )
			{
				m_alliance_guilds[allianceguild] = true;
				g->m_alliance_guilds[m_guildId] = false;
			}
			else
				return;
		}
		else
			return;
	}
	MSG_S2C::stGuildAllyStateNotify notify;
	notify.bestablish = true;
	notify.guild_id = m_guildId;
	notify.guild_name = m_guildName;
	g->SendPacket( notify );

	notify.guild_id = g->m_guildId;
	notify.guild_name = g->m_guildName;
	SendPacket( notify );

	MyLog::log->info( "guild:[%s] is in alliance with guild:[%s]", GetGuildName(), g->GetGuildName() );
}

void Guild::AllyReqTimeout()
{
	m_ready_ally_guild_id = 0;
}

bool Guild::IsAlliance( uint32 guildid )
{
	if( guildid == m_guildId ) return true;

	if( m_alliance_guilds.end() != m_alliance_guilds.find( guildid ) )
		return true;
	else
		return false;
}

void Guild::RemoveEnroll( uint32 mapid )
{
	m_enroll_castles.erase( mapid );
}

void Guild::AddEnroll( uint32 mapid )
{
	m_enroll_castles.insert( mapid );
}
