#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Guild.h"
#include "../../SDBase/Protocol/S2C_Guild.h"
#include "SunyouCastle.h"
#include "InstanceQueue.h"
#include "MultiLanguageStringMgr.h"
#include "ArenaTeam.h"

void WorldSession::HandleGuildQuery(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Query MsgRecv;packet>>MsgRecv;

	// we can skip some searches here if this is our guild
	if(_player && _player->GetGuildId() == MsgRecv.guild_id && _player->m_playerInfo->guild) {
		_player->m_playerInfo->guild->SendGuildQuery(this);
		return;
	}
	
	Guild * pGuild = objmgr.GetGuild( MsgRecv.guild_id );
	if(!pGuild)
		return;

	pGuild->SendGuildQuery(this);
}

void WorldSession::HandleCreateGuild(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Create MsgRecv;packet>>MsgRecv;

	if(_player->GetTradeTarget())
	{
		//SendNotification("交易状态不能执行这项操作");
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_0 );
		return;
	}

	if( MsgRecv.guild_name.length() > 50 )
	{
		//SendNotification( "部族名称太长" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_1 );
		return;
	}
	if( !is_valid_string( MsgRecv.guild_name ) )
	{
		//SendNotification( "部族名称含有非法字符" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_2 );
		return;
	}

	if(_player->m_playerInfo->guild)
	{
		MyLog::log->alert("player[%d][%s] who is creating guild is already in a guild", _player->GetLowGUID(), _player->m_playerInfo->name);

		//SendNotification( "你已经在部族里,请先脱离部族" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_3 );
		return;
	}

	if( (uint32)UNIXTIME - _player->GetLeaveGuildDate() < 24*3600 )
	{
		//SendNotification( "你24小时内加入过其他部族,请耐心等待" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_4 );
		return;
	}

	Guild * g = objmgr.GetGuildByGuildName( MsgRecv.guild_name );
	Charter * c = objmgr.GetCharterByName( MsgRecv.guild_name, CHARTER_TYPE_GUILD );
	if(g != 0 || c != 0)
	{
		//SendNotification( "此部族名字已经被使用" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_5 );
		return;
	}

	//1金
	if(_player->GetUInt32Value(PLAYER_FIELD_COINAGE) < 100000)
	{
		//SendNotification( "你必须拥有至少10金币" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_6 );
		return;
	}

	//10级
	if(_player->getLevel() < 10)
	{
		//SendNotification( "你的等级必须大于等于10级" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_7 );
		return;	
	}

	_player->ModCoin(-100000);

	Guild* pGuild = Guild::Create();
	pGuild->CreateFromPlayer(_player->m_playerInfo, MsgRecv.guild_name.c_str());
	if(!_player->HasSpell(88))
	{
		_player->addSpell(88);
	}
	sHookInterface.OnGuildCreate(_player, pGuild);
	MyLog::log->debug("player[%d][%s] created guild[%d][%s]", _player->GetLowGUID(), _player->m_playerInfo->name, pGuild->GetGuildId(), MsgRecv.guild_name.c_str());
}

/*void WorldSession::SendGuildCommandResult(uint32 typecmd,const char *  str,uint32 cmdresult)
{
	WorldPacket data;
	data.SetOpcode(SMSG_GUILD_COMMAND_RESULT);
	data << typecmd;
	data << str;
	data << cmdresult;
	SendPacket(&data);
}*/

void WorldSession::HandleInviteToGuild(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Invite MsgRecv;packet>>MsgRecv;
	std::string inviteeName = MsgRecv.inviteeName;

	Player *plyr = objmgr.GetPlayer( inviteeName.c_str() , false);
	Guild *pGuild = _player->m_playerInfo->guild;
	
	if(!plyr)
	{
		//Guild::SendGuildCommandResult(this, GUILD_INVITE_S,inviteeName.c_str(),GUILD_PLAYER_NOT_FOUND);
		//SendNotification( "没有找到该玩家" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_8 );
		return;
	}
	else if(!pGuild)
	{
		//Guild::SendGuildCommandResult(this, GUILD_CREATE_S,"",GUILD_PLAYER_NOT_IN_GUILD);
		//SendNotification( "你不属于任何一个部族" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_9 );
		return;
	}

	if( plyr->GetGuildId() )
	{
		//Guild::SendGuildCommandResult(this, GUILD_INVITE_S,plyr->GetName(),ALREADY_IN_GUILD);
		//SendNotification( "该玩家已经加入了一个部族" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_10 );
		return;
	}
	else if( plyr->GetGuildInvitersGuid())
	{
		//Guild::SendGuildCommandResult(this, GUILD_INVITE_S,plyr->GetName(),ALREADY_INVITED_TO_GUILD);
		//SendNotification( "该玩家已经被别人邀请加入部族" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_11 );
		return;
	}
	else if(!_player->m_playerInfo->guildRank->CanPerformCommand(GR_RIGHT_INVITE))
	{
		//Guild::SendGuildCommandResult(this, GUILD_INVITE_S,"",GUILD_PERMISSIONS);
		//SendNotification( "你没有权限邀请别人加入部族" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_12 );
		return;
	}
	/*
	else if(plyr->getRace() != _player->getRace())
	{
		//Guild::SendGuildCommandResult(this, GUILD_INVITE_S,"",GUILD_NOT_ALLIED);
		//SendNotification( "该玩家和你不是同一种族" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_13 );
		return;
	}
	*/
	else if( (uint32)UNIXTIME - plyr->GetLeaveGuildDate() < 24*3600 )
	{
		//SendNotification( "该玩家退出部族还没达到24小时" );
		SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_GUILD_HANDLER_14 );
		return;
	}
	Guild::SendGuildCommandResult(this, GUILD_INVITE_S,inviteeName.c_str(),GUILD_U_HAVE_INVITED);
	//41
  
	MSG_S2C::stGuild_Invite Msg;
	Msg.inviteeName = _player->GetName();
	Msg.guildName = pGuild->GetGuildName();
	plyr->GetSession()->SendPacket(Msg);

	plyr->SetGuildInvitersGuid( _player->GetLowGUID() );	
}

void WorldSession::HandleGuildAccept(CPacketUsn& packet)
{
	Player *plyr = GetPlayer();

	if(!plyr)
		return;

	Player *inviter = objmgr.GetPlayer( plyr->GetGuildInvitersGuid() );
	plyr->UnSetGuildInvitersGuid();

	if(!inviter)
	{
		return;
	}

	Guild *pGuild = inviter->m_playerInfo->guild;
	if(!pGuild)
	{
		return;
	}

	pGuild->AddGuildMember(plyr->m_playerInfo, NULL);
}

void WorldSession::HandleGuildDecline(CPacketUsn& packet)
{
	Player *plyr = GetPlayer();

	if(!plyr)
		return;

	Player *inviter = objmgr.GetPlayer( plyr->GetGuildInvitersGuid() );
	plyr->UnSetGuildInvitersGuid(); 

	if(!inviter)
		return;

	MSG_S2C::stGuild_Decline Msg;
	Msg.player_who_invite = plyr->GetName();
	inviter->GetSession()->SendPacket(Msg);
}

void WorldSession::HandleSetGuildInformation(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Set_Info MsgRecv;packet>>MsgRecv;
	std::string NewGuildInfo = MsgRecv.NewGuildInfo;

	Guild *pGuild = _player->m_playerInfo->guild;
	if(!pGuild)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S,"",GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	pGuild->SetGuildInformation(NewGuildInfo.c_str(), this);
}

void WorldSession::HandleGuildInfo(CPacketUsn& packet)
{
	if(_player->GetGuild() != NULL)
		_player->GetGuild()->SendGuildInfo(this);
}

void WorldSession::HandleGuildRoster(CPacketUsn& packet)
{
	if(!_player->m_playerInfo->guild)
		return;

	_player->m_playerInfo->guild->SendGuildRoster(this);
}

void WorldSession::HandleGuildPromote(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Promote MsgRecv;packet>>MsgRecv;

	std::string name = MsgRecv.name;

	if(!_player->m_playerInfo->guild)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S,"",GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	PlayerInfo * dstplr = objmgr.GetPlayerInfoByName(name.c_str());
	if(dstplr==NULL)
		return;

	_player->m_playerInfo->guild->PromoteGuildMember(dstplr, this);
}

void WorldSession::HandleGuildDemote(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Demote MsgRecv;packet>>MsgRecv;

	std::string name = MsgRecv.name;

	if(!_player->m_playerInfo->guild)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S,"",GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	PlayerInfo * dstplr = objmgr.GetPlayerInfoByName(name.c_str());
	if(dstplr==NULL)
		return;

	_player->m_playerInfo->guild->DemoteGuildMember(dstplr, this);
}

void WorldSession::HandleGuildSetMemberRank( CPacketUsn& packet )
{
	MSG_C2S::stGuildSetMemberRank MsgRecv;
	packet>>MsgRecv;
	
	if(!_player->m_playerInfo->guild)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S,"",GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	if (_player->GetGuild()&&_player->GetGuild()->GetGuildLeader()!= _player->GetGUID())
	{
		return;

	}
	PlayerInfo * dstplr = objmgr.GetPlayerInfoByName(MsgRecv.member.c_str());
	if(dstplr==NULL)
		return;

	_player->m_playerInfo->guild->SetGuildMemberRank(dstplr, this, MsgRecv.rank);
}

void WorldSession::HandleQueryGuildListReq(CPacketUsn& packet)
{
	if( _player->m_playerInfo->guildRank && _player->m_playerInfo->guildRank->iId == 0 )
	{
		Guild::SendGuildListInfo( this );
	}

}

void WorldSession::HandleGuildDeclareWar(CPacketUsn& packet)
{
	if( _player->m_playerInfo->guild )
	{
		MSG_C2S::stGuildDeclareWar msg;
		packet >> msg;
		_player->m_playerInfo->guild->DeclareWar( this, msg.hostileguild );
	}
}

void WorldSession::HandleGuildAllyReq(CPacketUsn& packet)
{
	if( _player->m_playerInfo->guild )
	{
		MSG_C2S::stGuildAllyReq msg;
		packet >> msg;
		_player->m_playerInfo->guild->AllyReq( this, msg.allianceguild );
	}
}

void WorldSession::HandleGuildLeave(CPacketUsn& packet)
{
	if(!_player->m_playerInfo->guild)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S,"",GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	_player->m_playerInfo->guild->RemoveGuildMember(_player->m_playerInfo, this);
}

void WorldSession::HandleGuildRemove(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Remove MsgRecv;packet>>MsgRecv;

	std::string name = MsgRecv.name;

	if(!_player->m_playerInfo->guild)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S,"",GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	PlayerInfo * dstplr = objmgr.GetPlayerInfoByName(name.c_str());
	if(dstplr==NULL)
		return;

	_player->m_playerInfo->guild->RemoveGuildMember(dstplr, this);
}

void WorldSession::HandleGuildDisband(CPacketUsn& packet)
{
	if(!_player->m_playerInfo->guild)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S,"",GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	if(_player->m_playerInfo->guild->GetGuildLeader() != _player->GetLowGUID())
	{
		Guild::SendGuildCommandResult(this, GUILD_INVITE_S, "", GUILD_PERMISSIONS);
		return;
	}

	_player->m_playerInfo->guild->Disband();
}

void WorldSession::HandleGuildLeader(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Set_Leader MsgRecv;packet>>MsgRecv;

	std::string name = MsgRecv.name;

	if(!_player->m_playerInfo->guild)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S,"",GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	PlayerInfo * dstplr = objmgr.GetPlayerInfoByName(name.c_str());
	if(dstplr==NULL)
		return;

	_player->m_playerInfo->guild->ChangeGuildMaster(dstplr, this);
}

void WorldSession::HandleGuildMotd(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Set_Motd MsgRecv;packet>>MsgRecv;
	std::string motd = MsgRecv.motd;

	if(!_player->m_playerInfo->guild)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S,"",GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	_player->m_playerInfo->guild->SetMOTD(motd.c_str(), this);
}

void WorldSession::HandleGuildRank(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Set_Bank MsgRecv;packet>>MsgRecv;

	if (MsgRecv.rankId == 0)
	{
		return;
	}

	if(!_player->m_playerInfo->guild)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S,"",GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	if(GetPlayer()->GetLowGUID() != _player->m_playerInfo->guild->GetGuildLeader())
	{
		Guild::SendGuildCommandResult(this, GUILD_INVITE_S,"",GUILD_PERMISSIONS);
		return;
	}

	uint32 rankId = MsgRecv.rankId;
	string newName;
	uint32 i;
	GuildRank * pRank;

	pRank = _player->m_playerInfo->guild->GetGuildRank(rankId);
	if(pRank == NULL)
		return;

	pRank->iRights = MsgRecv.iRights;
	newName = MsgRecv.newName;

	if(newName.length() < 2)
		newName = string(pRank->szRankName);
	
	if(strcmp(newName.c_str(), pRank->szRankName) != 0)
	{
		// name changed
		char * pTmp = pRank->szRankName;
		pRank->szRankName = strdup(newName.c_str());
		free(pTmp);
	}

	pRank->iGoldLimitPerDay = MsgRecv.iGoldLimitPerDay;

	for(i = 0; i < MAX_GUILD_BANK_TABS; ++i)
	{
		pRank->iTabPermissions[i].iFlags = MsgRecv.Permissions[i].iFlags;
		pRank->iTabPermissions[i].iStacksPerDay = MsgRecv.Permissions[i].iStacksPerDay;
	}

	sWorld.ExecuteSqlToDBServer("REPLACE INTO guild_ranks VALUES(%u, %u, \"%s\", %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)",
		_player->m_playerInfo->guild->GetGuildId(), pRank->iId, CharacterDatabase.EscapeString(newName).c_str(),
		pRank->iRights, pRank->iGoldLimitPerDay,
		pRank->iTabPermissions[0].iFlags, pRank->iTabPermissions[0].iStacksPerDay,
		pRank->iTabPermissions[1].iFlags, pRank->iTabPermissions[1].iStacksPerDay,
		pRank->iTabPermissions[2].iFlags, pRank->iTabPermissions[2].iStacksPerDay,
		pRank->iTabPermissions[3].iFlags, pRank->iTabPermissions[3].iStacksPerDay,
		pRank->iTabPermissions[4].iFlags, pRank->iTabPermissions[4].iStacksPerDay,
		pRank->iTabPermissions[5].iFlags, pRank->iTabPermissions[5].iStacksPerDay);
}

void WorldSession::HandleGuildAddRank(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Add_Rank MsgRecv;packet>>MsgRecv;
	string rankName = MsgRecv.rankName;
	Guild * pGuild = _player->GetGuild();

	if(pGuild == NULL)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S, "", GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	if(pGuild->GetGuildLeader() != _player->GetLowGUID())
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S, "", GUILD_PERMISSIONS);
		return;
	}

	if(rankName.size() < 2)
		return;

	pGuild->CreateGuildRank(rankName.c_str(), GR_RIGHT_DEFAULT, false);

	// there is probably a command result for this. need to find it.
	pGuild->SendGuildQuery(NULL);
	pGuild->SendGuildRoster(this);
}

void WorldSession::HandleGuildDelRank(CPacketUsn& packet)
{
	Guild * pGuild = _player->GetGuild();

	if(pGuild == NULL)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S, "", GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	if(pGuild->GetGuildLeader() != _player->GetLowGUID())
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S, "", GUILD_PERMISSIONS);
		return;
	}

	pGuild->RemoveGuildRank(this);

	// there is probably a command result for this. need to find it.
	pGuild->SendGuildQuery(NULL);
	pGuild->SendGuildRoster(this);
}

void WorldSession::HandleGuildSetPublicNote(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Set_Public_Note MsgRecv;packet>>MsgRecv;
	string target = MsgRecv.target;
	string newnote = MsgRecv.newnote;

	PlayerInfo * pTarget = objmgr.GetPlayerInfoByName(target.c_str());
	if(pTarget == NULL)
		return;

	if(!pTarget->guild)
		return;

	pTarget->guild->SetPublicNote(pTarget, newnote.c_str(), this);
}

void WorldSession::HandleGuildSetOfficerNote(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Set_Officer_Note MsgRecv;packet>>MsgRecv;
	string target = MsgRecv.target;
	string newnote = MsgRecv.newnote;

	PlayerInfo * pTarget = objmgr.GetPlayerInfoByName(target.c_str());
	if(pTarget == NULL)
		return;

	if(!pTarget->guild)
		return;

	pTarget->guild->SetOfficerNote(pTarget, newnote.c_str(), this);
}

void WorldSession::HandleRequestGuildLevelUp(CPacketUsn& packet)
{
	Guild* pGuild = _player->GetGuild();
	MSG_C2S::stGuild_Request_Guild_Level_Up MsgRecv;
	packet >> MsgRecv;
	int nLevel = pGuild->GetLevel();
	int nItem = 0;
	if (pGuild->GetGuildLeader() != _player->GetLowGUID())
	{
		SendNotification( build_language_string( BuildString_OnlyGuildLeaderCanLevelUpGuild) );
	}
	else
	{
		pGuild->LevelUp(this);
	}

}

void WorldSession::HandleSaveGuildEmblem(CPacketUsn& packet)
{
	uint64 guid;
	Guild * pGuild = _player->GetGuild();
	int32 cost = MONEY_ONE_GOLD * 10;
	uint32 emblemStyle, emblemColor, borderStyle, borderColor, backgroundColor;

	MSG_C2S::stGuild_Save_Emblem MsgRecv;packet>>MsgRecv;
	guid = MsgRecv.guid;
	emblemStyle = MsgRecv.emblemStyle;
	emblemColor = MsgRecv.emblemColor;
	borderStyle = MsgRecv.borderStyle;
	borderColor = MsgRecv.borderColor;
	backgroundColor = MsgRecv.backgroundColor;
	MSG_S2C::stGuild_Save_Emblem Msg;
	Msg.guid = guid;

	CHECK_INWORLD_RETURN;
	//CHECK_GUID_EXISTS(guid);

	Msg.emblemStyle = emblemStyle;
	Msg.emblemColor = emblemColor;
	Msg.borderStyle = borderStyle;
	Msg.borderColor = borderColor;
	Msg.backgroundColor = backgroundColor;
	if(pGuild==NULL)
	{
		Msg.error = uint32(ERR_GUILDEMBLEM_NOGUILD);
		SendPacket(Msg);
		return;
	}

	if(pGuild->GetGuildLeader() != _player->GetLowGUID())
	{
		Msg.error = uint32(ERR_GUILDEMBLEM_NOTGUILDMASTER);
		SendPacket(Msg);
		return;
	}

	if(_player->GetUInt32Value(PLAYER_FIELD_COINAGE) < (uint32)cost)
	{
		Msg.error = uint32(ERR_GUILDEMBLEM_NOTENOUGHMONEY);
		SendPacket(Msg);
		return;
	}

	Msg.error = uint32(ERR_GUILDEMBLEM_SUCCESS);
	SendPacket(Msg);

	// set in memory and database
	pGuild->SetTabardInfo(emblemStyle, emblemColor, borderStyle, borderColor, backgroundColor);

	// update all clients (probably is an event for this, again.)
	pGuild->SendGuildQuery(NULL);
}

// Charter part
void WorldSession::HandleCharterBuy(CPacketUsn& packet)
{
	/*
	{CLIENT} Packet: (0x01BD) CMSG_PETITION_BUY PacketSize = 85
	|------------------------------------------------|----------------|
	|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
	|------------------------------------------------|----------------|
	|50 91 00 00 6E 13 01 F0 00 00 00 00 00 00 00 00 |P...n...........|
	|00 00 00 00 53 74 6F 72 6D 62 72 69 6E 67 65 72 |....Stormbringer|
	|73 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |s...............|
	|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
	|00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 |................|
	|00 00 00 00 00								  |.....		   |
	-------------------------------------------------------------------
	*/

	uint64 creature_guid;
	//uint64 crap;
	//uint32 crap2;
	string name;
    uint8 error;
	uint32 /*crap3,crap4,crap5,crap6,crap7,crap8,crap9,crap10,crap11,*/arena_index/*,crap12*/;
// 	uint16 crap13;
// 	uint8 crap14;
// 	uint32 crap15;
	MSG_C2S::stGuild_Petition_Buy MsgRecv;packet>>MsgRecv;
	creature_guid = MsgRecv.creature_guid;
	name = MsgRecv.name;
	arena_index = MsgRecv.arena_index;

	Creature * crt = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(creature_guid));
	if(!crt)
	{
		Disconnect();
		return;
	}

	if(crt->GetEntry()==19861 || crt->GetEntry()==18897 || crt->GetEntry()==19856)		/* i am lazy! */
	{
		uint32 arena_type = arena_index - 1;
		if(arena_type > 2)
			return;

		if(_player->m_arenaTeams[arena_type] || _player->m_charters[arena_index])
		{
			SendNotification("You are already in an arena team.");
			return;
		}

		ArenaTeam * t = objmgr.GetArenaTeamByName(name, arena_type);
		if(t != NULL)
		{
			sChatHandler.SystemMessage(this,"That name is already in use.");
			return;
		}

		if(objmgr.GetCharterByName(name, (CharterTypes)arena_index))
		{
			sChatHandler.SystemMessage(this,"That name is already in use.");
			return;
		}

		if(_player->m_charters[arena_type])
		{
			SendNotification("You already have an arena charter.");
			return;
		}

		static uint32 item_ids[] = {ARENA_TEAM_CHARTER_2v2, ARENA_TEAM_CHARTER_3v3, ARENA_TEAM_CHARTER_5v5};
		static uint32 costs[] = {ARENA_TEAM_CHARTER_2v2_COST,ARENA_TEAM_CHARTER_3v3_COST,ARENA_TEAM_CHARTER_5v5_COST};

		if(_player->GetUInt32Value(PLAYER_FIELD_COINAGE) < costs[arena_type])
			return;			// error message needed here

		ItemPrototype * ip = ItemPrototypeStorage.LookupEntry(item_ids[arena_type]);
		ASSERT(ip);
		SlotResult res = _player->GetItemInterface()->FindFreeInventorySlot(ip);
		if(res.Result == 0)
		{
			_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_INVENTORY_FULL);
			return;
		}

		error = _player->GetItemInterface()->CanReceiveItem(ip,1);
		if(error)
		{
			_player->GetItemInterface()->BuildInventoryChangeError(NULL,NULL,error);
		}
		else
		{
			// Create the item and charter
			Item * i = objmgr.CreateItem(item_ids[arena_type], _player);
			Charter * c = objmgr.CreateCharter(_player->GetLowGUID(), (CharterTypes)arena_index);
			c->GuildName = name;
			c->ItemGuid = i->GetGUID();

			i->SetUInt32Value(ITEM_FIELD_STACK_COUNT, 1);
			i->SetUInt32Value(ITEM_FIELD_FLAGS, 1);
			i->SetUInt32Value(ITEM_FIELD_ENCHANTMENT, c->GetID());
			i->SetUInt32Value(ITEM_FIELD_PROPERTY_SEED, 57813883);
			if( !_player->GetItemInterface()->AddItemToFreeSlot(i) )
			{
				c->Destroy();
				delete i;
				return;
			}

			c->SaveToDB();

			/*WorldPacket data(45);
			BuildItemPushResult(&data, _player->GetGUID(), ITEM_PUSH_TYPE_RECEIVE, 1, item_ids[arena_type], 0);
			SendPacket(&data);*/
			SendItemPushResult(i, false, true, false, true, _player->GetItemInterface()->LastSearchItemBagSlot(), _player->GetItemInterface()->LastSearchItemSlot(), 1);
			_player->ModCoin(-(int32)costs[arena_type]);
			_player->m_charters[arena_index] = c;
			_player->SaveToDB(false);
		}
	}
	else
	{
		Guild * g = objmgr.GetGuildByGuildName(name);
		Charter * c = objmgr.GetCharterByName(name, CHARTER_TYPE_GUILD);
		if(g != 0 || c != 0)
		{
			SendNotification("A guild with that name already exists.");
			return;
		}

		if(_player->m_charters[CHARTER_TYPE_GUILD])
		{
			SendNotification("You already have a guild charter.");
			return;
		}

		ItemPrototype * ip = ItemPrototypeStorage.LookupEntry(ITEM_ENTRY_GUILD_CHARTER);
		assert(ip);
		SlotResult res = _player->GetItemInterface()->FindFreeInventorySlot(ip);
		if(res.Result == 0)
		{
			_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_INVENTORY_FULL);
			return;
		}

		error = _player->GetItemInterface()->CanReceiveItem(ItemPrototypeStorage.LookupEntry(ITEM_ENTRY_GUILD_CHARTER),1);
		if(error)
		{
			_player->GetItemInterface()->BuildInventoryChangeError(NULL,NULL,error);
		}
		else
		{
			// Meh...
			MSG_S2C::stPlay_Object_Sound Msg;
			Msg.creature_guid = creature_guid;
			Msg.value = uint32(0x000019C2);
			SendPacket(Msg);

			// Create the item and charter
			Item * i = objmgr.CreateItem(ITEM_ENTRY_GUILD_CHARTER, _player);
			c = objmgr.CreateCharter(_player->GetLowGUID(), CHARTER_TYPE_GUILD);
			c->GuildName = name;
			c->ItemGuid = i->GetGUID();


			i->SetUInt32Value(ITEM_FIELD_STACK_COUNT, 1);
			i->SetUInt32Value(ITEM_FIELD_FLAGS, 1);
			i->SetUInt32Value(ITEM_FIELD_ENCHANTMENT, c->GetID());
			i->SetUInt32Value(ITEM_FIELD_PROPERTY_SEED, 57813883);
			if( !_player->GetItemInterface()->AddItemToFreeSlot(i) )
			{
				c->Destroy();
				delete i;
				return;
			}

			c->SaveToDB();

			/*data.clear();
			data.resize(45);
			BuildItemPushResult(&data, _player->GetGUID(), ITEM_PUSH_TYPE_RECEIVE, 1, ITEM_ENTRY_GUILD_CHARTER, 0);
			SendPacket(&data);*/
			SendItemPushResult(i, false, true, false, true, _player->GetItemInterface()->LastSearchItemBagSlot(), _player->GetItemInterface()->LastSearchItemSlot(), 1);

			_player->m_charters[CHARTER_TYPE_GUILD] = c;
			_player->SaveToDB(false);
		}
	}
}

void SendShowSignatures(Charter * c, uint64 i, Player * p)
{
	MSG_S2C::stGuild_Petition_Show_Sign Msg;
	Msg.item_guid = i;
	Msg.leader_guid = (uint64)c->GetLeader();
	Msg.player_id = c->GetID();
	for(uint32 j = 0; j < c->Slots; ++j)
	{
		if(c->Signatures[j] == 0) continue;
		Msg.vSign.push_back ( uint64(c->Signatures[j]) << uint32(1) );
	}
	p->GetSession()->SendPacket(Msg);
}

void WorldSession::HandleCharterShowSignatures(CPacketUsn& packet)
{
	Charter * pCharter;
	MSG_C2S::stGuild_Petition_Show_Sign MsgRecv;packet>>MsgRecv;
	uint64 item_guid = MsgRecv.item_guid;
	pCharter = objmgr.GetCharterByItemGuid(item_guid);
	
	if(pCharter)
		SendShowSignatures(pCharter, item_guid, _player);
}

void WorldSession::HandleCharterQuery(CPacketUsn& packet)
{
	/*
	{SERVER} Packet: (0x01C7) SMSG_PETITION_QUERY_RESPONSE PacketSize = 77
	|------------------------------------------------|----------------|
	|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
	|------------------------------------------------|----------------|
	|20 08 00 00 28 32 01 00 00 00 00 00 53 74 6F 72 | ...(2......Stor|
	|6D 62 72 69 6E 67 65 72 73 00 00 09 00 00 00 09 |mbringers.......|
	|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
	|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
	|00 00 00 00 00 00 00 00 00 00 00 00 00		  |.............   |
	-------------------------------------------------------------------
	
	uint32 charter_id
	uint64 leader_guid
	string guild_name
	uint8  0			<-- maybe subname? or some shit.. who knows
	uint32 9
	uint32 9
	uint32[9] signatures
	uint8  0
	uint8  0
	*/
//this is wrong there are 42 bytes after 9 9, 4*9+2=38 not 42,
	//moreover it can't signature, blizz uses always fullguid so it must be uin64
	//moreover this does not show ppl who signed this, for this purpose there is another opcde
	MSG_C2S::stGuild_Petition_Query MsgRecv;packet>>MsgRecv;
	uint32 charter_id = MsgRecv.charter_id;
	uint64 item_guid = MsgRecv.item_guid;
	/*Charter * c = objmgr.GetCharter(charter_id,CHARTER_TYPE_GUILD);
	if(c == 0)
		c = objmgr.GetCharter(charter_id, CHARTER_TYPE_ARENA_2V2);
	if(c == 0)
		c = objmgr.GetCharter(charter_id, CHARTER_TYPE_ARENA_3V3);
	if(c == 0)
		c = objmgr.GetCharter(charter_id, CHARTER_TYPE_ARENA_5V5);*/

	Charter * c = objmgr.GetCharterByItemGuid(item_guid);
	if(c == 0)
		return;

	MSG_S2C::stGuild_Petition_Query_Response Msg;
	Msg.chater_id = charter_id;
	Msg.leader_guid = (uint64)c->LeaderGuid;
	Msg.GuildName = c->GuildName;
	Msg.CharterType = CHARTER_TYPE_GUILD;
	if(c->CharterType == CHARTER_TYPE_GUILD)
	{
		Msg.slots = 9;
	}
	else
	{
		Msg.slots = c->Slots;
	}
	SendPacket(Msg);
}

void WorldSession::HandleCharterOffer( CPacketUsn& packet )
{
	MSG_C2S::stGuild_Offer_Petition MsgRecv;packet>>MsgRecv;
	uint32 shit = MsgRecv.shit;
	uint64 item_guid = MsgRecv.item_guid;
	uint64 target_guid = MsgRecv.target_guid;
	Charter * pCharter;
	
	if(!_player->IsInWorld()) return;
	Player * pTarget = _player->GetMapMgr()->GetPlayer((uint32)target_guid);
	pCharter = objmgr.GetCharterByItemGuid(item_guid);

	if(pTarget == 0 || pTarget->GetTeam() != _player->GetTeam() || pTarget == _player)
	{
		SendNotification("Target is of the wrong faction.");
		return;
	}

	if(!pTarget->CanSignCharter(pCharter, _player))
	{
		SendNotification("Target player cannot sign your charter for one or more reasons.");
		return;
	}

	SendShowSignatures(pCharter, item_guid, pTarget);
}

void WorldSession::HandleCharterSign( CPacketUsn& packet )
{
	MSG_C2S::stGuild_Petition_Sign MsgRecv;packet>>MsgRecv;
	uint64 item_guid = MsgRecv.item_guid;

	Charter * c = objmgr.GetCharterByItemGuid(item_guid);
	if(c == 0)
		return;

	for(uint32 i = 0; i < 9; ++i)
	{
		if(c->Signatures[i] == _player->GetGUID())
		{
			SendNotification("You have already signed that charter.");
			return;
		}
	}

	if(c->IsFull())
		return;

	c->AddSignature(_player->GetLowGUID());
	c->SaveToDB();
	_player->m_charters[c->CharterType] = c;
	_player->SaveToDB(false);

	Player * l = _player->GetMapMgr()->GetPlayer(c->GetLeader());
	if(l == 0)
		return;

	MSG_S2C::stGuild_Petition_Sign_Result Msg;
	Msg.item_guid = item_guid;
	Msg.target_guid = _player->GetGUID();
	l->GetSession()->SendPacket(Msg);
	Msg.target_guid = (uint64)c->GetLeader();
	SendPacket(Msg);
}

void WorldSession::HandleCharterTurnInCharter(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Turn_In_Petition MsgRecv;packet>>MsgRecv;
	uint64 mooguid = MsgRecv.mooguid;
	Charter * pCharter = objmgr.GetCharterByItemGuid(mooguid);
	if(!pCharter) return;

	if(pCharter->CharterType == CHARTER_TYPE_GUILD)
	{
		Charter * gc = _player->m_charters[CHARTER_TYPE_GUILD];
		if(gc == 0)
			return;
// 		if(gc->SignatureCount < 9 && Config.MainConfig.GetBoolDefault("Server", "RequireAllSignatures", false))
// 		{
// 			SendNotification("You don't have the required amount of signatures to turn in this petition.");
// 			return;
// 		}

		// dont know hacky or not but only solution for now
		// If everything is fine create guild

		Guild *pGuild = Guild::Create();
		pGuild->CreateFromCharter(gc, this);
		if(!_player->HasSpell(88))
		{
			_player->addSpell(88);
		}

		// Destroy the charter
		_player->m_charters[CHARTER_TYPE_GUILD] = 0;
		gc->Destroy();

		_player->GetItemInterface()->RemoveItemAmt(ITEM_ENTRY_GUILD_CHARTER, 1);
		sHookInterface.OnGuildCreate(_player, pGuild);
	}
	else
	{
		/* Arena charter - TODO: Replace with correct messages */
		ArenaTeam * team;
		uint32 type;
		uint32 i;
		uint32 icon, iconcolor, bordercolor, border, background;
		icon = MsgRecv.icon;
		iconcolor = MsgRecv.iconcolor;
		bordercolor = MsgRecv.bordercolor;
		border = MsgRecv.border;
		background = MsgRecv.background;

		switch(pCharter->CharterType)
		{
		case CHARTER_TYPE_ARENA_2V2:
			type = ARENA_TEAM_TYPE_2V2;
			break;

		case CHARTER_TYPE_ARENA_3V3:
			type = ARENA_TEAM_TYPE_3V3;
			break;

		case CHARTER_TYPE_ARENA_5V5:
			type = ARENA_TEAM_TYPE_5V5;
			break;

		default:
			SendNotification("Internal Error");
			return;
		}

		if(_player->m_arenaTeams[pCharter->CharterType-1] != NULL)
		{
			sChatHandler.SystemMessage(this, "You are already in an arena team.");
			return;
		}

		if(pCharter->SignatureCount < pCharter->GetNumberOfSlotsByType() /*&& Config.MainConfig.GetBoolDefault("Server", "RequireAllSignatures", false)*/)
		{
			sChatHandler.SystemMessage(this, "You don't have the required amount of signatures to turn in this petition.");
			return;
		}

		team = new ArenaTeam(type, objmgr.GenerateArenaTeamId());
		team->m_name = pCharter->GuildName;
		team->m_emblemColour = iconcolor;
		team->m_emblemStyle = icon;
		team->m_borderColour = bordercolor;
		team->m_borderStyle = border;
		team->m_backgroundColour = background;
		team->m_leader=_player->GetLowGUID();
		team->m_stat_rating=1500;
        
		objmgr.AddArenaTeam(team);
		objmgr.UpdateArenaTeamRankings();
		team->AddMember(_player->m_playerInfo);
		

		/* Add the members */
		for(i = 0; i < pCharter->SignatureCount; ++i)
		{
			PlayerInfo * info = objmgr.GetPlayerInfo(pCharter->Signatures[i]);
			if(info)
			{
				team->AddMember(info);
			}
		}

		_player->GetItemInterface()->SafeFullRemoveItemByGuid(mooguid);
		_player->m_charters[pCharter->CharterType] = NULL;
		pCharter->Destroy();
	}

	MSG_S2C::stGuild_Turn_In_Petition_Result Msg;
	SendPacket( Msg );
}

void WorldSession::HandleCharterRename(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Petition_Rename MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;
	string name = MsgRecv.name;

	Charter * pCharter = objmgr.GetCharterByItemGuid(guid);
	if(pCharter == 0)
		return;

	Guild * g = objmgr.GetGuildByGuildName(name);
	Charter * c = objmgr.GetCharterByName(name, (CharterTypes)pCharter->CharterType);
	if(c || g)
	{
		SendNotification("That name is in use by another guild.");
		return;
	}

	c = pCharter;
	c->GuildName = name;
	c->SaveToDB();
	MSG_S2C::stGuild_Petition_Rename Msg;
	Msg.guid = guid;
	Msg.name = name;
	SendPacket(Msg);
}

void WorldSession::HandleGuildLog(CPacketUsn& packet)
{
	if(!_player->m_playerInfo->guild)
		return;

	_player->m_playerInfo->guild->SendGuildLog(this);
}

void WorldSession::HandleGuildBankBuyTab(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Bank_Purchase_Tab MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;

	if(!_player->IsInWorld())
		return;

	if(!_player->m_playerInfo->guild)
		return;

	if(_player->m_playerInfo->guild->GetGuildLeader() != _player->GetLowGUID())
	{
		Guild::SendGuildCommandResult(this, GUILD_MEMBER_S, "", GUILD_PERMISSIONS);
		return;
	}

	if(_player->m_playerInfo->guild->GetBankTabCount() < 6)
	{
		//                                        tab1, tab2, tab3, tab4, tab5, tab6
		const static int32 GuildBankPrices[6] = { 100, 250,  500,  1000, 2500, 5000 };
		int32 cost = MONEY_ONE_GOLD * GuildBankPrices[_player->m_playerInfo->guild->GetBankTabCount()];

		if(_player->GetUInt32Value(PLAYER_FIELD_COINAGE) < (unsigned)cost)
			return;

		_player->ModCoin(-cost);
		_player->m_playerInfo->guild->BuyBankTab(this);
		_player->m_playerInfo->guild->LogGuildEvent(GUILD_EVENT_BANKTABBOUGHT, 1, "");
	}
}

void WorldSession::HandleGuildBankGetAvailableAmount(CPacketUsn& packet)
{
	// calculate using the last withdrawl blablabla
	if(_player->m_playerInfo->guildMember == NULL)
		return;

	uint32 money = _player->m_playerInfo->guild->GetBankBalance();
	uint32 avail = _player->m_playerInfo->guildMember->CalculateAvailableAmount();

	/* pls gm mi hero poor give 1 gold coin pl0x */
	MSG_S2C::stGuild_Bank_Get_Abailable_Amound Msg;
	Msg.money = uint32(money>avail ? avail : money);
	SendPacket(Msg);
}

void WorldSession::HandleGuildBankModifyTab(CPacketUsn& packet)
{
	GuildBankTab * pTab;
	MSG_C2S::stGuild_Bank_Modify_Tab MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;
	uint8 slot = MsgRecv.slot;
	string tabname = MsgRecv.tabname;
	string tabicon = MsgRecv.tabicon;
	char * ptmp;

	if(_player->m_playerInfo->guild==NULL)
		return;

	pTab = _player->m_playerInfo->guild->GetBankTab((uint32)slot);
	if(pTab==NULL)
		return;

	if(_player->m_playerInfo->guild->GetGuildLeader() != _player->GetLowGUID())
	{
		Guild::SendGuildCommandResult(this, GUILD_MEMBER_S, "", GUILD_PERMISSIONS);
		return;
	}

	if(tabname.size())
	{
		if( !(pTab->szTabName && strcmp(pTab->szTabName, tabname.c_str()) == 0) )
		{
			ptmp = pTab->szTabName;
			pTab->szTabName = strdup(tabname.c_str());
			if(ptmp)
				free(ptmp);
	
			sWorld.ExecuteSqlToDBServer("UPDATE guild_banktabs SET tabName = \"%s\" WHERE guildId = %u AND tabId = %u", 
				CharacterDatabase.EscapeString(tabname).c_str(), _player->m_playerInfo->guild->GetGuildId(), (uint32)slot);
		}
	}
	else
	{
		if(pTab->szTabName)
		{
			ptmp = pTab->szTabName;
			pTab->szTabName = NULL;
			if(ptmp)
				free(ptmp);

			sWorld.ExecuteSqlToDBServer("UPDATE guild_banktabs SET tabName = '' WHERE guildId = %u AND tabId = %u", 
				_player->m_playerInfo->guild->GetGuildId(), (uint32)slot);
		}
	}

	if(tabicon.size())
	{
		if( !(pTab->szTabIcon && strcmp(pTab->szTabIcon, tabicon.c_str()) == 0) )
		{
			ptmp = pTab->szTabIcon;
			pTab->szTabIcon = strdup(tabname.c_str());
			if(ptmp)
				free(ptmp);

			sWorld.ExecuteSqlToDBServer("UPDATE guild_banktabs SET tabIcon = \"%s\" WHERE guildId = %u AND tabId = %u", 
				CharacterDatabase.EscapeString(tabicon).c_str(), _player->m_playerInfo->guild->GetGuildId(), (uint32)slot);
		}
	}
	else
	{
		if(pTab->szTabIcon)
		{
			ptmp = pTab->szTabIcon;
			pTab->szTabIcon = NULL;
			if(ptmp)
				free(ptmp);

			sWorld.ExecuteSqlToDBServer("UPDATE guild_banktabs SET tabIcon = '' WHERE guildId = %u AND tabId = %u", 
				_player->m_playerInfo->guild->GetGuildId(), (uint32)slot);
		}
	}

	// update the client
	_player->m_playerInfo->guild->SendGuildBankInfo(this);
}

void WorldSession::HandleGuildBankWithdrawMoney(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Bank_Withdraw_Money MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;
	uint32 money = MsgRecv.money;

	if(_player->m_playerInfo->guild==NULL)
		return;

	_player->m_playerInfo->guild->WithdrawMoney(this, money);
}

void WorldSession::HandleGuildBankDepositMoney(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Bank_Deposit_Money MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;
	uint32 money = MsgRecv.money;

	if(_player->m_playerInfo->guild==NULL)
		return;

	_player->m_playerInfo->guild->DepositMoney(this, money);
}

void WorldSession::HandleGuildBankDepositItem(CPacketUsn& packet)
{
	uint64 guid;
	uint8 source_isfrombank;
	uint32 wtf;
	uint8 wtf2;
	uint32 i;

	uint8 dest_bank;
	uint8 dest_bankslot;
	Guild * pGuild = _player->m_playerInfo->guild;
	GuildMember * pMember = _player->m_playerInfo->guildMember;
	
	if(pGuild==NULL || pMember==NULL)
		return;

	MSG_C2S::stGuild_Bank_Deposit_Item MsgRecv;packet>>MsgRecv;
	guid = MsgRecv.guid;
	source_isfrombank = MsgRecv.source_isfrombank;
	dest_bank = MsgRecv.dest_bank;
	dest_bankslot = MsgRecv.dest_bankslot;
	wtf = MsgRecv.wtf;

	if(source_isfrombank)
	{
		GuildBankTab * pSourceTab;
		GuildBankTab * pDestTab;
		Item * pSourceItem;
		Item * pDestItem;
		uint8 source_bank;
		uint8 source_bankslot;

		/* read packet */
		source_bank = MsgRecv.source_bank;
		source_bankslot = MsgRecv.source_bankslot;

		/* sanity checks to avoid overflows */
		if(source_bankslot >= MAX_GUILD_BANK_SLOTS ||
			dest_bankslot >= MAX_GUILD_BANK_SLOTS ||
			source_bank >= MAX_GUILD_BANK_TABS ||
			dest_bank >= MAX_GUILD_BANK_TABS)
		{
			return;
		}

		/* make sure we have permissions */
		if(!pMember->pRank->CanPerformBankCommand(GR_RIGHT_GUILD_BANK_DEPOSIT_ITEMS, dest_bank) || 
			!pMember->pRank->CanPerformBankCommand(GR_RIGHT_GUILD_BANK_DEPOSIT_ITEMS, source_bank))
			return;

		/* locate the tabs */
		pSourceTab = pGuild->GetBankTab((uint32)source_bank);
		pDestTab = pGuild->GetBankTab((uint32)dest_bank);
		if(pSourceTab == NULL || pDestTab == NULL)
			return;

		pSourceItem = pSourceTab->pSlots[source_bankslot];
		pDestItem = pDestTab->pSlots[dest_bankslot];

		if(pSourceItem == NULL && pDestItem == NULL)
			return;

		/* perform the actual swap */
		pSourceTab->pSlots[source_bankslot] = pDestItem;
		pDestTab->pSlots[dest_bankslot] = pSourceItem;

		/* update the client */
		if(pSourceTab == pDestTab)
		{
			/* send both slots in the packet */
			pGuild->SendGuildBank(this, pSourceTab, source_bankslot, dest_bankslot);
		}
		else
		{
			/* send a packet for each different bag */
			pGuild->SendGuildBank(this, pSourceTab, source_bankslot, -1);
			pGuild->SendGuildBank(this, pDestTab, dest_bankslot, -1);
		}

		/* update in sql */
		if(pDestItem == NULL)
		{
			/* this means the source slot is no longer being used. */
			sWorld.ExecuteSqlToDBServer("DELETE FROM guild_bankitems WHERE guildId = %u AND tabId = %u AND slotId = %u",
				pGuild->GetGuildId(), (uint32)pSourceTab->iTabId, (uint32)source_bankslot);
		}
		else
		{
			/* insert the new item */
			sWorld.ExecuteSqlToDBServer("REPLACE INTO guild_bankitems VALUES(%u, %u, %u, %u)", 
				pGuild->GetGuildId(), (uint32)pSourceTab->iTabId, (uint32)source_bankslot, pDestItem->GetLowGUID());
		}

		if(pSourceItem == NULL)
		{
			/* this means the destination slot is no longer being used. */
			sWorld.ExecuteSqlToDBServer("DELETE FROM guild_bankitems WHERE guildId = %u AND tabId = %u AND slotId = %u",
				pGuild->GetGuildId(), (uint32)pDestTab->iTabId, (uint32)dest_bankslot);
		}
		else
		{
			/* insert the new item */
			sWorld.ExecuteSqlToDBServer("REPLACE INTO guild_bankitems VALUES(%u, %u, %u, %u)", 
				pGuild->GetGuildId(), (uint32)pDestTab->iTabId, (uint32)dest_bankslot, pSourceItem->GetLowGUID());
		}
	}
	else
	{
		uint8 source_bagslot;
		uint8 source_slot;
		uint8 withdraw_stack=0;
		uint8 deposit_stack=0;
		GuildBankTab * pTab;
		Item * pSourceItem;
		Item * pDestItem;
		Item * pSourceItem2;

		/* read packet */
		wtf2 = MsgRecv.wtf2;
		if(wtf2)
			withdraw_stack = MsgRecv.withdraw_stack;

		source_bagslot = MsgRecv.source_bagslot;
		source_slot = MsgRecv.source_slot;

		if(!(source_bagslot == 1 && source_slot==0))
		{
			wtf2 = MsgRecv.wtf3;
			deposit_stack = MsgRecv.deposit_stack;
		}

		/* sanity checks to avoid overflows */
		if(dest_bank >= MAX_GUILD_BANK_TABS)
		{
			return;
		}

		/* make sure we have permissions */
		if(!pMember->pRank->CanPerformBankCommand(GR_RIGHT_GUILD_BANK_DEPOSIT_ITEMS, dest_bank))
			return;

		/* get tab */
		pTab = pGuild->GetBankTab((uint32)dest_bank);
		if(pTab==NULL)
			return;

		/* check if we are autoassigning */
		if(dest_bankslot == 0xff)
		{
			for(i = 0; i < MAX_GUILD_BANK_SLOTS; ++i)
			{
				if(pTab->pSlots[i] == NULL)
				{
					dest_bankslot = (uint8)i;
					break;
				}
			}

			if(dest_bankslot==0xff)
			{
				_player->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_BAG_FULL);
				return;
			}
		}

		/* another check here */
		if(dest_bankslot >= MAX_GUILD_BANK_SLOTS)
			return;

		/* check if we're pulling an item from the bank, make sure we're not cheating. */
		pDestItem = pTab->pSlots[dest_bankslot];

		/* grab the source/destination item */
		if(source_bagslot == 1 && source_slot == 0)
		{
			// find a free bag slot
			if(pDestItem == NULL)
			{
				// dis is fucked up mate
				return;
			}

			SlotResult sr = _player->GetItemInterface()->FindFreeInventorySlot(pDestItem->GetProto());
			if(!sr.Result)
			{
				_player->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_BAG_FULL);
				return;
			}

			source_bagslot = sr.ContainerSlot;
			source_slot = sr.Slot;
		}

		if(pDestItem != NULL)
		{
			if(pMember->pRank->iTabPermissions[dest_bank].iStacksPerDay == 0)
			{
				SystemMessage("You don't have permission to do that.");
				return;
			}

			if(pMember->pRank->iTabPermissions[dest_bank].iStacksPerDay > 0)
			{
				if(pMember->CalculateAllowedItemWithdraws(dest_bank) == 0)
				{
					// a "no permissions" notice would probably be better here
					SystemMessage("You have withdrawn the maximum amount for today.");
					return;
				}

				/* reduce his count by one */
				pMember->OnItemWithdraw(dest_bank);
			}
		}

		pSourceItem = _player->GetItemInterface()->GetInventoryItem(source_bagslot, source_slot);

		/* make sure that both arent null - wtf ? */
		if(pSourceItem == NULL && pDestItem == NULL)
			return;

		if(pSourceItem != NULL)
		{
			// make sure its not a soulbound item
			if(pSourceItem->IsSoulbound() || pSourceItem->GetProto()->Class == ITEM_CLASS_QUEST)
			{
				_player->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_CANT_DROP_SOULBOUND);
				return;
			}

			// pull the item from the slot
			if(deposit_stack && pSourceItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT) > deposit_stack)
			{
				pSourceItem2 = pSourceItem;
				pSourceItem = objmgr.CreateItem(pSourceItem2->GetEntry(), _player);
				pSourceItem->SetUInt32Value(ITEM_FIELD_STACK_COUNT, deposit_stack);
				pSourceItem->SetUInt32Value(ITEM_FIELD_CREATOR, pSourceItem2->GetUInt32Value(ITEM_FIELD_CREATOR));
				pSourceItem2->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, -(int32)deposit_stack);
				pSourceItem2->m_isDirty=true;
			}
			else
			{
				if(!_player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(source_bagslot, source_slot, false))
					return;

				pSourceItem->RemoveFromWorld();
			}
		}

		/* perform the swap. */
		/* pSourceItem = Source item from players backpack coming into guild bank */
		if(pSourceItem == NULL)
		{
			/* splitting */
			if(pDestItem != NULL && deposit_stack>0 && pDestItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT) > deposit_stack)
			{
				pSourceItem2 = pDestItem;

				pSourceItem2->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, -(int32)deposit_stack);
				pSourceItem2->SaveToDB(0,0,true, NULL);

				pDestItem = objmgr.CreateItem(pSourceItem2->GetEntry(), _player);
				pDestItem->SetUInt32Value(ITEM_FIELD_STACK_COUNT, deposit_stack);
				pDestItem->SetUInt32Value(ITEM_FIELD_CREATOR, pSourceItem2->GetUInt32Value(ITEM_FIELD_CREATOR));
			}
			else
			{
				/* that slot in the bank is now empty. */
				pTab->pSlots[dest_bankslot] = NULL;
				sWorld.ExecuteSqlToDBServer("DELETE FROM guild_bankitems WHERE guildId = %u AND tabId = %u AND slotId = %u",
					pGuild->GetGuildId(), (uint32)pTab->iTabId, (uint32)dest_bankslot);
			}			
		}
		else
		{
			/* there is a new item in that slot. */
			pTab->pSlots[dest_bankslot] = pSourceItem;
			sWorld.ExecuteSqlToDBServer("REPLACE INTO guild_bankitems VALUES(%u, %u, %u, %u)", 
				pGuild->GetGuildId(), (uint32)pTab->iTabId, (uint32)dest_bankslot, pSourceItem->GetLowGUID());

			/* remove the item's association with the player */
			pSourceItem->SetOwner(NULL);
			pSourceItem->SetUInt32Value(ITEM_FIELD_OWNER, 0);
			pSourceItem->SaveToDB(0, 0, true, NULL);

			/* log it */
			pGuild->LogGuildBankAction(GUILD_BANK_LOG_EVENT_DEPOSIT_ITEM, _player->GetLowGUID(), pSourceItem->GetEntry(), 
				(uint8)pSourceItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT), pTab);
		}

		/* pDestItem = Item from bank coming into players backpack */
		if(pDestItem == NULL)
		{
			/* the item has already been removed from the players backpack at this stage,
			   there isnt really much to do at this point. */			
		}
		else
		{
			/* the guild was robbed by some n00b! :O */
			pDestItem->SetOwner(_player);
			pDestItem->SetUInt32Value(ITEM_FIELD_OWNER, _player->GetLowGUID());
			pDestItem->SaveToDB(source_bagslot, source_slot, true, NULL);

			/* add it to him in game */
			if(!_player->GetItemInterface()->SafeAddItem(pDestItem, source_bagslot, source_slot))
			{
				/* this *really* shouldn't happen. */
				if(!_player->GetItemInterface()->AddItemToFreeSlot(pDestItem))
				{
					//pDestItem->DeleteFromDB();
					delete pDestItem;
				}
			}
			else
			{
				/* log it */
				pGuild->LogGuildBankAction(GUILD_BANK_LOG_EVENT_WITHDRAW_ITEM, _player->GetLowGUID(), pDestItem->GetEntry(), 
					(uint8)pDestItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT), pTab);
			}
		}

		/* update the clients view of the bank tab */
		pGuild->SendGuildBank(this, pTab, dest_bankslot);
	}		
}

void WorldSession::HandleGuildBankOpenVault(CPacketUsn& packet)
{
	GameObject * pObj;
	MSG_C2S::stGuild_Bank_Open MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;

	if(!_player->IsInWorld() || _player->m_playerInfo->guild==NULL)
	{
		Guild::SendGuildCommandResult(this, GUILD_CREATE_S, "", GUILD_PLAYER_NOT_IN_GUILD);
		return;
	}

	pObj = _player->GetMapMgr()->GetGameObject((uint32)guid);
	if(pObj==NULL)
		return;

	_player->m_playerInfo->guild->SendGuildBankInfo(this);
}

void WorldSession::HandleGuildBankViewTab(CPacketUsn& packet)
{
	MSG_C2S::stGuild_Bank_View_Tab MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;
	uint8 tabid = MsgRecv.tabid;
	GuildBankTab * pTab;
	Guild * pGuild = _player->m_playerInfo->guild;

	//Log.Warning("HandleGuildBankViewTab", "Tab %u", (uint32)tabid);

	// maybe last uint8 is "show additional info" such as tab names? *shrug*
	if(pGuild==NULL)
		return;

	pTab = pGuild->GetBankTab((uint32)tabid);
	if(pTab==NULL)
		return;

	pGuild->SendGuildBank(this, pTab);
}

void Guild::SendGuildBankInfo(WorldSession * pClient)
{
	GuildMember * pMember = pClient->GetPlayer()->m_playerInfo->guildMember;

	if(pMember==NULL)
		return;

	MSG_S2C::stGuild_Bank_View_Tab_Response Msg;
	Msg.bankBalance = m_bankBalance;
	Msg.iTabId = 0;
	Msg.AllowedItemWithdraws = 1;
	for(uint32 i = 0; i < m_bankTabCount; ++i)
	{
		GuildBankTab * pTab = GetBankTab(i);
		if(pTab==NULL || !pMember->pRank->CanPerformBankCommand(GR_RIGHT_GUILD_BANK_VIEW_TAB, i))
		{
			continue;
		}
		MSG_S2C::stGuild_Bank_View_Tab_Response::stTab guild_tab;

		if(pTab->szTabName)
			guild_tab.szTabName = pTab->szTabName;

		if(pTab->szTabIcon)
			guild_tab.szTabIcon = pTab->szTabIcon;
		Msg.vTabs.push_back( guild_tab );
	}

	pClient->SendPacket(Msg);
}

void Guild::SendGuildBank(WorldSession * pClient, GuildBankTab * pTab, ui8 updated_slot1 /* = -1 */, ui8 updated_slot2 /* = -1 */)
{
	//size_t pos;
	uint32 count=0;

	MSG_S2C::stGuild_Bank_View_Tab_Response Msg;
	GuildMember * pMember = pClient->GetPlayer()->m_playerInfo->guildMember;

	if(pMember==NULL || !pMember->pRank->CanPerformBankCommand(GR_RIGHT_GUILD_BANK_VIEW_TAB, pTab->iTabId))
		return;

	//Log.Debug("SendGuildBank", "sending tab %u to client.", pTab->iTabId);

	Msg.bankBalance = m_bankBalance;			// amount you have deposited
	Msg.iTabId = uint8(pTab->iTabId);				// unknown
	Msg.AllowedItemWithdraws = uint32(pMember->CalculateAllowedItemWithdraws(pTab->iTabId));		// remaining stacks for this day

	// no need to send tab names here..

	for(int32 j = 0; j < MAX_GUILD_BANK_SLOTS; ++j)
	{
		if(pTab->pSlots[j] != NULL)
		{
			if(updated_slot1 >= 0 && j == updated_slot1)
				updated_slot1 = -1;

			if(updated_slot2 >= 0 && j == updated_slot2)
				updated_slot2 = -1;

			++count;

			MSG_S2C::stGuild_Bank_View_Tab_Response::stTabItem guild_tabitem;
			// what i don't understand here, where is the field for random properties? :P - Burlex
			guild_tabitem.entry = pTab->pSlots[j]->GetEntry();
			guild_tabitem.enchant = uint32(0);			// this is an enchant
			guild_tabitem.stack_count = pTab->pSlots[j]->GetUInt32Value(ITEM_FIELD_STACK_COUNT);
		}
	}

	// send the forced update slots
	if(updated_slot1 >= 0)
	{
		// this should only be hit if the items null though..
		if(pTab->pSlots[updated_slot1]==NULL)
		{
			++count;
			Msg.updated_slot1 = uint8(updated_slot1);
		}
	}

	if(updated_slot2 >= 0)
	{
		// this should only be hit if the items null though..
		if(pTab->pSlots[updated_slot2]==NULL)
		{
			++count;
			Msg.updated_slot2 = uint8(updated_slot2);
		}
	}

	pClient->SendPacket(Msg);
}

void WorldSession::HandleGuildGetFullPermissions(CPacketUsn& packet)
{
	MSG_S2C::stGuild_Get_Full_Permissions Msg;
	GuildRank * pRank = _player->GetGuildRankS();
	uint32 i;

	if(_player->GetGuild() == NULL)
		return;

	Msg.rankId = pRank->iId;
	Msg.iRights = pRank->iRights;
	Msg.iGoldLimitPerDay = pRank->iGoldLimitPerDay;

	for(i = 0; i < MAX_GUILD_BANK_TABS; ++i) {
		Msg.Permissions[i].iFlags = pRank->iTabPermissions[i].iFlags;
		Msg.Permissions[i].iStacksPerDay = pRank->iTabPermissions[i].iStacksPerDay;
	}

	SendPacket(Msg);
}

void WorldSession::HandleGuildBankViewLog(CPacketUsn& packet)
{
	/* slot 6 = i'm requesting money log */
	MSG_C2S::stGuild_Bank_Log MsgRecv;packet>>MsgRecv;
	uint8 slotid = MsgRecv.slotid;

	if(_player->GetGuild() == NULL)
		return;

	_player->GetGuild()->SendGuildBankLog(this, slotid);
}

void WorldSession::HandleBuyCastleNPC(CPacketUsn& packet)
{
	if( !_player->IsInWorld() ) return;

	MSG_C2S::stBuyCastleNPC MsgRecv; packet >> MsgRecv;

	SunyouCastle* p = g_instancequeuemgr.GetCastleByMapID( MsgRecv.map_id );
	if( p )
	{
		p->BuyNPC( _player, MsgRecv.npc_id );
	}
}

void WorldSession::HandleQueryCastleState(CPacketUsn& packet)
{
	if( !_player->IsInWorld() ) return;
	
	MSG_S2C::stCastleStateAck ack;
	g_instancequeuemgr.BuildCastleState( ack );
	SendPacket( ack );
}

void WorldSession::HandleLadderQuery(CPacketUsn& packet)
{
	objmgr.SendLadderInfo2Player( this );
}
