#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_PVP.h"
#include "../../SDBase/Protocol/S2C_PVP.h"
#include "SunyouInstance.h"

void WorldSession::HandleSetVisibleRankOpcode(CPacketUsn& packet)
{
	MSG_C2S::stPVP_Set_Visible_Rank MsgRecv;packet>>MsgRecv;
	uint32 ChosenRank = MsgRecv.ChosenRank;
	if(ChosenRank == 0xFFFFFFFF)
		_player->SetUInt32Value(PLAYER_CHOSEN_TITLE, 0);
	else
		_player->SetUInt32Value(PLAYER_CHOSEN_TITLE, ChosenRank);
}

void HonorHandler::AddHonorPointsToPlayer(Player *pPlayer, uint32 uAmount)
{
	pPlayer->ModHonorCurrency(uAmount);
	pPlayer->ModTotalHonor(uAmount);
	return;
	/*if( pPlayer->GetMapId() == 559 || pPlayer->GetMapId() == 562 || pPlayer->GetMapId() == 572)
		return;
	pPlayer->m_honorPoints += uAmount;
	pPlayer->m_honorToday += uAmount;
	
	pPlayer->HandleProc(PROC_ON_GAIN_EXPIERIENCE, pPlayer, NULL);
	pPlayer->m_procCounter = 0;

	RecalculateHonorFields(pPlayer);*/


}

int32 HonorHandler::CalculateHonorPointsForKill( Player *pPlayer, Unit* pVictim )
{
	//return pVictim->getLevel() * 2 + 11;
	return pVictim->getLevel();
	/*
	// this sucks.. ;p
	if( pVictim == NULL )
	{
		int32 pts = rand() % 100 + 100;
		return pts;
	}

	// Suicide lol
	if( pVictim == pPlayer )
		return 0;

	if( pVictim->GetTypeId() != TYPEID_PLAYER )
		return 0;

	// How dishonorable, you fiend!
	if( pVictim->HasActiveAura( PLAYER_HONORLESS_TARGET_SPELL ) )
		return 0;

	uint32 k_level = pPlayer->GetUInt32Value( UNIT_FIELD_LEVEL );
	uint32 v_level = pVictim->GetUInt32Value( UNIT_FIELD_LEVEL );

	int k_honor = pPlayer->m_honorPoints;
	int v_honor = static_cast< Player* >( pVictim )->m_honorPoints;

	uint32 k_grey = 0;

	if( k_level > 5 && k_level < 40 )
	{
		k_grey = k_level - 5 - float2int32( floor( ((float)k_level) / 10.0f ) );
	}
	else
	{
		k_grey = k_level - 1 - float2int32( floor( ((float)k_level) / 5.0f ) );
	}

	if( k_honor == 0 )
		k_honor = 1;

	float diff_level = ((float)v_level - k_grey) / ((float)k_level - k_grey);
	if( diff_level > 2 ) diff_level = 2.0f;
	if( diff_level < 0 ) diff_level = 0.0f;

	float diff_honor = ((float)v_honor) / ((float)k_honor);
	if( diff_honor > 3 ) diff_honor = 3.0f;
	if( diff_honor < 0 ) diff_honor = 0.0f;

	float honor_points = diff_level * ( 150.0f + diff_honor * 60 );
	honor_points *= ((float)k_level) / 70.0f;
	honor_points *= World::getSingleton().getRate( RATE_HONOR );

	return float2int32( honor_points );
	*/
}

void HonorHandler::OnPlayerKilled( Player *pPlayer, Player* pVictim )
{
	if( pVictim == NULL || pPlayer == NULL || pVictim == pPlayer )
		return;
	if( pPlayer->getLevel() > pVictim->getLevel() && pPlayer->getLevel() - pVictim->getLevel() >= 10 )
		return;

	if( pPlayer->IsInWorld() && pPlayer->GetMapMgr()->m_sunyouinstance && pPlayer->GetMapMgr()->m_sunyouinstance->GetCategory() == INSTANCE_CATEGORY_TEAM_ARENA )
		return;

	pVictim->DropEquipmentOnGround( pPlayer );

	int32 Points = CalculateHonorPointsForKill(pPlayer, pVictim);

	std::set<Player*> s;
	Group* g = pPlayer->GetGroup();
	if( g )
		g->InsertNearbyMember2Set( pVictim, s );

	s.insert( pPlayer );

	if( !s.size() )
		return;

	int32 r = Points / s.size();
	if( r == 0 ) r = 1;

	for( std::set<Player*>::iterator it = s.begin(); it != s.end(); ++it )
	{
		Player* p = *it;

		uint32 reward_honor = (uint32)( (float)r * (float)p->m_ExtraHonorRatio / 100.f );
		p->ModHonorCurrency( reward_honor );
		p->ModTotalHonor( reward_honor );
		p->ModKillCount( 1 );
	}
	pVictim->ModHonorCurrency( -Points/2 );
	pVictim->ModBeKillCount( 1 );
}

void HonorHandler::OnCreatureKilled(Player *pPlayer, Creature* pVictim)
{
	if( pVictim == NULL || pPlayer == NULL )
		return;
	if( pPlayer->getLevel() - pVictim->getLevel() >= 10 )
		return;

	int32 Points = 0;
	creature_honor* ch = dbcCreatureHonorList.LookupEntry( pVictim->GetEntry() );
	if( ch )
	{
		Points = ch->honor;
	}
	else
		return;

	std::set<Player*> s;
	Group* g = pPlayer->GetGroup();
	if( g )
		g->InsertNearbyMember2Set( pVictim, s );

	s.insert( pPlayer );

	if( !s.size() )
		return;

	int32 r = Points / s.size();
	if( r == 0 ) r = 1;
	for( std::set<Player*>::iterator it = s.begin(); it != s.end(); ++it )
	{
		Player* p = *it;
		p->ModHonorCurrency( r );
		p->ModTotalHonor( r );
	}
}

void HonorHandler::RecalculateHonorFields(Player *pPlayer)
{
	// Why are we multiplying by 10.. ho well
//	pPlayer->SetUInt32Value(PLAYER_FIELD_KILLS, pPlayer->m_killsToday);
//	pPlayer->SetUInt32Value(PLAYER_FIELD_TODAY_CONTRIBUTION, pPlayer->m_honorToday);
//	pPlayer->SetUInt32Value(PLAYER_FIELD_YESTERDAY_CONTRIBUTION, pPlayer->m_killsYesterday | ( (pPlayer->m_honorYesterday * 10) << 16));
//	pPlayer->SetUInt32Value(PLAYER_FIELD_LIFETIME_HONORBALE_KILLS, pPlayer->m_killsLifetime);
//	pPlayer->SetUInt32Value(PLAYER_FIELD_HONOR_CURRENCY, pPlayer->m_honorPoints);
//	pPlayer->SetUInt32Value(PLAYER_FIELD_ARENA_CURRENCY, pPlayer->m_arenaPoints);
}

bool ChatHandler::HandleAddKillCommand(const char* args, WorldSession* m_session)
{
	uint32 KillAmount = args ? atol(args) : 1;
	Player *plr = getSelectedChar(m_session, true);
	if(plr == 0)
		return true;

	BlueSystemMessage(m_session, "Adding %u kills to player %s.", KillAmount, plr->GetName());
	GreenSystemMessage(plr->GetSession(), "You have had %u honor kills added to your character.", KillAmount);

//	for(uint32 i = 0; i < KillAmount; ++i)
//		HonorHandler::OnPlayerKilled(plr, 0);

	return true;
}

bool ChatHandler::HandleAddHonorCommand(const char* args, WorldSession* m_session)
{
	uint32 HonorAmount = args ? atol(args) : 1;
	Player *plr = getSelectedChar(m_session, true);
	if(plr == 0)
		return true;

	BlueSystemMessage(m_session, "Adding %u honor to player %s.", HonorAmount, plr->GetName());
	GreenSystemMessage(plr->GetSession(), "You have had %u honor points added to your character.", HonorAmount);

	HonorHandler::AddHonorPointsToPlayer(plr, HonorAmount);
	return true;
}

bool ChatHandler::HandlePVPCreditCommand(const char* args, WorldSession* m_session)
{
	return true ;
	uint32 Rank, Points;
	if(sscanf(args, "%u %u", (unsigned int*)&Rank, (unsigned int*)&Points) != 2)
	{
		RedSystemMessage(m_session, "Command must be in format <rank> <points>.");
		return true;
	}
	Points *= 10;
	uint64 Guid = m_session->GetPlayer()->GetSelection();
	if(Guid == 0)
	{
		RedSystemMessage(m_session, "A selection of a unit or player is required.");
		return true;
	}

	BlueSystemMessage(m_session, "Building packet with Rank %u, Points %u, GUID "I64FMT".", 
		Rank, Points, Guid);

	MSG_S2C::stPVP_Credit Msg;
	Msg.pvppoints = Points;
	Msg.Victim_guid = Guid;
	Msg.Victim_rank = Rank;
	m_session->SendPacket(Msg);
	return true;
}

bool ChatHandler::HandleGlobalHonorDailyMaintenanceCommand(const char* args, WorldSession* m_session)
{
	return false;
}

bool ChatHandler::HandleNextDayCommand(const char* args, WorldSession* m_session)
{
	return false;
}
