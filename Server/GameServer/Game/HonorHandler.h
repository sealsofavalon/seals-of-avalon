#ifndef HONORHANDLER_H
#define HONORHANDLER_H
class Player;
class Unit;
class HonorHandler
{
public:
	static int32 CalculateHonorPointsForKill(Player *pPlayer, Unit* pVictim);
	static void RecalculateHonorFields(Player *pPlayer);
	static void AddHonorPointsToPlayer(Player *pPlayer, uint32 uAmount);
	static void OnPlayerKilled(Player *pPlayer, Player* pVictim);
	static void OnCreatureKilled(Player *pPlayer, Creature* pVictim);
};


#endif
