#include "StdAfx.h"
#include "InstanceQueue.h"
#include "../../SDBase/Protocol/C2S.h"
#include "ObjectMgr.h"

void WorldSession::HandleQueueInstance(CPacketUsn& packet)
{
	MSG_C2S::stQueueInstance msg;
	packet >> msg;
	g_instancequeuemgr.JoinRequest( _player, msg.mapid, false );
}

void WorldSession::HandleLeaveQueueInstance(CPacketUsn& packet)
{
	MSG_C2S::stLeaveQueueInstance msg;
	packet >> msg;
	g_instancequeuemgr.LeaveRequest( _player, msg.mapid );


	Group* g = _player->GetGroup();
	if( g && g->GetLeader() == _player->m_playerInfo )
	{
		uint32 groups = g->GetSubGroupCount();
		for(uint32 i = 0; i < groups; i++)
		{
			SubGroup * sg = g->GetSubGroup(i);
			if(!sg) continue;

			GroupMembersSet::iterator git = sg->GetGroupMembersBegin();

			for( ; git != sg->GetGroupMembersEnd(); ++git )
			{
				PlayerInfo* pi = *git;
				if( pi->m_loggedInPlayer && pi->m_loggedInPlayer != _player )
					g_instancequeuemgr.LeavePlayer( pi->m_loggedInPlayer );
			}
		}
	}
}

void WorldSession::HandleEnterLeavePVPZone(CPacketUsn& packet)
{
	if( _player->m_sunyou_instance )
		return;
	if( _player->GetFFA() >= 11 && _player->GetFFA() <= 13 )
		return;
	if( _player->GetFFA() >= 21 && _player->GetFFA() <= 22 )
		return;

	MSG_C2S::stEnterLeavePVPZone msg;
	packet >> msg;
	if( msg.value < 3 )
	{
		_player->SetFFA( msg.value );
	}
	else
		this->Disconnect();
}
