#include "StdAfx.h"
#include "InstanceQueue.h"
#include "SunyouArena.h"
#include "SunyouMonsterGarden.h"
#include "SunyouDynamicInstance.h"
#include "SunyouEscape.h"
#include "SunyouPVPZone.h"
#include "SunyouCastle.h"
#include "SunyouBattleGround.h"
#include "SunyouTeamArena.h"
#include "SunyouRaid.h"
#include "SunyouAdvancedBattleGround.h"
#include "SunyouFairground.h"
#include "MultiLanguageStringMgr.h"
#include "GSC_InstanceQueue.h"
#include "GSP_InstanceQueue.h"

InstanceQueueMgr& g_instancequeuemgr = *new InstanceQueueMgr;

InstanceQueue::InstanceQueue( const sunyou_instance_configuration& conf, InstanceQueueMgr* father )
	: m_conf( conf ), m_father( father )
{

}

InstanceQueue::~InstanceQueue()
{
}

bool InstanceQueue::Check()
{
	if( m_players.size() >= (uint32)m_conf.maxplayer )
	{
		Start();
		return true;
	}
	return false;

}

void InstanceQueue::Join( Player* p )
{
	m_players.insert( p );
	p->AddQueueInstance( m_conf.mapid );
	MyLog::log->debug( "player:%s join instance queue, mapid:%d", p->GetName(), m_conf.mapid );
	Check();
}

void InstanceQueue::Leave( Player* p )
{
	m_players.erase( p );
	p->RemoveQueueInstance( m_conf.mapid );
	MyLog::log->debug( "player:%s leave instance queue, mapid:%d", p->GetName(), m_conf.mapid );
}

void InstanceQueue::Remove( Player* p )
{
	m_players.erase( p );
}

void InstanceQueue::Start()
{
	MapMgr* pMgr = sInstanceMgr.CreateBattlegroundInstance( m_conf.mapid );
	if( pMgr )
	{
		switch( m_conf.category )
		{
		case INSTANCE_CATEGORY_ARENA:
			pMgr->m_sunyouinstance = new SunyouArena;
			pMgr->m_sunyouinstance->Init( m_conf, pMgr );
			break;
		case INSTANCE_CATEGORY_BATTLE_GROUND:
			//pMgr->m_sunyouinstance = new SunyouBattleGround;
			//pMgr->m_sunyouinstance->Init( m_conf, pMgr );
			break;
		case INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND:
			//pMgr->m_sunyouinstance = new SunyouAdvancedBattleGround;
			//pMgr->m_sunyouinstance->Init( m_conf, pMgr );
			break;
		case INSTANCE_CATEGORY_TEAM_ARENA:
			//pMgr->m_sunyouinstance = new SunyouTeamArena;
			//pMgr->m_sunyouinstance->Init( m_conf, pMgr );
			break;
		case INSTANCE_CATEGORY_NEW_BATTLE_GROUND:
			{

			}
			break;
		default:
			assert( 0 );
			return;
		}
		std::set<Player*> tmp( m_players );
		std::set<uint32> factions;
		for( std::set<Player*>::iterator it = tmp.begin(); it != tmp.end(); ++it )
		{
			Player* p = *it;
			if( p->IsValid() )
			{
				factions.insert( p->getRace() - 1 );
				p->m_sunyou_instance = pMgr->m_sunyouinstance;
				if( m_conf.category != INSTANCE_CATEGORY_TEAM_ARENA )
					p->PrepareTeleportToSunyouInstance( m_conf.mapid, pMgr->GetInstanceID(), m_conf.random_vrl[rand() % m_conf.random_vrl.size()] );
				else
				{
					//SunyouTeamArena* sta = (SunyouTeamArena*)pMgr->m_sunyouinstance;
					//assert( p->m_team_arena_idx > 0 && p->m_team_arena_idx < 3 );
					//p->PrepareTeleportToSunyouInstance( m_conf.mapid, pMgr->GetInstanceID(), &(sta->m_ac->init_point[p->m_team_arena_idx - 1]) );
				}
			}
			else
				MyLog::log->debug( "queue instance start, player is not valid!!!" );
		}
		if( m_conf.category == INSTANCE_CATEGORY_BATTLE_GROUND )
		{
			if( factions.size() == 2 )
				static_cast<SunyouBattleGround*>( pMgr->m_sunyouinstance )->Set2Faction( *factions.begin(), *factions.rbegin() );
			return;
		}
		else if( m_conf.category == INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND )
		{
			return;
		}else if ( m_conf.category == INSTANCE_CATEGORY_NEW_BATTLE_GROUND)
		{
			return ;
		}
		else if( m_conf.category == INSTANCE_CATEGORY_TEAM_ARENA )
		{
			/*if( tmp.size() < 2 )
				static_cast<SunyouTeamArena*>( pMgr->m_sunyouinstance )->m_vs_type = 0;
			else
				static_cast<SunyouTeamArena*>( pMgr->m_sunyouinstance )->m_vs_type = (tmp.size() / 2) - 1;*/
			return;
		}
	}
	MyLog::log->notice( "create a sunyou instance mapid:%d", m_conf.mapid );
	m_father->RemoveQueue( this );
	delete this;
}

BattleGroundGroupQueue::BattleGroundGroupQueue( const sunyou_instance_configuration& conf, InstanceQueueMgr* father )
: InstanceQueue( conf, father )
{
}

BattleGroundGroupQueue::~BattleGroundGroupQueue()
{

}

bool BattleGroundGroupQueue::move_group2set( std::set<Player*>& dest, faction_player_list_t& source, uint32 count )
{
	typedef std::pair<int, std::set<Player*>*> search_unit_t;
	search_unit_t* p = new search_unit_t[source.size()];
	auto_array_ptr<search_unit_t> arp( p );
	int i = 0;
	for( faction_player_list_t::iterator it = source.begin(); it != source.end(); ++it )
	{
		std::set<Player*>& s( *it );
		p[i].first = s.size();
		p[i++].second = &s;
	}
	
	for( int i = source.size() - 1; i >= 0; --i )
	{
		std::vector<search_unit_t*> out;
		if( search_combine_checksum( p, source.size() - i, count, out ) )
		{
			for( std::vector<search_unit_t*>::iterator it = out.begin(); it != out.end(); ++it )
			{
				search_unit_t* su = *it;
				std::set<Player*>* s = su->second;
				dest.insert( s->begin(), s->end() );
				s->clear();
			}
			return true;
		}
	}
	return false;
}

bool BattleGroundGroupQueue::Check()
{
	/* support team queue
	std::set<Player*> dest[3];
	faction_player_list_t source[3];
	int count = 0;
	for( int i = 0; i < 3; ++i )
	{
		source[i] = m_faction_players[i];
		if( source[i].size() > 0 )
			if( move_group2set( dest[i], source[i], m_conf.bg_faction_max_player ) )
				++count;
	}
	int skip = -1;
	if( count > m_conf.bg_faction_count )
	{
		skip = rand() % 3;
	}
	else if( count < m_conf.bg_faction_count )
		return false;

	m_players.clear();
	for( int i = 0; i < 3; ++i )
	{
		if( i != skip )
		{
			m_players.insert( dest[i].begin(), dest[i].end() );
			m_faction_players[i] = source[i];
		}
	}
	*/

	// for beta test
	uint32 fcount = 0;
	m_players.clear();
	for( int i = 0; i < 3; ++i )
	{
		uint32 count = 0;
		for( faction_player_list_t::iterator it = m_faction_players[i].begin(); it != m_faction_players[i].end(); ++it )
		{
			std::set<Player*>& sp = *it;
			for( std::set<Player*>::iterator it2 = sp.begin(); it2 != sp.end(); ++it2 )
			{
				m_players.insert( *it2 );
				if( ++count >= m_conf.bg_faction_max_player )
					break;
			}
			if( count >= m_conf.bg_faction_max_player )
			{
				++fcount;
				break;
			}
		}
	}
	if( fcount >= m_conf.bg_faction_count )
	{
		Start();
		return true;
	}
	else
	{
		m_players.clear();
		return false;
	}
}

void BattleGroundGroupQueue::Join( Player* p )
{
	std::set<Player*> tmp;
	Group * pGroup = p->GetGroup();

	if( pGroup )
	{
		uint32 groups = pGroup->GetSubGroupCount();
		for(uint32 i = 0; i < groups; i++)
		{
			SubGroup * sg = pGroup->GetSubGroup(i);
			if(!sg) continue;

			for( GroupMembersSet::iterator it = sg->GetGroupMembersBegin(); it != sg->GetGroupMembersEnd(); ++it )
			{
				PlayerInfo* pi = *it;
				if( pi->m_loggedInPlayer )
				{
					if( pi->m_loggedInPlayer->IsInstanceBusy() )
					{
						//p->GetSession()->SystemMessage("玩家[%s]正在副本中,或者副本排队中", pi->name);
						p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueBusy, pi->name ) );
						return;
					}
					else if( pi->m_loggedInPlayer->getLevel() < m_conf.minlevel )
					{
						//p->GetSession()->SystemMessage("玩家[%s]级别不足%d", pi->name, m_conf.minlevel);
						p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueLowLevel, pi->name, quick_itoa(m_conf.minlevel) ) );
						return;
					}
					else if( pi->m_loggedInPlayer->getLevel() > m_conf.maxlevel )
					{
						//p->GetSession()->SystemMessage("玩家[%s]级别高于%d", pi->name, m_conf.maxlevel);
						p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueHighLevel, pi->name, quick_itoa(m_conf.maxlevel) ) );
						return;
					}
					else
						tmp.insert( pi->m_loggedInPlayer );
				}
				else
				{
					//p->GetSession()->SystemMessage("您队伍中有人不在线");
					p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_15 );
					return;
				}
			}
		}
	}
	else
		tmp.insert( p );

	for( std::set<Player*>::iterator it = tmp.begin(); it != tmp.end(); ++it )
	{
		Player* plr = *it;
		plr->AddQueueInstance( m_conf.mapid );
		plr->SetInstanceBusy( true );
		//plr->GetSession()->SendNotification( "已加入战场的队列中" );
		plr->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_16 );
	}

	m_faction_players[p->getRace() - 1].push_back( tmp );
	Check();
}

void BattleGroundGroupQueue::Leave( Player* p )
{
	Remove( p );

	if( p->HasInstanceQueue( m_conf.mapid ) )
	{
		//p->GetSession()->SendNotification( "战场的队列已取消" );
		p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_17 );
		p->RemoveQueueInstance( m_conf.mapid );
	}
	p->SetInstanceBusy( false );
}

void BattleGroundGroupQueue::Remove( Player* p )
{
	for( faction_player_list_t::iterator it = m_faction_players[p->getRace()-1].begin(); it != m_faction_players[p->getRace()-1].end(); ++it )
	{
		std::set<Player*>& set_src( *it );

		if( set_src.find( p ) != set_src.end() )
		{
			set_src.erase( p );
			if( set_src.size() == 0 )
			{
				m_faction_players[p->getRace()-1].erase( it );
				break;
			}
		}
	}
}

void BattleGroundGroupQueue::Start()
{
	InstanceQueue::Start();
	m_players.clear();
}

//////////////////////////////////////////////////////////////////////////

TeamArenaQueue::TeamArenaQueue( const sunyou_instance_configuration& conf, InstanceQueueMgr* father ) : InstanceQueue( conf, father )
{

}

TeamArenaQueue::~TeamArenaQueue()
{

}

bool TeamArenaQueue::Check()
{
	return true;
}

void TeamArenaQueue::Join( Player* p )
{
	Group* g = p->GetGroup();
	if( g && g->GetLeader() != p->m_playerInfo )
		return;

	team_t t;
	if( g )
	{
		if( g->MemberCount() > 5 )
		{
			//p->GetSession()->SendNotification( "您的队伍中人数超过5人,不可以加入竞技场队列" );
			p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_18 );
			return;
		}
		uint32 groups = g->GetSubGroupCount();
		for(uint32 i = 0; i < groups; i++)
		{
			SubGroup * sg = g->GetSubGroup(i);
			if(!sg) continue;

			for( GroupMembersSet::iterator it = sg->GetGroupMembersBegin(); it != sg->GetGroupMembersEnd(); ++it )
			{
				PlayerInfo* pi = *it;
				if( pi->m_loggedInPlayer )
				{
					if( m_conf.cost_type == 1 && m_conf.cost_value1 > 0 )
					{
						if( pi->m_loggedInPlayer->GetCardPoints() < m_conf.cost_value1 )
						{
							//p->GetSession()->SendNotification( "您队伍中有玩家元宝不足,不能进入竞技场" );
							p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_19 );
							return;
						}
					}

					if( pi->m_loggedInPlayer->getLevel() < m_conf.minlevel || pi->m_loggedInPlayer->getLevel() > m_conf.maxlevel )
					{
						//p->GetSession()->SendNotification( "您队伍中有玩家未满足进入竞技场的等级要求" );
						p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_20 );
						return;
					}
					if( pi->m_loggedInPlayer->IsInstanceBusy() )
					{
						//p->GetSession()->SendNotification( "您队伍中有玩家正在副本中或者正在副本排队中" );
						p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_21 );
						return;
					}
				}
				else
				{
					//p->GetSession()->SendNotification( "您队伍中有玩家下线了,不可以排队" );
					p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_22 );
					return;
				}
			}
		}

		for(uint32 i = 0; i < groups; i++)
		{
			SubGroup * sg = g->GetSubGroup(i);
			if(!sg) continue;
			for( GroupMembersSet::iterator it = sg->GetGroupMembersBegin(); it != sg->GetGroupMembersEnd(); ++it )
			{
				PlayerInfo* pi = *it;
				if( pi->m_loggedInPlayer )
				{
					t.players[t.member_count++] = pi->m_loggedInPlayer;
					pi->m_loggedInPlayer->AddQueueInstance( m_conf.mapid );
					pi->m_loggedInPlayer->SetInstanceBusy( true );

					q_players.insert( pi->m_loggedInPlayer );

					if( pi->m_loggedInPlayer->GetSession() )
					{
						//pi->m_loggedInPlayer->GetSession()->SendNotification( "已加入竞技场的队列中" );
						pi->m_loggedInPlayer->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_23 );
					}
				}
			}
		}
	}
	else
	{
		t.member_count = 1;
		t.players[0] = p;
		p->AddQueueInstance( m_conf.mapid );
		p->SetInstanceBusy( true );
		q_players.insert( p );

		if( p->GetSession() )
		{
			//p->GetSession()->SendNotification( "已加入竞技场的队列中" );
			p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_23 );
		}
	}
	m_teams.push_back( t );
	if( m_teams.size() >= 2 )
	{
		for( std::list<team_t>::iterator it = m_teams.begin(); it != m_teams.end(); ++it )
		{
			team_t& teamA = *it;
			std::list<team_t>::iterator it2 = it;
			
			for( ++it2; it2 != m_teams.end(); ++it2 )
			{
				team_t& teamB = *it2;
				if( teamA.member_count == teamB.member_count )
				{
					for( int i = 0; i < 5; ++i )
					{
						if( teamA.players[i] )
						{
							teamA.players[i]->m_team_arena_idx = 1;
							m_players.insert( teamA.players[i] );
						}
					}

					for( int i = 0; i < 5; ++i )
					{
						if( teamB.players[i] )
						{
							teamB.players[i]->m_team_arena_idx = 2;
							m_players.insert( teamB.players[i] );
						}
					}
					m_teams.erase( it );
					m_teams.erase( it2 );

					for( std::set<Player*>::iterator itp = m_players.begin(); itp != m_players.end(); )
					{
						std::set<Player*>::iterator itp2 = itp;
						Player* p = *itp2;
						++itp;
						if( m_conf.cost_type == 1 && m_conf.cost_value1 )
						{
							if( p->GetCardPoints() < m_conf.cost_value1 )
							{
								p->SetInstanceBusy( false );
								p->RemoveQueueInstance( m_conf.mapid );
								m_players.erase( itp2 );
								continue;
							}
							p->SpendPoints( m_conf.cost_value1, "team arena enter" );
							MyLog::yunyinglog->info("yuanbao-arena-player[%u][%s] enter spend [%u]", p->GetLowGUID(), p->GetName(), m_conf.cost_value1);
						}
					}
					if( m_players.size() > 0 )
						Start();
					return;
				}
			}
		}
	}
}

void TeamArenaQueue::Leave( Player* p )
{
	if( q_players.find( p ) != q_players.end() )
	{
		Remove( p );
		p->RemoveQueueInstance( m_conf.mapid );
		p->SetInstanceBusy( false );
		if( p->GetSession() )
		{
			//p->GetSession()->SendNotification( "竞技场队列已取消" );
			p->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_24 );
		}
		Group * pGroup = p->GetGroup();

		if( pGroup )
		{
			uint32 groups = pGroup->GetSubGroupCount();
			for(uint32 i = 0; i < groups; i++)
			{
				SubGroup * sg = pGroup->GetSubGroup(i);
				if(!sg) continue;

				for( GroupMembersSet::iterator it = sg->GetGroupMembersBegin(); it != sg->GetGroupMembersEnd(); ++it )
				{
					PlayerInfo* pi = *it;
					if( pi->m_loggedInPlayer && p != pi->m_loggedInPlayer && pi->m_loggedInPlayer->IsInstanceBusy() )
					{
						Remove( pi->m_loggedInPlayer );
						pi->m_loggedInPlayer->RemoveQueueInstance( m_conf.mapid );
						pi->m_loggedInPlayer->SetInstanceBusy( false );
						if( pi->m_loggedInPlayer->GetSession() )
						{
							//pi->m_loggedInPlayer->GetSession()->SendNotification( "竞技场队列已取消" );
							pi->m_loggedInPlayer->GetSession()->SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_INSTANCE_QUEUE_24 );
						}
					}
				}
			}
		}
	}
}

void TeamArenaQueue::Remove( Player* p )
{
	m_players.erase( p );
	for( std::list<team_t>::iterator it = m_teams.begin(); it != m_teams.end(); ++it )
	{
		team_t& t = *it;
		for( int i = 0; i < 5; ++i )
			if( t.players[i] == p )
			{
				t.players[i] = NULL;
				if( --t.member_count == 0 )
				{
					m_teams.erase( it );
				}
				return;
			}
	}
	q_players.erase( p );
}

void TeamArenaQueue::Start()
{
	InstanceQueue::Start();
	m_players.clear();
}

//////////////////////////////////////////////////////////////////////////

InstanceQueueMgr::InstanceQueueMgr() : m_silsg( NULL )
{
}

InstanceQueueMgr::~InstanceQueueMgr()
{
	for( std::vector<instance_queue*>::iterator it = m_iqv.begin(); it != m_iqv.end(); ++it )
	{
		instance_queue* iq = *it;
		for( std::vector<LocationVector*>::iterator it2 = iq->random_vrl.begin(); it2 != iq->random_vrl.end(); ++it2 )
			delete *it2;
	}

	if(m_silsg)
		delete m_silsg;

	SunyouBattleGround::ReleaseConfigurations();
}

void InstanceQueueMgr::Init()
{
	smart_ptr<QueryResult> qr = WorldDatabase.Query( "select * from sunyou_instance" );
	if( qr )
	{
		do
		{
			Field* f = qr->Fetch();
			instance_queue* iq = new instance_queue;
			iq->category = (instance_category_t)f[1].GetUInt32();
			iq->iconid = f[2].GetUInt32();
			iq->mapid = f[3].GetUInt32();
			iq->minlevel = f[4].GetUInt32();
			iq->maxlevel = f[5].GetUInt32();
			iq->minplayer = f[6].GetUInt32();
			iq->maxplayer = f[7].GetUInt32();
			iq->expire = f[8].GetUInt32() * 60;
			iq->iq = NULL;
			iq->gsc_iq = NULL;
			iq->gsp_iq = NULL;
			char lvstring[512] = { 0 };
			strcpy( lvstring, f[9].GetString() );
			iq->cost_type = f[10].GetUInt32();
			iq->cost_value1 = f[11].GetUInt32();
			iq->cost_value2 = f[12].GetUInt32();
			iq->enter_limit = f[13].GetUInt32();
			iq->bg_faction_min_player = f[14].GetUInt32();
			iq->bg_faction_max_player = f[15].GetUInt32();
			iq->bg_faction_count = f[16].GetUInt32();
			for( int i = 0; i < 6; ++i )
			{
				iq->open_start[i] = f[17+i*2].GetUInt32();
				iq->open_end[i] = f[17+i*2+1].GetUInt32();
			}
			iq->resurrect_time = f[29].GetUInt32();
			const char* s = f[30].GetString();
			iq->hide = f[31].GetUInt32();

			if( s[0] > 0 )
			{
				std::vector<std::string> v( SplitString( s, ';' ) );
				for( size_t i = 0; i < v.size(); ++i )
				{
#ifdef LUA_HOTFIX
					uint32 hash = crc32( (uint8*)v[i].c_str(), v[i].length() );
#else
					uint32 hash = LoadLuaFile( v[i].c_str() );
#endif
					iq->lua_scripts[hash] = v[i];
				}
			}


			char* start = lvstring;
			char* end = lvstring;
			float value[4] = { 0.f };
			int n = 0;
			while( true )
			{
				if( !*start ) break;
				end = strchr( start, ',' );
				if( end ) *end = 0;
				value[n] = atof( start );
				
				if( ++n >= 4 )
				{
					iq->random_vrl.push_back( new LocationVector( value[0], value[1], value[2], value[3] ) );
					n = 0;
				}
				if( !end ) break;
				start = end + 1;
				if( !*start ) break;
			}
			switch( iq->category )
			{
			case INSTANCE_CATEGORY_UNIQUE_PVP_ZONE:
				{
					MapMgr* pMgr = sInstanceMgr.CreateBattlegroundInstance( iq->mapid, iq->mapid );
					SunyouInstance* pInstance = new SunyouPVPZone;
					pInstance->Init( *iq, pMgr );
					pMgr->m_sunyouinstance = pInstance;
					m_map_unique_pvp_zone[iq->mapid] = pInstance;
					m_map_unique_pvp_zone_mgr[iq->mapid] = pMgr;
				}
				break;
			case INSTANCE_CATEGORY_UNIQUE_CASTLE:
				{
					MapMgr* pMgr = sInstanceMgr.CreateBattlegroundInstance( iq->mapid, iq->mapid );
					SunyouInstance* pInstance = new SunyouCastle;
					pInstance->Init( *iq, pMgr );
					pMgr->m_sunyouinstance = pInstance;
					m_map_unique_pvp_zone[iq->mapid] = pInstance;
					m_map_unique_pvp_zone_mgr[iq->mapid] = pMgr;
				}
				break;
			case INSTANCE_CATEGORY_BATTLE_GROUND:
			case INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND:
				{
// 					BattleGroundGroupQueue* q = new BattleGroundGroupQueue( *iq, this );
// 					iq->iq = q;
					if (sMaster.m_surpassserver)
					{
						GSP_BattleGroundGroup* q = new GSP_BattleGroundGroup(*iq, this);
						iq->gsp_iq = q;
					}else
					{
						GSC_InstanceQueue*q = new GSC_InstanceQueue(*iq, this);
						iq->gsc_iq = q;
					}
				}
				break;
			case INSTANCE_CATEGORY_NEW_BATTLE_GROUND:
				{
					if (sMaster.m_surpassserver)
					{
						GSP_TeamBattleGroundGroup* q = new GSP_TeamBattleGroundGroup(*iq, this);
						iq->gsp_iq = q;
					}else
					{
						GSC_InstanceQueue*q = new GSC_InstanceQueue(*iq, this);
						iq->gsc_iq = q;
					}
				}break;
			case INSTANCE_CATEGORY_TEAM_ARENA:
				{
					/*TeamArenaQueue* q = new TeamArenaQueue( *iq, this );*/
					//iq->iq = q;

					if (sMaster.m_surpassserver)
					{
						GSP_InstanceQueue* q = new GSP_InstanceQueue(*iq, this);
						iq->gsp_iq = q;
					}else
					{
						GSC_InstanceQueue*q = new GSC_InstanceQueue(*iq, this);
						iq->gsc_iq = q;
					}
				}
				break;
			default:
				break;
			}

			m_iqv.push_back( iq );
		}
		while( qr->NextRow() );
	}

	StorageContainerIterator<spawn_info>* it = dbcSunyouInstanceSpawnInfo.MakeIterator();
	while(!it->AtEnd())
	{
		spawn_info* si = it->Get();
		for( std::vector<instance_queue*>::iterator it2 = m_iqv.begin(); it2 != m_iqv.end(); ++it2 )
		{
			instance_queue* iq = *it2;
			if( si->mapid == iq->mapid )
			{
				iq->vsi.push_back( si );
				break;
			}
		}
		if(!it->Inc())
			break;
	}
	it->Destruct();

	SunyouBattleGround::LoadConfigurations();
	SunyouTeamArena::LoadConfigurations();
}

bool InstanceQueueMgr::JoinRequest( Player* p, uint32 mapid, bool isPrivate, uint32 player_limit )
{
	if( p->IsQueueInstanceFull() )
	{
		MyLog::log->debug( "[%s] instance queue is full", p->GetName() );
		return false;
	}
	if( p->IsInstanceBusy() )
	{
		MyLog::log->debug( "[%s] instance busy, cannot queue!", p->GetName() );
		return false;
	}

	for( std::vector<instance_queue*>::iterator it = m_iqv.begin(); it != m_iqv.end(); ++it )
	{
		instance_queue* i = *it;
		if( i->mapid == mapid && p->getLevel() >= (uint32)i->minlevel && p->getLevel() <= (uint32)i->maxlevel )
		{
			tm* ptm = localtime( &UNIXTIME );

			bool b = false;
			for( int j = 0; j < 6; ++j )
			{
				if( ptm->tm_hour >= i->open_start[j] && ptm->tm_hour <= i->open_end[j] )
				{
					b = true;
					break;
				}
			}
			if( !b )
				break;

			if( p->HasInstanceQueue( i->mapid ) )
			{
				MyLog::log->debug( "[%s] try to queue a instance which is already have, mapid:%d", p->GetName(), i->mapid );
				return false;
			}

			switch(i->cost_type)
			{
			case 1://元宝
				{
					if( p->GetCardPoints() < i->cost_value1 )
					{
						//p->GetSession()->SystemMessage("进入这个副本需要%d个元宝", i->cost_value1);
						p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueRMBNeed, quick_itoa(i->cost_value1) ) );
						return false;
					}
				}
				break;
			case 2://道具
				{
					if( p->GetItemInterface()->GetItemCount(i->cost_value1) < i->cost_value2 )
					{
						ItemPrototype* itemproto = ItemPrototypeStorage.LookupEntry(i->cost_value1);
						if(itemproto)
						{
							//p->GetSession()->SystemMessage("您需要%d个%s才能进入这个副本", i->cost_value2, Item::BuildStringWithProto(itemproto));
							p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueItemNeed, quick_itoa(i->cost_value2), Item::BuildStringWithProto(itemproto) ) );
						}
						else
						{
							MyLog::log->error("not found instance[%d] limit item [%d]", i->mapid, i->cost_value1);
							return false;
						}
						return false;
					}
				}
				break;
			case 3://金币
				{
					if( p->GetUInt32Value(PLAYER_FIELD_COINAGE) < i->cost_value1 )
					{
						//p->GetSession()->SystemMessage("进入这个副本需要%d个金币", i->cost_value1);
						p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueGoldNeed, quick_gold_s(i->cost_value1) ) );
						return false;
					}
				}
				break;
			default:
				break;
			}
			
			if( i->iq )
			{
				i->iq->Join( p );
			}else if (i->gsc_iq)
			{
				i->gsc_iq->Add(p);
			}
			else
			{
				switch( i->category )
				{
				case INSTANCE_CATEGORY_ARENA:
					{						
						i->iq = new InstanceQueue( *i, this );
						i->iq->Join( p );
					}
					break;
				case INSTANCE_CATEGORY_REFRESH_MONSTER:
				case INSTANCE_CATEGORY_DYNAMIC:
				case INSTANCE_CATEGORY_ESCAPE:
				case INSTANCE_CATEGORY_RAID:
				case INSTANCE_CATEGORY_FAIRGROUND:
					{
						GroupJoin( p, *i, isPrivate );
					}
					break;
				case INSTANCE_CATEGORY_UNIQUE_CASTLE:
					{
						SingleJoin( p, *i );
					}
					break;
				case INSTANCE_CATEGORY_BATTLE_GROUND:
				case INSTANCE_CATEGORY_TEAM_ARENA:
					{
						assert( 0 );
					}
					break;
				default:
					//assert( 0 );
					MyLog::log->error("not found instance type[%d]", i->category);
					break;
				}
			}
			return true;
		}
	}
	MyLog::log->debug( "cannot join instance, mapid:%d level:%d", mapid, p->getLevel() );
	return false;
}

uint32 InstanceQueueMgr::GetInstanceEnterLimit( uint32 mapid )
{
	for( std::vector<instance_queue*>::iterator it = m_iqv.begin(); it != m_iqv.end(); ++it )
	{
		instance_queue* i = *it;
		if( i->mapid == mapid )
		{
			return i->enter_limit;
		}
	}
	return 0;
}

SunyouCastle* InstanceQueueMgr::GetCastleByMapID( uint32 mapid )
{
	std::map<uint32, SunyouInstance*>::iterator it = m_map_unique_pvp_zone.find( mapid );
	if( it != m_map_unique_pvp_zone.end() )
		return static_cast<SunyouCastle*>( it->second );
	else
		return NULL;
}

void InstanceQueueMgr::BuildCastleState( MSG_S2C::stCastleStateAck& ack )
{
	ack.server_time = (uint32)UNIXTIME;
	for( std::map<uint32, SunyouInstance*>::iterator it = m_map_unique_pvp_zone.begin(); it != m_map_unique_pvp_zone.end(); ++it )
	{
		MSG_S2C::stCastleStateAck::castle_state cs;
		SunyouInstance* p = it->second;
		if( p->GetCategory() == INSTANCE_CATEGORY_UNIQUE_CASTLE )
		{
			SunyouCastle* castle = static_cast<SunyouCastle*>( p );
			cs.state = castle->m_state;
			cs.castle_name = castle->m_mapmgr->GetMapInfo()->name;
			cs.mapid = it->first;
			cs.fight_day_of_week = castle->m_start_fight_day_of_week;
			cs.fight_duaration = castle->m_castle_config->fight_duration;
			cs.fight_time = castle->m_castle_config->start_fight_hour;
			cs.owner_guild = castle->m_defence_guild;
			if( cs.owner_guild )
			{
				Guild* g = objmgr.GetGuild( cs.owner_guild );
				if( g )
					cs.owner_guild_name = g->GetGuildName();
			}
			cs.prepare_time = castle->m_castle_config->fight_prepare_time;
			ack.vstates.push_back( cs );
		}
	}
}

std::set<SunyouCastle*> InstanceQueueMgr::GetCastleByPlayer( Player* p )
{
	std::set<SunyouCastle*> tmp;
	for( std::map<uint32, SunyouInstance*>::iterator it = m_map_unique_pvp_zone.begin(); it != m_map_unique_pvp_zone.end(); ++it )
	{
		MSG_S2C::stCastleStateAck::castle_state cs;
		SunyouInstance* pInstance = it->second;
		if( pInstance->GetCategory() == INSTANCE_CATEGORY_UNIQUE_CASTLE )
		{
			SunyouCastle* castle = static_cast<SunyouCastle*>( pInstance );
			Guild* g = objmgr.GetGuild( castle->m_defence_guild );
			if( g )
			{
				if( g->IsAlliance( p->GetGuildId() ) )
					tmp.insert( castle );
			}
		}
	}
	return tmp;
}

void InstanceQueueMgr::RemoveSunyouFairground( SunyouFairground* p )
{
	std::map<uint32, std::set<SunyouFairground*> >::iterator it = m_map_fairgrounds.find( p->GetConfig()->mapid );
	if( it != m_map_fairgrounds.end() )
		it->second.erase( p );
	else
		assert( 0 );
}
void InstanceQueueMgr::Add_GSCMember(ui32 gs_id, ui32 mapid, vector<MSG_GS2GSP::stQueueInstanceReq::_group>& vgroup)
{
	for( std::vector<instance_queue*>::iterator it = m_iqv.begin(); it != m_iqv.end(); ++it )
	{
		instance_queue* i = *it;
		if( i->mapid == mapid )
		{
			if (i->gsp_iq)
			{
				i->gsp_iq->AddJoin(vgroup, gs_id);
			}
		}
	}
}
void InstanceQueueMgr::Leave_GSCMember(ui32 gs_id, ui32 pid)
{
	for( std::vector<instance_queue*>::iterator it = m_iqv.begin(); it != m_iqv.end(); ++it )
	{
		instance_queue* i = *it;
		if (i->gsp_iq)
			i->gsp_iq->RemoveJoin(pid, gs_id);
	}
}
void InstanceQueueMgr::LeaveRequest( Player* p, uint32 mapid )
{
	for( std::vector<instance_queue*>::iterator it = m_iqv.begin(); it != m_iqv.end(); ++it )
	{
		instance_queue* i = *it;
		if( i->mapid == mapid )
		{
			if (i->iq)
			{
				i->iq->Leave( p );
			}
			if (i->gsc_iq && p->HasInstanceQueue(mapid))
			{
				i->gsc_iq->Leave(p);
			}
			return;
		}
	}
}

void InstanceQueueMgr::CheckAll()
{

}

void InstanceQueueMgr::LeavePlayer( Player* p )
{
	for( std::vector<instance_queue*>::iterator it = m_iqv.begin(); it != m_iqv.end(); ++it )
	{
		instance_queue* i = *it;
		if( i->iq )
			i->iq->Leave( p );
		if (i->gsc_iq && p->HasInstanceQueue(i->mapid))
			i->gsc_iq->Leave(p);
	}
}

void InstanceQueueMgr::RemovePlayer( Player* p )
{
	for( std::vector<instance_queue*>::iterator it = m_iqv.begin(); it != m_iqv.end(); ++it )
	{
		instance_queue* i = *it;
		if( i->iq )
			i->iq->Remove( p );	
// 		if (i->gsc_iq && p->HasInstanceQueue(i->mapid))
// 		{
// 			i->gsc_iq->Leave(p);
// 		}
	}
}

void InstanceQueueMgr::RemovePlayerByType( Player* p, instance_category_t c )
{
	for( std::vector<instance_queue*>::iterator it = m_iqv.begin(); it != m_iqv.end(); ++it )
	{
		instance_queue* i = *it;
		if( i->iq && i->category == c )
			i->iq->Leave( p );
		if (i->gsc_iq && i->category == c && p->HasInstanceQueue(i->mapid))
			i->gsc_iq->Leave(p);
		
	}
}

void InstanceQueueMgr::RemoveQueue( InstanceQueue* q )
{
	for( std::vector<instance_queue*>::iterator it = m_iqv.begin(); it != m_iqv.end(); ++it )
	{
		instance_queue* i = *it;
		if( i->iq == q )
		{
			i->iq = NULL;
			break;
		}
	}
}

void InstanceQueueMgr::SendInstanceList2Player( Player* p )
{
	if( !p->GetSession() ) return;

	if( !m_silsg )
	{
		m_silsg = new MSG_S2C::stSunyouInstanceList;
		for( std::vector<instance_queue*>::iterator it = m_iqv.begin(); it != m_iqv.end(); ++it )
		{
			const sunyou_instance_configuration* sic = *it;
			if( sic->category == INSTANCE_CATEGORY_UNIQUE_CASTLE
				|| sic->hide )
				continue;

			m_silsg->v.push_back( *sic );
		}
	}
	p->GetSession()->SendPacket( *m_silsg );
}

void InstanceQueueMgr::SingleJoin( Player* p, const sunyou_instance_configuration& conf )
{
	MapMgr* pMgr = NULL;
	bool unique = false;
	if( conf.category != INSTANCE_CATEGORY_UNIQUE_PVP_ZONE && conf.category != INSTANCE_CATEGORY_UNIQUE_CASTLE )
	{
		pMgr = sInstanceMgr.CreateBattlegroundInstance( conf.mapid );
		unique = false;
	}
	else
		unique = true;
	switch( conf.category )
	{
	case INSTANCE_CATEGORY_REFRESH_MONSTER:
		pMgr->m_sunyouinstance = new SunyouMonsterGarden;
		break;
	case INSTANCE_CATEGORY_DYNAMIC:
		pMgr->m_sunyouinstance = new SunyouDynamicInstance;
		break;
	case INSTANCE_CATEGORY_ESCAPE:
		pMgr->m_sunyouinstance = new SunyouEscape;
		break;
	case INSTANCE_CATEGORY_UNIQUE_PVP_ZONE:
		pMgr = m_map_unique_pvp_zone_mgr[conf.mapid];
		break;
	case INSTANCE_CATEGORY_UNIQUE_CASTLE:
		pMgr = m_map_unique_pvp_zone_mgr[conf.mapid];
		break;
	default:
		//assert( 0 && "unknown instance category" );
		MyLog::log->error("unknown instance category[%d]", conf.category);
		return;
	}

	if( !unique )
		pMgr->m_sunyouinstance->Init( conf, pMgr );

	p->m_sunyou_instance = pMgr->m_sunyouinstance;
	p->PrepareTeleportToSunyouInstance( conf.mapid, pMgr->GetInstanceID(), conf.random_vrl[rand() % conf.random_vrl.size()] );
}

void InstanceQueueMgr::GroupJoin( Player* p, const sunyou_instance_configuration& conf, bool isPrivate )
{
	Group* pGroup = p->GetGroup();
	uint32 onlinePlayerCount = 0;
	if( pGroup )
	{
		PlayerInfo* leader = pGroup->GetLeader();
		if( leader->guid != p->GetLowGUID() )
		{
			MyLog::log->error( "player:%s is not a leader, cannot enter instance", p->GetName() );
			return;
		}

		uint32 groups = pGroup->GetSubGroupCount();
		if( conf.category == INSTANCE_CATEGORY_RAID )
		{
			if( pGroup->MemberCount() < conf.minplayer )
			{
				p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueMinPlayer, quick_itoa(conf.minplayer) ) );//"此副本不可以少于%d人", conf.minplayer );
				return;
			}
			else if( pGroup->MemberCount() > conf.maxplayer )
			{
				p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueMaxPlayer, quick_itoa(conf.maxplayer) ) );//"此副本不可以超过%d人", conf.maxplayer );
				return;
			}
		}
		bool bChecked = true;
		for(uint32 i = 0; i < groups; i++)
		{
			SubGroup * sg = pGroup->GetSubGroup(i);
			if(!sg) continue;

			uint32 playercnt = sg->GetMemberCount();
			GroupMembersSet::iterator git = sg->GetGroupMembersBegin();
			
			for( ; git != sg->GetGroupMembersEnd(); ++git )
			{
				if(!bChecked)
					break;
				PlayerInfo* pinfo = *git;
				if( pinfo->m_loggedInPlayer )
				{
					++onlinePlayerCount;
					if( pinfo->m_loggedInPlayer->IsInstanceBusy() )
					{
						//p->GetSession()->SystemMessage("玩家[%s]正在副本中,或者副本排队中", pinfo->name);
						p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueBusy, pinfo->name ) );
						bChecked = false;
					}
					else if( pinfo->m_loggedInPlayer->getLevel() < conf.minlevel )
					{
						//p->GetSession()->SystemMessage("玩家[%s]级别不足%d", pinfo->name, conf.minlevel);
						p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueLowLevel, pinfo->name, quick_itoa(conf.minlevel) ) );
						bChecked = false;
					}
					else if( pinfo->m_loggedInPlayer->getLevel() > conf.maxlevel )
					{
						//p->GetSession()->SystemMessage("玩家[%s]级别高于%d", pinfo->name, conf.maxlevel);
						p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueHighLevel, pinfo->name, quick_itoa(conf.maxlevel) ) );
						bChecked = false;
					}
					else if( int nLimit = g_instancequeuemgr.GetInstanceEnterLimit( conf.mapid ) )
					{
						if( pinfo->instanceEnterLimit[conf.mapid] >= (uint32)nLimit )
						{
							//p->GetSession()->SystemMessage("玩家[%s]今天进入副本已经超过%d次", pinfo->name, nLimit);
							p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueEnterLimit, pinfo->name, quick_itoa(nLimit) ) );
							bChecked = false;
						}
					}
					if( bChecked) 
					{
						switch(conf.cost_type)
						{
						case 1://元宝
							{
								if(pinfo->m_loggedInPlayer->GetCardPoints() < conf.cost_value1)
								{
									//p->GetSession()->SystemMessage("玩家[%s]元宝数不足%d个", pinfo->name, conf.cost_value1);
									p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueOtherRMBNeed, pinfo->name, quick_itoa(conf.cost_value1) ) );
									bChecked = false;
								}
							}
							break;
						case 2://道具
							{
								if(pinfo->m_loggedInPlayer->GetItemInterface()->GetItemCount(conf.cost_value1) < conf.cost_value2)
								{
									ItemPrototype* itemproto = ItemPrototypeStorage.LookupEntry(conf.cost_value1);
									if(itemproto)
									{
										//p->GetSession()->SystemMessage("玩家[%s]的道具[%s]不足%d个", pinfo->name, Item::BuildStringWithProto(itemproto), conf.cost_value2);
										p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueOtherItemNeed, pinfo->name, Item::BuildStringWithProto(itemproto), quick_itoa(conf.cost_value2) ) );
									}
									else
										MyLog::log->error("not found instance[%d] limit item [%d]", conf.mapid, conf.cost_value1);
									bChecked = false;
								}
							}
							break;
						case 3://金币
							{
								if(pinfo->m_loggedInPlayer->GetUInt32Value(PLAYER_FIELD_COINAGE) < conf.cost_value1)
								{
									//p->GetSession()->SystemMessage("玩家[%s]的金币不足%d个", pinfo->name, conf.cost_value1);
									p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueOtherGoldNeed, pinfo->name, quick_gold_s(conf.cost_value1) ) );
									bChecked = false;
								}
							}
							break;
						default:
							break;
						}
					}
				}
					
			}
			if(!bChecked)
				return;

			for(uint32 i = 0; i < groups; i++)
			{
				SubGroup * sg = pGroup->GetSubGroup(i);
				if(!sg) continue;
				git = sg->GetGroupMembersBegin();
				for( ; git != sg->GetGroupMembersEnd(); ++git )
				{
					PlayerInfo* pinfo = *git;
					if( pinfo->m_loggedInPlayer )
						//if( pinfo->m_loggedInPlayer->getLevel() >= conf.minlevel && pinfo->m_loggedInPlayer->getLevel() <= conf.maxlevel )
						{
							switch(conf.cost_type)
							{
							case 1://元宝
									pinfo->m_loggedInPlayer->SpendPoints(conf.cost_value1, "instance");
								break;
							case 2://道具
									pinfo->m_loggedInPlayer->GetItemInterface()->RemoveItemAmt(conf.cost_value1, conf.cost_value2);
								break;
							case 3://金币
									pinfo->m_loggedInPlayer->ModCoin(-conf.cost_value1);
								break;
							default:
								break;
							}
						}
				}
			}
		}
	}
	else
	{
		++onlinePlayerCount;
		if( p->getLevel() < conf.minlevel )
		{
			//p->GetSession()->SystemMessage("玩家[%s]级别不足%d", p->GetName(), conf.minlevel);
			p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueLowLevel, p->GetName(), quick_itoa(conf.minlevel) ) );
			return;
		}
		else if( p->getLevel() > conf.maxlevel )
		{
			//p->GetSession()->SystemMessage("玩家[%s]级别高于%d", p->GetName(), conf.maxlevel);
			p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueHighLevel, p->GetName(), quick_itoa(conf.maxlevel) ) );
			return;
		}
		else if( int nLimit = g_instancequeuemgr.GetInstanceEnterLimit( conf.mapid ) )
		{
			if( p->m_playerInfo->instanceEnterLimit[conf.mapid] >= (uint32)nLimit )
			{
				//p->GetSession()->SystemMessage("玩家[%s]今天进入副本已经超过%d次", p->GetName(), nLimit);
				p->GetSession()->SystemMessage( build_language_string( BuildString_InstanceQueueEnterLimit, p->GetName(), quick_itoa(nLimit) ) );
				return;
			}
		}
		
		{
			switch(conf.cost_type)
			{
			case 1://元宝
				p->SpendPoints(conf.cost_value1, "instance");
				break;
			case 2://道具
				p->GetItemInterface()->RemoveItemAmt(conf.cost_value1, conf.cost_value2);
				break;
			case 3://金币
				p->ModCoin(-conf.cost_value1);
				break;
			default:
				break;
			}
		}
		
	}
	MapMgr* pMgr = NULL;
	bool need_init = true;

	switch( conf.category )
	{
	case INSTANCE_CATEGORY_REFRESH_MONSTER:
		{
			pMgr = sInstanceMgr.CreateBattlegroundInstance( conf.mapid );
			pMgr->m_sunyouinstance = new SunyouMonsterGarden;
		}
		break;
	case INSTANCE_CATEGORY_DYNAMIC:
		{
			pMgr = sInstanceMgr.CreateBattlegroundInstance( conf.mapid );
			pMgr->m_sunyouinstance = new SunyouDynamicInstance;
		}
		break;
	case INSTANCE_CATEGORY_ESCAPE:
		{
			pMgr = sInstanceMgr.CreateBattlegroundInstance( conf.mapid );
			pMgr->m_sunyouinstance = new SunyouEscape;
		}
		break;
	case INSTANCE_CATEGORY_UNIQUE_PVP_ZONE:
		{
			pMgr = m_map_unique_pvp_zone_mgr[conf.mapid];
			need_init = false;
		}
		break;
	case INSTANCE_CATEGORY_UNIQUE_CASTLE:
		{
			pMgr = m_map_unique_pvp_zone_mgr[conf.mapid];
			need_init = false;
		}
		break;
	case INSTANCE_CATEGORY_RAID:
		{
			pMgr = sInstanceMgr.CreateBattlegroundInstance( conf.mapid );
			pMgr->m_sunyouinstance = new SunyouRaid;
		}
		break;
	case INSTANCE_CATEGORY_FAIRGROUND:
		{
			if( isPrivate )
			{
				pMgr = sInstanceMgr.CreateBattlegroundInstance( conf.mapid );
				pMgr->m_sunyouinstance = new SunyouFairground;
				pMgr->m_sunyouinstance->SetPrivate( true );
			}
			else
			{
				std::map<uint32, std::set<SunyouFairground*> >::iterator it = m_map_fairgrounds.find( conf.mapid );
				if( it != m_map_fairgrounds.end() )
				{
					std::set<SunyouFairground*>& s( it->second );
					for( std::set<SunyouFairground*>::iterator it2 = s.begin(); it2 != s.end(); ++it2 )
					{
						SunyouFairground* f = *it2;
						if( !f->IsPrivate() && onlinePlayerCount + f->GetPlayerCount() <= conf.maxplayer )
						{
							pMgr = f->GetMapMgr();
							need_init = false;
							break;
						}
					}

					if( !pMgr )
					{
						pMgr = sInstanceMgr.CreateBattlegroundInstance( conf.mapid );
						SunyouFairground* f = new SunyouFairground;
						s.insert( f );
						pMgr->m_sunyouinstance = f;
					}
				}
				else
				{
					pMgr = sInstanceMgr.CreateBattlegroundInstance( conf.mapid );
					SunyouFairground* f = new SunyouFairground;
					std::set<SunyouFairground*> s;
					s.insert( f );
					m_map_fairgrounds.insert( std::map<uint32, std::set<SunyouFairground*> >::value_type( (uint32)conf.mapid, s ) );
					pMgr->m_sunyouinstance = f;
				}
			}
		}
		break;
	default:
		//assert( 0 && "unknown instance category" );
		MyLog::log->error("unknown instance category[%d]", conf.category);
		return;
	}

	if( need_init )
		pMgr->m_sunyouinstance->Init( conf, pMgr );

	if( pGroup )
	{
		for(uint32 i = 0; i < pGroup->GetSubGroupCount(); i++)
		{
			SubGroup * sg = pGroup->GetSubGroup(i);
			if(!sg) continue;
			GroupMembersSet::iterator git = sg->GetGroupMembersBegin();
			for( ; git != sg->GetGroupMembersEnd(); ++git )
			{
				PlayerInfo* pinfo = *git;
				if( pinfo->m_loggedInPlayer )
				{
					pinfo->m_loggedInPlayer->m_sunyou_instance = pMgr->m_sunyouinstance;
					pinfo->m_loggedInPlayer->PrepareTeleportToSunyouInstance( conf.mapid, pMgr->GetInstanceID(), conf.random_vrl[rand() % conf.random_vrl.size()] );
				}
			}
		}
	}
	else
	{
		p->m_sunyou_instance = pMgr->m_sunyouinstance;
		p->PrepareTeleportToSunyouInstance( conf.mapid, pMgr->GetInstanceID(), conf.random_vrl[rand() % conf.random_vrl.size()] );
	}
}
