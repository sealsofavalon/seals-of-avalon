#ifndef _INSTANCE_QUEUE_HEAD
#define _INSTANCE_QUEUE_HEAD

#include "../../SDBase/Public/PublicDef.h"

class SunyouCastle;
class Player;

class InstanceQueueMgr;
class SunyouFairground;
class GSC_InstanceQueue;
class GSP_InstanceQueueBase;

class InstanceQueue
{
public:
	InstanceQueue( const sunyou_instance_configuration& conf, InstanceQueueMgr* father );
	virtual ~InstanceQueue();

public:
	virtual bool Check();
	virtual void Join( Player* p );
	virtual void Leave( Player* p );
	virtual void Remove( Player* p );
	virtual void Start();

protected:
	std::set<Player*> m_players;
	const sunyou_instance_configuration& m_conf;
	InstanceQueueMgr* m_father;
};

class BattleGroundGroupQueue : public InstanceQueue
{
public:
	BattleGroundGroupQueue( const sunyou_instance_configuration& conf, InstanceQueueMgr* father );
	~BattleGroundGroupQueue();

public:
	virtual bool Check();
	virtual void Join( Player* p );
	virtual void Leave( Player* p );
	virtual void Remove( Player* p );
	virtual void Start();

protected:
	typedef std::list<std::set<Player*> > faction_player_list_t;
	faction_player_list_t m_faction_players[3];
	bool move_group2set( std::set<Player*>& dest, faction_player_list_t& source, uint32 count );
};

class TeamArenaQueue : public InstanceQueue
{
public:
	TeamArenaQueue( const sunyou_instance_configuration& conf, InstanceQueueMgr* father );
	~TeamArenaQueue();

public:
	virtual bool Check();
	virtual void Join( Player* p );
	virtual void Leave( Player* p );
	virtual void Remove( Player* p );
	virtual void Start();

protected:
	struct team_t{
		team_t() : member_count( 0 ) {
			for( int i = 0; i < 5; ++i )
				players[i] = NULL;
		}
		Player* players[5];
		uint32 member_count;
	};
	std::list<team_t> m_teams;
	std::set<Player*> q_players;
};

class InstanceQueueMgr
{
public:
	InstanceQueueMgr();
	~InstanceQueueMgr();

public:
	void Init();
	bool JoinRequest( Player* p, uint32 mapid, bool isPrivate, uint32 player_limit = 30 );
	void LeaveRequest( Player* p, uint32 mapid );
	void CheckAll();
	void LeavePlayer( Player* p );
	void RemovePlayer( Player* p );
	void RemovePlayerByType( Player* p, instance_category_t c );
	void RemoveQueue( InstanceQueue* q );
	void SendInstanceList2Player( Player* p );
	SunyouCastle* GetCastleByMapID( uint32 mapid );
	uint32 GetInstanceEnterLimit( uint32 mapid );
	void BuildCastleState( MSG_S2C::stCastleStateAck& ack );
	std::set<SunyouCastle*> GetCastleByPlayer( Player* p );
	void RemoveSunyouFairground( SunyouFairground* p );

	void Add_GSCMember(ui32 gs_id,ui32 mapid, vector<MSG_GS2GSP::stQueueInstanceReq::_group>& vgroup);
	void Leave_GSCMember(ui32 gs_id, ui32 pid);
private:
	struct instance_queue : public sunyou_instance_configuration
	{
		InstanceQueue* iq;
		GSC_InstanceQueue* gsc_iq;  // 
		GSP_InstanceQueueBase* gsp_iq;  // ���������
	};
	std::vector<instance_queue*> m_iqv;
	MSG_S2C::stSunyouInstanceList* m_silsg;
	std::map<uint32, SunyouInstance*> m_map_unique_pvp_zone;
	std::map<uint32, MapMgr*> m_map_unique_pvp_zone_mgr;
	std::map<uint32, std::set<SunyouFairground*> > m_map_fairgrounds;

	void SingleJoin( Player* p, const sunyou_instance_configuration& conf );
	void GroupJoin( Player* p, const sunyou_instance_configuration& conf, bool isPrivate );
};

extern InstanceQueueMgr& g_instancequeuemgr;

#endif
