#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Item.h"
#include "MultiLanguageStringMgr.h"

#define ITEM_FIELD_VARIABLE_PROPERTIES(x) ITEM_FIELD_VARIABLE_PROPERTIES_##x

Item::Item()//this is called when constructing as container
{
	m_itemProto = NULL;
	m_owner = NULL;
	loot = NULL;
	locked = false;
	wrapped_item_id = 0;

	m_position = INVALID_LOCATION;
}

Item::Item( uint32 high, uint32 low )
{
	m_objectTypeId = TYPEID_ITEM;
	m_valuesCount = ITEM_END;
	m_uint32Values = _fields;
	memset( m_uint32Values, 0, (ITEM_END) * sizeof( uint32 ) );
	m_updateMask.SetCount(ITEM_END);
	SetUInt32Value( OBJECT_FIELD_TYPE,TYPE_ITEM | TYPE_OBJECT );
	SetUInt32Value( OBJECT_FIELD_GUID, low );
	SetUInt32Value( OBJECT_FIELD_GUID + 1, high );

	SetFloatValue( OBJECT_FIELD_SCALE_X, 1 );//always 1

	m_itemProto = NULL;
	m_owner = NULL;
	loot = NULL;
	locked = false;
	m_isDirty = true;
	random_prop = 0;
	random_suffix = 0;
	wrapped_item_id = 0;
	m_randompropapply = false;

	m_position = INVALID_LOCATION;
}

Item::~Item()
{
	if( loot != NULL )
		delete loot;

	sEventMgr.RemoveEvents( this );

	EnchantmentMap::iterator itr;
	for( itr = Enchantments.begin(); itr != Enchantments.end(); ++itr )
	{
		if( itr->second.Enchantment->type == 0 && itr->second.Slot == 0 && itr->second.ApplyTime == 0 && itr->second.Duration == 0 )
		{
			delete itr->second.Enchantment;
			itr->second.Enchantment = NULL;
		}
	}

	if( IsInWorld() )
		RemoveFromWorld();

	m_owner = NULL;
}

void Item::Create( uint32 itemid, Player* owner )
{
	SetUInt32Value( OBJECT_FIELD_ENTRY, itemid );
 
	if( owner != NULL )
	{
		SetUInt64Value( ITEM_FIELD_OWNER, owner->GetGUID() );
		SetUInt64Value( ITEM_FIELD_CONTAINED, owner->GetGUID() );
	}

	SetUInt32Value( ITEM_FIELD_STACK_COUNT, 1 );

	m_itemProto = ItemPrototypeStorage.LookupEntry( itemid );

	ASSERT( m_itemProto );
	 
	SetUInt32Value( ITEM_FIELD_SPELL_CHARGES, m_itemProto->Spells[0].Charges );
	SetUInt32Value( ITEM_FIELD_SPELL_CHARGES_01, m_itemProto->Spells[1].Charges );
	SetUInt32Value( ITEM_FIELD_SPELL_CHARGES_02, m_itemProto->Spells[2].Charges );
	SetUInt32Value( ITEM_FIELD_SPELL_CHARGES_03, m_itemProto->Spells[3].Charges );
	SetUInt32Value( ITEM_FIELD_SPELL_CHARGES_04, m_itemProto->Spells[4].Charges );
	SetUInt32Value( ITEM_FIELD_MAXDURABILITY, m_itemProto->MaxDurability );
	SetUInt32Value( ITEM_FIELD_DURABILITY, m_itemProto->MaxDurability );

	SetUInt32Value( ITEM_FIELD_LIFE_STYLE, m_itemProto->LifeStyle );
	if(m_itemProto->LifeStyle == ITEM_LIFE_STYLE_TIME)
		SetUInt64Value( ITEM_FIELD_LIFE_VALUE1, m_itemProto->LifeValue + time(NULL));

	/*
	pet_egg* pe = dbcPetEggList.LookupEntry( itemid );
	if( pe )
	{
		SetUInt32Value( ITEM_FIELD_GENERATE_TIME, (uint32)UNIXTIME );
		SetUInt32Value( ITEM_FIELD_VALID_AFTER_GENERATE_TIME, (uint32)pe->brood_time );
	}
	*/

	m_owner = owner;
	if( m_itemProto->LockId > 1 )
		locked = true;
	else
		locked = false;
	SetUInt32Value( ITEM_FIELD_USE_CNT, m_itemProto->usecnt );
}

void Item::LoadFromDB(Field* fields, Player* plr, bool light )
{
	uint32 itemid = fields[2].GetUInt32();
	uint32 random_prop, random_suffix;
	m_itemProto = ItemPrototypeStorage.LookupEntry( itemid );

	if( !m_itemProto )
	{
		assert( m_itemProto );
		return;
	}

	
	if(m_itemProto->LockId > 1)
		locked = true;
	else
		locked = false;
	
	SetUInt32Value( OBJECT_FIELD_ENTRY, itemid );
	m_owner = plr;

	wrapped_item_id=fields[3].GetUInt32();
	m_uint32Values[ITEM_FIELD_GIFTCREATOR] = fields[4].GetUInt32();
	m_uint32Values[ITEM_FIELD_CREATOR] = fields[5].GetUInt32();

	SetUInt32Value( ITEM_FIELD_STACK_COUNT,  fields[6].GetUInt32());

	// Again another for that did not indent to make it do anything for more than 
	// one iteration x == 0 was the only one executed
	uint32 x = 0;
	for( x = 0; x < 5; x++ )
	{
		if( m_itemProto->Spells[x].Id )
		{
			SetUInt32Value( ITEM_FIELD_SPELL_CHARGES + x , fields[7].GetUInt32() );
			break;
		}
	}

	SetUInt32Value( ITEM_FIELD_FLAGS, fields[8].GetUInt32() );
	random_prop = fields[9].GetUInt32();
	random_suffix = fields[10].GetUInt32();

	if( random_prop )
		SetRandomProperty( random_prop );
	else if( random_suffix )
		SetRandomSuffix( random_suffix );

	SetUInt32Value( ITEM_FIELD_ITEM_TEXT_ID, fields[11].GetUInt32() );
	SetUInt32Value( ITEM_FIELD_DURABILITY, fields[12].GetUInt32() );

	uint32 maxdura = fields[16].GetUInt32();
	SetUInt32Value( ITEM_FIELD_MAXDURABILITY, maxdura > 0 ? maxdura : m_itemProto->MaxDurability );
		
	SetUInt32Value( ITEM_FIELD_JINGLIAN_LEVEL, fields[17].GetUInt32() );
	SetUInt32Value( ITEM_FIELD_EFFECT, fields[18].GetUInt32() );
	//SetUInt32Value( ITEM_FIELD_MAXDURABILITY, m_itemProto->MaxDurability );

	if( light )
		return;

	for ( x = 0; x < 8; x++) {
		SetByte( ITEM_FIELD_VARIABLE_PROPERTIES_01+x/4, x&3, fields[28+x].GetUInt8() );
	}

	string enchant_field = fields[15].GetString();
	vector< string > enchants = StrSplit( enchant_field, ";" );
	uint32 enchant_id;
	EnchantEntry* entry;
	uint32 time_left;
	uint32 enchslot;

	for( vector<string>::iterator itr = enchants.begin(); itr != enchants.end(); ++itr )
	{
		if( sscanf( (*itr).c_str(), "%u,%u,%u", (unsigned int*)&enchant_id, (unsigned int*)&time_left, (unsigned int*)&enchslot) == 3 )
		{
			entry = dbcEnchant.LookupEntry( enchant_id );
			if( entry )
			{
				AddEnchantment( entry, time_left, ( time_left == 0 ), false, false, enchslot );
				//(enchslot != 2) ? false : true, false);
			}
			else
			{
				/*
				EnchantEntry *pEnchant = new EnchantEntry;
				memset(pEnchant,0,sizeof(EnchantEntry));

				pEnchant->Id = enchant_id;
				if(enchslot != 2)
					AddEnchantment(pEnchant,0,true, false);
				else
					AddEnchantment(pEnchant,0,false,false);
				*/
			}
		}
	}	

	// ApplyRandomProperties( false );

	SetUInt32Value( ITEM_FIELD_LIFE_STYLE, fields[19].GetUInt32() );
	SetUInt32Value( ITEM_FIELD_LIFE_VALUE1, fields[20].GetUInt32() );
	SetUInt32Value( ITEM_FIELD_RECORD_MAPID, fields[21].GetUInt32() );
	SetUInt32Value( ITEM_FIELD_RECORD_ZONEID, fields[22].GetUInt32());
	SetFloatValue( ITEM_FIELD_RECORD_POSX, fields[23].GetFloat());
	SetFloatValue( ITEM_FIELD_RECORD_POSY, fields[24].GetFloat());
	SetFloatValue( ITEM_FIELD_RECORD_POSZ, fields[25].GetFloat());
	SetUInt32Value( ITEM_FIELD_USE_CNT, fields[26].GetUInt32() );
	SetUInt32Value( ITEM_FIELD_GENERATE_TIME, fields[27].GetUInt32() );
	/*
	pet_egg* pe = dbcPetEggList.LookupEntry( itemid );
	if( pe )
	{
		SetUInt32Value( ITEM_FIELD_VALID_AFTER_GENERATE_TIME, (uint32)pe->brood_time );
	}
	*/
	SetUInt32Value( ITEM_FIELD_HOLE_CNT, fields[36].GetUInt32());
	SetUInt32Value( ITEM_FIELD_HOLE_1,   fields[37].GetUInt32());
	SetUInt32Value( ITEM_FIELD_HOLE_2,   fields[38].GetUInt32());
	SetUInt32Value( ITEM_FIELD_HOLE_3,   fields[39].GetUInt32());
}

void Item::Update( uint64 time_now )
{
	if(GetUInt32Value(ITEM_FIELD_LIFE_STYLE) == ITEM_LIFE_STYLE_TIME)
	{
		if( GetUInt64Value(ITEM_FIELD_LIFE_VALUE1) <= time_now )
		{
			m_owner->GetSession()->SystemMessage( build_language_string( BuildString_ItemExpireNotify, BuildString() ) );//"道具[%s]已过期", BuildString());
			//delete this
			m_owner->GetItemInterface()->SafeRemoveAndRetreiveItemByGuid(GetGUID(), true);
		}
	} 
}

void Item::ApplyRandomProperties( bool apply )
{
	// apply random properties
	if( m_uint32Values[ITEM_FIELD_RANDOM_PROPERTIES_ID] != 0 )
	{
		if( int32( m_uint32Values[ITEM_FIELD_RANDOM_PROPERTIES_ID] ) > 0 )		// Random Property
		{
			RandomProps* rp= dbcRandomProps.LookupEntry( m_uint32Values[ITEM_FIELD_RANDOM_PROPERTIES_ID] );
			if(!rp)
			{
				MyLog::log->error("not found RandomProps[%d]", m_uint32Values[ITEM_FIELD_RANDOM_PROPERTIES_ID]);
				return;
			}
			int32 Slot;
			for( int k = 0; k < 3; k++ )
			{
				if( rp->spells[k] != 0 )
				{	
					EnchantEntry* ee = dbcEnchant.LookupEntry( rp->spells[k] );
					if(!ee)
					{
						MyLog::log->error("not found enchant[%d]", rp->spells[k]);
						return;
					}
					Slot = HasEnchantment( ee->Id );
					if( Slot < 0 ) 
					{
						Slot = FindFreeEnchantSlot( ee, 1 );
						AddEnchantment( ee, 0, false, apply, true, Slot );
					}
					else
						if( apply )
							ApplyEnchantmentBonus( Slot, true );
				}
			}
		}
		else
		{
			ItemRandomSuffixEntry* rs = dbcItemRandomSuffix.LookupEntry( abs( int( m_uint32Values[ITEM_FIELD_RANDOM_PROPERTIES_ID] ) ) );
			if(!rs)
			{
				MyLog::log->error("not found ItemRandomSuffixEntry[%d]", m_uint32Values[ITEM_FIELD_RANDOM_PROPERTIES_ID]);
				return;
			}
			int32 Slot;
			for( uint32 k = 0; k < 3; ++k )
			{
				if( rs->enchantments[k] != 0 )
				{
					EnchantEntry* ee = dbcEnchant.LookupEntry( rs->enchantments[k] );
					if(!ee)
					{
						MyLog::log->error("not found enchant[%d]", rs->enchantments[k]);
						return;
					}
					Slot = HasEnchantment( ee->Id );
					if( Slot < 0 ) 
					{
						Slot = FindFreeEnchantSlot( ee, 2 );
						AddEnchantment( ee, 0, false, apply, true, Slot, rs->prefixes[k] );
					}
					else
						if( apply )
							ApplyEnchantmentBonus( Slot, true );
				}
			}
		}
	}
}

void Item::SaveToDB( ui8 containerslot, ui8 slot, bool firstsave, QueryBuffer* buf )
{
	if( !m_isDirty && !firstsave )
		return;

	std::stringstream ss;

	ss << "REPLACE INTO playeritems VALUES(";

	ss << m_uint32Values[ITEM_FIELD_OWNER] << ",";
    ss << m_uint32Values[OBJECT_FIELD_GUID] << ",";
	ss << m_uint32Values[OBJECT_FIELD_ENTRY] << ",";
	ss << wrapped_item_id << ",";
	ss << m_uint32Values[ITEM_FIELD_GIFTCREATOR] << ",";
	ss << m_uint32Values[ITEM_FIELD_CREATOR] << ",";

	ss << GetUInt32Value(ITEM_FIELD_STACK_COUNT) << ",";
	ss << GetChargesLeft() << ",";
	ss << GetUInt32Value(ITEM_FIELD_FLAGS) << ",";
	ss << random_prop << ", " << random_suffix << ", ";
	ss << GetUInt32Value(ITEM_FIELD_ITEM_TEXT_ID) << ",";
	ss << GetUInt32Value(ITEM_FIELD_DURABILITY) << ",";
	ss << static_cast<int>(containerslot) << ",";
	ss << static_cast<int>(slot) << ",'";

	// Pack together enchantment fields
	if( Enchantments.size() > 0 )
	{
		EnchantmentMap::iterator itr = Enchantments.begin();
		for(; itr != Enchantments.end(); ++itr)
		{
			if( itr->second.RemoveAtLogout )
				continue;

			uint32 elapsed_duration = uint32( UNIXTIME - itr->second.ApplyTime );
			int32 remaining_duration = itr->second.Duration - elapsed_duration;
			if( remaining_duration < 0 )
				remaining_duration = 0;
		
			/*
			if( !itr->second.RemoveAtLogout && (remaining_duration > 5 && itr->second.Slot != 2) || itr->second.Slot == 2)  // no point saving stuff with < 5 seconds... unless is perm enchant
			{
				ss << itr->second.Enchantment->Id << ",";
				ss << remaining_duration << ",";
				ss << itr->second.Slot << ";";
			}
			*/
		  
			if( itr->second.Enchantment && ( ( remaining_duration && remaining_duration > 5 ) || ( itr->second.Duration == 0 ) ) )
			{
				ss << itr->second.Enchantment->Id << ",";
				ss << remaining_duration << ",";
				ss << itr->second.Slot << ";";
			}
		}
	}
	ss << "',";
	ss << GetUInt32Value( ITEM_FIELD_MAXDURABILITY ) << ",";
	ss << GetUInt32Value( ITEM_FIELD_JINGLIAN_LEVEL ) << "," << GetUInt32Value(ITEM_FIELD_EFFECT) << ",";
	ss << GetUInt32Value( ITEM_FIELD_LIFE_STYLE) << "," << GetUInt32Value( ITEM_FIELD_LIFE_VALUE1 )<< ",";
	ss << GetUInt32Value( ITEM_FIELD_RECORD_MAPID) << "," << GetUInt32Value(ITEM_FIELD_RECORD_ZONEID)<< ",";
	ss << GetFloatValue( ITEM_FIELD_RECORD_POSX) << "," << GetFloatValue(ITEM_FIELD_RECORD_POSY) << ",";
	ss << GetFloatValue( ITEM_FIELD_RECORD_POSZ) << ",";
	ss << GetUInt32Value( ITEM_FIELD_USE_CNT ) << "," << GetUInt32Value( ITEM_FIELD_GENERATE_TIME );
;
	for ( uint32 x = 0; x < 8; x++ )
	{
		ss << "," << (uint32)GetByte(ITEM_FIELD_VARIABLE_PROPERTIES_01+x/4, x&3 );
	}
	ss << ",";
	ss << GetUInt32Value(ITEM_FIELD_HOLE_CNT) << "," << GetUInt32Value(ITEM_FIELD_HOLE_1) << ","
		<< GetUInt32Value(ITEM_FIELD_HOLE_2) << "," << GetUInt32Value(ITEM_FIELD_HOLE_3);
	ss << ")";
	
	if( firstsave )
		sWorld.ExecuteSqlToDBServer( ss.str().c_str() );
	else
	{
		if( buf == NULL )
			sWorld.ExecuteSqlToDBServer( ss.str().c_str() );
		else
			buf->AddQueryStr( ss.str() );
	}

	m_isDirty = false;
}

void Item::DeleteFromDB()
{
	if( m_itemProto->ContainerSlots>0 && GetTypeId() == TYPEID_CONTAINER )
	{
		/* deleting a container */
		for( uint32 i = 0; i < m_itemProto->ContainerSlots; ++i )
		{
			if( static_cast< Container* >( this )->GetItem( i ) != NULL )
			{
				/* abort the delete */
				return;
			}
		}
	}

	sWorld.ExecuteSqlToDBServer( "DELETE FROM playeritems WHERE guid = %u", m_uint32Values[OBJECT_FIELD_GUID] );
}

uint32 GetSkillByProto( uint32 Class, uint32 SubClass )
{
	if( Class == 4 && SubClass < 7 )
	{
		return arm_skills[SubClass];
	} 
    else if( Class == 2 )
	{
		if( SubClass < 20 )//no skill for fishing
		{
			return weap_skills[SubClass];
		}
	}
	return 0;
}

//This map is used for profess.
//Prof packe strcut: {SMSG_SET_PROFICIENCY,(uint8)item_class,(uint32)1<<item_subclass}
//ie: for fishing (it's class=2--weapon, subclass ==20 -- fishing rod) permissive packet
// will have structure 0x2,524288
//this table is needed to get class/subclass by skill, valid classes are 2 and 4

const ItemProf* GetProficiencyBySkill( uint32 skill )
{
	switch( skill )
	{
		case SKILL_CLOTH:
			return &prof[0];
		case SKILL_LEATHER:
			return &prof[1];
		case SKILL_MAIL:
			return &prof[2];
		case SKILL_PLATE_MAIL:
			return &prof[3];
		case SKILL_SHIELD:
			return &prof[4];
		case SKILL_AXES:
			return &prof[5];
		case SKILL_2H_AXES:
			return &prof[6];
		case SKILL_BOWS:
			return &prof[7];
		case SKILL_GUNS:
			return &prof[8];
		case SKILL_MACES:
			return &prof[9];
		case SKILL_2H_MACES:
			return &prof[10];
		case SKILL_POLEARMS:
			return &prof[11];
		case SKILL_SWORDS:
			return &prof[12];
		case SKILL_2H_SWORDS:
			return &prof[13];
		case SKILL_STAVES:
			return &prof[14];
		case SKILL_FIST_WEAPONS:
			return &prof[15];
		case SKILL_DAGGERS:
			return &prof[16];
		case SKILL_THROWN:
			return &prof[17];
		case SKILL_SPEARS:
			return &prof[18];
		case SKILL_CROSSBOWS:
			return &prof[19];
		case SKILL_WANDS:
			return &prof[20];
		case SKILL_FISHING:
			return &prof[21];
		default:
			return NULL;
	}
}

uint32 GetSellPriceForItem( ItemPrototype *proto, uint32 count, bool IsBuy )
{
	if( IsBuy )
		return proto->BuyPrice * count;
	else
		return proto->SellPrice * count;

	// modified by gui
	//int32 cost;
	//cost = proto->SellPrice * ( ( count < 1 ) ? 1 : count );
	/*
	float Price = IsBuy ? proto->BuyPrice : proto->SellPrice;
	float OPrice = 0.0f;
	if (proto->Class == ITEM_CLASS_CONSUMABLE)
	{
		if (proto->SubClass == ITEM_SUBCLASS_CONSUMABLE_POTION)
		{
			//药品
			Price = float(proto->ItemLevel) * 22.5f * 60.f * 0.5f * 0.01f;
			OPrice = IsBuy ? ( Price  + 0.49f ): ( Price * 0.5f + 0.49f );
			return OPrice < 0.1 ? (count) : (OPrice * count);
		}
	}
	else if (proto->Class == ITEM_CLASS_RECIPE)
	{
		ui32 SpellId = proto->Spells[0].Id;
		if (SpellId == 483)
		{
			//技能书
			Price = 40.f * proto->ItemLevel  + 0.5f;
			OPrice = IsBuy ? ( Price  + 0.49f ): ( Price * 0.5f + 0.49f );
			return OPrice < 0.1 ? (count) : (OPrice * count);
		}
	}
	else if (proto->Class == ITEM_CLASS_ARMOR || proto->Class == ITEM_CLASS_WEAPON)
	{
		//装备
		Price = proto->Damage[0].Max + proto->Armor;
		for(int i = 0; i < 10; i++)
		{
			if( proto->Stats[i].Type < 20 )
				Price += proto->Stats[i].Value * priceofstat[proto->Stats[i].Type] * 8;
		}
		OPrice = IsBuy ? ( Price  + 0.49f ): ( Price * 0.5f + 0.49f );
		return OPrice < 0.1 ? (count) : (OPrice * count);
	}
	return Price > 0.f ? (Price * count) : count;

	*/
}

uint32 GetBuyPriceForItem( ItemPrototype* proto, uint32 count, Player* plr, Creature* vendor )
{
	int32 cost = 0;
	// modified by gui
	//cost = proto->BuyPrice;
	return GetSellPriceForItem(proto, count, true);
}

uint32 GetSellPriceForItem( uint32 itemid, uint32 count )
{
	if( ItemPrototype* proto = ItemPrototypeStorage.LookupEntry( itemid ) )
		return GetSellPriceForItem(proto, count);
	else
		return 1;
}

uint32 GetBuyPriceForItem( uint32 itemid, uint32 count, Player* plr, Creature* vendor )
{
	if( ItemPrototype* proto = ItemPrototypeStorage.LookupEntry( itemid ) )
		return GetBuyPriceForItem( proto, count, plr, vendor );
	else
		return 1;
}

void Item::RemoveFromWorld()
{
	// if we have an owner->send destroy
	/*if( m_owner != NULL )
	{
		DestroyForPlayer( m_owner );
	}*/

	if( !IsInWorld() )
		return;

	mSemaphoreTeleport = true;
	m_mapMgr->RemoveObject( this, false );
	m_mapMgr = NULL;
  
	// update our event holder
	event_Relocate();
}

void Item::SetOwner( Player* owner )
{ 
	if( owner != NULL )
		SetUInt64Value( ITEM_FIELD_OWNER, static_cast< Object* >( owner )->GetGUID() );
	else SetUInt64Value( ITEM_FIELD_OWNER, 0 );

	m_owner = owner; 
}


int32 Item::AddEnchantment( EnchantEntry* Enchantment, uint32 Duration, bool Perm /* = false */, bool apply /* = true */, bool RemoveAtLogout /* = false */, uint32 Slot_, uint32 RandomSuffix )
{
	int32 Slot = Slot_;
	m_isDirty = true;

	/*
	if(Perm)
	{
		if(Slot_)
		{
			Slot=Slot_;
		}
		else
        {
			Slot = FindFreeEnchantSlot(Enchantment);
        }
	}
	else
	{
		if(Enchantment->EnchantGroups > 1) // replaceable temp enchants
		{
			Slot = 1;
			RemoveEnchantment(1);
		}
		else
		{
			Slot = FindFreeEnchantSlot(Enchantment);
			Slot = Enchantment->type ? 3 : 0;
			 //that's 's code
			for(uint32 Index = ITEM_FIELD_ENCHANTMENT_09; Index < ITEM_FIELD_ENCHANTMENT_32; Index += 3)
			{
				if(m_uint32Values[Index] == 0) break;;	
				++Slot;
			}

			Slot = FindFreeEnchantSlot(Enchantment);
			// reach max of temp enchants
			if(Slot >= 11) return -1;
		}
	}
	*/

	// Create the enchantment struct.
	EnchantmentInstance Instance;
	Instance.ApplyTime = UNIXTIME;
	Instance.BonusApplied = false;
	Instance.Slot = Slot;
	Instance.Enchantment = Enchantment;
	Instance.Duration = Duration;
	Instance.RemoveAtLogout = RemoveAtLogout;
	Instance.RandomSuffix = RandomSuffix;

	// Set the enchantment in the item fields.
	uint32 EnchantBase = ITEM_FIELD_ENCHANTMENT + Slot * 3;
	SetUInt32Value( EnchantBase, Enchantment->Id );
	SetUInt32Value( EnchantBase + 1, (uint32)Instance.ApplyTime );
	SetUInt32Value( EnchantBase + 2, 0 ); // charges

	// Add it to our map.
	Enchantments[Slot] = Instance;

	if( m_owner == NULL )
		return Slot;

	// Add the removal event.
	if( Duration )
	{
		sEventMgr.AddEvent( this, &Item::RemoveEnchantment, uint32(Slot), EVENT_REMOVE_ENCHANTMENT1 + Slot, Duration * 1000, 1, 0 );
	}

	// No need to send the log packet, if the owner isn't in world (we're still loading)
	if( !m_owner->IsInWorld() )
		return Slot;

	if( apply )
	{
		MSG_S2C::stItem_Enchant_Log Msg;
		Msg.owner_guid = m_owner->GetGUID();
		Msg.entry =m_uint32Values[OBJECT_FIELD_ENTRY];
		Msg.Enchantmentid = Enchantment->Id;
		m_owner->GetSession()->SendPacket( Msg );

		if( m_owner->GetTradeTarget() )
		{
			m_owner->SendTradeUpdate();
		}
	
		/* Only apply the enchantment bonus if we're equipped */
		uint8 slot = m_owner->GetItemInterface()->GetInventorySlotByGuid( GetGUID() );
		if( slot >= EQUIPMENT_SLOT_START && slot < EQUIPMENT_SLOT_END )
            ApplyEnchantmentBonus( Slot, APPLY );
	}

	return Slot;
}

void Item::RemoveEnchantment( uint32 EnchantmentSlot )
{
	// Make sure we actually exist.
	EnchantmentMap::iterator itr = Enchantments.find( EnchantmentSlot );
	if( itr == Enchantments.end() )
		return;

	m_isDirty = true;
	uint32 Slot = itr->first;
	if( itr->second.BonusApplied )
		ApplyEnchantmentBonus( EnchantmentSlot, REMOVE );

	// Unset the item fields.
 	uint32 EnchantBase = Slot * 3 + ITEM_FIELD_ENCHANTMENT;
 	SetUInt32Value( EnchantBase + 0, 0 );
 	SetUInt32Value( EnchantBase + 1, 0 );
 	SetUInt32Value( EnchantBase + 2, 0 );

	// Remove the enchantment event for removal.
	event_RemoveEvents( EVENT_REMOVE_ENCHANTMENT1 + Slot );

	// Remove the enchantment instance.
	Enchantments.erase( itr );
}

void Item::ApplyEnchantmentBonus( uint32 Slot, bool Apply )
{
	if( m_owner == NULL )
		return;

	EnchantmentMap::iterator itr = Enchantments.find( Slot );
	if( itr == Enchantments.end() )
		return;

	EnchantEntry* Entry = itr->second.Enchantment;
	uint32 RandomSuffixAmount = itr->second.RandomSuffix;

	if( itr->second.BonusApplied == Apply )
		return;

	itr->second.BonusApplied = Apply;

	if( Apply )
	{
		// Send the enchantment time update packet.
		//SendEnchantTimeUpdate( itr->second.Slot, itr->second.Duration );
	}

	// Apply the visual on the player.
	// SUPALOSA_: Not here
// 	uint8 s = m_owner->GetItemInterface()->GetInventorySlotByGuid( GetGUID() );
// 	if(s != ITEM_NO_SLOT_AVAILABLE)
// 	{
// 		uint32 ItemSlot = s * 12;
// 		uint32 VisibleBase = PLAYER_VISIBLE_ITEM_1_0 + ItemSlot;
// 		m_owner->SetUInt32Value( VisibleBase + 1 + Slot, Apply ? Entry->Id : 0 );
// 	}
// 	
	// 0 = ItemGUID
	// 1 = PermEnchant
	// 2 = TempEnchant
	// 3 = Gem1
	// 4 = Gem2
	// 5 = Gem3

	// Another one of those for loop that where not indented properly god knows what will break
	// but i made it actually affect the code below it
	for( uint32 c = 0; c < 3; c++ )
	{
		if ( Apply )
		{
			uint32 type = Entry->type[c];
			SpellEntry *spellInfo = dbcSpell.LookupEntry(Entry->spell[c] );
			uint32 procFlag = PROC_NULL;

			if( spellInfo )
			{
				if( spellInfo->Attributes & ATTRIBUTES_PASSIVE )
				{
					Spell *spell = new Spell(m_owner, spellInfo ,true, NULL);
					SpellCastTargets targets;
					targets.m_unitTarget = this->GetGUID();
					spell->prepare(&targets);
				}
				/*
				else if( type <= APPLY_TARGET_SELF )
				{
					procFlag = (type>0)?(1<<(type-1)):0;

					ProcTriggerSpell ts;
					ts.origId = 0;
					ts.spellId = Entry->spell[c];
					ts.procChance = Entry->ProcChance;
					ts.caster = this->GetGUID();
					ts.item = this;
					ts.procFlags = procFlag;
					ts.deleted = false;

					m_owner->m_procSpells.push_front(ts);
				}
				*/
			}
		}
		else
		{
			uint32 type = Entry->type[c];
			SpellEntry *spellInfo = dbcSpell.LookupEntry(Entry->spell[c] );
			if( spellInfo )
			{
				if( spellInfo->Attributes & ATTRIBUTES_PASSIVE )
				{
					m_owner->RemoveAura(Entry->spell[c]);
				}
				/*
				else if( type <= APPLY_TARGET_SELF )
				{
					std::list< struct ProcTriggerSpell >::iterator itr,itr2;
					for( itr = m_owner->m_procSpells.begin(); itr != m_owner->m_procSpells.end(); )
					{
						itr2 = itr;
						++itr;
						if (itr2->item == this)
						{
							m_owner->m_procSpells.erase( itr2 );
						}
					}
				}
				*/
			}
		}
		if( Entry->type[c] )
		{
			// Depending on the enchantment type, take the appropriate course of action.
			/*
			switch( Entry->type[c] )
			{
			case 1: //		生命
			case 2: //		魔法
			case 3: //		力量
			case 4: //		敏捷
			case 5: //		耐力
			case 6: //		智力
			case 7: //		精神
			case 8: //		体力
			case 10: //		闪避几率
			case 18://		暴击几率
				{
			*/
					int32 valmin = Entry->min[c];
					int val = valmin;
					/*
					int32 valmax = Entry->max[c];
					int val = valmin;
					if( valmax > valmin )
						val += RandomUInt(valmax-valmin);
					*/
					m_owner->ModifyBonuses( Entry->type[c], Apply ? val : -val );
			/*
				}
				break;
			default:
				{
					MyLog::log->error( "Unknown enchantment type: %u (%u)", Entry->type[c], Entry->Id );
				}break;
			}
			*/
		}
	}
}

void Item::OnEquip()
{
	for(int i = 0; i < 5; ++i)
	{
		if( GetProto()->Spells[i].Trigger == ON_EQUIP && GetProto()->Spells[i].Id != 0 )
		{
			uint32 spellid = GetProto()->Spells[i].Id;
			SpellCastTargets targets( m_owner->GetGUID() );
			SpellEntry* sp = sp = dbcSpell.LookupEntry( spellid );
			if( sp == NULL )
				continue;;
			Spell* spell;
			spell = new Spell( m_owner, sp, true, 0 );
			spell->p_caster = m_owner;
			spell->u_caster = (Unit*)m_owner;
			spell->prepare( &targets );	
			break;
		}
	}
	if( m_owner && m_owner->IsValid() && GetProto()->Class == ITEM_CLASS_WEAPON )
		m_owner->UpdateAttackSpeed();
}

void Item::OnUnEquip()
{
	for(int i = 0; i < 5; ++i)
	{
		if( GetProto()->Spells[i].Trigger == ON_UNEQUIP && GetProto()->Spells[i].Id != 0 )
		{
			uint32 spellid = GetProto()->Spells[i].Id;
			SpellCastTargets targets( m_owner->GetGUID() );
			SpellEntry* sp = sp = dbcSpell.LookupEntry( spellid );
			if( sp == NULL )
				continue;;
			Spell* spell;
			spell = new Spell( m_owner, sp, true, 0 );
			spell->p_caster = m_owner;
			spell->u_caster = (Unit*)m_owner;
			spell->prepare( &targets );	
			break;
		}
	}
}

void Item::ApplyEnchantmentBonuses()
{
	EnchantmentMap::iterator itr, itr2;
	for( itr = Enchantments.begin(); itr != Enchantments.end();  )
	{
		itr2 = itr++;
		ApplyEnchantmentBonus( itr2->first, APPLY );
	}
}

void Item::RemoveEnchantmentBonuses()
{
	EnchantmentMap::iterator itr, itr2;
	for( itr = Enchantments.begin(); itr != Enchantments.end(); )
	{
		itr2 = itr++;
		ApplyEnchantmentBonus( itr2->first, REMOVE );
	}
}

void Item::EventRemoveEnchantment( uint32 Slot )
{
	// Remove the enchantment.
	RemoveEnchantment( Slot );
}

int32 Item::FindFreeEnchantSlot( EnchantEntry* Enchantment, uint32 random_type )
{	
	if(!Enchantment) return -1;

	uint32 Slot = Enchantment->type ? 3 : 0;
	for(uint32 Index = ITEM_FIELD_ENCHANTMENT_09; Index < ITEM_FIELD_ENCHANTMENT_32; Index += 3)
	{
		if(m_uint32Values[Index] == 0) return Slot;	
		++Slot;
	}

	uint32 GemSlotsReserve = GetSocketsCount();
	if( GetProto()->SocketBonus )
		GemSlotsReserve++;

 	if( random_type == 1 )		// random prop
	{
		for( uint32 Slot = 8; Slot < 11; ++Slot )
			if( m_uint32Values[ITEM_FIELD_ENCHANTMENT + Slot * 3] == 0 )
				return Slot;
	}
	else if( random_type == 2 )	// random suffix
	{
		for( uint32 Slot = 6; Slot < 11; ++Slot )
			if( m_uint32Values[ITEM_FIELD_ENCHANTMENT + Slot * 3] == 0 )
				return Slot;
	}
	
	for( uint32 Slot = GemSlotsReserve + 2; Slot < 11; Slot++ )
	{
		if( m_uint32Values[ITEM_FIELD_ENCHANTMENT + Slot * 3] == 0 )
			return Slot;	
	}

	return -1;
}

int32 Item::HasEnchantment( uint32 Id )
{
	for( uint32 Slot = 0; Slot < 11; Slot++ )
	{
 		if( m_uint32Values[ITEM_FIELD_ENCHANTMENT + Slot * 3] == Id )
 			return Slot;
	}

	return -1;
}

void Item::ModifyEnchantmentTime( uint32 Slot, uint32 Duration )
{
	EnchantmentMap::iterator itr = Enchantments.find( Slot );
	if( itr == Enchantments.end() )
		return;

	// Reset the apply time.
	itr->second.ApplyTime = UNIXTIME;
	itr->second.Duration = Duration;

	// Change the event timer.
	event_ModifyTimeAndTimeLeft( EVENT_REMOVE_ENCHANTMENT1 + Slot, Duration * 1000 );

	// Send update packet
	SendEnchantTimeUpdate( itr->second.Slot, Duration );
}

void Item::SendEnchantTimeUpdate( uint32 Slot, uint32 Duration )
{
	/*
	{SERVER} Packet: (0x01EB) SMSG_ITEM_ENCHANT_TIME_UPDATE Size = 24
	|------------------------------------------------|----------------|
	|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
	|------------------------------------------------|----------------|
	|69 32 F0 35 00 00 00 40 01 00 00 00 08 07 00 00 |i2.5...@........|
	|51 46 35 00 00 00 00 00						 |QF5.....		|
	-------------------------------------------------------------------

	uint64 item_guid
	uint32 count?
	uint32 time_in_seconds
	uint64 player_guid
	*/

	MSG_S2C::stItem_Enchant_TimeUpdate Msg;;
	Msg.item_guid = GetGUID();
	Msg.Slot = Slot;
	Msg.duration = Duration;
	Msg.owner_guid = m_owner->GetGUID();
	//m_owner->delayedPackets.add( &Msg );
	m_owner->GetSession()->SendPacket( Msg );
}

void Item::RemoveAllEnchantments( bool OnlyTemporary )
{
	EnchantmentMap::iterator itr, it2;
	for( itr = Enchantments.begin(); itr != Enchantments.end(); )
	{
		it2 = itr++;

		if( OnlyTemporary && it2->second.Duration == 0 ) 
			continue;
			
		RemoveEnchantment( it2->first );
	}
}

void Item::RemoveRelatedEnchants( EnchantEntry* newEnchant )
{
	EnchantmentMap::iterator itr,itr2;
	for( itr = Enchantments.begin(); itr != Enchantments.end(); )
	{
		itr2 = itr++;
		
		if( itr2->second.Enchantment->Id == newEnchant->Id || ( itr2->second.Enchantment->EnchantGroups > 1 && newEnchant->EnchantGroups > 1 ) )
		{ 
			RemoveEnchantment( itr2->first );
		}
	}
}

void Item::RemoveProfessionEnchant()
{
	/*
	EnchantmentMap::iterator itr;
	for( itr = Enchantments.begin(); itr != Enchantments.end(); itr++ )
	{
		if( itr->second.Duration != 0 )// not perm
			continue;
		if( IsGemRelated( itr->second.Enchantment ) )
			continue;
		if( itr->second.Slot == 0 )
		{
			RemoveEnchantment( itr->first );
			return;
		}
	}
	*/
	RemoveEnchantment( 0 );
}

void Item::RemoveSocketBonusEnchant()
{
	EnchantmentMap::iterator itr;
	
	for( itr = Enchantments.begin(); itr != Enchantments.end(); itr++ )
	{
		if( itr->second.Enchantment->Id == GetProto()->SocketBonus )
		{
			RemoveEnchantment( itr->first );
			return;
		}	
	}
}

EnchantmentInstance* Item::GetEnchantment( uint32 slot )
{
	EnchantmentMap::iterator itr = Enchantments.find( slot );
	if( itr != Enchantments.end() )
		return &itr->second;
	else
		return NULL;
}

bool Item::IsGemRelated( EnchantEntry* Enchantment )
{
	if( GetProto()->SocketBonus == Enchantment->Id )
		return true;
	
	return( Enchantment->GemEntry != 0 );
}

uint32 Item::GetSocketsCount()
{
	uint32 c = 0;
	for( uint32 x = 0; x < 3; x++ )
		if( GetProto()->Sockets[x].SocketColor )
			c++;
	return c;
}

uint32 Item::GenerateRandomSuffixFactor( ItemPrototype* m_itemProto )
{
	double value;

	if( m_itemProto->Class == ITEM_CLASS_ARMOR && m_itemProto->Quality > ITEM_QUALITY_UNCOMMON_GREEN )
		value = SuffixMods[m_itemProto->InventoryType] * 1.24;
	else
		value = SuffixMods[m_itemProto->InventoryType];

	value = ( value * double( m_itemProto->ItemLevel ) ) + 0.5;
	return long2int32( value );
}

void Item::SetVariableProperties( uint32 id[], uint32 count )
{
	for (uint32 x=0; x<count; x++)
	{
		SetByte( ITEM_FIELD_VARIABLE_PROPERTIES_01+(x>>2), x&3, id[x]);
	}
}

void Item::OnPushToWorld()
{
 	if( NULL == GetOwner() )
 		sEventMgr.AddEvent(this, &Item::OnDurationExpired, EVENT_DELETE_TIMER, 180000, 1, EVENT_FLAG_DELETES_OBJECT);
}

void Item::OnDurationExpired()
{
	if( GetUInt32Value( ITEM_FIELD_OWNER ) )
		return;

 	MyLog::log->debug("mapitem[%u] expired", GetGUID());
 	RemoveFromWorld();
 	delete this;
}

void Item::SetCount( uint32 amt )
{
	SetUInt32Value( ITEM_FIELD_STACK_COUNT, amt );
}

void Item::SetRefineEffect( uint32 lv )
{
 	StorageContainerIterator<refine_item_effect>* it = dbcRefineItemEffect.MakeIterator();
 	while(!it->AtEnd())
 	{
 		refine_item_effect* rie = it->Get();
 		if( GetProto()->Class == ITEM_CLASS_WEAPON && GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_SHIELD )
 		{
 			if(!it->Inc())
 				break;
 			continue;
 		}
 		if( ( rie->part == GetProto()->SubClass && GetProto()->Class == ITEM_CLASS_ARMOR ) || ( rie->part == 100 && GetProto()->Class == ITEM_CLASS_WEAPON ) )
 		{
 			if( rie->rarity == GetProto()->Quality && lv >= rie->min_refine_level && lv <= rie->max_refine_level )
 			{
 				SetUInt32Value( ITEM_FIELD_EFFECT, rie->displayid );
				it->Destruct();
				return;
 			}
 		}
 
 		if(!it->Inc())
 			break;
 	}
 	it->Destruct();
	SetUInt32Value( ITEM_FIELD_EFFECT, 0 );
}

bool Item::CanJingLian()
{
	if( GetProto()->Class != ITEM_CLASS_WEAPON && GetProto()->Class != ITEM_CLASS_ARMOR )
		return false;
	else
		return true;
	/*
	else
	{
		if( GetProto()->Class == ITEM_CLASS_ARMOR )
		{
			switch( GetProto()->SubClass )
			{
			case ITEM_SUBCLASS_ARMOR_BOOTS:
			case ITEM_SUBCLASS_ARMOR_CHEST:
			case ITEM_SUBCLASS_ARMOR_GLOVES:
			case ITEM_SUBCLASS_ARMOR_HEAD:
			case ITEM_SUBCLASS_ARMOR_TROUSERS:
			case ITEM_SUBCLASS_ARMOR_SHOULDER:
			case ITEM_SUBCLASS_ARMOR_WAIST:
				break;
			default:
				return false;
			}
		}
		else if( GetProto()->Class == ITEM_CLASS_WEAPON )
		{
			if( GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_BAOZHU )
				return false;
			return true;
		}
	}
	*/
	return false;
}

void GetItemChatStr(ItemPrototype* proto, ItemExtraData* sItemData, std::string& str)
{
	char buf[256];
	sprintf(buf,"%u_", proto->ItemId);
	std::string EnChant = buf;
	if (sItemData)
	{
		uint32 tempValue = 0;
		tempValue = sItemData->jinglian_level;
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[0];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[1];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[2];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[3];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[4];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
		tempValue = sItemData->enchants[5];
		sprintf(buf, "%u_", tempValue);
		EnChant += buf;
	}
	static const char* colorstr[] = {"808080FF", "FFFFFFFF", "00FF00FF", "026DFEFF", "FF00FFFF", "EE9C00FF", "FF0000FF"};
	sprintf(buf, "<linkcolor:%s>", colorstr[proto->Quality]);
	std::string linkcolor = buf;
	str = linkcolor + "<a:#Item_" + EnChant + "></a>";
}

const char* Item::BuildString()
{
	ItemExtraData extra;
	for( int i = 0; i < 6; ++i )
		extra.enchants[i] = GetUInt32Value( ITEM_FIELD_ENCHANTMENT + i * 3 );
	extra.jinglian_level = GetUInt32Value( ITEM_FIELD_JINGLIAN_LEVEL );

	static std::string tmp;
	tmp.clear();
	GetItemChatStr( GetProto(), &extra, tmp );
	return tmp.c_str();
}

const char* Item::BuildStringWithProto( ItemPrototype* proto )
{
	ItemExtraData extra;

	static std::string tmp;
	tmp.clear();
	GetItemChatStr( proto, &extra, tmp );
	return tmp.c_str();
}

std::vector<uint32> Item::s_wash_enchantments[ITEM_QUALITY_LEGENDARY_ORANGE + 1][5];

void Item::LoadWashEnchantments()
{
	QueryResult* qr = WorldDatabase.Query( "select * from wash_item_dbc" );
	if( qr )
	{
		do
		{
			Field* f = qr->Fetch();
			uint32 entry = f[0].GetUInt32();
			uint32 warrior_count[ITEM_QUALITY_LEGENDARY_ORANGE + 1] = { 0 };
			uint32 bow_count[ITEM_QUALITY_LEGENDARY_ORANGE + 1] = { 0 };
			uint32 preist_count[ITEM_QUALITY_LEGENDARY_ORANGE + 1] = { 0 };
			uint32 mage_count[ITEM_QUALITY_LEGENDARY_ORANGE + 1] = { 0 };
			uint32 common_count[ITEM_QUALITY_LEGENDARY_ORANGE + 1] = { 0 };
			for( int i = 0; i < 4; ++i )
			{
				warrior_count[2 + i] = f[1 + i].GetUInt32();
				bow_count[2 + i] = f[5 + i].GetUInt32();
				preist_count[2 + i] = f[9 + i].GetUInt32();
				mage_count[2 + i] = f[13 + i].GetUInt32();
				common_count[2 + i] = f[17 + i].GetUInt32();
			}

			for( int i = ITEM_QUALITY_UNCOMMON_GREEN; i <= ITEM_QUALITY_LEGENDARY_ORANGE; ++i )
			{
				for( int j = 0; j < warrior_count[i]; ++j )
					s_wash_enchantments[i][CLASS_WARRIOR].push_back( entry );
				for( int j = 0; j < bow_count[i]; ++j )
					s_wash_enchantments[i][CLASS_BOW].push_back( entry );
				for( int j = 0; j < preist_count[i]; ++j )
					s_wash_enchantments[i][CLASS_PRIEST].push_back( entry );
				for( int j = 0; j < mage_count[i]; ++j )
					s_wash_enchantments[i][CLASS_MAGE].push_back( entry );
				for( int j = 0; j < common_count[i]; ++j )
					s_wash_enchantments[i][0].push_back( entry );
			}
		}
		while( qr->NextRow() );
		delete qr;
	}
}

void Item::WashEnchantment()
{
	if( m_itemProto->Class == ITEM_CLASS_ARMOR || m_itemProto->Class == ITEM_CLASS_WEAPON )
	{
		if( m_itemProto->Quality < ITEM_QUALITY_UNCOMMON_GREEN )
			return;

		RemoveEnchantment( 1 );
		uint32 key = 0;
		if( m_itemProto->AllowableClass > 0 && m_itemProto->AllowableClass <= 4 )
			key = m_itemProto->AllowableClass;

		uint32 entry = GetRandomWashEnchantments( key, m_itemProto->Quality );
		if( entry )
		{
			EnchantEntry* ee = dbcEnchant.LookupEntry( entry );
			if( ee )
			{
				bool apply = false;
				if( m_owner )
				{
					int slot = m_owner->GetItemInterface()->GetInventorySlotByGuid( GetGUID() );
					apply = (slot>=0 && slot <19);
				}

				AddEnchantment( ee, 0, true, apply, false, 1, 0 );
			}
		}
	}
}
