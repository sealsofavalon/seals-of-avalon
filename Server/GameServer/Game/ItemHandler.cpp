#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Item.h"
#include "../../SDBase/Protocol/S2C_Item.h"
#include "QuestMgr.h"

void WorldSession::HandleSplitOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;

	if(_player->GetTradeTarget())
		return;
	MSG_C2S::stItem_Split MsgRecv;packet>>MsgRecv;

	AddItemResult result;

	if(!GetPlayer())
		return;

	if(/*MsgRecv.count >= 127 || */(MsgRecv.srcbag <= 0 && MsgRecv.srcslot < INVENTORY_SLOT_ITEM_START) || (MsgRecv.dstbag <= 0 && MsgRecv.dstslot < INVENTORY_SLOT_ITEM_START))
	{
		/* exploit fix */
		return;
	}

	int32 c=MsgRecv.count;
	Item *i1 =_player->GetItemInterface()->GetInventoryItem(MsgRecv.srcbag,MsgRecv.srcslot);
	if(!i1)
		return;
	Item *i2=_player->GetItemInterface()->GetInventoryItem(MsgRecv.dstbag,MsgRecv.dstslot);

	if(_player->GetTradeTarget())
	{
		_player->GetItemInterface()->BuildInventoryChangeError(i1, i2, INV_ERR_COULDNT_SPLIT_ITEMS);
		return;
	}

	if( (i1 && i1->wrapped_item_id) || (i2 && i2->wrapped_item_id) || ( i1 && i1->GetProto()->MaxCount < 2 ) || ( i2 && i2->GetProto()->MaxCount < 2 ) || MsgRecv.count < 1 )
	{
		GetPlayer()->GetItemInterface()->BuildInventoryChangeError(i1, i2, INV_ERR_ITEM_CANT_STACK);
        return;
	}

	if(i2)//smth already in this slot
	{
		if(i1->GetEntry()==i2->GetEntry() )
		{
			//check if player has the required stacks to avoid exploiting.
			//safe exploit check
			if(c < (int32)i1->GetUInt32Value(ITEM_FIELD_STACK_COUNT))
			{
				//check if there is room on the other item.
				if(((c + i2->GetUInt32Value(ITEM_FIELD_STACK_COUNT)) <= i2->GetProto()->MaxCount))
				{
					i1->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, -c);
					i2->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, +c);
					i1->m_isDirty = true;
					i2->m_isDirty = true;
				}
				else
				{
					 GetPlayer()->GetItemInterface()->BuildInventoryChangeError(i1, i2, INV_ERR_ITEM_CANT_STACK);
				}
			}
			else
			{
				//error cant split item
				_player->GetItemInterface()->BuildInventoryChangeError(i1, i2, INV_ERR_COULDNT_SPLIT_ITEMS);
			}
		}
		else
		{
			 GetPlayer()->GetItemInterface()->BuildInventoryChangeError(i1, i2, INV_ERR_ITEM_CANT_STACK);
		}
	}
	else
	{
		if(MsgRecv.dstslot == 255)
		{
			// Find a free slot
			SlotResult res = _player->GetItemInterface()->FindFreeInventorySlot(i1->GetProto());
			if(!res.Result)
			{
				_player->GetItemInterface()->BuildInventoryChangeError(i1, i2, INV_ERR_BAG_FULL);
				//SendNotification("背包空间不足");
				return;
			}
			else
			{
				MsgRecv.dstslot = res.Slot;
				MsgRecv.dstbag = res.ContainerSlot;
			}
		}

		if(c < (int32)i1->GetUInt32Value(ITEM_FIELD_STACK_COUNT))
		{
			i1->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT,-c);

			i2=objmgr.CreateItem(i1->GetEntry(),_player);
			i2->SetUInt32Value(ITEM_FIELD_STACK_COUNT,c);
			i1->m_isDirty = true;
			i2->m_isDirty = true;

			result = _player->GetItemInterface()->SafeAddItem(i2,MsgRecv.dstbag,MsgRecv.dstslot);
			if(!result)
			{
				printf("HandleBuyItemInSlot: Error while adding item to dstslot");
				delete i2;
				_player->GetItemInterface()->BuildInventoryChangeError(i1, i2, INV_ERR_COULDNT_SPLIT_ITEMS);
				i2 = NULL;
			}
		}
		else
		{
			_player->GetItemInterface()->BuildInventoryChangeError(i1, i2, INV_ERR_COULDNT_SPLIT_ITEMS);
		}
	}
}

void WorldSession::HandleSwapItemOpcode(CPacketUsn& packet)
{
	LockQstItemNotify QstLocker( false );

	if(!_player->IsInWorld()) return;

	MSG_C2S::stItem_Swap MsgRecv;packet >> MsgRecv;
	Item *SrcItem = NULL;
	Item *DstItem = NULL;

	ui8 error = 0;
	//Item *SrcTemp = NULL;
	//Item *DstTemp = NULL;

	if(!GetPlayer())
		return;

	//MyLog::log->notice("ITEM: swap, DstInvSlot %i DstSlot %i MsgRecv.srcbag %i SrcSlot %i", MsgRecv.dstbag, MsgRecv.dstslot, MsgRecv.srcbag, MsgRecv.srcslot);

	if(MsgRecv.dstbag == MsgRecv.srcslot && MsgRecv.srcbag == 255) // player trying to add self container to self container slots
	{
		GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_ITEMS_CANT_BE_SWAPPED);
		return;
	}

	if( ( MsgRecv.dstbag <= 0 && MsgRecv.dstslot < 0 ) || MsgRecv.dstbag < -1 )
		return;
	
	if( ( MsgRecv.srcbag <= 0 && MsgRecv.srcslot < 0 ) || MsgRecv.srcbag < -1 )
		return;

	SrcItem=_player->GetItemInterface()->GetInventoryItem(MsgRecv.srcbag,MsgRecv.srcslot);
	if(!SrcItem)
		return;
	DstItem=_player->GetItemInterface()->GetInventoryItem(MsgRecv.dstbag,MsgRecv.dstslot);

	if(SrcItem)
	{   
		//换下武器,不指定目标位置
		if(MsgRecv.srcbag == INVENTORY_SLOT_NOT_SET && (MsgRecv.srcslot >= EQUIPMENT_SLOT_START && MsgRecv.srcslot <EQUIPMENT_SLOT_END))
		{
			SlotResult slotresult = _player->GetItemInterface()->FindFreeInventorySlot(SrcItem->GetProto());

			if(slotresult.Result)
			{
				MsgRecv.dstbag = slotresult.ContainerSlot;
				MsgRecv.dstslot = slotresult.Slot;
			}
			else
			{
				_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, INV_ERR_BAG_FULL);
				return;
			}
		}
		if(SrcItem->IsContainer())
		{
			if(((Container*)SrcItem)->HasItems())
			{
				_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, INV_ERR_NONEMPTY_BAG_OVER_OTHER_BAG);
				return;
			}
		}	
		if(MsgRecv.dstbag==INVENTORY_SLOT_NOT_SET)//not bag
		{
			if(MsgRecv.dstslot == INVENTORY_SLOT_NOT_SET)
			{
			}
			if(MsgRecv.dstslot >= INVENTORY_SLOT_ITEM_START && MsgRecv.dstslot < INVENTORY_SLOT_ITEM_END)
			{
				
			}
			else if(MsgRecv.dstslot >= INVENTORY_SLOT_BAG_START && MsgRecv.dstslot < INVENTORY_SLOT_BAG_END)
			{
				if(!SrcItem->IsContainer())
				{
					_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, INV_ERR_NOT_A_BAG);
					return;
				}
			}
			else if(MsgRecv.dstslot >= BANK_SLOT_BAG_START && MsgRecv.dstslot < BANK_SLOT_BAG_END )
			{
				if(!SrcItem->IsContainer())
				{
					_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, INV_ERR_NOT_A_BAG);
					return;
				}
			}
			//check if it will go to equipment slot
			else if(MsgRecv.dstslot >= EQUIPMENT_SLOT_START && MsgRecv.dstslot < EQUIPMENT_SLOT_END)
			{
				if((error=GetPlayer()->GetItemInterface()->CanEquipItemInSlot(MsgRecv.dstbag, MsgRecv.dstslot, SrcItem->GetProto())))
				{
					_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, error);
					return;
				}
			}
		}		
		else
		{
			if(SrcItem->IsContainer())
			{
				if(((Container*)SrcItem)->HasItems())
				{
					_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, INV_ERR_NONEMPTY_BAG_OVER_OTHER_BAG);
					return;
				}
				if(MsgRecv.srcbag == INVENTORY_SLOT_NOT_SET)
				{
					if(MsgRecv.dstbag == MsgRecv.srcslot)
					{
						_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, INV_ERR_ITEM_DOESNT_GO_INTO_BAG);
						return;
					}
				}
			}
		}
	}

	if(DstItem)
	{ 
		if(MsgRecv.srcbag == INVENTORY_SLOT_NOT_SET && MsgRecv.srcslot >= INVENTORY_SLOT_ITEM_START && MsgRecv.srcslot < INVENTORY_SLOT_ITEM_END)
		{
			if(DstItem->IsContainer())
			{
				if(((Container*)DstItem)->HasItems())
				{
					_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, INV_ERR_NONEMPTY_BAG_OVER_OTHER_BAG);
					return;
				}
			}
		}
		else if(MsgRecv.srcbag == INVENTORY_SLOT_NOT_SET && MsgRecv.srcslot >= INVENTORY_SLOT_BAG_START && MsgRecv.srcslot < INVENTORY_SLOT_BAG_END)
		{
			if(!SrcItem->IsContainer())
			{
				_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, INV_ERR_NOT_A_BAG);
				return;
			}
		}
		else if(MsgRecv.srcbag == INVENTORY_SLOT_NOT_SET && MsgRecv.srcslot >= BANK_SLOT_BAG_START && MsgRecv.srcslot < BANK_SLOT_BAG_END)
		{
			if(!SrcItem->IsContainer())
			{
				_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, INV_ERR_NOT_A_BAG);
				return;
			}
		}
		else if(MsgRecv.srcbag == INVENTORY_SLOT_NOT_SET && MsgRecv.srcslot >= EQUIPMENT_SLOT_START && MsgRecv.srcslot < EQUIPMENT_SLOT_END)
		{
			if((error=GetPlayer()->GetItemInterface()->CanEquipItemInSlot(MsgRecv.srcbag, MsgRecv.srcslot, DstItem->GetProto())))
			{
				_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, error);
				return;
			}
		}
// 		else if(MsgRecv.srcbag >= INVENTORY_SLOT_ITEM_START && MsgRecv.srcbag < INVENTORY_SLOT_ITEM_END)
// 		{
// 
// 		}
		else
		{
			if(DstItem->IsContainer())
			{
				if(((Container*)DstItem)->HasItems())
				{
					_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, INV_ERR_NONEMPTY_BAG_OVER_OTHER_BAG);
					return;
				}
			}
		}
	}

	/*if(GetPlayer()->CombatStatus.IsInCombat())
	{
		GetPlayer()->GetItemInterface()->BuildInventoryChangeError(SrcItem, NULL, INV_ERR_CANT_DO_IN_COMBAT);
		return;
	}*/

	if( MsgRecv.dstslot < INVENTORY_SLOT_BAG_START && MsgRecv.dstbag == INVENTORY_SLOT_NOT_SET ) //equip
	{
		if( SrcItem->GetProto()->Bonding == ITEM_BIND_ON_EQUIP )
			SrcItem->SoulBind();
		//SrcItem->OnEquip();
		//if(DstItem)
		//	DstItem->OnUnEquip();		
	}

	if( MsgRecv.srcbag == MsgRecv.dstbag )//in 1 bag
	{
		if( MsgRecv.srcbag == INVENTORY_SLOT_NOT_SET ) //in backpack
		{
			_player->GetItemInterface()->SwapItemSlots( MsgRecv.srcslot, MsgRecv.dstslot );
		}
		else//in bag
		{
			Item* pItem = _player->GetItemInterface()->GetInventoryItem( MsgRecv.srcbag );
			if( !pItem )
			{
				MyLog::log->error("HandleSwapItemOpcode : Not Found srcbag[%d]", MsgRecv.srcbag);
				return;
			}
			static_cast< Container* >( pItem )->SwapItems( MsgRecv.srcslot, MsgRecv.dstslot );
		}
	}
	else
	{
		// this is done in CanEquipItemInSlot ;)

		/*if (MsgRecv.dstbag != INVENTORY_SLOT_NOT_SET) 
		{
			uint32 DstInvSubClass = _player->GetItemInterface()->GetInventoryItem(MsgRecv.dstbag)->GetProto()->SubClass;
			uint32 SrcItemClass = SrcItem->GetProto()->Class;
			uint32 SrcItemSubClass = SrcItem->GetProto()->SubClass;
			uint32 DstInvClass = _player->GetItemInterface()->GetInventoryItem(MsgRecv.dstbag)->GetProto()->Class;

			// if its not ammo/arrows it shouldnt go there
			if( DstInvSubClass != 0  && SrcItemSubClass != DstInvSubClass || 
				( SrcItemClass == 11 && DstInvClass == 11 ) ) 
			{
				_player->GetItemInterface()->BuildInventoryChangeError(SrcItem, NULL, INV_ERR_ONLY_AMMO_CAN_GO_HERE);
				return;			
			}
		}*/

		//Check for stacking
		if(DstItem && SrcItem->GetEntry()==DstItem->GetEntry() && SrcItem->GetProto()->MaxCount>1 && SrcItem->wrapped_item_id == 0 && DstItem->wrapped_item_id == 0)
		{
			uint32 total=SrcItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT)+DstItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT);
			if(total<=DstItem->GetProto()->MaxCount)
			{
				DstItem->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT,SrcItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT));
				DstItem->m_isDirty = true;
				bool result = _player->GetItemInterface()->SafeFullRemoveItemFromSlot(MsgRecv.srcbag,MsgRecv.srcslot);
				if(!result)
				{
					GetPlayer()->GetItemInterface()->BuildInventoryChangeError(SrcItem, DstItem, INV_ERR_ITEM_CANT_STACK);
				}
				return;
			}
			else
			{
				if(DstItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT) == DstItem->GetProto()->MaxCount)
				{

				}
				else
				{
					int32 delta=DstItem->GetProto()->MaxCount-DstItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT);
					DstItem->SetUInt32Value(ITEM_FIELD_STACK_COUNT,DstItem->GetProto()->MaxCount);
					SrcItem->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT,-delta);
					SrcItem->m_isDirty = true;
					DstItem->m_isDirty = true;
					return;
				}
			}
		}
	   

		if(SrcItem)
		{
			/*
			if( MsgRecv.srcslot < EQUIPMENT_SLOT_END && MsgRecv.srcbag == INVENTORY_SLOT_NOT_SET )
			{
				if( GetPlayer()->GetItemInterface()->m_pItems[(int)MsgRecv.srcslot] != NULL )
				{
					GetPlayer()->ApplyItemMods( GetPlayer()->GetItemInterface()->m_pItems[(int)MsgRecv.srcslot], MsgRecv.srcslot, false );
					//GetPlayer()->GetItemInterface()->m_pItems[(int)MsgRecv.srcslot]->OnUnEquip();
				}
			}
			*/
			SrcItem = _player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(MsgRecv.srcbag,MsgRecv.srcslot, false);
		}

		if(DstItem)
		{
			/*
			if( MsgRecv.dstslot < EQUIPMENT_SLOT_END && MsgRecv.dstbag == INVENTORY_SLOT_NOT_SET )
			{
				if( GetPlayer()->GetItemInterface()->m_pItems[(int)MsgRecv.dstslot] != NULL )
				{
					GetPlayer()->ApplyItemMods( GetPlayer()->GetItemInterface()->m_pItems[(int)MsgRecv.dstslot], MsgRecv.dstslot, false );
					//GetPlayer()->GetItemInterface()->m_pItems[(int)MsgRecv.dstslot]->OnUnEquip();
				}
			}
			*/
			DstItem = _player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(MsgRecv.dstbag,MsgRecv.dstslot, false);
		}

		if(SrcItem)
		{
			AddItemResult result =_player->GetItemInterface()->SafeAddItem(SrcItem,MsgRecv.dstbag,MsgRecv.dstslot, false);

			if(!result)
			{
				printf("HandleSwapItem: Error while adding item to dstslot\n");
				delete SrcItem;
			}
			else if( MsgRecv.dstslot < EQUIPMENT_SLOT_END && MsgRecv.dstbag == INVENTORY_SLOT_NOT_SET )
			{
				GetPlayer()->ApplyItemMods( SrcItem, MsgRecv.dstslot, true );
				//SrcItem->OnEquip();
			}
		}

		if(DstItem)
		{
			AddItemResult result = _player->GetItemInterface()->SafeAddItem(DstItem,MsgRecv.srcbag,MsgRecv.srcslot, false);
			if(!result)
			{
				printf("HandleSwapItem: Error while adding item to srcslot\n");
				delete DstItem;
			}
			else if( MsgRecv.srcslot < EQUIPMENT_SLOT_END && MsgRecv.srcbag == INVENTORY_SLOT_NOT_SET )
			{
				GetPlayer()->ApplyItemMods( DstItem, MsgRecv.srcslot, true );
				//DstItem->OnEquip();
			}
		}
	}
}

void WorldSession::HandleSwapInvItemOpcode( CPacketUsn& packet )
{
	LockQstItemNotify QstLocker( false );
	if(!_player->IsInWorld()) return;

	MSG_C2S::stItem_Swap_Inv MsgRecv;packet >> MsgRecv;

	if(!GetPlayer())
		return;

//	MyLog::log->notice("ITEM: swap, src slot: %u dst slot: %u", (uint32)MsgRecv.srcslot, (uint32)MsgRecv.dstslot);

	if(MsgRecv.dstslot == MsgRecv.srcslot) // player trying to add item to the same slot
	{
		GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_ITEMS_CANT_BE_SWAPPED);
		return;
	}

	Item * dstitem = _player->GetItemInterface()->GetInventoryItem(MsgRecv.dstslot);
	Item * srcitem = _player->GetItemInterface()->GetInventoryItem(MsgRecv.srcslot);
	
	// allow weapon switching in combat
	bool skip_combat = false;
	if( MsgRecv.srcslot < EQUIPMENT_SLOT_END || MsgRecv.dstslot < EQUIPMENT_SLOT_END )	  // We're doing an equip swap.
	{
		if( _player->CombatStatus.IsInCombat() )
		{
			if( MsgRecv.srcslot < EQUIPMENT_SLOT_MAINHAND || MsgRecv.dstslot < EQUIPMENT_SLOT_MAINHAND )	// These can't be swapped
			{
				_player->GetItemInterface()->BuildInventoryChangeError(srcitem, dstitem, INV_ERR_CANT_DO_IN_COMBAT);
				return;
			}
			skip_combat= true;
		}
	}

	if( !srcitem )
	{
		_player->GetItemInterface()->BuildInventoryChangeError( srcitem, dstitem, INV_ERR_ITEM_DOESNT_GO_TO_SLOT );
		return;
	}

	if( MsgRecv.srcslot == MsgRecv.dstslot )
	{
		_player->GetItemInterface()->BuildInventoryChangeError( srcitem, dstitem, INV_ERR_ITEM_DOESNT_GO_TO_SLOT );
		return;
	}
	
	ui32 error = 0;
	if( ( error = _player->GetItemInterface()->CanEquipItemInSlot( INVENTORY_SLOT_NOT_SET, MsgRecv.dstslot, srcitem->GetProto(), skip_combat ) ) )
	{
		if( MsgRecv.dstslot < BANK_SLOT_BAG_END )
		{
			_player->GetItemInterface()->BuildInventoryChangeError( srcitem, dstitem, error );
			return;
		}
	}

		// extra check needed here for gems, because CanEquipInSlot does not pass enchantments, etc.
	// src is the item we're equipping, dst is the item we're replacing
	if( MsgRecv.dstslot < EQUIPMENT_SLOT_END && srcitem->HasEnchantments() )
	{
		ItemPrototype * gemitm;
		for( uint32 x = 2 ; x < 5 ; x ++ )
		{
			if( srcitem->GetEnchantment( x ) )
			{
				gemitm = ItemPrototypeStorage.LookupEntry( srcitem->GetEnchantment( x )->Enchantment->GemEntry );
				if( !gemitm || ( gemitm && !( gemitm->Flags & ITEM_FLAG_UNIQUE_EQUIP ) ) )
					continue; // baaad boy
				if( _player->GetItemInterface()->HasGemEquipped( srcitem->GetEnchantment( x )->Enchantment->GemEntry , MsgRecv.dstslot ) )
				{
					MyLog::log->debug("HandleSwapInvItemOpcode : testing gem %u on item %u" , srcitem->GetEnchantment( x )->Enchantment->GemEntry , srcitem->GetProto()->ItemId);
					_player->GetItemInterface()->BuildInventoryChangeError( srcitem, dstitem, INV_ERR_ITEM_MAX_COUNT_EQUIPPED_SOCKETED );
					return;
				}
			}
		}
	}

	if(dstitem)
	{
		if((error=_player->GetItemInterface()->CanEquipItemInSlot(INVENTORY_SLOT_NOT_SET, MsgRecv.srcslot, dstitem->GetProto(), skip_combat)))
		{
			if(MsgRecv.srcslot < BANK_SLOT_BAG_END)
			{
				MSG_S2C::stItem_Inv_Change_Failure Msg;
				Msg.error	= error;
				if(error == 1) 
				{
					Msg.RequiredLevel = dstitem->GetProto()->RequiredLevel;
				}
				Msg.srcguid = (srcitem ? srcitem->GetGUID() : uint64(0));
				Msg.dstguid = (dstitem ? dstitem->GetGUID() : uint64(0));
				SendPacket( Msg );
				return;
			}
		}
	}

	if(srcitem->IsContainer())
	{
		//source has items and dst is a backpack or bank
		if(((Container*)srcitem)->HasItems())
			if(!_player->GetItemInterface()->IsBagSlot(MsgRecv.dstslot))
			{
				_player->GetItemInterface()->BuildInventoryChangeError(srcitem,dstitem, INV_ERR_NONEMPTY_BAG_OVER_OTHER_BAG);
				return;
			}

		if(dstitem)
		{
			//source is a bag and dst slot is a bag inventory and has items
			if(dstitem->IsContainer())
			{
				if(((Container*)dstitem)->HasItems() && !_player->GetItemInterface()->IsBagSlot(MsgRecv.srcslot))
				{
					_player->GetItemInterface()->BuildInventoryChangeError(srcitem,dstitem, INV_ERR_NONEMPTY_BAG_OVER_OTHER_BAG);
					return;
				}
			}
			else
			{
				//dst item is not a bag, swap impossible
				_player->GetItemInterface()->BuildInventoryChangeError(srcitem,dstitem,INV_ERR_NONEMPTY_BAG_OVER_OTHER_BAG);
				return;
			}
		}

		//dst is bag inventory
		if(MsgRecv.dstslot < INVENTORY_SLOT_BAG_END)
		{
			if(srcitem->GetProto()->Bonding==ITEM_BIND_ON_EQUIP)
				srcitem->SoulBind();
			//srcitem->OnEquip();
			//dstitem->OnUnEquip();
		}
	}

	// swap items
	_player->GetItemInterface()->SwapItemSlots(MsgRecv.srcslot, MsgRecv.dstslot);
}

void WorldSession::HandleDestroyItemOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;

	if(_player->isDead())
	{
		SystemMessage("死亡状态无法销毁道具");
		return;
	}

	MSG_C2S::stItem_Destroy MsgRecv; packet >> MsgRecv;
	MyLog::log->notice( "ITEM: destroy, SrcInv Slot: %i Src slot: %i", MsgRecv.srcbag, MsgRecv.srcslot );
	Item *it = _player->GetItemInterface()->GetInventoryItem(MsgRecv.srcbag, MsgRecv.srcslot);

	if(it)
	{
		if(it->GetProto()->Bonding == ITEM_BIND_NOEXSTRANGE)
		{
			_player->GetItemInterface()->BuildInventoryChangeError(
				it, NULL, IVN_ERR_CANT_DESTORY);
			return;
		}
		if(it->IsContainer())
		{
			if(((Container*)it)->HasItems())
			{
				_player->GetItemInterface()->BuildInventoryChangeError(
				it, NULL, INV_ERR_CAN_ONLY_DO_WITH_EMPTY_BAGS);
				return;
			}
		}

		if(it->GetProto()->ItemId == ITEM_ENTRY_GUILD_CHARTER)
		{
			Charter *gc = _player->m_charters[CHARTER_TYPE_GUILD];
			if(gc)
				gc->Destroy();

			_player->m_charters[CHARTER_TYPE_GUILD] = NULL;
		}

		if(it->GetProto()->ItemId == ARENA_TEAM_CHARTER_2v2)
		{
			Charter *gc = _player->m_charters[CHARTER_TYPE_ARENA_2V2];
			if(gc)
				gc->Destroy();

			_player->m_charters[CHARTER_TYPE_ARENA_2V2] = NULL;
		}

		if(it->GetProto()->ItemId == ARENA_TEAM_CHARTER_5v5)
		{
			Charter *gc = _player->m_charters[CHARTER_TYPE_ARENA_5V5];
			if(gc)
				gc->Destroy();

			_player->m_charters[CHARTER_TYPE_ARENA_5V5] = NULL;
		}

		if(it->GetProto()->ItemId == ARENA_TEAM_CHARTER_3v3)
		{
			Charter *gc = _player->m_charters[CHARTER_TYPE_ARENA_3V3];
			if(gc)
				gc->Destroy();

			_player->m_charters[CHARTER_TYPE_ARENA_3V3] = NULL;
		}

		uint32 mail_id = it->GetUInt32Value(ITEM_FIELD_ITEM_TEXT_ID);
		if(mail_id)
			sMailSystem.RemoveMessageIfDeleted(mail_id, _player);
		


		/*bool result =  _player->GetItemInterface()->SafeFullRemoveItemFromSlot(MsgRecv.srcbag,SrcSlot);
		if(!result)
		{
			MyLog::log->notice("ITEM: Destroy, SrcInv Slot: %u Src slot: %u Failed", (uint32)MsgRecv.srcbag, (uint32)SrcSlot);
		}*/
		Item * pItem = _player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(MsgRecv.srcbag, MsgRecv.srcslot,false);
		if(!pItem)
			return;

		if(_player->GetCurrentSpell() && _player->GetCurrentSpell()->i_caster==pItem)
		{
			_player->GetCurrentSpell()->i_caster=NULL;
			_player->GetCurrentSpell()->cancel();
		}
		MyLog::yunyinglog->info("item-destroy-player[%u][%s] item["I64FMT"][%u] count[%u]", _player->GetLowGUID(), _player->GetName(), pItem->GetGUID(), pItem->GetProto()->ItemId, pItem->GetUInt32Value( ITEM_FIELD_STACK_COUNT ));
		sQuestMgr.OnPlayerItemLost( _player, pItem->GetEntry(), pItem->GetUInt32Value( ITEM_FIELD_STACK_COUNT ) );
		pItem->DeleteFromDB();
		delete pItem;
		_player->UpdateInrangeQuestGiverState();
	}
}

void WorldSession::HandleAutoEquipItemOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	if(_player->GetShapeShift())
	{
		MyLog::log->alert("player[%s] cann't equip in form", _player->GetName());;
		return;
	}
	MSG_C2S::stItem_AutoEquip MsgRecv;packet >> MsgRecv;
	AddItemResult result;
	int8 error=0;
	
	if(!GetPlayer())
		return;

	//MyLog::log->notice("ITEM: autoequip, Inventory slot: %i Source Slot: %i", MsgRecv.srcbag, MsgRecv.srcslot); 

	Item *eitem=_player->GetItemInterface()->GetInventoryItem(MsgRecv.srcbag, MsgRecv.srcslot);

	if(!eitem) 
	{
		_player->GetItemInterface()->BuildInventoryChangeError(eitem, NULL, INV_ERR_ITEM_NOT_FOUND);
		return;
	}

	ui8 Slot = _player->GetItemInterface()->GetItemSlotByType(eitem->GetProto()->InventoryType);
	if(Slot == ITEM_NO_SLOT_AVAILABLE)
	{
		_player->GetItemInterface()->BuildInventoryChangeError(eitem,NULL,INV_ERR_ITEM_CANT_BE_EQUIPPED);
		return;
	}
	// handle equipping of items which have unique-equipped gems
	if( eitem->HasEnchantments() )
	{
		EnchantmentInstance * enchinst;
		ItemPrototype * gemproto;
		for( uint32 x = 2 ; x < 5 ; x ++ )
		{
			if( eitem->GetEnchantment( x ) )
			{
				enchinst = eitem->GetEnchantment( x );
				gemproto = ItemPrototypeStorage.LookupEntry( enchinst->Enchantment->GemEntry );
				if( gemproto && gemproto->Flags & ITEM_FLAG_UNIQUE_EQUIP && _player->GetItemInterface()->HasGemEquipped( enchinst->Enchantment->GemEntry , MsgRecv.srcslot ) )
				{
					// boo-hoo, we got one already :/
					_player->GetItemInterface()->BuildInventoryChangeError(eitem,NULL,INV_ERR_ITEM_UNIQUE_EQUIPPABLE); // wrong error code
					return;
				}
			}
		}
	}

	// handle equipping of 2h when we have two items equipped! :) special case.
	if(Slot == EQUIPMENT_SLOT_MAINHAND || Slot == EQUIPMENT_SLOT_OFFHAND)
	{
		if( eitem->GetProto()->Class == ITEM_CLASS_WEAPON
			&& eitem->GetProto()->SubClass != ITEM_SUBCLASS_WEAPON_BAOZHU
			&& eitem->GetProto()->SubClass != ITEM_SUBCLASS_WEAPON_SHIELD )
			Slot = EQUIPMENT_SLOT_MAINHAND;//没有双持
		Item * mainhandweapon = _player->GetItemInterface()->GetInventoryItem(INVENTORY_SLOT_NOT_SET, EQUIPMENT_SLOT_MAINHAND);
		if( mainhandweapon )
		{
			if( mainhandweapon->GetProto()->InventoryType == INVTYPE_2HWEAPON )
			{
				if( Slot == EQUIPMENT_SLOT_OFFHAND && eitem->GetProto()->InventoryType == INVTYPE_WEAPON )
				{
					Slot = EQUIPMENT_SLOT_MAINHAND;
				}
			}
			else
			{
				if( Slot == EQUIPMENT_SLOT_OFFHAND && eitem->GetProto()->InventoryType == INVTYPE_WEAPON )
				{
					Slot = EQUIPMENT_SLOT_OFFHAND;
				}
			}
		}

		if((error = _player->GetItemInterface()->CanEquipItemInSlot(INVENTORY_SLOT_NOT_SET, Slot, eitem->GetProto(), true, true)))
		{
			_player->GetItemInterface()->BuildInventoryChangeError(eitem,NULL, error);
			return;
		}

		if(eitem->GetProto()->InventoryType == INVTYPE_2HWEAPON)
		{
			// see if we have a weapon equipped in the offhand, if so we need to remove it
			Item * offhandweapon = _player->GetItemInterface()->GetInventoryItem(INVENTORY_SLOT_NOT_SET, EQUIPMENT_SLOT_OFFHAND);
			if( offhandweapon != NULL )
			{
				// we need to de-equip this
				SlotResult result = _player->GetItemInterface()->FindFreeInventorySlot(offhandweapon->GetProto());
				if( !result.Result )
				{
					// no free slots for this item
					_player->GetItemInterface()->BuildInventoryChangeError(eitem,NULL, INV_ERR_BAG_FULL);
					return;
				}

				offhandweapon = _player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(INVENTORY_SLOT_NOT_SET, EQUIPMENT_SLOT_OFFHAND, false);
				if( offhandweapon == NULL )
					return;		// should never happen

				if( !_player->GetItemInterface()->SafeAddItem(offhandweapon, result.ContainerSlot, result.Slot) )
					if( !_player->GetItemInterface()->AddItemToFreeSlot(offhandweapon) )		// shouldn't happen either.
						delete offhandweapon;
			}
		}
		else
		{
			// can't equip a non-two-handed weapon with a two-handed weapon
			mainhandweapon = _player->GetItemInterface()->GetInventoryItem(INVENTORY_SLOT_NOT_SET, EQUIPMENT_SLOT_MAINHAND);
			if( mainhandweapon != NULL && mainhandweapon->GetProto()->InventoryType == INVTYPE_2HWEAPON )
			{
				// we need to de-equip this
				SlotResult result = _player->GetItemInterface()->FindFreeInventorySlot(mainhandweapon->GetProto());
				if( !result.Result )
				{
					// no free slots for this item
					_player->GetItemInterface()->BuildInventoryChangeError(eitem,NULL, INV_ERR_BAG_FULL);
					return;
				}

				mainhandweapon = _player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(INVENTORY_SLOT_NOT_SET, EQUIPMENT_SLOT_MAINHAND, false);
				if( mainhandweapon == NULL )
					return;		// should never happen

				if( !_player->GetItemInterface()->SafeAddItem(mainhandweapon, result.ContainerSlot, result.Slot) )
					if( !_player->GetItemInterface()->AddItemToFreeSlot(mainhandweapon) )		// shouldn't happen either.
						delete mainhandweapon;
			}
		}
	}
	else
	{
		if((error = _player->GetItemInterface()->CanEquipItemInSlot(INVENTORY_SLOT_NOT_SET, Slot, eitem->GetProto())))
		{
			_player->GetItemInterface()->BuildInventoryChangeError(eitem,NULL, error);
			return;
		}
	}

	if( Slot <= INVENTORY_SLOT_BAG_END )
	{
		if((error = _player->GetItemInterface()->CanEquipItemInSlot(INVENTORY_SLOT_NOT_SET, Slot, eitem->GetProto(), false, false)))
		{
			_player->GetItemInterface()->BuildInventoryChangeError(eitem,NULL, error);
			return;
		}
	}

	Item * oitem = NULL;

	if( MsgRecv.srcbag == INVENTORY_SLOT_NOT_SET )
	{
		_player->GetItemInterface()->SwapItemSlots( MsgRecv.srcslot, Slot );
	}
	else
	{
		eitem=_player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(MsgRecv.srcbag, MsgRecv.srcslot, false);
		oitem=_player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(INVENTORY_SLOT_NOT_SET, Slot, false);
		if(oitem)
		{
			result = _player->GetItemInterface()->SafeAddItem(oitem,MsgRecv.srcbag, MsgRecv.srcslot);
			if(!result)
			{
				printf("HandleAutoEquip: Error while adding item to SrcSlot");
				delete oitem;
				oitem = NULL;
			}
		}
		result = _player->GetItemInterface()->SafeAddItem(eitem, INVENTORY_SLOT_NOT_SET, Slot);
		if(!result)
		{
			printf("HandleAutoEquip: Error while adding item to Slot");
			delete eitem;
			eitem = NULL;
		}
		
	}

	if(eitem && eitem->GetProto()->Bonding==ITEM_BIND_ON_EQUIP)
		eitem->SoulBind();

	//eitem->OnEquip();
}

void WorldSession::HandleItemQuerySingleOpcode( CPacketUsn& packet )
{
	MSG_C2S::stItem_Query_Single MsgRecv;packet >> MsgRecv;

	ItemPrototype *itemProto = ItemPrototypeStorage.LookupEntry(MsgRecv.item);
	if(!itemProto)
	{
		MyLog::log->error( "WORLD: Unknown item id 0x%.8X", MsgRecv.item );
		return;
	} 

	MSG_S2C::stItem_Query_Single_Respone Msg;
	Msg.type = *itemProto;
	Msg.name = Item::BuildStringWithProto(itemProto);
	Msg.desc = itemProto->Description;
	SendPacket( Msg );
	
	/*if(SendThrottledPacket(data, true))
		delete data;*/
}

void WorldSession::HandleBuyBackOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stItem_BuyBack MsgRecv;packet >>MsgRecv;
	Item* add ;
	AddItemResult result;
	uint8 error;

	MyLog::log->notice( "WORLD: Received CMSG_BUYBACK_ITEM" );

	// prevent crashes
	//if( MsgRecv.slot < BUYBACK_SLOT_START || MsgRecv.slot >= BUYBACK_SLOT_END )
	//	return;

	//what a magical number 69???
	Item *it = _player->GetItemInterface()->GetBuyBack(MsgRecv.slot);
	if (it)
	{
		// Find free slot and break if inv full
		uint32 amount = it->GetUInt32Value(ITEM_FIELD_STACK_COUNT);
		uint32 itemid = it->GetUInt32Value(OBJECT_FIELD_ENTRY);
	  
		add = _player->GetItemInterface()->FindItemLessMax(itemid,amount, false);
	 
		   uint32 FreeSlots = _player->GetItemInterface()->CalculateFreeSlots(it->GetProto());
		if ((FreeSlots == 0) && (!add))
		{
			_player->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_INVENTORY_FULL);
			return;
		}
		
		// Check for gold
		int32 cost =_player->GetUInt32Value(PLAYER_FIELD_BUYBACK_PRICE_1 + MsgRecv.slot);
		if((int32)_player->GetUInt32Value(PLAYER_FIELD_COINAGE) < cost )
		{
			SendBuyFailed(MsgRecv.vendorguid, itemid, 2);
			return;
		}
		// Check for item uniqueness
		if ((error = _player->GetItemInterface()->CanReceiveItem(it->GetProto(), amount)))
		{
			_player->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, error);
			return;
		}
		_player->ModCoin(-cost);
		_player->GetItemInterface()->RemoveBuyBackItem(MsgRecv.slot);
		MyLog::yunyinglog->info("gold-buyback-player[%u][%s] use gold[%u] for item["I64FMT"][%u] count[%u]", _player->GetLowGUID(), _player->GetName(), cost, it->GetGUID(), itemid, amount);

		if (!add)
		{
			it->m_isDirty = true;			// save the item again on logout
			result = _player->GetItemInterface()->AddItemToFreeSlot(it);
			if(!result)
			{
				printf("HandleBuyBack: Error while adding item to free slot");
				delete it;
			}
			else
			{
				MyLog::yunyinglog->info("item-buyback-player[%u][%s] buyback["I64FMT"][%u] count[%u]", _player->GetLowGUID(), _player->GetName(), it->GetGUID(), it->GetEntry(), it->GetUInt32Value(ITEM_FIELD_STACK_COUNT));
			}
		}
		else
		{
			add->SetCount(add->GetUInt32Value(ITEM_FIELD_STACK_COUNT) + amount);
			add->m_isDirty = true;
			sQuestMgr.OnPlayerItemPickup(GetPlayer(),add);

			MyLog::yunyinglog->info("item-buyback-player[%u][%s] buyback["I64FMT"][%u] count[%u]", _player->GetLowGUID(), _player->GetName(), it->GetGUID(), it->GetEntry(), amount);
			// delete the item
			it->DeleteFromDB();

			// free the pointer
			it->DestroyForPlayer( _player );
			it->Delete();
		}

		MSG_S2C::stItem_Buy Msg;
		Msg.vendorguid = MsgRecv.vendorguid;
		Msg.itemid = itemid;
		Msg.count = amount;
		SendPacket( Msg );
	}
}

void WorldSession::HandleSellItemOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stItem_Sell MsgRecv;packet >> MsgRecv;
	MyLog::log->notice( "WORLD: Received CMSG_SELL_ITEM" );
	if(!GetPlayer())
		return;

	if(_player->isCasting())
		_player->InterruptSpell();

	// Check if item exists
	if(!MsgRecv.itemguid)
	{
		SendSellItem(MsgRecv.vendorguid, MsgRecv.itemguid, 1);
		return;
	}

	Creature *unit = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.vendorguid));
	// Check if Vendor exists
	if (unit == NULL)
	{
		SendSellItem(MsgRecv.vendorguid, MsgRecv.itemguid, 3);
		return;
	}

	Item* item = _player->GetItemInterface()->GetItemByGUID(MsgRecv.itemguid);
	if(!item)
	{
		SendSellItem(MsgRecv.vendorguid, MsgRecv.itemguid, 1);
		return; //our player doesn't have this item
	}

	ItemPrototype *it = item->GetProto();
	if(!it)
	{
		SendSellItem(MsgRecv.vendorguid, MsgRecv.itemguid, 2);
		return; //our player doesn't have this item
	}

	if(it->Bonding == ITEM_BIND_NOEXSTRANGE)
	{
		_player->GetItemInterface()->BuildInventoryChangeError(
			item, NULL, IVN_ERR_CANT_SELL);
		return;
	}

	if(item->IsContainer() && ((Container*)item)->HasItems())
	{
		SendSellItem(MsgRecv.vendorguid, MsgRecv.itemguid, 6);
		return;
	}

	// Check if item can be sold
	if (it->SellPrice == 0 || item->wrapped_item_id != 0 || it->BuyPrice == 0)
	{
		SendSellItem(MsgRecv.vendorguid, MsgRecv.itemguid, 2);
		return;
	}
	
	uint32 stackcount = item->GetUInt32Value(ITEM_FIELD_STACK_COUNT);
	uint32 quantity = 0;

	if (MsgRecv._count != 0)
	{
		quantity = MsgRecv._count;
	}
	else
	{
		quantity = stackcount; //allitems
	}

	if(quantity > stackcount) quantity = stackcount; //make sure we don't over do it
	
	uint32 price = GetSellPriceForItem(it, quantity);

	_player->ModCoin(price);
	MyLog::yunyinglog->info("gold-sellitem-player[%u][%s] get gold[%u] for item["I64FMT"][%u] count[%u]", _player->GetLowGUID(), _player->GetName(), price, item->GetGUID(), it->ItemId, quantity);

	if(quantity < stackcount)
	{
		item->SetCount(stackcount - quantity);
		item->m_isDirty = true;
	}
	else
	{
		//removing the item from the char's inventory
		item = _player->GetItemInterface()->SafeRemoveAndRetreiveItemByGuid(MsgRecv.itemguid, false); //again to remove item from slot
		if(item)
		{
			_player->GetItemInterface()->AddBuyBackItem(item, price);

			// commented by gui
			//item->DeleteFromDB();
		}
	}
	MyLog::yunyinglog->info("item-sell-player[%u][%s] sell["I64FMT"][%u] count[%u] to npc[%u]", _player->GetLowGUID(), _player->GetName(), MsgRecv.itemguid, it->ItemId, stackcount, MsgRecv.vendorguid);

	SendSellItem(MsgRecv.vendorguid, MsgRecv.itemguid, 0);
}

void WorldSession::HandleBuyItemInSlotOpcode( CPacketUsn& packet ) // drag & drop
{
	if( !_player->IsInWorld() )
		return;

	MSG_C2S::stItem_BuyInSlot MsgRecv;packet >> MsgRecv;
	MyLog::log->notice( "WORLD: Received CMSG_BUY_ITEM_IN_SLOT" );

	if( GetPlayer() == NULL )
		return;

	uint8 error;
	ui8 bagslot = INVENTORY_SLOT_NOT_SET;

	if(MsgRecv.count == 0)
      return;

	if( _player->isCasting() )
		_player->InterruptSpell();

	Creature* unit = _player->GetMapMgr()->GetCreature( GET_LOWGUID_PART(MsgRecv.vendorguid) );
	if( unit == NULL || !unit->HasItems() )
		return;

	Container* c = NULL;

	CreatureItem ci;
	unit->GetSellItemByItemId( MsgRecv.item, 1, ci );

	if( ci.itemid == 0 )
		return;

// 	if( ci.max_amount > 0 && ci.available_amount < MsgRecv.count )
// 	{
// 		_player->GetItemInterface()->BuildInventoryChangeError( 0, 0, INV_ERR_ITEM_IS_CURRENTLY_SOLD_OUT );
// 		return;
// 	}

	ItemPrototype* it = ItemPrototypeStorage.LookupEntry( MsgRecv.item );
	
	if( it == NULL)
		return;

	if(it->BuyPrice > _player->GetUInt32Value(PLAYER_FIELD_COINAGE))
	{
		_player->GetItemInterface()->BuildInventoryChangeError( 0, 0, INV_ERR_NOT_ENOUGH_MONEY );
		return;
	}

	if( it->MaxCount > 0 && ci.amount*MsgRecv.count > it->MaxCount)
	{
//		MyLog::log->debug( "SUPADBG can't carry #1 (%u>%u)" , ci.amount*amount , it->MaxCount );
		_player->GetItemInterface()->BuildInventoryChangeError( 0, 0, INV_ERR_CANT_CARRY_MORE_OF_THIS );
		return;
	}

	uint32 count_per_stack = ci.amount * MsgRecv.count;

    //if slot is diferent than -1, check for validation, else continue for auto storing.
	if(MsgRecv.slot != INVENTORY_SLOT_NOT_SET)
	{
		if(!(MsgRecv.bagguid>>32))//buy to bakcpack
		{
			if(MsgRecv.slot > INVENTORY_SLOT_ITEM_END || MsgRecv.slot < INVENTORY_SLOT_ITEM_START)
			{
				//hackers!
				_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_ITEM_DOESNT_GO_TO_SLOT);
				return;
			}
		}
		else
		{
			c=(Container*)_player->GetItemInterface()->GetItemByGUID(MsgRecv.bagguid);
			if(!c)return;
			bagslot = _player->GetItemInterface()->GetBagSlotByGuid(MsgRecv.bagguid);

			if(bagslot == INVENTORY_SLOT_NOT_SET || (c->GetProto() && (uint32)MsgRecv.slot > c->GetProto()->ContainerSlots))
			{
				_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_ITEM_DOESNT_GO_TO_SLOT);
				return;
			}
		}
	}
	else
	{
		if((MsgRecv.bagguid>>32))
		{
			c=(Container*)_player->GetItemInterface()->GetItemByGUID(MsgRecv.bagguid);
			if(!c)
			{
				_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_ITEM_NOT_FOUND);
				return;//non empty
			}

			bagslot = _player->GetItemInterface()->GetBagSlotByGuid(MsgRecv.bagguid);
			MsgRecv.slot = c->FindFreeSlot();
		}
		else
			MsgRecv.slot=_player->GetItemInterface()->FindFreeBackPackSlot();
	}

	if((error = _player->GetItemInterface()->CanReceiveItem(it, MsgRecv.count)))
	{
		_player->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, error);
		return;
	}

	if((error = _player->GetItemInterface()->CanAffordItem(it,MsgRecv.count,unit, ci.extended_cost)))
	{
		SendBuyFailed(MsgRecv.vendorguid, ci.itemid, error);
		return;
	}

	if(MsgRecv.slot==INVENTORY_SLOT_NOT_SET)
	{
		_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_BAG_FULL);
		return;
	}

	// ok our z and slot are set.
	Item * oldItem= NULL;
	Item * pItem=NULL;
	if(MsgRecv.slot != INVENTORY_SLOT_NOT_SET)
		oldItem = _player->GetItemInterface()->GetInventoryItem(bagslot, MsgRecv.slot);

	if(oldItem != NULL)
	{
		// try to add to the existing items stack
		if(oldItem->GetProto() != it)
		{
			_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_ITEM_DOESNT_GO_TO_SLOT);
			return;
		}

		if((oldItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT) + count_per_stack) > it->MaxCount)
		{
//			MyLog::log->debug( "SUPADBG can't carry #2" );
			_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_CANT_CARRY_MORE_OF_THIS);
			return;
		}

		oldItem->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, count_per_stack);
		oldItem->m_isDirty = true;
		pItem=oldItem;
	}
	else
	{
		// create new item
		if(MsgRecv.slot == INVENTORY_SLOT_NOT_SET)
			MsgRecv.slot = c->FindFreeSlot();
		
		if(MsgRecv.slot==ITEM_NO_SLOT_AVAILABLE || MsgRecv.slot == INVENTORY_SLOT_NOT_SET )
		{
			_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_BAG_FULL);
			return;
		}

        pItem = objmgr.CreateItem(it->ItemId, _player);
		if(pItem)
		{
			pItem->SetUInt32Value(ITEM_FIELD_STACK_COUNT, count_per_stack);
			pItem->m_isDirty = true;
//			MyLog::log->debug( "SUPADBG bagslot=%u, slot=%u" , bagslot, slot );
			if(!_player->GetItemInterface()->SafeAddItem(pItem, bagslot, MsgRecv.slot))
			{
				delete pItem;
				return;
			}
		}
		else
			return;
	}

	SendItemPushResult(pItem, false, true, false, (pItem==oldItem) ? false : true, bagslot, MsgRecv.slot, MsgRecv.count*ci.amount);

	MSG_S2C::stItem_Buy Msg;
	Msg.vendorguid = MsgRecv.vendorguid;
	Msg.itemid	= MsgRecv.item;
	Msg.count = MsgRecv.count;
	SendPacket( Msg );
	MyLog::log->notice( "WORLD: Sent SMSG_BUY_ITEM" );
	MyLog::yunyinglog->info("item-buy-player[%u][%s] buy["I64FMT"][%u] count[%u][%u] from npc[%u]", _player->GetLowGUID(), _player->GetName(), pItem->GetGUID(), it->ItemId, MsgRecv.count, ci.amount, MsgRecv.vendorguid);

	_player->GetItemInterface()->BuyItem(it,MsgRecv.count,unit, ci.extended_cost);
// 	if(ci.max_amount)
// 	{
// 		unit->ModAvItemAmount(ci.itemid,ci.amount*MsgRecv.count);
// 
// 		// there is probably a proper opcode for this. - burlex
// 		SendInventoryList(unit);
// 	}
}

void WorldSession::HandleBuyItemOpcode( CPacketUsn& packet ) // right-click on item
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stItem_Buy MsgRecv;packet >> MsgRecv;

	if(!GetPlayer())
		return;

	Item *add = NULL;
	uint8 error = 0;
	SlotResult slotresult;
	AddItemResult result;

	Creature *unit = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.vendorguid));
	if (unit == NULL || !unit->HasItems())
		return;

	if(MsgRecv.count < 1)
		MsgRecv.count = 1;

	CreatureItem item;
	unit->GetSellItemByItemId(MsgRecv.item, MsgRecv.amt, item);

	if(item.itemid == 0)
	{
		// vendor does not sell this item.. bitch about cheaters?
		_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_DONT_OWN_THAT_ITEM);
		return;
	}

// 	if (item.max_amount>0 && item.available_amount<MsgRecv.count)
// 	{
// 		_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_ITEM_IS_CURRENTLY_SOLD_OUT);
// 		return;
// 	}

	ItemPrototype *it = ItemPrototypeStorage.LookupEntry(MsgRecv.item);
	if(!it) 
	{
		_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_DONT_OWN_THAT_ITEM);
		return;
	}

	if( MsgRecv.count > it->MaxCount )
	{
		_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_ITEM_CANT_STACK);
		return;
	}

	if((error = _player->GetItemInterface()->CanReceiveItem(it, MsgRecv.count*item.amount)))
	{
		_player->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, error);
		return;
	}

   if((error = _player->GetItemInterface()->CanAffordItem(it, MsgRecv.count, unit, item.extended_cost)))
   {
      SendBuyFailed(MsgRecv.vendorguid, MsgRecv.item, error);
      return;
   }
 
   Item* qstItem = NULL;
	// Find free slot and break if inv full
	add = _player->GetItemInterface()->FindItemLessMax(MsgRecv.item,MsgRecv.count*item.amount, false);
	if (!add)
	{
		slotresult = _player->GetItemInterface()->FindFreeInventorySlot(it);
	}
	if ((!slotresult.Result) && (!add))
	{
		//Our User doesn't have a free Slot in there bag
		_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_INVENTORY_FULL);
		return;
	}
	qstItem = add;

	if(!add)
	{
		Item *itm = objmgr.CreateItem(item.itemid, _player);
		if(!itm)
		{
			_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_DONT_OWN_THAT_ITEM);
			return;
		}

		itm->m_isDirty=true;
		itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, MsgRecv.count*item.amount);
		itm->WashEnchantment();

		if(slotresult.ContainerSlot == ITEM_NO_SLOT_AVAILABLE)
		{
			result = _player->GetItemInterface()->SafeAddItem(itm, INVENTORY_SLOT_NOT_SET, slotresult.Slot);
			if(!result)
			{
				delete itm;
			}
			else
			{
				SendItemPushResult(itm, false, true, false, true, INVENTORY_SLOT_NOT_SET, slotresult.Result, MsgRecv.count*item.amount);
				qstItem = itm;
			}
		}
		else 
		{
			if(Item *bag = _player->GetItemInterface()->GetInventoryItem(slotresult.ContainerSlot))
			{
				if( !((Container*)bag)->AddItem(slotresult.Slot, itm) )
					delete itm;
				else
				{
					SendItemPushResult(itm, false, true, false, true, slotresult.ContainerSlot, slotresult.Result, item.amount);
					qstItem = itm;
				}
			}
		}
		MyLog::yunyinglog->info("item-buy-player[%u][%s] buy["I64FMT"][%u] count[%u][%u] from npc["I64FMT"]", _player->GetLowGUID(), _player->GetName(), itm->GetGUID(), it->ItemId, MsgRecv.count, item.amount, MsgRecv.vendorguid);
	}
	else
	{
		add->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, MsgRecv.count*item.amount);
		add->m_isDirty = true;
		sQuestMgr.OnPlayerItemPickup( _player, add );
		SendItemPushResult(add, false, true, false, false, _player->GetItemInterface()->GetBagSlotByGuid(add->GetGUID()), 1, MsgRecv.count*item.amount);
		MyLog::yunyinglog->info("item-buy-player[%u][%s] buy["I64FMT"][%u] count[%u][%u] from npc["I64FMT"]", _player->GetLowGUID(), _player->GetName(), add->GetGUID(), it->ItemId, MsgRecv.count, item.amount, MsgRecv.vendorguid);
	}

	MSG_S2C::stItem_Buy Msg;
	Msg.vendorguid = MsgRecv.vendorguid;
	Msg.itemid = MsgRecv.item;
	Msg.count = uint32(MsgRecv.count*item.amount);
	SendPacket( Msg );
	_player->GetItemInterface()->BuyItem(it,MsgRecv.count,unit, item.extended_cost);
// 	if(item.max_amount)
// 	{
// 	 unit->ModAvItemAmount(item.itemid,item.amount*MsgRecv.count);
// 
// 	 SendInventoryList(unit);
// 	}
}

void WorldSession::HandleListInventoryOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld())
		return;

	MSG_C2S::stItem_List_Inventory MsgRecv;packet >> MsgRecv;

	Creature *unit = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.vendorguid));
	if (unit == NULL)
		return;

	/*if(unit->GetAIInterface())
		unit->GetAIInterface()->StopMovement(180000);*/

	SendInventoryList(unit);
}

void WorldSession::SendInventoryList(Creature* unit)
{
	if(!unit->HasItems())
	{
		return;
	}

	MSG_S2C::stItem_List_Inventroy Msg;

	Msg.vendorguid	= unit->GetGUID();

	ItemPrototype * curItem;
	uint32 counter = 0;

	for(std::vector<CreatureItem>::iterator itr = unit->GetSellItemBegin(); itr != unit->GetSellItemEnd(); ++itr)
	{
		if(itr->itemid && (itr->max_amount == 0 || (itr->max_amount>0 && itr->available_amount >0)))
		{
			if((curItem = ItemPrototypeStorage.LookupEntry(itr->itemid)))
			{
// 				if(curItem->AllowableClass && !(_player->getClassMask() & curItem->AllowableClass))
// 					continue;
// 				if(curItem->AllowableRace && !(_player->getRaceMask() & curItem->AllowableRace))
// 					continue;

				int32 av_am = (itr->max_amount>0)?itr->available_amount:-1;

				Msg.vItem.push_back(MSG_S2C::stItem_List_Inventroy::stItem(++counter,curItem->ItemId,curItem->DisplayInfoID[0][0],av_am,GetBuyPriceForItem(curItem,1,_player,unit),-1,itr->amount,itr->extended_cost ? itr->extended_cost : (curItem->extended_cost ?curItem->extended_cost->costid : 0), itr->amount ));
			}
		}
	}

	SendPacket( Msg );
	MyLog::log->notice( "WORLD: Sent SMSG_LIST_INVENTORY" );
}
void WorldSession::HandleAutoStoreBagItemOpcode( CPacketUsn& packet )
{
	MSG_C2S::stItem_AutoStore_Bag MsgRecv;packet>>MsgRecv;
	MyLog::log->notice( "WORLD: Recvd CMSG_AUTO_STORE_BAG_ITEM" );
	
	if(!GetPlayer())
		return;

	//WorldPacket data;
	Item *srcitem = NULL;
	Item *dstitem= NULL;
	ui8 NewSlot = 0;
	int8 error;
	AddItemResult result;

	srcitem = _player->GetItemInterface()->GetInventoryItem(MsgRecv.srcbag, MsgRecv.srcslot);

	//source item exists
	if(srcitem)
	{
		//src containers cant be moved if they have items inside
		if(srcitem->IsContainer() && static_cast<Container*>(srcitem)->HasItems())
		{
			_player->GetItemInterface()->BuildInventoryChangeError(srcitem, 0, INV_ERR_NONEMPTY_BAG_OVER_OTHER_BAG);
			return;
		}
		if(srcitem->GetProto()->Class == ITEM_CLASS_QUEST)
		{
			_player->GetItemInterface()->BuildInventoryChangeError(srcitem, 0, INV_ERR_ITEM_DOESNT_GO_TO_SLOT);
			return;
		}
		//check for destination now before swaping.
		//destination is backpack
		if(MsgRecv.dstbag == INVENTORY_SLOT_NOT_SET)
		{
			//check for space
			NewSlot = _player->GetItemInterface()->FindFreeBackPackSlot();
			if(NewSlot == ITEM_NO_SLOT_AVAILABLE)
			{
				_player->GetItemInterface()->BuildInventoryChangeError(srcitem, 0, INV_ERR_BAG_FULL);
				return;
			}
			else
			{
				//free space found, remove item and add it to the destination
				srcitem = _player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(MsgRecv.srcbag, MsgRecv.srcslot, false);
				if( srcitem )
				{
					result = _player->GetItemInterface()->SafeAddItem(srcitem, INVENTORY_SLOT_NOT_SET, NewSlot);
					if(!result)
					{
						printf("HandleAutoStoreBagItem: Error while adding item to newslot");
						delete srcitem;
						return;
					}
					if( srcitem->IsContainer() && srcitem->GetProto()->Bonding == ITEM_BIND_ON_USE && !srcitem->IsSoulbound() )
						srcitem->SoulBind();
				}
			}
		}
		else
		{
			if((error=_player->GetItemInterface()->CanEquipItemInSlot(MsgRecv.dstbag,  MsgRecv.dstbag, srcitem->GetProto())))
			{
				if(MsgRecv.dstbag < BANK_SLOT_BAG_END)
				{
					_player->GetItemInterface()->BuildInventoryChangeError(srcitem,dstitem, error);
					return;
				}
			}

			//destination is a bag
			dstitem = _player->GetItemInterface()->GetInventoryItem(MsgRecv.dstbag);
			if(dstitem)
			{
				//dstitem exists, detect if its a container
				if(dstitem->IsContainer())
				{
					NewSlot = static_cast<Container*>(dstitem)->FindFreeSlot();
					if(NewSlot == ITEM_NO_SLOT_AVAILABLE)
					{
						_player->GetItemInterface()->BuildInventoryChangeError(srcitem, 0, INV_ERR_BAG_FULL);
						return;
					}
					else
					{
						srcitem = _player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(MsgRecv.srcbag, MsgRecv.srcslot, false);
						if( srcitem != NULL )
						{
							result = _player->GetItemInterface()->SafeAddItem(srcitem, MsgRecv.dstbag, NewSlot);
							if(!result)
							{
								printf("HandleBuyItemInSlot: Error while adding item to newslot");
								delete srcitem;
								return;
							}		
						}
						if( srcitem->IsContainer() && srcitem->GetProto()->Bonding == ITEM_BIND_ON_USE && !srcitem->IsSoulbound() )
							srcitem->SoulBind();
					}
				}
				else
				{
					_player->GetItemInterface()->BuildInventoryChangeError(srcitem, 0,  INV_ERR_ITEM_DOESNT_GO_TO_SLOT);
					return;
				}
			}
			else
			{
				_player->GetItemInterface()->BuildInventoryChangeError(srcitem, 0, INV_ERR_ITEM_DOESNT_GO_TO_SLOT);
				return;
			}
		}
	}
	else
	{
		_player->GetItemInterface()->BuildInventoryChangeError(srcitem, 0, INV_ERR_ITEM_NOT_FOUND);
		return;
	}
}

void WorldSession::HandleReadItemOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stItem_Read MsgRecv;packet >> MsgRecv;

	if(!GetPlayer())
		return;

	Item *item = _player->GetItemInterface()->GetInventoryItem(MsgRecv.bag, MsgRecv.slot);
	MyLog::log->debug("Received CMSG_READ_ITEM %d", MsgRecv.slot);

	if(item)
	{
		// Check if it has pagetext
	   
		if(item->GetProto()->PageId)
		{
			MSG_S2C::stItem_Read_OK Msg;
			Msg.guid = item->GetGUID();
			SendPacket(Msg);
			MyLog::log->debug("Sent SMSG_READ_OK %d", item->GetGUID());
		}	
	}
}

SUNYOU_INLINE uint32 RepairItemCost(Player * pPlayer, Item * pItem)
{
// 	DurabilityCostsEntry * dcosts = dbcDurabilityCosts.LookupEntry(pItem->GetProto()->ItemLevel);
// 	if(!dcosts)
// 	{
// 		MyLog::log->error("Repair: Unknown item level (%u)", dcosts);
// 		return 0;
// 	}
// 
// 	DurabilityQualityEntry * dquality = dbcDurabilityQuality.LookupEntry((pItem->GetProto()->Quality + 1) * 2);
// 	if(!dquality)
// 	{
// 		MyLog::log->error("Repair: Unknown item quality (%u)", dquality);
// 		return 0;
// 	}
// 
// 	uint32 dmodifier = dcosts->modifier[pItem->GetProto()->Class == ITEM_CLASS_WEAPON ? pItem->GetProto()->SubClass : pItem->GetProto()->SubClass + 21];
// 	uint32 cost = long2int32((pItem->GetDurabilityMax() - pItem->GetDurability()) * dmodifier * double(dquality->quality_modifier));
	//uint32 cost = (float)(pItem->GetDurabilityMax() - pItem->GetDurability())/pItem->GetDurabilityMax() * pItem->GetProto()->BuyPrice;
	if (pItem->GetDurabilityMax() > pItem->GetDurability() )
	{
		uint32 cost = (float)(pItem->GetDurabilityMax() - pItem->GetDurability())/pItem->GetDurabilityMax() * GetBuyPriceForItem(pItem->GetProto(), 1, pPlayer, NULL );
		cost = cost ? cost : 1;
		return cost ;
	}
	return 0;
}

SUNYOU_INLINE bool RepairItem(Player * pPlayer, Item * pItem)
{
	//int32 cost = (int32)pItem->GetUInt32Value( ITEM_FIELD_MAXDURABILITY ) - (int32)pItem->GetUInt32Value( ITEM_FIELD_DURABILITY );
	if(pItem->GetProto()->Class != ITEM_CLASS_WEAPON && pItem->GetProto()->Class != ITEM_CLASS_ARMOR)
	{
		return false;
	}
	int32 cost = RepairItemCost(pPlayer, pItem);
	if( cost <= 0 )
		return false;

	if( cost > (int32)pPlayer->GetUInt32Value( PLAYER_FIELD_COINAGE ) )
	{
		pPlayer->GetItemInterface()->BuildInventoryChangeError( 0, 0, INV_ERR_NOT_ENOUGH_MONEY );
		return false;
	}

	pPlayer->ModCoin(-cost );
	pItem->SetDurabilityToMax();
	pItem->m_isDirty = true;

	MyLog::yunyinglog->info("gold-repairitem-player[%u][%s] cost gold[%u] for item["I64FMT"][%u]", pPlayer->GetLowGUID(), pPlayer->GetName(), cost, pItem->GetGUID(), pItem->GetProto()->ItemId);
	return true;
}

void WorldSession::HandleRepairItemOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stItem_Repair MsgRecv;packet >> MsgRecv;
	if(!GetPlayer())
		return;
	Item * pItem;
	Container * pContainer;
	uint32 j, i;

	Creature * pCreature = _player->GetMapMgr()->GetCreature( GET_LOWGUID_PART(MsgRecv.npcguid) );
	if( pCreature == NULL )
		return;

	if( !pCreature->HasFlag( UNIT_NPC_FLAGS, UNIT_NPC_FLAG_ARMORER ) )
		return;

	if( !MsgRecv.itemguid ) 
	{
		for( i = 0; i < MAX_INVENTORY_SLOT - EXTRA_INVENTORY_SLOT_COUNT; i++ )
		{
			pItem = _player->GetItemInterface()->GetInventoryItem( i );
			if( pItem != NULL )
			{
				if( pItem->IsContainer() )
				{
					pContainer = static_cast<Container*>( pItem );
					for( j = 0; j < pContainer->GetProto()->ContainerSlots; ++j )
					{
						pItem = pContainer->GetItem( j );
						if( pItem != NULL )
							RepairItem( _player, pItem );
					}
				}
				else
				{
					if( pItem->GetDurabilityMax() > 0 && i < INVENTORY_SLOT_BAG_END && pItem->GetDurability() <= 0 )
					{
						if( RepairItem( _player, pItem ) )
							_player->ApplyItemMods( pItem, i, true );
					}
					else
					{
						RepairItem( _player, pItem );
					}					
				}
			}
		}
	}
	else 
	{
		Item *item = _player->GetItemInterface()->GetItemByGUID(MsgRecv.itemguid);
		if(item)
		{
			SlotResult *searchres=_player->GetItemInterface()->LastSearchResult();//this never gets null since we get a pointer to the inteface internal var
			if( item->GetDurabilityMax() > 0 && searchres->Slot < INVENTORY_SLOT_BAG_END && item->GetDurability() <= 0 )
			{
				if( RepairItem( _player, item ) )
					_player->ApplyItemMods( item, searchres->Slot, true );
			}
			else
			{
				RepairItem( _player, item );
			}

// 			uint32 dDurability = item->GetDurabilityMax() - item->GetDurability();
// 
// 			if (dDurability)
// 			{
// 				if( RepairItem(_player, item) )
// 					_player->ApplyItemMods(item, searchres->Slot, true);
// 			}
/*
			// the amount of durability that is needed to be added is the amount of money to be payed
				if (dDurability <= _player->GetUInt32Value(PLAYER_FIELD_COINAGE))
				{
					int32 cDurability = item->GetDurability();
					_player->ModUnsigned32Value( PLAYER_FIELD_COINAGE , -(int32)dDurability );
					item->SetDurabilityToMax();
					item->m_isDirty = true;
					
					//only apply item mods if they are on char equiped
	//printf("we are fixing a single item in inventory at bagslot %u and slot %u\n",searchres->ContainerSlot,searchres->Slot);
					if(cDurability <= 0 && searchres->ContainerSlot==INVALID_BACKPACK_SLOT && searchres->Slot<INVENTORY_SLOT_BAG_END)
						_player->ApplyItemMods(item, searchres->Slot, true);
				}
				else
				{
					// not enough money
				}
			}
*/
		}
	}

	_player->GetItemInterface()->BuildInventoryChangeError( 0, 0, IVN_ERR_BUILD_OK );
	MyLog::log->debug("Received CMSG_REPAIR_ITEM %d", MsgRecv.itemguid );
}
void WorldSession::HandleBuyBankSlotOpcode(CPacketUsn& packet) 
{
	if(!_player->IsInWorld()) return;
	//CHECK_PACKET_SIZE(recvPacket, 12);
	uint32 bytes,slots;
	int32 price;
	MyLog::log->debug("WORLD: CMSG_BUY_bytes_SLOT");

	bytes = GetPlayer()->GetUInt32Value(PLAYER_BYTES_2);
	slots =(uint8) (bytes >> 16);
	if( slots >= 7 )
		return;

	MyLog::log->notice("PLAYER: Buy bytes bag slot, slot number = %d", slots);
	BankSlotPrice* bsp = dbcBankSlotPrices.LookupEntry(slots+1);
	if( !bsp )
		return;

	price = bsp->Price;

	if( !bsp->isYuanbao )
	{
		if ((int32)_player->GetUInt32Value(PLAYER_FIELD_COINAGE) >= price) 
		{
		   _player->SetUInt32Value(PLAYER_BYTES_2, (bytes&0xff00ffff) | ((slots+1) << 16) );
		   _player->ModCoin(-price);
		   MyLog::yunyinglog->info("gold-buybankslot-player[%u][%s] cost gold[%u]", _player->GetLowGUID(), _player->GetName(), price);
		}
		else
		{
			_player->GetItemInterface()->BuildInventoryChangeError( 0, 0, INV_ERR_NOT_ENOUGH_MONEY );
		}
	}
	else
	{
		if ((int32)_player->GetCardPoints() >= price) 
		{
			_player->SetUInt32Value(PLAYER_BYTES_2, (bytes&0xff00ffff) | ((slots+1) << 16) );
			_player->SpendPoints( price, "buy bank slot" );
			MyLog::yunyinglog->info("yuanbao-buybankslot-player[%u][%s] cost yuanbao[%u]", _player->GetLowGUID(), _player->GetName(), price);
		}
		else
		{
			_player->GetItemInterface()->BuildInventoryChangeError( 0, 0, INV_ERR_NOT_ENOUGH_YUANBAO );
		}
	}
}

void WorldSession::HandleAutoBankItemOpcode(CPacketUsn& packet)
{
	if( !_player || !_player->IsInWorld() ) return;
	MSG_C2S::stItem_AutoBank MsgRecv;packet >> MsgRecv;
	MyLog::log->debug("WORLD: CMSG_AUTO_BANK_ITEM");

	//WorldPacket data;

	SlotResult slotresult;

	MyLog::log->notice("ITEM: Auto Bank, Inventory slot: %u Source Slot: %u", (uint32)MsgRecv.srcbag, (uint32)MsgRecv.srcslot);

	Item *eitem=_player->GetItemInterface()->GetInventoryItem(MsgRecv.srcbag,MsgRecv.srcslot);

	if(!eitem) 
	{
		_player->GetItemInterface()->BuildInventoryChangeError(eitem, NULL, INV_ERR_ITEM_NOT_FOUND);
		return;
	}

	slotresult =  _player->GetItemInterface()->FindFreeBankSlot(eitem->GetProto());

	if(!slotresult.Result)
	{
		_player->GetItemInterface()->BuildInventoryChangeError(eitem, NULL, INV_ERR_BANK_FULL);
		return;
	}
	else
	{
        eitem = _player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(MsgRecv.srcbag,MsgRecv.srcslot, false);
		if(!_player->GetItemInterface()->SafeAddItem(eitem, slotresult.ContainerSlot, slotresult.Slot))
		{
			MyLog::log->debug("[ERROR]AutoBankItem: Error while adding item to bank bag!\n");
            if( !_player->GetItemInterface()->SafeAddItem(eitem, MsgRecv.srcbag, MsgRecv.srcslot) )
				delete eitem;
		}
	}
}

void WorldSession::HandleAutoStoreBankItemOpcode(CPacketUsn& packet)
{
	if( !_player || !_player->IsInWorld() ) return;
	MSG_C2S::stItem_AutoStore_Bank MsgRecv;packet >> MsgRecv;
	MyLog::log->debug("WORLD: CMSG_AUTOSTORE_BANK_ITEM");

	//WorldPacket data;

	MyLog::log->notice("ITEM: AutoStore Bank Item, Inventory slot: %u Source Slot: %u", (uint32)MsgRecv.srcbag, (uint32)MsgRecv.srcslot);

	Item *eitem=_player->GetItemInterface()->GetInventoryItem(MsgRecv.srcbag,MsgRecv.srcslot);

	if(!eitem) 
	{
		_player->GetItemInterface()->BuildInventoryChangeError(eitem, NULL, INV_ERR_ITEM_NOT_FOUND);
		return;
	}

	if(eitem->GetProto()->Class == ITEM_CLASS_QUEST)
	{
		_player->GetItemInterface()->BuildInventoryChangeError(eitem, 0, INV_ERR_ITEM_DOESNT_GO_TO_SLOT);
		return;
	}

	SlotResult slotresult = _player->GetItemInterface()->FindFreeInventorySlot(eitem->GetProto());

	if(!slotresult.Result)
	{
		_player->GetItemInterface()->BuildInventoryChangeError(eitem, NULL, INV_ERR_INVENTORY_FULL);
		return;
	}
	else
	{
        eitem = _player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot(MsgRecv.srcbag, MsgRecv.srcslot, false);
		if (!_player->GetItemInterface()->AddItemToFreeSlot(eitem))
		{
			MyLog::log->debug("[ERROR]AutoStoreBankItem: Error while adding item from one of the bank bags to the player bag!\n");
           if( !_player->GetItemInterface()->SafeAddItem(eitem, MsgRecv.srcbag, MsgRecv.srcslot) )
			   delete eitem;
		}
	}
}

void WorldSession::HandleCancelTemporaryEnchantmentOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stItem_Cancel_Temporary_Enchantment MsgRecv;packet >> MsgRecv;
	Item * item = _player->GetItemInterface()->GetInventoryItem(MsgRecv.srcbag,MsgRecv.srcslot);
	if(!item) return;

	item->RemoveAllEnchantments(true);
}

void WorldSession::HandleInsertGemOpcode(CPacketUsn& packet)
{
	//GemPropertyEntry * gp;
	EnchantEntry * Enchantment;
	bool IsOK = false;
	MSG_C2S::stItem_Socket_Gems MsgRecv;packet >> MsgRecv;

	Item * TargetItem =_player->GetItemInterface()->GetItemByGUID(MsgRecv.itemguid);
	if(!TargetItem)
		return;
	int slot =_player->GetItemInterface()->GetInventorySlotByGuid(MsgRecv.itemguid);
	bool apply = (slot>=0 && slot <19);
	uint32 FilledSlots=0;

	bool ColorMatch[3] = { true, true, true };
	for(uint32 i = 0;i<TargetItem->GetSocketsCount();i++)
	{
		if( MsgRecv.vGemsguid.size() <= i )
		{
			MyLog::log->error("HandleInsertGemOpcode: not correct size[%d]", MsgRecv.vGemsguid.size() );
			return;
		}
		EnchantmentInstance * EI= TargetItem->GetEnchantment(2+i);
		if(EI)
		{
			FilledSlots++;
			ItemPrototype * ip = ItemPrototypeStorage.LookupEntry(EI->Enchantment->GemEntry);
			if( !ip )
				continue;

			if( !(ip->GemColorMask & TargetItem->GetProto()->Sockets[i].SocketColor) )
				ColorMatch[i] = false;
			else
				ColorMatch[i] = true;
		}

		if(MsgRecv.vGemsguid[i])//add or replace gem
		{
			Item* itTemp = _player->GetItemInterface()->GetItemByGUID( MsgRecv.vGemsguid[i] );
			if( !itTemp )
				continue;
			ItemPrototype* proto = itTemp->GetProto();
			if( (TargetItem->GetProto()->Sockets[i].SocketColor == META_SOCKET && proto->GemColorMask != META_SOCKET)
				|| (proto->GemColorMask == META_SOCKET && TargetItem->GetProto()->Sockets[i].SocketColor != META_SOCKET) )
				continue;

			if( proto->Flags & ITEM_FLAG_UNIQUE_EQUIP && _player->GetItemInterface()->HasGemEquipped( proto->ItemId , -1 ) )
			{
				_player->GetItemInterface()->BuildInventoryChangeError( TargetItem , TargetItem , INV_ERR_ITEM_MAX_COUNT_EQUIPPED_SOCKETED );
				continue;
			}

			Item * it = _player->GetItemInterface()->SafeRemoveAndRetreiveItemByGuid(MsgRecv.vGemsguid[i],true);
			if(!it)
				continue;

			delete it;
			//gp = GemPropertyStorage.LookupEntry(it->GetProto()->GemProperties);
			
		
			//if(!gp)
			//	continue;
			if( proto->GemColorMask == 0 )
				continue;

			if( !(proto->GemColorMask & TargetItem->GetProto()->Sockets[i].SocketColor) )
				ColorMatch[i] = false;
			else
				ColorMatch[i] = true;

			//if(!gp->EnchantmentID)//this is ok in few cases
			//	continue;
				  
			if(EI)//replace gem
				TargetItem->RemoveEnchantment(2+i);//remove previous
			else//add gem
			{
				FilledSlots++;
			}

			Enchantment = dbcEnchant.LookupEntry( proto->GemProperties );
			if(Enchantment)
				TargetItem->AddEnchantment(Enchantment, 0, true,apply,false,2+i);

			IsOK = true;
		}
	}

	//Add color match bonus
	if(TargetItem->GetProto()->SocketBonus)
	{
		if(ColorMatch[0] && ColorMatch[1] && ColorMatch[2] && (FilledSlots==TargetItem->GetSocketsCount()))
		{
			if(TargetItem->HasEnchantment(TargetItem->GetProto()->SocketBonus) > 0)
				return;

			Enchantment = dbcEnchant.LookupEntry(TargetItem->GetProto()->SocketBonus);
			if(Enchantment)
			{
				//uint32 Slot = TargetItem->FindFreeEnchantSlot(Enchantment,0);
				TargetItem->AddEnchantment(Enchantment, 0, true,apply,false, 5);
			}
		}else //remove
		{
			TargetItem->RemoveSocketBonusEnchant();
		}
	}

	if( IsOK )
	{
		TargetItem->m_isDirty = true;
		MSG_S2C::stInsertGemAck msg;
		SendPacket( msg );
	}
}

void WorldSession::HandleWrapItemOpcode( CPacketUsn& packet )
{
	if( !_player->IsInWorld() )
		return;
	MSG_C2S::stItem_Wrap MsgRecv;packet >> MsgRecv;
	uint32 source_entry;
	uint32 itemid;
	Item * src,*dst;

	src = _player->GetItemInterface()->GetInventoryItem( MsgRecv.srcbag, MsgRecv.srcslot );
	dst = _player->GetItemInterface()->GetInventoryItem( MsgRecv.dstbag, MsgRecv.dstslot );

	if( !src || !dst )
		return;

	if(src == dst || !(src->GetProto()->Class == 0 && src->GetProto()->SubClass == 8))
	{
		_player->GetItemInterface()->BuildInventoryChangeError( src, dst, INV_ERR_WRAPPED_CANT_BE_WRAPPED );
		return;
	}

	if( dst->GetUInt32Value( ITEM_FIELD_STACK_COUNT ) > 1 )
	{
		_player->GetItemInterface()->BuildInventoryChangeError( src, dst, INV_ERR_STACKABLE_CANT_BE_WRAPPED );
		return;
	}

	if( dst->GetProto()->MaxCount > 1 )
	{
		_player->GetItemInterface()->BuildInventoryChangeError( src, dst, INV_ERR_STACKABLE_CANT_BE_WRAPPED );
		return;
	}

	if( dst->IsSoulbound() && !GetPermissionCount())
	{
		_player->GetItemInterface()->BuildInventoryChangeError( src, dst, INV_ERR_BOUND_CANT_BE_WRAPPED );
		return;
	}

	if( dst->wrapped_item_id || src->wrapped_item_id )
	{
		_player->GetItemInterface()->BuildInventoryChangeError( src, dst, INV_ERR_WRAPPED_CANT_BE_WRAPPED );
		return;
	}

	if( dst->GetProto()->Unique )
	{
		_player->GetItemInterface()->BuildInventoryChangeError( src, dst, INV_ERR_UNIQUE_CANT_BE_WRAPPED );
		return;
	}

	if( dst->IsContainer() )
	{
		_player->GetItemInterface()->BuildInventoryChangeError( src, dst, INV_ERR_BAGS_CANT_BE_WRAPPED );
		return;
	}

	if( dst->HasEnchantments() )
	{
		_player->GetItemInterface()->BuildInventoryChangeError( src, dst, INV_ERR_ITEM_LOCKED );
		return;
	}
	if( MsgRecv.dstbag == (ui8)0xFF && ( MsgRecv.dstslot >= EQUIPMENT_SLOT_START && MsgRecv.dstslot <= INVENTORY_SLOT_BAG_END ) )
	{
		_player->GetItemInterface()->BuildInventoryChangeError( src, dst, INV_ERR_EQUIPPED_CANT_BE_WRAPPED );
		return;
	}

	// all checks passed ok
	source_entry = src->GetEntry();
	itemid = source_entry;
// 	switch( source_entry )
// 	{
// 	case 5042:
// 		itemid = 5043;
// 		break;
// 
// 	case 5048:
// 		itemid = 5044;
// 		break;
// 
// 	case 17303:
// 		itemid = 17302;
// 		break;
// 
// 	case 17304:
// 		itemid = 17305;
// 		break;
// 
// 	case 17307:
// 		itemid = 17308;
// 		break;
// 
// 	case 21830:
// 		itemid = 21831;
// 		break;
// 
// 	default:
// 		_player->GetItemInterface()->BuildInventoryChangeError( src, dst, INV_ERR_WRAPPED_CANT_BE_WRAPPED );
// 		return;
// 		break;
// 	}

	dst->SetProto( src->GetProto() );

	if( src->GetUInt32Value( ITEM_FIELD_STACK_COUNT ) <= 1 )
	{
		// destroy the source item
		_player->GetItemInterface()->SafeFullRemoveItemByGuid( src->GetGUID() );
	}
	else
	{
		// reduce stack count by one
		src->ModUnsigned32Value( ITEM_FIELD_STACK_COUNT, -1 );
		src->m_isDirty = true;
	}

	// change the dest item's entry
	dst->wrapped_item_id = dst->GetEntry();
	dst->SetUInt32Value( OBJECT_FIELD_ENTRY, itemid );

	// set the giftwrapper fields
	dst->SetUInt32Value( ITEM_FIELD_GIFTCREATOR, _player->GetLowGUID() );
	dst->SetUInt32Value( ITEM_FIELD_DURABILITY, 0 );
	dst->SetUInt32Value( ITEM_FIELD_MAXDURABILITY, 0 );
	dst->SetUInt32Value( ITEM_FIELD_FLAGS, 0x8008 );

	// save it
	dst->m_isDirty = true;
	dst->SaveToDB( MsgRecv.dstbag, MsgRecv.dstslot, false, NULL );
}

void WorldSession::HandleUnbindItem(CPacketUsn& packet)
{
	MSG_C2S::stUnbindItemReq req;
	packet >> req;

	MSG_S2C::stUnbindItemAck ack;
	ack.result = MSG_S2C::stUnbindItemAck::UNBIND_RESULT_UNK;
	Item* source = _player->GetItemInterface()->GetItemByGUID( req.sourceGUID );
	if( source && source->GetProto()->Class == ITEM_CLASS_CONSUMABLE && source->GetProto()->SubClass == ITEM_SUBCLASS_CONSUMABLE_UNBIND )
	{
		Item* target = _player->GetItemInterface()->GetItemByGUID( req.targetGUID );
		if( target )
		{
			if( target->CanUnbind() )
			{
				if( target->GetProto()->RequiredLevel <= source->GetProto()->RequiredLevel )
				{
					if( target->GetProto()->Quality <= source->GetProto()->Quality )
					{
						ack.result = MSG_S2C::stUnbindItemAck::UNBIND_RESULT_SUCCEED;
						target->SoulUnbind();
						
						source->m_isDirty = true;
						target->m_isDirty = true;

						if( source->GetUInt32Value( ITEM_FIELD_STACK_COUNT ) > 1 )
							source->ModUnsigned32Value( ITEM_FIELD_STACK_COUNT, -1 );
						else
							_player->GetItemInterface()->SafeFullRemoveItemByGuid( req.sourceGUID );
					}
					else
						ack.result = MSG_S2C::stUnbindItemAck::UNBIND_RESULT_QUALITY;
				}
				else
					ack.result = MSG_S2C::stUnbindItemAck::UNBIND_RESULT_LEVEL;
			}
			else
			{
				ack.result = MSG_S2C::stUnbindItemAck::UNBIND_RESULT_NEED_NOT;
			}
		}
		else
		{
			ack.result = MSG_S2C::stUnbindItemAck::UNBIND_RESULT_NO_TARGET;
		}
	}

	SendPacket( ack );
}


void WorldSession::HandleAppearItem(CPacketUsn& packet)
{
	MSG_C2S::stAppearItemReq req;
	packet >> req;

	MSG_S2C::stAppearItemAck ack;
	
	Item* source = _player->GetItemInterface()->GetItemByGUID( req.sourceGUID );
	if( source && source->GetProto()->Class == ITEM_CLASS_CONSUMABLE && source->GetProto()->SubClass == ITEM_SUBCLASS_CONSUMABLE_APPEAR )
	{
		PlayerInfo* pi = objmgr.GetPlayerInfoByName( req.targetName.c_str() );
		if( pi )
		{
			if( pi->m_loggedInPlayer && pi->m_loggedInPlayer->IsInWorld() )
			{
				if( pi->m_loggedInPlayer->GetMapMgr()->GetMapInfo()->type == 0 )
				{
					if( !pi->m_loggedInPlayer->IsBeingTeleported() )
					{
						if ( _player->GetMapId() == pi->m_loggedInPlayer->GetMapId() && _player->GetInstanceID() == pi->m_loggedInPlayer->GetInstanceID() )
							_player->SafeTeleport( pi->m_loggedInPlayer->GetMapId(), pi->m_loggedInPlayer->GetInstanceID(), pi->m_loggedInPlayer->GetPosition() );
						else
							_player->SafeTeleport(pi->m_loggedInPlayer->GetMapMgr(), pi->m_loggedInPlayer->GetPosition());

						source->m_isDirty = true;
						if( source->GetUInt32Value( ITEM_FIELD_STACK_COUNT ) > 1 )
							source->ModUnsigned32Value( ITEM_FIELD_STACK_COUNT, -1 );
						else
							_player->GetItemInterface()->SafeFullRemoveItemByGuid( req.sourceGUID );

						ack.result = MSG_S2C::stAppearItemAck::APPEAR_RESULT_SUCCEED;
					}
					else
						ack.result = MSG_S2C::stAppearItemAck::APPEAR_RESULT_WRONG_TELEPORTING;
				}
				else
					ack.result = MSG_S2C::stAppearItemAck::APPEAR_RESULT_INSTANCE;
			}
			else
				ack.result = MSG_S2C::stAppearItemAck::APPEAR_RESULT_OFFLINE;
		}
		else
			ack.result = MSG_S2C::stAppearItemAck::APPEAR_RESULT_WRONG_NAME;

		SendPacket( ack );
	}
}
