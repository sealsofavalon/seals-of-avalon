#ifndef _ITEMINTERFACE_H
#define _ITEMINTERFACE_H
#define INVALID_BACKPACK_SLOT 0xFF //In 1.8 client marked wrong slot like this

struct SlotResult
{
	SlotResult() { ContainerSlot = 0xFF, Slot = 0xFF, Result = false; }
	ui8 ContainerSlot;
	ui8 Slot;
	bool Result;
};

// sanity checking
enum AddItemResult
{
	ADD_ITEM_RESULT_ERROR			= 0,
	ADD_ITEM_RESULT_OK				= 1,
	ADD_ITEM_RESULT_DUPLICATED		= 2,
};

class SERVER_DECL ItemInterface
{
private:
	SlotResult result;
	Player *m_pOwner;
	Item* m_pBuyBack[MAX_BUYBACK_SLOT];

	AddItemResult m_AddItem(Item *item, ui8 ContainerSlot, ui8 slot, bool bQstItemNotify);

public:
	Item* m_pItems[MAX_INVENTORY_SLOT];
	friend class ItemIterator;
	ItemInterface( Player *pPlayer );
	~ItemInterface();

	Player *GetOwner() { return m_pOwner; }
	bool IsBagSlot(ui8 slot);

	uint32 m_CreateForPlayer(ByteBuffer *data);
	void m_DestroyForPlayer();

	void mLoadItemsFromDatabase(QueryResult * result);
	void mSaveItemsToDatabase(bool first, QueryBuffer * buf);

	Item *GetInventoryItem(ui8 slot);
	Item *GetInventoryItem(ui8 ContainerSlot, ui8 slot);
	ui8 GetInventorySlotById(uint32 ID);
	ui8 GetInventorySlotByGuid(uint64 guid);
	ui8 GetBagSlotByGuid(uint64 guid);

	Item *SafeAddItem(uint32 ItemId, ui8 ContainerSlot, ui8 slot, bool bQstItemNotify = true);
	AddItemResult SafeAddItem(Item *pItem, ui8 ContainerSlot, ui8 slot, bool bQstItemNotify = true);
	Item *SafeRemoveAndRetreiveItemFromSlot(ui8 ContainerSlot, ui8 slot, bool destroy); //doesnt destroy item from memory
	Item *SafeRemoveAndRetreiveItemByGuid(uint64 guid, bool destroy);
	bool SafeFullRemoveItemFromSlot(ui8 ContainerSlot, ui8 slot); //destroys item fully
	bool SafeFullRemoveItemByGuid(uint64 guid); //destroys item fully
	AddItemResult AddItemToFreeSlot(Item *item, bool bQstItemNotify = true);
	AddItemResult AddItemToFreeBankSlot(Item *item);
	void ClearInventorySlot( uint32 slot );
	
	Item* FindItemLessMax(uint32 itemid, uint32 cnt, bool IncBank);
	uint32 GetItemCount(uint32 itemid, bool IncBank = false);
	uint32 RemoveItemAmt(uint32 id, uint32 amt);
	uint32 RemoveItemAmt_ProtectPointer(uint32 id, uint32 amt, Item** pointer);
	void RemoveAllConjured();
	void BuyItem(ItemPrototype *item, uint32 total_amount, Creature * pVendor, uint32 extended_cost = 0);

	void Update(uint64 time_now);

	uint32 CalculateFreeSlots(ItemPrototype *proto);
	bool CanStoreItems( std::list<std::pair<uint32, uint32> >& l );
	void ReduceItemDurability();

	ui8 LastSearchItemBagSlot(){return result.ContainerSlot;}
	ui8 LastSearchItemSlot(){return result.Slot;}
	SlotResult *LastSearchResult(){return &result;}

	//Searching functions
	SlotResult FindFreeInventorySlot(ItemPrototype *proto);
	SlotResult FindFreeBankSlot(ItemPrototype *proto);
	ui8 FindFreeBackPackSlot();
	ui8 FindFreeKeyringSlot();
	ui8 FindSpecialBag(Item *item);


	ui8 CanEquipItemInSlot(ui8 DstInvSlot, ui8 slot, ItemPrototype* item, bool ignore_combat = false, bool skip_2h_check = false);
	ui8 CanReceiveItem(ItemPrototype * item, uint32 amount);
	ui8 CanAffordItem(ItemPrototype * item,uint32 amount, Creature * pVendor, uint32 extended_cost = 0);
	ui8 GetItemSlotByType(uint32 type);
	Item* GetItemByGUID(uint64 itemGuid);


	void BuildInventoryChangeError(Item *SrcItem, Item *DstItem, uint8 Error);
	void SwapItemSlots(ui8 srcslot, ui8 dstslot);

	ui8 GetInternalBankSlotFromPlayer(ui8 islot); //converts inventory slots into 0-x numbers
	// Checks if the player has slotted an item with an item ID
	bool HasGemEquipped( uint32 GemID , ui8 IgnoreSlot = -1 ); // (GemID: The item ID of the gem)

	//buyback stuff
	SUNYOU_INLINE Item* GetBuyBack(int32 slot) 
	{ 
		if(slot >= 0 && slot < 12)
			return m_pBuyBack[slot];
		else 
			return NULL;
	}
	void AddBuyBackItem(Item* it, uint32 price);
	void RemoveBuyBackItem(uint32 index);
	void EmptyBuyBack();
	bool IsEquipped(uint32 itemid);

	void CheckAreaItems();
};

class ItemIterator
{
	bool m_atEnd;
	bool m_searchInProgress;
	uint32 m_slot;
	uint32 m_containerSlot;
	Container * m_container;
	Item * m_currentItem;
	ItemInterface* m_target;
public:
	ItemIterator(ItemInterface* target) : m_atEnd(false),m_searchInProgress(false),m_slot(0),m_containerSlot(0),m_container(NULL),m_target(target) {}
	~ItemIterator() { if(m_searchInProgress) { EndSearch(); } }

	void BeginSearch()
	{
		// iteminterface doesn't use mutexes, maybe it should :P
		ASSERT(!m_searchInProgress);
		m_atEnd=false;
		m_searchInProgress=true;
		m_container=NULL;
		m_currentItem=NULL;
		m_slot=0;
		Increment();
	}

	void EndSearch()
	{
		// nothing here either
		ASSERT(m_searchInProgress);
		m_atEnd=true;
		m_searchInProgress=false;
	}

	Item* operator*() const
	{
		return m_currentItem;
	}

	Item* operator->() const
	{
		return m_currentItem;
	}

	void Increment();

	SUNYOU_INLINE Item* Grab() { return m_currentItem; }
	SUNYOU_INLINE bool End() { return m_atEnd; }
};

#endif
