#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S.h"
#include "../../SDBase/Protocol/S2C.h"
#include "Item.h"
#include "ItemInterface.h"
#include "JingLianItem.h"

void WorldSession::HandleRefineContainerMoveItemReq( CPacketUsn& packet )
{
	if( !_player ) return;
	MSG_C2S::stJingLianContainerMoveItemReq req;
	packet >> req;
	if( req.jinglian_container_slot >= MAX_JINGLIAN_SLOT )
		return;
	Item* pSource = _player->GetItemInterface()->GetInventoryItem( req.container_slot, req.slot );
	if( pSource )
	{
		if( req.jinglian_container_slot > 0 )
		{
			if( !FindRefineGem( pSource->GetEntry() ) )
				return;
		}
		else if( !pSource->CanJingLian() )
		{
			MSG_S2C::stJingLianItemAck ack;
			ack.result = JINGLIAN_RESULT_INVALID_ITEM;
			SendPacket( ack );
			return;
		}

	}

	/*
	Item* Equip = m_slots[0];
	EquipRefineRate* pEquipRefineRate = NULL;

	int RefineLevel = Equip->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
	if (Equip)
	{
	ItemPrototype* pPrototype = Equip->GetProto();
	if (pPrototype->Class == ITEM_CLASS_ARMOR)
	{
	pEquipRefineRate = FindEquipRefine(pPrototype->Quality);
	}
	}

	if (!pEquipRefineRate)
	{

	return JINGLIAN_RESULT_FAILED;
	}


	uint32 hasLow=0,hasHigh=0;
	uint32 GemChance = 0;
	for( int i = 2; i < MAX_JINGLIAN_SLOT; ++i )
	{
	if( m_slots[i] )
	{
	jinglian_item_gem* rig = FindRefineGem( m_slots[i]->GetEntry() );
	if( rig )
	{
	if(rig->type == JINGLIAN_GEM_TYPE_CHANCE_LOW)
	{
	GemChance += rig->chance;
	//if(hasHigh)
	//	return JINGLIAN_RESULT_NO_MATCH_GEM;
	hasLow++;
	}
	//else if(rig->type == JINGLIAN_GEM_TYPE_CHANCE_HIGH)
	//{
	//	if(hasLow)
	//		return JINGLIAN_RESULT_NO_MATCH_GEM;
	//	hasHigh++;
	//}
	else if(rig->type == JINGLIAN_GEM_TYPE_PROTECT)
	{
	bProtect = true;
	}
	}
	}
	}
	*/
	bool destroy = false;
	Item* ret = _player->GetJingLianItemInterface()->MoveSlot( req.jinglian_container_slot, pSource, req.to_inventory, destroy );
	if( pSource != ret )
	{
		_player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot( req.container_slot, req.slot, destroy );
		if( ret )
			_player->GetItemInterface()->SafeAddItem( ret, req.container_slot, req.slot );
	}


	EquipRefineRate* pEquipRefineRate = NULL;
	Item* Equip = _player->GetJingLianItemInterface()->GetSlot(0);
	if (Equip)
	{
		int RefineLevel = Equip->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
		if (Equip)
		{
			ItemPrototype* pPrototype = Equip->GetProto();
			if (pPrototype->Class == ITEM_CLASS_ARMOR)
			{
				pEquipRefineRate = FindEquipRefine(pPrototype->Quality);
			}
		}

	}


	int RefineLevel = Equip->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
}

void WorldSession::HandleRefineItemReq( CPacketUsn& packet )
{
	if( !_player ) return;
	_player->JingLianItem();
}

void WorldSession::HandleXiangqianContainerMoveItemReq( CPacketUsn& packet )
{
}

void WorldSession::HandleXiangqianItemReq( CPacketUsn& packet )
{
}
