#include "StdAfx.h"
#include "JingLianItem.h"
#include "Player.h"
#include "Item.h"
#include "ItemInterface.h"
#include "../../SDBase/Public/UpdateFields.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "MultiLanguageStringMgr.h"

JingLianItemInterface::JingLianItemInterface()
{
	max_slot = MAX_JINGLIAN_SLOT;
	player_field_begin = PLAYER_FIELD_JINGLIAN_SLOT_ITEM;
	inventory_slot_begin = INVENTORY_SLOT_JINGLIAN_BEGIN;
	inventory_slot_save = INVENTORY_SLOT_JINGLIAN;
}

JingLianItemInterface::~JingLianItemInterface()
{
}

jinglian_result_t JingLianItemInterface::JingLian( bool check /* = false */ )
{
	if( !m_slots[0] ) return JINGLIAN_RESULT_INVALID_ITEM;

	if( !m_slots[0]->CanJingLian() ) return JINGLIAN_RESULT_INVALID_ITEM;

	uint32 lv = m_slots[0]->GetUInt32Value( ITEM_FIELD_JINGLIAN_LEVEL );

	if( lv >= 9 ) return JINGLIAN_RESULT_ALREADY_MAX_LEVEL;

	//精炼花费的金钱：白装每次精炼200，绿装300，蓝装500，紫装700，橙装1000
	uint32 cost = 0;
	static const float default_chance_modifier[6] = { 0.1f, 0.06f, 0.05f, 0.04f, 0.03f, 0.02f };
	switch(m_slots[0]->GetProto()->Quality)
	{
	case ITEM_QUALITY_NORMAL_WHITE:cost = 200;break;
	case ITEM_QUALITY_UNCOMMON_GREEN:cost = 300;break;
	case ITEM_QUALITY_RARE_BLUE:cost = 500;break;
	case ITEM_QUALITY_EPIC_PURPLE:cost = 700;break;
	case ITEM_QUALITY_LEGENDARY_ORANGE:cost = 1000;break;
	default:cost = 100;break;
	}
	bool force_success = false;
	if( m_slots[0]->GetProto()->Class == ITEM_CLASS_ARMOR && m_slots[0]->GetProto()->SubClass >= ITEM_SUBCLASS_ARMOR_GUAIWUBEIBU )
		force_success = true;

	if( m_owner->GetUInt32Value( PLAYER_FIELD_COINAGE ) < cost )
		return JINGLIAN_RESULT_NOT_ENOUGH_GOLD;
	
//	int matchslot = 0;
	int matchlv = 0;
	int matchchance = 0;
	int chanceslot = 0;
	float chance_bonus = 0;
	bool bProtect = false;

	if(!m_slots[1])
	{//没有精炼宝石
		return JINGLIAN_RESULT_NO_MATCH_GEM;
	}

	if( check ) return JINGLIAN_RESULT_CHECK_SUCCESS;
 	else
 	{
 		jinglian_item_gem* rig = FindRefineGem( m_slots[1]->GetEntry() );
 		if( rig )
 		{
			if(rig->type != JINGLIAN_GEM_TYPE_MAIN)
				return JINGLIAN_RESULT_NO_MATCH_GEM;
		}
	}
// 			switch( rig->type )
// 			{
// 			case JINGLIAN_GEM_TYPE_UPGRADE_GREEN:
// 				{
// 					if(lv >= 1 && lv <= 3);
// 					else
// 						return JINGLIAN_RESULT_NO_MATCH_GEM;
// 				}break;
// 			case JINGLIAN_GEM_TYPE_UPGRADE_BLUE:
// 				{
// 					if(lv >= 1 && lv <= 6);
// 					else
// 						return JINGLIAN_RESULT_NO_MATCH_GEM;
// 					break;
// 				}
// 			case JINGLIAN_GEM_TYPE_UPGRADE_ZI:
// 				{
// 					if(lv >= 1 && lv <= 9);
// 					else
// 						return JINGLIAN_RESULT_NO_MATCH_GEM;
// 					break;
// 				}
// 			default:
// 				return JINGLIAN_RESULT_NO_MATCH_GEM;
// 				break;
// 			}
// 		}
// 	}
	Item* Equip = m_slots[0];
	EquipRefineRate* pEquipRefineRate = NULL;

	int RefineLevel = Equip->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
	if (Equip)
	{
		ItemPrototype* pPrototype = Equip->GetProto();
		if (pPrototype->Class == ITEM_CLASS_ARMOR)
		{
			pEquipRefineRate = FindEquipRefine(pPrototype->Quality);
		}
	}

	if (!pEquipRefineRate)
	{

		return JINGLIAN_RESULT_FAILED;
	}


	uint32 hasLow=0,hasHigh=0;
	uint32 GemChance = 0;
	for( int i = 2; i < MAX_JINGLIAN_SLOT; ++i )
	{
		if( m_slots[i] )
		{
			jinglian_item_gem* rig = FindRefineGem( m_slots[i]->GetEntry() );
			if( rig )
			{
				if(rig->type == JINGLIAN_GEM_TYPE_CHANCE_LOW)
				{
					GemChance += rig->chance;
					//if(hasHigh)
					//	return JINGLIAN_RESULT_NO_MATCH_GEM;
					hasLow++;
				}
				//else if(rig->type == JINGLIAN_GEM_TYPE_CHANCE_HIGH)
				//{
				//	if(hasLow)
				//		return JINGLIAN_RESULT_NO_MATCH_GEM;
				//	hasHigh++;
				//}
				else if(rig->type == JINGLIAN_GEM_TYPE_PROTECT)
				{
					bProtect = true;
				}
			}
		}
	}

	m_owner->ModCoin(-(int)cost);
	MyLog::yunyinglog->info("gold-refine-player[%u][%s] take gold[%u]", m_owner->GetLowGUID(), m_owner->GetName(), cost);

	for( int i = 1; i < MAX_JINGLIAN_SLOT; ++i )
	{
		if( m_slots[i] )
		{

			uint32 count = m_slots[i]->GetUInt32Value( ITEM_FIELD_STACK_COUNT );
			MyLog::yunyinglog->info("item-refine-player[%u][%s] removeitem["I64FMT"][%u] count[%u]", m_owner->GetLowGUID(), m_owner->GetName(), m_slots[i]->GetGUID(), m_slots[i]->GetProto()->ItemId, count);
			if( count == 1 )
			{
				m_slots[i]->DeleteFromDB();
				m_slots[i]->Delete();
				m_slots[i] = NULL;
			}
			else
			{
				m_slots[i]->SetCount( count - 1 );
				m_slots[i]->m_isDirty = true;
			}
			RefreshSlot( i );
		}
	}
/*
	//计算概率
	//基本概率表,行为级别,列为品质
	const float main_chance[10][6] = 
	{
		.0f,.99f,.99f,.84f,.63f,.41f,
		.0f,.99f,.84f,.63f,.41f,.23f,
		.0f,.89f,.72f,.47f,.27f,.12f,
		.0f,.80f,.61f,.35f,.17f,.07f,
		.0f,.72f,.52f,.27f,.12f,.04f,
		.0f,.65f,.44f,.20f,.11f,.02f,
		.0f,.58f,.37f,.15f,.05f,.01f,
		.0f,.53f,.32f,.11f,.03f,.01f,
		.0f,.47f,.27f,.08f,.02f,.0f,
		.0f,.43f,.23f,.06f,.01f,.0f,
	};

	//加成
	//行为个数,列为品质
	const float gem_bonus_chance[5][2] = 
	{
		.0f, .0f,
		100.f, 250.f,
		110.f, 275.f,
		166.f, 338.f,
		180.f, 375.f,
	};
	*/

	//（精炼基础成功率+(默认宝石加成率+（默认宝石加成率*加成））*（1+（1-精炼级别）%）
	//chance_bonus = main_chance[lv][m_slots[0]->GetProto()->Quality];
	/*float default_chance = hasLow ? 0.02f : 0.05f;
	if( hasLow || hasHigh )
		chance_bonus += ( default_chance + (default_chance*gem_bonus_chance[hasLow?hasLow:hasHigh][hasLow?0:1]/100.f) ) * (1.f+(1.f-lv)/100.f);

	chance_bonus += default_chance_modifier[m_slots[0]->GetProto()->Quality];*/

//	精炼成功率=精炼基础调整系数/（装备当前精炼等级+1）+宝石加成和*宝石调整系数/（装备当前精炼等级+1）
	chance_bonus = pEquipRefineRate->RefineRate / (RefineLevel + 1) + GemChance / (RefineLevel + 1);
	chance_bonus += m_owner->PctRefineChanceModifier;

	if( !force_success && RandomFloat( 100 ) > chance_bonus*100.f )
	{
		uint32 lv_down = rand()%3+1;
		if(lv >= 1)
		{
			if(bProtect)
			{
				//lv--;
			}
			else
			{
				if( lv >= 1 && lv <= 4)
				{
					lv--;
				}
				else
				{
					if(lv > lv_down)
						lv -= lv_down;
					else
						lv = 1;
				}
			}

		}		

		/*
		if(lv < 4)
			m_slots[0]->SetUInt32Value(ITEM_FIELD_HOLE_CNT, 0);
		else if(lv < 7)
			m_slots[0]->SetUInt32Value(ITEM_FIELD_HOLE_CNT, 1);
		else if(lv < 9)
			m_slots[0]->SetUInt32Value(ITEM_FIELD_HOLE_CNT, 2);
		else
			m_slots[0]->SetUInt32Value(ITEM_FIELD_HOLE_CNT, 3);
		*/

		m_slots[0]->SetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL, lv);
		m_slots[0]->SetRefineEffect(lv);
		return JINGLIAN_RESULT_FAILED;
	}

	m_slots[0]->SetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL, ++lv);
	m_slots[0]->SetRefineEffect(lv);

	/*
	if(lv < 4)
		m_slots[0]->SetUInt32Value(ITEM_FIELD_HOLE_CNT, 0);
	else if(lv < 7)
		m_slots[0]->SetUInt32Value(ITEM_FIELD_HOLE_CNT, 1);
	else if(lv < 9)
		m_slots[0]->SetUInt32Value(ITEM_FIELD_HOLE_CNT, 2);
	else
		m_slots[0]->SetUInt32Value(ITEM_FIELD_HOLE_CNT, 3);
	*/

	m_slots[0]->m_isDirty = true;

	bool bBroadCast = true;
	char szTxt[128] = {0};

	if( lv >= 7
		//&& ( ( m_slots[0]->GetProto()->Class == ITEM_CLASS_ARMOR && m_slots[0]->GetProto()->SubClass <= ITEM_SUBCLASS_ARMOR_BOOTS )
		//|| m_slots[0]->GetProto()->Class != ITEM_CLASS_ARMOR ) )
		)
	{
		MSG_S2C::stChat_Message Msg;
		sChatHandler.FillMessageData( CHAT_MSG_SYSTEM_BROADCAST, build_language_string( BuildString_Level9Broadcast, m_owner->GetName(), m_slots[0]->BuildString(), quick_itoa(lv) ), 0, &Msg, 0 );
		sWorld.SendGlobalMessage(Msg);
	}

	return JINGLIAN_RESULT_SUCCESS;
}
