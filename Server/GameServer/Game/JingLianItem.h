#ifndef _JINGLIAN_ITEM_HEAD
#define _JINGLIAN_ITEM_HEAD

#include "MakeItemBase.h"

class JingLianItemInterface : public MakeItemBase
{
public:
	JingLianItemInterface();
	~JingLianItemInterface();

	//virtual void RefreshCost();

	jinglian_result_t JingLian( bool check = false );
};

#endif