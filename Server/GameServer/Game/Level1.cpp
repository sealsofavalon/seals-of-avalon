/////////////////////////////////////////////////
//  GM Chat Commands
//

#include "StdAfx.h"
#include "../../../SDBase/Protocol/S2C_Move.h"
#include "../../Common/Protocol/GS2GT.h"

bool ChatHandler::HandleAnnounceCommand(const char* args, WorldSession *m_session)
{
	if( !*args || strlen(args) < 4 || strchr(args, '%'))
	{
		m_session->SystemMessage("syntax error.");
		return true;
	}

	char msg[1024];
	string input2;
// 	input2 = sWorld.ann_tagcolor;
// 	input2 += "[";
// 	input2 += sWorld.announce_tag + "]";
// 	if(sWorld.GMAdminTag)
// 	{
// 		input2 += sWorld.ann_gmtagcolor;
// 		if(m_session->CanUseCommand('z')) input2+="<Admin>";
// 		else if(m_session->GetPermissionCount()) input2+="<GM>";
// 	}

	if(sWorld.NameinAnnounce)
	{
// 		input2+=sWorld.ann_namecolor;
// 		input2+="|Hplayer:";
		input2+=m_session->GetPlayer()->GetName();
// 		input2+="|h[";
// 		input2+=m_session->GetPlayer()->GetName();
// 		input2+="]|h";
	}
	input2+=": ";
	snprintf((char*)msg, 1024, "%s%s", input2.c_str(), args);
	msg[1023] = '0';

	sWorld.SendWorldText(msg); // send message
	MyLog::yunyinglog->notice("gm used announce command, [%s]", args);
	return true;
}

bool ChatHandler::HandleWAnnounceCommand(const char* args, WorldSession *m_session)
{
	return true ;


	if(!*args)
		return false;

	char pAnnounce[1024];
	string input3;
// 	input3 = sWorld.ann_tagcolor;
// 	input3 +="[";
// 	input3 += sWorld.announce_tag;
// 	input3 += "]";
// 	if(sWorld.GMAdminTag)
// 	{
// 		input3 += sWorld.ann_gmtagcolor;
// 		if(m_session->CanUseCommand('z')) input3+="<Admin>";
// 		else if(m_session->GetPermissionCount()) input3+="<GM>";
// 	}
	if(sWorld.NameinWAnnounce)
	{
	input3+=sWorld.ann_namecolor;
	input3+="[";
	input3+=m_session->GetPlayer()->GetName();
	input3+="]";
	}
	input3+=": ";
	input3+=sWorld.ann_msgcolor;
	snprintf((char*)pAnnounce, 1024, "%s%s", input3.c_str(), args);

	sWorld.SendWorldWideScreenText(pAnnounce); // send message
	MyLog::gmlog->notice("used wannounce command [%s]", args);
	return true;
}



bool ChatHandler::HandleSystemNotifyRemoveCommand(const char* args, WorldSession *m_session)
{
	if(strlen(args) < 1)
	{
		SendMultilineMessage(m_session, "please input argument : id");
		return false;
	}

	uint32 id;
	if(sscanf(args, "%u", &id) < 1)
	{
		SendMultilineMessage(m_session, "please input argument : id");
		return false;
	}

	WorldDatabase.Execute("DELETE FROM SystemNotify where id = %u", id);
	SystemNotifyStorage.Reload();
	MSG_S2C::stSystemNotifyRemove Msg;
	Msg.id = id;
	sWorld.SendGlobalMessage(Msg);
	return true;
}
bool ChatHandler::HandleGMOnCommand(const char* args, WorldSession *m_session)
{
	return true ;
	/*uint32 newbytes = m_session->GetPlayer( )->GetUInt32Value(PLAYER_BYTES_2) | 0x8;
	m_session->GetPlayer( )->SetUInt32Value( PLAYER_BYTES_2, newbytes);

	GreenSystemMessage(m_session, "GM Flag Set.");*/
	GreenSystemMessage(m_session, "Setting GM Flag on yourself...");
	if(m_session->GetPlayer()->bGMTagOn)
		RedSystemMessage(m_session, "GM Flag is already set on. Use !gmoff to disable it.");
	else
	{
		m_session->GetPlayer()->bGMTagOn = true;
		m_session->GetPlayer()->SetFlag(PLAYER_FLAGS, PLAYER_FLAG_GM);	// <GM>
		BlueSystemMessage(m_session, "GM Flag Set. It will appear above your name and in chat messages until you use !gmoff.");
	}

	return true;
}


bool ChatHandler::HandleGMOffCommand(const char* args, WorldSession *m_session)
{
	return true ;
	//uint32 newbytes = m_session->GetPlayer( )->GetUInt32Value(PLAYER_BYTES_2) & ~(0x8);
	//m_session->GetPlayer( )->SetUInt32Value( PLAYER_BYTES_2, newbytes);

	//GreenSystemMessage(m_session, "GM Flag Unset.");
	if(sWorld.m_forceGMTag && !m_session->CanUseCommand('1'))
	{
		GreenSystemMessage(m_session, "You are forced to use the GM tag!");
		return false;
	}
	else
	{
	GreenSystemMessage(m_session, "Unsetting GM Flag on yourself...");
	if(!m_session->GetPlayer()->bGMTagOn)
		RedSystemMessage(m_session, "GM Flag not set. Use !gmon to enable it.");
	else
	{
		m_session->GetPlayer()->bGMTagOn = false;
		m_session->GetPlayer()->RemoveFlag(PLAYER_FLAGS, PLAYER_FLAG_GM);	// <GM>
		BlueSystemMessage(m_session, "GM Flag Removed. <GM> Will no longer show in chat messages or above your name.");
	}
	return true;
	}
}


bool ChatHandler::HandleGPSCommand(const char* args, WorldSession *m_session)
{
	return true ;


	Object *obj;

	uint64 guid = m_session->GetPlayer()->GetSelection();
	if (guid != 0)
	{
		if(!(obj = m_session->GetPlayer()->GetMapMgr()->GetUnit(guid)))
		{
			SystemMessage(m_session, "You should select a character or a creature.");
			return true;
		}
	}
	else
		obj = (Object*)m_session->GetPlayer();

	AreaTable * at = AreaStorage.LookupEntry(obj->GetMapMgr()->GetAreaID(obj->GetPositionX(), obj->GetPositionY()));
	if(!at) return true;

	char buf[256];
	snprintf((char*)buf, 256, "<color:ff00ff00>Current Position: <color:ffffffff>Map: <color:ff00ff00>%d <color:ffffffff>Zone: <color:ff00ff00>%u <color:ffffffff>X: <color:ff00ff00>%f <color:ffffffff>Y: <color:ff00ff00>%f <color:ffffffff>Z: <color:ff00ff00>%f <color:ffffffff>Orientation: <color:ff00ff00>%f<br>",
		(unsigned int)obj->GetMapId(), at->ZoneId, obj->GetPositionX(), obj->GetPositionY(), obj->GetPositionZ(), obj->GetOrientation());
	
	
	SystemMessage(m_session, buf);

	return true;
}


bool ChatHandler::HandleKickCommand(const char* args, WorldSession *m_session)
{

	if(!*args)
	{
		SystemMessage(m_session, ".kick user reason");
		return false;
	}

	bool SilentKick = false;

	char *pname = strtok((char*)args, " ");

	if(!pname)
	{
		RedSystemMessage(m_session, ".kick user reason");
		return true;
	}
	Player *chr = objmgr.GetPlayer((const char*)pname, false);
	if (chr)
	{
		char *reason = strtok(NULL, " ");

		std::string kickreason = "No reason";
		if(reason)
			kickreason = reason;
		char* pSilentKick = strtok(NULL, "\n");
		
		if (pSilentKick)
			SilentKick = (atoi(pSilentKick)>0?true:false);

		BlueSystemMessage(m_session, "Attempting to kick %s from the server for \"%s\".", chr->GetName(), kickreason.c_str());
		MyLog::gmlog->notice("Kicked player %s from the server for %s", chr->GetName(), kickreason.c_str());
		if(!m_session->CanUseCommand('1') && chr->GetSession()->CanUseCommand('1'))
		{
			RedSystemMessage(m_session, "You cannot kick %s, as they are a higher level gm than you.", chr->GetName());
			return true;
		}
		/*if(m_session->GetSecurity() < chr->GetSession()->GetSecurity())
		{
			SystemMessage(m_session, "You cannot kick %s, as he is a higher GM level than you.", chr->GetName());
			return true;
		}*/ // we might have to re-work this

		if(SilentKick == false && false)
		{
			char msg[200];
			snprintf(msg, 200, "%sGM: %s was kicked from the server by %s. Reason: %s", MSG_COLOR_RED, chr->GetName(), m_session->GetPlayer()->GetName(), kickreason.c_str());
			sWorld.SendWorldText(msg, NULL);
			//sWorld.SendIRCMessage(msg);
		}
		
		SystemMessageToPlr(chr, "You are being kicked from the server by %s. Reason: %s", m_session->GetPlayer()->GetName(), kickreason.c_str());
		
		MSG_S2C::stMove_Force_Root Msg;
		Msg.guid = chr->GetNewGUID();
		Msg.flag = 1;
		chr->SendMessageToSet(Msg, true);
		
		chr->Kick(6000);
		
		return true;
	} 
	else 
	{
		RedSystemMessage(m_session, "Player is not online at the moment.");
		return true;
	}
	return true;
}

bool ChatHandler::HandleAddFriendCommand(const char* args, WorldSession *m_session)
{
	if(!*args)
		return false;

	char *fname = strtok((char*)args, " ");
	char *note = "";
	if(!fname)
	{
		RedSystemMessage(m_session, "No friend to add specified.");
		return false;
	}

	Player* target = this->getSelectedChar(m_session, true);
	if(!target) 
		return false;
	target->Social_AddFriend(fname, note);
	return true;
}


bool ChatHandler::HandleAddTitleCommand(const char *args, WorldSession *m_session)
{
	uint32 id=0;

	if(strlen(args) < 1)
	{
		return false;
	}

	if(sscanf(args, "%u", &id) < 1)
		return false;

	Player *chr = NULL;
	//if(strcmp(m_session->GetPermissions(), "4") != 0)
	chr = getSelectedChar(m_session);
	if (chr == NULL) return true;
	else
		chr = m_session->GetPlayer();
	
	chr->InsertTitle(id);
	MSG_S2C::stTitleAdd msg;
	msg.title.title = id;
	chr->GetSession()->SendPacket(msg);

	return true;
}

bool ChatHandler::HandleAddItemWithTimeCommand(const char *args, WorldSession *m_session)
{
	return true ;
	uint32 itemid, t=1,randomprop,count=1;

	if(strlen(args) < 1)
	{
		return false;
	}

	if(sscanf(args, "%u %u", &itemid, &t, &count) < 2)
		return false;

	Player *chr = NULL;
	//if(strcmp(m_session->GetPermissions(), "4") != 0)
	{
		chr = getSelectedChar(m_session);
		if (chr == NULL) return true;
	}
// 	else
// 		chr = m_session->GetPlayer();

	ItemPrototype* it = ItemPrototypeStorage.LookupEntry(itemid);
	if(it)
	{
		MyLog::gmlog->notice("used add item command, item id %u to %s", it->ItemId, chr->GetName());

		ui32 nAddCount = count;
		while(nAddCount>0)
		{
			ui32 nAdded = ((nAddCount > it->MaxCount) ? it->MaxCount : nAddCount);
			if( !nAdded )
			{
				return false;
			}
			nAddCount-=nAdded;
			Item *item;
			item = objmgr.CreateItem( itemid, chr);
			item->SetUInt32Value(ITEM_FIELD_STACK_COUNT, nAdded);
			item->SetUInt32Value(ITEM_FIELD_LIFE_STYLE, ITEM_LIFE_STYLE_TIME);
			item->SetUInt64Value(ITEM_FIELD_LIFE_VALUE1, UNIXTIME+t);
			if(it->Bonding==ITEM_BIND_ON_PICKUP)
				item->SoulBind();

			if(randomprop!=0)
			{
				if(randomprop<0)
					item->SetRandomSuffix(abs(int(randomprop)));
				else
					item->SetRandomProperty(randomprop);

				item->ApplyRandomProperties(false);
			}


			item->WashEnchantment();

			if(!chr->GetItemInterface()->AddItemToFreeSlot(item))
			{
				m_session->SendNotification("No free slots were found in your inventory!");
				delete item;
				return true;
			}

			char messagetext[128];
			snprintf(messagetext, 128, "Adding item %d (%s) to %s's inventory.",(unsigned int)it->ItemId,Item::BuildStringWithProto(it), chr->GetName());
			SystemMessage(m_session, messagetext);
			snprintf(messagetext, 128, "%s added item %d (%s) to your inventory.", m_session->GetPlayer()->GetName(), (unsigned int)itemid, Item::BuildStringWithProto(it));
			SystemMessageToPlr(chr,  messagetext);

			MyLog::yunyinglog->info("item-gm give-player[%u][%s] buy["I64FMT"][%u] count[%u] by gm[%u][%s]", chr->GetLowGUID(), chr->GetName(), item->GetGUID(), it->ItemId, count, m_session->GetPlayer()->GetLowGUID(), m_session->GetPlayer()->GetName());

			SlotResult *lr = chr->GetItemInterface()->LastSearchResult();
			chr->GetSession()->SendItemPushResult(item,false,true,false,true,lr->ContainerSlot,lr->Slot,nAdded);

		}		

		return true;
	} else {
		RedSystemMessage(m_session, "Item %d is not a valid item!",itemid);
		return true;
	}
}
bool ChatHandler::HandleAddMapItemCommand(const char *args, WorldSession *m_session)
{
	MyLog::log->error( "WORLD: Received AddMapItemCommand" );
	uint32 itemid, count=1;
	int32 randomprop=0;
	float x=-1.0f,y,z;

	if(strlen(args) < 1)
	{
		return false;
	}

	if(sscanf(args, "%u %u %d %f %f %f", &itemid, &count, &randomprop, &x,&y,&z) < 1)
		return false;

	Player *chr = getSelectedChar(m_session);
	if (chr == NULL) return true;

	ItemPrototype* it = ItemPrototypeStorage.LookupEntry(itemid);
	if(it)
	{
		MyLog::gmlog->notice("used add map item command, item id %u [%s] to %s", it->ItemId, Item::BuildStringWithProto(it), chr->GetName());

		ui32 nAddCount = count;
		while(nAddCount>0)
		{
			ui32 nAdded = ((count > it->MaxCount) ? it->MaxCount : count);
			nAddCount-=nAdded;
			Item *item;
			item = objmgr.CreateItem( itemid, NULL);
			item->SetUInt32Value(ITEM_FIELD_STACK_COUNT, nAdded);
			if(it->Bonding==ITEM_BIND_ON_PICKUP)
				item->SoulBind();

			if(randomprop!=0)
			{
				if(randomprop<0)
					item->SetRandomSuffix(abs(int(randomprop)));
				else
					item->SetRandomProperty(randomprop);

				item->ApplyRandomProperties(false);
			}

			item->WashEnchantment();

			if( x < 0.0f )
				item->SetPosition(chr->GetPosition());
			else
				item->SetPosition(x,y,z, 0);
			item->PushToWorld(chr->GetMapMgr());
			
			char messagetext[128];
			snprintf(messagetext, 128, "Adding item %d (%s) to %s's map.",(unsigned int)it->ItemId, Item::BuildStringWithProto(it), chr->GetName());
			SystemMessage(m_session, messagetext);
			snprintf(messagetext, 128, "%s added item %d (%s) to your map.", m_session->GetPlayer()->GetName(), (unsigned int)itemid, Item::BuildStringWithProto(it));
			SystemMessageToPlr(chr,  messagetext);

		}		

		return true;
	} else {
		RedSystemMessage(m_session, "Item %d is not a valid item!",itemid);
		return true;
	}
}

bool ChatHandler::HandleAccountBannedCommand(const char * args, WorldSession * m_session)
{
	if(!*args) return false;

	/*char account[100];
	uint32 banned;
	int argc = sscanf(args, "%s %u", account, (unsigned int*)&banned);
	if(argc != 2)
	return false;*/

	ui32 acct;char duration[100] = {0};
	int argc = sscanf(args, "%d %s", &acct, duration);
	if(argc != 2)
		return false;
	if( duration[0] == '\0' )
		return false;

	int32 timeperiod = GetTimePeriodFromString(duration);
	if( timeperiod <= 0 )
	{
		timeperiod = 3600;
	}

	uint32 banned = (uint32)UNIXTIME+timeperiod;

	/*stringstream my_sql;
	my_sql << "UPDATE accounts SET banned = " << banned << " WHERE login = '" << CharacterDatabase.EscapeString(string(pAccount)) << "'";

	sLogonCommHandler.LogonDatabaseSQLExecute(my_sql.str().c_str());
	sLogonCommHandler.LogonDatabaseReloadAccounts();
	sLogonCommHandler.Account_SetBanned(pAccount, banned);*/

	GreenSystemMessage(m_session, "account '%d' banned until:%s.", acct, ConvertTimeStampToDataTime(banned).c_str());

	//MyLog::yunyinglog->log("gm[%s] baned account[%d][%s] until[%s]", _player->GetName(), pAccount)

	MyLog::yunyinglog->notice("gm banned account %d until %s", acct, ConvertTimeStampToDataTime(banned).c_str());
	MSG_GS2GT::stBan Msg;
	Msg.acct = acct;
	Msg.ban = banned;
	m_session->_GTSocket->PostSend(Msg);
	sWorld.DisconnectUsersWithAccountId(acct, m_session);
	return true;
}

bool ChatHandler::HandleTaxiCheatCommand(const char* args, WorldSession *m_session)
{
	return true ;
	if (!*args)
		return false;

	int flag = atoi((char*)args);

	Player *chr = getSelectedChar(m_session);
	if (chr == NULL) return true;
	
	char buf[256];

	// send message to user
	if (flag != 0)
	{
		snprintf((char*)buf,256, "%s has all taxi nodes now.", chr->GetName());
	}
	else
	{
		snprintf((char*)buf,256, "%s has no more taxi nodes now.", chr->GetName());
	}
	GreenSystemMessage(m_session, buf);
	
	// send message to player
	if (flag != 0)
	{
		snprintf((char*)buf,256, "%s has given you all taxi nodes.",
			m_session->GetPlayer()->GetName());
	}
	else
	{
		snprintf((char*)buf,256, "%s has deleted all your taxi nodes.",
			m_session->GetPlayer()->GetName());
	}
	SystemMessage(m_session, buf);

	for (uint8 i=0; i<8; i++)
	{
		if (flag != 0)
		{
			m_session->GetPlayer()->SetTaximask(i, 0xFFFFFFFF);
		}
		else
		{
			m_session->GetPlayer()->SetTaximask(i, 0);
		}
	}

	return true;
}
bool ChatHandler::HandleCharMuteCommand(const char * args, WorldSession * m_session)
{
	if(!*args) return false;

	char * pChar = (char*)args;
	// 	char * pDuration = strchr(pChar, ' ');
	// 	if(pDuration == NULL)
	// 		return false;
	// 	*pDuration = 0;
	// 	++pDuration;
	// 
	// 	int32 timeperiod = GetTimePeriodFromString(pDuration);
	// 	if(timeperiod <= 0)
	// 		return false;
	// 
	uint32 banned = (uint32)UNIXTIME+3600*24;

	//sLogonCommHandler.Account_SetMute( pAccount, banned );

	Player * plr = objmgr.GetPlayer(pChar);
	if(!plr)
	{
		GreenSystemMessage(m_session, "[%s] is offline", pChar);
		return false;
	}

	//string tsstr = ConvertTimeStampToDataTime(banned);
	GreenSystemMessage(m_session, "user [%s] muted", pChar);

	MyLog::gmlog->notice("user [%s] muted by [%s]", pChar, m_session->GetPlayer()->GetName());

	WorldSession * pSession = plr->GetSession();
	if( pSession != NULL )
	{
		pSession->m_muted = banned;
		pSession->SystemMessage("you were muted by a game master"/*, tsstr.c_str()*/);
	}

	return true;
}


bool ChatHandler::HandleLearnSkillCommand(const char *args, WorldSession *m_session)
{
	return true ;
	uint32 skill, min, max;
	min = max = 1;
	char *pSkill = strtok((char*)args, " ");
	if(!pSkill)
		return false;
	else
		skill = atol(pSkill);

	BlueSystemMessage(m_session, "Adding skill line %d", skill);

	char *pMin = strtok(NULL, " ");
	if(pMin)
	{
		min = atol(pMin);
		char *pMax = strtok(NULL, "\n");
		if(pMax)
			max = atol(pMax);
	} else {
		return false;
	}

	Player *plr = getSelectedChar(m_session, true);
	if(!plr) return false;
	if(plr->GetTypeId() != TYPEID_PLAYER) return false;
	MyLog::gmlog->notice("used add skill of %u %u %u on %s", skill, min, max, plr->GetName());

	plr->_AddSkillLine(skill, min, max);   

	return true;
}
bool ChatHandler::HandleAccountUnmuteCommand(const char * args, WorldSession * m_session)
{
	//sLogonCommHandler.Account_SetMute( args, 0 );

	char account[100];
	ui32 acct;
	int argc = sscanf(args, "%d", &acct);
	if(argc != 1)
		return false;

	MSG_GS2GT::stMute Msg;
	Msg.acct = acct;
	Msg.mute = 0;
	m_session->_GTSocket->PostSend(Msg);

	GreenSystemMessage(m_session, "account [%d] mute released.", acct);
	WorldSession * pSession = sWorld.FindSession(acct);
	if( pSession != NULL )
	{
		pSession->m_muted = 0;
		pSession->SystemMessage("you can chat now.");
	}
	MyLog::yunyinglog->notice("gm unmute account %d", acct );

	return true;
}



bool ChatHandler::HandleModifySkillCommand(const char *args, WorldSession *m_session)
{
	return true ;
	uint32 skill, min, max;
	min = max = 1;
	char *pSkill = strtok((char*)args, " ");
	if(!pSkill)
		return false;
	else
		skill = atol(pSkill);
	
	char *pMin = strtok(NULL, " ");
	uint32 cnt = 0;
	if(!pMin)
		cnt = 1;
	else
		cnt = atol(pMin);

	skill = atol(pSkill);
	
	BlueSystemMessage(m_session, "Modifying skill line %d. Advancing %d times.", skill, cnt);

	Player *plr = getSelectedChar(m_session, true);
	if(!plr) plr = m_session->GetPlayer();
	if(!plr) return false;
	MyLog::gmlog->notice("used modify skill of %u %u on %s", skill, cnt,plr->GetName());

	if(!plr->_HasSkillLine(skill))
	{
		SystemMessage(m_session, "Does not have skill line, adding.");
		plr->_AddSkillLine(skill, 1, 300);   
	} else {
		plr->_AdvanceSkillLine(skill,cnt);
	}	   

	return true;
}

/// DGM: Get skill level command for getting information about a skill
bool ChatHandler::HandleGetSkillLevelCommand(const char *args, WorldSession *m_session)
{
	return true ;
	uint32 skill = 0;
	char *pSkill = strtok((char*)args, " ");
	if(!pSkill)
		return false;
	else 
		skill = atol(pSkill);

	Player *plr = getSelectedChar(m_session, true);
	if(!plr) return false;

	if(skill > SkillNameManager->maxskill)
	{
		BlueSystemMessage(m_session, "Skill: %u does not exists", skill);
		return false;
	}

    char * SkillName = SkillNameManager->SkillNames[skill];

    if (SkillName==0)
    {
        BlueSystemMessage(m_session, "Skill: %u does not exists", skill);
        return false;
    }
    
    if (!plr->_HasSkillLine(skill))
    {
        BlueSystemMessage(m_session, "Player does not have %s skill.", SkillName);
        return false;
    }

	uint32 nobonus = plr->_GetSkillLineCurrent(skill,false);
	uint32 bonus = plr->_GetSkillLineCurrent(skill,true) - nobonus;
    uint32 max = plr->_GetSkillLineMax(skill);

    BlueSystemMessage(m_session, "Player's %s skill has level: %u maxlevel: %u. (+ %u bonus)", SkillName,max,nobonus, bonus);
	return true;
}

bool ChatHandler::HandleGetSkillsInfoCommand(const char *args, WorldSession *m_session)
{
	return true ;
    Player *plr = getSelectedChar(m_session, true);
    if(!plr) return false;
    
    uint32 nobonus = 0;
    int32  bonus = 0;
    uint32 max = 0;

    BlueSystemMessage(m_session, "Player: %s has skills", plr->GetName() );

    for (uint32 SkillId = 0; SkillId <= SkillNameManager->maxskill; SkillId++)
    {
        if (plr->_HasSkillLine(SkillId))
        {
            char * SkillName = SkillNameManager->SkillNames[SkillId];
            if (!SkillName)
            {
                RedSystemMessage(m_session, "Invalid skill: %u", SkillId);
                continue;
            }

            nobonus = plr->_GetSkillLineCurrent(SkillId,false);
            bonus = plr->_GetSkillLineCurrent(SkillId,true) - nobonus;
            max = plr->_GetSkillLineMax(SkillId);

            BlueSystemMessage(m_session, "  %s: Value: %u, MaxValue: %u. (+ %d bonus)", SkillName, nobonus,max, bonus);
        }
    }

    return true;
}


bool ChatHandler::HandleRemoveSkillCommand(const char *args, WorldSession *m_session)
{
	return true ;
	uint32 skill = 0;
	char *pSkill = strtok((char*)args, " ");
	if(!pSkill)
		return false;
	else 
		skill = atol(pSkill);
	BlueSystemMessage(m_session, "Removing skill line %d", skill);

	Player *plr = getSelectedChar(m_session, true);
	if(plr && plr->_HasSkillLine(skill) ) //fix bug; removing skill twice will mess up skills
	{
		plr->_RemoveSkillLine(skill);
		MyLog::gmlog->notice("used remove skill of %u on %s", skill, plr->GetName());
		SystemMessageToPlr(plr, "%s removed skill line %d from you. ", m_session->GetPlayer()->GetName(), skill);
	}
	else
	{
		BlueSystemMessage(m_session, "Player doesn't have skill line %d", skill);
	}
	return true;
}


bool ChatHandler::HandleEmoteCommand(const char* args, WorldSession *m_session)
{
	return true;
	uint32 emote = atoi((char*)args);
	Unit* target = this->getSelectedCreature(m_session);
	if(!target) return false;
	if(target) target->SetUInt32Value(UNIT_NPC_EMOTESTATE,emote);

	return true;
}


extern SERVER_DECL SQLStorage<AreaTrigger, HashMapStorageContainer<AreaTrigger> >				AreaTriggerStorage;
bool ChatHandler::HandleTriggerCommand(const char* args, WorldSession* m_session)
{
	return true;
	int32 instance_id;
	uint32 trigger_id;
	int valcount = sscanf(args, "%u %d", (unsigned int*)&trigger_id, (int*)&instance_id);
	if(!valcount)
		return false;
	if(valcount == 1)
		instance_id = 0;

	AreaTrigger *pTrigger = AreaTriggerStorage.LookupEntry(trigger_id);
	if(trigger_id == 0 || pTrigger == NULL)
	{
		RedSystemMessage(m_session, "Could not find trigger %s", (args == NULL ? "NULL" : args));
		return true;
	}

	m_session->GetPlayer()->SafeTeleport(pTrigger->Mapid, instance_id, LocationVector(pTrigger->x, pTrigger->y,
			pTrigger->z, pTrigger->o));

	BlueSystemMessage(m_session, "Teleported to trigger %u on [%u][%.2f][%.2f][%.2f]", pTrigger->AreaTriggerID,
		pTrigger->Mapid, pTrigger->x, pTrigger->y, pTrigger->z);
	return true;
}

bool ChatHandler::HandleUnlearnCommand(const char* args, WorldSession * m_session)
{
	Player * plr = getSelectedChar(m_session, true);
	if(plr == 0)
		return true;

	uint32 SpellId = atol(args);
	if(SpellId == 0)
	{
		RedSystemMessage(m_session, "You must specify a spell id.");
		return true;
	}

	MyLog::gmlog->notice("removed spell %u from %s", SpellId, plr->GetName());

	if(plr->HasSpell(SpellId))
	{
		GreenSystemMessageToPlr(plr, "Removed spell %u.", SpellId);
		plr->removeSpell(SpellId, false, false, 0);
	}
	else
	{
		RedSystemMessage(m_session, "That player does not have spell %u learnt.", SpellId);
	}

	return true;
}

bool ChatHandler::HandleNpcSpawnLinkCommand(const char* args, WorldSession *m_session)
{
	return true ;
	uint32 id;
	char sql[512];
	Creature* target = m_session->GetPlayer()->GetMapMgr()->GetCreature(GET_LOWGUID_PART(m_session->GetPlayer()->GetSelection()));
	if (!target)
		return false;

	int valcount = sscanf(args, "%u", (unsigned int*)&id);
	if(valcount)
	{
		snprintf(sql, 512, "UPDATE creature_spawns SET respawnlink = '%u' WHERE id = '%u'", (unsigned int)id, (unsigned int)target->GetSQL_id());
		WorldDatabase.WaitExecute( sql );
		BlueSystemMessage(m_session, "Spawn linking for this NPC has been updated: %u", id);
	}
	else
	{
		RedSystemMessage(m_session, "Sql entry invalid %u", id);
	}

	return true;
}

bool ChatHandler::HandleQueryAccountCommand( const char * args, WorldSession * m_session )
{
	char name[1024] = { NULL };
	int valcount = sscanf(args, "%s", name);

	if( valcount )
	{
		PlayerInfo* p = objmgr.GetPlayerInfoByName( name );
		if( p )
		{
			BlueSystemMessage( m_session, "user:%s account id: %u", name, p->acct );
		}
		else
		{
			BlueSystemMessage( m_session, "user:%s not found", name );
		}
	}
	else
	{
		RedSystemMessage(m_session, "query account invalid" );
	}

	return true;
}