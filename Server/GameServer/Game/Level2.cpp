
///////////////////////////////////////////////
//  Admin Movement Commands
//

#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Spell.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "../../Common/Protocol/GS2GT.h"


bool ChatHandler::HandleGMAnnounceCommand(const char* args, WorldSession *m_session)
{
	if(!*args)
	{
		printf("HandleGMAnnounceCommand !args = failed\n");
		return false;
	}

	char GMAnnounce[1024];
	string input2;
	// 	input2 = "<color:ffff6060>[TEAM]<";
	// 	if(m_session->CanUseCommand('z')) input2+="Admin";
	// 	else if(m_session->GetPermissionCount()) input2+="GM";
	// 	input2+="><br><color:1f40af20>player:";
	// 	input2+=m_session->GetPlayer()->GetName();
	// 	input2+=":<br> ";
	snprintf((char*)GMAnnounce, 1024, "%s%s", input2.c_str(), args);
	sWorld.SendGMWorldText(GMAnnounce);
	MyLog::gmlog->notice("used team announce command, [%s]", args);
	return true;
}

bool ChatHandler::HandleSystemNotifyAddCommand(const char* args, WorldSession *m_session)
{
	stSystemNotify* notify = new stSystemNotify;
	if(strlen(args) < 1)
	{
		SendMultilineMessage(m_session, "please input : id  type  msg");
		return false;
	}

	char szTips[256]={0};
	if(sscanf(args, "%u %u %s", &notify->id, &notify->ntype, &szTips) < 3)
	{	
		SendMultilineMessage(m_session, "please input : id  type  msg");
		return false;
	}

	notify->tips = szTips;

	SystemNotifyStorage.SetEntry(notify->id, notify);
	WorldDatabase.Execute("REPLACE INTO SystemNotify VALUES(%u, %u, '%s')", notify->id, notify->ntype, notify->tips);

	MSG_S2C::stSystemNotifyAdd Msg;
	Msg.id = notify->id;
	Msg.ntype = notify->ntype;
	Msg.tips = notify->tips;
	sWorld.SendGlobalMessage(Msg);
	return true;
}

bool ChatHandler::HandleBanCharacterCommand(const char* args, WorldSession *m_session)
{
	if(!*args)
	{
		SystemMessage(m_session, ".banchar  user time reason(date time format : 1y1m1d1h)");
		return false;
	}

	// this is rather complicated due to ban reasons being able to have spaces. so we'll have to some c string magic
	// rather than just sscanf'ing it.
	char * pCharacter = (char*)args;
	char * pBanDuration = strchr(pCharacter, ' ');
	PlayerInfo * pInfo = NULL;
	if(pBanDuration == NULL)
	{
		SystemMessage(m_session, ".banchar  user time reason(date time format : 1y1m1d1h)");
		return false;
	}

	char * pReason = strchr(pBanDuration+1, ' ');
	if(pReason == NULL)
	{
		SystemMessage(m_session, ".banchar  user time reason(date time format : 1y1m1d1h)");
		return false;
	}
	// zero them out to create sepearate strings.
	*pBanDuration = 0;
	++pBanDuration;
	*pReason = 0;
	++pReason;

	int32 BanTime = GetTimePeriodFromString(pBanDuration);
	if(BanTime < 1)
		return false;

	Player * pPlayer = objmgr.GetPlayer(pCharacter, false);
	if(pPlayer == NULL)
	{
		pInfo = objmgr.GetPlayerInfoByName(pCharacter);
		if(pInfo == NULL)
		{
			SystemMessage(m_session, "no such user.");
			return true;
		}

		SystemMessage(m_session, "ban: [%s], reason: [%s].", pCharacter, pReason);

		string escaped_reason = CharacterDatabase.EscapeString(string(pReason));

		CharacterDatabase.WaitExecute("UPDATE characters SET banned = %u, banReason = '%s' WHERE guid = %u",
			BanTime ? BanTime+(uint32)UNIXTIME : 1, escaped_reason.c_str(), pInfo->guid);
	}
	else
	{
		SystemMessage(pPlayer->GetSession(), "ban: [%s], reason: [%s].", pCharacter, pReason);
		string sReason = string(pReason);
		uint32 uBanTime = BanTime ? BanTime+(uint32)UNIXTIME : 1;
		pPlayer->GetSession()->m_baned = uBanTime;
		pPlayer->GetSession()->m_mutereason = sReason;
		pInfo = pPlayer->m_playerInfo;
	}

	SystemMessage(m_session, "ban account expire %s%s.", BanTime ? "on " : "", BanTime ? ConvertTimeStampToDataTime(BanTime+(uint32)UNIXTIME).c_str() : "Never");
	if(pPlayer)
	{
		SystemMessage(pPlayer->GetSession(), "ban expire: %s%s.", BanTime ? "on " : "", BanTime ? ConvertTimeStampToDataTime(BanTime+(uint32)UNIXTIME).c_str() : "Never");
		SystemMessage(m_session, "Kicking %s.", pPlayer->GetName());
		pPlayer->Kick();
	}

	MyLog::gmlog->notice("used ban character on %s reason %s for %s", pCharacter, pReason, BanTime ? ConvertTimeStampToString(BanTime).c_str() : "ever");
	if( sWorld.m_banTable && pInfo )
	{
		CharacterDatabase.WaitExecute("INSERT INTO %s VALUES('%s', '%s', %u, %u, '%s')", sWorld.m_banTable,
			m_session->GetPlayer()->GetName(), pInfo->name, (uint32)UNIXTIME, (uint32)UNIXTIME + BanTime, CharacterDatabase.EscapeString(string(pReason)).c_str() );
	}
	return true;
}

bool ChatHandler::HandleUnBanCharacterCommand(const char* args, WorldSession *m_session)
{
	if(!*args)
	{
		SystemMessage(m_session, ".unbanchar user.");
		return false;
	}

	char Character[255];
	if(sscanf(args, "%s", Character) == 0)
	{
		RedSystemMessage(m_session, "A character name and reason is required.");
		return true;
	}

	// Check if player is in world.
	Player * pPlayer = ObjectMgr::getSingleton( ).GetPlayer(Character, false);
	if(pPlayer != 0)
	{
		GreenSystemMessage(m_session, "Unbanned player %s ingame.",pPlayer->GetName());
		pPlayer->GetSession()->m_baned = 0;
	}
	else
	{
		GreenSystemMessage(m_session, "Player %s not found ingame.", Character);
	}

	// Ban in database
	CharacterDatabase.WaitExecute("UPDATE characters SET banned = 0 WHERE name = '%s'", CharacterDatabase.EscapeString(string(Character)).c_str());

	SystemMessage(m_session, "Unbanned character %s in database.", Character);
	MyLog::gmlog->notice("used unban character on %s", Character);
	return true;
}



bool ChatHandler::CreateGuildCommand(const char* args, WorldSession *m_session)
{
	if(!*args)
		return false;

	Player * ptarget = getSelectedChar(m_session);
	if(!ptarget) return false;

	uint32 arglen = strlen(args);
	if(arglen > 75)
	{
		// send message to user
		char buf[256];
		snprintf((char*)buf,256,"The name was too long by %i", strlen((char*)args)-75);
		SystemMessage(m_session, buf);
		return true;
	}

	for (uint32 i = 0; i < arglen; i++) {
		if(!isalpha(args[i]) && args[i]!=' ') {
			SystemMessage(m_session, "Error, name can only contain chars A-Z and a-z.");
			return true;
		}
	}

	Charter tempCharter(0, ptarget->GetLowGUID(), CHARTER_TYPE_GUILD);
	tempCharter.SignatureCount=0;
	tempCharter.GuildName = args;

	Guild * pGuild = Guild::Create();
	pGuild->CreateFromCharter(&tempCharter, ptarget->GetSession());
	if(!ptarget->HasSpell(88))
	{
		ptarget->addSpell(88);
	}
	SystemMessage(m_session, "Guild created");
	return true;
}

/*
#define isalpha(c)  {isupper(c) || islower(c))
#define isupper(c)  (c >=  'A' && c <= 'Z')
#define islower(c)  (c >=  'a' && c <= 'z')
*/

bool ChatHandler::HandleDeleteCommand(const char* args, WorldSession *m_session)
{

	return true;
	uint64 guid = m_session->GetPlayer()->GetSelection();
	if (guid == 0)
	{
		SystemMessage(m_session, "No selection.");
		return true;
	}

	Creature *unit = m_session->GetPlayer()->GetMapMgr()->GetCreature(GET_LOWGUID_PART(guid));
	if(!unit)
	{
		SystemMessage(m_session, "You should select a creature.");
		return true;
	}
	MyLog::gmlog->notice("used npc delete, sqlid %u, creature %s, pos %f %f %f",
		unit->GetSQL_id(), unit->GetCreatureName() ? unit->GetCreatureName()->Name : "wtfbbqhax", unit->GetPositionX(), unit->GetPositionY(),
		unit->GetPositionZ());
	if(unit->m_spawn == 0)
		return false;
	BlueSystemMessage(m_session, "Deleted creature ID %u", unit->spawnid);

	if(unit->IsInWorld())
	{
		if(unit->m_spawn)
		{
			uint32 cellx=float2int32(((_maxX-unit->m_spawn->x)/_cellSize));
			uint32 celly=float2int32(((_maxY-unit->m_spawn->y)/_cellSize));
			if(cellx <= _sizeX && celly <= _sizeY)
			{
				CellSpawns * c = unit->GetMapMgr()->GetBaseMap()->GetSpawnsListAndCreate(cellx, celly);
				for(CreatureSpawnList::iterator itr = c->CreatureSpawns.begin(); itr != c->CreatureSpawns.end(); ++itr)
					if((*itr) == unit->m_spawn)
					{
						c->CreatureSpawns.erase(itr);
						break;
					}
				delete unit->m_spawn;
			}
		}
		unit->RemoveFromWorld(false,true);
	}
	unit->DeleteFromDB();

	delete unit;

	return true;
}

bool ChatHandler::HandleRemoveCommand(const char* args, WorldSession *m_session)
{
	return true;
	uint64 guid = m_session->GetPlayer()->GetSelection();
	if (guid == 0)
	{
		SystemMessage(m_session, "No selection.");
		return true;
	}

	Creature *unit = m_session->GetPlayer()->GetMapMgr()->GetCreature(GET_LOWGUID_PART(guid));
	if(!unit)
	{
		SystemMessage(m_session, "You should select a creature.");
		return true;
	}
	MyLog::gmlog->notice("used npc remove, entry %u, creature %s, pos %f %f %f",
		unit->GetSQL_id(), unit->GetCreatureName() ? unit->GetCreatureName()->Name : "wtfbbqhax", unit->GetPositionX(), unit->GetPositionY(),
		unit->GetPositionZ());
	if(unit->m_spawn == 0)
		return false;
	BlueSystemMessage(m_session, "remove creature ID %u", unit->spawnid);

	if(unit->IsInWorld())
	{
		unit->RemoveFromWorld(false,true);
	}
	unit->DeleteFromDB();

	delete unit;

	return true;
}
bool ChatHandler::HandleDeMorphCommand(const char* args, WorldSession *m_session)
{
	return true ;


	MyLog::log->error("Demorphed %s",m_session->GetPlayer()->GetName());
	m_session->GetPlayer()->DeMorph();
	return true;
}



bool ChatHandler::HandleCastSpellCommand(const char* args, WorldSession *m_session)
{
	return true;
	Unit *caster = m_session->GetPlayer();
	Unit *target = getSelectedChar(m_session, false);
	if(!target)
		target = getSelectedCreature(m_session, false);
	if(!target)
	{
		RedSystemMessage(m_session, "Must select a char or creature.");
		return false;
	}

	uint32 spellid = atol(args);
	SpellEntry *spellentry = dbcSpell.LookupEntry(spellid);
	if(!spellentry)
	{
		RedSystemMessage(m_session, "Invalid spell id!");
		return false;
	}
	
	Spell *sp = new Spell(caster, spellentry, false, NULL);
	if(!sp)
	{
		RedSystemMessage(m_session, "Spell failed creation!");
		delete sp;
		return false;
	}

	BlueSystemMessage(m_session, "Casting spell %d on target.", spellid);
	SpellCastTargets targets;
	targets.m_unitTarget = target->GetGUID();
	sp->prepare(&targets);
	return true;
}



bool ChatHandler::HandleCastSpellNECommand(const char* args, WorldSession *m_session)
{
	return true;
	Unit *caster = m_session->GetPlayer();
	Unit *target = getSelectedChar(m_session, false);
	if(!target)
		target = getSelectedCreature(m_session, false);
	if(!target)
	{
		RedSystemMessage(m_session, "Must select a char or creature.");
		return false;
	}

	uint32 spellId = atol(args);
	SpellEntry *spellentry = dbcSpell.LookupEntry(spellId);
	if(!spellentry)
	{
		RedSystemMessage(m_session, "Invalid spell id!");
		return false;
	}
	BlueSystemMessage(m_session, "Casting spell %d on target.", spellId);

	MSG_S2C::stSpell_Start MsgStart;
	MsgStart.caster_item_guid	= caster->GetNewGUID();
	MsgStart.caster_unit_guid	= caster->GetNewGUID();
	MsgStart.spell_id			= spellId;
	MsgStart.extra_cast_number	= 0;
	MsgStart.cast_flags			= 0;
	MsgStart.casttime			= 0;
	MsgStart.targets.m_targetMask = TARGET_FLAG_UNIT;
	MsgStart.targets.m_unitTarget = target->GetGUID();
	m_session->SendPacket( MsgStart );

	MSG_S2C::stSpell_Go MsgGo;
	MsgGo.caster_item_guid		= caster->GetNewGUID();
	MsgGo.caster_unit_guid		= caster->GetNewGUID();
	MsgGo.spell_id				= spellId;
	MsgGo.castFlags				= 1;
	MsgGo.casttime				= getMSTime();
	MsgGo.vTargetHit.push_back(target->GetGUID());
	MsgStart.targets.m_targetMask = TARGET_FLAG_UNIT;
	MsgStart.targets.m_unitTarget = target->GetGUID();
	m_session->SendPacket( MsgGo );
	return true;
}


bool ChatHandler::HandleMonsterYellCommand(const char* args, WorldSession *m_session)
{
	return true ;
	Unit *crt = getSelectedCreature(m_session, false);
	if(!crt)
		crt = getSelectedChar(m_session, false);

	if(!crt)
	{
		RedSystemMessage(m_session, "Please select a creature or player before using this command.");
		return true;
	}
	if(crt->GetTypeId() == TYPEID_PLAYER)
	{
		MSG_S2C::stChat_Message Msg;
		this->FillMessageData(CHAT_MSG_YELL, args, crt->GetGUID(), &Msg, 0);
		crt->SendMessageToSet(Msg, true);
	}
	else
	{
		crt->SendChatMessage(CHAT_MSG_MONSTER_YELL, 0, args);
	}

	return true;
}


bool ChatHandler::HandleGOSelect(const char *args, WorldSession *m_session)
{
	return true;
	GameObject *GObj = NULL;

	std::set<Object*>::iterator Itr = m_session->GetPlayer()->GetInRangeSetBegin();
	std::set<Object*>::iterator Itr2 = m_session->GetPlayer()->GetInRangeSetEnd();
	float cDist = 9999.0f;
	float nDist = 0.0f;
	bool bUseNext = false;

	if(args)
	{
		if(args[0] == '1')
		{
			if(m_session->GetPlayer()->m_GM_SelectedGO == NULL)
				bUseNext = true;

			for(;;Itr++)
			{
				if(Itr == Itr2 && GObj == NULL && bUseNext)
					Itr = m_session->GetPlayer()->GetInRangeSetBegin();
				else if(Itr == Itr2)
					break;

				if((*Itr)->GetTypeId() == TYPEID_GAMEOBJECT)
				{
					// Find the current go, move to the next one
					if(bUseNext)
					{
						// Select the first.
						GObj = static_cast<GameObject*>(*Itr);
						break;
					} else {
						if(((*Itr) == m_session->GetPlayer()->m_GM_SelectedGO))
						{
							// Found him. Move to the next one, or beginning if we're at the end
							bUseNext = true;
						}
					}
				}
			}
		}
	}
	if(!GObj)
	{
		for( ; Itr != Itr2; Itr++ )
		{
			if( (*Itr)->GetTypeId() == TYPEID_GAMEOBJECT )
			{
				if( (nDist = m_session->GetPlayer()->CalcDistance( *Itr )) < cDist )
				{
					cDist = nDist;
					nDist = 0.0f;
					GObj = (GameObject*)(*Itr);
				}
			}
		}
	}


	if( GObj == NULL )
	{
		RedSystemMessage(m_session, "No inrange GameObject found.");
		return true;
	}

	m_session->GetPlayer()->m_GM_SelectedGO = GObj;

	GreenSystemMessage(m_session, "Selected GameObject [ %s ] which is %.3f meters away from you.",
		GameObjectNameStorage.LookupEntry(GObj->GetEntry())->Name, m_session->GetPlayer()->CalcDistance(GObj));

	return true;
}

bool ChatHandler::HandleGODelete(const char *args, WorldSession *m_session)
{
	return true;
	GameObject *GObj = m_session->GetPlayer()->m_GM_SelectedGO;
	if( !GObj )
	{
		RedSystemMessage(m_session, "No selected GameObject...");
		return true;
	}
	
	if(GObj->m_spawn != 0 && GObj->m_spawn->entry == GObj->GetEntry())
	{
		uint32 cellx=float2int32(((_maxX-GObj->m_spawn->x)/_cellSize));
		uint32 celly=float2int32(((_maxY-GObj->m_spawn->y)/_cellSize));

		if(cellx < _sizeX && celly < _sizeY)
		{
			//m_session->GetPlayer()->GetMapMgr()->GetBaseMap()->GetSpawnsListAndCreate(cellx,celly)->GOSpawns.erase(GObj->m_spawn);
			CellSpawns * c = GObj->GetMapMgr()->GetBaseMap()->GetSpawnsListAndCreate(cellx, celly);
			for(GOSpawnList::iterator itr = c->GOSpawns.begin(); itr != c->GOSpawns.end(); ++itr)
				if((*itr) == GObj->m_spawn)
				{
					c->GOSpawns.erase(itr);
					break;
				}
			GObj->DeleteFromDB();
			delete GObj->m_spawn;
		}
	}
	GObj->Despawn(0);
	
	delete GObj;

	m_session->GetPlayer()->m_GM_SelectedGO = 0;
  /*  std::stringstream sstext;

	GameObject *GObj = m_session->GetPlayer()->m_GM_SelectedGO;
	if( !GObj )
	{
		RedSystemMessage(m_session, "No selected GameObject...");
		return true;
	}

	GObj->GetMapMgr()->GetBaseMap()->GetTemplate()->RemoveIndex<GameObject>(GObj);	// remove index
	GObj->Despawn(3600000);
	GObj->DeleteFromDB();
	sObjHolder.Delete<GameObject>(GObj);

	m_session->GetPlayer()->m_GM_SelectedGO = NULL;

	GreenSystemMessage(m_session, "GameObject successfully deleted from world and database !");
*/
	return true;
}

bool ChatHandler::HandleGOSpawn(const char *args, WorldSession *m_session)
{
	return true;
	std::stringstream sstext;

	char* pEntryID = strtok((char*)args, " ");
	if (!pEntryID)
		return false;

	uint32 EntryID  = atoi(pEntryID);
	
	bool Save = false;
	char* pSave = strtok(NULL, " ");
	if (pSave)
		Save = (atoi(pSave)>0?true:false);

	GameObjectInfo* goi = GameObjectNameStorage.LookupEntry(EntryID);
	if(!goi)
	{
		sstext << "GameObject Info '" << EntryID << "' Not Found" << '\0';
		SystemMessage(m_session, sstext.str().c_str());
		return true;
	}

	MyLog::log->debug("Spawning GameObject By Entry '%u'", EntryID);
	sstext << "Spawning GameObject By Entry '" << EntryID << "'" << '\0';
	SystemMessage(m_session, sstext.str().c_str());

	GameObject *go = m_session->GetPlayer()->GetMapMgr()->CreateGameObject(EntryID);
	
	Player *chr = m_session->GetPlayer();
	uint32 mapid = chr->GetMapId();
	float x = chr->GetPositionX();
	float y = chr->GetPositionY();
	float z = chr->GetPositionZ();
	float o = chr->GetOrientation();

	go->SetInstanceID(chr->GetInstanceID());
	go->CreateFromProto(EntryID,mapid,x,y,z,o);
	
	/* fuck blizz coordinate system */
	go->SetFloatValue(GAMEOBJECT_ROTATION_02, sinf(o / 2));
	go->SetFloatValue(GAMEOBJECT_ROTATION_03, cosf(o / 2));
	go->PushToWorld(m_session->GetPlayer()->GetMapMgr());

	// Create sapwn instance
	GOSpawn * gs = new GOSpawn;
	gs->entry = go->GetEntry();
	gs->facing = go->GetOrientation();
	gs->faction = go->GetUInt32Value(GAMEOBJECT_FACTION);
	gs->flags = go->GetUInt32Value(GAMEOBJECT_FLAGS);
	gs->id = objmgr.GenerateGameObjectSpawnID();
	gs->o = go->GetFloatValue(GAMEOBJECT_ROTATION);
	gs->o1 = go->GetFloatValue(GAMEOBJECT_ROTATION_01);
	gs->o2 = go->GetFloatValue(GAMEOBJECT_ROTATION_02);
	gs->o3 = go->GetFloatValue(GAMEOBJECT_ROTATION_03);
	gs->scale = go->GetFloatValue(OBJECT_FIELD_SCALE_X);
	gs->x = go->GetPositionX();
	gs->y = go->GetPositionY();
	gs->z = go->GetPositionZ();
	gs->state = go->GetUInt32Value(GAMEOBJECT_STATE);
	//gs->stateNpcLink = 0;

	uint32 cx = m_session->GetPlayer()->GetMapMgr()->GetPosX(m_session->GetPlayer()->GetPositionX());
	uint32 cy = m_session->GetPlayer()->GetMapMgr()->GetPosY(m_session->GetPlayer()->GetPositionY());

	m_session->GetPlayer()->GetMapMgr()->GetBaseMap()->GetSpawnsListAndCreate(cx,cy)->GOSpawns.push_back(gs);
	go->m_spawn = gs;

	//go->AddToWorld();

	if(Save == true)
	{
		// If we're saving, create template and add index
		go->SaveToDB();
	}
	return true;
}

bool ChatHandler::HandleGOInfo(const char *args, WorldSession *m_session)
{
	return true;
	std::string name;
	GameObjectInfo *GOInfo = NULL;
	GameObject *GObj = NULL;

	GObj = m_session->GetPlayer()->m_GM_SelectedGO;
	if( !GObj )
	{
		RedSystemMessage(m_session, "No selected GameObject...");
		return true;
	}

	switch( GObj->GetUInt32Value(GAMEOBJECT_TYPE_ID) )
	{
	case GAMEOBJECT_TYPE_DOOR:		  name = "Door";	break;
	case GAMEOBJECT_TYPE_BUTTON:		name = "Button";	break;
	case GAMEOBJECT_TYPE_QUESTGIVER:	name = "Quest Giver";	break;
	case GAMEOBJECT_TYPE_CHEST:		 name = "Chest";	break;
	case GAMEOBJECT_TYPE_BINDER:		name = "Binder";	break;
	case GAMEOBJECT_TYPE_GENERIC:	   name = "Generic";	break;
	case GAMEOBJECT_TYPE_TRAP:		  name = "Trap";	break;
	case GAMEOBJECT_TYPE_CHAIR:		 name = "Chair";	break;
	case GAMEOBJECT_TYPE_SPELL_FOCUS:   name = "Spell Focus";	break;
	case GAMEOBJECT_TYPE_TEXT:		  name = "Text";	break;
	case GAMEOBJECT_TYPE_GOOBER:		name = "Goober";	break;
	case GAMEOBJECT_TYPE_TRANSPORT:	 name = "Transport";	break;
	case GAMEOBJECT_TYPE_AREADAMAGE:	name = "Area Damage";	break;
	case GAMEOBJECT_TYPE_CAMERA:		name = "Camera";	break;
	case GAMEOBJECT_TYPE_MAP_OBJECT:	name = "Map Object";	break;
	case GAMEOBJECT_TYPE_MO_TRANSPORT:  name = "Mo Transport";	break;
	case GAMEOBJECT_TYPE_DUEL_ARBITER:  name = "Duel Arbiter";	break;
	case GAMEOBJECT_TYPE_FISHINGNODE:   name = "Fishing Node";	break;
	case GAMEOBJECT_TYPE_RITUAL:		name = "Ritual";	break;
	case GAMEOBJECT_TYPE_MAILBOX:	   name = "Mailbox";	break;
	case GAMEOBJECT_TYPE_AUCTIONHOUSE:  name = "Auction House";	break;
	case GAMEOBJECT_TYPE_GUARDPOST:	 name = "Guard Post";	break;
	case GAMEOBJECT_TYPE_SPELLCASTER:  name = "Spell Caster";	break;
	case GAMEOBJECT_TYPE_MEETINGSTONE:  name = "Meeting Stone";	break;
	case GAMEOBJECT_TYPE_FLAGSTAND:	 name = "Flag Stand";	break;
	case GAMEOBJECT_TYPE_FISHINGHOLE:   name = "Fishing Hole";	break;
	case GAMEOBJECT_TYPE_FLAGDROP:	  name = "Flag Drop";	break;
	default:							name = "Unknown type. WTF?";	break; //OMG?! This should NEVER happen o.O
	}

	GOInfo = GameObjectNameStorage.LookupEntry(GObj->GetEntry());
	if( !GOInfo )
	{
		RedSystemMessage(m_session, "This GameObject doesn't have template, you won't be able to get some informations nor to spawn a GO with this entry.");
		return true;
	}
	else
	{
	BlueSystemMessage(m_session,"Informations:");
	GreenSystemMessage(m_session,"Entry:%s %d",MSG_COLOR_LIGHTBLUE, GObj->GetEntry());
	GreenSystemMessage(m_session,"Model:%s %d",MSG_COLOR_LIGHTBLUE, GObj->GetUInt32Value(GAMEOBJECT_DISPLAYID));
	GreenSystemMessage(m_session,"State:%s %d",MSG_COLOR_LIGHTBLUE, GObj->GetUInt32Value(GAMEOBJECT_STATE));
	GreenSystemMessage(m_session,"Flags:%s %d",MSG_COLOR_LIGHTBLUE, GObj->GetUInt32Value(GAMEOBJECT_FLAGS));
	GreenSystemMessage(m_session,"DynFlags:%s %d",MSG_COLOR_LIGHTBLUE, GObj->GetUInt32Value(GAMEOBJECT_DYN_FLAGS));
	GreenSystemMessage(m_session,"Faction:%s %d",MSG_COLOR_LIGHTBLUE, GObj->GetUInt32Value(GAMEOBJECT_FACTION));
	GreenSystemMessage(m_session,"TypeID:%s %d",MSG_COLOR_LIGHTBLUE, GObj->GetUInt32Value(GAMEOBJECT_TYPE_ID));
	GreenSystemMessage(m_session,"Type:%s %s", MSG_COLOR_LIGHTBLUE, name.c_str());
	GreenSystemMessage(m_session," "); //force whitespace line :/
	GreenSystemMessage(m_session,"Distance:%s %d metres",MSG_COLOR_LIGHTBLUE, uint32(GObj->CalcDistance((Object*)m_session->GetPlayer()))); //brackets much :D
	GreenSystemMessage(m_session,"Name:%s %s",MSG_COLOR_LIGHTBLUE, GOInfo->Name);
	GreenSystemMessage(m_session,"Size:%s %f",MSG_COLOR_LIGHTBLUE, GObj->GetFloatValue(OBJECT_FIELD_SCALE_X));
	}

	return true;
}

bool ChatHandler::HandleGOEnable(const char *args, WorldSession *m_session)
{
	return true;
	GameObject *GObj = NULL;

	GObj = m_session->GetPlayer()->m_GM_SelectedGO;
	if( !GObj )
	{
		RedSystemMessage(m_session, "No selected GameObject...");
		return true;
	}
	if(GObj->GetUInt32Value(GAMEOBJECT_DYN_FLAGS) == 1)
	{
		// Deactivate
		GObj->SetUInt32Value(GAMEOBJECT_DYN_FLAGS, 0);
	} else {
		// /Activate
		GObj->SetUInt32Value(GAMEOBJECT_DYN_FLAGS, 1);
	}
	BlueSystemMessage(m_session, "Gameobject activate/deactivated.");
	return true;
}

bool ChatHandler::HandleGOActivate(const char* args, WorldSession *m_session)
{
	return true;
	GameObject *GObj = NULL;

	GObj = m_session->GetPlayer()->m_GM_SelectedGO;
	if( !GObj )
	{
		RedSystemMessage(m_session, "No selected GameObject...");
		return true;
	}
	if(GObj->GetUInt32Value(GAMEOBJECT_STATE) == 1)
	{
		// Close/Deactivate
		GObj->SetUInt32Value(GAMEOBJECT_STATE, 0);
		GObj->SetUInt32Value(GAMEOBJECT_FLAGS, (GObj->GetUInt32Value(GAMEOBJECT_FLAGS)-1));
	} else {
		// Open/Activate
		GObj->SetUInt32Value(GAMEOBJECT_STATE, 1);
		GObj->SetUInt32Value(GAMEOBJECT_FLAGS, (GObj->GetUInt32Value(GAMEOBJECT_FLAGS)+1));
	}
	BlueSystemMessage(m_session, "Gameobject opened/closed.");
	return true;
}

bool ChatHandler::HandleGOScale(const char* args, WorldSession* m_session)
{
	return true;
	GameObject *go = m_session->GetPlayer()->m_GM_SelectedGO;
	if( !go )
	{
		RedSystemMessage(m_session, "No selected GameObject...");
		return true;
	}
	if(!args)
	{
		RedSystemMessage(m_session, "Invalid syntax. Should be .gobject scale 1.0");
		return false;
	}
	float scale = (float)atof(args);
	if(!scale) scale = 1;
	go->SetFloatValue(OBJECT_FIELD_SCALE_X, scale);
	BlueSystemMessage(m_session, "Set scale to %.3f", scale);
	return true;
}


bool ChatHandler::HandleMountCommand(const char *args, WorldSession *m_session)
{
	return true ;


	if(!args)
	{
		RedSystemMessage(m_session, "No model specified");
		return true;
	}
	uint32 modelid = atol(args);
	if(!modelid)
	{
		RedSystemMessage(m_session, "No model specified");
		return true;
	}

	Unit *m_target = NULL;
	Player *m_plyr = getSelectedChar(m_session, false);
	if(m_plyr)
		m_target = m_plyr;
	else
	{
		Creature *m_crt = getSelectedCreature(m_session, false);
		if(m_crt)
			m_target = m_crt;
	}

	if(!m_target)
	{
		RedSystemMessage(m_session, "No target found.");
		return true;
	}
	
	if(m_target->GetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID) != 0)
	{
		RedSystemMessage(m_session, "Target is already mounted.");
		return true;
	}

	m_target->SetUInt32Value( UNIT_FIELD_MOUNTDISPLAYID , modelid);
	//m_target->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_MOUNTED_TAXI);
	
	BlueSystemMessage(m_session, "Now mounted with model %d.", modelid);
	return true;
}

bool ChatHandler::HandleAddAIAgentCommand(const char* args, WorldSession *m_session)
{
	return true;
	char* agent = strtok((char*)args, " ");
	if(!agent)
		return false;
	char* procEvent = strtok(NULL, " ");
	if(!procEvent)
		return false;
	char* procChance = strtok(NULL, " ");
	if(!procChance)
		return false;
	char* procCount = strtok(NULL, " ");
	if(!procCount)
		return false;
	char* spellId = strtok(NULL, " ");
	if(!spellId)
		return false;
	char* spellType = strtok(NULL, " ");
	if(!spellType)
		return false;
	char* spelltargetType = strtok(NULL, " ");
	if(!spelltargetType)
		return false;
	char* spellCooldown = strtok(NULL, " ");
	if(!spellCooldown)
		return false;
	char* floatMisc1 = strtok(NULL, " ");
	if(!floatMisc1)
		return false;
	char* Misc2 = strtok(NULL, " ");
	if(!Misc2)
		return false;

	Unit* target = m_session->GetPlayer()->GetMapMgr()->GetCreature(GET_LOWGUID_PART(m_session->GetPlayer()->GetSelection()));
	if(!target)
	{
		RedSystemMessage(m_session, "You have to select a Creature!");
		return false;
	}

	std::stringstream qry;
	qry << "INSERT INTO ai_agents set entryId = '" << target->GetUInt32Value(OBJECT_FIELD_ENTRY) << "', AI_AGENT = '" << atoi(agent) << "', procEvent = '" << atoi(procEvent)<< "', procChance = '" << atoi(procChance)<< "', procCount = '" << atoi(procCount)<< "', spellId = '" << atoi(spellId)<< "', spellType = '" << atoi(spellType)<< "', spelltargetType = '" << atoi(spelltargetType)<< "', spellCooldown = '" << atoi(spellCooldown)<< "', floatMisc1 = '" << atof(floatMisc1)<< "', Misc2  ='" << atoi(Misc2)<< "'";
	WorldDatabase.WaitExecute( qry.str().c_str( ) );

	AI_Spell * sp = new AI_Spell;
	sp->agent = atoi(agent);
	sp->procChance = atoi(procChance);
/*	sp->procCount = atoi(procCount);*/
	sp->spell = dbcSpell.LookupEntry(atoi(spellId));
	sp->spellType = atoi(spellType);
	sp->spelltargetType = atoi(spelltargetType);
	sp->floatMisc1 = (float)atof(floatMisc1);
	sp->Misc2 = (uint32)atof(Misc2);
	sp->cooldown = (uint32)atoi(spellCooldown);
	sp->procCount=0;
	sp->procCounter=0;
	sp->cooldowntime=0;
	sp->custom_pointer=false;
	sp->minrange = GetMinRange(dbcSpellRange.LookupEntry(dbcSpell.LookupEntry(atoi(spellId))->rangeIndex));
	sp->maxrange = GetMaxRange(dbcSpellRange.LookupEntry(dbcSpell.LookupEntry(atoi(spellId))->rangeIndex));
	if(sp->agent == AGENT_CALLFORHELP)
		target->GetAIInterface()->m_canCallForHelp = true;
	else if(sp->agent == AGENT_FLEE)
		target->GetAIInterface()->m_canFlee = true;
	else if(sp->agent == AGENT_RANGED)
		target->GetAIInterface()->m_canRangedAttack = true;
	else if(sp->agent == AGENT_SPELL_ATTACK)
	{
		target->GetAIInterface()->m_canSpellAttack = true;
		target->GetAIInterface()->m_defaultAttackSpell = sp->spell;
	}
	//else 
	{
		target->GetAIInterface()->addSpellToList(sp);
	}

	return true;
}

bool ChatHandler::HandleListAIAgentCommand(const char* args, WorldSession *m_session)
{
	return true;
	Unit* target = m_session->GetPlayer()->GetMapMgr()->GetCreature(GET_LOWGUID_PART(m_session->GetPlayer()->GetSelection()));
	if(!target)
	{
		RedSystemMessage(m_session, "You have to select a Creature!");
		return false;
	}

	std::stringstream sstext;
	sstext << "agentlist of creature: " << target->GetGUID() << '\n';

	std::stringstream ss;
	ss << "SELECT * FROM ai_agents where entry=" << target->GetUInt32Value(OBJECT_FIELD_ENTRY);
	QueryResult *result = WorldDatabase.Query( ss.str().c_str() );

	if( !result )
		return false;

	do
	{
		Field *fields = result->Fetch();
		sstext << "agent: "   << fields[1].GetUInt16()
			<< " | spellId: " << fields[5].GetUInt32()
			<< " | Event: "   << fields[2].GetUInt32()
			<< " | chance: "  << fields[3].GetUInt32()
			<< " | count: "   << fields[4].GetUInt32() << '\n';
	} while( result->NextRow() );

	delete result;

	SendMultilineMessage(m_session, sstext.str().c_str());

	return true;
}

bool ChatHandler::HandleGOAnimProgress(const char * args, WorldSession * m_session)
{
	return true;
	if(!m_session->GetPlayer()->m_GM_SelectedGO)
		return false;

	uint32 ap = atol(args);
	m_session->GetPlayer()->m_GM_SelectedGO->SetUInt32Value(GAMEOBJECT_ANIMPROGRESS, ap);
	BlueSystemMessage(m_session, "Set ANIMPROGRESS to %u", ap);
	return true;
}

bool ChatHandler::HandleGOExport(const char * args, WorldSession * m_session)
{
	return true;
	/*if(!m_session->GetPlayer()->m_GM_SelectedGO)
		return false;
	
	std::stringstream name;
	if (*args)
	{
		name << "GO_" << args << ".sql";
	}
	else
	{
		name << "GO_" << m_session->GetPlayer()->m_GM_SelectedGO->GetEntry() << ".sql";
	}
   
	m_session->GetPlayer()->m_GM_SelectedGO->SaveToFile(name);

	BlueSystemMessage(m_session, "Go saved to: %s", name.str().c_str());*/

	Creature * pCreature = getSelectedCreature(m_session, true);
	if(!pCreature) return true;
	WorldDatabase.WaitExecute("INSERT INTO creature_names_export SELECT * FROM creature_names_copy WHERE entry = %u", pCreature->GetEntry());
	WorldDatabase.WaitExecute("INSERT INTO creature_proto_export SELECT * FROM creature_proto_copy WHERE entry = %u", pCreature->GetEntry());
	WorldDatabase.WaitExecute("INSERT INTO vendors_export SELECT * FROM vendors WHERE entry = %u", pCreature->GetEntry());
	QueryResult * qr = WorldDatabase.Query("SELECT * FROM vendors WHERE entry = %u", pCreature->GetEntry());
	if(qr != NULL)
	{
		do 
		{
			WorldDatabase.WaitExecute("INSERT INTO items_export SELECT * FROM items WHERE entry = %u", qr->Fetch()[1].GetUInt32());
		} while (qr->NextRow());
		delete qr;
	}
	return true;
}
bool ChatHandler::HandleAccountUnbanCommand(const char * args, WorldSession * m_session)
{
	if(!*args) return false;
	ui32 acct;
	int argc = sscanf(args, "%d %d", &acct);
	if(argc != 1)
		return false;

	//sLogonCommHandler.Account_SetBanned( pAccount, 0 );
	GreenSystemMessage(m_session, "Account '%d' has been unbanned. This change will be effective immediately.", acct);
	MSG_GS2GT::stBan Msg;
	Msg.acct = acct;
	Msg.ban = 0;
	m_session->_GTSocket->PostSend(Msg);
	MyLog::yunyinglog->notice("gm unbanned account %d", acct);
	return true;
}




