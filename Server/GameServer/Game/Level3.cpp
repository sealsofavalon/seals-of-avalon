/////////////////////////////////////////////////
//  Admin Chat Commands
//
#include "StdAfx.h"

#include "../../../SDBase/Protocol/S2C_Move.h"
#include "../../../SDBase/Protocol/S2C_Spell.h"
#include "../../Common/Protocol/GS2GT.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "SunyouCastle.h"
#include "InstanceQueue.h"
#include "../Master.h"
#include "ArenaTeam.h"

bool ChatHandler::HandleAddDefaultItemCommand(const char *args, WorldSession *m_session)
{
	uint32 id, lv = 0;
	int32 randomprop=0;

	if(strlen(args) < 1)
	{
		return false;
	}

	if(sscanf(args, "%u %u", &id, &lv) < 1)
		return false;

	Player *chr = NULL;
	//if(strcmp(m_session->GetPermissions(), "4") != 0)
	chr = getSelectedChar(m_session);
	if (chr == NULL) return true;
	else
		chr = m_session->GetPlayer();

	stDefaultAddItem* default_add = AddItemStorage.LookupEntry(id);
	if(default_add)
	{
		for(int i = 0; i < 14; i++)
		{
			if(!default_add->entry[i])
				break;
			ItemPrototype* it = ItemPrototypeStorage.LookupEntry(default_add->entry[i]);
			if(!it)
				continue;
			Item *item;
			item = objmgr.CreateItem( default_add->entry[i], chr);
			item->SetUInt32Value(ITEM_FIELD_STACK_COUNT, 1);
			if(it->Bonding==ITEM_BIND_ON_PICKUP)
				item->SoulBind();

			item->SetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL, lv);

			item->WashEnchantment();

			if(!chr->GetItemInterface()->AddItemToFreeSlot(item))
			{
				m_session->SendNotification("No free slots were found in your inventory!");
				delete item;
				return true;
			}

			char messagetext[128];
			snprintf(messagetext, 128, "Adding item %d (%s) to %s's inventory.",(unsigned int)it->ItemId,Item::BuildStringWithProto(it), chr->GetName());
			SystemMessage(m_session, messagetext);
			snprintf(messagetext, 128, "%s added item %d (%s) to your inventory.", m_session->GetPlayer()->GetName(), (unsigned int)default_add->entry[i], Item::BuildStringWithProto(it));
			SystemMessageToPlr(chr,  messagetext);

			MyLog::yunyinglog->info("item-gm give-player[%u][%s] buy["I64FMT"][%u] count[%u] by gm[%u][%s]", chr->GetLowGUID(), chr->GetName(), item->GetGUID(), it->ItemId, 1, m_session->GetPlayer()->GetLowGUID(), m_session->GetPlayer()->GetName());

			SlotResult *lr = chr->GetItemInterface()->LastSearchResult();
			chr->GetSession()->SendItemPushResult(item,false,true,false,true,lr->ContainerSlot,lr->Slot,1);

			item->m_isDirty = true;
		}		

		return true;
	}
	return false;
}

bool ChatHandler::HandleStartCommand(const char* args, WorldSession *m_session)
{
	std::string race;
	uint32 raceid = 0;

	Player *m_plyr = getSelectedChar(m_session, false);

	if (m_plyr && args && strlen(args) < 2)
	{
		raceid = m_plyr->getRace();
		switch (raceid)
		{
		case 1:
			race = "human";
			break;
		case 2:
			race = "orc";
			break;
		case 3:
			race = "dwarf";
			break;
		case 4:
			race = "nightelf";
			break;
		case 5:
			race = "undead";
			break;
		case 6:
			race = "tauren";
			break;
		case 7:
			race = "gnome";
			break;
		case 8:
			race = "troll";
			break;
		case 10:
			race = "bloodelf";
			break;
		case 11:
			race = "draenei";
			break;
		default:
			return false;
			break;
		}
	}
	else if (m_plyr && args && strlen(args) > 2)
	{
		race = args;
		ASCENT_TOLOWER(race);

		// Teleport to specific race
		if(race == "human")
			raceid = 1;
		else if(race == "orc")
			raceid = 2;
		else if(race == "dwarf")
			raceid = 3;
		else if(race == "nightelf")
			raceid = 4;
		else if(race == "undead")
			raceid = 5;
		else if(race == "tauren")
			raceid = 6;
		else if(race == "gnome")
			raceid = 7;
		else if(race == "troll")
			raceid = 8;
		else if(race == "bloodelf")
			raceid = 10;
		else if(race == "draenei")
			raceid = 11;
		else
		{
			RedSystemMessage(m_session, "Invalid start location! Valid locations are: human, dwarf, gnome, nightelf, draenei, orc, troll, tauren, undead, bloodelf");
			return true;
		}
	}
	else
	{
		return false;
	}

	// Try to find a class that works
	PlayerCreateInfo *info = NULL;
	for(uint32 i=1;i<11;i++)
	{
		info = objmgr.GetPlayerCreateInfo(raceid, i);
		if(info != NULL) break;
	}

	if(info == NULL)
	{
		RedSystemMessage(m_session, "Internal error: Could not find create info.");
		return false;
	}


	GreenSystemMessage(m_session, "Telporting %s to %s starting location.", m_plyr->GetName(), race.c_str());

	m_session->GetPlayer()->SafeTeleport(info->mapId, 0, LocationVector(info->positionX, info->positionY, info->positionZ));
	return true;
}



bool ChatHandler::HandleAddInvItemCommand(const char *args, WorldSession *m_session)
{
	uint32 itemid, count=1;
	int32 randomprop=0;

	if(strlen(args) < 1)
	{
		return false;
	}

	if(sscanf(args, "%u %u %d", &itemid, &count, &randomprop) < 1)
		return false;

	Player *chr = NULL;
	//if(strcmp(m_session->GetPermissions(), "4") != 0)
	{
		chr = getSelectedChar(m_session);
		if (chr == NULL) return true;
	}
	// 	else
	// 		chr = m_session->GetPlayer();

	ItemPrototype* it = ItemPrototypeStorage.LookupEntry(itemid);
	if(it)
	{
		MyLog::gmlog->notice("used add item command, item id %u to %s", it->ItemId, chr->GetName());

		ui32 nAddCount = count;
		while(nAddCount>0)
		{
			ui32 nAdded = ((nAddCount > it->MaxCount) ? it->MaxCount : nAddCount);
			if( !nAdded )
			{
				return false;
			}
			nAddCount-=nAdded;
			Item *item;
			item = objmgr.CreateItem( itemid, chr);
			item->SetUInt32Value(ITEM_FIELD_STACK_COUNT, nAdded);
			if(it->Bonding==ITEM_BIND_ON_PICKUP)
				item->SoulBind();

			if(randomprop!=0)
			{
				if(randomprop<0)
					item->SetRandomSuffix(abs(int(randomprop)));
				else
					item->SetRandomProperty(randomprop);

				item->ApplyRandomProperties(false);
			}

			item->WashEnchantment();

			if(!chr->GetItemInterface()->AddItemToFreeSlot(item))
			{
				m_session->SendNotification("No free slots were found in your inventory!");
				delete item;
				return true;
			}

			char messagetext[128];
			snprintf(messagetext, 128, "Adding item %d (%s) to %s's inventory.",(unsigned int)it->ItemId,Item::BuildStringWithProto(it), chr->GetName());
			SystemMessage(m_session, messagetext);
			snprintf(messagetext, 128, "%s added item %d (%s) to your inventory.", m_session->GetPlayer()->GetName(), (unsigned int)itemid, Item::BuildStringWithProto(it));
			SystemMessageToPlr(chr,  messagetext);

			MyLog::yunyinglog->info("item-gm give-player[%u][%s] buy["I64FMT"][%u] count[%u] by gm[%u][%s]", chr->GetLowGUID(), chr->GetName(), item->GetGUID(), it->ItemId, count, m_session->GetPlayer()->GetLowGUID(), m_session->GetPlayer()->GetName());

			SlotResult *lr = chr->GetItemInterface()->LastSearchResult();
			chr->GetSession()->SendItemPushResult(item,false,true,false,true,lr->ContainerSlot,lr->Slot,nAdded);

		}		

		return true;
	} else {
		RedSystemMessage(m_session, "Item %d is not a valid item!",itemid);
		return true;
	}
}
bool ChatHandler::HandleSaveAllCommand(const char *args, WorldSession *m_session)
{
	PlayerStorageMap::const_iterator itr;
	uint32 stime = getMSTime();
	uint32 count = 0;
	for (itr = objmgr._players.begin(); itr != objmgr._players.end(); itr++)
	{
		if(itr->second->GetSession())
		{
			itr->second->SaveToDB(false);
			count++;
		}
	}
	char msg[100];
	snprintf(msg, 100, "Saved all %d online players in %d msec.", (int)count, int((uint32)getMSTime() - stime));
	sWorld.SendWorldText(msg);
	sWorld.SendWorldWideScreenText(msg);
	MyLog::gmlog->notice("saved all players");
	//sWorld.SendIRCMessage(msg);
	return true;
}


bool ChatHandler::HandleClearCooldownsCommand(const char *args, WorldSession *m_session)
{
	return true ;
	uint32 guid = (uint32)m_session->GetPlayer()->GetSelection();
	Player *plr = getSelectedChar(m_session, true);
	
	if(!plr)
	{
		plr = m_session->GetPlayer();
		SystemMessage(m_session, "Auto-targeting self.");
	}
	if(!plr) return false;

	if(plr->getClass()==CLASS_WARRIOR)
	{
		plr->ClearCooldownsOnLine(26, guid);
		plr->ClearCooldownsOnLine(256, guid);
		plr->ClearCooldownsOnLine(257 , guid);
		BlueSystemMessage(m_session, "Cleared all Warrior cooldowns.");
		return true;
	}
	
	if(plr->getClass()==CLASS_BOW)
	{
		plr->ClearCooldownsOnLine(50, guid);
		plr->ClearCooldownsOnLine(51, guid);
		plr->ClearCooldownsOnLine(163, guid);
		BlueSystemMessage(m_session, "Cleared all Bow cooldowns.");
		return true;
	}
	if(plr->getClass()==CLASS_ROGUE)
	{
		plr->ClearCooldownsOnLine(253, guid);
		plr->ClearCooldownsOnLine(38, guid);
		plr->ClearCooldownsOnLine(39, guid);
		BlueSystemMessage(m_session, "Cleared all Rogue cooldowns.");
		return true;
	}
	if(plr->getClass()==CLASS_PRIEST)
	{
		plr->ClearCooldownsOnLine(56, guid);
		plr->ClearCooldownsOnLine(78, guid);
		plr->ClearCooldownsOnLine(613, guid);
		BlueSystemMessage(m_session, "Cleared all Priest cooldowns.");
		return true;
	}
	
	if(plr->getClass()==CLASS_MAGE)
	{
		plr->ClearCooldownsOnLine(6, guid);
		plr->ClearCooldownsOnLine(8, guid);
		plr->ClearCooldownsOnLine(237, guid);
		BlueSystemMessage(m_session, "Cleared all Mage cooldowns.");
		return true;
	}
	
	if(plr->getClass()==CLASS_DRUID)
	{
		plr->ClearCooldownsOnLine(573, guid);
		plr->ClearCooldownsOnLine(574, guid);
		plr->ClearCooldownsOnLine(134, guid);
		BlueSystemMessage(m_session, "Cleared all Druid cooldowns.");
		return true;
	}
	return true;
}

bool ChatHandler::HandleLearnCommand(const char* args, WorldSession *m_session)
{
	if (!*args)
		return false;

	Player *plr = getSelectedChar(m_session, true);
	if(!plr)
	{
		plr = m_session->GetPlayer();
		SystemMessage(m_session, "Auto-targeting self.");
	}
	if(!plr) return false;


	if(stricmp(args, "all")==0)
	{
		MyLog::gmlog->notice("taught %s all spells.", plr->GetName());
		static uint32 spellarray[CLASS_MAX][9999] = {
			{ 0 },		// CLASS 0
			{ 0 },		// CLASS 10
			{ 0 },		// CLASS 0
			{ 0 },		// CLASS 6
			{ 0 },		// CLASS 10
			{ 0 },		// CLASS 10
		};

		uint32 c = plr->getClass();
		for(uint32 i = 0; spellarray[c][i] != 0; ++i)
		{
			plr->addSpell(spellarray[c][i]);
		}


		StorageContainerIterator<SpellEntry> * itrSpell = dbcSpell.MakeIterator();
		while(!itrSpell->AtEnd())
		{
			SpellEntry * sp = itrSpell->Get();

			if(	sp && (sp->Attributes & ATTRIBUTES_PASSIVE) ) // passive
			{
				if(!itrSpell->Inc())
					break;
				continue;
			}
			plr->addSpell(sp->Id);
			if(!itrSpell->Inc())
				break;
		}
		itrSpell->Destruct();

		uint32 r = plr->getRace();

		return true;
	}

	uint32 spell = atol((char*)args);
	MyLog::gmlog->notice("taught %s spell %u", plr->GetName(), spell);

	SpellEntry * sp = dbcSpell.LookupEntry(spell);

	if(!sp)
	{
		MyLog::log->alert("not exist spell[%u]", spell);
		return true;
	}
	if(!plr->GetSession()->HasGMPermissions() && (sp->Effect[0]==SPELL_EFFECT_INSTANT_KILL||sp->Effect[1]==SPELL_EFFECT_INSTANT_KILL||sp->Effect[2]==SPELL_EFFECT_INSTANT_KILL))
	{
		SystemMessage(m_session, "don't be an idiot and teach players instakill spells. this action has been logged.");
		return true;
	}

	if (plr->HasSpell(spell)) // check to see if char already knows
	{
		std::string OutStr = plr->GetName();
		OutStr += " already knows that spell.";

		SystemMessage(m_session, OutStr.c_str());
		return true;
	}

	plr->addSpell(spell);
	BlueSystemMessageToPlr(plr, "%s taught you Spell %d", m_session->GetPlayer()->GetName(), spell);

	return true;
}

bool ChatHandler::HandleLearnClassCommand(const char* args, WorldSession *m_session)
{
	if (!*args)
		return false;

	Player *plr = getSelectedChar(m_session, true);
	if(!plr)
	{
		plr = m_session->GetPlayer();
		SystemMessage(m_session, "Auto-targeting self.");
	}
	if(!plr) return false;

	uint32 nclass = atol((char*)args);

	StorageContainerIterator<SpellEntry> * itrSpell = dbcSpell.MakeIterator();
	while(!itrSpell->AtEnd())
	{
		SpellEntry * sp = itrSpell->Get();

		if(sp->classmask_foreditor && (sp->classmask_foreditor & (1 << nclass)))
			plr->addSpell(sp->Id);
		if(!itrSpell->Inc())
			break;
	}
	itrSpell->Destruct();

	uint32 r = plr->getRace();

	return true;

}
bool ChatHandler::HandleReviveCommand(const char* args, WorldSession *m_session)
{
	Player* SelectedPlayer = getSelectedChar(m_session, true);
	if(!SelectedPlayer) return true;

	
	SelectedPlayer->SetMovement(MOVE_UNROOT, 1);
	SelectedPlayer->ResurrectPlayer();
	SelectedPlayer->SetUInt32Value(UNIT_FIELD_HEALTH, SelectedPlayer->GetUInt32Value(UNIT_FIELD_MAXHEALTH) );
	return true;
}
bool ChatHandler::HandleReviveStringcommand(const char* args, WorldSession* m_session)
{
	Player *plr = objmgr.GetPlayer(args, false);
	if(!plr)
	{
		RedSystemMessage(m_session, "Could not find player %s.", args);
		return true;
	}

	if(plr->isDead())
	{
		if(plr->GetInstanceID() == m_session->GetPlayer()->GetInstanceID())
			plr->RemoteRevive();
		else
			sEventMgr.AddEvent(plr, &Player::RemoteRevive, EVENT_PLAYER_REST, 1, 1,0);

		GreenSystemMessage(m_session, "Revived player %s.", args);
	} else {
		GreenSystemMessage(m_session, "Player %s is not dead.", args);
	}
	return true;
}

bool ChatHandler::HandleDismountCommand(const char* args, WorldSession *m_session)
{
	Unit *m_target = NULL;

	Player *p_target = getSelectedChar(m_session, false);

	if(p_target)
		m_target = p_target;
	else
	{
		Creature *m_crt = getSelectedCreature(m_session, false);
		if(m_crt)
			m_target = m_crt;
	}

	if(!m_target)
	{
		RedSystemMessage(m_session, "No target found.");
		return true;
	}

	if(m_target->GetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID) == 0)
	{
		RedSystemMessage(m_session, "Target is not mounted.");
		return true;
	}

	if(p_target && p_target->m_MountSpellId)
		p_target->RemoveAura(p_target->m_MountSpellId);

	m_target->SetUInt32Value( UNIT_FIELD_MOUNTDISPLAYID , 0);
	//m_target->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_MOUNTED_TAXI);

	BlueSystemMessage(m_session, "Now unmounted.");
	return true;
}

bool ChatHandler::HandleInfoCommand(const char* args, WorldSession *m_session)
{
	uint32 clientsNum = (uint32)sWorld.GetSessionCount();

	int gm = 0;
	int count = 0;
	int avg = 0;
	PlayerStorageMap::const_iterator itr;
	for (itr = objmgr._players.begin(); itr != objmgr._players.end(); itr++)
	{
		if(itr->second->GetSession())
		{
			count++;
			avg += itr->second->GetSession()->GetLatency();
			if(itr->second->GetSession()->GetPermissionCount())
				gm++;
		}			
	}
	BlueSystemMessage(m_session, "Server Revision	:  r%u/%s-%s-%s",BUILD_REVISION, CONFIG, PLATFORM_TEXT, ARCH);
	BlueSystemMessage(m_session, "Server Uptime	: %s", sWorld.GetUptimeString().c_str());
	BlueSystemMessage(m_session, "Current Players	: %d (%d GMs, %d queued)", clientsNum, gm,  0);
	// 	GreenSystemMessage(m_session, "Active Thread Count: %u<br>", ThreadPool.GetActiveThreadCount());
	// 	GreenSystemMessage(m_session, "Free Thread Count: %u<br>", ThreadPool.GetFreeThreadCount());
	BlueSystemMessage(m_session, "Average Latency	: %.3fms", (float)((float)avg / (float)count));
	// 	GreenSystemMessage(m_session, "SQL Query Cache Size (World): %u queries delayed<br>", WorldDatabase.GetQueueSize());
	// 	GreenSystemMessage(m_session, "SQL Query Cache Size (Character): <br>%u queries delayed", CharacterDatabase.GetQueueSize());

	return true;
}


bool ChatHandler::HandleExploreCheatCommand(const char* args, WorldSession *m_session)
{
	return true ;
	if (!*args)
		return false;

	int flag = atoi((char*)args);

	Player *chr = getSelectedChar(m_session);
	if (chr == NULL)
	{
		SystemMessage(m_session, "No character selected.");
		return true;
	}

	char buf[256];

	// send message to user
	if (flag != 0)
	{
		snprintf((char*)buf,256,"%s has explored all zones now.", chr->GetName());
	}
	else
	{
		snprintf((char*)buf,256,"%s has no more explored zones.", chr->GetName());
	}
	SystemMessage(m_session, buf);

	// send message to player
	if (flag != 0)
	{
		snprintf((char*)buf,256,"%s has explored all zones for you.",
			m_session->GetPlayer()->GetName());
	}
	else
	{
		snprintf((char*)buf,256,"%s has hidden all zones from you.", 
			m_session->GetPlayer()->GetName());
	}
	SystemMessage(m_session,  buf);

/*
	for (uint8 i=0; i<64; i++)
	{
		if (flag != 0)
		{
			chr->SetFlag(PLAYER_EXPLORED_ZONES_1+i,0xFFFFFFFF);
		}
		else
		{
			chr->SetFlag(PLAYER_EXPLORED_ZONES_1+i,0);
		}
	}
*/
	return true;
}


bool ChatHandler::HandleKillCommand(const char *args, WorldSession *m_session)
{
	Unit * target = m_session->GetPlayer()->GetMapMgr()->GetUnit(m_session->GetPlayer()->GetSelection());
	if(target == 0)
	{
		SystemMessage(m_session, "请选择kill目标");
		return true;
	}

	switch(target->GetTypeId())
	{
	case TYPEID_PLAYER:
		MyLog::gmlog->notice("used kill command on PLAYER %s", static_cast< Player* >( target )->GetName() );
		break;

	case TYPEID_UNIT:
		MyLog::gmlog->notice("used kill command on CREATURE %s", static_cast< Creature* >( target )->GetCreatureName() ? static_cast< Creature* >( target )->GetCreatureName()->Name : "unknown");
		break;
	}


	// If we're killing a player, send a message indicating a gm killed them.
	if(target->IsPlayer())
	{
		Player * plr = static_cast< Player* >(target);
		m_session->GetPlayer()->DealDamage(plr, plr->GetUInt32Value(UNIT_FIELD_HEALTH),0,0,0);
		//plr->SetUInt32Value(UNIT_FIELD_HEALTH, 0);
		//plr->KillPlayer();
		BlueSystemMessageToPlr(plr, "%s killed you with a GM command.", m_session->GetPlayer()->GetName());
	}
	else
	{

		// Cast insta-kill.
		SpellEntry * se = dbcSpell.LookupEntry(5);
		if(se == 0) return false;

		SpellCastTargets targets(target->GetGUID());
		Spell * sp = new Spell(m_session->GetPlayer(), se, true, 0);
		sp->prepare(&targets);

		/*		SpellEntry * se = dbcSpell.LookupEntry(20479);
		if(se == 0) return false;

		SpellCastTargets targets(target->GetGUID());
		Spell * sp = new Spell(target, se, true, 0);
		sp->prepare(&targets);*/
	}

	return true;
}
bool ChatHandler::HandleKillByPlrCommand( const char *args , WorldSession *m_session )
{
	if (!args)
	{
		return false ;
	}
	Player *plr = objmgr.GetPlayer(args, false);
	if(!plr)
	{
		RedSystemMessage(m_session, "Player %s is not online or does not exist.", args);
		return true;
	}

	if(plr->isDead())
	{
		RedSystemMessage(m_session, "Player %s is already dead.", args);
	} else {
		plr->SetUInt32Value(UNIT_FIELD_HEALTH, 0); // Die, insect
		plr->KillPlayer();
		BlueSystemMessageToPlr(plr, "You were killed by %s with a GM command.", m_session->GetPlayer()->GetName());
		GreenSystemMessage(m_session, "Killed player %s.", args);
		MyLog::gmlog->notice("remote killed "I64FMT" (Name: %s)", plr->GetGUID(), plr->GetNameString() );

	}
	return true;
}

bool ChatHandler::HandleGMTicketGetAllCommand(const char* args, WorldSession *m_session)
{
	return true;
	Channel *chn = channelmgr.GetChannel(sWorld.getGmClientChannel().c_str(),m_session->GetPlayer());
	if(!chn)
		return false;

	chn->Say(m_session->GetPlayer(), "GmTicket 2", m_session->GetPlayer(), true);
	for(GmTicketList::iterator itr = objmgr.GM_TicketList.begin(); itr != objmgr.GM_TicketList.end(); itr++)
	{
		uint32 cont = 0;
		uint32 zone = 0;
		Player* plr = objmgr.GetPlayer((uint32)(*itr)->guid);
		/*if(plr)
		{
			if(plr->IsInWorld())
			{
				zone = plr->GetZoneId();
				cont = plr->GetMapId();
			}
		}*/
		if( plr == NULL || !plr->IsInWorld() )
			continue;

		cont = plr->GetMapId();
		zone = plr->GetZoneId();

			std::stringstream str;
			str << "GmTicket 0,";
			str << (*itr)->name.c_str() << ","  << (*itr)->level << ","  << (*itr)->type << ",";
			str << zone;

			chn->Say(m_session->GetPlayer(),str.str().c_str(), m_session->GetPlayer(), true);
	}

	return true;
}

bool ChatHandler::HandleGMTicketGetByIdCommand(const char* args, WorldSession *m_session)
{
	return true;
	if(!*args)
		return false;


	GmTicketList::iterator i;
	for(i = objmgr.GM_TicketList.begin(); i != objmgr.GM_TicketList.end(); i++)
	{
		if(strcmp((*i)->name.c_str(), args) == 0)
		{
			Channel *chn = channelmgr.GetChannel(sWorld.getGmClientChannel().c_str(),m_session->GetPlayer());
			if(!chn)
				return false;

			std::stringstream str;
			str << "GmTicket 3,";
			str << (*i)->name.c_str() << "," << (*i)->message;
			chn->Say(m_session->GetPlayer(),str.str().c_str(), m_session->GetPlayer(), true);
		}
	}
	return true;
}

bool ChatHandler::HandleGMTicketDelByIdCommand(const char* args, WorldSession *m_session)
{
	return true;
	if(!*args)
		return false;

// 	GmTicketList::iterator i;
// 	int64 guid = -1;
// 	for(i = objmgr.GM_TicketList.begin(); i != objmgr.GM_TicketList.end(); i++)
// 	{
// 		if(strcmp((*i)->name.c_str(), args) == 0)
// 		{
// 			guid = (int64)(*i)->guid;
// 			break;
// 		}
// 	}
// 	if(guid != -1)
// 	{
// 		objmgr.remGMTicket(guid);
// 
// 		std::stringstream str;
// 		str << "GmTicket 1," << args;
// 
// 		Channel *chn = channelmgr.GetChannel(sWorld.getGmClientChannel().c_str(),m_session->GetPlayer());
// 		if(!chn)
// 			return false;
// 
// 		chn->Say(m_session->GetPlayer(), str.str().c_str(), NULL, true);
// 
// 
// 		Player* plr = objmgr.GetPlayer((uint32)guid);
// 		if(!plr)
// 			return true;
// 		if(!plr->IsInWorld())
// 			return true;
// 
// 		WorldPacket data(SMSG_GMTICKET_DELETETICKET, 4);
// 		data << uint32(9);
// 
// 		plr->GetSession()->SendPacket( &data );
// 	}

	return true;
}

bool ChatHandler::HandleAddSkillCommand(const char* args, WorldSession *m_session)
{
	char buf[256];
	Player* target = objmgr.GetPlayer((uint32)m_session->GetPlayer()->GetSelection());

	if(!target) {
		SystemMessage(m_session, "Select A Player first.");
		return true;
	}

	uint32 skillline;
	uint16 cur, max;

	char* pSkillline = strtok((char*)args, " ");
	if (!pSkillline)
		return false;

	char* pCurrent = strtok(NULL, " ");
	if (!pCurrent)
		return false;

	char* pMax = strtok(NULL, " ");
	if (!pMax)
		return false;

	skillline = (uint32)atol(pSkillline);
	cur = (uint16)atol(pCurrent);
	max = (uint16)atol(pMax);

	target->_AddSkillLine(skillline,cur,max);

	snprintf(buf,256,"SkillLine: %u CurrentValue %u Max Value %u Added.",(unsigned int)skillline,(unsigned int)cur,(unsigned int)max);
	MyLog::gmlog->notice("added skill line %u (%u/%u) to %s", skillline, cur, max, target->GetName());
	SystemMessage(m_session, buf);

	return true;
}
bool ChatHandler::HandleMonsterSayCommand(const char* args, WorldSession *m_session)
{
	Unit *crt = getSelectedCreature(m_session, false);
	if(!crt)
		crt = getSelectedChar(m_session, false);

	if(!crt)
	{
		RedSystemMessage(m_session, "Please select a creature or player before using this command.");
		return true;
	}
	if(crt->GetTypeId() == TYPEID_PLAYER)
	{
		MSG_S2C::stChat_Message Msg;
		this->FillMessageData(CHAT_MSG_SAY, args, crt->GetGUID(), &Msg, 0);
		crt->SendMessageToSet(Msg, true);
	}
	else
	{
		crt->SendChatMessage(CHAT_MSG_MONSTER_SAY, 0, args);
	}

	return true;
}

bool ChatHandler::HandleNpcInfoCommand(const char *args, WorldSession *m_session)
{
	char msg[512];
	uint32 guid = GUID_LOPART(m_session->GetPlayer()->GetSelection());
	Creature *crt = getSelectedCreature(m_session);
	if(!crt) return false;
	if(crt->GetCreatureName())
		BlueSystemMessage(m_session, "Showing creature info for %s", crt->GetCreatureName()->Name);
	snprintf(msg,512,"GUID: %d\nFaction: %d\nNPCFlags: %d\nDisplayID: %d", (int)guid, (int)crt->GetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE), (int)crt->GetUInt32Value(UNIT_NPC_FLAGS), (int)crt->GetUInt32Value(UNIT_FIELD_DISPLAYID));
	SystemMessage(m_session, msg);
	if(crt->m_faction)
		GreenSystemMessage(m_session, "Combat Support: 0x%.3X", crt->m_faction->FriendlyMask);
	GreenSystemMessage(m_session, "Base Health: %d", crt->GetUInt32Value(UNIT_FIELD_BASE_HEALTH));
	GreenSystemMessage(m_session, "Base Armor: %d", crt->GetFloatValue(UNIT_FIELD_RESISTANCES));
	GreenSystemMessage(m_session, "Base Mana: %d", crt->GetUInt32Value(UNIT_FIELD_MAXPOWER1));
	GreenSystemMessage(m_session, "Base Holy: %d", crt->GetFloatValue(UNIT_FIELD_RESISTANCES_01));
	GreenSystemMessage(m_session, "Base Fire: %d", crt->GetFloatValue(UNIT_FIELD_RESISTANCES_02));
	GreenSystemMessage(m_session, "Base Nature: %d", crt->GetFloatValue(UNIT_FIELD_RESISTANCES_03));
	GreenSystemMessage(m_session, "Base Frost: %d", crt->GetFloatValue(UNIT_FIELD_RESISTANCES_04));
	GreenSystemMessage(m_session, "Base Shadow: %d", crt->GetFloatValue(UNIT_FIELD_RESISTANCES_05));
	GreenSystemMessage(m_session, "Base Arcane: %d", crt->GetFloatValue(UNIT_FIELD_RESISTANCES_06));
	GreenSystemMessage(m_session, "Damage min/max: %f/%f", crt->GetFloatValue(UNIT_FIELD_MINDAMAGE),crt->GetFloatValue(UNIT_FIELD_MAXDAMAGE));
	
	ColorSystemMessage(m_session, MSG_COLOR_RED, "Entry ID: %d", crt->GetUInt32Value(OBJECT_FIELD_ENTRY));
	ColorSystemMessage(m_session, MSG_COLOR_RED, "SQL Entry ID: %d", crt->GetSQL_id());
	// show byte
	std::stringstream sstext;
	uint32 theBytes = crt->GetUInt32Value(UNIT_FIELD_BYTES_0);
	sstext << "UNIT_FIELD_BYTES_0 are " << uint16((uint8)theBytes & 0xFF) << " " << uint16((uint8)(theBytes >> 8) & 0xFF) << " ";
	sstext << uint16((uint8)(theBytes >> 16) & 0xFF) << " " << uint16((uint8)(theBytes >> 24) & 0xFF) << '\0';
	BlueSystemMessage(m_session, sstext.str().c_str());
	return true;
}

bool ChatHandler::HandleNpcComeCommand(const char* args, WorldSession* m_session)
{
	// moves npc to players location
	Player * plr = m_session->GetPlayer();
	Creature * crt = getSelectedCreature(m_session, true);
	if(!crt) return true;

	crt->GetAIInterface()->MoveTo(plr->GetPositionX(), plr->GetPositionY(), plr->GetPositionZ(), plr->GetOrientation());
	return true;
}


bool ChatHandler::HandleIncreaseWeaponSkill(const char *args, WorldSession *m_session)
{
	return true ;
	char *pMin = strtok((char*)args, " ");
	uint32 cnt = 0;
	if(!pMin)
		cnt = 1;
	else
		cnt = atol(pMin);

	Player *pr = getSelectedChar(m_session, true);
	
	uint32 SubClassSkill = 0;
	if(!pr) pr = m_session->GetPlayer();
	if(!pr) return false;
	Item *it = pr->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_MAINHAND);
	ItemPrototype* proto = NULL;
	//if (!it)
	//	it = pr->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_RANGED);
	if (it)
		proto = it->GetProto();
	if (proto)
	{
		switch(proto->SubClass)
		{
			// Weapons
		case 0:	// 1 handed axes
			SubClassSkill = SKILL_AXES;
			break;
		case 1:	// 2 handed axes
			SubClassSkill = SKILL_2H_AXES;
			break;		
		case 2:	// bows
			SubClassSkill = SKILL_BOWS;
			break;
		case 3:	// guns
			SubClassSkill = SKILL_GUNS;
			break;		
		case 4:	// 1 handed mace
			SubClassSkill = SKILL_MACES;
			break;
		case 5:	// 2 handed mace
			SubClassSkill = SKILL_2H_MACES;
			break;		
		case 6:	// polearms
			SubClassSkill = SKILL_POLEARMS;
			break;
		case 7: // 1 handed sword
			SubClassSkill = SKILL_SWORDS;
			break;
		case 8: // 2 handed sword
			SubClassSkill = SKILL_2H_SWORDS;
			break;
		case 9: // obsolete
			SubClassSkill = 136;
			break;
		case 10: //1 handed exotic
			SubClassSkill = 136;
			break;
		case 11: // 2 handed exotic
			SubClassSkill = 0;
			break;
		case 12: // fist
			SubClassSkill = SKILL_FIST_WEAPONS;
			break;
		case 13: // misc
			SubClassSkill = 0;
			break;
		case 15: // daggers
			SubClassSkill = SKILL_DAGGERS;
			break;
		case 16: // thrown
			SubClassSkill = SKILL_THROWN;
			break;
		case 17: // spears
			SubClassSkill = SKILL_SPEARS;
			break;
		case 18: // crossbows
			SubClassSkill = SKILL_CROSSBOWS;
			break;
		case 19: // wands
			SubClassSkill = SKILL_WANDS;
			break;
		case 20: // fishing
			SubClassSkill = SKILL_FISHING;
			break;
		}
	} 
	else
	{
		SubClassSkill = 162;
	}

	if(!SubClassSkill)
	{
		RedSystemMessage(m_session, "Can't find skill ID :-/");
		return false;
	}

	uint32 skill = SubClassSkill;

	BlueSystemMessage(m_session, "Modifying skill line %d. Advancing %d times.", skill, cnt);
	MyLog::gmlog->notice("increased weapon skill of %s by %u", pr->GetName(), cnt);

	if(!pr->_HasSkillLine(skill))
	{
		SystemMessage(m_session, "Does not have skill line, adding.");
		pr->_AddSkillLine(skill, 1, 300);   
	} 
	else 
	{
		pr->_AdvanceSkillLine(skill,cnt);
	}	   
	return true;	
}


bool ChatHandler::HandleResetTalentsCommand(const char* args, WorldSession *m_session)
{
	return true;
	Player *plr = this->getSelectedChar(m_session);
	if(!plr) return true;

	plr->Reset_Talents();

	SystemMessage(m_session, "Reset talents of %s.", plr->GetName());;
	BlueSystemMessageToPlr(plr, "%s reset all your talents.", m_session->GetPlayer()->GetName());
	MyLog::gmlog->notice("reset talents of %s", plr->GetName());
	return true;
}

bool ChatHandler::HandleResetSpellsCommand(const char* args, WorldSession *m_session)
{
	Player *plr = this->getSelectedChar(m_session);
	if(!plr) return true;

	plr->Reset_Spells();
	
	SystemMessage(m_session, "Reset spells of %s to level 1.", plr->GetName());;
	BlueSystemMessage(m_session, "%s reset all your spells to starting values.", m_session->GetPlayer()->GetName());
	MyLog::gmlog->notice("reset spells of %s", plr->GetName());
	return true;
}

bool ChatHandler::HandleAccountLevelCommand(const char * args, WorldSession * m_session)
{
	return true ;
    if(!*args) return false;

	char account[100];
	char gmlevel[100];
	int argc = sscanf(args, "%s %s", account, gmlevel);
	if(argc != 2)
		return false;

	//sLogonCommHandler.Account_SetGM( account, gmlevel );

	GreenSystemMessage(m_session, "Account '%s' level has been updated to '%s'. The change will be effective immediately.", account, gmlevel);
	MyLog::yunyinglog->notice("set account %s flags to %s", account, gmlevel);

	return true;
}


bool ChatHandler::HandleAccountMuteCommand(const char * args, WorldSession * m_session)
{
	if(!*args) return false;

	ui32 acct;char duration[100] = {0};
	int argc = sscanf(args, "%d %s", &acct, duration);
	if(argc != 2)
		return false;
	if(duration[0] == '\0')
		return false;

	int32 timeperiod = GetTimePeriodFromString(duration);
	if(timeperiod <= 0)
	{
		timeperiod = 3600;
	}

	uint32 muted = (uint32)UNIXTIME+timeperiod;

	//sLogonCommHandler.Account_SetMute( pAccount, banned );

	string tsstr = ConvertTimeStampToDataTime(muted);
	GreenSystemMessage(m_session, "账号'%d'被禁言  截止日期: %s. 立即生效", acct, 
		tsstr.c_str());

	MyLog::yunyinglog->notice("gm mutex account %d until %s", acct, ConvertTimeStampToDataTime(muted).c_str());

	WorldSession * pSession = sWorld.FindSession(acct);
	if( pSession != NULL )
	{
		pSession->m_muted = muted;
		pSession->SystemMessage("你已经被GM禁言,截止日期: %s", tsstr.c_str());
	}
	MSG_GS2GT::stMute Msg;
	Msg.acct = acct;
	Msg.mute = muted;
	m_session->_GTSocket->PostSend(Msg);
	return true;
}


bool ChatHandler::HandleCharUnmuteCommand(const char * args, WorldSession * m_session)
{
	//sLogonCommHandler.Account_SetMute( args, 0 );

	GreenSystemMessage(m_session, "角色 '%s' 禁言解除.", args);
	Player * plr = objmgr.GetPlayer(args);
	if(!plr)
	{
		GreenSystemMessage(m_session, "未发现这个角色 '%s' .", args);
		return false;
	}
	WorldSession * pSession = plr->GetSession();
	if( pSession != NULL )
	{
		pSession->m_muted = 0;
		pSession->SystemMessage("你的禁言解除了.");
	}

	return true;
}

bool ChatHandler::HandleBuyCastleNpcCommand( const char* args, WorldSession* m_session )
{
	return true;
	int npc_id = 0;
	sscanf( args, "%d", &npc_id );
	Player* p = m_session->GetPlayer();
	SunyouInstance* pInstance = p->GetMapMgr()->m_sunyouinstance;
	if( pInstance && pInstance->GetCategory() == INSTANCE_CATEGORY_UNIQUE_CASTLE )
	{
		return ((SunyouCastle*)pInstance)->BuyNPC( p, npc_id );
	}
	return false;
}

bool ChatHandler::HandleGetTransporterTime(const char* args, WorldSession* m_session)
{
	return true;
	//Player *plyr = m_session->GetPlayer();
	Creature * crt = getSelectedCreature(m_session, false);
	if( crt == NULL )
		return false;

	MSG_S2C::stAttacker_State_Update Msg;
	Msg.hitStats	= HITSTATUS_HITANIMATION | HITSTATUS_CRICTICAL | HITSTATUS_ABSORBED;
	Msg.attacker_guid	= crt->GetNewGUID();
	Msg.victim_guid		= m_session->GetPlayer()->GetNewGUID();
	Msg.realdamage	= 6;
	Msg.damageschool	= 1;
	Msg.fdamage_full = float(0x40c00000);
	Msg.ndamage_full = 6;
	Msg.damage_abs	 = 0;
	Msg.resisted_damage = 0;
	Msg.victim_stat  = 1;
	Msg.blocked_damage = 0;
	m_session->SendPacket(Msg);
	return true;
}

bool ChatHandler::HandleRemoveAurasCommand(const char *args, WorldSession *m_session)
{
	Player *plr = getSelectedChar(m_session, true);
	if(!plr) return false;

	BlueSystemMessage(m_session, "Removing all auras...");
	for(uint32 i = 0; i < MAX_AURAS + MAX_PASSIVE_AURAS; ++i)
	{
		if(plr->m_auras[i] != 0) plr->m_auras[i]->Remove();
	}
	return true;
}

bool ChatHandler::HandleRemoveRessurectionSickessAuraCommand(const char *args, WorldSession *m_session)
{
	return true;
	Player *plr = getSelectedChar(m_session, true);
	if(!plr) return false;

	BlueSystemMessage(m_session, "Removing ressurection sickness...");
	plr->RemoveAura( 15007 );
	return true;
}

bool ChatHandler::HandleParalyzeCommand(const char* args, WorldSession *m_session)
{
	return true ;
	//Player *plr = getSelectedChar(m_session, true);
	//if(!plr) return false;
	Unit *plr = m_session->GetPlayer()->GetMapMgr()->GetUnit(m_session->GetPlayer()->GetSelection());
	if(!plr || plr->GetTypeId() != TYPEID_PLAYER)
	{
		RedSystemMessage(m_session, "Invalid target.");
		return true;
	}

	BlueSystemMessage(m_session, "Rooting target.");
	BlueSystemMessageToPlr( static_cast< Player* >( plr ), "You have been rooted by %s.", m_session->GetPlayer()->GetName() );
	MSG_S2C::stMove_Force_Root Msg;
	Msg.guid	 = plr->GetNewGUID();
	Msg.flag	 = 1;
	plr->SendMessageToSet(Msg, true);
	return true;
}

bool ChatHandler::HandleUnParalyzeCommand(const char* args, WorldSession *m_session)
{
	return true;
	//Player *plr = getSelectedChar(m_session, true);
	//if(!plr) return false;
	Unit *plr = m_session->GetPlayer()->GetMapMgr()->GetUnit(m_session->GetPlayer()->GetSelection());
	if(!plr || plr->GetTypeId() != TYPEID_PLAYER)
	{
		RedSystemMessage(m_session, "Invalid target.");
		return true;
	}
	
	BlueSystemMessage(m_session, "Unrooting target.");
	BlueSystemMessageToPlr( static_cast< Player* >( plr ), "You have been unrooted by %s.", m_session->GetPlayer()->GetName() );

	MSG_S2C::stMove_Force_UnRoot Msg;
	Msg.guid	= plr->GetNewGUID();
	Msg.flag	= 5;
	plr->SendMessageToSet(Msg, true);
	return true;
}

bool ChatHandler::HandleSetMotdCommand(const char* args, WorldSession* m_session)
{
	return true;
	if(!args || strlen(args) < 2)
	{
		RedSystemMessage(m_session, "You must specify a message.");
		return true;
	}

	GreenSystemMessage(m_session, "Motd has been set to: %s", args);
	World::getSingleton().SetMotd(args);
	MyLog::gmlog->notice("Set MOTD to %s", args);
	return true;
}

bool ChatHandler::HandleAddItemSetCommand(const char* args, WorldSession* m_session)
{
	return true;
	uint32 setid = (args ? atoi(args) : 0);
	if(!setid)
	{
		RedSystemMessage(m_session, "You must specify a setid.");
		return true;
	}

	Player *chr = getSelectedChar(m_session);
	if (chr == NULL) {
	RedSystemMessage(m_session, "Unable to select character.");
	return true;
	}

	ItemSetEntry *entry = ItemSetStorage.LookupEntry(setid);
	std::list<ItemPrototype*>* l = objmgr.GetListForItemSet(setid);
	if(!entry || !l)
	{
		RedSystemMessage(m_session, "Invalid item set.");
		return true;
	}
	//const char* setname = sItemSetStore.LookupString(entry->name);
	BlueSystemMessage(m_session, "Searching item set %u...", setid);
	uint32 start = getMSTime();
	MyLog::gmlog->notice("used add item set command, set %u, target %s", setid, chr->GetName());
	for(std::list<ItemPrototype*>::iterator itr = l->begin(); itr != l->end(); ++itr)
	{
		Item *itm = objmgr.CreateItem((*itr)->ItemId, m_session->GetPlayer());
		if(!itm) continue;
		if(itm->GetProto()->Bonding == ITEM_BIND_ON_PICKUP)
			itm->SoulBind();

		if(!chr->GetItemInterface()->AddItemToFreeSlot(itm))
		{
			m_session->SendNotification("No free slots left!");
			delete itm;
			return true;
		} else {
			//SystemMessage(m_session, "Added item: %s [%u]", (*itr)->Name1, (*itr)->ItemId);
			SlotResult * le = chr->GetItemInterface()->LastSearchResult();
			chr->GetSession()->SendItemPushResult(itm,false,true,false,true,le->ContainerSlot,le->Slot,1);
		}
	}
	GreenSystemMessage(m_session, "Added set to inventory complete. Time: %u ms", getMSTime() - start);
	return true;
}

bool ChatHandler::HandleExitInstanceCommand(const char* args, WorldSession* m_session)
{
	BlueSystemMessage(m_session, "Attempting to exit from instance...");
	bool result = m_session->GetPlayer()->ExitInstance();
	if(!result)
	{
		RedSystemMessage(m_session, "Entry points not found.");
		return true;
	} else {
		GreenSystemMessage(m_session, "Removal successful.");
		return true;
	}
}

bool ChatHandler::HandleCastTimeCheatCommand(const char* args, WorldSession* m_session)
{
	Player * plyr = getSelectedChar(m_session, true);
	if(!plyr) return true;

	bool val = plyr->CastTimeCheat;
	BlueSystemMessage(m_session, "%s cast time cheat on %s.", val ? "Deactivating" : "Activating", plyr->GetName());
	GreenSystemMessageToPlr(plyr, "%s %s a cast time cheat on you.", m_session->GetPlayer()->GetName(), val ? "deactivated" : "activated");

	plyr->CastTimeCheat = !val;
	MyLog::gmlog->notice("%s cast time cheat on %s", val ? "disabled" : "enabled", plyr->GetName());
	return true;
}

bool ChatHandler::HandleCooldownCheatCommand(const char* args, WorldSession* m_session)
{
	Player * plyr = getSelectedChar(m_session, true);
	if(!plyr) return true;

	bool val = plyr->CooldownCheat;
	BlueSystemMessage(m_session, "%s cooldown cheat on %s.", val ? "Deactivating" : "Activating", plyr->GetName());
	GreenSystemMessageToPlr(plyr, "%s %s a cooldown cheat on you.", m_session->GetPlayer()->GetName(), val ? "deactivated" : "activated");

	plyr->CooldownCheat = !val;
	MyLog::gmlog->notice("%s cooldown cheat on %s", val ? "disabled" : "enabled", plyr->GetName());

	return true;
}

bool ChatHandler::HandleGodModeCommand(const char* args, WorldSession* m_session)
{
	Player * plyr = getSelectedChar(m_session, true);
	if(!plyr)
	{
		plyr = m_session->GetPlayer();
		return true;
	}

	bool val = plyr->GodModeCheat;
	BlueSystemMessage(m_session, "%s godmode cheat on %s.", val ? "Deactivating" : "Activating", plyr->GetName());
	GreenSystemMessageToPlr(plyr, "%s %s a godmode cheat on you.", m_session->GetPlayer()->GetName(), val ? "deactivated" : "activated");

	plyr->GodModeCheat = !val;
	MyLog::gmlog->notice("%s godmode cheat on %s", val ? "disabled" : "enabled", plyr->GetName());
	return true;
}

bool ChatHandler::HandlePowerCheatCommand(const char* args, WorldSession* m_session)
{
	Player * plyr = getSelectedChar(m_session, true);
	if(!plyr) return true;

	bool val = plyr->PowerCheat;
	BlueSystemMessage(m_session, "%s power cheat on %s.", val ? "Deactivating" : "Activating", plyr->GetName());
	GreenSystemMessageToPlr(plyr, "%s %s a power cheat on you.", m_session->GetPlayer()->GetName(), val ? "deactivated" : "activated");

	plyr->PowerCheat = !val;
	MyLog::gmlog->notice("%s powertime cheat on %s", val ? "disabled" : "enabled", plyr->GetName());
	return true;
}

bool ChatHandler::HandleShowCheatsCommand(const char* args, WorldSession* m_session)
{
	Player * plyr = getSelectedChar(m_session, true);
	if(!plyr) return true;

	uint32 active = 0, inactive = 0;
#define print_cheat_status(CheatName, CheatVariable) SystemMessage(m_session, "%s%s: %s%s", MSG_COLOR_LIGHTBLUE, CheatName, \
		CheatVariable ? MSG_COLOR_LIGHTRED : MSG_COLOR_GREEN, CheatVariable ? "Active" : "Inactive");  \
		if(CheatVariable) \
		active++; \
		else \
		inactive++; 

	GreenSystemMessage(m_session, "Showing cheat status for: %s", plyr->GetName());
	print_cheat_status("Cooldown", plyr->CooldownCheat);
	print_cheat_status("CastTime", plyr->CastTimeCheat);
	print_cheat_status("GodMode", plyr->GodModeCheat);
	print_cheat_status("Power", plyr->PowerCheat);
	print_cheat_status("Fly", plyr->FlyCheat);
	print_cheat_status("AuraStack", plyr->stack_cheat);
	SystemMessage(m_session, "%u cheats active, %u inactive.", active, inactive);

#undef print_cheat_status

	return true;
}

bool ChatHandler::HandleFlyCommand(const char* args, WorldSession* m_session)
{
	return true ;
	MSG_S2C::stMove_Set_Fly Msg;	
	Player *chr = getSelectedChar(m_session);
	
	if(!chr)
		chr = m_session->GetPlayer();
	
	chr->m_setflycheat = true;
	Msg.guid = chr->GetNewGUID();
	chr->SendMessageToSet(Msg, true);
	BlueSystemMessage(chr->GetSession(), "Flying mode enabled.");
	return 1;
}

bool ChatHandler::HandleLandCommand(const char* args, WorldSession* m_session)
{
	return true ;
	MSG_S2C::stMove_Set_UnFly Msg;	
	
	Player *chr = getSelectedChar(m_session);
	
	if(!chr)
		chr = m_session->GetPlayer();
	
	chr->m_setflycheat = false;
	Msg.guid = chr->GetNewGUID();
	chr->SendMessageToSet(Msg, true);
	BlueSystemMessage(chr->GetSession(), "Flying mode disabled.");
	return 1;
}

bool ChatHandler::HandleDBReloadCommand(const char* args, WorldSession* m_session)
{
	char str[200];
	if(!*args || strlen(args) < 3)
		return false;

	uint32 mstime = getMSTime();
	snprintf(str, 200, "%s%s initiated server-side reload of table `%s`. The server may experience some lag while this occurs.",
		MSG_COLOR_LIGHTRED, m_session->GetPlayer()->GetName(), args);
	sWorld.SendWorldText(str, 0);
	if(!Storage_ReloadTable(args))
		snprintf(str, 200, "%sDatabase reload failed.", MSG_COLOR_LIGHTRED);
	else
		snprintf(str, 200, "%sDatabase reload completed in %u ms.", MSG_COLOR_LIGHTBLUE, (unsigned int)(getMSTime() - mstime));
	sWorld.SendWorldText(str, 0);
	MyLog::gmlog->notice("reloaded table %s", args);
	return true;
}

bool ChatHandler::HandleFlySpeedCheatCommand(const char* args, WorldSession* m_session)
{
	return true ;
	float Speed = (float)atof(args);
	if(Speed == 0)
		Speed = 20;

	Player * plr = getSelectedChar(m_session);
	if(plr == 0)
		return true;

	BlueSystemMessage(m_session, "Setting the fly speed of %s to %f.", plr->GetName(), Speed);
	GreenSystemMessage(plr->GetSession(), "%s set your fly speed to %f.", m_session->GetPlayer()->GetName(), Speed);
	
	plr->SetPlayerSpeed( FLY, Speed );
	
	plr->m_flySpeed = Speed;
	
	return true;
}

bool ChatHandler::HandleModifyGuildScoreCommand(const char* args, WorldSession* m_session)
{
	if (!*args)
	{
		return false;
	}

	int Score = (float)atoi((char*)args);

	if (Score < 0)
	{
		RedSystemMessage(m_session, "Incorrect value. not < 0");
		return false;
	}
	Player* plr = getSelectedChar(m_session);
	if (plr == NULL)
	{
		return false;
	}
	
	char buff[256];
	BlueSystemMessage(m_session, "You set the guild score of %s to %d.", plr->GetName(), Score);
	snprintf((char*)buff,256, "%s set your guild score to %d.", plr->GetName(), Score);
	SystemMessage(plr->GetSession(), buff);
	plr->GetGuild()->ModifyGuildScore(Score);

	return true;
}

bool ChatHandler::HandleModifyGuildWarScoreCommand(const char* args, WorldSession* m_session)
{
	if (!*args)
	{
		return false;
	}

	int Score = (float)atoi((char*)args);

	if (Score < 0)
	{
		RedSystemMessage(m_session, "Incorrect value. not < 0");
		return false;
	}
	Player* plr = getSelectedChar(m_session);
	if (plr == NULL)
	{
		return false;
	}

	char buff[256];
	BlueSystemMessage(m_session, "You set the guild war score of %s to %d.", plr->GetName(), Score);
	snprintf((char*)buff,256, "%s set your guild war score to %d.", plr->GetName(), Score);
	SystemMessage(plr->GetSession(), buff);
	plr->GetGuild()->ModifyGuildHiddenScore(Score);

	return true;
}

bool ChatHandler::HandleModifySpeedCommand(const char* args, WorldSession *m_session)
{
	if (!*args)
		return false;

	float Speed = (float)atof((char*)args);

	if (Speed > 255 || Speed < 1)
	{
		RedSystemMessage(m_session, "Incorrect value. Range is 1..255");
		return true;
	}

	Player *chr = getSelectedChar(m_session);
	if( chr == NULL )
		return true;

	char buf[256];

	// send message to user
	BlueSystemMessage(m_session, "You set the speed of %s to %2.2f.", chr->GetName(), Speed);

	// send message to player
	snprintf((char*)buf,256, "%s set your speed to %2.2f.", m_session->GetPlayer()->GetName(), Speed);
	SystemMessage(chr->GetSession(), buf);

	chr->SetPlayerSpeed(RUN, Speed);
	chr->SetPlayerSpeed(SWIM, Speed);
	chr->SetPlayerSpeed(RUNBACK, Speed);
	chr->SetPlayerSpeed(FLY, Speed);

	return true;
}

bool ChatHandler::HandleModifyGoldCommand(const char* args, WorldSession *m_session)
{
	//	WorldPacket data;

	if ( *args == 0 )
		return false;

	Player *chr = NULL;
	//if(strcmp(m_session->GetPermissions(), "4") != 0)
	{
		chr = getSelectedChar( m_session, true );
		if( chr == NULL ) return true;
	}
	// 	else
	// 	{
	// 		chr = m_session->GetPlayer();
	// 	}

	int32 total   = atoi( (char*)args );

	// gold = total / 10000;
	// silver = (total / 100) % 100;
	// copper = total % 100;
	uint32 gold   = (uint32) floor( (float)int32abs( total ) / 10000.0f );
	uint32 silver = (uint32) floor( ((float)int32abs( total ) / 100.0f) ) % 100;
	uint32 copper = int32abs2uint32( total ) % 100;

	MyLog::gmlog->notice("used modify gold on %s, gold: %d", chr->GetName(), total);
	//sGMLog.writefromsession( m_session, "used modify gold on %s, gold: %d", chr->GetName(), total );

	int32 newgold = chr->GetUInt32Value( PLAYER_FIELD_COINAGE ) + total;

	if(newgold < 0)
	{
		BlueSystemMessage( m_session, "Taking all gold from %s's backpack...", chr->GetName() );
		GreenSystemMessageToPlr(chr, "%s took the all gold from your backpack.", m_session->GetPlayer()->GetName());
		newgold = 0;
	}
	else
	{
		if(total >= 0) {
			BlueSystemMessage( m_session,
				"Adding %u gold, %u silver, %u copper to %s's backpack...",
				gold, silver, copper,
				chr->GetName() );

			GreenSystemMessageToPlr( chr, "%s added %u gold, %u silver, %u copper to your backpack.",
				m_session->GetPlayer()->GetName(),
				gold, silver, copper );
		}
		else
		{
			BlueSystemMessage( m_session,
				"Taking %u gold, %u silver, %u copper from %s's backpack...",
				gold, silver, copper,
				chr->GetName() );

			GreenSystemMessageToPlr( chr, "%s took %u gold, %u silver, %u copper from your backpack.",
				m_session->GetPlayer()->GetName(),
				gold, silver, copper );
		}
	}

	chr->SetUInt32Value( PLAYER_FIELD_COINAGE, newgold );
	chr->m_playerInfo->total_coin = newgold;
	MyLog::yunyinglog->info("gold-gmset-player[%u][%s] set gold[%u] gm[%u][%s]", chr->GetLowGUID(), chr->GetName(), newgold, m_session->GetPlayer()->GetLowGUID(), m_session->GetPlayer()->GetName());

	return true;
}
bool ChatHandler::HandleModifyLevelCommand(const char* args, WorldSession* m_session)
{
	Player * plr = getSelectedChar(m_session, true);
	if(plr == 0) return true;

	uint32 Level = args ? atol(args) : 0;
	if(Level == 0 || Level > sWorld.m_levelCap)
	{
		RedSystemMessage(m_session, "A level (numeric) is required to be specified after this command.");
		return true;
	}

	// Set level message
	BlueSystemMessage(m_session, "Setting the level of %s to %u.", plr->GetName(), Level);
	GreenSystemMessageToPlr(plr, "%s set your level to %u.", m_session->GetPlayer()->GetName(), Level);

	MyLog::gmlog->notice("used modify level on %s, level %u", plr->GetName(), Level);

	// lookup level information
	LevelInfo * Info = objmgr.GetLevelInfo(plr->getRace(), plr->getClass(), Level);
	if(Info == 0)
	{
		RedSystemMessage(m_session, "Levelup information not found.");
		return true;
	}

	plr->ApplyLevelInfo(Info, Level);
	return true;
}

bool ChatHandler::HandleCreatePetCommand(const char* args, WorldSession* m_session)
{
	return true;
	if(!args || strlen(args) < 2)
		return false;

	uint32 Entry = atol(args);
	if(!Entry)
		return false;
	CreatureProto * pTemplate = CreatureProtoStorage.LookupEntry(Entry);
	CreatureInfo * pCreatureInfo = CreatureNameStorage.LookupEntry(Entry);
	if(!pTemplate || !pCreatureInfo)
	{
		RedSystemMessage(m_session, "Invalid creature spawn template: %u", Entry);
		return true;
	}
	Player * plr = m_session->GetPlayer();

	// spawn a creature of this id to create from
	Creature * pCreature = new Creature(HIGHGUID_TYPE_UNIT);//no need in guid
	CreatureSpawn * sp = new CreatureSpawn;
	sp->id = 1;
	sp->bytes = 0;
	sp->bytes2 = 0;
	sp->displayid = pCreatureInfo->Male_DisplayID;
	sp->emote_state = 0;
	sp->entry = pCreatureInfo->Id;
	sp->factionid = pTemplate->Faction;
	sp->flags = 0;
	sp->form = 0;
	sp->movetype = 0;
	sp->o = plr->GetOrientation();
	sp->x = plr->GetPositionX();
	sp->y = plr->GetPositionY();
	//sp->respawnNpcLink = 0;
	sp->stand_state = 0;
	sp->channel_spell=sp->channel_target_creature=sp->channel_target_go=0;
	pCreature->Load(sp, (uint32)NULL, NULL);

	Pet *old_tame = plr->GetSummon();
	if(old_tame != NULL)
	{
		old_tame->Dismiss(true);
	}

	// create a pet from this creature
	Pet * pPet = objmgr.CreatePet();
	pPet->SetInstanceID(plr->GetInstanceID());
	pPet->SetMapId(plr->GetMapId());
	pPet->CreateAsSummon(Entry, pCreatureInfo, pCreature, plr, NULL, 0x2, 0);

	// remove the temp creature
	delete sp;
	delete pCreature;

	MyLog::gmlog->notice("used create pet entry %u", Entry);
	return true;
}


#ifdef USE_SPECIFIC_AIAGENTS
//this is custom stuff !
bool ChatHandler::HandlePetSpawnAIBot(const char* args, WorldSession *m_session)
{
	if (!*args)
		return false;

	if( !m_session->GetPlayer() )
		return false; //wtf ?

	uint32 botprice = m_session->GetPlayer()->GetUInt32Value(UNIT_FIELD_LEVEL)*10000; //1 gold per level ?

	if( m_session->GetPlayer()->GetUInt32Value(PLAYER_FIELD_COINAGE) < botprice )
	{
		GreenSystemMessage(m_session, "You need a total of %u coins to afford a bot", botprice);
		return false;
	}

	uint8 botType = (uint8)atof((char*)args);

	if ( botType!=0 )
	{
		RedSystemMessage(m_session, "Incorrect value. Accepting value 0 only = healbot :)");
		return true;
	}

	uint32 Entry;
	char name[50];
	uint8 race = m_session->GetPlayer()->getRace();

	if( race == RACE_HUMAN || race == RACE_DWARF || race == RACE_NIGHTELF || race == RACE_GNOME || race == RACE_DRAENEI )
	{
		Entry = 1826;
		strcpy( name, "|cffff6060A_HealBot" );
	}
	else
	{
		Entry = 5473;
		strcpy( name, "|cffff6060H_HealBot" );
	}

	CreatureProto * pTemplate = CreatureProtoStorage.LookupEntry(Entry);
	CreatureInfo * pCreatureInfo = CreatureNameStorage.LookupEntry(Entry);
	if(!pTemplate || !pCreatureInfo)
	{
		RedSystemMessage(m_session, "Invalid creature spawn template: %u", Entry);
		return true;
	}
	Player * plr = m_session->GetPlayer();

	// spawn a creature of this id to create from
	Creature * pCreature = new Creature(HIGHGUID_UNIT ,1);//no need in guid
	CreatureSpawn * sp = new CreatureSpawn;
	sp->id = 1;
	sp->bytes = 0;
	sp->bytes2 = 0;
	sp->displayid = pCreatureInfo->Male_DisplayID;
	sp->emote_state = 0;
	sp->entry = pCreatureInfo->Id;
	sp->factionid = pTemplate->Faction;
	sp->flags = 0;
	sp->form = 0;
	sp->movetype = 0;
	sp->o = plr->GetOrientation();
	sp->x = plr->GetPositionX();
	sp->y = plr->GetPositionY();
	sp->respawnNpcLink = 0;
	sp->channel_spell=sp->channel_target_creature=sp->channel_target_go=0;
	pCreature->Load(sp, (uint32)NULL, NULL);

	Pet *old_tame = plr->GetSummon();
	if(old_tame != NULL)
	{
		old_tame->Dismiss(true);
	}

	// create a pet from this creature
	Pet * pPet = objmgr.CreatePet();
	pPet->SetInstanceID(plr->GetInstanceID());
	pPet->SetMapId(plr->GetMapId());

	pPet->SetFloatValue ( OBJECT_FIELD_SCALE_X, pTemplate->Scale / 2); //we do not wish to block visualy other players
	AiAgentHealSupport *new_interface = new AiAgentHealSupport;
	pPet->ReplaceAIInterface( (AIInterface *) new_interface );
//	new_interface->Init(pPet,AITYPE_PET,MOVEMENTTYPE_NONE,plr); // i think this will get called automatically for pet

	pPet->CreateAsSummon(Entry, pCreatureInfo, pCreature, plr, NULL, 0x2, 0);

	pPet->Rename(name);

	//healer bot should not have any specific ations
	pPet->SetActionBarSlot(0,PET_SPELL_FOLLOW);
	pPet->SetActionBarSlot(1,PET_SPELL_STAY);
	pPet->SetActionBarSlot(2,0);
	pPet->SetActionBarSlot(3,0);
	pPet->SetActionBarSlot(4,0);
	pPet->SetActionBarSlot(5,0);
	pPet->SetActionBarSlot(6,0);
	pPet->SetActionBarSlot(7,0);
	pPet->SetActionBarSlot(8,0);
	pPet->SetActionBarSlot(9,0);
	pPet->SendSpellsToOwner();

	// remove the temp creature
	delete sp;
	delete pCreature;

	MyLog::gmlog->notice("used create an AI bot");
	return true;
}
#endif

bool ChatHandler::HandleAddPetSpellCommand(const char* args, WorldSession* m_session)
{
	return true;
	Player * plr = m_session->GetPlayer();
	Pet * pPet = plr->GetSummon();
	if(pPet == 0)
	{
		RedSystemMessage(m_session, "You have no pet.");
		return true;
	}

	uint32 SpellId = atol(args);
	SpellEntry * spell = dbcSpell.LookupEntry(SpellId);
	if(!SpellId || !spell)
	{
		RedSystemMessage(m_session, "Invalid spell id requested.");
		return true;
	}

	pPet->AddSpell(spell, true);
	GreenSystemMessage(m_session, "Added spell %u to your pet.", SpellId);
	return true;
}

bool ChatHandler::HandleRemovePetSpellCommand(const char* args, WorldSession* m_session)
{
	return true;
	Player * plr = m_session->GetPlayer();
	Pet * pPet = plr->GetSummon();
	if(pPet == 0)
	{
		RedSystemMessage(m_session, "You have no pet.");
		return true;
	}

	uint32 SpellId = atol(args);
	SpellEntry * spell = dbcSpell.LookupEntry(SpellId);
	if(!SpellId || !spell)
	{
		RedSystemMessage(m_session, "Invalid spell id requested.");
		return true;
	}

	pPet->RemoveSpell(SpellId);
	GreenSystemMessage(m_session, "Added spell %u to your pet.", SpellId);
	return true;
}

bool ChatHandler::HandleRenamePetCommand(const char* args, WorldSession* m_session)
{
	return true;
	Player * plr = m_session->GetPlayer();
	Pet * pPet = plr->GetSummon();
	if(pPet == 0)
	{
		RedSystemMessage(m_session, "You have no pet.");
		return true;
	}

	if(strlen(args) < 1)
	{
		RedSystemMessage(m_session, "You must specify a name.");
		return true;
	}

	GreenSystemMessage(m_session, "Renamed your pet to %s.", args);
	pPet->Rename(args);
	return true;
}

bool ChatHandler::HandleShutdownCommand(const char* args, WorldSession* m_session)
{
	//m_session->GetPlayer()->SafeTeleport( 7, 0, 1409.65, 1448.81, 37.5, 0);
	//m_session->GetPlayer()->SafeTeleport( 6, 0, 1200, 1200, 37.5, 0);
	//return false;
	uint32 shutdowntime = atol(args);
	if(!args)
		shutdowntime = 5;

	MyLog::yunyinglog->info("server shutdown by [%s]", m_session->GetPlayer()->GetName());

	sMaster.SetShutdownTimer( shutdowntime );
	return true;
}

bool ChatHandler::HandleShutdownRestartCommand(const char* args, WorldSession* m_session)
{
	return true;
	uint32 shutdowntime = atol(args);
	if(!args)
		shutdowntime = 5;

	char msg[500];
	snprintf(msg, 500, "%sServer restart initiated by %s, shutting down in %u seconds.", MSG_COLOR_LIGHTBLUE,
		m_session->GetPlayer()->GetName(), (unsigned int)shutdowntime);

	MyLog::gmlog->notice("initiated server restart timer %u sec", shutdowntime);
	sWorld.SendWorldText(msg);
		shutdowntime *= 1000;
	sMaster.m_ShutdownTimer = shutdowntime;
	sMaster.m_ShutdownEvent = true;
	sMaster.m_restartEvent = true;
	return true;
}

bool ChatHandler::HandleAllowWhispersCommand(const char* args, WorldSession* m_session)
{
	return true;
	if(args == 0 || strlen(args) < 2) return false;
	Player * plr = objmgr.GetPlayer(args, false);
	if(!plr)
	{
		RedSystemMessage(m_session, "Player not found.");
		return true;
	}

	m_session->GetPlayer()->gmTargets.insert(plr);
	BlueSystemMessage(m_session, "Now accepting whispers from %s.", plr->GetName());
	return true;
}

bool ChatHandler::HandleBlockWhispersCommand(const char* args, WorldSession* m_session)
{
	return true;
	if(args == 0 || strlen(args) < 2) return false;
	Player * plr = objmgr.GetPlayer(args, false);
	if(!plr)
	{
		RedSystemMessage(m_session, "Player not found.");
		return true;
	}

	m_session->GetPlayer()->gmTargets.erase(plr);
	BlueSystemMessage(m_session, "Now blocking whispers from %s.", plr->GetName());
	return true;
}

bool ChatHandler::HandleAdvanceAllSkillsCommand(const char* args, WorldSession* m_session)
{
	return true;
	uint32 amt = args ? atol(args) : 0;
	if(!amt)
	{
		RedSystemMessage(m_session, "An amount to increment is required.");
		return true;
	}

	Player * plr = getSelectedChar(m_session);
	if(!plr)
		return true;


	plr->_AdvanceAllSkills(amt);
	GreenSystemMessageToPlr(plr, "Advanced all your skill lines by %u points.", amt);
	MyLog::gmlog->notice("advanced all skills by %u on %s", amt, plr->GetName());
	return true;
}

bool ChatHandler::HandleKillByPlayerCommand(const char* args, WorldSession* m_session)
{
	return true;
	if(!args || strlen(args) < 2)
	{
		RedSystemMessage(m_session, "A player's name is required.");
		return true;
	}

	sWorld.DisconnectUsersWithPlayerName(args,m_session);
	return true;
}

bool ChatHandler::HandleKillBySessionCommand(const char* args, WorldSession* m_session)
{
	return true;
	if(!args || strlen(args) < 2)
	{
		RedSystemMessage(m_session, "A player's name is required.");
		return true;
	}

	sWorld.DisconnectUsersWithAccount(args,m_session);
	return true;
}
bool ChatHandler::HandleKillByIPCommand(const char* args, WorldSession* m_session)
{
	return true;
	if(!args || strlen(args) < 2)
	{
		RedSystemMessage(m_session, "An IP is required.");
		return true;
	}

	sWorld.DisconnectUsersWithIP(args,m_session);
	return true;
}

bool ChatHandler::HandleMassSummonCommand(const char* args, WorldSession* m_session)
{
	return true;


	PlayerStorageMap::const_iterator itr;
	Player * summoner = m_session->GetPlayer();
	Player * plr;
	uint32 c=0;
	for (itr = objmgr._players.begin(); itr != objmgr._players.end(); itr++)
	{
		plr = itr->second;
		if(plr->GetSession() && plr->IsInWorld() && plr != summoner)
		{
			//plr->SafeTeleport(summoner->GetMapId(), summoner->GetInstanceID(), summoner->GetPosition());
			/* let's do this the blizz way */
			//plr->SummonRequest(summoner->GetLowGUID(), summoner->GetZoneId(), summoner->GetMapId(), summoner->GetInstanceID(), summoner->GetPosition());
			if(plr->GetMapMgr()==summoner->GetMapMgr())
				plr->_Relocate(summoner->GetMapId(),summoner->GetPosition(),false,false,summoner->GetInstanceID());
			else
			{
				sEventMgr.AddEvent(plr,&Player::EventPortToGM,summoner,0,1,1,0);
			}
			++c;
		}
	}
	MyLog::gmlog->notice("requested a mass summon of %u players.", c);
	return true;
}

bool ChatHandler::HandleCastAllCommand(const char* args, WorldSession* m_session)
{
	return true;
	if(!args || strlen(args) < 2)
	{
		RedSystemMessage(m_session, "No spellid specified.");
		return true;
	}
	Player * plr;
	uint32 spellid = atol(args);
	SpellEntry * info = dbcSpell.LookupEntry(spellid);
	if(!info)
	{
		RedSystemMessage(m_session, "Invalid spell specified.");
		return true;
	}

	// this makes sure no moron casts a learn spell on everybody and wrecks the server
	for (int i = 0; i < 3; i++)
	{
		if (info->Effect[i] == 36) //SPELL_EFFECT_LEARN_SPELL - 36
		{
			MyLog::gmlog->notice("used wrong / learnall castall command, spellid %u", spellid);
			RedSystemMessage(m_session, "Learn spell specified.");
			return true;
		}
	}

	MyLog::gmlog->notice("used castall command, spellid %u", spellid);

	PlayerStorageMap::const_iterator itr;
	for (itr = objmgr._players.begin(); itr != objmgr._players.end(); itr++)
	{
		plr = itr->second;
		if(plr->GetSession() && plr->IsInWorld())
		{
			if(plr->GetMapMgr() != m_session->GetPlayer()->GetMapMgr())
			{
				sEventMgr.AddEvent( static_cast< Unit* >( plr ), &Unit::EventCastSpell, static_cast< Unit* >( plr ), info, EVENT_PLAYER_CHECKFORCHEATS, 100, 1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT );
			}
			else
			{
				Spell * sp = new Spell(plr, info, true, 0);
				SpellCastTargets targets(plr->GetGUID());
				sp->prepare(&targets);
			}
		}
	}

	BlueSystemMessage(m_session, "Casted spell %u on all players!", spellid);
	return true;
}

bool ChatHandler::HandleNpcReturnCommand(const char* args, WorldSession* m_session)
{
	Creature * creature = getSelectedCreature(m_session);
	if(!creature || !creature->m_spawn) return true;

	// return to respawn coords
	float x = creature->m_spawn->x;
	float y = creature->m_spawn->y;
	float z = creature->m_spawn->z;
	float o = creature->m_spawn->o;
	
	// restart movement
	creature->GetAIInterface()->SetAIState(STATE_IDLE);
	creature->GetAIInterface()->WipeHateList();
	creature->GetAIInterface()->WipeTargetList();
	creature->GetAIInterface()->MoveTo(x, y, z, o);

	return true;
}

bool ChatHandler::HandleModPeriodCommand(const char* args, WorldSession * m_session)
{
	return true;
	Transporter * trans = m_session->GetPlayer()->m_CurrentTransporter;
	if(trans == 0)
	{
		RedSystemMessage(m_session, "You must be on a transporter.");
		return true;
	}

	uint32 np = args ? atol(args) : 0;
	if(np == 0)
	{
		RedSystemMessage(m_session, "A time in ms must be specified.");
		return true;
	}

	trans->SetPeriod(np);
	BlueSystemMessage(m_session, "Period of %s set to %u.", trans->GetInfo()->Name, np);
	return true;
}

bool ChatHandler::HandleFormationLink1Command(const char* args, WorldSession * m_session)
{
	return true;
	// set formation "master"
	Creature * pCreature = getSelectedCreature(m_session, true);
	if(pCreature == 0) return true;

	m_session->GetPlayer()->linkTarget = pCreature;
	BlueSystemMessage(m_session, "Linkup \"master\" set to %s.", pCreature->GetCreatureName()->Name);
	return true;
}

bool ChatHandler::HandleFormationLink2Command(const char* args, WorldSession * m_session)
{
	return true;
	// set formation "slave" with distance and angle
	float ang, dist;
	if(*args == 0 || sscanf(args, "%f %f", &dist, &ang) != 2)
	{
		RedSystemMessage(m_session, "You must specify a distance and angle.");
		return true;
	}

	if(m_session->GetPlayer()->linkTarget == 0 || m_session->GetPlayer()->linkTarget->GetTypeId() != TYPEID_UNIT)
	{
		RedSystemMessage(m_session, "Master not selected. select the master, and use formationlink1.");
		return true;
	}

	Creature * slave = getSelectedCreature(m_session, true);
	if(slave == 0) return true;

	slave->GetAIInterface()->m_formationFollowDistance = dist;
	slave->GetAIInterface()->m_formationFollowAngle = ang;
	slave->GetAIInterface()->m_formationLinkTarget = static_cast< Creature* >( m_session->GetPlayer()->linkTarget );
	slave->GetAIInterface()->m_formationLinkSqlId = slave->GetAIInterface()->m_formationLinkTarget->GetSQL_id();
	slave->GetAIInterface()->SetUnitToFollowAngle(ang);
	
	// add to db
	WorldDatabase.WaitExecute("INSERT INTO creature_formations VALUES(%u, %u, '%f', '%f')", 
		slave->GetSQL_id(), slave->GetAIInterface()->m_formationLinkSqlId, ang, dist);

	BlueSystemMessage(m_session, "%s linked up to %s with a distance of %f at %f radians.", slave->GetCreatureName()->Name, 
		static_cast< Creature* >( m_session->GetPlayer()->linkTarget )->GetCreatureName()->Name, dist, ang );

	return true;
}

bool ChatHandler::HandleNpcFollowCommand(const char* args, WorldSession * m_session)
{
	Creature * creature = getSelectedCreature(m_session, true);
	if(!creature) return true;

	creature->GetAIInterface()->SetUnitToFollow(m_session->GetPlayer());
	return true;
}

bool ChatHandler::HandleFormationClearCommand(const char* args, WorldSession * m_session)
{
	return true;
	Creature * c = getSelectedCreature(m_session, true);
	if(!c) return true;

	c->GetAIInterface()->m_formationLinkSqlId = 0;
	c->GetAIInterface()->m_formationLinkTarget = 0;
	c->GetAIInterface()->m_formationFollowAngle = 0.0f;
	c->GetAIInterface()->m_formationFollowDistance = 0.0f;
	c->GetAIInterface()->SetUnitToFollow(0);
	
	WorldDatabase.WaitExecute("DELETE FROM creature_formations WHERE spawn_id=%u", c->GetSQL_id());
	return true;
}

bool ChatHandler::HandleNullFollowCommand(const char* args, WorldSession * m_session)
{
	Creature * c = getSelectedCreature(m_session, true);
	if(!c) return true;

	// restart movement
	c->GetAIInterface()->SetAIState(STATE_IDLE);
	c->GetAIInterface()->SetUnitToFollow(0);
	return true;
}

bool ChatHandler::HandleStackCheatCommand(const char* args, WorldSession * m_session)
{
	Player * plyr = getSelectedChar(m_session, true);
	if(!plyr) return true;

	bool val = plyr->stack_cheat;
	BlueSystemMessage(m_session, "%s aura stack cheat on %s.", val ? "Deactivating" : "Activating", plyr->GetName());
	GreenSystemMessageToPlr(plyr, "%s %s an aura stack cheat on you.", m_session->GetPlayer()->GetName(), val ? "deactivated" : "activated");

	plyr->stack_cheat = !val;
	MyLog::gmlog->notice("used stack cheat on %s", plyr->GetName());
	return true;
}

bool ChatHandler::HandleTriggerpassCheatCommand(const char* args, WorldSession * m_session)
{
	Player * plr = getSelectedChar(m_session, true);
	if (!plr)
		return true;

	bool val = plr->triggerpass_cheat;
	BlueSystemMessage(m_session, "%s areatrigger prerequisites immunity cheat on %s.", val ? "Deactivated" : "Activated", plr->GetName());
	GreenSystemMessageToPlr(plr, "%s %s areatrigger prerequisites immunity cheat on you.", m_session->GetPlayer()->GetName(), val ? "deactivated" : "activated");

	plr->triggerpass_cheat = !val;
	MyLog::gmlog->notice("used areatrigger cheat on %s", plr->GetName());
	return true;
}

bool ChatHandler::HandleResetSkillsCommand(const char* args, WorldSession * m_session)
{
	return true;
	skilllineentry * se;
	Player * plr = getSelectedChar(m_session, true);
	if(!plr) return true;

	plr->_RemoveAllSkills();

	// Load skills from create info.
	PlayerCreateInfo * info = objmgr.GetPlayerCreateInfo(plr->getRace(), plr->getClass());
	if(!info) return true;

	for(std::list<CreateInfo_SkillStruct>::iterator ss = info->skills.begin(); ss!=info->skills.end(); ss++)
	{
		se = dbcSkillLine.LookupEntry(ss->skillid);
		if(se->type != SKILL_TYPE_LANGUAGE && ss->skillid && ss->currentval && ss->maxval)
			plr->_AddSkillLine(ss->skillid, ss->currentval, ss->maxval);		
	}
	//Chances depend on stats must be in this order!
	plr->UpdateStats();
	plr->UpdateChances();
	plr->_UpdateMaxSkillCounts();
	BlueSystemMessage(m_session, "Reset skills to default.");
	MyLog::gmlog->notice("reset skills of %s to default", plr->GetName());
	return true;
}


bool ChatHandler::HandleGlobalPlaySoundCommand(const char* args, WorldSession * m_session)
{
	return true;
	if(!*args) return false;
	uint32 sound = atoi(args);
	if(!sound) return false;

	MSG_S2C::stSound_Play Msg;
	Msg.sound = sound;
	sWorld.SendGlobalMessage(Msg, 0);
	BlueSystemMessage(m_session, "Broadcasted sound %u to server.", sound);
	MyLog::gmlog->notice("used play all command soundid %u", sound);
	return true;
}

bool ChatHandler::HandleIPBanCommand(const char * args, WorldSession * m_session)
{
	return true;
	char * pIp = (char*)args;
	char * pDuration = strchr(pIp, ' ');
	if(pDuration == NULL)
		return false;
	*pDuration = 0;
	++pDuration;

	int32 timeperiod = GetTimePeriodFromString(pDuration);
	if(timeperiod < 1)
		return false;

	uint32 o1, o2, o3, o4;
	if ( sscanf(pIp, "%3u.%3u.%3u.%3u", (unsigned int*)&o1, (unsigned int*)&o2, (unsigned int*)&o3, (unsigned int*)&o4) != 4
			|| o1 > 255 || o2 > 255 || o3 > 255 || o4 > 255)
	{
		RedSystemMessage(m_session, "Invalid IPv4 address [%s]", pIp);
		return true;	// error in syntax, but we wont remind client of command usage
	}

	time_t expire_time;
	if ( timeperiod == 0)		// permanent ban
		expire_time = 0;
	else
		expire_time = UNIXTIME + (time_t)timeperiod;
	string IP = pIp;
	string::size_type i = IP.find("/");
	if (i == string::npos)
	{
		RedSystemMessage(m_session, "Lack of CIDR address assumes a 32bit match (if you don't understand, dont worry, it worked)");
		IP.append("/32");
		pIp = (char*)IP.c_str(); //is this correct? - optical
	}
	SystemMessage(m_session, "Adding [%s] to IP ban table, expires %s", pIp, (expire_time == 0)? "Never" : ctime( &expire_time ));
// 	sLogonCommHandler.IPBan_Add( pIp, (uint32)expire_time );
	sWorld.DisconnectUsersWithIP(IP.substr(0,IP.find("/")).c_str(), m_session);
	MyLog::gmlog->notice("banned ip address %s, expires %s", pIp, (expire_time == 0)? "Never" : ctime( &expire_time ));
	return true;
}

bool ChatHandler::HandleIPUnBanCommand(const char * args, WorldSession * m_session)
{
	return true;
	string pIp = args;
	if (pIp.length() == 0)
		return false;

	if (pIp.find("/") == string::npos)
	{
		RedSystemMessage(m_session, "Lack of CIDR address assumes a 32bit match (if you don't understand, dont worry, it worked)");
		pIp.append("/32");
	}
	/**
	 * We can afford to be less fussy with the validty of the IP address given since
	 * we are only attempting to remove it.
	 * Sadly, we can only blindly execute SQL statements on the logonserver so we have
	 * no idea if the address existed and so the account/IPBanner cache requires reloading.
	 */

	SystemMessage(m_session,"Deleting [%s] from ip ban table if it exists",pIp.c_str());
	//sLogonCommHandler.IPBan_Remove( pIp.c_str() );
	MyLog::gmlog->notice("unbanned ip address %s", pIp.c_str());
	return true;
}
bool ChatHandler::HandleCreatureSpawnCommand(const char *args, WorldSession *m_session)
{
	uint32 entry = atol(args);
	if(entry == 0)
		return false;

	CreatureProto * proto = CreatureProtoStorage.LookupEntry(entry);
	CreatureInfo * info = CreatureNameStorage.LookupEntry(entry);
	if(proto == 0 || info == 0)
	{
		RedSystemMessage(m_session, "Invalid entry id.");
		return true;
	}

	CreatureSpawn * sp = new CreatureSpawn;
	//sp->displayid = info->DisplayID;
	info->GenerateModelId(&sp->displayid);
	sp->entry = entry;
	sp->form = 0;
	sp->id = objmgr.GenerateCreatureSpawnID();
	sp->movetype = 0;
	sp->x = m_session->GetPlayer()->GetPositionX();
	sp->y = m_session->GetPlayer()->GetPositionY();
	sp->z = m_session->GetPlayer()->GetPositionZ();
	sp->o = m_session->GetPlayer()->GetOrientation();
	sp->emote_state =0;
	sp->flags = 0;
	sp->factionid = proto->Faction;
	sp->bytes=0;
	sp->bytes2=0;
	//sp->respawnNpcLink = 0;
	sp->stand_state = 0;
	sp->channel_spell=sp->channel_target_creature=sp->channel_target_go=0;


	Creature * p = m_session->GetPlayer()->GetMapMgr()->CreateCreature(entry);
	ASSERT(p);
	p->Load(sp, (uint32)NULL, NULL);
	p->m_noRespawn = true;
	p->PushToWorld(m_session->GetPlayer()->GetMapMgr());
	
	uint32 x = m_session->GetPlayer()->GetMapMgr()->GetPosX(m_session->GetPlayer()->GetPositionX());
	uint32 y = m_session->GetPlayer()->GetMapMgr()->GetPosY(m_session->GetPlayer()->GetPositionY());

	// Add spawn to map
// 	m_session->GetPlayer()->GetMapMgr()->GetBaseMap()->GetSpawnsListAndCreate(
// 		x,
// 		y)->CreatureSpawns.push_back(sp);

	BlueSystemMessage(m_session, "Spawned a creature `%s` with entry %u at %f %f %f on map %u", info->Name, 
		entry, sp->x, sp->y, sp->z, m_session->GetPlayer()->GetMapId());

	// Save it to the database.
	//p->SaveToDB();

	MyLog::gmlog->notice("spawned a %s at %u %f %f %f", info->Name, m_session->GetPlayer()->GetMapId(),sp->x,sp->y,sp->z);

	return true;
}

bool ChatHandler::HandleCreatureSpawnLineCommand(const char *args, WorldSession *m_session)
{
	return true ;
	uint32 entry;uint32 cnt;
	if ( sscanf(args, "%u %u", (unsigned int*)&entry, (unsigned int*)&cnt) != 2 )
	{
		RedSystemMessage(m_session, "Invalid parameter [%s]", args);
		return true;	// error in syntax, but we wont remind client of command usage
	}

	if(entry == 0)
		return false;

	CreatureProto * proto = CreatureProtoStorage.LookupEntry(entry);
	CreatureInfo * info = CreatureNameStorage.LookupEntry(entry);
	if(proto == 0 || info == 0)
	{
		RedSystemMessage(m_session, "Invalid entry id.");
		return true;
	}

	uint32 x = m_session->GetPlayer()->GetMapMgr()->GetPosX(m_session->GetPlayer()->GetPositionX());
	uint32 y = m_session->GetPlayer()->GetMapMgr()->GetPosY(m_session->GetPlayer()->GetPositionY());

	// Add spawn to map

	for(int i = 0; i < cnt; i++)
	{
		Creature * p = m_session->GetPlayer()->GetMapMgr()->CreateCreature(entry);
		ASSERT(p);

		float ori = -m_session->GetPlayer()->GetOrientation()-M_H_PI/*+M_PI*/;				
		float posX = m_session->GetPlayer()->GetPositionX()+(i*(cosf(ori)));
		float posY = m_session->GetPlayer()->GetPositionY()+(i*(sinf(ori)));
		if( posX >= _maxX || posX <= _minX ) continue;
		if( posY >= _maxY || posY <= _minY ) continue;

		CreatureSpawn * sp = new CreatureSpawn;
		//sp->displayid = info->DisplayID;
		info->GenerateModelId(&sp->displayid);
		sp->entry = entry;
		sp->form = 0;
		sp->id = objmgr.GenerateCreatureSpawnID();
		sp->movetype = 0;
		sp->x = posX;
		sp->y = posY;
		sp->z = m_session->GetPlayer()->GetPositionZ();
		sp->o = m_session->GetPlayer()->GetOrientation();
		sp->emote_state =0;
		sp->flags = 0;
		sp->factionid = proto->Faction;
		sp->bytes=0;
		sp->bytes2=0;
		//sp->respawnNpcLink = 0;
		sp->stand_state = 0;
		sp->channel_spell=sp->channel_target_creature=sp->channel_target_go=0;

		p->Load(sp, (uint32)NULL, NULL);
		p->m_noRespawn = true;
		p->PushToWorld(m_session->GetPlayer()->GetMapMgr());

		m_session->GetPlayer()->GetMapMgr()->GetBaseMap()->GetSpawnsListAndCreate(
			m_session->GetPlayer()->GetMapMgr()->GetPosX(posX),
			m_session->GetPlayer()->GetMapMgr()->GetPosY(posY))->CreatureSpawns.push_back(sp);
	}

	BlueSystemMessage(m_session, "Spawned a line creature `%s` with entry %u at on map %u", info->Name, 
		entry, m_session->GetPlayer()->GetMapId());

	// Save it to the database.
	//p->SaveToDB();

	MyLog::gmlog->notice("spawned a line %s at %u", info->Name, m_session->GetPlayer()->GetMapId());

	return true;
}
bool ChatHandler::HandleRemoveItemCommand(const char * args, WorldSession * m_session)
{
	uint32 item_id;
	int32 count, ocount;
	int argc = sscanf(args, "%u %u", (unsigned int*)&item_id, (unsigned int*)&count);
	if(argc == 1)
		count = 1;
	else if(argc != 2 || !count)
		return false;

	ocount = count;
	Player * plr = getSelectedChar(m_session, true);
	if(!plr) return true;

	// loop until they're all gone.
	int32 loop_count = 0;
	int32 start_count = plr->GetItemInterface()->GetItemCount(item_id, true);
	int32 start_count2 = start_count;
	if(count > start_count)
		count = start_count;

	while(start_count >= count && (count > 0) && loop_count < 20)	 // Prevent a loop here.
	{
		plr->GetItemInterface()->RemoveItemAmt(item_id, count);
		start_count2 = plr->GetItemInterface()->GetItemCount(item_id, true);
		count -= (start_count - start_count2);
		start_count = start_count2;
		++loop_count;
	}

	MyLog::gmlog->notice("used remove item id %u count %u from %s", item_id, ocount, plr->GetName());
	BlueSystemMessage(m_session, "Removing %u copies of item %u from %s's inventory.", ocount, item_id, plr->GetName());
	BlueSystemMessage(plr->GetSession(), "%s removed %u copies of item %u from your inventory.", m_session->GetPlayer()->GetName(), ocount, item_id);
	return true;
}

bool ChatHandler::HandleForceRenameCommand(const char * args, WorldSession * m_session)
{
	return true;
	// prevent buffer overflow
	if(strlen(args) > 100)
		return false;

	string tmp = string(args);
	PlayerInfo * pi = objmgr.GetPlayerInfoByName(tmp.c_str());
	if(pi == 0)
	{
		RedSystemMessage(m_session, "Player with that name not found.");
		return true;
	}

	Player * plr = objmgr.GetPlayer((uint32)pi->guid);
	if(plr == 0)
	{
		CharacterDatabase.WaitExecute("UPDATE characters SET forced_rename_pending = 1 WHERE guid = %u", (uint32)pi->guid);
	}
	else
	{
		plr->rename_pending = true;
		plr->SaveToDB(false);
		BlueSystemMessageToPlr(plr, "%s forced your character to be renamed next logon.", m_session->GetPlayer()->GetName());
	}

	CharacterDatabase.WaitExecute("INSERT INTO banned_names VALUES('%s')", CharacterDatabase.EscapeString(string(pi->name)).c_str());
	GreenSystemMessage(m_session, "Forcing %s to rename his character next logon.", args);
	MyLog::gmlog->notice("forced %s to rename his charater (%u)", pi->name, pi->guid);
	return true;
}

void SendHighlightedName(WorldSession * m_session, const char* full_name, string& lowercase_name, string& highlight, uint32 id, bool item)
{
	char message[1024];
	char start[50];
	start[0] = message[0] = 0;

	snprintf(start, 50, "%s %u: %s", item ? "Item" : "Creature", (unsigned int)id, MSG_COLOR_WHITE);

	string::size_type hlen = highlight.length();
	string fullname = string(full_name);
	string::size_type offset = lowercase_name.find(highlight);
	string::size_type remaining = fullname.size() - offset - hlen;
	strcat(message, start);
	strncat(message, fullname.c_str(), offset);
	if(remaining > 0)
	{
		strcat(message, MSG_COLOR_LIGHTRED);
		strncat(message, (fullname.c_str() + offset), hlen);
		strcat(message, MSG_COLOR_WHITE);
		strncat(message, (fullname.c_str() + offset + hlen), remaining);
	}

	sChatHandler.SystemMessage(m_session, message);
}

bool ChatHandler::HandleLookupItemCommand(const char * args, WorldSession * m_session)
{
	if(!*args) return false;

	string x = string(args);
	ASCENT_TOLOWER(x);
	if(x.length() < 4)
	{
		RedSystemMessage(m_session, "Your search string must be at least 5 characters long.");
		return true;
	}

	StorageContainerIterator<ItemPrototype> * itr = ItemPrototypeStorage.MakeIterator();

	BlueSystemMessage(m_session, "Starting search of item `%s`...", x.c_str());
	uint32 t = getMSTime();
	ItemPrototype * it;
	uint32 count = 0;
	while(!itr->AtEnd())
	{
		it = itr->Get();
		if(FindXinYString(x, it->lowercase_name))
		{
			// Print out the name in a cool highlighted fashion
			SendHighlightedName(m_session, Item::BuildStringWithProto(it), it->lowercase_name, x, it->ItemId, true);
			++count;
			if(count == 25)
			{
				RedSystemMessage(m_session, "More than 25 results returned. aborting.");
				break;
			}
		}
		
		if(!itr->Inc())
			break;
	}
	itr->Destruct();

	BlueSystemMessage(m_session, "Search completed in %u ms.", getMSTime() - t);
	return true;
}

bool ChatHandler::HandleLookupCreatureCommand(const char * args, WorldSession * m_session)
{
	if(!*args) return false;

	string x = string(args);
	ASCENT_TOLOWER(x);
	if(x.length() < 4)
	{
		RedSystemMessage(m_session, "Your search string must be at least 5 characters long.");
		return true;
	}

	StorageContainerIterator<CreatureInfo> * itr = CreatureNameStorage.MakeIterator();

	GreenSystemMessage(m_session, "Starting search of creature `%s`...", x.c_str());
	uint32 t = getMSTime();
	CreatureInfo * i;
	uint32 count = 0;
	while(!itr->AtEnd())
	{
		i = itr->Get();
		if(FindXinYString(x, i->lowercase_name))
		{
			// Print out the name in a cool highlighted fashion
			SendHighlightedName(m_session, i->Name, i->lowercase_name, x, i->Id, false);

			++count;
			if(count == 25)
			{
				RedSystemMessage(m_session, "More than 25 results returned. aborting.");
				break;
			}
		}
		if(!itr->Inc())
			break;
	}
	itr->Destruct();

	GreenSystemMessage(m_session, "Search completed in %u ms.", getMSTime() - t);
	return true;
}

bool ChatHandler::HandleNPCFlagCommand(const char* args, WorldSession *m_session)
{
	if (!*args)
		return false;

	uint32 npcFlags = (uint32) atoi((char*)args);

	uint64 guid = m_session->GetPlayer()->GetSelection();
	if (guid == 0)
	{
		SystemMessage(m_session, "No selection.");
		return true;
	}

	Creature * pCreature = m_session->GetPlayer()->GetMapMgr()->GetCreature(GET_LOWGUID_PART(guid));
	if(!pCreature)
	{
		SystemMessage(m_session, "You should select a creature.");
		return true;
	}

	pCreature->SetUInt32Value(UNIT_NPC_FLAGS , npcFlags);
	//pCreature->SaveToDB();
	SystemMessage(m_session, "Value saved, you may need to rejoin or clean your client cache.");

	return true;
}




bool ChatHandler::HandleItemRemoveCommand(const char* args, WorldSession *m_session)
{
	return false;
	/*
	char* iguid = strtok((char*)args, " ");
	if (!iguid)
		return false;

	uint64 guid = m_session->GetPlayer()->GetSelection();
	if (guid == 0)
	{
		SystemMessage(m_session, "No selection.");
		return true;
	}

	Creature * pCreature = m_session->GetPlayer()->GetMapMgr()->GetCreature(GET_LOWGUID_PART(guid));
	if(!pCreature)
	{
		SystemMessage(m_session, "You should select a creature.");
		return true;
	}

	uint32 itemguid = atoi(iguid);
	int slot = pCreature->GetSlotByItemId(itemguid);

	std::stringstream sstext;
	if(slot != -1)
	{
		uint32 guidlow = GUID_LOPART(guid);

		std::stringstream ss;
		ss << "DELETE FROM vendors WHERE entry = " << guidlow << " AND item = " << itemguid << '\0';
		WorldDatabase.WaitExecute( ss.str().c_str() );

		pCreature->RemoveVendorItem(itemguid);
		ItemPrototype* tmpItem = ItemPrototypeStorage.LookupEntry(itemguid);
		if(tmpItem)
		{
			sstext << "Item '" << itemguid << "' '" << tmpItem->Name1 << "' Deleted from list" << '\0';
		}
		else
		{
			sstext << "Item '" << itemguid << "' Deleted from list" << '\0';
		}

	}
	else
	{
		sstext << "Item '" << itemguid << "' Not Found in List." << '\0';
	}

	SystemMessage(m_session, sstext.str().c_str());

	return true;
	*/
}

bool ChatHandler::HandleItemCommand(const char* args, WorldSession *m_session)
{
	return false;
	/*
	char* pitem = strtok((char*)args, " ");
	if (!pitem)
		return false;

	uint64 guid = m_session->GetPlayer()->GetSelection();
	if (guid == 0)
	{
		SystemMessage(m_session, "No selection.");
		return true;
	}

	Creature * pCreature = m_session->GetPlayer()->GetMapMgr()->GetCreature(GET_LOWGUID_PART(guid));
	if(!pCreature)
	{
		SystemMessage(m_session, "You should select a creature.");
		return true;
	}

	uint32 item = atoi(pitem);
	int amount = -1;

	char* pamount = strtok(NULL, " ");
	if (pamount)
		amount = atoi(pamount);

	ItemPrototype* tmpItem = ItemPrototypeStorage.LookupEntry(item);

	std::stringstream sstext;
	if(tmpItem)
	{
		std::stringstream ss;
		ss << "INSERT INTO vendors VALUES ('" << pCreature->GetUInt32Value(OBJECT_FIELD_ENTRY) << "', '" << item << "', '" << amount << "', 0, 0 )" << '\0';
		WorldDatabase.WaitExecute( ss.str().c_str() );

		pCreature->AddVendorItem(item, amount);

		sstext << "Item '" << item << "' '" << tmpItem->Name1 << "' Added to list" << '\0';
	}
	else
	{
		sstext << "Item '" << item << "' Not Found in Database." << '\0';
	}

	MyLog::gmlog->notice("added item %u to vendor %u", pCreature->GetEntry(), item);
	SystemMessage(m_session,  sstext.str().c_str());

	return true;
	*/
}

bool ChatHandler::HandleGORotate(const char * args, WorldSession * m_session)
{
	return true;
	GameObject *go = m_session->GetPlayer()->m_GM_SelectedGO;
	if( !go )
	{
		RedSystemMessage(m_session, "No selected GameObject...");
		return true;
	}

	float deg = (float)atof(args);
	if(deg == 0.0f)
		return false;

	// Convert the argument to radians
	float rad = deg * (float(M_PI) / 180.0f);

	// let's try rotation_0
	go->ModFloatValue(GAMEOBJECT_ROTATION, rad);
	go->ModFloatValue(GAMEOBJECT_ROTATION_01, rad);
	go->ModFloatValue(GAMEOBJECT_ROTATION_02, rad);
	go->ModFloatValue(GAMEOBJECT_ROTATION_03, rad);
	//go->SaveToDB();

	// despawn and respawn
	//go->Despawn(1000);
	go->RemoveFromWorld(true);
	go->SetNewGuid(m_session->GetPlayer()->GetMapMgr()->GenerateGameobjectGuid());
	go->PushToWorld(m_session->GetPlayer()->GetMapMgr());
	return true;
}

bool ChatHandler::HandleGOMove(const char * args, WorldSession * m_session)
{
	return true;
	// move the go to player's coordinates
	GameObject *go = m_session->GetPlayer()->m_GM_SelectedGO;
	if( !go )
	{
		RedSystemMessage(m_session, "No selected GameObject...");
		return true;
	}

	go->RemoveFromWorld(true);
	go->SetPosition(m_session->GetPlayer()->GetPosition());
	go->SetFloatValue(GAMEOBJECT_POS_X, m_session->GetPlayer()->GetPositionX());
	go->SetFloatValue(GAMEOBJECT_POS_Y, m_session->GetPlayer()->GetPositionY());
	go->SetFloatValue(GAMEOBJECT_POS_Z, m_session->GetPlayer()->GetPositionZ());
	go->SetFloatValue(GAMEOBJECT_FACING, m_session->GetPlayer()->GetOrientation());
	go->SetNewGuid(m_session->GetPlayer()->GetMapMgr()->GenerateGameobjectGuid());
	//go->SaveToDB();
	go->PushToWorld(m_session->GetPlayer()->GetMapMgr());
	return true;
}

bool ChatHandler::HandleNpcPossessCommand(const char * args, WorldSession * m_session)
{
	return true ;
	Unit * pTarget = getSelectedChar(m_session, false);
	if(!pTarget)
	{
		pTarget = getSelectedCreature(m_session, false);
		if(pTarget && (pTarget->IsPet() || pTarget->GetUInt32Value(UNIT_FIELD_CREATEDBY) != 0))
			return false;
	}
		
	if(!pTarget)
	{
		RedSystemMessage(m_session, "You must select a player/creature.");
		return true;
	}

	m_session->GetPlayer()->Possess(pTarget);
	BlueSystemMessage(m_session, "Possessed "I64FMT, pTarget->GetGUID());
	return true;
}

bool ChatHandler::HandleNpcUnPossessCommand(const char * args, WorldSession * m_session)
{
	return true ;
	Creature * creature = getSelectedCreature(m_session);
 	m_session->GetPlayer()->UnPossess();

	if(creature)
	{
			// restart movement
			creature->GetAIInterface()->SetAIState(STATE_IDLE);
			creature->GetAIInterface()->WipeHateList();
			creature->GetAIInterface()->WipeTargetList();

			if(creature->m_spawn)
			{
				// return to respawn coords
				float x = creature->m_spawn->x;
				float y = creature->m_spawn->y;
				float z = creature->m_spawn->z;
				float o = creature->m_spawn->o;
				creature->GetAIInterface()->MoveTo(x, y, z, o);
			}
	}
	GreenSystemMessage(m_session, "Removed any possessed targets.");
	return true;
}

bool ChatHandler::HandleRehashCommand(const char * args, WorldSession * m_session)
{
	return true;
	/* rehashes */
	char msg[250];
	snprintf(msg, 250, "%s is rehashing config file.", m_session->GetPlayer()->GetName());
	sWorld.SendWorldWideScreenText(msg, 0);
	sWorld.SendWorldText(msg, 0);
	sWorld.Rehash(true);
	return true;
}

struct spell_thingo
{
	uint32 type;
	uint32 target;
};

list<SpellEntry*> aiagent_spells;
map<uint32, spell_thingo> aiagent_extra;

bool ChatHandler::HandleAIAgentDebugBegin(const char * args, WorldSession * m_session)
{
	return true;

	QueryResult * result = WorldDatabase.Query("SELECT DISTINCT spell FROM ai_agents");
	if(!result) return false;

	do 
	{
		SpellEntry * se = dbcSpell.LookupEntry(result->Fetch()[0].GetUInt32());
		if(se)
			aiagent_spells.push_back(se);
	} while(result->NextRow());
	delete result;

	for(list<SpellEntry*>::iterator itr = aiagent_spells.begin(); itr != aiagent_spells.end(); ++itr)
	{
		result = WorldDatabase.Query("SELECT * FROM ai_agents WHERE spell = %u", (*itr)->Id);
		if( result )
		{
			spell_thingo t;
			t.type = result->Fetch()[6].GetUInt32();
			t.target = result->Fetch()[7].GetUInt32();
			delete result;
			aiagent_extra[(*itr)->Id] = t;
		}
		else
			ASSERT(result);
	}

	GreenSystemMessage(m_session, "Loaded %u spells for testing.", aiagent_spells.size());
	return true;
}

SpellCastTargets SetTargets(SpellEntry * sp, uint32 type, uint32 targettype, Unit * dst, Creature * src)
{
	SpellCastTargets targets;
	targets.m_unitTarget = 0;
	targets.m_itemTarget = 0;
	targets.m_srcX = 0;
	targets.m_srcY = 0;
	targets.m_srcZ = 0;
	targets.m_destX = 0;
	targets.m_destY = 0;
	targets.m_destZ = 0;

	if(targettype == TTYPE_ENMYSINGLETARGERT || targettype == TTYPE_FRIENDSINGLETARGET)
	{
		targets.m_targetMask = 2;
		targets.m_unitTarget = dst->GetGUID();
	}
	else if(targettype == TTYPE_SOURCE)
	{
		targets.m_targetMask = 32;
		targets.m_srcX = src->GetPositionX();
		targets.m_srcY = src->GetPositionY();
		targets.m_srcZ = src->GetPositionZ();
	}
	else if(targettype == TTYPE_ENMYDESTINATION || targettype == TTYPE_FRIENDDESTINATION)
	{
		targets.m_targetMask = 64;
		targets.m_destX = dst->GetPositionX();
		targets.m_destY = dst->GetPositionY();
		targets.m_destZ = dst->GetPositionZ();
	}

	return targets;
};

bool ChatHandler::HandleAIAgentDebugContinue(const char * args, WorldSession * m_session)
{
	return true;

	uint32 count = atoi(args);
	if(!count) return false;

	Creature * pCreature = getSelectedCreature(m_session, true);
	if(!pCreature) return true;

	Player * pPlayer = m_session->GetPlayer();

	for(uint32 i = 0; i < count; ++i)
	{
		if(!aiagent_spells.size())
			break;

		SpellEntry * sp = *aiagent_spells.begin();
		aiagent_spells.erase(aiagent_spells.begin());
		BlueSystemMessage(m_session, "Casting %u, "MSG_COLOR_SUBWHITE"%u remaining.", sp->Id, aiagent_spells.size());

		map<uint32, spell_thingo>::iterator it = aiagent_extra.find(sp->Id);
		ASSERT(it != aiagent_extra.end());

		SpellCastTargets targets;
		if(it->second.type == STYPE_BUFF)
			targets = SetTargets(sp, it->second.type, it->second.type, pCreature, pCreature );
		else
			targets = SetTargets(sp, it->second.type, it->second.type, pPlayer, pCreature );

		pCreature->GetAIInterface()->CastSpell(pCreature, sp, targets);
	}

	if(!aiagent_spells.size())
		RedSystemMessage(m_session, "Finished.");
	/*else
		BlueSystemMessage(m_session, "Got %u remaining.", aiagent_spells.size());*/
	return true;
}

bool ChatHandler::HandleAIAgentDebugSkip(const char * args, WorldSession * m_session)
{
	return true;

	uint32 count = atoi(args);
	if(!count) return false;

	for(uint32 i = 0; i < count; ++i)
	{
		if(!aiagent_spells.size())
			break;

		aiagent_spells.erase(aiagent_spells.begin());
	}
	BlueSystemMessage(m_session, "Erased %u spells.", count);
	return true;
}

bool ChatHandler::HandleRenameGuildCommand(const char* args, WorldSession *m_session)
{
	return true ;
	Player * plr = getSelectedChar(m_session);
	if(!plr || !plr->GetGuildId() || !args || !strlen(args)) return false;

	return true;
}

bool ChatHandler::HandleGuildAddScoreCommand(const char* args, WorldSession *m_session)
{
	Player * plr = getSelectedChar(m_session);
	if(!plr)
		plr = m_session->GetPlayer();
	if(!plr->GetGuildId() || !args || !strlen(args)) return false;

	uint32 score;
	if(sscanf(args, "%u", &score) != 1)
	{
		SystemMessage(m_session, "参数错误.");
		return true;
	}

	plr->GetGuild()->AddGuildScore( score );
	plr->GetGuild()->AddGuildHiddenScore(score);

	return true;
}

//People seem to get stuck in guilds from time to time. This should be helpfull. -DGM
bool ChatHandler::HandleGuildRemovePlayerCommand(const char* args, WorldSession *m_session)
{
	return true ;
	Player * plr = getSelectedChar(m_session);
	if(!plr || !plr->GetGuildId()) return false;
	return true;
}

//-DGM
bool ChatHandler::HandleGuildDisbandCommand(const char* args, WorldSession *m_session)
{
	return true ;
	Player * plr = getSelectedChar(m_session);
	if(!plr || !plr->GetGuildId() || !args || !strlen(args)) return false;
	plr->GetGuild()->Disband();
	return true;
}

bool ChatHandler::HandleGuildSetCastleTime(const char* args, WorldSession *m_session)
{
	return true ;
	if( !args || !strlen(args)) return false;
	std::set<uint32> s;
	uint32 tmp[7] = { 0 };
	uint32 mapid = 0;
	int n = sscanf( args, "%u %u %u %u %u %u %u %u %u", &mapid, &tmp[0], &tmp[1], &tmp[2], &tmp[3], &tmp[4], &tmp[5], &tmp[6] );
	if( n <= 0 || n > 8 ) return false;
	for( int i = 0; i < n - 1; ++i )
	{
		s.insert( tmp[i] );
	}
	SunyouCastle* castle = g_instancequeuemgr.GetCastleByMapID( mapid );
	if( castle )
	{
		castle->SetFightDayOfWeek( s );
	}
	return true;
}

//-DGM
bool ChatHandler::HandleGuildMembersCommand(const char* args, WorldSession *m_session)
{
	return true ;
	Player * plr = getSelectedChar(m_session);
	if(!plr || !plr->GetGuildId()) return false;
	return true;
}

bool ChatHandler::HandleCreateArenaTeamCommands(const char * args, WorldSession * m_session)
{
	return true;
	uint32 arena_team_type;
	char name[1000];
	uint32 real_type;
	Player * plr = getSelectedChar(m_session, true);
	if(sscanf(args, "%u %s", &arena_team_type, name) != 2)
	{
		SystemMessage(m_session, "Invalid syntax.");
		return true;
	}

	switch(arena_team_type)
	{
	case 2:
		real_type=0;
		break;
	case 3:
		real_type=1;
		break;
	case 5:
		real_type=2;
		break;
	default:
		SystemMessage(m_session, "Invalid arena team type specified.");
		return true;
	}

	if(!plr)
		return true;

	if(plr->m_arenaTeams[real_type] != NULL)
	{
		SystemMessage(m_session, "Already in an arena team of that type.");
		return true;
	}

	ArenaTeam * t = new ArenaTeam(real_type,objmgr.GenerateArenaTeamId());
	t->m_emblemStyle=22;
	t->m_emblemColour=4292133532UL;
	t->m_borderColour=4294931722UL;
	t->m_borderStyle=1;
	t->m_backgroundColour=4284906803UL;
	t->m_leader=plr->GetLowGUID();
	t->m_name = string(name);
	t->AddMember(plr->m_playerInfo);
	objmgr.AddArenaTeam(t);
	SystemMessage(m_session, "created arena team.");
	return true;
}

bool ChatHandler::HandleWhisperBlockCommand(const char * args, WorldSession * m_session)
{
	return true;
	if(m_session->GetPlayer()->bGMTagOn)
		return false;

	m_session->GetPlayer()->bGMTagOn = true;
	return true;
}

bool ChatHandler::HandleGenderChanger(const char* args, WorldSession *m_session)
{
	int gender;
	Player* target = objmgr.GetPlayer((uint32)m_session->GetPlayer()->GetSelection());
	if(!target) {
		SystemMessage(m_session, "Select A Player first.");
		return true;
	}
	if (!*args)
	{
		if (target->getGender()== 1)
			gender = 0;
		else
			gender = 1;
	}
	else
		gender = min((int)atoi((char*)args),1);
	target->setGender(gender);
	SystemMessage(m_session, "Gender changed to %u",gender);
	return true;
}

bool ChatHandler::HandleRaceChanger(const char* args, WorldSession *m_session)
{
	int race;
	Player* target = objmgr.GetPlayer((uint32)m_session->GetPlayer()->GetSelection());
	if(!target) {
		SystemMessage(m_session, "Select A Player first.");
		return true;
	}
	
	race = atoi((char*)args);
	if(race <= RACE_MIN || race >= RACE_MAX)
	{
		SystemMessage(m_session, "race %d is not valid",race);
		return true;
	}
	target->setRace(race);
	SystemMessage(m_session, "race changed to %u",race);
	return true;
}

bool ChatHandler::HandleClassChanger(const char* args, WorldSession *m_session)
{
	int nClass;
	Player* target = objmgr.GetPlayer((uint32)m_session->GetPlayer()->GetSelection());
	if(!target) {
		SystemMessage(m_session, "Select A Player first.");
		return true;
	}

	nClass = atoi((char*)args);
	if(nClass <= CLASS_MIN || nClass >= CLASS_MAX)
	{
		SystemMessage(m_session, "class %d is not valid",nClass);
		return true;
	}
	target->setClass(nClass);
	SystemMessage(m_session, "class changed to %u",nClass);
	return true;
}

bool ChatHandler::HandleDispelAllCommand(const char * args, WorldSession * m_session)
{
	return true;
	uint32 pos=0;
	if(*args)
		pos=atoi(args);

	Player * plr;

	MyLog::gmlog->notice("used dispelall command, pos %u", pos);

	PlayerStorageMap::const_iterator itr;
	for (itr = objmgr._players.begin(); itr != objmgr._players.end(); itr++)
	{
		plr = itr->second;
		if(plr->GetSession() && plr->IsInWorld())
		{
			if(plr->GetMapMgr() != m_session->GetPlayer()->GetMapMgr())
			{
				sEventMgr.AddEvent( static_cast< Unit* >( plr ), &Unit::DispelAll, pos ? true : false, EVENT_PLAYER_CHECKFORCHEATS, 100, 1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT );
			}
			else
			{
				plr->DispelAll(pos?true:false);
			}
		}
	}
	MyLog::gmlog->notice("used mass dispel");

	BlueSystemMessage(m_session, "Dispel action done.");
	return true;
}

bool ChatHandler::HandleShowItems(const char * args, WorldSession * m_session)
{
	// fatal error
	/*
	Player * plr = getSelectedChar(m_session, true);
	if(!plr) return true;

	ItemIterator itr(plr->GetItemInterface());
	itr.BeginSearch();
	for(; !itr.End(); itr.Increment())	// this will be endless loop
	{
		SystemMessage(m_session, "Item %s count %u", (*itr)->BuildString(), (*itr)->GetUInt32Value(ITEM_FIELD_STACK_COUNT));
	}
	itr.EndSearch();

	SkillIterator itr2(plr);
	itr2.BeginSearch();
	for(; !itr2.End(); itr2.Increment())
	{
		SystemMessage(m_session, "Skill %u %u/%u", itr2->Skill->id, itr2->CurrentValue, itr2->MaximumValue);
	}
	itr2.EndSearch();
	*/
	return true;
}

bool ChatHandler::HandleCollisionTestIndoor(const char * args, WorldSession * m_session)
{
	/*
#ifdef COLLISION
	Player * plr = m_session->GetPlayer();
	const LocationVector & loc = plr->GetPosition();
	bool res = CollideInterface.IsIndoor(plr->GetMapIDForCollision(), loc.x, loc.y, loc.z + 2.0f);
	SystemMessage(m_session, "Result was: %s.", res ? "indoors" : "outside");
	return true;
#else
	*/

	SystemMessage(m_session, "not compiled with collision support.");
	return true;
//#endif
}

bool ChatHandler::HandleCollisionTestLOS(const char * args, WorldSession * m_session)
{
#ifdef COLLISION
	Object * pObj = NULL;
	Creature * pCreature = getSelectedCreature(m_session, false);
	Player * pPlayer = getSelectedChar(m_session, false);
	if(pCreature)
		pObj = pCreature;
	else if(pPlayer)
		pObj = pPlayer;

	if(pObj == NULL)
	{
		SystemMessage(m_session, "Invalid target.");
		return true;
	}

	const LocationVector & loc2 = pObj->GetPosition();
	const LocationVector & loc1 = m_session->GetPlayer()->GetPosition();
	bool res = CollideInterface.CheckLOS(pObj->GetMapIDForCollision(), loc1.x, loc1.y, loc1.z, loc2.x, loc2.y, loc2.z);
	bool res2 = CollideInterface.CheckLOS(pObj->GetMapIDForCollision(), loc1.x, loc1.y, loc1.z+2.0f, loc2.x, loc2.y, loc2.z+2.0f);
	bool res3 = CollideInterface.CheckLOS(pObj->GetMapIDForCollision(), loc1.x, loc1.y, loc1.z+5.0f, loc2.x, loc2.y, loc2.z+5.0f);
	SystemMessage(m_session, "Result was: %s %s %s.", res ? "in LOS" : "not in LOS", res2 ? "in LOS" : "not in LOS", res3 ? "in LOS" : "not in LOS");
	return true;
#else
	SystemMessage(m_session, "not compiled with collision support.");
	return true;
#endif
}

bool ChatHandler::HandleCollisionGetHeight(const char * args, WorldSession * m_session)
{
#ifdef COLLISION
	Player * plr = m_session->GetPlayer();
	float z = CollideInterface.GetHeight(plr->GetMapMgr()->GetBaseMap(), plr->GetPositionX(), plr->GetPositionY(), plr->GetPositionZ() + 2.0f);
	float z2 = CollideInterface.GetHeight(plr->GetMapMgr()->GetBaseMap(), plr->GetPositionX(), plr->GetPositionY(), plr->GetPositionZ() + 5.0f);
	float z3 = CollideInterface.GetHeight(plr->GetMapMgr()->GetBaseMap(), plr->GetPositionX(), plr->GetPositionY(), plr->GetPositionZ());

	SystemMessage(m_session, "Results were: %f %f %f", z, z2, z3);
	return true;
#else
	SystemMessage(m_session, "not compiled with collision support.");
	return true;
#endif
}
bool ChatHandler::HandleLevelUpCommand(const char* args, WorldSession *m_session)
{
	int levels = 0;

	if (!*args)
		levels = 1;
	else
		levels = atoi(args);

	if(levels <= 0)
		return false;

	Player *plr = getSelectedChar(m_session, true);

	if(!plr) plr = m_session->GetPlayer();

	if(!plr) return false;

	MyLog::gmlog->notice("used level up command on %s, with %u levels", plr->GetName(), levels);

	levels += plr->getLevel();

	if(levels>100)
		levels=100;

	LevelInfo * inf = objmgr.GetLevelInfo(plr->getRace(),plr->getClass(),levels);
	if(!inf)
		return false;
	plr->ApplyLevelInfo(inf,levels);

	std::stringstream sstext;
	sstext << "You have been leveled up to Level " << levels << '\0';
	SystemMessage(plr->GetSession(), sstext.str().c_str());

	plr->Social_TellFriendsOnline();

	return true;
}

bool ChatHandler::HandleFixScaleCommand(const char * args, WorldSession * m_session)
{
	Creature * pCreature = getSelectedCreature(m_session, true);
	if( pCreature == NULL )
		return true;

	float sc = (float)atof(args);
	if(sc < 0.1f)
	{
		return false;
	}

	pCreature->SetFloatValue(OBJECT_FIELD_SCALE_X, sc);
	pCreature->proto->Scale = sc;
	WorldDatabase.WaitExecute("UPDATE creature_proto SET scale = '%f' WHERE entry = %u", sc, pCreature->GetEntry());
	return true;
}

bool ChatHandler::HandleAddTrainerSpellCommand( const char * args, WorldSession * m_session )
{
	return true;
	Creature * pCreature = getSelectedCreature(m_session, true);
	if( pCreature == NULL )
		return true;

	uint32 spellid, cost, reqspell, reqlevel, delspell;
	if( sscanf(args, "%u %u %u %u %u", &spellid, &cost, &reqspell, &reqlevel, &delspell) != 5 )
		return false;

	Trainer * pTrainer =  pCreature->GetTrainer();
	if( pTrainer == NULL )
	{
		RedSystemMessage(m_session, "Target is not a trainer.");
		return true;
	}

	SpellEntry* pSpell = dbcSpell.LookupEntry(spellid);
	if(pSpell==NULL)
	{
		RedSystemMessage(m_session, "Invalid spell.");
		return true;
	}

	if( pSpell->Effect[0] == SPELL_EFFECT_INSTANT_KILL || pSpell->Effect[1] == SPELL_EFFECT_INSTANT_KILL || pSpell->Effect[2] == SPELL_EFFECT_INSTANT_KILL )
	{
		RedSystemMessage(m_session, "No. You're not doing that.");
		return true;
	}

	TrainerSpell sp;
	sp.Cost = cost;
	sp.IsProfession = false;
	sp.pLearnSpell = pSpell;
	sp.pCastRealSpell = NULL;
	sp.pCastSpell = NULL;
	sp.RequiredLevel = reqlevel;
	sp.RequiredSpell = reqspell;
	sp.DeleteSpell = delspell;

	pTrainer->Spells.push_back(sp);
	pTrainer->SpellCount++;
	
	SystemMessage(m_session, "Added spell %u (%s) to trainer.", pSpell->Id, pSpell->Name);
	MyLog::gmlog->notice("added spell %u to trainer %u", spellid, pCreature->GetEntry());
	WorldDatabase.WaitExecute("INSERT INTO trainer_spells VALUES(%u, %u, %u, %u, %u, %u, %u, %u, %u, %u)", 
		pCreature->GetEntry(), (int)0, pSpell->Id, cost, reqspell, (int)0, (int)0, reqlevel, delspell, (int)0);

	return true;
}
