#include "StdAfx.h"
#include "LfgMgr.h"

uint32 LfgDungeonTypes[MAX_DUNGEONS];
initialiseSingleton( LfgMgr );

LfgMgr::LfgMgr()
{
	StorageContainerIterator<LfgDungeonType> * itr = dbcLfgDungeonTypes.MakeIterator();
	LfgDungeonType* lfg;
	while(!itr->AtEnd())
	{
		lfg = itr->Get();
		uint32 id = lfg->id;
		uint32 typ = lfg->type;

		if(id >= MAX_DUNGEONS)
			printf("!! warning: LFGDungeons contains an out of range dungeon id %u.\n", id);
		else
			LfgDungeonTypes[id] = typ;

		if(!itr->Inc())
			break;
	}
	itr->Destruct();
}

LfgMgr::~LfgMgr()
{
	
	
}

bool LfgMgr::AttemptLfgJoin(Player * pl, uint32 LfgDungeonId)
{
	if( pl == NULL )
		return false;

	if( !pl->IsInWorld() )
		return false;

	// if the player has autojoin enabled,
	// search for any groups that have auto add members enabled, also have this dungeon, and add him
	// if one is found.
	/*LfgPlayerList itr;
	Player * plr;

	// make sure the dungeon is usable by autojoin (should never be true)
	if(LfgDungeonTypes[LfgDungeonId] != LFG_INSTANCE && LfgDungeonTypes[LfgDungeonId] != LFG_HEROIC_DUNGEON)
		return false;


	for(itr = m_lookingForGroup[LfgDungeonId].begin(); itr != m_lookingForGroup[LfgDungeonId].end(); ++itr) {
		plr = *itr;
		if(plr->m_AutoAddMem) {
			if(plr->GetGroup() && !plr->GetGroup()->IsFull() && plr->GetGroup()->GetGroupType() == GROUP_TYPE_PARTY) {
				plr->GetGroup()->AddMember(pl->m_playerInfo);
				pl->SendMeetingStoneQueue(LfgDungeonId, 0);
				return true;
			}
		}
	}

	*/
	return false;
}

void LfgMgr::SetPlayerInLFGqueue(Player *pl,uint32 LfgDungeonId)
{
	if( pl == NULL )
		return;

	if( LfgDungeonId >= MAX_DUNGEONS )
		return;


	// there are either no groups free or we don't have autojoin enabled, put us in the queue.
	m_lookingForGroup[LfgDungeonId].push_back(pl);

	// release the lock
}

void LfgMgr::RemovePlayerFromLfgQueues(Player * pl)
{
	if( pl == NULL )
		return;

	for(uint32 i = 0; i < MAX_LFG_QUEUE_ID; ++i)
	{
		if(pl->LfgDungeonId[i] != 0)
		{
			if( pl->LfgDungeonId[i] < MAX_DUNGEONS )
				m_lookingForGroup[pl->LfgDungeonId[i]].remove(pl);

			if(pl->m_Autojoin)
				pl->SendMeetingStoneQueue(pl->LfgDungeonId[i], 0);

			pl->LfgDungeonId[i] = 0;
			pl->LfgType[i] = 0;
		}
	}

	if(pl->LfmDungeonId)
	{
		if( pl->LfmDungeonId < MAX_DUNGEONS )
			m_lookingForMore[pl->LfmDungeonId].remove(pl);

		pl->LfmDungeonId=0;
		pl->LfmType=0;
	}

}

void LfgMgr::RemovePlayerFromLfgQueue( Player* plr, uint32 LfgDungeonId )
{
	if( plr == NULL )
		return;

	if( LfgDungeonId >= MAX_DUNGEONS )
		return;

	m_lookingForGroup[LfgDungeonId].remove( plr );

	if( plr->m_Autojoin )
		plr->SendMeetingStoneQueue( LfgDungeonId, 0 );
}

void LfgMgr::UpdateLfgQueue(uint32 LfgDungeonId)
{
	if( LfgDungeonId >= MAX_DUNGEONS )
		return;

	LfgPlayerList possibleGroupLeaders;
	LfgPlayerList possibleMembers;
	LfgPlayerList::iterator itr;
	LfgPlayerList::iterator it2;
	LfgPlayerList::iterator it3;
	Player * plr;
	uint32 i;
	//LfgMatch * pMatch;

	// only update on autojoinable dungeons
	if(LfgDungeonTypes[LfgDungeonId] != LFG_INSTANCE && LfgDungeonTypes[LfgDungeonId] != LFG_HEROIC_DUNGEON)
		return;

	for(itr = m_lookingForGroup[LfgDungeonId].begin(); itr != m_lookingForGroup[LfgDungeonId].end(); ++itr)
	{
        plr = *itr;

		if(plr->m_lfgInviterGuid || plr->m_lfgMatch != NULL)
			continue;

		// possible member?
		if(plr->m_Autojoin)
			possibleMembers.push_back(plr);
	}

	for(itr = m_lookingForMore[LfgDungeonId].begin(); itr != m_lookingForMore[LfgDungeonId].end(); ++itr)
	{
		if(plr->GetGroup())
		{
			// check if this is a suitable candidate for a group for others to join
			if(plr->m_AutoAddMem && plr->IsGroupLeader() && !plr->GetGroup()->IsFull() && plr->GetGroup()->GetGroupType() == GROUP_TYPE_PARTY)
			{
				possibleGroupLeaders.push_back(plr);
				continue;
			}

			if(plr->m_lfgInviterGuid || plr->m_lfgMatch != NULL)
				continue;

			// just a guy in a group
			continue;
		}
	}

	// look through our members, as well as our groups and see if we can add anyone to any groups
	// if not, if we have enough members, create a new group for them.

	if(possibleMembers.size() > 0)
	{
		for(itr = possibleGroupLeaders.begin(); itr != possibleGroupLeaders.end(); ++itr)
		{
			for(it2 = possibleMembers.begin(); it2 != possibleMembers.end();)
			{
				if((*itr)->GetGroup()->IsFull())
				{
					++it2;
					continue;
				}

				// found a group for him, lets insert him.
				if((*itr)->GetGroup()->AddMember((*it2)->m_playerInfo))
				{
					(*it2)->m_lfgInviterGuid = (*itr)->GetLowGUID();

					it3 = it2;
					++it2;

					possibleMembers.erase(it2);
				}
				else
					++it2;
			}
		}
	}

	// do we still have any members left over (enough to form a group)
	while(possibleMembers.size() > 1)
	{
		/*pMatch = new LfgMatch(LfgDungeonId);
		for(i = 0; i < 5, possibleMembers.size() > 0; ++i)
		{
			pMatch->PendingPlayers.insert(possibleMembers.front());
			possibleMembers.front()->SendMeetingStoneQueue(LfgDungeonId, 0);
			possibleMembers.front()->GetSession()->OutPacket(SMSG_LFG_INVITE, 4, &LfgDungeonId);
			possibleMembers.front()->m_lfgMatch = pMatch;
			possibleMembers.pop_front();
		}
		*/
		Group * pGroup = new Group(true);
		for(i = 0; i < 5 && possibleMembers.size() > 0; ++i)
		{
			pGroup->AddMember( possibleMembers.front()->m_playerInfo );
			possibleMembers.front()->SendMeetingStoneQueue( LfgDungeonId, 0 );
			m_lookingForGroup[LfgDungeonId].remove( possibleMembers.front() );
			possibleMembers.pop_front();
		}
	}

}

void LfgMgr::SendLfgList( Player* plr, uint32 Dungeon )
{
	if( plr == NULL )
		return;

	if( Dungeon >= MAX_DUNGEONS )
		return;

// 	LfgPlayerList::iterator itr;
// 	GroupMembersSet::iterator it2;
// 	uint32 count = 0;
// 	Player * pl;
// 	uint32 i;
// 	uint64 tguid;
// 	SubGroup * sgrp;
// 
// 
// 	WorldPacket data(MSG_LOOKING_FOR_GROUP, ((m_lookingForGroup[Dungeon].size() + m_lookingForMore[Dungeon].size()) * 20) + 20);
// 	data << LfgDungeonTypes[Dungeon];
// 	data << Dungeon;
// 	data << uint32(m_lookingForGroup[Dungeon].size());
// 	data << uint32(m_lookingForGroup[Dungeon].size());
// 
// 	for(itr = m_lookingForGroup[Dungeon].begin(); itr != m_lookingForGroup[Dungeon].end(); ++itr)
// 	{
// 		pl = *itr;
// 		if(pl->GetTeam() != plr->GetTeam() || pl == plr)
// 			continue;
// 
//         ++count;
// 		data << pl->GetNewGUID();
// 		data << pl->getLevel();
// 		data << pl->GetZoneId();
// 		data << uint8(0);		// 1=LFG?
// 
//         for(i = 0; i < MAX_LFG_QUEUE_ID; ++i)
// 			data << pl->LfgDungeonId[i] << uint8(0) << pl->LfgType[i];
// 
// 		data << pl->Lfgcomment;
// 
// 		// LFG members are never in parties.
// 		data << uint32(0);
// 	}
// 
// 	for(itr = m_lookingForMore[Dungeon].begin(); itr != m_lookingForMore[Dungeon].end(); ++itr)
// 	{
// 		pl = *itr;
// 		if(pl->GetTeam() != plr->GetTeam() || pl == plr)
// 			continue;
// 
// 		++count;
// 		data << pl->GetNewGUID();
// 		data << pl->getLevel();
// 		data << pl->GetZoneId();
// 		data << uint8(1);		// 1=LFM?
// 
// 		for(i = 0; i < MAX_LFG_QUEUE_ID; ++i)
// 			data << pl->LfgDungeonId[i] << uint8(0) << pl->LfgType[i];
// 
// 		data << pl->Lfgcomment;
// 
// 		if(pl->GetGroup() && pl->GetGroup()->GetGroupType() == GROUP_TYPE_PARTY)
// 		{
// 			pl->GetGroup()->Lock();
// 			sgrp = pl->GetGroup()->GetSubGroup(0);
// 			data << uint32(sgrp->GetMemberCount() - 1);
// 			for(it2 = sgrp->GetGroupMembersBegin(); it2 != sgrp->GetGroupMembersEnd(); ++it2)
// 			{
// 				if((*it2)->m_loggedInPlayer)
// 					data << (*it2)->m_loggedInPlayer->GetNewGUID();
// 				else
// 				{
// 					tguid = (*it2)->guid;
// 					FastGUIDPack(data, tguid);
// 				}
// 			}
// 
// 			pl->GetGroup()->Unlock();
// 		}
// 		else
// 			data << uint32(0);
// 	}
// 
// 
// 	//*(uint32*)(data.contents()[8]) = count;
// 	//*(uint32*)(data.contents()[12]) = count;
// 	data.put(8, count);
// 	data.put(12, count);
// 
//     plr->GetSession()->SendPacket(&data);
}

void LfgMgr::SetPlayerInLfmList(Player * pl, uint32 LfgDungeonId)
{
	if( pl == NULL )
		return;

	if( !pl->IsInWorld() )
		return;

	if( LfgDungeonId >= MAX_DUNGEONS )
		return;

	m_lookingForMore[LfgDungeonId].push_back(pl);
}

void LfgMgr::RemovePlayerFromLfmList(Player * pl, uint32 LfmDungeonId)
{
	if( pl == NULL )
		return;

	if( !pl->IsInWorld() )
		return;

	if( LfmDungeonId >= MAX_DUNGEONS )
		return;

	m_lookingForMore[LfmDungeonId].remove(pl);
}
