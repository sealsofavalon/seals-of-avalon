#ifndef _LOOTMGR_H
#define _LOOTMGR_H
struct ItemPrototype;
class MapMgr;
class Player;
struct RandomProps;
struct ItemRandomSuffixEntry;
class CBattleground;

class LootRoll : public EventableObject
{
public:
	LootRoll(uint32 timer, uint32 groupcount, uint64 guid, uint32 slotid, uint32 itemid, uint32 itemunk1, uint32 itemunk2, MapMgr * mgr);
	~LootRoll();
	void PlayerRolled(Player *player, uint8 choice);
	void Finalize();

	int32 event_GetInstanceID();

private:
	std::map<uint32, uint32> m_NeedRolls;
	std::map<uint32, uint32> m_GreedRolls;
	set<uint32> m_passRolls;
	uint32 _groupcount;
	uint32 _slotid;
	uint32 _itemid;
	uint32 _itemunk1;
	uint32 _itemunk2;
	uint32 _remaining;
	uint64 _guid;
	MapMgr * _mgr;
};

typedef vector<pair<RandomProps*, float> > RandomPropertyVector;
typedef vector<pair<ItemRandomSuffixEntry*, float> > RandomSuffixVector;

typedef struct
{
	_LootItem item;
	float chance;
	float chance2;
	uint32 mincount;
	uint32 maxcount;
	uint32 ffa_loot;
	uint32 drop_on_ground;
}StoreLootItem;


typedef struct 
{
	uint32 count;
	StoreLootItem*items;
}StoreLootList;


struct tempy
{
	uint32 itemid;
	float chance;
	float chance_2;
	uint32 mincount;
	uint32 maxcount;
	uint32 ffa_loot;
	uint32 drop_on_ground;
};


//////////////////////////////////////////////////////////////////////////////////////////


typedef HM_NAMESPACE::hash_map<uint32, StoreLootList > LootStore;  



class LootMgr : public Singleton < LootMgr >
{
public:
	LootMgr();
	~LootMgr();

	void FillCreatureLoot(Loot * loot,uint32 loot_id, uint32 creature_level, uint32 maxitemcnt, bool heroic, uint32 addchance=0);
	void FillGOLoot(Loot * loot,uint32 loot_id, bool heroic);
	void FillItemLoot(Loot *loot, uint32 loot_id);
	void FillFishingLoot(Loot * loot,uint32 loot_id);
	void FillSkinningLoot(Loot * loot,uint32 loot_id);
	void FillPickpocketingLoot(Loot *loot, uint32 loot_id);
	void FillDisenchantingLoot(Loot *loot, uint32 loot_id);
	void FillProspectingLoot(Loot *loot, uint32 loot_id);
	void Gamble( Player* p, uint32 spell );

	bool CanGODrop(uint32 LootId,uint32 itemid);
	bool IsPickpocketable(uint32 creatureId);
	bool IsSkinnable(uint32 creatureId);
	bool IsFishable(uint32 zoneid);

	void LoadLoot();
	void LoadLootProp();
	void LoadGamble();
	
	LootStore	CreatureLoot;
	LootStore	FishingLoot;
	LootStore	SkinningLoot;
	LootStore	GOLoot;
	LootStore	ItemLoot;
	LootStore	ProspectingLoot;
	LootStore	DisenchantingLoot;
	LootStore	PickpocketingLoot;
	LootStore	WorldDropLoot;
	std::map<uint32, std::set<uint32> > quest_loot_go;

	void GetVariableProperties(ItemPrototype * itemproto, vector<uint32>& ids);
	RandomProps * GetRandomProperties(ItemPrototype * proto);
	ItemRandomSuffixEntry * GetRandomSuffix(ItemPrototype * proto);

	bool is_loading;
 
private:
	void LoadLootTables(const char * szTableName,LootStore * LootTable);
	void PushLoot(StoreLootList *list,Loot * loot, bool heroic, uint32 maxcnt = 10, uint32 addchance = 0);
	
	map<uint32, RandomPropertyVector> _randomprops;
	map<uint32, RandomSuffixVector> _randomsuffix;
	struct gamble_conf
	{
		uint32 spell;
		uint32 item;
		float prob;
		uint32 min_count;
		uint32 max_count;
	};
	std::map<uint32, std::vector<gamble_conf> > _gambleconfs;
};

#define lootmgr LootMgr::getSingleton()

#endif
