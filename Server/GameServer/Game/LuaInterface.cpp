#include "StdAfx.h"
#include "LuaInterface.h"
#include "LuaUnit.h"
#include "MultiLanguageStringMgr.h"

#ifndef ENABLE_MULTI_LANGUAGE
#ifdef _WIN32
std::string UTF8ToAnis(const std::string& str)
{
	char acUnicode[4096];
	char acAnsi[2048];

	unsigned short usLen;

	//UTF8 转换为 Unicode
	usLen = (unsigned short)MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.size(), (wchar_t*)acUnicode, sizeof(acUnicode)/2);

	//Unicode 转换为 Ansi
	usLen = (unsigned short)WideCharToMultiByte(936, 0, (wchar_t*)acUnicode, usLen, acAnsi, sizeof(acAnsi), NULL, false);

	return std::string(acAnsi, usLen);
}
#else
#include <iconv.h>
int code_convert(const char *from_charset, const char *to_charset,char *inbuf, size_t inlen, char *outbuf, size_t outlen) 
{ 
	iconv_t cd; 
	int rc; 
	char **pin = &inbuf; 
	char **pout = &outbuf; 

	cd = iconv_open(to_charset,from_charset); 
	if (cd==0) return -1; 
	memset(outbuf,0,outlen); 
	if (iconv(cd,pin,&inlen,pout,&outlen)==-1) return -1; 
	iconv_close(cd); 
	return 0; 
} 
//UNICODE码转为GB2312码 
int u2g(char *inbuf, size_t inlen,char *outbuf,size_t outlen) 
{ 
	return code_convert("utf-8","gb2312",inbuf,inlen,outbuf,outlen); 
} 


std::string UTF8ToAnis(const std::string& str)
{
	char to_str[1024] = { 0 };
	size_t n = 1024;
	u2g( (char*)str.c_str(), str.length(), (char*)to_str, n );

	return std::string( to_str );
}

#endif

#else

std::string UTF8ToAnis(const std::string& str)
{
	return str;
}

#endif

static std::map<uint32, std::pair<uint32, const char*> > s_mapLuaBuffer;

uint32 LoadLuaFile( const char* filename )
{
	uint32 hashCode = crc32( (const uint8*)filename, strlen( filename ) );
	std::map<uint32, std::pair<uint32, const char*> >::iterator it = s_mapLuaBuffer.find( hashCode );
	if( it != s_mapLuaBuffer.end() )
	{
		return hashCode;
	}
	else
	{
		FILE* fp = fopen( filename, "rb" );
		if( fp )
		{
			fseek( fp, 0, SEEK_END );
			uint32 len = ftell( fp );
			fseek( fp, 0, SEEK_SET );
			char* p = (char*)malloc( len );
			if( 1 == fread( p, len, 1, fp ) )
			{
				fclose( fp );

				s_mapLuaBuffer[hashCode] = std::make_pair( len, p );
				return hashCode;
			}
			else
			{
				free( p );
				fclose( fp );
				return 0;
			}
		}
		else
			return 0;
	}
}

const char* GetLuaBuffer( uint32 hashCode, size_t& outSize )
{
	std::map<uint32, std::pair<uint32, const char*> >::iterator it = s_mapLuaBuffer.find( hashCode );
	if( it != s_mapLuaBuffer.end() )
	{
		outSize = it->second.first;
		return it->second.second;
	}
	else
	{
		outSize = 0;
		return NULL;
	}
}


int LuaGetRaidObject( LuaState* s )
{
	LuaStack args( s );
	if( args[1].IsInteger() )
	{
		int id = args[1].GetInteger();
		LuaUnit* p = ((LuaUnit*)id);
		LuaObject obj = p->m_LuaState->BoxPointer( p );
		obj.SetMetaTable( *p->m_MetaTable );
		//obj.Push();
		return 1;
	}
	return 0;
}


int LuaRandom( LuaState* s )
{
	LuaStack args( s );
	int luamin = 0;
	if( args[1].IsInteger() )
		luamin = args[1].GetInteger();
	else if( args[1].IsNumber() )
		luamin = args[1].GetNumber();

	int luamax = 0;
	if( args[2].IsInteger() )
		luamax = args[2].GetInteger();
	else if( args[2].IsNumber() )
		luamax = args[2].GetNumber();

	if( luamax > luamin )
		s->PushInteger( luamin + rand() % (luamax - luamin + 1) );
	else
		s->PushInteger( luamin );

	return 1;
}

int BuildLanguageString( LuaState* s )
{
	LuaStack args( s );

	if( args[1].IsInteger() )
	{
		LuaFunction<const char*>* lf = g_multi_langage_string_mgr->GetLuaFunction( (build_string_t)args[1].GetInteger() );
		if( lf )
		{
			s->PushString( (*lf)() );
			return 1;
		}
	}
	assert( 0 && "int BuildLanguageString( LuaState* s )" );
	s->PushFString( "" );
	return 1;
}
