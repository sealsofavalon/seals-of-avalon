#ifndef _LUA_INTERFACE_HEAD
#define _LUA_INTERFACE_HEAD

#include "../../LuaPlusForLinux/LuaPlus.h"

#ifdef _WIN32
#define LUA_HOTFIX
#endif

using namespace LuaPlus;

extern LuaState* g_luastate;

template<class T>
class MyLuaObject
{
public:
	typedef MyLuaObject<T> this_type;
	typedef int (T::*FN)( LuaState* );

	MyLuaObject()
	{
		m_LuaIndex = ++s_index;
		s_mapObject[m_LuaIndex] = static_cast<T*>( this );
	}
	virtual ~MyLuaObject()
	{
		s_mapObject.erase( m_LuaIndex );
	}

	static void InitMyLua( const char* class_name )
	{
		s_MetaTable = g_luastate->GetGlobals().CreateTable( class_name );
		s_MetaTable.SetObject( "__index", s_MetaTable );
	}

	static void RegisterObjectFunctor( const char* name, FN fn )
	{
		s_MetaTable.RegisterObjectFunctor( name, fn );
	}

	static std::map<int, T*> s_mapObject;
	static LuaObject s_MetaTable;

protected:
	int m_LuaIndex;
	static int s_index;
};

template<class T>
LuaObject MyLuaObject<T>::s_MetaTable;

template<class T>
int MyLuaObject<T>::s_index = 0;

template<class T>
std::map<int, T*> MyLuaObject<T>::s_mapObject;


template<class T>
class InstanceLuaObject
{
public:
	typedef int (T::*FN)( LuaState* );

	InstanceLuaObject( const char* class_name )
	{
		m_LuaState = LuaState::Create();
		m_MetaTable = new LuaObject;
		*m_MetaTable = m_LuaState->GetGlobals().CreateTable( class_name );
		(*m_MetaTable).SetObject( "__index", *m_MetaTable );
		m_LuaState->OpenLibs();
	}
	virtual ~InstanceLuaObject()
	{
		delete m_MetaTable;
		LuaState::Destroy( m_LuaState );
	}

	void RegisterObjectFunctor( const char* name, FN fn )
	{
		(*m_MetaTable).RegisterObjectFunctor( name, fn );
	}

	template<typename RET>
	LuaFunction<RET>* CreateLuaFunction( const char* FunctionName )
	{
		LuaObject fnObj = m_LuaState->GetGlobal( FunctionName );
		if( !fnObj.IsNil() && fnObj.IsFunction() )
			return new LuaFunction<RET>( fnObj );
		else
			return NULL;
	}

	LuaObject* m_MetaTable;
	LuaState* m_LuaState;
};

std::string UTF8ToAnis(const std::string& str);
uint32 LoadLuaFile( const char* filename );
const char* GetLuaBuffer( uint32 hashCode, size_t& outSize );

extern "C" {

int LuaGetRaidObject( LuaState* s );
int LuaRandom( LuaState* s );
int BuildLanguageString( LuaState* s );

} // extern "C"

#endif