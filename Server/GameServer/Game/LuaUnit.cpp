#include "StdAfx.h"
#include "LuaUnit.h"
#include "SunyouRaid.h"
#include "SunyouAdvancedBattleGround.h"
#include "../../../SDBase/Protocol/S2C_Chat.h"
#include "../../../SDBase/Protocol/S2C_Move.h"
#include "MultiLanguageStringMgr.h"

LuaUnit::LuaUnit()
 : m_bRaidInCombat( false ), InstanceLuaObject<LuaUnit>( "LuaUnit" ),
	m_LastLuaUpdateTime( UNIXTIME ), m_eventID( 0 ), m_ContainerMapMaxID( 0 ), m_ContainerMultiMapMaxID( 0 ), m_PositionMax( 0 ), m_bEventChanged( false ), m_DataStructMax( 0 )
{
	memset( m_aLuaContainerMap, 0, sizeof( m_aLuaContainerMap ) );
	memset( m_aLuaContainerMultiMap, 0, sizeof( m_aLuaContainerMultiMap ) );

	for( int i = 0; i < 3; ++i )
	{
		m_BattlePower[i] = 0;
		m_BattleResource[i] = 0;
		m_BattlePlayerCount[i] = 0;
		m_BattleBannerCount[i] = 0;
	}
}

LuaUnit::~LuaUnit()
{
	for( std::set<int>::iterator it = m_threatChains.begin(); it != m_threatChains.end(); ++it )
		AIInterface::ThreatChain::Release( *it );

	for( int i = 0; i < m_ContainerMapMaxID; ++i )
		if( m_aLuaContainerMap[i] )
			delete m_aLuaContainerMap[i];

	for( int i = 0; i < m_ContainerMultiMapMaxID; ++i )
		if( m_aLuaContainerMultiMap[i] )
			delete m_aLuaContainerMultiMap[i];

	for( std::map<int, data_struct*>::iterator it = m_mapDataStructs.begin(); it != m_mapDataStructs.end(); ++it )
		delete it->second;
}

void LuaUnit::Init( SunyouRaid* raid, const char* lua_script, uint32 lua_hash )
{
	m_Raid = raid;
	m_mapmgr = raid->m_mapmgr;

	RegisterObjectFunctor( "SpawnGameObject", &LuaUnit::SpawnGameObject );
	RegisterObjectFunctor( "SpawnCreature", &LuaUnit::SpawnCreature );
	RegisterObjectFunctor( "InsertNPCGossip", &LuaUnit::InsertNPCGossip );
	RegisterObjectFunctor( "EnableNPCGossip", &LuaUnit::EnableNPCGossip );
	RegisterObjectFunctor( "ModifyThreat", &LuaUnit::ModifyThreat );
	RegisterObjectFunctor( "DespawnCreature", &LuaUnit::DespawnCreature );
	RegisterObjectFunctor( "DespawnGameObject", &LuaUnit::DespawnGameObject );
	RegisterObjectFunctor( "HasObject", &LuaUnit::HasObject );
	RegisterObjectFunctor( "CreatureSay", &LuaUnit::CreatureSay );
	RegisterObjectFunctor( "EjectPlayerFromInstance", &LuaUnit::EjectPlayerFromInstance );
	RegisterObjectFunctor( "GetPlayerBasicInfo", &LuaUnit::GetPlayerBasicInfo );
	RegisterObjectFunctor( "GetCreatureBasicInfo", &LuaUnit::GetCreatureBasicInfo );
	RegisterObjectFunctor( "TeleportPlayer", &LuaUnit::TeleportPlayer );
	RegisterObjectFunctor( "TeleportCreature", &LuaUnit::TeleportCreature );
	RegisterObjectFunctor( "ModifyCreatureHPPct", &LuaUnit::ModifyCreatureHPPct );
	RegisterObjectFunctor( "ModifyCreatureMPPct", &LuaUnit::ModifyCreatureMPPct );
	RegisterObjectFunctor( "ModifyPlayerHPPct", &LuaUnit::ModifyPlayerHPPct );
	RegisterObjectFunctor( "ModifyPlayerMPPct", &LuaUnit::ModifyPlayerMPPct );
	RegisterObjectFunctor( "ResetPlayerCooldown", &LuaUnit::ResetPlayerCooldown );
	RegisterObjectFunctor( "CreatureCastSpell", &LuaUnit::CreatureCastSpell );
	RegisterObjectFunctor( "AddBuffToPlayer", &LuaUnit::AddBuffToPlayer );
	RegisterObjectFunctor( "RemoveBuffFromPlayer", &LuaUnit::RemoveBuffFromPlayer );
	RegisterObjectFunctor( "RemoveBuffFromCreature", &LuaUnit::RemoveBuffFromCreature );
	RegisterObjectFunctor( "CreatureMoveTo", &LuaUnit::CreatureMoveTo );
	RegisterObjectFunctor( "GetCreatureThreatListCount", &LuaUnit::GetCreatureThreatListCount );
	RegisterObjectFunctor( "GetCreatureTargetByThreatIndex", &LuaUnit::GetCreatureTargetByThreatIndex );
	RegisterObjectFunctor( "GetRandomPlayerByClass", &LuaUnit::GetRandomPlayerByClass );
	RegisterObjectFunctor( "CreatureForceLeaveCombat", &LuaUnit::CreatureForceLeaveCombat );
	RegisterObjectFunctor( "ModifyObjectScale", &LuaUnit::ModifyObjectScale );
	RegisterObjectFunctor( "CreateThreatChain", &LuaUnit::CreateThreatChain );
	RegisterObjectFunctor( "AddCreatureToThreatChain", &LuaUnit::AddCreatureToThreatChain );
	RegisterObjectFunctor( "GetThreatChainByCreature", &LuaUnit::GetThreatChainByCreature );
	RegisterObjectFunctor( "RemoveCreatureFromThreatChain", &LuaUnit::RemoveCreatureFromThreatChain );
	RegisterObjectFunctor( "CreateEvent", &LuaUnit::CreateEvent );
	RegisterObjectFunctor( "KillEvent", &LuaUnit::KillEvent );
	RegisterObjectFunctor( "ModifyPlayerPVPFlag", &LuaUnit::ModifyPlayerPVPFlag );
	RegisterObjectFunctor( "PlaySound", &LuaUnit::PlaySound );
	RegisterObjectFunctor( "RandomShufflePlayers", &LuaUnit::RandomShufflePlayers );
	RegisterObjectFunctor( "GetNextRandomShufflePlayer", &LuaUnit::GetNextRandomShufflePlayer );
	RegisterObjectFunctor( "GetCreatureThreat", &LuaUnit::GetCreatureThreat );
	RegisterObjectFunctor( "SetCreatureImmune", &LuaUnit::SetCreatureImmune );
	RegisterObjectFunctor( "ResurrectPlayer", &LuaUnit::ResurrectPlayer );
	RegisterObjectFunctor( "GetObjectPosition", &LuaUnit::GetObjectPosition );
	RegisterObjectFunctor( "SendSystemNotify", &LuaUnit::SendSystemNotify );
	RegisterObjectFunctor( "GetMinHPPctCreatureFromThreatChain", &LuaUnit::GetMinHPPctCreatureFromThreatChain );
	RegisterObjectFunctor( "GetMinHPPctUnitFromCreatureThreatList", &LuaUnit::GetMinHPPctUnitFromCreatureThreatList );
	RegisterObjectFunctor( "GetUnitTarget", &LuaUnit::GetUnitTarget );
	RegisterObjectFunctor( "SummonPlayer", &LuaUnit::SummonPlayer );
	RegisterObjectFunctor( "ResurrectCreature", &LuaUnit::ResurrectCreature );
	RegisterObjectFunctor( "GetGameObjectEntry", &LuaUnit::GetGameObjectEntry );
	RegisterObjectFunctor( "GetCreatureEntry", &LuaUnit::GetCreatureEntry );

	RegisterObjectFunctor( "DismissMountAndShift", &LuaUnit::DismissMountAndShift );
	RegisterObjectFunctor( "ScreenShake", &LuaUnit::ScreenShake );
	RegisterObjectFunctor( "SceneEffect", &LuaUnit::SceneEffect );
	RegisterObjectFunctor( "DespawnCreatureByEntry", &LuaUnit::DespawnCreatureByEntry );
	RegisterObjectFunctor( "DespawnGameObjectByEntry", &LuaUnit::DespawnGameObjectByEntry );
	RegisterObjectFunctor( "AddPlayerTitle", &LuaUnit::AddPlayerTitle );
	RegisterObjectFunctor( "HonorReward", &LuaUnit::HonorReward );
	RegisterObjectFunctor( "ItemReward", &LuaUnit::ItemReward );
	RegisterObjectFunctor( "GetPlayerRace", &LuaUnit::GetPlayerRace );
	RegisterObjectFunctor( "CreatureWhisper", &LuaUnit::CreatureWhisper );
	RegisterObjectFunctor( "ModifyBattleGroundPower", &LuaUnit::ModifyBattleGroundPower );
	RegisterObjectFunctor( "HasBuff", &LuaUnit::HasBuff );
	RegisterObjectFunctor( "ModifyBattleGroundResource", &LuaUnit::ModifyBattleGroundResource );
	RegisterObjectFunctor( "GetGameObjectFlag", &LuaUnit::GetGameObjectFlag );
	RegisterObjectFunctor( "ModifyGameObjectFlag", &LuaUnit::ModifyGameObjectFlag );
	RegisterObjectFunctor( "ForceRootPlayer", &LuaUnit::ForceRootPlayer );
	RegisterObjectFunctor( "ForceUnrootPlayer", &LuaUnit::ForceUnrootPlayer );
	RegisterObjectFunctor( "AddPlayerResurrectPosition", &LuaUnit::AddPlayerResurrectPosition );
	RegisterObjectFunctor( "SearchNearestPlayerResurrectPosition", &LuaUnit::SearchNearestPlayerResurrectPosition );
	RegisterObjectFunctor( "RemovePlayerResurrectPosition", &LuaUnit::RemovePlayerResurrectPosition );
	RegisterObjectFunctor( "CreatePosition", &LuaUnit::CreatePosition );
	RegisterObjectFunctor( "GetPosition", &LuaUnit::GetPosition );
	RegisterObjectFunctor( "UpdateBattleResourceToPlayers", &LuaUnit::UpdateBattleResourceToPlayers );
	RegisterObjectFunctor( "UpdateBattlePowerToPlayers", &LuaUnit::UpdateBattlePowerToPlayers );
	RegisterObjectFunctor( "BattleGroundEndByRace", &LuaUnit::BattleGroundEndByRace );
	RegisterObjectFunctor( "BattleGroundBannerCaptured", &LuaUnit::BattleGroundBannerCaptured );
	RegisterObjectFunctor( "BattleGroundBannerDefended", &LuaUnit::BattleGroundBannerDefended );
	RegisterObjectFunctor( "BattleGroundBannerSuddenStricken", &LuaUnit::BattleGroundBannerSuddenStricken );
	RegisterObjectFunctor( "AddBattleGroundBanner", &LuaUnit::AddBattleGroundBanner );
	RegisterObjectFunctor( "ContainerMapCreate", &LuaUnit::ContainerMapCreate );
	RegisterObjectFunctor( "ContainerMapInsert", &LuaUnit::ContainerMapInsert );
	RegisterObjectFunctor( "ContainerMapFind", &LuaUnit::ContainerMapFind );
	RegisterObjectFunctor( "ContainerMapErase", &LuaUnit::ContainerMapErase );
	RegisterObjectFunctor( "ContainerMapDestroy", &LuaUnit::ContainerMapDestroy );
	RegisterObjectFunctor( "ContainerMapBegin", &LuaUnit::ContainerMapBegin );
	RegisterObjectFunctor( "ContainerMapNext", &LuaUnit::ContainerMapNext );
	RegisterObjectFunctor( "ContainerMapRandom", &LuaUnit::ContainerMapRandom );
	RegisterObjectFunctor( "ContainerMapRandomExceptKey", &LuaUnit::ContainerMapRandomExceptKey );
	RegisterObjectFunctor( "ContainerMapMinKey", &LuaUnit::ContainerMapMinKey );
	RegisterObjectFunctor( "ContainerMapMaxKey", &LuaUnit::ContainerMapMaxKey );
	RegisterObjectFunctor( "ContainerMapClear", &LuaUnit::ContainerMapClear );
	RegisterObjectFunctor( "ContainerMapModifyValue", &LuaUnit::ContainerMapModifyValue );
	RegisterObjectFunctor( "ModifyObjectFaction", &LuaUnit::ModifyObjectFaction );
	RegisterObjectFunctor( "GeneratePuzzlePositionIntoContainerMap", &LuaUnit::GeneratePuzzlePositionIntoContainerMap );
	RegisterObjectFunctor( "CreateMazeStateMap",&LuaUnit::CreateMazeStateMapInToContainerMap);
	RegisterObjectFunctor( "ContainerMapShuffle", &LuaUnit::ContainerMapShuffle );
	RegisterObjectFunctor( "SwitchCreatureAIMode", &LuaUnit::SwitchCreatureAIMode );
	RegisterObjectFunctor( "DispelAura", &LuaUnit::DispelAura );
	RegisterObjectFunctor( "CreatureForcePlayAnimation", &LuaUnit::CreatureForcePlayAnimation );
	RegisterObjectFunctor( "CreatureForcePlayAnimationByName", &LuaUnit::CreatureForcePlayAnimationByName );
	RegisterObjectFunctor( "ConvertInteger2String", &LuaUnit::ConvertInteger2String );
	RegisterObjectFunctor( "ForceEjectAllPlayers", &LuaUnit::ForceEjectAllPlayers );
	RegisterObjectFunctor( "ForceGameStart", &LuaUnit::ForceGameStart );
	RegisterObjectFunctor( "SetChestLootGold", &LuaUnit::SetChestLootGold );
	RegisterObjectFunctor( "GetAuraStackCount", &LuaUnit::GetAuraStackCount );
	RegisterObjectFunctor( "CalculatePositionDistance", &LuaUnit::CalculatePositionDistance );
	RegisterObjectFunctor( "CalculatePositionDistanceD", &LuaUnit::CalculatePositionDistanceD );
	RegisterObjectFunctor( "CalculateObjDistance", &LuaUnit::CalculateObjDistance );
	RegisterObjectFunctor( "CalculateAngle", &LuaUnit::CalculateAngle );
	RegisterObjectFunctor( "AddPlayerSpellCastLimit", &LuaUnit::AddPlayerSpellCastLimit );
	RegisterObjectFunctor( "ClearPlayerSpellCastLimit", &LuaUnit::ClearPlayerSpellCastLimit );
	RegisterObjectFunctor( "RemovePlayerSpellCastLimit", &LuaUnit::RemovePlayerSpellCastLimit );
	RegisterObjectFunctor( "SetUnitInvisible", &LuaUnit::SetUnitInvisible );
	RegisterObjectFunctor( "LockManaRegeneration", &LuaUnit::LockManaRegeneration );
	RegisterObjectFunctor( "UnlockManaRegeneration", &LuaUnit::UnlockManaRegeneration );
	RegisterObjectFunctor( "AddUnitMana", &LuaUnit::AddUnitMana );
	RegisterObjectFunctor( "CreatePositionByStorage", &LuaUnit::CreatePositionByStorage );
	RegisterObjectFunctor( "GetAttachByPositionStorage", &LuaUnit::GetAttachByPositionStorage );
	RegisterObjectFunctor( "ExchangeItem2Yuanbao", &LuaUnit::ExchangeItem2Yuanbao );
	RegisterObjectFunctor( "ModifyGameObjectDynamicFlags", &LuaUnit::ModifyGameObjectDynamicFlags );
	RegisterObjectFunctor( "GetPlayerName", &LuaUnit::GetPlayerName );
	RegisterObjectFunctor( "EnableMeleeAttack", &LuaUnit::EnableMeleeAttack );
	RegisterObjectFunctor( "GetPlayerShapeShiftSpellEntry", &LuaUnit::GetPlayerShapeShiftSpellEntry );
	RegisterObjectFunctor( "ConsumeCurrency", &LuaUnit::ConsumeCurrency );
	RegisterObjectFunctor( "GoldReward", &LuaUnit::GoldReward );
	RegisterObjectFunctor( "CreatureStopMove", &LuaUnit::CreatureStopMove );
	RegisterObjectFunctor( "CheckLineOfSight", &LuaUnit::CheckLineOfSight );
	RegisterObjectFunctor( "GetLandHeight", &LuaUnit::GetLandHeight );
	RegisterObjectFunctor( "ModifyObjectVariable", &LuaUnit::ModifyObjectVariable );
	RegisterObjectFunctor( "GetObjectVariable", &LuaUnit::GetObjectVariable );
	RegisterObjectFunctor( "ClearObjectVariable", &LuaUnit::ClearObjectVariable );
	RegisterObjectFunctor( "RemoveBuffFromUnit", &LuaUnit::RemoveBuffFromUnit );
	RegisterObjectFunctor( "AddBuffToUnit", &LuaUnit::AddBuffToUnit );
	RegisterObjectFunctor( "ContainerMultiMapCreate", &LuaUnit::ContainerMultiMapCreate );
	RegisterObjectFunctor( "ContainerMultiMapClear", &LuaUnit::ContainerMultiMapClear );
	RegisterObjectFunctor( "ContainerMultiMapInsert", &LuaUnit::ContainerMultiMapInsert );
	RegisterObjectFunctor( "ContainerMultiMapBegin", &LuaUnit::ContainerMultiMapBegin );
	RegisterObjectFunctor( "ContainerMultiMapNext", &LuaUnit::ContainerMultiMapNext );
	RegisterObjectFunctor( "KillCreature", &LuaUnit::KillCreature );
	RegisterObjectFunctor( "CreateStructure", &LuaUnit::CreateStructure );
	RegisterObjectFunctor( "GetStructureData", &LuaUnit::GetStructureData );
	RegisterObjectFunctor( "DestroyStructure", &LuaUnit::DestroyStructure );
	RegisterObjectFunctor( "AddData2Structure", &LuaUnit::AddData2Structure );
	RegisterObjectFunctor( "ModifyDataFromStructure", &LuaUnit::ModifyDataFromStructure );
	RegisterObjectFunctor( "CreateStructureFromDatabase", &LuaUnit::CreateStructureFromDatabase );
	RegisterObjectFunctor( "DisplayTextOnObjectHead", &LuaUnit::DisplayTextOnObjectHead );
	RegisterObjectFunctor( "AddExtraSpell2Player", &LuaUnit::AddExtraSpell2Player );
	RegisterObjectFunctor( "RemoveExtraSpellFromPlayer", &LuaUnit::RemoveExtraSpellFromPlayer );

	//  new BattleGround interface
	RegisterObjectFunctor( "AddNewBattleGroundResource", &LuaUnit::AddNewBattleGroundResource);
	RegisterObjectFunctor( "UpdateNewBattleGroundResource", &LuaUnit::UpdateNewBattleGroundResource);
	RegisterObjectFunctor( "AddNewBattleGroundPower", &LuaUnit::AddNewBattleGroundPower);
	RegisterObjectFunctor( "RemoveNewBattleGroundPower", &LuaUnit::RemoveNewBattleGroundPower);
	RegisterObjectFunctor( "UpdateNewBattleGroundPower", &LuaUnit::UpdateNewBattleGroundPower);
	RegisterObjectFunctor( "NewBattleGroundEnd", &LuaUnit::NewBattleGroundEnd);
	RegisterObjectFunctor( "NewBattleGroundLoserTeam", &LuaUnit::NewBattleGroundLoserTeam);
	RegisterObjectFunctor( "NewBattleGetPlayerTeam", &LuaUnit::NewBattleGetPlayerTeam);
	RegisterObjectFunctor( "GetInstanceMinLevel",&LuaUnit::GetInstanceMinLevel);
	RegisterObjectFunctor( "ModifyUnitPVPFlag", &LuaUnit::ModifyUnitPVPFlag);

	m_LuaState->GetGlobals().Register( "LuaGetRaidObject", LuaGetRaidObject );
	m_LuaState->GetGlobals().Register( "LuaRandom", LuaRandom );
	m_LuaState->GetGlobals().Register( "BuildLanguageString", BuildLanguageString );

#ifdef LUA_HOTFIX		// for hot fix
	m_LuaState->DoFile( lua_script );
#else
	size_t size = 0;
	const char* luabuf = GetLuaBuffer( lua_hash, size );
	assert( size && luabuf );
	m_LuaState->DoBuffer( luabuf, size, lua_script );
#endif

	lf_OnUnitDie = CreateLuaFunction<void>( "OnUnitDie" );
	lf_OnGameObjectTrigger = CreateLuaFunction<void>( "OnGameObjectTrigger" );
	lf_OnUnitEnterCombat = CreateLuaFunction<void>( "OnUnitEnterCombat" );
	lf_OnUnitLeaveCombat = CreateLuaFunction<void>( "OnUnitLeaveCombat" );
	lf_OnPlayerResurrect = CreateLuaFunction<void>( "OnPlayerResurrect" );
	ls_OnCreatureResurrect = CreateLuaFunction<void>( "OnCreatureResurrect" );
	lf_OnNPCGossipTrigger = CreateLuaFunction<void>( "OnNPCGossipTrigger" );
	lf_OnRaidWipe = CreateLuaFunction<void>( "OnRaidWipe" );
	lf_OnUnitCastSpell = CreateLuaFunction<void>( "OnUnitCastSpell" );
	lf_OnUnitBeginCastSpell = CreateLuaFunction<void>( "OnUnitBeginCastSpell" );
	lf_OnEvent = CreateLuaFunction<void>( "OnEvent" );
	lf_OnEnd = CreateLuaFunction<void>( "OnEnd" );
	lf_OnUpdate = CreateLuaFunction<void>( "OnUpdate" );
	lf_OnPlayerEnterInstance = CreateLuaFunction<void>( "OnPlayerEnterInstance" );
	lf_OnPlayerLeaveInstance = CreateLuaFunction<void>( "OnPlayerLeaveInstance" );
	lf_OnStart = CreateLuaFunction<void>( "OnStart" );
	lf_OnCreatureMoveArrival = CreateLuaFunction<void>( "OnCreatureMoveArrival" );
	lf_OnGameObjectDespawn = CreateLuaFunction<void>( "OnGameObjectDespawn" );
	lf_OnPlayerLootItem = CreateLuaFunction<void>( "OnPlayerLootItem" );
	lf_OnUnitHitWithSpell = CreateLuaFunction<void>( "OnUnitHitWithSpell" );
	lf_OnDynamicObjectCreated = CreateLuaFunction<void>( "OnDynamicObjectCreated" );
	lf_OnDynamicObjectMoveArrival = CreateLuaFunction<void>( "OnDynamicObjectMoveArrival" );
	lf_OnDynamicObjectDestroyed = CreateLuaFunction<void>( "OnDynamicObjectDestroyed" );

}

void LuaUnit::Release()
{
	if( lf_OnEnd )
		(*lf_OnEnd)();
}

void LuaUnit::OnPlayerJoin( Player* p )
{
	if( m_Raid->GetCategory() == INSTANCE_CATEGORY_NEW_BATTLE_GROUND )
	{
		assert( p->m_team_arena_idx );
		m_BattlePlayerCount[p->m_team_arena_idx - 1]++;
	}
	else
		m_BattlePlayerCount[p->getRace() - 1]++;

	if( lf_OnPlayerEnterInstance )
		(*lf_OnPlayerEnterInstance)( p->GetUniqueIDForLua() );
}

void LuaUnit::OnPlayerLeave( Player* p )
{
	if( m_Raid->GetCategory() == INSTANCE_CATEGORY_NEW_BATTLE_GROUND )
	{
		assert( p->m_team_arena_idx );
		m_BattlePlayerCount[p->m_team_arena_idx - 1]--;
	}else
		m_BattlePlayerCount[p->getRace() - 1]--;

	for( std::vector<Player*>::iterator it = m_listRandomShufflePlayers.begin(); it != m_listRandomShufflePlayers.end(); ++it )
	{
		if( p == *it )
		{
			m_listRandomShufflePlayers.erase( it );
			break;
		}
	}

	if( lf_OnPlayerLeaveInstance )
		(*lf_OnPlayerLeaveInstance)( p->GetUniqueIDForLua() );
}

void LuaUnit::OnInstanceStartup()
{
	if( lf_OnStart )
		(*lf_OnStart)( (int)this );
}

void LuaUnit::Update()
{
	if( m_Raid->m_InstanceStarted )
	{
		if( (uint32)UNIXTIME > m_LastLuaUpdateTime )
		{
			if( lf_OnUpdate )
				(*lf_OnUpdate)( (uint32)UNIXTIME - m_Raid->m_starttime );

			m_LastLuaUpdateTime = (uint32)UNIXTIME;
		}

		int deadCount = 0;
		for( std::set<Player*>::iterator it = m_Raid->m_players.begin(); it != m_Raid->m_players.end(); ++it )
		{
			Player* p = *it;
			if( p->isDead() )
				++deadCount;
		}
		if( m_bRaidInCombat && deadCount == m_Raid->m_players.size() )
		{
			m_bRaidInCombat = false;
			OnRaidWipe();
		}

		if( m_Raid->GetCategory() != INSTANCE_CATEGORY_FAIRGROUND || m_Raid->IsPrivate() )
		{
			if( (uint32)UNIXTIME - m_Raid->m_starttime > 5 )
			{
				if( m_Raid->m_players.size() == 0 && m_Raid->m_conf.expire == 0 )
				{
					m_Raid->m_InstanceStarted = false;
					sEventMgr.AddEvent( m_Raid, &SunyouRaid::Expire, EVENT_SUNYOU_INSTANCE_EXPIRE, 1, 1, 0 );
				}
			}
		}

		for( std::map<int, Event*>::iterator it = m_events.begin(); it != m_events.end(); ++it )
		{
			Event* p = it->second;
			uint32 now = getMSTime();
			if( now - p->lasttime >= p->interval )
			{
				p->lasttime = now;
				int id = p->id;
				int attach[5];
				memcpy( attach, p->attach, sizeof( attach ) );
				if( --p->count <= 0 )
				{
					delete p;
					m_events.erase( it );

					OnEvent( id, attach );
					break;
				}

				m_bEventChanged = false;
				OnEvent( id, attach );
				if( m_bEventChanged )
					break;
			}
		}
	}
}

int LuaUnit::SpawnCreature( LuaState* s )
{
	LuaStack args( s );

	uint32 guid = 0;
	if( args[2].IsInteger() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() && args[7].IsInteger() )
	{
		int entry = args[2].GetInteger();
		float x = args[3].GetNumber();
		float y = args[4].GetNumber();
		float z = args[5].GetNumber();
		float o = args[6].GetNumber();
		int respawn = args[7].GetInteger();
		
		//CreatureProto * proto = CreatureProtoStorage.LookupEntry(entry);
		CreatureInfo * info = CreatureNameStorage.LookupEntry(entry);
		if( info )
		{
			CreatureSpawn * sp = new CreatureSpawn;
			info->GenerateModelId(&sp->displayid);
			sp->entry = entry;
			sp->form = 0;
			sp->id = objmgr.GenerateCreatureSpawnID();
			sp->movetype = 0;
			sp->x = x;
			sp->y = y;
			sp->z = z;
			sp->o = o;
			sp->emote_state =0;
			sp->flags = 911;
			sp->factionid = 0;//proto->Faction;
			sp->bytes=0;
			sp->bytes2=0;
			sp->stand_state = 0;
			sp->channel_spell=sp->channel_target_creature=sp->channel_target_go=0;

			Creature* p = m_mapmgr->CreateCreature(entry);
			if( p->Load(sp, (uint32)NULL, NULL) )
			{
				p->m_noRespawn = !respawn;
				p->PushToWorld( m_mapmgr );
				guid = p->GetUniqueIDForLua();
				m_CreatureStorage.insert( GET_LOWGUID_PART( p->GetGUID() ) );
			}
			else
			{
				delete p;
				delete sp;
			}
		}
	}
	s->PushInteger( guid );
	return 1;
}
int LuaUnit::HasObject( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Object* p = objmgr.FindObjectForLua( id );
		if( !p )
		{
			s->PushInteger( 1 );
			return 1;
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::SpawnGameObject( LuaState* s )
{
 	LuaStack args( s );

	uint32 guid = 0;
	if( args[2].IsInteger() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() && args[7].IsInteger() && args[8].IsNumber() && args[9].IsInteger() && args[10].IsInteger() && args[11].IsInteger() )
	{
		int entry = args[2].GetInteger();
		float x = args[3].GetNumber();
		float y = args[4].GetNumber();
		float z = args[5].GetNumber();
		float o = args[6].GetNumber();
		int respawn = args[7].GetInteger();
		float range = args[8].GetNumber();
		int faction = args[9].GetInteger();
		int cansee = args[10].GetInteger();
		int triggerByPlayer = args[11].GetInteger();
		if( range < 10.f )
			range = 10.f;

		GOSpawn * sp = new GOSpawn;
		sp->entry = entry;
		sp->x = x;
		sp->y = y;
		sp->z = z;
		sp->o = o;
		sp->o1 = 0;
		sp->o2 = 0;
		sp->o3 = 0;
		sp->facing = 0;
		sp->flags = 911;
		sp->state = 0;
		sp->faction = faction;
		sp->scale = 1.f;

		GameObject* p = m_mapmgr->CreateGameObject( entry );
		if( p->Load( sp ) )
		{
			p->PushToWorld( m_mapmgr );
			p->m_loadedFromDB = true;
			p->m_noRespawn = !respawn;
			p->range = range;

			if(p->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_TRAP)
			{
				p->invisible = true;
				p->invisibilityFlag = INVIS_FLAG_TRAP;
				p->charges = 1;
				p->checkrate = 1;
				p->m_TriggerByPlayer = triggerByPlayer;
				p->m_AlwaysCanSee = cansee;
				sEventMgr.AddEvent(p, &GameObject::TrapSearchTarget, EVENT_GAMEOBJECT_TRAP_SEARCH_TARGET, 100, 0,0);
			}

			p->SetUInt32Value( GAMEOBJECT_STATE, 1 );
			guid = p->GetUniqueIDForLua();

			m_GameObjectStorage.insert( GET_LOWGUID_PART( p->GetGUID() ) );
		}
		else
		{
			delete p;
			delete sp;
		}
	}
	s->PushInteger( guid );
	return 1;
}

int LuaUnit::InsertNPCGossip( LuaState* s )
{
	LuaStack args( s );

	int index = 0;
	if( args[2].IsInteger() && args[3].IsString() )
	{
		int id = args[2].GetInteger();
		Creature* c = objmgr.FindCreatureForLua( id );
		if( c )
		{
			s->PushInteger( c->InsertRaidGossip( args[3].GetString() ) );
			return 1;
		}
	}
	s->PushInteger( -1 );
	return 1;
}

int LuaUnit::EnableNPCGossip( LuaState* s )
{
	LuaStack args( s );

	if( args[2].IsInteger() && args[3].IsInteger() && args[4].IsInteger() )
	{
		int id = args[2].GetInteger();
		uint32 index = args[3].GetInteger();
		uint8 enable = args[4].GetInteger();

		Creature* c = objmgr.FindCreatureForLua( id );
		if( c )
		{
			c->EnableRaidGossip( index, enable );
		}
	}
	return 0;
}

int LuaUnit::ModifyThreat( LuaState* s )
{
	LuaStack args( s );

	if( args[2].IsInteger() && args[3].IsInteger() && args[4].IsInteger() )
	{
		int id = args[2].GetInteger();
		int targetID = args[3].GetInteger();
		int threatMod = args[4].GetInteger();
		Creature* c = objmgr.FindCreatureForLua( id );
		Unit* target = objmgr.FindUnitForLua( targetID );

		if( c && target )
		{
			int curThreat = c->GetAIInterface()->getThreatByPtr( target );
			if( curThreat > 0 )
			{
				threatMod = curThreat * threatMod / 100;
				c->GetAIInterface()->modThreatByPtr( target, threatMod );
			}
			else
				c->GetAIInterface()->AttackReaction( target, abs(threatMod) );
		}
	}
	return 0;
}

int LuaUnit::DespawnCreature( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Creature* c = objmgr.FindCreatureForLua( id );
		if( c )
		{
			m_mapmgr->DespawnActivityCreature( GET_LOWGUID_PART( c->GetGUID() ), true );
			m_CreatureStorage.erase( GET_LOWGUID_PART( c->GetGUID() ) );
		}
	}
	return 0;
}

int LuaUnit::DespawnGameObject( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		GameObject* go = objmgr.FindGameObjectForLua( id );
		if( go )
		{
			m_mapmgr->DespawnActivityCreature( GET_LOWGUID_PART( go->GetGUID() ), false );
			m_GameObjectStorage.erase( GET_LOWGUID_PART( go->GetGUID() ) );
		}
	}
	return 0;
}

int LuaUnit::CreatureSay( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsString() && args[4].IsInteger() )
	{
		Creature* c = objmgr.FindCreatureForLua( args[2].GetInteger() );
		if( c )
		{
			std::string sayWhat = args[3].GetString();
			c->SendChatMessage( args[4].GetInteger() ? 14 : 12, 0, sayWhat.c_str() );
		}
	}
	return 0;
}

int LuaUnit::EjectPlayerFromInstance( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
			p->EjectFromInstance();
	}
	return 0;
}

int LuaUnit::GetPlayerBasicInfo( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			s->PushInteger( p->getClass() );
			s->PushInteger( p->getLevel() );
			s->PushNumber( (float)p->GetUInt32Value( UNIT_FIELD_HEALTH ) / (float)p->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) );
			s->PushNumber( (float)p->GetUInt32Value( UNIT_FIELD_POWER1 ) / (float)p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) );
			return 4;
		}
	}
	s->PushInteger( 0 );
	s->PushInteger( 0 );
	s->PushNumber( 0 );
	s->PushNumber( 0 );
	return 4;
}

int LuaUnit::GetCreatureBasicInfo( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Creature* c = objmgr.FindCreatureForLua( id );
		if( c )
		{
			s->PushNumber( (float)c->GetUInt32Value( UNIT_FIELD_HEALTH ) / (float)c->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) );
			s->PushNumber( (float)c->GetUInt32Value( UNIT_FIELD_POWER1 ) / (float)c->GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) );
			return 2;
		}
	}
	s->PushNumber( 0 );
	s->PushNumber( 0 );
	return 2;
}

int LuaUnit::TeleportPlayer( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() )
	{
		int id = args[2].GetInteger();
		float x = args[3].GetNumber();
		float y = args[4].GetNumber();
		float z = args[5].GetNumber();
		float o = args[6].GetNumber();
		Player* p = objmgr.FindPlayerForLua( id );
		
		if( p )
		{
			sEventMgr.AddEvent( m_Raid, &SunyouRaid::SafeTeleportPlayer, p, LocationVector( x, y, z, o ), EVENT_SUNYOU_RAID_TELEPORT, 1, 1, 0 );
		}
	}
	return 0;
}

int LuaUnit::TeleportCreature( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() )
	{
		int id = args[2].GetInteger();
		float x = args[3].GetNumber();
		float y = args[4].GetNumber();
		float z = args[5].GetNumber();
		float o = args[6].GetNumber();
		Creature* p = objmgr.FindCreatureForLua( id );
		if( p )
		{
			p->SetPosition( x, y, z, o, false );
			p->SendMonsterMove( x, y, z, o, MOVE_TYPE_POLL, 0, 0 );
		}
	}
	return 0;
}

int LuaUnit::ModifyCreatureHPPct( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() )
	{
		int id = args[2].GetInteger();
		float pct = args[3].GetNumber();
		Creature* p = objmgr.FindCreatureForLua( id );
		if( p )
		{
			int mod = pct * (float)p->GetUInt32Value( UNIT_FIELD_MAXHEALTH );
			mod += p->GetUInt32Value( UNIT_FIELD_HEALTH );
			if( mod > (int)p->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) )
				mod = p->GetUInt32Value( UNIT_FIELD_MAXHEALTH );
			else if( mod < 0 )
				mod = 0;

			p->SetUInt32Value( UNIT_FIELD_HEALTH, mod );
		}
	}
	return 0;
}

int LuaUnit::ModifyCreatureMPPct( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() )
	{
		int id = args[2].GetInteger();
		float pct = args[3].GetNumber();
		Creature* p = objmgr.FindCreatureForLua( id );
		if( p )
		{
			int mod = pct * (float)p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 );
			mod += p->GetUInt32Value( UNIT_FIELD_POWER1 );
			if( mod > (int)p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) )
				mod = p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 );
			else if( mod < 0 )
				mod = 0;

			p->SetUInt32Value( UNIT_FIELD_POWER1, mod );
		}
	}
	return 0;
}

int LuaUnit::ModifyPlayerHPPct( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() )
	{
		int id = args[2].GetInteger();
		float pct = args[3].GetNumber();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			int mod = pct * p->GetUInt32Value( UNIT_FIELD_MAXHEALTH );
			mod += p->GetUInt32Value( UNIT_FIELD_HEALTH );
			if( mod > (int)p->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) )
				mod = p->GetUInt32Value( UNIT_FIELD_MAXHEALTH );
			else if( mod < 0 )
				mod = 0;

			p->SetUInt32Value( UNIT_FIELD_HEALTH, mod );
		}
	}
	return 0;
}

int LuaUnit::ModifyPlayerMPPct( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() )
	{
		int id = args[2].GetInteger();
		float pct = args[3].GetNumber();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			int mod = pct * p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 );
			mod += p->GetUInt32Value( UNIT_FIELD_POWER1 );
			if( mod > (int)p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) )
				mod = p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 );
			else if( mod < 0 )
				mod = 0;

			p->SetUInt32Value( UNIT_FIELD_POWER1, mod );
		}
	}
	return 0;
}

int LuaUnit::ResetPlayerCooldown( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			p->ResetAllCooldowns();
		}
	}
	return 0;
}

int LuaUnit::CreatureCastSpell( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() && args[4].IsInteger() && args[5].IsInteger() && args[6].IsInteger() )
	{
		int id = args[2].GetInteger();
		int entry = args[3].GetInteger();
		int castType = args[4].GetInteger();
		int targetID = args[5].GetInteger();
		int forceCast = args[6].GetInteger();


		Creature* p = objmgr.FindCreatureForLua( id );
		if( !p || !p->isAlive() )
			return 0;

		Unit* target = objmgr.FindUnitForLua( targetID );

		SpellEntry* spellInfo = dbcSpell.LookupEntry( entry );

		if( p && target && spellInfo )
		{
			if( !forceCast && p->GetCurrentSpell() && p->GetAIInterface()->m_ForceCasting )
			{
				return 0;
			}

			p->GetAIInterface()->m_ForceCasting = forceCast;

			if( target != p && !p->GetAIInterface()->m_ForceMoveMode )
			{
				p->GetAIInterface()->SetNextTarget( target );
			}
			
			SpellCastTargets targets;
			targets.m_unitTarget = target ? target->GetGUID() : 0;
			targets.m_itemTarget = 0;
			targets.m_srcX = 0;
			targets.m_srcY = 0;
			targets.m_srcZ = 0;
			targets.m_destX = 0;
			targets.m_destY = 0;
			targets.m_destZ = 0;

			switch(castType)
			{
			case TTYPE_CASTER:
				{
					targets.m_targetMask = 2;
					targets.m_unitTarget = p->GetGUID();
					p->GetAIInterface()->CastSpell( p, spellInfo, targets );
				}
				break;
			case TTYPE_FRIENDSINGLETARGET:
			case TTYPE_ENMYSINGLETARGERT:
				{
					targets.m_targetMask = 2;
					targets.m_unitTarget = target->GetGUID();
					p->GetAIInterface()->CastSpell( p, spellInfo, targets );
					break;
				}
			case TTYPE_SOURCE:
				{
					targets.m_targetMask = 32;
					targets.m_srcX = p->GetPositionX();
					targets.m_srcY = p->GetPositionY();
					targets.m_srcZ = p->GetPositionZ();

					p->CastSpellAoFSource( targets, spellInfo, false);
					break;
				}
			case TTYPE_FRIENDDESTINATION:
			case TTYPE_ENMYDESTINATION:
				{
					targets.m_targetMask = 64;
					targets.m_destX = target->GetPositionX();
					targets.m_destY = target->GetPositionY();
					targets.m_destZ = target->GetPositionZ();

					p->CastSpellAoFDest( targets, spellInfo, false);
					break;
				}
			}
		}
	}
	return 0;
}

int LuaUnit::AddBuffToPlayer( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int entry = args[3].GetInteger();

		Player* p = objmgr.FindPlayerForLua( id );
		SpellEntry* spellInfo = dbcSpell.LookupEntry( entry );

		if( p && spellInfo )
		{
			Spell * sp = new Spell( p, spellInfo, true, 0 );
			SpellCastTargets targets( p->GetGUID() );
			sp->prepare(&targets);
		}
	}
	return 0;
}

int LuaUnit::RemoveBuffFromPlayer( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int entry = args[3].GetInteger();

		Player* p = objmgr.FindPlayerForLua( id );

		if( p )
		{
			p->RemoveAura( entry );
		}
	}
	return 0;
}

int LuaUnit::RemoveBuffFromCreature( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int entry = args[3].GetInteger();

		Creature* p = objmgr.FindCreatureForLua( id );

		if( p )
		{
			p->RemoveAura( entry );
		}
	}
	return 0;
}

int LuaUnit::CreatureMoveTo( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() && args[7].IsInteger() )
	{
		int id = args[2].GetInteger();
		float x = args[3].GetNumber();
		float y = args[4].GetNumber();
		float z = args[5].GetNumber();
		float o = args[6].GetNumber();
		bool bRun = args[7].GetInteger() != 0;

		Creature* p = objmgr.FindCreatureForLua( id );
		if( p )
		{
			p->GetAIInterface()->ForceMoveTo( x, y, z, o, bRun );
		}
	}
	return 0;
}

int LuaUnit::GetCreatureThreatListCount( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		Creature* p = objmgr.FindCreatureForLua( id );

		if( p )
		{
			s->PushInteger( p->GetAIInterface()->GetThreatTargetCount() );
			return 1;
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::GetCreatureTargetByThreatIndex( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int index = args[3].GetInteger();

		Creature* p = objmgr.FindCreatureForLua( id );

		if( p )
		{
			Unit* u = p->GetAIInterface()->GetTargetByThreatIndex( index );
			if( u )
			{
				s->PushInteger( u->GetUniqueIDForLua() );
				return 1;
			}
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::GetRandomPlayerByClass( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int cls = args[2].GetInteger();
		std::vector<Player*> v;
		for( std::set<Player*>::iterator it = m_Raid->m_players.begin(); it != m_Raid->m_players.end(); ++it )
		{
			Player* p = *it;
			if( p->getClass() == cls || cls == 0 )
				v.push_back( p );
		}

		if( v.size() > 0 )
		{
			Player* plr = v[rand() % v.size()];
			if( plr->IsValid() )
			{
				s->PushInteger( plr->GetUniqueIDForLua() );
				return 1;
			}
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::CreatureForceLeaveCombat( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Creature* c = objmgr.FindCreatureForLua( id );
		if( c )
		{
			c->GetAIInterface()->ClearThreatListAndLeaveCombat();
		}
	}
	return 0;
}

int LuaUnit::ModifyObjectScale( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() )
	{
		int id = args[2].GetInteger();
		float scale = args[3].GetNumber();
		Object* c = objmgr.FindObjectForLua( id );
		if( c )
		{
			c->SetFloatValue( OBJECT_FIELD_SCALE_X, scale );
		}
	}
	return 0;
}

int LuaUnit::CreateThreatChain( LuaState* s )
{
	AIInterface::ThreatChain* tc = AIInterface::ThreatChain::Create();
	m_threatChains.insert( tc->m_id );
	s->PushInteger( tc->m_id );
	return 1;
}

int LuaUnit::AddCreatureToThreatChain( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int creatureID = args[2].GetInteger();
		int chainID = args[3].GetInteger();
		Creature* c = objmgr.FindCreatureForLua( creatureID );
		if( c )
		{
			AIInterface::ThreatChain* tc = AIInterface::ThreatChain::Find( chainID );
			if( tc )
				tc->AddCreature( c );
		}
	}
	return 0;
}

int LuaUnit::GetThreatChainByCreature( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int creatureID = args[2].GetInteger();
		Creature* c = objmgr.FindCreatureForLua( creatureID );
		if( c )
		{
			if( c->GetAIInterface()->m_threat_chain )
			{
				s->PushInteger( c->GetAIInterface()->m_threat_chain->m_id );
				return 1;
			}
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::RemoveCreatureFromThreatChain( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int creatureID = args[2].GetInteger();
		Creature* c = objmgr.FindCreatureForLua( creatureID );
		if( c )
		{
			if( c->GetAIInterface()->m_threat_chain )
				c->GetAIInterface()->m_threat_chain->RemoveCreature( c );
		}
	}
	return 0;
}

int LuaUnit::CreateEvent( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		Event* p = new Event;
		p->id = ++m_eventID;
		p->interval = args[2].GetInteger();
		p->count = args[3].GetInteger();
		p->lasttime = getMSTime();

		for( int i = 0; i < 5; ++i )
		{
			if( args[4 + i].IsInteger() )
				p->attach[i] = args[4 + i].GetInteger();
			else
				p->attach[i] = 0;
		}
		m_events[p->id] = p;
		
		s->PushInteger( p->id );

		m_bEventChanged = true;
		return 1;
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::KillEvent( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		std::map<int, Event*>::iterator it = m_events.find( id );
		if( it != m_events.end() )
		{
			delete it->second;
			m_events.erase( it );

			m_bEventChanged = true;
		}
	}
	return 0;
}

int LuaUnit::ModifyPlayerPVPFlag( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int playerID = args[2].GetInteger();
		int ffa = args[3].GetInteger();
		Player* p = objmgr.FindPlayerForLua( playerID );
		if( p )
			p->SetUInt32Value( UNIT_FIELD_FFA, ffa );
	}
	return 0;
}

int LuaUnit::PlaySound( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsString() )
	{
		int id = args[2].GetInteger();
		Object* p = objmgr.FindObjectForLua( id );
		MSG_S2C::stPlaySoundNotify notify;
		notify.Sound3DBindObjectID = p ? p->GetGUID() : 0;
		notify.SourceFile = args[3].GetString();

		if( args[4].IsInteger() )
			notify.LoopCount = args[4].GetInteger();
		else
			notify.LoopCount = 1;

		if( args[5].IsInteger() )
			notify.bMusic = args[5].GetInteger();
		else
			notify.bMusic = 0;
		
		if( args[6].IsInteger() )
		{
			int plrID = args[6].GetInteger();
			if( plrID )
			{
				Player* plr = objmgr.FindPlayerForLua( plrID );
				if( plr )
				{
					plr->GetSession()->SendPacket( notify );
				}
			}
			else
				m_Raid->Broadcast( notify );
		}
		else
			m_Raid->Broadcast( notify );
	}
	return 0;
}

int LuaUnit::RandomShufflePlayers( LuaState* s )
{
	m_listRandomShufflePlayers.clear();

	for( std::set<Player*>::iterator it = m_Raid->m_players.begin(); it != m_Raid->m_players.end(); ++it )
	{
		Player* p = *it;
		if( p->isAlive() )
			m_listRandomShufflePlayers.push_back( *it );
	}

	srand( (uint32)UNIXTIME );
	std::random_shuffle( m_listRandomShufflePlayers.begin(), m_listRandomShufflePlayers.end() );
	
	s->PushInteger( m_listRandomShufflePlayers.size() );
	return 1;
}

int LuaUnit::GetNextRandomShufflePlayer( LuaState* s )
{
	if( !m_listRandomShufflePlayers.empty() )
	{
		Player* p = m_listRandomShufflePlayers.front();
		m_listRandomShufflePlayers.erase( m_listRandomShufflePlayers.begin() );
		if( p->IsValid() )
		{
			s->PushInteger( p->GetUniqueIDForLua() );
			return 1;
		}
	}

	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::GetCreatureThreat( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int creatureID = args[2].GetInteger();
		int targetID = args[3].GetInteger();

		Creature* c = objmgr.FindCreatureForLua( creatureID );
		Unit* u = objmgr.FindUnitForLua( targetID );
		if( c )
		{
			int threat = c->GetAIInterface()->getThreatByPtr( u );
			s->PushInteger( threat );
			return 1;
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::SetCreatureImmune( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int creatureID = args[2].GetInteger();
		int immnue = args[3].GetInteger();

		Creature* c = objmgr.FindCreatureForLua( creatureID );
		if( c && immnue > 0 && immnue < MECHANIC_MAX )
		{
			c->MechanicsDispels[immnue]++;
		}
	}
	return 0;
}

int LuaUnit::ResurrectPlayer( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() )
	{
		int creatureID = args[2].GetInteger();
		float x = args[3].GetNumber();
		float y = args[4].GetNumber();
		float z = args[5].GetNumber();
		float o = args[6].GetNumber();

		Player* p = objmgr.FindPlayerForLua( creatureID );
		if( p )
		{
			p->ResurrectPlayerForceLocation( x, y, z, o );
		}
	}
	return 0;
}

int LuaUnit::GetObjectPosition( LuaState* s )
{
	LuaStack args( s );
	float x = 0.f, y = 0.f, z = 0.f, o = 0.f;
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		Object* p = objmgr.FindObjectForLua( id );
		if( p )
		{
			x = p->GetPosition().x;
			y = p->GetPosition().y;
			z = p->GetPosition().z;
			o = p->GetPosition().o;
		}
	}
	s->PushNumber( x );
	s->PushNumber( y );
	s->PushNumber( z );
	s->PushNumber( o );
	return 4;
}

int LuaUnit::SendSystemNotify( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsString() )
	{
		MSG_S2C::stNotify_Msg msg;
		msg.notify = args[2].GetString();
		m_Raid->Broadcast( msg );
	}
	return 0;
}

int LuaUnit::GetMinHPPctCreatureFromThreatChain( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		AIInterface::ThreatChain* tc = AIInterface::ThreatChain::Find( args[2].GetInteger() );
		Creature* c = tc->GetMinHPPctCreature();
		if( c )
		{
			s->PushInteger( c->GetUniqueIDForLua() );
			return 1;
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::GetMinHPPctUnitFromCreatureThreatList( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Creature* c = objmgr.FindCreatureForLua( id );
		float min = 1.f;
		Unit* minUnit = NULL;
		if( c )
		{
			for( TargetMap::iterator it = c->GetAIInterface()->m_aiTargets.begin(); it != c->GetAIInterface()->m_aiTargets.end(); ++it )
			{
				Unit* p = it->first;
				if( p->IsValid() && p->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) > 0 )
				{
					float pct = (float)p->GetUInt32Value( UNIT_FIELD_HEALTH ) / (float)p->GetUInt32Value( UNIT_FIELD_MAXHEALTH );
					if( pct <= min )
					{
						minUnit = p;
						min = pct;
					}
				}
			}
			if( minUnit )
			{
				s->PushInteger( minUnit->GetUniqueIDForLua() );
				return 1;
			}
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::GetUnitTarget( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Unit* u = objmgr.FindUnitForLua( id );
		if( u )
		{
			Unit* target = m_mapmgr->GetUnit( u->GetUInt64Value( UNIT_FIELD_TARGET ) );
			if( target )
			{
				s->PushInteger( target->GetUniqueIDForLua() );
				return 1;
			}
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::SummonPlayer( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() )
	{
		int id = args[2].GetInteger();
		float x = args[3].GetNumber();
		float y = args[4].GetNumber();
		float z = args[5].GetNumber();
		float o = args[6].GetNumber();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			p->SafeTeleport( m_mapmgr->GetMapId(), m_mapmgr->GetInstanceID(), LocationVector( x, y, z, o ) );
		}
	}
	return 1;
}

int LuaUnit::ResurrectCreature( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() )
	{
		int id = args[2].GetInteger();
		float x = args[3].GetNumber();
		float y = args[4].GetNumber();
		float z = args[5].GetNumber();
		float o = args[6].GetNumber();
		Creature* c = objmgr.FindCreatureForLua( id );
		if( c )
		{
			c->Resurrect( LocationVector( x, y, z, o ) );
		}
	}
	return 0;
}

int LuaUnit::GetGameObjectEntry( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		GameObject* go = objmgr.FindGameObjectForLua( id );
		if( go )
		{
			s->PushInteger( go->GetInfo()->ID );
			return 1;
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::GetCreatureEntry( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Creature* c = objmgr.FindCreatureForLua( id );
		if( c )
		{
			s->PushInteger( c->GetEntry() );
			return 1;
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::DismissMountAndShift( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int PlayerID = args[2].GetInteger();

		Player* p = objmgr.FindPlayerForLua( PlayerID ); 
		if( p )
		{
			p->RemoveShapeShift();
			p->DismissMount();
		}
	}
	return 0;
}

int LuaUnit::ScreenShake( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int shakeduration = args[2].GetInteger();
		MSG_S2C::stScreenShake msg;
		msg.duration = shakeduration;
		m_Raid->Broadcast( msg );
	}
	return 0;
}

int LuaUnit::SceneEffect( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsString() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsInteger() && args[7].IsNumber() && args[8].IsString() && args[9].IsInteger() )
	{
		MSG_S2C::stSceneEffect msg;
		msg.effect_file_name = args[2].GetString();
		msg.x = args[3].GetNumber();
		msg.y = args[4].GetNumber();
		msg.z = args[5].GetNumber();
		msg.loop_count = args[6].GetInteger();
		msg.scale = args[7].GetNumber();
		msg.sound_file_name = args[8].GetString();
		msg.play_sound_delay = args[9].GetInteger();
		m_Raid->Broadcast( msg );
	}
	return 0;
}

int LuaUnit::DespawnGameObjectByEntry( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		uint32 entry = args[2].GetInteger();
		for( std::set<uint32>::iterator it = m_GameObjectStorage.begin(); it != m_GameObjectStorage.end(); )
		{
			std::set<uint32>::iterator it2 = it;
			++it;
			uint32 id = *it2;
			GameObject* c = m_mapmgr->GetGameObject( id );
			if( c && c->IsValid() )
			{
				if( c->GetInfo()->ID == entry )
				{
					m_mapmgr->DespawnActivityCreature( GET_LOWGUID_PART( c->GetGUID() ), false );
					m_GameObjectStorage.erase( it2 );
				}
			}
		}
	}
	return 0;
}

int LuaUnit::DespawnCreatureByEntry( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		uint32 entry = args[2].GetInteger();
		for( std::set<uint32>::iterator it = m_CreatureStorage.begin(); it != m_CreatureStorage.end(); )
		{
			std::set<uint32>::iterator it2 = it;
			++it;
			uint32 id = *it2;
			Creature* c = m_mapmgr->GetCreature( id );
			if( c && c->IsValid() )
			{
				if( c->GetEntry() == entry )
				{
					m_mapmgr->DespawnActivityCreature( GET_LOWGUID_PART( c->GetGUID() ), true );
					m_CreatureStorage.erase( it2 );
				}
			}
		}
	}
	return 0;
}

int LuaUnit::AddPlayerTitle( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int PlayerID = args[2].GetInteger();
		int TitleID = args[3].GetInteger();

		Player* p = objmgr.FindPlayerForLua( PlayerID );
		if( p && !p->HasTitle( TitleID ) )
			p->QuickAddTitle( TitleID );
	}
	return 0;
}

int LuaUnit::HonorReward( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int honor = args[3].GetInteger();

		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			p->ModTotalHonor( honor );
			p->ModHonorCurrency( honor );
		}
	}
	return 0;
}

int LuaUnit::ItemReward( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() && args[4].IsInteger() )
	{
		int id = args[2].GetInteger();
		int itemid = args[3].GetInteger();
		int count = args[4].GetInteger();

		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			ItemPrototype* item = ItemPrototypeStorage.LookupEntry( itemid );
			if( item )
			{
				if( count <= item->MaxCount )
					p->GiveItem( itemid, item, count );
				else
				{
					while( count > 0 )
					{
						uint32 add = count > item->MaxCount ? item->MaxCount : count;
						p->GiveItem( itemid, item, add );
						count -= add;
					}
				}
			}
		}
	}
	return 0;
}

int LuaUnit::GetPlayerRace( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			s->PushInteger( p->getRace() );
			return 1;
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::CreatureWhisper( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() && args[4].IsString() )
	{
		int id = args[2].GetInteger();
		int target = args[3].GetInteger();
		std::string sayWhat = args[4].GetString();

		Creature* c = objmgr.FindCreatureForLua( id );
		Player* p = objmgr.FindPlayerForLua( target );

		if( c && p )
			c->SendChatMessageToPlayer( CHAT_MSG_MONSTER_WHISPER, 0, sayWhat.c_str(), p );
	}

	return 0;
}

int LuaUnit::ModifyBattleGroundPower( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int race = args[2].GetInteger();
		if( race > 3 || race <= 0 )
		{
			s->PushInteger( 0 );
			assert( 0 );
			return 0;
		}
		
		int power = args[3].GetInteger();

		m_BattlePower[race - 1] += power;
		s->PushInteger( m_BattlePower[race - 1] );
		return 1;
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::HasBuff( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int spellid = args[3].GetInteger();

		Unit* u = objmgr.FindUnitForLua( id );
		if( u )
		{
			if( u->HasAura( spellid ) )
				s->PushInteger( 1 );
			else
				s->PushInteger( 0 );
			return 1;
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::ModifyBattleGroundResource( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int race = args[2].GetInteger();
		if( race > 3 || race <= 0 )
		{
			s->PushInteger( 0 );
			assert( 0 );
			return 0;
		}

		int resource = args[3].GetInteger();

		m_BattleResource[race - 1] += resource;
		s->PushInteger( m_BattleResource[race - 1] );
		return 1;
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::GetGameObjectFlag( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		GameObject* p = objmgr.FindGameObjectForLua( id );
		if( p )
		{
			s->PushInteger( p->GetUInt32Value( GAMEOBJECT_FLAGS ) );
			return 1;
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::ModifyGameObjectFlag( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int flag = args[3].GetInteger();

		GameObject* p = objmgr.FindGameObjectForLua( id );
		if( p )
		{
			p->SetUInt32Value( GAMEOBJECT_FLAGS, flag );
		}
	}
	return 0;
}

int LuaUnit::ForceRootPlayer( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			p->Root( true );
		}
	}
	return 0;
}

int LuaUnit::ForceUnrootPlayer( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			p->Unroot( true );
		}
	}
	return 0;
}

int LuaUnit::AddPlayerResurrectPosition( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int race = args[2].GetInteger();
		if( race <= 0 || race > 3 )
		{
			assert( 0 );
			return 0;
		}
		int pos = args[3].GetInteger();

		m_ResurrectPosition[race - 1].insert( pos );
	}
	return 0;
}

int LuaUnit::SearchNearestPlayerResurrectPosition( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			std::multimap<float, int> m;
			for( std::set<int>::iterator it = m_ResurrectPosition[p->getRace() - 1].begin(); it != m_ResurrectPosition[p->getRace() - 1].end(); ++it )
			{
				int pos = *it;
				std::map<int, LocationVector>::iterator it2 = m_Position.find( pos );
				if( it2 != m_Position.end() )
				{
					float distance = p->CalcDistance( it2->second );
					m.insert( std::make_pair( distance, pos ) );
				}
			}

			if( m.size() )
			{
				int result = m.begin()->second;
				s->PushInteger( result );
				return 1;
			}
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::RemovePlayerResurrectPosition( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int race = args[2].GetInteger();
		int pos = args[3].GetInteger();
		
		if( race <= 0 || race > 3 )
		{
			assert( 0 );
			return 0;
		}

		m_ResurrectPosition[race - 1].erase( pos );
	}
	return 0;
}

int LuaUnit::CreatePosition( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() )
	{
		float x = args[2].GetNumber();
		float y = args[3].GetNumber();
		float z = args[4].GetNumber();
		float o = args[5].GetNumber();
		
		m_Position[++m_PositionMax] = LocationVector( x, y, z, o );
		s->PushInteger( m_PositionMax );
		return 1;
	}

	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::GetPosition( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		if( id <= m_PositionMax && id > 0 )
		{
			s->PushNumber( m_Position[id].x );
			s->PushNumber( m_Position[id].y );
			s->PushNumber( m_Position[id].z );
			s->PushNumber( m_Position[id].o );
			return 4;
		}
	}

	s->PushNumber( 0.f );
	s->PushNumber( 0.f );
	s->PushNumber( 0.f );
	s->PushNumber( 0.f );
	return 4;
}

int LuaUnit::UpdateBattleResourceToPlayers( LuaState* s )
{
	MSG_S2C::stBattleGroundResourceUpdate msg;
	for( int i = 0; i < 3; ++i )
	{
		msg.resource[i] = m_BattleResource[i];
		msg.player_count[i] = m_BattlePlayerCount[i];
		m_BattleBannerCount[i] = 0;
	}
	for( int i = 0; i < 5; ++i )
	{
		BattleBanner* bb = GetBattleBannerByIndex( i );
		if( bb )
		{
			msg.banner_owner_state[i] = bb->OwnerRace;

			if( bb->OwnerRace < 10 && bb->OwnerRace > 0 )
			m_BattleBannerCount[bb->OwnerRace - 1]++;
		}
		else
			msg.banner_owner_state[i] = 0;
	}

	m_Raid->Broadcast( msg );

	/*
	char txt[1024] = { 0 };
	sprintf( txt, "资源更新:[妖族|%d]:%d [人族|%d]:%d [巫族|%d]%d", m_BattleBannerCount[0], m_BattleResource[0], m_BattleBannerCount[1], m_BattleResource[1], m_BattleBannerCount[2], m_BattleResource[2] );
	MSG_S2C::stNotify_Msg notify;
	notify.notify = txt;
	m_Raid->Broadcast( notify );
	*/
	return 0;
}

int LuaUnit::UpdateBattlePowerToPlayers( LuaState* s )
{
	MSG_S2C::stBattleGroundPowerUpdate msg;
	for( int i = 0; i < 3; ++i )
	{
		msg.side[i] = i;
		msg.power[i] = m_BattlePower[i];
		msg.num[i] = m_BattlePlayerCount[i];
		msg.groupName[i] = "";
	}
	msg.isParty = 0;
	m_Raid->Broadcast( msg );
	return 0;
}

int LuaUnit::BattleGroundEndByRace( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		if( m_Raid->GetCategory() == INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND )
		{
			int WinnerRace = args[2].GetInteger();
			int LoserRace = args[3].GetInteger();
			static_cast<SunyouAdvancedBattleGround*>( m_Raid )->OnBattleGroundEndByRace( WinnerRace, LoserRace );
		}
	}
	return 0;
}

int LuaUnit::BattleGroundBannerCaptured( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int Race = args[2].GetInteger();
		int Index = args[3].GetInteger();
		BattleBanner* bb = GetBattleBannerByIndex( Index );
		if( bb && Race > 0 && Race < 4 )
		{
			//char txt[1024] = { 0 };
			//sprintf( txt, "[%s族] 夺取了 [%s]", race_name[Race], bb->Name.c_str() );
			MSG_S2C::stNotify_Msg msg;
			msg.notify = build_language_string( BuildString_RaidBannerCaptrued, _race_name[Race], bb->Name.c_str() );//txt;
			m_Raid->Broadcast( msg );

			bb->OwnerRace = Race;
		}
		else
			assert( 0 );
	}
	return 0;
}

int LuaUnit::BattleGroundBannerDefended( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int Race = args[2].GetInteger();
		int Index = args[3].GetInteger();
		BattleBanner* bb = GetBattleBannerByIndex( Index );
		if( bb && Race > 0 && Race < 4 )
		{
			//char txt[1024] = { 0 };
			//sprintf( txt, "[%s族] 守住了 [%s]", race_name[Race], bb->Name.c_str() );
			MSG_S2C::stNotify_Msg msg;
			msg.notify = build_language_string( BuildString_RaidBannerDefended, _race_name[Race], bb->Name.c_str() );//txt;
			m_Raid->Broadcast( msg );

			bb->OwnerRace = Race;
		}
		else
			assert( 0 );
	}
	return 0;
}

int LuaUnit::BattleGroundBannerSuddenStricken( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int PlayerID = args[2].GetInteger();
		int Index = args[3].GetInteger();
		Player* p = objmgr.FindPlayerForLua( PlayerID );
		BattleBanner* bb = GetBattleBannerByIndex( Index );
		if( p && bb )
		{
			//char txt[1024] = { 0 };
			//sprintf( txt, "[%s族]的[%s] 突袭了 [%s]", race_name[p->getRace()], p->GetName(), bb->Name.c_str() );
			MSG_S2C::stNotify_Msg msg;
			msg.notify = build_language_string( BuildString_RaidBannerSudenStricken, _race_name[p->getRace()], p->GetName(), bb->Name.c_str() );//txt;
			m_Raid->Broadcast( msg );

			if( bb->OwnerRace < 10 )
				bb->OwnerRace = (bb->OwnerRace + 1) * 10 + p->getRace();
			else if( bb->OwnerRace < 20 )
				bb->OwnerRace = 10 + p->getRace();
			else
				bb->OwnerRace = (bb->OwnerRace % 10) * 10 + p->getRace();

			s->PushInteger( bb->OwnerRace );
			return 1;
		}
		else
			assert( 0 );
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::AddBattleGroundBanner( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsString() )
	{
		BattleBanner* p = new BattleBanner;
		p->OwnerRace = 0;
		p->Name = args[3].GetString();
		m_BattleBanners.push_back( p );
	}
	return 0;
}

int LuaUnit::ContainerMapCreate( LuaState* s )
{
	m_aLuaContainerMap[++m_ContainerMapMaxID] = new std::map<int, int>;
	s->PushInteger( m_ContainerMapMaxID );
	return 1;
}

int LuaUnit::ContainerMapInsert( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() && args[4].IsInteger() )
	{
		int id = args[2].GetInteger();
		int key = args[3].GetInteger();
		int value = args[4].GetInteger();

		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>* pm = m_aLuaContainerMap[id];
			if( pm )
			{
				(*pm)[key] = value;
			}
		}
	}
	return 0;
}

int LuaUnit::ContainerMapFind( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int key = args[3].GetInteger();

		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>* pm = m_aLuaContainerMap[id];
			if( pm )
			{
				std::map<int, int>::iterator it = pm->find( key );
				if( it != pm->end() )
				{
					s->PushInteger( it->second );
					return 1;
				}
			}
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::ContainerMapErase( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int key = args[3].GetInteger();

		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>* pm = m_aLuaContainerMap[id];
			if( pm )
			{
				pm->erase( key );
			}
		}
	}
	return 0;
}

int LuaUnit::ContainerMapDestroy( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>*& pm = m_aLuaContainerMap[id];
			if( pm )
			{
				delete pm;
				pm = NULL;
			}
		}
	}
	return 0;
}

int LuaUnit::ContainerMapBegin( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>* pm = m_aLuaContainerMap[id];
			if( pm )
			{
				std::list<std::pair<int, int> > storage;

				for( std::map<int, int>::iterator it = pm->begin(); it != pm->end(); ++it )
					storage.push_back( std::make_pair( it->first, it->second ) );

				m_ContainerMapTempStorage[id] = storage;
				s->PushInteger( pm->size() );
				return 1;
			}
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::ContainerMapNext( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		std::map<int, std::list<std::pair<int, int> > >::iterator it = m_ContainerMapTempStorage.find( id );
		if( it != m_ContainerMapTempStorage.end() )
		{
			std::list<std::pair<int, int> >& storage = it->second;
			if( !storage.empty() )
			{
				s->PushInteger( storage.front().first );
				s->PushInteger( storage.front().second );

				storage.pop_front();
				return 2;
			}
		}
	}
	s->PushInteger( 0 );
	s->PushInteger( 0 );
	return 2;
}

int LuaUnit::ContainerMapRandom( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>* pm = m_aLuaContainerMap[id];
			if( pm && pm->size() > 0 )
			{
				std::vector<std::map<int, int>::iterator > v;
				for( std::map<int, int>::iterator it = pm->begin(); it != pm->end(); ++it )
					v.push_back( it );

				std::map<int, int>::iterator it = v[rand() % v.size()];

				s->PushInteger( it->first );
				s->PushInteger( it->second );
				return 2;
			}
		}
	}
	s->PushInteger( 0 );
	s->PushInteger( 0 );
	return 2;
}

int LuaUnit::ContainerMapRandomExceptKey( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int ekey = args[3].GetInteger();
		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>* pm = m_aLuaContainerMap[id];
			if( pm && pm->size() > 0 )
			{
				std::vector<std::map<int, int>::iterator > v;
				for( std::map<int, int>::iterator it = pm->begin(); it != pm->end(); ++it )
				{
					if( it->first == ekey )
						continue;
					v.push_back( it );
				}
				if( v.size() > 0)
				{
					std::map<int, int>::iterator it = v[rand() % v.size()];
					s->PushInteger( it->first );
					s->PushInteger( it->second );
					return 2;
				}
			}
		}
	}
	s->PushInteger( 0 );
	s->PushInteger( 0 );
	return 2;
}

int LuaUnit::ContainerMapMinKey( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>* pm = m_aLuaContainerMap[id];
			if( pm && pm->size() > 0 )
			{
				std::map<int, int>::iterator it = m_aLuaContainerMap[id]->begin();

				s->PushInteger( it->first );
				s->PushInteger( it->second );
				return 2;
			}
		}
	}
	s->PushInteger( 0 );
	s->PushInteger( 0 );
	return 2;
}

int LuaUnit::ContainerMapMaxKey( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>* pm = m_aLuaContainerMap[id];
			if( pm && pm->size() > 0 )
			{
				std::map<int, int>::reverse_iterator it = m_aLuaContainerMap[id]->rbegin();

				s->PushInteger( it->first );
				s->PushInteger( it->second );
				return 2;
			}
		}
	}
	s->PushInteger( 0 );
	s->PushInteger( 0 );
	return 2;
}

int LuaUnit::ContainerMapClear( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>* pm = m_aLuaContainerMap[id];
			if( pm )
			{
				pm->clear();
			}
		}
	}
	return 0;
}

int LuaUnit::ContainerMapModifyValue( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() && args[4].IsInteger() )
	{
		int id = args[2].GetInteger();
		int key = args[3].GetInteger();
		int newValue = args[4].GetInteger();

		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>* pm = m_aLuaContainerMap[id];
			if( pm )
			{
				std::map<int, int>::iterator it = pm->find( key );
				if( it != pm->end() )
					it->second = newValue;
			}
		}
	}
	return 0;
}

int LuaUnit::ModifyObjectFaction( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int faction = args[3].GetInteger();

		Object* p = objmgr.FindObjectForLua( id );
		s->PushInteger( p->_getFaction() );

		if( p->IsGameObject() )
		{
			p->SetUInt32Value( GAMEOBJECT_FACTION, faction );
			p->_setFaction();
			p->UpdateOppFactionSet();
		}
		else if( p->IsUnit() )
		{
			p->SetUInt32Value( UNIT_FIELD_FACTIONTEMPLATE, faction );
			p->_setFaction();
			p->UpdateOppFactionSet();
		}
		return 1;
	}
	s->PushInteger( 0 );
	return 1;
}
int LuaUnit::CreateMazeStateMapInToContainerMap(LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger())
	{
		int mapid = args[2].GetInteger();
		std::map<int, int>* pm = NULL;
		if( mapid <= m_ContainerMapMaxID && mapid > 0 )
		{
			pm = m_aLuaContainerMap[mapid];
		}else
		{
			return 0 ;
		}

		int Endindex = 0;
		sWorld.GetMaze(*pm, Endindex);

		s->PushInteger(Endindex);
		return 1;
	}
	
	return 0;
}
int LuaUnit::GeneratePuzzlePositionIntoContainerMap( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() && args[7].IsNumber() && args[8].IsNumber() )
	{	
		int mapid = args[2].GetInteger();
		std::map<int, int>* pm = NULL;
		if( mapid <= m_ContainerMapMaxID && mapid > 0 )
		{
			pm = m_aLuaContainerMap[mapid];
		}
		else
			return 0;

		
		int num = args[3].GetInteger();
		float interval = args[4].GetNumber();
		float x = args[5].GetNumber();
		float y = args[6].GetNumber();
		float z = args[7].GetNumber();
		float o = args[8].GetNumber();

		LocationVector start( x, y, z, o );
		m_Position[++m_PositionMax] = start;
		(*pm)[1] = m_PositionMax;

		int index = 1;
		int xy = 0;
		int dir = 0;
		//fprintf( fp, "1: [%0.1f] [%0.1f]\n", start.x, start.y );
		for( int i = 0; i < num; )
		{
			dir = (index % 4 == 0 || index % 4 == 1) ? 1 : -1;
			for( int j = 0; j < index; ++j, ++i )
			{
				((float*)&start)[xy] += interval * dir;
				//fprintf( fp, "%d: [%0.1f] [%0.1f]\n", i+2, start.x, start.y );
				m_Position[++m_PositionMax] = start;
				(*pm)[i + 2] = m_PositionMax;
			}
			xy = !xy;
			index++;
		}
	}
	return 0;
}

int LuaUnit::ContainerMapShuffle( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		if( id <= m_ContainerMapMaxID && id > 0 )
		{
			std::map<int, int>* pm = m_aLuaContainerMap[id];
			if( pm )
			{
				std::vector<std::pair<int, int> > storage;
				for( std::map<int, int>::iterator it = pm->begin(); it != pm->end(); ++it )
					storage.push_back( std::make_pair( it->first, it->second ) );

				srand( (uint32)UNIXTIME );
				std::random_shuffle( storage.begin(), storage.end() );

				std::list<std::pair<int, int> > l;
				for( size_t i = 0; i < storage.size(); ++i )
					l.push_back( storage[i] );

				m_ContainerMapTempStorage[id] = l;
				s->PushInteger( pm->size() );
				return 1;
			}
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::SwitchCreatureAIMode( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Creature* p = objmgr.FindCreatureForLua( id );
		if( p )
			p->GetAIInterface()->m_ForceMoveMode = !p->GetAIInterface()->m_ForceMoveMode;
	}
	return 0;
}

int LuaUnit::DispelAura( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int count = args[3].GetInteger();
		Unit* p = objmgr.FindUnitForLua( id );
		if( p )
		{
			for(uint32 x=0; x<MAX_POSITIVE_AURAS; x++)
			{
				if( p->m_auras[x] && !p->m_auras[x]->IsRemainWhenDead() )
				{
					p->m_auras[x]->Remove();
					if( --count <= 0 )
						break;
				}
			}
		}
	}
	return 0;
}

int LuaUnit::CreatureForcePlayAnimation( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int CreatureID = args[2].GetInteger();
		int AnimationID = args[3].GetInteger();
		Creature* p = objmgr.FindCreatureForLua( CreatureID );
		if( p )
		{
			MSG_S2C::stCreatureForcePlayAnimation msg;
			msg.CreatureID = p->GetGUID();
			msg.AnimationID = AnimationID;
			m_Raid->Broadcast( msg );
		}
	}
	return 0;
}

int LuaUnit::CreatureForcePlayAnimationByName( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsString() )
	{
		int CreatureID = args[2].GetInteger();
		const char* AnimationName = args[3].GetString();
		Creature* p = objmgr.FindCreatureForLua( CreatureID );
		if( p )
		{
			MSG_S2C::stCreatureForcePlayAnimation msg;
			msg.CreatureID = p->GetGUID();
			msg.AnimationName = AnimationName;
			m_Raid->Broadcast( msg );
		}
	}
	return 0;
}

int LuaUnit::ConvertInteger2String( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int n = args[2].GetInteger();
		char str[32] = { 0 };
		sprintf( str, "%d", n );
		s->PushString( str );
		return 1;
	}
	s->PushString( "error!" );
	return 1;
}

int LuaUnit::ForceEjectAllPlayers( LuaState* s )
{
	std::set<Player*> tmp( m_Raid->m_players );
	for( std::set<Player*>::iterator it = tmp.begin(); it != tmp.end(); ++it )
	{
		Player* p = *it;
		p->EjectFromInstance();
	}
	return 0;
}

int LuaUnit::ForceGameStart( LuaState* s )
{
	m_bRaidInCombat = true;
	return 0;
}

int LuaUnit::SetChestLootGold( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int gold = args[3].GetInteger();
		GameObject* go = objmgr.FindGameObjectForLua( id );
		if( go )
			go->gold = gold;
	}

	return 0;
}

int LuaUnit::GetAuraStackCount( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int entry = args[3].GetInteger();

		Unit* u = objmgr.FindUnitForLua( id );
		if( u )
		{
			for(uint32 x=0; x<MAX_AURAS; x++)
			{
				if( u->m_auras[x] && u->m_auras[x]->GetSpellId() == entry )
				{
					s->PushInteger( u->m_auras[x]->m_AuraCount );
					return 1;
				}
			}
		}
	}
	s->PushInteger( 0 );
	return 1;
}
int LuaUnit::CalculateObjDistance(LuaState* s)
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger())
	{
		int idA = args[2].GetInteger();	
		int idB = args[3].GetInteger();	
		
		Object* p1 = objmgr.FindObjectForLua( idA );
		Object* p2 = objmgr.FindObjectForLua( idB );
		if( p1 && p2 )
		{
			LocationVector v2 = p2->GetPosition();
			LocationVector v1 = p1->GetPosition();
			s->PushNumber( v1.Distance( v2) );
			return 1;
		}

	}

	s->PushNumber( 9999 );
	return 1;
}
int LuaUnit::CalculatePositionDistanceD( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() )
	{
		int p1 = args[2].GetInteger();
		float x, y,z,o;
		x = args[3].GetNumber();
		y = args[4].GetNumber();
		z = args[5].GetNumber();
		o = args[6].GetNumber();
		std::map<int, LocationVector>::iterator it1 = m_Position.find( p1 );
		if( it1 != m_Position.end()  )
		{
			s->PushNumber( it1->second.Distance(x,y,z));
			return 1;
		}
	}

	s->PushNumber( 9999 );
	return 1;
}
int LuaUnit::CalculatePositionDistance( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int p1 = args[2].GetInteger();
		int p2 = args[3].GetInteger();
		std::map<int, LocationVector>::iterator it1 = m_Position.find( p1 );
		std::map<int, LocationVector>::iterator it2 = m_Position.find( p2 );
		if( it1 != m_Position.end() && it2 != m_Position.end() )
		{
			s->PushNumber( it1->second.Distance( it2->second ) );
			return 1;
		}
	}

	s->PushNumber( 9999 );
	return 1;
}

int LuaUnit::CalculateAngle( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int p1 = args[2].GetInteger();
		int p2 = args[3].GetInteger();
		std::map<int, LocationVector>::iterator it1 = m_Position.find( p1 );
		std::map<int, LocationVector>::iterator it2 = m_Position.find( p2 );
		if( it1 != m_Position.end() && it2 != m_Position.end() )
		{
			const LocationVector& a( it1->second );
			const LocationVector& b( it2->second );

			float angle = 3.14f * 3.f / 2.f - ( Object::calcAngle( a.x, a.y, b.x, b.y ) * 3.14f / 180.f );
			s->PushNumber( angle );
			return 1;
		}
	}

	s->PushNumber( 0.0 );
	return 1;
}

int LuaUnit::AddPlayerSpellCastLimit( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int entry = args[3].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
			p->AddOnlyCanCastSpell( entry );
	}

	return 0;
}

int LuaUnit::RemovePlayerSpellCastLimit( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int entry = args[3].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
			p->RemoveOnlyCanCastSpell( entry );
	}

	return 0;
}

int LuaUnit::ClearPlayerSpellCastLimit( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
			p->ClearOnlyCanCastSpell();
	}

	return 0;
}

int LuaUnit::SetUnitInvisible( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int inv = args[3].GetInteger();

		Unit* p = objmgr.FindUnitForLua( id );
		if( p )
		{
			p->m_invisible = inv != 0;
			p->bInvincible = inv != 0;
			p->UpdateVisibility();
		}
	}

	return 0;
}

int LuaUnit::LockManaRegeneration( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Unit* p = objmgr.FindUnitForLua( id );
		if( p )
			p->m_powerRegenerateLock = true;
	}
	return 0;
}

int LuaUnit::UnlockManaRegeneration( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Unit* p = objmgr.FindUnitForLua( id );
		if( p )
			p->m_powerRegenerateLock = false;
	}
	return 0;
}

int LuaUnit::AddUnitMana( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int mod = args[3].GetInteger();
		Unit* p = objmgr.FindUnitForLua( id );
		if( p )
		{
			if( (int)p->GetUInt32Value( UNIT_FIELD_POWER1 ) + mod > (int)p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) )
			{
				p->SetUInt32Value( UNIT_FIELD_POWER1, p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) );
			}
			else
				p->ModUnsigned32Value( UNIT_FIELD_POWER1, mod );
		}
	}
	return 0;
}

int LuaUnit::CreatePositionByStorage( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		uint32 entry = args[2].GetInteger();
		position_t* p = dbcPositionList.LookupEntry( entry );
		if( p )
		{
			m_Position[++m_PositionMax] = LocationVector( p->x, p->y, p->z, p->o );
			s->PushInteger( m_PositionMax );
		}
		else
			s->PushInteger( 0 );
	}
	else
		s->PushInteger( 0 );

	return 1;
}

int LuaUnit::GetAttachByPositionStorage( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		uint32 entry = args[2].GetInteger();
		position_t* p = dbcPositionList.LookupEntry( entry );
		
		if( p )
			s->PushInteger( p->mapid );
		else
			s->PushInteger( 0 );
	}
	else
		s->PushInteger( 0 );

	return 1;
}

int LuaUnit::ExchangeItem2Yuanbao( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() && args[4].IsInteger() && args[5].IsInteger() )
	{
		int player = args[2].GetInteger();
		int entry = args[3].GetInteger();
		int count = args[4].GetInteger();
		int yuanbao = args[5].GetInteger();
		Player* p = objmgr.FindPlayerForLua( player );
		if( p )
		{
			if( count <= p->GetItemInterface()->GetItemCount( entry ) )
			{
				if( p->AddCardPoints( yuanbao, Player::ADD_CARD_TRADE ) )
				{
					p->RemoveItemAmt( entry, count );
					s->PushInteger( 0 );
					return 1;
				}
				else
				{
					s->PushInteger( 1 );
					return 1;
				}
			}
		}
	}
	s->PushInteger( 2 );
	return 1;
}

int LuaUnit::ModifyGameObjectDynamicFlags( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int need = args[3].GetInteger();
		GameObject* go = objmgr.FindGameObjectForLua( id );
		if( go )
			go->SetUInt32Value( GAMEOBJECT_DYN_FLAGS, need );
	}
	return 0;
}

int LuaUnit::GetPlayerName( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			s->PushString( p->GetName() );
			return 1;
		}
	}
	s->PushString( "" );
	return 1;
}

int LuaUnit::EnableMeleeAttack( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int enable = args[3].GetInteger();

		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			p->m_EnableMeleeAttack = enable;
			return 0;
		}
	}
	return 0;
}

int LuaUnit::GetPlayerShapeShiftSpellEntry( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			s->PushInteger( p->m_ShapeShifted );
			return 1;
		}
	}

	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::ConsumeCurrency( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() && args[4].IsInteger() && args[5].IsInteger() )
	{
		int playerID = args[2].GetInteger();
		int currencyType = args[3].GetInteger();
		int itemEntry = args[4].GetInteger();
		int amount = args[5].GetInteger();

		Player* p = objmgr.FindPlayerForLua( playerID );
		if( p )
		{
			switch( currencyType )
			{
			case 0:// item
				{
					if( p->GetItemInterface()->GetItemCount( itemEntry ) < amount )
					{
						s->PushInteger( 0 );
						return 1;
					}
					p->RemoveItemAmt( itemEntry, amount );
				}
				break;
			case 1:// gold
				{
					if( p->GetUInt32Value( PLAYER_FIELD_COINAGE ) < amount )
					{
						s->PushInteger( 0 );
						return 1;
					}
					p->GetModPUInt32Value( PLAYER_FIELD_COINAGE, -amount );
				}
				break;
			case 2:// honor
				{
					if( p->GetUInt32Value( PLAYER_FIELD_HONOR_CURRENCY ) < amount )
					{
						s->PushInteger( 0 );
						return 1;
					}
					p->ModHonorCurrency( -amount );
				}
				break;
			case 3:// yuanbao
				{
					if( p->GetCardPoints() < amount )
					{
						s->PushInteger( 0 );
						return 1;
					}
					p->SpendPoints( amount, "instance" );
				}
				break;
			}
			s->PushInteger( 1 );
			return 1;
		}
	}

	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::GoldReward( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int amount = args[3].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
		{
			p->ModUnsigned32Value( PLAYER_FIELD_COINAGE, amount );
		}
	}
	return 0;
}

int LuaUnit::CreatureStopMove( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Creature* p = objmgr.FindCreatureForLua( id );
		if( p )
		{
			p->GetAIInterface()->ForceStopMove();
		}
	}
	return 0;
}

int LuaUnit::CheckLineOfSight( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber() && args[5].IsNumber() && args[6].IsNumber() && args[7].IsNumber() )
	{
		float x1 = args[2].GetNumber();
		float y1 = args[3].GetNumber();
		float z1 = args[4].GetNumber();
		float x2 = args[5].GetNumber();
		float y2 = args[6].GetNumber();
		float z2 = args[7].GetNumber();

		if( CollideInterface.CheckLOS( m_mapmgr->GetBaseMap()->GetMapIDForCollision(), x1, y1, z1 + 2.f, x2, y2, z2 + 2.f ) )
		{
			s->PushInteger( 1 );
		}
		else
		{
			s->PushInteger( 0 );
		}
	}
	else
		s->PushInteger( 0 );

	return 1;
}

int LuaUnit::GetLandHeight( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsNumber() && args[3].IsNumber() && args[4].IsNumber() )
	{
		float x = args[2].GetNumber();
		float y = args[3].GetNumber();
		float z = args[4].GetNumber();
		
		float h = CollideInterface.GetHeight( m_mapmgr->GetBaseMap(), x, y, z + 2.f );

		s->PushNumber( h );
	}
	else
		s->PushNumber( 0.f );

	return 1;
}

int LuaUnit::StopCreatureCasting( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		Creature* c = objmgr.FindCreatureForLua( id );
		if( c )
		{
			Spell* cs = c->GetCurrentSpell();
			if( cs )
				cs->cancel();
		}
	}
	return 0;
}

int LuaUnit::ModifyObjectVariable( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() && args[4].IsInteger() )
	{
		int id = args[2].GetInteger();
		int index = args[3].GetInteger();
		int value = args[4].GetInteger();
		Object* p = objmgr.FindObjectForLua( id );
		if( p && index >= 0 && index < 10 )
			p->m_varForLua[index] = value;
	}

	return 0;
}

int LuaUnit::GetObjectVariable( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int index = args[3].GetInteger();

		Object* p = objmgr.FindObjectForLua( id );
		if( p && index >= 0 && index < 10 )
			s->PushInteger( p->m_varForLua[index] );
		else
			s->PushInteger( 0 );
	}
	else
		s->PushInteger( 0 );

	return 1;
}

int LuaUnit::ClearObjectVariable( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		Object* p = objmgr.FindObjectForLua( id );
		if( p )
			memset( p->m_varForLua, 0, sizeof( p->m_varForLua ) );
	}

	return 0;
}

int LuaUnit::AddBuffToUnit( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int entry = args[3].GetInteger();

		Unit* p = objmgr.FindUnitForLua( id );
		SpellEntry* spellInfo = dbcSpell.LookupEntry( entry );

		if( p && spellInfo )
		{
			Spell * sp = new Spell( p, spellInfo, true, 0 );
			SpellCastTargets targets( p->GetGUID() );
			sp->prepare(&targets);
		}
	}
	return 0;
}

int LuaUnit::RemoveBuffFromUnit( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int entry = args[3].GetInteger();

		Unit* p = objmgr.FindUnitForLua( id );

		if( p )
		{
			p->RemoveAura( entry );
		}
	}
	return 0;
}
int LuaUnit::ContainerMultiMapClear( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		if( id <= m_ContainerMultiMapMaxID && id > 0 )
		{
			std::multimap<float, int>* pm = m_aLuaContainerMultiMap[id];
			if( pm )
			{
				pm->clear();
			}
		}
	}
	return 0;
}
int LuaUnit::ContainerMultiMapCreate( LuaState* s )
{
	m_aLuaContainerMultiMap[++m_ContainerMultiMapMaxID] = new std::multimap<float, int>;
	s->PushInteger( m_ContainerMultiMapMaxID );
	return 1;
}

int LuaUnit::ContainerMultiMapInsert( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[4].IsInteger() )
	{
		int id = args[2].GetInteger();
		float key = 0.f;
		if( args[3].IsNumber() )
			key = args[3].GetNumber();
		else if( args[3].IsInteger() )
			key = args[3].GetInteger();

		int value = args[4].GetInteger();

		if( id <= m_ContainerMultiMapMaxID && id > 0 )
		{
			std::multimap<float, int>* pm = m_aLuaContainerMultiMap[id];
			if( pm )
				pm->insert( std::make_pair( key, value ) );
		}
	}
	return 0;
}

int LuaUnit::ContainerMultiMapBegin( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();

		if( id <= m_ContainerMultiMapMaxID && id > 0 )
		{
			std::multimap<float, int>* pm = m_aLuaContainerMultiMap[id];
			if( pm )
			{
				m_ContainerMultiMapTempStorage.clear();
				for( std::multimap<float, int>::iterator it = pm->begin(); it != pm->end(); ++it )
					m_ContainerMultiMapTempStorage.push_front( std::make_pair( it->first, it->second ) );

				s->PushInteger( pm->size() );
				return 1;
			}
		}
	}
	s->PushInteger( 0 );
	return 1;
}

int LuaUnit::ContainerMultiMapNext( LuaState* s )
{
	if( !m_ContainerMultiMapTempStorage.empty() )
	{
		s->PushNumber( m_ContainerMultiMapTempStorage.front().first );
		s->PushInteger( m_ContainerMultiMapTempStorage.front().second );

		m_ContainerMultiMapTempStorage.pop_front();
		return 2;
	}


	s->PushNumber( 0.f );
	s->PushInteger( 0 );
	return 2;
}

int LuaUnit::KillCreature( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Unit* u = objmgr.FindUnitForLua( id );
		if( u )
		{
			u->SetUInt32Value( UNIT_FIELD_HEALTH, 0 );
			u->SetUInt32Value( UNIT_FIELD_POWER1, 0 );
			u->setDeathState( JUST_DIED );
		}
	}
	return 0;
}

int LuaUnit::CreateStructure( LuaState* s )
{
	m_mapDataStructs[++m_DataStructMax] = new data_struct;
	s->PushInteger( m_DataStructMax );
	return 1;
}

int LuaUnit::GetStructureData( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int dskey = args[2].GetInteger();
		int index = args[3].GetInteger();
		std::map<int, data_struct*>::iterator it = m_mapDataStructs.find( dskey );
		if( it != m_mapDataStructs.end() )
		{
			data_struct* ds = it->second;
			if( (uint32)index < ds->v.size() )
			{
				switch( ds->v[index].type )
				{
				case 's':
					s->PushString( ds->v[index].s );
					break;
				case 'i':
					s->PushInteger( ds->v[index].i );
					break;
				case 'f':
					s->PushNumber( ds->v[index].f );
					break;
				default:
					assert( 0 );
					return 0;
				}
				return 1;
			}
		}
	}
	assert( 0 );
	return 0;
}

int LuaUnit::DestroyStructure( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int dskey = args[2].GetInteger();
		std::map<int, data_struct*>::iterator it = m_mapDataStructs.find( dskey );
		if( it != m_mapDataStructs.end() )
		{
			delete it->second;
			m_mapDataStructs.erase( it );
		}
	}
	return 0;
}

int LuaUnit::AddData2Structure( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int dskey = args[2].GetInteger();

		std::map<int, data_struct*>::iterator it = m_mapDataStructs.find( dskey );
		if( it != m_mapDataStructs.end() )
		{
			data_struct* ds = it->second;
			if( args[3].IsInteger() )
				ds->v.push_back( data_t( 'i', args[3].GetInteger() ) );
			else if( args[3].IsNumber() )
				ds->v.push_back( data_t( 'f', (float)args[3].GetNumber() ) );
			if( args[3].IsString() )
				ds->v.push_back( data_t( 's', strdup(args[3].GetString()) ) );
		}
	}
	return 0;
}

int LuaUnit::ModifyDataFromStructure( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int dskey = args[2].GetInteger();
		int index = args[3].GetInteger();

		std::map<int, data_struct*>::iterator it = m_mapDataStructs.find( dskey );
		if( it != m_mapDataStructs.end() )
		{
			data_struct* ds = it->second;
			if( ds->v.size() < (uint32)index )
			{
				if( ds->v[index].type == 's' && ds->v[index].s )
					free( ds->v[index].s );

				if( args[4].IsInteger() )
				{
					ds->v[index].type = 'i';
					ds->v[index].i = args[4].GetInteger();
				}
				else if( args[4].IsNumber() )
				{
					ds->v[index].type = 'f';
					ds->v[index].f = args[4].GetNumber();
				}
				if( args[4].IsString() )
				{
					ds->v[index].type = 's';
					ds->v[index].s = strdup(args[4].GetString());
				}
			}
		}
	}
	return 0;
}

int LuaUnit::CreateStructureFromDatabase( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsString() && args[3].IsString() )
	{
		const char* TableName = args[2].GetString();
		const char* DataType = args[3].GetString();
		int len = strlen( DataType );

		QueryResult* qr = WorldDatabase.Query( "select * from %s", TableName );
		if( qr )
		{
			int first = m_DataStructMax + 1;
			do
			{
				data_struct* ds = new data_struct;
				m_mapDataStructs[++m_DataStructMax] = ds;
				Field* f = qr->Fetch();

				for( int i = 0; i < len; ++i )
				{
					switch( DataType[i] )
					{
					case 'i':
						ds->v.push_back( data_t( DataType[i], f[i].GetInt32() ) );
						break;
					case 'f':
						ds->v.push_back( data_t( DataType[i], f[i].GetFloat() ) );
						break;
					case 's':
						ds->v.push_back( data_t( DataType[i], strdup(f[i].GetString()) ) );
						break;
					}
				}
			}
			while( qr->NextRow() );

			delete qr;

			if( first <= m_DataStructMax )
			{
				s->PushInteger( first );
				s->PushInteger( m_DataStructMax );
				return 2;
			}
		}
	}

	s->PushInteger( 0 );
	s->PushInteger( 0 );
	return 2;
}

int LuaUnit::DisplayTextOnObjectHead( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsString() && args[4].IsInteger() )
	{
		int id = args[2].GetInteger();
		Object* p = objmgr.FindObjectForLua( id );
		if( p )
		{
			MSG_S2C::stDisplayTextOnObjectHead msg;
			msg.guid = p->GetGUID();
			msg.text = args[3].GetString();
			Player* ply = objmgr.FindPlayerForLua( args[4].GetInteger() );
			if( ply )
				msg.self_id = ply->GetLowGUID();
			else
				msg.self_id = 0;
			
			p->SendMessageToSet( msg, false );
		}
	}

	return 0;
}

int LuaUnit::AddExtraSpell2Player( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int entry = args[3].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
			p->AddExtraSpell( entry );
	}

	return 0;
}

int LuaUnit::RemoveExtraSpellFromPlayer( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int id = args[2].GetInteger();
		int entry = args[3].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if( p )
			p->RemoveExtraSpell( entry );
	}

	return 0;
}


int LuaUnit::AddNewBattleGroundResource( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int team = args[2].GetInteger();
		if( team >= 3 || team < 0 )
		{
			s->PushInteger( 0 );
			assert( 0 );
			return 1;
		}

		int resource = args[3].GetInteger();

		m_BattleResource[team] += resource;
		s->PushInteger( m_BattleResource[team] );
		return 1;
	}
	s->PushInteger( 0 );
	return 1;
}
int LuaUnit::UpdateNewBattleGroundResource( LuaState* s )
{
	return 0 ;
}
int LuaUnit::RemoveNewBattleGroundPower( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int team = args[2].GetInteger();
		if( team >= 3 || team < 0 )
		{
			s->PushInteger( 0 );
			assert( 0 );
			return 1;
		}

		int power = args[3].GetInteger();

		m_BattlePower[team] -= power;
		if (m_BattlePower[team] < 0)
			m_BattlePower[team] = 0;

		s->PushInteger( m_BattlePower[team] );
		return 1;
	}
	s->PushInteger( 0 );
	return 1;
}
int LuaUnit::AddNewBattleGroundPower( LuaState* s )
{
	LuaStack args( s );
	if( args[2].IsInteger() && args[3].IsInteger() )
	{
		int team = args[2].GetInteger();
		if( team >= 3 || team < 0 )
		{
			s->PushInteger( 0 );
			assert( 0 );
			return 1;
		}

		int power = args[3].GetInteger();

		m_BattlePower[team] += power;
		s->PushInteger( m_BattlePower[team] );
		return 1;
	}
	s->PushInteger( 0 );
	return 1;
}
int LuaUnit::UpdateNewBattleGroundPower( LuaState* s )
{
	MSG_S2C::stBattleGroundPowerUpdate msg;
	for( int i = 0; i < 3; ++i )
	{
		msg.side[i] = i;
		msg.power[i] = m_BattlePower[i];
		msg.num[i] = m_BattlePlayerCount[i];
		msg.groupName[i] = "";
	}
	LuaStack args( s );
	if (args[2].IsString())
		msg.groupName[0] = args[2].GetString();
	if (args[3].IsString())
		msg.groupName[1] = args[3].GetString();
	if (args[4].IsString())
		msg.groupName[2] = args[4].GetString();

	msg.isParty = 1;
	m_Raid->Broadcast( msg );
	return 0;
}
int LuaUnit::ModifyUnitPVPFlag( LuaState* s  )
{
	LuaStack args( s );
	if( args[2].IsInteger()  && args[3].IsInteger())
	{
		int UnitID = args[2].GetInteger();
		int PVPFlag = args[3].GetInteger();
		
		Unit* p = objmgr.FindUnitForLua( UnitID );
		if (p)
			p->SetFFA(PVPFlag);
	}
	return 0;
	
}
int LuaUnit::GetInstanceMinLevel(LuaState* s)
{
	if (m_Raid)
		s->PushInteger(m_Raid->m_conf.minlevel);
	else
		s->PushInteger(0);
	return 1;
}
int LuaUnit::NewBattleGetPlayerTeam( LuaState* s)
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		int id = args[2].GetInteger();
		Player* p = objmgr.FindPlayerForLua( id );
		if(p &&  m_Raid->GetCategory() == INSTANCE_CATEGORY_NEW_BATTLE_GROUND )
		{
			s->PushInteger(p->m_team_arena_idx);
			return 1;
		}		
	}
	s->PushInteger(3);
	return 1;
}
int LuaUnit::NewBattleGroundLoserTeam(LuaState* s)
{
	LuaStack args( s );
	if( args[2].IsInteger() )
	{
		if( m_Raid->GetCategory() == INSTANCE_CATEGORY_NEW_BATTLE_GROUND )
		{
			int loserTeam = args[2].GetInteger();
			if (loserTeam > 0 && loserTeam <= 3)
				static_cast<SunyouTeamGroupBattleGround*>( m_Raid )->OnTeamLoser(loserTeam);
		}
	}
	return 0;
}
int LuaUnit::NewBattleGroundEnd( LuaState* s)
{
	LuaStack args( s );
	if( args[2].IsInteger()  &&  args[3].IsString())
	{
		if( m_Raid->GetCategory() == INSTANCE_CATEGORY_NEW_BATTLE_GROUND )
		{
			int WinnerTeam = args[2].GetInteger();
			const char* str = args[3].GetString();
			if (WinnerTeam > 0 && WinnerTeam <= 3)
				static_cast<SunyouTeamGroupBattleGround*>( m_Raid )->OnBattleGroundEnd( WinnerTeam, str);
		}
	}
	return 0;
}
/////////////////////////////////////////////////////////////////////////////

void LuaUnit::OnPlayerDie( Player* p, Object* attacker )
{
	if( lf_OnUnitDie )
		(*lf_OnUnitDie)( p->GetUniqueIDForLua(), attacker->GetUniqueIDForLua() );
}

void LuaUnit::OnCreatureDie( Creature* c, Object* attacker )
{
	if( lf_OnUnitDie )
		(*lf_OnUnitDie)( c->GetUniqueIDForLua(), attacker->GetUniqueIDForLua() );
}

void LuaUnit::OnGameObjectTrigger( GameObject* go, Player* who )
{
	if( lf_OnGameObjectTrigger )
		(*lf_OnGameObjectTrigger)( go->GetUniqueIDForLua(), who->GetUniqueIDForLua() );
}

void LuaUnit::OnUnitEnterCombat( Unit* p )
{
	if( lf_OnUnitEnterCombat )
		(*lf_OnUnitEnterCombat)( p->GetUniqueIDForLua() );

	if( !m_bRaidInCombat )
		m_bRaidInCombat = true;
}

void LuaUnit::OnUnitLeaveCombat( Unit* p )
{
	if( p->IsCreature() )
		p->GetAIInterface()->m_ForceCasting = false;

	if( lf_OnUnitLeaveCombat )
		(*lf_OnUnitLeaveCombat)( p->GetUniqueIDForLua() );
}

void LuaUnit::OnPlayerResurrect( Player* p )
{
	if( lf_OnPlayerResurrect )
		(*lf_OnPlayerResurrect)( p->GetUniqueIDForLua() );
}

void LuaUnit::OnCreatureResurrect( Creature* p )
{
	if( ls_OnCreatureResurrect )
		(*ls_OnCreatureResurrect)( p->GetUniqueIDForLua() );
}

void LuaUnit::OnNPCGossipTrigger( Unit* npc, int gossip_index, Player* who )
{
	if( lf_OnNPCGossipTrigger )
		(*lf_OnNPCGossipTrigger)( npc->GetUniqueIDForLua(), gossip_index, who->GetUniqueIDForLua() );
}

void LuaUnit::OnRaidWipe()
{
	if( lf_OnRaidWipe )
		(*lf_OnRaidWipe)();
}

void LuaUnit::OnUnitCastSpell( int spell_id, Unit* who )
{
	if( who->IsCreature() )
	{
		if( who->GetAIInterface()->m_AIState == STATE_CASTING )
			who->GetAIInterface()->m_AIState = STATE_IDLE;
	}
	if( lf_OnUnitCastSpell )
		(*lf_OnUnitCastSpell)( spell_id, who->GetUniqueIDForLua() );
}

void LuaUnit::OnUnitBeginCastSpell( int spell_id, Unit* who )
{
	if( lf_OnUnitBeginCastSpell )
		(*lf_OnUnitBeginCastSpell)( spell_id, who->GetUniqueIDForLua() );
}

void LuaUnit::OnEvent( int evt_id, int evt_attach[] )
{
	if( lf_OnEvent )
		(*lf_OnEvent)( evt_id, evt_attach[0], evt_attach[1], evt_attach[2], evt_attach[3], evt_attach[4] );
}

void LuaUnit::OnCreatureMoveArrival( Creature* p )
{
	if( lf_OnCreatureMoveArrival )
		(*lf_OnCreatureMoveArrival)( p->GetUniqueIDForLua() );
}

void LuaUnit::OnGameObjectDespawn( GameObject* p )
{
	if( lf_OnGameObjectDespawn )
		(*lf_OnGameObjectDespawn)( p->GetUniqueIDForLua() );
}

void LuaUnit::OnPlayerLootItem( Player* p, uint32 entry, uint32 amount )
{
	if( lf_OnPlayerLootItem )
		(*lf_OnPlayerLootItem)( p->GetUniqueIDForLua(), entry, amount );
}

void LuaUnit::OnUnitHitWithSpell( int spell_id, Unit* attacker, Unit* victim )
{
	if( lf_OnUnitHitWithSpell )
		(*lf_OnUnitHitWithSpell)( spell_id, attacker->GetUniqueIDForLua(), victim->GetUniqueIDForLua() );
}

void LuaUnit::OnDynamicObjectCreated( Object* caster, DynamicObject* dynobj )
{
	if( lf_OnDynamicObjectCreated )
		(*lf_OnDynamicObjectCreated)( caster->GetUniqueIDForLua(), dynobj->GetUniqueIDForLua() );
}

void LuaUnit::OnDynamicObjectMoveArrival( DynamicObject* p )
{
	if( lf_OnDynamicObjectMoveArrival )
		(*lf_OnDynamicObjectMoveArrival)( p->GetUniqueIDForLua() );
}

void LuaUnit::OnDynamicObjectDestroyed( DynamicObject* p )
{
	if( lf_OnDynamicObjectDestroyed )
		(*lf_OnDynamicObjectDestroyed)( p->GetUniqueIDForLua() );
}

