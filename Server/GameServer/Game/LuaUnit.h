#ifndef _LUA_FATHER_HEAD
#define _LUA_FATHER_HEAD

#include "LuaInterface.h"

class SunyouRaid;

class LuaUnit : public InstanceLuaObject<LuaUnit>
{
public:
	struct Event
	{
		int id;
		int interval;
		int count;
		int lasttime;
		int attach[5];
	};

	LuaUnit();
	virtual ~LuaUnit();

	void Init( SunyouRaid* raid, const char* lua_script, uint32 lua_hash );
	void Update();
	void Release();	
	void OnInstanceStartup();
	void OnPlayerJoin( Player* p );
	void OnPlayerLeave( Player* p );

	int SpawnGameObject( LuaState* s );
	int SpawnCreature( LuaState* s );
	int HasObject( LuaState* s );
	int InsertNPCGossip( LuaState* s );
	int EnableNPCGossip( LuaState* s );
	int ModifyThreat( LuaState* s );
	int DespawnCreature( LuaState* s );
	int DespawnGameObject( LuaState* s );
	int CreatureSay( LuaState* s );
	int EjectPlayerFromInstance( LuaState* s );
	int GetPlayerBasicInfo( LuaState* s );
	int GetCreatureBasicInfo( LuaState* s );
	int TeleportPlayer( LuaState* s );
	int TeleportCreature( LuaState* s );
	int ModifyCreatureHPPct( LuaState* s );
	int ModifyCreatureMPPct( LuaState* s );
	int ModifyPlayerHPPct( LuaState* s );
	int ModifyPlayerMPPct( LuaState* s );
	int ResetPlayerCooldown( LuaState* s );
	int CreatureCastSpell( LuaState* s );
	int AddBuffToPlayer( LuaState* s );
	int RemoveBuffFromPlayer( LuaState* s );
	int RemoveBuffFromCreature( LuaState* s );
	int CreatureMoveTo( LuaState* s );
	int GetCreatureThreatListCount( LuaState* s );
	int GetCreatureTargetByThreatIndex( LuaState* s );
	int GetRandomPlayerByClass( LuaState* s );
	int CreatureForceLeaveCombat( LuaState* s );
	int ModifyObjectScale( LuaState* s );
	int CreateThreatChain( LuaState* s );
	int AddCreatureToThreatChain( LuaState* s );
	int GetThreatChainByCreature( LuaState* s );
	int RemoveCreatureFromThreatChain( LuaState* s );
	int CreateEvent( LuaState* s );
	int KillEvent( LuaState* s );
	int ModifyPlayerPVPFlag( LuaState* s );
	int PlaySound( LuaState* s );
	int RandomShufflePlayers( LuaState* s );
	int GetNextRandomShufflePlayer( LuaState* s );
	int GetCreatureThreat( LuaState* s );
	int SetCreatureImmune( LuaState* s );
	int ResurrectPlayer( LuaState* s );
	int GetObjectPosition( LuaState* s );
	int SendSystemNotify( LuaState* s );
	int GetMinHPPctCreatureFromThreatChain( LuaState* s );
	int GetMinHPPctUnitFromCreatureThreatList( LuaState* s );
	int GetUnitTarget( LuaState* s );
	int SummonPlayer( LuaState* s );
	int ResurrectCreature( LuaState* s );
	int GetGameObjectEntry( LuaState* s );
	int GetCreatureEntry( LuaState* s );

	int DismissMountAndShift( LuaState* s );
	int ScreenShake( LuaState* s );
	int SceneEffect( LuaState* s );
	int DespawnCreatureByEntry( LuaState* s );
	int DespawnGameObjectByEntry( LuaState* s );
	int AddPlayerTitle( LuaState* s );
	int HonorReward( LuaState* s );
	int ItemReward( LuaState* s );
	int GetPlayerRace( LuaState* s );
	int CreatureWhisper( LuaState* s );
	int ModifyBattleGroundPower( LuaState* s );
	int HasBuff( LuaState* s );
	int ModifyBattleGroundResource( LuaState* s );
	int ModifyBattleBannerCount( LuaState* s );
	int GetGameObjectFlag( LuaState* s );
	int ModifyGameObjectFlag( LuaState* s );
	int ForceRootPlayer( LuaState* s );
	int ForceUnrootPlayer( LuaState* s );
	int AddPlayerResurrectPosition( LuaState* s );
	int SearchNearestPlayerResurrectPosition( LuaState* s );
	int RemovePlayerResurrectPosition( LuaState* s );
	int CreatePosition( LuaState* s );
	int GetPosition( LuaState* s );
	int UpdateBattleResourceToPlayers( LuaState* s );
	int UpdateBattlePowerToPlayers( LuaState* s );
	int BattleGroundEndByRace( LuaState* s );
	int BattleGroundBannerCaptured( LuaState* s );
	int BattleGroundBannerDefended( LuaState* s );
	int BattleGroundBannerSuddenStricken( LuaState* s );
	int AddBattleGroundBanner( LuaState* s );
	int ContainerMapCreate( LuaState* s );
	int ContainerMapInsert( LuaState* s );
	int ContainerMapFind( LuaState* s );
	int ContainerMapErase( LuaState* s );
	int ContainerMapDestroy( LuaState* s );
	int ContainerMapBegin( LuaState* s );
	int ContainerMapNext( LuaState* s );
	int ContainerMapRandom( LuaState* s );
	int ContainerMapRandomExceptKey( LuaState* s );
	int ContainerMapMinKey( LuaState* s );
	int ContainerMapMaxKey( LuaState* s );
	int ContainerMapClear( LuaState* s );
	int ContainerMapModifyValue( LuaState* s );
	int ModifyObjectFaction( LuaState* s );
	int GeneratePuzzlePositionIntoContainerMap( LuaState* s );
	int CreateMazeStateMapInToContainerMap(LuaState* s );
	int ContainerMapShuffle( LuaState* s );
	int SwitchCreatureAIMode( LuaState* s );
	int DispelAura( LuaState* s );
	int CreatureForcePlayAnimation( LuaState* s );
	int CreatureForcePlayAnimationByName( LuaState* s );
	int ConvertInteger2String( LuaState* s );
	int ForceEjectAllPlayers( LuaState* s );
	int ForceGameStart( LuaState* s );
	int SetChestLootGold( LuaState* s );
	int GetAuraStackCount( LuaState* s );
	int CalculatePositionDistance( LuaState* s );
	int CalculatePositionDistanceD( LuaState* s );
	int CalculateObjDistance(LuaState* s);
	int CalculateAngle( LuaState* s );
	int AddPlayerSpellCastLimit( LuaState* s );
	int RemovePlayerSpellCastLimit( LuaState* s );
	int ClearPlayerSpellCastLimit( LuaState* s );
	int SetUnitInvisible( LuaState* s );
	int LockManaRegeneration( LuaState* s );
	int UnlockManaRegeneration( LuaState* s );
	int AddUnitMana( LuaState* s );
	int CreatePositionByStorage( LuaState* s );
	int GetAttachByPositionStorage( LuaState* s );
	int ExchangeItem2Yuanbao( LuaState* s );
	int ModifyGameObjectDynamicFlags( LuaState* s );
	int GetPlayerName( LuaState* s );
	int EnableMeleeAttack( LuaState* s );
	int GetPlayerShapeShiftSpellEntry( LuaState* s );
	int ConsumeCurrency( LuaState* s );
	int GoldReward( LuaState* s );
	int CreatureStopMove( LuaState* s );
	int CheckLineOfSight( LuaState* s );
	int GetLandHeight( LuaState* s );
	int StopCreatureCasting( LuaState* s );
	int ModifyObjectVariable( LuaState* s );
	int GetObjectVariable( LuaState* s );
	int ClearObjectVariable( LuaState* s );
	int AddBuffToUnit( LuaState* s );
	int RemoveBuffFromUnit( LuaState* s );
	int ContainerMultiMapCreate( LuaState* s );
	int ContainerMultiMapClear( LuaState* s );
	int ContainerMultiMapInsert( LuaState* s );
	int ContainerMultiMapBegin( LuaState* s );
	int ContainerMultiMapNext( LuaState* s );
	int KillCreature( LuaState* s );
	int CreateStructure( LuaState* s );
	int GetStructureData( LuaState* s );
	int DestroyStructure( LuaState* s );
	int AddData2Structure( LuaState* s );
	int ModifyDataFromStructure( LuaState* s );
	int CreateStructureFromDatabase( LuaState* s );
	int DisplayTextOnObjectHead( LuaState* s );
	int AddExtraSpell2Player( LuaState* s );
	int RemoveExtraSpellFromPlayer( LuaState* s );
	
	// new BattleGround interface
	int AddNewBattleGroundResource( LuaState* s );
	int UpdateNewBattleGroundResource( LuaState* s );
	int AddNewBattleGroundPower( LuaState* s );
	int RemoveNewBattleGroundPower( LuaState* s );
	int UpdateNewBattleGroundPower( LuaState* s );
	int NewBattleGroundEnd( LuaState* s);
	int NewBattleGroundLoserTeam(LuaState* s);
	int NewBattleGetPlayerTeam( LuaState* s);
	int GetInstanceMinLevel(LuaState* s);

	int ModifyUnitPVPFlag( LuaState* s);
	int ModifyDynamicObjectDisplayID( LuaState* s );
	int DynamicObjectMoveTo( LuaState* s );

	void OnPlayerDie( Player* p, Object* attacker );
	void OnCreatureDie( Creature* c, Object* attacker );
	void OnGameObjectTrigger( GameObject* go, Player* who );
	void OnUnitEnterCombat( Unit* p );
	void OnUnitLeaveCombat( Unit* p );
	void OnPlayerResurrect( Player* p );
	void OnCreatureResurrect( Creature* p );
	void OnNPCGossipTrigger( Unit* npc, int gossip_index, Player* who );
	void OnRaidWipe();
	void OnUnitCastSpell( int spell_id, Unit* who );
	void OnUnitBeginCastSpell( int spell_id, Unit* who );
	void OnEvent( int evt_id, int evt_attach[] );
	void OnCreatureMoveArrival( Creature* p );
	void OnGameObjectDespawn( GameObject* p );
	void OnPlayerLootItem( Player* p, uint32 entry, uint32 amount );
	void OnUnitHitWithSpell( int spell_id, Unit* attacker, Unit* victim );
	void OnDynamicObjectCreated( Object* caster, DynamicObject* dynobj );
	void OnDynamicObjectMoveArrival( DynamicObject* p );
	void OnDynamicObjectDestroyed( DynamicObject* p );


protected:
	SunyouRaid* m_Raid;
	bool m_bRaidInCombat;
	std::set<int> m_threatChains;
	std::map<int, Event*> m_events;
	uint32 m_LastLuaUpdateTime;
	int m_eventID;
	std::vector<Player*> m_listRandomShufflePlayers;

	enum { CONTAINER_MAX = 1024 };
	std::map<int, int>* m_aLuaContainerMap[CONTAINER_MAX];
	int m_ContainerMapMaxID;

	std::multimap<float, int>* m_aLuaContainerMultiMap[CONTAINER_MAX];
	int m_ContainerMultiMapMaxID;

	int m_BattlePower[3];
	int m_BattleResource[3];
	int m_BattleBannerCount[3];
	int m_BattlePlayerCount[3];
	struct BattleBanner
	{
		int OwnerRace;
		std::string Name;
	};
	std::vector<BattleBanner*> m_BattleBanners;
	inline BattleBanner* GetBattleBannerByIndex( int idx )
	{
		if( idx >= 0 && idx < (int)m_BattleBanners.size() )
			return m_BattleBanners[idx];
		else
			return NULL;
	}
	std::map<int, LocationVector> m_Position;
	int m_PositionMax;
	std::set<int> m_ResurrectPosition[3];
	std::map<int, std::list<std::pair<int, int> > > m_ContainerMapTempStorage;
	std::list<std::pair<float, int> > m_ContainerMultiMapTempStorage;
	std::set<uint32> m_CreatureStorage;
	std::set<uint32> m_GameObjectStorage;

	struct data_t {
		char type;
		union {
			int i;
			float f;
			char* s;
		};

		data_t() : type( 0 ), i( 0 ) {}
		data_t( char t, int value ) : type( t ), i( value ) {}
		data_t( char t, float value ) : type( t ), f( value ) {}
		data_t( char t, char* value ) : type( t ), s( value ) {}
		~data_t() {
			if( type == 's' )
				free( s );
		}
	};
	struct data_struct {
		std::vector<data_t> v;
	};
	std::map<int, data_struct*> m_mapDataStructs;
	int m_DataStructMax;

	LuaFunction<void>* lf_OnUnitDie;
	LuaFunction<void>* lf_OnGameObjectTrigger;
	LuaFunction<void>* lf_OnUnitEnterCombat;
	LuaFunction<void>* lf_OnUnitLeaveCombat;
	LuaFunction<void>* lf_OnPlayerResurrect;
	LuaFunction<void>* ls_OnCreatureResurrect;
	LuaFunction<void>* lf_OnNPCGossipTrigger;
	LuaFunction<void>* lf_OnRaidWipe;
	LuaFunction<void>* lf_OnUnitCastSpell;
	LuaFunction<void>* lf_OnUnitBeginCastSpell;
	LuaFunction<void>* lf_OnEvent;
	LuaFunction<void>* lf_OnEnd;
	LuaFunction<void>* lf_OnUpdate;
	LuaFunction<void>* lf_OnPlayerEnterInstance;
	LuaFunction<void>* lf_OnPlayerLeaveInstance;
	LuaFunction<void>* lf_OnStart;
	LuaFunction<void>* lf_OnCreatureMoveArrival;
	LuaFunction<void>* lf_OnGameObjectDespawn;
	LuaFunction<void>* lf_OnPlayerLootItem;
	LuaFunction<void>* lf_OnUnitHitWithSpell;
	LuaFunction<void>* lf_OnDynamicObjectCreated;
	LuaFunction<void>* lf_OnDynamicObjectMoveArrival;
	LuaFunction<void>* lf_OnDynamicObjectDestroyed;

	bool m_bEventChanged;
	MapMgr* m_mapmgr;
};

#endif