#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Mail.h"
#include "../../SDBase/Protocol/S2C_Mail.h"
#include "MultiLanguageStringMgr.h"

initialiseSingleton(MailSystem);

void MailSystem::StartMailSystem()
{
	m_maxID = 1;
	QueryResult * result = CharacterDatabase.Query("SELECT MAX(message_id) FROM mailbox");
	if(result)
	{
		m_maxID = result->Fetch()[0].GetUInt32()+1;
		delete result;
	}

	MakeGUIDByGroupID( sMaster.m_group_id, m_maxID );
}

MailError MailSystem::DeliverMessage(uint64 recipent, MailMessage* message)
{
	if( g_crosssrvmgr->m_isInstanceSrv )
		return MAIL_OK;

	// assign a new id
	message->message_id = Generate_Message_Id();

	Player * plr = objmgr.GetPlayer((uint32)recipent);
	if(plr != NULL)
	{
		plr->m_mailBox->AddMessage(message);
		if((uint32)UNIXTIME >= message->delivery_time)
		{
			uint32 v = 0;
			MSG_S2C::stMail_Received Msg;
			Msg.unk = 0;
			plr->GetSession()->SendPacket( Msg );
		}
	}

	SaveMessageToSQL(message);
	return MAIL_OK;
}

void Mailbox::AddMessage(MailMessage* Message)
{
	Messages[Message->message_id] = *Message;
}

void Mailbox::DeleteMessage(uint32 MessageId, bool sql)
{
	Messages.erase(MessageId);
	if(sql)
		sWorld.ExecuteSqlToDBServer("DELETE FROM mailbox WHERE message_id = %u", MessageId);
}

void Mailbox::BuildMailboxListingPacket(MSG_S2C::stMail_List_Result* MsgMailList)
{
	MessageMap::iterator itr;
	uint32 count = 0;
	uint32 t = (uint32)UNIXTIME;

	// do cleanup on request mail
	CleanupExpiredMessages();

	for(itr = Messages.begin(); itr != Messages.end(); ++itr)
	{
		if(itr->second.expire_time && t > itr->second.expire_time)
			continue;	   // expired mail -> skip it

		if((uint32)UNIXTIME < itr->second.delivery_time)
			continue;		// undelivered
		MSG_S2C::stMail_List_Result::stMail Mail;
		if(itr->second.AddMessageDataToPacket(&Mail))
			++count;
		MsgMailList->vMails.push_back(Mail);
		if(count == 50)
			break;
	}
}

void Mailbox::CleanupExpiredMessages()
{
	MessageMap::iterator itr, it2;
	uint32 curtime = (uint32)UNIXTIME;

	for(itr = Messages.begin(); itr != Messages.end();)
	{
		it2 = itr++;
		if(it2->second.expire_time && it2->second.expire_time < curtime)
		{
			if( it2->second.cod )
			{
				//sMailSystem.SaveMessageToSQL( &it2->second);

				MailMessage msg;
				msg.message_type = NORMAL;
				msg.sender_guid = it2->second.player_guid;
				msg.player_guid = it2->second.sender_guid;
				msg.subject = build_language_string( BuildString_MailboxSystemReturn );//"系统退回";
				msg.body = "";
				msg.money = it2->second.money;
				msg.cod = 0;
				msg.items = it2->second.items;
				msg.stationary = it2->second.stationary;
				msg.delivery_time = (uint32)UNIXTIME;
				msg.expire_time = 0;
				msg.read_flag = false;
				msg.copy_made = false;
				msg.deleted_flag = false;
				msg.yp = it2->second.yp;
				msg.is_yuanbao = it2->second.is_yuanbao;
				it2->second.cod = 0;

				// Send the message.
				sMailSystem.DeliverMessage(msg.player_guid, &msg);
				sWorld.ExecuteSqlToDBServer("DELETE FROM mailbox WHERE message_id = %u", it2->second.message_id);
			}
			Messages.erase(it2);
		}
	}
}

bool MailMessage::AddMessageDataToPacket(void* voidMail)
{
	MSG_S2C::stMail_List_Result::stMail* Mail = (MSG_S2C::stMail_List_Result::stMail*)voidMail;
	uint8 i = 0;
	uint32 j;
// 	size_t pos;
	vector<uint64>::iterator itr;
	Item * pItem;

	// add stuff
	if(deleted_flag)
		return false;

	Mail->message_id = message_id;
	Mail->message_type = uint8(message_type);
	Mail->sender_guid = sender_guid;

	Mail->cod 	=  cod;			// cod
	Mail->stationary = stationary;
	Mail->money		= money;		// money
	if( expire_time > 0 )
		Mail->expire_time = ui32((expire_time - (uint32)UNIXTIME) / 86400.0f);
	else
		Mail->expire_time = 0;
	Mail->subject	= subject;
	Mail->read_flag = read_flag;
	Mail->yp = yp;
	Mail->is_yuanbao = is_yuanbao;
	Mail->sender_name = sender_name;

	if( !items.empty( ) )
	{
		for( itr = items.begin( ); itr != items.end( ); ++itr )
		{
			pItem = objmgr.LoadItem( *itr );
			if( pItem == NULL )
				continue;

			MSG_S2C::stMail_List_Result::stMail::stMailItem mailItem;
			mailItem.guid = pItem->GetGUID();
			mailItem.entry = pItem->GetEntry();

			for( int i = 0; i < 6; ++i )
				mailItem.extra.enchants[i] = pItem->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + i * 3 );
			mailItem.extra.jinglian_level	= pItem->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);

			mailItem.stack_count = uint8( pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT) );
			mailItem.ChargesLeft = uint32( pItem->GetChargesLeft() );
			mailItem.maxdurability = pItem->GetUInt32Value( ITEM_FIELD_MAXDURABILITY );
			mailItem.durability = pItem->GetUInt32Value( ITEM_FIELD_DURABILITY );
			Mail->vMailItems.push_back(mailItem);
		}
	}

	return true;
}

void MailSystem::SaveMessageToSQL(MailMessage * message)
{
	stringstream ss;
	vector< uint64 >::iterator itr;
	ss << "REPLACE INTO mailbox VALUES("
		<< message->message_id << ","
		<< message->message_type << ","
		<< message->player_guid << ","
		<< message->sender_guid << ",'"
		<< message->subject << "','"
		<< message->body << "',"
		<< message->money << ",'";

	for( itr = message->items.begin( ); itr != message->items.end( ); ++itr )
		ss << (*itr) << ",";

	ss << "',"
		<< message->cod << ","
		<< message->stationary << ","
		<< message->expire_time << ","
		<< message->delivery_time << ","
		<< message->copy_made << ","
		<< message->read_flag << ","
		<< message->deleted_flag << ","
		<< (int)message->yp << ","
		<< (int)message->is_yuanbao << ",'"
		<< message->sender_name.c_str() << "')";
	sWorld.ExecuteSqlToDBServer(ss.str().c_str());
}

void WorldSession::HandleSendMail(CPacketUsn& packet )
{
	MailMessage msg;
	uint64 gameobject;
	uint8 itemcount;
	uint8 itemslot;
	uint8 i;
	uint64 itemguid;
	set< Item* > items;
	set< Item* >::iterator itr;
	string recepient;
	Item * pItem;
	//uint32 err = MAIL_OK;

	MSG_C2S::stMail_Send MsgRecv;packet>>MsgRecv;
	gameobject = MsgRecv.gameobject;
	recepient = MsgRecv.recepient;
	msg.subject = MsgRecv.subject;
	msg.body = MsgRecv.body;
	msg.stationary = 0;//MsgRecv.stationary;

	itemcount = (ui32)MsgRecv.vMailItems.size();
	if( itemcount > 12 )
	{
		//SystemMessage("Sorry,  not support sending multiple items at this time. (don't want to lose your item do you) Remove some items, and try again.");
		SendMailError(MAIL_ERR_INTERNAL_ERROR);
		return;
	}

	for( i = 0; i < itemcount; ++i )
	{
		itemslot = MsgRecv.vMailItems[i].itemslot;
		itemguid = MsgRecv.vMailItems[i].itemguid;

        pItem = _player->GetItemInterface()->GetItemByGUID( itemguid );
		if( pItem == NULL || pItem->HasFlag( ITEM_FIELD_FLAGS, ITEM_FLAG_CONJURED ) )
		{
			SendMailError( MAIL_ERR_INTERNAL_ERROR );
			return;
		}
		if(pItem->IsSoulbound() && !GetPermissionCount())
		{
			SendMailError( MAIL_ERR_INTERNAL_ERROR );
			return;
		}

		items.insert( pItem );
	}

	msg.money = MsgRecv.money;
	msg.cod	= MsgRecv.cod;
	// left over: (TODO- FIX ME BURLEX!)
	// uint32
	// uint32
	// uint8

	// Search for the recipient
	PlayerInfo* player = ObjectMgr::getSingleton().GetPlayerInfoByName(recepient.c_str());
	if( player == NULL )
	{
		SendMailError( MAIL_ERR_RECIPIENT_NOT_FOUND );
		return;
	}

	bool interfaction = false;
// 	if( sMailSystem.MailOption( MAIL_FLAG_CAN_SEND_TO_OPPOSITE_FACTION ) || (HasGMPermissions() && sMailSystem.MailOption( MAIL_FLAG_CAN_SEND_TO_OPPOSITE_FACTION_GM ) ) )
// 	{
// 		interfaction = true;
// 	}

	// Check we're sending to the same faction (disable this for testing)
	/*
	if( player->race != _player->getRace() && !interfaction && !GetPermissionCount() )
	{
		SendMailError( MAIL_ERR_NOT_YOUR_ALLIANCE );
		return;
	}
	*/

	// Check if we're sending mail to ourselves
	if(player->name == _player->GetName() && !GetPermissionCount())
	{
		SendMailError(MAIL_ERR_CANNOT_SEND_TO_SELF);
		return;
	}

	if( msg.stationary == 0x3d || msg.stationary == 0x3d && !HasGMPermissions())
	{
		SendMailError(MAIL_ERR_INTERNAL_ERROR);
		return;
	}

	// Instant delivery time by default.
	msg.delivery_time = (uint32)UNIXTIME;

	// Set up the cost
	int32 cost = 0;
	//if( !sMailSystem.MailOption( MAIL_FLAG_DISABLE_POSTAGE_COSTS ) && !( GetPermissionCount() && sMailSystem.MailOption( MAIL_FLAG_NO_COST_FOR_GM ) ) )
	{
		cost = 30;
	}

	// Check for attached money
	if( msg.money > 0 )
		cost += msg.money;

	if( cost < 0 )
	{
		SendMailError(MAIL_ERR_INTERNAL_ERROR);
		return;
	}

	// check that we have enough in our backpack
	if( (int32)_player->GetUInt32Value( PLAYER_FIELD_COINAGE ) < cost )
	{
		SendMailError( MAIL_ERR_NOT_ENOUGH_MONEY );
		return;
	}

	//MyLog::yunyinglog->info("MailSendBegin From[%u][%s] To[%u][%s]");
	// Check for the item, and required item.
	if( !items.empty( ) )
	{
		MyLog::yunyinglog->info("With Item");
		for( itr = items.begin(); itr != items.end(); ++itr )
		{
			pItem = *itr;
			if( _player->GetItemInterface()->SafeRemoveAndRetreiveItemByGuid(pItem->GetGUID(), false) != pItem )
				continue;		// should never be hit.

			pItem->RemoveFromWorld();
			pItem->SetOwner( NULL );
			pItem->SaveToDB( INVENTORY_SLOT_NOT_SET, 0, true, NULL );
			msg.items.push_back( pItem->GetUInt32Value(OBJECT_FIELD_GUID) );
			MyLog::yunyinglog->info("item-send-player[%u][%s] item["I64FMT"][%u] count[%u] to player[%u][%s]", _player->GetLowGUID(), _player->GetName(), pItem->GetGUID(), pItem->GetProto()->ItemId, pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT), player->guid, player->name);

			if( GetPermissionCount() > 0 )
			{
			}

			delete pItem;
		}
	}

	if(msg.money != 0 || msg.cod != 0 || !msg.items.size() && player->acct != _player->GetSession()->GetAccountId())
	{
		if(!sMailSystem.MailOption(MAIL_FLAG_DISABLE_HOUR_DELAY_FOR_ITEMS))
			msg.delivery_time += 3600;  // 1hr
	}

	// take the money
	_player->ModCoin(-cost);
	MyLog::yunyinglog->info("gold-sendmail-player[%u][%s] cost gold[%u] send gold[%u] to player[%u][%s]", _player->GetLowGUID(), _player->GetName(), cost-msg.money, msg.money, player->guid, player->name);

	// Fill in the rest of the info
	msg.player_guid = player->guid;
	msg.sender_guid = _player->GetGUID();
	msg.sender_name = _player->GetName();

	// 30 day expiry time for unread mail mail
	if(!sMailSystem.MailOption(MAIL_FLAG_NO_EXPIRY))
		msg.expire_time = (uint32)UNIXTIME + (TIME_DAY * 30);
	else
		msg.expire_time = 0;

	msg.copy_made = false;
	msg.read_flag = false;
	msg.deleted_flag = false;
	msg.message_type = 0;
	msg.yp = false;
	msg.is_yuanbao = false;

	// Great, all our info is filled in. Now we can add it to the other players mailbox.
	sMailSystem.DeliverMessage(player->guid, &msg);

	// Success packet :)
	SendMailError(MAIL_OK);
}

void WorldSession::HandleMarkAsRead(CPacketUsn& packet )
{
	MSG_C2S::stMail_Mark_As_Read MsgRecv;packet>>MsgRecv;
	uint64 mailbox = MsgRecv.mailbox;
	uint32 message_id = MsgRecv.message_id;
	MailMessage * message = _player->m_mailBox->GetMessage(message_id);
	if(message == 0) return;

	// mark the message as read
	message->read_flag = true;

	// mail now has a 3 day expiry time
	if(!sMailSystem.MailOption(MAIL_FLAG_NO_EXPIRY))
		message->expire_time = (uint32)UNIXTIME + (TIME_DAY * 3);

	// update it in sql
	sWorld.ExecuteSqlToDBServer("UPDATE mailbox SET read_flag = 1, expiry_time = %u WHERE message_id = %u", message->expire_time, message->message_id );
}

void WorldSession::HandleMailDelete(CPacketUsn& packet )
{
	MSG_C2S::stMail_Delete MsgRecv;packet>>MsgRecv;
	uint64 mailbox = MsgRecv.mailbox;
	uint32 message_id = MsgRecv.message_id;

	MSG_S2C::stMail_Send_Result Msg;
	Msg.message_id = message_id;
	Msg.res	= uint32(MAIL_RES_DELETED);

	MailMessage * message = _player->m_mailBox->GetMessage(message_id);
	if(message == 0)
	{
		Msg.result = uint32(MAIL_ERR_INTERNAL_ERROR);
		SendPacket(Msg);

		return;
	}

	if(message->cod > 0)
	{
		MailMessage msg;
		msg.message_type = NORMAL;
		msg.sender_guid = message->player_guid;
		msg.player_guid = message->sender_guid;

		msg.subject = build_language_string( BuildString_MailboxSystemReturn );//"系统退回";
		msg.body = "";
		msg.money = message->money;
		msg.cod = 0;
		msg.items = message->items;
		msg.stationary = message->stationary;
		msg.delivery_time = (uint32)UNIXTIME;
		msg.expire_time = 0;
		msg.read_flag = false;
		msg.copy_made = false;
		msg.deleted_flag = false;
		msg.yp = message->yp;
		msg.is_yuanbao = message->is_yuanbao;

		// Send the message.
		sMailSystem.DeliverMessage(msg.player_guid, &msg);
	}
	if(message->copy_made)
	{
		// we have the message as a copy on the item. we can't delete it or this item
		// will no longer function.

		// deleted_flag prevents it from being shown in the mail list.
		message->deleted_flag = 1;

		// update in sql
		sWorld.ExecuteSqlToDBServer("UPDATE mailbox SET deleted_flag = 1 WHERE message_id = %u", message_id);
	}
	else
	{
		// delete the message, there are no other references to it.
		_player->m_mailBox->DeleteMessage(message_id, true);
	}

	Msg.result = uint32(MAIL_OK);
	SendPacket(Msg);
}

void WorldSession::HandleTakeItem(CPacketUsn& packet )
{
	MSG_C2S::stMail_Take_Item MsgRecv;packet>>MsgRecv;
	uint64 mailbox = MsgRecv.mailbox;
	uint32 message_id = MsgRecv.message_id;
	uint32 lowguid = MsgRecv.lowguid;
	vector< uint64 >::iterator itr;

	MSG_S2C::stMail_Send_Result Msg;
	Msg.message_id = message_id;
	Msg.res = uint32(MAIL_RES_ITEM_TAKEN);

	MailMessage * message = _player->m_mailBox->GetMessage(message_id);
	if(message == 0 || message->items.empty())
	{
		Msg.result = uint32(MAIL_ERR_INTERNAL_ERROR);
		SendPacket(Msg);

		MyLog::log->error( "mail not found" );
		return;
	}

	for( itr = message->items.begin( ); itr != message->items.end( ); ++itr )
	{
		uint32 itemID = *itr;
		if ( itemID == lowguid )
			break;
	}

	if( itr == message->items.end( ) )
	{
		Msg.result = uint32(MAIL_ERR_INTERNAL_ERROR);
		SendPacket(Msg);

		MyLog::log->error( "item not found in this mail" );
		return;
	}

	// check for cod credit
	if(message->cod > 0)
	{
		if(_player->GetUInt32Value(PLAYER_FIELD_COINAGE) < message->cod)
		{
			Msg.result = uint32(MAIL_ERR_NOT_ENOUGH_MONEY);
			SendPacket(Msg);
			return;
		}
	}

	// grab the item
	Item * item = objmgr.LoadItem( *itr );
	if(item == 0)
	{
		// doesn't exist
		Msg.result = uint32(MAIL_ERR_INTERNAL_ERROR);
		SendPacket(Msg);

		return;
	}

	// find a free bag slot
	SlotResult result = _player->GetItemInterface()->FindFreeInventorySlot(item->GetProto());
	if(result.Result == 0)
	{
		// no free slots left!
		Msg.result = uint32(MAIL_ERR_BAG_FULL);
		SendPacket(Msg);

		delete item;
		return;
	}

	// all is good
	// delete the item (so when its resaved it'll have an association)
	item->DeleteFromDB();

	// add the item to their backpack
	item->m_isDirty = true;

	// send complete packet
	Msg.result = uint32(MAIL_OK);
	Msg.item_guid = item->GetGUID();
	Msg.stack_count = item->GetUInt32Value(ITEM_FIELD_STACK_COUNT);

	if( !_player->GetItemInterface()->AddItemToFreeSlot(item) )
		delete item;
	else
		MyLog::yunyinglog->info("item-mailtake-player[%u][%s] get item["I64FMT"][%u] count[%u] from player[%u][%s]", _player->GetLowGUID(), _player->GetName(), item->GetGUID(), item->GetProto()->ItemId, Msg.stack_count, (uint32)message->sender_guid, message->sender_name.c_str());


	message->items.erase( itr );

	// re-save (update the items field)
	sMailSystem.SaveMessageToSQL( message);
	SendPacket(Msg);

	if( message->cod > 0 )
	{
		_player->ModCoin(-int32(message->cod));
		string subject = message->subject;
		sMailSystem.SendAutomatedMessage(NORMAL, message->player_guid, message->sender_guid, subject, "", message->cod, 0, 0, 1);

		MyLog::yunyinglog->info("gold-takemailcod-player[%u][%s] cod[%u] from player[%u][%s]", _player->GetLowGUID(), _player->GetName(), message->cod, (uint32)message->sender_guid, message->sender_name.c_str());
		message->cod = 0;
		sWorld.ExecuteSqlToDBServer("UPDATE mailbox SET cod = 0 WHERE message_id = %u", message->message_id);
	}

	// prolly need to send an item push here
}

void WorldSession::HandleTakeMoney(CPacketUsn& packet )
{
	MSG_C2S::stMail_Take_Money MsgRecv;packet>>MsgRecv;
	uint64 mailbox = MsgRecv.mailbox;
	uint32 message_id = MsgRecv.message_id;

	MSG_S2C::stMail_Send_Result Msg;
	Msg.message_id = message_id;
	Msg.res = uint32(MAIL_RES_MONEY_TAKEN);

	MailMessage * message = _player->m_mailBox->GetMessage(message_id);
	if(message == 0 || !message->money)
	{
		Msg.result = uint32(MAIL_ERR_INTERNAL_ERROR);
		SendPacket(Msg);

		return;
	}

	// add the money to the player
	//if( !message->yp )
	{
		if( message->is_yuanbao )
		{
			_player->AddCardPoints( message->money, Player::ADD_CARD_TRADE );
			MyLog::yunyinglog->info("yuanbao-takemailyuanbao-player[%u][%s] take yuanbao[%u] from player[%u][%s]", _player->GetLowGUID(), _player->GetName(), message->money, (uint32)message->sender_guid, message->sender_name.c_str());
		}
		else
		{
			_player->ModCoin(message->money);
			MyLog::yunyinglog->info("gold-takemailgold-player[%u][%s] take gold[%u] from player[%u][%s]", _player->GetLowGUID(), _player->GetName(), message->money, (uint32)message->sender_guid, message->sender_name.c_str());
		}
	}

	// message no longer has any money
	message->money = 0;

	// update in sql!
	sWorld.ExecuteSqlToDBServer("UPDATE mailbox SET money = 0 WHERE message_id = %u", message->message_id);

	// send result
	Msg.result = uint32(MAIL_OK);
	SendPacket(Msg);
}

void WorldSession::HandleReturnToSender(CPacketUsn& packet )
{
	MSG_C2S::stMail_Return_To_Sender MsgRecv;packet>>MsgRecv;
	uint64 mailbox = MsgRecv.mailbox;
	uint32 message_id = MsgRecv.message_id;

	MSG_S2C::stMail_Send_Result Msg;
	Msg.message_id = message_id;
	Msg.res = uint32(MAIL_RES_RETURNED_TO_SENDER);

	MailMessage * msg = _player->m_mailBox->GetMessage(message_id);
	if(msg == 0)
	{
		Msg.result = uint32(MAIL_ERR_INTERNAL_ERROR);
		SendPacket(Msg);

		return;
	}

	// copy into a new struct
	MailMessage message = *msg;

	// remove the old message
	_player->m_mailBox->DeleteMessage(message_id, true);

	// re-assign the owner/sender
	message.player_guid = message.sender_guid;
	message.sender_guid = _player->GetGUID();

	// turn off the read flag
	message.read_flag = false;
	message.deleted_flag = false;
	message.copy_made = false;

	// null out the cod charges. (the sender doesnt want to have to pay for his own item
	// that he got nothing for.. :p)
	message.cod = 0;

	// assign new delivery time
	message.delivery_time = message.items.empty() ? (uint32)UNIXTIME : (uint32)UNIXTIME + 3600;

	// add to the senders mailbox
	sMailSystem.DeliverMessage(message.player_guid, &message);

	// finish the packet
	Msg.result = uint32(MAIL_OK);
	SendPacket(Msg);
}

void WorldSession::HandleMailCreateTextItem(CPacketUsn& packet )
{
	MSG_C2S::stMail_Create_Text_Item MsgRecv;packet>>MsgRecv;
	uint64 mailbox = MsgRecv.mailbox;
	uint32 message_id = MsgRecv.message_id;

	MSG_S2C::stMail_Send_Result Msg;
	Msg.message_id = message_id;
	Msg.res = uint32(MAIL_RES_MADE_PERMANENT);

	ItemPrototype * proto = ItemPrototypeStorage.LookupEntry(8383);
	MailMessage * message = _player->m_mailBox->GetMessage(message_id);
	if(message == 0 || !proto)
	{
		Msg.result = uint32(MAIL_ERR_INTERNAL_ERROR);
		SendPacket(Msg);
		return;
	}

	SlotResult result = _player->GetItemInterface()->FindFreeInventorySlot(proto);
	if(result.Result == 0)
	{
		Msg.result = uint32(MAIL_ERR_INTERNAL_ERROR);
		SendPacket(Msg);

		return;
	}

	Item * pItem = objmgr.CreateItem(8383, _player);
	pItem->SetUInt32Value(ITEM_FIELD_ITEM_TEXT_ID, message_id);
	if( _player->GetItemInterface()->AddItemToFreeSlot(pItem) )
	{
		// mail now has an item after it
		message->copy_made = true;

		// update in sql
		sWorld.ExecuteSqlToDBServer("UPDATE mailbox SET copy_made = 1 WHERE message_id = %u", message_id);

		Msg.result = uint32(MAIL_OK);
		SendPacket(Msg);
	}
	else
	{
		delete pItem;
	}
}

void WorldSession::HandleItemTextQuery(CPacketUsn& packet)
{
	MSG_C2S::stMail_Text_Query MsgRecv;packet>>MsgRecv;
	uint32 message_id = MsgRecv.message_id;
	string body = "Internal Error";

	MailMessage * msg = _player->m_mailBox->GetMessage(message_id);
	if(msg)
		body = msg->body;

	MSG_S2C::stMail_Text_Query_Response Msg;
	Msg.message_id = message_id;
	Msg.body = body;
	SendPacket(Msg);
}

void Mailbox::FillTimePacket(MSG_S2C::stMail_Query_Next_Time* Msg)
{
	uint32 c = 0;
	MessageMap::iterator iter = Messages.begin();

	for(; iter != Messages.end(); ++iter)
	{
		if(iter->second.deleted_flag == 0 && iter->second.read_flag == 0 && (uint32)UNIXTIME >= iter->second.delivery_time)
		{
			// unread message, w00t.
			++c;
			MSG_S2C::stMail_Query_Next_Time::stMail Mail;
			Mail.sender_guid = uint64(iter->second.sender_guid);
			Mail.stationary =  uint32(iter->second.stationary);
			Mail.delivery_time = ui32(UNIXTIME-iter->second.delivery_time);
			Msg->vMails.push_back( Mail );
		}
	}
}

void WorldSession::HandleMailTime(CPacketUsn& packet)
{
	MSG_S2C::stMail_Query_Next_Time Msg;
	_player->m_mailBox->FillTimePacket(&Msg);
	SendPacket(Msg);
}

void WorldSession::SendMailError(uint32 error)
{
	MSG_S2C::stMail_Send_Result Msg;
	Msg.message_id = 0;
	Msg.res = uint32(MAIL_RES_MAIL_SENT);
	Msg.result = error;
	SendPacket(Msg);
}

void WorldSession::HandleGetMail(CPacketUsn& packet )
{
	MSG_S2C::stMail_List_Result MsgMailList;
	_player->m_mailBox->BuildMailboxListingPacket(&MsgMailList);
	SendPacket(MsgMailList);
}

void MailSystem::RemoveMessageIfDeleted(uint32 message_id, Player * plr)
{
	MailMessage * msg = plr->m_mailBox->GetMessage(message_id);
	if(msg == 0) return;

	if(msg->deleted_flag)   // we've deleted from inbox
		plr->m_mailBox->DeleteMessage(message_id, true);   // wipe the message
}
void MailSystem::SendAutomatedMessage( MailMessage& message)
{
	Player* ply = objmgr.GetPlayer( message.sender_guid );
	if( ply )
	{
		message.sender_name = ply->GetName();
	}
	// Send the message.
	DeliverMessage(message.player_guid, &message);
}
void MailSystem::SendAutomatedMessage(uint32 type, uint64 sender, uint64 receiver, string subject, string body,
									  uint32 money, uint32 cod, uint64 item_guid, uint32 stationary, bool yp, bool is_yuanbao )
{
	// This is for sending automated messages, for example from an auction house.
	MailMessage msg;
	msg.message_type = type;
	msg.sender_guid = sender;
	Player* ply = objmgr.GetPlayer( (uint32)sender );
	if( ply )
	{
		msg.sender_name = ply->GetName();
	}
	msg.player_guid = receiver;
	msg.subject = subject;
	msg.body = body;
	msg.money = money;
	msg.cod = cod;
	if( GUID_LOPART(item_guid) != 0 )
		msg.items.push_back( GUID_LOPART(item_guid) );

	msg.stationary = stationary;
	msg.delivery_time = (uint32)UNIXTIME;
	msg.expire_time = 0;
	msg.read_flag = false;
	msg.copy_made = false;
	msg.deleted_flag = false;
	msg.yp = yp;
	msg.is_yuanbao = is_yuanbao;

	// Send the message.
	DeliverMessage(receiver, &msg);
}

void MailSystem::SendMail2InsertQueue( uint32 sender, uint32 receiver, const std::string& subject, const std::string& body, uint32 money, uint32 itementry, uint32 itemcnt, uint32 stationary )
{
	Database* db = g_crosssrvmgr->GetDBByGUID( receiver );
	if( db )
		db->WaitExecute( "insert into mailbox_insert_queue values(%u, %u, '%s', '%s', %u, %u, %u, %u)", sender, receiver, subject.c_str(), body.c_str(), stationary, money, itementry, itemcnt );
}

uint32 MailSystem::Generate_Message_Id()
{
	/** I know this is horrible. But when you have external mail sources unfortunately this is the only way to do this.
	 * - Burlex
	 */

	/*
	uint32 id = 1;
	QueryResult * result = CharacterDatabase.Query("SELECT MAX(message_id) FROM mailbox");
	if(result)
	{
		id = result->Fetch()[0].GetUInt32()+1;
		delete result;
	}
	*/

	return m_maxID++;
}

void Mailbox::Load(QueryResult * result)
{
	if(!result)
		return;

	Field * fields;
	MailMessage msg;
	uint32 i;
	char * str;
	char * p;
	uint64 itemguid;

	if(result->GetRowCount() >= 50)
	{
		Player* plr = (Player*)objmgr.GetPlayer(GetOwner());
		if(plr)
			plr->GetSession()->SendMailError( MAIL_ERR_NOT_ENOUGH_MONEY );
	}
	do
	{
		fields = result->Fetch();

		// Create message struct
		i = 0;
		msg.items.clear();
		msg.message_id = fields[i++].GetUInt32();
		msg.message_type = fields[i++].GetUInt32();
		msg.player_guid = fields[i++].GetUInt32();
		msg.sender_guid = fields[i++].GetUInt32();
		msg.subject = fields[i++].GetString();
		msg.body = fields[i++].GetString();
		msg.money = fields[i++].GetUInt32();
		str = (char*)fields[i++].GetString();
		p = strchr(str, ',');
		if( p == NULL )
		{
			itemguid = atoi(str);
			if( itemguid != 0 )
				msg.items.push_back( itemguid );
		}
		else
		{
			while( p )
			{
				*p = 0;
				p++;

				itemguid = atoi( str );
				if( itemguid != 0 )
					msg.items.push_back( itemguid );

                str = p;
				p = strchr( str, ',' );
			}
		}

		msg.cod = fields[i++].GetUInt32();
		msg.stationary = fields[i++].GetUInt32();
		msg.expire_time = fields[i++].GetUInt32();
		msg.delivery_time = fields[i++].GetUInt32();
		msg.copy_made = fields[i++].GetBool();
		msg.read_flag = fields[i++].GetBool();
		msg.deleted_flag = fields[i++].GetBool();
		msg.yp = fields[i++].GetBool();
		msg.is_yuanbao = fields[i++].GetBool();
		msg.sender_name = fields[i++].GetString();
		AddMessage(&msg);

	} while(result->NextRow());
}
