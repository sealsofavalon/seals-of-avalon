#ifndef __MAIL_H
#define __MAIL_H

namespace MSG_S2C
{
	struct stMail_List_Result;
	struct stMail_Query_Next_Time;
}
struct MailMessage
{
	uint32 message_id;
	uint32 message_type;
	uint64 player_guid;
	uint64 sender_guid;
	string subject;
	string body;
	uint32 money;
	vector<uint64> items;
	uint32 cod;
	uint32 stationary;
	uint32 expire_time;
	uint32 delivery_time;
	uint32 copy_made;
	bool read_flag;
	bool deleted_flag;
	bool yp;
	bool is_yuanbao;
	std::string sender_name;
	
	bool AddMessageDataToPacket(void* Mail);
};

typedef map<uint32, MailMessage> MessageMap;

class Mailbox
{
protected:
	uint64 owner;
	MessageMap Messages;

public:
	Mailbox(uint64 owner_) : owner(owner_) {}

	void AddMessage(MailMessage* Message);
	void DeleteMessage(uint32 MessageId, bool sql);
	MailMessage * GetMessage(uint32 message_id)
	{
		MessageMap::iterator iter = Messages.find(message_id);
		if(iter == Messages.end())
			return NULL;
		return &(iter->second);
	}

	void BuildMailboxListingPacket(MSG_S2C::stMail_List_Result* MsgMailList);
	void CleanupExpiredMessages();
	SUNYOU_INLINE size_t MessageCount() { return Messages.size(); }
	void FillTimePacket(MSG_S2C::stMail_Query_Next_Time* Msg);
	SUNYOU_INLINE uint64 GetOwner() { return owner; }
	void Load(QueryResult * result);
};


class MailSystem : public Singleton<MailSystem>, public EventableObject
{
public:

	void StartMailSystem();
	MailError DeliverMessage(uint64 recipent, MailMessage* message);
	void RemoveMessageIfDeleted(uint32 message_id, Player * plr);
	void SaveMessageToSQL(MailMessage * message);
	void SendAutomatedMessage( MailMessage& message);
	void SendAutomatedMessage(uint32 type, uint64 sender, uint64 receiver, string subject, string body, uint32 money,
		uint32 cod, uint64 item_guid, uint32 stationary, bool yp = false, bool is_yuanbao = false );
	void SendMail2InsertQueue( uint32 sender, uint32 receiver, const std::string& subject, const std::string& body, uint32 money, uint32 itementry, uint32 itemcnt, uint32 stationary );

	SUNYOU_INLINE bool MailOption(uint32 flag)
	{
		return (config_flags & flag) ? true : false;
	}
	uint32 config_flags;

	uint32 Generate_Message_Id();

private:
	uint32 m_maxID;
};

#define sMailSystem MailSystem::getSingleton()

#endif
