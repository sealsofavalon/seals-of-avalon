#ifndef _MAINSERVER_DEFINES_H
#define _MAINSERVER_DEFINES_H

class Database;

// SERVER_DECL extern Database* Database_Character;
// SERVER_DECL extern Database* Database_World;
// 
SERVER_DECL extern Database* Database_Realm;

#define WorldDatabase (*Database_World)
#define CharacterDatabase (*Database_Character)
#define RealmDatabase (*Database_Realm)

#endif
