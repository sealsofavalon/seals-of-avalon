#include "StdAfx.h"
#include "MakeItemBase.h"
#include "Player.h"
#include "Item.h"
#include "ItemInterface.h"
#include "../../SDBase/Public/UpdateFields.h"

MakeItemBase::MakeItemBase()
{
	memset( m_slots, 0, sizeof( m_slots ) );
}

MakeItemBase::~MakeItemBase()
{
	for( int i = 0; i < max_slot; ++i )
	{
		if( m_slots[i] )
			delete m_slots[i];
	}
}

Item* MakeItemBase::MoveSlot( uint32 slot, Item* source, bool to_inventory, bool& destroy )
{
	destroy = false;
	if( m_slots[slot] && source )
	{
		if( source->GetEntry() == m_slots[slot]->GetEntry() && slot != 0
			&& source->GetProto()->Class != ITEM_CLASS_ARMOR && source->GetProto()->Class != ITEM_CLASS_WEAPON )
		{
			Item*& item_src = to_inventory ? m_slots[slot] : source;
			Item*& item_des = to_inventory ? source : m_slots[slot];

			uint32 amtd = item_des->GetUInt32Value( ITEM_FIELD_STACK_COUNT );
			uint32 amts = item_src->GetUInt32Value( ITEM_FIELD_STACK_COUNT );
			if( amtd >= 200 )
				;
			else if( amtd + amts > 200 )
			{
				item_src->SetCount( amtd + amts - 200 );
				item_des->SetCount( 200 );
				item_src->m_isDirty = true;
				item_des->m_isDirty = true;
				return source;
			}
			else
			{
				item_des->SetCount( amtd + amts );
				item_des->m_isDirty = true;
				destroy = true;
				if( to_inventory )
				{
					item_src->DeleteFromDB();
					item_src->Delete();
					item_src = NULL;
					RefreshSlot( slot );
				}
				else
					item_src = NULL;
				return source;
			}
		}
	}
	else if( !m_slots[slot] && !source )
	{
		return NULL;
	}

	std::swap( m_slots[slot], source );

	RefreshSlot( slot );
	return source;
}

void MakeItemBase::Init( Player* owner )
{
	m_owner = owner;
	for( int i = 0; i < max_slot; ++i )
	{
		uint64 guid = 0;
		guid = owner->GetUInt64Value( player_field_begin + i * 2 );
		if( guid == 0 )
			m_slots[i] = NULL;
		else
		{
			m_slots[i] = owner->GetItemInterface()->GetInventoryItem( inventory_slot_begin + i );
			if( !m_slots[i] )
				owner->SetUInt64Value( player_field_begin + i * 2, 0 );
			else
				if( m_slots[i]->GetGUID() != guid )
					MyLog::log->error("MakeItemBase player load not fount same guid");
		}
	}

	//RefreshCost();
}

void MakeItemBase::RefreshSlot( uint32 slot )
{
	if( m_slots[slot] )
	{
		m_owner->SetUInt64Value( player_field_begin + slot * 2, m_slots[slot]->GetGUID() );
		m_slots[slot]->m_isDirty = true;
	}
	else
	{
		//m_owner->GetItemInterface()->ClearInventorySlot( inventory_slot_begin + slot );
		m_owner->SetUInt64Value( player_field_begin + slot * 2, 0 );
	}

	if( slot == 0 )
	{
		//RefreshCost();
	}
}

uint32 MakeItemBase::FindItem( uint32 entry, uint32 count )
{
	for( int i = 1; i < max_slot; ++i )
	{
		if( m_slots[i] )
		{
			if( m_slots[i]->GetEntry() == entry && m_slots[i]->GetUInt32Value( ITEM_FIELD_STACK_COUNT ) >= count )
				return i;
		}
	}
	return 0;
}

void MakeItemBase::SaveToDB( QueryBuffer* qb )
{
	for( int i = 0; i < max_slot; ++i )
	{
		if( m_slots[i] )
			m_slots[i]->SaveToDB( inventory_slot_save, i, false, qb );
	}
}

Item* MakeItemBase::GetSlot( uint32 slot )
{
	if( slot < max_slot )
		return m_slots[slot];
	else
		return NULL;
}
uint32 MakeItemBase::CreateUpdateBlocks( ByteBuffer* data )
{
	uint32 count = 0;
	for( int i = 0; i < max_slot; ++i )
	{
		if( m_slots[i] )
			count += m_slots[i]->BuildCreateUpdateBlockForPlayer( data, m_owner );
	}
	return count;
}
