#ifndef _MAKE_ITEM_BASE_HEAD_
#define _MAKE_ITEM_BASE_HEAD_

class Item;

class MakeItemBase
{
public:
	MakeItemBase();
	virtual ~MakeItemBase();

	void Init( Player* owner );
	Item* MoveSlot( uint32 slot, Item* source, bool to_inventory, bool& destroy );
	void SaveToDB( QueryBuffer* qb );
	Item* GetSlot( uint32 slot );
	uint32 CreateUpdateBlocks( ByteBuffer* data );
protected:
	void RefreshSlot( uint32 slot );
	//virtual void RefreshCost() = 0;
	uint32 FindItem( uint32 entry, uint32 count );
	Item* m_slots[6];
	Player* m_owner;
	uint32 max_slot;
	uint32 player_field_begin;
	uint32 player_field_cost;
	uint32 inventory_slot_begin;
	uint32 inventory_slot_save;
};
#endif