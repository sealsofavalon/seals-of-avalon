// Class Map
// Holder for all instances of each mapmgr, handles transferring
// players between, and template holding.

#include "StdAfx.h"
#include "Stream/FileStream.h"
#include "Stream/TagStream.h"

#define CHUNK_SIZE			(UNIT_COUNT * UNIT_SIZE) //32
#define CHUNK_COUNT			(16)

CChunk::CChunk()
{
	m_pkPositionList = new LocationVector[VERTEX_COUNT*VERTEX_COUNT];
	memset(m_pkPositionList, 0, sizeof(LocationVector)*VERTEX_COUNT*VERTEX_COUNT);

	memset( m_GridInfos, 0xFF, sizeof(m_GridInfos) );
}

void CChunk::Set( int x, int y, CTile* pTile )
{
	m_pTile = pTile;

	m_iX = x;
	m_iY = y;
	m_fMinX = pTile->GetMinX() + m_iX*ChunkSize;
	m_fMinY = pTile->GetMinY() + m_iY*ChunkSize;
}

bool CChunk::Load(CStream& s)
{
	CTagStream stream;
	while( stream.ReadFromStream(s) )
	{
		switch( stream.GetTag() )
		{
		/*
		case 'HEAD': //Header
			{
				if(m_pTile->GetVersion() < MakeVersion(1, 0))
				{
					stream.Seek( 4 * sizeof( int ) );
				}
				else
				{
					stream.Seek( 20 );
				}
			}
			break;
		case 'NRML': //法线
			{
				stream.Seek( VERTEX_COUNT*VERTEX_COUNT*sizeof(char)*3 );
			}
			break;

		case 'LMAP': //层贴图
			{
				m_uiNumLayers = stream.GetSize() / sizeof(unsigned int);
				assert( m_uiNumLayers <= 6 );
				stream.Seek( 0, CTagStream::SeekEnd );
			}
			break;
		case 'ALPH':
			{
				uint8 bHasShadow;
				stream.Read(&bHasShadow, sizeof(bHasShadow));
			}
			break;
		case 'LQUD': //水
			{
				stream.Seek( 64 );

				float afHeights[9][9];

				stream.Seek( 9 * 9 * sizeof( float ) + 4 );
			}
			break;
		case 'FYGD':
			{
				uint8 bUniformGrid;

				int iX; int iY;
				stream.Read(&iX, sizeof(int));
				stream.Read(&iY, sizeof(int));

				assert( iX == m_iX && iY == m_iY );

				stream.Read(&bUniformGrid, sizeof(bUniformGrid));
				if( bUniformGrid )
				{
					stream.Seek( 1 );
				}
				else
				{
					stream.Seek( sizeof( uint32 ) * GridsCount );
				}
			}
			break;

		//所引用的模型
		case 'MODL':
			{
				unsigned int uiCount;
				stream.Read(&uiCount, sizeof(uiCount));
				stream.Seek( uiCount* 12 );
			}
			break;

        case 'GRSS':
            {
				unsigned int uiGrassCount;
				stream.Read(&uiGrassCount, sizeof(uiGrassCount));
				if(uiGrassCount == 0)
					return false;

				CBitArray<32*32> kGrassMask; //1024 bits
				unsigned int* puiBits = kGrassMask.GetBits();
				unsigned int uiCount = kGrassMask.GetBitCount() / 32;
				stream.Read(puiBits, uiCount * sizeof(unsigned int));

				unsigned int uiBits;
				unsigned short usGrass;
				for(unsigned int i = 0; i < 32; ++i)
				{
					uiBits = *puiBits++;
					for(unsigned int j = 0; j < 32; ++j)
					{
						if(uiBits & (1 << j))
						{
							stream.Read(&usGrass, sizeof(unsigned short));
						}
					}
				}
            }
            break;

        case 'SNDE':
            {
                unsigned int uiSoundEmitterCount;
                stream.Read(&uiSoundEmitterCount, sizeof(uiSoundEmitterCount));

				std::string kName;
                std::string kFilename;

                unsigned int uiTimeInterval;
                unsigned int uiLoopCount;
                float fMinDistance;
                float fMaxDistance;

                for(unsigned int i = 0; i < uiSoundEmitterCount; ++i)
                {
                    stream.ReadString(kName);
                    stream.ReadString(kFilename);
                    stream.Seek(12);
                    stream.Read(&uiTimeInterval, sizeof(unsigned int));
                    stream.Read(&uiLoopCount, sizeof(unsigned int));
                    stream.Read(&fMinDistance, sizeof(float));
                    stream.Read(&fMaxDistance, sizeof(float));
                }
            }
            break;
		*/
		case 'HGHT': //height
			{
				assert( stream.GetSize() == VERTEX_COUNT*VERTEX_COUNT*sizeof(float) );

				float h;

				LocationVector* pkPosition = m_pkPositionList;
				for(int  x = 0; x < VERTEX_COUNT; x++)
				{
					for( int y = 0; y < VERTEX_COUNT; y++ )
					{
						stream.Read(&h, sizeof(float));
						pkPosition->x = float(x*UNIT_SIZE);
						pkPosition->y = float(y*UNIT_SIZE);
						pkPosition->z = h;

						pkPosition++;
					}
				}
			}
			break;

		case 'GRID':
			{
				unsigned char bUniformGrid;

				//多余的信息
				int iX; int iY;
				stream.Read(&iX, sizeof(int));
				stream.Read(&iY, sizeof(int));
				assert( iX == m_iX && iY == m_iY );

				bUniformGrid = stream.ReadUChar();
				if( bUniformGrid )
				{
					unsigned char ucUniformInfo = stream.ReadUChar();
					if( ucUniformInfo )
					{
						memset( m_GridInfos, 0xFF, sizeof(m_GridInfos) );
					}
					else
					{
						memset( m_GridInfos, 0, sizeof(m_GridInfos) );
					}
				}
				else
				{
					assert( GridsCount == 32 );
					stream.Read(m_GridInfos, sizeof(unsigned int)*GridsCount);
				}
			}
			break;
		}
	}

	return true;
}

int CChunk::GetWalkableState(float x, float y) const
{
	int iX = int((x - m_fMinX) / GridSize);
	int iY = int((y - m_fMinY) / GridSize);

	assert( iX >= 0 && iX < GridsCount );
	assert( iY >= 0 && iY < GridsCount );

	return (m_GridInfos[iX] & (1 << iY)) ? 1 : 0;
}

bool CChunk::GetVertexIndex(float inX, float inY, int& iIndex) const
{
	float x = inX - m_fMinX;
	float y = inY - m_fMinY;

	unsigned int X = (unsigned int)(x/UNIT_SIZE);
	unsigned int Y = (unsigned int)(y/UNIT_SIZE);

	if( X >= VERTEX_COUNT || Y >= VERTEX_COUNT )
	{
		return false;
	}

	iIndex = X * VERTEX_COUNT + Y;
	return true;
}

float CChunk::GetHeight(float x, float y)
{
	int iIndex;
	if( !GetVertexIndex( x, y, iIndex ) )
		return 0.0f;


	//  v3	          v2  
	//   *------------*
	//   |            |
	//   |            |
	//   |            |
	//   |            |
	//   |            |
	//   *------------*
	//  v0            v1

	float h0 = m_pkPositionList[iIndex].z;
	float h1 = m_pkPositionList[iIndex + VERTEX_COUNT].z;
	float h2 = m_pkPositionList[iIndex + VERTEX_COUNT + 1].z;
	float h3 = m_pkPositionList[iIndex + 1].z;
	float h = 0.f;

	float deltaX = fmod(x - m_fMinX, UNIT_SIZE)/float(UNIT_SIZE);
	float deltaY = fmod(y - m_fMinY, UNIT_SIZE)/float(UNIT_SIZE);

	if( deltaX + deltaY < 1.f )
	{
		//v3v0v1
		h = h0 + deltaX*(h1 - h0) + deltaY*(h3 - h0);
	}
	else
	{
		//v1v2v3
		h = h2 + (1 - deltaX)*(h3 - h2) + (1 - deltaY)*(h1 - h2);
	}

	return h;
}

extern char* g_mapLoadBuff;

bool CTile::Load( const char* pcMapName, uint32 hashID )
{
	char filename[1024];
	sprintf(filename, "Data/Maps/%s/%s_%d_%d.tle", pcMapName, pcMapName, m_iX, m_iY);
	/*
	CFileStream file;
	if( !file.Open(filename) )
		return false;
		*/

	FILE* fp = fopen( filename, "rb" );
	if( !fp )
		return false;

	CollideInterface.LoadVMap( hashID, m_iX, m_iY );

	CTagStream stream( g_mapLoadBuff );
	while( stream.ReadFromFile( fp ) )
	{
		switch( stream.GetTag() )
		{
			/*
		case 'VERS':
			stream.Read( &m_version, sizeof( m_version ) );
			stream.SetVersion( m_version );
			break;
		case 'TEXS'://贴图列表
			{
				std::string str;
				while( stream.ReadString( str ) )
				{
					stream.Seek( 2 * sizeof( float ) );
				}
			}
			break;
		case 'MODL': //物件模型
			{
				while( stream.ReadModelInstance() )
				{
				}				
			}
			break;
			*/
		case 'CHNK':
			{
				int x = stream.ReadInt();
				int y = stream.ReadInt();

				assert( x >= 0 && x < ChunksCount );
				assert( y >= 0 && y < ChunksCount );

				m_Chunks[x][y].Set(x, y, this);
				m_Chunks[x][y].Load( stream );
			}
			break;
		}
	}
	MyLog::log->notice( "Tile:[%s] load success!", filename );

	fclose( fp );
	return true;
}

CChunk* CTile::GetChunk(float x, float y)
{
	int iX = int((x - m_fMinX)/ChunkSize);
	int iY = int((y - m_fMinY)/ChunkSize);

	assert( iX >= 0 && iX < ChunksCount );
	assert( iY >= 0 && iY < ChunksCount );

	return &m_Chunks[iX][iY];
}

Map::Map(uint32 mapid, MapInfo * inf)
{
	memset(spawns,0,sizeof(CellSpawns*) * _sizeX);

	_mapInfo = inf;
	_mapId = mapid;

	//new stuff Load Spawns
	LoadSpawns(false);

	// get our name
	if(_mapInfo)
		name = _mapInfo->map_file;
	else
		name = "Unknown";

	memset(m_Tiles, 0, sizeof(m_Tiles));
	m_hashCode = 0;

	LoadTerrain();
/*
#ifdef COLLISION
	CollideInterface.ActivateMap(GetMapIDForCollision());
#endif
*/
}

Map::~Map()
{
	MyLog::log->notice("Map : ~Map %u", this->_mapId);

	for(uint32 x=0;x<_sizeX;x++)
	{
		if(spawns[x])
		{
			for(uint32 y=0;y<_sizeY;y++)
			{
				if(spawns[x][y])
				{	
					CellSpawns * sp=spawns[x][y];
					for(CreatureSpawnList::iterator i = sp->CreatureSpawns.begin();i!=sp->CreatureSpawns.end();i++)
						delete (*i);
					for(GOSpawnList::iterator it = sp->GOSpawns.begin();it!=sp->GOSpawns.end();it++)
						delete (*it);

					delete sp;
					spawns[x][y]=NULL;
				}
			}
			delete [] spawns[x];
		}
	}

	for(CreatureSpawnList::iterator i = staticSpawns.CreatureSpawns.begin(); i != staticSpawns.CreatureSpawns.end(); ++i)
		delete *i;
	for(GOSpawnList::iterator i = staticSpawns.GOSpawns.begin(); i != staticSpawns.GOSpawns.end(); ++i)
		delete *i;

	for( int i = 0; i < TilesCount; i++ )
	{
		for( int j = 0; j < TilesCount; j++ )
		{
			delete m_Tiles[i][j];
			m_Tiles[i][j] = NULL;
		}
	}
/*
#ifdef COLLISION
	CollideInterface.DeactivateMap(GetMapIDForCollision());
#endif
*/
}

bool first_table_warning = true;
bool CheckResultLengthCreatures(QueryResult * res)
{
	if( res->GetFieldCount() != 19 && res->GetFieldCount() != 15 )
	{
		if( first_table_warning )
		{
			first_table_warning = false;
			MyLog::log->error("One of your creature_spawns table has the wrong column count.");
			MyLog::log->error("skipped loading this table in order to avoid crashing.");
			MyLog::log->error("Please correct this, if you do not no spawns will show.");
		}

		return false;
	}
	else
		return true;
}

bool first_table_warningg = true;
bool CheckResultLengthGameObject(QueryResult * res)
{
	if( res->GetFieldCount() != 16 )
	{
		if( first_table_warningg )
		{
			first_table_warningg = false;
			MyLog::log->error("One of your creature_spawns table has the wrong column count.");
			MyLog::log->error("has skipped loading this table in order to avoid crashing.");
			MyLog::log->error("Please correct this, if you do not no spawns will show.");
		}

		return false;
	}
	else
		return true;
}

void Map::LoadSpawns(bool reload)
{
	//uint32 st=getMSTime();
	CreatureSpawnCount = 0;

	StorageContainerIterator<MapSpawnPos>* it = MapSpawnPosStorage.MakeIterator();
	while(!it->AtEnd())
	{
		MapSpawnPos* si = it->Get();
		
		if( si->mapid == _mapId )
		{
			m_vPlrSpawnPos.push_back( si );
		}

		if(!it->Inc())
			break;
	}
	it->Destruct();

	if(reload)//perform cleanup
		for(uint32 x=0;x<_sizeX;x++)
			for(uint32 y=0;y<_sizeY;y++)
				if(spawns[x][y])
				{	
					CellSpawns * sp=spawns[x][y];
					for(CreatureSpawnList::iterator i = sp->CreatureSpawns.begin();i!=sp->CreatureSpawns.end();i++)
						delete (*i);
					for(GOSpawnList::iterator it = sp->GOSpawns.begin();it!=sp->GOSpawns.end();it++)
						delete (*it);

					delete sp;
					spawns[x][y]=NULL;
				}

				QueryResult * result;
				set<string>::iterator tableiterator;
				for(tableiterator=ExtraMapCreatureTables.begin(); tableiterator!=ExtraMapCreatureTables.end();++tableiterator)
				{
					result = WorldDatabase.Query("SELECT * FROM %s WHERE Map = %u",(*tableiterator).c_str(),this->_mapId);
					if(result)
					{
						if(CheckResultLengthCreatures( result) )
						{
							do{
								Field * fields = result->Fetch();
								CreatureSpawn * cspawn = new CreatureSpawn;
								cspawn->id = fields[0].GetUInt32();
								cspawn->form = FormationMgr::getSingleton().GetFormation(cspawn->id);
								cspawn->entry = fields[1].GetUInt32();
								cspawn->x = fields[3].GetFloat();
								cspawn->y = fields[4].GetFloat();
								cspawn->z = fields[5].GetFloat();
								cspawn->o = fields[6].GetFloat();
								/*uint32 cellx=float2int32(((_maxX-cspawn->x)/_cellSize));
								uint32 celly=float2int32(((_maxY-cspawn->y)/_cellSize));*/
								uint32 cellx=CellHandler::GetPosX(cspawn->x);
								uint32 celly=CellHandler::GetPosY(cspawn->y);
								if(spawns[cellx]==NULL)
								{
									spawns[cellx]=new CellSpawns*[_sizeY];
									memset(spawns[cellx],0,sizeof(CellSpawns*)*_sizeY);
								}

								if(!spawns[cellx][celly])
									spawns[cellx][celly]=new CellSpawns;
								cspawn->movetype = fields[7].GetUInt8();
								cspawn->displayid = fields[8].GetUInt32();
								cspawn->factionid = fields[9].GetUInt32();
								cspawn->flags = fields[10].GetUInt32();
								cspawn->bytes = fields[11].GetUInt32();
								cspawn->bytes2 = fields[12].GetUInt32();
								cspawn->emote_state = fields[13].GetUInt32();
								//cspawn->respawnNpcLink = fields[14].GetUInt32();
								cspawn->channel_spell = fields[15].GetUInt16();
								cspawn->channel_target_go = fields[16].GetUInt32();
								cspawn->channel_target_creature = fields[17].GetUInt32();
								cspawn->stand_state = fields[18].GetUInt16();
								spawns[cellx][celly]->CreatureSpawns.push_back(cspawn);
								m_allspawns.insert( cspawn );
								m_mapspawns[cspawn->id] = cspawn;
								++CreatureSpawnCount;
							}while(result->NextRow());
						}

						delete result;
					}
				}

				result = WorldDatabase.Query("SELECT * FROM creature_staticspawns WHERE Map = %u",this->_mapId);
				if(result)
				{
					if( CheckResultLengthCreatures(result) )
					{
						do{
							Field * fields = result->Fetch();
							CreatureSpawn * cspawn = new CreatureSpawn;
							cspawn->id = fields[0].GetUInt32();
							cspawn->form = FormationMgr::getSingleton().GetFormation(cspawn->id);
							cspawn->entry = fields[1].GetUInt32();
							cspawn->x = fields[3].GetFloat();
							cspawn->y = fields[4].GetFloat();
							cspawn->z = fields[5].GetFloat();
							cspawn->o = fields[6].GetFloat();
							cspawn->movetype = fields[7].GetUInt8();
							cspawn->displayid = fields[8].GetUInt32();
							cspawn->factionid = fields[9].GetUInt32();
							cspawn->flags = fields[10].GetUInt32();
							cspawn->bytes = fields[11].GetUInt32();
							cspawn->bytes2 = fields[12].GetUInt32();
							cspawn->emote_state = fields[13].GetUInt32();
							//cspawn->respawnNpcLink = fields[14].GetUInt32();
							cspawn->channel_spell=0;
							cspawn->channel_target_creature=0;
							cspawn->channel_target_go=0;
							cspawn->stand_state = fields[18].GetUInt16();
							staticSpawns.CreatureSpawns.push_back(cspawn);
							++CreatureSpawnCount;
						}while(result->NextRow());
					}

					delete result;
				}

				GameObjectSpawnCount = 0;
				result = WorldDatabase.Query("SELECT * FROM gameobject_staticspawns WHERE Map = %u",this->_mapId);
				if(result)
				{
					if( CheckResultLengthGameObject(result) )
					{
						do{
							Field * fields = result->Fetch();
							GOSpawn * gspawn = new GOSpawn;
							gspawn->entry = fields[1].GetUInt32();
							gspawn->id = fields[0].GetUInt32();
							gspawn->x=fields[3].GetFloat();
							gspawn->y=fields[4].GetFloat();
							gspawn->z=fields[5].GetFloat();
							gspawn->facing=fields[6].GetFloat();
							gspawn->o =fields[7].GetFloat();
							gspawn->o1=fields[8].GetFloat();
							gspawn->o2=fields[9].GetFloat();
							gspawn->o3=fields[10].GetFloat();
							gspawn->state=fields[11].GetUInt32();
							gspawn->flags=fields[12].GetUInt32();
							gspawn->faction=fields[13].GetUInt32();
							gspawn->scale = fields[14].GetFloat();
							//gspawn->stateNpcLink = fields[15].GetUInt32();
							staticSpawns.GOSpawns.push_back(gspawn);
							++GameObjectSpawnCount;
						}while(result->NextRow());
					}

					delete result;
				}

				for(tableiterator=ExtraMapGameObjectTables.begin(); tableiterator!=ExtraMapGameObjectTables.end();++tableiterator)
				{
					result = WorldDatabase.Query("SELECT * FROM %s WHERE map = %u",(*tableiterator).c_str(),this->_mapId);
					if(result)
					{
						if( CheckResultLengthGameObject(result) )
						{
							do{
								Field * fields = result->Fetch();
								GOSpawn * gspawn = new GOSpawn;
								gspawn->entry = fields[1].GetUInt32();
								gspawn->id = fields[0].GetUInt32();
								gspawn->x=fields[3].GetFloat();
								gspawn->y=fields[4].GetFloat();
								gspawn->z=fields[5].GetFloat();
								gspawn->facing=fields[6].GetFloat();
								gspawn->o =fields[7].GetFloat();
								gspawn->o1=fields[8].GetFloat();
								gspawn->o2=fields[9].GetFloat();
								gspawn->o3=fields[10].GetFloat();
								gspawn->state=fields[11].GetUInt32();
								gspawn->flags=fields[12].GetUInt32();
								gspawn->faction=fields[13].GetUInt32();
								gspawn->scale = fields[14].GetFloat();
								//gspawn->stateNpcLink = fields[15].GetUInt32();

								//uint32 cellx=float2int32(((_maxX-gspawn->x)/_cellSize));
								//uint32 celly=float2int32(((_maxY-gspawn->y)/_cellSize));
								uint32 cellx=CellHandler::GetPosX(gspawn->x);
								uint32 celly=CellHandler::GetPosY(gspawn->y);
								if(spawns[cellx]==NULL)
								{
									spawns[cellx]=new CellSpawns*[_sizeY];
									memset(spawns[cellx],0,sizeof(CellSpawns*)*_sizeY);
								}

								if(!spawns[cellx][celly])
									spawns[cellx][celly]=new CellSpawns;

								spawns[cellx][celly]->GOSpawns.push_back(gspawn);
								m_mapGOSpawns[gspawn->id] = gspawn;
								++GameObjectSpawnCount;
							}while(result->NextRow());
						}

						delete result;
					}
				}

				MyLog::log->notice("Map : %u creatures / %u gameobjects on map %u cached.", CreatureSpawnCount, GameObjectSpawnCount, _mapId);
}


CellSpawns* Map::GetSpawnsList(uint32 cellx,uint32 celly)
{
	ASSERT(cellx < _sizeX);
	ASSERT(celly < _sizeY);

	if( cellx < _sizeX && celly < _sizeY )
	{
		if(spawns[cellx]==NULL) return NULL;

		return spawns[cellx][celly];
	}
	else
		return NULL;
}

CellSpawns* Map::GetSpawnsListAndCreate(uint32 cellx, uint32 celly)
{
	ASSERT(cellx < _sizeX);
	ASSERT(celly < _sizeY);
	if(spawns[cellx]==NULL)
	{
		spawns[cellx] = new CellSpawns*[_sizeY];
		memset(spawns[cellx],0,sizeof(CellSpawns*)*_sizeY);
	}

	if(spawns[cellx][celly] == 0)
		spawns[cellx][celly] = new CellSpawns;
	return spawns[cellx][celly];
}

CTile* Map::GetTile(float x, float y)
{
	int iX = int(x / TileSize);
	int iY = int(y / TileSize);

	if( iX >= 0 && iX <= TilesCount && iY >= 0 && iY <= TilesCount )
		return m_Tiles[iX][iY];
	else
		return NULL;
}

CChunk* Map::GetChunk(float x, float y)
{
	CTile* pTile = GetTile(x, y);
	if( pTile == NULL )
		return NULL;

	return pTile->GetChunk(x, y);
}

struct tileflags
{
	int iTileFlags[TilesCount][TilesCount];
};

void Map::LoadTerrain()
{
	char filename[1024];
	sprintf(filename, "Data/Maps/%s/%s.map", name.c_str(), name.c_str());
	//CFileStream file;
	FILE* fp = fopen( filename, "rb" );

	//if( !file.Open(filename) )
	//	return;
	if( !fp )
		return;

	tileflags t;
	tileflags* pTile = &t;
	memset( pTile, 0, sizeof(tileflags) );

	CTagStream stream;

	while( stream.ReadFromFile( fp ) )
	{
		switch( stream.GetTag() )
		{
		case 'MAIN':
			{
				assert( stream.GetSize() == sizeof(pTile->iTileFlags) );
				stream.Read( pTile->iTileFlags, sizeof(pTile->iTileFlags));
			}
			break;
		}
	}

	for( int i = 0; i < TilesCount; i++ )
	{
		for( int j = 0; j < TilesCount; j++ )
		{
			if( pTile->iTileFlags[i][j] )
			{
				m_Tiles[i][j] = new CTile(i, j);
				m_Tiles[i][j]->Load(name.c_str(), GetMapIDForCollision() );
			}
			else
			{
				m_Tiles[i][j] = NULL;
			}
		}
	}

	fclose( fp );
}


float Map::GetLandHeight(float x, float y)
{

	CChunk* chunk = GetChunk(x, y);
	if( chunk )
		return chunk->GetHeight(x, y);

	//边界点
	if( (int(x) & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(x - 1.f, y);
		if( chunk )
			return chunk->GetHeight(x, y);
	}

	//边界点
	if( (int(y) & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(x, y - 1.f);
		if( chunk )
			return chunk->GetHeight(x, y);
	}

	//顶点
	if( (int(x) & (CHUNK_SIZE - 1)) == 0 && (int(y) & (CHUNK_SIZE - 1)) == 0 )
	{
		chunk = GetChunk(x - 1.f, y - 1.f);
		if( chunk )
			return chunk->GetHeight(x, y);
	}

	return NO_WMO_HEIGHT;
}

float Map::GetWaterHeight(float x, float y) 
{ 
	return 999999.0f; 
}

uint8 Map::GetWaterType(float x, float y)
{
	return 0;
}

uint8 Map::GetWalkableState(float x, float y)
{
	CChunk* pChunk = GetChunk(x, y);
	if( pChunk )
		return pChunk->GetWalkableState(x, y);

	return 1; 
}

uint16 Map::GetAreaID(float x, float y)
{
	return 0xFFFF;
}

uint32 Map::GetMapIDForCollision()
{
	if( m_hashCode == 0 && _mapInfo->map_file[0] != 0 )
		m_hashCode = crc32( (uint8*)_mapInfo->map_file, (uint32)strlen( _mapInfo->map_file ) );

	return m_hashCode;
}

MapSpawnPos* Map::GetNearestSpawnPos(uint32 race, float x, float y)
{
	uint32 index = -1;
	uint32 range = 0;
	for(int i = 0; i < (int)m_vPlrSpawnPos.size(); i++)
	{
		if(m_vPlrSpawnPos[i]->race == race)
		{
			float delta_x = m_vPlrSpawnPos[i]->x - x;
			float delta_y = m_vPlrSpawnPos[i]->y - y;
			uint32 newrange = sqrt (delta_x*delta_x + delta_y*delta_y);
			if(!range)
			{
				range = newrange;
				index = i;
			}
			if(newrange < range)
			{
				range = newrange;
				index = i;
			}
		}
	}

	if(index >= 0 && index < (int)m_vPlrSpawnPos.size())
	{
		return m_vPlrSpawnPos[index];
	}
	else
		return NULL;
}