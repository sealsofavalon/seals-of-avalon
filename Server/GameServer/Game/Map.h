#ifndef __MAP_H
#define __MAP_H

class CStream;
class CTile;

#define UNIT_SIZE			(4)
#define UNIT_COUNT			(8)
#define VERTEX_COUNT		(UNIT_COUNT + 1) //9

class CChunk
{
public:
	CChunk();

	bool Load(CStream& s);
	void Set( int x, int y, CTile* pTile );

	int GetX() const { return m_iX; }
	int GetY() const { return m_iY; }
	float GetMinX() const { return m_fMinX; }
	float GetMinY() const { return m_fMinY; }

	float GetHeight(float x, float y);
	bool GetVertexIndex(float x, float y, int& iIndex) const;

	int GetWalkableState(float x, float y) const;


private:
	int m_iX;
	int m_iY;
	float m_fMinX;
	float m_fMinY;
	CTile* m_pTile;
	LocationVector* m_pkPositionList;
	uint32 m_uiNumLayers;
	unsigned int m_GridInfos[GridsCount]; //每一位表示一个格点信息
};

class CTile
{
public:
	CTile( int x, int y )
		:m_iX(x)
		,m_iY(y)
		,m_fMinX(x*TileSize)
		,m_fMinY(y*TileSize)
	{}

	int GetX() const { return m_iX; }
	int GetY() const { return m_iY; }
	float GetMinX() const { return m_fMinX; }
	float GetMinY() const { return m_fMinY; }

	bool Load( const char* pcMapName, uint32 hashID );
	CChunk* GetChunk(float x, float y);
	int GetVersion() { return m_version; }

private:
	int m_iX;
	int m_iY;

	float m_fMinX;
	float m_fMinY;
	int m_version;
	CChunk	m_Chunks[ChunksCount][ChunksCount];
};

class SERVER_DECL Map
{
public:
	Map(uint32 mapid, MapInfo * inf);
	virtual ~Map();

	const string& GetNameString() { return name; }
	const char* GetName() { return name.c_str(); }
	const uint32 GetMapId() {return _mapId;}

	CellSpawns *GetSpawnsList(uint32 cellx,uint32 celly);
	CellSpawns * GetSpawnsListAndCreate(uint32 cellx, uint32 celly);

	void LoadSpawns(bool reload);//set to true to make clean up
	uint32 CreatureSpawnCount;
	uint32 GameObjectSpawnCount;

	CTile* GetTile(float x, float y);
	CChunk* GetChunk(float x, float y);
	void LoadTerrain();
	float  GetLandHeight(float x, float y);
	float  GetWaterHeight(float x, float y);
	uint8  GetWaterType(float x, float y);
	uint8  GetWalkableState(float x, float y);
	uint16 GetAreaID(float x, float y);
	uint32 GetMapIDForCollision();

	std::vector<MapSpawnPos*> m_vPlrSpawnPos;
	MapSpawnPos* GetNearestSpawnPos(uint32 race, float x, float y);
private:
	MapInfo *	 _mapInfo;
	uint32		 _mapId;
	string		 name;
	uint32		m_hashCode;

	//new stuff
	CellSpawns **spawns[_sizeX];

	CTile* m_Tiles[TilesCount][TilesCount];

public:
	CellSpawns staticSpawns;
	std::set<CreatureSpawn*> m_allspawns;
	std::map<uint32, CreatureSpawn*> m_mapspawns;
	std::map<uint32, GOSpawn*> m_mapGOSpawns;
};

#endif
