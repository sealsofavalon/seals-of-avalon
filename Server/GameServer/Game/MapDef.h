#pragma once


/*
#define MAX_LAYERS			(6)
#define ALPHA_MAP_SIZE		(64)

#define UNIT_SIZE			(4)
#define UNIT_COUNT			(8)
#define VERTEX_COUNT		(UNIT_COUNT + 1) //9

#define CHUNK_SIZE			(UNIT_COUNT * UNIT_SIZE) //32
#define CHUNK_COUNT			(16)

#define TILE_SIZE			(CHUNK_COUNT * CHUNK_SIZE) //512
#define TILE_COUNT			(64)

#define TILE_INDEX(x)		(int(x) / TILE_SIZE)			 

#define MAP_SIZE			(TILE_COUNT * TILE_SIZE) //32768

#define ALIGN( X, ALIGNMENT ) ( int(int(X) & ~(ALIGNMENT-1)) )
*/

#define TilesCount 64
/*533.33333f*/
#define TileSize 512.f
/*
#define _minY (-TilesCount*TileSize/2)
#define _minX (-TilesCount*TileSize/2)

#define _maxY (TilesCount*TileSize/2)
#define _maxX (TilesCount*TileSize/2)
*/
static const float _minY = (-TilesCount*TileSize/2);
static const float _minX = (-TilesCount*TileSize/2);

static const float _maxY = (TilesCount*TileSize/2);
static const float _maxX = (TilesCount*TileSize/2);

#define CellsPerTile 8

/*
#define _cellSize (TileSize/CellsPerTile)
#define _sizeX (TilesCount*CellsPerTile)
#define _sizeY (TilesCount*CellsPerTile)
*/
static const int _cellSize = (TileSize/CellsPerTile);
static const int _sizeX = (TilesCount*CellsPerTile);
static const int _sizeY = (TilesCount*CellsPerTile);


#define ChunksCount 16

//#define ChunkSize (TileSize / ChunksCount) //32.0
static const int ChunkSize = 32;

static const int GridSize = 1;

//#define GridSize (ChunkSize / GridsCount) //1.0

static const int GridsCount = (ChunkSize / GridSize);


#define GetRelatCoord(Coord,CellCoord) ((_maxX-Coord)-CellCoord*_cellSize)
struct Formation{
	uint32 fol;
	float ang;
	float dist;
};
typedef struct
{
	uint32	id;//spawn ID
	uint32	entry;
	float	x;
	float	y;
	float	z;
	float	o;
	Formation* form;
	uint8 movetype;
	uint32 displayid;
	uint32 factionid;
	uint32 flags;
	uint32 bytes;
	uint32 bytes2;
	uint32 emote_state;
	//uint32 respawnNpcLink;
	uint16 channel_spell;
	uint32 channel_target_go;
	uint32 channel_target_creature;
	uint16 stand_state;
}CreatureSpawn;

typedef struct
{
	uint32	id;//spawn ID
	uint32	entry;
	float	x;
	float	y;
	float	z;
	float	o;
	float	o1;
	float	o2;
	float	o3;
	float	facing;
	uint32	flags;
	uint32	state;
	uint32	faction;
	//	uint32	level;
	float scale;
	//uint32 stateNpcLink;
} GOSpawn;

typedef std::vector<CreatureSpawn*> CreatureSpawnList;
typedef std::vector<GOSpawn*> GOSpawnList;
typedef struct
{
	CreatureSpawnList CreatureSpawns;
	GOSpawnList GOSpawns;
}CellSpawns;