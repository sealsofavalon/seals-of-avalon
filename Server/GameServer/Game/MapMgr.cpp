#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "../../SDBase/Protocol/S2C_Move.h"
#include "DynamicObject.h"
#include "Unit.h"
#include "AIInterface.h"
#include "SunyouInstance.h"

#define MAP_MGR_UPDATE_PERIOD 100
#define MAPMGR_INACTIVE_MOVE_TIME 10
extern bool bServerShutdown;
static int global_mapmgr_count = 0;
MapMgr::MapMgr(Map *map, uint32 mapId, uint32 instanceid) : CellHandler(map), _mapId(mapId), eventHolder(instanceid), m_pInstance( NULL ), m_SunyouInstanceLastUpdate( 0 )
{
	m_MaxPlayerCnt = 0;
	_shutdown = false;
	m_instanceID = instanceid;
	pMapInfo = WorldMapInfoStorage.LookupEntry(mapId);
	m_UpdateDistance = pMapInfo->update_distance * pMapInfo->update_distance;
	iInstanceMode = 0;

	// Create script interface
	ScriptInterface = new MapScriptInterface(*this);

	// Set up storage arrays
	m_CreatureArraySize = map->CreatureSpawnCount;
	m_GOArraySize = map->GameObjectSpawnCount;

	//m_CreatureStorage = new Creature*[m_CreatureArraySize];
	m_CreatureStorage = (Creature**)malloc(sizeof(Creature*) * m_CreatureArraySize);
	memset(m_CreatureStorage,0,sizeof(Creature*)*m_CreatureArraySize);

	//m_GOStorage = new GameObject*[m_GOArraySize];
	m_GOStorage = (GameObject**)malloc(sizeof(GameObject*) * m_GOArraySize);
	memset(m_GOStorage,0,sizeof(GameObject*)*m_GOArraySize);
	m_GOHighGuid = m_CreatureHighGuid = 0;
	m_DynamicObjectHighGuid=0;
	lastUnitUpdate = getMSTime();
	lastGameobjectUpdate = getMSTime();

	m_holder = &eventHolder;
	m_event_Instanceid = eventHolder.GetInstanceID();
	forced_expire = false;
	InactiveMoveTime = 0;
	mLoopCounter=0;
	pInstance = NULL;
	m_sunyouinstance = NULL;

	sInstanceMgr.IncreaseMapMgrCount();
	MyLog::log->info( "global map manager count:[%d]", ++global_mapmgr_count );
}


MapMgr::~MapMgr()
{
	sInstanceMgr.DecreaseMapMgrCount();
	_shutdown=true;
	sEventMgr.RemoveEvents(this);
	delete ScriptInterface;

	// Remove objects
	if(_cells)
	{
		for (uint32 i = 0; i < _sizeX; i++)
		{
			if(_cells[i] != 0)
			{
				for (uint32 j = 0; j < _sizeY; j++)
				{
					if(_cells[i][j] != 0)
					{
						_cells[i][j]->_unloadpending=false;
						_cells[i][j]->RemoveObjects();
					}
				}
			}
		}
	}

	for(set<Object*>::iterator itr = _mapWideStaticObjects.begin(); itr != _mapWideStaticObjects.end(); ++itr)
	{
		if((*itr)->IsInWorld())
			(*itr)->RemoveFromWorld(false);
		delete (*itr);
	}


	free(m_GOStorage);
	free(m_CreatureStorage);

	Corpse * pCorpse;
	for(set<Corpse*>::iterator itr = m_corpses.begin(); itr != m_corpses.end();)
	{
		pCorpse = *itr;
		++itr;

		if(pCorpse->IsInWorld())
			pCorpse->RemoveFromWorld(false);

		delete pCorpse;
	}

	MyLog::log->notice("MapMgr : Instance %u shut down. (%s)" , m_instanceID, GetBaseMap()->GetName());
	MyLog::log->info( "global map manager count:[%d]", --global_mapmgr_count );
}

uint32 MapMgr::GetTeamPlayersCount(uint32 teamId)
{
	uint32 result = 0;
	PlayerStorageMap::iterator itr = m_PlayerStorage.begin();
	for(; itr != m_PlayerStorage.end(); itr++)
	{
		Player * pPlayer = (itr->second);
		if(pPlayer->GetTeam() == teamId)
			result++;
	}
	return result;
}


void MapMgr::PushObject(Object *obj)
{
	/////////////
	// Assertions
	/////////////
	ASSERT(obj);


	if( m_pInstance && obj->IsPlayer() )
	{
		m_pInstance->m_expiration = 0;
	}

	if( obj->m_isNeedUpdate )
		_updates.insert( obj );

	// That object types are not map objects. TODO: add AI groups here?
	if(obj->GetTypeId() == TYPEID_ITEM || obj->GetTypeId() == TYPEID_CONTAINER)
	{
		// mark object as updatable and exit
		if( ((Item*)obj)->GetOwner() )// ourself mapitem
			return;
	}

	if(obj->GetTypeId() == TYPEID_CORPSE)
	{
		m_corpses.insert(((Corpse*)obj));
	}

	if( obj->IsPlayer() )
	{
		((Player*)obj)->m_resurrectInstanceID = GetInstanceID();
		((Player*)obj)->m_resurrectMapId = GetMapId();
		((Player*)obj)->m_resurrectPosX = GetMapInfo()->repopx;
		((Player*)obj)->m_resurrectPosY = GetMapInfo()->repopy;
		((Player*)obj)->m_resurrectPosZ = GetMapInfo()->repopz;
	}

	obj->ClearInRangeSet();
	ASSERT(obj->GetMapId() == _mapId);
	if(!(obj->GetPositionX() < _maxX && obj->GetPositionX() > _minX) ||
	   !(obj->GetPositionY() < _maxY && obj->GetPositionY() > _minY))
	{
		if(obj->IsPlayer())
		{
			Player * plr = static_cast< Player* >( obj );
			if(plr->GetBindMapId() != GetMapId())
			{
				plr->SafeTeleport(plr->GetBindMapId(),0,plr->GetBindPositionX(),plr->GetBindPositionY(),plr->GetBindPositionZ(),0);
				plr->GetSession()->SystemMessage("Teleported you to your hearthstone location as you were out of the map boundaries.");
				return;
			}
			else
			{
				obj->GetPositionV()->ChangeCoords(plr->GetBindPositionX(),plr->GetBindPositionY(),plr->GetBindPositionZ(),0);
				plr->GetSession()->SystemMessage("Teleported you to your hearthstone location as you were out of the map boundaries.");
				MSG_S2C::stMove_Teleport_Ack Msg;
				plr->BuildTeleportAckMsg(plr->GetPosition(), &Msg);
				plr->GetSession()->SendPacket(Msg);
			}
		}
		else
		{
			obj->GetPositionV()->ChangeCoords(0,0,0,0);
		}
	}

	ASSERT(obj->GetPositionY() < _maxY && obj->GetPositionY() > _minY);
	ASSERT(_cells);

	///////////////////////
	// Get cell coordinates
	///////////////////////

	uint32 x = GetPosX(obj->GetPositionX());
	uint32 y = GetPosY(obj->GetPositionY());

	if(x >= _sizeX || y >= _sizeY)
	{
		if(obj->IsPlayer())
		{
			Player * plr = static_cast< Player* >( obj );
			if(plr->GetBindMapId() != GetMapId())
			{
				plr->SafeTeleport(plr->GetBindMapId(),0,plr->GetBindPositionX(),plr->GetBindPositionY(),plr->GetBindPositionZ(),0);
				plr->GetSession()->SystemMessage("Teleported you to your hearthstone location as you were out of the map boundaries.");
				return;
			}
			else
			{
				obj->GetPositionV()->ChangeCoords(plr->GetBindPositionX(),plr->GetBindPositionY(),plr->GetBindPositionZ(),0);
				plr->GetSession()->SystemMessage("Teleported you to your hearthstone location as you were out of the map boundaries.");
				MSG_S2C::stMove_Teleport_Ack Msg;
				plr->BuildTeleportAckMsg(plr->GetPosition(), &Msg);
				plr->GetSession()->SendPacket(Msg);
			}
		}
		else
		{
			obj->GetPositionV()->ChangeCoords(0,0,0,0);
		}

		x = GetPosX(obj->GetPositionX());
		y = GetPosY(obj->GetPositionY());
	}

	MapCell *objCell = GetCell(x,y);
	if (!objCell)
	{
		objCell = Create(x,y);
		objCell->Init(x, y, _mapId, this);

		if( m_sunyouinstance && m_sunyouinstance->IsGardenMode() )
			objCell->SetActivity( true );
	}

	uint32 endX = (x <= _sizeX) ? x + 1 : (_sizeX-1);
	uint32 endY = (y <= _sizeY) ? y + 1 : (_sizeY-1);
	uint32 startX = x > 0 ? x - 1 : 0;
	uint32 startY = y > 0 ? y - 1 : 0;
	uint32 posX, posY;
	MapCell *cell;
	MapCell::ObjectSet::iterator iter;

	static ByteBuffer s_buffer( 25000 );
	ByteBuffer * buf = &s_buffer;
	buf->clear();

	uint32 count;
	Player *plObj;

	if(obj->GetTypeId() == TYPEID_PLAYER)
		plObj = static_cast< Player* >( obj );
	else
		plObj = NULL;

	if(plObj)
	{
		MyLog::log->notice("Creating player "I64FMT" for himself.", obj->GetGUID());

 		static ByteBuffer pbuf(10000);
		pbuf.clear();

 		count = plObj->BuildCreateUpdateBlockForPlayer(&pbuf, plObj);
 		plObj->PushCreationData(&pbuf, count);
	}

	//////////////////////
	// Build in-range data
	//////////////////////

	for (posX = startX; posX <= endX; posX++ )
	{
		for (posY = startY; posY <= endY; posY++ )
		{
			cell = GetCell(posX, posY);
			if (cell)
			{
				UpdateInRangeSet(obj, plObj, cell, &buf);
			}
		}
	}

	//Add to the cell's object list
	objCell->AddObject(obj);

	obj->SetMapCell(objCell);
	 //Add to the mapmanager's object list
	if(plObj)
	{
	   m_PlayerStorage[plObj->GetLowGUID()] = plObj;
	   UpdateCellActivity(x, y, 2);
	}
	else
	{
		switch(obj->GetTypeFromGUID())
		{
		case HIGHGUID_TYPE_PET:
			m_PetStorage[obj->GetUIdFromGUID()] = static_cast< Pet* >( obj );
			break;

		case HIGHGUID_TYPE_UNIT:
			{
				ASSERT((obj->GetUIdFromGUID()) <= m_CreatureHighGuid);
				m_CreatureStorage[obj->GetUIdFromGUID()] = (Creature*)obj;
				if(((Creature*)obj)->m_spawn != NULL)
				{
					_sqlids_creatures.insert(make_pair( ((Creature*)obj)->m_spawn->id, ((Creature*)obj) ) );
				}
			}break;

		case HIGHGUID_TYPE_GAMEOBJECT:
			{
				m_GOStorage[obj->GetUIdFromGUID()] = (GameObject*)obj;
				if(((GameObject*)obj)->m_spawn != NULL)
				{
					_sqlids_gameobjects.insert(make_pair( ((GameObject*)obj)->m_spawn->id, ((GameObject*)obj) ) );
				}
			}break;

		case HIGHGUID_TYPE_DYNAMICOBJECT:
			m_DynamicObjectStorage[obj->GetLowGUID()] = (DynamicObject*)obj;
			break;
 		case HIGHGUID_TYPE_ITEM:
 		case HIGHGUID_TYPE_CONTAINER:
			if( obj->GetUInt32Value( ITEM_FIELD_OWNER ) == 0 )
 				m_ItemStorage[obj->GetLowGUID()] = (Item*)obj;
 			break;
		}
	}

	// Handle activation of that object.
	if(objCell->IsActive() && obj->CanActivate())
		obj->Activate(this);

	// Add the session to our set if it is a player.
	if(plObj)
	{
		Sessions.insert(plObj->GetSession());

		// Change the instance ID, this will cause it to be removed from the world thread (return value 1)
		plObj->GetSession()->SetInstance(GetInstanceID());

		/* Add the map wide objects */
		if(_mapWideStaticObjects.size())
		{
			//if(!buf)
			//	buf = new ByteBuffer(300);

			for(set<Object*>::iterator itr = _mapWideStaticObjects.begin(); itr != _mapWideStaticObjects.end(); ++itr)
			{
				count = (*itr)->BuildCreateUpdateBlockForPlayer(buf, plObj);
				plObj->PushCreationData(buf, count);
			}
		}
	}

	//if(buf)
	//	delete buf;

	if(plObj && InactiveMoveTime && !forced_expire)
		InactiveMoveTime = 0;
}


void MapMgr::PushStaticObject(Object *obj)
{
	_mapWideStaticObjects.insert(obj);

	switch(obj->GetTypeFromGUID())
	{
		case HIGHGUID_TYPE_UNIT:
			m_CreatureStorage[obj->GetUIdFromGUID()] = (Creature*)obj;
			break;

		case HIGHGUID_TYPE_GAMEOBJECT:
			m_GOStorage[obj->GetUIdFromGUID()] = (GameObject*)obj;
			break;

		default:
			// maybe add a warning, shouldnt be needed
			break;
	}
}

#define OPTIONAL_IN_RANGE_SETS

void MapMgr::RemoveObject(Object *obj, bool free_guid)
{
	/////////////
	// Assertions
	/////////////

	ASSERT(obj);

	if( !g_crosssrvmgr->m_isInstanceSrv )
		ASSERT(obj->GetMapId() == _mapId);

	//ASSERT(obj->GetPositionX() > _minX && obj->GetPositionX() < _maxX);
	//ASSERT(obj->GetPositionY() > _minY && obj->GetPositionY() < _maxY);
	ASSERT(_cells);

	if(obj->Active)
		obj->Deactivate(this);

	if( m_sunyouinstance && obj->IsPlayer() )
	{
		if( static_cast<Player*>( obj )->m_sunyou_instance == m_sunyouinstance )
		{
			m_sunyouinstance->TriggerLeave( static_cast<Player*>( obj ) );
		}
		static_cast<Player*>( obj )->m_sunyou_instance = NULL;
	}

	_updates.erase( obj );
	//obj->ClearUpdateMask();
	Player* plObj = (obj->GetTypeId() == TYPEID_PLAYER) ? static_cast< Player* >( obj ) : 0;

	///////////////////////////////////////
	// Remove object from all needed places
	///////////////////////////////////////
	// That object types are not map objects. TODO: add AI groups here?
	if(obj->GetTypeId() == TYPEID_ITEM || obj->GetTypeId() == TYPEID_CONTAINER )
	{
		if( ((Item*)obj)->GetOwner() )// ourself mapitem
			return;
	}

	switch(obj->GetTypeFromGUID())
	{
		case HIGHGUID_TYPE_UNIT:
			ASSERT(obj->GetUIdFromGUID() <= m_CreatureHighGuid);
			m_CreatureStorage[obj->GetUIdFromGUID()] = 0;
			if(((Creature*)obj)->m_spawn != NULL)
			{
				_sqlids_creatures.erase(((Creature*)obj)->m_spawn->id);
			}

			if(free_guid)
				_reusable_guids_creature.push_back(obj->GetUIdFromGUID());

			  break;

		case HIGHGUID_TYPE_PET:
			m_PetStorage.erase(obj->GetUIdFromGUID());
			break;

		case HIGHGUID_TYPE_DYNAMICOBJECT:
			m_DynamicObjectStorage.erase(obj->GetLowGUID());
			break;
 		case HIGHGUID_TYPE_ITEM:
 		case HIGHGUID_TYPE_CONTAINER:
 			m_ItemStorage.erase(obj->GetLowGUID());
 			break;
		case HIGHGUID_TYPE_GAMEOBJECT:
			ASSERT(obj->GetUIdFromGUID() <= m_GOHighGuid);
			m_GOStorage[obj->GetUIdFromGUID()] = 0;
			if(((GameObject*)obj)->m_spawn != NULL)
			{
				_sqlids_gameobjects.erase(((GameObject*)obj)->m_spawn->id);
			}

			if(free_guid)
				_reusable_guids_gameobject.push_back(obj->GetUIdFromGUID());

			break;
	}



	if(obj->GetTypeId() == TYPEID_CORPSE)
	{
		m_corpses.erase(((Corpse*)obj));
	}

	if(!obj->GetMapCell())
	{
		/* set the map cell correctly */
		if(obj->GetPositionX() >= _maxX || obj->GetPositionX() <= _minY ||
			obj->GetPositionY() >= _maxY || obj->GetPositionY() <= _minY)
		{
			// do nothing
		}
		else
		{
			obj->SetMapCell(this->GetCellByCoords(obj->GetPositionX(), obj->GetPositionY()));
		}
	}

	if(obj->GetMapCell())
	{
		ASSERT(obj->GetMapCell());

		// Remove object from cell
		obj->GetMapCell()->RemoveObject(obj);

		// Unset object's cell
		obj->SetMapCell(NULL);
	}

	// Clear any updates pending
	if(obj->GetTypeId() == TYPEID_PLAYER)
	{
		//_processQueue.erase( static_cast< Player* >( obj ) );
		//static_cast< Player* >( obj )->ClearAllPendingUpdates();
	}

	// Remove object from all objects 'seeing' him
	for (Object::InRangeSet::iterator iter = obj->GetInRangeSetBegin();
		iter != obj->GetInRangeSetEnd(); ++iter)
	{
		if( (*iter) )
		{
			if( (*iter)->GetTypeId() == TYPEID_PLAYER )
			{
				if( static_cast< Player* >( *iter )->IsVisible( obj ) && static_cast< Player* >( *iter )->m_TransporterGUID != obj->GetGUID() )
					static_cast< Player* >( *iter )->PushOutOfRange(obj->GetNewGUID());
			}
			(*iter)->RemoveInRangeObject(obj);
		}
	}

	// Clear object's in-range set
	obj->ClearInRangeSet();

	// If it's a player - update his nearby cells
	if(!_shutdown && obj->GetTypeId() == TYPEID_PLAYER)
	{
		// get x/y
		if(obj->GetPositionX() >= _maxX || obj->GetPositionX() <= _minY ||
			obj->GetPositionY() >= _maxY || obj->GetPositionY() <= _minY)
		{
			// do nothing
		}
		else
		{
			uint32 x = GetPosX(obj->GetPositionX());
			uint32 y = GetPosY(obj->GetPositionY());
			UpdateCellActivity(x, y, 2);
		}
		m_PlayerStorage.erase( static_cast< Player* >( obj )->GetLowGUID() );
	}

	// Remove the session from our set if it is a player.
	if(plObj)
	{
		for(set<Object*>::iterator itr = _mapWideStaticObjects.begin(); itr != _mapWideStaticObjects.end(); ++itr)
		{
			plObj->PushOutOfRange((*itr)->GetNewGUID());
		}

		// Setting an instance ID here will trigger the session to be removed
		// by MapMgr::run(). :)
		plObj->GetSession()->SetInstance(0);

		// Add it to the global session set.
		// Don't "re-add" to session if it is being deleted.
		if(!plObj->GetSession()->bDeleted)
			sWorld.AddGlobalSession(plObj->GetSession());
	}

	if(!HasPlayers() && !InactiveMoveTime && !forced_expire && GetMapInfo()->type != INSTANCE_NULL)
	{
		InactiveMoveTime = UNIXTIME + (MAPMGR_INACTIVE_MOVE_TIME * 60);	   // 5 mins -> move to inactive
	}

	if( obj->IsPlayer() && m_pInstance && !HasPlayers() )
	{
		m_pInstance->m_expiration = UNIXTIME + 60*5;
	}
}

void MapMgr::ChangeObjectLocation( Object *obj )
{
	if( obj->GetMapMgr() != this )
		return;

	// Items and containers are of no interest for us
	if( obj->GetTypeId() == TYPEID_ITEM || obj->GetTypeId() == TYPEID_CONTAINER )	{
		if( ((Item*)obj)->GetOwner() )// ourself mapitem
			return;
	}

	Player* plObj;

	if( obj->GetTypeId() == TYPEID_PLAYER )
	{
		plObj = static_cast< Player* >( obj );
	}
	else
	{
		plObj = NULL;
	}

	Object* curObj;
	float fRange;

	///////////////////////////////////////
	// Update in-range data for old objects
	///////////////////////////////////////

	/** let's duplicate some code here :P Less branching is always good.
	 * - Burlex
	 */
/*#define IN_RANGE_LOOP \
	for (Object::InRangeSet::iterator iter = obj->GetInRangeSetBegin(), iter2; \
		iter != obj->GetInRangeSetEnd();) \
	{ \
		curObj = *iter; \
		iter2 = iter; \
		++iter; \
		if(curObj->IsPlayer() && obj->IsPlayer() && plObj->m_TransporterGUID && plObj->m_TransporterGUID == static_cast< Player* >( curObj )->m_TransporterGUID ) \
			fRange = 0.0f;		\
		else if((curObj->GetGUIDHigh() == HIGHGUID_TRANSPORTER || obj->GetGUIDHigh() == HIGHGUID_TRANSPORTER)) \
			fRange = 0.0f;		\
		else if((curObj->GetGUIDHigh() == HIGHGUID_GAMEOBJECT && curObj->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_TRANSPORT || obj->GetGUIDHigh() == HIGHGUID_GAMEOBJECT && obj->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_TRANSPORT)) \
			fRange = 0.0f;		\
		else \
			fRange = m_UpdateDistance;	\
		if (curObj->GetDistance2dSq(obj) > fRange && fRange > 0) \

#define END_IN_RANGE_LOOP } \

	if(plObj)
	{
		IN_RANGE_LOOP
		{
			plObj->RemoveIfVisible(curObj);
			plObj->RemoveInRangeObject(iter2);

			if(curObj->NeedsInRangeSet())
				curObj->RemoveInRangeObject(obj);

			if(curObj->IsPlayer())
				static_cast< Player* >( curObj )->RemoveIfVisible(obj);
		}
		END_IN_RANGE_LOOP
	}
	else if(obj->NeedsInRangeSet())
	{
		IN_RANGE_LOOP
		{
			if(curObj->NeedsInRangeSet())
				curObj->RemoveInRangeObject(obj);

			if(curObj->IsPlayer())
				static_cast< Player* >( curObj )->RemoveIfVisible(obj);

			obj->RemoveInRangeObject(iter2);
		}
		END_IN_RANGE_LOOP
	}
	else
	{
		IN_RANGE_LOOP
		{
			if(curObj->NeedsInRangeSet())
				curObj->RemoveInRangeObject(obj);

			if(curObj->IsPlayer())
			{
				static_cast< Player* >( curObj )->RemoveIfVisible(obj);
				obj->RemoveInRangePlayer(curObj);
			}
		}
		END_IN_RANGE_LOOP
	}

#undef IN_RANGE_LOOP
#undef END_IN_RANGE_LOOP*/

	if(obj->HasInRangeObjects()) {
		for (Object::InRangeSet::iterator iter = obj->GetInRangeSetBegin(), iter2;
			iter != obj->GetInRangeSetEnd();)
		{
			curObj = *iter;
			iter2 = iter++;
			if( curObj->IsPlayer() && obj->IsPlayer() && plObj->m_TransporterGUID && plObj->m_TransporterGUID == static_cast< Player* >( curObj )->m_TransporterGUID )
				fRange = 0.0f; // unlimited distance for people on same boat
			else if( curObj->GetTypeFromGUID() == HIGHGUID_TYPE_TRANSPORTER )
				fRange = 0.0f; // unlimited distance for transporters (only up to 2 cells +/- anyway.)
			else
				fRange = m_UpdateDistance; // normal distance

			if( fRange > 0.0f && curObj->GetDistance2dSq(obj) > fRange )
			{
				if( plObj )
					plObj->RemoveIfVisible(curObj);

				if( curObj->IsPlayer() )
					static_cast< Player* >( curObj )->RemoveIfVisible(obj);

				curObj->RemoveInRangeObject(obj);

				if( obj->GetMapMgr() != this )
				{
					/* Something removed us. */
					return;
				}
				obj->RemoveInRangeObject(iter2);
			}
		}
	}

	///////////////////////////
	// Get new cell coordinates
	///////////////////////////
	if(obj->GetMapMgr() != this)
	{
		/* Something removed us. */
		return;
	}

	if(obj->GetPositionX() >= _maxX || obj->GetPositionX() <= _minX ||
		obj->GetPositionY() >= _maxY || obj->GetPositionY() <= _minY)
	{
		if(obj->IsPlayer())
		{
			Player* plr = static_cast< Player* >( obj );
			if(plr->GetBindMapId() != GetMapId())
			{
				plr->SafeTeleport(plr->GetBindMapId(),0,plr->GetBindPositionX(),plr->GetBindPositionY(),plr->GetBindPositionZ(),0);
				plr->GetSession()->SystemMessage("Teleported you to your hearthstone location as you were out of the map boundaries.");
				return;
			}
			else
			{
				obj->GetPositionV()->ChangeCoords(plr->GetBindPositionX(),plr->GetBindPositionY(),plr->GetBindPositionZ(),0);
				plr->GetSession()->SystemMessage("Teleported you to your hearthstone location as you were out of the map boundaries.");
				MSG_S2C::stMove_Teleport_Ack Msg;
				plr->BuildTeleportAckMsg(plr->GetPosition(), &Msg);
				plr->GetSession()->SendPacket(Msg);
			}
		}
		else
		{
			obj->GetPositionV()->ChangeCoords(0,0,0,0);
		}
	}

	uint32 cellX = GetPosX(obj->GetPositionX());
	uint32 cellY = GetPosY(obj->GetPositionY());

	if(cellX >= _sizeX || cellY >= _sizeY)
	{
		return;
	}

	MapCell *objCell = GetCell(cellX, cellY);
	MapCell * pOldCell = obj->GetMapCell();
	if (!objCell)
	{
		objCell = Create(cellX,cellY);
		objCell->Init(cellX, cellY, _mapId, this);
	}

	// If object moved cell
	if (objCell != obj->GetMapCell())
	{
		// THIS IS A HACK!
		// Current code, if a creature on a long waypoint path moves from an active
		// cell into an inactive one, it will disable itself and will never return.
		// This is to prevent cpu leaks. I will think of a better solution very soon :P

		if(!objCell->IsActive() && !plObj && obj->Active)
			obj->Deactivate(this);

		if(obj->GetMapCell())
			obj->GetMapCell()->RemoveObject(obj);

		objCell->AddObject(obj);
		obj->SetMapCell(objCell);

		// if player we need to update cell activity
		// radius = 2 is used in order to update both
		// old and new cells
		if(obj->GetTypeId() == TYPEID_PLAYER)
		{
			// have to unlock/lock here to avoid a deadlock situation.
			UpdateCellActivity(cellX, cellY, 2);
			if( pOldCell != NULL )
			{
				// only do the second check if theres -/+ 2 difference
				if( abs( (int)cellX - (int)pOldCell->_x ) > 2 ||
					abs( (int)cellY - (int)pOldCell->_y ) > 2 )
				{
					UpdateCellActivity( pOldCell->_x, pOldCell->_y, 2 );
				}
			}
		}
	}


	//////////////////////////////////////
	// Update in-range set for new objects
	//////////////////////////////////////
	
	static ByteBuffer s_buffer( 25000 );
	ByteBuffer* buf = &s_buffer;
	buf->clear();

	if( m_UpdateDistance > 40000 )
	{
		uint32 endX = (_sizeX-1);
		uint32 endY = (_sizeY-1);
		uint32 startX = 0;
		uint32 startY = 0;
		uint32 posX, posY;
		MapCell *cell;

		for (posX = startX; posX <= endX; ++posX )
		{
			for (posY = startY; posY <= endY; ++posY )
			{
				cell = GetCell(posX, posY);
				if (cell)
					UpdateInRangeSet(obj, plObj, cell, &buf);
			}
		}
	}
	else
	{
		uint32 endX = cellX <= _sizeX ? cellX + 1 : (_sizeX-1);
		uint32 endY = cellY <= _sizeY ? cellY + 1 : (_sizeY-1);
		uint32 startX = cellX > 0 ? cellX - 1 : 0;
		uint32 startY = cellY > 0 ? cellY - 1 : 0;
		uint32 posX, posY;
		MapCell *cell;

		for (posX = startX; posX <= endX; ++posX )
		{
			for (posY = startY; posY <= endY; ++posY )
			{
				cell = GetCell(posX, posY);
				if (cell)
				UpdateInRangeSet(obj, plObj, cell, &buf);
			}
		}
	}
}

void MapMgr::UpdateInRangeSet( Object *obj, Player *plObj, MapCell* cell, ByteBuffer ** buf )
{
	#define CHECK_BUF if(!*buf) *buf = new ByteBuffer(2500)

	if( cell == NULL )
		return;

	Object* curObj;
	Player* plObj2;
	int count;
	ObjectSet::iterator iter = cell->Begin();
	ObjectSet::iterator itr;
	float fRange;
	bool cansee, isvisible;

	while( iter != cell->End() )
	{
		curObj = *iter;
		++iter;

		if( curObj == NULL )
			continue;

		if( curObj->IsPlayer() && obj->IsPlayer() && plObj->m_TransporterGUID && plObj->m_TransporterGUID == static_cast< Player* >( curObj )->m_TransporterGUID )
			fRange = 0.0f; // unlimited distance for people on same boat
		else if( curObj->GetTypeFromGUID() == HIGHGUID_TYPE_TRANSPORTER )
			fRange = 0.0f; // unlimited distance for transporters (only up to 2 cells +/- anyway.)
		else
			fRange = m_UpdateDistance; // normal distance

		if ( curObj != obj && ( curObj->GetDistance2dSq( obj ) <= fRange || fRange == 0.0f ) )
		{
			if( !obj->IsInRangeSet( curObj ) )
			{
				// Object in range, add to set
				obj->AddInRangeObject( curObj );
				curObj->AddInRangeObject( obj );

				if( curObj->IsPlayer() )
				{
					plObj2 = static_cast< Player* >( curObj );

					if( plObj2->CanSee( obj ) && !plObj2->IsVisible( obj ) )
					{
						CHECK_BUF;
						count = obj->BuildCreateUpdateBlockForPlayer(*buf, plObj2);
						plObj2->PushCreationData(*buf, count);
						plObj2->AddVisibleObject(obj);
						(*buf)->clear();

						if(obj->IsCreature())
							((Unit*)obj)->GetAIInterface()->OnPlayerEnterInRange(plObj2);
					}
				}

				if( plObj != NULL )
				{
					if( plObj->CanSee( curObj ) && !plObj->IsVisible( curObj ) )
					{
						CHECK_BUF;
						count = curObj->BuildCreateUpdateBlockForPlayer( *buf, plObj );
						plObj->PushCreationData( *buf, count );
						plObj->AddVisibleObject( curObj );
						(*buf)->clear();

						//monster move ai
						if(curObj->IsCreature())
							((Unit*)curObj)->GetAIInterface()->OnPlayerEnterInRange(plObj);
					}
				}
			}
			else
			{
				// Check visiblility
				if( curObj->IsPlayer() )
				{
					plObj2 = static_cast< Player* >( curObj );
					cansee = plObj2->CanSee(obj);
					isvisible = plObj2->GetVisibility(obj, &itr);
					if(!cansee && isvisible)
					{
						plObj2->PushOutOfRange(obj->GetNewGUID());
					}
					else if(cansee && !isvisible)
					{
						CHECK_BUF;
						count = obj->BuildCreateUpdateBlockForPlayer(*buf, plObj2);
						plObj2->PushCreationData(*buf, count);
						plObj2->AddVisibleObject(obj);
						(*buf)->clear();

						//monster move ai
						if(obj->IsCreature())
							((Unit*)obj)->GetAIInterface()->OnPlayerEnterInRange(plObj2);
					}
				}

				if( plObj )
				{
					cansee = plObj->CanSee( curObj );
					isvisible = plObj->GetVisibility( curObj, &itr );
					if(!cansee && isvisible)
					{
						plObj->PushOutOfRange( curObj->GetNewGUID() );
					}
					else if(cansee && !isvisible)
					{
						CHECK_BUF;
						count = curObj->BuildCreateUpdateBlockForPlayer( *buf, plObj );
						plObj->PushCreationData( *buf, count );
						plObj->AddVisibleObject( curObj );
						(*buf)->clear();

						//monster move ai
						if(curObj->IsCreature())
							((Unit*)curObj)->GetAIInterface()->OnPlayerEnterInRange(plObj);
					}
				}
			}
		}
	}
}

void MapMgr::_UpdateObjects()
{
	if(!_updates.size() /*&& !_processQueue.size()*/)
		return;

	Object *pObj;
	Player *pOwner;
	//std::set<Object*>::iterator it_start, it_end, itr;
	std::set<Player*>::iterator it_start, it_end, itr;
	Player * lplr;
	static ByteBuffer update(66000);
	update.clear();
	uint32 count = 0;

	UpdateQueue::iterator iter = _updates.begin();
	PUpdateQueue::iterator it, eit;

	for(; iter != _updates.end();)
	{
		pObj = *iter;
		++iter;
		if(!pObj) continue;
		if( !pObj->IsValid() ) continue;

		if(pObj->GetTypeId() == TYPEID_ITEM || pObj->GetTypeId() == TYPEID_CONTAINER)
		{
			// our update is only sent to the owner here.
			pOwner = static_cast< Item* >(pObj)->GetOwner();
			if( pOwner != NULL )
			{
				count = static_cast< Item* >( pObj )->BuildValuesUpdateBlockForPlayer( &update, pOwner );
				// send update to owner
				if( count )
				{
					pOwner->PushUpdateData( &update, count );
					update.clear();
				}
			}
 			else// ourself mapitem
 			{
 				if( pObj->IsInWorld() )
 				{
 					// build the update
 					count = pObj->BuildValuesUpdateBlockForPlayer( &update, static_cast< Player* >( NULL ) );

					if( count )
					{
						it_start = pObj->GetInRangePlayerSetBegin();
						it_end = pObj->GetInRangePlayerSetEnd();
						for(itr = it_start; itr != it_end;)
						{
							lplr = *itr;
							++itr;
							// Make sure that the target player can see us.
							if( lplr->GetTypeId() == TYPEID_PLAYER && lplr->IsVisible( pObj ) )
								lplr->PushUpdateData( &update, count );
						}
						update.clear();
					}
				}
			}
		}
		else
		{
			if( pObj->IsInWorld() )
			{
				// players have to receive their own updates ;)
				if( pObj->GetTypeId() == TYPEID_PLAYER )
				{
					// need to be different! ;)
					count = pObj->BuildValuesUpdateBlockForPlayer( &update, static_cast< Player* >( pObj ) );
					if( count )
					{
						static_cast< Player* >( pObj )->PushUpdateData( &update, count );
						update.clear();
					}
				}

				if( pObj->IsUnit() && pObj->HasUpdateField( UNIT_FIELD_HEALTH ) )
					static_cast< Unit* >( pObj )->EventHealthChangeSinceLastUpdate();

				// build the update
				count = pObj->BuildValuesUpdateBlockForPlayer( &update, static_cast< Player* >( NULL ) );

				if( count )
				{
					it_start = pObj->GetInRangePlayerSetBegin();
					it_end = pObj->GetInRangePlayerSetEnd();
					for(itr = it_start; itr != it_end;)
					{
						lplr = *itr;
						++itr;
						// Make sure that the target player can see us.
						if( lplr->GetTypeId() == TYPEID_PLAYER && lplr->IsVisible( pObj ) )
							lplr->PushUpdateData( &update, count );
					}
					update.clear();
				}
			}
		}
		pObj->ClearUpdateMask();
		pObj->m_isNeedUpdate = false;
	}
	_updates.clear();
}
void MapMgr::LoadAllCells()
{
	// eek
	MapCell * cellInfo;
	CellSpawns * spawns;

	for( uint32 x = 0 ; x < _sizeX ; x ++ )
	{
		for( uint32 y = 0 ; y < _sizeY ; y ++ )
		{
			cellInfo = GetCell( x , y );

			if( !cellInfo )
			{
				// Cell doesn't exist, create it.
				// There is no spoon. Err... cell.
				cellInfo = Create( x , y );
				cellInfo->Init( x , y , _mapId , this );
				MyLog::log->notice( "Created cell [%u,%u] on map %d (instance %d)." , x , y , _mapId , m_instanceID );
				cellInfo->SetActivity( true );
				ASSERT( !cellInfo->IsLoaded() );

				//if( m_sunyouinstance && !m_sunyouinstance->IsInitCreatures() )
				//	continue;

				spawns = _map->GetSpawnsList( x , y );
				if( spawns )
					cellInfo->LoadObjects( spawns );
			}
			else
			{
				// Cell exists, but is inactive
				if ( !cellInfo->IsActive() )
				{
					MyLog::log->notice("Activated cell [%u,%u] on map %d (instance %d).", x, y, _mapId, m_instanceID );
					cellInfo->SetActivity( true );

					if (!cellInfo->IsLoaded())
					{
						//MyLog::log->notice("Loading objects for Cell [%d][%d] on map %d (instance %d)...",
						//	posX, posY, this->_mapId, m_instanceID);
						//if( m_sunyouinstance && !m_sunyouinstance->IsInitCreatures() )
						//	continue;

						spawns = _map->GetSpawnsList( x , y );
						if( spawns )
							cellInfo->LoadObjects( spawns );
					}
				}
			}
		}
	}
}

void MapMgr::UpdateCellActivity( int x, int y, int radius)
{
	CellSpawns * sp;
	uint32 endX = (x + radius) <= _sizeX ? x + radius : (_sizeX-1);
	uint32 endY = (y + radius) <= _sizeY ? y + radius : (_sizeY-1);
	uint32 startX = x - radius > 0 ? x - radius : 0;
	uint32 startY = y - radius > 0 ? y - radius : 0;
	uint32 posX, posY;

	MapCell *objCell;

	for (posX = startX; posX <= endX; posX++ )
	{
		for (posY = startY; posY <= endY; posY++ )
		{
			objCell = GetCell(posX, posY);

			if (!objCell)
			{
				if (_CellActive(posX, posY))
				{
					objCell = Create(posX, posY);
					objCell->Init(posX, posY, _mapId, this);

					//MyLog::log->notice("Cell [%d,%d] on map %d (instance %d) is now active.",
					//	posX, posY, this->_mapId, m_instanceID);
					objCell->SetActivity(true);

					//if( m_sunyouinstance && !m_sunyouinstance->IsInitCreatures() )
					//	continue;

					ASSERT(!objCell->IsLoaded());

					//MyLog::log->notice("Loading objects for Cell [%d][%d] on map %d (instance %d)...",
					//	posX, posY, this->_mapId, m_instanceID);

					sp = _map->GetSpawnsList(posX, posY);
					if(sp) objCell->LoadObjects(sp);
				}
			}
			else
			{
				//Cell is now active
				if (_CellActive(posX, posY) && !objCell->IsActive())
				{
					//MyLog::log->notice("Cell [%d,%d] on map %d (instance %d) is now active.",
					//	posX, posY, this->_mapId, m_instanceID);
					objCell->SetActivity(true);

					//if( m_sunyouinstance && !m_sunyouinstance->IsInitCreatures() )
					//	continue;

					if (!objCell->IsLoaded())
					{
					//	MyLog::log->notice("Loading objects for Cell [%d][%d] on map %d (instance %d)...",
					//		posX, posY, this->_mapId, m_instanceID);
						sp = _map->GetSpawnsList(posX, posY);
						if(sp) objCell->LoadObjects(sp);
					}
				}
				//Cell is no longer active
				else if (!_CellActive(posX, posY) && objCell->IsActive())
				{
					//MyLog::log->notice("Cell [%d,%d] on map %d (instance %d) is now idle.",
					//	posX, posY, this->_mapId, m_instanceID);
					if( m_sunyouinstance && !m_sunyouinstance->IsInitCreatures() )
						continue;

					objCell->SetActivity(false);
				}
			}
		}
	}

}

bool MapMgr::_CellActive(uint32 x, uint32 y)
{
	if( m_pInstance ) return true;

	uint32 endX = ((x+1) < _sizeX) ? x + 1 : (_sizeX-1);
	uint32 endY = ((y+1) < _sizeY) ? y + 1 : (_sizeY-1);
	uint32 startX = x > 0 ? x - 1 : 0;
	uint32 startY = y > 0 ? y - 1 : 0;
	uint32 posX, posY;

	MapCell *objCell;

	for (posX = startX; posX <= endX; posX++ )
	{
		for (posY = startY; posY <= endY; posY++ )
		{
			objCell = GetCell(posX, posY);

			if (objCell)
			{
				if (objCell->HasPlayers())
				{
					return true;
				}
			}
		}
	}

	return false;
}

void MapMgr::ObjectUpdated(Object *obj)
{
	// set our fields to dirty
	// stupid fucked up code in places.. i hate doing this but i've got to :<
	// - burlex
	_updates.insert(obj);
}

void MapMgr::PushToProcessed(Player* plr)
{
//	_processQueue.insert(plr);
}

void MapMgr::ChangeFarsightLocation(Player *plr, DynamicObject *farsight)
{
	if(farsight == 0)
	{
		// We're clearing.
		for(ObjectSet::iterator itr = plr->m_visibleFarsightObjects.begin(); itr != plr->m_visibleFarsightObjects.end();
			++itr)
		{
			if(plr->IsVisible((*itr)) && !plr->CanSee((*itr)))
			{
				// Send destroy
				plr->PushOutOfRange((*itr)->GetNewGUID());
			}
		}
		plr->m_visibleFarsightObjects.clear();
	}
	else
	{
		uint32 cellX = GetPosX(farsight->GetPositionX());
		uint32 cellY = GetPosY(farsight->GetPositionY());
		uint32 endX = (cellX <= _sizeX) ? cellX + 1 : (_sizeX-1);
		uint32 endY = (cellY <= _sizeY) ? cellY + 1 : (_sizeY-1);
		uint32 startX = cellX > 0 ? cellX - 1 : 0;
		uint32 startY = cellY > 0 ? cellY - 1 : 0;
		uint32 posX, posY;
		MapCell *cell;
		Object *obj;
		MapCell::ObjectSet::iterator iter, iend;
		uint32 count;
		for (posX = startX; posX <= endX; ++posX )
		{
			for (posY = startY; posY <= endY; ++posY )
			{
				cell = GetCell(posX, posY);
				if (cell)
				{
					iter = cell->Begin();
					iend = cell->End();
					for(; iter != iend; ++iter)
					{
						obj = (*iter);
						if(!plr->IsVisible(obj) && plr->CanSee(obj) && farsight->GetDistance2dSq(obj) <= m_UpdateDistance)
						{
							static ByteBuffer buf( 25000 );
							buf.clear();
							count = obj->BuildCreateUpdateBlockForPlayer(&buf, plr);
							plr->PushCreationData(&buf, count);
							plr->m_visibleFarsightObjects.insert(obj);
						}
					}

				}
			}
		}
	}
}

/* new stuff
*/

bool MapMgr::Init()
{
	ObjectSet::iterator i;

	/* create static objects */
	for(GOSpawnList::iterator itr = _map->staticSpawns.GOSpawns.begin(); itr != _map->staticSpawns.GOSpawns.end(); ++itr)
	{
		GameObject * obj = CreateGameObject((*itr)->entry);
		obj->Load((*itr));
		_mapWideStaticObjects.insert(obj);
	}

	for(CreatureSpawnList::iterator itr = _map->staticSpawns.CreatureSpawns.begin(); itr != _map->staticSpawns.CreatureSpawns.end(); ++itr)
	{
		Creature * obj = CreateCreature((*itr)->entry);
		obj->Load(*itr, 0, pMapInfo);
		_mapWideStaticObjects.insert(obj);
	}

	/* add static objects */
	for(set<Object*>::iterator itr = _mapWideStaticObjects.begin(); itr != _mapWideStaticObjects.end(); ++itr)
		PushStaticObject(*itr);

	/* load corpses */
	objmgr.LoadCorpses(this);

	// always declare local variables outside of the loop!
	// otherwise theres a lot of sub esp; going on.
	m_startTime=getMSTime();

	return true;
}

void MapMgr::SpawnCreatures( const std::set<uint32>& ids, Unit* threadc, std::set<uint32>& out )
{
	for( std::set<CreatureSpawn*>::iterator itr = _map->m_allspawns.begin(); itr != _map->m_allspawns.end(); ++itr)
	{
		CreatureSpawn* cs = *itr;

		if( ids.end() == ids.find( cs->id ) )
			continue;

		CreatureProto* proto = CreatureProtoStorage.LookupEntry( cs->entry );
		if( !proto )
			continue;

		CreatureInfo* info = CreatureNameStorage.LookupEntry( cs->entry );
		if(!info)
			continue;
		Creature * obj = CreateCreature( cs->entry );
		if( obj->Load( cs, 0, pMapInfo, proto ) )
		{
			obj->m_noRespawn = true;
			obj->m_gardenmode = false;
			obj->PushToWorld( this );

			if( threadc )
			{
				obj->GetAIInterface()->AttackReaction( threadc, 100 );

				if( threadc->IsCreature() )
					((Creature*)threadc)->GetAIInterface()->AttackReaction( obj, 1000 );
			}
			out.insert( GET_LOWGUID_PART(obj->GetGUID()) );
		}
		else
			delete obj;
	}
}

void MapMgr::SpawnActivityCreature( uint32 spawnid, uint32& guid, bool respawn, bool is_creature )
{
	if( is_creature )
	{
		std::map<uint32, CreatureSpawn*>::iterator it = _map->m_mapspawns.find( spawnid );
		if( it != _map->m_mapspawns.end() )
		{
			CreatureSpawn* cs = it->second;
			CreatureProto* proto = CreatureProtoStorage.LookupEntry( cs->entry );
			if( !proto )
				return;

			CreatureInfo* info = CreatureNameStorage.LookupEntry( cs->entry );
			if(!info)
				return;
			Creature * obj = CreateCreature( cs->entry );
			if( obj->Load( cs, 0, pMapInfo, proto ) )
			{
				obj->m_noRespawn = !respawn;
				obj->PushToWorld( this );
				guid = GET_LOWGUID_PART(obj->GetGUID());
			}
			else
				delete obj;
		}
	}
	else
	{
		std::map<uint32, GOSpawn*>::iterator it = _map->m_mapGOSpawns.find( spawnid );
		if( it != _map->m_mapGOSpawns.end() )
		{
			GOSpawn* gos = it->second;
			GameObject * go= CreateGameObject(gos->entry);
			if(go->Load( gos ))
			{
				//uint32 state = go->GetUInt32Value(GAMEOBJECT_STATE);

				// FIXME - burlex
				/*
				if(pInstance && pInstance->FindObject((*i)->stateNpcLink))
				{
					go->SetUInt32Value(GAMEOBJECT_STATE, (state ? 0 : 1));
				}*/			   

				go->m_loadedFromDB = true;
				go->PushToWorld( this );
				guid = GET_LOWGUID_PART(go->GetGUID());
			}
			else
				delete go;//missing proto or smth of that kind
		}
	}
}

void MapMgr::DespawnActivityCreature( uint32 guid, bool is_creature )
{
	if( is_creature )
	{
		Creature* p = GetCreature( guid );
		if( p && p->IsValid() )
			p->SafeDelete();
	}
	else
	{
		GameObject* p = GetGameObject( guid );
		if( p && p->IsValid() )
			p->Despawn( 0 );
	}
}

void MapMgr::SpawnCreatures( uint32 minlv, uint32 maxlv, bool autoSpawn, bool gardenmode, std::set<Player*>& threatadd )
{
	for( std::set<CreatureSpawn*>::iterator itr = _map->m_allspawns.begin(); itr != _map->m_allspawns.end(); ++itr)
	{
		CreatureSpawn* cs = *itr;

		CreatureProto* proto = CreatureProtoStorage.LookupEntry( cs->entry );
		if( !proto )
			continue;

		CreatureInfo* info = CreatureNameStorage.LookupEntry( cs->entry );
		if(!info)
			continue;
		if( proto->MinLevel >= minlv && proto->MinLevel <= maxlv )
		{
			Creature * obj = CreateCreature( cs->entry );
			if( obj->Load( cs, 0, pMapInfo, proto ) )
			{
				obj->m_noRespawn = !autoSpawn;
				obj->m_gardenmode = gardenmode;
				obj->PushToWorld( this );

				std::map<float, Unit*> mu;
				for( std::set<Player*>::iterator it = threatadd.begin(); it != threatadd.end(); ++it )
				{
					Unit* p = *it;
					if( p->IsValid() )
					{
						float dis = p->GetDistance2dSq( obj );
						mu.insert( std::make_pair( dis, p ) );
					}
				}
				size_t n = mu.size();
				for( std::map<float, Unit*>::iterator it = mu.begin(); it != mu.end(); ++it )
				{
					Unit* p = it->second;
					obj->GetAIInterface()->AttackReaction( p, n-- );
				}
			}
			else
				delete obj;
		}
	}
	MyLog::log->debug( "instance:%d spawn creatures:%d", GetInstanceID(), _map->m_allspawns.size() );
}

void MapMgr::SpawnCreatures( SunyouCastle* castle, const std::set<uint32>& ids, std::set<uint32>& out, bool invincible /* = false */ )
{
	out.clear();
	for( std::set<CreatureSpawn*>::iterator itr = _map->m_allspawns.begin(); itr != _map->m_allspawns.end(); ++itr)
	{
		CreatureSpawn* cs = *itr;
		if( ids.end() == ids.find( cs->id ) )
			continue;

		CreatureProto* proto = CreatureProtoStorage.LookupEntry( cs->entry );
		if( !proto )
			continue;

		CreatureInfo* info = CreatureNameStorage.LookupEntry( cs->entry );
		if(!info)
			continue;

		Creature * obj = CreateCreature( cs->entry );
		if( obj->Load( cs, 0, pMapInfo, proto ) )
		{
			obj->m_noRespawn = true;
			obj->m_castlemode = true;
			obj->m_castle = castle;
			obj->m_is_castle_invincible = invincible;
			if( invincible )
			{
				obj->GetAIInterface()->InitForCastle( false, true );
			}
			obj->PushToWorld( this );
			out.insert( GET_LOWGUID_PART(obj->GetGUID()) );
		}
		else
			delete obj;
	}
}

void MapMgr::SpawnCreatures( SunyouBattleGround* bg, const std::map<uint32, std::pair<uint32, bool> >& ids, std::set<uint32>& out, uint32 faction )
{
	out.clear();
	for( std::set<CreatureSpawn*>::iterator itr = _map->m_allspawns.begin(); itr != _map->m_allspawns.end(); ++itr)
	{
		CreatureSpawn* cs = *itr;
		
		std::map<uint32, std::pair<uint32, bool> >::const_iterator it2 = ids.find( cs->id );
		if( ids.end() == it2 )
			continue;

		CreatureProto* proto = CreatureProtoStorage.LookupEntry( cs->entry );
		if( !proto )
			continue;

		CreatureInfo* info = CreatureNameStorage.LookupEntry( cs->entry );
		if(!info)
			continue;

		Creature * obj = CreateCreature( cs->entry );
		if( obj->Load( cs, 0, pMapInfo, proto ) )
		{
			obj->m_battleground = bg;
			obj->m_battlepower = it2->second.first;
			obj->m_noRespawn = !it2->second.second;
			obj->m_bgfaction = faction;
			obj->PushToWorld( this );
			obj->SetFFA( 11 + faction );
			out.insert( GET_LOWGUID_PART(obj->GetGUID()) );
		}
		else
			delete obj;
	}
}

void MapMgr::SpawnCastleBoss( SunyouCastle* castle, uint32 boss, uint32& boss_guid )
{
	boss_guid = 0;
	for( std::set<CreatureSpawn*>::iterator itr = _map->m_allspawns.begin(); itr != _map->m_allspawns.end(); ++itr)
	{
		CreatureSpawn* cs = *itr;
		if( boss != cs->id )
			continue;

		CreatureProto* proto = CreatureProtoStorage.LookupEntry( cs->entry );
		if( !proto )
			continue;

		CreatureInfo* info = CreatureNameStorage.LookupEntry( cs->entry );
		if(!info)
			continue;

		Creature * obj = CreateCreature( cs->entry );
		if( obj->Load( cs, 0, pMapInfo, proto ) )
		{
			obj->m_noRespawn = true;
			obj->m_castlemode = true;
			obj->m_is_castle_boss = true;
			obj->m_castle = castle;
			obj->PushToWorld( this );
			obj->GetAIInterface()->InitForCastle( false, false );
			boss_guid = GET_LOWGUID_PART(obj->GetGUID());
		}
		else
			delete obj;
	}
}

void MapMgr::DespawnCreatures( const std::set<uint32>& ids )
{
	for( std::set<uint32>::const_iterator it = ids.begin(); it != ids.end(); ++it )
	{
		Creature* p = GetCreature( *it );
		if( p && p->IsValid() )
		{
			p->SafeDelete();
		}
	}
}

void MapMgr::DespawnCreaturesByEntry( uint32 entry )
{
	/*
	for( uint32 i = 0; i < m_CreatureHighGuid; ++i )
	{
		Creature* c = m_CreatureStorage[i];
		if( c && c->GetEntry() == entry )
			c->SafeDelete();
	}
	*/
}

void MapMgr::DespawnGameObjectByEntry( uint32 entry )
{
	/*
	for( uint32 i = 0; i < m_GOHighGuid; ++i )
	{
		GameObject* go = m_GOStorage[i];
		if( go && go->GetInfo()->ID == entry )
			go->Despawn( 0 );
	}
	*/
}

bool MapMgr::Release()
{
	if(pInstance)
	{
		// check for a non-raid instance, these expire after 10 minutes.
		if(GetMapInfo()->type == INSTANCE_NONRAID || pInstance->m_isBattleground)
		{
			pInstance->m_mapMgr = NULL;
			sInstanceMgr._DeleteInstance(pInstance, true);
		}
		else
		{
			// just null out the pointer
			pInstance->m_mapMgr=NULL;
		}
	}
	else if(GetMapInfo()->type == INSTANCE_NULL)
		sInstanceMgr.m_singleMaps[GetMapId()] = NULL;

	// Teleport any left-over players out.
	TeleportPlayers();

	// delete ourselves
	delete this;
	return true;
}
bool MapMgr::Update( uint32 mstime )
{
	//Now update sessions of this map + objects
	_PerformObjectDuties( mstime );
	//////////////////////////////////////////////////////////////////////////
	// Check if we have to die :P
	//////////////////////////////////////////////////////////////////////////
// 	if(InactiveMoveTime && UNIXTIME >= InactiveMoveTime)
// 		return false;

	if( m_sunyouinstance )
		m_sunyouinstance->Update();

	return false;
}

void MapMgr::BeginInstanceExpireCountdown()
{
	MSG_S2C::stRaid_Group_Only Msg;
	PlayerStorageMap::iterator itr;

	// so players getting removed don't overwrite us
	forced_expire = true;

	// send our sexy packet
	Msg.unk1 = uint32(60000);
	Msg.unk2 = uint32(1);
	for(itr = m_PlayerStorage.begin(); itr != m_PlayerStorage.end(); ++itr)
	{
		if(!itr->second->raidgrouponlysent)
			itr->second->GetSession()->SendPacket(Msg);
	}

	// set our expire time to 60 seconds.
	InactiveMoveTime = UNIXTIME + 60;
}

void MapMgr::AddObject(Object *obj)
{
	//m_objectinsertpool.insert(obj);
	obj->PushToWorld(this);
	if(obj->IsPlayer())
	{
		if( m_MaxPlayerCnt < m_PlayerStorage.size() )
		{
			m_MaxPlayerCnt = m_PlayerStorage.size();
		}
	}
}

Unit* MapMgr::GetUnit(const uint64 & guid)
{
/*#ifdef USING_BIG_ENDIAN
	switch (((uint32*)&guid)[0])
#else
	switch (((uint32*)&guid)[1])
#endif
	{
	case	HIGHGUID_PLAYER:
		return GetPlayer((uint32)guid);
		break;
	case	HIGHGUID_UNIT:
		return GetCreature((uint32)guid);
		break;
	case	HIGHGUID_PET:
		return GetPet((uint32)guid);
		break;
	default:
		return NULL;
	}*/

	/*uint32 highguid = GUID_HIPART(guid);
	if( highguid & HIGHGUID_UNIT )
		return GetCreature( ((uint32)guid&LOWGUID_UNIT_MASK) );
	else if( highguid == HIGHGUID_PLAYER )			// players are always zero
		return GetPlayer( (uint32)guid );
	else if( highguid & HIGHGUID_PET )
		return GetPet( (uint32)guid );
	else
		return NULL;*/

	switch(GET_TYPE_FROM_GUID(guid))
	{
	case HIGHGUID_TYPE_UNIT:
		return GetCreature( GET_LOWGUID_PART(guid) );
		break;

	case HIGHGUID_TYPE_PLAYER:
		return GetPlayer( (uint32)guid );
		break;

	case HIGHGUID_TYPE_PET:
		return GetPet( (uint32)guid );
		break;
	}

	return NULL;
}

Player* MapMgr::GetPlayer(uint32 guid)
{
	PlayerStorageMap::iterator itr = m_PlayerStorage.find(guid);
	if(itr != m_PlayerStorage.end())
	{
		if( itr->second->GetSession() )
			return itr->second;
	}
	return NULL;
}

Object* MapMgr::_GetObject(const uint64 & guid)
{
	switch(GET_TYPE_FROM_GUID(guid))
	{
	case	HIGHGUID_TYPE_GAMEOBJECT:
		return GetGameObject(GET_LOWGUID_PART(guid));
		break;
	case	HIGHGUID_TYPE_UNIT:
		return GetCreature(GET_LOWGUID_PART(guid));
		break;
	case	HIGHGUID_TYPE_DYNAMICOBJECT:
		return GetDynamicObject((uint32)guid);
		break;
	case	HIGHGUID_TYPE_TRANSPORTER:
		return objmgr.GetTransporter(GUID_LOPART(guid));
		break;
// 	case	HIGHGUID_TYPE_ITEM:
// 	case	HIGHGUID_TYPE_CONTAINER:
// 		return GetItem(GUID_LOPART(guid));
	default:
		return GetUnit(guid);
		break;
	}
}

void MapMgr::_PerformObjectDuties( uint32 mstime )
{
	++mLoopCounter;
	uint32 difftime = mstime - lastUnitUpdate;
	lastUnitUpdate = mstime;

	// Update any events.
	eventHolder.Update(difftime);

	if( mLoopCounter % 2 )
	{
		difftime = mstime - lastGameobjectUpdate;
		lastGameobjectUpdate = mstime;

		// Update creatures.
		{
			Creature * ptr;
			
			CreatureSet::iterator itr = activeCreatures.begin();
			for(; itr != activeCreatures.end();)
			{
				CreatureSet::iterator itr2 = itr++;
				ptr = *itr2;
				if( ptr->IsValid() )
					ptr->Update(difftime);
				else
					activeCreatures.erase( itr2 );
			}

		}

		{
			PetStorageMap::iterator itr = m_PetStorage.begin();
			Pet * ptr;
			for(; itr != m_PetStorage.end();)
			{
				PetStorageMap::iterator itr2 = itr++;
				ptr = itr2->second;

				if( ptr->IsValid() )
					ptr->Update(difftime);
				else
					m_PetStorage.erase( itr2 );
			}
		}

		// Update players.
		{
			PlayerStorageMap::iterator itr = m_PlayerStorage.begin();
			Player* ptr;
			for(; itr != m_PlayerStorage.end(); )
			{
				PlayerStorageMap::iterator itr2 = itr++;
				ptr = (itr2->second);
				
				if( ptr->IsValid() )
					ptr->Update( difftime );
				else
					m_PlayerStorage.erase( itr2 );
			}
		}

		// Update gameobjects (not on every loop, however)
		{
			GameObjectSet::iterator itr = activeGameObjects.begin();
			GameObject * ptr;
			for(; itr != activeGameObjects.end(); )
			{
				GameObjectSet::iterator itr2 = itr++;
				ptr = *itr2;

				if( ptr->IsValid() )
					ptr->Update( difftime );
				else
					activeGameObjects.erase( itr2 );
			}
		}

		{
			DynamicObjectSet::iterator itr = activeDynamicObjects.begin();
			DynamicObject * ptr;
			for(; itr != activeDynamicObjects.end(); )
			{
				DynamicObjectSet::iterator itr2 = itr++;
				ptr = *itr2;

				if( ptr->IsValid() )
					ptr->Update( difftime );
				else
					activeDynamicObjects.erase( itr2 );
			}
		}
		// Finally, A9 Building/Distribution
		_UpdateObjects();
	}
}

void MapMgr::EventCorpseDespawn(uint64 guid)
{
	Corpse * pCorpse = objmgr.GetCorpse((uint32)guid);
	if(pCorpse == 0)	// Already Deleted
		return;

	if(pCorpse->GetMapMgr() != this)
		return;

	pCorpse->Despawn();
	delete pCorpse;
}

void MapMgr::TeleportPlayers()
{
	PlayerStorageMap::iterator itr =  m_PlayerStorage.begin();
//	if(!bServerShutdown)
//	{
		for(; itr !=  m_PlayerStorage.end();)
		{
			Player *p = itr->second;
			++itr;
			p->EjectFromInstance();
		}
		/*
	}
	else
	{
		for(; itr !=  m_PlayerStorage.end();)
		{
			Player *p = itr->second;
			++itr;
			if(p->GetSession())
				p->GetSession()->Disconnect();
			else
				delete p;
		}
	}
	*/
}

void MapMgr::UnloadCell(uint32 x,uint32 y)
{
	MapCell * c = GetCell(x,y);
	if(c == NULL || c->HasPlayers() || _CellActive(x,y) || !c->IsUnloadPending()) return;

	MyLog::log->notice("Unloading Cell [%d][%d] on map %d (instance %d)...",
		x,y,_mapId,m_instanceID);

	c->Unload();
}

void MapMgr::EventRespawnCreature(Creature * c, MapCell * p)
{
	ObjectSet::iterator itr = p->_respawnObjects.find( ((Object*)c) );
	if(itr != p->_respawnObjects.end())
	{
		c->m_respawnCell=NULL;
		p->_respawnObjects.erase(itr);
		c->OnRespawn(this);
	}
}

void MapMgr::EventRespawnGameObject(GameObject * o, MapCell * c)
{
	ObjectSet::iterator itr = c->_respawnObjects.find( ((Object*)o) );
	if(itr != c->_respawnObjects.end())
	{
		o->m_respawnCell=NULL;
		c->_respawnObjects.erase(itr);
		o->Spawn(this);
	}
}

void MapMgr::SendMessageToCellPlayers(Object * obj, PakHead& packet, uint32 cell_radius /* = 2 */)
{
	uint32 cellX = GetPosX(obj->GetPositionX());
	uint32 cellY = GetPosY(obj->GetPositionY());
	uint32 endX = ((cellX+cell_radius) <= _sizeX) ? cellX + cell_radius : (_sizeX-1);
	uint32 endY = ((cellY+cell_radius) <= _sizeY) ? cellY + cell_radius : (_sizeY-1);
	uint32 startX = (cellX-cell_radius) > 0 ? cellX - cell_radius : 0;
	uint32 startY = (cellY-cell_radius) > 0 ? cellY - cell_radius : 0;

	uint32 posX, posY;
	MapCell *cell;
	MapCell::ObjectSet::iterator iter, iend;
	for (posX = startX; posX <= endX; ++posX )
	{
		for (posY = startY; posY <= endY; ++posY )
		{
			cell = GetCell(posX, posY);
			if (cell && cell->HasPlayers() )
			{
				iter = cell->Begin();
				iend = cell->End();
				for(; iter != iend; ++iter)
				{
					if((*iter)->IsPlayer())
					{
						static_cast< Player* >(*iter)->GetSession()->SendPacket(packet);
					}
				}
			}
		}
	}
}

void MapMgr::SendChatMessageToCellPlayers(Object * obj, MSG_S2C::stChat_Message* Msg, uint32 cell_radius, WorldSession * originator)
{
	uint32 cellX = GetPosX(obj->GetPositionX());
	uint32 cellY = GetPosY(obj->GetPositionY());
	uint32 endX = ((cellX+cell_radius) <= _sizeX) ? cellX + cell_radius : (_sizeX-1);
	uint32 endY = ((cellY+cell_radius) <= _sizeY) ? cellY + cell_radius : (_sizeY-1);
	uint32 startX = (cellX-cell_radius) > 0 ? cellX - cell_radius : 0;
	uint32 startY = (cellY-cell_radius) > 0 ? cellY - cell_radius : 0;

	uint32 posX, posY;
	MapCell *cell;
	MapCell::ObjectSet::iterator iter, iend;
	for (posX = startX; posX <= endX; ++posX )
	{
		for (posY = startY; posY <= endY; ++posY )
		{
			cell = GetCell(posX, posY);
			if (cell && cell->HasPlayers() )
			{
				iter = cell->Begin();
				iend = cell->End();
				for(; iter != iend; ++iter)
				{
					if((*iter)->IsPlayer())
					{
						//static_cast< Player* >(*iter)->GetSession()->SendPacket(packet);
						static_cast< Player* >(*iter)->GetSession()->SendChatPacket(Msg, originator);
					}
				}
			}
		}
	}
}

Creature * MapMgr::GetSqlIdCreature(uint32 sqlid)
{
	CreatureSqlIdMap::iterator itr = _sqlids_creatures.find(sqlid);
	return (itr == _sqlids_creatures.end()) ? NULL : itr->second;
}

GameObject * MapMgr::GetSqlIdGameObject(uint32 sqlid)
{
	GameObjectSqlIdMap::iterator itr = _sqlids_gameobjects.find(sqlid);
	return (itr == _sqlids_gameobjects.end()) ? NULL : itr->second;
}

void MapMgr::HookOnAreaTrigger(Player * plr, uint32 id)
{
	switch (id)
	{
	case 4591:
		//Only opens when the first one steps in, if 669 if you find a way, put it in :P (else was used to increase the time the door stays opened when another one steps on it)
		GameObject *door = GetInterface()->GetGameObjectNearestCoords(803.827f, 6869.38f, -38.5434f, 184212);
		if (door && (door->GetUInt32Value(GAMEOBJECT_STATE) == 1))
		{
			door->SetUInt32Value(GAMEOBJECT_STATE, 0);
			//sEventMgr.AddEvent(door, &GameObject::SetUInt32Value, GAMEOBJECT_STATE, 1, EVENT_SCRIPT_UPDATE_EVENT, 10000, 1, 0);
		}
		//else
		//{
			//sEventMgr.RemoveEvents(door);
			//sEventMgr.AddEvent(door, &GameObject::SetUInt32Value,GAMEOBJECT_STATE, 0, EVENT_SCRIPT_UPDATE_EVENT, 10000, 1, 0);
		//}
		break;
	}
}

void MapMgr::AddSameFactionNearbyCreatures( Creature* c, Unit* addtarget )
{
	if( this->pMapInfo->type == 0 || this->m_sunyouinstance )
		return;

	Object::InRangeSet::iterator it = addtarget->GetInRangeSetBegin();
	for( ; it != addtarget->GetInRangeSetEnd(); ++it )
	{
		Object* obj = *it;
		if( obj->IsCreature() )
		{
			Creature* c2 = static_cast<Creature*>( obj );
			if( c->_getFaction() == 10 && c2->_getFaction() == 10  && c != c2 && c2->GetDistance2dSq( c ) <= 80 )
			{
				if( !c2->GetAIInterface()->GetNextTarget() )
					c2->GetAIInterface()->AttackReaction( addtarget, 1 );
			}
		}
	}
}

void MapMgr::ForceUpdateObjects()
{
	_UpdateObjects();
}

Creature * MapMgr::CreateCreature(uint32 entry)
{
	uint64 newguid = (uint64)HIGHGUID_TYPE_UNIT << 32;
	char * pHighGuid = (char*)&newguid;
	char * pEntry = (char*)&entry;
	pHighGuid[3] |= pEntry[0];
	pHighGuid[4] |= pEntry[1];
	pHighGuid[5] |= pEntry[2];
	pHighGuid[6] |= pEntry[3];

	if(_reusable_guids_creature.size())
	{
		uint32 guid = _reusable_guids_creature.front();
		_reusable_guids_creature.pop_front();

		newguid |= guid;
		return new Creature(newguid);
	}

	if(++m_CreatureHighGuid  >= m_CreatureArraySize)
	{
		// Reallocate array with larger size.
		m_CreatureArraySize += RESERVE_EXPAND_SIZE;
		m_CreatureStorage = (Creature**)realloc(m_CreatureStorage, sizeof(Creature*) * m_CreatureArraySize);
		memset(&m_CreatureStorage[m_CreatureHighGuid],0,(m_CreatureArraySize-m_CreatureHighGuid)*sizeof(Creature*));
	}

	newguid |= m_CreatureHighGuid;
	return new Creature(newguid);
}

GameObject * MapMgr::CreateGameObject(uint32 entry)
{
	if(_reusable_guids_gameobject.size())
	{
		uint32 guid = _reusable_guids_gameobject.front();
		_reusable_guids_gameobject.pop_front();
		return new GameObject((uint64)HIGHGUID_TYPE_GAMEOBJECT<<32 | guid);
	}

	if(++m_GOHighGuid  >= m_GOArraySize)
	{
		// Reallocate array with larger size.
		m_GOArraySize += RESERVE_EXPAND_SIZE;
		m_GOStorage = (GameObject**)realloc(m_GOStorage, sizeof(GameObject*) * m_GOArraySize);
		memset(&m_GOStorage[m_GOHighGuid],0,(m_GOArraySize-m_GOHighGuid)*sizeof(GameObject*));
	}
	return new GameObject((uint64)HIGHGUID_TYPE_GAMEOBJECT<<32 | m_GOHighGuid);
}

DynamicObject * MapMgr::CreateDynamicObject()
{
	return new DynamicObject(HIGHGUID_TYPE_DYNAMICOBJECT,(++m_DynamicObjectHighGuid));
}

