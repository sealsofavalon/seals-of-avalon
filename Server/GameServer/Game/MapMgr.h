#ifndef __MAPMGR_H
#define __MAPMGR_H
class Pet;
class Instance;
class Corpse;
class SunyouInstance;
class SunyouCastle;
class SunyouBattleGround;

namespace MSG_S2C
{
	struct stChat_Message;
}
enum MapMgrTimers
{
	MMUPDATE_OBJECTS = 0,
	MMUPDATE_SESSIONS = 1,
	MMUPDATE_FIELDS = 2,
	MMUPDATE_IDLE_OBJECTS = 3,
	MMUPDATE_ACTIVE_OBJECTS = 4,
	MMUPDATE_COUNT = 5
};

enum ObjectActiveState
{
	OBJECT_STATE_NONE	 = 0,
	OBJECT_STATE_INACTIVE = 1,
	OBJECT_STATE_ACTIVE   = 2,
};

typedef std::set<Object*> ObjectSet;
typedef std::set<Object*> UpdateQueue;
typedef std::set<Player*> PUpdateQueue;
typedef std::set<Player*> PlayerSet;
typedef HM_NAMESPACE::hash_map<uint32, Object*> StorageMap;
typedef set<uint64> CombatProgressMap;
typedef set<Creature*> CreatureSet;
typedef set<GameObject*> GameObjectSet;
typedef set<DynamicObject*> DynamicObjectSet;
typedef HM_NAMESPACE::hash_map<uint32, Creature*> CreatureSqlIdMap;
typedef HM_NAMESPACE::hash_map<uint32, GameObject*> GameObjectSqlIdMap;

#define MAX_TRANSPORTERS_PER_MAP 25

#define RESERVE_EXPAND_SIZE 1024
class MapScriptInterface;
class SERVER_DECL MapMgr : public CellHandler, public EventableObject
{
	friend class UpdateObjectThread;
	friend class ObjectUpdaterThread;
	friend class MapCell;
	friend class MapScriptInterface;
public:

	//This will be done in regular way soon

	ObjectSet m_objectinsertpool;
	void AddObject(Object *);

////////////////////////////////////////////////////////
// Local (mapmgr) storage/generation of GameObjects
/////////////////////////////////////////////
	uint32 m_GOArraySize;
	uint32 m_GOHighGuid;
	GameObject ** m_GOStorage;
	GameObject * CreateGameObject(uint32 entry);

	SUNYOU_INLINE uint32 GenerateGameobjectGuid() { return ++m_GOHighGuid; }
	SUNYOU_INLINE GameObject * GetGameObject(uint32 guid)
	{
		if(guid > m_GOHighGuid)
			return 0;
		return m_GOStorage[guid];
	}

/////////////////////////////////////////////////////////
// Local (mapmgr) storage/generation of Creatures
/////////////////////////////////////////////
	uint32 m_CreatureArraySize;
	uint32 m_CreatureHighGuid;
	Creature ** m_CreatureStorage;
	Creature * CreateCreature(uint32 entry);

	__inline Creature * GetCreature(uint32 guid)
	{
		if(guid > m_CreatureHighGuid)
			return 0;
		return m_CreatureStorage[guid];
	}

//////////////////////////////////////////////////////////
// Local (mapmgr) storage/generation of DynamicObjects
////////////////////////////////////////////
	uint32 m_DynamicObjectHighGuid;
	typedef HM_NAMESPACE::hash_map<uint32, DynamicObject*> DynamicObjectStorageMap;
	DynamicObjectStorageMap m_DynamicObjectStorage;
	DynamicObject * CreateDynamicObject();

	SUNYOU_INLINE DynamicObject * GetDynamicObject(uint32 guid)
	{
		DynamicObjectStorageMap::iterator itr = m_DynamicObjectStorage.find(guid);
		return (itr != m_DynamicObjectStorage.end()) ? itr->second : 0;
	}

//////////////////////////////////////////////////////////
// Local (mapmgr) storage of pets
///////////////////////////////////////////
	typedef HM_NAMESPACE::hash_map<uint32, Pet*> PetStorageMap;
	PetStorageMap m_PetStorage;
	__inline Pet * GetPet(uint32 guid)
	{
		PetStorageMap::iterator itr = m_PetStorage.find(guid);
		return (itr != m_PetStorage.end()) ? itr->second : 0;
	}

//////////////////////////////////////////////////////////
// Local (mapmgr) storage of players for faster lookup
////////////////////////////////

    // double typedef lolz// a compile breaker..
	typedef HM_NAMESPACE::hash_map<uint32, Player*>                     PlayerStorageMap;
	PlayerStorageMap m_PlayerStorage;
	Player * GetPlayer(uint32 guid);
	ui32 m_MaxPlayerCnt;
	ui32 GetPlayerMaxCnt()
	{
		return m_MaxPlayerCnt;
	}
	ui32 GetPlayerCnt()
	{
		return (ui32)m_PlayerStorage.size();
	}

	//////////////////////////////////////////////////////////
	// Local (mapmgr) storage of items for faster lookup
	////////////////////////////////
	// double typedef lolz// a compile breaker..
 	typedef HM_NAMESPACE::hash_map<uint32, Item*>                     ItemStorageMap;
 	ItemStorageMap m_ItemStorage;
 	__inline Item * GetItem(uint32 guid)
 	{
 		ItemStorageMap::iterator itr = m_ItemStorage.find(guid);
 		return (itr != m_ItemStorage.end()) ? itr->second : 0;
 	}

//////////////////////////////////////////////////////////
// Local (mapmgr) storage of combats in progress
////////////////////////////////
	CombatProgressMap _combatProgress;
	void AddCombatInProgress(uint64 guid)
	{
		_combatProgress.insert(guid);
	}
	void RemoveCombatInProgress(uint64 guid)
	{
		_combatProgress.erase(guid);
	}

//////////////////////////////////////////////////////////
// Lookup Wrappers
///////////////////////////////////
	Unit * GetUnit(const uint64 & guid);
	Object * _GetObject(const uint64 & guid);

	ui32 m_startTime;
	bool Init();
	void SpawnActivityCreature( uint32 spawnid, uint32& guid, bool respawn, bool is_creature );
	void DespawnActivityCreature( uint32 guid, bool is_creature );

	void SpawnCreatures( const std::set<uint32>& ids, Unit* threadc, std::set<uint32>& out );
	void SpawnCreatures( uint32 minlv, uint32 maxlv, bool autoSpawn, bool gardenmode, std::set<Player*>& threatadd );
	void SpawnCreatures( SunyouCastle* castle, const std::set<uint32>& ids, std::set<uint32>& out, bool invincible = false );
	void SpawnCreatures( SunyouBattleGround* bg, const std::map<uint32, std::pair<uint32, bool> >& ids, std::set<uint32>& out, uint32 faction );
	void SpawnCastleBoss( SunyouCastle* castle, uint32 boss, uint32& boss_guid );
	void DespawnCreatures( const std::set<uint32>& ids );
	void DespawnCreaturesByEntry( uint32 entry );
	void DespawnGameObjectByEntry( uint32 entry );

	bool Update( uint32 mstime );
	bool Release();

	MapMgr(Map *map, uint32 mapid, uint32 instanceid);
	~MapMgr();

	void PushObject(Object *obj);
	void PushStaticObject(Object * obj);
	void RemoveObject(Object *obj, bool free_guid);
	void ChangeObjectLocation(Object *obj); // update inrange lists
	void ChangeFarsightLocation(Player *plr, DynamicObject *farsight);

	//! Mark object as updated
	void ObjectUpdated(Object *obj);
	void UpdateCellActivity( int x, int y, int radius);

	// Terrain Functions
	SUNYOU_INLINE float  GetLandHeight(float x, float y) { return GetBaseMap()->GetLandHeight(x, y); }
	SUNYOU_INLINE float  GetWaterHeight(float x, float y) { return GetBaseMap()->GetWaterHeight(x, y); }
	SUNYOU_INLINE uint8  GetWaterType(float x, float y) { return GetBaseMap()->GetWaterType(x, y); }
	SUNYOU_INLINE uint8  GetWalkableState(float x, float y) { return GetBaseMap()->GetWalkableState(x, y); }
	SUNYOU_INLINE uint16 GetAreaID(float x, float y) { return GetBaseMap()->GetAreaID(x, y); }

	SUNYOU_INLINE uint32 GetMapId() { return _mapId; }
	SUNYOU_INLINE uint32 IsRunInstance(){ return _mapId == 16 || _mapId == 17; }

	void PushToProcessed(Player* plr);

	SUNYOU_INLINE bool HasPlayers() { return (m_PlayerStorage.size() > 0); }
	SUNYOU_INLINE bool IsCombatInProgress() { return (_combatProgress.size() > 0); }
	void TeleportPlayers();

	SUNYOU_INLINE uint32 GetInstanceID() { return m_instanceID; }
	SUNYOU_INLINE MapInfo *GetMapInfo() { return pMapInfo; }

	bool _shutdown;

	SUNYOU_INLINE MapScriptInterface * GetInterface() { return ScriptInterface; }
	virtual int32 event_GetInstanceID() { return m_instanceID; }

	void LoadAllCells();
	SUNYOU_INLINE size_t GetPlayerCount() { return m_PlayerStorage.size(); }
	uint32 GetTeamPlayersCount(uint32 teamId);

	void _PerformObjectDuties( uint32 mstime );
	uint32 mLoopCounter;
	uint32 lastGameobjectUpdate;
	uint32 lastUnitUpdate;
	void EventCorpseDespawn(uint64 guid);

	time_t InactiveMoveTime;
    uint32 iInstanceMode;

	void UnloadCell(uint32 x,uint32 y);
	void EventRespawnCreature(Creature * c, MapCell * p);
	void EventRespawnGameObject(GameObject * o, MapCell * c);
	void SendMessageToCellPlayers(Object * obj, PakHead& packet, uint32 cell_radius = 2);
	void SendChatMessageToCellPlayers(Object * obj, MSG_S2C::stChat_Message* Msg, uint32 cell_radius, WorldSession * originator);

	Instance * pInstance;
	void BeginInstanceExpireCountdown();
	void HookOnAreaTrigger(Player * plr, uint32 id);

	// better hope to clear any references to us when calling this :P
	void InstanceShutdown()
	{
		pInstance = NULL;
	}
	void AddSameFactionNearbyCreatures( Creature* c, Unit* addtarget );

	void ForceUpdateObjects();

protected:

	//! Collect and send updates to clients
	void _UpdateObjects();

private:
	//! Objects that exist on map

	uint32 _mapId;
	set<Object*> _mapWideStaticObjects;

	bool _CellActive(uint32 x, uint32 y);
	void UpdateInRangeSet(Object *obj, Player *plObj, MapCell* cell, ByteBuffer ** buf);

public:
	// Distance a Player can "see" other objects and receive updates from them (!! ALREADY dist*dist !!)
	float m_UpdateDistance;

private:
	/* Update System */
	FastMutex m_updateMutex;		// use a user-mode mutex for extra speed
	UpdateQueue _updates;
	PUpdateQueue _processQueue;

	/* Sessions */

	typedef set<WorldSession*> SessionSet;
	SessionSet Sessions;

	/* Map Information */
	MapInfo *pMapInfo;
	uint32 m_instanceID;

	MapScriptInterface * ScriptInterface;

public:
#ifdef WIN32
	DWORD threadid;
#endif
	DynamicObjectSet activeDynamicObjects;
	GameObjectSet activeGameObjects;
	CreatureSet activeCreatures;
	EventableObjectHolder eventHolder;
	set<Corpse*> m_corpses;
	CreatureSqlIdMap _sqlids_creatures;
	GameObjectSqlIdMap _sqlids_gameobjects;

	Creature * GetSqlIdCreature(uint32 sqlid);
	GameObject * GetSqlIdGameObject(uint32 sqlid);
	deque<uint32> _reusable_guids_gameobject;
	deque<uint32> _reusable_guids_creature;

	bool forced_expire;
	SunyouInstance* m_sunyouinstance;
	Instance* m_pInstance;
	uint32 m_SunyouInstanceLastUpdate;
};

#endif
