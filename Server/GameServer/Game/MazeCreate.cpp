#include "StdAfx.h"
#include "MazeCreate.h"

MazeCreate::MazeCreate()
{
	for(int i = 0 ; i < Map_max; i++)
	{
		m_MazeMap[i] = NULL ;
	}
	Reset();
}
MazeCreate::~MazeCreate()
{
	for(int i = 0 ; i < Map_max; i++)
	{
		if (m_MazeMap[i])
		{
			delete m_MazeMap[i] ;
			m_MazeMap[i] = NULL ;
		}
	}
}

void MazeCreate::Reset()
{
	m_CreateIndexCount = 0 ;
	m_CreateZACount = 0;
	m_CreateMazeDate.Reset();
}
bool MazeCreate::InitMazeMap()
{
	MyLog::log->notice("maze generation started");
	MyLog::log->notice("......");
	for (int i = 0; i < Map_max; i++)
	{
		
		CreateMazeMap( i);
	}
	MyLog::log->notice("maze generation finished");
	return true ;
}
bool MazeCreate::CreateMazeMap(uint32 Index)
{
	if (Index >= 0 && Index < Map_max)
	{
		Reset();
		while(!GetRoad())
		{
			Reset();
			GetRoad();
		}

		MazeDate* pkDate = new MazeDate ;
		*pkDate = m_CreateMazeDate;

		m_MazeMap[Index] = pkDate ;
		return true ;
	}
	return false ;
}
bool MazeCreate::GetMaze(std::map<int, int>& sMap, int& end)
{
	srand((uint32)UNIXTIME);
	uint32 index = rand() %  Map_max;

	static MazeDate TemDate ;
	TemDate = *m_MazeMap[index];

	int curZA = TemDate.GetZACount();


	static vector<uint32> v_t;
	v_t.clear();
	v_t.reserve(51);
	bool bAddBaoxiang = false;
	if (curZA > Min_ZA)
	{
		int curBaox = curZA - Min_ZA;
		for (int it = 0 ; it < curZA; it++)
		{
			if (it < curBaox)
			{
				v_t.push_back(1);
			}else
			{
				v_t.push_back(0);
			}
		}
		srand((uint32)UNIXTIME);
		random_shuffle(v_t.begin(), v_t.end());

		bAddBaoxiang = true ;
	}
	
	if (bAddBaoxiang)
	{
		int ZA = 0 ;
		for (int i = 0 ; i < R_max; i ++)
		{
			for (int j = 0; j < C_max; j++)
			{
				if (TemDate.Date[i][j] == 2)
				{
					if (ZA < curZA && v_t[ZA] == 1)
					{
						TemDate.Date[i][j] = 3;
					}

					ZA++ ;

				}
			}
		}


		for (int i = 0 ; i < R_max; i ++)
		{
			for (int j = 0; j < C_max; j++)
			{
				if (TemDate.Date[i][j] == 3)
				{
					if (i > 0 && !TemDate.Date[i - 1][j])
					{
						TemDate.Date[i - 1][j] = 2;
					}

					if (j > 0 && !TemDate.Date[i][j - 1])
					{
						TemDate.Date[i][j - 1] = 2;
					}

					if (i < R_max - 1 && !TemDate.Date[i + 1][j])
					{
						TemDate.Date[i + 1][j] = 2;
					}
					if (j < C_max - 1 && !TemDate.Date[i][j + 1])
					{
						TemDate.Date[i][j + 1] = 2;
					}
				}
			}
		}
	}
	


	for (int i = 0 ; i < R_max; i ++)
	{
		for (int j = 0; j < C_max; j++)
		{
			sMap[i * C_max + j] = TemDate.Date[i][j] ;
		}
	}
	end = TemDate.Endindex;


	return true ;
}
bool MazeCreate::GetRoad()
{
	if (!LockNext(0, Start_C))
	{
		return false ;
	}
	return ( m_CreateZACount >= Min_ZA);
}

bool MazeCreate::LockNext(int line, int last)
{
	m_CreateMazeDate.Date[line][last] = 1 ;

	if (line < R_max - 1)
	{
		static uint32* tem[4];
		tem[0] = NULL; // bottom
		tem[1] = NULL; // left
		tem[2] = NULL; // top
		tem[3] = NULL; // right

		if (line > 0 && !m_CreateMazeDate.Date[line - 1][last])
		{
			tem[0] = &m_CreateMazeDate.Date[line - 1][last];
		}

		if (last > 0 && !m_CreateMazeDate.Date[line][last - 1])
		{
			tem[1] = &m_CreateMazeDate.Date[line][last - 1];
		}

		if (line < R_max - 1 && !m_CreateMazeDate.Date[line + 1][last])
		{
			tem[2] = &m_CreateMazeDate.Date[line + 1][last];
		}
		if (last < C_max - 1 && !m_CreateMazeDate.Date[line][last + 1])
		{
			tem[3] = &m_CreateMazeDate.Date[line][last + 1];
		}

		int useCount = 0;
		for (int i = 0; i <4 ; i++)
		{
			if (tem[i])
			{
				useCount ++ ;
				if (!*tem[i] )
				{
					if (*tem[i] != 2 )
					{
						m_CreateZACount += 1;
					}
					*tem[i] = 2;
				}
			}
		}
		if (useCount == 0 )
		{
			Reset();
			return false ;
		}

		//static LARGE_INTEGER freq;
		//QueryPerformanceCounter(&freq);
		//srand(freq.QuadPart);

		srand(get_tick_count());
		int index = rand() % useCount;

		int temc = 0;
		for (int i = 0 ; i < 4; i++)
		{
			if (tem[i])
			{
				if (index == temc)
				{
					m_CreateZACount -= 1;
					switch (i)
					{
					case 0 :
						return LockNext(line - 1, last);
					case 1:
						return LockNext(line, last - 1);
					case 2:
						return LockNext(line + 1, last);
					case 3:
						return LockNext(line, last + 1);

					}
				}
				temc ++ ;
			}
		}

		Reset();
		return false ;
	}else
	{
		m_CreateMazeDate.Endindex = line * C_max + last ;
		return true;
	}
}