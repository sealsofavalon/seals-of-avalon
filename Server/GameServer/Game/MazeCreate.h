#ifndef __MAZECREATE_h__
#define __MAZECREATE_h__


static const int R_max = 15;
static const int C_max = 7;
static const int Map_max = 500;
static const int Start_C = 4;
static const int Min_ZA = 29;

struct MazeDate
{
	uint32 Date[R_max][C_max];
	uint32 Endindex ;
	void Reset()
	{
		for (int i = 0; i < R_max; i++)
		{
			for(int j = 0; j < C_max; j++)
			{
				Date[i][j] = 0;
			}
		}

		Endindex = 0;
	}

	int GetZACount()
	{
		uint32 count = 0;
		for (int i = 0; i < R_max; i++)
		{
			for(int j = 0; j < C_max; j++)
			{
				if (Date[i][j] == 2)
				{
					count ++ ;
				}
			}
		}

		return count ;
	}
	MazeDate & operator=(const MazeDate & item)
	{
		if (this == &item)
		{
			return *this;
		}

		for (int i = 0; i < R_max; i++)
		{
			for(int j = 0; j < C_max; j++)
			{
				Date[i][j] = item.Date[i][j];
			}
		}

		Endindex = item.Endindex;
		return *this;
	}
};
class MazeCreate
{
public:
	MazeCreate();
	~MazeCreate();

	bool InitMazeMap();
	
	bool CreateMazeMap(uint32 Index);
	bool GetRoad();
	bool GetMaze(std::map<int, int>& sMap, int& end);

	void Reset();
	bool LockNext(int line, int last);
private:
	MazeDate* m_MazeMap[Map_max];
	MazeDate m_CreateMazeDate;
	int m_CreateIndexCount ;  //生成的步骤要求 
	int m_CreateZACount; //生成的障碍数目
};
#endif