#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Loot.h"
#include "../../SDBase/Protocol/C2S_Loot.h"
#include "../../SDBase/Protocol/C2S_Item.h"
#include "../../SDBase/Protocol/S2C_Move.h"
#include "../../SDBase/Protocol/C2S_Move.h"
#include "../../SDBase/Protocol/S2C_Spell.h"
#include "../../SDBase/Protocol/S2C_AI.h"
#include "../Common/Protocol/GS2GT.h"
#include "SunyouInstance.h"
#include "WeatherMgr.h"
#include "QuestMgr.h"

void WorldSession::HandleShowShizhuang(CPacketUsn& packet)
{
	MSG_C2S::stItem_Show_Shizhuang MsgRecv;packet >> MsgRecv;
	_player->SetUInt32Value(PLAYER_VISIBLE_ITEM_SHOW_SHIZHUANG, MsgRecv.bShow);
}

void WorldSession::HandleInstanceDeadChoice(CPacketUsn& packet)
{
	MSG_C2S::stChoiceInstanceDeadExit MsgRecv;packet >> MsgRecv;
	if(MsgRecv.bExit)
	{
		if( _player->m_sunyou_instance )
		{
			SunyouInstance* p = _player->m_sunyou_instance;
			_player->m_resurrect_lock = false;
			_player->m_escape_mode = true;
			if(_player->isAlive())
				_player->EjectFromInstance();
			else
				_player->ResurrectPlayer();

			_player->m_escape_mode = false;

			if( !p->GetPlayerCount() )
				p->Expire();
		}
	}
	else
	{
		GetPlayer()->m_escape_mode = false;
		if(!_player->isAlive())
		{
			MSG_S2C::stAI_Died MsgDied;
			MsgDied.died_guid = GetPlayer()->GetGUID();
			GetPlayer()->SendMessageToSet(MsgDied, true);
			GetPlayer()->ResurrectPlayer();
		}		
	}
}

void WorldSession::HandleRepopRequestOpcode( CPacketUsn& packet )
{
	/*
	MyLog::log->debug( "WORLD: Recved CMSG_REPOP_REQUEST Message" );
	if(_player->m_CurrentTransporter)
		_player->m_CurrentTransporter->RemovePlayer(_player);

	  GetPlayer()->RepopRequestedPlayer();
  */
}

void WorldSession::HandleAutostoreLootItemOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
//	uint8 slot = 0;
	uint32 itemid = 0;
	uint32 amt = 1;
	uint8 lootSlot = 0;
	uint8 error = 0;
	SlotResult slotresult;

	Item *add;
	Loot *pLoot = NULL;

	if(_player->isCasting())
		_player->InterruptSpell();
	GameObject * pGO = NULL;
	Creature * pCreature = NULL;

	uint32 guidtype = GET_TYPE_FROM_GUID(_player->GetLootGUID());
	if(guidtype == HIGHGUID_TYPE_UNIT)
	{
		pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(GetPlayer()->GetLootGUID()));
		if (!pCreature)return;
		pLoot=&pCreature->loot;
	}
	else if(guidtype == HIGHGUID_TYPE_GAMEOBJECT)
	{
		pGO = _player->GetMapMgr()->GetGameObject(GET_LOWGUID_PART(GetPlayer()->GetLootGUID()));
		if(!pGO)return;
		pLoot=&pGO->loot;
	}else if( guidtype == HIGHGUID_TYPE_ITEM )
	{
		Item *pItem = _player->GetItemInterface()->GetItemByGUID(_player->GetLootGUID());
		if(!pItem)
			return;
		pLoot = pItem->loot;
	}else if( guidtype == HIGHGUID_TYPE_PLAYER )
	{
		Player * pl = _player->GetMapMgr()->GetPlayer((uint32)GetPlayer()->GetLootGUID());
		if(!pl) return;
		pLoot = &pl->loot;
	}

	if(!pLoot) return;

	MSG_C2S::stMSG_AutoStore_Loot_Item MsgRecv;packet>>MsgRecv;
	lootSlot = MsgRecv.lootSlot;
	if (lootSlot >= pLoot->items.size())
	{
		MyLog::log->debug("AutoLootItem: Player %s might be using a hack! (slot %d, size %d)",
						GetPlayer()->GetName(), lootSlot, pLoot->items.size());
		return;
	}

	amt = pLoot->items.at(lootSlot).iItemsCount;
	if( pLoot->items.at(lootSlot).roll != NULL )
	{
		//MyLog::log->debug("not roll loot item for test");
		return;
	}

	if (!pLoot->items.at(lootSlot).ffa_loot)
	{
		if (!amt)//Test for party loot
		{  
			GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL, NULL,INV_ERR_ALREADY_LOOTED);
			return;
		}
	}
	else
	{
		//make sure this player can still loot it in case of ffa_loot
		LooterSet::iterator itr = pLoot->items.at(lootSlot).has_looted.find(_player->GetLowGUID());

		if (pLoot->items.at(lootSlot).has_looted.end() != itr)
		{
			GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL, NULL,INV_ERR_ALREADY_LOOTED);
			return;
		}
	}

	itemid = pLoot->items.at(lootSlot).item.itemproto->ItemId;
	ItemPrototype* it = pLoot->items.at(lootSlot).item.itemproto;

	if((error = _player->GetItemInterface()->CanReceiveItem(it, 1)))
	{
		_player->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, error);
		return;
	}

	if(pGO)
		CALL_GO_SCRIPT_EVENT(pGO, OnLootTaken)(_player, it);
	else if(pCreature)
		CALL_SCRIPT_EVENT(pCreature, OnLootTaken)(_player, it);

	add = GetPlayer()->GetItemInterface()->FindItemLessMax(itemid, amt, false);
	sHookInterface.OnLoot(_player, pCreature, 0, itemid);
	if (!add)
	{
		slotresult = GetPlayer()->GetItemInterface()->FindFreeInventorySlot(it);
		if(!slotresult.Result)
		{
			GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_INVENTORY_FULL);
			return;
		}
	
		//MyLog::log->debug("AutoLootItem MISC");
		Item *item = objmgr.CreateItem( itemid, GetPlayer());
	   
		item->SetUInt32Value(ITEM_FIELD_STACK_COUNT,amt);

		if( item->GetProto()->SubClass < ITEM_SUBCLASS_ARMOR_GUAIWUBEIBU )
		{
			if (pLoot->items.at(lootSlot).count)
			{
				item->SetVariableProperties(pLoot->items.at(lootSlot).id, pLoot->items.at(lootSlot).count);
				item->ApplyRandomProperties(false);
			}

			if(pLoot->items.at(lootSlot).iRandomProperty!=NULL)
			{
				item->SetRandomProperty(pLoot->items.at(lootSlot).iRandomProperty->ID);
				item->ApplyRandomProperties(false);
			}
			else if(pLoot->items.at(lootSlot).iRandomSuffix != NULL)
			{
				item->SetRandomSuffix(pLoot->items.at(lootSlot).iRandomSuffix->id);
				item->ApplyRandomProperties(false);
			}
		}

		if( GetPlayer()->GetItemInterface()->SafeAddItem(item,slotresult.ContainerSlot, slotresult.Slot) )
		{
			_player->GetSession()->SendItemPushResult(item,false,true,true,true,slotresult.ContainerSlot,slotresult.Slot, amt);
			MyLog::yunyinglog->info("item-loot-player[%u][%s] loot item["I64FMT"][%u]", GetPlayer()->GetLowGUID(), GetPlayer()->GetName(),
				item->GetGUID(), item->GetProto()->ItemId);
		}
		else
			delete item;
	}
	else 
	{	
		add->SetCount(add->GetUInt32Value(ITEM_FIELD_STACK_COUNT) + amt);
		add->m_isDirty = true;

		sQuestMgr.OnPlayerItemPickup(GetPlayer(),add);
		_player->GetSession()->SendItemPushResult(add, false, false, true, false, _player->GetItemInterface()->GetBagSlotByGuid(add->GetGUID()), 0xFFFFFFFF,amt);
	}

	//in case of ffa_loot update only the player who recives it.
	if (!pLoot->items.at(lootSlot).ffa_loot)
	{
		pLoot->items.at(lootSlot).iItemsCount = 0;

		// this gets sent to all looters
		MSG_S2C::stMsg_Loot_Removed Msg;
		Msg.lootSlot = lootSlot;
		Player * plr;
		for(LooterSet::iterator itr = pLoot->looters.begin(); itr != pLoot->looters.end(); ++itr)
		{
			if((plr = _player->GetMapMgr()->GetPlayer(*itr)))
				plr->GetSession()->SendPacket(Msg);
		}
	}
	else
	{
		pLoot->items.at(lootSlot).has_looted.insert(_player->GetLowGUID());
		MSG_S2C::stMsg_Loot_Removed Msg;
		Msg.lootSlot = lootSlot;
		_player->GetSession()->SendPacket(Msg);
	}

/*    WorldPacket idata(45);
    if(it->Class == ITEM_CLASS_QUEST)
    {
        uint32 pcount = _player->GetItemInterface()->GetItemCount(it->ItemId, true);
		BuildItemPushResult(&idata, _player->GetGUID(), ITEM_PUSH_TYPE_LOOT, amt, itemid, pLoot->items.at(lootSlot).iRandomProperty ? pLoot->items.at(lootSlot).iRandomProperty->ID : 0,0xFF,0,0xFFFFFFFF,pcount);
    }
	else BuildItemPushResult(&idata, _player->GetGUID(), ITEM_PUSH_TYPE_LOOT, amt, itemid, pLoot->items.at(lootSlot).iRandomProperty ? pLoot->items.at(lootSlot).iRandomProperty->ID : 0);

	if(_player->InGroup())
		_player->GetGroup()->SendPacketToAll(&idata);
	else
		SendPacket(&idata);*/

	/* any left yet? (for fishing bobbers) */
	if(pGO && pGO->GetEntry() ==GO_FISHING_BOBBER)
	{
		int count=0;
		for(vector<__LootItem>::iterator itr = pLoot->items.begin(); itr != pLoot->items.end(); ++itr)
			count += (*itr).iItemsCount;
		if(!count)
			pGO->ExpireAndDelete();
	}
}

void WorldSession::HandleLootMoneyOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	Loot * pLoot = NULL;
	uint64 lootguid=GetPlayer()->GetLootGUID();
	if(!lootguid)
		return;   // duno why this happens

	if(_player->isCasting())
		_player->InterruptSpell();

	Unit * pt = 0;
	uint32 guidtype = GET_TYPE_FROM_GUID(lootguid);

	if(guidtype == HIGHGUID_TYPE_UNIT)
	{
		Creature* pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(lootguid));
		if(!pCreature)return;
		pLoot=&pCreature->loot;
		pt = pCreature;
		bool bClear = false;

		if(bClear)
		{
			pCreature->m_corpseEvent = true;
			pCreature->BuildFieldUpdatePacket( _player, UNIT_DYNAMIC_FLAGS, 0 );

			if( !pCreature->Skinned )
			{
				if( lootmgr.IsSkinnable( pCreature->GetEntry() ) )
				{
					pCreature->BuildFieldUpdatePacket( _player, UNIT_FIELD_FLAGS, UNIT_FLAG_SKINNABLE );
				}
			}
		}
	}
	else if(guidtype == HIGHGUID_TYPE_GAMEOBJECT)
	{
		GameObject* pGO = _player->GetMapMgr()->GetGameObject(GET_LOWGUID_PART(lootguid));
		if(!pGO)return;
		pLoot=&pGO->loot;
	}
	else if(guidtype == HIGHGUID_TYPE_CORPSE)
	{
		Corpse *pCorpse = objmgr.GetCorpse((uint32)lootguid);
		if(!pCorpse)return;
		pLoot=&pCorpse->loot;
	}
	else if(guidtype == HIGHGUID_TYPE_PLAYER)
	{
		Player * pPlayer = _player->GetMapMgr()->GetPlayer((uint32)lootguid);
		if(!pPlayer) return;
		pLoot = &pPlayer->loot;
		pPlayer->bShouldHaveLootableOnCorpse = false;
		pt = pPlayer;
	}
	else if( guidtype == HIGHGUID_TYPE_ITEM )
	{
		Item *pItem = _player->GetItemInterface()->GetItemByGUID(lootguid);
		if(!pItem)
			return;
		pLoot = pItem->loot;
	}

	if (!pLoot)
	{
		//bitch about cheating maybe?
		return;
	}

	uint32 money = pLoot->gold;

	MSG_S2C::stLoot_Clear_Money MsgClearMoney;
	// send to all looters
	Player * plr;
	for(LooterSet::iterator itr = pLoot->looters.begin(); itr != pLoot->looters.end(); ++itr)
	{
		if((plr = _player->GetMapMgr()->GetPlayer(*itr)))
			plr->GetSession()->SendPacket(MsgClearMoney);
	}

	if(!_player->InGroup())
	{
		if(money)
		{
			switch(_player->GetSession()->m_ChenmiStat)
			{
			case 1:
				money /= 2;break;
			case 2:
				money = 0;
				pLoot->gold = 0;
				return;
			}
			MSG_S2C::stLoot_Money_Notify Msg;
			Msg.money = money;
			SendPacket(Msg);
			GetPlayer()->ModCoin(money);
			MyLog::yunyinglog->info("gold-loot-player[%u][%s] take gold[%u]", GetPlayer()->GetLowGUID(), GetPlayer()->GetName(), money);
			sHookInterface.OnLoot(_player, pt, pLoot->gold, 0);
			pLoot->gold = 0;
		}
	}
	else
	{
		//this code is wrong mustbe party not raid!
		Group* party = _player->GetGroup();
		if(party)
		{
			/*uint32 share = money/party->MemberCount();*/
			vector<Player*> targets;
			targets.reserve(party->MemberCount());

			GroupMembersSet::iterator itr;
			SubGroup * sgrp;
			for(uint32 i = 0; i < party->GetSubGroupCount(); i++)
			{
				sgrp = party->GetSubGroup(i);
				for(itr = sgrp->GetGroupMembersBegin(); itr != sgrp->GetGroupMembersEnd(); ++itr)
				{
					if((*itr)->m_loggedInPlayer && (*itr)->m_loggedInPlayer->GetZoneId() == _player->GetZoneId() && _player->GetInstanceID() == (*itr)->m_loggedInPlayer->GetInstanceID()
						&& _player->GetDistance2dSq((*itr)->m_loggedInPlayer) < 100 )
						targets.push_back((*itr)->m_loggedInPlayer);
				}
			}

			if(!targets.size())
				return;


			MSG_S2C::stLoot_Money_Notify Msg;
			uint32 share = money / uint32(targets.size());

			if(share == 0)
			{
				switch(_player->GetSession()->m_ChenmiStat)
				{
				case 1:
					money /= 2;break;
				case 2:
					money = 0;return;
				}
				Msg.money = money;
				_player->ModCoin(money);
				MyLog::yunyinglog->info("gold-loot-player[%u][%s] take gold[%u]", GetPlayer()->GetLowGUID(), GetPlayer()->GetName(), money);
				_player->GetSession()->SendPacket( Msg );
				pLoot->gold = 0;
			}
			else
			{
				Msg.money = share;

				for(vector<Player*>::iterator itr = targets.begin(); itr != targets.end(); ++itr)
				{
					ui32 money2 = share;
					switch((*itr)->GetSession()->m_ChenmiStat)
					{
					case 1:
						money2 /= 2;break;
					case 2:
						money2 = 0;continue;
					}
					Msg.money = money2;

					money -= money2;
					(*itr)->ModCoin(money2);
					MyLog::yunyinglog->info("gold-loot-player[%u][%s] take gold[%u]", GetPlayer()->GetLowGUID(), GetPlayer()->GetName(), money2);
					(*itr)->GetSession()->SendPacket( Msg );
					pLoot->gold -= share;
				}

				if(money)
				{
					switch(_player->GetSession()->m_ChenmiStat)
					{
					case 1:
						money /= 2;break;
					case 2:
						money = 0;return;
					}
					Msg.money = money;
					_player->ModCoin(money);
					MyLog::yunyinglog->info("gold-loot-player[%u][%s] take gold[%u]", GetPlayer()->GetLowGUID(), GetPlayer()->GetName(), money);
					_player->GetSession()->SendPacket( Msg );
					pLoot->gold = 0;
				}
			}

		}
	}

}

void WorldSession::HandleLootOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld())
		return;

	MSG_C2S::stMsg_Loot MsgRecv;packet >> MsgRecv;
	uint64 guid = MsgRecv.guid;;

	if(_player->isCasting())
		_player->InterruptSpell();

	uint32 type = GET_TYPE_FROM_GUID( guid );
	if( type == HIGHGUID_TYPE_ITEM || type == HIGHGUID_TYPE_CONTAINER )
	{
		MapMgr* mgr = _player->GetMapMgr();
		Item* it = mgr->GetItem( guid );
		if( it )
		{
			uint8 error = _player->GetItemInterface()->CanReceiveItem( it->GetProto(), it->GetUInt32Value( ITEM_FIELD_STACK_COUNT ) );
			if( error )
			{
				_player->GetItemInterface()->BuildInventoryChangeError(0, 0, error);
				return;
			}

			if( !_player->GiveItemAndRemoveFromGround( it ) )
				_player->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_BAG_FULL);
		}
		return;
	}

	if(_player->InGroup())
	{
		Group * party = _player->GetGroup();
		if(party)
		{
			if(party->GetMethod() == PARTY_LOOT_MASTER)
			{				
				MSG_S2C::stLoot_Master_List Msg;  // wont be any larger
				Msg.member_count =(uint8)party->MemberCount();
				uint32 real_count = 0;
				SubGroup *s;
				GroupMembersSet::iterator itr;
				for(uint32 i = 0; i < party->GetSubGroupCount(); ++i)
				{
					s = party->GetSubGroup(i);
					for(itr = s->GetGroupMembersBegin(); itr != s->GetGroupMembersEnd(); ++itr)
					{
						if((*itr)->m_loggedInPlayer && _player->GetZoneId() == (*itr)->m_loggedInPlayer->GetZoneId())
						{
							Msg.vMember.push_back((*itr)->m_loggedInPlayer->GetGUID());
							++real_count;
						}
					}
				}

				party->SendPacketToAll(Msg);
			}
/*			//this commented code is not used because it was never tested and finished !
			else if(party->GetMethod() == PARTY_LOOT_RR)
			{
				Creature *target=GetPlayer()->GetMapMgr()->GetCreature(guid); //maybe we should extend this to other object types too
				if(target)
				{
					if(target->TaggerGuid==GetPlayer()->GetGUID())
						GetPlayer()->SendLoot(guid,LOOT_CORPSE);
					else return;
				}
			}*/
		}	
	}
	GetPlayer()->SendLoot(guid,LOOT_CORPSE);
}


void WorldSession::HandleLootReleaseOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stLoot_Release MsgRecv;packet >> MsgRecv;
	uint64 guid = MsgRecv.guid;;

	MSG_S2C::stLoot_Release_Response Msg;
	Msg.guid = guid;
	SendPacket( Msg );

	_player->SetLootGUID(0);
	_player->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOOTING);
	_player->m_currentLoot = 0;

	if( GET_TYPE_FROM_GUID( guid ) == HIGHGUID_TYPE_UNIT )
	{
		Creature* pCreature = _player->GetMapMgr()->GetCreature( GET_LOWGUID_PART(guid) );
		if( pCreature == NULL )
			return;
		// remove from looter set
		pCreature->loot.looters.erase(_player->GetLowGUID());

		if( pCreature->loot.gold <= 0)
		{			
			bool bHasItem = false;
			for( std::vector<__LootItem>::iterator i = pCreature->loot.items.begin(); i != pCreature->loot.items.end(); i++ )
			if( i->iItemsCount > 0 && !bHasItem)
			{
				ItemPrototype* proto = i->item.itemproto;
// 				if( proto->Class != 12 )
// 					return;
				if( i->item.itemproto->Bonding == ITEM_BIND_QUEST_DROP && i->item.itemproto->QuestId )
				{
					if( _player->HasQuestForItem( i->item.itemproto->ItemId ) )
						bHasItem = true;
				}
				else
					bHasItem = true;
			}
			
			if(!bHasItem)
			{
				pCreature->m_corpseEvent = true;
				//pCreature->BuildFieldUpdatePacket( _player, UNIT_DYNAMIC_FLAGS, 0 );
				pCreature->SetUInt32Value( UNIT_DYNAMIC_FLAGS, 0 );
				//SystemMessage("清除特效");
			}
			//else
				//SystemMessage("不清除特效");


			if( !pCreature->Skinned )
			{
				if( lootmgr.IsSkinnable( pCreature->GetEntry() ) )
				{
					pCreature->BuildFieldUpdatePacket( _player, UNIT_FIELD_FLAGS, UNIT_FLAG_SKINNABLE );
				}
			}
		}
	}
	else if( GET_TYPE_FROM_GUID( guid ) == HIGHGUID_TYPE_GAMEOBJECT )
	{	   
		GameObject* pGO = _player->GetMapMgr()->GetGameObject( (uint32)guid );
		if( pGO == NULL )
			return;
		pGO->m_release_loot = true;

//         switch( pGO->GetUInt32Value( GAMEOBJECT_TYPE_ID ) )
//         {
//         case GAMEOBJECT_TYPE_FISHINGNODE:
//             {
// 		        pGO->loot.looters.erase(_player->GetLowGUID());
//                 if(pGO->IsInWorld())
// 			    {
// 				    pGO->RemoveFromWorld(true);
// 			    }
// 			    delete pGO;
//             }break;
//         case GAMEOBJECT_TYPE_CHEST:
//             {
//                 pGO->loot.looters.erase( _player->GetLowGUID() );
//                 //check for locktypes
//                 
//                 Lock* pLock = dbcLock.LookupEntry( pGO->GetInfo()->SpellFocus );
//                 if( pLock )
//                 {
//                     for( uint32 i=0; i < 5; i++ )
//                     {
//                         if( pLock->locktype[i] )
//                         {
//                             if( pLock->locktype[i] == 1 ) //Item or Quest Required;
// 							{
// 								pGO->Despawn((sQuestMgr.GetGameObjectLootQuest(pGO->GetEntry()) ? 60000 : 12000 + ( RandomUInt( 60000 ) ) ) );
// 								return;
// 							}
// 							else if( pLock->locktype[i] == 2 ) //locktype;
//                             {
//                                 //herbalism and mining;
//                                 if( pLock->lockmisc[i] == LOCKTYPE_MINING || pLock->lockmisc[i] == LOCKTYPE_HERBALISM )
//                                 {
//                                     //we still have loot inside.
//                                     if( pGO->HasLoot() )
//                                     {
//                                         pGO->SetUInt32Value( GAMEOBJECT_STATE, 1 );
// 										// TODO : redo this temporary fix, because for some reason hasloot is true even when we loot everything
// 										// my guess is we need to set up some even that rechecks the GO in 10 seconds or something
// 										//pGO->Despawn( 600000 + ( RandomUInt( 300000 ) ) );
//                                         return;
//                                     }
// 
//                                     if( pGO->CanMine() )
//                                     {
// 										pGO->loot.items.clear();
//                                         pGO->UseMine();
// 										return;
//                                     }
//                                     else
//                                     {
//     									pGO->CalcMineRemaining( true );
// 										pGO->Despawn( 600000 + ( RandomUInt( 180000 ) ) );
// 										return;
//                                     }
//                                 }
// 								else
// 								{
// 									if(pGO->HasLoot())
// 									{
// 										pGO->SetUInt32Value(GAMEOBJECT_STATE, 1);
// 										return;
// 									}
// 									pGO->Despawn((sQuestMgr.GetGameObjectLootQuest(pGO->GetEntry()) ? 60000 : 12000 + ( RandomUInt( 60000 ) ) ) );
// 									return;
// 								}
// 							}
// 							else //other type of locks that i dont bother to split atm ;P
// 							{
// 								if(pGO->HasLoot())
// 								{
// 									pGO->SetUInt32Value(GAMEOBJECT_STATE, 1);
// 									return;
// 								}
// 								pGO->Despawn((sQuestMgr.GetGameObjectLootQuest(pGO->GetEntry()) ? 60000 : 12000 + ( RandomUInt( 60000 ) ) ) );
// 								return;
// 							}
//                         }
//                     }
// 				}
//                 else
//                 {
                    if( pGO->HasLoot() )
                    {
                        //pGO->SetUInt32Value(GAMEOBJECT_STATE, 1);
                        return;
                    }
                    //pGO->Despawn((sQuestMgr.GetGameObjectLootQuest(pGO->GetEntry()) ? 60000 : 12000 + ( RandomUInt( 60000 ) ) ) );
					pGO->Despawn( pGO->pInfo->spawntime );
//                 }
//             }
//         default: break;
//         }
	}
	else if(GET_TYPE_FROM_GUID(guid) == HIGHGUID_TYPE_CORPSE)
	{
		Corpse *pCorpse = objmgr.GetCorpse((uint32)guid);
		if(pCorpse) 
			pCorpse->SetUInt32Value(CORPSE_FIELD_DYNAMIC_FLAGS, 0);
	}
	else if(GET_TYPE_FROM_GUID(guid) == HIGHGUID_TYPE_PLAYER)
	{
		Player *plr = objmgr.GetPlayer((uint32)guid);
		if(plr)
		{
			plr->bShouldHaveLootableOnCorpse = false;
			plr->loot.items.clear();
			plr->RemoveFlag(UNIT_DYNAMIC_FLAGS, U_DYN_FLAG_LOOTABLE);
		}
	}
	else if(GUID_HIPART(guid))
	{
		// suicide!
		_player->GetItemInterface()->SafeFullRemoveItemByGuid(guid);
	}
}

void WorldSession::HandleWhoOpcode( CPacketUsn& packet )
{
// 	MSG_C2S::stMsg_Who MsgRecv;packet>>MsgRecv;
// 	uint32 min_level = MsgRecv.min_level;
// 	uint32 max_level = MsgRecv.max_level;
// 	string chatname = MsgRecv.chatname;
// 	uint32 class_mask = MsgRecv.class_mask;
// 	uint32 race_mask = MsgRecv.race_mask;
// 	uint32 zone_count = MsgRecv.zone_count;
// 	uint32 * zones = 0;
// 	uint32 name_count;
// 	string * names = 0;
// 	string unkstr;
// 	bool cname;
// 	uint32 i;
// 
// 	if(zone_count > 0 && zone_count < 10)
// 	{
// 		zones = new uint32[zone_count];
// 	
// 		for(i = 0; i < zone_count; ++i)
// 			recv_data >> zones[i];
// 	}
// 	else
// 	{
// 		zone_count = 0;
// 	}
// 
// 	recv_data >> name_count;
// 	if(name_count > 0 && name_count < 10)
// 	{
// 		names = new string[name_count];
// 
// 		for(i = 0; i < name_count; ++i)
// 			recv_data >> names[i];
// 	}
// 	else
// 	{
// 		name_count = 0;
// 	}
// 
// 	if(chatname.length() > 0)
// 		cname = true;
// 	else
// 		cname = false;
// 
// 	MyLog::log->debug( "WORLD: Recvd CMSG_WHO Message with %u zones and %u names", zone_count, name_count );
// 	
// 	bool gm = false;
// 	uint32 team = _player->GetTeam();
// 	if(HasGMPermissions())
// 		gm = true;
// 
// 	uint32 sent_count = 0;
// 	uint32 total_count = 0;
// 
// 	PlayerStorageMap::const_iterator itr,iend;
// 	Player * plr;
// 	uint32 lvl;
// 	bool add;
// 
// 	MSG_S2C::stMsg_Who Msg;
// 
// 	objmgr._playerslock.AcquireReadLock();
// 	iend=objmgr._players.end();
// 	itr=objmgr._players.begin();
// 	while(itr !=iend && sent_count < 50)
// 	{
// 		MSG_S2C::stMsg_Who::stWho Who;
// 		plr = itr->second;
// 		++itr;
// 
// 		if(!plr->GetSession() || !plr->IsInWorld())
// 			continue;
// 
// 		if(!sWorld.show_gm_in_who_list && !HasGMPermissions())
// 		{
// 			if(plr->GetSession()->HasGMPermissions())
// 				continue;
// 		}
// 
// 		// Team check
// 		if(!gm && plr->GetTeam() != team && !plr->GetSession()->HasGMPermissions())
// 			continue;
// 
// 		++total_count;
// 
// 		// Add by default, if we don't have any checks
// 		add = true;
// 
// 		// Chat name
// 		if(cname && chatname != *plr->GetNameString())
// 			continue;
// 		
// 		// Level check
// 		lvl = plr->m_uint32Values[UNIT_FIELD_LEVEL];
// 		if(min_level && max_level)
// 		{
// 			// skip players outside of level range
// 			if(lvl < min_level || lvl > max_level)
// 				continue;
// 		}
// 
// 		// Zone id compare
// 		if(zone_count)
// 		{
// 			// people that fail the zone check don't get added
// 			add = false;
// 			for(i = 0; i < zone_count; ++i)
// 			{
// 				if(zones[i] == plr->GetZoneId())
// 				{
// 					add = true;
// 					break;
// 				}
// 			}
// 		}
// 
// 		if(!(class_mask & plr->getClassMask()) || !(race_mask & plr->getRaceMask()))
// 			add = false;
// 
// 		// skip players that fail zone check
// 		if(!add)
// 			continue;
// 
// 		// name check
// 		if(name_count)
// 		{
// 			// people that fail name check don't get added
// 			add = false;
// 			for(i = 0; i < name_count; ++i)
// 			{
// 				if(!strnicmp(names[i].c_str(), plr->GetName(), names[i].length()))
// 				{
// 					add = true;
// 					break;
// 				}
// 			}
// 		}
// 
// 		if(!add)
// 			continue;
// 
// 		// if we're here, it means we've passed all testing
// 		// so add the names :)
// 		Who.name = plr->GetName();
// 		if(plr->m_playerInfo->guild)
// 			Who.guildname = plr->m_playerInfo->guild->GetGuildName();
// 
// 		Who.Level = plr->m_uint32Values[UNIT_FIELD_LEVEL];
// 		Who.Class = uint32(plr->getClass());
// 		Who.Race = uint32(plr->getRace());
// 		Who.ZoneID = uint32(plr->GetZoneId());
// 		++sent_count;
// 		Msg.vPlayers.push_back(Who);
// 	}
// 	objmgr._playerslock.ReleaseReadLock();
// 
// 	SendPacket(Msg);
// 
// 	// free up used memory
// 	if(zones)
// 		delete [] zones;
// 	if(names)
// 		delete [] names;
}

void WorldSession::HandleLogoutRequestOpcode( CPacketUsn& packet )
{
	Player *pPlayer = GetPlayer();
	MSG_S2C::stLogout_Response Msg;

	MyLog::log->debug( "WORLD: Recvd CMSG_LOGOUT_REQUEST Message" );

	if(pPlayer)
	{
		sHookInterface.OnLogoutRequest(pPlayer);
		if(pPlayer->m_isResting ||	  // We are resting so log out instantly
			pPlayer->GetTaxiState() ||  // or we are on a taxi
			HasGMPermissions())		   // or we are a gm
		{
			pPlayer->SoftDisconnect();
			return;
		}

		if(pPlayer->DuelingWith != NULL || pPlayer->CombatStatus.IsInCombat())
		{
			//can't quit still dueling or attacking
			Msg.accepted = 0;
			SendPacket( Msg );
			return;
		}

		Msg.accepted = 0;
		SendPacket( Msg );

		//stop player from moving
		pPlayer->SetMovement(MOVE_ROOT,1);

		// Set the "player locked" flag, to prevent movement
		pPlayer->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER);

		//make player sit
		pPlayer->SetStandState(STANDSTATE_SIT);
		SetLogoutTimer(20000);
	}
	/*
	> 0 = You can't Logout Now
	*/
}

void WorldSession::HandlePlayerLogoutOpcode( CPacketUsn& packet )
{
	if( _player )
	{
		MyLog::log->debug( "WORLD: Recvd Logout acct:%u", this->_accountId );

		MSG_GS2GT::stLogoutAck Msg;
		Msg.guid = _player->GetGUID();
		this->SendPacket(Msg);

		m_disableMessage = true;
		LogoutPlayer(true, false);
	}
	else if( this == g_activity_mgr->tool_session )
	{
		g_activity_mgr->tool_session = NULL;
	}

	sWorld.DeleteSession( this );
}

void WorldSession::HandlePlayerExitOpcode(CPacketUsn& packet)
{
	MyLog::log->debug( "WORLD: a client has disconnected from gate!" );

	m_disableMessage = true;
	LogoutPlayer(true, false);
	sWorld.DeleteSession(this);
	if( this == g_activity_mgr->tool_session )
	{
		g_activity_mgr->tool_session = NULL;
	}
}

void WorldSession::HandleLogoutCancelOpcode( CPacketUsn& packet )
{

	MyLog::log->debug( "WORLD: Recvd CMSG_LOGOUT_CANCEL Message" );

	Player *pPlayer = GetPlayer();
	if(!pPlayer)
		return;

	//Cancel logout Timer
	SetLogoutTimer(0);

	//tell client about cancel
	OutPacket(SMSG_LOGOUT_CANCEL_ACK);

	//unroot player
	pPlayer->SetMovement(MOVE_UNROOT,5);

	// Remove the "player locked" flag, to allow movement
	pPlayer->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER);

	//make player stand
	pPlayer->SetStandState(STANDSTATE_STAND);

	MyLog::log->debug( "WORLD: sent SMSG_LOGOUT_CANCEL_ACK Message" );
}

void WorldSession::HandleZoneUpdateOpcode( CPacketUsn& packet )
{
	MSG_C2S::stZone_Update MsgRecv;packet >> MsgRecv;
	uint32 newZone = MsgRecv.newZone;
	WPAssert(GetPlayer());

	if (GetPlayer()->GetZoneId() == newZone)
		return;

	sWeatherMgr.SendWeather(GetPlayer());
	_player->ZoneUpdate(newZone);

	//clear buyback
	_player->GetItemInterface()->EmptyBuyBack();
}

void WorldSession::HandleSetTargetOpcode( CPacketUsn& packet )
{
	MSG_C2S::stSet_Target MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;

		if(guid == 0) // deselected target
		{
			// wait dude, 5 seconds -.-
			_player->CombatStatusHandler_ResetPvPTimeout();
			//_player->CombatStatus.ClearPrimaryAttackTarget();
		}
	if( GetPlayer( ) != 0 ){
		if(GetPlayer()->GetUInt64Value(PLAYER_FOOT))
		{
			Player* plr = GetPlayer()->GetMapMgr()->GetPlayer(GetPlayer()->GetUInt64Value(PLAYER_FOOT));
			if(plr)
				plr->SetTarget(guid);
			GetPlayer( )->SetTarget(guid);
		}
		if(GetPlayer()->GetUInt64Value(PLAYER_HEAD))
			return;
		else
			GetPlayer( )->SetTarget(guid);
	}
}

void WorldSession::HandleSetSelectionOpcode( CPacketUsn& packet )
{
	if( !_player->IsInWorld() ) return;

	MSG_C2S::stSet_Selection MsgRecv;packet>>MsgRecv;
	uint64 guid =MsgRecv.guid;

	if(!_player->GetUInt64Value(PLAYER_HEAD))
	{
		if(_player->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED) && !guid)
			_player->m_inarmedtimeleft = 20000;
		else if(guid && !MsgRecv.ToAttack)
			_player->m_inarmedtimeleft = 20000;
		else if(guid&&MsgRecv.ToAttack)
			_player->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);

		if(guid == 0) // deselected target
		{
			_player->CombatStatusHandler_ResetPvPTimeout();
			//_player->CombatStatus.ClearPrimaryAttackTarget();
		}

		_player->SetSelection(guid);
		_player->SetUInt64Value(UNIT_FIELD_TARGET, guid);
		Player* plr = _player->GetMapMgr()->GetPlayer(_player->GetUInt64Value(PLAYER_FOOT));
		if(plr)
		{
			plr->SetSelection(guid);
			plr->SetUInt64Value(UNIT_FIELD_TARGET, guid);
		}
	}
	else
	{
	}
}

void WorldSession::HandleStandStateChangeOpcode( CPacketUsn& packet )
{
	MSG_C2S::stStandState_Change MsgRecv;packet>>MsgRecv;
	uint8 animstate = MsgRecv.animstate;
	_player->SetStandState(animstate);
}

void WorldSession::HandleBugOpcode( CPacketUsn& packet )
{
	MSG_C2S::stBugReport MsgRecv;packet>>MsgRecv;
	if( MsgRecv.suggestion == 0 )
		MyLog::log->debug( "WORLD: Received CMSG_BUG [Bug Report]" );
	else
		MyLog::log->debug( "WORLD: Received CMSG_BUG [Suggestion]" );

	MyLog::log->debug( MsgRecv.type.c_str( ) );
	MyLog::log->debug( MsgRecv.content.c_str( ) );
}

void WorldSession::HandleCorpseReclaimOpcode(CPacketUsn& packet)
{
	if(_player->isAlive())
		return;

	MyLog::log->notice("WORLD: Received CMSG_RECLAIM_CORPSE");

	MSG_C2S::stCorpse_Reclaim MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;;

	Corpse* pCorpse = objmgr.GetCorpse( (uint32)guid );

	if( pCorpse == NULL )
		return;

	// Check that we're reviving from a corpse, and that corpse is associated with us.
	if( pCorpse->GetUInt32Value( CORPSE_FIELD_OWNER ) != _player->GetLowGUID() && pCorpse->GetUInt32Value( CORPSE_FIELD_FLAGS ) == 5 )
	{
		MSG_S2C::stResurrect_Failed Msg;
		SendPacket(Msg);
		return;
	}

	// Check we are actually in range of our corpse
    if ( pCorpse->GetDistance2dSq( _player ) > CORPSE_MINIMUM_RECLAIM_RADIUS_SQ )
  	{
		MSG_S2C::stResurrect_Failed Msg;
		SendPacket(Msg);
		return;
	}

    // Check death clock before resurrect they must wait for release to complete
    if( pCorpse->GetDeathClock() + CORPSE_RECLAIM_TIME > time( NULL ) )
	{
		MSG_S2C::stResurrect_Failed Msg;
		SendPacket(Msg);
		return;
	}

	GetPlayer()->ResurrectPlayer();
	GetPlayer()->SetUInt32Value(UNIT_FIELD_HEALTH, GetPlayer()->GetUInt32Value(UNIT_FIELD_MAXHEALTH)/2 );
}

void WorldSession::HandleResurrectResponseOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MyLog::log->notice("WORLD: Received CMSG_RESURRECT_RESPONSE");

	if(GetPlayer()->isAlive())
		return;

	MSG_C2S::stResurrect_Response MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;;
	uint8 status = MsgRecv.status;

	// need to check guid
	Player * pl = _player->GetMapMgr()->GetPlayer((uint32)guid);
	if(!pl)
		pl = objmgr.GetPlayer((uint32)guid);
		
	
	if(pl == 0 || status != 1)
	{
		_player->m_resurrectHealth = 0;
		_player->m_resurrectMana = 0;
		return;
	}

	_player->ResurrectPlayer();
	_player->SetMovement(MOVE_UNROOT, 1);
}

void WorldSession::HandleUpdateAccountData(CPacketUsn& packet)
{
	//MyLog::log->notice("WORLD: Received CMSG_UPDATE_ACCOUNT_DATA");

// 	uint32 uiID;
// 	if(!sWorld.m_useAccountData)
// 		return;
// 
// 	recv_data >> uiID;
// 
// 	if(uiID > 8)
// 	{
// 		// Shit..
// 		MyLog::log->info("WARNING: Accountdata > 8 (%d) was requested to be updated by %s of account %d!", uiID, GetPlayer()->GetName(), this->GetAccountId());
// 		return;
// 	}
// 
// 	uint32 uiDecompressedSize;
// 	recv_data >> uiDecompressedSize;
// 	uLongf uid = uiDecompressedSize;
// 
// 	// client wants to 'erase' current entries
// 	if(uiDecompressedSize == 0)
// 	{
// 		SetAccountData(uiID, NULL, false,0);
// 		return;
// 	}
// 
// 	if(uiDecompressedSize>100000)
// 	{
// 		Disconnect();
// 		return;
// 	}
// 
// 	if(uiDecompressedSize >= 65534)
// 	{
// 		// BLOB fields can't handle any more than this.
// 		return;
// 	}
// 
// 	size_t ReceivedPackedSize = recv_data.size() - 8;
// 	char* data = new char[uiDecompressedSize+1];
// 	memset(data,0,uiDecompressedSize+1);	/* fix umr here */
// 
// 	if(uiDecompressedSize > ReceivedPackedSize) // if packed is compressed
// 	{
// 		int32 ZlibResult;
// 
// 		ZlibResult = uncompress((uint8*)data, &uid, recv_data.contents() + 8, (uLong)ReceivedPackedSize);
// 		
// 		switch (ZlibResult)
// 		{
// 		case Z_OK:				  //0 no error decompression is OK
// 			SetAccountData(uiID, data, false,uiDecompressedSize);
// 			MyLog::log->notice("WORLD: Successfully decompressed account data %d for %s, and updated storage array.", uiID, GetPlayer()->GetName());
// 			break;
// 		
// 		case Z_ERRNO:			   //-1
// 		case Z_STREAM_ERROR:		//-2
// 		case Z_DATA_ERROR:		  //-3
// 		case Z_MEM_ERROR:		   //-4
// 		case Z_BUF_ERROR:		   //-5
// 		case Z_VERSION_ERROR:	   //-6
// 		{
// 			delete [] data;	 
// 			MyLog::log->info("WORLD WARNING: Decompression of account data %d for %s FAILED.", uiID, GetPlayer()->GetName());
// 			break;
// 		}
// 
// 		default:
// 			delete [] data;	 
// 			MyLog::log->info("WORLD WARNING: Decompression gave a unknown error: %x, of account data %d for %s FAILED.", ZlibResult, uiID, GetPlayer()->GetName());
// 			break;
// 		}
// 	}
// 	else
// 	{
// 		memcpy(data,recv_data.contents() + 8,uiDecompressedSize);
// 		SetAccountData(uiID, data, false,uiDecompressedSize);
// 	}
}

void WorldSession::HandleRequestAccountData(CPacketUsn& packet)
{
	//MyLog::log->notice("WORLD: Received CMSG_REQUEST_ACCOUNT_DATA");

// 	uint32 id;
// 	if(!sWorld.m_useAccountData)
// 		return;
// 	recv_data >> id;
// 	
// 	if(id > 8)
// 	{
// 		// Shit..
// 		MyLog::log->info("WARNING: Accountdata > 8 (%d) was requested by %s of account %d!", id, GetPlayer()->GetName(), this->GetAccountId());
// 		return;
// 	}
// 
// 	AccountDataEntry* res = GetAccountData(id);
// 		WorldPacket data ;
// 		data.SetOpcode(SMSG_UPDATE_ACCOUNT_DATA);
// 		data << id;
// 	// if red does not exists if ID == 7 and if there is no data send 0
// 	if (!res || !res->data) // if error, send a NOTHING packet
// 	{
// 		data << (uint32)0;
// 	}
// 	else
// 	{
// 		data << res->sz;
// 		uLongf destsize;
// 		if(res->sz>200)
// 		{
// 			data.resize( res->sz+800 );  // give us plenty of room to work with..
// 			
// 			if ( ( compress(const_cast<uint8*>(data.contents()) + (sizeof(uint32)*2), &destsize, (const uint8*)res->data, res->sz)) != Z_OK)
// 			{
// 				MyLog::log->error("Error while compressing ACCOUNT_DATA");
// 				return;
// 			}
// 			
// 			data.resize(destsize+8);
// 		}
// 		else 
// 			data.append(	res->data,res->sz);	
// 	}
// 		
// 	SendPacket(&data);	
}

void WorldSession::HandleSetActionButtonOpcode(CPacketUsn& packet)
{
//	MyLog::log->debug( "WORLD: Received CMSG_SET_ACTION_BUTTON" ); 
	MSG_C2S::stActionButton_Set MsgRecv;packet>>MsgRecv;
	uint8 button = MsgRecv.button;
	uint8 misc = MsgRecv.misc;
	uint8 type = MsgRecv.type;  
	uint32 action = MsgRecv.action; 
//	MyLog::log->debug( "BUTTON: %u ACTION: %u TYPE: %u MISC: %u", button, action, type, misc ); 
	if(action==0)
	{
//		MyLog::log->debug( "MISC: Remove action from button %u", button ); 
		//remove the action button from the db
		GetPlayer()->setAction(button, 0, 0, 0);
	}
	else
	{ 
		if(button >= 120)
			return;

		if(type == ActionButton_Type_MACRO || type == 65) 
		{
			MyLog::log->debug( "MISC: Added Macro %u into button %u", action, button );
			GetPlayer()->setAction(button,action,type,misc);
		}
		else if(type == ActionButton_Type_Item)
		{
			MyLog::log->debug( "MISC: Added Item %u into button %u", action, button );
			GetPlayer()->setAction(button,action,type,misc);
		}
		else if(type == ActionButton_Type_Spell)
		{
			MyLog::log->debug( "MISC: Added Spell %u into button %u", action, button );
			GetPlayer()->setAction(button,action,type,misc);
		} 
	}

#ifdef OPTIMIZED_PLAYER_SAVING
	_player->save_Actions();
#endif
}

void WorldSession::HandleSetWatchedFactionIndexOpcode(CPacketUsn& packet)
{
	MSG_C2S::stFaction_Set_Watched_Index MsgRecv;packet>>MsgRecv;
	GetPlayer()->SetUInt32Value(PLAYER_FIELD_WATCHED_FACTION_INDEX, MsgRecv.factionid);

#ifdef OPTIMIZED_PLAYER_SAVING
	_player->save_Misc();
#endif
}

void WorldSession::HandleTogglePVPOpcode(CPacketUsn& packet)
{
	_player->PvPToggle();
}

void WorldSession::HandleAmmoSetOpcode(CPacketUsn& packet)
{
// 	uint32 ammoId;
// 	recv_data >> ammoId;
// 
// 	if(!ammoId)
// 		return;
// 
// 	ItemPrototype * xproto=ItemPrototypeStorage.LookupEntry(ammoId);
// 	if(!xproto)
// 		return;
// 
// 	if(xproto->Class != ITEM_CLASS_PROJECTILE || GetPlayer()->GetItemInterface()->GetItemCount(ammoId) == 0)
// 	{
// 		sCheatLog.writefromsession(GetPlayer()->GetSession(), "Definately cheating. tried to add %u as ammo.", ammoId);
// 		GetPlayer()->GetSession()->Disconnect();
// 		return;
// 	}
// 
// 	if(xproto->RequiredLevel)
// 	{
// 		if(GetPlayer()->getLevel() < xproto->RequiredLevel)
// 		{
// 			GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL,NULL,INV_ERR_ITEM_RANK_NOT_ENOUGH);
// 			_player->SetUInt32Value(PLAYER_AMMO_ID, 0);
// 			_player->CalcDamage();
// 			return;
// 		}
// 	}
// 	if(xproto->RequiredSkill)
// 	{
// 		if(!GetPlayer()->_HasSkillLine(xproto->RequiredSkill))
// 		{
// 			GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL,NULL,INV_ERR_ITEM_RANK_NOT_ENOUGH);
// 			_player->SetUInt32Value(PLAYER_AMMO_ID, 0);
// 			_player->CalcDamage();
// 			return;
// 		}
// 
// 		if(xproto->RequiredSkillRank)
// 		{
// 			if(_player->_GetSkillLineCurrent(xproto->RequiredSkill, false) < xproto->RequiredSkillRank)
// 			{
// 				GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL,NULL,INV_ERR_ITEM_RANK_NOT_ENOUGH);
// 				_player->SetUInt32Value(PLAYER_AMMO_ID, 0);
// 				_player->CalcDamage();
// 				return;
// 			}
// 		}
// 	}
// 	_player->SetUInt32Value(PLAYER_AMMO_ID, ammoId);
// 	_player->CalcDamage();
// 
// #ifdef OPTIMIZED_PLAYER_SAVING
// 	_player->save_Misc();
// #endif
}

#define OPEN_CHEST 11437 
void WorldSession::HandleGameObjectUse(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stGameObj_Use MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;
	SpellCastTargets targets;
	Spell *spell = NULL;;
	SpellEntry *spellInfo = NULL;;
	MyLog::log->debug("WORLD: CMSG_GAMEOBJ_USE: [GUID %d]", guid);   

	GameObject *obj = _player->GetMapMgr()->GetGameObject((uint32)guid);
	if (!obj) return;
	GameObjectInfo *goinfo= obj->GetInfo();
	if (!goinfo) return;

	Player *plyr = GetPlayer();
   
	CALL_GO_SCRIPT_EVENT(obj, OnActivate)(_player);

	uint32 type = obj->GetUInt32Value(GAMEOBJECT_TYPE_ID);
	switch (type)
	{
		case GAMEOBJECT_TYPE_CHAIR:
		{

			/*WorldPacket data(MSG_MOVE_HEARTBEAT, 66);
			data << plyr->GetNewGUID();
			data << uint8(0);
			data << uint64(0);
			data << obj->GetPositionX() << obj->GetPositionY() << obj->GetPositionZ() << obj->GetOrientation();
			plyr->SendMessageToSet(&data, true);*/
			plyr->SafeTeleport( plyr->GetMapId(), plyr->GetInstanceID(), obj->GetPositionX(), obj->GetPositionY(), obj->GetPositionZ(), obj->GetOrientation() );
			plyr->SetStandState(STANDSTATE_SIT_MEDIUM_CHAIR);
			plyr->m_lastRunSpeed = 0; //counteract mount-bug; reset speed to zero to force update SetPlayerSpeed in next line.
			plyr->SetPlayerSpeed(RUN,plyr->m_base_runSpeed);
		}break;
	case GAMEOBJECT_TYPE_CHEST://cast da spell
		{
			spellInfo = dbcSpell.LookupEntry( OPEN_CHEST );
			spell = new Spell(plyr, spellInfo, true, NULL);
			_player->m_currentSpell = spell;
			targets.m_unitTarget = obj->GetGUID();
			spell->prepare(&targets); 
		}break;
	case GAMEOBJECT_TYPE_FISHINGNODE:
		{
			obj->UseFishingNode(plyr);
		}break;
	case GAMEOBJECT_TYPE_DOOR:
		{
			// door
			if((obj->GetUInt32Value(GAMEOBJECT_STATE) == 1) && (obj->GetUInt32Value(GAMEOBJECT_FLAGS) == 33))
				obj->EventCloseDoor();
			else
			{
				obj->SetUInt32Value(GAMEOBJECT_FLAGS, 33);
				obj->SetUInt32Value(GAMEOBJECT_STATE, 0);
				sEventMgr.AddEvent(obj,&GameObject::EventCloseDoor,EVENT_GAMEOBJECT_DOOR_CLOSE,20000,1,0);
			}
		}break;
	case GAMEOBJECT_TYPE_FLAGSTAND:
		{
		}break;
	case GAMEOBJECT_TYPE_FLAGDROP:
		{
		}break;
	case GAMEOBJECT_TYPE_QUESTGIVER:
		{
			// Questgiver
			if(obj->HasQuests())
			{
				sQuestMgr.OnActivateQuestGiver(obj, plyr);
			}
		}break;
	case GAMEOBJECT_TYPE_SPELLCASTER:
		{
			SpellEntry *info = dbcSpell.LookupEntry(goinfo->SpellFocus);
			if(!info)
				break;
			Spell * spell = new Spell(plyr, info, false, NULL);
			//spell->SpellByOther = true;
			SpellCastTargets targets;
			targets.m_unitTarget = plyr->GetGUID();
			spell->prepare(&targets);
			if(obj->charges>0 && !--obj->charges)
				obj->ExpireAndDelete();
		}break;
	case GAMEOBJECT_TYPE_RITUAL: 
		{
			// store the members in the ritual, cast sacrifice spell, and summon.
			uint32 i = 0;
			if(!obj->m_ritualmembers || !obj->m_ritualspell || !obj->m_ritualcaster /*|| !obj->m_ritualtarget*/)
				return;

			for(i=0;i<goinfo->SpellFocus;i++)
			{
				if(!obj->m_ritualmembers[i])
				{
					obj->m_ritualmembers[i] = plyr->GetLowGUID();
					plyr->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, obj->GetGUID());
					plyr->SetUInt32Value(UNIT_CHANNEL_SPELL, obj->m_ritualspell);
					break;
				}else if(obj->m_ritualmembers[i] == plyr->GetLowGUID()) 
				{
					// we're deselecting :(
					obj->m_ritualmembers[i] = 0;
					plyr->SetUInt32Value(UNIT_CHANNEL_SPELL, 0);
					plyr->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, 0);
					return;
				}
			}

			if(i == goinfo->SpellFocus - 1)
			{
				obj->m_ritualspell = 0;
				Player * plr;
				for(i=0;i<goinfo->SpellFocus;i++)
				{
					plr = _player->GetMapMgr()->GetPlayer(obj->m_ritualmembers[i]);
					if(plr)
					{
						plr->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, 0);
						plr->SetUInt32Value(UNIT_CHANNEL_SPELL, 0);
					}
				}
				
				SpellEntry *info = NULL;				
				if(goinfo->ID == 36727) // summon portal
				{
					if(!obj->m_ritualtarget)
						return;
					info = dbcSpell.LookupEntry(goinfo->castspell);
					if(!info)
						break;
					Player * target = _player->GetMapMgr()->GetPlayer(obj->m_ritualtarget);
					if(!target)
						return;

					spell = new Spell(obj,info,true,NULL);
					SpellCastTargets targets;
					targets.m_unitTarget = target->GetGUID();
					spell->prepare(&targets);
				}
				else if(goinfo->ID == 177193) // doom portal
				{
					Player *psacrifice = NULL;
					Spell * spell = NULL;
					
					// kill the sacrifice player
					psacrifice = _player->GetMapMgr()->GetPlayer(obj->m_ritualmembers[(int)(rand()%(goinfo->SpellFocus-1))]);
					Player * pCaster = obj->GetMapMgr()->GetPlayer(obj->m_ritualcaster);
					if(!psacrifice || !pCaster)
						return;

					info = dbcSpell.LookupEntry(goinfo->castspell);
					if(!info)
						break;
					spell = new Spell(psacrifice, info, true, NULL);
					targets.m_unitTarget = psacrifice->GetGUID();
					spell->prepare(&targets);
					
					// summons demon		   
					info = dbcSpell.LookupEntry(goinfo->castspell);
					spell = new Spell(pCaster, info, true, NULL);
					SpellCastTargets targets;
					targets.m_unitTarget = pCaster->GetGUID();
					spell->prepare(&targets);					
				}
				else if(goinfo->ID == 179944)			// Summoning portal for meeting stones
				{
					Player * plr = _player->GetMapMgr()->GetPlayer(obj->m_ritualtarget);
					if(!plr)
						return;

					Player * pleader = _player->GetMapMgr()->GetPlayer(obj->m_ritualcaster);
					if(!pleader)
						return;

					info = dbcSpell.LookupEntry(goinfo->castspell);
					Spell * spell = new Spell(pleader, info, true, 0);
					SpellCastTargets targets(plr->GetGUID());
					spell->prepare(&targets);

					/* expire the gameobject */
					obj->ExpireAndDelete();
				}
			}
		}break;
	case GAMEOBJECT_TYPE_GOOBER:
		{
			//Quest related mostly
		}
	case GAMEOBJECT_TYPE_CAMERA://eye of azora
		{
			/*WorldPacket pkt(SMSG_TRIGGER_CINEMATIC,4);
			pkt << (uint32)1;//i ve found only on such item,id =1
			SendPacket(&pkt);*/

			/* these are usually scripted effects. but in the case of some, (e.g. orb of translocation) the spellid is located in unknown1 */
			SpellEntry * sp = dbcSpell.LookupEntry(0/*goinfo->Unknown1*/);
			if(sp != NULL)
				_player->CastSpell(_player,sp,true);
		}break;
	case GAMEOBJECT_TYPE_MEETINGSTONE:	// Meeting Stone
		{
			/* Use selection */
            Player * pPlayer = objmgr.GetPlayer((uint32)_player->GetSelection());
			if(!pPlayer || _player->GetGroup() != pPlayer->GetGroup() || !_player->GetGroup())
				return;

			GameObjectInfo * info = GameObjectNameStorage.LookupEntry(179944);
			if(!info)
				return;

			/* Create the summoning portal */
			GameObject * pGo = _player->GetMapMgr()->CreateGameObject(179944);
			pGo->CreateFromProto(179944, _player->GetMapId(), _player->GetPositionX(), _player->GetPositionY(), _player->GetPositionZ(), 0);
			pGo->m_ritualcaster = _player->GetLowGUID();
			pGo->m_ritualtarget = pPlayer->GetLowGUID();
			pGo->m_ritualspell = 18540;	// meh
			pGo->PushToWorld(_player->GetMapMgr());

			/* member one: the (w00t) caster */
			pGo->m_ritualmembers[0] = _player->GetLowGUID();
			_player->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, pGo->GetGUID());
			_player->SetUInt32Value(UNIT_CHANNEL_SPELL, pGo->m_ritualspell);
			
			/* expire after 2mins*/
			sEventMgr.AddEvent(pGo, &GameObject::_Expire, EVENT_GAMEOBJECT_EXPIRE, 120000, 1,0);
		}break;
	}   
}

void WorldSession::HandleTutorialFlag( CPacketUsn& packet )
{
	MSG_C2S::stTutorial_Flag MsgRecv;packet>>MsgRecv;
	uint32 iFlag = MsgRecv.flag;

	uint32 wInt = (iFlag / 8);
	uint32 rInt = (iFlag % 64);

	if(wInt >= 7)
	{
		Disconnect();
		return;
	}

	uint32 tutflag = GetPlayer()->GetTutorialInt( wInt );
	tutflag |= (1 << rInt);
	GetPlayer()->SetTutorialInt( wInt, tutflag );

	MyLog::log->debug("Received Tutorial Flag Set {%u}.", iFlag);
}

void WorldSession::HandleTutorialClear( CPacketUsn& packet )
{
	for ( uint32 iI = 0; iI < 8; iI++)
		GetPlayer()->SetTutorialInt( iI, 0xFFFFFFFF );
}

void WorldSession::HandleTutorialReset( CPacketUsn& packet )
{
	for ( uint32 iI = 0; iI < 8; iI++)
		GetPlayer()->SetTutorialInt( iI, 0x00000000 );
}

void WorldSession::HandleSetSheathedOpcode( CPacketUsn& packet )
{
	MSG_C2S::stSet_Sheathed MsgRecv;packet>>MsgRecv;
	uint32 active = MsgRecv.active;
	_player->SetByte(UNIT_FIELD_BYTES_2,0,(uint8)active); 
}

void WorldSession::HandlePlayedTimeOpcode( CPacketUsn& packet )
{
	uint32 playedt = (uint32)UNIXTIME - _player->m_playedtime[2];
	if(playedt)
	{
		_player->m_playedtime[0] += playedt;
		_player->m_playedtime[1] += playedt;
		_player->m_playedtime[2] += playedt;
	}

	MSG_S2C::stPlayed_Time Msg;
	Msg.play_time1 = _player->m_playedtime[1];
	Msg.play_time2 = _player->m_playedtime[0];
	SendPacket( Msg );
}

void WorldSession::HandleInspectOpcode( CPacketUsn& packet )
{
/*
	CHECK_INWORLD_RETURN

	MSG_C2S::stInspect MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;
	uint32 talent_points = 0x0000003D;

	Player * player = _player->GetMapMgr()->GetPlayer( (uint32)guid );
    
	if( player == NULL )
	{
		MyLog::log->error( "HandleInspectOpcode: guid was null" );
		return;
	}

	_player->SetSelection( guid );

	MSG_S2C::stInspect_Talents Msg;
	Msg.talent_points = talent_points;

	uint32 talent_tab_pos = 0;
	uint32 talent_max_rank;
	uint32 talent_tab_id;
	uint32 talent_index;
	uint32 rank_index;
	uint32 rank_slot;
	uint32 rank_offset;
	uint32 mask;

//     for( uint32 i = 0; i < talent_points; ++i )
//         data << uint8( 0 );

    for( uint32 i = 0; i < 3; ++i )
    {
		talent_tab_id = sWorld.InspectTalentTabPages[player->getClass()][i];

		for( uint32 j = 0; j < dbcTalent.GetNumRows(); ++j )
		{
			TalentEntry const* talent_info = dbcTalent.LookupRow( j );

			//MyLog::log->debug( "HandleInspectOpcode: i(%i) j(%i)", i, j );

			if( talent_info == NULL )
				continue;

			//MyLog::log->debug( "HandleInspectOpcode: talent_info->TalentTree(%i) talent_tab_id(%i)", talent_info->TalentTree, talent_tab_id );

			if( talent_info->TalentTree != talent_tab_id )
				continue;

			talent_max_rank = 0;
			for( uint32 k = 5; k > 0; --k )
			{
				//MyLog::log->debug( "HandleInspectOpcode: k(%i) RankID(%i) HasSpell(%i) TalentTree(%i) Tab(%i)", k, talent_info->RankID[k - 1], player->HasSpell( talent_info->RankID[k - 1] ), talent_info->TalentTree, talent_tab_id );
				if( talent_info->RankID[k - 1] != 0 && player->HasSpell( talent_info->RankID[k - 1] ) )
				{
					talent_max_rank = k;
					break;
				}
			}

			//MyLog::log->debug( "HandleInspectOpcode: RankID(%i) talent_max_rank(%i)", talent_info->RankID[talent_max_rank-1], talent_max_rank );

			if( talent_max_rank <= 0 )
				continue;

			talent_index = talent_tab_pos;

			std::map< uint32, uint32 >::iterator itr = sWorld.InspectTalentTabPos.find( talent_info->TalentID );

			if( itr != sWorld.InspectTalentTabPos.end() )
				talent_index += itr->second;
			//else
				//MyLog::log->debug( "HandleInspectOpcode: talent(%i) rank_id(%i) talent_index(%i) talent_tab_pos(%i) rank_index(%i) rank_slot(%i) rank_offset(%i)", talent_info->TalentID, talent_info->RankID[talent_max_rank-1], talent_index, talent_tab_pos, rank_index, rank_slot, rank_offset );

			rank_index = ( uint32( ( talent_index + talent_max_rank - 1 ) / 7 ) ) * 8  + ( uint32( ( talent_index + talent_max_rank - 1 ) % 7 ) );
			rank_slot = rank_index / 8;
			rank_offset = rank_index % 8;
			mask = 1 << rank_offset;
			//data.put< uint8 >( 4 + rank_slot, mask & 0xFF );

			MyLog::log->debug( "HandleInspectOpcode: talent(%i) talent_max_rank(%i) rank_id(%i) talent_index(%i) talent_tab_pos(%i) rank_index(%i) rank_slot(%i) rank_offset(%i)", talent_info->TalentID, talent_max_rank, talent_info->RankID[talent_max_rank-1], talent_index, talent_tab_pos, rank_index, rank_slot, rank_offset );
		}

		std::map< uint32, uint32 >::iterator itr = sWorld.InspectTalentTabSize.find( talent_tab_id );

		if( itr != sWorld.InspectTalentTabSize.end() )
			talent_tab_pos += itr->second;

	}

    SendPacket( Msg );
*/
}

void WorldSession::HandleSetActionBarTogglesOpcode(CPacketUsn& packet)
{
	MSG_C2S::stActionBar_Set_Toggles MsgRecv;packet>>MsgRecv;
	uint8 cActionBarId = MsgRecv.cActionBarId;
	MyLog::log->debug("Received CMSG_SET_ACTIONBAR_TOGGLES for actionbar id %d.", cActionBarId);
   
	GetPlayer()->SetByte(PLAYER_FIELD_BYTES,2, cActionBarId);
}

// Handlers for acknowledgement opcodes (removes some 'unknown opcode' flood from the logs)
void WorldSession::HandleAcknowledgementOpcodes( CPacketUsn& packet )
{
	MSG_C2S::stMove_Ack MsgRecv;packet>>MsgRecv;
	switch(MsgRecv.moveAck)
	{
	case MOVE_WATER_WALK_ACK:
		_player->m_waterwalk = _player->m_setwaterwalk;
		break;

	case MOVE_SET_FLY_ACK:
		_player->FlyCheat = _player->m_setflycheat;
		break;

	case MOVE_FORCE_RUN_SPEED_CHANGE_ACK:
	case MOVE_FORCE_RUN_BACK_SPEED_CHANGE_ACK:
	case MOVE_FORCE_SWIM_SPEED_CHANGE_ACK:
	case MOVE_FORCE_SWIM_BACK_SPEED_CHANGE_ACK:
	case MOVE_FORCE_FLY_BACK_SPEED_CHANGE_ACK:
	case MOVE_FORCE_MOVE_SET_FLY_SPEED_ACK:
		_player->ResetHeartbeatCoords();
		_player->DelaySpeedHack( 5000 );			// give the client a chance to fall/catch up
		_player->_speedChangeInProgress = false;
		break;
	}

   /* uint16 opcode = recv_data.GetOpcode();
	std::stringstream ss;
	ss << "Received ";
	switch( opcode )
	{
	case CMSG_MOVE_FEATHER_FALL_ACK:			ss << "Move_Feather_Fall"; break;
	case CMSG_MOVE_WATER_WALK_ACK:			  ss << "Move_Water_Walk"; break;
	case CMSG_MOVE_KNOCK_BACK_ACK:			  ss << "Move_Knock_Back"; break;
	case CMSG_MOVE_HOVER_ACK:				   ss << "Move_Hover"; break;
	case CMSG_FORCE_WALK_SPEED_CHANGE_ACK:	  ss << "Force_Walk_Speed_Change"; break;
	case CMSG_FORCE_SWIM_SPEED_CHANGE_ACK:	  ss << "Force_Swim_Speed_Change"; break;
	case CMSG_FORCE_SWIM_BACK_SPEED_CHANGE_ACK: ss << "Force_Swim_Back_Speed_Change"; break;
	case CMSG_FORCE_TURN_RATE_CHANGE_ACK:	   ss << "Force_Turn_Rate_Change"; break;
	case CMSG_FORCE_RUN_SPEED_CHANGE_ACK:	   ss << "Force_Run_Speed_Change"; break;
	case CMSG_FORCE_RUN_BACK_SPEED_CHANGE_ACK:  ss << "Force_Run_Back_Speed_Change"; break;
	case CMSG_FORCE_MOVE_ROOT_ACK:			  ss << "Force_Move_Root"; break;
	case CMSG_FORCE_MOVE_UNROOT_ACK:			ss << "Force_Move_Unroot"; break;
	default:									ss << "Unknown"; break;
	}
	ss << " Acknowledgement. PktSize: " << recv_data.size();
	MyLog::log->debug( ss.str().c_str() );*/

	/*uint16 opcode = recv_data.GetOpcode();
	if (opcode == CMSG_FORCE_RUN_SPEED_CHANGE_ACK)
	{
 
		uint64 GUID;
		uint32 Flags, unk0, unk1, d_time;
		float X, Y, Z, O, speed;
		
		recv_data >> GUID;
		recv_data >> unk0 >> Flags;
		if (Flags & (0x2000 | 0x6000))			 //0x2000 == jumping  0x6000 == Falling
		{
			uint32 unk2, unk3, unk4, unk5;
			float OldSpeed;

			recv_data >> d_time;
			recv_data >> X >> Y >> Z >> O;
			recv_data >> unk2 >> unk3;						  //no idea, maybe unk2 = flags2
			recv_data >> unk4 >> unk5;						  //no idea
			recv_data >> OldSpeed >> speed;
		}
		else													//single check
		{
			recv_data >> d_time;
			recv_data >> X >> Y >> Z >> O;
			recv_data >> unk1 >> speed;
		}
		
		// if its not good kick player???
		if (_player->GetPlayerSpeed() != speed)
		{
			MyLog::log->error("SpeedChange player:%s is NOT correct, its set to: %f he seems to be cheating",_player->GetName(), speed);
		}
	}*/

}

void WorldSession::HandleSelfResurrectOpcode(CPacketUsn& packet)
{
	uint32 self_res_spell = _player->GetUInt32Value(PLAYER_SELF_RES_SPELL);
	if(self_res_spell)
	{
		SpellEntry * sp=dbcSpell.LookupEntry(self_res_spell);
		Spell *s=new Spell(_player,sp,true,NULL);
		SpellCastTargets tgt;
		tgt.m_unitTarget=_player->GetGUID();
		s->prepare(&tgt);	
	}
}

void WorldSession::HandleRandomRollOpcode(CPacketUsn& packet)
{
	MSG_C2S::stMsg_Random_Roll MsgRecv;packet>>MsgRecv;
	uint32 minValue = MsgRecv.min;
	uint32 max = MsgRecv.max;

	if( max <= minValue )
		return;

	MyLog::log->notice("WORLD: Received MSG_RANDOM_ROLL: %u-%u", minValue, max);

	MSG_S2C::stMsg_Random_Roll Msg;
	Msg.min = minValue;
	Msg.max = max;

	uint32 roll;

	// generate number
	//roll = minValue + int( ((max-minValue)+1) * rand() / (RAND_MAX + 1.0) );
	roll = minValue + rand() % (max - minValue);
	
	// append to packet, and guid
	Msg.rollnum = roll;
	Msg.player_guid = _player->GetGUID();

	// send to set
    if(_player->InGroup())
		_player->GetGroup()->SendPacketToAll(Msg);
	else
	    GetPlayer()->SendMessageToSet(Msg, true, true);
}

void WorldSession::HandleLootMasterGiveOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
//	uint8 slot = 0;
	uint32 itemid = 0;
	uint32 amt = 1;
	uint8 error = 0;
	SlotResult slotresult;

	Creature *pCreature = NULL;
	Loot *pLoot = NULL;
	/* struct:
	{CLIENT} Packet: (0x02A3) CMSG_LOOT_MASTER_GIVE PacketSize = 17
	|------------------------------------------------|----------------|
	|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
	|------------------------------------------------|----------------|
	|39 23 05 00 81 02 27 F0 01 7B FC 02 00 00 00 00 |9#....'..{......|
	|00											  |.			   |
	-------------------------------------------------------------------

		uint64 creatureguid
		uint8  slotid
		uint64 target_playerguid

	*/

	MSG_C2S::stLoot_Master_Give MsgRecv;packet>>MsgRecv;
	uint64 creatureguid = MsgRecv.creatureguid;
	ui64	target_playerguid =MsgRecv.target_playerguid;
	uint8 slotid = MsgRecv.slotid;

	if(_player->GetGroup() == NULL || _player->GetGroup()->GetLooter() != _player->m_playerInfo)
		return;

	Player *player = _player->GetMapMgr()->GetPlayer((uint32)target_playerguid);
	if(!player)
		return;

	// cheaterz!
	if(_player->GetLootGUID() != creatureguid)
		return;

	//now its time to give the loot to the target player
	if(GET_TYPE_FROM_GUID(GetPlayer()->GetLootGUID()) == HIGHGUID_TYPE_UNIT)
	{
		pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(creatureguid));
		if (!pCreature)return;
		pLoot=&pCreature->loot;	
	}

	if(!pLoot) return;

	if (slotid >= pLoot->items.size())
	{
		MyLog::log->debug("AutoLootItem: Player %s might be using a hack! (slot %d, size %d)",
						GetPlayer()->GetName(), slotid, pLoot->items.size());
		return;
	}

	amt = pLoot->items.at(slotid).iItemsCount;

	if (!pLoot->items.at(slotid).ffa_loot)
	{
		if (!amt)//Test for party loot
		{  
			GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL, NULL,INV_ERR_ALREADY_LOOTED);
			return;
		} 
	}
	else
	{
		//make sure this player can still loot it in case of ffa_loot
		LooterSet::iterator itr = pLoot->items.at(slotid).has_looted.find(player->GetLowGUID());

		if (pLoot->items.at(slotid).has_looted.end() != itr)
		{
			GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL, NULL,INV_ERR_ALREADY_LOOTED);
			return;
		}
	}

	itemid = pLoot->items.at(slotid).item.itemproto->ItemId;
	ItemPrototype* it = pLoot->items.at(slotid).item.itemproto;

	if((error = player->GetItemInterface()->CanReceiveItem(it, 1)))
	{
		_player->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, error);
		return;
	}

	if(pCreature)
		CALL_SCRIPT_EVENT(pCreature, OnLootTaken)(player, it);
	
	
	slotresult = player->GetItemInterface()->FindFreeInventorySlot(it);
	if(!slotresult.Result)
	{
		GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_INVENTORY_FULL);
		return;
	}

	Item *item = objmgr.CreateItem( itemid, player);
	
	item->SetUInt32Value(ITEM_FIELD_STACK_COUNT,amt);

	if( item->GetProto()->SubClass < ITEM_SUBCLASS_ARMOR_GUAIWUBEIBU )
	{
		if (pLoot->items.at(slotid).count)
		{
			item->SetVariableProperties(pLoot->items.at(slotid).id, pLoot->items.at(slotid).count);
			item->ApplyRandomProperties(false);
		}

		if(pLoot->items.at(slotid).iRandomProperty!=NULL)
		{
			item->SetRandomProperty(pLoot->items.at(slotid).iRandomProperty->ID);
			item->ApplyRandomProperties(false);
		}
		else if(pLoot->items.at(slotid).iRandomSuffix != NULL)
		{
			item->SetRandomSuffix(pLoot->items.at(slotid).iRandomSuffix->id);
			item->ApplyRandomProperties(false);
		}
	}

	if( player->GetItemInterface()->SafeAddItem(item,slotresult.ContainerSlot, slotresult.Slot) )
	{
		player->GetSession()->SendItemPushResult(item,false,true,true,true,slotresult.ContainerSlot,slotresult.Slot,amt);
		if(item->GetProto()->Quality >= ITEM_QUALITY_UNCOMMON_GREEN)
			MyLog::yunyinglog->info("item-pick-player[%u][%s] greenitem["I64FMT"][%u] count[%u] from creature["I64FMT"][%s]", _player->GetLowGUID(), _player->GetName(), item->GetGUID(), itemid, amt, creatureguid, pCreature->GetCreatureName()->Name);

	}
	else
		delete item;

	pLoot->items.at(slotid).iItemsCount=0;

	// this gets sent to all looters
	if (!pLoot->items.at(slotid).ffa_loot)
	{
		pLoot->items.at(slotid).iItemsCount=0;

		// this gets sent to all looters
		MSG_S2C::stMsg_Loot_Removed Msg;
		Msg.lootSlot = slotid;
		Player * plr;
		for(LooterSet::iterator itr = pLoot->looters.begin(); itr != pLoot->looters.end(); ++itr)
		{
			if((plr = _player->GetMapMgr()->GetPlayer(*itr)))
				plr->GetSession()->SendPacket(Msg);
		}
	}
	else
	{
		pLoot->items.at(slotid).has_looted.insert(player->GetLowGUID());
	}

/*    WorldPacket idata(45);
    if(it->Class == ITEM_CLASS_QUEST)
    {
        uint32 pcount = player->GetItemInterface()->GetItemCount(it->ItemId, true);
		BuildItemPushResult(&idata, GetPlayer()->GetGUID(), ITEM_PUSH_TYPE_LOOT, amt, itemid, pLoot->items.at(slotid).iRandomProperty ? pLoot->items.at(slotid).iRandomProperty->ID : 0,0xFF,0,0xFFFFFFFF,pcount);
    }
    else
    {
		BuildItemPushResult(&idata, player->GetGUID(), ITEM_PUSH_TYPE_LOOT, amt, itemid, pLoot->items.at(slotid).iRandomProperty ? pLoot->items.at(slotid).iRandomProperty->ID : 0);
    }

	if(_player->InGroup())
		_player->GetGroup()->SendPacketToAll(&idata);
	else
		SendPacket(&idata);*/
}

void WorldSession::HandleLootRollOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;

	/* struct:

	{CLIENT} Packet: (0x02A0) CMSG_LOOT_ROLL PacketSize = 13
	|------------------------------------------------|----------------|
	|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
	|------------------------------------------------|----------------|
	|11 4D 0B 00 BD 06 01 F0 00 00 00 00 02		  |.M...........   |
	-------------------------------------------------------------------

	uint64 creatureguid
	uint21 slotid
	uint8  choice

	*/
	MSG_C2S::stMsg_Loot_Roll MsgRecv;packet >> MsgRecv;
	uint64 creatureguid = MsgRecv.creatureguid;
	uint32 slotid = MsgRecv.slotid;
	uint8 choice = MsgRecv.choice;
	uint32 mapid = MsgRecv.mapid;
	uint32 instanceid = MsgRecv.instanceid;

	LootRoll *li = NULL;

	uint32 guidtype = GET_TYPE_FROM_GUID(creatureguid);
	if (guidtype == HIGHGUID_TYPE_GAMEOBJECT) 
	{
		GameObject* pGO = _player->GetMapMgr()->GetGameObject((uint32)creatureguid);
		if (!pGO)
			return;
		if (slotid >= pGO->loot.items.size() || pGO->loot.items.size() == 0)
			return;
		//if (pGO->GetInfo() && pGO->GetInfo()->Type == GAMEOBJECT_TYPE_CHEST)
		li = pGO->loot.items[slotid].roll;
	} 
	else if (guidtype == HIGHGUID_TYPE_UNIT) 
	{
		// Creatures
		//Creature *pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(creatureguid));
		MapMgr* pMapMgr = sInstanceMgr.GetUniqueMapMgr( mapid, instanceid );
		if( !pMapMgr )
			return;
		Creature* pCreature = pMapMgr->GetCreature(GET_LOWGUID_PART(creatureguid));
		if (!pCreature)
			return;

		if (slotid >= pCreature->loot.items.size() || pCreature->loot.items.size() == 0)
			return;

		li = pCreature->loot.items[slotid].roll;
	} 
	else 
		return;

	if(!li)
		return;
	if( _player->GetMapId() != mapid || _player->GetInstanceID() != instanceid )
		choice = 3;

	li->PlayerRolled(_player, choice);
}

void WorldSession::HandleOpenItemOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stItem_Open MsgRecv;packet >> MsgRecv;
	ui8 slot = MsgRecv.slot;
	ui8 containerslot = MsgRecv.containerslot;

	Item *pItem = _player->GetItemInterface()->GetInventoryItem(containerslot, slot);
	if(!pItem)
		return;

	// gift wrapping handler
	if(pItem->GetUInt32Value(ITEM_FIELD_GIFTCREATOR) && pItem->wrapped_item_id)
	{
		ItemPrototype * it = ItemPrototypeStorage.LookupEntry(pItem->wrapped_item_id);

		pItem->SetUInt32Value(ITEM_FIELD_GIFTCREATOR,0);
		pItem->SetUInt32Value(OBJECT_FIELD_ENTRY,pItem->wrapped_item_id);
		pItem->wrapped_item_id=0;
		pItem->SetProto(it);

		if(it->Bonding==ITEM_BIND_ON_PICKUP)
			pItem->SetUInt32Value(ITEM_FIELD_FLAGS,1);
		else
			pItem->SetUInt32Value(ITEM_FIELD_FLAGS,0);

		if(it->MaxDurability)
		{
			pItem->SetUInt32Value(ITEM_FIELD_DURABILITY,it->MaxDurability);
			pItem->SetUInt32Value(ITEM_FIELD_MAXDURABILITY,it->MaxDurability);
		}

		pItem->m_isDirty=true;
		pItem->SaveToDB(containerslot,slot, false, NULL);
		return;
	}

	Lock *lock = dbcLock.LookupEntry( pItem->GetProto()->LockId );

	uint32 removeLockItems[5] = {0,0,0,0,0};

	if(lock) // locked item
	{
		for(int i=0;i<5;i++)
		{
			if(lock->locktype[i] == 1 && lock->lockmisc[i] > 0)
			{
				ui8 slot = _player->GetItemInterface()->GetInventorySlotById(lock->lockmisc[i]);
				if(slot != ITEM_NO_SLOT_AVAILABLE && slot >= INVENTORY_SLOT_ITEM_START && slot < INVENTORY_SLOT_ITEM_END)
				{
					removeLockItems[i] = lock->lockmisc[i];
				}
				else
				{
					_player->GetItemInterface()->BuildInventoryChangeError(pItem,NULL,INV_ERR_ITEM_LOCKED);
					return;
				}
			}
			else if(lock->locktype[i] == 2 && pItem->locked)
			{
				_player->GetItemInterface()->BuildInventoryChangeError(pItem,NULL,INV_ERR_ITEM_LOCKED);
				return;
			}
		}
		for(int i=0;i<5;i++)
			if(removeLockItems[i])
				_player->GetItemInterface()->RemoveItemAmt(removeLockItems[i],1);
	}

	// fill loot
	_player->SetLootGUID(pItem->GetGUID());
	if(!pItem->loot)
	{
		pItem->loot = new Loot;
		lootmgr.FillItemLoot(pItem->loot, pItem->GetEntry());
	}
	_player->SendLoot(pItem->GetGUID(), LOOT_DISENCHANTING);
}

void WorldSession::HandleCompleteCinematic(CPacketUsn& packet)
{
	// when a Cinematic is started the player is going to sit down, when its finished its standing up.
	_player->SetStandState(STANDSTATE_STAND);
};

void WorldSession::HandleResetInstanceOpcode(CPacketUsn& packet)
{
	sInstanceMgr.ResetSavedInstances(_player);
}

void EncodeHex(const char* source, char* dest, uint32 size)
{
	char temp[12];
	for(uint32 i = 0; i < size; ++i)
	{
		sprintf(temp, "%02X", source[i]);
		strcat(dest, temp);
	}
}

void DecodeHex(const char* source, char* dest, uint32 size)
{
	char temp;
	char* acc = const_cast<char*>(source);
	for(uint32 i = 0; i < size; ++i)
	{
		sscanf("%02X", &temp);
		acc = ((char*)&source[2]);
		strcat(dest, &temp);
	}
}

void WorldSession::HandleToggleCloakOpcode(CPacketUsn& packet)
{
	//////////////////////////
	//	PLAYER_FLAGS									   = 3104 / 0x00C20 / 0000000000000000000110000100000
	//																							 ^ 
	// This bit, on = toggled OFF, off = toggled ON.. :S

	//uint32 SetBit = 0 | (1 << 11);

	if(_player->HasFlag(PLAYER_FLAGS, PLAYER_FLAG_NOCLOAK))
		_player->RemoveFlag(PLAYER_FLAGS, PLAYER_FLAG_NOCLOAK);
	else
		_player->SetFlag(PLAYER_FLAGS, PLAYER_FLAG_NOCLOAK);
}

void WorldSession::HandleToggleHelmOpcode(CPacketUsn& packet)
{
	//////////////////////////
	//	PLAYER_FLAGS									   = 3104 / 0x00C20 / 0000000000000000000110000100000
	//																							  ^ 
	// This bit, on = toggled OFF, off = toggled ON.. :S

	//uint32 SetBit = 0 | (1 << 10);

	if(_player->HasFlag(PLAYER_FLAGS, PLAYER_FLAG_NOHELM))
		_player->RemoveFlag(PLAYER_FLAGS, PLAYER_FLAG_NOHELM);
	else
		_player->SetFlag(PLAYER_FLAGS, PLAYER_FLAG_NOHELM);
}

void WorldSession::HandleDungeonDifficultyOpcode(CPacketUsn& packet)
{
	MSG_C2S::stMsg_Dungeon_Difficulty MsgRecv;packet >> MsgRecv;
    uint32 data = MsgRecv.instanceType;

    if(_player->GetGroup() && _player->IsGroupLeader())
    {
		MSG_S2C::stMsg_Dungeon_Difficulty Msg;
		Msg.instanceType = data;

        _player->iInstanceType = data;
        sInstanceMgr.ResetSavedInstances(_player);

        Group * m_Group = _player->GetGroup();

		for(uint32 i = 0; i < m_Group->GetSubGroupCount(); ++i)
		{
			for(GroupMembersSet::iterator itr = m_Group->GetSubGroup(i)->GetGroupMembersBegin(); itr != m_Group->GetSubGroup(i)->GetGroupMembersEnd(); ++itr)
			{
				if((*itr)->m_loggedInPlayer)
				{
                    (*itr)->m_loggedInPlayer->iInstanceType = data;
					(*itr)->m_loggedInPlayer->GetSession()->SendPacket(Msg);
				}
			}
		}
    }
    else if(!_player->GetGroup())
    {
        _player->iInstanceType = data;
        sInstanceMgr.ResetSavedInstances(_player);
    }

#ifdef OPTIMIZED_PLAYER_SAVING
	_player->save_InstanceType();
#endif
}

void WorldSession::HandleSummonResponseOpcode(CPacketUsn& packet)
{
	if(!_player->m_summoner)
	{
		//SendNotification(NOTIFICATION_MESSAGE_NO_PERMISSION);
		return;
	}

	//if(_player->CombatStatus.IsInCombat())
	//	return;
	MapMgr* mgr = sInstanceMgr.GetInstance( _player->m_summonMapId, _player->m_summonInstanceId );
	if( !mgr )
	{
		_player->SendCastResult(0, SPELL_FAILED_INSTANCE_NOT_FOUND, 0, 0);
		return;
	}
	if( mgr->m_sunyouinstance && mgr->m_sunyouinstance->IsFull() )
	{
		_player->SendCastResult(0, SPELL_FAILED_INSTANCE_FULL, 0, 0);
		return;
	}

	_player->SafeTeleport(_player->m_summonMapId, _player->m_summonInstanceId, 
		_player->m_summonPos);

	_player->m_summoner = _player->m_summonInstanceId = _player->m_summonMapId = 0;
}

void WorldSession::HandleDismountOpcode(CPacketUsn& packet)
{
	MyLog::log->debug( "WORLD: Received CMSG_DISMOUNT"  );

	if( !_player->IsInWorld() || _player->GetTaxiState())
		return;

	if( _player->m_MountSpellId )
		_player->RemoveAura( _player->m_MountSpellId );
}

void WorldSession::HandleSetAutoLootPassOpcode(CPacketUsn& packet)
{
	MSG_C2S::stLoot_Set_Auto_Pass MsgRecv;packet>>MsgRecv;
	uint32 on = MsgRecv.on;

	if( _player->IsInWorld() )
		_player->BroadcastMessage("Auto loot passing is now %s.", on ? "on" : "off");

	_player->m_passOnLoot = (on!=0) ? true : false;
}

void WorldSession::HandleResurrectByOther(CPacketUsn& packet)
{
	MSG_C2S::stResurrectByOther MsgRecv;packet >> MsgRecv;
	if(MsgRecv.bAccept)
	{
		if(_player->isAlive() || !_player->IsInWorld())
			return;

		if(!_player->resurrector)
			return;

		sEventMgr.RemoveEvents(_player, EVENT_DELETE_TIMER);
		_player->ResurrectPlayer();
	}
	else
	{
		if(_player->isAlive() || !_player->IsInWorld())
			return;

		if(!_player->resurrector)
			return;
		
		_player->resurrector = 0;
	}
}
void WorldSession::HandleReliveReq(CPacketUsn& packet)
{
	MSG_C2S::stReliveReq MsgRecv;packet >> MsgRecv;
	if(_player->isAlive() || !_player->IsInWorld())
		return;

	if(_player->GetMapMgr()->GetMapInfo()->type != INSTANCE_NULL)
	{
		if(MsgRecv.type == MSG_C2S::stReliveReq::Relive_Item)
		{
			if( _player->IsInRaid() )
			{
				SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_RELIVE_ITEM_IN_RAID );
				return;
			}
			else if( _player->m_sunyou_instance && (_player->m_sunyou_instance->IsBattleGround() ) )
			{
				SendSystemNotifyEnum( MSG_S2C::stSystemNotifyEnum::SYSTEM_NOTIFY_RELIVE_ITEM_IN_BG );
				return;
			}

			if(_player->GetItemInterface()->GetItemCount(SPECIAL_ITEM_RESURRECT) < 1)
			{
				MSG_S2C::stResurrect_Failed Msg;
				SendPacket(Msg);
				return;
			}
			_player->m_bReliveItem = true;
			/*
			_player->m_resurrectMapId	= _player->GetMapId();
			_player->m_resurrectPosX	= _player->GetPositionX();
			_player->m_resurrectPosY	= _player->GetPositionY();
			_player->m_resurrectPosZ	= _player->GetPositionZ();
			_player->m_resurrectInstanceID = _player->GetInstanceID();
			*/
			_player->GetItemInterface()->RemoveItemAmt(SPECIAL_ITEM_RESURRECT, 1);
		}

		_player->resurrector = 0;
		sEventMgr.RemoveEvents(_player, EVENT_DELETE_TIMER);
		_player->ResurrectPlayer();
		return;
	}

	// Check death clock before resurrect they must wait for release to complete
	if( _player->GetDeathClock() + 5*60*1000 < getMSTime() )
	{
		MSG_S2C::stResurrect_Failed Msg;
		SendPacket(Msg);
		return;
	}

	if(MsgRecv.type == MSG_C2S::stReliveReq::Relive_Home)
	{
		_player->m_resurrectMapId	= _player->m_bind_mapid;
		_player->m_resurrectPosX	= _player->m_bind_pos_x;
		_player->m_resurrectPosY	= _player->m_bind_pos_y;
		_player->m_resurrectPosZ	= _player->m_bind_pos_z;
		_player->m_resurrectHealth	= 50;
		_player->m_resurrectMana	= 50;
	}
	else if(MsgRecv.type == MSG_C2S::stReliveReq::Relive_Item)
	{
		if(_player->GetItemInterface()->GetItemCount(SPECIAL_ITEM_RESURRECT) < 1)
		{
			MSG_S2C::stResurrect_Failed Msg;
			SendPacket(Msg);
			return;
		}
		_player->m_bReliveItem = true;
		_player->m_resurrectMapId	= _player->GetMapId();
		_player->m_resurrectPosX	= _player->GetPositionX();
		_player->m_resurrectPosY	= _player->GetPositionY();
		_player->m_resurrectPosZ	= _player->GetPositionZ();
		_player->m_resurrectInstanceID = _player->GetInstanceID();
		_player->m_resurrectHealth	= 100;
		_player->m_resurrectMana	= 100;
		_player->GetItemInterface()->RemoveItemAmt(SPECIAL_ITEM_RESURRECT, 1);
	}
	else
	{
		
	}
	
	_player->resurrector = 0;
	sEventMgr.RemoveEvents(_player, EVENT_DELETE_TIMER);
	_player->ResurrectPlayer();
}

void WorldSession::HandleServerTime( CPacketUsn& packet )
{
	MSG_S2C::stServerTime Msg;
	Msg.timeServer = (uint32)UNIXTIME;//time(NULL);
	SendPacket( Msg );
}

void WorldSession::HandleRecruitReq(CPacketUsn& packet)
{
	MSG_C2S::stRecruitReq msg;
	packet >> msg;
	_player->Recruit( msg.student );
}

void WorldSession::HandleRecruitReply(CPacketUsn& packet)
{
	MSG_C2S::stRecruitReply msg;
	packet >> msg;
	_player->ReplyRecruit( msg.accept );
}

void WorldSession::HandleTitleList(CPacketUsn& packet)
{
	_player->SendTitleList();
}

void WorldSession::HandleChooseTitle(CPacketUsn& packet)
{
	MSG_C2S::stChooseTitle MsgRecv;packet>>MsgRecv;	
	if(_player->m_Titles.find(MsgRecv.title_index)!=_player->m_Titles.end())
	{
		if(_player->GetUInt32Value(PLAYER_CHOSEN_TITLE))
		{
			stPlayerTitle* pEntry = PlayerTitleStorage.LookupEntry(_player->GetUInt32Value(PLAYER_CHOSEN_TITLE));
			if(pEntry)
			{
				_player->ApplyTitleMods(pEntry, false);
			}
		}
		_player->SetUInt32Value(PLAYER_CHOSEN_TITLE, MsgRecv.title_index);
		stPlayerTitle* pEntry = PlayerTitleStorage.LookupEntry(MsgRecv.title_index);
		if(pEntry)
		{
			_player->ApplyTitleMods(pEntry, true);
		}
	}
	else if( MsgRecv.title_index == 0 )
	{
		if(_player->GetUInt32Value(PLAYER_CHOSEN_TITLE))
		{
			stPlayerTitle* pEntry = PlayerTitleStorage.LookupEntry(_player->GetUInt32Value(PLAYER_CHOSEN_TITLE));
			if(pEntry)
			{
				_player->ApplyTitleMods(pEntry, false);
			}
			_player->SetUInt32Value(PLAYER_CHOSEN_TITLE, 0);
		}
	}
}

void WorldSession::HandleQueryPlayers( CPacketUsn& packet )
{
	MSG_C2S::stQueryPlayers msg;
	packet >> msg;
	objmgr.QueryPlayerProc( msg, this );
}


void WorldSession::HandleCreditAction( CPacketUsn& packet )
{
	if( _player->GetUInt32Value( PLAYER_FIELD_CREDIT_BANNED ) )
		return;

	MSG_C2S::stCreditActionReq req;
	packet >> req;

	MSG_S2C::stCreditActionAck ack;
	ack.is_restore = req.is_restore;
	ack.result = MSG_S2C::stCreditActionAck::CREDIT_ACTION_SYSTEM_BUSY;

	if( req.is_restore )
	{
		if( _player->GetCardPoints() >= req.yuanbao && req.yuanbao > 0 )
		{
			if( _player->RestoreCredit( req.yuanbao ) )
			{
				if( _player->SpendPoints( req.yuanbao, "restore credit card" ) )
				{
					ack.result = MSG_S2C::stCreditActionAck::CREDIT_ACTION_SUCCESS;

					char sql[256] = { 0 };
					sprintf( sql, "insert into credit_card_history values(%u, %d, %u)", GetAccountID(), (int)req.yuanbao, (uint32)UNIXTIME );
					RealmDatabase.Execute( sql );
				}
				else
					_player->UseCredit( req.yuanbao );
			}
		}
		else
		{
			ack.result = MSG_S2C::stCreditActionAck::CREDIT_ACTION_NOT_ENOUGH_YUANBAO;
		}
	}
	else
	{
		if( _player->UseCredit( req.yuanbao ) )
		{
			if( _player->AddCardPoints( req.yuanbao, Player::ADD_CARD_CREDIT ) )
			{
				ack.result = MSG_S2C::stCreditActionAck::CREDIT_ACTION_SUCCESS;

				char sql[256] = { 0 };
				sprintf( sql, "insert into credit_card_history values(%u, %d, %u)", GetAccountID(), -(int)req.yuanbao, (uint32)UNIXTIME );
				RealmDatabase.Execute( sql );
			}
			else
				_player->RestoreCredit( req.yuanbao );
		}
		else
		{
			ack.result = MSG_S2C::stCreditActionAck::CREDIT_ACTION_NOT_ENOUGH_YUANBAO;
		}
	}
	SendPacket( ack );
}

void WorldSession::HandleQueryCreditHistoryReq( CPacketUsn& packet )
{
	_player->QueryCreditHistory();
}

void WorldSession::HandleCreditApplyReq( CPacketUsn& packet )
{
	_player->CreditApply();
}

void WorldSession::HandleFallSpeedOnLand( CPacketUsn& packet )
{
	if( _player->m_sunyou_instance )
	{
		if( _player->m_sunyou_instance->GetCategory() == INSTANCE_CATEGORY_FAIRGROUND )
			return;
	}

	MSG_C2S::stFallSpeedOnLand msg;
	packet >> msg;
	if( msg.speed >= 24.f )
		_player->DealDamage( _player, _player->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) * ((msg.speed - 24.f) / 48.f), 0, 0, 0 );
}
