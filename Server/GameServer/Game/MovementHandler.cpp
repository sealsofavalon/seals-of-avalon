#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Move.h"
#include "../../SDBase/Protocol/C2S_Move.h"
#include "Platform/Common.h"
#include "../Common/Protocol/GS2DB.h"
#include "../Net/DSSocket.h"
#define SWIMMING_TOLERANCE_LEVEL -0.08f
#define MOVEMENT_PACKET_TIME_DELAY 500

#ifdef WIN32

#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")
#define DELTA_EPOCH_IN_USEC  11644473600000000ULL

uint32 TimeStamp()
{
	//return timeGetTime();

	FILETIME ft;
	uint64 t;
	GetSystemTimeAsFileTime(&ft);

	t = (uint64)ft.dwHighDateTime << 32;
	t |= ft.dwLowDateTime;
	t /= 10;
	t -= DELTA_EPOCH_IN_USEC;

	return uint32(((t / 1000000L) * 1000) + ((t % 1000000L) / 1000));
}

SUNYOU_INLINE uint32 mTimeStamp()
{
	return timeGetTime();
}

#else

uint32 TimeStamp()
{
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return (tp.tv_sec * 1000) + (tp.tv_usec / 1000);
}

SUNYOU_INLINE uint32 mTimeStamp()
{
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return (tp.tv_sec * 1000) + (tp.tv_usec / 1000);
}

#endif

void WorldSession::HandleMoveWorldportAckOpcode( CPacketUsn& packet )
{
	GetPlayer()->SetPlayerStatus(NONE);
	if(_player->IsInWorld())
	{
		// get outta here
		return;
	}
	MyLog::log->debug( "WORLD: got MSG_MOVE_WORLDPORT_ACK." );

	if(_player->m_CurrentTransporter && _player->GetMapId() != _player->m_CurrentTransporter->GetMapId())
	{
		/* wow, our pc must really suck. */
		Transporter * pTrans = _player->m_CurrentTransporter;
		float c_tposx = pTrans->GetPositionX() + _player->m_TransporterX;
		float c_tposy = pTrans->GetPositionY() + _player->m_TransporterY;
		float c_tposz = pTrans->GetPositionZ() + _player->m_TransporterZ;

		MSG_S2C::stMove_New_World Msg;
		Msg.map_id	= pTrans->GetMapId();
		Msg.pos_x	= c_tposx;
		Msg.pos_y	= c_tposy;
		Msg.pos_z	= c_tposz;
		Msg.orientation	= _player->GetOrientation();
		SendPacket(Msg);
	}
	else
	{
		_player->m_TeleportState = 2;
		_player->AddToWorld();
	}
}

void WorldSession::HandleMoveTeleportAckOpcode( CPacketUsn& packet )
{
	MSG_C2S::stMove_Teleport_Ack MsgRecv;packet >> MsgRecv;

	if(MsgRecv.guid == _player->GetGUID())
	{
		//if(sWorld.antihack_teleport && !(HasGMPermissions() && sWorld.no_antihack_on_gm) && _player->GetPlayerStatus() != TRANSFER_PENDING)
		//{
			/* we're obviously cheating */
			//sCheatLog.writefromsession(this, "Used teleport hack, disconnecting.");
		//	Disconnect();
		//	return;
		//}

//		if(sWorld.antihack_teleport && !(HasGMPermissions() && sWorld.no_antihack_on_gm) && _player->m_position.Distance2DSq(_player->m_sentTeleportPosition) > 625.0f)	/* 25.0f*25.0f */
//		{
//			// cheating.... :(
//			//sCheatLog.writefromsession(this, "Used teleport hack {2}, disconnecting.");
//			Disconnect();
//			return;
//		}

		//MyLog::log->debug( "WORLD: got MSG_MOVE_TELEPORT_ACK." );
		GetPlayer()->SetPlayerStatus(NONE);
		//if( GetPlayer()->m_rooted <= 0 )
		//	GetPlayer()->SetMovement(MOVE_UNROOT,5);
		_player->ResetHeartbeatCoords();

		//if(GetPlayer()->GetSummon() != NULL)		// move pet too
		//	GetPlayer()->GetSummon()->SetPosition((GetPlayer()->GetPositionX() + 2), (GetPlayer()->GetPositionY() + 2), GetPlayer()->GetPositionZ(), float(M_PI));
		if(_player->m_sentTeleportPosition.x != 999999.0f)
		{
			_player->m_position = _player->m_sentTeleportPosition;
			_player->m_sentTeleportPosition.ChangeCoords(999999.0f,999999.0f,999999.0f);
		}
	}

}

void _HandleBreathing(MovementInfo &movement_info, Player * _player, WorldSession * pSession)
{

	// no water breathing is required
	if( !sWorld.BreathingEnabled || _player->FlyCheat || _player->m_bUnlimitedBreath || !_player->isAlive() || _player->GodModeCheat )
	{
		// player is flagged as in water
		if( _player->m_UnderwaterState & UNDERWATERSTATE_SWIMMING  )
		{
			_player->m_UnderwaterState &= ~UNDERWATERSTATE_SWIMMING;
			if( _player->FlyCheat )
			{
				if(_player->m_lastMoveType != 2)
				{
					_player->m_lastMoveType = 2; // flying
					_player->ResetHeartbeatCoords();
				}
			}
			else
			{
				if(_player->m_lastMoveType != 1)
				{
					_player->m_lastMoveType = 1; // swimming
					_player->ResetHeartbeatCoords();
				}
			}
		}

		// player is flagged as under water
		if( _player->m_UnderwaterState & UNDERWATERSTATE_UNDERWATER )
		{
			_player->m_UnderwaterState &= ~UNDERWATERSTATE_UNDERWATER;
			MSG_S2C::stStartMirrorTimer Msg;
			Msg.type = MSG_S2C::BREATH_TIMER;
			Msg.CurValue	= _player->m_UnderwaterTime;
			Msg.MaxValue	= _player->m_UnderwaterMaxTime;
			Msg.BreathRegen	= -1;
			pSession->SendPacket(Msg);
		}

		// player is above water level
		if( pSession->m_bIsWLevelSet )
		{
			if( ( movement_info.z + _player->m_noseLevel ) > pSession->m_wLevel )
			{
				// remove druid aquatic form on land
				if( _player->getClass() == CLASS_DRUID )
					_player->RemoveAura( 1066 );

				// unset swim session water level
				pSession->m_bIsWLevelSet = false;
			}
		}

		return;
	}

	//player is swiming and not flagged as in the water
	if( movement_info.flags & MOVEFLAG_SWIMMING && !( _player->m_UnderwaterState & UNDERWATERSTATE_SWIMMING ) )
	{
		// dismount if mounted
		if( _player->m_MountSpellId )
			_player->RemoveAura( _player->m_MountSpellId );

		// get water level only if it was not set before
		if( !pSession->m_bIsWLevelSet )
		{
			// water level is somewhere below the nose of the character when entering water
			pSession->m_wLevel = movement_info.z + _player->m_noseLevel * 0.95f;
			pSession->m_bIsWLevelSet = true;
		}

		if( _player->FlyCheat )
		{
			if( _player->m_lastMoveType != 2 )
			{
				_player->m_lastMoveType = 2; // flying
				_player->ResetHeartbeatCoords();
			}
		}
		else
		{
			if( _player->m_lastMoveType != 1 )
			{
				_player->m_lastMoveType = 1; // swimming
				_player->ResetHeartbeatCoords();
			}
		}
		_player->m_UnderwaterState |= UNDERWATERSTATE_SWIMMING;
	}

	// player is not swimming and is not stationary and is flagged as in the water
	if( !( movement_info.flags & MOVEFLAG_SWIMMING ) && !( movement_info.flags & MOVEFLAG_MOVE_STOP ) && (_player->m_UnderwaterState & UNDERWATERSTATE_SWIMMING) )
	{
		// player is above water level
		if( ( movement_info.z + _player->m_noseLevel ) > pSession->m_wLevel )
		{
			// remove druid aquatic form on land
			if( _player->getClass() == CLASS_DRUID )
				_player->RemoveAura( 1066 );

			// unset swim session water level
			pSession->m_bIsWLevelSet = false;

			if( movement_info.flags & MOVEFLAG_AIR_SWIMMING )
			{
				if( _player->FlyCheat )
				{
					if( _player->m_lastMoveType != 2 )
					{
						_player->m_lastMoveType = 2; // flying
						_player->ResetHeartbeatCoords();
					}
				}
			}
			else
			{
				if( _player->m_lastMoveType != 0 )
				{
					_player->m_lastMoveType = 0; // walk or run on land
					_player->ResetHeartbeatCoords();
				}
			}
			_player->m_UnderwaterState &= ~UNDERWATERSTATE_SWIMMING;
		}
	}

	// player is flagged as in the water and is not flagged as under the water
	if( _player->m_UnderwaterState & UNDERWATERSTATE_SWIMMING && !( _player->m_UnderwaterState & UNDERWATERSTATE_UNDERWATER ) )
	{
		//the player is in the water and has gone under water, requires breath bar.
		if( ( movement_info.z + _player->m_noseLevel ) < pSession->m_wLevel )
		{
			_player->m_UnderwaterState |= UNDERWATERSTATE_UNDERWATER;
			MSG_S2C::stStartMirrorTimer Msg;
			Msg.type = MSG_S2C::BREATH_TIMER;
			Msg.CurValue	= _player->m_UnderwaterTime;
			Msg.MaxValue	= _player->m_UnderwaterMaxTime;
			Msg.BreathRegen	= -1;
			pSession->SendPacket(Msg);
		}
	}

	// player is flagged as in the water and is flagged as under the water
	if( _player->m_UnderwaterState & UNDERWATERSTATE_SWIMMING && _player->m_UnderwaterState & UNDERWATERSTATE_UNDERWATER )
	{
		//the player is in the water but their face is above water, no breath bar neeeded.
		if( ( movement_info.z + _player->m_noseLevel ) > pSession->m_wLevel )
		{
			_player->m_UnderwaterState &= ~UNDERWATERSTATE_UNDERWATER;
			MSG_S2C::stStartMirrorTimer Msg;
			Msg.type = MSG_S2C::BREATH_TIMER;
			Msg.CurValue	= _player->m_UnderwaterTime;
			Msg.MaxValue	= _player->m_UnderwaterMaxTime;
			Msg.BreathRegen	= 10;
			pSession->SendPacket(Msg);
		}
	}

	// player is flagged as not in the water and is flagged as under the water
	if( !( _player->m_UnderwaterState & UNDERWATERSTATE_SWIMMING ) && _player->m_UnderwaterState & UNDERWATERSTATE_UNDERWATER )
	{
		//the player is out of the water, no breath bar neeeded.
		if( ( movement_info.z + _player->m_noseLevel ) > pSession->m_wLevel )
		{
			_player->m_UnderwaterState &= ~UNDERWATERSTATE_UNDERWATER;
			MSG_S2C::stStartMirrorTimer Msg;
			Msg.type = MSG_S2C::BREATH_TIMER;
			Msg.CurValue	= _player->m_UnderwaterTime;
			Msg.MaxValue	= _player->m_UnderwaterMaxTime;
			Msg.BreathRegen	= 10;
			pSession->SendPacket(Msg);
		}
	}

}

void WorldSession::HandleMoveOP(MSG_C2S::stMove_OP* moveop)
{
	/*
	if( _player->IsIceBlocking() )
	{
		if( !_player->GetUInt64Value( PLAYER_FOOT ) )
			return;
	}
	*/
	// spell cancel on movement, for now only fishing is added
	Object * t_go = _player->m_SummonedObject;
	uint32 mstime_s;
	if (t_go)
	{
		if (t_go->GetEntry() == GO_FISHING_BOBBER)
			((GameObject*)t_go)->EndFishing(_player,true);
	}

	ui64 myguid = m_Movement->guid;
	//m_Movement->move.transGuid = 0;
	*m_Movement = *moveop;
	m_Movement->guid = myguid;
	m_Movement->wProNO = MSG_S2C::MOVE_OP;
	// 	packet >> m_Movement->moveOP;
	// 	packet >> m_Movement->move;
	MovementInfo& movement_info = m_Movement->move;

	if( ( (movement_info.flags & MOVEFLAG_MOVING_MASK) || (movement_info.flags & MOVEFLAG_JUMPING) || (movement_info.flags & MOVEFLAG_STRAFING_MASK) ) && _player->GetCurrentSpell() &&
		( _player->GetCurrentSpell()->getState() == SPELL_STATE_PREPARING || _player->GetCurrentSpell()->getState() == SPELL_STATE_CASTING ))
		_player->GetCurrentSpell()->cancel();

	/************************************************************************/
	/* Update player movement state                                         */
	/************************************************************************/
	if( m_Movement->moveOP == MOVE_STOP
		|| m_Movement->moveOP == MOVE_STOP_STRAFE
		|| m_Movement->moveOP == MOVE_STOP_TURN
		|| m_Movement->moveOP == MOVE_FALL_LAND
		|| ( m_Movement->moveOP == MOVE_SET_FACING && !(movement_info.flags & MOVEFLAG_MOVING_MASK) ) )
	{
		if( _player->m_isMoving )
		{
#ifdef _DEBUG
			//			printf("MOVING: FALSE (Packet %s)\n", LookupName( recv_data.GetOpcode(), g_worldOpcodeNames ) );
#endif
			mstime_s = getMSTime();
			_player->_SpeedhackCheck(mstime_s);
			_player->m_isMoving = false;
			_player->_startMoveTime = 0;
		}
	}
	else
	{
		if( !_player->m_isMoving )
		{
#ifdef _DEBUG
			//			printf("MOVING: TRUE (Packet %s)\n", LookupName( recv_data.GetOpcode(), g_worldOpcodeNames ) );
#endif
			mstime_s = getMSTime();
			_player->m_isMoving = true;
			_player->_startMoveTime = mstime_s;
			_player->_lastHeartbeatPosition = _player->GetPosition();
		}
	}

	/************************************************************************/
	/* Remove Emote State                                                   */
	/************************************************************************/
	if(_player->m_uint32Values[UNIT_NPC_EMOTESTATE])
		_player->SetUInt32Value(UNIT_NPC_EMOTESTATE,0);

	/************************************************************************/
	/* Make sure the co-ordinates are valid.                                */
	/************************************************************************/
	//if( !HasGMPermissions() )
	//{
	//	if( !((movement_info.y >= _minY) && (movement_info.y <= _maxY)) || !((movement_info.x >= _minX) && (movement_info.x <= _maxX)) )
	//	{
	//		_player->GetSession()->Disconnect();
	//		return;
	//	}
	//}
	/************************************************************************/
	/* Dump movement flags - Wheee!                                         */
	/************************************************************************/
#if 0
	printf("=========================================================\n");
	printf("Full movement flags: 0x%.8X\n", movement_info.flags);
	uint32 z, b;
	for(z = 1, b = 1; b < 32;)
	{
		if(movement_info.flags & z)
			printf("   Bit %u (0x%.8X or %u) is set!\n", b, z, z);

		z <<= 1;
		b+=1;
	}
	printf("=========================================================\n");
#endif

	/************************************************************************/
	/* Orientation dumping                                                  */
	/************************************************************************/
#if 0
	printf("Packet: 0x%03X (%s)\n", recv_data.GetOpcode(), LookupName( recv_data.GetOpcode(), g_worldOpcodeNames ) );
	printf("Orientation: %.10f\n", movement_info.orientation);
#endif

	/************************************************************************/
	/* Anti-Hack Checks                                                     */
	/************************************************************************/
	//if( !HasGMPermissions() && !_player->m_uint32Values[UNIT_FIELD_CHARM] && !_player->_heartbeatDisable)
	//{
		/************************************************************************/
		/* Anti-Teleport                                                        */
		/************************************************************************/
		//if(sWorld.antihack_teleport && _player->m_position.Distance2DSq(movement_info.x, movement_info.y) > 5625.0f
		//	&& _player->m_runSpeed < 50.0f && !_player->m_TransporterGUID)
		//{
			//sCheatLog.writefromsession(this, "Used teleport hack {3}, speed was %f", _player->m_runSpeed);
		//	Disconnect();
		//	return;
		//}
	//}

	/************************************************************************/
	/* Calculate the timestamp of the packet we have to send out            */
	/************************************************************************/
	uint32 mstime = mTimeStamp();
	int32 move_time;
	//if(m_clientTimeDelay == 0)
	//	m_clientTimeDelay = mstime - movement_info.time;

	/************************************************************************/
	/* Copy into the output buffer.                                         */
	/************************************************************************/
	if(_player->m_inRangePlayers.size())
	{
		//move_time = (movement_info.time - (mstime - m_clientTimeDelay)) + MOVEMENT_PACKET_TIME_DELAY + mstime;

		/************************************************************************/
		/* Distribute to all inrange players.                                   */
		/************************************************************************/
		static ByteBuffer data( 256 );
		

		for(set<Player*>::iterator itr = _player->m_inRangePlayers.begin(); itr != _player->m_inRangePlayers.end(); ++itr)
		{
			Player* plr = (*itr);
			//m_Movement->move.time = uint32(move_time) + plr->GetSession()->m_clientTimeDelay;

			data.clear();
			data << uint8( 0 );
			data << m_Movement->guid;
			data << m_Movement->moveOP;
			data << m_Movement->move;
			data.put( 0, (uint8)data.wpos() );

			plr->GetSession()->PushMovement( &data );
		}
	}

	/************************************************************************/
	/* Falling damage checks                                                */
	/************************************************************************/

	if( _player->blinked )
	{
		_player->blinked = false;
		_player->m_fallDisabledUntil = UNIXTIME + 5;
		_player->DelaySpeedHack( 5000 );
	}
	else
	{
		if( m_Movement->moveOP == MOVE_FALL_LAND )
		{
			// player has finished falling
			//if _player->z_axisposition contains no data then set to current position
			if( !_player->z_axisposition )
				_player->z_axisposition = movement_info.z;

			// calculate distance fallen
			uint32 falldistance = float2int32( _player->z_axisposition - movement_info.z );

			/*if player is a rogue or druid(in cat form), then apply -17 modifier to fall distance.
			these checks need improving, low level rogue/druid should not receive this benefit*/
			if( ( _player->getClass() == CLASS_ROGUE ) || ( _player->GetShapeShift() == FORM_CAT ) )
			{
				if( falldistance > 17 )
					falldistance -=17;
				else
					falldistance = 1;
			}

			//checks that player has fallen more than 12 units, otherwise no damage will be dealt
			//falltime check is also needed here, otherwise sudden changes in Z axis position, such as using !recall, may result in death
			/*
			if( _player->isAlive() && !_player->GodModeCheat && falldistance > 12 && ( UNIXTIME >= _player->m_fallDisabledUntil ) && movement_info.FallTime > 1000 )
			{
				// 1.7% damage for each unit fallen on Z axis over 13
				uint32 health_loss = float2int32( float( _player->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) * ( ( falldistance - 12 ) * 0.017 ) ) );

				if( health_loss >= _player->GetUInt32Value( UNIT_FIELD_HEALTH ) )
					health_loss = _player->GetUInt32Value( UNIT_FIELD_HEALTH );

				_player->SendEnvironmentalDamageLog( _player->GetGUID(), DAMAGE_FALL, health_loss );
				_player->DealDamage( _player, health_loss, 0, 0, 0 );

				_player->RemoveStealth(); // Fall Damage will cause stealthed units to lose stealth.
			}
			*/
			_player->z_axisposition = 0.0f;
		}
		else
			//whilst player is not falling, continuosly update Z axis position.
			//once player lands this will be used to determine how far he fell.
			if( !( movement_info.flags & MOVEFLAG_FALLING ) )
				_player->z_axisposition = movement_info.z;
	}

	/************************************************************************/
	/* Transporter Setup                                                    */
	/************************************************************************/
// 	if(!_player->m_lockTransportVariables)
// 	{
// 		if(_player->m_TransporterGUID && !movement_info.transGuid)
// 		{
// 			/* we left the transporter we were on */
// 			if(_player->m_CurrentTransporter)
// 			{
// 				_player->m_CurrentTransporter->RemovePlayer(_player);
// 				_player->m_CurrentTransporter = NULL;
// 			}
// 
// 			_player->m_TransporterGUID = 0;
// 			_player->ResetHeartbeatCoords();
// 		}
// 		else if(movement_info.transGuid)
// 		{
// 			if(!_player->m_TransporterGUID)
// 			{
// 				/* just walked into a transport */
// 				if(_player->IsMounted())
// 					_player->RemoveAura(_player->m_MountSpellId);
// 
// 				_player->m_CurrentTransporter = objmgr.GetTransporter(GUID_LOPART(movement_info.transGuid));
// 				if(_player->m_CurrentTransporter)
// 					_player->m_CurrentTransporter->AddPlayer(_player);
// 
// 				/* set variables */
// 				_player->m_TransporterGUID = movement_info.transGuid;
// 				_player->m_TransporterUnk = movement_info.transTime;
// 				_player->m_TransporterX = movement_info.transX;
// 				_player->m_TransporterY = movement_info.transY;
// 				_player->m_TransporterZ = movement_info.transZ;
// 			}
// 			else
// 			{
// 				/* no changes */
// 				_player->m_TransporterUnk = movement_info.transTime;
// 				_player->m_TransporterX = movement_info.transX;
// 				_player->m_TransporterY = movement_info.transY;
// 				_player->m_TransporterZ = movement_info.transZ;
// 			}
// 		}
// 		/*float x = movement_info.x - movement_info.transX;
// 		float y = movement_info.y - movement_info.transY;
// 		float z = movement_info.z - movement_info.transZ;
// 		Transporter* trans = _player->m_CurrentTransporter;
// 		if(trans) sChatHandler.SystemMessageToPlr(_player, "Client t pos: %f %f\nServer t pos: %f %f   Diff: %f %f", x,y, trans->GetPositionX(), trans->GetPositionY(), trans->CalcDistance(x,y,z), trans->CalcDistance(movement_info.x, movement_info.y, movement_info.z));*/
// 	}

	/************************************************************************/
	/* Anti-Speed Hack Checks                                               */
	/************************************************************************/



	/************************************************************************/
	/* Breathing System                                                     */
	/************************************************************************/
	//_HandleBreathing(movement_info, _player, _player->GetSession());

	/************************************************************************/
	/* Remove Spells                                                        */
	/************************************************************************/
	_player->RemoveAurasByInterruptFlag(AURA_INTERRUPT_ON_MOVEMENT);

	if((movement_info.x < -10000) || (movement_info.x > 10000))
		return;

	/************************************************************************/
	/* Update our position in the server.                                   */
	/************************************************************************/
	if( _player->m_CurrentCharm )
		_player->m_CurrentCharm->SetPosition(movement_info.x, movement_info.y, movement_info.z, movement_info.orientation);
	else
	{
		if(!_player->m_CurrentTransporter)
		{
			if( !_player->SetPosition(movement_info.x, movement_info.y, movement_info.z, movement_info.orientation) )
			{
				_player->SetUInt32Value(UNIT_FIELD_HEALTH, 0);
				_player->KillPlayer();
			}
		}
		else
		{
			_player->SetPosition(movement_info.x, movement_info.y, movement_info.z,
				movement_info.orientation, false);
		}
	}
}
void WorldSession::HandleMovementOpcodes( CPacketUsn& packet )
{
	if(!_player->IsInWorld() || _player->m_uint32Values[UNIT_FIELD_CHARMEDBY] || _player->GetPlayerStatus() == TRANSFER_PENDING || _player->GetTaxiState())
		return;

	if(_player->GetUInt64Value(PLAYER_FOOT))
	{
		//MyLog::log->alert("player[%s] cann't move who is riding on other player", _player->GetName());
		return;
	}
	_player->RemoveFlag(PLAYER_FLAGS, PLAYER_FLAG_AFK);
	_player->m_lastShotTime = (uint32)UNIXTIME;

	/************************************************************************/
	/* Make sure the packet is the correct size range.                      */
	/************************************************************************/
	/************************************************************************/
	/* Read Movement Data Packet                                            */
	/************************************************************************/

	MSG_C2S::stMove_OP moveop;
	packet >> moveop;
	moveop.move.x = finiteAlways(moveop.move.x);
	moveop.move.y = finiteAlways(moveop.move.y);
	moveop.move.z = finiteAlways(moveop.move.z);
	moveop.move.orientation = finiteAlways(moveop.move.orientation);
	HandleMoveOP(&moveop);

	if( !_player )
		return;

	if(_player->GetUInt64Value(PLAYER_HEAD))
	{
		Player* player = objmgr.GetPlayer(_player->GetUInt64Value(PLAYER_HEAD));
		if(!player)
		{
			_player->SetUInt64Value( PLAYER_HEAD, 0 );
			//MyLog::log->debug("player[%s] not found rider[IFM64]", _player->GetName(), _player->GetUInt64Value(PLAYER_HEAD) );
			return;
		}
		player->GetSession()->HandleMoveOP(&moveop);
	}
}

void WorldSession::HandleMoveTimeSkippedOpcode( CPacketUsn& packet )
{

}

void WorldSession::HandleMoveNotActiveMoverOpcode( CPacketUsn& packet )
{

}


void WorldSession::HandleSetActiveMoverOpcode( CPacketUsn& packet )
{
	// set current movement object
	MSG_C2S::stMove_Set_Acive_Mover MsgRecv;packet >> MsgRecv;

	if(MsgRecv.guid != m_Movement->guid)
	{
		// make sure the guid is valid and we aren't cheating
		if( !(_player->m_CurrentCharm && _player->m_CurrentCharm->GetGUID() == MsgRecv.guid) &&
			!(_player->GetGUID() == MsgRecv.guid) )
		{
			// cheater!
			return;
		}

		// generate wowguid
		if(MsgRecv.guid != 0)
			m_Movement->guid = MsgRecv.guid;
		else
			m_Movement->guid = _player->GetGUID();

		// set up to the movement packet
	}
}

void WorldSession::HandleMoveSplineCompleteOpcode(CPacketUsn& packet)
{

}

void WorldSession::HandleMountSpecialAnimOpcode(CPacketUsn& packet)
{
	MSG_S2C::stMove_Mount_Special_Anim Msg;
	Msg.guid = _player->GetGUID();
	_player->SendMessageToSet(Msg, true);
}

void WorldSession::HandleWorldportOpcode(CPacketUsn& packet)
{
	MSG_C2S::stMove_World_TelePort MsgRecv;packet >> MsgRecv;

	if(!_player->IsInWorld())
		return;

	if(!HasGMPermissions())
	{
		//SendNotification("You do not have permission to use this function.");
		return;
	}

	LocationVector vec(MsgRecv.x,MsgRecv.y,MsgRecv.z,MsgRecv.o);
	_player->SafeTeleport(MsgRecv.mapid,0,vec);
}

void WorldSession::HandleTeleportToUnitOpcode(CPacketUsn& packet)
{
	Unit * target;

	if(!_player->IsInWorld())
		return;

	if(!HasGMPermissions())
	{
		//SendNotification("You do not have permission to use this function.");
		return;
	}

	if( (target = _player->GetMapMgr()->GetUnit(_player->GetSelection())) == NULL )
		return;

	_player->SafeTeleport(_player->GetMapId(), _player->GetInstanceID(), target->GetPosition());
}

void WorldSession::HandleTeleportCheatOpcode(CPacketUsn& packet)
{
	MSG_C2S::stMove_TelePort_Cheat MsgRecv;packet >> MsgRecv;
	LocationVector vec;
	if(!HasGMPermissions())
	{
		//SendNotification("You do not have permission to use this function.");
		return;
	}

	vec.ChangeCoords(MsgRecv.x,MsgRecv.y,MsgRecv.z,MsgRecv.o);
	_player->SafeTeleport(_player->GetMapId(),_player->GetInstanceID(),vec);
}
//
//void MovementInfo::init(WorldPacket & data)
//{
//	transGuid = 0;
//	unk13 = 0;
//	data >> flags >> unk_230 >> time;
//	data >> x >> y >> z >> orientation;
//
//	if (flags & MOVEFLAG_TAXI)
//	{
//		data >> transGuid >> transX >> transY >> transZ >> transO >> transTime;
//	}
//	if (flags & MOVEFLAG_SWIMMING)
//	{
//		data >> s_pitch;
//	}
//	if (flags & MOVEFLAG_FALLING || flags & MOVEFLAG_JUMPING)
//	{
//		data >> FallTime >> unk8 >> unk9 >> unk10;
//	}
//	if (flags & MOVEFLAG_SPLINE_MOVER)
//	{
//		data >> u_unk1;
//	}
//
//	data >> unklast;
//	if(data.rpos() != data.wpos())
//	{
//		if(data.rpos() + 4 == data.wpos())
//			data >> unk13;
//		else
//			MyLog::log->debug("Extra bits of movement packet left");
//	}
//}
//
//void MovementInfo::write(WorldPacket & data)
//{
//	data << flags << unk_230 << getMSTime();
//
//	data << x << y << z << orientation;
//
//	if (flags & MOVEFLAG_TAXI)
//	{
//		data << transGuid << transX << transY << transZ << transO << transTime;
//	}
//	if (flags & MOVEFLAG_SWIMMING)
//	{
//		data << s_pitch;
//	}
//	if (flags & MOVEFLAG_FALLING)
//	{
//		data << FallTime << unk8 << unk9 << unk10;
//	}
//	if (flags & MOVEFLAG_SPLINE_MOVER)
//	{
//		data << u_unk1;
//	}
//	data << unklast;
//	if(unk13)
//		data << unk13;
//}

void WorldSession::HandleMountInviteOpcode( CPacketUsn& packet )
{
	/*
	if( _player->GetFloatValue( OBJECT_FIELD_SCALE_X ) > 1.f || _player->GetFloatValue( OBJECT_FIELD_SCALE_X ) < 1.f )
	{
		SendNotification( "变大变小状态下不可邀请别人乘骑" );
		return;
	}
	*/
	MSG_C2S::stMount_Invite MsgRecv;
	packet>>MsgRecv;

	Player * player = NULL;
	player = objmgr.GetPlayer(MsgRecv.player_guid);

	if ( player == NULL)
	{
		SendMountResult(_player, "", MSG_S2C::MOUNT_RESULT_NOT_FOUND);
		return;
	}
	/*
	if( player->GetFloatValue( OBJECT_FIELD_SCALE_X ) > 1.f || player->GetFloatValue( OBJECT_FIELD_SCALE_X ) < 1.f )
	{
		SendNotification( "对方正在变大变小状态下不可邀请乘骑" );
		return;
	}
	*/
	string player_name =player->GetName();
	//距离过远
	if(player->GetDistance2dSq(_player) > 40.0f)
	{
		SendMountResult(_player, player_name, MSG_S2C::MOUNT_RESULT_TOO_FAR);
		return;
	}
	MyLog::log->debug("[%s] invite [%s] for ride", _player->GetNameString()->c_str(), player_name.c_str());

	uint32 entry = _player->GetShapeShift();
	//是否是变身状态
	if( !entry )
	{
		MyLog::log->alert("[%s] wanted to invite other ride without shapeshift", _player->GetNameString()->c_str() );
		return;
	}

	if(_player->HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_SHAPE_CANTMOUNT))
		return;
	//是否可以骑乘
// 	CreatureProto* proto = CreatureProtoStorage.LookupEntry(entry);
// 	if(!proto)
// 		return;
// 	if(!proto->ride)
// 	{
// 		SendMountResult(_player, "", MSG_S2C::MOUNT_RESULT_CANNOTRIDE);
// 		return;
// 	}
	//是否已经骑乘
	if( _player->GetUInt64Value(PLAYER_FOOT))
	{
		MyLog::log->alert("[%s] wanted to invite other ride who has already be ride", _player->GetNameString()->c_str() );
		return;
	}

	if (player == _player)
	{
		SendMountResult(_player, player_name, MSG_S2C::MOUNT_RESULT_NOT_FOUND);
		return;
	}

	//对方变身了
	if( player->GetShapeShift())
	{
		SendMountResult(_player, player_name, MSG_S2C::MOUNT_RESULT_TARGET_SHAPESHIFT);
		return;
	}

	//对方有坐骑
	if( player->GetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID))
	{
		SendMountResult(_player, player_name, MSG_S2C::MOUNT_RESULT_ALREADY_RIDE);
		return;
	}

	//对方已经骑乘了
	if( player->GetUInt64Value(PLAYER_FOOT) )
	{
		SendMountResult(_player, player_name, MSG_S2C::MOUNT_RESULT_ALREADY_RIDE);
		return;
	}

	if( player->GetUInt64Value(PLAYER_HEAD))
	{
		SendMountResult(_player, player_name, MSG_S2C::MOUNT_RESULT_ALREADY_IN_RIDE);
		return;
	}

	if ( player->HasBeenInvited() )
	{
		SendMountResult(_player, player_name, MSG_S2C::MOUNT_RESULT_BUSY);
		return;
	}

	player->SetInviter(_player->GetLowGUID());
	MSG_S2C::stMount_Invite Msg;
	Msg.player_name = GetPlayer()->GetName();
	Msg.player_guid = GetPlayer()->GetGUID();
	player->GetSession()->SendPacket(Msg);
}

void WorldSession::HandleMountAcceptOpcode( CPacketUsn& packet )
{
	Player *player = objmgr.GetPlayer(_player->GetInviter());
	if ( !player )
	{
		_player->SetInviter(0);
		return;
	}

	player->SetInviter(0);
	_player->SetInviter(0);
	//距离过远
	if(player->GetDistance2dSq(_player) > 40.0f)
	{
		SendMountResult(player, _player->GetName(), MSG_S2C::MOUNT_RESULT_TOO_FAR);
		return;
	}

	MyLog::log->debug("[%s] accepted [%s] mount invite", _player->GetName(), player->GetName());

	if(_player->GetShapeShift())
	{
		MyLog::log->alert("[%s] cann't ride in shapeshift", _player->GetNameString()->c_str() );
		SendMountResult(player, _player->GetName(), MSG_S2C::MOUNT_RESULT_TARGET_SHAPESHIFT);
		return;
	}

	uint32 entry = player->GetShapeShift();
	if(!entry)
	{
		MyLog::log->alert("[%s] cann't invite other to ride without shapeshift", player->GetNameString()->c_str() );
		SendMountResult(_player, player->GetName(), MSG_S2C::MOUNT_RESULT_TARGET_SHAPESHIFT);
		return;
	}

	if(player->GetUInt64Value(PLAYER_HEAD))
	{
		SendMountResult(_player, player->GetName(), MSG_S2C::MOUNT_RESULT_ALREADY_RIDE);
		return;
	}

	//是否可以骑乘
	CreatureProto* proto = CreatureProtoStorage.LookupEntry(entry);
	if(!proto)
		return;
	if(!proto->ride)
	{
		SendMountResult(_player, "", MSG_S2C::MOUNT_RESULT_CANNOTRIDE);
		return;
	}

	_player->SetUInt64Value(PLAYER_FOOT, player->GetGUID());
	player->SetUInt64Value(PLAYER_HEAD, _player->GetGUID());

	MSG_S2C::stMount_Accept Msg;
	Msg.player_name = _player->GetName();
	player->GetSession()->SendPacket(Msg);
}

void WorldSession::HandleMountDeclineOpcode( CPacketUsn& packet )
{
	Player *player = objmgr.GetPlayer(_player->GetInviter());
	if ( !player )
	{
		_player->SetInviter(0);
		return;
	}

	MyLog::log->debug("[%s] declined [%s] mount invite", _player->GetName(), player->GetName());
	player->SetInviter(0);
	_player->SetInviter(0);

	MSG_S2C::stMount_Decline Msg;
	Msg.player_name = _player->GetName();
	player->GetSession()->SendPacket(Msg);
}

void WorldSession::HandleMountDisbandOpcode( CPacketUsn& packet )
{
	MyLog::log->debug("[%s] disband mount", _player->GetName());
	uint64 guid = _player->GetUInt64Value(PLAYER_HEAD);
	if(!guid)
	{
		guid = _player->GetUInt64Value(PLAYER_FOOT);
		if(!guid)
		{
			SendMountResult(_player, "", MSG_S2C::MOUNT_RESULT_NOT_RIDE);
			return;
		}
	}
	_player->SetUInt64Value(PLAYER_HEAD, 0);
	_player->SetUInt64Value(PLAYER_FOOT, 0);

	Player* player = objmgr.GetPlayer(guid);
	if(!player)
	{
		MyLog::log->alert("disband mount player - [%u] not found!", guid);
		return;
	}
	player->SetUInt64Value(PLAYER_HEAD, 0);
	player->SetUInt64Value(PLAYER_FOOT, 0);
	SendMountResult(_player, "", MSG_S2C::MOUNT_RESULT_DISBAND);
	player->GetSession()->SendMountResult(player, _player->GetName(), MSG_S2C::MOUNT_RESULT_DISBAND);
}

void WorldSession::SendMountResult(Player *pPlayer, const std::string& name, uint32 err)
{
	MSG_S2C::stMount_Result Msg;
	Msg.result = err;
	Msg.player_name = name;
	pPlayer->GetSession()->SendPacket(Msg);
}

void WorldSession::PushMovement( ByteBuffer* data )
{
	if( m_movmentMessage.buffer.size() + data->size() > 63000 )
		ProcessPendingMovements();

	m_movmentMessage.buffer.append( *data );
}

void WorldSession::PushMonstMovement( const MonsterMovementInfo& info )
{
	static ByteBuffer s_buf;
	s_buf.clear();

	s_buf << (uint8)0 << info;
	s_buf.put( 0, (uint8)s_buf.wpos() );

	if( m_monstMoveMessage.buffer.size() + s_buf.size() > 63000 )
		ProcessPendingMonstMovements();

	m_monstMoveMessage.buffer.append( s_buf );

	if( info.move_type == MOVE_TYPE_STOP )
		ProcessPendingMonstMovements();
}

void WorldSession::ProcessPendingMovements()
{
	if( m_movmentMessage.buffer.size() > 0 )
	{
		this->SendPacket( m_movmentMessage );
		m_movmentMessage.buffer.clear();
	}
}

void WorldSession::ProcessPendingMonstMovements()
{
	if( m_monstMoveMessage.buffer.size() > 0 )
	{
		this->SendPacket( m_monstMoveMessage );
		m_monstMoveMessage.buffer.clear();
	}
}
