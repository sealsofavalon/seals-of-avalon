#include "StdAfx.h"
#include "MultiLanguageStringMgr.h"

const char* _creature_say_list = NULL;
char _race_name[4][32];

LanguageString::LanguageString() : InstanceLuaObject<LanguageString>( "LanguageString" )
{
	memset( m_luaFunctions, 0, sizeof( m_luaFunctions ) );
}

LanguageString::~LanguageString()
{
	for( int i = 0; i < BuildString_Max; ++i )
		if( m_luaFunctions[i] )
			delete m_luaFunctions[i];
}

bool LanguageString::Init( language_t lan )
{
	std::string filename( "script/languages/" );
	filename += languages_luas[lan];
	FILE* fp = fopen( filename.c_str(), "r" );
	if( fp )
	{
		fclose( fp );
		m_LuaState->DoFile( filename.c_str() );

		for( int i = 0; i < BuildString_Max; ++i )
		{
			char str[256] = { 0 };
			sprintf( str, "build_string_%u", i );
			m_luaFunctions[i] = CreateLuaFunction<const char*>( str );
			assert( m_luaFunctions[i] );
			if( !m_luaFunctions[i] )
				return false;
		}
		_race_name[0][0] = 0;
		strcpy( _race_name[1], build_language_string( BuildString_CommonRace, 1 ) );
		strcpy( _race_name[2], build_language_string( BuildString_CommonRace, 2 ) );
		strcpy( _race_name[3], build_language_string( BuildString_CommonRace, 3 ) );
		return true;
	}
	else
		return false;
}

LuaFunction<const char*>* LanguageString::GetLuaFunction( build_string_t e )
{
	return m_luaFunctions[e];
}


MultiLanguageStringMgr* g_multi_langage_string_mgr = NULL;

void MultiLanguageStringMgr::Init()
{
	m_currentLanguage = LANGUAGE_CHINESE_SIMPLIFIED;
	for( int i = 0; i < LANGUAGE_MAX; ++i )
	{
		m_languages[i] = new LanguageString;
		if( !m_languages[i]->Init( (language_t)i ) )
		{
			delete m_languages[i];
			m_languages[i] = NULL;
		}
	}
}

static const char* __creature_say_list[] = {
	"creature_say_list_chinese_simplified",
	"creature_say_list_chinese_traditional",
	"creature_say_list_english",
	"creature_say_list_thai",
	"creature_say_list_vietnam",
	"creature_say_list_french",
	"creature_say_list_japanese",
	"creature_say_list_korean",
	"creature_say_list_german"
};

bool MultiLanguageStringMgr::SwitchLanguage( language_t lan )
{
	if( !m_languages[lan] )
		return false;
	else
	{
		m_currentLanguage = lan;
#ifdef ENABLE_MULTI_LANGUAGE
		_creature_say_list = __creature_say_list[lan];
#else
		_creature_say_list = "creature_say_list";
#endif
		return true;
	}
}

LuaFunction<const char*>* MultiLanguageStringMgr::GetLuaFunction( build_string_t e )
{
	return m_languages[m_currentLanguage]->GetLuaFunction( e );
}
