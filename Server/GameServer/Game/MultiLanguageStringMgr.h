#ifndef _MULTI_LANGUAGE_STRING_MGR_HEAD
#define _MULTI_LANGUAGE_STRING_MGR_HEAD

#ifdef _CODE_UTF8
#define ENABLE_MULTI_LANGUAGE
#endif

#include "LuaInterface.h"

enum build_string_t
{
	// SunyouTeamArena.cpp
	BuildString_ArenaStartFight								= 0,
	BuildString_ArenaEndWithoutEnemy						= 1,
	BuildString_ArenaWinnerNotification						= 2,
	BuildString_ArenaLoserNotification						= 3,
	BuildString_ArenaEndNotification						= 4,
	//////////////////////////////////////////////////////////////////////////
	// JingLianItem.cpp
	BuildString_Level9Broadcast								= 5,
	//////////////////////////////////////////////////////////////////////////
	// AuctionHouse.cpp
	BuildString_AuctionMailSubjectExpired					= 6,
	BuildString_AuctionMailSubjectSucceed					= 7,
	BuildString_AuctionMailSubjectDonationSucceed			= 8,
	BuildString_AuctionMailBodyDonationSucceed				= 9,
	BuildString_AuctionMailBodySucceedCashCurrency			= 10,
	BuildString_AuctionMailBodySucceedGold					= 11,
	BuildString_AuctionSystemMessageToBidder				= 12,
	BuildString_AuctionSystemMessageToOwner					= 13,
	BuildString_AuctionMailSubjectCancel					= 14,
	BuildString_AuctionMailBodyCancelTaxGold				= 15,
	BuildString_AuctionMailBodyCancelReturnGold				= 16,
	BuildString_AuctionMailBodyCancelTaxCashCurrency		= 17,
	BuildString_AuctionMailBodyCancelReturnCashCurrency		= 18,
	BuildString_AuctionSystemMessageLowPrice				= 19,
	BuildString_AuctionSystemMessageBuyOut					= 20,
	BuildString_AuctionSystemMessageAlreadyHighestPrice		= 21,
	BuildString_AuctionMailSubjectBidFail					= 22,
	BuildString_AuctionMailBodyBidFailCashCurrency			= 23,
	BuildString_AuctionMailBodyBidFailGold					= 24,
	BuildString_AuctionSystemMessageBuyOutLessThanBidPrice	= 25,
	//////////////////////////////////////////////////////////////////////////
	// CardSerialSystem.cpp
	BuildString_CardSerialSystemMailSubject					= 26,
	BuildString_CardSerialSystemMailBody					= 27,
	//////////////////////////////////////////////////////////////////////////
	// DonationSystem.cpp
	BuildString_DonationMailSubjectOnceGift					= 28,
	BuildString_DonationMailBodyOnceGift					= 29,
	BuildString_DonationMailSubjectAccumulationGift			= 30,
	BuildString_DonationMailBodyAccumulationGift			= 31,
	BuildString_DonationSystemMessageOnce					= 32,
	BuildString_DonationSystemMessageAccumulation			= 33,

	//////////////////////////////////////////////////////////////////////////
	// MailSystem.cpp
	BuildString_MailboxSystemReturn							= 34,

	//////////////////////////////////////////////////////////////////////////
	// CharacterHandler.cpp
	BuildString_CharacterHandlerSystemMessageChemi			= 35,

	//////////////////////////////////////////////////////////////////////////
	// Player.cpp
	BuildString_PlayerMailSubjectSystemGift					= 36,
	BuildString_PlayerMailBodySystemGift					= 37,
	BuildString_PlayerSystemMessageExpGift					= 38,
	BuildString_PlayerSystemMessageGoldGift					= 39,
	BuildString_PlayerMailSubjectBGGift						= 40,
	BuildString_PlayerMailSubjectArenaGift					= 41,
	BuildString_PlayerMailSubjectTitleGift					= 42,
	BuildString_PlayerMailSubjectInstanceGift				= 43,
	BuildString_PlayerMailSubjectGambleGift					= 44,
	BuildString_PlayerMailBodyBGGift						= 45,
	BuildString_PlayerMailBodyArenaGift						= 46,
	BuildString_PlayerMailBodyTitleGift						= 47,
	BuildString_PlayerMailBodyInstanceGift					= 48,
	BuildString_PlayerMailBodyGambleGift					= 49,
	BuildString_PlayerSystemMessageSystemGift				= 50,
	BuildString_PlayerSystemMessageBGGift					= 51,
	BuildString_PlayerSystemMessageArenaGift				= 52,
	BuildString_PlayerSystemMessageTitleGift				= 53,
	BuildString_PlayerSystemMessageInstanceGift				= 54,
	BuildString_PlayerSystemMessageGambleGift				= 55,
	BuildString_PlayerSystemMessageChenmiNotHealth			= 56,
	BuildString_PlayerSystemMessageChenmiTired				= 57,
	BuildString_PlayerSystemMessageChenmiNotify				= 58,
	BuildString_PlayerSystemMessageZodiacGift				= 59,
	BuildString_PlayerSystemMessageRecruitInShortTime		= 60,
	BuildString_PlayerSystemMessageStudentLevelOverTeacherLevel = 61,


	//////////////////////////////////////////////////////////////////////////
	// SpellHandler.cpp
	BuildString_SpellHandlerMuted							= 62,
	BuildString_SpellHandlerCannotUseInInstance				= 63,
	BuildString_SpellHandlerCannotFeedWithoutPet			= 64,
	BuildString_SpellHandlerPetCannotSpawn					= 65,
	BuildString_SpellHandlerPetCountLimited					= 66,

	//////////////////////////////////////////////////////////////////////////
	// Common
	BuildString_CommonRace									= 67,

	//////////////////////////////////////////////////////////////////////////
	// SunyouAdvancedBattleGround.cpp
	BuildString_ABGReport									= 68,

	//////////////////////////////////////////////////////////////////////////
	// SunyouBattleGround.cpp
	BuildString_BGWinnerReport								= 69,
	BuildString_BGLoserReport								= 70,

	//////////////////////////////////////////////////////////////////////////
	// World.spp
	BuildString_WorldServerShutdownInSeconds				= 71,
	BuildString_WorldServerShutdownInMinutes				= 72,

	//////////////////////////////////////////////////////////////////////////
	// ScriptMgr.cpp
	BuildString_ScriptAuction								= 73,
	BuildString_ScriptHomeBind								= 74,
	BuildString_ScriptBank									= 75,
	BuildString_ScriptInputGiftSerial						= 76,
	BuildString_ScriptCreateGuild							= 77,
	BuildString_ScriptQueryGuild							= 78,
	BuildString_ScriptUpgradeGuild							= 79,
	BuildString_ScriptDisbandTeacherStudent					= 80,
	BuildString_ScriptTeleport2Ren							= 81,
	BuildString_ScriptTeleport2Wu							= 82,
	BuildString_ScriptTeleport2Yao							= 83,

	//////////////////////////////////////////////////////////////////////////
	// lua string
	BuildString_LuaArathiBanner0							= 84,
	BuildString_LuaArathiBanner1							= 85,
	BuildString_LuaArathiBanner2							= 86,
	BuildString_LuaArathiBanner3							= 87,
	BuildString_LuaArathiBanner4							= 88,
	BuildString_LuaShaernaTeleportGossip					= 89,
	BuildString_LuaShaernaMobsHelpGossip					= 90,
	BuildString_LuaShaernaDieGossip							= 91,
	BuildString_LuaShaernaKillGossip						= 92,
	BuildString_LuaShaernaPoisonGossip						= 93,
	BuildString_LuaShaernaAOEGossip							= 94,
	BuildString_LuaShaernaIceBlastGossip					= 95,
	BuildString_LuaShaernaWelcomeGossip						= 96,
	BuildString_LuaHappyBearDanceAgain						= 97,
	BuildString_LuaHappyBearGiveGift						= 98,
	BuildString_LuaHappyBearFollow2Dance					= 99,
	BuildString_LuaHappyBearDefeated						= 100,
	BuildString_LuaHappyBearWelcomeGossip					= 101,
	BuildString_LuaHappyBearDanceFailed						= 102,
	BuildString_LuaHappyBearDanceWon						= 103,

	//////////////////////////////////////////////////////////////////////////
	// SunyouRaid.cpp
	BuildString_RaidBannerCaptrued							= 104,
	BuildString_RaidBannerDefended							= 105,
	BuildString_RaidBannerSudenStricken						= 106,

	//////////////////////////////////////////////////////////////////////////
	// Guild.cpp
	BuildString_GuildRankName0								= 107,
	BuildString_GuildRankName1								= 108,
	BuildString_GuildRankName2								= 109,
	BuildString_GuildRankName3								= 110,
	BuildString_GuildSetRankPermission						= 111,
	BuildString_GuildRankNotFound							= 112,
	BuildString_GuildCannotInviteOtherRace					= 113,
	BuildString_GuildInvitePermission						= 114,
	BuildString_GuildFull									= 115,
	BuildString_GuildPermission								= 116,
	BuildString_GuildMasterCannotQuit						= 117,
	BuildString_GuildUpgrade								= 118,
	BuildString_GuildLevelCap								= 119,
	BuildString_GuildUpgradeRequireItem						= 120,
	BuildString_GuildWarRunning								= 121,
	BuildString_GuildWarSelf								= 122,
	BuildString_GuildWarAlliance							= 123,
	BuildString_GuildNotFound								= 124,
	BuildString_GuildAllianceWaiting						= 125,
	BuildString_GuildAllianceSelf							= 126,
	BuildString_GuildAllianceWarRunning						= 127,
	BuildString_GuildAllianceAlreadyIs						= 128,
	BuildString_GuildAllianceLowLevel						= 129,
	BuildString_GuildYouAreNotTheLeader						= 130,
	BuildString_GuildAllianceTargetLeaderOffline			= 131,
	BuildString_GuildAllianceInviterDisband					= 132,
	BuildString_GuildAllianceExpire							= 133,

	//////////////////////////////////////////////////////////////////////////
	// Item.cpp
	BuildString_ItemExpireNotify							= 134,
	
	//////////////////////////////////////////////////////////////////////////
	// ChatHandler.cpp
	BuidlString_ChatHandlerBanned							= 135,
	BuidlString_ChatHandlerMuted							= 136,

	//////////////////////////////////////////////////////////////////////////
	// ScriptMgr.cpp
	BuildString_ScriptWhatDoYouNeed							= 137,

	BuildString_GuildFull_LevelUp							= 138,

	//////////////////////////////////////////////////////////////////////////
	// ShopMgr.cpp
	BuildString_ShopDiscountNotify							= 139,

	//////////////////////////////////////////////////////////////////////////
	// InstanceQueue.cpp
	BuildString_InstanceQueueBusy							= 140,
	BuildString_InstanceQueueLowLevel						= 141,
	BuildString_InstanceQueueHighLevel						= 142,
	BuildString_InstanceQueueRMBNeed						= 143,
	BuildString_InstanceQueueItemNeed						= 144,
	BuildString_InstanceQueueGoldNeed						= 145,
	BuildString_InstanceQueueEnterLimit						= 146,
	BuildString_InstanceQueueOtherRMBNeed					= 147,
	BuildString_InstanceQueueOtherItemNeed					= 148,
	BuildString_InstanceQueueOtherGoldNeed					= 149,
	BuildString_InstanceQueueMinPlayer						= 150,
	BuildString_InstanceQueueMaxPlayer						= 151,

	BuildString_PlayerAddPointDonate						= 152,
	BuildString_PlayerAddPointTrade							= 153,
	BuildString_PlayerAddPointCredit						= 154,

	BuildString_PlayerKillHostileGuildPlayer				= 155,
	BuildString_PlayerBeKilledByHostileGuildPlayer			= 156,

	//////////////////////////////////////////////////////////////////////////
	// lua script HappyFairground
	BuildString_FairgroundGiveMeAChange						= 157,
	BuildString_FairgroundExchange100Happycoin				= 158,
	BuildString_FairgroundExchange1000Happycoin				= 159,
	BuildString_FairgroundExchange5000Happycoin				= 160,
	BuildString_OnlyGuildLeaderCanLevelUpGuild				= 161,
	BuildString_GuildLevelUp								= 162,
	BuildString_GuildTrainerSpellWarrior					= 163,
	BuildString_GuildTrainerSpellMage						= 164,
	BuildString_GuildTrainerSpellBow						= 165,
	BuildString_GuildTrainerSpellPriest 					= 166,
	BuildString_GuildKillBossBonus							= 167,

	BuildString_ChatAfk										= 168,
	BuildString_ChatDnd										= 169,
	BuildString_PlayerMailSubjectGuildGift					= 170,
	BuildString_PlayerMailBodyGuildGift						= 171,
	BuildString_PlayerSystemMessageGuildGift				= 172,
	BuildString_Target_Guild_In_No_War						= 173,
	BuildString_Guild_Begin_No_War							= 174,
	BuildString_Guild_In_No_War								= 175,

	BuildString_Max,
};

class LanguageString : public InstanceLuaObject<LanguageString>
{
public:
	LanguageString();
	~LanguageString();
	bool Init( language_t lan );
	LuaFunction<const char*>* GetLuaFunction( build_string_t e );

private:
	LuaFunction<const char*>* m_luaFunctions[BuildString_Max];
};

class MultiLanguageStringMgr
{
public:
	void Init();
	bool SwitchLanguage( language_t lan );
	LuaFunction<const char*>* GetLuaFunction( build_string_t e );

private:
	LanguageString* m_languages[LANGUAGE_MAX];
	language_t m_currentLanguage;
};

extern MultiLanguageStringMgr* g_multi_langage_string_mgr;

inline const char* __build_language_string( build_string_t e ) {
	static std::string str;
	str = (*g_multi_langage_string_mgr->GetLuaFunction( e ))();
	return str.c_str();
}

template<typename P1>
inline const char* __build_language_string( build_string_t e, P1 p1 ) {
	static std::string str;
	str = (*g_multi_langage_string_mgr->GetLuaFunction( e ))( p1 );
	return str.c_str();
}

template<typename P1, typename P2>
inline const char* __build_language_string( build_string_t e, P1 p1, P2 p2 ) {
	static std::string str;
	str = (*g_multi_langage_string_mgr->GetLuaFunction( e ))( p1, p2 );
	return str.c_str();
}

template<typename P1, typename P2, typename P3>
inline const char* __build_language_string( build_string_t e, P1 p1, P2 p2, P3 p3 ) {
	static std::string str;
	str = (*g_multi_langage_string_mgr->GetLuaFunction( e ))( p1, p2, p3 );
	return str.c_str();
}

template<typename P1, typename P2, typename P3, typename P4>
inline const char* __build_language_string( build_string_t e, P1 p1, P2 p2, P3 p3, P4 p4 ) {
	static std::string str;
	str = (*g_multi_langage_string_mgr->GetLuaFunction( e ))( p1, p2, p3, p4 );
	return str.c_str();
}

template<typename P1, typename P2, typename P3, typename P4, typename P5>
inline const char* __build_language_string( build_string_t e, P1 p1, P2 p2, P3 p3, P4 p4, P5 p5 ) {
	static std::string str;
	str = (*g_multi_langage_string_mgr->GetLuaFunction( e ))( p1, p2, p3, p4, p5 );
	return str.c_str();
}

template<typename P1, typename P2, typename P3, typename P4, typename P5, typename P6>
inline const char* __build_language_string( build_string_t e, P1 p1, P2 p2, P3 p3, P4 p4, P5 p5, P6 p6 ) {
	static std::string str;
	str = (*g_multi_langage_string_mgr->GetLuaFunction( e ))( p1, p2, p3, p4, p5, p6 );
	return str.c_str();
}

template<typename P1, typename P2, typename P3, typename P4, typename P5, typename P6, typename P7>
inline const char* __build_language_string( build_string_t e, P1 p1, P2 p2, P3 p3, P4 p4, P5 p5, P6 p6, P7 p7 ) {
	static std::string str;
	str = (*g_multi_langage_string_mgr->GetLuaFunction( e ))( p1, p2, p3, p4, p5, p6, p7 );
	return str.c_str();
}

#define build_language_string __build_language_string

inline const char* quick_itoa( int n )
{
	static int index = 0;
	static char temp[10][32];

	itoa( n, temp[index%10], 10 );
	return temp[index++%10];
}

inline const char* quick_gold_s( int n )
{
	static int index = 0;
	static char temp[10][256];

	if( n < 100 )
		sprintf( temp[index%10], "%u<img:copper.png>", n%100 );
	else if( n < 10000 )
		sprintf( temp[index%10], "%u<img:silver.png>%u<img:copper.png>", (n%10000)/100, n%100 );
	else
		sprintf( temp[index%10], "%u<img:gold.png>%u<img:silver.png>%u<img:copper.png>", n/10000, (n%10000)/100, n%100 );

	return temp[index++%10];
}

extern char _race_name[4][32];

#endif // _MULTI_LANGUAGE_STRING_MGR_HEAD

