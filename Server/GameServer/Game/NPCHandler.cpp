#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Quest.h"
#include "../../SDBase/Protocol/S2C_Quest.h"
#include "../../SDBase/Protocol/C2S_Auction.h"
#include "../../SDBase/Protocol/S2C_Auction.h"
#include "../../SDBase/Protocol/S2C_Spell.h"
#include "QuestMgr.h"

trainertype trainer_types[TRAINER_TYPE_MAX] = 
{
{	"Warrior",			   0 },
{	"Paladin",			   0 },
{	"Rogue"  ,			   0 },
{	"Warlock",			   0 },
{	"Mage",				  0 },
{	"Shaman",				0 },
{	"Priest",				0 },
{	"Hunter",				0 },
{	"Druid",				 0 },
{	"Leatherwork",		   2 },
{	"Skinning",			  2 },
{	"Fishing",			   2 },
{	"First Aid",			 2 },
{	"Physician",			 2 },
{	"Engineer",			  2 },
{	"Weapon Master",		 0 },
};

bool CanTrainAt(Player * plr, Trainer * trn)
{
	if ( (trn->RequiredClass && plr->getClass() != trn->RequiredClass) ||
		 (trn->RequiredSkill && !plr->_HasSkillLine(trn->RequiredSkill)) ||
		 (trn->RequiredSkillLine && plr->_GetSkillLineCurrent(trn->RequiredSkill) < trn->RequiredSkillLine) )
	{
		return false;
	}
	
	return true;
}

//////////////////////////////////////////////////////////////
/// This function handles MSG_TABARDVENDOR_ACTIVATE:
//////////////////////////////////////////////////////////////
void WorldSession::HandleTabardVendorActivateOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stNPC_TabardVendor_Activate MsgRecv;packet >> MsgRecv;
	Creature *pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
	if(!pCreature) return;

	SendTabardHelp(pCreature);
}

void WorldSession::SendTabardHelp(Creature* pCreature)
{
	if(!_player->IsInWorld()) return;
	MSG_S2C::stNPC_TabardVendor_Activate Msg;
	Msg.guid	= pCreature->GetGUID();
	SendPacket( Msg );
}


//////////////////////////////////////////////////////////////
/// This function handles CMSG_BANKER_ACTIVATE:
//////////////////////////////////////////////////////////////
void WorldSession::HandleBankerActivateOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stNPC_Banker_Activate MsgRecv;packet >> MsgRecv;

	Creature *pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
	if(!pCreature) return;

	SendBankerList(pCreature);
}

void WorldSession::SendBankerList(Creature* pCreature)
{
	if(!_player->IsInWorld()) return;
	MSG_S2C::stNPC_Show_Bank Msg;
	if( pCreature )
		Msg.guid = pCreature->GetGUID();
	else
		Msg.guid = 0;
	SendPacket( Msg );
}

//////////////////////////////////////////////////////////////
/// This function handles CMSG_TRAINER_LIST
//////////////////////////////////////////////////////////////
//NOTE: we select prerequirements for spell that TEACHES you
//not by spell that you learn!
void WorldSession::HandleTrainerListOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	// Inits, grab creature, check.
	MSG_C2S::stNPC_Trainer_List MsgRecv;packet >> MsgRecv;
	Creature *train = GetPlayer()->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
	if(!train) return;

	SendTrainerList(train);
}

void WorldSession::SendTrainerList(Creature* pCreature)
{
	Trainer * pTrainer = pCreature->GetTrainer();
	//if(pTrainer == 0 || !CanTrainAt(_player, pTrainer)) return;
	if(pTrainer==0)
		return;

	if(!CanTrainAt(_player,pTrainer))
	{
		GossipMenu * pMenu;
		objmgr.CreateGossipMenuForPlayer(&pMenu,pCreature->GetGUID(),pTrainer->Cannot_Train_GossipTextId,_player);
		pMenu->SendTo(_player);
		return;
	}

	MSG_S2C::stNPC_Trainer_List Msg;
	TrainerSpell * pSpell;
	uint32 Spacer = 0;
	uint32 Count=0;
	uint8 Status;
	string Text;

	Msg.guid	= pCreature->GetGUID();
	Msg.TrainerType	= pTrainer->TrainerType;
	Msg.Id = pTrainer->nId;
	/*

	for(vector<TrainerSpell>::iterator itr = pTrainer->Spells.begin(); itr != pTrainer->Spells.end(); ++itr)
	{
		MSG_S2C::stNPC_Trainer_List::stTrainerSpell Spell;
		pSpell = &(*itr);
		Status = TrainerGetSpellStatus(pSpell);
		if( pSpell->pCastRealSpell != NULL )
			Spell.CastRealSpell_id = pSpell->pCastRealSpell->Id;
		else if( pSpell->pLearnSpell )
			Spell.LearnSpell_id = pSpell->pLearnSpell->Id;
		else
			continue;

		Spell.Status = Status;
		Spell.Cost = pSpell->Cost;
		Spell.IsProfession = uint32(pSpell->IsProfession);
		Spell.RequiredLevel = (pSpell->RequiredLevel);
		Spell.RequiredSkillLine = pSpell->RequiredSkillLine;
		Spell.RequiredSkillLineValue = pSpell->RequiredSkillLineValue;
		Spell.RequiredSpell = pSpell->RequiredSpell;
		Spell.name = pSpell->name;
		Msg.vSpells.push_back(Spell);
		++Count;
	}
	*/

	Msg.message = pTrainer->UIMessage;
	SendPacket(Msg);
}

void WorldSession::HandleTrainerBuySpellOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;

	MSG_C2S::stNPC_Trainer_Buy_Spell MsgRecv;packet >> MsgRecv;
	
	Creature *pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
	if(pCreature == 0) return;

	Trainer *pTrainer = pCreature->GetTrainer();
	if(pTrainer == 0 || !CanTrainAt(_player, pTrainer)) return;

	TrainerSpell * pSpell=NULL;
	for(vector<TrainerSpell>::iterator itr = pTrainer->Spells.begin(); itr != pTrainer->Spells.end(); ++itr)
	{
		if( ( itr->pCastRealSpell && itr->pCastRealSpell->Id == MsgRecv.TeachingSpellID ) ||
			( itr->pLearnSpell && itr->pLearnSpell->Id == MsgRecv.TeachingSpellID ) )
		{
			pSpell = &(*itr);
		}
	}
	
	if(pSpell == NULL)
		return;

	if(TrainerGetSpellStatus(pSpell) > 0) return;
	

	_player->ModCoin(-(int32)pSpell->Cost);

	if( pSpell->pCastSpell)
	{
		// Cast teaching spell on player
		pCreature->CastSpell(_player, pSpell->pCastSpell, true);
	}

	if( pSpell->pLearnSpell )
	{

		uint32 i;
		uint32 points_remaining=_player->GetUInt32Value(PLAYER_CHARACTER_POINTS2);
		for (i = 0; i < 3; i ++)
		{
			if( pSpell->pLearnSpell->Effect[i] == SPELL_EFFECT_SKILL )
			{
				skilllineentry *sk=dbcSkillLine.LookupEntry(pSpell->pLearnSpell->EffectMiscValue[i]);
				if (sk->type == SKILL_TYPE_PROFESSION)
				{
					if (!_player->_HasSkillLine(pSpell->pLearnSpell->EffectMiscValue[i]))
					{
						if ( points_remaining >= 2)
						{

							_player->ModCoin((int32)pSpell->Cost);
							return;
						}
					}
				}

			}
		}

		for( i = 0; i < 3; ++i)
		{
			if(pSpell->pLearnSpell->Effect[i] == SPELL_EFFECT_PROFICIENCY || pSpell->pLearnSpell->Effect[i] == SPELL_EFFECT_LEARN_SPELL ||
				pSpell->pLearnSpell->Effect[i] == SPELL_EFFECT_WEAPON)
			{
				_player->CastSpell(_player, pSpell->pLearnSpell, true);
				break;
			}
		}

		for( i = 0; i < 3; ++i)
		{
			if( pSpell->pLearnSpell->Effect[i] == SPELL_EFFECT_SKILL )
			{
				uint32 skill = pSpell->pLearnSpell->EffectMiscValue[i];
				uint32 val = (pSpell->pLearnSpell->EffectBasePoints[i]) /** 75*/;
				if( val > 350 )
					val = 350;

				if( _player->_GetSkillLineMax(skill) >= val )
					return;

				if( skill == SKILL_RIDING )
					_player->_AddSkillLine( skill, val, val );
				else
				{
					if( _player->_HasSkillLine(skill) )
						_player->_ModifySkillMaximum(skill, val);
					else
					{
						if (points_remaining <  2)
						{
							_player->_AddSkillLine( skill, 1, val);
						}

					}
				}
			}
		}

		// add the spell
		_player->addSpell( pSpell->pLearnSpell->Id );
	}

	if(pSpell->DeleteSpell)
	{
		// Remove old spell.
		if( pSpell->pLearnSpell )
			_player->removeSpell(pSpell->DeleteSpell, true, true, pSpell->pLearnSpell->Id);
		else if(pSpell->pCastSpell)
			_player->removeSpell(pSpell->DeleteSpell, true, true, pSpell->pCastRealSpell->Id);
		else
			_player->removeSpell(pSpell->DeleteSpell,true,false,0);
	}

}

uint8 WorldSession::TrainerGetSpellStatus(TrainerSpell* pSpell)
{
	if(!pSpell->pCastSpell && !pSpell->pLearnSpell)
		return TRAINER_STATUS_NOT_LEARNABLE;

	if( pSpell->pCastRealSpell && (_player->HasSpell(pSpell->pCastRealSpell->Id) || _player->HasDeletedSpell(pSpell->pCastRealSpell->Id)) )
		return TRAINER_STATUS_ALREADY_HAVE;

	if( pSpell->pLearnSpell && (_player->HasSpell(pSpell->pLearnSpell->Id) || _player->HasDeletedSpell(pSpell->pLearnSpell->Id)) )
		return TRAINER_STATUS_ALREADY_HAVE;

	if(pSpell->DeleteSpell && _player->HasDeletedSpell(pSpell->DeleteSpell))
		return TRAINER_STATUS_ALREADY_HAVE;

	if(	(pSpell->RequiredLevel && _player->getLevel()<pSpell->RequiredLevel)
		|| (pSpell->RequiredSpell && !_player->HasSpell(pSpell->RequiredSpell))
		|| (pSpell->Cost && _player->GetUInt32Value(PLAYER_FIELD_COINAGE) < pSpell->Cost)
		|| (pSpell->RequiredSkillLine && _player->_GetSkillLineCurrent(pSpell->RequiredSkillLine,true) < pSpell->RequiredSkillLineValue)
		|| (pSpell->IsProfession && pSpell->RequiredSkillLine==0 && _player->GetUInt32Value(PLAYER_CHARACTER_POINTS2) == 0)//check level 1 professions if we can learn a new proffesion
		)
		return TRAINER_STATUS_NOT_LEARNABLE;
	return TRAINER_STATUS_LEARNABLE;
}

//////////////////////////////////////////////////////////////
/// This function handles CMSG_PETITION_SHOWLIST:
//////////////////////////////////////////////////////////////
void WorldSession::HandleCharterShowListOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stNPC_Pettion_ShowList MsgRecv;packet >> MsgRecv;

	Creature *pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
	if(!pCreature) return;

	SendCharterRequest(pCreature);
}

void WorldSession::SendCharterRequest(Creature* pCreature)
{
	if(!_player->IsInWorld()) return;
	if(pCreature && pCreature->GetEntry()==19861 ||
		pCreature->GetEntry()==18897 || pCreature->GetEntry()==19856)
	{
		MSG_S2C::stNPC_Pettion_ShowList Msg;
		Msg.guid = pCreature->GetGUID();
		SendPacket(Msg);
	}
	else
	{
		MSG_S2C::stNPC_Pettion_ShowList Msg;
		Msg.guid = pCreature->GetGUID();
		SendPacket( Msg );
	}
}

//////////////////////////////////////////////////////////////
/// This function handles MSG_AUCTION_HELLO:
//////////////////////////////////////////////////////////////
void WorldSession::HandleAuctionHelloOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stAuction_Hello MsgRecv;packet >> MsgRecv;
	Creature* auctioneer = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.vendorguid));
	if(!auctioneer)
		return;

	SendAuctionList(auctioneer);
}

void WorldSession::SendAuctionList(Creature* auctioneer)
{
	AuctionHouse* AH = NULL;
	if( auctioneer )
		AH = sAuctionMgr.GetAuctionHouse(auctioneer->GetEntry());
	else
		AH = sAuctionMgr.GetAuctionHouse( 0 );

	if(!AH)
	{
		sChatHandler.BlueSystemMessage(this, "Report to devs: Unbound auction house npc %u.", auctioneer->GetEntry());
		return;
	}

	MSG_S2C::stAuction_Hello Msg;
	if( auctioneer )
		Msg.vendorguid = auctioneer->GetGUID();
	else
		Msg.vendorguid = 0;

	Msg.auctionhouse_id = AH->GetID();
	SendPacket( Msg );
}

//////////////////////////////////////////////////////////////
/// This function handles CMSG_GOSSIP_HELLO:
//////////////////////////////////////////////////////////////
void WorldSession::HandleGossipHelloOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	uint64 guid;
	list<QuestRelation *>::iterator it;
	std::set<uint32> ql;

	MSG_C2S::stNPC_Gossip_Hello MsgRecv;packet >> MsgRecv;
	guid = MsgRecv.guid;
	Creature *qst_giver = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(guid));
	if(!qst_giver) 
		return;

	//stop when talked to for 3 min
	/*if(qst_giver->GetAIInterface())
		qst_giver->GetAIInterface()->StopMovement(180000);*/
 
	// unstealth meh
	if( _player->IsStealth() )
		_player->RemoveAllAuraType( SPELL_AURA_MOD_STEALTH );

	//MyLog::log->debug( "WORLD: Received CMSG_GOSSIP_HELLO from %u",GUID_LOPART(guid) );

	GossipScript * Script = qst_giver->GetCreatureName() ? qst_giver->GetCreatureName()->gossip_script : NULL;
	if(!Script)
		return;

	if (qst_giver->isQuestGiver() && qst_giver->HasQuests())
	{
		MSG_S2C::stNPC_Gossip_Message Msg;
		Script->GossipHello(qst_giver, _player, false);
		if(!_player->CurrentGossipMenu)
			return;

		_player->CurrentGossipMenu->BuildPacket(Msg);
		uint32 count=0;//sQuestMgr.ActiveQuestsCount(qst_giver, GetPlayer());
		for (it = qst_giver->QuestsBegin(); it != qst_giver->QuestsEnd(); ++it)
		{
			uint32 status = sQuestMgr.CalcQuestStatus(qst_giver, GetPlayer(), *it);
			if (status >= QMGR_QUEST_CHAT)
			{
				if (!ql.count((*it)->qst->id) )
				{	
					ql.insert((*it)->qst->id);
					count++;
					MSG_S2C::stQuest Quest;
					Quest.quest_id = (*it)->qst->id;
					/*data << status;//sQuestMgr.CalcQuestStatus(qst_giver, GetPlayer(), *it);
					data << uint32(0);*/
					switch(status)
					{
// 					case QMGR_QUEST_NOT_FINISHED:
// 						Quest.stat = QMGR_QUEST_REPEATABLE_FINISHED;
// 						break;
// 
// 					case QMGR_QUEST_FINISHED:
// 						Quest.stat = QMGR_QUEST_REPEATABLE_FINISHED;
// 						break;
// 
					case QMGR_QUEST_CHAT:
						Quest.stat = QMGR_QUEST_AVAILABLE;
						break;

					default:
						Quest.stat = status;
						break;
					}

					Quest.title = (*it)->qst->title;
					Msg.vQuests.push_back(Quest);
				}
			}
		}
		SendPacket(Msg);
		//MyLog::log->debug( "WORLD: Sent SMSG_GOSSIP_MESSAGE" );
	}
	else
	{
		Script->GossipHello(qst_giver, _player, true);
	}
}

//////////////////////////////////////////////////////////////
/// This function handles CMSG_GOSSIP_SELECT_OPTION:
//////////////////////////////////////////////////////////////
void WorldSession::HandleGossipSelectOptionOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	//WorldPacket data;
	MSG_C2S::stNPC_Gossip_Select_Option MsgRecv;packet >> MsgRecv;
	int8 extra=0;

	ui64 guid = MsgRecv.guid;
	ui32 option = MsgRecv.option;

	//MyLog::log->notice("WORLD: CMSG_GOSSIP_SELECT_OPTION Option %i Guid %.8X", option, guid );
	GossipScript * Script=NULL;
	Object * qst_giver=NULL;
	uint32 guidtype = GET_TYPE_FROM_GUID(guid);

	if(guidtype==HIGHGUID_TYPE_UNIT)
	{
		Creature *crt = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(guid));
		if(!crt)
			return;

		qst_giver=crt;
		Script=crt->GetCreatureName()?crt->GetCreatureName()->gossip_script:NULL;
	}
	else if(guidtype==HIGHGUID_TYPE_ITEM)
	{
		Item * pitem = _player->GetItemInterface()->GetItemByGUID(guid);
		if(pitem==NULL)
			return;

		qst_giver=pitem;
		Script=pitem->GetProto()->gossip_script;
	}
	else if(guidtype==HIGHGUID_TYPE_GAMEOBJECT)
	{
        GameObject *gobj = _player->GetMapMgr()->GetGameObject(GET_LOWGUID_PART(guid));
		if(!gobj)
			return;
        
		qst_giver=gobj;
        Script=gobj->GetInfo()->gossip_script;
    }
	if(!Script||!qst_giver)
		return;

	uint32 IntId = 1;
	if(_player->CurrentGossipMenu)
	{
		GossipMenuItem item = _player->CurrentGossipMenu->GetItem(option);
		IntId = item.IntId;
		extra = item.Extra;
	}

	if(extra)
	{
		string str;
		str = MsgRecv.ExtraStr;

		Script->GossipSelectOption(qst_giver, _player, option, IntId, str.c_str());
	}
	else
		Script->GossipSelectOption(qst_giver, _player, option, IntId, NULL);
}

//////////////////////////////////////////////////////////////
/// This function handles CMSG_SPIRIT_HEALER_ACTIVATE:
//////////////////////////////////////////////////////////////
void WorldSession::HandleSpiritHealerActivateOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld() ||!_player->isDead()) return;
	GetPlayer( )->DeathDurabilityLoss(0.25);
	GetPlayer( )->ResurrectPlayer();

	if(_player->getLevel() > 10)
	{
		Aura *aur = GetPlayer()->FindAura(15007);
		
		if(aur) // If the player already have the aura, just extend it.
		{
			GetPlayer()->SetAurDuration(15007,aur->GetDuration());
		}
		else // else add him one, that fucker, he think he will get away!?
		{
			SpellEntry *spellInfo = dbcSpell.LookupEntry( 15007 );//resurrection sickness
			SpellCastTargets targets;
			targets.m_unitTarget = GetPlayer()->GetGUID();
			Spell*sp=new Spell(_player,spellInfo,true,NULL);
			sp->prepare(&targets);
		}
	}

	GetPlayer( )->SetUInt32Value(UNIT_FIELD_HEALTH, GetPlayer()->GetUInt32Value(UNIT_FIELD_MAXHEALTH)/2);
}

//////////////////////////////////////////////////////////////
/// This function handles CMSG_NPC_TEXT_QUERY:
//////////////////////////////////////////////////////////////
void WorldSession::HandleNpcTextQueryOpcode( CPacketUsn& packet )
{
	MSG_C2S::stNPC_Text_Query MsgRecv;packet >> MsgRecv;
	uint32 textID = MsgRecv.textID;
	uint64 targetGuid = MsgRecv.targetGuid;
	GossipText *pGossip;

	MyLog::log->notice("WORLD: CMSG_NPC_TEXT_QUERY ID '%u'", textID );

	GetPlayer()->SetUInt64Value(UNIT_FIELD_TARGET, targetGuid);

	pGossip = NpcTextStorage.LookupEntry(textID);

	MSG_S2C::stNPC_Text_Update Msg;
	Msg.textID = textID;
	
	MSG_S2C::stNPC_Text_Update::stGossipText gossiptext;
	if(pGossip)
	{
		for(uint32 i=0;i<8;i++)
		{
			gossiptext.text[0] = pGossip->Texts[i].Text[0];
			gossiptext.text[1] = pGossip->Texts[i].Text[1];

			for(uint32 e=0;e<6;e++)
				gossiptext.emote[e] = uint32(pGossip->Texts[i].Emote[e]);
			Msg.vGossipText.push_back(gossiptext);
		}
	} 
	else 
	{
		gossiptext.text[0] =  "Hey there, $N. How can I help you?";
		gossiptext.text[1] =  "Hey there, $N. How can I help you?";
		for(uint32 e=0;e<6;e++)
			gossiptext.emote[e] =uint32(0x00);
	}

	SendPacket(Msg);
	return;
}

void WorldSession::HandleBinderActivateOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stNPC_Binder_Activate MsgRecv;packet >> MsgRecv;

	Creature *pC = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
	if(!pC) return;

	SendInnkeeperBind(pC);
}


#define BIND_SPELL_ID 3286

void WorldSession::SendInnkeeperBind(Creature* pCreature)
{
	if(!_player->IsInWorld()) return;

	//////////////////////////////////////////////////////////////////////////
	//test
	_player->SetBindPoint(_player->GetPositionX(),_player->GetPositionY(),_player->GetPositionZ(),_player->GetMapId(),_player->GetZoneId());
	MSG_S2C::stNPC_Gossip_Complete MsgComplete1;
	SendPacket( MsgComplete1 );
	//return;
	//////////////////////////////////////////////////////////////////////////
// 	if(!_player->bHasBindDialogOpen)
// 	{
// 		MSG_S2C::stNPC_Gossip_Complete Msg;
// 		SendPacket( Msg );
// 
// 		MSG_S2C::stNPC_Binder_Confirm MsgConfirm;
// 		MsgConfirm.CreatureGuid = pCreature->GetGUID();
// 		MsgConfirm.ZoneID = _player->GetZoneId();
// 		SendPacket(Msg);
// 
// 		_player->bHasBindDialogOpen = true;
// 		return;
// 	}

	// Add a hearthstone if they don't have one
	if(!_player->GetItemInterface()->GetItemCount(ITEM_ID_HEARTH_STONE, true))
	{
		SystemMessage("您当前没有回程石,请您到杂货店购买");
		return;
		// We don't have a hearthstone. Add one.
		if(_player->GetItemInterface()->CalculateFreeSlots(NULL) > 0)
		{
			Item *item = objmgr.CreateItem( ITEM_ID_HEARTH_STONE, _player);
			if( _player->GetItemInterface()->AddItemToFreeSlot(item) )
			{
				SlotResult * lr = _player->GetItemInterface()->LastSearchResult();
				SendItemPushResult(item,false,true,false,true,lr->ContainerSlot,lr->Slot,1);
			}
			else
				delete item;
		}
	}

	//_player->bHasBindDialogOpen = false;

	_player->SetBindPoint(_player->GetPositionX(),_player->GetPositionY(),_player->GetPositionZ(),_player->GetMapId(),_player->GetZoneId());
	Item* pReturnStone = _player->GetItemInterface()->FindItemLessMax(ITEM_ID_HEARTH_STONE, 0, false);
	if(pReturnStone)
	{
		pReturnStone->SetUInt32Value(ITEM_FIELD_RECORD_MAPID, _player->GetMapId());
		pReturnStone->SetFloatValue(ITEM_FIELD_RECORD_POSX,  _player->GetPositionX());
		pReturnStone->SetFloatValue(ITEM_FIELD_RECORD_POSY,  _player->GetPositionY());
		pReturnStone->SetFloatValue(ITEM_FIELD_RECORD_POSZ,  _player->GetPositionZ());
		pReturnStone->SetUInt32Value(ITEM_FIELD_RECORD_ZONEID,_player->GetZoneId());
		MSG_S2C::stOnItemFieldRecordChange msg;
		msg.entry = pReturnStone->GetEntry();
		msg.zoneid = _player->GetZoneId();
		SendPacket( msg );
		pReturnStone->m_isDirty = true;
	}
/*
	MSG_S2C::stNPC_Binder_Update MsgUpdate;
	MsgUpdate.x = _player->GetBindPositionX();
	MsgUpdate.y	= _player->GetBindPositionY();
	MsgUpdate.z	= _player->GetBindPositionZ();
	MsgUpdate.mapID = _player->GetBindMapId();
	MsgUpdate.ZoneID = _player->GetBindZoneId();
	SendPacket( MsgUpdate );

	MSG_S2C::stNPC_Bind_Player MsgPlayer;
	MsgPlayer.CreatureGuid = pCreature->GetGUID();
	MsgPlayer.ZoneID = _player->GetBindZoneId();
	SendPacket(MsgPlayer);

	MSG_S2C::stNPC_Gossip_Complete MsgComplete;
    SendPacket( MsgComplete );

	MSG_S2C::stSpell_Start MsgStart;
	MsgStart.caster_item_guid	= pCreature->GetNewGUID();
	MsgStart.caster_unit_guid	= pCreature->GetNewGUID();
	MsgStart.spell_id			= BIND_SPELL_ID;
	MsgStart.extra_cast_number	= 0;
	MsgStart.cast_flags			= 0;
	MsgStart.casttime			= 0;
	MsgStart.targets.m_targetMask = TARGET_FLAG_UNIT;
	MsgStart.targets.m_unitTarget = _player->GetGUID();
	_player->SendMessageToSet(MsgStart, true);

	MSG_S2C::stSpell_Go MsgGo;
	MsgGo.caster_item_guid		= pCreature->GetNewGUID();
	MsgGo.caster_unit_guid		= pCreature->GetNewGUID();
	MsgGo.spell_id				= BIND_SPELL_ID;
	MsgGo.castFlags				= 1;
	MsgGo.casttime				= getMSTime();
	MsgGo.vTargetHit.push_back(_player->GetGUID());
	MsgGo.targets.m_targetMask	= TARGET_FLAG_UNIT;
	MsgGo.targets.m_unitTarget		= _player->GetGUID();
	_player->SendMessageToSet( MsgGo, true );
*/
}

#undef BIND_SPELL_ID

void WorldSession::SendSpiritHealerRequest(Creature* pCreature)
{
	MSG_S2C::stNPC_Spirite_Healer_Confirm Msg;
	Msg.CreatureGuid = pCreature->GetGUID();
	SendPacket(Msg);
}
