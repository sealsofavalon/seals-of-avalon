#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Move.h"
#include "../../SDBase/Protocol/S2C_AI.h"
#include "../../SDBase/Protocol/S2C_Spell.h"
#include "../../SDBase/Protocol/S2C_Group.h"
#include "SunyouCastle.h"
#include "SunyouBattleGround.h"
#include "SunyouRaid.h"
#include "MultiLanguageStringMgr.h"
#include "DynamicObject.h"
#include "QuestMgr.h"

Object::Object() : m_position(0,0,0,0), m_spawnLocation(0,0,0,0), m_uniqueIDForLua( 0 )
{
	m_isNeedUpdate = false;
	m_mapId = 0;
	m_zoneId = 0;

	m_uint32Values = 0;
	m_objectUpdated = false;


	m_valuesCount = 0;

	//official Values
	m_walkSpeed = 2.5f;
	m_runSpeed = 3.0f;
	m_base_runSpeed = m_runSpeed;
	m_base_walkSpeed = m_walkSpeed;

	m_flySpeed = 7.0f;
	m_backFlySpeed = 4.5f;

	m_backWalkSpeed = 4.5f;	// this should really be named m_backRunSpeed
	m_swimSpeed = 4.722222f;
	m_backSwimSpeed = 2.5f;
	m_turnRate = 3.141593f;

	m_mapMgr = 0;
	m_mapCell = 0;

	mSemaphoreTeleport = false;


	m_faction = NULL;
	m_factionDBC = NULL;

	m_instanceId = 0;
	Active = false;
	m_inQueue = false;
	m_extensions = NULL;
	m_loadedFromDB = false;

	s_global_objects.insert( this );
	objmgr.AddObjectForLua( this );
}

Object::~Object( )
{
	if(m_objectTypeId != TYPEID_ITEM && m_inQueue)
		MyLog::log->error("~Object( ) : object[%d] is still in addtoworld queue", GetGUID());//ASSERT(!m_inQueue);

	if (this->IsInWorld() && m_objectTypeId != TYPEID_ITEM && m_objectTypeId != TYPEID_CONTAINER)	{
		this->RemoveFromWorld(false);
	}

	// for linux
	m_instanceId = -1;
	m_objectTypeId=TYPEID_UNUSED;

	if( m_extensions != NULL )
		delete m_extensions;

	s_global_objects.erase( this );
	objmgr.RemoveObjectForLua( this );
}


void Object::_Create( uint32 mapid, float x, float y, float z, float ang )
{
	m_mapId = mapid;
	m_position.ChangeCoords(x, y, z, ang);
	m_spawnLocation.ChangeCoords(x, y, z, ang);
	m_lastMapUpdatePosition.ChangeCoords(x,y,z,ang);
}

uint32 Object::BuildCreateUpdateBlockForPlayer(ByteBuffer *data, Player *target)
{
	uint8 flags = 0;
	uint32 flags2 = 0;

	uint8 updatetype = UPDATETYPE_CREATE_OBJECT;
	if(m_objectTypeId == TYPEID_CORPSE)
	{
		if(m_uint32Values[CORPSE_FIELD_DISPLAY_ID] == 0)
			return 0;
	}

	// any other case
	switch(m_objectTypeId)
	{
		// items + containers: 0x8
	case TYPEID_CONTAINER:
	case TYPEID_ITEM:
		flags = UPDATEFLAG_LOWGUID | UPDATEFLAG_HIGHGUID;
		if( this->GetUInt32Value(ITEM_FIELD_OWNER) == 0 )
		{
			flags = flags | UPDATEFLAG_HASPOSITION;
		}
		break;
		// player/unit: 0x68 (except self)
	case TYPEID_UNIT:
		flags = UPDATEFLAG_HIGHGUID | UPDATEFLAG_LIVING | UPDATEFLAG_HASPOSITION;
		break;

	case TYPEID_PLAYER:
		flags = UPDATEFLAG_HIGHGUID | UPDATEFLAG_LIVING | UPDATEFLAG_HASPOSITION;
		break;

		// gameobject/dynamicobject
	case TYPEID_GAMEOBJECT:
	case TYPEID_CORPSE:
		flags = UPDATEFLAG_LOWGUID | UPDATEFLAG_HIGHGUID | UPDATEFLAG_HASPOSITION;
		break;

	case TYPEID_DYNAMICOBJECT:
		return 0;		// closed by gui
		flags = UPDATEFLAG_LOWGUID | UPDATEFLAG_HIGHGUID | UPDATEFLAG_LIVING | UPDATEFLAG_HASPOSITION;
		break;
		// anyone else can get fucked and die!
	}

	if(target == this)
	{
		// player creating self
		flags |= UPDATEFLAG_SELF;
		updatetype = UPDATETYPE_CREATE_YOURSELF;
	}

	// gameobject stuff
	/* commented by gui
	if(m_objectTypeId == TYPEID_GAMEOBJECT)
	{
		switch(m_uint32Values[GAMEOBJECT_TYPE_ID])
		{
			case GAMEOBJECT_TYPE_MO_TRANSPORT:
				{
					if(GetTypeFromGUID() != HIGHGUID_TYPE_TRANSPORTER)
						return 0;   // bad transporter
					else
						flags = UPDATEFLAG_TRANSPORT | UPDATEFLAG_LOWGUID | UPDATEFLAG_HIGHGUID | UPDATEFLAG_HASPOSITION;
				}break;

			case GAMEOBJECT_TYPE_TRANSPORT:
				{
					flags = UPDATEFLAG_TRANSPORT | UPDATEFLAG_LOWGUID | UPDATEFLAG_HIGHGUID | UPDATEFLAG_HASPOSITION;
				}break;

			case GAMEOBJECT_TYPE_DUEL_ARBITER:
				{
					// duel flags have to stay as updatetype 3, otherwise
					// it won't animate
					updatetype = UPDATETYPE_CREATE_YOURSELF;
				}break;
		}
	}
	*/
	
	uint32 before = data->wpos();

	*data << (uint16)0;

	// build our actual update
	*data << updatetype;

	// we shouldn't be here, under any cercumstances, unless we have a wowguid..
	*data << GetNewGUID();

	*data << m_objectTypeId;


	_BuildMovementUpdate(data, flags, flags2, target);

	// we have dirty data, or are creating for ourself.
	UpdateMask updateMask;
	updateMask.SetCount( m_valuesCount );
	_SetCreateBits( &updateMask, target );

	// this will cache automatically if needed
	_BuildValuesUpdate( data, &updateMask, target );

	if( IsPlayer() )
	{
		*data << ((Player*)this)->GetName();

		if(GetUInt32Value(PLAYER_GUILDID) && ((Player*)this)->GetGuild())
			*data << ((Player*)this)->GetGuild()->GetGuildName();
		else
			*data << "";

		/*
		PlayerInfo* teacher = ((Player*)this)->GetTeacher();
		if( teacher )
		{
			*data << teacher->name;
		}
		else
		{
			PlayerInfo* student = ((Player*)this)->GetStudent();
			if( student )
				*data << student->name;
		}
		*/
	}
	else if( IsPet() )
	{
		*data << ((Pet*)this)->GetName();
		*data << (int)QMGR_QUEST_NOT_AVAILABLE;
	}
	else if( IsCreature() )
	{
		if( target && ((Creature*)this)->isQuestGiver() )
			*data << sQuestMgr.CalcStatus( this, target );
		else
			*data << (int)QMGR_QUEST_NOT_AVAILABLE;
	}

	data->put( before, uint16(data->wpos() - before) );
	// update count: 1 ;)
	return 1;
}


//That is dirty fix it actually creates update of 1 field with
//the given value ignoring existing changes in fields and so on
//usefull if we want update this field for certain players
//NOTE: it does not change fields. This is also very fast method
void Object::BuildFieldUpdatePacket( uint32 index,uint32 value, MSG_S2C::stObjectUpdate* Msg)
{
   // uint64 guidfields = GetGUID();
   // uint8 guidmask = 0;
	//Msg->buffer << (uint32)1;//number of update/create blocks
	//Msg->buffer << (uint8)0;//unknown

	uint32 before = Msg->buffer.wpos();
	Msg->buffer << (uint16)0;
	Msg->buffer << (uint8) UPDATETYPE_VALUES;		// update type == update
	Msg->buffer << GetNewGUID();

	uint32 mBlocks = index/32+1;
	Msg->buffer << (uint8)mBlocks;
	Msg->buffer << index;

// 	for(uint32 dword_n=mBlocks-1;dword_n;dword_n--)
// 	Msg->buffer <<(uint32)0;
//
// 	Msg->buffer <<(((uint32)(1))<<(index%32));
	UpdateMask updateMask;
	updateMask.SetCount(mBlocks<<5);
	updateMask.SetBit(index);
	Msg->buffer.append(updateMask.GetMask(), updateMask.GetLength());
	Msg->buffer << value;

	Msg->buffer.put( before, uint16( Msg->buffer.wpos() - before ) );
	return;
}

void Object::BuildFieldUpdatePacket(Player* Target, uint32 Index, uint32 Value)
{
	static ByteBuffer buf(500);
	buf.clear();
	
	buf << (uint16)0;
	buf << uint8(UPDATETYPE_VALUES);
	buf << GetNewGUID();

	uint32 mBlocks = Index/32+1;
	buf << (uint8)mBlocks;
	buf << Index;
	UpdateMask updateMask;
	updateMask.SetCount(mBlocks<<5);
	updateMask.SetBit(Index);
	buf.append(updateMask.GetMask(), updateMask.GetLength());
	buf << Value;

	buf.put( 0, buf.wpos() );

	Target->PushUpdateData(&buf, 1);
}

void Object::BuildFieldUpdatePacket(ByteBuffer * buf, uint32 Index, uint32 Value)
{
	uint32 before = buf->wpos();
	*buf << (uint16)0;
	*buf << uint8(UPDATETYPE_VALUES);
	*buf << GetNewGUID();
	// 	UpdateMask updateMask;
	// 	updateMask.SetCount( Index );
	// 	updateMask.SetBit(Index);
	// 	*buf << (uint8)updateMask.GetBlockCount();
	// 	*buf << updateMask.GetCount();
	// 	buf->append( updateMask.GetMask(), (uint8)updateMask.GetBlockCount()*4 );

// 	UpdateMask mask;
// 	mask.SetCount(m_valuesCount);
// 	mask.SetBit(Index);
// 	_BuildValuesUpdate(buf, &mask, 0);
// 	return;
	uint32 mBlocks = Index/32+1;
	*buf << (uint8)mBlocks;
	*buf << Index;

	UpdateMask updateMask;
	updateMask.SetCount(mBlocks<<5);
	updateMask.SetBit(Index);
	buf->append(updateMask.GetMask(), updateMask.GetLength());
//
// 	for(uint32 dword_n=mBlocks-1;dword_n;dword_n--)
// 		*buf <<(uint32)0;

//	*buf <<(((uint32)(1))<<(Index%32));
	*buf << Value;

	buf->put( before, uint16( buf->wpos() - before ) );
}

uint32 Object::BuildValuesUpdateBlockForPlayer(ByteBuffer *data, Player *target)
{
	UpdateMask updateMask;
	updateMask.SetCount( m_valuesCount );
	_SetUpdateBits( &updateMask, target );
	for(uint32 x = 0; x < m_valuesCount; ++x)
	{
		if(updateMask.GetBit(x))
		{
			uint32 before = data->wpos();
			*data << (uint16)0;
			*data << (uint8) UPDATETYPE_VALUES;		// update type == update
			*data << GetNewGUID();
			_BuildValuesUpdate( data, &updateMask, target );

			data->put( before, uint16(data->wpos() - before) );
			return 1;
		}
	}

	return 0;
}

uint32 Object::BuildValuesUpdateBlockForPlayer(ByteBuffer * buf, UpdateMask * mask )
{
	uint32 before = buf->wpos();

	*buf << (uint16)0;
	// returns: update count
	*buf << (uint8) UPDATETYPE_VALUES;		// update type == update

	*buf << GetNewGUID();

	_BuildValuesUpdate( buf, mask, 0 );

	buf->put( before, uint16(buf->wpos() - before) );
	// 1 update.
	return 1;
}

void Object::DestroyForPlayer(Player *target) const
{
	if(target->GetSession() == 0) return;

	ASSERT(target);

	MSG_S2C::stDestroyObj Msg;
	Msg.guid = GetGUID();
	target->GetSession()->SendPacket( Msg );
}


///////////////////////////////////////////////////////////////
/// Build the Movement Data portion of the update packet
/// Fills the data with this object's movement/speed info
/// TODO: rewrite this stuff, document unknown fields and flags
uint32 TimeStamp();

void Object::_BuildMovementUpdate(ByteBuffer * data, uint8 flags, uint32 flags2, Player* target )
{
	//ByteBuffer *splinebuf = (m_objectTypeId == TYPEID_UNIT) ? target->GetAndRemoveSplinePacket(GetGUID()) : 0;
	*data << (uint8)flags;

	Player * pThis = 0;
	if(m_objectTypeId == TYPEID_PLAYER)
	{
		pThis = static_cast< Player* >( this );
		if(target == this)
		{
			// Updating our last speeds.
			pThis->UpdateLastSpeeds();
		}
	}

	if (flags & UPDATEFLAG_LIVING)
	{
		if(pThis && pThis->m_TransporterGUID != 0)
			flags2 |= MOVEFLAG_TAXI;
		else if(m_objectTypeId==TYPEID_UNIT && ((Creature*)this)->m_transportGuid != 0 && ((Creature*)this)->m_transportPosition != NULL)
			flags2 |= MOVEFLAG_TAXI;

		/*
		if(splinebuf)
		{
			flags2 |= 0x08000001;	   //1=move forward
			if(GetTypeId() == TYPEID_UNIT)
			{
				if( ((Unit*)this)->GetAIInterface()->m_moveRun == false)
					flags2 |= 0x100;	//100=walk
			}			
		}
		*/

		if(GetTypeId() == TYPEID_UNIT)
		{
			//		 Don't know what this is, but I've only seen it applied to spirit healers.
			//		 maybe some sort of invisibility flag? :/

			switch(GetEntry())
			{
			case 6491:  // Spirit Healer
			case 13116: // Alliance Spirit Guide
			case 13117: // Horde Spirit Guide
				{
					flags2 |= MOVEFLAG_WATER_WALK;
				}break;
			}

			if(static_cast<Unit*>(this)->GetAIInterface()->IsFlying())
//				flags2 |= 0x800; //in 2.3 this is some state that i was not able to decode yet
				flags2 |= MOVEFLAG_NO_COLLISION; //Zack : Teribus the Cursed had flag 400 instead of 800 and he is flying all the time
// 			if(static_cast<Creature*>(this)->proto && static_cast<Creature*>(this)->proto->extra_a9_flags)
// 			{
// 				if(!(flags2 & MOVEFLAG_TAXI))
// 					flags2 |= static_cast<Creature*>(this)->proto->extra_a9_flags;
// 			}
/*			if(GetGUIDHigh() == HIGHGUID_WAYPOINT)
			{
				if(GetUInt32Value(UNIT_FIELD_STAT0) == 768)		// flying waypoint
					flags2 |= 0x800;
			}*/
		}

		*data << (uint32)flags2;

		//*data << (uint8)0;

		*data << getMSTime(); // this appears to be time in ms but can be any thing

		// this stuff:
		//   0x01 -> Enable Swimming?
		//   0x04 -> ??
		//   0x10 -> disables movement compensation and causes players to jump around all the place
		//   0x40 -> disables movement compensation and causes players to jump around all the place

		/*static uint8 fl = 0x04;
		*data << uint8(fl);		// wtf? added in 2.3.0*/
		/*if(target==this)
			*data<<uint8(0x53);
		else
			*data<<uint8(0);*/
		//*data << uint8(0x1);
	}

	if (flags & UPDATEFLAG_HASPOSITION)
	{
		*data << (float)m_position.x;
		*data << (float)m_position.y;
		*data << (float)m_position.z;
		*data << (float)m_position.o;

		if(flags & UPDATEFLAG_LIVING && flags2 & MOVEFLAG_TAXI)
		{
			if(pThis)
			{
				*data << pThis->m_TransporterGUID;
				*data << uint32(HIGHGUID_TYPE_TRANSPORTER);
				*data << pThis->m_TransporterX << pThis->m_TransporterY << pThis->m_TransporterZ << pThis->m_TransporterO;
				//*data << pThis->m_TransporterUnk;
			}
			else if(m_objectTypeId==TYPEID_UNIT && ((Creature*)this)->m_transportPosition != NULL)
			{
				*data << ((Creature*)this)->m_transportGuid;
				*data << uint32(HIGHGUID_TYPE_TRANSPORTER);
				*data << ((Creature*)this)->m_transportPosition->x << ((Creature*)this)->m_transportPosition->y <<
					((Creature*)this)->m_transportPosition->z << ((Creature*)this)->m_transportPosition->o;
				//*data << float(0.0f);
			}
		}
	}

// 	if (flags & UPDATEFLAG_LIVING)
// 	{
// 		*data << (uint32)0;
// 	}

// 	if (flags & UPDATEFLAG_LIVING && flags2 & MOVEFLAG_FALLING)
// 	{
// 		*data << (float)0;
// 		*data << (float)1.0;
// 		*data << (float)0;
// 		*data << (float)0;
// 	}

	if (flags & UPDATEFLAG_LIVING)
	{
		*data << m_walkSpeed;	 // walk speed
		*data << m_runSpeed;	  // run speed
		*data << m_backWalkSpeed; // backwards walk speed
		*data << m_swimSpeed;	 // swim speed
		*data << m_backSwimSpeed; // backwards swim speed
		*data << m_flySpeed;		// fly speed
		*data << m_backFlySpeed;	// back fly speed
		*data << m_turnRate;	  // turn rate
	}

	/*
 	if(splinebuf)
 	{
 		data->append(*splinebuf);
 		delete splinebuf;
 	}
	*/

	if(flags & UPDATEFLAG_LOWGUID)
	{
		*data << GetUInt32Value(OBJECT_FIELD_GUID);
		if(flags & UPDATEFLAG_HIGHGUID)
			*data << GetUInt32Value(OBJECT_FIELD_GUID_01);
	}
	else if(flags & UPDATEFLAG_HIGHGUID)
		*data << GetUInt32Value(OBJECT_FIELD_GUID);

	if(flags & UPDATEFLAG_TRANSPORT)
	{
		if(target)
		{
			/*int32 m_time = TimeStamp() - target->GetSession()->m_clientTimeDelay;
			m_time += target->GetSession()->m_moveDelayTime;
			*data << m_time;*/
			*data << getMSTime();
		}
		else
            *data << getMSTime();
	}
}


//=======================================================================================
//  Creates an update block with the values of this object as
//  determined by the updateMask.
//=======================================================================================
void Object::_BuildValuesUpdate(ByteBuffer * data, UpdateMask *updateMask, Player* target)
{
	bool activate_quest_object = false;
	bool reset = false;
	uint32 oldflags = 0;

	if(updateMask->GetBit(OBJECT_FIELD_GUID) && target)	   // We're creating.
	{
		Creature * pThis = static_cast<Creature*>(this);
		if(GetTypeId() == TYPEID_UNIT && pThis->Tagged && (pThis->loot.gold || pThis->loot.items.size()))
		{
			// Let's see if we're the tagger or not.
			oldflags = m_uint32Values[UNIT_DYNAMIC_FLAGS];
			uint32 Flags = m_uint32Values[UNIT_DYNAMIC_FLAGS];
			uint32 oldFlags = 0;

			if(pThis->TaggerGuid == target->GetGUID())

			{
				// Our target is our tagger.
				oldFlags = U_DYN_FLAG_TAGGED_BY_OTHER;

				if(Flags & U_DYN_FLAG_TAGGED_BY_OTHER)
					Flags &= ~oldFlags;

				if(!(Flags & U_DYN_FLAG_LOOTABLE))
					Flags |= U_DYN_FLAG_LOOTABLE;
			}
			else
			{
				// Target is not the tagger.
				oldFlags = U_DYN_FLAG_LOOTABLE;

				if(!(Flags & U_DYN_FLAG_TAGGED_BY_OTHER))
					Flags |= U_DYN_FLAG_TAGGED_BY_OTHER;

				if(Flags & U_DYN_FLAG_LOOTABLE)
					Flags &= ~oldFlags;
			}

			m_uint32Values[UNIT_DYNAMIC_FLAGS] = Flags;

			updateMask->SetBit(UNIT_DYNAMIC_FLAGS);

			reset = true;
		}

		if(target && GetTypeId() == TYPEID_GAMEOBJECT)
		{
			GameObject *go = ((GameObject*)this);
			QuestLogEntry *qle;
			GameObjectInfo *info;
			if( go->HasQuests() )
			{
				activate_quest_object = true;
			}
			else
			{
				info = go->GetInfo();
				if( info && ( info->goMap.size() || info->itemMap.size() ) )
				{
					for( GameObjectGOMap::iterator itr = go->GetInfo()->goMap.begin(); itr != go->GetInfo()->goMap.end(); ++itr )
					{
						qle = target->GetQuestLogForEntry( itr->first->id );
						if( qle != NULL )
						{
							if( qle->GetQuest()->count_required_mob == 0 )
								continue;
							for( uint32 i = 0; i < 4; ++i )
							{
								if( qle->GetQuest()->required_mob[i] == go->GetEntry() && qle->GetMobCount(i) < qle->GetQuest()->required_mobcount[i])
								{
									activate_quest_object = true;
									break;
								}
							}
							if(activate_quest_object)
								break;
						}
					}

					if(!activate_quest_object)
					{
						for(GameObjectItemMap::iterator itr = go->GetInfo()->itemMap.begin();
							itr != go->GetInfo()->itemMap.end();
							++itr)
						{
							for(std::map<uint32, uint32>::iterator it2 = itr->second.begin();
								it2 != itr->second.end();
								++it2)
							{
								if((qle = target->GetQuestLogForEntry(itr->first->id)))
								{
									if(target->GetItemInterface()->GetItemCount(it2->first) < it2->second)
									{
										activate_quest_object = true;
										break;
									}
								}
							}
							if(activate_quest_object)
								break;
						}
					}
				}
			}
		}
	}


	if(activate_quest_object)
	{
		oldflags = m_uint32Values[GAMEOBJECT_DYN_FLAGS];
		if(!updateMask->GetBit(GAMEOBJECT_DYN_FLAGS))
			updateMask->SetBit(GAMEOBJECT_DYN_FLAGS);
		m_uint32Values[GAMEOBJECT_DYN_FLAGS] = 1;
		reset = true;
	}

	WPAssert( updateMask && updateMask->GetCount() == m_valuesCount );
	uint32 bc;
	uint32 values_count;
	if( m_valuesCount > ( 2 * 0x20 ) )//if number of blocks > 2->  unit and player+item container
	{
		bc = updateMask->GetUpdateBlockCount();
		values_count = (uint32)min( bc * 32, m_valuesCount );

	}
	else
	{
		bc=updateMask->GetBlockCount();
		values_count=m_valuesCount;
	}

	*data << (uint8)bc;
	*data << values_count;
	data->append( updateMask->GetMask(), bc*4 );

	for( uint32 index = 0; index < values_count; index ++ )
	{
		if( updateMask->GetBit( index ) )
		{
// 			switch(index)
// 			{
// 			case UNIT_FIELD_MAXHEALTH:
// 				{
// 					if(m_valuesCount < UNIT_END)
// 						*data << m_uint32Values[index];
// 					else
// 					{
// 						switch(m_objectTypeId)
// 						{
// 						case TYPEID_PLAYER:
// 							*data << m_uint32Values[index];
// 						break;
//
// 						case TYPEID_UNIT:
// 							{
// 								if(IsPet())
// 								{
// 									*data << m_uint32Values[index];
// 									break;
// 								}
// 								else
// 								{
// 									*data << (uint32)100;
// 								}
// 							}
// 						}
// 					}
// 				}
// 				break;
// 			case UNIT_FIELD_HEALTH:
// 				{
// 					if(m_valuesCount < UNIT_END)
//						*data << m_uint32Values[index];
// 					else
// 					{
// 						switch(m_objectTypeId)
// 						{
// 						case TYPEID_PLAYER:
// 							*data << m_uint32Values[index];
// 						break;
//
// 						case TYPEID_UNIT:
// 							{
// 								//if(IsPet())
// 								{
// 									*data << m_uint32Values[index];
// 									break;
// 								}
// // 								else
// // 								{
// // 									uint32 pct = uint32(float( float(m_uint32Values[index]) / float(m_uint32Values[UNIT_FIELD_MAXHEALTH]) * 100.0f));
// //
// // 									/* fix case where health value got rounded down and the client sees health as dead */
// // 									if(!pct && m_uint32Values[UNIT_FIELD_HEALTH] != 0)
// // 										++pct;
// // 									*data << pct;
// // 								}
// 							}
// 						}
// 					}
// 				}
// 				break;
//
// 			default:
				*data << m_uint32Values[ index ];
// 				break;
// 			}
		}
	}

	if(reset)
	{
		switch(GetTypeId())
		{
		case TYPEID_UNIT:
			m_uint32Values[UNIT_DYNAMIC_FLAGS] = oldflags;
			break;
		case TYPEID_GAMEOBJECT:
			m_uint32Values[GAMEOBJECT_DYN_FLAGS] = oldflags;
			break;
		}
	}

}

void Object::BuildHeartBeatMsg(MSG_S2C::stMove_OP* Msg) const
{
	/*
	Msg->moveOP = MOVE_HEARTBEAT;
	Msg->guid = GetGUID();
	Msg->move.flags = 0;
	Msg->move.x = m_position.x;
	Msg->move.y = m_position.y;
	Msg->move.z = m_position.z;
	Msg->move.orientation = m_position.o;
	*/
}

void Object::BuildTeleportAckMsg(const LocationVector & v, MSG_S2C::stMove_Teleport_Ack* Msg)
{
	///////////////////////////////////////
	//Update player on the client with TELEPORT_ACK
	static_cast< Player* >( this )->SetPlayerStatus( TRANSFER_PENDING );

	Msg->guid	= GetNewGUID();
	Msg->x		= v.x;
	Msg->y		= v.y;
	Msg->z		= v.z;
	Msg->orientation	= v.o;
	//First 4 bytes = no idea what it is
// 	*data << uint32(2); // flags
// 	*data << uint32(0); // mysterious value #1
// 	*data << uint8(0);
//
// 	*data << float(0);
// 	*data << v;
// 	*data << v.o;
// 	*data << uint16(2);
// 	*data << uint8(0);
	return;
}

bool Object::SetPosition(const LocationVector & v, bool allowPorting /* = false */)
{
	bool updateMap = false, result = true;

	if (m_position.x != v.x || m_position.y != v.y || m_position.z != v.z)
		updateMap = true;

	m_position = const_cast<LocationVector&>(v);

	if (!allowPorting && v.z < -500)
	{
		m_position.z = 500;
		MyLog::log->error( "setPosition: fell through map; height ported" );

		result = false;
	}

	if (IsInWorld() && updateMap)
	{
		m_mapMgr->ChangeObjectLocation(this);
	}

	return result;
}

bool Object::SetPosition( float newX, float newY, float newZ, float newOrientation, bool allowPorting )
{
	bool updateMap = false, result = true;

	//if (m_position.x != newX || m_position.y != newY)
		//updateMap = true;
	if(m_lastMapUpdatePosition.Distance2DSq(newX, newY) > 4.0f)		/* 2.0f */
		updateMap = true;

	m_position.ChangeCoords(newX, newY, newZ, newOrientation);

	if (!allowPorting && newZ < -500)
	{
		m_position.z = 500;
		MyLog::log->error( "setPosition: fell through map; height ported" );

		result = false;
	}

	if (IsInWorld() && updateMap)
	{
		m_lastMapUpdatePosition.ChangeCoords(newX,newY,newZ,newOrientation);
		m_mapMgr->ChangeObjectLocation(this);

		if( m_objectTypeId == TYPEID_PLAYER && static_cast< Player* >( this )->GetGroup() && static_cast< Player* >( this )->m_last_group_position.Distance2DSq(m_position) > 25.0f ) // distance of 5.0
		{
            static_cast< Player* >( this )->GetGroup()->HandlePartialChange( PARTY_UPDATE_FLAG_POSITION, static_cast< Player* >( this ) );
		}
	}

	return result;
}

void Object::SetRotation( uint64 guid )
{
	MSG_S2C::stAI_ReAction Msg;
	Msg.guid = guid;
	Msg.reaction = 2;
	SendMessageToSet(Msg, false);
}
//
// void Object::OutPacketToSet(uint16 Opcode, uint16 Len, const void * Data, bool self)
// {
// 	if(self && m_objectTypeId == TYPEID_PLAYER)
// 		static_cast< Player* >( this )->GetSession()->OutPacket(Opcode, Len, Data);
//
// 	if(!IsInWorld())
// 		return;
//
// 	std::set<Player*>::iterator itr = m_inRangePlayers.begin();
// 	std::set<Player*>::iterator it_end = m_inRangePlayers.end();
// 	int gm = ( m_objectTypeId == TYPEID_PLAYER ? static_cast< Player* >( this )->m_isGmInvisible : 0 );
// 	for(; itr != it_end; ++itr)
// 	{
// 		ASSERT((*itr)->GetSession());
// 		if( gm )
// 		{
// 			if( (*itr)->GetSession()->GetPermissionCount() > 0 )
// 				(*itr)->GetSession()->OutPacket(Opcode, Len, Data);
// 		}
// 		else
// 		{
// 			(*itr)->GetSession()->OutPacket(Opcode, Len, Data);
// 		}
// 	}
// }

void Object::SendMessageToSet(PakHead& packet, bool bToSelf,bool myteam_only, bool process_pending)
{
	CPacketSn sn;
	sn << packet;
	sn.SetProLen();
	const char* buff = sn.GetBuf();
	size_t len = sn.GetSize();

	if(bToSelf && m_objectTypeId == TYPEID_PLAYER)
	{
		static_cast< Player* >( this )->GetSession()->SendData( buff, len );
	}

	if(!IsInWorld())
		return;

	std::set<Player*>::iterator itr = m_inRangePlayers.begin();
	std::set<Player*>::iterator it_end = m_inRangePlayers.end();
	bool gminvis = (m_objectTypeId == TYPEID_PLAYER ? static_cast< Player* >( this )->m_isGmInvisible : false);
	//Zehamster: Splitting into if/else allows us to avoid testing "gminvis==true" at each loop...
	//		   saving cpu cycles. Chat messages will be sent to everybody even if player is invisible.
	if(myteam_only)
	{
		Player* plr = NULL;
		if( this->IsPlayer() )
			plr = (Player*)this;
		else if( this->IsPet() )
			plr = ((Pet*)this)->GetPetOwner();
		if (plr)
		{
			uint32 myteam=plr->GetTeam();
			if(gminvis && packet.wProNO!=MSG_S2C::CHAT_MESSAGE)
			{
				for(; itr != it_end; ++itr)
				{
					Player* ply = *itr;
					ASSERT( ply->GetSession());
					if(ply->GetSession()->GetPermissionCount() > 0 && ply->GetTeam()==myteam)
					{
						ply->GetSession()->SendData( buff, len );
					}
				}
			}
			else
			{
				for(; itr != it_end; ++itr)
				{
					Player* ply = *itr;
					ASSERT(ply->GetSession());
					if(ply->GetTeam()==myteam)
					{
						ply->GetSession()->SendData( buff, len );
					}
				}
			}
		}
			

	}
	else
	{
		if(gminvis && packet.wProNO!=MSG_S2C::CHAT_MESSAGE)
		{
			for(; itr != it_end; ++itr)
			{
				Player* ply = *itr;
				ASSERT(ply->GetSession());
				if(ply->GetSession()->GetPermissionCount() > 0)
				{
					ply->GetSession()->SendData( buff, len );
				}
			}
		}
		else
		{
			for(; itr != it_end; ++itr)
			{
				Player* ply = *itr;
				ASSERT(ply->GetSession());
				ply->GetSession()->SendData( buff, len );
			}
		}
	}
}

void Object::PushMonsterMoveToSet(const MonsterMovementInfo& info, bool bToSelf, bool myteam_only, bool process_pending )
{
	if(bToSelf && m_objectTypeId == TYPEID_PLAYER)
	{
		static_cast< Player* >( this )->GetSession()->PushMonstMovement( info );
	}

	if(!IsInWorld())
		return;

	std::set<Player*>::iterator itr = m_inRangePlayers.begin();
	std::set<Player*>::iterator it_end = m_inRangePlayers.end();
	bool gminvis = (m_objectTypeId == TYPEID_PLAYER ? static_cast< Player* >( this )->m_isGmInvisible : false);
	//Zehamster: Splitting into if/else allows us to avoid testing "gminvis==true" at each loop...
	//		   saving cpu cycles. Chat messages will be sent to everybody even if player is invisible.
	if(myteam_only)
	{
		Player* plr = NULL;
		if( this->IsPlayer() )
			plr = (Player*)this;
		else if( this->IsPet() )
			plr = ((Pet*)this)->GetPetOwner();
		if (plr)
		{
			uint32 myteam=plr->GetTeam();
			if(gminvis)
			{
				for(; itr != it_end; ++itr)
				{
					Player* ply = *itr;
					ASSERT( ply->GetSession());
					if(ply->GetSession()->GetPermissionCount() > 0 && ply->GetTeam()==myteam)
					{
						ply->GetSession()->PushMonstMovement( info );
					}
				}
			}
			else
			{
				for(; itr != it_end; ++itr)
				{
					Player* ply = *itr;
					ASSERT(ply->GetSession());
					if(ply->GetTeam()==myteam)
					{
						ply->GetSession()->PushMonstMovement( info );
					}
				}
			}
		}


	}
	else
	{
		if(gminvis)
		{
			for(; itr != it_end; ++itr)
			{
				Player* ply = *itr;
				ASSERT(ply->GetSession());
				if(ply->GetSession()->GetPermissionCount() > 0)
				{
					ply->GetSession()->PushMonstMovement( info );
				}
			}
		}
		else
		{
			for(; itr != it_end; ++itr)
			{
				Player* ply = *itr;
				ASSERT(ply->GetSession());
				ply->GetSession()->PushMonstMovement( info );
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////
/// Fill the object's Update Values from a space deliminated list of values.
void Object::LoadValues(const char* data)
{
	// thread-safe ;) strtok is not.
	std::string ndata = data;
	std::string::size_type last_pos = 0, pos = 0;
	uint32 index = 0;
	uint32 val;
	do
	{
		// prevent overflow
		if(index >= m_valuesCount)
		{
			break;
		}
		pos = ndata.find(" ", last_pos);
		val = atol(ndata.substr(last_pos, (pos-last_pos)).c_str());
		if(m_uint32Values[index] == 0)
			m_uint32Values[index] = val;
		last_pos = pos+1;
		++index;
	} while(pos != std::string::npos);
}

void Object::_SetUpdateBits(UpdateMask *updateMask, Player *target) const
{
	*updateMask = m_updateMask;
}


void Object::_SetCreateBits(UpdateMask *updateMask, Player *target) const
{
	/*for( uint16 index = 0; index < m_valuesCount; index++ )
	{
		if(GetUInt32Value(index) != 0)
			updateMask->SetBit(index);
	}*/
	for(uint32 i = 0; i < m_valuesCount; ++i)
		if(m_uint32Values[i] != 0)
			updateMask->SetBit(i);
}

void Object::AddToWorld()
{
	MapMgr *mapMgr = sInstanceMgr.GetInstance(this);
	if(!mapMgr)
		return; //instance add failed

	m_mapMgr = mapMgr;
	m_inQueue = true;

	mapMgr->AddObject(this);

	// correct incorrect instance id's
	m_instanceId = m_mapMgr->GetInstanceID();
//	if( IsPlayer() && mapMgr->m_pInstance )
//		static_cast<Player*>( this )->m_playerInfo->instances.insert( m_instanceId );
	if( IsPlayer() && m_mapMgr->m_pInstance )
		static_cast<Player*>( this )->m_playerInfo->instanceEnterLimit[m_mapMgr->GetMapId()]++;

	mSemaphoreTeleport = false;
}

void Object::AddToWorld(MapMgr * pMapMgr)
{
	if(!pMapMgr)
		return; //instance add failed

	m_mapMgr = pMapMgr;
	m_inQueue = true;

	pMapMgr->AddObject(this);

	// correct incorrect instance id's
	m_instanceId = pMapMgr->GetInstanceID();

	//	if( IsPlayer() && pMapMgr->m_pInstance )
	//		static_cast<Player*>( this )->m_playerInfo->instances.insert( m_instanceId );
	if( IsPlayer() && pMapMgr->m_pInstance )
		static_cast<Player*>( this )->m_playerInfo->instanceEnterLimit[pMapMgr->GetMapId()]++;

	mSemaphoreTeleport = false;

	if( m_objectTypeId == TYPEID_PLAYER && static_cast< Player* >( this )->GetGroup() )
		static_cast< Player* >( this )->GetGroup()->HandlePartialChange( PARTY_UPDATE_FLAG_MAPID, static_cast< Player* >( this ) );
}

//Unlike addtoworld it pushes it directly ignoring add pool
//this can only be called from the thread of mapmgr!!!
void Object::PushToWorld(MapMgr*mgr)
{
	if(!mgr/* || (m_mapMgr != NULL && m_mapCell != NULL) */)
		return; //instance add failed

	m_mapId=mgr->GetMapId();
	m_instanceId = mgr->GetInstanceID();

	m_mapMgr = mgr;
	OnPrePushToWorld();

	mgr->PushObject(this);

	// correct incorrect instance id's
	mSemaphoreTeleport = false;
	m_inQueue = false;

	event_Relocate();

	// call virtual function to handle stuff.. :P
	OnPushToWorld();
}

void Object::RemoveFromWorld(bool free_guid)
{
	if(!m_mapMgr)
	{
		MyLog::log->error("Object::RemoveFromWorld, but m_mapMgr == NULL");
		return;
	}
	MapMgr * m = m_mapMgr;
	mSemaphoreTeleport = true;

	m->RemoveObject(this, free_guid);
	m_mapMgr = 0;
	// update our event holder
	event_Relocate();
}

//! Set uint32 property
void Object::SetUInt32Value( const uint32 index, const uint32 value )
{
	if( index >= m_valuesCount )
	{
		MyLog::log->error("index[%d] >= valuecount[%d]", index, m_valuesCount);
		return;
	}
	// save updating when val isn't changing.
	if(m_uint32Values[index] == value)
		return;

	uint32 tmp = m_uint32Values[index];
	m_uint32Values[ index ] = value;

	if( index == UNIT_FIELD_HEALTH )
	{
		if( IsCreature() )
		{
			int mod = (int)value - (int)tmp;
			if( mod < 0 )
				static_cast<Creature*>(this)->SayEventHPChange( mod );
		}
	}

	// modified by gui
	m_updateMask.SetBit( index );
	m_isNeedUpdate = true;

	if(IsInWorld())
	{
		if(!m_objectUpdated)
		{
			m_mapMgr->ObjectUpdated(this);
			m_objectUpdated = true;
		}
	}

	// Group update handling
	if(m_objectTypeId == TYPEID_PLAYER)
	{
		if(IsInWorld())
		{
			Group* pGroup = static_cast< Player* >( this )->GetGroup();
			if( pGroup != NULL )
				pGroup->HandleUpdateFieldChange( index, static_cast< Player* >( this ) );
		}

#ifdef OPTIMIZED_PLAYER_SAVING
		switch(index)
		{
		case UNIT_FIELD_LEVEL:
		case PLAYER_XP:
			static_cast< Player* >( this )->save_LevelXP();
			break;

		case PLAYER_FIELD_COINAGE:
			static_cast< Player* >( this )->save_Gold();
			break;
		}
#endif
	}
}
/*
//must be in %
void Object::ModPUInt32Value(const uint32 index, const int32 value, bool apply )
{
	ASSERT( index < m_valuesCount );
	int32 basevalue = (int32)m_uint32Values[ index ];
	if(apply)
		m_uint32Values[ index ] += ((basevalue*value)/100);
	else
		m_uint32Values[ index ] = (basevalue*100)/(100+value);

	if(IsInWorld())
	{
		m_updateMask.SetBit( index );

		if(!m_objectUpdated )
		{
			m_mapMgr->ObjectUpdated(this);
			m_objectUpdated = true;
		}
	}
}
*/
uint32 Object::GetModPUInt32Value(const uint32 index, const int32 value)
{
	//ASSERT( index < m_valuesCount );
	if( index >= m_valuesCount )
	{
		assert( 0 );
		return 0;
	}
	int32 basevalue = (int32)m_uint32Values[ index ];
	return ((basevalue*value)/100);
}

void Object::ModUnsigned32Value(uint32 index, int32 mod)
{
	//ASSERT( index < m_valuesCount );
	if( index >= m_valuesCount )
	{
		assert( 0 );
		return;
	}

	if(mod == 0)
		return;

	uint32 old = m_uint32Values[ index ];
	m_uint32Values[ index ] += mod;
	if( (int32)m_uint32Values[index] < 0 )
		m_uint32Values[index] = 0;

	if( old == m_uint32Values[index] )
		return;

	m_updateMask.SetBit( index );
	m_isNeedUpdate = true;

	if(IsInWorld())
	{
		if(!m_objectUpdated)
		{
			m_mapMgr->ObjectUpdated(this);
			m_objectUpdated = true;
		}
	}

	if(m_objectTypeId == TYPEID_PLAYER)
	{
#ifdef OPTIMIZED_PLAYER_SAVING
		switch(index)
		{
		case UNIT_FIELD_LEVEL:
		case PLAYER_XP:
			static_cast< Player* >( this )->save_LevelXP();
			break;

		case PLAYER_FIELD_COINAGE:
			static_cast< Player* >( this )->save_Gold();
			break;
		}
#endif
	}

	if( IsCreature() && index == UNIT_FIELD_HEALTH && mod < 0 )
		static_cast<Creature*>(this)->SayEventHPChange( mod );
}

void Object::ModSignedInt32Value(uint32 index, int32 value )
{
	//ASSERT( index < m_valuesCount );
	if( index >= m_valuesCount )
	{
		assert( 0 );
		return;
	}

	if(value == 0)
		return;

	m_uint32Values[ index ] += value;
	m_updateMask.SetBit( index );
	m_isNeedUpdate = true;

	if(IsInWorld())
	{
		if(!m_objectUpdated)
		{
			m_mapMgr->ObjectUpdated(this);
			m_objectUpdated = true;
		}
	}

	if(m_objectTypeId == TYPEID_PLAYER)
	{
#ifdef OPTIMIZED_PLAYER_SAVING
		switch(index)
		{
		case UNIT_FIELD_LEVEL:
		case PLAYER_XP:
			static_cast< Player* >( this )->save_LevelXP();
			break;

		case PLAYER_FIELD_COINAGE:
			static_cast< Player* >( this )->save_Gold();
			break;
		}
#endif
	}
}

void Object::ModFloatValue(const uint32 index, const float value )
{
	//ASSERT( index < m_valuesCount );
	if( index >= m_valuesCount )
	{
		assert( 0 );
		return;
	}
	m_floatValues[ index ] += value;
	m_updateMask.SetBit( index );
	m_isNeedUpdate = true;

	if(IsInWorld())
	{
		if(!m_objectUpdated)
		{
			m_mapMgr->ObjectUpdated(this);
			m_objectUpdated = true;
		}
	}
}
//! Set uint64 property
void Object::SetUInt64Value( const uint32 index, const uint64 value )
{
	//assert( index + 1 < m_valuesCount );
	if( index + 1 >= m_valuesCount )
	{
		assert( 0 );
		return;
	}
#ifndef USING_BIG_ENDIAN
	if(m_uint32Values[index] == GUID_LOPART(value) && m_uint32Values[index+1] == GUID_HIPART(value))
		return;

	m_uint32Values[ index ] = *((uint32*)&value);
	m_uint32Values[ index + 1 ] = *(((uint32*)&value) + 1);
#else
	m_uint32Values[index] = value & 0xffffffff;
	m_uint32Values[index+1] = (value >> 32) & 0xffffffff;
#endif

	m_updateMask.SetBit( index );
	m_updateMask.SetBit( index + 1 );
	m_isNeedUpdate = true;

	if(IsInWorld())
	{
		if(!m_objectUpdated)
		{
			m_mapMgr->ObjectUpdated(this);
			m_objectUpdated = true;
		}
	}
}

//! Set float property
void Object::SetFloatValue( const uint32 index, const float value )
{
	//ASSERT( index < m_valuesCount );
	if( index >= m_valuesCount )
	{
		assert( 0 );
		return;
	}
	if(m_floatValues[index] == value)
		return;

	m_floatValues[ index ] = value;
	m_updateMask.SetBit( index );
	m_isNeedUpdate = true;

	if(IsInWorld())
	{
		if(!m_objectUpdated)
		{
			m_mapMgr->ObjectUpdated(this);
			m_objectUpdated = true;
		}
	}
}


void Object::SetFlag( const uint32 index, uint32 newFlag )
{
	//ASSERT( index < m_valuesCount );
	if( index >= m_valuesCount )
	{
		assert( 0 );
		return;
	}

	//no change -> no update
	if((m_uint32Values[ index ] & newFlag)==newFlag)
		return;

	m_uint32Values[ index ] |= newFlag;

	m_updateMask.SetBit( index );
	m_isNeedUpdate = true;

	if(IsInWorld())
	{
		if(!m_objectUpdated)
		{
			m_mapMgr->ObjectUpdated(this);
			m_objectUpdated = true;
		}
	}
}


void Object::RemoveFlag( const uint32 index, uint32 oldFlag )
{
	if( index >= m_valuesCount )
	{
		assert( 0 );
		return;
	}
	//ASSERT( index < m_valuesCount );

	//no change -> no update
	if((m_uint32Values[ index ] & oldFlag)==0)
		return;

	m_uint32Values[ index ] &= ~oldFlag;

	m_updateMask.SetBit( index );
	m_isNeedUpdate = true;

	if(IsInWorld())
	{
		if(!m_objectUpdated)
		{
			m_mapMgr->ObjectUpdated(this);
			m_objectUpdated = true;
		}
	}
}

////////////////////////////////////////////////////////////

float Object::CalcDistance(Object *Ob)
{
	return CalcDistance(this->GetPositionX(), this->GetPositionY(), this->GetPositionZ(), Ob->GetPositionX(), Ob->GetPositionY(), Ob->GetPositionZ());
}
float Object::CalcDistance(float ObX, float ObY, float ObZ)
{
	return CalcDistance(this->GetPositionX(), this->GetPositionY(), this->GetPositionZ(), ObX, ObY, ObZ);
}
float Object::CalcDistance(Object *Oa, Object *Ob)
{
	return CalcDistance(Oa->GetPositionX(), Oa->GetPositionY(), Oa->GetPositionZ(), Ob->GetPositionX(), Ob->GetPositionY(), Ob->GetPositionZ());
}
float Object::CalcDistance(Object *Oa, float ObX, float ObY, float ObZ)
{
	return CalcDistance(Oa->GetPositionX(), Oa->GetPositionY(), Oa->GetPositionZ(), ObX, ObY, ObZ);
}

float Object::CalcDistance(float OaX, float OaY, float OaZ, float ObX, float ObY, float ObZ)
{
	float xdest = OaX - ObX;
	float ydest = OaY - ObY;
	float zdest = OaZ - ObZ;
	return sqrtf(zdest*zdest + ydest*ydest + xdest*xdest);
}

float Object::calcAngle( float Position1X, float Position1Y, float Position2X, float Position2Y )
{


	float dx = Position2X - Position1X;
	float dy = Position2Y - Position1Y;

	float ang = atan2(dy, dx);
	ang = (ang >= 0) ? ang : 2 * M_PI + ang;
	return  getEasyAngle( ang );



	//float dx = Position2X-Position1X;
	//float dy = Position2Y-Position1Y;
	//double angle=0.0f;

	//// Calculate angle
	//if (dx == 0.0)
	//{
	//	if (dy == 0.0)
	//		angle = 0.0;
	//	else if (dy > 0.0)
	//		angle = M_PI * 0.5 /* / 2 */;
	//	else
	//		angle = M_PI * 3.0 * 0.5/* / 2 */;
	//}
	//else if (dy == 0.0)
	//{
	//	if (dx > 0.0)
	//		angle = 0.0;
	//	else
	//		angle = M_PI;
	//}
	//else
	//{


	//	if (dx <0.0 && dy < 0.0)
	//	{
	//		angle = atanf(dy/dx) + M_PI;
	//	}
	//	else if (dx < 0.0 && dy > 0.0 )
	//	{
	//		angle = M_PI - atanf(dy/dx);
	//	}			
	//	else if (dy < 0.0 && dx > 0.0)
	//	{
	//		angle = atanf(dy/dx) + (2*M_PI);
	//	}		
	//	else
	//	{
	//		angle = atanf(dy/dx);
	//	}
	//		
	//}

	//// Convert to degrees
	////angle = angle * float(180 / M_PI);

	//// Return
	////return float(angle);

	//return getEasyAngle( angle );
}
float Object::calcRadAngle( float Position1X, float Position1Y, float Position2X, float Position2Y )
{
	double dx = double(Position2X-Position1X);
	double dy = double(Position2Y-Position1Y);
	double angle=0.0;

	// Calculate angle
	if (dx == 0.0)
	{
		if (dy == 0.0)
			angle = 0.0;
		else if (dy > 0.0)
			angle = M_PI * 0.5/*/ 2.0*/;
		else
			angle = M_PI * 3.0 * 0.5/*/ 2.0*/;
	}
	else if (dy == 0.0)
	{
		if (dx > 0.0)
			angle = 0.0;
		else
			angle = M_PI;
	}
	else
	{
		if (dx < 0.0)
			angle = atan(dy/dx) + M_PI;
		else if (dy < 0.0)
			angle = atan(dy/dx) + (2*M_PI);
		else
			angle = atan(dy/dx);
	}

	// Return
	return float(angle);
}

float Object::getEasyAngle( float angle )
{
	angle = angle * float(180 / M_PI);

	while ( angle < 0 ) {
		angle = angle + 360;
	}
	while ( angle >= 360 ) {
		angle = angle - 360;
	}
	return angle;
}

bool Object::inArc(float Position1X, float Position1Y, float FOV, float Orientation, float Position2X, float Position2Y )
{
	Orientation = M_PI * 3.f / 2.f - Orientation;
	float angle = calcAngle( Position1X, Position1Y, Position2X, Position2Y );
	float lborder = getEasyAngle( ( Orientation - (FOV*0.5f/*/2*/) ) );
	float rborder = getEasyAngle( ( Orientation + (FOV*0.5f/*/2*/) ) );
	//MyLog::log->debug("Orientation: %f Angle: %f LeftBorder: %f RightBorder %f",Orientation,angle,lborder,rborder);
	if(((angle >= lborder) && (angle <= rborder)) || ((lborder > rborder) && ((angle < rborder) || (angle > lborder))))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Object::isInFront(Object* target)
{
	/*
	// check if we facing something ( is the object within a 180 degree slice of our positive y axis )
    double x = target->GetPositionX() - m_position.x;
    double y = target->GetPositionY() - m_position.y;

    double angle = atan2( -y, x ) - M_PI/2;
    angle = ( angle >= 0.0 ) ? angle : 2.0 * M_PI + angle;
	angle -= m_position.o;

    while( angle > M_PI)
        angle -= 2.0 * M_PI;

    while(angle < -M_PI)
        angle += 2.0 * M_PI;

	// replace M_PI in the two lines below to reduce or increase angle

    double left = -1.0 * ( M_PI / 2.0 );
    double right = ( M_PI / 2.0 );

    return( ( angle >= left ) && ( angle <= right ) );
	*/

	return inArc( m_position.x, m_position.y, M_PI, this->m_position.o, target->GetPositionX(), target->GetPositionY() );
}

bool Object::isInBack(Object* target)
{
	return !inArc( m_position.x, m_position.y, M_PI, this->m_position.o, target->GetPositionX(), target->GetPositionY() );
	/*
	// check if we are behind something ( is the object within a 180 degree slice of our negative y axis )

    double x = m_position.x - target->GetPositionX();
    double y = m_position.y - target->GetPositionY();

    double angle = atan2( y, x );
    angle = ( angle >= 0.0 ) ? angle : 2.0 * M_PI + angle;

	// if we are a unit and have a UNIT_FIELD_TARGET then we are always facing them
	if( m_objectTypeId == TYPEID_UNIT && m_uint32Values[UNIT_FIELD_TARGET] != 0 && static_cast< Unit* >( this )->GetAIInterface()->GetNextTarget() )
	{
		Unit* pTarget = static_cast< Unit* >( this )->GetAIInterface()->GetNextTarget();
		angle -= double( Object::calcRadAngle( target->m_position.x, target->m_position.y, pTarget->m_position.x, pTarget->m_position.y ) );
	}
	else
		angle -= target->GetOrientation();

    while( angle > M_PI)
        angle -= 2.0 * M_PI;

    while(angle < -M_PI)
        angle += 2.0 * M_PI;

	// replace M_H_PI in the two lines below to reduce or increase angle

    double left = -1.0 * ( M_H_PI / 2.0 );
    double right = ( M_H_PI / 2.0 );

    return( ( angle <= left ) && ( angle >= right ) );
	*/
}
bool Object::isInArc(Object* target , float angle) // angle in degrees
{
    return inArc( GetPositionX() , GetPositionY() , angle , GetOrientation() , target->GetPositionX() , target->GetPositionY() );
}

bool Object::isInRange(Object* target, float range)
{
	float dist = CalcDistance( target );
	return( dist <= range );
}

bool Object::IsPet()
{
	if( this->GetTypeId() != TYPEID_UNIT )
		return false;

	if( static_cast< Unit* >( this )->m_isPet && m_uint32Values[UNIT_FIELD_CREATEDBY] != 0 && m_uint32Values[UNIT_FIELD_SUMMONEDBY] != 0 )
		return true;

	return false;
}

void Object::_setFaction()
{
	FactionTemplateDBC* factT = NULL;

	if(GetTypeId() == TYPEID_UNIT || GetTypeId() == TYPEID_PLAYER)
	{
		factT = dbcFactionTemplate.LookupEntry(GetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE));
	}
	else
	if(GetTypeId() == TYPEID_GAMEOBJECT)
	{
		factT = dbcFactionTemplate.LookupEntry(GetUInt32Value(GAMEOBJECT_FACTION));
	}

	if(!factT)
	{
		return;
	}
	m_faction = factT;
	m_factionDBC = dbcFaction.LookupEntry(factT->Faction);
}

void Object::UpdateOppFactionSet()
{
	m_oppFactsInRange.clear();
	for(Object::InRangeSet::iterator i = GetInRangeSetBegin(); i != GetInRangeSetEnd(); ++i)
	{
		if (((*i)->GetTypeId() == TYPEID_UNIT) || ((*i)->GetTypeId() == TYPEID_PLAYER) || ((*i)->GetTypeId() == TYPEID_GAMEOBJECT))
		{
			if (isHostile(this, (*i)))
			{
				if(!(*i)->IsInRangeOppFactSet(this))
					(*i)->m_oppFactsInRange.insert(this);
				if (!IsInRangeOppFactSet((*i)))
					m_oppFactsInRange.insert((*i));

			}
			else
			{
				if((*i)->IsInRangeOppFactSet(this))
					(*i)->m_oppFactsInRange.erase(this);
				if (IsInRangeOppFactSet((*i)))
					m_oppFactsInRange.erase((*i));
			}
		}
	}
}

void Object::EventSetUInt32Value(uint32 index, uint32 value)
{
	SetUInt32Value(index,value);
}

void Object::DealDamage(Unit *pVictim, uint32 damage, uint32 targetEvent, uint32 unitEvent, uint32 spellId, bool no_remove_auras)
{
	if( spellId )
	{
		SunyouRaid* raid = pVictim->GetSunyouRaid();
		if( raid && this->IsUnit() )
		{
			raid->OnUnitHitWithSpell( spellId, static_cast<Unit*>( this ), pVictim );
		}

		if( this->IsUnit() )
			static_cast<Unit*>( this )->lastSpellDamage = damage;
	}

	if (pVictim->IsPet())
	{
		return;
	}
	if( pVictim->IsPlayer() && !((Player*)pVictim)->m_EnableMeleeAttack )
		return;
	if( damage == 0 )
		return;
	if( pVictim->IsCreature() && ((Creature*)pVictim)->GetAIInterface()->m_ForceMoveMode )
	{
		/*
		MSG_S2C::stCCResult ccmsg;
		ccmsg.result = MSG_S2C::stCCResult::_IMMUNE;
		ccmsg.casterid = this->GetGUID();
		ccmsg.victimid = pVictim->GetGUID();
		ccmsg.spellid = spellId;
		ccmsg.effect = MSG_S2C::stCCResult::_DAMAGE;

		if( this->IsPlayer() && this != pVictim )
			((Player*)this)->GetSession()->SendPacket( ccmsg );
		*/
		return;
	}
	
	if( pVictim->IsPlayer() && this->IsCreature() )
		((Unit*)this)->GetAIInterface()->AttackReaction( pVictim, 1 );

	if( pVictim->IsIceBlocking() )
	{
		MSG_S2C::stCCResult ccmsg;
		ccmsg.result = MSG_S2C::stCCResult::_IMMUNE;
		ccmsg.casterid = this->GetGUID();
		ccmsg.victimid = pVictim->GetGUID();
		ccmsg.spellid = spellId;
		ccmsg.effect = MSG_S2C::stCCResult::_DAMAGE;
		if( pVictim->IsPlayer() )
			((Player*)pVictim)->GetSession()->SendPacket( ccmsg );
		if( this->IsPlayer() && this != pVictim )
			((Player*)this)->GetSession()->SendPacket( ccmsg );
		return;
	}

	Player* plr = 0;

	if( !pVictim || !pVictim->isAlive() || !pVictim->IsInWorld() || !IsInWorld() )
		return;
	if( pVictim->GetTypeId() == TYPEID_PLAYER && static_cast< Player* >( pVictim )->GodModeCheat == true )
		return;
	if( pVictim->IsSpiritHealer() )
		return;

	uint32 addhealth = pVictim->m_fDamage2Health * damage;
	if( this->IsUnit() && pVictim->IsUnit() && pVictim != this )
	{
		// Set our attack target to the victim.
		// modified by gui
		//static_cast< Unit* >( this )->CombatStatus.OnDamageDealt( pVictim );

		if( pVictim->IsCreature() )
			pVictim->GetAIInterface()->AttackReaction( static_cast<Unit*>( this ), damage, spellId );
		else
			static_cast<Unit*>( this )->CombatStatus.OnDamageDealt( pVictim );
	}

	if( pVictim->GetStandState() )//not standing-> standup
	{
		pVictim->SetStandState( STANDSTATE_STAND );//probably mobs also must standup
	}

	// This one is easy. If we're attacking a hostile target, and we're not flagged, flag us.
	// Also, you WONT get flagged if you are dueling that person - FiShBaIt
	if( pVictim->IsPlayer() && IsPlayer() )
	{
		if( isHostile( this, pVictim ) && static_cast< Player* >( pVictim )->DuelingWith != static_cast< Player* >( this ) )
			static_cast< Player* >( this )->SetPvPFlag();

		//static_cast<Player*>( pVictim )->AddDamageTakenInfo( static_cast<Player*>( this )->GetLowGUID(), damage );

		pVictim->CombatStatus.TouchWith( (Unit*)this, true );
	}
	//If our pet attacks  - flag us.
	if( pVictim->IsPlayer() && IsPet() )
	{
		Player* owner = static_cast< Player* >( static_cast< Pet* >( this )->GetPetOwner() );
		if( owner != NULL )
		{
			if( owner->isAlive() && static_cast< Player* >( pVictim )->DuelingWith != owner )
				owner->SetPvPFlag();

			pVictim->CombatStatus.TouchWith( owner, true );
			//static_cast<Player*>( pVictim )->AddDamageTakenInfo( owner->GetLowGUID(), damage );
		}
	}

	if( pVictim->IsCreature() && ( IsPlayer() || IsPet() ) )
	{
		Player* tempPlayer = NULL;
		if( this->IsPet() )
			tempPlayer = static_cast<Pet*>( this )->GetPetOwner();
		else
			tempPlayer = static_cast<Player*>( this );
		if( tempPlayer )
		{
			Guild* g = tempPlayer->GetGuild();
			if( static_cast<Creature*>( pVictim )->m_is_castle_boss )
			{
				uint32 guildid = 0;
				if( g )
				{
					guildid = g->GetGuildId();
				}
				std::map<uint32, uint32>::iterator it = static_cast<Creature*>( pVictim )->m_damage_taken_record.find( guildid );
				if( it != static_cast<Creature*>( pVictim )->m_damage_taken_record.end() )
					it->second += damage;
				else
					static_cast<Creature*>( pVictim )->m_damage_taken_record[guildid] = damage;

			}
		}
	}

	if(!no_remove_auras)
	{
		//zack 2007 04 24 : root should not remove self (and also other unknown spells)
		if(spellId)
		{
			pVictim->RemoveAurasByInterruptFlagButSkip(AURA_INTERRUPT_ON_ANY_DAMAGE_TAKEN,spellId);
			if(Rand(35.0f))
				pVictim->RemoveAurasByInterruptFlagButSkip(AURA_INTERRUPT_ON_UNUSED2,spellId);
		}
		else
		{
			pVictim->RemoveAurasByInterruptFlag(AURA_INTERRUPT_ON_ANY_DAMAGE_TAKEN);
			if(Rand(35.0f))
				pVictim->RemoveAurasByInterruptFlag(AURA_INTERRUPT_ON_UNUSED2);
		}
	}
	if(this->IsUnit())
	{
/*		if(!pVictim->isInCombat() && pVictim->IsPlayer())
			sHookInterface.OnEnterCombat( static_cast< Player* >( pVictim ), static_cast< Unit* >( this ) );

		if(IsPlayer() && ! static_cast< Player* >( this )->isInCombat())
			sHookInterface.OnEnterCombat( static_cast< Player* >( this ), static_cast< Player* >( this ) );*/

		//the black sheep , no actually it is paladin : Ardent Defender
		if(static_cast<Unit*>(this)->DamageTakenPctModOnHP35 && HasFlag(UNIT_FIELD_AURASTATE , AURASTATE_FLAG_HEALTH35) )
			damage = damage - float2int32(damage * static_cast<Unit*>(this)->DamageTakenPctModOnHP35) / 100 ;

		plr = 0;
		if(IsPet())
			plr = static_cast<Pet*>(this)->GetPetOwner();
		else if(IsPlayer())
			plr = static_cast< Player* >( this );

		if( plr && plr->m_sunyou_instance && plr->m_sunyou_instance->IsBattleGround() )
		{
			SunyouBattleGroundBase* pBGBase = plr->m_sunyou_instance->GetBGBase();
			if( pBGBase )
			{
				pBGBase->OnBGPlayerDamage( plr, damage );
			}
		}

		if(pVictim->GetTypeId()==TYPEID_UNIT && plr && plr->GetTypeId() == TYPEID_PLAYER) // Units can't tag..
		{
			// Tagging
			Creature *victim = static_cast<Creature*>(pVictim);
			bool taggable;
			if(victim->GetCreatureName() && victim->GetCreatureName()->Type == CRITTER || victim->IsPet())
				taggable = false;
			else taggable = true;

			if(!victim->Tagged && taggable)
			{
				victim->Tagged = true;
				victim->TaggerGuid = plr->GetGUID();

				/* set loot method */
				if( plr->GetGroup() != NULL )
					victim->m_lootMethod = plr->GetGroup()->GetMethod();

				// For new players who get a create object
				uint32 Flags = pVictim->m_uint32Values[UNIT_DYNAMIC_FLAGS];
				Flags |= U_DYN_FLAG_TAPPED_BY_PLAYER;

				pVictim->m_uint32Values[UNIT_DYNAMIC_FLAGS] |= U_DYN_FLAG_TAGGED_BY_OTHER;

				// Update existing players.
				static ByteBuffer buf(500);
				buf.clear();

				static ByteBuffer buf1(500);
				buf1.clear();

				pVictim->BuildFieldUpdatePacket(&buf1, UNIT_DYNAMIC_FLAGS, Flags);
				pVictim->BuildFieldUpdatePacket(&buf, UNIT_DYNAMIC_FLAGS, pVictim->m_uint32Values[UNIT_DYNAMIC_FLAGS]);

				// Loop inrange set, append to their update data.
				for(std::set<Player*>::iterator itr = m_inRangePlayers.begin(); itr != m_inRangePlayers.end(); ++itr)
				{
					Player* ply = *itr;
					if (static_cast< Player* >(plr)->InGroup())
					{
						if (ply->GetGroup() && plr->GetGroup()->GetID() == ply->GetGroup()->GetID())
							ply->PushUpdateData(&buf1, 1);
						else
							ply->PushUpdateData(&buf, 1);
					}
					else
					{
						ply->PushUpdateData(&buf, 1);
					}
				}

				// Update ourselves
				plr->PushUpdateData(&buf1, 1);
			}
		}
	}

	if( pVictim->IsPlayer() )
	{
		Player *pThis = static_cast< Player* >(pVictim);
		if(pThis->cannibalize)
		{
			sEventMgr.RemoveEvents(pVictim, EVENT_CANNIBALIZE);
			pThis->SetUInt32Value(UNIT_NPC_EMOTESTATE, 0);
			pThis->cannibalize = false;
		}
		//pThis->m_incombattimeleft = 5000;

		if( pThis->GetUInt64Value( UNIT_FIELD_TARGET ) == 0 && !pThis->GetUInt64Value( PLAYER_HEAD ) )
		{
			pThis->SetUInt64Value( UNIT_FIELD_TARGET, GetGUID() );
			pThis->SetTarget( GetGUID() );
		}
	}

	//* BATTLEGROUND DAMAGE COUNTER *//
	if( pVictim != this )
	{
		if( IsPlayer() )
		{
			plr = static_cast< Player* >( this );
		}
		else if( IsPet() )
		{
			plr = static_cast< Pet* >( this )->GetPetOwner();
			if (plr)
				if( plr != NULL && plr->GetMapMgr() == GetMapMgr() )
					plr = NULL;
		}
	}

	uint32 health = pVictim->GetUInt32Value(UNIT_FIELD_HEALTH );

	/*------------------------------------ DUEL HANDLERS --------------------------*/
	if((pVictim->IsPlayer()) && (this->IsPlayer()) && static_cast< Player* >(pVictim)->DuelingWith == static_cast< Player* >( this ) ) //Both Players
	{
		if((health <= damage) && static_cast< Player* >( this )->DuelingWith != NULL)
		{
			//Player in Duel and Player Victim has lost
			uint32 NewHP = pVictim->GetUInt32Value(UNIT_FIELD_MAXHEALTH)/100;

			if(NewHP < 5)
				NewHP = 5;

			//Set there health to 1% or 5 if 1% is lower then 5
			pVictim->SetUInt32Value(UNIT_FIELD_HEALTH, NewHP);
			//End Duel
			static_cast< Player* >( this )->EndDuel(DUEL_WINNER_KNOCKOUT);

			// surrender emote
			pVictim->Emote(EMOTE_ONESHOT_BEG);			// Animation

			return;
		}
	}

	if((pVictim->IsPlayer()) && (IsPet()))
	{
		if((health <= damage) && static_cast< Player* >(pVictim)->DuelingWith == static_cast<Pet*>(this)->GetPetOwner())
		{
			Player *petOwner = static_cast<Pet*>(this)->GetPetOwner();
			if(petOwner)
			{
				//Player in Duel and Player Victim has lost
				uint32 NewHP = pVictim->GetUInt32Value(UNIT_FIELD_MAXHEALTH)/100;
				if(NewHP < 5) NewHP = 5;

				//Set there health to 1% or 5 if 1% is lower then 5
				pVictim->SetUInt32Value(UNIT_FIELD_HEALTH, NewHP);
				//End Duel
				petOwner->EndDuel(DUEL_WINNER_KNOCKOUT);
				return;
			}
		}
	}
	/*------------------------------------ DUEL HANDLERS END--------------------------*/

	bool isCritter = false;
	if(pVictim->GetTypeId() == TYPEID_UNIT && ((Creature*)pVictim)->GetCreatureName())
	{
			if(((Creature*)pVictim)->GetCreatureName()->Type == CRITTER)
				isCritter = true;
	}

	if( pVictim->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_FLEEING) )
	{
		pVictim->m_fearDamageTaken += damage;
		if( (float)pVictim->m_fearDamageTaken >= ((float)pVictim->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) * 0.15f) )
		{
			pVictim->m_fearDamageTaken = 0;

			pVictim->RemoveAllAurasByMechanic(MECHANIC_FLEEING, -1, true);
		}
	}

	/* -------------------------- HIT THAT CAUSES VICTIM TO DIE ---------------------------*/
	if ((isCritter || health <= damage) )
	{
		//warlock - seed of corruption
		if( IsUnit() )
		{
			SpellEntry *killerspell;
			if( spellId )
				killerspell = dbcSpell.LookupEntry( spellId );
			else killerspell = NULL;
			pVictim->HandleProc( PROC_ON_DIE, static_cast< Unit* >( this ), killerspell );
			pVictim->m_procCounter = 0;
			static_cast< Unit* >( this )->HandleProc( PROC_ON_TARGET_DIE, pVictim, killerspell );
			static_cast< Unit* >( this )->m_procCounter = 0;
		}
		// check if pets owner is combat participant
		bool owner_participe = false;
		if( IsPet() )
		{
			Player* owner = static_cast<Pet*>( this )->GetPetOwner();
			if( owner != NULL && pVictim->GetAIInterface()->getThreatByPtr( owner ) > 0 )
				owner_participe = true;
		}
		/* victim died! */
		if( pVictim->IsPlayer() )
			static_cast< Player* >( pVictim )->KillPlayer();
		else
		{
			pVictim->setDeathState( JUST_DIED );
			pVictim->GetAIInterface()->HandleEvent( EVENT_LEAVECOMBAT, static_cast< Unit* >( this ), 0);
		}
//
// 		if(IsPlayer())
// 		{
// 			((Player*)this)->m_inarmedtimeleft = 5000;
// 		}

		if( pVictim->IsPlayer() )
		{
			if( IsCreature() && !((Creature*)this)->m_gardenmode && !((Creature*)this)->m_castlemode )
				static_cast< Player* >( pVictim )->DeathDurabilityLoss(0.10);
			else if( pVictim == this )
				static_cast< Player* >( pVictim )->DeathDurabilityLoss(0.10);
		}

		/* Zone Under Attack */
		/*
        MapInfo * pMapInfo = WorldMapInfoStorage.LookupEntry(GetMapId());
        if( pMapInfo && pMapInfo->type == INSTANCE_NULL && !pVictim->IsPlayer() && !pVictim->IsPet() && ( IsPlayer() || IsPet() ) )
		{
			// Only NPCs that bear the PvP flag can be truly representing their faction.
			if( ((Creature*)pVictim)->HasFlag( UNIT_FIELD_FLAGS, UNIT_FLAG_PVP ) )
			{
				Player * pAttacker = NULL;
				if( IsPet() )
					pAttacker = static_cast< Pet* >( this )->GetPetOwner();
				else if(IsPlayer())
					pAttacker = static_cast< Player* >( this );

				if( pAttacker != NULL)
                {
				    uint8 teamId = (uint8)pAttacker->GetTeam();
				    if(teamId == 0) // Swap it.
					    teamId = 1;
				    else
					    teamId = 0;
				    uint32 AreaID = pVictim->GetMapMgr()->GetAreaID(pVictim->GetPositionX(), pVictim->GetPositionY());
				    if(!AreaID)
					    AreaID = pAttacker->GetZoneId(); // Failsafe for a shitty TerrainMgr

				    if(AreaID)
				    {
						MSG_S2C::stAttack_Zone_Under_Attack Msg;
						Msg.AreaID = AreaID;
					    sWorld.SendFactionMessage(Msg, teamId);
				    }
                }
			}
		}

		if(pVictim->GetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT) > 0)
		{
			if(pVictim->GetCurrentSpell())
			{
				Spell *spl = pVictim->GetCurrentSpell();
				for(int i = 0; i < 3; i++)
				{
					if(spl->m_spellInfo->Effect[i] == SPELL_EFFECT_PERSISTENT_AREA_AURA)
					{
						DynamicObject *dObj = GetMapMgr()->GetDynamicObject(pVictim->GetUInt32Value(UNIT_FIELD_CHANNEL_OBJECT));
						if(!dObj)
							return;

						MSG_S2C::stGameObj_Despawn_Anim Msg;
						Msg.guid = dObj->GetGUID();
						dObj->SendMessageToSet(Msg, false);
						dObj->RemoveFromWorld(true);
						delete dObj;
					}
				}
				if(spl->m_spellInfo->ChannelInterruptFlags == 48140) spl->cancel();
			}
		}
		*/

		/* Remove all Auras */
		pVictim->DropAurasOnDeath();

		/* Stop players from casting */
		std::set<Player*>::iterator itr;
		for( itr = pVictim->GetInRangePlayerSetBegin() ; itr != pVictim->GetInRangePlayerSetEnd() ; itr ++ )
		{
			Player* ply = *itr;
			//if player has selection on us

			if (ply->GetMapMgr()&&ply->GetCurrentSpell()&& ply->GetCurrentSpell()->m_spellInfo && ply->GetCurrentSpell()->m_spellInfo->ChannelInterruptFlags)
			{
				DynamicObject* dynObj=ply->GetMapMgr()->GetDynamicObject(ply->GetUInt32Value(UNIT_FIELD_CHANNEL_OBJECT));
				if (dynObj == NULL)
				{
					Unit* pUnit = ply->GetMapMgr()->GetUnit(ply->GetUInt32Value(UNIT_FIELD_CHANNEL_OBJECT));

					if (!pUnit || (pUnit&&pUnit->GetGUID() == pVictim->GetGUID()))
					{
						if( ply->isCasting() )
							ply->CancelSpell( NULL ); //cancel current casting spell
					}
				}
			}

			/*

			if( ply->GetSelection()==pVictim->GetGUID())
			{


				if (ply->GetCurrentSpell()->)
				{
				}
				if( ply->isCasting() )
					ply->CancelSpell( NULL ); //cancel current casting spell
			}
			*/
		}
		/* Stop victim from attacking */
		if( this->IsUnit() )
			pVictim->smsg_AttackStop( static_cast< Unit* >( this ) );

		if( pVictim->GetTypeId() == TYPEID_PLAYER )
			static_cast< Player* >( pVictim )->EventAttackStop();

		/* Set victim health to 0 */

		if(pVictim->IsPlayer())
		{
			uint32 self_res_spell = static_cast< Player* >( pVictim )->SoulStone;
			static_cast< Player* >( pVictim )->SoulStone = static_cast< Player* >( pVictim )->SoulStoneReceiver = 0;

			if( !self_res_spell && static_cast< Player* >( pVictim )->bReincarnation )
			{
				SpellEntry* m_reincarnSpellInfo = dbcSpell.LookupEntry( 20608 );
				if( static_cast< Player* >( pVictim )->Cooldown_CanCast( m_reincarnSpellInfo ) )
				{
					uint32 ankh_count = static_cast< Player* >( pVictim )->GetItemInterface()->GetItemCount( 17030 );
					if( ankh_count )
						self_res_spell = 21169;
				}
			}
			pVictim->SetUInt32Value( PLAYER_SELF_RES_SPELL, self_res_spell );
			pVictim->SetUInt32Value( UNIT_FIELD_MOUNTDISPLAYID , 0 );
			//pVictim->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_MOUNTED_TAXI);
		}
		//else
			pVictim->SetUInt32Value(UNIT_FIELD_HEALTH, 0);

		/* -------------------------------- HONOR + BATTLEGROUND CHECKS ------------------------ */
		plr = NULL;
		if( IsPlayer() )
			plr = static_cast< Player* >( this );
		else if(IsPet())
			plr = static_cast< Pet* >( this )->GetPetOwner();

		if( this->IsCreature() && pVictim->IsPlayer() )
		{
			((Creature*)this)->SayEventKillPlayer();
		}

		if( plr != NULL)
		{
			if( plr->GetFFA() == 1 )
				plr->IncreaseArenaKill();

			if( pVictim->IsPlayer() )
			{
				//if( plr->GetFFA() == 0 && static_cast<Player*>( pVictim )->GetFFA() == 0 )
				if( !( plr->m_sunyou_instance && plr->m_sunyou_instance->GetCategory() == INSTANCE_CATEGORY_UNIQUE_CASTLE ) )
					HonorHandler::OnPlayerKilled(plr, static_cast<Player*>( pVictim ));
				//sHookInterface.OnKillPlayer( plr, static_cast< Player* >( pVictim ) );
				/*
				if(plr->getLevel() > pVictim->getLevel())
				{
					unsigned int diff = plr->getLevel() - pVictim->getLevel();
					if( diff <= 8 )
					{
						HonorHandler::OnPlayerKilledUnit(plr, pVictim);
						SetFlag( UNIT_FIELD_AURASTATE, AURASTATE_FLAG_LASTKILLWITHHONOR );
					}
					else
						RemoveFlag( UNIT_FIELD_AURASTATE, AURASTATE_FLAG_LASTKILLWITHHONOR );
				}
				else
				{
					HonorHandler::OnPlayerKilledUnit( plr, pVictim );
					SetFlag( UNIT_FIELD_AURASTATE, AURASTATE_FLAG_LASTKILLWITHHONOR );
				}
				*/
				
				if( ( plr->GetHostileGuild() && plr->GetHostileGuild() == static_cast<Player*>( pVictim )->GetGuildId() )
					|| ( static_cast<Player*>( pVictim )->GetHostileGuild() && static_cast<Player*>( pVictim )->GetHostileGuild() == plr->GetGuildId() ) )
				{
					Guild* pg = plr->GetGuild();
					Player* pVictimPlayer = static_cast<Player*>( pVictim );
					Guild* pVg =  pVictimPlayer->GetGuild();
					if( pg && pVg)
					{
						if (pVictimPlayer->getLevel() >= 75)
						{
							int DifLevel = pVictimPlayer->getLevel() - plr->getLevel();
							int DifGuildLevel = pg->GetHiddenScore() - pVg->GetHiddenScore();
							if (DifLevel < -5 || DifGuildLevel > 200 )
							{
								
							}
							else
							{
								int score = 1;
								if (DifLevel >5 || (DifGuildLevel < -100 && DifLevel > 0))
								{
									score = 2;
								}

								MyLog::log->info( "player:[%s] killed hostile player:[%s] get guild contribution points:1", plr->GetName(), pVictimPlayer->GetName() );
								MyLog::log->info( "player:[%s] killed hostile player:[%s].guild:[%s] get score:1", plr->GetName(), pVictimPlayer->GetName(), pVg->GetGuildName() );
								pg->AddGuildScore( score );
								pg->AddGuildHiddenScore(score);
								if (pVictimPlayer->GetSession())
								{
									pVictimPlayer->GetSession()->SendNotification( 
										build_language_string( BuildString_PlayerKillHostileGuildPlayer ,pg->GetGuildName(), plr->GetName(), score));
								}

								if (plr->GetSession())
								{
									plr->GetSession()->SendNotification( 
										build_language_string( BuildString_PlayerBeKilledByHostileGuildPlayer ,pVg->GetGuildName(), pVictimPlayer->GetName() , score ));
								}
								
								pVg->SpendScore(score);
								pVg->SpendHiddenScore(score);

								//pg->AddMemberContributionPoints( plr->m_playerInfo, 1 );
							}
						}
					}
				}
				
			}
			else if( pVictim->IsCreature() )
			{
				//if (!isCritter) // REPUTATION
				//{
				//	RemoveFlag( UNIT_FIELD_AURASTATE, AURASTATE_FLAG_LASTKILLWITHHONOR );
				//}
				if( plr->m_playerInfo->guild )
				{
					guild_kill_bonus* gkb = dbcGuildKillBonusList.LookupEntry( pVictim->GetEntry() );
					if( gkb )
					{
						MyLog::log->info( "player:[%s] killed monster:[%s] get guild contribution points:[%d]", plr->GetName(), ((Creature*)pVictim)->GetCreatureName(), gkb->points );
						MyLog::log->info( "player:[%s] killed monster:[%s].guild:[%s] get score:[%d]", plr->GetName(), ((Creature*)pVictim)->GetCreatureName(), plr->m_playerInfo->guild->GetGuildName(), gkb->score );

						//plr->GetSession()->SendNotification( "您的部族获得%d点积分，您个人获得%d点贡献值", gkb->score, gkb->points );
						plr->GetSession()->SendNotification( build_language_string( BuildString_GuildKillBossBonus, quick_itoa(gkb->score), quick_itoa(gkb->points) ) );
						plr->m_playerInfo->guild->AddGuildScore( gkb->score );
						plr->m_playerInfo->guild->AddMemberContributionPoints( plr->m_playerInfo, gkb->points );
					}
				}
				((Creature*)pVictim)->OnDefeated( this );

				HonorHandler::OnCreatureKilled(plr, static_cast<Creature*>( pVictim ));
			}
		}
		/* -------------------------------- HONOR + BATTLEGROUND CHECKS END------------------------ */

		// Wipe our attacker set on death
		pVictim->CombatStatus.Vanished();
		pVictim->RemoveAllAuras(false);

		//		 sent to set. don't send it to the party, becuase if they're out of
		//		 range they won't know this guid exists -> possible 132.

		/*if (this->IsPlayer())
			if( static_cast< Player* >( this )->InGroup() )
				static_cast< Player* >( this )->GetGroup()->SendPartyKillLog( this, pVictim );*/

		/* Stop Unit from attacking */
		if( this->IsPlayer() )
			static_cast< Player* >( this )->EventAttackStop();

		if( this->IsUnit() )
		{
			CALL_SCRIPT_EVENT( this, OnTargetDied )( pVictim );
			static_cast< Unit* >( this )->smsg_AttackStop( pVictim );

			/* Tell Unit that it's target has Died */
			static_cast< Unit* >( this )->addStateFlag( UF_TARGET_DIED );

			// We will no longer be attacking this target, as it's dead.
			//static_cast<Unit*>(this)->setAttackTarget(NULL);
		}

		uint64 victimGuid = pVictim->GetGUID();

		if( pVictim->IsPlayer() )
		{
			Player* pv = static_cast<Player*>( pVictim );
			if( pv->IsInWorld() )
			{
				if( pv->GetMapMgr()->m_sunyouinstance )
					pv->GetMapMgr()->m_sunyouinstance->OnPlayerDie( pv, this );
			}
		}

		if(pVictim->GetTypeId() == TYPEID_UNIT)
		{
			pVictim->GetAIInterface()->OnDeath(this);
			if(GetTypeId() == TYPEID_PLAYER)
			{
				MSG_S2C::stParty_Kill_Log Msg;
				Msg.playerguid = GetGUID();
				Msg.unitguid = pVictim->GetGUID();
				SendMessageToSet(Msg, true);
			}

			// it Seems that pets some how dont get a name and cause a crash here
			//bool isCritter = (pVictim->GetCreatureName() != NULL)? pVictim->GetCreatureName()->Type : 0;

			//-----------------------------------LOOOT--------------------------------------------
			if ((!pVictim->IsPet())&& ( !isCritter ))
			{
				Creature * victim = static_cast<Creature*>(pVictim);

				uint32 addgold=0,additemchance=0;

				if (this->IsPlayer())
				{
					addgold = static_cast< Player* >( this )->m_addgold;
					additemchance = static_cast< Player* >( this )->m_additemchance;
				}

				// fill loot vector.
				victim->generateLoot(addgold, additemchance);

				if( victim->m_is_castle_boss && victim->m_castle )
				{
					victim->m_castle->OnBossDie( victim );
				}
				/*
				if( victim->m_battleground && victim->m_battlepower )
					victim->m_battleground->OnCreatureDie( victim, this );
					*/
				if( victim->GetMapMgr()->m_sunyouinstance )
					victim->GetMapMgr()->m_sunyouinstance->OnCreatureDie( victim, this );

				if( victim->m_escort_players.size() && victim->m_escort_qst )
				{
					sQuestMgr.OnEscortCreatureDie( victim );
				}

				Player *owner = 0;
				if(victim->TaggerGuid)
					owner = GetMapMgr()->GetPlayer( (uint32)victim->TaggerGuid );
				else if( this->IsGameObject() )
				{
					if( ((GameObject*)this)->m_summoner && ((GameObject*)this)->m_summoner->IsPlayer() )
					{
						owner = (Player*)(((GameObject*)this)->m_summoner);
						victim->TaggerGuid = owner->GetGUID();
					}
				}

				if(owner == 0)  // no owner
				{
					// donno why this would happen, but anyway.. anyone can loot ;p
					// no owner no loot
					//victim->SetFlag(UNIT_DYNAMIC_FLAGS, U_DYN_FLAG_LOOTABLE);
				}
				else
				{
					// Build the actual update.
					static ByteBuffer buf( 500 );
					buf.clear();

					uint32& Flags = victim->m_uint32Values[ UNIT_DYNAMIC_FLAGS ];
					int n = sAIInterfaceManager.FindNewObj(victim->GetUniqueIDForLua(), "Reborn");
					if ( n < 0)
					{

						Flags |= U_DYN_FLAG_LOOTABLE;
						Flags |= U_DYN_FLAG_TAPPED_BY_PLAYER;

					}
					victim->BuildFieldUpdatePacket( &buf, UNIT_DYNAMIC_FLAGS, Flags );
					//victim->SetUInt32Value( UNIT_DYNAMIC_FLAGS, Flags );



					// Check for owner's group.
					Group * pGroup = owner->GetGroup();
					if( pGroup != NULL )
					{
						// Owner was in a party.
						// Check loot method.
						switch( pGroup->GetMethod() )
						{
						case PARTY_LOOT_RR:
/*						//this commented code is not used because it was never tested and finished !
						{
								//get new tagger for creature
								Player *tp = pGroup->GetnextRRlooter();
								if(tp)
								{
									//we force on creature a new tagger
									victim->TaggerGuid = tp->GetGUID();
									victim->Tagged = true;
									if(tp->IsVisible(victim))  // Save updates for non-existant creatures
										tp->PushUpdateData(&buf, 1);
								}
							}break;*/
						case PARTY_LOOT_FFA:
						case PARTY_LOOT_GROUP:
						case PARTY_LOOT_NBG:
							{
								// Loop party players and push update data.
								GroupMembersSet::iterator itr;
								SubGroup * sGrp;
								for( uint32 Index = 0; Index < pGroup->GetSubGroupCount(); ++Index )
								{
									sGrp = pGroup->GetSubGroup( Index );
									itr = sGrp->GetGroupMembersBegin();
									for( ; itr != sGrp->GetGroupMembersEnd(); ++itr )
									{
										PlayerInfo* plyinfo = *itr;
										if( plyinfo->m_loggedInPlayer && plyinfo->m_loggedInPlayer->IsVisible( victim ) )	   // Save updates for non-existant creatures
										{
											plyinfo->m_loggedInPlayer->PushUpdateData( &buf, 1 );
											plyinfo->m_loggedInPlayer->ProcessPendingUpdates();
										}
									}
								}
							}break;
						case PARTY_LOOT_MASTER:
							{
								// Master loot: only the loot master gets the update.
								Player * pLooter = pGroup->GetLooter() ? pGroup->GetLooter()->m_loggedInPlayer : NULL;
								if( pLooter == NULL )
									pLooter = pGroup->GetLeader()->m_loggedInPlayer;

								if( pLooter->IsVisible( victim ) )  // Save updates for non-existant creatures
								{
									pLooter->PushUpdateData( &buf, 1 );
									pLooter->ProcessPendingUpdates();
								}

							}break;
						}
					}
					else
					{
						// Owner killed the mob solo.
						if( owner->IsVisible( victim ) )
						{
							owner->PushUpdateData( &buf, 1 );
							owner->ProcessPendingUpdates();
						}
					}
				}
			}
			//---------------------------------looot-----------------------------------------

			if( GetTypeId() == TYPEID_PLAYER &&
				pVictim->GetUInt64Value( UNIT_FIELD_CREATEDBY ) == 0 &&
				pVictim->GetUInt64Value( OBJECT_FIELD_CREATED_BY ) == 0 &&
				!pVictim->IsPet() )
			{
				// TODO: lots of casts are bad make a temp member pointer to use for batches like this
				// that way no local loadhitstore and its just one assignment

				Player* xpowner = NULL;
				xpowner = pVictim->GetMapMgr()->GetPlayer(pVictim->m_firststrikeby);
				if(!xpowner)
					xpowner = static_cast< Player* >( this );

				uint32 title_index = 0;
				if(pVictim->IsCreature())
				{
					switch ( static_cast< Creature* >(pVictim)->creature_info->Id )
					{
					case 110002010:title_index = 21;break;
					case 110002034:title_index = 22;break;
					case 110002043:title_index = 23;break;
					case 110002060:title_index = 24;break;
					case 110002069:title_index = 25;break;
					case 110002105:title_index = 26;break;
					case 110000313:title_index = 33;break;
					case 110000314:title_index = 34;break;
					case 110000315:title_index = 35;break;
					case 110000316:title_index = 36;break;

					case 110000369:title_index = 116;break;
					case 110000370:title_index = 117;break;
					case 110000371:title_index = 118;break;
					case 110000372:title_index = 119;break;
					case 110000373:title_index = 120;break;
					}
				}

				// Is this player part of a group
				if( static_cast< Player* >( this)->InGroup() )
				{
					if(title_index)
					{
						Group* g = ((Player*)this)->GetGroup();
						GroupMembersSet::iterator itr;
						for(uint32 i = 0; i < g->GetSubGroupCount(); i++) {
							for(itr = g->GetSubGroup(i)->GetGroupMembersBegin(); itr != g->GetSubGroup(i)->GetGroupMembersEnd(); ++itr)
							{
								Player* pGroupGuy = (*itr)->m_loggedInPlayer;
								if( pGroupGuy && pGroupGuy->IsInRangeSet( pVictim ) )
								{
									pGroupGuy->InsertTitle(title_index);
									MSG_S2C::stTitleAdd msg;
									msg.title.title = title_index;
									pGroupGuy->GetSession()->SendPacket(msg);
								}
							}
						}
					}

					//Calc Group XP
					static_cast< Player* >( this )->GiveGroupXP( pVictim, static_cast< Player* >( this ) );
					//TODO: pet xp if player in group
				}
				else
				{
					((Player*)this)->InsertTitle(title_index);
					MSG_S2C::stTitleAdd msg;
					msg.title.title = title_index;
					((Player*)this)->GetSession()->SendPacket(msg);

					uint32 xp = CalculateXpToGive( pVictim, xpowner );
					if( xp > 0 )
					{
						if( pVictim->IsCreature() && pVictim->IsInWorld() && pVictim->GetMapMgr()->m_sunyouinstance )
							xpowner->GiveXP( xp, victimGuid, true, GIVE_EXP_TYPE_DYNAMIC_INSTANCE );
						else
							xpowner->GiveXP( xp, victimGuid, true, GIVE_EXP_TYPE_KILL_MONSTER );
						if( xpowner->GetSummon() && xpowner->GetSummon()->GetUInt32Value( UNIT_CREATED_BY_SPELL ) == 0 )
						{
							xp = CalculateXpToGive( pVictim, xpowner->GetSummon() );
							if( xp > 0 )
								xpowner->GetSummon()->GiveXP( xp );
						}
					}
				}

				if( pVictim->GetTypeId() != TYPEID_PLAYER )
					sQuestMgr.OnPlayerKill( xpowner, static_cast< Creature* >( pVictim ) );
			}
			else /* is Creature or GameObject*/
			{
				/* ----------------------------- PET XP HANDLING -------------- */
				if( owner_participe && IsPet() && !pVictim->IsPet() )
				{
					Player* petOwner = static_cast< Pet* >( this )->GetPetOwner();
					if( petOwner != NULL && petOwner->GetTypeId() == TYPEID_PLAYER )
					{
						if( petOwner->InGroup() )
						{
							//Calc Group XP
							static_cast< Unit* >( this )->GiveGroupXP( pVictim, petOwner );
							//TODO: pet xp if player in group
						}
						else
						{
							uint32 xp = CalculateXpToGive( pVictim, petOwner );
							if( xp > 0 )
							{
								if( pVictim->IsCreature() && pVictim->IsInWorld() && pVictim->GetMapMgr()->m_sunyouinstance )
									petOwner->GiveXP( xp, victimGuid, true, GIVE_EXP_TYPE_DYNAMIC_INSTANCE );
								else
									petOwner->GiveXP( xp, victimGuid, true, GIVE_EXP_TYPE_KILL_MONSTER );

								if( !static_cast< Pet* >( this )->IsSummon() )
								{
									xp = CalculateXpToGive( pVictim, static_cast< Pet* >( this ) );
									if( xp > 0 )
										static_cast< Pet* >( this )->GiveXP( xp );
								}
							}
						}
					}
					if( petOwner != NULL && pVictim->GetTypeId() != TYPEID_PLAYER &&
						pVictim->GetTypeId() == TYPEID_UNIT )
					{
						sQuestMgr.OnPlayerKill( petOwner, static_cast< Creature* >( pVictim ) );						
					}
				}
				/* ----------------------------- PET XP HANDLING END-------------- */

				/* ----------------------------- PET DEATH HANDLING -------------- */
				if( pVictim->IsPet() )
				{
					// dying pet looses 1 happiness level
					if( !static_cast< Pet* >( pVictim )->IsSummon() )
					{
						uint32 hap = static_cast< Pet* >( pVictim )->GetUInt32Value( UNIT_FIELD_POWER1 );
						hap = hap - PET_HAPPINESS_UPDATE_VALUE > 0 ? hap - PET_HAPPINESS_UPDATE_VALUE : 0;
						static_cast< Pet* >( pVictim )->SetUInt32Value( UNIT_FIELD_POWER1, hap );
					}

					static_cast< Pet* >( pVictim )->DelayedRemove( false, true );

					//remove owner warlock soul link from caster
					Player* owner = static_cast<Pet*>( pVictim )->GetPetOwner();
					if( owner != NULL )
						owner->EventDismissPet();
				}
				/* ----------------------------- PET DEATH HANDLING END -------------- */
				else if( pVictim->GetUInt64Value( UNIT_FIELD_CHARMEDBY ) )
				{
					//remove owner warlock soul link from caster
					Unit *owner=pVictim->GetMapMgr()->GetUnit( pVictim->GetUInt64Value( UNIT_FIELD_CHARMEDBY ) );
					if( owner != NULL && owner->IsPlayer())
						static_cast< Player* >( owner )->EventDismissPet();
				}
			}
		}
		else if( pVictim->GetTypeId() == TYPEID_PLAYER )
		{

			/* -------------------- RESET BREATH STATE ON DEATH -------------- */
			static_cast< Player* >( pVictim )->m_UnderwaterTime = 0;
			static_cast< Player* >( pVictim )->m_UnderwaterState = 0;
			static_cast< Player* >( pVictim )->m_BreathDamageTimer = 0;
			static_cast< Player* >( pVictim )->m_SwimmingTime = 0;

			/* -------------------- KILL PET WHEN PLAYER DIES ---------------*/
			if( static_cast< Player* >( pVictim )->GetSummon() != NULL )
			{
				if( pVictim->GetUInt32Value( UNIT_CREATED_BY_SPELL ) > 0 )
					static_cast< Player* >( pVictim )->GetSummon()->Dismiss( true );
				else
				{
					static_cast< Player* >( pVictim )->GetSummon()->Remove( true, true, true );
				}
			}
			/* -------------------- KILL PET WHEN PLAYER DIES END---------------*/
		}
		else MyLog::log->error("DealDamage for Unknown Object.");
	}
	else /* ---------- NOT DEAD YET --------- */
	{
		if(pVictim != this /* && updateskill */)
		{
			// Send AI Reaction UNIT vs UNIT
//			if( GetTypeId() ==TYPEID_UNIT )
//			{
//				static_cast< Unit* >( this )->GetAIInterface()->AttackReaction( pVictim, damage, spellId );
//			}

			// Send AI Victim Reaction
			if( this->IsPlayer() || this->GetTypeId() == TYPEID_UNIT )
			{
				if( pVictim->IsCreature() )
				{
					static_cast< Creature* >( pVictim )->GetAIInterface()->AttackReaction( static_cast< Unit* >( this ), damage, spellId );
				}
			}
			else if( this->IsGameObject() )
			{
				if( pVictim->IsCreature() )
				{
					static_cast< Creature* >( pVictim )->GetAIInterface()->AttackReaction( ((GameObject*)this)->m_summoner, damage, spellId );
				}
			}
		}

		// TODO: Mark victim as a HK
		/*if( static_cast< Player* >( pVictim )->GetCurrentBattleground() != NULL && static_cast< Player* >( this )->GetCurrentBattleground() != NULL)
		{

		}*/

		pVictim->SetUInt32Value( UNIT_FIELD_HEALTH, health - damage + addhealth );
	}
}

void Object::SpellNonMeleeDamageLog(Unit *pVictim, uint32 spellID, uint32 damage, bool allowProc, bool static_damage, 
									bool no_remove_auras, uint16 damageseq, bool stato, float overWrite )
{
//==========================================================================================
//==============================Unacceptable Cases Processing===============================
//==========================================================================================
	if(!pVictim || !pVictim->isAlive())
		return;

	SpellEntry *spellInfo = dbcSpell.LookupEntry( spellID );
	if(!spellInfo)
        return;

	if (this->IsPlayer() && !static_cast< Player* >( this )->canCast(spellInfo))
		return;

//==========================================================================================
//==============================Variables Initialization====================================
//==========================================================================================
	uint32 school = spellInfo->School;
	float res = float(damage);
	uint32 aproc = PROC_ON_ANY_HOSTILE_ACTION | PROC_ON_SPELL_HIT;
	uint32 vproc = PROC_ON_ANY_HOSTILE_ACTION | PROC_ON_ANY_DAMAGE_VICTIM | PROC_ON_SPELL_HIT_VICTIM;
	bool critical = false;


//==========================================================================================
//==============================+Spell Damage Bonus Calculations============================
//==========================================================================================
//------------------------------by stats----------------------------------------------------
	if( this->IsUnit() && !static_damage )
	{
		Unit* caster = static_cast< Unit* >( this );
		caster->RemoveAurasByInterruptFlag( AURA_INTERRUPT_ON_START_ATTACK );
		res += caster->GetSpellDmgBonus( pVictim, spellInfo, ( int )res, false, false , overWrite );
//==========================================================================================
//==============================Post +SpellDamage Bonus Modifications=======================
//==========================================================================================
		if( res < 0 )
			res = 0;
		else if( spellInfo->spell_can_crit == true )
		{
//------------------------------critical strike chance--------------------------------------
			// lol ranged spells were using spell crit chance
			float CritChance;
			if(IsPlayer())
			{
				if (spellInfo->School)
				{
					CritChance = GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1) + GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1 + spellInfo->School);
				}
				else
				{
					CritChance = GetFloatValue(PLAYER_CRIT_PERCENTAGE);
				}
			}	
			else
				CritChance = 5.0f;

			if( spellInfo->Spell_Dmg_Type ==  SPELL_DMG_TYPE_RANGED)
			{

				if( IsPlayer() )
				{
					CritChance = GetFloatValue( PLAYER_RANGED_CRIT_PERCENTAGE );
					if( pVictim->IsPlayer() )
						CritChance += static_cast< Player* >(pVictim)->res_R_crit_get();

					CritChance += (float)(pVictim->AttackerCritChanceMod[spellInfo->School]);
				}
				else
				{
					CritChance = 5.0f; // static value for mobs.. not blizzlike, but an unfinished formula is not fatal :)
				}
				if( pVictim->IsPlayer() )
				CritChance -= static_cast< Player* >(pVictim)->CalcRating( PLAYER_RATING_MODIFIER_RANGED_CRIT_RESILIENCE );
			}
			else if( spellInfo->Spell_Dmg_Type == SPELL_DMG_TYPE_MELEE )
			{
				// Same shit with the melee spells, such as Judgement/Seal of Command

				if( pVictim->IsPlayer() )
				{
					CritChance += static_cast< Player* >(pVictim)->res_R_crit_get(); //this could be ability but in that case we overwrite the value
				}
				// Resilience
				CritChance -= pVictim->IsPlayer() ? static_cast< Player* >(pVictim)->CalcRating( PLAYER_RATING_MODIFIER_MELEE_CRIT_RESILIENCE ) : 0.0f;
				// Victim's (!) crit chance mod for physical attacks?
				CritChance += (float)(pVictim->AttackerCritChanceMod[school]);

			}
			else
			{
				CritChance += caster->spellcritperc + caster->SpellCritChanceSchool[school] + pVictim->AttackerCritChanceMod[school];
				if( caster->IsPlayer() && ( pVictim->m_rooted || pVictim->m_stunned ) )
					CritChance += static_cast< Player* >( caster )->m_RootedCritChanceBonus;

				if( pVictim->IsPlayer() )
				CritChance -= static_cast< Player* >(pVictim)->CalcRating( PLAYER_RATING_MODIFIER_SPELL_CRIT_RESILIENCE );
			}
			
			if (IsUnit())
			{
				Unit* pUnit = (Unit*)this;
				if (pUnit->getLevel() < pVictim->getLevel())
				{
					bool pvp = pVictim->IsPlayer() && this->IsPlayer();
					if(pvp)
					{
						CritChance -= 5 * (pVictim->getLevel() - pUnit->getLevel()) * 0.04f;
					}else
					{
						CritChance -= 5 * (pVictim->getLevel() - pUnit->getLevel()) * 0.2f;
					}
				}
				else
				{
					CritChance += 5 * (pUnit->getLevel() - pVictim->getLevel()) * 0.2f;
				}

				if (CritChance > 75)
				{
					CritChance = 75;
				}
			}

			if( spellInfo->SpellGroupType )
				SM_FFValue(caster->SM_CriticalChance, &CritChance, spellInfo->SpellGroupType);

			if( CritChance < 0 ) CritChance = 0;
			critical = Rand(CritChance);
			//MyLog::log->info( "SpellNonMeleeDamageLog: Crit Chance %f%%, WasCrit = %s" , CritChance , critical ? "Yes" : "No" );
//==========================================================================================
//==============================Spell Critical Hit==========================================
//==========================================================================================
			if (critical)
			{
				float critical_bonus = 1.5f;
				if( spellInfo->SpellGroupType )
					SM_PFValue( caster->SM_PCriticalDamage, &critical_bonus, spellInfo->SpellGroupType );
				
				res *= critical_bonus;

				if( pVictim->IsPlayer() )
				{
					//res = res*(1.0f-2.0f*static_cast< Player* >(pVictim)->CalcRating(PLAYER_RATING_MODIFIER_MELEE_CRIT_RESISTANCE));
					//Resilience is a special new rating which was created to reduce the effects of critical hits against your character.
					//It has two components; it reduces the chance you will be critically hit by x%,
					//and it reduces the damage dealt to you by critical hits by 2x%. x is the percentage resilience granted by a given resilience rating.
					//It is believed that resilience also functions against spell crits,
					//though it's worth noting that NPC mobs cannot get critical hits with spells.
					float dmg_reduction_pct = 2 * static_cast< Player* >(pVictim)->CalcRating( PLAYER_RATING_MODIFIER_MELEE_CRIT_RESILIENCE ) / 100.0f;
					if( dmg_reduction_pct > 1.0f )
						dmg_reduction_pct = 1.0f; //we cannot resist more then he is criticalling us, there is no point of the critical then :P
					res = res - res * dmg_reduction_pct;
				}

				pVictim->Emote( EMOTE_ONESHOT_WOUNDCRITICAL );
				aproc |= PROC_ON_SPELL_CRIT_HIT;
				vproc |= PROC_ON_SPELL_CRIT_HIT_VICTIM;
			}
		}
	}
//==========================================================================================
//==============================Post Roll Calculations======================================
//==========================================================================================
//------------------------------absorption--------------------------------------------------
	uint32 ress=(uint32)res;
	uint32 abs_dmg = pVictim->AbsorbDamage(school, &ress);
	uint32 ms_abs_dmg= pVictim->ManaShieldAbsorb(ress);
	if (ms_abs_dmg)
	{
		if(ms_abs_dmg > ress)
			ress = 0;
		else
			ress-=ms_abs_dmg;

		abs_dmg += ms_abs_dmg;
	}

	if(ress < 0) ress = 0;

	res=(float)ress;
	dealdamage dmg;
	dmg.school_type = school;
	dmg.full_damage = ress;
	dmg.resisted_damage = 0;

	if(res <= 0)
		dmg.resisted_damage = dmg.full_damage;

	//------------------------------resistance reducing-----------------------------------------
	if(res > 0 && this->IsUnit())
	{
		// removed by Nathan Gui
		 static_cast<Unit*>(this)->CalculateResistanceReduction(pVictim,&dmg,spellInfo);
		if((int32)dmg.resisted_damage > dmg.full_damage)
			res = 0;
		else
			res = float(dmg.full_damage - dmg.resisted_damage);
	}
	//------------------------------special states----------------------------------------------
	if(pVictim->GetTypeId() == TYPEID_PLAYER && static_cast< Player* >(pVictim)->GodModeCheat == true)
	{
		res = float(dmg.full_damage);
		dmg.resisted_damage = dmg.full_damage;
	}
	//DK:FIXME->SplitDamage
	// Completed (Supa)
	// Paladin: Blessing of Sacrifice, and Warlock: Soul Link
		if( !pVictim->m_damageSplitTargets.empty() )
		{
			std::list< DamageSplitTarget >::iterator itr;
			Unit * splittarget;
			uint32 splitdamage, tmpsplit;
			for( itr = pVictim->m_damageSplitTargets.begin() ; itr != pVictim->m_damageSplitTargets.end() ; itr ++ )
			{
				// TODO: Separate damage based on school.
				splittarget = pVictim->GetMapMgr() ? pVictim->GetMapMgr()->GetUnit( itr->m_target ) : NULL;
				if( splittarget && res > 0 )
				{
					// calculate damage
					tmpsplit = itr->m_flatDamageSplit;
					if( tmpsplit > (ui32)float2int32( res ))
						tmpsplit = float2int32( res ); // prevent < 0 damage
					splitdamage = tmpsplit;
					res -= (float)tmpsplit;
					tmpsplit = itr->m_pctDamageSplit * res;
					if( tmpsplit > (ui32)float2int32( res ) )
						tmpsplit = float2int32( res );
					splitdamage += tmpsplit;
					res -= (float)tmpsplit;
					// TODO: pct damage

					if( splitdamage )
					{
						pVictim->DealDamage( splittarget , splitdamage , 0 , 0 , 0 , false );
						// Send damage log
						pVictim->SendSpellNonMeleeDamageLog( pVictim , splittarget , 27148 , splitdamage , /*SCHOOL_HOLY*/SCHOOL_NORMAL , 0 , 0 , true , 0 , 0 , true, damageseq);
					}
				}
			}
		}
//==========================================================================================
//==============================Data Sending ProcHandling===================================
//==========================================================================================
	SendSpellNonMeleeDamageLog(this, pVictim, spellID, float2int32(res), school, abs_dmg, dmg.resisted_damage, false, 0, critical, IsPlayer(), damageseq);
	DealDamage( pVictim, float2int32( res ), 2, 0, spellID );

	if( this->IsUnit() && allowProc && spellInfo->Id != 25501 )
	{
		pVictim->HandleProc( vproc, static_cast< Unit* >( this ), spellInfo, float2int32( res ) );
		pVictim->m_procCounter = 0;
		static_cast< Unit* >( this )->HandleProc( aproc, pVictim, spellInfo, float2int32( res ) );
		static_cast< Unit* >( this )->m_procCounter = 0;
	}
	if( this->IsPlayer() )
	{
			static_cast< Player* >( this )->m_casted_amount[school] = ( uint32 )res;
	}



	if( pVictim->GetCurrentSpell() )
		pVictim->GetCurrentSpell()->AddTime( school );

//==========================================================================================
//==============================Post Damage Processing======================================
//==========================================================================================
	if( (int32)dmg.resisted_damage == dmg.full_damage && !abs_dmg )
	{
		//Magic Absorption
		if( pVictim->IsPlayer() )
		{
			if( static_cast< Player* >( pVictim )->m_RegenManaOnSpellResist )
			{
				Player* pl = static_cast< Player* >( pVictim );
				uint32 maxmana = pl->GetUInt32Value( UNIT_FIELD_MAXPOWER1 );

				//TODO: wtf is this ugly mess of casting bullshit
				uint32 amount = uint32(float( float(maxmana)*pl->m_RegenManaOnSpellResist));

				pVictim->Energize( pVictim, 29442, amount, POWER_TYPE_MANA );
			}
			// we still stay in combat dude
			static_cast< Player* >(pVictim)->CombatStatusHandler_ResetPvPTimeout();
		}
		if( IsPlayer() )
			static_cast< Player* >(this)->CombatStatusHandler_ResetPvPTimeout();
	}
	if( school == NORMAL_DAMAGE )
	{
		if( IsPlayer() && ((Unit*)this)->isAlive() && ((Player*)this)->getClass() == CLASS_PRIEST )
			((Player*)this)->VampiricSpell(float2int32(res), pVictim);

		if( pVictim->isAlive() && this->IsUnit() )
		{
			//Shadow Word:Death
			if( spellID == 32379 || spellID == 32996 )
			{
				uint32 damage = (uint32)( res + abs_dmg );
				uint32 absorbed = static_cast< Unit* >( this )->AbsorbDamage( school, &damage );
				DealDamage( static_cast< Unit* >( this ), damage, 2, 0, spellID );
				SendSpellNonMeleeDamageLog( this, this, spellID, damage, school, absorbed, 0, false, 0, false, this->IsPlayer(), damageseq );
			}
		}
	}
}

//*****************************************************************************************
//* SpellLog packets just to keep the code cleaner and better to read
//*****************************************************************************************

void Object::SendSpellLog(Object *Caster, Object *Target,uint32 Ability, uint8 SpellLogType)
{
	if( Ability >= 328 && Ability <= 332 )
		return;

	if ((!Caster || !Target) && Ability)
		return;

	MSG_S2C::stSpell_Log_Miss Msg;
	Msg.spellid = Ability;
	Msg.caster_guid = Caster->GetGUID();
	Msg.target_guid = Target->GetGUID();
	Msg.SpellLogType = SpellLogType;
	Caster->SendMessageToSet(Msg, true);
}


void Object::SendSpellNonMeleeDamageLog( Object* Caster, Object* Target, uint32 SpellID, uint32 Damage, uint8 School, uint32 AbsorbedDamage, uint32 ResistedDamage, bool PhysicalDamage, uint32 BlockedDamage, bool CriticalHit, bool bToset, uint16 damageseq )
{
	if( SpellID >= 328 && SpellID <= 332 )
		return;

	if ((!Caster || !Target) && SpellID)
		return;

	MSG_S2C::stSpell_NoMelee_Damage_Log Msg;
	Msg.target_guid = Target->GetNewGUID();
	Msg.caster_guid = Caster->GetNewGUID();
	Msg.spellid		= SpellID;
	Msg.Damage		= Damage;
	Msg.School		= g_spellSchoolConversionTable[School];
	Msg.AbsorbedDamage	= AbsorbedDamage;
	Msg.ResistedDamage	= ResistedDamage;
	Msg.PhysicalDamage	= PhysicalDamage;
	Msg.BlockedDamage	= BlockedDamage;
	Msg.damageSeq		= damageseq;
	Msg.bCri			= CriticalHit;
	Caster->SendMessageToSet( Msg, bToset );
}

int32 Object::event_GetInstanceID()
{
	// return -1 for non-inworld.. so we get our shit moved to the right thread
	if(!IsInWorld())
		return -1;
	else
		return m_instanceId;
}

void Object::EventSpellDamage(uint64 Victim, uint32 SpellID, uint32 Damage, uint16 damageseq,  float overWrite)
{
	if(!IsInWorld())
		return;

	Unit * pUnit = GetMapMgr()->GetUnit(Victim);
	if(pUnit == 0) return;

	/*
	if(IsPlayer())
	{
		Player* plr = (Player*)this;
		if(plr->m_bTriggerXP)
		{
			Damage += (21/4)*plr->getLevel()*(5/4)*12.5;
			plr->RemoveAura(2);
		}
	}
	*/

	SpellNonMeleeDamageLog(pUnit, SpellID, Damage, true, false, false, damageseq, overWrite);
}

bool Object::CanActivate()
{
	switch(m_objectTypeId)
	{
	case TYPEID_UNIT:
		{
			if(!IsPet())
				return true;
		}break;

	case TYPEID_GAMEOBJECT:
		{
			if(static_cast<GameObject*>(this)->HasAI() && GetUInt32Value(GAMEOBJECT_TYPE_ID) != GAMEOBJECT_TYPE_TRAP)
				return true;
		}break;
	}

	return false;
}

void Object::Activate(MapMgr * mgr)
{
	switch(m_objectTypeId)
	{
	case TYPEID_UNIT:
		mgr->activeCreatures.insert((Creature*)this);
		break;

	case TYPEID_GAMEOBJECT:
		mgr->activeGameObjects.insert((GameObject*)this);
		break;

	case TYPEID_DYNAMICOBJECT:
		mgr->activeDynamicObjects.insert((DynamicObject*)this);
	}

	Active = true;
}

void Object::Deactivate(MapMgr * mgr)
{
	switch(m_objectTypeId)
	{
	case TYPEID_UNIT:
		mgr->activeCreatures.erase((Creature*)this);
		break;

	case TYPEID_GAMEOBJECT:
		mgr->activeGameObjects.erase((GameObject*)this);
		break;

	case TYPEID_DYNAMICOBJECT:
		mgr->activeDynamicObjects.erase((DynamicObject*)this);
	}
	Active = false;
}

void Object::SetByte(uint32 index, uint32 index1,uint8 value)
{
	ASSERT( index < m_valuesCount );
	// save updating when val isn't changing.
	#ifndef USING_BIG_ENDIAN
	uint8 * v =&((uint8*)m_uint32Values)[index*4+index1];
	#else
	uint8 * v = &((uint8*)m_uint32Values)[index*4+(3-index1)];
	#endif
	if(*v == value)
		return;

	*v = value;

	m_updateMask.SetBit( index );
	m_isNeedUpdate = true;
	if(IsInWorld())
	{
		if(!m_objectUpdated)
		{
			m_mapMgr->ObjectUpdated(this);
			m_objectUpdated = true;
		}
	}

}

void Object::SetZoneId(uint32 newZone)
{
	m_zoneId = newZone;
	if( m_objectTypeId == TYPEID_PLAYER && static_cast< Player* >( this )->GetGroup() )
		static_cast< Player* >( this )->GetGroup()->HandlePartialChange( PARTY_UPDATE_FLAG_ZONEID, static_cast< Player* >( this ) );
}

uint32 Object::GetMapIDForCollision()
{
	if( m_mapMgr && m_mapMgr->GetBaseMap() )
	{
		return m_mapMgr->GetBaseMap()->GetMapIDForCollision();
	}
	else
		return 0;
}

void Object::PlaySoundToSet(uint32 sound_entry)
{
	MSG_S2C::stSound_Play Msg;
	Msg.sound = sound_entry;
	SendMessageToSet(Msg, true);
}

void Object::_SetExtension(const string& name, void* ptr)
{
	if( m_extensions == NULL )
		m_extensions = new ExtensionSet;

	m_extensions->insert( make_pair( name, ptr ) );
}

HM_NAMESPACE::hash_set<const Object*> Object::s_global_objects;

bool Object::IsValid() const
{
	if( this == NULL ) return false;

	if( s_global_objects.find( this ) != s_global_objects.end() )
		return true;
	else
		return false;
}

SunyouRaid* Object::GetSunyouRaid()
{
	if( m_mapMgr && m_mapMgr->m_sunyouinstance )
	{
		if( m_mapMgr->m_sunyouinstance->GetCategory() == INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND
			|| m_mapMgr->m_sunyouinstance->GetCategory() == INSTANCE_CATEGORY_RAID
			|| m_mapMgr->m_sunyouinstance->GetCategory() == INSTANCE_CATEGORY_FAIRGROUND
			|| m_mapMgr->m_sunyouinstance->GetCategory() == INSTANCE_CATEGORY_NEW_BATTLE_GROUND)
			return static_cast<SunyouRaid*>( m_mapMgr->m_sunyouinstance );
	}
	return NULL;
}

void Object::DropItemInWorld( uint32 entry, uint32 stack )
{
	if( m_mapMgr )
	{
		Item* item = objmgr.CreateItem( entry, NULL );
		item->SetUInt32Value(ITEM_FIELD_STACK_COUNT, stack);
		
		float z = CollideInterface.GetHeight( m_mapMgr->GetBaseMap(), m_position.x, m_position.y, m_position.z + 2.f );

		item->SetPosition( m_position.x, m_position.y, z, m_position.o );
		item->PushToWorld( m_mapMgr );
	}
}

void Object::SendMonsterMove(float NewPosX, float NewPosY, float NewPosZ, float orientation, uint32 type, uint32 MovementFlags, uint32 Time, bool sendtoall)
{
	MonsterMovementInfo info;
	info.guid	= GetNewGUID();
	info.x		= GetPositionX();
	info.y		= GetPositionY();
	info.z		= GetPositionZ();

	// unknown field - unrelated to orientation
	// seems to increment about 1000 for every 1.7 seconds
	// for now, we'll just use mstime

	info.move_type = type; 
	switch(type)
	{
	case MOVE_TYPE_NORMAL:                                             // normal packet
		break;
	case MOVE_TYPE_STOP:                                             // stop packet
		{
			if( sendtoall )
				PushMonsterMoveToSet( info, true, false, true );
			else if( IsPlayer() )
			{
				((Player*)this)->GetSession()->PushMonstMovement( info );
			}
			return;
		}
	case MOVE_TYPE_UNUSED:                                             // not used currently
		//data << uint64(0);                              // probably target guid
		break;
	case MOVE_TYPE_POLL:                                             // not used currently
		info.orientation = orientation;
		break;
	}

	//Movement Flags (0x0 = walk, 0x100 = run, 0x200 = fly/swim)
	info.move_flag = uint32(MovementFlags);

	info.time_between = Time;                                // Time in between points
	info.time_between = Time;                                // Time in between points
	info.wx = NewPosX;
	info.wy = NewPosY;
	info.wz = NewPosZ;// 1 single waypoint // the single waypoint Point B

	if( sendtoall )
		PushMonsterMoveToSet( info, true, false, true );
	else if( IsPlayer() )
	{
		((Player*)this)->GetSession()->PushMonstMovement( info );
	}
}

void Object::SendMonsterMoveToPlayer(float NewPosX, float NewPosY, float NewPosZ, float orientation, uint32 type, uint32 MovementFlags, uint32 Time, Player* plr)
{
	MonsterMovementInfo info;
	info.guid	= GetNewGUID();
	info.x		= GetPositionX();
	info.y		= GetPositionY();
	info.z		= GetPositionZ();

	// unknown field - unrelated to orientation
	// seems to increment about 1000 for every 1.7 seconds
	// for now, we'll just use mstime

	info.move_type = type;                                    // unknown
	switch(type)
	{
	case MOVE_TYPE_NORMAL:                                             // normal packet
		break;
	case MOVE_TYPE_STOP:                                             // stop packet
		{
			plr->GetSession()->PushMonstMovement( info );
			return;
		}
	case MOVE_TYPE_UNUSED:                                             // not used currently
		//data << uint64(0);                              // probably target guid
		break;
	case MOVE_TYPE_POLL:                                             // not used currently
		info.orientation = orientation;
		break;
	}

	//Movement Flags (0x0 = walk, 0x100 = run, 0x200 = fly/swim)
	info.move_flag = uint32(MovementFlags);

	info.time_between = Time;                                // Time in between points
	info.wx = NewPosX;
	info.wy = NewPosY;
	info.wz = NewPosZ;// 1 single waypoint // the single waypoint Point B

	plr->GetSession()->PushMonstMovement( info );
}
