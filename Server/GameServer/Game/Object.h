
#ifndef _OBJECT_H
#define _OBJECT_H
//#include "WorldPacket.h"


class Unit;
class MapCell;
class SunyouRaid;

namespace MSG_S2C
{
	struct stObjectUpdate;
	struct stMove_OP;
	struct stMove_Teleport_Ack;
}
//====================================================================
//  Object
//  Base object for every item, unit, player, corpse, container, etc
//====================================================================
class SERVER_DECL Object : public EventableObject
{
public:
	typedef std::set<Object*> InRangeSet;
	typedef std::map<string, void*> ExtensionSet;

	virtual ~Object ( );

	virtual void Update ( uint32 time ) { }
  //! True if object exists in world
 
	bool m_isNeedUpdate;
	SUNYOU_INLINE bool IsInWorld() { if( this == NULL ) return false; return m_mapMgr != NULL; }
	virtual void AddToWorld();
	virtual void AddToWorld(MapMgr * pMapMgr);
	void PushToWorld(MapMgr*);
	virtual void OnPushToWorld() { }
	virtual void OnPrePushToWorld() { }
	virtual void RemoveFromWorld(bool free_guid);

	// guid always comes first
#ifndef USING_BIG_ENDIAN
	SUNYOU_INLINE const uint64& GetGUID() const { return *((uint64*)m_uint32Values); }
#else
	SUNYOU_INLINE const uint64 GetGUID() const { return GetUInt64Value(0); }
#endif

	SUNYOU_INLINE const uint64& GetNewGUID() const { return GetGUID(); }
	SUNYOU_INLINE uint32 GetEntry(){return m_uint32Values[3];}
	
	SUNYOU_INLINE const uint32 GetEntryFromGUID() const
	{
/*		uint64 entry = *(uint64*)m_uint32Values;
		entry >>= 24;
		return (uint32)(entry & 0xFFFFFFFF);*/

		return uint32( (*(uint64*)m_uint32Values >> 24) & 0xFFFFFFFF );
	}

	SUNYOU_INLINE const uint32 GetTypeFromGUID() const { return (m_uint32Values[1] & HIGHGUID_TYPE_MASK); }
	SUNYOU_INLINE const uint32 GetUIdFromGUID() const { return (m_uint32Values[0] & LOWGUID_ENTRY_MASK); }
	SUNYOU_INLINE const uint32 GetLowGUID() const { return (m_uint32Values[0]); }

	// type
	SUNYOU_INLINE const uint8& GetTypeId() const { return m_objectTypeId; }
	SUNYOU_INLINE bool IsUnit()	const { return ( m_objectTypeId == TYPEID_UNIT || m_objectTypeId == TYPEID_PLAYER ); }
	SUNYOU_INLINE bool IsPlayer() const { return m_objectTypeId == TYPEID_PLAYER; }
	SUNYOU_INLINE bool IsCreature() const { return m_objectTypeId == TYPEID_UNIT; }
	SUNYOU_INLINE bool IsGameObject() const { return m_objectTypeId == TYPEID_GAMEOBJECT; }
	SUNYOU_INLINE bool IsDynamicObject() const { return m_objectTypeId == TYPEID_DYNAMICOBJECT; }

	bool IsPet();

	//! This includes any nested objects we have, inventory for example.
	virtual uint32 __fastcall BuildCreateUpdateBlockForPlayer( ByteBuffer *data, Player *target );
	uint32 __fastcall BuildValuesUpdateBlockForPlayer( ByteBuffer *buf, Player *target );
	uint32 __fastcall BuildValuesUpdateBlockForPlayer( ByteBuffer * buf, UpdateMask * mask );
	uint32 __fastcall BuildOutOfRangeUpdateBlock( ByteBuffer *buf );

	void BuildFieldUpdatePacket(uint32 index,uint32 value, MSG_S2C::stObjectUpdate* Msg);
	void BuildFieldUpdatePacket(Player* Target, uint32 Index, uint32 Value);
	void BuildFieldUpdatePacket(ByteBuffer * buf, uint32 Index, uint32 Value);

	void DealDamage(Unit *pVictim, uint32 damage, uint32 targetEvent, uint32 unitEvent, uint32 spellId, bool no_remove_auras = false);
	

	virtual void DestroyForPlayer( Player *target ) const;

	void BuildHeartBeatMsg( MSG_S2C::stMove_OP* Msg ) const;
	void BuildTeleportAckMsg( const LocationVector & v, MSG_S2C::stMove_Teleport_Ack* Msg);
	bool IsBeingTeleported() { return mSemaphoreTeleport; }
	void SetSemaphoreTeleport(bool semphsetting) { mSemaphoreTeleport = semphsetting; }

	bool SetPosition( float newX, float newY, float newZ, float newOrientation, bool allowPorting = false );
	bool SetPosition( const LocationVector & v, bool allowPorting = false);
	void SetRotation( uint64 guid );

	SUNYOU_INLINE const float GetPositionX( ) const { return m_position.x; }
	SUNYOU_INLINE const float GetPositionY( ) const { return m_position.y; }
	SUNYOU_INLINE const float GetPositionZ( ) const { return m_position.z; }
	SUNYOU_INLINE const float GetOrientation( ) const { return m_position.o; }
	SUNYOU_INLINE void SetOrientation( float o ) { m_position.o = o; }

	SUNYOU_INLINE const float& GetSpawnX( ) const { return m_spawnLocation.x; }
	SUNYOU_INLINE const float& GetSpawnY( ) const { return m_spawnLocation.y; }
	SUNYOU_INLINE const float& GetSpawnZ( ) const { return m_spawnLocation.z; }
	SUNYOU_INLINE const float& GetSpawnO( ) const { return m_spawnLocation.o; }

	SUNYOU_INLINE const LocationVector & GetPosition() { return m_position; }
	SUNYOU_INLINE LocationVector & GetPositionNC() { return m_position; }
	SUNYOU_INLINE LocationVector * GetPositionV() { return &m_position; }

	//Distance Calculation
	float CalcDistance(Object* Ob);
	float CalcDistance(float ObX, float ObY, float ObZ);
	float CalcDistance(Object *Oa, Object *Ob);
	float CalcDistance(Object *Oa, float ObX, float ObY, float ObZ);
	float CalcDistance(float OaX, float OaY, float OaZ, float ObX, float ObY, float ObZ);

	//! Only for MapMgr use
	SUNYOU_INLINE MapCell* GetMapCell() const { return m_mapCell; }
	//! Only for MapMgr use
	SUNYOU_INLINE void SetMapCell(MapCell* cell) { m_mapCell = cell; }
	//! Only for MapMgr use
	SUNYOU_INLINE MapMgr* GetMapMgr() const { return m_mapMgr; }

	SUNYOU_INLINE void SetMapId(uint32 newMap) { m_mapId = newMap; }
	void SetZoneId(uint32 newZone);

	SUNYOU_INLINE const uint32 GetMapId( ) const { return m_mapId; }
	uint32 GetMapIDForCollision();
	SUNYOU_INLINE const uint32& GetZoneId( ) const { return m_zoneId; }

	//! Get uint32 property
	SUNYOU_INLINE const uint32& GetUInt32Value( uint32 index ) const
	{
		ASSERT( index < m_valuesCount );
		return m_uint32Values[ index ];
	}

	SUNYOU_INLINE int GetInt32Value( uint32 index ) const
	{
		ASSERT( index < m_valuesCount );
		return (int)m_uint32Values[ index ];
	}

	//! Get uint64 property
#ifdef USING_BIG_ENDIAN
        __inline const uint64 GetUInt64Value( uint32 index ) const
#else
	SUNYOU_INLINE const uint64& GetUInt64Value( uint32 index ) const
#endif
	{
		ASSERT( index + uint32(1) < m_valuesCount );
#ifdef USING_BIG_ENDIAN
		/* these have to be swapped here :< */
		return uint64((uint64(m_uint32Values[index+1]) << 32) | m_uint32Values[index]);
#else
		return *((uint64*)&(m_uint32Values[ index ]));
#endif
	}

	//! Get float property
	SUNYOU_INLINE const float GetFloatValue( uint32 index ) const
	{
		if( index >= m_valuesCount )
			return 0.0f;
		return m_floatValues[ index ];
	}

	void __fastcall ModFloatValue(const uint32 index, const float value );
	void ModSignedInt32Value(uint32 index, int32 value);
	void ModUnsigned32Value(uint32 index, int32 mod);
	uint32 GetModPUInt32Value(const uint32 index, const int32 value);

	//! Set uint32 property
	void SetByte(uint32 index, uint32 index1,uint8 value);

	SUNYOU_INLINE uint8 GetByte(uint32 i,uint32 i1)
	{
		ASSERT( i < m_valuesCount);
		ASSERT(i1 < 4);
#ifdef USING_BIG_ENDIAN
		return ((uint8*)m_uint32Values)[i*4+(3-i1)];
#else
		return ((uint8*)m_uint32Values)[i*4+i1];
#endif
	}
	
	SUNYOU_INLINE void SetNewGuid(uint32 Guid)
	{
		SetUInt32Value(OBJECT_FIELD_GUID, Guid);
	}

	void EventSetUInt32Value(uint32 index, uint32 value);
	void __fastcall SetUInt32Value( const uint32 index, const uint32 value );

	//! Set uint64 property
	void __fastcall SetUInt64Value( const uint32 index, const uint64 value );

	//! Set float property
	void __fastcall SetFloatValue( const uint32 index, const float value );

	void __fastcall SetFlag( const uint32 index, uint32 newFlag );

	void __fastcall RemoveFlag( const uint32 index, uint32 oldFlag );

	SUNYOU_INLINE bool HasFlag( const uint32 index, uint32 flag ) const
	{
		ASSERT( index < m_valuesCount );
		return (m_uint32Values[ index ] & flag) != 0;
	}
	
	////////////////////////////////////////
	void ClearUpdateMask( )
	{
		m_updateMask.Clear();
		m_objectUpdated = false;
	}

	bool HasUpdateField(uint32 index)
	{
		ASSERT( index < m_valuesCount);
		return m_updateMask.GetBit(index);
	}

	//use it to check if a object is in range of another
	bool isInRange(Object* target, float range);


	// Use it to Check if a object is in front of another one
	bool isInFront(Object* target);
	bool isInBack(Object* target);
	// Check to see if an object is in front of a target in a specified arc (in degrees)
	bool isInArc(Object* target , float degrees); 
	/* Calculates the angle between two Positions */
	static float calcAngle( float Position1X, float Position1Y, float Position2X, float Position2Y );
	static float calcRadAngle( float Position1X, float Position1Y, float Position2X, float Position2Y );

	/* converts to 360 > x > 0 */
	static float getEasyAngle( float angle );

	SUNYOU_INLINE const float GetDistanceSq(Object* obj)
	{
		if(obj->GetMapId() != m_mapId) return 40000.0f; //enough for out of range
		return m_position.DistanceSq(obj->GetPosition());
	}

	SUNYOU_INLINE float GetDistanceSq(LocationVector & comp)
	{
		return comp.DistanceSq(m_position);
	}

	SUNYOU_INLINE float CalcDistance(LocationVector & comp)
	{
		return comp.Distance(m_position);
	}

	SUNYOU_INLINE const float GetDistanceSq(float x, float y, float z)
	{
		return m_position.DistanceSq(x, y, z);
	}

	SUNYOU_INLINE const float GetDistance2dSq( Object* obj )
	{
		if( obj->GetMapId() != m_mapId )
			return 40000.0f; //enough for out of range
		return m_position.Distance2DSq( obj->m_position );
	}

	// In-range object management, not sure if we need it
	SUNYOU_INLINE bool IsInRangeSet( Object* pObj )
	{
		return !( m_objectsInRange.find( pObj ) == m_objectsInRange.end() );
	}
	
	virtual void AddInRangeObject(Object* pObj)
	{
		if( pObj == NULL )
			return;

		m_objectsInRange.insert( pObj );

		// NOTES: Since someone will come along and try and change it.
		// Don't reinrepret_cast has to be used static_cast will not work when we are
		// inside the class we are casting from if we want to cast up the inheritance
		// chain, as Object has no concept of Player.

		if( pObj->GetTypeId() == TYPEID_PLAYER )
			m_inRangePlayers.insert( reinterpret_cast< Player* >( pObj ) );
	}

	SUNYOU_INLINE void RemoveInRangeObject( Object* pObj )
	{
		if( pObj == NULL )
			return;

		OnRemoveInRangeObject( pObj );
		m_objectsInRange.erase( pObj );
	}

	SUNYOU_INLINE bool HasInRangeObjects()
	{
		return ( m_objectsInRange.size() > 0 );
	}

	virtual void OnRemoveInRangeObject( Object * pObj )
	{
		if( pObj->GetTypeId() == TYPEID_PLAYER )
			//ASSERT( m_inRangePlayers.erase( reinterpret_cast< Player* >( pObj ) ) == 1 );
			m_inRangePlayers.erase( reinterpret_cast< Player* >( pObj ) );
	}

	virtual void ClearInRangeSet()
	{
		m_objectsInRange.clear();
		m_inRangePlayers.clear();
		m_oppFactsInRange.clear();
	}

	SUNYOU_INLINE size_t GetInRangeCount() { return m_objectsInRange.size(); }
	SUNYOU_INLINE size_t GetInRangePlayersCount() { return m_inRangePlayers.size();}
	SUNYOU_INLINE InRangeSet::iterator GetInRangeSetBegin() { return m_objectsInRange.begin(); }
	SUNYOU_INLINE InRangeSet::iterator GetInRangeSetEnd() { return m_objectsInRange.end(); }
	SUNYOU_INLINE InRangeSet::iterator FindInRangeSet(Object * obj) { return m_objectsInRange.find(obj); }

	void RemoveInRangeObject(InRangeSet::iterator itr)
	{ 
		OnRemoveInRangeObject(*itr);
		m_objectsInRange.erase(itr);
	}

	SUNYOU_INLINE bool RemoveIfInRange( Object * obj )
	{
		InRangeSet::iterator itr = m_objectsInRange.find(obj);
		if( obj->GetTypeId() == TYPEID_PLAYER )
			m_inRangePlayers.erase( reinterpret_cast< Player* >( obj ) );

		if( itr == m_objectsInRange.end() )
			return false;
		
		m_objectsInRange.erase( itr );
		return true;
	}

	SUNYOU_INLINE void AddInRangePlayer( Object * obj )
	{
		m_inRangePlayers.insert( reinterpret_cast< Player* >( obj ) );
	}

	SUNYOU_INLINE void RemoveInRangePlayer( Object * obj )
	{
		m_inRangePlayers.erase( reinterpret_cast< Player* >( obj ) );
	}

	bool IsInRangeOppFactSet(Object* pObj) { return (m_oppFactsInRange.count(pObj) > 0); }
	void UpdateOppFactionSet();
	SUNYOU_INLINE std::set<Object*>::iterator GetInRangeOppFactsSetBegin() { return m_oppFactsInRange.begin(); }
	SUNYOU_INLINE std::set<Object*>::iterator GetInRangeOppFactsSetEnd() { return m_oppFactsInRange.end(); }
	SUNYOU_INLINE std::set<Player*>::iterator GetInRangePlayerSetBegin() { return m_inRangePlayers.begin(); }
	SUNYOU_INLINE std::set<Player*>::iterator GetInRangePlayerSetEnd() { return m_inRangePlayers.end(); }
	SUNYOU_INLINE std::set<Player*> * GetInRangePlayerSet() { return &m_inRangePlayers; };

	void __fastcall SendMessageToSet(PakHead& pak, bool self,bool myteam_only=false, bool process_pending=false);
	void PushMonsterMoveToSet(const MonsterMovementInfo& info, bool self, bool myteam_only = false, bool process_pending = false );
	//SUNYOU_INLINE void SendMessageToSet(StackBufferBase * data, bool self) { OutPacketToSet(data->GetOpcode(), data->GetSize(), data->GetBufferPointer(), self); }
	//void OutPacketToSet(uint16 Opcode, uint16 Len, const void * Data, bool self);

	//! Fill values with data from a space seperated string of uint32s.
	void LoadValues(const char* data);

	SUNYOU_INLINE uint16 GetValuesCount() const { return m_valuesCount; }

	//! Blizzard seem to send those for all object types. weird.
	float m_walkSpeed;
	float m_runSpeed;
	float m_backWalkSpeed;
	float m_swimSpeed;
	float m_backSwimSpeed;
	float m_turnRate;
	float m_flySpeed;
	float m_backFlySpeed;

	float m_base_runSpeed;
	float m_base_walkSpeed;

	void EventSpellDamage(uint64 Victim, uint32 SpellID, uint32 Damage, uint16 damageseq,  float overWrite = 1.0f);
	void SpellNonMeleeDamageLog(Unit *pVictim, uint32 spellID, uint32 damage, bool allowProc, bool static_damage = false, bool no_remove_auras = false, uint16 damageseq=0, bool stato = false, float overWrite = 1.0f);
	
	//*****************************************************************************************
	//* SpellLog packets just to keep the code cleaner and better to read
	//*****************************************************************************************
	void SendSpellLog(Object *Caster, Object *Target,uint32 Ability, uint8 SpellLogType);
	void SendSpellNonMeleeDamageLog( Object* Caster, Object* Target, uint32 SpellID, uint32 Damage, uint8 School, uint32 AbsorbedDamage, uint32 ResistedDamage, bool PhysicalDamage, uint32 BlockedDamage, bool CriticalHit, bool bToSet, uint16 damageseq=0 );

	//object faction
	void _setFaction();
	uint32 _getFaction(){
		if( m_faction )
			return m_faction->Faction;
		else
			return 0;
	}
	
	FactionTemplateDBC *m_faction;
	FactionDBC *m_factionDBC;

	SUNYOU_INLINE void SetInstanceID(int32 instance) { m_instanceId = instance; }
	SUNYOU_INLINE int32 GetInstanceID() { return m_instanceId; }

	int32 event_GetInstanceID();

	bool Active;
	bool CanActivate();
	void Activate(MapMgr * mgr);
	void Deactivate(MapMgr * mgr);
	bool m_inQueue;
	SUNYOU_INLINE void SetMapMgr(MapMgr * mgr) { m_mapMgr = mgr; }

	void Delete()
	{
		if(IsInWorld())
			RemoveFromWorld(true);
		delete this;
	}

	void GMScriptEvent(void * function, uint32 argc, uint32 * argv, uint32 * argt);
	SUNYOU_INLINE size_t GetInRangeOppFactCount() { return m_oppFactsInRange.size(); }
	void PlaySoundToSet(uint32 sound_entry);

	bool IsValid() const;

	SunyouRaid* GetSunyouRaid();
	int m_varForLua[10];

	void DropItemInWorld( uint32 entry, uint32 stack );

protected:
	static HM_NAMESPACE::hash_set<const Object*> s_global_objects;

	Object (  );

	//void _Create (uint32 guidlow, uint32 guidhigh);
	void _Create( uint32 mapid, float x, float y, float z, float ang);

	//! Mark values that need updating for specified player.
	virtual void _SetUpdateBits(UpdateMask *updateMask, Player *target) const;
	//! Mark values that player should get when he/she/it sees object for first time.
	virtual void _SetCreateBits(UpdateMask *updateMask, Player *target) const;

	void _BuildMovementUpdate( ByteBuffer *data, uint8 flags, uint32 flags2, Player* target );
	void _BuildValuesUpdate( ByteBuffer *data, UpdateMask *updateMask, Player* target );

public:
	 //! Type id.
	uint8 m_objectTypeId;
	inline int GetUniqueIDForLua() const { return m_uniqueIDForLua; }
	inline void SetUniqueIDForLua( int id ) { m_uniqueIDForLua = id; }
	void SendMonsterMove(float NewPosX, float NewPosY, float NewPosZ, float orientation, uint32 type, uint32 MovementFlags, uint32 Time, bool sendtoall = true);
	void SendMonsterMoveToPlayer(float NewPosX, float NewPosY, float NewPosZ, float orientation, uint32 type, uint32 MovementFlags, uint32 Time, Player* plr);

protected:
	//! Zone id.
	uint32 m_zoneId;
	//! Continent/map id.
	uint32 m_mapId;
	//! Map manager
	MapMgr *m_mapMgr;
	//! Current map cell
	MapCell *m_mapCell;

	/* Main Function called by isInFront(); */
	static bool inArc(float Position1X, float Position1Y, float FOV, float Orientation, float Position2X, float Position2Y );
	
	LocationVector m_position;
	LocationVector m_lastMapUpdatePosition;
	LocationVector m_spawnLocation;

	// Semaphores - needed to forbid two operations on the same object at the same very time (may cause crashing\lack of data)
	bool mSemaphoreTeleport;

	//! Object properties.
	union {
		uint32* m_uint32Values;
		float* m_floatValues;
	};

	//! Number of properties
	uint32 m_valuesCount;

	//! List of object properties that need updating.
	UpdateMask m_updateMask;

	//! True if object was updated
	bool m_objectUpdated;

	//! Set of Objects in range.
	//! TODO: that functionality should be moved into WorldServer.
	std::set<Object*> m_objectsInRange;
	std::set<Player*> m_inRangePlayers;
	std::set<Object*> m_oppFactsInRange;
   
  
	//! Remove object from map
	void _RemoveFromMap();

	int32 m_instanceId;

	ExtensionSet * m_extensions;
	void _SetExtension(const string& name, void* ptr);		// so we can set from scripts. :)

	int m_uniqueIDForLua;
public:

	template<typename T>
		void SetExtension(const string& name, T ptr)
	{
		_SetExtension(name, ((void*)ptr));
	}

	template<typename T>
		T GetExtension(const string& name)
	{
		if( m_extensions == NULL )
			return ((T)NULL);
		else
		{
			ExtensionSet::iterator itr = m_extensions->find( name );
			if( itr == m_extensions->end() )
				return ((T)NULL);
			else
				return ((T)itr->second);
		}
	}

	bool m_loadedFromDB;
};

#endif


