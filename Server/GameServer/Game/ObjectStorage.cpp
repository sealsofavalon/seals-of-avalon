#include "StdAfx.h"
#include "ShopMgr.h"

extern void ApplyNormalFixes();
/** Table formats converted to strings
 */

const char * gDefaultAddItemFormat						= "uuuuuuuuuuuuuuu";
const char * gPlayerTitleFormat							= "usu";
const char * gConsume2GiftFormat						= "uuuuuuuuuuu";
const char * gSystemNotifyFormat						= "uus";
const char * gMapSpawnPosFormat							= "uuufff";
const char * gIdleGiftFormat							= "uuuuu";
const char * gPlaytimeTriggerFormat						= "uuuuuuuuuuuuu";
const char * gItemPrototypeFormat						= "uuusuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuffuffuffuffuffuufuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuusuuuuuuuuuuuuuuuuuuuuuuusuuuuuuuu";
const char * gCreatureNameFormat						= "ussuuuuuuu";
const char * gGameObjectNameFormat						= "uuusuuuuuuuuuuu";
const char * gCreatureProtoFormat						= "uuuuuuufuuuffuuuuuuuuuffuuuufffuuuusuuuuuuu";
const char * gAreaTriggerFormat							= "ucuusffffuu";
const char * gItemPageFormat							= "usu";
const char * gNpcTextFormat								= "ufssuuuuuuufssuuuuuuufssuuuuuuufssuuuuuuufssuuuuuuufssuuuuuuufssuuuuuuufssuuuuuuu";
const char * gQuestFormat								= "uuuuuuuuuuuuuuuuuuussssssssssuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuus";
const char * gSpellExtraFormat							= "uuuu";
const char * gGraveyardFormat							= "uffffuuuux";
const char * gTeleportCoordFormat						= "uxufffx";
const char * gPvPAreaFormat								= "ush";
const char * gFishingFormat								= "uuu";
const char * gWorldMapInfoFormat						= "uuuuufffusuuuuuuufuss";
const char * gZoneGuardsFormat							= "uuu";
const char * gSpellFormat								= "uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuufuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuffffffuuuuuuuuuuuuuuuuuuuuufffuuuuuuuuuuuufffuuuuussssuuuuiuufffuuuuuuusfuuuuuuuuuuu";

const char* gAreaTableFormat							= "uuuuuuusu";
const char* gGemPropertyEntryFormat = "uuu";
const char* gItemSetFormat = "usuuuuuuuuuuuuuuuuuuuuuuuuuuuu";
const char* gLockFormat = "uuuuuuuuuuuuuuuuuuuuuuuuu";
const char* gQuestTipsFormat = "uuuuus";

const char* gSpellradiusFormat = "uff";
const char* gSpellcasttimeFormat = "uu";
const char* gSpellrangeFormat = "uff";
const char* gSpelldurationFormat = "uuuu";
const char* gEmoteEntryFormat = "uxuuuuxuxuxxxxxxxxx";

const char* gtfloatformat = "uf";

const char* gFactiontemplatedbcFormat = "uuuuuuuuuuuuuu";
const char* gFactiondbcFormat = "uuuuuuus";

const char* gTaxinodeFormat = "uufffuu";
const char* gTaxipathFormat = "uuuu";
const char* gTaxipathnodeFormat = "uuuufffu";

const char* gEnchantEntrYFormat = "uuuuuuuuuuuuusuuuu";
const char* gRandompropsFormat = "uuuu";
const char* gSkilllinespellFormat = "uuuuuuuu";
const char* gSkilllineentrYFormat = "uus";

const char* gAuctionhousedbcFormat = "uuu";
const char* gChatchannelformat = "uu";
const char* gMapentryFormat = "usus";

const char* gCreaturespelldataFormat = "uuuuuuuuu";
const char* gCreaturefamilyFormat = "ufufuuuus";
const char* gCharraceFormat = "uuus";
const char* gCharclassFormat = "uus";

const char* gItemextendedcostFormat = "uuuuuuuuuuuuuuuuu";
const char* gItemrandomsuffixformat = "uuuuuuu";
const char * gDurabilityqualityFormat = "uf";
const char * gDurabilitycostsFormat = "uuuuuuuuuuuuuuuuuuuuuuuuuuuuuu";
const char* gBankslotpriceformat = "uuu";

const char* gTalententryFormat = "uuuuuuuuuuu";
const char* gTalenttabentryFormat = "uuu";

const char* gNameGenFormat = "us";
const char* gLfgDungeonFormat = "uu";

const char* gSunyouInstanceSpawnListFormat = "uuuuuu";
const char* gContributionGiftFormat = "uuuuuu";
const char* gDynamicInstanceConfiguration = "uuuuuuuuuuuuuuuuuuuuuuu";
const char* gSunyouInstancePeriodicGift = "uuuuuu";
const char* gRefineItemGem = "uuuuuu";
const char* gRefineItemUpgrade = "uuuu";
const char* gRefineItemEffect = "uuuuuu";
const char* gCombineMaterial = "uu";
const char* gCombineNeed = "uuuuuuuuuu";
const char* gCombineSpecial = "uu";
const char* gGuildKillBonusList = "uuu";
const char* gPetFoodList = "uu";
const char* gPetEggList = "uu";
const char* gCastleNPCList = "uuusu";
const char* gCastleConfig = "usssussuuusuuuu";
const char* gGuildUpgradeList = "uuuusuu";
const char* gSunyouBattleGroundList = "ussssssssssssssssssuuuuuuuuuu";
const char* gBattleGroundNpcList = "uuusuuu";
const char* gCreatureHonorList = "uu";
const char* gCreatureSayList = "usuuuuuusssssssssssssss";
const char* gNpcTransPos = "uuuffff";
const char* gNpcScriptTrans = "uusu";
const char* gTitleRewards = "uuuuuuuus";
const char* gPositionList = "uuffffs";
const char* gShapeSpellConf = "usuuuuuuuuuu";
const char* gEquipRefineRate = "uff";

/** SQLStorage symbols
 */

extern const char* _creature_say_list;

SERVER_DECL SQLStorage<stDefaultAddItem, HashMapStorageContainer<stDefaultAddItem> >		AddItemStorage;
SERVER_DECL SQLStorage<stPlayerTitle, HashMapStorageContainer<stPlayerTitle> >				PlayerTitleStorage;
SERVER_DECL SQLStorage<stConsume2GiftEntry, HashMapStorageContainer<stConsume2GiftEntry> >	Consume2GiftStorage;
SERVER_DECL SQLStorage<stSystemNotify, HashMapStorageContainer<stSystemNotify> >			SystemNotifyStorage;
SERVER_DECL SQLStorage<MapSpawnPos, HashMapStorageContainer<MapSpawnPos> >					MapSpawnPosStorage;
SERVER_DECL SQLStorage<Idle_Gift, HashMapStorageContainer<Idle_Gift> >						IdleGiftStorage;
SERVER_DECL SQLStorage<Playtime_Trigger, HashMapStorageContainer<Playtime_Trigger> >		PlaytimeTriggerStorage;
SERVER_DECL SQLStorage<ItemPrototype, HashMapStorageContainer<ItemPrototype> >				ItemPrototypeStorage;
SERVER_DECL SQLStorage<CreatureInfo, HashMapStorageContainer<CreatureInfo> >				CreatureNameStorage;
SERVER_DECL SQLStorage<GameObjectInfo, HashMapStorageContainer<GameObjectInfo> >			GameObjectNameStorage;
SERVER_DECL SQLStorage<CreatureProto, HashMapStorageContainer<CreatureProto> >				CreatureProtoStorage;
SERVER_DECL SQLStorage<AreaTrigger, HashMapStorageContainer<AreaTrigger> >					AreaTriggerStorage;
SERVER_DECL SQLStorage<ItemPage, HashMapStorageContainer<ItemPage> >						ItemPageStorage;
SERVER_DECL SQLStorage<Quest, HashMapStorageContainer<Quest> >								QuestStorage;
SERVER_DECL SQLStorage<GossipText, HashMapStorageContainer<GossipText> >					NpcTextStorage;
SERVER_DECL SQLStorage<GraveyardTeleport, HashMapStorageContainer<GraveyardTeleport> >		GraveyardStorage;
SERVER_DECL SQLStorage<TeleportCoords, HashMapStorageContainer<TeleportCoords> >			TeleportCoordStorage;
SERVER_DECL SQLStorage<FishingZoneEntry, HashMapStorageContainer<FishingZoneEntry> >		FishingZoneStorage;
SERVER_DECL SQLStorage<MapInfo, ArrayStorageContainer<MapInfo> >							WorldMapInfoStorage;
SERVER_DECL SQLStorage<ZoneGuardEntry, HashMapStorageContainer<ZoneGuardEntry> >			ZoneGuardStorage;
SERVER_DECL SQLStorage<SpellEntry, ArrayStorageContainer<SpellEntry> >						SpellStorage;
SERVER_DECL SQLStorage<AreaTable, ArrayStorageContainer<AreaTable> >						AreaStorage;
SERVER_DECL SQLStorage<GemPropertyEntry, ArrayStorageContainer<GemPropertyEntry> >			GemPropertyStorage;
SERVER_DECL SQLStorage<ItemSetEntry, ArrayStorageContainer<ItemSetEntry> >					ItemSetStorage;
SERVER_DECL SQLStorage<Lock, ArrayStorageContainer<Lock> >									dbcLock;
SERVER_DECL SQLStorage<SpellEntry, HashMapStorageContainer<SpellEntry> >						dbcSpell;
SERVER_DECL SQLStorage<SpellRadius, HashMapStorageContainer<SpellRadius> >					dbcSpellRadius;
SERVER_DECL SQLStorage<SpellDuration, HashMapStorageContainer<SpellDuration> >				dbcSpellDuration;
SERVER_DECL SQLStorage<SpellRange, HashMapStorageContainer<SpellRange> >						dbcSpellRange;
SERVER_DECL SQLStorage<SpellCastTime, HashMapStorageContainer<SpellCastTime> >				dbcSpellCastTime;
SERVER_DECL SQLStorage<emoteentry, ArrayStorageContainer<emoteentry> >						dbcEmoteEntry;

SERVER_DECL SQLStorage<QuestTips, ArrayStorageContainer<QuestTips> > QuestTipsStorage;

SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 							dbcMeleeCrit;
SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 							dbcMeleeCritBase;
SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 							dbcSpellCrit;
SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 							dbcSpellCritBase;
SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 							dbcManaRegen;
SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 							dbcManaRegenBase;
SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 							dbcHPRegen;
SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 							dbcHPRegenBase;

SERVER_DECL SQLStorage<FactionTemplateDBC, ArrayStorageContainer<FactionTemplateDBC> >		dbcFactionTemplate;
SERVER_DECL SQLStorage<FactionDBC, ArrayStorageContainer<FactionDBC> >						dbcFaction;

SERVER_DECL SQLStorage<DBCTaxiNode, ArrayStorageContainer<DBCTaxiNode> > dbcTaxiNode;
SERVER_DECL SQLStorage<DBCTaxiPath, ArrayStorageContainer<DBCTaxiPath> > dbcTaxiPath;
SERVER_DECL SQLStorage<DBCTaxiPathNode, ArrayStorageContainer<DBCTaxiPathNode> > dbcTaxiPathNode;

SERVER_DECL SQLStorage<EnchantEntry, ArrayStorageContainer<EnchantEntry> > dbcEnchant;
SERVER_DECL SQLStorage<RandomProps, ArrayStorageContainer<RandomProps> > dbcRandomProps;
SERVER_DECL SQLStorage<skilllinespell, ArrayStorageContainer<skilllinespell> > dbcSkillLineSpell;
SERVER_DECL SQLStorage<skilllineentry, ArrayStorageContainer<skilllineentry> > dbcSkillLine;

SERVER_DECL SQLStorage<AuctionHouseDBC,ArrayStorageContainer<AuctionHouseDBC> > dbcAuctionHouse;
SERVER_DECL SQLStorage<ChatChannelDBC, ArrayStorageContainer<ChatChannelDBC> > dbcChatChannels;
SERVER_DECL SQLStorage<MapEntry, ArrayStorageContainer<MapEntry> > dbcMap;

SERVER_DECL SQLStorage<CreatureSpellDataEntry, ArrayStorageContainer<CreatureSpellDataEntry> > dbcCreatureSpellData;
SERVER_DECL SQLStorage<CreatureFamilyEntry, ArrayStorageContainer<CreatureFamilyEntry> > dbcCreatureFamily;
SERVER_DECL SQLStorage<CharClassEntry, ArrayStorageContainer<CharClassEntry> > dbcCharClass;
SERVER_DECL SQLStorage<CharRaceEntry, ArrayStorageContainer<CharRaceEntry> > dbcCharRace;


SERVER_DECL SQLStorage<ItemExtendedCostEntry, ArrayStorageContainer<ItemExtendedCostEntry> > dbcItemExtendedCost;
SERVER_DECL SQLStorage<ItemRandomSuffixEntry, ArrayStorageContainer<ItemRandomSuffixEntry> > dbcItemRandomSuffix;
SERVER_DECL SQLStorage<DurabilityCostsEntry, ArrayStorageContainer<DurabilityCostsEntry> > dbcDurabilityCosts;
SERVER_DECL SQLStorage<DurabilityQualityEntry, ArrayStorageContainer<DurabilityQualityEntry> > dbcDurabilityQuality;
SERVER_DECL SQLStorage<BankSlotPrice, ArrayStorageContainer<BankSlotPrice> > dbcBankSlotPrices;
SERVER_DECL SQLStorage<BankSlotPrice, ArrayStorageContainer<BankSlotPrice> > dbcStableSlotPrices; //uses same structure as Bank

SERVER_DECL SQLStorage<TalentEntry, ArrayStorageContainer<TalentEntry> > dbcTalent;
SERVER_DECL SQLStorage<TalentTabEntry, ArrayStorageContainer<TalentTabEntry> > dbcTalentTab;
SERVER_DECL SQLStorage<CombatRatingDBC, ArrayStorageContainer<CombatRatingDBC> > dbcCombatRating;

SERVER_DECL set<string> ExtraMapCreatureTables;
SERVER_DECL set<string> ExtraMapGameObjectTables;

SERVER_DECL SQLStorage<NameGenData, HashMapStorageContainer<NameGenData> > dbcNameGen;
SERVER_DECL SQLStorage<LfgDungeonType, HashMapStorageContainer<LfgDungeonType> > dbcLfgDungeonTypes;

SERVER_DECL SQLStorage<spawn_info, ArrayStorageContainer<spawn_info> > dbcSunyouInstanceSpawnInfo;
SERVER_DECL SQLStorage<contribution_gift, ArrayStorageContainer<contribution_gift> > dbcContributionGift;
SERVER_DECL SQLStorage<dynamic_instance_configuration, ArrayStorageContainer<dynamic_instance_configuration> > dbcDynamicInstanceConfiguration;
SERVER_DECL SQLStorage<sunyou_instance_periodic_gift, ArrayStorageContainer<sunyou_instance_periodic_gift> > dbcSunyouInstancePeriodicGift;
SERVER_DECL SQLStorage<jinglian_item_gem, ArrayStorageContainer<jinglian_item_gem> > dbcRefineItemGem;
//SERVER_DECL SQLStorage<jinglian_item_upgrade, ArrayStorageContainer<refine_item_upgrade> > dbcRefineItemUpgrade;
SERVER_DECL SQLStorage<refine_item_effect, ArrayStorageContainer<refine_item_effect> > dbcRefineItemEffect;

SERVER_DECL SQLStorage<combine_material, HashMapStorageContainer<combine_material> > dbcCombineMaterial;
SERVER_DECL SQLStorage<combine_need, HashMapStorageContainer<combine_need> > dbcCombineNeed;
SERVER_DECL SQLStorage<combine_special, HashMapStorageContainer<combine_special> > dbcCombineSpecialList;
SERVER_DECL SQLStorage<guild_kill_bonus, HashMapStorageContainer<guild_kill_bonus> > dbcGuildKillBonusList;
SERVER_DECL SQLStorage<pet_food, HashMapStorageContainer<pet_food> > dbcPetFoodList;
SERVER_DECL SQLStorage<pet_egg, HashMapStorageContainer<pet_egg> > dbcPetEggList;
SERVER_DECL SQLStorage<castle_npc, HashMapStorageContainer<castle_npc> > dbcCastleNPCList;
SERVER_DECL SQLStorage<castle_config, HashMapStorageContainer<castle_config> > dbcCastleConfig;
SERVER_DECL SQLStorage<guild_upgrade_list, ArrayStorageContainer<guild_upgrade_list> > dbcGuildUpgradeList;
SERVER_DECL SQLStorage<sunyou_battle_ground, HashMapStorageContainer<sunyou_battle_ground> > dbcSunyouBattleGroundList;
SERVER_DECL SQLStorage<battle_ground_npc, HashMapStorageContainer<battle_ground_npc> > dbcBattleGroundNpcList;
SERVER_DECL SQLStorage<creature_honor, HashMapStorageContainer<creature_honor> > dbcCreatureHonorList;
SERVER_DECL SQLStorage<creature_say, HashMapStorageContainer<creature_say> > dbcCreatureSayList;
SERVER_DECL SQLStorage<stNpcScriptTrans, HashMapStorageContainer<stNpcScriptTrans> > dbcNpcScriptTrans;
SERVER_DECL SQLStorage<stNpcTransPos, HashMapStorageContainer<stNpcTransPos> > dbcNpcTransPos;
SERVER_DECL SQLStorage<title_reward, HashMapStorageContainer<title_reward> > dbcTitleRewards;
SERVER_DECL SQLStorage<position_t, HashMapStorageContainer<position_t> > dbcPositionList;
SERVER_DECL SQLStorage<shapespell_conf, HashMapStorageContainer<shapespell_conf> > dbcShapeSpellConf;
SERVER_DECL SQLStorage<EquipRefineRate, HashMapStorageContainer<EquipRefineRate> > dbcEquipRefineRate;

void ObjectMgr::LoadExtraCreatureProtoStuff()
{
	{
		StorageContainerIterator<CreatureProto> * itr = CreatureProtoStorage.MakeIterator();
		CreatureProto * cn;
		while(!itr->AtEnd())
		{
			cn = itr->Get();
			cn->m_canSpellAttack = false;
			cn->m_defaultAttackSpell = NULL;

			/*if(itr->Get()->aura_string)
			{
				string auras = string(itr->Get()->aura_string);
				vector<string> aurs = StrSplit(auras, " ");
				for(vector<string>::iterator it = aurs.begin(); it != aurs.end(); ++it)
				{
					uint32 id = atol((*it).c_str());
					if(id)
						itr->Get()->start_auras.insert( id );
				}
			}*/

			if(!itr->Get()->MinHealth)
				itr->Get()->MinHealth = 1;
			if(!itr->Get()->MaxHealth)
				itr->Get()->MaxHealth = 1;
// 			if (itr->Get()->AttackType > SCHOOL_ARCANE)
				itr->Get()->AttackType = SCHOOL_NORMAL;

			cn->m_canFlee = cn->m_canRangedAttack = cn->m_canCallForHelp = false;
			cn->m_fleeHealth = 0.0f;
			// please.... m_fleeDuration is a uint32...
			//cn->m_fleeDuration = 0.0f;
			cn->m_fleeDuration = 0;

			if(!itr->Inc())
				break;
		}

		itr->Destruct();
	}

	{
		StorageContainerIterator<CreatureInfo> * itr = CreatureNameStorage.MakeIterator();
		CreatureInfo * ci;
		while(!itr->AtEnd())
		{
			ci = itr->Get();

			ci->lowercase_name = string(ci->Name);
			for(uint32 j = 0; j < ci->lowercase_name.length(); ++j)
				ci->lowercase_name[j] = tolower(ci->lowercase_name[j]); // Darvaleo 2008/08/15 - Copied lowercase conversion logic from ItemPrototype task

			ci->gossip_script = sScriptMgr.GetDefaultGossipScript();

			if(!itr->Inc())
				break;
		}
		itr->Destruct();
	}

	{
		StorageContainerIterator<Quest> * itr = QuestStorage.MakeIterator();
		Quest * qst;
		while(!itr->AtEnd())
		{
			qst = itr->Get();
			//qst->pQuestScript = NULL;

			if( !itr->Inc() )
				break;
		}
		itr->Destruct();
	}

	// Load AI Agents
	QueryResult * result = WorldDatabase.Query( "SELECT * FROM ai_agents" );
	CreatureProto * cn;

	if( result != NULL )
	{
		AI_Spell *sp;
		SpellEntry * spe;
		uint32 entry;

		if(Config.MainConfig.GetBoolDefault("Server", "LoadAIAgents", true))
		{
			do
			{
				Field *fields = result->Fetch();
				entry = fields[0].GetUInt32();
				cn = CreatureProtoStorage.LookupEntry(entry);
				spe = dbcSpell.LookupEntry(fields[5].GetUInt32());
				if( spe == NULL )
				{
					if (!fields[5].GetUInt32())
					{
						MyLog::log->warn("AIAgent : For %u has nonexistant spell %u.", fields[0].GetUInt32(), fields[5].GetUInt32());
					}
				}
				if(!cn)
					continue;

				sp = new AI_Spell;
				sp->entryId = fields[0].GetUInt32();
				sp->agent = fields[1].GetUInt16();
				sp->AIEvent = fields[2].GetUInt16();
				sp->procChance = fields[3].GetUInt32();
				sp->procCount = fields[4].GetUInt32();
				sp->spell = spe;
				sp->spellType = fields[6].GetUInt32();
				sp->spelltargetType = fields[7].GetUInt32();
				sp->cooldown = fields[8].GetUInt32();
				sp->floatMisc1 = fields[9].GetFloat();
				sp->autocast_type=(uint32)-1;
				sp->custom_pointer=false;
				sp->cooldowntime=getMSTime();
				sp->procCounter=0;

		/*		if (!sp->procCountDB) 
					sp->procCount = uint32(-1);
				else sp->procCount = sp->procCountDB;*/
				sp->Misc2 = fields[10].GetUInt32();
				if(sp->agent == AGENT_SPELL)
				{
					if(!sp->spell)
					{
						//printf("SpellId %u in ai_agent for %u is invalid.\n", (unsigned int)fields[5].GetUInt32(), (unsigned int)sp->entryId);
						delete sp;
						continue;
					}
					
					if(sp->spell->Effect[0] == SPELL_EFFECT_LEARN_SPELL || sp->spell->Effect[1] == SPELL_EFFECT_LEARN_SPELL ||
						sp->spell->Effect[2] == SPELL_EFFECT_LEARN_SPELL)
					{
						//printf("Teaching spell %u in ai_agent for %u\n", (unsigned int)fields[5].GetUInt32(), (unsigned int)sp->entryId);
						delete sp;
						continue;
					}

					sp->minrange = GetMinRange(dbcSpellRange.LookupEntry(sp->spell->rangeIndex));
					sp->maxrange = GetMaxRange(dbcSpellRange.LookupEntry(sp->spell->rangeIndex));

					//omg the poor darling has no clue about making ai_agents
					if(sp->cooldown==0xffffffff)
					{
						//now this will not be exact cooldown but maybe a bigger one to not make him spam spells to often
						int cooldown;
						SpellDuration *sd=dbcSpellDuration.LookupEntry(sp->spell->DurationIndex);
						int Dur=0;
						int Casttime=0;//most of the time 0
						int RecoveryTime=sp->spell->RecoveryTime;
						if(sp->spell->DurationIndex)
							Dur =::GetDuration(sd);
						Casttime=GetCastTime(dbcSpellCastTime.LookupEntry(sp->spell->CastingTimeIndex));
						cooldown=Dur+Casttime+RecoveryTime;
						if(cooldown<0)
							sp->cooldown=0;//huge value that should not loop while adding some timestamp to it
						else sp->cooldown=cooldown;
					}

		
					//now apply the morron filter
					if(sp->procChance==0)
					{
						//printf("SpellId %u in ai_agent for %u is invalid.\n", (unsigned int)fields[5].GetUInt32(), (unsigned int)sp->entryId);
						delete sp;
						continue;
					}
					else
					{
						cn->spells.push_back(sp);
					}
					/*
					if(sp->spellType==0)
					{
						//right now only these 2 are used
						if(IsBeneficSpell(sp->spell))
							sp->spellType==STYPE_HEAL;
						else sp->spellType==STYPE_BUFF;
					}
					if(sp->spelltargetType==0)
						sp->spelltargetType = RecommandAISpellTargetType(sp->spell);
						*/
				}

				if(sp->agent == AGENT_RANGED)
				{
					cn->m_canRangedAttack = true;
					delete sp;
				}
				else if(sp->agent == AGENT_FLEE)
				{
					cn->m_canFlee = true;
					if(sp->floatMisc1)
						//cn->m_canFlee = (sp->floatMisc1>0.0f ? true : false);
						cn->m_fleeHealth = sp->floatMisc1;
					else
						cn->m_fleeHealth = 0.2f;

					if(sp->Misc2)
						cn->m_fleeDuration = sp->Misc2;
					else
						cn->m_fleeDuration = 0;

					delete sp;
				}
				else if(sp->agent == AGENT_CALLFORHELP)
				{
					cn->m_canCallForHelp = true;
					if(sp->floatMisc1)
						cn->m_callForHelpHealth = 0.2f;
					delete sp;
				}
				else if(sp->agent == AGENT_SPELL_ATTACK)
				{
					cn->m_canSpellAttack = true;
					cn->m_defaultAttackSpell = sp->spell;
					cn->spells.push_back(sp);
					if(sp->spell)
					{
						sp->minrange = GetMinRange(dbcSpellRange.LookupEntry(sp->spell->rangeIndex));
						sp->maxrange = GetMaxRange(dbcSpellRange.LookupEntry(sp->spell->rangeIndex));
					}
				}
				else
				{
					cn->spells.push_back(sp);
				}
			} while( result->NextRow() );
		}

		delete result;
	}
}

void ObjectMgr::LoadExtraItemStuff()
{
	map<uint32,uint32> foodItems;
	QueryResult * result = WorldDatabase.Query("SELECT * FROM itempetfood ORDER BY entry");
	if(result)
	{
		Field *f = result->Fetch();
		do
		{		
			foodItems.insert( make_pair( f[0].GetUInt32(), f[1].GetUInt32() ) );
		}
		while(result->NextRow());
	}
	delete result;

	StorageContainerIterator<ItemPrototype> * itr = ItemPrototypeStorage.MakeIterator();
	ItemPrototype * pItemPrototype;
	while(!itr->AtEnd())
	{
		pItemPrototype = itr->Get();
		if(pItemPrototype->ItemSet > 0)
		{
			ItemSetContentMap::iterator itr = mItemSets.find(pItemPrototype->ItemSet);
			std::list<ItemPrototype*>* l;
			if(itr == mItemSets.end())
			{
				l = new std::list<ItemPrototype*>;				
				mItemSets.insert( ItemSetContentMap::value_type( pItemPrototype->ItemSet, l) );
			} else {
				l = itr->second;
			}
			l->push_back(pItemPrototype);
		}


		// lowercase name, used for searches
		pItemPrototype->lowercase_name = pItemPrototype->Name1;
		for(uint32 j = 0; j < pItemPrototype->lowercase_name.length(); ++j)
			pItemPrototype->lowercase_name[j] = tolower(pItemPrototype->lowercase_name[j]);

		//load item_pet_food_type from extra table
		uint32 ft = 0;
		map<uint32,uint32>::iterator iter = foodItems.find(pItemPrototype->ItemId);
		if(iter != foodItems.end())
			ft = iter->second;
		pItemPrototype->FoodType = ft ;
	
		pItemPrototype->gossip_script=NULL;

		// forced pet entries
		switch( pItemPrototype->ItemId )
		{
		case 28071:
			// Felguard
			pItemPrototype->ForcedPetId = 17252;
			break;

		case 16321:
		case 16322:
			// Imp
			pItemPrototype->ForcedPetId = 416;
			break;
		case 16357:
			// Voidwalker
			pItemPrototype->ForcedPetId = 1860;
			break;
		case 16380:
			// Succubus
			pItemPrototype->ForcedPetId = 1863;
			break;

		case 21283:
			// Player
			pItemPrototype->ForcedPetId = 0;
			break;

		default:
			pItemPrototype->ForcedPetId = -1;
			break;
		}

		pItemPrototype->extended_cost = NULL;
        if(!itr->Inc())
			break;
	}

	itr->Destruct();
	foodItems.clear();

	result = WorldDatabase.Query("SELECT * FROM items_extendedcost");
	ItemExtendedCostEntry * ec;
	if( result != NULL )
	{
		do 
		{
			ec = dbcItemExtendedCost.LookupEntry( result->Fetch()[1].GetUInt32() );//LookupEntryForced( result->Fetch()[1].GetUInt32() );
			if( ec == NULL )
			{
				MyLog::log->warn("LoadItems: Extendedcost for item %u references nonexistant EC %u", result->Fetch()[0].GetUInt32(), result->Fetch()[1].GetUInt32());
				continue;
			}

			pItemPrototype = ItemPrototypeStorage.LookupEntry( result->Fetch()[0].GetUInt32() );
			if( pItemPrototype == NULL )
			{
				MyLog::log->warn("LoadItems : Extendedcost for item %u references nonexistant item %u", result->Fetch()[0].GetUInt32(), result->Fetch()[1].GetUInt32());
				continue;
			}

			pItemPrototype->extended_cost = ec;

		} while (result->NextRow());
		delete result;
	}
}

#define make_task(storage, itype, storagetype, tablename, format) tl.AddTask( new Task( \
	new CallbackP2< SQLStorage< itype, storagetype< itype > >, const char *, const char *> \
    (&storage, &SQLStorage< itype, storagetype< itype > >::Load, tablename, format) ) )

void Storage_FillTaskList()
{
	AddItemStorage.Load("default_additem", gDefaultAddItemFormat);
	PlayerTitleStorage.Load("title", gPlayerTitleFormat);
	Consume2GiftStorage.Load("consume2gift", gConsume2GiftFormat);
	SystemNotifyStorage.Load("SystemNotify", gSystemNotifyFormat);
	MapSpawnPosStorage.Load("mapspawnpos", gMapSpawnPosFormat);
	IdleGiftStorage.Load("idlegift", gIdleGiftFormat);
	PlaytimeTriggerStorage.Load("Playtime_Trigger", gPlaytimeTriggerFormat);
	//QuestTipsStorage.Load("QuestTips", gQuestTipsFormat);
	ItemPrototypeStorage.Load("items_copy", gItemPrototypeFormat);
	CreatureNameStorage.Load("creature_names_copy", gCreatureNameFormat);
	GameObjectNameStorage.Load("gameobject_names", gGameObjectNameFormat);
	CreatureProtoStorage.Load("creature_proto_copy", gCreatureProtoFormat);
	AreaTriggerStorage.Load("areatriggers", gAreaTriggerFormat);
	ItemPageStorage.Load("itempages", gItemPageFormat);
	QuestStorage.Load("quests_copy", gQuestFormat);
	GraveyardStorage.Load("graveyards", gGraveyardFormat);
	TeleportCoordStorage.Load("teleport_coords", gTeleportCoordFormat);
	FishingZoneStorage.Load("fishing", gFishingFormat);
	NpcTextStorage.Load("npc_text", gNpcTextFormat);
	WorldMapInfoStorage.Load("worldmap_info", gWorldMapInfoFormat);
	ZoneGuardStorage.Load("zoneguards", gZoneGuardsFormat);

	AreaStorage.Load("AreaTable_dbc", gAreaTableFormat);
	GemPropertyStorage.Load("GemProperty_dbc", gGemPropertyEntryFormat);
	ItemSetStorage.Load("ItemSetEntry_dbc", gItemSetFormat);
	dbcLock.Load("Lock_dbc", gLockFormat);
	dbcSpell.Load("Spell_dbc", gSpellFormat);
	objmgr.LoadAIThreatToSpellId();
	dbcSpellDuration.Load("SpellDuration_dbc", gSpelldurationFormat);
	dbcSpellRadius.Load("SpellRadius_dbc", gSpellradiusFormat);
	dbcSpellRange.Load("SpellRange_dbc", gSpellrangeFormat);
	dbcSpellCastTime.Load("SpellCastTime_dbc", gSpellcasttimeFormat);

	dbcMeleeCrit.Load("gtChanceToMeleeCrit_dbc", gtfloatformat);
	dbcMeleeCritBase.Load("gtChanceToMeleeCritBase_dbc", gtfloatformat);
	dbcSpellCrit.Load("gtChanceToSpellCrit_dbc", gtfloatformat);
	dbcSpellCritBase.Load("gtChanceToSpellCritBase_dbc", gtfloatformat);
	dbcHPRegen.Load("gtRegenHPPerSpt_dbc", gtfloatformat);
	dbcHPRegenBase.Load("gtOCTRegenHP_dbc", gtfloatformat);
	dbcManaRegen.Load("gtRegenMPPerSpt_dbc", gtfloatformat);
	dbcManaRegenBase.Load("gtOCTRegenMP_dbc", gtfloatformat);

	dbcFaction.Load("faction_dbc", gFactiondbcFormat);
	dbcFactionTemplate.Load("factionTemplate_dbc", gFactiontemplatedbcFormat);

	dbcTaxiNode.Load("TaxiNode_dbc", gTaxinodeFormat);
	dbcTaxiPath.Load("TaxiPath_dbc", gTaxipathFormat);
	dbcTaxiPathNode.Load("TaxiPathNode_dbc", gTaxipathnodeFormat);

	dbcEnchant.Load("EnchantEntry_dbc", gEnchantEntrYFormat);
	dbcRandomProps.Load("randomprops_dbc", gRandompropsFormat);
	dbcSkillLineSpell.Load("skilllinespell_dbc", gSkilllinespellFormat);
	dbcSkillLine.Load("skilllineentry_dbc", gSkilllineentrYFormat);

	dbcAuctionHouse.Load("AuctionHouse_dbc", gAuctionhousedbcFormat);
	dbcChatChannels.Load("ChatChannel_dbc", gChatchannelformat);
	dbcMap.Load("MapEntry_dbc", gMapentryFormat);
	//dbcEmoteEntry.Load(")
	dbcCreatureSpellData.Load("CreatureSpellDataEntry_dbc", gCreaturespelldataFormat);
	dbcCreatureFamily.Load("CreatureFamilyEntry_dbc", gCreaturefamilyFormat);
	dbcCharClass.Load("CharClassEntry_dbc", gCharclassFormat);
	dbcCharRace.Load("CharRaceEntry_dbc", gCharraceFormat);

	dbcItemExtendedCost.Load("ItemExtendedCostEntry_dbc", gItemextendedcostFormat);
	dbcItemRandomSuffix.Load("ItemRandomSuffixEntry_dbc", gItemrandomsuffixformat);
	dbcDurabilityCosts.Load("DurabilityCostsEntry_dbc", gDurabilitycostsFormat);
	dbcDurabilityQuality.Load("DurabilityQualityEntry_dbc", gDurabilityqualityFormat);
	dbcBankSlotPrices.Load("BankSlotPrices_dbc", gBankslotpriceformat);
	dbcStableSlotPrices.Load("StableSlotPrices_dbc", gBankslotpriceformat);

	dbcCombatRating.Load("CombatRating_dbc", gtfloatformat);
	dbcTalent.Load("TalentEntry_dbc", gTalententryFormat);
	dbcTalentTab.Load("TalentTabEntry_dbc", gTalenttabentryFormat);

	dbcNameGen.Load("NameGen_dbc", gNameGenFormat);
	dbcLfgDungeonTypes.Load("LfgDungeonTypes_dbc", gLfgDungeonFormat);
	dbcSunyouInstanceSpawnInfo.Load( "sunyou_instance_spawn_list", gSunyouInstanceSpawnListFormat );
	dbcContributionGift.Load( "contribution_gift", gContributionGiftFormat );
	dbcDynamicInstanceConfiguration.Load( "dynamic_instance_configuration", gDynamicInstanceConfiguration );
	dbcSunyouInstancePeriodicGift.Load( "sunyou_instance_periodic_gift", gSunyouInstancePeriodicGift );
	dbcRefineItemGem.Load( "refine_item_gem_list", gRefineItemGem );
// 	dbcRefineItemUpgrade.Load( "refine_item_upgrade_list", gRefineItemUpgrade );
 	dbcRefineItemEffect.Load( "refine_item_effect_list", gRefineItemEffect );
	dbcCombineMaterial.Load( "combine_material", gCombineMaterial );
	dbcCombineNeed.Load( "combine_need", gCombineNeed );
	dbcCombineSpecialList.Load( "combine_special_list", gCombineSpecial );
	dbcGuildKillBonusList.Load( "guild_kill_bonus_list", gGuildKillBonusList ); 
	dbcPetFoodList.Load( "pet_food_list", gPetFoodList );
	dbcPetEggList.Load( "pet_egg_list", gPetEggList );
	dbcCastleNPCList.Load( "castle_npc_list", gCastleNPCList );
	dbcCastleConfig.Load( "castle_config", gCastleConfig );
	dbcGuildUpgradeList.Load( "guild_upgrade_list", gGuildUpgradeList );
	dbcSunyouBattleGroundList.Load( "sunyou_battle_ground_list", gSunyouBattleGroundList );
	dbcBattleGroundNpcList.Load( "battle_ground_npc_list", gBattleGroundNpcList );
	dbcCreatureHonorList.Load( "creature_honor_list", gCreatureHonorList );
	dbcCreatureSayList.Load( _creature_say_list, gCreatureSayList );
	dbcNpcScriptTrans.Load("npcscript_trans", gNpcScriptTrans);
	dbcNpcTransPos.Load("npctranspos", gNpcTransPos);
	dbcTitleRewards.Load( "title_reward", gTitleRewards );
	dbcPositionList.Load( "position_storage", gPositionList );
	dbcShapeSpellConf.Load( "shapespell_conf", gShapeSpellConf );
	dbcEquipRefineRate.Load("EquipRefineRate",  gEquipRefineRate);

	ItemPriceFix();

	//ApplyNormalFixes();
}

void ItemPriceFix()
{
	StorageContainerIterator<ItemPrototype> * itr = ItemPrototypeStorage.MakeIterator();
	ItemPrototype * proto;uint32 cost;
	while(!itr->AtEnd())
	{
		proto = itr->Get();
		cost = proto->BuyPrice;

		if(proto->Class == ITEM_CLASS_CONSUMABLE && proto->SubClass == ITEM_SUBCLASS_CONSUMABLE_POTION)
		{
			cost =(22.5f * proto->ItemLevel * 60 ) * 0.5f / 100 + 0.5f;
		}
		else if(proto->Class == ITEM_CLASS_WEAPON || proto->Class == ITEM_CLASS_ARMOR)
		{
			cost = proto->Damage[0].Max + proto->Armor;
			for(int i = 0; i < 10; i++)
			{
				cost += proto->Stats[i].Value * priceofstat[proto->Stats[i].Type]*8  + 0.5f;
			}

		}
// 		else if(proto->Class == ITEM_CLASS_RECIPE)
// 		{
// 			cost = 40.f * proto->ItemLevel + 0.5f;
// 		}

		proto->BuyPrice = cost;
		proto->SellPrice = cost/2;

		if( !proto->SellPrice )
			proto->SellPrice = 1;
		if( !proto->BuyPrice )
			proto->BuyPrice = 1;

		if(!itr->Inc())
			break;
	}
	itr->Destruct();	
}

void Storage_Cleanup()
{
	{
		StorageContainerIterator<CreatureProto> * itr = CreatureProtoStorage.MakeIterator();
		CreatureProto * p;
		while(!itr->AtEnd())
		{
			p = itr->Get();
			for(list<AI_Spell*>::iterator it = p->spells.begin(); it != p->spells.end(); ++it)
				delete (*it);
			p->spells.clear();
			p->start_auras.clear();
			if(!itr->Inc())
				break;
		}
		itr->Destruct();
	}
	Consume2GiftStorage.Cleanup();
	SystemNotifyStorage.Cleanup();
	MapSpawnPosStorage.Cleanup();
	IdleGiftStorage.Cleanup();
	PlaytimeTriggerStorage.Cleanup();
	//QuestTipsStorage.Cleanup();
	ItemPrototypeStorage.Cleanup();
	CreatureNameStorage.Cleanup();
	GameObjectNameStorage.Cleanup();
	CreatureProtoStorage.Cleanup();
	AreaTriggerStorage.Cleanup();
	ItemPageStorage.Cleanup();
	QuestStorage.Cleanup();
	GraveyardStorage.Cleanup();
	TeleportCoordStorage.Cleanup();
	FishingZoneStorage.Cleanup();
	NpcTextStorage.Cleanup();
	WorldMapInfoStorage.Cleanup();
	ZoneGuardStorage.Cleanup();
	AreaStorage.Cleanup();
	GemPropertyStorage.Cleanup();
	ItemSetStorage.Cleanup();
	dbcLock.Cleanup();
	dbcSpell.Cleanup();
	dbcSpellDuration.Cleanup();
	dbcSpellRadius.Cleanup();
	dbcSpellRange.Cleanup();
	dbcSpellCastTime.Cleanup();
	dbcMeleeCrit.Cleanup();
	dbcMeleeCritBase.Cleanup();
	dbcSpellCrit.Cleanup();
	dbcSpellCritBase.Cleanup();
	dbcHPRegen.Cleanup();
	dbcHPRegenBase.Cleanup();
	dbcManaRegen.Cleanup();
	dbcManaRegenBase.Cleanup();
	dbcFaction.Cleanup();
	dbcFactionTemplate.Cleanup();
	dbcTaxiNode.Cleanup();
	dbcTaxiPath.Cleanup();
	dbcTaxiPathNode.Cleanup();
	dbcEnchant.Cleanup();
	dbcRandomProps.Cleanup();
	dbcSkillLineSpell.Cleanup();
	dbcSkillLine.Cleanup();

	dbcAuctionHouse.Cleanup();
	dbcChatChannels.Cleanup();
	dbcMap.Cleanup();

	dbcCreatureSpellData.Cleanup();
	dbcCreatureFamily.Cleanup();
	dbcCharClass.Cleanup();
	dbcCharRace.Cleanup();

	dbcItemExtendedCost.Cleanup();
	dbcItemRandomSuffix.Cleanup();
	dbcDurabilityCosts.Cleanup();
	dbcDurabilityQuality.Cleanup();
	dbcBankSlotPrices.Cleanup();
	dbcStableSlotPrices.Cleanup();

	dbcCombatRating.Cleanup();
	dbcTalent.Cleanup();
	dbcTalentTab.Cleanup();

	dbcNameGen.Cleanup();
	dbcLfgDungeonTypes.Cleanup();
	dbcSunyouInstanceSpawnInfo.Cleanup();
	dbcContributionGift.Cleanup();
	dbcDynamicInstanceConfiguration.Cleanup();
	dbcSunyouInstancePeriodicGift.Cleanup();
	dbcRefineItemGem.Cleanup();
// 	dbcRefineItemUpgrade.Cleanup();
 	dbcRefineItemEffect.Cleanup();
	dbcCombineMaterial.Cleanup();
	dbcCombineNeed.Cleanup();
	dbcCombineSpecialList.Cleanup();
	dbcGuildKillBonusList.Cleanup();
	dbcPetFoodList.Cleanup();
	dbcPetEggList.Cleanup();
	dbcCastleNPCList.Cleanup();
	dbcCastleConfig.Cleanup();
	dbcGuildUpgradeList.Cleanup();
	dbcSunyouBattleGroundList.Cleanup();
	dbcBattleGroundNpcList.Cleanup();
	dbcCreatureHonorList.Cleanup();
	dbcCreatureSayList.Cleanup();
	dbcNpcScriptTrans.Cleanup();
	dbcNpcTransPos.Cleanup();
	dbcTitleRewards.Cleanup();
	dbcPositionList.Cleanup();
	dbcShapeSpellConf.Cleanup();
}

vector<pair<string,string> > additionalTables;

bool LoadAdditionalTable(const char * TableName, const char * SecondName)
{
	if(!stricmp(TableName, "creature_spawns"))
	{
		ExtraMapCreatureTables.insert(string(SecondName));
		return false;
	}
	else if(!stricmp(TableName, "consume2gift"))
	{
		ExtraMapCreatureTables.insert(string(SecondName));
	}
	else if(!stricmp(TableName, "gameobject_spawns"))
	{
		ExtraMapGameObjectTables.insert(string(SecondName));
		return false;
	}
	else if(!stricmp(TableName, "SystemNotify"))
		SystemNotifyStorage.LoadAdditionalData(SecondName, gSystemNotifyFormat);
	else if(!stricmp(TableName, "MapSpawnPos"))
		MapSpawnPosStorage.LoadAdditionalData(SecondName, gMapSpawnPosFormat);
	else if(!stricmp(TableName, "idlegift"))
		IdleGiftStorage.LoadAdditionalData(SecondName, gIdleGiftFormat);
	else if(!stricmp(TableName, "Playtime_Trigger"))
		PlaytimeTriggerStorage.LoadAdditionalData(SecondName, gPlaytimeTriggerFormat);
	else if(!stricmp(TableName, "items_copy"))					// Items
		ItemPrototypeStorage.LoadAdditionalData(SecondName, gItemPrototypeFormat);
	else if(!stricmp(TableName, "creature_proto_copy"))		// Creature Proto
		CreatureProtoStorage.LoadAdditionalData(SecondName, gCreatureProtoFormat);
	else if(!stricmp(TableName, "creature_names_copy"))		// Creature Names
		CreatureNameStorage.LoadAdditionalData(SecondName, gCreatureNameFormat);
	else if(!stricmp(TableName, "gameobject_names"))	// GO Names
		GameObjectNameStorage.LoadAdditionalData(SecondName, gGameObjectNameFormat);
	else if(!stricmp(TableName, "areatriggers"))		// Areatriggers
		AreaTriggerStorage.LoadAdditionalData(SecondName, gAreaTriggerFormat);
	else if(!stricmp(TableName, "itempages"))			// Item Pages
		ItemPageStorage.LoadAdditionalData(SecondName, gItemPageFormat);
	else if(!stricmp(TableName, "quests_copy"))				// Quests
		QuestStorage.LoadAdditionalData(SecondName, gQuestFormat);
	else if(!stricmp(TableName, "npc_text"))			// NPC Text Storage
		NpcTextStorage.LoadAdditionalData(SecondName, gNpcTextFormat);
	else if(!stricmp(TableName, "fishing"))				// Fishing Zones
		FishingZoneStorage.LoadAdditionalData(SecondName, gFishingFormat);
	else if(!stricmp(TableName, "teleport_coords"))		// Teleport coords
		TeleportCoordStorage.LoadAdditionalData(SecondName, gTeleportCoordFormat);
	else if(!stricmp(TableName, "graveyards"))			// Graveyards
		GraveyardStorage.LoadAdditionalData(SecondName, gGraveyardFormat);
	else if(!stricmp(TableName, "worldmap_info"))		// WorldMapInfo
		WorldMapInfoStorage.LoadAdditionalData(SecondName, gWorldMapInfoFormat);
	else if(!stricmp(TableName, "zoneguards"))
		ZoneGuardStorage.LoadAdditionalData(SecondName, gZoneGuardsFormat);
	else if(!stricmp(TableName, "AreaTable_dbc"))
		AreaStorage.LoadAdditionalData(SecondName, gAreaTableFormat);
	else if(!stricmp(TableName, "GemProperty_dbc"))
		GemPropertyStorage.LoadAdditionalData("GemProperty_dbc", gGemPropertyEntryFormat);
	else if(!stricmp(TableName, "ItemSet_dbc"))
		ItemSetStorage.LoadAdditionalData("ItemSet_dbc", gItemSetFormat);
	else if(!stricmp(TableName, "Lock_dbc"))
		dbcLock.LoadAdditionalData(TableName, gLockFormat);
	else if(!stricmp(TableName, "Spell_dbc"))
		dbcSpell.LoadAdditionalData(SecondName, gSpellFormat);
	else if(!stricmp(TableName, "SpellDuration_dbc"))
		dbcSpellDuration.LoadAdditionalData(SecondName, gSpelldurationFormat);
	else if(!stricmp(TableName, "SpellRadius_dbc"))
		dbcSpellRadius.LoadAdditionalData(SecondName, gSpellradiusFormat);
	else if(!stricmp(TableName, "SpellRange_dbc"))
		dbcSpellRange.LoadAdditionalData(SecondName, gSpellrangeFormat);
	else if(!stricmp(TableName, "SpellCastTime_dbc"))
		dbcSpellCastTime.LoadAdditionalData(SecondName, gSpellcasttimeFormat);
	else if(!stricmp(TableName, "gtChanceToMeleeCrit_dbc"))
		dbcMeleeCrit.LoadAdditionalData(SecondName, gtfloatformat);
	else if(!stricmp(TableName, "gtChanceToMeleeCritBase_dbc"))
		dbcMeleeCritBase.LoadAdditionalData(SecondName, gtfloatformat);
	else if(!stricmp(TableName, "gtChanceToSpellCrit_dbc"))
		dbcSpellCrit.LoadAdditionalData(SecondName, gtfloatformat);
	else if(!stricmp(TableName, "gtChanceToSpellCritBase_dbc"))
		dbcSpellCritBase.LoadAdditionalData(SecondName, gtfloatformat);
	else if(!stricmp(TableName, "gtRegenHPPerSpt_dbc"))
		dbcHPRegen.LoadAdditionalData(SecondName, gtfloatformat);
	else if(!stricmp(TableName, "gtOCTRegenHP_dbc"))
		dbcHPRegenBase.LoadAdditionalData(SecondName, gtfloatformat);
	else if(!stricmp(TableName, "gtRegenMPPerSpt_dbc"))
		dbcManaRegen.LoadAdditionalData(SecondName, gtfloatformat);
	else if(!stricmp(TableName, "gtOCTRegenMP_dbc"))
		dbcManaRegenBase.LoadAdditionalData(SecondName, gtfloatformat);
	else if(!stricmp(TableName, "faction_dbc"))
		dbcFaction.LoadAdditionalData(SecondName, gFactiondbcFormat);
	else if(!stricmp(TableName, "factionTemplate_dbc"))
		dbcFactionTemplate.LoadAdditionalData(SecondName, gFactiontemplatedbcFormat);
	else if(!stricmp(TableName, "TaxiNode_dbc"))
		dbcTaxiNode.LoadAdditionalData(SecondName, gTaxinodeFormat);
	else if(!stricmp(TableName, "TaxiPath_dbc"))
		dbcTaxiPath.LoadAdditionalData(SecondName, gTaxipathFormat);
	else if(!stricmp(TableName, "TaxiPathNode_dbc"))
		dbcTaxiPathNode.LoadAdditionalData(SecondName, gTaxipathnodeFormat);
	else if(!stricmp(TableName, "EnchantEntry_dbc"))
		dbcEnchant.LoadAdditionalData(SecondName, gEnchantEntrYFormat);
	else if(!stricmp(TableName, "randomprops_dbc"))
		dbcRandomProps.LoadAdditionalData(SecondName, gRandompropsFormat);
	else if(!stricmp(TableName, "skilllinespell_dbc"))
		dbcSkillLineSpell.LoadAdditionalData(SecondName, gSkilllinespellFormat);
	else if(!stricmp(TableName, "skilllineentry_dbc"))
		dbcSkillLine.LoadAdditionalData(SecondName, gSkilllineentrYFormat);
	else if(!stricmp(TableName, "AuctionHouse_dbc"))
		dbcAuctionHouse.LoadAdditionalData(SecondName, gAuctionhousedbcFormat);
	else if(!stricmp(TableName, "ChatChannel_dbc"))
		dbcChatChannels.LoadAdditionalData(SecondName, gChatchannelformat);
	else if(!stricmp(TableName, "MapEntry_dbc"))
		dbcMap.LoadAdditionalData(SecondName, gMapentryFormat);
	else if(!stricmp(TableName, "CreatureSpellDataEntry_dbc"))
		dbcCreatureSpellData.LoadAdditionalData(SecondName, gCreaturespelldataFormat);
	else if(!stricmp(TableName, "CreatureFamilyEntry_dbc"))
		dbcCreatureFamily.LoadAdditionalData(SecondName, gCreaturefamilyFormat);
	else if(!stricmp(TableName, "CharClassEntry_dbc"))
		dbcCharClass.LoadAdditionalData(SecondName, gCharclassFormat);
	else if(!stricmp(TableName, "CharRaceEntry_dbc"))
		dbcCharRace.LoadAdditionalData(SecondName, gCharraceFormat);
	else if(!stricmp(TableName, "ItemExtendedCostEntry_dbc"))
		dbcItemExtendedCost.LoadAdditionalData(SecondName, gItemextendedcostFormat);
	else if(!stricmp(TableName, "ItemRandomSuffixEntry_dbc"))
		dbcItemRandomSuffix.LoadAdditionalData(SecondName, gItemrandomsuffixformat);
	else if(!stricmp(TableName, "DurabilityCostsEntry_dbc"))
		dbcDurabilityCosts.LoadAdditionalData(SecondName, gDurabilitycostsFormat);
	else if(!stricmp(TableName, "DurabilityQualityEntry_dbc"))
		dbcDurabilityQuality.LoadAdditionalData(SecondName, gDurabilityqualityFormat);
	else if(!stricmp(TableName, "BankSlotPrices_dbc"))
		dbcBankSlotPrices.LoadAdditionalData(SecondName, gBankslotpriceformat);
	else if(!stricmp(TableName, "StableSlotPrices_dbc"))
		dbcStableSlotPrices.LoadAdditionalData(SecondName, gBankslotpriceformat);
	else if(!stricmp(TableName, "CombatRating_dbc"))
		dbcCombatRating.LoadAdditionalData(SecondName, gtfloatformat);
	else if(!stricmp(TableName, "TalentEntry_dbc"))
		dbcTalent.LoadAdditionalData(SecondName, gTalententryFormat);
	else if(!stricmp(TableName, "TalentTabEntry_dbc"))
		dbcTalentTab.LoadAdditionalData(SecondName, gTalenttabentryFormat);
	else if(!stricmp(TableName, "NameGen_dbc"))
		dbcNameGen.LoadAdditionalData(SecondName, gNameGenFormat);
	else if(!stricmp(TableName, "LfgDungeonTypes_dbc"))
		dbcLfgDungeonTypes.LoadAdditionalData(SecondName, gLfgDungeonFormat);
	else if(!stricmp(TableName, "npcscript_trans"))
		dbcNpcScriptTrans.LoadAdditionalData(SecondName, gNpcScriptTrans);
	else if(!stricmp(TableName, "npctranspos"))
		dbcNpcTransPos.LoadAdditionalData(SecondName, gNpcTransPos);
	else
		return false;

	return true;
}

bool Storage_ReloadTable(const char * TableName)
{
	// bur: mah god this is ugly :P
	if(!stricmp(TableName, "QuestTips"))
		QuestTipsStorage.Reload();
	else if(!stricmp(TableName, "title"))
		PlayerTitleStorage.Reload();
	else if(!stricmp(TableName, "default_additem"))
		AddItemStorage.Reload();
	else if(!stricmp(TableName, "consume2gift"))
		Consume2GiftStorage.Reload();
	else if(!stricmp(TableName, "items_copy"))					// Items
		ItemPrototypeStorage.Reload();
	else if(!stricmp(TableName, "creature_proto_copy"))		// Creature Proto
		CreatureProtoStorage.Reload();
	else if(!stricmp(TableName, "creature_names_copy"))		// Creature Names
		CreatureNameStorage.Reload();
	else if(!stricmp(TableName, "gameobject_names"))	// GO Names
		GameObjectNameStorage.Reload();
	else if(!stricmp(TableName, "areatriggers"))		// Areatriggers
		AreaTriggerStorage.Reload();
	else if(!stricmp(TableName, "itempages"))			// Item Pages
		ItemPageStorage.Reload();
	else if(!stricmp(TableName, "quests_copy"))				// Quests
		QuestStorage.Reload();
	else if(!stricmp(TableName, "npc_text"))			// NPC Text Storage
		NpcTextStorage.Reload();
	else if(!stricmp(TableName, "fishing"))				// Fishing Zones
		FishingZoneStorage.Reload();
	else if(!stricmp(TableName, "teleport_coords"))		// Teleport coords
		TeleportCoordStorage.Reload();
	else if(!stricmp(TableName, "graveyards"))			// Graveyards
		GraveyardStorage.Reload();
	else if(!stricmp(TableName, "worldmap_info"))		// WorldMapInfo
		WorldMapInfoStorage.Reload();
	else if(!stricmp(TableName, "zoneguards"))
		ZoneGuardStorage.Reload();
	else if(!stricmp(TableName, "Spell_dbc"))
	{
		dbcSpell.Reload();
		objmgr.LoadAIThreatToSpellId();
		ApplyNormalFixes();
	}
	else if(!stricmp(TableName, "areatable_dbc"))
		AreaStorage.Reload();
	else if(!stricmp(TableName, "GemProperty_dbc"))
		GemPropertyStorage.Reload();
	else if(!stricmp(TableName, "ItemSet_dbc"))
		ItemSetStorage.Reload();
	else if(!stricmp(TableName, "Lock_dbc"))
		dbcLock.Reload();
	else if(!stricmp(TableName, "SpellDuration_dbc"))
		dbcSpellDuration.Reload();
	else if(!stricmp(TableName, "SpellRidius_dbc"))
		dbcSpellRadius.Reload();
	else if(!stricmp(TableName, "SpellRange_dbc"))
		dbcSpellRange.Reload();
	else if(!stricmp(TableName, "SpellCastTime_dbc"))
		dbcSpellCastTime.Reload();
	else if(!stricmp(TableName, "gtChanceToMeleeCrit_dbc"))
		dbcMeleeCrit.Reload();
	else if(!stricmp(TableName, "gtChanceToMeleeCritBase_dbc"))
		dbcMeleeCritBase.Reload();
	else if(!stricmp(TableName, "gtChanceToSpellCrit_dbc"))
		dbcSpellCrit.Reload();
	else if(!stricmp(TableName, "gtChanceToSpellCritBase_dbc"))
		dbcSpellCritBase.Reload();
	else if(!stricmp(TableName, "gtRegenHPPerSpt_dbc"))
		dbcHPRegen.Reload();
	else if(!stricmp(TableName, "gtOCTRegenHP_dbc"))
		dbcHPRegenBase.Reload();
	else if(!stricmp(TableName, "gtRegenMPPerSpt_dbc"))
		dbcManaRegen.Reload();
	else if(!stricmp(TableName, "gtOCTRegenMP_dbc"))
		dbcManaRegenBase.Reload();
	else if(!stricmp(TableName, "faction_dbc"))
		dbcFaction.Reload();
	else if(!stricmp(TableName, "factionTemplate_dbc"))
		dbcFactionTemplate.Reload();
	else if(!stricmp(TableName, "TaxiNode_dbc"))
		dbcTaxiNode.Reload();
	else if(!stricmp(TableName, "TaxiPath_dbc"))
		dbcTaxiPath.Reload();
	else if(!stricmp(TableName, "TaxiPathNode_dbc"))
		dbcTaxiPathNode.Reload();
	else if(!stricmp(TableName, "EnchantEntry_dbc"))
		dbcEnchant.Reload();
	else if(!stricmp(TableName, "randomprops_dbc"))
		dbcRandomProps.Reload();
	else if(!stricmp(TableName, "skilllinespell_dbc"))
		dbcSkillLineSpell.Reload();
	else if(!stricmp(TableName, "skilllineentry_dbc"))
		dbcSkillLine.Reload();
	else if(!stricmp(TableName, "AuctionHouse_dbc"))
		dbcAuctionHouse.Reload();
	else if(!stricmp(TableName, "AuctionHouse"))
		dbcAuctionHouse.Reload();
	else if(!stricmp(TableName, "ChatChannel_dbc"))
		dbcChatChannels.Reload();
	else if(!stricmp(TableName, "MapEntry_dbc"))
		dbcMap.Reload();
	else if(!stricmp(TableName, "CreatureSpellDataEntry_dbc"))
		dbcCreatureSpellData.Reload();
	else if(!stricmp(TableName, "CreatureFamilyEntry_dbc"))
		dbcCreatureFamily.Reload();
	else if(!stricmp(TableName, "CharClassEntry_dbc"))
		dbcCharClass.Reload();
	else if(!stricmp(TableName, "CharRaceEntry_dbc"))
		dbcCharRace.Reload();
	else if(!stricmp(TableName, "ItemExtendedCostEntry_dbc"))
		dbcItemExtendedCost.Reload();
	else if(!stricmp(TableName, "ItemRandomSuffixEntry_dbc"))
		dbcItemRandomSuffix.Reload();
	else if(!stricmp(TableName, "DurabilityCostsEntry_dbc"))
		dbcDurabilityCosts.Reload();
	else if(!stricmp(TableName, "DurabilityQualityEntry_dbc"))
		dbcDurabilityQuality.Reload();
	else if(!stricmp(TableName, "BankSlotPrices_dbc"))
		dbcBankSlotPrices.Reload();
	else if(!stricmp(TableName, "StableSlotPrices_dbc"))
		dbcStableSlotPrices.Reload();
	else if(!stricmp(TableName, "CombatRating_dbc"))
		dbcCombatRating.Reload();
	else if(!stricmp(TableName, "TalentEntry_dbc"))
		dbcTalent.Reload();
	else if(!stricmp(TableName, "TalentTabEntry_dbc"))
		dbcTalentTab.Reload();
	else if(!stricmp(TableName, "NameGen_dbc"))
		dbcNameGen.Reload();
	else if(!stricmp(TableName, "LfgDungeonTypes_dbc"))
		dbcLfgDungeonTypes.Reload();
	else if(!stricmp(TableName, "sunyou_instance_spawn_list"))
		dbcSunyouInstanceSpawnInfo.Reload();
	else if(!stricmp(TableName, "contribution_gift"))
		dbcContributionGift.Reload();
	else if(!stricmp(TableName, "dynamic_instance_configuration"))
		dbcDynamicInstanceConfiguration.Reload();
	else if(!stricmp(TableName, "sunyou_instance_periodic_gift"))
		dbcSunyouInstancePeriodicGift.Reload();
	else if( !stricmp( TableName, "refine_item_gem_list" ) )
		dbcRefineItemGem.Reload();
// 	else if( !stricmp( TableName, "refine_item_upgrade_list" ) )
// 		dbcRefineItemUpgrade.Reload();
// 	else if( !stricmp( TableName, "refine_item_effect_list" ) )
// 		dbcRefineItemEffect.Reload();
	else if( !stricmp( TableName, "combine_material" ) )
		dbcCombineMaterial.Reload();
	else if( !stricmp( TableName, "combine_need" ) )
		dbcCombineNeed.Reload();
	else if( !stricmp( TableName, "combine_special_list" ) )
		dbcCombineSpecialList.Reload();
	else if( !stricmp( TableName, "guild_kill_bonus_list" ) )
		dbcGuildKillBonusList.Reload();
	else if(!stricmp(TableName, "pet_food_list" ) )
		dbcPetFoodList.Reload();
	else if(!stricmp(TableName, "pet_egg_list" ) )
		dbcPetEggList.Reload();
	else if( !stricmp( TableName, "castle_npc_list" ) )
		dbcCastleNPCList.Reload();
	else if( !stricmp( TableName, "castle_config" ) )
		dbcCastleConfig.Reload();
	else if( !stricmp( TableName, "guild_upgrade_list" ) )
		dbcGuildUpgradeList.Reload();
	else if( !stricmp( TableName, "sunyou_battle_ground_list" ) )
		dbcSunyouBattleGroundList.Reload();
	else if( !stricmp( TableName, "battle_ground_npc_list" ) )
		dbcBattleGroundNpcList.Reload();
	else if( !stricmp( TableName, "creature_honor_list" ) )
		dbcCreatureHonorList.Reload();
	else if( !stricmp( TableName, _creature_say_list ) )
		dbcCreatureSayList.Reload();
	else if(!stricmp(TableName, "command_overrides"))	// Command Overrides
	{
		CommandTableStorage::getSingleton().Dealloc();
		CommandTableStorage::getSingleton().Init();
		CommandTableStorage::getSingleton().Load();
	}
	else if(!stricmp(TableName, "SystemNotify"))
		SystemNotifyStorage.Reload();
	else if(!stricmp(TableName, "mapspawnpos"))
		MapSpawnPosStorage.Reload();
	else if(!stricmp(TableName, "shopitems"))
		sShopMgr.LoadShopItems();
	else if(!stricmp(TableName, "npcscript_trans"))
		dbcNpcScriptTrans.Reload();
	else if(!stricmp(TableName, "npctranspos"))
		dbcNpcTransPos.Reload();
	else if(!stricmp(TableName, "title_reward") )
		dbcTitleRewards.Reload();
	else if(!stricmp(TableName, "position_storage"))
		dbcPositionList.Reload();
	else if(!stricmp(TableName, "shapespell_conf" ))
		dbcShapeSpellConf.Reload();
	else
		return false;
	
	uint32 len = (uint32)strlen(TableName);
	uint32 len2;
	for(vector<pair<string,string> >::iterator itr = additionalTables.begin(); itr != additionalTables.end(); ++itr)
	{
		len2=(uint32)itr->second.length();
		if(!strnicmp(TableName, itr->second.c_str(), min(len,len2)))
			LoadAdditionalTable(TableName, itr->first.c_str());
	}
	return true;
}

void Storage_LoadAdditionalTables()
{
	ExtraMapCreatureTables.insert(string("creature_spawns"));
	ExtraMapGameObjectTables.insert(string("gameobject_spawns"));

	string strData = Config.MainConfig.GetStringDefault("Startup", "LoadAdditionalTables", "");
	if(strData.empty())
		return;

	vector<string> strs = StrSplit(strData, ",");
	if(strs.empty())
		return;

	for(vector<string>::iterator itr = strs.begin(); itr != strs.end(); ++itr)
	{
		char s1[200];
		char s2[200];
		if(sscanf((*itr).c_str(), "%s %s", s1, s2) != 2)
			continue;

		if(LoadAdditionalTable(s2, s1)) {
			pair<string,string> tmppair;
			tmppair.first = string(s1);
			tmppair.second = string(s2);
			additionalTables.push_back(tmppair);
		}
	}
}


jinglian_item_gem* FindRefineGem( uint32 entry )
{
	jinglian_item_gem* rig = NULL;
	StorageContainerIterator<jinglian_item_gem>* it = dbcRefineItemGem.MakeIterator();
	while(!it->AtEnd())
	{
		jinglian_item_gem* temp = it->Get();
		if( temp->gem == entry )
		{
			rig = temp;
			break;
		}
		if(!it->Inc())
			break;
	}
	it->Destruct();
	return rig;
}

EquipRefineRate* FindEquipRefine(int uType)
{	
	EquipRefineRate* rig = NULL;
	StorageContainerIterator<EquipRefineRate>* it = dbcEquipRefineRate.MakeIterator();
	while(!it->AtEnd())
	{
		EquipRefineRate* temp = it->Get();
		if( temp->Type == uType )
		{
			rig = temp;
			break;
		}
		if(!it->Inc())
			break;
	}
	it->Destruct();
	return rig;
}