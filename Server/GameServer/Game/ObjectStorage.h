#ifndef OBJECT_STORAGE_H
#define OBJECT_STORAGE_H

extern SERVER_DECL SQLStorage<stPlayerTitle, HashMapStorageContainer<stPlayerTitle> >				PlayerTitleStorage;
extern SERVER_DECL SQLStorage<stDefaultAddItem, HashMapStorageContainer<stDefaultAddItem> >				AddItemStorage;
extern SERVER_DECL SQLStorage<stConsume2GiftEntry, HashMapStorageContainer<stConsume2GiftEntry> >	Consume2GiftStorage;
extern SERVER_DECL SQLStorage<stSystemNotify, HashMapStorageContainer<stSystemNotify> >			SystemNotifyStorage;
extern SERVER_DECL SQLStorage<MapSpawnPos, HashMapStorageContainer<MapSpawnPos> >				MapSpawnPosStorage;
extern SERVER_DECL SQLStorage<Idle_Gift, HashMapStorageContainer<Idle_Gift> >					IdleGiftStorage;
extern SERVER_DECL SQLStorage<Playtime_Trigger, HashMapStorageContainer<Playtime_Trigger> >		PlaytimeTriggerStorage;
extern SERVER_DECL SQLStorage<ItemPrototype, HashMapStorageContainer<ItemPrototype> >			ItemPrototypeStorage;
extern SERVER_DECL SQLStorage<CreatureInfo, HashMapStorageContainer<CreatureInfo> >				CreatureNameStorage;
extern SERVER_DECL SQLStorage<GameObjectInfo, HashMapStorageContainer<GameObjectInfo> >			GameObjectNameStorage;
extern SERVER_DECL SQLStorage<CreatureProto, HashMapStorageContainer<CreatureProto> >			CreatureProtoStorage;
// extern SERVER_DECL SQLStorage<AreaTrigger, HashMapStorageContainer<AreaTrigger> >				AreaTriggerStorage;
extern SERVER_DECL SQLStorage<ItemPage, HashMapStorageContainer<ItemPage> >						ItemPageStorage;
extern SERVER_DECL SQLStorage<Quest, HashMapStorageContainer<Quest> >							QuestStorage;
extern SERVER_DECL SQLStorage<GossipText, HashMapStorageContainer<GossipText> >					NpcTextStorage;
extern SERVER_DECL SQLStorage<GraveyardTeleport, HashMapStorageContainer<GraveyardTeleport> >	GraveyardStorage;
extern SERVER_DECL SQLStorage<TeleportCoords, HashMapStorageContainer<TeleportCoords> >			TeleportCoordStorage;
extern SERVER_DECL SQLStorage<FishingZoneEntry, HashMapStorageContainer<FishingZoneEntry> >		FishingZoneStorage;
extern SERVER_DECL SQLStorage<MapInfo, ArrayStorageContainer<MapInfo> >							WorldMapInfoStorage;
extern SERVER_DECL SQLStorage<ZoneGuardEntry, HashMapStorageContainer<ZoneGuardEntry> >			ZoneGuardStorage;
extern SERVER_DECL SQLStorage<SpellEntry, HashMapStorageContainer<SpellEntry> >					dbcSpell;
extern SERVER_DECL SQLStorage<AreaTable, ArrayStorageContainer<AreaTable> >						AreaStorage;
extern SERVER_DECL SQLStorage<GemPropertyEntry, ArrayStorageContainer<GemPropertyEntry> >		GemPropertyStorage;
extern SERVER_DECL SQLStorage<ItemSetEntry, ArrayStorageContainer<ItemSetEntry> >				ItemSetStorage;
extern SERVER_DECL SQLStorage<Lock, ArrayStorageContainer<Lock> >								dbcLock;

extern SERVER_DECL SQLStorage<SpellDuration, HashMapStorageContainer<SpellDuration> > dbcSpellDuration;
extern SERVER_DECL SQLStorage<SpellRange, HashMapStorageContainer<SpellRange> > dbcSpellRange;
extern SERVER_DECL SQLStorage<SpellCastTime, HashMapStorageContainer<SpellCastTime> > dbcSpellCastTime;
extern SERVER_DECL SQLStorage<SpellRadius, HashMapStorageContainer<SpellRadius> > dbcSpellRadius;
extern SERVER_DECL SQLStorage<emoteentry, ArrayStorageContainer<emoteentry> > dbcEmoteEntry;


extern SERVER_DECL SQLStorage<QuestTips, ArrayStorageContainer<QuestTips> > QuestTipsStorage;

extern SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 						dbcMeleeCrit;
extern SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 						dbcMeleeCritBase;
extern SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 						dbcSpellCrit;
extern SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 						dbcSpellCritBase;
extern SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 						dbcManaRegen;
extern SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 						dbcManaRegenBase;
extern SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 						dbcHPRegen;
extern SERVER_DECL SQLStorage<gtFloat, ArrayStorageContainer<gtFloat> > 						dbcHPRegenBase;

extern SERVER_DECL SQLStorage<FactionTemplateDBC, ArrayStorageContainer<FactionTemplateDBC> >	dbcFactionTemplate;
extern SERVER_DECL SQLStorage<FactionDBC, ArrayStorageContainer<FactionDBC> >					dbcFaction;

extern SERVER_DECL SQLStorage<DBCTaxiNode, ArrayStorageContainer<DBCTaxiNode> > dbcTaxiNode;
extern SERVER_DECL SQLStorage<DBCTaxiPath, ArrayStorageContainer<DBCTaxiPath> > dbcTaxiPath;
extern SERVER_DECL SQLStorage<DBCTaxiPathNode, ArrayStorageContainer<DBCTaxiPathNode> > dbcTaxiPathNode;

extern SERVER_DECL SQLStorage<EnchantEntry, ArrayStorageContainer<EnchantEntry> > dbcEnchant;
extern SERVER_DECL SQLStorage<RandomProps, ArrayStorageContainer<RandomProps> > dbcRandomProps;
extern SERVER_DECL SQLStorage<skilllinespell, ArrayStorageContainer<skilllinespell> > dbcSkillLineSpell;
extern SERVER_DECL SQLStorage<skilllineentry, ArrayStorageContainer<skilllineentry> > dbcSkillLine;

extern SERVER_DECL SQLStorage<AuctionHouseDBC,ArrayStorageContainer<AuctionHouseDBC> > dbcAuctionHouse;
extern SERVER_DECL SQLStorage<ChatChannelDBC, ArrayStorageContainer<ChatChannelDBC> > dbcChatChannels;
extern SERVER_DECL SQLStorage<MapEntry, ArrayStorageContainer<MapEntry> > dbcMap;

extern SERVER_DECL SQLStorage<CreatureSpellDataEntry, ArrayStorageContainer<CreatureSpellDataEntry> > dbcCreatureSpellData;
extern SERVER_DECL SQLStorage<CreatureFamilyEntry, ArrayStorageContainer<CreatureFamilyEntry> > dbcCreatureFamily;
extern SERVER_DECL SQLStorage<CharClassEntry, ArrayStorageContainer<CharClassEntry> > dbcCharClass;
extern SERVER_DECL SQLStorage<CharRaceEntry, ArrayStorageContainer<CharRaceEntry> > dbcCharRace;

extern SERVER_DECL SQLStorage<ItemExtendedCostEntry, ArrayStorageContainer<ItemExtendedCostEntry> > dbcItemExtendedCost;
extern SERVER_DECL SQLStorage<ItemRandomSuffixEntry, ArrayStorageContainer<ItemRandomSuffixEntry> > dbcItemRandomSuffix;
extern SERVER_DECL SQLStorage<DurabilityCostsEntry, ArrayStorageContainer<DurabilityCostsEntry> > dbcDurabilityCosts;
extern SERVER_DECL SQLStorage<DurabilityQualityEntry, ArrayStorageContainer<DurabilityQualityEntry> > dbcDurabilityQuality;
extern SERVER_DECL SQLStorage<BankSlotPrice, ArrayStorageContainer<BankSlotPrice> > dbcBankSlotPrices;
extern SERVER_DECL SQLStorage<BankSlotPrice, ArrayStorageContainer<BankSlotPrice> > dbcStableSlotPrices; //uses same structure as Bank

extern SERVER_DECL SQLStorage<TalentEntry, ArrayStorageContainer<TalentEntry> > dbcTalent;
extern SERVER_DECL SQLStorage<TalentTabEntry, ArrayStorageContainer<TalentTabEntry> > dbcTalentTab;
extern SERVER_DECL SQLStorage<CombatRatingDBC, ArrayStorageContainer<CombatRatingDBC> > dbcCombatRating;

extern SERVER_DECL SQLStorage<NameGenData, HashMapStorageContainer<NameGenData> > dbcNameGen;
extern SERVER_DECL SQLStorage<LfgDungeonType, HashMapStorageContainer<LfgDungeonType> > dbcLfgDungeonTypes;

extern SERVER_DECL SQLStorage<stNpcScriptTrans, HashMapStorageContainer<stNpcScriptTrans> > dbcNpcScriptTrans;
extern SERVER_DECL SQLStorage<stNpcTransPos, HashMapStorageContainer<stNpcTransPos> > dbcNpcTransPos;

void Storage_FillTaskList();
void Storage_Cleanup();
bool Storage_ReloadTable(const char * TableName);
void Storage_LoadAdditionalTables();

extern SERVER_DECL set<string> ExtraMapCreatureTables;
extern SERVER_DECL set<string> ExtraMapGameObjectTables;

extern SERVER_DECL SQLStorage<spawn_info, ArrayStorageContainer<spawn_info> > dbcSunyouInstanceSpawnInfo;


struct contribution_gift
{
	uint32 entry;
	uint32 is_accumulation;
	uint32 condition;
	uint32 type;
	uint32 misc1;
	uint32 misc2;
};
extern SERVER_DECL SQLStorage<contribution_gift, ArrayStorageContainer<contribution_gift> > dbcContributionGift;

struct dynamic_instance_configuration
{
	uint32 entry;
	uint32 mapid;
	uint32 cls;
	uint32 spellid[10];
	uint32 aruaid[10];
};
extern SERVER_DECL SQLStorage<dynamic_instance_configuration, ArrayStorageContainer<dynamic_instance_configuration> > dbcDynamicInstanceConfiguration;

struct sunyou_instance_periodic_gift
{
	uint32 entry;
	uint32 mapid;
	uint32 time;
	uint32 type;
	uint32 misc1;
	uint32 misc2;
};
extern SERVER_DECL SQLStorage<sunyou_instance_periodic_gift, ArrayStorageContainer<sunyou_instance_periodic_gift> > dbcSunyouInstancePeriodicGift;

struct jinglian_item_gem
{
	uint32 entry;
	uint32 gem;
	uint32 type;
	uint32 minlevel;
	uint32 maxlevel;
	uint32 chance;
};
extern SERVER_DECL SQLStorage<jinglian_item_gem, ArrayStorageContainer<jinglian_item_gem> > dbcRefineItemGem;

struct refine_item_upgrade
{
	uint32 level;
	uint32 cost;
	uint32 rate;
	uint32 chance;
};
//extern SERVER_DECL SQLStorage<refine_item_upgrade, ArrayStorageContainer<refine_item_upgrade> > dbcRefineItemUpgrade;

struct refine_item_effect
{
	uint32 entry;
	uint32 rarity;
	uint32 min_refine_level;
	uint32 max_refine_level;
	uint32 part;
	uint32 displayid;
};
extern SERVER_DECL SQLStorage<refine_item_effect, ArrayStorageContainer<refine_item_effect> > dbcRefineItemEffect;

jinglian_item_gem* FindRefineGem( uint32 entry );

struct combine_material
{
	uint32 entry;
	uint32 level;
};
extern SERVER_DECL SQLStorage<combine_material, HashMapStorageContainer<combine_material> > dbcCombineMaterial;

struct combine_need
{
	uint32 entry;

	struct __material
	{
		uint32 material;
		uint32 count;
	}need_array[4];
	uint32 cost;
};

extern SERVER_DECL SQLStorage<combine_need, HashMapStorageContainer<combine_need> > dbcCombineNeed;

struct combine_special
{
	uint32 entry;
	uint32 new_entry;
};
extern SERVER_DECL SQLStorage<combine_special, HashMapStorageContainer<combine_special> > dbcCombineSpecialList;

struct guild_kill_bonus
{
	uint32 entry;
	uint32 score;
	uint32 points;
};
extern SERVER_DECL SQLStorage<guild_kill_bonus, HashMapStorageContainer<guild_kill_bonus> > dbcGuildKillBonusList;

struct pet_food
{
	uint32 entry;
	uint32 exp;
};
extern SERVER_DECL SQLStorage<pet_food, HashMapStorageContainer<pet_food> > dbcPetFoodList;

struct pet_egg
{
	uint32 entry;
	uint32 brood_time;
};
extern SERVER_DECL SQLStorage<pet_egg, HashMapStorageContainer<pet_egg> > dbcPetEggList;

struct castle_npc
{
	uint32 entry;
	uint32 gold;
	uint32 points;
	char* comments;
	uint32 creature_entry;
};
extern SERVER_DECL SQLStorage<castle_npc, HashMapStorageContainer<castle_npc> > dbcCastleNPCList;

struct castle_config
{
	uint32 mapid;
	char* offense_resurrect_points;
	char* defence_resurrect_points;
	char* neutral_resurrect_points;
	uint32 kick_out_map_id;
	char* kick_out_points;
	char* start_fight_day_of_week;
	uint32 start_fight_hour;
	uint32 fight_duration;
	uint32 fight_prepare_time;
	char* invincible_npcs;
	uint32 enroll_cost;
	uint32 boss_id;
	uint32 exp_map_id;
	uint32 exp_rate;
};
extern SERVER_DECL SQLStorage<castle_config, HashMapStorageContainer<castle_config> > dbcCastleConfig;

struct guild_upgrade_list
{
	uint32 level;
	uint32 item_require;
	uint32 score_require;
	uint32 membercount;
	char* szdescribe;
	uint32 MemberMin;
	uint32 gold;
};

extern SERVER_DECL SQLStorage<guild_upgrade_list, ArrayStorageContainer<guild_upgrade_list> > dbcGuildUpgradeList;

struct sunyou_battle_ground
{
	uint32 entry;
	char* init_pts[9];
	char* resurrect_pts[9];
	int battle_power[3];
	uint32 win_honor_reward;
	uint32 lose_honor_reward;
	uint32 win_item_reward;
	uint32 win_item_count;
	uint32 lose_item_reward;
	uint32 lose_item_count;
	uint32 random_abc;
};

extern SERVER_DECL SQLStorage<sunyou_battle_ground, HashMapStorageContainer<sunyou_battle_ground> > dbcSunyouBattleGroundList;

struct battle_ground_npc
{
	uint32 npcid;
	uint32 mapid;
	uint32 faction;
	char* side;
	uint32 battle_power;
	uint32 spawn_time;
	uint32 dota_mode;
};

extern SERVER_DECL SQLStorage<battle_ground_npc, HashMapStorageContainer<battle_ground_npc> > dbcBattleGroundNpcList;

struct creature_honor
{
	uint32 entry;
	int honor;
};
extern SERVER_DECL SQLStorage<creature_honor, HashMapStorageContainer<creature_honor> > dbcCreatureHonorList;

struct creature_say
{
	uint32 entry;
	char* kill_player_say;
	uint32 spell_cast_id[6];
	char* spell_cast_say[6];
	char* hp_80pct_say;
	char* hp_60pct_say;
	char* hp_40pct_say;
	char* hp_20pct_say;
	char* agrro_say;
	char* idle_20pct_60sec_say;
	char* idle_50pct_60sec_say;
	char* idle_30pct_60sec_say;
	char* defeated_say;
};

extern SERVER_DECL SQLStorage<creature_say, HashMapStorageContainer<creature_say> > dbcCreatureSayList;

struct title_reward
{
	uint32 entry;
	uint32 item1;
	uint32 count1;
	uint32 item2;
	uint32 count2;
	uint32 item3;
	uint32 count3;
	uint32 need_broadcast;
	char* broadcast_words;
};

extern SERVER_DECL SQLStorage<title_reward, HashMapStorageContainer<title_reward> > dbcTitleRewards;

void ItemPriceFix();

struct position_t
{
	uint32 entry;
	uint32 mapid;
	float x;
	float y;
	float z;
	float o;
	char* comments;
};

extern SERVER_DECL SQLStorage<position_t, HashMapStorageContainer<position_t> > dbcPositionList;

struct shapespell_conf
{
	uint32 shapespell;
	char * comments;
	uint32 spell[10];
};

extern SERVER_DECL SQLStorage<shapespell_conf, HashMapStorageContainer<shapespell_conf> > dbcShapeSpellConf;

struct EquipRefineRate
{	
	int Type;
	float RefineRate;
	float gemRate;

};
extern SERVER_DECL SQLStorage<EquipRefineRate, HashMapStorageContainer<EquipRefineRate> > dbcEquipRefineRate;
EquipRefineRate* FindEquipRefine(int uType);
#endif
