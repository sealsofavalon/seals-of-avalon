#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Pet.h"
#include "../../SDBase/Protocol/S2C_Pet.h"
#include "../../SDBase/Protocol/S2C_Item.h"
#include "../../SDBase/Protocol/S2C_Spell.h"

void WorldSession::HandlePetAction(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;

	//WorldPacket data;
	MSG_C2S::stPet_Action MsgRecv;packet>>MsgRecv;
	uint64 petGuid = MsgRecv.petGuid;
	uint16 misc = MsgRecv.misc;
	uint16 action = MsgRecv.action;

	uint64 targetguid = MsgRecv.targetguid;

	//printf("Pet_Action: 0x%.4X 0x%.4X\n", misc, action);

	if(GET_TYPE_FROM_GUID(petGuid) == HIGHGUID_TYPE_UNIT)
	{
		Creature *pCharm = GetPlayer()->GetMapMgr()->GetCreature(GET_LOWGUID_PART(petGuid));
		if(!pCharm) 
			return;

		// must be a mind controled creature..
		if(action == PET_ACTION_ACTION)
		{
			switch(misc)
			{
			case PET_ACTION_ATTACK:
				{
					if(!sEventMgr.HasEvent(_player, EVENT_PLAYER_CHARM_ATTACK))
					{
						uint32 timer = pCharm->GetUInt32Value(UNIT_FIELD_BASEATTACKTIME);
						if(!timer) timer = 2000;

						sEventMgr.AddEvent(_player, &Player::_EventCharmAttack, EVENT_PLAYER_CHARM_ATTACK, timer, 0,0);
						_player->_EventCharmAttack();
					}
				}break;
			}
		}
		return;
	}
	Pet *pPet = _player->GetMapMgr()->GetPet((uint32)petGuid);
	if(!pPet || !pPet->isAlive())
		return;

	Unit *pTarget = NULL;

	if(action == PET_ACTION_SPELL || action == PET_ACTION_SPELL_1 || action == PET_ACTION_SPELL_2 || (action == PET_ACTION_ACTION && misc == PET_ACTION_ATTACK )) // >> target
	{
		pTarget = _player->GetMapMgr()->GetUnit(MsgRecv.targetguid);
		if(!pTarget) pTarget = pPet;	// target self
	}

	if(action==PET_ACTION_ACTION && misc==PET_ACTION_STAY)//sit if STAY commanded
		pPet->SetStandState(STANDSTATE_SIT);
	else 
		pPet->SetStandState(STANDSTATE_STAND);
	switch(action)
	{
	case PET_ACTION_ACTION:
		{
			pPet->SetPetAction(misc);	   // set current action
			switch(misc)
			{
			case PET_ACTION_ATTACK:
				{
					// make sure the target is attackable
					if(pTarget == pPet || !isAttackable(pPet, pTarget))
					{
						MSG_S2C::stSpell_Failure Msg;
						Msg.caster_guid		= _player->GetNewGUID();
						Msg.spell_id		= 0;
						Msg.error			= SPELL_FAILED_BAD_TARGETS;
						SendPacket(Msg);
						return;
					}

					// Clear the threat
					pPet->GetAIInterface()->WipeTargetList();
					pPet->GetAIInterface()->WipeHateList();

					// Attack target with melee if the owner if we dont have spells - other wise cast. All done by AIInterface.
					if(pPet->GetAIInterface()->getUnitToFollow() == NULL)
						pPet->GetAIInterface()->SetUnitToFollow(_player);

					// EVENT_PET_ATTACK
					pPet->GetAIInterface()->SetAIState(STATE_ATTACKING);
					pPet->GetAIInterface()->AttackReaction(pTarget, 1, 0);
				}break;
			case PET_ACTION_FOLLOW:
				{
					// Clear the threat
					pPet->GetAIInterface()->WipeTargetList();
					pPet->GetAIInterface()->WipeHateList();

					// Follow the owner... run to him...
					pPet->GetAIInterface()->SetUnitToFollow(_player);
					pPet->GetAIInterface()->HandleEvent(EVENT_FOLLOWOWNER, pPet, 0);
				}break;
			case PET_ACTION_STAY:
				{
					// Clear the threat
					pPet->GetAIInterface()->WipeTargetList();
					pPet->GetAIInterface()->WipeHateList();

					// Stop following the owner, and sit.
					pPet->GetAIInterface()->SetUnitToFollow(NULL);
				}break;
			case PET_ACTION_DISMISS:
				{
					// Bye byte...
					pPet->Dismiss();
				}break;
			}
		}break;

	case PET_ACTION_SPELL_2:
	case PET_ACTION_SPELL_1:
	case PET_ACTION_SPELL:
		{
			// misc == spellid
			SpellEntry *entry = dbcSpell.LookupEntry(misc);
			if(!entry) 
				return;

			AI_Spell * sp = pPet->GetAISpellForSpellId(entry->Id);
			if(sp)
			{
				// Check the cooldown
				if(sp->cooldowntime && getMSTime() < sp->cooldowntime)
				{
					MSG_S2C::stSpell_Failure Msg;
					Msg.caster_guid = pPet->GetNewGUID();
					Msg.spell_id	= sp->spell->Id;
					Msg.error		= SPELL_FAILED_NOT_READY;
					//SendNotification("That spell is still cooling down.");
					SendPacket(Msg);
				}
				else
				{
					if(sp->spellType != STYPE_BUFF)
					{
						// make sure the target is attackable
						if(pTarget == pPet || !isAttackable(pPet, pTarget))
						{
							MSG_S2C::stSpell_Failure Msg;
							Msg.caster_guid = _player->GetNewGUID();
							Msg.spell_id	= sp->spell->Id;
							Msg.error		= SPELL_FAILED_BAD_TARGETS;
							SendPacket(Msg);
							return;
						}
					}

					if(sp->autocast_type != AUTOCAST_EVENT_ATTACK)
					{
						if(sp->autocast_type == AUTOCAST_EVENT_OWNER_ATTACKED)
							pPet->CastSpell(_player, sp->spell, false);
						else
							pPet->CastSpell(pPet, sp->spell, false);
					}
					else
					{
						// Clear the threat
						pPet->GetAIInterface()->WipeTargetList();
						pPet->GetAIInterface()->WipeHateList();

						pPet->GetAIInterface()->AttackReaction(pTarget, 1, 0);
						pPet->GetAIInterface()->SetNextSpell(sp);
					}
				}
			}

			/*// cast spell
			SpellCastTargets targets;
			
			//HACK HACK HACK
			switch (misc)
			{
				case 7812:
				case 19438:
				case 19440:
				case 19441:
				case 19442:
				case 19443:
					targets.m_unitTarget = pPet->GetGUID(); // dono maybe it should be NULL;
					break;
				default:
					targets.m_unitTarget = (pTarget ? pTarget->GetGUID() : pPet->GetGUID());
					break;
			}
						
			targets.m_targetMask = 0x2; // unit

			pPet->GetAIInterface()->CastSpell(pPet, entry, targets);*/
		}break;
	case PET_ACTION_STATE:
		{
			pPet->SetPetState(misc);
		}break;
	default:
		{
			printf("WARNING: Unknown pet action received. Action = %.4X, Misc = %.4X\n", action, misc);
		}break;
	}

	/* Send pet action sound - WHEE THEY TALK */
	MSG_S2C::stPet_Action_Sound Msg;
	Msg.pet_guid = pPet->GetGUID();
	Msg.unk = uint32(1);
	SendPacket(Msg);
}

void WorldSession::HandlePetInfo(CPacketUsn& packet)
{
	//nothing
	MyLog::log->debug("HandlePetInfo is called");
}

void WorldSession::HandlePetNameQuery(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stQuery_Pet MsgRecv;packet>>MsgRecv;
	uint32 petNumber = MsgRecv.pet_number;
	uint64 petGuid = MsgRecv.petGuid;

	Pet *pPet = _player->GetMapMgr()->GetPet((uint32)petGuid);
	if(!pPet) return;

	MSG_S2C::stPet_Name_Query_Response Msg;
	Msg.pet_guid = pPet->GetGUID();
	Msg.pet_name = pPet->GetName();
	Msg.unk_timestamp =pPet->GetUInt32Value(UNIT_FIELD_PET_NAME_TIMESTAMP);		// stops packet flood
	SendPacket(Msg);
}

void WorldSession::HandleStablePet(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;

	// remove pet from world and association with player
	Pet *pPet = _player->GetSummon();
	if(pPet && pPet->GetUInt32Value(UNIT_CREATED_BY_SPELL) != 0) 
		return;
	
	PlayerPet *pet = _player->GetPlayerPet(_player->GetUnstabledPetNumber());
	if(!pet) return;
	pet->stablestate = STABLE_STATE_PASSIVE;
	_player->iActivePet = 0;
	pet->active = false;
	
	if(pPet) pPet->Remove(false, true, true);	// no safedelete needed

	MSG_S2C::stPet_Stable_Result Msg;
	Msg.result = uint8(0x8);  // success
	SendPacket(Msg);
}

void WorldSession::HandleUnstablePet(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;

	if(_player->isDead())
	{
		return;
	}
	MSG_C2S::stPet_UnStable MsgRecv;packet>>MsgRecv;
	uint64 npcguid = MsgRecv.npcguid;
	uint32 petnumber = MsgRecv.pet_number;

	if(_player->GetSummon())
	{

		return;
		/*_player->GetSummon()->Remove(true, true, false);*/
	}

	PlayerPet *pet = _player->GetPlayerPet(petnumber);
	if(!pet)
	{
		MyLog::log->error("PET SYSTEM: Player "I64FMT" tried to unstable non-existant pet %d", _player->GetGUID(), petnumber);
		return;
	}
	_player->SpawnPet(petnumber);
	pet->stablestate = STABLE_STATE_ACTIVE;


	MSG_S2C::stPet_Stable_Result Msg;
	Msg.result = uint8(0x9);  // success
	SendPacket(Msg);
}
void WorldSession::HandleStableSwapPet(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;

	MSG_C2S::stPet_Stable_Swap MsgRecv;packet>>MsgRecv;
	uint64 npcguid = MsgRecv.npcguid;
	uint32 petnumber = MsgRecv.pet_number;

	PlayerPet *pet = _player->GetPlayerPet(petnumber);
	if(!pet)
	{
		MyLog::log->error("PET SYSTEM: Player "I64FMT" tried to unstable non-existant pet %d", _player->GetGUID(), petnumber);
		return;
	}
	Pet *pPet = _player->GetSummon();
	if(pPet && pPet->GetUInt32Value(UNIT_CREATED_BY_SPELL) != 0) return;

	//stable current pet
	PlayerPet *pet2 = _player->GetPlayerPet(_player->GetUnstabledPetNumber());
	if(!pet2) return;
	if(pPet)
		pPet->Remove(false, true, true);	// no safedelete needed
	pet2->stablestate = STABLE_STATE_PASSIVE;

	//unstable selected pet
	_player->SpawnPet(petnumber);
	pet->stablestate = STABLE_STATE_ACTIVE;

	MSG_S2C::stPet_Stable_Result Msg;
	Msg.result = uint8(0x9);  // success
	SendPacket(Msg);
}

void WorldSession::HandleStabledPetList(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stPets_List_Stabled MsgRecv;packet>>MsgRecv;
	MSG_S2C::stPets_List_Stabled Msg;

	uint64 npcguid = MsgRecv.npc_guid;
	Msg.npc_guid = npcguid;

	Msg.StableSlotCount = uint8(_player->m_StableSlotCount);
	char i=0;
	for(std::map<uint32, PlayerPet*>::iterator itr = _player->m_Pets.begin(); itr != _player->m_Pets.end(); ++itr)
	{
		MSG_S2C::stPets_List_Stabled::stPet pet;
		pet.pet_no = uint32(itr->first); // pet no
		pet.entry_id = uint32(itr->second->entry); // entryid
		pet.level = uint32(itr->second->level); // level
		pet.name = itr->second->name;		  // name
		pet.loyaltylvl = uint32(itr->second->loyaltylvl);
		if(itr->second->stablestate == STABLE_STATE_ACTIVE)
			pet.state = uint8(STABLE_STATE_ACTIVE);
		else
		{
			pet.state = uint8(STABLE_STATE_PASSIVE + i);
			i++;
		}
		Msg.vPets.push_back( pet );
	}

	SendPacket(Msg);
}

void WorldSession::HandleBuyStableSlot(CPacketUsn& packet)
{
	if(!_player->IsInWorld() || _player->GetStableSlotCount() == 2) return;
	uint8 scount = _player->GetStableSlotCount();
	BankSlotPrice* bsp = dbcStableSlotPrices.LookupEntry(scount+1);
	int32 cost = (bsp != NULL) ? bsp->Price : 99999999;
	if(cost > (int32)_player->GetUInt32Value(PLAYER_FIELD_COINAGE))
		return;

	_player->ModCoin(-cost);
	
	MSG_S2C::stPet_Stable_Result Msg;
	Msg.result = uint8(0xA);  // success
	SendPacket(Msg);
	if(_player->GetStableSlotCount() > 2)
		_player->m_StableSlotCount = 2;
	else
		_player->m_StableSlotCount++;
#ifdef OPTIMIZED_PLAYER_SAVING
	_player->save_Misc();
#endif
}


void WorldSession::HandlePetSetActionOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stPet_Set_Action MsgRecv;packet>>MsgRecv;
	uint32 unk1 = MsgRecv.unk1;
	uint32 unk2 = MsgRecv.unk2;
	uint32 slot = MsgRecv.slot;
	uint16 spell = MsgRecv.spell;;
	uint16 state = MsgRecv.state;
	if(!_player->GetSummon())
		return;

	Pet * pet = _player->GetSummon();
	SpellEntry * spe = dbcSpell.LookupEntry( spell );
	if( spe == NULL )
		return;

	// do we have the spell? if not don't set it (exploit fix)
	PetSpellMap::iterator itr = pet->GetSpells()->find( spe );
	if( itr == pet->GetSpells()->end( ) )
		return;

	pet->ActionBar[slot] = spell;
	pet->SetSpellState(spell, state);
}

void WorldSession::HandlePetRename(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stPet_Rename MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;
	string name = MsgRecv.name;

	if(!_player->GetSummon() || _player->GetSummon()->GetGUID() != guid)
	{
		sChatHandler.SystemMessage(this, "That pet is not your current pet, or you do not have a pet.");
		return;
	}

	Pet * pet = _player->GetSummon();
	pet->Rename(name);
	MSG_S2C::stPet_Rename_Ack Msg;
	Msg.guid = guid;
	Msg.name = name;
	_player->SendMessageToSet(Msg, true);
	// Disable pet rename.
	pet->SetUInt32Value(UNIT_FIELD_BYTES_2, 1 | (0x28 << 8) | (0x2 << 16));
}

void WorldSession::HandlePetAbandon(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	Pet * pet = _player->GetSummon();
	if(!pet) return;

	pet->Dismiss(false);
}
void WorldSession::HandlePetUnlearn(CPacketUsn& packet)
{
	if( !_player->IsInWorld() )
		return;

	MSG_C2S::stPet_Unlearn MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;

	Pet* pPet = _player->GetSummon();
	if( pPet == NULL || pPet->GetGUID() != guid )
	{
		sChatHandler.SystemMessage(this, "That pet is not your current pet, or you do not have a pet.");
		return;
	}

	int32 cost = pPet->GetUntrainCost();
	if( cost > ( int32 )_player->GetUInt32Value( PLAYER_FIELD_COINAGE ) )
	{
		MSG_S2C::stItem_Buy_Failure Msg;
		Msg.vendorguid = _player->GetGUID();
		Msg.itementry = 0;
		Msg.error = 2;
		SendPacket( Msg );
		return;	
	}
	_player->ModCoin(-cost );
	pPet->WipeSpells();
}
