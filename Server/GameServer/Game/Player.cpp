#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Player.h"
#include "../../SDBase/Protocol/S2C_Spell.h"
#include "../../SDBase/Protocol/S2C_Move.h"
#include "../../SDBase/Protocol/S2C_Loot.h"
#include "../../SDBase/Protocol/S2C_Pet.h"
#include "../../SDBase/Protocol/S2C_Trade.h"
#include "../../SDBase/Protocol/S2C_Duel.h"
#include "../../SDBase/Protocol/S2C_Guild.h"
#include "../../SDBase/Protocol/S2C_Group.h"
#include "../../SDBase/Protocol/S2C_AI.h"
#include "../../SDBase/Protocol/S2C_Chat.h"

#include "../Common/Protocol/GS2GT.h"
#include "../Common/Protocol/GS2DB.h"

#include "SunyouInstance.h"
#include "InstanceQueue.h"
#include "JingLianItem.h"
#include "SunyouCastle.h"
#include "SunyouBattleGroundBase.h"
#include "MultiLanguageStringMgr.h"
#include "WeatherMgr.h"
#include "TaxiMgr.h"
#include "QuestMgr.h"

UpdateMask Player::m_visibleUpdateMask;
#define COLLISION_MOUNT_CHECK_INTERVAL 1000

#define SUNYOU_FEE
//#define SUNYOU_FEE_DEBUG

Player::Player( uint32 guid )
{
	PctRefineChanceModifier = 0.f;
	m_isReturn2Mother = false;
	mother_groupid = 0;
	m_mapId = 0;
	m_instanceId = 0;
	m_EnableMeleeAttack = true;
	m_spellDamageModifier = 0;
	m_beforeTeleport2Instance = false;
	m_lock_sunyou_instance_join = false;
	MaxPowerModPctPos = 0;
	MaxPowerModPctNeg = 0;

	MaxHealthModPctPos = 0;
	MaxHealthModPctNeg = 0;


	m_before_delete = false;
	m_sunyou_instance = NULL;
	m_team_arena_idx = 0;
	m_resurrect_lock = false;
	m_animalgroupspell = 0;
	m_is_add_extra_spells = false;
	m_escape_mode = false;
	m_comatbat_xp_time = 0;
	m_bTriggerXP = false;
	m_entercombat_time = 0;

	m_bEXPIdle = false;
	m_EXPIdleTime = 0;

	m_ExtraEXPRatio = 100;
	m_ExtraEXPRatioQuest = 100;
	m_ExtraHonorRatio = 100;
	m_ExtraHonorRatioQuest = 100;

	m_isForbidTrade = false;
	m_playtimeTriggerId = 0;
	m_playtimeTriggerTime = UNIXTIME;
	m_playtimeTriggerDiff = 0;
	m_playtimeCounted = 0;
	memset( m_queuedmapid, 0, sizeof( m_queuedmapid ) );
	m_objectTypeId = TYPEID_PLAYER;
	m_valuesCount = PLAYER_END;
	m_uint32Values = _fields;

	memset(m_uint32Values, 0,(PLAYER_END)*sizeof(uint32));
	m_updateMask.SetCount(PLAYER_END);
	SetUInt32Value( OBJECT_FIELD_TYPE,TYPE_PLAYER|TYPE_UNIT|TYPE_OBJECT);
	SetUInt32Value( OBJECT_FIELD_GUID,guid);

	m_mailBox = new Mailbox(guid);
	m_finishingmovesdodge = false;
	iActivePet			  = 0;
	resurrector			 = 0;
	SpellCrtiticalStrikeRatingBonus=0;
	SpellHasteRatingBonus   = 0;
	m_lifetapbonus		  = 0;
	info					= NULL;				 // Playercreate info
	SoulStone			   = 0;
	SoulStoneReceiver		= 0;
	bReincarnation			= false;
	m_furorChance			= 0;
	Seal					= 0;
	judgespell			  = 0;
	m_session			   = 0;
	TrackingSpell		   = 0;
	m_status				= 0;
	offhand_dmg_mod		 = 0.5;
	m_walkSpeed			= 2.5f;
	m_runSpeed			  = PLAYER_NORMAL_RUN_SPEED;
	m_isMoving			  = false;
	m_bIsJumping		  = false;
	m_ShapeShifted		  = 0;
	m_curTarget			 = 0;
	m_curSelection		  = 0;
	m_curSwingTarget      = 0;
	m_lootGuid			  = 0;
	m_Summon				= NULL;

	m_PetNumberMax		  = 4;
	m_lastShotTime		  = 0;

	m_H_regenTimer			= 0;
	m_P_regenTimer			= 0;
	m_onTaxi				= false;

	m_taxi_pos_x			= 0;
	m_taxi_pos_y			= 0;
	m_taxi_pos_z			= 0;
	m_taxi_ride_time		= 0;

	// Attack related variables
	m_blockfromspellPCT	 = 0;
	m_blockfromspell		= 0;
	m_critfromspell		 = 0;
	m_spellcritfromspell	= 0;
	m_dodgefromspell		= 0;
	m_parryfromspell		= 0;
	m_hitfromspell		  = 0;
	m_hitfpctfromspell    = 0;
	m_hitfrommeleespell	 = 0;
	m_meleeattackspeedmod   = 0;
	m_rangedattackspeedmod  = 0;

	m_healthfromspell	   = 0;
	m_manafromspell		 = 0;
	m_healthfromitems	   = 0;
	m_manafromitems		 = 0;

	m_talentresettimes	  = 0;
	m_nLastProcessMsgBufferTime = 0;

	m_nextSave			  = (uint32)UNIXTIME + 3 * TIME_MINUTE;

	m_currentSpell		  = NULL;
	m_resurrectHealth	   = m_resurrectMana = 0;
	m_bReliveItem = false;

	m_itemupdate = 0;

	m_GroupInviter		  = 0;

	Lfgcomment = "";

	for(int i=0;i<3;i++)
	{
		LfgType[i]=0;
		LfgDungeonId[i]=0;
	}

	m_Autojoin = false;
	m_AutoAddMem = false;
	LfmDungeonId=0;
	LfmType=0;

	m_invitersGuid		  = 0;

	m_currentMovement	   = MOVE_UNROOT;
	m_isGmInvisible		 = false;

	//DK
	m_invitersGuid		  = 0;

	m_addgold            = 0;
	m_additemchance      = 0;

	//Trade
	ResetTradeVariables();
	mTradeTarget = 0;

	//Duel
	DuelingWith			 = NULL;
	m_duelCountdownTimer	= 0;
	m_duelStatus			= 0;
	m_duelState			 = DUEL_STATE_FINISHED;		// finished

	//WayPoint
	waypointunit			= NULL;

	//PVP
	//PvPTimeoutEnabled	   = false;

	//Tutorials
	for ( int aX = 0 ; aX < 8 ; aX++ )
		m_Tutorials[ aX ] = 0x00;

	m_lootGuid			  = 0;
	//m_banned				= false;

	//Bind possition
	m_bind_pos_x			= 0;
	m_bind_pos_y			= 0;
	m_bind_pos_z			= 0;
	m_bind_mapid			= 0;
	m_bind_zoneid		   = 0;

	m_bgEntryPointX = 0;
	m_bgEntryPointY = 0;
	m_bgEntryPointZ = 0;
	m_bgEntryPointO = 0;
	m_bgEntryPointMap = 0;

	// Rest
	m_timeLogoff			= 0;
	m_isResting			 = 0;
	m_restState			 = 0;
	m_restAmount			= 0;
	m_afk_reason			= "";
	m_playedtime[0]		 = 0;
	m_playedtime[1]		 = 0;
	m_playedtime[2]		 = (uint32)UNIXTIME;

	m_AllowAreaTriggerPort  = true;

	m_bgEntryPointInstance  = 0;

	// gm stuff
	//m_invincible			= false;
	bGMTagOn				= false;
	CooldownCheat		   = false;
	CastTimeCheat		   = false;
	PowerCheat			  = false;
	GodModeCheat			= false;
	FlyCheat				= false;

	//FIX for professions
	weapon_proficiency	  = 0x4000;//2^14
	//FIX for shit like shirt etc
	armor_proficiency	   = 1;

	m_bUnlimitedBreath	  = false;
	m_UnderwaterState	   = 0;
	m_UnderwaterTime		= 60000;
	m_UnderwaterMaxTime	 = 60000;
	m_UnderwaterLastDmg	 = getMSTime();
	m_SwimmingTime		  = 0;
	m_BreathDamageTimer	 = 0;

	//transport shit
	m_TransporterGUID		= 0;
	m_TransporterX			= 0.0f;
	m_TransporterY			= 0.0f;
	m_TransporterZ			= 0.0f;
	m_TransporterO			= 0.0f;
	m_TransporterUnk		= 0.0f;
	m_lockTransportVariables= false;

	// Autoshot variables
	m_AutoShotTarget		= 0;
	m_onAutoShot			= false;
	m_AutoShotDuration		= 0;
	m_AutoShotAttackTimer	= 0;
	m_AutoShotSpell			= NULL;

	m_AttackMsgTimer		= 0;
	m_incombattimeleft		= -1;
	m_inarmedtimeleft		= -1;

	timed_quest_slot		= 0;
	m_GM_SelectedGO			= NULL;

	for(uint32 x = 0;x < 7; x++)
	{
		FlatResistanceModifierPos[x] = 0;
		FlatResistanceModifierNeg[x] = 0;
		BaseResistanceModPctPos[x] = 0;
		BaseResistanceModPctNeg[x] = 0;
		BaseSpellResistanceModPctPos[x] = 0;
		BaseSpellResistanceModPctNeg[x] = 0;
		ResistanceModPctPos[x] = 0;
		ResistanceModPctNeg[x] = 0;
		SpellDelayResist[x] = 0;
		m_casted_amount[x] = 0;
	}

	for(uint32 a = 0; a < 6; a++)
		for(uint32 x = 0; x < 7; x++)
		{
			SpellDmgDoneByAttribute[a][x] = 0;
			SpellHealDoneByAttribute[a][x] = 0;
		}

	for(uint32 x = 0; x < 6; x++)
	{
		FlatStatModPos[x] = 0;
		FlatStatModNeg[x] = 0;
		StatModPctPos[x] = 0;
		StatModPctNeg[x] = 0;
		TotalStatModPctPos[x] = 0;
		TotalStatModPctNeg[x] = 0;
	}


	for(uint32 x = 0; x < 12; x++)
	{
		IncreaseDamageByType[x] = 0;
		IncreaseDamageByTypePCT[x] = 0;
		IncreaseCricticalByTypePCT[x] = 0;
	}

	PctIgnoreRegenModifier  = 0.0f;
	m_retainedrage          = 0;
	DetectedRange		   = 0;

	m_targetIcon			= 0;
	bShouldHaveLootableOnCorpse = false;
	m_MountSpellId		  = 0;
	bHasBindDialogOpen	  = false;
	m_CurrentCharm		  = NULL;
	m_CurrentTransporter	= NULL;
	m_SummonedObject		= NULL;
	m_currentLoot		   = (uint64)NULL;
	roll					= 0;
	m_ObjectUpdateMsg.buffer.reserve( 65000 );
	bProcessPending		 = false;
	for(int i = 0; i < QUEST_MAX_COUNT; ++i)
		m_questlog[i] = NULL;

	m_ItemInterface		 = new ItemInterface(this);
	m_JingLianItemInterface = new JingLianItemInterface;
	CurrentGossipMenu	   = NULL;

	ResetHeartbeatCoords();
	cannibalize			 = false;

	m_AreaID				= 0;
	m_actionsDirty		  = false;
	cannibalizeCount		= 0;
	rageFromDamageDealt	 = 0;

	m_honorToday			= 0;
	m_honorYesterday		= 0;
	m_honorPoints		   = 0;
	m_killsToday			= 0;
	m_killsYesterday		= 0;
	m_killsLifetime		 = 0;
	m_honorless			 = false;
	m_lastSeenWeather	   = 0;
	m_attacking			 = false;

	myCorpse				= 0;
	bCorpseCreateable	   = true;
	blinked				 = false;
	m_speedhackChances	  = 3;
	m_explorationTimer	  = getMSTime();
	linkTarget			  = 0;
	stack_cheat			 = false;
	triggerpass_cheat = false;
	m_pvpTimer			  = 0;
	m_globalCooldown = 0;
	m_lastHonorResetTime	= 0;
	memset(&mActions, 0, PLAYER_ACTION_BUTTON_SIZE);
	tutorialsDirty = true;
	m_TeleportState = 1;
	m_beingPushed = false;
	for(int i = 0; i < NUM_CHARTER_TYPES; ++i)
		m_charters[i]=NULL;
	for(int i = 0; i < NUM_ARENA_TEAM_TYPES; ++i)
		m_arenaTeams[i]=NULL;

		flying_aura = 0;
	resend_speed = false;
	rename_pending = false;
	iInstanceType		   = 0;

	SetFloatValue(UNIT_FIELD_ATTACK_POWER_MULTIPLIER, 0.0f);
	SetFloatValue(UNIT_FIELD_RANGED_ATTACK_POWER_MULTIPLIER, 0.0f);

	UpdateLastSpeeds();

	m_resist_critical[0]=m_resist_critical[1]=0;
	for (uint32 x =0;x<3;x++)
	{
		m_resist_hit[x]=0;
	}
	ok_to_remove = false;
	trigger_on_stun = 0;
	trigger_on_stun_chance = 100;
	m_modphyscritdmgPCT = 0;
	m_RootedCritChanceBonus = 0;

	m_ModInterrMRegenPCT = 0;
	m_ModInterrMRegen =0;
	m_RegenManaOnSpellResist=0;
	m_rap_mod_pct = 0;
	m_modblockabsorbvalue = 0;
	m_modblockvaluefromspells = 0;
	m_summoner = m_summonInstanceId = m_summonMapId = 0;
	m_lastMoveType = 0;
	m_tempSummon = 0;
	m_deathVision = false;
	last_heal_spell = NULL;
	m_playerInfo = NULL;
	m_sentTeleportPosition.ChangeCoords(999999.0f,999999.0f,999999.0f);
	m_speedChangeCounter=1;
	memset(&m_bgScore,0,sizeof(BGScore));
	m_arenaPoints = 0;
	memset(&m_spellIndexTypeTargets, 0, sizeof(uint64)*NUM_SPELL_TYPE_INDEX);
	m_base_runSpeed = m_runSpeed;
	m_base_walkSpeed = m_walkSpeed;
	m_arenateaminviteguid=0;
	m_arenaPoints=0;
	m_honorRolloverTime=0;
	hearth_of_wild_pct = 0;
	raidgrouponlysent=false;
	loot.gold=0;
	m_waterwalk=false;
	m_setwaterwalk=false;
	m_areaSpiritHealer_guid=0;
	m_CurrentTaxiPath=NULL;
	m_setflycheat = false;
	m_fallDisabledUntil = 0;
	m_lfgMatch = NULL;
	m_lfgInviterGuid = 0;
	m_mountCheckTimer = 0;
	m_taxiMapChangeNode = 0;
	_startMoveTime = 0;

#ifdef ENABLE_COMPRESSED_MOVEMENT
	m_movementBuffer.reserve(5000);
#endif

	_heartbeatDisable = 0;
	m_safeFall = 0;
	m_noFallDamage = false;
	z_axisposition = 0.0f;
	m_KickDelay = 0;
	m_speedhackCheckTimer = 0;
	_speedChangeInProgress = false;
	m_passOnLoot = false;
	m_changingMaps = true;
	m_outStealthDamageBonusPct = m_outStealthDamageBonusPeriod = m_outStealthDamageBonusTimer = 0;
	m_vampiricEmbrace = m_vampiricTouch = 0;
#ifdef COLLISION
	m_flyhackCheckTimer = 0;
#endif

	m_points = 0;
	m_expire = 0;
	m_timeLastCheckFee = 0;

#ifdef SUNYOU_FEE
	m_expireflag = false;
#else
	m_expireflag = true;
#endif

	m_bRecvLoadingOK = false;

	memset( m_extra_spells, 0, sizeof( m_extra_spells ) );
	memset( m_extra_aruas, 0, sizeof( m_extra_aruas ) );
	m_leave_guild_date = 0;
	m_temp_teacher_id = 0;
	m_last_recruit_time = 0;
	m_castle_debuff_spell_id = 0;
	leap_x = 0.f;
	leap_y = 0.f;
	leap_z = 0.f;
	leap_o = 0.f;

	m_HealthBeforeEnterInstance = 0;
	m_ManaBeforeEnterInstance = 0;

	SetUInt32Value( OBJECT_FIELD_UNIQUE_ID, this->GetUniqueIDForLua() );
}

void Player::OnExpIdleTime()
{
	StorageContainerIterator<Idle_Gift> * itr = IdleGiftStorage.MakeIterator();
	while(!itr->AtEnd())
	{
		Idle_Gift* pg = itr->Get();
		if(pg->level == GetUInt32Value(UNIT_FIELD_LEVEL))
		{
			GiveGift(pg->type, pg->miscvalue1, pg->miscvalue2);
		}
		if(!itr->Inc())
			break;
	}
	itr->Destruct();
}

void Player::GiveItem(uint32 itemid, ItemPrototype* it, uint32 cnt)
{
	// Find free slot and break if inv full
	SlotResult slotresult;
	AddItemResult result;
	Item* add = GetItemInterface()->FindItemLessMax(itemid,cnt, false);
	if (!add)
	{
		slotresult = GetItemInterface()->FindFreeInventorySlot(it);
	}
	if ((!slotresult.Result) && (!add))
	{
		//Our User doesn't have a free Slot in there bag
		// mail to player
		if( !g_crosssrvmgr->m_isInstanceSrv )
		{
			MailMessage msg;
			msg.subject = build_language_string( BuildString_PlayerMailSubjectSystemGift );//"系统礼物";
			msg.body = "";
			msg.stationary = 0;
			msg.delivery_time = (uint32)UNIXTIME;
			msg.cod = 0;
			msg.money = 0;
			Item *itm = objmgr.CreateItem(itemid, this);
			if(!itm)
			{
				GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_DONT_OWN_THAT_ITEM);
				return;
			}

			itm->m_isDirty=true;
			itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, cnt);

			itm->RemoveFromWorld();
			itm->SetOwner(NULL);
			itm->SaveToDB( INVENTORY_SLOT_NOT_SET, 0, true, NULL );
			msg.items.push_back( itm->GetUInt32Value(OBJECT_FIELD_GUID) );
			delete itm;

			msg.player_guid = GetGUID();
			msg.sender_guid = GetGUID();
			// 30 day expiry time for unread mail mail
			if(!sMailSystem.MailOption(MAIL_FLAG_NO_EXPIRY))
				msg.expire_time = (uint32)UNIXTIME + (TIME_DAY * 30);
			else
				msg.expire_time = 0;

			msg.copy_made = false;
			msg.read_flag = false;
			msg.deleted_flag = false;
			msg.message_type = 0;

			// Great, all our info is filled in. Now we can add it to the other players mailbox.
			sMailSystem.DeliverMessage(GetGUID(), &msg);
		}
		else
		{
			std::string subject( build_language_string( BuildString_PlayerMailSubjectSystemGift ) );
			sMailSystem.SendMail2InsertQueue( GetGUID(), GetGUID(), subject, "", 0, itemid, cnt, 0 );
		}
		// m_session->SystemMessage("恭喜您，得到了系统赠送的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱");
		m_session->SystemMessage( build_language_string( BuildString_PlayerMailBodySystemGift ) );
		MSG_S2C::stSystem_Gift_Notify Msg;
		Msg.ntype = 1;
		Msg.miscvalue1 = itemid;
		Msg.miscvalue2 = cnt;
		m_session->SendPacket(Msg);
		return;
	}
	if(!add)
	{
		Item *itm = objmgr.CreateItem(itemid, this);
		if(!itm)
		{
			GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_DONT_OWN_THAT_ITEM);
			return;
		}

		itm->m_isDirty=true;
		itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, cnt);

		if(slotresult.ContainerSlot == ITEM_NO_SLOT_AVAILABLE)
		{
			result = GetItemInterface()->SafeAddItem(itm, INVENTORY_SLOT_NOT_SET, slotresult.Slot);
			if(!result)
			{
				delete itm;
			}
			else
				m_session->SendItemPushResult(itm, false, true, false, true, INVENTORY_SLOT_NOT_SET, slotresult.Result, cnt);
		}
		else
		{
			if(Item *bag = GetItemInterface()->GetInventoryItem(slotresult.ContainerSlot))
			{
				if( !((Container*)bag)->AddItem(slotresult.Slot, itm) )
					delete itm;
				else
					m_session->SendItemPushResult(itm, false, true, false, true, slotresult.ContainerSlot, slotresult.Result, cnt);
			}
		}
	}
	else
	{
		add->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, cnt);
		add->m_isDirty = true;
		sQuestMgr.OnPlayerItemPickup( this, add );
		m_session->SendItemPushResult(add, false, true, false, false, GetItemInterface()->GetBagSlotByGuid(add->GetGUID()), 1, cnt);
	}
}

bool Player::GiveItemAndRemoveFromGround( Item* itm )
{
	// Find free slot and break if inv full
	SlotResult slotresult;
	AddItemResult result;
	uint32 cnt = itm->GetUInt32Value( ITEM_FIELD_STACK_COUNT );
	Item* add = GetItemInterface()->FindItemLessMax( itm->GetEntry(), cnt, false);
	if (!add)
	{
		slotresult = GetItemInterface()->FindFreeInventorySlot(itm->GetProto());
	}
	if ((!slotresult.Result) && (!add))
	{
		return false;
	}
	if(!add)
	{
		itm->m_isDirty=true;
		if(slotresult.ContainerSlot == ITEM_NO_SLOT_AVAILABLE)
		{
			result = GetItemInterface()->SafeAddItem(itm, INVENTORY_SLOT_NOT_SET, slotresult.Slot);
			if(!result)
			{
				return false;
			}
			else
				m_session->SendItemPushResult(itm, false, true, false, true, INVENTORY_SLOT_NOT_SET, slotresult.Result, cnt);
		}
		else
		{
			if(Item *bag = GetItemInterface()->GetInventoryItem(slotresult.ContainerSlot))
			{
				if( !((Container*)bag)->AddItem(slotresult.Slot, itm) )
				{
					return false;
				}
				else
					m_session->SendItemPushResult(itm, false, true, false, true, slotresult.ContainerSlot, slotresult.Result, cnt);
			}
		}

		itm->SetOwner( NULL );
		itm->RemoveFromWorld();
		itm->SetOwner( this );
		itm->PushToWorld( m_mapMgr );
		static ByteBuffer buf(2500);
		buf.clear();
		uint32 count = itm->BuildCreateUpdateBlockForPlayer( &buf, this );
		PushCreationData(&buf, count);
	}
	else
	{
		add->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, cnt);
		add->m_isDirty = true;
		sQuestMgr.OnPlayerItemPickup( this, add );
		m_session->SendItemPushResult(add, false, true, false, false, GetItemInterface()->GetBagSlotByGuid(add->GetGUID()), 1, cnt);

		itm->RemoveFromWorld();
		delete itm;
	}
	return true;
}

void Player::GiveGift(uint32 Type, uint32 miscvalue1, uint32 miscvalue2, bool bSendMsg, int category)
{
	if(Type == 3)
	{
		uint32 xp = 0;
		if( category == GIFT_CATEGORY_INSTANCE || category == GIFT_CATEGORY_BATTLE_GROUND )
			xp = GiveXP(miscvalue1, 0, false, GIVE_EXP_TYPE_DYNAMIC_INSTANCE);
		else
			xp = GiveXP(miscvalue1, 0, false, GIVE_EXP_TYPE_GIFT);
		if( xp == 0 )
			return;

		if(bSendMsg)
		{
			m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageExpGift, quick_itoa(xp) ) );//"恭喜您，得到了系统赠送的经验%u点", xp);
		}
		MSG_S2C::stSystem_Gift_Notify Msg;
		Msg.ntype = Type;
		Msg.miscvalue1 = xp;
		m_session->SendPacket(Msg);
	}
	else if(Type == 2)
	{
		ModCoin((int32)miscvalue1 );
		MyLog::yunyinglog->info("gold-sysgift-player[%u][%s] take gold[%u]", GetLowGUID(), GetName(), miscvalue1);
		if(bSendMsg)
		{
			m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageGoldGift, quick_gold_s(miscvalue1) ) );//"恭喜您，得到了系统赠送的金币");
		}
		MSG_S2C::stSystem_Gift_Notify Msg;
		Msg.ntype = Type;
		Msg.miscvalue1 = miscvalue1;
		m_session->SendPacket(Msg);
		return;
	}
	else if(Type == 1)
	{
		ItemPrototype *it = ItemPrototypeStorage.LookupEntry(miscvalue1);
		if(!it)
		{
			MyLog::log->error("OnPlaytimeTrigger not found itemid[%d]", miscvalue1);
			return;
		}
		// Find free slot and break if inv full
		SlotResult slotresult;
		AddItemResult result;
		Item* add = GetItemInterface()->FindItemLessMax(miscvalue1,miscvalue2, false);
		if (!add)
		{
			slotresult = GetItemInterface()->FindFreeInventorySlot(it);
		}
		if ((!slotresult.Result) && (!add))
		{
			//Our User doesn't have a free Slot in there bag

			// mail to player
			std::string subject;
			switch( category )
			{
			case GIFT_CATEGORY_NORMAL:
				subject = build_language_string( BuildString_PlayerMailSubjectSystemGift );//"系统赠送道具";
				break;
			case GIFT_CATEGORY_BATTLE_GROUND:
				subject = build_language_string( BuildString_PlayerMailSubjectBGGift );//"战场奖励道具";
				break;
			case GIFT_CATEGORY_TEAM_ARENA:
				subject = build_language_string( BuildString_PlayerMailSubjectArenaGift );//"竞技场奖励道具";
				break;
			case GIFT_CATEGORY_TITLE:
				subject = build_language_string( BuildString_PlayerMailSubjectTitleGift );//"称号奖励道具";
				break;
			case GIFT_CATEGORY_INSTANCE:
				subject = build_language_string( BuildString_PlayerMailSubjectInstanceGift );//"副本奖励道具";
				break;
			case GIFT_CATEGORY_GAMBLE:
				subject = build_language_string( BuildString_PlayerMailSubjectGambleGift );//"合成道具";
				break;
			case GIFT_CATEGORY_GUILD_GIFT:
				subject = build_language_string( BuildString_PlayerMailSubjectGuildGift );//"部族奖励";
				break;
			}
			
			if( !g_crosssrvmgr->m_isInstanceSrv )
			{
				MailMessage msg;
				msg.subject = subject;
				msg.body = "";
				msg.stationary = 0;
				msg.delivery_time = (uint32)UNIXTIME;
				msg.cod = 0;
				msg.money = 0;
				Item *itm = objmgr.CreateItem(miscvalue1, this);
				if(!itm)
				{
					GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_DONT_OWN_THAT_ITEM);
					return;
				}

				itm->m_isDirty=true;
				itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, miscvalue2);

				itm->RemoveFromWorld();
				itm->SetOwner(NULL);
				itm->SaveToDB( INVENTORY_SLOT_NOT_SET, 0, true, NULL );
				msg.items.push_back( itm->GetUInt32Value(OBJECT_FIELD_GUID) );


				msg.player_guid = GetGUID();
				msg.sender_guid = GetGUID();
				// 30 day expiry time for unread mail mail
				if(!sMailSystem.MailOption(MAIL_FLAG_NO_EXPIRY))
					msg.expire_time = (uint32)UNIXTIME + (TIME_DAY * 30);
				else
					msg.expire_time = 0;

				msg.copy_made = false;
				msg.read_flag = false;
				msg.deleted_flag = false;
				msg.message_type = 0;

				// Great, all our info is filled in. Now we can add it to the other players mailbox.
				sMailSystem.DeliverMessage(GetGUID(), &msg);
				MyLog::yunyinglog->info("item-sysgift-player[%u][%s] sys senditem["I64FMT"][%u] count[%u]", this->GetLowGUID(), this->GetName(), itm->GetGUID(), itm->GetProto()->ItemId, miscvalue2);
				delete itm;
			}
			else
			{
				sMailSystem.SendMail2InsertQueue( GetGUID(), GetGUID(), subject, "", 0, miscvalue1, miscvalue2, 0 );
			}
			

			
			if(bSendMsg)
			{
				switch( category )
				{
				case GIFT_CATEGORY_NORMAL:
					m_session->SystemMessage( build_language_string( BuildString_PlayerMailBodySystemGift ) );//"恭喜您，得到了系统赠送的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱");
					break;
				case GIFT_CATEGORY_BATTLE_GROUND:
					m_session->SystemMessage( build_language_string( BuildString_PlayerMailBodyBGGift ) );//"恭喜您，得到了战场奖励的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱");
					break;
				case GIFT_CATEGORY_TEAM_ARENA:
					m_session->SystemMessage( build_language_string( BuildString_PlayerMailBodyArenaGift ) );//"恭喜您，得到了竞技场奖励的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱");
					break;
				case GIFT_CATEGORY_TITLE:
					m_session->SystemMessage( build_language_string( BuildString_PlayerMailBodyTitleGift ) );//"恭喜您，得到了称号奖励的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱");
					break;
				case GIFT_CATEGORY_INSTANCE:
					m_session->SystemMessage( build_language_string( BuildString_PlayerMailBodyInstanceGift ) );//"恭喜您，得到了副本奖励的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱");
					break;
				case GIFT_CATEGORY_GAMBLE:
					m_session->SystemMessage( build_language_string( BuildString_PlayerMailBodyGambleGift, Item::BuildStringWithProto( it ) ) );//"您制造了:%s,由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱");
					break;
				case GIFT_CATEGORY_GUILD_GIFT:
					m_session->SystemMessage( build_language_string( BuildString_PlayerMailBodyGuildGift ) );//"恭喜您，得到了部族奖励的道具，由于您的背包已满，系统将通过邮件发送给您，请查收您的邮箱");
					break;
				}
			}
			MSG_S2C::stSystem_Gift_Notify Msg;
			Msg.ntype = Type;
			Msg.miscvalue1 = miscvalue1;
			Msg.miscvalue2 = miscvalue2;
			m_session->SendPacket(Msg);
			return;
		}
		if(!add)
		{
			Item *itm = objmgr.CreateItem(miscvalue1, this);
			if(!itm)
			{
				GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_DONT_OWN_THAT_ITEM);
				return;
			}

			itm->m_isDirty=true;
			itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, miscvalue2);

			if(slotresult.ContainerSlot == ITEM_NO_SLOT_AVAILABLE)
			{
				result = GetItemInterface()->SafeAddItem(itm, INVENTORY_SLOT_NOT_SET, slotresult.Slot);
				if(!result)
				{
					delete itm;
					return;
				}
				else
					m_session->SendItemPushResult(itm, false, true, false, true, INVENTORY_SLOT_NOT_SET, slotresult.Result, miscvalue2);
			}
			else
			{
				if(Item *bag = GetItemInterface()->GetInventoryItem(slotresult.ContainerSlot))
				{
					if( !((Container*)bag)->AddItem(slotresult.Slot, itm) )
					{
						delete itm;
						return;
					}
					else
						m_session->SendItemPushResult(itm, false, true, false, true, slotresult.ContainerSlot, slotresult.Result, miscvalue2);
				}
			}
			add = itm;
		}
		else
		{
			add->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, miscvalue2);
			add->m_isDirty = true;
			sQuestMgr.OnPlayerItemPickup( this, add );
			m_session->SendItemPushResult(add, false, true, false, false, GetItemInterface()->GetBagSlotByGuid(add->GetGUID()), 1, miscvalue2);
		}

		if(bSendMsg)
		{
			switch( category )
			{
			case GIFT_CATEGORY_NORMAL:
				m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageSystemGift ) );//"恭喜您，得到了系统赠送的道具，请查看您的背包");
				break;
			case GIFT_CATEGORY_BATTLE_GROUND:
				m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageBGGift ) );//"恭喜您，得到了战场奖励的道具，请查看您的背包");
				break;
			case GIFT_CATEGORY_TEAM_ARENA:
				m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageArenaGift ) );//"恭喜您，得到了竞技场奖励的道具，请查看您的背包");
				break;
			case GIFT_CATEGORY_TITLE:
				m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageTitleGift ) );//"恭喜您，得到了称号奖励的道具，请查看您的背包");
				break;
			case GIFT_CATEGORY_INSTANCE:
				m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageInstanceGift ) );//"恭喜您，得到了副本奖励的道具，请查看您的背包");
				break;
			case GIFT_CATEGORY_GAMBLE:
				//m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageGambleGift,  ) );//"你制造了:%s
				break;
			case GIFT_CATEGORY_GUILD_GIFT:
				m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageGuildGift ) );//"恭喜您，得到了部族奖励的道具，请查看您的背包");
				break;
			}
		}
		MSG_S2C::stSystem_Gift_Notify Msg;
		Msg.ntype = Type;
		Msg.miscvalue1 = miscvalue1;
		Msg.miscvalue2 = miscvalue2;
		m_session->SendPacket(Msg);

		if( add )
			sQuestMgr.OnPlayerItemPickup( this, add );
	}
}

void Player::OnPlaytimeTrigger(uint32 Type, uint32 miscvalue1, uint32 miscvalue2)
{
	m_playtimeCounted--;
	GiveGift(Type, miscvalue1, miscvalue2);
}

void Player::PlaytimeTriggerCheck()
{
	if(m_playtimeCounted)
		return;
	Playtime_Trigger* pt_found = NULL;
	StorageContainerIterator<Playtime_Trigger> * itr = PlaytimeTriggerStorage.MakeIterator();
	while(!itr->AtEnd())
	{
		Playtime_Trigger* pt = itr->Get();

		uint32 timediff = m_playedtime[1];
		if(m_playtimeTriggerDiff)
		{
			timediff = m_playedtime[1] - m_playtimeTriggerDiff;
		}
		else
		{
			if(m_playedtime[1] >= pt->play_time_min && m_playedtime[1] < pt->play_time_max)
			{
				pt_found = pt;
				m_playtimeTriggerDiff = m_playedtime[1] - pt->play_time_min;
				break;
			}
		}
		if(timediff >= pt->play_time_min && timediff < pt->play_time_max)
		{
			pt_found = pt;
			break;
		}

		if(!itr->Inc())
			break;
	}
	itr->Destruct();

	if(pt_found)
	{
		if(pt_found->id == m_playtimeTriggerId)
		{
			return;
		}

		if(pt_found->type1)
		{
			m_playtimeCounted++;
			m_playtimeTriggerId = pt_found->id;

			MSG_S2C::stSystem_Gift Msg;
			Msg.timecounter = pt_found->waittime;
			m_session->SendPacket(Msg);
			sEventMgr.AddEvent(this, &Player::OnPlaytimeTrigger, pt_found->type1, pt_found->miscvalue1_1, pt_found->miscvalue1_2, EVENT_PLAYER_EXPIRE, pt_found->waittime*1000, 1,0);
		}
		if(pt_found->type2)
		{
			m_playtimeCounted++;
			m_playtimeTriggerId = pt_found->id;

			MSG_S2C::stSystem_Gift Msg;
			Msg.timecounter = pt_found->waittime;
			m_session->SendPacket(Msg);
			sEventMgr.AddEvent(this, &Player::OnPlaytimeTrigger, pt_found->type2, pt_found->miscvalue2_1, pt_found->miscvalue2_2, EVENT_PLAYER_EXPIRE, pt_found->waittime*1000, 1,0);
		}
		if(pt_found->type3)
		{
			m_playtimeCounted++;
			m_playtimeTriggerId = pt_found->id;

			MSG_S2C::stSystem_Gift Msg;
			Msg.timecounter = pt_found->waittime;
			m_session->SendPacket(Msg);
			sEventMgr.AddEvent(this, &Player::OnPlaytimeTrigger, pt_found->type3, pt_found->miscvalue3_1, pt_found->miscvalue3_2, EVENT_PLAYER_EXPIRE, pt_found->waittime*1000, 1,0);
		}
	}
}

void Player::ApplyAllEquipments()
{
	Item * pItem;
	for(uint32 i = 0; i < EQUIPMENT_SLOT_END; i++)
	{
		pItem = GetItemInterface()->GetInventoryItem(i);
		if( pItem != NULL )
		{
			if(i < EQUIPMENT_SLOT_END && pItem->GetDurabilityMax() > 0 && pItem->GetDurability() > 0 )	  // only equipment slots get mods.
			{
				_ApplyItemMods(pItem, i, true, false, true);
				pItem->OnEquip();
			}
		}
	}

	SetUInt32Value( UNIT_FIELD_HEALTH, ( load_health > m_uint32Values[UNIT_FIELD_MAXHEALTH] ? m_uint32Values[UNIT_FIELD_MAXHEALTH] : load_health ));
	SetUInt32Value( UNIT_FIELD_POWER1, ( load_mana > m_uint32Values[UNIT_FIELD_MAXPOWER1] ? m_uint32Values[UNIT_FIELD_MAXPOWER1] : load_mana ));
}

void Player::QuickAddTitle( uint32 titleid )
{
	if( InsertTitle( titleid ) )
	{
		MSG_S2C::stTitleAdd msg;
		msg.title.title = titleid;
		m_session->SendPacket(msg);
	}
	else
		MyLog::log->error( "cannot found title id:%u", titleid );
}

void Player::CheckArenaTitles( bool only_check_item )
{
	if( !only_check_item )
	{
		if( !HasTitle( 68 ) )
		{
			uint32 totalwin = 0;
			for( int i = 0; i < 5; ++i )
				totalwin += m_playerInfo->m_arena_win[i];
			if( totalwin >= 50 )
			{
				QuickAddTitle( 68 );
			}
		}

		if( !HasTitle( 69 ) )
		{
			uint32 totalwin = 0;
			for( int i = 0; i < 5; ++i )
				totalwin += m_playerInfo->m_arena_win[i];
			if( totalwin >= 100 )
			{
				QuickAddTitle( 69 );
			}
		}

		if( !HasTitle( 70 ) )
		{
			uint32 totalwin = 0;
			for( int i = 0; i < 5; ++i )
				totalwin += m_playerInfo->m_arena_win[i];
			if( totalwin >= 200 )
			{
				QuickAddTitle( 70 );
			}
		}

		if( !HasTitle( 71 ) )
		{
			uint32 totalwin = 0;
			for( int i = 0; i < 5; ++i )
				totalwin += m_playerInfo->m_arena_win[i];
			if( totalwin >= 500 )
			{
				QuickAddTitle( 71 );
			}
		}

		if( !HasTitle( 72 ) )
			if( m_playerInfo->m_arena_win_lv80[1] >= 600 && ( float(m_playerInfo->m_arena_win_lv80[1]) / float(m_playerInfo->m_arena_win_lv80[1]+m_playerInfo->m_arena_lose_lv80[1]) ) >= 0.7f )
				QuickAddTitle( 72 );

		if( !HasTitle( 73 ) )
			if( m_playerInfo->m_arena_win_lv80[2] >= 600 && ( float(m_playerInfo->m_arena_win_lv80[2]) / float(m_playerInfo->m_arena_win_lv80[2]+m_playerInfo->m_arena_lose_lv80[2]) ) >= 0.6f )
				QuickAddTitle( 73 );

		if( !HasTitle( 74 ) )
		{
			uint32 totalwin = m_playerInfo->m_arena_win_lv80[3] + m_playerInfo->m_arena_win_lv80[4];
			uint32 totallose = m_playerInfo->m_arena_lose_lv80[3] + m_playerInfo->m_arena_lose_lv80[4];

			if( totalwin >= 600 && ( float(totalwin) / float(totalwin + totallose) ) >= 0.55f )
				QuickAddTitle( 74 );
		}
	}

	// snow ball
	if( !HasTitle( 89 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 58 );
		if( n >= 20 )
			QuickAddTitle( 89 );
	}
	// snow ball
	if( !HasTitle( 90 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 58 );
		if( n >= 100 )
			QuickAddTitle( 90 );
	}
	// snow ball
	if( !HasTitle( 91 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 58 );
		if( n >= 200 )
			QuickAddTitle( 91 );
	}

	//////////////////////////////////////////////////////////////////////////

	if( !HasTitle( 92 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 59 );
		if( n >= 10 )
			QuickAddTitle( 92 );
	}
	if( !HasTitle( 93 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 59 );
		if( n >= 50 )
			QuickAddTitle( 93 );
	}
	if( !HasTitle( 94 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 59 );
		if( n >= 100 )
			QuickAddTitle( 94 );
	}

	//////////////////////////////////////////////////////////////////////////
	
	if( !HasTitle( 95 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 73 );
		if( n >= 5 )
			QuickAddTitle( 95 );
	}
	if( !HasTitle( 96 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 73 );
		if( n >= 20 )
			QuickAddTitle( 96 );
	}
	if( !HasTitle( 97 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 73 );
		if( n >= 50 )
			QuickAddTitle( 97 );
	}
	if( !HasTitle( 98 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 73 );
		if( n >= 100 )
			QuickAddTitle( 98 );
	}
	if( !HasTitle( 99 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 73 );
		if( n >= 200 )
			QuickAddTitle( 99 );
	}
	//////////////////////////////////////////////////////////////////////////

	if( !HasTitle( 100 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 74 );
		if( n >= 10 )
			QuickAddTitle( 100 );
	}
	if( !HasTitle( 101 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 74 );
		if( n >= 50 )
			QuickAddTitle( 101 );
	}
	if( !HasTitle( 102 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 74 );
		if( n >= 100 )
			QuickAddTitle( 102 );
	}
	//////////////////////////////////////////////////////////////////////////

	if( !HasTitle( 103 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 75 );
		if( n >= 10 )
			QuickAddTitle( 103 );
	}
	if( !HasTitle( 104 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 75 );
		if( n >= 50 )
			QuickAddTitle( 104 );
	}
	if( !HasTitle( 105 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 75 );
		if( n >= 100 )
			QuickAddTitle( 105 );
	}
	//////////////////////////////////////////////////////////////////////////

	if( !HasTitle( 106 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 76 );
		if( n >= 10 )
			QuickAddTitle( 106 );
	}
	if( !HasTitle( 107 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 76 );
		if( n >= 50 )
			QuickAddTitle( 107 );
	}
	if( !HasTitle( 108 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 76 );
		if( n >= 100 )
			QuickAddTitle( 108 );
	}
	//////////////////////////////////////////////////////////////////////////
	if( !HasTitle( 109 ) && HasTitle( 108 ) && HasTitle( 105 ) && HasTitle( 102 ) && HasTitle( 99 ) && HasTitle( 91 ) )
		QuickAddTitle( 109 );
	//////////////////////////////////////////////////////////////////////////

	if( !HasTitle( 76 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 900001011 );
		if( n >= 500 )
			QuickAddTitle( 76 );
	}
	if( !HasTitle( 77 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 900001012 );
		if( n >= 1000 )
			QuickAddTitle( 77 );
	}

	if( !HasTitle( 78 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 900001013 );
		if( n >= 2000 )
			QuickAddTitle( 78 );
	}

	if( !HasTitle( 79 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 900001014 );
		if( n >= 1000 )
			QuickAddTitle( 79 );
	}

	if( !HasTitle( 80 ) )
	{
		uint32 n = m_playerInfo->GetItemObtainCount( 900001015 );
		if( n >= 2000 )
			QuickAddTitle( 80 );
	}

	if( !HasTitle( 81 ) )
	{
		for( int i = 0; i < 7; ++i )
		{
			uint32 n = m_playerInfo->GetItemObtainCount( 100210510 + i * 100000 );
			if( !n )
				goto end_item_set1_81;
		}
		goto success_item_set_81;
end_item_set1_81:
		for( int i = 0; i < 7; ++i )
		{
			uint32 n = m_playerInfo->GetItemObtainCount( 100210512 + i * 100000 );
			if( !n )
				goto end_item_set2_81;
		}
		goto success_item_set_81;
end_item_set2_81:
		for( int i = 0; i < 7; ++i )
		{
			uint32 n = m_playerInfo->GetItemObtainCount( 100210514 + i * 100000 );
			if( !n )
				goto end_item_set3_81;
		}
		goto success_item_set_81;
success_item_set_81:
		QuickAddTitle( 81 );
end_item_set3_81:
		;
	}

	if( !HasTitle( 82 ) )
	{
		for( int i = 0; i < 7; ++i )
		{
			uint32 n = m_playerInfo->GetItemObtainCount( 100210511 + i * 100000 );
			if( !n )
				goto end_item_set1_82;
		}
		goto success_item_set_82;
end_item_set1_82:
		for( int i = 0; i < 7; ++i )
		{
			uint32 n = m_playerInfo->GetItemObtainCount( 100210513 + i * 100000 );
			if( !n )
				goto end_item_set2_82;
		}
		goto success_item_set_82;
end_item_set2_82:
		for( int i = 0; i < 7; ++i )
		{
			uint32 n = m_playerInfo->GetItemObtainCount( 100210515 + i * 100000 );
			if( !n )
				goto end_item_set3_82;
		}
		goto success_item_set_82;
success_item_set_82:
		QuickAddTitle( 82 );
end_item_set3_82:
		;
	}


	if( !HasTitle( 83 ) )
	{
		int i = 0;
		for( ; i < 7; ++i )
		{
			uint32 n = m_playerInfo->GetItemObtainCount( 100210500 + i * 100000 );
			if( !n )
				break;
		}
		if( i == 7 )
			QuickAddTitle( 83 );
	}

	if( !HasTitle( 84 ) )
	{
		int i = 0;
		for( ; i < 7; ++i )
		{
			uint32 n = m_playerInfo->GetItemObtainCount( 100210502 + i * 100000 );
			if( !n )
				break;
		}
		if( i == 7 )
			QuickAddTitle( 84 );
	}

	if( !HasTitle( 85 ) )
	{
		int i = 0;
		for( ; i < 7; ++i )
		{
			uint32 n = m_playerInfo->GetItemObtainCount( 100210501 + i * 100000 );
			if( !n )
				break;
		}
		if( i == 7 )
			QuickAddTitle( 85 );
	}

	if( !HasTitle( 86 ) )
	{
		int i = 0;
		for( ; i < 7; ++i )
		{
			uint32 n = m_playerInfo->GetItemObtainCount( 100210503 + i * 100000 );
			if( !n )
				break;
		}
		if( i == 7 )
			QuickAddTitle( 86 );
	}
	if( !HasTitle( 87 ) && HasTitle( 72 ) && HasTitle( 73 ) && HasTitle( 74 ) && HasTitle( 75 )
		&& HasTitle( 77 ) && HasTitle( 78 ) && HasTitle( 79 ) && HasTitle( 80 ) )
	{
		QuickAddTitle( 87 );
	}
}

void Player::AddArenaResult( uint32 arena_level, bool win, uint32 vs_type )
{
	if( arena_level == 80 )
	{
		if( win )
			SetUInt32Value( PLAYER_FIELD_ARENA_LV80_1V1_WIN + vs_type, ++m_playerInfo->m_arena_win_lv80[vs_type] );
		else
			SetUInt32Value( PLAYER_FIELD_ARENA_LV80_1V1_LOSE + vs_type, ++m_playerInfo->m_arena_lose_lv80[vs_type] );
	}
	else
	{
		if( win )
			SetUInt32Value( PLAYER_FIELD_ARENA_1V1_WIN + vs_type, ++m_playerInfo->m_arena_win[vs_type] );
		else
			SetUInt32Value( PLAYER_FIELD_ARENA_1V1_LOSE + vs_type, ++m_playerInfo->m_arena_lose[vs_type] );
	}
}

void Player::OnLogin()
{
	//加入Playtime_Trigger
	//PlaytimeTriggerCheck();
}

Player::~Player ( )
{
	if( !g_crosssrvmgr->m_isInstanceSrv )
		g_instancequeuemgr.LeavePlayer( this );

	Group* g = GetGroup();
	if( g && g->GetLeader() == m_playerInfo )
	{
		uint32 groups = g->GetSubGroupCount();
		for(uint32 i = 0; i < groups; i++)
		{
			SubGroup * sg = g->GetSubGroup(i);
			if(!sg) continue;

			GroupMembersSet::iterator git = sg->GetGroupMembersBegin();

			for( ; git != sg->GetGroupMembersEnd(); ++git )
			{
				PlayerInfo* pi = *git;
				if( pi->m_loggedInPlayer && pi->m_loggedInPlayer != this )
					g_instancequeuemgr.LeavePlayer( pi->m_loggedInPlayer );
			}
		}
	}

	if(!ok_to_remove)
	{
		printf("Player deleted from non-logoutplayer!\n");
#ifdef WIN32
		CStackWalker sw;
		sw.ShowCallstack();
#endif
		objmgr.RemovePlayer(this);
	}

	if(m_session)
		m_session->SetPlayer(0);

	delete m_mailBox;
	Player * pTarget;
	if(mTradeTarget != 0)
	{
		pTarget = GetTradeTarget();
		if(pTarget)
			pTarget->mTradeTarget = 0;
	}

	pTarget = objmgr.GetPlayer(GetInviter());
	if(pTarget)
		pTarget->SetInviter(0);

	if(m_Summon)
		m_Summon->ClearPetOwner();

	mTradeTarget = 0;

	if(DuelingWith != 0)
		DuelingWith->DuelingWith = 0;
	DuelingWith = 0;

	CleanupGossipMenu();
	ASSERT(!IsInWorld());

	sEventMgr.RemoveEvents(this);

	// delete m_talenttree

	CleanupChannels();
	for(int i = 0; i < QUEST_MAX_COUNT; ++i)
	{
		if(m_questlog[i] != NULL)
		{
			delete m_questlog[i];
		}
	}

	for(SplineMap::iterator itr = _splineMap.begin(); itr != _splineMap.end(); ++itr)
		delete itr->second;

	if(m_ItemInterface)
		delete m_ItemInterface;

	if( m_JingLianItemInterface )
		delete m_JingLianItemInterface;

	m_objectTypeId = TYPEID_UNUSED;

	if(m_playerInfo)
		m_playerInfo->m_loggedInPlayer=NULL;

	if(m_Summon)
	{
		m_Summon->ClearPetOwner();
		m_Summon->Delete();
		m_Summon=NULL;
	}

	m_mapSpellGroupColdown.clear();


	//while( delayedPackets.size() )
	{
		//WorldPacket * pck = delayedPackets.next();
		//delete pck;
	}
}

SUNYOU_INLINE uint32 GetSpellForLanguage(uint32 SkillID)
{
	switch(SkillID)
	{
	case SKILL_LANG_COMMON:
		return 668;
		break;

	case SKILL_LANG_ORCISH:
		return 669;
		break;

	case SKILL_LANG_TAURAHE:
		return 670;
		break;

	case SKILL_LANG_DARNASSIAN:
		return 671;
		break;

	case SKILL_LANG_DWARVEN:
		return 672;
		break;

	case SKILL_LANG_THALASSIAN:
		return 813;
		break;

	case SKILL_LANG_DRACONIC:
		return 814;
		break;

	case SKILL_LANG_DEMON_TONGUE:
		return 815;
		break;

	case SKILL_LANG_TITAN:
		return 816;
		break;

	case SKILL_LANG_OLD_TONGUE:
		return 817;
		break;

	case SKILL_LANG_GNOMISH:
		return 7430;
		break;

	case SKILL_LANG_TROLL:
		return 7341;
		break;

	case SKILL_LANG_GUTTERSPEAK:
		return 17737;
		break;

	case SKILL_LANG_DRAENEI:
		return 29932;
		break;
	}

	return 0;
}

void Player::UpdatePointsAndExpireProc( QueryResultVector& v )
{
	if( !IsValid() ) return;
	if( v.size() == 0 )
	{
		m_points = 0;
		m_expire = 0;
		MyLog::log->error("UpdatePointsAndExpireProc v.size() == 0");
		//assert( 0 );
		return;
	}

	QueryResult *result = v[0].result;
	if(result)
	{
		m_points = result->Fetch()[0].GetInt32();
		m_expire = result->Fetch()[1].GetUInt32();
	}
}

void Player::SendLogoutInFewMinutesMsg()
{
	if( m_session )
	{
		if( m_expireMinutes == 0 )
		{
			AccountExpire();
			return;
		}

		MyLog::log->alert( "account expire in %d minutes, account = %u character name = %s", m_expireMinutes, m_session->GetAccountId(), m_playerInfo->name );

		MSG_S2C::stAccountExpireInFewMinutes msg;
		msg.howlong = m_expireMinutes--;
		m_session->SendPacket( msg );
	}
	else
	{
		MyLog::log->error("SendLogoutInFewMinutesMsg m_session == NULL");
		//assert( 0 );
	}
}

void Player::AccountExpire()
{
	if( m_session )
	{
		if( m_points > 1 )
		{
			m_expireflag = false;
			return;
		}
		MyLog::log->alert( "account expire, account = %u character name = %s", m_session->GetAccountId(), m_playerInfo->name );

		SoftDisconnect();
	}
	else
	{
		MyLog::log->error("AccountExpire m_session == NULL");
		//assert( 0 );
	}
}

void Player::Update( uint32 p_time )
{
	if( !HasFlag( PLAYER_FLAGS, PLAYER_FLAG_AFK ) )
	{
		if( m_lastShotTime > 0 && (uint32)UNIXTIME - m_lastShotTime > 5 * 60 )
		{
			SetFlag( PLAYER_FLAGS, PLAYER_FLAG_AFK );
			m_afk_reason = build_language_string( BuildString_ChatAfk );
			m_lastShotTime = 0;
		}
	}

	uint32 deltatime = UNIXTIME - GetSession()->m_lastPing;
	if(deltatime > 900)
	{
		this->Kick();
		return;

	}
	
	if(!IsInWorld())
		return;

	//CombatStatus.Update();

	if( !g_crosssrvmgr->m_isInstanceSrv )
	{
		if(m_bEXPIdle && m_EXPIdleTime)
		{
			if(UNIXTIME - m_EXPIdleTime > 5*60)
			{
				//send an exp item
				OnExpIdleTime();
				m_EXPIdleTime = UNIXTIME;
			}
		}

		if(UNIXTIME - m_playtimeTriggerTime > 20)
		{
			PlaytimeTriggerCheck();
			m_playedtime[0] += UNIXTIME - m_playtimeTriggerTime;
			m_playedtime[1] += UNIXTIME - m_playtimeTriggerTime;
			m_playedtime[2] += UNIXTIME - m_playtimeTriggerTime;
			//Calc played times
			m_playtimeTriggerTime = UNIXTIME;
		}
	}

	if( 0 )//m_session && !m_expireflag )
	{
		if( m_timeLastCheckFee == 0 )
			m_timeLastCheckFee = UNIXTIME;
#ifndef SUNYOU_FEE_DEBUG
		else if( UNIXTIME - m_timeLastCheckFee > 5 * 60 )
#else
		else if( UNIXTIME - m_timeLastCheckFee > 10 )
#endif
		{
			if( UNIXTIME >= m_expire )
			{
				MyLog::log->alert( "account decrease card point by one, account = %u character name = %s", m_session->GetAccountId(), m_playerInfo->name );

				if( --m_points <= 0 )
				{
					//LogonDatabase.WaitExecute( "UPDATE accounts SET points = points - 1 WHERE acct = %u", m_session->GetAccountId() );
					m_points = 0;
					AccountExpire();
					return;
				}
				else if( m_points == 1 )
				{
					//LogonDatabase.WaitExecute( "UPDATE accounts SET points = points - 2 WHERE acct = %u", m_session->GetAccountId() );
					m_expireMinutes = 5;
					SendLogoutInFewMinutesMsg();
					sEventMgr.AddEvent( this, &Player::SendLogoutInFewMinutesMsg, EVENT_PLAYER_LOGOUT_IN_FEW_MINUTES, 60*1000, 5, 0 );
					//sEventMgr.AddEvent( this, &Player::AccountExpire, EVENT_PLAYER_EXPIRE, 5*60*1000, 1, EVENT_FLAG_DELETES_OBJECT );
					m_expireflag = true;

					SQLCallbackBase* pCallBack = new SQLClassCallbackP0<Player>(this, &Player::UpdatePointsAndExpireProc);
					AsyncQuery * q = new AsyncQuery( pCallBack );
					q->AddQuery( "SELECT points, expire FROM accounts WHERE acct = %u", m_session->GetAccountId() );
					//LogonDatabase.QueueAsyncQuery( q );
				}
				else
					;//LogonDatabase.WaitExecute( "UPDATE accounts SET points = points - 1 WHERE acct = %u", m_session->GetAccountId() );

			}
			m_timeLastCheckFee = UNIXTIME;
		}
	}
	Unit::Update( p_time );
	uint32 mstime = getMSTime();

	if( mstime - m_itemupdate > 10*1000 )
	{
		m_itemupdate = mstime;
		m_ItemInterface->Update(time(NULL));
	}

	if(m_attacking)
	{
		if( m_curSelection == GetGUID() )
		{
			m_attacking = false;
			MyLog::log->info( "select target is self, stop attack" );
		}
		else
		{
			// Check attack timer.
			if(mstime >= m_attackTimer)
				_EventAttack(false);

			if(m_duelWield && mstime >= m_attackTimer_1)
				_EventAttack(true);
		}
	}

	if( m_onAutoShot)
	{
		if (m_curSelection == 0 || m_curSelection == GetGUID())
		{		
			m_onAutoShot = false;
			MSG_S2C::stSpell_Cancel_Auto_Repeat Msg;
			Msg.spell_id = 5003;
			m_session->SendPacket( Msg );
			MyLog::log->info( "select target is not enmy, stop autoshot" );
		}
		else
		{
			if( m_AutoShotAttackTimer > p_time )
			{
				//MyLog::log->debug( "HUNTER AUTOSHOT 0) %i, %i", m_AutoShotAttackTimer, p_time );
				m_AutoShotAttackTimer -= p_time;
			}
			else
			{
				//MyLog::log->debug( "HUNTER AUTOSHOT 1) %i", p_time );
				EventRepeatSpell();
			}
		}
	}
	else if(m_AutoShotAttackTimer > 0)
	{
		if(m_AutoShotAttackTimer > p_time)
			m_AutoShotAttackTimer -= p_time;
		else
			m_AutoShotAttackTimer = 0;
	}

	// Breathing
	if( m_UnderwaterState & UNDERWATERSTATE_UNDERWATER )
	{
		// keep subtracting timer
		if( m_UnderwaterTime )
		{
			// not taking dmg yet
			if(p_time >= m_UnderwaterTime)
				m_UnderwaterTime = 0;
			else
				m_UnderwaterTime -= p_time;
		}

		if( !m_UnderwaterTime )
		{
			// check last damage dealt timestamp, and if enough time has elapsed deal damage
			if( mstime >= m_UnderwaterLastDmg )
			{
				uint32 damage = m_uint32Values[UNIT_FIELD_MAXHEALTH] / 10;
				MSG_S2C::stDamage_Environmental_Log Msg;
				Msg.target_guid = GetGUID();
				Msg.type		= DAMAGE_DROWNING;
				Msg.damage		= damage;
				SendMessageToSet(Msg, true);

				DealDamage(this, damage, 0, 0, 0);
				m_UnderwaterLastDmg = mstime + 1000;
			}
		}
	}
	else
	{
		// check if we're not on a full breath timer
		if(m_UnderwaterTime < m_UnderwaterMaxTime)
		{
			// regenning
			m_UnderwaterTime += (p_time * 10);

			if(m_UnderwaterTime >= m_UnderwaterMaxTime)
			{
				m_UnderwaterTime = m_UnderwaterMaxTime;
				//StopMirrorTimer(1);
			}
		}
	}

	// Lava Damage
	if(m_UnderwaterState & UNDERWATERSTATE_LAVA)
	{
		// check last damage dealt timestamp, and if enough time has elapsed deal damage
		if(mstime >= m_UnderwaterLastDmg)
		{
			uint32 damage = m_uint32Values[UNIT_FIELD_MAXHEALTH] / 5;
			MSG_S2C::stDamage_Environmental_Log Msg;
			Msg.target_guid = GetGUID();
			Msg.type		= DAMAGE_LAVA;
			Msg.damage		= damage;
			SendMessageToSet(Msg, true);

			DealDamage(this, damage, 0, 0, 0);
			m_UnderwaterLastDmg = mstime + 1000;
		}
	}

	// Autosave items
	if( (uint32)UNIXTIME >= m_nextSave )
	{
		this->SaveToDB( true );
		m_nextSave = (uint32)UNIXTIME + 3 * TIME_MINUTE;
	}

	if(m_CurrentTransporter && !m_lockTransportVariables)
	{
		// Update our position, using trnasporter X/Y
		float c_tposx = m_CurrentTransporter->GetPositionX() + m_TransporterX;
		float c_tposy = m_CurrentTransporter->GetPositionY() + m_TransporterY;
		float c_tposz = m_CurrentTransporter->GetPositionZ() + m_TransporterZ;
		SetPosition(c_tposx, c_tposy, c_tposz, GetOrientation(), false);
	}


	if( !m_bTriggerXP && HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT) && GetUInt32Value(PLAYER_XP_TRIGGER) < 100 )
	{
		m_comatbat_xp_time += (UNIXTIME - m_entercombat_time);
		m_entercombat_time = UNIXTIME;

		const int t = 30*60;
		SetUInt32Value(PLAYER_XP_TRIGGER, m_comatbat_xp_time*100/t);
		if(m_comatbat_xp_time >= /*2*60*60*/t)
		{
			m_comatbat_xp_time = 0;
		}
	}

	if(m_incombattimeleft > 0)
	{
		m_incombattimeleft -= p_time;

		if(m_incombattimeleft <= 0)
		{
			RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);
		}
	}

	if(m_inarmedtimeleft > 0)
	{
		m_inarmedtimeleft -= p_time;
		if(m_inarmedtimeleft <= 0)
		{
			RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);
		}
	}

	if(m_pvpTimer)
	{
		if(p_time >= m_pvpTimer)
		{
			RemovePvPFlag();
			m_pvpTimer = 0;
		}
		else
			m_pvpTimer -= p_time;
	}

	if( mstime >= m_speedhackCheckTimer )
	{
		_SpeedhackCheck( mstime );
		m_speedhackCheckTimer = mstime + 1000;
	}

	if(m_session->m_bFangchenmiAccount)
	{
		uint32 playedt = (uint32)UNIXTIME - m_entergametime;
		if(m_session->m_ChenmiStat == 1)
		{
			if(m_session->m_ChenmiTime + playedt > TIME_BUJIANGKANG_GAME)
			{
				m_session->m_ChenmiStat = 2;
				m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageChenmiNotHealth ) ); //"您的账号被纳入防沉迷系统，累计在线时间已经进入不健康游戏时间，不会获得收益。请及时到网站填写防沉迷信息认证。");
			}
		}
		else if(m_session->m_ChenmiStat == 0)
		{
			if(m_session->m_ChenmiTime + playedt > TIME_BUJIANGKANG_GAME)
			{
				m_session->m_ChenmiStat = 2;
				m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageChenmiNotHealth ) ); //"您的账号被纳入防沉迷系统，累计在线时间已经进入不健康游戏时间，不会获得收益。请及时到网站填写防沉迷信息认证。");
			}
			else if(m_session->m_ChenmiTime + playedt > TIME_PILAO_GAME)
			{
				m_session->m_ChenmiStat = 1;
				m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageChenmiTired ) ); //"您的账号被纳入防沉迷系统，累计在线时间已经进入疲劳游戏时间，获得收益将减半。请及时到网站填写防沉迷信息认证。");
			}
		}

		if (playedt % (2 * TIME_MINUTE) == 0 && playedt / (2 * TIME_MINUTE) && (mstime % 1000) < 200 )
		{
			//m_session->SystemMessage("您本次上线时间:%s,当前累积游戏时间:%s,当前沉迷等级为%d ，请尽快去官方网站填写防沉迷资料！ ", ConvertTimeStampToString(playedt).c_str(),   ConvertTimeStampToString(m_session->m_ChenmiTime + playedt).c_str(), m_session->m_ChenmiStat);
			m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageChenmiNotify, quick_itoa(playedt/60), quick_itoa((m_session->m_ChenmiTime + playedt)/60), quick_itoa(m_session->m_ChenmiStat) ) );
		}
	}

	for( std::map<uint32, uint32>::iterator it = m_Titles.begin(); it != m_Titles.end(); )
	{
		std::map<uint32, uint32>::iterator it2 = it++;
		uint32 time_left = it2->second;
		uint32 id = it2->first;
		if( time_left > 0 && time_left <= (uint32)UNIXTIME )
		{
			m_Titles.erase( it2 );
			MSG_S2C::stTitleRemove msg;
			msg.title.title = id;
			m_session->SendPacket( msg );
		}
	}
}

void Player::EventDismount(uint32 money, float x, float y, float z)
{
	//ModUnsigned32Value( PLAYER_FIELD_COINAGE , -(int32)money );

	SetPosition(x, y, z, true);
	if(!m_taxiPaths.size())
		SetTaxiState(false);

	SetTaxiPath(NULL);
	UnSetTaxiPos();
	m_taxi_ride_time = 0;

	SetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID , 0);
	RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_MOUNTED_TAXI);
	RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER);

	SetPlayerSpeed(RUN, m_runSpeed);

	sEventMgr.RemoveEvents(this, EVENT_PLAYER_TAXI_INTERPOLATE);

	// Save to database on dismount
	SaveToDB(false);

	// If we have multiple "trips" to do, "jump" on the next one :p
	if(m_taxiPaths.size())
	{
		TaxiPath * p = *m_taxiPaths.begin();
		m_taxiPaths.erase(m_taxiPaths.begin());
		TaxiStart(p, taxi_model_id, 0);
	}

	ResetHeartbeatCoords();
}

void Player::_EventAttack( bool offhand )
{
	if( !m_EnableMeleeAttack )
		return;

	if( IsIceBlocking() )
		return;

	if(HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_SHAPE_CANTFIGHT))
		return;
	if(HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_MOUNT_CANTFIGHT))
		return;

	if (m_currentSpell)
	{
		//m_currentSpell->cancel();
		setAttackTimer(500, offhand);
		return;
	}

	if( IsFeared() || IsStunned() | IsAsleep())
		return;

	Unit *pVictim = NULL, *pSwingVictim = NULL;
	if(m_curSelection)
		pVictim = GetMapMgr()->GetUnit(m_curSelection);

	if (m_curSwingTarget)
		pSwingVictim = GetMapMgr()->GetUnit(m_curSwingTarget);

	if (pSwingVictim && pSwingVictim != pVictim)
	{
		m_curSwingTarget = 0;
		EventAttackStop();
		smsg_AttackStop(pVictim);
		return;
	}
	else if (!pSwingVictim)
	{
		m_curSwingTarget = 0;
	}

	//Can't find victim, stop attacking
	if (!pVictim)
	{
		MyLog::log->notice("Player::Update:  No valid current selection to attack, stopping attack\n");
		setHRegenTimer(5000); //prevent clicking off creature for a quick heal
		EventAttackStop();
		return;
	}

	float distance = sqrt(GetDistanceSq(pVictim));

	bool badfacing = false;
	if (!canReachWithAttack(pVictim, badfacing))
	{
		if( (uint32)UNIXTIME - m_AttackMsgTimer >= 2 )
		{
			if( !badfacing )
			{
				MSG_S2C::stAttack_Swing_Not_In_Range Msg;
				m_session->SendPacket( Msg );
			}
			else
			{
				MSG_S2C::stAttack_Swing_Bad_Facing Msg;
				m_session->SendPacket( Msg );
			}

			m_AttackMsgTimer = (uint32)UNIXTIME;
			m_curSwingTarget = pVictim->GetGUID();
		}
		else
		{
			//RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);
			//m_incombattimeleft = 5000;
			//EventAttackStop();
			//smsg_AttackStop(pVictim);
			//setAttackTimer(300, offhand);
		}
	}
	else if(distance >= 12.0f)
	{
		RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);
		//m_incombattimeleft = 5000;
		EventAttackStop();
		smsg_AttackStop(pVictim);
	}
	else if(isFriendly(this, pVictim))
	{
		//RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);
		//m_incombattimeleft = 5000;
		EventAttackStop();
		smsg_AttackStop(pVictim);
	}
	else if(!isInFront(pVictim))
	{
		// We still have to do this one.
		if( (uint32)UNIXTIME - m_AttackMsgTimer >= 2 )
		{
			MSG_S2C::stAttack_Swing_Bad_Facing Msg;
			m_session->SendPacket( Msg );
			m_AttackMsgTimer = (uint32)UNIXTIME;
		}
		setAttackTimer(300, offhand);
	}
	else if(m_special_state&UNIT_STATE_STORM)
	{
		//m_incombattimeleft = 5000;
	}
	else
	{
		m_curSwingTarget = 0;
		m_AttackMsgTimer = 0;
		m_incombattimeleft = -1;

		// Set to weapon time.
		setAttackTimer(0, offhand);

		//pvp timeout reset
		if(pVictim->IsPlayer())
		{
			if (static_cast< Player* >(pVictim)->cannibalize)
			{
				sEventMgr.RemoveEvents(pVictim, EVENT_CANNIBALIZE);
				pVictim->SetUInt32Value(UNIT_NPC_EMOTESTATE, 0);
				static_cast< Player* >(pVictim)->cannibalize = false;
			}
		}

		if(this->IsStealth())
		{
			RemoveAura( m_stealth );
			SetStealth(0);
		}

		if (!GetOnMeleeSpell() || offhand)
		{
			SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);
			m_inarmedtimeleft = 20000;

			Strike( pVictim, ( offhand ? OFFHAND : MELEE ), NULL, 0, 0, 0, false, true , false, true, true);
		}
		else
		{
			SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);
			m_inarmedtimeleft = 20000;

			SpellEntry *spellInfo = dbcSpell.LookupEntry(GetOnMeleeSpell());
			SetOnMeleeSpell(0);
			Spell *spell = new Spell(this,spellInfo,true,NULL);
			SpellCastTargets targets;
			targets.m_unitTarget = GetSelection();
			spell->prepare(&targets);
		}
	}
}

void Player::_EventCharmAttack()
{
	if(!m_CurrentCharm)
		return;

	Unit *pVictim = NULL;
	if(!IsInWorld())
	{
		m_CurrentCharm=NULL;
		sEventMgr.RemoveEvents(this,EVENT_PLAYER_CHARM_ATTACK);
		return;
	}

	if(m_curSelection == 0)
	{
		sEventMgr.RemoveEvents(this, EVENT_PLAYER_CHARM_ATTACK);
		return;
	}

	pVictim= GetMapMgr()->GetUnit(m_curSelection);

	//Can't find victim, stop attacking
	if (!pVictim)
	{
		MyLog::log->error( "WORLD: "I64FMT" doesn't exist.",m_curSelection);
		MyLog::log->notice("Player::Update:  No valid current selection to attack, stopping attack\n");
		this->setHRegenTimer(5000); //prevent clicking off creature for a quick heal
		clearStateFlag(UF_ATTACKING);
		EventAttackStop();
	}
	else
	{
		bool badfacing = false;
		if (!m_CurrentCharm->canReachWithAttack(pVictim, badfacing))
		{
			sEventMgr.ModifyEventTimeLeft(this, EVENT_PLAYER_CHARM_ATTACK, 100);
			if( (uint32)UNIXTIME - m_AttackMsgTimer >= 2 )
			{
				//m_session->OutPacket(SMSG_ATTACKSWING_NOTINRANGE);
				//m_AttackMsgTimer = GetUInt32Value(UNIT_FIELD_BASEATTACKTIME);		// 2 sec till next msg.
				if( !badfacing )
				{
					MSG_S2C::stAttack_Swing_Not_In_Range Msg;
					m_session->SendPacket( Msg );
				}
				else
				{
					MSG_S2C::stAttack_Swing_Bad_Facing Msg;
					m_session->SendPacket( Msg );
				}

				m_AttackMsgTimer = (uint32)UNIXTIME;
			}
			// Shorten, so there isnt a delay when the client IS in the right position.
		}
		/*
		else if(!m_CurrentCharm->isInFront(pVictim))
		{
			if(m_AttackMsgTimer == 0)
			{
				MSG_S2C::stAttack_Swing_Bad_Facing Msg;
				m_session->SendPacket( Msg );
				m_AttackMsgTimer = GetUInt32Value(UNIT_FIELD_BASEATTACKTIME);		// 2 sec till next msg.
			}
			// Shorten, so there isnt a delay when the client IS in the right position.
			sEventMgr.ModifyEventTimeLeft(this, EVENT_PLAYER_CHARM_ATTACK, 100);
		}
		*/
		else
		{
			//if(pVictim->GetTypeId() == TYPEID_UNIT)
			//	pVictim->GetAIInterface()->StopMovement(5000);

			//pvp timeout reset
			/*if(pVictim->IsPlayer())
			{
				if( static_cast< Player* >( pVictim )->DuelingWith == NULL)//Dueling doesn't trigger PVP
					static_cast< Player* >( pVictim )->PvPTimeoutUpdate(false); //update targets timer

				if(DuelingWith == NULL)//Dueling doesn't trigger PVP
					PvPTimeoutUpdate(false); //update casters timer
			}*/

			if (!m_CurrentCharm->GetOnMeleeSpell())
			{
				m_CurrentCharm->Strike( pVictim, MELEE, NULL, 0, 0, 0, false, false, false, true, true );
			}
			else
			{
				SpellEntry *spellInfo = dbcSpell.LookupEntry(m_CurrentCharm->GetOnMeleeSpell());
				m_CurrentCharm->SetOnMeleeSpell(0);
				Spell *spell = new Spell(m_CurrentCharm,spellInfo,true,NULL);
				SpellCastTargets targets;
				targets.m_unitTarget = GetSelection();
				spell->prepare(&targets);
				//delete spell;		 // deleted automatically, no need to do this.
			}
		}
	}
}

void Player::EventAttackStart()
{
	m_attacking = true;
	m_inarmedtimeleft = 5000;
}

void Player::EventAttackStop()
{
	if(m_CurrentCharm != NULL)
		sEventMgr.RemoveEvents(this, EVENT_PLAYER_CHARM_ATTACK);

	m_attacking = false;
	//CombatStatus.Vanished();

	//m_incombattimeleft = 5000;
}

void Player::_EventExploration()
{
	return;
	if (isDead())
		return;

	if (!IsInWorld())
		return;

	if(m_position.x > _maxX || m_position.x < _minX || m_position.y > _maxY || m_position.y < _minY)
		return;

	if(GetMapMgr()->GetCellByCoords(GetPositionX(),GetPositionY()) == NULL)
		return;

	uint16 AreaId = GetMapMgr()->GetAreaID(GetPositionX(),GetPositionY());

	if(!AreaId || AreaId == 0xFFFF)
		return;

	// AreaId fix for undercity and ironforge.  This will now enable rest for these 2 cities.
	// since they're both on the same map, only 1 map id check
	if (GetMapId() == 0)
	{
		// get position
		float ss_x = m_position.x;
		float ss_y = m_position.y;
		float ss_z = m_position.z;

		// Check for Undercity, Tirisfal Glades, and Ruins of Lordaeron, if neither, skip
		if (AreaId == 153 || AreaId == 85 || m_AreaID == 1497)
		{
			// ruins check
			if (ss_z < 74)
			{
				// box with coord 1536,174 -> 1858,353; and z < 62.5 for reachable areas
				if (ss_y > 174 && ss_y < 353 && ss_x > 1536 && ss_x < 1858)
				{
					AreaId = 1497;
				}
			}
			// inner city check
			if (ss_z < 38)
			{
				// box with coord 1238, 11 -> 1823, 640; and z < 38 for undeground
				if (ss_y > 11 && ss_y < 640 && ss_x > 1238 && ss_x < 1823)
				{
					AreaId = 1497;
				}
			}
			// todo bat tunnel, only goes part way, but should be fine for now
		}
		// Check for Ironforge, and Gates of IronForge.. if neither skip
		if (AreaId == 809 || m_AreaID == 1537) {
			// height check
			if (ss_z > 480)
			{
				// box with coord -5097.3, -828 -> -4570, -1349.3; and z > 480.
				if (ss_y > -1349.3 && ss_y < -828 && ss_x > -5097.3 && ss_x < -4570)
				{
					AreaId = 1537;
				}
			}
		}
	}

	AreaTable * at = AreaStorage.LookupEntry(AreaId);
	if(at == 0)
		return;

	/*char areaname[200];
	if(at)
	{
		strcpy(areaname, sAreaStore.LookupString((uint32)at->name));
	}
	else
	{
		strcpy(areaname, "UNKNOWN");
	}
    sChatHandler.BlueSystemMessageToPlr(this,areaname);*/
/*
	int offset = at->explorationFlag / 32;
	offset += PLAYER_EXPLORED_ZONES_1;

	uint32 val = (uint32)(1 << (at->explorationFlag % 32));
	uint32 currFields = GetUInt32Value(offset);

	if(AreaId != m_AreaID)
	{
		m_AreaID = AreaId;
		UpdatePvPArea();
		MSG_S2C::stPartyMemberStat MsgNULL;
		if(GetGroup())
            GetGroup()->UpdateOutOfRangePlayer(this, 128, true, &MsgNULL);
	}

	// Zone update, this really should update to a parent zone if one exists.
	//  Will show correct location on your character screen, as well zoneid in DB will have correct value
	//  for any web sites that access that data.
	if(at->ZoneId == 0 && m_zoneId != AreaId)
	{
		ZoneUpdate(AreaId);
	}
	else if (at->ZoneId != 0 && m_zoneId != at->ZoneId)
	{
		ZoneUpdate(at->ZoneId);
	}


	if(at->ZoneId != 0 && m_zoneId != at->ZoneId)
		ZoneUpdate(at->ZoneId);

	bool rest_on = false;
	// Check for a restable area
    if(at->AreaFlags & AREA_CITY_AREA || at->AreaFlags & AREA_CITY)
	{
		// check faction
		if((at->category == AREAC_ALLIANCE_TERRITORY && GetTeam() == 0) || (at->category == AREAC_HORDE_TERRITORY && GetTeam() == 1) )
		{
			rest_on = true;
		}
		else if(at->category != AREAC_ALLIANCE_TERRITORY && at->category != AREAC_HORDE_TERRITORY)
		{
			rest_on = true;
		}
	}
	else
	{
        //second AT check for subzones.
        if(at->ZoneId)
        {
            AreaTable * at2 = AreaStorage.LookupEntry(at->ZoneId);
            if(at2 && at2->AreaFlags & AREA_CITY_AREA || at2 && at2->AreaFlags & AREA_CITY)
            {
                if((at2->category == AREAC_ALLIANCE_TERRITORY && GetTeam() == 0) || (at2->category == AREAC_HORDE_TERRITORY && GetTeam() == 1) )
				{
					rest_on = true;
				}
				else if(at2->category != AREAC_ALLIANCE_TERRITORY && at2->category != AREAC_HORDE_TERRITORY)
				{
					rest_on = true;
				}
            }
		}
	}
	if (rest_on)
	{
		if(!m_isResting) ApplyPlayerRestState(true);
	}
	else
	{
		if(m_isResting)
		{
#ifdef COLLISION
			const LocationVector & loc = GetPosition();
			if(!CollideInterface.IsIndoor(GetMapIDForCollision(), loc.x, loc.y, loc.z + 2.0f))
				ApplyPlayerRestState(false);
#else
			ApplyPlayerRestState(false);
#endif
		}
	}

	if( !(currFields & val) && !GetTaxiState() && !m_TransporterGUID)//Unexplored Area		// bur: we dont want to explore new areas when on taxi
	{
		SetUInt32Value(offset, (uint32)(currFields | val));

		uint32 explore_xp = at->level * 10;

		MSG_S2C::stArea_Exploration_XP Msg;
		Msg.areaid = at->AreaId;
		Msg.xp		= explore_xp;
		m_session->SendPacket(Msg);

		if(getLevel() < GetUInt32Value(PLAYER_FIELD_MAX_LEVEL) && explore_xp)
			GiveXP(explore_xp, 0, false);
	}
	*/
}

void Player::EventDeath()
{
	if (m_state & UF_ATTACKING)
		EventAttackStop();

	if (m_onTaxi)
		sEventMgr.RemoveEvents(this, EVENT_PLAYER_TAXI_DISMOUNT);

	SetUInt32Value( UNIT_FIELD_HEALTH, 0 );
	SetUInt32Value( UNIT_FIELD_POWER1, 0 );
}

void Player::AttackAfterSpell(uint64 target_guid, SpellEntry* sp)
{
	if( !m_EnableMeleeAttack )
		return;

	Unit* unittarget = GetMapMgr()->GetUnit(target_guid);

	if(unittarget && unittarget->isAlive() && isAttackable(this, unittarget))
	{
		bool badfacing = false;
		if(getClass() == CLASS_BOW)
		{
			Item* itm = GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_MAINHAND );
			if(itm &&( itm->GetProto()->Class == ITEM_CLASS_WEAPON && (itm->GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_BOW 
				||itm->GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_GUN || itm->GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_CROSSBOW)))
			{
				if (!m_onAutoShot)
				{
					m_onAutoShot = true;
					MSG_S2C::stSpell_Begin_AutoRepeat MsgBegin;
					MsgBegin.spell_id = 5003;
					m_session->SendPacket( MsgBegin );
				}
			
				m_AutoShotTarget = target_guid;
				if (sp&& sp->Id == 5003)
				{
					m_AutoShotAttackTimer = GetUInt32Value(UNIT_FIELD_RANGEDATTACKTIME);
				}

				return;


			}
		}

		if(!canReachWithAttack(unittarget, badfacing))
		{
			if( !badfacing )
			{
				MSG_S2C::stAttack_Swing_Not_In_Range Msg;
				m_session->SendPacket( Msg );
			}
			else
			{
				MSG_S2C::stAttack_Swing_Bad_Facing Msg;
				m_session->SendPacket( Msg );
			}

			//MyLog::log->debug( "Enemy %s %u far from attaker", "creature", unittarget->GetGUID());
			return;
		}	


		if(!IsAttacking())
		{
			smsg_AttackStart(unittarget);
			EventAttackStart();
		}
	}
}

///  This function sends the message displaying the purple XP gain for the char
///  It assumes you will send out an UpdateObject packet at a later time.
uint32 Player::GiveXP(uint32 xp, const uint64 &guid, bool allowbonus, give_exp_type type )
{
	if( getLevel() >= 80 )
		return 0;

	if(m_session->m_ChenmiStat == 2)
	{
		xp = 0;
		return 0;
	}
	//xp = 650;
	if ( xp < 1 )
		return 0;

	if(getLevel() >= GetUInt32Value(PLAYER_FIELD_MAX_LEVEL))
		return 0;

	uint32 restxp = xp;

	//add reststate bonus (except for quests)
	if(m_restState == RESTSTATE_RESTED && allowbonus)
	{
		//restxp = SubtractRestXP(xp);
		//xp += restxp;
	}

	// for activity tools
	xp *= g_activity_mgr->exp_modifier[type];

	if(m_session->m_ChenmiStat == 1)
	{
		xp /= 2;
	}

	UpdateRestState();
	SendLogXPGain(guid,xp,restxp,guid == 0 ? true : false);

	/*
	uint32 curXP = GetUInt32Value(PLAYER_XP);
	uint32 nextLvlXP = GetUInt32Value(PLAYER_NEXT_LEVEL_XP);
	uint32 newXP = curXP + xp;
	uint32 level = GetUInt32Value(UNIT_FIELD_LEVEL);
	bool levelup = false;

	if(m_Summon != NULL && m_Summon->GetUInt32Value(UNIT_CREATED_BY_SPELL) == 0)
		m_Summon->GiveXP(xp);

	uint32 TotalHealthGain = 0, TotalManaGain = 0;
	uint32 cl=getClass();
	// Check for level-up
	while (newXP >= nextLvlXP)
	{
		levelup = true;
		// Level-Up!
		newXP -= nextLvlXP;  // reset XP to 0, but add extra from this xp add
		level ++;	// increment the level
		if( level > 9)
		{
			//Give Talent Point
			uint32 curTalentPoints = GetUInt32Value(PLAYER_CHARACTER_POINTS1);
			SetUInt32Value(PLAYER_CHARACTER_POINTS1,curTalentPoints+1);
		}
	}
	*/

	int32 newxp = m_uint32Values[PLAYER_XP] + xp;
	int32 nextlevelxp = lvlinfo->XPToNextLevel;
	uint32 level = m_uint32Values[UNIT_FIELD_LEVEL];
	LevelInfo * li;
	bool levelup = false;

	while(newxp >= nextlevelxp && newxp > 0)
	{
		++level;
		li = objmgr.GetLevelInfo(getRace(), getClass(), level);
		newxp -= nextlevelxp;
		nextlevelxp = li->XPToNextLevel;
		levelup = true;

		if(level > 9)
			ModUnsigned32Value(PLAYER_CHARACTER_POINTS1, 1);

		if(level >= GetUInt32Value(PLAYER_FIELD_MAX_LEVEL))
			break;
	}

	if(level > GetUInt32Value(PLAYER_FIELD_MAX_LEVEL))
		level = GetUInt32Value(PLAYER_FIELD_MAX_LEVEL);

	if(levelup)
	{
		m_playerInfo->lastLevel = level;
		uint32 titleindex = 0;
		switch(level)
		{
		case 10:titleindex = 1;break;
		case 20:titleindex = 2;break;
		case 30:titleindex = 3;break;
		case 40:titleindex = 4;break;
		case 50:titleindex = 5;break;
		case 60:titleindex = 6;break;
		case 70:titleindex = 7;break;
		case 80:titleindex = 8;break;
		}
		if(titleindex)
		{
			InsertTitle(titleindex);
			MSG_S2C::stTitleAdd msg;
			msg.title.title = titleindex;
			m_session->SendPacket(msg);
		}
		if(level == 20)
		{
			//发送生肖道具
			const uint32 animal_item[] = {102510001,102510002,102510003,102510004,102510005, 102510006,102510007,102510008,102510009,102510010,102510011,102510012,};
			uint32 index = rand()%12;
			GiveGift(1, animal_item[index], 1);

			m_session->SystemMessage( build_language_string( BuildString_PlayerSystemMessageZodiacGift ) );//"恭喜您到达了20级,非常幸运的获得了一个生肖玉佩!");
		}
		m_playedtime[0] = 0; //Reset the "Current level played time"

		SetUInt32Value(UNIT_FIELD_LEVEL, level);
		LevelInfo * oldlevel = lvlinfo;
		lvlinfo = objmgr.GetLevelInfo(getRace(), getClass(), level);
		CalculateBaseStats();

		if( level == 9 )
			SetUInt32Value( PLAYER_XP_TRIGGER, XP_TRIGGER_BAR_MAX );
		// Generate Level Info Packet and Send to client
        SendLevelupInfo(
            level,
            lvlinfo->HP - oldlevel->HP,
            lvlinfo->Mana - oldlevel->Mana,
            lvlinfo->Stat[0] - oldlevel->Stat[0],
            lvlinfo->Stat[1] - oldlevel->Stat[1],
            lvlinfo->Stat[2] - oldlevel->Stat[2],
            lvlinfo->Stat[3] - oldlevel->Stat[3],
            lvlinfo->Stat[4] - oldlevel->Stat[4],
			lvlinfo->Stat[5] - oldlevel->Stat[5] );

		_UpdateMaxSkillCounts();

		//UpdateChances();

		// Set next level conditions
		SetUInt32Value(PLAYER_NEXT_LEVEL_XP, lvlinfo->XPToNextLevel);

		// ScriptMgr hook for OnPostLevelUp
		sHookInterface.OnPostLevelUp(this);

		// Set stats
		for(uint32 i = 0; i < 6; ++i)
		{
			BaseStats[i] = lvlinfo->Stat[i];
			CalcStat(i);
		}
		UpdateStats();
		//set full hp and mana
		SetUInt32Value(UNIT_FIELD_HEALTH,GetUInt32Value(UNIT_FIELD_MAXHEALTH));
		SetUInt32Value(UNIT_FIELD_POWER1,GetUInt32Value(UNIT_FIELD_MAXPOWER1));

		//check teacher student level
		CheckTeacherStudentLevel();

		if( level == 80 )
		{
			for( int i = 0; i < 3; ++i )
			{
				if( !objmgr.first_lv_80[i] )
				{
					objmgr.first_lv_80[i] = GetLowGUID();
					sWorld.ExecuteSqlToDBServer( "insert into first_lv_80 values(%u, %u, '%s', %u)", objmgr.first_lv_80[i], m_playerInfo->acct, GetName(), (uint32)UNIXTIME );
					InsertTitle( 40 + i );
					break;
				}
				else
				{
					if( objmgr.first_lv_80[i] == GetLowGUID() )
						break;
				}
			}
		}
/*
		MSG_S2C::stSystem_Tips Msg;

		StorageContainerIterator<QuestTips> * itr = QuestTipsStorage.MakeIterator();
		QuestTips * p;
		while(!itr->AtEnd())
		{
			p = itr->Get();
			if((p->racemask & getRaceMask()) && (p->classmask & getClassMask()) && p->level == level)
			{
				MSG_S2C::stSystem_Tips::tips tip;
				tip.ntype = p->type;
				tip.tip = p->text;
				Msg.vtips.push_back(tip);
				MyLog::log->notice( "player:%s level up, send system tip to him", GetName() );
			}
			if(!itr->Inc())
				break;
		}
		itr->Destruct();
		if(Msg.vtips.size())
			m_session->SendPacket(Msg);
*/
	}
	// Set the update bit
	SetUInt32Value(PLAYER_XP, newxp);

	HandleProc(PROC_ON_GAIN_EXPIERIENCE, this, NULL);
	m_procCounter = 0;

	BetaBonus();

	UpdateInrangeQuestGiverState();
	return xp;
}

uint32 Player::CalcCastleExpBonus( Unit* victim, uint32 exp )
{
	float result = (float)exp;
	std::set<SunyouCastle*> tmp = g_instancequeuemgr.GetCastleByPlayer( this );
	for( std::set<SunyouCastle*>::iterator it = tmp.begin(); it != tmp.end(); ++it )
	{
		SunyouCastle* castle = *it;
		result *= 1.05;
		if( victim->GetMapId() == castle->GetCastleConfig()->exp_map_id )
			result *= ( (float)castle->GetCastleConfig()->exp_rate + 100.f ) / 100.f;
	}
	return exp;
}

void Player::smsg_InitialSpells()
{
	PlayerCooldownMap::iterator itr, itr2;

	uint16 spellCount = (uint16)mSpells.size();
	uint32 mstime = getMSTime();

	MSG_S2C::stSpells_Initial Msg;
	Msg.spellCount = spellCount;

	SpellSet::iterator sitr;
	for (sitr = mSpells.begin(); sitr != mSpells.end(); ++sitr)
	{
		// todo: check out when we should send 0x0 and when we should send 0xeeee
		// this is not slot,values is always eeee or 0,seems to be cooldown
		Msg.vSpells.push_back(*sitr);				   // spell id
	}

	Msg.itemCount = 0;
	for( itr = m_cooldownMap[COOLDOWN_TYPE_SPELL].begin(); itr != m_cooldownMap[COOLDOWN_TYPE_SPELL].end(); )
	{
		itr2 = itr++;

		// don't keep around expired cooldowns
		if( itr2->second.ExpireTime < mstime || (itr2->second.ExpireTime - mstime) < 10000 )
		{
			m_cooldownMap[COOLDOWN_TYPE_SPELL].erase( itr2 );
			continue;
		}

		Msg.vSpellCDs.push_back(MSG_S2C::stSpells_Initial::stCoolDown(itr2->first, itr2->second.ItemId, 0, itr2->second.ExpireTime - mstime, 0));
		++Msg.itemCount;

#ifdef _DEBUG
//		MyLog::log->debug("InitialSpells : sending spell cooldown for spell %u to %u ms", itr2->first, itr2->second.ExpireTime - mstime);
#endif
	}

	for( itr = m_cooldownMap[COOLDOWN_TYPE_CATEGORY].begin(); itr != m_cooldownMap[COOLDOWN_TYPE_CATEGORY].end(); )
	{
		itr2 = itr++;

		// don't keep around expired cooldowns
		if( itr2->second.ExpireTime < mstime || (itr2->second.ExpireTime - mstime) < 10000 )
		{
			m_cooldownMap[COOLDOWN_TYPE_CATEGORY].erase( itr2 );
			continue;
		}

		Msg.vSpellCDs.push_back(MSG_S2C::stSpells_Initial::stCoolDown(itr2->second.SpellId, itr2->second.ItemId, itr2->first, 0, itr2->second.ExpireTime - mstime));
		++Msg.itemCount;

#ifdef _DEBUG
//		MyLog::log->debug("InitialSpells : sending category cooldown for cat %u to %u ms", itr2->first, itr2->second.ExpireTime - mstime);
#endif
	}

	m_session->SendPacket(Msg);

	uint32 v = 0;
	//GetSession()->OutPacket(0x041d, 4, &v);
	//Log::getSingleton( ).outDetail( "CHARACTER: Sent Initial Spells" );
}

void Player::_SavePet(QueryBuffer * buf)
{
	// Remove any existing info
	buf->AddQuery("DELETE FROM playerpets WHERE ownerguid=%u", GetUInt32Value(OBJECT_FIELD_GUID));

	if(m_Summon&&m_Summon->IsInWorld()&&m_Summon->GetPetOwner()==this)	// update PlayerPets array with current pet's info
	{
		PlayerPet*pPet = GetPlayerPet(m_Summon->m_PetNumber);
		if(!pPet || pPet->active == false)
			m_Summon->UpdatePetInfo(true);
		else m_Summon->UpdatePetInfo(false);

		if(!m_Summon->Summon)	   // is a pet
		{
			// save pet spellz
			PetSpellMap::iterator itr = m_Summon->mSpells.begin();
			uint32 pn = m_Summon->m_PetNumber;
			buf->AddQuery("DELETE FROM playerpetspells WHERE petnumber=%u", pn);

			for(; itr != m_Summon->mSpells.end(); ++itr)
			{
				buf->AddQuery("INSERT INTO playerpetspells VALUES(%u, %u, %u, %u)", GetLowGUID(), pn, itr->first->Id, itr->second);
			}
		}
	}

	std::stringstream ss;

	for(std::map<uint32, PlayerPet*>::iterator itr = m_Pets.begin(); itr != m_Pets.end(); itr++)
	{
		ss.rdbuf()->str("");
		ss << "INSERT INTO playerpets VALUES("
			<< GetLowGUID() << ","
			<< itr->second->number << ",'"
			<< CharacterDatabase.EscapeString(itr->second->name) << "',"
			<< itr->second->entry << ",'"
			<< itr->second->fields << "',"
			<< itr->second->xp << ","
			<< (itr->second->active ?  1 : 0) + itr->second->stablestate * 10 << ","
			<< itr->second->level << ","
			<< itr->second->loyaltyxp << ",'"
			<< itr->second->actionbar << "',"
			<< itr->second->happinessupdate << ","
			<< itr->second->summon << ","
			<< itr->second->loyaltypts << ","
			<< itr->second->loyaltyupdate << ","
			<< itr->second->LastFeedTime << ")";

		buf->AddQueryStr(ss.str());
	}
}

void Player::_SavePetSpells(QueryBuffer * buf)
{
	// Remove any existing
	buf->AddQuery("DELETE FROM playersummonspells WHERE ownerguid=%u", GetLowGUID());

	// Save summon spells
	map<uint32, set<uint32> >::iterator itr = SummonSpells.begin();
	for(; itr != SummonSpells.end(); ++itr)
	{
		set<uint32>::iterator it = itr->second.begin();
		for(; it != itr->second.end(); ++it)
		{
			buf->AddQuery("INSERT INTO playersummonspells VALUES(%u, %u, %u)", GetLowGUID(), itr->first, (*it));
		}
	}
}

void Player::AddSummonSpell(uint32 Entry, uint32 SpellID)
{
	SpellEntry * sp = dbcSpell.LookupEntry(SpellID);
	map<uint32, set<uint32> >::iterator itr = SummonSpells.find(Entry);
	if(itr == SummonSpells.end())
		SummonSpells[Entry].insert(SpellID);
	else
	{
		set<uint32>::iterator it3;
		for(set<uint32>::iterator it2 = itr->second.begin(); it2 != itr->second.end();)
		{
			it3 = it2++;
			if(dbcSpell.LookupEntry(*it3)->NameHash == sp->NameHash)
				itr->second.erase(it3);
		}
		itr->second.insert(SpellID);
	}
}

void Player::RemoveSummonSpell(uint32 Entry, uint32 SpellID)
{
	map<uint32, set<uint32> >::iterator itr = SummonSpells.find(Entry);
	if(itr != SummonSpells.end())
	{
		itr->second.erase(SpellID);
		if(itr->second.size() == 0)
			SummonSpells.erase(itr);
	}
}

set<uint32>* Player::GetSummonSpells(uint32 Entry)
{
	map<uint32, set<uint32> >::iterator itr = SummonSpells.find(Entry);
	if(itr != SummonSpells.end())
	{
		return &(itr->second);
	}
	return 0;
}

void Player::_LoadPet(QueryResult * result)
{
	m_PetNumberMax=4;
	if(!result)
		return;

	do
	{
		Field *fields = result->Fetch();
		fields = result->Fetch();

		PlayerPet *pet = new PlayerPet;
		pet->number  = fields[1].GetUInt32();
		pet->name	= fields[2].GetString();
		pet->entry   = fields[3].GetUInt32();
		pet->fields  = fields[4].GetString();
		pet->xp	  = fields[5].GetUInt32();
		pet->active  = fields[6].GetInt8()%10 > 0 ? true : false;
		pet->stablestate = fields[6].GetInt8() / 10;
		pet->level   = fields[7].GetUInt32();
		pet->loyaltyxp = fields[8].GetUInt32();
		pet->actionbar = fields[9].GetString();
		pet->happinessupdate = fields[10].GetUInt32();
		pet->summon = (fields[11].GetUInt32()>0 ? true : false);
		pet->loyaltypts = fields[12].GetUInt32();
		pet->loyaltyupdate = fields[13].GetUInt32();
		pet->LastFeedTime = fields[14].GetUInt32();

		m_Pets[pet->number] = pet;
		if(pet->active)
		{
			if(iActivePet)  // how the hell can this happen
			{
				//printf("pet warning - >1 active pet.. weird..");
			}
			else
				iActivePet = pet->number;
		}

		if(pet->number > m_PetNumberMax)
			m_PetNumberMax =  pet->number;
	}while(result->NextRow());
}

void Player::SpawnPet(uint32 pet_number)
{
	std::map<uint32, PlayerPet*>::iterator itr = m_Pets.find(pet_number);
	if(itr == m_Pets.end())
	{
		MyLog::log->error("PET SYSTEM: "I64FMT" Tried to load invalid pet %d", GetGUID(), pet_number);
		return;
	}
	Pet *pPet = this->GetSummon();
	if(pPet)
	{
		pPet->Remove(false, false, true);
	}
	else
	{
		pPet = objmgr.CreatePet();
	}
	pPet->SetInstanceID(GetInstanceID());
	pPet->LoadFromDB(this, itr->second);

	iActivePet = pet_number;

}

void Player::_LoadPetSpells(QueryResult * result)
{
	std::stringstream query;
	std::map<uint32, std::list<uint32>* >::iterator itr;
	uint32 entry = 0;
	uint32 spell = 0;

	if(result)
	{
		do
		{
			Field *fields = result->Fetch();
			entry = fields[1].GetUInt32();
			spell = fields[2].GetUInt32();
			AddSummonSpell(entry, spell);
		}
		while( result->NextRow() );
	}
}

void Player::addSpell(uint32 spell_id, bool disp_new_learn)
{
	SpellSet::iterator iter = mSpells.find(spell_id);
	if(iter != mSpells.end())
		return;

	SpellEntry * se = dbcSpell.LookupEntry(spell_id);
	if( !se ) return;
	mSpells.insert(spell_id);
	if( se->Attributes & ATTRIBUTES_PASSIVE )
	{
		SpellCastTargets targets;
		targets.m_unitTarget = this->GetGUID();
		targets.m_targetMask = 0x2;
		Spell * spell=new Spell(this,se,true,NULL);
		spell->prepare(&targets);
	}

	if(IsInWorld())
	{
		MSG_S2C::stSpell_Learned Msg;
		Msg.spell_id = spell_id;
		Msg.disp_new_learn = disp_new_learn;
		m_session->SendPacket( Msg );
	}

	// Check if we're a deleted spell
	iter = mDeletedSpells.find(spell_id);
	if(iter != mDeletedSpells.end())
		mDeletedSpells.erase(iter);

	// Check if we're logging in.
	if(!IsInWorld())
		return;

	// Add the skill line for this spell if we don't already have it.
	skilllinespell * sk = objmgr.GetSpellSkill(spell_id);
	if(sk && !_HasSkillLine(sk->skilline))
	{
		skilllineentry * skill = dbcSkillLine.LookupEntry(sk->skilline);
		SpellEntry * spell = dbcSpell.LookupEntry(spell_id);
		uint32 max = 1;
		switch(skill->type)
		{
			case SKILL_TYPE_PROFESSION:
				max=75*((spell->RankNumber)+1);
				ModUnsigned32Value( PLAYER_CHARACTER_POINTS2, -1 ); // we are learning a proffesion, so substract a point.
				break;
			case SKILL_TYPE_SECONDARY:
				max=75*((spell->RankNumber)+1);
				break;
			case SKILL_TYPE_WEAPON:
				max=5*getLevel();
				break;
			case SKILL_TYPE_CLASS:
			case SKILL_TYPE_ARMOR:
				if(skill->id == SKILL_LOCKPICKING || skill->id == SKILL_POISONS)
					max=5*getLevel();
				break;
		};

		_AddSkillLine(sk->skilline, 1, max);
		_UpdateMaxSkillCounts();
	}
}

//===================================================================================================================
//  Set Create Player Bits -- Sets bits required for creating a player in the updateMask.
//  Note:  Doesn't set Quest or Inventory bits
//  updateMask - the updatemask to hold the set bits
//===================================================================================================================
void Player::_SetCreateBits(UpdateMask *updateMask, Player *target) const
{
	if(target == this)
	{
		Object::_SetCreateBits(updateMask, target);
	}
	else
	{
		for(uint32 index = 0; index < m_valuesCount; index++)
		{
			if(m_uint32Values[index] != 0 && Player::m_visibleUpdateMask.GetBit(index))
				updateMask->SetBit(index);
		}
	}
}


void Player::_SetUpdateBits(UpdateMask *updateMask, Player *target) const
{
	if(target == this)
	{
		Object::_SetUpdateBits(updateMask, target);
	}
	else
	{
		Object::_SetUpdateBits(updateMask, target);
		*updateMask &= Player::m_visibleUpdateMask;
	}
}


void Player::InitVisibleUpdateBits()
{
	Player::m_visibleUpdateMask.SetCount(PLAYER_END);
	Player::m_visibleUpdateMask.SetBit(OBJECT_FIELD_GUID);
	Player::m_visibleUpdateMask.SetBit(OBJECT_FIELD_TYPE);
	Player::m_visibleUpdateMask.SetBit(OBJECT_FIELD_SCALE_X);
	Player::m_visibleUpdateMask.SetBit(OBJECT_FIELD_UNIQUE_ID);

	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_SUMMON);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_SUMMON+1);

	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_TARGET);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_TARGET+1);

	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_HEALTH);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_POWER1);

	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_MAXHEALTH);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_MAXPOWER1);

	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_LEVEL);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_FACTIONTEMPLATE);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_BYTES_0);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_FLAGS);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_FLAGS_2);
	for(uint32 i = UNIT_FIELD_AURA; i <= UNIT_FIELD_AURASTATE; i ++)
		Player::m_visibleUpdateMask.SetBit(i);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_BASEATTACKTIME);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_BASEATTACKTIME+1);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_BOUNDINGRADIUS);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_COMBATREACH);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_DISPLAYID);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_NATIVEDISPLAYID);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_MOUNTDISPLAYID);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_BYTES_1);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_PETNUMBER);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_PET_NAME_TIMESTAMP);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_CHANNEL_OBJECT);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_CHANNEL_OBJECT+1);
	Player::m_visibleUpdateMask.SetBit(UNIT_CHANNEL_SPELL);
	Player::m_visibleUpdateMask.SetBit(UNIT_DYNAMIC_FLAGS);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_DEATH_STATE);

	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_FORMDISPLAYID);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_FFA);
	Player::m_visibleUpdateMask.SetBit(PLAYER_HEAD);
	Player::m_visibleUpdateMask.SetBit(PLAYER_HEAD1);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FOOT);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FOOT1);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FLAGS);
	Player::m_visibleUpdateMask.SetBit(PLAYER_BYTES);
	Player::m_visibleUpdateMask.SetBit(PLAYER_BYTES_2);
	Player::m_visibleUpdateMask.SetBit(PLAYER_BYTES_3);
	Player::m_visibleUpdateMask.SetBit(PLAYER_GUILD_TIMESTAMP);
	Player::m_visibleUpdateMask.SetBit(PLAYER_DUEL_TEAM);
	Player::m_visibleUpdateMask.SetBit(PLAYER_DUEL_ARBITER);
	Player::m_visibleUpdateMask.SetBit(PLAYER_DUEL_ARBITER+1);
	Player::m_visibleUpdateMask.SetBit(PLAYER_GUILDID);
	Player::m_visibleUpdateMask.SetBit(PLAYER_GUILDRANK);
	Player::m_visibleUpdateMask.SetBit(UNIT_FIELD_BYTES_2);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FIELD_HOSTILE_GUILDID);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FIELD_TEACHER);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FIELD_STUDENT);

	for(uint16 i = 0; i < EQUIPMENT_SLOT_END; i++)
	{
		Player::m_visibleUpdateMask.SetBit((uint16)(PLAYER_VISIBLE_ITEM_1_0 + (i*12))); // visual items for other players
		Player::m_visibleUpdateMask.SetBit((uint16)(PLAYER_VISIBLE_ITEM_1_0+1 + (i*12))); // visual items for other players
		Player::m_visibleUpdateMask.SetBit((uint16)(PLAYER_VISIBLE_ITEM_1_0+2 + (i*12))); // visual items for other players
		Player::m_visibleUpdateMask.SetBit((uint16)(PLAYER_VISIBLE_ITEM_1_0+3 + (i*12))); // visual items for other players
		Player::m_visibleUpdateMask.SetBit((uint16)(PLAYER_VISIBLE_ITEM_1_0+4 + (i*12))); // visual items for other players
		Player::m_visibleUpdateMask.SetBit((uint16)(PLAYER_VISIBLE_ITEM_1_0+5 + (i*12))); // visual items for other players
		Player::m_visibleUpdateMask.SetBit((uint16)(PLAYER_VISIBLE_ITEM_1_0+6 + (i*12))); // visual items for other players
		Player::m_visibleUpdateMask.SetBit((uint16)(PLAYER_VISIBLE_ITEM_1_0+7 + (i*12))); // visual items for other players
		Player::m_visibleUpdateMask.SetBit((uint16)(PLAYER_VISIBLE_ITEM_1_0+8 + (i*12))); // visual items for other players
		Player::m_visibleUpdateMask.SetBit((uint16)(PLAYER_VISIBLE_ITEM_1_0+9 + (i*12))); // visual items for other players
	}

	Player::m_visibleUpdateMask.SetBit(PLAYER_FIELD_SHAPE_MOUNT_FLAG);
	Player::m_visibleUpdateMask.SetBit(PLAYER_VISIBLE_ITEM_SHOW_SHIZHUANG);
	Player::m_visibleUpdateMask.SetBit(PLAYER_CHOSEN_TITLE);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FIELD_IS_FLYING);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FIELD_GROUP_ID);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FIELD_GRAVITY_RATE);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FIELD_LOOT_EQUIPMENT_RATE);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FIELD_DROP_EQUIPMENT_RATE);
	Player::m_visibleUpdateMask.SetBit(PLAYER_FIELD_AVATAR_SETTING);

	/* fuck i hate const - burlex */
	/*if(target && target->GetGroup() == const_cast<Player*>(this)->GetGroup() && const_cast<Player*>(this)->GetSubGroup() == target->GetSubGroup())
	{
	// quest fields are the same for party members
	for(uint32 i = PLAYER_QUEST_LOG_1_01; i < PLAYER_QUEST_LOG_25_2; ++i)
	Player::m_visibleUpdateMask.SetBit(i);
	}*/
}


void Player::DestroyForPlayer( Player *target ) const
{
	Unit::DestroyForPlayer( target );
}

#define IS_ARENA(x) ( (x) >= BATTLEGROUND_ARENA_2V2 && (x) <= BATTLEGROUND_ARENA_5V5 )

void Player::SaveToDB(bool periodic_save /* = false */)
{
	bool in_arena = false;
	QueryBuffer* buf = new QueryBuffer;

	if(m_uint32Values[PLAYER_CHARACTER_POINTS2]>2)
		m_uint32Values[PLAYER_CHARACTER_POINTS2]=2;

	//Calc played times
	uint32 playedt = (uint32)UNIXTIME - m_playedtime[2];
	m_playedtime[0] += playedt;
	m_playedtime[1] += playedt;
	m_playedtime[2] += playedt;

	std::stringstream ss;
	ss << "REPLACE INTO characters VALUES ("

	<< GetLowGUID() << ", "
	<< GetSession()->GetAccountId() << ","

	// stat saving
	<< "'" << CharacterDatabase.EscapeString(m_name) << "', "
	<< uint32(getRace()) << ","
	<< uint32(getClass()) << ","
	<< uint32(getGender()) << ",";

	if(m_uint32Values[UNIT_FIELD_FACTIONTEMPLATE] != info->factiontemplate)
		ss << m_uint32Values[UNIT_FIELD_FACTIONTEMPLATE] << ",";
	else
		ss << "0,";

	ss << uint32(getLevel()) << ","
	<< m_uint32Values[PLAYER_XP] << ","

	// dump exploration data
	<< "'";
	ss << "','";

	// dump skill data
	/*for(uint32 i=PLAYER_SKILL_INFO_1_1;i<PLAYER_CHARACTER_POINTS1;i+=3)
	{
		if(m_uint32Values[i])
		{
			ss << m_uint32Values[i] << ","
			  << m_uint32Values[i+1]<< ",";
		}
	}*/

	/*for(uint32 i = PLAYER_SKILL_INFO_1_1; i < PLAYER_CHARACTER_POINTS1; ++i)
		ss << m_uint32Values[i] << " ";
	*/

	for(SkillMap::iterator itr = m_skills.begin(); itr != m_skills.end(); ++itr)
	{
		if(itr->first && itr->second.Skill->type != SKILL_TYPE_LANGUAGE)
		{
			ss << itr->first << ";"
				<< itr->second.CurrentValue << ";"
				<< itr->second.MaximumValue << ";";
		}
	}

	uint32 player_flags = m_uint32Values[PLAYER_FLAGS];
	{
		// Remove un-needed and problematic player flags from being saved :p
		if(player_flags & PLAYER_FLAG_PARTY_LEADER)
			player_flags &= ~PLAYER_FLAG_PARTY_LEADER;
		if(player_flags & PLAYER_FLAG_AFK)
			player_flags &= ~PLAYER_FLAG_AFK;
		if(player_flags & PLAYER_FLAG_DND)
			player_flags &= ~PLAYER_FLAG_DND;
		if(player_flags & PLAYER_FLAG_GM)
			player_flags &= ~PLAYER_FLAG_GM;
		if(player_flags & PLAYER_FLAG_PVP_TOGGLE)
			player_flags &= ~PLAYER_FLAG_PVP_TOGGLE;
		if(player_flags & PLAYER_FLAG_FREE_FOR_ALL_PVP)
			player_flags &= ~PLAYER_FLAG_FREE_FOR_ALL_PVP;
	}

	ss << "', "
	<< m_uint32Values[PLAYER_FIELD_WATCHED_FACTION_INDEX] << ","
	<< m_uint32Values[PLAYER_CHOSEN_TITLE] << ","
	<< m_uint32Values[PLAYER__FIELD_KNOWN_TITLES] << ","
	<< m_uint32Values[PLAYER_FIELD_COINAGE] << ","
	<< m_uint32Values[PLAYER_AMMO_ID] << ","
	<< m_uint32Values[PLAYER_CHARACTER_POINTS2] << ",";

	if (m_uint32Values[PLAYER_CHARACTER_POINTS1] > 61 &&  ! GetSession()->HasGMPermissions())
            SetUInt32Value(PLAYER_CHARACTER_POINTS1, 61);
	ss << m_uint32Values[PLAYER_CHARACTER_POINTS1] << ","
	<< load_health << ","
	<< load_mana << ","
	<< uint32(GetPVPRank()) << ","
	<< m_uint32Values[PLAYER_BYTES] << ","
	<< m_uint32Values[PLAYER_BYTES_2] << ","
	<< player_flags << ","
	<< m_uint32Values[PLAYER_FIELD_BYTES] << ",";

	if( m_sunyou_instance )
	{
		// if its an arena, save the entry coords instead
		ss << m_bgEntryPointX << ", ";
		ss << m_bgEntryPointY << ", ";
		ss << m_bgEntryPointZ << ", ";
		ss << m_bgEntryPointO << ", ";
		ss << m_bgEntryPointMap << ", ";
	}
	else
	{
		// save the normal position
		ss << m_position.x << ", "
			<< m_position.y << ", "
			<< m_position.z << ", "
			<< m_position.o << ", "
			<< m_mapId << ", ";
	}

	ss << GetSession()->m_muted << ", '";
	
	std::string temp;
	SaveMapToString( m_Titles, temp );
	ss << temp;

	ss << "', "

	<< GetSession()->m_baned << ", '"
	<< CharacterDatabase.EscapeString(GetSession()->m_banreason) << "', "
	<< (uint32)UNIXTIME << ",";

	//online state
	if(GetSession()->_loggingOut)
	{
		ss << "0,";
	}else
	{
		ss << "1,";
	}

	ss
	<< m_bind_pos_x			 << ", "
	<< m_bind_pos_y			 << ", "
	<< m_bind_pos_z			 << ", "
	<< m_bind_mapid			 << ", "
	<< m_bind_zoneid			<< ", "

	<< uint32(m_isResting)	  << ", "
	<< uint32(m_restState)	  << ", "
	<< uint32(m_restAmount)	 << ", '"

	<< uint32(m_playedtime[0])  << " "
	<< uint32(m_playedtime[1])  << " "
	<< uint32(playedt)		  << " ', "
	<< GetUInt32Value( UNIT_FIELD_DEATH_STATE )	 << ", "

	<< m_talentresettimes	   << ", "
	<< m_FirstLogin			 << ", "
	<< rename_pending
	<< "," << /*m_arenaPoints*/m_playtimeTriggerId << ","
		<< (uint32)m_StableSlotCount << ",";

	// instances
	if( in_arena )
	{
		ss << m_bgEntryPointInstance << ", ";
	}
	else
	{
		ss << m_instanceId		   << ", ";
	}

	ss << m_bgEntryPointMap	  << ", "
	<< m_bgEntryPointX		<< ", "
	<< m_bgEntryPointY		<< ", "
	<< m_bgEntryPointZ		<< ", "
	<< m_bgEntryPointO		<< ", "
	<< m_bgEntryPointInstance << ", ";

	// taxi
	if(m_onTaxi&&m_CurrentTaxiPath) {
		ss << m_CurrentTaxiPath->GetID() << ", ";
		ss << lastNode << ", ";
		ss << GetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID);
	} else {
		ss << "0, 0, 0";
	}

	ss << "," << (m_CurrentTransporter ? m_CurrentTransporter->GetEntry() : (uint32)0);
	ss << ",'" << m_TransporterX << "','" << m_TransporterY << "','" << m_TransporterZ << "'";
	ss << ",'";

	// Dump spell data to stringstream
	SpellSet::iterator spellItr = mSpells.begin();
	
	for(; spellItr != mSpells.end(); ++spellItr)
	{
		uint32 entry = *spellItr;
		if( m_is_add_extra_spells )
		{
			bool skip = false;
			for( int i = 0; i < 10; ++i )
				if( entry == m_extra_spells[i] )
				{
					skip = true;
					break;
				}
			if( skip )
				continue;
		}
		ss << entry << ",";
	}
	ss << "','";
	// Dump deleted spell data to stringstream
	spellItr = mDeletedSpells.begin();
	for(; spellItr != mDeletedSpells.end(); ++spellItr)
	{
		ss << uint32(*spellItr) << ",";
	}

	ss << "','";
	// Dump reputation data
	ss << "','";

	// Add player action bars
	if( m_is_add_extra_spells )
	{
		for(uint32 i = 0; i < 120; ++i)
		{
			ss << uint32(m_oldactionbar[i].Action) << ","
				<< uint32(m_oldactionbar[i].Misc) << ","
				<< uint32(m_oldactionbar[i].Type) << ",";
		}
	}
	else
	{
		for(uint32 i = 0; i < 120; ++i)
		{
			ss << uint32(mActions[i].Action) << ","
				<< uint32(mActions[i].Misc) << ","
				<< uint32(mActions[i].Type) << ",";
		}
	}
	ss << "','";
	SaveAuras(ss, periodic_save);
	ss << "','";

	// Add player finished quests
	set<uint32>::iterator fq = m_finishedQuests.begin();
	for(; fq != m_finishedQuests.end(); ++fq)
	{
		ss << (*fq) << ",";
	}

	ss << "', ";
	ss << m_honorRolloverTime << ", ";
	ss << m_killsToday << ", " << m_killsYesterday << ", " << GetUInt32Value( PLAYER_FIELD_KILL_COUNT ) << ", ";
	ss << GetUInt32Value( PLAYER_FIELD_BE_KILL_COUNT ) << ", " << GetUInt32Value( PLAYER_FIELD_TOTAL_HONOR ) << ", ";
	ss << GetUInt32Value( PLAYER_FIELD_HONOR_CURRENCY ) << ", ";
	//ss << iInstanceType << ", 0)";
    ss << iInstanceType << ", 0, ";
	ss << m_leave_guild_date << ", " << m_uint32Values[PLAYER_FIELD_TEACHER] << ", " << m_uint32Values[PLAYER_FIELD_AVATAR_SETTING] << ", " << m_uint32Values[PLAYER_FIELD_ENCHANT_TEACHER_CHARGES] << "," ;
	string tempname = this->GetName();
	std::transform( tempname.begin(), tempname.end(), tempname.begin(), ::toupper );
	ss << crc32((const unsigned char*)tempname.c_str(), tempname.length()) <<  "," << GetUInt32Value(PLAYER_VISIBLE_ITEM_SHOW_SHIZHUANG);

	std::string instance_enter_string, battle_ground_win_string, item_obtain_string;
	SaveMapToString( m_playerInfo->instance_enter, instance_enter_string );
	SaveMapToString( m_playerInfo->battleground_win, battle_ground_win_string );
	SaveMapToString( m_playerInfo->item_obtain, item_obtain_string );
	ss << ",'" << instance_enter_string << "','" << battle_ground_win_string << "','" << item_obtain_string << "', "<< m_HealthBeforeEnterInstance<<","<<m_ManaBeforeEnterInstance<<")";

	buf->AddQueryStr(ss.str());

	//Save Other related player stuff

	// Inventory
	 GetItemInterface()->mSaveItemsToDatabase(false, buf);

	// save quest progress
	_SaveQuestLogEntry(buf);

	// Tutorials
	_SaveTutorials(buf);

	// GM Ticket
	objmgr.SaveGMTicket(GetGUID(), buf);

	// Cooldown Items
	_SavePlayerCooldowns( buf );

	_SaveRefineItemContainer( buf );

	_SavePet(buf);
	_SavePetSpells(buf);

	char sql_arena_history[512] = { 0 };

	sprintf( sql_arena_history, "replace into player_arena_history values(%u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u)",
		GetLowGUID(), m_playerInfo->m_arena_win[0], m_playerInfo->m_arena_win[1], m_playerInfo->m_arena_win[2], m_playerInfo->m_arena_win[3], m_playerInfo->m_arena_win[4], 
		m_playerInfo->m_arena_lose[0], m_playerInfo->m_arena_lose[1], m_playerInfo->m_arena_lose[2], m_playerInfo->m_arena_lose[3], m_playerInfo->m_arena_lose[4],
		m_playerInfo->m_arena_win_lv80[0], m_playerInfo->m_arena_win_lv80[1], m_playerInfo->m_arena_win_lv80[2], m_playerInfo->m_arena_win_lv80[3], m_playerInfo->m_arena_win_lv80[4], 
		m_playerInfo->m_arena_lose_lv80[0], m_playerInfo->m_arena_lose_lv80[1], m_playerInfo->m_arena_lose_lv80[2], m_playerInfo->m_arena_lose_lv80[3], m_playerInfo->m_arena_lose_lv80[4] );

	buf->AddQueryStr( sql_arena_history );

	sWorld.ExecuteSqlToDBServer( buf );
}

void Player::_SaveQuestLogEntry(QueryBuffer * buf)
{
	/*
	for(std::set<uint32>::iterator itr = m_removequests.begin(); itr != m_removequests.end(); ++itr)
	{
		buf->AddQuery("DELETE FROM questlog WHERE player_guid=%u AND quest_id=%u", GetLowGUID(), (*itr));
	}
	*/

	//m_removequests.clear();
	buf->AddQuery( "DELETE FROM questlog WHERE player_guid=%u", GetLowGUID() );

	for(int i = 0; i < QUEST_MAX_COUNT; ++i)
	{
		if(m_questlog[i] != NULL)
			m_questlog[i]->SaveToDB(buf);
	}
}

bool Player::canCast(SpellEntry *m_spellInfo)
{
	if (m_spellInfo->EquippedItemClass != 0)
	{
		if(this->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_MAINHAND))
		{
			if((int32)this->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_MAINHAND)->GetProto()->Class == m_spellInfo->EquippedItemClass)
			{
				if (m_spellInfo->EquippedItemSubClass != 0)
				{
					if (m_spellInfo->EquippedItemSubClass != 173555 && m_spellInfo->EquippedItemSubClass != 96 && m_spellInfo->EquippedItemSubClass != 262156)
					{
						if (pow(2.0,(this->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_MAINHAND)->GetProto()->SubClass) != m_spellInfo->EquippedItemSubClass))
							return false;
					}
				}
			}
		}
		else if(m_spellInfo->EquippedItemSubClass == 173555)
			return false;

		/*if (this->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_RANGED))
		{
			if((int32)this->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_RANGED)->GetProto()->Class == m_spellInfo->EquippedItemClass)
			{
				if (m_spellInfo->EquippedItemSubClass != 0)
				{
					if (m_spellInfo->EquippedItemSubClass != 173555 && m_spellInfo->EquippedItemSubClass != 96 && m_spellInfo->EquippedItemSubClass != 262156)
					{
						if (pow(2.0,(this->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_RANGED)->GetProto()->SubClass) != m_spellInfo->EquippedItemSubClass))							return false;
					}
				}
			}
		}*/
		else if
			(m_spellInfo->EquippedItemSubClass == 262156)
			return false;
	}
	return true;
}

void Player::RemovePendingPlayer()
{
	if(m_session)
	{
		uint8 respons = 0x42;		// CHAR_LOGIN_NO_CHARACTER
		//m_session->OutPacket(SMSG_CHARACTER_LOGIN_FAILED, 1, &respons);
		m_session->m_loggingInPlayer = NULL;
		m_session->Disconnect();
	}
	else
	{
		ok_to_remove = true;
		delete this;
	}	
}

bool Player::LoadFromDB(uint32 guid)
{
	// success
	SQLCallbackBase* pCallBack = new SQLClassCallbackP0<Player>(this, &Player::LoadFromDBProc);
	AsyncQuery * q = new AsyncQuery( pCallBack );
	//q->AddQuery("SELECT * FROM characters WHERE guid=%u AND forced_rename_pending = 0",guid);
	q->AddQuery("SELECT * FROM characters WHERE guid=%u",guid);
	q->AddQuery("SELECT * FROM tutorials WHERE playerId=%u",guid);
	q->AddQuery("SELECT cooldown_type, cooldown_misc, cooldown_expire_time, cooldown_spellid, cooldown_itemid FROM playercooldowns WHERE player_guid=%u", guid);
	q->AddQuery("SELECT * FROM questlog WHERE player_guid=%u",guid);
	q->AddQuery("SELECT * FROM playeritems WHERE ownerguid=%u ORDER BY containerslot DESC", guid);

	q->AddQuery("SELECT * FROM playerpets WHERE ownerguid=%u ORDER BY petnumber", guid);
	q->AddQuery("SELECT * FROM playersummonspells where ownerguid=%u ORDER BY entryid", guid);
	q->AddQuery("SELECT * FROM mailbox WHERE player_guid = %u", guid);

	// social
	q->AddQuery("SELECT friend_guid, friend_name, note FROM social_friends WHERE character_guid = %u", guid);
	q->AddQuery("SELECT character_guid FROM social_friends WHERE friend_guid = %u", guid);
	q->AddQuery("SELECT ignore_guid FROM social_ignores WHERE character_guid = %u", guid);
	q->AddQuery( "select * from player_arena_history where guid = %u", guid );
	if( !g_crosssrvmgr->m_isInstanceSrv )
		q->AddQuery( "select * from playertitles where player_guid = %u and time_left > %u", guid, (uint32)UNIXTIME );
	// queue it!
	m_uint32Values[OBJECT_FIELD_GUID] = guid;
	_db->QueueAsyncQuery(q);

	if( !g_crosssrvmgr->m_isInstanceSrv && _db != &CharacterDatabase )
	{
		SQLCallbackBase* pCallBack3 = new SQLClassCallbackP0<Player>(this, &Player::LoadMailBoxProc);
		AsyncQuery* q3 = new AsyncQuery( pCallBack3 );
		q3->AddQuery("SELECT * FROM mailbox WHERE player_guid = %u", guid);
		q3->AddQuery( "select * from tutorials where playerId = %u", guid );
		CharacterDatabase.QueueAsyncQuery( q3 );
	}

	SQLCallbackBase* pCallBack2 = new SQLClassCallbackP0<Player>( this, &Player::LoadRechargeableCardInfoProc );
	AsyncQuery * q2 = new AsyncQuery( pCallBack2 );
	q2->AddQuery( "select card_serial, remain_points, date from acc_point_card where acc_id = %u", m_session->GetAccountId() );

	RealmDatabase.QueueAsyncQuery( q2 );

	/*
	SQLCallbackBase* pCallBack3 = new SQLClassCallbackP0<Player>( this, &Player::LoadCreditCardInfoProc );
	AsyncQuery * q3 = new AsyncQuery( pCallBack3 );
	q3->AddQuery( "select * from credit_card where acc_id = %u", m_session->GetAccountId() );
	RealmDatabase.QueueAsyncQuery( q3 );
	*/

	//SQLCallbackBase* pCallBack3 = new SQLClassCallbackP0<Player>( this, &Player::LoadArenaHistoryProc );
	//AsyncQuery * q3 = new AsyncQuery( pCallBack3 );
	//q3->AddQuery( "select * from player_arena_history where guid = %u", guid );

	//CharacterDatabase.QueueAsyncQuery( q3 );	

	return true;
}

void Player::LoadFeeInfoProc( QueryResultVector& results )
{
	if( !IsValid() ) return;

	//assert( 0 );
	MyLog::log->error("assert LoadFeeInfoProc direct return");
	return;
	uint32 guid = m_points;

	if( GetSession() == NULL || results.size() == 0 )		// should have 8 queryresults for aplayer load.
	{
		RemovePendingPlayer();
		return;
	}

	QueryResult *result = results[0].result;
	if(!result)
	{
		MyLog::log->error("Player login query failed., guid %u", GetLowGUID());
		RemovePendingPlayer();
		return;
	}

	Field *fields = result->Fetch();
	m_points = fields[0].GetInt32();
	m_expire = fields[1].GetUInt32();
	if( UNIXTIME >= m_expire )
	{
		if( m_points <= 0 )
		{
			MyLog::log->error("account expire, guid %u", GetLowGUID());
			RemovePendingPlayer();
			return;
		}
	}
}

void Player::LoadRechargeableCardInfoProc( QueryResultVector& results )
{
	if( !IsValid() ) return;

	if( !m_session )
	{
		RemovePendingPlayer();
		return;
	}
	if( results.size()  == 0 )
	{
		//MyLog::log->error( "load rechargeable card info failed! guid:%u", GetLowGUID() );
		return;
	}
	QueryResult *result = results[0].result;
	if( !result )
	{
		//MyLog::log->error( "load rechargeable card info failed! guid:%u", GetLowGUID() );
		return;
	}

	int n = 0;
	do
	{
		Field* f = result->Fetch();
		if( f )
		{
			rechargeable_card rc;
			rc.card_serial = f[0].GetString();
			rc.remain_points = f[1].GetInt32();
			if( rc.remain_points < 0 )
				rc.remain_points = 0;
			int date = f[2].GetUInt32();
			m_mapRechargeableCards.insert( std::make_pair( date, rc ) );
			n += rc.remain_points;
		}
	}
	while( result->NextRow() );
	SetUInt32Value( PLAYER_FIELD_YUANBAO, n );
	//if( m_playerInfo )
	//	m_playerInfo->yuanbao = GetUInt32Value(PLAYER_FIELD_YUANBAO);
}

void Player::LoadCreditCardInfoProc( QueryResultVector& results )
{
	if( !IsValid() ) return;

	if( !m_session )
	{
		RemovePendingPlayer();
		return;
	}
	if( results.size()  == 0 )
	{
		//MyLog::log->error( "load credit card info failed! guid:%u", GetLowGUID() );
		return;
	}
	QueryResult *result = results[0].result;
	if( !result )
	{
		//MyLog::log->error( "load credit card info failed! guid:%u", GetLowGUID() );
		return;
	}
	Field* f = result->Fetch();
	if( f )
	{
		if( f[0].GetUInt32() != m_session->GetAccountID() )
			return;

		SetUInt32Value( PLAYER_FIELD_CREDIT_AVAILABLE, f[1].GetUInt32() );
		SetUInt32Value( PLAYER_FIELD_LINE_OF_CREDIT, f[2].GetUInt32() );
		SetUInt32Value( PLAYER_FIELD_CREDIT_RESTORE, f[3].GetUInt32() );
		SetUInt32Value( PLAYER_FIELD_CREDIT_APPLY_DATE, f[4].GetUInt32() );
		SetUInt32Value( PLAYER_FIELD_CREDIT_BILL_DATE, f[5].GetUInt32() );
		SetUInt32Value( PLAYER_FIELD_CREDIT_RESTORE_MATURITY, f[6].GetUInt32() );
		SetUInt32Value( PLAYER_FIELD_CREDIT_BANNED, f[8].GetUInt32() );
	}
}

int Player::GetCardPoints() const
{
	int n = 0;
	for( std::multimap<int, rechargeable_card>::const_iterator it = m_mapRechargeableCards.begin(); it != m_mapRechargeableCards.end(); ++it )
	{
		const rechargeable_card& rc = it->second;
		n += rc.remain_points;
	}
	return n;
}

bool Player::SpendPoints( int n, const char* description )
{
	if( n <= 0 )
		return false;
	if( !m_session )
		return false;
	int tp = GetCardPoints();
	if( tp < n )
		return false;

	scoped_sql_transaction_proc sstp( &RealmDatabase );
	for( std::multimap<int, rechargeable_card>::iterator it = m_mapRechargeableCards.begin(); it != m_mapRechargeableCards.end(); ++it )
	{
		rechargeable_card& rc = it->second;
		if( rc.remain_points <= 0 )
			continue;
		if( rc.remain_points >= n )
		{
			if( !_DecreaseCardPoints( rc.card_serial, n, description ) )
				return false;
			rc.remain_points -= n;

			sstp.success();
			ModUnsigned32Value( PLAYER_FIELD_YUANBAO, -n );
			//if( m_playerInfo )
			//	m_playerInfo->yuanbao = GetUInt32Value(PLAYER_FIELD_YUANBAO);
			return true;
		}
		else
		{
			if( !_DecreaseCardPoints( rc.card_serial, rc.remain_points, description ) )
				return false;
			n -= rc.remain_points;
			rc.remain_points = 0;
		}
	}

	return false;
}

bool Player::SpendPointsNotUseLock( int n, const char* description )
{
	if( n <= 0 )
		return false;
	if( !m_session )
		return false;
	int tp = GetCardPoints();
	if( tp < n )
		return false;

	for( std::multimap<int, rechargeable_card>::iterator it = m_mapRechargeableCards.begin(); it != m_mapRechargeableCards.end(); ++it )
	{
		rechargeable_card& rc = it->second;
		if( rc.remain_points <= 0 )
			continue;
		if( rc.remain_points >= n )
		{
			if( !_DecreaseCardPoints( rc.card_serial, n, description ) )
				return false;
			rc.remain_points -= n;

			ModUnsigned32Value( PLAYER_FIELD_YUANBAO, -n );
			//if( m_playerInfo )
			//	m_playerInfo->yuanbao = GetUInt32Value(PLAYER_FIELD_YUANBAO);
			return true;
		}
		else
		{
			if( !_DecreaseCardPoints( rc.card_serial, rc.remain_points, description ) )
				return false;
			n -= rc.remain_points;
			rc.remain_points = 0;
		}
	}

	return false;
}

bool Player::AddCardPoints( int n, add_card_type type /* = ADD_CARD_GM_DONATE */ )
{
	char sql[512] = { 0 };
	char temp[128] = { 0 };
	sprintf( temp, "GAME_%u", m_session->GetAccountID() );

	char system_message[512] = { 0 };
	switch( type )
	{
	case ADD_CARD_GM_DONATE:
		//sprintf( system_message, "恭喜您，得到了系统赠送的%u个元宝", n );
		strcpy( system_message, build_language_string( BuildString_PlayerAddPointDonate, quick_itoa(n) ) );
		break;
	case ADD_CARD_TRADE:
		//sprintf( system_message, "您得到了%u个元宝", n );
		strcpy( system_message, build_language_string( BuildString_PlayerAddPointTrade, quick_itoa(n) ) );
		break;
	case ADD_CARD_CREDIT:
		//sprintf( system_message, "恭喜您，成功从信用卡账户转帐到基础账户:%u个元宝", n );
		strcpy( system_message, build_language_string( BuildString_PlayerAddPointCredit, quick_itoa(n) ) );
		break;
	default:
		break;
	}
	for( std::multimap<int, rechargeable_card>::iterator it = m_mapRechargeableCards.begin(); it != m_mapRechargeableCards.end(); ++it )
	{
		rechargeable_card& rc = it->second;
		if( rc.card_serial == temp )
		{
			sprintf( sql, "update acc_point_card set init_points = init_points + %d, remain_points = remain_points + %d where acc_id = %u and card_serial = '%s'", n, n, m_session->GetAccountID(), temp );
			if( RealmDatabase.WaitExecute( sql ) )
			{
				rc.remain_points += n;
				ModUnsigned32Value( PLAYER_FIELD_YUANBAO, n );
				//if( m_playerInfo )
				//	m_playerInfo->yuanbao = GetUInt32Value(PLAYER_FIELD_YUANBAO);

				if( system_message[0] != 0 )
					m_session->SystemMessage( system_message );
				return true;
			}
		}
	}


	sprintf( sql, "insert into acc_point_card (acc_id, card_serial, type, init_points, remain_points, date, description) values(%u, '%s', 1001, %u, %u, %u, '%s')",
				m_session->GetAccountID(), temp, n, n, (uint32)UNIXTIME, "GM donate" );

	if( RealmDatabase.WaitExecute( sql ) )
	{
		rechargeable_card rc;
		rc.card_serial = temp;
		rc.remain_points = n;
		m_mapRechargeableCards.insert( std::make_pair( (int)UNIXTIME, rc ) );

		ModUnsigned32Value( PLAYER_FIELD_YUANBAO, n );
		//if( m_playerInfo )
		//	m_playerInfo->yuanbao = GetUInt32Value(PLAYER_FIELD_YUANBAO);
		if( system_message[0] != 0 )
			m_session->SystemMessage( system_message );
		return true;
	}
	else
		return false;
}

int Player::GetCreditAvailable()
{
	return GetUInt32Value( PLAYER_FIELD_CREDIT_AVAILABLE );
}

void Player::CreditApply()
{
	if( GetUInt32Value( PLAYER_FIELD_CREDIT_BANNED ) )
		return;

	MSG_S2C::stCreditApplyAck ack;
	if( GetUInt32Value( PLAYER_FIELD_LINE_OF_CREDIT ) > 0 )
	{
		ack.result = MSG_S2C::stCreditApplyAck::CREDIT_APPLY_ALREADY_HAVE;
	}
	else if( getLevel() < 80 )
	{
		ack.result = MSG_S2C::stCreditApplyAck::CREDIT_APPLY_LEVEL_LESS_80;
	}
	else if( GetUInt32Value( PLAYER_FIELD_COINAGE ) < 100 * 10000 )
	{
		ack.result = MSG_S2C::stCreditApplyAck::CREDIT_APPLY_NOT_ENOUGH_GOLD;
	}
	else
	{
		char sql[256] = { 0 };
		sprintf( sql, "insert into credit_card values(%u, %u, %u, %u, %u, %u, %u, 0, 0)",
			m_session->GetAccountID(), 1000, 1000, 0, (uint32)UNIXTIME, (uint32)UNIXTIME + TIME_DAY * 15, (uint32)UNIXTIME + TIME_DAY * 30 );

		if( RealmDatabase.WaitExecute( sql ) )
		{
			ack.result = MSG_S2C::stCreditApplyAck::CREDIT_APPLY_SUCCESS;

			SetUInt32Value( PLAYER_FIELD_CREDIT_AVAILABLE, 1000 );
			SetUInt32Value( PLAYER_FIELD_LINE_OF_CREDIT, 1000 );
			SetUInt32Value( PLAYER_FIELD_CREDIT_APPLY_DATE, (uint32)UNIXTIME );
			SetUInt32Value( PLAYER_FIELD_CREDIT_BILL_DATE, (uint32)UNIXTIME + TIME_DAY * 15 );
			SetUInt32Value( PLAYER_FIELD_CREDIT_RESTORE_MATURITY, (uint32)UNIXTIME + TIME_DAY * 30 );
		}
		else
		{
			ack.result = MSG_S2C::stCreditApplyAck::CREDIT_APPLY_SYSTEM_BUSY;
		}
	}
	m_session->SendPacket( ack );
}

bool Player::RestoreCredit( uint32 n )
{
	if( n == 0 )
		return false;

	uint32 r = GetUInt32Value( PLAYER_FIELD_CREDIT_RESTORE );

	char sql[256] = { 0 };
	uint32 upgrade = 0;
	if( r && n >= r )
	{
		if( r > 10000 )
		{
			upgrade = r / 10;

			sprintf( sql, "update credit_card set available = available + %u, line_of_credit = line_of_credit + %u, is_restored = 1, restore_points = 0, restore_maturity = restore_maturity + %u where acc_id = %u",
					n, upgrade, m_session->GetAccountID(), TIME_DAY * 30 );

			char txt[512] = { 0 };
			int y, m, d, h, min, s;
			convert_unix_time( (uint32)UNIXTIME, &y, &m, &d, &h, &min, &s );
			sprintf( txt, "亲爱的信用卡用户,由于您在规定期限内足额归还帐单提示所欠金额,系统已自动提升您的信用额度为:[%u + %u = %u]个元宝<br>日期:[%d-%d-%d %d:%d:%d]",
				GetUInt32Value( PLAYER_FIELD_LINE_OF_CREDIT ), upgrade, GetUInt32Value( PLAYER_FIELD_LINE_OF_CREDIT ) + upgrade, y, m, d, h, min, s );

			MailMessage msg;
			msg.subject = "君·天下信用卡额度提升通知";
			msg.body = txt;
			msg.stationary = 0;
			msg.delivery_time = (uint32)UNIXTIME;
			msg.cod = 0;
			msg.money = 0;

			msg.sender_guid = 0;
			msg.expire_time = 0;

			msg.copy_made = false;
			msg.read_flag = false;
			msg.deleted_flag = false;
			msg.message_type = 0;
			msg.player_guid = GetGUID();
			sMailSystem.DeliverMessage( GetGUID(), &msg);
			GetSession()->SendNotification( txt );
		}
		else
		{
			sprintf( sql, "update credit_card set available = available + %u, is_restored = 1, restore_points = 0, restore_maturity = restore_maturity + %u where acc_id = %u",
				n, m_session->GetAccountID(), TIME_DAY * 30 );
		}
	}
	else
		sprintf( sql, "update credit_card set available = available + %u, restore_points = restore_points - %u where acc_id = %u", n, n, m_session->GetAccountID() );

	if( RealmDatabase.WaitExecute( sql ) )
	{
		ModUnsigned32Value( PLAYER_FIELD_CREDIT_AVAILABLE, n );

		ModUnsigned32Value( PLAYER_FIELD_CREDIT_RESTORE, -(int)n );

		if( r && n >= r )
		{
			if( upgrade > 0 )
				ModUnsigned32Value( PLAYER_FIELD_LINE_OF_CREDIT, (int)upgrade );
			SetUInt32Value( PLAYER_FIELD_CREDIT_RESTORE_MATURITY, GetUInt32Value( PLAYER_FIELD_CREDIT_RESTORE_MATURITY ) + TIME_DAY * 30 );
		}
		return true;
	}
	else
		return false;
}

bool Player::UseCredit( uint32 n )
{
	if( GetCreditAvailable() < n )
		return false;

	char sql[256] = { 0 };
	sprintf( sql, "update credit_card set available = available - %u where acc_id = %u", n, m_session->GetAccountID() );
	if( RealmDatabase.WaitExecute( sql ) )
	{
		ModUnsigned32Value( PLAYER_FIELD_CREDIT_AVAILABLE, -(int)n );
		return true;
	}
	else
		return false;
}

void Player::QueryCreditHistory()
{
	//if( GetUInt32Value( PLAYER_FIELD_CREDIT_BANNED ) )
	//	return;

	SQLCallbackBase* pCallBack = new SQLClassCallbackP0<Player>( this, &Player::QueryCreditHistoryProc );
	AsyncQuery * q = new AsyncQuery( pCallBack );
	q->AddQuery( "select * from credit_card_history where acc_id = %u", m_session->GetAccountId() );
	RealmDatabase.QueueAsyncQuery( q );
}

void Player::QueryCreditHistoryProc( QueryResultVector& v )
{
	if( !IsValid() || !m_session )
		return;

	MSG_S2C::stCreditHistoryAck ack;
	for( size_t i = 0; i < v.size(); ++i )
	{
		QueryResult* qr = v[i].result;
		if( qr )
		{
			do
			{
				MSG_S2C::stCreditHistoryAck::_history h;
				Field* f = qr->Fetch();
				h.yuanbao = f[1].GetUInt32();
				h.datetime = f[2].GetUInt32();
				ack.v.push_back( h );
			}
			while( qr->NextRow() );
		}
	}
	m_session->SendPacket( ack );
}

bool Player::_DecreaseCardPoints( const std::string& card_serial, int decrease_points, const char* description )
{
	if( !RealmDatabase.TransactionExecute( "update acc_point_card set remain_points = remain_points - %d where card_serial = '%s'",
							decrease_points, card_serial.c_str() ) )
	{
		return false;
	}
	char sql[10240] = { 0 };
	sprintf( sql, "insert into expense_record (acc_id, acc_name, role_id, role_name, date, card_serial, expense_points, description) values(%u, '%s', %u, '%s', %u, '%s', %u, '%s')",
							m_session->GetAccountId(), m_session->GetAccountNameS(), GetLowGUID(), GetName(), (uint32)UNIXTIME, card_serial.c_str(), decrease_points, description );
	if( !RealmDatabase.TransactionExecute( sql ) )
		return false;

	return true;
}

bool Player::IsInRaid() const
{
	if( m_sunyou_instance && m_sunyou_instance->GetCategory() == INSTANCE_CATEGORY_RAID )
		return true;
	else
		return false;
}

void Player::LoadMailBoxProc( QueryResultVector& results )
{
	if( !IsValid() ) return;

	if( results[0].result )
		m_mailBox->Load(results[0].result);

	_LoadTutorials( results[1].result );
}

void Player::LoadFromDBProc(QueryResultVector & results)
{
	if( !IsValid() ) return;

	uint32 field_index = 2;
#define get_next_field fields[field_index++]

	if(GetSession() == NULL || results.size() < 8)		// should have 8 queryresults for aplayer load.
	{
		RemovePendingPlayer();
		return;
	}
	memset( m_queuedmapid, 0, sizeof( m_queuedmapid ) );

	QueryResult *result = results[0].result;
	if(!result)
	{
		MyLog::log->error("Player login query failed., guid %u\n", GetLowGUID());
		RemovePendingPlayer();
		return;
	}

	Field *fields = result->Fetch();

	if(fields[1].GetUInt32() != m_session->GetAccountId())
	{
		//sCheatLog.writefromsession(m_session, "player tried to load character not belonging to them (guid %u, on account %u)",
		//	fields[0].GetUInt32(), fields[1].GetUInt32());
		MyLog::log->alert("account data not match, maybe two severs online");
		RemovePendingPlayer();
		return;
	}

	uint32 banned = fields[32].GetUInt32();
	if(banned && (banned < 100 || banned > (uint32)UNIXTIME))
	{
		char szMsg[1024];
		sprintf(szMsg, "ban character. expire:%s", ConvertTimeStampToDataTime(banned).c_str());
		
		MSG_S2C::stMessageBox MsgBox;
		MsgBox.message = szMsg;
		m_session->SendPacket(MsgBox);
		RemovePendingPlayer();
		return;
	}

	// Load name
	m_name = get_next_field.GetString();

	// Load race/class from fields
	setRace(get_next_field.GetUInt8());
	setClass(get_next_field.GetUInt8());
	setGender(get_next_field.GetUInt8());
	uint32 cfaction = get_next_field.GetUInt32();

	// set race dbc
	myRace = dbcCharRace.LookupEntry(getRace());
	myClass = dbcCharClass.LookupEntry(getClass());
	if(!myClass || !myRace)
	{
		// bad character
		printf("guid %u failed to login, no race or class dbc found. (race %u class %u)\n", (unsigned int)GetLowGUID(), (unsigned int)getRace(), (unsigned int)getClass());
		RemovePendingPlayer();
		return;
	}

	m_bgTeam = 0;
	m_team = getRace();

	SetNoseLevel();

	// set power type
	SetPowerType(myClass->power_type);

	// obtain player create info
	info = objmgr.GetPlayerCreateInfo(getRace(), getClass());
	if(!info)
	{
		MyLog::log->error("assert not found playercreateinfo race[%d] class[%d]", getRace(), getClass());
		return;
	}
	//assert(info);

	// set level
	m_uint32Values[UNIT_FIELD_LEVEL] = get_next_field.GetUInt32();
	/*if(m_uint32Values[UNIT_FIELD_LEVEL] > 70)
		m_uint32Values[UNIT_FIELD_LEVEL] = 70;*/

	// obtain level/stats information
	lvlinfo = objmgr.GetLevelInfo(getRace(), getClass(), getLevel());

	if(!lvlinfo)
	{
		printf("guid %u level %u class %u race %u levelinfo not found!\n", (unsigned int)GetLowGUID(), (unsigned int)getLevel(), (unsigned int)getClass(), (unsigned int)getRace());
		RemovePendingPlayer();
		return;
	}

	CalculateBaseStats();

	// set xp
	m_uint32Values[PLAYER_XP] = get_next_field.GetUInt32();

	// Process exploration data.
	uint32 Counter = 0;
	char * end;
	char * start = (char*)get_next_field.GetString();//buff;
	//while(Counter <64)
	//{
	//	end = strchr(start,',');
	//	if(!end)break;
	//	*end=0;
	//	SetUInt32Value(PLAYER_EXPLORED_ZONES_1 + Counter, atol(start));
	//	start = end +1;
	//	Counter++;
	//}

	// Process skill data.
	Counter = 0;
	start = (char*)get_next_field.GetString();//buff;

	// new format
	const ItemProf * prof;
	if(!strchr(start, ' ') && !strchr(start,';'))
	{
		/* no skills - reset to defaults */
		for(std::list<CreateInfo_SkillStruct>::iterator ss = info->skills.begin(); ss!=info->skills.end(); ss++)
		{
			if(ss->skillid && ss->currentval && ss->maxval && !::GetSpellForLanguage(ss->skillid))
				_AddSkillLine(ss->skillid, ss->currentval, ss->maxval);
		}
	}
	else
	{
		char * f = strdup(start);
		start = f;
		if(!strchr(start,';'))
		{
			/* old skill format.. :< */
			uint32 v1,v2,v3;
			PlayerSkill sk;
			for(;;)
			{
				end = strchr(start, ' ');
				if(!end)
					break;

				*end = 0;
				v1 = atol(start);
				start = end + 1;

				end = strchr(start, ' ');
				if(!end)
					break;

				*end = 0;
				v2 = atol(start);
				start = end + 1;

				end = strchr(start, ' ');
				if(!end)
					break;

				v3 = atol(start);
				start = end + 1;
				if(v1 & 0xffff)
				{
					sk.Reset(v1 & 0xffff);
					sk.CurrentValue = v2 & 0xffff;
					sk.MaximumValue = (v2 >> 16) & 0xffff;

					if( !sk.CurrentValue )
						sk.CurrentValue = 1;

					m_skills.insert( make_pair(sk.Skill->id, sk) );
				}
			}
		}
		else
		{
			uint32 v1,v2,v3;
			PlayerSkill sk;
			for(;;)
			{
				end = strchr(start, ';');
				if(!end)
					break;

				*end = 0;
				v1 = atol(start);
				start = end + 1;

				end = strchr(start, ';');
				if(!end)
					break;

				*end = 0;
				v2 = atol(start);
				start = end + 1;

				end = strchr(start, ';');
				if(!end)
					break;

				v3 = atol(start);
				start = end + 1;

				/* add the skill */
				if(v1)
				{
					sk.Reset(v1);
					sk.CurrentValue = v2;
					sk.MaximumValue = v3;

					if( !sk.CurrentValue )
						sk.CurrentValue = 1;

					m_skills.insert(make_pair(v1, sk));
				}
			}
		}
		free(f);
	}

	for(SkillMap::iterator itr = m_skills.begin(); itr != m_skills.end(); ++itr)
	{
		if(itr->first == SKILL_RIDING)
		{
			itr->second.CurrentValue = itr->second.MaximumValue;
		}

		prof = GetProficiencyBySkill(itr->first);
		if(prof)
		{
			if(prof->itemclass==4)
				armor_proficiency|=prof->subclass;
			else
				weapon_proficiency|=prof->subclass;
		}
	}


	_UpdateSkillFields();

	// set the rest of the shit
	uint32 PvPRanks[] = { PVPTITLE_NONE, PVPTITLE_PRIVATE, PVPTITLE_CORPORAL, PVPTITLE_SERGEANT, PVPTITLE_MASTER_SERGEANT, PVPTITLE_SERGEANT_MAJOR, PVPTITLE_KNIGHT, PVPTITLE_KNIGHT_LIEUTENANT, PVPTITLE_KNIGHT_CAPTAIN, PVPTITLE_KNIGHT_CHAMPION, PVPTITLE_LIEUTENANT_COMMANDER, PVPTITLE_COMMANDER, PVPTITLE_MARSHAL, PVPTITLE_FIELD_MARSHAL, PVPTITLE_GRAND_MARSHAL, PVPTITLE_SCOUT, PVPTITLE_GRUNT, PVPTITLE_HSERGEANT, PVPTITLE_SENIOR_SERGEANT, PVPTITLE_FIRST_SERGEANT, PVPTITLE_STONE_GUARD, PVPTITLE_BLOOD_GUARD, PVPTITLE_LEGIONNAIRE, PVPTITLE_CENTURION, PVPTITLE_CHAMPION, PVPTITLE_LIEUTENANT_GENERAL, PVPTITLE_GENERAL, PVPTITLE_WARLORD, PVPTITLE_HIGH_WARLORD };
	m_uint32Values[PLAYER_FIELD_WATCHED_FACTION_INDEX]  = get_next_field.GetUInt32();
	m_uint32Values[PLAYER_CHOSEN_TITLE]				 = get_next_field.GetUInt32();
	field_index++;
	m_uint32Values[PLAYER_FIELD_COINAGE]				= get_next_field.GetUInt32();
	m_uint32Values[PLAYER_AMMO_ID]					  = get_next_field.GetUInt32();
	m_uint32Values[PLAYER_CHARACTER_POINTS2]			= get_next_field.GetUInt32();
	m_uint32Values[PLAYER_CHARACTER_POINTS1]			= get_next_field.GetUInt32();
	load_health										 = get_next_field.GetUInt32();
	load_mana										   = get_next_field.GetUInt32();
	uint8 pvprank = get_next_field.GetUInt8();
	SetUInt32Value(PLAYER_BYTES, get_next_field.GetUInt32());
	SetUInt32Value(PLAYER_BYTES_2, get_next_field.GetUInt32());
	SetUInt32Value(PLAYER_BYTES_3, getGender() | (pvprank << 24));
	uint32 offset = 13 * GetTeam();
	SetUInt32Value(PLAYER__FIELD_KNOWN_TITLES, /*PvPRanks[GetPVPRank() + offset]*/0);
	SetUInt32Value(PLAYER_FLAGS, get_next_field.GetUInt32());
	SetUInt32Value(PLAYER_FIELD_BYTES, get_next_field.GetUInt32());
	//m_uint32Values[0x22]=(m_uint32Values[0x22]>0x46)?0x46:m_uint32Values[0x22];

	m_position.x										= get_next_field.GetFloat();
	m_position.y										= get_next_field.GetFloat();
	m_position.z										= get_next_field.GetFloat();
	m_position.o										= get_next_field.GetFloat();

	if( !m_mapId )
		m_mapId											 = get_next_field.GetUInt32();
	else
		++field_index;

	/*m_zoneId*/GetSession()->m_muted											= get_next_field.GetUInt32();

	// Calculate the base stats now they're all loaded
	for(uint32 i = 0; i < 6; ++i)
		CalcStat(i);

  //  for(uint32 x = PLAYER_SPELL_CRIT_PERCENTAGE1; x < PLAYER_SPELL_CRIT_PERCENTAGE06 + 1; ++x)
	///	SetFloatValue(x, 0.0f);

	for(uint32 x = PLAYER_FIELD_MOD_DAMAGE_DONE_PCT; x < PLAYER_FIELD_MOD_HEALING_DONE_POS; ++x)
		SetFloatValue(x, 0.0f);

	// Normal processing...
//	UpdateMaxSkills();
	UpdateStats();
	//UpdateChances();

	// Initialize 'normal' fields
	//SetFloatValue(OBJECT_FIELD_SCALE_X, ((getRace()==RACE_TAUREN)?1.3f:1.0f));
	SetFloatValue(OBJECT_FIELD_SCALE_X, 1.45f);
	SetFloatValue( PLAYER_FIELD_GRAVITY_RATE, 1.f );
	//SetUInt32Value(UNIT_FIELD_POWER2, 0);
// 	if(getClass() == CLASS_WARRIOR)
// 		SetShapeShift(FORM_BATTLESTANCE);

	SetUInt32Value(UNIT_FIELD_BYTES_2, (0x28 << 8) );
	SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PLAYER_CONTROLLED);
	SetFloatValue(UNIT_FIELD_BOUNDINGRADIUS, 0.388999998569489f );
	SetFloatValue(UNIT_FIELD_COMBATREACH, 1.5f );

	if(getRace() != 10)
	{
		SetUInt32Value(UNIT_FIELD_DISPLAYID, getGender() == GENDER_MALE ? info->displayId : info->displayId + 100000000/* + getGender()*/ );
		SetUInt32Value(UNIT_FIELD_NATIVEDISPLAYID, getGender() == GENDER_MALE ? info->displayId : info->displayId + 100000000/* + getGender()*/ );
	} else	{
		SetUInt32Value(UNIT_FIELD_DISPLAYID, info->displayId - getGender() );
		SetUInt32Value(UNIT_FIELD_NATIVEDISPLAYID, info->displayId - getGender() );
	}

	SetFloatValue(UNIT_MOD_CAST_SPEED, 1.0f);
	SetUInt32Value(PLAYER_FIELD_MAX_LEVEL, sWorld.m_levelCap);
	SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, info->factiontemplate);
	if(cfaction)
	{
		SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, info->factiontemplate);
		m_team = cfaction;
	}

	LoadMapFromString( m_Titles, get_next_field.GetString() );
	for( std::map<uint32, uint32>::iterator it = m_Titles.begin(); it != m_Titles.end(); )
	{
		std::map<uint32, uint32>::iterator it2 = it++;
		if( (it2->second > 0 && uint32(UNIXTIME) >= it2->second) || !PlayerTitleStorage.LookupEntry( it2->first ) )
			m_Titles.erase( it2 );
	}

	GetSession()->m_baned = get_next_field.GetUInt32(); //Character ban
	GetSession()->m_banreason = get_next_field.GetString();
	m_timeLogoff = get_next_field.GetUInt32();
	field_index++;

	m_resurrectPosX = m_bind_pos_x = get_next_field.GetFloat();
	m_resurrectPosY = m_bind_pos_y = get_next_field.GetFloat();
	m_resurrectPosZ = m_bind_pos_z = get_next_field.GetFloat();
	m_resurrectMapId = m_bind_mapid = get_next_field.GetUInt32();
	m_bind_zoneid = get_next_field.GetUInt32();


	m_isResting = get_next_field.GetUInt8();
	m_restState = get_next_field.GetUInt8();
	m_restAmount = get_next_field.GetUInt32();


	std::string tmpStr = get_next_field.GetString();
	m_playedtime[0] = (uint32)atoi((const char*)strtok((char*)tmpStr.c_str()," "));
	m_playedtime[1] = (uint32)atoi((const char*)strtok(NULL," "));

	//get_next_field;
	SetUInt32Value( UNIT_FIELD_DEATH_STATE, get_next_field.GetUInt32() );

	m_talentresettimes = get_next_field.GetUInt32();
	m_FirstLogin = get_next_field.GetBool();
	rename_pending = get_next_field.GetBool();
	/*m_arenaPoints*/m_playtimeTriggerId = get_next_field.GetUInt32();
	for(uint32 z = 0; z < NUM_CHARTER_TYPES; ++z)
		m_charters[z] = objmgr.GetCharterByGuid(GetGUID(), (CharterTypes)z);
	/*
	for(uint32 z = 0; z < NUM_ARENA_TEAM_TYPES; ++z)
	{
		m_arenaTeams[z] = objmgr.GetArenaTeamByGuid(GetLowGUID(), z);
		if(m_arenaTeams[z] != NULL)
		{
			SetUInt32Value(PLAYER_FIELD_ARENA_TEAM_INFO_1_1 + (z*6), m_arenaTeams[z]->m_id);
			if(m_arenaTeams[z]->m_leader == GetLowGUID())
				SetUInt32Value(PLAYER_FIELD_ARENA_TEAM_INFO_1_1 + (z*6) + 1, 0);
			else
				SetUInt32Value(PLAYER_FIELD_ARENA_TEAM_INFO_1_1 + (z*6) + 1, 1);
		}
	}
	*/
	m_StableSlotCount = get_next_field.GetUInt32();
	if( !m_instanceId )
		m_instanceId = get_next_field.GetUInt32();
	else
		++field_index;

	m_bgEntryPointMap = get_next_field.GetUInt32();
	m_bgEntryPointX = get_next_field.GetFloat();
	m_bgEntryPointY = get_next_field.GetFloat();
	m_bgEntryPointZ = get_next_field.GetFloat();
	m_bgEntryPointO = get_next_field.GetFloat();
	m_bgEntryPointInstance = get_next_field.GetUInt32();

	if( GetUInt32Value( UNIT_FIELD_DEATH_STATE ) != ALIVE )
	{
		MapSpawnPos* pInfo = NULL;
		MapMgr *mapMgr = sInstanceMgr.GetInstance(this);
		if (mapMgr&&mapMgr->GetBaseMap())
		{
			pInfo = mapMgr->GetBaseMap()->GetNearestSpawnPos(getRace(), GetPositionX(), GetPositionY());
		}

		if( pInfo )
		{
			m_mapId = pInfo->mapid;
			m_position.x = pInfo->x;
			m_position.y = pInfo->y;
			m_position.z = pInfo->z;
		}
		else
		{
			Instance* pInstance = sInstanceMgr.GetInstanceByID( m_instanceId );
			if( pInstance )
			{
				m_mapId = pInstance->m_mapInfo->mapid;
				m_position.x = pInstance->m_mapInfo->repopx;
				m_position.y = pInstance->m_mapInfo->repopy;
				m_position.z = pInstance->m_mapInfo->repopz;
			}
			else
			{
				m_mapId = m_resurrectMapId;
				m_position.x = m_resurrectPosX;
				m_position.y = m_resurrectPosY;
				m_position.z = m_resurrectPosZ;
			}
		}
		SetUInt32Value( UNIT_FIELD_DEATH_STATE, ALIVE );
		SetUInt32Value( UNIT_FIELD_HEALTH, 1 );
		SetUInt32Value( UNIT_FIELD_POWER1, 1 );
	}

	uint32 taxipath = get_next_field.GetUInt32();
	TaxiPath *path = NULL;
	if(taxipath)
	{
		path = sTaxiMgr.GetTaxiPath(taxipath);
		lastNode = get_next_field.GetUInt32();
		if(path)
		{
			SetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID, get_next_field.GetUInt32());
			SetTaxiPath(path);
			m_onTaxi = true;
		}
		else
			field_index++;
	}
	else
	{
		field_index++;
		field_index++;
	}

	m_TransporterGUID = get_next_field.GetUInt32();
	if(m_TransporterGUID)
	{
		Transporter * t = objmgr.GetTransporter(GUID_LOPART(m_TransporterGUID));
		m_TransporterGUID = t ? t->GetGUID() : 0;
	}

	m_TransporterX = get_next_field.GetFloat();
	m_TransporterY = get_next_field.GetFloat();
	m_TransporterZ = get_next_field.GetFloat();

	// Load Spells from CSV data.
	start = (char*)get_next_field.GetString();//buff;
	SpellEntry * spProto;
	while(true)
	{
		end = strchr(start,',');
		if(!end)break;
		*end=0;
		//mSpells.insert(atol(start));
		spProto = dbcSpell.LookupEntry(atol(start));
//#define _language_fix_ 1
#ifndef _language_fix_
		if(spProto)
			mSpells.insert(spProto->Id);
#else
		if (spProto)
		{
			skilllinespell * _spell = objmgr.GetSpellSkill(spProto->Id);
			if (_spell)
			{
				skilllineentry * _skill = dbcSkillLine.LookupEntry(_spell->skilline);
				if (_skill && _skill->type != SKILL_TYPE_LANGUAGE)
				{
					mSpells.insert(spProto->Id);
				}
			}
			else
			{
				mSpells.insert(spProto->Id);
			}
		}
#endif
//#undef _language_fix_

		start = end +1;
	}

	start = (char*)get_next_field.GetString();//buff;
	while(true)
	{
		end = strchr(start,',');
		if(!end)break;
		*end=0;
		spProto = dbcSpell.LookupEntry(atol(start));
		if(spProto)
			mDeletedSpells.insert(spProto->Id);
		start = end +1;
	}

	// Load Reputatation CSV Data
	start =(char*) get_next_field.GetString();

	// Load saved actionbars
	start =  (char*)get_next_field.GetString();
	Counter =0;
	while(Counter < 120)
	{
		end = strchr(start,',');
		if(!end)break;
		*end=0;
		mActions[Counter].Action = (uint32)atol(start);
		start = end +1;
		end = strchr(start,',');
		if(!end)break;
		*end=0;
		mActions[Counter].Misc = (uint8)atol(start);
		start = end +1;
		end = strchr(start,',');
		if(!end)break;
		*end=0;
		mActions[Counter++].Type = (uint8)atol(start);
		start = end +1;
	}

	//LoadAuras = get_next_field.GetString();
	start = (char*)get_next_field.GetString();//buff;
	do
	{
		end = strchr(start,',');
		if(!end)break;
		*end=0;
		LoginAura la;
		la.id = atol(start);
		start = end +1;
		end = strchr(start,',');
		if(!end)break;
		*end=0;
		la.dur = atol(start);
		start = end +1;
		loginauras.push_back(la);
	} while(true);

	// Load saved finished quests

	start =  (char*)get_next_field.GetString();
	while(true)
	{
		end = strchr(start,',');
		if(!end)break;
		*end=0;
		m_finishedQuests.insert(atol(start));
		start = end +1;
	}

	m_honorRolloverTime = get_next_field.GetUInt32();
	m_killsToday = get_next_field.GetUInt32();
	m_killsYesterday = get_next_field.GetUInt32();

	// KILL
	SetUInt32Value( PLAYER_FIELD_KILL_COUNT, get_next_field.GetUInt32() );

	// be kill
	SetUInt32Value( PLAYER_FIELD_BE_KILL_COUNT, get_next_field.GetUInt32() );

	// total honor
	SetUInt32Value( PLAYER_FIELD_TOTAL_HONOR, get_next_field.GetUInt32() );

	// honor
	SetUInt32Value( PLAYER_FIELD_HONOR_CURRENCY, get_next_field.GetUInt32() );

	RolloverHonor();
    iInstanceType = get_next_field.GetUInt32();

	//HonorHandler::RecalculateHonorFields(this);

	field_index++;
	m_leave_guild_date = get_next_field.GetUInt32();
	SetUInt32Value( PLAYER_FIELD_TEACHER, get_next_field.GetUInt32() );
	SetUInt32Value( PLAYER_FIELD_AVATAR_SETTING, get_next_field.GetUInt32() );
	SetUInt32Value( PLAYER_FIELD_ENCHANT_TEACHER_CHARGES, get_next_field.GetUInt32() );

	/*
	if( m_uint32Values[PLAYER_FIELD_STUDENT] == 0 && m_uint32Values[PLAYER_FIELD_TEACHER] == 0 )
	{
		removeSpell( 2010, false, false, 0 );
		removeSpell( 2011, false, false, 0 );
	}
	*/

	field_index++;
	m_uint32Values[PLAYER_VISIBLE_ITEM_SHOW_SHIZHUANG] = get_next_field.GetUInt32();

	if( !m_playerInfo )
		m_playerInfo = objmgr.GetPlayerInfo( GetLowGUID() );

	if(m_playerInfo == 0)
	{
		m_playerInfo = new PlayerInfo;
		m_playerInfo->acct = m_session->GetAccountId();
		m_playerInfo->cl = getClass();
		m_playerInfo->gender = getGender();
		m_playerInfo->guid = GetLowGUID();
		m_playerInfo->name = strdup(GetName());
		m_playerInfo->lastLevel = getLevel();
		m_playerInfo->lastOnline = UNIXTIME;
		m_playerInfo->lastZone = GetZoneId();
		m_playerInfo->race = getRace();
		m_playerInfo->team = GetTeam();
		m_playerInfo->guild=NULL;
		m_playerInfo->guildRank=NULL;
		m_playerInfo->guildMember=NULL;
		m_playerInfo->m_Group=0;
		m_playerInfo->subGroup=0;
		m_playerInfo->team = m_playerInfo->race;
		objmgr.AddPlayerInfo(m_playerInfo);
	}

	if( m_playerInfo )
	{
		LoadMapFromString( m_playerInfo->instance_enter, get_next_field.GetString() );
		LoadMapFromString( m_playerInfo->battleground_win, get_next_field.GetString() );
		LoadMapFromString( m_playerInfo->item_obtain, get_next_field.GetString() );
	}

	m_HealthBeforeEnterInstance = get_next_field.GetUInt32();
	m_ManaBeforeEnterInstance = get_next_field.GetUInt32();

	for(uint32 x=0;x<5;x++)
		BaseStats[x]=GetFloatValue(UNIT_FIELD_STAT0+x);

	_setFaction();

	_LoadPet(results[5].result);
	_LoadPetSpells(results[6].result);

	OnlineTime	= (uint32)UNIXTIME;
	if(GetGuildId())
		SetUInt32Value(PLAYER_GUILD_TIMESTAMP, (uint32)UNIXTIME);

#undef get_next_field

	// load properties
	_LoadTutorials(results[1].result);
	_LoadPlayerCooldowns(results[2].result);
	_LoadQuestLogEntry(results[3].result);

	if( !sInstanceMgr.HasInstance( GetMapId(), GetInstanceID() ) )
	{
		m_mapId = m_bind_mapid;
		m_instanceId = 0;
		m_position.x = m_bind_pos_x;
		m_position.y = m_bind_pos_y;
		m_position.z = m_bind_pos_z;
	}

	MSG_S2C::stEnterGameAck msg;
	msg.nMapID = m_mapId;
	msg.x = m_position.x;
	msg.y = m_position.y;
	msg.z = m_position.z;

	msg.guid = m_session->_roleID;
	msg.nTransID = m_session->_SessionID;
	m_session->SendPacket(msg);

	m_ItemInterface->mLoadItemsFromDatabase(results[4].result);

	m_mailBox->Load(results[7].result);

	// SOCIAL
	if( results[8].result != NULL )			// this query is "who are our friends?"
	{
		result = results[8].result;
		do
		{
			fields = result->Fetch();
			if( strlen( fields[1].GetString() ) )
				m_friends.insert( make_pair( fields[0].GetUInt32(), std::make_pair( strdup(fields[1].GetString()), strdup(fields[2].GetString()) ) ) );
			else
				m_friends.insert( make_pair( fields[0].GetUInt32(), std::make_pair( (char*)NULL, (char*)NULL ) ) );

		} while (result->NextRow());
	}

	if( results[9].result != NULL )			// this query is "who has us in their friends?"
	{
		result = results[9].result;
		do
		{
			m_hasFriendList.insert( result->Fetch()[0].GetUInt32() );
		} while (result->NextRow());
	}

	if( results[10].result != NULL )		// this query is "who are we ignoring"
	{
		result = results[10].result;
		do
		{
			m_ignores.insert( result->Fetch()[0].GetUInt32() );
		} while (result->NextRow());
	}

	// END SOCIAL
	m_JingLianItemInterface = new JingLianItemInterface();
	m_JingLianItemInterface->Init( this );

	if( results[11].result != NULL )		// this query is "who are we ignoring"
	{
		result = results[11].result;
		for( int i = 0; i < 5; ++i )
		{
			m_playerInfo->m_arena_win[i] = result->Fetch()[i+1].GetUInt32();
			m_playerInfo->m_arena_lose[i] = result->Fetch()[i+6].GetUInt32();
			m_playerInfo->m_arena_win_lv80[i] = result->Fetch()[i+11].GetUInt32();
			m_playerInfo->m_arena_lose_lv80[i] = result->Fetch()[i+16].GetUInt32();
		}
	}

	if( !g_crosssrvmgr->m_isInstanceSrv )
	{
		if( results[12].result )
		{
			result = results[11].result;
			do
			{
				uint32 id = result->Fetch()[1].GetUInt32();
				uint32 time_left = result->Fetch()[2].GetUInt32();
				m_Titles[id] = time_left;
			} while (result->NextRow());
			sWorld.ExecuteSqlToDBServer( "delete from playertitles where player_guid = %u", (uint32)m_session->_roleID );
		}
	}

	m_session->SetPlayer(this);
	m_session->m_loggingInPlayer=NULL;

	if( m_isReturn2Mother )
	{
		load_health = GetUInt32Value( UNIT_FIELD_MAXHEALTH );
		load_mana = GetUInt32Value( UNIT_FIELD_MAXPOWER1 );
	}
	SetUInt32Value( UNIT_FIELD_HEALTH, load_health );
	SetUInt32Value( UNIT_FIELD_POWER1, load_mana );

	this->OnLogin();
}

void Player::RolloverHonor()
{
	uint32 current_val = (g_localTime.tm_year << 16) | g_localTime.tm_yday;
	if( current_val != m_honorRolloverTime )
	{
		m_honorRolloverTime = current_val;
		m_honorYesterday = m_honorToday;
		m_killsYesterday = m_killsToday;
		m_honorToday = m_killsToday = 0;
	}
}

bool Player::HasSpell(uint32 spell)
{
	return mSpells.find(spell) != mSpells.end();
}

void Player::_LoadQuestLogEntry(QueryResult * result)
{
	QuestLogEntry *entry;
	Quest *quest;
	Field *fields;
	uint32 questid;
	uint32 baseindex;

	// clear all fields
	for(int i = 0; i < QUEST_MAX_COUNT; ++i)
	{
		baseindex = PLAYER_QUEST_LOG_1_1 + (i * 4);
		SetUInt32Value(baseindex + 0, 0);
		SetUInt32Value(baseindex + 1, 0);
		SetUInt32Value(baseindex + 2, 0);
		SetUInt32Value(baseindex + 3, 0);
	}

	int slot = 0;

	if(result)
	{
		do
		{
			fields = result->Fetch();
			questid = fields[1].GetUInt32();
			quest = QuestStorage.LookupEntry(questid);
			slot = fields[2].GetUInt32();
			ASSERT(slot != -1);

			// remove on next save if bad quest
			if(!quest)
			{
				//m_removequests.insert(questid);
				continue;
			}
			if(m_questlog[slot] != 0)
				continue;

			entry = new QuestLogEntry;
			entry->Init(quest, this, slot);
			entry->LoadFromDB(fields);
			entry->UpdatePlayerFields();
		} while(result->NextRow());
	}
}

QuestLogEntry* Player::GetQuestLogForEntry(uint32 quest)
{
	for(int i = 0; i < QUEST_MAX_COUNT; ++i)
	{
		if(m_questlog[i] == ((QuestLogEntry*)0x00000001))
			m_questlog[i] = NULL;

		if(m_questlog[i] != NULL)
		{
			if(m_questlog[i]->GetQuest() && m_questlog[i]->GetQuest()->id == quest)
				return m_questlog[i];
		}
	}
	return NULL;
	/*uint32 x = PLAYER_QUEST_LOG_1_1;
	uint32 y = 0;
	for(; x < PLAYER_VISIBLE_ITEM_1_CREATOR && y < 25; x += 3, y++)
	{
		if(m_uint32Values[x] == quest)
			return m_questlog[y];
	}
	return NULL;*/
}

void Player::SetQuestLogSlot(QuestLogEntry *entry, uint32 slot)
{
	m_questlog[slot] = entry;
}

void Player::AddToWorld()
{
	function_spend_time_monitor fm( "Player::AddToWorld" );
	FlyCheat = false;
	m_setflycheat=false;
	// check transporter
	if(m_TransporterGUID && m_CurrentTransporter)
	{
		SetPosition(m_CurrentTransporter->GetPositionX() + m_TransporterX,
			m_CurrentTransporter->GetPositionY() + m_TransporterY,
			m_CurrentTransporter->GetPositionZ() + m_TransporterZ,
			GetOrientation(), false);
	}

	// If we join an invalid instance and get booted out, this will prevent our stats from doubling :P
	if(IsInWorld())
		return;

	m_beingPushed = true;

	if( GetInstanceID() )
	{
		if( !sInstanceMgr.HasInstance( GetMapId(), GetInstanceID() ) )
		{
			// EjectFromInstance();
			m_beingPushed = false;
			m_mapId = 0;
			SafeTeleport(m_bind_mapid, 0, m_bind_pos_x, m_bind_pos_y, m_bind_pos_z, 0);
			return;
		}
	}

	Object::AddToWorld();

	// Add failed.
	if(m_mapMgr == NULL)
	{
		// eject from instance
		m_beingPushed = false;
		//EjectFromInstance();
		m_mapId = 0;
		SafeTeleport(m_bind_mapid, 0, m_bind_pos_x, m_bind_pos_y, m_bind_pos_z, 0);
		return;
	}

	if( m_mapMgr->m_sunyouinstance )
		m_mapMgr->m_sunyouinstance->TriggerJoin( this );
	if(m_session)
		m_session->SetInstance(m_mapMgr->GetInstanceID());
	if (!m_Summon &&iActivePet)
	{
		SpawnPet(iActivePet);
	}
}

void Player::AddToWorld(MapMgr * pMapMgr)
{
	FlyCheat = false;
	m_setflycheat=false;
	// check transporter
	if(m_TransporterGUID && m_CurrentTransporter)
	{
		SetPosition(m_CurrentTransporter->GetPositionX() + m_TransporterX,
			m_CurrentTransporter->GetPositionY() + m_TransporterY,
			m_CurrentTransporter->GetPositionZ() + m_TransporterZ,
			GetOrientation(), false);
	}

	// If we join an invalid instance and get booted out, this will prevent our stats from doubling :P
	if(IsInWorld())
		return;

	m_beingPushed = true;
	Object::AddToWorld(pMapMgr);

	// Add failed.
	if(m_mapMgr == NULL)
	{
		// eject from instance
		//m_beingPushed = false;
		//EjectFromInstance();
		MyLog::log->error( "fatal error! Player.cpp:4858 AddToWorld. Kick Player!" );
		m_sunyou_instance = NULL;
		GetSession()->LogoutPlayer( true );
		return;
	}

	if( m_mapMgr->m_sunyouinstance )
		m_mapMgr->m_sunyouinstance->TriggerJoin( this );
	if(m_session)
		m_session->SetInstance(m_mapMgr->GetInstanceID());

}

void Player::OnPrePushToWorld()
{
}

void Player::OnPushToWorld()
{
	SendInitialLogonPackets();

	if(m_TeleportState == 2)   // Worldport Ack
		OnWorldPortAck();

	ResetSpeedHack();
	m_beingPushed = false;
	AddItemsToWorld();
	m_lockTransportVariables = false;

	// delay the unlock movement packet
//	MSG_S2C::stMove_UnLock_Movement MsgMove_UnLock;
//	MsgMove_UnLock.unk = uint32(0);
	//delayedPackets.add(&MsgMove_UnLock);
//	GetSession()->SendPacket( MsgMove_UnLock );
	sWorld.mInWorldPlayerCount++;

	// Update PVP Situation
	LoginPvPSetup();

	Unit::OnPushToWorld();

	if(m_FirstLogin)
	{
		sHookInterface.OnFirstEnterWorld(this);
		LevelInfo * Info = objmgr.GetLevelInfo(getRace(), getClass(), sWorld.m_startLevel);
		ApplyLevelInfo(Info, sWorld.m_startLevel);
		m_FirstLogin = false;

		Quest* qst = QuestStorage.LookupEntry(1);
		if(!qst)
			return;

		//InsertTitle(59);

		m_bind_mapid = GetMapMgr()->GetMapId();
		m_bind_pos_x = GetPositionX();
		m_bind_pos_y = GetPositionY();
		m_bind_pos_z = GetPositionZ();

		SetBindPoint(GetPositionX(),GetPositionY(),GetPositionZ(),GetMapId(),GetZoneId());
		Item* pReturnStone = GetItemInterface()->FindItemLessMax(ITEM_ID_HEARTH_STONE, 0, false);
		if(pReturnStone)
		{
			pReturnStone->SetUInt32Value(ITEM_FIELD_RECORD_MAPID, GetMapId());
			pReturnStone->SetFloatValue(ITEM_FIELD_RECORD_POSX,  GetPositionX());
			pReturnStone->SetFloatValue(ITEM_FIELD_RECORD_POSY,  GetPositionY());
			pReturnStone->SetFloatValue(ITEM_FIELD_RECORD_POSZ,  GetPositionZ());
			uint32 stonezoneid = 0;
			switch( getRace() )
			{
			case 1: stonezoneid = 31; break; //妖族-31
			case 2: stonezoneid = 2; break; //人族-2
			case 3: stonezoneid = 19; break; //巫族-19
			}
			pReturnStone->SetUInt32Value(ITEM_FIELD_RECORD_ZONEID,stonezoneid);
			MSG_S2C::stOnItemFieldRecordChange msg;
			msg.entry = pReturnStone->GetEntry();
			msg.zoneid = stonezoneid;
			GetSession()->SendPacket( msg );
			pReturnStone->m_isDirty = true;
		}

		SetUInt32Value( PLAYER_VISIBLE_ITEM_SHOW_SHIZHUANG, 3 );

// 		MSG_S2C::stQuestGiver_Quest_Details Msg;
// 		sQuestMgr.BuildQuestDetails(&Msg, qst, 0, 0);
// 		m_session->SendPacket(Msg);
/*
		MSG_S2C::stSystem_Tips Msg;
		StorageContainerIterator<QuestTips> * itr = QuestTipsStorage.MakeIterator();
		QuestTips * p;
		while(!itr->AtEnd())
		{
			p = itr->Get();
			if((p->racemask & getRaceMask()) && (p->classmask & getClassMask()) && p->level == 1)
			{
				MSG_S2C::stSystem_Tips::tips tip;
				tip.ntype = p->type;
				tip.tip = p->text;
				Msg.vtips.push_back(tip);
			}
			if(!itr->Inc())
				break;
		}
		itr->Destruct();
		if(Msg.vtips.size())
			m_session->SendPacket(Msg);
*/
	}
	sHookInterface.OnEnterWorld(this);
	if(m_session->HasGMPermissions() && !m_session->CanUseCommand('1'))
	{
		//m_session->GetPlayer()->bGMTagOn = true;
		m_session->GetPlayer()->SetFlag(PLAYER_FLAGS, PLAYER_FLAG_GM);	// <GM> & Blizz symbol
	}
	if(m_session->HasGMPermissions())
	{
		//m_session->GetPlayer()->bGMTagOn = true;
		m_session->GetPlayer()->SetFlag(PLAYER_FLAGS, PLAYER_FLAG_GM);	// <GM> & Blizz symbol
	}

	if(m_TeleportState == 1)		// First world enter
		CompleteLoading();

	m_TeleportState = 0;

	if(GetTaxiState())
	{
		if( m_taxiMapChangeNode != 0 )
		{
			lastNode = m_taxiMapChangeNode;
		}

		// Create HAS to be sent before this!
		//ProcessPendingUpdates();
		TaxiStart(GetTaxiPath(),
			GetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID),
			lastNode);

		m_taxiMapChangeNode = 0;
	}

	 if(flying_aura && m_mapId != 530)
	{
		RemoveAura(flying_aura);
		flying_aura = 0;
	}

	ResetHeartbeatCoords();
	_heartbeatDisable = 0;
	_speedChangeInProgress =false;
	m_lastMoveType = 0;

	/* send weather */
	//sWeatherMgr.SendWeather(this);

	SetUInt32Value( UNIT_FIELD_HEALTH, ( load_health > m_uint32Values[UNIT_FIELD_MAXHEALTH] ? m_uint32Values[UNIT_FIELD_MAXHEALTH] : load_health ));
	SetUInt32Value( UNIT_FIELD_POWER1, ( load_mana > m_uint32Values[UNIT_FIELD_MAXPOWER1] ? m_uint32Values[UNIT_FIELD_MAXPOWER1] : load_mana ));

	if( !GetSession()->HasGMPermissions() )
		GetItemInterface()->CheckAreaItems();

	CalcResistance( 0 );

#ifdef ENABLE_COMPRESSED_MOVEMENT
	//sEventMgr.AddEvent(this, &Player::EventDumpCompressedMovement, EVENT_PLAYER_FLUSH_MOVEMENT, World::m_movementCompressInterval, 0, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
	MovementCompressor->AddPlayer(this);
#endif

	/*
	if( m_mapMgr && m_mapMgr->m_battleground != NULL && m_bg != m_mapMgr->m_battleground )
	{
		m_bg = m_mapMgr->m_battleground;
		m_bg->PortPlayer( this, true );
	}

	if( m_bg != NULL )
		m_bg->OnPlayerPushed( this );
	*/

	z_axisposition = 0.0f;
	m_changingMaps = false;
}

void Player::ResetHeartbeatCoords()
{
	_lastHeartbeatPosition = m_position;
	_lastHeartbeatV = m_runSpeed;
	if( m_isMoving )
		_startMoveTime = getMSTime();
	else
		_startMoveTime = 0;

	//_lastHeartbeatT = getMSTime();
}

void Player::RemoveFromWorld()
{
	if(raidgrouponlysent)
		event_RemoveEvents(EVENT_PLAYER_EJECT_FROM_INSTANCE);

	load_health = m_uint32Values[UNIT_FIELD_HEALTH];
	load_mana = m_uint32Values[UNIT_FIELD_POWER1];

	MapMgr* tempMgr = m_mapMgr;

	if(m_tempSummon)
	{
		m_tempSummon->RemoveFromWorld(false, true);
		if(m_tempSummon)
			m_tempSummon->SafeDelete();

		m_tempSummon = 0;
		SetUInt64Value(UNIT_FIELD_SUMMON, 0);
	}

	// Cancel trade if it's active.
	Player * pTarget;
	if(mTradeTarget != 0)
	{
		pTarget = GetTradeTarget();
		if(pTarget)
			pTarget->ResetTradeVariables();

		ResetTradeVariables();
	}
	//clear buyback
	GetItemInterface()->EmptyBuyBack();

	for(uint32 x=0;x<4;x++)
	{
		if(m_TotemSlots[x])
			m_TotemSlots[x]->TotemExpire();
	}

	ResetHeartbeatCoords();
	//ClearSplinePackets();

	if(m_Summon)
	{
		m_Summon->GetAIInterface()->SetPetOwner(0);
		m_Summon->Remove(false, true, false);
		if(m_Summon)
		{
			m_Summon->ClearPetOwner();
			m_Summon=NULL;
		}
	}

	if(m_SummonedObject)
	{
		if(m_SummonedObject->GetInstanceID() != GetInstanceID())
		{
			sEventMgr.AddEvent(m_SummonedObject, &Object::Delete, EVENT_GAMEOBJECT_EXPIRE, 100, 1,0);
		}else
		{
			if(m_SummonedObject->GetTypeId() == TYPEID_PLAYER)
			{

			}
			else
			{
				if(m_SummonedObject->IsInWorld())
				{
					m_SummonedObject->RemoveFromWorld(true);
				}
				delete m_SummonedObject;
			}
		}
		m_SummonedObject = NULL;
	}

	if(IsInWorld())
	{
		//tj:移到光环保存之前移除,解决道具光环被保存的问题
		Unit::RemoveFromWorld(false);
	}

	sWorld.mInWorldPlayerCount--;
#ifdef ENABLE_COMPRESSED_MOVEMENT
	MovementCompressor->RemovePlayer(this);
	m_movementBuffer.clear();
	//sEventMgr.RemoveEvents(this, EVENT_PLAYER_FLUSH_MOVEMENT);

#endif

	if(GetTaxiState())
		event_RemoveEvents( EVENT_PLAYER_TAXI_INTERPOLATE );

	m_changingMaps = true;


	CombatStatus.Vanished();
}

void Player::ApplyTitleMods( stPlayerTitle* pTitle, bool apply )
{
	if ( apply )
	{
		{
			EnchantEntry* ee = dbcEnchant.LookupEntry( pTitle->title_property );

			if (ee)
			{
				for(int i = 0; i < 3; i++)
				{
					uint32 type = ee->type[i];
					SpellEntry *spellInfo = dbcSpell.LookupEntry(ee->spell[i] );
					uint32 procFlag = PROC_NULL;

					if (spellInfo && spellInfo->Attributes & ATTRIBUTES_PASSIVE)
					{
						Spell *spell = new Spell(this, spellInfo ,true, NULL);
						SpellCastTargets targets;
						targets.m_unitTarget = this->GetGUID();
						spell->prepare(&targets);
					}
					else
					{
						procFlag = (type>0)?(1<<(type-1)):0;

						ProcTriggerSpell ts;
						ts.origId = 0;
						ts.spellId = ee->spell[i];
						ts.procChance = ee->ProcChance;
						ts.caster = this->GetGUID();
						ts.item = this;
						ts.procFlags = procFlag;
						ts.deleted = false;

						m_procSpells.push_front(ts);
					}
				}				
			}
		}
	}
	else
	{
		{
			EnchantEntry* ee = dbcEnchant.LookupEntry( pTitle->title_property );
			if (ee)
			{
				for(int i = 0; i < 3; i++)
				{
					uint32 type = ee->type[i];
					SpellEntry *spellInfo = dbcSpell.LookupEntry(ee->spell[i] );
					if (spellInfo && spellInfo->Attributes & ATTRIBUTES_PASSIVE)
					{
						this->RemoveAura(ee->spell[i]);
					}
					else if (type <= APPLY_TARGET_SELF )
					{
						std::list< struct ProcTriggerSpell >::iterator itr,itr2;
						for( itr = this->m_procSpells.begin(); itr != this->m_procSpells.end(); )
						{
							itr2 = itr;
							++itr;
							if (itr2->item == this)
							{
								this->m_procSpells.erase( itr2 );
							}
						}
					}
				}				
			}
		}
	}
	//EnchantEntry* Entry = dbcEnchant.LookupEntry( pTitle->title_property );
	//if(!Entry)
	//{
	//	MyLog::log->error("not found enchant[%d]", pTitle->title_property);
	//	return;
	//}
	//for( uint32 c = 0; c < 3; c++ )
	//{
	//	if( Entry->type[c] )
	//	{
	//		// Depending on the enchantment type, take the appropriate course of action.
	//		switch( Entry->type[c] )
	//		{
	//		case 101:		 // Trigger spell on melee attack.
	//			{
	//				if( apply && Entry->spell[c] != 0 )
	//				{
	//					// Create a proc trigger spell

	//					ProcTriggerSpell TS;
	//					TS.caster = GetGUID();
	//					TS.origId = 0;
	//					TS.procFlags = PROC_ON_MELEE_ATTACK;
	//					TS.procCharges = 0;
	//					/* This needs to be modified based on the attack speed of the weapon.
	//					* Secondly, need to assign some static chance for instant attacks (ss,
	//					* gouge, etc.) */

	//					if( !Entry->min[c] /*&& GetProto()->Class == 2*/ )
	//					{
	//						float speed = (float)/*GetProto()->Delay*/2000;
	//						/////// procChance calc ///////
	//						float ppm = 0;
	//						SpellEntry* sp = dbcSpell.LookupEntry( Entry->spell[c] );
	//						if( sp )
	//						{
	//							switch( sp->NameHash )
	//							{
	//							case SPELL_HASH_FROSTBRAND_ATTACK:
	//								ppm = 9;
	//								break;
	//							}
	//						}
	//						if( ppm != 0 )
	//						{
	//							float pcount = 60/ppm;
	//							float chance = (speed/10) / pcount;
	//							TS.procChance = (uint32)chance;
	//						}
	//						else
	//							TS.procChance = (uint32)( speed / 600.0f );
	//						///////////////////////////////
	//					}
	//					else
	//						TS.procChance = Entry->min[c];
	//					MyLog::log->debug("Enchant : Setting procChance to %u%%.", TS.procChance);
	//					TS.deleted = false;
	//					TS.spellId = Entry->spell[c];
	//					m_procSpells.push_back( TS );
	//				}
	//				else
	//				{
	//					// Remove the proctriggerspell
	//					uint32 SpellId;
	//					list< struct ProcTriggerSpell >::iterator itr/*, itr2*/;
	//					for( itr = m_procSpells.begin(); itr != m_procSpells.end(); )
	//					{
	//						SpellId = itr->spellId;
	//						/*itr2 = itr++;*/

	//						if( SpellId == Entry->spell[c] )
	//						{
	//							//m_owner->m_procSpells.erase(itr2);
	//							itr->deleted = true;
	//						}
	//						itr++;
	//					}
	//				}
	//			}break;

	//		case 102:		 // Mod damage done.
	//			{
	//				int32 valmin = Entry->min[c];
	//				//int32 valmax = Entry->max[c];
	//				int val = valmin;// + RandomUInt(valmax-valmin);
	//				//if( RandomSuffixAmount )
	//				//	val = RANDOM_SUFFIX_MAGIC_CALCULATION( RandomSuffixAmount, GetItemRandomSuffixFactor() );

	//				if( apply )
	//					ModFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_POS, val );
	//				else
	//					ModFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_POS, -val );
	//				CalcDamage();
	//			}break;

	//		case 103:		 // Cast spell (usually means apply aura)
	//			{
	//				if( apply )
	//				{
	//					SpellCastTargets targets( GetGUID() );
	//					SpellEntry* sp;
	//					Spell* spell;

	//					if( Entry->spell[c] != 0 )
	//					{
	//						sp = dbcSpell.LookupEntry( Entry->spell[c] );
	//						if( sp == NULL )
	//							continue;

	//						spell = new Spell( this, sp, true, 0 );
	//						spell->p_caster = this;
	//						spell->prepare( &targets );
	//					}
	//				}
	//				else
	//				{
	//					if( Entry->spell[c] != 0 )
	//						RemoveAura( Entry->spell[c] );
	//				}

	//			}break;

	//		case 104:		 // Modify physical resistance
	//			{
	//				int32 valmin = Entry->min[c];
	//				//int32 valmax = Entry->max[c];
	//				int val = valmin;// + RandomUInt(valmax-valmin);

	//				if( apply )
	//				{
	//					FlatResistanceModifierPos[Entry->type[c]] += val;
	//				}
	//				else
	//				{
	//					FlatResistanceModifierPos[Entry->type[c]] -= val;
	//				}
	//				CalcResistance( Entry->type[c] );
	//			}break;

	//		case 105:	 //Modify rating ...order is PLAYER_FIELD_COMBAT_RATING_1 and above
	//			{
	//				//spellid is enum ITEM_STAT_TYPE
	//				//min=max is amount
	//				int32 valmin = Entry->min[c];
	//				//int32 valmax = Entry->max[c];
	//				int val = valmin;// + RandomUInt(valmax-valmin);

	//				ModifyBonuses( Entry->type[c], apply ? val : -val );
	//				UpdateStats();
	//			}break;

	//		case 106:	 // Rockbiter weapon (increase damage per second... how the hell do you calc that)
	//			{
	//				if( apply )
	//				{
	//					//m_owner->ModFloatValue(PLAYER_FIELD_MOD_DAMAGE_DONE_POS, Entry->min[c]);
	//					//if i'm not wrong then we should apply DMPS formula for this. This will have somewhat a larger value 28->34
	//					int32 valmin = Entry->min[c];
	//					//int32 valmax = Entry->max[c];
	//					int val = valmin;// + RandomUInt(valmax-valmin);

	//					int32 value = /*GetProto()->Delay*/2000 * val / 1000;
	//					ModFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_POS, value );
	//				}
	//				else
	//				{
	//					int32 valmin = Entry->min[c];
	//					//int32 valmax = Entry->max[c];
	//					int val = valmin;// + RandomUInt(valmax-valmin);

	//					int32 value =- (int32)(/*GetProto()->Delay*/2000 * val / 1000 );
	//					ModFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_POS, value );
	//				}
	//				CalcDamage();
	//			}break;

	//		case 1: //		生命
	//		case 2: //		魔法
	//		case 3: //		力量
	//		case 4: //		敏捷
	//		case 5: //		耐力
	//		case 6: //		智力
	//		case 7: //		精神
	//		case 8: //		体力
	//		case 10: //		闪避几率
	//		case 18://		暴击几率
	//			{
	//				int32 valmin = Entry->min[c];
	//				//int32 valmax = Entry->max[c];
	//				int val = valmin;// + RandomUInt(valmax-valmin);
	//				ModifyBonuses( Entry->type[c], apply ? val : -val );
	//			}
	//			break;
	//		default:
	//			{
	//				MyLog::log->error( "Unknown enchantment type: %u (%u)", Entry->type[c], Entry->Id );
	//			}break;
	//		}
	//	}
	//}
}

// TODO: perhaps item should just have a list of mods, that will simplify code
void Player::_ApplyItemMods(Item* item, ui8 slot, bool apply, bool justdrokedown /* = false */, bool skip_stat_apply /* = false  */)
{
	if (slot >= INVENTORY_SLOT_BAG_END)
		return;

	ASSERT( item );

	if(apply)
	{
		if(	m_itemAlreadyApplied.find(slot) != m_itemAlreadyApplied.end() )
		{
			MyLog::log->error("item which is alraedy applied try to apply again");
			return;
		}
		m_itemAlreadyApplied.insert(slot);
	}	
	else
	{
		if( m_itemAlreadyApplied.find(slot) == m_itemAlreadyApplied.end() )
		{
			MyLog::log->error("item which is not applied try to unapply");
			return;
		}
		m_itemAlreadyApplied.erase(slot);
	}

	ItemPrototype* proto = item->GetProto();

	//check for rnd prop
	item->ApplyRandomProperties( apply );

	//fast check to skip mod applying if the item doesnt meat the requirements.
	if( item->GetUInt32Value( ITEM_FIELD_DURABILITY ) == 0 && item->GetUInt32Value( ITEM_FIELD_MAXDURABILITY ) && justdrokedown == false )
	{
		return;
	}

	//Items Set check
	uint32 setid = proto->ItemSet;

	// These season pvp itemsets are interchangeable and each set group has the same
	// bonuses if you have a full set made up of parts from any of the 3 similar sets
	// you will get the highest sets bonus

	// TODO: make a config for server so they can configure which season is active season

	// Set
	if( setid != 0 )
	{
		ItemSetEntry* set = ItemSetStorage.LookupEntry( setid );
		if( set )
		{
			ItemSet* Set = NULL;
			std::list<ItemSet>::iterator i;
			for( i = m_itemsets.begin(); i != m_itemsets.end(); i++ )
			{
				if( i->setid == setid )
				{
					Set = &(*i);
					break;
				}
			}

			if( apply )
			{
				if( Set == NULL )
				{
					Set = new ItemSet;
					memset( Set, 0, sizeof( ItemSet ) );
					Set->itemscount = 1;
					Set->setid = setid;
					if( 9 == item->GetUInt32Value( ITEM_FIELD_JINGLIAN_LEVEL ) )
						Set->refine_lv9_cnt = 1;
				}
				else
				{
					Set->itemscount++;
					if( 9 == item->GetUInt32Value( ITEM_FIELD_JINGLIAN_LEVEL ) )
						Set->refine_lv9_cnt++;
				}

				uint32 max_set_count = 0;
				if( !set->RequiredSkillID || ( _GetSkillLineCurrent( set->RequiredSkillID, true ) >= set->RequiredSkillAmt ) )
				{
					int ultimate_x = -1;
					for( int x = 7; x >= 0; --x )
					{
						if( set->itemscount[x] && max_set_count == 0 )
							max_set_count = set->itemscount[x];

						if( ultimate_x < 0 && set->itemscount[x] > 0 )
							ultimate_x = x;

						if( Set->itemscount==set->itemscount[x])
						{//cast new spell
							if( x != ultimate_x || Set->refine_lv9_cnt == Set->itemscount )
							{
								SpellEntry *info = dbcSpell.LookupEntry( set->SpellID[x] );
								Spell * spell = new Spell( this, info, true, NULL );
								SpellCastTargets targets;
								targets.m_unitTarget = this->GetGUID();
								spell->prepare( &targets );
							}
						}
					}
				}
				if( max_set_count && set->titleid && !HasTitle( set->titleid ) && Set->itemscount == max_set_count )
					QuickAddTitle( set->titleid );

				if( i == m_itemsets.end() )
				{
					m_itemsets.push_back( *Set );
					delete Set;
				}
			}
			else
			{
				if( Set )
				{
					for( uint32 x = 0; x < 8; x++ )
						if( Set->itemscount == set->itemscount[x] )
						{
							this->RemoveAura( set->SpellID[x], GetGUID() );
							break;
						}

					--Set->itemscount;
					if( 9 == item->GetUInt32Value( ITEM_FIELD_JINGLIAN_LEVEL ) )
						--Set->refine_lv9_cnt;

					if( Set->itemscount == 0 )
						m_itemsets.erase(i);
				}
			}
		}
		else
		{
			MyLog::log->error( "item set cannot found id :%u, entry:%u", setid, proto->ItemId );
		}
	}

	int extern_damage = 0, extern_armo = 0, extern_stat = 0;
	switch(proto->Quality)
	{
	case ITEM_QUALITY_POOR_GREY:break;		
	case ITEM_QUALITY_NORMAL_WHITE:		extern_damage = 3;	extern_armo = 1;						break;
	case ITEM_QUALITY_UNCOMMON_GREEN:	extern_damage = 5;	extern_armo = 2;	extern_stat = 1;	break;
	case ITEM_QUALITY_RARE_BLUE:		extern_damage = 7;	extern_armo = 3;	extern_stat = 2;	break;
	case ITEM_QUALITY_EPIC_PURPLE:		extern_damage = 9;	extern_armo = 4;	extern_stat = 3;	break;
	case ITEM_QUALITY_LEGENDARY_ORANGE: extern_damage = 12; extern_armo = 5;	extern_stat = 4;	break;
	}
	uint32 item_jinglian_lv = item->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
	extern_damage *= item_jinglian_lv;
	extern_armo   *= item_jinglian_lv;
	extern_stat   *= item_jinglian_lv;

	for( int i = 0; i < 10; i++ )
	{
		int32 val = proto->Stats[i].Value;
		if( val == 0 )
			continue;
		if(val)
		{
			ModifyBonuses( proto->Stats[i].Type, apply ? val : -val );
			ModifyBonuses( proto->Stats[i].Type, apply ? extern_stat : -extern_stat );
		}
	}

	// Armor
	//if( proto->Armor )
	//{
		int armor = proto->Armor;
		if( apply ) BaseResistance[0] += armor;
		else  BaseResistance[0] -= armor;
		BaseResistance[0] += apply ? extern_armo : -extern_armo;
		CalcResistance( 0 );
	/*}*/

	// Stats

	// Damage
	if( proto->Damage[0].Min )
	{
		int dmgmin = proto->Damage[0].Min;
		int dmgmax = proto->Damage[0].Max;
		if( proto->InventoryType == INVTYPE_RANGED /*|| proto->InventoryType == INVTYPE_RANGEDRIGHT || proto->InventoryType == INVTYPE_THROWN */)
		{
			BaseRangedDamage[0] += apply ? dmgmin : -dmgmin;
			BaseRangedDamage[1] += apply ? dmgmax : -dmgmax;
		}
		else
		{
			if( slot == EQUIPMENT_SLOT_OFFHAND )
			{
				BaseOffhandDamage[0] = apply ? dmgmin : 0;
				BaseOffhandDamage[1] = apply ? dmgmax : 0;
				BaseOffhandDamage[0] += apply ? extern_damage : 0;
				BaseOffhandDamage[1] += apply ? extern_damage : 0;
			}
			else
			{
				BaseDamage[0] = apply ? dmgmin : 0;
				BaseDamage[1] = apply ? dmgmax : 0;
				BaseDamage[0] += apply ? extern_damage : 0;
				BaseDamage[1] += apply ? extern_damage : 0;
			}
		}
	}

	// Misc
	if( apply )
	{
		// Apply all enchantment bonuses
		item->ApplyEnchantmentBonuses();

		for( int k = 0; k < 5; k++ )
		{
			// stupid fucked dbs
			if( item->GetProto()->Spells[k].Id == 0 )
				continue;

			if( item->GetProto()->Spells[k].Trigger == 1 )
			{
				SpellEntry* spells = dbcSpell.LookupEntry( item->GetProto()->Spells[k].Id );
				if( spells->RequiredShapeShift )
				{
					AddShapeShiftSpell( spells->Id );
					continue;
				}

				Spell *spell = new Spell( this, spells ,true, NULL );
				SpellCastTargets targets;
				targets.m_unitTarget = this->GetGUID();
				spell->castedItemId = item->GetEntry();
				spell->prepare( &targets );

			}
			else if( item->GetProto()->Spells[k].Trigger == 2 )
			{
				ProcTriggerSpell ts;
				ts.origId = 0;
				ts.spellId = item->GetProto()->Spells[k].Id;
				ts.procChance = 5;
				ts.caster = this->GetGUID();
				ts.procFlags = PROC_ON_MELEE_ATTACK;
				ts.deleted = false;
				m_procSpells.push_front( ts );
			}
		}
	}
	else
	{
		// Remove certain "enchantments" that are not actually enchantments - WTF! Damn you, Blizzard.
		// Classic example: http://www.wowhead.com/?spell=29720
		// (Greater Ward of Shielding) - Absorbs 4000 damage. Requires a shield.
		// Doesn't get removed when we unequip the shield. Why? Because it's just an Aura. Not an enchantment.
			// damn bitwise values

		for( uint32 x = 0 ; x <= MAX_POSITIVE_AURAS ; x ++ )
		{
			if( m_auras[x] )
			{
				//Log.Notice( "Player" , " Test: (%u) %u (%s) [%u,%u]" , x , m_auras[x]->GetSpellId() , m_auras[x]->GetSpellProto()->Name ,m_auras[x]->GetSpellProto()->EquippedItemClass , m_auras[x]->GetSpellProto()->EquippedItemSubClass );
				if( m_auras[x]->GetSpellProto()->EquippedItemClass && m_auras[x]->GetSpellProto()->EquippedItemSubClass )
				{
					if( item->GetProto()->Class == m_auras[x]->GetSpellProto()->EquippedItemClass && ( 1 << item->GetProto()->SubClass ) & m_auras[x]->GetSpellProto()->EquippedItemSubClass ) // fucking bits
						m_auras[x]->Remove();
				}
			}
		}
		// Remove all enchantment bonuses
		item->RemoveEnchantmentBonuses();
		for( int k = 0; k < 5; k++ )
		{
			if( item->GetProto()->Spells[k].Trigger == 1 )
			{
				SpellEntry* spells = dbcSpell.LookupEntry( item->GetProto()->Spells[k].Id );
				if( spells )
				{
					if( spells->RequiredShapeShift )
						RemoveShapeShiftSpell( spells->Id );
					else
						RemoveAura( item->GetProto()->Spells[k].Id );
				}
			}
			else if( item->GetProto()->Spells[k].Trigger == 2 )
			{
				std::list<struct ProcTriggerSpell>::iterator i;
				// Debug: i changed this a bit the if was not indented to the for
				// so it just set last one to deleted looks like unintended behaviour
				// because you can just use end()-1 to remove last so i put the if
				// into the for
				for( i = m_procSpells.begin(); i != m_procSpells.end(); i++ )
				{
					if( (*i).spellId == item->GetProto()->Spells[k].Id && !(*i).deleted )
					{
						//m_procSpells.erase(i);
						i->deleted = true;
						break;
					}
				}
			}
		}
	}

	//if( !skip_stat_apply )
		UpdateStats();
}


void Player::SetMovement(uint8 pType, uint32 flag)
{

	switch(pType)
	{
	case MOVE_ROOT:
		{
			MSG_S2C::stMove_Force_Root Msg;
			Msg.guid	= GetNewGUID();
			Msg.flag	= flag;
			m_currentMovement = MOVE_ROOT;
			SendMessageToSet(Msg, true);
		}break;
	case MOVE_UNROOT:
		{
			MSG_S2C::stMove_Force_UnRoot Msg;
			Msg.guid	= GetNewGUID();
			Msg.flag	= flag;
			m_currentMovement = MOVE_UNROOT;

			SendMessageToSet(Msg, true);
		}break;
	case MOVE_WATER_WALK:
		{
			m_setwaterwalk=true;
			MSG_S2C::stMove_Water_Walk Msg;
			Msg.guid	= GetNewGUID();
			Msg.flag	= flag;
			SendMessageToSet(Msg, true);
		}break;
	case MOVE_LAND_WALK:
		{
			m_setwaterwalk=false;
			MSG_S2C::stMove_Land_Walk Msg;
			Msg.guid	= GetNewGUID();
			Msg.flag	= flag;
			SendMessageToSet(Msg, true);
		}break;
	default:break;
	}
}

void Player::SetPlayerSpeed(uint8 SpeedType, float value)
{
	MSG_S2C::stMove_Set_Speed Msg;
	Msg.guid = GetNewGUID();
	Msg.speed = value;
	Msg.SpeedType = SpeedType;

	if( SpeedType != SWIMBACK )
	{
		Msg.speedChangeCounter = m_speedChangeCounter++;
	}
	else
	{
		Msg.timeNow = getMSTime();
		Msg.x = GetPosition().x;
		Msg.y = GetPosition().y;
		Msg.z = GetPosition().z;
		Msg.orientation = m_position.o;
	}

	switch(SpeedType)
	{
	case RUN:
		{
			if(value == m_lastRunSpeed)
				return;

			m_runSpeed = value;
			m_lastRunSpeed = value;
		}break;
	case RUNBACK:
		{
			if(value == m_lastRunBackSpeed)
				return;

			m_backWalkSpeed = value;
			m_lastRunBackSpeed = value;
		}break;
	case SWIM:
		{
			if(value == m_lastSwimSpeed)
				return;

			m_swimSpeed = value;
			m_lastSwimSpeed = value;
		}break;
	case SWIMBACK:
		{
			if(value == m_lastBackSwimSpeed)
				break;

			m_backSwimSpeed = value;
			m_lastBackSwimSpeed = value;
		}break;
	case FLY:
		{
			if(value == m_lastFlySpeed)
				return;

			m_flySpeed = value;
			m_lastFlySpeed = value;
		}break;
	default:return;
	}

	SendMessageToSet(Msg , true);

	// dont mess up on these
	_speedChangeInProgress = true;
}

void Player::BuildPlayerRepop()
{
	SetUInt32Value( UNIT_FIELD_HEALTH, 1 );

	//8326 --for all races but ne,  9036 20584--ne
	/*SpellCastTargets tgt;
	tgt.m_unitTarget=this->GetGUID();

	{

		SpellEntry *inf=dbcSpell.LookupEntry(8326);
		Spell*sp=new Spell(this,inf,true,NULL);
		sp->prepare(&tgt);
	}*/

	//StopMirrorTimer(0);
	//StopMirrorTimer(1);
	//StopMirrorTimer(2);

	SetFlag(PLAYER_FLAGS, 0x10);

	SetMovement(MOVE_UNROOT, 1);
	SetMovement(MOVE_WATER_WALK, 1);
}

void Player::ResurrectPlayerOnSite()
{
	if( isAlive() || m_resurrect_lock )
		return;

	RemoveAllAuras( false );
	RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);
	//test for resurrect
	SetPlayerStatus( NONE );
	UpdateVisibility();
	SetMovement(MOVE_LAND_WALK, 1);
	RemoveFlag(PLAYER_FLAGS, 0x10);

	setDeathState(ALIVE);

	SetUInt32Value(UNIT_FIELD_HEALTH, GetUInt32Value( UNIT_FIELD_MAXHEALTH ) * 0.3f);
	SetUInt32Value(UNIT_FIELD_POWER1, GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) * 0.3f);

	MSG_S2C::stReliveAck msg;
	msg.guid = GetGUID();
	SendMessageToSet( msg, true );

	sEventMgr.RemoveEvents(this,EVENT_PLAYER_FORECED_RESURECT); //in case somebody resurected us before this event happened
	m_resurrectHealth = m_resurrectMana = 0;

	if(GetGroup())
	{
		sAnimalMgr.OnAniamlRelation(GetGroup()->GetSubGroup(GetSubGroup()));
	}

	sEventMgr.RemoveEvents( this, EVENT_SUNYOU_INSTANCE_RESURRECT_PLAYER );
}

void Player::ResurrectPlayer( )
{
	if( isAlive() || m_resurrect_lock )
		return;

	RemoveAllAuras( false );
	RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);
	//test for resurrect
	SetPlayerStatus( NONE );
	UpdateVisibility();
	SetMovement(MOVE_LAND_WALK, 1);
	RemoveFlag(PLAYER_FLAGS, 0x10);

	setDeathState(ALIVE);

	//SetUInt32Value(UNIT_FIELD_HEALTH, GetUInt32Value( UNIT_FIELD_MAXHEALTH ) * mphp);
	//SetUInt32Value(UNIT_FIELD_POWER1, GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) * mphp);
	if( m_ResurrectForceLocation.x > 0.f || m_ResurrectForceLocation.y > 0.f || m_ResurrectForceLocation.z > 0.f || m_ResurrectForceLocation.o > 0.f )
	{
		SafeTeleport( m_mapMgr->GetMapId(), m_mapMgr->GetInstanceID(), m_ResurrectForceLocation );
		m_ResurrectForceLocation.x = 0.f;
		m_ResurrectForceLocation.y = 0.f;
		m_ResurrectForceLocation.z = 0.f;
		m_ResurrectForceLocation.o = 0.f;
	}
	else if( m_escape_mode && m_mapMgr->m_sunyouinstance )
	{
		EjectFromInstance();
	}
	else if(m_bReliveItem)
	{
		m_resurrectHealth = 100;
		m_resurrectMana = 100;
	}
	else if(resurrector && IsInWorld())
	{
		resurrector=0;
		SafeTeleport(m_resurrectMapId,m_resurrectInstanceID,m_resurrectPosX, m_resurrectPosY, m_resurrectPosZ, 0);
	}
	else if( m_random_vrl.size() )
	{
		LocationVector* plv = m_random_vrl[rand() % m_random_vrl.size()];
		SafeTeleport( m_resurrectMapId, m_resurrectInstanceID, *plv );
	}
	else if( m_resurrectMapId )
	{
		MapSpawnPos* pInfo = GetMapMgr()->GetBaseMap()->GetNearestSpawnPos(getRace(), GetPositionX(), GetPositionY());
		if( pInfo && !resurrector )
		{
			SafeTeleport(pInfo->mapid, 0, pInfo->x, pInfo->y, pInfo->z, 0);
		}
		else
			SafeTeleport(m_resurrectMapId, m_resurrectInstanceID, m_resurrectPosX, m_resurrectPosY, m_resurrectPosZ, 0);

		resurrector = 0;
	}
	else
		SafeTeleport(m_bind_mapid,0, m_bind_pos_x, m_bind_pos_y, m_bind_pos_z, 0);

	MSG_S2C::stReliveAck msg;
	msg.guid = GetGUID();
	SendMessageToSet( msg, true );

	sEventMgr.RemoveEvents(this,EVENT_PLAYER_FORECED_RESURECT); //in case somebody resurected us before this event happened
	//if(!m_bReliveItem)
	{
		if( m_random_vrl.size() && !m_bReliveItem)
		{
			SetUInt32Value( UNIT_FIELD_HEALTH, m_uint32Values[UNIT_FIELD_MAXHEALTH] );
			SetUInt32Value( UNIT_FIELD_POWER1, m_uint32Values[UNIT_FIELD_MAXPOWER1] );

			RestoreAllExtraAuras();

			if( m_castle_debuff_spell_id )
			{
				SpellEntry* proto = dbcSpell.LookupEntry( m_castle_debuff_spell_id );
				if( proto )
				{
					Spell * sp = new Spell( this, proto, true, 0 );
					SpellCastTargets targets( GetGUID() );
					sp->prepare(&targets);
				}
			}
		}
		else
		{
			uint32 h = (uint32)( (float)m_resurrectHealth / 100.f * GetUInt32Value( UNIT_FIELD_MAXHEALTH ) );
			uint32 m = (uint32)( (float)m_resurrectMana / 100.f * GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) );

			if( h )
				SetUInt32Value( UNIT_FIELD_HEALTH, (uint32)min( h, m_uint32Values[UNIT_FIELD_MAXHEALTH] ) );
			if( m )
				SetUInt32Value( UNIT_FIELD_POWER1, (uint32)min( m, m_uint32Values[UNIT_FIELD_MAXPOWER1] ) );
		}

		m_resurrectHealth = m_resurrectMana = 0;

		if(GetGroup())
		{
			sAnimalMgr.OnAniamlRelation(GetGroup()->GetSubGroup(GetSubGroup()));
		}
	}
	
	if(m_bReliveItem)
	{
		m_bReliveItem = false;
	}

	if( m_sunyou_instance )
		m_sunyou_instance->OnPlayerResurrect( this );

	sEventMgr.RemoveEvents( this, EVENT_SUNYOU_INSTANCE_RESURRECT_PLAYER );
}

void Player::ResurrectPlayerForceLocation( float x, float y, float z, float o )
{
	m_ResurrectForceLocation.x = x;
	m_ResurrectForceLocation.y = y;
	m_ResurrectForceLocation.z = z;
	m_ResurrectForceLocation.o = o;

	ResurrectPlayer();
}

void Player::KillPlayer()
{
	CombatStatus.Vanished();
	if( m_currentSpell )
	{
		if( m_currentSpell->getState() == SPELL_STATE_CASTING )
		{
			// cancel the existing channel spell, cast this one
			m_currentSpell->cancel();
		}
		else
		{
			// send the error message
			SendCastResult(m_currentSpell->m_spellInfo->Id, SPELL_FAILED_SPELL_IN_PROGRESS, 0, 0);
			//return;
		}
	}

	m_inarmedtimeleft = -1;

	MSG_S2C::stCombat_Cancel MsgCombatCancel;
	m_session->SendPacket(MsgCombatCancel);
	MSG_S2C::stSpell_Cancel_Auto_Repeat Msg;
	m_session->SendPacket( Msg );

	setDeathState(JUST_DIED);
	EventDeath();
	//StopMirrorTimer(0);
	//StopMirrorTimer(1);
	//StopMirrorTimer(2);

	uint64 guid;
	guid  = GetUInt64Value(PLAYER_HEAD);
	if( guid )
	{
		Player* player = objmgr.GetPlayer(guid);
		if(player)
		{
			MyLog::log->debug("player[%s] disride for player[%s] died", player->GetName(), GetName());
			player->SetUInt64Value(PLAYER_FOOT, 0);
		}
		SetUInt64Value(PLAYER_HEAD, 0);
	}
	else
	{
		guid = GetUInt64Value(PLAYER_FOOT);
		if(guid)
		{
			Player* player = objmgr.GetPlayer(guid);
			if(player)
			{
				MyLog::log->debug("player[%s] disride for player[%s] died", player->GetName(), GetName());
				player->SetUInt64Value(PLAYER_HEAD, 0);
			}
		}
		SetUInt64Value(PLAYER_FOOT, 0);
	}

	RemoveAllAuras(false);

	//SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PLAYER_CONTROLLED); //player death animation, also can be used with DYNAMIC_FLAGS <- huh???
	//SetUInt32Value( UNIT_DYNAMIC_FLAGS, 0x00 );
	//if(this->getClass() == CLASS_WARRIOR) //rage resets on death
	SetUInt32Value(UNIT_FIELD_POWER1, 0);

	sHookInterface.OnDeath(this);

	//ResurrectPlayer();
	if( GetMapMgr()->m_sunyouinstance )
	{
		if( GetMapMgr()->IsRunInstance() )
		{
			ResetDeathClock();
			MSG_S2C::stChoiceWhileInInstance Msg;
			m_session->SendPacket(Msg);
			MSG_S2C::stAI_Died MsgDied;
			MsgDied.died_guid = GetGUID();
			SendMessageToSet(MsgDied, false);
			return;
		}
		else
		{
			if( GetMapMgr()->m_sunyouinstance->GetConfig()->resurrect_time > 0 
				&& (GetMapMgr()->m_sunyouinstance->GetCategory() != INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND 
				|| GetMapMgr()->m_sunyouinstance->GetCategory() != INSTANCE_CATEGORY_NEW_BATTLE_GROUND))
				sEventMgr.AddEvent(this, &Player::ResurrectPlayer, EVENT_SUNYOU_INSTANCE_RESURRECT_PLAYER, GetMapMgr()->m_sunyouinstance->GetConfig()->resurrect_time*1000, 0, 2);//ResurrectPlayer();
		}
	}
	else
		sEventMgr.AddEvent(this, &Player::ResurrectPlayer, EVENT_SUNYOU_INSTANCE_RESURRECT_PLAYER, 5*60*1000, 0, 2);
	ResetDeathClock();

	MSG_S2C::stAI_Died MsgDied;
	MsgDied.died_guid = GetGUID();
	SendMessageToSet(MsgDied, true);

	return;
}

void Player::CreateCorpse()
{
	Corpse *pCorpse;
	uint32 _uf, _pb, _pb2, _cfb1, _cfb2;

	objmgr.DelinkPlayerCorpses(this);
	if(!this->bCorpseCreateable)
	{
		bCorpseCreateable = true;   // for next time
		return; // no corpse allowed!
	}

	pCorpse = objmgr.CreateCorpse();
	pCorpse->SetInstanceID(GetInstanceID());
	pCorpse->Create(this, GetMapId(), GetPositionX(),
		GetPositionY(), GetPositionZ(), GetOrientation());

	_uf = GetUInt32Value(UNIT_FIELD_BYTES_0);
	_pb = GetUInt32Value(PLAYER_BYTES);
	_pb2 = GetUInt32Value(PLAYER_BYTES_2);

	uint8 race	   = (uint8)(_uf);
	uint8 skin	   = (uint8)(_pb);
	uint8 face	   = (uint8)(_pb >> 8);
	uint8 hairstyle  = (uint8)(_pb >> 16);
	uint8 haircolor  = (uint8)(_pb >> 24);
	uint8 facialhair = (uint8)(_pb2);

	_cfb1 = ((0x00) | (race << 8) | (0x00 << 16) | (skin << 24));
	_cfb2 = ((face) | (hairstyle << 8) | (haircolor << 16) | (facialhair << 24));

	pCorpse->SetZoneId( GetZoneId() );
	pCorpse->SetUInt32Value( CORPSE_FIELD_BYTES_1, _cfb1 );
	pCorpse->SetUInt32Value( CORPSE_FIELD_BYTES_2, _cfb2 );
	pCorpse->SetUInt32Value( CORPSE_FIELD_FLAGS, 4 );
	pCorpse->SetUInt32Value( CORPSE_FIELD_DISPLAY_ID, GetUInt32Value(UNIT_FIELD_DISPLAYID) );

	pCorpse->loot.gold = 0;

	uint32 iDisplayID = 0;
	uint16 iIventoryType = 0;
	uint32 _cfi = 0;
	Item * pItem;
	for (ui8 i = 0; i < EQUIPMENT_SLOT_END; i++)
	{
		if(( pItem = GetItemInterface()->GetInventoryItem(i)))
		{
			iDisplayID = pItem->GetProto()->DisplayInfoID[0][0];
			iIventoryType = (uint16)pItem->GetProto()->InventoryType;

			_cfi =  (uint16(iDisplayID)) | (iIventoryType)<< 24;
			pCorpse->SetUInt32Value(CORPSE_FIELD_ITEM + i,_cfi);
		}
	}
	//save corpse in db for future use
	pCorpse->SaveToDB();
}

void Player::SpawnCorpseBody()
{
	Corpse *pCorpse;

	pCorpse = objmgr.GetCorpseByOwner(this->GetLowGUID());
	if(pCorpse && !pCorpse->IsInWorld())
	{
		if(bShouldHaveLootableOnCorpse && pCorpse->GetUInt32Value(CORPSE_FIELD_DYNAMIC_FLAGS) != 1)
			pCorpse->SetUInt32Value(CORPSE_FIELD_DYNAMIC_FLAGS, 1); // sets it so you can loot the plyr

		if(m_mapMgr == 0)
			pCorpse->AddToWorld();
		else
			pCorpse->PushToWorld(m_mapMgr);
	}
	myCorpse = pCorpse;
}

void Player::SpawnCorpseBones()
{
	Corpse *pCorpse;
	pCorpse = objmgr.GetCorpseByOwner(GetLowGUID());
	myCorpse = 0;
	if(pCorpse)
	{
		if (pCorpse->IsInWorld() && pCorpse->GetCorpseState() == CORPSE_STATE_BODY)
		{
			if(pCorpse->GetInstanceID() != GetInstanceID())
			{
				sEventMgr.AddEvent(pCorpse, &Corpse::SpawnBones, EVENT_CORPSE_SPAWN_BONES, 100, 1,0);
			}
			else
				pCorpse->SpawnBones();
		}
		else
		{
			//Cheater!
		}
	}
}

void Player::DeathDurabilityLoss(double percent)
{
	if( m_mapMgr && m_mapMgr->m_sunyouinstance )
	{
		if( m_mapMgr->m_sunyouinstance->GetCategory() == INSTANCE_CATEGORY_TEAM_ARENA
			|| m_mapMgr->m_sunyouinstance->GetCategory() == INSTANCE_CATEGORY_BATTLE_GROUND )
		{
			return;
		}
	}
	//m_session->OutPacket(SMSG_DURABILITY_DAMAGE_DEATH);
	uint32 pDurability;
	uint32 pMaxDurability;
	int32 pNewDurability;
	Item * pItem;

	for (ui8 i = 0; i < EQUIPMENT_SLOT_END; i++)
	{
		if((pItem = GetItemInterface()->GetInventoryItem(i)))
		{
			pMaxDurability = pItem->GetUInt32Value(ITEM_FIELD_MAXDURABILITY);
			pDurability =  pItem->GetUInt32Value(ITEM_FIELD_DURABILITY);
			if(pDurability)
			{
				pNewDurability = (uint32)(pMaxDurability*percent);
				pNewDurability = (pDurability - pNewDurability);
				if(pNewDurability < 0)
					pNewDurability = 0;

				if(pNewDurability <= 0)
				{
					ApplyItemMods(pItem, i, false, true);
				}

				pItem->SetUInt32Value(ITEM_FIELD_DURABILITY,(uint32)pNewDurability);
				pItem->m_isDirty = true;
			}
		}
	}
}

void Player::JoinedChannel(Channel *c)
{
	if( c != NULL )
		m_channels.insert(c);
}

void Player::LeftChannel(Channel *c)
{
	if( c != NULL )
		m_channels.erase(c);
}

void Player::CleanupChannels()
{
	set<Channel *>::iterator i;
	Channel * c;
	for(i = m_channels.begin(); i != m_channels.end();)
	{
		c = *i;
		++i;

		c->Part(this);
	}
}

void Player::SendInitialActions()
{
	/* we can't do this the fast way on ppc, due to endianness */
	MSG_S2C::stAction_Buttons Msg;
	for(uint32 i = 0; i < PLAYER_ACTION_BUTTON_COUNT; ++i)
	{
		MSG_S2C::stAction_Buttons::stActionButton actionButton;
		actionButton.Action = mActions[i].Action;
		actionButton.Type = mActions[i].Type;
		actionButton.Misc = mActions[i].Misc;
		Msg.vActionButtons.push_back( actionButton );
	}
	m_session->SendPacket(Msg);
}

void Player::setAction(uint8 button, uint32 action, uint8 type, uint8 misc)
{
	if( button < PLAYER_ACTION_BUTTON_COUNT )
	{
		mActions[button].Action = action;
		mActions[button].Type = type;
		mActions[button].Misc = misc;
	}
	else
		MyLog::log->error( "user:%s Player::setAction button >= PLAYER_ACTION_BUTTON_COUNT", m_playerInfo->name );
}

//Groupcheck
bool Player::IsGroupMember(Player *plyr)
{
	if(m_playerInfo->m_Group != NULL)
		return m_playerInfo->m_Group->HasMember(plyr->m_playerInfo);

	return false;
}
bool Player::IsGroupLeader()
{
	{
		if(m_playerInfo->m_Group != NULL)
		{
			if(m_playerInfo->m_Group->GetLeader() == m_playerInfo)
				return true;
		}
		return false;
	}
}
int32 Player::GetOpenQuestSlot()
{
	// for Milestone-1-Release
	for (uint32 i = 0; i < 25; ++i)
		if (m_questlog[i] == NULL)
			return i;

	return -1;
}

void Player::AddToFinishedQuests(uint32 quest_id)
{
	//maybe that shouldn't be an assert, but i'll leave it for now
	//ASSERT(m_finishedQuests.find(quest_id) == m_finishedQuests.end());
	//Removed due to crash
	//If it failed though, then he's probably cheating.
	if (m_finishedQuests.find(quest_id) != m_finishedQuests.end())
		return;

	m_finishedQuests.insert(quest_id);
}

bool Player::HasFinishedQuest(uint32 quest_id)
{
	Quest * pQuest = QuestStorage.LookupEntry(quest_id);
	if(pQuest)
	{
		if( sQuestMgr.IsDailyQuest(pQuest) )
			return false;
		return (m_finishedQuests.find(quest_id) != m_finishedQuests.end());
	}
	else return false;
}

//From Mangos Project
void Player::_LoadTutorials(QueryResult * result)
{
	if(result)
	{
		 Field *fields = result->Fetch();
		 for (int iI=0; iI<8; iI++)
			 m_Tutorials[iI] = fields[iI+1].GetUInt32();
	}
	tutorialsDirty = false;
}

void Player::_SaveTutorials(QueryBuffer * buf)
{
//	if(tutorialsDirty)
//	{
		buf->AddQuery("REPLACE INTO tutorials VALUES('%u','%u','%u','%u','%u','%u','%u','%u','%u')", GetLowGUID(), m_Tutorials[0], m_Tutorials[1], m_Tutorials[2], m_Tutorials[3], m_Tutorials[4], m_Tutorials[5], m_Tutorials[6], m_Tutorials[7]);

//		tutorialsDirty = false;
//	}
}

uint64 Player::GetTutorialInt(uint32 intId )
{
	ASSERT( intId < 8 );
	return m_Tutorials[intId];
}

void Player::SetTutorialInt(uint32 intId, uint32 value)
{
	if(intId >= 8)
		return;

	ASSERT( (intId < 8) );
	m_Tutorials[intId] = value;
	tutorialsDirty = true;
}

//Player stats calculation for saving at lvl up, etc
/*void Player::CalcBaseStats()
{//static_cast< Player* >( this )->getClass() == HUNTER ||
	//TODO take into account base stats at create
	uint32 AP, RAP;
	//Save AttAck power
	if(getClass() == CLASS_ROGUE || getClass() == HUNTER)
	{
		AP = GetBaseUInt32Value(UNIT_FIELD_STAT0) + GetBaseUInt32Value(UNIT_FIELD_STAT1);
		RAP = (GetBaseUInt32Value(UNIT_FIELD_STAT1) * 2);
		SetBaseUInt32Value(UNIT_FIELD_ATTACK_POWER, AP);
		SetBaseUInt32Value(UNIT_FIELD_RANGED_ATTACK_POWER, RAP);
	}
	else
	{
		AP = (GetBaseUInt32Value(UNIT_FIELD_STAT0) * 2);
		RAP = (GetBaseUInt32Value(UNIT_FIELD_STAT1) * 2);
		SetBaseUInt32Value(UNIT_FIELD_ATTACK_POWER, AP);
		SetBaseUInt32Value(UNIT_FIELD_RANGED_ATTACK_POWER, RAP);
	}

}*/

void Player::UpdateHit(int32 hit)
{
   /*std::list<Affect*>::iterator i;
	Affect::ModList::const_iterator j;
	Affect *aff;
	uint32 in = hit;
	for (i = GetAffectBegin(); i != GetAffectEnd(); i++)
	{
		aff = *i;
		for (j = aff->GetModList().begin();j != aff->GetModList().end(); j++)
		{
			Modifier mod = (*j);
			if ((mod.GetType() == SPELL_AURA_MOD_HIT_CHANCE))
			{
				SpellEntry *spellInfo = sSpellStore.LookupEntry(aff->GetSpellId());
				if (this->canCast(spellInfo))
					in += mod.GetAmount();
			}
		}
	}
	SetHitFromSpell(in);*/
}

void Player::UpdateChances()
{


	float level = (float)getLevel();
	float agi = (float)GetFloatValue( UNIT_FIELD_STAT0 + STAT_AGILITY );

	//物理暴击 1%+（力量*3%+敏捷*1.5%）%
	float NormalAgility = 1.f;
	switch(getClass())
	{
	case CLASS_BOW:
		{
			NormalAgility = 60.33f;
		}
		break;
	default:
		{
			NormalAgility = 40.7f;
		}
		break;
	}
	float block = 5.f;//1.f + 3.f*GetFloatValue(UNIT_FIELD_STAT0+STAT_STRENGTH)/100 + 2.f * GetFloatValue( UNIT_FIELD_STAT0 + STAT_STAMINA) / 100.f;
	float cri = 0.f; //1.f + 3.f*GetFloatValue(UNIT_FIELD_STAT0+STAT_STRENGTH)/100.f +1.5f*GetFloatValue(UNIT_FIELD_STAT0+STAT_AGILITY)/100.f;

	float dodge = 5.f +  1.f * (level / 80)  * agi / NormalAgility + CalcRating(PLAYER_RATING_MODIFIER_DODGE);//0.01f * GetUInt32Value(PLAYER_RATING_MODIFIER_DODGE) / dodgeRC; 
	dodge += GetDodgeFromSpell();
	block += GetBlockFromSpell();

	map<uint32, float>::iterator itrhit = hitchance.begin();
	for(; itrhit != hitchance.end(); ++itrhit)
	{
		block -= itrhit->second;
	}
	SetFloatValue( PLAYER_BLOCK_PERCENTAGE, block );
	SetFloatValue( PLAYER_DODGE_PERCENTAGE, dodge );


	//SetFloatValue( PLAYER_CRIT_PERCENTAGE, cri );

	float fLvc = 10.f + powf(getLevel() / 10.f , 2.f) * 0.6f ;
	cri = 1.f * (GetFloatValue(UNIT_FIELD_STAT0+STAT_INTELLECT) / fLvc) + GetSpellCritFromSpell() + CalcRating(PLAYER_RATING_MODIFIER_SPELL_CRIT);


	SetFloatValue( PLAYER_SPELL_CRIT_PERCENTAGE1, cri + spellcritperc);



	float fagi = 0.f;
	switch (getClass())
	{
	case CLASS_WARRIOR:		//	武修	MASK = 1
		{
			fagi = GetFloatValue(UNIT_FIELD_STAT0+STAT_AGILITY) * 1.4f;
		}
		break;
	//case CLASS_BOW:		//	羽箭	MASK = 2
	//	{
	//		fagi = GetFloatValue(UNIT_FIELD_STAT0+STAT_AGILITY) * 0.8f;
	//	}
	//	break;
	default:
		{
			fagi = GetFloatValue(UNIT_FIELD_STAT0+STAT_AGILITY) * 1.0f;
		}
		break;
	}


	cri = 5.f + fagi/(10.f + 0.6f * powf(getLevel() / 10.f, 2.f)) +  CalcRating(PLAYER_RATING_MODIFIER_MELEE_CRIT);
	fagi = GetFloatValue(UNIT_FIELD_STAT0+STAT_AGILITY) * 0.8f;
	float rangedcri = 5.f + fagi/(10.f + 0.6f * powf(getLevel() / 10.f, 2.f)) + CalcRating(PLAYER_RATING_MODIFIER_RANGED_CRIT);

	std::list<WeaponModifier>::iterator itr = tocritchance.begin();
	for(; itr != tocritchance.end(); ++itr )
	{
		cri += itr->value;
		rangedcri += itr->value;
	}

	// 法爆= （1+智力*3%+敏捷*2%）%           （法术攻击1.3倍）
	//cri = GetSpellCritFromSpell() + 1.0f + 0.03f*GetFloatValue(UNIT_FIELD_STAT0+STAT_INTELLECT) + 0.02f*GetFloatValue(UNIT_FIELD_STAT0+STAT_SPIRIT);
	SetFloatValue( PLAYER_CRIT_PERCENTAGE, cri );
	SetFloatValue( PLAYER_RANGED_CRIT_PERCENTAGE, rangedcri);
}

void Player::UpdateChanceFields()
{
	// Update spell crit values in fields
	/*
	for(uint32 i = 0; i < 7; ++i)
	{
		SetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1 + i, SpellCritChanceSchool[i]+spellcritperc);
	}
	*/
}

void Player::UpdateAttackSpeed()
{
	uint32 speed = 2000;
	

	Item *weap = NULL;

	weap = GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_MAINHAND );
	if(weap &&( weap->GetProto()->Class == ITEM_CLASS_WEAPON && (weap->GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_BOW 
		||weap->GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_GUN || weap->GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_CROSSBOW)))
	{
		uint32 rangspeed = 2000;
		rangspeed = weap->GetProto()->Delay;
		SetUInt32Value( UNIT_FIELD_RANGEDATTACKTIME,
			( uint32 )( ( rangspeed * ( ( 100.0f - (float)m_rangedattackspeedmod ) / 100.0f ) ) * ( ( 100.0f - CalcRating(PLAYER_RATING_MODIFIER_RANGED_HASTE)) / 100.0f ) ) );
	}
	else if (weap&&(weap->GetProto()->InventoryType == INVTYPE_2HWEAPON || weap->GetProto()->InventoryType ==  INVTYPE_WEAPONMAINHAND))
	{
		speed = weap->GetProto()->Delay;
	
	}
	SetUInt32Value( UNIT_FIELD_BASEATTACKTIME,
		( uint32 )( ( speed * (( 100.0f - ( float )m_meleeattackspeedmod ) / 100.0f ) ) * ( ( 100.0f - CalcRating( PLAYER_RATING_MODIFIER_MELEE_HASTE ) ) / 100.0f ) ));
//	if(GetShapeShift()==FORM_CAT)//cat form
//	{
//		speed = 1000;
//	}else if(GetShapeShift()==FORM_BEAR || GetShapeShift()==FORM_DIREBEAR)
//	{
//		speed = 2500;
//	}
//	else //regular
//	if( !disarmed )
//	{
//		weap = GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_MAINHAND) ;
//		if( weap != NULL )
//			speed = /*weap->GetProto()->Delay*/2000;
//	}
//	uint32 agility = GetFloatValue( UNIT_FIELD_STAT0 + STAT_AGILITY );

	//Item* mainweapon = GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_MAINHAND);
	//float basespeed = 1000.f;
	/*
	if(mainweapon) //item exists
	{
		if( mainweapon->GetProto()->InventoryType == INVTYPE_2HWEAPON )
			basespeed = 2000.f;
	}
	else
		basespeed = 2000.f;
	*/
	/*speed = float2int32( basespeed *( 1.f - ( (float)agility / 5.f ) / 1440.f ) );
	SetUInt32Value( UNIT_FIELD_BASEATTACKTIME,
				    ( uint32 )( ( speed * (( 100.0f - ( float )m_meleeattackspeedmod ) / 100.0f ) ) * ( ( 100.0f - CalcRating( PLAYER_RATING_MODIFIER_MELEE_HASTE ) ) / 100.0f ) ) );*/

//	weap = GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_OFFHAND );
//	if( weap != NULL && weap->GetProto()->Class == 2 )// 2 is a weapon
//	{
//		speed = /*weap->GetProto()->Delay*/2000;
		//SetUInt32Value( UNIT_FIELD_BASEATTACKTIME_01,
					//    ( uint32 )( ( speed * ( ( 100.0f - ( float )m_meleeattackspeedmod ) / 100.0f ) ) * ( ( 100.0f - CalcRating( PLAYER_RATING_MODIFIER_MELEE_HASTE ) ) / 100.0f ) ) );
//	}//


/*
	if(getClass() == CLASS_BOW)
	{
		SetUInt32Value( UNIT_FIELD_RANGEDATTACKTIME,
			( uint32 )( ( speed * ( ( 100.0f - (float)m_rangedattackspeedmod ) / 100.0f ) ) * ( ( 100.0f - CalcRating(PLAYER_RATING_MODIFIER_RANGED_HASTE)) / 100.0f ) ) );
	}

	*/
}

void Player::UpdateStats()
{
	// formulas from wowwiki

	int32 AP = 0;
	int32 RAP = 0;
	int32 hpdelta = 0;
	int32 manadelta = 0;

	uint32 str = GetFloatValue(UNIT_FIELD_STAT0);
	uint32 agi = GetFloatValue(UNIT_FIELD_STAT1);
	uint32 lev = getLevel();

	SetUInt32Value( UNIT_FIELD_ATTACK_POWER, AP );

	float LongDisAttackPower = 2.f * getLevel() + 3.f * GetFloatValue(UNIT_FIELD_STAT0 + STAT_AGILITY);
	SetUInt32Value(UNIT_FIELD_RANGED_ATTACK_POWER, int(LongDisAttackPower));

	float AttackPower = GetFloatValue(UNIT_FIELD_STAT0 + STAT_STRENGTH) * 2.f + 0.5f * GetFloatValue(UNIT_FIELD_STAT0 + STAT_AGILITY) + getLevel();
	SetUInt32Value(UNIT_FIELD_ATTACK_POWER, int(AttackPower));

	LevelInfo* lvlinfo = objmgr.GetLevelInfo( this->getRace(), this->getClass(), lev );
	int32 stam = 0;
	int32 intl = 0;
	if( lvlinfo != NULL )
	{
		stam = lvlinfo->Stat[STAT_STAMINA];
		intl = lvlinfo->Stat[STAT_INTELLECT];
	}
	int32 hp = GetUInt32Value( UNIT_FIELD_BASE_HEALTH );
	stam += GetUInt32Value( UNIT_FIELD_POSSTAT2 ) - GetUInt32Value( UNIT_FIELD_NEGSTAT2 );
	int32 res = hp + stam * 10 + m_healthfromspell + m_healthfromitems;

	if (res < 1)
		res = 1;

	int32 oldmaxhp = GetUInt32Value( UNIT_FIELD_MAXHEALTH );

	if( res < hp ) res = hp;
	SetUInt32Value( UNIT_FIELD_MAXHEALTH, res );

	//SetUInt32Value( UNIT_FIELD_RANGED_ATTACK_POWER, RAP );

	/*
	LevelInfo* lvlinfo = objmgr.GetLevelInfo( this->getRace(), this->getClass(), lev );

	if( lvlinfo != NULL )
	{
		hpdelta = lvlinfo->HP;
		manadelta = lvlinfo->Mana;
	}

	lvlinfo = objmgr.GetLevelInfo( this->getRace(), this->getClass(), 1 );

	if( lvlinfo != NULL )
	{
		hpdelta -= lvlinfo->HP;
		manadelta -= lvlinfo->Mana;
	}
	*/

	//int32 hp = GetUInt32Value( UNIT_FIELD_BASE_HEALTH );

	//int32 stat_bonus = GetUInt32Value( UNIT_FIELD_POSSTAT2 ) - GetUInt32Value( UNIT_FIELD_NEGSTAT2 );
	//if ( stat_bonus < 0 )
	//	stat_bonus = 0; //avoid of having negative health
	//int32 bonus = (int32)( (float)stat_bonus * 10 + m_healthfromspell + m_healthfromitems );

	//int32 res = hp + bonus + hpdelta;
	////int32 oldmaxhp = GetUInt32Value( UNIT_FIELD_MAXHEALTH );

	//if( res < hp ) res = hp;
	//SetUInt32Value( UNIT_FIELD_MAXHEALTH, res );

	//if( cl != CLASS_WARRIOR && cl != CLASS_ROGUE )
	int32 stat_bonus = 0;
	int32 bonus = 0;
	{
		// MP
		SetUInt32Value(UNIT_FIELD_BASE_MANA, intl * 10);
		int32 mana = GetUInt32Value( UNIT_FIELD_BASE_MANA );

		stat_bonus += GetUInt32Value( UNIT_FIELD_POSSTAT3 ) - GetUInt32Value( UNIT_FIELD_NEGSTAT3 );
		if ( stat_bonus < 0 )
			stat_bonus = 0; //avoid of having negative mana
		bonus = stat_bonus * 10 + m_manafromspell + m_manafromitems ;

		res = mana + bonus + manadelta;
		if( res < mana )res = mana;
		SetUInt32Value(UNIT_FIELD_MAXPOWER1, res);

		//Manaregen
//		const static float ClassMultiplier[12] = {0.0f,0.0f,0.2f,0.2f,0.0f,0.25f,0.0f,0.2f,0.25f,0.2f,0.0f,0.225f};
//		const static float ClassFlatMod[12] = {0.0f,0.0f,15.0f,15.0f,0.0f,12.5f,0.0f,15.0f,12.5f,15.0f,0.0f,15.0f};
		uint32 Spirit = GetUInt32Value( UNIT_FIELD_STAT4 );
//		float amt = (Spirit*ClassMultiplier[cl]+ClassFlatMod[cl])*PctPowerRegenModifier[POWER_TYPE_MANA]*0.5f;
		SetFloatValue( PLAYER_FIELD_MOD_MANA_REGEN, (float)Spirit * 0.25f );
//		SetFloatValue(PLAYER_FIELD_MOD_MANA_REGEN_INTERRUPT,amt*m_ModInterrMRegenPCT/100.0f+m_ModInterrMRegen/5.0f);
	}

	/////////////////////RATINGS STUFF/////////////////
	float cast_speed = CalcRating( PLAYER_RATING_MODIFIER_SPELL_HASTE );
	if( cast_speed >= 50 ) 		// spell haste/slow is limited to 100% fast
		cast_speed = 50;
	if( cast_speed != SpellHasteRatingBonus )
	{
		ModFloatValue( UNIT_MOD_CAST_SPEED, ( SpellHasteRatingBonus - cast_speed ) / 100.0f );

		SpellHasteRatingBonus = cast_speed;
	}
	////////////////////RATINGS STUFF//////////////////////

	// Shield Block
// 	Item* shield = GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_OFFHAND);
// 	if( shield != NULL && shield->GetProto()->InventoryType == INVTYPE_SHIELD )
// 	{
// 		float block_multiplier = ( 100.0f + float( m_modblockabsorbvalue ) ) / 100.0f;
// 		if( block_multiplier < 1.0f )block_multiplier = 1.0f;
//
// 		int32 blockable_damage = float2int32( float( shield->GetProto()->Block ) +( float( m_modblockvaluefromspells + GetUInt32Value( PLAYER_RATING_MODIFIER_BLOCK ) ) * block_multiplier ) + ( ( float( str ) / 20.0f ) - 1.0f ) );
// 		SetUInt32Value( PLAYER_SHIELD_BLOCK, blockable_damage );
// 	}
// 	else
	{
		SetUInt32Value( PLAYER_SHIELD_BLOCK, 0 );
	}



	UpdateChances();
	CalcDamage();

	float power_modifier = float(MaxPowerModPctPos - MaxPowerModPctNeg) / 100.f + 1.f;
	float health_modifier = float(MaxHealthModPctPos - MaxHealthModPctNeg) / 100.f + 1.f;

	SetUInt32Value( UNIT_FIELD_MAXHEALTH, (uint32)( (float)GetUInt32Value( UNIT_FIELD_MAXHEALTH ) * health_modifier ) );
	SetUInt32Value( UNIT_FIELD_MAXPOWER1, (uint32)( (float)GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) * power_modifier ) );

	if( GetUInt32Value( UNIT_FIELD_HEALTH ) > GetUInt32Value( UNIT_FIELD_MAXHEALTH ) )
		SetUInt32Value( UNIT_FIELD_HEALTH, GetUInt32Value( UNIT_FIELD_MAXHEALTH ) );

	if( GetUInt32Value( UNIT_FIELD_POWER1 ) > GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) )
		SetUInt32Value( UNIT_FIELD_POWER1, GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) );

	UpdateAttackSpeed();
}

uint32 Player::SubtractRestXP(uint32 amount)
{
	if(GetUInt32Value(UNIT_FIELD_LEVEL) >= GetUInt32Value(PLAYER_FIELD_MAX_LEVEL))		// Save CPU, don't waste time on this if you've reached max_level
		amount = 0;

	int32 restAmount = m_restAmount - (amount << 1);									// remember , we are dealing with xp without restbonus, so multiply by 2

	if( restAmount < 0)
		m_restAmount = 0;
	else
		m_restAmount = restAmount;

	MyLog::log->debug("REST : Subtracted %d rest XP to a total of %d", amount, m_restAmount);
	UpdateRestState();																	// Update clients interface with new values.
	return amount;
}

void Player::AddCalculatedRestXP(uint32 seconds)
{
	// At level one, players will all start in the normal tier.
	// When a player rests in a city or at an inn they will gain rest bonus at a very slow rate.
	// Eight hours of rest will be needed for a player to gain one "bubble" of rest bonus.
	// At any given time, players will be able to accumulate a maximum of 30 "bubbles" worth of rest bonus which
	// translates into approximately 1.5 levels worth of rested play (before your character returns to normal rest state).
	// Thanks to the comforts of a warm bed and a hearty meal, players who rest or log out at an Inn will
	// accumulate rest credit four times faster than players logged off outside of an Inn or City.
	// Players who log out anywhere else in the world will earn rest credit four times slower.
	// http://www.worldofwarcraft.com/info/basics/resting.html


	// Define xp for a full bar ( = 20 bubbles)
	uint32 xp_to_lvl = uint32(lvlinfo->XPToNextLevel);

	// get RestXP multiplier from config.
	float bubblerate = sWorld.getRate(RATE_RESTXP);

	// One bubble (5% of xp_to_level) for every 8 hours logged out.
	// if multiplier RestXP (from ascent.config) is f.e 2, you only need 4hrs/bubble.
	uint32 rested_xp = uint32(0.05f * xp_to_lvl * ( seconds / (3600 * ( 8 / bubblerate))));

	// if we are at a resting area rest_XP goes 4 times faster (making it 1 bubble every 2 hrs)
	if (m_isResting)
		rested_xp <<= 2;

	// Add result to accumulated rested XP
	m_restAmount += uint32(rested_xp);

	// and set limit to be max 1.5 * 20 bubbles * multiplier (1.5 * xp_to_level * multiplier)
	if (m_restAmount > xp_to_lvl + (uint32)((float)( xp_to_lvl >> 1 ) * bubblerate ))
		m_restAmount = xp_to_lvl + (uint32)((float)( xp_to_lvl >> 1 ) * bubblerate );

	MyLog::log->debug("REST : Add %d rest XP to a total of %d, RestState %d", rested_xp, m_restAmount,m_isResting);

	// Update clients interface with new values.
	UpdateRestState();
}

void Player::UpdateRestState()
{
	if(m_restAmount && GetUInt32Value(UNIT_FIELD_LEVEL) < GetUInt32Value(PLAYER_FIELD_MAX_LEVEL))
		m_restState = RESTSTATE_RESTED;
	else
		m_restState = RESTSTATE_NORMAL;

	// Update RestState 100%/200%
	SetUInt32Value(PLAYER_BYTES_2, ((GetUInt32Value(PLAYER_BYTES_2) & 0x00FFFFFF) | (m_restState << 24)));

	//update needle (weird, works at 1/2 rate)
	SetUInt32Value(PLAYER_REST_STATE_EXPERIENCE, m_restAmount >> 1);
}

void Player::ApplyPlayerRestState(bool apply)
{
	if(apply)
	{
		m_restState = RESTSTATE_RESTED;
		m_isResting = true;
		SetFlag(PLAYER_FLAGS, PLAYER_FLAG_RESTING);	//put zzz icon
	}
	else
	{
		m_isResting = false;
		RemoveFlag(PLAYER_FLAGS,PLAYER_FLAG_RESTING);	//remove zzz icon
	}
	UpdateRestState();
}

#define CORPSE_VIEW_DISTANCE 1600 // 40*40

bool Player::CanSee(Object* obj) // * Invisibility & Stealth Detection - Partha *
{
	if (obj == this)
	   return true;

	uint32 object_type = obj->GetTypeId();

	if(getDeathState() == CORPSE) // we are dead and we have released our spirit
	{
		if(object_type == TYPEID_PLAYER)
		{
			Player *pObj = static_cast< Player* >(obj);

			if(myCorpse && myCorpse->GetDistanceSq(obj) <= CORPSE_VIEW_DISTANCE)
				return !pObj->m_isGmInvisible; // we can see all players within range of our corpse except invisible GMs

			if(m_deathVision) // if we have arena death-vision we can see all players except invisible GMs
				return !pObj->m_isGmInvisible;

			return (pObj->getDeathState() == CORPSE); // we can only see players that are spirits
		}

		if(myCorpse)
		{
			if(myCorpse == obj)
				return true;

			if(myCorpse->GetDistanceSq(obj) <= CORPSE_VIEW_DISTANCE)
				return true; // we can see everything within range of our corpse
		}

		if(m_deathVision) // if we have arena death-vision we can see everything
			return true;

		if(object_type == TYPEID_UNIT)
		{
			Unit *uObj = static_cast<Unit *>(obj);

			return uObj->IsSpiritHealer(); // we can't see any NPCs except spirit-healers
		}

		return false;
	}
	//------------------------------------------------------------------

	switch(object_type) // we are alive or we haven't released our spirit yet
	{
		case TYPEID_PLAYER:
			{
				Player *pObj = static_cast< Player* >(obj);

				if(pObj->m_invisible) // Invisibility - Detection of Players
				{
					if(pObj->getDeathState() == CORPSE)
						return bGMTagOn; // only GM can see players that are spirits

					if(GetGroup() && pObj->GetGroup() == GetGroup() // can see invisible group members except when dueling them
							&& DuelingWith != pObj)
						return true;

					if(pObj->stalkedby == GetGUID()) // Hunter's Mark / MindVision is visible to the caster
						return true;

					if(m_invisDetect[INVIS_FLAG_NORMAL] < 1 // can't see invisible without proper detection
							|| pObj->m_isGmInvisible) // can't see invisible GM
						return bGMTagOn; // GM can see invisible players
				}

				if(pObj->IsStealth()) // Stealth Detection (  I Hate Rogues :P  )
				{
					if(GetGroup() && pObj->GetGroup() == GetGroup() // can see stealthed group members except when dueling them
							&& DuelingWith != pObj)
						return true;

					if(pObj->stalkedby == GetGUID()) // Hunter's Mark / MindVision is visible to the caster
						return true;

					if(isInFront(pObj)) // stealthed player is in front of us
					{
						// Detection Range = 5yds + (Detection Skill - Stealth Skill)/5
						if(getLevel() < 70)
							detectRange = 5.0f + getLevel() + 0.2f * (float)(GetStealthDetectBonus() - pObj->GetStealthLevel());
						else
							detectRange = 75.0f + 0.2f * (float)(GetStealthDetectBonus() - pObj->GetStealthLevel());
						// Hehe... stealth skill is increased by 5 each level and detection skill is increased by 5 each level too.
						// This way, a level 70 should easily be able to detect a level 4 rogue (level 4 because that's when you get stealth)
						//	detectRange += 0.2f * ( getLevel() - pObj->getLevel() );
						if(detectRange < 1.0f) detectRange = 1.0f; // Minimum Detection Range = 1yd
					}
					else // stealthed player is behind us
					{
						if(GetStealthDetectBonus() > 1000) return true; // immune to stealth
						else detectRange = 0.0f;
					}

					detectRange += GetFloatValue(UNIT_FIELD_BOUNDINGRADIUS); // adjust range for size of player
					detectRange += pObj->GetFloatValue(UNIT_FIELD_BOUNDINGRADIUS); // adjust range for size of stealthed player
					//MyLog::log->info( "Player::CanSee(%s): detect range = %f yards (%f ingame units), cansee = %s , distance = %f" , pObj->GetName() , detectRange , detectRange * detectRange , ( GetDistance2dSq(pObj) > detectRange * detectRange ) ? "yes" : "no" , GetDistanceSq(pObj) );
					if(GetDistanceSq(pObj) > detectRange * detectRange)
						return bGMTagOn; // GM can see stealthed players
				}

				return true;
			}
		//------------------------------------------------------------------

		case TYPEID_UNIT:
			{
				Unit *uObj = static_cast<Unit *>(obj);

				if(uObj->IsSpiritHealer()) // can't see spirit-healers when alive
					return false;

				if(uObj->m_invisible // Invisibility - Detection of Units
						&& m_invisDetect[uObj->m_invisFlag] < 1) // can't see invisible without proper detection
					return bGMTagOn; // GM can see invisible units

				return true;
			}
		//------------------------------------------------------------------

		case TYPEID_GAMEOBJECT:
			{
				GameObject *gObj = static_cast<GameObject *>(obj);

				if( gObj->m_AlwaysCanSee )
					return true;

				if(gObj->invisible) // Invisibility - Detection of GameObjects
				{
					uint64 owner = gObj->GetUInt64Value(OBJECT_FIELD_CREATED_BY);

					if(GetGUID() == owner) // the owner of an object can always see it
						return true;

					if(GetGroup())
					{
						PlayerInfo * inf = objmgr.GetPlayerInfo((uint32)owner);
						if(inf && GetGroup()->HasMember(inf))
							return true;
					}

					if(m_invisDetect[gObj->invisibilityFlag] < 1) // can't see invisible without proper detection
						return bGMTagOn; // GM can see invisible objects
				}

				return true;
			}
		//------------------------------------------------------------------
		case TYPEID_ITEM:
		case TYPEID_CONTAINER:
			{
				Item* it = static_cast<Item*>( obj );
				if( it->GetUInt32Value( ITEM_FIELD_OWNER ) == 0 )
					return true;
				else
					return false;
			}
			break;
		default:
			return true;
	}
}

void Player::AddInRangeObject(Object* pObj)
{
	//Send taxi move if we're on a taxi
	if (m_CurrentTaxiPath && (pObj->GetTypeId() == TYPEID_PLAYER))
	{
		uint32 ntime = getMSTime();

		if (ntime > m_taxi_ride_time)
			m_CurrentTaxiPath->SendMoveForTime( this, static_cast< Player* >( pObj ), ntime - m_taxi_ride_time);
		/*else
			m_CurrentTaxiPath->SendMoveForTime( this, static_cast< Player* >( pObj ), m_taxi_ride_time - ntime);*/
	}

	Unit::AddInRangeObject(pObj);

	//if the object is a unit send a move packet if they have a destination
	//if( pObj->IsCreature() )
	{
		//add an event to send move update have to send guid as pointer was causing a crash :(
		//sEventMgr.AddEvent( static_cast< Creature* >( pObj )->GetAIInterface(), &AIInterface::SendCurrentMove, this, EVENT_UNIT_SENDMOVE, 10, 1, 0);
		//static_cast< Creature* >( pObj )->GetAIInterface()->SendCurrentMove(this);
		//AddNeedSendCurrentMoveCreature( pObj->GetGUID() );
		//((Unit*)pObj)->GetAIInterface()->SendCurrentMove( this );
	}
}

void Player::OnRemoveInRangeObject(Object* pObj)
{
	//if (/*!CanSee(pObj) && */IsVisible(pObj))
	//{
		//RemoveVisibleObject(pObj);
	//}
	if(m_tempSummon == pObj)
	{
		m_tempSummon->RemoveFromWorld(false, true);
		if(m_tempSummon)
			m_tempSummon->SafeDelete();

		m_tempSummon = 0;
		SetUInt64Value(UNIT_FIELD_SUMMON, 0);
	}

	m_visibleObjects.erase(pObj);
	Unit::OnRemoveInRangeObject(pObj);

	if( pObj == m_CurrentCharm )
	{
		Unit * p = m_CurrentCharm;

		this->UnPossess();
		if(m_currentSpell)
			m_currentSpell->cancel();	   // cancel the spell
		m_CurrentCharm=NULL;

		if( p->m_temp_summon&&p->GetTypeId() == TYPEID_UNIT )
			static_cast< Creature* >( p )->SafeDelete();
	}

	/* comment by gui
	if(pObj == m_Summon)
	{
		if(m_Summon->IsSummon())
		{
			m_Summon->Dismiss(true);
		}
		else
		{
			m_Summon->Remove(true, true, false);
		}
		if(m_Summon)
		{
			m_Summon->ClearPetOwner();
			m_Summon = 0;
		}
	}
	*/

	/* wehee loop unrolling */
/*	if(m_spellTypeTargets[0] == pObj)
		m_spellTypeTargets[0] = NULL;
	if(m_spellTypeTargets[1] == pObj)
		m_spellTypeTargets[1] = NULL;
	if(m_spellTypeTargets[2] == pObj)
		m_spellTypeTargets[2] = NULL;*/
	if(pObj->IsUnit())
	{
		for(uint32 x = 0; x < NUM_SPELL_TYPE_INDEX; ++x)
			if(m_spellIndexTypeTargets[x] == pObj->GetGUID())
				m_spellIndexTypeTargets[x] = 0;
	}
}

void Player::ClearInRangeSet()
{
	m_visibleObjects.clear();
	Unit::ClearInRangeSet();
}

void Player::EventCannibalize(uint32 amount)
{
	uint32 amt = (GetUInt32Value(UNIT_FIELD_MAXHEALTH)*amount)/100;

	uint32 newHealth = GetUInt32Value(UNIT_FIELD_HEALTH) + amt;

	if(newHealth <= GetUInt32Value(UNIT_FIELD_MAXHEALTH))
		SetUInt32Value(UNIT_FIELD_HEALTH, newHealth);
	else
		SetUInt32Value(UNIT_FIELD_HEALTH, GetUInt32Value(UNIT_FIELD_MAXHEALTH));

	cannibalizeCount++;
	if(cannibalizeCount == 5)
		SetUInt32Value(UNIT_NPC_EMOTESTATE, 0);

	MSG_S2C::stAura_Periodic_Log Msg;
	Msg.caster_guid = GetNewGUID();
	Msg.target_guid = GetNewGUID();
	Msg.spell_entry = 20577;

	Msg.school		= FLAG_PERIODIC_HEAL;
	Msg.amount		= amt;							// amount of done to target / heal / damage
	SendMessageToSet(Msg, true);
}

void Player::EventReduceDrunk(bool full)
{
	uint8 drunk = ((GetUInt32Value(PLAYER_BYTES_3) >> 24) & 0xFF);
	if(full) drunk = 0;
	else drunk -= 10;
	SetUInt32Value(PLAYER_BYTES_3, ((GetUInt32Value(PLAYER_BYTES_3) & 0xFFFF00FF) | (drunk << 8)));
	if(drunk == 0) sEventMgr.RemoveEvents(this, EVENT_PLAYER_REDUCEDRUNK);
}

bool Player::HasQuestForItem(uint32 itemid)
{
	Quest *qst;
	for( uint32 i = 0; i < QUEST_MAX_COUNT; ++i )
	{
		if( m_questlog[i] != NULL )
		{
			qst = m_questlog[i]->GetQuest();

			// Check the item_quest_association table for an entry related to this item
			QuestAssociationList *tempList = QuestMgr::getSingleton().GetQuestAssociationListForItemId( itemid );
			if( tempList != NULL )
			{
				QuestAssociationList::iterator it;
				for (it = tempList->begin(); it != tempList->end(); ++it)
				{
					if ( ((*it)->qst == qst) && (GetItemInterface()->GetItemCount( itemid ) < (*it)->item_count) )
					{
						return true;
					} // end if
				} // end for
			} // end if

			// No item_quest association found, check the quest requirements
			if( !qst->count_required_item )
				continue;

			for( uint32 j = 0; j < 4; ++j )
				if( qst->required_item[j] == itemid && ( GetItemInterface()->GetItemCount( itemid ) < qst->required_itemcount[j] ) )
					return true;
		}
	}
	return false;
}

/*Loot type MUST be
1-corpse, go
2-skinning/herbalism/minning
3-Fishing
*/
void Player::SendLoot(uint64 guid,uint8 loot_type)
{
	Group * m_Group = m_playerInfo->m_Group;
	if(!IsInWorld()) return;
	Loot * pLoot = NULL;
	uint32 guidtype = GET_TYPE_FROM_GUID(guid);
	int8 loot_method = -1;

	if(guidtype == HIGHGUID_TYPE_UNIT)
	{
		Creature* pCreature = GetMapMgr()->GetCreature(GET_LOWGUID_PART(guid));
		if(!pCreature)return;

		if( pCreature->TaggerGuid == GetGUID() || (m_Group && m_Group->HasMember(pCreature->TaggerGuid)) )
		{

		}
		else
		{
			return;
		}

		pLoot=&pCreature->loot;
		m_currentLoot = pCreature->GetGUID();
		loot_method = pCreature->m_lootMethod;
	}else if(guidtype == HIGHGUID_TYPE_GAMEOBJECT)
	{
		GameObject* pGO = GetMapMgr()->GetGameObject(GET_LOWGUID_PART(guid));
		if(!pGO)return;
		pGO->SetUInt32Value(GAMEOBJECT_STATE,0);
		pLoot=&pGO->loot;
		m_currentLoot = pGO->GetGUID();
		//loot_method = PARTY_LOOT_FFA;
	}
	else if((guidtype == HIGHGUID_TYPE_PLAYER) )
	{
		Player *p=GetMapMgr()->GetPlayer((uint32)guid);
		if(!p)return;
		pLoot=&p->loot;
		m_currentLoot = p->GetGUID();
	}
	else if( (guidtype == HIGHGUID_TYPE_CORPSE))
	{
		Corpse *pCorpse = objmgr.GetCorpse((uint32)guid);
		if(!pCorpse)return;
		pLoot=&pCorpse->loot;
		m_currentLoot = pCorpse->GetGUID();
	}
	else if( (guidtype == HIGHGUID_TYPE_ITEM) )
	{
		Item *pItem = GetItemInterface()->GetItemByGUID(guid);
		if(!pItem)
			return;
		pLoot = pItem->loot;
		m_currentLoot = pItem->GetGUID();
	}

	if(!pLoot)
	{
		// something whack happened.. damn cheaters..
		return;
	}

	if( loot_method < 0 )
	{
		// not set
		if( m_Group != NULL )
			loot_method = m_Group->GetMethod();
		else
			loot_method = PARTY_LOOT_FFA;
	}

	// add to looter set
	pLoot->looters.insert(GetLowGUID());

	MSG_S2C::stLoot_Response Msg;

	m_lootGuid = guid;

	Msg.lootGuid = guid;
	Msg.loot_type = loot_type;
	Msg.gold = pLoot->gold;

	std::vector<__LootItem>::iterator iter=pLoot->items.begin();
	uint32 count=0;
	uint8 slottype = 0;

	for(uint32 x=0;iter!=pLoot->items.end();iter++,x++)
	{
		if (iter->iItemsCount == 0)
			continue;

		LooterSet::iterator itr = iter->has_looted.find(GetLowGUID());
		if (iter->has_looted.end() != itr)
			continue;

		ItemPrototype* itemProto =iter->item.itemproto;
		if (!itemProto)
			continue;
        //quest items check. type 4/5
        //quest items that dont start quests.
        if((itemProto->Bonding == ITEM_BIND_QUEST_DROP) && (itemProto->QuestId) && !HasQuestForItem(iter->item.itemproto->ItemId))
            continue;

        //quest items that start quests need special check to avoid drops all the time.
        if((itemProto->Bonding == ITEM_BIND_QUEST_DROP) && (itemProto->QuestId) && !GetQuestLogForEntry(itemProto->QuestId))
            continue;

        if((itemProto->Bonding == ITEM_BIND_QUEST_DROP) && (itemProto->QuestId) && HasFinishedQuest(itemProto->QuestId))
            continue;

        //check for starting item quests that need questlines.
        if((itemProto->QuestId && itemProto->Bonding != ITEM_BIND_QUEST_DROP))
        {
            bool HasRequiredQuests = true;
            Quest * pQuest = QuestStorage.LookupEntry(itemProto->QuestId);
            if(pQuest)
            {
                //check if its a questline.
                for(uint32 i = 0; i < pQuest->count_requiredquests; i++)
                {
                    if(pQuest->required_quests[i])
                    {
                        if(!HasFinishedQuest(pQuest->required_quests[i]) || GetQuestLogForEntry(pQuest->required_quests[i]))
                        {
                            HasRequiredQuests = false;
                            break;
                        }
                    }
                }
                if(!HasRequiredQuests)
                    continue;
            }
        }


		slottype = 0;
		if(m_Group != NULL && loot_type < 2)
		{
			switch(loot_method)
			{
			case PARTY_LOOT_MASTER:
				slottype = 2;
				break;
			case PARTY_LOOT_GROUP:
			case PARTY_LOOT_RR:
			case PARTY_LOOT_NBG:
				slottype = 1;
				//slottype = 0;
				break;
			default:
				slottype = 0;
				break;
			}
			// only quality items are distributed
			if(itemProto->Quality < m_Group->GetThreshold())
			{
				slottype = 0;
			}

			/* if all people passed anyone can loot it? :P */
			if(iter->passed)
				slottype = 0;					// All players passed on the loot

			//if it is ffa loot and not an masterlooter
			if(iter->ffa_loot && slottype != 2)
				slottype = 0;
		}

		MSG_S2C::stLoot_Response::stLootItem lootItem;
		lootItem.x = uint8(x);
		lootItem.entry = uint32(itemProto->ItemId);
		lootItem.count = uint32(iter->iItemsCount);//nr of items of this type
		lootItem.display_id = uint32(iter->item.displayid);
		//data << uint32(iter->iRandomSuffix ? iter->iRandomSuffix->id : 0);
		//data << uint32(iter->iRandomProperty ? iter->iRandomProperty->ID : 0);

		if(  itemProto->SubClass < ITEM_SUBCLASS_ARMOR_GUAIWUBEIBU )
		{
			if(iter->iRandomSuffix)
			{
				lootItem.factor = Item::GenerateRandomSuffixFactor(itemProto);
				lootItem.random_property = int32(-int32(iter->iRandomSuffix->id));
			}
			else if(iter->iRandomProperty)
			{
				lootItem.factor = uint32(0);
				lootItem.random_property = uint32(iter->iRandomProperty->ID);
			}
			else
			{
				lootItem.factor = uint32(0);
				lootItem.random_property = (0);
			}
		}
		else
		{
			lootItem.factor = uint32(0);
			lootItem.random_property = (0);
		}

		lootItem.slot_type = slottype;   // "still being rolled for" flag
		Msg.vLootItems.push_back( lootItem );

		if(slottype == 1)
		{
			if(iter->roll == NULL && !iter->passed)
			{
				int32 ipid = 0;
				uint32 factor=0;
				if(iter->iRandomProperty)
					ipid=iter->iRandomProperty->ID;
				else if(iter->iRandomSuffix)
				{
					ipid = -int32(iter->iRandomSuffix->id);
					factor=Item::GenerateRandomSuffixFactor(iter->item.itemproto);
				}

				if(iter->item.itemproto)
				{
					iter->roll = new LootRoll(60000, (m_Group != NULL ? m_Group->GetNearbyMemberCount( this ) : 1),  guid, x, iter->item.itemproto->ItemId, factor, uint32(ipid), GetMapMgr());

					MSG_S2C::stMsg_Loot_Start_Roll MsgStartLoot;
					MsgStartLoot.loot_guid = guid;
					MsgStartLoot.x	= x;
					MsgStartLoot.itemid = uint32(iter->item.itemproto->ItemId);
					MsgStartLoot.factor = uint32(factor);
					MsgStartLoot.mapid = GetMapId();
					MsgStartLoot.instanceid = GetInstanceID();
					if(iter->iRandomProperty)
						MsgStartLoot.RandomProperty = uint32(iter->iRandomProperty->ID);
					else if(iter->iRandomSuffix)
						MsgStartLoot.RandomProperty = uint32(ipid);
					else
						MsgStartLoot.RandomProperty = uint32(0);

					MsgStartLoot.countdown = uint32(60000); // countdown
					if( m_Group )
						m_Group->SendMessage2NearbyMembers( MsgStartLoot );
				}

				Group * pGroup = m_playerInfo->m_Group;
				if(pGroup)
				{
					for(uint32 i = 0; i < pGroup->GetSubGroupCount(); ++i)
					{
						for(GroupMembersSet::iterator itr = pGroup->GetSubGroup(i)->GetGroupMembersBegin(); itr != pGroup->GetSubGroup(i)->GetGroupMembersEnd(); ++itr)
						{
							PlayerInfo* plyinfo = *itr;
							if(plyinfo->m_loggedInPlayer && plyinfo->m_loggedInPlayer->GetItemInterface()->CanReceiveItem(itemProto, iter->iItemsCount) == 0)
							{
								if( plyinfo->m_loggedInPlayer->m_passOnLoot )
									iter->roll->PlayerRolled( plyinfo->m_loggedInPlayer, 3 );		// passed
								//else if( plyinfo->m_loggedInPlayer->IsInRangeSet( this ) )
									//plyinfo->m_loggedInPlayer->GetSession()->SendPacket(Msg);
							}
						}
					}
				}
		
				GetSession()->SendPacket(Msg);
			
			}
		}
		count++;
	}

	if( count == 0 && !loot.gold )
	{
		loot.looters.erase(GetLowGUID());
		if(guidtype == HIGHGUID_TYPE_UNIT)
		{
			Creature* pCreature = GetMapMgr()->GetCreature(GET_LOWGUID_PART(guid));
			if(!pCreature)return;
			//pCreature->m_corpseEvent = true;
		}
	}
	GetSession ()->SendPacket(Msg);
	SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOOTING);
}



void Player::EventAllowTiggerPort(bool enable)
{
	m_AllowAreaTriggerPort = enable;
}

uint32 Player::CalcTalentResetCost(uint32 resetnum)
{

	if(resetnum ==0 )
		return  10000;
	else
	{
		if(resetnum>10)
		return  500000;
		else return resetnum*50000;
	}
}

void Player::SendTalentResetConfirm()
{
	MSG_S2C::stTalent_Wipe_Confirm Msg;
	Msg.player_guid = GetGUID();
	Msg.cost = CalcTalentResetCost(GetTalentResetTimes());
	GetSession()->SendPacket(Msg);
}
void Player::SendPetUntrainConfirm()
{
	Pet* pPet = GetSummon();
	if( pPet == NULL )
		return;
	MSG_S2C::stPet_UnLearn_Confirm Msg;
	Msg.pet_guid = pPet->GetGUID();
	Msg.unlearn_cost = pPet->GetUntrainCost();
	GetSession()->SendPacket( Msg );
}

int32 Player::CanShootRangedWeapon( uint32 spellid, Unit* target, bool autoshot )
{
	SpellEntry* spellinfo = dbcSpell.LookupEntry( spellid );

	if( spellinfo == NULL )
		return -1;
	//MyLog::log->info( "Canshootwithrangedweapon!?!? spell: [%u] %s" , spellinfo->Id , spellinfo->Name );

	// Check if Morphed
	if( polySpell > 0 )
		return SPELL_FAILED_NOT_SHAPESHIFT;

	// Check ammo
// 	Item* itm = GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_RANGED );
	Item* itm = GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_MAINHAND );
	if( itm == NULL || !( itm->GetProto()->Class == ITEM_CLASS_WEAPON && (itm->GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_BOW
		|| itm->GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_GUN || itm->GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_CROSSBOW)) )
		return SPELL_FAILED_NO_AMMO;

	// Check ammo level
// 	ItemPrototype * iprot=ItemPrototypeStorage.LookupEntry(GetUInt32Value(PLAYER_AMMO_ID));
// 	if( iprot && getLevel()< iprot->RequiredLevel)
// 		return SPELL_FAILED_LOWLEVEL;

	// Player has clicked off target. Fail spell.
	if( m_curSelection != m_AutoShotTarget )
		return SPELL_FAILED_INTERRUPTED;

	// Check if target is allready dead
	if( target->isDead() )
		return SPELL_FAILED_TARGETS_DEAD;

	// Check if in line of sight (need collision detection).
#ifdef COLLISION
	if (GetMapId() == target->GetMapId() && !CollideInterface.CheckLOS(GetMapIDForCollision(),GetPositionNC(),target->GetPositionNC()))
		return SPELL_FAILED_LINE_OF_SIGHT;
#endif

	// Check if we aren't casting another spell allready
	if( GetCurrentSpell() )
		return -1;

	// Supalosa - The hunter ability Auto Shot is using Shoot range, which is 5 yards shorter.
	// So we'll use 114, which is the correct 35 yard range used by the other Hunter abilities (arcane shot, concussive shot...)
	uint8 fail = 0;
	uint32 rIndex = spellinfo->rangeIndex;
	SpellRange* range = dbcSpellRange.LookupEntry( rIndex );
	float minrange = GetMinRange( range );
	float dist = CalcDistance( this, target );
	float maxr = GetMaxRange( range ) + 2.52f;

	if( spellinfo->SpellGroupType )
	{
		SM_FFValue( this->SM_FRange, &maxr, spellinfo->SpellGroupType );
		SM_PFValue( this->SM_PRange, &maxr, spellinfo->SpellGroupType );
	}

	//float bonusRange = 0;
	// another hackfix: bonus range from hunter talent hawk eye: +2/4/6 yard range to ranged weapons
	//if(autoshot)
	//SM_FFValue( SM_FRange, &bonusRange, dbcSpell.LookupEntry( 75 )->SpellGroupType ); // HORRIBLE hackfixes :P
	// Partha: +2.52yds to max range, this matches the range the client is calculating.
	// see extra/supalosa_range_research.txt for more info
	//bonusRange = 2.52f;
	//MyLog::log->info( "Bonus range = %f" , bonusRange );

	// Check for too close
	if( spellid != SPELL_RANGED_WAND )//no min limit for wands
		if( minrange > dist )
			fail = SPELL_FAILED_TOO_CLOSE;

	if( dist > maxr )
	{
		//	MyLog::log->info( "Auto shot failed: out of range (Maxr: %f, Dist: %f)" , maxr , dist );
		fail = SPELL_FAILED_OUT_OF_RANGE;
	}

	if( spellid == SPELL_RANGED_THROW )
	{
		if( itm != NULL ) // no need for this
			if( itm->GetProto() )
				if( GetItemInterface()->GetItemCount( itm->GetProto()->ItemId ) == 0 )
					fail = SPELL_FAILED_NO_AMMO;
	}
/*  else
	{
		if(GetUInt32Value(PLAYER_AMMO_ID))//for wand
			if(this->GetItemInterface()->GetItemCount(GetUInt32Value(PLAYER_AMMO_ID)) == 0)
				fail = SPELL_FAILED_NO_AMMO;
	}
*/
	if( fail > 0 )// && fail != SPELL_FAILED_OUT_OF_RANGE)
	{
		//SendCastResult( autoshot ? 75 : spellid, fail, 0, 0 );
		MSG_S2C::stSpell_Cast_Result Msg;
		Msg.SpellId			= autoshot ? 75 : spellid;
		Msg.ErrorMessage	= fail;
		Msg.MultiCast		= 0;
		Msg.Extra			= 0;
		m_session->SendPacket(Msg);
		if( fail != SPELL_FAILED_OUT_OF_RANGE )
		{
			uint32 spellid2 = autoshot ? 75 : spellid;
			MSG_S2C::stSpell_Cancel_Auto_Repeat Msg;
			Msg.spell_id = spellid2;
			m_session->SendPacket( Msg );
		}
		//MyLog::log->info( "Result for CanShootWIthRangedWeapon: %u" , fail );
		//MyLog::log->debug( "Can't shoot with ranged weapon: %u (Timer: %u)" , fail , m_AutoShotAttackTimer );
		return fail;
	}

	return 0;
}

void Player::EventRepeatSpell()
{
	if( !m_EnableMeleeAttack )
		return;

	if( !m_curSelection )
		return;

	Unit* target = GetMapMgr()->GetUnit( m_curSelection );
	if( target == NULL )
	{

		m_AutoShotAttackTimer = 0; //avoid flooding client with error mesages
		m_onAutoShot = false;
		MSG_S2C::stSpell_Cancel_Auto_Repeat Msg;
		Msg.spell_id = 5003;
		m_session->SendPacket( Msg );
		//MyLog::log->debug( "Can't cast Autoshot: Target changed! (Timer: %u)" , m_AutoShotAttackTimer );
		return;
	}



	m_AutoShotDuration = m_uint32Values[UNIT_FIELD_RANGEDATTACKTIME];

	//if( m_isMoving )
	//{
	//	//MyLog::log->debug( "HUNTER AUTOSHOT 2) %i, %i", m_AutoShotAttackTimer, m_AutoShotDuration );
	//	//m_AutoShotAttackTimer = m_AutoShotDuration;//avoid flooding client with error mesages
	//	//MyLog::log->debug( "Can't cast Autoshot: You're moving! (Timer: %u)" , m_AutoShotAttackTimer );
	//	m_AutoShotAttackTimer = 100; // shoot when we can
	//	return;
	//}

	int32 f = this->CanShootRangedWeapon( m_AutoShotSpell->Id, target, true );

	if( f != 0 )
	{
		if( f != SPELL_FAILED_OUT_OF_RANGE )
		{
			m_AutoShotAttackTimer = 0;
			m_onAutoShot=false;
			MSG_S2C::stSpell_Cancel_Auto_Repeat Msg;
			Msg.spell_id = 5003;
			m_session->SendPacket( Msg );
		}
		else
		{
			m_AutoShotAttackTimer = m_AutoShotDuration;//avoid flooding client with error mesages
		}
		return;
	}
	else
	{
		m_AutoShotAttackTimer = m_AutoShotDuration;

		Spell* sp = new Spell( this, m_AutoShotSpell, false, NULL );
		SpellCastTargets tgt;
		tgt.m_unitTarget = m_curSelection;
		tgt.m_targetMask = TARGET_FLAG_UNIT;
		bool b = sp->prepare( &tgt );
		if (!b)
		{


			m_AutoShotAttackTimer = 0; //avoid flooding client with error mesages
			m_onAutoShot = false;
			MSG_S2C::stSpell_Cancel_Auto_Repeat Msg;
			Msg.spell_id = 5003;
			m_session->SendPacket( Msg );
			return;
			
		
		}
	}
}

void Player::removeSpellByHashName(uint32 hash)
{
	SpellSet::iterator it,iter;

	for(iter= mSpells.begin();iter != mSpells.end();)
	{
		it = iter++;
		uint32 SpellID = *it;
		SpellEntry *e = dbcSpell.LookupEntry(SpellID);
		if(e->NameHash == hash)
		{
			if(info->spell_list.find(e->Id) != info->spell_list.end())
				continue;

			RemoveAura(SpellID,GetGUID());
			MSG_S2C::stSpell_Removed Msg;
			Msg.spell_id = SpellID;
			m_session->SendPacket( Msg );

			mSpells.erase(it);
		}
	}

	for(iter= mDeletedSpells.begin();iter != mDeletedSpells.end();)
	{
		it = iter++;
		uint32 SpellID = *it;
		SpellEntry *e = dbcSpell.LookupEntry(SpellID);
		if(e->NameHash == hash)
		{
			if(info->spell_list.find(e->Id) != info->spell_list.end())
				continue;

			RemoveAura(SpellID,GetGUID());
			MSG_S2C::stSpell_Removed Msg;
			Msg.spell_id = SpellID;
			m_session->SendPacket( Msg );
			mDeletedSpells.erase(it);
		}
	}
}

bool Player::removeSpell(uint32 SpellID, bool MoveToDeleted, bool SupercededSpell, uint32 SupercededSpellID)
{
	SpellSet::iterator iter = mSpells.find(SpellID);
	if(iter != mSpells.end())
	{
		mSpells.erase(iter);

		if( SpellID != 73 && SpellID != 74 )
			RemoveAura(SpellID,GetGUID());
	}
	else
	{
		return false;
	}


	for ( int i = 0; i < PLAYER_ACTION_BUTTON_COUNT; i ++)
	{
		if (mActions[i].Action == SpellID)
		{
			mActions[i].Action = 0;
			mActions[i].Type = 0;
			mActions[i].Misc = 0;
		}
	}
	if(MoveToDeleted)
		mDeletedSpells.insert(SpellID);

	if(!IsInWorld())
		return true;
	
	SendInitialActions();
	if(SupercededSpell)
	{
		MSG_S2C::stSpell_Superceded Msg;
		Msg.spell_id = SpellID;
		Msg.superceded_spell_id = SupercededSpellID;
		m_session->SendPacket(Msg);
	}
	else
	{
		MSG_S2C::stSpell_Removed Msg;
		Msg.spell_id = SpellID;
		m_session->SendPacket( Msg );
	}

	return true;
}

void Player::EventActivateGameObject(GameObject* obj)
{
	obj->BuildFieldUpdatePacket(this, GAMEOBJECT_DYN_FLAGS, 1);
}

void Player::EventDeActivateGameObject(GameObject* obj)
{
	obj->BuildFieldUpdatePacket(this, GAMEOBJECT_DYN_FLAGS, 0);
}

void Player::EventTimedQuestExpire(Quest *qst, QuestLogEntry *qle, uint32 log_slot)
{
	MSG_S2C::stQuest_Update_Failed Msg;
	sQuestMgr.BuildQuestFailed(&Msg, qst->id);
	GetSession()->SendPacket(Msg);
	qle->Finish(false);
}

void Player::SendInitialLogonPackets()
{
	// Initial Packets... they seem to be re-sent on port.
//	MSG_S2C::stSet_Rest_Start MsgRest;
//	MsgRest.timeLogoff = m_timeLogoff;
//	m_session->SendPacket( MsgRest );

// 	MSG_S2C::stNPC_Binder_Update MsgBind;
// 	MsgBind.x = m_bind_pos_x;
// 	MsgBind.y = m_bind_pos_y;
// 	MsgBind.z = m_bind_pos_z;
// 	MsgBind.mapID = m_bind_mapid;
// 	MsgBind.ZoneID = m_bind_zoneid;
// 	GetSession()->SendPacket( MsgBind );

	//Proficiencies
    //SendSetProficiency(4,armor_proficiency);
    //SendSetProficiency(2,weapon_proficiency);

//	MSG_S2C::stSet_Proficiency MsgSetProficientcy;
//	MsgSetProficientcy.ItemClass = 4;
//	MsgSetProficientcy.Profinciency = armor_proficiency;
//	m_session->SendPacket( MsgSetProficientcy );
//	MsgSetProficientcy.ItemClass = 2;
//	MsgSetProficientcy.Profinciency = weapon_proficiency;
//	m_session->SendPacket( MsgSetProficientcy );

	ProcessPendingUpdates();
	UpdateSpeed();
	//Tutorial Flags
	MSG_S2C::stTutorial_Flags MsgTutorial;
	for (int i = 0; i < 8; i++)
		MsgTutorial.Tutorials[i] = uint32( m_Tutorials[i] );
	m_session->SendPacket(MsgTutorial);

	//Initial Spells
	smsg_InitialSpells();

	//Initial Actions
	SendInitialActions();

    /* Some minor documentation about the time field
    // MOVE THIS DOCUMENATION TO THE WIKI

    minute's = 0x0000003F                  00000000000000000000000000111111
    hour's   = 0x000007C0                  00000000000000000000011111000000
    weekdays = 0x00003800                  00000000000000000011100000000000
    days     = 0x000FC000                  00000000000011111100000000000000
    months   = 0x00F00000                  00000000111100000000000000000000
    years    = 0x1F000000                  00011111000000000000000000000000
    unk	     = 0xE0000000                  11100000000000000000000000000000
    */

//	MSG_S2C::stLogin_Set_TimeSpeed MsgSetTimeSpeed;
//	time_t minutes = sWorld.GetGameTime( ) / 60;
//	time_t hours = minutes / 60; minutes %= 60;
//	time_t gameTime = 0;

    // TODO: Add stuff to handle these variable's

// 	uint32 DayOfTheWeek = -1;		//	(0b111 = (any) day, 0 = Monday ect)
//     uint32 DayOfTheMonth = 20-1;	//	Day - 1 (0 is actual 1) its now the 20e here. TODO: replace this one with the proper date
//     uint32 CurrentMonth = 9-1;		//	Month - 1 (0 is actual 1) same as above. TODO: replace it with the proper code
//     uint32 CurrentYear = 7;			//	2000 + this number results in a correct value for this crap. TODO: replace this with the propper code

//    #define MINUTE_BITMASK      0x0000003F
//     #define HOUR_BITMASK        0x000007C0
//     #define WEEKDAY_BITMASK     0x00003800
//     #define DAY_BITMASK         0x000FC000
//     #define MONTH_BITMASK       0x00F00000
//     #define YEAR_BITMASK        0x1F000000
//     #define UNK_BITMASK         0xE0000000
//
//     #define MINUTE_SHIFTMASK    0
//     #define HOUR_SHIFTMASK      6
//     #define WEEKDAY_SHIFTMASK   11
//     #define DAY_SHIFTMASK       14
//     #define MONTH_SHIFTMASK     20
//     #define YEAR_SHIFTMASK      24
//     #define UNK_SHIFTMASK       29
//
//     gameTime = ((minutes << MINUTE_SHIFTMASK) & MINUTE_BITMASK);
//     gameTime|= ((hours << HOUR_SHIFTMASK) & HOUR_BITMASK);
//     gameTime|= ((DayOfTheWeek << WEEKDAY_SHIFTMASK) & WEEKDAY_BITMASK);
//     gameTime|= ((DayOfTheMonth << DAY_SHIFTMASK) & DAY_BITMASK);
//     gameTime|= ((CurrentMonth << MONTH_SHIFTMASK) & MONTH_BITMASK);
//     gameTime|= ((CurrentYear << YEAR_SHIFTMASK) & YEAR_BITMASK);
//
//     MsgSetTimeSpeed.gametime = (uint32)gameTime;
// 	MsgSetTimeSpeed.gamespeed = (float)0.0166666669777748f;  // Normal Game Speed
// 	GetSession()->SendPacket( MsgSetTimeSpeed);

	MyLog::log->notice("WORLD: Sent initial logon packets for %s.", GetName());
}

void Player::Reset_Spells()
{
	PlayerCreateInfo *info = objmgr.GetPlayerCreateInfo(getRace(), getClass());
	ASSERT(info);

	std::list<uint32> spelllist;

	for(SpellSet::iterator itr = mSpells.begin(); itr!=mSpells.end(); itr++)
	{
		spelllist.push_back((*itr));
	}

	for(std::list<uint32>::iterator itr = spelllist.begin(); itr!=spelllist.end(); itr++)
	{
		removeSpell((*itr), false, false, 0);
	}

	for(std::set<uint32>::iterator sp = info->spell_list.begin();sp!=info->spell_list.end();sp++)
	{
		if(*sp)
		{
			addSpell(*sp);
		}
	}
}

void Player::Reset_Talents()
{
	StorageContainerIterator<TalentEntry> * itr = dbcTalent.MakeIterator();
	TalentEntry * tmpTalent;
	while(!itr->AtEnd())
	{
		tmpTalent = itr->Get();
		if(!tmpTalent)
			continue; //should not ocur
		//this is a normal talent (i hope )
		for (int j = 0; j < 5; j++)
		{
			if (tmpTalent->RankID[j] != 0)
			{
				SpellEntry *spellInfo;
				spellInfo = dbcSpell.LookupEntry( tmpTalent->RankID[j] );
				if(spellInfo)
				{
					for(int k=0;k<3;k++)
						if(spellInfo->Effect[k] == SPELL_EFFECT_LEARN_SPELL)
						{
							//removeSpell(spellInfo->EffectTriggerSpell[k], false, 0, 0);
							//remove higher ranks of this spell too (like earth shield lvl 1 is talent and the rest is thought from trainer)
							SpellEntry *spellInfo2;
							spellInfo2 = dbcSpell.LookupEntry( spellInfo->EffectTriggerSpell[k] );
							if(spellInfo2)
								removeSpellByHashName(spellInfo2->NameHash);
						}
						//remove them all in 1 shot
						removeSpellByHashName(spellInfo->NameHash);
				}
			}
			else
				break;
		}
		if(!itr->Inc())
			break;
	}

	itr->Destruct();

	uint32 l=getLevel();
	if(l>9)
	{
		SetUInt32Value(PLAYER_CHARACTER_POINTS1, l - 9);
	}
	else
	{
		SetUInt32Value(PLAYER_CHARACTER_POINTS1, 0);
	}

}

void Player::Reset_ToLevel1()
{
	RemoveAllAuras();
	// clear aura fields
	for(int i=UNIT_FIELD_AURA;i<UNIT_FIELD_AURASTATE;++i)
	{
		SetUInt32Value(i, 0);
	}
	SetUInt32Value(UNIT_FIELD_LEVEL, 1);
	PlayerCreateInfo *info = objmgr.GetPlayerCreateInfo(getRace(), getClass());
	ASSERT(info);

	SetUInt32Value(UNIT_FIELD_HEALTH, info->health);
	SetUInt32Value(UNIT_FIELD_POWER1, info->mana );
	SetUInt32Value(UNIT_FIELD_MAXHEALTH, info->health);
	SetUInt32Value(UNIT_FIELD_BASE_HEALTH, info->health);
	SetUInt32Value(UNIT_FIELD_BASE_MANA, info->mana);
	SetUInt32Value(UNIT_FIELD_MAXPOWER1, info->mana );
	SetFloatValue(UNIT_FIELD_STAT0, info->strength );
	SetFloatValue(UNIT_FIELD_STAT1, info->ability );
	SetFloatValue(UNIT_FIELD_STAT2, info->stamina );
	SetFloatValue(UNIT_FIELD_STAT3, info->intellect );
	SetFloatValue(UNIT_FIELD_STAT4, info->spirit );
	SetFloatValue(UNIT_FIELD_STAT5, info->vitality );
	//SetUInt32Value(UNIT_FIELD_ATTACK_POWER, info->attackpower );
	SetUInt32Value(PLAYER_CHARACTER_POINTS1,0);
	SetUInt32Value(PLAYER_CHARACTER_POINTS2,2);
}

void Player::CalcResistance(uint32 type)
{
	float res = 0;
	int32 pos = 0;
	int32 neg = 0;
	ASSERT(type < 7);

	if (type == 0)
	{
		res = (float)(2.f * (float)GetFloatValue(UNIT_FIELD_STAT0 + STAT_AGILITY));
	}

	res +=/* 0.75f **/ BaseResistance[type];
	pos = ( res * BaseResistanceModPctPos[type] ) / 100 + 0.5f;
	neg = ( res * BaseResistanceModPctNeg[type] ) / 100 + 0.5f;
	res = res + pos - neg;

// 	pos = ( res * BaseSpellResistanceModPctPos[type] ) / 100 + 0.5f;
// 	neg = ( res * BaseSpellResistanceModPctNeg[type] ) / 100 + 0.5f;
// 	res = res + pos - neg;
// 
// 	pos = ( res * ResistanceModPctPos[type] ) / 100 + 0.5f;
// 	neg = ( res * ResistanceModPctNeg[type] ) / 100 + 0.5f;
// 	res = res + pos - neg;

	SetFloatValue(UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE+type,pos);
	SetFloatValue(UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE+type,neg);

	res += FlatResistanceModifierPos[type] - FlatResistanceModifierNeg[type];
	
	SetFloatValue(UNIT_FIELD_RESISTANCES + type,res>0?res:0);


/*


	if (type >= 7)
	{
		return;
	}
	type = 0;
	float res = 0;
	int32 pos = 0;
	int32 neg = 0;
	ASSERT(type < 6);

	res = (float)(0.6f * (float)GetFloatValue(UNIT_FIELD_STAT0 + STAT_VITALITY) );
	res += 0.75f * BaseResistance[type];
	pos = ( res * BaseResistanceModPctPos[type] ) / 100 + 0.5f;
	neg = ( res * BaseResistanceModPctNeg[type] ) / 100 + 0.5f;
	res = res + pos - neg;

	SetFloatValue(UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE+type,pos);
	SetFloatValue(UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE+type,neg);

	{
		res = .5f*GetFloatValue(UNIT_FIELD_STAT0+STAT_VITALITY)+.75f*BaseResistance[type]+1.5f*GetFloatValue(UNIT_FIELD_STAT0 + STAT_SPIRIT);
		pos = ( res * BaseSpellResistanceModPctPos[type] ) / 100 + 0.5f;
		neg = ( res * BaseSpellResistanceModPctNeg[type] ) / 100 + 0.5f;
		res = res + pos - neg;



		pos = ( res * ResistanceModPctPos[type] ) / 100 + 0.5f;
		neg = ( res * ResistanceModPctNeg[type] ) / 100 + 0.5f;
		res = res + pos - neg;

		SetFloatValue(UNIT_FIELD_RESISTANCES + type, res>0?res:0);
	}

	*/
}

void Player::UpdateNearbyGameObjects()
{
	for (Object::InRangeSet::iterator itr = GetInRangeSetBegin(); itr != GetInRangeSetEnd(); ++itr)
	{
		if((*itr)->GetTypeId() == TYPEID_GAMEOBJECT)
		{
			bool activate_quest_object = false;
			GameObject *go = ((GameObject*)*itr);
			QuestLogEntry *qle;
			GameObjectInfo *info;

			info = go->GetInfo();
			bool deactivate = false;
			if(info &&
				(info->goMap.size() || info->itemMap.size()) )
			{
				for(GameObjectGOMap::iterator itr = go->GetInfo()->goMap.begin();
					itr != go->GetInfo()->goMap.end();
					++itr)
				{
					if((qle = GetQuestLogForEntry(itr->first->id)))
					{
						for(uint32 i = 0; i < qle->GetQuest()->count_required_mob; ++i)
						{
							if(qle->GetQuest()->required_mob[i] == go->GetEntry() &&
								qle->GetMobCount(i) < qle->GetQuest()->required_mobcount[i])
							{
								activate_quest_object = true;
								break;
							}
						}
						if(activate_quest_object)
							break;
					}
				}

				if(!activate_quest_object)
				{
					for(GameObjectItemMap::iterator itr = go->GetInfo()->itemMap.begin();
						itr != go->GetInfo()->itemMap.end();
						++itr)
					{
						for(std::map<uint32, uint32>::iterator it2 = itr->second.begin();
							it2 != itr->second.end();
							++it2)
						{
							if(GetItemInterface()->GetItemCount(it2->first) < it2->second)
							{
								activate_quest_object = true;
								break;
							}
						}
						if(activate_quest_object)
							break;
					}
				}

				if(!activate_quest_object)
				{
					deactivate = true;
				}
			}
			bool bPassed = !deactivate;
			if(go->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_QUESTGIVER)
			{
				if(go->m_quests)
				{
					if(go->m_quests->size() > 0)
					{
						std::list<QuestRelation*>::iterator itr2 = go->m_quests->begin();
						for(;itr2!=go->m_quests->end();++itr2)
						{
							QuestRelation* qr = *itr2;
							uint32 status = sQuestMgr.CalcQuestStatus(NULL, this, qr->qst, qr->type, false);
							if(status == QMGR_QUEST_CHAT || status == QMGR_QUEST_AVAILABLE || status == QMGR_QUEST_REPEATABLE || status == QMGR_QUEST_FINISHED)
							{
								// Activate gameobject
								EventActivateGameObject( go );
								bPassed = true;
								break;
							}
						}
					}
				}
			}
			if(!bPassed)
				EventDeActivateGameObject( go );
		}
	}
}


void Player::EventTaxiInterpolate()
{
	if(!m_CurrentTaxiPath || m_mapMgr==NULL) return;

	float x,y,z;
	uint32 ntime = getMSTime();

	if (ntime > m_taxi_ride_time)
		m_CurrentTaxiPath->SetPosForTime(x, y, z, ntime - m_taxi_ride_time, &lastNode, m_mapId);
	/*else
		m_CurrentTaxiPath->SetPosForTime(x, y, z, m_taxi_ride_time - ntime, &lastNode);*/

	if(x < _minX || x > _maxX || y < _minY || y > _maxX)
		return;

	SetPosition(x,y,z,0);
}

void Player::TaxiStart(TaxiPath *path, uint32 modelid, uint32 start_node)
{
	/*
	int32 mapchangeid = -1;
	float mapchangex;
	float mapchangey;
	float mapchangez;
	uint32 cn = m_taxiMapChangeNode;

	m_taxiMapChangeNode = 0;

	DismissMount();

	//also remove morph spells
	if(GetUInt32Value(UNIT_FIELD_DISPLAYID)!=GetUInt32Value(UNIT_FIELD_NATIVEDISPLAYID))
	{
		RemoveAllAuraType(SPELL_AURA_TRANSFORM);
		RemoveAllAuraType(SPELL_AURA_MOD_SHAPESHIFT);
	}

	SetUInt32Value( UNIT_FIELD_MOUNTDISPLAYID, modelid );
	SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_MOUNTED_TAXI);
	SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER);

	SetTaxiPath(path);
	SetTaxiPos();
	SetTaxiState(true);
	m_taxi_ride_time = getMSTime();

	//uint32 traveltime = uint32(path->getLength() * TAXI_TRAVEL_SPEED); // 36.7407
	float traveldist = 0;

	float lastx = 0, lasty = 0, lastz = 0;
	TaxiPathNode *firstNode = path->GetPathNode(start_node);
	uint32 add_time = 0;

	// temporary workaround for taximodes with changing map
	if (path->GetID() == 766 || path->GetID() == 767 || path->GetID() == 771 || path->GetID() == 772)
	{
		JumpToEndTaxiNode(path);
		return;
	}

	if(start_node)
	{
		TaxiPathNode *pn = path->GetPathNode(0);
		float dist = 0;
		lastx = pn->x;
		lasty = pn->y;
		lastz = pn->z;
		for(uint32 i = 1; i <= start_node; ++i)
		{
			pn = path->GetPathNode(i);
			if(!pn)
			{
				JumpToEndTaxiNode(path);
				return;
			}

			dist += CalcDistance(lastx, lasty, lastz, pn->x, pn->y, pn->z);
			lastx = pn->x;
			lasty = pn->y;
			lastz = pn->z;
		}
		add_time = uint32( dist * TAXI_TRAVEL_SPEED );
		lastx = lasty = lastz = 0;
	}
	size_t endn = path->GetNodeCount();
	if(m_taxiPaths.size())
		endn-= 2;

	for(uint32 i = start_node; i < endn; ++i)
	{
		TaxiPathNode *pn = path->GetPathNode(i);

		// temporary workaround for taximodes with changing map
		if (!pn || path->GetID() == 766 || path->GetID() == 767 || path->GetID() == 771 || path->GetID() == 772)
		{
			JumpToEndTaxiNode(path);
			return;
		}

		if( pn->mapid != m_mapId )
		{
			endn = (i - 1);
			m_taxiMapChangeNode = i;

			mapchangeid = (int32)pn->mapid;
			mapchangex = pn->x;
			mapchangey = pn->y;
			mapchangez = pn->z;
			break;
		}

		if(!lastx || !lasty || !lastz)
		{
			lastx = pn->x;
			lasty = pn->y;
			lastz = pn->z;
		} else {
			float dist = CalcDistance(lastx,lasty,lastz,
				pn->x,pn->y,pn->z);
			traveldist += dist;
			lastx = pn->x;
			lasty = pn->y;
			lastz = pn->z;
		}
	}

	uint32 traveltime = uint32(traveldist * TAXI_TRAVEL_SPEED);

	if( start_node > endn || (endn - start_node) > 200 )
		return;

	MSG_S2C::stMove_Monster Msg;
	Msg.guid	= GetNewGUID();
	Msg.x		= firstNode->x;
	Msg.y		= firstNode->y;
	Msg.z		= firstNode->z;
	Msg.move_type = 0;
	Msg.move_flag = 0x00000300;
	Msg.timestamp = m_taxi_ride_time;
	Msg.time_between = traveltime;

	if(!cn)
		m_taxi_ride_time -= add_time;

// 	data << uint32( endn - start_node );
//	uint32 timer = 0, nodecount = 0;
//	TaxiPathNode *lastnode = NULL;

	for(uint32 i = start_node; i < endn; i++)
	{
		TaxiPathNode *pn = path->GetPathNode(i);
		if(!pn)
		{
			JumpToEndTaxiNode(path);
			return;
		}

		Msg.vWayPoints.push_back(MSG_S2C::stMove_Monster::stWaypoint(pn->x, pn->y, pn->z));
	}

	SendMessageToSet(Msg, true);

	sEventMgr.AddEvent(this, &Player::EventTaxiInterpolate,
		EVENT_PLAYER_TAXI_INTERPOLATE, 900, 0,0);

	if( mapchangeid < 0 )
	{
		TaxiPathNode *pn = path->GetPathNode((uint32)path->GetNodeCount() - 1);
		sEventMgr.AddEvent(this, &Player::EventDismount, path->GetPrice(),
			pn->x, pn->y, pn->z, EVENT_PLAYER_TAXI_DISMOUNT, traveltime, 1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
	}
	else
	{
		sEventMgr.AddEvent( this, &Player::EventTeleport, (uint32)mapchangeid, mapchangex, mapchangey, mapchangez, EVENT_PLAYER_TELEPORT, traveltime, 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
	}
	*/
}

void Player::JumpToEndTaxiNode(TaxiPath * path)
{
	// this should *always* be safe in case it cant build your position on the path!
	TaxiPathNode * pathnode = path->GetPathNode((uint32)path->GetNodeCount()-1);
	if(!pathnode) return;

	ModCoin(-(int32)path->GetPrice());

	SetTaxiState(false);
	SetTaxiPath(NULL);
	UnSetTaxiPos();
	m_taxi_ride_time = 0;

	SetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID , 0);
	RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_MOUNTED_TAXI);
	RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER);

	SetPlayerSpeed(RUN, m_runSpeed);

	SafeTeleport(pathnode->mapid, 0, LocationVector(pathnode->x, pathnode->y, pathnode->z));

	// Start next path if any remaining
	if(m_taxiPaths.size())
	{
		TaxiPath * p = *m_taxiPaths.begin();
		m_taxiPaths.erase(m_taxiPaths.begin());
		TaxiStart(p, taxi_model_id, 0);
	}
}

void Player::RemoveSpellsFromLine(uint32 skill_line)
{
	StorageContainerIterator<skilllinespell> * itr = dbcSkillLineSpell.MakeIterator();
	skilllinespell * sp;
	while(!itr->AtEnd())
	{
		sp = itr->Get();

		if(sp)
		{
			if(sp->skilline == skill_line)
			{
				// Check ourselves for this spell, and remove it..
					removeSpell(sp->spell, 0, 0, 0);
			}
		}
		if(!itr->Inc())
			break;
	}

	itr->Destruct();
}

void Player::CalcStat(uint32 type)
{
	float res;
	ASSERT( type < 6 );
	float pos = (BaseStats[type] * StatModPctPos[type] ) / 100 + FlatStatModPos[type];
	float neg = (BaseStats[type] * StatModPctNeg[type] ) / 100 + FlatStatModNeg[type];
	res = pos + BaseStats[type] - neg;
	pos += ( res * static_cast< Player* >( this )->TotalStatModPctPos[type] ) / 100;
	neg += ( res * static_cast< Player* >( this )->TotalStatModPctNeg[type] ) / 100;
	res = pos + BaseStats[type] - neg;
	if( res <= 0 )
		res = 1;

	SetUInt32Value( UNIT_FIELD_POSSTAT0 + type, pos );
	SetUInt32Value( UNIT_FIELD_NEGSTAT0 + type, neg );
	SetFloatValue( UNIT_FIELD_STAT0 + type, res > 0 ?res : 0 );

	if( type == STAT_AGILITY )
		CalcResistance( 0 );
	//else if(type == STAT_SPIRIT)
	//	CalcResistance( 1 );
}

void Player::RegenerateMana(bool is_interrupted)
{

	if (m_interruptRegen)
		return;

	float spirit = (float)GetFloatValue( UNIT_FIELD_STAT0 + STAT_SPIRIT );
	float intellect = (float)GetFloatValue( UNIT_FIELD_STAT0 + STAT_INTELLECT );
	float IntC = -log10( intellect / 300.f);
	float basemanaregen = (spirit / (10.f + 6.f * IntC * IntC + 7.2f * IntC) + (10.f + 1.5f * getLevel()) * 0.1f) * 2.5f;
	//float basemanaregen = ( 4.f * spirit + intellect ) / 5.f;
	float manaregen = basemanaregen + (float)m_extraRegenManaPoint;
	manaregen += basemanaregen * m_extraRegenManaPointPCT;

	uint32 cur = GetUInt32Value( UNIT_FIELD_POWER1 );
	uint32 mm = GetUInt32Value( UNIT_FIELD_MAXPOWER1 );
	if( cur >= mm )
		return;


	if( manaregen <= 1.0f )//this fixes regen like 0.98
		cur++;
	else
		cur += float2int32( manaregen );
	SetUInt32Value( UNIT_FIELD_POWER1, (cur>=mm) ? mm : cur );


	/*
	uint32 cur = GetUInt32Value(UNIT_FIELD_POWER1);
	uint32 mm = GetUInt32Value(UNIT_FIELD_MAXPOWER1);
	if(cur >= mm)return;
	float amt = (is_interrupted) ? GetFloatValue(PLAYER_FIELD_MOD_MANA_REGEN_INTERRUPT) : GetFloatValue(PLAYER_FIELD_MOD_MANA_REGEN);
	amt *= 2; //floats are Mana Regen Per Sec. Regen Applied every 2 secs so real value =X*2 . Shady
	//Apply shit from conf file
	amt *= sWorld.getRate(RATE_POWER1);

	if((amt<=1.0)&&(amt>0))//this fixes regen like 0.98
	{
		if(is_interrupted)
			return;
		cur++;
	}
	else
		cur += float2int32(amt);
	SetUInt32Value(UNIT_FIELD_POWER1,(cur >= mm) ? mm : cur);
	*/
}

void Player::RegenerateHealth( bool inCombat )
{
	if( inCombat )
		return;

	float vitality = (float)GetFloatValue( UNIT_FIELD_STAT0 + STAT_VITALITY );
	float basehpregen = 0.234375 * vitality;

	float hpregen = basehpregen + (float)m_extraRegenHealPoint;
	hpregen += basehpregen * (float)m_extraRegenHealPCT;

	uint32 cur = GetUInt32Value(UNIT_FIELD_HEALTH);
	uint32 mh = GetUInt32Value(UNIT_FIELD_MAXHEALTH);
	if(cur >= mh)
		return;

	if( hpregen <= 1.0f )//this fixes regen like 0.98
		cur++;
	else
		cur += float2int32( hpregen );
	SetUInt32Value(UNIT_FIELD_HEALTH,(cur>=mh) ? mh : cur);

}

uint32 Player::GeneratePetNumber()
{
	uint32 val = m_PetNumberMax + 1;
	for (uint32 i = 1; i < m_PetNumberMax; i++)
		if(m_Pets.find(i) == m_Pets.end())
			return i;					   // found a free one

	return val;
}

void Player::RemovePlayerPet(uint32 pet_number)
{
	std::map<uint32, PlayerPet*>::iterator itr = m_Pets.find(pet_number);
	if(itr != m_Pets.end())
	{
		delete itr->second;
		m_Pets.erase(itr);
		EventDismissPet();
	}
}
#ifndef CLUSTERING
void Player::_Relocate(uint32 mapid, const LocationVector & v, bool sendpending, bool force_new_world, uint32 instance_id)
{
	//this func must only be called when switching between maps!
	if(sendpending && mapid != m_mapId && force_new_world)
	{
		MSG_S2C::stTransfer_Pending Msg;
		Msg.mapid = mapid;
		GetSession()->SendPacket(Msg);
	}

	if(m_mapId != mapid || force_new_world)
	{
		uint32 status = sInstanceMgr.PreTeleport(mapid, this, instance_id);
		if(status != INSTANCE_OK)
		{
			MSG_S2C::stTransfer_Abouted Msg;
			Msg.mapid = mapid;
			Msg.stat = status;
			GetSession()->SendPacket(Msg);
			m_sunyou_instance = NULL;
			return;
		}

		if(instance_id && m_instanceId != instance_id)
			m_instanceId=instance_id;
		
		if(IsInWorld())
		{
			RemoveFromWorld();
			UpdateVisibility();
		}

		int lastmapid = m_mapId;

		SetMapId(mapid);
		MapMgr* pMapMgr = sInstanceMgr.GetMapMgr( mapid );
		if( !pMapMgr )
		{
			pMapMgr = sInstanceMgr.GetInstance( this );
			if( !pMapMgr )
			{
				MyLog::log->error( "connot find instance map ID:%d", mapid );
				Kick();
				return;
			}

			m_bgEntryPointX = GetPositionX();
			m_bgEntryPointY = GetPositionY();
			m_bgEntryPointZ = GetPositionZ();
			m_bgEntryPointO = GetOrientation();
			m_bgEntryPointInstance = GetInstanceID();
			m_bgEntryPointMap = lastmapid;

			m_resurrectMapId = m_mapId;
			m_resurrectPosX = pMapMgr->GetMapInfo()->repopx;
			m_resurrectPosY = pMapMgr->GetMapInfo()->repopy;
			m_resurrectPosZ = pMapMgr->GetMapInfo()->repopz;
			m_resurrectInstanceID = pMapMgr->GetInstanceID();
		}

		MSG_S2C::stMove_New_World Msg;
		Msg.map_id	= mapid;
		Msg.pos_x	= v.x;
		Msg.pos_y	= v.y;
		Msg.pos_z	= v.z;
		Msg.orientation	= v.o;
		GetSession()->SendPacket( Msg );

		AddToWorld(pMapMgr);
		if( pMapMgr->GetMapInfo()->type == 2 )
		{
			std::map<uint32, uint32>::iterator it = m_playerInfo->instance_enter.find( mapid );
			if( it != m_playerInfo->instance_enter.end() )
			{
				it->second++;
			}
			else
				m_playerInfo->instance_enter[mapid] = 1;

			if(m_playerInfo->m_loggedInPlayer->m_Titles.find(27) != m_playerInfo->m_loggedInPlayer->m_Titles.end())
			{
				if(m_playerInfo->instance_enter[19] + m_playerInfo->instance_enter[29] + m_playerInfo->instance_enter[30] + m_playerInfo->instance_enter[31] >= 100)
				{//遗失陵墓
					InsertTitle(27);
					MSG_S2C::stTitleAdd msg;
					msg.title.title = 27;
					GetSession()->SendPacket(msg);
				}
			}
			if(m_Titles.find(28) != m_Titles.end())
			{
				if(m_playerInfo->instance_enter[16] + m_playerInfo->instance_enter[17] + m_playerInfo->instance_enter[33] + m_playerInfo->instance_enter[34] + 
					m_playerInfo->instance_enter[35] + m_playerInfo->instance_enter[36] + m_playerInfo->instance_enter[37] >= 100)
				{//修行岛
					InsertTitle(28);
					MSG_S2C::stTitleAdd msg;
					msg.title.title = 28;
					GetSession()->SendPacket(msg);
				}
			}
		}
		
	}
	else
	{
		// via teleport ack msg
		MSG_S2C::stMove_Teleport_Ack Msg;
		Msg.guid = GetGUID();
		BuildTeleportAckMsg(v, &Msg);
		//m_session->SendPacket(Msg);
		SendMessageToSet( Msg, true );
	}
	SetPlayerStatus(TRANSFER_PENDING);
	m_sentTeleportPosition = v;
	ResetHeartbeatCoords();

	SetPosition(v);


	if (!m_Summon &&iActivePet)
	{
		SpawnPet(iActivePet);
	}
	else
	{
		if (m_Summon)
		{
			m_Summon->SetPosition(v);
		}
		
	}
	UpdateVisibility();
	z_axisposition = 0.0f;
}
#endif

// Player::AddItemsToWorld
// Adds all items to world, applies any modifiers for them.

void Player::AddItemsToWorld()
{
	Item * pItem;
	for(uint32 i = 0; i < BANK_SLOT_BAG_END; i++)
	{
		pItem = GetItemInterface()->GetInventoryItem(i);
		if( pItem != NULL )
		{
			pItem->PushToWorld(m_mapMgr);

			//if(i < INVENTORY_SLOT_BAG_END)	  // only equipment slots get mods.
			{
				//_ApplyItemMods(pItem, i, true, false, true);
			}

			if(pItem->IsContainer() && GetItemInterface()->IsBagSlot(i))
			{
				for(uint32 e=0; e < pItem->GetProto()->ContainerSlots; e++)
				{
					Item *item = ((Container*)pItem)->GetItem(e);
					if(item)
					{
						item->PushToWorld(m_mapMgr);
					}
				}
			}
		}
	}
	for( uint32 i = 0; i < MAX_JINGLIAN_SLOT; ++i )
	{
		pItem = m_JingLianItemInterface->GetSlot( i );
		if( pItem )
		{
			pItem->PushToWorld(m_mapMgr);
		}
	}

	UpdateStats();
}

// Player::RemoveItemsFromWorld
// Removes all items from world, reverses any modifiers.

void Player::RemoveItemsFromWorld()
{
	Item * pItem;
	for(uint32 i = 0; i < BANK_SLOT_BAG_END; i++)
	{
		pItem = m_ItemInterface->GetInventoryItem((ui8)i);
		if(pItem)
		{
			if(pItem->IsInWorld())
			{

				if(i < EQUIPMENT_SLOT_END && pItem->GetDurabilityMax() > 0 && pItem->GetDurability() > 0)	  // only equipment slots get mods.
				{
					_ApplyItemMods(pItem, i, false, false, true);
					pItem->OnUnEquip();
				}
				pItem->RemoveFromWorld();
			}

			if(pItem->IsContainer() && GetItemInterface()->IsBagSlot(i))
			{
				for(uint32 e=0; e < pItem->GetProto()->ContainerSlots; e++)
				{
					Item *item = ((Container*)pItem)->GetItem(e);
					if(item && item->IsInWorld())
					{
						item->RemoveFromWorld();
					}
				}
			}
		}
	}

	UpdateStats();
}

uint32 Player::BuildCreateUpdateBlockForPlayer(ByteBuffer *data, Player *target )
{
	int count = 0;
	if(target == this)
	{
		// we need to send create objects for all items.
		count += GetItemInterface()->m_CreateForPlayer(data);
		count += GetJingLianItemInterface()->CreateUpdateBlocks(data);
	}
	count += Unit::BuildCreateUpdateBlockForPlayer(data, target);
	return count;
}

void Player::Kick(uint32 delay /* = 0 */)
{
	if(!delay)
	{
		m_KickDelay = 0;
		_Kick();
	} else {
		m_KickDelay = delay;
		sEventMgr.AddEvent(this, &Player::_Kick, EVENT_PLAYER_KICK, 1000, 0, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
	}
}

void Player::_Kick()
{
	if(!m_KickDelay)
	{
		SoftDisconnect();
	} else {
		if((m_KickDelay - 1000) < 500)
		{
			m_KickDelay = 0;
		} else {
			m_KickDelay -= 1000;
		}
		sChatHandler.BlueSystemMessageToPlr(this, "you will disconnect in %u second.", (uint32)(m_KickDelay/1000));
	}
}

bool Player::HasDeletedSpell(uint32 spell)
{
	return (mDeletedSpells.count(spell) > 0);
}

void Player::ClearCooldownForSpell(uint32 spell_id)
{
	MSG_S2C::stSpell_Clear_CD Msg;
	Msg.spell_id = spell_id;
	Msg.guid = GetGUID();
	GetSession()->SendPacket(Msg);

	// remove cooldown data from Server side lists
	uint32 i;
	PlayerCooldownMap::iterator itr, itr2;
	SpellEntry * spe = dbcSpell.LookupEntry(spell_id);
	if(!spe) return;

	for(i = 0; i < NUM_COOLDOWN_TYPES; ++i)
	{
		for( itr = m_cooldownMap[i].begin(); itr != m_cooldownMap[i].end(); )
		{
			itr2 = itr++;
			if( ( i == COOLDOWN_TYPE_CATEGORY && itr2->first == spe->Category ) ||
				( i == COOLDOWN_TYPE_SPELL && itr2->first == spe->Id ) )
			{
				m_cooldownMap[i].erase( itr2 );
			}
		}
	}
}

void Player::ResetAllCooldowns()
{
	MSG_S2C::stSpell_Clear_CD Msg;
	Msg.spell_id = 0;
	Msg.guid = GetGUID();
	GetSession()->SendPacket(Msg);

	for( uint32 i = 0; i < NUM_COOLDOWN_TYPES; ++i )
	{
		m_cooldownMap[i].clear();
	}

	m_mapSpellGroupColdown.clear();
}

void Player::ClearCooldownsOnLine(uint32 skill_line, uint32 called_from)
{
	// found an easier way.. loop spells, check skill line
	SpellSet::const_iterator itr = mSpells.begin();
	skilllinespell *sk;
	for(; itr != mSpells.end(); ++itr)
	{
		if((*itr) == called_from)	   // skip calling spell.. otherwise spammies! :D
			continue;

		sk = objmgr.GetSpellSkill((*itr));
		if(sk && sk->skilline == skill_line)
			ClearCooldownForSpell((*itr));
	}
}

void Player::PushUpdateData(ByteBuffer *data, uint32 updatecount, bool send)
{
	if( m_ObjectUpdateMsg.buffer.size() + data->size() >= 63000 )
		ProcessPendingUpdates( false );

	m_ObjectUpdateMsg.buffer.append( *data );

	//if( send )
		//ProcessPendingUpdates();
}

void Player::SendPetList()
{
	MSG_S2C::stPets_List_Stabled Msg;
	Msg.StableSlotCount = uint8(m_StableSlotCount);
	char i=0;
	for(std::map<uint32, PlayerPet*>::iterator itr = m_Pets.begin(); itr != m_Pets.end(); ++itr)
	{
		MSG_S2C::stPets_List_Stabled::stPet pet;
		pet.pet_no = uint32(itr->first); // pet no
		pet.entry_id = uint32(itr->second->entry); // entryid
		pet.level = uint32(itr->second->level); // level
		pet.name = itr->second->name;		  // name
		pet.loyaltylvl = uint32(itr->second->loyaltylvl);
		if(itr->second->stablestate == STABLE_STATE_ACTIVE)
			pet.state = uint8(STABLE_STATE_ACTIVE);
		else
		{
			pet.state = uint8(STABLE_STATE_PASSIVE + i);
			i++;
		}
		Msg.vPets.push_back( pet );
	}

	if (GetSession())
	{
		GetSession()->SendPacket(Msg);
	}
}

bool Player::IsHavePet(uint32 PetEntry)
{
	for(std::map<uint32, PlayerPet*>::iterator itr = m_Pets.begin(); itr != m_Pets.end(); ++itr)
	{
		if (itr->second->entry == PetEntry)
		{
			return true;
		}

	}

	return false;
}


void Player::PushOutOfRange(ui64 guid)
{
	if( m_ObjectUpdateMsg.buffer.size() + 11 >= 63000 )
		ProcessPendingUpdates();

	m_ObjectUpdateMsg.buffer << (uint16)11;
	m_ObjectUpdateMsg.buffer << (uint8)UPDATETYPE_OUT_OF_RANGE_OBJECTS;
	m_ObjectUpdateMsg.buffer << guid;

	//ProcessPendingUpdates();
}

void Player::PushCreationData(ByteBuffer *data, uint32 updatecount, bool send)
{
	if( updatecount == 0 )
		return;

	if( m_ObjectUpdateMsg.buffer.size() + data->size() >= 63000 )
		ProcessPendingUpdates( false );

	m_ObjectUpdateMsg.buffer.append( *data );
	//if( send )
		//ProcessPendingUpdates();
}

void Player::ProcessPendingUpdates( bool SendCurrentMove )
{
	if( m_ObjectUpdateMsg.buffer.size() > 0 )
	{
		m_session->SendPacket( m_ObjectUpdateMsg );
		m_ObjectUpdateMsg.buffer.clear();

		if( SendCurrentMove )
		{
			for( std::map<uint64, uint32>::iterator it = m_setNeedSendCurrentMoveCreature.begin(); it != m_setNeedSendCurrentMoveCreature.end(); )
			{
				std::map<uint64, uint32>::iterator it2 = it++;
				uint64 guid = it2->first;
				uint32& count = it2->second;

				if( --count == 0 )
				{
					Object* p = m_mapMgr->_GetObject( guid );
					if( m_objectsInRange.find( p ) != m_objectsInRange.end() )
					{
						if( p->IsUnit() )
							((Unit*)p)->GetAIInterface()->SendCurrentMove( this );
					}
					m_setNeedSendCurrentMoveCreature.erase( it2 );
				}
			}
		}
	}

	bProcessPending = false;

	// resend speed if needed
	if( resend_speed )
	{
		SetPlayerSpeed( RUN, m_runSpeed );
		SetPlayerSpeed( FLY, m_flySpeed );
		SetPlayerSpeed( RUNBACK, m_backWalkSpeed );
		resend_speed = false;
	}
}

bool Player::CompressAndSendUpdateBuffer(uint32 size, const uint8* update_buffer)
{
	uint32 destsize = size + size/10 + 16;
	int rate = sWorld.getIntRate(INTRATE_COMPRESSION);
	if(size >= 40000 && rate < 6)
		rate = 6;

	// set up stream
	z_stream stream;
	stream.zalloc = 0;
	stream.zfree  = 0;
	stream.opaque = 0;

	if(deflateInit(&stream, rate) != Z_OK)
	{
		MyLog::log->error("deflateInit failed.");
		return false;
	}

	uint8 *buffer = new uint8[destsize];
	//memset(buffer,0,destsize);	/* fix umr - burlex */

	// set up stream pointers
	stream.next_out  = (Bytef*)buffer+4;
	stream.avail_out = destsize;
	stream.next_in   = (Bytef*)update_buffer;
	stream.avail_in  = size;

	// call the actual process
	if(deflate(&stream, Z_NO_FLUSH) != Z_OK ||
		stream.avail_in != 0)
	{
		MyLog::log->error("deflate failed.");
		delete [] buffer;
		return false;
	}

	// finish the deflate
	if(deflate(&stream, Z_FINISH) != Z_STREAM_END)
	{
		MyLog::log->error("deflate failed: did not end stream");
		delete [] buffer;
		return false;
	}

	// finish up
	if(deflateEnd(&stream) != Z_OK)
	{
		MyLog::log->error("deflateEnd failed.");
		delete [] buffer;
		return false;
	}

	// fill in the full size of the compressed stream
#ifdef USING_BIG_ENDIAN
	*(uint32*)&buffer[0] = swap32(size);
#else
	*(uint32*)&buffer[0] = size;
#endif

	// send it
	m_session->OutPacket(SMSG_COMPRESSED_UPDATE_OBJECT, (uint16)stream.total_out + 4, buffer);

	// cleanup memory
	delete [] buffer;

	return true;
}

void Player::ClearAllPendingUpdates()
{
	bProcessPending = false;
	//mUpdateCount = 0;
	//bUpdateBuffer.clear();
}

void Player::AddSplinePacket(uint64 guid, ByteBuffer* packet)
{
	SplineMap::iterator itr = _splineMap.find(guid);
	if(itr != _splineMap.end())
	{
		delete itr->second;
		_splineMap.erase(itr);
	}
	_splineMap.insert( SplineMap::value_type( guid, packet ) );
}

ByteBuffer* Player::GetAndRemoveSplinePacket(uint64 guid)
{
	SplineMap::iterator itr = _splineMap.find(guid);
	if(itr != _splineMap.end())
	{
		ByteBuffer *buf = itr->second;
		_splineMap.erase(itr);
		return buf;
	}
	return NULL;
}

void Player::ClearSplinePackets()
{
	SplineMap::iterator it2;
	for(SplineMap::iterator itr = _splineMap.begin(); itr != _splineMap.end(); ++itr)
	{
		delete itr->second;
	}
	_splineMap.clear();
}



bool Player::ExitInstance()
{
	if(!m_bgEntryPointX)
		return false;

	RemoveFromWorld();

	SafeTeleport(m_bgEntryPointMap, m_bgEntryPointInstance, LocationVector(
		m_bgEntryPointX, m_bgEntryPointY, m_bgEntryPointZ, m_bgEntryPointO));

	return true;
}

void Player::SaveEntryPoint(uint32 mapId)
{
	if(IS_INSTANCE(GetMapId()))
		return; // dont save if we're not on the main continent.
	//otherwise we could end up in an endless loop :P
	MapInfo * pMapinfo = WorldMapInfoStorage.LookupEntry(mapId);

	if(pMapinfo)
	{
		m_bgEntryPointX = pMapinfo->repopx;
		m_bgEntryPointY = pMapinfo->repopy;
		m_bgEntryPointZ = pMapinfo->repopz;
		m_bgEntryPointO = GetOrientation();
		m_bgEntryPointMap = pMapinfo->repopmapid;
		m_bgEntryPointInstance = GetInstanceID();
	}
	else
	{
		m_bgEntryPointMap	 = 0;
		m_bgEntryPointX		 = 0;
		m_bgEntryPointY		 = 0;
		m_bgEntryPointZ		 = 0;
		m_bgEntryPointO		 = 0;
		m_bgEntryPointInstance  = 0;
	}
}

void Player::CleanupGossipMenu()
{
	if(CurrentGossipMenu)
	{
		delete CurrentGossipMenu;
		CurrentGossipMenu = NULL;
	}
}

void Player::Gossip_Complete()
{
	MSG_S2C::stNPC_Gossip_Complete Msg;
	m_session->SendPacket( Msg );
	CleanupGossipMenu();
}

void Player::ZoneUpdate(uint32 ZoneId)
{
	m_zoneId = ZoneId;
	/* how the f*ck is this happening */
	if( m_playerInfo == NULL )
	{
		m_playerInfo = objmgr.GetPlayerInfo(GetLowGUID());
		if( m_playerInfo == NULL )
		{
			MyLog::log->alert("Player::ZoneUpdate m_playerinfo==NULL disconnect it");
			m_session->Disconnect();
			return;
		}
	}

	m_playerInfo->lastZone = ZoneId;
	sHookInterface.OnZone(this, ZoneId);

	AreaTable * at = AreaStorage.LookupEntry(GetAreaID());
	if(at && (at->category == AREAC_SANCTUARY || at->AreaFlags & AREA_SANCTUARY))
	{
		Unit * pUnit = (GetSelection() == 0) ? 0 : (m_mapMgr ? m_mapMgr->GetUnit(GetSelection()) : 0);
		if(pUnit && DuelingWith != pUnit)
		{
			EventAttackStop();
			smsg_AttackStop(pUnit);
		}

		if(m_currentSpell)
		{
			Unit * target = m_currentSpell->GetUnitTarget();
			if(target && target != DuelingWith && target != this)
				m_currentSpell->cancel();
		}
	}


#ifdef OPTIMIZED_PLAYER_SAVING
	save_Zone();
#endif

	/*std::map<uint32, AreaTable*>::iterator iter = sWorld.mZoneIDToTable.find(ZoneId);
	if(iter == sWorld.mZoneIDToTable.end())
		return;

	AreaTable *p = iter->second;
	if(p->AreaId != m_AreaID)
	{
		m_AreaID = p->AreaId;
		UpdatePVPStatus(m_AreaID);
	}

	MyLog::log->notice("ZONE_UPDATE: Player %s entered zone %s", GetName(), sAreaStore.LookupString((int)p->name));*/
}

void Player::SendTradeUpdate()
{
	Player * pTarget = GetTradeTarget();
	if(!pTarget)
		return;

	MSG_S2C::stTradeStat_Ext Msg;
	Msg.slotcount1	= m_tradeSequence;
	Msg.slotcount2	= m_tradeSequence++;
	Msg.gold		= mTradeGold;

	for(uint8 i = 0; i < TRADE_SLOT_COUNT; i++)
	{
		Item * pItem = mTradeItems[i];
		if(pItem != 0)
		{
			Msg.tradeitem[i].entry			= pItem->GetProto()->ItemId;      // entry
			Msg.tradeitem[i].displayinfoid	= pItem->GetProto()->DisplayInfoID[0][0];
			Msg.tradeitem[i].stackcount		= pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT);
			Msg.tradeitem[i].giftcreator	= pItem->GetUInt64Value(ITEM_FIELD_GIFTCREATOR);
			Msg.tradeitem[i].creator		= pItem->GetUInt64Value(ITEM_FIELD_CREATOR);
			Msg.tradeitem[i].spellcharge	= pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT);       // charges
			for( int j = 0; j < 6; ++j )
				Msg.tradeitem[i].extra.enchants[j] = pItem->GetUInt32Value( ITEM_FIELD_ENCHANTMENT + j * 3 );
			Msg.tradeitem[i].extra.jinglian_level	= pItem->GetUInt32Value(ITEM_FIELD_JINGLIAN_LEVEL);
			Msg.tradeitem[i].maxduration	= pItem->GetUInt32Value( ITEM_FIELD_MAXDURABILITY );
			Msg.tradeitem[i].duration		= pItem->GetUInt32Value( ITEM_FIELD_DURABILITY );
		}
	}

	pTarget->GetSession()->SendPacket(Msg);
}

void Player::RequestDuel(Player *pTarget)
{
	// We Already Dueling or have already Requested a Duel

	if( DuelingWith != NULL )
		return;

	if( m_duelState != DUEL_STATE_FINISHED )
		return;

	SetDuelState( DUEL_STATE_REQUESTED );

	//Setup Duel
	pTarget->DuelingWith = this;
	DuelingWith = pTarget;

	//Get Flags position
	float dist = CalcDistance(pTarget);
	dist = dist * 0.5f; //half way
	float x = (GetPositionX() + pTarget->GetPositionX()*dist)/(1+dist) + cos(GetOrientation()+(float(M_PI)/2))*2;
	float y = (GetPositionY() + pTarget->GetPositionY()*dist)/(1+dist) + sin(GetOrientation()+(float(M_PI)/2))*2;
	float z = (GetPositionZ() + pTarget->GetPositionZ()*dist)/(1+dist);

	//Create flag/arbiter
	GameObject* pGameObj = GetMapMgr()->CreateGameObject(21680);
	pGameObj->CreateFromProto(21680,GetMapId(), x, y, z, GetOrientation());
	pGameObj->SetInstanceID(GetInstanceID());

	//Spawn the Flag
	pGameObj->SetUInt64Value(OBJECT_FIELD_CREATED_BY, GetGUID());
	pGameObj->SetUInt32Value(GAMEOBJECT_FACTION, GetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE));
	pGameObj->SetUInt32Value(GAMEOBJECT_LEVEL, GetUInt32Value(UNIT_FIELD_LEVEL));

	//Assign the Flag
	SetUInt64Value(PLAYER_DUEL_ARBITER,pGameObj->GetGUID());
	pTarget->SetUInt64Value(PLAYER_DUEL_ARBITER,pGameObj->GetGUID());

	pGameObj->PushToWorld(m_mapMgr);

	MSG_S2C::stDuel_Request Msg;
	Msg.flag_guid = pGameObj->GetGUID();
	Msg.player_guid = GetGUID();
	pTarget->GetSession()->SendPacket(Msg);
}

void Player::DuelCountdown()
{
	if( DuelingWith == NULL )
		return;

	m_duelCountdownTimer -= 1000;

	if( m_duelCountdownTimer < 0 )
		m_duelCountdownTimer = 0;

	if( m_duelCountdownTimer == 0 )
	{
		// Start Duel.
		//Give the players a Team
		DuelingWith->SetUInt32Value( PLAYER_DUEL_TEAM, 1 ); // Duel Requester
		SetUInt32Value( PLAYER_DUEL_TEAM, 2 );

		SetDuelState( DUEL_STATE_STARTED );
		DuelingWith->SetDuelState( DUEL_STATE_STARTED );

		sEventMgr.AddEvent( this, &Player::DuelBoundaryTest, EVENT_PLAYER_DUEL_BOUNDARY_CHECK, 500, 0, 0 );
		sEventMgr.AddEvent( DuelingWith, &Player::DuelBoundaryTest, EVENT_PLAYER_DUEL_BOUNDARY_CHECK, 500, 0, 0 );
	}
}

void Player::DuelBoundaryTest()
{
	//check if in bounds
	if(!IsInWorld())
		return;

	GameObject * pGameObject = GetMapMgr()->GetGameObject(GetUInt32Value(PLAYER_DUEL_ARBITER));
	if(!pGameObject)
	{
		EndDuel(DUEL_WINNER_RETREAT);
		return;
	}

	float Dist = CalcDistance((Object*)pGameObject);

	if(Dist > 75.0f)
	{
		// Out of bounds
		if(m_duelStatus == DUEL_STATUS_OUTOFBOUNDS)
		{
			// we already know, decrease timer by 500
			m_duelCountdownTimer -= 500;
			if(m_duelCountdownTimer == 0)
			{
				// Times up :p
				DuelingWith->EndDuel(DUEL_WINNER_RETREAT);
			}
		}
		else
		{
			// we just went out of bounds
			// set timer
			m_duelCountdownTimer = 10000;

			// let us know
			MSG_S2C::stDuel_OutOfBounds Msg;
			Msg.duelCountdownTimer = m_duelCountdownTimer;
			m_session->SendPacket( Msg );
			m_duelStatus = DUEL_STATUS_OUTOFBOUNDS;
		}
	}
	else
	{
		// we're in range
		if(m_duelStatus == DUEL_STATUS_OUTOFBOUNDS)
		{
			// just came back in range
			MSG_S2C::stDuel_InBounds Msg;
			m_session->SendPacket( Msg );
			m_duelStatus = DUEL_STATUS_INBOUNDS;
		}
	}
}

void Player::EndDuel(uint8 WinCondition)
{
	if( m_duelState == DUEL_STATE_FINISHED )
		return;

	// Remove the events
	sEventMgr.RemoveEvents( this, EVENT_PLAYER_DUEL_COUNTDOWN );
	sEventMgr.RemoveEvents( this, EVENT_PLAYER_DUEL_BOUNDARY_CHECK );

	for( uint32 x = 0; x < MAX_AURAS; ++x )
	{
		if( m_auras[x] == NULL )
			continue;
		if( m_auras[x]->WasCastInDuel() )
			m_auras[x]->Remove();
	}

	m_duelState = DUEL_STATE_FINISHED;

	if( DuelingWith == NULL )
		return;

	sEventMgr.RemoveEvents( DuelingWith, EVENT_PLAYER_DUEL_BOUNDARY_CHECK );
	sEventMgr.RemoveEvents( DuelingWith, EVENT_PLAYER_DUEL_COUNTDOWN );

	// spells waiting to hit
	sEventMgr.RemoveEvents(this, EVENT_SPELL_DAMAGE_HIT);

	for( uint32 x = 0; x < MAX_AURAS; ++x )
	{
		if( DuelingWith->m_auras[x] == NULL )
			continue;
		if( DuelingWith->m_auras[x]->WasCastInDuel() )
			DuelingWith->m_auras[x]->Remove();
	}

	DuelingWith->m_duelState = DUEL_STATE_FINISHED;

	//Announce Winner
	MSG_S2C::stDuel_Winner MsgWin;
	MsgWin.WinCondition = uint8( WinCondition );
	MsgWin.winner_name = GetName();
	MsgWin.DuelingWith_name = DuelingWith->GetName();
	SendMessageToSet( MsgWin, true );

	MSG_S2C::stDuel_Complete Msg;
	Msg.unk = 1;
	SendMessageToSet( Msg, true );

	//Clear Duel Related Stuff

	GameObject* arbiter = m_mapMgr ? GetMapMgr()->GetGameObject(GetUInt32Value(PLAYER_DUEL_ARBITER)) : 0;

	if( arbiter != NULL )
	{
		arbiter->RemoveFromWorld( true );
		delete arbiter;
	}

	SetUInt64Value( PLAYER_DUEL_ARBITER, 0 );
	DuelingWith->SetUInt64Value( PLAYER_DUEL_ARBITER, 0 );

	SetUInt32Value( PLAYER_DUEL_TEAM, 0 );
	DuelingWith->SetUInt32Value( PLAYER_DUEL_TEAM, 0 );

	EventAttackStop();
	DuelingWith->EventAttackStop();

	// Call off pet
	if( this->GetSummon() != NULL )
	{
		this->GetSummon()->CombatStatus.Vanished();
		this->GetSummon()->RemoveAllAuras();
		this->GetSummon()->GetAIInterface()->SetUnitToFollow( this );
		this->GetSummon()->GetAIInterface()->HandleEvent( EVENT_FOLLOWOWNER, this->GetSummon(), 0 );
		this->GetSummon()->GetAIInterface()->WipeTargetList();
	}

	// removing auras that kills players after if low HP
	/*RemoveNegativeAuras(); NOT NEEDED. External targets can always gank both duelers with DoTs. :D
	DuelingWith->RemoveNegativeAuras();*/

	//Stop Players attacking so they don't kill the other player
	MSG_S2C::stCombat_Cancel MsgCombatCancel;
	m_session->SendPacket(MsgCombatCancel);
	DuelingWith->m_session->SendPacket(MsgCombatCancel);

	smsg_AttackStop( DuelingWith );
	DuelingWith->smsg_AttackStop( this );

	DuelingWith->m_duelCountdownTimer = 0;
	m_duelCountdownTimer = 0;

	DuelingWith->DuelingWith = NULL;
	DuelingWith = NULL;
}

void Player::StopMirrorTimer(uint32 Type)
{
//	MSG_S2C::stStopMirrorTimer Msg;
//	Msg.type = Type;
//	m_session->SendPacket( Msg );
}

void Player::EventTeleport(uint32 mapid, float x, float y, float z)
{
	SafeTeleport(mapid, 0, LocationVector(x, y, z));
}

void Player::ApplyLevelInfo(LevelInfo* Info, uint32 Level)
{
	// Apply level
	SetUInt32Value(UNIT_FIELD_LEVEL, Level);

	// Set next level conditions
	SetUInt32Value(PLAYER_NEXT_LEVEL_XP, Info->XPToNextLevel);

	// Set stats
	for(uint32 i = 0; i < 6; ++i)
	{
		BaseStats[i] = Info->Stat[i];
		CalcStat(i);
	}

	// Set health / mana
	SetUInt32Value(UNIT_FIELD_HEALTH, Info->HP);
	SetUInt32Value(UNIT_FIELD_MAXHEALTH, Info->HP);
	SetUInt32Value(UNIT_FIELD_POWER1, Info->Mana);
	SetUInt32Value(UNIT_FIELD_MAXPOWER1, Info->Mana);

	// Calculate talentpoints
	uint32 TalentPoints = 0;
	if(Level >= 10)
		TalentPoints = Level - 9;

	SetUInt32Value(PLAYER_CHARACTER_POINTS1, TalentPoints);

	// Set base fields
	SetUInt32Value(UNIT_FIELD_BASE_HEALTH, Info->HP);
	SetUInt32Value(UNIT_FIELD_BASE_MANA, Info->Mana);

	_UpdateMaxSkillCounts();
	UpdateStats();
	//UpdateChances();
	m_playerInfo->lastLevel = Level;
	this->lvlinfo = Info;

	MyLog::log->notice("Player %s set parameters to level %u", GetName(), Level);
}

void Player::BroadcastMessage(const char* Format, ...)
{
	va_list l;
	va_start(l, Format);
	char Message[1024];
	vsnprintf(Message, 1024, Format, l);
	va_end(l);

	MSG_S2C::stChat_Message Msg;
	sChatHandler.FillSystemMessageData(Message, &Msg);
	m_session->SendPacket(Msg);
}
/*
const double BaseRating []= {
	2.5,//weapon_skill_ranged!!!!
	1.5,//defense=comba_r_1
	12,//dodge
	20,//parry=3
	5,//block=4
	10,//melee hit
	10,//ranged hit
	8,//spell hit=7
	14,//melee critical strike=8
	14,//ranged critical strike=9
	14,//spell critical strike=10
	0,//
	0,
	0,
	25,//resilience=14
	25,//resil .... meaning unknown
	25,//resil .... meaning unknown
	10,//MELEE_HASTE_RATING=17
	10,//RANGED_HASTE_RATING=18
	10,//spell_haste_rating = 19???
	2.5,//melee weapon skill==20
	2.5,//melee second hand=21

};
*/
float Player::CalcRating( uint32 index )
{

	float rating = float(m_uint32Values[index]);
	float RC = 10.f + getLevel() * 0.2f;
	float dodgeRC = 0.f;
	float spellRC= 13.f + getLevel() * 0.1f;

	float PhyRC = 10.f + getLevel() * 0.1f;

	switch(getClass())
	{
	case CLASS_BOW:
		{
			//NormalAgility = 60.33f;
			dodgeRC = 30.f;
		}
		break;
	default:
		{
			//NormalAgility = 40.7f;
			dodgeRC = 20.f;
		}
		break;
	}
	dodgeRC = dodgeRC + getLevel() * 0.1f;

	float returnValue = 0.f;
	switch(index)
	{
	case PLAYER_RATING_MODIFIER_DODGE:
		{
			 returnValue = 0.01f * float(GetUInt32Value(PLAYER_RATING_MODIFIER_DODGE)) / dodgeRC;
		}
		break;
	case PLAYER_RATING_MODIFIER_MELEE_HIT:
		{
			returnValue = 0.01f * float(GetUInt32Value(PLAYER_RATING_MODIFIER_MELEE_HIT)) / PhyRC;
		}
		break;
	case PLAYER_RATING_MODIFIER_RANGED_HIT:
		{
			returnValue = 0.01f * float(GetUInt32Value(PLAYER_RATING_MODIFIER_RANGED_HIT)) / PhyRC;
		}
		break;
		
	case PLAYER_RATING_MODIFIER_SPELL_HIT:
		{
			returnValue = 0.01f * float(GetUInt32Value( PLAYER_RATING_MODIFIER_SPELL_HIT )) /spellRC;
		}
		break;
	case PLAYER_RATING_MODIFIER_MELEE_CRIT:
		{
			returnValue =  0.01f * float(GetUInt32Value(PLAYER_RATING_MODIFIER_MELEE_CRIT))/ RC;
		}
		break;
	case PLAYER_RATING_MODIFIER_RANGED_CRIT:
		{
			returnValue =  0.01f * float(GetUInt32Value(PLAYER_RATING_MODIFIER_RANGED_CRIT))/ RC; 
		}
		break;
	case PLAYER_RATING_MODIFIER_SPELL_CRIT:
		{
			returnValue = .01f * float(GetUInt32Value(PLAYER_RATING_MODIFIER_SPELL_CRIT)) / spellRC;
		}
		break;
	case PLAYER_RATING_MODIFIER_MELEE_HIT_AVOIDANCE:
	case PLAYER_RATING_MODIFIER_RANGED_HIT_AVOIDANCE:
	case PLAYER_RATING_MODIFIER_SPELL_HIT_AVOIDANCE:
	case PLAYER_RATING_MODIFIER_MELEE_CRIT_RESILIENCE:
	case PLAYER_RATING_MODIFIER_RANGED_CRIT_RESILIENCE:
	case PLAYER_RATING_MODIFIER_SPELL_CRIT_RESILIENCE:
	case PLAYER_RATING_MODIFIER_MELEE_HASTE	:
	case PLAYER_RATING_MODIFIER_RANGED_HASTE:
	case PLAYER_RATING_MODIFIER_SPELL_HASTE:
	case PLAYER_RATING_MODIFIER_MELEE_MAIN_HAND_SKILL:
	case PLAYER_RATING_MODIFIER_MELEE_OFF_HAND_SKILL:
	case PLAYER_RATING_MODIFIER_RANGED_SKILL:
	case PLAYER_RATING_MODIFIER_DEFENCE:
	case PLAYER_RATING_MODIFIER_PARRY:
	case PLAYER_RATING_MODIFIER_BLOCK:
		{
			returnValue = 0.f;
		}
		break;

	};

	return returnValue;

	//uint32 relative_index = index - (PLAYER_FIELD_COMBAT_RATING_1);
	//float rating = float(m_uint32Values[index]);
	///*if( relative_index <= 10 || ( relative_index >= 14 && relative_index <= 21 ) )
	//{
	//	double rating = (double)m_uint32Values[index];
	//	int level = getLevel();
	//	if( level < 10 )//this is not dirty fix -> it is from wowwiki
	//		level = 10;
	//	double cost;
	//	if( level < 60 )
	//		cost = ( double( level ) - 8.0 ) / 52.0;
	//	else
	//		cost = 82.0 / ( 262.0 - 3.0 *  double( level ) );
	//	return float( rating / ( BaseRating[relative_index] * cost ) );
	//}
	//else
	//	return 0.0f;*/

	//uint32 level = m_uint32Values[UNIT_FIELD_LEVEL];
	//if( level > 100 )
	//	level = 100;

	//CombatRatingDBC * pDBCEntry = dbcCombatRating.LookupEntry( relative_index * 100 + level - 1 );
	//if( pDBCEntry == NULL )
	//	return rating;
	//else
	//	return (rating / pDBCEntry->val);
}

bool Player::SafeTeleport(uint32 MapID, uint32 InstanceID, float X, float Y, float Z, float O)
{
	return SafeTeleport(MapID, InstanceID, LocationVector(X, Y, Z, O));
}

bool Player::SafeTeleport(uint32 MapID, uint32 InstanceID, const LocationVector & vec)
{
#ifdef CLUSTERING
	/* Clustering Version */
	MapInfo * mi = WorldMapInfoStorage.LookupEntry(MapID);

	uint32 instance_id;
	bool map_change = false;
	if(mi && mi->type == 0)
	{
		// single instance map
		if(MapID != m_mapId)
		{
			map_change = true;
			instance_id = 0;
		}
	}
	else
	{
		// instanced map
		if(InstanceID != GetInstanceID())
			map_change = true;

		instance_id = InstanceID;
	}

	if(map_change)
	{
		MSG_S2C::stTransfer_Pending Msg;
		Msg.mapid = MapID;
		GetSession()->SendPacket(Msg);
		sClusterInterface.RequestTransfer(this, MapID, instance_id, vec);
		return;
	}

	m_sentTeleportPosition = vec;
	SetPosition(vec);
	ResetHeartbeatCoords();

	MSG_S2C::stMove_Teleport_Ack Msg;
	BuildTeleportAckMsg(Msg);
	m_session->SendPacket(Msg);
#else

	/* Normal Version */
	bool instance = false;
	MapInfo * mi = WorldMapInfoStorage.LookupEntry(MapID);

	if(InstanceID && (uint32)m_instanceId != InstanceID)
	{
		instance = true;
		this->SetInstanceID(InstanceID);
#ifndef COLLISION
#endif
	}
	else if(m_mapId != MapID)
	{
		instance = true;
#ifndef COLLISION
#endif
	}

	//DismissMount();
	DismissFootAndHead();

	// make sure player does not drown when teleporting from under water
	if (m_UnderwaterState & UNDERWATERSTATE_UNDERWATER)
		m_UnderwaterState &= ~UNDERWATERSTATE_UNDERWATER;

	if(flying_aura && MapID != 530)
	{
		RemoveAura(flying_aura);
		flying_aura = 0;
	}

	_Relocate(MapID, vec, true, instance, InstanceID);
	return true;
#endif
}

bool Player::DismissFootAndHead()
{
	if(GetUInt64Value(PLAYER_HEAD))
	{
		Player* plr = ((Player*)GetMapMgr()->_GetObject( GetUInt64Value(PLAYER_HEAD) ));
		SetUInt64Value(PLAYER_HEAD, 0);
		if(plr)
			plr->SetUInt64Value(PLAYER_FOOT, 0);
	}
	if(GetUInt64Value(PLAYER_FOOT))
	{
		Player* plr = ((Player*)GetMapMgr()->_GetObject( GetUInt64Value(PLAYER_FOOT) ));
		SetUInt64Value(PLAYER_FOOT, 0);
		if(plr)
			plr->SetUInt64Value(PLAYER_HEAD, 0);
	}
	return true;
}

bool Player::DismissMount()
{
	/*
	if(m_ShapeShifted)
	{
		RemoveAura(m_ShapeShifted);
	}
	*/
	if(m_MountSpellId)
	{
		RemoveAura(m_MountSpellId);
	}
	//m_ShapeShifted = 0;
	m_MountSpellId = 0;

	DismissFootAndHead();
	SetUInt32Value( PLAYER_FIELD_IS_FLYING, 0 );

	if( m_mapMgr )
		m_mapMgr->ForceUpdateObjects();
	return true;
}

bool Player::RemoveShapeShift()
{
	if(m_ShapeShifted)
	{
		RemoveAura(m_ShapeShifted);
	}
	m_ShapeShifted = 0;

	DismissFootAndHead();
	SetUInt32Value( PLAYER_FIELD_IS_FLYING, 0 );
	return true;
}

void Player::SafeTeleport(MapMgr * mgr, const LocationVector & vec)
{
	if(flying_aura && mgr->GetMapId()!=530) {
		RemoveAura(flying_aura);
		flying_aura=0;
	}

	//DismissMount();
	DismissFootAndHead();

	if(IsInWorld())
	{
		RemoveFromWorld();
		UpdateVisibility();
	}

	m_mapId = mgr->GetMapId();
	m_instanceId = mgr->GetInstanceID();
	MSG_S2C::stTransfer_Pending MsgTrans;
	MsgTrans.mapid = mgr->GetMapId();
	GetSession()->SendPacket(MsgTrans);

	MSG_S2C::stMove_New_World Msg;
	Msg.map_id	= m_mapId;
	Msg.pos_x	= vec.x;
	Msg.pos_y	= vec.y;
	Msg.pos_z	= vec.z;
	Msg.orientation	= vec.o;
	GetSession()->SendPacket(Msg);

	AddToWorld(mgr);

	SetPosition(vec);

	UpdateVisibility();

	SetPlayerStatus(TRANSFER_PENDING);
	m_sentTeleportPosition = vec;
	ResetHeartbeatCoords();
}

void Player::SetGuildId(uint32 guildId)
{
	if(IsInWorld())
	{
		const uint32 field = PLAYER_GUILDID;
		sEventMgr.AddEvent(((Object*)this), &Object::EventSetUInt32Value, field, guildId, EVENT_PLAYER_SEND_PACKET, 1,
			1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
	}
	else
	{
		SetUInt32Value(PLAYER_GUILDID,guildId);
	}
	if( guildId == 0 )
		SetUInt32Value( PLAYER_FIELD_HOSTILE_GUILDID, 0 );
}

void Player::SetHostileGuild( uint32 id )
{
	SetUInt32Value( PLAYER_FIELD_HOSTILE_GUILDID, id );
}

void Player::SetGuildRank(uint32 guildRank)
{
	if(IsInWorld())
	{
		const uint32 field = PLAYER_GUILDRANK;
		sEventMgr.AddEvent(((Object*)this), &Object::EventSetUInt32Value, field, guildRank, EVENT_PLAYER_SEND_PACKET, 1,
			1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
	}
	else
	{
		SetUInt32Value(PLAYER_GUILDRANK,guildRank);
	}
}

void Player::UpdatePvPArea()
{
	AreaTable * at = AreaStorage.LookupEntry(m_AreaID);
	if(at == 0)
        return;

	// This is where all the magic happens :P
    if((at->category == AREAC_ALLIANCE_TERRITORY && GetTeam() == 0) || (at->category == AREAC_HORDE_TERRITORY && GetTeam() == 1))
	{
		if(!HasFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE) && !m_pvpTimer)
		{
			// I'm flagged and I just walked into a zone of my type. Start the 5min counter.
			ResetPvPTimer();
		}
        return;
	}
	else
	{
        //Enemy city check
        if(at->AreaFlags & AREA_CITY_AREA || at->AreaFlags & AREA_CITY)
        {
            if((at->category == AREAC_ALLIANCE_TERRITORY && GetTeam() == 1) || (at->category == AREAC_HORDE_TERRITORY && GetTeam() == 0))
            {
                if(!IsPvPFlagged()) SetPvPFlag();
                    StopPvPTimer();
                return;
            }
        }

        //fix for zone areas.
        if(at->ZoneId)
        {
            AreaTable * at2 = AreaStorage.LookupEntry(at->ZoneId);
            if(at2 && (at2->category == AREAC_ALLIANCE_TERRITORY && GetTeam() == 0) || at2 && (at2->category == AREAC_HORDE_TERRITORY && GetTeam() == 1))
            {
                if(!HasFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE) && !m_pvpTimer)
		        {
			        // I'm flagged and I just walked into a zone of my type. Start the 5min counter.
			        ResetPvPTimer();
		        }
                return;
            }
            //enemy territory check
            if(at2 && at2->AreaFlags & AREA_CITY_AREA || at2 && at2->AreaFlags & AREA_CITY)
            {
                if(at2 && (at2->category == AREAC_ALLIANCE_TERRITORY && GetTeam() == 1) || at2 && (at2->category == AREAC_HORDE_TERRITORY && GetTeam() == 0))
                {
                   if(!IsPvPFlagged()) SetPvPFlag();
                       StopPvPTimer();
                   return;
                }
            }
        }

		// I just walked into either an enemies town, or a contested zone.
		// Force flag me if i'm not already.
        if(at->category == AREAC_SANCTUARY || at->AreaFlags & AREA_SANCTUARY)
        {
            if(IsPvPFlagged()) RemovePvPFlag();

			RemoveFlag(PLAYER_FLAGS, PLAYER_FLAG_FREE_FOR_ALL_PVP);

            StopPvPTimer();
        }
        else
        {
            //contested territory
            if(sWorld.GetRealmType() == REALM_PVP)
            {
                //automaticaly sets pvp flag on contested territorys.
                if(!IsPvPFlagged()) SetPvPFlag();
                StopPvPTimer();
            }

            if(sWorld.GetRealmType() == REALM_PVE)
            {
                if(HasFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE))
                {
                    if(!IsPvPFlagged()) SetPvPFlag();
                }
                else if(!HasFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE) && IsPvPFlagged() && !m_pvpTimer)
                {
                    ResetPvPTimer();
                }
            }

            if(at->AreaFlags & AREA_PVP_ARENA)			/* ffa pvp arenas will come later */
            {
                if(!IsPvPFlagged()) SetPvPFlag();

			    SetFlag(PLAYER_FLAGS, PLAYER_FLAG_FREE_FOR_ALL_PVP);
            }
            else
            {
			    RemoveFlag(PLAYER_FLAGS, PLAYER_FLAG_FREE_FOR_ALL_PVP);
            }
        }
	}
}

void Player::BuildFlagUpdateForNonGroupSet(uint32 index, uint32 flag)
{
    Object *curObj;
    for (Object::InRangeSet::iterator iter = GetInRangeSetBegin(); iter != GetInRangeSetEnd();)
	{
		curObj = *iter;
		iter++;
        if(curObj->IsPlayer())
        {
            Group* pGroup = static_cast< Player* >( curObj )->GetGroup();
            if( pGroup != NULL && pGroup == GetGroup())
            {
				//TODO: huh? if this is unneeded change the if to the inverse and don't waste jmp space
            }
            else
            {
                BuildFieldUpdatePacket( static_cast< Player* >( curObj ), index, flag );
            }
        }
    }
}

void Player::LoginPvPSetup()
{
	// Make sure we know our area ID.
	_EventExploration();

//     if(isAlive())
//         CastSpell(this, PLAYER_HONORLESS_TARGET_SPELL, true);
}

void Player::PvPToggle()
{
    if(sWorld.GetRealmType() == REALM_PVE)
    {
	    if(m_pvpTimer > 0)
	    {
		    // Means that we typed /pvp while we were "cooling down". Stop the timer.
		    StopPvPTimer();

		    SetFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE);

            if(!IsPvPFlagged()) SetPvPFlag();
	    }
	    else
	    {
		    if(IsPvPFlagged())
		    {
                AreaTable * at = AreaStorage.LookupEntry(m_AreaID);
                if(at && at->AreaFlags & AREA_CITY_AREA || at && at->AreaFlags & AREA_CITY)
                {
                    if(at && (at->category == AREAC_ALLIANCE_TERRITORY && GetTeam() == 1) || at && (at->category == AREAC_HORDE_TERRITORY && GetTeam() == 0))
                    {
                    }
                    else
                    {
                        // Start the "cooldown" timer.
			            ResetPvPTimer();
                    }
                }
                else
                {
                    // Start the "cooldown" timer.
			        ResetPvPTimer();
                }
			    RemoveFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE);
		    }
		    else
		    {
			    // Move into PvP state.
			    SetFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE);

			    StopPvPTimer();
			    SetPvPFlag();
		    }
	    }
    }
    else if(sWorld.GetRealmType() == REALM_PVP)
    {
        AreaTable * at = AreaStorage.LookupEntry(m_AreaID);
	    if(at == 0)
            return;

	    // This is where all the magic happens :P
        if((at->category == AREAC_ALLIANCE_TERRITORY && GetTeam() == 0) || (at->category == AREAC_HORDE_TERRITORY && GetTeam() == 1))
	    {
            if(m_pvpTimer > 0)
	        {
		        // Means that we typed /pvp while we were "cooling down". Stop the timer.
		        StopPvPTimer();

		        SetFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE);

                if(!IsPvPFlagged()) SetPvPFlag();
	        }
	        else
	        {
		        if(IsPvPFlagged())
		        {
                    // Start the "cooldown" timer.
			        ResetPvPTimer();

			        RemoveFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE);
		        }
		        else
		        {
			        // Move into PvP state.
			        SetFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE);

			        StopPvPTimer();
			        SetPvPFlag();
		        }
	        }
        }
        else
        {
            if(at->ZoneId)
            {
                AreaTable * at2 = AreaStorage.LookupEntry(at->ZoneId);
                if(at2 && (at2->category == AREAC_ALLIANCE_TERRITORY && GetTeam() == 0) || at2 && (at2->category == AREAC_HORDE_TERRITORY && GetTeam() == 1))
                {
                    if(m_pvpTimer > 0)
	                {
		                // Means that we typed /pvp while we were "cooling down". Stop the timer.
		                StopPvPTimer();

		                SetFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE);

                        if(!IsPvPFlagged()) SetPvPFlag();
	                }
	                else
	                {
		                if(IsPvPFlagged())
		                {
			                // Start the "cooldown" timer.
			                ResetPvPTimer();

			                RemoveFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE);
		                }
		                else
		                {
			                // Move into PvP state.
			                SetFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE);

			                StopPvPTimer();
			                SetPvPFlag();
		                }
	                }
                    return;
                }
            }

            if(!HasFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE))
		        SetFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE);
            else
            {
			    RemoveFlag(PLAYER_FLAGS, PLAYER_FLAG_PVP_TOGGLE);
            }
        }
    }
}

void Player::ResetPvPTimer()
{
	 m_pvpTimer = sWorld.getIntRate(INTRATE_PVPTIMER);
}

void Player::CalculateBaseStats()
{
	if(!lvlinfo) return;

	memcpy(BaseStats, lvlinfo->Stat, sizeof(BaseStats));

	LevelInfo * levelone = objmgr.GetLevelInfo(this->getRace(),this->getClass(),1);
	SetUInt32Value(UNIT_FIELD_MAXHEALTH, lvlinfo->HP);
	SetUInt32Value(UNIT_FIELD_BASE_HEALTH, lvlinfo->HP);
	SetUInt32Value(PLAYER_NEXT_LEVEL_XP, lvlinfo->XPToNextLevel);


	if(GetPowerType() == POWER_TYPE_MANA)
	{
		SetUInt32Value(UNIT_FIELD_BASE_MANA, lvlinfo->Mana);
		SetUInt32Value(UNIT_FIELD_MAXPOWER1, lvlinfo->Mana);
	}
}

void Player::CompleteLoading()
{
	switch (getClass())
	{
	case CLASS_BOW:
		{
			if(!m_AutoShotSpell)
			{
				m_AutoShotSpell = dbcSpell.LookupEntry(5003);
			}
		}
		break;
	case CLASS_MAGE:
		{
		}
		break;
	case CLASS_WARRIOR:
		{
		}
		break;
	case CLASS_PRIEST:
		{
		}
		break;
	default:
		break;
	}

    // cast passive initial spells	  -- grep note: these shouldnt require plyr to be in world
	SpellSet::iterator itr;
	SpellCastTargets targets;
	targets.m_unitTarget = this->GetGUID();
	targets.m_targetMask = 0x2;

	//UpdateStats();
	g_instancequeuemgr.SendInstanceList2Player( this );

	if( GetUInt32Value(PLAYER_CHOSEN_TITLE) )
	{
		if( m_Titles.find( GetUInt32Value(PLAYER_CHOSEN_TITLE) ) == m_Titles.end() )
		{
			SetUInt32Value(PLAYER_CHOSEN_TITLE, 0);
		}
		else
		{
			stPlayerTitle* pEntry = PlayerTitleStorage.LookupEntry(GetUInt32Value(PLAYER_CHOSEN_TITLE));
			if(pEntry)
			{
				ApplyTitleMods(pEntry, true);
			}
		}
	}
	//* opened by Gui.
	for (itr = mSpells.begin(); itr != mSpells.end(); ++itr)
	{
		SpellEntry* info = dbcSpell.LookupEntry(*itr);

		if(	info && (info->Attributes & ATTRIBUTES_PASSIVE) ) // passive
		{
			if (
			info->Id == 70011 || info->Id == 70010 ||
			info->Id == 70009 || info->Id == 70008 ||
			info->Id == 70007 || info->Id == 70006 ||
			info->Id == 70005 || info->Id == 70004 ||
			info->Id == 70003 || info->Id == 70037 ||
			info->Id == 70038 || info->Id == 70039 ||
			info->Id == 70042 || info->Id == 70041 || 
			info->Id == 70040
			)
				continue;

			Spell * spell=new Spell(this,info,true,NULL);
			spell->prepare(&targets);
		}
	}
	//*/

	std::list<LoginAura>::iterator i =  loginauras.begin();

    for(;i!=loginauras.end(); i++)
	{

		//check if we already have this aura
//		if(this->HasActiveAura((*i).id))
//			continue;
		//how many times do we intend to put this oura on us
/*		uint32 count_appearence=0;
		std::list<LoginAura>::iterator i2 =  i;
		for(;i2!=loginauras.end();i2++)
			if((*i).id==(*i2).id)
			{
				count_appearence++;
			}
*/

		// this stuff REALLY needs to be fixed - Burlex
		SpellEntry * sp = dbcSpell.LookupEntry((*i).id);

		if(sp)
		{
			if ( sp->c_is_flags & SPELL_FLAG_IS_EXPIREING_WITH_PET )
				continue; //do not load auras that only exist while pet exist. We should recast these when pet is created anyway

			Aura* a = new Aura(sp, (*i).dur, this, this);

			for(uint32 x =0;x<3;x++)
			{
				if(sp->Effect[x]==SPELL_EFFECT_APPLY_AURA)
				{
					a->AddMod(sp->EffectApplyAuraName[x],sp->EffectBasePoints[x]+1,sp->EffectMiscValue[x],x);
				}
			}

			this->AddAura(a);		//FIXME: must save amt,pos/neg
		}

		//Somehow we should restore number of appearence. Right now i have no idea how :(
//		if(count_appearence>1)
//			this->AddAuraVisual((*i).id,count_appearence-1,a->IsPositive());
	}

	//switch( getGender() )
	//{
	//case 0:
	//	{
	//		addSpell( 7008 );

	//		removeSpell( 7009, false, false, 0 );
	//	}
	//	break;
	//case 1:
	//	{
	//		addSpell( 7009 );

	//		removeSpell( 7008, false, false, 0 );
	//	}
	//	break;
	//}

	//switch( getRace() )
	//{
	//case RACE_YAO:
	//	{
	//		addSpell( 7006 );
	//		addSpell( 7007 );
	//		addSpell( 7010 );

	//		removeSpell( 7002, false, false, 0 );
	//		removeSpell( 7003, false, false, 0 );
	//		removeSpell( 7004, false, false, 0 );
	//		removeSpell( 7005, false, false, 0 );
	//		removeSpell( 7011, false, false, 0 );
	//		removeSpell( 7012, false, false, 0 );
	//	}
	//	break;
	//case RACE_REN:
	//	{
	//		addSpell( 7002 );
	//		addSpell( 7003 );
	//		addSpell( 7011 );

	//		removeSpell( 7004, false, false, 0 );
	//		removeSpell( 7005, false, false, 0 );
	//		removeSpell( 7006, false, false, 0 );
	//		removeSpell( 7007, false, false, 0 );
	//		removeSpell( 7010, false, false, 0 );
	//		removeSpell( 7012, false, false, 0 );
	//	}
	//	break;
	//case RACE_WU:
	//	{
	//		addSpell( 7004 );
	//		addSpell( 7005 );
	//		addSpell( 7012 );

	//		removeSpell( 7002, false, false, 0 );
	//		removeSpell( 7003, false, false, 0 );
	//		removeSpell( 7006, false, false, 0 );
	//		removeSpell( 7007, false, false, 0 );
	//		removeSpell( 7010, false, false, 0 );
	//		removeSpell( 7011, false, false, 0 );
	//	}
	//	break;
	//}

	//if(iActivePet)
	//	SpawnPet(iActivePet);	   // only spawn if >0

	// Banned
	if(GetSession() && GetSession()->m_baned > (uint32)UNIXTIME)
	{
		Kick(10000);
		BroadcastMessage("character is banned. expire:[%s].", ConvertTimeStampToDataTime(GetSession()->m_baned).c_str());
		//BroadcastMessage("You have been banned for: %s", GetBanReason().c_str());
	}

	if(m_playerInfo->m_Group)
	{
		sEventMgr.AddEvent(this, &Player::EventGroupFullUpdate, EVENT_UNK, 100, 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
		SetUInt32Value( PLAYER_FIELD_GROUP_ID, m_playerInfo->m_Group->GetID() );
	}

	MSG_S2C::stSystemNotifyTopMove MsgNotify;
	StorageContainerIterator<stSystemNotify>* notify_itr = SystemNotifyStorage.MakeIterator();
	while(!notify_itr->AtEnd())
	{
		stSystemNotify* notify = notify_itr->Get();
		MSG_S2C::stSystemNotifyTopMove::stNotify data;
		data.id = notify->id;
		data.ntype = notify->ntype;
		data.tips = notify->tips;
		MsgNotify.vNotify.push_back(data);
		if(!notify_itr->Inc())
			break;
	}
	notify_itr->Destruct();
	GetSession()->SendPacket(MsgNotify);

	SendTitleList();

	m_entergametime = (uint32)UNIXTIME;

	sEventMgr.AddEvent(this, &Player::ApplyAllEquipments, EVENT_PLAYER_LOGIN, 1, 1,0);

	BetaBonus();
}

std::map<uint32, uint32> Player::s_bonus_acct;

void Player::BetaBonus()
{
	return;

	/*
	if( getLevel() >= 10 && getLevel() < 48 && GetSession() )
	{
		uint32 acct = GetSession()->GetAccountID();
		std::map<uint32, uint32>::iterator it = s_bonus_acct.find( acct );
		if( it != s_bonus_acct.end() )
		{
			return;
		}
		else
		{
			s_bonus_acct[acct] = 1;
			FILE* fp = fopen( "bonus.record", "a" );
			if( fp )
			{
				fprintf( fp, "%u\n", acct );
				fclose( fp );
			}
		}

		LevelInfo * Info = objmgr.GetLevelInfo( getRace(), getClass(), 48 );
		if(Info == 0)
		{
			return;
		}

		ApplyLevelInfo(Info, 48);
		ModUnsigned32Value( PLAYER_FIELD_COINAGE, 10000 * 10000 );
		GetSession()->SendNotification( "亲爱的玩家，非常感谢您参与本次测试，为感谢您对我们的支持，官方特举办“扶摇直上”活动，您将直接达到本次测试的最高等级(48级)，并得到官方赠送的10000金游戏币，祝您游戏愉快，谢谢！" );
		MyLog::yunyinglog->info( "gold-betabonus-player:[%u][%s] give gold:[10000]", GetLowGUID(), GetName() );
	}
	*/
}

void Player::OnWorldPortAck()
{
	//only rezz if player is porting to a instance portal
	MapInfo *pMapinfo = WorldMapInfoStorage.LookupEntry(GetMapId());
	if(isDead())
	{
		if(pMapinfo)
		{
			if(pMapinfo->type != INSTANCE_NULL)
				ResurrectPlayer();
		}
	}

	if(pMapinfo)
	{
		if(pMapinfo->HasFlag(WMI_INSTANCE_WELCOME) && GetMapMgr())
		{
			std::string welcome_msg;
			welcome_msg = "Welcome to ";
			welcome_msg += pMapinfo->name;
			welcome_msg += ". ";
            if(pMapinfo->type != INSTANCE_NONRAID && m_mapMgr->pInstance)
			{
				/*welcome_msg += "This instance is scheduled to reset on ";
				welcome_msg += asctime(localtime(&m_mapMgr->pInstance->m_expiration));*/
				welcome_msg += "This instance is scheduled to reset on ";
				welcome_msg += ConvertTimeStampToDataTime((uint32)m_mapMgr->pInstance->m_expiration);
			}
			sChatHandler.SystemMessage(m_session, welcome_msg.c_str());
		}
	}

	ResetHeartbeatCoords();
}

void Player::ModifyBonuses(uint32 type,int32 val)
{
	// Added some updateXXXX calls so when an item modifies a stat they get updated
	// also since this is used by auras now it will handle it for those
	switch (type)
		{
		case MANA:
			ModUnsigned32Value( UNIT_FIELD_MAXPOWER1, val );
			m_manafromitems += val;
			break;
		case HEALTH:
			ModUnsigned32Value( UNIT_FIELD_MAXHEALTH, val );
			m_healthfromitems += val;
			break;
		case AGILITY: // modify agility
			FlatStatModPos[STAT_AGILITY] += val;
			CalcStat( STAT_AGILITY );
			break;
		case STRENGTH: //modify strength
			FlatStatModPos[STAT_STRENGTH] += val;
			CalcStat( STAT_STRENGTH );
			break;
		case INTELLECT: //modify intellect
			FlatStatModPos[STAT_INTELLECT] += val;
			CalcStat( STAT_INTELLECT );
			break;
		 case SPIRIT: //modify spirit
			FlatStatModPos[STAT_SPIRIT] += val;
			CalcStat( STAT_SPIRIT );
			break;
		case STAMINA: //modify stamina
			FlatStatModPos[STAT_STAMINA] += val;
			CalcStat( STAT_STAMINA );
			break;
		case VITALITY:
			FlatStatModPos[STAT_VITALITY] += val;
			CalcStat( STAT_VITALITY );
			break;
		//case WEAPON_SKILL_RATING:
		//	{
		//		ModUnsigned32Value( PLAYER_RATING_MODIFIER_RANGED_SKILL, val ); // ranged
		//		ModUnsigned32Value( PLAYER_RATING_MODIFIER_MELEE_MAIN_HAND_SKILL, val ); // melee main hand
		//		ModUnsigned32Value( PLAYER_RATING_MODIFIER_MELEE_OFF_HAND_SKILL, val ); // melee off hand
		//	}break;
		case DEFENSE_RATING:
			{
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_DEFENCE, val );
			}break;
		case DODGE_RATING:
			{
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_DODGE, val );
			}break;
		case PARRY_RATING:
			{
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_PARRY, val );
			}break;
		case SHIELD_BLOCK_RATING:
			{
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_BLOCK, val );
			}break;
		case HIT_RATING:
			{
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_MELEE_HIT, val );//melee
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_RANGED_HIT, val );//ranged
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_SPELL_HIT, val);//spell
			}break;
		case CRITICAL_STRIKE_RATING:
			{
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_MELEE_CRIT, val );//melee
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_RANGED_CRIT, val );//ranged
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_SPELL_CRIT, val );//spell
			}break;
		case HASTE_RATING:
			{
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_MELEE_HASTE, val );//melee
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_RANGED_HASTE, val );//ranged
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_SPELL_HASTE, val );//spell
			}break;
		case  SPELL_DAMAGE:
			{
				m_spellDamageModifier += val;
				ModUnsigned32Value( UNIT_FIELD_SPELLDAMAGE, val );
			}
			break;
		//case HIT_AVOIDANCE_RATING:// this is guessed based on layout of other fields
		//	{
		//		ModUnsigned32Value( PLAYER_RATING_MODIFIER_MELEE_HIT_AVOIDANCE, val );//melee
		//		ModUnsigned32Value( PLAYER_RATING_MODIFIER_RANGED_HIT_AVOIDANCE, val );//ranged
		//		ModUnsigned32Value( PLAYER_RATING_MODIFIER_SPELL_HIT_AVOIDANCE, val );//spell
		//	}break;
		case EXPERTISE_RATING:
			{
				ModUnsigned32Value( PLAYER_RATING_MODIFIER_EXPERTISE, val );
				ModUnsigned32Value( PLAYER_EXPERTISE, val );
			}break;
		case NORMAL_RESISITANCE:
			{
				ModUnsigned32Value( UNIT_FIELD_RESISTANCES, val );
			}
			break;
		case HOLY_RESISITANCE:
			{
				ModUnsigned32Value( UNIT_FIELD_RESISTANCES_01, val );
			}
			break;
		case FIRE_RESISITANCE:
			{
				ModUnsigned32Value( UNIT_FIELD_RESISTANCES_02, val );
			}
			break;
		case NATURE_RESISITANCE:
			{
				ModUnsigned32Value( UNIT_FIELD_RESISTANCES_03, val );
			}
			break;
		case FROST_RESISITANCE:
			{
				ModUnsigned32Value( UNIT_FIELD_RESISTANCES_04, val );
			}
			break;
		case SHADOW_RESISITANCE:
			{
				ModUnsigned32Value( UNIT_FIELD_RESISTANCES_05, val );
			}
			break;
		case ARCANE_RESISITANCE:
			{
				ModUnsigned32Value( UNIT_FIELD_RESISTANCES_06, val );
			}
			break;
		default:
			MyLog::log->error( "Player::ModifierBonuses type:%u val:%d", type, val );
		}
}

bool Player::IsFlyMountSpell( uint32 spellid )
{
	if( (spellid >= 55005 && spellid <= 55008)
		|| spellid == 54006
		|| spellid == 54009
		|| spellid == 54011
		|| spellid == 55017
		|| spellid == 54012 )
		return true;
	else
		return false;
}

bool Player::IsFlyShiftSpell( uint32 spellid )
{
	if( spellid == 56307 )
		return true;
	else
		return false;
}

bool Player::IsInFlyMap()
{
	switch( GetMapId() )
	{
	case 10:
	case 20:
	case 21:
	case 24:
	case 25:
	case 6:
	case 53:
	case 7:
		return true;
	default:
		return false;
	}
}

bool Player::CanSignCharter(Charter * charter, Player * requester)
{
	if(charter->CharterType >= CHARTER_TYPE_ARENA_2V2 && m_arenaTeams[charter->CharterType-1] != NULL)
		return false;

	if(charter->CharterType == CHARTER_TYPE_GUILD && IsInGuild())
		return false;

	if(m_charters[charter->CharterType] || requester->GetTeam() != GetTeam())
		return false;
	else
		return true;
}

void Player::SaveAuras(stringstream &ss, bool periodic_save)
{
	// Add player auras
//	for(uint32 x=0;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
	for(uint32 x=0;x<MAX_AURAS;x++)
	{
		if(m_auras[x])
		{
			Aura *aur=m_auras[x];
			bool skip = false;

			if(aur->GetSpellId() >= 2001 && aur->GetSpellId() <= 2012 )
				skip = true;

			if( aur->GetSpellId() == m_castle_debuff_spell_id )
				continue;
			for(uint32 i = 0; i < 3; ++i)
			{
				if(/*aur->m_spellProto->Effect[i] == SPELL_EFFECT_APPLY_AREA_AURA ||*/
					aur->m_spellProto->Effect[i] == SPELL_EFFECT_ADD_FARSIGHT)
				{
					skip = true;
					break;
				}
			}

			if( aur->pSpellId )
				skip = true; //these auras were gained due to some proc. We do not save these eighter to avoid exploits of not removing them

			if ( aur->m_spellProto->c_is_flags & SPELL_FLAG_IS_EXPIREING_WITH_PET )
				skip = true;

			// skipped spells due to bugs
/*			why these should't be save??
			switch(aur->m_spellProto->Id)
			{
			case 12043: // Presence of mind
			case 11129: // Combustion
			case 28682: // Combustion proc
			case 16188: // Natures Swiftness
			case 17116: // Natures Swiftness
			case 34936: // Backlash
			case 35076: // Blessing of A'dal
			case 23333:	// WSG
			case 23335:	// WSG
			case 45438:	// Ice Block
			case 642:
			case 1020:	//divine shield

				skip = true;
				break;
			}*/

			//disabled proc spells until proper loading is fixed. Some spells tend to block or not remove when restored
			/* commented by Borland
			if(aur->GetSpellProto()->procFlags)
			{
//				MyLog::log->debug("skipping aura %d because has flags %d",aur->GetSpellId(),aur->GetSpellProto()->procFlags);
				skip = true;
			}
			*/
			//disabled proc spells until proper loading is fixed. We cannot recover the charges that were used up. Will implement later
			if(aur->GetSpellProto()->procCharges)
			{
//				MyLog::log->debug("skipping aura %d because has proccharges %d",aur->GetSpellId(),aur->GetSpellProto()->procCharges);
				skip = true;
			}
			//we are going to cast passive spells anyway on login so no need to save auras for them
            if(aur->IsPassive() && !(aur->GetSpellProto()->AttributesEx & 1024))
				skip = true;
			//shapeshift
//			if(m_ShapeShifted==aur->m_spellProto->Id)
//				skip=true;
			if( m_is_add_extra_spells )
			{
				for( int i = 0; i < m_extra_aruas[i]; ++i )
					if( aur->GetSpellId() == m_extra_aruas[i] )
					{
						skip = true;
						break;
					}

				if( m_ShapeShifted == aur->GetSpellId() )
					skip = true;
			}
			if( periodic_save && aur->GetSpellProto()->field114 & SPELL_AVOID_MASK_AURA_CANNOT_REMOVE_BYSELF )
				skip = true;

			if(skip)continue;
			uint32 d=aur->GetTimeLeft();
			if(d>3000)
				ss  << aur->GetSpellId() << "," << d << ",";
		}
	}
}

void Player::SetShapeShift(ui32 ss)
{
	//uint8 old_ss = GetByte( UNIT_FIELD_BYTES_2, 3 );
	//SetByte( UNIT_FIELD_BYTES_2, 3, ss );
	ui32 old_formdisplay = GetUInt32Value(UNIT_FIELD_FORMDISPLAYID);
	SetUInt32Value(UNIT_FIELD_FORMDISPLAYID, ss);

	MyLog::log->debug("[%s] shapeshift to [%d]", GetName(), ss);
/* commited by  matrol for player reform
	//remove auras that we should not have
	for( uint32 x = 0; x < MAX_AURAS + MAX_PASSIVE_AURAS; x++ )
	{
		if( m_auras[x] != NULL )
		{
			uint32 reqss = m_auras[x]->GetSpellProto()->RequiredShapeShift;
			if( reqss != 0 && m_auras[x]->IsPositive() )
			{
				if( old_ss > 0 )
				{
					if(  ( ((uint32)1 << (old_ss-1)) & reqss ) &&		// we were in the form that required it
						!( ((uint32)1 << (ss-1) & reqss) ) )			// new form doesnt have the right form
					{
						m_auras[x]->Remove();
						continue;
					}
				}
			}

			if( this->getClass() == CLASS_DRUID )
			{
				for (uint32 y = 0; y < 3; ++y )
				{
					switch( m_auras[x]->GetSpellProto()->EffectApplyAuraName[y])
					{
					case SPELL_AURA_MOD_ROOT: //Root
					case SPELL_AURA_MOD_DECREASE_SPEED: //Movement speed
					case SPELL_AURA_MOD_CONFUSE:  //Confuse (polymorph)
						{
							m_auras[x]->Remove();
						}
						break;
					default:
						break;
					}

					if( m_auras[x] == NULL )
						break;
				}
			}
		}
	}

	// apply any talents/spells we have that apply only in this form.
	set<uint32>::iterator itr;
	SpellEntry * sp;
	Spell * spe;
	SpellCastTargets t(GetGUID());

	for( itr = mSpells.begin(); itr != mSpells.end(); ++itr )
	{
		sp = dbcSpell.LookupEntry( *itr );
		if( sp->apply_on_shapeshift_change || sp->Attributes & 64 )		// passive/talent
		{
			if( sp->RequiredShapeShift && ((uint32)1 << (ss-1)) & sp->RequiredShapeShift )
			{
				spe = new Spell( this, sp, true, NULL );
				spe->prepare( &t );
			}
		}
	}

	// now dummy-handler stupid hacky fixed shapeshift spells (leader of the pack, etc)
	for( itr = mShapeShiftSpells.begin(); itr != mShapeShiftSpells.end(); ++itr )
	{
		sp = dbcSpell.LookupEntry( *itr );
		if( sp->RequiredShapeShift && ((uint32)1 << (ss-1)) & sp->RequiredShapeShift )
		{
			spe = new Spell( this, sp, true, NULL );
			spe->prepare( &t );
		}
	}
*/
	// kill speedhack detection for 2 seconds (not needed with new code but bleh)
	DelaySpeedHack( GetUInt32Value(UNIT_FIELD_BASEATTACKTIME) );

	UpdateStats();
	UpdateChances();
}

void Player::CalcDamage()
{
	Unit::CalcDamage();

	Player* pPlayer= (Player*)(this);
	float stat_damage = 0.f;//= GetFloatValue(UNIT_FIELD_STAT0 + STAT_STRENGTH) + .8f * GetFloatValue(UNIT_FIELD_STAT0 + STAT_AGILITY);
	float LongDisAttackPower = 2.f * getLevel() + 3.f * GetFloatValue(UNIT_FIELD_STAT0 + STAT_AGILITY);
	SetUInt32Value(UNIT_FIELD_RANGED_ATTACK_POWER, int(LongDisAttackPower));
	float AttackPower = GetFloatValue(UNIT_FIELD_STAT0 + STAT_STRENGTH) * 2.f + 0.5f * GetFloatValue(UNIT_FIELD_STAT0 + STAT_AGILITY) + getLevel();
	SetUInt32Value(UNIT_FIELD_ATTACK_POWER, int(AttackPower));

	float delta = (float)GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_POS ) - (float)GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_NEG );
	float modpct = 1.0f  + GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_PCT );

	if(BaseDamage[0] > BaseDamage[1])
	{
		std::swap(BaseDamage[0], BaseDamage[1]);
	}

	Item* weap = pPlayer->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_MAINHAND );
	SetFloatValue(UNIT_FIELD_MINRANGEDDAMAGE, (float(GetRAP()) / 20 * 2.8f + delta) * modpct);
	SetFloatValue(UNIT_FIELD_MAXRANGEDDAMAGE, (float(GetRAP()) / 20 * 2.8f + delta) * modpct);
	SetFloatValue(UNIT_FIELD_MINDAMAGE, (float(GetAP()) /14 * 3.3f  + delta) * modpct);
	SetFloatValue(UNIT_FIELD_MAXDAMAGE, (float(GetAP()) /14 * 3.3f  + delta) * modpct);	
	if (weap &&weap->GetProto()->Class == ITEM_CLASS_WEAPON)
	{
		switch (weap->GetProto()->SubClass)
		{
		case ITEM_SUBCLASS_WEAPON_BOW:
		case ITEM_SUBCLASS_WEAPON_GUN:
		case ITEM_SUBCLASS_WEAPON_CROSSBOW:
			{
				SetFloatValue(UNIT_FIELD_MINRANGEDDAMAGE, (float(GetRAP()) / 20 * 2.8f + BaseDamage[0] + delta) * modpct);
				SetFloatValue(UNIT_FIELD_MAXRANGEDDAMAGE, (float(GetRAP()) / 20 * 2.8f + BaseDamage[1] + delta) * modpct);
			}
			break;
		case ITEM_SUBCLASS_WEAPON_TWOHAND_AXE:
		case ITEM_SUBCLASS_WEAPON_TWOHAND_SWORD:
		case ITEM_SUBCLASS_WEAPON_TWOHAND_STAFF:
		case ITEM_SUBCLASS_WEAPON_TWOHAND_HAMMER:
		case ITEM_SUBCLASS_WEAPON_TWOBLADE:
			{
				SetFloatValue(UNIT_FIELD_MINDAMAGE, (float(GetAP()) /14 * 3.3f + BaseDamage[0] + delta) * modpct);
				SetFloatValue(UNIT_FIELD_MAXDAMAGE, (float(GetAP()) /14 * 3.3f + BaseDamage[1] + delta) * modpct);			
			}
			break;
		case ITEM_SUBCLASS_WEAPON_AXE:
		case ITEM_SUBCLASS_WEAPON_SWORD:
		case ITEM_SUBCLASS_WEAPON_STAFF:
		case ITEM_SUBCLASS_WEAPON_HAMMER:
		case ITEM_SUBCLASS_WEAPON_BAOZHU:
		case ITEM_SUBCLASS_WEAPON_KNIFE:
		case ITEM_SUBCLASS_WEAPON_BLADE:
			{
				SetFloatValue(UNIT_FIELD_MINDAMAGE, (float(GetAP()) /14 * 2.3f + BaseDamage[0] + delta) * modpct);
				SetFloatValue(UNIT_FIELD_MAXDAMAGE, (float(GetAP()) /14 * 2.3f + BaseDamage[1] + delta) * modpct);	
			}
			break;
		}
	}
	else
	{
		SetFloatValue(UNIT_FIELD_MINDAMAGE, (float(GetAP()) /14 * 1.0f + BaseDamage[0] + delta) * modpct);
		SetFloatValue(UNIT_FIELD_MAXDAMAGE, (float(GetAP()) /14 * 1.0f + BaseDamage[1] + delta) * modpct);	
	}


	//stat_damage = 2.5f*GetFloatValue(UNIT_FIELD_STAT0 + STAT_INTELLECT);
	stat_damage = GetFloatValue(UNIT_FIELD_STAT0 + STAT_INTELLECT) * 1.5f + getLevel();
	stat_damage = stat_damage + stat_damage*GetFloatValue(PLAYER_FIELD_MOD_DAMAGE_DONE_PCT);
	stat_damage += GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_POS ) - GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_NEG );
	stat_damage += stat_damage * static_cast< Player* >(this)->SpellDmgDoneByAttribute[STAT_INTELLECT][0];
	stat_damage += static_cast< Player* >(this)->m_spellDamageModifier;


	SetFloatValue(UNIT_FIELD_SPELLDAMAGE, stat_damage);

	/*float delta = (float)GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_POS ) - (float)GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_NEG );
	float r1 = BaseDamage[0] + GetAP() + delta;
	float r2 = BaseDamage[1] + GetAP() + delta;

	float modpct = GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_PCT );
	r1 *= modpct;
	r2 *= modpct;

	SetFloatValue( UNIT_FIELD_MINDAMAGE, r1 );
	SetFloatValue( UNIT_FIELD_MAXDAMAGE, r2 );

	Item* it = static_cast< Player* >( this )->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_OFFHAND);
	if(it)
	{
		r1 = BaseOffhandDamage[0] + GetAP() + delta;
		r2 = BaseOffhandDamage[1] + GetAP() + delta;

		r1 *= modpct;
		r2 *= modpct;

		SetFloatValue( UNIT_FIELD_MINOFFHANDDAMAGE, r1 );
		SetFloatValue( UNIT_FIELD_MAXOFFHANDDAMAGE, r2 );
	}
	return;*/



/*



	float delta;
	float r;
	int ss = GetShapeShift();
/////////////////MAIN HAND
		float ap_bonus = GetAP()/14000.0f;
		delta = (float)GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_POS ) - (float)GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_NEG );

		if(IsInFeralForm())
		{
			uint32 lev = getLevel();

			if(ss == FORM_CAT)
				r = lev + delta + ap_bonus * 1000.0f;
			else
				r = lev + delta + ap_bonus * 2500.0f;

			//SetFloatValue(UNIT_FIELD_MINDAMAGE,r);
			//SetFloatValue(UNIT_FIELD_MAXDAMAGE,r);

			r *= 0.9f;
			r *= 1.1f;

			SetFloatValue(UNIT_FIELD_MINDAMAGE,r>0?r:0);
			SetFloatValue(UNIT_FIELD_MAXDAMAGE,r>0?r:0);

			return;
		}
//////no druid ss
		uint32 speed=GetUInt32Value(UNIT_FIELD_BASEATTACKTIME);
		Item *it = GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_MAINHAND);

		if(!disarmed)
		{
			//speed = it->GetProto()->Delay;
			if(it)
			{
//				speed = 1-(F2/5)/1440;
			}
		}

//		float bonus=ap_bonus*speed;
		uint32 strength = GetUInt32Value( UNIT_FIELD_STAT0 + STAT_STRENGTH );
		float bonus = 6.4f * (float)strength / 5.f;
		float tmp = 1;
		map<uint32, WeaponModifier>::iterator i;
		for(i = damagedone.begin();i!=damagedone.end();i++)
		{
			if((i->second.wclass == (uint32)-1) || //any weapon
				(it && ((1 << it->GetProto()->SubClass) & i->second.subclass) )
				)
					tmp+=i->second.value/100.0f;
		}

		r = BaseDamage[0]+delta+bonus;
		r *= tmp;
		SetFloatValue(UNIT_FIELD_MINDAMAGE,r>0?r:0);

		r = BaseDamage[1]+delta+bonus;
		r *= tmp;
		SetFloatValue(UNIT_FIELD_MAXDAMAGE,r>0?r:0);

		uint32 cr = 0;
		if( it )
		{
			if( static_cast< Player* >( this )->m_wratings.size() )
			{
				std::map<uint32, uint32>::iterator itr = m_wratings.find( it->GetProto()->SubClass );
				if( itr != m_wratings.end() )
					cr=itr->second;
			}
		}
		SetUInt32Value( PLAYER_RATING_MODIFIER_MELEE_MAIN_HAND_SKILL, cr );
		/////////////// MAIN HAND END

		/////////////// OFF HAND START
		cr = 0;
		it = static_cast< Player* >( this )->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_OFFHAND);
		if(it)
		{
			if(!disarmed)
			{
				//speed =it->GetProto()->Delay;
			}
//			else speed  = 2000;

//			bonus = ap_bonus * speed;
			i = damagedone.begin();
			tmp = 1;
			for(;i!=damagedone.end();i++)
			{
				if((i->second.wclass==(uint32)-1) || //any weapon
					(( (1 << it->GetProto()->SubClass) & i->second.subclass)  )
					)
					tmp+=i->second.value/100.0f;
			}

			r = (BaseOffhandDamage[0]+delta+bonus)*offhand_dmg_mod;
			r *= tmp;
			SetFloatValue(UNIT_FIELD_MINOFFHANDDAMAGE,r>0?r:0);
			r = (BaseOffhandDamage[1]+delta+bonus)*offhand_dmg_mod;
			r *= tmp;
			SetFloatValue(UNIT_FIELD_MAXOFFHANDDAMAGE,r>0?r:0);
			if(m_wratings.size ())
			{
				std::map<uint32, uint32>::iterator itr=m_wratings.find(it->GetProto()->SubClass);
				if(itr!=m_wratings.end())
					cr=itr->second;
			}
		}
		SetUInt32Value( PLAYER_RATING_MODIFIER_MELEE_OFF_HAND_SKILL, cr );


		//tmp = 1;
		tmp = 0;
		for(i = damagedone.begin();i != damagedone.end();i++)
		if(i->second.wclass==(uint32)-1)  //any weapon
			tmp += i->second.value/100.0f;

		//display only modifiers for any weapon

		// OMG?
		ModFloatValue(PLAYER_FIELD_MOD_DAMAGE_DONE_PCT ,tmp);
 */
}

uint32 Player::GetMainMeleeDamage(uint32 AP_owerride)
{
	float min_dmg,max_dmg;
	float delta;
	float r;
	int ss = GetShapeShift();
/////////////////MAIN HAND
	float ap_bonus;
	if(AP_owerride)
		ap_bonus = AP_owerride/14000.0f;
	else
		ap_bonus = GetAP()/14000.0f;
	delta = (float)GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_POS ) - (float)GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_NEG );
	if(IsInFeralForm())
	{
		uint32 lev = getLevel();
		if(ss == FORM_CAT)
			r = lev + delta + ap_bonus * 1000.0f;
		else
			r = lev + delta + ap_bonus * 2500.0f;
		min_dmg = r * 0.9f;
		max_dmg = r * 1.1f;
		return float2int32(std::max((min_dmg + max_dmg)/2.0f,0.0f));
	}
//////no druid ss
	uint32 speed=GetUInt32Value(UNIT_FIELD_BASEATTACKTIME);
	Item *it = GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_MAINHAND);
	if(!disarmed)
	{
		if(it)
			speed = /*it->GetProto()->Delay*/GetUInt32Value(UNIT_FIELD_BASEATTACKTIME);
	}
	float bonus=ap_bonus*speed;
	float tmp = 1;
	map<uint32, WeaponModifier>::iterator i;
	for(i = damagedone.begin();i!=damagedone.end();i++)
	{
		if((i->second.wclass == (uint32)-1) || //any weapon
			(it && ((1 << it->GetProto()->SubClass) & i->second.subclass) )
			)
				tmp+=i->second.value/100.0f;
	}

	r = BaseDamage[0]+delta+bonus;
	r *= tmp;
	min_dmg = r * 0.9f;
	r = BaseDamage[1]+delta+bonus;
	r *= tmp;
	max_dmg = r * 1.1f;

	return float2int32(std::max((min_dmg + max_dmg)/2.0f,0.0f));
}

void Player::EventPortToGM(Player *p)
{
	SafeTeleport(p->GetMapId(),p->GetInstanceID(),p->GetPosition());
}

void Player::SendAreaTriggerMessage(const char * message, ...)
{
	va_list ap;
	va_start(ap, message);
	char msg[500];
	vsnprintf(msg, 500, message, ap);
	va_end(ap);

	MSG_S2C::stArea_Trigger_Message Msg;
	Msg.message = msg;
	m_session->SendPacket(Msg);
}

Player* Player::GetTradeTarget()
{
	if(!IsInWorld()) return 0;
	return m_mapMgr->GetPlayer((uint32)mTradeTarget);
}
void Player::removeSoulStone()
{
	if(!this->SoulStone) return;
	uint32 sSoulStone = 0;
	switch(this->SoulStone)
	{
	case 3026:
		{
			sSoulStone = 20707;
		}break;
	case 20758:
		{
			sSoulStone = 20762;
		}break;
	case 20759:
		{
			sSoulStone = 20763;
		}break;
	case 20760:
		{
			sSoulStone = 20764;
		}break;
	case 20761:
		{
			sSoulStone = 20765;
		}break;
	case 27240:
		{
			sSoulStone = 27239;
		}break;
	}
	this->RemoveAura(sSoulStone);
	this->SoulStone = this->SoulStoneReceiver = 0; //just incase
}

void Player::SoftDisconnect()
{
	sEventMgr.RemoveEvents(this, EVENT_PLAYER_SOFT_DISCONNECT);
	WorldSession *session=GetSession();
	session->Disconnect();
}

void Player::SetNoseLevel()
{
	// Set the height of the player
	switch (getRace())
	{
		case RACE_REN:
		// female
			if (getGender()) m_noseLevel = 1.72f;
			// male
			else m_noseLevel = 1.78f;
		break;
		case RACE_WU:
			if (getGender()) m_noseLevel = 1.82f;
			else m_noseLevel = 1.98f;
		break;
		case RACE_YAO:
		if (getGender()) m_noseLevel = 1.27f;
			else m_noseLevel = 1.4f;
		break;
	}
}

void Player::Possess(Unit * pTarget)
{
	return;
	if( m_CurrentCharm)
		return;

	m_CurrentCharm = pTarget;
	if(pTarget->GetTypeId() == TYPEID_UNIT)
	{
		// unit-only stuff.
		pTarget->setAItoUse(false);
		pTarget->GetAIInterface()->StopMovement(0);
		pTarget->m_redirectSpellPackets = this;
	}

	m_noInterrupt++;
	SetUInt64Value(UNIT_FIELD_CHARM, pTarget->GetGUID());
	SetUInt64Value(PLAYER_FARSIGHT, pTarget->GetGUID());
	ResetHeartbeatCoords();

	pTarget->SetUInt64Value(UNIT_FIELD_CHARMEDBY, GetGUID());
	pTarget->SetCharmTempVal(pTarget->GetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE));
	pTarget->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, GetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE));
	pTarget->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PLAYER_CONTROLLED_CREATURE);

	SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER);

	/* send "switch mover" packet */
	MSG_S2C::stAI_Swtich_Active_Mover Msg;
	Msg.target_guid = pTarget->GetNewGUID();
	Msg.flag = 1;
	m_session->SendPacket(Msg);

	/* update target faction set */
	pTarget->_setFaction();
	pTarget->UpdateOppFactionSet();

	list<uint32> avail_spells;
	for(list<AI_Spell*>::iterator itr = pTarget->GetAIInterface()->m_spells.begin(); itr != pTarget->GetAIInterface()->m_spells.end(); ++itr)
	{
		if((*itr)->agent == AGENT_SPELL)
			avail_spells.push_back((*itr)->spell->Id);
	}
	list<uint32>::iterator itr = avail_spells.begin();

	/* build + send pet_spells packet */
	if(pTarget->m_temp_summon)
		return;

	MSG_S2C::stPet_Spells MsgPetSpells;
	MsgPetSpells.target_guid = pTarget->GetGUID();
	// First spell is attack.
	//MsgPetSpells.vActionBar.push_back( uint32(PET_SPELL_ATTACK) );

	// Send the actionbar
	for(uint32 i = 1; i < 10; ++i)
	{
		MSG_S2C::stPet_Spells::stActionBar pet_actionbar;
		if(itr != avail_spells.end())
		{
			pet_actionbar.bar_spell.ActionBar = uint16((*itr));
			pet_actionbar.bar_spell.SpellState = uint16(DEFAULT_SPELL_STATE);
			MsgPetSpells.vActionBar.push_back(pet_actionbar);
			++itr;
		}
	}
	// Send the rest of the spells.
	for(itr = avail_spells.begin(); itr != avail_spells.end(); ++itr)
	{
		MSG_S2C::stPet_Spells::stSpell pet_spell;
		pet_spell.spell_id = uint16(*itr);
		pet_spell.spell_stat =uint16(DEFAULT_SPELL_STATE);
		MsgPetSpells.vSpells.push_back(pet_spell);
	}
	m_session->SendPacket(MsgPetSpells);
}

void Player::UnPossess()
{
	return;
	if( m_Summon || !m_CurrentCharm )
		return;

	Unit * pTarget = m_CurrentCharm;
	m_CurrentCharm = 0;

	if(pTarget->GetTypeId() == TYPEID_UNIT)
	{
		// unit-only stuff.
		pTarget->setAItoUse(true);
		pTarget->m_redirectSpellPackets = 0;
	}

	ResetHeartbeatCoords();

	m_noInterrupt--;
	SetUInt64Value(PLAYER_FARSIGHT, 0);
	SetUInt64Value(UNIT_FIELD_CHARM, 0);
	pTarget->SetUInt64Value(UNIT_FIELD_CHARMEDBY, 0);

	RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER);
	pTarget->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PLAYER_CONTROLLED_CREATURE);
	pTarget->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, pTarget->GetCharmTempVal());
	pTarget->_setFaction();
	pTarget->UpdateOppFactionSet();

	/* send "switch mover" packet */
	MSG_S2C::stAI_Swtich_Active_Mover Msg;
	Msg.target_guid = GetNewGUID();
	Msg.flag = 1;
	m_session->SendPacket(Msg);

	if(pTarget->m_temp_summon)
		return;

	MSG_S2C::stPet_Spells MsgPetSpells;
	MsgPetSpells.target_guid = 0;
	m_session->SendPacket(Msg);
}

//what is an Immobilize spell ? Have to add it later to spell effect handler
void Player::EventStunOrImmobilize(Unit *proc_target)
{
	if(trigger_on_stun)
	{
		if(trigger_on_stun_chance<100 && !Rand(trigger_on_stun_chance))
			return;
		SpellEntry *spellInfo = dbcSpell.LookupEntry(trigger_on_stun);
		if(!spellInfo)
			return;
		Spell *spell = new Spell(this, spellInfo ,true, NULL);
		SpellCastTargets targets;
/*		if(spellInfo->procFlags & PROC_TAGRGET_ATTACKER)
		{
			if(!attacker)
				return;
			targets.m_unitTarget = attacker->GetGUID();
		}
		else targets.m_unitTarget = GetGUID();
		*/
		if(proc_target)
			targets.m_unitTarget = proc_target->GetGUID();
		else targets.m_unitTarget = GetGUID();
		spell->prepare(&targets);
	}
}

void Player::SummonRequest( const char* Requestor, uint32 RequestorID, uint32 MapID, uint32 InstanceID, const LocationVector & Position )
{
	m_summonInstanceId = InstanceID;
	m_summonPos = Position;
	m_summoner = RequestorID;
	m_summonMapId = MapID;

	MSG_S2C::stSummon_Request Msg;
	Msg.Requestor = Requestor;
	Msg.mapid = MapID;
	Msg.duration = 60000;//1 minute
	m_session->SendPacket(Msg);
}

#ifdef CLUSTERING
void Player::EventRemoveAndDelete()
{

}
#endif

void Player::_AddSkillLine(uint32 SkillLine, uint32 Curr_sk, uint32 Max_sk)
{
	skilllineentry * CheckedSkill = dbcSkillLine.LookupEntry(SkillLine);
	if (CheckedSkill->type == 200)
	{
		return;
	}
	if (!CheckedSkill) //skill doesn't exist, exit here
		return;

	// force to be within limits
	Curr_sk = ( Curr_sk > 375 ? 375 : ( Curr_sk <1 ? 1 : Curr_sk ) );
	Max_sk = ( Max_sk > 375 ? 375 : Max_sk );

	ItemProf * prof;
	SkillMap::iterator itr = m_skills.find(SkillLine);
	if(itr != m_skills.end())
	{
		if( (Curr_sk > itr->second.CurrentValue && Max_sk >= itr->second.MaximumValue) || (Curr_sk == itr->second.CurrentValue && Max_sk > itr->second.MaximumValue) )
		{
			itr->second.CurrentValue = Curr_sk;
			itr->second.MaximumValue = Max_sk;
			_UpdateMaxSkillCounts();
		}
	}
	else
	{
		bool add = false;
		skilllineentry *sk=dbcSkillLine.LookupEntry(SkillLine);
		if (sk && sk->type == SKILL_TYPE_PROFESSION && GetUInt32Value(PLAYER_CHARACTER_POINTS2) < 2)
		{
			add = true;
			SetUInt32Value(PLAYER_CHARACTER_POINTS2,GetUInt32Value(PLAYER_CHARACTER_POINTS2)+ 1);
		}
		else if(sk && sk->type != SKILL_TYPE_PROFESSION)
		{
			add = true;
		}
		
		if (add)
		{
			PlayerSkill inf;
			inf.Skill = CheckedSkill;
			inf.MaximumValue = Max_sk;
			inf.CurrentValue = ( inf.Skill->id != SKILL_RIDING ? Curr_sk : Max_sk );
			inf.BonusValue = 0;
			m_skills.insert( make_pair( SkillLine, inf ) );
			_UpdateSkillFields();
		}



	}
	//Add to proficiency
	if((prof=(ItemProf *)GetProficiencyBySkill(SkillLine)))
	{
		MSG_S2C::stSet_Proficiency Msg;
		Msg.ItemClass = prof->itemclass;
		if(prof->itemclass==4)
		{
				armor_proficiency|=prof->subclass;
				//SendSetProficiency(prof->itemclass,armor_proficiency);
				Msg.Profinciency = armor_proficiency;
		}
		else
		{
				weapon_proficiency|=prof->subclass;
				//SendSetProficiency(prof->itemclass,weapon_proficiency);
				Msg.Profinciency = weapon_proficiency;
		}
		m_session->SendPacket( Msg );
	}

	// hackfix for poisons
	if(SkillLine==SKILL_POISONS && !HasSpell(2842))
		addSpell(2842);
}

void Player::_UpdateSkillFields()
{

	uint32 f = PLAYER_SKILL_INFO_1_1;
	/* Set the valid skills */
	for(SkillMap::iterator itr = m_skills.begin(); itr != m_skills.end();)
	{
		if(!itr->first)
		{
			SkillMap::iterator it2 = itr++;
			m_skills.erase(it2);
			continue;
		}

		ASSERT(f <= PLAYER_CHARACTER_POINTS1);
		if(itr->second.Skill->type == SKILL_TYPE_PROFESSION)
			SetUInt32Value(f++, itr->first | 0x10000);
		else
			SetUInt32Value(f++, itr->first);

		SetUInt32Value(f++, (itr->second.MaximumValue << 16) | itr->second.CurrentValue);
		SetUInt32Value(f++, itr->second.BonusValue);
		++itr;
	}

	/* Null out the rest of the fields */
	for(; f < PLAYER_CHARACTER_POINTS1; ++f)
	{
		if(m_uint32Values[f] != 0)
			SetUInt32Value(f, 0);
	}

}

bool Player::_HasSkillLine(uint32 SkillLine)
{
	return (m_skills.find(SkillLine) != m_skills.end());
}

void Player::_AdvanceSkillLine(uint32 SkillLine, uint32 Count /* = 1 */)
{
	SkillMap::iterator itr = m_skills.find(SkillLine);
	if(itr == m_skills.end())
	{
		/* Add it */
		_AddSkillLine(SkillLine, Count, getLevel() * 5);
		_UpdateMaxSkillCounts();
	}
	else
	{
		uint32 curr_sk = itr->second.CurrentValue;
		itr->second.CurrentValue = min(curr_sk + Count,itr->second.MaximumValue);
		if (itr->second.CurrentValue != curr_sk)
			_UpdateSkillFields();
	}
}

uint32 Player::_GetSkillLineMax(uint32 SkillLine)
{
	SkillMap::iterator itr = m_skills.find(SkillLine);
	return (itr == m_skills.end()) ? 0 : itr->second.MaximumValue;
}

uint32 Player::_GetSkillLineCurrent(uint32 SkillLine, bool IncludeBonus /* = true */)
{
	SkillMap::iterator itr = m_skills.find(SkillLine);
	if(itr == m_skills.end())
		return 0;

	return (IncludeBonus ? itr->second.CurrentValue + itr->second.BonusValue : itr->second.CurrentValue);
}

void Player::_RemoveSkillLine(uint32 SkillLine)
{
	SkillMap::iterator itr = m_skills.find(SkillLine);
	if(itr == m_skills.end())
		return;
	MSG_S2C::stSkill_Notify_Unlearn Msg;
	Msg.skill_id = SkillLine;
	m_session->SendPacket( Msg );
	m_skills.erase(itr);
	_UpdateSkillFields();
}

void Player::_UpdateMaxSkillCounts()
{
	bool dirty = false;
	uint32 new_max;
	for(SkillMap::iterator itr = m_skills.begin(); itr != m_skills.end(); ++itr)
	{
		if(itr->second.Skill->type == SKILL_TYPE_WEAPON || itr->second.Skill->id == SKILL_LOCKPICKING || itr->second.Skill->id == SKILL_POISONS)
		{
			new_max = 5 * getLevel();
		}
		else if (itr->second.Skill->type == SKILL_TYPE_LANGUAGE)
		{
			new_max = 300;
		}
		else if (itr->second.Skill->type == SKILL_TYPE_PROFESSION || itr->second.Skill->type == SKILL_TYPE_SECONDARY)
		{
			new_max = itr->second.MaximumValue;
			if (new_max >= 350)
				new_max = 375;
		}
		else
		{
			new_max = 1;
		}

		// force to be within limits
		if (new_max > 375)
			new_max = 375;
		if (new_max < 1)
			new_max = 1;


		if(itr->second.MaximumValue != new_max)
		{
			dirty = true;
			itr->second.MaximumValue = new_max;
		}
		if (itr->second.CurrentValue > new_max)
		{
			dirty = true;
			itr->second.CurrentValue = new_max;
		}
	}

	if(dirty)
		_UpdateSkillFields();
}

void Player::_ModifySkillBonus(uint32 SkillLine, int32 Delta)
{
	SkillMap::iterator itr = m_skills.find(SkillLine);
	if(itr == m_skills.end())
		return;

	itr->second.BonusValue += Delta;
	_UpdateSkillFields();
}

void Player::_ModifySkillBonusByType(uint32 SkillType, int32 Delta)
{
	bool dirty = false;
	for(SkillMap::iterator itr = m_skills.begin(); itr != m_skills.end(); ++itr)
	{
		if(itr->second.Skill->type == SkillType)
		{
			itr->second.BonusValue += Delta;
			dirty=true;
		}
	}

	if(dirty)
		_UpdateSkillFields();
}

/** Maybe this formula needs to be checked?
 * - Burlex
 */

float PlayerSkill::GetSkillUpChance()
{
	float diff = float(MaximumValue - CurrentValue);
	return (diff * 100.0f / float(MaximumValue)) / 3.0f;
}

void Player::_RemoveLanguages()
{
	for(SkillMap::iterator itr = m_skills.begin(), it2; itr != m_skills.end();)
	{
		if(itr->second.Skill->type == SKILL_TYPE_LANGUAGE)
		{
			it2 = itr++;
			m_skills.erase(it2);
		}
		else
			++itr;
	}
}

void PlayerSkill::Reset(uint32 Id)
{
	MaximumValue = 0;
	CurrentValue = 0;
	BonusValue = 0;
	Skill = (Id == 0) ? NULL : dbcSkillLine.LookupEntry(Id);
}

float Player::GetSkillUpChance(uint32 id)
{
	SkillMap::iterator itr = m_skills.find(id);
	if(itr == m_skills.end())
		return 0.0f;

	return itr->second.GetSkillUpChance();
}

void Player::_RemoveAllSkills()
{
	m_skills.clear();
	_UpdateSkillFields();
}

void Player::_AdvanceAllSkills(uint32 count)
{
	bool dirty=false;
	for(SkillMap::iterator itr = m_skills.begin(); itr != m_skills.end(); ++itr)
	{
		if(itr->second.CurrentValue != itr->second.MaximumValue)
		{
			itr->second.CurrentValue += count;
			if(itr->second.CurrentValue >= itr->second.MaximumValue)
				itr->second.CurrentValue = itr->second.MaximumValue;
			dirty=true;
		}
	}

	if(dirty)
		_UpdateSkillFields();
}

void Player::_ModifySkillMaximum(uint32 SkillLine, uint32 NewMax)
{
	// force to be within limits
	NewMax = ( NewMax > 375 ? 375 : NewMax );

	SkillMap::iterator itr = m_skills.find(SkillLine);
	if(itr == m_skills.end())
		return;

	if(NewMax > itr->second.MaximumValue)
	{
		if(SkillLine == SKILL_RIDING)
			itr->second.CurrentValue = NewMax;

		itr->second.MaximumValue = NewMax;
		_UpdateSkillFields();
	}
}

void Player::RemoveSpellTargets(uint32 Type)
{
	if(m_spellIndexTypeTargets[Type] != 0)
	{
		Unit * pUnit = m_mapMgr ? m_mapMgr->GetUnit(m_spellIndexTypeTargets[Type]) : NULL;
		if(pUnit)
            pUnit->RemoveAurasByBuffIndexType(Type, GetGUID());

		m_spellIndexTypeTargets[Type] = 0;
	}
}

void Player::RemoveSpellIndexReferences(uint32 Type)
{
	m_spellIndexTypeTargets[Type] = 0;
}

void Player::SetSpellTargetType(uint32 Type, Unit* target)
{
	m_spellIndexTypeTargets[Type] = target->GetGUID();
}

void Player::RecalculateHonor()
{
	HonorHandler::RecalculateHonorFields(this);
}

//wooot, crapy code rulez.....NOT
void Player::EventTalentHearthOfWildChange(bool apply)
{
	if(!hearth_of_wild_pct)
		return;

	//druid hearth of the wild should add more features based on form
	int tval;
	if(apply)
		tval = hearth_of_wild_pct;
	else tval = -hearth_of_wild_pct;

	uint32 SS=GetShapeShift();

	//increase stamina if :
	if(SS==FORM_BEAR || SS==FORM_DIREBEAR)
	{
		TotalStatModPctPos[STAT_STAMINA] += tval;
		CalcStat(STAT_STAMINA);
		UpdateStats();
		UpdateChances();
	}
	//increase attackpower if :
	else if(SS==FORM_CAT)
	{
		SetFloatValue(UNIT_FIELD_ATTACK_POWER_MULTIPLIER,GetFloatValue(UNIT_FIELD_ATTACK_POWER_MULTIPLIER)+float(tval/200.0f));
		SetFloatValue(UNIT_FIELD_RANGED_ATTACK_POWER_MULTIPLIER, GetFloatValue(UNIT_FIELD_RANGED_ATTACK_POWER_MULTIPLIER)+float(tval/200.0f));
		UpdateStats();
		UpdateChances();
	}
}

void Player::EventGroupFullUpdate()
{
	if(m_playerInfo->m_Group)
	{
		//m_playerInfo->m_Group->Update();
		m_playerInfo->m_Group->UpdateAllOutOfRangePlayersFor(this);
	}
}

void Player::EjectFromInstance()
{
	if( m_sunyou_instance )
	{
		if( !this->isAlive() )
 			ResurrectPlayerOnSite();
		if( m_bgEntryPointX && m_bgEntryPointY && m_bgEntryPointZ && m_bgEntryPointO && m_bgEntryPointMap )
		{
			MapInfo* info = WorldMapInfoStorage.LookupEntry( m_bgEntryPointMap );
			if( info && info->type == INSTANCE_NULL )
				if( SafeTeleport(m_bgEntryPointMap, 0, m_bgEntryPointX, m_bgEntryPointY, m_bgEntryPointZ, m_bgEntryPointO) )
					return;
		}

		SafeTeleport(m_bind_mapid, 0, m_bind_pos_x, m_bind_pos_y, m_bind_pos_z, 0);
	}
}

bool Player::HasQuestSpell(uint32 spellid) //Only for Cast Quests
{
	if (quest_spells.size()>0 && quest_spells.find(spellid) != quest_spells.end())
		return true;
	return false;
}
void Player::RemoveQuestSpell(uint32 spellid) //Only for Cast Quests
{
	if (quest_spells.size()>0)
		quest_spells.erase(spellid);
}

bool Player::HasQuestMob(uint32 entry) //Only for Kill Quests
{
	if (quest_mobs.size()>0 && quest_mobs.find(entry) != quest_mobs.end())
		return true;
	return false;
}

bool Player::HasQuest(uint32 entry)
{
	for(uint32 i=0;i<QUEST_MAX_COUNT;i++)
	{
		if ( m_questlog[i] && m_questlog[i]->GetQuest()->id == entry)
			return true;
	}
	return false;
}

void Player::RemoveQuestMob(uint32 entry) //Only for Kill Quests
{
// 	if (quest_mobs.size()>0)
// 		quest_mobs.erase(entry);
	std::map<uint32, uint32>::iterator it = quest_mobs.find( entry );
	if( it != quest_mobs.end() )
	{
		if( it->second <= 1 )
			quest_mobs.erase( it );
		else
			--it->second;
	}
}

PlayerInfo::PlayerInfo()
{
	guid = 0;
	acct = 0;
	name = NULL;
	race = 0;
	gender = 0;
	cl = 0;
	team = 0;

	lastOnline = 0;
	lastZone = 0;
	lastLevel = 0;
	m_Group = NULL;
	subGroup = 0;

	m_loggedInPlayer = NULL;
	guild = NULL;
	guildRank = NULL;
	guildMember = NULL;
	kill_count = NULL;
	be_kill_count = NULL;
	total_honor = NULL;
	honor_currency = NULL;

	total_coin = NULL;
	yuanbao = 0;

	for( int i = 0; i < 5; ++i )
	{
		m_arena_win[i] = 0;
		m_arena_lose[i] = 0;
		m_arena_win_lv80[i] = 0;
		m_arena_lose_lv80[i] = 0;
	}
}

PlayerInfo::~PlayerInfo()
{
	if(m_Group)
		m_Group->RemovePlayer(this);
}

void PlayerInfo::ResetAllInstance()
{
	/*
	for( std::set<uint32>::iterator it = instances.begin(); it != instances.end(); ++it )
	{
		Instance* p = sInstanceMgr.GetInstanceByID( *it );
		if( p )
		{
			p->expire = true;
		}
	}
	*/
	//instances.clear();
}

void Player::CopyAndSendDelayedPacket(PakHead& pak)
{
// 	WorldPacket * data2 = new WorldPacket(*data);
 	//delayedPackets.add(&pak);
	GetSession()->SendPacket( pak );
}

void Player::SendMeetingStoneQueue(uint32 DungeonId, uint8 Status)
{
// 	WorldPacket data(SMSG_MEETINGSTONE_SETQUEUE, 5);
// 	data << DungeonId << Status;
// 	m_session->SendPacket(Msg);
}

void Player::PartLFGChannel()
{
	Channel * pChannel = channelmgr.GetChannel("LookingForGroup", this);
	if( pChannel == NULL )
		return;

	/*for(list<Channel*>::iterator itr = m_channels.begin(); itr != m_channels.end(); ++itr)
	{
		if( (*itr) == pChannel )
		{
			pChannel->Part(this);
			return;
		}
	}*/
	if( m_channels.find( pChannel) == m_channels.end() )
		return;

	pChannel->Part( this );
}

uint32 Player::GetUnstabledPetNumber()
{
	if(m_Pets.size() == 0) return 0;
	std::map<uint32, PlayerPet*>::iterator itr = m_Pets.begin();
	for(;itr != m_Pets.end();itr++)
		if(itr->second->stablestate == STABLE_STATE_ACTIVE)
			return itr->first;
	return 0;
}
//if we charmed or simply summoned a pet, this function should get called
void Player::EventSummonPet( Pet *new_pet )
{
	if ( !new_pet )
		return ; //another wtf error

	SpellSet::iterator it,iter;
	for(iter= mSpells.begin();iter != mSpells.end();)
	{
		it = iter++;
		uint32 SpellID = *it;
		SpellEntry *spellInfo = dbcSpell.LookupEntry(SpellID);
		if( spellInfo->c_is_flags & SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER )
		{
			this->RemoveAllAuras( SpellID, this->GetGUID() ); //this is required since unit::addaura does not check for talent stacking
			SpellCastTargets targets( this->GetGUID() );
			Spell *spell = new Spell(this, spellInfo ,true, NULL);	//we cast it as a proc spell, maybe we should not !
			spell->prepare(&targets);
		}
		if( spellInfo->c_is_flags & SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET )
		{
			this->RemoveAllAuras( SpellID, this->GetGUID() ); //this is required since unit::addaura does not check for talent stacking
			SpellCastTargets targets( new_pet->GetGUID() );
			Spell *spell = new Spell(this, spellInfo ,true, NULL);	//we cast it as a proc spell, maybe we should not !
			spell->prepare(&targets);
		}
	}
	//there are talents that stop working after you gain pet
	for(uint32 x=0;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
		if(m_auras[x] && m_auras[x]->GetSpellProto()->c_is_flags & SPELL_FLAG_IS_EXPIREING_ON_PET)
			m_auras[x]->Remove();
	//pet should inherit some of the talents from caster
	//new_pet->InheritSMMods(); //not required yet. We cast full spell to have visual effect too
}

//if pet/charm died or whatever hapened we should call this function
//!! note function might get called multiple times :P
void Player::EventDismissPet()
{
	for(uint32 x=0;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
		if(m_auras[x] && m_auras[x]->GetSpellProto()->c_is_flags & SPELL_FLAG_IS_EXPIREING_WITH_PET)
			m_auras[x]->Remove();
}

#ifdef ENABLE_COMPRESSED_MOVEMENT

CMovementCompressorThread *MovementCompressor;

void Player::AppendMovementData(uint32 op, uint32 sz, const uint8* data)
{
	//printf("AppendMovementData(%u, %u, 0x%.8X)\n", op, sz, data);
	m_movementBuffer << uint8(sz + 2);
	m_movementBuffer << uint16(op);
	m_movementBuffer.append( data, sz );
}

bool CMovementCompressorThread::run()
{
	set<Player*>::iterator itr;
	while(running)
	{
		for(itr = m_players.begin(); itr != m_players.end(); ++itr)
		{
			(*itr)->EventDumpCompressedMovement();
		}
		Sleep(World::m_movementCompressInterval);
	}

	return true;
}

void CMovementCompressorThread::AddPlayer(Player * pPlayer)
{
	m_players.insert(pPlayer);
}

void CMovementCompressorThread::RemovePlayer(Player * pPlayer)
{
	m_players.erase(pPlayer);
}

void Player::EventDumpCompressedMovement()
{
	if( m_movementBuffer.size() == 0 )
		return;

	uint32 size = m_movementBuffer.size();
	uint32 destsize = size + size/10 + 16;
	int rate = World::m_movementCompressRate;
	if(size >= 40000 && rate < 6)
		rate = 6;
	if(size <= 100)
		rate = 0;			// don't bother compressing packet smaller than this, zlib doesnt really handle them well

	// set up stream
	z_stream stream;
	stream.zalloc = 0;
	stream.zfree  = 0;
	stream.opaque = 0;

	if(deflateInit(&stream, rate) != Z_OK)
	{
		MyLog::log->error("deflateInit failed.");
		return;
	}

	uint8 *buffer = new uint8[destsize];

	// set up stream pointers
	stream.next_out  = (Bytef*)buffer+4;
	stream.avail_out = destsize;
	stream.next_in   = (Bytef*)m_movementBuffer.contents();
	stream.avail_in  = size;

	// call the actual process
	if(deflate(&stream, Z_NO_FLUSH) != Z_OK ||
		stream.avail_in != 0)
	{
		MyLog::log->error("deflate failed.");
		delete [] buffer;
		return;
	}

	// finish the deflate
	if(deflate(&stream, Z_FINISH) != Z_STREAM_END)
	{
		MyLog::log->error("deflate failed: did not end stream");
		delete [] buffer;
		return;
	}

	// finish up
	if(deflateEnd(&stream) != Z_OK)
	{
		MyLog::log->error("deflateEnd failed.");
		delete [] buffer;
		return;
	}

	// fill in the full size of the compressed stream
#ifdef USING_BIG_ENDIAN
	*(uint32*)&buffer[0] = swap32(size);
#else
	*(uint32*)&buffer[0] = size;
#endif

	// send it
	m_session->OutPacket(763, (uint16)stream.total_out + 4, buffer);
	//printf("Compressed move compressed from %u bytes to %u bytes.\n", m_movementBuffer.size(), stream.total_out + 4);

	// cleanup memory
	delete [] buffer;
	m_movementBuffer.clear();
}
#endif

void Player::AddShapeShiftSpell(uint32 id)
{
	SpellEntry * sp = dbcSpell.LookupEntry( id );
	mShapeShiftSpells.insert( id );

	if( sp->RequiredShapeShift && ((uint32)1 << (GetShapeShift()-1)) & sp->RequiredShapeShift )
	{
		Spell * spe = new Spell( this, sp, true, NULL );
		SpellCastTargets t(this->GetGUID());
		spe->prepare( &t );
	}
}

void Player::RemoveShapeShiftSpell(uint32 id)
{
	mShapeShiftSpells.erase( id );
	RemoveAura( id );
}

// COOLDOWNS
void Player::_Cooldown_Add(uint32 Type, uint32 Misc, uint32 Time, uint32 SpellId, uint32 ItemId)
{
	SpellEntry* pSpell = dbcSpell.LookupEntry(Misc);
	if(!pSpell)
		return;
	PlayerCooldownMap::iterator itr = m_cooldownMap[Type].find( Misc );
	if( itr != m_cooldownMap[Type].end( ) )
	{
		if( itr->second.ExpireTime < Time )
		{
			itr->second.ExpireTime = Time;
			itr->second.ItemId = ItemId;
			itr->second.SpellId = SpellId;
		}
	}
	else
	{
		PlayerCooldown cd;
		cd.ExpireTime = Time;
		cd.ItemId = ItemId;
		cd.SpellId = SpellId;
		m_cooldownMap[Type].insert( make_pair( Misc, cd ) );
	}


	
	SpellGroupColdown::iterator itfind = m_mapSpellGroupColdown.find(pSpell->SpellGroupType);
	if(itfind == m_mapSpellGroupColdown.end())
	{
		m_mapSpellGroupColdown.insert(SpellGroupColdown::value_type(pSpell->SpellGroupType, Time));
	}
	else
	{

		itfind->second = Time;
	}

	//m_spellGroupCD[pSpell->SpellGroupType] = Time;

#ifdef _DEBUG
//	MyLog::log->debug("Cooldown : added cooldown for type %u misc %u time %u item %u spell %u", Type, Misc, Time - getMSTime(), ItemId, SpellId);
#endif
}

void Player::Cooldown_Add(SpellEntry * pSpell, Item * pItemCaster)
{
	uint32 mstime = getMSTime();
	int32 cool_time = 0;;

/*
	if( pSpell->CategoryRecoveryTime > 0 && pSpell->Category )
	{
		cool_time = pSpell->CategoryRecoveryTime;
		if( pSpell->SpellGroupType )
		{
			SM_FIValue(SM_FCooldownTime, &cool_time, pSpell->SpellGroupType);
			SM_PIValue(SM_PCooldownTime, &cool_time, pSpell->SpellGroupType);
		}

		_Cooldown_Add( COOLDOWN_TYPE_CATEGORY, pSpell->Category, mstime + cool_time, pSpell->Id, pItemCaster ? pItemCaster->GetProto()->ItemId : 0 );
	}
*/
	if(pSpell->Id == 5003)
	{
		cool_time = GetUInt32Value(UNIT_FIELD_RANGEDATTACKTIME);

	}
	else if (pSpell->Id == 1)
	{
		cool_time = GetUInt32Value(UNIT_FIELD_BASEATTACKTIME);

	}
	else
	if( pSpell->RecoveryTime > 0 )
	{
		cool_time = pSpell->RecoveryTime;
		if( pSpell->SpellGroupType )
		{
			SM_FIValue(SM_FCooldownTime, &cool_time, pSpell->SpellGroupType);
			SM_PIValue(SM_PCooldownTime, &cool_time, pSpell->SpellGroupType);
		}

		if(m_increasespellcdt.find(pSpell->Id) != m_increasespellcdt.end())
		{
			cool_time -= m_increasespellcdt[pSpell->Id];
		}

	}

	if (cool_time != 0)
	{

		_Cooldown_Add( COOLDOWN_TYPE_SPELL, pSpell->Id, mstime + cool_time, pSpell->Id, pItemCaster ? pItemCaster->GetProto()->ItemId : 0 );
	}
}

void Player::Cooldown_AddStart(SpellEntry * pSpell)
{
	if( pSpell->StartRecoveryTime == 0 )
		return;

	uint32 mstime = getMSTime();
	int32 atime = /*(int32)(*/float(pSpell->StartRecoveryTime) /** m_floatValues[UNIT_MOD_CAST_SPEED])*/;
	if( atime <= 0 )
		return;
 	if( atime >= 1500 )
		atime = 1500; // global cooldown is decreased by spell haste, but it's not INCREASED by spell slow.

	//if( pSpell->StartRecoveryCategory )		// if we have a different cool category to the actual spell category - only used by few spells
	//	_Cooldown_Add( COOLDOWN_TYPE_CATEGORY, pSpell->StartRecoveryCategory, mstime + atime, pSpell->Id, 0 );
	/*else if( pSpell->Category )				// cooldowns are grouped
		_Cooldown_Add( COOLDOWN_TYPE_CATEGORY, pSpell->Category, mstime + pSpell->StartRecoveryTime, pSpell->Id, 0 );*/
	//else									// no category, so it's a gcd
	{
#ifdef _DEBUG
		//MyLog::log->debug("Cooldown : Global cooldown adding: %u ms", atime);
#endif

		m_globalCooldown = mstime + atime;
 		if(pSpell->Id == 5003)
		{
			m_globalCooldown = mstime + GetUInt32Value(UNIT_FIELD_RANGEDATTACKTIME);
		}
		else if (pSpell->Id == 1)
		{
			m_globalCooldown = mstime + GetUInt32Value(UNIT_FIELD_BASEATTACKTIME);
		}
	}
}

bool Player::Cooldown_CanCast(SpellEntry * pSpell)
{
	PlayerCooldownMap::iterator itr;
	uint32 mstime = getMSTime();
/*
	if( pSpell->Category )
	{
		itr = m_cooldownMap[COOLDOWN_TYPE_CATEGORY].find( pSpell->Category );
		if( itr != m_cooldownMap[COOLDOWN_TYPE_CATEGORY].end( ) )
		{
			if( mstime < itr->second.ExpireTime )
				return false;
			else
				m_cooldownMap[COOLDOWN_TYPE_CATEGORY].erase( itr );
		}
	}
*/
	itr = m_cooldownMap[COOLDOWN_TYPE_SPELL].find( pSpell->Id );
	if( itr != m_cooldownMap[COOLDOWN_TYPE_SPELL].end( ) )
	{
		if( mstime < itr->second.ExpireTime )
			return false;
		else
			m_cooldownMap[COOLDOWN_TYPE_SPELL].erase( itr );
	}

	if( pSpell->StartRecoveryTime && m_globalCooldown )			/* gcd doesn't affect spells without a cooldown it seems */
	{
		if( mstime < m_globalCooldown )
			return false;
		else
			m_globalCooldown = 0;
	}

	if (pSpell->SpellGroupType >= 1024)
	{
		return true;
	}

/*

	if(pSpell->SpellGroupType && m_spellGroupCD[pSpell->SpellGroupType] > mstime)
		return false;*/

	if (pSpell->SpellGroupType)
	{
		SpellGroupColdown::iterator itfind = m_mapSpellGroupColdown.find(pSpell->SpellGroupType);
		if (itfind != m_mapSpellGroupColdown.end())
		{
			if (itfind->second > mstime)
			{
				return false;
			}
		}
	}


	return true;
}

void Player::Cooldown_AddItem(ItemPrototype * pProto, uint32 x)
{
	if( pProto->Spells[x].CategoryCooldown <= 0 && pProto->Spells[x].Cooldown <= 0 )
		return;

	ItemSpell * isp = &pProto->Spells[x];
	uint32 mstime = getMSTime();

	if( isp->CategoryCooldown > 0)
		_Cooldown_Add( COOLDOWN_TYPE_CATEGORY, isp->Category, isp->CategoryCooldown + mstime, isp->Id, pProto->ItemId );

	if( isp->Cooldown > 0 )
		_Cooldown_Add( COOLDOWN_TYPE_SPELL, isp->Id, isp->Cooldown + mstime, isp->Id, pProto->ItemId );
}

bool Player::Cooldown_CanCast(ItemPrototype * pProto, uint32 x)
{
	PlayerCooldownMap::iterator itr;
	ItemSpell * isp = &pProto->Spells[x];
	uint32 mstime = getMSTime();
//
// 	if( isp->Category )
// 	{
// 		itr = m_cooldownMap[COOLDOWN_TYPE_CATEGORY].find( isp->Category );
// 		if( itr != m_cooldownMap[COOLDOWN_TYPE_CATEGORY].end( ) )
// 		{
// 			if( mstime < itr->second.ExpireTime )
// 				return false;
// 			else
// 				m_cooldownMap[COOLDOWN_TYPE_CATEGORY].erase( itr );
// 		}
// 	}

	itr = m_cooldownMap[COOLDOWN_TYPE_SPELL].find( isp->Id );
	if( itr != m_cooldownMap[COOLDOWN_TYPE_SPELL].end( ) )
	{
		if( mstime < itr->second.ExpireTime )
			return false;
		else
			m_cooldownMap[COOLDOWN_TYPE_SPELL].erase( itr );
	}

	SpellEntry* pSpell = dbcSpell.LookupEntry(isp->Id);
	if(!pSpell)
		return false;

	if (pSpell->SpellGroupType)
	{
		SpellGroupColdown::iterator itfind = m_mapSpellGroupColdown.find(pSpell->SpellGroupType);
		if (itfind != m_mapSpellGroupColdown.end())
		{
			if (itfind->second > mstime)
			{
				return false;
			}
		}
	}

//
	return true;
}

#define COOLDOWN_SKIP_SAVE_IF_MS_LESS_THAN 10000

void Player::_SavePlayerCooldowns(QueryBuffer * buf)
{
	PlayerCooldownMap::iterator itr;
	PlayerCooldownMap::iterator itr2;
	uint32 i;
	uint32 seconds;
	uint32 mstime = getMSTime();

	// clear them (this should be replaced with an update queue later)
	sWorld.ExecuteSqlToDBServer("DELETE FROM playercooldowns WHERE player_guid = %u", m_uint32Values[OBJECT_FIELD_GUID] );		// 0 is guid always

	for( i = 0; i < NUM_COOLDOWN_TYPES; ++i )
	{
		itr = m_cooldownMap[i].begin( );
		for( ; itr != m_cooldownMap[i].end( ); )
		{
			itr2 = itr++;

			// expired ones - no point saving, nor keeping them around, wipe em
			if( mstime >= itr2->second.ExpireTime )
			{
				m_cooldownMap[i].erase( itr2 );
				continue;
			}

			// skip small cooldowns which will end up expiring by the time we log in anyway
			if( ( itr2->second.ExpireTime - mstime ) < COOLDOWN_SKIP_SAVE_IF_MS_LESS_THAN )
				continue;

			// work out the cooldown expire time in unix timestamp format
			// burlex's reason: 30 day overflow of 32bit integer, also
			// under windows we use GetTickCount() which is the system uptime, if we reboot
			// the server all these timestamps will appear to be messed up.

			seconds = (itr2->second.ExpireTime - mstime) / 1000;
			// this shouldnt ever be nonzero because of our check before, so no check needed

			if( buf != NULL )
			{
				buf->AddQuery( "INSERT INTO playercooldowns VALUES(%u, %u, %u, %u, %u, %u)", m_uint32Values[OBJECT_FIELD_GUID],
					i, itr2->first, seconds + (uint32)UNIXTIME, itr2->second.SpellId, itr2->second.ItemId );
			}
		}
	}
}

void Player::_SaveRefineItemContainer( QueryBuffer* buf )
{
	m_JingLianItemInterface->SaveToDB( buf );
}

void Player::_LoadPlayerCooldowns(QueryResult * result)
{
	if( result == NULL )
		return;

	// we should only really call getMSTime() once to avoid user->system transitions, plus
	// the cost of calling a function for every cooldown the player has
	uint32 mstime = getMSTime();
	uint32 type;
	uint32 misc;
	uint32 rtime;
	uint32 realtime;
	uint32 itemid;
	uint32 spellid;
	PlayerCooldown cd;

	do
	{
		type = result->Fetch()[0].GetUInt32();
		misc = result->Fetch()[1].GetUInt32();
		rtime = result->Fetch()[2].GetUInt32();
		spellid = result->Fetch()[3].GetUInt32();
		itemid = result->Fetch()[4].GetUInt32();

		if( type >= NUM_COOLDOWN_TYPES )
			continue;

		// remember the cooldowns were saved in unix timestamp format for the reasons outlined above,
		// so restore them back to mstime upon loading

		if( (uint32)UNIXTIME > rtime )
			continue;

		rtime -= (uint32)UNIXTIME;

		if( rtime < 10 )
			continue;

		realtime = mstime + ( ( rtime ) * 1000 );

		// apply it back into cooldown map
		cd.ExpireTime = realtime;
		cd.ItemId = itemid;
		cd.SpellId = spellid;
		m_cooldownMap[type].insert( make_pair( misc, cd ) );

	} while ( result->NextRow( ) );
}

#ifdef COLLISION
void Player::_FlyhackCheck()
{
	if(!sWorld.antihack_flight || m_TransporterGUID != 0 || GetTaxiState() || (sWorld.no_antihack_on_gm && GetSession()->HasGMPermissions()))
		return;

	MovementInfo * mi = GetSession()->GetMovementInfo();
	if(!mi) return; //wtf?

	// Falling, CCs, etc. All stuff that could potentially trap a player in mid-air.
	if(!(mi->flags & MOVEFLAG_FALLING) && !(mi->flags & MOVEFLAG_SWIMMING) && !(mi->flags & MOVEFLAG_LEVITATE)&&
		!(m_special_state & UNIT_STATE_CHARM || m_special_state & UNIT_STATE_FEAR || m_special_state & UNIT_STATE_ROOT || m_special_state & UNIT_STATE_STUN || m_special_state & UNIT_STATE_POLYMORPH || m_special_state & UNIT_STATE_CONFUSE || m_special_state & UNIT_STATE_FROZEN)
		&& !flying_aura && !FlyCheat)
	{
		float t_height = CollideInterface.GetHeight(GetMapMgr()->GetBaseMap(), GetPositionX(), GetPositionY(), GetPositionZ() + 2.0f);
		if(t_height == 99999.0f || t_height == NO_WMO_HEIGHT )
			t_height = GetMapMgr()->GetLandHeight(GetPositionX(), GetPositionY());
			if(t_height == 99999.0f || t_height == 0.0f) // Can't rely on anyone these days...
				return;

		float p_height = GetPositionZ();

		int32 diff = p_height - t_height;
		if(diff < 0)
			diff = -diff;

		if(t_height != p_height && (uint32)diff > sWorld.flyhack_threshold)
		{
			// Fly hax!
			EventTeleport(GetMapId(), GetPositionX(), GetPositionY(), t_height + 2.0f); // relog fix.
			//sCheatLog.writefromsession(GetSession(), "Caught fly hacking on map %u hovering %u over the terrain.", GetMapId(), diff);
			GetSession()->Disconnect();
		}
	}
}
#endif

void Player::_SpeedhackCheck(uint32 mstime)
{
	if( sWorld.antihack_speed && !GetTaxiState() && m_isMoving )
	{
		if( ( sWorld.no_antihack_on_gm && GetSession()->HasGMPermissions() ) )
			return; // do not check GMs speed been the config tells us not to.
		/*if( m_position == _lastHeartbeatPosition && m_isMoving )
		{
			// this means the client is probably lagging. don't update the timestamp, don't do anything until we start to receive
			// packets again (give the poor laggers a chance to catch up)
			return;
		}*/

		// simplified; just take the fastest speed. less chance of fuckups too
		float speed = ( flying_aura ) ? m_flySpeed : ( m_swimSpeed > m_runSpeed ) ? m_swimSpeed : m_runSpeed;
		if( speed != _lastHeartbeatV )
		{
			if( m_isMoving )
				_startMoveTime = mstime;
			else
				_startMoveTime = 0;

			_lastHeartbeatPosition = m_position;
			_lastHeartbeatV = speed;
			return;
		}

		if( !_heartbeatDisable && !m_uint32Values[UNIT_FIELD_CHARM] && m_TransporterGUID == 0 && !_speedChangeInProgress )
		{
			// latency compensation a little
			speed += 0.25f;

			float distance = m_position.Distance2D( _lastHeartbeatPosition );
			uint32 time_diff = mstime - _startMoveTime;
			uint32 move_time = float2int32( ( distance / ( speed * 0.001f ) ) );
			int32 difference = time_diff - move_time;
#ifdef _DEBUG
//			MyLog::log->debug("speed: %f diff: %i dist: %f move: %u tdiff: %u\n", speed, difference, distance, move_time, time_diff );
#endif
			if( difference < World::m_speedHackThreshold )
			{
				if( m_speedhackChances == 1 )
				{
					SetMovement( MOVE_ROOT, 1 );
					BroadcastMessage( "You have used all your speedhacking chances. You will be logged out in 7 seconds. Debug data: " );
					BroadcastMessage( "speed: %f diff: %i dist: %f move: %u tdiff: %u\n", speed, difference, distance, move_time, time_diff );
					sEventMgr.AddEvent( this, &Player::_Kick, EVENT_PLAYER_KICK, 7000, 1, 0 );
					m_speedhackChances = 0;
				}
				else if( m_speedhackChances > 1 )
				{
					BroadcastMessage( "Speedhack warning, you have %u chances left.", --m_speedhackChances );
				}
			}
		}
	}
}

void Player::ResetSpeedHack()
{
	ResetHeartbeatCoords();
	_heartbeatDisable = 0;
}
void Player::RemoteRevive()
{
	ResurrectPlayer();
	SetMovement(MOVE_UNROOT, 5);
	SetPlayerSpeed(RUN, (float)7);
	SetPlayerSpeed(SWIM, (float)4.9);
	SetMovement(MOVE_LAND_WALK, 8);
	SetUInt32Value(UNIT_FIELD_HEALTH, GetUInt32Value(UNIT_FIELD_MAXHEALTH) );
}

void Player::DelaySpeedHack(uint32 ms)
{
	uint32 t;
	_heartbeatDisable = 1;

	if( event_GetTimeLeft( EVENT_PLAYER_RESET_HEARTBEAT, &t ) )
	{
		if( t > ms )		// dont override a slower reset
			return;

		// override it
		event_ModifyTimeAndTimeLeft( EVENT_PLAYER_RESET_HEARTBEAT, ms );
		return;
	}

	// add a new event
	sEventMgr.AddEvent( this, &Player::ResetSpeedHack, EVENT_PLAYER_RESET_HEARTBEAT, ms, 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT );
}

/************************************************************************/
/* SOCIAL                                                               */
/************************************************************************/

void Player::Social_AddFriend(const char * name, const char * note)
{
	MSG_S2C::stFriend_Stat Msg;
	map<uint32, std::pair<char*, char*> >::iterator itr;
	PlayerInfo * info;
	//Player * pTarget;

	// lookup the player
	info = objmgr.GetPlayerInfoByName(name);
	if( info == NULL )
	{
		Msg.friend_result = FRIEND_NOT_FOUND;
		m_session->SendPacket(Msg);
		return;
	}

	// gm check - commented until i come up with a cleaner way of doing this, -comp.
	/*pTarget = m_session->GetPlayer()->GetMapMgr()->GetPlayer(info->guid);
	if( pTarget->GetSession()->HasGMPermissions() && !m_session->HasGMPermissions() && !sWorld.allow_gm_friends)
	{
		data << uint8(FRIEND_NOT_FOUND);
		m_session->SendPacket(Msg);
		return;
	}*/

	/*
	// team check
	if( info->team != m_playerInfo->team )
	{
		Msg.friend_result = FRIEND_ENEMY;
		Msg.guid = info->guid;
		m_session->SendPacket(Msg);
		return;
	}
	*/

	// are we ourselves?
	if( info == m_playerInfo )
	{
		Msg.friend_result = FRIEND_SELF;
		Msg.guid = GetGUID();
		m_session->SendPacket(Msg);
		return;
	}

	itr = m_friends.find(info->guid);
	if( itr != m_friends.end() )
	{
		Msg.friend_result = FRIEND_ALREADY;
		Msg.guid = info->guid;
		m_session->SendPacket(Msg);
		return;
	}

	if( info->m_loggedInPlayer != NULL )
	{
		Msg.friend_result = FRIEND_ADDED_ONLINE;
		Msg.guid = info->guid;
		if( note != NULL )
			Msg.note = note;

		Msg.bOnline = 1;
		Msg.level = info->lastLevel;
		Msg.Class = info->cl;

		info->m_loggedInPlayer->m_hasFriendList.insert( GetLowGUID() );

		if( note != NULL )
			m_friends.insert( make_pair(info->guid, std::make_pair( strdup( name ), strdup(note) ) ) );
		else
			m_friends.insert( make_pair(info->guid, std::make_pair( (char*)NULL, (char*)NULL ) ) );

		m_session->SendPacket(Msg);

		// dump into the db
		sWorld.ExecuteSqlToDBServer("INSERT INTO social_friends VALUES(%u, %u, \"%s\", \"%s\")",
			GetLowGUID(), info->guid, CharacterDatabase.EscapeString(string(name)).c_str(), note ? CharacterDatabase.EscapeString(string(note)).c_str() : "");
	}
	else
	{
		Msg.friend_result= FRIEND_ADDED_OFFLINE;
		Msg.guid = info->guid;
	}
}

void Player::Social_RemoveFriend(uint32 guid)
{
	MSG_S2C::stFriend_Stat Msg;
	map<uint32, std::pair<char*, char*> >::iterator itr;

	// are we ourselves?
	if( guid == GetLowGUID() )
	{
		Msg.friend_result = FRIEND_SELF;
		Msg.guid = GetGUID();
		m_session->SendPacket(Msg);
		return;
	}

	itr = m_friends.find(guid);
	if( itr != m_friends.end() )
	{
		if( itr->second.first )
			free(itr->second.first);
		if( itr->second.second )
			free(itr->second.second);


		m_friends.erase(itr);
	}

	Msg.friend_result = FRIEND_REMOVED;
	Msg.guid = guid;


	Player * pl = objmgr.GetPlayer( (uint32)guid );
	if( pl != NULL )
	{
		pl->m_hasFriendList.erase( GetLowGUID() );
	}

	m_session->SendPacket(Msg);

	// remove from the db
	sWorld.ExecuteSqlToDBServer("DELETE FROM social_friends WHERE character_guid = %u AND friend_guid = %u",
		GetLowGUID(), (uint32)guid);
}

void Player::Social_SetNote(uint32 guid, const char * note)
{
	map<uint32, std::pair<char*, char*> >::iterator itr;

	itr = m_friends.find(guid);

	if( itr == m_friends.end() )
	{
		return;
	}

	if( itr->second.second != NULL )
		free(itr->second.second);

	if( note != NULL )
		itr->second.second = strdup( note );
	else
		itr->second.second = NULL;

	sWorld.ExecuteSqlToDBServer("UPDATE social_friends SET note = \"%s\" WHERE character_guid = %u AND friend_guid = %u",
		note ? CharacterDatabase.EscapeString(string(note)).c_str() : "", GetLowGUID(), guid);
}

void Player::Social_AddIgnore(const char * name)
{
	MSG_S2C::stFriend_Stat Msg;
	set<uint32>::iterator itr;
	PlayerInfo * info;

	// lookup the player
	info = objmgr.GetPlayerInfoByName(name);
	if( info == NULL )
	{
		Msg.friend_result = FRIEND_IGNORE_NOT_FOUND;
		Msg.guid = 0;
		m_session->SendPacket(Msg);
		return;
	}

	// are we ourselves?
	if( info == m_playerInfo )
	{
		Msg.friend_result = FRIEND_IGNORE_SELF;
		Msg.guid = GetGUID();
		m_session->SendPacket(Msg);
		return;
	}

	itr = m_ignores.find(info->guid);
	if( itr != m_ignores.end() )
	{
		Msg.friend_result = FRIEND_IGNORE_ALREADY;
		Msg.guid = info->guid;
		m_session->SendPacket(Msg);
		return;
	}

	Msg.friend_result = FRIEND_IGNORE_ADDED;
	Msg.guid = info->guid;
	Msg.name = info->name;
	Msg.Class = info->cl;
	Msg.race = info->race;
	if (info->guild)
	{
		Msg.guildname = info->guild->GetGuildName();
	}

	m_ignores.insert( info->guid );

	m_session->SendPacket(Msg);

	// dump into db
	sWorld.ExecuteSqlToDBServer("INSERT INTO social_ignores VALUES(%u, %u)", GetLowGUID(), info->guid);
}

void Player::Social_RemoveIgnore(uint32 guid)
{
	MSG_S2C::stFriend_Stat Msg;
	set<uint32>::iterator itr;

	// are we ourselves?
	if( guid == GetLowGUID() )
	{
		Msg.friend_result = FRIEND_IGNORE_SELF;
		Msg.guid = GetGUID();
		m_session->SendPacket(Msg);
		return;
	}

	itr = m_ignores.find(guid);
	if( itr != m_ignores.end() )
	{
		m_ignores.erase(itr);
	}

	Msg.friend_result = FRIEND_IGNORE_REMOVED;
	Msg.guid = guid;


	m_session->SendPacket(Msg);

	// remove from the db
	sWorld.ExecuteSqlToDBServer("DELETE FROM social_ignores WHERE character_guid = %u AND ignore_guid = %u",
		GetLowGUID(), (uint32)guid);
}

bool Player::Social_IsIgnoring(PlayerInfo * m_info)
{
	bool res;
	if( m_ignores.find( m_info->guid ) == m_ignores.end() )
		res = false;
	else
		res = true;

	return res;
}

bool Player::Social_IsIgnoring(uint32 guid)
{
	bool res;
	if( m_ignores.find( guid ) == m_ignores.end() )
		res = false;
	else
		res = true;

	return res;
}

void Player::Social_TellFriendsOnline()
{
	if( m_hasFriendList.empty() )
		return;

	MSG_S2C::stFriend_Stat Msg;
	set<uint32>::iterator itr;
	Player * pl;
	Msg.friend_result = FRIEND_ONLINE;
	Msg.guid = GetGUID();
	Msg.level = getLevel();
	Msg.Class = getClass();

	for( itr = m_hasFriendList.begin(); itr != m_hasFriendList.end(); ++itr )
	{
		pl = objmgr.GetPlayer(*itr);
		if( pl != NULL )
			pl->GetSession()->SendPacket(Msg);
	}
}

void Player::Social_TellFriendsOffline()
{
	if( m_hasFriendList.empty() )
		return;

	MSG_S2C::stFriend_Stat Msg;

	set<uint32>::iterator itr;
	Player * pl;
	Msg.friend_result = FRIEND_OFFLINE;
	Msg.guid = GetGUID();

	for( itr = m_hasFriendList.begin(); itr != m_hasFriendList.end(); ++itr )
	{
		pl = objmgr.GetPlayer(*itr);
		if( pl != NULL )
			pl->GetSession()->SendPacket(Msg);
	}
}

void Player::Social_SendFriendList(uint32 flag)
{
	MSG_S2C::stFriend_List Msg;
	map<uint32, std::pair<char*, char*> >::iterator itr;
	set<uint32>::iterator itr2;
	Player * plr;


	Msg.flag = flag;
	for( itr = m_friends.begin(); itr != m_friends.end(); ++itr )
	{
		MSG_S2C::stFriend_List::stFriend Fri;
		// guid
		Fri.guid = uint64( itr->first );

		// friend/ignore flag.
		// 1 - friend
		// 2 - ignore
		// 3 - muted?
		Fri.flag = uint32( 1 );

		// player note
		if( itr->second.second != NULL )
			Fri.note = itr->second.second;
		if( itr->second.first != NULL )
			Fri.name = itr->second.first;
		else
			assert( 0 );
		// online/offline flag
		plr = objmgr.GetPlayer( itr->first );
		Fri.bOnline =false;
		if( plr != NULL )
		{
			Fri.bOnline = uint8( 1 );
			Fri.level = plr->getLevel();
			Fri.Class = plr->getClass();
		}
		Msg.vFriends.push_back( Fri );
	}

	for( itr2 = m_ignores.begin(); itr2 != m_ignores.end(); ++itr2 )
	{

		PlayerInfo* playerInfo = objmgr.GetPlayerInfo( uint64( (*itr2) )  );
		MSG_S2C::stFriend_List::stIgnores Ignores;
		Ignores.Class = playerInfo->cl;
		Ignores.guid = playerInfo->guid;
		Ignores.name = playerInfo->name;
		Ignores.Race = playerInfo->race;
		if (playerInfo->guild)
		{
			Ignores.GuildName = playerInfo->guild->GetGuildName();	
		}
		Msg.vIgnores.push_back( Ignores );
	}

	m_session->SendPacket(Msg);
}

void Player::VampiricSpell(uint32 dmg, Unit* pTarget)
{
	float fdmg = float(dmg);
	uint32 bonus;
	int32 perc;
	Group * pGroup = GetGroup();
	SubGroup * pSubGroup = (pGroup != NULL) ? pGroup->GetSubGroup(GetSubGroup()) : NULL;
	GroupMembersSet::iterator itr;

	if( ( !m_vampiricEmbrace && !m_vampiricTouch ) || getClass() != CLASS_PRIEST )
		return;

	if( m_vampiricEmbrace > 0 && pTarget->m_hasVampiricEmbrace > 0 && pTarget->HasAurasOfNameHashWithCaster(SPELL_HASH_VAMPIRIC_EMBRACE, this) )
	{
		perc = 15;

		bonus = float2int32(fdmg * (float(perc)/100.0f));
		if( bonus > 0 )
		{
			Heal(this, 15286, bonus);

			// loop party
			if( pSubGroup != NULL )
			{
				for( itr = pSubGroup->GetGroupMembersBegin(); itr != pSubGroup->GetGroupMembersEnd(); ++itr )
				{
					if( (*itr)->m_loggedInPlayer != NULL && (*itr) != m_playerInfo )
						Heal( (*itr)->m_loggedInPlayer, 15286, bonus );
				}
			}
		}
	}

	if( m_vampiricTouch > 0 && pTarget->m_hasVampiricTouch > 0 && pTarget->HasAurasOfNameHashWithCaster(SPELL_HASH_VAMPIRIC_TOUCH, this) )
	{
		perc = 5;
		//SM_FIValue(SM_FSPELL_VALUE, &perc, 4);

		bonus = float2int32(fdmg * (float(perc)/100.0f));
		if( bonus > 0 )
		{
			Energize(this, 34919, bonus, POWER_TYPE_MANA);

			// loop party
			if( pSubGroup != NULL )
			{
				for( itr = pSubGroup->GetGroupMembersBegin(); itr != pSubGroup->GetGroupMembersEnd(); ++itr )
				{
					if( (*itr)->m_loggedInPlayer != NULL && (*itr) != m_playerInfo && (*itr)->m_loggedInPlayer->GetPowerType() == POWER_TYPE_MANA )
						Energize((*itr)->m_loggedInPlayer, 34919, bonus, POWER_TYPE_MANA);
				}
			}
		}
	}
}


void Player::SendDelayedPacket(PakHead& packet, bool bDeleteOnSend)
{
	if(GetSession() != NULL) GetSession()->SendPacket(packet);
}

void Player::SwitchTrade( bool b )
{
	m_isForbidTrade = b;
}

void Player::SendForbidTradeMsg( WorldSession* s )
{
	if( s )
	{
		MSG_S2C::stTargetForbidTrade msg;
		msg.guid = GetLowGUID();
		s->SendPacket( msg );
	}
}

void Player::OnJoinSunyouInstance( SunyouInstance* si )
{
	if( si->IsValid() )
		si->OnPlayerJoin( this );
	else
		this->SetInstanceBusy( false );
}

void Player::OnLeaveSunyouInstance( SunyouInstance* si )
{
	if( si->IsValid() )
		si->OnPlayerLeave( this );
}

void Player::SendUpdateInstanceQueueMsg()
{
	MSG_S2C::stQueueInstanceUpdate msg;
	memcpy( msg.mapid, m_queuedmapid, sizeof( m_queuedmapid ) );
	m_session->SendPacket( msg );
}

void Player::SendEnterInstanceCountDownMsg( uint32 mapid, uint8 seconds )
{
	MSG_S2C::stEnterInstanceCountDown msg;
	msg.mapid = mapid;
	msg.seconds = seconds;
	m_session->SendPacket( msg );
}

void Player::RemoveQueueInstance( uint32 mapid )
{
	for( int i = 0; i < 3; ++i )
		if( m_queuedmapid[i] == mapid )
		{
			m_queuedmapid[i] = 0;
			break;
		}
	SendUpdateInstanceQueueMsg();
}

void Player::AddQueueInstance( uint32 mapid )
{
	for( int i = 0; i < 3; ++i )
		if( !m_queuedmapid[i] )
		{
			m_queuedmapid[i] = mapid;
			break;
		}
	SendUpdateInstanceQueueMsg();
}

bool Player::IsQueueInstanceFull() const
{
	for( int i = 0; i < 3; ++i )
		if( m_queuedmapid[i] == 0 )
			return false;
	return true;
}

bool Player::HasInstanceQueue( uint32 mapid ) const
{
	for( int i = 0; i < 3; ++i )
		if( m_queuedmapid[i] == mapid )
			return true;
	return false;
}

void Player::ResetQueueInstance()
{
	if( !m_queuedmapid[0] && !m_queuedmapid[1] && !m_queuedmapid[2] )
		return;

	memset( m_queuedmapid, 0, sizeof( m_queuedmapid ) );
	SendUpdateInstanceQueueMsg();
}

void Player::BeforeJump2InstanceSrv()
{
	if( !isAlive() )
		ResurrectPlayerOnSite();

	m_bgEntryPointX = GetPositionX();
	m_bgEntryPointY = GetPositionY();
	m_bgEntryPointZ = GetPositionZ();
	m_bgEntryPointO = GetOrientation();
	m_bgEntryPointInstance = GetInstanceID();
	m_bgEntryPointMap = m_mapId;
}

void Player::InstanceTeleport( uint32 mapid, uint32 instanceid, LocationVector* plv )
{
	//if( m_sunyou_instance )
	//	m_sunyou_instance->DecreasePlayerCount();
	SafeTeleport( mapid, instanceid, *plv );
}

void Player::IncreaseArenaKill()
{
	MapMgr* p = GetMapMgr();
	if( p && p->m_sunyouinstance && p->m_sunyouinstance->GetCategory() == INSTANCE_CATEGORY_ARENA )
	{
		m_arenakill++;
		MSG_S2C::stArenaUpdateState msg;
		MSG_S2C::stArenaUpdateState::info info;
		info.name = GetName();
		info.level = getLevel();
		info.race = getRace();
		info.cls = getClass();
		info.kill = m_arenakill;
		msg.vinfo.push_back( info );
		p->m_sunyouinstance->Broadcast( msg );
	}
}

void Player::SetArenaKill( uint32 n )
{
	m_arenakill = n;
}

void Player::SetRandomResurrectLocation( const std::vector<LocationVector*>& v )
{
	m_random_vrl = v;
}

void Player::ClearRandomResurrectLocation()
{
	m_random_vrl.clear();
}

void Player::PrepareTeleportToSunyouInstance( uint32 mapid, uint32 instanceid, LocationVector* plv )
{
	m_beforeTeleport2Instance = true;
	SetInstanceBusy( true );
	ResetQueueInstance();
	g_instancequeuemgr.RemovePlayer( this );
	//SendEnterInstanceCountDownMsg( mapid, 10 );
	//m_sunyou_instance->IncreasePlayerCount();
	InstanceTeleport( mapid, instanceid, plv );
	//sEventMgr.AddEvent( this, &Player::InstanceTeleport, mapid, instanceid, plv, EVENT_SUNYOU_INSTANCE_TELEPORT, 10000, 1, 0 );
}

void Player::AddExtraSpells( const uint32 extra_spells[10] )
{
	bool b = false;
	for( int i = 0; i < 10; ++i )
		if( extra_spells[i] )
		{
			b = true;
			break;
		}
	if( !b )
		return;

	MSG_S2C::stUpdateActionButtons msg;
	MSG_S2C::stUpdateActionButtons::stActionButton ab;

	memcpy( m_extra_spells, extra_spells, sizeof( m_extra_spells ) );

	for( int i = 0; i < PLAYER_ACTION_BUTTON_COUNT; ++i )
		m_oldactionbar[i] = mActions[i];

	for( int i = 0; i < 10; ++i )
	{
		uint32 entry = m_extra_spells[i];
		if( entry )
			addSpell( entry, false );

		mActions[i].Action = entry;
		mActions[i].Type = 0;
		mActions[i].Misc = 0;

		ab.pos = i;
		ab.Action = entry;
		ab.Type = 0;
		ab.Misc = 0;
		msg.v.push_back( ab );
	}
	mActions[10].Action = 0;
	mActions[10].Type = 0;
	mActions[10].Misc = 0;

	mActions[11].Action = 0;
	mActions[11].Type = 0;
	mActions[11].Misc = 0;

	GetSession()->SendPacket( msg );
	m_is_add_extra_spells = true;
}

void Player::AddExtraSpell( uint32 entry )
{
	if( !m_is_add_extra_spells )
		return;

	for( int i = 0; i < 10; ++i )
	{
		if( m_extra_spells[i] == 0 )
		{
			m_extra_spells[i] = entry;
			addSpell( entry, false );

			for( int j = 0; j < 10; ++j )
			{
				if( mActions[i].Action == 0 )
				{
					mActions[i].Action = entry;
					mActions[i].Type = 0;
					mActions[i].Misc = 0;
					MSG_S2C::stUpdateActionButtons msg;
					MSG_S2C::stUpdateActionButtons::stActionButton st;
					st.Action = entry;
					st.pos = i;
					st.Misc = 0;
					st.Type = 0;
					msg.v.push_back( st );
					GetSession()->SendPacket( msg );
					return;
				}
			}
			return;
		}
	}
}

void Player::RemoveExtraSpell( uint32 entry )
{
	if( !m_is_add_extra_spells )
		return;

	for( int i = 0; i < 10; ++i )
	{
		if( m_extra_spells[i] == entry )
		{
			m_extra_spells[i] = 0;
			for( int j = 0; j < 10; ++j )
			{
				if( mActions[j].Action == entry )
				{
					mActions[j].Action = 0;
					mActions[i].Type = 0;
					mActions[i].Misc = 0;
					MSG_S2C::stUpdateActionButtons msg;
					MSG_S2C::stUpdateActionButtons::stActionButton st;
					st.Action = 0;
					st.pos = i;
					st.Misc = 0;
					st.Type = 0;
					msg.v.push_back( st );
					GetSession()->SendPacket( msg );
				}
			}
			return;
		}
	}
}

void Player::RemoveAllExtraSpells()
{
	if( !m_is_add_extra_spells )
		return;

	for( int i = 0; i < 10; ++i )
	{
		if( m_extra_spells[i] && m_extra_spells[i] != 1)
		{
			removeSpell( m_extra_spells[i], false, false, 0 );
			m_extra_spells[i] = 0;
		}
	}
	for( int i = 0; i < PLAYER_ACTION_BUTTON_COUNT; ++i )
		mActions[i] = m_oldactionbar[i];

	SendInitialActions();
	m_is_add_extra_spells = false;
}

void Player::AddExtraAuras( const uint32 extra_auras[10] )
{
	memcpy( m_extra_aruas, extra_auras, sizeof( m_extra_aruas ) );
	for( int i = 0; i < 10; ++i )
	{
		if( m_extra_aruas[i] )
		{
			SpellEntry* proto = dbcSpell.LookupEntry( m_extra_aruas[i] );
			if( proto )
			{
				Spell * sp = new Spell( this, proto, true, 0 );
				SpellCastTargets targets( GetGUID() );
				sp->prepare(&targets);
			}
		}
	}
}

void Player::RemoveAllExtraAuars()
{
	for( int i = 0; i < 10; ++i )
		if( m_extra_aruas[i] )
		{
			RemoveAura( m_extra_aruas[i] );
			m_extra_aruas[i] = 0;
		}
}

void Player::RestoreAllExtraAuras()
{
	for( int i = 0; i < 10; ++i )
	{
		if( m_extra_aruas[i] )
		{
			SpellEntry* proto = dbcSpell.LookupEntry( m_extra_aruas[i] );
			if( proto )
			{
				Spell * sp = new Spell( this, proto, true, 0 );
				SpellCastTargets targets( GetGUID() );
				sp->prepare(&targets);
			}
		}
	}
}

void Player::LeaveSunyouInstanceProc()
{
	uint32 maxhealth = GetUInt32Value(UNIT_FIELD_MAXHEALTH);
	uint32 maxmana = GetUInt32Value(UNIT_FIELD_MAXPOWER1);
	SetUInt32Value( UNIT_FIELD_HEALTH, m_HealthBeforeEnterInstance > maxhealth ? maxhealth : m_HealthBeforeEnterInstance );
	SetUInt32Value( UNIT_FIELD_POWER1, m_ManaBeforeEnterInstance > maxmana ? maxmana : m_ManaBeforeEnterInstance );

	if( m_HealthBeforeEnterInstance == 0 )
	{
		SetUInt32Value( UNIT_FIELD_POWER1, 0 );
		setDeathState( JUST_DIED );
	}
	SetFFA( 0 );
	SetInstanceBusy( false );
	m_resurrectInstanceID = 0;
	m_resurrectMapId = 0;
	ClearRandomResurrectLocation();
	UnlockSunyouInstanceJoin();

	g_crosssrvmgr->OnPlayerReturn2Mother( this );
}

void Player::SetLeaveGuildDate()
{
	m_leave_guild_date = (uint32)UNIXTIME;
}

void Player::JingLianItem()
{
	uint8 ret = m_JingLianItemInterface->JingLian( true );

	MSG_S2C::stJingLianItemAck ack;
	ack.result = ret;
	GetSession()->SendPacket( ack );
	/*
	if( JINGLIAN_RESULT_CHECK_SUCCESS == ret )
	{
		sEventMgr.AddEvent( this, &Player::JingLianCommit, EVENT_SUNYOU_JINGLIAN_ITEM_COMMIT, 3000, 1, 0 );
	}
	*/
}

void Player::JingLianCommit()
{
	MSG_S2C::stJingLianItemAck ack;
	ack.result = m_JingLianItemInterface->JingLian();
	GetSession()->SendPacket( ack );
}

bool Player::RefineItem( bool b )
{
	if( b )
	{
		MSG_S2C::stJingLianItemAck ack;
		ack.result = m_JingLianItemInterface->JingLian();
		GetSession()->SendPacket( ack );
		return true;
	}
	else
	{
		uint8 ret = m_JingLianItemInterface->JingLian( true );
		MSG_S2C::stJingLianItemAck ack;
		ack.result = ret;
		GetSession()->SendPacket( ack );
		return ret == JINGLIAN_RESULT_CHECK_SUCCESS;
	}
}

PlayerInfo* Player::GetTeacher()
{
	if( !m_uint32Values[PLAYER_FIELD_TEACHER] ) return NULL;

	return objmgr.GetPlayerInfo( m_uint32Values[PLAYER_FIELD_TEACHER] );
}

PlayerInfo* Player::GetStudent()
{
	if( !m_uint32Values[PLAYER_FIELD_STUDENT] ) return NULL;

	return objmgr.GetPlayerInfo( m_uint32Values[PLAYER_FIELD_STUDENT] );
}

void Player::Recruit( const std::string& student )
{
	if( (uint32)UNIXTIME - m_last_recruit_time < 60 )
	{
		GetSession()->SendNotification( build_language_string( BuildString_PlayerSystemMessageRecruitInShortTime ) );//"短时间内不能连续收徒" );
		return;
	}
	MSG_S2C::stRecruitAck ack;
	if( HasTeacherOrStudent() ) 
	{
		ack.ret = MSG_S2C::stRecruitAck::RESULT_YOU_ALREADY_HAVE;
		GetSession()->SendPacket( ack );
	}
	else
	{
		Player* plr = objmgr.GetPlayer( student.c_str() );
		if( plr )
		{
			if( plr->GetFactionId() != GetFactionId() )
				ack.ret = MSG_S2C::stRecruitAck::RESULT_DIFFERENT_FACTION;
			if( plr->m_temp_teacher_id == GetLowGUID() )
				ack.ret = MSG_S2C::stRecruitAck::RESULT_DUPLICATE;
			else if( plr->m_temp_teacher_id )
				ack.ret = MSG_S2C::stRecruitAck::RESULT_TARGET_ALREADY_HAVE;
			else if( plr->HasTeacherOrStudent() )
				ack.ret = MSG_S2C::stRecruitAck::RESULT_TARGET_ALREADY_HAVE;
			else if( plr->getLevel() > getLevel() )
				ack.ret = MSG_S2C::stRecruitAck::RESULT_LOW_LEVEL;
			else
			{
				m_last_recruit_time = (uint32)UNIXTIME;
				plr->BeRecruit( m_playerInfo->name, GetLowGUID() );
				return;
			}

			GetSession()->SendPacket( ack );
		}
		else
		{
			ack.ret = MSG_S2C::stRecruitAck::RESULT_OFFLINE;
			GetSession()->SendPacket( ack );
		}
	}
}

void Player::BeRecruit( const char* teacher, uint32 id )
{
	MSG_S2C::stRecruitNotify notify;
	notify.who = teacher;
	GetSession()->SendPacket( notify );
	m_temp_teacher_id = id;
}

void Player::ReplyRecruit( bool accept )
{
	if( !m_temp_teacher_id ) return;

	MSG_S2C::stRecruitAck ack;
	Player* plr = objmgr.GetPlayer( m_temp_teacher_id );
	if( !plr )
	{
		ack.ret = MSG_S2C::stRecruitAck::RESULT_OFFLINE;
	}
	else
	{
		if( plr->HasTeacherOrStudent() )
		{
			ack.ret = MSG_S2C::stRecruitAck::RESULT_TARGET_ALREADY_HAVE;
			GetSession()->SendPacket( ack );
		}
		else
		{
			ack.teacher = plr->GetName();
			ack.teacher_id = plr->GetLowGUID();
			ack.student = GetName();
			ack.student_id = GetLowGUID();
			if( accept )
			{
				ack.ret = MSG_S2C::stRecruitAck::RESULT_ACCEPT;
				GetSession()->SendPacket( ack );
				plr->GetSession()->SendPacket( ack );

				plr->SetUInt32Value( PLAYER_FIELD_STUDENT, GetLowGUID() );
				plr->EstablishTeacherStudentRelationship();

				SetUInt32Value( PLAYER_FIELD_TEACHER, plr->GetLowGUID() );
				EstablishTeacherStudentRelationship();
			}
			else
			{
				ack.ret = MSG_S2C::stRecruitAck::RESULT_REFUSE;
				plr->GetSession()->SendPacket( ack );
			}
		}
	}
	m_temp_teacher_id = 0;
}

void Player::EstablishTeacherStudentRelationship()
{
	if( m_uint32Values[PLAYER_FIELD_STUDENT] && m_uint32Values[PLAYER_FIELD_STUDENT] != GetLowGUID() )
	{
		addSpell( 2100 );
		addSpell( 2101 );
		SetUInt32Value( PLAYER_FIELD_ENCHANT_TEACHER_CHARGES, 1 );

		sEventMgr.AddEvent( this, &Player::CheckTeacherStudentRelationship, EVENT_PLAYER_CHECK_ENCHANT_TEACHER_RELATIONSHIP, 60*1000, 0xFFFFFFFF, 0 );
	}
	else if( m_uint32Values[PLAYER_FIELD_TEACHER] && m_uint32Values[PLAYER_FIELD_TEACHER] != GetLowGUID() )
	{
		addSpell( 2100 );
	}
	else
		assert( 0 && "void Player::EstablishTeacherStudentRelationship()" );
}

void Player::RemoveTeacherStudentRelationship( bool bfirst )
{
	if( m_uint32Values[PLAYER_FIELD_TEACHER] && m_uint32Values[PLAYER_FIELD_TEACHER] != GetLowGUID() )
	{
		if( bfirst )
		{
			Player* plr = objmgr.GetPlayer( m_uint32Values[PLAYER_FIELD_TEACHER] );
			if( plr )
				plr->RemoveTeacherStudentRelationship( false );
			else
			{
				sWorld.ExecuteSqlToDBServer( "update characters set student_id = 0 where guid = %u", m_uint32Values[PLAYER_FIELD_TEACHER] );
			}
		}
		SetUInt32Value( PLAYER_FIELD_TEACHER, 0 );
	}
	
	if( m_uint32Values[PLAYER_FIELD_STUDENT] && m_uint32Values[PLAYER_FIELD_STUDENT] != GetLowGUID() )
	{
		if( bfirst )
		{
			Player* plr = objmgr.GetPlayer( m_uint32Values[PLAYER_FIELD_STUDENT] );
			if( plr )
				plr->RemoveTeacherStudentRelationship( false );
			else
			{
				sWorld.ExecuteSqlToDBServer( "update characters set teacher_id = 0 where guid = %u", m_uint32Values[PLAYER_FIELD_STUDENT] );
			}
		}
		SetUInt32Value( PLAYER_FIELD_STUDENT, 0 );
		removeSpell( 2101, false, false, 0 );
		sEventMgr.RemoveEvents( this, EVENT_PLAYER_CHECK_ENCHANT_TEACHER_RELATIONSHIP );
	}
	removeSpell( 2100, false, false, 0 );
}

void Player::CheckTeacherStudentRelationship()
{
	if( m_uint32Values[PLAYER_FIELD_STUDENT] && m_uint32Values[PLAYER_FIELD_STUDENT] != GetLowGUID() )
	{
		Player* plr = objmgr.GetPlayer( m_uint32Values[PLAYER_FIELD_STUDENT] );
		if( plr && plr->GetMapId() == GetMapId() && plr->GetInstanceID() == GetInstanceID() )
		{
			if( CalcDistance( plr ) <= 30.f )
			{
				m_enchant_teacher_time++;
				if( m_enchant_teacher_time >= 30 )
				{
					m_enchant_teacher_time = 0;
					if( m_uint32Values[PLAYER_FIELD_ENCHANT_TEACHER_CHARGES] < 30 )
						ModUnsigned32Value( PLAYER_FIELD_ENCHANT_TEACHER_CHARGES, 1 );
				}
			}
		}
	}
}

void Player::CheckTeacherStudentLevel()
{
	if( m_uint32Values[PLAYER_FIELD_TEACHER] && m_uint32Values[PLAYER_FIELD_TEACHER] != GetLowGUID() )
	{
		PlayerInfo* plr = objmgr.GetPlayerInfo( m_uint32Values[PLAYER_FIELD_TEACHER] );
		if( plr )
		{
			if( plr->lastLevel < getLevel() )
			{
				RemoveTeacherStudentRelationship();

				std::string notify( build_language_string( BuildString_PlayerSystemMessageStudentLevelOverTeacherLevel ) );
				if( plr->m_loggedInPlayer )
					plr->m_loggedInPlayer->GetSession()->SendNotification( notify.c_str() );//"徒弟等级超过师傅，已自动脱离师徒关系" );
				GetSession()->SendNotification( notify.c_str() );//"徒弟等级超过师傅，已自动脱离师徒关系" );
				return;
			}
		}
	}

	if( m_uint32Values[PLAYER_FIELD_STUDENT] && m_uint32Values[PLAYER_FIELD_STUDENT] != GetLowGUID() )
	{
		PlayerInfo* plr = objmgr.GetPlayerInfo( m_uint32Values[PLAYER_FIELD_STUDENT] );
		if( plr )
		{
			if( plr->lastLevel > getLevel() )
			{
				RemoveTeacherStudentRelationship();

				std::string notify( build_language_string( BuildString_PlayerSystemMessageStudentLevelOverTeacherLevel ) );
				if( plr->m_loggedInPlayer )
					plr->m_loggedInPlayer->GetSession()->SendNotification( notify.c_str() );//"徒弟等级超过师傅，已自动脱离师徒关系" );
				GetSession()->SendNotification( notify.c_str() );//"徒弟等级超过师傅，已自动脱离师徒关系" );
				return;
			}
		}
	}
}

void Player::AddFinishedDailyQuest( uint32 id )
{
	m_playerInfo->finishedDailyQuests.insert( id );
}

bool Player::HasFinishedDailyQuest( uint32 id ) const
{
	return m_playerInfo->finishedDailyQuests.find( id ) != m_playerInfo->finishedDailyQuests.end();
}

void Player::AddQuestMobs( uint32 entry )
{
	std::map<uint32, uint32>::iterator it = quest_mobs.find( entry );
	if( it != quest_mobs.end() )
		++it->second;
	else
		quest_mobs[entry] = 1;
}

void Player::UpdateGuildScore( uint32 score )
{
	SetUInt32Value( PLAYER_FIELD_GUILD_SCORE, score );
}

void Player::UpdateGuildEmblem()
{
//	if( m_playerInfo->guild )
//	{
//		SetUInt32Value( PLAYER_FIELD_GUILD_EMBLEM_STYLE, m_playerInfo->guild->GetEmblemStyle() );
//		SetUInt32Value( PLAYER_FIELD_GUILD_EMBLEM_COLOR, m_playerInfo->guild->GetEmblemColor() );
//	}
}

void Player::SetCastleDebuffSpellID( uint32 id )
{
	if( id == 0 && m_castle_debuff_spell_id )
		RemoveAura( m_castle_debuff_spell_id );
	else if( m_castle_debuff_spell_id == 0 && id )
	{
		SpellEntry* proto = dbcSpell.LookupEntry( id );
		if( proto )
		{
			Spell * sp = new Spell( this, proto, true, 0 );
			SpellCastTargets targets( GetGUID() );
			sp->prepare(&targets);
		}
	}
	m_castle_debuff_spell_id = id;
}

void Player::ModCoin( int delta )
{
	ModUnsigned32Value( PLAYER_FIELD_COINAGE, delta );
	m_playerInfo->total_coin = GetUInt32Value( PLAYER_FIELD_COINAGE );
}

void Player::ModKillCount( int32 delta )
{
	ModUnsigned32Value( PLAYER_FIELD_KILL_COUNT, delta );
	m_playerInfo->kill_count = GetUInt32Value( PLAYER_FIELD_KILL_COUNT );
}

void Player::ModBeKillCount( int32 delta )
{
	ModUnsigned32Value( PLAYER_FIELD_BE_KILL_COUNT, delta );
	m_playerInfo->be_kill_count = GetUInt32Value( PLAYER_FIELD_BE_KILL_COUNT );
}

void Player::ModTotalHonor( int32 delta )
{
	ModUnsigned32Value( PLAYER_FIELD_TOTAL_HONOR, delta );
	m_playerInfo->total_honor = GetUInt32Value( PLAYER_FIELD_TOTAL_HONOR );

	if( m_playerInfo->total_honor >= 1000 )
	{
		if( !HasTitle( 18 ) )
			QuickAddTitle( 18 );
	}
	if( m_playerInfo->total_honor >= 10000 )
	{
		if( !HasTitle( 19 ) )
			QuickAddTitle( 19 );
	}
	if( m_playerInfo->total_honor >= 100000 )
	{
		if( !HasTitle( 20 ) )
			QuickAddTitle( 20 );
	}
}

void Player::ModHonorCurrency( int32 delta )
{
	ModUnsigned32Value( PLAYER_FIELD_HONOR_CURRENCY, delta );
	m_playerInfo->honor_currency = GetUInt32Value( PLAYER_FIELD_HONOR_CURRENCY );

	if( m_sunyou_instance && m_sunyou_instance->IsBattleGround() )
	{
		SunyouBattleGroundBase* pBGBase = m_sunyou_instance->GetBGBase();
		if( pBGBase )
			pBGBase->OnBGPlayerHonorModify( this, delta );
	}
}

bool Player::InsertTitle(uint32 titleid, uint32 time_left)
{
	if( HasTitle( titleid ) )
		return false;

	m_Titles[titleid] = time_left;
	title_reward* tr = dbcTitleRewards.LookupEntry( titleid );
	if( tr )
	{
		if( tr->item1 && tr->count1 )
			GiveGift( 1, tr->item1, tr->count1, true, GIFT_CATEGORY_TITLE );
		if( tr->item2 && tr->count2 )
			GiveGift( 1, tr->item2, tr->count2, true, GIFT_CATEGORY_TITLE );
		if( tr->item3 && tr->count3 )
			GiveGift( 1, tr->item3, tr->count3, true, GIFT_CATEGORY_TITLE );

		if( tr->need_broadcast )
		{
			MSG_S2C::stTitleBroadcast Msg;
			Msg.Who = GetName();
			Msg.TitleID = titleid;
			sWorld.SendGlobalMessage(Msg);
		}
		return true;
	}
	else
		return false;
}

void Player::SendTitleList()
{
	MSG_S2C::stTitleList Msg;
	for( std::map<uint32, uint32>::iterator it = m_Titles.begin(); it != m_Titles.end(); ++it )
		Msg.vTitleList[it->first] = stTitle( it->first, it->second );
	m_session->SendPacket(Msg);
}

bool Player::HasTitle( uint32 titleid ) const
{
	return m_Titles.find( titleid ) != m_Titles.end();
}

void Player::KnockBack( LocationVector casterPosition, uint32 spellid, uint32 distance, uint32 casterid )
{
	if( !isAlive() )
		return;

	DismissMount();
	RemoveAllAurasByMechanic(MECHANIC_FLEEING, -1, true);

	float dx, dy;
	float angle = Object::calcAngle( casterPosition.x, casterPosition.y, GetPositionX(), GetPositionY() );
	angle = angle * M_PI / 180.f;

	MSG_S2C::stMove_Knock_Back Msg;
	Msg.guid = GetGUID();
	Msg.distance = distance;
	Msg.orientation = 3.f * M_PI / 2.f - angle;
	SendMessageToSet( Msg, true );
}

void Player::UpdateInrangeQuestGiverState()
{
	MSG_S2C::stQuestGiver_Inrange_Status_Query_Response Msg;
	Object::InRangeSet::iterator itr;
	Creature * pCreature;

	// 32 count
	// <foreach count>
	//    64 guid
	//    8 status
	bool has = false;

	for( itr = m_objectsInRange.begin(); itr != m_objectsInRange.end(); ++itr )
	{
		pCreature = static_cast<Creature*>(*itr);
		if( pCreature->GetTypeId() != TYPEID_UNIT )
			continue;

		if( pCreature->isQuestGiver() )
		{
			MSG_S2C::stQuestGiver_Inrange_Status_Query_Response::stQuestGiver quest_giver;
			quest_giver.npc_guid = pCreature->GetGUID();
			quest_giver.stat = uint8(sQuestMgr.CalcStatus( pCreature, this ));
			Msg.vQuestGivers.push_back( quest_giver );
			has = true;
		}
	}

	if( has )
		GetSession()->SendPacket(Msg);
}

void Player::UpdateQuestGiverState( Creature* QuestGiver )
{
	MSG_S2C::stQuestGiver_Stats Msg;
	Msg.guid	= QuestGiver->GetGUID();
	Msg.stats	= sQuestMgr.CalcStatus(QuestGiver, this);
	GetSession()->SendPacket( Msg );
}

void Player::RemoveItemAmt( uint32 entry, uint32 amt )
{
	GetItemInterface()->RemoveItemAmt( entry, amt );
	sQuestMgr.OnPlayerItemLost( this, entry, amt );
}

void Player::AddNeedSendCurrentMoveCreature( uint64 guid )
{
	m_setNeedSendCurrentMoveCreature.insert( std::make_pair(guid, 2) );
}

void Player::DropEquipmentOnGround( Player* attacker )
{
	if( g_crosssrvmgr->m_isInstanceSrv )
		return;

	if( attacker->GetGuildId() > 0 && attacker->GetGuildId() == this->GetGuildId() )
		return;

	int rate = attacker->GetInt32Value( PLAYER_FIELD_LOOT_EQUIPMENT_RATE ) + this->GetInt32Value( PLAYER_FIELD_DROP_EQUIPMENT_RATE );
	if( rate && Rand( rate ) )
	{
		uint8 slot = EQUIPMENT_SLOT_START + rand() % EQUIPMENT_SLOT_END;
		Item* p = this->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot( INVENTORY_SLOT_NOT_SET, slot, false );
		if( p )
		{
			p->RemoveFromWorld();
			this->PushOutOfRange( p->GetGUID() );
			this->ProcessPendingUpdates();
			p->SetOwner( NULL );
			float z = CollideInterface.GetHeight( m_mapMgr->GetBaseMap(), m_position.x, m_position.y, m_position.z + 2.f );
			p->SetPosition( m_position.x, m_position.y, z, m_position.o );
			p->PushToWorld( m_mapMgr );
		}
	}
}

void Player::SendSystemMail( uint32 item_entry1, uint32 item_count1, uint32 item_entry2, uint32 item_count2, uint32 money, uint8 isyuanbao, const char* str_subject, const char* str_body, build_string_t subject, build_string_t body, const char* arg1, const char* arg2, const char* arg3, const char* arg4, const char* arg5 )
{
	MailMessage msg;
	msg.message_type = NORMAL;
	msg.sender_guid = GetGUID();
	msg.player_guid = GetGUID();
	if( subject )
		msg.subject = str_subject;
	else
		msg.subject = build_language_string( subject );
	msg.money = money;
	msg.cod = 0;
	if( item_entry1 )
	{
		Item *itm = objmgr.CreateItem(item_entry1, this);
		if(!itm)
			return;

		itm->m_isDirty=true;
		itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, item_count1);
		itm->RemoveFromWorld();
		itm->SetOwner(NULL);
		itm->SaveToDB( INVENTORY_SLOT_NOT_SET, 0, true, NULL );
		msg.items.push_back( itm->GetUInt32Value(OBJECT_FIELD_GUID) );
		delete itm;
	}
	if( item_entry2 )
	{
		Item *itm = objmgr.CreateItem(item_entry2, this);
		if(!itm)
			return;

		itm->m_isDirty=true;
		itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, item_count2);
		itm->RemoveFromWorld();
		itm->SetOwner(NULL);
		itm->SaveToDB( INVENTORY_SLOT_NOT_SET, 0, true, NULL );
		msg.items.push_back( itm->GetUInt32Value(OBJECT_FIELD_GUID) );
		delete itm;
	}
	msg.stationary = 0;
	msg.delivery_time = (uint32)UNIXTIME;
	msg.read_flag = false;
	msg.copy_made = false;
	msg.deleted_flag = false;
	msg.yp = 0;
	msg.is_yuanbao = isyuanbao;

	if( str_body )
		msg.body = str_body;
	else
	{
		if( arg5 )
			msg.body = build_language_string( body, arg1, arg2, arg3, arg4, arg5 );
		else if( arg4 )
			msg.body = build_language_string( body, arg1, arg2, arg3, arg4 );
		else if( arg3 )
			msg.body = build_language_string( body, arg1, arg2, arg3 );
		else if( arg2 )
			msg.body = build_language_string( body, arg1, arg2 );
		else if( arg1 )
			msg.body = build_language_string( body, arg1 );
		else
			msg.body = build_language_string( body );
	}

	if(!sMailSystem.MailOption(MAIL_FLAG_NO_EXPIRY))
		msg.expire_time = (uint32)UNIXTIME + (TIME_DAY * 30);
	else
		msg.expire_time = 0;

	// Great, all our info is filled in. Now we can add it to the other players mailbox.
	sMailSystem.DeliverMessage(GetGUID(), &msg);
}
