#ifndef _PLAYER_H
#define _PLAYER_H

#include "../../../SDBase/Protocol/S2C.h"
#include "MultiLanguageStringMgr.h"

class Guild;
struct GuildRank;
struct GuildMember;
class LfgMatch;
class ArenaTeam;
class TaxiPath;
class Channel;
struct LevelInfo;
class Charter;
class GossipMenu;
class Transporter;
class JingLianItemInterface;
class DynamicObject;
class CDSSocket;

#define MAX_PET_NO 3
#define PLAYER_NORMAL_RUN_SPEED 7.f
#define PLAYER_NORMAL_SWIM_SPEED 4.722222f
#define PLAYER_NORMAL_FLIGHT_SPEED 7.f
#define PLAYER_HONORLESS_TARGET_SPELL 2479
#define MONSTER_NORMAL_RUN_SPEED 7.f
/* action button defines */
#define PLAYER_ACTION_BUTTON_COUNT 132
#define PLAYER_ACTION_BUTTON_SIZE PLAYER_ACTION_BUTTON_COUNT * sizeof(ActionButton)
//====================================================================
//  Inventory
//  Holds the display id and item type id for objects in
//  a character's inventory
//====================================================================


enum PlayerStatus
{
	NONE			 = 0,
	TRANSFER_PENDING = 1,
};

enum RankTitles
{
	PVPTITLE_NONE                   = 0x0,
	PVPTITLE_PRIVATE                = 0x02,
	PVPTITLE_CORPORAL               = 0x04,
	PVPTITLE_SERGEANT               = 0x08,
	PVPTITLE_MASTER_SERGEANT        = 0x10,
	PVPTITLE_SERGEANT_MAJOR         = 0x20,
	PVPTITLE_KNIGHT                 = 0x40,
	PVPTITLE_KNIGHT_LIEUTENANT      = 0x80,
	PVPTITLE_KNIGHT_CAPTAIN         = 0x100,
	PVPTITLE_KNIGHT_CHAMPION        = 0x200,
	PVPTITLE_LIEUTENANT_COMMANDER   = 0x400,
	PVPTITLE_COMMANDER              = 0x800,
	PVPTITLE_MARSHAL                = 0x1000,
	PVPTITLE_FIELD_MARSHAL          = 0x2000,
	PVPTITLE_GRAND_MARSHAL          = 0x4000,
	PVPTITLE_SCOUT                  = 0x8000,
	PVPTITLE_GRUNT                  = 0x10000,
	PVPTITLE_HSERGEANT              = 0x20000,
	PVPTITLE_SENIOR_SERGEANT        = 0x40000,
	PVPTITLE_FIRST_SERGEANT         = 0x80000,
	PVPTITLE_STONE_GUARD            = 0x100000,
	PVPTITLE_BLOOD_GUARD            = 0x200000,
	PVPTITLE_LEGIONNAIRE            = 0x400000,
	PVPTITLE_CENTURION              = 0x800000,
	PVPTITLE_CHAMPION               = 0x1000000,
	PVPTITLE_LIEUTENANT_GENERAL     = 0x2000000,
	PVPTITLE_GENERAL                = 0x4000000,
	PVPTITLE_WARLORD                = 0x8000000,
	PVPTITLE_HIGH_WARLORD           = 0x10000000,
};

enum PvPAreaStatus
{
    AREA_ALLIANCE = 1,
    AREA_HORDE = 2,
    AREA_CONTESTED = 3,
    AREA_PVPARENA = 4,
};

enum PlayerMovementType
{
    MOVE_ROOT	    = 1,
    MOVE_UNROOT	    = 2,
    MOVE_WATER_WALK = 3,
    MOVE_LAND_WALK  = 4,
};


enum Standing
{
    STANDING_HATED,
    STANDING_HOSTILE,
    STANDING_UNFRIENDLY,
    STANDING_NEUTRAL,
    STANDING_FRIENDLY,
    STANDING_HONORED,
    STANDING_REVERED,
    STANDING_EXALTED
};


enum CooldownTypes
{
	COOLDOWN_TYPE_SPELL			= 0,
	COOLDOWN_TYPE_CATEGORY		= 1,
	NUM_COOLDOWN_TYPES,
};

struct spells
{
	uint16  spellId;
	uint16  slotId;
};

#pragma pack(push,1)
struct ActionButton
{
	uint32  Action;
	uint8   Type;
	uint8   Misc;
};
#pragma pack(pop)

struct CreateInfo_ItemStruct
{
	uint32  protoid;
	uint8   slot;
	uint32  amount;
};

struct CreateInfo_SkillStruct
{
	uint32  skillid;
	uint32  currentval;
	uint32  maxval;
};

struct CreateInfo_ActionBarStruct
{
	uint32  button;
	uint32  action;
	uint32  type;
	uint32  misc;
};

struct PlayerCreateInfo{
	uint8   index;
	uint8   race;
	uint32  factiontemplate;
	uint8   class_;
	uint32  mapId;
	uint32  zoneId;
	float   positionX;
	float   positionY;
	float   positionZ;
	uint32  displayId;
	uint8   strength;
	uint8   ability;
	uint8   stamina;
	uint8   intellect;
	uint8   spirit;
	uint8	vitality;
	uint32  health;
	uint32  mana;
	uint32  rage;
	uint32  focus;
	uint32  energy;
	uint32  attackpower;
	float   mindmg;
	float   maxdmg;
	std::list<CreateInfo_ItemStruct> items;
	std::list<CreateInfo_SkillStruct> skills;
	std::list<CreateInfo_ActionBarStruct> actionbars;
	std::set<uint32> spell_list;
	//uint32 item[10];
	//uint8 item_slot[10];
	//uint16 spell[10];
};

struct DamageSplit
{
	Player* caster;
	Aura*   aura;
	uint32  miscVal;
	union
	{
		uint32 damage;
		float damagePCT;
	};
};

struct LoginAura
{
    uint32 id;
    uint32 dur;
};

enum FactionRating
{
	HATED,
	HOSTILE,
	UNFRIENDLY,
	NEUTRAL,
	FRIENDLY,
	HONORED,
	REVERED,
	EXALTED
};
struct FactionReputation
{
	int32 standing;
	uint8 flag;
	int32 baseStanding;
	SUNYOU_INLINE int32 CalcStanding() { return standing - baseStanding; }
	SUNYOU_INLINE bool Positive() { return standing >= 0; }
};

inline void SaveMapToString( const std::map<uint32, uint32>& m, std::string& str )
{
	char tmp[64];
	for( std::map<uint32, uint32>::const_iterator it = m.begin(); it != m.end(); ++it )
	{
		itoa( it->first, tmp, 10 );
		str += tmp;
		str += ":";
		itoa( it->second, tmp, 10 );
		str += tmp;
		str += ",";
	}
}
inline void LoadMapFromString( std::map<uint32, uint32>& m, const char* str )
{
	if( !str || str[0] == 0 )
		return;
	while( true )
	{
		const char* p = strstr( str, ":" );
		if( !p )
			break;
		m[atoi( str )] = atoi( p + 1 );
		p = strstr( str, "," );
		if( !p )
			break;
		str = p + 1;
	}
}

struct PlayerInfo
{
	PlayerInfo();
	~PlayerInfo();
	void ResetAllInstance();

	uint32 guid;
	uint32 acct;
	char * name;
	uint32 race;
	uint32 gender;
	uint32 cl;
	uint32 team;

	time_t lastOnline;
	uint32 lastZone;
	uint32 lastLevel;
	Group * m_Group;
	int8 subGroup;
#ifdef VOICE_CHAT
	int8 groupVoiceId;
#endif

	Player * m_loggedInPlayer;
	Guild * guild;
	GuildRank * guildRank;
	GuildMember * guildMember;
	std::set<uint32> finishedDailyQuests;
	std::set<uint32> instances;
	std::map<uint32, uint32> instanceEnterLimit;
	uint32 kill_count;
	uint32 be_kill_count;
	uint32 total_honor;
	uint32 honor_currency;

	uint32 total_coin;
	//uint32 total_contribute;
	//uint32 total_charge;
	std::map<uint32, uint32> instance_enter;
	std::map<uint32, uint32> battleground_win;
	std::map<uint32, uint32> item_obtain;

	uint32 m_arena_win[5];
	uint32 m_arena_lose[5];
	uint32 m_arena_win_lv80[5];
	uint32 m_arena_lose_lv80[5];

	uint32 GetBGWinCount( uint32 mapid )
	{
		std::map<uint32, uint32>::iterator it =  battleground_win.find( mapid ) ;
		if( it != battleground_win.end() )
			return it->second;
		else
			return 0;
	}
	void AddBGWinCount( uint32 mapid )
	{
		std::map<uint32, uint32>::iterator it =  battleground_win.find( mapid ) ;
		if( it != battleground_win.end() )
			++(it->second);
		else
			battleground_win.insert( std::make_pair( mapid, 1 ) );
	}

	uint32 GetItemObtainCount( uint32 entry )
	{
		std::map<uint32, uint32>::iterator it =  item_obtain.find( entry ) ;
		if( it != item_obtain.end() )
			return it->second;
		else
			return 0;
	}
	void AddItemObtain( uint32 entry, uint32 amt )
	{
		std::map<uint32, uint32>::iterator it =  item_obtain.find( entry ) ;
		if( it != item_obtain.end() )
			(it->second) += amt;
		else
			item_obtain.insert( std::make_pair( entry, amt ) );
	}
	uint32 yuanbao;
};
struct PlayerPet
{
	string name;
	uint32 entry;
	string fields;
	uint32 xp;
	bool active;
	char stablestate;
	uint32 number;
	uint32 level;
	uint32 loyaltyxp;
	uint32 happinessupdate;
	string actionbar;
	bool summon;
	uint32 loyaltypts;
	uint32 loyaltyupdate;
	char loyaltylvl;
	uint32 LastFeedTime;
};
enum MeetingStoneQueueStatus
{
	MEETINGSTONE_STATUS_NONE								= 0,
	MEETINGSTONE_STATUS_JOINED_MEETINGSTONE_QUEUE_FOR	   = 1,
	MEETINGSTONE_STATUS_PARTY_MEMBER_LEFT_LFG			   = 2,
	MEETINGSTONE_STATUS_PARTY_MEMBER_REMOVED_PARTY_REMOVED  = 3,
	MEETINGSTONE_STATUS_LOOKING_FOR_NEW_PARTY_IN_QUEUE	  = 4,
	MEETINGSTONE_STATUS_NONE_UNK							= 5,
};
enum ItemPushResultTypes
{
	ITEM_PUSH_TYPE_LOOT			 = 0x00000000,
	ITEM_PUSH_TYPE_RECEIVE		  = 0x00000001,
	ITEM_PUSH_TYPE_CREATE		   = 0x00000002,
};
struct WeaponModifier
{
	uint32 spellid;
	uint32 wclass;
	uint32 subclass;
	float value;
};
struct PetActionBar
{
	uint32 spell[10];
};
struct classScriptOverride
{
	uint32 id;
	uint32 effect;
	uint32 aura;
	uint32 damage;
	bool percent;
};

#define RESTSTATE_RESTED			 1
#define RESTSTATE_NORMAL			 2
#define RESTSTATE_TIRED100		     3
#define RESTSTATE_TIRED50			 4
#define RESTSTATE_EXHAUSTED		     5
#define UNDERWATERSTATE_NONE		 0
#define UNDERWATERSTATE_SWIMMING	 1
#define UNDERWATERSTATE_UNDERWATER   2
#define UNDERWATERSTATE_RECOVERING   4
#define UNDERWATERSTATE_TAKINGDAMAGE 8
#define UNDERWATERSTATE_FATIGUE	     16
#define UNDERWATERSTATE_LAVA		 32
#define UNDERWATERSTATE_SLIME		 64


enum DUEL_STATUS
{
	DUEL_STATUS_OUTOFBOUNDS,
	DUEL_STATUS_INBOUNDS
};
enum DUEL_STATE
{
	DUEL_STATE_REQUESTED,
	DUEL_STATE_STARTED,
	DUEL_STATE_FINISHED
};
enum DUEL_WINNER
{
	DUEL_WINNER_KNOCKOUT,
	DUEL_WINNER_RETREAT,
};
#define PLAYER_ATTACK_TIMEOUT_INTERVAL	5000
#define PLAYER_FORCED_RESURECT_INTERVAL	360000 // 1000*60*6= 6 minutes 

struct PlayerSkill
{
	skilllineentry * Skill;
	uint32 CurrentValue;
	uint32 MaximumValue;
	uint32 BonusValue;
	float GetSkillUpChance();
	void Reset(uint32 Id);
};

enum SPELL_INDEX
{
	SPELL_TYPE_INDEX_MARK			= 1,
	SPELL_TYPE_INDEX_POLYMORPH		= 2,
	SPELL_TYPE_INDEX_FEAR			= 3,
	SPELL_TYPE_INDEX_SAP			= 4,
	SPELL_TYPE_INDEX_SCARE_BEAST	= 5,
	SPELL_TYPE_INDEX_HIBERNATE		= 6,
	SPELL_TYPE_INDEX_EARTH_SHIELD	= 7,
	SPELL_TYPE_INDEX_CYCLONE		= 8,
	SPELL_TYPE_INDEX_BANISH			= 9,
	SPELL_TYPE_INDEX_JUDGEMENT		= 10,	
	NUM_SPELL_TYPE_INDEX			= 11,
};

#define PLAYER_RATING_MODIFIER_RANGED_SKILL						PLAYER_FIELD_ALL_WEAPONS_SKILL_RATING
#define PLAYER_RATING_MODIFIER_DEFENCE							PLAYER_FIELD_DEFENCE_RATING
#define PLAYER_RATING_MODIFIER_DODGE							PLAYER_FIELD_DODGE_RATING
#define PLAYER_RATING_MODIFIER_PARRY							PLAYER_FIELD_PARRY_RATING
#define PLAYER_RATING_MODIFIER_BLOCK							PLAYER_FIELD_BLOCK_RATING
#define PLAYER_RATING_MODIFIER_MELEE_HIT						PLAYER_FIELD_MELEE_HIT_RATING
#define PLAYER_RATING_MODIFIER_RANGED_HIT						PLAYER_FIELD_RANGED_HIT_RATING
#define PLAYER_RATING_MODIFIER_SPELL_HIT						PLAYER_FIELD_SPELL_HIT_RATING
#define PLAYER_RATING_MODIFIER_MELEE_CRIT						PLAYER_FIELD_MELEE_CRIT_RATING
#define PLAYER_RATING_MODIFIER_RANGED_CRIT						PLAYER_FIELD_RANGED_CRIT_RATING
#define PLAYER_RATING_MODIFIER_SPELL_CRIT						PLAYER_FIELD_SPELL_CRIT_RATING
#define PLAYER_RATING_MODIFIER_MELEE_HIT_AVOIDANCE				PLAYER_FIELD_HIT_TAKEN_MELEE_RATING // GUESSED
#define PLAYER_RATING_MODIFIER_RANGED_HIT_AVOIDANCE				PLAYER_FIELD_HIT_TAKEN_RANGED_RATING // GUESSED
#define PLAYER_RATING_MODIFIER_SPELL_HIT_AVOIDANCE				PLAYER_FIELD_HIT_TAKEN_SPELL_RATING // GUESSED
#define PLAYER_RATING_MODIFIER_MELEE_CRIT_RESILIENCE			PLAYER_FIELD_CRIT_TAKEN_MELEE_RATING
#define PLAYER_RATING_MODIFIER_RANGED_CRIT_RESILIENCE			PLAYER_FIELD_CRIT_TAKEN_RANGED_RATING
#define PLAYER_RATING_MODIFIER_SPELL_CRIT_RESILIENCE			PLAYER_FIELD_CRIT_TAKEN_SPELL_RATING
#define PLAYER_RATING_MODIFIER_MELEE_HASTE						PLAYER_FIELD_MELEE_HASTE_RATING
#define PLAYER_RATING_MODIFIER_RANGED_HASTE						PLAYER_FIELD_RANGED_HASTE_RATING
#define PLAYER_RATING_MODIFIER_SPELL_HASTE						PLAYER_FIELD_SPELL_HASTE_RATING
#define PLAYER_RATING_MODIFIER_MELEE_MAIN_HAND_SKILL			PLAYER_FIELD_MELEE_WEAPON_SKILL_RATING
#define PLAYER_RATING_MODIFIER_MELEE_OFF_HAND_SKILL				PLAYER_FIELD_OFFHAND_WEAPON_SKILL_RATING
// #define UNKNOWN												PLAYER_FIELD_RANGED_WEAPON_SKILL_RATING
#define PLAYER_RATING_MODIFIER_EXPERTISE						PLAYER_FIELD_EXPERTISE_RATING

struct PlayerCooldown
{
	uint32 ExpireTime;
	uint32 ItemId;
	uint32 SpellId;
};

//====================================================================
//  Player
//  Class that holds every created character on the server.
//
//  TODO:  Attach characters to user accounts
//====================================================================
typedef std::set<uint32>	                        SpellSet;
typedef std::list<classScriptOverride*>             ScriptOverrideList;
typedef std::set<uint32>                            SaveSet;
typedef std::map<uint64, ByteBuffer*>               SplineMap;
typedef std::map<uint32, ScriptOverrideList* >      SpellOverrideMap;
typedef std::map<uint32, uint32>                    SpellOverrideExtraAuraMap;
typedef std::map<uint32, FactionReputation*>        ReputationMap;
typedef std::map<uint32, uint64>                    SoloSpells;
typedef std::map<SpellEntry*, pair<uint32, uint32> >StrikeSpellMap;
typedef std::map<uint32, OnHitSpell >               StrikeSpellDmgMap;
typedef std::map<uint32, PlayerSkill>				SkillMap;
typedef std::set<Player**>							ReferenceSet;
typedef std::map<uint32, PlayerCooldown>			PlayerCooldownMap;
typedef std::map<uint32, uint32>					SpellGroupColdown;

//#define OPTIMIZED_PLAYER_SAVING

class SERVER_DECL Player : public Unit
{
	friend class WorldSession;
	friend class Pet;
	friend class SkillIterator;

public:

	Player ( uint32 guid );
	~Player ( );

	bool DismissMount();
	bool RemoveShapeShift();
	bool DismissFootAndHead();

	SUNYOU_INLINE Guild * GetGuild() { return m_playerInfo->guild; }
	SUNYOU_INLINE GuildMember * GetGuildMember() { return m_playerInfo->guildMember; }
	SUNYOU_INLINE GuildRank * GetGuildRankS() { return m_playerInfo->guildRank; }

	void EventGroupFullUpdate();

	/************************************************************************/
	/* Skill System															*/
	/************************************************************************/

	void _AdvanceSkillLine(uint32 SkillLine, uint32 Count = 1);
	void _AddSkillLine(uint32 SkillLine, uint32 Current, uint32 Max);
	uint32 _GetSkillLineMax(uint32 SkillLine);
	uint32 _GetSkillLineCurrent(uint32 SkillLine, bool IncludeBonus = true);
	void _RemoveSkillLine(uint32 SkillLine);
	void _UpdateMaxSkillCounts();
	void _ModifySkillBonus(uint32 SkillLine, int32 Delta);
	void _ModifySkillBonusByType(uint32 SkillType, int32 Delta);
	bool _HasSkillLine(uint32 SkillLine);
	void RemoveSpellsFromLine(uint32 skill_line);
	void _RemoveAllSkills();
	void _RemoveLanguages();
	void _AddLanguages(bool All);
	void _AdvanceAllSkills(uint32 count);
	void _ModifySkillMaximum(uint32 SkillLine, uint32 NewMax);


	void RecalculateHonor();

	LfgMatch * m_lfgMatch;
	uint32 m_lfgInviterGuid;

	void EventTimeoutLfgInviter();

	std::map<uint32, uint32> m_Titles;
	bool InsertTitle(uint32 titleid, uint32 time_left = 0);
	void SendTitleList();
	bool HasTitle( uint32 titleid ) const;
	void QuickAddTitle( uint32 titleid );

protected:

	void _UpdateSkillFields();

	SkillMap m_skills;

	// COOLDOWNS
	PlayerCooldownMap m_cooldownMap[NUM_COOLDOWN_TYPES];
	uint32 m_globalCooldown;
	SpellGroupColdown m_mapSpellGroupColdown;

public:
	void Cooldown_AddStart(SpellEntry * pSpell);
	void Cooldown_Add(SpellEntry * pSpell, Item * pItemCaster);
	void Cooldown_AddItem(ItemPrototype * pProto, uint32 x);
	bool Cooldown_CanCast(SpellEntry * pSpell);
	bool Cooldown_CanCast(ItemPrototype * pProto, uint32 x);

protected:
	void _Cooldown_Add(uint32 Type, uint32 Misc, uint32 Time, uint32 SpellId, uint32 ItemId);
	void _LoadPlayerCooldowns(QueryResult * result);
	void _SavePlayerCooldowns(QueryBuffer * buf);
	void _SaveRefineItemContainer( QueryBuffer* buf );
	// END COOLDOWNS

public:

	bool ok_to_remove;
	uint64 m_spellIndexTypeTargets[NUM_SPELL_TYPE_INDEX];
	void OnLogin();//custom stuff on player login.
	void OnPlaytimeTrigger(uint32 Type, uint32 miscvalue1, uint32 miscvalue2);
	void PlaytimeTriggerCheck();
	uint32 m_playtimeTriggerId;
	uint32 m_playtimeTriggerTime;
	uint32 m_playtimeTriggerDiff;
	uint32 m_playtimeCounted;

	enum {
		GIFT_CATEGORY_NORMAL,
		GIFT_CATEGORY_BATTLE_GROUND,
		GIFT_CATEGORY_TEAM_ARENA,
		GIFT_CATEGORY_TITLE,
		GIFT_CATEGORY_INSTANCE,
		GIFT_CATEGORY_GAMBLE,
		GIFT_CATEGORY_GUILD_GIFT,
	};
	void GiveGift(uint32 Type, uint32 miscvalue1, uint32 miscvalue2, bool bSendMsg=true, int category = GIFT_CATEGORY_NORMAL);
	void GiveItem(uint32 itemid, ItemPrototype* it, uint32 cnt);
	bool GiveItemAndRemoveFromGround( Item* itm );
	void OnExpIdleTime();
	void RemoveSpellTargets(uint32 Type);
	void RemoveSpellIndexReferences(uint32 Type);
	void SetSpellTargetType(uint32 Type, Unit* target);
	void SendMeetingStoneQueue(uint32 DungeonId, uint8 Status);

	void AddToWorld();
	void AddToWorld(MapMgr * pMapMgr);
	void RemoveFromWorld();
	//bool Create ( WorldPacket &data );
	void Update( uint32 time );
// 	void BuildEnumData( WorldPacket * p_data );
    void BuildFlagUpdateForNonGroupSet(uint32 index, uint32 flag);
	std::string m_afk_reason;
	void SetAFKReason(std::string reason) { m_afk_reason = reason; };
	SUNYOU_INLINE const char* GetName() { return m_name.c_str(); }
	SUNYOU_INLINE std::string* GetNameString() { return &m_name; }
	void Die();
	//void KilledMonster(uint32 entry, const uint64 &guid);
	uint32 GiveXP(uint32 xp, const uint64 &guid, bool allowbonus, give_exp_type type );   // to stop rest xp being given
	uint32 CalcCastleExpBonus( Unit* victim, uint32 exp );
	void ModifyBonuses(uint32 type,int32 val);
	std::map<uint32, uint32> m_wratings;

	bool IsFlyMountSpell( uint32 spellid );
	bool IsFlyShiftSpell( uint32 spellid );
	bool IsInFlyMap();

	ArenaTeam * m_arenaTeams[NUM_ARENA_TEAM_TYPES];
	
    /************************************************************************/
    /* Taxi                                                                 */
    /************************************************************************/
    SUNYOU_INLINE TaxiPath*    GetTaxiPath() { return m_CurrentTaxiPath; }
    SUNYOU_INLINE bool         GetTaxiState() { return m_onTaxi; }
    const uint32&       GetTaximask( uint8 index ) const { return m_taximask[index]; }
    void                TaxiStart(TaxiPath* path, uint32 modelid, uint32 start_node);
    void                JumpToEndTaxiNode(TaxiPath * path);
    void                EventDismount(uint32 money, float x, float y, float z);
    void                EventTaxiInterpolate();

    SUNYOU_INLINE void         SetTaxiState    (bool state) { m_onTaxi = state; }
    SUNYOU_INLINE void         SetTaximask     (uint8 index, uint32 value ) { m_taximask[index] = value; }
    SUNYOU_INLINE void         SetTaxiPath     (TaxiPath *path) { m_CurrentTaxiPath = path; }
    SUNYOU_INLINE void         SetTaxiPos()	{m_taxi_pos_x = m_position.x; m_taxi_pos_y = m_position.y; m_taxi_pos_z = m_position.z;}
    SUNYOU_INLINE void         UnSetTaxiPos()	{m_taxi_pos_x = 0; m_taxi_pos_y = 0; m_taxi_pos_z = 0; }
 
	// Taxi related variables
	vector<TaxiPath*>   m_taxiPaths;
    TaxiPath*           m_CurrentTaxiPath;
    uint32              taxi_model_id;
	uint32              lastNode;
    uint32              m_taxi_ride_time;
    uint32              m_taximask[8];
    float               m_taxi_pos_x;
    float               m_taxi_pos_y;
    float               m_taxi_pos_z;
    bool                m_onTaxi;
	uint32				m_taxiMapChangeNode;
	bool				m_before_delete;

    /************************************************************************/
    /* Quests                                                               */
    /************************************************************************/
	bool HasQuests() 
	{
		for(int i = 0; i < QUEST_MAX_COUNT; ++i)
		{
			if(m_questlog[i] != 0)
				return true;
		}
		return false;
	}

	int32                GetOpenQuestSlot();
	QuestLogEntry*       GetQuestLogForEntry(uint32 quest);
	SUNYOU_INLINE QuestLogEntry*GetQuestLogInSlot(uint32 slot)  { return m_questlog[slot]; }
    SUNYOU_INLINE uint32        GetQuestSharer()                { return m_questSharer; }
    
    SUNYOU_INLINE void         SetQuestSharer(uint32 guid)     { m_questSharer = guid; }
    void                SetQuestLogSlot(QuestLogEntry *entry, uint32 slot);
    
	SUNYOU_INLINE void         PushToRemovedQuests(uint32 questid)	{ }//m_removequests.insert(questid);}
    void                AddToFinishedQuests(uint32 quest_id);
    void                EventTimedQuestExpire(Quest *qst, QuestLogEntry *qle, uint32 log_slot);
	
	bool                HasFinishedQuest(uint32 quest_id);
	bool                HasQuestForItem(uint32 itemid);
    bool                CanFinishQuest(Quest* qst);
	bool                HasQuestSpell(uint32 spellid);
	void                RemoveQuestSpell(uint32 spellid);
	bool                HasQuestMob(uint32 entry);
	bool                HasQuest(uint32 entry);
	void                RemoveQuestMob(uint32 entry);

    //Quest related variables
	uint32 m_questbarrier1[QUEST_MAX_COUNT];
    QuestLogEntry*      m_questlog[QUEST_MAX_COUNT];
	uint32 m_questbarrier2[QUEST_MAX_COUNT];
    std::set<uint32>    m_QuestGOInProgress;
    std::set<uint32>    m_removequests;
    std::set<uint32>    m_finishedQuests;
    uint32              m_questSharer;
    uint32              timed_quest_slot;
	std::set<uint32>    quest_spells;
	std::map<uint32, uint32>    quest_mobs;

	uint32				m_animalgroupspell;

    /************************************************************************/
    /* Stun Immobilize                                                      */
    /************************************************************************/
    void SetTriggerStunOrImmobilize(uint32 newtrigger,uint32 new_chance)
    {
        trigger_on_stun = newtrigger;
        trigger_on_stun_chance = new_chance;
    }
    void EventStunOrImmobilize(Unit *proc_target);

    
    void EventPortToGM(Player *p);
	SUNYOU_INLINE uint32 GetTeam() { return m_team; }
	SUNYOU_INLINE void SetTeam(uint32 t) { m_team = t; m_bgTeam=t; }
	SUNYOU_INLINE void ResetTeam() { m_team = myRace->team_id==7 ? 0 : 1; m_bgTeam=m_team; }

	SUNYOU_INLINE bool IsInFeralForm()
	{
		int s = GetShapeShift();
		if( s <= 0 )
			return false;

		//Fight forms that do not use player's weapon
		return ( s == 1 || s == 5 || s == 8 );
	}
	virtual void CalcDamage();
	uint32 GetMainMeleeDamage(uint32 AP_owerride); //i need this for windfury

    const uint64& GetSelection( ) const { return m_curSelection; }
	const uint64& GetTarget( ) const { return m_curTarget; }
	void SetSelection(const uint64 &guid) { m_curSelection = guid; }
	void SetTarget(const uint64 &guid) { m_curTarget = guid; }
	
    /************************************************************************/
    /* Spells                                                               */
    /************************************************************************/
	bool HasSpell(uint32 spell);
	bool HasDeletedSpell(uint32 spell);
	void smsg_InitialSpells();
	void addSpell(uint32 spell_idy, bool disp_new_learn = true);
	void removeSpellByHashName(uint32 hash);
	bool removeSpell(uint32 SpellID, bool MoveToDeleted, bool SupercededSpell, uint32 SupercededSpellID);

	void AttackAfterSpell(uint64 target_guid, SpellEntry* sp);

    // PLEASE DO NOT INLINE!
    void AddOnStrikeSpell(SpellEntry* sp, uint32 delay)
    {
        m_onStrikeSpells.insert( map< SpellEntry*, pair<uint32, uint32> >::value_type( sp, make_pair( delay, 0 ) ) );
    }
    void RemoveOnStrikeSpell(SpellEntry *sp)
    {
        m_onStrikeSpells.erase(sp);
    }
    void AddOnStrikeSpellDamage(uint32 spellid, uint32 mindmg, uint32 maxdmg)
    {
        OnHitSpell sp;
        sp.spellid = spellid;
        sp.mindmg = mindmg;
        sp.maxdmg = maxdmg;
        m_onStrikeSpellDmg[spellid] = sp;
    }
    void RemoveOnStrikeSpellDamage(uint32 spellid)
    {
        m_onStrikeSpellDmg.erase(spellid);
    }

    //Spells variables
    StrikeSpellMap      m_onStrikeSpells;
    StrikeSpellDmgMap   m_onStrikeSpellDmg;
    SpellOverrideMap    mSpellOverrideMap;
    SpellSet            mSpells;
    SpellSet            mDeletedSpells;
	SpellSet			mShapeShiftSpells;

	void AddShapeShiftSpell(uint32 id);
	void RemoveShapeShiftSpell(uint32 id);


	//Dynamic Object vector, removed these when player logout
	set<DynamicObject*> m_setDynamicObjs;
    /************************************************************************/
    /* Actionbar                                                            */
    /************************************************************************/
	void                setAction(uint8 button, uint32 action, uint8 type, uint8 misc);
	void                SendInitialActions();
    bool                m_actionsDirty;
	
    /************************************************************************/
    /* Factions                                                             */
    /************************************************************************/
	inline uint32 GetFactionId() const { return m_playerInfo->race; }
    // factions variables

    /************************************************************************/
    /* PVP                                                                  */
    /************************************************************************/
	SUNYOU_INLINE uint8 GetPVPRank()
	{
		return (uint8)((GetUInt32Value(PLAYER_BYTES_3) >> 24) & 0xFF);
	}
	SUNYOU_INLINE void SetPVPRank(int newrank)
	{
		SetUInt32Value(PLAYER_BYTES_3, ((GetUInt32Value(PLAYER_BYTES_3) & 0x00FFFFFF) | (uint8(newrank) << 24)));
	}

	SUNYOU_INLINE uint32 GetAnimal()
	{
		return GetUInt32Value( PLAYER_FIELD_SHENGXIAO );
	}
	SUNYOU_INLINE void SetAnimal(int newanimal)
	{
		SetUInt32Value(PLAYER_FIELD_SHENGXIAO, newanimal);
	}

    /************************************************************************/
    /* Groups                                                               */
    /************************************************************************/
	void                SetInviter(uint32 pInviter) { m_GroupInviter = pInviter; }
	SUNYOU_INLINE uint32       GetInviter() { return m_GroupInviter; }
	SUNYOU_INLINE bool         InGroup() { return (m_playerInfo->m_Group != NULL && !m_GroupInviter); }
	bool                IsGroupLeader();
	SUNYOU_INLINE int          HasBeenInvited() { return m_GroupInviter != 0; }
	SUNYOU_INLINE Group*       GetGroup() { return m_playerInfo ? m_playerInfo->m_Group : NULL; }
	SUNYOU_INLINE int8		   GetSubGroup() { return m_playerInfo->subGroup; }
    bool                IsGroupMember(Player *plyr);
// 	SUNYOU_INLINE bool         IsBanned()
// 	{
// 		if(m_banned)
// 		{
// 			if(m_banned < 100 || (uint32)getMSTime() < m_banned)
// 				return true;
// 		}
// 		return false;
// 	}
// 	SUNYOU_INLINE uint32       GetBanned() { return m_banned;}
// 	SUNYOU_INLINE void         SetBanned() { m_banned = 4;}
// 	SUNYOU_INLINE void         SetBanned(string Reason) { m_banned = 4; m_banreason = Reason;}
// 	SUNYOU_INLINE void         SetBanned(uint32 timestamp, string& Reason) { m_banned = timestamp; m_banreason = Reason; }
// 	SUNYOU_INLINE void         UnSetBanned() { m_banned = 0; }
// 	SUNYOU_INLINE string       GetBanReason() {return m_banreason;}

    /************************************************************************/
    /* Guilds                                                               */
    /************************************************************************/
	SUNYOU_INLINE  bool        IsInGuild() {return (m_uint32Values[PLAYER_GUILDID] != 0) ? true : false;}
	SUNYOU_INLINE uint32       GetGuildId() { return m_uint32Values[PLAYER_GUILDID]; }
	inline uint32 GetHostileGuild() const { return m_uint32Values[PLAYER_FIELD_HOSTILE_GUILDID]; }
	void SetHostileGuild( uint32 id );
	void                SetGuildId(uint32 guildId);
	SUNYOU_INLINE uint32       GetGuildRank() { return m_uint32Values[PLAYER_GUILDRANK]; }
	void                SetGuildRank(uint32 guildRank);
	uint32              GetGuildInvitersGuid() { return m_invitersGuid; }
	void                SetGuildInvitersGuid( uint32 guid ) { m_invitersGuid = guid; }
	void                UnSetGuildInvitersGuid() { m_invitersGuid = 0; }
  
    /************************************************************************/
    /* Duel                                                                 */
    /************************************************************************/
    void                RequestDuel(Player *pTarget);
	void                DuelBoundaryTest();
	void                EndDuel(uint8 WinCondition);
	void                DuelCountdown();
	void                SetDuelStatus(uint8 status) { m_duelStatus = status; }
	SUNYOU_INLINE uint8        GetDuelStatus() { return m_duelStatus; }
	void                SetDuelState(uint8 state) { m_duelState = state; }
	SUNYOU_INLINE uint8        GetDuelState() { return m_duelState; }
    // duel variables
    Player*             DuelingWith;

    /************************************************************************/
    /* Trade                                                                */
    /************************************************************************/
	void                SendTradeUpdate(void);
	void         ResetTradeVariables()
	{
		mTradeGold = 0;
		memset(&mTradeItems, 0, sizeof(Item*) * MAX_TRADE_SLOT);
		mTradeStatus = 0;
		mTradeTarget = 0;
		m_tradeSequence = 2;
	}
	
    /************************************************************************/
    /* Pets                                                                 */
    /************************************************************************/
	SUNYOU_INLINE void			SetSummon(Pet *pet) { m_Summon = pet; }
	SUNYOU_INLINE Pet*			GetSummon(void) { return m_Summon; }
	uint32						GeneratePetNumber(void);
	SUNYOU_INLINE void			SetMaxPetNumber(int Num ){m_PetNumberMax = Num;}
	SUNYOU_INLINE uint32	    GetMaxPetNumber() {return m_PetNumberMax;}
	void						RemovePlayerPet(uint32 pet_number);
	SUNYOU_INLINE void			AddPlayerPet(PlayerPet* pet, uint32 index) { m_Pets[index] = pet; }
	SUNYOU_INLINE PlayerPet*	GetPlayerPet(uint32 idx)
	{
		std::map<uint32, PlayerPet*>::iterator itr = m_Pets.find(idx);
		if(itr != m_Pets.end()) return itr->second;
		else
			return NULL;
	}
	void						SpawnPet(uint32 pet_number);
	void						DespawnPet();
	uint32						GetFirstPetNumber(void)
	{
		if(m_Pets.size() == 0) return 0;
		std::map<uint32, PlayerPet*>::iterator itr = m_Pets.begin();
		return itr->first;
	}
	inline uint32 GetPetCount() const { return m_Pets.size(); }
	SUNYOU_INLINE PlayerPet*	GetFirstPet(void) { return GetPlayerPet(GetFirstPetNumber()); }
	SUNYOU_INLINE void			SetStableSlotCount(uint8 count) { m_StableSlotCount = count; }
	SUNYOU_INLINE uint8			GetStableSlotCount(void) { return m_StableSlotCount; }
	uint32						GetUnstabledPetNumber(void);
	
	void						EventSummonPet(Pet *new_pet); //if we charmed or simply summoned a pet, this function should get called
	void						EventDismissPet(); //if pet/charm died or whatever happned we should call this function

    /************************************************************************/
    /* Item Interface                                                       */
    /************************************************************************/
	SUNYOU_INLINE ItemInterface* GetItemInterface() { return m_ItemInterface; } // Player Inventory Item storage
	SUNYOU_INLINE JingLianItemInterface* GetJingLianItemInterface() { return m_JingLianItemInterface; }
	SUNYOU_INLINE void         ApplyItemMods(Item *item, ui8 slot, bool apply,bool justdrokedown=false) {  _ApplyItemMods(item, slot, apply,justdrokedown); if( apply ) item->OnEquip(); else item->OnUnEquip(); }
    // item interface variables
    ItemInterface *     m_ItemInterface;
	JingLianItemInterface* m_JingLianItemInterface;
	
    /************************************************************************/
    /* Loot                                                                 */
    /************************************************************************/
	SUNYOU_INLINE const uint64& GetLootGUID() const { return m_lootGuid; }
	SUNYOU_INLINE void         SetLootGUID(const uint64 &guid) { m_lootGuid = guid; }
	void                SendLoot(uint64 guid,uint8 loot_type);
    // loot variables
    uint64              m_lootGuid;
    uint64              m_currentLoot;
    bool                bShouldHaveLootableOnCorpse;

    /************************************************************************/
    /* World Session                                                        */
    /************************************************************************/
	SUNYOU_INLINE WorldSession* GetSession() const { return m_session; }
	void SetSession(WorldSession *s) { m_session = s; }
	void SetBindPoint(float x, float y, float z, uint32 m, uint32 v) { m_bind_pos_x = x; m_bind_pos_y = y; m_bind_pos_z = z; m_bind_mapid = m; m_bind_zoneid = v;}
	void SendDelayedPacket(PakHead& packet, bool bDeleteOnSend);
	
	float offhand_dmg_mod;
	float GetSpellTimeMod(uint32 id);
	int GetSpellDamageMod(uint32 id);
	int32 GetSpellManaMod(uint32 id);
	
	// Talents
	// These functions build a specific type of A9 packet
	uint32 __fastcall BuildCreateUpdateBlockForPlayer( ByteBuffer *data, Player *target );
	void DestroyForPlayer( Player *target ) const;
	void SetTalentHearthOfWildPCT(int value){hearth_of_wild_pct=value;}
	void EventTalentHearthOfWildChange(bool apply);
	
	std::list<LoginAura> loginauras;

    std::set<uint32> OnMeleeAuras;

    /************************************************************************/
    /* Player loading and savings                                           */
    /* Serialize character to db                                            */
    /************************************************************************/
	void SaveToDB(bool periodic_save = false);
	void SaveAuras(stringstream&, bool periodic_save = false);
	bool LoadFromDB(uint32 guid);
	void LoadFromDBProc(QueryResultVector & results);
	void LoadMailBoxProc( QueryResultVector& results );

	void LoadFeeInfoProc( QueryResultVector& results );
	void LoadRechargeableCardInfoProc( QueryResultVector& results );
	void LoadCreditCardInfoProc( QueryResultVector& results );

	void LoadNamesFromDB(uint32 guid);
	bool m_FirstLogin;

    /************************************************************************/
    /* Death system                                                         */
    /************************************************************************/
	void SpawnCorpseBody();
	void SpawnCorpseBones();
	void CreateCorpse();
	void KillPlayer();
	void ResurrectPlayer();
	void ResurrectPlayerOnSite();
	void ResurrectPlayerForceLocation( float x, float y, float z, float o ); 
	void BuildPlayerRepop();
	void DeathDurabilityLoss(double percent);
	
    /************************************************************************/
    /* Movement system                                                      */
    /************************************************************************/
	void SetMovement(uint8 pType, uint32 flag);
	void SetPlayerSpeed(uint8 SpeedType, float value);
	float GetPlayerSpeed(){return m_runSpeed;}
	uint8 m_currentMovement;
	bool m_isMoving;
	//Invisibility stuff
	bool m_isGmInvisible;
	bool m_bIsJumping;
	
    /************************************************************************/
    /* Channel stuff                                                        */
    /************************************************************************/
	void JoinedChannel(Channel *c);
	void LeftChannel(Channel *c);
	void CleanupChannels();
	//Attack stuff
	void EventAttackStart();
	void EventAttackStop();
	void EventAttackUpdateSpeed() { }
	void EventDeath();
	//Note:ModSkillLine -> value+=amt;ModSkillMax -->value=amt; --wierd
	float GetSkillUpChance(uint32 id);
	//SUNYOU_INLINE std::list<struct skilllines>getSkillLines() { return m_skilllines; }
	float SpellCrtiticalStrikeRatingBonus;
	float SpellHasteRatingBonus;
	void UpdateAttackSpeed();
	void UpdateChances();
	void UpdateStats();
	void UpdateHit(int32 hit);
   
	bool canCast(SpellEntry *m_spellInfo);
	SUNYOU_INLINE float GetBlockFromSpell() { return m_blockfromspell; }
	SUNYOU_INLINE float GetSpellCritFromSpell() { return m_spellcritfromspell; }
	SUNYOU_INLINE float GetHitFromMeleeSpell() { return m_hitfrommeleespell; }
	SUNYOU_INLINE float GetHitPCTFromSpell() { return m_hitfpctfromspell; }
	SUNYOU_INLINE float GetHitFromSpell() { return m_hitfromspell; }
	SUNYOU_INLINE float GetParryFromSpell() { return m_parryfromspell; }
	SUNYOU_INLINE float GetDodgeFromSpell() { return m_dodgefromspell; }
	void SetBlockFromSpell(float value) { m_blockfromspell = value; }
	void SetSpellCritFromSpell(float value) { m_spellcritfromspell = value; }
	void SetParryFromSpell(float value) { m_parryfromspell = value; }
	void SetDodgeFromSpell(float value) { m_dodgefromspell = value; }
	void SetHitFromMeleeSpell(float value) { m_hitfrommeleespell = value; }
	void SetHitPCTFromSpell(float value) { m_hitfpctfromspell = value; }
	void SetHitFromSpell(float value) { m_hitfromspell = value; }
	SUNYOU_INLINE uint32 GetHealthFromSpell() { return m_healthfromspell; }
	SUNYOU_INLINE uint32 GetManaFromSpell() { return m_manafromspell; }
	void SetHealthFromSpell(uint32 value) { m_healthfromspell = value;}
	void SetManaFromSpell(uint32 value) { m_manafromspell = value;}
	uint32 CalcTalentResetCost(uint32 resetnum);
	void SendTalentResetConfirm();
	void SendPetUntrainConfirm();
	uint32 GetTalentResetTimes() { return m_talentresettimes; }
	SUNYOU_INLINE void SetTalentResetTimes(uint32 value) { m_talentresettimes = value; }
	void SetPlayerStatus(uint8 pStatus) { m_status = pStatus; }
	SUNYOU_INLINE uint8 GetPlayerStatus() { return m_status; }
	const float& GetBindPositionX( ) const { return m_bind_pos_x; }
	const float& GetBindPositionY( ) const { return m_bind_pos_y; }
	const float& GetBindPositionZ( ) const { return m_bind_pos_z; }
	const uint32& GetBindMapId( ) const { return m_bind_mapid; }
	const uint32& GetBindZoneId( ) const { return m_bind_zoneid; }
	SUNYOU_INLINE uint32 GetShapeShift()
	{
		return GetUInt32Value(UNIT_FIELD_FORMDISPLAYID);//GetByte(UNIT_FIELD_BYTES_2,3);
	}

	
	void delayAttackTimer(int32 delay)
	{
		if(!delay)
			return;

		if( delay < 0 && m_attackTimer < (uint32)(-delay) )
			m_attackTimer = 0;
		else
			m_attackTimer += delay;

		if( delay < 0 && m_attackTimer_1 < (uint32)(-delay) )
			m_attackTimer_1 = 0;
		else
			m_attackTimer_1 += delay;
	}

	int32		m_incombattimeleft;
	int32		m_inarmedtimeleft;
	int32		m_comatbat_xp_time;
	bool		m_bTriggerXP;		//下个技能触发xp伤害
	uint32		m_entercombat_time;

	bool		m_bEXPIdle;
	uint32		m_EXPIdleTime;
	
	uint32		m_ExtraEXPRatio;
	uint32		m_ExtraEXPRatioQuest;
	uint32		m_ExtraHonorRatio;
	uint32		m_ExtraHonorRatioQuest;

	void SetShapeShift(ui32 ss);

	uint32 m_furorChance;

    //Showing Units WayPoints
	AIInterface* waypointunit;
	
	uint32 m_nextSave;
	//Tutorials
	uint64 GetTutorialInt(uint32 intId );
	void SetTutorialInt(uint32 intId, uint32 value);
	//Base stats calculations
	//void CalcBaseStats();
	// Rest
	uint32 SubtractRestXP(uint32 amount);
	void AddCalculatedRestXP(uint32 seconds);
	void ApplyPlayerRestState(bool apply);
	void UpdateRestState();
	bool m_noFallDamage;
	float z_axisposition;
	int32 m_safeFall;
	// Gossip
	GossipMenu* CurrentGossipMenu;
	void CleanupGossipMenu();
	void Gossip_Complete();
	int m_lifetapbonus;
	uint32 m_lastShotTime;
	
	bool m_bUnlimitedBreath;
	uint32 m_UnderwaterTime;
	uint32 m_UnderwaterState;
	uint32 m_SwimmingTime;
	uint32 m_BreathDamageTimer;
	// Visible objects
	bool CanSee(Object* obj);
	SUNYOU_INLINE bool IsVisible(Object* pObj) { return !(m_visibleObjects.find(pObj) == m_visibleObjects.end()); }
	void AddInRangeObject(Object* pObj);
	void OnRemoveInRangeObject(Object* pObj);
	void ClearInRangeSet();
	SUNYOU_INLINE void AddVisibleObject(Object* pObj) { m_visibleObjects.insert(pObj); }
	SUNYOU_INLINE void RemoveVisibleObject(Object* pObj) { m_visibleObjects.erase(pObj); }
	SUNYOU_INLINE void RemoveVisibleObject(InRangeSet::iterator itr) { m_visibleObjects.erase(itr); }
	SUNYOU_INLINE InRangeSet::iterator FindVisible(Object * obj) { return m_visibleObjects.find(obj); }
	SUNYOU_INLINE void RemoveIfVisible(Object * obj)
	{
		InRangeSet::iterator itr = m_visibleObjects.find(obj);
		if(itr == m_visibleObjects.end())
			return;

		m_visibleObjects.erase(obj);
		PushOutOfRange(obj->GetNewGUID());
	}

	SUNYOU_INLINE bool GetVisibility(Object * obj, InRangeSet::iterator *itr)
	{
		*itr = m_visibleObjects.find(obj);
		return ((*itr) != m_visibleObjects.end());
	}

	SUNYOU_INLINE InRangeSet::iterator GetVisibleSetBegin() { return m_visibleObjects.begin(); }
	SUNYOU_INLINE InRangeSet::iterator GetVisibleSetEnd() { return m_visibleObjects.end(); }
	
	//Transporters
	bool m_lockTransportVariables;
	uint64 m_TransporterGUID;
	float m_TransporterX;
	float m_TransporterY;
	float m_TransporterZ;
	float m_TransporterO;
	float m_TransporterUnk;
	// Misc
	void EventCannibalize(uint32 amount);
	void EventReduceDrunk(bool full);
	bool m_AllowAreaTriggerPort;
	void EventAllowTiggerPort(bool enable);
	int32 m_rangedattackspeedmod;
	int32 m_meleeattackspeedmod;
	uint32 m_modblockabsorbvalue;
	uint32 m_modblockvaluefromspells;
	void SendInitialLogonPackets();
	void Reset_Spells();
	void Reset_Talents();
	void Reset_ToLevel1();
	uint32 m_bgEntryPointMap;
	float m_bgEntryPointX;	
	float m_bgEntryPointY;
	float m_bgEntryPointZ;
	float m_bgEntryPointO;
	int32 m_bgEntryPointInstance;
	bool m_bgHasFlag;
	bool m_bgIsQueued;
	uint32 m_bgQueueType;
	uint32 m_bgQueueInstanceId;
	void EventRepeatSpell();
	void EventCastRepeatedSpell(uint32 spellid, Unit *target);
	int32 CanShootRangedWeapon(uint32 spellid, Unit *target, bool autoshot);
	uint32 m_AutoShotDuration;
	uint32 m_AutoShotAttackTimer;
	bool m_onAutoShot;
	uint64 m_AutoShotTarget;
	SpellEntry *m_AutoShotSpell;
	void EventActivateGameObject(GameObject* obj);
	void EventDeActivateGameObject(GameObject* obj);
	void UpdateNearbyGameObjects();
	
	void CalcResistance(uint32 type);
	SUNYOU_INLINE float res_M_crit_get(){return m_resist_critical[0];}
	SUNYOU_INLINE void res_M_crit_set(float newvalue){m_resist_critical[0]=newvalue;}
	SUNYOU_INLINE float res_R_crit_get(){return m_resist_critical[1];}
	SUNYOU_INLINE void res_R_crit_set(float newvalue){m_resist_critical[1]=newvalue;}
	int32 FlatResistanceModifierPos[7];
	int32 FlatResistanceModifierNeg[7];
	int32 BaseResistanceModPctPos[7];
	int32 BaseResistanceModPctNeg[7];
	int32 BaseSpellResistanceModPctPos[7];
	int32 BaseSpellResistanceModPctNeg[7];

	int32 ResistanceModPctPos[7];
	int32 ResistanceModPctNeg[7];
	
	int MaxPowerModPctPos;
	int MaxPowerModPctNeg;

	int MaxHealthModPctPos;
	int MaxHealthModPctNeg;

	float m_resist_critical[2];//when we are a victim we can have talents to decrease chance for critical hit. This is a negative value and it's added to critchances
	float m_resist_hit[3]; // 0 = melee; 1= ranged; 2=spells
	float SpellDmgDoneByAttribute[6][7];
	float SpellHealDoneByAttribute[6][7];
	uint32 m_modphyscritdmgPCT;
	uint32 m_RootedCritChanceBonus; // Class Script Override: Shatter

	uint32 m_ModInterrMRegenPCT;
	int32 m_ModInterrMRegen;
	float m_RegenManaOnSpellResist;
	uint32 m_casted_amount[7]; //Last casted spells amounts. Need for some spells. Like Ignite etc. DOesn't count HoTs and DoTs. Only directs
	
	uint32 FlatStatModPos[6];
	uint32 FlatStatModNeg[6];
	uint32 StatModPctPos[6];
	uint32 StatModPctNeg[6];
	uint32 TotalStatModPctPos[6];
	uint32 TotalStatModPctNeg[6];
	int32 IncreaseDamageByType[12]; //mod dmg by creature type
	float IncreaseDamageByTypePCT[12];
	float IncreaseCricticalByTypePCT[12];
	int32 DetectedRange;
	float PctIgnoreRegenModifier;
	float PctRefineChanceModifier;
	uint32 m_retainedrage;
/*	
	union {
		float mRatingToPct[37];
		uint32 mRatingToPoint[37]; //block, skill.. cant be decimal values
	};
*/
	SUNYOU_INLINE uint32* GetPlayedtime() { return m_playedtime; };
	void CalcStat(uint32 t);
	float CalcRating(uint32 t);
	void RecalcAllRatings();
	void RegenerateMana(bool is_interrupted);
	void RegenerateHealth(bool inCombat);
	
    uint32 SoulStone;
	uint32 SoulStoneReceiver;
	void removeSoulStone();

    SUNYOU_INLINE uint32 GetSoulStoneReceiver(){return SoulStoneReceiver;}
    SUNYOU_INLINE void SetSoulStoneReceiver(uint32 StoneGUID){SoulStoneReceiver = StoneGUID;}
    SUNYOU_INLINE uint32 GetSoulStone(){return SoulStone;}
    SUNYOU_INLINE void SetSoulStone(uint32 StoneID){SoulStone = StoneID;}

	bool bReincarnation;

	map<uint32, WeaponModifier> damagedone;
	std::list<WeaponModifier> tocritchance;
	map<uint32, float> hitchance;
	uint32 Seal;
	uint32 judgespell;
	bool cannibalize;
	uint8 cannibalizeCount;
	int32 rageFromDamageDealt;
	// GameObject commands
	GameObject *m_GM_SelectedGO;
	
#ifndef CLUSTERING
	void _Relocate(uint32 mapid,const LocationVector & v, bool sendpending, bool force_new_world, uint32 instance_id);
#else
	void RelocateCallback(uint32 instance_were_going_to);
#endif
	void AddItemsToWorld();
	void RemoveItemsFromWorld();
	
	uint32 m_ShapeShifted;
	uint32 m_MountSpellId;
    
	SUNYOU_INLINE bool IsMounted() {return m_MountSpellId || GetUInt64Value(PLAYER_HEAD) || GetUInt64Value(PLAYER_FOOT); }
	
    bool bHasBindDialogOpen;
	bool bGMTagOn;
	uint32 TrackingSpell;
	void _EventCharmAttack();
	void _Kick();
	void Kick(uint32 delay = 0);
	void SoftDisconnect();
	uint32 m_KickDelay;
	Unit * m_CurrentCharm;
	Transporter * m_CurrentTransporter;
	
	Object * GetSummonedObject () {return m_SummonedObject;};
	void SetSummonedObject (Object * t_SummonedObject) {m_SummonedObject = t_SummonedObject;};
	uint32 roll;

	void ClearCooldownsOnLine(uint32 skill_line, uint32 called_from);
	void ResetAllCooldowns();
	void ClearCooldownForSpell(uint32 spell_id);
	void SendPetList();
	bool IsHavePet(uint32 PetEntry);

	bool bProcessPending;
	void PushUpdateData(ByteBuffer *data, uint32 updatecount, bool send=true);
    void PushCreationData(ByteBuffer *data, uint32 updatecount, bool send=true);
	void PushOutOfRange(ui64 guid);
	void ProcessPendingUpdates( bool SendCurrentMove = true );
	bool __fastcall CompressAndSendUpdateBuffer(uint32 size, const uint8* update_buffer);
	void ClearAllPendingUpdates();
	
	uint32 GetArmorProficiency() { return armor_proficiency; }
	uint32 GetWeaponProficiency() { return weapon_proficiency; }

	void ResetHeartbeatCoords();

	LocationVector _lastHeartbeatPosition;
	float _lastHeartbeatV; // velocity
	uint32 _startMoveTime;	// time
	int32 _heartbeatDisable;
	bool _speedChangeInProgress;

	void AddSplinePacket(uint64 guid, ByteBuffer* packet);
	ByteBuffer* GetAndRemoveSplinePacket(uint64 guid);
	void ClearSplinePackets();
	bool ExitInstance();
	void SaveEntryPoint(uint32 mapId);
	bool CooldownCheat;
	bool CastTimeCheat;
	bool GodModeCheat;
	bool PowerCheat;
	bool FlyCheat;
	void ZoneUpdate(uint32 ZoneId);
	SUNYOU_INLINE uint32 GetAreaID() { return m_AreaID; }
	void SetAreaID(uint32 area) { m_AreaID = area; }
	
	
	std::string Lfgcomment;
	uint16 LfgDungeonId[3];
	uint8 LfgType[3];
	uint16 LfmDungeonId;
	uint8 LfmType;
	bool m_Autojoin;
	bool m_AutoAddMem;
	void StopMirrorTimer(uint32 Type);
	BGScore m_bgScore;
	uint32 m_bgTeam;
	void UpdateChanceFields();
	//Honor Variables
	time_t m_fallDisabledUntil;
	uint32 m_honorToday;
	uint32 m_honorYesterday;
	
	void RolloverHonor();
	uint32 m_honorPoints;
	uint32 m_honorRolloverTime;
	uint32 m_killsToday;
	uint32 m_killsYesterday;
	uint32 m_killsLifetime;
	uint32 m_arenaPoints;
	bool m_honorless;
	uint32 m_lastSeenWeather;
	set<Object*> m_visibleFarsightObjects;
	void EventTeleport(uint32 mapid, float x, float y, float z);
	void ApplyLevelInfo(LevelInfo* Info, uint32 Level);
	void BroadcastMessage(const char* Format, ...);
	map<uint32, set<uint32> > SummonSpells;
	map<uint32, PetSpellMap*> PetSpells;
	void AddSummonSpell(uint32 Entry, uint32 SpellID);
	void RemoveSummonSpell(uint32 Entry, uint32 SpellID);
	set<uint32>* GetSummonSpells(uint32 Entry);
	LockedQueue<PakHead> delayedPackets;
	set<Player *> gmTargets;
	uint32 m_UnderwaterMaxTime;
	uint32 m_UnderwaterLastDmg;
	SUNYOU_INLINE void setMyCorpse(Corpse * corpse) { myCorpse = corpse; }
	SUNYOU_INLINE Corpse * getMyCorpse() { return myCorpse; }
	bool bCorpseCreateable;

	bool m_bReliveItem;//	原地复活
	uint32 m_resurrectHealth, m_resurrectMana;
	uint32 m_resurrectMapId;
	float m_resurrectPosX,m_resurrectPosY,m_resurrectPosZ;
	uint32 m_resurrectInstanceID;
	uint32 resurrector;

	void ResetDeathClock(){ m_deathtime = getMSTime(); }
	time_t GetDeathClock(){ return m_deathtime; }
	ui32 m_deathtime;

	ui32 m_itemupdate;

	bool blinked;
	uint16 m_speedhackChances;
	uint32 m_explorationTimer;
	// DBC stuff
	CharRaceEntry * myRace;
	CharClassEntry * myClass;
	Unit * linkTarget;
	bool stack_cheat;
	bool triggerpass_cheat;
	bool SafeTeleport(uint32 MapID, uint32 InstanceID, float X, float Y, float Z, float O);
	bool SafeTeleport(uint32 MapID, uint32 InstanceID, const LocationVector & vec);
	void SafeTeleport(MapMgr * mgr, const LocationVector & vec);
	void EjectFromInstance();
	bool raidgrouponlysent;
	
	void EventSafeTeleport(uint32 MapID, uint32 InstanceID, LocationVector vec)
	{
		SafeTeleport(MapID, InstanceID, vec);
	}

	/*****************
	  PVP Stuff
	******************/
	uint32 m_pvpTimer;
	
	//! Is PVP flagged?
	SUNYOU_INLINE bool IsPvPFlagged() { return HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PVP); }
	SUNYOU_INLINE void SetPvPFlag()
	{
		// reset the timer as well..
		StopPvPTimer();
		SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PVP);
	}
	//! Removal
	SUNYOU_INLINE void RemovePvPFlag()
	{
		StopPvPTimer();
		RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PVP);
	}
	//! Do this on /pvp off
	SUNYOU_INLINE void ResetPvPTimer();
	//! Stop the timer for pvp off
	SUNYOU_INLINE void StopPvPTimer() { m_pvpTimer = 0; }
	
	//! Called at login to add the honorless buff, etc.
	void LoginPvPSetup();
	//! Update our pvp area (called when zone changes)
	void UpdatePvPArea();
	//! PvP Toggle (called on /pvp)
	void PvPToggle();

	SUNYOU_INLINE uint32 LastHonorResetTime() const { return m_lastHonorResetTime; }
	SUNYOU_INLINE void LastHonorResetTime(uint32 val) { m_lastHonorResetTime = val; }
	uint32 OnlineTime;
	bool tutorialsDirty;
	LevelInfo * lvlinfo;
	void CalculateBaseStats();
	uint32 load_health;
	uint32 load_mana;
	void CompleteLoading();
	void BetaBonus();
	static std::map<uint32, uint32> s_bonus_acct;
	set<SpellEntry *> castSpellAtLogin;
	void OnPushToWorld();
	void OnPrePushToWorld();
	void OnWorldPortAck();
	uint32 m_TeleportState;
	set<Unit*> visiblityChangableSet;
	bool m_beingPushed;
	bool CanSignCharter(Charter * charter, Player * requester);
	Charter * m_charters[NUM_CHARTER_TYPES];
	uint32 flying_aura;
	stringstream LoadAuras;
	bool resend_speed;
	bool rename_pending;
	uint32 iInstanceType;
	SUNYOU_INLINE void SetName(string& name) { m_name = name; }
	// spell to (delay, last time)
	
	uint32 m_speedChangeCounter;

	void SendAreaTriggerMessage(const char * message, ...);
        
	// Trade Target
	//Player *getTradeTarget() {return mTradeTarget;};

	Player * GetTradeTarget();
	Item *getTradeItem(uint32 slot) {return mTradeItems[slot];};
        
	// Water level related stuff (they are public because they need to be accessed fast)
	// Nose level of the character (needed for proper breathing)
	float m_noseLevel;

	/* Mind Control */
	void Possess(Unit * pTarget);
	void UnPossess();

	/* Last Speeds */
	SUNYOU_INLINE void UpdateLastSpeeds()
	{
		m_lastRunSpeed = m_runSpeed;
		m_lastRunBackSpeed = m_backWalkSpeed;
		m_lastSwimSpeed = m_swimSpeed;
		m_lastRunBackSpeed = m_backSwimSpeed;
		m_lastFlySpeed = m_flySpeed;
	}

	void RemoteRevive();
	void ResetSpeedHack();
	void DelaySpeedHack(uint32 ms);

	LocationVector m_last_group_position;
	int32 m_rap_mod_pct;
	void SummonRequest( const char* Requestor, uint32 RequestorID, uint32 MapID, uint32 InstanceID, const LocationVector & Position );
	uint8 m_lastMoveType;
#ifdef OPTIMIZED_PLAYER_SAVING
	void save_LevelXP();
	void save_Skills();
	void save_ExploreData();
	void save_Gold();
	void save_Misc();
	void save_PositionHP();
	void save_BindPosition();
	void save_Honor();
	void save_EntryPoint();
	void save_Taxi();
	void save_Transporter();
	void save_Spells();
	void save_Actions();
	void save_Reputation();
	void save_Auras();
	void save_InstanceType();
	void save_Zone();
	void save_PVP();
#endif

#ifdef CLUSTERING
	void EventRemoveAndDelete();
	void PackPlayerData(ByteBuffer & data);
	bool UnpackPlayerData(ByteBuffer & data);
#endif

	Creature * m_tempSummon;
	bool m_deathVision;
	SpellEntry * last_heal_spell;
	LocationVector m_sentTeleportPosition;

	void FullHPMP();
	uint32 m_arenateaminviteguid;

    /************************************************************************/
    /* Spell Packet wrapper Please keep this separated                      */
    /************************************************************************/
    void SendLevelupInfo(uint32 level, uint32 Hp, uint32 Mana, uint32 Stat0, uint32 Stat1, uint32 Stat2, uint32 Stat3, uint32 Stat4, uint32 Stat5);
    void SendLogXPGain(uint64 guid, uint32 NormalXP, uint32 RestedXP, bool type);
    void SendEnvironmentalDamageLog(const uint64 & guid, uint8 type, uint32 damage);
	void SendWorldStateUpdate(uint32 WorldState, uint32 Value);
	void SendCastResult(uint32 SpellId, uint8 ErrorMessage, uint8 MultiCast, uint32 Extra);
	void Gossip_SendPOI(float X, float Y, uint32 Icon, uint32 Flags, uint32 Data, const char* Name);
    /************************************************************************/
    /* End of SpellPacket wrapper                                           */
    /************************************************************************/

	class Mailbox* m_mailBox;
	bool m_waterwalk;
	bool m_setwaterwalk;
	bool m_setflycheat;
	uint64 m_areaSpiritHealer_guid;
	bool m_finishingmovesdodge;
	uint32 iActivePet;
	SUNYOU_INLINE bool IsAttacking() {return m_attacking; }

	static void InitVisibleUpdateBits();
	static UpdateMask m_visibleUpdateMask;

	void CopyAndSendDelayedPacket(PakHead& pak);
	void PartLFGChannel();

protected:
	LocationVector m_ResurrectForceLocation;
	LocationVector m_summonPos;
	uint32 m_summonInstanceId;
	uint32 m_summonMapId;
	uint32 m_summoner;

	void _SetCreateBits(UpdateMask *updateMask, Player *target) const;
	void _SetUpdateBits(UpdateMask *updateMask, Player *target) const;

	/* Update system components */
	//ByteBuffer bUpdateBuffer;
    //ByteBuffer bCreationBuffer;
	//uint32 mUpdateCount;
    //uint32 mCreationCount;
	//uint32 mOutOfRangeIdCount;
	//ByteBuffer mOutOfRangeIds;
	SplineMap _splineMap;
	uint32 m_nLastProcessMsgBufferTime;
	MSG_S2C::stObjectUpdate m_ObjectUpdateMsg;
	/* End update system */

	void _LoadTutorials(QueryResult * result);
	void _SaveTutorials(QueryBuffer * buf);
	void _SaveInventory(bool firstsave);
	void _SaveQuestLogEntry(QueryBuffer * buf);
	void _LoadQuestLogEntry(QueryResult * result);

	void _LoadPet(QueryResult * result);
	void _LoadPetNo();
	void _LoadPetSpells(QueryResult * result);
public:
	void _SavePet(QueryBuffer * buf);
	void ApplyTitleMods( stPlayerTitle* pTitle, bool apply );
	void SendSystemMail( uint32 item_entry1, uint32 item_count1, uint32 item_entry2, uint32 item_count2, uint32 money, uint8 isyuanbao, const char* str_subject, const char* str_body, build_string_t subject, build_string_t body, const char* arg1 = NULL, const char* arg2 = NULL, const char* arg3 = NULL, const char* arg4 = NULL, const char* arg5 = NULL );

protected:
	void _SavePetSpells(QueryBuffer * buf);
	void _ApplyItemMods( Item* item, ui8 slot, bool apply, bool justdrokedown = false, bool skip_stat_apply = false );
	void _EventAttack( bool offhand );
	void _EventExploration();

	// Water level related stuff
	void SetNoseLevel();

	/************************************************************************/
	/* Trade																*/
	/************************************************************************/
	Item* mTradeItems[MAX_TRADE_SLOT];
	uint32 mTradeGold;
	uint32 mTradeTarget;
	uint32 mTradeStatus;

    /************************************************************************/
    /* Player Class systems, info and misc things                           */
    /************************************************************************/
    PlayerCreateInfo *info;
	uint32      m_AttackMsgTimer;	// "too far away" and "wrong facing" timer
	bool        m_attacking;
	std::string m_name;	// max 21 character name
	uint64      m_Tutorials[8];

    // Character Ban
	//uint32      m_banned;
	//string      m_banreason;
	uint32      m_AreaID;
	Pet*        m_Summon;
	uint32      m_PetNumberMax;
	std::map<uint32, PlayerPet*> m_Pets;
	
    uint32      m_invitersGuid; // It is guild inviters guid ,0 when its not used
    

    // bind
	float m_bind_pos_x;
	float m_bind_pos_y;
	float m_bind_pos_z;
	uint32 m_bind_mapid;
	uint32 m_bind_zoneid;

	std::set<ui8> m_itemAlreadyApplied;
	std::list<ItemSet> m_itemsets;
	//Duel
	uint32 m_duelCountdownTimer;
	uint8 m_duelStatus;
	uint8 m_duelState;
	// Rested State Stuff
	uint32 m_timeLogoff;
	// Played time
	uint32 m_playedtime[3];//0=当前等级游戏时间,1=所有游戏时间,2=当前游戏时间
	uint8 m_isResting;
	uint8 m_restState;
	uint32 m_restAmount;
	//combat mods
	float m_blockfromspell;
	float m_blockfromspellPCT;
	float m_critfromspell;
	float m_spellcritfromspell;
	float m_dodgefromspell;
	float m_parryfromspell;
	float m_hitfromspell;
	float m_hitfpctfromspell;
	float m_hitfrommeleespell;
	//stats mods
	uint32 m_healthfromspell;
	uint32 m_manafromspell;
	uint32 m_healthfromitems;
	uint32 m_manafromitems;
	
	uint32 armor_proficiency;
	uint32 weapon_proficiency;
	// Talents
	uint32 m_talentresettimes;
	// STATUS
	uint8 m_status;
	// guid of current target
	uint64 m_curTarget;
	// guid of current selection
	uint64 m_curSelection;
	// 
	uint64 m_curSwingTarget;
	// Raid
	uint8 m_targetIcon;
	//Player Action Bar
	ActionButton mActions[PLAYER_ACTION_BUTTON_COUNT];
	// Pointer to this char's game client
	WorldSession *m_session;
	// Channels
	std::set<Channel*> m_channels;
	// Visible objects
	std::set<Object*> m_visibleObjects;
	// Groups/Raids
	uint32 m_GroupInviter;
	uint8 m_StableSlotCount;

    // Fishing related
	Object *m_SummonedObject;

    // other system
	Corpse *    myCorpse;

	uint32      m_lastHonorResetTime;
	uint32      _fields[PLAYER_END];
	uint32	    trigger_on_stun;        //bah, warrior talent but this will not get triggered on triggered spells if used on proc so i'm forced to used a special variable
	uint32	    trigger_on_stun_chance; //also using this for mage "Frostbite" talent
	int			hearth_of_wild_pct;		//druid hearth of wild talent used on shapeshifting. We eighter know what is last talent level or memo on learn

	uint32 m_team;
	float       m_lastRunSpeed;
	float       m_lastRunBackSpeed;
	float       m_lastSwimSpeed;
	float       m_lastBackSwimSpeed;
	float       m_lastFlySpeed;

	uint32 m_mountCheckTimer;
	void RemovePendingPlayer();
public:
	bool		m_bRecvLoadingOK;
#ifdef ENABLE_COMPRESSED_MOVEMENT
	void EventDumpCompressedMovement();
	void AppendMovementData(uint32 op, uint32 sz, const uint8* data);
	ByteBuffer m_movementBuffer;
#endif

	void addDeletedSpell(uint32 id) { mDeletedSpells.insert( id ); }

	map<uint32, uint32> m_forcedReactions;

	uint32 m_speedhackCheckTimer;
#ifdef COLLISION
	uint32 m_flyhackCheckTimer;
	void _FlyhackCheck();
#endif
	void _SpeedhackCheck(uint32 mstime);		// save a call to getMSTime() yes i am a stingy bastard

	bool m_passOnLoot;
	uint32 m_tradeSequence;
	bool m_changingMaps;

	/************************************************************************/
	/* SOCIAL                                                               */
	/************************************************************************/
private:
	/* we may have multiple threads on this(chat) - burlex */
	map<uint32, std::pair<char*, char*> > m_friends;
	set<uint32> m_ignores;
	set<uint32> m_hasFriendList;

	void Social_SendFriendList(uint32 flag);

	void Social_SetNote(uint32 guid, const char * note);

public:
	int m_spellDamageModifier;
	void Social_AddFriend(const char * name, const char * note);
	void Social_RemoveFriend(uint32 guid);
	
	void Social_AddIgnore(const char * name);
	void Social_RemoveIgnore(uint32 guid);
	bool Social_IsIgnoring(PlayerInfo * m_info);
	bool Social_IsIgnoring(uint32 guid);

	void Social_TellFriendsOnline();
	void Social_TellFriendsOffline();

	/************************************************************************/
	/* end social                                                           */
	/************************************************************************/

	PlayerInfo * m_playerInfo;
	uint32 m_outStealthDamageBonusPct;
	uint32 m_outStealthDamageBonusPeriod;
	uint32 m_outStealthDamageBonusTimer;

	int16 m_vampiricEmbrace;
	int16 m_vampiricTouch;
	void VampiricSpell(uint32 dmg, Unit* pTarget);

	//added by Gui for fee
	void UpdatePointsAndExpireProc( QueryResultVector& v );
	void SendLogoutInFewMinutesMsg();
	void AccountExpire();

public:
	enum add_card_type {
		ADD_CARD_GM_DONATE,
		ADD_CARD_TRADE,
		ADD_CARD_CREDIT,
		ADD_CARD_NONE,
	};
	int GetCardPoints() const;
	bool SpendPoints( int n, const char* description );
	bool SpendPointsNotUseLock( int n, const char* description );
	bool AddCardPoints( int n, add_card_type type = ADD_CARD_GM_DONATE );
	int GetCreditAvailable();

	void CreditApply();
	bool RestoreCredit( uint32 n );
	bool UseCredit( uint32 n );
	void QueryCreditHistory();
	void QueryCreditHistoryProc( QueryResultVector& v );
	bool ExecuteSql( QueryBuffer* buf );
	bool ExecuteSql( const char* sql, ... );
	Database* _db;

private:
	bool _DecreaseCardPoints( const std::string& card_serial, int decrease_points, const char* description );
	struct rechargeable_card
	{
		std::string card_serial;
		int remain_points;
	};
	std::multimap<int, rechargeable_card> m_mapRechargeableCards;

private:
	int32 m_points;
	uint32 m_expire;
	uint32 m_timeLastCheckFee;
	bool m_expireflag;
	uint8 m_expireMinutes;
	
public:
	uint32 m_entergametime;
	SunyouInstance* m_sunyou_instance;
	bool IsInRaid() const;

	int32 m_addgold;
	uint32 m_additemchance;
	uint32 m_team_arena_idx;
	bool m_resurrect_lock;
	float leap_x;
	float leap_y;
	float leap_z;
	float leap_o;
	uint32 m_bg_kill_count;
	uint32 m_bg_kill_in_12seconds;
	uint32 m_bg_kill_last_time;
	uint32 m_bg_dota_state;
	bool m_beforeTeleport2Instance;
	bool m_EnableMeleeAttack;

private:
	bool m_isForbidTrade;

public:
	inline bool IsForbidTrade() const { return m_isForbidTrade; }
	void SwitchTrade( bool b );
	void SendForbidTradeMsg( WorldSession* s );

	void OnJoinSunyouInstance( SunyouInstance* si );
	void OnLeaveSunyouInstance( SunyouInstance* si );
	void SendUpdateInstanceQueueMsg();
	void SendEnterInstanceCountDownMsg( uint32 mapid, uint8 seconds );
	void RemoveQueueInstance( uint32 mapid );
	void AddQueueInstance( uint32 mapid );
	bool IsQueueInstanceFull() const;
	bool HasInstanceQueue( uint32 mapid ) const;
	void ResetQueueInstance();
	void InstanceTeleport( uint32 mapid, uint32 instanceid, LocationVector* plv );
	void BeforeJump2InstanceSrv();
	void IncreaseArenaKill();
	void SetArenaKill( uint32 n );
	void SetRandomResurrectLocation( const std::vector<LocationVector*>& v );
	void ClearRandomResurrectLocation();
	void PrepareTeleportToSunyouInstance( uint32 mapid, uint32 instanceid, LocationVector* plv );
	void AddExtraSpells( const uint32 extra_spells[10] );
	void AddExtraSpell( uint32 entry );
	void RemoveExtraSpell( uint32 entry );
	void RemoveAllExtraSpells();
	void AddExtraAuras( const uint32 extra_auras[10] );
	void RemoveAllExtraAuars();
	void RestoreAllExtraAuras();
	void LeaveSunyouInstanceProc();

	inline void SetInstanceBusy( bool b ) { SetUInt32Value(PLAYER_FIELD_INSTANCE_BUSY, (uint32)b); }
	inline bool IsInstanceBusy() const { return GetUInt32Value( PLAYER_FIELD_INSTANCE_BUSY ); }
	
	inline void LockSunyouInstanceJoin() { m_lock_sunyou_instance_join = true; }
	inline void UnlockSunyouInstanceJoin() { m_lock_sunyou_instance_join = false; }
	inline bool IsSunyouInstanceJoinLocked() const { return m_lock_sunyou_instance_join; }

	inline uint32 GetArenaKill() const { return m_arenakill; }
	bool m_escape_mode;
	inline uint32 GetLeaveGuildDate() const { return m_leave_guild_date; }
	void SetLeaveGuildDate();

	void JingLianItem();
	void JingLianCommit();
	bool RefineItem( bool b );

	PlayerInfo* GetTeacher();
	PlayerInfo* GetStudent();
	void Recruit( const std::string& student );
	void BeRecruit( const char* teacher, uint32 id );
	void ReplyRecruit( bool accept );
	inline bool HasTeacherOrStudent() const { return ( m_uint32Values[PLAYER_FIELD_TEACHER] || m_uint32Values[PLAYER_FIELD_STUDENT] ); }
	void EstablishTeacherStudentRelationship();
	void RemoveTeacherStudentRelationship( bool bfirst = true );
	void CheckTeacherStudentRelationship();
	void CheckTeacherStudentLevel();
	void AddFinishedDailyQuest( uint32 id );
	bool HasFinishedDailyQuest( uint32 id ) const;
	void AddQuestMobs( uint32 entry );
	void UpdateGuildScore( uint32 score );
	void UpdateGuildEmblem();

	void ModCoin(int delta);
	void SetCastleDebuffSpellID( uint32 id );
	void ModKillCount( int32 delta );
	void ModBeKillCount( int32 delta );
	void ModTotalHonor( int32 delta );
	void ModHonorCurrency( int32 delta );

	void ApplyAllEquipments();

	void CheckArenaTitles( bool only_check_item = false );
	bool m_arena_leave_reason_win;
	void AddArenaResult( uint32 arena_level, bool win, uint32 vs_type );

	inline bool IsOnlyCanCastSpell( uint32 entry ) const {
		if( m_setOnlyCanCastSpell.size() == 0 )
			return true;

		return m_setOnlyCanCastSpell.end() != m_setOnlyCanCastSpell.find( entry );
	}

	inline void AddOnlyCanCastSpell( uint32 entry ) {
		m_setOnlyCanCastSpell.insert( entry );
	}

	inline void RemoveOnlyCanCastSpell( uint32 entry ) {
		m_setOnlyCanCastSpell.erase( entry );
	}

	inline void ClearOnlyCanCastSpell() {
		m_setOnlyCanCastSpell.clear();
	}

	void KnockBack( LocationVector casterPosition, uint32 spellid, uint32 distance, uint32 casterid );

	void UpdateInrangeQuestGiverState();
	void UpdateQuestGiverState( Creature* QuestGiver );
	void RemoveItemAmt( uint32 entry, uint32 amt );
	void AddNeedSendCurrentMoveCreature( uint64 guid );
	void DropEquipmentOnGround( Player* attacker );
	uint32 m_HealthBeforeEnterInstance;
	uint32 m_ManaBeforeEnterInstance;
	uint32 mother_groupid;
	bool m_isReturn2Mother;

private:
	uint32 m_queuedmapid[3];
	uint32 m_arenakill;
	std::vector<LocationVector*> m_random_vrl;
	uint32 m_extra_spells[10];
	uint32 m_extra_aruas[10];
	ActionButton m_oldactionbar[PLAYER_ACTION_BUTTON_COUNT];
	bool m_is_add_extra_spells;
	uint32 m_leave_guild_date;
	uint32 m_temp_teacher_id;
	uint32 m_enchant_teacher_time;
	uint32 m_last_recruit_time;
	uint32 m_castle_debuff_spell_id;
	bool m_lock_sunyou_instance_join;
	std::set<uint32> m_setOnlyCanCastSpell;
	std::map<uint64, uint32> m_setNeedSendCurrentMoveCreature;
};

class SkillIterator
{
	SkillMap::iterator m_itr;
	SkillMap::iterator m_endItr;
	bool m_searchInProgress;
	Player * m_target;
public:
	SkillIterator(Player* target) : m_searchInProgress(false),m_target(target) {}
	~SkillIterator() { if(m_searchInProgress) { EndSearch(); } }

	void BeginSearch()
	{
		// iteminterface doesn't use mutexes, maybe it should :P
		ASSERT(!m_searchInProgress);
		m_itr = m_target->m_skills.begin();
		m_endItr = m_target->m_skills.end();
		m_searchInProgress=true;
	}

	void EndSearch()
	{
		// nothing here either
		ASSERT(m_searchInProgress);
		m_searchInProgress=false;
	}

	PlayerSkill* operator*() const
	{
		return &m_itr->second;
	}

	PlayerSkill* operator->() const
	{
		return &m_itr->second;
	}

	void Increment()
	{
		if(!m_searchInProgress)
			BeginSearch();

		if(m_itr==m_endItr)
			return;

		++m_itr;
	}

	SUNYOU_INLINE PlayerSkill* Grab() { return &m_itr->second; }
	SUNYOU_INLINE bool End() { return (m_itr==m_endItr)?true:false; }
};

#ifdef ENABLE_COMPRESSED_MOVEMENT

class CMovementCompressorThread : public ThreadBase
{
	bool running;
	set<Player*> m_players;
public:
	CMovementCompressorThread() { running = true; }

	void AddPlayer(Player * pPlayer);
	void RemovePlayer(Player * pPlayer);

	void OnShutdown() { running = false; }
	bool run();
};

extern CMovementCompressorThread * MovementCompressor;

#endif

#endif
