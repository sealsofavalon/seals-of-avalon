#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Quest.h"
#include "../../SDBase/Protocol/S2C_Quest.h"
#include "../../SDBase/Protocol/S2C_Spell.h"
#include "../../SDBase/Protocol/S2C_BattleGround.h"

void Player::SendWorldStateUpdate(uint32 WorldState, uint32 Value)
{
	MSG_S2C::stBG_Update_World_States Msg;
	Msg.index = WorldState;
	Msg.value = Value;
	GetSession()->SendPacket(Msg);
}


void Player::Gossip_SendPOI(float X, float Y, uint32 Icon, uint32 Flags, uint32 Data, const char* Name)
{
	size_t namelen = strlen(Name);
	MSG_S2C::stNPC_Gossip_Poi Msg;
	Msg.Flags = Flags;
	Msg.X = X;
	Msg.Y = Y;
	Msg.Icon = Icon;
	Msg.Data = Data;
	Msg.Name = Name?Name:"";

	GetSession()->SendPacket(Msg);
}
  
void Player::SendLevelupInfo(uint32 level, uint32 Hp, uint32 Mana, uint32 Stat0, uint32 Stat1, uint32 Stat2, uint32 Stat3, uint32 Stat4, uint32 Stat5)
{
	MSG_S2C::stLevel_Up_Info Msg;
    Msg.level = level;
    Msg.Hp = Hp;
    Msg.Mana = Mana;

    // grep: these are probably the other powers :)
    Msg.unk0 = 0;
    Msg.unk1 = 0;
    Msg.unk2 = 0;
    Msg.unk3 = 0;

    // Append stat differences
    Msg.Stat0 = Stat0;
    Msg.Stat1 = Stat1;
    Msg.Stat2 = Stat2;
    Msg.Stat3 = Stat3;
    Msg.Stat4 = Stat4;
	Msg.Stat5 = Stat5;
    GetSession()->SendPacket( Msg );
}

void Player::SendLogXPGain(uint64 guid, uint32 NormalXP, uint32 RestedXP, bool type)
{
	MSG_S2C::stLog_XP_Gain Msg;
    if (type == false)
    {
        Msg.guid     = guid;
        Msg.xp       = NormalXP;
        Msg.type     = (uint8)type;
        Msg.restxp   = RestedXP;
		GetSession()->SendPacket(Msg);
    }
    else if (type == true)
    {
        Msg.guid = 0; // does not need to be set for quest xp
        Msg.xp = NormalXP;
        Msg.type = (uint8)type;
        GetSession()->SendPacket(Msg);
    }
}

// this one needs to be send inrange...
void Player::SendEnvironmentalDamageLog(const uint64 & guid, uint8 type, uint32 damage)
{
	MSG_S2C::stDamage_Environmental_Log Msg;
	Msg.target_guid = guid;
	Msg.type		= type;
	Msg.damage		= damage;
    GetSession()->SendPacket(Msg);
}


void Player::SendCastResult(uint32 SpellId, uint8 ErrorMessage, uint8 MultiCast, uint32 Extra)
{
	MSG_S2C::stSpell_Cast_Result Msg;
	Msg.SpellId			= SpellId;
	Msg.ErrorMessage	= ErrorMessage;
	Msg.MultiCast		= MultiCast;
	Msg.Extra			= Extra;
	m_session->SendPacket(Msg);
}


