#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Item.h"
#include "../../SDBase/Protocol/S2C_Item.h"
#include "QuestMgr.h"

//////////////////////////////////////////////////////////////
/// This function handles CMSG_NAME_QUERY:
//////////////////////////////////////////////////////////////
void WorldSession::HandleNameQueryOpcode( CPacketUsn& packet )
{
	MSG_C2S::stQuery_Name MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;
	PlayerInfo *pn = objmgr.GetPlayerInfo( (uint32)guid );

	if(!pn)
		return;

	MyLog::log->debug( "Received CMSG_NAME_QUERY for: %s", pn->name );

	MSG_S2C::stQuery_Name_Response Msg;
	Msg.guid = pn->guid;
	Msg.name =pn->name;
	Msg.race = pn->race;
	Msg.gender = pn->gender;
	Msg.Class = pn->cl;
	SendPacket( Msg );
}

//////////////////////////////////////////////////////////////
/// This function handles CMSG_QUERY_TIME:
//////////////////////////////////////////////////////////////
void WorldSession::HandleQueryTimeOpcode( CPacketUsn& packet )
{
	uint32 t = (uint32)UNIXTIME;
	MSG_S2C::stQuery_Time_Response Msg;
	Msg.time = t;
	SendPacket( Msg );
}

//////////////////////////////////////////////////////////////
/// This function handles CMSG_CREATURE_QUERY:
//////////////////////////////////////////////////////////////
void WorldSession::HandleCreatureQueryOpcode( CPacketUsn& packet )
{
	MSG_C2S::stQuery_Creature MsgRecv;packet>>MsgRecv;
	MSG_S2C::stQuery_Creature_Response Msg;
	uint32 entry = MsgRecv.entry;
	uint64 guid = MsgRecv.guid;
	CreatureInfo *ci;

	if(entry == WAYPOINTENTRY)
	{
		Msg.entry = entry;
		Msg.name = "WayPoint";
	}
	else
	{
		ci = CreatureNameStorage.LookupEntry(entry);
		if(ci == NULL)
			return;

			MyLog::log->notice("WORLD: CMSG_CREATURE_QUERY '%s'", ci->Name);
			Msg.entry = entry;
			Msg.name = ci->Name;

		Msg.info_str = ci->info_str; //!!! this is a string in 2.3.0 Example: stormwind guard has : "Direction"
		Msg.Type = ci->Type;
		Msg.Family = ci->Family;
		Msg.Rank = ci->Rank;
		Msg.Male_DisplayID = ci->Male_DisplayID;
		Msg.Female_DisplayID = ci->Female_DisplayID;
		Msg.Civilian	= ci->Civilian;
	}

	SendPacket( Msg );
}

//////////////////////////////////////////////////////////////
/// This function handles CMSG_GAMEOBJECT_QUERY:
//////////////////////////////////////////////////////////////
void WorldSession::HandleGameObjectQueryOpcode( CPacketUsn& packet )
{
	MSG_C2S::stQuery_GameObj MsgRecv;packet>>MsgRecv;
	MSG_S2C::stQuery_GameObj_Response Msg;

	uint32 entryID = MsgRecv.entry;
	uint64 guid = MsgRecv.guid;
	GameObjectInfo *goinfo;
	
	MyLog::log->notice("WORLD: CMSG_GAMEOBJECT_QUERY '%u'", entryID);

	goinfo = GameObjectNameStorage.LookupEntry(entryID);
	if(goinfo == NULL)
		return;

    
	Msg.entry = entryID;
	Msg.Type = goinfo->Type;
	Msg.DisplayID = goinfo->DisplayID;
	Msg.name = goinfo->Name;

	Msg.SpellFocus = goinfo->SpellFocus;
	SendPacket( Msg );
}

//////////////////////////////////////////////////////////////
/// This function handles MSG_CORPSE_QUERY:
//////////////////////////////////////////////////////////////
void WorldSession::HandleCorpseQueryOpcode(CPacketUsn& packet)
{
	MyLog::log->notice("WORLD: Received MSG_CORPSE_QUERY");

	Corpse *pCorpse;
	MSG_S2C::stQuery_Corpse_Response Msg;
	MapInfo *pMapinfo;

	pCorpse = objmgr.GetCorpseByOwner(GetPlayer()->GetLowGUID());
	if(pCorpse)
	{
		pMapinfo = WorldMapInfoStorage.LookupEntry(pCorpse->GetMapId());
		if(pMapinfo)
		{
			if(pMapinfo->type == INSTANCE_NULL || pMapinfo->type == INSTANCE_PVP)
			{
				Msg.bShow = uint8(0x01); //show ?
				Msg.mapid = pCorpse->GetMapId(); // mapid (that tombstones shown on)
				Msg.x = pCorpse->GetPositionX();
				Msg.y = pCorpse->GetPositionY();
				Msg.z = pCorpse->GetPositionZ();
				Msg.instance_mapid = pCorpse->GetMapId(); //instance mapid (needs to be same as mapid to be able to recover corpse)
				SendPacket(Msg);
			}
			else
			{
				Msg.bShow = uint8(0x01); //show ?
				Msg.mapid = pMapinfo->repopmapid; // mapid (that tombstones shown on)
				Msg.x = pMapinfo->repopx;
				Msg.y = pMapinfo->repopy;
				Msg.z = pMapinfo->repopz;
				Msg.instance_mapid = pCorpse->GetMapId(); //instance mapid (needs to be same as mapid to be able to recover corpse)
				SendPacket(Msg);
			}
		}
		else
		{

			Msg.bShow = uint8(0x01); //show ?
			Msg.mapid = pCorpse->GetMapId(); // mapid (that tombstones shown on)
			Msg.x = pCorpse->GetPositionX();
			Msg.y = pCorpse->GetPositionY();
			Msg.z = pCorpse->GetPositionZ();
			Msg.instance_mapid = pCorpse->GetMapId(); //instance mapid (needs to be same as mapid to be able to recover corpse)
			SendPacket(Msg);

		}
	}
}

void WorldSession::HandlePageTextQueryOpcode( CPacketUsn& packet )
{
	MSG_C2S::stQuery_Page_Text MsgRecv;packet>>MsgRecv;
	uint32 pageid = MsgRecv.pageid;
	while(pageid)
	{
		ItemPage * page = ItemPageStorage.LookupEntry(pageid);
		if(!page)
			return;

		MSG_S2C::stQuery_Page_Text_Response Msg;
		Msg.pageid = pageid;
		Msg.text = page->text;
		Msg.next_pageid = page->next_page;
		pageid = page->next_page;
		SendPacket(Msg);
	}

}
//////////////////////////////////////////////////////////////
/// This function handles CMSG_ITEM_NAME_QUERY:
//////////////////////////////////////////////////////////////
void WorldSession::HandleItemNameQueryOpcode( CPacketUsn& packet )
{
	MSG_C2S::stItem_Name_Query MsgRecv;packet>>MsgRecv;
	MSG_S2C::stItem_Name_Query_Response Msg;
	uint32 itemid = MsgRecv.item_entry;
	Msg.item_entry = itemid;
	ItemPrototype *proto=ItemPrototypeStorage.LookupEntry(itemid);
	if(!proto)
		Msg.name = "Unknown Item";
	else
	{
		Msg.name = Item::BuildStringWithProto(proto);
	}
	SendPacket(Msg);	
}

void WorldSession::HandleInrangeQuestgiverQuery(CPacketUsn& packet)
{
	CHECK_INWORLD_RETURN;

	_player->UpdateInrangeQuestGiverState();
}
