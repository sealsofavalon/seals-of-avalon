#ifndef QUEST_H
#define QUEST_H

#include "LuaInterface.h"

using namespace std;
/*
not available because low level = 1
available but quest low level = 2
question mark = 3 (not got objectives)
blue question mark = 4
blue exclamation mark = 5
yellow exclamation mark = 6
yellow question mark = 7
finished = 8
132 error = 9
*/

static const int QUEST_MAX_COUNT = 50;

enum QUESTGIVER_QUEST_TYPE
{
	QUESTGIVER_QUEST_START  = 0x01,
	QUESTGIVER_QUEST_END	= 0x02,
};

enum QUEST_TYPE
{
	QUEST_GATHER	= 0x01,
	QUEST_SLAY	  = 0x02,
};

enum QUEST_FLAG
{
	QUEST_FLAG_NONE		  = 0,   
	QUEST_FLAG_DELIVER	   = 1,   
	QUEST_FLAG_KILL		  = 2,   
	QUEST_FLAG_SPEAKTO	   = 4,
	QUEST_FLAG_REPEATABLE	= 8,   
	QUEST_FLAG_EXPLORATION   = 16,
	QUEST_FLAG_TIMED		 = 32,
	QUEST_FLAG_REPUTATION	= 128,
	QUEST_FLAG_ESCORT		= 256,
};

class QuestLogEntry;

class QuestLuaUnit : public InstanceLuaObject<QuestLuaUnit>
{
public:
	QuestLuaUnit( QuestLogEntry* qle );
	~QuestLuaUnit();

	void Init( Quest* qst );

	int AddQuestMobCount( LuaState* s );
	int QuestFail( LuaState* s );
	int Mail2Player( LuaState* s );

	//////////////////////////////////////////////////////////////////////////
	void OnMailboxOpen( Player* plr );
	void OnFriendAdd( Player* plr );
	void OnShopOpen( Player* plr );

private:
	LuaFunction<void>* lf_OnMailboxOpen;
	LuaFunction<void>* lf_OnFriendAdd;
	LuaFunction<void>* lf_OnShopOpen;
	QuestLogEntry* m_qle;
};

class QuestLogEntry : public EventableObject
{
	friend class QuestMgr;

public:

	QuestLogEntry();
	~QuestLogEntry();

	SUNYOU_INLINE Quest* GetQuest() { return m_quest; };
	void Init(Quest* quest, Player* plr, uint32 slot);

	bool CanBeFinished();
	void SubtractTime(uint32 value);
	void SaveToDB(QueryBuffer * buf);
	bool LoadFromDB(Field *fields);
	void UpdatePlayerFields(bool bFinish=false);

	void SetTrigger(uint32 i);
	void SetMobCount(uint32 i, uint32 count);
	void SetDirty(bool b){ mDirty = b; }

	bool IsUnitAffected(Unit* target);
	SUNYOU_INLINE bool IsCastQuest() { return iscastquest;}
	void AddAffectedUnit(Unit* target);
	void ClearAffectedUnits();

	void SetSlot(int32 i);
	void Finish(bool bComplete = true);

	void SendQuestComplete( bool UpdateGiverState = true );
	void SendUpdateAddKill(uint32 i);
	SUNYOU_INLINE uint32 GetMobCount(uint32 i) { return m_mobcount[i]; }
	SUNYOU_INLINE uint32 GetExploredAreas(uint32 i) { return m_explored_areas[i]; }

	SUNYOU_INLINE uint32 GetBaseField(uint32 slot)
	{
		return PLAYER_QUEST_LOG_1_1 + (slot * 4);
	}

private:
	uint32 completed;

	bool mInitialized;
	bool mDirty;

	Quest *m_quest;
	Player *m_plr;
	
	uint32 m_mobcount[4];
	uint32 m_explored_areas[4];

	std::set<uint64> m_affected_units;
	bool iscastquest;

	uint32 m_time_left;
	int32 m_slot;
	QuestLuaUnit* m_luaUnit;
};

#endif
