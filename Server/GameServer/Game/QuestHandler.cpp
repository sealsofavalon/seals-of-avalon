#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Quest.h"
#include "../../SDBase/Protocol/S2C_Quest.h"

#include "Object.h"
#include "GameObject.h"
#include "Item.h"
#include "ItemInterface.h"
#include "Creature.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "QuestMgr.h"
#include "MapMgr.h"
#include "ObjectMgr.h"
#include "ObjectStorage.h"
#include "Quest.h"
#include "Group.h"
#include "WorldSession.h"
#include "QuestMgr.h"

initialiseSingleton( QuestMgr );

void WorldSession::HandleQuestgiverStatusQueryOpcode( CPacketUsn& packet )
{
	MyLog::log->error( "Received stQuestGiver_Stats_Query!!!" );
	return;

	if(!_player) return;
    if(!_player->IsInWorld()) return;

	MSG_C2S::stQuestGiver_Stats_Query MsgRecv;packet >> MsgRecv;
    Object *qst_giver = NULL;

	uint32 guidtype = GET_TYPE_FROM_GUID(MsgRecv.guid);
    if(guidtype==HIGHGUID_TYPE_UNIT)
    {
        Creature *quest_giver = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
        if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return;

        if (!quest_giver->isQuestGiver())
	    {
		    MyLog::log->debug("WORLD: Creature is not a questgiver.");
		    return;
	    }
    }
    else if(guidtype==HIGHGUID_TYPE_ITEM)
	{
		Item *quest_giver = GetPlayer()->GetItemInterface()->GetItemByGUID(MsgRecv.guid);
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return;
	}
    else if(guidtype==HIGHGUID_TYPE_GAMEOBJECT)
	{
		GameObject *quest_giver = _player->GetMapMgr()->GetGameObject(GET_LOWGUID_PART(MsgRecv.guid));
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return;
	}

	if (!qst_giver)
	{
		MyLog::log->debug("WORLD: Invalid questgiver GUID "I64FMT".", MsgRecv.guid);
		return;
	}

	MSG_S2C::stQuestGiver_Stats Msg;
	Msg.guid	= MsgRecv.guid;
	Msg.stats	= sQuestMgr.CalcStatus(qst_giver, GetPlayer());
	SendPacket( Msg );
}

void WorldSession::HandleQuestgiverHelloOpcode( CPacketUsn& packet )
{
	MyLog::log->debug( "WORLD: Received CMSG_QUESTGIVER_HELLO." );
    if(!_player) return;
	if(!_player->IsInWorld()) return;

	MSG_C2S::stQuestGiver_Hello MsgRecv;packet >> MsgRecv;
	Creature *qst_giver = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));

	if (!qst_giver)
	{
		MyLog::log->debug("WORLD: Invalid questgiver GUID.");
		return;
	}

	if (!qst_giver->isQuestGiver())
	{
		MyLog::log->debug("WORLD: Creature is not a questgiver.");
		return;
	}

	/*if(qst_giver->GetAIInterface()) // NPC Stops moving for 3 minutes
		qst_giver->GetAIInterface()->StopMovement(180000);*/

	//qst_giver->Emote(EMOTE_ONESHOT_TALK); // this doesnt work
	sQuestMgr.OnActivateQuestGiver(qst_giver, GetPlayer());
}

void WorldSession::HandleQuestGiverQueryQuestOpcode( CPacketUsn& packet )
{
	MSG_C2S::stQuestGiver_Stats_Query MsgRecv;packet >> MsgRecv;
	HandleQuestGiverQueryQuestOpcode( MsgRecv );
}

void WorldSession::HandleQuestgiverAcceptQuestOpcode( CPacketUsn& packet )
{
	MyLog::log->debug( "WORLD: Received CMSG_QUESTGIVER_ACCEPT_QUEST" );
    if(!_player) return;
	if(!_player->IsInWorld()) return;

	//WorldPacket data;

	MSG_C2S::stQuestGiver_Accept_Quest MsgRecv;packet >> MsgRecv;

	bool bValid = false;
	bool hasquest = true;
	bool bSkipLevelCheck = false;
	Quest *qst = NULL;
	Object *qst_giver = NULL;
	uint32 guidtype = GET_TYPE_FROM_GUID(MsgRecv.guid);

	if(guidtype==HIGHGUID_TYPE_UNIT)
	{
		Creature *quest_giver = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
		if(quest_giver)
		{
			qst_giver = (Object*)quest_giver;
			if( quest_giver->m_escort_players.size() && quest_giver->m_escort_qst )
			{
				SendNotification( "目标正忙，无法接任务" );
				return;
			}
		}
		else
			return;
		bValid = quest_giver->isQuestGiver();
		hasquest = quest_giver->HasQuest(MsgRecv.quest_id, 1);
		if(bValid)
			qst = QuestStorage.LookupEntry(MsgRecv.quest_id);
	} 
	else if(guidtype==HIGHGUID_TYPE_GAMEOBJECT)
	{
		GameObject *quest_giver = _player->GetMapMgr()->GetGameObject(GET_LOWGUID_PART(MsgRecv.guid));
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return;
		//bValid = quest_giver->isQuestGiver();
		//if(bValid)
		bValid = true;
			qst = QuestStorage.LookupEntry(MsgRecv.quest_id);
	} 
	else if(guidtype==HIGHGUID_TYPE_ITEM)
	{
		Item *quest_giver = GetPlayer()->GetItemInterface()->GetItemByGUID(MsgRecv.guid);
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return;
		bValid = true;
		bSkipLevelCheck=true;
		qst = QuestStorage.LookupEntry(MsgRecv.quest_id);
	}
	else if(guidtype==HIGHGUID_TYPE_PLAYER)
	{
		Player *quest_giver = _player->GetMapMgr()->GetPlayer((uint32)MsgRecv.guid);
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return;
		bValid = true;
		qst = QuestStorage.LookupEntry(MsgRecv.quest_id);
	}

	if (!qst_giver)
	{
		MyLog::log->debug("WORLD: Invalid questgiver GUID.");
		return;
	}

	if( !bValid || qst == NULL )
	{
		MyLog::log->debug("WORLD: Creature is not a questgiver.");
		return;
	}

	if( _player->GetQuestLogForEntry( qst->id ) )
		return;

	if( qst_giver->GetTypeId() == TYPEID_UNIT && static_cast< Creature* >( qst_giver )->m_escorter != NULL )
	{
		SystemMessage("You cannot accept this quest at this time.");
		return;
	}

	// Check the player hasn't already taken this quest, or
	// it isn't available.
	uint32 status = sQuestMgr.CalcQuestStatus(qst_giver, _player,qst,3, bSkipLevelCheck);

	if( (!sQuestMgr.IsQuestRepeatable(qst) && (!sQuestMgr.IsDailyQuest(qst)) && _player->HasFinishedQuest(qst->id)) || ( status != QMGR_QUEST_AVAILABLE && status != QMGR_QUEST_REPEATABLE && status != QMGR_QUEST_CHAT )
		|| !hasquest)
	{
		// We've got a hacker. Disconnect them.
		//sCheatLog.writefromsession(this, "tried to accept incompatible quest %u from %u.", qst->id, qst_giver->GetEntry());
		//Disconnect();
		return;
	}

	int32 log_slot = GetPlayer()->GetOpenQuestSlot();

	if (log_slot == -1)
	{
		sQuestMgr.SendQuestLogFull(GetPlayer());
		return;
	}

	//FIXME
	/*if(Player Has Timed quest && qst->HasFlag(QUEST_FLAG_TIMED))
		sQuestMgr.SendQuestInvalid(INVALID_REASON_HAVE_TIMED_QUEST);*/

	if(qst->count_receiveitems || qst->srcitem)
	{
		uint32 slots_required = qst->count_receiveitems;

		if(GetPlayer()->GetItemInterface()->CalculateFreeSlots(NULL) < slots_required)
		{
			GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_BAG_FULL);
			sQuestMgr.SendQuestFailed(FAILED_REASON_INV_FULL, qst, GetPlayer(), true);
			return;
		}
	}	
	
/*	if(qst_giver->GetTypeId() == TYPEID_UNIT && !ScriptSystem->OnQuestRequireEvent(qst, static_cast< Creature* >( qst_giver ), _player, QUEST_EVENT_CAN_ACCEPT))
		return;*/

	MSG_S2C::stQuset_Confirm_Accpet Msg;
	Msg.questid = qst->id;
	SendPacket(Msg);

	QuestLogEntry *qle = new QuestLogEntry();
	qle->Init(qst, _player, log_slot);
	qle->UpdatePlayerFields();

	// If the quest should give any items on begin, give them the items.
	for(uint32 i = 0; i < 4; ++i)
	{
		if(qst->receive_items[i])
		{
			Item *item = objmgr.CreateItem( qst->receive_items[i], GetPlayer());
			MyLog::yunyinglog->info("item-questreceive-player[%u][%s] item["I64FMT"][%u] count[%u]", _player->GetLowGUID(), _player->GetName(), item->GetGUID(), item->GetProto()->ItemId, item->GetUInt32Value(ITEM_FIELD_STACK_COUNT));

			if(!GetPlayer()->GetItemInterface()->AddItemToFreeSlot(item, false))
			{
				delete item;
			}
			else
				SendItemPushResult(item, false, true, false, true, 
				_player->GetItemInterface()->LastSearchItemBagSlot(), _player->GetItemInterface()->LastSearchItemSlot(),
				1);
		}
	}

	if(qst->srcitem && qst->srcitem != qst->receive_items[0])
	{
		Item * item = objmgr.CreateItem( qst->srcitem, _player );
		if(item)
		{
			item->SetUInt32Value(ITEM_FIELD_STACK_COUNT, qst->srcitemcount ? qst->srcitemcount : 1);
			if(!_player->GetItemInterface()->AddItemToFreeSlot(item, false))
				delete item;
		}
	}

	// Timed quest handler.
	if(qst->time > 0)
	{
		//Start Quest Timer Event Here
		//sEventMgr.AddEvent(GetPlayer(), &Player::EventTimedQuestExpire, qst, qle, static_cast<uint32>(log_slot), EVENT_TIMED_QUEST_EXPIRE, qst->time * 1000, 1);
		//uint32 qtime = static_cast<uint32>(time(NULL) + qst->time);
		//GetPlayer()->SetUInt32Value(log_slot+2, qtime);
		//GetPlayer()->SetUInt32Value(PLAYER_QUEST_LOG_1_01 + (log_slot * 3), qtime);
		//GetPlayer()->timed_quest_slot = log_slot;
	}

	if(qst->count_required_item || qst_giver->GetTypeId() == TYPEID_GAMEOBJECT)	// gameobject quests deactivate
		GetPlayer()->UpdateNearbyGameObjects();

	//ScriptSystem->OnQuestEvent(qst, static_cast< Creature* >( qst_giver ), _player, QUEST_EVENT_ON_ACCEPT);

	sQuestMgr.OnQuestAccepted(_player,qst,qst_giver);
	if( qst->quest_flags & QUEST_FLAG_ESCORT && qst_giver->IsCreature() )
	{
		if( _player->GetGroup() )
		{
			std::set<Player*> s;
			_player->GetGroup()->InsertNearbyMember2Set( _player, s );
			MSG_S2C::stQuestEscortNotify notify;
			notify.npc_id = GET_LOWGUID_PART( qst_giver->GetGUID() );
			notify.qst_id = qst->id;
			notify.who = _player->GetName();
			for( std::set<Player*>::iterator it = s.begin(); it != s.end(); ++it )
			{
				Player* plr = *it;
				plr->GetSession()->SendPacket( notify );
			}
		}
	}

	MyLog::log->debug("WORLD: Added new QLE.");
	sHookInterface.OnQuestAccept(_player, qst);
}

void WorldSession::HandleQuestgiverCancelOpcode(CPacketUsn& packet)
{
	MSG_S2C::stNPC_Gossip_Complete Msg;
	SendPacket( Msg );

	MyLog::log->debug("WORLD: Sent SMSG_GOSSIP_COMPLETE");
}

void WorldSession::HandleQuestlogRemoveQuestOpcode(CPacketUsn& packet)
{
	MyLog::log->debug( "WORLD: Received CMSG_QUESTLOG_REMOVE_QUEST" );
    if(!_player) return;
	if(!_player->IsInWorld()) return;

	MSG_C2S::stQuestLog_Remove_Quest MsgRecv;packet >> MsgRecv;
	if(MsgRecv.quest_slot >= QUEST_MAX_COUNT)
		return;

	QuestLogEntry *qEntry = GetPlayer()->GetQuestLogInSlot(MsgRecv.quest_slot);
	if (!qEntry)
	{
		MyLog::log->debug("WORLD: No quest in slot %d.", MsgRecv.quest_slot);
		return;		
	}
	Quest *qPtr = qEntry->GetQuest();
	qEntry->Finish(false);

	// Remove all items given by the questgiver at the beginning
	for(uint32 i = 0; i < 4; ++i)
	{
		if(qPtr->receive_items[i])
			GetPlayer()->GetItemInterface()->RemoveItemAmt( qPtr->receive_items[i], qPtr->receive_itemcount[i] );
	}
	if(qPtr->srcitem)
		GetPlayer()->GetItemInterface()->RemoveItemAmt( qPtr->srcitem, qPtr->srcitemcount );
	if(qPtr->time > 0)
	{
		GetPlayer()->timed_quest_slot = 0;
	}
	GetPlayer()->UpdateNearbyGameObjects();

	sHookInterface.OnQuestCancelled(_player, qPtr);
	sQuestMgr.OnQuestGiveuped( _player, qPtr );
}

void WorldSession::HandleQuestQueryOpcode( CPacketUsn& packet )
{
    if(!_player) return;
	if(!_player->IsInWorld()) return;
	MyLog::log->debug( "WORLD: Received CMSG_QUEST_QUERY" );

	MSG_C2S::stQuest_Query MsgRecv;packet >> MsgRecv;

	Quest *qst = QuestStorage.LookupEntry(MsgRecv.quest_id);

	if (!qst)
	{
		MyLog::log->debug("WORLD: Invalid quest ID.");
		return;
	}

	MSG_S2C::stQuery_Quest_Response Msg;
	BuildQuestQueryResponse(qst, &Msg);
	SendPacket(Msg);
	MyLog::log->debug( "WORLD: Sent SMSG_QUEST_QUERY_RESPONSE." );
}

void WorldSession::HandleQuestgiverRequestRewardOpcode( CPacketUsn& packet )
{
    if(!_player) return;
	if(!_player->IsInWorld()) return;
	MyLog::log->debug( "WORLD: Received CMSG_QUESTGIVER_REQUESTREWARD_QUEST." );

	MSG_C2S::stQuestGiver_Request_Reward MsgRecv;packet >> MsgRecv;
	bool bValid = false;
	Quest *qst = NULL;
	Object *qst_giver = NULL;
	uint32 status = 0;
	uint32 guidtype = GET_TYPE_FROM_GUID(MsgRecv.guid);

	if(guidtype==HIGHGUID_TYPE_UNIT)
	{
		Creature *quest_giver = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return;
		bValid = quest_giver->isQuestGiver();
		if(bValid)
		{
			qst = quest_giver->FindQuest(MsgRecv.quest_id, QUESTGIVER_QUEST_END);
			if(!qst)
				qst = quest_giver->FindQuest(MsgRecv.quest_id, QUESTGIVER_QUEST_START);

			/*if(!qst) 
				qst = QuestStorage.LookupEntry(quest_id);*/
			if(!qst)
			{
				MyLog::log->error("WARNING: Cannot complete quest, as it doesnt exist.");
				return;
			}
			status = sQuestMgr.CalcQuestStatus(qst_giver, GetPlayer(), qst, (uint8)quest_giver->GetQuestRelation(qst->id),false);
		}
	} 
	else if(guidtype==HIGHGUID_TYPE_GAMEOBJECT)
	{
		GameObject *quest_giver = _player->GetMapMgr()->GetGameObject(GET_LOWGUID_PART(MsgRecv.guid));
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return; // oops..
		bValid = quest_giver->isQuestGiver();
		if(bValid)
		{
			qst = quest_giver->FindQuest(MsgRecv.quest_id, QUESTGIVER_QUEST_END);
			/*if(!qst) sQuestMgr.FindQuest(quest_id);*/
			if(!qst)
			{
				MyLog::log->error("WARNING: Cannot complete quest, as it doesnt exist.");
				return;
			}
			status = sQuestMgr.CalcQuestStatus(qst_giver, GetPlayer(), qst, (uint8)quest_giver->GetQuestRelation(qst->id),false);
		}
	}

	if (!qst_giver)
	{
		MyLog::log->debug("WORLD: Invalid questgiver GUID.");
		return;
	}

	if (!bValid || qst == NULL)
	{
		MyLog::log->debug("WORLD: Creature is not a questgiver.");
		return;
	}

	if (status == QMGR_QUEST_FINISHED)
	{
        MSG_S2C::stQuestGiver_Offer_Reward Msg;
		sQuestMgr.BuildOfferReward(&Msg, qst, qst_giver, 1);
		SendPacket(Msg);
		MyLog::log->debug( "WORLD: Sent SMSG_QUESTGIVER_REQUEST_ITEMS." );
	}

	// if we got here it means we're cheating
}

void WorldSession::HandleQuestgiverCompleteQuestOpcode( CPacketUsn& packet )
{
    if(!_player) return;
	if(!_player->IsInWorld()) return;
	MyLog::log->debug( "WORLD: Received CMSG_QUESTGIVER_COMPLETE_QUEST." );

	MSG_C2S::stQuestGiver_Complete_Quest MsgRecv;packet >> MsgRecv;

	bool bValid = false;
	Quest *qst = NULL;
	Object *qst_giver = NULL;
	uint32 status = 0;
	uint32 guidtype = GET_TYPE_FROM_GUID(MsgRecv.guid);

	if(guidtype==HIGHGUID_TYPE_UNIT)
	{
		Creature *quest_giver = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
		if(quest_giver)
		{
			qst_giver = (Object*)quest_giver;
			if( quest_giver->m_escort_players.size() && quest_giver->m_escort_qst )
			{
				return;
			}
		}
		else
			return;
		bValid = quest_giver->isQuestGiver();
		if(bValid)
		{
			qst = quest_giver->FindQuest(MsgRecv.quest_id, QUESTGIVER_QUEST_END);
			/*if(!qst) 
				sQuestMgr.FindQuest(quest_id);*/
			if(!qst)
			{
				MyLog::log->error("WARNING: Cannot complete quest, as it doesnt exist.");
				return;
			}
			status = sQuestMgr.CalcQuestStatus(qst_giver, GetPlayer(), qst, (uint8)quest_giver->GetQuestRelation(qst->id),false);
		}
	} 
	else if(guidtype==HIGHGUID_TYPE_GAMEOBJECT)
	{
		GameObject *quest_giver = _player->GetMapMgr()->GetGameObject(GET_LOWGUID_PART(MsgRecv.guid));
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return; // oops..
		bValid = quest_giver->isQuestGiver();
		if(bValid)
		{
			qst = quest_giver->FindQuest(MsgRecv.quest_id, QUESTGIVER_QUEST_END);
			/*if(!qst) sQuestMgr.FindQuest(quest_id);*/
			if(!qst)
			{
				MyLog::log->error("WARNING: Cannot complete quest, as it doesnt exist.");
				return;
			}
			status = sQuestMgr.CalcQuestStatus(qst_giver, GetPlayer(), qst, (uint8)quest_giver->GetQuestRelation(qst->id),false);
		}
	}

	if (!qst_giver)
	{
		MyLog::log->debug("WORLD: Invalid questgiver GUID.");
		return;
	}

	if (!bValid || qst == NULL)
	{
		MyLog::log->debug("WORLD: Creature is not a questgiver.");
		return;
	}

	if (status == QMGR_QUEST_NOT_FINISHED || status == QMGR_QUEST_REPEATABLE)
	{
        MSG_S2C::stQuestGiver_Request_Items Msg;
		sQuestMgr.BuildRequestItems(&Msg, qst, qst_giver, status);
		SendPacket(Msg);
		MyLog::log->debug( "WORLD: Sent SMSG_QUESTGIVER_REQUEST_ITEMS." );
	}

	if (status == QMGR_QUEST_FINISHED)
	{
		MSG_S2C::stQuestGiver_Offer_Reward Msg;
		sQuestMgr.BuildOfferReward(&Msg, qst, qst_giver, 1);
		SendPacket(Msg);
		MyLog::log->debug( "WORLD: Sent SMSG_QUESTGIVER_REQUEST_ITEMS." );

// 		if( qst->count_reward_choiceitem == 0 )
// 		{
// 			sQuestMgr.OnQuestFinished(GetPlayer(), qst, qst_giver, 0);
// 			//if(qst_giver->GetTypeId() == TYPEID_UNIT) qst->LUA_SendEvent(static_cast< Creature* >( qst_giver ),GetPlayer(),ON_QUEST_COMPLETEQUEST);
// 
// 			if(qst->next_quest_id)
// 			{
// 				MSG_C2S::stQuestGiver_Query_Quest Msg;
// 				Msg.guid	= MsgRecv.guid;
// 				Msg.quest_id	= qst->next_quest_id;
// 				HandleQuestGiverQueryQuestOpcode(packet);
// 			}
// 			if(qst->time > 0)
// 			{
// 				GetPlayer()->timed_quest_slot = 0;
// 			}
// 		}
	}
	
	//sHookInterface.OnQuestFinished(_player, qst);
}

void WorldSession::HandleQuestgiverChooseRewardOpcode(CPacketUsn& packet)
{
    if(!_player) return;
	if(!_player->IsInWorld()) return;
	MyLog::log->debug( "WORLD: Received CMSG_QUESTGIVER_CHOOSE_REWARD." );

	MSG_C2S::stQuestGiver_Choose_Reward MsgRecv;packet >> MsgRecv;

	if( MsgRecv.reward_slot >= 6 )
		return;

	bool bValid = false;
	Quest *qst = NULL;
	Object *qst_giver = NULL;
	uint32 guidtype = GET_TYPE_FROM_GUID(MsgRecv.guid);

	if(guidtype==HIGHGUID_TYPE_UNIT)
	{
		Creature *quest_giver = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return;
		bValid = quest_giver->isQuestGiver();
		if(bValid)
			qst = QuestStorage.LookupEntry(MsgRecv.quest_id);
	} 
	else if(guidtype==HIGHGUID_TYPE_GAMEOBJECT)
	{
		GameObject *quest_giver = _player->GetMapMgr()->GetGameObject(GET_LOWGUID_PART(MsgRecv.guid));
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return;
		//bValid = quest_giver->isQuestGiver();
		//if(bValid)
		bValid = true;
			qst = QuestStorage.LookupEntry(MsgRecv.quest_id);
	}

	if (!qst_giver)
	{
		MyLog::log->debug("WORLD: Invalid questgiver GUID.");
		return;
	}

	if (!bValid || qst == NULL)
	{
		MyLog::log->debug("WORLD: Creature is not a questgiver.");
		return;
	}

	//FIXME: Some Quest givers talk in the end of the quest.
	//   qst_giver->SendChatMessage(CHAT_MSG_MONSTER_SAY,LANG_UNIVERSAL,qst->GetQuestEndMessage().c_str());
	QuestLogEntry *qle = _player->GetQuestLogForEntry(MsgRecv.quest_id);

    if (!qle /*&& !qst->is_repeatable*/)
	{
		MyLog::log->debug("WORLD: QuestLogEntry not found.");
		return;
	}

	if (qle && !qle->CanBeFinished())
	{
		MyLog::log->debug("WORLD: Quest not finished.");
		return;
	}

	// remove icon
	/*if(qst_giver->GetTypeId() == TYPEID_UNIT)
	{
		qst_giver->BuildFieldUpdatePacket(GetPlayer(), UNIT_DYNAMIC_FLAGS, qst_giver->GetUInt32Value(UNIT_DYNAMIC_FLAGS));
	}*/

    //check for room in inventory for all items
	FAILED_REASON e = sQuestMgr.CanStoreReward(GetPlayer(),qst,MsgRecv.reward_slot);

	if( e != FAILED_REASON_SUCCESS )
    {
        sQuestMgr.SendQuestFailed(e, qst, GetPlayer(), false);
        return;
    }
	
	sQuestMgr.OnQuestFinished(GetPlayer(), qst, qst_giver, MsgRecv.reward_slot);
	//if(qst_giver->GetTypeId() == TYPEID_UNIT) qst->LUA_SendEvent(static_cast< Creature* >( qst_giver ),GetPlayer(),ON_QUEST_COMPLETEQUEST);

	if(qst->next_quest_id)
	{
		MSG_C2S::stQuestGiver_Query_Quest Msg;
		Msg.guid	= MsgRecv.guid;
		Msg.quest_id	= qst->next_quest_id;
		HandleQuestGiverQueryQuestOpcode( Msg );
	}
	if(qst->time > 0)
	{
		GetPlayer()->timed_quest_slot = 0;
	}
}

void WorldSession::HandleQuestGiverQueryQuestOpcode( const PakHead& msg )
{
	const MSG_C2S::stQuestGiver_Query_Quest& MsgRecv = (const MSG_C2S::stQuestGiver_Query_Quest&)msg;
	MyLog::log->debug( "WORLD: Received CMSG_QUESTGIVER_QUERY_QUEST." );
	if(!_player) return;
	if(!_player->IsInWorld()) return;

	uint32 status = 0;

	Object *qst_giver = NULL;

	bool bValid = false;
	Quest* qst = QuestStorage.LookupEntry(MsgRecv.quest_id);
	
	if (!qst)
	{
		MyLog::log->debug("WORLD: Invalid quest ID.");
		return;
	}

	uint32 guidtype = GET_TYPE_FROM_GUID(MsgRecv.guid);
	if(guidtype==HIGHGUID_TYPE_UNIT)
	{
		Creature *quest_giver = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(MsgRecv.guid));
		if(quest_giver)
		{
			qst_giver = (Object*)quest_giver;
			//if( quest_giver->m_escort_plr && quest_giver->m_escort_qst )
			//	return;
		}
		else
			return;
		bValid = quest_giver->isQuestGiver();
		if(bValid)
			status = sQuestMgr.CalcQuestStatus(qst_giver, GetPlayer(), qst, (uint8)quest_giver->GetQuestRelation(qst->id), false);
	} 
	else if(guidtype==HIGHGUID_TYPE_GAMEOBJECT)
	{
		GameObject *quest_giver = _player->GetMapMgr()->GetGameObject(GET_LOWGUID_PART(MsgRecv.guid));
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return;
		bValid = quest_giver->isQuestGiver();
		if(bValid)
			status = sQuestMgr.CalcQuestStatus(qst_giver, GetPlayer(), qst, (uint8)quest_giver->GetQuestRelation(qst->id), false);
	} 
	else if(guidtype==HIGHGUID_TYPE_ITEM)
	{
		Item *quest_giver = GetPlayer()->GetItemInterface()->GetItemByGUID(MsgRecv.guid);
		if(quest_giver)
			qst_giver = (Object*)quest_giver;
		else
			return;
		bValid = true;
		status = sQuestMgr.CalcQuestStatus(qst_giver, GetPlayer(), qst, 1, false);
	}
	
	if (!qst_giver)
	{
		MyLog::log->debug("WORLD: Invalid questgiver GUID.");
		return;
	}

	if (!bValid)
	{
		MyLog::log->debug("WORLD: object is not a questgiver.");
		return;
	}

	/*if (!qst_giver->FindQuest(quest_id, QUESTGIVER_QUEST_START | QUESTGIVER_QUEST_END))
	{
		MyLog::log->debug("WORLD: QuestGiver doesn't have that quest.");
		return;
	}*/	// bleh.. not needed.. maybe for antihack later on would be a good idea though
	
	if ((status == QMGR_QUEST_AVAILABLE) || (status == QMGR_QUEST_REPEATABLE) || (status == QMGR_QUEST_CHAT))
	{
		MSG_S2C::stQuestGiver_Quest_Details Msg;
		sQuestMgr.BuildQuestDetails(&Msg, qst,qst_giver->GetGUID(),1);	 // 0 because we want goodbye to function
		SendPacket(Msg);
		MyLog::log->debug( "WORLD: Sent SMSG_QUESTGIVER_QUEST_DETAILS." );
	}
	/*else if (status == QMGR_QUEST_FINISHED)
	{
		sQuestMgr.BuildOfferReward(&data, qst, qst_giver, 1);
		SendPacket(&data);
		MyLog::log->debug( "WORLD: Sent SMSG_QUESTGIVER_OFFER_REWARD." );
	}*/
	else if (status == QMGR_QUEST_NOT_FINISHED || status == QMGR_QUEST_FINISHED)
	{
		MSG_S2C::stQuestGiver_Request_Items Msg;
		sQuestMgr.BuildRequestItems(&Msg, qst, qst_giver, status);
		SendPacket(Msg);
		MyLog::log->debug( "WORLD: Sent SMSG_QUESTGIVER_REQUEST_ITEMS." );
	}
}

void WorldSession::HandlePushQuestToPartyOpcode(CPacketUsn& packet)
{
    if(!_player) return;
	if(!_player->IsInWorld()) return;
	uint32 questid, status;
	MSG_C2S::stQuest_PushTo_Party MsgRecv;packet>>MsgRecv;
	questid = MsgRecv.quest_id;

	MyLog::log->notice( "WORLD: Received CMSG_PUSHQUESTTOPARTY quest = %u", questid );

	Quest *pQuest = QuestStorage.LookupEntry(questid);
	if(pQuest)
	{
		if(pQuest->count_receiveitems)
		{
			sQuestMgr.SendPushToPartyResponse(_player, _player, QUEST_SHARE_MSG_CANT_TAKE_QUEST);
			return;
		}
		Group *pGroup = _player->GetGroup();
		if(pGroup)
		{
			uint32 pguid = _player->GetLowGUID();
			SubGroup * sgr = NULL;
			for (int i = 0; i < pGroup-> GetSubGroupCount( ); i ++)
			{
				sgr = pGroup->GetSubGroup(i);

				if(sgr)
				{
					GroupMembersSet::iterator itr;
					for(itr = sgr->GetGroupMembersBegin(); itr != sgr->GetGroupMembersEnd(); ++itr)
					{
						Player *pPlayer = (*itr)->m_loggedInPlayer;
						if(pPlayer && pPlayer->GetGUID() !=  pguid)
						{
							MSG_S2C::stQuest_Push_Result Msg;
							Msg.guid	= pPlayer->GetGUID();
							Msg.share_result		= uint32(QUEST_SHARE_MSG_SHARING_QUEST);
							_player->GetSession()->SendPacket(Msg);

							uint32 response = 0;
							if(_player->GetDistance2dSq(pPlayer) > 100)
							{
								response = QUEST_SHARE_MSG_TOO_FAR;
								continue;
							}
							QuestLogEntry *qst = pPlayer->GetQuestLogForEntry(questid);
							if(qst)
							{
								response = QUEST_SHARE_MSG_HAVE_QUEST;
								continue;
							}
							status = sQuestMgr.PlayerMeetsReqs(pPlayer, pQuest, false);
							if(status != QMGR_QUEST_AVAILABLE && status != QMGR_QUEST_CHAT)
							{
								response = QUEST_SHARE_MSG_CANT_TAKE_QUEST;
								continue;
							}
							if(pPlayer->HasFinishedQuest(questid))
							{
								response = QUEST_SHARE_MSG_FINISH_QUEST;
								continue;
							}
							if(pPlayer->GetOpenQuestSlot() == -1)
							{
								response = QUEST_SHARE_MSG_LOG_FULL;
								continue;
							}
							//Anything more?
							if(pPlayer->DuelingWith)
							{
								response = QUEST_SHARE_MSG_BUSY;
								continue;
							}
							if(response > 0)
								sQuestMgr.SendPushToPartyResponse(_player, pPlayer, response);

							MSG_S2C::stQuestGiver_Quest_Details MsgDetail;
							sQuestMgr.BuildQuestDetails(&MsgDetail, pQuest, pPlayer->GetGUID(), 1);
							pPlayer->GetSession()->SendPacket(MsgDetail);
							pPlayer->SetQuestSharer(pguid);
						}
					}
				}		
			}
		}
	}
}

void WorldSession::HandleQuestPushResult(CPacketUsn& packet)
{
    if(!_player) return;
	if(!_player->IsInWorld()) return;
	MSG_C2S::stQuest_Push_Result MsgRecv;packet >> MsgRecv;

	MyLog::log->notice( "WORLD: Received MSG_QUEST_PUSH_RESULT " );

	if(GetPlayer()->GetQuestSharer())
	{
		Player *pPlayer = objmgr.GetPlayer(GetPlayer()->GetQuestSharer());
		if(pPlayer)
		{
			MSG_S2C::stQuest_Push_Result Msg;
			Msg.guid	= MsgRecv.guid;
			Msg.share_result		= uint32(MsgRecv.msg);
			//data << uint8(0);
			pPlayer->GetSession()->SendPacket(Msg);
			GetPlayer()->SetQuestSharer(0);
		}
	}
}


void WorldSession::HandleQuestEscortReply( CPacketUsn& packet )
{
	if(!_player) return;
	if(!_player->IsInWorld()) return;

	MSG_C2S::stQuestEscortReply rep;
	packet >> rep;
	if( rep.is_accept )
	{
		Creature* qst_giver = _player->GetMapMgr()->GetCreature( rep.npc_id );
		if( qst_giver )
		{
			if( !qst_giver->isQuestGiver() ) return;
			uint8 qstrelid = (uint8)qst_giver->GetQuestRelation(rep.quest_id);
			Quest* qst = qst_giver->FindQuest(rep.quest_id, qstrelid);
			if( qst )
			{
				if( _player->GetQuestLogForEntry( qst->id ) )
					return;

				// Check the player hasn't already taken this quest, or
				// it isn't available.
				uint32 status = sQuestMgr.CalcQuestStatus(qst_giver, _player,qst,3, false);

				if( (!sQuestMgr.IsQuestRepeatable(qst) && (!sQuestMgr.IsDailyQuest(qst)) && _player->HasFinishedQuest(qst->id)) || ( status != QMGR_QUEST_AVAILABLE && status != QMGR_QUEST_REPEATABLE && status != QMGR_QUEST_CHAT ))
				{
					// We've got a hacker. Disconnect them.
					//sCheatLog.writefromsession(this, "tried to accept incompatible quest %u from %u.", qst->id, qst_giver->GetEntry());
					//Disconnect();
					return;
				}

				int32 log_slot = GetPlayer()->GetOpenQuestSlot();

				if (log_slot == -1)
				{
					sQuestMgr.SendQuestLogFull(GetPlayer());
					return;
				}

				//FIXME
				/*if(Player Has Timed quest && qst->HasFlag(QUEST_FLAG_TIMED))
					sQuestMgr.SendQuestInvalid(INVALID_REASON_HAVE_TIMED_QUEST);*/

				if(qst->count_receiveitems || qst->srcitem)
				{
					uint32 slots_required = qst->count_receiveitems;

					if(GetPlayer()->GetItemInterface()->CalculateFreeSlots(NULL) < slots_required)
					{
						GetPlayer()->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_BAG_FULL);
						sQuestMgr.SendQuestFailed(FAILED_REASON_INV_FULL, qst, GetPlayer(), false);
						return;
					}
				}	
				
			/*	if(qst_giver->GetTypeId() == TYPEID_UNIT && !ScriptSystem->OnQuestRequireEvent(qst, static_cast< Creature* >( qst_giver ), _player, QUEST_EVENT_CAN_ACCEPT))
					return;*/

				QuestLogEntry *qle = new QuestLogEntry();
				qle->Init(qst, _player, log_slot);
				qle->UpdatePlayerFields();

				// If the quest should give any items on begin, give them the items.
				for(uint32 i = 0; i < 4; ++i)
				{
					if(qst->receive_items[i])
					{
						Item *item = objmgr.CreateItem( qst->receive_items[i], GetPlayer());
						MyLog::yunyinglog->info("item-questreceive-player[%u][%s] item["I64FMT"][%u] count[%u]", _player->GetLowGUID(), _player->GetName(), item->GetGUID(), item->GetProto()->ItemId, item->GetUInt32Value(ITEM_FIELD_STACK_COUNT));

						if(!GetPlayer()->GetItemInterface()->AddItemToFreeSlot(item))
						{
							delete item;
						}
						else
							SendItemPushResult(item, false, true, false, true, 
							_player->GetItemInterface()->LastSearchItemBagSlot(), _player->GetItemInterface()->LastSearchItemSlot(),
							1);
					}
				}

				if(qst->srcitem && qst->srcitem != qst->receive_items[0])
				{
					Item * item = objmgr.CreateItem( qst->srcitem, _player );
					if(item)
					{
						item->SetUInt32Value(ITEM_FIELD_STACK_COUNT, qst->srcitemcount ? qst->srcitemcount : 1);
						if(!_player->GetItemInterface()->AddItemToFreeSlot(item))
							delete item;
					}
				}

				// Timed quest handler.
				if(qst->time > 0)
				{
					//Start Quest Timer Event Here
					//sEventMgr.AddEvent(GetPlayer(), &Player::EventTimedQuestExpire, qst, qle, static_cast<uint32>(log_slot), EVENT_TIMED_QUEST_EXPIRE, qst->time * 1000, 1);
					//uint32 qtime = static_cast<uint32>(time(NULL) + qst->time);
					//GetPlayer()->SetUInt32Value(log_slot+2, qtime);
					//GetPlayer()->SetUInt32Value(PLAYER_QUEST_LOG_1_01 + (log_slot * 3), qtime);
					//GetPlayer()->timed_quest_slot = log_slot;
				}

				if(qst->count_required_item || qst_giver->GetTypeId() == TYPEID_GAMEOBJECT)	// gameobject quests deactivate
					GetPlayer()->UpdateNearbyGameObjects();

				//ScriptSystem->OnQuestEvent(qst, static_cast< Creature* >( qst_giver ), _player, QUEST_EVENT_ON_ACCEPT);

				//sQuestMgr.OnQuestAccepted(_player,qst,qst_giver);

				qst_giver->m_escort_players.insert( _player->m_playerInfo );

				MyLog::log->debug("WORLD: Added new QLE.");
				sHookInterface.OnQuestAccept(_player, qst);
			}
		}
	}
}