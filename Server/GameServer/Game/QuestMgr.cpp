#include "StdAfx.h"
#include "QuestMgr.h"
#include "../../SDBase/Protocol/S2C_Quest.h"
#include "../../SDBase/Protocol/C2S_Quest.h"
#include "../../SDBase/Protocol/S2C_Spell.h"


bool LockQstItemNotify::s_bLockQstItemNotify = true;

uint32 QuestMgr::CalcQuestStatus(Object* quest_giver, Player* plr, QuestRelation* qst)
{
	return CalcQuestStatus(quest_giver, plr, qst->qst, qst->type, false);
}

bool QuestMgr::isRepeatableQuestFinished(Player *plr, Quest *qst)
{
    uint32 i;

	for(i = 0; i < 4; ++i)
	{
		if(qst->required_item[i])
		{
			if(plr->GetItemInterface()->GetItemCount(qst->required_item[i]) < qst->required_itemcount[i])
			{
				return false;
			}
		}
	}

	return true;
}

uint32 QuestMgr::PlayerMeetsReqs(Player* plr, Quest* qst, bool skiplevelcheck)
{
	std::list<uint32>::iterator itr;
	uint32 status = QMGR_QUEST_AVAILABLE;

	if (!sQuestMgr.IsQuestRepeatable(qst))
	{
		if(sQuestMgr.IsDailyQuest(qst))
		{
			if( plr->HasFinishedDailyQuest( qst->id ) )
				return QMGR_QUEST_NOT_AVAILABLE;
			else
				status = QMGR_QUEST_AVAILABLE;
		}
	}
	else
    {
		status = QMGR_QUEST_REPEATABLE;
    }

	if(!skiplevelcheck)
	{
		if (plr->getLevel() < qst->min_level)
			return QMGR_QUEST_AVAILABLELOW_LEVEL;

		if(sQuestMgr.IsDailyQuest(qst))
		{
			if(plr->getLevel() > qst->max_level)
				return QMGR_QUEST_AVAILABLELOW_LEVEL;
		}
	}

	if(qst->required_class)
		if(!(qst->required_class & plr->getClassMask()))
			return QMGR_QUEST_NOT_AVAILABLE;

	bool bMale = true;
	bool bFemale = true;

	bMale = (bool)(qst->bSex & 0x1);
	bFemale = (bool)(qst->bSex & 0x2);
	if (!bFemale && !bMale)
	{
		bFemale = true;
		bMale = true;
	}

	if ((!bFemale && plr->m_playerInfo->gender == 0) || (!bMale && plr->m_playerInfo->gender == 1))
	{
		 return QMGR_QUEST_NOT_AVAILABLE;
	}


    if(qst->required_races)
    {
        if(!(qst->required_races & plr->getRaceMask()))
            return QMGR_QUEST_NOT_AVAILABLE;
    }

	if(qst->required_tradeskill)
	{
		if(!plr->_HasSkillLine(qst->required_tradeskill))
			return QMGR_QUEST_NOT_AVAILABLE;
		if (qst->required_tradeskill_value && plr->_GetSkillLineCurrent(qst->required_tradeskill) < qst->required_tradeskill_value)
			return QMGR_QUEST_NOT_AVAILABLE;
	}

	if (qst->previous_quest_id)
	{
		Quest* prev = QuestStorage.LookupEntry(qst->previous_quest_id);
		if(!prev)
			return QMGR_QUEST_NOT_AVAILABLE;

		if(sQuestMgr.IsDailyQuest(prev))
		{
			if( !plr->HasFinishedDailyQuest( qst->previous_quest_id ) )
				status = QMGR_QUEST_NOT_AVAILABLE;
			else
				status = QMGR_QUEST_AVAILABLE;
		}
		else
			if(!plr->HasFinishedQuest(qst->previous_quest_id))
				return QMGR_QUEST_NOT_AVAILABLE;
	}

	if(!sQuestMgr.IsDailyQuest(qst) && !sQuestMgr.IsQuestRepeatable(qst))
		if (plr->HasFinishedQuest(qst->id))
			return QMGR_QUEST_NOT_AVAILABLE;

	for(uint32 i = 0; i < 4; ++i)
	{
		if (qst->required_quests[i] > 0 && !plr->HasFinishedQuest(qst->required_quests[i]))
		{
			return QMGR_QUEST_NOT_AVAILABLE;
		}
	}

	// check quest level
	if( plr->getLevel() >= ( qst->max_level + 5 ) && ( status != QMGR_QUEST_REPEATABLE ) )
		return QMGR_QUEST_CHAT;

	return status;
}

uint32 QuestMgr::CalcQuestStatus(Object* quest_giver, Player* plr, Quest* qst, uint8 type, bool skiplevelcheck)
{
	QuestLogEntry* qle;

	qle = plr->GetQuestLogForEntry(qst->id);

	if (!qle)
	{
		if (type & QUESTGIVER_QUEST_START)
		{
			return PlayerMeetsReqs(plr, qst, skiplevelcheck);
		}
	}
	else
	{		
		if(type & QUESTGIVER_QUEST_END) 
		{
			if (!qle->CanBeFinished())
			{
				return QMGR_QUEST_NOT_FINISHED;
			}
			else
			{
				return QMGR_QUEST_FINISHED;					
			}
		}
		else if (type & QUESTGIVER_QUEST_START)
		{

		}
	}
// 	else
// 	{		
// 		if (!qle->CanBeFinished())
// 		{
// 			return QMGR_QUEST_NOT_FINISHED;
// 		}
// 		else
// 		{
// 			if (type & QUESTGIVER_QUEST_END) 
// 			{
// 				return QMGR_QUEST_FINISHED;					
// 			}
// 			else
// 			{
// 				return QMGR_QUEST_NOT_AVAILABLE;
// 			}
// 		}
// 	}

	return QMGR_QUEST_NOT_AVAILABLE;
}

uint32 QuestMgr::CalcStatus(Object* quest_giver, Player* plr)
{
	uint32 status = QMGR_QUEST_NOT_AVAILABLE;
	std::list<QuestRelation *>::const_iterator itr;
	std::list<QuestRelation *>::const_iterator q_begin;
	std::list<QuestRelation *>::const_iterator q_end;
	bool bValid = false;

	if( quest_giver->GetTypeId() == TYPEID_GAMEOBJECT )
	{
        bValid = ((GameObject*)quest_giver)->HasQuests();
        if(bValid)
		{
			q_begin = ((GameObject*)quest_giver)->QuestsBegin();
			q_end = ((GameObject*)quest_giver)->QuestsEnd();
		}
	} 
	else if( quest_giver->GetTypeId() == TYPEID_UNIT )
	{
		bValid = static_cast< Creature* >( quest_giver )->HasQuests();
		if(bValid)
		{
			q_begin = ((Creature*)quest_giver)->QuestsBegin();
			q_end = ((Creature*)quest_giver)->QuestsEnd();
		}
	}
    else if( quest_giver->GetTypeId() == TYPEID_ITEM )
    {
        if( static_cast< Item* >( quest_giver )->GetProto()->QuestId )
            bValid = true;
    }
	//This will be handled at quest share so nothing important as status
	else if(quest_giver->GetTypeId() == TYPEID_PLAYER)
	{
		status = QMGR_QUEST_AVAILABLE;
	}

	if(!bValid)
	{
        //anoying msg that is not needed since all objects dont exactly have quests 
		//MyLog::log->debug("QUESTS: Warning, invalid NPC "I64FMT" specified for CalcStatus. TypeId: %d.", quest_giver->GetGUID(), quest_giver->GetTypeId());
		return status;
	}

    if(quest_giver->GetTypeId() == TYPEID_ITEM)
    {
        Quest *pQuest = QuestStorage.LookupEntry( static_cast<Item*>(quest_giver)->GetProto()->QuestId );
        QuestRelation qr;
        qr.qst = pQuest;
        qr.type = 1;

        uint32 tmp_status = CalcQuestStatus(quest_giver,plr, &qr);
        if(tmp_status > status)
            status = tmp_status;
    }
	else
	{
		for(itr = q_begin; itr != q_end; ++itr)
		{
			uint32 tmp_status = CalcQuestStatus(quest_giver, plr, *itr);	// save a call
			if (tmp_status > status)
				status = tmp_status;
		}
	}

    return status;
}

uint32 QuestMgr::ActiveQuestsCount(Object* quest_giver, Player* plr)
{
	std::list<QuestRelation *>::const_iterator itr;
	map<uint32, uint8> tmp_map;
	uint32 questCount = 0;

	std::list<QuestRelation *>::const_iterator q_begin;
	std::list<QuestRelation *>::const_iterator q_end;
	bool bValid = false;

	if(quest_giver->GetTypeId() == TYPEID_GAMEOBJECT)
	{
        bValid = ((GameObject*)quest_giver)->HasQuests();
		if(bValid)
		{
			q_begin = ((GameObject*)quest_giver)->QuestsBegin();
			q_end   = ((GameObject*)quest_giver)->QuestsEnd();
			
		}
	} 
	else if(quest_giver->GetTypeId() == TYPEID_UNIT)
	{
		bValid = ((Creature*)quest_giver)->HasQuests();
		if(bValid)
		{
			q_begin = ((Creature*)quest_giver)->QuestsBegin();
			q_end   = ((Creature*)quest_giver)->QuestsEnd();
		}
	}

	if(!bValid)
	{
		MyLog::log->debug("QUESTS: Warning, invalid NPC "I64FMT" specified for ActiveQuestsCount. TypeId: %d.", quest_giver->GetGUID(), quest_giver->GetTypeId());
		return 0;
	}

	for(itr = q_begin; itr != q_end; ++itr)
	{
		QuestRelation* qr = *itr;
		if (CalcQuestStatus(quest_giver, plr, qr) >= QMGR_QUEST_CHAT)
		{
			if (tmp_map.find(qr->qst->id) == tmp_map.end())
			{
				tmp_map.insert(std::map<uint32,uint8>::value_type(qr->qst->id, 1));
				questCount++;
			}
		}
	}

	return questCount;
}

void QuestMgr::BuildOfferReward(MSG_S2C::stQuestGiver_Offer_Reward* Msg, Quest* qst, Object* qst_giver, uint32 menutype)
{
	ItemPrototype * it;
	Msg->giver_guid	= qst_giver->GetGUID();
	Msg->questid		= qst->id;
	Msg->title		= qst->title;
	Msg->message		= qst->completiontext;

	Msg->hasNextQuest= (qst->next_quest_id ? uint32(1) : uint32(0));	  // next quest shit
	Msg->required_money	= uint32(0);										 // maybe required money

	if (qst->count_reward_choiceitem)
    {
        for(uint32 i = 0; i < 6; ++i)
        {
			MSG_S2C::stQuestItem Item;
            if(qst->reward_choiceitem[i])
            {
				it = ItemPrototypeStorage.LookupEntry(qst->reward_choiceitem[i]);
				if( it )
				{
					Item.item_entry = qst->reward_choiceitem[i];
	                Item.count		= qst->reward_choiceitemcount[i];
		            Item.DisplayInfoID = it->DisplayInfoID[0][0];
					Msg->vChoice.push_back(Item);
				}
            }
        }
    }
    

    if (qst->count_reward_item)
    {
        for(uint32 i = 0; i < 4; ++i)
        {
			MSG_S2C::stQuestItem Item;
            if(qst->reward_item[i])
            {
				Item.item_entry = qst->reward_item[i];
				Item.count		= qst->reward_itemcount[i];
				it = ItemPrototypeStorage.LookupEntry(qst->reward_choiceitem[i]);
				Item.DisplayInfoID = (it ? it->DisplayInfoID[0][0] : uint32(0));
				Msg->vReward.push_back(Item);
            }
        }
    }
	

	Msg->reward_money = qst->reward_money;
	Msg->reward_spell = qst->reward_spell;
}

void QuestMgr::BuildQuestDetails(MSG_S2C::stQuestGiver_Quest_Details* Msg, Quest* qst, uint64 questgiver_guid, uint32 menutype)
{
	std::map<uint32, uint8>::const_iterator itr;

	Msg->giver_guid	= questgiver_guid;
	Msg->quest_id	= qst->id;
	
	Msg->title		=  qst->title;
	Msg->details		=  qst->details;
	Msg->objectives	=  qst->objectives;

	ItemPrototype *ip;
	uint32 i;

	for(i = 0; i < 6; ++i)
	{
		ip = ItemPrototypeStorage.LookupEntry(qst->reward_choiceitem[i]);
		if(!qst->reward_choiceitem[i]) continue;

		MSG_S2C::stQuestItem Item;
		Item.item_entry = qst->reward_choiceitem[i];
		Item.count		= qst->reward_choiceitemcount[i];
		Item.DisplayInfoID = ip ? ip->DisplayInfoID[0][0] : 0;
		Msg->vChoice.push_back(Item);
	}

	for(i = 0; i < 4; ++i)
	{
		ip = ItemPrototypeStorage.LookupEntry(qst->reward_item[i]);
		if(!qst->reward_item[i]) continue;

		MSG_S2C::stQuestItem Item;
		Item.item_entry = qst->reward_item[i];
		Item.count		= qst->reward_itemcount[i];
		Item.DisplayInfoID = ip ? ip->DisplayInfoID[0][0] : 0;
		Msg->vReward.push_back(Item);
	}

	Msg->reward_money = qst->reward_money;
	Msg->reward_spell = qst->reward_spell;
}

void QuestMgr::BuildRequestItems(MSG_S2C::stQuestGiver_Request_Items* Msg, Quest* qst, Object* qst_giver, uint32 status)
{
	ItemPrototype * it;
	Msg->giver_guid	= qst_giver->GetGUID();
	Msg->questid		= qst->id;

	Msg->title		= qst->title;
	Msg->message		= (qst->incompletetext[0] ? qst->incompletetext : qst->details);
	Msg->emote		= ( QMGR_QUEST_FINISHED == status ) ? 1 : 0;
	Msg->required_money	= qst->required_money;	   // Required Money

	// item count
	Msg->count_required_item = qst->count_required_item;
	
	// (loop for each item)
	for(uint32 i = 0; i < 4; ++i)
	{
		if(qst->required_item[i] != 0)
		{
			MSG_S2C::stQuestItem Item;
			Item.item_entry = qst->required_item[i];
			Item.count		= qst->required_itemcount[i];
			it = ItemPrototypeStorage.LookupEntry(qst->required_item[i]);
			Item.DisplayInfoID = (it ? it->DisplayInfoID[0][0] : uint32(0));
			Msg->vItems.push_back(Item);
		}
	}
}

void QuestMgr::BuildQuestComplete(Player*plr, Quest* qst, uint32 choose_reward)
{
	uint32 xp ;
	if(plr->getLevel() >= plr->GetUInt32Value(PLAYER_FIELD_MAX_LEVEL))
	{
		plr->ModCoin(qst->reward_xp_as_money);
		MyLog::yunyinglog->info("gold-questcomplete-player[%u][%s] take gold[%u]", plr->GetLowGUID(), plr->GetName(), qst->reward_xp_as_money);
		xp = 0;
	}else
	{
		// 编辑器里面任务从“ID 700001 ～ ID 799999”现在设定为节日任务专用ID，经验奖励是按照人物接任务时等级经验条的百分比来计算
		if( qst->id >= 700001 && qst->id <= 799999 )
		{
			xp = plr->GetUInt32Value( PLAYER_NEXT_LEVEL_XP ) * qst->reward_xp / 100;
		}
		else
			xp = float2int32(GenerateQuestXP(plr,qst) * sWorld.getRate(RATE_QUESTXP));

		xp = (uint32)( (float)xp * (float)plr->m_ExtraEXPRatioQuest / 100.f );
		
		xp = plr->GiveXP(xp, 0, false,GIVE_EXP_TYPE_QUEST);
	}
  
	MSG_S2C::stQuestGiver_Quest_Complete Msg;


	Msg.questid = qst->id;
	Msg.xp		= xp;
	Msg.reward_money = uint32(qst->reward_money);
	Msg.reward_honor = (uint32)( (float)qst->reward_honor * (float)plr->m_ExtraHonorRatioQuest / 100.f );
	if(plr->GetSession()->m_ChenmiStat == 1)
	{
		Msg.reward_money /= 2;
		Msg.reward_honor /= 2;
	}
	else if(plr->GetSession()->m_ChenmiStat == 2)
	{
		Msg.reward_money = 0;
		Msg.reward_honor = 0;
	}
	Msg.count_reward_item = uint32(qst->count_reward_item); //Reward item count

	for(uint32 i = 0; i < 4; ++i)
	{
		MSG_S2C::stQuestItem Item;
		if(qst->reward_item[i])
		{
			Item.item_entry = qst->reward_item[i];
			Item.count = qst->reward_itemcount[i];
			Msg.vItems.push_back(Item);
		}
	}
	if( choose_reward < 6 )
	{
		MSG_S2C::stQuestItem Item;
		if(qst->reward_choiceitem[choose_reward])
		{
			Item.item_entry = qst->reward_choiceitem[choose_reward];
			Item.count = qst->reward_choiceitemcount[choose_reward];
			Msg.vItems.push_back(Item);
		}
	}
	plr->GetSession()->SendPacket(Msg);
}

void QuestMgr::BuildQuestList(MSG_S2C::stQuestGiver_Query_List* Msg, Object* qst_giver, Player *plr)
{
	uint32 status;
	list<QuestRelation *>::iterator it;
	list<QuestRelation *>::iterator st;
	list<QuestRelation *>::iterator ed;
	map<uint32, uint8> tmp_map;

	Msg->giver_guid	= qst_giver->GetGUID();
	Msg->hello_message = "How can I help you?"; //Hello line 
	Msg->EmoteDelay	= 1;//Emote Delay
	Msg->Emote		= 1;//Emote

	bool bValid = false;
	if(qst_giver->GetTypeId() == TYPEID_GAMEOBJECT)
	{
		bValid = ((GameObject*)qst_giver)->HasQuests();
		if(bValid)
		{
			st = ((GameObject*)qst_giver)->QuestsBegin();
			ed = ((GameObject*)qst_giver)->QuestsEnd();
		}
	} 
	else if(qst_giver->GetTypeId() == TYPEID_UNIT)
	{
		bValid = ((Creature*)qst_giver)->HasQuests();
		if(bValid)
		{
			st = ((Creature*)qst_giver)->QuestsBegin();
			ed = ((Creature*)qst_giver)->QuestsEnd();
		}
	}

	Msg->bValid	= bValid;
	if(!bValid)
	{
		return;
	}
	
	Msg->QuestCount = uint8(sQuestMgr.ActiveQuestsCount(qst_giver, plr));

	for (it = st; it != ed; ++it)
	{
		status = sQuestMgr.CalcQuestStatus(qst_giver, plr, *it);
		if (status >= QMGR_QUEST_CHAT)
		{
			if (tmp_map.find((*it)->qst->id) == tmp_map.end())
			{
				tmp_map.insert(std::map<uint32,uint8>::value_type((*it)->qst->id, 1));
				MSG_S2C::stQuest Quest;
				Quest.quest_id = (*it)->qst->id;
				/**data << sQuestMgr.CalcQuestStatus(qst_giver, plr, *it);
				*data << uint32(0);*/
				
				switch(status)
				{
// 				case QMGR_QUEST_NOT_FINISHED:
// 					Quest.stat = /*QMGR_QUEST_REPEATABLE_FINISHED*/QMGR_QUEST_NOT_FINISHED ;
// 					break;
// 
// 				case QMGR_QUEST_FINISHED:
// 					Quest.stat = /*QMGR_QUEST_REPEATABLE_FINISHED*/QMGR_QUEST_FINISHED ;
// 					break;
// 
				case QMGR_QUEST_CHAT:
					Quest.stat = uint32( QMGR_QUEST_AVAILABLE );
					break;

				default:
					Quest.stat = status;
				}
				Quest.title = (*it)->qst->title;
				Msg->vQuests.push_back(Quest);
			}
		}
	}
}

void QuestMgr::BuildQuestUpdateAddItem(MSG_S2C::stQuest_Update_Add_Item* Msg, uint32 itemid, uint32 count)
{
	Msg->item_entry = itemid;
	Msg->count;
}

void QuestMgr::SendQuestUpdateAddKill(Player* plr, uint32 questid, uint32 entry, uint32 count, uint32 tcount, uint64 guid)
{
	MSG_S2C::stQuest_Update_Add_Skill Msg;
	Msg.questid = questid;
	Msg.entry	= entry;
	Msg.count	= count;
	Msg.tcount	= tcount;
	Msg.guid	= guid;
	plr->GetSession()->SendPacket(Msg);
}

void QuestMgr::BuildQuestUpdateComplete(MSG_S2C::stQuest_Update_Complete* Msg, Quest* qst)
{
	Msg->questid = qst->id;
}

void QuestMgr::SendPushToPartyResponse(Player *plr, Player* pTarget, uint32 response)
{
	MSG_S2C::stQuest_Push_Result Msg;
	Msg.guid	= pTarget->GetGUID();
	Msg.share_result		= response;
	pTarget->GetSession()->SendPacket(Msg);
}

bool QuestMgr::OnGameObjectActivate(Player *plr, GameObject *go)
{
	uint32 i, j;
	QuestLogEntry *qle;
	uint32 entry = go->GetEntry();

	for(i = 0; i < QUEST_MAX_COUNT; ++i)
	{
		qle = plr->GetQuestLogInSlot( i );
		if( qle != NULL )
		{
			// dont waste time on quests without mobs
			if( qle->GetQuest()->count_required_mob == 0 )
				continue;

			for( j = 0; j < 4; ++j )
			{
				if( qle->GetQuest()->required_mob[j] == entry &&
					qle->GetQuest()->required_mobtype[j] == QUEST_MOB_TYPE_GAMEOBJECT &&
					qle->m_mobcount[j] < qle->GetQuest()->required_mobcount[j] )
				{
					// add another kill.
					// (auto-dirtys it)
					qle->SetMobCount( j, qle->m_mobcount[j] + 1 );
					qle->SendUpdateAddKill( j );

					if( qle->CanBeFinished() )
					{
						qle->SendQuestComplete();
						qle->UpdatePlayerFields(true);
					}
					else
						qle->UpdatePlayerFields();

					return true;
				}
			}
		}
	}
	return false;
}

void QuestMgr::OnPlayerKill(Player* plr, Creature* victim)
{
	if(!plr)
		return;

	uint32 i, j;
	uint32 entry = victim->GetEntry();
	QuestLogEntry *qle;

	if (plr->HasQuestMob(entry))
	{
		for(i = 0; i < QUEST_MAX_COUNT; ++i)
		{
			qle = plr->GetQuestLogInSlot( i );
			if( qle != NULL )
			{
				// dont waste time on quests without mobs
				if( qle->GetQuest()->count_required_mob == 0 )
					continue;

				for( j = 0; j < 4; ++j )
				{
					if( qle->GetQuest()->required_mob[j] == entry &&
						qle->GetQuest()->required_mobtype[j] == QUEST_MOB_TYPE_CREATURE &&
						qle->m_mobcount[j] < qle->GetQuest()->required_mobcount[j] )
					{
						// add another kill.(auto-dirtys it)
						qle->SetMobCount( j, qle->m_mobcount[j] + 1 );
						qle->SendUpdateAddKill( j );

						if( qle->CanBeFinished() )
						{
							qle->SendQuestComplete();
							qle->UpdatePlayerFields(true);
						}
						else
							qle->UpdatePlayerFields();
						break;
					}
				}
			}
		}
	}

	// Shared kills
	Player *gplr = NULL;

	if(plr->InGroup())
	{
		if(Group* pGroup = plr->GetGroup())
		{
//			removed by Zack How the hell will healers get the kills then ?
//			if(pGroup->GetGroupType() != GROUP_TYPE_PARTY) 
//				return;  // Raid's don't get shared kills.

			GroupMembersSet::iterator gitr;
			for(uint32 k = 0; k < pGroup->GetSubGroupCount(); k++)
			{
				for(gitr = pGroup->GetSubGroup(k)->GetGroupMembersBegin(); gitr != pGroup->GetSubGroup(k)->GetGroupMembersEnd(); ++gitr)
				{
					gplr = (*gitr)->m_loggedInPlayer;
					if(gplr && gplr != plr && plr->IsInRangeSet(gplr) && gplr->HasQuestMob(entry)) // dont double kills also dont give kills to party members at another side of the world
					{
						for( i = 0; i < QUEST_MAX_COUNT; ++i )
						{
							qle = gplr->GetQuestLogInSlot(i);
							if( qle != NULL )
							{
								// dont waste time on quests without mobs
								if( qle->GetQuest()->count_required_mob == 0 )
									continue;

								for( j = 0; j < 4; ++j )
								{
									if( qle->GetQuest()->required_mob[j] == entry &&
										qle->GetQuest()->required_mobtype[j] == QUEST_MOB_TYPE_CREATURE &&
										qle->m_mobcount[j] < qle->GetQuest()->required_mobcount[j] )
									{
										// add another kill.
										// (auto-dirtys it)
										qle->SetMobCount(j, qle->m_mobcount[j] + 1);
										qle->SendUpdateAddKill( j );
										if( qle->CanBeFinished() )
										{
											qle->SendQuestComplete();
											qle->UpdatePlayerFields(true);
										}
										else
											qle->UpdatePlayerFields();

										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void QuestMgr::OnPlayerCast(Player* plr, uint32 spellid, uint64& victimguid)
{
	if(!plr || !plr->HasQuestSpell(spellid))
		return;

	Unit * victim = plr->GetMapMgr() ? plr->GetMapMgr()->GetUnit(victimguid) : NULL;
	if(victim==NULL)
		return;

	uint32 i, j;
	uint32 entry = victim->GetEntry();
	QuestLogEntry *qle;
	for(i = 0; i < QUEST_MAX_COUNT; ++i)
	{
		if((qle = plr->GetQuestLogInSlot(i)))
		{
			// dont waste time on quests without casts
			if(!qle->IsCastQuest())
				continue;

			for(j = 0; j < 4; ++j)
			{
				if(qle->GetQuest()->required_mob[j] == entry &&
					qle->GetQuest()->required_spell[j] == spellid &&
					qle->m_mobcount[j] < qle->GetQuest()->required_mobcount[j] &&
					!qle->IsUnitAffected(victim))
				{
					// add another kill.(auto-dirtys it)
					qle->AddAffectedUnit(victim);
					qle->SetMobCount(j, qle->m_mobcount[j] + 1);
					qle->SendUpdateAddKill(j);
					qle->UpdatePlayerFields();
					break;
				}
			}
		}
	}
}



void QuestMgr::OnPlayerItemPickup(Player* plr, Item* item)
{
	if( !LockQstItemNotify::s_bLockQstItemNotify )
		return;

	if( item->GetProto()->Bonding == ITEM_BIND_ON_PICKUP && plr->m_bRecvLoadingOK && !item->IsSoulbound() )
	{
		item->SoulBind();
		item->m_isDirty;
	}

	uint32 i, j;
	uint32 pcount;
	uint32 entry = item->GetEntry();
	QuestLogEntry *qle;
	for( i = 0; i < QUEST_MAX_COUNT; ++i )
	{
		if( ( qle = plr->GetQuestLogInSlot( i ) ) )
		{
			if( qle->GetQuest()->count_required_item == 0 )
				continue;

			for( j = 0; j < 4; ++j )
			{
				if( qle->GetQuest()->required_item[j] == entry )
				{
					pcount = plr->GetItemInterface()->GetItemCount(entry, true);
					if(pcount <= qle->GetQuest()->required_itemcount[j])
					{
						MSG_S2C::stQuest_Update_Add_Item Msg;
						Msg.item_entry	= qle->GetQuest()->required_item[j];
						Msg.count		= pcount;
						Msg.tcount		= qle->GetQuest()->required_itemcount[j];
						plr->GetSession()->SendPacket(Msg);
						if(qle->CanBeFinished())
						{
							plr->UpdateNearbyGameObjects();
							qle->SendQuestComplete();
						}
						qle->SetDirty(true);
						qle->SetMobCount(j, pcount);
						qle->UpdatePlayerFields();
						break;
					}
					else
					{
						MSG_S2C::stQuest_Update_Add_Item Msg;
						Msg.item_entry	= qle->GetQuest()->required_item[j];
						Msg.count		= qle->GetQuest()->required_itemcount[j];
						Msg.tcount		= qle->GetQuest()->required_itemcount[j];
						plr->GetSession()->SendPacket(Msg);
						if(qle->CanBeFinished())
						{
							plr->UpdateNearbyGameObjects();
							qle->SendQuestComplete();
						}
						qle->SetDirty(true);
						qle->SetMobCount(j, Msg.count);
						qle->UpdatePlayerFields();
						break;
					}
				}
			}
		}
	}
}



void QuestMgr::OnPlayerItemLost(Player* plr, uint32 entry, uint32 cnt)
{
	if( !LockQstItemNotify::s_bLockQstItemNotify )
		return;

	uint32 i, j;
	uint32 pcount;
	QuestLogEntry *qle;
	for( i = 0; i < QUEST_MAX_COUNT; ++i )
	{
		if( ( qle = plr->GetQuestLogInSlot( i ) ) )
		{
			if( qle->GetQuest()->count_required_item == 0 )
				continue;

			for( j = 0; j < 4; ++j )
			{
				if( qle->GetQuest()->required_item[j] == entry )
				{
					pcount = plr->GetItemInterface()->GetItemCount(entry, true);
					if(pcount <= qle->GetQuest()->required_itemcount[j])
					{
						qle->SetDirty(true);
						qle->SetMobCount(j, pcount);
						qle->UpdatePlayerFields();
						break;
					}
				}
			}
		}
	}
}


void QuestMgr::OnPlayerExploreArea(Player* plr, uint32 AreaID)
{
	uint32 i, j;
	QuestLogEntry *qle;
	for( i = 0; i < QUEST_MAX_COUNT; ++i )
	{
		if((qle = plr->GetQuestLogInSlot(i)))
		{
			// dont waste time on quests without triggers
			if( qle->GetQuest()->count_requiredtriggers == 0 )
				continue;

			for( j = 0; j < 4; ++j )
			{
				if(qle->GetQuest()->required_triggers[j] == AreaID &&
					!qle->m_explored_areas[j])
				{
					qle->SetTrigger(j);
					if( qle->CanBeFinished() )
					{
						plr->UpdateNearbyGameObjects();
						qle->SendQuestComplete();
						qle->UpdatePlayerFields(true);
					}
					else
						qle->UpdatePlayerFields();

					break;
				}
			}
		}
	}
}

void QuestMgr::OnQuestAccepted(Player* plr, Quest* qst, Object *qst_giver)
{
	if( qst->quest_flags & QUEST_FLAG_ESCORT && qst_giver->IsCreature() )
	{
		escort_info* ei = GetEscortInfo( qst->id, 0 );
		if( ei )
		{
			((Creature*)qst_giver)->m_escort_qst = qst;
			((Creature*)qst_giver)->m_escort_players.clear();
			((Creature*)qst_giver)->m_escort_players.insert( plr->m_playerInfo );
			((Creature*)qst_giver)->GetAIInterface()->m_lastReachWP = 0;

			OnEscortReachWayPoint( (Creature*)qst_giver, 0 );
			((Creature*)qst_giver)->m_escort_finished = false;
		}
	}
	else
	{
		QuestLogEntry* qle = plr->GetQuestLogForEntry( qst->id );
		if( !qle )
			return;

		bool bUpdate = false;
		for( int j = 0; j < 4; ++j )
		{
			if( qst->required_item[j] )
			{
				int itemcount = plr->GetItemInterface()->GetItemCount( qst->required_item[j] );
				if( itemcount == 0 )
					continue;

				qle->SetDirty(true);
				if( itemcount < qst->required_itemcount[j] )
					qle->SetMobCount(j, itemcount);
				else
					qle->SetMobCount(j, qst->required_itemcount[j]);

				bUpdate = true;
			}
		}

		if( bUpdate )
		{
			if(qle->CanBeFinished())
			{
				plr->UpdateNearbyGameObjects();
				qle->SendQuestComplete( false );
			}
			qle->UpdatePlayerFields();
		}
	}
	
	plr->UpdateInrangeQuestGiverState();
}

void QuestMgr::OnQuestGiveuped(Player* plr, Quest* qst)
{
	plr->UpdateInrangeQuestGiverState();
}

void QuestMgr::OnQuestFinished(Player* plr, Quest* qst, Object *qst_giver, uint32 reward_slot)
{
//Re-Check for Gold Requirement (needed for possible xploit)
    if(qst->required_money && (plr->GetUInt32Value(PLAYER_FIELD_COINAGE) < qst->required_money)) 
       return;
    QuestLogEntry *qle = NULL;
    //if(!qst->is_repeatable)
    {
	    qle = plr->GetQuestLogForEntry(qst->id);
	    if(!qle)
		    return;
    }

	uint32 reward_honor = qst->reward_honor;
	uint32 reward_money = qst->reward_money;
	if(plr->GetSession()->m_ChenmiStat == 1)
	{
		reward_honor /= 2;
		reward_money /= 2;
	}
	else if(plr->GetSession()->m_ChenmiStat == 2)
	{
		reward_honor = 0;
		reward_money = 0;
	}

    BuildQuestComplete(plr, qst, reward_slot);
    //if(!qst->is_repeatable) 
	//if(!qst->is_repeatable) 
	{
		for (uint32 x=0;x<4;x++)
		{
			if (qst->required_spell[x]!=0)
			{
				if (plr->HasQuestSpell(qst->required_spell[x]))
					plr->RemoveQuestSpell(qst->required_spell[x]);
			}
			else if (qst->required_mob[x]!=0)
			{
				if (plr->HasQuestMob(qst->required_mob[x]))
					plr->RemoveQuestMob(qst->required_mob[x]);
			} 
		}


		if(qst->required_honor)
		{
			plr->m_honorPoints -= qst->required_honor;
			plr->ModHonorCurrency(-(int)qst->required_honor);
		}

		qle->ClearAffectedUnits();
		qle->Finish();
	}
	
	if(qst_giver->GetTypeId() == TYPEID_UNIT)
	{
		if(!((Creature*)qst_giver)->HasQuest(qst->id, 2))
		{
			//sCheatLog.writefromsession(plr->GetSession(), "tried to finish quest from invalid npc.");
			plr->GetSession()->Disconnect();
			return;
		}
	}

	if(IsDailyQuest(qst))
		plr->AddFinishedDailyQuest( qst->id );

    //details: hmm as i can remember, repeatable quests give faction rep still after first completation
    if(IsQuestRepeatable(qst))
    {
        // Static Item reward
	    for(uint32 i = 0; i < 4; ++i)
	    {
		    if(qst->reward_item[i])
		    {
			    ItemPrototype *proto = ItemPrototypeStorage.LookupEntry(qst->reward_item[i]);
			    if(!proto)
			    {
				    MyLog::log->error("Invalid item prototype in quest reward! ID %d, quest %d", qst->reward_item[i], qst->id);
			    }
			    else
			    {
					Item *add;
					SlotResult slotresult;
					add = plr->GetItemInterface()->FindItemLessMax(qst->reward_item[i], qst->reward_itemcount[i], false);
					if (!add)
					{
						slotresult = plr->GetItemInterface()->FindFreeInventorySlot(proto);
						if(!slotresult.Result)
						{
							plr->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_INVENTORY_FULL);
						}
						else
						{
							Item *itm = objmgr.CreateItem(qst->reward_item[i], plr);
							itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, uint32(qst->reward_itemcount[i]));
							if( !plr->GetItemInterface()->SafeAddItem(itm,slotresult.ContainerSlot, slotresult.Slot) )
								delete itm;

							MyLog::yunyinglog->info("item-questreward-player[%u][%s] item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), itm->GetGUID(), itm->GetProto()->ItemId, qst->reward_itemcount[i]);
						}
					}
					else
					{
						add->SetCount(add->GetUInt32Value(ITEM_FIELD_STACK_COUNT) + qst->reward_itemcount[i]);
						add->m_isDirty = true;
						sQuestMgr.OnPlayerItemPickup(plr,add);
						MyLog::yunyinglog->info("item-questreward-player[%u][%s] item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), add->GetGUID(), add->GetProto()->ItemId, qst->reward_itemcount[i]);
					}
			    }
		    }
	    }

	    // Choice Rewards
	    if(qst->reward_choiceitem[reward_slot])
	    {
		    ItemPrototype *proto = ItemPrototypeStorage.LookupEntry(qst->reward_choiceitem[reward_slot]);
		    if(!proto)
		    {
			    MyLog::log->error("Invalid item prototype in quest reward! ID %d, quest %d", qst->reward_choiceitem[reward_slot], qst->id);
		    }
		    else
		    {
				Item *add;
				SlotResult slotresult;
				add = plr->GetItemInterface()->FindItemLessMax(qst->reward_choiceitem[reward_slot], qst->reward_choiceitemcount[reward_slot], false);
				if (!add)
				{
					slotresult = plr->GetItemInterface()->FindFreeInventorySlot(proto);
					if(!slotresult.Result)
					{
						plr->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_INVENTORY_FULL);
					}
					else
					{
						Item *itm = objmgr.CreateItem(qst->reward_choiceitem[reward_slot], plr);
						itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, uint32(qst->reward_choiceitemcount[reward_slot]));
						if( !plr->GetItemInterface()->SafeAddItem(itm,slotresult.ContainerSlot, slotresult.Slot) )
							delete itm;
						MyLog::yunyinglog->info("item-questchoice-player[%u][%s] item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), itm->GetGUID(), itm->GetProto()->ItemId, qst->reward_choiceitemcount[reward_slot]);
					}
				}
				else
				{
					add->SetCount(add->GetUInt32Value(ITEM_FIELD_STACK_COUNT) + qst->reward_choiceitemcount[reward_slot]);
					add->m_isDirty = true;
					sQuestMgr.OnPlayerItemPickup(plr,add);
					MyLog::yunyinglog->info("item-questchoice-player[%u][%s] item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), add->GetGUID(), add->GetProto()->ItemId, qst->reward_choiceitemcount[reward_slot]);
				}
		    }
	    }

	    // Remove items
	    for(uint32 i = 0; i < 4; ++i)
	    {
			if(qst->required_item[i])
			{
				plr->GetItemInterface()->RemoveItemAmt(qst->required_item[i],qst->required_itemcount[i]);
				MyLog::yunyinglog->info("item-questrequire-player[%u][%s] item[0][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), qst->required_item[i], qst->required_itemcount[i]);
			}
	    }

	    // Remove srcitem
	    if(qst->srcitem && qst->srcitem != qst->receive_items[0])
		{
			plr->GetItemInterface()->RemoveItemAmt(qst->srcitem, qst->srcitemcount ? qst->srcitemcount : 1);
			MyLog::yunyinglog->info("item-questsrcitem-player[%u][%s] item[0][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), qst->srcitem, qst->srcitemcount ? qst->srcitemcount : 1);
		}

        // cast Effect Spell
	    if(qst->effect_on_player)
	    {
		    SpellEntry  * inf =dbcSpell.LookupEntry(qst->effect_on_player);
		    if(inf)
		    {
			    Spell * spe = new Spell(qst_giver,inf,true,NULL);
			    SpellCastTargets tgt;
			    tgt.m_unitTarget = plr->GetGUID();
			    spe->prepare(&tgt);
		    }
	    }
   }
    else
    { 	
	    // Static Item reward
	    for(uint32 i = 0; i < 4; ++i)
	    {
		    if(qst->reward_item[i])
		    {
			    ItemPrototype *proto = ItemPrototypeStorage.LookupEntry(qst->reward_item[i]);
			    if(!proto)
			    {
				    MyLog::log->error("Invalid item prototype in quest reward! ID %d, quest %d", qst->reward_item[i], qst->id);
			    }
			    else
			    {   
					Item *add;
					SlotResult slotresult;
					add = plr->GetItemInterface()->FindItemLessMax(qst->reward_item[i], qst->reward_itemcount[i], false);
					if (!add)
					{
						slotresult = plr->GetItemInterface()->FindFreeInventorySlot(proto);
						if(!slotresult.Result)
						{
							plr->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_INVENTORY_FULL);
						}
						else
						{
							Item *itm = objmgr.CreateItem(qst->reward_item[i], plr);
							itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, uint32(qst->reward_itemcount[i]));
							if( !plr->GetItemInterface()->SafeAddItem(itm,slotresult.ContainerSlot, slotresult.Slot) )
								delete itm;
							MyLog::yunyinglog->info("item-questreward-player[%u][%s] item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), itm->GetGUID(), itm->GetProto()->ItemId, qst->reward_itemcount[i]);
						}
					}
					else
					{
						add->SetCount(add->GetUInt32Value(ITEM_FIELD_STACK_COUNT) + qst->reward_itemcount[i]);
						add->m_isDirty = true;
						sQuestMgr.OnPlayerItemPickup(plr,add);
						MyLog::yunyinglog->info("item-questreward-player[%u][%s] item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), add->GetGUID(), add->GetProto()->ItemId, qst->reward_itemcount[i]);
					}
			    }
		    }
	    }

	    // Choice Rewards
	    if(qst->reward_choiceitem[reward_slot])
	    {
		    ItemPrototype *proto = ItemPrototypeStorage.LookupEntry(qst->reward_choiceitem[reward_slot]);
		    if(!proto)
		    {
			    MyLog::log->error("Invalid item prototype in quest reward! ID %d, quest %d", qst->reward_choiceitem[reward_slot], qst->id);
		    }
		    else
		    {
				Item *add;
				SlotResult slotresult;
				add = plr->GetItemInterface()->FindItemLessMax(qst->reward_choiceitem[reward_slot], qst->reward_choiceitemcount[reward_slot], false);
				if (!add)
				{
					slotresult = plr->GetItemInterface()->FindFreeInventorySlot(proto);
					if(!slotresult.Result)
					{
						plr->GetItemInterface()->BuildInventoryChangeError(NULL, NULL, INV_ERR_INVENTORY_FULL);
					}
					else 
					{
						Item *itm = objmgr.CreateItem(qst->reward_choiceitem[reward_slot], plr);
						itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, uint32(qst->reward_choiceitemcount[reward_slot]));
						if( !plr->GetItemInterface()->SafeAddItem(itm,slotresult.ContainerSlot, slotresult.Slot) )
							delete itm;
						MyLog::yunyinglog->info("item-questchoice-player[%u][%s] item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), itm->GetGUID(), itm->GetProto()->ItemId, qst->reward_choiceitemcount[reward_slot]);
					}
				}
				else
				{
					add->SetCount(add->GetUInt32Value(ITEM_FIELD_STACK_COUNT) + qst->reward_choiceitemcount[reward_slot]);
					add->m_isDirty = true;
					sQuestMgr.OnPlayerItemPickup(plr,add);
					MyLog::yunyinglog->info("item-questchoice-player[%u][%s] item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), add->GetGUID(), add->GetProto()->ItemId, qst->reward_choiceitemcount[reward_slot]);
				}
		    }
	    }

	    // Remove items
	    for(uint32 i = 0; i < 4; ++i)
	    {
		    if(qst->required_item[i]) plr->GetItemInterface()->RemoveItemAmt(qst->required_item[i],qst->required_itemcount[i]);
			MyLog::yunyinglog->info("item-questrequire-player[%u][%s] item[%u] count[%u]", plr->GetLowGUID(), plr->GetName(), qst->required_item[i], qst->required_itemcount[i]);
	    }

			for(uint32 i = 0; i < 4; ++i)
			{
				if(qst->required_money && (plr->GetUInt32Value(PLAYER_FIELD_COINAGE) >= qst->required_money))
				{
					plr->ModCoin(qst->required_money);
					MyLog::yunyinglog->info("gold-questcomplete-player[%u][%s] take gold[%u]", plr->GetLowGUID(), plr->GetName(), qst->reward_money);
				}
			}

	    // Remove srcitem
	    if(qst->srcitem && qst->srcitem != qst->receive_items[0])
		{
			plr->GetItemInterface()->RemoveItemAmt(qst->srcitem, qst->srcitemcount ? qst->srcitemcount : 1);
			MyLog::yunyinglog->info("item-questsrcitem-player[%u][%s] item[%u] count[%u]", plr->GetLowGUID(), plr->GetName(), qst->srcitem, qst->srcitemcount ? qst->srcitemcount : 1);
		}

	    // cast learning spell
	    if(qst->reward_spell)
	    {
		    if(!plr->HasSpell(qst->reward_spell))
		    {
			    // "Teaching" effect
				MSG_S2C::stSpell_Start MsgStart;
				MsgStart.caster_item_guid	= qst_giver->GetNewGUID();
				MsgStart.caster_unit_guid	= qst_giver->GetNewGUID();
				MsgStart.spell_id			= 7763;
				MsgStart.extra_cast_number	= 0;
				MsgStart.cast_flags			= 0;
				MsgStart.casttime			= 0;
				MsgStart.targets.m_targetMask		= 2;
				MsgStart.targets.m_unitTarget	= plr->GetGUID();
			    plr->GetSession()->SendPacket( MsgStart );

				MSG_S2C::stSpell_Go MsgGo;
				MsgGo.caster_item_guid		= qst_giver->GetNewGUID();
				MsgGo.caster_unit_guid		= qst_giver->GetNewGUID();
				MsgGo.spell_id				= 7763;
				MsgGo.castFlags				= 1;
				MsgGo.casttime				= getMSTime();
				MsgGo.targets.m_targetMask			= TARGET_FLAG_UNIT;
				MsgGo.vTargetHit.push_back(plr->GetGUID());

			    plr->GetSession()->SendPacket( MsgGo );

			    // Teach the spell
			    plr->addSpell(qst->reward_spell);
		    }
	    }

	    // cast Effect Spell
	    if(qst->effect_on_player)
	    {
		    SpellEntry  * inf =dbcSpell.LookupEntry(qst->effect_on_player);
		    if(inf)
		    {
			    Spell * spe = new Spell(qst_giver,inf,true,NULL);
			    SpellCastTargets tgt;
			    tgt.m_unitTarget = plr->GetGUID();
			    spe->prepare(&tgt);
		    }
	    }

	    //Add to finished quests
	    plr->AddToFinishedQuests(qst->id);
    }

	if(reward_honor)
	{
		reward_honor = (uint32)( (float)reward_honor * (float)plr->m_ExtraHonorRatioQuest / 100.f );
		plr->ModHonorCurrency(reward_honor);
		plr->ModTotalHonor(reward_honor);
	}

	plr->ModCoin(reward_money);
	MyLog::yunyinglog->info("gold-questcomplete-player[%u][%s] take gold[%u]", plr->GetLowGUID(), plr->GetName(), reward_money);

	plr->UpdateInrangeQuestGiverState();
}

/////////////////////////////////////
//		Quest Management		 //
/////////////////////////////////////

void QuestMgr::LoadNPCQuests(Creature *qst_giver)
{
	qst_giver->SetQuestList(GetCreatureQuestList(qst_giver->GetEntry()));
}

void QuestMgr::LoadGOQuests(GameObject *go)
{
	go->SetQuestList(GetGOQuestList(go->GetEntry()));
}

QuestRelationList* QuestMgr::GetGOQuestList(uint32 entryid)
{
	HM_NAMESPACE::hash_map<uint32, QuestRelationList* > &olist = _GetList<GameObject>();
	HM_NAMESPACE::hash_map<uint32, QuestRelationList* >::iterator itr = olist.find(entryid);
	return (itr == olist.end()) ? 0 : itr->second;
}

QuestRelationList* QuestMgr::GetCreatureQuestList(uint32 entryid)
{
	HM_NAMESPACE::hash_map<uint32, list<QuestRelation *>* > &olist = _GetList<Creature>();
	HM_NAMESPACE::hash_map<uint32, QuestRelationList* >::iterator itr = olist.find(entryid);
	return (itr == olist.end()) ? 0 : itr->second;
}

template <class T> void QuestMgr::_AddQuest(uint32 entryid, Quest *qst, uint8 type)
{
	HM_NAMESPACE::hash_map<uint32, list<QuestRelation *>* > &olist = _GetList<T>();
	std::list<QuestRelation *>* nlist;
	QuestRelation *ptr = NULL;

	if (olist.find(entryid) == olist.end())
	{
		nlist = new std::list<QuestRelation *>;

		olist.insert(HM_NAMESPACE::hash_map<uint32, list<QuestRelation *>* >::value_type(entryid, nlist));
	}
	else
	{
		nlist = olist.find(entryid)->second;
	}

	list<QuestRelation *>::iterator it;
	for (it = nlist->begin(); it != nlist->end(); ++it)
	{
		if ((*it)->qst == qst)
		{
			ptr = (*it);
			break;
		}
	}

	if (ptr == NULL)
	{
		ptr = new QuestRelation;
		ptr->qst = qst;
		ptr->type = type;

		nlist->push_back(ptr);
	}
	else
	{
		ptr->type |= type;
	}
}



void QuestMgr::_CleanLine(std::string *str) 
{
	_RemoveChar((char*)"\r", str);
	_RemoveChar((char*)"\n", str);

	while (str->c_str()[0] == 32) 
	{
		str->erase(0,1);
	}
}

void QuestMgr::_RemoveChar(char *c, std::string *str) 
{
	string::size_type pos = str->find(c,0);

	while (pos != string::npos)
	{
		str->erase(pos, 1);
		pos = str->find(c, 0);
	}	
}

uint32 QuestMgr::GenerateQuestXP(Player *plr, Quest *qst)	
{	
// 	if(qst->is_repeatable)
// 		return 0;	
	{	
  //if( plr->getLevel() <= qst->max_level +  5 )
      return qst->reward_xp;	
//   if( plr->getLevel() == qst->max_level +  6 )
//       return (uint32)(qst->reward_xp * 0.8);
//   if( plr->getLevel() == qst->max_level +  7 )
//       return (uint32)(qst->reward_xp * 0.6);
//   if( plr->getLevel() == qst->max_level +  8 )
//       return (uint32)(qst->reward_xp * 0.4);
//   if( plr->getLevel() == qst->max_level +  9 )
//       return (uint32)(qst->reward_xp * 0.2);
// 		     
//   else
//       return 0;
   }   
}
/*
#define XP_INC 50
#define XP_DEC 10
#define XP_INC100 15
#define XP_DEC100 5
	double xp, pxp, mxp, mmx;

	// hack fix
	xp  = qst->max_level * XP_INC;
	if(xp <= 0)
		xp = 1;

	pxp  = xp + (xp / 100) * XP_INC100;

	xp   = XP_DEC;

	mxp  = xp + (xp / 100) * XP_DEC100;

	mmx = (pxp - mxp);

	if(qst->quest_flags & QUEST_FLAG_SPEAKTO)
		mmx *= 0.6;
	if(qst->quest_flags & QUEST_FLAG_TIMED)
		mmx *= 1.1;
	if(qst->quest_flags & QUEST_FLAG_EXPLORATION)
		mmx *= 1.2;

	if(mmx < 0)
		return 1;

	mmx *= sWorld.getRate(RATE_QUESTXP);
	return (int)mmx;*/


void QuestMgr::SendQuestInvalid(INVALID_REASON reason, Player *plyr)
{
	if(!plyr)
		return;
	MSG_S2C::stQuestGiver_Quest_Invalid Msg;
	Msg.invalid_reason = reason;
	plyr->GetSession()->SendPacket( Msg );

	MyLog::log->debug("WORLD:Sent SMSG_QUESTGIVER_QUEST_INVALID");
}

void QuestMgr::SendQuestFailed(FAILED_REASON failed, Quest * qst, Player *plyr, uint8 baccept)
{
	if(!plyr)
		return;

	MSG_S2C::stQuestGiver_Quest_Failed Msg;
	Msg.questid = qst->id;
	Msg.failed_reason = failed;
	Msg.baccept = baccept;
    plyr->GetSession()->SendPacket(Msg);
	MyLog::log->debug("WORLD:Sent SMSG_QUESTGIVER_QUEST_FAILED");
}

void QuestMgr::SendQuestUpdateFailedTimer(Quest *pQuest, Player *plyr)
{
	if(!plyr)
		return;

	MSG_S2C::stQuest_Update_FailedTimer Msg;
	Msg.questid = pQuest->id;
	plyr->GetSession()->SendPacket(Msg);

	MyLog::log->debug("WORLD:Sent SMSG_QUESTUPDATE_FAILEDTIMER");
}

void QuestMgr::SendQuestUpdateFailed(Quest *pQuest, Player *plyr)
{
	if(!plyr)
		return;

	MSG_S2C::stQuest_Update_Failed Msg;
	Msg.questid = pQuest->id;
	plyr->GetSession()->SendPacket(Msg);
	MyLog::log->debug("WORLD:Sent SMSG_QUESTUPDATE_FAILED");
}

void QuestMgr::SendQuestLogFull(Player *plyr)
{
	if(!plyr)
		return;

	MSG_S2C::stQuest_Log_Full Msg;
	plyr->GetSession()->SendPacket(Msg);
	MyLog::log->debug("WORLD:Sent QUEST_LOG_FULL_MESSAGE");
}

uint32 QuestMgr::GetGameObjectLootQuest(uint32 GO_Entry)
{
	HM_NAMESPACE::hash_map<uint32, uint32>::iterator itr = m_ObjectLootQuestList.find(GO_Entry);
	if(itr == m_ObjectLootQuestList.end()) return 0;
	
	return itr->second;
}

void QuestMgr::SetGameObjectLootQuest(uint32 GO_Entry, uint32 Item_Entry)
{
	if(m_ObjectLootQuestList.find(GO_Entry) != m_ObjectLootQuestList.end())
	{
		//MyLog::log->error("WARNING: Gameobject %d has more than 1 quest item allocated in it's loot template!", GO_Entry);
	}

	// Find the quest that has that item
	uint32 QuestID = 0;
	uint32 i;
	StorageContainerIterator<Quest> * itr = QuestStorage.MakeIterator();
	while(!itr->AtEnd())
	{
		Quest *qst = itr->Get();
		for(i = 0; i < 4; ++i)
		{
			if(qst->required_item[i] == Item_Entry)
			{
				QuestID = qst->id;
				m_ObjectLootQuestList[GO_Entry] = QuestID;
				itr->Destruct();
				return;
			}
		}
		if(!itr->Inc())
			break;
	}
	itr->Destruct();

	//MyLog::log->error("WARNING: No coresponding quest was found for quest item %d", Item_Entry);
}

void QuestMgr::BuildQuestFailed(MSG_S2C::stQuest_Update_Failed* Msg, uint32 questid)
{
	Msg->questid = questid;
}

bool QuestMgr::OnActivateQuestGiver(Object *qst_giver, Player *plr)
{
	if(qst_giver->GetTypeId() == TYPEID_GAMEOBJECT && !((GameObject*)qst_giver)->HasQuests())
		return false;

	uint32 questCount = sQuestMgr.ActiveQuestsCount(qst_giver, plr);

	if (questCount == 0) 
	{
		MyLog::log->debug("WORLD: Invalid NPC for CMSG_QUESTGIVER_HELLO.");
		return false;
	}
	else if (questCount == 1)
	{
		std::list<QuestRelation *>::const_iterator itr;
		std::list<QuestRelation *>::const_iterator q_begin;
		std::list<QuestRelation *>::const_iterator q_end;

		bool bValid = false;

		if(qst_giver->GetTypeId() == TYPEID_GAMEOBJECT)
		{
            bValid = ((GameObject*)qst_giver)->HasQuests();
            if(bValid)
            {
				q_begin = ((GameObject*)qst_giver)->QuestsBegin();
				q_end   = ((GameObject*)qst_giver)->QuestsEnd();
			}
		} 
		else if(qst_giver->GetTypeId() == TYPEID_UNIT)
		{
			bValid = ((Creature*)qst_giver)->HasQuests();
			if(bValid)
			{
				q_begin = ((Creature*)qst_giver)->QuestsBegin();
				q_end   = ((Creature*)qst_giver)->QuestsEnd();
			}
		}

		if(!bValid)
		{
			MyLog::log->debug("QUESTS: Warning, invalid NPC "I64FMT" specified for OnActivateQuestGiver. TypeId: %d.", qst_giver->GetGUID(), qst_giver->GetTypeId());
			return false;
		}
		
		for(itr = q_begin; itr != q_end; ++itr) 
			if (sQuestMgr.CalcQuestStatus(qst_giver, plr, *itr) >= QMGR_QUEST_CHAT)
				break;

		if (sQuestMgr.CalcStatus(qst_giver, plr) < QMGR_QUEST_CHAT)
			return false; 

		ASSERT(itr != q_end);

		uint32 status = sQuestMgr.CalcStatus(qst_giver, plr);

		if ((status == QMGR_QUEST_AVAILABLE) || (status == QMGR_QUEST_REPEATABLE) || (status == QMGR_QUEST_CHAT))
		{
			MSG_S2C::stQuestGiver_Quest_Details Msg;
			sQuestMgr.BuildQuestDetails(&Msg, (*itr)->qst, qst_giver->GetGUID(), 1);		// 1 because we have 1 quest, and we want goodbye to function
			plr->GetSession()->SendPacket(Msg);
			MyLog::log->debug( "WORLD: Sent SMSG_QUESTGIVER_QUEST_DETAILS." );
		}
		else if (status == QMGR_QUEST_FINISHED)
		{
			MSG_S2C::stQuestGiver_Offer_Reward Msg;
			sQuestMgr.BuildOfferReward(&Msg, (*itr)->qst, qst_giver, 1);
			plr->GetSession()->SendPacket(Msg);
			//ss
			MyLog::log->debug( "WORLD: Sent SMSG_QUESTGIVER_OFFER_REWARD." );
		}
		else if (status == QMGR_QUEST_NOT_FINISHED)
		{
			MSG_S2C::stQuestGiver_Request_Items Msg;
			sQuestMgr.BuildRequestItems(&Msg, (*itr)->qst, qst_giver, status);
			plr->GetSession()->SendPacket(Msg);
			MyLog::log->debug( "WORLD: Sent SMSG_QUESTGIVER_REQUEST_ITEMS." );
		}
	}
	else 
	{
		MSG_S2C::stQuestGiver_Query_List Msg;
		sQuestMgr.BuildQuestList(&Msg, qst_giver ,plr);
		plr->GetSession()->SendPacket(Msg);
		MyLog::log->debug( "WORLD: Sent SMSG_QUESTGIVER_QUEST_LIST." );
	}
	return true;
}

QuestMgr::~QuestMgr()
{
	HM_NAMESPACE::hash_map<uint32, Quest*>::iterator itr1;
	HM_NAMESPACE::hash_map<uint32, list<QuestRelation *>* >::iterator itr2;
	list<QuestRelation*>::iterator itr3;

	// clear relations
	for(itr2 = m_obj_quests.begin(); itr2 != m_obj_quests.end(); ++itr2)
	{
		if(!itr2->second)
			continue;

		itr3 = itr2->second->begin();
		for(; itr3 != itr2->second->end(); ++itr3)
		{
			delete (*itr3);
		}
		itr2->second->clear();
		delete itr2->second;
	}

	for(itr2 = m_npc_quests.begin(); itr2 != m_npc_quests.end(); ++itr2)
	{
		if(!itr2->second)
			continue;

		itr3 = itr2->second->begin();
		for(; itr3 != itr2->second->end(); ++itr3)
		{
			delete (*itr3);
		}
		itr2->second->clear();
		delete itr2->second;
	}

	for(itr2 = m_itm_quests.begin(); itr2 != m_itm_quests.end(); ++itr2)
	{
		if(!itr2->second)
			continue;

		itr3 = itr2->second->begin();
		for(; itr3 != itr2->second->end(); ++itr3)
		{
			delete (*itr3);
		}
		itr2->second->clear();
		delete itr2->second;
	}

}


FAILED_REASON QuestMgr::CanStoreReward(Player *plyr, Quest *qst, uint32 reward_slot)
{
    // Static Item reward
	std::list<std::pair<uint32, uint32> > l;
    for(uint32 i = 0; i < 4; ++i)
    {
        if(qst->reward_item[i])
        {
			l.push_back( std::make_pair(qst->reward_item[i], qst->reward_itemcount[i]) );
            //slotsrequired++;
            ItemPrototype *proto = ItemPrototypeStorage.LookupEntry(qst->reward_item[i]);
            if(!proto)
                MyLog::log->error("Invalid item prototype in quest reward! ID %d, quest %d", qst->reward_item[i], qst->id);
            else if(plyr->GetItemInterface()->CanReceiveItem(proto, qst->reward_itemcount[i]))
				return FAILED_REASON_DUPE_ITEM_FOUND;
        }
    }

    // Choice Rewards
    if(qst->reward_choiceitem[reward_slot])
    {
        //slotsrequired++;
		l.push_back( std::make_pair(qst->reward_choiceitem[reward_slot], qst->reward_choiceitemcount[reward_slot]) );
        ItemPrototype *proto = ItemPrototypeStorage.LookupEntry(qst->reward_choiceitem[reward_slot]);
        if(!proto)
            MyLog::log->error("Invalid item prototype in quest reward! ID %d, quest %d", qst->reward_choiceitem[reward_slot], qst->id);
        else if(plyr->GetItemInterface()->CanReceiveItem(proto, qst->reward_choiceitemcount[reward_slot]))
			return FAILED_REASON_DUPE_ITEM_FOUND;
    }

	if( plyr->GetItemInterface()->CanStoreItems( l ) )
		return FAILED_REASON_SUCCESS;
	else
		return FAILED_REASON_INV_FULL;
}

void QuestMgr::LoadExtraQuestStuff()
{
	StorageContainerIterator<Quest> * it = QuestStorage.MakeIterator();
	Quest * qst;
	while(!it->AtEnd())
	{
		qst = it->Get();

		// 0 them out
		qst->count_required_item = 0;
		qst->count_required_mob = 0;
		qst->count_requiredtriggers = 0;
		qst->count_receiveitems = 0;
		qst->count_reward_item = 0;
		qst->count_reward_choiceitem = 0;
		qst->reward_xp_as_money = 0;

		qst->required_mobtype[0] = 0;
		qst->required_mobtype[1] = 0;
		qst->required_mobtype[2] = 0;
		qst->required_mobtype[3] = 0;

		qst->count_requiredquests = 0;

		for(int i = 0 ; i < 4; ++i)
		{
			if(qst->required_mob[i])
			{
				GameObjectInfo *go_info = GameObjectNameStorage.LookupEntry(qst->required_mob[i]);
				CreatureInfo   *c_info  = CreatureNameStorage.LookupEntry(qst->required_mob[i]);
				if( qst->quest_flags & QUEST_FLAG_ESCORT )
					qst->required_mobtype[i] = QUEST_MOB_TYPE_ESCORT;
				else if(go_info && (go_info->Type == 10 || qst->quest_flags == 10 || !c_info))
					qst->required_mobtype[i] = QUEST_MOB_TYPE_GAMEOBJECT;
				else
					qst->required_mobtype[i] = QUEST_MOB_TYPE_CREATURE;

				qst->count_required_mob++;
			}

			if(qst->required_item[i])
				qst->count_required_item++;

			if(qst->reward_item[i])
				qst->count_reward_item++;

			if(qst->required_triggers[i])
				qst->count_requiredtriggers++;

			if(qst->receive_items[i])
				qst->count_receiveitems++;

			if(qst->required_quests[i])
				qst->count_requiredquests++;
		}

		if(qst->srcitem)
			qst->count_receiveitems++;

		for(int i = 0; i < 6; ++i)
		{
			if(qst->reward_choiceitem[i])
				qst->count_reward_choiceitem++;
		}

		if(!it->Inc())
			break;
	}

	it->Destruct();

	// load creature starters
	uint32 creature, quest;

	QueryResult * pResult = WorldDatabase.Query("SELECT * FROM creature_quest_starter");
	uint32 pos = 0;
	uint32 total;
	if(pResult)
	{
		total = pResult->GetRowCount();
		do 
		{
			Field *data = pResult->Fetch();
			creature = data[0].GetUInt32();
			quest = data[1].GetUInt32();

			qst = QuestStorage.LookupEntry(quest);
			if(!qst)
			{
				//printf("Tried to add starter to npc %d for non-existant quest %d.\n", creature, quest);
			}
			else 
			{
				_AddQuest<Creature>(creature, qst, 1);  // 1 = starter
			}
		} while(pResult->NextRow());
		delete pResult;
	}

	pResult = WorldDatabase.Query("SELECT * FROM creature_quest_finisher");
	pos = 0;
	if(pResult)
	{
		total = pResult->GetRowCount();
		do 
		{
			Field *data = pResult->Fetch();
			creature = data[0].GetUInt32();
			quest = data[1].GetUInt32();

			qst = QuestStorage.LookupEntry(quest);
			if(!qst)
			{
				//printf("Tried to add finisher to npc %d for non-existant quest %d.\n", creature, quest);
			} 
			else 
			{
				_AddQuest<Creature>(creature, qst, 2);  // 1 = starter
			}
		} while(pResult->NextRow());
		delete pResult;
	}

	pResult = WorldDatabase.Query("SELECT * FROM gameobject_quest_starter");
	pos = 0;
	if(pResult)
	{
		total = pResult->GetRowCount();
		do 
		{
			Field *data = pResult->Fetch();
			creature = data[0].GetUInt32();
			quest = data[1].GetUInt32();

			qst = QuestStorage.LookupEntry(quest);
			if(!qst)
			{
				//printf("Tried to add starter to go %d for non-existant quest %d.\n", creature, quest);
			} 
			else
			{
				_AddQuest<GameObject>(creature, qst, 1);  // 1 = starter
			}
		} while(pResult->NextRow());
		delete pResult;
	}

	pResult = WorldDatabase.Query("SELECT * FROM gameobject_quest_finisher");
	pos = 0;
	if(pResult)
	{
		total = pResult->GetRowCount();
		do 
		{
			Field *data = pResult->Fetch();
			creature = data[0].GetUInt32();
			quest = data[1].GetUInt32();

			qst = QuestStorage.LookupEntry(quest);
			if(!qst)
			{
				//printf("Tried to add finisher to go %d for non-existant quest %d.\n", creature, quest);
			} 
			else 
			{
				_AddQuest<GameObject>(creature, qst, 2);  // 2 = finish
			}
		} while(pResult->NextRow());
		delete pResult;
	}
	objmgr.ProcessGameobjectQuests();

	//load item quest associations
	uint32 item;
	uint8 item_count;

	pResult = WorldDatabase.Query("SELECT * FROM item_quest_association");
	pos = 0;
	if( pResult != NULL)
	{
		total = pResult->GetRowCount();
		do 
		{
			Field *data = pResult->Fetch();
			item = data[0].GetUInt32();
			quest = data[1].GetUInt32();
			item_count = data[2].GetUInt8();

			qst = QuestStorage.LookupEntry(quest);
			if(!qst)
			{
				//printf("Tried to add association to item %d for non-existant quest %d.\n", item, quest);
			} 
			else 
			{
				AddItemQuestAssociation( item, qst, item_count );
			}
		} while( pResult->NextRow() );
		delete pResult;
	}
}

void QuestMgr::AddItemQuestAssociation( uint32 itemId, Quest *qst, uint8 item_count)
{
	HM_NAMESPACE::hash_map<uint32, list<QuestAssociation *>* > &associationList = GetQuestAssociationList();
	std::list<QuestAssociation *>* tempList;
	QuestAssociation *ptr = NULL;
	
	// look for the item in the associationList
	if (associationList.find( itemId ) == associationList.end() )
	{
		// not found. Create a new entry and QuestAssociationList
		tempList = new std::list<QuestAssociation *>;

		associationList.insert(HM_NAMESPACE::hash_map<uint32, list<QuestAssociation *>* >::value_type(itemId, tempList));
	}
	else
	{
		// item found, now we'll search through its QuestAssociationList
		tempList = associationList.find( itemId )->second;
	}
	
	// look through this item's QuestAssociationList for a matching quest entry
	list<QuestAssociation *>::iterator it;
	for (it = tempList->begin(); it != tempList->end(); ++it)
	{
		if ((*it)->qst == qst)
		{
			// matching quest found
			ptr = (*it);
			break;
		}
	}

	// did we find a matching quest?
	if (ptr == NULL)
	{
		// nope, create a new QuestAssociation for this item and quest
		ptr = new QuestAssociation;
		ptr->qst = qst;
		ptr->item_count = item_count;

		tempList->push_back( ptr );
	}
	else
	{
		// yep, update the QuestAssociation with the new item_count information 
		ptr->item_count = item_count;
		MyLog::log->debug( "WARNING: Duplicate entries found in item_quest_association, updating item #%d with new item_count: %d.", itemId, item_count );
	}
}

QuestAssociationList* QuestMgr::GetQuestAssociationListForItemId (uint32 itemId)
{
	HM_NAMESPACE::hash_map<uint32, QuestAssociationList* > &associationList = GetQuestAssociationList();
	HM_NAMESPACE::hash_map<uint32, QuestAssociationList* >::iterator itr = associationList.find( itemId );
	if( itr == associationList.end() )
	{
		return 0;
	} else {
		return itr->second;
	}
}


void QuestMgr::LoadEscortQuestInfo()
{
	smart_ptr<QueryResult> qr = WorldDatabase.Query( "select * from escort_quest" );
	if( qr )
	{
		do
		{
			Field* f = qr->Fetch();
			escort_info* ei = new escort_info;
			ei->quest_id = f[0].GetUInt32();
			ei->waypoint_id = f[1].GetUInt32();
			std::string tmp( f[2].GetString() );

			uint32 Counter = 0;
			char * end;
			char * start = (char*)tmp.c_str();
			while( true )
			{
				end = strchr(start,',');
				if(!end)break;
				*end=0;
				uint32 value = atol(start);
				ei->creature_spawn_id.insert( value );
				start = end +1;
				Counter++;
			}
			ei->gossip = f[3].GetString();
			ei->gossip_type = f[4].GetUInt32();
			ei->move_speed = f[5].GetFloat();
			ei->finish = f[6].GetUInt32();
			m_escort_info[ei->quest_id][ei->waypoint_id] = ei;
		}
		while( qr->NextRow() );
	}
}

void QuestMgr::OnEscortReachWayPoint( Creature* c, uint32 wp )
{
	if( 0 == c->m_escort_players.size() || !c->m_escort_qst )
		return;

	escort_info* ei = GetEscortInfo( c->m_escort_qst->id, wp );
	if( ei )
	{
		if( ei->gossip.length() )
		{
			c->SendChatMessage( ei->gossip_type, 0, ei->gossip.c_str() );
		}
		if( ei->move_speed > 0.f )
		{
			c->GetAIInterface()->m_walkSpeed = ei->move_speed / 1000.f;
		}
		if( ei->creature_spawn_id.size() )
		{
			if( c->IsInWorld() )
				c->GetMapMgr()->SpawnCreatures( ei->creature_spawn_id, c, c->m_escort_hostile_creatures );
		}
		if( ei->finish == 1 )
		{
			QuestLogEntry *qle;
			for( std::set<PlayerInfo*>::iterator it = c->m_escort_players.begin(); it != c->m_escort_players.end(); ++it )
			{
				PlayerInfo* pi = *it;
				if( !pi->m_loggedInPlayer ) continue;

				for( int i = 0; i < QUEST_MAX_COUNT; ++i )
				{
					if( ( qle = pi->m_loggedInPlayer->GetQuestLogInSlot( i ) ) )
					{
						if( qle->GetQuest()->id == c->m_escort_qst->id )
						{
							qle->SetMobCount( 0, 1 );
							qle->UpdatePlayerFields( true );
							break;
						}
					}
				}
			}
			c->m_escort_finished = true;
		}
		if( ei->finish == 2 )
		{
			c->RemoveFromWorld( true, true );
			c->m_escort_finished = false;
			return;
		}
	}
}

QuestMgr::escort_info* QuestMgr::GetEscortInfo( uint32 questid, uint32 wp )
{
	std::map<uint32, std::map<uint32, escort_info*> >::iterator it = m_escort_info.find( questid );
	if( it != m_escort_info.end() )
	{
		std::map<uint32, escort_info*>::iterator it2 = it->second.find( wp );
		if( it2 != it->second.end() )
		{
			return it2->second;
		}
	}
	return NULL;
}

void QuestMgr::OnEscortCreatureDie( Creature* c )
{
	if( c->m_escort_finished )
		return;

	if( c->IsInWorld() )
	{
		c->GetMapMgr()->DespawnCreatures( c->m_escort_hostile_creatures );
		c->m_escort_hostile_creatures.clear();
	}

	for( std::set<PlayerInfo*>::iterator it = c->m_escort_players.begin(); it != c->m_escort_players.end(); ++it )
	{
		PlayerInfo* pi = *it;
		if( !pi->m_loggedInPlayer ) continue;
		SendQuestFailed( FAILED_REASON_FAILED, c->m_escort_qst, pi->m_loggedInPlayer, false );
	}
	c->m_escort_qst = NULL;
	c->m_escort_players.clear();
	c->GetAIInterface()->m_lastReachWP = 0;
}
