#ifndef __QUESTMGR_H
#define __QUESTMGR_H
struct QuestRelation
{
	Quest *qst;
	uint8 type;
};

struct QuestAssociation
{
	Quest *qst;
	uint8 item_count;
};

typedef std::list<QuestRelation *> QuestRelationList;
typedef std::list<QuestAssociation *> QuestAssociationList;

struct LockQstItemNotify
{
	static bool s_bLockQstItemNotify;

	LockQstItemNotify( bool b ) {
		s_bLockQstItemNotify = b;
	}
	~LockQstItemNotify() {
		s_bLockQstItemNotify = !s_bLockQstItemNotify;
	}
};

namespace MSG_S2C
{
	struct stQuestGiver_Offer_Reward;
	struct stQuestGiver_Quest_Details;
	struct stQuestGiver_Request_Items;
	struct stQuestGiver_Query_List;
	struct stQuest_Update_Add_Item;
	struct stQuest_Update_Complete;
	struct stQuest_Update_Failed;
}

class SERVER_DECL QuestMgr :  public Singleton < QuestMgr >
{
public:

	~QuestMgr();

	uint32 PlayerMeetsReqs(Player* plr, Quest* qst, bool skiplevelcheck);

	uint32 CalcStatus(Object* quest_giver, Player* plr);
	uint32 CalcQuestStatus(Object* quest_giver, Player* plr, QuestRelation* qst);
	uint32 CalcQuestStatus(Object* quest_giver, Player* plr, Quest* qst, uint8 type, bool skiplevelcheck);
	uint32 ActiveQuestsCount(Object* quest_giver, Player* plr);

	//Packet Forging...
	void BuildOfferReward(MSG_S2C::stQuestGiver_Offer_Reward* Msg,Quest* qst, Object* qst_giver, uint32 menutype);
	void BuildQuestDetails(MSG_S2C::stQuestGiver_Quest_Details* Msg, Quest* qst, uint64 questgiver_guid, uint32 menutype);
	void BuildRequestItems(MSG_S2C::stQuestGiver_Request_Items* Msg, Quest* qst, Object* qst_giver, uint32 status);
	void BuildQuestComplete(Player*, Quest* qst, uint32 choose_reward);
	void BuildQuestList(MSG_S2C::stQuestGiver_Query_List* Msg, Object* qst_giver, Player* plr);
	bool OnActivateQuestGiver(Object *qst_giver, Player *plr);
    bool isRepeatableQuestFinished(Player *plr, Quest *qst);

	void SendQuestUpdateAddKill(Player* plr, uint32 questid, uint32 entry, uint32 count, uint32 tcount, uint64 guid);
	void BuildQuestUpdateAddItem(MSG_S2C::stQuest_Update_Add_Item* Msg, uint32 itemid, uint32 count);
	void BuildQuestUpdateComplete(MSG_S2C::stQuest_Update_Complete* Msg, Quest* qst);
	void BuildQuestFailed(MSG_S2C::stQuest_Update_Failed* Msg, uint32 questid);
	void SendPushToPartyResponse(Player *plr, Player* pTarget, uint32 response);

	bool OnGameObjectActivate(Player *plr, GameObject *go);
	void OnPlayerKill(Player* plr, Creature* victim);
	void OnPlayerCast(Player* plr, uint32 spellid, uint64& victimguid);
	void OnPlayerItemPickup(Player* plr, Item* item);
	void OnPlayerItemLost(Player* plr, uint32 entry, uint32 cnt);
	void OnPlayerExploreArea(Player* plr, uint32 AreaID);
	void OnQuestAccepted(Player* plr, Quest* qst, Object *qst_giver);
	void OnQuestFinished(Player* plr, Quest* qst, Object *qst_giver, uint32 reward_slot);
	void OnQuestGiveuped(Player* plr, Quest* qst);

	uint32 GenerateQuestXP(Player *pl, Quest *qst);

	void SendQuestInvalid( INVALID_REASON reason, Player *plyr);
	void SendQuestFailed(FAILED_REASON failed, Quest *qst, Player *plyr, uint8 baccept);
	void SendQuestUpdateFailed(Quest *pQuest, Player *plyr);
	void SendQuestUpdateFailedTimer(Quest *pQuest, Player *plyr);
	void SendQuestLogFull(Player *plyr);

	void LoadNPCQuests(Creature *qst_giver);
	void LoadGOQuests(GameObject *go);

	QuestRelationList* GetCreatureQuestList(uint32 entryid);
	QuestRelationList* GetGOQuestList(uint32 entryid);
	QuestAssociationList* GetQuestAssociationListForItemId (uint32 itemId);
	uint32 GetGameObjectLootQuest(uint32 GO_Entry);
	void SetGameObjectLootQuest(uint32 GO_Entry, uint32 Item_Entry);
	SUNYOU_INLINE bool IsQuestRepeatable(Quest *qst) { return qst->type & QUEST_TYPE_REPEATEABLE; /*(qst->is_repeatable!=0 ? true : false);*/ }
	SUNYOU_INLINE bool IsDailyQuest(Quest *qst) { return qst->type & QUEST_TYPE_DAILY; }

	FAILED_REASON CanStoreReward(Player *plyr, Quest *qst, uint32 reward_slot);

	SUNYOU_INLINE int32 QuestHasMob(Quest* qst, uint32 mob)
	{
		for(uint32 i = 0; i < 4; ++i)
			if(qst->required_mob[i] == mob)
				return qst->required_mobcount[i];
		return -1;
	}

	SUNYOU_INLINE int32 GetOffsetForMob(Quest *qst, uint32 mob)
	{
		for(uint32 i = 0; i < 4; ++i)
			if(qst->required_mob[i] == mob)
				return i;

		return -1;
	}

	SUNYOU_INLINE int32 GetOffsetForItem(Quest *qst, uint32 itm)
	{
		for(uint32 i = 0; i < 4; ++i)
			if(qst->required_item[i] == itm)
				return i;

		return -1;
	}
	void LoadExtraQuestStuff();

	void LoadEscortQuestInfo();
	void OnEscortReachWayPoint( Creature* c, uint32 wp );
	void OnEscortCreatureDie( Creature* c );

private:
	struct escort_info
	{
		uint32 quest_id;
		uint32 waypoint_id;
		std::set<uint32> creature_spawn_id;
		std::string gossip;
		uint32 gossip_type;
		float move_speed;
		uint32 finish;
	};
	escort_info* GetEscortInfo( uint32 questid, uint32 wp );
	std::map<uint32, std::map<uint32, escort_info*> > m_escort_info;

	HM_NAMESPACE::hash_map<uint32, list<QuestRelation *>* > m_npc_quests;
	HM_NAMESPACE::hash_map<uint32, list<QuestRelation *>* > m_obj_quests;
	HM_NAMESPACE::hash_map<uint32, list<QuestRelation *>* > m_itm_quests;

	HM_NAMESPACE::hash_map<uint32, list<QuestAssociation *>* > m_quest_associations;
	SUNYOU_INLINE HM_NAMESPACE::hash_map<uint32, list<QuestAssociation *>* >& GetQuestAssociationList()
		{return m_quest_associations;}

	HM_NAMESPACE::hash_map<uint32, uint32>		  m_ObjectLootQuestList;

	template <class T> void _AddQuest(uint32 entryid, Quest *qst, uint8 type);

	template <class T> HM_NAMESPACE::hash_map<uint32, list<QuestRelation *>* >& _GetList();

	void AddItemQuestAssociation( uint32 itemId, Quest *qst, uint8 item_count);

	// Quest Loading
	void _RemoveChar(char* c, std::string *str);
	void _CleanLine(std::string *str);
};

template<> SUNYOU_INLINE HM_NAMESPACE::hash_map<uint32, list<QuestRelation *>* >& QuestMgr::_GetList<Creature>()
	{return m_npc_quests;}
template<> SUNYOU_INLINE HM_NAMESPACE::hash_map<uint32, list<QuestRelation *>* >& QuestMgr::_GetList<GameObject>()
	{return m_obj_quests;}
template<> SUNYOU_INLINE HM_NAMESPACE::hash_map<uint32, list<QuestRelation *>* >& QuestMgr::_GetList<Item>()
	{return m_itm_quests;}


#define sQuestMgr QuestMgr::getSingleton()

#endif
