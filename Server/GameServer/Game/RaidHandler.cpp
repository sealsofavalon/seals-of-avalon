#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Group.h"
#include "../../SDBase/Protocol/S2C_Group.h"
void WorldSession::HandleConvertGroupToRaidOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	// This is just soooo easy now   
	Group *pGroup = _player->GetGroup();
	if(!pGroup) return;

	if ( pGroup->GetLeader() != _player->m_playerInfo )   //access denied
	{
		SendPartyCommandResult(_player, 0, "", ERR_PARTY_YOU_ARE_NOT_LEADER);
		return;
	}

	pGroup->ExpandToRaid();
	SendPartyCommandResult(_player, 0, "", ERR_PARTY_NO_ERROR);
}

void WorldSession::HandleGroupChangeSubGroup(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	if (!_player->m_playerInfo || !_player->m_playerInfo->m_Group || _player->m_playerInfo->m_Group->GetLeader() != _player->m_playerInfo)
	{
		return;
		
	}
	MSG_C2S::stGroup_Change_Sub_Group MsgRecv;packet>>MsgRecv;
	std::string name = MsgRecv.name;
	uint8 subGroup = MsgRecv.subGroup;

	

	PlayerInfo * inf = objmgr.GetPlayerInfoByName(name.c_str());
	if(inf == NULL || inf->m_Group == NULL || inf->m_Group != _player->m_playerInfo->m_Group)
		return;

	_player->GetGroup()->MovePlayer(inf, subGroup);
}

void WorldSession::HandleGroupSwepMemberName(CPacketUsn& packet)
{
	if (!_player->IsInWorld())
	{
		return;
	}

	MSG_C2S::stGroupSwepName Msg;
	packet >> Msg;

	PlayerInfo* infosrc = NULL;
	PlayerInfo* infodes = NULL;
	infosrc = objmgr.GetPlayerInfoByName(Msg.src.c_str());
	infodes = objmgr.GetPlayerInfoByName(Msg.des.c_str());

	if(infodes == NULL || infodes->m_Group == NULL || infodes->m_Group != _player->m_playerInfo->m_Group)
		return;


	if(infosrc == NULL || infosrc->m_Group == NULL || infosrc->m_Group != _player->m_playerInfo->m_Group)
		return;

	if (infosrc->m_Group->GetLeader() != _player->m_playerInfo)
	{
		return;
	}

	_player->GetGroup()->SwepPlayer(infodes, infosrc);
}

void WorldSession::HandleGroupSwepMember(CPacketUsn& packet)
{
	if (!_player->IsInWorld())
	{
		return;
	}

	MSG_C2S::stGroupSwep Msg;
	packet >> Msg;

	PlayerInfo* infosrc = NULL;
	PlayerInfo* infodes = NULL;
	infosrc = objmgr.GetPlayerInfo(Msg.srcguid);
	infodes = objmgr.GetPlayerInfo(Msg.desguid);

	if(infodes == NULL || infodes->m_Group == NULL || infodes->m_Group != _player->m_playerInfo->m_Group)
		return;


	if(infosrc == NULL || infosrc->m_Group == NULL || infosrc->m_Group != _player->m_playerInfo->m_Group)
		return;

	if (infosrc->m_Group->GetLeader() != _player->m_playerInfo)
	{
		return;
	}
	
	_player->GetGroup()->SwepPlayer(infodes, infosrc);

}

void WorldSession::HandleGroupAssistantLeader(CPacketUsn& packet)
{
	MSG_C2S::stGroup_Assistant_Leader MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;
	uint8 on = MsgRecv.on;

	if(!_player->IsInWorld())
		return;

	if(_player->GetGroup() == NULL)
		return;

	if ( _player->GetGroup()->GetLeader() != _player->m_playerInfo )   //access denied
	{
		SendPartyCommandResult(_player, 0, "", ERR_PARTY_YOU_ARE_NOT_LEADER);
		return;
	}

	if(on == 0)
        _player->GetGroup()->SetAssistantLeader(NULL);
	else
	{
		PlayerInfo * np = objmgr.GetPlayerInfo((uint32)guid);
		if(np==NULL)
			_player->GetGroup()->SetAssistantLeader(NULL);
		else
		{
			if(_player->GetGroup()->HasMember(np))
				_player->GetGroup()->SetAssistantLeader(np);
		}
	}
}

void WorldSession::HandleGroupPromote(CPacketUsn& packet)
{
	uint8 promotetype, on;
	uint64 guid;

	if(!_player->IsInWorld())
		return;

	if(_player->GetGroup() == NULL)
		return;

	if ( _player->GetGroup()->GetLeader() != _player->m_playerInfo )   //access denied
	{
		SendPartyCommandResult(_player, 0, "", ERR_PARTY_YOU_ARE_NOT_LEADER);
		return;
	}

	MSG_C2S::stGroup_Promote MsgRecv;packet>>MsgRecv;
	promotetype = MsgRecv.promotetype;
	on = MsgRecv.on;
	guid = MsgRecv.guid;

	void(Group::*function_to_call)(PlayerInfo*);

	if(promotetype == 0)
		function_to_call = &Group::SetMainTank;
	else if(promotetype==1)
		function_to_call = &Group::SetMainAssist;

	if(on == 0)
		(_player->GetGroup()->*function_to_call)(NULL);
	else
	{
		PlayerInfo * np = objmgr.GetPlayerInfo((uint32)guid);
		if(np==NULL)
			(_player->GetGroup()->*function_to_call)(NULL);
		else
		{
			if(_player->GetGroup()->HasMember(np))
				(_player->GetGroup()->*function_to_call)(np);
		}
	}
}

void WorldSession::HandleRequestRaidInfoOpcode(CPacketUsn& packet)
{  
	//		  SMSG_RAID_INSTANCE_INFO			 = 716,  //(0x2CC)	
	//sInstanceSavingManager.BuildRaidSavedInstancesForPlayer(_player);
	sInstanceMgr.BuildRaidSavedInstancesForPlayer(_player);
}

void WorldSession::HandleReadyCheckOpcode(CPacketUsn& packet)
{
	Group * pGroup  = _player->GetGroup();
	MSG_C2S::stRaid_ReadyCheck MsgRecv;packet>>MsgRecv;
	uint8 ready = MsgRecv.ready;
	MSG_S2C::stRaid_ReadyCheck Msg;
	Msg.player_guid = _player->GetGUID();
	Msg.ready = ready;

	if(!pGroup || ! _player->IsInWorld())
		return;
	time_t time = UNIXTIME - pGroup->m_unRaidReadyCheck;

	if(MsgRecv.ready==0)
	{

		if(pGroup->GetLeader() == _player->m_playerInfo)
		{
			if (time >= 30)
			{

			/* send packet to group */
				pGroup->SendPacketToAllButOne(Msg, _player);
				pGroup->m_unRaidReadyCheck = UNIXTIME;

			}
			else
			{		
				SendNotification("Be wait a moment");
			}
		}
		else
		{
			SendNotification(NOTIFICATION_MESSAGE_NO_PERMISSION);
		}





	}
	else
	{
		if(_player->m_playerInfo != pGroup->GetLeader())
		{
			if(pGroup->GetLeader() && pGroup->GetLeader()->m_loggedInPlayer)
				pGroup->GetLeader()->m_loggedInPlayer->GetSession()->SendPacket(Msg);
		}
	}
}

