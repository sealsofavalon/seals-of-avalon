#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S.h"
#include "../../SDBase/Protocol/S2C.h"
#include "Item.h"
#include "ItemInterface.h"
#include "RefineItem.h"
#include "CombineItem.h"

void WorldSession::HandleRefineContainerMoveItemReq( CPacketUsn& packet )
{
	if( !_player ) return;
	MSG_C2S::stRefineContainerMoveItemReq req;
	packet >> req;
	if( req.refine_container_slot >= MAX_REFINE_SLOT )
		return;
	Item* pSource = _player->GetItemInterface()->GetInventoryItem( req.container_slot, req.slot );
	if( pSource )
	{
		if( req.refine_container_slot > 0 )
		{
			if( !FindRefineGem( pSource->GetEntry() ) )
				return;
		}
		else if( !pSource->CanRefine() )
		{
			MSG_S2C::stRefineItemAck ack;
			ack.result = REFINE_RESULT_INVALID_ITEM;
			SendPacket( ack );
			return;
		}

	}
	bool destroy = false;
	Item* ret = _player->GetRefineItemInterface()->MoveSlot( req.refine_container_slot, pSource, req.to_inventory, destroy );
	if( pSource != ret )
	{
		_player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot( req.container_slot, req.slot, destroy );
		if( ret )
			_player->GetItemInterface()->SafeAddItem( ret, req.container_slot, req.slot );
	}
}

void WorldSession::HandleRefineItemReq( CPacketUsn& packet )
{
	if( !_player ) return;
	_player->RefineItem();
}

void WorldSession::HandleCombineContainerMoveItemReq( CPacketUsn& packet )
{
	if( !_player ) return;
	MSG_C2S::stCombineContainerMoveItemReq req;
	packet >> req;
	if( req.combine_container_slot >= MAX_COMBINE_SLOT )
		return;
	Item* pSource = _player->GetItemInterface()->GetInventoryItem( req.container_slot, req.slot );
	if( pSource )
	{
		if( req.combine_container_slot > 0 )
		{
		}
		else if( !pSource->CanRefine() )
		{
			MSG_S2C::stCombineItemAck ack;
			ack.result = COMBINE_RESULT_INVALID_ITEM;
			SendPacket( ack );
			return;
		}

	}
	bool destroy = false;
	Item* ret = _player->GetCombineItemInterface()->MoveSlot( req.combine_container_slot, pSource, req.to_inventory, destroy );
	if( pSource != ret )
	{
		_player->GetItemInterface()->SafeRemoveAndRetreiveItemFromSlot( req.container_slot, req.slot, destroy );
		if( ret )
			_player->GetItemInterface()->SafeAddItem( ret, req.container_slot, req.slot );
	}
}

void WorldSession::HandleCombineItemReq( CPacketUsn& packet )
{
	if( !_player ) return;
	_player->CombineItem();
}
