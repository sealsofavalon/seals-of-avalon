#include "StdAfx.h"
#include "RefineItem.h"
#include "Player.h"
#include "Item.h"
#include "ItemInterface.h"
#include "../../SDBase/Public/UpdateFields.h"

RefineItemInterface::RefineItemInterface()
{
	max_slot = MAX_REFINE_SLOT;
	player_field_cost = PLAYER_FIELD_REFINE_COST;
	player_field_begin = PLAYER_FIELD_REFINE_SLOT_ITEM;
	inventory_slot_begin = INVENTORY_SLOT_REFINE_BEGIN;
	inventory_slot_save = INVENTORY_SLOT_REFINE;
}

RefineItemInterface::~RefineItemInterface()
{
}

void RefineItemInterface::RefreshCost()
{
	if( m_slots[0] )
	{
		refine_item_upgrade* riu = dbcRefineItemUpgrade.LookupEntry( m_slots[0]->GetRefineLevel() + 1 );
		if( riu )
		{
			m_owner->SetUInt32Value( player_field_cost, riu->cost );
			return;
		}
	}
	m_owner->SetUInt32Value( player_field_cost, 0 );
}


refine_result_t RefineItemInterface::Refine( bool check /* = false */ )
{
	if( !m_slots[0] ) return REFINE_RESULT_INVALID_ITEM;

	if( !m_slots[0]->CanRefine() ) return REFINE_RESULT_INVALID_ITEM;

	uint32 lv = m_slots[0]->GetUInt32Value( ITEM_FIELD_REFINE_LEVEL ) + 1;

	if( lv > 9 ) return REFINE_RESULT_ALREADY_MAX_LEVEL;

	refine_item_upgrade* riu = dbcRefineItemUpgrade.LookupEntry( lv );
	if( riu )
	{
		if( m_owner->GetUInt32Value( PLAYER_FIELD_COINAGE ) < riu->cost )
			return REFINE_RESULT_NOT_ENOUGH_GOLD;
	}
	else
	{
		return REFINE_RESULT_ALREADY_MAX_LEVEL;
	}
	int matchslot = 0;
	int matchlv = 0;
	int matchchance = 0;
	int chanceslot = 0;
	int chance_bonus = 0;
	

	for( int i = 1; i < MAX_REFINE_SLOT; ++i )
	{
		if( m_slots[i] )
		{
			refine_item_gem* rig = FindRefineGem( m_slots[i]->GetEntry() );
			if( rig )
			{
				if( rig->type == REFINE_GEM_TYPE_UPGRADE )
				{
					if( lv >= rig->minlevel && lv <= rig->maxlevel )
					{
						if( rig->maxlevel > matchlv )
						{
							matchslot = i;
							matchlv = rig->maxlevel;
							matchchance = rig->chance;
						}
					}
				}
				else
				{
					chance_bonus += rig->chance;
					if( !check )
						m_slots[i]->SetRefineGam();
				}
			}
		}
	}

	if( !matchslot ) return REFINE_RESULT_NO_MATCH_GEM;
	if( check ) return REFINE_RESULT_CHECK_SUCCESS;

	chance_bonus += matchchance;

	for( int i = 1; i < MAX_REFINE_SLOT; ++i )
	{
		if( m_slots[i] && ( m_slots[i]->IsRefineGem() || i == matchslot ) )
		{
			
			uint32 count = m_slots[i]->GetUInt32Value( ITEM_FIELD_STACK_COUNT );
			MyLog::yunyinglog->info("item-refine-player[%u][%s] removeitem["I64FMT"][%u][%s] count[%u]", m_owner->GetLowGUID(), m_owner->GetName(), m_slots[i]->GetGUID(), m_slots[i]->GetProto()->ItemId, m_slots[i]->GetProto()->Name1, count);
			if( count == 1 )
			{
				m_slots[i]->DeleteFromDB();
				m_slots[i]->Delete();
				m_slots[i] = NULL;
			}
			else
			{
				m_slots[i]->SetCount( count - 1 );
				m_slots[i]->m_isDirty = true;
			}
			RefreshSlot( i );
		}
	}

	m_owner->ModCoin(-riu->cost );
	MyLog::yunyinglog->info("gold-refine-player[%u][%s] take gold[%u]", m_owner->GetLowGUID(), m_owner->GetName(), riu->cost);

	float chance = 100.f * float( riu->chance + chance_bonus ) / 1000.f;
	float val = 100.f * (float)( rand() % 32767 ) / 32767.f;
	if( val <= chance )
	{
		m_slots[0]->SetUInt32Value( ITEM_FIELD_REFINE_LEVEL, lv );
		m_slots[0]->RefreshRefine();

		float maxdur = m_slots[0]->GetProto()->MaxDurability;
		if( maxdur > 0.f )
		{
			float curdurrate = ( (float)m_slots[0]->GetDurability() / (float)m_slots[0]->GetDurabilityMax() );
			if( curdurrate > 1.f )
				curdurrate = 1.f;

			m_slots[0]->SetUInt32Value( ITEM_FIELD_MAXDURABILITY, (uint32)( m_slots[0]->refine_rate * maxdur ) );
			m_slots[0]->SetUInt32Value( ITEM_FIELD_DURABILITY, (uint32)( curdurrate * (float)m_slots[0]->GetDurabilityMax() ) );		
		}
		m_slots[0]->SetRefineEffect( lv );
		m_slots[0]->m_isDirty = true;
		MyLog::yunyinglog->info("item-refineitem-player[%u][%s] refineitem["I64FMT"][%u][%s] level[%d]", m_owner->GetLowGUID(), m_owner->GetName(), m_slots[0]->GetGUID(), m_slots[0]->GetProto()->ItemId, m_slots[0]->GetProto()->Name1, m_slots[0]->GetUInt32Value( ITEM_FIELD_REFINE_LEVEL));

		RefreshCost();

		return REFINE_RESULT_SUCCESS;
	}

	return REFINE_RESULT_FAILED;
}
