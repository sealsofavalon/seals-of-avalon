#ifndef _REFINE_ITEM_HEAD
#define _REFINE_ITEM_HEAD

#include "MakeItemBase.h"

class RefineItemInterface : public MakeItemBase
{
public:
	RefineItemInterface();
	~RefineItemInterface();

	virtual void RefreshCost();


	refine_result_t Refine( bool check = false );
};

#endif