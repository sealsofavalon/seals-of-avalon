#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Mail.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "SunyouCastle.h"
#include "InstanceQueue.h"
#include "SunyouRaid.h"
#include "CardSerialSystem.h"
#include "CreatureAIScriptSunyou.h"
#include "MultiLanguageStringMgr.h"

#ifndef WIN32
    #include <dlfcn.h>
    #include <unistd.h>
    #include <dirent.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <cstdlib>
    #include <cstring>
#endif

#include <svn_revision.h>
#define SCRIPTLIB_HIPART(x) ((x >> 16))
#define SCRIPTLIB_LOPART(x) ((x & 0x0000ffff))
#define SCRIPTLIB_VERSION_MINOR (BUILD_REVISION % 1000)
#define SCRIPTLIB_VERSION_MAJOR (BUILD_REVISION / 1000)

initialiseSingleton(ScriptMgr);
initialiseSingleton(HookInterface);

ScriptMgr::ScriptMgr()
{
	DefaultGossipScript = new GossipScript();
}

ScriptMgr::~ScriptMgr()
{

}

struct ScriptingEngine
{
#ifdef WIN32
	HMODULE Handle;
#else
	void* Handle;
#endif
	exp_script_register InitializeCall;
	uint32 Type;
};

void ScriptMgr::LoadScripts()
{

	_Scriptluastate = LuaState::Create();
	_Scriptluastate->OpenLibs();
	/*
	if(!HookInterface::getSingletonPtr())
		new HookInterface;

	MyLog::log->notice("Server : Loading External Script Libraries...");

	string start_path = Config.MainConfig.GetStringDefault( "Script", "BinaryLocation", "script_bin" ) + "\\";
	string search_path = start_path + "*.";

	vector< ScriptingEngine > ScriptEngines;

	
#ifdef WIN32
	search_path += "dll";

	WIN32_FIND_DATA data;
	uint32 count = 0;
	HANDLE find_handle = FindFirstFile( search_path.c_str(), &data );
	if(find_handle == INVALID_HANDLE_VALUE)
		MyLog::log->error( "  No external scripts found! Server will continue to function with limited functionality." );
	else
	{
		do
		{
			string full_path = start_path + data.cFileName;
			HMODULE mod = LoadLibrary( full_path.c_str() );
			printf( "  %s : 0x%p : ", data.cFileName, reinterpret_cast< uint32* >( mod ));
			if( mod == 0 )
			{
				printf( "error!\n" );
			}
			else
			{
				// find version import
				exp_get_version vcall = (exp_get_version)GetProcAddress(mod, "_exp_get_version");
				exp_script_register rcall = (exp_script_register)GetProcAddress(mod, "_exp_script_register");
				exp_get_script_type scall = (exp_get_script_type)GetProcAddress(mod, "_exp_get_script_type");
				if(vcall == 0 || rcall == 0 || scall == 0)
				{
					printf("version functions not found!\n");
					FreeLibrary(mod);
				}
				else
				{
					uint32 version = vcall();
					uint32 stype = scall();
					if(SCRIPTLIB_LOPART(version) == SCRIPTLIB_VERSION_MINOR && SCRIPTLIB_HIPART(version) == SCRIPTLIB_VERSION_MAJOR)
					{
						if( stype & SCRIPT_TYPE_SCRIPT_ENGINE )
						{
							printf("v%u.%u : ", SCRIPTLIB_HIPART(version), SCRIPTLIB_LOPART(version));
							printf("delayed load.\n");

							ScriptingEngine se;
							se.Handle = mod;
							se.InitializeCall = rcall;
							se.Type = stype;

							ScriptEngines.push_back( se );
						}
						else
						{
							_handles.push_back(((SCRIPT_MODULE)mod));
							printf("v%u.%u : ", SCRIPTLIB_HIPART(version), SCRIPTLIB_LOPART(version));
							rcall(this);
							printf("loaded.\n");
						}

						++count;
					}
					else
					{
						FreeLibrary(mod);
						printf("version mismatch!\n");
					}
				}
			}
		}
		while(FindNextFile(find_handle, &data));
		FindClose(find_handle);
		MyLog::log->notice("Server : Loaded %u external libraries.", count);
		MyLog::log->notice("Server : Loading optional scripting engines...");
		for(vector<ScriptingEngine>::iterator itr = ScriptEngines.begin(); itr != ScriptEngines.end(); ++itr)
		{
			if( itr->Type & SCRIPT_TYPE_SCRIPT_ENGINE_LUA )
			{
				// lua :O
				if( Config.MainConfig.GetBoolDefault("ScriptBackends", "LUA", true))
				{
					MyLog::log->notice("Server : Initializing LUA script engine...");
					itr->InitializeCall(this);
					_handles.push_back( (SCRIPT_MODULE)itr->Handle );
				}
				else
				{
					FreeLibrary( itr->Handle );
				}
			}
			else if( itr->Type & SCRIPT_TYPE_SCRIPT_ENGINE_AS )
			{
				if( Config.MainConfig.GetBoolDefault("ScriptBackends", "AS", false) )
				{
					MyLog::log->notice("Server : Initializing AngelScript script engine...");
					itr->InitializeCall(this);
					_handles.push_back( (SCRIPT_MODULE)itr->Handle );
				}
				else
				{
					FreeLibrary( (*itr).Handle );
				}
			}
			else
			{
				MyLog::log->notice("Server : Unknown script engine type: 0x%.2X, please contact developers.", (*itr).Type);
				FreeLibrary( itr->Handle );
			}
		}
		MyLog::log->notice("Server : Done loading script engines...");
	}
#else
	
	struct dirent ** list;
	int filecount = scandir( "/lib/", &list, 0, 0 );
	uint32 count = 0;

	if(!filecount || !list || filecount < 0)
		MyLog::log->error("  No external scripts found! Server will continue to function with limited functionality.");
	else
	{
char *ext;
		while(filecount--)
		{
			ext = strrchr(list[filecount]->d_name, '.');
#ifdef HAVE_DARWIN
			if (ext != NULL && strstr(list[filecount]->d_name, ".0.dylib") == NULL && !strcmp(ext, ".dylib")) {
#else
                        if (ext != NULL && !strcmp(ext, ".so")) {
#endif
				string full_path = "../lib/" + string(list[filecount]->d_name);
				SCRIPT_MODULE mod = dlopen(full_path.c_str(), RTLD_NOW);
				printf("  %s : 0x%p : ", list[filecount]->d_name, mod);
				if(mod == 0)
					printf("error! [%s]\n", dlerror());
				else
				{
					// find version import
					exp_get_version vcall = (exp_get_version)dlsym(mod, "_exp_get_version");
					exp_script_register rcall = (exp_script_register)dlsym(mod, "_exp_script_register");
					exp_get_script_type scall = (exp_get_script_type)dlsym(mod, "_exp_get_script_type");
					if(vcall == 0 || rcall == 0 || scall == 0)
					{
						printf("version functions not found!\n");
						dlclose(mod);
					}
					else
					{
						int32 version = vcall();
						uint32 stype = scall();
						if(SCRIPTLIB_LOPART(version) == SCRIPTLIB_VERSION_MINOR && SCRIPTLIB_HIPART(version) == SCRIPTLIB_VERSION_MAJOR)
						{
							if( stype & SCRIPT_TYPE_SCRIPT_ENGINE )
							{
								printf("v%u.%u : ", SCRIPTLIB_HIPART(version), SCRIPTLIB_LOPART(version));
								printf("delayed load.\n");

								ScriptingEngine se;
								se.Handle = mod;
								se.InitializeCall = rcall;
								se.Type = stype;

								ScriptEngines.push_back( se );
							}
							else
							{
								_handles.push_back(((SCRIPT_MODULE)mod));
								printf("v%u.%u : ", SCRIPTLIB_HIPART(version), SCRIPTLIB_LOPART(version));
								rcall(this);
								printf("loaded.\n");
							}

							++count;
						}
						else
						{
							dlclose(mod);
							printf("version mismatch!\n");
						}
					}
				}
			}
			free(list[filecount]);
		}
		free(list);
		MyLog::log->info("");
		MyLog::log->info("Loaded %u external libraries.", count);
		MyLog::log->info("");

		MyLog::log->info("Loading optional scripting engines...");
		for(vector<ScriptingEngine>::iterator itr = ScriptEngines.begin(); itr != ScriptEngines.end(); ++itr)
		{
			if( itr->Type & SCRIPT_TYPE_SCRIPT_ENGINE_LUA )
			{
				// lua :O
				if( Config.MainConfig.GetBoolDefault("ScriptBackends", "LUA", false) )
				{
					MyLog::log->info("   Initializing LUA script engine...");
					itr->InitializeCall(this);
					_handles.push_back( (SCRIPT_MODULE)itr->Handle );
				}
				else
				{
					dlclose( itr->Handle );
				}
			}
			else if( itr->Type & SCRIPT_TYPE_SCRIPT_ENGINE_AS )
			{
				if( Config.MainConfig.GetBoolDefault("ScriptBackends", "AS", false) )
				{
					MyLog::log->info("   Initializing AngelScript script engine...");
					itr->InitializeCall(this);
					_handles.push_back( (SCRIPT_MODULE)itr->Handle );
				}
				else
				{
					dlclose( (*itr).Handle );
				}
			}
			else
			{
				MyLog::log->info("  Unknown script engine type: 0x%.2X, please contact developers.", (*itr).Type );
				dlclose( itr->Handle );
			}
		}
		MyLog::log->info("Done loading script engines...");
	}
#endif
		*/
}

void ScriptMgr::UnloadScripts()
{
	/*
	if(HookInterface::getSingletonPtr())
		delete HookInterface::getSingletonPtr();

	for(CustomGossipScripts::iterator itr = _customgossipscripts.begin(); itr != _customgossipscripts.end(); ++itr)
		(*itr)->Destroy();
	_customgossipscripts.clear();
	delete this->DefaultGossipScript;
	this->DefaultGossipScript=NULL;

	LibraryHandleMap::iterator itr = _handles.begin();
	for(; itr != _handles.end(); ++itr)
	{
#ifdef WIN32
		FreeLibrary(((HMODULE)*itr));
#else
		dlclose(*itr);
#endif
	}
	_handles.clear();
	*/
}


void ScriptMgr::LoadString(const char* sz)
{

	if (_Scriptluastate)
	{
		_Scriptluastate->DoFile(sz);
	}
}


void ScriptMgr::register_creature_script(uint32 entry, exp_create_creature_ai callback)
{
	_creatures.insert( CreatureCreateMap::value_type( entry, callback ) );
}

void ScriptMgr::register_gameobject_script(uint32 entry, exp_create_gameobject_ai callback)
{
	_gameobjects.insert( GameObjectCreateMap::value_type( entry, callback ) );
}

void ScriptMgr::register_dummy_aura(uint32 entry, exp_handle_dummy_aura callback)
{
	_auras.insert( HandleDummyAuraMap::value_type( entry, callback ) );
}

void ScriptMgr::register_dummy_spell(uint32 entry, exp_handle_dummy_spell callback)
{
	_spells.insert( HandleDummySpellMap::value_type( entry, callback ) );
}

void ScriptMgr::register_gossip_script(uint32 entry, GossipScript * gs)
{
	CreatureInfo * ci = CreatureNameStorage.LookupEntry(entry);
	if(ci)
		ci->gossip_script = gs;

	_customgossipscripts.insert(gs);
}

void ScriptMgr::register_go_gossip_script(uint32 entry, GossipScript * gs)
{
	GameObjectInfo * gi = GameObjectNameStorage.LookupEntry(entry);
	if(gi)
		gi->gossip_script = gs;

	_customgossipscripts.insert(gs);
}

void ScriptMgr::register_quest_script(uint32 entry, QuestScript * qs)
{
	Quest * q = QuestStorage.LookupEntry( entry );
// 	if( q != NULL )
// 		q->pQuestScript = qs;

	_questscripts.insert( qs );
}

CreatureAIScript* ScriptMgr::CreateAIScriptClassForEntry(Creature* pCreature)
{

	//CreatureCreateMap::iterator itr = _creatures.find(pCreature->GetEntry());
	//if(itr == _creatures.end())
	//	return NULL;

	if( !pCreature->proto )
		return NULL;

	//exp_create_creature_ai function_ptr = itr->second;
	//return (function_ptr)(pCreature);
	CreatureAIScript* pAi = NULL;


	if (!pCreature->proto->lua_script)
	{
		return NULL;
	}
	
	std::string strScript = pCreature->proto->lua_script;

	std::string strEntry;
	MAPScripts::iterator it = m_mapScripts.find(strScript);
	if(it == m_mapScripts.end())
	{
		if (pCreature->proto->lua_script[0] != 0)
		{
			pAi = new CreatureAIScriptSunyou(pCreature);

			std::string strTemp;
			int nSize = strScript.size();
			for (int i = 0; i < nSize  ; i ++)
			{
				if ( strScript[i] == '/')
				{
					strTemp.clear();
				}
				else if (strScript[i] == '.')
				{
					break;
				}
				else 
				{
					strTemp.push_back(strScript[i]);
				}


			}

				int n = atoi(strTemp.c_str());
				((CreatureAIScriptSunyou*)pAi)->m_str = "_" + strTemp;

				m_mapScripts.insert(MAPScripts::value_type(strScript,pAi ));

			pAi->Init();	
			//_mapCreature.insert(MAPCREATURE::value_type(pCreature->GetEntry(), pAi));

		
		}
	}
	else
	{
		pAi = it->second;
	}

	CreatureAIScriptSunyou* pAiNew = NULL;
	if (pAi)
	{
		_Scriptluastate->DoFile(strScript.c_str());
		pAiNew =new CreatureAIScriptSunyou(pCreature);
		pAiNew->Init();
		pAiNew->SetUnit( pCreature);
		pAiNew->m_str = ((CreatureAIScriptSunyou*)(pAi))->m_str;
		pAiNew->mLuaState = _Scriptluastate;
	}

	return pAiNew;



}

GameObjectAIScript * ScriptMgr::CreateAIScriptClassForGameObject(uint32 uEntryId, GameObject* pGameObject)
{
	GameObjectCreateMap::iterator itr = _gameobjects.find(pGameObject->GetEntry());
	if(itr == _gameobjects.end())
		return NULL;

	exp_create_gameobject_ai function_ptr = itr->second;
	return (function_ptr)(pGameObject);
}

bool ScriptMgr::CallScriptedDummySpell(uint32 uSpellId, uint32 i, Spell* pSpell)
{
	HandleDummySpellMap::iterator itr = _spells.find(uSpellId);
	if(itr == _spells.end())
		return false;

	exp_handle_dummy_spell function_ptr = itr->second;
	return (function_ptr)(i, pSpell);
}

bool ScriptMgr::CallScriptedDummyAura(uint32 uSpellId, uint32 i, Aura* pAura, bool apply)
{
	HandleDummyAuraMap::iterator itr = _auras.find(uSpellId);
	if(itr == _auras.end())
		return false;

	exp_handle_dummy_aura function_ptr = itr->second;
	return (function_ptr)(i, pAura, apply);
}

bool ScriptMgr::CallScriptedItem(Item * pItem, Player * pPlayer)
{
	if(pItem->GetProto()->gossip_script)
	{
		pItem->GetProto()->gossip_script->GossipHello(pItem,pPlayer,true);
		return true;
	}

	return false;
}

void ScriptMgr::register_item_gossip_script(uint32 entry, GossipScript * gs)
{
	ItemPrototype * proto = ItemPrototypeStorage.LookupEntry(entry);
	if(proto)
		proto->gossip_script = gs;

	_customgossipscripts.insert(gs);
}

/* CreatureAI Stuff */
CreatureAIScript::CreatureAIScript(Creature* creature) : _unit(creature)
{

}
CreatureAIScript::CreatureAIScript()
{

}

void CreatureAIScript::RegisterAIUpdateEvent(uint32 frequency)
{
	sEventMgr.AddEvent(_unit, &Creature::CallScriptUpdate, EVENT_SCRIPT_UPDATE_EVENT, frequency, 0,0);
}

void CreatureAIScript::ModifyAIUpdateEvent(uint32 newfrequency)
{
	sEventMgr.ModifyEventTimeAndTimeLeft(_unit, EVENT_SCRIPT_UPDATE_EVENT, newfrequency);
}

void CreatureAIScript::RemoveAIUpdateEvent()
{
	sEventMgr.RemoveEvents(_unit, EVENT_SCRIPT_UPDATE_EVENT);
}

/* GameObjectAI Stuff */

GameObjectAIScript::GameObjectAIScript(GameObject* goinstance) : _gameobject(goinstance)
{

}

void GameObjectAIScript::RegisterAIUpdateEvent(uint32 frequency)
{
	sEventMgr.AddEvent(_gameobject, &GameObject::CallScriptUpdate, EVENT_SCRIPT_UPDATE_EVENT, frequency, 0,0);
}


/* InstanceAI Stuff */

InstanceScript::InstanceScript(MapMgr *instance) : _instance(instance)
{
}

/* QuestScript Stuff */

/* Gossip Stuff*/

GossipScript::GossipScript()
{

}

void GossipScript::GossipEnd(Object* pObject, Player* Plr)
{
	Plr->CleanupGossipMenu();
}

bool CanTrainAt(Player * plr, Trainer * trn);
void GossipScript::GossipHello(Object* pObject, Player* Plr, bool AutoSend)
{
	GossipMenu *Menu;
	uint32 TextID = 2;
	Creature * pCreature = (pObject->GetTypeId()==TYPEID_UNIT)?static_cast< Creature* >( pObject ):NULL;
	if(!pCreature)
		return;

	Trainer *pTrainer = pCreature->GetTrainer();
	uint32 Text = objmgr.GetGossipTextForNpc(pCreature->GetEntry());
	//if(Text != 0)
	//{
		//GossipText * text = NpcTextStorage.LookupEntry(Text);
		//if(text != 0)
			TextID = Text;
	//}

	objmgr.CreateGossipMenuForPlayer(&Menu, pCreature->GetGUID(), TextID, Plr);

	uint32 flags = pCreature->GetUInt32Value(UNIT_NPC_FLAGS);

	if(flags & UNIT_NPC_FLAG_VENDOR)
	{
		Menu->AddItem(ICON_BUY, build_language_string( BuildString_ScriptWhatDoYouNeed ), 1 );//"您想要点什么?", 1);
	}

	if((flags & UNIT_NPC_FLAG_TRAINER_CLASS || flags & UNIT_NPC_FLAG_TRAINER_PROF) && pTrainer != 0)
	{
		string name = pCreature->GetCreatureName()->Name;
		string::size_type pos = name.find(" ");	  // only take first name
		if(pos != string::npos)
			name = name.substr(0, pos);

		if(CanTrainAt(Plr, pTrainer))
			Menu->SetTextID(pTrainer->Can_Train_Gossip_TextId);
		else
			Menu->SetTextID(pTrainer->Cannot_Train_GossipTextId);

		string msg;
		if(pTrainer->RequiredClass)
		{
			switch(Plr->getClass())
			{
			case CLASS_MAGE:
				msg =build_language_string( BuildString_GuildTrainerSpellMage, name.c_str() );
				break;
			case CLASS_WARRIOR:
				msg =build_language_string( BuildString_GuildTrainerSpellWarrior,name.c_str() );
				break;
			case CLASS_BOW:
				msg =build_language_string( BuildString_GuildTrainerSpellBow,name.c_str() );
				break;
			case CLASS_ROGUE:
				msg += "rogue";
				break;
			case CLASS_DRUID:
				msg += "druid";
				break;
			case CLASS_PRIEST:
				msg =build_language_string( BuildString_GuildTrainerSpellPriest,name.c_str() );
				break;
			}
			//msg += " training, ";
			//msg += name;
			//msg += ".";

			Menu->AddItem(ICON_SPELLTRAINER, msg.c_str(), 2);

		}
		else
		{
			//msg += "training, ";
			//msg += name;
			//msg += ".";
			msg =build_language_string( BuildString_GuildTrainerSpellPriest,name.c_str() );

			Menu->AddItem(ICON_SKILLTRAINER, msg.c_str(), 2);
		}
	}

	//if(flags & UNIT_NPC_FLAG_TAXIVENDOR)
	//	Menu->AddItem(2, "Give me a ride.", 3);

	if(flags & UNIT_NPC_FLAG_AUCTIONEER)
		Menu->AddItem(ICON_AUCTION, build_language_string( BuildString_ScriptAuction ), 4);

	if(flags & UNIT_NPC_FLAG_INNKEEPER)
		Menu->AddItem(ICON_INN, build_language_string( BuildString_ScriptHomeBind ), 5);

	if(flags & UNIT_NPC_FLAG_BANKER)
		Menu->AddItem(ICON_BANK, build_language_string( BuildString_ScriptBank ), 6);

	if(flags & UNIT_NPC_FLAG_ARENACHARTER)
		Menu->AddItem(0, "How do I create a guild/arena team?", 8);

	if(flags & UNIT_NPC_FLAG_BATTLEFIELDPERSON)
		Menu->AddItem(0, "I would like to go to the battleground.", 10);

	//if(flags & UNIT_NPC_FLAG_MAIL)
	//	Menu->AddItem(0, "打开我的邮箱", 15);

	if(flags & UNIT_NPC_FLAG_JIAYUAN) //激活奖励
		Menu->AddItem(0, build_language_string( BuildString_ScriptInputGiftSerial ), 18);
	

	if(flags & UNIT_NPC_FLAG_TRANSPORTER)
	{
		StorageContainerIterator<stNpcScriptTrans>* it = dbcNpcScriptTrans.MakeIterator();
		while(!it->AtEnd())
		{
			stNpcScriptTrans* nst = it->Get();
			if(pCreature->creature_info->Id == nst->npc_entry && nst->transpos_id)
				Menu->AddItem(ICON_TELEPORT, nst->name, nst->transpos_id);
			if(!it->Inc())
				break;
		}
		it->Destruct();
		/*
		if( pCreature->creature_info->Id == 517 )
		{
			if(Plr->getRace() == RACE_REN)
				Menu->AddItem(0, build_language_string( BuildString_ScriptTeleport2Ren ), 10001 );//"传送到人族地图", 10001);
			else if(Plr->getRace() == RACE_WU)
				Menu->AddItem(0, build_language_string( BuildString_ScriptTeleport2Wu ), 10002 );//"传送到巫族地图", 10002);
			else if(Plr->getRace() == RACE_YAO)
				Menu->AddItem(0, build_language_string( BuildString_ScriptTeleport2Yao ), 10003 );//"传送到妖族地图", 10003);
			//Menu->AddItem(0, "传送到本种族地图", 100020406);
		}
		*/
	}

	if(flags & UNIT_NPC_FLAG_GUILD)
	{
		Menu->AddItem(ICON_GUILD, build_language_string( BuildString_ScriptCreateGuild ), 300);
		Menu->AddItem(ICON_GUILD, build_language_string( BuildString_ScriptQueryGuild ), 301);
		if (Plr->GetGuild()&&Plr->GetGuild()->GetGuildLeader() == Plr->GetGUID() && Plr->GetGuild()->GetLevel() < 10)
		{
			Menu->AddItem(ICON_GUILD, build_language_string( BuildString_ScriptUpgradeGuild ), 302);
		}

	
		//Menu->AddItem(0, "报名攻打[瑞卡斯战场]", 303);
		//Menu->AddItem(0, "进入[瑞卡斯战场]", 304);
		//Menu->AddItem(0, "查看[瑞卡斯战场]的守卫情况", 305);
	}

	if( flags & UNIT_NPC_FLAG_BAISHI )
	{
		Menu->AddItem( 0, build_language_string( BuildString_ScriptDisbandTeacherStudent ), 401 );
	}

	if( pTrainer &&
		pTrainer->RequiredClass &&					  // class trainer
		pTrainer->RequiredClass == Plr->getClass() &&   // correct class
		pCreature->getLevel() > 10 &&				   // creature level
		Plr->getLevel() > 10 )						  // player level
	{
		//modify by hao zi
		//Menu->AddItem(0, "I would like to reset my talents.", 11);
	}

	if( pTrainer &&
		pTrainer->TrainerType == TRAINER_TYPE_PET &&	// pet trainer type
		Plr->getClass() == CLASS_BOW &&					// hunter class
		Plr->GetSummon() != NULL )						// have pet
	{
		Menu->AddItem(0, "I would like to untrain my pet.", 13); //TODO: Find proper message
	}

	if( pCreature->GetSunyouRaid() )
	{
		for( size_t i = 0; i < pCreature->m_vRaidGossip.size(); ++i )
		{
			if( pCreature->m_vRaidGossipEnabled[i] )
				Menu->AddItem(0, pCreature->m_vRaidGossip[i].c_str(), 3000 + i );
		}
	}

	if(AutoSend)
		Menu->SendTo(Plr);
}

void GossipScript::GossipSelectOption(Object* pObject, Player* Plr, uint32 Id, uint32 IntId, const char * EnteredCode)
{
	if( pObject->GetTypeId() != TYPEID_UNIT )
		return;
	Creature* pCreature = static_cast< Creature* >( pObject );

	stNpcTransPos* pPos = dbcNpcTransPos.LookupEntry(IntId);
	if(pPos)
	{
		Plr->SafeTeleport(pPos->mapid, 0, pPos->x, pPos->y, pPos->z, pPos->o);return;
	}

	switch( IntId )
	{
	case 1:
		// vendor
		Plr->GetSession()->SendInventoryList(pCreature);
		break;
	case 2:
		// trainer
		Plr->GetSession()->SendTrainerList(pCreature);
		break;
	case 3:
		// taxi
		Plr->GetSession()->SendTaxiList(pCreature);
		break;
	case 4:
		// auction
		Plr->GetSession()->SendAuctionList(pCreature);
		break;
	case 5:
		// innkeeper
		Plr->GetSession()->SendInnkeeperBind(pCreature);
		break;
	case 6:
		// banker
		Plr->GetSession()->SendBankerList(pCreature);
		break;
	case 7:
		// spirit
		Plr->GetSession()->SendSpiritHealerRequest(pCreature);
		break;
	case 8:
		// petition
		Plr->GetSession()->SendCharterRequest(pCreature);
		break;
	case 9:
		// guild crest
		Plr->GetSession()->SendTabardHelp(pCreature);
		break;
	case 10:
		// battlefield
		//Plr->GetSession()->SendBattlegroundList(pCreature, 0);
		break;
	case 11:
		// switch to talent reset message
		{
			GossipMenu *Menu;
			objmgr.CreateGossipMenuForPlayer(&Menu, pCreature->GetGUID(), 5674, Plr);
			Menu->AddItem(0, "I understand, continue.", 12);
			Menu->SendTo(Plr);
		}break;
	case 12:
		// talent reset
		{
			Plr->Gossip_Complete();
			Plr->SendTalentResetConfirm();
		}break;
	case 13:
		// switch to untrain message
		{
			GossipMenu *Menu;
			objmgr.CreateGossipMenuForPlayer(&Menu, pCreature->GetGUID(), 7722, Plr);
			Menu->AddItem(0, "Yes, please do.", 14);
			Menu->SendTo(Plr);
		}break;
	case 14:
		// untrain pet
		{
			Plr->Gossip_Complete();
			Plr->SendPetUntrainConfirm();
		}break;
	case 15:
		{
			MSG_S2C::stMail_List_Result MsgMailList;
			Plr->m_mailBox->BuildMailboxListingPacket(&MsgMailList);
			MsgMailList.npc_guid = pCreature->GetGUID();
			Plr->GetSession()->SendPacket(MsgMailList);
		}
		break;
	case 18:
		{
			//发送领取激活码的消息
			g_Serial_System->SerialToPlayer(Plr);
		}
		break;
	case 300:
		{
			MSG_S2C::stGuild_Create Msg;
			Plr->GetSession()->SendPacket(Msg);
		}
		break;
	case 301:
		{
			//if( Plr->m_playerInfo->guildRank && Plr->m_playerInfo->guildRank->iId == 0 )
			{
				Guild::SendGuildListInfo( Plr->GetSession() );
			}
		}
		break;
	case 302:
		{

			if (Plr->GetGuild()&&Plr->GetGuild()->GetGuildLeader() == Plr->GetGUID() && Plr->GetGuild()->GetLevel() <= 9)
			{
				MSG_S2C::stGuild_Npc_Level_Up Msg;
				Msg.Level = Plr->GetGuild()->GetLevel();
				Plr->GetSession()->SendPacket(Msg);
			}

			//if( Plr->m_playerInfo->guildRank && Plr->m_playerInfo->guildRank->iId == 0 )
			//{
			//	if( Plr->m_playerInfo->guild )
			//		Plr->m_playerInfo->guild->LevelUp( Plr->GetSession() );
			//}
		}
		break;
	case 303:
		{
			/*
			SunyouCastle* castle = g_instancequeuemgr.GetCastleByMapID( 28 );
			if( castle )
				castle->Enroll( Plr );
				*/
		}
		break;
	case 304:
		{			
			//g_instancequeuemgr.JoinRequest( Plr, 28 );
		}
		break;
	case 305:
		{
			SunyouCastle* castle = g_instancequeuemgr.GetCastleByMapID( 28 );
			if( castle )
				castle->SendNPCState2Player( Plr );
		}
		break;
	case 401:
		{
			Plr->RemoveTeacherStudentRelationship();
		}
		break;
	case 3000:
	case 3001:
	case 3002:
	case 3003:
	case 3004:
	case 3005:
	case 3006:
	case 3007:
	case 3008:
	case 3009:
	case 3010:
		{
			if( pCreature->GetSunyouRaid() && pCreature->IsRaidGossipEnabled( IntId - 3000 ) )
			{
				pCreature->GetSunyouRaid()->OnNPCGossipTrigger( pCreature, IntId - 3000, Plr );
			}
		}
		break;
	default:
		MyLog::log->error("Unknown IntId %u on entry %u", IntId, pCreature->GetEntry());
		break;
	}
}

void GossipScript::Destroy()
{
	delete this;
}

void ScriptMgr::register_hook(ServerHookEvents event, void * function_pointer)
{
	ASSERT(event < NUM_SERVER_HOOKS);
	_hooks[event].push_back(function_pointer);
}

/* Hook Implementations */
#define OUTER_LOOP_BEGIN(type, fptr_type) if(!sScriptMgr._hooks[type].size()) { \
	return; } \
	fptr_type call; \
	for(ServerHookList::iterator itr = sScriptMgr._hooks[type].begin(); itr != sScriptMgr._hooks[type].end(); ++itr) { \
	call = ((fptr_type)*itr);

#define OUTER_LOOP_END }

#define OUTER_LOOP_BEGIN_COND(type, fptr_type) if(!sScriptMgr._hooks[type].size()) { \
	return true; } \
	fptr_type call; \
	bool ret_val = true; \
	for(ServerHookList::iterator itr = sScriptMgr._hooks[type].begin(); itr != sScriptMgr._hooks[type].end(); ++itr) { \
		call = ((fptr_type)*itr);

#define OUTER_LOOP_END_COND } return ret_val;

bool HookInterface::OnNewCharacter(uint32 Race, uint32 Class, WorldSession * Session, const char * Name)
{
	OUTER_LOOP_BEGIN_COND(SERVER_HOOK_EVENT_ON_NEW_CHARACTER, tOnNewCharacter)
		ret_val = (call)(Race, Class, Session, Name);
	OUTER_LOOP_END_COND
}

void HookInterface::OnKillPlayer(Player * pPlayer, Player * pVictim)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_KILL_PLAYER, tOnKillPlayer)
		(call)(pPlayer, pVictim);
	OUTER_LOOP_END
}

void HookInterface::OnFirstEnterWorld(Player * pPlayer)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_FIRST_ENTER_WORLD, tOnFirstEnterWorld)
		(call)(pPlayer);
	OUTER_LOOP_END
}

void HookInterface::OnCharacterCreate(Player * pPlayer)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_CHARACTER_CREATE, tOCharacterCreate)
		(call)(pPlayer);
	OUTER_LOOP_END
}

void HookInterface::OnEnterWorld(Player * pPlayer)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_ENTER_WORLD, tOnEnterWorld)
		(call)(pPlayer);
	OUTER_LOOP_END
}

void HookInterface::OnGuildCreate(Player * pLeader, Guild * pGuild)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_GUILD_CREATE, tOnGuildCreate)
		(call)(pLeader, pGuild);
	OUTER_LOOP_END
}

void HookInterface::OnGuildJoin(Player * pPlayer, Guild * pGuild)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_GUILD_JOIN, tOnGuildJoin)
		(call)(pPlayer, pGuild);
	OUTER_LOOP_END
}

void HookInterface::OnDeath(Player * pPlayer)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_DEATH, tOnDeath)
		(call)(pPlayer);
	OUTER_LOOP_END
}

bool HookInterface::OnRepop(Player * pPlayer)
{
	OUTER_LOOP_BEGIN_COND(SERVER_HOOK_EVENT_ON_REPOP, tOnRepop)
		ret_val = (call)(pPlayer);
	OUTER_LOOP_END_COND
}

void HookInterface::OnEmote(Player * pPlayer, uint32 Emote)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_EMOTE, tOnEmote)
		(call)(pPlayer, Emote);
	OUTER_LOOP_END
}

void HookInterface::OnEnterCombat(Player * pPlayer, Unit * pTarget)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_ENTER_COMBAT, tOnEnterCombat)
		(call)(pPlayer, pTarget);
	OUTER_LOOP_END
}

bool HookInterface::OnCastSpell(Player * pPlayer, SpellEntry* pSpell)
{
	OUTER_LOOP_BEGIN_COND(SERVER_HOOK_EVENT_ON_CAST_SPELL, tOnCastSpell)
		ret_val = (call)(pPlayer, pSpell);
	OUTER_LOOP_END_COND
}

bool HookInterface::OnLogoutRequest(Player * pPlayer)
{
	OUTER_LOOP_BEGIN_COND(SERVER_HOOK_EVENT_ON_LOGOUT_REQUEST, tOnLogoutRequest)
		ret_val = (call)(pPlayer);
	OUTER_LOOP_END_COND
}

void HookInterface::OnLogout(Player * pPlayer)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_LOGOUT, tOnLogout)
		(call)(pPlayer);
	OUTER_LOOP_END
}

void HookInterface::OnQuestAccept(Player * pPlayer, Quest * pQuest)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_QUEST_ACCEPT, tOnQuestAccept)
		(call)(pPlayer, pQuest);
	OUTER_LOOP_END
}

void HookInterface::OnZone(Player * pPlayer, uint32 Zone)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_ZONE, tOnZone)
		(call)(pPlayer, Zone);
	OUTER_LOOP_END
}

bool HookInterface::OnChat(Player * pPlayer, uint32 Type, uint32 Lang, const char * Message, const char * Misc)
{
	OUTER_LOOP_BEGIN_COND(SERVER_HOOK_EVENT_ON_CHAT, tOnChat)
		ret_val = (call)(pPlayer, Type, Lang, Message, Misc);
	OUTER_LOOP_END_COND
}

void HookInterface::OnLoot(Player * pPlayer, Unit * pTarget, uint32 Money, uint32 ItemId)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_LOOT, tOnLoot)
		(call)(pPlayer, pTarget, Money, ItemId);
	OUTER_LOOP_END
}

void HookInterface::OnObjectLoot(Player * pPlayer, Object * pTarget, uint32 Money, uint32 ItemId)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_OBJECTLOOT, tOnObjectLoot)
		(call)(pPlayer, pTarget, Money, ItemId);
	OUTER_LOOP_END
}

void HookInterface::OnEnterWorld2(Player * pPlayer)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_ENTER_WORLD_2, tOnEnterWorld)
		(call)(pPlayer);
	OUTER_LOOP_END
}

void HookInterface::OnQuestCancelled(Player * pPlayer, Quest * pQuest)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_QUEST_CANCELLED, tOnQuestCancel)
		(call)(pPlayer, pQuest);
	OUTER_LOOP_END
}

void HookInterface::OnQuestFinished(Player * pPlayer, Quest * pQuest)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_QUEST_FINISHED, tOnQuestFinished)
		(call)(pPlayer, pQuest);
	OUTER_LOOP_END
}

void HookInterface::OnHonorableKill(Player * pPlayer, Player * pKilled)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_HONORABLE_KILL, tOnHonorableKill)
		(call)(pPlayer, pKilled);
	OUTER_LOOP_END
}

void HookInterface::OnArenaFinish(Player * pPlayer, ArenaTeam* pTeam, bool victory, bool rated)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_ARENA_FINISH, tOnArenaFinish)
		(call)(pPlayer, pTeam, victory, rated);
	OUTER_LOOP_END
}

void HookInterface::OnPostLevelUp(Player * pPlayer)
{
	OUTER_LOOP_BEGIN(SERVER_HOOK_EVENT_ON_POST_LEVELUP, tOnPostLevelUp)
		(call)(pPlayer);
	OUTER_LOOP_END
}
