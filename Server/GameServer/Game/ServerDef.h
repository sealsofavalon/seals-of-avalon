#pragma once
#include "PublicDef.h"

//������ʱ��
extern uint32 TIME_PILAO_GAME;
extern uint32 TIME_BUJIANGKANG_GAME;

#define ITEM_ID_HEARTH_STONE 11

#define ITEM_ID_DALABA 9
#define ITEM_ID_XIAOLABA 8

#define SPELL_ID_CAIJI 4
#define SPELL_ID_KAIQI 5
#define SPELL_ID_KAIXIANGZI 6
#define SPELL_ID_KAIYOUXIANG 86
#define SPELL_ID_REFINE 87

#define SPELL_ID_BAISHI 2101

#define ITEM_ID_XINCHUNGE 700000001

enum INVIS_FLAG
{
	INVIS_FLAG_NORMAL, // players and units with no special invisibility flags
	INVIS_FLAG_SPIRIT1,
	INVIS_FLAG_SPIRIT2,
	INVIS_FLAG_TRAP,
	INVIS_FLAG_QUEST,
	INVIS_FLAG_GHOST,
	INVIS_FLAG_SHADOWMOON,
	INVIS_FLAG_NETHERSTORM,
	INVIS_FLAG_TOTAL
};

struct BGScore
{
	uint32 KillingBlows;
	uint32 HonorableKills;
	uint32 Deaths;
	uint32 BonusHonor;
	uint32 DamageDone;
	uint32 HealingDone;
	uint32 Misc1;
	uint32 Misc2;
};


enum UNIT_TYPE
{
	NOUNITTYPE	  = 0,
	BEAST		   = 1,
	DRAGONSKIN	  = 2,
	DEMON		   = 3,
	ELEMENTAL	   = 4,
	GIANT		   = 5,
	UNDEAD		  = 6,
	HUMANOID		= 7,
	CRITTER		 = 8,
	MECHANICAL	  = 9,
	UNIT_TYPE_MISC  = 10,
};


enum ArenaTeamTypes
{
	ARENA_TEAM_TYPE_2V2			= 0,
	ARENA_TEAM_TYPE_3V3			= 1,
	ARENA_TEAM_TYPE_5V5			= 2,
	NUM_ARENA_TEAM_TYPES		= 3,
};


#define UF_TARGET_DIED  1
#define UF_ATTACKING	2 // this unit is attacking it's selection

#define UNIT_TYPE_HUMANOID_BIT (1 << (HUMANOID-1)) //should get computed by precompiler ;)


typedef map<SpellEntry*, uint16> PetSpellMap;


struct DamageProc
{
	uint32 m_spellId;
	uint32 m_damage;
	//  uint64 m_caster;//log is: some reflects x arcane/nature damage to 'attacker' no matter who casted
	uint32 m_school;
	uint32 m_flags;
	void  *owner;//mark the owner of this proc to know which one to delete
};

struct DamageSplitTarget
{
	uint64 m_target; // we store them
	uint32 m_spellId;
	float m_pctDamageSplit; // % of taken damage to transfer (i.e. Soul Link)
	uint32 m_flatDamageSplit; // flat damage to transfer (i.e. Blessing of Sacrifice)
	uint8 damage_type; // bitwise 0-127 thingy
	void * creator;
};

#ifndef NEW_PROCFLAGS
struct ProcTriggerSpell
{
	ProcTriggerSpell() : origId(0), /*trigger(0),*/ spellId(0), caster(0), item(0), procChance(0), procFlags(0), procCharges(0), ref(0) { }
	uint32 origId;
	// uint32 trigger;
	uint32 spellId;
	uint64 caster;
	void* item;
	uint32 procChance;
	uint32 procFlags;
	uint32 procCharges;
	//    SpellEntry *ospinfo;
	//    SpellEntry *spinfo;
	uint32 LastTrigger;
	uint32 ProcType; //0=triggerspell/1=triggerclassspell
	uint32 ref;
	bool deleted;
};
#else
struct ProcTriggerSpell
{
	uint32 spellId;
	uint32 parentId;
	uint32 procFlags;
	uint32 procChance;
	uint32 procCharges;
	uint32 LastTrigger;
};
#endif



struct SpellCharge
{
	uint32 spellId;
	uint32 count;
	uint32 ProcFlag;
	uint32 lastproc;
	uint32 procdiff;
};
//////////////////////////////////////////////////////////////////////////
//	Guild
//////////////////////////////////////////////////////////////////////////

#define ITEM_ENTRY_GUILD_CHARTER 5863
#define ARENA_TEAM_CHARTER_2v2      23560
#define ARENA_TEAM_CHARTER_2v2_COST 800000  // 80 G
#define ARENA_TEAM_CHARTER_3v3      23561
#define ARENA_TEAM_CHARTER_3v3_COST 1200000 // 120 G
#define ARENA_TEAM_CHARTER_5v5      23562
#define ARENA_TEAM_CHARTER_5v5_COST 2000000 // 200 G

#define MAX_GUILD_BANK_SLOTS 98

//////////////////////////////////////////////////////////////////////////
//	Spell
//////////////////////////////////////////////////////////////////////////


// converting schools for 2.4.0 client
static const uint32 g_spellSchoolConversionTable[/*SCHOOL_ARCANE+*/1] = {
	1,				// SCHOOL_NORMAL
	//2,				// SCHOOL_HOLY
	//4,				// SCHOOL_FIRE
	//8,				// SCHOOL_NATURE
	//16,				// SCHOOL_FROST
	//32,				// SCHOOL_SHADOW
	//64,				// SCHOOL_ARCANE
};
//////////////////////////////////////////////////////////////////////////
//	Loot
//////////////////////////////////////////////////////////////////////////

typedef struct
{
	ItemPrototype * itemproto;
	uint32 displayid;
}_LootItem;

typedef std::set<uint32> LooterSet;

class LootRoll;
struct RandomProps;
struct ItemRandomSuffixEntry;
typedef struct
{
	_LootItem item;
	uint32 iItemsCount;
	RandomProps * iRandomProperty;
	ItemRandomSuffixEntry * iRandomSuffix;
	uint32 count;
	uint32 id[8];
	LootRoll *roll;
	bool passed;
	LooterSet has_looted;
	uint32 ffa_loot;
	uint32 drop_on_ground;
}__LootItem;

typedef struct
{
	std::vector<__LootItem> items;
	uint32 gold;
	LooterSet looters;
}Loot;


//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////

struct CreatureItem
{
	uint32 itemid;
	uint32 amount; //!!!!! stack amount.
	uint32 available_amount;
	uint32 max_amount;
	uint32 incrtime;
	uint32 extended_cost;
};

const float priceofstat[20] = 
{
	0.f,0.f,
	6.0f,5.5f,2.5f,1.5f,5.5f,1.5f,
	0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
};