#include "StdAfx.h"
#include "ShopActivity.h"

ShopActivity* g_shopactivity_system = NULL ;

ShopActivity::ShopActivity()
{
	for (int i = 0 ; i < MAX_SHOP_CATEGORY; i++)
	{
		m_use_activitys[i] = NULL;
	}
}
ShopActivity::~ShopActivity()
{

}

activity_mgr::shop_event_config_t* ShopActivity::FindShopActivity(ui32 actid)
{
	std::map<ui32, activity_mgr::shop_event_config_t*>::iterator it ;
	it = m_vshop_activitys.find(actid);
	if (it != m_vshop_activitys.end())
	{
		return  it->second; 
	}
	return NULL ;
}
void ShopActivity::AddShopActivity(activity_mgr::shop_event_config_t* p, bool fromdb)
{
	if (!p)
	{
		return  ;
	}
	if (!p->starttime || !p->expire)
	{
		return ;
	}

	if (p->expire <= (ui32) UNIXTIME)
	{
		return ;
	}
	
	if (!FindShopActivity(p->id))
	{
		m_vshop_activitys.insert(std::map<ui32, activity_mgr::shop_event_config_t*>::value_type(p->id, p ));
	}
	
	if (isIntime(p))
	{
		m_use_activitys[p->shop_item_category] = p;
	}
}
void ShopActivity::RemoveShopActivity(activity_mgr::shop_event_config_t* p)
{
	std::map<ui32, activity_mgr::shop_event_config_t*>::iterator it ;
	it = m_vshop_activitys.find(p->id);
	if (it != m_vshop_activitys.end())
	{
		m_vshop_activitys.erase(it);
	}
	
	if (m_use_activitys[p->shop_item_category] == p)
	{
		MSG_S2C::stShopActivity_Update msg;
		msg.category = p->shop_item_category;
		msg.discount = 100;
		sWorld.SendGlobalMessage(msg);
		m_use_activitys[p->shop_item_category] = NULL;
	}
}
void ShopActivity::UpdateShopActivity(activity_mgr::shop_event_config_t*p)
{
	for (int i = 0 ; i < MAX_SHOP_CATEGORY ; i++)
	{
		if (m_use_activitys[i] == p)
		{
			MSG_S2C::stShopActivity_Update msg;
			msg.category = i;
			msg.discount = 100;
			sWorld.SendGlobalMessage(msg);
			m_use_activitys[i] = NULL;
		}
	}

	if (isIntime(p))
	{
		
		MSG_S2C::stShopActivity_Update msg;
		msg.category = p->shop_item_category;
		msg.discount = p->discount;
		sWorld.SendGlobalMessage(msg);
		m_use_activitys[p->shop_item_category] = p;
	}
}
bool ShopActivity::isIntime(activity_mgr::shop_event_config_t* p)
{
	if (!p)
	{
		return false;
	}

	if (p->starttime  && p->expire)
	{
		if( UNIXTIME >= p->starttime && UNIXTIME <= p->expire )
		{
			for( int i = 0; i < 7; ++i )
			{	
				if(p->repeat_dayofweek[i] && is_dayofweek( UNIXTIME, i ) && in_duration( UNIXTIME,p->starthour, p->expirehour ) )
					return true;
			}
			return false;
		}
	}

	return false ;
}
void ShopActivity::GetShopActivity(vector<MSG_S2C::stShopActivity>& v_a)
{
	for (int i = 0 ; i < MAX_SHOP_CATEGORY ; i++)
	{
		if (m_use_activitys[i])
		{
			MSG_S2C::stShopActivity vi;
			vi.category = m_use_activitys[i]->shop_item_category;
			vi.discount = m_use_activitys[i]->discount;
			v_a.push_back(vi);
		}

	}
}
void ShopActivity::Update()
{
	for (int i = 0 ; i < MAX_SHOP_CATEGORY ; i++)
	{
		if (m_use_activitys[i] && !isIntime(m_use_activitys[i] ))
		{
			MSG_S2C::stShopActivity_Update msg;
			msg.category = m_use_activitys[i]->shop_item_category;
			msg.discount = 100;
			sWorld.SendGlobalMessage(msg);
			m_use_activitys[i] = NULL;
		}
	}

	std::map<ui32, activity_mgr::shop_event_config_t*>::iterator it2 = m_vshop_activitys.begin();
	for (it2; it2 != m_vshop_activitys.end(); ++it2)
	{
		activity_mgr::shop_event_config_t* p = it2->second;
		if (isIntime(p) && m_use_activitys[p->shop_item_category] != p)
		{
			MSG_S2C::stShopActivity_Update msg;
			msg.category = p->shop_item_category;
			msg.discount = p->discount;
			sWorld.SendGlobalMessage(msg);
			m_use_activitys[p->shop_item_category] = p;
		}
	}
}
ui32 ShopActivity::GetShopItemDiscount(ui8 itemcategory)
{
	ui32 r = 100;
	if (itemcategory < MAX_SHOP_CATEGORY && m_use_activitys[itemcategory] && m_use_activitys[itemcategory]->discount < r)
		r = m_use_activitys[itemcategory]->discount ;
	return r;
}