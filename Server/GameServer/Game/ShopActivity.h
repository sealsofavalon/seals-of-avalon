#ifndef  __SHOP_ACTIVITY_H__ 
#define  __SHOP_ACTIVITY_H__
#include "activity.h"
#include "../../../SDBase/Protocol/S2C_Auction.h"

#define  MAX_SHOP_CATEGORY 12
class  ShopActivity
{
public:
	ShopActivity();
	~ShopActivity();

	bool isIntime(activity_mgr::shop_event_config_t* p);
	activity_mgr::shop_event_config_t* FindShopActivity(ui32 actid);

	void AddShopActivity(activity_mgr::shop_event_config_t* p, bool fromdb = false);
	void RemoveShopActivity(activity_mgr::shop_event_config_t* p);
	void UpdateShopActivity(activity_mgr::shop_event_config_t*p);

	ui32 GetShopItemDiscount(ui8 itemcategory);
	void GetShopActivity(vector<MSG_S2C::stShopActivity>& v_a);
	void Update();
protected:
private:
	std::map<ui32, activity_mgr::shop_event_config_t*> m_vshop_activitys;
	activity_mgr::shop_event_config_t* m_use_activitys[MAX_SHOP_CATEGORY];
};

extern ShopActivity* g_shopactivity_system;
#endif