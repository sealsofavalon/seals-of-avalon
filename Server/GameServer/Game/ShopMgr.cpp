#include "StdAfx.h"
#include "ShopMgr.h"
#include "../../../SDBase/Protocol/C2S_Auction.h"
#include "ShopActivity.h"
#include "MultiLanguageStringMgr.h"
#include "QuestMgr.h"

initialiseSingleton( ShopMgr );

void ShopMgr::LoadShopItems()
{
	MyLog::log->notice("ShopMgr: Loading Shop Items...");

	vShopHotItems.clear();
	vShopItems.clear();
	QueryResult * res = WorldDatabase.Query("SELECT * FROM shopitems");
	if(res)
	{
		do 
		{
			MSG_S2C::stShopItem* item = new MSG_S2C::stShopItem;;
			item->id = res->Fetch()[0].GetUInt32();
			item->itementry = res->Fetch()[1].GetUInt32();

			ItemPrototype* itemproto = ItemPrototypeStorage.LookupEntry( item->itementry );
			if(!itemproto)
			{
				MyLog::log->alert("ShopMgr : itementry[%u] is not valid", item->itementry);
				continue;
			}
			item->itemcnt = res->Fetch()[2].GetUInt32();
			item->category = res->Fetch()[3].GetUInt32();
			item->subcategory = res->Fetch()[4].GetUInt32();
			for(int i = 0; i < 5; i++)
			{
				item->buytype[i] = res->Fetch()[i*2+5].GetInt32();
				item->buyprice[i] = res->Fetch()[i*2+6].GetUInt32();
			}
			vShopItems.insert(make_pair(item->id, item));
		} while (res->NextRow());

		MyLog::log->notice("ShopMgr: Done Load %u Items complete.", res->GetRowCount());
		delete res;
	}

	res = WorldDatabase.Query("SELECT * FROM shopitemhot");
	if(res)
	{
		do 
		{
			MSG_S2C::stShopHotItem* item = new MSG_S2C::stShopHotItem;
			item->id = res->Fetch()[0].GetUInt32();
			vShopHotItems.insert(make_pair(item->id, item));
		} while (res->NextRow());
		MyLog::log->notice("ShopMgr: Done Load %u HotItems complete.", res->GetRowCount());
		delete res;
	}
}

void ShopMgr::Update()
{
	if((++loopcount % 100))
		return;
}

ShopMgr::~ShopMgr()
{
	map<uint32, MSG_S2C::stShopItem*>::iterator itr = vShopItems.begin();
	for(; itr != vShopItems.end(); ++itr)
	{
		delete itr->second;
	}
	vShopItems.clear();

	map<uint32, MSG_S2C::stShopHotItem*>::iterator itr2 = vShopHotItems.begin();
	for(; itr2 != vShopHotItems.end(); ++itr2)
	{
		delete itr2->second;
	}
	vShopHotItems.clear();
}

ShopMgr::ShopMgr()
{
	loopcount = 0;
}

void ShopMgr::ProcItemBuy(Player* plr, uint32 id, uint8 buytype, uint8 cnt)
{
	map<uint32, MSG_S2C::stShopItem*>::iterator itr = vShopItems.find(id);
	if(itr == vShopItems.end())
	{
		MyLog::log->alert("ShopMgr : Not found item index[%d]", id);
		return;
	}

	MSG_S2C::stShopItem* item = itr->second;
	
	bool bFoundBuyType = false;uint8 buytype_index;
	for(int i = 0; i < 5; i++)
	{
		if(item->buytype[i] == buytype)
		{
			bFoundBuyType = true;
			buytype_index = i;
		}
	}
	if(!bFoundBuyType)
	{
		MyLog::log->alert("ShopMgr : Not Found Item BuyType[%u]", buytype);
		return;
	}

	//have enough money?
	int price = 0;
	cnt = item->itemcnt;
	// to be continued

	price = item->buyprice[buytype_index];
	if (g_shopactivity_system)
	{
		ui32 zhekou = g_shopactivity_system->GetShopItemDiscount(item->category);
		price = price * zhekou / 100;
		if (price == 0)
		{
			price = 1 ;
		}
	}

	if(plr->GetCardPoints() < price )
	{
		MyLog::log->alert("ShopMgr : player[%s] Have not enough gold", plr->GetName());
		return;
	}

	//have enough bag slot?
	// Find free slot and break if inv full
	SlotResult slotresult;
	AddItemResult result;
	Item* qstItem = NULL;

	Item *add = plr->GetItemInterface()->FindItemLessMax(item->itementry, cnt, false);
	ItemPrototype* itemproto = ItemPrototypeStorage.LookupEntry( item->itementry );
	if (!add)
	{
		slotresult = plr->GetItemInterface()->FindFreeInventorySlot(itemproto);
	}
	if ((!slotresult.Result) && (!add))
	{
		//Our User doesn't have a free Slot in there bag
		plr->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_INVENTORY_FULL);
		return;
	}

	qstItem = add;
	if(!add)
	{
		Item *itm = objmgr.CreateItem(item->itementry, plr);
		if(!itm)
		{
			plr->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_DONT_OWN_THAT_ITEM);
			return;
		}

		itm->m_isDirty=true;
		itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, cnt);
		itm->WashEnchantment();

		switch(item->buytype[buytype_index])
		{
		case 1:
			//1天的价格
			itm->SetUInt32Value(ITEM_FIELD_LIFE_STYLE, ITEM_LIFE_STYLE_TIME);
			itm->SetUInt64Value(ITEM_FIELD_LIFE_VALUE1, UNIXTIME+24*60*60);
			break;
		case 2:
			//1周的价格
			itm->SetUInt32Value(ITEM_FIELD_LIFE_STYLE, ITEM_LIFE_STYLE_TIME);
			itm->SetUInt64Value(ITEM_FIELD_LIFE_VALUE1, UNIXTIME+7*24*60*60);
			break;
		case 3:
			//1月的价格
			itm->SetUInt32Value(ITEM_FIELD_LIFE_STYLE, ITEM_LIFE_STYLE_TIME);
			itm->SetUInt64Value(ITEM_FIELD_LIFE_VALUE1, UNIXTIME+30*24*60*60);
			break;
		case 4:
			//无期限价格
			itm->SetUInt32Value(ITEM_FIELD_LIFE_STYLE, ITEM_LIFE_STYLE_NONE);
			break;
		default:
			break;
		}

		if(slotresult.ContainerSlot == ITEM_NO_SLOT_AVAILABLE)
		{
			result = plr->GetItemInterface()->SafeAddItem(itm, INVENTORY_SLOT_NOT_SET, slotresult.Slot);
			if(!result)
			{
				delete itm;
			}
			else
			{
				plr->GetSession()->SendItemPushResult(itm, false, true, false, true, INVENTORY_SLOT_NOT_SET, slotresult.Result, cnt);
				qstItem = itm;
				MyLog::yunyinglog->info("yuanbao-shop-player[%u][%s] spend yuanbao[%u] for item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), price, itm->GetGUID(), itm->GetProto()->ItemId, cnt);
				MyLog::yunyinglog->info("item-shop-player[%u][%s] spend yuanbao[%u] for item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), price, itm->GetGUID(), itm->GetProto()->ItemId, cnt);
			}
		}
		else 
		{
			if(Item *bag = plr->GetItemInterface()->GetInventoryItem(slotresult.ContainerSlot))
			{
				if( !((Container*)bag)->AddItem(slotresult.Slot, itm) )
					delete itm;
				else
				{
					qstItem = itm;
					plr->GetSession()->SendItemPushResult(itm, false, true, false, true, slotresult.ContainerSlot, slotresult.Result, cnt);
					MyLog::yunyinglog->info("yuanbao-shop-player[%u][%s] spend yuanbao[%u] for item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), price, itm->GetGUID(), itm->GetProto()->ItemId, cnt);
					MyLog::yunyinglog->info("item-shop-player[%u][%s] spend yuanbao[%u] for item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), price, itm->GetGUID(), itm->GetProto()->ItemId, cnt);
				}
			}
		}
	}
	else
	{
		add->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, cnt);
		add->m_isDirty = true;
		sQuestMgr.OnPlayerItemPickup( plr, add );
		plr->GetSession()->SendItemPushResult(add, false, true, false, false, plr->GetItemInterface()->GetBagSlotByGuid(add->GetGUID()), 1, cnt);
		MyLog::yunyinglog->info("yuanbao-shop-player[%u][%s] spend yuanbao[%u] for item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), price, add->GetGUID(), add->GetProto()->ItemId, cnt);
		MyLog::yunyinglog->info("item-shop-player[%u][%s] spend yuanbao[%u] for item["I64FMT"][%u] count[%u]", plr->GetLowGUID(), plr->GetName(), price, add->GetGUID(), add->GetProto()->ItemId, cnt);
	}

	char szDesc[128];
	sprintf(szDesc, "plr["I64FMT"][%s] spend points[%u] to buy a item[%u] index[%u] in shop", plr->GetGUID(), plr->GetName(), price, item->itementry, item->id );
	plr->SpendPoints(price, szDesc);

	sWorld.ExecuteSqlToDBServer( "insert into shop_history values(%u, %u, %u, %u, %u, %u)", plr->GetLowGUID(), item->itementry, item->itemcnt, item->buytype[buytype_index], item->buyprice[buytype_index], (uint32)UNIXTIME );
}

void ShopMgr::ProcItemGift(Player* plr, uint64 to_guid, uint32 id, uint8 buytype)
{
	map<uint32, MSG_S2C::stShopItem*>::iterator itr = vShopItems.find(id);
	if(itr == vShopItems.end())
	{
		MyLog::log->alert("ShopMgr : Not found item index[%d]", id);
		return;
	}

	MSG_S2C::stShopItem* item = itr->second;

	ItemPrototype* proto = ItemPrototypeStorage.LookupEntry( item->itementry );
	if( !proto )
	{
		MyLog::log->warn( "ShopMgr : no such item, entry:%u", item->itementry );
		return;
	}

	bool bFoundBuyType = false;uint8 buytype_index;
	for(int i = 0; i < 5; i++)
	{
		if(item->buytype[i] == buytype)
		{
			bFoundBuyType = true;
			buytype_index = i;
		}
	}
	if(!bFoundBuyType)
	{
		MyLog::log->alert("ShopMgr : Not Found Item BuyType[%u]", buytype);
		return;
	}

	int price = item->buyprice[buytype_index];
	if (g_shopactivity_system)
	{
		ui32 zhekou = g_shopactivity_system->GetShopItemDiscount(item->category);
		price = price * zhekou / 100;
		if (price == 0)
		{
			price = 1 ;
		}
		if (zhekou < 100 && zhekou > 0)
		{
			plr->GetSession()->SystemMessage( build_language_string( BuildString_ShopDiscountNotify, Item::BuildStringWithProto( proto ), item->itemcnt, item->buyprice[buytype_index], price ) );//"恭喜您享受商城打折活动。本次购买折扣为%.2f折,", (float(zhekou))/ 100.f);
		}
	}

	//have enough money?
	if(plr->GetCardPoints() < price )
	{
		MyLog::log->alert("ShopMgr : player[%s] Have not enough gold", plr->GetName());
		return;
	}
	

	//email
	Item *itm = objmgr.CreateItem(item->itementry, plr);
	if(!itm)
	{
		plr->GetItemInterface()->BuildInventoryChangeError(0, 0, INV_ERR_DONT_OWN_THAT_ITEM);
		return;
	}

	itm->m_isDirty=true;
	itm->SetUInt32Value(ITEM_FIELD_STACK_COUNT, item->itemcnt);

	switch(item->buytype[buytype_index])
	{
	case 1:
		//1天的价格
		itm->SetUInt32Value(ITEM_FIELD_LIFE_STYLE, ITEM_LIFE_STYLE_TIME);
		itm->SetUInt64Value(ITEM_FIELD_LIFE_VALUE1, UNIXTIME+24*60*60);
		break;
	case 2:
		//1周的价格
		itm->SetUInt32Value(ITEM_FIELD_LIFE_STYLE, ITEM_LIFE_STYLE_TIME);
		itm->SetUInt64Value(ITEM_FIELD_LIFE_VALUE1, UNIXTIME+7*24*60*60);
		break;
	case 3:
		//1月的价格
		itm->SetUInt32Value(ITEM_FIELD_LIFE_STYLE, ITEM_LIFE_STYLE_TIME);
		itm->SetUInt64Value(ITEM_FIELD_LIFE_VALUE1, UNIXTIME+30*24*60*60);
		break;
	case 4:
		//无期限价格
		itm->SetUInt32Value(ITEM_FIELD_LIFE_STYLE, ITEM_LIFE_STYLE_NONE);
		break;
	default:
		break;
	}

	sMailSystem.SendAutomatedMessage(SHOP, plr->GetGUID(), to_guid, "send you a gift", "", 0, 0, itm->GetGUID(), 62);
	delete itm;

	char szDesc[128];
	sprintf(szDesc, "plr["I64FMT"][%s] spend points[%u] to buy a item[%u] index[%u] in shop", plr->GetGUID(), plr->GetName(), price, item->itementry, item->id );
	plr->SpendPoints(price, szDesc);
}

void ShopMgr::SendShopItems(Player* plr)
{
	MSG_S2C::stShop_ItemList Msg;
	map<uint32, MSG_S2C::stShopItem*>::iterator itr = vShopItems.begin();
	for(; itr != vShopItems.end(); ++itr)
	{
		MSG_S2C::stShopItem* item = itr->second;
		Msg.vItems.push_back(*item);
	}

	map<uint32, MSG_S2C::stShopHotItem*>::iterator itr2 = vShopHotItems.begin();
	for(; itr2 != vShopHotItems.end(); ++itr2)
	{
		MSG_S2C::stShopHotItem* item = itr2->second;
		Msg.vHotItems.push_back(*item);
	}

	g_shopactivity_system->GetShopActivity(Msg.vShopActivity);
	plr->GetSession()->SendPacket(Msg);
}

void WorldSession::HandleShopBuy(CPacketUsn& packet)
{
	MSG_C2S::stShop_Buy MsgRecv;
	packet>>MsgRecv;
	if(MsgRecv.gift_to_name.size())
	{
		PlayerInfo* playerinfo = ObjectMgr::getSingleton().GetPlayerInfoByName(MsgRecv.gift_to_name.c_str());
		if(!playerinfo)
		{
			MyLog::log->debug("ShopMgr : gift to player[%s] not found!", MsgRecv.gift_to_name.c_str());
			return;
		}
		sShopMgr.ProcItemGift(_player, playerinfo->guid, MsgRecv.id, MsgRecv.buytype);
	}
	else
		sShopMgr.ProcItemBuy(_player, MsgRecv.id, MsgRecv.buytype, MsgRecv.cnt);
}

void WorldSession::handleShopCategory(CPacketUsn& packet)
{

}

void WorldSession::handleShopGetList(CPacketUsn& packet)
{
	sShopMgr.SendShopItems(_player);
}