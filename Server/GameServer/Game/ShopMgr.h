#ifndef __SHOPMGR_H
#define __SHOPMGR_H

#include "../../../SDBase/Protocol/S2C_Auction.h"

class ShopMgr : public Singleton <ShopMgr>
{
public:
	ShopMgr();

	~ShopMgr();

	void LoadShopItems();
	void LoadShopHotItems();
	void Update();

	void SendShopItems(Player* plr);
	void ProcItemBuy(Player* plr, uint32 id, uint8 buytype, uint8 cnt);
	void ProcItemGift(Player* plr, uint64 to_guid, uint32 id, uint8 buytype);
private:
	map<uint32, MSG_S2C::stShopItem*> vShopItems;
	map<uint32, MSG_S2C::stShopHotItem*> vShopHotItems;
	uint32 loopcount;
};

#define sShopMgr ShopMgr::getSingleton()

#endif
