#ifndef SKILL_NAME_MGR_H
#define SKILL_NAME_MGR_H

class SkillNameMgr
{
public:
	char **SkillNames;
	uint32 maxskill;

	SkillNameMgr()
	{
		StorageContainerIterator<skilllineentry> * itr = dbcSkillLine.MakeIterator();
		skilllineentry* it = NULL;
		while(!itr->AtEnd())
		{
			it = itr->Get();
			SkillNames = new char * [dbcSkillLine.Size()+1];
			memset(SkillNames,0,(dbcSkillLine.Size()+1) * sizeof(char *));
			unsigned int SkillID = it->id;
			const char *SkillName = it->Name;

			SkillNames[SkillID] = new char [strlen(SkillName)+1];
			//When the DBCFile gets cleaned up, so does the record data, so make a copy of it..
			memcpy(SkillNames[SkillID],SkillName,strlen(SkillName)+1);

			if(!itr->Inc())
				break;
		}
		itr->Destruct();
	}
	~SkillNameMgr()
	{
		for(uint32 i = 0;i<=maxskill;i++)
		{
			if(SkillNames[i] != 0)
				delete SkillNames[i];
		}
		delete SkillNames;
	}
};
#endif

