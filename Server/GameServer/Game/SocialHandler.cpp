#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Guild.h"
#include "../../SDBase/Protocol/S2C_Guild.h"
#include "../../Common/Protocol/GS2MS.h"

#include "../Net/MSSocket.h"
void WorldSession::HandleFriendListOpcode( CPacketUsn& packet )
{
	MyLog::log->debug( "WORLD: Received CMSG_FRIEND_LIST"  );
// 	packet.SetProNo(MSG_GS2MS::PROXY_MSG);
// 	packet.SetSubproto(MSG_C2S::FRIEND_LIST);
// 	sMSSocket.PostSend(packet.m_buff, packet.m_size);
	MSG_C2S::stFriend_List MsgRecv;packet>>MsgRecv;
	uint32 flag = MsgRecv.flag;
	_player->Social_SendFriendList( flag );
}

void WorldSession::HandleAddFriendOpcode( CPacketUsn& packet )
{
	MyLog::log->debug( "WORLD: Received CMSG_ADD_FRIEND"  );
// 	packet.SetProNo(MSG_GS2MS::PROXY_MSG);
// 	packet.SetSubproto(MSG_C2S::FRIEND_ADD);
// 	sMSSocket.PostSend(packet.m_buff, packet.m_size);

 	MSG_C2S::stFriend_Add MsgRecv;packet>>MsgRecv;
 	string name, note;
 	name = MsgRecv.name;
 	note = MsgRecv.note;
 
 	_player->Social_AddFriend( name.c_str(), note.size() ? note.c_str() : NULL );
}

void WorldSession::HandleDelFriendOpcode( CPacketUsn& packet )
{
	MyLog::log->debug( "WORLD: Received CMSG_DEL_FRIEND"  );
// 	packet.SetProNo(MSG_GS2MS::PROXY_MSG);
// 	packet.SetSubproto(MSG_C2S::FRIEND_DEL);
// 	sMSSocket.PostSend(packet.m_buff, packet.m_size);
	MSG_C2S::stFriend_Del MsgRecv;packet>>MsgRecv;
	uint64 FriendGuid = MsgRecv.FriendGuid;
	_player->Social_RemoveFriend( (uint32)FriendGuid );
}

void WorldSession::HandleAddIgnoreOpcode( CPacketUsn& packet )
{
	MyLog::log->debug( "WORLD: Received CMSG_ADD_IGNORE" );
// 	packet.SetProNo(MSG_GS2MS::PROXY_MSG);
// 	packet.SetSubproto(MSG_C2S::FRIEND_ADD_IGNORE);
// 	sMSSocket.PostSend(packet.m_buff, packet.m_size);

	MSG_C2S::stFriend_Add_Ignore MsgRecv;packet>>MsgRecv;
	std::string ignoreName = "UNKNOWN";
	ignoreName = MsgRecv.ignore_name;
	_player->Social_AddIgnore( ignoreName.c_str() );
}

void WorldSession::HandleDelIgnoreOpcode( CPacketUsn& packet )
{
	MyLog::log->debug( "WORLD: Received CMSG_DEL_IGNORE" );
// 	packet.SetProNo(MSG_GS2MS::PROXY_MSG);
// 	packet.SetSubproto(MSG_C2S::FRIEND_DEL_IGNORE);
// 	sMSSocket.PostSend(packet.m_buff, packet.m_size);

	MSG_C2S::stFriend_Del_Ignore MsgRecv;packet>>MsgRecv;
	uint64 IgnoreGuid = MsgRecv.IgnoreGuid;
	_player->Social_RemoveIgnore( (uint32)IgnoreGuid );
}

void WorldSession::HandleSetFriendNote(CPacketUsn& packet)
{
// 	packet.SetProNo(MSG_GS2MS::PROXY_MSG);
// 	packet.SetSubproto(MSG_C2S::FRIEND_SET_NOTE);
// 	sMSSocket.PostSend(packet.m_buff, packet.m_size);

	MSG_C2S::stFriend_Set_Note MsgRecv;packet>>MsgRecv;
	uint64 guid = MsgRecv.guid;
	string note = MsgRecv.note;
	_player->Social_SetNote( (uint32)guid, note.size() ? note.c_str() : NULL );
}
