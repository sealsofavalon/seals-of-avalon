#include "StdAfx.h"
#define SPELL_CHANNEL_UPDATE_INTERVAL 1000
#include "../../SDBase/Protocol/S2C_Spell.h"
#include "SunyouInstance.h"
#include "SunyouRaid.h"
#include "DynamicObject.h"
#include "QuestMgr.h"

/// externals for spell system
extern pSpellEffect SpellEffectsHandler[TOTAL_SPELL_EFFECTS];
extern pSpellTarget SpellTargetHandler[TOTAL_SPELL_TARGET];
uint16 Spell::m_attackcnt = 1;

enum SpellTargetSpecification
{
    TARGET_SPECT_NONE       = 0,
    TARGET_SPEC_INVISIBLE   = 1,
    TARGET_SPEC_DEAD        = 2,
};

Spell::Spell(Object* Caster, SpellEntry *info, bool triggered, Aura* aur) : m_external( false ), EffectTargetGUID( 0 )
{
	ASSERT( Caster != NULL && info != NULL );

	damage = 0;
	m_spellInfo = info;
	/*
	if( m_spellInfo->School > 1 )
	{
		MyLog::log->error("assert m_spellInfo->School[%d] > 1", m_spellInfo->School);
		//assert( 0 && "error spell school type" );
		m_spellInfo->School = 1;
		return;
	}
	*/
	m_caster = Caster;
	duelSpell = false;

	switch( Caster->GetTypeId() )
	{
		case TYPEID_PLAYER:
        {
		    g_caster = NULL;
		    i_caster = NULL;
		    u_caster = static_cast< Unit* >( Caster );
		    p_caster = static_cast< Player* >( Caster );
			if( p_caster->GetDuelState() == DUEL_STATE_STARTED )
			    duelSpell = true;
        }break;

		case TYPEID_UNIT:
        {
		    g_caster = NULL;
		    i_caster = NULL;
		    p_caster = NULL;
		    u_caster = static_cast< Unit* >( Caster );
		    if( u_caster->IsPet() && static_cast< Pet* >( u_caster)->GetPetOwner() != NULL && static_cast< Pet* >( u_caster )->GetPetOwner()->GetDuelState() == DUEL_STATE_STARTED )
			    duelSpell = true;
        }break;

		case TYPEID_ITEM:
		case TYPEID_CONTAINER:
        {
		    g_caster = NULL;
		    u_caster = NULL;
		    p_caster = NULL;
		    i_caster = static_cast< Item* >( Caster );
			if( i_caster->GetOwner() && i_caster->GetOwner()->GetDuelState() == DUEL_STATE_STARTED )
				duelSpell = true;
        }break;

		case TYPEID_GAMEOBJECT:
        {
		    u_caster = NULL;
		    p_caster = NULL;
		    i_caster = NULL;
		    g_caster = static_cast< GameObject* >( Caster );
        }break;
        default:
            MyLog::log->debug("[DEBUG][SPELL] Incompatible object type, please report this to the dev's");
        break;
	}

	m_spellState = SPELL_STATE_NULL;

	m_castPositionX = m_castPositionY = m_castPositionZ = 0;
	//TriggerSpellId = 0;
	//TriggerSpellTarget = 0;
	m_triggeredSpell = triggered;
	m_AreaAura = false;

	m_triggeredByAura = aur;

	damageToHit = 0;
	castedItemId = 0;

	m_usesMana = false;
	m_Spell_Failed = false;
	m_CanRelect = false;
	m_IsReflected = false;
	hadEffect = false;
	bDurSet = false;
	bRadSet[0] = false;
	bRadSet[1] = false;
	bRadSet[2] = false;

	cancastresult = SPELL_CANCAST_OK;

	m_requiresCP = false;
	unitTarget = NULL;
	ModeratedTargets.clear();
	itemTarget = NULL;
	gameObjTarget = NULL;
	playerTarget = NULL;
	corpseTarget = NULL;
	judgement = false;
	add_damage = 0;
	m_Delayed = false;
	m_nSpellDelayed = 0;
	pSpellId = 0;
	m_cancelled = false;
	ProcedOnSpell = 0;
	forced_basepoints[0] = forced_basepoints[1] = forced_basepoints[2] = 0;
	extra_cast_number = 0;
	m_reflectedParent = NULL;
	m_isCasting = false;

	m_effects_hide[0] = false;
	m_effects_hide[1] = false;
	m_effects_hide[2] = false;

	m_curattackcnt = Spell::GetDamageSeq();
}

Spell::~Spell()
{
	if( m_external )
		return;

	if( u_caster != NULL && u_caster->GetCurrentSpell() == this )
		u_caster->SetCurrentSpell(NULL);

	if( i_caster && ( cancastresult == SPELL_CANCAST_OK && !GetSpellFailed() ) )
	{
		if(i_caster->GetProto()->Bonding == ITEM_BIND_ON_USE)
			i_caster->SoulBind();
	}

	if( i_caster && i_caster->GetProto()->Class != ITEM_CLASS_USE )
	{
		uint32 nCnt = i_caster->GetUInt32Value(ITEM_FIELD_USE_CNT);
		if(nCnt == 1)
		{
			if(hadEffect || ( cancastresult == SPELL_CANCAST_OK && !GetSpellFailed() ) )
			{
				RemoveItems();
			}
		}
		else
		{
			if( nCnt == 0 && cancastresult == SPELL_CANCAST_OK )
			{
				RemoveItems();
			}
			else if( hadEffect || ( cancastresult == SPELL_CANCAST_OK && !GetSpellFailed() ) )
			{
				--nCnt;
				i_caster->SetUInt32Value( ITEM_FIELD_USE_CNT, nCnt );
			}
		}
	}
}

//i might forget conditions here. Feel free to add them
bool Spell::IsStealthSpell()
{
	//check if aura name is some stealth aura
	if( m_spellInfo->EffectApplyAuraName[0] == 16 ||
		m_spellInfo->EffectApplyAuraName[1] == 16 ||
		m_spellInfo->EffectApplyAuraName[2] == 16 )
		return true;
	return false;
}

//i might forget conditions here. Feel free to add them
bool Spell::IsInvisibilitySpell()
{
	//check if aura name is some invisibility aura
	if( m_spellInfo->EffectApplyAuraName[0] == 18 ||
		m_spellInfo->EffectApplyAuraName[1] == 18 ||
		m_spellInfo->EffectApplyAuraName[2] == 18 )
		return true;
	return false;
}

void Spell::FillSpecifiedTargetsInArea( float srcx, float srcy, float srcz, uint32 ind, uint32 specification )
{
    FillSpecifiedTargetsInArea( ind, srcx, srcy, srcz, GetRadius(ind), specification );
}

// for the moment we do invisible targets
void Spell::FillSpecifiedTargetsInArea(uint32 i,float srcx,float srcy,float srcz, float range, uint32 specification)
{
	TargetsList *tmpMap=&m_targetUnits[i];
    //IsStealth()
    float r = range * range;
	uint8 did_hit_result;
    for(std::set<Object*>::iterator itr = m_caster->GetInRangeSetBegin(); itr != m_caster->GetInRangeSetEnd(); itr++ )
    {
		Object* obj = *itr;
        // don't add objects that are not units and that are dead
        if( !( obj->IsUnit() ) || ! static_cast< Unit* >( obj )->isAlive())
            continue;

        //static_cast< Unit* >( *itr )->IsStealth()
        if( m_spellInfo->TargetCreatureType)
        {
            if(obj->GetTypeId()!= TYPEID_UNIT)
                continue;
            CreatureInfo *inf = ((Creature*)obj)->GetCreatureName();
            if(!inf || !(1<<(inf->Type-1) & m_spellInfo->TargetCreatureType))
                continue;
        }

        if(IsInrange(srcx,srcy,srcz,obj,r))
        {
            if( u_caster != NULL )
            {
                if( isAttackable( u_caster, static_cast< Unit* >( obj ),!(m_spellInfo->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED)))
                {
					did_hit_result = DidHit(i, static_cast< Unit* >( obj ) );
					if( did_hit_result != SPELL_DID_HIT_SUCCESS )
					{
						SafeAddModeratedTarget(obj->GetGUID(), did_hit_result);						
					}
					else if (did_hit_result != SPELL_DID_HIT_MAX)
					{
						SafeAddTarget(tmpMap, obj->GetGUID());
					}
                }
            }
            else //cast from GO
            {
				if( g_caster)
				{
					if(g_caster && g_caster->GetUInt32Value(OBJECT_FIELD_CREATED_BY) && g_caster->m_summoner)
					{
						//trap, check not to attack owner and friendly
						if(isAttackable(g_caster->m_summoner,(Unit*)obj,!(m_spellInfo->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED)))
						{
							SafeAddTarget(tmpMap, obj->GetGUID());
						}
					}
					else if( isAttackable( g_caster, obj ))
					{
						SafeAddTarget(tmpMap, obj->GetGUID());
					}
				}
				else
				{
					SafeAddTarget(tmpMap, obj->GetGUID());
				}
            }
            if( m_spellInfo->MaxTargets)
            {
                if( m_spellInfo->MaxTargets >= tmpMap->size())
                    return;
            }
        }
    }
}
void Spell::FillAllTargetsInArea(LocationVector & location,uint32 ind)
{
    FillAllTargetsInArea(ind,location.x,location.y,location.z,GetRadius(ind));
}

void Spell::FillAllTargetsInArea(float srcx,float srcy,float srcz,uint32 ind)
{
	FillAllTargetsInArea(ind,srcx,srcy,srcz,GetRadius(ind));
}

/// We fill all the targets in the area, including the stealth ed one's
void Spell::FillAllTargetsInArea(uint32 i,float srcx,float srcy,float srcz, float range)
{
	TargetsList *tmpMap=&m_targetUnits[i];
	float r = range*range;
	uint8 did_hit_result;
	for( std::set<Object*>::iterator itr = m_caster->GetInRangeSetBegin(); itr != m_caster->GetInRangeSetEnd(); itr++ )
	{
		if( !( (*itr)->IsUnit() ) || ! static_cast< Unit* >( *itr )->isAlive() || ( static_cast< Creature* >( *itr )->IsTotem() && !static_cast< Unit* >( *itr )->IsPlayer() ) )
			continue;

		Unit* u = static_cast<Unit*>( *itr );
		if( m_spellInfo->TargetCreatureType )
		{
			if( (*itr)->GetTypeId()!= TYPEID_UNIT )
				continue;
			CreatureInfo *inf = ((Creature*)u)->GetCreatureName();
			if( !inf || !( 1 << (inf->Type-1) & m_spellInfo->TargetCreatureType ) )
				continue;
		}
		if( IsInrange( srcx, srcy, srcz, (*itr), r ) )
		{
#ifdef COLLISION
			if( !(m_spellInfo->field114 & SPELL_AVOID_MASK_COLLISION) )
				if( !CollideInterface.CheckLOS( u->GetMapIDForCollision(), LocationVector( srcx, srcy, srcz ), u->GetPositionNC() ) )
					continue;
#endif
			if( u_caster != NULL )
			{
				if( isAttackable( u_caster, u, !(m_spellInfo->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED) ) )
				{
					did_hit_result = DidHit(i, u );//
					//did_hit_result = SPELL_DID_HIT_SUCCESS ;
					if( did_hit_result == SPELL_DID_HIT_SUCCESS )
					{
						SafeAddTarget(tmpMap, u->GetGUID());
					}
					else if(did_hit_result != SPELL_DID_HIT_MAX)
					{

						SafeAddModeratedTarget(u->GetGUID(), did_hit_result);
					}
						
				}
			}
			else //cast from GO
			{
				if( g_caster )
				{
					if( g_caster->GetUInt32Value( OBJECT_FIELD_CREATED_BY ) && g_caster->m_summoner != NULL )
					{
						//trap, check not to attack owner and friendly
						if( isAttackable( g_caster->m_summoner, u, !(m_spellInfo->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED) ) )
						{
							SafeAddTarget(tmpMap, u->GetGUID());
						}
					}
					else if( isAttackable( g_caster, u ))
					{
						SafeAddTarget(tmpMap, u->GetGUID());
					}
				}
				else
				{
					SafeAddTarget(tmpMap, u->GetGUID());
				}
			}
			if( m_spellInfo->MaxTargets )
				if( m_spellInfo->MaxTargets == tmpMap->size() )
					return;
		}
	}
}

// We fill all the targets in the area, including the stealth ed one's
void Spell::FillAllFriendlyInArea( uint32 i, float srcx, float srcy, float srcz, float range )
{
	// added by Gui. for HappyInstance
	bool PlayerOnly = false;
	if( m_spellInfo->Id == 68 || m_spellInfo->Id == 69 )
	{
		PlayerOnly = true;
	}

	TargetsList *tmpMap=&m_targetUnits[i];
	float r = range * range;
	uint8 did_hit_result;
	Unit* pUnit = NULL;
	for( std::set<Object*>::iterator itr = m_caster->GetInRangeSetBegin(); itr != m_caster->GetInRangeSetEnd(); itr++ )
	{
		if( !((*itr)->IsUnit()) || !static_cast< Unit* >( *itr )->isAlive() )
			continue;
		pUnit = (Unit*)(*itr);

		if( PlayerOnly && pUnit->IsCreature() )
			continue;

		if( m_spellInfo->TargetCreatureType )
		{
			if(pUnit->GetTypeId()!= TYPEID_UNIT)
				continue;
			CreatureInfo *inf = ((Creature*)pUnit)->GetCreatureName();
			if(!inf || !(1<<(inf->Type-1) & m_spellInfo->TargetCreatureType))
				continue;
		}

		if( IsInrange( srcx, srcy, srcz, pUnit, r ) )
		{
			if( u_caster != NULL )
			{
				if( isFriendly( u_caster, static_cast< Unit* >( pUnit ) ) )
				{
					did_hit_result = DidHit(i, static_cast< Unit* >( pUnit ) );
					if( did_hit_result == SPELL_DID_HIT_SUCCESS)
					{
						SafeAddTarget(tmpMap, pUnit->GetGUID());
					}
					else if(did_hit_result != SPELL_DID_HIT_MAX)
					{
						SafeAddModeratedTarget(pUnit->GetGUID(),  did_hit_result);
						//ModeratedTargets.push_back( SpellTargetMod( (*itr)->GetGUID(), did_hit_result ) );
					}
						
				}
			}
			else //cast from GO
			{
				if( g_caster )
				{
					if( g_caster->GetUInt32Value( OBJECT_FIELD_CREATED_BY ) && g_caster->m_summoner != NULL )
					{
						//trap, check not to attack owner and friendly
						if( isFriendly( g_caster->m_summoner, pUnit ))
						{
							SafeAddTarget(tmpMap, pUnit->GetGUID());
						}
					}
					else if( isFriendly( g_caster, pUnit ) )
					{
						SafeAddTarget(tmpMap, pUnit->GetGUID());
					}
				}
				else
				{
					SafeAddTarget(tmpMap, pUnit->GetGUID());
				}
			}
			if( m_spellInfo->MaxTargets )
				if( m_spellInfo->MaxTargets == tmpMap->size() )
					return;
		}
	}
}

uint64 Spell::GetSinglePossibleEnemy(uint32 i,float prange)
{
	float r;
	if(prange)
		r = prange;
	else
	{
		r = m_spellInfo->base_range_or_radius_sqr;
		if( m_spellInfo->SpellGroupType && u_caster)
		{
			SM_FFValue(u_caster->SM_FRadius,&r,m_spellInfo->SpellGroupType);
			SM_PFValue(u_caster->SM_PRadius,&r,m_spellInfo->SpellGroupType);
		}
	}
	float srcx = m_caster->GetPositionX(), srcy = m_caster->GetPositionY(), srcz = m_caster->GetPositionZ();
	for( std::set<Object*>::iterator itr = m_caster->GetInRangeSetBegin(); itr != m_caster->GetInRangeSetEnd(); itr++ )
	{
		if( !( (*itr)->IsUnit() ) || !static_cast< Unit* >( *itr )->isAlive() )
			continue;

		if( m_spellInfo->TargetCreatureType )
		{
			if( (*itr)->GetTypeId() != TYPEID_UNIT )
				continue;
			CreatureInfo *inf = ((Creature*)(*itr))->GetCreatureName();
			if(!inf || !(1<<(inf->Type-1) & m_spellInfo->TargetCreatureType))
				continue;
		}
		if(IsInrange(srcx,srcy,srcz,(*itr),r))
		{
			if( u_caster != NULL )
			{
				if(isAttackable(u_caster, static_cast< Unit* >( *itr ),!(m_spellInfo->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED)) && DidHit(i,((Unit*)*itr))==SPELL_DID_HIT_SUCCESS)
					return (*itr)->GetGUID();
			}
			else //cast from GO
			{
				if(g_caster && g_caster->GetUInt32Value(OBJECT_FIELD_CREATED_BY) && g_caster->m_summoner)
				{
					//trap, check not to attack owner and friendly
					if( isAttackable( g_caster->m_summoner, static_cast< Unit* >( *itr ),!(m_spellInfo->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED)))
						return (*itr)->GetGUID();
				}
			}
		}
	}
	return 0;
}

uint64 Spell::GetSinglePossibleFriend(uint32 i,float prange)
{
	float r;
	if(prange)
		r = prange;
	else
	{
		r = m_spellInfo->base_range_or_radius_sqr;
		if( m_spellInfo->SpellGroupType && u_caster)
		{
			SM_FFValue(u_caster->SM_FRadius,&r,m_spellInfo->SpellGroupType);
			SM_PFValue(u_caster->SM_PRadius,&r,m_spellInfo->SpellGroupType);
		}
	}
	float srcx=m_caster->GetPositionX(),srcy=m_caster->GetPositionY(),srcz=m_caster->GetPositionZ();
	for(std::set<Object*>::iterator itr = m_caster->GetInRangeSetBegin(); itr != m_caster->GetInRangeSetEnd(); itr++ )
	{
		if( !( (*itr)->IsUnit() ) || !static_cast< Unit* >( *itr )->isAlive() )
			continue;
		if( m_spellInfo->TargetCreatureType )
		{
			if((*itr)->GetTypeId()!= TYPEID_UNIT)
				continue;
			CreatureInfo *inf = ((Creature*)(*itr))->GetCreatureName();
				if(!inf || !(1<<(inf->Type-1) & m_spellInfo->TargetCreatureType))
					continue;
		}
		if(IsInrange(srcx,srcy,srcz,(*itr),r))
		{
			if( u_caster != NULL )
			{
				if( isFriendly( u_caster, static_cast< Unit* >( *itr ) ) && DidHit(i, ((Unit*)*itr))==SPELL_DID_HIT_SUCCESS)
					return (*itr)->GetGUID();
			}
			else //cast from GO
			{
				if(g_caster && g_caster->GetUInt32Value(OBJECT_FIELD_CREATED_BY) && g_caster->m_summoner)
				{
					//trap, check not to attack owner and friendly
					if( isFriendly( g_caster->m_summoner, static_cast< Unit* >( *itr ) ) )
						return (*itr)->GetGUID();
				}
			}
		}
	}
	return 0;
}

uint8 Spell::DidHit(uint32 effindex,Unit* target)
{
	//note resistchance is vise versa, is full hit chance
	Unit* u_victim = target;

	if( this->m_spellInfo->Attributes & ATTRIBUTES_IGNORE_INVULNERABILITY )
		return SPELL_DID_HIT_SUCCESS;

	if (m_SetModeratedTargets.count(target->GetGUID()))
	{
		return SPELL_DID_HIT_MAX;
	}
	

	//if( u_victim->IsIceBlocking() )
	//	return SPELL_DID_HIT_IMMUNE;

	Player* p_victim = ( target->GetTypeId() == TYPEID_PLAYER ) ? static_cast< Player* >( target ) : NULL;

	//
	float baseresist[3] = { 4.0f, 5.0f, 6.0f };
	int32 lvldiff;
	float resistchance ;
	if( u_victim == NULL )
		return SPELL_DID_HIT_MISS;

	if (u_victim->IsPet())
	{
		return SPELL_DID_HIT_MISS;
	}

	/************************************************************************/
	/* Elite mobs always hit                                                */
	/************************************************************************/
	if(u_caster && u_caster->GetTypeId()==TYPEID_UNIT && ((Creature*)u_caster)->GetCreatureName() && ((Creature*)u_caster)->GetCreatureName()->Rank >= 3)
		return SPELL_DID_HIT_SUCCESS;

	/************************************************************************/
	/* Can't resist non-unit                                                */
	/************************************************************************/
	if(!u_caster)
		return SPELL_DID_HIT_SUCCESS;

	/************************************************************************/
	/* Can't reduce your own spells                                         */
	/************************************************************************/
	if(u_caster == u_victim)
		return SPELL_DID_HIT_SUCCESS;

	/************************************************************************/
	/* Check if the unit is evading                                         */
	/************************************************************************/
	if(u_victim->GetTypeId()==TYPEID_UNIT && u_victim->GetAIInterface()->getAIState()==STATE_EVADE)
		return SPELL_DID_HIT_EVADE;

	/************************************************************************/
	/* Check if the target is immune to this spell school                   */
	/************************************************************************/
	if (!IsHelpfulSpell(m_spellInfo))
	{
		if(u_victim->SchoolImmunityList[m_spellInfo->School])
			return SPELL_DID_HIT_IMMUNE;

	}

	/*************************************************************************/
	/* Check if the target is immune to this mechanic                        */
	/*************************************************************************/


	// modified by gui
//	if( RandomFloat( 100.f ) < 5.f )
//		return SPELL_DID_HIT_MISS;

	if(m_spellInfo->EffectApplyAuraName[effindex] && u_victim->MechanicsDispels[m_spellInfo->MechanicsType])
	{


		//return SPELL_DID_HIT_IMMUNE;
	
		if(u_victim->MechanicsDispelsCnt[m_spellInfo->MechanicsType] != -1 && u_victim->MechanicsDispelsCnt[m_spellInfo->MechanicsType] > 0)
		{
			u_victim->MechanicsDispelsCnt[m_spellInfo->MechanicsType]--;
			if(!u_victim->MechanicsDispelsCnt[m_spellInfo->MechanicsType] && u_victim->MechanicsDispelsAura[m_spellInfo->MechanicsType])
			{
				u_victim->RemoveAura(u_victim->MechanicsDispelsAura[m_spellInfo->MechanicsType]);
			}
			return SPELL_DID_HIT_MISS; // Moved here from Spell::CanCast
		}

	
		

		// Immune - IF, and ONLY IF, there is no damage component!
 		bool no_damage_component = true;
 		for( int x = 0 ; x <= 2 ; x ++ )
 		{
 			if( m_spellInfo->Effect[x] == SPELL_EFFECT_SCHOOL_DAMAGE
 				|| m_spellInfo->Effect[x] == SPELL_EFFECT_WEAPON_PERCENT_DAMAGE
 				|| m_spellInfo->Effect[x] == SPELL_EFFECT_WEAPON_DAMAGE
 				|| m_spellInfo->Effect[x] == SPELL_EFFECT_WEAPON_DAMAGE_NOSCHOOL
 				|| m_spellInfo->Effect[x] == SPELL_EFFECT_DUMMY
				|| m_spellInfo->Effect[x] == SPELL_EFFECT_CHAIN_DAMAGE_WITH_CHANGE_VALUE 
 				|| ( m_spellInfo->Effect[x] == SPELL_EFFECT_APPLY_AURA &&
 					( m_spellInfo->Effect[x] == SPELL_AURA_PERIODIC_DAMAGE
 					) )
 				)
 			{
 				no_damage_component = false;
 				break;
 			}
 		}
 		if( no_damage_component )
		{
			return SPELL_DID_HIT_IMMUNE;
		}
	}

	//return SPELL_DID_HIT_SUCCESS;


	/************************************************************************/
	/* Check if the target has a % resistance to this mechanic              */
	/************************************************************************/
		/* Never mind, it's already done below. Lucky I didn't go through with this, or players would get double resistance. */

	/************************************************************************/
	/* Check if the spell is a melee attack and if it was missed/parried    */
	/************************************************************************/
	uint32 melee_test_result;
	if( /*m_spellInfo->is_melee_spell || m_spellInfo->is_ranged_spell*/m_spellInfo->Spell_Dmg_Type == SPELL_DMG_TYPE_RANGED || m_spellInfo->Spell_Dmg_Type == SPELL_DMG_TYPE_MELEE )
	{
		uint32 _type;
		if( GetType() == SPELL_DMG_TYPE_RANGED )
			_type = RANGED;
		else
		{
			if (m_spellInfo->Flags4 & 0x1000000)
				_type =  OFFHAND;
			else
				_type = MELEE;
		}

		//return SPELL_DID_HIT_SUCCESS;

		melee_test_result = u_caster->GetSpellDidHitResult( u_victim, _type, m_spellInfo );
		//if(melee_test_result != SPELL_DID_HIT_SUCCESS)
		//	return (uint8)melee_test_result;

		return melee_test_result;
	}

	/************************************************************************/
	/* Check if the spell is resisted.                                      */
	/************************************************************************/
		int nLevelDif = 0;
	bool pvp =(p_caster && p_victim);

	if (u_caster && u_victim)
	{
		float hit = 0.f;
		nLevelDif = int(u_caster->getLevel()) - int(u_victim->getLevel());
		if (nLevelDif + 2 >= 0)
		{
			hit = (0.95f + 0.0125f * nLevelDif);
			if (hit > 1.f)
				hit = 1.f;
		}
		else
		{
			if(pvp)
			{
				hit = 0.95f + 0.07f * (nLevelDif);
			}
			else
			{
				hit = 0.95f + 0.11f * (nLevelDif);
			}
		}
		if (hit < 0.5f)
			hit = 0.5f;
		if (u_caster->IsCreature())
		{
			Creature* pCreature = (Creature*) u_caster;
			if (pCreature->proto&&pCreature->GetCreatureName()->Rank != ELITE_NORMAL)
			{
				if (hit < 0.8)
				{
					hit = 0.8;
				}
			}
			else
			{
				if (hit < 0.7)
				{
					hit = 0.7;
				}
			}			
		}


		resistchance = 1.f - hit;
		resistchance*= 100;
	}

	if( m_spellInfo->MechanicsType < MECHANIC_MAX )
	{
		if(p_victim)
			resistchance += p_victim->MechanicsResistancesPCT[m_spellInfo->MechanicsType];
		else
			resistchance += u_victim->MechanicsResistancesPCT[m_spellInfo->MechanicsType];
	}


	//rating bonus
	if( p_caster != NULL )
	{
		float RC= 13.f + p_caster->getLevel() * 0.1f;
		float PhyRC = 10.f + p_caster->getLevel() * 0.1f;
		float hit = 0.f;
		switch (m_spellInfo->Spell_Dmg_Type)
		{
		case SPELL_DMG_TYPE_MELEE:
			{
				hit = p_caster->GetHitFromMeleeSpell() + p_caster->CalcRating(PLAYER_RATING_MODIFIER_MELEE_HIT);
			}
			break;
		case SPELL_DMG_TYPE_RANGED:
			{
				hit = p_caster->GetHitFromMeleeSpell() + p_caster->CalcRating(PLAYER_RATING_MODIFIER_RANGED_HIT);
			}
			break;
		case SPELL_DMG_TYPE_MAGIC:
			{
				hit =p_caster->GetHitFromSpell() + p_caster->CalcRating( PLAYER_RATING_MODIFIER_SPELL_HIT );
			}
			break;
		}
		resistchance -= hit;

	}

	if(p_victim)
		resistchance += p_victim->m_resist_hit[2];

	if(m_spellInfo->SpellGroupType && u_caster)
	{
		float hitchance=0;
		SM_FFValue(u_caster->SM_FHitchance,&hitchance,m_spellInfo->SpellGroupType);
		resistchance -= hitchance;
	}

	if (m_spellInfo->Attributes & ATTRIBUTES_IGNORE_INVULNERABILITY)
		resistchance = 0.f;

	if( m_spellInfo->field114 & SPELL_AVOID_MASK_RESIST )
		resistchance = 0.f;

	if(resistchance >= 100.0f)
		return SPELL_DID_HIT_RESIST;
	else
	{
		uint32 res;
		if( resistchance< 0.01f )//resist chance >=1
			res =  SPELL_DID_HIT_SUCCESS;
		else
			res =  (Rand(resistchance) ? SPELL_DID_HIT_RESIST : SPELL_DID_HIT_SUCCESS);

		if (res == SPELL_DID_HIT_SUCCESS) // proc handling. mb should be moved outside this function
			target->HandleProc(PROC_ON_SPELL_LAND_VICTIM,u_caster,m_spellInfo);

		return res;
	}

}

//generate possible target list for a spell. Use as last resort since it is not acurate
//this function makes a rough estimation for possible target !
//!!!disabled parts that were not tested !!
void Spell::GenerateTargets(SpellCastTargets *store_buff)
{
	float r = m_spellInfo->base_range_or_radius_sqr;
	if( m_spellInfo->SpellGroupType && u_caster)
	{
		SM_FFValue(u_caster->SM_FRadius,&r,m_spellInfo->SpellGroupType);
		SM_PFValue(u_caster->SM_PRadius,&r,m_spellInfo->SpellGroupType);
	}
	uint32 cur;
	for(uint32 i=0;i<3;i++)
		for(uint32 j=0;j<2;j++)
		{
			if(j==0)
				cur = m_spellInfo->EffectImplicitTargetA[i];
			else // if(j==1)
				cur = m_spellInfo->EffectImplicitTargetB[i];

			uint32 uRadius = m_spellInfo->EffectRadiusIndex[i];
			


			switch(cur)
			{
				case EFF_TARGET_NONE:{
					//this is bad for us :(
					}break;
				case EFF_TARGET_SELF:{
						if(m_caster->IsUnit())
							store_buff->m_unitTarget = m_caster->GetGUID();
					}break;
					// need more research
				case EFF_TARGET_FRIENDLY:{ // dono related to "Wandering Plague", "Spirit Steal", "Contagion of Rot", "Retching Plague" and "Copy of Wandering Plague"
					}break;
				case EFF_TARGET_PET:
					{// Target: Pet
						if(p_caster && p_caster->GetSummon())
							store_buff->m_unitTarget = p_caster->GetSummon()->GetGUID();
					}break;
				case EFF_TARGET_SINGLE_ENEMY:// Single Target Enemy
				case EFF_TARGET_SELECTED_ENEMY_CHANNELED:					// grep: i think this fits
				case EFF_TARGET_ALL_TARGETABLE_AROUND_LOCATION_IN_RADIUS: // related to Chess Move (DND), Firecrackers, Spotlight, aedm, Spice Mortar
				case EFF_TARGET_ALL_ENEMY_IN_AREA: // All Enemies in Area of Effect (TEST)
				case EFF_TARGET_ALL_ENEMY_IN_AREA_INSTANT: // All Enemies in Area of Effect instant (e.g. Flamestrike)
				case EFF_TARGET_ALL_ENEMIES_AROUND_CASTER:
				case EFF_TARGET_IN_FRONT_OF_CASTER:
				case EFF_TARGET_ALL_ENEMY_IN_AREA_CHANNELED:// All Enemies in Area of Effect(Blizzard/Rain of Fire/volley) channeled
				case EFF_TARGET_ALL_TARGETABLE_AROUND_LOCATION_IN_RADIUS_OVER_TIME:// related to scripted effects
				case EFF_TARGET_CURRENT_SELECTION:// Target Area by Players CurrentSelection()
				case EFF_TARGET_TARGET_AT_ORIENTATION_TO_CASTER:// Targets in Front of the Caster
					{
						if( p_caster != NULL )
						{
							Unit *selected = p_caster->GetMapMgr()->GetUnit(p_caster->GetSelection());
							if(isAttackable(p_caster,selected,!(m_spellInfo->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED)))
								store_buff->m_unitTarget = p_caster->GetSelection();
						}
						else if( u_caster != NULL )
						{
							if(	u_caster->GetAIInterface()->GetNextTarget() &&
								isAttackable(u_caster,u_caster->GetAIInterface()->GetNextTarget(),!(m_spellInfo->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED)) &&
								u_caster->GetDistanceSq(u_caster->GetAIInterface()->GetNextTarget()) <= r)
							{
								store_buff->m_unitTarget = u_caster->GetAIInterface()->GetNextTarget()->GetGUID();
							}
							if(u_caster->GetAIInterface()->getAITargetsCount())
							{
								//try to get most hated creature
								TargetMap *m_aiTargets = u_caster->GetAIInterface()->GetAITargets();
								TargetMap::iterator itr;
								for(itr = m_aiTargets->begin(); itr != m_aiTargets->end();itr++)
								{
									if( /*m_caster->GetMapMgr()->GetUnit(itr->first->GetGUID()) &&*/ itr->first->GetMapMgr() == m_caster->GetMapMgr() &&
										itr->first->isAlive() &&
										m_caster->GetDistanceSq(itr->first) <= r &&
										isAttackable(u_caster,itr->first,!(m_spellInfo->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED))
										)
									{
										store_buff->m_unitTarget=itr->first->GetGUID();
										break;
									}
								}
							}
						}
						//try to get a whatever target
						if(!store_buff->m_unitTarget)
						{
							store_buff->m_unitTarget=GetSinglePossibleEnemy(i);
						}
						//if we still couldn't get a target, check maybe we could use
//						if(!store_buff->m_unitTarget)
//						{
//						}
					}break;
					// spells like 17278:Cannon Fire and 21117:Summon Son of Flame A
				case EFF_TARGET_TELEPORT_LOCATION: // A single target at a xyz location or the target is a possition xyz
				case EFF_TARGET_LOCATION_TO_SUMMON:// Land under caster.Maybe not correct
					{
						store_buff->m_srcX=m_caster->GetPositionX();
						store_buff->m_srcY=m_caster->GetPositionY();
						store_buff->m_srcZ=m_caster->GetPositionZ();
						store_buff->m_targetMask |= TARGET_FLAG_SOURCE_LOCATION;
					}break;
				case EFF_TARGET_ALL_PARTY_AROUND_CASTER:
					{// All Party Members around the Caster in given range NOT RAID!
						Player* p = p_caster;
						if( p == NULL)
						{
							if( static_cast< Creature* >( u_caster )->IsTotem() )
								p = static_cast< Player* >( static_cast< Creature* >( u_caster )->GetTotemOwner() );
						}
						if( p == NULL )
							break;

						if(IsInrange(m_caster->GetPositionX(),m_caster->GetPositionY(),m_caster->GetPositionZ(),p,r))
						{
							store_buff->m_unitTarget = m_caster->GetGUID();
							break;
						}
						/*
						SubGroup * subgroup = p->GetGroup() ?
							p->GetGroup()->GetSubGroup(p->GetSubGroup()) : 0;

						if(subgroup)
						*/
						if( p->GetGroup() )
						{
							for( int i = 0; i < p->GetGroup()->GetSubGroupCount(); ++i )
							{
								SubGroup* subgroup = p->GetGroup()->GetSubGroup( i );
								if( subgroup )
								{
									for(GroupMembersSet::iterator itr = subgroup->GetGroupMembersBegin(); itr != subgroup->GetGroupMembersEnd(); ++itr)
									{
										if(!(*itr)->m_loggedInPlayer || m_caster == (*itr)->m_loggedInPlayer)
											continue;
										if(IsInrange(m_caster->GetPositionX(),m_caster->GetPositionY(),m_caster->GetPositionZ(),(*itr)->m_loggedInPlayer,r))
										{
											store_buff->m_unitTarget = (*itr)->m_loggedInPlayer->GetGUID();
											break;
										}
									}
								}
							}
						}
					}break;
				case EFF_TARGET_SINGLE_FRIEND:
				case EFF_TARGET_CHAIN:// Chain,!!only for healing!! for chain lightning =6
				case EFF_TARGET_PARTY_MEMBER:// Targeted Party Member
					{// Single Target Friend
						if( p_caster != NULL )
						{
							if(isFriendly(p_caster,p_caster->GetMapMgr()->GetUnit(p_caster->GetSelection())))
								store_buff->m_unitTarget = p_caster->GetSelection();
							else store_buff->m_unitTarget = p_caster->GetGUID();
						}
						else if( u_caster != NULL )
						{
							if(u_caster->GetUInt64Value(UNIT_FIELD_CREATEDBY))
								store_buff->m_unitTarget = u_caster->GetUInt64Value(UNIT_FIELD_CREATEDBY);
							else store_buff->m_unitTarget = u_caster->GetGUID();
						}
						else store_buff->m_unitTarget=GetSinglePossibleFriend(i,r);
					}break;
				case EFF_TARGET_GAMEOBJECT:
					{
						if(p_caster && p_caster->GetSelection())
							store_buff->m_unitTarget = p_caster->GetSelection();
					}break;
				case EFF_TARGET_DUEL:
					{// Single Target Friend Used in Duel
						if(p_caster && p_caster->DuelingWith && p_caster->DuelingWith->isAlive() && IsInrange(p_caster,p_caster->DuelingWith,r))
							store_buff->m_unitTarget = p_caster->GetSelection();
					}break;
				case EFF_TARGET_GAMEOBJECT_ITEM:{// Gameobject/Item Target
						//shit
					}break;
				case EFF_TARGET_PET_MASTER:{ // target is owner of pet
					// please correct this if not correct does the caster variablen need a Pet caster variable?
						if(u_caster && u_caster->IsPet())
							store_buff->m_unitTarget = static_cast< Pet* >( u_caster )->GetPetOwner()->GetGUID();
					}break;
				case EFF_TARGET_MINION:
				case EFF_TARGET_NETHETDRAKE_SUMMON_LOCATION:
					{// Minion Target
						if(m_caster->GetUInt64Value(UNIT_FIELD_SUMMON) == 0)
							store_buff->m_unitTarget = m_caster->GetGUID();
						else store_buff->m_unitTarget = m_caster->GetUInt64Value(UNIT_FIELD_SUMMON);
					}break;
				case EFF_TARGET_ALL_PARTY_IN_AREA://Party members of totem, inside given range
				case EFF_TARGET_SINGLE_PARTY:// Single Target Party Member
				case EFF_TARGET_ALL_PARTY: // all Members of the targets party
					{
						Player *p=NULL;
						if( p_caster != NULL )
								p = p_caster;
						else if( u_caster && u_caster->GetTypeId() == TYPEID_UNIT && static_cast< Creature* >( u_caster )->IsTotem() )
								p = static_cast< Player* >( static_cast< Creature* >( u_caster )->GetTotemOwner() );
						if( p_caster != NULL )
						{
							if(IsInrange(m_caster->GetPositionX(),m_caster->GetPositionY(),m_caster->GetPositionZ(),p,r))
							{
								store_buff->m_unitTarget = p->GetGUID();
								break;
							}
							/*
							SubGroup * pGroup = p_caster->GetGroup() ?
								p_caster->GetGroup()->GetSubGroup(p_caster->GetSubGroup()) : 0;
							*/

							if( p->GetGroup() )
							{
								for( int i = 0; i < p->GetGroup()->GetSubGroupCount(); ++i )
								{
									SubGroup* subgroup = p->GetGroup()->GetSubGroup( i );
									if( subgroup )
									{
										for(GroupMembersSet::iterator itr = subgroup->GetGroupMembersBegin(); itr != subgroup->GetGroupMembersEnd(); ++itr)
										{
											if(!(*itr)->m_loggedInPlayer || p == (*itr)->m_loggedInPlayer)
												continue;
											if(IsInrange(m_caster->GetPositionX(),m_caster->GetPositionY(),m_caster->GetPositionZ(),(*itr)->m_loggedInPlayer,r))
											{
												store_buff->m_unitTarget = (*itr)->m_loggedInPlayer->GetGUID();
												break;
											}
										}
									}
								}
							}
						}
					}break;
				case EFF_TARGET_SCRIPTED_OR_SINGLE_TARGET:{//Dummy Target
					//have no idea
					}break;
				case EFF_TARGET_SELF_FISHING://Fishing
				case EFF_TARGET_SCIPTED_OBJECT_LOCATION://Unknown Summon Atal'ai Skeleton
				case EFF_TARGET_DYNAMIC_OBJECT:// Portal
				case EFF_TARGET_LOCATION_NEAR_CASTER:	// Lightwells, etc
					{
						store_buff->m_unitTarget = m_caster->GetGUID();
					}break;
				case EFF_TARGET_SCRIPTED_GAMEOBJECT://Activate Object target(probably based on focus)
				case EFF_TARGET_TOTEM_EARTH:
				case EFF_TARGET_TOTEM_WATER:
				case EFF_TARGET_TOTEM_AIR:
				case EFF_TARGET_TOTEM_FIRE:// Totem
					{
						if( p_caster != NULL )
						{
							uint32 slot = m_spellInfo->Effect[i] - SPELL_EFFECT_SUMMON_TOTEM_SLOT1;
							if(p_caster->m_TotemSlots[slot] != 0)
								store_buff->m_unitTarget = p_caster->m_TotemSlots[slot]->GetGUID();
						}
					}break;
				case EFF_TARGET_AREAEFFECT_PARTY_AND_CLASS:{ // targets with the same group/raid and the same class
					//shit again
				}break;
				case EFF_TARGET_ALL_FRIENDLY_IN_AREA:{

				}break;

			}//end switch
		}//end for
	if(store_buff->m_unitTarget)
		store_buff->m_targetMask |= TARGET_FLAG_UNIT;
	if(store_buff->m_srcX)
		store_buff->m_targetMask |= TARGET_FLAG_SOURCE_LOCATION;
	if(store_buff->m_destX)
		store_buff->m_targetMask |= TARGET_FLAG_DEST_LOCATION;
}//end function

uint8 Spell::prepare( SpellCastTargets * targets )
{
	uint8 ccr;

	chaindamage = 0;
	m_targets = *targets;

	if( !m_triggeredSpell && p_caster != NULL && p_caster->CastTimeCheat )
		m_castTime = 0;
	else
	{
		m_castTime = GetCastTime( dbcSpellCastTime.LookupEntry( m_spellInfo->CastingTimeIndex ) );

		if( m_castTime && m_spellInfo->SpellGroupType && u_caster != NULL )
		{
			SM_FIValue( u_caster->SM_FCastTime, (int32*)&m_castTime, m_spellInfo->SpellGroupType );
			SM_PIValue( u_caster->SM_PCastTime, (int32*)&m_castTime, m_spellInfo->SpellGroupType );
		}

		// handle MOD_CAST_TIME
		if( u_caster != NULL && m_castTime )
		{
			float ftemp = 0.f;
			float fcastmod = u_caster->GetFloatValue( UNIT_MOD_CAST_SPEED );
			ftemp = fcastmod;
			fcastmod -= 1.f;
			if (fcastmod > 0)
			{
				m_castTime = float2int32( m_castTime * (ftemp));
			}
			else
			{
				ftemp = -ftemp + 2.0f;
				m_castTime = float2int32( m_castTime / (ftemp ));

			}
		//	m_castTime = float2int32( m_castTime * u_caster->GetFloatValue( UNIT_MOD_CAST_SPEED ) );
		}
		if( m_castTime < 0 )
			m_castTime = 0;
	}

	if( p_caster != NULL )
	{
		if( p_caster->cannibalize )
		{
			sEventMgr.RemoveEvents( p_caster, EVENT_CANNIBALIZE );
			p_caster->SetUInt32Value( UNIT_NPC_EMOTESTATE, 0 );
			p_caster->cannibalize = false;
		}

		p_caster->m_lastShotTime = (uint32)UNIXTIME;
	}

	//let us make sure cast_time is within decent range
	//this is a hax but there is no spell that has more then 10 minutes cast time

	if( m_castTime < 0 )
		m_castTime = 0;
	else if( m_castTime > 60 * 10 * 1000)
		m_castTime = 60 * 10 * 1000; //we should limit cast time to 10 minutes right ?

	m_timer = m_castTime;

	//if( p_caster != NULL )
	//   m_castTime -= 100;	  // session update time


	if( !m_triggeredSpell && p_caster != NULL && p_caster->CooldownCheat )
		p_caster->ClearCooldownForSpell( m_spellInfo->Id );

	m_spellState = SPELL_STATE_PREPARING;

	if( p_caster && p_caster->m_special_state & UNIT_STATE_STORM )
	{
		if ( !i_caster || (i_caster && (IsShapeSpell(m_spellInfo) || IsMountingSpell(m_spellInfo))))
			cancastresult = SPELL_FAILED_SPELL_UNAVAILABLE;
	}

//	if( cancastresult == SPELL_CANCAST_OK )
	if( m_triggeredSpell )
		cancastresult = SPELL_CANCAST_OK;
	else
	if ( cancastresult == SPELL_CANCAST_OK )
		cancastresult = CanCast(false);

	//if( cancastresult != SPELL_CANCAST_OK )
	//	MyLog::log->error( "CanCast result: %u. Refer to SpellFailure.h to work out why." , cancastresult );

	ccr = cancastresult;
	if( cancastresult != SPELL_CANCAST_OK )
	{
		SendCastResult( cancastresult );

		if( m_triggeredByAura )
		{
			SendChannelUpdate( 0 );
			if( u_caster != NULL )
				u_caster->RemoveAura( m_triggeredByAura );
		}
		else
		{
			// HACK, real problem is the way spells are handled
			// when a spell is channeling and a new spell is casted
			// that is a channeling spell, but not triggert by a aura
			// the channel bar/spell is bugged
			if( u_caster->GetUInt64Value( UNIT_FIELD_CHANNEL_OBJECT) > 0 && u_caster->GetCurrentSpell() )
			{
				u_caster->GetCurrentSpell()->cancel();
				SendChannelUpdate( 0 );
				cancel();
				return ccr;
			}
		}
		finish();
		return ccr;
	}
	else
	{
		if (m_timer > 0)
		{
			SendSpellStart();
		}
	

		// start cooldown handler
		if( p_caster != NULL && !p_caster->CastTimeCheat && !m_triggeredSpell )
		{
			AddStartCooldown();
		}

		if( i_caster == NULL )
		{
			if( p_caster != NULL && m_timer > 0 && !m_triggeredSpell )
				p_caster->delayAttackTimer( m_timer + 1000 );
				//p_caster->setAttackTimer(m_timer + 1000, false);
		}

		// aura state removal
		if( m_spellInfo->CasterAuraState )
			u_caster->RemoveFlag( UNIT_FIELD_AURASTATE, m_spellInfo->CasterAuraState );
	}

	//instant cast(or triggered) and not channeling
	if( u_caster != NULL && ( m_castTime > 0 || m_spellInfo->ChannelInterruptFlags ) && !m_triggeredSpell )
	{
		m_castPositionX = m_caster->GetPositionX();
		m_castPositionY = m_caster->GetPositionY();
		m_castPositionZ = m_caster->GetPositionZ();

		u_caster->castSpell( this );
	}
	else
		cast( false );

	return ccr;
}

void Spell::cancel(bool b)
{

	if (b)
	{
		SendInterrupted(SPELL_FAILED_INTERRUPTED);
	}


	if(m_spellState == SPELL_STATE_CASTING )
	{
		if( u_caster != NULL )
			u_caster->RemoveAura(m_spellInfo->Id);

		if(m_timer > 0 || m_Delayed)
		{
			if(p_caster && p_caster->IsInWorld())
			{
				Unit *pTarget = p_caster->GetMapMgr()->GetUnit(m_caster->GetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT));
				if(!pTarget)
					pTarget = p_caster->GetMapMgr()->GetUnit(p_caster->GetSelection());

				if(pTarget)
				{
					pTarget->RemoveAura(m_spellInfo->Id, m_caster->GetGUID());
				}
				if(m_AreaAura)//remove of blizz and shit like this
				{

					DynamicObject* dynObj=m_caster->GetMapMgr()->GetDynamicObject(m_caster->GetUInt32Value(UNIT_FIELD_CHANNEL_OBJECT));
					if(dynObj)
					{
						dynObj->RemoveFromWorld(true);
						delete dynObj;
					}
				}

				if(p_caster->GetSummonedObject())
				{
					if(p_caster->GetSummonedObject()->IsInWorld())
						p_caster->GetSummonedObject()->RemoveFromWorld(true);
					// for now..
					ASSERT(p_caster->GetSummonedObject()->GetTypeId() == TYPEID_GAMEOBJECT);
					delete ((GameObject*)(p_caster->GetSummonedObject()));
					p_caster->SetSummonedObject(NULL);
				}
				if (m_timer > 0)
					p_caster->delayAttackTimer(-m_timer);
//				p_caster->setAttackTimer(1000, false);
			 }
		}
		SendChannelUpdate(0);
	}

	//m_spellState = SPELL_STATE_FINISHED;

	// prevent memory corruption. free it up later.
	// if this is true it means we are currently in the cast() function somewhere else down the stack
	// (recursive spells) and we don't wanna have this class delete'd when we return to it.
	// at the end of cast() it will get freed anyway.
	if( !m_isCasting )
		finish();
}

void Spell::AddCooldown()
{
	if( p_caster != NULL )
		p_caster->Cooldown_Add( m_spellInfo, i_caster );
}

void Spell::AddStartCooldown()
{
	if( p_caster != NULL )
		p_caster->Cooldown_AddStart( m_spellInfo );
}

void Spell::cast(bool check)
{
	if( duelSpell && (
		( p_caster != NULL && p_caster->GetDuelState() != DUEL_STATE_STARTED ) ||
		( u_caster != NULL && u_caster->IsPet() && static_cast< Pet* >( u_caster )->GetPetOwner() && static_cast< Pet* >( u_caster )->GetPetOwner()->GetDuelState() != DUEL_STATE_STARTED ) ) )
	{
		// Can't cast that!
		SendInterrupted( SPELL_FAILED_TARGET_FRIENDLY );
		finish();
		return;
	}

	//MyLog::log->debug("Spell::cast %u, Unit: %u", m_spellInfo->Id, m_caster->GetLowGUID());

	if(check)
		cancastresult = CanCast(true);
	else
		cancastresult = SPELL_CANCAST_OK;

	if(cancastresult == SPELL_CANCAST_OK)
	{
		if (p_caster && !m_triggeredSpell && p_caster->IsInWorld() && GET_TYPE_FROM_GUID(m_targets.m_unitTarget)==HIGHGUID_TYPE_UNIT)
		{
			sQuestMgr.OnPlayerCast(p_caster,m_spellInfo->Id,m_targets.m_unitTarget);
		}
		if( m_spellInfo->Attributes & ATTRIBUTE_ON_NEXT_ATTACK)
		{
			if(!m_triggeredSpell)
			{
				// on next attack - we don't take the mana till it actually attacks.
				if(!HasPower())
				{
					SendInterrupted(SPELL_FAILED_NO_POWER);
					finish();
					return;
				}
			}
			else
			{
				// this is the actual spell cast
				if(!TakePower())	// shouldn't happen
				{
					SendInterrupted(SPELL_FAILED_NO_POWER);
					finish();
					return;
				}
			}
		}
		else
		{
			if(!m_triggeredSpell)
			{
				if(!TakePower()) //not enough mana
				{
					//MyLog::log->debug("Spell::Not Enough Mana");
					SendInterrupted(SPELL_FAILED_NO_POWER);
					finish();
					return;
				}
			}
		}

		for(uint32 i=0;i<3;i++)
        {
			if( m_spellInfo->Effect[i] && m_spellInfo->Effect[i] != SPELL_EFFECT_PERSISTENT_AREA_AURA)
				 FillTargetMap(i);
        }

		SendCastResult(cancastresult);
		if(cancastresult != SPELL_CANCAST_OK)
		{
			finish();
			return;
		}

		m_isCasting = true;

		//MyLog::log->info( "CanCastResult: %u" , cancastresult );
		if(!m_triggeredSpell)
			AddCooldown();

		if( p_caster )
		{
			if( m_spellInfo->NameHash == SPELL_HASH_SLAM)
			{
				/* slam - reset attack timer */
				p_caster->setAttackTimer( 0, true );
				p_caster->setAttackTimer( 0, false );
			}
			if( p_caster->IsStealth() && !(m_spellInfo->AttributesEx & ATTRIBUTESEX_NOT_BREAK_STEALTH) && m_spellInfo->Id != 1 ) // <-- baaaad, baaad hackfix - for some reason some spells were triggering Spell ID #1 and stuffing up the spell system.
			{
				/* talents procing - don't remove stealth either */
				if (!(m_spellInfo->Attributes & ATTRIBUTES_PASSIVE) &&
					!( pSpellId && dbcSpell.LookupEntry(pSpellId)->Attributes & ATTRIBUTES_PASSIVE ) )
				{
					p_caster->RemoveAura(p_caster->m_stealth);
					p_caster->m_stealth = 0;
				}
			}
		}

		/*SpellExtraInfo* sp = objmgr.GetSpellExtraData(m_spellInfo->Id);
		if(sp)
		{
			Unit *Target = objmgr.GetUnit(m_targets.m_unitTarget);
			if(Target)
				Target->RemoveBySpecialType(sp->specialtype, p_caster->GetGUID());
		}*/
		
		if (!(m_spellInfo->Attributes & ATTRIBUTE_ON_NEXT_ATTACK ))
		{
			SendSpellGo();
		}


		if(!(m_spellInfo->Attributes & ATTRIBUTE_ON_NEXT_ATTACK  && !m_triggeredSpell))//on next attack
		{
 			//SendSpellGo();

			//******************** SHOOT SPELLS ***********************
			//* Flags are now 1,4,19,22 (4718610) //0x480012

			if (m_spellInfo->Flags4 & 0x8000 && m_caster->IsPlayer() && m_caster->IsInWorld())
			{
                /// Part of this function contains a hack fix
                /// hack fix for shoot spells, should be some other resource for it
                //p_caster->SendSpellCoolDown(m_spellInfo->Id, m_spellInfo->RecoveryTime ? m_spellInfo->RecoveryTime : 2300);
				MSG_S2C::stSpellCoolDown Msg;
				Msg.caster_guid	= p_caster->GetNewGUID();
				Msg.spell_id	= m_spellInfo->Id;
				Msg.recover_time = uint32(m_spellInfo->RecoveryTime ? m_spellInfo->RecoveryTime : 2300);
				p_caster->GetSession()->SendPacket(Msg);
			}
			else
			{
				if( m_spellInfo->ChannelInterruptFlags != 0 && !m_triggeredSpell)
				{
					/*
					Channeled spells are handled a little differently. The five second rule starts when the spell's channeling starts; i.e. when you pay the mana for it.
					The rule continues for at least five seconds, and longer if the spell is channeled for more than five seconds. For example,
					Mind Flay channels for 3 seconds and interrupts your regeneration for 5 seconds, while Tranquility channels for 10 seconds
					and interrupts your regeneration for the full 10 seconds.
					*/

					uint32 channelDuration = GetDuration();
					m_spellState = SPELL_STATE_CASTING;
					SendChannelStart(channelDuration);
					if( p_caster != NULL )
					{
						//Use channel interrupt flags here
						if(m_targets.m_targetMask == TARGET_FLAG_DEST_LOCATION || m_targets.m_targetMask == TARGET_FLAG_SOURCE_LOCATION)
							u_caster->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, p_caster->GetSelection());
						else if(p_caster->GetSelection() == m_caster->GetGUID())
						{
							if(p_caster->GetSummon())
								u_caster->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, p_caster->GetSummon()->GetGUID());
							else if(m_targets.m_unitTarget)
								u_caster->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, m_targets.m_unitTarget);
							else
								u_caster->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, p_caster->GetSelection());
						}
						else
						{
							if(p_caster->GetSelection())
								u_caster->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, p_caster->GetSelection());
							else if(p_caster->GetSummon())
								u_caster->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, p_caster->GetSummon()->GetGUID());
							else if(m_targets.m_unitTarget)
								u_caster->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, m_targets.m_unitTarget);
							else
							{
								m_isCasting = false;
								cancel();
								return;
							}
						}
					}
					if(u_caster && u_caster->GetPowerType()==POWER_TYPE_MANA)
					{
						if(channelDuration <= 5000)
							u_caster->DelayPowerRegeneration(5000);
						else
							u_caster->DelayPowerRegeneration(channelDuration);
					}
				}
			}

			std::vector<uint64>::iterator i, i2;

			// this is here to avoid double search in the unique list
			//bool canreflect = false, reflected = false;
			for(int j=0;j<3;j++)
			{
				switch(m_spellInfo->EffectImplicitTargetA[j])
				{
					case EFF_TARGET_SINGLE_ENEMY:
					case EFF_TARGET_ALL_ENEMIES_AROUND_CASTER:
					case EFF_TARGET_IN_FRONT_OF_CASTER:
					case EFF_TARGET_DUEL:
						SetCanReflect();
						break;
				}
				if(GetCanReflect())
					continue;
				else
					break;
			}

			if(GetCanReflect() && m_caster->IsInWorld())
			{
				for(i= UniqueTargets.begin();i != UniqueTargets.end();i++)
				{
					Unit *Target = m_caster->GetMapMgr()->GetUnit(*i);
					if(Target)
                    {
                       SetReflected(Reflect(Target));
                    }

                    // if the spell is reflected
					if(IsReflected())
						break;
				}
			}

			bool isDuelEffect = false;
			//uint32 spellid = m_spellInfo->Id;

            // if the spell is not reflected
			if(!IsReflected())
			{
				for(uint32 x=0;x<3;x++)
				{
                    // check if we actualy have a effect
					if( m_spellInfo->Effect[x] && !m_effects_hide[x] )
					{
						isDuelEffect = isDuelEffect ||  m_spellInfo->Effect[x] == SPELL_EFFECT_DUEL;
						if( m_spellInfo->Effect[x] == SPELL_EFFECT_PERSISTENT_AREA_AURA ||
							m_spellInfo->Effect[x] == SPELL_EFFECT_APPLY_AREA_AURA ||
							m_spellInfo->Effect[x] == SPELL_EFFECT_AREA_SCHOOL_DAMAGE ||
							m_spellInfo->Effect[x] == SPELL_EFFECT_AREA_SCHOOL_HEAL ||
							m_spellInfo->Effect[x] == SPELL_EFFECT_AREA_SCHOOL_ENERGY ||
							m_spellInfo->Effect[x] == SPELL_EFFECT_AREA_BUF			//||
							//m_spellInfo->Effect[x] == SPELL_EFFECT_THREAT
							)
                        {
							HandleEffects(m_caster->GetGUID(),x);
                        }
						else if( m_spellInfo->Effect[x] == SPELL_EFFECT_DISENCHANT
							|| m_spellInfo->Effect[x] == SPELL_EFFECT_ENCHANT_ITEM
							|| m_spellInfo->Effect[x] == SPELL_EFFECT_ENCHANT_ITEM_TEMPORARY
							|| m_spellInfo->Effect[x] == SPELL_EFFECT_WASH_ITEM )
						{
							HandleEffects( m_targets.m_itemTarget, x );
						}
						else if((m_spellInfo->Effect[x] == SPELL_EFFECT_OPEN_LOCK
							) && m_targets.m_unitTarget)
							HandleEffects(m_targets.m_unitTarget, x);
						else if (m_targetUnits[x].size()>0)
						{
							for(int i = 0; i < m_targetUnits[x].size(); i++)
							{
								HandleEffects(m_targetUnits[x][i], x);
							}
						}
						else if(m_spellInfo->Effect[x] == SPELL_EFFECT_APPLY_AURA
							|| m_spellInfo->Effect[x] == SPELL_EFFECT_SCHOOL_DAMAGE 
							|| m_spellInfo->Effect[x] == SPELL_EFFECT_WEAPON_DAMAGE
							|| m_spellInfo->Effect[x] == SPELL_EFFECT_WEAPON_DAMAGE_NOSCHOOL
							|| m_spellInfo->Effect[x] == SPELL_EFFECT_KNOCK_BACK
							|| m_spellInfo->Effect[x] == SPELL_EFFECT_PLAYER_PULL 
							|| m_spellInfo->Effect[x] == SPELL_EFFECT_CHAIN_DAMAGE_WITH_CHANGE_VALUE
							|| m_spellInfo->Effect[x] ==  SPELL_EFFECT_POWER_BURN
							|| m_spellInfo->Effect[x] == SPELL_EFFECT_PHYSATTACK
							|| m_spellInfo->Effect[x] == SPELL_EFFECT_TRIGGER_SPELL_WITH_RANDOM_VALUE)
							;

						// Capt: The way this is done is NOT GOOD. Target code should be redone.
// 						else if( m_spellInfo->Effect[x] == SPELL_EFFECT_TELEPORT_UNITS)
//                         {
// 							HandleEffects(m_caster->GetGUID(),x);
//                         }
// 						else if( m_spellInfo->Effect[x] == SPELL_EFFECT_STUCK)
// 						{
// 							HandleEffects(m_caster->GetGUID(),x);
// 						}
// 						else if( m_spellInfo->Effect[x] == SPELL_EFFECT_SUMMON_WILD)
//                         {
// 							HandleEffects(m_caster->GetGUID(),x);
//                         }
						else
						{
							HandleEffects(m_caster->GetGUID(),x);
						}
					}
				}

				if( m_spellInfo->EffectApplyAuraName[0] != 0 || m_spellInfo->EffectApplyAuraName[1] != 0 ||
					m_spellInfo->EffectApplyAuraName[2] != 0)
				{
					hadEffect = true; // spell has had an effect (for item removal & possibly other things)
				}

				if(!(u_caster && !g_caster && m_spellInfo->speed > 0))
				{
					/* don't call HandleAddAura unless we actually have auras... - Burlex*/
					if( m_spellInfo->EffectApplyAuraName[0] != 0 || m_spellInfo->EffectApplyAuraName[1] != 0 ||
						m_spellInfo->EffectApplyAuraName[2] != 0)
					{

						for(i= UniqueTargets.begin();i != UniqueTargets.end();i++)
						{
							HandleAddAura((*i));
						}
					}
				}
				
				//m_SetModeratedTargets.clear();
				if (!ModeratedTargets.empty())
				{

					MSG_S2C::stSpell_Log_Miss Msg;
					Msg.spellid = m_spellInfo->Id;
					Msg.caster_guid = u_caster->GetGUID();
					bool bchannelFlag = false;
					bool bCancel = false;
					if (m_spellInfo->ChannelInterruptFlags)
					{
						bchannelFlag = true;
					}

					SpellTargetsList::iterator i;
					for(i = ModeratedTargets.begin() ; i != ModeratedTargets.end(); i ++)
					{

						Msg.target_guid = (*i).TargetGuid;
						Msg.SpellLogType= (*i).TargetModType;
						Unit* pUnit = u_caster->GetMapMgr()->GetUnit((*i).TargetGuid);
						if (pUnit&&pUnit->IsCreature()&&IsDamagingSpell(m_spellInfo))
						{
							pUnit->GetAIInterface()->AttackReaction(u_caster, 1, 0);
						}
						u_caster->SendMessageToSet(Msg, true, true);	
						if (bchannelFlag)
						{
							if (u_caster->GetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT) == (*i).TargetGuid)
							{
								bCancel = true;
							}

						}

					}
					if (bCancel)
					{
						cancel(bCancel);
					}
				}

				// spells that proc on spell cast, some talents
				if(p_caster && p_caster->IsInWorld())
				{
					for(i= UniqueTargets.begin();i != UniqueTargets.end();i++)
					{
						Unit * Target = p_caster->GetMapMgr()->GetUnit((*i));

						if(!Target)
							continue; //we already made this check, so why make it again ?

						if(!m_triggeredSpell)
						{
							p_caster->HandleProc(PROC_ON_CAST_SPECIFIC_SPELL | PROC_ON_CAST_SPELL,Target, m_spellInfo);
							p_caster->m_procCounter = 0; //this is required for to be able to count the depth of procs (though i have no idea where/why we use proc on proc)
						}

						Target->RemoveFlag(UNIT_FIELD_AURASTATE,m_spellInfo->TargetAuraState);
					}
				}
			}

			// we're much better to remove this here, because otherwise spells that change powers etc,
			// don't get applied.

			if(u_caster && !m_triggeredSpell && !m_triggeredByAura)
				u_caster->RemoveAurasByInterruptFlagButSkip(AURA_INTERRUPT_ON_CAST_SPELL, m_spellInfo->Id);

			m_isCasting = false;

			if(m_spellState != SPELL_STATE_CASTING)
				finish();


			
		}
		else //this shit has nothing to do with instant, this only means it will be on NEXT melee hit
		{
			// we're much better to remove this here, because otherwise spells that change powers etc,
			// don't get applied.

			if(u_caster && !m_triggeredSpell && !m_triggeredByAura)
				u_caster->RemoveAurasByInterruptFlagButSkip(AURA_INTERRUPT_ON_CAST_SPELL, m_spellInfo->Id);

			//not sure if it must be there...
			/*if( p_caster != NULL )
			{
				if(p_caster->m_onAutoShot)
				{
					p_caster->GetSession()->OutPacket(SMSG_CANCEL_AUTO_REPEAT);
					p_caster->GetSession()->OutPacket(SMSG_CANCEL_COMBAT);
					p_caster->m_onAutoShot = false;
				}
			}*/

			m_isCasting = false;
			SendCastResult(cancastresult);
			if( u_caster != NULL )
				u_caster->SetOnMeleeSpell(m_spellInfo->Id);

			finish();
		}
	}
	else
	{
		// cancast failed
		//SendCastResult(cancastresult);


		SendInterrupted(cancastresult);
		finish();
	}
}

void Spell::AddTime(uint32 type)
{
	if(u_caster && u_caster->IsPlayer())
	{
		if( m_spellInfo->InterruptFlags & CAST_INTERRUPT_ON_DAMAGE_TAKEN)
		{
			cancel();
			return;
		}
		if( m_spellInfo->SpellGroupType && u_caster)
		{
			float ch=0;
			SM_FFValue(u_caster->SM_PNonInterrupt,&ch,m_spellInfo->SpellGroupType);
			if(Rand(ch))
				return;
		}
		if( p_caster != NULL )
		{
			if(Rand(p_caster->SpellDelayResist[type]))
				return;
		}
		if(m_spellState==SPELL_STATE_PREPARING)
		{
			if( m_nSpellDelayed ++ < 2 )
			{
				int32 delay = m_castTime/5;
				m_timer+=delay;
				if(m_timer>m_castTime)
				{
					delay -= (m_timer - m_castTime);
					m_timer=m_castTime;
					if(delay<0)
						delay = 1;
				}

				MSG_S2C::stSpell_Delayed Msg;
				Msg.caster_guid = u_caster->GetNewGUID();
				Msg.delay		= delay;
				u_caster->SendMessageToSet(Msg, true);

				if(!p_caster)
				{
					if(m_caster->GetTypeId() == TYPEID_UNIT)
						u_caster->GetAIInterface()->AddStopTime(delay);
				}
				//in case cast is delayed, make sure we do not exit combat
				else
				{
					// sEventMgr.ModifyEventTimeLeft(p_caster,EVENT_ATTACK_TIMEOUT,PLAYER_ATTACK_TIMEOUT_INTERVAL,true);
					// also add a new delay to offhand and main hand attacks to avoid cutting the cast short
					p_caster->delayAttackTimer(delay);
				}
			}
		}
		else if( m_spellInfo->ChannelInterruptFlags != 48140)
		{
			int32 delay = GetDuration()/3;
			m_timer-=delay;
			if(m_timer<0)
				m_timer=0;
			else
				p_caster->delayAttackTimer(-delay);

			m_Delayed = true;
			if(m_timer>0)
				SendChannelUpdate(m_timer);

		}
	}
}

void Spell::update(uint32 difftime)
{

	if (m_spellInfo->ChannelInterruptFlags)
	{
		if (u_caster&&m_targets.m_unitTarget && u_caster->GetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT) > 0)
		{
			Unit* pTarget;
			pTarget = p_caster->GetMapMgr()->GetUnit(m_caster->GetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT));
			if(!pTarget)
				pTarget = p_caster->GetMapMgr()->GetUnit(p_caster->GetSelection());

		
			float maxRange = GetMaxRange( dbcSpellRange.LookupEntry( m_spellInfo->rangeIndex ) );
			if ((u_caster && pTarget&&
				(u_caster->GetMapMgr() != pTarget->GetMapMgr() ||
				u_caster->GetMapMgr() == pTarget->GetMapMgr() &&u_caster->CalcDistance(pTarget) > (maxRange + 6.f)))|| !u_caster || !pTarget )
			{
				
		
				cancel();
				return;
				
			}
			

		}
		
		if (m_spellInfo->ChannelInterruptFlags & CHANNEL_INTERRUPT_ON_MOVEMENT)
		{
			if( fabs(m_castPositionX - m_caster->GetPositionX()) > 0.1f ||
				fabs(m_castPositionY - m_caster->GetPositionY()) > 0.1f ||
				fabs(m_castPositionZ - m_caster->GetPositionZ()) > 0.1f )
			{
				if( u_caster != NULL )
				{
					if(u_caster->HasNoInterrupt() == 0)
					{	
						cancel();
						return;
					}
				}
			}

		}
	}
	// skip cast if we're more than 2/3 of the way through
	if( ( m_spellInfo->InterruptFlags & CAST_INTERRUPT_ON_MOVEMENT ) &&
	(((float)m_castTime /*/ 1.5f*/) > (float)m_timer ) /*&&*/
//		float(m_castTime)/float(m_timer) >= 2.0f		&&
	)
	{
		if(
			fabs(m_castPositionX - m_caster->GetPositionX()) > 0.001f ||
			fabs(m_castPositionY - m_caster->GetPositionY()) > 0.001f ||
			fabs(m_castPositionZ - m_caster->GetPositionZ()) > 0.001f
			)
		{
			if( u_caster != NULL )
			{
				if(u_caster->HasNoInterrupt() == 0 && m_spellInfo->EffectMechanic[1] != 14)
				{
					cancel();
					return;
				}
			}
		}

		if(u_caster && u_caster->IsPlayer())
		{
			if( static_cast<Player*>(u_caster)->m_bIsJumping )
			{
				if(u_caster->HasNoInterrupt() == 0 && m_spellInfo->EffectMechanic[1] != 14)
				{
					cancel();
					return;
				}
			}
		}
	}

	if(m_cancelled)
	{
		cancel();
		return;
	}

	switch(m_spellState)
	{
	case SPELL_STATE_PREPARING:
		{
			if(unitTarget)
			{
				if( !unitTarget->isAlive() && !IsTargetDieingSpell(m_spellInfo) )
				{
					//MyLog::log->debug("Spell Update: target is dead");
					SendCastResult(SPELL_FAILED_TARGETS_DEAD);
					cancel();
					return;
				}
			}
			//printf("spell::update m_timer %u, difftime %d, newtime %d\n", m_timer, difftime, m_timer-difftime);
			if((int32)difftime >= m_timer)
				cast(true);
			else
			{
				m_timer -= difftime;
				if((int32)difftime >= m_timer)
				{
					m_timer = 0;
					cast(true);
				}
			}
		} break;
	case SPELL_STATE_CASTING:
		{
			if(m_timer > 0)
			{
				if((int32)difftime >= m_timer)
					m_timer = 0;
				else
					m_timer -= difftime;
			}
			if(m_timer <= 0)
			{
				SendChannelUpdate(0);
				finish();
			}
		} break;
	}
}

void Spell::finish()
{

	m_SetModeratedTargets.clear();
	m_spellState = SPELL_STATE_FINISHED;

	if( u_caster != NULL )
	{
		u_caster->m_canMove = true;

		if (m_spellInfo->ChannelInterruptFlags != 0 && u_caster->GetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT) > 0)
		{
			u_caster->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT, 0);
			u_caster->SetUInt32Value(UNIT_CHANNEL_SPELL,0 );

		}

		// mana           channeled                                                     power type is mana
		if(m_usesMana && (m_spellInfo->ChannelInterruptFlags == 0 && !m_triggeredSpell) && u_caster->GetPowerType()==POWER_TYPE_MANA)
		{
			/*
			Five Second Rule
			After a character expends mana in casting a spell, the effective amount of mana gained per tick from spirit-based regeneration becomes a ratio of the normal
			listed above, for a period of 5 seconds. During this period mana regeneration is said to be interrupted. This is commonly referred to as the five second rule.
			By default, your interrupted mana regeneration ratio is 0%, meaning that spirit-based mana regeneration is suspended for 5 seconds after casting.
			Several effects can increase this ratio, including:
			*/

			u_caster->DelayPowerRegeneration(5000);
		}
	}
	/* Mana Regenerates while in combat but not for 5 seconds after each spell */
	/* Only if the spell uses mana, will it cause a regen delay.
	   is this correct? is there any spell that doesn't use mana that does cause a delay?
	   this is for creatures as effects like chill (when they have frost armor on) prevents regening of mana	*/

	//moved to spellhandler.cpp -> remove item when click on it! not when it finishes

	//enable pvp when attacking another player with spells
	if( p_caster != NULL )
	{
		if (m_spellInfo->Attributes & ATTRIBUTES_STOP_ATTACK)
		{
			p_caster->EventAttackStop();
			p_caster->smsg_AttackStop( unitTarget );
		}
		else
		{
			if(IsDamagingSpell(m_spellInfo) && unitTarget/* && m_spellInfo->rangeIndex*/)
			{
				//sEventMgr.AddEvent(p_caster,&Player::AttackAfterSpell,unitTarget->GetGUID(),
				//	m_spellInfo, EVENT_SPELL_DAMAGE_HIT, 500+p_caster->GetUInt32Value(UNIT_FIELD_BASEATTACKTIME), 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
				
				if( p_caster->getClass() == CLASS_WARRIOR || p_caster->getClass() == CLASS_BOW)
				{
					sEventMgr.AddEvent(p_caster,&Player::AttackAfterSpell,unitTarget->GetGUID(),
						m_spellInfo, EVENT_SPELL_DAMAGE_HIT, 500, 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
				}
			}
		}

	
		Unit *pTarget = NULL;
		if ( m_spellInfo->ChannelInterruptFlags != 0 && !m_triggeredSpell)
		{
			if( p_caster->IsInWorld())
			{
				p_caster->RemoveAura(m_spellInfo->Id, m_caster->GetGUID());
				pTarget = p_caster->GetMapMgr()->GetUnit(m_caster->GetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT));
				if(!pTarget)
					pTarget = p_caster->GetMapMgr()->GetUnit(p_caster->GetSelection());


				if(pTarget)
				{
					pTarget->RemoveAura(m_spellInfo->Id, m_caster->GetGUID());
				}
			}
			else
			{
				unitTarget->RemoveAura(m_spellInfo->Id, m_caster->GetGUID());
			}


		}

	
	}

	if( m_spellInfo->Effect[0] == SPELL_EFFECT_SUMMON_OBJECT ||
		m_spellInfo->Effect[1] == SPELL_EFFECT_SUMMON_OBJECT ||
		m_spellInfo->Effect[2] == SPELL_EFFECT_SUMMON_OBJECT)
		if( p_caster != NULL )
			p_caster->SetSummonedObject(NULL);
	/*
	Set cooldown on item
	*/
	if( i_caster && i_caster->GetOwner() && cancastresult == SPELL_CANCAST_OK && !GetSpellFailed() )
	{
			uint32 x;
		for(x = 0; x < 5; x++)
		{
			if(i_caster->GetProto()->Spells[x].Trigger == USE)
			{
				if(i_caster->GetProto()->Spells[x].Id)
					break;
			}
		}
		i_caster->GetOwner()->Cooldown_AddItem( i_caster->GetProto() , x );
	}
	/*
	We set current spell only if this spell has cast time or is channeling spell
	otherwise it's instant spell and we delete it right after completion
	*/

	if( u_caster != NULL )
	{
		if(!m_triggeredSpell && (m_spellInfo->ChannelInterruptFlags || m_castTime>0))
			u_caster->SetCurrentSpell(NULL);

		if( cancastresult == SPELL_CANCAST_OK && !GetSpellFailed() )
		{
			SunyouRaid* raid = u_caster->GetSunyouRaid();
			if( raid )
				raid->OnUnitCastSpell( m_spellInfo->Id, u_caster );
		}
	}
	delete this;
}

void Spell::SendCastResult(uint8 result)
{
	uint32 Extra = 0;
	if(result == SPELL_CANCAST_OK) return;

	SetSpellFailed();

	if(!m_caster->IsInWorld()) return;

	Player * plr = p_caster;

	if(!plr && u_caster)
		plr = u_caster->m_redirectSpellPackets;
	if(!plr) return;

	// for some reason, the result extra is not working for anything, including SPELL_FAILED_REQUIRES_SPELL_FOCUS
	switch( result )
	{
	case SPELL_FAILED_REQUIRES_SPELL_FOCUS:
		Extra = m_spellInfo->RequiresSpellFocus;
		break;

	case SPELL_FAILED_REQUIRES_AREA:
		Extra = m_spellInfo->RequiresAreaId;
		break;

	case SPELL_FAILED_TOTEMS:
		Extra = m_spellInfo->Totem[1] ? m_spellInfo->Totem[1] : m_spellInfo->Totem[0];
		break;

	//case SPELL_FAILED_TOTEM_CATEGORY: seems to be fully client sided.
	}

	//plr->SendCastResult(m_spellInfo->Id, result, extra_cast_number, Extra);
	MSG_S2C::stSpell_Cast_Result Msg;
	Msg.SpellId			= m_spellInfo->Id;
	Msg.ErrorMessage	= result;
	Msg.MultiCast		= extra_cast_number;
	Msg.Extra			= Extra;
	plr->GetSession()->SendPacket(Msg);
}

// uint16 0xFFFF
enum SpellStartFlags
{
    //0x01
    SPELL_START_FLAG_DEFAULT = 0x02, // atm set as default flag
    //0x04
    //0x08
    //0x10
    SPELL_START_FLAG_RANGED = 0x20,
    //0x40
    //0x80
    //0x100
    //0x200
    //0x400
    //0x800
    //0x1000
    //0x2000
    //0x4000
    //0x8000
};

void Spell::SendSpellStart()
{
	// no need to send this on passive spells
	if( !m_caster->IsInWorld() || m_spellInfo->Attributes & 64 || m_triggeredSpell )
		return;

	uint16 cast_flags = 2;

	if( GetType() == SPELL_DMG_TYPE_RANGED )
		cast_flags |= 0x20;

    if( m_spellInfo->Id == 8326 ) // death
        cast_flags = 0x0F;

	MSG_S2C::stSpell_Start Msg;
	Msg.caster_item_guid	= i_caster ? i_caster->GetNewGUID() : m_caster->GetNewGUID();
	Msg.caster_unit_guid	= i_caster ? u_caster->GetNewGUID() : m_caster->GetNewGUID();
	Msg.spell_id			= m_spellInfo->Id;
	Msg.extra_cast_number	= extra_cast_number;
	Msg.cast_flags			= cast_flags;
	Msg.casttime			= m_castTime;
	Msg.targets				= m_targets;

/*
	if( GetType() == SPELL_DMG_TYPE_RANGED )
	{
		ItemPrototype* ip = NULL;
        if( m_spellInfo->Id == SPELL_RANGED_THROW ) // throw
        {
			if( p_caster != NULL )
			{
				Item *itm = p_caster->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_RANGED );
				if( itm != NULL )
				{
	                ip = itm->GetProto();
					/* Throwing Weapon Patch by Supalosa
					p_caster->GetItemInterface()->RemoveItemAmt(it->GetEntry(),1);
					(Supalosa: Instead of removing one from the stack, remove one from durability)
					We don't need to check if the durability is 0, because you can't cast the Throw spell if the thrown weapon is broken, because it returns "Requires Throwing Weapon" or something.

					// burlex - added a check here anyway (wpe suckers :P)
					if( itm->GetDurability() > 0 )
					{
	                    itm->SetDurability( itm->GetDurability() - 1 );
						if( itm->GetDurability() == 0 )
							p_caster->ApplyItemMods( itm, EQUIPMENT_SLOT_RANGED, false, true );
					}
				}
				else
				{
					ip = ItemPrototypeStorage.LookupEntry( 2512 );	/*rough arrow
				}
            }
        }
//         else if( m_spellInfo->Flags4 & FLAGS4_PLAYER_RANGED_SPELLS )
//         {
// 			if( p_caster != NULL )
// 				ip = ItemPrototypeStorage.LookupEntry( p_caster->GetUInt32Value( PLAYER_AMMO_ID ) );
// 			else
// 				ip = ItemPrototypeStorage.LookupEntry( 2512 );	/*rough arrow
//         }
//
// 		if( ip != NULL )
// 			data << ip->DisplayInfoID << ip->InventoryType;
	}
*/
	m_caster->SendMessageToSet( Msg, true );

	if( u_caster )
	{
		SunyouRaid* raid = u_caster->GetSunyouRaid();
		if( raid )
			raid->OnUnitBeginCastSpell( m_spellInfo->Id, u_caster );
	}
}

void Spell::SendSpellGo()
{
	// Fill UniqueTargets
	TargetsList::iterator i, j;
	for( uint32 x = 0; x < 3; x++ )
	{
		if( m_spellInfo->Effect[x] )
		{
            bool add = true;
			for( i = m_targetUnits[x].begin(); i != m_targetUnits[x].end(); i++ )
			{
				add = true;
				for( j = UniqueTargets.begin(); j != UniqueTargets.end(); j++ )
				{
					if( (*j) == (*i) )
					{
						add = false;
						break;
					}
				}
				if( add && (*i) != 0 )
					UniqueTargets.push_back( (*i) );
                //TargetsList::iterator itr = std::unique(m_targetUnits[x].begin(), m_targetUnits[x].end());
                //UniqueTargets.insert(UniqueTargets.begin(),));
                //UniqueTargets.insert(UniqueTargets.begin(), itr);
			}
		}
	}

    // no need to send this on passive spells
    if( !m_caster->IsInWorld() || m_spellInfo->Attributes & 64 || m_triggeredSpell )
        return;

	// Start Spell
	MSG_S2C::stSpell_Go MsgGo;

	uint16 flags = 0;

	if ( GetType() == SPELL_DMG_TYPE_RANGED )
		flags |= SPELL_GO_FLAGS_RANGED; // 0x20 RANGED

	if( i_caster != NULL )
		flags |= SPELL_GO_FLAGS_ITEM_CASTER; // 0x100 ITEM CASTER

	if( ModeratedTargets.size() > 0 )
		flags |= SPELL_GO_FLAGS_EXTRA_MESSAGE; // 0x400 TARGET MISSES AND OTHER MESSAGES LIKE "Resist"

    // hacky..
    if( m_spellInfo->Id == 8326 ) // death
		flags = SPELL_GO_FLAGS_ITEM_CASTER | 0x0D;

	MsgGo.caster_item_guid		= i_caster && u_caster ? i_caster->GetNewGUID() : m_caster->GetNewGUID();
	MsgGo.caster_unit_guid		= i_caster && u_caster ? u_caster->GetNewGUID() : m_caster->GetNewGUID();
	MsgGo.spell_id				= m_spellInfo->Id;
	MsgGo.castFlags				= flags;
	MsgGo.casttime				= getMSTime();
	MsgGo.damageSeq				= m_curattackcnt;

	writeSpellGoTargets( &MsgGo );
	writeSpellMissedTargets( &MsgGo );
	MsgGo.targets = m_targets;// this write is included the target flag
/*
	// er why handle it being null inside if if you can't get into if if its null
	if( GetType() == SPELL_DMG_TYPE_RANGED )
	{
		ItemPrototype* ip = NULL;
		if( m_spellInfo->Id == SPELL_RANGED_THROW )
		{
			if( p_caster != NULL )
			{
				Item* it = p_caster->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_RANGED );
				if( it != NULL )
					ip = it->GetProto();
			}
			else
			{
				ip = ItemPrototypeStorage.LookupEntry(2512);	/*rough arrow*/
/*			}
        }
// 		else
// 		{
// 			if( p_caster != NULL )
// 				ip = ItemPrototypeStorage.LookupEntry(p_caster->GetUInt32Value( PLAYER_AMMO_ID ) );
// 			else // HACK FIX
// 				ip = ItemPrototypeStorage.LookupEntry(2512);	/*rough arrow*/
// 		}
// 		if( ip != NULL)
// 			data << ip->DisplayInfoID << ip->InventoryType;
/*	}
*/
	m_caster->SendMessageToSet( MsgGo, true );
	// spell log execute is still send 2.08
	// as I see with this combination, need to test it more
	//if (flags != 0x120 && m_spellInfo->Attributes & 16) // not ranged and flag 5
	  //  SendLogExecute(0,m_targets.m_unitTarget);
}

void Spell::writeSpellGoTargets( MSG_S2C::stSpell_Go* MsgGo )
{
	TargetsList::iterator i;
	for ( i = UniqueTargets.begin(); i != UniqueTargets.end(); i++ )
	{
		SendCastSuccess(*i);
		MsgGo->vTargetHit.push_back(*i);
	}
}

void Spell::writeSpellMissedTargets( MSG_S2C::stSpell_Go* MsgGo )
{
	/*
	 * The flags at the end known to us so far are.
	 * 1 = Miss
	 * 2 = Resist
	 * 3 = Dodge // mellee only
	 * 4 = Deflect
	 * 5 = Block // mellee only
	 * 6 = Evade
	 * 7 = Immune
	 */
	SpellTargetsList::iterator i;
	if(u_caster && u_caster->isAlive())
	{
		for ( i = ModeratedTargets.begin(); i != ModeratedTargets.end(); i++ )
		{
			MsgGo->vTargetMiss.push_back(MSG_S2C::stSpell_Go::stTargetMiss((*i).TargetGuid, (*i).TargetModType));
			///handle proc on resist spell
			Unit* target = u_caster->GetMapMgr()->GetUnit((*i).TargetGuid);
			if(target && target->isAlive())
			{
				u_caster->HandleProc(PROC_ON_RESIST_VICTIM,target,m_spellInfo/*,damage*/);		/** Damage is uninitialized at this point - burlex */
				target->CombatStatusHandler_ResetPvPTimeout(); // aaa
				u_caster->CombatStatusHandler_ResetPvPTimeout(); // bbb
			}
		}
	}
	else
		for ( i = ModeratedTargets.begin(); i != ModeratedTargets.end(); i++ )
		{
			MsgGo->vTargetMiss.push_back(MSG_S2C::stSpell_Go::stTargetMiss((*i).TargetGuid, (*i).TargetModType));
		}
}

void Spell::SendLogExecute(uint32 damage, uint64 & targetGuid)
{
	MSG_S2C::stSpell_Log_Excute Msg;
	Msg.caster_guid	= m_caster->GetNewGUID();
	Msg.spell_id	= m_spellInfo->Id;
	Msg.target_guid	= targetGuid;
	Msg.damage		= damage;
	m_caster->SendMessageToSet(Msg,true);
}

void Spell::SendInterrupted(uint8 result)
{
	SetSpellFailed();

	if(!m_caster->IsInWorld()) return;

	// send the failure to pet owner if we're a pet
	Player *plr = p_caster;
	if(!plr && m_caster->IsPet())
		plr = static_cast<Pet*>(m_caster)->GetPetOwner();
	if(!plr && u_caster)
		plr = u_caster->m_redirectSpellPackets;

	MSG_S2C::stSpell_Failure Msg;

	Msg.caster_guid = m_caster->GetNewGUID();
	Msg.spell_id	= m_spellInfo->Id;
	Msg.error		= result;
	if(plr&&plr->IsPlayer())
	{

		plr->GetSession()->SendPacket(Msg);
	}

	m_caster->SendMessageToSet(Msg, false);
}

void Spell::SendChannelUpdate(uint32 time)
{
	if(time == 0)
	{
		if(u_caster && u_caster->IsInWorld())
		{
			DynamicObject* dynObj=u_caster->GetMapMgr()->GetDynamicObject(u_caster->GetUInt32Value(UNIT_FIELD_CHANNEL_OBJECT));
			if(dynObj)
			{
				dynObj->RemoveFromWorld(true);
				delete dynObj;
			}
			u_caster->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT,0);
			u_caster->SetUInt32Value(UNIT_CHANNEL_SPELL,0);
		}
	}

	if (!p_caster)
		return;

	MSG_S2C::stSpell_Channel_Update Msg;
	Msg.caster_guid = p_caster->GetNewGUID();
	Msg.time		= time;
	p_caster->SendMessageToSet(Msg, true);
}

void Spell::SendChannelStart(uint32 duration)
{
	if (m_caster->GetTypeId() != TYPEID_GAMEOBJECT)
	{
		// Send Channel Start
		MSG_S2C::stSpell_Channel_Start Msg;
		Msg.caster_guid = m_caster->GetNewGUID();
		Msg.SpellId		= m_spellInfo->Id;
		Msg.duration	= duration;
		m_caster->SendMessageToSet(Msg, true);
	}

	m_castTime = m_timer = duration;

	if( u_caster != NULL )
		u_caster->SetUInt32Value(UNIT_CHANNEL_SPELL,m_spellInfo->Id);

	/*
	Unit* target = objmgr.GetCreature( static_cast< Player* >( m_caster )->GetSelection());
	if(!target)
		target = objmgr.GetObject<Player>( static_cast< Player* >( m_caster )->GetSelection());
	if(!target)
		return;

	m_caster->SetUInt32Value(UNIT_FIELD_CHANNEL_OBJECT,target->GetGUIDLow());
	m_caster->SetUInt32Value(UNIT_FIELD_CHANNEL_OBJECT+1,target->GetGUIDHigh());
	//disabled it can be not only creature but GO as well
	//and GO is not selectable, so this method will not work
	//these fields must be filled @ place of call
	*/
}

void Spell::SendResurrectRequest(Player* target)
{
	MSG_S2C::stSpell_Resurrect_Request Msg;
	Msg.caster_guid	= m_caster->GetGUID();
	target->GetSession()->SendPacket(Msg);
}

bool Spell::HasPower()
{
	int32 powerField;
	if( u_caster != NULL )
		if(u_caster->HasFlag(UNIT_NPC_FLAGS,UNIT_NPC_FLAG_TRAINER_CLASS) || u_caster->HasFlag(UNIT_NPC_FLAGS,UNIT_NPC_FLAG_TRAINER_PROF))
			return true;

	if(p_caster && p_caster->PowerCheat)
		return true;

	switch(m_spellInfo->powerType)
	{
	case POWER_TYPE_HEALTH:{
		powerField = UNIT_FIELD_HEALTH;
						   }break;
	case POWER_TYPE_MANA:{
		powerField = UNIT_FIELD_POWER1;
		m_usesMana=true;
						 }break;
	case POWER_TYPE_GUILD_SCORE:
		{
			powerField = -1;
		}
		break;
	
	default:{
		MyLog::log->debug("unknown power type");
		// we should'nt be here to return
		return false;
			}break;
	}


	//FIXME: add handler for UNIT_FIELD_POWER_COST_MODIFIER
	//UNIT_FIELD_POWER_COST_MULTIPLIER
	if( u_caster != NULL )
	{
		if( m_spellInfo->AttributesEx & 2 && powerField != -1) // Uses %100 mana
		{
			m_caster->SetUInt32Value(powerField, 0);
			return true;
		}
	}
	
	Guild* pGuild = NULL;
	GuildRank* pGuildRank = NULL;
	int32 currentPower;
	if (powerField != -1)
	{
		currentPower = m_caster->GetUInt32Value(powerField);
	}
	else
	{
		if (p_caster)
		{
			if (p_caster->m_playerInfo)
			{
				pGuildRank = p_caster->m_playerInfo->guildRank;
				pGuild = p_caster->m_playerInfo->guild;
			}

			if (pGuild && pGuildRank && (pGuildRank->iRights == GR_RIGHT_ALL ||pGuildRank->iRights & GR_RIGHT_USE_SKILL ))
			{
				currentPower = pGuild->GetScore();
			}
			else
			{
				return false;  
			}
		}
		else
		{
			return false;
		}
		
	}

	int32 cost;
	if( m_spellInfo->ManaCostPercentage)//Percentage spells cost % of !!!BASE!!! mana
	{
		switch(m_spellInfo->powerType)
		{
		case POWER_TYPE_MANA:
			{
				cost = (m_caster->GetUInt32Value(UNIT_FIELD_BASE_MANA)*m_spellInfo->ManaCostPercentage)/100;
			}
			break;
		case POWER_TYPE_HEALTH:
			{
				cost = (m_caster->GetUInt32Value(UNIT_FIELD_BASE_HEALTH)*m_spellInfo->ManaCostPercentage)/100;
			}
			break;
		case POWER_TYPE_GUILD_SCORE:
			{
				if (p_caster&&pGuild)
				{
					cost = (pGuild->GetScore()*m_spellInfo->ManaCostPercentage)/100;	
				}
			}
			break;
		}
	}
	else
	{
		cost = m_spellInfo->manaCost;
	}

	if((int32)m_spellInfo->powerType==POWER_TYPE_HEALTH)
		cost -= m_spellInfo->baseLevel;//FIX for life tap
	else if( u_caster != NULL )
	{
		/*if( m_spellInfo->powerType==POWER_TYPE_MANA)*/
		cost += u_caster->PowerCostMod[m_spellInfo->School];//this is not percent!
		//else
		//	cost += u_caster->PowerCostMod[0];
		cost +=float2int32(cost*u_caster->GetFloatValue(UNIT_FIELD_POWER_COST_MULTIPLIER+m_spellInfo->School));
	}

	//apply modifiers
	if( m_spellInfo->SpellGroupType && u_caster)
	{
		SM_FIValue(u_caster->SM_FCost,&cost,m_spellInfo->SpellGroupType);
		SM_PIValue(u_caster->SM_PCost,&cost,m_spellInfo->SpellGroupType);
	}

	if (cost <=0)
		return true;

	//FIXME:DK:if field value < cost what happens
	if(powerField == UNIT_FIELD_HEALTH)
	{
		return true;
	}
	else
	{
		if(cost <= currentPower) // Unit has enough power (needed for creatures)
		{
			return true;
		}
		else
			return false;
	}
}

bool Spell::TakePower()
{
	int32 powerField;
	if( u_caster != NULL )
	if(u_caster->HasFlag(UNIT_NPC_FLAGS,UNIT_NPC_FLAG_TRAINER_CLASS) || u_caster->HasFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_TRAINER_PROF))
		return true;

	if(p_caster && p_caster->PowerCheat)
		return true;

	switch(m_spellInfo->powerType)
	{
		case POWER_TYPE_HEALTH:{
			powerField = UNIT_FIELD_HEALTH;
							   }break;
		case POWER_TYPE_MANA:{
			powerField = UNIT_FIELD_POWER1;
			m_usesMana=true;
							 }break;
		case POWER_TYPE_GUILD_SCORE:
			{
				powerField = -1;
			}
			break;
		default:{
			MyLog::log->debug("unknown power type");
			// we should'nt be here to return
			return false;
				}break;
	}

	//FIXME: add handler for UNIT_FIELD_POWER_COST_MODIFIER
	//UNIT_FIELD_POWER_COST_MULTIPLIER
	if( u_caster != NULL )
	{
		if( m_spellInfo->AttributesEx & ATTRIBUTESEX_DRAIN_WHOLE_MANA ) // Uses %100 mana
		{
			m_caster->SetUInt32Value(powerField, 0);
			return true;
		}
	}
	int32 currentPower = 0;
	Guild* pGuild = NULL;
	GuildRank* pGuildRank = NULL;
	if (powerField != -1)
	{
	     currentPower = m_caster->GetUInt32Value(powerField);
	}
	else
	{
		if (p_caster)
		{
			if (p_caster->m_playerInfo)
			{
				pGuildRank = p_caster->m_playerInfo->guildRank;
				pGuild = p_caster->m_playerInfo->guild;
			}

			if (pGuild && pGuildRank && (pGuildRank->iRights == GR_RIGHT_ALL ||pGuildRank->iRights & GR_RIGHT_USE_SKILL ))
			{
				currentPower = pGuild->GetScore();
			}
			else
			{
				return false;  
			}
		}
		else
		{
			return false;
		}
	}

	int32 cost;
	if( m_spellInfo->ManaCostPercentage)//Percentage spells cost % of !!!BASE!!! mana
	{
		switch(m_spellInfo->powerType)
		{
		case POWER_TYPE_MANA:
			{
				cost = (m_caster->GetUInt32Value(UNIT_FIELD_BASE_MANA)*m_spellInfo->ManaCostPercentage)/100;
			}
			break;
		case  POWER_TYPE_HEALTH:
			{
				cost = (m_caster->GetUInt32Value(UNIT_FIELD_BASE_HEALTH)*m_spellInfo->ManaCostPercentage)/100;
			}
			break;

		case  POWER_TYPE_GUILD_SCORE:
			{
				if (p_caster)
				{
					if (pGuild)
					{
						cost = (pGuild->GetScore()* m_spellInfo->ManaCostPercentage)/ 100;
					}			
				}
			}
			break;
		}
	}
	else
	{
		cost = m_spellInfo->manaCost;
	}

	if((int32)m_spellInfo->powerType==POWER_TYPE_HEALTH)
			cost -= m_spellInfo->baseLevel;//FIX for life tap
	else if( u_caster != NULL )
	{
		//if( m_spellInfo->powerType==POWER_TYPE_MANA)
		//	cost += u_caster->PowerCostMod[m_spellInfo->School];//this is not percent!
		//else
		cost += u_caster->PowerCostMod[m_spellInfo->School];
		cost +=float2int32(cost*u_caster->GetFloatValue(UNIT_FIELD_POWER_COST_MULTIPLIER+m_spellInfo->School));
	}

	//apply modifiers
	if( m_spellInfo->SpellGroupType && u_caster)
	{
		  SM_FIValue(u_caster->SM_FCost,&cost,m_spellInfo->SpellGroupType);
		  SM_PIValue(u_caster->SM_PCost,&cost,m_spellInfo->SpellGroupType);
	}

	if (cost <=0)
		return true;

	//FIXME:DK:if field value < cost what happens
	if(powerField == UNIT_FIELD_HEALTH)
	{
		m_caster->DealDamage(u_caster, cost, 0, 0, 0,true);
		return true;
	}
	else
	{
		if(cost <= currentPower) // Unit has enough power (needed for creatures)
		{
			if (powerField != -1)
			{
				m_caster->SetUInt32Value(powerField, currentPower - cost);
			}
			else
			{
				if (pGuild)
				{
					return pGuild->SpendScore(cost);
				}
			}
			return true;
		}
		else
			return false;
	}
}

void Spell::HandleEffects(uint64 guid, uint32 i)
{
	EffectTargetGUID = guid;
	if(guid == m_caster->GetGUID())
	{
		unitTarget = u_caster;
		gameObjTarget = g_caster;
		playerTarget = p_caster;
		itemTarget = i_caster;
	}
	else
	{
		if(!m_caster->IsInWorld())
		{
			unitTarget = 0;
			playerTarget = 0;
			itemTarget = 0;
			gameObjTarget = 0;
			corpseTarget = 0;
		}
		else if(m_targets.m_targetMask & TARGET_FLAG_TRADE_ITEM)
		{
			if( p_caster != NULL )
			{
				Player * plr = p_caster->GetTradeTarget();
				if(plr)
					itemTarget = plr->getTradeItem((uint32)guid);
			}
		}
		else
		{
			unitTarget = NULL;
			playerTarget = NULL;
			switch(GET_TYPE_FROM_GUID(guid))
			{
			case HIGHGUID_TYPE_UNIT:
				unitTarget = m_caster->GetMapMgr()->GetCreature(GET_LOWGUID_PART(guid));
				break;
			case HIGHGUID_TYPE_PET:
				unitTarget = m_caster->GetMapMgr()->GetPet(GET_LOWGUID_PART(guid));
				break;
			case HIGHGUID_TYPE_PLAYER:
				{
					unitTarget =  m_caster->GetMapMgr()->GetPlayer((uint32)guid);
					playerTarget = static_cast< Player* >(unitTarget);
				}break;
			case HIGHGUID_TYPE_ITEM:
				if( p_caster != NULL )
					itemTarget = p_caster->GetItemInterface()->GetItemByGUID(guid);
				break;
			case HIGHGUID_TYPE_GAMEOBJECT:
				gameObjTarget = m_caster->GetMapMgr()->GetGameObject(GET_LOWGUID_PART(guid));
				break;
			case HIGHGUID_TYPE_CORPSE:
				corpseTarget = objmgr.GetCorpse((uint32)guid);
				break;
			}
		}
	}

	if( unitTarget && unitTarget->isDead() && IsDamagingSpell(m_spellInfo) )
		return;

	damage = CalculateEffect(i,unitTarget);

	//MyLog::log->debug( "WORLD: Spell effect id = %u, damage = %d", m_spellInfo->Effect[i], damage);

	if( m_spellInfo->Effect[i]<TOTAL_SPELL_EFFECTS)
	{
		/*if(unitTarget && p_caster && isAttackable(p_caster,unitTarget))
			sEventMgr.ModifyEventTimeLeft(p_caster,EVENT_ATTACK_TIMEOUT,PLAYER_ATTACK_TIMEOUT_INTERVAL);*/
		(*this.*SpellEffectsHandler[m_spellInfo->Effect[i]])(i);
	}
	else
		MyLog::log->error("SPELL: unknown effect %u spellid %u",m_spellInfo->Effect[i], m_spellInfo->Id);
}

void Spell::HandleAddAura(uint64 guid)
{
	Unit * Target = 0;
	if(guid == 0)
		return;

	if(u_caster && u_caster->GetGUID() == guid)
		Target = u_caster;
	else if(m_caster->IsInWorld())
		Target = m_caster->GetMapMgr()->GetUnit(guid);

	if(!Target)
		return;

	// Applying an aura to a flagged target will cause you to get flagged.
    // self casting doesnt flag himself.
	if(Target->IsPlayer() && p_caster && p_caster != static_cast< Player* >(Target))
	{
		if(static_cast< Player* >(Target)->IsPvPFlagged())
			p_caster->SetPvPFlag();
	}

	// remove any auras with same type
//	if( m_spellInfo->buffType > 0)
//		Target->RemoveAurasByBuffType(m_spellInfo->buffType, m_caster->GetGUID(),m_spellInfo->Id);

	uint32 spellid = 0;
//
// 	if( m_spellInfo->MechanicsType == 25 && m_spellInfo->Id != 25771) // Cast spell Forbearance
// 		spellid = 25771;
// 	else if( m_spellInfo->NameHash == SPELL_HASH_AVENGING_WRATH )
// 		spellid = 25771;
// 	else if( m_spellInfo->MechanicsType == 16 && m_spellInfo->Id != 11196) // Cast spell Recently Bandaged
// 		spellid = 11196;
// 	else if( m_spellInfo->MechanicsType == 19 && m_spellInfo->Id != 6788) // Cast spell Weakened Soul
// 		spellid = 6788;
// 	else if( m_spellInfo->Id == 45438) // Cast spell Hypothermia
// 		spellid = 41425;
// 	else if( m_spellInfo->Id == 30451) // Cast spell Arcane Blast
// 		spellid = 36032;
// 	else if( m_spellInfo->Id == 20572 || m_spellInfo->Id == 33702 || m_spellInfo->Id == 33697) // Cast spell Blood Fury
// 		spellid = 23230;

	if(spellid && p_caster)
	{
		SpellEntry *spellInfo = dbcSpell.LookupEntry( spellid );
		if(!spellInfo) return;
		Spell *spell = new Spell(p_caster, spellInfo ,true, NULL);
		SpellCastTargets targets(Target->GetGUID());
		spell->prepare(&targets);
	}

	// avoid map corruption
	if(Target->GetInstanceID()!=m_caster->GetInstanceID())
		return;

	std::map<uint32,Aura*>::iterator itr=Target->tmpAura.find(m_spellInfo->Id);
	if(itr!=Target->tmpAura.end())
	{
		Aura* a = itr->second;
		if( a )
		{
			if( !a->IsValid() )
			{
				MyLog::log->error( "invalid aura! spell id: %d", m_spellInfo->Id );
				Target->tmpAura.erase(itr);
				return;
			}
			if( a->GetSpellProto()->procCharges>0)
			{
				Aura *aur=NULL;
				for(int i=0;i<a->GetSpellProto()->procCharges-1;i++)
				{
					aur = new Aura(a->GetSpellProto(),a->GetDuration(),a->GetCaster(),a->GetTarget());
					Target->AddAura(aur);
					aur=NULL;
				}
				if(!(a->GetSpellProto()->procFlags & PROC_REMOVEONUSE))
				{
					SpellCharge charge;
					charge.count=a->GetSpellProto()->procCharges;
					charge.spellId=a->GetSpellId();
					charge.ProcFlag=a->GetSpellProto()->procFlags;
					charge.lastproc = 0;
					Target->m_chargeSpells.insert(make_pair(a->GetSpellId(),charge));
				}
			}
			Target->AddAura(a); // the real spell is added last so the modifier is removed last
			Target->tmpAura.erase(itr);
		}
	}
}

/*
void Spell::TriggerSpell()
{
	if(TriggerSpellId != 0)
	{
		// check for spell id
		SpellEntry *spellInfo = sSpellStore.LookupEntry(TriggerSpellId );

		if(!spellInfo)
		{
			MyLog::log->error("WORLD: unknown spell id %i\n", TriggerSpellId);
			return;
		}

		Spell *spell = new Spell(m_caster, spellInfo,false, NULL);
		WPAssert(spell);

		SpellCastTargets targets;
		if(TriggerSpellTarget)
			targets.m_unitTarget = TriggerSpellTarget;
		else
			targets.m_unitTarget = m_targets.m_unitTarget;

		spell->prepare(&targets);
	}
}*/

bool Spell::IsAspect()
{
	return m_spellInfo->Id ==  2596;
}

bool Spell::IsSeal()
{
	return false;
}

uint8 Spell::CanCast(bool tolerate)
{
	uint32 i;
	if(!HasPower())	// shouldn't happen
	{
		return SPELL_FAILED_NO_POWER;
	}
	if(objmgr.IsSpellDisabled(m_spellInfo->Id))
		return SPELL_FAILED_SPELL_UNAVAILABLE;

	if( p_caster != NULL )
	{
		if(IsExtraXPSpell(m_spellInfo))
		{
			if(p_caster->m_ExtraEXPRatio != 100)
			{
				return SPELL_FAILED_SPELL_IN_PROGRESS;
			}
		}
		bool bCreateItem = false;

		bool bConsumeSelf = false;
		uint32 SelfEntry = 0;
		for (int i = 0; i < 3; i ++)
		{
			if (m_spellInfo->Effect[i] == SPELL_EFFECT_ITEMS_CHANGE_ITEM ||m_spellInfo->Effect[i] == SPELL_EFFECT_FOR_ITEMS_CHANGE_ITEM
				|| m_spellInfo->Effect[i] == SPELL_EFFECT_SKILL_CREATE_ITEM)
			{
				if( i_caster && m_spellInfo->EffectMiscValue[i] == i_caster->GetEntry() )
				{
					bConsumeSelf = true;
					SelfEntry = i_caster->GetEntry();
					break;
				}
			}
		}
		
		Group* g = p_caster->GetGroup();
		for (int i = 0; i < 3; i ++)
		{
			if( m_spellInfo->Effect[i] == SPELL_EFFECT_PRIVATE_FAIRGROUND )
			{
				if( g )
				{
					if( g->GetLeader() != p_caster->m_playerInfo )
						return SPELL_FAILED_YOU_ARE_NOT_GROUP_LEADER;

					if( g->GetCurrentMemberCount() > m_spellInfo->EffectBasePoints[i] )
						return SPELL_FAILED_TOO_MANY_GROUP_MEMBERS;
				}
			}
			if (m_spellInfo->Effect[i] == SPELL_EFFECT_ADDPETTOLIST)
			{
				if (p_caster->IsHavePet(m_spellInfo->EffectMiscValue[i]))
				{
					return SPELL_FAILED_HAVE_PET;
				}
			}
			if (m_spellInfo->Effect[i] == SPELL_EFFECT_ITEMS_CHANGE_ITEM ||m_spellInfo->Effect[i] == SPELL_EFFECT_FOR_ITEMS_CHANGE_ITEM
				|| m_spellInfo->Effect[i] == SPELL_EFFECT_SKILL_CREATE_ITEM)
			{
				if( p_caster->GetItemInterface()->GetItemCount( m_spellInfo->EffectMiscValue[i] ) < m_spellInfo->EffectBasePoints[i] )
				{
					return SPELL_FAILED_ITEM_NOT_FOUND;
				}
				if( bConsumeSelf && SelfEntry == m_spellInfo->EffectMiscValue[i] )
				{
					if( p_caster->GetItemInterface()->GetItemCount( m_spellInfo->EffectMiscValue[i] ) < m_spellInfo->EffectBasePoints[i] + 1 )
					{
						return SPELL_FAILED_ITEM_NOT_FOUND;
					}
				}
			}

			if (m_spellInfo->Effect[i] == SPELL_EFFECT_SKILL_CREATE_ITEM)
			{
				skilllinespell* skill = objmgr.GetSpellSkill(m_spellInfo->Id);
				if( skill )
				{
					if (!p_caster->_HasSkillLine(skill->skilline))
					{
						return SPELL_FAILED_MIN_SKILL;
					}
					uint32 amt = p_caster->_GetSkillLineCurrent( skill->skilline, false );
					if (amt < skill->minrank)
					{
						return SPELL_FAILED_MIN_SKILL;
					}
				}
				else
					return SPELL_FAILED_UNKNOWN;
			}
			
			if( m_spellInfo->Effect[i] == SPELL_EFFECT_SUMMON_PLAYER )
			{
				if( p_caster->m_sunyou_instance )
				{
					instance_category_t c = p_caster->m_sunyou_instance->GetCategory();
					if( c == INSTANCE_CATEGORY_BATTLE_GROUND
						|| c == INSTANCE_CATEGORY_TEAM_ARENA
						|| c == INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND 
						|| c == INSTANCE_CATEGORY_NEW_BATTLE_GROUND)
					{
						return SPELL_FAILED_NOT_IN_BATTLEGROUND;
					}
					else if( p_caster->m_sunyou_instance->IsFull() )
					{
						return SPELL_FAILED_INSTANCE_FULL;
					}
				}
			}

			if (m_spellInfo->Effect[i] == SPELL_EFFECT_GUILD_NOWAR)
			{
				bool bCanCast = true;
				if (p_caster)
				{
					Guild* pGuild = p_caster->GetGuild();
					if (!(pGuild && pGuild->GetGuildLeader() == p_caster->GetLowGUID()))
					{
						return SPELL_FAILED_YOU_ARE_NOT_GUILD_LEADER;
					}

				}
			}
		}

		if( m_spellInfo )

		if (p_caster->GetUInt32Value(UNIT_FIELD_ICE_BLOCK) > 0)
			return SPELL_FAILED_SPELL_UNAVAILABLE;

		if (m_spellInfo->Id == XP_TRIGGER_SPELL)
		{
			if(p_caster->GetUInt32Value(PLAYER_XP_TRIGGER) < 100)
				return SPELL_FAILED_NOT_READY;
		}
		//if(p_caster->GetFFA() == 1 && ( IsTeleportSpell(m_spellInfo) || m_spellInfo-> )
		//{
		//	return SPELL_FAILED_SPELL_UNAVAILABLE;
		//}
		// if theres any spells that should be cast while dead let me know
		if( !p_caster->isAlive() )
		{
			return SPELL_FAILED_CASTER_DEAD;
		}
		/*
#ifdef COLLISION
		if (m_spellInfo->MechanicsType == MECHANIC_MOUNTED)
		{
			if (CollideInterface.IsIndoor( p_caster->GetMapIDForCollision(), p_caster->GetPositionNC() ))
				return SPELL_FAILED_NO_MOUNTS_ALLOWED;
		}
		else if( m_spellInfo->Attributes & ATTRIBUTES_ONLY_OUTDOORS )
		{
			if( !CollideInterface.IsOutdoor( p_caster->GetMapIDForCollision(), p_caster->GetPositionNC() ) )
				return SPELL_FAILED_ONLY_OUTDOORS;
		}
#endif
		*/

		// backstab/ambush
		if( m_spellInfo->NameHash == SPELL_HASH_BACKSTAB || m_spellInfo->NameHash == SPELL_HASH_AMBUSH )
		{
			if( m_spellInfo->NameHash == SPELL_HASH_AMBUSH && !p_caster->IsStealth() )
				return SPELL_FAILED_ONLY_STEALTHED;

			Item * pMainHand = p_caster->GetItemInterface()->GetInventoryItem( INVENTORY_SLOT_NOT_SET, EQUIPMENT_SLOT_MAINHAND );
			if( !pMainHand || pMainHand->GetProto()->Class != 2 || pMainHand->GetProto()->SubClass != 15 )
				return SPELL_FAILED_EQUIPPED_ITEM_CLASS_MAINHAND;
		}

		// check for cooldowns
		if(!tolerate && !p_caster->Cooldown_CanCast(m_spellInfo))
				return SPELL_FAILED_NOT_READY;

		if(p_caster->GetDuelState() == DUEL_STATE_REQUESTED)
		{
			for(i = 0; i < 3; ++i)
			{
				if( m_spellInfo->Effect[i] && m_spellInfo->Effect[i] != SPELL_EFFECT_APPLY_AURA && m_spellInfo->Effect[i] != SPELL_EFFECT_APPLY_PET_AURA
					&& m_spellInfo->Effect[i] != SPELL_EFFECT_APPLY_AREA_AURA)
				{
					return SPELL_FAILED_TARGET_DUELING;
				}
			}
		}

		if(IsTeleportSpell(m_spellInfo))
		{
			if(p_caster->IsMounted())
			{
				return SPELL_FAILED_NO_MOUNTS_ALLOWED;
			}
		}

		if(i_caster && p_caster)
		{
			int nRet = IsConsume2GiftSpell(p_caster, m_spellInfo);
			if(nRet == 1)//�����ռ䲻��
			{
				return SPELL_FAILED_NOT_ENOUGH_BAG_POS;
			}
		}

		// check for duel areas
// 		if(p_caster && m_spellInfo->Id == 7266)
// 		{
// 			AreaTable* at = AreaStorage.LookupEntry( p_caster->GetAreaID() );
// 			if(at->AreaFlags & AREA_CITY_AREA)
// 				return SPELL_FAILED_NO_DUELING;
// 		}

		// check if spell is allowed while player is on a taxi
// 		if(p_caster->m_onTaxi)
// 		{
// 			if( m_spellInfo->Id != 33836) // exception for Area 52 Special
// 				return SPELL_FAILED_NOT_ON_TAXI;
// 		}

		// check if spell is allowed while player is on a transporter
		if(p_caster->m_CurrentTransporter)
		{
			// no mounts while on transporters
			if( m_spellInfo->EffectApplyAuraName[0] == SPELL_AURA_MOUNTED || m_spellInfo->EffectApplyAuraName[1] == SPELL_AURA_MOUNTED || m_spellInfo->EffectApplyAuraName[2] == SPELL_AURA_MOUNTED)
				return SPELL_FAILED_NOT_ON_TRANSPORT;
		}

		if( p_caster->m_sunyou_instance && p_caster->m_sunyou_instance->IsValid() && p_caster->m_sunyou_instance->GetCategory() == INSTANCE_CATEGORY_TEAM_ARENA )
		{
			if( m_spellInfo->Id == 30201 ||  m_spellInfo->Id == 30202 || m_spellInfo->Id == 30203 )
				return SPELL_FAILED_NOT_IN_ARENA;
		}

		if(p_caster->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT))
		{
			if(IsShapeSpell(m_spellInfo))
			{
				return SPELL_FAILED_CANTUSE_IN_COMBAT;
			}
			if(IsMountingSpell(m_spellInfo))
			{
				return SPELL_FAILED_CANTUSE_IN_COMBAT;
			}
			if( m_spellInfo->Id == 30201 ||  m_spellInfo->Id == 30202 || m_spellInfo->Id == 30203 )
				return SPELL_FAILED_CANTUSE_IN_COMBAT;
		}
		if( IsShapeSpell(m_spellInfo) )
		{
			if( p_caster->IsMounted() )
				return SPELL_FAILED_CANTUSE_IN_MOUNT;
		}

		// check if spell is allowed while not mounted
// 		if(!p_caster->IsMounted())
// 		{
// 			if( m_spellInfo->Id == 25860) // Reindeer Transformation
// 				return SPELL_FAILED_ONLY_MOUNTED;
// 		}
// 		else
// 		{
// 			if (!(m_spellInfo->Attributes & ATTRIBUTES_MOUNT_CASTABLE))
// 				return SPELL_FAILED_NOT_MOUNTED;
// 		}

		// check if spell is allowed while shapeshifted
// 		if(p_caster->GetShapeShift())
// 		{
// 			switch(p_caster->GetShapeShift())
// 			{
// 				case FORM_TREE:
// 				case FORM_BATTLESTANCE:
// 				case FORM_DEFENSIVESTANCE:
// 				case FORM_BERSERKERSTANCE:
// 				case FORM_SHADOW:
// 				case FORM_STEALTH:
// 				case FORM_MOONKIN:
// 				{
// 					break;
// 				}
//
// 				case FORM_SWIFT:
// 				case FORM_FLIGHT:
// 				{
// 					// check if item is allowed (only special items allowed in flight forms)
// 					if(i_caster && !(i_caster->GetProto()->Flags & ITEM_FLAG_SHAPESHIFT_OK))
// 						return SPELL_FAILED_NO_ITEMS_WHILE_SHAPESHIFTED;
//
// 					break;
// 				}
//
// 				//case FORM_CAT:
// 				//case FORM_TRAVEL:
// 				//case FORM_AQUA:
// 				//case FORM_BEAR:
// 				//case FORM_AMBIENT:
// 				//case FORM_GHOUL:
// 				//case FORM_DIREBEAR:
// 				//case FORM_CREATUREBEAR:
// 				//case FORM_GHOSTWOLF:
// 				//case FORM_SPIRITOFREDEMPTION:
//
// 				default:
// 				{
// 					// check if item is allowed (only special & equipped items allowed in other forms)
// 					if(i_caster && !(i_caster->GetProto()->Flags & ITEM_FLAG_SHAPESHIFT_OK))
// 						if(i_caster->GetProto()->InventoryType == INVTYPE_NON_EQUIP)
// 							return SPELL_FAILED_NO_ITEMS_WHILE_SHAPESHIFTED;
// 				}
// 			}
// 		}

		// check if spell is allowed while we have a battleground flag
// 		if(p_caster->m_bgHasFlag)
// 		{
// 			switch(m_spellInfo->Id)
// 			{
// 				// stealth spells
// 				case 26889:
// 				{
// 					return SPELL_FAILED_SPELL_UNAVAILABLE;
// 					break;
// 				}
// 			}
// 		}

		// item spell checks
		if(i_caster && i_caster->GetProto())
		{
			if( i_caster->GetProto()->ZoneNameID && i_caster->GetProto()->ZoneNameID != i_caster->GetZoneId() )
				return SPELL_FAILED_NOT_HERE;
			if( i_caster->GetProto()->MapID && i_caster->GetProto()->MapID != i_caster->GetMapId() )
				return SPELL_FAILED_NOT_HERE;

			if(i_caster->GetProto()->Spells[0].Charges != 0)
			{
				// check if the item has the required charges
				if(i_caster->GetUInt32Value(ITEM_FIELD_SPELL_CHARGES) == 0)
					return SPELL_FAILED_NO_CHARGES_REMAIN;

				// for items that combine to create a new item, check if we have the required quantity of the item
				if(i_caster->GetProto()->ItemId == m_spellInfo->Reagent[0])
					if(p_caster->GetItemInterface()->GetItemCount(m_spellInfo->Reagent[0]) < 1 + m_spellInfo->ReagentCount[0])
						return SPELL_FAILED_ITEM_GONE;
			}
		}

		// check if we have the required reagents
		for(i=0; i<8 ;i++)
		{
			if( m_spellInfo->Reagent[i] == 0 || m_spellInfo->ReagentCount[i] == 0)
				continue;

			if(p_caster->GetItemInterface()->GetItemCount(m_spellInfo->Reagent[i]) < m_spellInfo->ReagentCount[i])
				return SPELL_FAILED_ITEM_GONE;
		}

		// check if we have the required tools, totems, etc
		if( m_spellInfo->Totem[0] != 0)
		{
			if(!p_caster->GetItemInterface()->GetItemCount(m_spellInfo->Totem[0]))
				return SPELL_FAILED_TOTEMS;
		}
		if( m_spellInfo->Totem[1] != 0)
		{
			if(!p_caster->GetItemInterface()->GetItemCount(m_spellInfo->Totem[1]))
				return SPELL_FAILED_TOTEMS;
		}

		// stealth check
		if( m_spellInfo->NameHash == SPELL_HASH_STEALTH )
		{
			if( p_caster->CombatStatus.IsInCombat() )
				return SPELL_FAILED_TARGET_IN_COMBAT;
		}

		// check if we have the required gameobject focus
		float focusRange;

		if( m_spellInfo->RequiresSpellFocus)
		{
			bool found = false;

            for(std::set<Object*>::iterator itr = p_caster->GetInRangeSetBegin(); itr != p_caster->GetInRangeSetEnd(); itr++ )
			{
				if((*itr)->GetTypeId() != TYPEID_GAMEOBJECT)
					continue;

				if((*itr)->GetUInt32Value(GAMEOBJECT_TYPE_ID) != GAMEOBJECT_TYPE_SPELL_FOCUS)
					continue;

				GameObjectInfo *info = ((GameObject*)(*itr))->GetInfo();
				if(!info)
				{
					MyLog::log->debug("Warning: could not find info about game object %u",(*itr)->GetEntry());
					continue;
				}

				// professions use rangeIndex 1, which is 0yds, so we will use 5yds, which is standard interaction range.
// 				if(info->castspell)
// 					focusRange = float(info->sound1);
// 				else
					focusRange = GetMaxRange(dbcSpellRange.LookupEntry(m_spellInfo->rangeIndex));

				// check if focus object is close enough
				if(!IsInrange(p_caster->GetPositionX(), p_caster->GetPositionY(), p_caster->GetPositionZ(), (*itr), (focusRange * focusRange)))
					continue;

				if(info->SpellFocus == m_spellInfo->RequiresSpellFocus)
				{
					found = true;
					break;
				}
			}

			if(!found)
				return SPELL_FAILED_REQUIRES_SPELL_FOCUS;
		}

		if (m_spellInfo->RequiresAreaId && m_spellInfo->RequiresAreaId != p_caster->GetMapMgr()->GetAreaID(p_caster->GetPositionX(),p_caster->GetPositionY()))
			return SPELL_FAILED_REQUIRES_AREA;

		// aurastate check
		if( m_spellInfo->CasterAuraState )
		{
			if( !p_caster->HasFlag( UNIT_FIELD_AURASTATE, m_spellInfo->CasterAuraState ) )
				return SPELL_FAILED_CASTER_AURASTATE;
		}
	}

	// Targetted Item Checks
	if(m_targets.m_itemTarget && p_caster)
	{
		Item *i_target = NULL;

		// check if the targeted item is in the trade box
		if( m_targets.m_targetMask & TARGET_FLAG_TRADE_ITEM )
		{
			switch( m_spellInfo->Effect[0] )
			{
				// only lockpicking and enchanting can target items in the trade box
				case SPELL_EFFECT_OPEN_LOCK:
				case SPELL_EFFECT_ENCHANT_ITEM:
				case SPELL_EFFECT_ENCHANT_ITEM_TEMPORARY:
				{
					// check for enchants that can only be done on your own items
					if( m_spellInfo->Flags3 & FLAGS3_ENCHANT_OWN_ONLY )
						return SPELL_FAILED_BAD_TARGETS;

					// get the player we are trading with
					Player* t_player = p_caster->GetTradeTarget();
					// get the targeted trade item
					if( t_player != NULL )
						i_target = t_player->getTradeItem((uint32)m_targets.m_itemTarget);
				}
			}
		}
		// targeted item is not in a trade box, so get our own item
		else
		{
			i_target = p_caster->GetItemInterface()->GetItemByGUID( m_targets.m_itemTarget );
		}

		// check to make sure we have a targeted item
		if( i_target == NULL )
			return SPELL_FAILED_BAD_TARGETS;

		ItemPrototype* proto = i_target->GetProto();

		// check to make sure we have it's prototype info
		if(!proto) return SPELL_FAILED_BAD_TARGETS;

		// check to make sure the targeted item is acceptable
		switch(m_spellInfo->Effect[0])
		{
			// Lock Picking Targeted Item Check
			case SPELL_EFFECT_OPEN_LOCK:
			{
				// this is currently being handled in SpellEffects
				break;
			}

			// Enchanting Targeted Item Check
			case SPELL_EFFECT_ENCHANT_ITEM:
			case SPELL_EFFECT_ENCHANT_ITEM_TEMPORARY:
			{
				// check if we have the correct class, subclass, and inventory type of target item
				if( m_spellInfo->EquippedItemClass != (int32)proto->Class)
					return SPELL_FAILED_BAD_TARGETS;

				if( m_spellInfo->EquippedItemSubClass && !(m_spellInfo->EquippedItemSubClass & (1 << proto->SubClass)))
					return SPELL_FAILED_BAD_TARGETS;

				//if( m_spellInfo->RequiredItemFlags && !(m_spellInfo->RequiredItemFlags & (1 << proto->InventoryType)))
				//	return SPELL_FAILED_BAD_TARGETS;

				/*
				if (m_spellInfo->Effect[0] == SPELL_EFFECT_ENCHANT_ITEM &&
					m_spellInfo->baseLevel && (m_spellInfo->baseLevel > proto->ItemLevel))
					return int8(SPELL_FAILED_BAD_TARGETS); // maybe there is different err code
				*/

				if( m_spellInfo->Flags3 & FLAGS3_ENCHANT_OWN_ONLY && !(i_target->IsSoulbound()))
					return SPELL_FAILED_BAD_TARGETS;

				break;
			}

			case SPELL_EFFECT_WASH_ITEM:
				{
					if( proto->Class != ITEM_CLASS_ARMOR && proto->Class != ITEM_CLASS_WEAPON )
						return SPELL_FAILED_BAD_TARGETS;
				}
				break;
			// Disenchanting Targeted Item Check
			case SPELL_EFFECT_DISENCHANT:
			{
				// check if item can be disenchanted
				if(proto->DisenchantReqSkill < 1)
					return SPELL_FAILED_CANT_BE_DISENCHANTED;

				// check if we have high enough skill
				if((int32)p_caster->_GetSkillLineCurrent(SKILL_ENCHANTING) < proto->DisenchantReqSkill)
					return SPELL_FAILED_CANT_BE_DISENCHANTED_SKILL;

				break;
			}

			// Feed Pet Targeted Item Check
			case SPELL_EFFECT_FEED_PET:
			{
				Pet *pPet = p_caster->GetSummon();

				// check if we have a pet
				if(!pPet)
					return SPELL_FAILED_NO_PET;

				// check if item is food
				if(!proto->FoodType)
					return SPELL_FAILED_BAD_TARGETS;

				// check if food type matches pets diet
				if(!(pPet->GetPetDiet() & (1 << (proto->FoodType - 1))))
					return SPELL_FAILED_WRONG_PET_FOOD;

				// check food level: food should be max 30 lvls below pets level
				if(pPet->getLevel() > proto->ItemLevel + 30)
					return SPELL_FAILED_FOOD_LOWLEVEL;

				break;
			}

			// Prospecting Targeted Item Check
			case SPELL_EFFECT_PROSPECTING:
			{
				// check if the item can be prospected
				if(!(proto->Flags & ITEM_FLAG_PROSPECTABLE))
					return SPELL_FAILED_CANT_BE_PROSPECTED;

				// check if we have at least 5 of the item
				if(p_caster->GetItemInterface()->GetItemCount(proto->ItemId) < 5)
					return SPELL_FAILED_ITEM_GONE;

				// check if we have high enough skill
				if(p_caster->_GetSkillLineCurrent(SKILL_JEWELCRAFTING) < proto->RequiredSkillRank)
					return SPELL_FAILED_LOW_CASTLEVEL;

				break;
			}
		}
	}

	// set up our max Range
	float maxRange = GetMaxRange( dbcSpellRange.LookupEntry( m_spellInfo->rangeIndex ) );
	float minRange = GetMinRange( dbcSpellRange.LookupEntry( m_spellInfo->rangeIndex ) );

	if( m_spellInfo->SpellGroupType && u_caster != NULL )
	{
		SM_FFValue( u_caster->SM_FRange, &maxRange, m_spellInfo->SpellGroupType );
		SM_PFValue( u_caster->SM_PRange, &maxRange, m_spellInfo->SpellGroupType );
	}

	// Targeted Location Checks (AoE spells)
	if( m_targets.m_targetMask == TARGET_FLAG_DEST_LOCATION )
	{
		if( !IsInrange( m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ, m_caster, ( maxRange * maxRange ) ) )
			return SPELL_FAILED_OUT_OF_RANGE;

#ifdef COLLISION
		if( !(m_spellInfo->field114 & SPELL_AVOID_MASK_COLLISION) )
			if (!CollideInterface.CheckLOS(m_caster->GetMapIDForCollision(),m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ() + 2.f, m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ + 2.f ))
				return SPELL_FAILED_LINE_OF_SIGHT;
#endif
	}

	Unit *target = NULL;

	// Targeted Unit Checks
	if(m_targets.m_unitTarget)
	{
		target = (m_caster->IsInWorld()) ? m_caster->GetMapMgr()->GetUnit(m_targets.m_unitTarget) : NULL;

		if(target)
		{
			// if the target is not the unit caster and not the masters pet
			if(target != u_caster && !m_caster->IsPet())
			{
				// Dummy spells check
// 				if( m_spellInfo->Id == 38177) //Blackwhelp Net
// 				{
// 					// should only affect Wyrmcult Blackwhelps
// 					if(target->GetEntry()!= 21387)
// 						return SPELL_FAILED_BAD_TARGETS;
// 				}

				/***********************************************************
				* Inface checks, these are checked in 2 ways
				* 1e way is check for damage type, as 3 is always ranged
				* 2e way is trough the data in the extraspell db
				*
				**********************************************************/

				/* burlex: units are always facing the target! */
				if(p_caster && /*!(m_spellInfo->buffType & SPELL_TYPE_CURSE) && */!m_spellInfo->canCastInback)
				{


					if( m_spellInfo->Spell_Dmg_Type == SPELL_DMG_TYPE_RANGED || m_spellInfo->Spell_Dmg_Type == SPELL_DMG_TYPE_MELEE)
					{ // our spell is a ranged spell
						if(!p_caster->isInFront(target))
							return SPELL_FAILED_UNIT_NOT_INFRONT;
					}
					else
					{ // our spell is not a ranged spell
						if( GetType() == SPELL_DMG_TYPE_MAGIC && m_spellInfo->in_front_status == 1 )
						{
							// must be in front
							if( isHostile( p_caster, target ) )
								if(!u_caster->isInFront(target))
									return SPELL_FAILED_UNIT_NOT_INFRONT;
						}
						else if( m_spellInfo->in_front_status == 2)
						{
							// behind
							if(target->isInFront(u_caster))
								return SPELL_FAILED_NOT_BEHIND;
						}
					}
				}
			}

			if( target->IsPlayer() && p_caster && (m_spellInfo->maxLevel & 0xFF) > 0 )
			{
				if( target->getLevel() < (m_spellInfo->maxLevel & 0xFF) )
				{
					return SPELL_FAILED_LOWLEVEL;
				}
			}
#ifdef COLLISION
			if( !(m_spellInfo->field114 & SPELL_AVOID_MASK_COLLISION) )
				if (m_caster->GetMapId() == target->GetMapId() && !CollideInterface.CheckLOS(m_caster->GetMapIDForCollision(),m_caster->GetPositionNC(),target->GetPositionNC()))
					return SPELL_FAILED_LINE_OF_SIGHT;
#endif
			// Partha: +2.52yds to max range, this matches the range the client is calculating.
			// see extra/supalosa_range_research.txt for more info

			if( tolerate ) // add an extra 33% to range on final check (squared = 1.78x)
			{
				if( !IsInrange( m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ(), target, ( ( maxRange ) * ( maxRange ) * 1.78f ) ) )
					return SPELL_FAILED_OUT_OF_RANGE;
			}
			else
			{
				if( !IsInrange( m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ(), target, ( ( maxRange ) * ( maxRange ) ) ) )
					return SPELL_FAILED_OUT_OF_RANGE;

				if( minRange > 0.001 && IsInrange( m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ(), target, ( ( minRange ) * ( minRange ) ) ) )
					return SPELL_FAILED_TOO_NEAR;
			}

			if( p_caster != NULL )
			{
				if(IsDamagingSpell(m_spellInfo) && isFriendly(p_caster, target) && !(m_spellInfo->Targets & TARGET_FLAG_SELF))
				{
					return SPELL_FAILED_TARGET_FRIENDLY;
				}
				if(IsHelpfulSpell(m_spellInfo) && !isFriendly(p_caster, target))
				{
					return SPELL_FAILED_TARGET_ENEMY;
				}				
				
				if(p_caster != target)
				if(!target || !isFriendly(p_caster, target))
				for(int i = 0; i < 3; i++)
				{
					if(m_spellInfo->Effect[i])
					if(m_spellInfo->EffectImplicitTargetA[i] == EFF_TARGET_FRIENDLY || m_spellInfo->EffectImplicitTargetA[i] == EFF_TARGET_SELF)
					{
						target = u_caster;
					}
				}

				if(p_caster == target)
				{
					if(IsShapeSpell(m_spellInfo) && p_caster->GetShapeShift())
						return SPELL_FAILED_CANTUSE_IN_SHAPESHIFT;
					if(IsMountingSpell(m_spellInfo) && (p_caster->GetShapeShift() || p_caster->m_MountSpellId))
					{
						return SPELL_FAILED_CANTUSE_IN_MOUNT;
					}
				}
				if(IsResurrectSpell(m_spellInfo))
				{
					if(!target->IsPlayer())
					{
						return SPELL_FAILED_BAD_TARGETS;
					}
					if(target->GetUInt32Value(UNIT_FIELD_HEALTH))
					{
						return SPELL_FAILED_TARGET_NOT_DEAD;
					}
				}
				if (!IsResurrectSpell(m_spellInfo) && !target->isAlive())
				{
					return SPELL_FAILED_TARGETS_DEAD;
				}

// 				if( m_spellInfo->Id == SPELL_RANGED_THROW)
// 				{
// 					Item * itm = p_caster->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_RANGED);
// 					if(!itm || itm->GetDurability() == 0)
// 						return SPELL_FAILED_NO_AMMO;
// 				}
				/*
#ifdef COLLISION
				if( !(m_spellInfo->field114 & IDC_CHECK_AVOID_RESIST) )
					if (p_caster->GetMapId() == target->GetMapId() && !CollideInterface.CheckLOS(p_caster->GetMapIDForCollision(),p_caster->GetPositionNC(),target->GetPositionNC()))
						return SPELL_FAILED_LINE_OF_SIGHT;
#endif
				*/

				// check aurastate
				if( m_spellInfo->TargetAuraState )
				{
					if( !target->HasFlag( UNIT_FIELD_AURASTATE, m_spellInfo->TargetAuraState ) )
					{
						return SPELL_FAILED_TARGET_AURASTATE;
					}
				}

				if(target->IsPlayer())
				{
					if(m_spellInfo->EffectImplicitTargetA[0] == EFF_TARGET_CREATURE)
						return SPELL_FAILED_BAD_TARGETS;
					// disallow spell casting in sanctuary zones
					// allow attacks in duels
					if( p_caster->DuelingWith != target && !isFriendly( p_caster, target ) )
					{
// 						AreaTable* atCaster = AreaStorage.LookupEntry( p_caster->GetAreaID() );
// 						AreaTable* atTarget = AreaStorage.LookupEntry( static_cast< Player* >( target )->GetAreaID() );
// 						if( atCaster->AreaFlags & 0x800 || atTarget->AreaFlags & 0x800 )
// 							return SPELL_FAILED_NOT_HERE;
					}
				}
				else
				{
					if (target->GetAIInterface()->GetIsSoulLinked() && u_caster && target->GetAIInterface()->getSoullinkedWith() != u_caster)
						return SPELL_FAILED_BAD_TARGETS;
				}

				// pet training
				if( m_spellInfo->EffectImplicitTargetA[0] == EFF_TARGET_PET &&
					m_spellInfo->Effect[0] == SPELL_EFFECT_LEARN_SPELL )
				{
					Pet *pPet = p_caster->GetSummon();
					// check if we have a pet
					if( pPet == NULL )
						return SPELL_FAILED_NO_PET;

					// other checks
					SpellEntry* trig = dbcSpell.LookupEntry( m_spellInfo->EffectTriggerSpell[0] );
					if( trig == NULL )
						return SPELL_FAILED_SPELL_UNAVAILABLE;

					uint32 status = pPet->CanLearnSpell( trig );
					if( status != 0 )
						return status;
				}

				if( m_spellInfo->EffectApplyAuraName[0]==2)//mind control
				{
					if( m_spellInfo->EffectBasePoints[0])//got level req;
					{
						if((int32)target->getLevel() > m_spellInfo->EffectBasePoints[0]+1 + int32(p_caster->getLevel() - m_spellInfo->spellLevel))
							return SPELL_FAILED_HIGHLEVEL;
						else if(target->GetTypeId() == TYPEID_UNIT)
						{
							Creature * c = (Creature*)(target);
							if (c&&c->GetCreatureName()&&c->GetCreatureName()->Rank >ELITE_ELITE)
								return SPELL_FAILED_HIGHLEVEL;
						}
					}
				}
			}

			// scripted spell stuff
// 			switch(m_spellInfo->Id)
// 			{
// 				case 30077:
// 				{
// 					if( target && target->IsCreature() && target->GetEntry()!=17226 ) // Viera Sunwhisper
// 						return SPELL_FAILED_BAD_TARGETS;
// 				}break;
// 			}

			// if target is already skinned, don't let it be skinned again
			if( m_spellInfo->Effect[0] == SPELL_EFFECT_SKINNING) // skinning
				if(target->IsUnit() && (((Creature*)target)->Skinned) )
					return SPELL_FAILED_TARGET_UNSKINNABLE;

			// all spells with target 61 need to be in group or raid
			// TODO: need to research this if its not handled by the client!!!
			if(
				m_spellInfo->EffectImplicitTargetA[0] == 61 ||
				m_spellInfo->EffectImplicitTargetA[1] == 61 ||
				m_spellInfo->EffectImplicitTargetA[2] == 61)
			{
				if( target->IsPlayer() && !static_cast< Player* >( target )->InGroup() )
					return SPELL_FAILED_NOT_READY;//return SPELL_FAILED_TARGET_NOT_IN_PARTY or SPELL_FAILED_TARGET_NOT_IN_PARTY;
			}

			// pet's owner stuff
			/*if (m_spellInfo->EffectImplicitTargetA[0] == 27 ||
				m_spellInfo->EffectImplicitTargetA[1] == 27 ||
				m_spellInfo->EffectImplicitTargetA[2] == 27)
			{
				if (!target->IsPlayer())
					return SPELL_FAILED_TARGET_NOT_PLAYER; //if you are there something is very wrong
			}*/

			// target 39 is fishing, all fishing spells are handled
			if( m_spellInfo->EffectImplicitTargetA[0] == 39 )//||
			 //m_spellInfo->EffectImplicitTargetA[1] == 39 ||
			 //m_spellInfo->EffectImplicitTargetA[2] == 39)
			{
				uint32 entry = m_spellInfo->EffectMiscValue[0];
				if(entry == GO_FISHING_BOBBER)
				{
					//uint32 mapid = p_caster->GetMapId();
					float px=u_caster->GetPositionX();
					float py=u_caster->GetPositionY();
					//float pz=u_caster->GetPositionZ();
					float orient = m_caster->GetOrientation();
					float posx = 0,posy = 0,posz = 0;
					float co = cos(orient);
					float si = sin(orient);
					MapMgr * map = m_caster->GetMapMgr();

					float r;
					for(r=20; r>10; r--)
					{
						posx = px + r * co;
						posy = py + r * si;
						/*if(!(map->GetWaterType(posx,posy) & 1))//water
							continue;*/
						posz = map->GetWaterHeight(posx,posy);
						if(posz > map->GetLandHeight(posx,posy))//water
							break;
					}
					if(r<=10)
						return SPELL_FAILED_NOT_FISHABLE;

					// if we are already fishing, dont cast it again
					if(p_caster->GetSummonedObject())
						if(p_caster->GetSummonedObject()->GetEntry() == GO_FISHING_BOBBER)
							return SPELL_FAILED_SPELL_IN_PROGRESS;
				}
			}

			if( p_caster != NULL )
			{
				if( m_spellInfo->NameHash == SPELL_HASH_GOUGE )// Gouge
					if(!target->isInFront(p_caster))
						return SPELL_FAILED_NOT_INFRONT;

				if( m_spellInfo->Category==1131)//Hammer of wrath, requires target to have 20- % of hp
				{
					if(target->GetUInt32Value(UNIT_FIELD_HEALTH) == 0)
						return SPELL_FAILED_BAD_TARGETS;

					if(target->GetUInt32Value(UNIT_FIELD_MAXHEALTH)/target->GetUInt32Value(UNIT_FIELD_HEALTH)<5)
						 return SPELL_FAILED_BAD_TARGETS;
				}
				else if( m_spellInfo->Category==672)//Conflagrate, requires immolation spell on victim
				{
					if(!target->HasAuraVisual(46))
						return SPELL_FAILED_BAD_TARGETS;
				}

				if(target->dispels[m_spellInfo->DispelType])
					return SPELL_FAILED_PREVENTED_BY_MECHANIC-1;			// hackfix - burlex

				// Removed by Supalosa and moved to 'completed cast'
				//if(target->MechanicsDispels[m_spellInfo->MechanicsType])
				//	return SPELL_FAILED_PREVENTED_BY_MECHANIC-1; // Why not just use 	SPELL_FAILED_DAMAGE_IMMUNE                                   = 144,
			}

			// if we're replacing a higher rank, deny it
			AuraCheckResponse acr = target->AuraCheck(m_spellInfo->NameHash, m_spellInfo->RankNumber,m_caster);
			if( acr.Error == AURA_CHECK_RESULT_HIGHER_BUFF_PRESENT )
				return SPELL_FAILED_AURA_BOUNCED;

			//check if we are trying to stealth or turn invisible but it is not allowed right now
			if( IsStealthSpell() || IsInvisibilitySpell() )
			{
				//if we have Faerie Fire, we cannot stealth or turn invisible
				if( u_caster->HasNegativeAuraWithNameHash( SPELL_HASH_FAERIE_FIRE ) || u_caster->HasNegativeAuraWithNameHash( SPELL_HASH_FAERIE_FIRE__FERAL_ ) )
					return SPELL_FAILED_SPELL_UNAVAILABLE;
			}
		}
	}

	// Special State Checks (for creatures & players)
	if( u_caster )
	{
		if( u_caster->SchoolCastPrevent[m_spellInfo->School] )
		{
			uint32 now_ = getMSTime();
			if( now_ > u_caster->SchoolCastPrevent[m_spellInfo->School] )//this limit has expired,remove
				u_caster->SchoolCastPrevent[m_spellInfo->School] = 0;
		}

		if( !i_caster && u_caster->m_silenced ) // can only silence non-physical
		{
			if( (u_caster->IsPlayer() && u_caster->getClass() == CLASS_BOW && this->m_spellInfo->Id == 5003)
				|| this->m_spellInfo->Id == 4 || this->m_spellInfo->Id == 5 )
				;
			else
				return SPELL_FAILED_SILENCED;
		}

		if(target) /* -Supalosa- Shouldn't this be handled on Spell Apply? */
		{
			for( int i = 0; i < 3; i++ ) // if is going to cast a spell that breaks stun remove stun auras, looks a bit hacky but is the best way i can find
			{
				if( m_spellInfo->EffectApplyAuraName[i] == SPELL_AURA_MECHANIC_IMMUNITY )
				{
					target->RemoveAllAurasByMechanic( m_spellInfo->EffectMiscValue[i] , -1 , true );
					// Remove all debuffs of that mechanic type.
					// This is also done in SpellAuras.cpp - wtf?
				}
			}
		}

		if( u_caster->IsPacified() && m_spellInfo->School == NORMAL_DAMAGE ) // only affects physical damage
		{
		}

		if( u_caster->IsStunned() || u_caster->IsFeared())
		{
		}

		if(u_caster->GetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT) > 0)
		{
			SpellEntry * t_spellInfo = (u_caster->GetCurrentSpell() ? u_caster->GetCurrentSpell()->m_spellInfo : NULL);

			if(!t_spellInfo || !m_triggeredSpell)
				return SPELL_FAILED_SPELL_IN_PROGRESS;
			else if (t_spellInfo)
			{
				if(
					t_spellInfo->EffectTriggerSpell[0] != m_spellInfo->Id &&
					t_spellInfo->EffectTriggerSpell[1] != m_spellInfo->Id &&
					t_spellInfo->EffectTriggerSpell[2] != m_spellInfo->Id)
				{
					return SPELL_FAILED_SPELL_IN_PROGRESS;
				}
			}
		}
	}

	// no problems found, so we must be ok
	return SPELL_CANCAST_OK;
}

void Spell::RemoveItems()
{
	// Item Charges & Used Item Removal
	if(i_caster && (i_caster->GetProto()->Class == ITEM_CLASS_CONSUMABLE) || i_caster->GetProto()->Class == ITEM_CLASS_RECIPE)
	{
		MyLog::yunyinglog->info("item-use-player[%u][%s] item["I64FMT"][%u] count[%u] left[%u]", i_caster->GetOwner()->GetLowGUID(), i_caster->GetOwner()->GetName(), i_caster->GetGUID(), i_caster->GetProto()->ItemId, 1, i_caster->GetUInt32Value(ITEM_FIELD_STACK_COUNT)-1);
		// Stackable Item -> remove 1 from stack
		if(i_caster->GetUInt32Value(ITEM_FIELD_STACK_COUNT) > 1)
		{
			i_caster->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, -1);
			i_caster->m_isDirty = true;
		}
		// Expendable Item
		else if(i_caster->GetProto()->Spells[0].Charges < 0
		     || i_caster->GetProto()->Spells[1].Charges == -1) // hackfix for healthstones/mana gems/depleted items
		{
			// if item has charges remaining -> remove 1 charge
			if(((int32)i_caster->GetUInt32Value(ITEM_FIELD_SPELL_CHARGES)) < -1)
			{
				i_caster->ModUnsigned32Value(ITEM_FIELD_SPELL_CHARGES, 1);
				i_caster->m_isDirty = true;
			}
			// if item has no charges remaining -> delete item
			else
			{
				i_caster->GetOwner()->GetItemInterface()->SafeFullRemoveItemByGuid(i_caster->GetGUID());
				i_caster = NULL;
			}
		}
		// Non-Expendable Item -> remove 1 charge
		else if(i_caster->GetProto()->Spells[0].Charges > 0)
		{
			i_caster->ModUnsigned32Value(ITEM_FIELD_SPELL_CHARGES, -1);
			i_caster->m_isDirty = true;
		}
		else
		{
			i_caster->GetOwner()->GetItemInterface()->SafeFullRemoveItemByGuid(i_caster->GetGUID());
			i_caster = NULL;
		}
	}

	// Ammo Removal
	if( m_spellInfo->Flags3 == FLAGS3_REQ_RANGED_WEAPON || m_spellInfo->Flags4 & FLAGS4_PLAYER_RANGED_SPELLS)
	{
		p_caster->GetItemInterface()->RemoveItemAmt_ProtectPointer(p_caster->GetUInt32Value(PLAYER_AMMO_ID), 1, &i_caster);
	}

	// Reagent Removal
	for(uint32 i=0; i<8 ;i++)
	{
		if( m_spellInfo->Reagent[i])
		{
			p_caster->GetItemInterface()->RemoveItemAmt_ProtectPointer(m_spellInfo->Reagent[i], m_spellInfo->ReagentCount[i], &i_caster);
		}
	}
}

int32 Spell::CalculateEffect(uint32 i,Unit *target)
{
	float basePointsPerLevel    = m_spellInfo->EffectRealPointsPerLevel[i];
	float randomPointsPerLevel  = m_spellInfo->EffectDicePerLevel[i];
	int32 basePoints = m_spellInfo->EffectBasePoints[i]; // + 1; removed by Nathan Gui.
	int32 randomPoints = m_spellInfo->EffectDieSides[i];
	if( m_spellInfo->Effect[i] == SPELL_EFFECT_SCHOOL_DAMAGE && u_caster )
	{
		int32 spell_flat_modifers=0;
		int32 spell_pct_modifers=0;

		SM_FIValue(u_caster->SM_FSPELL_VALUE[i],&spell_flat_modifers,m_spellInfo->SpellGroupType);
		SM_FIValue(u_caster->SM_PSPELL_VALUE[i],&spell_pct_modifers,m_spellInfo->SpellGroupType);
		basePoints += spell_flat_modifers;
		basePoints = basePoints + basePoints*(spell_pct_modifers)/100;

//		if(p_caster) 
//			damage = CalculateDamage(p_caster, target, 0, 0, m_spellInfo, RandomFloat(100)<p_caster->GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1), basePoints);
//		else if(u_caster)
		damage = CalculateDamage(u_caster, target, 0, 0, m_spellInfo, false, basePoints);
		return damage;
	}
	int32 value = 0;


	/* Random suffix value calculation */
	/*
	if(i_caster && (int32(i_caster->GetUInt32Value(ITEM_FIELD_RANDOM_PROPERTIES_ID)) < 0))
	{
        ItemRandomSuffixEntry * si = dbcItemRandomSuffix.LookupEntry(abs(int(i_caster->GetUInt32Value(ITEM_FIELD_RANDOM_PROPERTIES_ID))));
		EnchantEntry * ent;
		uint32 j,k;

		for(j = 0; j < 3; ++j)
		{
			if(si->enchantments[j] != 0)
			{
				ent = dbcEnchant.LookupEntry(si->enchantments[j]);
				for(k = 0; k < 3; ++k)
				{
					if(ent->spell[k] == m_spellInfo->Id)
					{
						if(si->prefixes[k] == 0)
							goto exit;

						value = RANDOM_SUFFIX_MAGIC_CALCULATION(si->prefixes[j], i_caster->GetItemRandomSuffixFactor());

						if(value == 0)
							goto exit;

						return value;
					}
				}
			}
		}
	}

	if( u_caster != NULL )
	{
		int32 diff = -(int32)m_spellInfo->baseLevel;
		if (m_spellInfo->maxLevel && u_caster->getLevel()>m_spellInfo->maxLevel)
			diff +=m_spellInfo->maxLevel;
		else
			diff +=u_caster->getLevel();
		randomPoints += float2int32(diff * randomPointsPerLevel);
		basePoints += float2int32(diff * basePointsPerLevel );
	}
	*/

	if(randomPoints <= 1)
		value = basePoints;
	else
		value = basePoints + rand() % randomPoints;

	if( p_caster != NULL )
	{
		SpellOverrideMap::iterator itr = p_caster->mSpellOverrideMap.find(m_spellInfo->Id);
		if(itr != p_caster->mSpellOverrideMap.end())
		{
			ScriptOverrideList::iterator itrSO;
			for(itrSO = itr->second->begin(); itrSO != itr->second->end(); ++itrSO)
			{
				//DK:FIXME->yeni bir map olu�tur
                // Capt: WHAT THE FUCK DOES THIS MEAN....
				// Supa: WHAT THE FUCK DOES THIS MEAN?
				value += RandomUInt((*itrSO)->damage);
			}
		}
	 }

	// TODO: INHERIT ITEM MODS FROM REAL ITEM OWNER - BURLEX BUT DO IT PROPERLY

	if( u_caster != NULL )
	{
		int32 spell_flat_modifers=0;
		int32 spell_pct_modifers=0;

		SM_FIValue(u_caster->SM_FSPELL_VALUE[i],&spell_flat_modifers,m_spellInfo->SpellGroupType);
		SM_FIValue(u_caster->SM_PSPELL_VALUE[i],&spell_pct_modifers,m_spellInfo->SpellGroupType);

		value += spell_flat_modifers;
		value = value + value*(spell_pct_modifers)/100;
	}
	else if( i_caster != NULL && target)
	{
		//we should inherit the modifiers from the conjured food caster
		Unit* owner = i_caster->GetOwner();//target->GetMapMgr()->GetUnit( i_caster->GetUInt64Value( ITEM_FIELD_CREATOR ) );
		if( owner != NULL )
		{
			int32 spell_flat_modifers=0;
			int32 spell_pct_modifers=0;

			SM_FIValue(owner->SM_FSPELL_VALUE[i],&spell_flat_modifers,m_spellInfo->SpellGroupType);
			SM_FIValue(owner->SM_PSPELL_VALUE[i],&spell_pct_modifers,m_spellInfo->SpellGroupType);
			value += spell_flat_modifers;
			value = value + value*(spell_pct_modifers)/100;
		}
	}


	return value;
}

void Spell::HandleTeleport(uint32 id, Unit* Target)
{
	if(Target->GetTypeId()!=TYPEID_PLAYER)
		return;

	Player* pTarget = static_cast< Player* >( Target );

	float x,y,z;
	uint32 mapid;

    // predefined behavior
	if (m_spellInfo->Id == 8690 || m_spellInfo->Id == 556 || m_spellInfo->Id == 39937)// 556 - Astral Recall ; 39937 - Ruby Slippers
	{
		x = pTarget->GetBindPositionX();
		y = pTarget->GetBindPositionY();
		z = pTarget->GetBindPositionZ();
		mapid = pTarget->GetBindMapId();
	}
	else // normal behavior
	{
		TeleportCoords* TC = TeleportCoordStorage.LookupEntry(id);
		if(!TC)
			return;

		x=TC->x;
		y=TC->y;
		z=TC->z;
		mapid=TC->mapId;
	}

	pTarget->EventAttackStop();
	pTarget->SetSelection(0);

	// We use a teleport event on this one. Reason being because of UpdateCellActivity,
	// the game object set of the updater thread WILL Get messed up if we teleport from a gameobject
	// caster.
	if(!sEventMgr.HasEvent(pTarget, EVENT_PLAYER_TELEPORT))
		sEventMgr.AddEvent(pTarget, &Player::EventTeleport, mapid, x, y, z, EVENT_PLAYER_TELEPORT, 1, 1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
}

void Spell::CreateItem(uint32 itemId)
{
    if( !itemId )
        return;

	Player*			pUnit = static_cast< Player* >( m_caster );
	Item*			newItem;
	Item*			add;
	SlotResult		slotresult;
	ItemPrototype*	m_itemProto;

	m_itemProto = ItemPrototypeStorage.LookupEntry( itemId );
	if( m_itemProto == NULL )
	    return;

	if (pUnit->GetItemInterface()->CanReceiveItem(m_itemProto, 1))
	{
		SendCastResult(SPELL_FAILED_TOO_MANY_OF_ITEM);
		return;
	}

	add = pUnit->GetItemInterface()->FindItemLessMax(itemId, 1, false);
	if (!add)
	{
		slotresult = pUnit->GetItemInterface()->FindFreeInventorySlot(m_itemProto);
		if(!slotresult.Result)
		{
			 SendCastResult(SPELL_FAILED_TOO_MANY_OF_ITEM);
			 return;
		}

		newItem = objmgr.CreateItem(itemId, pUnit);
		AddItemResult result = pUnit->GetItemInterface()->SafeAddItem(newItem, slotresult.ContainerSlot, slotresult.Slot);
		if(!result)
		{
			delete newItem;
			return;
		}

		newItem->SetUInt64Value(ITEM_FIELD_CREATOR,m_caster->GetGUID());
		newItem->SetUInt32Value(ITEM_FIELD_STACK_COUNT, damage);

		/*WorldPacket data(45);
		p_caster->GetSession()->BuildItemPushResult(&data, p_caster->GetGUID(), 1, 1, itemId ,0,0xFF,1,0xFFFFFFFF);
		p_caster->SendMessageToSet(&data, true);*/
		p_caster->GetSession()->SendItemPushResult(newItem,true,false,true,true,slotresult.ContainerSlot,slotresult.Slot,damage);
		newItem->m_isDirty = true;

	}
	else
	{
		add->SetUInt32Value(ITEM_FIELD_STACK_COUNT,add->GetUInt32Value(ITEM_FIELD_STACK_COUNT) + damage);
		/*WorldPacket data(45);
		p_caster->GetSession()->BuildItemPushResult(&data, p_caster->GetGUID(), 1, 1, itemId ,0,0xFF,1,0xFFFFFFFF);
		p_caster->SendMessageToSet(&data, true);*/
		p_caster->GetSession()->SendItemPushResult(add,true,false,true,false,p_caster->GetItemInterface()->GetBagSlotByGuid(add->GetGUID()),0xFFFFFFFF,damage);
		add->m_isDirty = true;
	}
}

/*void Spell::_DamageRangeUpdate()
{
	if(unitTarget)
	{
		if(unitTarget->isAlive())
		{
			m_caster->SpellNonMeleeDamageLog(unitTarget,m_spellInfo->Id, damageToHit);
		}
		else
		{	if( u_caster != NULL )
			if(u_caster->GetCurrentSpell() != this)
			{
					if(u_caster->GetCurrentSpell() != NULL)
					{
						u_caster->GetCurrentSpell()->SendChannelUpdate(0);
						u_caster->GetCurrentSpell()->cancel();
					}
			}
			SendChannelUpdate(0);
			cancel();
		}
		sEventMgr.RemoveEvents(this, EVENT_SPELL_DAMAGE_HIT);
		delete this;
	}
	else if(gameObjTarget)
	{
		sEventMgr.RemoveEvents(this, EVENT_SPELL_DAMAGE_HIT);
		delete this;
		//Go Support
	}
	else
	{
		sEventMgr.RemoveEvents(this, EVENT_SPELL_DAMAGE_HIT);
		delete this;
	}
}*/

void Spell::SendHealSpellOnPlayer(Object* caster, Object* target, uint32 dmg,bool critical)
{
	if(!caster || !target)
		return;

	MSG_S2C::stSpell_Heal_On_Player Msg;
	Msg.target_guid	= target->GetNewGUID();
	Msg.caster_guid	= caster->GetNewGUID();
	Msg.spell_id	= pSpellId ? pSpellId : m_spellInfo->Id;
	Msg.dmg			= dmg;
	Msg.critical	= critical;
	Msg.damageseq	= m_curattackcnt;
	caster->SendMessageToSet(Msg, true);
}

void Spell::SendHealManaSpellOnPlayer(Object * caster, Object * target, uint32 dmg, uint32 powertype)
{
	if(!caster || !target)
		return;

	MSG_S2C::stSpell_HealMana_On_Player Msg;
	Msg.caster_guid = caster->GetNewGUID();
	Msg.target_guid = target->GetNewGUID();
	Msg.spell_id	= (pSpellId ? pSpellId : m_spellInfo->Id);
	Msg.power_type	= powertype;
	Msg.dmg			= dmg;
	Msg.damageseq	= m_curattackcnt;
	caster->SendMessageToSet(Msg, true);
}

void Spell::Heal(int32 amount, bool ForceCrit, float jump_reduce)
{
	int32 base_amount = amount; //store base_amount for later use

	if(!unitTarget || !unitTarget->isAlive())
		return;

	if( unitTarget->IsCreature() && ((Creature*)unitTarget)->IsImmuneHeal() )
		return;

	if( p_caster != NULL )
		p_caster->last_heal_spell=m_spellInfo;


	int res = u_caster->GetHealSpellBonus(unitTarget, m_spellInfo,  amount, false, jump_reduce);
	amount += res;
    //self healing shouldn't flag himself
	if(p_caster && playerTarget && p_caster != playerTarget)
	{
		// Healing a flagged target will flag you.
		if(playerTarget->IsPvPFlagged())
			p_caster->SetPvPFlag();
	}

	//Make it critical
	bool critical = false;
	float critchance = 0;
	int32 bonus = 0;
	float healdoneaffectperc = 1.0f;
	if( u_caster != NULL )
	{
		//Downranking
		//if(p_caster && p_caster->IsPlayer() && m_spellInfo->baseLevel > 0 && m_spellInfo->maxLevel > 0)
		//{
		//	float downrank1 = 1.0f;
		//	if (m_spellInfo->baseLevel < 20)
		//		downrank1 = 1.0f - (20.0f - float (m_spellInfo->baseLevel) ) * 0.0375f;
		//	float downrank2 = ( float(m_spellInfo->maxLevel + 5.0f) / float(p_caster->getLevel()) );
		//	if (downrank2 >= 1 || downrank2 < 0)
		//		downrank2 = 1.0f;
		//	healdoneaffectperc *= downrank1 * downrank2;
		//}

		////Spells Not affected by Bonus Healing
		//if(m_spellInfo->NameHash == SPELL_HASH_SEAL_OF_LIGHT) //Seal of Light
		//	healdoneaffectperc = 0.0f;

		//if(m_spellInfo->NameHash == SPELL_HASH_LESSER_HEROISM) //Lesser Heroism
		//	healdoneaffectperc = 0.0f;

		////Spells affected by Bonus Healing
		//if(m_spellInfo->NameHash == SPELL_HASH_EARTH_SHIELD) //Earth Shield
		//	healdoneaffectperc = 0.3f;

		//Basic bonus
		//bonus += u_caster->HealDoneMod[m_spellInfo->School];
		//bonus += unitTarget->HealTakenMod[m_spellInfo->School];

		////Bonus from Intellect & Spirit
		//if( p_caster != NULL )
		//{
		//	for(uint32 a = 0; a < 6; a++)
		//		bonus += float2int32(p_caster->SpellHealDoneByAttribute[a][m_spellInfo->School] * p_caster->GetFloatValue(UNIT_FIELD_STAT0 + a));
		//}

		////Spell Coefficient
		//if(  m_spellInfo->Dspell_coef_override >= 0 ) //In case we have forced coefficients
		//	bonus = float2int32( float( bonus ) * m_spellInfo->Dspell_coef_override );
		//else
		//{
		//	//Bonus to DH part
		//	if( m_spellInfo->fixed_dddhcoef >= 0 )
		//		bonus = float2int32( float( bonus ) * m_spellInfo->fixed_dddhcoef );
		//}
		if (p_caster)
		{
			if (m_spellInfo->School)
			{
				critchance = p_caster->GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1) + 
					p_caster->GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1 + m_spellInfo->School) + p_caster->SpellCritChanceSchool[m_spellInfo->School];
			}
			else
			{
				critchance = p_caster->GetFloatValue(PLAYER_CRIT_PERCENTAGE);
			}

			if( m_spellInfo->SpellGroupType )
				SM_FFValue( p_caster->SM_CriticalChance, &critchance, m_spellInfo->SpellGroupType );

		}

		critical = RandomFloat( 100.f ) < critchance;
		if( critical || ForceCrit )
		{
			float critical_bonus = 2.f;
			if( m_spellInfo->SpellGroupType )
				SM_PFValue( u_caster->SM_PCriticalDamage, &critical_bonus, m_spellInfo->SpellGroupType );

			res *= critical_bonus;

			unitTarget->HandleProc(PROC_ON_SPELL_CRIT_HIT_VICTIM, u_caster, m_spellInfo, amount);
			u_caster->HandleProc(PROC_ON_SPELL_CRIT_HIT, unitTarget, m_spellInfo, amount);
		}
	}

	if(amount <= 0)
		amount = 1;

	if( p_caster != NULL )
	{
		SendHealSpellOnPlayer( p_caster,  unitTarget, amount, critical );
		
		p_caster->m_bgScore.HealingDone += amount;
	}
	uint32 curHealth = unitTarget->GetUInt32Value(UNIT_FIELD_HEALTH);
	uint32 maxHealth = unitTarget->GetUInt32Value(UNIT_FIELD_MAXHEALTH);
	int delta = 0;
	if((curHealth + amount) >= maxHealth)
	{
		unitTarget->SetUInt32Value(UNIT_FIELD_HEALTH, maxHealth);
		delta = maxHealth - curHealth;
	}
	else
	{
		unitTarget->ModUnsigned32Value(UNIT_FIELD_HEALTH, amount);
		delta = amount;
	}

	if (p_caster)
	{
		p_caster->m_casted_amount[m_spellInfo->School]=amount;
		p_caster->HandleProc( PROC_ON_CAST_SPECIFIC_SPELL | PROC_ON_CAST_SPELL, unitTarget, m_spellInfo );
	}

	int doneTarget = 0;

	// add threat
	if( u_caster != NULL )
	{
		//preventing overheal ;)
		if( (curHealth + base_amount) >= maxHealth )
			base_amount = maxHealth - curHealth;

		uint32 base_threat=GetBaseThreat(base_amount);
		int count = 0;
		Unit *unit;
		std::vector<Unit*> target_threat;
		if(base_threat)
		{
			/*
			http://www.wowwiki.com/Threat
			Healing threat is global, and is normally .5x of the amount healed.
			Healing effects cause no threat if the target is already at full health.

			Example: Player 1 is involved in combat with 5 mobs. Player 2 (priest) heals Player 1 for 1000 health,
			and has no threat reduction talents. A 1000 heal generates 500 threat,
			however that 500 threat is split amongst the 5 mobs.
			Each of the 5 mobs now has 100 threat towards Player 2.
			*/

			target_threat.reserve(u_caster->GetInRangeCount()); // this helps speed

			for(std::set<Object*>::iterator itr = u_caster->GetInRangeSetBegin(); itr != u_caster->GetInRangeSetEnd(); ++itr)
			{
				if((*itr)->GetTypeId() != TYPEID_UNIT)
					continue;
				unit = static_cast<Unit*>((*itr));
				//if(unit->GetAIInterface()->GetNextTarget() == unitTarget)
				if( unit->GetAIInterface()->getThreatByPtr( unitTarget ) )
				{
					target_threat.push_back(unit);
					++count;
				}
			}
			count = ( count == 0 ? 1 : count );  // division against 0 protection

			// every unit on threatlist should get 1/2 the threat, divided by size of list
			int32 threat = base_threat / (count * 2);

			SM_FIValue( u_caster->SM_FThreatReduce, &threat, m_spellInfo->SpellGroupType );
			SM_PIValue( u_caster->SM_PThreatReduce, &threat, m_spellInfo->SpellGroupType );

			if( threat > 0 )
			{
				// update threatlist (HealReaction)
				for(std::vector<Unit*>::iterator itr = target_threat.begin(); itr != target_threat.end(); ++itr)
				{
					// for now we'll just use heal amount as threat.. we'll prolly need a formula though
					static_cast< Unit* >( *itr )->GetAIInterface()->HealReaction( u_caster, unitTarget, threat );

					if( (*itr)->GetGUID() == u_caster->CombatStatus.GetPrimaryAttackTarget() )
						doneTarget = 1;
				}
			}
		}
		// remember that we healed (for combat status)
		if(unitTarget->IsInWorld() && u_caster->IsInWorld())
			u_caster->CombatStatus.WeHealed(unitTarget, delta);
	}
}

void Spell::DetermineSkillUp(uint32 skillid,uint32 targetlevel)
{
	if(!p_caster)return;
	if(p_caster->GetSkillUpChance(skillid)<0.01)return;//to preven getting higher skill than max
	int32 diff=p_caster->_GetSkillLineCurrent(skillid,false)/5-targetlevel;
	if(diff<0)diff=-diff;
	float chance;
	if(diff<=5)chance=95.0;
	else if(diff<=10)chance=66;
	else if(diff <=15)chance=33;
	else return;
	if(Rand(chance*sWorld.getRate(RATE_SKILLCHANCE)))
		p_caster->_AdvanceSkillLine(skillid, float2int32( 1.0f * sWorld.getRate(RATE_SKILLRATE)));
}

void Spell::DetermineSkillUp(uint32 skillid)
{
	//This code is wrong for creating items and disenchanting.
	if(!p_caster)return;
	float chance = 0.0f;
	skilllinespell* skill = objmgr.GetSpellSkill(m_spellInfo->Id);
	if( skill != NULL && static_cast< Player* >( m_caster )->_HasSkillLine( skill->skilline ) )
	{
		uint32 amt = static_cast< Player* >( m_caster )->_GetSkillLineCurrent( skill->skilline, false );
		uint32 max = static_cast< Player* >( m_caster )->_GetSkillLineMax( skill->skilline );
		if( amt >= max )
			return;
		if( amt >= skill->grey ) //grey
			chance = 0.0f;
		else if( ( amt >= ( ( ( skill->grey - skill->green) / 2 ) + skill->green ) ) ) //green
			chance = 33.0f;
		else if( amt >= skill->green ) //yellow
			chance = 66.0f;
		else //brown
			chance=100.0f;
	}
	if(Rand(chance*sWorld.getRate(RATE_SKILLCHANCE)))
		p_caster->_AdvanceSkillLine(skillid, float2int32( 1.0f * sWorld.getRate(RATE_SKILLRATE)));
}

void Spell::SafeAddTarget(TargetsList* tgt,uint64 guid)
{
	if(guid == 0)
		return;

	for(TargetsList::iterator i=tgt->begin();i!=tgt->end();i++)
		if((*i)==guid)
			return;

	tgt->push_back(guid);
}

void Spell::SafeAddMissedTarget(uint64 guid)
{
    for(SpellTargetsList::iterator i=ModeratedTargets.begin();i!=ModeratedTargets.end();i++)
        if((*i).TargetGuid==guid)
        {
            //MyLog::log->debug("[SPELL] Something goes wrong in spell target system");
			// this isnt actually wrong, since we only have one missed target map,
			// whereas hit targets have multiple maps per effect.
            return;
        }

    ModeratedTargets.push_back(SpellTargetMod(guid,2));
	if (!m_SetModeratedTargets.count(guid))
	{
		m_SetModeratedTargets.insert(guid);
	}
}

void Spell::SafeAddModeratedTarget(uint64 guid, uint16 type)
{
	for(SpellTargetsList::iterator i=ModeratedTargets.begin();i!=ModeratedTargets.end();i++)
		if((*i).TargetGuid==guid)
        {
            //MyLog::log->debug("[SPELL] Something goes wrong in spell target system");
			// this isnt actually wrong, since we only have one missed target map,
			// whereas hit targets have multiple maps per effect.
			return;
        }

	ModeratedTargets.push_back(SpellTargetMod(guid, (uint8)type));
	if (!m_SetModeratedTargets.count(guid))
	{
		m_SetModeratedTargets.insert(guid);
	}
}

bool Spell::Reflect(Unit *refunit)
{
	SpellEntry * refspell = NULL;

	if( m_reflectedParent != NULL )
		return false;

	// if the spell to reflect is a reflect spell, do nothing.
	for(int i=0; i<3; i++)
    {
		if( m_spellInfo->Effect[i] == 6 && (m_spellInfo->EffectApplyAuraName[i] == 74 || m_spellInfo->EffectApplyAuraName[i] == 28))
			return false;
    }
	for(std::list<struct ReflectSpellSchool*>::iterator i = refunit->m_reflectSpellSchool.begin();i != refunit->m_reflectSpellSchool.end();i++)
	{
		if((*i)->school == -1 || (*i)->school == (int32)m_spellInfo->School)
		{
			if(Rand((float)(*i)->chance))
			{
				//the god blessed special case : mage - Frost Warding = is an augmentation to frost warding
				if((*i)->require_aura_hash && u_caster && !u_caster->HasAurasWithNameHash((*i)->require_aura_hash))
                {
					continue;
                }
				refspell = m_spellInfo;
			}
		}
	}

	if(!refspell || m_caster == refunit) return false;

	Spell *spell = new Spell(refunit, refspell, true, NULL);
	SpellCastTargets targets;
	targets.m_unitTarget = m_caster->GetGUID();
	spell->prepare(&targets);
	return true;
}

void ApplyDiminishingReturnTimer(uint32 * Duration, Unit * Target, SpellEntry * spell)
{
	uint32 status = GetDiminishingGroup(spell->NameHash);
	uint32 Grp = status & 0xFFFF;   // other bytes are if apply to pvp
	uint32 PvE = (status >> 16) & 0xFFFF;

	// Make sure we have a group
	if(Grp == 0xFFFF) return;

	// Check if we don't apply to pve
	if(!PvE && Target->GetTypeId() != TYPEID_PLAYER && !Target->IsPet())
		return;

	// TODO: check for spells that should do this
	float Dur = float(*Duration);

	switch(Target->m_diminishCount[Grp])
	{
	case 0: // Full effect
		if ( ( Target->IsPlayer() || Target->IsPet() ) && Dur > 10000)
		{
			Dur = 10000;
		}
		break;

	case 1: // Reduced by 50%
		Dur *= 0.5f;
		if ( ( Target->IsPlayer() || Target->IsPet() ) && Dur > 5000)
		{
			Dur = 5000;
		}
		break;

	case 2: // Reduced by 75%
		Dur *= 0.25f;
		if ( ( Target->IsPlayer() || Target->IsPet() ) && Dur > 2500)
		{
			Dur = 2500;
		}
		break;

	default:// Target immune to spell
		{
			*Duration = 0;
			return;
		}break;
	}

	// Convert back
	*Duration = (ui32)(Dur);

	// Reset the diminishing return counter, and add to the aura count (we don't decrease the timer till we
	// have no auras of this type left.
	++Target->m_diminishAuraCount[Grp];
	++Target->m_diminishCount[Grp];
}

void UnapplyDiminishingReturnTimer(Unit * Target, SpellEntry * spell)
{
	uint32 status = GetDiminishingGroup(spell->NameHash);
	uint32 Grp = status & 0xFFFF;   // other bytes are if apply to pvp
	uint32 PvE = (status >> 16) & 0xFFFF;

	// Make sure we have a group
	if(Grp == 0xFFFF) return;

	// Check if we don't apply to pve
	if(!PvE && Target->GetTypeId() != TYPEID_PLAYER && !Target->IsPet())
		return;

	Target->m_diminishAuraCount[Grp]--;

	// start timer decrease
	if(!Target->m_diminishAuraCount[Grp])
	{
		Target->m_diminishActive = true;
		Target->m_diminishTimer[Grp] = 15000;
	}
}

/// Calculate the Diminishing Group. This is based on a name hash.
/// this off course is very hacky, but as its made done in a proper way
/// I leave it here.
uint32 GetDiminishingGroup(uint32 NameHash)
{
	int32 grp = -1;
	bool pve = false;

	switch(NameHash)
	{
	case SPELL_HASH_SAP:					// Sap
	case SPELL_HASH_GOUGE:					// Gouge
		grp = 0;
		break;

	case SPELL_HASH_CHEAP_SHOT:				// Cheap Shot
		{
			grp = 1;
			pve = true;
		}break;

	case SPELL_HASH_KIDNEY_SHOT:			// Kidney Shot
		{
			grp = 2;
			pve = true;
		}break;

	case SPELL_HASH_BASH:					// Bash
		grp = 3;
		break;

	case SPELL_HASH_ENTANGLING_ROOTS:		// Entangling Roots
		grp = 4;
		break;

	case SPELL_HASH_HAMMER_OF_JUSTICE:		// Hammer of Justice
		{
			grp = 5;
			pve = true;
		}break;

	case SPELL_HASH_STUN:					// Stuns (all of them)
		grp = 6;
		break;

	case SPELL_HASH_CHARGE:					// Charge
	case SPELL_HASH_INTERCEPT :				// Intercept
	case SPELL_HASH_CONCUSSION_BLOW:		// Concussion Blow
		{
			grp = 7;
			pve = true;
		}break;

	case SPELL_HASH_FEAR:					// Fear
	case SPELL_HASH_SEDUCTION:				// Seduction
	case SPELL_HASH_HOWL_OF_TERROR:			// Howl of Terror
		grp = 8;
		break;

	case SPELL_HASH_FROST_NOVA:				// Frost Nova
		grp = 9;
		break;

	case SPELL_HASH_POLYMORPH:				// Polymorph
	case SPELL_HASH_POLYMORPH__CHICKEN:		// Chicken
	case SPELL_HASH_POLYMORPH__PIG:			// Pig
	case SPELL_HASH_POLYMORPH__TURTLE:		// Turtle
	case SPELL_HASH_POLYMORPH__SHEEP:		// Good ol' sheep
		{
			grp = 10;
			pve = true;
		}break;

	case SPELL_HASH_PSYCHIC_SCREAM:			// Psychic Scream
		grp = 11;
		break;

	case SPELL_HASH_MIND_CONTROL:			// Mind Control
		grp = 12;
		break;

	//case SPELL_HASH_FROST_SHOCK:			// Frost Shock
		//grp = 13;
		//break;

	case SPELL_HASH_HIBERNATE:				// Hibernate
		grp = 14;
		break;

	case SPELL_HASH_CYCLONE:				// Cyclone
	case SPELL_HASH_BLIND:					// Blind
		{
			grp = 15;
			pve = true;
		}break;

	case SPELL_HASH_CELESTIAL_FOCUS:		// Celestial Focus
		{
			grp = 16;
			pve = true;
		}break;

	case SPELL_HASH_IMPACT:					// Impact
		{
			grp = 17;
			pve = true;
		}break;

	case SPELL_HASH_BLACKOUT:				// Blackout
		{
			grp = 18;
			pve = true;
		}break;

	case SPELL_HASH_BANISH:					// Banish
		grp = 19;
		break;

	case SPELL_HASH_FREEZING_TRAP_EFFECT:	// Freezing Trap Effect
		grp = 20;
		break;

	case SPELL_HASH_SCARE_BEAST:			// Scare Beast
		grp = 21;
		break;

	case SPELL_HASH_ENSLAVE_DEMON:			// Enslave Demon
		grp = 22;
		break;
	case SPELL_HASH_SLEEP:					// Sleep
	case SPELL_HASH_RECKLESS_CHARGE:		// Reckless Charge
		grp = 23;
		break;
	case SPELL_HASH_RIPOSTE:
		grp = 24;
		break;
	}
	uint32 ret;
	if( pve )
		ret = grp | (1 << 16);
	else
		ret = grp;

	return ret;
}

void Spell::SendCastSuccess(Object * target)
{
	Player * plr = p_caster;
	if(!plr && u_caster)
		plr = u_caster->m_redirectSpellPackets;
	if(!plr||!plr->IsPlayer())
		return;

	MSG_S2C::stSpell_Target_Cast_Result Msg;
	Msg.target_guid	= (target != 0) ? target->GetNewGUID() : 0;
	Msg.spell_id	= m_spellInfo->Id;
	plr->GetSession()->SendPacket(Msg);
}

void Spell::SendCastSuccess(const uint64& guid)
{
	Player * plr = p_caster;
	if(!plr && u_caster)
		plr = u_caster->m_redirectSpellPackets;
	if(!plr || !plr->IsPlayer())
		return;

	MSG_S2C::stSpell_Target_Cast_Result Msg;
	Msg.target_guid	= guid;
	Msg.spell_id	= m_spellInfo->Id;
}

uint32 Spell::GetDuration()
{
	if(bDurSet)return Dur;
	bDurSet=true;
	int32 c_dur = 0;

	if(m_spellInfo->DurationIndex)
	{
		SpellDuration *sd=dbcSpellDuration.LookupEntry(m_spellInfo->DurationIndex);
		if(sd)
		{
			//check for negative and 0 durations.
			//duration affected by level
			if((int32)sd->Duration1 < 0 && sd->Duration2 && u_caster)
			{
				this->Dur = uint32(((int32)sd->Duration1 + (sd->Duration2 * u_caster->getLevel())));
				if((int32)this->Dur > 0 && sd->Duration3 > 0 && (int32)this->Dur > (int32)sd->Duration3)
				{
					this->Dur = sd->Duration3;
				}

				if((int32)this->Dur < 0)
					this->Dur = 0;
				c_dur = this->Dur;
			}
			if(sd->Duration1 >= 0 && !c_dur)
			{
				this->Dur = sd->Duration1;
			}

			if(m_spellInfo->SpellGroupType && u_caster)
			{
				SM_FIValue(u_caster->SM_FDur,(int32*)&this->Dur,m_spellInfo->SpellGroupType);
				SM_PIValue(u_caster->SM_PDur,(int32*)&this->Dur,m_spellInfo->SpellGroupType);
			}
		}
		else
		{
			this->Dur = (uint32)-1;
		}
	}
	else
	{
		this->Dur = (uint32)-1;
	}

	return this->Dur;
}
/*
bool IsBeneficSpell(SpellEntry *sp)
{
	uint32 cur;
	for(uint32 i=0;i<3;i++)
		for(uint32 j=0;j<2;j++)
		{
			if(j==0)
				cur = sp->EffectImplicitTargetA[i];
			else // if(j==1)
				cur = sp->EffectImplicitTargetB[i];
			switch(cur)
			{
				case EFF_TARGET_SELF:
				case EFF_TARGET_PET:
				case EFF_TARGET_ALL_PARTY_AROUND_CASTER:
				case EFF_TARGET_SINGLE_FRIEND:
				case 45:// Chain,!!only for healing!! for chain lightning =6
				case 57:// Targeted Party Member
				case 27: // target is owner of pet
				case EFF_TARGET_MINION:// Minion Target
				case 33://Party members of totem, inside given range
				case EFF_TARGET_SINGLE_PARTY:// Single Target Party Member
				case EFF_TARGET_ALL_PARTY: // all Members of the targets party
				case EFF_TARGET_SELF_FISHING://Fishing
				case 46://Unknown Summon Atal'ai Skeleton
				case 47:// Portal
				case 52:	// Lightwells, etc
				case 40://Activate Object target(probably based on focus)
				case EFF_TARGET_TOTEM_EARTH:
				case EFF_TARGET_TOTEM_WATER:
				case EFF_TARGET_TOTEM_AIR:
				case EFF_TARGET_TOTEM_FIRE:// Totem
				case 61: // targets with the same group/raid and the same class
				case 32:
				case 73:
					return true;
			}//end switch
		}//end for
	return false;
}

AI_SpellTargetType RecommandAISpellTargetType(SpellEntry *sp)
{
	uint32 cur;
	for(uint32 i=0;i<3;i++)
		for(uint32 j=0;j<2;j++)
		{
			if(j==0)
				cur = sp->EffectImplicitTargetA[i];
			else // if(j==1)
				cur = sp->EffectImplicitTargetB[i];
			switch(cur)
			{
				case EFF_TARGET_NONE:
				case EFF_TARGET_GAMEOBJECT:
				case EFF_TARGET_GAMEOBJECT_ITEM:// Gameobject/Item Target
				case EFF_TARGET_SELF_FISHING://Fishing
				case 47:// Portal
				case 52:	// Lightwells, etc
				case 40://Activate Object target(probably based on focus)
					return TTYPE_NULL;

				case EFF_TARGET_SELF:
				case 38://Dummy Target
				case 32:
				case 73:
					return TTYPE_CASTER;

				case EFF_TARGET_ALL_ENEMY_IN_AREA: // All Enemies in Area of Effect (TEST)
				case EFF_TARGET_ALL_ENEMY_IN_AREA_INSTANT: // All Enemies in Area of Effect instant (e.g. Flamestrike)
				case EFF_TARGET_ALL_ENEMY_IN_AREA_CHANNELED:// All Enemies in Area of Effect(Blizzard/Rain of Fire/volley) channeled
					return TTYPE_DESTINATION;

				case 4: // dono related to "Wandering Plague", "Spirit Steal", "Contagion of Rot", "Retching Plague" and "Copy of Wandering Plague"
				case EFF_TARGET_PET:
				case EFF_TARGET_SINGLE_ENEMY:// Single Target Enemy
				case 77:					// grep: i think this fits
				case 8: // related to Chess Move (DND), Firecrackers, Spotlight, aedm, Spice Mortar
				case EFF_TARGET_IN_FRONT_OF_CASTER:
				case 31:// related to scripted effects
				case 53:// Target Area by Players CurrentSelection()
				case 54:// Targets in Front of the Caster
				case EFF_TARGET_ALL_PARTY_AROUND_CASTER:
				case EFF_TARGET_SINGLE_FRIEND:
				case 45:// Chain,!!only for healing!! for chain lightning =6
				case 57:// Targeted Party Member
				case EFF_TARGET_DUEL:
				case 27: // target is owner of pet
				case EFF_TARGET_MINION:// Minion Target
				case 33://Party members of totem, inside given range
				case EFF_TARGET_SINGLE_PARTY:// Single Target Party Member
				case EFF_TARGET_ALL_PARTY: // all Members of the targets party
				case EFF_TARGET_TOTEM_EARTH:
				case EFF_TARGET_TOTEM_WATER:
				case EFF_TARGET_TOTEM_AIR:
				case EFF_TARGET_TOTEM_FIRE:// Totem
				case 61: // targets with the same group/raid and the same class
					return TTYPE_SINGLETARGET;

					// spells like 17278:Cannon Fire and 21117:Summon Son of Flame A
				case 17: // A single target at a xyz location or the target is a possition xyz
				case 18:// Land under caster.Maybe not correct
				case 46://Unknown Summon Atal'ai Skeleton
				case EFF_TARGET_ALL_ENEMIES_AROUND_CASTER:
					return TTYPE_SOURCE;

			}//end switch
		}//end for
	return TTYPE_NULL;// this means a new spell :P
}
*/


bool IsShapeSpell(SpellEntry* sp)
{
	for(int i = 0; i < 3; i++)
	{
		if( sp->Effect[i] == SPELL_EFFECT_APPLY_AURA ||
			sp->Effect[i] == SPELL_EFFECT_APPLY_AREA_AURA )
		{
			switch( sp->EffectApplyAuraName[i] )
			{
			case SPELL_AURA_SHAPESHIFT_FIGHT_NOMOUNT:
			case SPELL_AURA_SHAPESHIFT_NOFIGHT_NOMOUNT:
			case SPELL_AURA_SHAPESHIFT_FIGHT_MOUNT:
			case SPELL_AURA_SHAPESHIFT_NOFIGHT_MOUNT:
				return true;
			default: break;
			}
		}
	}

	return false;
}

bool IsExtraXPSpell(SpellEntry* sp)
{
	for(int i = 0; i < 3; i++)
	{
		if( sp->Effect[i] == SPELL_EFFECT_APPLY_AURA ||
			sp->Effect[i] == SPELL_EFFECT_APPLY_AREA_AURA )
		{
			switch( sp->EffectApplyAuraName[i] )
			{
			case SPELL_AURA_EXTRA_XP:
				return true;
			default: break;
			}
		}
	}

	return false;
}

bool IsLuoPanShi(SpellEntry* sp)
{
	for(int i = 0; i < 3; i++)
	{
		if(sp->Effect[i] == SPELL_EFFECT_RECORDPOS)
		{
			return true;
		}
	}
	return false;
}

bool IsImmunitySpell(SpellEntry* sp, uint32& miscType, uint32& cnt)
{
	for(int i = 0; i < 3; i++)
	{
		if( sp->Effect[i] == SPELL_EFFECT_APPLY_AURA ||
			sp->Effect[i] == SPELL_EFFECT_APPLY_AREA_AURA )
		{
			switch( sp->EffectApplyAuraName[i] )
			{
			case SPELL_AURA_MECHANIC_IMMUNITY:
				miscType = sp->EffectMiscValue[i];
				cnt = sp->EffectBasePoints[i];
				return true;
			default: break;
			}
		}
	}

	return false;
}

bool IsMountingSpell(SpellEntry* sp)
{
	for(int i = 0; i < 3; i++)
	{
		if( sp->Effect[i] == SPELL_EFFECT_APPLY_AURA ||
			sp->Effect[i] == SPELL_EFFECT_APPLY_AREA_AURA )
		{
			switch( sp->EffectApplyAuraName[i] )
			{
			case SPELL_AURA_SHAPESHIFT_FIGHT_NOMOUNT:
			case SPELL_AURA_SHAPESHIFT_NOFIGHT_NOMOUNT:
			case SPELL_AURA_SHAPESHIFT_FIGHT_MOUNT:
			case SPELL_AURA_SHAPESHIFT_NOFIGHT_MOUNT:
			case SPELL_AURA_MOUNT_FIGHT:
			case SPELL_AURA_MOUNT_NOFIGHT:
				return true;
			default: break;
			}
		}
	}

	return false;
}

bool IsPassiveSpell(SpellEntry* sp)
{
	return false;
}

bool IsTargetDieingSpell(SpellEntry *sp)
{
	for( int i = 0; i < 3; i++){
		switch( sp->Effect[i] )
		{
		case SPELL_EFFECT_RESURRECT:
		case SPELL_EFFECT_RESURRECT_FLAT:
		case SPELL_EFFECT_SELF_RESURRECT:
			return true;
		}
	}
	return false;
}

int IsConsume2GiftSpell(Player* p, SpellEntry* sp)
{
	bool b = false;
	for(int i = 0; i < 3; i++)
	{
		if(sp->Effect[i] == SPELL_EFFECT_CONSUME2GIFT)
		{
			b = true;
		}
	}

	if(!b)
		return -1;
	int nCnt = 0;
	for(int i = 0; i < 3; i++)
	if(sp->EffectMiscValue[i])
	{
		stConsume2GiftEntry* pEntry = Consume2GiftStorage.LookupEntry(sp->EffectMiscValue[i]);
		if(!pEntry)
		{
			MyLog::log->error("not found consume2gift id[%d] in db", sp->EffectMiscValue[i]);
			return 0;
		}

		for(int i = 0; i < 5; i++)
		{
			if(pEntry->item[i] && pEntry->itemcount[i])
				nCnt++;
		}
	}
	if(p->GetItemInterface()->CalculateFreeSlots(NULL) < nCnt)
	{
		return 1;
	}
	return 0;
}

bool IsTeleportSpell(SpellEntry* sp)
{
	for(int i = 0; i < 3; i++)
	{
		switch(sp->Effect[i])
		{
		case SPELL_EFFECT_TELEPORT_UNITS:
		case SPELL_EFFECT_STUCK:
		case SPELL_EFFECT_LEAP:
		case SPELL_EFFECT_RECORDPOS:
			return true;
		}
	}
	return false;
}

bool IsResurrectSpell(SpellEntry* sp)
{
	for(int i = 0; i < 3; i++)
	{
		switch( sp->Effect[i] )
		{
		case SPELL_EFFECT_RESURRECT:
		case SPELL_EFFECT_RESURRECT_FLAT:
		case SPELL_EFFECT_SELF_RESURRECT:
			return true;
		}
	}

	return false;
}

bool IsHealingSpell(SpellEntry *sp)
{
	for(int i = 0; i < 1; i++)
	{
		switch( sp->Effect[i] )
		{
		case SPELL_EFFECT_HEALTH_LEECH:
		case SPELL_EFFECT_HEAL:
		case SPELL_EFFECT_HEALTH_FUNNEL:
		case SPELL_EFFECT_HEAL_MAX_HEALTH:
			if(sp->EffectImplicitTargetA[i] && sp->EffectImplicitTargetA[i] != EFF_TARGET_SINGLE_ENEMY && sp->EffectImplicitTargetA[i] != EFF_TARGET_CREATURE)
				return true;
		default: break;
		}

		if( sp->Effect[i] == SPELL_EFFECT_APPLY_AURA ||
			sp->Effect[i] == SPELL_EFFECT_APPLY_AREA_AURA ||
			sp->Effect[i]==SPELL_EFFECT_PERSISTENT_AREA_AURA)
		{
			switch( sp->EffectApplyAuraName[i] )
			{
			case SPELL_AURA_PERIODIC_HEAL:
			case SPELL_AURA_PERIODIC_HEALTH_FUNNEL:
			case SPELL_AURA_SCHOOL_ABSORB:
				if(sp->EffectImplicitTargetA[i] && sp->EffectImplicitTargetA[i] != EFF_TARGET_SINGLE_ENEMY && sp->EffectImplicitTargetA[i] != EFF_TARGET_CREATURE)
					return true;
			default: break;
			}
		}
	}

	//flash of light, holy light uses scripted effect which is not neceserally heal spell
	if( sp->NameHash == SPELL_HASH_HOLY_LIGHT || sp->NameHash == SPELL_HASH_FLASH_OF_LIGHT  )
		return true;

	return false;
}

bool IsHelpfulSpell(SpellEntry *sp)
{
	bool bRet = IsHealingSpell(sp);
	if(bRet)
		return true;

	bRet = IsResurrectSpell(sp);
	if(bRet)
		return true;

	for(int i = 0; i < 3; i++)
	{
		switch( sp->Effect[i] )
		{
		case SPELL_EFFECT_HEAL_MAX_HEALTH:
			return true;
		default: break;
		}

		if( sp->Effect[i] == SPELL_EFFECT_APPLY_AURA ||
			sp->Effect[i] == SPELL_EFFECT_APPLY_AREA_AURA ||
			sp->Effect[i]==SPELL_EFFECT_PERSISTENT_AREA_AURA)
		{

			if (sp->isDebuf)
			{
				return false;
			}
			else
			{
				return true;
			}

			/*
			switch( sp->EffectApplyAuraName[i] )
			{
			case SPELL_AURA_SCHOOL_ABSORB:
			case SPELL_AURA_MOD_STAT:
			case SPELL_AURA_MOD_INCREASE_SPEED:
			case SPELL_AURA_MOD_INCREASE_HEALTH:
			case SPELL_AURA_MOD_INCREASE_ENERGY:
			case SPELL_AURA_MOD_INCREASE_ENERGY_PERCENT:
			case SPELL_AURA_MOD_INCREASE_HEALTH_PERCENT:
			case SPELL_AURA_ICE_BLOCK:
			case SPELL_AURA_MECHANIC_IMMUNITY:
			case SPELL_AURA_SCHOOL_IMMUNITY:
			case SPELL_AURA_PERIODIC_HEAL:
			case SPELL_AURA_PERIODIC_ENERGIZE:
				return true;
			default: break;
			}
			*/
		}
	}

	return false;
}
bool IsTargetingStealthed(SpellEntry *sp)
{
	if(
		sp->EffectImplicitTargetA[0]==EFF_TARGET_INVISIBLE_OR_HIDDEN_ENEMIES_AT_LOCATION_RADIUS ||
		sp->EffectImplicitTargetA[1]==EFF_TARGET_INVISIBLE_OR_HIDDEN_ENEMIES_AT_LOCATION_RADIUS ||
		sp->EffectImplicitTargetA[2]==EFF_TARGET_INVISIBLE_OR_HIDDEN_ENEMIES_AT_LOCATION_RADIUS ||
		sp->EffectImplicitTargetB[0]==EFF_TARGET_INVISIBLE_OR_HIDDEN_ENEMIES_AT_LOCATION_RADIUS ||
		sp->EffectImplicitTargetB[1]==EFF_TARGET_INVISIBLE_OR_HIDDEN_ENEMIES_AT_LOCATION_RADIUS ||
		sp->EffectImplicitTargetB[2]==EFF_TARGET_INVISIBLE_OR_HIDDEN_ENEMIES_AT_LOCATION_RADIUS ||
		sp->EffectImplicitTargetA[0]==EFF_TARGET_ALL_ENEMIES_AROUND_CASTER ||
		sp->EffectImplicitTargetA[1]==EFF_TARGET_ALL_ENEMIES_AROUND_CASTER ||
		sp->EffectImplicitTargetA[2]==EFF_TARGET_ALL_ENEMIES_AROUND_CASTER ||
		sp->EffectImplicitTargetB[0]==EFF_TARGET_ALL_ENEMIES_AROUND_CASTER ||
		sp->EffectImplicitTargetB[1]==EFF_TARGET_ALL_ENEMIES_AROUND_CASTER ||
		sp->EffectImplicitTargetB[2]==EFF_TARGET_ALL_ENEMIES_AROUND_CASTER ||
		sp->EffectImplicitTargetA[0]==EFF_TARGET_ALL_ENEMY_IN_AREA_CHANNELED ||
		sp->EffectImplicitTargetA[1]==EFF_TARGET_ALL_ENEMY_IN_AREA_CHANNELED ||
		sp->EffectImplicitTargetA[2]==EFF_TARGET_ALL_ENEMY_IN_AREA_CHANNELED ||
		sp->EffectImplicitTargetB[0]==EFF_TARGET_ALL_ENEMY_IN_AREA_CHANNELED ||
		sp->EffectImplicitTargetB[1]==EFF_TARGET_ALL_ENEMY_IN_AREA_CHANNELED ||
		sp->EffectImplicitTargetB[2]==EFF_TARGET_ALL_ENEMY_IN_AREA_CHANNELED
		)
		return 1;

	if(
		sp->NameHash == SPELL_HASH_MAGMA_TOTEM
		)
		return 1;

	return 0;
}


bool TargetTypeCheck(Object *obj,uint32 ReqCreatureTypeMask)
{
	if( !ReqCreatureTypeMask )
		return true;

	if( obj->GetTypeId() == TYPEID_UNIT )
	{
		CreatureInfo* inf = static_cast< Creature* >( obj )->GetCreatureName();
		if( inf == NULL || !( 1 << ( inf->Type - 1 ) & ReqCreatureTypeMask ) )
			return false;
	}
	else if(obj->GetTypeId() == TYPEID_PLAYER && !(UNIT_TYPE_HUMANOID_BIT & ReqCreatureTypeMask))
		return false;
	else return false;//omg, how in the hack did we cast it on a GO ? But who cares ?
	return true;
}
