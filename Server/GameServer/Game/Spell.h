#ifndef __SPELL_H
#define __SPELL_H
/* Spell Ranges:(for 1.10.2)
Range ID|Range|Description
1       0-0     Self Only
2       0-5     Combat Range
3       0-20    Short Range
4       0-30    Medium Range
5       0-40    Long Range
6       0-100   Vision Range
7       0-10    Very Short Range
8       10-20   Short Range
9       10-30   Medium Range
10      10-40   Long Range
11      0-15    Shorter Range
12      0-5     Interact Range
13      0-50000 Anywhere
14      0-60    Extra Long Range
34      0-25    Medium-Short Range
35      0-35    Medium-Long Range
36      0-45    Longer Range
37      0-50    Extended Range
38      10-25   Extra Medium Range
54      5-30    Geoff Monster Shoot
74      8-30    Ranged Weapon
94      8-40    Sting
95      8-25    Charge
96      0-2     Trap
114     8-35    Hunter Range Hunter
134     0-80    Tower 80 Tower
135     0-100   Tower 100 Tower
*/

/*FLAT PCT
0    x    x
1    x    x
2    x    x
3         x
4    x
5    x    x
6    x    x
7    x =  x//both add % to crit
8    x    x
9         x
10   x    x
11   x    x
12   x
13    
14   x    x
15        x
16   x    
17   x
18   x    x
19   x
20        x
21    
22   x    x
23   x(enslave dem)
24    
25
26   x(obsolete)
27        x
*/

//wooohooo, there are 19 spells that actually require to add a proccounter for these 
//first spell catched is "presence of mind"
//an ugly solution would be to add a proc flag to remove aura on event it should expire (like attack or cast) but that is only if count=1

namespace MSG_S2C
{
	struct stSpell_Go;
}

//bool IsBeneficSpell(SpellEntry *sp);
//AI_SpellTargetType RecommandAISpellTargetType(SpellEntry *sp);

enum enchantApplyType
{
	APPLY_NULL							= 0,
	APPLY_ON_ANY_HOSTILE_ACTION			= 1,
	APPLY_ON_GAIN_EXPIERIENCE			= 2,
	APPLY_ON_MELEE_ATTACK				= 3,
	APPLY_ON_CRIT_HIT_VICTIM			= 4,
	APPLY_ON_CAST_SPELL					= 5,
	APPLY_ON_PHYSICAL_ATTACK_VICTIM		= 6,
	APPLY_ON_RANGED_ATTACK				= 7,
	APPLY_ON_RANGED_CRIT_ATTACK			= 8,
	APPLY_ON_PHYSICAL_ATTACK			= 9,
	APPLY_ON_MELEE_ATTACK_VICTIM		= 10,
	APPLY_ON_SPELL_HIT					= 11,
	APPLY_ON_RANGED_CRIT_ATTACK_VICTIM	= 12,
	APPLY_ON_CRIT_ATTACK				= 13,
	APPLY_ON_RANGED_ATTACK_VICTIM		= 14,
	APPLY_ON_PRE_DISPELL_AURA_VICTIM	= 15,
	APPLY_ON_SPELL_LAND_VICTIM			= 16,
	APPLY_ON_CAST_SPECIFIC_SPELL		= 17,
	APPLY_ON_SPELL_HIT_VICTIM			= 18,
	APPLY_ON_SPELL_CRIT_HIT_VICTIM		= 19,
	APPLY_ON_TARGET_DIE					= 20,
	APPLY_ON_ANY_DAMAGE_VICTIM			= 21,
	APPLY_ON_TRAP_TRIGGER				= 22, //triggers on trap activation)
	APPLY_ON_AUTO_SHOT_HIT				= 23,
	APPLY_ON_ABSORB						= 24,
	APPLY_ON_RESIST_VICTIM				= 25,
	APPLY_ON_DODGE_VICTIM				= 26,
	APPLY_ON_DIE						= 27,
	APPLY_REMOVEONUSE					= 28,//remove prochcharge only when it is used
	APPLY_MISC							= 29,//our custom flag to decide if proc dmg or shield
	APPLY_ON_BLOCK_VICTIM				= 30,
	APPLY_ON_SPELL_CRIT_HIT				= 31,
	APPLY_TARGET_SELF					= 32,//our custom flag to decide if proc target is self or victim
	APPLY_ON_EQUIP_CHANGE				= 33,
};

#ifndef NEW_PROCFLAGS 
enum procFlags
{
	PROC_NULL							= 0x0,
	PROC_ON_ANY_HOSTILE_ACTION			= 0x1,
	PROC_ON_GAIN_EXPIERIENCE			= 0x2,
	PROC_ON_MELEE_ATTACK				= 0x4,
	PROC_ON_CRIT_HIT_VICTIM				= 0x8,
	PROC_ON_CAST_SPELL					= 0x10,
	PROC_ON_PHYSICAL_ATTACK_VICTIM		= 0x20,
	PROC_ON_RANGED_ATTACK				= 0x40,
	PROC_ON_RANGED_CRIT_ATTACK			= 0x80,
	PROC_ON_PHYSICAL_ATTACK				= 0x100,
	PROC_ON_MELEE_ATTACK_VICTIM			= 0x200,
	PROC_ON_SPELL_HIT					= 0x400,
	PROC_ON_RANGED_CRIT_ATTACK_VICTIM	= 0x800,
	PROC_ON_CRIT_ATTACK					= 0x1000,
	PROC_ON_RANGED_ATTACK_VICTIM		= 0x2000,
	PROC_ON_PRE_DISPELL_AURA_VICTIM		= 0x4000,
	PROC_ON_SPELL_LAND_VICTIM			= 0x8000,
	PROC_ON_CAST_SPECIFIC_SPELL			= 0x10000,
	PROC_ON_SPELL_HIT_VICTIM			= 0x20000,
	PROC_ON_SPELL_CRIT_HIT_VICTIM		= 0x40000,
	PROC_ON_TARGET_DIE					= 0x80000,
	PROC_ON_ANY_DAMAGE_VICTIM			= 0x100000,
	PROC_ON_TRAP_TRIGGER				= 0x200000, //triggers on trap activation)
	PROC_ON_AUTO_SHOT_HIT				= 0x400000,
	PROC_ON_ABSORB						= 0x800000,
	PROC_ON_RESIST_VICTIM				= 0x1000000,
	PROC_ON_DODGE_VICTIM				= 0x2000000,
	PROC_ON_DIE							= 0x4000000,
	PROC_REMOVEONUSE					= 0x8000000,//remove prochcharge only when it is used
	PROC_MISC							= 0x10000000,//our custom flag to decide if proc dmg or shield
	PROC_ON_BLOCK_VICTIM				= 0x20000000,
	PROC_ON_SPELL_CRIT_HIT				= 0x40000000,
	PROC_TARGET_SELF					= 0x80000000,//our custom flag to decide if proc target is self or victim
};
#else
//REMEMBER: Wands r not ranged based weapons. So it shouldn't be any ranged proc when u hit with wands!
enum procFlags
{
	PROC_NULL                          = 0x0,
	PROC_ON_NPC_ACTION                 = 0x1, //on GO action too btw. related to quests in general.
	PROC_ON_XP_GAIN                    = 0x2, //on honor gain too btw.
	PROC_ON_MELEE_HIT                  = 0x4, //on successful white melee attack
	PROC_ON_MELEE_HIT_VICTIM           = 0x8, //on successful white melee attack victim
	PROC_ON_MELEE_ABILITY_LAND         = 0x10, //on successful melee ability attack. Abilities that was resisted/dodged etc doesn't proc with this flag
	PROC_ON_MELEE_ABILITY_LAND_VICTIM  = 0x20, //on successful melee ability victim but not white damage. Abilities that was resisted/dodged etc doesn't proc with this flag
	PROC_ON_RANGED_HIT                 = 0x40,  //on successful ranged white attack
	PROC_ON_RANGED_HIT_VICTIM          = 0x80,  //on successful ranged white attack victim
	PROC_ON_RANGED_ABILITY_LAND        = 0x100, //on successful ranged ability attack. Abilities that was resisted/dodged etc doesn't proc with this flag
	PROC_ON_RANGED_ABILITY_LAND_VICTIM = 0x200,  //on successful ranged ability victim but not white damage. Abilities that was resisted/dodged etc doesn't proc with this flag
	PROC_ON_CAST_SPELL                 = 0x400, //on nonability (spell) cast. Spells that was resisted don't proc with this flag
	PROC_ON_CAST_SPELL_VICTIM          = 0x800,  //on nonability (spell) cast victim. Spells that was resisted don't proc with this flag.
	PROC_ON_ANY_DAMAGE                 = 0x1000, // mb wrong
	PROC_ON_ANY_DAMAGE_VICTIM          = 0x2000, // mb wrong
	PROC_ON_HEALSPELL_LAND             = 0x4000, //on heal (direct or HoT) spell land.
	PROC_ON_HEALSPELL_LAND_VICTIM      = 0x8000,  //on heal (direct or HoT) spell land victim.
	PROC_ON_HARMFULSPELL_LAND          = 0x10000, //on harmfull spell land (DoT damage not included in this flag!)
	PROC_ON_HARMFULSPELL_LAND_VICTIM   = 0x20000, //on harmfull spell land victim (DoT damage not included in this flag!)
	PROC_ON_DOT_DAMAGE                 = 0x40000, //on harmfull non direct damage (DoTs)
	PROC_ON_DOT_DAMAGE_VICTIM          = 0x80000,  //on harmfull non direct damage (DoTs) victim
	PROC_REMOVEONUSE                   = 0x100000, //something supercustom. 99% wrong :P used by bombs and grenades in general.
	PROC_ON_TRAP_TRIGGER               = 0x200000,
	PROC_UNUSED1                       = 0x400000,
	PROC_ON_OFFHAND_HIT                = 0x800000, //only 1 spellname "Combat Potency"
	PROC_ON_UNK1                       = 0x1000000,//only 1 spellname "Captured Totem"
};
enum customProcFlags
{
	CUSTOMPROC_NULL                       = 0x0;
	CUSTOMPROC_ON_CRIT                    = 0x1; //doesn't matter victim or not. 
	CUSTOMPROC_ON_MISS_VICTIM             = 0x2;
	CUSTOMPROC_ON_DODGE_VICTIM            = 0x4;
	CUSTOMPROC_ON_BLOCK_VICTIM            = 0x8;
	CUSTOMPROC_ON_PARRY_VICTIM            = 0x10;
	CUSTOMPROC_ON_RESIST_VICTIM           = 0x20;
	CUSTOMPROC_ON_DIE                     = 0x40;//proc on our death
	CUSTOMPROC_ON_FINISHMOVE              = 0x80; //procs when we use finish move ability
	CUSTOMPROC_ON_ADDCOMBO                = 0x100; //procs when we use ability with +combo point
	CUSTOMPROC_PROC_ON_SELF               = 0x200; //proc on self
};
#endif

//
//enum SpellCastFlags
//{
//    CAST_FLAG_UNKNOWN1           = 0x2,
//    CAST_FLAG_UNKNOWN2           = 0x10, // no idea yet, i saw it in blizzard spell
//    CAST_FLAG_AMMO               = 0x20 // load ammo display id (uint32) and ammo inventory type (uint32)
//};


enum ReplenishType
{
    REPLENISH_UNDEFINED = 0,
    REPLENISH_HEALTH    = 20,
    REPLENISH_MANA      = 21,
    REPLENISH_RAGE      = 22 //don't know if rage is 22 or what, but will do for now
};

enum SpellTargetType
{
    TARGET_TYPE_NULL       = 0x0,
    TARGET_TYPE_BEAST      = 0x1,
    TARGET_TYPE_DRAGONKIN  = 0x2,
    TARGET_TYPE_DEMON      = 0x4,
    TARGET_TYPE_ELEMENTAL  = 0x8,
    TARGET_TYPE_GIANT      = 0x10,
    TARGET_TYPE_UNDEAD     = 0x20,
    TARGET_TYPE_HUMANOID   = 0x40,
    TARGET_TYPE_CRITTER    = 0x80,
    TARGET_TYPE_MECHANICAL = 0x100,
};

/****************SpellExtraFlags*****************/
/* SpellExtraFlags defines                      */
/*                                              */
/* Used for infront check and other checks      */
/* when they are not in spell.dbc               */
/*                                              */
/************************************************/
#define SPELL_EXTRA_INFRONT 1
#define SPELL_EXTRA_BEHIND  2
#define SPELL_EXTRA_UNDEF0  4 // not used yet
#define SPELL_EXTRA_UNDEF1  8 // not used yet


/***************Ranged spellid*******************/
/* Note: These spell id's are checked for 2.0.x */
/************************************************/
#define SPELL_RANGED_GENERAL    210001
#define SPELL_RANGED_THROW      2764
#define SPELL_RANGED_WAND       5019

struct TotemSpells
{
    uint32 spellId;
    uint32 spellToCast[3];
};



// spell target system
#define TOTAL_SPELL_TARGET 91 // note: all spells with target type's > 80 are test spells
// 
// enum SPELL_ENTRY
// {
//     SPELL_ENTRY_Id,
//     SPELL_ENTRY_School,
//     SPELL_ENTRY_Category,
//     SPELL_ENTRY_field4,
//     SPELL_ENTRY_DispelType,
//     SPELL_ENTRY_MechanicsType,
//     SPELL_ENTRY_Attributes,
//     SPELL_ENTRY_AttributesEx,
//     SPELL_ENTRY_Flags3,
//     SPELL_ENTRY_Flags4,
//     SPELL_ENTRY_Flags5,
//     SPELL_ENTRY_unk201_1,
//     SPELL_ENTRY_RequiredShapeShift,
//     SPELL_ENTRY_UNK14,
//     SPELL_ENTRY_Targets,
//     SPELL_ENTRY_TargetCreatureType,
//     SPELL_ENTRY_RequiresSpellFocus,
//     SPELL_ENTRY_CasterAuraState,
//     SPELL_ENTRY_TargetAuraState,
//     SPELL_ENTRY_unk201_2,
//     SPELL_ENTRY_unk201_3,
//     SPELL_ENTRY_CastingTimeIndex,
//     SPELL_ENTRY_RecoveryTime,
//     SPELL_ENTRY_CategoryRecoveryTime,
//     SPELL_ENTRY_InterruptFlags,
//     SPELL_ENTRY_AuraInterruptFlags,
//     SPELL_ENTRY_ChannelInterruptFlags,
//     SPELL_ENTRY_procFlags,
//     SPELL_ENTRY_procChance,
//     SPELL_ENTRY_procCharges,
//     SPELL_ENTRY_maxLevel,
//     SPELL_ENTRY_baseLevel,
//     SPELL_ENTRY_spellLevel,
//     SPELL_ENTRY_DurationIndex,
//     SPELL_ENTRY_powerType,
//     SPELL_ENTRY_manaCost,
//     SPELL_ENTRY_manaCostPerlevel,
//     SPELL_ENTRY_manaPerSecond,
//     SPELL_ENTRY_manaPerSecondPerLevel,
//     SPELL_ENTRY_rangeIndex,
//     SPELL_ENTRY_speed,
//     SPELL_ENTRY_modalNextSpell,
//     SPELL_ENTRY_maxstack,
//     SPELL_ENTRY_Totem_1,
//     SPELL_ENTRY_Totem_2,
//     SPELL_ENTRY_Reagent_1,
//     SPELL_ENTRY_Reagent_2,
//     SPELL_ENTRY_Reagent_3,
//     SPELL_ENTRY_Reagent_4,
//     SPELL_ENTRY_Reagent_5,
//     SPELL_ENTRY_Reagent_6,
//     SPELL_ENTRY_Reagent_7,
//     SPELL_ENTRY_Reagent_8,
//     SPELL_ENTRY_ReagentCount_1,
//     SPELL_ENTRY_ReagentCount_2,
//     SPELL_ENTRY_ReagentCount_3,
//     SPELL_ENTRY_ReagentCount_4,
//     SPELL_ENTRY_ReagentCount_5,
//     SPELL_ENTRY_ReagentCount_6,
//     SPELL_ENTRY_ReagentCount_7,
//     SPELL_ENTRY_ReagentCount_8,
//     SPELL_ENTRY_EquippedItemClass,
//     SPELL_ENTRY_EquippedItemSubClass,
//     SPELL_ENTRY_RequiredItemFlags,
//     SPELL_ENTRY_Effect_1,
//     SPELL_ENTRY_Effect_2,
//     SPELL_ENTRY_Effect_3,
//     SPELL_ENTRY_EffectDieSides_1,
//     SPELL_ENTRY_EffectDieSides_2,
//     SPELL_ENTRY_EffectDieSides_3,
//     SPELL_ENTRY_EffectBaseDice_1,
//     SPELL_ENTRY_EffectBaseDice_2,
//     SPELL_ENTRY_EffectBaseDice_3,
//     SPELL_ENTRY_EffectDicePerLevel_1,
//     SPELL_ENTRY_EffectDicePerLevel_2,
//     SPELL_ENTRY_EffectDicePerLevel_3,
//     SPELL_ENTRY_EffectRealPointsPerLevel_1,
//     SPELL_ENTRY_EffectRealPointsPerLevel_2,
//     SPELL_ENTRY_EffectRealPointsPerLevel_3,
//     SPELL_ENTRY_EffectBasePoints_1,
//     SPELL_ENTRY_EffectBasePoints_2,
//     SPELL_ENTRY_EffectBasePoints_3,
//     SPELL_ENTRY_EffectMechanic_1,
//     SPELL_ENTRY_EffectMechanic_2,
//     SPELL_ENTRY_EffectMechanic_3,
//     SPELL_ENTRY_EffectImplicitTargetA_1,
//     SPELL_ENTRY_EffectImplicitTargetA_2,
//     SPELL_ENTRY_EffectImplicitTargetA_3,
//     SPELL_ENTRY_EffectImplicitTargetB_1,
//     SPELL_ENTRY_EffectImplicitTargetB_2,
//     SPELL_ENTRY_EffectImplicitTargetB_3,
//     SPELL_ENTRY_EffectRadiusIndex_1,
//     SPELL_ENTRY_EffectRadiusIndex_2, 
//     SPELL_ENTRY_EffectRadiusIndex_3, 
//     SPELL_ENTRY_EffectApplyAuraName_1,
//     SPELL_ENTRY_EffectApplyAuraName_2,
//     SPELL_ENTRY_EffectApplyAuraName_3,
//     SPELL_ENTRY_EffectAmplitude_1,
//     SPELL_ENTRY_EffectAmplitude_2,
//     SPELL_ENTRY_EffectAmplitude_3,
//     SPELL_ENTRY_Effectunknown_1,
//     SPELL_ENTRY_Effectunknown_2,
//     SPELL_ENTRY_Effectunknown_3,
//     SPELL_ENTRY_EffectChainTarget_1,
//     SPELL_ENTRY_EffectChainTarget_2,
//     SPELL_ENTRY_EffectChainTarget_3,
//     SPELL_ENTRY_EffectSpellGroupRelation_1,
//     SPELL_ENTRY_EffectSpellGroupRelation_2,
//     SPELL_ENTRY_EffectSpellGroupRelation_3,
//     SPELL_ENTRY_EffectMiscValue_1,
//     SPELL_ENTRY_EffectMiscValue_2,
//     SPELL_ENTRY_EffectMiscValue_3,
//     SPELL_ENTRY_EffectTriggerSpell_1,
//     SPELL_ENTRY_EffectTriggerSpell_2,
//     SPELL_ENTRY_EffectTriggerSpell_3,
//     SPELL_ENTRY_EffectPointsPerComboPoint_1,
//     SPELL_ENTRY_EffectPointsPerComboPoint_2,
//     SPELL_ENTRY_EffectPointsPerComboPoint_3,
//     SPELL_ENTRY_SpellVisual,
//     SPELL_ENTRY_field114,
//     SPELL_ENTRY_dummy,
//     SPELL_ENTRY_CoSpell,
//     SPELL_ENTRY_spellPriority,
//     SPELL_ENTRY_Name,
//     SPELL_ENTRY_NameAlt1,
//     SPELL_ENTRY_NameAlt2,
//     SPELL_ENTRY_NameAlt3,
//     SPELL_ENTRY_NameAlt4,
//     SPELL_ENTRY_NameAlt5,
//     SPELL_ENTRY_NameAlt6,
//     SPELL_ENTRY_NameAlt7,
//     SPELL_ENTRY_NameAlt8,
//     SPELL_ENTRY_NameAlt9,
//     SPELL_ENTRY_NameAlt10,
//     SPELL_ENTRY_NameAlt11,
//     SPELL_ENTRY_NameAlt12,
//     SPELL_ENTRY_NameAlt13,
//     SPELL_ENTRY_NameAlt14,
//     SPELL_ENTRY_NameAlt15,
//     SPELL_ENTRY_NameFlags,
//     SPELL_ENTRY_Rank,
//     SPELL_ENTRY_RankAlt1,
//     SPELL_ENTRY_RankAlt2,
//     SPELL_ENTRY_RankAlt3,
//     SPELL_ENTRY_RankAlt4,
//     SPELL_ENTRY_RankAlt5,
//     SPELL_ENTRY_RankAlt6,
//     SPELL_ENTRY_RankAlt7,
//     SPELL_ENTRY_RankAlt8,
//     SPELL_ENTRY_RankAlt9,
//     SPELL_ENTRY_RankAlt10,
//     SPELL_ENTRY_RankAlt11,
//     SPELL_ENTRY_RankAlt12,
//     SPELL_ENTRY_RankAlt13,
//     SPELL_ENTRY_RankAlt14,
//     SPELL_ENTRY_RankAlt15,
//     SPELL_ENTRY_RankFlags,
//     SPELL_ENTRY_Description,
//     SPELL_ENTRY_DescriptionAlt1,
//     SPELL_ENTRY_DescriptionAlt2,
//     SPELL_ENTRY_DescriptionAlt3,
//     SPELL_ENTRY_DescriptionAlt4,
//     SPELL_ENTRY_DescriptionAlt5,
//     SPELL_ENTRY_DescriptionAlt6,
//     SPELL_ENTRY_DescriptionAlt7,
//     SPELL_ENTRY_DescriptionAlt8,
//     SPELL_ENTRY_DescriptionAlt9,
//     SPELL_ENTRY_DescriptionAlt10,
//     SPELL_ENTRY_DescriptionAlt11,
//     SPELL_ENTRY_DescriptionAlt12,
//     SPELL_ENTRY_DescriptionAlt13,
//     SPELL_ENTRY_DescriptionAlt14,
//     SPELL_ENTRY_DescriptionAlt15,
//     SPELL_ENTRY_DescriptionFlags,
//     SPELL_ENTRY_BuffDescription,
//     SPELL_ENTRY_BuffDescriptionAlt1,
//     SPELL_ENTRY_BuffDescriptionAlt2,
//     SPELL_ENTRY_BuffDescriptionAlt3,
//     SPELL_ENTRY_BuffDescriptionAlt4,
//     SPELL_ENTRY_BuffDescriptionAlt5,
//     SPELL_ENTRY_BuffDescriptionAlt6,
//     SPELL_ENTRY_BuffDescriptionAlt7,
//     SPELL_ENTRY_BuffDescriptionAlt8,
//     SPELL_ENTRY_BuffDescriptionAlt9,
//     SPELL_ENTRY_BuffDescriptionAlt10,
//     SPELL_ENTRY_BuffDescriptionAlt11,
//     SPELL_ENTRY_BuffDescriptionAlt12,
//     SPELL_ENTRY_BuffDescriptionAlt13,
//     SPELL_ENTRY_BuffDescriptionAlt14,
//     SPELL_ENTRY_BuffDescriptionAlt15,
//     SPELL_ENTRY_buffdescflags,
//     SPELL_ENTRY_ManaCostPercentage,
//     SPELL_ENTRY_unkflags,
//     SPELL_ENTRY_StartRecoveryTime,
//     SPELL_ENTRY_StartRecoveryCategory,
//     SPELL_ENTRY_SpellFamilyName,
//     SPELL_ENTRY_SpellGroupType,
//     SPELL_ENTRY_unkne,
//     SPELL_ENTRY_MaxTargets,
//     SPELL_ENTRY_Spell_Dmg_Type,
//     SPELL_ENTRY_FG,
//     SPELL_ENTRY_FH,
//     SPELL_ENTRY_dmg_multiplier_1,
//     SPELL_ENTRY_dmg_multiplier_2,
//     SPELL_ENTRY_dmg_multiplier_3,
//     SPELL_ENTRY_FL,
//     SPELL_ENTRY_FM,
//     SPELL_ENTRY_FN,
// 	SPELL_ENTRY_TotemCategory1,
// 	SPELL_ENTRY_TotemCategory2,
// 	SPELL_ENTRY_RequiredAreaID
// };

// target type flags
enum SpellTargetTypes
{
    SPELL_TARGET_TYPE_NONE              = 0x01,
    SPELL_TARGET_TYPE_PROFESSION        = 0x02,
    SPELL_TARGET_TYPE_NONE1             = 0x04,
    SPELL_TARGET_TYPE_NONE2             = 0x08,
    SPELL_TARGET_TYPE_ENCHANTABLE_ITEM  = 0x10,
    SPELL_TARGET_TYPE_UNK               = 0x20,     // seems to be scripted stuff
    SPELL_TARGET_TYPE_UNK0              = 0x40,     // lots of spells interesting to research this one further
    SPELL_TARGET_TYPE_UNK1              = 0x80,     // something todo with scripted and GM stuff
    SPELL_TARGET_TYPE_UNK2              = 0x100,    // lots of spells interesting to research this one further...
    SPELL_TARGET_TYPE_PLAYER_CORPSE     = 0x200,
    SPELL_TARGET_TYPE_DEATHx            = 0x400,
    SPELL_TARGET_TYPE_NONE3             = 0x800,
    SPELL_TARGET_TYPE_NONE4             = 0x1000,
    SPELL_TARGET_TYPE_NONE5             = 0x2000,
    SPELL_TARGET_TYPE_GAME_OBJECTS      = 0x4000, // like chests and mining
    SPELL_TARGET_TYPE_DEATH             = 0x8000,
};

enum SpellTypes // SPELL_ENTRY_buffType
{
    SPELL_TYPE_NONE                 = 0x00000000,
    SPELL_TYPE_SEAL                 = 0x00000001,
    SPELL_TYPE_ASPECT               = 0x00000002,
    SPELL_TYPE_BLESSING             = 0x00000004,
    SPELL_TYPE_CURSE                = 0x00000008,
    SPELL_TYPE_STING                = 0x00000010,
    SPELL_TYPE_ARMOR                = 0x00000020,
    SPELL_TYPE_AURA                 = 0x00000040,
    //hmm these could be named simply incompatible spells. One active at a time
    SPELL_TYPE_MARK_GIFT            = 0x00000080,
    SPELL_TYPE_TRACK                = 0x00000100,
    SPELL_TYPE_HUNTER_TRAP          = 0x00000200,
    SPELL_TYPE_MAGE_INTEL           = 0x00000400,
    SPELL_TYPE_MAGE_MAGI            = 0x00000800,
    SPELL_TYPE_MAGE_WARDS           = 0x00001000,
    SPELL_TYPE_PRIEST_SH_PPROT      = 0x00002000,
    SPELL_TYPE_SHIELD               = 0x00004000,
    SPELL_TYPE_FORTITUDE            = 0x00008000,
    SPELL_TYPE_SPIRIT               = 0x00010000,
    SPELL_TYPE_MAGE_AMPL_DUMP       = 0x00020000,
    SPELL_TYPE_WARLOCK_IMMOLATE     = 0x00040000, //maybe there is a better way to trigger the aura state for immolate spell
    SPELL_TYPE_ELIXIR_BATTLE		= 0x00080000, 
    SPELL_TYPE_ELIXIR_GUARDIAN      = 0x00100000, 
    SPELL_TYPE_ELIXIR_FLASK         = SPELL_TYPE_ELIXIR_BATTLE | SPELL_TYPE_ELIXIR_GUARDIAN, //weee, this contains both battle and guardian elixirs ;)
    SPELL_TYPE_HUNTER_MARK			= 0x00200000,
    SPELL_TYPE_WARRIOR_SHOUT        = 0x00400000,
};

//custom stuff generated for spells that will not change in time
enum SpellIsFlags
{
    SPELL_FLAG_IS_DAMAGING				= 0x00000001,
    SPELL_FLAG_IS_HEALING				= 0x00000002,
    SPELL_FLAG_IS_TARGETINGSTEALTHED	= 0x00000004,
    SPELL_FLAG_IS_REQUIRECOOLDOWNUPDATE	= 0x00000008, //it started with rogue cold blood but i'm sure others will come
    SPELL_FLAG_IS_POISON				= 0x00000010, //rogue has a few spells that can stack so can't use the spell_type enum ;)
    SPELL_FLAG_IS_FINISHING_MOVE		= 0x00000020, //rogue has a few spells that can stack so can't use the spell_type enum ;)
    SPELL_FLAG_IS_NOT_USING_DMG_BONUS	= 0x00000040, 
    SPELL_FLAG_IS_CHILD_SPELL			= 0x00000080, //auras proc auras that have same name, these should not remove mother aura when adding to target
    SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET		= 0x00000100, //we should cast these on pet too
    SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER	= 0x00000200, //we should cast these on owner too
    SPELL_FLAG_IS_EXPIREING_WITH_PET	= 0x00000400, //when pet dies, we remove this too
    SPELL_FLAG_IS_EXPIREING_ON_PET		= 0x00000800, //when pet is summoned
	SPELL_FLAG_IS_FORCEDDEBUFF			= 0x00001000, // forced to be a debuff
	SPELL_FLAG_IS_FORCEDBUFF			= 0x00002000, // forced to be a buff
};

enum SpellCoefficientsFlags
{
	SPELL_FLAG_IS_DOT_OR_HOT_SPELL		= 0x00000001, //Damage over Time or Healing over Time Spells
	SPELL_FLAG_IS_DD_OR_DH_SPELL		= 0x00000002, //Direct Damage or Direct Healing Spells
	SPELL_FLAG_IS_DD_DH_DOT_SPELL		= SPELL_FLAG_IS_DOT_OR_HOT_SPELL | SPELL_FLAG_IS_DD_OR_DH_SPELL, //DoT+(DD|DH) Spells
	SPELL_FLAG_AOE_SPELL				= 0x00000004, //AoE Spells
	SPELL_FLAG_ADITIONAL_EFFECT			= 0x00000008, //Spells with aditional effect not DD or DoT or HoT
};

SUNYOU_INLINE bool CanAgroHash(uint32 spellhashname)
{
    if (spellhashname == 4287212498UL) //hunter's mark
        return false;
    else
        return true;
}

/************************************************************************/
/* IsDamagingSpell, this function seems slow, its only used rarely      */
/************************************************************************/
SUNYOU_INLINE bool IsDamagingSpell(SpellEntry *sp)
{
	for(int i = 0; i < 3; i++)
	{
		switch (sp->Effect[i])
		{
		case SPELL_EFFECT_SCHOOL_DAMAGE:
		case SPELL_EFFECT_ENVIRONMENTAL_DAMAGE:
		case SPELL_EFFECT_HEALTH_LEECH:
		case SPELL_EFFECT_WEAPON_DAMAGE_NOSCHOOL:
		case SPELL_EFFECT_ADD_EXTRA_ATTACKS:
		case SPELL_EFFECT_WEAPON_PERCENT_DAMAGE:
		case SPELL_EFFECT_POWER_BURN:
		case SPELL_EFFECT_ATTACK:
		case SPELL_EFFECT_CHONGZHUANG:
		case SPELL_EFFECT_PHYSATTACK:
			return true;
		}

		if( sp->Effect[i]==SPELL_EFFECT_APPLY_AURA ||
			sp->Effect[i]==SPELL_EFFECT_APPLY_AREA_AURA ||
			sp->Effect[i]==SPELL_EFFECT_PERSISTENT_AREA_AURA)
		{

			if (sp->isDebuf)
			{
				return true;
			}
			else
			{
				return false;
			}
			switch (sp->EffectApplyAuraName[i])
			{
			case SPELL_AURA_PERIODIC_DAMAGE:
			case SPELL_AURA_PROC_TRIGGER_DAMAGE:
			case SPELL_AURA_PERIODIC_DAMAGE_PERCENT:
			case SPELL_AURA_POWER_BURN:
			case SPELL_AURA_MOD_FEAR:
			//case SPELL_AURA_MOD_STUN:
			case SPELL_AURA_MOD_ROOT:
			case SPELL_AURA_MOD_SILENCE:
			case SPELL_AURA_MOD_DAMAGE_TAKEN:
			case SPELL_AURA_MOD_TAUNT:
			case SPELL_AURA_MOD_THREAT:
				return true;
			}
		}
	}
  
    return false;
}

bool IsHealingSpell(SpellEntry *sp);
bool IsHelpfulSpell(SpellEntry *sp);
bool IsTargetDieingSpell(SpellEntry *sp);
bool IsImmunitySpell(SpellEntry* sp, uint32& miscType, uint32& cnt);
bool IsMountingSpell(SpellEntry* sp);
bool IsShapeSpell(SpellEntry* sp);
bool IsTeleportSpell(SpellEntry* sp);
bool IsResurrectSpell(SpellEntry* sp);
int IsConsume2GiftSpell(Player* p, SpellEntry* sp);
bool IsLuoPanShi(SpellEntry* sp);
bool IsExtraXPSpell(SpellEntry* sp);
bool IsRootSpell(SpellEntry* sp);
bool IsDecSpeedSpell(SpellEntry* sp);
bool IsFearSpell(SpellEntry* sp);
bool IsStunSpell(SpellEntry* sp);
bool IsPassiveSpell(SpellEntry* sp);//是否是被动技能

SUNYOU_INLINE bool IsInrange(LocationVector & location, Object * o, float square_r)
{
    float r = o->GetDistanceSq(location);
    return ( r<square_r);
}

SUNYOU_INLINE bool IsInrange(float x1,float y1, float z1, Object * o,float square_r)
{
    float r = o->GetDistanceSq(x1, y1, z1);
    return ( r<square_r);
}

SUNYOU_INLINE bool IsInrange(float x1,float y1, float z1,float x2,float y2, float z2,float square_r)
{
    float t;
    float r;
    t=x1-x2;
    r=t*t;
    t=y1-y2;
    r+=t*t;
    t=z1-z2;
    r+=t*t;
    return ( r<=square_r);
}
   
SUNYOU_INLINE bool IsInrange(Object * o1,Object * o2,float square_r)
{
    return IsInrange(o1->GetPositionX(),o1->GetPositionY(),o1->GetPositionZ(),
        o2->GetPositionX(),o2->GetPositionY(),o2->GetPositionZ(),square_r);
}

SUNYOU_INLINE bool TargetTypeCheck(Object *obj,uint32 ReqCreatureTypeMask);


enum SpellState
{
    SPELL_STATE_NULL      = 0,
    SPELL_STATE_PREPARING = 1,
    SPELL_STATE_CASTING   = 2,
    SPELL_STATE_FINISHED  = 3,
    SPELL_STATE_IDLE      = 4
};

bool IsTargetingStealthed(SpellEntry *sp);

// slow
struct SpellTargetMod
{
    SpellTargetMod(uint64 _TargetGuid, uint8 _TargetModType) : TargetGuid(_TargetGuid), TargetModType(_TargetModType)
    {

    }
    uint64 TargetGuid;
    uint8  TargetModType;
};


typedef std::vector<uint64> TargetsList;
typedef std::vector<SpellTargetMod> SpellTargetsList;

typedef void(Spell::*pSpellEffect)(uint32 i);
typedef void(Spell::*pSpellTarget)(uint32 i, uint32 j);

#define GO_FISHING_BOBBER 35591

#define SPELL_SPELL_CHANNEL_UPDATE_INTERVAL 1000
class DummySpellHandler;

enum SpellDidHitResult
{
	SPELL_DID_HIT_SUCCESS					= 0,
	SPELL_DID_HIT_MISS						= 1,
	SPELL_DID_HIT_RESIST					= 2,
	SPELL_DID_HIT_DODGE						= 3,
	SPELL_DID_HIT_DEFLECT					= 4,
	SPELL_DID_HIT_BLOCK						= 5,
	SPELL_DID_HIT_EVADE						= 6,
	SPELL_DID_HIT_IMMUNE					= 7,
	SPELL_DID_HIT_MAX						= 8,
};

// Spell instance
class SERVER_DECL Spell
{
public:
    friend class DummySpellHandler;
    Spell( Object* Caster, SpellEntry *info, bool triggered, Aura* aur);
    ~Spell();

    // Fills specified targets at the area of effect
    void FillSpecifiedTargetsInArea(float srcx,float srcy,float srcz,uint32 ind, uint32 specification);
    // Fills specified targets at the area of effect. We suppose we already inited this spell and know the details
    void FillSpecifiedTargetsInArea(uint32 i,float srcx,float srcy,float srcz, float range, uint32 specification);
    // Fills the targets at the area of effect
    void FillAllTargetsInArea(uint32 i, float srcx,float srcy,float srcz, float range);
    // Fills the targets at the area of effect. We suppose we already inited this spell and know the details
    void FillAllTargetsInArea(float srcx,float srcy,float srcz,uint32 ind);
    // Fills the targets at the area of effect. We suppose we already inited this spell and know the details
    void FillAllTargetsInArea(LocationVector & location,uint32 ind);
    // Fills the targets at the area of effect. We suppose we already inited this spell and know the details
    void FillAllFriendlyInArea(uint32 i, float srcx,float srcy,float srcz, float range);
    //get single Enemy as target
    uint64 GetSinglePossibleEnemy(uint32 i, float prange=0);
    //get single Enemy as target
    uint64 GetSinglePossibleFriend(uint32 i, float prange=0);
    //generate possible target list for a spell. Use as last resort since it is not acurate
    void GenerateTargets(SpellCastTargets *store_buff);
    // Fills the target map of the spell packet
    void FillTargetMap(uint32);
    // See if we hit the target or can it resist (evade/immune/resist on spellgo) (0=success)
    uint8 DidHit(uint32 effindex,Unit* target);
    // Prepares the spell thats going to cast to targets
    uint8 prepare(SpellCastTargets * targets);
    // Cancels the current spell
    void cancel(bool b = true);
    // Update spell state based on time difference
    void update(uint32 difftime);
    // Casts the spell
    void cast(bool);
    // Finishes the casted spell
    void finish();
    // Handle the Effects of the Spell
    void HandleEffects(uint64 guid, uint32 i);
    // Take Power from the caster based on spell power usage
    bool TakePower();
    // Has power?
    bool HasPower();
    // Trigger Spell function that triggers triggered spells
    void TriggerSpell();
    // Checks the caster is ready for cast
    uint8 CanCast(bool);
    // Removes reagents, ammo, and items/charges
    void RemoveItems();
    // Calculates the i'th effect value
    int32 CalculateEffect(uint32, Unit *target);
    // Handles Teleport function
    void HandleTeleport(uint32 id, Unit* Target);
    // Determines how much skill caster going to gain
    void DetermineSkillUp();
    // Increases cast time of the spell
    void AddTime(uint32 type);
    void AddCooldown();
    void AddStartCooldown();


    bool Reflect(Unit * refunit);

    SUNYOU_INLINE uint32 getState() { return m_spellState; }
    SUNYOU_INLINE void SetUnitTarget(Unit *punit){unitTarget=punit;}

    // Send Packet functions
    void SendCastResult(uint8 result);
    void SendSpellStart();
    void SendSpellGo();
    void SendLogExecute(uint32 damage, uint64 & targetGuid);
    void SendInterrupted(uint8 result);
    void SendChannelUpdate(uint32 time);
    void SendChannelStart(uint32 duration);
    void SendResurrectRequest(Player* target);
    void SendHealSpellOnPlayer(Object* caster, Object* target, uint32 dmg,bool critical);
    void SendHealManaSpellOnPlayer(Object * caster, Object * target, uint32 dmg, uint32 powertype);
    

    void HandleAddAura(uint64 guid);
    void writeSpellGoTargets( MSG_S2C::stSpell_Go* MsgGo );
    void writeSpellMissedTargets( MSG_S2C::stSpell_Go* MsgGo );

    SpellEntry* m_spellInfo;
    uint32 pSpellId;
    SpellEntry *ProcedOnSpell; //some spells need to know the origins of the proc too
    SpellCastTargets m_targets;

	uint16 m_curattackcnt;
	static uint16 m_attackcnt;
	static uint16 GetDamageSeq(bool bInc=true){
		if(bInc) m_attackcnt++;
		if(!m_attackcnt)
			m_attackcnt++;
		return m_attackcnt;
	}
    void CreateItem(uint32 itemId);

    // Effect Handlers
	void SpellEffectRecordPos(uint32 i);
	void SpellEffectConsume2Gift(uint32 i);
	void SpellEffectTriggerXP(uint32 i);
	void SpellEffectChongzhuang(uint32 i);
	void SpellEffectHealMana(uint32 i);
	void SpellEffectBanYueZhan(uint32 i);
    void SpellEffectNULL(uint32 i);
    void SpellEffectInstantKill(uint32 i);
    void SpellEffectSchoolDMG(uint32 i);
	void SpellEffectChainDamageWithChangeValue(uint32 i);
    void SpellEffectDummy(uint32 i);
    void SpellEffectTeleportUnits(uint32 i);
    void SpellEffectApplyAura(uint32 i);
    void SpellEffectPowerDrain(uint32 i);
    void SpellEffectHealthLeech(uint32 i);
    void SpellEffectHeal(uint32 i);
    void SpellEffectQuestComplete(uint32 i);
    void SpellEffectWeapondamageNoschool(uint32 i);
    void SpellEffectResurrect(uint32 i);
    void SpellEffectAddExtraAttacks(uint32 i);
    void SpellEffectDodge(uint32 i);
    void SpellEffectBlock(uint32 i);
    void SpellEffectParry(uint32 i);
    void SpellEffectCreateItem(uint32 i);
	void SpellEffectSkillCreateItem(uint32 i);
    void SpellEffectPersistentAA(uint32 i);
    void SpellEffectSummon(uint32 i);
    void SpellEffectLeap(uint32 i);				//跳跃，飞跃
	void SpellAuraExtraXPRatio(uint32 i);
    void SpellEffectEnergize(uint32 i);
    void SpellEffectWeaponDmgPerc(uint32 i);
    void SpellEffectTriggerMissile(uint32 i);
    void SpellEffectOpenLock(uint32 i);
	void SpellEffectApplyAADamage(uint32 i);
    void SpellEffectApplyAA(uint32 i);
    void SpellEffectLearnSpell(uint32 i);
    void SpellEffectSpellDefense(uint32 i);
    void SpellEffectDispel(uint32 i);
	void SpellEffectPosDispel(uint32 i);
	void SpellEffectNegDispel(uint32 i);	
	void SpellEffectDispelAuraById(uint32 i);
    void SpellEffectSummonWild(uint32 i);
    void SpellEffectSummonGuardian(uint32 i);
    void SpellEffectSkillStep(uint32 i);
    void SpellEffectSummonObject(uint32 i);
    void SpellEffectEnchantItem(uint32 i);
    void SpellEffectEnchantItemTemporary(uint32 i);
    void SpellEffectTameCreature(uint32 i);
	void SpellEffectSummonPet(uint32 i);
	void SpellEffectAddPetToList(uint32 i);
    void SpellEffectWeapondamage(uint32 i);
    void SpellEffectPowerBurn(uint32 i);
    void SpellEffectPowerBrunFullDamage(uint32 i);
    void SpellEffectThreat(uint32 i);
    void SpellEffectTriggerSpell(uint32 i);
    void SpellEffectHealthFunnel(uint32 i);
    void SpellEffectPowerFunnel(uint32 i);
    void SpellEffectHealMaxHealth(uint32 i);
    void SpellEffectInterruptCast(uint32 i);
    void SpellEffectDistract(uint32 i);
    void SpellEffectPickpocket(uint32 i);
    void SpellEffectAddFarsight(uint32 i);
    void SpellEffectSummonPossessed(uint32 i);
    void SpellEffectCreateSummonTotem(uint32 i);
    void SpellEffectHealMechanical(uint32 i);
    void SpellEffectSummonObjectWild(uint32 i);
    void SpellEffectScriptEffect(uint32 i);
    void SpellEffectSanctuary(uint32 i);			//禁猎，保护区
    void SpellEffectAddComboPoints(uint32 i);
	void SpellEffectCreateHouse(uint32 i);
    void SpellEffectDuel(uint32 i);
    void SpellEffectStuck(uint32 i);
    void SpellEffectSummonPlayer(uint32 i);
    void SpellEffectActivateObject(uint32 i);
    void SpellEffectSummonTotem(uint32 i);
    void SpellEffectProficiency(uint32 i);
    void SpellEffectSendEvent(uint32 i);
    void SpellEffectSkinning(uint32 i);
    void SpellEffectCharge(uint32 i);
    void SpellEffectSummonCritter(uint32 i);
    void SpellEffectKnockBack(uint32 i);			//击退
    void SpellEffectInebriate(uint32 i);			//醉了
    void SpellEffectFeedPet(uint32 i);
    void SpellEffectDismissPet(uint32 i);
    void SpellEffectReputation(uint32 i);
    void SpellEffectSummonObjectSlot(uint32 i);
    void SpellEffectDispelMechanic(uint32 i);
    void SpellEffectSummonDeadPet(uint32 i);
    void SpellEffectDestroyAllTotems(uint32 i);
    void SpellEffectSummonDemon(uint32 i);
    void SpellEffectAttackMe(uint32 i);
    void SpellEffectSkill(uint32 i);
    void SpellEffectApplyPetAura(uint32 i);
    void SpellEffectDummyMelee(uint32 i);
	void SpellEffectAreaSchoolDamage(uint32 i);
	void SpellEffectAreaHeal(uint32 i);
	void SpellEffectAreaEnergy(uint32 i);
	void SpellEffectAreaBuf(uint32 i);
	void SpellEffectSetAnimal(uint32 i);
	void SpellEffectUnSetAnimal(uint32 i);
	void SpellEffectDamage2Health(uint32 i);
	void SpellEffectSchoolDamageNearByTwo(uint32 i);
    void SpellEffectPlayerPull( uint32 i );
    void SpellEffectSpellSteal(uint32 i);
    void SpellEffectProspecting(uint32 i);			//探矿
    void SpellEffectOpenLockItem(uint32 i);
    void SpellEffectSelfResurrect(uint32 i);
    void SpellEffectDisenchant(uint32 i);
    void SpellEffectWeapon(uint32 i);
    void SpellEffectDefense(uint32 i);
    void SpellEffectDualWield(uint32 i);
    void SpellEffectSkinPlayerCorpse(uint32 i);
    void SpellEffectResurrectNew(uint32 i);
    void SpellEffectTranformItem(uint32);
    void SpellEffectEnvironmentalDamage(uint32);
    void SpellEffectLearnPetSpell(uint32 i);
    void SpellEffectEnchantHeldItem(uint32 i);
    void SpellEffectAddHonor(uint32 i);
    void SpellEffectSpawn(uint32 i);
    void SpellEffectApplyAura128(uint32 i);
	void SpellEffectTriggerSpellWithValue(uint32 i);
	void SpellEffectTriggerSpellWithRandomValue(uint32 i);
	void SpellEffectChangeGuildLeader(uint32 i);
	void SpellEffectSwapPosition(uint32 i);
	void SpellEffectGuildNoWar(uint32 i);

	void SpellEffectReflectDamage(uint32 i);
	void SpellEffectPhysAttack(uint32 i);
	void SpellEffectExtraDamage(uint32 i);

	void SpellEffectHealPct(uint32 i);
	void SpellEffectEnergizePct(uint32 i);
	void SpellEffectCloseAllAreaAura(uint32 i);
	void SpellEffectItemsChangeItem(uint32 i);
	void SpellEffectForItemsChangeItem(uint32 i);
	void SpellEffectCreateChangeItem(uint32 i);
	void SpellExtraAddTitle(uint32 i);
	void SpellDismount(uint32 i);
	void SpellEffectRefineItem( uint32 i );
	void SpellEffectAreaApplyEffect( uint32 i );
	void SpellEffectItemGift( uint32 i );
	void SpellEffectStablePet(uint32 i);

	void SpellEffectMobileBank( uint32 i );
	void SpellEffectMobileAH( uint32 i );
	void SpellEffectMobileMailbox( uint32 i );
	void SpellEffectPrivateFairground( uint32 i );
	void SpellEffectWashItem( uint32 i );

    // Spell Targets Handlers
    void SpellTargetNULL(uint32 i, uint32 j);
    void SpellTargetDefault(uint32 i, uint32 j);
    void SpellTargetSelf(uint32 i, uint32 j);
    void SpellTargetInvisibleAOE(uint32 i, uint32 j);
    void SpellTargetFriendly(uint32 i, uint32 j);
    void SpellTargetPet(uint32 i, uint32 j);
    void SpellTargetSingleTargetEnemy(uint32 i, uint32 j);
    void SpellTargetCustomAreaOfEffect(uint32 i, uint32 j);
    void SpellTargetAreaOfEffect(uint32 i, uint32 j);
    void SpellTargetLandUnderCaster(uint32 i, uint32 j); /// I don't think this is the correct name for this one
    void SpellTargetAllPartyMembersRangeNR(uint32 i, uint32 j);
    void SpellTargetSingleTargetFriend(uint32 i, uint32 j);
    void SpellTargetAoE(uint32 i, uint32 j); // something special
    void SpellTargetSingleGameobjectTarget(uint32 i, uint32 j);
    void SpellTargetInFrontOfCaster(uint32 i, uint32 j);
    void SpellTargetSingleFriend(uint32 i, uint32 j);
    void SpellTargetGameobject_itemTarget(uint32 i, uint32 j);
    void SpellTargetPetOwner(uint32 i, uint32 j);
    void SpellTargetEnemysAreaOfEffect(uint32 i, uint32 j);
    void SpellTargetTypeTAOE(uint32 i, uint32 j);
    void SpellTargetAllFriendlyAroundCaster(uint32 i, uint32 j);
    void SpellTargetScriptedEffects(uint32 i, uint32 j);
    void SpellTargetSummon(uint32 i, uint32 j);
    void SpellTargetNearbyPartyMembers(uint32 i, uint32 j);
    void SpellTargetSingleTargetPartyMember(uint32 i, uint32 j);
    void SpellTargetScriptedEffects2(uint32 i, uint32 j);
    void SpellTargetPartyMember(uint32 i, uint32 j);
    void SpellTargetDummyTarget(uint32 i, uint32 j);
    void SpellTargetFishing(uint32 i, uint32 j);
    void SpellTargetType40(uint32 i, uint32 j);
    void SpellTargetTotem(uint32 i, uint32 j);
    void SpellTargetChainTargeting(uint32 i, uint32 j);
    void SpellTargetSimpleTargetAdd(uint32 i, uint32 j);
    void SpellTarget56(uint32 o, uint32 j);
    void SpellTargetTargetAreaSelectedUnit(uint32 i, uint32 j);
    void SpellTargetInFrontOfCaster2(uint32 i, uint32 j);
    void SpellTargetTargetPartyMember(uint32 i, uint32 j);
    void SpellTargetSameGroupSameClass(uint32 i, uint32 j);
	void SpellTargetSameGuildMembers(uint32 i, uint32 j);
	void SpellTargetAllOfMembersInSameGuild(uint32 i, uint32 j);
	void SpellTargetAllFriendlyInArea( uint32 i, uint32 j );
	void SpellTargetSingleTargetGroupMemberOutOfRange( uint32 i, uint32 j );
	
    void Heal(int32 amount, bool ForceCrit = false, float jump_reduce = 0);

    GameObject*		g_caster;
    Unit*			u_caster;
    Item*			i_caster;
    Player*			p_caster;
    Object*			m_caster;
	bool			m_external;
	bool			m_effects_hide[3];

    // 15007 = resurecting sickness
	
	// This returns SPELL_ENTRY_Spell_Dmg_Type where 0 = SPELL_DMG_TYPE_NONE, 1 = SPELL_DMG_TYPE_MAGIC, 2 = SPELL_DMG_TYPE_MELEE, 3 = SPELL_DMG_TYPE_RANGED
	// It should NOT be used for weapon_damage_type which needs: 0 = MELEE, 1 = OFFHAND, 2 = RANGED
	SUNYOU_INLINE uint32 GetType() { return ( m_spellInfo->Spell_Dmg_Type == SPELL_DMG_TYPE_NONE ? SPELL_DMG_TYPE_MAGIC : m_spellInfo->Spell_Dmg_Type ); }

    std::vector<uint64> UniqueTargets;
    SpellTargetsList    ModeratedTargets;
	std::set<uint64>    m_SetModeratedTargets;

    SUNYOU_INLINE Item* GetItemTarget() { return itemTarget; }
    SUNYOU_INLINE Unit* GetUnitTarget() { return unitTarget; }
    SUNYOU_INLINE Player* GetPlayerTarget() { return playerTarget; }
    SUNYOU_INLINE GameObject* GetGameObjectTarget() { return gameObjTarget; }

    int chaindamage;
    // -------------------------------------------

    bool IsAspect();
    bool IsSeal();

    uint32 GetDuration();    

    SUNYOU_INLINE float GetRadius(uint32 i)
    {
        if(bRadSet[i])return Rad[i];
        bRadSet[i]=true;
        Rad[i]=::GetRadius(dbcSpellRadius.LookupEntry(m_spellInfo->EffectRadiusIndex[i]));
        if(m_spellInfo->SpellGroupType && u_caster)
        {
            SM_FFValue(u_caster->SM_FRadius,&Rad[i],m_spellInfo->SpellGroupType);
            SM_PFValue(u_caster->SM_PRadius,&Rad[i],m_spellInfo->SpellGroupType);
        }

        return Rad[i];
    }

    SUNYOU_INLINE static uint32 GetBaseThreat(uint32 dmg)
    {
        //there should be a formula to determine what spell cause threat and which don't
/*        switch(m_spellInfo->NameHash)
        {
            //hunter's mark
            case 4287212498:
                {
                    return 0;
                }break;
        }*/
        return dmg;
    }
    bool IsStealthSpell();
    bool IsInvisibilitySpell();
    
    int32 damage;
    Aura* m_triggeredByAura;
	signed int	forced_basepoints[3]; //some talent inherit base points from previous caster spells

    bool m_triggeredSpell;
    bool m_AreaAura;
    //uint32 TriggerSpellId;  // used to set next spell to use
    //uint64 TriggerSpellTarget; // used to set next spell target
    bool m_requiresCP;
    float m_castPositionX;
    float m_castPositionY;
    float m_castPositionZ;
   
    int32 damageToHit;
    uint32 castedItemId;
    bool judgement;
	uint8 extra_cast_number;

    void SendCastSuccess(Object * target);
    void SendCastSuccess(const uint64& guid);

    bool duelSpell;

	SUNYOU_INLINE void safe_cancel()
	{
		m_cancelled = true;
	}

    /// Spell state's
    /// Spell failed
    SUNYOU_INLINE bool GetSpellFailed(){return m_Spell_Failed;}
    SUNYOU_INLINE void SetSpellFailed(bool failed = true){m_Spell_Failed = failed;}

    SUNYOU_INLINE bool IsReflected() {return m_IsReflected;}
    SUNYOU_INLINE void SetReflected(bool reflected = true) {m_IsReflected = reflected;}
    
    /// Spell possibility's
    SUNYOU_INLINE bool GetCanReflect() {return m_CanRelect;}
    SUNYOU_INLINE void SetCanReflect(bool reflect = true) {m_CanRelect = reflect;}


	Spell * m_reflectedParent;
    int32   m_timer;
protected:

    /// Spell state's
    bool    m_usesMana;
    bool    m_Spell_Failed;        //for 5sr
    bool    m_IsReflected;
    bool    m_Delayed;
    uint32	m_nSpellDelayed;
    
    // Spell possibility's
    bool m_CanRelect;
    
    bool m_IsCastedOnSelf;

    bool hadEffect;

    uint32  m_spellState;
    int32   m_castTime;
 
    
    

    // Current Targets to be used in effect handler
    Unit*       unitTarget;
    Item*       itemTarget;
    GameObject* gameObjTarget;
    Player*     playerTarget;
    Corpse*     corpseTarget;
    uint32      add_damage;

	uint64		EffectTargetGUID;

    uint8       cancastresult;
    uint32      Dur;
    bool        bDurSet;
    float       Rad[3];
    bool        bRadSet[3];
	bool        m_cancelled;
	bool m_isCasting;
    //void _DamageRangeUpdate();

	SUNYOU_INLINE bool HasTarget(const uint64& guid, TargetsList* tmpMap)
	{
		for(TargetsList::iterator itr = tmpMap->begin(); itr != tmpMap->end(); ++itr)
			if((*itr)==guid)
				return true;

		for(SpellTargetsList::iterator itr = ModeratedTargets.begin(); itr != ModeratedTargets.end(); ++itr)
			if((*itr).TargetGuid==guid)
				return true;

		return false;
	}

private:
    TargetsList m_targetUnits[3];
    void SafeAddTarget(TargetsList* tgt,uint64 guid);
    
    void SafeAddMissedTarget(uint64 guid);
    void SafeAddModeratedTarget(uint64 guid, uint16 type);

    friend class DynamicObject;
    void DetermineSkillUp(uint32 skillid,uint32 targetlevel);
    void DetermineSkillUp(uint32 skillid);
};

void ApplyDiminishingReturnTimer(uint32 * Duration, Unit * Target, SpellEntry * spell);
void UnapplyDiminishingReturnTimer(Unit * Target, SpellEntry * spell);
uint32 GetDiminishingGroup(uint32 NameHash);

#endif
