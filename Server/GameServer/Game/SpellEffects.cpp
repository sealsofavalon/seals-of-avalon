#include "StdAfx.h"

#include "../../../SDBase/Protocol/S2C_Spell.h"
#include "../../../SDBase/Protocol/S2C_Move.h"
#include "../../../SDBase/Protocol/S2C_Mail.h"
#include "../../../SDBase/Protocol/S2C_Pet.h"
#include "ObjectStorage.h"
#include <math.h>
#include "SunyouRaid.h"
#include "DynamicObject.h"
#include "QuestMgr.h"
#include "InstanceQueue.h"

pSpellEffect SpellEffectsHandler[TOTAL_SPELL_EFFECTS]={
		&Spell::SpellEffectNULL,//SPELL_EFFECT_NULL - 0
		&Spell::SpellEffectInstantKill,//SPELL_EFFECT_INSTAKILL - 1
		&Spell::SpellEffectSchoolDMG,//SPELL_EFFECT_SCHOOL_DAMAGE - 2
		&Spell::SpellEffectDummy,//SPELL_EFFECT_DUMMY - 3
		&Spell::SpellEffectNULL,//SPELL_EFFECT_PORTAL_TELEPORT - 4
		&Spell::SpellEffectTeleportUnits,//SPELL_EFFECT_TELEPORT_UNITS - 5
		&Spell::SpellEffectApplyAura,//SPELL_EFFECT_APPLY_AURA - 6
		&Spell::SpellEffectEnvironmentalDamage,//SPELL_EFFECT_ENVIRONMENTAL_DAMAGE - 7
		&Spell::SpellEffectPowerDrain,//SPELL_EFFECT_POWER_DRAIN - 8
		&Spell::SpellEffectHealthLeech,//SPELL_EFFECT_HEALTH_LEECH - 9
		&Spell::SpellEffectHeal,//SPELL_EFFECT_HEAL - 10
		&Spell::SpellEffectNULL,//SPELL_EFFECT_BIND - 11
		&Spell::SpellEffectNULL,//SPELL_EFFECT_PORTAL - 12
		&Spell::SpellEffectNULL,//SPELL_EFFECT_RITUAL_BASE - 13
		&Spell::SpellEffectNULL,//SPELL_EFFECT_RITUAL_SPECIALIZE - 14
		&Spell::SpellEffectNULL,//SPELL_EFFECT_RITUAL_ACTIVATE_PORTAL - 15
		&Spell::SpellEffectQuestComplete,//SPELL_EFFECT_QUEST_COMPLETE - 16
		&Spell::SpellEffectWeapondamageNoschool,//SPELL_EFFECT_WEAPON_DAMAGE_NOSCHOOL - 17
		&Spell::SpellEffectResurrect,//SPELL_EFFECT_RESURRECT - 18
		&Spell::SpellEffectAddExtraAttacks,//SPELL_EFFECT_ADD_EXTRA_ATTACKS - 19
		&Spell::SpellEffectDodge,//SPELL_EFFECT_DODGE - 20
		&Spell::SpellEffectNULL,//SPELL_EFFECT_EVADE - 21
		&Spell::SpellEffectParry,//SPELL_EFFECT_PARRY - 22
		&Spell::SpellEffectBlock,//SPELL_EFFECT_BLOCK - 23
		&Spell::SpellEffectCreateItem,//SPELL_EFFECT_CREATE_ITEM - 24
		&Spell::SpellEffectWeapon,//SPELL_EFFECT_WEAPON - 25
		&Spell::SpellEffectDefense,//SPELL_EFFECT_DEFENSE - 26
		&Spell::SpellEffectPersistentAA,//SPELL_EFFECT_PERSISTENT_AREA_AURA - 27
		&Spell::SpellEffectSummon,//SPELL_EFFECT_SUMMON - 28
		&Spell::SpellEffectLeap,//SPELL_EFFECT_LEAP - 29
		&Spell::SpellEffectEnergize,//SPELL_EFFECT_ENERGIZE - 30
		&Spell::SpellEffectWeaponDmgPerc,//SPELL_EFFECT_WEAPON_PERCENT_DAMAGE - 31
		&Spell::SpellEffectTriggerMissile,//SPELL_EFFECT_TRIGGER_MISSILE - 32
		&Spell::SpellEffectOpenLock,//SPELL_EFFECT_OPEN_LOCK - 33
		&Spell::SpellEffectTranformItem,//SPELL_EFFECT_TRANSFORM_ITEM - 34
		&Spell::SpellEffectApplyAA,//SPELL_EFFECT_APPLY_AREA_AURA - 35
		&Spell::SpellEffectLearnSpell,//SPELL_EFFECT_LEARN_SPELL - 36
		&Spell::SpellEffectSpellDefense,//SPELL_EFFECT_SPELL_DEFENSE - 37
		&Spell::SpellEffectDispel,//SPELL_EFFECT_DISPEL - 38
		&Spell::SpellEffectNULL,//SPELL_EFFECT_LANGUAGE - 39
		&Spell::SpellEffectDualWield,//SPELL_EFFECT_DUAL_WIELD - 40
		&Spell::SpellEffectSummonWild,//SPELL_EFFECT_SUMMON_WILD - 41
		&Spell::SpellEffectSummonGuardian,//SPELL_EFFECT_SUMMON_GUARDIAN - 42
		&Spell::SpellEffectNULL,//SPELL_EFFECT_TELEPORT_UNITS_FACE_CASTER - 43
		&Spell::SpellEffectSkillStep,//SPELL_EFFECT_SKILL_STEP - 44
		&Spell::SpellEffectAddHonor,//SPELL_EFFECT_ADD_HONOR - 45
		&Spell::SpellEffectSpawn,//SPELL_EFFECT_SPAWN - 46
		&Spell::SpellEffectNULL,//SPELL_EFFECT_TRADE_SKILL - 47
		&Spell::SpellEffectNULL,//SPELL_EFFECT_STEALTH - 48
		&Spell::SpellEffectNULL,//SPELL_EFFECT_DETECT - 49
		&Spell::SpellEffectSummonObject,//SPELL_EFFECT_SUMMON_OBJECT - 50
		&Spell::SpellEffectNULL,//SPELL_EFFECT_FORCE_CRITICAL_HIT - 51 NA
		&Spell::SpellEffectNULL,//SPELL_EFFECT_GUARANTEE_HIT - 52 NA
		&Spell::SpellEffectEnchantItem,//SPELL_EFFECT_ENCHANT_ITEM - 53
		&Spell::SpellEffectEnchantItemTemporary,//SPELL_EFFECT_ENCHANT_ITEM_TEMPORARY - 54
		&Spell::SpellEffectTameCreature,//SPELL_EFFECT_TAMECREATURE - 55
		&Spell::SpellEffectSummonPet,//SPELL_EFFECT_SUMMON_PET	- 56
		&Spell::SpellEffectLearnPetSpell,//SPELL_EFFECT_LEARN_PET_SPELL - 57
		&Spell::SpellEffectWeapondamage,//SPELL_EFFECT_WEAPON_DAMAGE - 58
		&Spell::SpellEffectOpenLockItem,//SPELL_EFFECT_OPEN_LOCK_ITEM - 59
		&Spell::SpellEffectProficiency,//SPELL_EFFECT_PROFICIENCY - 60
		&Spell::SpellEffectSendEvent,//SPELL_EFFECT_SEND_EVENT - 61
		&Spell::SpellEffectPowerBurn,//SPELL_EFFECT_POWER_BURN - 62
		&Spell::SpellEffectThreat,//SPELL_EFFECT_THREAT - 63
		&Spell::SpellEffectTriggerSpell,//SPELL_EFFECT_TRIGGER_SPELL - 64
		&Spell::SpellEffectHealthFunnel,//SPELL_EFFECT_HEALTH_FUNNEL - 65
		&Spell::SpellEffectPowerFunnel,//SPELL_EFFECT_POWER_FUNNEL - 66
		&Spell::SpellEffectHealMaxHealth,//SPELL_EFFECT_HEAL_MAX_HEALTH - 67
		&Spell::SpellEffectInterruptCast,//SPELL_EFFECT_INTERRUPT_CAST - 68
		&Spell::SpellEffectDistract,//SPELL_EFFECT_DISTRACT - 69
		&Spell::SpellEffectNULL,//SPELL_EFFECT_PULL - 70
		&Spell::SpellEffectPickpocket,//SPELL_EFFECT_PICKPOCKET - 71
		&Spell::SpellEffectAddFarsight,//SPELL_EFFECT_ADD_FARSIGHT - 72
		&Spell::SpellEffectSummonPossessed,//SPELL_EFFECT_SUMMON_POSSESSED - 73
		&Spell::SpellEffectCreateSummonTotem,//SPELL_EFFECT_SUMMON_TOTEM - 74
		&Spell::SpellEffectHealMechanical,//SPELL_EFFECT_HEAL_MECHANICAL - 75
		&Spell::SpellEffectSummonObjectWild,//SPELL_EFFECT_SUMMON_OBJECT_WILD - 76
		&Spell::SpellEffectScriptEffect,//SPELL_EFFECT_SCRIPT_EFFECT - 77
		&Spell::SpellEffectNULL,//SPELL_EFFECT_ATTACK - 78
		&Spell::SpellEffectSanctuary,//SPELL_EFFECT_SANCTUARY - 79
		&Spell::SpellEffectAddComboPoints,//SPELL_EFFECT_ADD_COMBO_POINTS - 80
		&Spell::SpellEffectCreateHouse,//SPELL_EFFECT_CREATE_HOUSE - 81
		&Spell::SpellEffectNULL,//SPELL_EFFECT_BIND_SIGHT - 82
		&Spell::SpellEffectDuel,//SPELL_EFFECT_DUEL - 83
		&Spell::SpellEffectStuck,//SPELL_EFFECT_STUCK - 84
		&Spell::SpellEffectSummonPlayer,//SPELL_EFFECT_SUMMON_PLAYER - 85
		&Spell::SpellEffectActivateObject,//SPELL_EFFECT_ACTIVATE_OBJECT - 86
		&Spell::SpellEffectSummonTotem,//SPELL_EFFECT_SUMMON_TOTEM_SLOT1 - 87
		&Spell::SpellEffectSummonTotem,//SPELL_EFFECT_SUMMON_TOTEM_SLOT2 - 88
		&Spell::SpellEffectSummonTotem,//SPELL_EFFECT_SUMMON_TOTEM_SLOT3 - 89
		&Spell::SpellEffectSummonTotem,//SPELL_EFFECT_SUMMON_TOTEM_SLOT4 - 90
		&Spell::SpellEffectNULL,//SPELL_EFFECT_THREAT_ALL - 91 UNUSED
		&Spell::SpellEffectEnchantHeldItem,//SPELL_EFFECT_ENCHANT_HELD_ITEM - 92
		&Spell::SpellEffectNULL,//SPELL_EFFECT_SUMMON_PHANTASM - 93 OLD
		&Spell::SpellEffectSelfResurrect,//SPELL_EFFECT_SELF_RESURRECT - 94
		&Spell::SpellEffectSkinning,//SPELL_EFFECT_SKINNING - 95
		&Spell::SpellEffectCharge,//SPELL_EFFECT_CHARGE - 96
		&Spell::SpellEffectSummonCritter,//SPELL_EFFECT_SUMMON_CRITTER - 97
		&Spell::SpellEffectKnockBack,//SPELL_EFFECT_KNOCK_BACK - 98
		&Spell::SpellEffectDisenchant,//SPELL_EFFECT_DISENCHANT - 99
		&Spell::SpellEffectInebriate,//SPELL_EFFECT_INEBRIATE - 100
		&Spell::SpellEffectFeedPet,//SPELL_EFFECT_FEED_PET - 101
		&Spell::SpellEffectDismissPet,//SPELL_EFFECT_DISMISS_PET - 102
		&Spell::SpellEffectReputation,//SPELL_EFFECT_REPUTATION - 103
		&Spell::SpellEffectSummonObjectSlot,//SPELL_EFFECT_SUMMON_OBJECT_SLOT1 - 104
		&Spell::SpellEffectSummonObjectSlot,//SPELL_EFFECT_SUMMON_OBJECT_SLOT2 - 105
		&Spell::SpellEffectSummonObjectSlot,//SPELL_EFFECT_SUMMON_OBJECT_SLOT3 - 106
		&Spell::SpellEffectSummonObjectSlot,//SPELL_EFFECT_SUMMON_OBJECT_SLOT4 - 107
		&Spell::SpellEffectDispelMechanic,//SPELL_EFFECT_DISPEL_MECHANIC - 108
		&Spell::SpellEffectSummonDeadPet,//SPELL_EFFECT_SUMMON_DEAD_PET - 109
		&Spell::SpellEffectDestroyAllTotems,//SPELL_EFFECT_DESTROY_ALL_TOTEMS - 110
		&Spell::SpellEffectNULL,//SPELL_EFFECT_DURABILITY_DAMAGE - 111
		&Spell::SpellEffectSummonDemon,//SPELL_EFFECT_SUMMON_DEMON - 112
		&Spell::SpellEffectResurrectNew,//SPELL_EFFECT_RESURRECT_NEW - 113
		&Spell::SpellEffectAttackMe,//SPELL_EFFECT_ATTACK_ME - 114
		&Spell::SpellEffectNULL,//SPELL_EFFECT_DURABILITY_DAMAGE_PCT - 115
		&Spell::SpellEffectSkinPlayerCorpse,//SPELL_EFFECT_SKIN_PLAYER_CORPSE - 116
		&Spell::SpellEffectNULL,//SPELL_EFFECT_SPIRIT_HEAL - 117//Not used
		&Spell::SpellEffectSkill,//SPELL_EFFECT_SKILL - 118
		&Spell::SpellEffectApplyPetAura,//SPELL_EFFECT_APPLY_PET_AURA - 119
		&Spell::SpellEffectNULL,//SPELL_EFFECT_TELEPORT_GRAVEYARD - 120//Not used
		&Spell::SpellEffectDummyMelee,//SPELL_EFFECT_DUMMYMELEE	- 121
		&Spell::SpellEffectAreaSchoolDamage,//unknown - 122 //not used
		&Spell::SpellEffectAreaHeal,//SPELL_EFFECT_FILMING - 123 // http://www.thottbot.com/?sp=27998: flightpath 
		&Spell::SpellEffectPlayerPull, // SPELL_EFFECT_PLAYER_PULL - 124 - http://thottbot.com/e2312
		&Spell::SpellEffectAreaEnergy,//unknown - 125 // Reduce Threat by % //http://www.thottbot.com/?sp=32835
		&Spell::SpellEffectSpellSteal,//SPELL_EFFECT_SPELL_STEAL - 126 // Steal Beneficial Buff (Magic) //http://www.thottbot.com/?sp=30449
		&Spell::SpellEffectProspecting,//unknown - 127 // Search 5 ore of a base metal for precious gems.  This will destroy the ore in the process.
		&Spell::SpellEffectApplyAura128,//unknown - 128 // Adjust a stats by %: Mod Stat // ITS FLAT
		&Spell::SpellEffectAreaBuf,// unknown - 129 // Mod Dmg % (Spells)
		&Spell::SpellEffectSetAnimal,// unknown - 130 // http://www.thottbot.com/s34477
		&Spell::SpellEffectUnSetAnimal,// unknown - 131 // test spell
		&Spell::SpellEffectHealPct,// unknown - 132 // no spells
		&Spell::SpellEffectPowerBrunFullDamage,// SPELL_EFFECT_FORGET_SPECIALIZATION - 133 // http://www.thottbot.com/s36441 // I think this is a gm/npc spell
		&Spell::SpellEffectSchoolDamageNearByTwo,// SPELL_EFFECT_SCHOOL_DAMAGE_NEARBYTWO - 134 // related to summoning objects and removing them, http://www.thottbot.com/s39161
		&Spell::SpellEffectDamage2Health,// unknown - 135 // no spells
		&Spell::SpellEffectAddPetToList,// SPELL_EFFECT_ADDPETTOLIST - 136 // http://www.thottbot.com/s41542 and http://www.thottbot.com/s39703
		&Spell::SpellEffectRecordPos,// unknown - 137 // http://www.thottbot.com/s41542
		&Spell::SpellEffectEnergizePct,// unknown - 138 // related to superjump or even "*jump" spells http://www.thottbot.com/?e=Unknown%20138
		&Spell::SpellAuraExtraXPRatio,// unknown - 139 // no spells
		&Spell::SpellEffectTeleportUnits,//SPELL_EFFECT_TELEPORT_UNITS - 140 IronForge teleport / portal only it seems
		&Spell::SpellEffectReflectDamage,// unknown - 141 // triggers spell, magic one,  (Mother spell) http://www.thottbot.com/s41065
		&Spell::SpellEffectTriggerSpellWithValue,// SPELL_EFFECT_TRIGGER_SPELL_WITH_VALUE - 142 // triggers some kind of "Put spell on target" thing... (dono for sure) http://www.thottbot.com/s40872 and http://www.thottbot.com/s33076
		&Spell::SpellEffectTriggerXP,// unknown - 143 // Master -> deamon effecting spell, http://www.thottbot.com/s25228 and http://www.thottbot.com/s35696
		&Spell::SpellEffectBanYueZhan,
		&Spell::SpellEffectHealMana,
		&Spell::SpellEffectChongzhuang,
		&Spell::SpellEffectConsume2Gift,
		&Spell::SpellEffectPhysAttack,	//SPELL_EFFECT_PHYSATTACK
		&Spell::SpellEffectExtraDamage, // SPELL_EFFECT_EXTRA_DAMAGE
		&Spell::SpellExtraAddTitle,
		&Spell::SpellEffectCloseAllAreaAura,
		&Spell::SpellDismount,
		&Spell::SpellEffectPosDispel,		//SPELL_EFFECT_DISPEL_POS_AURA
		&Spell::SpellEffectNegDispel,		//SPELL_EFFECT_DISPEL_NEG_AURA
		&Spell::SpellEffectTriggerSpellWithRandomValue, // SPELL_EFFECT_TRIGGER_SPELL_WITH_RANDOM_VALUE
		&Spell::SpellEffectChainDamageWithChangeValue,  //SPELL_EFFECT_CHAIN_DAMAGE_WITH_CHANGE_VALUE
		&Spell::SpellEffectItemsChangeItem,				//SPELL_EFFECT_ITEMS_CHANGE_ITEM
		&Spell::SpellEffectCreateChangeItem,				//SPELL_EFFECT_CREATE_CHANGE_ITEM
		&Spell::SpellEffectForItemsChangeItem,				//SPELL_EFFECT_FOR_ITEMS_CHANGE_ITEM
		&Spell::SpellEffectRefineItem,						//SPELL_EFFECT_REFINE_ITEM
		&Spell::SpellEffectChangeGuildLeader,				//SPELL_EFFECT_CHANGE_GUILD_MASTER
		&Spell::SpellEffectSkillCreateItem,					//SPELL_EFFECT_SKILL_CREATE_ITEM
		&Spell::SpellEffectSwapPosition,					// SPELL_EFFECT_SWAP_POSITION
		&Spell::SpellEffectAreaApplyEffect,					// SPELL_EFFECT_AREA_APPLY_EFFECT
		&Spell::SpellEffectDispelAuraById,					// SPELL_EFFECT_DISPEL_AURA_BY_ID
		&Spell::SpellEffectItemGift	,						// SPELL_EFFECT_ITEM_GIFT
		&Spell::SpellEffectStablePet,						// SPELL_EFFECT_STABLEPET
		&Spell::SpellEffectGuildNoWar,						// SPELL_EFFECT_GUILD_NOWAR
		&Spell::SpellEffectMobileBank,						// SPELL_EFFECT_MOBILE_BANK,				// 169
		&Spell::SpellEffectMobileAH,						// SPELL_EFFECT_MOBILE_AH,					// 170
		&Spell::SpellEffectMobileMailbox,					// SPELL_EFFECT_MOBILE_MAILBOX,			// 171
		&Spell::SpellEffectPrivateFairground,				// SPELL_EFFECT_PRIVATE_FAIRGROUND		// 172
		&Spell::SpellEffectWashItem,						// SPELL_EFFECT_WASH_ITEM
};

void Spell::SpellEffectPhysAttack(uint32 i)
{
	float time = 0.0f;

	time = (u_caster->CalcDistance(unitTarget) * 1000.0f) / m_spellInfo->speed;

	if (m_spellInfo->Spell_Dmg_Type == SPELL_DMG_TYPE_MAGIC)
	{
		return;
	}
	else
	{
		int dmg = damage;
		if (m_spellInfo->EffectDieSides[i] > 0)
		{
			int n = RandomUInt() % m_spellInfo->EffectDieSides[i];
			dmg += n;
		}

		if(m_spellInfo->speed > 0.01f)
		{
			sEventMgr.AddEvent(u_caster,&Unit::EventStrikeWithAbility,(uint64)unitTarget->GetGUID(),
				(SpellEntry*)m_spellInfo,(int32)dmg, (int32)0, (uint32)0, false, false, true, true, false,  EVENT_SPELL_DAMAGE_HIT, uint32(time), 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
		}
		else
		{
			u_caster->Strike( unitTarget,( GetType() == SPELL_DMG_TYPE_RANGED ? RANGED : MELEE ), m_spellInfo, dmg, 0, 0, false, false, true, true , false);
		}
	}
}
void Spell::SpellExtraAddTitle(uint32 i)
{
	((Player*)m_caster)->InsertTitle(m_spellInfo->EffectMiscValue[i], m_spellInfo->EffectBasePoints[i]);
	MSG_S2C::stTitleAdd msg;
	msg.title.title = m_spellInfo->EffectMiscValue[i];
	((Player*)m_caster)->GetSession()->SendPacket(msg);
}
void Spell::SpellEffectExtraDamage(uint32 i)
{
	if (!unitTarget)
		return;

	unitTarget->extradamage += damage;
}

void Spell::SpellEffectReflectDamage(uint32 i)
{
	m_caster->DealDamage(unitTarget, ceil(forced_basepoints[0] * 0.03), 0, 0, 0);
}

void Spell::SpellEffectRecordPos(uint32 i)
{
	if(i_caster)
	{
		if(!i_caster->GetUInt32Value(ITEM_FIELD_RECORD_MAPID))
		{
			i_caster->SetUInt32Value(ITEM_FIELD_RECORD_MAPID,	i_caster->GetOwner()->GetMapId());
			i_caster->SetUInt32Value(ITEM_FIELD_RECORD_ZONEID,	i_caster->GetOwner()->GetZoneId());
			i_caster->SetFloatValue(ITEM_FIELD_RECORD_POSX,	i_caster->GetOwner()->GetPositionX());
			i_caster->SetFloatValue(ITEM_FIELD_RECORD_POSY,	i_caster->GetOwner()->GetPositionY());
			i_caster->SetFloatValue(ITEM_FIELD_RECORD_POSZ,	i_caster->GetOwner()->GetPositionZ());
			MSG_S2C::stOnItemFieldRecordChange msg;
			msg.entry = i_caster->GetEntry();
			msg.zoneid = i_caster->GetOwner()->GetZoneId();
			i_caster->GetOwner()->GetSession()->SendPacket( msg );
			i_caster->m_isDirty = true;
		}
		else
		{
			i_caster->GetOwner()->SafeTeleport(i_caster->GetUInt32Value(ITEM_FIELD_RECORD_MAPID), 0, i_caster->GetFloatValue(ITEM_FIELD_RECORD_POSX), 
				i_caster->GetFloatValue(ITEM_FIELD_RECORD_POSY), i_caster->GetFloatValue(ITEM_FIELD_RECORD_POSZ), 0.0f);
		}
	}
}

void Spell::SpellEffectConsume2Gift(uint32 i)
{
	if(m_spellInfo->EffectMiscValue[i])
	{
		stConsume2GiftEntry* pEntry = Consume2GiftStorage.LookupEntry(m_spellInfo->EffectMiscValue[i]);
		if(!pEntry)
		{
			MyLog::log->error("not found consume2gift id[%d] in db", m_spellInfo->EffectMiscValue[i]);
			return;
		}

		for(int i = 0; i < 5; i++)
		{
			if(pEntry->item[i] && pEntry->itemcount[i])
			{
				ItemPrototype* it = ItemPrototypeStorage.LookupEntry(pEntry->item[i]);
				if(it)
				{
					Item *item;
					item = objmgr.CreateItem( pEntry->item[i], p_caster);
					item->SetUInt32Value(ITEM_FIELD_STACK_COUNT, pEntry->itemcount[i]);
					if(it->Bonding==ITEM_BIND_ON_PICKUP)
						item->SoulBind();
					
					if(!p_caster->GetItemInterface()->AddItemToFreeSlot(item))
					{
						delete item;
						return;
					}
				}
			}
		}
	}
}

void Spell::SpellEffectChongzhuang(uint32 i)
{
	if(m_spellInfo->speed > 0 && unitTarget)
	{
		float dist = m_caster->CalcDistance( unitTarget ) - 0.5f;
		float time = ((dist*1000.0f)/m_spellInfo->speed);

		float dx = m_caster->GetPositionX() - m_targets.m_destX;
		float dy = m_caster->GetPositionY() - m_targets.m_destY;
		float dz = m_caster->GetPositionZ() - m_targets.m_destZ;
		if( dx*dx + dy*dy + dz*dz > 500 )
		{
			return;
		}
		sEventMgr.AddEvent( static_cast< Unit* >( m_caster ), &Unit::EventChongzhuang, static_cast< Unit* >( unitTarget ), m_spellInfo, m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ, EVENT_COMBAT_TIMER, (uint32)time, 1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT );
	}
}

void Spell::SpellEffectTriggerXP(uint32 i)
{
	if(p_caster)
		p_caster->m_bTriggerXP = true;
}
void Spell::SpellEffectHealMana(uint32 i)
{
	
}
void Spell::SpellEffectBanYueZhan(uint32 i )
{

}
void Spell::SpellEffectNULL(uint32 i)
{
	MyLog::log->debug("Unhandled spell effect %u in spell %u.\n",m_spellInfo->Effect[i],m_spellInfo->Id);
}

void Spell::SpellEffectInstantKill(uint32 i)
{
	if(!unitTarget || !unitTarget->isAlive())
		return;

	//Sacrifice: if spell caster has "void walker" pet, pet dies and spell caster gets a 
	/*Sacrifices the Voidwalker, giving its owner a shield that will absorb 
	305 damage for 30 sec. While the shield holds, spellcasting will not be \
	interrupted by damage.*/

	/*
	Demonic Sacrifice

	When activated, sacrifices your summoned demon to grant you an effect that lasts 
	30 minutes. The effect is canceled if any Demon is summoned. 
	Imp: Increases your Fire damage by 15%. 
	Voidwalker: Restores 3% of total Health every 4 sec. 
	Succubus: Increases your Shadow damage by 15%. 
	Felhunter: Restores 2% of total Mana every 4 sec.

	When activated, sacrifices your summoned demon to grant you an effect that lasts $18789d.  The effect is canceled if any Demon is summoned.

	Imp: Increases your Fire damage by $18789s1%.

	Voidwalker: Restores $18790s1% of total Health every $18790t1 sec.

	Succubus: Increases your Shadow damage by $18791s1%.

	Felhunter: Restores $18792s1% of total Mana every $18792t1 sec.

	*/
	uint32 spellId = m_spellInfo->Id;

	switch( m_spellInfo->NameHash )
	{
	case SPELL_HASH_SACRIFICE:
		{
			if( !u_caster->IsPet() )
				return;

			static_cast<Pet*>(u_caster)->Dismiss( true );
			return;

		}break;

	default:
		{
			// moar cheaters
			if( p_caster == NULL || (u_caster != NULL && u_caster->IsPet() ) )
				return;

			if( p_caster->GetSession()->GetPermissionCount() == 0 )
				return;
		}
	}
	//instant kill effects don't have a log
	//m_caster->SpellNonMeleeDamageLog(unitTarget, m_spellInfo->Id, unitTarget->GetUInt32Value(UNIT_FIELD_HEALTH), true);
	m_caster->DealDamage(unitTarget, unitTarget->GetUInt32Value(UNIT_FIELD_HEALTH), 0, 0, 0);
}

void Spell::SpellEffectChainDamageWithChangeValue(uint32 i)
{
	if(!unitTarget || !unitTarget->isAlive())
		return;

	if(unitTarget->SchoolImmunityList[m_spellInfo->School])
	{
		SendCastResult(SPELL_FAILED_IMMUNE);
		return;
	}


	float scale = float(m_spellInfo->EffectDieSides[i]) / 100;
	uint32 dmg = damage;
	float TempScale = 0.0f;
	TempScale = scale;

	for (int x = 0;  x < m_targetUnits[i].size(); x ++)
	{		
		if (m_targetUnits[i][x]== unitTarget->GetGUID())
		{
			if (x)
			{
				dmg =  float(damage) * TempScale;
			}	
			break;
		}
		if (x)
		{
			TempScale *= scale;
		}
	}

	float dist = 0.0f;
	float time = 0.0f;
	if(m_spellInfo->speed > 0)
	{
		damageToHit = dmg;
		dist = m_caster->CalcDistance( unitTarget );
		time = ((dist*1000.0f)/m_spellInfo->speed);
	}

	if (m_spellInfo->Spell_Dmg_Type == SPELL_DMG_TYPE_MAGIC)
	{
		if (m_spellInfo->speed > 0.01f)
		{
			sEventMgr.AddEvent(m_caster, &Object::EventSpellDamage, unitTarget->GetGUID(), 
				m_spellInfo->Id, (uint32)dmg, m_curattackcnt,1.0f, EVENT_SPELL_DAMAGE_HIT, uint32(time), 1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
		}
		else
		{
			m_caster->SpellNonMeleeDamageLog(unitTarget,m_spellInfo->Id, dmg, true);
		}
	}
	else
	{
		if(m_spellInfo->speed > 0)
		{
			float dist = m_caster->CalcDistance( unitTarget );
			float time = ((dist*1000.0f)/m_spellInfo->speed);
			damageToHit = dmg;

			sEventMgr.AddEvent(u_caster,&Unit::EventStrikeWithAbility,unitTarget->GetGUID(),
				m_spellInfo, (int32)0, (int32)0, (uint32)dmg, false, false, false, true, false, EVENT_SPELL_DAMAGE_HIT, uint32(time), 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
		}
		else
		{
			u_caster->Strike(unitTarget, ( GetType() == SPELL_DMG_TYPE_RANGED ? RANGED : MELEE ), m_spellInfo, 0, 0,dmg, false , false, true, false, false);	
		}
	}

}

void Spell::SpellEffectSchoolDMG(uint32 i) // dmg school
{
	if(!unitTarget || !unitTarget->isAlive())
		return;

	if(unitTarget->SchoolImmunityList[m_spellInfo->School])
	{
		SendCastResult(SPELL_FAILED_IMMUNE);
		return;
	}
   
	uint32 dmg = 0;
	bool static_damage=false;
	//dmg =  CalculateDamage(u_caster, unitTarget, 0, 0, m_spellInfo, false, m_spellInfo->EffectBasePoints[i], true, true, false);



	float fReduce = 1.0f;
	if(m_spellInfo->EffectChainTarget[i])//chain
	{
		int32 reduce = (int32)(m_spellInfo->EffectDieSides[i]);
		
		if(reduce && chaindamage)
		{
			if(m_spellInfo->SpellGroupType && u_caster)
			{
				SM_FIValue(u_caster->SM_PJumpReduce,&reduce,m_spellInfo->SpellGroupType);
			}
			chaindamage -= chaindamage * reduce / 100;

			fReduce =float(chaindamage)/ m_spellInfo->EffectBasePoints[i];
		}
		else
		{
			chaindamage = m_spellInfo->EffectBasePoints[i];
		}

		if( chaindamage <= 0 )
			dmg = 1;
		else
			dmg = chaindamage;
	}
	else 
	{
		dmg = m_spellInfo->EffectBasePoints[i];;
		if (m_spellInfo->EffectDieSides[i] > 0)
		{
			int nrandPoint = RandomUInt()%m_spellInfo->EffectDieSides[i];
			dmg += nrandPoint;
		}
	}

	// check for no more damage left (chains)
	//if(!dmg) return;


	///*********************************************************************
	// CONFLAGRATE SHOULD REMOVE THE IMMOLATE DEBUFF
	// **********************************************************************/
	//if( m_spellInfo->NameHash == SPELL_HASH_CONFLAGRATE )
	//	if( unitTarget->HasAurasWithNameHash(SPELL_HASH_IMMOLATE) )
	//		unitTarget->RemoveAuraByNameHash(SPELL_HASH_IMMOLATE );


	///**************************************************************************
	//* This handles the correct damage of "Judgement of Command" (all ranks)
	//**************************************************************************/
	//if (m_spellInfo->NameHash == SPELL_HASH_JUDGEMENT_OF_COMMAND && !unitTarget->IsStunned())
	//		dmg = dmg >> 1;

	//dmg = damage;
	//if(p_caster) 
	//	dmg = CalculateDamage(p_caster, unitTarget, 0, 0, m_spellInfo, true, m_spellInfo->EffectBasePoints[i], true, false, false);
	//else if(u_caster)
	//	dmg = CalculateDamage(u_caster, unitTarget, 0, 0, m_spellInfo, true, m_spellInfo->EffectBasePoints[i], true, false, false);

	
	float dist = 0.0f;
	float time = 0.0f;
	if(m_spellInfo->speed > 0)
	{
		damageToHit = dmg;
		dist = m_caster->CalcDistance( unitTarget );
		time = ((dist*1000.0f)/m_spellInfo->speed);			
	}
//void SpellNonMeleeDamageLog(Unit *pVictim, uint32 spellID, uint32 damage, bool allowProc, bool static_damage = false,
//							bool no_remove_auras = false, uint16 damageseq=0, bool stato = false, float overWrite = 1.0f);
	if (m_spellInfo->Spell_Dmg_Type == SPELL_DMG_TYPE_MAGIC)
	{
		if (m_spellInfo->speed > 0.01f)
		{
			sEventMgr.AddEvent(m_caster, &Object::EventSpellDamage, unitTarget->GetGUID(), 
				m_spellInfo->Id, (uint32)dmg, m_curattackcnt,fReduce, EVENT_SPELL_DAMAGE_HIT, uint32(time), 1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
		}
		else
		{
			m_caster->SpellNonMeleeDamageLog(unitTarget,m_spellInfo->Id, dmg, true, false, false, 0, true, fReduce);
		}
	}

}

void Spell::SpellEffectDummy(uint32 i) // Dummy(Scripted events)
{
}

void Spell::SpellEffectTeleportUnits( uint32 i )  // Teleport Units
{
	uint32 spellId = m_spellInfo->Id;

	if( unitTarget == NULL )
		return;

	// Try a dummy SpellHandler
	if( sScriptMgr.CallScriptedDummySpell( spellId, i, this ) )
		return;

	// Shadowstep
	if( m_spellInfo->NameHash == SPELL_HASH_SHADOWSTEP && p_caster && p_caster->IsInWorld() )
	{
		/* this is rather tricky actually. we have to calculate the orientation of the creature/player, and then calculate a little bit of distance behind that. */
		float ang;
		if( unitTarget == m_caster )
		{
			/* try to get a selection */
 			unitTarget = m_caster->GetMapMgr()->GetUnit(p_caster->GetSelection());
//			if( (unitTarget == NULL ) || !isHostile(p_caster, unitTarget) || (unitTarget->CalcDistance(p_caster) > 25.0f)) //removed by Zack : no idea why hostile is used. Isattackable should give a wider solution range
			if( (unitTarget == NULL ) || !isAttackable(p_caster, unitTarget, !(m_spellInfo->c_is_flags & SPELL_FLAG_IS_TARGETINGSTEALTHED) ) || (unitTarget->CalcDistance(p_caster) > 25.0f))
				return;
		}

		if( unitTarget->GetTypeId() == TYPEID_UNIT )
		{
			if( unitTarget->GetUInt64Value( UNIT_FIELD_TARGET ) != 0 )
			{
				/* We're chasing a target. We have to calculate the angle to this target, this is our orientation. */
				ang = m_caster->calcAngle(m_caster->GetPositionX(), m_caster->GetPositionY(), unitTarget->GetPositionX(), unitTarget->GetPositionY());

				/* convert degree angle to radians */
				ang = ang * float(M_PI) / 180.0f;
			}
			else
			{
				/* Our orientation has already been set. */
				ang = unitTarget->GetOrientation();
			}
		}
		else
		{
			/* Players orientation is sent in movement packets */
			ang = unitTarget->GetOrientation();
		}

		// avoid teleporting into the model on scaled models
		const static float shadowstep_distance = 1.6f * unitTarget->GetFloatValue(OBJECT_FIELD_SCALE_X);
		float new_x = unitTarget->GetPositionX() - (shadowstep_distance * cosf(ang));
		float new_y = unitTarget->GetPositionY() - (shadowstep_distance * sinf(ang));
		
		/* Send a movement packet to "charge" at this target. Similar to warrior charge. */
		p_caster->z_axisposition = 0.0f;
		p_caster->SafeTeleport(p_caster->GetMapId(), p_caster->GetInstanceID(), LocationVector(new_x, new_y, (unitTarget->GetPositionZ() + 0.1f), unitTarget->GetOrientation()));


		return;
	}

	/* TODO: Remove Player From bg */

	if(unitTarget->GetTypeId() == TYPEID_PLAYER)
		HandleTeleport(spellId, unitTarget);
}

void Spell::SpellEffectApplyAura(uint32 i)  // Apply Aura
{
	if(!unitTarget)
		return;

	if(u_caster && !g_caster && m_spellInfo->speed > 0)
	{
		float dist = m_caster->CalcDistance( unitTarget );
		float time = ((dist*1000.0f)/m_spellInfo->speed);
		damageToHit = damage;
		sEventMgr.AddEvent(u_caster, &Unit::EventApplyAura, unitTarget->GetGUID(), 
			m_spellInfo->Id, damage, i, GetDuration(), EVENT_SPELL_DAMAGE_HIT, uint32(time), 1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
		return;
	}
	// can't apply stuns/fear/polymorph/root etc on boss
	if ( !playerTarget )
	{
		Creature * c = (Creature*)( unitTarget );
		if (c&&c->GetCreatureName()&&c->GetCreatureName()->Rank == ELITE_WORLDBOSS)
		{
			switch(m_spellInfo->EffectApplyAuraName[i])
			{
			case SPELL_AURA_MOD_CONFUSE:  // confuse
			case SPELL_AURA_MOD_CHARM:  // charm
			case SPELL_AURA_MOD_FEAR:  // fear
			case SPELL_AURA_MOD_STUN: // stun
			case SPELL_AURA_MOD_PACIFY: // pacify
			case SPELL_AURA_MOD_ROOT: // root
			case SPELL_AURA_MOD_SILENCE: // silence
			case SPELL_AURA_MOD_INCREASE_SPEED: // increase speed
			case SPELL_AURA_MOD_INCREASE_MOUNTED_SPEED: // decrease speed
			case SPELL_AURA_MOD_SLEEP:
				//SendCastResult(SPELL_FAILED_IMMUNE);
				return;
			}
		}
	}
	
	// avoid map corruption.
	if(unitTarget->GetInstanceID()!=m_caster->GetInstanceID())
		return;

	//check if we already have stronger aura
	Aura *pAura;

	std::map<uint32,Aura*>::iterator itr=unitTarget->tmpAura.find(m_spellInfo->Id);
	//if we do not make a check to see if the aura owner is the same as the caster then we will stack the 2 auras and they will not be visible client sided
	if(itr==unitTarget->tmpAura.end())
	{
		//buf优先级
		{
			if(m_spellInfo->EffectApplyAuraName[i] >= TOTAL_SPELL_AURAS)
			{
				MyLog::log->error("find a auraname which is bigger than %u", TOTAL_SPELL_AURAS);
				return;
			}
			uint32 oldspellid = unitTarget->m_spellAuras[m_spellInfo->EffectApplyAuraName[i]];
			if(oldspellid)
			{
				SpellEntry* pOldSpell = dbcSpell.LookupEntry(oldspellid);
				if(pOldSpell)
				{
					if(pOldSpell->SpellGroupType == m_spellInfo->SpellGroupType && pOldSpell->spellLevel > m_spellInfo->spellLevel)
					{
						return;
					}
				}
			}
		}
		uint32 Duration=this->GetDuration();
		
		// Handle diminishing returns, if it should be resisted, it'll make duration 0 here.
		if(!(m_spellInfo->Attributes & ATTRIBUTES_PASSIVE)) // Passive
			::ApplyDiminishingReturnTimer(&Duration, unitTarget, m_spellInfo);

		if(!Duration)
		{
			//maybe add some resist messege to client here ?
			//return;
			Duration = 0xFFFFFFFF;
		}
		if(g_caster && g_caster->GetUInt32Value(OBJECT_FIELD_CREATED_BY) && g_caster->m_summoner)
			pAura=new Aura(m_spellInfo, Duration, g_caster->m_summoner, unitTarget);
		else
			pAura=new Aura(m_spellInfo, Duration, m_caster, unitTarget);

		pAura->pSpellId = pSpellId; //this is required for triggered spells
		
		unitTarget->tmpAura[m_spellInfo->Id] = pAura;
		unitTarget->m_spellAuras[m_spellInfo->EffectApplyAuraName[i]] = m_spellInfo->Id;
		unitTarget->m_spellAurasEffectPtr[m_spellInfo->EffectApplyAuraName[i]] = pAura;
	}
	else
	{
		 pAura=itr->second;
	} 
	pAura->AddMod(m_spellInfo->EffectApplyAuraName[i], damage, m_spellInfo->EffectMiscValue[i], i);
}

void Spell::SpellEffectPowerDrain(uint32 i)  // Power Drain
{
	if(!unitTarget || !unitTarget->isAlive())
		return;

	uint32 powerField = UNIT_FIELD_POWER1+m_spellInfo->EffectMiscValue[i];
	uint32 curPower = unitTarget->GetUInt32Value(powerField);
	if( powerField == UNIT_FIELD_POWER1 && unitTarget->IsPlayer() )
	{
		// Resilience - reduces the effect of mana drains by (CalcRating*2)%.

		damage *= float2int32( 1 - ( ( static_cast<Player*>(unitTarget)->CalcRating( PLAYER_RATING_MODIFIER_SPELL_CRIT_RESILIENCE ) * 2 ) / 100.0f ) );
	}
	uint32 amt=damage+((u_caster->GetDamageDoneMod(m_spellInfo->School)*80)/100);
	if(amt>curPower)
	{
		amt=curPower;
	}
	unitTarget->SetUInt32Value(powerField,curPower-amt);
	uint32 m=u_caster->GetUInt32Value(UNIT_FIELD_MAXPOWER1+m_spellInfo->EffectMiscValue[i]);
	if(u_caster->GetUInt32Value(powerField)+amt>m)
		u_caster->SetUInt32Value(powerField,m);
	else
		u_caster->SetUInt32Value(powerField,u_caster->GetUInt32Value(powerField)+amt);	

	SendHealManaSpellOnPlayer(u_caster, u_caster, amt, m_spellInfo->EffectMiscValue[i]);
}

void Spell::SpellEffectHealthLeech(uint32 i) // Health Leech
{
	if(unitTarget)
	{
		if( !unitTarget->isAlive() )
		{
			MyLog::log->debug("Spell Head: target is dead");
			SendCastResult(SPELL_FAILED_TARGETS_DEAD);
			return;
		}
		if(!isFriendly(unitTarget, m_caster))
		{
			MyLog::log->debug("Spell Head: target is not friend");
			SendCastResult(SPELL_FAILED_TARGET_ENEMY);
			return;
		}
	}
	else
	{
		unitTarget = (Unit*)m_caster;
		if(m_caster->IsPlayer())
		{
			playerTarget = (Player*)m_caster;
		}
	}

	uint32 curHealth = unitTarget->GetUInt32Value(UNIT_FIELD_HEALTH);
	uint32 amt = damage;
	if(amt > curHealth)
	{
		amt = curHealth;
	}
	m_caster->DealDamage(unitTarget, damage, 0, 0, m_spellInfo->Id);

	uint32 playerCurHealth = m_caster->GetUInt32Value(UNIT_FIELD_HEALTH);
	uint32 playerMaxHealth = m_caster->GetUInt32Value(UNIT_FIELD_MAXHEALTH);

	if(playerCurHealth + amt > playerMaxHealth)
	{
		m_caster->SetUInt32Value(UNIT_FIELD_HEALTH, playerMaxHealth);
	}
	else
	{
		m_caster->SetUInt32Value(UNIT_FIELD_HEALTH, playerCurHealth + amt);		   
	}
}

void Spell::SpellEffectHeal(uint32 i) // Heal
{
	if(unitTarget)
	{
		 if( !unitTarget->isAlive() )
		 {
			 MyLog::log->debug("Spell Head: target is dead");
			 SendCastResult(SPELL_FAILED_TARGETS_DEAD);
			 return;
		 }
		if(!isFriendly(unitTarget, m_caster))
		{
			MyLog::log->debug("Spell Head: target is not friend");
			SendCastResult(SPELL_FAILED_TARGET_ENEMY);
			return;
		}
		/*
		if(unitTarget->IsCreature())
		{
			SendCastResult(SPELL_FAILED_BAD_TARGETS);
			return;
		}
		*/
	}
	else
	{
		unitTarget = (Unit*)m_caster;
		if(m_caster->IsPlayer())
		{
			playerTarget = (Player*)m_caster;
		}
	}

	int nrandPoint = 0;
	if (m_spellInfo->EffectDieSides[i] > 0)
	{
		nrandPoint = RandomUInt()%m_spellInfo->EffectDieSides[i];
	}
	
	if(m_spellInfo->EffectChainTarget[i])//chain
	{
		if(!chaindamage)
		{
			chaindamage = m_spellInfo->EffectBasePoints[i];
			Heal((int32)chaindamage + nrandPoint,false, 1.0f);
		}
		else
		{
			int32 reduce=m_spellInfo->EffectDieSides[i];
			if(m_spellInfo->SpellGroupType && u_caster)
			{
				SM_FIValue(u_caster->SM_PJumpReduce,&reduce,m_spellInfo->SpellGroupType);
			}

			chaindamage -= chaindamage * reduce / 100;
			float Freduce = float(chaindamage) / m_spellInfo->EffectBasePoints[i];				
			Heal((int32)chaindamage + nrandPoint, false, Freduce);
		}
	}
	else
	{
		Heal( damage + nrandPoint );
	}
}

void Spell::SpellEffectQuestComplete(uint32 i) // Quest Complete
{
	//misc value is id of the quest to complete
}

//wand->
void Spell::SpellEffectWeapondamageNoschool(uint32 i) // Weapon damage + (no School)
{
	if(!unitTarget ||!u_caster)
		return;

	float time;
	if (m_spellInfo->speed > 0.0f)
	{
		time = (u_caster->CalcDistance(unitTarget) * 1000.0f) / m_spellInfo->speed;
	}

	if (m_spellInfo->Spell_Dmg_Type == SPELL_DMG_TYPE_MAGIC)
	{
		return;
	}
	else
	{
		if(m_spellInfo->speed > 0.01f)
		{
			sEventMgr.AddEvent(u_caster,&Unit::EventStrikeWithAbility,unitTarget->GetGUID(),
				(SpellEntry*)m_spellInfo, damage, (int32)0, (uint32)0, false, false, false, true, true,  EVENT_SPELL_DAMAGE_HIT, float2int32(time), 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
		}
		else
		{
			damage = u_caster->Strike( unitTarget, ( GetType() == SPELL_DMG_TYPE_RANGED ? RANGED : MELEE ), m_spellInfo, damage, 0, 0, false, false, false, true , true);
		}
	}

}

void Spell::SpellEffectAddExtraAttacks(uint32 i) // Add Extra Attacks
{
	if(!u_caster)
		return;
	u_caster->m_extraattacks += damage;		
}

void Spell::SpellEffectDodge(uint32 i)
{
	//i think this actually enbles the skill to be able to dodge melee+ranged attacks
	//value is static and sets value directly which will be modified by other factors
	//this is only basic value and will be overwiten elsewhere !!!
	if( unitTarget && unitTarget->IsPlayer() )
		unitTarget->SetFloatValue( PLAYER_DODGE_PERCENTAGE, damage );
}

void Spell::SpellEffectParry(uint32 i)
{
	if(unitTarget)
		unitTarget->setcanperry(true);
}

void Spell::SpellEffectBlock(uint32 i)
{
	//i think this actually enbles the skill to be able to block melee+ranged attacks
	//value is static and sets value directly which will be modified by other factors
//	if(unitTarget->IsPlayer())
//		unitTarget->SetFloatValue(PLAYER_BLOCK_PERCENTAGE,damage);
}

void Spell::SpellEffectSkillCreateItem(uint32 i)
{

	if(!p_caster)return;
	float chance = 0.0f;
	skilllinespell* skill = objmgr.GetSpellSkill(m_spellInfo->Id);
	if (!skill ||!p_caster->_HasSkillLine(skill->skilline))
	{
		return;
	}
	uint32 amt = p_caster->_GetSkillLineCurrent( skill->skilline, false );
	if (amt < skill->minrank)
	{
		return;
	}

	SpellEffectItemsChangeItem(i);
	DetermineSkillUp(skill->skilline);
}

void Spell::SpellEffectCreateItem(uint32 i) // Create item 
{
	if(!p_caster)
		return;

	Item* newItem;
	Item *add;
	uint8 slot;
	SlotResult slotresult;

	skilllinespell* skill = objmgr.GetSpellSkill(m_spellInfo->Id);

	for(int j=0; j<3; j++) // now create the Items
	{
		ItemPrototype *m_itemProto;
		m_itemProto = ItemPrototypeStorage.LookupEntry( m_spellInfo->EffectSpellGroupRelation[j] );
		if (!m_itemProto)
			 continue;

		if(m_spellInfo->EffectSpellGroupRelation[j] == 0)
			continue;

		uint32 item_count = 0;
		if (m_itemProto->Class != ITEM_CLASS_CONSUMABLE || m_spellInfo->SpellFamilyName != 3) //SpellFamilyName 3 is mage
			item_count = damage;
		else if(p_caster->getLevel() >= m_spellInfo->spellLevel)
			item_count = ((p_caster->getLevel() - (m_spellInfo->spellLevel-1))*damage);

		if(!item_count)
			item_count = damage;

		if (skill && skill->skilline == SKILL_ALCHEMY)
		{
			//Potion Master
			if (strstr(Item::BuildStringWithProto(m_itemProto), "Potion"))
			{
				if(p_caster->HasSpell(28675)) 
					while (Rand(20) && item_count<10) item_count++;
			}
			//Elixir Master
			if (strstr(Item::BuildStringWithProto(m_itemProto), "Elixir") || strstr(Item::BuildStringWithProto(m_itemProto), "Flask"))
			{
				if(p_caster->HasSpell(28677)) 
					while (Rand(20) && item_count<10) item_count++;
			}
			//Transmutation Master
			if (m_spellInfo->Category == 310)
			{
				if(p_caster->HasSpell(28672)) 
					while (Rand(20) && item_count<10) item_count++;
			}
		}

		// item count cannot be more than allowed in a single stack
		if (item_count > m_itemProto->MaxCount)
			item_count = m_itemProto->MaxCount;

		// item count cannot be more than item unique value
		if (m_itemProto->Unique && item_count > m_itemProto->Unique)
			item_count = m_itemProto->Unique;

		if(p_caster->GetItemInterface()->CanReceiveItem(m_itemProto, item_count)) //reversed since it sends >1 as invalid and 0 as valid
		{
			SendCastResult(SPELL_FAILED_TOO_MANY_OF_ITEM);
			return;
		}

		slot = 0;
		add = p_caster->GetItemInterface()->FindItemLessMax(m_spellInfo->EffectSpellGroupRelation[j],1, false);
		if (!add)
		{
			slotresult = p_caster->GetItemInterface()->FindFreeInventorySlot(m_itemProto);
			if(!slotresult.Result)
			{
				  SendCastResult(SPELL_FAILED_TOO_MANY_OF_ITEM);
				  return;
			}
			
			newItem =objmgr.CreateItem(m_spellInfo->EffectSpellGroupRelation[i],p_caster);
			newItem->SetUInt64Value(ITEM_FIELD_CREATOR,m_caster->GetGUID());
			newItem->SetUInt32Value(ITEM_FIELD_STACK_COUNT, item_count);

			if (m_itemProto->RandomPropId)
			{
				RandomProps * iRandomProperty = lootmgr.GetRandomProperties(m_itemProto);
				newItem->SetRandomProperty(iRandomProperty->ID);
				newItem->ApplyRandomProperties(false);
			}
			if (m_itemProto->RandomSuffixId)
			{
				ItemRandomSuffixEntry * iRandomSuffix = lootmgr.GetRandomSuffix(m_itemProto);
				newItem->SetRandomSuffix(iRandomSuffix->id);
				newItem->ApplyRandomProperties(false);
			}

			if(p_caster->GetItemInterface()->SafeAddItem(newItem,slotresult.ContainerSlot, slotresult.Slot))
			{
				p_caster->GetSession()->SendItemPushResult(newItem,true,false,true,true,slotresult.ContainerSlot,slotresult.Slot,item_count);
			} else {
				delete newItem;
			}

			if(skill)
				DetermineSkillUp(skill->skilline);
		} 
		else 
		{
			//scale item_count down if total stack will be more than 20
			if(add->GetUInt32Value(ITEM_FIELD_STACK_COUNT) + item_count > 20)
			{
				uint32 item_count_filled;
				item_count_filled = 20 - add->GetUInt32Value(ITEM_FIELD_STACK_COUNT);
				add->SetCount(20);
				add->m_isDirty = true;

				slotresult = p_caster->GetItemInterface()->FindFreeInventorySlot(m_itemProto);
				if(!slotresult.Result)
					item_count = item_count_filled;
				else
				{
					newItem =objmgr.CreateItem(m_spellInfo->EffectSpellGroupRelation[i],p_caster);
					newItem->SetUInt64Value(ITEM_FIELD_CREATOR,m_caster->GetGUID());
					newItem->SetUInt32Value(ITEM_FIELD_STACK_COUNT, item_count - item_count_filled);
					if(!p_caster->GetItemInterface()->SafeAddItem(newItem,slotresult.ContainerSlot, slotresult.Slot))
					{
						delete newItem;
						item_count = item_count_filled;
					}
					else
						p_caster->GetSession()->SendItemPushResult(newItem, true, false, true, true, slotresult.ContainerSlot, slotresult.Slot, item_count-item_count_filled);
                }
			}
			else
			{
				add->SetCount(add->GetUInt32Value(ITEM_FIELD_STACK_COUNT) + item_count);
				add->m_isDirty = true;
				sQuestMgr.OnPlayerItemPickup(p_caster,add);
				p_caster->GetSession()->SendItemPushResult(add, true,false,true,false,p_caster->GetItemInterface()->GetBagSlotByGuid(add->GetGUID()),0xFFFFFFFF,item_count);
			}

			if(skill)
				DetermineSkillUp(skill->skilline);
		}
	}	   
}

void Spell::SpellEffectWeapon(uint32 i)
{
	if( playerTarget == NULL )
		return;

	uint32 skill = 0;
	uint32 spell = 0;

	switch( this->m_spellInfo->Id )
	{
	default:
		{
			skill = 0;
			MyLog::log->debug("WARNING: Could not determine skill for spell id %d (SPELL_EFFECT_WEAPON)", this->m_spellInfo->Id);
		}break;
	}

	if(skill)
	{
		if(spell)
			playerTarget->addSpell(spell);
		
		// if we do not have the skill line
		if(!playerTarget->_HasSkillLine(skill))
		{
			playerTarget->_AddSkillLine(skill, 1, playerTarget->getLevel()*5);
		}
		else // unhandled.... if we have the skill line
		{
		}
	}
}

void Spell::SpellEffectDefense(uint32 i)
{
	//i think this actually enbles the skill to be able to use defense
	//value is static and sets value directly which will be modified by other factors
	//this is only basic value and will be overwiten elsewhere !!!
//	if(unitTarget->IsPlayer())
//		unitTarget->SetFloatValue(UNIT_FIELD_RESISTANCES,damage);
}

void Spell::SpellEffectPersistentAA(uint32 i) // Persistent Area Aura
{
	if(m_AreaAura == true || !m_caster->IsInWorld())
		return;
	//create only 1 dyn object
	uint32 dur = GetDuration();
	float r = GetRadius(i);

	//Note: this code seems to be useless
	//this must be only source point or dest point
	//this AREA aura it's apllied on area
	//it can'be on unit or self or item or object
	//uncomment it if i'm wrong
	//We are thinking in general so it might be useful later DK
	
	// grep: this is a hack!
	// our shitty dynobj system doesnt support GO casters, so we gotta
	// kinda have 2 summoners for traps that apply AA.
	DynamicObject* dynObj = m_caster->GetMapMgr()->CreateDynamicObject();
	 
	if(g_caster && g_caster->m_summoner && !unitTarget)
	{
		Unit * caster = g_caster->m_summoner;
		dynObj->Create(caster, this, g_caster->GetPositionX(), g_caster->GetPositionY(), 
			g_caster->GetPositionZ(), dur, r);
		m_AreaAura = true;
		return;
	}
		
	switch(m_targets.m_targetMask)
	{		
	case TARGET_FLAG_SELF:
		{
			dynObj->Create(u_caster, this,	m_caster->GetPositionX(), 
				m_caster->GetPositionY(), m_caster->GetPositionZ(), dur,r);		 
		}break;
	case TARGET_FLAG_UNIT:
		{
			if(!unitTarget||!unitTarget->isAlive())
				break;
			dynObj->Create( u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
				dur, r);
		}break;
	case TARGET_FLAG_OBJECT:
		{
			if(!unitTarget)
				break;
			if(!unitTarget->isAlive())
				break;
			dynObj->Create(u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
				dur, r);
		}break;
	case TARGET_FLAG_SOURCE_LOCATION:
		{
			dynObj->SetInstanceID(m_caster->GetInstanceID());
			dynObj->Create(u_caster, this, m_targets.m_srcX,
				m_targets.m_srcY, m_targets.m_srcZ, dur,r);
		}break;
	case TARGET_FLAG_DEST_LOCATION:
		{
			dynObj->SetInstanceID(m_caster->GetInstanceID());
			dynObj->Create(u_caster?u_caster:g_caster->m_summoner, this,
				m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ,dur,r);
		}break;
	default:
		delete dynObj;
		return;
	}   
	
	if(u_caster)
	if(m_spellInfo->ChannelInterruptFlags > 0)
	{
		u_caster->SetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT,dynObj->GetGUID());
		u_caster->SetUInt32Value(UNIT_CHANNEL_SPELL,m_spellInfo->Id);
	}
	m_AreaAura = true;
}

void Spell::SpellEffectSummon(uint32 i) // Summon
{
	if(!p_caster || !p_caster->IsInWorld())
		return;

	if(p_caster->m_tempSummon)
	{
		p_caster->m_tempSummon->RemoveFromWorld(false,true);
		if(p_caster->m_tempSummon)
			p_caster->m_tempSummon->SafeDelete();

		p_caster->m_tempSummon = 0;
		p_caster->SetUInt64Value(UNIT_FIELD_SUMMON, 0);
	}

	/* This is for summon water elemenal, etc */
	CreatureInfo * ci = CreatureNameStorage.LookupEntry(m_spellInfo->EffectMiscValue[i]);
	CreatureProto * cp = CreatureProtoStorage.LookupEntry(m_spellInfo->EffectMiscValue[i]);
	if( !ci || !cp )
		return;


	if(m_spellInfo->EffectMiscValue[i] == 510)	// Water Elemental
	{
		Pet *summon = objmgr.CreatePet();
		summon->SetInstanceID(m_caster->GetInstanceID());
		summon->CreateAsSummon(m_spellInfo->EffectMiscValue[i], ci, NULL, p_caster, m_spellInfo, 1, 45000);
		summon->SetUInt32Value(UNIT_FIELD_LEVEL, p_caster->getLevel());
		summon->AddSpell(dbcSpell.LookupEntry(31707), true);
		summon->AddSpell(dbcSpell.LookupEntry(33395), true);
       summon->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, p_caster->GetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE));
       summon->_setFaction();
	   p_caster->m_tempSummon = summon;
	}
	else
	{
	       Creature * pCreature = p_caster->GetMapMgr()->CreateCreature(cp->Id);
	       pCreature->Load(cp, p_caster->GetPositionX(), p_caster->GetPositionY(), p_caster->GetPositionZ(), p_caster->GetOrientation());
	       pCreature->_setFaction();
	       pCreature->GetAIInterface()->Init(pCreature,AITYPE_PET,MOVEMENTTYPE_NONE,u_caster);
	       pCreature->GetAIInterface()->SetUnitToFollow(u_caster);
	       pCreature->GetAIInterface()->SetUnitToFollowAngle(float(-(M_PI/2)));
	       pCreature->GetAIInterface()->SetFollowDistance(3.0f);
	       pCreature->SetUInt32Value(UNIT_FIELD_LEVEL, p_caster->getLevel());
	       pCreature->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, p_caster->GetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE));
	       pCreature->_setFaction();
	       p_caster->SetUInt64Value(UNIT_FIELD_SUMMON, pCreature->GetGUID());
	       p_caster->m_tempSummon = pCreature;
	       pCreature->PushToWorld(p_caster->GetMapMgr());

	       /*if(p_caster->isInCombat())
	       {
		       Unit * target = p_caster->GetMapMgr()->GetUnit(p_caster->getAttackTarget());
		       if(target)
			       pCreature->GetAIInterface()->AttackReaction(target, 1, 0);
	       }*/
	       
	       /* not sure on this */
	       sEventMgr.AddEvent(pCreature, &Creature::SafeDelete, EVENT_CREATURE_REMOVE_CORPSE, /*GetDuration()*/45000, 1, 0);
	}
}

void Spell::SpellEffectLeap(uint32 i) // Leap
{
	float radius = GetRadius(i);

	//FIXME: check for obstacles
	 /*
	float ori = m_caster->GetOrientation();				
	float posX = m_caster->GetPositionX()+(radius*(cos(ori)));
	float posY = m_caster->GetPositionY()+(radius*(sin(ori)));
	float z= m_caster->GetMapMgr()->GetLandHeight(posX,posY);

	if(fabs(p_caster->GetPositionZ() - z) > 2)
		z=p_caster->GetPositionZ()+2;

	m_caster->SetPosition(posX,posY,z,ori,true);

	WorldPacket data(MSG_S2C::MSG_MOVE_HEARTBEAT, 33);
	data << m_caster->GetNewGUID();
	data << uint32(0) << uint32(0);
	data << posX;
	data << posY;
	data << z;
	data << ori;
	m_caster->SendMessageToSet(&data, true); 
	*/
	if(!p_caster) return;

	if( p_caster->m_beforeTeleport2Instance )
	{
		p_caster->GetSession()->SendNotification( "传送之前不可以使用" );
		return;
	}

	// remove movement impeding auras
	p_caster->RemoveAurasByInterruptFlag(AURA_INTERRUPT_ON_ANY_DAMAGE_TAKEN);
	// just in case
	for(uint32 i = MAX_POSITIVE_AURAS; i < MAX_AURAS; ++i)
	{
		if( p_caster->m_auras[i] != NULL )
		{
			for(uint32 j = 0; j < 3; ++j)
			{
				if( p_caster->m_auras[i]->GetSpellProto()->EffectApplyAuraName[j] == SPELL_AURA_MOD_STUN || 
					p_caster->m_auras[i]->GetSpellProto()->EffectApplyAuraName[j] == SPELL_AURA_MOD_ROOT || 
					p_caster->m_auras[i]->GetSpellProto()->EffectApplyAuraName[j] == SPELL_AURA_MOD_SLEEP )
				{
					p_caster->m_auras[i]->Remove();
					break;
				}
			}
		}
	}
//#ifndef COLLISION
	p_caster->blinked = true;

// 	MSG_S2C::stMove_Knock_Back Msg;
// 	Msg.guid = p_caster->GetNewGUID();
// 	Msg.timeNow = getMSTime();
// 	Msg.x = cosf(p_caster->GetOrientation());
// 	Msg.y = sinf(p_caster->GetOrientation());
// 	Msg.z = radius;
// 	Msg.orientation = float(-10.0f);
// 	p_caster->GetSession()->SendPacket(Msg);
//	//m_caster->SendMessageToSet(&data, true);

//	float ori = -p_caster->GetOrientation()-M_H_PI+M_PI;				
//	float posX = p_caster->GetPositionX()+(radius*(cosf(ori)));
//	float posY = p_caster->GetPositionY()+(radius*(sinf(ori)));
//	LocationVector dest(posX, posY, p_caster->GetPositionZ(), ori);
//	LocationVector destest(posX, posY, dest.z, ori);
//	dest.o = p_caster->GetOrientation();
	
	if( p_caster->leap_x < 0.1f && p_caster->leap_y < 0.1f && p_caster->leap_z < 0.1f )
		return;

	LocationVector dest( p_caster->leap_x, p_caster->leap_y, p_caster->leap_z, p_caster->leap_o );
	if( dest.Distance2DSq( p_caster->GetPositionX(), p_caster->GetPositionY() ) >= 225 )
	{
		// hack
	}
	p_caster->leap_x = 0.f;
	p_caster->leap_y = 0.f;
	p_caster->leap_z = 0.f;
	p_caster->leap_o = 0.f;

	p_caster->SafeTeleport( p_caster->GetMapId(), p_caster->GetInstanceID(), dest );
	/*
#else
	if(!p_caster)
		return;

	float ori = m_caster->GetOrientation();				
	float posX = m_caster->GetPositionX()+(radius*(cosf(ori)));
	float posY = m_caster->GetPositionY()+(radius*(sinf(ori)));
	float z = CollideInterface.GetHeight(m_caster->GetMapMgr()->GetBaseMap(), posX, posY, m_caster->GetPositionZ() + 2.0f);
	if(z == NO_WMO_HEIGHT)		// not found height, or on adt
		z = m_caster->GetMapMgr()->GetLandHeight(posX,posY);

	if( fabs( z - m_caster->GetPositionZ() ) >= 10.0f )
		return;

	LocationVector dest(posX, posY, z + 2.0f, ori);
	LocationVector destest(posX, posY, dest.z, ori);
	LocationVector src(m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ() + 2.0f);

	if(CollideInterface.GetFirstPoint(m_caster->GetMapIDForCollision(), src, destest, dest, -1.5f))
	{
		// hit an object new point is in dest.
		// is this necessary?
		//dest.z = CollideInterface.GetHeight(m_caster->GetMapIDForCollision(), dest.x, dest.y, dest.z + 2.0f);
	}
	else
		dest.z = z;

	dest.o = p_caster->GetOrientation();
	p_caster->blinked = true;
	p_caster->SafeTeleport( p_caster->GetMapId(), p_caster->GetInstanceID(), dest );
#endif
	*/

	// reset heartbeat for a little while, 2 seconds maybe?
	p_caster->DelaySpeedHack( 5000 );
	++p_caster->_heartbeatDisable;
	p_caster->z_axisposition = 0.0f;
}

void Spell::SpellAuraExtraXPRatio(uint32 i)
{
}

void Spell::SpellEffectHealPct(uint32 i)
{
	if(!unitTarget || !unitTarget->isAlive())
		return;

	uint32 curHealth = (uint32)p_caster->GetUInt32Value(UNIT_FIELD_HEALTH);
	uint32 maxHealth = (uint32)p_caster->GetUInt32Value(UNIT_FIELD_MAXHEALTH);
	uint32 modHealth;
	//yess there is always someone special : shamanistic rage - talent
	modHealth = maxHealth * damage / 100;

	SendHealSpellOnPlayer( p_caster, p_caster, modHealth, false );

	if(modHealth + curHealth > maxHealth)
		p_caster->SetUInt32Value(UNIT_FIELD_HEALTH,maxHealth);
	else
		p_caster->SetUInt32Value(UNIT_FIELD_HEALTH,modHealth + curHealth);
}

void Spell::SpellEffectCloseAllAreaAura(uint32 i)
{
	if (p_caster)
	{
		p_caster->RemoveAllAreaAuras();
	}
}

void Spell::SpellEffectCreateChangeItem(uint32 i)
{

	if (p_caster)
	{
		lootmgr.Gamble( p_caster, m_spellInfo->Id );
	}
}

void Spell::SpellEffectForItemsChangeItem(uint32 i)
{
	if( p_caster )
	{
		//p_caster->GetItemInterface()->RemoveItemAmt( m_spellInfo->EffectMiscValue[i], m_spellInfo->EffectBasePoints[i] );
		sEventMgr.AddEvent( p_caster, &Player::RemoveItemAmt, m_spellInfo->EffectMiscValue[i], (uint32)m_spellInfo->EffectBasePoints[i], EVENT_PLAYER_REMOVE_ITEM_AMT, 1, 1, 0 );
	}
}

void Spell::SpellEffectItemsChangeItem(uint32 i)
{
	if (p_caster)
	{
		//p_caster->GetItemInterface()->RemoveItemAmt( m_spellInfo->EffectMiscValue[i], m_spellInfo->EffectBasePoints[i] );
		sEventMgr.AddEvent( p_caster, &Player::RemoveItemAmt, m_spellInfo->EffectMiscValue[i], (uint32)m_spellInfo->EffectBasePoints[i], EVENT_PLAYER_REMOVE_ITEM_AMT, 1, 1, 0 );
		lootmgr.Gamble( p_caster, m_spellInfo->Id );
	}
}

void Spell::SpellDismount(uint32 i)
{
	if( playerTarget )
		playerTarget->DismissMount();
}

void Spell::SpellEffectRefineItem( uint32 i )
{
	if( m_caster && m_caster->IsPlayer() )
	{
		((Player*)m_caster)->RefineItem( true );
	}
}

void Spell::SpellEffectEnergizePct(uint32 i)
{
	if(!unitTarget || !unitTarget->isAlive())
		return;

	uint32 curEnergy = (uint32)p_caster->GetUInt32Value(UNIT_FIELD_POWER1);
	uint32 maxEnergy = (uint32)p_caster->GetUInt32Value(UNIT_FIELD_MAXPOWER1);
	uint32 modEnergy;
	//yess there is always someone special : shamanistic rage - talent
	modEnergy = maxEnergy * damage / 100;

	SendHealManaSpellOnPlayer(p_caster, p_caster, modEnergy, UNIT_FIELD_POWER1);

	if(modEnergy + curEnergy > maxEnergy)
		p_caster->SetUInt32Value(UNIT_FIELD_POWER1,maxEnergy);
	else
		p_caster->SetUInt32Value(UNIT_FIELD_POWER1,modEnergy + curEnergy);
}

void Spell::SpellEffectEnergize(uint32 i) // Energize
{
	if(!unitTarget || !unitTarget->isAlive())
		return;

	uint32 POWER_TYPE=UNIT_FIELD_POWER1+m_spellInfo->EffectMiscValue[i];

	uint32 curEnergy = (uint32)unitTarget->GetUInt32Value(UNIT_FIELD_POWER1);
	uint32 maxEnergy = (uint32)unitTarget->GetUInt32Value(UNIT_FIELD_MAXPOWER1);
	uint32 modEnergy;
	//yess there is always someone special : shamanistic rage - talent
    modEnergy = damage;

	//if(unitTarget->HasAura(17619)) 
	//{ 
	//	modEnergy = uint32(modEnergy*1.4f);      
	//} 
	SendHealManaSpellOnPlayer(u_caster, unitTarget, modEnergy, m_spellInfo->EffectMiscValue[i]);

	if(modEnergy + curEnergy > maxEnergy)
		unitTarget->SetUInt32Value(POWER_TYPE,maxEnergy);
	else
		unitTarget->SetUInt32Value(POWER_TYPE,modEnergy + curEnergy);
}

void Spell::SpellEffectWeaponDmgPerc(uint32 i) // Weapon Percent damage
{
	if(!unitTarget  || !u_caster)
		return;

	uint32 DmgPrc = damage;

	float fdmg = (float)CalculateDamage( u_caster, unitTarget,  GetType(), 0, m_spellInfo, false, 0, false, true, true );
	float dmg = CalculateDamage(u_caster, unitTarget, GetType(), 0, m_spellInfo, true, 0, true, true, true);
	uint32 realdmg = float2int32(fdmg*(float(dmg/100.0f)));
	int addDamage = realdmg - dmg;

	float dist = 0.0f;
	float time = 0.0f;
	if (m_spellInfo->speed > 0.0f)
	{
		dist = m_caster->CalcDistance( unitTarget );
		time = ((dist*1000.0f)/m_spellInfo->speed);
	}

	if (m_spellInfo->Spell_Dmg_Type != SPELL_DMG_TYPE_MAGIC)
	{

		if(m_spellInfo->speed > 0.01f)
		{
			sEventMgr.AddEvent(u_caster,&Unit::EventStrikeWithAbility,unitTarget->GetGUID(),
				(SpellEntry*)m_spellInfo, (int32)damage, (int32)0, (uint32)0, false, false, false, true, false,  EVENT_SPELL_DAMAGE_HIT, float2int32(time), 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
		}
		else
		{
			damage = u_caster->Strike( unitTarget, ( GetType() == SPELL_DMG_TYPE_RANGED ? RANGED : MELEE ), m_spellInfo, damage, 0, 0, false, false, true, true , true);
		}
	}
}

void Spell::SpellEffectTriggerMissile(uint32 i) // Trigger Missile
{
	//Used by mortar team
	//Triggers area affect spell at destinatiom
	if(!m_caster)
		return;

	uint32 spellid = m_spellInfo->EffectTriggerSpell[i];
	if(spellid == 0)
		return;

	SpellEntry *spInfo = dbcSpell.LookupEntry(spellid);
	if(!spInfo)
		return;

	float spellRadius = GetRadius(i);

	for(std::set<Object*>::iterator itr = m_caster->GetInRangeSetBegin(); itr != m_caster->GetInRangeSetEnd(); itr++ )
	{
		if(!((*itr)->IsUnit()) || !((Unit*)(*itr))->isAlive())
			continue;
		Unit *t=(Unit*)(*itr);
	
		float r;
		float d=m_targets.m_destX-t->GetPositionX();
		r=d*d;
		d=m_targets.m_destY-t->GetPositionY();
		r+=d*d;
		d=m_targets.m_destZ-t->GetPositionZ();
		r+=d*d;
		if(sqrt(r)> spellRadius)
			continue;
		
		if(!isAttackable(m_caster, (Unit*)(*itr)))//Fixme only enemy targets?
			continue;

		Spell*sp=new Spell(m_caster,spInfo,true,NULL);
		SpellCastTargets tgt;
		tgt.m_unitTarget=(*itr)->GetGUID();
		sp->prepare(&tgt);
	}
}

void Spell::SpellEffectOpenLock(uint32 i) // Open Lock
{
	if(!p_caster)
		return;
		
	uint8 loottype = 0;

	uint32 locktype=m_spellInfo->EffectMiscValue[i];
	switch(locktype)
	{
		case LOCKTYPE_PICKLOCK:
		{
			uint32 v = 0;
			uint32 lockskill = p_caster->_GetSkillLineCurrent(SKILL_LOCKPICKING);

			if(itemTarget)
			{	
				if(!itemTarget->locked)
				return;
						
				Lock *lock = dbcLock.LookupEntry( itemTarget->GetProto()->LockId );
				if(!lock) return;
				for(int i=0;i<5;i++)
					if(lock->locktype[i] == 2 && lock->minlockskill[i] && lockskill >= lock->minlockskill[i])
					{
						v = lock->minlockskill[i];
						itemTarget->locked = false;
						itemTarget->SetFlag(ITEM_FIELD_FLAGS,4); // unlock
						DetermineSkillUp(SKILL_LOCKPICKING,v/5);
						break;
					}
			}
			else if(gameObjTarget)
			{
				GameObjectInfo *info = GameObjectNameStorage.LookupEntry(gameObjTarget->GetEntry());
				if(!info || gameObjTarget->GetUInt32Value(GAMEOBJECT_STATE) == 0) return;
				Lock *lock = dbcLock.LookupEntry( info->SpellFocus );
				if(lock == 0)
					return;

				for(int i=0;i<5;i++)
				{
					if(lock->locktype[i] == 2 && lock->minlockskill[i] && lockskill >= lock->minlockskill[i])
					{
						v = lock->minlockskill[i];
						gameObjTarget->SetUInt32Value(GAMEOBJECT_FLAGS, 0);
						gameObjTarget->SetUInt32Value(GAMEOBJECT_STATE, 1);
						//Add Fill GO loot here
						if(gameObjTarget->loot.items.size() == 0)
						{
							lootmgr.FillGOLoot(&gameObjTarget->loot,gameObjTarget->GetEntry(), gameObjTarget->GetMapMgr() ? (gameObjTarget->GetMapMgr()->iInstanceMode ? true : false) : false);
							DetermineSkillUp(SKILL_LOCKPICKING,v/5); //to prevent free skill up
						}
						loottype = LOOT_CORPSE;
						//End of it
						break;
					}
				}
			}
		}
		case LOCKTYPE_HERBALISM:
		{
			if(!gameObjTarget) return;	  
			
			uint32 v = gameObjTarget->GetGOReqSkill();
			bool bAlreadyUsed = false;
		 
			if(Rand(100.0f)) // 3% chance to fail//why?
			{
				if( static_cast< Player* >( m_caster )->_GetSkillLineCurrent( SKILL_HERBALISM ) < v )
				{
					//SendCastResult(SPELL_FAILED_LOW_CASTLEVEL);
					return;
				}
				else
				{
					if( gameObjTarget->loot.items.size() == 0 )
					{
						lootmgr.FillGOLoot(&gameObjTarget->loot,gameObjTarget->GetEntry(), gameObjTarget->GetMapMgr() ? (gameObjTarget->GetMapMgr()->iInstanceMode ? true : false) : false);
					}
					else
						bAlreadyUsed = true;
				}
				loottype = LOOT_SKINNING;
			}
			else
			{
				/*
				if(rand()%100 <= 30)
				{
					//30% chance to not be able to reskin on fail
					((Creature*)unitTarget)->Skinned = true;
					WorldPacket *pkt=unitTarget->BuildFieldUpdatePacket(UNIT_FIELD_FLAGS,0);
					static_cast< Player* >( m_caster )->GetSession()->SendPacket(pkt);
					delete pkt;

				}*/
				SendCastResult(SPELL_FAILED_TRY_AGAIN);
			}
			//Skill up
			if(!bAlreadyUsed) //Avoid cheats with opening/closing without taking the loot
				DetermineSkillUp(SKILL_HERBALISM,v/5); 
		}
		break;
		case LOCKTYPE_MINING:
		{
			if(!gameObjTarget) return;

			uint32 v = gameObjTarget->GetGOReqSkill();
			bool bAlreadyUsed = false;

			if( Rand( 100.0f ) ) // 3% chance to fail//why?
			{
				if( static_cast< Player* >( m_caster )->_GetSkillLineCurrent( SKILL_MINING ) < v )
				{
					SendCastResult(SPELL_FAILED_LOW_CASTLEVEL);
					return;
				}
				else if( gameObjTarget->loot.items.size() == 0 )
				{
					lootmgr.FillGOLoot(&gameObjTarget->loot,gameObjTarget->GetEntry(), gameObjTarget->GetMapMgr() ? (gameObjTarget->GetMapMgr()->iInstanceMode ? true : false) : false);
				}	
				else
					bAlreadyUsed = true;

				loottype = LOOT_SKINNING;
			}
			else
			{
				SendCastResult(SPELL_FAILED_TRY_AGAIN);
			}
			//Skill up
			if(!bAlreadyUsed) //Avoid cheats with opening/closing without taking the loot
				DetermineSkillUp(SKILL_MINING,v/5);
		}
		break;
		case LOCKTYPE_SLOW_OPEN: // used for Raid go's
		{
			if(!gameObjTarget ) return;
			
			uint32 spellid = 0;//!gameObjTarget->GetInfo()->Unknown1 ? 23932 : gameObjTarget->GetInfo()->Unknown1;
			SpellEntry*en=dbcSpell.LookupEntry(spellid);
			Spell *sp=new Spell(p_caster,en,true,NULL);
			SpellCastTargets tgt;
			tgt.m_unitTarget=gameObjTarget->GetGUID();
			sp->prepare(&tgt);
			return;
		}	
		break;
		case LOCKTYPE_QUICK_CLOSE:
			{
				if(!gameObjTarget ) return;
				gameObjTarget->EventCloseDoor();
			}
		break;
		default://not profession
		{
			if(!gameObjTarget ) return;

//			if( gameObjTarget->GetUInt32Value( GAMEOBJECT_TYPE_ID ) == GAMEOBJECT_TYPE_GOOBER)
//					CALL_GO_SCRIPT_EVENT(gameObjTarget, OnActivate)(static_cast<Player*>(p_caster));

			if(gameObjTarget->HasQuests() && sQuestMgr.OnActivateQuestGiver(gameObjTarget, p_caster))
				return;

			if(sQuestMgr.OnGameObjectActivate(p_caster, gameObjTarget))
			{
				p_caster->UpdateNearbyGameObjects();
				//return;
			}

			uint32 type = gameObjTarget->GetUInt32Value( GAMEOBJECT_TYPE_ID );
			if( type == GAMEOBJECT_TYPE_PICK )
			{
				if ( !gameObjTarget->GetInfo()->reqspell&& !gameObjTarget->GetInfo()->SpellFocus)
				{
					if(gameObjTarget->loot.items.size() == 0 && gameObjTarget->loot.gold == 0)
					{
						lootmgr.FillGOLoot(&gameObjTarget->loot,gameObjTarget->GetEntry(), gameObjTarget->GetMapMgr() ? (gameObjTarget->GetMapMgr()->iInstanceMode ? true : false) : false);
						gameObjTarget->loot.gold = gameObjTarget->gold;
					}
					if(gameObjTarget->loot.items.size() == 0 && gameObjTarget->loot.gold == 0)
					{
						//gameObjTarget->Despawn((sQuestMgr.GetGameObjectLootQuest(gameObjTarget->GetEntry()) ? 60000 : 12000 + ( RandomUInt( 60000 ) ) ) );
						gameObjTarget->Despawn( gameObjTarget->GetInfo()->spawntime );
					}
					loottype= LOOT_CORPSE ;
					gameObjTarget->m_release_loot = false;
				}

			}
// 			else if( type == GAMEOBJECT_TYPE_FLAGSTAND )
// 			{
// 				if( gameObjTarget->GetMapMgr()->m_sunyouinstance )
// 					gameObjTarget->GetMapMgr()->m_sunyouinstance->OnGameObjectTrigger( gameObjTarget, p_caster );
// 			}

			SunyouRaid* raid = gameObjTarget->GetSunyouRaid();
			if( raid )
				raid->OnGameObjectTrigger( gameObjTarget, p_caster );
		}
		break;
	};
	if( gameObjTarget != NULL && gameObjTarget->GetUInt32Value( GAMEOBJECT_TYPE_ID ) == GAMEOBJECT_TYPE_PICK )
		static_cast< Player* >( m_caster )->SendLoot( gameObjTarget->GetGUID(), loottype );
}

void Spell::SpellEffectOpenLockItem(uint32 i)
{
	Unit* caster = u_caster;
	if(!caster && i_caster)
		caster = i_caster->GetOwner();

	if(!gameObjTarget || !gameObjTarget->IsInWorld()) 
		return;

	if( !u_caster->IsPlayer() )
		return;

	if( !p_caster )
		return;

	if( gameObjTarget->GetUInt32Value( GAMEOBJECT_TYPE_ID ) == GAMEOBJECT_TYPE_MAILBOX )
	{
		MSG_S2C::stMail_List_Result MsgMailList;
		p_caster->m_mailBox->BuildMailboxListingPacket(&MsgMailList);
		MsgMailList.npc_guid = gameObjTarget->GetGUID();
		p_caster->GetSession()->SendPacket(MsgMailList);
		return;
	}
	
	if( caster && caster->IsPlayer() && sQuestMgr.OnGameObjectActivate( (static_cast<Player*>(caster)), gameObjTarget ) )
		static_cast<Player*>(caster)->UpdateNearbyGameObjects();

	CALL_GO_SCRIPT_EVENT(gameObjTarget, OnActivate)(static_cast<Player*>(caster));
	//gameObjTarget->SetUInt32Value(GAMEOBJECT_STATE, 0);	

// 	if( gameObjTarget->GetEntry() == 183146)
// 	{
// 		gameObjTarget->Despawn(1);
// 		return;
// 	}

	if( gameObjTarget->GetUInt32Value( GAMEOBJECT_TYPE_ID ) == GAMEOBJECT_TYPE_CHEST)
	{
		if( gameObjTarget->GetUInt32Value( GAMEOBJECT_STATE ) == 1 )
		{
			lootmgr.FillGOLoot(&gameObjTarget->loot,gameObjTarget->GetEntry(), gameObjTarget->GetMapMgr() ? (gameObjTarget->GetMapMgr()->iInstanceMode ? true : false) : false);
			gameObjTarget->loot.gold = gameObjTarget->gold;
			gameObjTarget->SetUInt32Value( GAMEOBJECT_STATE, 0 );

			SunyouRaid* raid = gameObjTarget->GetSunyouRaid();
			if( raid )
				raid->OnGameObjectTrigger( gameObjTarget, (Player*)m_caster );
		}
		
		if(gameObjTarget->loot.items.size() > 0 || gameObjTarget->loot.gold > 0 )
		{
			((Player*)caster)->SendLoot(gameObjTarget->GetGUID(),LOOT_CORPSE);
		}
	}

	if( gameObjTarget->GetUInt32Value( GAMEOBJECT_TYPE_ID ) == GAMEOBJECT_TYPE_DOOR)
		gameObjTarget->SetUInt32Value(GAMEOBJECT_FLAGS, 33);

	if(gameObjTarget->GetMapMgr()->GetMapInfo()->type==INSTANCE_NULL)//dont close doors for instances
		sEventMgr.AddEvent(gameObjTarget,&GameObject::EventCloseDoor, EVENT_GAMEOBJECT_DOOR_CLOSE,10000,1,0);
	
	//sEventMgr.AddEvent(gameObjTarget, &GameObject::Despawn, (uint32)1, EVENT_GAMEOBJECT_ITEM_SPAWN, 6*60*1000, 1, 0);
}

void Spell::SpellEffectProficiency(uint32 i)
{
	uint32 skill = 0;
	skilllinespell* skillability = objmgr.GetSpellSkill(m_spellInfo->Id);
	if (skillability)
		skill = skillability->skilline;
	skilllineentry* sk = dbcSkillLine.LookupEntry(skill);
	if(skill)
	{
		if(playerTarget)
		{
			if(playerTarget->_HasSkillLine(skill))
			{
				// Increase it by one
			   // playerTarget->AdvanceSkillLine(skill);
			}
			else
			{
				// Don't add skills to players logging in.
				/*if((m_spellInfo->Attributes & 64) && playerTarget->m_TeleportState == 1)
					return;*/

				if(sk && sk->type == SKILL_TYPE_WEAPON)
					playerTarget->_AddSkillLine(skill, 1, 5*playerTarget->getLevel());
				else
					playerTarget->_AddSkillLine(skill, 1, 1);				
			}
		}
	}
}

void Spell::SpellEffectSendEvent(uint32 i) //Send Event
{
	//This is mostly used to trigger events on quests or some places

	uint32 spellId = m_spellInfo->Id;

	// Try a dummy SpellHandler
	if(sScriptMgr.CallScriptedDummySpell(spellId, i, this))
		return;
}

void Spell::SpellEffectApplyAADamage(uint32 i)
{
	if (!unitTarget || !unitTarget->isAlive())
		return;

	if (u_caster != unitTarget)
		return;

	Aura* pAura = new Aura(m_spellInfo, GetDuration(), m_caster, unitTarget);

}

void Spell::SpellEffectApplyAA(uint32 i) // Apply Area Aura
{
	if(!unitTarget || !unitTarget->isAlive())
		return;
	if(u_caster!=unitTarget)
		return;

	unitTarget->RemoveAllAreaAuras();
	
	Aura*pAura;
	std::map<uint32,Aura*>::iterator itr=unitTarget->tmpAura.find(m_spellInfo->Id);
	if(itr==unitTarget->tmpAura.end())
	{
		pAura=new Aura(m_spellInfo,GetDuration(),m_caster,unitTarget);
		
		unitTarget->tmpAura [m_spellInfo->Id]= pAura;
	
		float r=GetRadius(i);
		if(!sEventMgr.HasEvent(pAura, EVENT_AREAAURA_UPDATE))		/* only add it once */
			sEventMgr.AddEvent(pAura, &Aura::EventUpdateAA,r*r, EVENT_AREAAURA_UPDATE, 1000, 0,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);	

	}else 
	{
		pAura=itr->second;	
	}
	
	uint32 miscvalue = m_spellInfo->EffectMiscValue[i];
	if( m_caster && m_caster->IsPlayer() )
	{
		float mindamage = ((Player*)m_caster)->GetFloatValue( UNIT_FIELD_MINDAMAGE );
		float maxdamage = ((Player*)m_caster)->GetFloatValue( UNIT_FIELD_MAXDAMAGE );
		int delta = (int)(maxdamage - mindamage);
		if( delta >= 1 )
			mindamage += rand() % delta;
		 miscvalue += (int32)mindamage;
	}
	pAura->AddMod( m_spellInfo->EffectApplyAuraName[i], damage, miscvalue, i );
}

void Spell::SpellEffectLearnSpell(uint32 i) // Learn Spell
{
	if(playerTarget == 0 && unitTarget && unitTarget->IsPet())
	{
		// bug in target map fill?
		//playerTarget = m_caster->GetMapMgr()->GetPlayer((uint32)m_targets.m_unitTarget);
		SpellEffectLearnPetSpell(i);
		return;
	}

	if( m_spellInfo->Id == 483 )		// "Learning"
	{
		if( !i_caster || !p_caster )
			return;

		uint32 spellid = 0;
		for(int i = 0; i < 5; ++i)
		{
			if( i_caster->GetProto()->Spells[i].Trigger == LEARNING && i_caster->GetProto()->Spells[i].Id != 0 )
			{
				spellid = i_caster->GetProto()->Spells[i].Id;
				break;
			}
		}

		if( !spellid || !dbcSpell.LookupEntry(spellid) )
			return;

		// 已经学会这个技能
		if(p_caster->HasSpell(spellid))
		{
			return;
		}
		// learn me!
		p_caster->addSpell( spellid );

		// no normal handler
		return;
	}
	else 
	{
		uint32 spellid = 0;
		spellid = m_spellInfo->EffectMiscValue[i];
		if( !spellid || !dbcSpell.LookupEntry(spellid) )
			return;

		// 已经学会这个技能
		if(p_caster->HasSpell(spellid))
		{
			return;
		}
		// learn me!
		p_caster->addSpell( spellid );
	}

	// if we got here... try via pet spells..
	SpellEffectLearnPetSpell(i);
}

void Spell::SpellEffectSpellDefense(uint32 i)
{
	//used to enable this ability. We use it all the time ...
}

void Spell::SpellEffectLearnPetSpell(uint32 i)
{
	/*if(unitTarget && m_caster->GetTypeId() == TYPEID_PLAYER)
	{
		if(unitTarget->IsPet() && unitTarget->GetTypeId() == TYPEID_UNIT)
		{
			static_cast< Player* >(m_caster)->AddPetSpell(m_spellInfo->EffectTriggerSpell[i], unitTarget->GetEntry());
		}
	}*/

	if(unitTarget && unitTarget->IsPet() && p_caster)
	{
		Pet * pPet = static_cast<Pet*>( unitTarget );
		if(pPet->IsSummon())
			p_caster->AddSummonSpell(unitTarget->GetEntry(), m_spellInfo->EffectTriggerSpell[i]);
		
		pPet->AddSpell( dbcSpell.LookupEntry( m_spellInfo->EffectTriggerSpell[i] ), true );

		// Send Packet
		MSG_S2C::stAura_Set_Single Msg;
		Msg.target_guid = pPet->GetGUID();
		Msg.slot		= 0;
		Msg.spellid		= m_spellInfo->EffectTriggerSpell[i];
		Msg.duration1	= -1;
		Msg.duration2	= 0;
		Msg.count       = 1;
		p_caster->GetSession()->SendPacket(Msg);
	}
}

void Spell::SpellEffectPosDispel(uint32 i)
{
	if(!u_caster || !unitTarget)
		return;

	if(!isAttackable(u_caster,unitTarget))
	{
		return;
	}
	
	Aura *aur;
	uint32 start = 0,end = MAX_POSITIVE_AURAS;
	int nDispCount = damage;

	MSG_S2C::stSpell_Dispel_Log Msg;

	for(uint32 x=start;x<end;x++)
	{
		if (nDispCount <=0)
		{
			break;
		}
		if(unitTarget->m_auras[x])
		{
			aur = unitTarget->m_auras[x];
			//Nothing can dispel resurrection sickness;
			if( aur->IsRemainWhenDead() )
				continue;

			if( aur->GetSpellId() == 10801 || aur->GetSpellId() == 10802 || aur->GetSpellId() == 10803 )
				continue;

			if(!(aur->GetSpellProto()->Attributes & ATTRIBUTES_IGNORE_INVULNERABILITY))
			{
				Unit* u = aur->GetUnitCaster();
				if( u )
				{
					int resistchance = 0;
					SM_FIValue(u->SM_FRezist_dispell, &resistchance, aur->GetSpellProto()->SpellGroupType);
					SM_PIValue(u->SM_PRezist_dispell, &resistchance, aur->GetSpellProto()->SpellGroupType);
					if( Rand( resistchance ) )
						continue;
				}
				unitTarget->HandleProc( PROC_ON_PRE_DISPELL_AURA_VICTIM , u_caster , m_spellInfo, aur->GetSpellId() );
				
				Msg.caster_guid = m_caster->GetNewGUID();
				Msg.unit_guid = unitTarget->GetNewGUID();
				Msg.dispel_type = (uint32)1;//probably dispel type
				Msg.spell_id = aur->GetSpellId();
				m_caster->SendMessageToSet(Msg,true);
				unitTarget->RemoveAura(aur);
				
				if (nDispCount >0)
				{
					nDispCount --;
				}
			}
		}
	}
}

void Spell::SpellEffectDispelAuraById(uint32 i)
{
	if(!u_caster || !unitTarget)
		return;
	if(isAttackable(u_caster,unitTarget))
	{
		return;
	}
	int SpellId = m_spellInfo->EffectMiscValue[i];
	unitTarget->RemoveAura(SpellId);

}

void Spell::SpellEffectNegDispel(uint32 i)
{
	if(!u_caster || !unitTarget)
		return;
	if(isAttackable(u_caster,unitTarget))
	{
		return;
	}

	Aura *aur;
	uint32 start,end;

	start=MAX_POSITIVE_AURAS;
	end=MAX_AURAS;
	int nDispCount = damage;
	MSG_S2C::stSpell_Dispel_Log Msg;

	for(uint32 x=start;x<end;x++)
	{
		if (nDispCount <= 0)
		{
			break;
		}
		if(unitTarget->m_auras[x])
		{
			aur = unitTarget->m_auras[x];
			//Nothing can dispel resurrection sickness;
			if( aur->IsRemainWhenDead() )
				continue;

			if( aur->GetSpellId() == 10801 || aur->GetSpellId() == 10802 || aur->GetSpellId() == 10803 )
				continue;

			if(!(aur->GetSpellProto()->Attributes & ATTRIBUTES_IGNORE_INVULNERABILITY))
			{

				Unit* u = aur->GetUnitCaster();
				if( u )
				{
					int resistchance = 0;
					SM_FIValue(u->SM_FRezist_dispell, &resistchance, aur->GetSpellProto()->SpellGroupType);
					SM_PIValue(u->SM_PRezist_dispell, &resistchance, aur->GetSpellProto()->SpellGroupType);
					if( Rand( resistchance ) )
						continue;
				}
				unitTarget->HandleProc( PROC_ON_PRE_DISPELL_AURA_VICTIM , u_caster , m_spellInfo, aur->GetSpellId() );
				
				Msg.caster_guid = m_caster->GetNewGUID();
				Msg.unit_guid = unitTarget->GetNewGUID();
				Msg.dispel_type = (uint32)1;//probably dispel type
				Msg.spell_id = aur->GetSpellId();
				m_caster->SendMessageToSet(Msg,true);
				unitTarget->RemoveAura(aur);

				if (nDispCount >0)
				{
					nDispCount --;
				}
			}
		}
	}
}

void Spell::SpellEffectDispel(uint32 i) // Dispel
{
	if(!u_caster || !unitTarget)
		return;

	Aura *aur;
	uint32 start,end;
	if(isAttackable(u_caster,unitTarget))
	{
		start=0;
		end=MAX_POSITIVE_AURAS;
	}
	else
	{
		start=MAX_POSITIVE_AURAS;
		end=MAX_AURAS;
	}
	

	int nDispCount = damage;

	MSG_S2C::stSpell_Dispel_Log Msg;

	for(uint32 x=start;x<end;x++)
	{

		if (nDispCount <= 0)
		{
			break;
		}
		if(unitTarget->m_auras[x])
		{
			aur = unitTarget->m_auras[x];
			//Nothing can dispel resurrection sickness;
			if( aur->IsRemainWhenDead() )
				continue;

			if( aur->GetSpellId() == 10801 || aur->GetSpellId() == 10802 || aur->GetSpellId() == 10803 )
				continue;

			//if( aur->GetSpellProto()->StartRecoveryTime == 0 )
			//	continue;

			if(!aur->IsPassive() && !(aur->GetSpellProto()->Attributes & ATTRIBUTES_IGNORE_INVULNERABILITY))
			{
				
				if(m_spellInfo->DispelType == DISPEL_ALL)
				{
				
					unitTarget->HandleProc( PROC_ON_PRE_DISPELL_AURA_VICTIM , u_caster , m_spellInfo, aur->GetSpellId() );
					
					Msg.caster_guid = m_caster->GetNewGUID();
					Msg.unit_guid = unitTarget->GetNewGUID();
					Msg.dispel_type = (uint32)1;//probably dispel type
					Msg.spell_id = aur->GetSpellId();
					m_caster->SendMessageToSet(Msg,true);
					unitTarget->RemoveAura(aur);

					if (nDispCount >0)
					{
						nDispCount --;
					}

				}
				else if(aur->GetSpellProto()->DispelType == m_spellInfo->EffectMiscValue[i] || m_spellInfo->EffectMiscValue[i] == DISPEL_ZGTRINKETS)
				{
					Msg.caster_guid = m_caster->GetNewGUID();
					Msg.unit_guid = unitTarget->GetNewGUID();
					Msg.dispel_type = (uint32)1;
					Msg.spell_id = aur->GetSpellId();
					m_caster->SendMessageToSet(Msg,true);
					unitTarget->RemoveAllAuras(aur->GetSpellProto()->Id,aur->GetCasterGUID());

					if (nDispCount >0)
					{
						nDispCount --;
					}
					
				}
					
			}
		}
	}
}

void Spell::SpellEffectDualWield(uint32 i)
{
	if(m_caster->GetTypeId() != TYPEID_PLAYER) 
		return;

	Player *pPlayer = static_cast< Player* >( m_caster );

	if( !pPlayer->_HasSkillLine( SKILL_DUAL_WIELD ) )
		 pPlayer->_AddSkillLine( SKILL_DUAL_WIELD, 1, 1 );
	
		// Increase it by one
		//dual wield is 1/1 , it never increases it's not even displayed in skills tab

	//note: probably here must be not caster but unitVictim
}

void Spell::SpellEffectSummonWild(uint32 i)  // Summon Wild
{
	//these are some cretures that have your faction and do not respawn
	//number of creatures is actualy dmg (the usual formula), sometimes =3 sometimes =1
	if( !u_caster )
		return;

	uint32 cr_entry=m_spellInfo->EffectMiscValue[i];
	CreatureProto * proto = CreatureProtoStorage.LookupEntry(cr_entry);
	CreatureInfo * info = CreatureNameStorage.LookupEntry(cr_entry);
	if(!proto || !info)
	{
		MyLog::log->notice("Warning : Missing summon creature template %u used by spell %u!",cr_entry,m_spellInfo->Id);
		return;
	}
	for(int i=0;i<damage;i++)
	{
		float m_fallowAngle=-(float(M_PI)/2*i);
		float x = u_caster->GetPositionX()+(3*(cosf(m_fallowAngle+u_caster->GetOrientation())));
		float y = u_caster->GetPositionY()+(3*(sinf(m_fallowAngle+u_caster->GetOrientation())));
		float z = u_caster->GetPositionZ();
		z = CollideInterface.GetHeight( u_caster->GetMapMgr()->GetBaseMap(), x, y, z + 2.f );
		Creature * p = u_caster->GetMapMgr()->CreateCreature(cr_entry);
		//ASSERT(p);
		p->Load( proto, x, y, z, u_caster->GetOrientation() );
		p->SetZoneId( m_caster->GetZoneId() );

		//p->SetUInt64Value( UNIT_FIELD_SUMMONEDBY, m_caster->GetGUID() );
		//p->SetUInt64Value( UNIT_FIELD_CREATEDBY, u_caster->GetGUID() );
		p->SetUInt32Value( UNIT_FIELD_FACTIONTEMPLATE, u_caster->GetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE) );
		p->_setFaction();
		p->SetFFA( u_caster->GetFFA() );
		p->PushToWorld(u_caster->GetMapMgr());
		//make sure they will be desumonized (roxor)
		sEventMgr.AddEvent(p, &Creature::SummonExpire, EVENT_SUMMON_EXPIRE, GetDuration(), 1,0);
	}
}

void Spell::SpellEffectSummonGuardian(uint32 i) // Summon Guardian
{
	if ( !u_caster )
		return;

	uint32 cr_entry = m_spellInfo->EffectMiscValue[i];

	uint32 level = 0;//u_caster->getLevel();
	/*if ( u_caster->GetTypeId()==TYPEID_PLAYER && itemTarget )
	{
		if (itemTarget->GetProto() && itemTarget->GetProto()->RequiredSkill == SKILL_ENGINERING)
		{
			uint32 skill202 = static_cast< Player* >( m_caster )->_GetSkillLineCurrent(SKILL_ENGINERING);
			if (skill202>0)
			{
				level = skill202/5;
			}
		}
	}*/

	float angle_for_each_spawn = -float(M_PI) * 2 / damage;
	for( int i = 0; i < damage; i++ )
	{
		float m_fallowAngle = angle_for_each_spawn * i;
		u_caster->create_guardian(cr_entry,GetDuration(),m_fallowAngle,level);
	}
}

void Spell::SpellEffectSkillStep(uint32 i) // Skill Step
{
	Player*target;
	if(m_caster->GetTypeId() != TYPEID_PLAYER)
	{
		// Check targets
		if( m_targets.m_unitTarget )
		{
			target = objmgr.GetPlayer((uint32)m_targets.m_unitTarget);
			if( target == NULL ) 
				return;
		}
		else
			return;
	}
	else
	{
		target = static_cast< Player* >( m_caster );
	}
	
	uint32 skill = m_spellInfo->EffectMiscValue[i];
	if( skill == 242 )
		skill = SKILL_LOCKPICKING; // somehow for lockpicking misc is different than the skill :s

	skilllineentry* sk = dbcSkillLine.LookupEntry( skill );

	if( sk == NULL )
		return;

	uint32 max = 1;
	switch( sk->type )
	{
		case SKILL_TYPE_PROFESSION:
		case SKILL_TYPE_SECONDARY:
			max = damage * 75;
			break;
		case SKILL_TYPE_WEAPON:
			max = 5 * target->getLevel();
			break;
		case SKILL_TYPE_CLASS:
		case SKILL_TYPE_ARMOR:
			if( skill == SKILL_LOCKPICKING )
				max = damage * 75;
			else
				max = 1;
			break;
		default: //u cant learn other types in game
			return;
	};

	if( target->_HasSkillLine( skill ) )
	{
		target->_ModifySkillMaximum( skill, max );
	}		
	else
	{
		// Don't add skills to players logging in.
		/*if((m_spellInfo->Attributes & 64) && playerTarget->m_TeleportState == 1)
			return;*/

		if( sk->type == SKILL_TYPE_PROFESSION )
			target->ModUnsigned32Value( PLAYER_CHARACTER_POINTS2, -1 );
	  
		if( skill == SKILL_RIDING )
			target->_AddSkillLine( skill, max, max );
		else
			target->_AddSkillLine( skill, 1, max );
	}
}

void Spell::SpellEffectSummonObject(uint32 i)
{
	if( !p_caster )
		return;

	uint32 entry = m_spellInfo->EffectMiscValue[i];

	uint32 mapid = u_caster->GetMapId();
	float px = u_caster->GetPositionX();
	float py = u_caster->GetPositionY();
	float pz = u_caster->GetPositionZ();
	pz = CollideInterface.GetHeight( u_caster->GetMapMgr()->GetBaseMap(), px, py, pz + 2.f );
	float orient = m_caster->GetOrientation();

	GameObjectInfo * goI = GameObjectNameStorage.LookupEntry(entry);
	if(!goI)
	{
		if( p_caster != NULL )
		{
			sChatHandler.BlueSystemMessage(p_caster->GetSession(),
			"non-existant gameobject %u tried to be created by SpellEffectSummonObject. Report to devs!", entry);
		}
		return;
	}
	GameObject *go=u_caster->GetMapMgr()->CreateGameObject(entry);
	
	go->SetInstanceID(m_caster->GetInstanceID());
	go->CreateFromProto(entry, mapid, px, py, pz, orient);
	go->SetUInt32Value(GAMEOBJECT_STATE, 1);
	go->SetUInt64Value(OBJECT_FIELD_CREATED_BY,m_caster->GetGUID());
	go->PushToWorld(m_caster->GetMapMgr());	  
	sEventMgr.AddEvent(go, &GameObject::ExpireAndDelete, EVENT_GAMEOBJECT_EXPIRE, GetDuration(), 1,0);
	if(entry ==17032)//this is a portal
	{//enable it for party only
		go-> SetUInt32Value( GAMEOBJECT_STATE,0);
		//disable by default
		MSG_S2C::stObjectUpdate MsgObject;
		go->BuildFieldUpdatePacket(GAMEOBJECT_STATE, 1, &MsgObject);
		SubGroup * pGroup = p_caster->GetGroup() ?
			p_caster->GetGroup()->GetSubGroup(p_caster->GetSubGroup()) : 0;

		if(pGroup)
		{
			for(GroupMembersSet::iterator itr = pGroup->GetGroupMembersBegin();
				itr != pGroup->GetGroupMembersEnd(); ++itr)
			{
				if((*itr)->m_loggedInPlayer && m_caster != (*itr)->m_loggedInPlayer)
					(*itr)->m_loggedInPlayer->GetSession()->SendPacket(MsgObject);
			}
		}
	}
	else if(entry == 36727 || entry == 177193) // Portal of Summoning and portal of doom
	{
		Player * pTarget = p_caster->GetMapMgr()->GetPlayer((uint32)p_caster->GetSelection());
		if(!pTarget)
			return;

		go->m_ritualmembers[0] = p_caster->GetLowGUID();
		go->m_ritualcaster = p_caster->GetLowGUID();
		go->m_ritualtarget = pTarget->GetLowGUID();
		go->m_ritualspell = m_spellInfo->Id;	 
	}
	else//Lightwell,if there is some other type -- add it
	{
		go->charges=5;//Max 5 charges
	}
	p_caster->SetSummonedObject(go);		
}

void Spell::SpellEffectEnchantItem(uint32 i) // Enchant Item Permanent
{
	if(!itemTarget || !p_caster) return;
	EnchantEntry * Enchantment = dbcEnchant.LookupEntry(m_spellInfo->EffectMiscValue[i]);
	if(!Enchantment) return;

	if(p_caster->GetSession()->GetPermissionCount() > 0)
	{
		MyLog::gmlog->notice("enchanted item for %s", itemTarget->GetOwner()->GetName());
		//sGMLog.writefromsession(p_caster->GetSession(), "enchanted item for %s", itemTarget->GetOwner()->GetName());
	}

	//remove other perm enchantment that was enchanted by profession
	itemTarget->RemoveProfessionEnchant();
	int32 Slot = itemTarget->AddEnchantment(Enchantment, 0, true, true, false, 0);
	if(Slot < 0)
		return; // Apply failed
			
	//DetermineSkillUp(SKILL_ENCHANTING);
}

void Spell::SpellEffectEnchantItemTemporary(uint32 i)  // Enchant Item Temporary
{
	if(!itemTarget || !p_caster) return;
	uint32 Duration = damage > 1 ? damage : 1800;

	EnchantEntry * Enchantment = dbcEnchant.LookupEntry(m_spellInfo->EffectMiscValue[i]);
	if(!Enchantment) return;

	itemTarget->RemoveEnchantment(1);
	int32 Slot = itemTarget->AddEnchantment(Enchantment, Duration, false, true, false, 1);
	if(Slot < 0)
		return; // Apply failed

	/*
	skilllinespell* skill = objmgr.GetSpellSkill(m_spellInfo->Id);
	if(skill)
		DetermineSkillUp(skill->skilline,itemTarget->GetProto()->ItemLevel);
	*/
}

void Spell::SpellEffectTameCreature(uint32 i)
{
	Creature *tame = ((unitTarget->GetTypeId() == TYPEID_UNIT) ? ((Creature*)unitTarget) : 0);
	if(!tame)
		return;

	uint8 result = SPELL_CANCAST_OK;

	if(!tame || !p_caster || !p_caster->isAlive() || !tame->isAlive() || p_caster->getClass() != CLASS_BOW )
		result = SPELL_FAILED_BAD_TARGETS;
	else if(!tame->GetCreatureName())
		result = SPELL_FAILED_BAD_TARGETS;
	else if(tame->GetCreatureName()->Type != BEAST)
		result = SPELL_FAILED_BAD_TARGETS;
	else if(tame->getLevel() > p_caster->getLevel())
		result = SPELL_FAILED_HIGHLEVEL;
	else if(p_caster->GeneratePetNumber() == 0)
		result = SPELL_FAILED_BAD_TARGETS;
	else if(!tame->GetCreatureName()->Family)
		result = SPELL_FAILED_BAD_TARGETS;
	else if(p_caster->GetSummon() || p_caster->GetUnstabledPetNumber())
		result = SPELL_FAILED_ALREADY_HAVE_SUMMON;
	{
		CreatureFamilyEntry *cf = dbcCreatureFamily.LookupEntry(tame->GetCreatureName()->Family);
		if(cf && !cf->tameable)
				result = SPELL_FAILED_BAD_TARGETS;
	}
	if(result != SPELL_CANCAST_OK)
	{
		SendCastResult(result);
		return;
	}
	// Remove target
	tame->GetAIInterface()->HandleEvent(EVENT_LEAVECOMBAT, p_caster, 0);
	Pet *pPet = objmgr.CreatePet();
	pPet->SetInstanceID(p_caster->GetInstanceID());
	pPet->CreateAsSummon(tame->GetEntry(), tame->GetCreatureName(), tame, static_cast<Unit*>(p_caster), NULL, 2, 0);
	//tame->SafeDelete();
	//delete tame;
	tame->Despawn(0,tame->proto? tame->proto->RespawnTime:0);
}

void Spell::SpellEffectAddPetToList(uint32 i)
{
	uint32 entryId = m_spellInfo->EffectMiscValue[i];

	if(!p_caster)
		return;

	CreatureProto * pTemplate = CreatureProtoStorage.LookupEntry(entryId);
	CreatureInfo * pCreatureInfo = CreatureNameStorage.LookupEntry(entryId);
	if(!pTemplate || !pCreatureInfo)
	{
		MyLog::log->debug("Invalid creature spawn template: %u", entryId);
		return;
	}

	// spawn a creature of this id to create from
	Creature * pCreature = new Creature(HIGHGUID_TYPE_UNIT);//no need in guid
	CreatureSpawn * sp = new CreatureSpawn;
	sp->id = 1;
	sp->bytes = 0;
	sp->bytes2 = 0;
	sp->displayid = pCreatureInfo->Male_DisplayID;
	sp->emote_state = 0;
	sp->entry = pCreatureInfo->Id;
	sp->factionid = pTemplate->Faction;
	sp->flags = 0;
	sp->form = 0;
	sp->movetype = 0;
	sp->o = p_caster->GetOrientation();
	sp->x = p_caster->GetPositionX();
	sp->y = p_caster->GetPositionY();
	//sp->respawnNpcLink = 0;
	sp->stand_state = 0;
	sp->channel_spell=sp->channel_target_creature=sp->channel_target_go=0;
	pCreature->Load(sp, (uint32)NULL, NULL);

	// Create PlayerPet struct (Rest done by UpdatePetInfo)
	PlayerPet *pp = new PlayerPet;
	pp->number = p_caster->GeneratePetNumber();
	p_caster->SetMaxPetNumber(pp->number);
	pp->stablestate = STABLE_STATE_ACTIVE;
	pp->entry = entryId;
	Pet * pPet = objmgr.CreatePet();
	pPet->CreateAsSummon(entryId, pCreatureInfo, pCreature, p_caster, NULL, 0x3, 0, pp);
	pPet->UpdatePetInfo(false);

	QueryBuffer* buf = new QueryBuffer;
	p_caster->_SavePet( buf );
	sWorld.ExecuteSqlToDBServer( buf );
	p_caster->SendPetList();

	
	delete pPet;

	// remove the temp creature
	delete sp;
	delete pCreature;
}

void Spell::SpellEffectSummonPet(uint32 i) //summon - pet
{
	uint32 entryId = m_spellInfo->EffectMiscValue[i];

	//VoidWalker:torment, sacrifice, suffering, consume shadows
	//Succubus:lash of pain, soothing kiss, seduce , lesser invisibility
	//felhunter:	 Devour Magic,Paranoia,Spell Lock,	Tainted Blood

	if(!p_caster/* || p_caster->getClass() != WARLOCK*/)
		return;
// 		
// 		// remove old pet
// 		Pet *old = static_cast< Player* >(m_caster)->GetSummon();
// 		if(old)
// 			old->Dismiss(false);		
// 		
// 		CreatureInfo *ci = CreatureNameStorage.LookupEntry(m_spellInfo->EffectMiscValue[i]);
// 		if(ci)
// 		{
// 			//if demonic sacrifice auras are still active, remove them
// 			//uint32 spids[] = { 18789, 18790, 18791, 18792, 35701, 0 };
// 			//p_caster->RemoveAuras(spids);
// 			p_caster->RemoveAura(18789);
// 			p_caster->RemoveAura(18790);
// 			p_caster->RemoveAura(18791);
// 			p_caster->RemoveAura(18792);
// 			p_caster->RemoveAura(35701);
// 
// 			Pet *summon = objmgr.CreatePet();
// 			summon->SetInstanceID(m_caster->GetInstanceID());
// 			summon->CreateAsSummon(m_spellInfo->EffectMiscValue[i], ci, NULL, u_caster, m_spellInfo, 1, 0);
// 		}

	CreatureProto * pTemplate = CreatureProtoStorage.LookupEntry(entryId);
	CreatureInfo * pCreatureInfo = CreatureNameStorage.LookupEntry(entryId);
	if(!pTemplate || !pCreatureInfo)
	{
		MyLog::log->debug("Invalid creature spawn template: %u", entryId);
		return;
	}

	// spawn a creature of this id to create from
	Creature * pCreature = new Creature(HIGHGUID_TYPE_UNIT);//no need in guid
	CreatureSpawn * sp = new CreatureSpawn;
	sp->id = 1;
	sp->bytes = 0;
	sp->bytes2 = 0;
	sp->displayid = pCreatureInfo->Male_DisplayID;
	sp->emote_state = 0;
	sp->entry = pCreatureInfo->Id;
	sp->factionid = pTemplate->Faction;
	sp->flags = 0;
	sp->form = 0;
	sp->movetype = 0;
	sp->o = p_caster->GetOrientation();
	sp->x = p_caster->GetPositionX();
	sp->y = p_caster->GetPositionY();
	//sp->respawnNpcLink = 0;
	sp->stand_state = 0;
	sp->channel_spell=sp->channel_target_creature=sp->channel_target_go=0;
	pCreature->Load(sp, (uint32)NULL, NULL);

	Pet *old_tame = p_caster->GetSummon();
	if(old_tame != NULL)
	{
		old_tame->Dismiss(true);
	}

	// create a pet from this creature
	Pet * pPet = objmgr.CreatePet();
	pPet->SetInstanceID(p_caster->GetInstanceID());
	pPet->SetMapId(p_caster->GetMapId());
	pPet->CreateAsSummon(entryId, pCreatureInfo, pCreature, p_caster, NULL, 0x2, 0);

	// remove the temp creature
	delete sp;
	delete pCreature;
}

void Spell::SpellEffectWeapondamage( uint32 i ) // Weapon damage +
{
	if( unitTarget == NULL || u_caster == NULL )
		return;

	uint32 _type;
	if( GetType() == SPELL_DMG_TYPE_RANGED )
		_type = RANGED;
	else
	{
		if (m_spellInfo->Flags4 & 0x1000000)
			_type =  OFFHAND;
		else
			_type = MELEE;
	}

	if(m_spellInfo->speed > 0)
	{
		float dist = m_caster->CalcDistance( unitTarget );
		float time = ((dist*1000.0f)/m_spellInfo->speed);
		damageToHit = damage;
		sEventMgr.AddEvent(u_caster,&Unit::EventStrikeWithAbility,unitTarget->GetGUID(),
			m_spellInfo, (int32)0, (int32)0, (uint32)damage, false, false, false, true, false, EVENT_SPELL_DAMAGE_HIT, float2int32(time), 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);

	}
	else
	{
		u_caster->Strike( unitTarget, _type, m_spellInfo, damage, 0, 0, false, false, true, true, true );
	}
	
}

void Spell::SpellEffectPowerBrunFullDamage(uint32 i)
{
	if(!unitTarget)
		return;
	if(!unitTarget->isAlive())
		return;
	if (unitTarget->GetPowerType() != POWER_TYPE_MANA)
		return;

	int burnmana = 0;
	if( m_spellInfo->EffectMiscValue[i] == 0 )
		burnmana = unitTarget->GetUInt32Value( UNIT_FIELD_POWER1 ) * damage / 100;
	else
		burnmana = unitTarget->GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) * damage / 100;

	if( burnmana )
		unitTarget->ModUnsigned32Value( UNIT_FIELD_POWER1, -burnmana );
}



void Spell::SpellEffectPowerBurn(uint32 i) // power burn
{
	if(!unitTarget)
		return;
	if(!unitTarget->isAlive())
		return;
	if (unitTarget->GetPowerType() != POWER_TYPE_MANA)
		return;
	if( unitTarget->IsPlayer() )
	{
		// Resilience - reduces the effect of mana drains by (CalcRating*2)%.

		damage *= float2int32( 1 - ( ( static_cast<Player*>(unitTarget)->CalcRating( PLAYER_RATING_MODIFIER_SPELL_CRIT_RESILIENCE ) * 2 ) / 100.0f ) );
	}
	int32 mana = (int32)min( (int32)unitTarget->GetUInt32Value( UNIT_FIELD_POWER1 ), damage );
	unitTarget->ModUnsigned32Value(UNIT_FIELD_POWER1,-mana);
	
	if(p_caster && p_caster->m_bTriggerXP)
	{
		damage += (21/4)*p_caster->getLevel()*(5/4)*12.5;
		p_caster->RemoveAura(XP_TRIGGER_SPELL);
	}
	float scale = float(m_spellInfo->EffectDieSides[i]) / 100;

	m_caster->SpellNonMeleeDamageLog(unitTarget,m_spellInfo->Id, (uint32)(mana * scale), true,true, m_curattackcnt);   
}



void Spell::SpellEffectThreat(uint32 i) // Threat
{
 	if(!unitTarget || !unitTarget->isAlive() || !unitTarget->IsCreature() )
 		return;
 
 	bool chck = unitTarget->GetAIInterface()->modThreatByPtr(u_caster, damage);
 	if(chck == false)
 		unitTarget->GetAIInterface()->AttackReaction(u_caster, damage, 0);

	return;

	/*
	uint32 dur = GetDuration();
	float r = GetRadius(i);

	switch(m_targets.m_targetMask)
	{		
	case TARGET_FLAG_SELF:
		{
			// 			dynObj->Create(u_caster, this,	m_caster->GetPositionX(), 
			// 				m_caster->GetPositionY(), m_caster->GetPositionZ(), dur,r);
			FillAllTargetsInArea(i, m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ(), r);
		}break;
	case TARGET_FLAG_UNIT:
		{
			if(!unitTarget||!unitTarget->isAlive())
				break;
			// 			dynObj->Create( u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
			// 				dur, r);
			FillAllTargetsInArea(i, m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ(), r);
		}break;
	case TARGET_FLAG_OBJECT:
		{
			if(!unitTarget)
				break;
			if(!unitTarget->isAlive())
				break;
			// 			dynObj->Create(u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
			// 				dur, r);
			FillAllTargetsInArea(i, u_caster->GetPositionX(), u_caster->GetPositionY(), u_caster->GetPositionZ(), r);
		}break;
	case TARGET_FLAG_SOURCE_LOCATION:
		{
			// 			dynObj->SetInstanceID(m_caster->GetInstanceID());
			// 			dynObj->Create(u_caster, this, m_targets.m_srcX,
			// 				m_targets.m_srcY, m_targets.m_srcZ, dur,r);
			FillAllTargetsInArea(i, m_targets.m_srcX,m_targets.m_srcY, m_targets.m_srcZ, r);
		}break;
	case TARGET_FLAG_DEST_LOCATION:
		{
			// 			dynObj->SetInstanceID(m_caster->GetInstanceID());
			// 			dynObj->Create(u_caster?u_caster:g_caster->m_summoner, this,
			// 				m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ,dur,r);
			FillAllTargetsInArea(i, m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ, r);
		}break;
	default:
		return;
	}

	if(IsDamagingSpell(m_spellInfo))
	{
		p_caster->m_inarmedtimeleft = 20000;
		//else
		p_caster->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);
	}

	for(TargetsList::iterator iter=m_targetUnits->begin();iter!=m_targetUnits->end();iter++)
	{
		Unit* target = u_caster->GetMapMgr()->GetUnit(*iter);

		if(target && target->isAlive())
		{
			bool chck = target->GetAIInterface()->modThreatByPtr(u_caster,m_spellInfo->EffectBasePoints[i]);
			if(chck == false)
				target->GetAIInterface()->AttackReaction(u_caster,1,0);
		}
	}
	*/
}

void Spell::SpellEffectTriggerSpell(uint32 i) // Trigger Spell
{
	if(!unitTarget)
		return;

	Spell*sp=new Spell(m_caster,dbcSpell.LookupEntry(m_spellInfo->EffectTriggerSpell[i]),true,NULL);
	SpellCastTargets tgt(unitTarget->GetGUID());
	sp->prepare(&tgt);

	//if(m_spellInfo->EffectTriggerSpell[i] != 0)
	  //  TriggerSpellId = m_spellInfo->EffectTriggerSpell[i];	
}

void Spell::SpellEffectHealthFunnel(uint32 i) // Health Funnel
{
	if(!unitTarget)
		return;		
	if(!unitTarget->isAlive() || !unitTarget->IsPet())
		return;

	//does not exist
}

void Spell::SpellEffectPowerFunnel(uint32 i) // Power Funnel
{
	if(!unitTarget)
		return;		
	if(!unitTarget->isAlive() || !unitTarget->IsPet())
		return;

	//does not exist
}

void Spell::SpellEffectHealMaxHealth(uint32 i)   // Heal Max Health
{
	if(unitTarget)
	{
		if( !unitTarget->isAlive() )
		{
			MyLog::log->debug("Spell Head: target is dead");
			SendCastResult(SPELL_FAILED_TARGETS_DEAD);
			return;
		}
		if(!isFriendly(unitTarget, m_caster))
		{
			MyLog::log->debug("Spell Head: target is not friend");
			SendCastResult(SPELL_FAILED_TARGET_ENEMY);
			return;
		}
	}
	else
	{
		unitTarget = (Unit*)m_caster;
		if(m_caster->IsPlayer())
		{
			playerTarget = (Player*)m_caster;
		}
	}

	uint32 dif = unitTarget->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) - unitTarget->GetUInt32Value( UNIT_FIELD_HEALTH );
	if( !dif )
	{
		SendCastResult( SPELL_FAILED_ALREADY_AT_FULL_HEALTH );
		return;
	}

	
	SendHealSpellOnPlayer( m_caster, unitTarget , dif, false );
	
	unitTarget->ModUnsigned32Value( UNIT_FIELD_HEALTH, dif );
}

void Spell::SpellEffectInterruptCast(uint32 i) // Interrupt Cast
{
	if(!unitTarget || !unitTarget->isAlive())
		return;
	// can't apply stuns/fear/polymorph/root etc on boss
	if(unitTarget->GetTypeId()==TYPEID_UNIT)
	{
		Creature * c = (Creature*)( unitTarget );
		if (c&&c->GetCreatureName()&&c->GetCreatureName()->Rank == ELITE_WORLDBOSS)
			return;
	}
	// FIXME:This thing prevent target from spell casting too but cant find.
	uint32 school=0;
	if(unitTarget->GetCurrentSpell())
	{
		school=unitTarget->GetCurrentSpell()->m_spellInfo->School;
	}
	unitTarget->InterruptSpell();
	if(school)//prevent from casts in this school
	{
		unitTarget->SchoolCastPrevent[school]=GetDuration()+getMSTime();
		// TODO: visual!
	}
}

void Spell::SpellEffectDistract(uint32 i) // Distract
{
	//spellId 1725 Distract:Throws a distraction attracting the all monsters for ten sec's
	if(!unitTarget)
		return;
	if(!unitTarget->isAlive())
		return;

	if(m_targets.m_destX != 0.0f || m_targets.m_destY != 0.0f || m_targets.m_destZ != 0.0f)
	{
//		unitTarget->GetAIInterface()->MoveTo(m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ, 0);
		uint32 Stare_duration=GetDuration();
		if(Stare_duration>30*60*1000)
			Stare_duration=10000;//if we try to stare for more then a half an hour then better not stare at all :P (bug)
		float newo=unitTarget->calcRadAngle(unitTarget->GetPositionX(),unitTarget->GetPositionY(),m_targets.m_destX,m_targets.m_destY);
		unitTarget->GetAIInterface()->StopMovement(Stare_duration);
		unitTarget->SetFacing(newo);
	}

	//Smoke Emitter 164870
	//Smoke Emitter Big 179066
	//Unit Field Target of 
}

void Spell::SpellEffectPickpocket(uint32 i) // pickpocket
{
	//Show random loot based on roll,	
	if(!unitTarget)
		return; // impossible..
	if(!p_caster)
		return;

	if(unitTarget->GetTypeId() != TYPEID_UNIT)
		return;

	Creature *target = static_cast<Creature*>( unitTarget );
	if(target->IsPickPocketed() || (target->GetCreatureName() && target->GetCreatureName()->Type != HUMANOID))
	{
		SendCastResult(SPELL_FAILED_TARGET_NO_POCKETS);
		return;
	}
			
  lootmgr.FillPickpocketingLoot(&((Creature*)unitTarget)->loot,unitTarget->GetEntry());

	uint32 _rank = ((Creature*)unitTarget)->GetCreatureName() ? ((Creature*)unitTarget)->GetCreatureName()->Rank : 0;
	unitTarget->loot.gold = float2int32((_rank+1) * unitTarget->getLevel() * (RandomUInt(5) + 1) * sWorld.getRate(RATE_MONEY));

	p_caster->SendLoot(unitTarget->GetGUID(), LOOT_PICKPOCKETING );
	target->SetPickPocketed(true);
}

void Spell::SpellEffectAddFarsight(uint32 i) // Add Farsight
{
	if(!p_caster)
		return;

	float x = m_targets.m_destX;
	float y = m_targets.m_destY;
	float z = m_targets.m_destZ;
	if(x == 0)
		x = m_targets.m_srcX;
	if(y == 0)
		y = m_targets.m_srcY;
	if(z == 0)
		z = m_targets.m_srcZ;

	DynamicObject * dynObj = p_caster->GetMapMgr()->CreateDynamicObject();
	dynObj->Create(u_caster, this, x, y, z, GetDuration(), GetRadius(i));
	if( dynObj == NULL ) //i <3 burlex :P
	{
		delete dynObj;
		return;
	}
    dynObj->SetUInt32Value(OBJECT_FIELD_TYPE, 65);
    dynObj->SetUInt32Value(DYNAMICOBJECT_BYTES, 0x80000002);
	dynObj->SetInstanceID(p_caster->GetInstanceID());	
	p_caster->SetUInt64Value(PLAYER_FARSIGHT, dynObj->GetGUID());

	p_caster->GetMapMgr()->ChangeFarsightLocation(p_caster, dynObj);
}

void Spell::SpellEffectSummonPossessed(uint32 i) // eye of kilrog
{
	/*
	m_target->DisableAI();
	pCaster->SetUInt64Value(UNIT_FIELD_SUMMON, 0);
	m_target->SetUInt64Value(UNIT_FIELD_SUMMONEDBY, 0);
	pCaster->SetUInt64Value(UNIT_FIELD_CHARM, m_target->GetGUID());
	m_target->SetUInt64Value(UNIT_FIELD_CHARMEDBY, pCaster->GetGUID());
	pCaster->SetUInt64Value(PLAYER_FARSIGHT, m_target->GetGUID());
	pCaster->m_CurrentCharm = ((Creature*)m_target);
	m_target->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PLAYER_CONTROLLED_CREATURE);
	pCaster->m_noInterrupt = 1;
	pCaster->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER);

	WorldPacket data(SMSG_DEATH_NOTIFY_OBSOLETE);
	data << m_target->GetNewGUID() << uint8(1);
	pCaster->GetSession()->SendPacket(&data);
	*/

/*
	CreatureInfo *ci = CreatureNameStorage.LookupEntry(m_spellInfo->EffectMiscValue[i]);
	if( ci)
	{
		Creature* NewSummon = m_caster->GetMapMgr()->CreateCreature();
		// Create
		NewSummon->SetInstanceID(m_caster->GetInstanceID());
		NewSummon->Create( ci->Name, m_caster->GetMapId(), 
			m_caster->GetPositionX()+(3*(cos((float(M_PI)/2)+m_caster->GetOrientation()))), m_caster->GetPositionY()+(3*(cos((float(M_PI)/2)+m_caster->GetOrientation()))), m_caster->GetPositionZ(), m_caster->GetOrientation());

		// Fields
		NewSummon->SetUInt32Value(UNIT_FIELD_LEVEL,m_caster->GetUInt32Value(UNIT_FIELD_LEVEL));
		NewSummon->SetUInt32Value(UNIT_FIELD_DISPLAYID,  ci->Male_DisplayID);
		NewSummon->SetUInt32Value(UNIT_FIELD_NATIVEDISPLAYID, ci->Male_DisplayID);
		NewSummon->SetUInt64Value(UNIT_FIELD_SUMMONEDBY, m_caster->GetGUID());
		NewSummon->SetUInt64Value(UNIT_FIELD_CREATEDBY, m_caster->GetGUID());
		NewSummon->SetUInt32Value(UNIT_FIELD_HEALTH , 100);
		NewSummon->SetUInt32Value(UNIT_FIELD_MAXHEALTH , 100);
		NewSummon->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, 35);
		NewSummon->SetFloatValue(OBJECT_FIELD_SCALE_X,1.0f);
		NewSummon->SetUInt32Value(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_9 | UNIT_FLAG_PLAYER_CONTROLLED_CREATURE);

		//Setting faction
		NewSummon->_setFaction();
		NewSummon->m_temp_summon=true;

		// Add To World
		NewSummon->PushToWorld(m_caster->GetMapMgr());
		
		// Force an update on the player to create this guid.
		p_caster->ProcessPendingUpdates();

		//p_caster->SetUInt64Value(UNIT_FIELD_SUMMON, NewSummon->GetGUID());
		//p_caster->SetUInt64Value(PLAYER_FARSIGHT, NewSummon->GetGUID());
		//p_caster->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER);
		p_caster->Possess(NewSummon);
	}*/
}

void Spell::SpellEffectCreateSummonTotem(uint32 i)
{
	//Atalai Skeleton Totem
}

void Spell::SpellEffectHealMechanical(uint32 i)
{	
	if(!unitTarget)
		return;
	if(unitTarget->GetTypeId() != TYPEID_UNIT)
		return;
	if(((Creature*)unitTarget)->GetCreatureName()->Type != MECHANICAL)
		return;

	Heal((int32)damage);
}

void Spell::SpellEffectSummonObjectWild(uint32 i)
{
	if(!u_caster)
		return;
  
	// spawn a new one
	GameObject *GoSummon = u_caster->GetMapMgr()->CreateGameObject(m_spellInfo->EffectMiscValue[i]);
	if(!GoSummon->CreateFromProto(m_spellInfo->EffectMiscValue[i],
		m_caster->GetMapId(), m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ(), m_caster->GetOrientation() ))
	{
		delete GoSummon;
		return;
	}
	
	GoSummon->SetUInt32Value( GAMEOBJECT_FACTION, u_caster->GetUInt32Value( UNIT_FIELD_FACTIONTEMPLATE ) );
	GoSummon->_setFaction();
	GoSummon->SetInstanceID(m_caster->GetInstanceID());
	GoSummon->SetUInt32Value(GAMEOBJECT_LEVEL, u_caster->getLevel());
	//GoSummon->SetUInt64Value(OBJECT_FIELD_CREATED_BY, m_caster->GetGUID());
	GoSummon->SetUInt32Value(GAMEOBJECT_STATE, 0);
	GoSummon->PushToWorld(u_caster->GetMapMgr());
	GoSummon->SetSummoned(u_caster);
	
	sEventMgr.AddEvent(GoSummon, &GameObject::ExpireAndDelete, EVENT_GAMEOBJECT_EXPIRE, GetDuration(), 1,0);
}

void Spell::SpellEffectScriptEffect(uint32 i) // Script Effect
{
	uint32 spellId = m_spellInfo->Id;

	// Try a dummy SpellHandler
	if(sScriptMgr.CallScriptedDummySpell(spellId, i, this))
		return;
}

void Spell::SpellEffectSanctuary(uint32 i) // Stop all attacks made to you
{
	if(!u_caster)
		return;

	Object::InRangeSet::iterator itr = u_caster->GetInRangeOppFactsSetBegin();
	Object::InRangeSet::iterator itr_end = u_caster->GetInRangeOppFactsSetEnd();
	Unit * pUnit = NULL;

	for( ; itr != itr_end; ++itr ) {
		pUnit = static_cast<Unit*>(*itr);

		if( pUnit->GetTypeId() == TYPEID_UNIT )
			pUnit->GetAIInterface()->RemoveThreatByPtr( unitTarget );
	}
}

void Spell::SpellEffectAddComboPoints(uint32 i) // Add Combo Points
{
	if(!p_caster)
		return;
}

void Spell::SpellEffectCreateHouse(uint32 i) // Create House
{


}

void Spell::SpellEffectDuel(uint32 i) // Duel
{
	if(!p_caster)
		return;
	if(!p_caster->isAlive())
		return;

	if (p_caster->IsStealth())
	{
		SendCastResult(SPELL_FAILED_CANT_DUEL_WHILE_STEALTHED);
		return; // Player is stealth
	}
	if(!playerTarget)
		return;
	if(playerTarget == p_caster)
		return;

	/* not implemented yet
	TODO: dueling zones ? ( SPELL_FAILED_NO_DUELING )
	if (player->IsInvisible())
	{
		SendCastResult(SPELL_FAILED_CANT_DUEL_WHILE_INVISIBLE);
		return;
	}
	*/

	//Player *pTarget = sObjHolder.GetObject<Player>(player->GetSelection());	   //  hacky.. and will screw up if plr is deselected..
	if (!playerTarget)
	{
		SendCastResult(SPELL_FAILED_BAD_TARGETS);
		return; // invalid Target
	}
	if (!playerTarget->isAlive())
	{
		SendCastResult(SPELL_FAILED_TARGETS_DEAD);
		return; // Target not alive
	}
	if (playerTarget->hasStateFlag(UF_ATTACKING))
	{
		SendCastResult(SPELL_FAILED_TARGET_IN_COMBAT);
		return; // Target in combat with another unit
	}
	if (playerTarget->DuelingWith != NULL)
	{
		SendCastResult(SPELL_FAILED_TARGET_DUELING);
		return; // Already Dueling
	}

	p_caster->RequestDuel(playerTarget);
}

void Spell::SpellEffectStuck(uint32 i)
{
    if(!playerTarget || playerTarget != p_caster)
        return;

	sEventMgr.AddEvent(playerTarget,&Player::EventTeleport,playerTarget->GetBindMapId(),playerTarget->GetBindPositionX(),playerTarget->GetBindPositionY(),
		playerTarget->GetBindPositionZ(),EVENT_PLAYER_TELEPORT,50,1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
	/*
	playerTarget->SafeTeleport(playerTarget->GetBindMapId(), 0, playerTarget->GetBindPositionX(), playerTarget->GetBindPositionY(), playerTarget->GetBindPositionZ(), 3.14f);*/
}

void Spell::SpellEffectSummonPlayer(uint32 i)
{
	if( !EffectTargetGUID || !p_caster )
		return;

	//if(m_caster->GetMapMgr()->GetMapInfo() && m_caster->GetMapMgr()->GetMapInfo()->type != INSTANCE_NULL && m_caster->GetMapId() != playerTarget->GetMapId())
	//	return;
	if( GET_TYPE_FROM_GUID(EffectTargetGUID) == 0 )
	{
		playerTarget = objmgr.GetPlayer( GET_LOWGUID_PART(EffectTargetGUID) );
		unitTarget = playerTarget;

		if( playerTarget )
			playerTarget->SummonRequest( p_caster->GetName(), p_caster->GetLowGUID(), p_caster->GetMapId(), p_caster->GetInstanceID(), p_caster->GetPosition() );
	}
}

void Spell::SpellEffectActivateObject(uint32 i) // Activate Object
{
	/*if(!p_caster)
		return;

	if(!gameObjTarget)
		return;

	gameObjTarget->SetUInt32Value(GAMEOBJECT_DYN_FLAGS, 1);

	sEventMgr.AddEvent(gameObjTarget, &GameObject::Deactivate, EVENT_GAMEOBJECT_DEACTIVATE, GetDuration(), 1);*/
}

void Spell::SpellEffectSummonTotem(uint32 i) // Summon Totem
{
	if(!p_caster) 
		return;

	float x = p_caster->GetPositionX();
	float y = p_caster->GetPositionY();

	uint32 slot = m_spellInfo->Effect[i] - SPELL_EFFECT_SUMMON_TOTEM_SLOT1;

	switch(m_spellInfo->Effect[i])
	{
	case SPELL_EFFECT_SUMMON_TOTEM_SLOT1: 
		x -= 1.5f;
		y -= 1.5f;
		break;
	case SPELL_EFFECT_SUMMON_TOTEM_SLOT2: 
		x -= 1.5f;
		y += 1.5f;
		break;
	case SPELL_EFFECT_SUMMON_TOTEM_SLOT3:  
		x += 1.5f;
		y -= 1.5f;
		break;
	case SPELL_EFFECT_SUMMON_TOTEM_SLOT4: 
		x += 1.5f;
		y += 1.5f;
		break;
	default:
		break;
	}

	float z = CollideInterface.GetHeight( p_caster->GetMapMgr()->GetBaseMap(), x, y, p_caster->GetPositionZ() + 2.f );

	if(p_caster->m_TotemSlots[slot] != 0)
		p_caster->m_TotemSlots[slot]->TotemExpire();

	uint32 entry = m_spellInfo->EffectMiscValue[i];

	CreatureInfo* ci = CreatureNameStorage.LookupEntry(entry);
	if(!ci)
	{
		MyLog::log->debug("Missing totem creature entry : %u \n",entry);
		return;
	}

	// Obtain the spell we will be casting.
	SpellEntry * TotemSpell = ObjectMgr::getSingleton().GetTotemSpell(m_spellInfo->Id);
	if(TotemSpell == 0) 
	{
		MyLog::log->debug("Totem %u does not have any spells to cast, exiting\n",entry);
		return;
	}

	Creature * pTotem = p_caster->GetMapMgr()->CreateCreature(entry);

	CreatureProto* proto = CreatureProtoStorage.LookupEntry(entry);
	CreatureInfo* creature_info = CreatureNameStorage.LookupEntry(entry);
	pTotem->proto = proto;
	pTotem->creature_info = creature_info;
	//pTotem->Load((CreatureProto*)NULL, 0, 0, 0);

	p_caster->m_TotemSlots[slot] = pTotem;
	pTotem->SetTotemOwner(p_caster);
	pTotem->SetTotemSlot(slot);

	float landh = CollideInterface.GetHeight( p_caster->GetMapMgr()->GetBaseMap(), x, y, z + 4.f );
	float landdiff = landh - p_caster->GetPositionZ();

	if (fabs(landdiff) > 5.f)
		pTotem->Create(ci->Name, p_caster->GetMapId(), x, y, p_caster->GetPositionZ(), p_caster->GetOrientation());
	else
		pTotem->Create(ci->Name, p_caster->GetMapId(), x, y, landh, p_caster->GetOrientation());

	uint32 displayID = 0;

	if( p_caster->GetTeam() == 0 )
	{
		if ( ci->Female_DisplayID != 0 )
		{
			displayID = ci->Female_DisplayID; //this is the nice solution provided by emsy
		}
		else //this is the case when you are using a blizzlike db
		{
			displayID = ci->Male_DisplayID;
		}
	}
	else
	{
		displayID = ci->Male_DisplayID;
	}

	// Set up the creature.
	pTotem->SetUInt32Value(OBJECT_FIELD_ENTRY, entry);
	pTotem->SetFloatValue(OBJECT_FIELD_SCALE_X, 1.0f);
	pTotem->SetUInt64Value(UNIT_FIELD_CREATEDBY, p_caster->GetGUID());
	pTotem->SetUInt32Value(UNIT_FIELD_HEALTH, 5);
	pTotem->SetUInt32Value(UNIT_FIELD_MAXHEALTH, 5);
	pTotem->SetUInt32Value(UNIT_FIELD_POWER1, p_caster->getLevel() * 30);
	pTotem->SetUInt32Value(UNIT_FIELD_MAXPOWER1, p_caster->getLevel() * 30);
	pTotem->SetUInt32Value(UNIT_FIELD_LEVEL, p_caster->getLevel());
	pTotem->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, p_caster->GetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE));
	pTotem->SetUInt32Value(UNIT_FIELD_BYTES_0, (1 << 8) | (2 << 16) | (1 << 24));
	pTotem->SetUInt32Value(UNIT_FIELD_FLAGS, UNIT_FLAG_PLAYER_CONTROLLED | UNIT_FLAG_SELF_RES);
	pTotem->SetUInt32Value(UNIT_FIELD_BASEATTACKTIME, 2000);
	pTotem->SetUInt32Value(UNIT_FIELD_BASEATTACKTIME_01, 2000);
	pTotem->SetFloatValue(UNIT_FIELD_BOUNDINGRADIUS, 1.0f);
	pTotem->SetFloatValue(UNIT_FIELD_COMBATREACH, 1.0f);
	pTotem->SetUInt32Value(UNIT_FIELD_DISPLAYID, displayID);
	pTotem->SetUInt32Value(UNIT_FIELD_NATIVEDISPLAYID, ci->Male_DisplayID); //blizzlike :P
	pTotem->SetFloatValue(UNIT_MOD_CAST_SPEED, 1.0f);
	pTotem->SetUInt32Value(UNIT_CREATED_BY_SPELL, m_spellInfo->Id);
	pTotem->SetUInt32Value(UNIT_FIELD_BYTES_2, 1 | (0x28 << 8));

	// Initialize faction stuff.
	pTotem->m_faction = p_caster->m_faction;
	pTotem->m_factionDBC = p_caster->m_factionDBC;

	//added by Zack : Some shaman talents are casted on player but it should be inherited or something by totems
	pTotem->InheritSMMods(p_caster);

	// Set up AI, depending on our spells.
	uint32 j;
	for( j = 0; j < 3; ++j )
	{
		if( TotemSpell->Effect[j] == SPELL_EFFECT_APPLY_AREA_AURA || TotemSpell->Effect[j] == SPELL_EFFECT_PERSISTENT_AREA_AURA || TotemSpell->EffectApplyAuraName[j] == SPELL_AURA_PERIODIC_TRIGGER_SPELL )
		{
			break;
		}
	}
	// Setup complete. Add us to the world.
	pTotem->PushToWorld(m_caster->GetMapMgr());

	if(j != 3)
	{
		// We're an area aura. Simple. Disable AI and cast the spell.
		pTotem->DisableAI();
		pTotem->GetAIInterface()->totemspell = m_spellInfo;

		Spell * pSpell = new Spell(pTotem, TotemSpell, true, 0);

		SpellCastTargets targets;
		targets.m_destX = pTotem->GetPositionX();
		targets.m_destY = pTotem->GetPositionY();
		targets.m_destZ = pTotem->GetPositionZ();
		targets.m_targetMask = TARGET_FLAG_DEST_LOCATION;

		pSpell->prepare(&targets);
	}
	else
	{
		// We're a casting totem. Switch AI on, and tell it to cast this spell.
		pTotem->EnableAI();
		pTotem->GetAIInterface()->Init(pTotem, AITYPE_TOTEM, MOVEMENTTYPE_NONE, p_caster);
		pTotem->GetAIInterface()->totemspell = TotemSpell;
		uint32 timer = 3000;	// need a proper resource for this.

		switch(TotemSpell->Id)
		{
		default:break;
		}

		pTotem->GetAIInterface()->m_totemspelltimer = m_spellInfo->EffectAmplitude[i];
		pTotem->GetAIInterface()->m_totemspelltime = m_spellInfo->EffectAmplitude[i];
	}

	//in case these are our elemental totems then we should set them up

	// Set up the deletion event. The totem needs to expire after a certain time, or upon its death.
	sEventMgr.AddEvent(pTotem, &Creature::TotemExpire, EVENT_TOTEM_EXPIRE, GetDuration(), 1,0);
}

void Spell::SpellEffectSelfResurrect(uint32 i)
{
	if(!p_caster)   
		return;
	if(!playerTarget)
		return;
	if(playerTarget->isAlive())
		return;
	uint32 mana;
	uint32 health;
	uint32 class_=unitTarget->getClass();
	
	health = m_spellInfo->EffectMiscValue[i];
	mana = abs( damage );
	
	playerTarget->m_resurrectHealth = health;
	playerTarget->m_resurrectMana = mana;

	playerTarget->ResurrectPlayer();
	playerTarget->SetMovement(MOVE_UNROOT, 1);

	playerTarget->SetUInt32Value(PLAYER_SELF_RES_SPELL, 0);
}

void Spell::SpellEffectSkinning(uint32 i)
{
	if( unitTarget == NULL )
		return;

	uint32 sk = static_cast< Player* >( m_caster )->_GetSkillLineCurrent( SKILL_SKINNING );
	uint32 lvl = unitTarget->getLevel();

	if( ( sk >= lvl * 5 ) || ( ( sk + 100 ) >= lvl * 10 ) )
	{
		//Fill loot for Skinning
		lootmgr.FillSkinningLoot(&((Creature*)unitTarget)->loot,unitTarget->GetEntry());
		static_cast< Player* >( m_caster )->SendLoot( unitTarget->GetGUID(), LOOT_SKINNING );
		
		//Not skinable again
		((Creature*)unitTarget)->Skinned = true;
		unitTarget->BuildFieldUpdatePacket( p_caster, UNIT_FIELD_FLAGS, 0 );

		//still lootable
		//pkt=unitTarget->BuildFieldUpdatePacket(UNIT_DYNAMIC_FLAGS,U_DYN_FLAG_LOOTABLE);
		//static_cast< Player* >( m_caster )->GetSession()->SendPacket(pkt);
		//delete pkt;	 
		DetermineSkillUp(SKILL_SKINNING,sk<lvl*5?sk/5:lvl);
	}
	else
	{
		SendCastResult(SPELL_FAILED_TARGET_UNSKINNABLE);
	}   
			
//	DetermineSkillUp(SKILL_SKINNING,unitTarget->getLevel());
}

void Spell::SpellEffectCharge(uint32 i)
{
	if(!unitTarget)
		return;
	//if(!p_caster) who said units can't charge? :P
	//	return;
	if(!unitTarget->isAlive())
		return;
    if (u_caster->IsAsleep() || u_caster->IsStunned() || u_caster->m_rooted || u_caster->IsPacified() || u_caster->IsFeared())
        return;

	float x, y, z;
	float dx,dy;

	//if(unitTarget->GetTypeId() == TYPEID_UNIT)
	//	if(unitTarget->GetAIInterface())
	//		unitTarget->GetAIInterface()->StopMovement(5000);
	if(unitTarget->GetPositionX() == 0.0f || unitTarget->GetPositionY() == 0.0f)
		return;
	
	dx=unitTarget->GetPositionX()-m_caster->GetPositionX();
	dy=unitTarget->GetPositionY()-m_caster->GetPositionY();
	if(dx == 0.0f || dy == 0.0f)
		return;

	float d = sqrt(dx*dx+dy*dy)-unitTarget->GetFloatValue(UNIT_FIELD_BOUNDINGRADIUS)-m_caster->GetFloatValue(UNIT_FIELD_BOUNDINGRADIUS);
	float alpha = atanf(dy/dx);
	if(dx<0)
		alpha += float(M_PI);

	x = d*cosf(alpha)+m_caster->GetPositionX();
	y = d*sinf(alpha)+m_caster->GetPositionY();
	z = unitTarget->GetPositionZ();

	uint32 time = uint32( (m_caster->CalcDistance(unitTarget) / ((m_caster->m_runSpeed * 3.5) * 0.001f)) + 0.5);

	(static_cast<Unit*>(m_caster))->SendMonsterMove(x, y, z, 0, MOVE_TYPE_NORMAL, MOVEFLAG_WALK, time);
	if(unitTarget->GetTypeId() == TYPEID_UNIT)
		unitTarget->GetAIInterface()->StopMovement(2000);
	
	u_caster->SetPosition(x,y,z,alpha,true);
	u_caster->addStateFlag(UF_ATTACKING);
	u_caster ->smsg_AttackStart( unitTarget );
	if( p_caster )
	{
		p_caster->EventAttackStart();
		p_caster->ResetHeartbeatCoords();
	}
	u_caster->setAttackTimer(time, false);
	u_caster->setAttackTimer(time, true);


	// trigger an event to reset speedhack detection
	if( p_caster )
	{
		p_caster->DelaySpeedHack( time + 1000 );
		p_caster->z_axisposition = 0.0f;
	}
}

void Spell::SpellEffectPlayerPull( uint32 i )
{
	if( unitTarget == NULL || !unitTarget->isAlive() || !unitTarget->IsPlayer() )
		return;

	Player* p_target = static_cast< Player* >( unitTarget );

	if( !p_target->isAlive() )
		return;

	if (p_target->MechanicsDispels[MECHANIC_PULL] > 0)
	{
		MSG_S2C::stCCResult ccmsg;
		ccmsg.result = MSG_S2C::stCCResult::_IMMUNE;
		ccmsg.effect = MSG_S2C::stCCResult::_PULL;
		ccmsg.spellid = m_spellInfo->Id;
		ccmsg.casterid = m_caster->GetGUID();
		ccmsg.victimid = p_target->GetGUID();
		p_target->GetSession()->SendPacket( ccmsg );
		if( m_caster && m_caster->IsPlayer() )
		{
			((Player*)m_caster)->GetSession()->SendPacket( ccmsg );
		}
		return;
	}

	// calculate destination
	float pullD = p_target->CalcDistance( m_caster ) - p_target->GetFloatValue( UNIT_FIELD_BOUNDINGRADIUS ) - m_caster->GetFloatValue( UNIT_FIELD_BOUNDINGRADIUS ) - 1.0f;
	float pullO = p_target->calcRadAngle( p_target->GetPositionX(), p_target->GetPositionY(), m_caster->GetPositionX(), m_caster->GetPositionY() );
	float pullX = p_target->GetPositionX() + pullD * cosf( pullO );
	float pullY = p_target->GetPositionY() + pullD * sinf( pullO );
	float pullZ = CollideInterface.GetHeight( m_caster->GetMapMgr()->GetBaseMap(), pullX, pullY, m_caster->GetPositionZ() + 2.f );
	if( fabs( pullZ - m_caster->GetPositionZ() ) > 10.f )
	{
		pullX = p_caster->GetPositionX();
		pullY = p_caster->GetPositionY();
		pullZ = p_caster->GetPositionZ();
	}
	uint32 time = uint32( pullD * 42.0f );

	p_target->SetOrientation( pullO );

	p_target->DismissMount();
	p_target->RemoveAllAurasByMechanic(MECHANIC_FLEEING, -1, true);
	p_target->SendMonsterMove(pullX, pullY, pullZ, pullO, MOVE_TYPE_POLL, MOVEFLAG_WALK, time);

	p_target->SetPosition( pullX, pullY, pullZ, false );
}

void Spell::SpellEffectSummonCritter(uint32 i)
{
	if(!u_caster || u_caster->IsInWorld() == false)
		return;

	uint32 SummonCritterID = m_spellInfo->EffectMiscValue[i];

	// m_spellInfo->EffectDieSides[i] has something to do with dismissing our critter
	// when it is 1, it means to just dismiss it if we already have it
	// when it is 0, it could mean to always summon a new critter, but there seems to be exceptions

	if(u_caster->critterPet)
	{
		// if we already have this critter, we will just dismiss it and return
		if(u_caster->critterPet->GetCreatureName() && u_caster->critterPet->GetCreatureName()->Id == SummonCritterID)
		{
			u_caster->critterPet->RemoveFromWorld(false,true);
			delete u_caster->critterPet;
			u_caster->critterPet = NULL;
			return;
		}
		// this is a different critter, so we will dismiss our current critter and then go on to summon the new one
		else
		{
			u_caster->critterPet->RemoveFromWorld(false,true);
			delete u_caster->critterPet;
			u_caster->critterPet = NULL;
		}
	}

	if(!SummonCritterID) return;

	CreatureInfo * ci = CreatureNameStorage.LookupEntry(SummonCritterID);
	CreatureProto * cp = CreatureProtoStorage.LookupEntry(SummonCritterID);

	if(!ci || !cp) return;

	Creature * pCreature = u_caster->GetMapMgr()->CreateCreature(SummonCritterID);
	pCreature->SetInstanceID(u_caster->GetMapMgr()->GetInstanceID());
	pCreature->Load(cp, m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ(), m_caster->GetOrientation());
	pCreature->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, u_caster->GetUInt32Value( UNIT_FIELD_FACTIONTEMPLATE ));
	pCreature->_setFaction();
	pCreature->SetFFA( u_caster->GetFFA() );
	pCreature->SetUInt32Value(UNIT_FIELD_LEVEL, 1);
	pCreature->GetAIInterface()->Init(pCreature,AITYPE_PET,MOVEMENTTYPE_NONE,u_caster);
	pCreature->GetAIInterface()->SetUnitToFollow(u_caster);
	pCreature->GetAIInterface()->SetUnitToFollowAngle(float(-(M_PI/2)));
	pCreature->GetAIInterface()->SetFollowDistance(3.0f);
	pCreature->GetAIInterface()->disable_melee = true;
	pCreature->bInvincible = true;
	pCreature->PushToWorld(u_caster->GetMapMgr());
	u_caster->critterPet = pCreature;
}

void Spell::SpellEffectKnockBack(uint32 i)
{
	if(playerTarget == NULL || !playerTarget->isAlive() || !m_caster)
		return;

	if (playerTarget->MechanicsDispels[MECHANIC_KNOCK_BACK] >0 )
	{
		MSG_S2C::stCCResult ccmsg;
		ccmsg.result = MSG_S2C::stCCResult::_IMMUNE;
		ccmsg.effect = MSG_S2C::stCCResult::_KNOCK_BACK;
		ccmsg.spellid = m_spellInfo->Id;
		ccmsg.casterid = m_caster->GetGUID();
		ccmsg.victimid = playerTarget->GetGUID();
		playerTarget->GetSession()->SendPacket( ccmsg );

		if( p_caster )
		{
			p_caster->GetSession()->SendPacket( ccmsg );
		}
		return;
	}

	float time = 0.f;

	LocationVector lv;
	if( m_targets.m_targetMask & TARGET_FLAG_DEST_LOCATION )
	{
		lv.x = m_targets.m_destX;
		lv.y = m_targets.m_destY;
		lv.z = m_targets.m_destZ;
	}
	else
		lv = m_caster->GetPosition();

	playerTarget->KnockBack( lv, m_spellInfo->Id, (uint32)damage, (uint32)m_caster->GetGUID() );
}

void Spell::SpellEffectDisenchant(uint32 i)
{
	Player* caster = static_cast< Player* >( m_caster );
	Item* it = caster->GetItemInterface()->GetItemByGUID(m_targets.m_itemTarget);
	if( it == NULL )
	{
		SendCastResult(SPELL_FAILED_CANT_BE_DISENCHANTED);
		return;
	}

	//Check for skill first, we can increase it upto 75 
	uint32 skill=caster->_GetSkillLineCurrent( SKILL_ENCHANTING );
	if(skill < 75)//can up skill
	{
		if(Rand(float(100-skill*100.0/75.0)))
		{
			caster->_AdvanceSkillLine(SKILL_ENCHANTING, float2int32( 1.0f * sWorld.getRate(RATE_SKILLRATE)));
		}
	}
	//Fill disenchanting loot
	caster->SetLootGUID(it->GetGUID());
	if(!it->loot)
	{
		it->loot = new Loot;
		lootmgr.FillDisenchantingLoot(it->loot, it->GetEntry());
	}
	if ( it->loot->items.size() > 0 )
	{
		MyLog::log->debug("SpellEffect : Succesfully disenchanted item %d", uint32(itemTarget->GetEntry()));
		p_caster->SendLoot( itemTarget->GetGUID(), LOOT_DISENCHANTING );
	} 
	else
	{
		MyLog::log->debug("SpellEffect : Disenchanting failed, item %d has no loot", uint32(itemTarget->GetEntry()));
		SendCastResult(SPELL_FAILED_CANT_BE_DISENCHANTED);
	}
	if(it==i_caster)
		i_caster=NULL;
}

void Spell::SpellEffectInebriate(uint32 i) // lets get drunk!
{
	if(!p_caster)
		return;

	// Drunkee!
	uint8 b2 = m_caster->GetByte(PLAYER_BYTES_3,1);
	b2 += damage;	// 10 beers will get you smassssshed!
  
	m_caster->SetByte(PLAYER_BYTES_3,1,b2>90?90:b2);
	sEventMgr.RemoveEvents(p_caster, EVENT_PLAYER_REDUCEDRUNK);
	sEventMgr.AddEvent(p_caster, &Player::EventReduceDrunk, false, EVENT_PLAYER_REDUCEDRUNK, 300000, 0,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
}

void Spell::SpellEffectFeedPet(uint32 i)  // Feed Pet
{
	// food flags and food level are checked in Spell::CanCast()
	if(!itemTarget || !p_caster)
		return;
	
	Pet *pPet = p_caster->GetSummon();
	if(!pPet)
		return;

	/**	Cast feed pet effect
	- effect is item level and pet level dependent, aura ticks are 35, 17, 8 (*1000) happiness
	- http://petopia.brashendeavors.net/html/articles/basics_feeding.shtml */
	int8 deltaLvl = pPet->getLevel() - itemTarget->GetProto()->ItemLevel;
	damage /= 1000; //damage of Feed pet spell is 35000
	if(deltaLvl > 10) damage = damage >> 1;//divide by 2
	if(deltaLvl > 20) damage = damage >> 1;
	damage *= 1000;

	SpellEntry *spellInfo = dbcSpell.LookupEntry(m_spellInfo->EffectTriggerSpell[i]);
	Spell *sp= new Spell((Object *)p_caster,spellInfo,true,NULL);
	sp->forced_basepoints[0] = damage - 1;
	SpellCastTargets tgt;
	tgt.m_unitTarget=pPet->GetGUID();
	sp->prepare(&tgt);

	if(itemTarget->GetUInt32Value(ITEM_FIELD_STACK_COUNT)>1)
	{
		itemTarget->ModUnsigned32Value(ITEM_FIELD_STACK_COUNT, -1);
		itemTarget->m_isDirty=true;
	}
	else
	{
		p_caster->GetItemInterface()->SafeFullRemoveItemByGuid(itemTarget->GetGUID());
		itemTarget=NULL;
	}
}

void Spell::SpellEffectReputation(uint32 i)
{
	if(!playerTarget)
		return;
}

void Spell::SpellEffectSummonObjectSlot(uint32 i)
{
	if(!u_caster || !u_caster->IsInWorld())
		return;

	GameObject *GoSummon = NULL;

	uint32 slot=m_spellInfo->Effect[i] - SPELL_EFFECT_SUMMON_OBJECT_SLOT1;
	GoSummon = u_caster->m_ObjectSlots[slot] ? u_caster->GetMapMgr()->GetGameObject(u_caster->m_ObjectSlots[slot]) : 0;
	u_caster->m_ObjectSlots[slot] = 0;
	
	if( GoSummon )
	{
		if(GoSummon->GetInstanceID() != u_caster->GetInstanceID())
			GoSummon->ExpireAndDelete();
		else
		{
			if( GoSummon->IsInWorld() )
				GoSummon->RemoveFromWorld(true);
			delete GoSummon;
		}
	}


	uint32 mapid = u_caster->GetMapId();
	float px = u_caster->GetPositionX();
	float py = u_caster->GetPositionY();
	float pz = u_caster->GetPositionZ();
	pz = CollideInterface.GetHeight( u_caster->GetMapMgr()->GetBaseMap(), px, py, pz + 2.f );
	float orient = m_caster->GetOrientation();

	// spawn a new one
	GoSummon = u_caster->GetMapMgr()->CreateGameObject(m_spellInfo->EffectMiscValue[i]);
   if( !GoSummon->CreateFromProto(m_spellInfo->EffectMiscValue[i], mapid, px, py, pz, orient ) )
   {
		delete GoSummon;
		return;
   }
	
	GoSummon->SetUInt32Value(GAMEOBJECT_LEVEL, u_caster->getLevel());
	GoSummon->SetUInt64Value(OBJECT_FIELD_CREATED_BY, m_caster->GetGUID()); 
	GoSummon->SetInstanceID(m_caster->GetInstanceID());

	if(GoSummon->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_TRAP)
	{
		GoSummon->invisible = true;
		GoSummon->invisibilityFlag = INVIS_FLAG_TRAP;
		GoSummon->charges = 1;
		GoSummon->checkrate = 1;
		sEventMgr.AddEvent(GoSummon, &GameObject::TrapSearchTarget, EVENT_GAMEOBJECT_TRAP_SEARCH_TARGET, 100, 0,0);
	}
	sEventMgr.AddEvent(GoSummon, &GameObject::ExpireAndDelete, EVENT_GAMEOBJECT_EXPIRE, GetDuration(), 1,0);
	GoSummon->SetSummoned(u_caster);
	GoSummon->PushToWorld(m_caster->GetMapMgr());

	u_caster->m_ObjectSlots[slot] = GoSummon->GetUIdFromGUID();
}

void Spell::SpellEffectDispelMechanic(uint32 i)
{
	if( !unitTarget || !unitTarget->isAlive() )
		return;
	/* this was already working before...
	uint32 sMisc = m_spellInfo->EffectMiscValue[i];

	for( uint32 x = 0 ; x<MAX_AURAS ; x++ )
	{
		if( unitTarget->m_auras[x] && !unitTarget->m_auras[x]->IsPositive())
		{
			if( unitTarget->m_auras[x]->GetSpellProto()->MechanicsType == sMisc )
				unitTarget->m_auras[x]->Remove();
		}
	}
	*/
	unitTarget->RemoveAllAurasByMechanic( m_spellInfo->EffectMiscValue[i], damage, true );
	if( playerTarget && m_spellInfo->NameHash == SPELL_HASH_DAZED && playerTarget->IsMounted() )
		playerTarget->RemoveAura(playerTarget->m_MountSpellId);
}

void Spell::SpellEffectSummonDeadPet(uint32 i)
{//this is pet resurrect
	if(!p_caster) 
		return;
	Pet *pPet = p_caster->GetSummon();
	if(pPet)
	{
		pPet->SetUInt32Value(UNIT_DYNAMIC_FLAGS, 0);
		pPet->SetUInt32Value(UNIT_FIELD_HEALTH, (uint32)(pPet->GetUInt32Value(UNIT_FIELD_MAXHEALTH) * 0.5));
		pPet->setDeathState(ALIVE);
		pPet->GetAIInterface()->HandleEvent(EVENT_FOLLOWOWNER, pPet, 0);
		sEventMgr.RemoveEvents(pPet, EVENT_PET_DELAYED_REMOVE);
	}
}

/* This effect has 2 functions
 * 1. It delete's all current totems from the player
 * 2. It returns a percentage of the mana back to the player
 *
 * Bur kick my ass if this is not safe:P
*/

void Spell::SpellEffectDestroyAllTotems(uint32 i)
{
	if(!p_caster || !p_caster->IsInWorld()) return;

	float RetreivedMana = 0.0f;
	for(uint32 x=0;x<4;x++)
	{
		// atm totems are considert creature's
		if(p_caster->m_TotemSlots[x])
		{
			uint32 SpellID = p_caster->m_TotemSlots[x]->GetUInt32Value(UNIT_CREATED_BY_SPELL);
			SpellEntry * sp = dbcSpell.LookupEntry(SpellID);
			if (!sp)
				continue;

			float pts = float(m_spellInfo->EffectBasePoints[i]+1) / 100.0f;
			RetreivedMana += float(sp->manaCost) * pts;

			p_caster->m_TotemSlots[x]->TotemExpire();
		}

		if(p_caster->m_ObjectSlots[x])
		{
			GameObject * obj = p_caster->GetMapMgr()->GetGameObject(p_caster->m_ObjectSlots[x]);
			if(obj)
			{
				obj->ExpireAndDelete();
			}
			p_caster->m_ObjectSlots[x] = 0;
		}
	}

	// get the current mana, get the max mana. Calc if we overflow
	SendHealManaSpellOnPlayer(m_caster, m_caster, (uint32)RetreivedMana, 0);
	RetreivedMana += float(m_caster->GetUInt32Value(UNIT_FIELD_POWER1));
	uint32 max = m_caster->GetUInt32Value(UNIT_FIELD_MAXPOWER1);
	if((uint32)RetreivedMana > max)
		RetreivedMana = (float)max;
	m_caster->SetUInt32Value(UNIT_FIELD_POWER1, (uint32)RetreivedMana);
}

void Spell::SpellEffectSummonDemon(uint32 i)
{
	if(!p_caster/* ||  p_caster->getClass() != WARLOCK */) //summoning a demon shouldn't be warlock only, see spells 25005, 24934, 24810 etc etc
		return;
	Pet *pPet = p_caster->GetSummon();
	if(pPet)
	{
		pPet->Dismiss(false);
	}

	CreatureInfo *ci = CreatureNameStorage.LookupEntry(m_spellInfo->EffectMiscValue[i]);
	if(ci)
	{

		pPet = objmgr.CreatePet();
		pPet->SetInstanceID(p_caster->GetInstanceID());
		pPet->CreateAsSummon(m_spellInfo->EffectMiscValue[i], ci, NULL, p_caster, m_spellInfo, 1, 300000);
	}
	//Create Enslave Aura if its inferno spell
	if(m_spellInfo->Id == 1122)
	{
		SpellEntry *spellInfo = dbcSpell.LookupEntry(11726);
		
		Spell *sp=new Spell((Object *)pPet,spellInfo,true,NULL);
		SpellCastTargets tgt;
		tgt.m_unitTarget=pPet->GetGUID();
		sp->prepare(&tgt);
	}
}

void Spell::SpellEffectResurrect(uint32 i) // Resurrect (Flat)
{
	if(!playerTarget)
	{
		if(!corpseTarget)
		{
			// unit resurrection handler
			if(unitTarget)
			{
				if(unitTarget->GetTypeId()==TYPEID_UNIT && unitTarget->IsPet() && unitTarget->isDead())
				{
					uint32 hlth = ((uint32)damage > unitTarget->GetUInt32Value(UNIT_FIELD_MAXHEALTH)) ? unitTarget->GetUInt32Value(UNIT_FIELD_MAXHEALTH) : (uint32)damage;
					uint32 mana = ((uint32)damage > unitTarget->GetUInt32Value(UNIT_FIELD_MAXPOWER1)) ? unitTarget->GetUInt32Value(UNIT_FIELD_MAXPOWER1) : (uint32)damage;

					if(!unitTarget->IsPet())
					{
						sEventMgr.RemoveEvents(unitTarget, EVENT_CREATURE_REMOVE_CORPSE);
					}
					else
					{
						sEventMgr.RemoveEvents(unitTarget, EVENT_PET_DELAYED_REMOVE);
						sEventMgr.RemoveEvents(unitTarget, EVENT_CREATURE_REMOVE_CORPSE);
					}
					unitTarget->SetUInt32Value(UNIT_FIELD_HEALTH, hlth);
					unitTarget->SetUInt32Value(UNIT_FIELD_POWER1, mana);
					unitTarget->SetUInt32Value(UNIT_DYNAMIC_FLAGS, 0);
					unitTarget->setDeathState(ALIVE);
					((Creature*)unitTarget)->Tagged=false;
					((Creature*)unitTarget)->TaggerGuid=false;
					((Creature*)unitTarget)->loot.gold=0;
					((Creature*)unitTarget)->loot.looters.clear();
					((Creature*)unitTarget)->loot.items.clear();
				}
			}

			return;
		}
		playerTarget = objmgr.GetPlayer(corpseTarget->GetUInt32Value(CORPSE_FIELD_OWNER));
		if(!playerTarget) return;
	}

	if(playerTarget->isAlive() || !playerTarget->IsInWorld())
		return;

	uint32 health = m_spellInfo->EffectBasePoints[i];
	uint32 mana = m_spellInfo->EffectMiscValue[i];
	
	playerTarget->resurrector = p_caster->GetLowGUID();
	playerTarget->m_resurrectHealth = health;
	playerTarget->m_resurrectMana = mana;
	playerTarget->m_resurrectMapId	= p_caster->GetMapId();
	playerTarget->m_resurrectPosX	= p_caster->GetPositionX();
	playerTarget->m_resurrectPosY	= p_caster->GetPositionY();
	playerTarget->m_resurrectPosZ	= p_caster->GetPositionZ();
	playerTarget->m_resurrectInstanceID = p_caster->GetInstanceID();

	SendResurrectRequest(playerTarget);   
	playerTarget->SetMovement(MOVE_UNROOT, 1);
}

void Spell::SpellEffectAttackMe(uint32 i)
{
	if(!unitTarget)
		return;
	if(!unitTarget->isAlive())
		return;

	unitTarget->GetAIInterface()->AttackReaction(u_caster,1,0);	
}

void Spell::SpellEffectSkinPlayerCorpse(uint32 i)
{
	Corpse * corpse = 0;
	if(!playerTarget)
	{
		// means we're "skinning" a corpse
		corpse = objmgr.GetCorpse((uint32)m_targets.m_unitTarget);  // hacky
	}
	else if(playerTarget->getDeathState() == CORPSE)	// repopped while we were casting 
	{
		corpse = objmgr.GetCorpse(playerTarget->GetLowGUID());
	}

	if(!m_caster->IsPlayer()) 
		return;
 
	if(playerTarget && !corpse)
	{
		// Set all the lootable stuff on the player. If he repops before we've looted, we'll set the flags
		// on corpse then :p

		playerTarget->bShouldHaveLootableOnCorpse = false;
		playerTarget->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_SKINNABLE);
		playerTarget->SetFlag(UNIT_DYNAMIC_FLAGS, U_DYN_FLAG_LOOTABLE);

		// Send the loot.
		p_caster->SendLoot(playerTarget->GetGUID(), LOOT_SKINNING);

		// Send a message to the died player, telling him he has to resurrect at the graveyard.
		// Send an empty corpse location too, :P
		
		MSG_S2C::stPlayer_Skinned MsgSkin;
		playerTarget->GetSession()->SendPacket( MsgSkin );
		MSG_S2C::stQuery_Corpse_Response MsgCorpse;
		playerTarget->GetSession()->SendPacket( MsgCorpse );

		// don't allow him to spawn a corpse
		playerTarget->bCorpseCreateable = false;

	}else if(corpse)
	{
		// find the corpses' owner
		Player * owner = objmgr.GetPlayer(corpse->GetUInt32Value(CORPSE_FIELD_OWNER));
		if(owner)
		{
			MSG_S2C::stPlayer_Skinned MsgSkin;
			playerTarget->GetSession()->SendPacket( MsgSkin );
			MSG_S2C::stQuery_Corpse_Response MsgCorpse;
			playerTarget->GetSession()->SendPacket( MsgCorpse );
		}

		if(corpse->GetUInt32Value(CORPSE_FIELD_DYNAMIC_FLAGS) != 1)
			corpse->SetUInt32Value(CORPSE_FIELD_DYNAMIC_FLAGS, 1); // sets it so you can loot the plyr
		
		// remove skinnable flag
		corpse->SetUInt32Value(CORPSE_FIELD_FLAGS, 5);

		// remove owner association
		corpse->SetUInt64Value(CORPSE_FIELD_OWNER, 0);
		corpse->SetCorpseState(CORPSE_STATE_BONES);
		corpse->DeleteFromDB();
		objmgr.CorpseAddEventDespawn(corpse);

		// send loot
		p_caster->SendLoot(corpse->GetGUID(), LOOT_SKINNING);
	}
}

void Spell::SpellEffectSkill(uint32 i)
{
	// Used by professions only
	// Effect should be renamed in RequireSkill

	if ( !p_caster || p_caster->_GetSkillLineMax(m_spellInfo->EffectMiscValue[i]) >= m_spellInfo->EffectBasePoints[i]/*uint32( damage * 75 )*/ )
		return;
	
	if ( p_caster->_HasSkillLine( m_spellInfo->EffectMiscValue[i]) )
		p_caster->_ModifySkillMaximum( m_spellInfo->EffectMiscValue[i], m_spellInfo->EffectBasePoints[i]/*uint32( damage * 75 )*/ );
	else
		p_caster->_AddSkillLine( m_spellInfo->EffectMiscValue[i], 1, m_spellInfo->EffectBasePoints[i]/*uint32( damage * 75 )*/ );
}

void Spell::SpellEffectApplyPetAura(uint32 i)
{
	SpellEffectApplyAura(i);
}

void Spell::SpellEffectAreaHeal(uint32 i)
{
	//create only 1 dyn object
	uint32 dur = 0;
	float r = GetRadius(i);

	DynamicObject * dynObj = m_caster->GetMapMgr()->CreateDynamicObject();

	if(g_caster && g_caster->m_summoner && !unitTarget)
	{
		Unit * caster = g_caster->m_summoner;
		dynObj->Create(caster, this, g_caster->GetPositionX(), g_caster->GetPositionY(), 
			g_caster->GetPositionZ(), dur, r);
		return;
	}

	switch(m_targets.m_targetMask)
	{		
	case TARGET_FLAG_SELF:
		{
			dynObj->Create(u_caster, this,	m_caster->GetPositionX(), 
				m_caster->GetPositionY(), m_caster->GetPositionZ(), dur,r);		 
		}break;
	case TARGET_FLAG_UNIT:
		{
			if(!unitTarget||!unitTarget->isAlive())
				break;
			dynObj->Create( u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
				dur, r);
		}break;
	case TARGET_FLAG_OBJECT:
		{
			if(!unitTarget)
				break;
			if(!unitTarget->isAlive())
				break;
			dynObj->Create(u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
				dur, r);
		}break;
	case TARGET_FLAG_SOURCE_LOCATION:
		{
			dynObj->SetInstanceID(m_caster->GetInstanceID());
			dynObj->Create(u_caster, this, m_targets.m_srcX,
				m_targets.m_srcY, m_targets.m_srcZ, dur,r);
		}break;
	case TARGET_FLAG_DEST_LOCATION:
		{
			dynObj->SetInstanceID(m_caster->GetInstanceID());
			dynObj->Create(u_caster?u_caster:g_caster->m_summoner, this,
				m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ,dur,r);
		}break;
	default:
		return;
	}

	dynObj->UpdateTargets_HealHP_NoAura_Area_Radiu(damage);
}
void Spell::SpellEffectAreaEnergy(uint32 i)
{
	//create only 1 dyn object
	uint32 dur = 0;
	float r = GetRadius(i);

	DynamicObject * dynObj = m_caster->GetMapMgr()->CreateDynamicObject();

	if(g_caster && g_caster->m_summoner && !unitTarget)
	{
		Unit * caster = g_caster->m_summoner;
		dynObj->Create(caster, this, g_caster->GetPositionX(), g_caster->GetPositionY(), 
			g_caster->GetPositionZ(), dur, r);
		return;
	}

	switch(m_targets.m_targetMask)
	{		
	case TARGET_FLAG_SELF:
		{
			dynObj->Create(u_caster, this,	m_caster->GetPositionX(), 
				m_caster->GetPositionY(), m_caster->GetPositionZ(), dur,r);		 
		}break;
	case TARGET_FLAG_UNIT:
		{
			if(!unitTarget||!unitTarget->isAlive())
				break;
			dynObj->Create( u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
				dur, r);
		}break;
	case TARGET_FLAG_OBJECT:
		{
			if(!unitTarget)
				break;
			if(!unitTarget->isAlive())
				break;
			dynObj->Create(u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
				dur, r);
		}break;
	case TARGET_FLAG_SOURCE_LOCATION:
		{
			dynObj->SetInstanceID(m_caster->GetInstanceID());
			dynObj->Create(u_caster, this, m_targets.m_srcX,
				m_targets.m_srcY, m_targets.m_srcZ, dur,r);
		}break;
	case TARGET_FLAG_DEST_LOCATION:
		{
			dynObj->SetInstanceID(m_caster->GetInstanceID());
			dynObj->Create(u_caster?u_caster:g_caster->m_summoner, this,
				m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ,dur,r);
		}break;
	default:
		return;
	}

	dynObj->UpdateTargets_HealMP_NoAura_Area_Radiu(damage);
}

void Spell::SpellEffectSetAnimal(uint32 i)
{
	p_caster->SetAnimal(m_spellInfo->EffectMiscValue[i]);
	if(p_caster->GetGroup())
	{
		sAnimalMgr.OnAniamlRelation(p_caster->GetGroup()->GetSubGroup(p_caster->GetSubGroup()));
	}
}

void Spell::SpellEffectSchoolDamageNearByTwo(uint32 i)
{
	//create only 1 dyn object
	uint32 dur = GetDuration();
	float r = GetRadius(i);

	FillAllTargetsInArea(i, m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ(), r);
	while( m_targetUnits[i].size() > 2 )
		m_targetUnits[i].pop_back();

	if(IsDamagingSpell(m_spellInfo))
	{
		p_caster->m_inarmedtimeleft = 20000;
		//else
		p_caster->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);
	}

	TargetsList& targetlist = m_targetUnits[i];

	for(int l = 0; l < targetlist.size(); l++)
	{
		float time = 0.0f;
		Unit* pTarget = p_caster->GetMapMgr()->GetUnit(targetlist[i]);
		if(pTarget)
		{
			if(m_spellInfo->speed > 0.01f)
				time = (u_caster->CalcDistance(pTarget) * 1000.0f) / m_spellInfo->speed;
			sEventMgr.AddEvent(u_caster,&Unit::EventStrikeWithAbility,targetlist[l],
				m_spellInfo, (int32)damage,(int32)0, (uint32)0, false, false, true, true, false,  EVENT_SPELL_DAMAGE_HIT, float2int32(time), 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
		}	
	}
}

void Spell::SpellEffectDamage2Health(uint32 i)
{
	if( u_caster->lastSpellDamage )
		u_caster->EventHealHPWithAbility(u_caster->GetGUID(), m_spellInfo, (uint32)( (float)u_caster->lastSpellDamage * (float)damage / 100.f ));
}

void Spell::SpellEffectUnSetAnimal(uint32 i)
{
	p_caster->SetAnimal(0);
	if(p_caster->GetGroup())
	{
		sAnimalMgr.OnAniamlRelation(p_caster->GetGroup()->GetSubGroup(p_caster->GetSubGroup()));
	}
}

void Spell::SpellEffectAreaBuf(uint32 i)
{
	//create only 1 dyn object
	uint32 dur = GetDuration();
	float r = GetRadius(i);

// 	DynamicObject * dynObj = m_caster->GetMapMgr()->CreateDynamicObject();
// 
// 	if(g_caster && g_caster->m_summoner && !unitTarget)
// 	{
// 		Unit * caster = g_caster->m_summoner;
// 		dynObj->Create(caster, this, g_caster->GetPositionX(), g_caster->GetPositionY(), 
// 			g_caster->GetPositionZ(), dur, r);
// 		return;
// 	}

	if( !m_targetUnits[i].size() )
	{
		switch(m_targets.m_targetMask)
		{		
		case TARGET_FLAG_SELF:
			{
	// 			dynObj->Create(u_caster, this,	m_caster->GetPositionX(), 
	// 				m_caster->GetPositionY(), m_caster->GetPositionZ(), dur,r);
				FillAllTargetsInArea(i, m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ(), r);
			}break;
		case TARGET_FLAG_UNIT:
			{
				if(!unitTarget||!unitTarget->isAlive())
					break;
	// 			dynObj->Create( u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
	// 				dur, r);
				FillAllTargetsInArea(i, m_caster->GetPositionX(), m_caster->GetPositionY(), m_caster->GetPositionZ(), r);
			}break;
		case TARGET_FLAG_OBJECT:
			{
				if(!unitTarget)
					break;
				if(!unitTarget->isAlive())
					break;
	// 			dynObj->Create(u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
	// 				dur, r);
				FillAllTargetsInArea(i, u_caster->GetPositionX(), u_caster->GetPositionY(), u_caster->GetPositionZ(), r);
			}break;
		case TARGET_FLAG_SOURCE_LOCATION:
			{
	// 			dynObj->SetInstanceID(m_caster->GetInstanceID());
	// 			dynObj->Create(u_caster, this, m_targets.m_srcX,
	// 				m_targets.m_srcY, m_targets.m_srcZ, dur,r);
				FillAllTargetsInArea(i, m_targets.m_srcX,m_targets.m_srcY, m_targets.m_srcZ, r);
			}break;
		case TARGET_FLAG_DEST_LOCATION:
			{
	// 			dynObj->SetInstanceID(m_caster->GetInstanceID());
	// 			dynObj->Create(u_caster?u_caster:g_caster->m_summoner, this,
	// 				m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ,dur,r);
				FillAllTargetsInArea(i, m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ, r);
			}break;
		default:
			return;
		}
	}

	if(IsDamagingSpell(m_spellInfo))
	{
		if( p_caster )
		{
			p_caster->m_inarmedtimeleft = 20000;
			//else
			p_caster->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);
		}
	}

	//check if we already have stronger aura
	Aura *pAura;

	for(TargetsList::iterator iter=m_targetUnits[i].begin();iter!=m_targetUnits[i].end();iter++)
	{
		Unit* target = m_caster->GetMapMgr()->GetUnit(*iter);

		if(target)
		{
			std::map<uint32,Aura*>::iterator itr=target->tmpAura.find(m_spellInfo->Id);
			//if we do not make a check to see if the aura owner is the same as the caster then we will stack the 2 auras and they will not be visible client sided
			if(itr==target->tmpAura.end())
			{
				//buf优先级
				{
					if(m_spellInfo->EffectApplyAuraName[i] >= TOTAL_SPELL_AURAS)
					{
						MyLog::log->error("find a auraname which is bigger than %u", TOTAL_SPELL_AURAS);
						return;
					}
					uint32 oldspellid = target->m_spellAuras[m_spellInfo->EffectApplyAuraName[i]];
					if(oldspellid)
					{
						SpellEntry* pOldSpell = dbcSpell.LookupEntry(oldspellid);
						if(pOldSpell)
						{
							if(pOldSpell->SpellGroupType == m_spellInfo->SpellGroupType && pOldSpell->spellLevel > m_spellInfo->spellLevel)
							{
								return;
							}
						}
					}
				}
				uint32 Duration=GetDuration();

				// Handle diminishing returns, if it should be resisted, it'll make duration 0 here.
				if(!(m_spellInfo->Attributes & ATTRIBUTES_PASSIVE)) // Passive
					::ApplyDiminishingReturnTimer(&Duration, target, m_spellInfo);

				if(!Duration)
				{
					//maybe add some resist messege to client here ?
					return;
				}

				if( p_caster )
					pAura=new Aura(m_spellInfo, Duration, p_caster, target);
				else if( g_caster )
					pAura=new Aura(m_spellInfo, Duration, g_caster, target);
				else if( i_caster )
					pAura=new Aura(m_spellInfo, Duration, i_caster, target);
				else if( u_caster )
					pAura=new Aura(m_spellInfo, Duration, u_caster, target);

				pAura->pSpellId = m_spellInfo->Id; //this is required for triggered spells

				target->tmpAura[m_spellInfo->Id] = pAura;		
				target->m_spellAuras[m_spellInfo->EffectApplyAuraName[i]] = m_spellInfo->Id;
				target->m_spellAurasEffectPtr[m_spellInfo->EffectApplyAuraName[i]] = pAura;
			}
			else
			{
				pAura=itr->second;
			} 
			pAura->AddMod(m_spellInfo->EffectApplyAuraName[i], damage, m_spellInfo->EffectMiscValue[i], i);

			if( !pAura->IsValid() )
			{
				MyLog::log->error( "invalid aura! spell id: %d", m_spellInfo->Id );
				target->tmpAura.erase(itr);
				return;
			}
			if( pAura->GetSpellProto()->procCharges>0)
			{
				Aura *aur=NULL;
				for(int i=0;i<pAura->GetSpellProto()->procCharges-1;i++)
				{
					aur = new Aura(pAura->GetSpellProto(),pAura->GetDuration(),pAura->GetCaster(),pAura->GetTarget());
					target->AddAura(aur);
					aur=NULL;
				}
				if(!(pAura->GetSpellProto()->procFlags & PROC_REMOVEONUSE))
				{
					SpellCharge charge;
					charge.count=pAura->GetSpellProto()->procCharges;
					charge.spellId=pAura->GetSpellId();
					charge.ProcFlag=pAura->GetSpellProto()->procFlags;
					charge.lastproc = 0;
					target->m_chargeSpells.insert(make_pair(pAura->GetSpellId(),charge));
				}
			}
			target->AddAura(pAura); // the real spell is added last so the modifier is removed last
			target->tmpAura.erase(m_spellInfo->Id);
		}
	}
	
	//dynObj->UpdateTargets_Buf_Area_Radiu(i, damage);
}

void Spell::SpellEffectAreaApplyEffect( uint32 i )
{
	uint32 effect = (uint32)m_spellInfo->EffectBasePoints[i];

	if( effect > 2 )
	{
		MyLog::log->error( "fatal error - Spell::SpellEffectAreaApplyEffect, effect : %u", effect );
		return;
	}
	m_effects_hide[effect] = true;

	bool friendly = (bool)m_spellInfo->EffectMiscValue[i];

	uint32 dur = 0;
	float r = GetRadius(i);

	DynamicObject* dynamic_object = m_caster->GetMapMgr()->CreateDynamicObject();

	if(g_caster && g_caster->m_summoner && !unitTarget)
	{
		Unit * caster = g_caster->m_summoner;
		dynamic_object->Create(caster, this, g_caster->GetPositionX(), g_caster->GetPositionY(), 
			g_caster->GetPositionZ(), dur, r);
		dynamic_object->g_caster = g_caster;

		dynamic_object->UpdateTargets_Apply_Effect( effect, friendly );
		return;
	}

	switch(m_targets.m_targetMask)
	{		
	case TARGET_FLAG_SELF:
		{
			dynamic_object->Create(u_caster, this,	m_caster->GetPositionX(), 
				m_caster->GetPositionY(), m_caster->GetPositionZ(), dur,r);
		}break;
	case TARGET_FLAG_UNIT:
		{
			if(!unitTarget||!unitTarget->isAlive())
				break;
			dynamic_object->Create( u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
				dur, r);
		}break;
	case TARGET_FLAG_OBJECT:
		{
			if(!unitTarget)
				break;
			if(!unitTarget->isAlive())
				break;
			dynamic_object->Create(u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
				dur, r);
		}break;
	case TARGET_FLAG_SOURCE_LOCATION:
		{
			dynamic_object->SetInstanceID(m_caster->GetInstanceID());
			dynamic_object->Create(u_caster, this, m_targets.m_srcX,
				m_targets.m_srcY, m_targets.m_srcZ, dur,r);
		}break;
	case TARGET_FLAG_DEST_LOCATION:
		{
			dynamic_object->SetInstanceID(m_caster->GetInstanceID());
			dynamic_object->Create(u_caster?u_caster:g_caster->m_summoner, this,
				m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ,dur,r);
		}break;
	default:
		return;
	}

	if(p_caster && p_caster->m_bTriggerXP)
	{
		damage += (21/4)*p_caster->getLevel()*(5/4)*12.5;
		p_caster->RemoveAura(XP_TRIGGER_SPELL);
	}

	if(p_caster)
	{
		//if(p_caster->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED))
		p_caster->m_inarmedtimeleft = 20000;
		//else
		p_caster->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);

	}

	float time = 0.f;
	if( m_spellInfo->speed > 0.01f )
		if( u_caster )
			time = (u_caster->CalcDistance(dynamic_object) * 1000.0f) / m_spellInfo->speed;
		else if( g_caster )
			time = (g_caster->CalcDistance(dynamic_object) * 1000.0f) / m_spellInfo->speed;

	if( time > 10.f )
		sEventMgr.AddEvent( dynamic_object, &DynamicObject::UpdateTargets_Apply_Effect, effect, friendly, EVENT_DYNAMICOBJECT_CALL_FOR_SPEED, (uint32)time, 1, 0 );
	else
		dynamic_object->UpdateTargets_Apply_Effect(effect, friendly);
}

void Spell::SpellEffectAreaSchoolDamage(uint32 i)
{
	//create only 1 dyn object
	uint32 dur = 0;
	float r = GetRadius(i);

	DynamicObject* dynamic_object = m_caster->GetMapMgr()->CreateDynamicObject();

	if(g_caster && g_caster->m_summoner && !unitTarget)
	{
		Unit * caster = g_caster->m_summoner;
		dynamic_object->Create(caster, this, g_caster->GetPositionX(), g_caster->GetPositionY(), 
			g_caster->GetPositionZ(), dur, r);
		dynamic_object->g_caster = g_caster;

		dynamic_object->UpdateTargets_Damage_NoAura_Area_Radiu( damage );
		return;
	}
	
	switch(m_targets.m_targetMask)
	{		
	case TARGET_FLAG_SELF:
		{
			dynamic_object->Create(u_caster, this,	m_caster->GetPositionX(), 
				m_caster->GetPositionY(), m_caster->GetPositionZ(), dur,r);
		}break;
	case TARGET_FLAG_UNIT:
		{
			if(!unitTarget||!unitTarget->isAlive())
				break;
			dynamic_object->Create( u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
				dur, r);
		}break;
	case TARGET_FLAG_OBJECT:
		{
			if(!unitTarget)
				break;
			if(!unitTarget->isAlive())
				break;
			dynamic_object->Create(u_caster, this, unitTarget->GetPositionX(), unitTarget->GetPositionY(), unitTarget->GetPositionZ(),
				dur, r);
		}break;
	case TARGET_FLAG_SOURCE_LOCATION:
		{
			dynamic_object->SetInstanceID(m_caster->GetInstanceID());
			dynamic_object->Create(u_caster, this, m_targets.m_srcX,
				m_targets.m_srcY, m_targets.m_srcZ, dur,r);
		}break;
	case TARGET_FLAG_DEST_LOCATION:
		{
			dynamic_object->SetInstanceID(m_caster->GetInstanceID());
			dynamic_object->Create(u_caster?u_caster:g_caster->m_summoner, this,
				m_targets.m_destX, m_targets.m_destY, m_targets.m_destZ,dur,r);
		}break;
	default:
		return;
	}

	if(p_caster && p_caster->m_bTriggerXP)
	{
		damage += (21/4)*p_caster->getLevel()*(5/4)*12.5;
		p_caster->RemoveAura(XP_TRIGGER_SPELL);
	}

	if(p_caster)
	{
		//if(p_caster->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED))
		p_caster->m_inarmedtimeleft = 20000;
		//else
		p_caster->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ARMED);

	}

	float time = 0.f;
	if( m_spellInfo->speed > 0.01f )
		if( u_caster )
			time = (u_caster->CalcDistance(dynamic_object) * 1000.0f) / m_spellInfo->speed;
		else if( g_caster )
			time = (g_caster->CalcDistance(dynamic_object) * 1000.0f) / m_spellInfo->speed;

	if( time > 10.f )
		sEventMgr.AddEvent( dynamic_object, &DynamicObject::UpdateTargets_Damage_NoAura_Area_Radiu, (uint32)damage, EVENT_DYNAMICOBJECT_CALL_FOR_SPEED, (uint32)time, 1, 0 );
	else
		dynamic_object->UpdateTargets_Damage_NoAura_Area_Radiu(damage);
}

void Spell::SpellEffectDummyMelee( uint32 i ) // Normalized Weapon damage +
{
	if( unitTarget == NULL || u_caster == NULL )
		return;

	// rogue ambush etc
	for (uint32 x =0;x<3;x++)
		if(m_spellInfo->Effect[x] == SPELL_EFFECT_WEAPON_PERCENT_DAMAGE)
		{
			add_damage = damage * (m_spellInfo->EffectBasePoints[x]+1) /100;
			return;
		}

	uint32 pct_dmg_mod = 100;

	uint32 _type;
	if( GetType() == SPELL_DMG_TYPE_RANGED )
		_type = RANGED;
	else
	{
		if (m_spellInfo->Flags4 & 0x1000000)
			_type =  OFFHAND;
		else
			_type = MELEE;
	}
	u_caster->Strike( unitTarget, _type, m_spellInfo, damage, pct_dmg_mod, 0, false, false, true, true, true );
}

void Spell::SpellEffectSpellSteal( uint32 i )
{
	if (!unitTarget || !u_caster || !unitTarget->isAlive())
		return;

		Aura *aur;
	uint32 start,end;
	int32 spells_to_steal = damage;
	if(isAttackable(u_caster,unitTarget))
	{
		start=0;
		end=MAX_POSITIVE_AURAS;
	}
	else
		return;
	
	MSG_S2C::stSpell_Dispel_Log Msg;

	for(uint32 x=start;x<end;x++)
	if(unitTarget->m_auras[x])
	{
		aur = unitTarget->m_auras[x];
		if(aur->GetSpellId() != 15007 && !aur->IsPassive() && aur->IsPositive()) //Nothing can dispel resurrection sickness
		{
			if(aur->GetSpellProto()->DispelType == DISPEL_MAGIC)
			{
				Msg.caster_guid = m_caster->GetNewGUID();
				Msg.unit_guid = unitTarget->GetNewGUID();
				Msg.dispel_type = (uint32)1;
				Msg.spell_id = aur->GetSpellId();
				m_caster->SendMessageToSet(Msg,true);
				Aura *aura = new Aura(aur->GetSpellProto(), (aur->GetDuration()>120000) ? 120000 : aur->GetDuration(), u_caster, u_caster);
				u_caster->AddAura(aura);
				unitTarget->RemoveAura(aur);
				if( --spells_to_steal <= 0 )
					break; //exit loop now
			}			
		}
	}   
}

void Spell::SpellEffectProspecting(uint32 i)
{
	if(!p_caster) return;

	if(!itemTarget) // this should never happen
	{
		SendCastResult(SPELL_FAILED_CANT_BE_PROSPECTED);
		return;
	}

	//Fill Prospecting loot
	p_caster->SetLootGUID(itemTarget->GetGUID());
	if( !itemTarget->loot )
		{
			itemTarget->loot = new Loot;
			lootmgr.FillProspectingLoot( itemTarget->loot , itemTarget->GetEntry());
		}

	if ( itemTarget->loot->items.size() > 0 )
	{
		MyLog::log->debug("SpellEffect : Succesfully prospected item %d", uint32(itemTarget->GetEntry()));
		p_caster->SendLoot( itemTarget->GetGUID(), LOOT_PROSPECTING );
	} 
	else // this should never happen either
	{
		MyLog::log->debug("SpellEffect : Prospecting failed, item %d has no loot", uint32(itemTarget->GetEntry()));
		SendCastResult(SPELL_FAILED_CANT_BE_PROSPECTED);
	}
}

void Spell::SpellEffectResurrectNew(uint32 i)
{
	//base p =hp,misc mana
	if(!playerTarget)
	{
		if(!corpseTarget)
		{
			// unit resurrection handler
			if(unitTarget)
			{
				if(unitTarget->GetTypeId()==TYPEID_UNIT && unitTarget->IsPet() && unitTarget->isDead())
				{
					uint32 hlth = ((uint32)damage > unitTarget->GetUInt32Value(UNIT_FIELD_MAXHEALTH)) ? unitTarget->GetUInt32Value(UNIT_FIELD_MAXHEALTH) : (uint32)damage;
					uint32 mana = ((uint32)damage > unitTarget->GetUInt32Value(UNIT_FIELD_MAXPOWER1)) ? unitTarget->GetUInt32Value(UNIT_FIELD_MAXPOWER1) : (uint32)damage;

					if(!unitTarget->IsPet())
					{
						sEventMgr.RemoveEvents(unitTarget, EVENT_CREATURE_REMOVE_CORPSE);
					}
					else
					{
						sEventMgr.RemoveEvents(unitTarget, EVENT_PET_DELAYED_REMOVE);
						sEventMgr.RemoveEvents(unitTarget, EVENT_CREATURE_REMOVE_CORPSE);
					}
					unitTarget->SetUInt32Value(UNIT_FIELD_HEALTH, hlth);
					unitTarget->SetUInt32Value(UNIT_FIELD_POWER1, mana);
					unitTarget->SetUInt32Value(UNIT_DYNAMIC_FLAGS, 0);
					unitTarget->setDeathState(ALIVE);
					((Creature*)unitTarget)->Tagged=false;
					((Creature*)unitTarget)->TaggerGuid=false;
					((Creature*)unitTarget)->loot.gold=0;
					((Creature*)unitTarget)->loot.looters.clear();
					((Creature*)unitTarget)->loot.items.clear();
				}
			}

			return;
		}
		playerTarget = objmgr.GetPlayer(corpseTarget->GetUInt32Value(CORPSE_FIELD_OWNER));
		if(!playerTarget) return;
	}

	if(playerTarget->isAlive() || !playerTarget->IsInWorld())
		return;
   //resurr
	playerTarget->resurrector = p_caster->GetLowGUID();
	playerTarget->m_resurrectHealth = damage;
	playerTarget->m_resurrectMana = m_spellInfo->EffectMiscValue[i];

	SendResurrectRequest(playerTarget);
}

void Spell::SpellEffectTranformItem(uint32 i)
{
	bool result;
	AddItemResult result2;

	if(!i_caster)
		return;
	uint32 itemid=m_spellInfo->EffectSpellGroupRelation[i];
	if(!itemid)
		return;

	//Save durability of the old item
	Player * owner=i_caster->GetOwner();
	uint32 dur= i_caster->GetUInt32Value(ITEM_FIELD_DURABILITY);
	//	int8 slot=owner->GetItemInterface()->GetInventorySlotByGuid(i_caster->GetGUID());
	//	uint32 invt=i_caster->GetProto()->InventoryType;

	   result  = owner->GetItemInterface()->SafeFullRemoveItemByGuid(i_caster->GetGUID());
	if(!result)
	{
		//something went wrong if this happen, item doesnt exist, so it wasnt destroyed.
		return;
	}

	i_caster=NULL;

	Item *it=objmgr.CreateItem(itemid,owner);
	it->SetDurability(dur);
	//additem
	
	   //additem
	result2 = owner->GetItemInterface()->AddItemToFreeSlot(it);
	if(!result2) //should never get here
	{ 
		owner->GetItemInterface()->BuildInventoryChangeError(NULL,NULL,INV_ERR_BAG_FULL);
		delete it;
	}
}

void Spell::SpellEffectEnvironmentalDamage(uint32 i)
{
	if(!playerTarget)
		return;

	if(unitTarget->SchoolImmunityList[m_spellInfo->School])
	{
		SendCastResult(SPELL_FAILED_IMMUNE);
		return;
	}
	//this is GO, not unit	
	///float dmg = CalculateDamage(u_caster, unitTarget, GetType(), 0, m_spellInfo, false, m_spellInfo->EffectBasePoints[i], false, false);
	m_caster->SpellNonMeleeDamageLog(unitTarget,m_spellInfo->Id, damage, true, m_curattackcnt);
  /*
	MSG_S2C::stDamage_Environmental_Log Msg;
	Msg.target_guid = unitTarget->GetGUID();
	Msg.type		= DAMAGE_FIRE;
	Msg.damage		= dmg;
	unitTarget->SendMessageToSet( Msg, true );*/
}

void Spell::SpellEffectDismissPet(uint32 i)
{
	// remove pet.. but don't delete so it can be called later
	if(!p_caster)
		return;

	Pet *pPet = p_caster->GetSummon();
	if(!pPet)
		return;
	pPet->Remove(true, true, true);
}

void Spell::SpellEffectEnchantHeldItem( uint32 i )
{
	if( playerTarget == NULL )
		return;

	Item * item = playerTarget->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_MAINHAND );
	if( item == NULL )
		return;

	uint32 Duration = 1800; // Needs to be found in dbc.. I guess?
	switch( m_spellInfo->NameHash )
	{
		case SPELL_HASH_WINDFURY_TOTEM_EFFECT: // Windfury Totem Effect
		{   
			Duration = 10;
		}
	}
	EnchantEntry * Enchantment = dbcEnchant.LookupEntry( m_spellInfo->EffectMiscValue[i] );
	
	if( Enchantment == NULL )
		return;

	item->RemoveEnchantment( 1 );
	item->AddEnchantment( Enchantment, Duration, false, true, false, 1 );
}

void Spell::SpellEffectAddHonor(uint32 i)
{
	if( playerTarget == NULL )
		return;

	HonorHandler::AddHonorPointsToPlayer( playerTarget, damage );
}

void Spell::SpellEffectSpawn(uint32 i)
{
	// this effect is mostly for custom teleporting
	switch(m_spellInfo->Id)
	{
	  case 10418: // Arugal spawn-in spell , teleports it to 3 locations randomly sneeking players (bastard ;P)   
	  { 
		if(!u_caster || u_caster->IsPlayer())
			return;
		 
		static float coord[3][3]= {{-108.9034f,2129.5678f,144.9210f},{-108.9034f,2155.5678f,155.678f},{-77.9034f,2155.5678f,155.678f}};
		
		int i = (int)(rand()%3);
		u_caster->GetAIInterface()->SendMoveToPacket(coord[i][0],coord[i][1],coord[i][2],0.0f,0,u_caster->GetAIInterface()->getMoveFlags());
	  }
	}
}

void Spell::SpellEffectApplyAura128(uint32 i)
{
	if(m_spellInfo->EffectApplyAuraName[i] != 0)
		SpellEffectApplyAura(i);
}

void Spell::SpellEffectChangeGuildLeader(uint32 i)
{
	if (p_caster && playerTarget&& p_caster->GetGuild()&& p_caster->GetGuild()
		&&playerTarget->GetGuild()&&p_caster->GetGuild()->GetGuildLeader() == p_caster->GetGUID())
	{
		p_caster->GetGuild()->ChangeGuildMaster(playerTarget->m_playerInfo, p_caster->GetSession());
		// 已经学会这个技能
		if(!playerTarget->HasSpell(m_spellInfo->Id))
		{
			playerTarget->addSpell(m_spellInfo->Id);
		}

		if (p_caster->HasSpell(m_spellInfo->Id))
		{
			p_caster->removeSpell(m_spellInfo->Id, false, false, 0);
		}
		// learn me!
	}
}

void Spell::SpellEffectTriggerSpellWithRandomValue(uint32 i)
{
	if( unitTarget == NULL )
		return;

	uint32 random = damage;
	if (RandomUInt( 100) < random)
	{

		SpellEntry* TriggeredSpell = dbcSpell.LookupEntry(m_spellInfo->EffectTriggerSpell[i]);
		if( TriggeredSpell == NULL )
			return;

		Spell*sp=new Spell(m_caster,dbcSpell.LookupEntry(TriggeredSpell->Id),true,NULL);

		SpellCastTargets tgt(unitTarget->GetGUID());
		sp->prepare(&tgt);
	}
}

void Spell::SpellEffectTriggerSpellWithValue(uint32 i)
{
	if( unitTarget == NULL )
		return;

	SpellEntry* TriggeredSpell = dbcSpell.LookupEntry(m_spellInfo->EffectTriggerSpell[i]);
	if( TriggeredSpell == NULL )
		return;

	Spell*sp=new Spell(m_caster,dbcSpell.LookupEntry(TriggeredSpell->Id),true,NULL);

	for(uint32 x=0;x<3;x++)
	{
		sp->forced_basepoints[x] = TriggeredSpell->EffectBasePoints[i];
	}

	SpellCastTargets tgt(unitTarget->GetGUID());
	sp->prepare(&tgt);
}

void Spell::SpellEffectSwapPosition(uint32 i)
{
	if( unitTarget == NULL || unitTarget == u_caster || !u_caster->IsPlayer() || !unitTarget->IsPlayer() || unitTarget->GetMapMgr() != u_caster->GetMapMgr() )
		return;

	LocationVector casterPosition = u_caster->GetPosition();
	LocationVector targetPosition = unitTarget->GetPosition();

	((Player*)u_caster)->SafeTeleport( u_caster->GetMapId(), u_caster->GetInstanceID(), targetPosition );
	((Player*)unitTarget)->SafeTeleport( u_caster->GetMapId(), u_caster->GetInstanceID(), casterPosition );
}

void Spell::SpellEffectGuildNoWar(uint32 i)
{
	if (p_caster)
	{
		Guild* pGuild = p_caster->GetGuild();
		if (pGuild->GetGuildLeader() == p_caster->GetLowGUID())
		{
			
			pGuild->BeginNoWar();
		}
	}
}

void Spell::SpellEffectItemGift( uint32 i )
{
	if( !playerTarget )
		return;

	playerTarget->GiveGift( 1, this->m_spellInfo->EffectMiscValue[i], damage, true, Player::GIFT_CATEGORY_GUILD_GIFT );
}


void Spell::SpellEffectStablePet(uint32 i)
{
	if (p_caster)
	{
		Pet *pPet = p_caster->GetSummon();
		if(pPet && pPet->GetUInt32Value(UNIT_CREATED_BY_SPELL) != 0) 
			return;

		PlayerPet *pet = p_caster->GetPlayerPet(p_caster->GetUnstabledPetNumber());
		if(!pet) return;
		pet->stablestate = STABLE_STATE_PASSIVE;
		p_caster->iActivePet = 0;
		pet->active = false;

		if(pPet) pPet->Remove(false, true, true);	// no safedelete needed

		MSG_S2C::stPet_Stable_Result Msg;
		Msg.result = uint8(0x8);  // success
		if (p_caster->GetSession())
		{
			p_caster->GetSession()->SendPacket(Msg);
		}
	}
}

void Spell::SpellEffectMobileBank( uint32 i )
{
	if( p_caster )
	{
		p_caster->GetSession()->SendBankerList( NULL );
	}
}

void Spell::SpellEffectMobileAH( uint32 i )
{
	if( p_caster )
	{
		p_caster->GetSession()->SendAuctionList( NULL );
	}
}

void Spell::SpellEffectMobileMailbox( uint32 i )
{
	if( p_caster )
	{
		MSG_S2C::stMail_List_Result MsgMailList;
		p_caster->m_mailBox->BuildMailboxListingPacket( &MsgMailList );
		MsgMailList.npc_guid = p_caster->GetGUID();
		p_caster->GetSession()->SendPacket(MsgMailList);
	}
}

void Spell::SpellEffectPrivateFairground( uint32 i )
{
	if( p_caster )
	{
		g_instancequeuemgr.JoinRequest( p_caster, 78, true, m_spellInfo->EffectBasePoints[i] );
	}
}

void Spell::SpellEffectWashItem( uint32 i )
{
	if(!itemTarget || !p_caster) return;

	itemTarget->WashEnchantment();
}
