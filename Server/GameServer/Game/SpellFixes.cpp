#include "StdAfx.h"

void CreateDummySpell(uint32 id)
{
	const char * name = "Dummy Trigger";
	SpellEntry * sp = new SpellEntry;
	memset(sp, 0, sizeof(SpellEntry));
	sp->Id = id;
	sp->Attributes = 384;
	sp->AttributesEx = 268435456;
	sp->Flags3 = 4;
	sp->CastingTimeIndex=1;
	sp->procChance=75;
	sp->rangeIndex=13;
	sp->EquippedItemClass=uint32(-1);
	sp->Effect[0]=3;
	sp->EffectImplicitTargetA[0]=25;
	sp->NameHash=crc32((const unsigned char*)name, (unsigned int)strlen(name));
	sp->dmg_multiplier[0]=1.0f;
	dbcSpell.SetEntry(id,sp);
	sWorld.dummyspells.push_back(sp);
}

void ApplyExtraDataFixes()
{
	SpellEntry * sp;
	// Spell 53 (Backstab Rank 1) is in behind from spell extra.
	sp = dbcSpell.LookupEntry(53);
	if( sp != NULL )
		sp->in_front_status = 2;
}

void Apply112SpellFixes()
{
	//SpellEntry * sp;
	//

	//// Spell 10610 Proc Chance (Windfury Totem Rank 3)
	//sp = dbcSpell.LookupEntry(10610);
	//if(sp != NULL)
	//	sp->procChance = 10;

}

void ApplyNormalFixes()
{

	//Updating spell.dbc--this is slow like hell due to we cant read string fields
	//dbc method will be changed in future
	Apply112SpellFixes();

	uint32 effect;
	uint32 All_Seal_Groups_Combined=0;
	// Relation Groups

	//uint32 group_relation_paladin_healing_light = 0;
	uint32 group_relation_paladin_sanctified_light = 0;
	uint32 group_relation_shaman_enhancing_totems = 0;
	uint32 group_relation_shaman_restorative_totems = 0;
	uint32 group_relation_shaman_totems = 0;
	uint32 group_relation_shaman_lightning = 0;
	uint32 group_relation_shaman_shock = 0;
	uint32 group_relation_shaman_mental_quickness = 0;
	uint32 group_relation_rogue_elusiveness = 0;
	uint32 group_relation_rogue_poisons = 0;
	uint32 group_relation_rogue_find_weakness = 0;
	uint32 group_relation_rogue_shadow_step = 0;
	uint32 group_relation_rogue_lethality = 0;

	map<uint32, uint32> talentSpells;
	map<uint32,uint32>::iterator talentSpellIterator;
	unsigned int i,j;
	StorageContainerIterator<TalentEntry> * itr = dbcTalent.MakeIterator();
	while(!itr->AtEnd())
	{
		TalentEntry* tal = itr->Get();
		for(j = 0; j < 5; ++j)
			if(tal->RankID[j] != 0)
				talentSpells.insert(make_pair(tal->RankID[j], tal->TalentTree));	
		if(!itr->Inc())
			break;
	}
	itr->Destruct();

	StorageContainerIterator<SpellEntry> * itrSpell = dbcSpell.MakeIterator();
	while(!itrSpell->AtEnd())
	{
		uint32 result = 0;

		uint32 rank = 0;
		uint32 type = 0;
		uint32 namehash = 0;

		// get spellentry
		SpellEntry * sp = itrSpell->Get();

//		if(sp->speed < 0.000001f)
//			sp->speed = 100.0f;
		/*
		if(IsDamagingSpell(sp))
			sp->in_front_status = 1;
			*/
		/*
		if(sp->SpellGroupType > 512)
		{
			sp->SpellGroupType = 512;
			MyLog::log->error("Spell [%u] has a grouptype value which is bigger than 512", sp->SpellGroupType);
		}
		*/
		// Description field
		char* desc = sp->Description;
		const char* ranktext = sp->Rank;
		if( sp->Name == NULL )
			sp->Name = "";
		const char* nametext = sp->Name;
		sp->self_cast_only = false;
		sp->apply_on_shapeshift_change = false;
		sp->always_apply = false;
		//sp->AuraInterruptFlags = AURA_INTERRUPT_NULL;
		// hash the name
		//!!!!!!! representing all strings on 32 bits is dangerous. There is a chance to get same hash for a lot of strings ;)
		//namehash = crc32((const unsigned char*)nametext, (unsigned int)strlen(nametext));
		//sp->NameHash   = namehash; //need these set before we start processing spells

		float fRadius1 = 0;
		float fRadius2 = 0;
		float fRadius3 = 0;
		SpellRadius* pRadius = dbcSpellRadius.LookupEntry(sp->EffectRadiusIndex[0]);
		if(pRadius)
			fRadius1 = pRadius->Radius;
		pRadius = dbcSpellRadius.LookupEntry(sp->EffectRadiusIndex[1]);
		if(pRadius)
			fRadius2 = pRadius->Radius;
		pRadius = dbcSpellRadius.LookupEntry(sp->EffectRadiusIndex[2]);
		if(pRadius)
			fRadius3 = pRadius->Radius;
		float radius=std::max(fRadius1,fRadius2);
		radius=std::max(fRadius3,radius);
		SpellRange* pRange = dbcSpellRange.LookupEntry(sp->rangeIndex);
		float fMaxRange = 0;
		if(pRange)
			fMaxRange = pRange->maxRange;
		radius=std::max(fMaxRange,radius);
		sp->base_range_or_radius_sqr = radius*radius;

		if (!sp->Attach3)
		{

			sp->Dspell_coef_override =  1.0f;                             //!!! CUSTOM, overrides any spell coefficient calculation and use this value in DD&DH

		}
		else
		{
			sp->Dspell_coef_override = float(sp->Attach3) / 100.0f;

		}

		if (!sp->Attach2)
		{
			sp->OTspell_coef_override = 1.0f;
		}
		else
		{
			sp->OTspell_coef_override = float(sp->Attach2) / 100.0f;
		}


		if (sp->Attach4)
		{
			sp->cone_width = float(sp->Attach4) / float(180 / M_PI);
		}
		
		if (sp->Attach5)
		{
			int temp = sp->Attach5 / 10000;
			sp->fixed_hotdotcoef = float(temp)/100;                                   //!!! CUSTOM, fixed DD-DH coefficient for some spells
			sp->fixed_dddhcoef = float(sp->Attach5 -(temp * 10000))/ 100;       
		}
                          //!!! CUSTOM, fixed HOT-DOT coefficient for some spells

		if( sp->maxLevel > 0 )
		{
			int n = 0;
			if (sp->Id == 20101)
			{
				n ++;
			}
			sp->canCastInback = ( sp->maxLevel & (1 << 16) ) > 0 ? 1 : 0;
			sp->isDebuf = (sp->maxLevel & (1 << 17) ) > 0 ? 1 : 0;
			sp->maxLevel = sp->maxLevel & 0xFF;
		}
		else
		{
			sp->canCastInback = false;
			sp->isDebuf = false;
			sp->maxLevel = 0;
		}
			
		//	sp->cone_width = sp->Attach4;

		// NEW SCHOOLS AS OF 2.4.0:
		/* (bitwise)
		SCHOOL_NORMAL = 1,
		SCHOOL_HOLY   = 2,
		SCHOOL_FIRE   = 4,
		SCHOOL_NATURE = 8,
		SCHOOL_FROST  = 16,
		SCHOOL_SHADOW = 32,
		SCHOOL_ARCANE = 64
		*/

		/*
		uint32 school = 0;
		#define SET_SCHOOL(x) if( school != 0 ) { printf("Spell %u HAS MULTIPLE SCHOOLS!!!! %u\n", sp->Id, sp->School); } else { school = x; }

		if(sp->School & 1)
		{
		SET_SCHOOL(SCHOOL_NORMAL);
		}
		else if(sp->School & 2)
		{
		SET_SCHOOL(SCHOOL_HOLY);
		}
		else if(sp->School & 4)
		{
		SET_SCHOOL(SCHOOL_FIRE);
		}
		else if(sp->School & 8)
		{
		SET_SCHOOL(SCHOOL_NATURE);
		}
		else if(sp->School & 16)
		{
		SET_SCHOOL(SCHOOL_FROST);
		}
		else if(sp->School & 32)
		{
		SET_SCHOOL(SCHOOL_SHADOW);
		}
		else if(sp->School & 64)
		{
		SET_SCHOOL(SCHOOL_ARCANE);
		}
		else
		{
		printf("UNKNOWN SCHOOL %u\n", sp->School);
		}
		*/

//#define SET_SCHOOL(x) sp->School = x
//		if(sp->School & 1)
//			SET_SCHOOL(SCHOOL_NORMAL);
		//else if(sp->School & 2)
		//	SET_SCHOOL(SCHOOL_HOLY);
		//else if(sp->School & 4)
		//	SET_SCHOOL(SCHOOL_FIRE);
		//else if(sp->School & 8)
		//	SET_SCHOOL(SCHOOL_NATURE);
		//else if(sp->School & 16)
		//	SET_SCHOOL(SCHOOL_FROST);
		//else if(sp->School & 32)
		//	SET_SCHOOL(SCHOOL_SHADOW);
		//else if(sp->School & 64)
		//	SET_SCHOOL(SCHOOL_ARCANE);
		//else
		//	printf("UNKNOWN SCHOOL %u\n", sp->School);
//#undef SET_SCHOOL

		/*
		AURASTATE_FLAG_DODGE_BLOCK			= 1,        //1
		AURASTATE_FLAG_HEALTH20             = 2,        //2
		AURASTATE_FLAG_BERSERK              = 4,        //3
		AURASTATE_FLAG_JUDGEMENT            = 16,       //5
		AURASTATE_FLAG_PARRY                = 64,       //7
		AURASTATE_FLAG_LASTKILLWITHHONOR    = 512,      //10
		AURASTATE_FLAG_CRITICAL             = 1024,     //11
		AURASTATE_FLAG_HEALTH35             = 4096,     //13
		AURASTATE_FLAG_IMMOLATE             = 8192,     //14
		AURASTATE_FLAG_REJUVENATE           = 16384,    //15 //where do i use this ?
		AURASTATE_FLAG_POISON               = 32768,    //16
		*/

		// correct caster aura state
	/*
		if( sp->CasterAuraState != 0 )
		{
			switch( sp->CasterAuraState )
			{
			case 1:
				sp->CasterAuraState = AURASTATE_FLAG_DODGE_BLOCK;
				break;

			case 2:
				sp->CasterAuraState = AURASTATE_FLAG_HEALTH20;
				break;

			case 3:
				sp->CasterAuraState = AURASTATE_FLAG_BERSERK;
				break;

			case 5:
				sp->CasterAuraState = AURASTATE_FLAG_JUDGEMENT;
				break;

			case 7:
				sp->CasterAuraState = AURASTATE_FLAG_PARRY;
				break;

			case 10:
				sp->CasterAuraState = AURASTATE_FLAG_LASTKILLWITHHONOR;
				break;

			case 11:
				sp->CasterAuraState = AURASTATE_FLAG_CRITICAL;
				break;

			case 13:
				sp->CasterAuraState = AURASTATE_FLAG_HEALTH35;
				break;

			case 14:
				sp->CasterAuraState = AURASTATE_FLAG_IMMOLATE;
				break;

			case 15:
				sp->CasterAuraState = AURASTATE_FLAG_REJUVENATE;
				break;

			case 16:
				sp->CasterAuraState = AURASTATE_FLAG_POISON;
				break;

			default:
				MyLog::log->error("AuraState : Spell %u (%s) has unknown caster aura state %u\n", sp->Id, sp->Name, sp->CasterAuraState);
				break;
			}
		}

		if( sp->TargetAuraState != 0 )
		{
			switch( sp->TargetAuraState )
			{
			case 1:
				sp->TargetAuraState = AURASTATE_FLAG_DODGE_BLOCK;
				break;

			case 2:
				sp->TargetAuraState = AURASTATE_FLAG_HEALTH20;
				break;

			case 3:
				sp->TargetAuraState = AURASTATE_FLAG_BERSERK;
				break;

			case 5:
				sp->TargetAuraState = AURASTATE_FLAG_JUDGEMENT;
				break;

			case 7:
				sp->TargetAuraState = AURASTATE_FLAG_PARRY;
				break;

			case 10:
				sp->TargetAuraState = AURASTATE_FLAG_LASTKILLWITHHONOR;
				break;

			case 11:
				sp->TargetAuraState = AURASTATE_FLAG_CRITICAL;
				break;

			case 13:
				sp->TargetAuraState = AURASTATE_FLAG_HEALTH35;
				break;

			case 14:
				sp->TargetAuraState = AURASTATE_FLAG_IMMOLATE;
				break;

			case 15:
				sp->TargetAuraState = AURASTATE_FLAG_REJUVENATE;
				break;

			case 16:
				sp->TargetAuraState = AURASTATE_FLAG_POISON;
				break;

			default:
				MyLog::log->error("AuraState : Spell %u (%s) has unknown target aura state %u\n", sp->Id, sp->Name, sp->TargetAuraState);
				break;
			}
		}

		// apply on shapeshift change
		if( sp->NameHash == SPELL_HASH_TRACK_HUMANOIDS )
			sp->apply_on_shapeshift_change = true;

		if( sp->NameHash == SPELL_HASH_BLOOD_FURY || sp->NameHash == SPELL_HASH_SHADOWSTEP )
			sp->always_apply = true;

		//there are some spells that change the "damage" value of 1 effect to another : devastate = bonus first then damage
		//this is a total bullshit so remove it when spell system supports effect overwriting
		for( uint32 col1_swap = 0; col1_swap < 2 ; col1_swap++ )
			for( uint32 col2_swap = col1_swap ; col2_swap < 3 ; col2_swap++ )
				if( sp->Effect[col1_swap] == SPELL_EFFECT_WEAPON_PERCENT_DAMAGE && sp->Effect[col2_swap] == SPELL_EFFECT_DUMMYMELEE )
				{
					uint32 temp;
					float ftemp;
					temp = sp->Effect[col1_swap];			sp->Effect[col1_swap] = sp->Effect[col2_swap] ;						sp->Effect[col2_swap] = temp;
					temp = sp->EffectDieSides[col1_swap];	sp->EffectDieSides[col1_swap] = sp->EffectDieSides[col2_swap] ;		sp->EffectDieSides[col2_swap] = temp;
					temp = sp->EffectBaseDice[col1_swap];	sp->EffectBaseDice[col1_swap] = sp->EffectBaseDice[col2_swap] ;		sp->EffectBaseDice[col2_swap] = temp;
					ftemp = sp->EffectDicePerLevel[col1_swap];			sp->EffectDicePerLevel[col1_swap] = sp->EffectDicePerLevel[col2_swap] ;				sp->EffectDicePerLevel[col2_swap] = ftemp;
					ftemp = sp->EffectRealPointsPerLevel[col1_swap];	sp->EffectRealPointsPerLevel[col1_swap] = sp->EffectRealPointsPerLevel[col2_swap] ;	sp->EffectRealPointsPerLevel[col2_swap] = ftemp;
					temp = sp->EffectBasePoints[col1_swap];	sp->EffectBasePoints[col1_swap] = sp->EffectBasePoints[col2_swap] ;	sp->EffectBasePoints[col2_swap] = temp;
					temp = sp->EffectMechanic[col1_swap];	sp->EffectMechanic[col1_swap] = sp->EffectMechanic[col2_swap] ;	sp->EffectMechanic[col2_swap] = temp;
					temp = sp->EffectImplicitTargetA[col1_swap];	sp->EffectImplicitTargetA[col1_swap] = sp->EffectImplicitTargetA[col2_swap] ;	sp->EffectImplicitTargetA[col2_swap] = temp;
					temp = sp->EffectImplicitTargetB[col1_swap];	sp->EffectImplicitTargetB[col1_swap] = sp->EffectImplicitTargetB[col2_swap] ;	sp->EffectImplicitTargetB[col2_swap] = temp;
					temp = sp->EffectRadiusIndex[col1_swap];	sp->EffectRadiusIndex[col1_swap] = sp->EffectRadiusIndex[col2_swap] ;	sp->EffectRadiusIndex[col2_swap] = temp;
					temp = sp->EffectApplyAuraName[col1_swap];	sp->EffectApplyAuraName[col1_swap] = sp->EffectApplyAuraName[col2_swap] ;	sp->EffectApplyAuraName[col2_swap] = temp;
					temp = sp->EffectAmplitude[col1_swap];		sp->EffectAmplitude[col1_swap] = sp->EffectAmplitude[col2_swap] ;	sp->EffectAmplitude[col2_swap] = temp;
					ftemp = sp->Effectunknown[col1_swap];		sp->Effectunknown[col1_swap] = sp->Effectunknown[col2_swap] ;	sp->Effectunknown[col2_swap] = ftemp;
					temp = sp->EffectChainTarget[col1_swap];	sp->EffectChainTarget[col1_swap] = sp->EffectChainTarget[col2_swap] ;	sp->EffectChainTarget[col2_swap] = temp;
					temp = sp->EffectSpellGroupRelation[col1_swap];	sp->EffectSpellGroupRelation[col1_swap] = sp->EffectSpellGroupRelation[col2_swap] ;	sp->EffectSpellGroupRelation[col2_swap] = temp;
					temp = sp->EffectMiscValue[col1_swap];		sp->EffectMiscValue[col1_swap] = sp->EffectMiscValue[col2_swap] ;	sp->EffectMiscValue[col2_swap] = temp;
					temp = sp->EffectTriggerSpell[col1_swap];	sp->EffectTriggerSpell[col1_swap] = sp->EffectTriggerSpell[col2_swap] ;	sp->EffectTriggerSpell[col2_swap] = temp;
					ftemp = sp->EffectPointsPerComboPoint[col1_swap];	sp->EffectPointsPerComboPoint[col1_swap] = sp->EffectPointsPerComboPoint[col2_swap] ;	sp->EffectPointsPerComboPoint[col2_swap] = ftemp;
				}
					*/

				//for(uint32 b=0;b<3;++b)
				//{
				//	if(sp->EffectTriggerSpell[b] != 0 && dbcSpell.LookupEntry(sp->EffectTriggerSpell[b]) == NULL)
				//	{
				//		/* proc spell referencing non-existant spell. create a dummy spell for use w/ it. */
				//		CreateDummySpell(sp->EffectTriggerSpell[b]);
				//	}
				//	/** Load teaching spells (used for hunters when learning pets wild abilities) */
				//	if(sp->Effect[b]==SPELL_EFFECT_LEARN_SPELL && sp->EffectImplicitTargetA[b]==EFF_TARGET_PET)
				//	{
				//		map<uint32,uint32>::iterator itr = sWorld.TeachingSpellMap.find(sp->EffectTriggerSpell[b]);
				//		if(itr == sWorld.TeachingSpellMap.end())
				//			sWorld.TeachingSpellMap.insert(make_pair(sp->EffectTriggerSpell[b],sp->Id));
				//	}

				//	if( sp->Attributes & ATTRIBUTES_ONLY_OUTDOORS && sp->EffectApplyAuraName[b] == SPELL_AURA_MOUNTED )
				//	{
				//		sp->Attributes &= ~ATTRIBUTES_ONLY_OUTDOORS;
				//	}

				//	// fill in more here
				//	/*switch( sp->EffectImplicitTargetA[b] )
				//	{
				//	case 1:
				//	case 9:
				//	sp->self_cast_only = true;
				//	break;
				//	}

				//	// fill in more here too
				//	switch( sp->EffectImplicitTargetB[b] )
				//	{
				//	case 1:
				//	case 9:
				//	sp->self_cast_only = true;
				//	break;
				//	}*/
				//}

				//if(sp->self_cast_only && !(sp->Attributes&64))
				//printf("SPELL SELF CAST ONLY: %s %u\n", sp->Name, sp->Id);

				//if(!strcmp(sp->Name, "Hearthstone") || !strcmp(sp->Name, "Stuck") || !strcmp(sp->Name, "Astral Recall"))
				//	sp->self_cast_only = true;

				sp->proc_interval = 0;//trigger at each event
				sp->c_is_flags = 0;
				sp->spell_coef_flags = 0;
			//	sp->Dspell_coef_override = -1;
			//	sp->OTspell_coef_override = -1;
				sp->casttime_coef = 0;
			//	sp->fixed_dddhcoef = -1;
			//	sp->fixed_hotdotcoef = -1;

				talentSpellIterator = talentSpells.find(sp->Id);
//				if(talentSpellIterator == talentSpells.end())
//					sp->talent_tree = 0;
//				else
//					sp->talent_tree = talentSpellIterator->second;
//
//				// parse rank text
//				if( !sscanf( ranktext, "Rank %d", (unsigned int*)&rank) )
//					rank = 0;
//
//				//seal of light 
//				if( namehash == SPELL_HASH_SEAL_OF_LIGHT )			
//					sp->procChance = 45;	/* this will do */
//
//				//seal of command
//				else if( namehash == SPELL_HASH_SEAL_OF_COMMAND )		
//					sp->Spell_Dmg_Type = SPELL_DMG_TYPE_MAGIC;
//
//				//judgement of command
//				else if( namehash == SPELL_HASH_JUDGEMENT_OF_COMMAND )		
//					sp->Spell_Dmg_Type = SPELL_DMG_TYPE_MAGIC;
//
//				else if( namehash == SPELL_HASH_ARCANE_SHOT )		
//					sp->c_is_flags |= SPELL_FLAG_IS_NOT_USING_DMG_BONUS;
//
//				else if( namehash == SPELL_HASH_SERPENT_STING )		
//					sp->c_is_flags |= SPELL_FLAG_IS_NOT_USING_DMG_BONUS;
//
//				//Rogue: Posion time fix for 2.3
//				if( strstr( nametext, "Crippling Poison") && sp->Effect[0] == 54 ) //I, II
//					sp->EffectBasePoints[0] = 3599;
//				if( strstr( nametext, "Mind-numbing Poison") && sp->Effect[0] == 54 ) //I,II,III
//					sp->EffectBasePoints[0] = 3599;
//				if( strstr( nametext, "Instant Poison") && sp->Effect[0] == 54 ) //I,II,III,IV,V,VI,VII    
//					sp->EffectBasePoints[0] = 3599;
//				if( strstr( nametext, "Deadly Poison") && sp->Effect[0] == 54 ) //I,II,III,IV,V,VI,VII
//					sp->EffectBasePoints[0] = 3599;
//				if( strstr( nametext, "Wound Poison") && sp->Effect[0] == 54 ) //I,II,III,IV,V
//					sp->EffectBasePoints[0] = 3599;
//				if( strstr( nametext, "Anesthetic Poison") && sp->Effect[0] == 54 ) //I
//					sp->EffectBasePoints[0] = 3599;
//
//				if( strstr( nametext, "Sharpen Blade") && sp->Effect[0] == 54 ) //All BS stones
//					sp->EffectBasePoints[0] = 3599;
//
//				//these mostly do not mix so we can use else 
//				// look for seal, etc in name
//				if( strstr( nametext, "Seal"))
//				{
//					type |= SPELL_TYPE_SEAL;
//					All_Seal_Groups_Combined |= sp->SpellGroupType;
//				}
//				else if( strstr( nametext, "Blessing"))
//					type |= SPELL_TYPE_BLESSING;
//				else if( strstr( nametext, "Curse"))
//					type |= SPELL_TYPE_CURSE;
//				else if( strstr( nametext, "Aspect"))
//					type |= SPELL_TYPE_ASPECT;
//				else if( strstr( nametext, "Sting") || strstr( nametext, "sting"))
//					type |= SPELL_TYPE_STING;
//				// don't break armor items!
//				else if(strcmp(nametext, "Armor") && strstr( nametext, "Armor") || strstr( nametext, "Demon Skin"))
//					type |= SPELL_TYPE_ARMOR;
//				else if( strstr( nametext, "Aura"))
//					type |= SPELL_TYPE_AURA;
//				else if( strstr( nametext, "Track")==nametext)
//					type |= SPELL_TYPE_TRACK;
//				else if( namehash == SPELL_HASH_GIFT_OF_THE_WILD || namehash == SPELL_HASH_MARK_OF_THE_WILD )
//					type |= SPELL_TYPE_MARK_GIFT;
//				else if( namehash == SPELL_HASH_IMMOLATION_TRAP || namehash == SPELL_HASH_FREEZING_TRAP || namehash == SPELL_HASH_FROST_TRAP || namehash == SPELL_HASH_EXPLOSIVE_TRAP || namehash == SPELL_HASH_SNAKE_TRAP )
//					type |= SPELL_TYPE_HUNTER_TRAP;
//				else if( namehash == SPELL_HASH_ARCANE_INTELLECT || namehash == SPELL_HASH_ARCANE_BRILLIANCE )
//					type |= SPELL_TYPE_MAGE_INTEL;
//				else if( namehash == SPELL_HASH_AMPLIFY_MAGIC || namehash == SPELL_HASH_DAMPEN_MAGIC )
//					type |= SPELL_TYPE_MAGE_MAGI;
//				else if( namehash == SPELL_HASH_FIRE_WARD || namehash == SPELL_HASH_FROST_WARD )
//					type |= SPELL_TYPE_MAGE_WARDS;
//				else if( namehash == SPELL_HASH_SHADOW_PROTECTION || namehash == SPELL_HASH_PRAYER_OF_SHADOW_PROTECTION )
//					type |= SPELL_TYPE_PRIEST_SH_PPROT;
//				else if( namehash == SPELL_HASH_WATER_SHIELD || namehash == SPELL_HASH_EARTH_SHIELD || namehash == SPELL_HASH_LIGHTNING_SHIELD )
//					type |= SPELL_TYPE_SHIELD;
//				else if( namehash == SPELL_HASH_POWER_WORD__FORTITUDE || namehash == SPELL_HASH_PRAYER_OF_FORTITUDE )
//					type |= SPELL_TYPE_FORTITUDE;
//				else if( namehash == SPELL_HASH_DIVINE_SPIRIT || namehash == SPELL_HASH_PRAYER_OF_SPIRIT )
//					type |= SPELL_TYPE_SPIRIT;
//				//		else if( strstr( nametext, "Curse of Weakness") || strstr( nametext, "Curse of Agony") || strstr( nametext, "Curse of Recklessness") || strstr( nametext, "Curse of Tongues") || strstr( nametext, "Curse of the Elements") || strstr( nametext, "Curse of Idiocy") || strstr( nametext, "Curse of Shadow") || strstr( nametext, "Curse of Doom"))
//				//		else if(namehash==4129426293 || namehash==885131426 || namehash==626036062 || namehash==3551228837 || namehash==2784647472 || namehash==776142553 || namehash==3407058720 || namehash==202747424)
//				//		else if( strstr( nametext, "Curse of "))
//				//            type |= SPELL_TYPE_WARLOCK_CURSES;
//				else if( strstr( nametext, "Immolate") || strstr( nametext, "Conflagrate"))
//					type |= SPELL_TYPE_WARLOCK_IMMOLATE;
//				else if( strstr( nametext, "Amplify Magic") || strstr( nametext, "Dampen Magic"))
//					type |= SPELL_TYPE_MAGE_AMPL_DUMP;
//				else if( strstr( desc, "Battle Elixir"))
//					type |= SPELL_TYPE_ELIXIR_BATTLE;
//				else if( strstr( desc, "Guardian Elixir"))
//					type |= SPELL_TYPE_ELIXIR_GUARDIAN;
//				else if( strstr( desc, "Battle and Guardian elixir"))
//					type |= SPELL_TYPE_ELIXIR_FLASK;
//				else if( namehash == SPELL_HASH_HUNTER_S_MARK )		// hunter's mark
//					type |= SPELL_TYPE_HUNTER_MARK;
//				else if( namehash == SPELL_HASH_COMMANDING_SHOUT || namehash == SPELL_HASH_BATTLE_SHOUT )
//					type |= SPELL_TYPE_WARRIOR_SHOUT;
//				else if( strstr( desc, "Finishing move")==desc)
//					sp->c_is_flags |= SPELL_FLAG_IS_FINISHING_MOVE;
//				if( IsDamagingSpell( sp ) )
//					sp->c_is_flags |= SPELL_FLAG_IS_DAMAGING;
//				if( IsHealingSpell( sp ) )
//					sp->c_is_flags |= SPELL_FLAG_IS_HEALING;
//				if( IsTargetingStealthed( sp ) )
//					sp->c_is_flags |= SPELL_FLAG_IS_TARGETINGSTEALTHED;
//
//
//				//stupid spell ranking problem
//				if(sp->spellLevel==0)
//				{
//					uint32 new_level=0;
//					if( strstr( nametext, "Apprentice "))
//						new_level = 1;
//					else if( strstr( nametext, "Journeyman "))
//						new_level = 2;
//					else if( strstr( nametext, "Expert "))
//						new_level = 3;
//					else if( strstr( nametext, "Artisan "))
//						new_level = 4;
//					else if( strstr( nametext, "Master "))
//						new_level = 5;
//					if(new_level!=0)
//					{
//						uint32 teachspell=0;
//						if(sp->Effect[0]==SPELL_EFFECT_LEARN_SPELL)
//							teachspell = sp->EffectTriggerSpell[0];
//						else if(sp->Effect[1]==SPELL_EFFECT_LEARN_SPELL)
//							teachspell = sp->EffectTriggerSpell[1];
//						else if(sp->Effect[2]==SPELL_EFFECT_LEARN_SPELL)
//							teachspell = sp->EffectTriggerSpell[2];
//						if(teachspell)
//						{
//							SpellEntry *spellInfo;
//							spellInfo = dbcSpell.LookupEntry(teachspell);
//							spellInfo->spellLevel = new_level;
//							sp->spellLevel = new_level;
//						}
//					}
//				}
//
//				/*FILE * f = fopen("C:\\spells.txt", "a");
//				fprintf(f, "case 0x%08X:		// %s\n", namehash, nametext);
//				fclose(f);*/
//
//				// find diminishing status
//				sp->DiminishStatus = GetDiminishingGroup(namehash);
//				sp->buffIndexType=0;
//				switch(namehash)
//				{
//				case SPELL_HASH_HUNTER_S_MARK:		// Hunter's mark
//					sp->buffIndexType = SPELL_TYPE_INDEX_MARK;
//					break;
//
//				case SPELL_HASH_POLYMORPH:			// Polymorph
//				case SPELL_HASH_POLYMORPH__CHICKEN:	// Polymorph: Chicken
//				case SPELL_HASH_POLYMORPH__PIG:		// Polymorph: Pig
//				case SPELL_HASH_POLYMORPH__SHEEP:	// Polymorph: Sheep
//				case SPELL_HASH_POLYMORPH__TURTLE:	// Polymorph: Turtle
//					sp->buffIndexType = SPELL_TYPE_INDEX_POLYMORPH;
//					break;
//
//				case SPELL_HASH_FEAR:				// Fear
//					sp->buffIndexType = SPELL_TYPE_INDEX_FEAR;
//					break;
//
//				case SPELL_HASH_SAP:				// Sap
//					sp->buffIndexType = SPELL_TYPE_INDEX_SAP;
//					break;
//
//				case SPELL_HASH_SCARE_BEAST:		// Scare Beast
//					sp->buffIndexType = SPELL_TYPE_INDEX_SCARE_BEAST;
//					break;
//
//				case SPELL_HASH_HIBERNATE:			// Hibernate
//					sp->buffIndexType = SPELL_TYPE_INDEX_HIBERNATE;
//					break;
//
//					//		removed by Zack Earth shield stacks 10 times. Current code does not support it
//					//		case SPELL_HASH_EARTH_SHIELD:		// Earth Shield
//					//			sp->buffIndexType = SPELL_TYPE_INDEX_EARTH_SHIELD;
//					//			break;
//
//				case SPELL_HASH_CYCLONE:			// Cyclone
//					sp->buffIndexType = SPELL_TYPE_INDEX_CYCLONE;
//					break;
//
//				case SPELL_HASH_BANISH:				// Banish
//					sp->buffIndexType = SPELL_TYPE_INDEX_BANISH;
//					break;
//
//					//case SPELL_HASH_JUDGEMENT_OF_VENGEANCE:
//				case SPELL_HASH_JUDGEMENT_OF_THE_CRUSADER:
//				case SPELL_HASH_JUDGEMENT_OF_LIGHT:
//				case SPELL_HASH_JUDGEMENT_OF_WISDOM:
//				case SPELL_HASH_JUDGEMENT_OF_JUSTICE:
//					sp->buffIndexType = SPELL_TYPE_INDEX_JUDGEMENT;
//					break;
//				}
//
//				// HACK FIX: Break roots/fear on damage.. this needs to be fixed properly!
//				if(!(sp->AuraInterruptFlags & AURA_INTERRUPT_ON_ANY_DAMAGE_TAKEN))
//				{
//					for(uint32 z = 0; z < 3; ++z) {
//						//if(sp->EffectApplyAuraName[z] == SPELL_AURA_MOD_FEAR ||
//						//	sp->EffectApplyAuraName[z] == SPELL_AURA_MOD_ROOT)
//						//{
//						//	sp->AuraInterruptFlags |= AURA_INTERRUPT_ON_UNUSED2;
//						//	break;
//						//}
//
//						if( ( sp->Effect[z] == SPELL_EFFECT_SCHOOL_DAMAGE && sp->Spell_Dmg_Type == SPELL_DMG_TYPE_MELEE ) || sp->Effect[z] == SPELL_EFFECT_WEAPON_DAMAGE_NOSCHOOL || sp->Effect[z] == SPELL_EFFECT_WEAPON_DAMAGE || sp->Effect[z] == SPELL_EFFECT_WEAPON_PERCENT_DAMAGE || sp->Effect[z] == SPELL_EFFECT_DUMMYMELEE )
//							sp->is_melee_spell = true;
//						if( ( sp->Effect[z] == SPELL_EFFECT_SCHOOL_DAMAGE && sp->Spell_Dmg_Type == SPELL_DMG_TYPE_RANGED ) )
//						{
//							//Log.Notice( "SpellFixes" , "Ranged Spell: %u [%s]" , sp->Id , sp->Name );
//							sp->is_ranged_spell = true;
//						}
//					}
//				}
//
//				// set extra properties
//				sp->buffType   = type;
//				sp->RankNumber = rank;
//
//				uint32 pr=sp->procFlags;
//				for(uint32 y=0;y < 3; y++)
//				{
//					// get the effect number from the spell
//					effect = sp->Effect[y]; // spelleffect[0] = 64 // 2.0.1
//
//					//spell group
//					/*if(effect==SPELL_EFFECT_SUMMON_TOTEM_SLOT1||effect==SPELL_EFFECT_SUMMON_TOTEM_SLOT2||
//					effect==SPELL_EFFECT_SUMMON_TOTEM_SLOT3||effect==SPELL_EFFECT_SUMMON_TOTEM_SLOT4)
//					{
//
//					const char *p=desc;
//					while(p=strstr(p,"$"))
//					{
//					p++;
//					//got $  -> check if spell
//					if(*p>='0' && *p <='9')
//					{//woot this is spell id
//					uint32 tmp=atoi(p);
//					SpellEntry*s=sSpellStore.LookupEntryForced(tmp);
//					bool ch=false;
//					for(uint32 i=0;i<3;i++)
//					if(s->EffectTriggerSpell[i])
//					{
//					ch=true;
//					result=tmp;
//					break;
//					}
//					if(ch)break;
//					result=tmp;
//
//					}
//
//					}
//
//					}else*/
//					/*if(effect==SPELL_EFFECT_ENCHANT_ITEM)//add inventory type check
//					{
//					result=0;
//					//136--desc field
//					//dirty code
//					if( strstr( desc,"head"))
//					result|=(1<<INVTYPE_HEAD);
//					if( strstr( desc,"leg"))
//					result|=(1<<INVTYPE_LEGS);
//					if( strstr( desc,"neck"))
//					result|=(1<<INVTYPE_NECK);
//					if( strstr( desc,"shoulder"))
//					result|=(1<<INVTYPE_SHOULDERS);
//					if( strstr( desc,"body"))
//					result|=(1<<INVTYPE_BODY);
//					if( strstr( desc,"chest"))
//					result|=((1<<INVTYPE_CHEST)|(1<<INVTYPE_ROBE));
//					if( strstr( desc,"waist"))
//					result|=(1<<INVTYPE_WAIST);
//					if( strstr( desc,"foot")||strstr(desc,"feet")||strstr(desc,"boot"))
//					result|=(1<<INVTYPE_FEET);
//					if( strstr( desc,"wrist")||strstr(desc,"bracer"))
//					result|=(1<<INVTYPE_WRISTS);
//					if( strstr( desc,"hand")||strstr(desc,"glove"))
//					result|=(1<<INVTYPE_HANDS);
//					if( strstr( desc,"finger")||strstr(desc,"ring"))
//					result|=(1<<INVTYPE_FINGER);
//					if( strstr( desc,"trinket"))
//					result|=(1<<INVTYPE_TRINKET);
//					if( strstr( desc,"shield"))
//					result|=(1<<INVTYPE_SHIELD);
//					if( strstr( desc,"cloak"))
//					result|=(1<<INVTYPE_CLOAK);
//					if( strstr( desc,"robe"))
//					result|=(1<<INVTYPE_ROBE);
//					//if( strstr( desc,"two")||strstr(desc,"Two"))
//					//	result|=(1<<INVTYPE_2HWEAPON);<-handled in subclass
//					}
//					else*/
//					if(effect==SPELL_EFFECT_APPLY_AURA)
//					{
//						uint32 aura = sp->EffectApplyAuraName[y]; // 58+30+3 = 91
//						if( aura == SPELL_AURA_PROC_TRIGGER_SPELL ||
//							aura == SPELL_AURA_PROC_TRIGGER_DAMAGE
//							)//search for spellid in description
//						{
//							const char *p=desc;
//							while((p=strstr(p,"$")))
//							{
//								p++;
//								//got $  -> check if spell
//								if(*p>='0' && *p <='9')
//								{//woot this is spell id
//
//									result=atoi(p);
//								}					
//							}
//							pr=0;
//
//							uint32 len = (uint32)strlen(desc);
//							for(i = 0; i < len; ++i)
//								desc[i] = tolower(desc[i]);
//							//dirty code for procs, if any1 got any better idea-> u are welcome
//							//139944 --- some magic number, it will trigger on all hits etc
//							//for seems to be smth like custom check
//							if( strstr( desc,"your ranged criticals"))
//								pr|=PROC_ON_RANGED_CRIT_ATTACK;
//							if( strstr( desc,"chance on hit"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"takes damage"))
//								pr|=PROC_ON_ANY_DAMAGE_VICTIM;
//							if( strstr( desc,"attackers when hit"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"character strikes an enemy"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"strike you with a melee attack"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"target casts a spell"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc,"your harmful spells land"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc,"on spell critical hit"))
//								pr|=PROC_ON_SPELL_CRIT_HIT;
//							if( strstr( desc,"spell critical strikes"))
//								pr|=PROC_ON_SPELL_CRIT_HIT;
//							if( strstr( desc,"being able to resurrect"))
//								pr|=PROC_ON_DIE;
//							if( strstr( desc,"any damage caused"))
//								pr|=PROC_ON_ANY_DAMAGE_VICTIM;
//							if( strstr( desc,"the next melee attack against the caster"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"when successfully hit"))
//								pr|=PROC_ON_MELEE_ATTACK ;
//							if( strstr( desc,"an enemy on hit"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"when it hits"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"when successfully hit"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"on a successful hit"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"damage to attacker on hit"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"on a hit"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"strikes you with a melee attack"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"when caster takes damage"))
//								pr|=PROC_ON_ANY_DAMAGE_VICTIM;
//							if( strstr( desc,"when the caster is using melee attacks"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"when struck in combat") || strstr(desc,"When struck in combat"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"successful melee attack"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"chance per attack"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"chance per hit"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"that strikes a party member"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"when hit by a melee attack"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"landing a melee critical strike"))
//								pr|=PROC_ON_CRIT_ATTACK;
//							if( strstr( desc,"your critical strikes"))
//								pr|=PROC_ON_CRIT_ATTACK;
//							if( strstr( desc,"whenever you deal ranged damage"))
//								pr|=PROC_ON_RANGED_ATTACK;
//							//					if( strstr( desc,"whenever you deal melee damage"))
//							if( strstr( desc,"you deal melee damage"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"your melee attacks"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"damage with your Sword"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"when struck in melee combat"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"any successful spell cast against the priest"))
//								pr|=PROC_ON_SPELL_HIT_VICTIM;
//							if( strstr( desc,"the next melee attack on the caster"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"striking melee or ranged attackers"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM|PROC_ON_RANGED_ATTACK_VICTIM;
//							if( strstr( desc,"when damaging an enemy in melee"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"victim of a critical strike"))
//								pr|=PROC_ON_CRIT_HIT_VICTIM;
//							if( strstr( desc,"on successful melee or ranged attack"))
//								pr|=PROC_ON_MELEE_ATTACK|PROC_ON_RANGED_ATTACK;
//							if( strstr( desc,"enemy that strikes you in melee"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"after getting a critical strike"))
//								pr|=PROC_ON_CRIT_ATTACK;
//							if( strstr( desc,"whenever damage is dealt to you"))
//								pr|=PROC_ON_ANY_DAMAGE_VICTIM;
//							if( strstr( desc,"when ranged or melee damage is dealt"))
//								pr|=PROC_ON_MELEE_ATTACK|PROC_ON_RANGED_ATTACK;
//							if( strstr( desc,"damaging melee attacks"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"on melee or ranged attack"))
//								pr|=PROC_ON_MELEE_ATTACK|PROC_ON_RANGED_ATTACK;
//							if( strstr( desc,"on a melee swing"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"Chance on melee"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"spell criticals against you"))
//								pr|=PROC_ON_SPELL_CRIT_HIT_VICTIM;
//							if( strstr( desc,"after being struck by a melee or ranged critical hit"))
//								pr|=PROC_ON_CRIT_HIT_VICTIM;
//							//					if( strstr( desc,"on a critical hit"))
//							if( strstr( desc,"critical hit"))
//								pr|=PROC_ON_CRIT_ATTACK;
//							if( strstr( desc,"strikes the caster"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"a spell, melee or ranged attack hits the caster"))
//								pr|=PROC_ON_ANY_DAMAGE_VICTIM;
//							if( strstr( desc,"after dealing a critical strike"))
//								pr|=PROC_ON_CRIT_ATTACK;
//							if( strstr( desc,"each melee or ranged damage hit against the priest"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM|PROC_ON_RANGED_ATTACK_VICTIM;				
//							if( strstr( desc, "a chance to deal additional"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc, "chance to get an extra attack"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc, "melee attacks has"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc, "any damage spell hits a target"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc, "giving each melee attack a chance"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc, "damage when hit"))
//								pr|=PROC_ON_ANY_DAMAGE_VICTIM; //myabe melee damage ?
//							if( strstr( desc, "gives your"))
//							{
//								if( strstr( desc, "melee"))
//									pr|=PROC_ON_MELEE_ATTACK;
//								else if( strstr( desc,"sinister strike, backstab, gouge and shiv"))
//									pr|=PROC_ON_CAST_SPELL;
//								else if( strstr( desc,"chance to daze the target"))
//									pr|=PROC_ON_CAST_SPELL;
//								else if( strstr( desc,"finishing moves"))
//									pr|=PROC_ON_CAST_SPELL;
//								//						else if( strstr( desc,"shadow bolt, shadowburn, soul fire, incinerate, searing pain and conflagrate"))
//								//							pr|=PROC_ON_CAST_SPELL|PROC_TARGET_SELF;
//								//we should find that specific spell (or group) on what we will trigger
//								else pr|=PROC_ON_CAST_SPECIFIC_SPELL;
//							}
//							if( strstr( desc, "chance to add an additional combo") && strstr(desc, "critical") )
//								pr|=PROC_ON_CRIT_ATTACK;
//							else if( strstr( desc, "chance to add an additional combo"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc, "victim of a melee or ranged critical strike"))
//								pr|=PROC_ON_CRIT_HIT_VICTIM;
//							if( strstr( desc, "getting a critical effect from"))
//								pr|=PROC_ON_SPELL_CRIT_HIT_VICTIM;
//							if( strstr( desc, "damaging attack is taken"))
//								pr|=PROC_ON_ANY_DAMAGE_VICTIM;
//							if( strstr( desc, "struck by a Stun or Immobilize"))
//								pr|=PROC_ON_SPELL_HIT_VICTIM;
//							if( strstr( desc, "melee critical strike"))
//								pr|=PROC_ON_CRIT_ATTACK;
//							if( strstr( nametext, "Bloodthirst"))
//								pr|=PROC_ON_MELEE_ATTACK | PROC_TARGET_SELF;
//							if( strstr( desc, "experience or honor"))
//								pr|=PROC_ON_GAIN_EXPIERIENCE;
//							if( strstr( desc,"your next offensive ability"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc,"hit by a melee or ranged attack"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM | PROC_ON_RANGED_ATTACK_VICTIM;
//							if( strstr( desc,"enemy strikes the caster"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"melee and ranged attacks against you"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM | PROC_ON_RANGED_ATTACK_VICTIM;
//							if( strstr( desc,"when a block occurs"))
//								pr|=PROC_ON_BLOCK_VICTIM;
//							if( strstr( desc,"dealing a critical strike from a weapon swing, spell, or ability"))
//								pr|=PROC_ON_CRIT_ATTACK|PROC_ON_SPELL_CRIT_HIT;
//							if( strstr( desc,"dealing a critical strike from a weapon swing, spell, or ability"))
//								pr|=PROC_ON_CRIT_ATTACK|PROC_ON_SPELL_CRIT_HIT;
//							if( strstr( desc,"shadow bolt critical strikes increase shadow damage"))
//								pr|=PROC_ON_SPELL_CRIT_HIT;
//							if( strstr( desc,"next offensive ability"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc,"after being hit with a shadow or fire spell"))
//								pr|=PROC_ON_SPELL_LAND_VICTIM;
//							if( strstr( desc,"giving each melee attack"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"each strike has"))
//								pr|=PROC_ON_MELEE_ATTACK;		
//							if( strstr( desc,"your Fire damage spell hits"))
//								pr|=PROC_ON_CAST_SPELL;		//this happens only on hit ;)
//							if( strstr( desc,"corruption, curse of agony, siphon life and seed of corruption spells also cause"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc,"pain, mind flay and vampiric touch spells also cause"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc,"shadow damage spells have"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc,"on successful spellcast"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc,"your spell criticals have"))
//								pr|=PROC_ON_SPELL_CRIT_HIT | PROC_ON_SPELL_CRIT_HIT_VICTIM;
//							if( strstr( desc,"after dodging their attack"))
//							{
//								pr|=PROC_ON_DODGE_VICTIM;
//								if( strstr( desc,"add a combo point"))
//									pr|=PROC_TARGET_SELF;
//							}
//							if( strstr( desc,"fully resisting"))
//								pr|=PROC_ON_RESIST_VICTIM;
//							if( strstr( desc,"Your Shadow Word: Pain, Mind Flay and Vampiric Touch spells also cause the target"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc,"chance on spell hit"))
//								pr|=PROC_ON_CAST_SPELL;
//							if( strstr( desc,"your melee and ranged attacks"))
//								pr|=PROC_ON_MELEE_ATTACK|PROC_ON_RANGED_ATTACK;
//							//					if( strstr( desc,"chill effect to your Blizzard"))
//							//						pr|=PROC_ON_CAST_SPELL;	
//							//////////////////////////////////////////////////
//							//proc dmg flags
//							//////////////////////////////////////////////////
//							if( strstr( desc,"each attack blocked"))
//								pr|=PROC_ON_BLOCK_VICTIM;
//							if( strstr( desc,"into flame, causing an additional"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"victim of a critical melee strike"))
//								pr|=PROC_ON_CRIT_HIT_VICTIM;
//							if( strstr( desc,"damage to melee attackers"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"target blocks a melee attack"))
//								pr|=PROC_ON_BLOCK_VICTIM;
//							if( strstr( desc,"ranged and melee attacks to deal"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM | PROC_ON_RANGED_ATTACK_VICTIM;
//							if( strstr( desc,"damage on hit"))
//								pr|=PROC_ON_ANY_DAMAGE_VICTIM;
//							if( strstr( desc,"chance on hit"))
//								pr|=PROC_ON_MELEE_ATTACK;
//							if( strstr( desc,"after being hit by any damaging attack"))
//								pr|=PROC_ON_ANY_DAMAGE_VICTIM;
//							if( strstr( desc,"striking melee or ranged attackers"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM | PROC_ON_RANGED_ATTACK_VICTIM;
//							if( strstr( desc,"damage to attackers when hit"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"striking melee attackers"))
//								pr|=PROC_ON_MELEE_ATTACK_VICTIM;
//							if( strstr( desc,"whenever the caster takes damage"))
//								pr|=PROC_ON_ANY_DAMAGE_VICTIM;
//							if( strstr( desc,"damage on every attack"))
//								pr|=PROC_ON_MELEE_ATTACK | PROC_ON_RANGED_ATTACK;
//							if( strstr( desc,"chance to reflect Fire spells"))
//								pr|=PROC_ON_SPELL_HIT_VICTIM;
//							if( strstr( desc,"hunter takes on the aspects of a hawk"))
//								pr|=PROC_TARGET_SELF | PROC_ON_RANGED_ATTACK;
//							if( strstr( desc,"successful auto shot attacks"))
//								pr|=PROC_ON_AUTO_SHOT_HIT;
//							if( strstr( desc,"after getting a critical effect from your"))
//								pr=PROC_ON_SPELL_CRIT_HIT;
//							//					if( strstr( desc,"Your critical strikes from Fire damage"))
//							//						pr|=PROC_ON_SPELL_CRIT_HIT;
//						}//end "if procspellaura"
//						//dirty fix to remove auras that should expire on event and they are not
//						//				else if(sp->procCharges>0)
//						//				{
//						//there are at least 185 spells that should loose charge uppon some event.Be prepared to add more here !
//						// ! watch it cause this might conflict with our custom modified spells like : lighning shield !
//
//						//spells like : Presence of Mind,Nature's Swiftness, Inner Focus,Amplify Curse,Coup de Grace
//						//SELECT * FROM dbc_spell where proc_charges!=0 and (effect_aura_1=108 or effect_aura_2=108 and effect_aura_3=108) and description!=""
//						//					if(aura == SPELL_AURA_ADD_PCT_MODIFIER)
//						//						sp->AuraInterruptFlags |= AURA_INTERRUPT_ON_CAST_SPELL;
//						//most of them probably already have these flags...not sure if we should add to all of them without checking
//						/*					if( strstr( desc, "melee"))
//						sp->AuraInterruptFlags |= AURA_INTERRUPT_ON_START_ATTACK;
//						if( strstr( desc, "ranged"))
//						sp->AuraInterruptFlags |= AURA_INTERRUPT_ON_START_ATTACK;*/
//						//				}
//					}//end "if aura"
//				}//end "for each effect"
//				sp->procFlags = pr;
//
//				if( strstr( desc, "Must remain seated"))
//				{
//					sp->RecoveryTime = 1000;
//					sp->CategoryRecoveryTime = 1000;
//				}
//
//				//////////////////////////////////////////////////////////////////////////////////////////////////////
//				// procintervals
//				//////////////////////////////////////////////////////////////////////////////////////////////////////
//				//omg lighning shield trigger spell id's are all wrong ?
//				//if you are bored you could make thiese by hand but i guess we might find other spells with this problem..and this way it's safe
//				if( strstr( nametext, "Lightning Shield" ) && sp->EffectTriggerSpell[0] )
//				{
//					//check if we can find in the desription
//					char *startofid = strstr(desc, "for $");
//					if( startofid )
//					{
//						startofid += strlen("for $");
//						sp->EffectTriggerSpell[0] = atoi( startofid ); //get new lightning shield trigger id
//					}
//					sp->proc_interval = 3000; //few seconds
//				}
//				//mage ignite talent should proc only on some chances
//				else if( strstr( nametext, "Ignite") && sp->Id>=11119 && sp->Id<=12848 && sp->EffectApplyAuraName[0] == 4 )
//				{
//					//check if we can find in the desription
//					char *startofid=strstr(desc, "an additional ");
//					if(startofid)
//					{
//						startofid += strlen("an additional ");
//						sp->EffectBasePoints[0]=atoi(startofid); //get new value. This is actually level*8 ;)
//					}
//					sp->Effect[0] = 6; //aura
//					sp->EffectApplyAuraName[0] = 42; //force him to use procspell effect
//					sp->EffectTriggerSpell[0] = 12654; //evil , but this is good for us :D
//					sp->procFlags = PROC_ON_SPELL_CRIT_HIT; //add procflag here since this was not processed with the others !
//				}
//				// Winter's Chill handled by frost school
//				else if( strstr( nametext, "Winter's Chill"))
//				{
//					sp->School = 4;
//				}
//				// Blackout handled by Shadow school
//				else if( strstr( nametext, "Blackout"))
//				{
//					sp->School = 5;
//				}
//#ifndef NEW_PROCFLAGS
//				// Shadow Weaving
//				else if( strstr( nametext, "Shadow Weaving"))
//				{
//					sp->School = 5;
//					sp->EffectApplyAuraName[0] = 42;
//					sp->procChance = sp->EffectBasePoints[0] + 1;
//					sp->procFlags = PROC_ON_CAST_SPECIFIC_SPELL;
//				}
//#endif
//				//Improved Aspect of the Hawk
//				else if( strstr( nametext, "Improved Aspect of the Hawk"))
//					sp->EffectSpellGroupRelation[1] = 0x00100000;
//				//more triggered spell ids are wrong. I think blizz is trying to outsmart us :S
//				else if( strstr( nametext, "Nature's Guardian"))
//				{
//					sp->EffectTriggerSpell[0] = 31616;
//					sp->proc_interval = 5000;
//				}
//				//Chain Heal all ranks %50 heal value (49 + 1)
//				else if( strstr( nametext, "Chain Heal"))
//				{
//					sp->EffectDieSides[0] = 49;
//				}
//				//this starts to be an issue for trigger spell id : Deep Wounds
//				else if( strstr( nametext, "Deep Wounds") && sp->EffectTriggerSpell[0])
//				{
//					//check if we can find in the desription
//					char *startofid=strstr(desc, "over $");
//					if(startofid)
//					{
//						startofid += strlen("over $");
//						sp->EffectTriggerSpell[0] = atoi(startofid);
//					}
//				}
//				else if( strstr( nametext, "Holy Shock"))
//				{
//					//check if we can find in the desription
//					char *startofid=strstr(desc, "causing $");
//					if(startofid)
//					{
//						startofid += strlen("causing $");
//						sp->EffectTriggerSpell[0] = atoi(startofid);
//					}
//					//check if we can find in the desription
//					startofid=strstr(desc, " or $");
//					if(startofid)
//					{
//						startofid += strlen(" or $");
//						sp->EffectTriggerSpell[1]=atoi(startofid);
//					}
//				}
//				else if( strstr( nametext, "Touch of Weakness"))
//				{
//					//check if we can find in the desription
//					char *startofid=strstr(desc, "cause $");
//					if(startofid)
//					{
//						startofid += strlen("cause $");
//						sp->EffectTriggerSpell[0] = atoi(startofid);
//						sp->EffectTriggerSpell[1]=sp->EffectTriggerSpell[0]; //later versions of this spell changed to eff[1] the aura
//						sp->procFlags = uint32(PROC_ON_MELEE_ATTACK_VICTIM);
//					}
//				}
//				else if( strstr( nametext, "Firestone Passive"))
//				{
//					//Enchants the main hand weapon with fire, granting each attack a chance to deal $17809s1 additional fire damage.
//					//check if we can find in the desription
//					char * startofid=strstr(desc, "to deal $");
//					if(startofid)
//					{
//						startofid += strlen("to deal $");
//						sp->EffectTriggerSpell[0] = atoi(startofid);
//						sp->EffectApplyAuraName[0] = 42;
//						sp->procFlags = PROC_ON_MELEE_ATTACK;
//						sp->procChance = 50;
//					}
//				}
//				//some procs trigger at intervals
//				else if( strstr( nametext, "Water Shield"))
//				{
//					sp->proc_interval = 3000; //few seconds
//					sp->procFlags |= PROC_TARGET_SELF;
//				}
//				else if( strstr( nametext, "Earth Shield"))
//					sp->proc_interval = 3000; //few seconds
//				else if( strstr( nametext, "Shadowguard"))
//					sp->proc_interval = 3000; //few seconds
//				else if( strstr( nametext, "Poison Shield"))
//					sp->proc_interval = 3000; //few seconds
//				else if( strstr( nametext, "Infused Mushroom"))
//					sp->proc_interval = 10000; //10 seconds
//				else if( strstr( nametext, "Aviana's Purpose"))
//					sp->proc_interval = 10000; //10 seconds
//				//don't change to namehash since we are searching only a protion of the name
//				else if( strstr( nametext, "Crippling Poison"))
//				{
//					sp->SpellGroupType |= 16384; //some of them do have the flags but i's hard to write down those some from 130 spells
//					sp->c_is_flags |= SPELL_FLAG_IS_POISON;
//				}
//				else if( strstr( nametext, "Mind-Numbing Poison"))
//				{
//					sp->SpellGroupType |= 32768; //some of them do have the flags but i's hard to write down those some from 130 spells
//					sp->c_is_flags |= SPELL_FLAG_IS_POISON;
//				}
//				else if( strstr( nametext, "Instant Poison"))
//				{
//					sp->SpellGroupType |= 8192; //some of them do have the flags but i's hard to write down those some from 130 spells
//					sp->c_is_flags |= SPELL_FLAG_IS_POISON;
//				}
//				else if( strstr( nametext, "Deadly Poison"))
//				{
//					sp->SpellGroupType |= 65536; //some of them do have the flags but i's hard to write down those some from 130 spells
//					sp->c_is_flags |= SPELL_FLAG_IS_POISON;
//				}
//				else if( strstr( nametext, "Wound Poison"))
//				{
//					sp->SpellGroupType |= 268435456; //some of them do have the flags but i's hard to write down those some from 130 spells
//					sp->c_is_flags |= SPELL_FLAG_IS_POISON;
//				}
//				else if( strstr( nametext, "Scorpid Poison") )
//				{
//					// groups?
//					sp->c_is_flags |= SPELL_FLAG_IS_POISON;
//				}
//
//				//warlock - shadow bolt
//				if( sp->NameHash == SPELL_HASH_SHADOW_BOLT )
//					sp->SpellGroupType |= 1; //some of them do have the flags but i's hard to write down those some from 130 spells
//
//				//mage Ice Floes affects these spells : Cone of Cold,Cold Snap,Ice Barrier,Ice Block
//				if( sp->NameHash == SPELL_HASH_CONE_OF_COLD || sp->NameHash == SPELL_HASH_COLD_SNAP || sp->NameHash == SPELL_HASH_ICE_BARRIER || sp->NameHash == SPELL_HASH_ICE_BLOCK )
//					sp->EffectSpellGroupRelation[0] = 0x00200000;
//
//				/*		else if( strstr( nametext, "Anesthetic Poison"))
//				sp->SpellGroupType |= 0; //not yet known ? 
//				else if( strstr( nametext, "Blinding Powder"))
//				sp->SpellGroupType |= 0; //not yet known ?
//				else 
//				if( sp->NameHash == SPELL_HASH_ILLUMINATION )
//				sp->EffectTriggerSpell[0] = 20272;*/  // broken trigger spell, do not use
//				//sp->dummy=result;
//				/*		//if there is a proc spell and has 0 as charges then it's probably going to triger infinite times. Better not save these
//				if(sp->procCharges==0)
//				sp->procCharges=-1;*/
//
//				// Set default mechanics if we don't already have one
//				if( !sp->MechanicsType )
//				{
//					//Set Silencing spells mechanic.
//					if( sp->EffectApplyAuraName[0] == 27 || 
//						sp->EffectApplyAuraName[1] == 27 ||
//						sp->EffectApplyAuraName[2] == 27 )
//						sp->MechanicsType = MECHANIC_SILENCED;
//
//					//Set Stunning spells mechanic.
//					if( sp->EffectApplyAuraName[0] == 12 || 
//						sp->EffectApplyAuraName[1] == 12 ||
//						sp->EffectApplyAuraName[2] == 12 )
//						sp->MechanicsType = MECHANIC_STUNNED;
//
//					//Set Fearing spells mechanic
//					if( sp->EffectApplyAuraName[0] == 7 || 
//						sp->EffectApplyAuraName[1] == 7 ||
//						sp->EffectApplyAuraName[2] == 7 )
//						sp->MechanicsType = MECHANIC_FLEEING;
//				}
//				if( sp->proc_interval != 0 )
//					sp->procFlags |= PROC_REMOVEONUSE;
//
//				// Seal of Command - Proc Chance
//				if( sp->NameHash == SPELL_HASH_SEAL_OF_COMMAND )
//				{
//					sp->procChance = 25;
//					sp->School = SCHOOL_NORMAL; //the procspells of the original seal of command have fizical school instead of holy
//					sp->Spell_Dmg_Type = SPELL_DMG_TYPE_MAGIC; //heh, crazy spell uses melee/ranged/magic dmg type for 1 spell. Now which one is correct ?
//				}
//
//				//Seal of Jusice - Proc Chance
//				if( sp->NameHash == SPELL_HASH_SEAL_OF_JUSTICE )
//					sp->procChance = 25;
//
//				/* Decapitate */
//				if( sp->NameHash == SPELL_HASH_DECAPITATE )
//					sp->procChance = 30;
//
//				//shaman - shock, has no spellgroup.very dangerous move !
//				if( sp->NameHash == SPELL_HASH_SHOCK )
//					sp->SpellGroupType = 4;
//
//				//mage - fireball. Only some of the spell has the flags 
//				if( sp->NameHash == SPELL_HASH_FIREBALL )
//					sp->SpellGroupType |= 1;
//
//				if( sp->NameHash == SPELL_HASH_DIVINE_SHIELD || sp->NameHash == SPELL_HASH_DIVINE_PROTECTION || sp->NameHash == SPELL_HASH_BLESSING_OF_PROTECTION )
//					sp->MechanicsType = 25;
//
//				/* hackfix for this - FIX ME LATER - Burlex */
//				if( namehash == SPELL_HASH_SEAL_FATE )
//					sp->procFlags = 0;
//
//				if(
//					((sp->Attributes & ATTRIBUTES_TRIGGER_COOLDOWN) && (sp->AttributesEx & ATTRIBUTESEX_NOT_BREAK_STEALTH)) //rogue cold blood
//					|| ((sp->Attributes & ATTRIBUTES_TRIGGER_COOLDOWN) && (!sp->AttributesEx || sp->AttributesEx & ATTRIBUTESEX_REMAIN_OOC))
//					)
//				{
//					sp->c_is_flags |= SPELL_FLAG_IS_REQUIRECOOLDOWNUPDATE;
//				}
//
//				if( namehash == SPELL_HASH_SHRED || namehash == SPELL_HASH_BACKSTAB || namehash == SPELL_HASH_AMBUSH )
//				{
//					// Shred, Backstab, Ambush
//					sp->in_front_status = 2;
//				}

				//junk code to get me has :P 
				//if(sp->Id==11267 || sp->Id==11289 || sp->Id==6409)
				//	printf("!!!!!!! name %s , id %u , hash %u \n",nametext,sp->Id, namehash);
		if(!itrSpell->Inc())
			break;
	}
	//itrSpell->Destruct();


	/////////////////////////////////////////////////////////////////
	//SPELL COEFFICIENT SETTINGS START
	//////////////////////////////////////////////////////////////////
	itrSpell = dbcSpell.MakeIterator();
	while(!itrSpell->AtEnd())
	{
		// get spellentry
		SpellEntry * sp = itrSpell->Get();
		// SpellID
		uint32 spellid = sp->Id;

		//Setting Cast Time Coefficient
		SpellCastTime *sd = dbcSpellCastTime.LookupEntry(sp->CastingTimeIndex);
		float castaff = float(GetCastTime(sd));
		if(castaff < 1500) castaff = 1500;
		else
			if(castaff > 7000) castaff = 7000;

		sp->casttime_coef = castaff / 3500;		 

		SpellEntry * spz;
		bool spcheck = false;

		//Flag for DoT and HoT
		for( uint8 i = 0 ; i < 3 ; i++ )
		{
			if (sp->EffectApplyAuraName[i] == SPELL_AURA_PERIODIC_DAMAGE ||
				sp->EffectApplyAuraName[i] == SPELL_AURA_PERIODIC_HEAL ||
				sp->EffectApplyAuraName[i] == SPELL_AURA_PERIODIC_LEECH )
			{
				sp->spell_coef_flags |= SPELL_FLAG_IS_DOT_OR_HOT_SPELL;
				break;
			}
		}

		//Flag for DD or DH
		for( uint8 i = 0 ; i < 3 ; i++ )
		{
			if ( sp->EffectApplyAuraName[i] == SPELL_AURA_PERIODIC_TRIGGER_SPELL && sp->EffectTriggerSpell[i] )
			{
				spz = dbcSpell.LookupEntry( sp->EffectTriggerSpell[i] );
				if( spz &&
					spz->Effect[i] == SPELL_EFFECT_SCHOOL_DAMAGE ||
					spz->Effect[i] == SPELL_EFFECT_HEAL
					) 
					spcheck = true;
			}
			if (sp->Effect[i] == SPELL_EFFECT_SCHOOL_DAMAGE ||
				sp->Effect[i] == SPELL_EFFECT_HEAL ||
				spcheck
				)
			{
				sp->spell_coef_flags |= SPELL_FLAG_IS_DD_OR_DH_SPELL;
				break;
			}
		}

		for(uint8 i = 0 ; i < 3; i++)
		{
			switch (sp->EffectImplicitTargetA[i])
			{
				//AoE
			case EFF_TARGET_ALL_TARGETABLE_AROUND_LOCATION_IN_RADIUS:
			case EFF_TARGET_ALL_ENEMY_IN_AREA:
			case EFF_TARGET_ALL_ENEMY_IN_AREA_INSTANT:
			case EFF_TARGET_ALL_PARTY_AROUND_CASTER:
			case EFF_TARGET_ALL_ENEMIES_AROUND_CASTER:
			case EFF_TARGET_IN_FRONT_OF_CASTER:
			case EFF_TARGET_ALL_ENEMY_IN_AREA_CHANNELED:
			case EFF_TARGET_ALL_PARTY_IN_AREA_CHANNELED:
			case EFF_TARGET_ALL_FRIENDLY_IN_AREA:
			case EFF_TARGET_ALL_TARGETABLE_AROUND_LOCATION_IN_RADIUS_OVER_TIME:
			case EFF_TARGET_ALL_PARTY:
			case EFF_TARGET_LOCATION_INFRONT_CASTER:
			case EFF_TARGET_BEHIND_TARGET_LOCATION:
			case EFF_TARGET_LOCATION_INFRONT_CASTER_AT_RANGE:
				{
					sp->spell_coef_flags |= SPELL_FLAG_AOE_SPELL;
					break;
				}
			}	
		}

		for(uint8 i = 0 ; i < 3 ; i++)
		{
			switch (sp->EffectImplicitTargetB[i])
			{
				//AoE
			case EFF_TARGET_ALL_TARGETABLE_AROUND_LOCATION_IN_RADIUS:
			case EFF_TARGET_ALL_ENEMY_IN_AREA:
			case EFF_TARGET_ALL_ENEMY_IN_AREA_INSTANT:
			case EFF_TARGET_ALL_PARTY_AROUND_CASTER:
			case EFF_TARGET_ALL_ENEMIES_AROUND_CASTER:
			case EFF_TARGET_IN_FRONT_OF_CASTER:
			case EFF_TARGET_ALL_ENEMY_IN_AREA_CHANNELED:
			case EFF_TARGET_ALL_PARTY_IN_AREA_CHANNELED:
			case EFF_TARGET_ALL_FRIENDLY_IN_AREA:
			case EFF_TARGET_ALL_TARGETABLE_AROUND_LOCATION_IN_RADIUS_OVER_TIME:
			case EFF_TARGET_ALL_PARTY:
			case EFF_TARGET_LOCATION_INFRONT_CASTER:
			case EFF_TARGET_BEHIND_TARGET_LOCATION:
			case EFF_TARGET_LOCATION_INFRONT_CASTER_AT_RANGE:
				{
					sp->spell_coef_flags |= SPELL_FLAG_AOE_SPELL;
					break;
				}
			}	
		}

		//Special Cases
		//Holy Light & Flash of Light
		if(sp->NameHash == SPELL_HASH_HOLY_LIGHT ||
			sp->NameHash == SPELL_HASH_FLASH_OF_LIGHT)
			sp->spell_coef_flags |= SPELL_FLAG_IS_DD_OR_DH_SPELL;

		//Additional Effect (not healing or damaging)
		for( uint8 i = 0 ; i < 3 ; i++ )
		{
			if(sp->Effect[i] == 0)
				continue;

			switch (sp->Effect[i])
			{
			case SPELL_EFFECT_SCHOOL_DAMAGE:
			case SPELL_EFFECT_ENVIRONMENTAL_DAMAGE:
			case SPELL_EFFECT_HEALTH_LEECH:
			case SPELL_EFFECT_WEAPON_DAMAGE_NOSCHOOL:
			case SPELL_EFFECT_ADD_EXTRA_ATTACKS:
			case SPELL_EFFECT_WEAPON_PERCENT_DAMAGE:
			case SPELL_EFFECT_POWER_BURN:
			case SPELL_EFFECT_ATTACK:
			case SPELL_EFFECT_HEAL:
			case SPELL_EFFECT_HEALTH_FUNNEL:
			case SPELL_EFFECT_HEAL_MAX_HEALTH:
			case SPELL_EFFECT_DUMMY:
				continue;
			}

			switch (sp->EffectApplyAuraName[i])
			{
			case SPELL_AURA_PERIODIC_DAMAGE:
			case SPELL_AURA_PROC_TRIGGER_DAMAGE:
			case SPELL_AURA_PERIODIC_DAMAGE_PERCENT:
			case SPELL_AURA_POWER_BURN:
			case SPELL_AURA_PERIODIC_HEAL:
			case SPELL_AURA_MOD_INCREASE_HEALTH:
			case SPELL_AURA_PERIODIC_HEALTH_FUNNEL:
			case SPELL_AURA_DUMMY:
				continue;
			}

			sp->spell_coef_flags |= SPELL_FLAG_ADITIONAL_EFFECT;
			break;

		}

		////Calculating fixed coeficients
		////Channeled spells
		//if( sp->ChannelInterruptFlags != 0 )
		//{
		//	float Duration = float( GetDuration( dbcSpellDuration.LookupEntry( sp->DurationIndex ) ));
		//	if(Duration < 1500) Duration = 1500;
		//	else if(Duration > 7000) Duration = 7000;
		//	sp->fixed_hotdotcoef = (Duration / 3500.0f);

		//	if( sp->spell_coef_flags & SPELL_FLAG_ADITIONAL_EFFECT )
		//		sp->fixed_hotdotcoef *= 0.95f;
		//	if( sp->spell_coef_flags & SPELL_FLAG_AOE_SPELL )
		//		sp->fixed_hotdotcoef *= 0.5f;
		//}

		////Standard spells
		//else if( (sp->spell_coef_flags & SPELL_FLAG_IS_DD_OR_DH_SPELL) && !(sp->spell_coef_flags & SPELL_FLAG_IS_DOT_OR_HOT_SPELL) )
		//{
		//	sp->fixed_dddhcoef = sp->casttime_coef;
		//	if( sp->spell_coef_flags & SPELL_FLAG_ADITIONAL_EFFECT )
		//		sp->fixed_dddhcoef *= 0.95f;
		//	if( sp->spell_coef_flags & SPELL_FLAG_AOE_SPELL )
		//		sp->fixed_dddhcoef *= 0.5f;
		//}

		////Over-time spells
		//else if( !(sp->spell_coef_flags & SPELL_FLAG_IS_DD_OR_DH_SPELL) && (sp->spell_coef_flags & SPELL_FLAG_IS_DOT_OR_HOT_SPELL) )
		//{
		//	float Duration = float( GetDuration( dbcSpellDuration.LookupEntry( sp->DurationIndex ) ));
		//	sp->fixed_hotdotcoef = (Duration / 15000.0f);

		//	if( sp->spell_coef_flags & SPELL_FLAG_ADITIONAL_EFFECT )
		//		sp->fixed_hotdotcoef *= 0.95f;
		//	if( sp->spell_coef_flags & SPELL_FLAG_AOE_SPELL )
		//		sp->fixed_hotdotcoef *= 0.5f;

		//}

		////Combined standard and over-time spells
		//else if( sp->spell_coef_flags & SPELL_FLAG_IS_DD_DH_DOT_SPELL )
		//{
		//	float Duration = float( GetDuration( dbcSpellDuration.LookupEntry( sp->DurationIndex ) ));
		//	float Portion_to_Over_Time = (Duration / 15000.0f) / ((Duration / 15000.0f) + sp->casttime_coef );
		//	float Portion_to_Standard = 1.0f - Portion_to_Over_Time;

		//	sp->fixed_dddhcoef = sp->casttime_coef * Portion_to_Standard;
		//	sp->fixed_hotdotcoef = (Duration / 15000.0f) * Portion_to_Over_Time;

		//	if( sp->spell_coef_flags & SPELL_FLAG_ADITIONAL_EFFECT )
		//	{
		//		sp->fixed_dddhcoef *= 0.95f;
		//		sp->fixed_hotdotcoef *= 0.95f;
		//	}
		//	if( sp->spell_coef_flags & SPELL_FLAG_AOE_SPELL )
		//	{
		//		sp->fixed_dddhcoef *= 0.5f;
		//		sp->fixed_hotdotcoef *= 0.5f;
		//	}		
		//}

		///	SPELLS CAN CRIT ///
		sp->spell_can_crit = true; // - except in special cases noted in this section


		//////////////////////////////////////////////////////
		// CLASS-SPECIFIC SPELL FIXES						//
		//////////////////////////////////////////////////////

		/* Note: when applying spell hackfixes, please follow a template */
/*
		//////////////////////////////////////////
		// CLASS_WARRIOR								//
		//////////////////////////////////////////

		// Insert warrior spell fixes here

		// Sword Specialization - Rank 1 to 5 - Proc Chance
		// 12281 12812 12813 12814
		sp = dbcSpell.LookupEntry(12281); // Rank 1
		if(sp != NULL)
			sp->procChance = 1;
		sp = dbcSpell.LookupEntry(12812); // Rank 2
		if(sp != NULL)
			sp->procChance = 2;
		sp = dbcSpell.LookupEntry(12813); // Rank 3
		if(sp != NULL)
			sp->procChance = 3;
		sp = dbcSpell.LookupEntry(12814); // Rank 4
		if(sp != NULL)
			sp->procChance = 4;
		sp = dbcSpell.LookupEntry(12815); // Rank 5
		if(sp != NULL)
			sp->procChance = 5;

		//////////////////////////////////////////
		// PALADIN								//
		//////////////////////////////////////////

		// Insert paladin spell fixes here

		// Seal of Righteousness - cannot crit
		if( sp->NameHash == SPELL_HASH_SEAL_OF_RIGHTEOUSNESS )
			sp->spell_can_crit = false;

		//////////////////////////////////////////
		// HUNTER								//
		//////////////////////////////////////////

		// Insert hunter spell fixes here

		// THESE FIXES ARE GROUPED FOR CODE CLEANLINESS.
		/*
		// Concussive Shot, Distracting Shot, Silencing Shot - ranged spells
		if( sp->NameHash == SPELL_HASH_CONCUSSIVE_SHOT || sp->NameHash == SPELL_HASH_DISTRACTING_SHOT || sp->NameHash == SPELL_HASH_SILENCING_SHOT || sp->NameHash == SPELL_HASH_SCATTER_SHOT || sp->NameHash == SPELL_HASH_TRANQUILIZING_SHOT )
		sp->is_ranged_spell = true;

		// All stings - ranged spells
		if( sp->NameHash == SPELL_HASH_SERPENT_STING || sp->NameHash == SPELL_HASH_SCORPID_STING || sp->NameHash == SPELL_HASH_VIPER_STING || sp->NameHash == SPELL_HASH_WYVERN STING )
		sp->is_ranged_spell = true;
		*//*/
*/
/*
		// come to think of it... anything *castable* requiring a ranged weapon is a ranged spell -.-
		// Note that talents etc also come under this, however it does not matter
		// if they get flagged as ranged spells because is_ranged_spell is only used for
		// differentiating between resistable and physically avoidable spells.
		if( sp->EquippedItemClass == 2 && sp->EquippedItemSubClass & 262156 ) // 4 + 8 + 262144 ( becomes item classes 2, 3 and 18 which correspond to bow, gun and crossbow respectively)
		{
			sp->is_ranged_spell = true;
		}

		//////////////////////////////////////////
		// CLASS_ROGUE								//
		//////////////////////////////////////////

		// Insert rogue spell fixes here

		//////////////////////////////////////////
		// CLASS_PRIEST								//
		//////////////////////////////////////////

		// Insert priest spell fixes here

		//////////////////////////////////////////
		// SHAMAN								//
		//////////////////////////////////////////

		// Insert shaman spell fixes here

		// Lightning Shield - cannot crit
		if( sp->NameHash == SPELL_HASH_LIGHTNING_SHIELD ) // not a mistake, the correct proc spell for lightning shield is also called lightning shield
			sp->spell_can_crit = false;

		//////////////////////////////////////////
		// CLASS_MAGE								//
		//////////////////////////////////////////

		// Insert mage spell fixes here

		// Invisibility: changed to "Dummy" effect for invisibility to trigger.
		if( sp->NameHash == SPELL_HASH_INVISIBILITY )
		{
			sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
			sp->EffectApplyAuraName[0] = SPELL_AURA_DUMMY;
		}

		// Hypothermia: undispellable
		if( sp->NameHash == SPELL_HASH_HYPOTHERMIA )
			sp->c_is_flags |= SPELL_FLAG_IS_FORCEDDEBUFF;

		//////////////////////////////////////////
		// WARLOCK								//
		//////////////////////////////////////////

		// Insert warlock spell fixes here

		//////////////////////////////////////////
		// CLASS_DRUID								//
		//////////////////////////////////////////

		// Insert druid spell fixes here
*/
		if(!itrSpell->Inc())
			break;
	}
	//itrSpell->Destruct();

	/*
	//Settings for special cases
	QueryResult * resultx = WorldDatabase.Query("SELECT * FROM spell_coef_override");
	if( resultx != NULL )
	{
		do 
		{
			Field * f;
			f = resultx->Fetch();
			SpellEntry * sp = dbcSpell.LookupEntry( f[0].GetUInt32() );
			if( sp != NULL )
			{
				sp->Dspell_coef_override = f[2].GetFloat();
				sp->OTspell_coef_override = f[3].GetFloat();
			}
			else
				MyLog::log->warn("SpellCoefOverride : Has nonexistant spell %u.", f[0].GetUInt32());
		} while( resultx->NextRow() );
		delete resultx;
	}

	//Fully loaded coefficients, we must share channeled coefficient to its triggered spells
	itrSpell = dbcSpell.MakeIterator();
	while(!itrSpell->AtEnd())
	{
		// get spellentry
		SpellEntry * sp = itrSpell->Get();
		SpellEntry * spz;
		//Case SPELL_AURA_PERIODIC_TRIGGER_SPELL
		for( uint8 i = 0 ; i < 3 ; i++ )
		{
			if ( sp->EffectApplyAuraName[i] == SPELL_AURA_PERIODIC_TRIGGER_SPELL )
			{
				spz = dbcSpell.LookupEntry( sp->EffectTriggerSpell[i] );
				if( spz != NULL ) 
				{
					if( sp->Dspell_coef_override >= 0 )
						spz->Dspell_coef_override = sp->Dspell_coef_override;
					else
					{
						//we must set bonus per tick on triggered spells now (i.e. Arcane Missiles)
						if( sp->ChannelInterruptFlags != 0 )
						{
							float Duration = float( GetDuration( dbcSpellDuration.LookupEntry( sp->DurationIndex ) ));
							float amp = float(sp->EffectAmplitude[i]);
							sp->fixed_dddhcoef = sp->fixed_hotdotcoef * amp / Duration;
						}			
						spz->fixed_dddhcoef = sp->fixed_dddhcoef;
					}

					if( sp->OTspell_coef_override >= 0 )
						spz->OTspell_coef_override = sp->OTspell_coef_override;
					else
					{
						//we must set bonus per tick on triggered spells now (i.e. Arcane Missiles)
						if( sp->ChannelInterruptFlags != 0 )
						{
							float Duration = float( GetDuration( dbcSpellDuration.LookupEntry( sp->DurationIndex ) ));
							float amp = float(sp->EffectAmplitude[i]);
							sp->fixed_hotdotcoef *= amp / Duration;
						}
						spz->fixed_hotdotcoef = sp->fixed_hotdotcoef;
					}
					break;
				}
			}
		}
	
		if(!itrSpell->Inc())
			break;
	}
	itrSpell->Destruct();
	*/

	/////////////////////////////////////////////////////////////////
	//SPELL COEFFICIENT SETTINGS END
	/////////////////////////////////////////////////////////////////
	//SpellEntry* sp;

	/********************************************************
	 * Windfury Enchantment
	 ********************************************************/
	//EnchantEntry* Enchantment;
		
	//Enchantment = dbcEnchant.LookupEntry( 283 );
	//if( Enchantment != NULL )
	//{
	//	Enchantment->spell[0] = 33757; //this is actually good
	//	sp = dbcSpell.LookupEntry( 33757 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK; //we do not need proc on spell ;)
	//		sp->EffectTriggerSpell[0] = 8232; //for the logs and rest
	//		sp->procChance = 20;
	//		sp->proc_interval = 3000;//http://www.wowwiki.com/Windfury_Weapon
	//		sp->maxstack = 1;
	//	}
	//}

	//Enchantment = dbcEnchant.LookupEntry( 284 );
	//if( Enchantment != NULL )
	//{
	//	Enchantment->spell[0] = 33756; 
	//	sp = dbcSpell.LookupEntry( 33756 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK; //we do not need proc on spell ;)
	//		sp->EffectTriggerSpell[0] = 8235; //for the logs and rest
	//		sp->procChance = 20;
	//		sp->proc_interval = 3000; //http://www.wowwiki.com/Windfury_Weapon
	//		sp->maxstack = 1;
	//	}
	//}

	//Enchantment = dbcEnchant.LookupEntry( 525 );
	//if( Enchantment != NULL )
	//{
	//	Enchantment->spell[0] = 33755; 
	//	sp = dbcSpell.LookupEntry( 33755 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK; //we do not need proc on spell ;)
	//		sp->EffectTriggerSpell[0] = 10486; //for the logs and rest
	//		sp->procChance = 20;
	//		sp->proc_interval = 3000;//http://www.wowwiki.com/Windfury_Weapon
	//		sp->maxstack = 1;
	//	}
	//}

	//Enchantment = dbcEnchant.LookupEntry( 1669 );
	//if( Enchantment != NULL )
	//{
	//	Enchantment->spell[0] = 33754; 
	//	sp = dbcSpell.LookupEntry( 33754 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK; //we do not need proc on spell ;)
	//		sp->EffectTriggerSpell[0] = 16362; //for the logs and rest
	//		sp->procChance = 20;
	//		sp->proc_interval = 3000;//http://www.wowwiki.com/Windfury_Weapon
	//		sp->maxstack = 1;
	//	}
	//}

	//Enchantment = dbcEnchant.LookupEntry( 2636 );
	//if( Enchantment != NULL )
	//{
	//	Enchantment->spell[0] = 33727; 
	//	sp = dbcSpell.LookupEntry( 33727 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK; //we do not need proc on spell ;)
	//		sp->EffectTriggerSpell[0] = 25505; //for the logs and rest
	//		sp->procChance = 20;
	//		sp->proc_interval = 3000;//http://www.wowwiki.com/Windfury_Weapon
	//		sp->maxstack = 1;
	//	}
	//}

	///**********************************************************
	// *	PROFFESION - Enchant Cloak - Major Resistance
	// **********************************************************/
	//sp = dbcSpell.LookupEntry( 27962 );
	//if( sp != NULL )
	//	sp->EffectMiscValue[0] = 2998;
	//sp = dbcSpell.LookupEntry( 36285 );
	//if( sp != NULL )
	//	sp->EffectMiscValue[0] = 2998;

	///**********************************************************
	// *	Wand Shoot
	// **********************************************************/
	//sp = dbcSpell.LookupEntry( 5019 );
	//if( sp != NULL )
	//	sp->SpellGroupType = 134217728;

	///**********************************************************
	// * Clearcasting
	// **********************************************************/
	//sp = dbcSpell.LookupEntry( 12536 );
	//if( sp != NULL )
	//{
	//	sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//	sp->EffectMiscValue[0] = SMT_COST;
	//	sp->EffectSpellGroupRelation[0] = 0xFFFFFFFF; //all possible spells we can affect
	//}
	//sp = dbcSpell.LookupEntry( 16246 );
	//if( sp != NULL )
	//{
	//	sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//	sp->EffectMiscValue[0] = SMT_COST;
	//	sp->EffectSpellGroupRelation[0] = 0xFFFFFFFF; //all possible spells we can affect
	//}
	//sp = dbcSpell.LookupEntry( 16870 );
	//if( sp != NULL )
	//{
	//	sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//	sp->EffectMiscValue[0] = SMT_COST;
	//	sp->EffectSpellGroupRelation[0] = 0xFFFFFFFF; //all possible spells we can affect
	//}
	//sp = dbcSpell.LookupEntry( 34754 );
	//if( sp != NULL )
	//{
	//	sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//	sp->EffectMiscValue[0] = SMT_COST;
	//	sp->EffectSpellGroupRelation[0] = 0xFFFFFFFF; //all possible spells we can affect
	//}

	///**********************************************************
	// * Berserking - TROLL'S RACIAL SPELL
	// **********************************************************/
	//sp = dbcSpell.LookupEntry( 20554 );
	//if( sp != NULL )
	//{
	//	sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//	sp->EffectTriggerSpell[0] = 26635;
	//}
	//sp = dbcSpell.LookupEntry( 26296 );
	//if( sp != NULL )
	//{
	//	sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//	sp->EffectTriggerSpell[0] = 26635;
	//}
	//sp = dbcSpell.LookupEntry( 26297 );
	//if( sp != NULL )
	//{
	//	sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//	sp->EffectTriggerSpell[0] = 26635;
	//}

	//sp = dbcSpell.LookupEntry( 20134 ); // << --- WTF?
	//if( sp != NULL )
	//	sp->procChance = 50;

	///**********************************************************
	// * Mana Tap - BLOOD ELF RACIAL
	// **********************************************************/
	//sp = dbcSpell.LookupEntry( 28734 );
	//if( sp != NULL )
	//	sp->Effect[0] = SPELL_EFFECT_POWER_BURN; // should be Power Burn, not Power Drain. Power Drain leeches mana which is incorrect.

	///**********************************************************
	// * thrown - add a 1.6 second cooldown
	// **********************************************************/
	//const static uint32 thrown_spells[] = {SPELL_RANGED_GENERAL,SPELL_RANGED_THROW,SPELL_RANGED_WAND, 26679, 27084, 29436, 37074, 41182, 41346, 0};
	//for(i = 0; thrown_spells[i] != 0; ++i)
	//{
	//	sp = dbcSpell.LookupEntry( thrown_spells[i] );
	//	if( sp != NULL && sp->RecoveryTime==0 && sp->StartRecoveryTime == 0 )
	//		sp->RecoveryTime = 1600;
	//}

	///**********************************************************
	// * Wands
	// **********************************************************/
	//sp = dbcSpell.LookupEntry( SPELL_RANGED_WAND );
	//if( sp != NULL )
	//	sp->Spell_Dmg_Type = SPELL_DMG_TYPE_RANGED;

	///**********************************************************
	// * Misc stuff (questfixes etc)
	// **********************************************************/

	//sp = dbcSpell.LookupEntry( 30877 );
	//if( sp != NULL )
	//{
	//	sp->EffectImplicitTargetB[0]=0;
	//}

	//sp = dbcSpell.LookupEntry(23179);
	//if( sp != NULL )
	//	sp->EffectMiscValue[0] = 1434;

	////////////////////////////////////////////////////////
	//// CLASS-SPECIFIC SPELL FIXES						//
	////////////////////////////////////////////////////////

	///* Note: when applying spell hackfixes, please follow a template */

	////////////////////////////////////////////
	//// CLASS_WARRIOR								//
	////////////////////////////////////////////

	//// Insert warrior spell fixes here

	//	//Warrior - Retaliation
	//	sp = dbcSpell.LookupEntry( 20230 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; //aura
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 22858; //evil , but this is good for us :D
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM; //add procflag here since this was not processed with the others !
	//	}

	//	//"bloodthirst" new version is ok but old version is wrong from now on :(
	//	sp = dbcSpell.LookupEntry( 23881 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64; //cast on us, it is good
	//		sp->EffectTriggerSpell[1] = 23885; //evil , but this is good for us :D
	//	}
	//	sp = dbcSpell.LookupEntry( 23892 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64;
	//		sp->EffectTriggerSpell[1] = 23886; //evil , but this is good for us :D
	//	}
	//	sp = dbcSpell.LookupEntry( 23893 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64; //
	//		sp->EffectTriggerSpell[1] = 23887; //evil , but this is good for us :D
	//	}
	//	sp = dbcSpell.LookupEntry( 23894 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64; //
	//		sp->EffectTriggerSpell[1] = 23888; //evil , but this is good for us :D
	//	}
	//	sp = dbcSpell.LookupEntry( 25251 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64; //aura
	//		sp->EffectTriggerSpell[1] = 25252; //evil , but this is good for us :D
	//	}
	//	sp = dbcSpell.LookupEntry( 30335 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64; //aura
	//		sp->EffectTriggerSpell[1] = 30339; //evil , but this is good for us :D
	//	}
	//	//rend
	//	sp = dbcSpell.LookupEntry( 772 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 6546 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 6547 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 6548 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 11572 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 11573 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 11574 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 25208 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;

	//	//warrior - Rampage
	//	sp = dbcSpell.LookupEntry( 29801 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK | PROC_TARGET_SELF;
	//		sp->EffectTriggerSpell[0] = sp->EffectTriggerSpell[1];
	//	}
	//	sp = dbcSpell.LookupEntry( 30030 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK | PROC_TARGET_SELF;
	//		sp->EffectTriggerSpell[0] = sp->EffectTriggerSpell[1];
	//	}
	//	sp = dbcSpell.LookupEntry( 30033 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK | PROC_TARGET_SELF;
	//		sp->EffectTriggerSpell[0] = sp->EffectTriggerSpell[1];
	//	}
	//#ifdef NEW_PROCFLAGS
	//	//warrior - deep wounds
	//	sp = dbcSpell.LookupEntry( 12162);
	//	if ( sp!=NULL )
	//		sp->SpellGroupType = 32;
	//	sp = dbcSpell.LookupEntry( 12850);
	//	if ( sp!=NULL )
	//		sp->SpellGroupType = 32;
	//	sp = dbcSpell.LookupEntry( 12868);
	//	if ( sp!=NULL )
	//		sp->SpellGroupType = 32;
	//#endif

	//	//warrior - second wind should trigger on self
	//	sp = dbcSpell.LookupEntry( 29841 );
	//	if( sp != NULL )
	//		sp->procFlags |= PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 29842 );
	//	if( sp != NULL )
	//		sp->procFlags |= PROC_TARGET_SELF;

	//	//warrior - Improved Disciplines
	//	sp = dbcSpell.LookupEntry( 29725 );
	//	if (sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16 | 8192;
	//		sp->EffectSpellGroupRelation_high[0] = 8;
	//		sp->EffectSpellGroupRelation[1] = 16 | 8192;
	//		sp->EffectSpellGroupRelation_high[1] = 8;
	//	}
	//	sp = dbcSpell.LookupEntry( 29724 );
	//	if (sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16 | 8192;
	//		sp->EffectSpellGroupRelation_high[0] = 8;
	//		sp->EffectSpellGroupRelation[1] = 16 | 8192;
	//		sp->EffectSpellGroupRelation_high[1] = 8;
	//	}
	//	sp = dbcSpell.LookupEntry( 29723 );
	//	if (sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16 | 8192;
	//		sp->EffectSpellGroupRelation_high[0] = 8;
	//		sp->EffectSpellGroupRelation[1] = 16 | 8192;
	//		sp->EffectSpellGroupRelation_high[1] = 8;
	//	}

	//	//warrior - berserker rage is missing 1 effect = regenerate rage
	//	sp = dbcSpell.LookupEntry( 18499 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[2] = 6;
	//		sp->EffectApplyAuraName[2] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[2] = 37521; //not sure if this is the one. In my time this adds 30 rage
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM | PROC_TARGET_SELF;
	//	}

	//	//warrior - improved berserker rage
	//	sp = dbcSpell.LookupEntry( 20500 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 20501 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL | PROC_TARGET_SELF;

	//	//warrior - berserker rage is missing 1 effect = regenerate rage
	//	sp = dbcSpell.LookupEntry( 18499 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[2] = 6;
	//		sp->EffectApplyAuraName[2] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[2] = 9174; //not sure if this is the one. In my time this adds 30 rage
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM | PROC_TARGET_SELF;
	//	}

	//	//warrior - Blood Frenzy
	//	sp = dbcSpell.LookupEntry( 29836 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 29859 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//	}

	//	//warrior - Unbridled Wrath
	//	sp = dbcSpell.LookupEntry( 12322 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_MELEE_ATTACK | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 12999 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_MELEE_ATTACK | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 13000 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_MELEE_ATTACK | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 13001 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_MELEE_ATTACK | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 13002 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_MELEE_ATTACK | PROC_TARGET_SELF;

	//	//warrior - Commanding Presence
	//	sp = dbcSpell.LookupEntry( 12318 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 ;
	//		sp->EffectSpellGroupRelation_high[0] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 12857 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 ;
	//		sp->EffectSpellGroupRelation_high[0] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 12858 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 ;
	//		sp->EffectSpellGroupRelation_high[0] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 12860 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 ;
	//		sp->EffectSpellGroupRelation_high[0] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 12861 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 ;
	//		sp->EffectSpellGroupRelation_high[0] = 128;
	//	}

	//	//warrior - Booming Voice
	//	sp = dbcSpell.LookupEntry( 12321 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 | 131072;
	//		sp->EffectSpellGroupRelation_high[0] = 128;
	//		sp->EffectSpellGroupRelation[1] = 65536 | 131072;
	//		sp->EffectSpellGroupRelation_high[1] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 12835 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 | 131072;
	//		sp->EffectSpellGroupRelation_high[0] = 128;
	//		sp->EffectSpellGroupRelation[1] = 65536 | 131072;
	//		sp->EffectSpellGroupRelation_high[1] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 12836 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 | 131072;
	//		sp->EffectSpellGroupRelation_high[0] = 128;
	//		sp->EffectSpellGroupRelation[1] = 65536 | 131072;
	//		sp->EffectSpellGroupRelation_high[1] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 12837 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 | 131072;
	//		sp->EffectSpellGroupRelation_high[0] = 128;
	//		sp->EffectSpellGroupRelation[1] = 65536 | 131072;
	//		sp->EffectSpellGroupRelation_high[1] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 12838 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 | 131072;
	//		sp->EffectSpellGroupRelation_high[0] = 128;
	//		sp->EffectSpellGroupRelation[1] = 65536 | 131072;
	//		sp->EffectSpellGroupRelation_high[1] = 128;
	//	}

	//	//warrior - Improved Intercept
	//	sp = dbcSpell.LookupEntry( 29888 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1073741824;
	//	sp = dbcSpell.LookupEntry( 29889 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1073741824;

	//	//warrior - Improved Mortal Strike

	//	sp = dbcSpell.LookupEntry( 35446 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[1] = 33554432;
	//	sp->EffectSpellGroupRelation[0] = 33554432;
	//	}
	//	sp = dbcSpell.LookupEntry( 35448 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[1] = 33554432;
	//	sp->EffectSpellGroupRelation[0] = 33554432;
	//	}
	//	sp = dbcSpell.LookupEntry( 35449 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[1] = 33554432;
	//	sp->EffectSpellGroupRelation[0] = 33554432;
	//	}
	//	sp = dbcSpell.LookupEntry( 35450 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[1] = 33554432;
	//	sp->EffectSpellGroupRelation[0] = 33554432;
	//	}
	//	sp = dbcSpell.LookupEntry( 35451 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[1] = 33554432;
	//	sp->EffectSpellGroupRelation[0] = 33554432;
	//	}
	//	//watrior - Improved Thudner Clap
	//	sp = dbcSpell.LookupEntry( 12287 );
	//	if ( sp != NULL )
	//		sp->EffectSpellGroupRelation[1] = 128;
	//	sp = dbcSpell.LookupEntry( 12665 );
	//	if ( sp != NULL )
	//		sp->EffectSpellGroupRelation[1] = 128;	
	//	sp = dbcSpell.LookupEntry( 12666 );
	//	if ( sp != NULL )
	//		sp->EffectSpellGroupRelation[1] = 128;
	//	//warrior - Tactical Mastery
	//	sp = dbcSpell.LookupEntry( 12677 );
	//	if ( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[1] = 1024;
	//		sp->EffectSpellGroupRelation[2] =33554432;
	//	}
	//	sp = dbcSpell.LookupEntry( 12676 );
	//	if ( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[1] = 1024;
	//		sp->EffectSpellGroupRelation[2] =33554432;
	//	}
	//	sp = dbcSpell.LookupEntry( 12295 );
	//	if ( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[1] = 1024;
	//		sp->EffectSpellGroupRelation[2] =33554432;
	//	}

	//	//warrior - Focused Rage
	//	sp = dbcSpell.LookupEntry( 29787 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8 | 1 | 4 | 32 | 2 | 4194304 | 536870912 | 2097152 | 128 | 134217728 | 1073741824UL | 2048 | 64 | 1024 | 33554432;
	//		sp->EffectSpellGroupRelation_high[0] = 8 | 256 | 64;
	//	}
	//	sp = dbcSpell.LookupEntry( 29790 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8 | 1 | 4 | 32 | 2 | 4194304 | 536870912 | 2097152 | 128 | 134217728 | 1073741824UL | 2048 | 64 | 1024 | 33554432;
	//		sp->EffectSpellGroupRelation_high[0] = 8 | 256 | 64;
	//	}
	//	sp = dbcSpell.LookupEntry( 29792 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8 | 1 | 4 | 32 | 2 | 4194304 | 536870912 | 2097152 | 128 | 134217728 | 1073741824UL | 2048 | 64 | 1024 | 33554432;
	//		sp->EffectSpellGroupRelation_high[0] = 8 | 256 | 64;
	//	}

	//	//warrior - impale
	//	sp = dbcSpell.LookupEntry( 16493 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8 | 1 | 4 | 32 | 2 | 4194304 | 536870912 | 2097152 | 128 | 134217728 | 1073741824 | 2048 | 64 | 1024;
	//	sp = dbcSpell.LookupEntry( 16494 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8 | 1 | 4 | 32 | 2 | 4194304 | 536870912 | 2097152 | 128 | 134217728 | 1073741824 | 2048 | 64 | 1024;

	//	//warrior - Improved Whirlwind
	//	sp = dbcSpell.LookupEntry( 29721 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation_high[0] = 4;
	//	sp = dbcSpell.LookupEntry( 29776 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation_high[0] = 4;

	//	//warrior - Improved Commanding Shout
	//	sp = dbcSpell.LookupEntry( 38408 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation_high[0] = 128;

	////////////////////////////////////////////
	//// PALADIN								//
	////////////////////////////////////////////

	//// Insert paladin spell fixes here

	//	// Seal of Command - Holy damage, but melee mechanics (crit damage, chance, etc)
	//	sp = dbcSpell.LookupEntry( 20424 );
	//	if( sp != NULL )
	//		sp->is_melee_spell = true;

	//	/**********************************************************
	//	 *	Blessing of Light
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 19977 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_DUMMY;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_DUMMY;
	//	}
	//	sp = dbcSpell.LookupEntry( 19978 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_DUMMY;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_DUMMY;
	//	}
	//	sp = dbcSpell.LookupEntry( 19979 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_DUMMY;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_DUMMY;
	//	}
	//	sp = dbcSpell.LookupEntry( 27144 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_DUMMY;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_DUMMY;
	//	}
	//	sp = dbcSpell.LookupEntry( 32770 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_DUMMY;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_DUMMY;
	//	}
	//	/**********************************************************
	//	 * Improved Righteous Fury
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 20468 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1;
	//		sp->EffectSpellGroupRelation[1] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 20469 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1;
	//		sp->EffectSpellGroupRelation[1] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 20470 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1;
	//		sp->EffectSpellGroupRelation[1] = 1;
	//	}

	//	/**********************************************************
	//	 * Healing Light
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 20237 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 0x40000000 | 0x80000000;
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 0x40000000 | 0x80000000;
	//	sp = dbcSpell.LookupEntry( 20239 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 0x40000000 | 0x80000000;

	//	/**********************************************************
	//	 * Improved Devotion Aura
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 20142 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64;
	//	sp = dbcSpell.LookupEntry( 20141 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64;
	//	sp = dbcSpell.LookupEntry( 20140 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64;
	//	sp = dbcSpell.LookupEntry( 20139 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64;
	//	sp = dbcSpell.LookupEntry( 20138 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64;

	//	/**********************************************************
	//	 * Guardian's Favor
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 20175 );
	//	if (sp != NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0]=128;
	//		sp->EffectSpellGroupRelation[1]=268435472;
	//	}
	//	sp = dbcSpell.LookupEntry( 20174 );
	//	if (sp != NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0]=128;
	//		sp->EffectSpellGroupRelation[1]=268435472;
	//	}

	//	/**********************************************************
	//	 * Improved Concentration Aura
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 20256 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=131072;
	//	sp = dbcSpell.LookupEntry( 20255 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=131072;
	//	sp = dbcSpell.LookupEntry( 20254 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=131072;

	//	/**********************************************************
	//	 * Sacred Duty
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 31849 );
	//	if (sp != NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0]=4194304;
	//		sp->EffectSpellGroupRelation[1]=4194304;
	//	}
	//	sp = dbcSpell.LookupEntry( 31848 );
	//	if (sp != NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0]=4194304;
	//		sp->EffectSpellGroupRelation[1]=4194304;
	//	}

	//	/**********************************************************
	//	 * Aura Mastery
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 31821 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 131072 | 67108864 | 8 | 64;
	//	
	//	/**********************************************************
	//	 * Sanctified Light
	//	 **********************************************************/
	//	group_relation_paladin_sanctified_light = 0x80000000; // <-- Grouping
	//	sp = dbcSpell.LookupEntry( 20359 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_paladin_sanctified_light;
	//	sp = dbcSpell.LookupEntry( 20360 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_paladin_sanctified_light;
	//	sp = dbcSpell.LookupEntry( 20361 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_paladin_sanctified_light;

	//	/**********************************************************
	//	 * Improved Seal of the Crusader
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 20335 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 536870912;
	//	sp = dbcSpell.LookupEntry( 20336 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 536870912;
	//	sp = dbcSpell.LookupEntry( 20337 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 536870912;
	//	sp = dbcSpell.LookupEntry( 28852 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 536870912;
	//	sp = dbcSpell.LookupEntry( 33557 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 536870912;
	//	
	//	/**********************************************************
	//	 * Light's Grace
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 31834 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 0x80000000;

	//	/**********************************************************
	//	 * Stoicism
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 31844 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[1] = 0xFFFFFFFF;
	//	sp = dbcSpell.LookupEntry( 31845 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[1] = 0xFFFFFFFF;

	//	/**********************************************************
	//	 * Fanaticism
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 31879 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1048576 | 524288 | 1024 | 536870912 | 524288;
	//	sp = dbcSpell.LookupEntry( 31880 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1048576 | 524288 | 1024 | 536870912 | 524288;
	//	sp = dbcSpell.LookupEntry( 31881 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1048576 | 524288 | 1024 | 536870912 | 524288;
	//	sp = dbcSpell.LookupEntry( 31882 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1048576 | 524288 | 1024 | 536870912 | 524288;
	//	sp = dbcSpell.LookupEntry( 31883 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1048576 | 524288 | 1024 | 536870912 | 524288;

	//	/**********************************************************
	//	 * Seal of Vengeance
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 31801 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//		sp->EffectTriggerSpell[0] = 31803;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//	}

	//	/**********************************************************
	//	 * Reckoning
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 20177 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 20179 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 20180 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 20181 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 20182 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM | PROC_TARGET_SELF;

	//	/**********************************************************
	//	 * Reckoning Effect
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 20178 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance = 100;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK | PROC_TARGET_SELF;
	//	}

	//	/**********************************************************
	//	 * Judgement of Wisdom
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 20186 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM;
	//		sp->EffectTriggerSpell[0] = 20268;
	//	}
	//	sp = dbcSpell.LookupEntry( 20354 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM;
	//		sp->EffectTriggerSpell[0] = 20352;
	//	}
	//	sp = dbcSpell.LookupEntry( 20355 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM;
	//		sp->EffectTriggerSpell[0] = 20353;
	//	}
	//	sp = dbcSpell.LookupEntry( 27164 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM;
	//		sp->EffectTriggerSpell[0] = 27165;
	//	}

	//	sp = dbcSpell.LookupEntry( 20268 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SINGLE_ENEMY;
	//	sp = dbcSpell.LookupEntry( 20352 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SINGLE_ENEMY;
	//	sp = dbcSpell.LookupEntry( 20353 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SINGLE_ENEMY;
	//	sp = dbcSpell.LookupEntry( 27165 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SINGLE_ENEMY;

	//	/**********************************************************
	//	 * Judgement of Light
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 20185 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->EffectTriggerSpell[0] = 20267;
	//	}
	//	sp = dbcSpell.LookupEntry( 20344 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->EffectTriggerSpell[0] = 20341;
	//	}
	//	sp = dbcSpell.LookupEntry( 20345 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->EffectTriggerSpell[0] = 20342;
	//	}
	//	sp = dbcSpell.LookupEntry( 20346 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->EffectTriggerSpell[0] = 20343;
	//	}
	//	sp = dbcSpell.LookupEntry( 27162 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->EffectTriggerSpell[0] = 27163;
	//	}
	//	sp = dbcSpell.LookupEntry( 20267 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SINGLE_ENEMY;
	//	sp = dbcSpell.LookupEntry( 20341 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SINGLE_ENEMY;
	//	sp = dbcSpell.LookupEntry( 20342 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SINGLE_ENEMY;
	//	sp = dbcSpell.LookupEntry( 20343 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SINGLE_ENEMY;
	//	sp = dbcSpell.LookupEntry( 27163 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SINGLE_ENEMY;

	//	/**********************************************************
	//	 * Eye for an Eye
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 9799 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT_VICTIM;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 25997;
	//	}
	//	sp = dbcSpell.LookupEntry( 25988 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT_VICTIM;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 25997;
	//	}

	//	/**********************************************************
	//	 * sanctified judgement
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 31876 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 31930;
	//	}
	//	sp = dbcSpell.LookupEntry( 31877 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 31930;
	//	}
	//	sp = dbcSpell.LookupEntry( 31878 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 31930;
	//		//sp->procChance=100;
	//	}

	//	/**********************************************************
	//	 * Blessed Life
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 31828 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM;
	//		sp->EffectTriggerSpell[0] = 31828;
	//	}
	//	sp = dbcSpell.LookupEntry( 31829 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM;
	//		sp->EffectTriggerSpell[0] = 31828;
	//	}
	//	sp = dbcSpell.LookupEntry( 31830 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ANY_DAMAGE_VICTIM;
	//		sp->EffectTriggerSpell[0] = 31828;
	//	}

	//	/**********************************************************
	//	 * Light's Grace
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 31833 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	sp = dbcSpell.LookupEntry( 31835 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	sp = dbcSpell.LookupEntry( 31836 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;

	//	/**********************************************************
	//	 * Holy Shield
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 20925 ); // -- rank 1
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_DAMAGE;
	//		sp->procFlags = PROC_ON_BLOCK_VICTIM;
	//		sp->procChance = 100;
	//	}
	//	sp = dbcSpell.LookupEntry( 20927 ); // -- rank 2
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_DAMAGE;
	//		sp->procFlags = PROC_ON_BLOCK_VICTIM;
	//		sp->procChance = 100;
	//	}
	//	sp = dbcSpell.LookupEntry( 20928 ); // -- rank 3
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_DAMAGE;
	//		sp->procFlags = PROC_ON_BLOCK_VICTIM;
	//		sp->procChance = 100;
	//	}
	//	sp = dbcSpell.LookupEntry( 27179 ); // -- rank 4
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_DAMAGE;
	//		sp->procFlags = PROC_ON_BLOCK_VICTIM;
	//		sp->procChance = 100;
	//	}
	//	//Paladin: Seal of Wisdom
	//	uint32 procchance = 0;
	//	sp = dbcSpell.LookupEntry( 27116 );
	//	if( sp != NULL )
	//		procchance = sp->procChance;
	//	sp = dbcSpell.LookupEntry( 20166 );
	//	if( sp != NULL )
	//		sp->procChance = procchance;
	//	sp = dbcSpell.LookupEntry( 20356 );
	//	if( sp != NULL )
	//		sp->procChance = procchance;
	//	sp = dbcSpell.LookupEntry( 20357 );
	//	if( sp != NULL )
	//		sp->procChance = procchance;
	//	sp = dbcSpell.LookupEntry( 27166 );
	//	if( sp != NULL )
	//		sp->procChance = procchance;
	//	//paladin - seal of blood
	//	sp = dbcSpell.LookupEntry( 31892 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 31893;
	//	}
	//	sp = dbcSpell.LookupEntry( 38008 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 31893;
	//	}

	//	//paladin - Spiritual Attunement 
	//	sp = dbcSpell.LookupEntry( 31785 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_HIT_VICTIM | PROC_TARGET_SELF ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 31786;
	//	}
	//	sp = dbcSpell.LookupEntry( 33776 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_HIT_VICTIM | PROC_TARGET_SELF;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 31786;
	//	}

	//	//Seal of Justice -lowered proc chance (experimental values !)
	//	sp = dbcSpell.LookupEntry( 20164 );
	//	if( sp != NULL )
	//		sp->procChance = 20;
	//	sp = dbcSpell.LookupEntry( 31895 );
	//	if( sp != NULL )
	//		sp->procChance = 20;

	//	// paladin - Improved Sanctity Aura
	//	sp = dbcSpell.LookupEntry( 31869 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 67108864;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//	}
	//	sp = dbcSpell.LookupEntry( 31870 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 67108864;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//	}
	//	//Paladin - Improved Lay on Hands
	//	sp = dbcSpell.LookupEntry( 20234 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	sp = dbcSpell.LookupEntry( 20235 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;

	////////////////////////////////////////////
	//// HUNTER								//
	////////////////////////////////////////////

	//// Insert hunter spell fixes here

	//	// Hunter - Master Tactician
	//	sp = dbcSpell.LookupEntry( 34506 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_RANGED_ATTACK | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 34507 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_RANGED_ATTACK | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 34508 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_RANGED_ATTACK | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 34838 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_RANGED_ATTACK | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 34839 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_RANGED_ATTACK | PROC_TARGET_SELF;

	//	// Hunter - Spirit Bond
	//	sp = dbcSpell.LookupEntry( 19578 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 19579;
	//	}
	//	sp = dbcSpell.LookupEntry( 20895 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 24529;
	//	}
	//	sp = dbcSpell.LookupEntry( 19579 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[1] = SPELL_EFFECT_APPLY_AURA; //we should do the same for player too as we did for pet
	//		sp->EffectApplyAuraName[1] = sp->EffectApplyAuraName[0];
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->EffectBasePoints[1] = sp->EffectBasePoints[0];
	//		sp->EffectAmplitude[1] = sp->EffectAmplitude[0];
	//		sp->EffectDieSides[1] = sp->EffectDieSides[0];
	//	}
	//	sp = dbcSpell.LookupEntry( 24529 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[1] = SPELL_EFFECT_APPLY_AURA; //we should do the same for player too as we did for pet
	//		sp->EffectApplyAuraName[1] = sp->EffectApplyAuraName[0];
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->EffectBasePoints[1] = sp->EffectBasePoints[0];
	//		sp->EffectAmplitude[1] = sp->EffectAmplitude[0];
	//		sp->EffectDieSides[1] = sp->EffectDieSides[0];
	//	}

	//	//Hunter Silencing Shot
	//	//http://www.naxxramas.net/bug_list/showreport.php?bugid=234 NTY
	//	sp = dbcSpell.LookupEntry(34490);
	//	if(sp != NULL)
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_SILENCE ;
	//	}

	//	// Hunter - Animal Handler
	//	sp = dbcSpell.LookupEntry( 34453 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_HIT_CHANCE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 34454 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_HIT_CHANCE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//	}

	//	// Hunter - Catlike Reflexes
	//	sp = dbcSpell.LookupEntry( 34462 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[1] = sp->EffectApplyAuraName[0];
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 34464 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[1] = sp->EffectApplyAuraName[0];
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 34465 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[1] = sp->EffectApplyAuraName[0];
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//	}

	//	// Hunter - Serpent's Swiftness
	//	sp = dbcSpell.LookupEntry( 34466 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_HASTE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 34467 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_HASTE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 34468 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_HASTE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 34469 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_HASTE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 34470 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_HASTE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//	}

	//	// Hunter - Ferocious Inspiration
	//	sp = dbcSpell.LookupEntry( 34455 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectTriggerSpell[0] = 34456;
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT | PROC_TARGET_SELF; //maybe target master ?
	//		sp->Effect[1] = 0; //remove this
	//	}
	//	sp = dbcSpell.LookupEntry( 34459 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectTriggerSpell[0] = 34456;
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT | PROC_TARGET_SELF; 
	//		sp->Effect[1] = 0; //remove this
	//	}
	//	sp = dbcSpell.LookupEntry( 34460 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectTriggerSpell[0] = 34456;
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT | PROC_TARGET_SELF;
	//		sp->Effect[1] = 0; //remove this
	//	}

	//	// Hunter - Focused Fire
	//	sp = dbcSpell.LookupEntry( 35029 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 35060;
	//		sp->EffectSpellGroupRelation_high[1] = 2048;
	//	}
	//	sp = dbcSpell.LookupEntry( 35030 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 35061;
	//		sp->EffectSpellGroupRelation_high[1] = 2048;
	//	}
	//	sp = dbcSpell.LookupEntry( 35060 );
	//	if( sp != NULL )
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//	sp = dbcSpell.LookupEntry( 35061 );
	//	if( sp != NULL )
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;

	//	// Hunter - Thick Hide
	//	sp = dbcSpell.LookupEntry( 19609 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_RESISTANCE; //we do not support armor rating for pets yet !
	//		sp->EffectBasePoints[0] *= 10; //just give it a little juice :P
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19610 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_RESISTANCE; //we do not support armor rating for pets yet !
	//		sp->EffectBasePoints[0] *= 10; //just give it a little juice :P
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19612 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_RESISTANCE; //we do not support armor rating for pets yet !
	//		sp->EffectBasePoints[0] *= 10; //just give it a little juice :P
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}

	//	// Hunter - Ferocity
	//	sp = dbcSpell.LookupEntry( 19612 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_CRIT_PERCENT;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19599 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_CRIT_PERCENT;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19600 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_CRIT_PERCENT;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19601 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_CRIT_PERCENT;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19602 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_CRIT_PERCENT;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}

	//	// Hunter - Bestial Swiftness
	//	sp = dbcSpell.LookupEntry( 19596 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_INCREASE_SPEED; 
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}

	//	// Hunter - Endurance Training
	//	sp = dbcSpell.LookupEntry( 19583 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_INCREASE_HEALTH_PERCENT;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19584 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_INCREASE_HEALTH_PERCENT;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19585 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_INCREASE_HEALTH_PERCENT;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19586 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_INCREASE_HEALTH_PERCENT;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19587 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_INCREASE_HEALTH_PERCENT;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}

	//	// Hunter - Thrill of the Hunt
	//	sp = dbcSpell.LookupEntry( 34497 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT | PROC_TARGET_SELF;
	//		sp->procChance = sp->EffectBasePoints[0]+1;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 34720;
	//	}
	//	sp = dbcSpell.LookupEntry( 34498 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT | PROC_TARGET_SELF;
	//		sp->procChance = sp->EffectBasePoints[0]+1;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 34720;
	//	}
	//	sp = dbcSpell.LookupEntry( 34499 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT | PROC_TARGET_SELF;
	//		sp->procChance = sp->EffectBasePoints[0]+1;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 34720;
	//	}

	//	// Hunter - Expose Weakness
	//	sp = dbcSpell.LookupEntry( 34500 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_RANGED_CRIT_ATTACK;
	//	sp = dbcSpell.LookupEntry( 34502 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_RANGED_CRIT_ATTACK;
	//	sp = dbcSpell.LookupEntry( 34503 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_RANGED_CRIT_ATTACK;

	//	// Hunter - Hawk Eye
	//	sp = dbcSpell.LookupEntry( 19498 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//	sp = dbcSpell.LookupEntry( 19499 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//	sp = dbcSpell.LookupEntry( 19500 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;

	//	//Hunter - Frenzy
	//	sp = dbcSpell.LookupEntry( 19621 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 19615;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->procChance = sp->EffectBasePoints[0];
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET | SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET | PROC_TARGET_SELF;
	//	}
	//	sp = dbcSpell.LookupEntry( 19622 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 19615;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->procChance = sp->EffectBasePoints[0];
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET | SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET | PROC_TARGET_SELF;
	//	}
	//	sp = dbcSpell.LookupEntry( 19623 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 19615;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->procChance = sp->EffectBasePoints[0];
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET | SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET | PROC_TARGET_SELF;
	//	}
	//	sp = dbcSpell.LookupEntry( 19624 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 19615;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->procChance = sp->EffectBasePoints[0];
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET | SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET | PROC_TARGET_SELF;
	//	}
	//	sp = dbcSpell.LookupEntry( 19625 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 19615;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->procChance = sp->EffectBasePoints[0];
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET | SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET | PROC_TARGET_SELF;
	//	}

	//	//Hunter - Unleashed Fury
	//	sp = dbcSpell.LookupEntry( 19616 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectMiscValue[0] = 1; //tweekign melee dmg
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET | SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19617 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectMiscValue[0] = 1; //tweekign melee dmg
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET | SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19618 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectMiscValue[0] = 1; //tweekign melee dmg
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET | SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19619 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectMiscValue[0] = 1; //tweekign melee dmg
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET | SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 19620 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectMiscValue[0] = 1; //tweekign melee dmg
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET | SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET;
	//	}

	//	//Hunter : Pathfinding
	//	sp = dbcSpell.LookupEntry( 19559 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2097152;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//	}
	//	sp = dbcSpell.LookupEntry( 19560 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2097152;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//	}
	//	sp = dbcSpell.LookupEntry( 5131 );
	//	if( sp != NULL )
	//		sp->SpellGroupType = 2097152;
	//	sp = dbcSpell.LookupEntry( 13160 );
	//	if( sp != NULL )
	//		sp->SpellGroupType = 2097152;

	//	//Hunter : Rapid Killing - might need to add honor trigger too here. I'm guessing you receive Xp too so i'm avoiding double proc
	//	sp = dbcSpell.LookupEntry( 34948 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_GAIN_EXPIERIENCE | PROC_TARGET_SELF;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}
	//	sp = dbcSpell.LookupEntry( 34949 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_GAIN_EXPIERIENCE | PROC_TARGET_SELF;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}
	//	//Hunter : Rapid Killing - PROC
	//	sp = dbcSpell.LookupEntry( 35098 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 131072 | 2048 | 1;
	//	sp = dbcSpell.LookupEntry( 35099 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 131072 | 2048 | 1;

	//	//Hunter : Improved Stings
	//	sp = dbcSpell.LookupEntry( 19464 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16384 | 65536;
	//		sp->EffectSpellGroupRelation[1] = 65536;
	//		sp->EffectSpellGroupRelation[2] = 16384 | 65536 | 32768;
	//	}
	//	sp = dbcSpell.LookupEntry( 19465 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16384 | 65536;
	//		sp->EffectSpellGroupRelation[1] = 65536;
	//		sp->EffectSpellGroupRelation[2] = 16384 | 65536 | 32768;
	//	}
	//	sp = dbcSpell.LookupEntry( 19466 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16384 | 65536;
	//		sp->EffectSpellGroupRelation[1] = 65536;
	//		sp->EffectSpellGroupRelation[2] = 16384 | 65536 | 32768;
	//	}
	//	sp = dbcSpell.LookupEntry( 19467 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16384 | 65536;
	//		sp->EffectSpellGroupRelation[1] = 65536;
	//		sp->EffectSpellGroupRelation[2] = 16384 | 65536 | 32768;
	//	}
	//	sp = dbcSpell.LookupEntry( 19468 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16384 | 65536;
	//		sp->EffectSpellGroupRelation[1] = 65536;
	//		sp->EffectSpellGroupRelation[2] = 16384 | 65536 | 32768;
	//	}

	//	//we need to adress this somehow : shot
	//	sp = dbcSpell.LookupEntry( 3018 );
	//	if( sp != NULL )
	//		sp->SpellGroupType = 4;

	//	//Hunter : Mortal Shots
	//	sp = dbcSpell.LookupEntry( 19485 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 4 ; //simple shot
	//	sp = dbcSpell.LookupEntry( 19487 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 4 ; //simple shot
	//	sp = dbcSpell.LookupEntry( 19488 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 4 ; //simple shot
	//	sp = dbcSpell.LookupEntry( 19489 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 4 ; //simple shot
	//	sp = dbcSpell.LookupEntry( 19490 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 4 ; //simple shot

	//	//Hunter : Improved Barrage
	//	sp = dbcSpell.LookupEntry( 35104 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4096;
	//		sp->EffectSpellGroupRelation[1] = 8192;
	//	}
	//	sp = dbcSpell.LookupEntry( 35110 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4096;
	//		sp->EffectSpellGroupRelation[1] = 8192;
	//	}
	//	sp = dbcSpell.LookupEntry( 35111 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4096;
	//		sp->EffectSpellGroupRelation[1] = 8192;
	//	}

	//	//Hunter : Clever Traps
	//	sp = dbcSpell.LookupEntry( 19239 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16 | 8;
	//		sp->EffectSpellGroupRelation[0] = 4;
	//		sp->EffectSpellGroupRelation[0] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 19245 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16 | 8;
	//		sp->EffectSpellGroupRelation[0] = 4;
	//		sp->EffectSpellGroupRelation[0] = 128;
	//	}

	//	//Hunter : Resourcefulness
	//	sp = dbcSpell.LookupEntry( 34491 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 128 | 2 | 64; 
	//		sp->EffectSpellGroupRelation[1] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 34492 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 128 | 2 | 64; 
	//		sp->EffectSpellGroupRelation[1] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 34493 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 128 | 2 | 64; 
	//		sp->EffectSpellGroupRelation[1] = 128;
	//	}

	//	/*
	//	//Hunter : Entrapment
	//	sp = dbcSpell.LookupEntry( 19184 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	sp = dbcSpell.LookupEntry( 19387 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	sp = dbcSpell.LookupEntry( 19388 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	*/
	//	/* aspect of the pack - change to AA */
	//	sp = dbcSpell.LookupEntry( 13159 );
	//	if( sp != NULL )
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AREA_AURA;
	//	sp = dbcSpell.LookupEntry( 13159 );
	//	if( sp != NULL )
	//		sp->Effect[1] = SPELL_EFFECT_APPLY_AREA_AURA;

	//	/* aspect of the cheetah - add proc flags */
	//	sp = dbcSpell.LookupEntry( 5118 );
	//	if( sp != NULL )
	//		sp->procFlags = 139944;

	//	sp = dbcSpell.LookupEntry(34471);
	//	if(sp!=NULL)
	//	{
	//				sp->EffectApplyAuraName[0] = 72 ;
	//				sp->EffectApplyAuraName[1] = 79 ;
	//				sp->EffectApplyAuraName[2] = 77 ;
	//	}

	////////////////////////////////////////////
	//// CLASS_ROGUE								//
	////////////////////////////////////////////

	//// Insert rogue spell fixes here

	//	/**********************************************************
	//	 *	Garrote - this is used? 
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 37066 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SINGLE_ENEMY;
	//	}

	//	//rogue ( grouping ) Elusiveness = blind + vanish
	//	group_relation_rogue_elusiveness = 0x00000800 | 0x01000000;

	//	//rogue - Elusiveness
	//	sp = dbcSpell.LookupEntry( 13981 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_elusiveness;
	//	sp = dbcSpell.LookupEntry( 14066 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_elusiveness;

	//	//rogue ( grouping ) Poisons
	//	group_relation_rogue_poisons = 0x00002000 | 0x00004000 | 0x00008000 | 0x00010000 | 0x10000000;

	//	//rogue - Vile Poisons
	//	sp = dbcSpell.LookupEntry( 14168 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;
	//		sp->EffectSpellGroupRelation[1] = 0x00800000; //maybe this is mixed up with 0 grouprelation ?
	//		sp->EffectSpellGroupRelation[2] = group_relation_rogue_poisons;
	//	}
	//	sp = dbcSpell.LookupEntry( 16514 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;
	//		sp->EffectSpellGroupRelation[1] = 0x00800000; //maybe this is mixed up with 0 grouprelation ?
	//		sp->EffectSpellGroupRelation[2] = group_relation_rogue_poisons;
	//	}
	//	sp = dbcSpell.LookupEntry( 16515 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;
	//		sp->EffectSpellGroupRelation[1] = 0x00800000; //maybe this is mixed up with 0 grouprelation ?
	//		sp->EffectSpellGroupRelation[2] = group_relation_rogue_poisons;
	//	}
	//	sp = dbcSpell.LookupEntry( 16719 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;
	//		sp->EffectSpellGroupRelation[1] = 0x00800000; //maybe this is mixed up with 0 grouprelation ?
	//		sp->EffectSpellGroupRelation[2] = group_relation_rogue_poisons;
	//	}
	//	sp = dbcSpell.LookupEntry( 16720 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;
	//		sp->EffectSpellGroupRelation[1] = 0x00800000; //maybe this is mixed up with 0 grouprelation ?
	//		sp->EffectSpellGroupRelation[2] = group_relation_rogue_poisons;
	//	}

	//	//rogue - Improved Poisons
	//	sp = dbcSpell.LookupEntry( 14113 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;
	//	sp = dbcSpell.LookupEntry( 14114 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;
	//	sp = dbcSpell.LookupEntry( 14115 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;
	//	sp = dbcSpell.LookupEntry( 14116 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;
	//	sp = dbcSpell.LookupEntry( 14117 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;
	//	sp = dbcSpell.LookupEntry( 21881 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;

	//	//rogue - Improved Expose Armor
	//	sp = dbcSpell.LookupEntry( 14168 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 0x00080000;
	//	sp = dbcSpell.LookupEntry( 14169 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 0x00080000;

	//	//rogue - Master Poisoner.
	//	sp = dbcSpell.LookupEntry( 31226 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;
	//	sp = dbcSpell.LookupEntry( 31227 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_poisons;

	//	//rogue - Find Weakness.
	//	sp = dbcSpell.LookupEntry( 31233 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	sp = dbcSpell.LookupEntry( 31239 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	sp = dbcSpell.LookupEntry( 31240 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	sp = dbcSpell.LookupEntry( 31241 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	sp = dbcSpell.LookupEntry( 31242 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;

	//	//rogue ( grouping ) Find Weakness
	//	group_relation_rogue_find_weakness = 0x00000008 | 0x00000010 | 0x00000100 | 0x00100000 | 0x00800000 | 0x04000000 | 0x20000000;

	//	//rogue - Find Weakness. The effect
	//	sp = dbcSpell.LookupEntry( 31234 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_find_weakness;
	//	sp = dbcSpell.LookupEntry( 31235 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_find_weakness;
	//	sp = dbcSpell.LookupEntry( 31236 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_find_weakness;
	//	sp = dbcSpell.LookupEntry( 31237 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_find_weakness;
	//	sp = dbcSpell.LookupEntry( 31238 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_find_weakness;
	//	
	//	//rogue - Camouflage.
	//	sp = dbcSpell.LookupEntry( 13975 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4194304;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[1] = 4194304;
	//	}
	//	sp = dbcSpell.LookupEntry( 14062 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4194304;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[1] = 4194304;
	//	}
	//	sp = dbcSpell.LookupEntry( 14063 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4194304;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[1] = 4194304;
	//	}
	//	sp = dbcSpell.LookupEntry( 14064 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4194304;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[1] = 4194304;
	//	}
	//	sp = dbcSpell.LookupEntry( 14065 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4194304;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[1] = 4194304;
	//	}

	//	//rogue - Vanish : Second Trigger Spell
	//	sp = dbcSpell.LookupEntry( 18461 );
	//	if( sp != NULL )
	//		sp->AttributesEx |= ATTRIBUTESEX_NOT_BREAK_STEALTH;



	//	// rogue - Blind (Make it able to miss!)
	//	sp = dbcSpell.LookupEntry( 2094 );
	//	if( sp != NULL )
	//	{
	//		sp->Spell_Dmg_Type = SPELL_DMG_TYPE_RANGED;
	//		sp->is_ranged_spell = true; 
	//	}
	//	//rogue - Mace Specialization.
	//	sp = dbcSpell.LookupEntry( 13709 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//	sp = dbcSpell.LookupEntry( 13800 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//	sp = dbcSpell.LookupEntry( 13801 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//	sp = dbcSpell.LookupEntry( 13802 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//	sp = dbcSpell.LookupEntry( 13803 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;

	//	//rogue - Dirty Tricks 
	//	sp = dbcSpell.LookupEntry( 14076 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16777216 | 128; // blind + sap
	//		sp->EffectSpellGroupRelation[1] = 16777216 | 128; // blind + sap
	//	}
	//	sp = dbcSpell.LookupEntry( 14094 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 16777216 | 128; // blind + sap
	//		sp->EffectSpellGroupRelation[1] = 16777216 | 128; // blind + sap
	//	}

	//	//rogue - Dirty Deeds
	//	sp = dbcSpell.LookupEntry( 14082 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1024 | 256; // Cheap Shot + Garrote
	//	sp = dbcSpell.LookupEntry( 14083 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1024 | 256; // Cheap Shot + Garrote


	//	//rogue ( grouping ) Shadowstep
	//	group_relation_rogue_shadow_step |= 512;//rogue - ambush (only a part of the whole group since it would affect other spells too)
	//	group_relation_rogue_shadow_step |= 4;//rogue - Backstab (only a part of the whole group since it would affect other spells too)
	//	group_relation_rogue_shadow_step |= 256;//Garrote
	//	group_relation_rogue_shadow_step |= 536870912 | 16 | 8 | 8389120 | 41943040 | 33554432 | 32 | 67108864 | 64 | 128 ;

	//	//rogue - Shadowstep
	//	sp = dbcSpell.LookupEntry( 36563 ); 
	//	if( sp != NULL )
	//	{
	//		// Effect goes from 0-2, not 1-3... -.-
	//		sp->EffectSpellGroupRelation[2] = group_relation_rogue_shadow_step;
	//		sp->EffectSpellGroupRelation_high[2] = 256 | 128 ;
	//		sp->EffectMiscValue[1] = SMT_SPELL_VALUE;
	//	}
	//	// Still related to shadowstep - prevent the trigger spells from breaking stealth.
	//	sp = dbcSpell.LookupEntry( 44373 );
	//	if( sp )
	//		sp->AttributesEx |= ATTRIBUTESEX_NOT_BREAK_STEALTH;
	//	sp = dbcSpell.LookupEntry( 36563 );
	//	if( sp )
	//		sp->AttributesEx |= ATTRIBUTESEX_NOT_BREAK_STEALTH;
	//	sp = dbcSpell.LookupEntry( 36554 );
	//	if( sp != NULL )
	//		sp->AttributesEx |= ATTRIBUTESEX_NOT_BREAK_STEALTH;

	//	//rogue ( grouping ) Lethality
	//	group_relation_rogue_lethality |= 2;//rogue - Sinister Strike (only a part of the whole group since it would affect other spells too)
	//	group_relation_rogue_lethality |= 4;//rogue - backstab (only a part of the whole group since it would affect other spells too)
	//	group_relation_rogue_lethality |= 8;//rogue - Gouge (only a part of the whole group since it would affect other spells too)
	//	group_relation_rogue_lethality |= 33554432UL;//rogue - Hemorrhage (only a part of the whole group since it would affect other spells too)
	//	group_relation_rogue_lethality |= 536870912UL;//rogue - Shiv (only a part of the whole group since it would affect other spells too)
	//	group_relation_rogue_lethality |= 1073741824UL;//rogue - Ghostly Strike (only a part of the whole group since it would affect other spells too)

	//	//rogue Lethality
	//	sp = dbcSpell.LookupEntry( 14128 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_lethality;
	//	sp = dbcSpell.LookupEntry( 14132 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_lethality;
	//	sp = dbcSpell.LookupEntry( 14135 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_lethality;
	//	sp = dbcSpell.LookupEntry( 14136 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_lethality;
	//	sp = dbcSpell.LookupEntry( 14137 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_rogue_lethality;

	//	//rogue - Endurance 
	//	sp = dbcSpell.LookupEntry( 13742 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 32;	//Sprint + Evasion
	//	sp = dbcSpell.LookupEntry( 13872 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 32;	//Sprint + Evasion
	//	//rogue - Seal Fate
	//	sp = dbcSpell.LookupEntry( 14186 );
	//	if( sp != NULL ) 
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 14189;
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->procChance = 20;
	//	}
	//	sp = dbcSpell.LookupEntry( 14190 );
	//	if( sp != NULL ) 
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 14189;
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->procChance = 40;
	//	}
	//	sp = dbcSpell.LookupEntry( 14193 );
	//	if( sp != NULL ) 
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 14189;
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->procChance = 60;
	//	}
	//	sp = dbcSpell.LookupEntry( 14194 );
	//	if( sp != NULL ) 
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 14189;
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->procChance = 80;
	//	}
	//	sp = dbcSpell.LookupEntry( 14195 );
	//	if( sp != NULL ) 
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 14189;
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->procChance = 100;
	//	}
	//#ifndef NEW_PROCFLAGS
	//	//Improved Sprint
	//	sp = dbcSpell.LookupEntry( 13743 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->procChance = 50;
	//	}
	//	sp = dbcSpell.LookupEntry( 13875 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance = 100;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//#else
	//	//Improved Sprint
	//	sp = dbcSpell.LookupEntry( 13743 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0]=64;
	//	sp = dbcSpell.LookupEntry( 13875 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0]=64;
	//#endif
	//	//rogue-shiv -> add 1 combo point
	//	sp = dbcSpell.LookupEntry( 5938 );
	//	if( sp != NULL )
	//		sp->Effect[1]=80;
	//	//garrot
	//	sp = dbcSpell.LookupEntry( 703 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 8631 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 8632 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 8633 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 11289 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 11290 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 26839 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 26884 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;

	//	//rupture
	//	sp = dbcSpell.LookupEntry( 1943 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 8639 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 8640 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 11273 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 11274 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 11275 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 26867 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//#ifndef NEW_PROCFLAGS
	//	//Relentless Strikes
	//	sp = dbcSpell.LookupEntry( 14179 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;//proc spell
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectBasePoints[1] = 20; //client showes 20% chance but whe do not have it ? :O
	//	}
	//#else
	//	//Relentless Strikes
	//	sp = dbcSpell.LookupEntry( 14179 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0]= 262144 | 2097152 | 8388608 | 8519680 | 524288 | 1048576 | 8388608;
	//#endif
	//#ifndef NEW_PROCFLAGS
	//	//rogue - intiative
	//	sp = dbcSpell.LookupEntry( 13976 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = uint32(PROC_ON_CAST_SPELL|PROC_TARGET_SELF);
	//	}
	//	sp = dbcSpell.LookupEntry( 13979 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = uint32(PROC_ON_CAST_SPELL|PROC_TARGET_SELF);
	//	}
	//	sp = dbcSpell.LookupEntry( 13980 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = uint32(PROC_ON_CAST_SPELL|PROC_TARGET_SELF);
	//	}
	//#else
	//	//rogue - intiative
	//	sp = dbcSpell.LookupEntry( 13976 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8389120 | 256 | 1024;
	//	sp = dbcSpell.LookupEntry( 13979 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8389120 | 256 | 1024;
	//	sp = dbcSpell.LookupEntry( 13980 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8389120 | 256 | 1024;
	//#endif

	////////////////////////////////////////////
	//// CLASS_PRIEST								//
	////////////////////////////////////////////

	//// Insert priest spell fixes here

	//	/**********************************************************
	//	 *	Holy Nova
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 15237 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64;
	//		sp->EffectTriggerSpell[1] = 23455;
	//	}
	//	sp = dbcSpell.LookupEntry( 15430 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64;
	//		sp->EffectTriggerSpell[1] = 23458;
	//	}
	//	sp = dbcSpell.LookupEntry( 15431 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64;
	//		sp->EffectTriggerSpell[1] = 23459;
	//	}
	//	sp = dbcSpell.LookupEntry( 27799 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64;
	//		sp->EffectTriggerSpell[1] = 27803;
	//	}
	//	sp = dbcSpell.LookupEntry( 27800 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64;
	//		sp->EffectTriggerSpell[1] = 27804;
	//	}
	//	sp = dbcSpell.LookupEntry( 27801 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64;
	//		sp->EffectTriggerSpell[1] = 27805;
	//	}
	//	sp = dbcSpell.LookupEntry( 25331 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 64;
	//		sp->EffectTriggerSpell[1] = 25329;
	//	}
	//	 //priest - Holy Concentration
	//	 sp = dbcSpell.LookupEntry( 34753 );
	//	 if (sp != NULL)
	//		  sp->procFlags = PROC_ON_CAST_SPELL;
	//	 sp = dbcSpell.LookupEntry( 34859 );
	//	 if (sp != NULL)
	//		 sp->procFlags = PROC_ON_CAST_SPELL;
	//	 sp = dbcSpell.LookupEntry( 34860 );
	//	 if (sp != NULL)
	//		  sp->procFlags = PROC_ON_CAST_SPELL;
	//	 sp = dbcSpell.LookupEntry( 34754 );
	//	 if (sp != NULL)
	//	 {
	//		  //sp->EffectSpellGroupRelation[0] = 2048 | 4096;
	//		  sp->EffectSpellGroupRelation_high[0] = 4;
	//	 }

	//	//priest - Focused Mind 
	//	sp = dbcSpell.LookupEntry( 33213 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8192 | 131072 | 8388608; // Mind Blast + Mind Control + Mind Flay
	//	sp = dbcSpell.LookupEntry( 33214 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8192 | 131072 | 8388608; // Mind Blast + Mind Control + Mind Flay
	//	sp = dbcSpell.LookupEntry( 33215 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8192 | 131072 | 8388608; // Mind Blast + Mind Control + Mind Flay

	//	//Priest: Blessed Recovery
	//	sp = dbcSpell.LookupEntry(27811);
	//	if(sp != NULL)
	//	{
	//		sp->EffectTriggerSpell[0] = 27813;
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;
	//	}
	//	sp = dbcSpell.LookupEntry(27815);
	//	if(sp != NULL)
	//	{
	//		sp->EffectTriggerSpell[0] = 27817;
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;
	//	}
	//	sp = dbcSpell.LookupEntry(27816);
	//	if(sp != NULL)
	//	{
	//		sp->EffectTriggerSpell[0] = 27818;
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;
	//	}
	//	//priest- Blessed Resilience
	//	sp = dbcSpell.LookupEntry( 33142 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;
	//	sp = dbcSpell.LookupEntry( 33145 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;
	//	sp = dbcSpell.LookupEntry( 33146 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;

	//	//priest- Focused Will
	//	sp = dbcSpell.LookupEntry( 45234 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;
	//	sp = dbcSpell.LookupEntry( 45243 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;
	//	sp = dbcSpell.LookupEntry( 45244 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;

	//	//priest - Improved Divine Spirit 
	//	sp = dbcSpell.LookupEntry( 33174 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32 ;
	//		sp->EffectSpellGroupRelation[1] = 32 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 33182 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32 ;
	//		sp->EffectSpellGroupRelation[1] = 32 ;
	//	}

	//	//priest - Empowered Healing 
	//	sp = dbcSpell.LookupEntry( 33158 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4096 ;
	//		sp->EffectSpellGroupRelation[1] = 2048 ;
	//		sp->EffectSpellGroupRelation_high[1] = 4 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 33159 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4096 ;
	//		sp->EffectSpellGroupRelation[1] = 2048 ;
	//		sp->EffectSpellGroupRelation_high[1] = 4 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 33160 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4096 ;
	//		sp->EffectSpellGroupRelation[1] = 2048 ;
	//		sp->EffectSpellGroupRelation_high[1] = 4 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 33161 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4096 ;
	//		sp->EffectSpellGroupRelation[1] = 2048 ;
	//		sp->EffectSpellGroupRelation_high[1] = 4 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 33162 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4096 ;
	//		sp->EffectSpellGroupRelation[1] = 2048 ;
	//		sp->EffectSpellGroupRelation_high[1] = 4 ;
	//	}

	//	//priest - Force of Will 
	//	sp = dbcSpell.LookupEntry( 18544 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4194304 | 128 | 32768 | 8192 | 16 | 1048576 | 8388608 | 2097152 | 67108864;
	//		sp->EffectSpellGroupRelation[1] = 4194304 | 128 | 32768 | 8192 | 16 | 1048576 | 8388608 | 2097152 | 67108864;
	//		sp->EffectSpellGroupRelation[2] = 0 ;	//1-2 mod the same ?
	//		sp->EffectSpellGroupRelation_high[0] = 2 | 1024 ;
	//		sp->EffectSpellGroupRelation_high[1] = 2 | 1024 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 18547 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4194304 | 128 | 32768 | 8192 | 16 | 1048576 | 8388608 | 2097152 | 67108864;
	//		sp->EffectSpellGroupRelation[1] = 4194304 | 128 | 32768 | 8192 | 16 | 1048576 | 8388608 | 2097152 | 67108864;
	//		sp->EffectSpellGroupRelation[2] = 0 ;	//1-2 mod the same ?
	//		sp->EffectSpellGroupRelation_high[0] = 2 | 1024 ;
	//		sp->EffectSpellGroupRelation_high[1] = 2 | 1024 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 18548 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4194304 | 128 | 32768 | 8192 | 16 | 1048576 | 8388608 | 2097152 | 67108864;
	//		sp->EffectSpellGroupRelation[1] = 4194304 | 128 | 32768 | 8192 | 16 | 1048576 | 8388608 | 2097152 | 67108864 ;
	//		sp->EffectSpellGroupRelation[2] = 0 ;	//1-2 mod the same ?
	//		sp->EffectSpellGroupRelation_high[0] = 2 | 1024 ;
	//		sp->EffectSpellGroupRelation_high[1] = 2 | 1024 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 18549 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4194304 | 128 | 32768 | 8192 | 16 | 1048576 | 8388608 | 2097152 | 67108864;
	//		sp->EffectSpellGroupRelation[1] = 4194304 | 128 | 32768 | 8192 | 16 | 1048576 | 8388608 | 2097152 | 67108864;
	//		sp->EffectSpellGroupRelation[2] = 0;	//1-2 mod the same ?
	//		sp->EffectSpellGroupRelation_high[0] = 2 | 1024 ;
	//		sp->EffectSpellGroupRelation_high[1] = 2 | 1024 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 18550 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4194304 | 128 | 32768 | 8192 | 16 | 1048576 | 8388608 | 2097152 | 67108864;
	//		sp->EffectSpellGroupRelation[1] = 4194304 | 128 | 32768 | 8192 | 16 | 1048576 | 8388608 | 2097152 | 67108864;
	//		sp->EffectSpellGroupRelation[2] = 0;	//1-2 mod the same ?
	//		sp->EffectSpellGroupRelation_high[0] = 2 | 1024;
	//		sp->EffectSpellGroupRelation_high[1] = 2 | 1024;
	//	}

	//	//Priest: Shadowguard
	//	sp = dbcSpell.LookupEntry( 18137 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_HIT_VICTIM | PROC_ON_RANGED_ATTACK_VICTIM | PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->proc_interval = 3000; //every 3 seconds
	//		sp->EffectTriggerSpell[0] = 28377;
	//	}
	//	sp = dbcSpell.LookupEntry( 19308 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_HIT_VICTIM | PROC_ON_RANGED_ATTACK_VICTIM | PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->proc_interval = 3000; //every 3 seconds
	//		sp->EffectTriggerSpell[0] = 28378;
	//	}
	//	sp = dbcSpell.LookupEntry( 19309 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_HIT_VICTIM | PROC_ON_RANGED_ATTACK_VICTIM | PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->proc_interval = 3000; //every 3 seconds
	//		sp->EffectTriggerSpell[0] = 28379;
	//	}
	//	sp = dbcSpell.LookupEntry( 19310 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_HIT_VICTIM | PROC_ON_RANGED_ATTACK_VICTIM | PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->proc_interval = 3000; //every 3 seconds
	//		sp->EffectTriggerSpell[0] = 28380;
	//	}
	//	sp = dbcSpell.LookupEntry( 19311 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_HIT_VICTIM | PROC_ON_RANGED_ATTACK_VICTIM | PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->proc_interval = 3000; //every 3 seconds
	//		sp->EffectTriggerSpell[0] = 28381;
	//	}
	//	sp = dbcSpell.LookupEntry( 19312 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_HIT_VICTIM | PROC_ON_RANGED_ATTACK_VICTIM | PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->proc_interval = 3000; //every 3 seconds
	//		sp->EffectTriggerSpell[0] = 28382;
	//	}
	//	sp = dbcSpell.LookupEntry( 25477 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_HIT_VICTIM | PROC_ON_RANGED_ATTACK_VICTIM | PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->proc_interval = 3000; //every 3 seconds
	//		sp->EffectTriggerSpell[0] = 28385;
	//	}

	//	//priest - Absolution 
	//	sp = dbcSpell.LookupEntry( 33167 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 33171 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 33172 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 128;
	//	}

	//	//priest - Mental Agility - all instant spells. I wonder if it conflicts with any other spells 
	//	sp = dbcSpell.LookupEntry( 14520 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL | 65536 | 67108864UL | 4 | 1 | 64 | 32 | 4194304UL | 32768 | 8388608UL | 8 | 16384 | 2 | 256 | 16777216UL | 2097152UL | 33554432UL;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 64 | 2;
	//	}
	//	sp = dbcSpell.LookupEntry( 14780 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL | 65536 | 67108864UL | 4 | 1 | 64 | 32 | 4194304UL | 32768 | 8388608UL | 8 | 16384 | 2 | 256 | 16777216UL | 2097152UL | 33554432UL;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 64 | 2;
	//	}
	//	sp = dbcSpell.LookupEntry( 14781 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL | 65536 | 67108864UL | 4 | 1 | 64 | 32 | 4194304UL | 32768 | 8388608UL | 8 | 16384 | 2 | 256 | 16777216UL | 2097152UL | 33554432UL;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 64 | 2;
	//	}
	//	sp = dbcSpell.LookupEntry( 14782 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL | 65536 | 67108864UL | 4 | 1 | 64 | 32 | 4194304UL | 32768 | 8388608UL | 8 | 16384 | 2 | 256 | 16777216UL | 2097152UL | 33554432UL;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 64 | 2;
	//	}
	//	sp = dbcSpell.LookupEntry( 14783 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL | 65536 | 67108864UL | 4 | 1 | 64 | 32 | 4194304UL | 32768 | 8388608UL | 8 | 16384 | 2 | 256 | 16777216UL | 2097152UL | 33554432UL;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 64 | 2;
	//	}

	//	//priest - Focused Power
	//	sp = dbcSpell.LookupEntry( 33186 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL;
	//		sp->EffectSpellGroupRelation[1] = 128 | 8192 | 2147483648UL;
	//	}
	//	sp = dbcSpell.LookupEntry( 33190 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL;
	//		sp->EffectSpellGroupRelation[1] = 128 | 8192 | 2147483648UL;
	//	}

	//	//priest - Shadow Reach 
	//	sp = dbcSpell.LookupEntry( 17322 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 32768 | 65536 | 4 | 8192 | 16 | 8388608 | 2147483648UL;
	//	sp = dbcSpell.LookupEntry( 17323 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 32768 | 65536 | 4 | 8192 | 16 | 8388608 | 2147483648UL;

	//	//priest - Shadow Focus 
	//	sp = dbcSpell.LookupEntry( 15260 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 | 67108864 | 131072 | 2147483648UL | 32768 | 8192 | 16 | 4 | 8388608 | 16384 | 256 | 33554432;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 2 | 1024;
	//	}
	//	sp = dbcSpell.LookupEntry( 15327 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 | 67108864 | 131072 | 2147483648UL | 32768 | 8192 | 16 | 4 | 8388608 | 16384 | 256 | 33554432;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 2 | 1024;
	//	}
	//	sp = dbcSpell.LookupEntry( 15328 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 | 67108864 | 131072 | 2147483648UL | 32768 | 8192 | 16 | 4 | 8388608 | 16384 | 256 | 33554432;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 2 | 1024;
	//	}
	//	sp = dbcSpell.LookupEntry( 15329 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 | 67108864 | 131072 | 2147483648UL | 32768 | 8192 | 16 | 4 | 8388608 | 16384 | 256 | 33554432;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 2 | 1024;
	//	}
	//	sp = dbcSpell.LookupEntry( 15330 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 65536 | 67108864 | 131072 | 2147483648UL | 32768 | 8192 | 16 | 4 | 8388608 | 16384 | 256 | 33554432;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 2 | 1024;
	//	}
	//	//Priest - Wand Specialization
	//	sp = dbcSpell.LookupEntry( 14524 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[0] = 134217728;
	//	}
	//	sp = dbcSpell.LookupEntry( 14525 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[0] = 134217728;
	//	}
	//	sp = dbcSpell.LookupEntry( 14526 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[0] = 134217728;
	//	}
	//	sp = dbcSpell.LookupEntry( 14527 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[0] = 134217728;
	//	}
	//	sp = dbcSpell.LookupEntry( 14528 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[0] = 134217728;
	//	}
	//	//Priest: Shadow Power
	//	sp = dbcSpell.LookupEntry( 15310 );
	//	if( sp != NULL )
	//	{
	//		uint32 group = sp->EffectSpellGroupRelation[0];
	//		sp = dbcSpell.LookupEntry( 33221 );
	//		if( sp != NULL )
	//			sp->EffectSpellGroupRelation[0] = group;
	//		sp = dbcSpell.LookupEntry( 33222 );
	//		if( sp != NULL )
	//			sp->EffectSpellGroupRelation[0] = group;
	//		sp = dbcSpell.LookupEntry( 33223 );
	//		if( sp != NULL )
	//			sp->EffectSpellGroupRelation[0] = group;
	//		sp = dbcSpell.LookupEntry( 33224 );
	//		if( sp != NULL )
	//			sp->EffectSpellGroupRelation[0] = group;
	//		sp = dbcSpell.LookupEntry( 33225 );
	//		if( sp != NULL )
	//			sp->EffectSpellGroupRelation[0] = group;
	//	}

	//#ifdef NEW_PROCFLAGS
	//	//priest -  Shadow Weaving
	//	if (sp != NULL)
	//	{
	//		uint32 group = sp->EffectSpellGroupRelation[0];
	//		sp = dbcSpell.LookupEntry(15334);
	//		if (sp !=NULL)
	//			sp->EffectSpellGroupRelation[0] = group;
	//		sp = dbcSpell.LookupEntry(15333);
	//		if (sp !=NULL)
	//			sp->EffectSpellGroupRelation[0] = group;
	//		sp = dbcSpell.LookupEntry(15332);
	//		if (sp !=NULL)
	//			sp->EffectSpellGroupRelation[0] = group;
	//		sp = dbcSpell.LookupEntry(15331);
	//		if (sp !=NULL)
	//			sp->EffectSpellGroupRelation[0] = group;
	//		sp = dbcSpell.LookupEntry(15257);
	//		if (sp !=NULL)
	//			sp->EffectSpellGroupRelation[0] = group;
	//	}
	//#endif

	//	//Priest - Inspiration proc spell
	//	sp = dbcSpell.LookupEntry( 14893 );
	//	if( sp != NULL )
	//		sp->rangeIndex = 4;
	//	sp = dbcSpell.LookupEntry( 15357 );
	//	if( sp != NULL )
	//		sp->rangeIndex = 4;
	//	sp = dbcSpell.LookupEntry( 15359 );
	//	if( sp != NULL )
	//		sp->rangeIndex = 4;

	//	//priest - surge of light
	//	sp = dbcSpell.LookupEntry( 33150 );
	//	if( sp != NULL )
	//		sp->procFlags = uint32(PROC_ON_SPELL_CRIT_HIT_VICTIM | PROC_TARGET_SELF);
	//	sp = dbcSpell.LookupEntry( 33154 );
	//	if( sp != NULL )
	//		sp->procFlags = uint32(PROC_ON_SPELL_CRIT_HIT_VICTIM | PROC_TARGET_SELF);
	//	sp = dbcSpell.LookupEntry( 33151 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 128;
	//		sp->EffectSpellGroupRelation[1] = 128;
	//		sp->EffectSpellGroupRelation[2] = 128;
	//		sp->AuraInterruptFlags = AURA_INTERRUPT_ON_CAST_SPELL;
	//	}
	//	// priest - Reflective Shield
	//	sp = dbcSpell.LookupEntry( 33201 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ABSORB;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 33619; //!! WRONG spell, we will make direct dmg here
	//	}
	//	sp = dbcSpell.LookupEntry( 33202 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ABSORB;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 33619; //!! WRONG spell, we will make direct dmg here
	//	}
	//	sp = dbcSpell.LookupEntry( 33203 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ABSORB;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 33619; //!! WRONG spell, we will make direct dmg here
	//	}
	//	sp = dbcSpell.LookupEntry( 33204 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ABSORB;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 33619; //!! WRONG spell, we will make direct dmg here
	//	}
	//	sp = dbcSpell.LookupEntry( 33205 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_ABSORB;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 33619; //!! WRONG spell, we will make direct dmg here
	//	}

	////////////////////////////////////////////
	//// SHAMAN								//
	////////////////////////////////////////////

	//// Insert shaman spell fixes here

	//	/**********************************************************
	//	 *	Shamanistic Rage
	//	 **********************************************************/
	//	SpellEntry*  parentsp = dbcSpell.LookupEntry( 30823 );
	//	SpellEntry* triggersp = dbcSpell.LookupEntry( 30824 );
	//	if( parentsp != NULL && triggersp != NULL )
	//		triggersp->EffectBasePoints[0] = parentsp->EffectBasePoints[0];

	//	/**********************************************************
	//	 *	Elemental Focus
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16164 );
	//	if( sp != NULL && sp->Id == 16164 )
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;

	//	/**********************************************************
	//	 *	Stormstrike
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 17364 );
	//	if( sp != NULL && sp->Id == 17364 )
	//		sp->Effect[0] = 0;

	//	/**********************************************************
	//	 *	Bloodlust
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 2825 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[2] = 0;
	//		sp->EffectImplicitTargetB[0] = 0;
	//		sp->EffectImplicitTargetB[1] = 0;
	//		sp->EffectImplicitTargetB[2] = 0;
	//	}

	//	/**********************************************************
	//	 *	Heroism
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 32182 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[2] = 0;
	//		sp->EffectImplicitTargetB[0] = 0;
	//		sp->EffectImplicitTargetB[1] = 0;
	//		sp->EffectImplicitTargetB[2] = 0;
	//	}
	//	/**********************************************************
	//	 *	Lightning Mastery
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16578 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0]=1|2;
	//	sp = dbcSpell.LookupEntry( 16579 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0]=1|2;
	//	sp = dbcSpell.LookupEntry( 16580 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0]=1|2;
	//	sp = dbcSpell.LookupEntry( 16581 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0]=1|2;
	//	sp = dbcSpell.LookupEntry( 16582 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0]=1|2;
	//	/**********************************************************
	//	 *	Lightning Overload 
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 30675 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 39805;//proc something (we will owerride this)
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 30678 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 39805;//proc something (we will owerride this)
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 30679 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 39805;//proc something (we will owerride this)
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 30680 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 39805;//proc something (we will owerride this)
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 30681 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 39805;//proc something (we will owerride this)
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	/**********************************************************
	//	 *	Purge 
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 370 ); 
	//	if( sp != NULL )
	//		sp->DispelType = DISPEL_MAGIC;
	//	sp = dbcSpell.LookupEntry( 8012 ); 
	//	if( sp != NULL )
	//		sp->DispelType = DISPEL_MAGIC;
	//	sp = dbcSpell.LookupEntry( 27626 ); 
	//	if( sp != NULL )
	//		sp->DispelType = DISPEL_MAGIC;
	//	sp = dbcSpell.LookupEntry( 33625 ); 
	//	if( sp != NULL )
	//		sp->DispelType = DISPEL_MAGIC;

	//	/**********************************************************
	//	 *	Elemental mastery
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16166 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 0xFFFFFFFF;//nature+fire+frost is all that shaman can do
	//		sp->EffectSpellGroupRelation_high[0] = 0xFFFFFFFF;//nature+fire+frost is all that shaman can do
	//		sp->EffectSpellGroupRelation[1] = 0xFFFFFFFF;//nature+fire+frost is all that shaman can do
	//		sp->EffectSpellGroupRelation_high[1] = 0xFFFFFFFF;//nature+fire+frost is all that shaman can do
	//	}
	//	/**********************************************************
	//	 *	Eye of the Storm
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 29062 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;
	//	sp = dbcSpell.LookupEntry( 29064 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;
	//	sp = dbcSpell.LookupEntry( 29065 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;

	//	/**********************************************************
	//	 *	Shamanistic Focus
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 43338 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->EffectTriggerSpell[0] = 43339;
	//		sp->procChance = 100;
	//		sp->maxstack = 1;
	//	}

	//	/**********************************************************
	//	 *	focused
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 43339 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectMiscValue[0] = SMT_COST;
	//		sp->EffectSpellGroupRelation[0] = 1048576UL | 268435456UL | 2147483648UL;
	//	}

	//	/**********************************************************
	//	 *	Focused Casting
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 29063 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] =  0xFFFFFFFF; // shaman spells. Guess that wraps them all 
	//		sp->EffectSpellGroupRelation_high[0] =  0xFFFFFFFF; // shaman spells. Guess that wraps them all 
	//	}

	//	/**********************************************************
	//	 *	Healing Focus
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16181 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] =  64 | 128 | 256;
	//	sp = dbcSpell.LookupEntry( 16230 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] =  64 | 128 | 256;
	//	sp = dbcSpell.LookupEntry( 16232 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] =  64 | 128 | 256;
	//	sp = dbcSpell.LookupEntry( 16233 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] =  64 | 128 | 256;
	//	sp = dbcSpell.LookupEntry( 16234 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] =  64 | 128 | 256;

	//	/**********************************************************
	//	 *	Improved Lightning shield 
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16261 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1024;
	//	sp = dbcSpell.LookupEntry( 16290 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1024;
	//	sp = dbcSpell.LookupEntry( 16291 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1024;
	//	/**********************************************************
	//	 *	Tidal focus
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16179 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 128 | 256;
	//	sp = dbcSpell.LookupEntry( 16214 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 128 | 256;
	//	sp = dbcSpell.LookupEntry( 16215 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 128 | 256;
	//	sp = dbcSpell.LookupEntry( 16216 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 128 | 256;
	//	sp = dbcSpell.LookupEntry( 16217 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 128 | 256;

	//	/**********************************************************
	//	 *	Enhancing Totems
	//	 **********************************************************/
	//	group_relation_shaman_enhancing_totems = 0x00010000 | 0x00020000; // <--- GROUPING
	//	sp = dbcSpell.LookupEntry( 16259 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_enhancing_totems;
	//	sp = dbcSpell.LookupEntry( 16295 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_enhancing_totems;

	//	/**********************************************************
	//	 *	Elemental Fury - ! Not finished !
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16089 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 0xFFFFFFFF ; //damn, what other spells do there remain after that list ? Maybe later :P
	//		//sp->EffectSpellGroupRelation[0] = 1073741824 | 32 | 1048576 | 1 | ... ; //Searing/Magma/Fire Nova Totem effects and Fire,Frost,Nature spells
	//		sp->EffectSpellGroupRelation_high[0] = 0xFFFFFFFF ; //damn, what other spells do there remain after that list ? Maybe later :P
	//	}

	//	/**********************************************************
	//	 *	Restorative Totems
	//	 **********************************************************/
	//	group_relation_shaman_restorative_totems = 0x00004000 | 0x00002000; // <--- GROUPING
	//	sp = dbcSpell.LookupEntry( 16259 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_restorative_totems;
	//	sp = dbcSpell.LookupEntry( 16205 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_restorative_totems;
	//	sp = dbcSpell.LookupEntry( 16206 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_restorative_totems;
	//	sp = dbcSpell.LookupEntry( 16207 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_restorative_totems;
	//	sp = dbcSpell.LookupEntry( 16208 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_restorative_totems;

	//	/**********************************************************
	//	 *	Healing Way
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 29202 ); 
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 29205 ); 
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 29206 ); 
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}

	//	/**********************************************************
	//	 *	Elemental Devastation
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 29179 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;
	//	sp = dbcSpell.LookupEntry( 29180 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;
	//	sp = dbcSpell.LookupEntry( 30160 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;

	//	/**********************************************************
	//	 *	Ancestral healing
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16176 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;
	//	sp = dbcSpell.LookupEntry( 16235 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;
	//	sp = dbcSpell.LookupEntry( 16240 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;

	//	/**********************************************************
	//	 *	Ancestral healing proc spell
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16177 );
	//	if( sp != NULL )
	//		sp->rangeIndex = 4;
	//	sp = dbcSpell.LookupEntry( 16236 );
	//	if( sp != NULL )
	//		sp->rangeIndex = 4;
	//	sp = dbcSpell.LookupEntry( 16237 );
	//	if( sp != NULL )
	//		sp->rangeIndex = 4;

	//	/**********************************************************
	//	 *	Mental Quickness
	//	 **********************************************************/
	//	group_relation_shaman_mental_quickness = 0x00000008 | 0x00000010 | 0x00000200 | 0x00000400 | 0x00080000 | 0x00100000 | 0x00400000 | 0x20000000 | 0x10000000 | 0x80000000;
	//	sp = dbcSpell.LookupEntry( 30812 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_mental_quickness;
	//	sp = dbcSpell.LookupEntry( 30813 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_mental_quickness;
	//	sp = dbcSpell.LookupEntry( 30814 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_mental_quickness;

	//	/**********************************************************
	//	 *	Totems grouping
	//	 **********************************************************/
	//	group_relation_shaman_totems |= 0x00000008 | 0x00000010 | 0x00001000 | 0x00080000 | 0x20000000;
	//	
	//	/**********************************************************
	//	 *	Totemic focus
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16173 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_totems;
	//	sp = dbcSpell.LookupEntry( 16222 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_totems;
	//	sp = dbcSpell.LookupEntry( 16223 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_totems;
	//	sp = dbcSpell.LookupEntry( 16224 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_totems;
	//	sp = dbcSpell.LookupEntry( 16225 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_totems;
	//	
	//	/**********************************************************
	//	 *	Lightning - Grouping
	//	 **********************************************************/
	//	group_relation_shaman_lightning = 0x00000001 | 0x00000002;
	//	
	//	/**********************************************************
	//	 *	Call of Thunder
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16041 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16117 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16118 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16119 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16120 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_lightning;
	//	
	//	/**********************************************************
	//	 *	Shock Grouping
	//	 **********************************************************/
	//	group_relation_shaman_shock = 0x00100000 | 0x10000000 | 0x80000000;

	//	/**********************************************************
	//	 *	Convection
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16039 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_shock | group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16109 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_shock | group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16110 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_shock | group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16111 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_shock | group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16112 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_shock | group_relation_shaman_lightning;

	//	/**********************************************************
	//	 *	Concussion
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 16035 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_shock | group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16105 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_shock | group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16106 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_shock | group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16107 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_shock | group_relation_shaman_lightning;
	//	sp = dbcSpell.LookupEntry( 16108 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = group_relation_shaman_shock | group_relation_shaman_lightning;
	//	
	//	// Shaman - Storm Reach
	//	sp = dbcSpell.LookupEntry( 28999 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 2 | 1;
	//	sp = dbcSpell.LookupEntry( 29000 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 2 | 1;

	//	//wrath of air totem targets sorounding creatures instead of us
	//	sp = dbcSpell.LookupEntry( 2895 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SELF;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_SELF;
	//		sp->EffectImplicitTargetA[2] = 0;
	//		sp->EffectImplicitTargetB[0] = 0;
	//		sp->EffectImplicitTargetB[1] = 0;
	//		sp->EffectImplicitTargetB[2] = 0;
	//	}

	//	sp = dbcSpell.LookupEntry( 20608 ); //Reincarnation
	//	if( sp != NULL )
	//	{
	//		for(uint32 i=0;i<8;i++)
	//		{
	//			if(sp->Reagent[i])
	//			{
	//				sp->Reagent[i] = 0;
	//				sp->ReagentCount[i] = 0;
	//			}
	//		}
	//	}

	////////////////////////////////////////////
	//// CLASS_MAGE								//
	////////////////////////////////////////////

	//// Insert mage spell fixes here

	//	/**********************************************************
	//	 *	Arcane Power
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 12042 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 8192 | 4194304 | 8388608 | 262144 | 131072 | 536870912 | 524352 | 4 | 4096 | 2 | 2048 | 16;
	//		sp->EffectSpellGroupRelation[1] = 1 | 8192 | 4194304 | 8388608 | 262144 | 131072 | 536870912 | 524352 | 4 | 4096 | 2 | 2048 | 16;
	//		sp->EffectSpellGroupRelation[2] = 1 | 8192 | 4194304 | 8388608 | 262144 | 131072 | 536870912 | 524352 | 4 | 4096 | 2 | 2048 | 16;
	//	}

	//	/**********************************************************
	//	 *	Arcane Concentration
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 11213 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_HIT | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 12574 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_HIT | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 12575 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_HIT | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 12576 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_HIT | PROC_TARGET_SELF;
	//	sp = dbcSpell.LookupEntry( 12577 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_HIT | PROC_TARGET_SELF;

	//	//Mage - Icy Veins
	//	sp = dbcSpell.LookupEntry( 12472 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[1] = 512;
	//		sp->EffectMiscValue[1] = SMT_TRIGGER;
	//	}
	//	//Mage - Wand Specialization. Not the forst thing we messed up. Blizz uses attack as magic and wandds as weapons :S
	//	sp = dbcSpell.LookupEntry( 6057 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[0] = 134217728;
	//	}
	//	sp = dbcSpell.LookupEntry( 6085 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectSpellGroupRelation[0] = 134217728;
	//	}
	//	//Mage - Spell Power
	//	sp = dbcSpell.LookupEntry( 35578 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectMiscValue[0] = SMT_CRITICAL_DAMAGE;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectSpellGroupRelation[0] = 524288 | 131072 | 0 | 1 | 2 | 4 | 22 | 12 | 29 | 11;
	//		//sp->EffectSpellGroupRelation[0] = 0xFFFFFFFF; // This shit made spells get 150-200% bonus!
	//		//sp->EffectSpellGroupRelation_high[0] = 0xFFFFFFFF;
	//	}
	//	sp = dbcSpell.LookupEntry( 35581 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectMiscValue[0] = SMT_CRITICAL_DAMAGE;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectSpellGroupRelation[0] = 524288 | 131072 | 0 | 1 | 2 | 4 | 22 | 12 | 29 | 11;
	//		//sp->EffectSpellGroupRelation[0] = 0xFFFFFFFF;
	//		//sp->EffectSpellGroupRelation_high[0] = 0xFFFFFFFF;
	//	}

	//	//Mage - Frost Channeling
	//	sp = dbcSpell.LookupEntry( 11160 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 524288 | 131072  ;
	//		sp->EffectSpellGroupRelation_high[0] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 12518 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 524288 | 131072  ;
	//		sp->EffectSpellGroupRelation_high[0] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 12519 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 524288 | 131072  ;
	//		sp->EffectSpellGroupRelation_high[0] = 1;
	//	}

	//	//Mage - Elemental Precision
	//	sp = dbcSpell.LookupEntry( 29438 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectSpellGroupRelation[0] = 8388608 | 2 | 16 | 4 | 1573376 | 524288 | 8 | 131072 | 262144 | 4194304 | 1 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 1;
	//		sp->EffectMiscValue[0] = SMT_COST;
	//	}
	//	sp = dbcSpell.LookupEntry( 29439 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectSpellGroupRelation[0] = 8388608 | 2 | 16 | 4 | 1573376 | 524288 | 8 | 131072 | 262144 | 4194304 | 1 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 1;
	//		sp->EffectMiscValue[0] = SMT_COST;
	//	}
	//	sp = dbcSpell.LookupEntry( 29440 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectSpellGroupRelation[0] = 8388608 | 2 | 16 | 4 | 1573376 | 524288 | 8 | 131072 | 262144 | 4194304 | 1 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 1;
	//		sp->EffectMiscValue[0] = SMT_COST;
	//	}

	//	//Mage - Arcane Blast
	//	sp = dbcSpell.LookupEntry( 30451 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[1] = 36032;
	//		sp->procFlags = PROC_ON_CAST_SPECIFIC_SPELL;
	//	}

	//	//Mage - Magic Attunement
	//	sp = dbcSpell.LookupEntry( 11247 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8192;
	//	sp = dbcSpell.LookupEntry( 12606 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8192; //strange lvl 2 was working 

	//	//Mage - Arcane Blast proc spell
	//	sp = dbcSpell.LookupEntry( 36032 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 536870912;
	//		sp->EffectSpellGroupRelation[1] = 536870912;
	//	}

	//	//mage : Improved Blink
	//	sp = dbcSpell.LookupEntry( 31569 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 65536;
	//	sp = dbcSpell.LookupEntry( 31570 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 65536;

	//	//mage : Empowered Arcane Missiles
	//	sp = dbcSpell.LookupEntry( 31579 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2097152;
	//		sp->EffectBasePoints[0] *= 5; //heh B thinks he is smart by adding this to description ? If it doesn;t work std then it still needs to made by hand
	//		sp->EffectSpellGroupRelation[1] = 2048;
	//	}
	//	sp = dbcSpell.LookupEntry( 31582 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2097152;
	//		sp->EffectBasePoints[0] *= 5; //heh B thinks he is smart by adding this to description ? If it doesn;t work std then it still needs to made by hand
	//		sp->EffectSpellGroupRelation[1] = 2048;
	//	}
	//	sp = dbcSpell.LookupEntry( 31583 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2097152; //damage
	//		sp->EffectBasePoints[0] *= 5; //heh B thinks he is smart by adding this to description ? If it doesn;t work std then it still needs to made by hand
	//		sp->EffectSpellGroupRelation[1] = 2048; //cost
	//	}

	//	//mage : Empowered Fireball
	//	sp = dbcSpell.LookupEntry( 31656 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;
	//	sp = dbcSpell.LookupEntry( 31657 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;
	//	sp = dbcSpell.LookupEntry( 31658 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;
	//	sp = dbcSpell.LookupEntry( 31659 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;
	//	sp = dbcSpell.LookupEntry( 31660 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;

	//	//mage : Ice Floes
	//	sp = dbcSpell.LookupEntry( 31670 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 512;
	//		sp->EffectSpellGroupRelation_high[0] = 4 | 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 31672 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 512;
	//		sp->EffectSpellGroupRelation_high[0] = 4 | 1;
	//	}

	//	//mage : Empowered Frostbolt
	//	sp = dbcSpell.LookupEntry( 31682 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}
	//	sp = dbcSpell.LookupEntry( 31683 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}
	//	sp = dbcSpell.LookupEntry( 31684 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}
	//	sp = dbcSpell.LookupEntry( 31685 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}
	//	sp = dbcSpell.LookupEntry( 31686 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}

	//	//Mage - Ice Shards
	//	sp = dbcSpell.LookupEntry( 11207 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] =  524288 | 131072;
	//	sp = dbcSpell.LookupEntry( 12672 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] =  524288 | 131072;
	//	sp = dbcSpell.LookupEntry( 15047 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] =  524288 | 131072;
	//	sp = dbcSpell.LookupEntry( 15052 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] =  524288 | 131072;
	//	sp = dbcSpell.LookupEntry( 15053 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] =  524288 | 131072;

	//	//Mage - Improved Blizzard
	//	sp = dbcSpell.LookupEntry( 11185 );
	//	if( sp != NULL )
	//	{    
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 12484;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 12487 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 12485;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 12488 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 12486;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}

	//	//mage: Fire Power
	//	sp = dbcSpell.LookupEntry( 11124 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;
	//		sp->EffectSpellGroupRelation[1] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 12398 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;
	//		sp->EffectSpellGroupRelation[1] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 12399 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;
	//		sp->EffectSpellGroupRelation[1] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 12400 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;
	//		sp->EffectSpellGroupRelation[1] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 12378 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;
	//		sp->EffectSpellGroupRelation[1] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;
	//	}
	//	
	//	//mage - Burning Soul
	//	sp = dbcSpell.LookupEntry( 11083 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;
	//	sp = dbcSpell.LookupEntry( 12351 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8388608 | 16 | 2 | 4 | 8 | 262144 | 4194304 | 1;

	//	//mage - Combustion
	//	sp = dbcSpell.LookupEntry( 11129 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL | PROC_ON_SPELL_CRIT_HIT | PROC_TARGET_SELF;
	//		sp->procCharges = 0;
	//		sp->c_is_flags |= SPELL_FLAG_IS_REQUIRECOOLDOWNUPDATE;
	//	}
	//	sp = dbcSpell.LookupEntry( 28682 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 8388608 | 16 | 2 | 4 | 4194304 | 1;

	//	//mage - Empowered Fireball
	//	sp = dbcSpell.LookupEntry( 31656 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;
	//	sp = dbcSpell.LookupEntry( 31657 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;
	//	sp = dbcSpell.LookupEntry( 31658 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;
	//	sp = dbcSpell.LookupEntry( 31659 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;
	//	sp = dbcSpell.LookupEntry( 31660 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;

	//	//mage - Empowered Frostbolt
	//	sp = dbcSpell.LookupEntry( 31682 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}
	//	sp = dbcSpell.LookupEntry( 31683 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}
	//	sp = dbcSpell.LookupEntry( 31684 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}
	//	sp = dbcSpell.LookupEntry( 31685 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}
	//	sp = dbcSpell.LookupEntry( 31686 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32;
	//		sp->EffectSpellGroupRelation[1] = 32;
	//	}

	//	//mage - Master of Elements
	//	sp = dbcSpell.LookupEntry( 29074 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 29077;
	//		sp->procFlags = uint32(PROC_ON_SPELL_CRIT_HIT|PROC_TARGET_SELF);
	//		sp->procChance = 100;
	//	}
	//	sp = dbcSpell.LookupEntry( 29075 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 29077;
	//		sp->procFlags = uint32(PROC_ON_SPELL_CRIT_HIT|PROC_TARGET_SELF);
	//		sp->procChance = 100;
	//	}
	//	sp = dbcSpell.LookupEntry( 29076 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 29077;
	//		sp->procFlags = uint32(PROC_ON_SPELL_CRIT_HIT|PROC_TARGET_SELF);
	//		sp->procChance = 100;
	//	}

	//	//mage: Blazing Speed
	//	sp = dbcSpell.LookupEntry( 31641 ); 
	//	if( sp != NULL )
	//		sp->EffectTriggerSpell[0] = 31643;
	//	sp = dbcSpell.LookupEntry( 31642 );
	//	if( sp != NULL )
	//		sp->EffectTriggerSpell[0] = 31643;

	//	// Mage: Cold Snap ( set spell group relation for second effect, "gives you 100% chance to avoid interruption caused by damage while casting."
	//	sp = dbcSpell.LookupEntry( 12472 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[1] = 0xFFFFFFFF; // we can be sure this won't fuck up like Spell Power did, since it's 100%, not additive :P 1

	//	//mage talent "frostbyte". we make it to be dummy
	//	sp = dbcSpell.LookupEntry( 11071 );
	//	if( sp != NULL )
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_DUMMY;
	//	sp = dbcSpell.LookupEntry( 12496 );
	//	if( sp != NULL )
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_DUMMY;
	//	sp = dbcSpell.LookupEntry( 12497 );
	//	if( sp != NULL )
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_DUMMY;
	//	//Mage - Improved Scorch
	//	sp = dbcSpell.LookupEntry( 11095 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procChance =33;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 12872 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL; 
	//		sp->procChance =66;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 12873 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procChance =100;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	//mage - Presence of Mind
	//	sp = dbcSpell.LookupEntry(12043);
	//	if (sp!= NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0]= 0xFFFFFFFF;
	//		sp->EffectSpellGroupRelation_high[0]= 0xFFFFFFFF;
	//	}

	//	// mage - Frost Warding
	//	sp = dbcSpell.LookupEntry( 11189 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 33554432;
	//	sp = dbcSpell.LookupEntry( 28332 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 33554432;

	////////////////////////////////////////////
	//// WARLOCK								//
	////////////////////////////////////////////

	//// Insert warlock spell fixes here

	//	/**********************************************************
	//	 *	Nether Protection
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 30299 );
	//	if (sp != NULL)
	//	{
	//		sp->procChance = 10;
	//		sp->proc_interval = 13000;
	//	}
	//	sp = dbcSpell.LookupEntry( 30301 );
	//	if (sp != NULL)
	//	{
	//		sp->procChance = 20;
	//		sp->proc_interval = 13000;
	//	}
	//	sp = dbcSpell.LookupEntry( 30302 );
	//	if (sp != NULL)
	//	{
	//		sp->procChance = 30;
	//		sp->proc_interval = 13000;
	//	}

	//	/**********************************************************
	//	 *	Backlash
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 34935 );
	//	if (sp != NULL)
	//	{
	//		sp->proc_interval = 8000;
	//		sp->procFlags |= PROC_ON_MELEE_ATTACK_VICTIM | PROC_TARGET_SELF;
	//	}
	//	sp = dbcSpell.LookupEntry( 34938 );
	//	if (sp != NULL)
	//	{
	//		sp->proc_interval = 8000;
	//		sp->procFlags |= PROC_ON_MELEE_ATTACK_VICTIM | PROC_TARGET_SELF;
	//	}
	//	sp = dbcSpell.LookupEntry( 34939 );
	//	if (sp != NULL)
	//	{
	//		sp->proc_interval = 8000;
	//		sp->procFlags |= PROC_ON_MELEE_ATTACK_VICTIM | PROC_TARGET_SELF;
	//	}
	//	sp = dbcSpell.LookupEntry( 34936 );
	//	if (sp != NULL)
	//	{
	//		sp->AuraInterruptFlags = AURA_INTERRUPT_ON_CAST_SPELL;
	//		sp->EffectSpellGroupRelation[0] = 1 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64;
	//	}

	//	/**********************************************************
	//	 *	Demonic Knowledge
	//	 **********************************************************/
	//	sp = dbcSpell.LookupEntry( 35691 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_DAMAGE_DONE;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SELF;
	//		sp->Effect[1] = 6;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_DONE;
	//		sp->EffectBasePoints[1] = sp->EffectBasePoints[0];
	//		sp->EffectImplicitTargetA[1]= EFF_TARGET_PET;
	//		sp->Effect[2] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[2] = 35696;
	//		sp->EffectImplicitTargetA[2]=EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 35692 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_DAMAGE_DONE;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SELF;
	//		sp->Effect[1] = 6;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_DONE;
	//		sp->EffectBasePoints[1] = sp->EffectBasePoints[0];
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->Effect[2] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[2] = 35696;
	//		sp->EffectImplicitTargetA[2] = EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 35693 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_DAMAGE_DONE;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SELF;
	//		sp->Effect[1] = 6;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_DONE;
	//		sp->EffectBasePoints[1] = sp->EffectBasePoints[0];
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->Effect[2] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[2] = 35696;
	//		sp->EffectImplicitTargetA[2] = EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 35696 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; //making this only for the visible effect
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_DUMMY; //no effect here
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	//warlock -  Seed of Corruption
	//	sp = dbcSpell.LookupEntry( 27243 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[1] = 27285;
	//		sp->procFlags = PROC_ON_SPELL_HIT_VICTIM | PROC_ON_DIE;
	//		sp->procChance = 100;
	//	}

	//	//warlock -  soul link
	//	sp = dbcSpell.LookupEntry( 19028 );
	//	if( sp != NULL )
	//	{
	//		//this is for the trigger effect
	//		sp->Effect[0]=SPELL_EFFECT_APPLY_AURA;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_SPLIT_DAMAGE;
	//		sp->EffectMiscValue[0]=20;
	//		//sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		//this is for the extra 5% dmg for caster and pet
	//		sp->Effect[1] = 6;
	//		sp->EffectApplyAuraName[1] = 79;
	//		sp->EffectBasePoints[1] = 4; //4+1=5
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_SELF;
	//		sp->EffectImplicitTargetB[1] = EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//	}

	//	//warlock: Demonic Aegis
	//	sp = dbcSpell.LookupEntry( 30143 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation_high[0] = 32;
	//	sp = dbcSpell.LookupEntry( 30144 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation_high[0] = 32;
	//	sp = dbcSpell.LookupEntry( 30145 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation_high[0] = 32;

	//	//warlock: Nightfall
	//	sp = dbcSpell.LookupEntry( 18094 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 17941;
	//		sp->procFlags = PROC_ON_ANY_HOSTILE_ACTION | PROC_TARGET_SELF;
	//		sp->procChance = 2;
	//	}
	//	sp = dbcSpell.LookupEntry( 18095 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 17941;
	//		sp->procFlags = PROC_ON_ANY_HOSTILE_ACTION | PROC_TARGET_SELF;
	//		sp->procChance = 4;
	//	}
	//	//Shadow Trance should be removed on the first SB
	//	sp = dbcSpell.LookupEntry( 17941 );
	//	if( sp != NULL )
	//	{
	//		sp->AuraInterruptFlags = AURA_INTERRUPT_ON_CAST_SPELL;
	//		sp->EffectSpellGroupRelation[0] = 1;
	//	}
	//	//Fel Concentration
	//	sp = dbcSpell.LookupEntry( 17783 );
	//	if( sp != NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8 | 16 | 16384;
	//	}
	//	sp = dbcSpell.LookupEntry( 17784 );
	//	if( sp != NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8 | 16 | 16384;
	//	}
	//	sp = dbcSpell.LookupEntry( 17785 );
	//	if( sp != NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8 | 16 | 16384;
	//	}
	//	sp = dbcSpell.LookupEntry( 17786 );
	//	if( sp != NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8 | 16 | 16384;
	//	}
	//	sp = dbcSpell.LookupEntry( 17787 );
	//	if( sp != NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8 | 16 | 16384;
	//	}
	//	//warlock: Contagion
	//	sp = dbcSpell.LookupEntry( 30060 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1024 | 2;
	//		sp->EffectSpellGroupRelation_high[0] = 32784;
	//		sp->EffectSpellGroupRelation[1] = 1024 | 2;
	//		sp->EffectSpellGroupRelation_high[1] = 32784;
	//		sp->EffectSpellGroupRelation[2] = 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144UL | 16 | 524288UL | 4194304UL;
	//	}
	//	sp = dbcSpell.LookupEntry( 30061 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1024 | 2;
	//		sp->EffectSpellGroupRelation_high[0] = 32784;
	//		sp->EffectSpellGroupRelation[1] = 1024 | 2;
	//		sp->EffectSpellGroupRelation_high[1] = 32784;
	//		sp->EffectSpellGroupRelation[2] = 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144UL | 16 | 524288UL | 4194304UL;
	//	}
	//	sp = dbcSpell.LookupEntry( 30062 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1024 | 2;
	//		sp->EffectSpellGroupRelation_high[0] = 32784;
	//		sp->EffectSpellGroupRelation[1] = 1024 | 2;
	//		sp->EffectSpellGroupRelation_high[1] = 32784;
	//		sp->EffectSpellGroupRelation[2] = 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144UL | 16 | 524288UL | 4194304UL;
	//	}
	//	sp = dbcSpell.LookupEntry( 30063 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1024 | 2;
	//		sp->EffectSpellGroupRelation_high[0] = 32784;
	//		sp->EffectSpellGroupRelation[1] = 1024 | 2;
	//		sp->EffectSpellGroupRelation_high[1] = 32784;
	//		sp->EffectSpellGroupRelation[2] = 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144UL | 16 | 524288UL | 4194304UL;
	//	}
	//	sp = dbcSpell.LookupEntry( 30064 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1024 | 2;
	//		sp->EffectSpellGroupRelation_high[0] = 32784;
	//		sp->EffectSpellGroupRelation[1] = 1024 | 2;
	//		sp->EffectSpellGroupRelation_high[1] = 32784;
	//		sp->EffectSpellGroupRelation[2] = 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144UL | 16 | 524288UL | 4194304UL;
	//	}

	//	//warlock: Malediction
	//	sp = dbcSpell.LookupEntry( 32477 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2097152;
	//		sp->EffectSpellGroupRelation_high[0] = 512;
	//	}
	//	sp = dbcSpell.LookupEntry( 32483 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2097152;
	//		sp->EffectSpellGroupRelation_high[0] = 512;
	//	}
	//	sp = dbcSpell.LookupEntry( 32484 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2097152;
	//		sp->EffectSpellGroupRelation_high[0] = 512;
	//	}

	//	//warlock: Improved Searing Pain
	//	sp = dbcSpell.LookupEntry( 17927 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 256;
	//	sp = dbcSpell.LookupEntry( 17929 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 256;
	//	sp = dbcSpell.LookupEntry( 17930 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 256;

	//	//warlock: Empowered Corruption
	//	sp = dbcSpell.LookupEntry( 32381 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectBasePoints[0] *= 6;
	//		sp->EffectSpellGroupRelation[0] = 2;
	//	}
	//	sp = dbcSpell.LookupEntry( 32382 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectBasePoints[0] *= 6;
	//		sp->EffectSpellGroupRelation[0] = 2;
	//	}
	//	sp = dbcSpell.LookupEntry( 32383 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectBasePoints[0] *= 6;
	//		sp->EffectSpellGroupRelation[0] = 2;
	//	}

	//	//warlock: Improved Enslave Demon
	//	sp = dbcSpell.LookupEntry( 18821 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2048;
	//		sp->EffectMiscValue[0]=SMT_SPELL_VALUE_PCT;
	//		sp->EffectBasePoints[0] = -(sp->EffectBasePoints[0]+2);
	//		//sp->EffectSpellGroupRelation[1] = 2048; //we do not handle this misc type yet anyway. Removed it just as a reminder
	//		sp->EffectSpellGroupRelation[2] = 2048;
	//	}
	//	sp = dbcSpell.LookupEntry( 18822 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2048;
	//		sp->EffectSpellGroupRelation[1] = 2048;
	//		sp->EffectSpellGroupRelation[2] = 2048;
	//	}
	//	//warlock: Devastation
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4 | 1 | 64 | 256 | 32 | 128 | 512; //destruction spells
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 |4096;
	//	}
	//	sp = dbcSpell.LookupEntry( 18131 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4 | 1 | 64 | 256 | 32 | 128 | 512;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 |4096;
	//	}
	//	sp = dbcSpell.LookupEntry( 18132 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4 | 1 | 64 | 256 | 32 | 128 | 512;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 |4096;
	//	}
	//	sp = dbcSpell.LookupEntry( 18133 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4 | 1 | 64 | 256 | 32 | 128 | 512;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 |4096;
	//	}
	//	sp = dbcSpell.LookupEntry( 18134 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4 | 1 | 64 | 256 | 32 | 128 | 512;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 |4096;
	//	}

	//	//warlock - Shadow Mastery
	//	sp = dbcSpell.LookupEntry( 18271 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] =  2147483648UL | 4194304 | 1 | 2 | 16384 | 1024 | 8 | 262144 | 524288 | 2147483648UL | 16777216UL | 128 | 16 | 32768;
	//		sp->EffectSpellGroupRelation[1] =  2147483648UL | 4194304 | 1 | 2 | 16384 | 1024 | 8 | 262144 | 524288 | 2147483648UL | 16777216UL | 128 | 16 | 32768;
	//	}
	//	sp = dbcSpell.LookupEntry( 18272 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] =  2147483648UL | 4194304 | 1 | 2 | 16384 | 1024 | 8 | 262144 | 524288 | 2147483648UL | 16777216UL | 128 | 16 | 32768;
	//		sp->EffectSpellGroupRelation[1] =  2147483648UL | 4194304 | 1 | 2 | 16384 | 1024 | 8 | 262144 | 524288 | 2147483648UL | 16777216UL | 128 | 16 | 32768;
	//	}
	//	sp = dbcSpell.LookupEntry( 18273 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] =  2147483648UL | 4194304 | 1 | 2 | 16384 | 1024 | 8 | 262144 | 524288 | 2147483648UL | 16777216UL | 128 | 16 | 32768;
	//		sp->EffectSpellGroupRelation[1] =  2147483648UL | 4194304 | 1 | 2 | 16384 | 1024 | 8 | 262144 | 524288 | 2147483648UL | 16777216UL | 128 | 16 | 32768;
	//	}
	//	sp = dbcSpell.LookupEntry( 18274 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] =  2147483648UL | 4194304 | 1 | 2 | 16384 | 1024 | 8 | 262144 | 524288 | 2147483648UL | 16777216UL | 128 | 16 | 32768;
	//		sp->EffectSpellGroupRelation[1] =  2147483648UL | 4194304 | 1 | 2 | 16384 | 1024 | 8 | 262144 | 524288 | 2147483648UL | 16777216UL | 128 | 16 | 32768;
	//	}
	//	sp = dbcSpell.LookupEntry( 18275 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] =  2147483648UL | 4194304 | 1 | 2 | 16384 | 1024 | 8 | 262144 | 524288 | 2147483648UL | 16777216UL | 128 | 16 | 32768;
	//		sp->EffectSpellGroupRelation[1] =  2147483648UL | 4194304 | 1 | 2 | 16384 | 1024 | 8 | 262144 | 524288 | 2147483648UL | 16777216UL | 128 | 16 | 32768;
	//	}
	//	//warlock - Improved Curse of Weakness
	//	sp = dbcSpell.LookupEntry( 18179 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 32768;
	//	sp = dbcSpell.LookupEntry( 18180 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 32768;

	//	//warlock - Improved Howl of Terror
	//	sp = dbcSpell.LookupEntry( 30054 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation_high[0] = 8;
	//	sp = dbcSpell.LookupEntry( 30057 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation_high[0] = 8;

	//	//warlock - Emberstorm
	//	sp = dbcSpell.LookupEntry( 17954 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32 | 64 | 4 | 256 | 512;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128;
	//		sp->EffectSpellGroupRelation[1] = 4;
	//		
	//	}
	//	sp = dbcSpell.LookupEntry( 17955 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32 | 64 | 4 | 256 | 512;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128;
	//		sp->EffectSpellGroupRelation[1] = 4;
	//	}
	//	sp = dbcSpell.LookupEntry( 17956 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32 | 64 | 4 | 256 | 512;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128;
	//		sp->EffectSpellGroupRelation[1] = 4;
	//	}
	//	sp = dbcSpell.LookupEntry( 17957 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32 | 64 | 4 | 256 | 512;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128;
	//		sp->EffectSpellGroupRelation[1] = 4;
	//	}
	//	sp = dbcSpell.LookupEntry( 17958 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32 | 64 | 4 | 256 | 512;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128;
	//		sp->EffectSpellGroupRelation[1] = 4;
	//	}

	//	//warlock - Shadow and Flame
	//	sp = dbcSpell.LookupEntry( 30288 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1;
	//		sp->EffectSpellGroupRelation_high[0] = 64;
	//	}
	//	sp = dbcSpell.LookupEntry( 30289 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1;
	//		sp->EffectSpellGroupRelation_high[0] = 64;
	//	}
	//	sp = dbcSpell.LookupEntry( 30290 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1;
	//		sp->EffectSpellGroupRelation_high[0] = 64;
	//	}
	//	sp = dbcSpell.LookupEntry( 30291 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1;
	//		sp->EffectSpellGroupRelation_high[0] = 64;
	//	}
	//	sp = dbcSpell.LookupEntry( 30292 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1;
	//		sp->EffectSpellGroupRelation_high[0] = 64;
	//	}

	//	//warlock - Ruin
	//	sp = dbcSpell.LookupEntry( 17959 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 4096 ;
	//	}

	//	//warlock - destructive reach 
	//	sp = dbcSpell.LookupEntry( 17917 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 4096;
	//		sp->EffectSpellGroupRelation[1] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[1] = 64 | 128 | 4096 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 17918 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 4096;
	//		sp->EffectSpellGroupRelation[1] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[1] = 64 | 128 | 4096 ;
	//	}

	//	//warlock - Cataclysm 
	//	sp = dbcSpell.LookupEntry( 17778 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 4096 ;
	//		sp->EffectSpellGroupRelation[1] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[1] = 64 | 128 | 4096 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 17779 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 4096;
	//		sp->EffectSpellGroupRelation[1] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[1] = 64 | 128 | 4096 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 17780 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 4096;
	//		sp->EffectSpellGroupRelation[1] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[1] = 64 | 128 | 4096 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 17781 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 4096;
	//		sp->EffectSpellGroupRelation[1] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[1] = 64 | 128 | 4096 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 17782 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 4096;
	//		sp->EffectSpellGroupRelation[1] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[1] = 64 | 128 | 4096 ;
	//	}

	//	//warlock - Intensity 
	//	sp = dbcSpell.LookupEntry( 18135 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 4096 ;
	//	}
	//	sp = dbcSpell.LookupEntry( 18136 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4 | 32 | 64 | 128 | 256 | 512 ;
	//		sp->EffectSpellGroupRelation_high[0] = 64 | 128 | 4096 ;
	//	}

	//	//warlock - Suppression
	//	sp = dbcSpell.LookupEntry( 18174 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//	}
	//	sp = dbcSpell.LookupEntry( 18175 );
	//	if( sp != NULL )
	//		{
	//		sp->EffectSpellGroupRelation[0] = 1 | 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//	}
	//	sp = dbcSpell.LookupEntry( 18176 );
	//	if( sp != NULL )
	//		{
	//		sp->EffectSpellGroupRelation[0] = 1 | 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//	}
	//	sp = dbcSpell.LookupEntry( 18177 );
	//	if( sp != NULL )
	//		{
	//		sp->EffectSpellGroupRelation[0] = 1 | 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//	}
	//	sp = dbcSpell.LookupEntry( 18178 );
	//	if( sp != NULL )
	//		{
	//		sp->EffectSpellGroupRelation[0] = 1 | 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//		sp->EffectSpellGroupRelation_high[0] = 1 | 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//	}
	//	//warlock - Improved Curse of Agony
	//	sp = dbcSpell.LookupEntry( 18827 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1024;
	//	sp = dbcSpell.LookupEntry( 18829 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1024;

	//	//warlock - Grim Reach

	//	sp = dbcSpell.LookupEntry( 18792 );
	//	if( sp != NULL )
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_ON_PET;
	//	sp = dbcSpell.LookupEntry( 35701 );
	//	if( sp != NULL )
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_ON_PET;

	//	//warlock - Demonic Tactics
	//	sp = dbcSpell.LookupEntry( 30242 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 0; //disble this. This is just blizz crap. Pure proove that they suck :P
	//		sp->EffectImplicitTargetB[1] = EFF_TARGET_PET;
	//		sp->EffectApplyAuraName[2] = SPELL_AURA_MOD_SPELL_CRIT_CHANCE; //lvl 1 has it fucked up :O
	//		sp->EffectImplicitTargetB[2] = EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//	}
	//	sp = dbcSpell.LookupEntry( 30245 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 0; //disble this. This is just blizz crap. Pure proove that they suck :P
	//		sp->EffectImplicitTargetB[1] = EFF_TARGET_PET;
	//		sp->EffectImplicitTargetB[2] = EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//	}
	//	sp = dbcSpell.LookupEntry( 30246 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 0; //disble this. This is just blizz crap. Pure proove that they suck :P
	//		sp->EffectImplicitTargetB[1] = EFF_TARGET_PET;
	//		sp->EffectImplicitTargetB[2] = EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//	}
	//	sp = dbcSpell.LookupEntry( 30247 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 0; //disble this. This is just blizz crap. Pure proove that they suck :P
	//		sp->EffectImplicitTargetB[1] = EFF_TARGET_PET;
	//		sp->EffectImplicitTargetB[2] = EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//	}
	//	sp = dbcSpell.LookupEntry( 30248 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 0; //disble this. This is just blizz crap. Pure proove that they suck :P
	//		sp->EffectImplicitTargetB[1] = EFF_TARGET_PET;
	//		sp->EffectImplicitTargetB[2] = EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//	}

	//	//warlock - Demonic Resilience
	//	sp = dbcSpell.LookupEntry( 30319 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//	}
	//	sp = dbcSpell.LookupEntry( 30320 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//	}
	//	sp = dbcSpell.LookupEntry( 30321 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_PET_OWNER ;
	//	}

	//	//warlock - Improved Imp
	//	sp = dbcSpell.LookupEntry( 18694 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 4096 | 8388608;
	//	}
	//	sp = dbcSpell.LookupEntry( 18695 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 4096 | 8388608;
	//	}
	//	sp = dbcSpell.LookupEntry( 18696 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 4096 | 8388608;
	//	}

	//	//warlock - Improved Voidwalker
	//	sp = dbcSpell.LookupEntry( 18705 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 33554432;
	//	}
	//	sp = dbcSpell.LookupEntry( 18706 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 33554432;
	//	}
	//	sp = dbcSpell.LookupEntry( 18707 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 33554432;
	//	}

	//	//warlock - Improved Succubus
	//	sp = dbcSpell.LookupEntry( 18754 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 8192 | 1073741824;
	//		sp->EffectSpellGroupRelation[1] = 1073741824;
	//	}
	//	sp = dbcSpell.LookupEntry( 18755 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 8192 | 1073741824;
	//		sp->EffectSpellGroupRelation[1] = 1073741824;
	//	}
	//	sp = dbcSpell.LookupEntry( 18756 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 8192 | 1073741824;
	//		sp->EffectSpellGroupRelation[1] = 1073741824;
	//	}

	//	//warlock - Fel Intellect
	//	sp = dbcSpell.LookupEntry( 18731 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_PERCENT_STAT;
	//		sp->EffectMiscValue[0] = 3;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 18743 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_PERCENT_STAT;
	//		sp->EffectMiscValue[0] = 3;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 18744 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_PERCENT_STAT;
	//		sp->EffectMiscValue[0] = 3;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}

	//	//warlock - Fel Stamina
	//	sp = dbcSpell.LookupEntry( 18748 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_PERCENT_STAT;
	//		sp->EffectMiscValue[0] = 2;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 18749 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_PERCENT_STAT;
	//		sp->EffectMiscValue[0] = 2;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}
	//	sp = dbcSpell.LookupEntry( 18750 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_MOD_PERCENT_STAT;
	//		sp->EffectMiscValue[0] = 2;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	}

	//	//warlock - Demonic Tactics
	//	sp = dbcSpell.LookupEntry( 30242 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		//this is required since blizz uses spells for melee attacks while we use fixed functions
	//		sp->Effect[1] = SPELL_EFFECT_APPLY_AURA;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->EffectMiscValue[1] = SCHOOL_NORMAL;
	//		sp->EffectBasePoints[1] = sp->EffectBasePoints[0] ;
	//	}

	//	//warlock - Unholy Power
	//	sp = dbcSpell.LookupEntry( 18769 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 4096;
	//		//this is required since blizz uses spells for melee attacks while we use fixed functions
	//		sp->Effect[1] = SPELL_EFFECT_APPLY_AURA;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->EffectMiscValue[1] = SCHOOL_NORMAL;
	//		sp->EffectBasePoints[1] = sp->EffectBasePoints[0] ;
	//	}
	//	sp = dbcSpell.LookupEntry( 18770 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 4096;
	//		//this is required since blizz uses spells for melee attacks while we use fixed functions
	//		sp->Effect[1] = SPELL_EFFECT_APPLY_AURA;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->EffectMiscValue[1] = SCHOOL_NORMAL;
	//		sp->EffectBasePoints[1] = sp->EffectBasePoints[0] ;
	//	}
	//	sp = dbcSpell.LookupEntry( 18771 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 4096;
	//		//this is required since blizz uses spells for melee attacks while we use fixed functions
	//		sp->Effect[1] = SPELL_EFFECT_APPLY_AURA;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->EffectMiscValue[1] = SCHOOL_NORMAL;
	//		sp->EffectBasePoints[1] = sp->EffectBasePoints[0] ;
	//	}
	//	sp = dbcSpell.LookupEntry( 18772 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 4096;
	//		//this is required since blizz uses spells for melee attacks while we use fixed functions
	//		sp->Effect[1] = SPELL_EFFECT_APPLY_AURA;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->EffectMiscValue[1] = SCHOOL_NORMAL;
	//		sp->EffectBasePoints[1] = sp->EffectBasePoints[0] ;
	//	}
	//	sp = dbcSpell.LookupEntry( 18773 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET ;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//		sp->EffectSpellGroupRelation[0] = 4096;
	//		//this is required since blizz uses spells for melee attacks while we use fixed functions
	//		sp->Effect[1] = SPELL_EFFECT_APPLY_AURA;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_PET;
	//		sp->EffectMiscValue[1] = SCHOOL_NORMAL;
	//		sp->EffectBasePoints[1] = sp->EffectBasePoints[0] ;
	//	}

	//	//warlock - Master Demonologist - 25 spells here
	//	sp = dbcSpell.LookupEntry( 23785 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 23784;
	//	}
	//	sp = dbcSpell.LookupEntry( 23822 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 23830;
	//	}
	//	sp = dbcSpell.LookupEntry( 23823 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 23831;
	//	}
	//	sp = dbcSpell.LookupEntry( 23824 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 23832;
	//	}
	//	sp = dbcSpell.LookupEntry( 23825 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_CASTED_ON_PET_SUMMON_ON_PET | SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 35708;
	//	}
	//	//and the rest
	//	sp = dbcSpell.LookupEntry( 23784 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	sp = dbcSpell.LookupEntry( 23830 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	sp = dbcSpell.LookupEntry( 23831 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	sp = dbcSpell.LookupEntry( 23832 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	sp = dbcSpell.LookupEntry( 35708 );
	//	if( sp != NULL )
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_PET;
	//	sp = dbcSpell.LookupEntry( 23759 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//	}
	//	sp = dbcSpell.LookupEntry( 23760 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//	}
	//	sp = dbcSpell.LookupEntry( 23761 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//	}
	//	sp = dbcSpell.LookupEntry( 23762 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//	}
	//	sp = dbcSpell.LookupEntry( 23826 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//	}
	//	sp = dbcSpell.LookupEntry( 23827 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//	}
	//	sp = dbcSpell.LookupEntry( 23828 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//	}
	//	sp = dbcSpell.LookupEntry( 23829 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//	}
	//	for(uint32 i=23833;i<=23844;i++)
	//	{
	//		sp = dbcSpell.LookupEntry( i );
	//		if( sp != NULL )
	//		{
	//			sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//			sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//		}
	//	}
	//	sp = dbcSpell.LookupEntry( 35702 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//		sp->Effect[1] = 0; //hacks, we are handling this in another way
	//	}
	//	sp = dbcSpell.LookupEntry( 35703 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//		sp->Effect[1] = 0; //hacks, we are handling this in another way
	//	}
	//	sp = dbcSpell.LookupEntry( 35704 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//		sp->Effect[1] = 0; //hacks, we are handling this in another way
	//	}
	//	sp = dbcSpell.LookupEntry( 35705 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//		sp->Effect[1] = 0; //hacks, we are handling this in another way
	//	}
	//	sp = dbcSpell.LookupEntry( 35706 );
	//	if( sp != NULL )
	//	{
	//		sp->c_is_flags |= SPELL_FLAG_IS_EXPIREING_WITH_PET;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//		sp->Effect[1] = 0; //hacks, we are handling this in another way
	//	}
	//	//warlock - Improved Healthstone
	//	sp = dbcSpell.LookupEntry( 18692 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SELF;
	//		sp->EffectSpellGroupRelation[0] = 65536;
	//	}
	//	sp = dbcSpell.LookupEntry( 18693 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_ADD_PCT_MODIFIER;
	//		sp->EffectMiscValue[0] = SMT_SPELL_VALUE;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SELF;
	//		sp->EffectSpellGroupRelation[0] = 65536;
	//	}

	//	//warlock - Improved Drain Soul
	//	sp = dbcSpell.LookupEntry( 18213 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_TARGET_DIE | PROC_TARGET_SELF;
	//		sp->procChance = 100;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 18371;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SELF;
	//		sp->EffectSpellGroupRelation[1] = 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//		sp->Effect[2] = 0 ; //remove this effect
	//	}
	//	sp = dbcSpell.LookupEntry( 18372 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_TARGET_DIE | PROC_TARGET_SELF;
	//		sp->procChance = 100;
	//		sp->Effect[0] = SPELL_EFFECT_APPLY_AURA;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 18371;
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_SELF;
	//		sp->EffectSpellGroupRelation[1] = 2 | 8 | 32768 | 2147483648UL | 1024 | 16384 | 262144 | 16 | 524288 | 4194304;
	//		sp->Effect[2] = 0 ; //remove this effect
	//	}

	//	//warlock - Shadow Embrace
	//	sp = dbcSpell.LookupEntry( 32385 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->Effect[1] = 0 ; //remove this effect ? Maybe remove the other one :P xD
	//	}
	//	sp = dbcSpell.LookupEntry( 32387 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->Effect[1] = 0 ; //remove this effect ? Maybe remove the other one :P xD
	//	}
	//	sp = dbcSpell.LookupEntry( 32392 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->Effect[1] = 0 ; //remove this effect ? Maybe remove the other one :P xD
	//	}
	//	sp = dbcSpell.LookupEntry( 32393 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->Effect[1] = 0 ; //remove this effect ? Maybe remove the other one :P xD
	//	}
	//	sp = dbcSpell.LookupEntry( 32394 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->Effect[1] = 0 ; //remove this effect ? Maybe remove the other one :P xD
	//	}

	//	//warlock - Bane
	//	sp = dbcSpell.LookupEntry( 17788 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//		sp->EffectSpellGroupRelation_high[1] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 17789 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//		sp->EffectSpellGroupRelation_high[1] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 17790 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//		sp->EffectSpellGroupRelation_high[1] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 17791 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//		sp->EffectSpellGroupRelation_high[1] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 17792 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//		sp->EffectSpellGroupRelation_high[1] = 128;
	//	}

	//	//warlock - soul leech
	//	sp = dbcSpell.LookupEntry( 30293 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; //aura
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 30294;
	//		sp->procFlags = uint32(PROC_ON_CAST_SPELL|PROC_TARGET_SELF);
	//	}
	//	sp = dbcSpell.LookupEntry( 30295 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; //aura
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 30294;
	//		sp->procFlags = uint32(PROC_ON_CAST_SPELL|PROC_TARGET_SELF);
	//	}
	//	sp = dbcSpell.LookupEntry( 30296 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; //aura
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 30294;
	//		sp->procFlags = uint32(PROC_ON_CAST_SPELL|PROC_TARGET_SELF);
	//	}

	//	//warlock - Pyroclasm
	//	sp = dbcSpell.LookupEntry( 18073 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 0; //delete this owerride effect :P
	//		sp->EffectTriggerSpell[1] = 18093; //trigger spell was wrong :P
	//		sp->procFlags = PROC_ON_ANY_HOSTILE_ACTION;
	//		sp->procChance = 13; //god, save us from fixed values !
	//	}
	//	sp = dbcSpell.LookupEntry( 18096 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 0; //delete this owerride effect :P
	//		sp->EffectTriggerSpell[1] = 18093; //trigger spell was wrong :P
	//		sp->procFlags = PROC_ON_ANY_HOSTILE_ACTION;
	//		sp->procChance = 26; //god, save us from fixed values !
	//	}

	////////////////////////////////////////////
	//// CLASS_DRUID								//
	////////////////////////////////////////////

	//// Insert druid spell fixes here

	//	//Druid: Feral Swiftness
	//	sp = dbcSpell.LookupEntry( 17002 );
	//	if ( sp != NULL )
	//	{
	//		sp->Effect[1] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[1] = 24864;
	//	}
	//	sp = dbcSpell.LookupEntry( 24866 );
	//	if ( sp != NULL )
	//	{
	//		sp->Effect[1] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[1] = 24867;
	//	}

	//	//Druid: Natural Perfection
	//	sp = dbcSpell.LookupEntry( 33881 );
	//	if ( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;
	//	sp = dbcSpell.LookupEntry( 33882 );
	//	if ( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;
	//	sp = dbcSpell.LookupEntry( 33883 );
	//	if ( sp != NULL )
	//		sp->procFlags = PROC_ON_CRIT_HIT_VICTIM;

	//	//Druid: Frenzied Regeneration
	//	sp = dbcSpell.LookupEntry( 22842 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PERIODIC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 22845;
	//	}
	//	sp = dbcSpell.LookupEntry( 22895 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PERIODIC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 22845;
	//	}
	//	sp = dbcSpell.LookupEntry( 22896 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PERIODIC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 22845;
	//	}
	//	sp = dbcSpell.LookupEntry( 26999 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PERIODIC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 22845;
	//	}

	//	//Druid - Ferocity.
	//	sp = dbcSpell.LookupEntry( 16934 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2048;
	//		sp->EffectSpellGroupRelation_high[0] = 1048576 | 64;
	//		sp->EffectSpellGroupRelation[1] = 4096;
	//		sp->EffectSpellGroupRelation_high[1] = 1024;
	//	}
	//	sp = dbcSpell.LookupEntry( 16935 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2048;
	//		sp->EffectSpellGroupRelation_high[0] = 1048576 | 64;
	//		sp->EffectSpellGroupRelation[1] = 4096;
	//		sp->EffectSpellGroupRelation_high[1] = 1024;
	//	}
	//	sp = dbcSpell.LookupEntry( 16936 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2048;
	//		sp->EffectSpellGroupRelation_high[0] = 1048576 | 64;
	//		sp->EffectSpellGroupRelation[1] = 4096;
	//		sp->EffectSpellGroupRelation_high[1] = 1024;
	//	}
	//	sp = dbcSpell.LookupEntry( 16937 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2048;
	//		sp->EffectSpellGroupRelation_high[0] = 1048576 | 64;
	//		sp->EffectSpellGroupRelation[1] = 4096;
	//		sp->EffectSpellGroupRelation_high[1] = 1024;
	//	}
	//	sp = dbcSpell.LookupEntry( 16938 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2048;
	//		sp->EffectSpellGroupRelation_high[0] = 1048576 | 64;
	//		sp->EffectSpellGroupRelation[1] = 4096;
	//		sp->EffectSpellGroupRelation_high[1] = 1024;
	//	}

	//	//Druid - Focused Starlight
	//	sp = dbcSpell.LookupEntry( 35363 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//	sp = dbcSpell.LookupEntry( 35364 ); 
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;

	//	//Druid - Celestial Focus
	//	sp = dbcSpell.LookupEntry( 16850 ); 
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectSpellGroupRelation[1] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 16923 ); 
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectSpellGroupRelation[1] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 16924 ); 
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectSpellGroupRelation[1] = 1;
	//	}

	//	//Druid - Feral Aggression. Blizz made a mistake here ?
	//	sp = dbcSpell.LookupEntry( 16858 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8;
	//		sp->EffectSpellGroupRelation[1] = 8388608;
	//	}
	//	sp = dbcSpell.LookupEntry( 16859 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8;
	//		sp->EffectSpellGroupRelation[1] = 8388608;
	//	}
	//	sp = dbcSpell.LookupEntry( 16860 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8;
	//		sp->EffectSpellGroupRelation[1] = 8388608;
	//	}
	//	sp = dbcSpell.LookupEntry( 16861 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8;
	//		sp->EffectSpellGroupRelation[1] = 8388608;
	//	}
	//	sp = dbcSpell.LookupEntry( 16862 ); 
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8;
	//		sp->EffectSpellGroupRelation[1] = 8388608;
	//	}
	//	//Druid: Leader of the Pack
	//	sp = dbcSpell.LookupEntry( 24932 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[1] = 0;
	//		sp->Effect[2] = 0; //removing strange effects.
	//	}

	//	//Druid: Improved Leader of the Pack
	//	sp = dbcSpell.LookupEntry( 34299 );
	//	if( sp != NULL )
	//		sp->proc_interval = 6000;//6 secs
	//	//druid Savage Fury
	//	sp = dbcSpell.LookupEntry( 16998 );
	//	if( sp != NULL ) 
	//	{
	//		sp->EffectSpellGroupRelation_high[0] |= 1024;
	//		//sp->EffectSpellGroupRelation_high[1] |= 1024;
	//		//sp->EffectSpellGroupRelation_high[2] |= 1024;
	//	}
	//	sp = dbcSpell.LookupEntry( 16999 );
	//	if( sp != NULL ) 
	//	{
	//		sp->EffectSpellGroupRelation_high[0] |= 1024;
	//		//sp->EffectSpellGroupRelation_high[1] |= 1024;
	//		//sp->EffectSpellGroupRelation_high[2] |= 1024;
	//	}

	//	//druid - Blood Frenzy
	//	sp = dbcSpell.LookupEntry( 16954 );
	//	if( sp != NULL ) 
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 16953;
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->procChance = 100;
	//	}
	//	sp = dbcSpell.LookupEntry( 16952 );
	//	if( sp != NULL ) 
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 16953;
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->procChance = 50;
	//	}

	//	//druid - Primal Fury
	//	sp = dbcSpell.LookupEntry( 16961 );
	//	if( sp != NULL ) 
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 16959;
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->procChance = 100;
	//	}
	//	sp = dbcSpell.LookupEntry( 16958 );
	//	if( sp != NULL ) 
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 16959;
	//		sp->procFlags = PROC_ON_CRIT_ATTACK;
	//		sp->procChance = 50;
	//	}

	//	//druid - Intensity
	//	sp = dbcSpell.LookupEntry( 17106 );
	//	if( sp != NULL )
	//	{
	//	   sp->EffectApplyAuraName[1] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//	   sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 17107 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		 sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 17108 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	//Nature's Grasp
	//	sp = dbcSpell.LookupEntry( 16689 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; 
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 339; 
	//		sp->Effect[1] = 0; 
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM | PROC_REMOVEONUSE;
	//		sp->AuraInterruptFlags = 0; //we remove it on proc or timeout
	//	}
	//	sp = dbcSpell.LookupEntry( 16810 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; 
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 1062; 
	//		sp->Effect[1] = 0; 
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM | PROC_REMOVEONUSE;
	//		sp->AuraInterruptFlags = 0; //we remove it on proc or timeout
	//	}
	//	sp = dbcSpell.LookupEntry( 16811 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; 
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 5195; 
	//		sp->Effect[1] = 0; 
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM | PROC_REMOVEONUSE;
	//		sp->AuraInterruptFlags = 0; //we remove it on proc or timeout
	//	}
	//	sp = dbcSpell.LookupEntry( 16812 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; 
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 5196; 
	//		sp->Effect[1] = 0; 
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM | PROC_REMOVEONUSE;
	//		sp->AuraInterruptFlags = 0; //we remove it on proc or timeout
	//	}
	//	sp = dbcSpell.LookupEntry( 16813 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; 
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 9852; 
	//		sp->Effect[1] = 0; 
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM | PROC_REMOVEONUSE;
	//		sp->AuraInterruptFlags = 0; //we remove it on proc or timeout
	//	}
	//	sp = dbcSpell.LookupEntry( 17329 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; 
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 9853; 
	//		sp->Effect[1] = 0; 
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM | PROC_REMOVEONUSE;
	//		sp->AuraInterruptFlags = 0; //we remove it on proc or timeout
	//	}
	//	sp = dbcSpell.LookupEntry( 27009 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6; 
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 26989; 
	//		sp->Effect[1] = 0; 
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM | PROC_REMOVEONUSE;
	//		sp->AuraInterruptFlags = 0; //we remove it on proc or timeout
	//	}
	//	//pounce
	//	sp = dbcSpell.LookupEntry( 9007 );
	//	if( sp != NULL )
	//	{
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	}
	//	sp = dbcSpell.LookupEntry( 9824 );
	//	if( sp != NULL )
	//	{
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	}
	//	sp = dbcSpell.LookupEntry( 9826 );
	//	if( sp != NULL )
	//	{
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	}
	//	sp = dbcSpell.LookupEntry( 27007 );
	//	if( sp != NULL )
	//	{
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	}
	//	//rip
	//	sp = dbcSpell.LookupEntry( 1079 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 9492 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 9493 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 9752 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 9894 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 9896 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 27008 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	//rake
	//	sp = dbcSpell.LookupEntry( 1822 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 1823 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 1824 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 9904 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	sp = dbcSpell.LookupEntry( 27003 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;
	//	//lacerate
	//	sp = dbcSpell.LookupEntry( 33745 );
	//	if( sp != NULL )
	//		sp->MechanicsType = MECHANIC_BLEEDING;

	//	//Pounce Bleed
	//	sp = dbcSpell.LookupEntry( 9007 );
	//	if( sp != NULL )
	//		sp->DurationIndex = 18000;
	//	sp = dbcSpell.LookupEntry( 9824 );
	//	if( sp != NULL )
	//		sp->DurationIndex = 18000;
	//	sp = dbcSpell.LookupEntry( 9826 );
	//	if( sp != NULL )
	//		sp->DurationIndex = 18000;
	//	sp = dbcSpell.LookupEntry( 27007 );
	//	if( sp != NULL )
	//		sp->DurationIndex = 18000;

	//	//Druid: Natural Shapeshifter
	//	sp = dbcSpell.LookupEntry( 16833 );
	//	if( sp != NULL )
	//		sp->DurationIndex = 0;
	//	sp = dbcSpell.LookupEntry( 16834 );
	//	if( sp != NULL )
	//		sp->DurationIndex = 0;
	//	sp = dbcSpell.LookupEntry( 16835 );
	//	if( sp != NULL )
	//		sp->DurationIndex = 0;
	//	// druid - Tree of Life
	//	sp = dbcSpell.LookupEntry( 5420 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[2] = 32 | 64 | 16; //for the mana cost tweak
	//		sp->EffectSpellGroupRelation_high[2] = 2 | 4096 | 128 | 524288 ;
	//	}

	//	// druid - Shredding Attacks
	//	sp = dbcSpell.LookupEntry( 16966 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32768;
	//		sp->EffectSpellGroupRelation_high[1] = 256;
	//	}
	//	sp = dbcSpell.LookupEntry( 16968 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32768;
	//		sp->EffectSpellGroupRelation_high[1] = 256;
	//	}

	//	// druid - Naturalist
	//	sp = dbcSpell.LookupEntry( 17069 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectMiscValue[1] = 1;  
	//	}
	//	sp = dbcSpell.LookupEntry( 17070 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectMiscValue[1] = 1; 
	//	}
	//	sp = dbcSpell.LookupEntry( 17071 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectMiscValue[1] = 1; 
	//	}
	//	sp = dbcSpell.LookupEntry( 17072 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectMiscValue[1] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 17073 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_MOD_DAMAGE_PERCENT_DONE;
	//		sp->EffectMiscValue[1] = 1; 
	//	}

	//	// druid - Gift of Nature
	//	sp = dbcSpell.LookupEntry( 17104 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 268435456 | 32 | 64 | 16 | 128 | 268435456;
	//		sp->EffectSpellGroupRelation_high[0] |= 16;
	//		sp->EffectSpellGroupRelation[1] = 268435456 | 32 | 64 | 16 | 128 | 268435456;
	//		sp->EffectSpellGroupRelation_high[1] |= 16;
	//	}
	//	sp = dbcSpell.LookupEntry( 24943 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 268435456 | 32 | 64 | 16 | 128 | 268435456;
	//		sp->EffectSpellGroupRelation_high[0] |= 16;
	//		sp->EffectSpellGroupRelation[1] = 268435456 | 32 | 64 | 16 | 128 | 268435456;
	//		sp->EffectSpellGroupRelation_high[1] |= 16;
	//	}
	//	sp = dbcSpell.LookupEntry( 24944 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 268435456 | 32 | 64 | 16 | 128 | 268435456;
	//		sp->EffectSpellGroupRelation_high[0] |= 16;
	//		sp->EffectSpellGroupRelation[1] = 268435456 | 32 | 64 | 16 | 128 | 268435456;
	//		sp->EffectSpellGroupRelation_high[1] |= 16;
	//	}
	//	sp = dbcSpell.LookupEntry( 24945 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 268435456 | 32 | 64 | 16 | 128 | 268435456;
	//		sp->EffectSpellGroupRelation_high[0] |= 16;
	//		sp->EffectSpellGroupRelation[1] = 268435456 | 32 | 64 | 16 | 128 | 268435456;
	//		sp->EffectSpellGroupRelation_high[1] |= 16;
	//	}
	//	sp = dbcSpell.LookupEntry( 24946 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 268435456 | 32 | 64 | 16 | 128 | 268435456;
	//		sp->EffectSpellGroupRelation_high[0] |= 16;
	//		sp->EffectSpellGroupRelation[1] = 268435456 | 32 | 64 | 16 | 128 | 268435456;
	//		sp->EffectSpellGroupRelation_high[1] |= 16;
	//	}

	//	// druid - Empowered Touch
	//	sp = dbcSpell.LookupEntry( 33879 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 32;
	//	sp = dbcSpell.LookupEntry( 33880 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 32;

	//	// druid - Empowered Rejuvenation
	//	sp = dbcSpell.LookupEntry( 33886 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 16 | 128;
	//	sp = dbcSpell.LookupEntry( 33887 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 16 | 128;
	//	sp = dbcSpell.LookupEntry( 33888 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 16 | 128;
	//	sp = dbcSpell.LookupEntry( 33889 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 16 | 128;
	//	sp = dbcSpell.LookupEntry( 33890 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64 | 16 | 128;

	//	// druid - Wrath of cenarius
	//	sp = dbcSpell.LookupEntry( 33603 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4;
	//		sp->EffectSpellGroupRelation[1] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 33604 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4;
	//		sp->EffectSpellGroupRelation[1] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 33605 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4;
	//		sp->EffectSpellGroupRelation[1] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 33606 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4;
	//		sp->EffectSpellGroupRelation[1] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 33607 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4;
	//		sp->EffectSpellGroupRelation[1] = 1;
	//	}

	//	// druid - Nature's Grace
	//	sp = dbcSpell.LookupEntry( 16880 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;

	//	sp = dbcSpell.LookupEntry( 16886 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 0xFFFFFFFF; //all spells, too bad not all spells have grouping flags :S
	//		sp->procCharges = 1;
	//	}

	//	// druid - Starlight Wrath
	//	sp = dbcSpell.LookupEntry( 16814 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//	sp = dbcSpell.LookupEntry( 16815 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//	sp = dbcSpell.LookupEntry( 16816 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//	sp = dbcSpell.LookupEntry( 16817 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;
	//	sp = dbcSpell.LookupEntry( 16818 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1 | 4;

	//	// Druid: Omen of Clarity
	//	sp = dbcSpell.LookupEntry( 16864 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance=100; //procchance dynamic. 3ppm
	//		sp->procFlags = PROC_ON_MELEE_ATTACK | PROC_ON_CRIT_ATTACK;
	//	}
	//	uint32 mm = (1<<(FORM_BEAR-1))|(1<<(FORM_DIREBEAR-1))|(1<<(FORM_MOONKIN-1))|(1<<(FORM_CAT-1));

	//	sp = dbcSpell.LookupEntry( 16972 );
	//	if( sp != NULL )
	//		sp->RequiredShapeShift = mm;
	//	sp = dbcSpell.LookupEntry( 16974 );
	//	if( sp != NULL )
	//		sp->RequiredShapeShift = mm;
	//	sp = dbcSpell.LookupEntry( 16975 );
	//	if( sp != NULL )
	//		sp->RequiredShapeShift = mm;

	////////////////////////////////////////////
	//// ITEMS								//
	////////////////////////////////////////////

	//// Insert items spell fixes here

	//#ifdef NEW_PROCFLAGS
	//	//Bonescythe Armor
	//	sp = dbcSpell.LookupEntry( 28814 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=8519680;

	//	//Tome of the Lightbringer
	//	sp = dbcSpell.LookupEntry( 41042 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=8388608;
	//	//Gladiator's Libram of Fortitude
	//	sp = dbcSpell.LookupEntry( 43850 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=8388608;
	//	//Vengeful Gladiator's Libram of Fortitude
	//	sp = dbcSpell.LookupEntry( 43852 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=8388608;
	//	//Merciless Gladiator's Libram of Fortitude
	//	sp = dbcSpell.LookupEntry( 43851 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=8388608;
	//	//Gladiator's Libram of Vengeance
	//	sp = dbcSpell.LookupEntry( 43854 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation_high[0]=64;
	//	//Merciless Gladiator's Libram of Vengeance
	//	sp = dbcSpell.LookupEntry( 43855 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation_high[0]=64;
	//	//Vengeful Gladiator's Libram of Vengeance
	//	sp = dbcSpell.LookupEntry( 43856 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation_high[0]=64;
	//	//The Earthshatterer
	//	sp = dbcSpell.LookupEntry( 28821 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=1024;
	//	//Idol of the White Stag
	//	sp = dbcSpell.LookupEntry( 41037 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation_high[0]=64 | 1024;
	//	//Merciless Gladiator's Idol of Resolve
	//	sp = dbcSpell.LookupEntry( 43842 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation_high[0]=64 | 1024;
	//	//Vengeful Gladiator's Idol of Resolve
	//	sp = dbcSpell.LookupEntry( 43843 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation_high[0]=64 | 1024;
	//	//Merciless Gladiator's Idol of Steadfastness
	//	sp = dbcSpell.LookupEntry( 43844 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=2;
	//	//Vengeful Gladiator's Idol of Steadfastness
	//	sp = dbcSpell.LookupEntry( 43845 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=2;
	//	//Merciless Gladiator's Totem of Indomitability
	//	sp = dbcSpell.LookupEntry( 43858 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation_high[0]=16;
	//	//Vengeful Gladiator's Totem of Indomitability
	//	sp = dbcSpell.LookupEntry( 43859 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation_high[0]=16;
	//	//Gladiator's Totem of Indomitability
	//	sp = dbcSpell.LookupEntry( 43857 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation_high[0]=16;
	//	//Merciless Gladiator's Totem of Survival
	//	sp = dbcSpell.LookupEntry( 43861 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]= 1048576 |268435456 | 2147483648;
	//	//Vengeful Gladiator's Totem of Survival
	//	sp = dbcSpell.LookupEntry( 43862 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]= 1048576 |268435456 | 2147483648;
	//	//Gladiator's Totem of Survival
	//	sp = dbcSpell.LookupEntry( 43861 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]= 1048576 |268435456 | 2147483648;
	//	//Wolfshead Helm
	//	sp = dbcSpell.LookupEntry( 17768 );
	//	if (sp != NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0]= 1073741824;
	//		sp->EffectSpellGroupRelation[1]= 2147483648;
	//	}
	//	//Set: Plagueheart Raiment
	//	sp = dbcSpell.LookupEntry( 28831 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]= 1;
	//	//Set: Gladiator's Idol of Resolve
	//	sp = dbcSpell.LookupEntry( 37191 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation_high[0]=64 | 1024;
	//	//Set: Gladiator's Idol of Steadfastness
	//	sp = dbcSpell.LookupEntry( 43841 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=2;
	//	//Set: Incarnate Raiment
	//	sp = dbcSpell.LookupEntry( 37564 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=512;
	//	//Talon of Al'ar
	//	sp = dbcSpell.LookupEntry( 37507 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=2048;
	//	//Set: Crystalforge Armor
	//	sp = dbcSpell.LookupEntry( 37191 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation_high[0]=64;
	//	//Set: Redemption Armor
	//	sp = dbcSpell.LookupEntry( 28787 );
	//	if (sp != NULL)
	//		sp->EffectSpellGroupRelation[0]=4096;
	//	//Idol of the Claw
	//	sp = dbcSpell.LookupEntry( 34323 );
	//	if( sp != NULL )
	//	{
	//		sp->Flags5 = FLAGS5_PROCCHANCE_COMBOBASED;
	//		sp->EffectSpellGroupRelation[0]=8388608;
	//		sp->EffectSpellGroupRelation_high[0]=128;
	//	}
	//#endif
	//	//Extract Gas 
	//	sp = dbcSpell.LookupEntry( 30427 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = SPELL_EFFECT_DUMMY;
	//	}

	//	//Relic - Idol of the Unseen Moon
	//	sp = dbcSpell.LookupEntry( 43739 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 43740;
	//	}

	//	//Tome of Fiery Redemption
	//	sp = dbcSpell.LookupEntry( 37197 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 37198;
	//		sp->procChance = 15;
	//	}

	//	//Thunderfury
	//	sp = dbcSpell.LookupEntry( 21992 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[2] = SPELL_EFFECT_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[2] = 27648;
	//		sp->EffectImplicitTargetA[2] = EFF_TARGET_SELF;
	//	}

	//	//Solarian's Sapphire
	//	sp = dbcSpell.LookupEntry( 37536 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 65536;

	//	//Totem of the Pulsing Earth
	//	sp = dbcSpell.LookupEntry( 37740 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;

	//	//Totem of the Maelstrom
	//	sp = dbcSpell.LookupEntry( 37738 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64;

	//	//Totem of Living Water
	//	sp = dbcSpell.LookupEntry( 43752 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 256;

	//	//Totem of Healing Rains
	//	sp = dbcSpell.LookupEntry( 38322 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 256;

	//	//Totem of Lightning
	//	sp = dbcSpell.LookupEntry( 33696 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;

	//	//Everbloom Idol
	//	sp = dbcSpell.LookupEntry( 33693 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 32768;

	//	//Idol of the Avian Heart
	//	sp = dbcSpell.LookupEntry( 38321 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 32;

	//	//Idol of the Crescent Goddess
	//	sp = dbcSpell.LookupEntry( 37737 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 64;

	//	//Idol of the Avenger
	//	sp = dbcSpell.LookupEntry( 37760 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 1;

	//	//Energized 
	//	sp = dbcSpell.LookupEntry( 43750 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;

	//	//Spell Haste Trinket
	//	sp = dbcSpell.LookupEntry( 33297 ); 
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL | PROC_TARGET_SELF;

	//	// Band of the Eternal Sage
	//	sp = dbcSpell.LookupEntry( 35083 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;

	//	// Band of the Eternal Restorer 
	//	sp = dbcSpell.LookupEntry( 35086 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;

	//	// Ashtongue Talisman of Shadows 
	//	sp = dbcSpell.LookupEntry( 40478 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;

	//	// Ashtongue Talisman of Swiftness
	//	sp = dbcSpell.LookupEntry( 40485 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;

	//	// Ashtongue Talisman of Valor
	//	sp = dbcSpell.LookupEntry( 40458 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;

	//	// Memento of Tyrande
	//	sp = dbcSpell.LookupEntry( 37655 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_CAST_SPELL;

	//	// Ashtongue Talisman of Insight
	//	sp = dbcSpell.LookupEntry( 40482 );
	//	if( sp != NULL )
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;

	//	//Ashtongue Talisman of Equilibrium
	//	sp = dbcSpell.LookupEntry( 40442 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procChance = 40;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectTriggerSpell[0] = 40452;
	//		sp->Effect[1] = 6;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procChance = 25;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectTriggerSpell[1] = 40445;
	//		sp->Effect[2] = 6;
	//		sp->EffectApplyAuraName[2] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procChance = 25;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectTriggerSpell[2] = 40446;
	//		sp->maxstack = 1;
	//	}

	//	//Ashtongue Talisman of Acumen
	//	sp = dbcSpell.LookupEntry( 40438 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procChance = 10;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectTriggerSpell[0] = 40441;
	//		sp->Effect[1] = 6;
	//		sp->EffectApplyAuraName[1] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procChance = 10;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectTriggerSpell[1] = 40440;
	//		sp->maxstack = 1;
	//	}
	//	// Drums of war targets sorounding party members instead of us
	//	sp = dbcSpell.LookupEntry( 35475 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[2] = 0;
	//		sp->EffectImplicitTargetB[0] = 0;
	//		sp->EffectImplicitTargetB[1] = 0;
	//		sp->EffectImplicitTargetB[2] = 0;
	//	}
	//	// Symbol of Hope targets sorounding party members instead of us
	//	sp = dbcSpell.LookupEntry( 32548 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[2] = 0;
	//		sp->EffectImplicitTargetB[0] = 0;
	//		sp->EffectImplicitTargetB[1] = 0;
	//		sp->EffectImplicitTargetB[2] = 0;
	//	}

	//	// Drums of Battle targets sorounding party members instead of us
	//	sp = dbcSpell.LookupEntry( 35476 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[2] = 0;
	//		sp->EffectImplicitTargetB[0] = 0;
	//		sp->EffectImplicitTargetB[1] = 0;
	//		sp->EffectImplicitTargetB[2] = 0;
	//	}

	//	// Drums of Panic targets sorounding creatures instead of us
	//	sp = dbcSpell.LookupEntry( 35474 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_ALL_ENEMIES_AROUND_CASTER;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_ALL_ENEMIES_AROUND_CASTER;
	//		sp->EffectImplicitTargetA[2] = 0;
	//		sp->EffectImplicitTargetB[0] = 0;
	//		sp->EffectImplicitTargetB[1] = 0;
	//		sp->EffectImplicitTargetB[2] = 0;
	//	}

	//	// Drums of Restoration targets sorounding party members instead of us
	//	sp = dbcSpell.LookupEntry( 35478 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[2] = 0;
	//		sp->EffectImplicitTargetB[0] = 0;
	//		sp->EffectImplicitTargetB[1] = 0;
	//		sp->EffectImplicitTargetB[2] = 0;
	//	}
	//	// Drums of Speed targets sorounding party members instead of us
	//	sp = dbcSpell.LookupEntry( 35477 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectImplicitTargetA[0] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[1] = EFF_TARGET_ALL_PARTY;
	//		sp->EffectImplicitTargetA[2] = 0;
	//		sp->EffectImplicitTargetB[0] = 0;
	//		sp->EffectImplicitTargetB[1] = 0;
	//		sp->EffectImplicitTargetB[2] = 0;
	//	}

	//	sp = dbcSpell.LookupEntry( 34774 );
	//	if( sp != NULL ) //dragonspine trophy proc
	//	{
	//		sp->procChance = 6;
	//	}
	//#ifndef NEW_PROCFLAGS
	//	//Ashtongue Talisman of Lethality
	//	sp = dbcSpell.LookupEntry( 40460 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procChance = 20;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectTriggerSpell[0] = 40461;
	//		sp->maxstack = 1;
	//	}
	//#else
	//	sp = dbcSpell.LookupEntry( 40460 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation[0] = 262144 | 2097152 | 8388608 | 8519680 | 524288 | 1048576 | 8388608;
	//#endif

	//	//Serpent-Coil Braid
	//	sp = dbcSpell.LookupEntry( 37447 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procChance = 100;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->EffectTriggerSpell[0] = 37445;
	//		sp->maxstack = 1;
	//	}

	//	//Item: Assassination Armor
	//	sp = dbcSpell.LookupEntry(37166);
	//	if (sp != NULL)
	//	{
	//		sp->EffectSpellGroupRelation[0]=8519680;
	//		sp->EffectSpellGroupRelation_high[0]=8;
	//	}

	//	//Item Set: Thunderheart Harness
	//	sp = dbcSpell.LookupEntry( 38447 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] |=1024;
	//		sp->EffectSpellGroupRelation_high[1] |=64;
	//	}
	//	sp = dbcSpell.LookupEntry( 38416 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] |=8388608;
	//		sp->EffectSpellGroupRelation_high[0] |=1048576;
	//		sp->EffectSpellGroupRelation[1] |=8388608;
	//	}

	//	//Item Set: Thunderheart Regalia
	//	sp = dbcSpell.LookupEntry( 38414 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] |=2;
	//	}
	//	sp = dbcSpell.LookupEntry( 38415 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] |=4;
	//	}

	//	//Item Set: Thunderheart Raiment
	//	sp = dbcSpell.LookupEntry( 38417 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] |=2;
	//	}
	//	sp = dbcSpell.LookupEntry( 38420 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] |=32;
	//	}

	//	//Item Set: Nordrassil Harness
	//	sp = dbcSpell.LookupEntry( 37333 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[1] |=256;
	//		sp->EffectSpellGroupRelation[0] |=32768;
	//	}

	//	//Item Set: Nordrassil Raiment
	//	sp = dbcSpell.LookupEntry( 37313 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] |=64;
	//	}
	//	sp = dbcSpell.LookupEntry( 37314 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] |=16;
	//	}

	//	//Item Set: Malorne Raiment
	//	sp = dbcSpell.LookupEntry( 37292 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] |=524288;
	//	}

	//	//Item Set: Malorne Regalia
	//	sp = dbcSpell.LookupEntry( 37297 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] |=4096;
	//	}

	//	//Item Set: Malorne Harness
	//	sp = dbcSpell.LookupEntry( 37306 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance = 4;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//	}
	//	sp = dbcSpell.LookupEntry( 37311 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance = 4;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//	}

	//	//Item Set: Slayer's Armor
	//	sp = dbcSpell.LookupEntry( 38388 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] |=262144;
	//	}
	//	sp = dbcSpell.LookupEntry( 38389 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] |= 2 | 4;
	//		sp->EffectSpellGroupRelation[0] |= 8388612 |8388610 |41943040;
	//	}

	//	//Item Set: Deathmantle
	//	sp = dbcSpell.LookupEntry( 37170 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance = 4;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//	}

	//	//Item Set: Netherblade
	//	sp = dbcSpell.LookupEntry( 37167 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] |= 262144;
	//	}
	//	sp = dbcSpell.LookupEntry( 37168 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance = 15;
	//		//sp->procFlags = PROC_ON_CAST_SPELL; Need new flag - PROC_ON_FINISH_MOVE;
	//	}

	//	//Item Set: Tempest Regalia
	//	sp = dbcSpell.LookupEntry( 38396 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 67108864;
	//	}
	//	sp = dbcSpell.LookupEntry( 38397 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1572896 | 1 | 2048;
	//	}

	//	//Item Set: Tirisfal Regalia
	//	sp = dbcSpell.LookupEntry( 37441 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 536870912;
	//		sp->EffectSpellGroupRelation[1] = 536870912;
	//	}
	//	sp = dbcSpell.LookupEntry( 37443 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;
	//	}

	//	//Item Set: Aldor Regalia
	//	sp = dbcSpell.LookupEntry( 37438 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1572896 | 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 37439 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 32;
	//		sp->EffectSpellGroupRelation_high[1] = 64;
	//		//sp->EffectSpellGroupRelation_high[2] = 64; NEED DBC 2.3.2 :P
	//	}

	//	//Item Set: Absolution Regalia
	//	sp = dbcSpell.LookupEntry( 38413 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32768;
	//	}
	//	sp = dbcSpell.LookupEntry( 38412 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8192;
	//	}

	//	//Item Set: Vestments of Absolution
	//	sp = dbcSpell.LookupEntry( 38410 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 512;
	//	}
	//	sp = dbcSpell.LookupEntry( 38411 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4096 ;
	//	}

	//	//Item Set: Avatar Raiment
	//	sp = dbcSpell.LookupEntry( 26171 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 64;
	//	}

	//	//Item Set: Avatar Regalia
	//	sp = dbcSpell.LookupEntry( 37600 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->procChance = 6;
	//	}

	//	//Item Set: Incarnate Raiment
	//	sp = dbcSpell.LookupEntry( 37568 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 37565 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4096;
	//	}

	//	//Item Set: Incarnate Regalia
	//	sp = dbcSpell.LookupEntry( 37570 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[1] = 256;
	//	}
	//	sp = dbcSpell.LookupEntry( 37571 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 128;
	//		sp->EffectSpellGroupRelation[1] = 8388608;
	//	}

	//	//Item Set: Malefic Raiment
	//	sp = dbcSpell.LookupEntry( 38393 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1;
	//		sp->EffectSpellGroupRelation_high[0] = 64;
	//	}

	//	//Item Set: Voidheart Raiment
	//	sp = dbcSpell.LookupEntry( 37377 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procChance = 5;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->proc_interval = 20;
	//		sp->EffectTriggerSpell[0] = 37379;
	//	}
	//	sp = dbcSpell.LookupEntry( 39437 );
	//	if( sp != NULL )
	//	{
	//		sp->Effect[0] = 6;
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->procChance = 5;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->proc_interval = 20;
	//		sp->EffectTriggerSpell[0] = 37378;
	//	}
	//	sp = dbcSpell.LookupEntry( 37380 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2|4;
	//	}

	//	//Item Set: Gronnstalker's Armor
	//	sp = dbcSpell.LookupEntry( 38392 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 1;
	//	}

	//	//Item Set: Rift Stalker Armor
	//	sp = dbcSpell.LookupEntry( 37505 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 1;
	//	}

	//	//Item Set: Demon Stalker Armor
	//	sp = dbcSpell.LookupEntry( 37484 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 256;
	//	}
	//	sp = dbcSpell.LookupEntry( 37485 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 4096;
	//	}

	//	//Item Set: Skyshatter Harness
	//	sp = dbcSpell.LookupEntry( 38429 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1048576 | 268435456 | 2147483648UL;
	//	}

	//	//Item Set: Skyshatter Raiment
	//	sp = dbcSpell.LookupEntry( 38434 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 256;
	//	}
	//	sp = dbcSpell.LookupEntry( 38435 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 256;
	//	}

	//	//Item Set: Skyshatter Regalia
	//	sp = dbcSpell.LookupEntry( 38436 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1;
	//	}

	//	//Item Set: Cataclysm Raiment
	//	sp = dbcSpell.LookupEntry( 37225 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 37227 );
	//	if( sp != NULL )
	//	{
	//		sp->proc_interval = 60000;
	//		sp->procChance = 100;
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;
	//	}
	//	sp = dbcSpell.LookupEntry( 39950 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 64;
	//	}

	//	//Item Set: Cataclysm Regalia
	//	sp = dbcSpell.LookupEntry( 37228 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance = 7;
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}
	//	sp = dbcSpell.LookupEntry( 37234 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 37237 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance = 25;
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;
	//	}

	//	//Item Set: Cataclysm Harness
	//	sp = dbcSpell.LookupEntry( 37239 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance = 2;
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//	}
	//	sp = dbcSpell.LookupEntry( 37240 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 37241 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 512;
	//	}

	//	//Item Set: Cyclone Raiment
	//	sp = dbcSpell.LookupEntry( 37210 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 524288;
	//	}
	//	sp = dbcSpell.LookupEntry( 37211 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 524288;
	//	}

	//	//Item Set: Cyclone Harness
	//	sp = dbcSpell.LookupEntry( 37224 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 16;
	//	}
	//	sp = dbcSpell.LookupEntry( 37223 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 536870912UL;
	//	}

	//	//Item Set: Cyclone Regalia
	//	sp = dbcSpell.LookupEntry( 37212 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 256;
	//	}
	//	sp = dbcSpell.LookupEntry( 37213 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance = 11;
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;
	//	}

	//	//Item Set: Lightbringer Armor
	//	sp = dbcSpell.LookupEntry( 38421 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 4096;
	//	}
	//	sp = dbcSpell.LookupEntry( 38422 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 32;
	//	}

	//	//Item Set: Lightbringer Battlegear
	//	sp = dbcSpell.LookupEntry( 38427 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_MELEE_ATTACK;
	//		sp->procChance = 20;
	//	}
	//	sp = dbcSpell.LookupEntry( 38424 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 128;
	//	}

	//	//Item Set: Lightbringer Raiment
	//	sp = dbcSpell.LookupEntry( 38426 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL;
	//	}
	//	sp = dbcSpell.LookupEntry( 38425 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1073741824UL;
	//	}

	//	//Item Set: Crystalforge Armor
	//	sp = dbcSpell.LookupEntry( 37190 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 8;
	//	}
	//	sp = dbcSpell.LookupEntry( 37191 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}

	//	//Item Set: Crystalforge Battlegear
	//	sp = dbcSpell.LookupEntry( 37190 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 1024 | 524288 | 1048576 | 536870912UL;
	//		sp->EffectSpellGroupRelation_high[0] = 1|520;
	//	}
	//	sp = dbcSpell.LookupEntry( 37195 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->procChance = 6;
	//	}

	//	//Item Set: Crystalforge Raiment
	//	sp = dbcSpell.LookupEntry( 37189 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_SPELL_CRIT_HIT;
	//		sp->proc_interval = 60000;
	//	}
	//	sp = dbcSpell.LookupEntry( 43837 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 2147483648UL;
	//	}
	//	sp = dbcSpell.LookupEntry( 37188 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//	}

	//	//Item Set: Justicar Raiment
	//	sp = dbcSpell.LookupEntry( 37182 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 1;
	//	}
	//	sp = dbcSpell.LookupEntry( 37183 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 256;
	//	}

	//	//Item Set: Justicar Armor
	//	sp = dbcSpell.LookupEntry( 37184 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 1024;
	//		sp->EffectSpellGroupRelation[0] = 134217728;
	//		sp->EffectSpellGroupRelation_high[1] = 8;
	//	}
	//	sp = dbcSpell.LookupEntry( 37185 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 64;
	//	}

	//	//Item Set: Justicar Battlegear
	//	sp = dbcSpell.LookupEntry( 37186 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 536870912UL;
	//	}
	//	sp = dbcSpell.LookupEntry( 37187 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 520;
	//	}

	//	//Item Set: Onslaught Battlegear
	//	sp = dbcSpell.LookupEntry( 38399 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 33554432UL;
	//		sp->EffectSpellGroupRelation_high[0] = 1024;
	//	}
	//	sp = dbcSpell.LookupEntry( 38398 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 536870912UL;
	//	}

	//	//Item Set: Onslaught Armor
	//	sp = dbcSpell.LookupEntry( 38408 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 128;
	//	}
	//	sp = dbcSpell.LookupEntry( 38407 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation_high[0] = 513;
	//	}

	//	//Item Set: Destroyer Armor
	//	sp = dbcSpell.LookupEntry( 37525 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_MELEE_ATTACK_VICTIM;
	//		sp->procChance = 7;
	//	}

	//	//Item Set: Destroyer Battlegear
	//	sp = dbcSpell.LookupEntry( 37528 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->procChance = 100;
	//	}
	//	sp = dbcSpell.LookupEntry( 37535 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectSpellGroupRelation[0] = 33554432;
	//		sp->EffectSpellGroupRelation_high[0] = 1024;
	//	}

	//	//Item Set: Warbringer Armor
	//	sp = dbcSpell.LookupEntry( 37516 );
	//	if( sp != NULL )
	//	{
	//		sp->procFlags = PROC_ON_CAST_SPELL;
	//		sp->procChance = 100;
	//	}

	//	//Item Set: Warbringer Battlegear
	//	sp = dbcSpell.LookupEntry( 37518 );
	//	if( sp != NULL )
	//		sp->EffectSpellGroupRelation_high[0] = 4;

	//	//all Drums 
	//	sp = dbcSpell.LookupEntry( 35474 );
	//	if( sp != NULL )
	//		sp->RequiredShapeShift = 0;
	//	sp = dbcSpell.LookupEntry( 35475 );
	//	if( sp != NULL )
	//		sp->RequiredShapeShift = 0;
	//	sp = dbcSpell.LookupEntry( 35476 );
	//	if( sp != NULL )
	//		sp->RequiredShapeShift = 0;
	//	sp = dbcSpell.LookupEntry( 35477 );
	//	if( sp != NULL )
	//		sp->RequiredShapeShift = 0;
	//	sp = dbcSpell.LookupEntry( 35478 );
	//	if( sp != NULL )
	//		sp->RequiredShapeShift = 0;

	//	//this an on equip item spell(2824) :  ice arrow(29501)
	//	sp = dbcSpell.LookupEntry( 29501 );
	//	if( sp != NULL )
	//	{
	//		sp->procChance = 30;//some say it is triggered every now and then
	//		sp->procFlags = PROC_ON_RANGED_ATTACK;
	//	}

	//	//Purify helboar meat
	//	sp = dbcSpell.LookupEntry( 29200 );
	//	if( sp != NULL )
	//	{
	//		sp->Reagent[1] = 0;
	//		sp->ReagentCount[1] = 0;
	//		sp->EffectSpellGroupRelation[0] = 23248;
	//		sp->Effect[0] = 24;
	//	}
	//	// Clefthide Leg Armor - not ENCHANT_OWN_ONLY.
	//	sp = dbcSpell.LookupEntry( 35489 );
	//	if( sp != NULL )
	//	{
	//		sp->Flags3 = 0;
	//	}

	//	// - Warrior - Warbringer Armor
	//	// 2 pieces: You have a chance each time you parry to gain Blade Turning, absorbing 200 damage for 15 sec.
	//	// SPELL ID = 37514 (http://www.wowhead.com/?spell=37514)

	//	sp = dbcSpell.LookupEntry( 37514 );
	//	if( sp != NULL )
	//	{
	//		sp->EffectApplyAuraName[0] = SPELL_AURA_PROC_TRIGGER_SPELL;
	//		sp->EffectTriggerSpell[0] = 37515;
	//		sp->procChance = 25;
	//	}
	//	sp = dbcSpell.LookupEntry( 40475 );		// Black temple melee trinket
	//	if( sp != NULL )
	//		sp->procChance = 50;

	//	// Band of the Eternal Champion: reduced proc rate
	//	sp = dbcSpell.LookupEntry( 35080 );
	//	if( sp != NULL )
	//		sp->procChance = 5;

	//	// Band of the Eternal Sage: reduced proc rate
	//	sp = dbcSpell.LookupEntry( 35083 );
	//	if( sp != NULL )
	//		sp->procChance = 5;

	//	// Band of the Eternal Defender: reduced proc rate
	//	sp = dbcSpell.LookupEntry( 35077 );
	//	if( sp != NULL )
	//		sp->procChance = 5;

	//	// Band of the Eternal Restorer: reduced proc rate
	//	sp = dbcSpell.LookupEntry( 35086 );
	//	if( sp != NULL )
	//		sp->procChance = 5;




	//	//////////////////////////////////////////
	//	// BOSSES								//
	//	//////////////////////////////////////////

	//	// Insert boss spell fixes here
	//	
	//		// Dark Glare
	//		sp = dbcSpell.LookupEntry( 26029 );
	//		if( sp != NULL )
	//			sp->cone_width = 15.0f; // 15 degree cone
}
