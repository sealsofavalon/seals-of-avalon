#include "StdAfx.h"

#include "../../SDBase/Protocol/C2S_Item.h"
#include "../../SDBase/Protocol/C2S_Spell.h"
#include "../../SDBase/Protocol/S2C_Quest.h"
#include "../../SDBase/Protocol/S2C_Spell.h"
#include "SunyouInstance.h"
#include "JingLianItem.h"
#include "MultiLanguageStringMgr.h"
#include "QuestMgr.h"

uint32 forbidden_item_in_combat[79] = {
600000001,
600000002,
600000003,
600000004,
600000005,
600000006,
600000007,
600000008,
600000009,
600000010,
600000011,
600000012,
600000013,
600000014,
600000015,
600000016,
601000001,
601000002,
601000003,
601000004,
601000005,
601000006,
601000007,
601000008,
601000009,
601000010,
601000011,
601000012,
601000013,
601000014,
601000015,
601000016,
602000001,
602000002,
602000003,
602000004,
602000005,
602000006,
602000007,
602000008,
602000009,
602000010,
602000011,
602000012,
602000013,
602000014,
602000015,
602000016,
603000001,
603000002,
603000003,
603000004,
603000005,
603000006,
603000007,
603000008,
603000009,
603000010,
603000011,
603000012,
603000013,
603000014,
603000015,
603000016,
604000001,
604000002,
604000003,
604000004,
604000005,
604000006,
604000007,
604000008,
604000009,
604000010,
604000011,
604000012,
604000013,
5		 ,
11};

bool IsFightMountingSpell( SpellEntry* sp )
{
	for(int i = 0; i < 3; i++)
	{
		if( sp->Effect[i] == SPELL_EFFECT_APPLY_AURA ||
			sp->Effect[i] == SPELL_EFFECT_APPLY_AREA_AURA )
		{
			switch( sp->EffectApplyAuraName[i] )
			{
			case SPELL_AURA_SHAPESHIFT_FIGHT_MOUNT:
			case SPELL_AURA_MOUNT_FIGHT:
				return true;
			default: break;
			}
		}
	}

	return false;
}
		 
void WorldSession::HandleUseItemOpcode(CPacketUsn& packet)
{		 
	if(!_player->IsInWorld()) return;

	MSG_C2S::stItem_Use MsgRecv;packet >> MsgRecv;
	Player* p_User = GetPlayer();
	//MyLog::log->notice("WORLD: got use Item packet");
	uint32 spellId = 0;

	
	uint8 cn = MsgRecv.MultiCast;
	Item* tmpItem = NULL;
	tmpItem = p_User->GetItemInterface()->GetInventoryItem(MsgRecv.bag,MsgRecv.slot);
	if (!tmpItem)
		tmpItem = p_User->GetItemInterface()->GetInventoryItem(MsgRecv.slot);
	if (!tmpItem)
		return;

	if( tmpItem->GetEntry() != 14 )
		if( _player->IsStunned() || _player->IsFeared() || _player->IsPacified() || _player->IsAsleep())
			return;

	if(tmpItem->GetEntry() == ITEM_ID_XIAOLABA || tmpItem->GetEntry() == ITEM_ID_DALABA)
	{
		if( m_muted && m_muted >= (uint32)UNIXTIME )
		{
			SystemMessage( build_language_string( BuildString_SpellHandlerMuted ) ); // "你已被禁言.");
			return;
		}
	}

	if( _player->CombatStatus.IsInCombat() )
	{
		if( &forbidden_item_in_combat[79] != std::find( forbidden_item_in_combat, &forbidden_item_in_combat[79], tmpItem->GetEntry() ) )
		{
			MSG_S2C::stSpell_Cast_Result Msg;
			Msg.SpellId			= 0;
			Msg.ErrorMessage	= SPELL_FAILED_CANTUSE_IN_COMBAT;
			Msg.MultiCast		= 0;
			Msg.Extra			= 0;
			SendPacket(Msg);
			return;
		}
	}
	if( _player->GetMapMgr()->GetMapInfo()->type == 2 )
	{
		if( tmpItem->GetEntry() == 5 )
		{
			SendNotification( build_language_string( BuildString_SpellHandlerCannotUseInInstance ) );//"副本内禁用" );
			return;
		}
		if( _player->m_sunyou_instance && _player->m_sunyou_instance->GetCategory() == INSTANCE_CATEGORY_TEAM_ARENA && tmpItem->GetEntry() == 11 )
		{
			SendNotification( build_language_string( BuildString_SpellHandlerCannotUseInInstance ) );//"副本内禁用" );
			return;
		}
	}

	/*
	pet_food* pf = dbcPetFoodList.LookupEntry( tmpItem->GetEntry() );
	if( pf )
	{
		Pet* pPet = _player->GetSummon();
		if( !pPet )
		{
			SendNotification( build_language_string( BuildString_SpellHandlerCannotFeedWithoutPet ) );//"你还没有召唤宠物，不能喂食" );
			return;
		}
		if( pPet->Feed( pf->entry, pf->exp ) )
		{
			//_player->GetItemInterface()->RemoveItemAmt( pf->entry, 1 );
			if( tmpItem->GetUInt32Value( ITEM_FIELD_STACK_COUNT ) > 1 )
				tmpItem->ModUnsigned32Value( ITEM_FIELD_STACK_COUNT, -1 );
			else
				//_player->GetItemInterface()->SafeFullRemoveItemByGuid( tmpItem->GetGUID() );
				_player->GetItemInterface()->SafeFullRemoveItemFromSlot( MsgRecv.bag, MsgRecv.slot );
		}
		return;
	}

	if( tmpItem->GetUInt32Value( ITEM_FIELD_VALID_AFTER_GENERATE_TIME ) )
	{
		if( (uint32)UNIXTIME - tmpItem->GetUInt32Value( ITEM_FIELD_GENERATE_TIME ) < tmpItem->GetUInt32Value( ITEM_FIELD_VALID_AFTER_GENERATE_TIME ) )
		{
			SendNotification( build_language_string( BuildString_SpellHandlerPetCannotSpawn ) );//"宠物蛋还未到孵化时间，不能破蛋而出" );
			return;
		}
		else
		{
			//if( _player->GetPetCount() >= 4 )
			//{
			//	SendNotification( build_language_string( BuildString_SpellHandlerPetCountLimited ) ); //"您的宠物数量已经达到上限，不能再孵化了" );
			//	return;
			//}
		}
	}
	*/

	//////////////////////////////////////////////////////////////////////////
	// for test use item bug
	//p_User->GetItemInterface()->SafeFullRemoveItemByGuid( tmpItem->GetGUID() );
	//return;
	//////////////////////////////////////////////////////////////////////////

	ItemPrototype *itemProto = tmpItem->GetProto();
	if(!itemProto)
		return;

	if(_player->getDeathState()==CORPSE)
		return;

	if(sScriptMgr.CallScriptedItem(tmpItem,_player))
		return;

	if(itemProto->QuestId)
	{
		if(itemProto->Bonding == ITEM_BIND_QUEST_TRIGGER || itemProto->Bonding == ITEM_BIND_QUEST_DROP_AND_TRIGGER)
		{
			// Item Starter
			Quest *qst = QuestStorage.LookupEntry(itemProto->QuestId);
			if(!qst) 
				return;

			MSG_S2C::stQuestGiver_Quest_Details Msg;
			sQuestMgr.BuildQuestDetails(&Msg, qst, tmpItem->GetGUID(), 0);
			SendPacket(Msg);
		}
	}
	
	SpellCastTargets targets = MsgRecv.targets;
	uint32 x;
	for(x = 0; x < 5; x++)
	{
		if(itemProto->Spells[x].Trigger == USE)
		{
			if(itemProto->Spells[x].Id)
			{
				spellId = itemProto->Spells[x].Id;

				if(x<5)
				{
					if( itemProto->Spells[x+1].Trigger == LEARNING )
					{
						if( _player->HasSpell( itemProto->Spells[x+1].Id ) )
						{
							return;
						}
					}
				}
				break;
			}
		}
	}
	if(!spellId)
		return;

	if( !_player->IsOnlyCanCastSpell( spellId ) )
	{
		MSG_S2C::stSpell_Failure Msg;
		Msg.caster_guid = _player->GetNewGUID();
		Msg.spell_id	= spellId;
		Msg.error		= SPELL_FAILED_CANTUSE_IN_SHAPESHIFT;
		SendPacket(Msg);
		return;
	}

	// check for spell id
	SpellEntry *spellInfo = dbcSpell.LookupEntry( spellId );

	if(!spellInfo)
	{
		MyLog::log->error("WORLD: unknown spell id %i\n", spellId);
		return;
	}

	/*if((_player->IsMounted() || _player->GetUInt32Value(UNIT_FIELD_FORMDISPLAYID)) && (IsTeleportSpell(spellInfo) || IsLuoPanShi(spellInfo)))
	{
		_player->GetItemInterface()->BuildInventoryChangeError(tmpItem,NULL,INV_ERR_CANT_USE_ON_MOUNT);
	}*/

	if(_player->GetMapMgr()->m_sunyouinstance )
	{
		switch( _player->GetMapMgr()->m_sunyouinstance->GetCategory() )
		{
		case INSTANCE_CATEGORY_REFRESH_MONSTER:
		case INSTANCE_CATEGORY_DYNAMIC:
		case INSTANCE_CATEGORY_ESCAPE:
		case INSTANCE_CATEGORY_TEAM_ARENA:
			if(IsMountingSpell(spellInfo))
			{
				_player->GetItemInterface()->BuildInventoryChangeError(tmpItem,NULL,INV_ERR_CANT_USE_ON_INSTACE);
				return;
			}
			break;
		case INSTANCE_CATEGORY_ARENA:
		case INSTANCE_CATEGORY_UNIQUE_PVP_ZONE:
		case INSTANCE_CATEGORY_TOWER_DEFENSE:
		case INSTANCE_CATEGORY_UNIQUE_CASTLE:
		case INSTANCE_CATEGORY_BATTLE_GROUND:
		case INSTANCE_CATEGORY_RAID:
		case INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND:
		case INSTANCE_CATEGORY_NEW_BATTLE_GROUND:
			if(IsFightMountingSpell(spellInfo))
			{
				_player->GetItemInterface()->BuildInventoryChangeError(tmpItem,NULL,INV_ERR_CANT_USE_ON_INSTACE);
				return;
			}
			break;
		default:
			break;
		}
		
		if(IsLuoPanShi(spellInfo))
		{
			_player->GetItemInterface()->BuildInventoryChangeError(tmpItem,NULL,INV_ERR_CANT_USE_ON_INSTACE);
				return;
		}
	}

	if (spellInfo->AuraInterruptFlags & AURA_INTERRUPT_ON_STAND_UP)
	{
		if (p_User->CombatStatus.IsInCombat() || p_User->IsMounted())
		{
			_player->GetItemInterface()->BuildInventoryChangeError(tmpItem,NULL,INV_ERR_CANT_DO_IN_COMBAT);
			return;
		}
		if(p_User->GetStandState()!=1)
		p_User->SetStandState(STANDSTATE_SIT);
		// loop through the auras and removing existing eating spells
	}

	if(spellInfo->AuraInterruptFlags & AURA_INTERRUPT_ON_MOUNT)
	{
		if(p_User->IsMounted())
		{
			_player->GetItemInterface()->BuildInventoryChangeError(tmpItem,NULL,INV_ERR_CANT_USE_ON_MOUNT);
			return;
		}
	}

	if(itemProto->RequiredLevel)
	{
		if(_player->getLevel() < itemProto->RequiredLevel)
		{
			_player->GetItemInterface()->BuildInventoryChangeError(tmpItem,NULL,INV_ERR_ITEM_RANK_NOT_ENOUGH);
			return;
		}
	}

	if(itemProto->RequiredSkill)
	{
		if(!_player->_HasSkillLine(itemProto->RequiredSkill))
		{
			_player->GetItemInterface()->BuildInventoryChangeError(tmpItem,NULL,INV_ERR_ITEM_RANK_NOT_ENOUGH);
			return;
		}

		if(itemProto->RequiredSkillRank)
		{
			if(_player->_GetSkillLineCurrent(itemProto->RequiredSkill, false) < itemProto->RequiredSkillRank)
			{
				_player->GetItemInterface()->BuildInventoryChangeError(tmpItem,NULL,INV_ERR_ITEM_RANK_NOT_ENOUGH);
				return;
			}
		}
	}
	
	if( itemProto->AllowableClass && !(_player->getClassMask() & itemProto->AllowableClass) )
	{
		_player->GetItemInterface()->BuildInventoryChangeError(tmpItem,NULL,INV_ERR_YOU_CAN_NEVER_USE_THAT_ITEM2);
		return;
	}		

	if( itemProto->AllowableRace && !(_player->getRaceMask() & itemProto->AllowableRace) )
	{
		_player->GetItemInterface()->BuildInventoryChangeError(tmpItem,NULL,INV_ERR_YOU_CAN_NEVER_USE_THAT_ITEM);
		return;
	}

	if( !_player->Cooldown_CanCast( itemProto, x ) )
	{
		_player->SendCastResult(spellInfo->Id, SPELL_FAILED_NOT_READY, cn, 0);
		return;
	}

	if(_player->m_currentSpell)
	{
		_player->SendCastResult(spellInfo->Id, SPELL_FAILED_SPELL_IN_PROGRESS, cn, 0);
		return;
	}

	if( _player->GetShapeShift() )
	{
		if( IsMountingSpell( spellInfo ) || IsShapeSpell( spellInfo ) )
		{
			MSG_S2C::stSpell_Failure Msg;
			Msg.caster_guid = _player->GetNewGUID();
			Msg.spell_id	= spellId;
			Msg.error		= SPELL_FAILED_CANTUSE_IN_SHAPESHIFT;
			return;
		}
	}

	if( _player->IsMounted() )
	{
		if( IsMountingSpell( spellInfo ) || IsShapeSpell( spellInfo ) )
		{
			MSG_S2C::stSpell_Failure Msg;
			Msg.caster_guid = _player->GetNewGUID();
			Msg.spell_id	= spellId;
			Msg.error		= SPELL_FAILED_CANTUSE_IN_MOUNT;
			SendPacket(Msg);
			return;
		}
	}

	if( itemProto->ForcedPetId >= 0 )
	{
		if( itemProto->ForcedPetId == 0 )
		{
			if( _player->GetGUID() != targets.m_unitTarget )
			{
				_player->SendCastResult(spellInfo->Id, SPELL_FAILED_BAD_TARGETS, cn, 0);
				return;
			}
		}
		else
		{
			if( !_player->GetSummon() || _player->GetSummon()->GetEntry() != (uint32)itemProto->ForcedPetId )
			{
				_player->SendCastResult(spellInfo->Id, SPELL_FAILED_SPELL_IN_PROGRESS, cn, 0);
				return;
			}
		}
	}

	if(tmpItem->GetEntry() == ITEM_ID_XINCHUNGE)
	{
		_player->InsertTitle(58, (ui32)UNIXTIME + 60 * 24 * 7);
	}

	Spell *spell = new Spell(_player, spellInfo, false, NULL);
	uint8 result;
	spell->extra_cast_number=cn;
	spell->i_caster = tmpItem;
	result = spell->prepare(&targets);
}

extern const uint32 * GetShapeShiftSpell(uint32 creature_entry);
void WorldSession::HandleCastSpellOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	if(_player->getDeathState()==CORPSE)
		return;

	uint32 spellId;
	uint8 cn;

	MSG_C2S::stSpell_Cast MsgRecv; packet >> MsgRecv;
	spellId	= MsgRecv.spellId;
	cn	= MsgRecv.cn;

	if( _player->IsStunned() )
	{
		if( spellId != 22401 && spellId != 22402 && spellId != 771 && spellId != 22701 && spellId != 22702 )
			return;
	}
	if( _player->IsAsleep() )
	{
		if( spellId != 22701 && spellId != 22702 && spellId != 771 )
			return;
	}

	if( _player->IsFeared() || _player->IsPacified() )
	{
		if( spellId != 22701 && spellId != 22702 && spellId != 771 && spellId != 13301 && spellId != 13302 && spellId != 13303 )
			return;
	}

	if( _player->m_ForceRoot && ( spellId == 22401 || spellId == 22402 || spellId == 22701 || spellId == 22702 ) )
	{
		return;
	}

	if( _player->m_rooted > 0 && MsgRecv.spellId >= 10301 && MsgRecv.spellId <= 10304 )
	{
		MSG_S2C::stSpell_Failure Msg;
		Msg.caster_guid = _player->GetNewGUID();
		Msg.spell_id	= spellId;
		Msg.error		= SPELL_FAILED_ROOTED;
		SendPacket(Msg);
		return;
	}

	if(_player->HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_SHAPE_CANTFIGHT))
	{
		MSG_S2C::stSpell_Failure Msg;
		Msg.caster_guid = _player->GetNewGUID();
		Msg.spell_id	= spellId;
		Msg.error		= SPELL_FAILED_CANTUSE_IN_SHAPESHIFT;
		SendPacket(Msg);
		return;
	}
	if(_player->HasFlag(PLAYER_FIELD_SHAPE_MOUNT_FLAG, PLAYER_MOUNT_CANTFIGHT))
	{
		MSG_S2C::stSpell_Failure Msg;
		Msg.caster_guid = _player->GetNewGUID();
		Msg.spell_id	= spellId;
		Msg.error		= SPELL_FAILED_CANTUSE_IN_MOUNT;
		SendPacket(Msg);
		return;
	}

	if( !_player->IsOnlyCanCastSpell( spellId ) )
	{
		MSG_S2C::stSpell_Failure Msg;
		Msg.caster_guid = _player->GetNewGUID();
		Msg.spell_id	= spellId;
		Msg.error		= SPELL_FAILED_CANTUSE_IN_SHAPESHIFT;
		SendPacket(Msg);
		return;
	}

	bool bFindInShapeShiftSpells = false;

	/*
	if( _player->GetShapeShift() )
	{
		const uint32* spells = GetShapeShiftSpell(_player->GetShapeShift());
		for(int i = 0; i < 10; i++)
		{
			if(spells[i] == spellId)
				bFindInShapeShiftSpells = true;
		}
		if(!bFindInShapeShiftSpells)
			return;
	}
	*/

	if( _player->GetUInt64Value(PLAYER_FOOT) == _player->GetGUID() )
	{
		MSG_S2C::stSpell_Failure Msg;
		Msg.caster_guid = _player->GetNewGUID();
		Msg.spell_id	= spellId;
		Msg.error		= SPELL_FAILED_CANTUSE_IN_MOUNT;
		SendPacket(Msg);
		return;
	}

	// check for spell id
	SpellEntry *spellInfo = dbcSpell.LookupEntry(spellId );

	if(!spellInfo || !sHookInterface.OnCastSpell(_player, spellInfo))
	{
		MyLog::log->error("WORLD: unknown spell id %i\n", spellId);
		return;
	}

	if( _player->GetShapeShift() )
	{
		if( IsMountingSpell( spellInfo ) || IsShapeSpell( spellInfo ) )
		{
			MSG_S2C::stSpell_Failure Msg;
			Msg.caster_guid = _player->GetNewGUID();
			Msg.spell_id	= spellId;
			Msg.error		= SPELL_FAILED_CANTUSE_IN_SHAPESHIFT;
			return;
		}
	}

	if( _player->IsMounted() )
	{
		if( IsMountingSpell( spellInfo ) || IsShapeSpell( spellInfo ) )
		{
			MSG_S2C::stSpell_Failure Msg;
			Msg.caster_guid = _player->GetNewGUID();
			Msg.spell_id	= spellId;
			Msg.error		= SPELL_FAILED_CANTUSE_IN_MOUNT;
			SendPacket(Msg);
			return;
		}
	}

// 	MyLog::log->notice("WORLD: got cast spell packet, spellId - %i (%s)",
// 		spellId, spellInfo->Name);
	
	// Cheat Detection only if player and not from an item
	// this could fuck up things but meh it's needed ALOT of the newbs are using WPE now
	// WPE allows them to mod the outgoing packet and basicly choose what ever spell they want :(

	if( !GetPlayer()->HasSpell(spellId) || spellInfo->Attributes & ATTRIBUTES_PASSIVE )
	{
		if( spellId != SPELL_ID_CAIJI
			&& spellId != SPELL_ID_KAIYOUXIANG
			&& spellId != SPELL_ID_KAIXIANGZI
			&& spellId != SPELL_ID_KAIQI
			&& spellId != SPELL_ID_REFINE
			&& spellId != 120
			&& spellId != 502)
		{
			MyLog::log->notice("WORLD: Spell isn't casted because player \"%s\" is cheating", GetPlayer()->GetName());
			return;
		}
	}

	if( spellId == SPELL_ID_REFINE )
	{
		uint8 ret = _player->GetJingLianItemInterface()->JingLian( true );
		MSG_S2C::stJingLianItemAck ack;
		ack.result = ret;
		SendPacket( ack );

		if( ret != JINGLIAN_RESULT_CHECK_SUCCESS )
			return;
	}

	if(spellInfo->weaponMask != 0xFFFFFF)
	{
		Item* pWeapon = GetPlayer()->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_MAINHAND);
		if(!pWeapon || !(1 << pWeapon->GetProto()->SubClass & spellInfo->weaponMask) )
		{
			pWeapon = GetPlayer()->GetItemInterface()->GetInventoryItem(EQUIPMENT_SLOT_OFFHAND);
			if(!pWeapon || !(1 << pWeapon->GetProto()->SubClass & spellInfo->weaponMask) )
			{
				MSG_S2C::stSpell_Failure Msg;
				Msg.caster_guid = _player->GetNewGUID();
				Msg.spell_id	= spellId;
				Msg.error		= SPELL_FAILED_WRONG_WEAPON;
				SendPacket(Msg);
				return;
			}
		}
	}

	if (GetPlayer()->GetOnMeleeSpell() != spellId)
	{
        if(_player->m_currentSpell)
        {
			if( _player->m_currentSpell->getState() == SPELL_STATE_CASTING )
			{
				// cancel the existing channel spell, cast this one
				_player->m_currentSpell->cancel();
			}
			else
			{
				// send the error message
				_player->SendCastResult(spellInfo->Id, SPELL_FAILED_SPELL_IN_PROGRESS, cn, 0);
				return;
			}
        }

		// some anticheat stuff
		if( spellInfo->self_cast_only )
		{
			if( MsgRecv.targets.m_unitTarget && MsgRecv.targets.m_unitTarget != _player->GetGUID() )
			{
				// send the error message
				_player->SendCastResult(spellInfo->Id, SPELL_FAILED_BAD_TARGETS, cn, 0);
				return;
			}
		}

		if( spellId == SPELL_ID_BAISHI )
		{
			if( _player->GetUInt32Value( PLAYER_FIELD_ENCHANT_TEACHER_CHARGES ) == 0 )
			{
				// send the error message
				_player->SendCastResult(spellInfo->Id, SPELL_FAILED_NO_CHARGES_REMAIN, cn, 0);
				return;
			}
		}
		
		//life spell


		Spell *spell = new Spell(GetPlayer(), spellInfo, false, NULL);
		spell->extra_cast_number=cn;
		spell->prepare(&MsgRecv.targets);
		if( spellId == 2101 )
			_player->ModUnsigned32Value( PLAYER_FIELD_ENCHANT_TEACHER_CHARGES, -1 );
	}
}

void WorldSession::HandleCancelCastOpcode(CPacketUsn& packet)
{
	MSG_C2S::stSpell_Cancel_Cast MsgRecv;packet >> MsgRecv;

	if(GetPlayer()->m_currentSpell)
		GetPlayer()->m_currentSpell->cancel();
}

void WorldSession::HandleCancelAuraOpcode( CPacketUsn& packet)
{
	MSG_C2S::stSpell_Cancel_Aura MsgRecv;packet >> MsgRecv;

	SpellEntry* sp = dbcSpell.LookupEntry( MsgRecv.spellId );
	if( sp && sp->field114 & SPELL_AVOID_MASK_AURA_CANNOT_REMOVE_BYSELF )
		return;
	if( !sp )
		return;

	SpellEntry* pspellentry = dbcSpell.LookupEntry(MsgRecv.spellId);
	if(!pspellentry)
	{
		return;
	}
	if( pspellentry->Attributes & ATTRIBUTES_PASSIVE )
	{
		MyLog::log->debug( "cannot cancel passive aura! player:[%d]", _player->GetLowGUID() );
		return;
	}

	for(int i = 0; i < 3; i++)
	{
		if( pspellentry->Effect[i] == SPELL_EFFECT_APPLY_AURA && pspellentry->EffectApplyAuraName[i] == SPELL_AURA_EXTRA_XP )
			return;
	}
	
	for(uint32 x = 0; x < MAX_AURAS+MAX_POSITIVE_AURAS; ++x)
	{
		if(_player->m_auras[x] && _player->m_auras[x]->IsPositive() && _player->m_auras[x]->GetSpellId() == MsgRecv.spellId)
		{
			for( int i = 0; i < 3; ++i )
			{
				if( pspellentry->Effect[i] == SPELL_EFFECT_APPLY_AREA_AURA )
				{
					if( _player->m_auras[x]->GetCaster() != _player )
						return;
				}
			}

			_player->m_auras[x]->m_ignoreunapply = true; // prevent abuse
			_player->m_auras[x]->Remove();
		}
	}
	MyLog::log->debug("removing aura %u",MsgRecv.spellId);
}

void WorldSession::HandleCancelChannellingOpcode( CPacketUsn& packet)
{
	MSG_C2S::stSpell_Cancel_Channelling MsgRecv;packet >> MsgRecv;

	Player *plyr = GetPlayer();
	if(!plyr)
		return;
	if(plyr->m_currentSpell)
	{		
		plyr->m_currentSpell->cancel();
	}
}

void WorldSession::HandleCancelAutoRepeatSpellOpcode(CPacketUsn& packet)
{
	//MyLog::log->info("Received CMSG_CANCEL_AUTO_REPEAT_SPELL message.");
	//on original we automatically enter combat when creature got close to us
//	GetPlayer()->GetSession()->OutPacket(SMSG_CANCEL_COMBAT);
	GetPlayer()->m_onAutoShot = false;
	MSG_S2C::stSpell_Cancel_Auto_Repeat Msg;
	Msg.spell_id = 5003;
	SendPacket( Msg );
}

void WorldSession::HandleAddDynamicTargetOpcode(CPacketUsn& packet)
{
	MSG_C2S::stSpell_Add_Dynamic_Target_Obsolete MsgRecv;packet >> MsgRecv;
	uint64 guid = MsgRecv.guid;
	uint32 spellid = MsgRecv.spellid;
	
	SpellEntry * sp = dbcSpell.LookupEntry(spellid);
	if (!_player->m_CurrentCharm || guid != _player->m_CurrentCharm->GetGUID())
    {
        return;
    }
	
	Spell * pSpell = new Spell(_player->m_CurrentCharm, sp, false, 0);
	pSpell->prepare(&MsgRecv.targets);
}

void WorldSession::HandleLeapPrepare( CPacketUsn& packet )
{
	if( !_player || !_player->IsInWorld() )
		return;

	MSG_C2S::stLeapPrepare msg;
	packet >> msg;
	_player->leap_x = msg.x;
	_player->leap_y = msg.y;
	_player->leap_z = msg.z;
	_player->leap_o = msg.o;
}