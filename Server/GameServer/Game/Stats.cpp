#include "StdAfx.h"
#include <math.h>
uint32 getConColor(uint16 AttackerLvl, uint16 VictimLvl)
{

	//	const uint32 grayLevel[sWorld.LevelCap+1] = {0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,13,14,15,16,17,18,19,20,21,22,22,23,24,25,26,27,28,29,30,31,31,32,33,34,35,35,36,37,38,39,39,40,41,42,43,43,44,45,46,47,47,48,49,50,51,51,52,53,54,55,55};
#define PLAYER_LEVEL_CAP 100
	const uint32 grayLevel[PLAYER_LEVEL_CAP+1] = {0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,13,14,15,16,17,18,19,20,21,22,22,23,24,25,26,27,28,29,30,31,31,32,33,34,35,35,36,37,38,39,39,40,41,42,43,43,44,45,46,47,47,48,49,50,51,51,52,53,54,55,55,52,53,54,55,55,52,53,54,55,55,52,53,54,55,55,52,53,54,55,55,52,53,54,55,55,52,53,54,55,55};
	if(AttackerLvl + 5 <= VictimLvl)
	{
		if(AttackerLvl + 10 <= VictimLvl)
		{
			return 5;
		}
		return 4;
	}
	else
	{
		switch(VictimLvl - AttackerLvl)
		{
		case 4:
		case 3:
			return 3;
			break;
		case 2:
		case 1:
		case 0:
		case -1:
		case -2:
			return 2;
			break;
		default:
			// More adv formula for grey/green lvls:
			if(AttackerLvl <= 6)
			{
				return 1; //All others are green.
			}
			else
			{
				if(AttackerLvl > PLAYER_LEVEL_CAP)
					return 1;//gm
				if(AttackerLvl<PLAYER_LEVEL_CAP && VictimLvl <= grayLevel[AttackerLvl])
					return 0;
				else
					return 1;
			}
		}
	}
#undef PLAYER_LEVEL_CAP
}

uint32 CalculateXpToGive(Unit *pVictim, Unit *pAttacker, bool bInGroup, uint32 total_level, uint32 total_player_cnt)
{
	if(pVictim->IsPlayer())
		return 0;
	if( !pAttacker->IsPlayer() )
		return 0;
	Player* ply = static_cast<Player*>( pAttacker );

	if(((Creature*)pVictim)->IsTotem())
		return 0;

	CreatureInfo *victimI;
	victimI = ((Creature*)pVictim)->GetCreatureName();

	float VictimLvl = (float)((Creature*)pVictim)->proto->MinLevel;
	float AttackerLvl = (float)ply->getLevel();
	if( AttackerLvl == 0 )
		return 0;

	//float exp = 50*(VictimLvl * VictimLvl * VictimLvl + 5 * VictimLvl * VictimLvl ) - 80;
	//float exp = 50.f + 7.5f * ( ( ( VictimLvl * VictimLvl ) / 128.f + VictimLvl * 25.f / 8.f ) / 5.f + VictimLvl / ( VictimLvl + 1.f ) );
	//exp *= 2;
// #ifdef XINJIANG_VERSION
// 	float exp = 50 + 7.5f * ( ( VictimLvl * VictimLvl / 128.f + 25.f / 8.f * VictimLvl ) / 5.f + VictimLvl / ( VictimLvl + 1 ) );
// #else
// 	float d = log(VictimLvl);
// 	float T = 70 * d*d*d*d +50;
// 	float F2 = 6*( ply->GetUInt32Value(PLAYER_NEXT_LEVEL_XP) / T );
// 	float exp = 50 + 2 * sqrt( VictimLvl*VictimLvl*F2/100);
// #endif
	float exp = 45 + 5 * AttackerLvl;

	if(!bInGroup)
	{
		if (8 - AttackerLvl + VictimLvl > 0)
		{
			exp = exp * float( 8 - AttackerLvl + VictimLvl) / 8;
		}else
			exp = 0;
		//exp = exp * VictimLvl / AttackerLvl;
	}
	else
		exp = exp * VictimLvl * total_player_cnt / total_level;

	if(ply)
		exp = exp * ply->m_ExtraEXPRatio / 100;

	//float rate = VictimLvl <= AttackerLvl ? VictimLvl / AttackerLvl : 1.f;
	//exp *= rate;

	switch( victimI->Rank )
	{
	case 0: break;
	case 1: exp *= 3.f; break;
	case 2: exp *= 3.5f; break;
	case 3: exp *= 5.f; break;
	default: break;
	}
	exp = ply->CalcCastleExpBonus( pVictim, exp );

	//exp *= 1.8f;
	return (uint32)exp;
}

float CalculateStat( uint16 level, uint8 playerclass, uint8 Stat )
{
	if( level == 0 )
		return 0;
	float value = 0.f;
	float x[6] = { 0.f };
	float base[6] = {0.f};
	switch( playerclass )
	{
	case CLASS_WARRIOR:
		{
			x[STAT_STAMINA] = 2.5f;
			x[STAT_INTELLECT] = 0.5f;
			x[STAT_STRENGTH] = 3.f;
			x[STAT_SPIRIT] = 0.f;
			x[STAT_AGILITY] = 1.5f;
			x[STAT_VITALITY] = 1.5f;

			base[STAT_STAMINA] = 19.f;
			base[STAT_INTELLECT] = 4.f;
			base[STAT_STRENGTH] = 16.f;
			base[STAT_SPIRIT] = 8.f;
			base[STAT_AGILITY] = 9.f;
			base[STAT_VITALITY] = 14.f;
		}
		break;
	case CLASS_BOW:
		{
			x[STAT_STAMINA] = 1.5f;
			x[STAT_INTELLECT] = 1.f;
			x[STAT_STRENGTH] = 2.f;
			x[STAT_SPIRIT] = 0.5f;
			x[STAT_AGILITY] = 3.5f;
			x[STAT_VITALITY] = 0.5f;

			base[STAT_STAMINA] = 10.f;
			base[STAT_INTELLECT] = 12.f;
			base[STAT_STRENGTH] = 14.5f;
			base[STAT_SPIRIT] = 5.f;
			base[STAT_AGILITY] = 20.5f;
			base[STAT_VITALITY] = 8.f;
		}
		break;
	case CLASS_MAGE://����
		{
			x[STAT_STAMINA] = 1.8f				;
			x[STAT_INTELLECT] = 2.5f;
			x[STAT_STRENGTH] = 1.0f;
			x[STAT_SPIRIT] = 3.f;
			x[STAT_AGILITY] = 0.2f;
			x[STAT_VITALITY] = 0.5f;

			base[STAT_STAMINA] = 14.f;
			base[STAT_INTELLECT] = 20.5f;
			base[STAT_STRENGTH] = 4.f;
			base[STAT_SPIRIT] = 20.5f;
			base[STAT_AGILITY] = 3.f;
			base[STAT_VITALITY] = 8.f;
		}
		break;
	case CLASS_PRIEST: //�ɵ�
		{
			x[STAT_STAMINA] = 1.5f;
			x[STAT_INTELLECT] = 3.8f;
			x[STAT_STRENGTH] = 0.5f;
			x[STAT_SPIRIT] = 2.f;
			x[STAT_AGILITY] = 0.5f;
			x[STAT_VITALITY] = 0.7f;

			base[STAT_STAMINA] = 13.f;
			base[STAT_INTELLECT] = 30.f;
			base[STAT_STRENGTH] = 3.f;
			base[STAT_SPIRIT] = 12.f;
			base[STAT_AGILITY] = 3.f;
			base[STAT_VITALITY] = 9.f;
		}
		break;
	default:
		{
			assert( 0 );
			return 0;
		}
	}
	value = base[Stat] + x[Stat] * level;
	return value;
	/*
	float basevalue = 0.f;
	switch( Stat )
	{
	case STAT_STRENGTH: basevalue = 2.f * float(level) * 5.f; break;
	case STAT_AGILITY: basevalue = 2.f * float(level) * 5.f; break;
	case STAT_STAMINA: basevalue = 6.4f * float(level) * 5.f; break;
	case STAT_INTELLECT: basevalue = 6.4f * float(level) * 5.f; break;
	case STAT_SPIRIT: basevalue = 3.6f * float(level) * 5.f; break;
	case STAT_VITALITY: basevalue = 2.f * float(level) * 5.f; break;
	default:
		{
			assert( 0 );
			return 0;
		}
	}
	float x[6];
	switch( playerclass )
	{
	case CLASS_WARRIOR:
		{
			x[STAT_STRENGTH] = 6.f;
			x[STAT_AGILITY] = 3.f;
			x[STAT_STAMINA] = 4.f;
			x[STAT_INTELLECT] = 1.f;
			x[STAT_SPIRIT] = 2.f;
			x[STAT_VITALITY] = 5.f;
		}
		break;
	case CLASS_BOW:
		{
			x[STAT_STRENGTH] = 5.f;
			x[STAT_AGILITY] = 6.f;
			x[STAT_STAMINA] = 2.f;
			x[STAT_INTELLECT] = 2.f;
			x[STAT_SPIRIT] = 2.f;
			x[STAT_VITALITY] = 4.f;
		}
		break;
	case CLASS_PRIEST:
		{
			x[STAT_STRENGTH] = 1.f;
			x[STAT_AGILITY] = 4.f;
			x[STAT_STAMINA] = 2.f;
			x[STAT_INTELLECT] = 6.f;
			x[STAT_SPIRIT] = 6.f;
			x[STAT_VITALITY] = 2.f;
		}
		break;
	case CLASS_MAGE:
		{
			x[STAT_STRENGTH] = 2.f;
			x[STAT_AGILITY] = 1.f;
			x[STAT_STAMINA] = 6.f;
			x[STAT_INTELLECT] = 5.f;
			x[STAT_SPIRIT] = 4.f;
			x[STAT_VITALITY] = 3.f;
		}
		break;
	default:
		{
			assert( 0 );
			return 0;
		}
	}
	uint32 value = float2int32( basevalue * x[Stat] / 3.5f );
	return value;
	*/
}

uint32 GainStat(uint16 level, uint8 playerclass,uint8 Stat)
{
	uint32 value1 = CalculateStat( level - 1, playerclass, Stat );
	uint32 value2 = CalculateStat( level, playerclass, Stat );
	return value2 - value1;
}

float CalculateDamage( Unit* pAttacker, Unit* pVictim, uint32 weapon_damage_type, uint64 spellgroup, SpellEntry* ability,
					  bool cri, uint32 spell_dmg, bool bSchool, bool stato, bool weapon  ) // spellid is used only for 2-3 spells, that have AP bonus
{
	int VictimLvl = pVictim ? pVictim->getLevel() : 1;
	int AttackerLvl = pAttacker->getLevel();
	uint32 pharmor = 0;
	uint32 sparmor = 0;
	uint32 Resistance = 0;
	float DamagePct = .0f;
	float DamageAdd = 0.0f;
	float DamageDe = 0.0f;
	float attack = 0;
	attack = spell_dmg;
	uint32 School = 0;
	uint32 delta = 0;
	int MinDamage;
	int MaxDamage;

	if (ability && (ability->School >= 0 || ability->School < 8))
	{
		School = ability->School;
	}

	if (pVictim)
	{
		Resistance = pVictim->GetResistance(School);
	}

	if (pAttacker && pAttacker->IsPlayer())
	{
		DamageAdd = pAttacker->GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_POS + School ); 	
		DamageDe = pAttacker->GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + School);
	}

	float stato_damage = 0.f;
	
	if(ability)
	{
		int res = pAttacker->GetSpellDmgBonus(pVictim, ability, spell_dmg, false, false);
		attack += res;
		if (weapon)
		{
			MinDamage = pAttacker->GetFloatValue(UNIT_FIELD_MINDAMAGE);
			MaxDamage = pAttacker->GetFloatValue(UNIT_FIELD_MAXDAMAGE);
			delta = float2int32( MaxDamage - MinDamage );
			if( delta > 0 )
			{
				delta = rand() % (delta + 1);
			}
			attack = attack + pAttacker->BaseDamage[0] + delta;
		}

		int dmg_bonus_pct = 0;
		SM_FFValue(pAttacker->SM_FDamageBonus, &attack, ability->SpellGroupType);
		SM_FIValue(pAttacker->SM_PDamageBonus,&dmg_bonus_pct,ability->SpellGroupType);
		attack = attack + attack * dmg_bonus_pct / 100;
	}
	else
	{

		MinDamage = pAttacker->GetFloatValue(UNIT_FIELD_MINDAMAGE);
		MaxDamage = pAttacker->GetFloatValue(UNIT_FIELD_MAXDAMAGE);

		delta = float2int32( MaxDamage - MinDamage );
		if( delta > 0 )
		{
			delta = rand() % (delta + 1);
		}

		attack = attack + MinDamage + (float)delta;
		attack += (DamageAdd - DamageDe);
		float inital_dmg = float(attack);

		if( attack <= 1.0f )
			attack = 1.0f;
		//float dd_mod = pAttacker->GetDamageDonePctMod( School );
		if( pVictim)
			attack += float2int32( inital_dmg * (pVictim->DamageTakenPctMod[ School ]  - 1.0f) );

		//if( dd_mod > 1.0f )
		//	attack += float2int32( ( inital_dmg * dd_mod ) );

	}

	/*
	if(cri)
	{
		attack *= 2.0f;
	}
	*/

	if( attack <= 1.0f )
		attack = 1.0f;

	return attack;
}

bool isEven (int num)
{
	if ((num%2)==0)
	{
		return true;
	}

	return false;
}



