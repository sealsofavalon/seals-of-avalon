#ifndef __STATS_H
#define __STATS_H


SERVER_DECL uint32 getConColor(uint16 AttackerLvl, uint16 VictimLvl);
SERVER_DECL uint32 CalculateXpToGive(Unit *pVictim, Unit *pAttacker, bool bInGroup = false, uint32 total_level = 1, uint32 total_player_cnt = 1);
SERVER_DECL float CalculateStat( uint16 level, uint8 playerclass, uint8 Stat );
SERVER_DECL float CalculateDamage( Unit* pAttacker, Unit* pVictim, uint32 weapon_damage_type, uint64 spellgroup, SpellEntry* ability, bool cri = false, uint32 spell_dmg = 0, 
								  bool bSchool = false, bool stato = false, bool weapon = true );
SERVER_DECL uint32 GainStat(uint16 level, uint8 playerclass,uint8 Stat);
SERVER_DECL bool isEven (int num);

#endif
