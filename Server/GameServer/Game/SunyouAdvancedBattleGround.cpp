#include "StdAfx.h"
#include "SunyouAdvancedBattleGround.h"
#include "MultiLanguageStringMgr.h"

SunyouAdvancedBattleGround::SunyouAdvancedBattleGround()
{
	m_pBGBase = new SunyouBattleGroundBase( this );
}

SunyouAdvancedBattleGround::~SunyouAdvancedBattleGround()
{
	delete m_pBGBase;
}

void SunyouAdvancedBattleGround::OnPlayerJoin( Player* p )
{
	SunyouRaid::OnPlayerJoin( p );
	m_pBGBase->OnBGPlayerJoin( p );
	p->SetFFA( 10 + p->getRace() );
}

void SunyouAdvancedBattleGround::OnPlayerLeave( Player* p )
{
	SunyouRaid::OnPlayerLeave( p );
	m_pBGBase->OnBGPlayerLeave( p );
}

void SunyouAdvancedBattleGround::OnPlayerDie( Player* p, Object* attacker )
{
	SunyouRaid::OnPlayerDie( p, attacker );
	m_pBGBase->OnBGPlayerDie( p, attacker );
}

void SunyouAdvancedBattleGround::Expire()
{
	SunyouRaid::Expire();
}

void SunyouAdvancedBattleGround::OnBattleGroundEndByRace( int WinnerRace, int LoserRace )
{
	m_pBGBase->OnBGEndByRace( WinnerRace, LoserRace );
	
	MSG_S2C::stNotify_Msg msg;

	//sprintf( txt, "[%s族]战胜了[%s族], 您将在60秒后离开战场", race_name[WinnerRace], race_name[LoserRace] );
	msg.notify = build_language_string( BuildString_ABGReport, _race_name[WinnerRace], _race_name[LoserRace] );
	Broadcast( msg );

	sEventMgr.AddEvent( this, &SunyouAdvancedBattleGround::Expire, EVENT_SUNYOU_INSTANCE_EXPIRE, 60 * 1000, 1, 0 );
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
SunyouTeamGroupBattleGround::SunyouTeamGroupBattleGround()
{
	m_pBGBase = new SunyouTeamGroupBattleGroundBase( this );
	m_ffa_[0] = 11;
	m_ffa_[1] = 12;
	m_ffa_[2] = 13;
}
SunyouTeamGroupBattleGround::~SunyouTeamGroupBattleGround()
{
	delete m_pBGBase ;
}
void SunyouTeamGroupBattleGround::OnPlayerLeave( Player* p )
{
	SunyouRaid::OnPlayerLeave( p );
	m_pBGBase->OnBGPlayerLeave( p );
}
void SunyouTeamGroupBattleGround::OnPlayerDie( Player* p, Object* attacker )
{
	SunyouRaid::OnPlayerDie( p, attacker );
	m_pBGBase->OnBGPlayerDie( p, attacker );
}
void SunyouTeamGroupBattleGround::Expire()
{
	SunyouRaid::Expire();
}
void SunyouTeamGroupBattleGround::OnPlayerJoin( Player* p )
{
	p->SetFFA(m_ffa_[p->m_team_arena_idx - 1]);
	SunyouRaid::OnPlayerJoin( p );
	m_pBGBase->OnBGPlayerJoin( p );
	
}
void SunyouTeamGroupBattleGround::OnTeamLoser(int loserTeam)
{
	
}
void SunyouTeamGroupBattleGround::OnBattleGroundEnd( int WinTeam, const char* str )
{
	m_pBGBase->OnBGEnd( WinTeam );

 	MSG_S2C::stNotify_Msg msg;

 	msg.notify = str;
	Broadcast( msg );

	sEventMgr.AddEvent( this, &SunyouTeamGroupBattleGround::Expire, EVENT_SUNYOU_INSTANCE_EXPIRE, 60 * 1000, 1, 0 );
}