#ifndef _SUNYOU_ADVANCED_BATTLE_GROUND_HEAD
#define _SUNYOU_ADVANCED_BATTLE_GROUND_HEAD

#include "SunyouRaid.h"
#include "SunyouBattleGroundBase.h"

class SunyouAdvancedBattleGround : public SunyouRaid
{
public:
	friend class SunyouInstance;

	SunyouAdvancedBattleGround();
	~SunyouAdvancedBattleGround();

	void OnPlayerJoin( Player* p );
	void OnPlayerLeave( Player* p );
	void OnPlayerDie( Player* p, Object* attacker );
	void Expire();

	void OnBattleGroundEndByRace( int WinnerRace, int LoserRace );

protected:
	SunyouBattleGroundBase* m_pBGBase;
};


class SunyouTeamGroupBattleGround : public SunyouRaid
{
public:
	friend class SunyouInstance;

	SunyouTeamGroupBattleGround();
	~SunyouTeamGroupBattleGround();

	virtual void OnPlayerJoin( Player* p );
	virtual void OnPlayerLeave( Player* p );
	virtual void OnPlayerDie( Player* p, Object* attacker );
	virtual void Expire();
	void OnBattleGroundEnd( int WinTeam, const char* str );
	void OnTeamLoser(int loserTeam);
protected:
	SunyouTeamGroupBattleGroundBase* m_pBGBase;
	ui32 m_ffa_[3];
};
#endif
