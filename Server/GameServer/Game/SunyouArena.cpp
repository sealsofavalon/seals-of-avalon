#include "StdAfx.h"
#include "SunyouArena.h"

SunyouArena::SunyouArena()
{

}

void SunyouArena::Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr )
{
	SunyouInstance::Init( conf, pMapMgr );
}

struct rankkill
{
	uint32 kill;
	uint32 rank;
	Player* p;
	MSG_S2C::stInstanceBillboard::info* info;
	float baseexp;
};

void SunyouArena::Expire()
{
	MSG_S2C::stInstanceBillboard msg;
	std::multimap<uint32, MSG_S2C::stInstanceBillboard::info> billboard;
	for( std::set<Player*>::iterator it = m_players.begin(); it != m_players.end(); ++it )
	{
		MSG_S2C::stInstanceBillboard::info info;
		Player* p = *it;
		info.name = p->GetName();
		info.level = p->getLevel();
		info.kill = p->GetArenaKill();
		info.exp = (uint32)p;
		info.gold = 0;
		billboard.insert( std::make_pair( info.kill, info ) );
	}
	int n = 0;
	std::vector<rankkill> vrk;
	vrk.reserve( 10 );
	uint32 rank = 1;
	uint32 overlap[10] = { 0 };
	float x = 0.f, y = 0.f, z = 0.f, sx = 0.f, sy = 0.f, sz = 0.f;
	for( std::multimap<uint32, MSG_S2C::stInstanceBillboard::info>::reverse_iterator it = billboard.rbegin(); it != billboard.rend(); ++it )
	{
		MSG_S2C::stInstanceBillboard::info& info = it->second;
		Player* p = (Player*)info.exp;
		LevelInfo* lv = objmgr.GetLevelInfo( p->getRace(), p->getClass(), p->getLevel() );

		rankkill rk = { info.kill, rank, p, &info, (float)lv->XPToNextLevel };
		if( vrk.size() == 0 )
		{
			overlap[rank - 1] = 1;
		}
		else if( rk.kill < vrk.back().kill )
		{
			rank = ++rk.rank;
			overlap[rank - 1] = 1;
		}
		else
			++overlap[rank - 1];
		info.rank = rank;
		if( rank == 2 )
			sy += 0.04f/25 * rk.baseexp;
		vrk.push_back( rk );
	}
	for( std::vector<rankkill>::iterator it = vrk.begin(); it != vrk.end(); ++it )
	{
		const rankkill& rk = *it;
		if( rk.rank == 3 )
		{
			z = 0.04f/25 * rk.baseexp / (float)overlap[2];
			sz += z;
		}
	}

	for( std::vector<rankkill>::iterator it = vrk.begin(); it != vrk.end(); ++it )
	{
		rankkill& rk = *it;
		float expbonus = (float)rk.info->kill * rk.baseexp * 0.0002f;
		switch( rk.rank )
		{
		case 1:
			expbonus += ( 0.02f * rk.baseexp - sy - sz ) / overlap[0];
			break;
		case 2:
			expbonus += 0.04f/25 * rk.baseexp;
			break;
		case 3:
			expbonus += 0.04f/25 * rk.baseexp / overlap[2];
			break;
		default:
			break;
		}
		rk.info->exp = (uint32)expbonus;

		if( rk.info->gold > 0 )
		{
			rk.p->ModCoin(rk.info->gold );
			MyLog::yunyinglog->info("gold-SunyouArena-player[%u][%s] take gold[%u]", rk.p->GetLowGUID(), rk.p->GetName(), rk.info->gold);
		}
		if( rk.info->exp > 0 )
			rk.p->GiveXP( rk.info->exp, 0, false, GIVE_EXP_TYPE_DYNAMIC_INSTANCE );
		msg.vinfo.push_back( *rk.info );
	}
	Broadcast( msg );
	SunyouInstance::Expire();
}

void SunyouArena::OnPlayerJoin( Player* p )
{
	if( !p->IsValid() || !p->IsInWorld() || p->IsSunyouInstanceJoinLocked() )
		return;

	SunyouInstance::OnPlayerJoin( p );
	p->SetFFA( 1 );

	MSG_S2C::stArenaUpdateState msg1, msg2;

	MSG_S2C::stArenaUpdateState::info info;
	info.name = p->GetName();
	info.level = p->getLevel();
	info.race = p->getRace();
	info.cls = p->getClass();
	std::map<uint32, uint32>::iterator it = m_scores.find( p->GetLowGUID() );
	if( it != m_scores.end() )
		info.kill = it->second;
	else
		info.kill = 0;
	p->SetArenaKill( info.kill );

	msg2.vinfo.push_back( info );

	for( std::set<Player*>::iterator it = m_players.begin(); it != m_players.end(); ++it )
	{
		Player* ply = *it;
		info.name = ply->GetName();
		info.level = ply->getLevel();
		info.race = ply->getRace();
		info.cls = ply->getClass();
		info.kill = ply->GetArenaKill();
		msg1.vinfo.push_back( info );
		if( ply != p )
			ply->GetSession()->SendPacket( msg2 );
	}
	p->GetSession()->SendPacket( msg1 );
}

void SunyouArena::OnPlayerLeave( Player* p )
{
	if( !p->m_sunyou_instance )
		return;
	SunyouInstance::OnPlayerLeave( p );
	m_scores[p->GetLowGUID()] = p->GetArenaKill();
}

void SunyouArena::OnInstanceStartup()
{
	SunyouInstance::OnInstanceStartup();
}
