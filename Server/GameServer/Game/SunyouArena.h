#ifndef _SUNYOU_ARENA_HEAD
#define _SUNYOU_ARENA_HEAD

#include "SunyouInstance.h"

class SunyouArena : public SunyouInstance
{
public:
	SunyouArena();

	virtual void Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr );
	virtual void Expire();
	virtual void OnPlayerJoin( Player* p );
	virtual void OnPlayerLeave( Player* p );
	virtual void OnInstanceStartup();

protected:
	std::map<uint32, uint32> m_scores;
};

#endif