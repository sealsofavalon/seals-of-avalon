#include "StdAfx.h"
#include "SunyouBattleGround.h"
#include "MultiLanguageStringMgr.h"

SunyouBattleGround::SunyouBattleGround() : m_ended( false )
{
	faction_table[0] = 'A';
	faction_table[1] = 'B';
	faction_table[2] = 'C';

	m_pBGBase = new SunyouBattleGroundBase( this );
}

SunyouBattleGround::~SunyouBattleGround()
{
	delete m_pBGBase;
}

void SunyouBattleGround::Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr )
{
	SunyouInstance::Init( conf, pMapMgr );

	std::map<uint32, sunyou_battle_ground_configuration*>::iterator it = s_configs.find( conf.mapid );
	if( it != s_configs.end() )
	{
		m_sbgc = it->second;

		if( m_sbgc->random_abc )
			std::random_shuffle( faction_table, &faction_table[3] );
	}
	else
	{
		assert( 0 && "battle ground configuration not found!" );
	}
}

void SunyouBattleGround::Update()
{
	if( m_ended )
		return;

	if( m_starttime && (uint32)UNIXTIME > m_starttime + 60 )
	{
		int alive = 0;
		for( int i = 0; i < 3; ++i )
		{
			if( !m_bg_factions[i].inited || m_bg_factions[i].defeated ) continue;

			if( m_bg_factions[i].battle_power <= 0 && !m_bg_factions[i].defeated )
				DefeatFaction( i );
			else
				++alive;
		}

		if( alive == 1 )
		{
			std::set<Player*> winners, losers;
			for( int i = 0; i < 3; ++i )
			{
				if( !m_bg_factions[i].inited || m_bg_factions[i].defeated ) continue;

				winners.insert( m_bg_factions[i].players.begin(), m_bg_factions[i].players.end() );
			}
			End( winners, losers );
		}
		else if( alive == 0 )
		{
			SunyouInstance::Expire();
			return;
		}
	}

	for( int i = 0; i < 3; ++i )
	{
		if( !m_bg_factions[i].inited || m_bg_factions[i].defeated ) continue;

		std::map<uint32, std::pair<uint32, bool> > npc;
		for( std::map<battle_ground_npc*, uint32>::iterator it = m_dotanpc_respawn.begin(); it != m_dotanpc_respawn.end(); ++it )
		{
			battle_ground_npc* bgn = it->first;
			if( bgn->faction == i + 1 && (uint32)UNIXTIME - it->second > bgn->spawn_time )
			{
				npc.insert( std::make_pair( bgn->npcid, std::make_pair( bgn->battle_power, false ) ) );
				it->second = (uint32)UNIXTIME;
			}
		}

		if( npc.size() )
		{
			std::set<uint32> out;
			m_mapmgr->SpawnCreatures( this, npc, out, i + 1 );
			m_bg_factions[i].npc_guid.insert( out.begin(), out.end() );
		}
	}
}

void SunyouBattleGround::Expire()
{
	if( !m_ended )
	{
		int maxbp = 0;
		for( int i = 0; i < 3; ++i )
		{
			if( !m_bg_factions[i].inited || m_bg_factions[i].defeated )
				continue;

			if( m_bg_factions[i].battle_power > maxbp )
			{
				maxbp = m_bg_factions[i].battle_power;
			}
		}
		std::set<Player*> winners, losers;
		int w = 0, l = 0;
		for( int i = 0; i < 3; ++i )
		{
			if( m_bg_factions[i].battle_power >= maxbp && !m_bg_factions[i].defeated && m_bg_factions[i].inited )
			{
				winners.insert( m_bg_factions[i].players.begin(), m_bg_factions[i].players.end() );
				++w;
			}
			else
			{
				losers.insert( m_bg_factions[i].players.begin(), m_bg_factions[i].players.end() );
				++l;
			}
		}
		if( w > 1 && l == 0 )
			std::swap( winners, losers );

		End( winners, losers );
	}
	else
		SunyouInstance::Expire();
}

void SunyouBattleGround::OnPlayerJoin( Player* p )
{
	if( !p->IsValid() || !p->IsInWorld() || p->IsSunyouInstanceJoinLocked() )
		return;

	int faction = p->getRace() - 1;
	if( m_bg_factions[faction].defeated )
	{
		p->EjectFromInstance();
		return;
	}

	SunyouInstance::OnPlayerJoin( p );
	m_pBGBase->OnBGPlayerJoin( p );
	int side = faction_table[faction] - 'A';

	if( !m_bg_factions[faction].inited )
		InitFaction( faction, side );

	m_bg_factions[faction].players.insert( p );

	p->SetRandomResurrectLocation( m_sbgc->resurrect_points[faction][side] );
	p->SetFFA( 11 + side );

	sEventMgr.AddEvent( this, &SunyouBattleGround::Teleport2Initpoints, p, EVENT_SUNYOU_TEAM_ARENA_TELEPORT, 500, 1, 0 );

	UpdateBattlePower();
}

void SunyouBattleGround::OnPlayerLeave( Player* p )
{
	if( !p->m_sunyou_instance )
		return;

	SunyouInstance::OnPlayerLeave( p );
	m_pBGBase->OnBGPlayerLeave( p );

	m_bg_factions[p->getRace() - 1].players.erase( p );
	if( m_bg_factions[p->getRace() - 1].players.size() == 0 )
	{
		DefeatFaction( p->getRace() - 1 );
	}
	UpdateBattlePower( p );
	Update();
}

void SunyouBattleGround::OnInstanceStartup()
{
	SunyouInstance::OnInstanceStartup();

	for( std::vector<battle_ground_npc*>::iterator it = m_sbgc->npcs.begin(); it != m_sbgc->npcs.end(); ++it )
	{
		battle_ground_npc* bgn = *it;
		if( bgn->dota_mode )
			m_dotanpc_respawn[bgn] = UNIXTIME;
	}
}

void SunyouBattleGround::OnCreatureDie( Creature* victim, Object* attacker )
{
	if( m_ended )
		return;
	if( victim->m_battleground == 0 )
		return;
	else if( victim->m_battlepower < 0 )
	{
		if( attacker->IsPlayer() )
			m_bg_factions[static_cast<Player*>( attacker )->getRace() - 1].battle_power -= victim->m_battlepower;
		else if( attacker->IsCreature() )
		{
			if( static_cast<Creature*>( attacker )->m_bgfaction < 4 )
				m_bg_factions[static_cast<Creature*>( attacker )->m_bgfaction - 1].battle_power -= victim->m_battlepower;
		}
	}
	else
		m_bg_factions[victim->m_bgfaction - 1].battle_power -= victim->m_battlepower;

	UpdateBattlePower();
	Update();
}

void SunyouBattleGround::OnPlayerDie( Player* victim, Object* attacker )
{
	if( m_ended )
		return;

	m_bg_factions[victim->getRace()-1].battle_power--;

	m_pBGBase->OnBGPlayerDie( victim, attacker );

	UpdateBattlePower();
	Update();
}

void SunyouBattleGround::Set2Faction( int f1, int f2 )
{
	faction_table[f1] = 'A';
	faction_table[f2] = 'B';
}

void SunyouBattleGround::Teleport2Initpoints( Player* p )
{
	if( p->IsValid() && p->IsInWorld() && p->GetMapMgr()->m_sunyouinstance == this )
	{
		int faction = p->getRace() - 1;
		int side = faction_table[faction] - 'A';
		p->SafeTeleport( m_mapmgr->GetMapId(), m_mapmgr->GetInstanceID(), *m_sbgc->init_points[faction][side][rand()%m_sbgc->init_points[faction][side].size()] );
	}
}

void SunyouBattleGround::End( std::set<Player*>& winners, std::set<Player*>& losers )
{
	if( m_ended )
		return;
	int WinnerRace = 0;
	int LoserRace = 0;

	for( std::set<Player*>::iterator it = winners.begin(); it != winners.end(); ++it )
	{
		Player* p = *it;
		if( !p->IsValid() )
			continue;

		if( WinnerRace == 0 )
			WinnerRace = p->getRace();
		p->ModTotalHonor( m_sbgc->win_honor_reward );
		p->ModHonorCurrency( m_sbgc->win_honor_reward );
		if( m_sbgc->win_item_reward )
			p->GiveGift( 1, m_sbgc->win_item_reward, m_sbgc->win_item_count, true, Player::GIFT_CATEGORY_BATTLE_GROUND );

		//p->GetSession()->SendNotification( build_language_string( BuildString_BGWinnerReport ) ); //"恭喜您，获得战场的胜利，将在10秒内被传送出副本" );
	}
	for( std::set<Player*>::iterator it = losers.begin(); it != losers.end(); ++it )
	{
		Player* p = *it;
		if( !p->IsValid() )
			continue;

		if( LoserRace == 0 )
			LoserRace = p->getRace();

		p->ModTotalHonor( m_sbgc->lose_honor_reward );
		p->ModHonorCurrency( m_sbgc->lose_honor_reward );
		if( m_sbgc->lose_item_reward )
			p->GiveGift( 1, m_sbgc->lose_item_reward, m_sbgc->lose_item_count, true, Player::GIFT_CATEGORY_BATTLE_GROUND );

		//p->GetSession()->SendNotification( build_language_string( BuildString_BGLoserReport ) ); //"您在战场中被击败，将在10秒内被传送出副本" );
	}

	m_ended = true;
	m_pBGBase->OnBGEndByRace( WinnerRace, LoserRace );
	sEventMgr.AddEvent( this, &SunyouBattleGround::Expire, EVENT_SUNYOU_INSTANCE_EXPIRE, 10 * 1000, 1, 0 );
}

void SunyouBattleGround::InitFaction( int faction, int side )
{
	bg_faction& bgf = m_bg_factions[faction];
	bgf.inited = true;
	bgf.faction = faction;
	bgf.battle_power = m_sbgc->battle_power[side];
	bgf.side = side;

	std::map<uint32, std::pair<uint32, bool> > npc;
	for( std::vector<battle_ground_npc*>::iterator it = m_sbgc->npcs.begin(); it != m_sbgc->npcs.end(); ++it )
	{
		battle_ground_npc* bgn = *it;
		if( bgn->faction == faction + 1 && ( *bgn->side == side + 'A' || *bgn->side == side + 'a' ) )
			npc.insert( std::make_pair( bgn->npcid, std::make_pair( bgn->battle_power, bgn->spawn_time > 0 ? true : false ) ) );
	}

	m_mapmgr->SpawnCreatures( this, npc, bgf.npc_guid, faction + 1 );
}

void SunyouBattleGround::DefeatFaction( int faction )
{
	bg_faction& bgf = m_bg_factions[faction];
	if( !bgf.inited || bgf.defeated )
		return;

	bgf.defeated = true;
	bgf.battle_power = 0;
	m_mapmgr->DespawnCreatures( bgf.npc_guid );
	bgf.npc_guid.clear();

	UpdateBattlePower();
	std::set<Player*> tmp( bgf.players );
	for( std::set<Player*>::iterator it = tmp.begin(); it != tmp.end(); ++it )
	{
		Player* p = *it;
		p->ModTotalHonor( m_sbgc->lose_honor_reward );
		p->ModHonorCurrency( m_sbgc->lose_honor_reward );
		if( m_sbgc->lose_item_reward )
			p->GiveGift( 1, m_sbgc->lose_item_reward, m_sbgc->lose_item_count, true, Player::GIFT_CATEGORY_BATTLE_GROUND );

		p->GetSession()->SendNotification( build_language_string( BuildString_BGLoserReport ) );//"您在战场中被击败，将在10秒内被传送出副本" );
		sEventMgr.AddEvent( p, &Player::EjectFromInstance, EVENT_PLAYER_EJECT_FROM_INSTANCE, 10*1000, 1, 0 );
	}
	bgf.players.clear();
}

void SunyouBattleGround::UpdateBattlePower( Player* p )
{
	MSG_S2C::stBattleGroundPowerUpdate msg;
	for( int i = 0; i < 3; ++i )
	{
		if( m_bg_factions[i].battle_power >= 0 )
			msg.power[i] = m_bg_factions[i].battle_power;
		else
			msg.power[i] = 0;
		
		msg.side[i] = m_bg_factions[i].side;
		msg.num[i] = m_bg_factions[i].players.size();
	}
	if( p )
		p->GetSession()->SendPacket( msg );
	else
		Broadcast( msg );
}

extern void parse_string_and_push_to_vector( const char* str, std::vector<LocationVector*>& v );

std::map<uint32, sunyou_battle_ground_configuration*> SunyouBattleGround::s_configs;

void SunyouBattleGround::LoadConfigurations()
{
	StorageContainerIterator<sunyou_battle_ground>* it = dbcSunyouBattleGroundList.MakeIterator();
	while(!it->AtEnd())
	{
		sunyou_battle_ground* sbg = it->Get();

		sunyou_battle_ground_configuration* c = new sunyou_battle_ground_configuration;

		for( int i = 0; i < 3; ++i )
		{
			for( int j = 0; j < 3; ++j )
			{
				parse_string_and_push_to_vector( sbg->init_pts[i*3+j], c->init_points[i][j] );
				parse_string_and_push_to_vector( sbg->resurrect_pts[i*3+j], c->resurrect_points[i][j] );
			}
		}
		memcpy( c->battle_power, sbg->battle_power, sizeof( uint32 ) * 3 );
		c->win_honor_reward = sbg->win_honor_reward;
		c->lose_honor_reward = sbg->lose_honor_reward;
		c->win_item_reward = sbg->win_item_reward;
		c->win_item_count = sbg->win_item_count;
		c->lose_item_reward = sbg->lose_item_reward;
		c->lose_item_count = sbg->lose_item_count;
		c->random_abc = sbg->random_abc;

		StorageContainerIterator<battle_ground_npc>* it2 = dbcBattleGroundNpcList.MakeIterator();
		while(!it2->AtEnd())
		{
			battle_ground_npc* bgn = it2->Get();
			if( bgn->mapid == sbg->entry )
				c->npcs.push_back( bgn );
			if( !it2->Inc() )
				break;
		}
		it2->Destruct();
		
		s_configs[sbg->entry] = c;

		if( !it->Inc() )
			break;
	}
	it->Destruct();
}

void SunyouBattleGround::ReleaseConfigurations()
{
	for( std::map<uint32, sunyou_battle_ground_configuration*>::iterator it = s_configs.begin(); it != s_configs.end(); ++it )
		delete it->second;

	s_configs.clear();
}
