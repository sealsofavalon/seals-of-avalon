#ifndef _SUNYOU_BATTLE_GROUND_HEAD
#define _SUNYOU_BATTLE_GROUND_HEAD

#include "SunyouInstance.h"
#include "SunyouBattleGroundBase.h"


struct sunyou_battle_ground_configuration
{
	std::vector<LocationVector*> init_points[3][3];
	std::vector<LocationVector*> resurrect_points[3][3];
	int battle_power[3];
	uint32 win_honor_reward;
	uint32 lose_honor_reward;
	uint32 win_item_reward;
	uint32 win_item_count;
	uint32 lose_item_reward;
	uint32 lose_item_count;
	uint32 random_abc;
	std::vector<battle_ground_npc*> npcs;
};

class SunyouBattleGround : public SunyouInstance
{
public:
	friend class SunyouInstance;
	SunyouBattleGround();
	virtual ~SunyouBattleGround();

	virtual void Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr );
	virtual void Update();
	virtual void Expire();
	virtual void OnPlayerJoin( Player* p );
	virtual void OnPlayerLeave( Player* p );
	virtual void OnInstanceStartup();

	virtual void OnCreatureDie( Creature* c, Object* attacker );
	virtual void OnPlayerDie( Player* victim, Object* attacker );

	void Set2Faction( int f1, int f2 );
	void Teleport2Initpoints( Player* p );

	static void LoadConfigurations();
	static void ReleaseConfigurations();

protected:
	struct bg_faction;

	void End( std::set<Player*>& winners, std::set<Player*>& losers );
	void InitFaction( int faction, int side );
	void DefeatFaction( int faction );
	void UpdateBattlePower( Player* p = NULL );

	struct bg_faction
	{
		bool inited;
		bool defeated;
		int side;
		int faction;
		int battle_power;
		std::set<Player*> players;
		std::set<uint32> npc_guid;

		bg_faction() : inited( false ), defeated( false ), side( 0 ), battle_power( 0 ) {}
	};
	bg_faction m_bg_factions[3];
	sunyou_battle_ground_configuration* m_sbgc;
	char faction_table[3];
	std::map<battle_ground_npc*, uint32> m_dotanpc_respawn;
	bool m_ended;
	SunyouBattleGroundBase* m_pBGBase;

	static std::map<uint32, sunyou_battle_ground_configuration*> s_configs;
};

#endif