#include "StdAfx.h"
#include "SunyouBattleGroundBase.h"
#include "SunyouInstance.h"

SunyouBattleGroundBase::SunyouBattleGroundBase( SunyouInstance* father ) : m_FirstBlood( 0 ), m_father( father )
{
	for(uint32 i = 0; i < 3; ++i)
	{
		m_groups[i] = new Group(true);
		m_groups[i]->m_disbandOnNoMembers = false;
		m_groups[i]->ExpandToRaid();
	}
}

SunyouBattleGroundBase::~SunyouBattleGroundBase()
{
	for(uint32 i = 0; i < 3; ++i)
	{
		PlayerInfo *inf;
		for(uint32 j = 0; j < m_groups[i]->GetSubGroupCount(); ++j) {
			for(GroupMembersSet::iterator itr = m_groups[i]->GetSubGroup(j)->GetGroupMembersBegin(); itr != m_groups[i]->GetSubGroup(j)->GetGroupMembersEnd();) {
				inf = (*itr);
				++itr;
				m_groups[i]->RemovePlayer(inf);
			}
		}
		delete m_groups[i];
	}

	for( std::map<ui32, battle_ground_player_detail_information*>::iterator it = m_mapInformation.begin(); it != m_mapInformation.end(); ++it )
		delete it->second;
	
	m_mapInformation.clear();
}

void SunyouBattleGroundBase::OnBGStart()
{
	sEventMgr.AddEvent( this, &SunyouBattleGroundBase::BroadCastInformation, 0, EVENT_SUNYOU_BATTLE_GROUND_UPDATE_INFORMATION, 3000, 10000, 0 );
}

void SunyouBattleGroundBase::OnBGPlayerDie( Player* victim, Object* attacker )
{
	battle_ground_player_detail_information* info = FindInfoByPlayer( victim );
	if( info )
		info->bekill++;

	if( victim == attacker )
		return;

	victim->m_bg_kill_count = 0;
	Player* p_a = NULL;
	if( attacker->IsPet() )
		p_a = ((Pet*)attacker)->GetPetOwner();
	else if( attacker->IsPlayer() )
		p_a = (Player*)attacker;

	if( p_a )
	{
		info = FindInfoByPlayer( p_a );
		if( info )
			info->kill++;

		if( victim->m_bg_dota_state >= 32 )
		{
			MSG_S2C::stBattleGroundSpecialNotify notify;
			notify.is_break = true;
			notify.who = victim->GetName();
			notify.state = victim->m_bg_dota_state;
			notify.who_break = p_a->GetName();
			m_father->Broadcast( notify );
		}
		victim->m_bg_dota_state = 0;

		if( ++p_a->m_bg_kill_count == 1 )
		{
			p_a->m_bg_kill_in_12seconds = 0;
		}
		else
		{
			if( (uint32)UNIXTIME - p_a->m_bg_kill_last_time < (12 - p_a->m_bg_kill_in_12seconds * 2) )
			{
				if( ++p_a->m_bg_kill_in_12seconds > 4 )
					p_a->m_bg_kill_in_12seconds = 4;
			}
			else
				p_a->m_bg_kill_in_12seconds = 0;
		}
		p_a->m_bg_kill_last_time = (uint32)UNIXTIME;
		p_a->m_bg_dota_state = 0;

		if( !m_FirstBlood )
		{
			p_a->m_bg_dota_state |= DOTA_FIRST_BLOOD;
			m_FirstBlood = true;
		}

		switch( p_a->m_bg_kill_in_12seconds )
		{
		case 1:
			p_a->m_bg_dota_state |= DOTA_2_KILL;
			break;
		case 2:
			p_a->m_bg_dota_state |= DOTA_3_KILL;
			break;
		case 3:
			p_a->m_bg_dota_state |= DOTA_4_KILL;
			break;
		case 4:
			p_a->m_bg_dota_state |= DOTA_5_KILL;
			break;
		default:
			break;
		}		
		switch( p_a->m_bg_kill_count )
		{
		case 1:
		case 2:
			break;
		case 3:
			p_a->m_bg_dota_state |= DOTA_KILLING_SPREE;
			break;
		case 4:
			p_a->m_bg_dota_state |= DOTA_DOMINATING;
			break;
		case 5:
			p_a->m_bg_dota_state |= DOTA_MEGAKILL;
			break;
		case 6:
			p_a->m_bg_dota_state |= DOTA_UNSTOPPABLE;
			break;
		case 7:
			p_a->m_bg_dota_state |= DOTA_WHICKEDSICK;
			break;
		case 8:
			p_a->m_bg_dota_state |= DOTA_MONSTERKILL;
			break;
		case 9:
			p_a->m_bg_dota_state |= DOTA_GODLIKE;
			break;
		default:
			p_a->m_bg_dota_state |= DOTA_HOLYSHIT;
			break;
		}
		if( p_a->m_bg_dota_state != 0 )
		{
			MSG_S2C::stBattleGroundSpecialNotify notify;
			notify.state = p_a->m_bg_dota_state;
			notify.is_break = false;
			notify.who = p_a->GetName();
			m_father->Broadcast( notify );
		}
	}
}

void SunyouBattleGroundBase::OnBGPlayerJoin( Player* p )
{
	if( p->GetGroup() )
		p->GetGroup()->RemovePlayer( p->m_playerInfo );

	if( p->GetGroup() == NULL )
	{
		if ( p->m_isGmInvisible == false ) //do not join invisible gm's into bg groups.
			m_groups[p->getRace() - 1]->AddMember( p->m_playerInfo );
	}

	p->m_bg_kill_count = 0;
	p->m_bg_dota_state = 0;
	p->m_bg_kill_in_12seconds = 0;
	p->m_bg_kill_last_time = 0;

	battle_ground_player_detail_information* info = new battle_ground_player_detail_information;
	info->id = p->GetLowGUID();
	info->name = p->GetName();
	info->race = p->getRace();
	info->cls = p->getClass();
	info->kill = 0;
	info->bekill = 0;
	info->honor = 0;
	info->damage = 0;
	info->healing = 0;
	info->team = info->race - 1;

	m_mapInformation[p->GetLowGUID()] = info;
}

void SunyouBattleGroundBase::OnBGPlayerLeave( Player* p )
{
	if( p->GetGroup() == m_groups[p->getRace() - 1] )
		p->GetGroup()->RemovePlayer( p->m_playerInfo );

// 	battle_ground_player_detail_information* info = FindInfoByPlayer( p );
// 	if( info )
// 	{
// 		delete info;
// 		m_mapInformation.erase( p );
// 	}
}

void SunyouBattleGroundBase::OnBGEndByRace( int WinnerRace, int LoserRace )
{
	for( std::set<Player*>::iterator it = m_father->m_players.begin(); it != m_father->m_players.end(); ++it )
	{
		Player* p = *it;
		if( WinnerRace == p->getRace() )
			OnBGWin( p );
	}
	sEventMgr.RemoveEvents( this, EVENT_SUNYOU_BATTLE_GROUND_UPDATE_INFORMATION );
	BroadCastInformation( WinnerRace );
}

void SunyouBattleGroundBase::OnBGWin( Player* p )
{
	p->m_playerInfo->AddBGWinCount( m_father->m_conf.mapid );

	uint32 ncnt = 0;//����Ͽ��ʤ������
	ncnt = p->m_playerInfo->GetBGWinCount(32) + p->m_playerInfo->GetBGWinCount(42) + p->m_playerInfo->GetBGWinCount(43) + 
		p->m_playerInfo->GetBGWinCount(44) + p->m_playerInfo->GetBGWinCount(45) + p->m_playerInfo->GetBGWinCount(46) + p->m_playerInfo->GetBGWinCount(47);
	if(p->m_Titles.find(29) != p->m_Titles.end())
	{
		if(ncnt >= 10)
		{//����սʿ
			p->InsertTitle(27);
			MSG_S2C::stTitleAdd msg;
			msg.title.title = 27;
			p->GetSession()->SendPacket(msg);
		}
	}
	if(p->m_Titles.find(30) != p->m_Titles.end())
	{
		if(ncnt >= 20)
		{//������ʿ
			p->InsertTitle(30);
			MSG_S2C::stTitleAdd msg;
			msg.title.title = 30;
			p->GetSession()->SendPacket(msg);
		}
	}
	if(p->m_Titles.find(31) != p->m_Titles.end())
	{
		if(ncnt >= 50)
		{//�¶�Ӣ��
			p->InsertTitle(31);
			MSG_S2C::stTitleAdd msg;
			msg.title.title = 31;
			p->GetSession()->SendPacket(msg);
		}
	}
	if(p->m_Titles.find(32) != p->m_Titles.end())
	{
		if(ncnt >= 100)
		{//����ս��
			p->InsertTitle(32);
			MSG_S2C::stTitleAdd msg;
			msg.title.title = 32;
			p->GetSession()->SendPacket(msg);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	ncnt = p->m_playerInfo->GetBGWinCount(49) + p->m_playerInfo->GetBGWinCount(50) + p->m_playerInfo->GetBGWinCount(51) + p->m_playerInfo->GetBGWinCount(52);
	if( ncnt >= 100 && !p->HasTitle( 67 ) )
	{
		p->QuickAddTitle( 67 );
	}
	else if( ncnt >= 50 && !p->HasTitle( 66 ) )
	{
		p->QuickAddTitle( 66 );
	}
	else if( ncnt >= 20 && !p->HasTitle( 65 ) )
	{
		p->QuickAddTitle( 65 );
	}
	else if( ncnt >= 10 && !p->HasTitle( 64 ) )
	{
		p->QuickAddTitle( 64 );
	}

	ncnt = p->m_playerInfo->GetBGWinCount(69) + p->m_playerInfo->GetBGWinCount(74) + p->m_playerInfo->GetBGWinCount(75) + p->m_playerInfo->GetBGWinCount(76);
	if( ncnt >= 100 && !p->HasTitle( 113 ) )
	{
		p->QuickAddTitle( 113 );
	}
	else if( ncnt >= 50 && !p->HasTitle( 112 ) )
	{
		p->QuickAddTitle( 112 );
	}
	else if( ncnt >= 20 && !p->HasTitle( 111 ) )
	{
		p->QuickAddTitle( 111 );
	}
	else if( ncnt >= 10 && !p->HasTitle( 110 ) )
	{
		p->QuickAddTitle( 110 );
	}

	if( p->HasTitle( 113 ) && p->HasTitle( 67 ) && p->HasTitle( 32 ) && !p->HasTitle( 114 ) )
		p->QuickAddTitle( 114 );
}

void SunyouBattleGroundBase::OnBGPlayerHonorModify( Player* p, int mod )
{
	battle_ground_player_detail_information* info = FindInfoByPlayer( p );
	if( info )
		info->honor += mod;
}

void SunyouBattleGroundBase::OnBGPlayerDamage( Player* p, uint32 damage )
{
	battle_ground_player_detail_information* info = FindInfoByPlayer( p );
	if( info )
		info->damage += damage;
}

void SunyouBattleGroundBase::OnBGPlayerHealing( Player* p, uint32 healing )
{
	battle_ground_player_detail_information* info = FindInfoByPlayer( p );
	if( info )
		info->healing += healing;
}

void SunyouBattleGroundBase::BroadCastInformation( int WinnerRace )
{
	MSG_S2C::stBattleGroundDetailInformation msg;
	msg.WinnerRace = WinnerRace;
	msg.IsByParty = 0; 

	for( std::map<ui32, battle_ground_player_detail_information*>::iterator it = m_mapInformation.begin(); it != m_mapInformation.end(); ++it )
		msg.v.push_back( *it->second );

	if( msg.v.size() > 0 )
		m_father->Broadcast( msg );
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

SunyouTeamGroupBattleGroundBase::SunyouTeamGroupBattleGroundBase(SunyouInstance* father)
:SunyouBattleGroundBase(father)
{
}
SunyouTeamGroupBattleGroundBase::~SunyouTeamGroupBattleGroundBase()
{

}

void SunyouTeamGroupBattleGroundBase::BroadCastNewInformation( int WinTeam )
{
	MSG_S2C::stBattleGroundDetailInformation msg;
	msg.WinnerRace = WinTeam;
	msg.IsByParty = 1;

	for( std::map<ui32, battle_ground_player_detail_information*>::iterator it = m_mapInformation.begin(); it != m_mapInformation.end(); ++it )
		msg.v.push_back( *it->second );

	if( msg.v.size() > 0 )
		m_father->Broadcast( msg );
}
void SunyouTeamGroupBattleGroundBase::OnBGStart()
{
	sEventMgr.AddEvent( this, &SunyouTeamGroupBattleGroundBase::BroadCastNewInformation, 0, EVENT_SUNYOU_BATTLE_GROUND_UPDATE_INFORMATION, 3000, 10000, 0 );
}

void SunyouTeamGroupBattleGroundBase::OnBGEnd( ui32 teamid )
{
	for( std::set<Player*>::iterator it = m_father->m_players.begin(); it != m_father->m_players.end(); ++it )
	{
		Player* p = *it;
		if( p->m_team_arena_idx == teamid )
			OnBGWin( p );
	}
	sEventMgr.RemoveEvents( this, EVENT_SUNYOU_BATTLE_GROUND_UPDATE_INFORMATION );
	BroadCastNewInformation( teamid );
}
void SunyouTeamGroupBattleGroundBase::OnBGPlayerJoin( Player* p )
{
	if( p->GetGroup() )
		p->GetGroup()->RemovePlayer( p->m_playerInfo );

	if( p->GetGroup() == NULL )
	{
		if ( p->m_isGmInvisible == false) //do not join invisible gm's into bg groups.
			m_groups[p->m_team_arena_idx - 1]->AddMember( p->m_playerInfo );
	}

	p->m_bg_kill_count = 0;
	p->m_bg_dota_state = 0;
	p->m_bg_kill_in_12seconds = 0;
	p->m_bg_kill_last_time = 0;

	battle_ground_player_detail_information* info = new battle_ground_player_detail_information;
	info->id = p->GetLowGUID();
	info->name = p->GetName();
	info->race = p->getRace();
	info->cls = p->getClass();
	info->kill = 0;
	info->bekill = 0;
	info->honor = 0;
	info->damage = 0;
	info->healing = 0;
	info->team = p->m_team_arena_idx - 1;

	m_mapInformation[p->GetLowGUID()] = info;
}
void SunyouTeamGroupBattleGroundBase::OnBGPlayerLeave( Player* p )
{
	if( p->GetGroup() == m_groups[p->m_team_arena_idx - 1]  )
		p->GetGroup()->RemovePlayer( p->m_playerInfo );

// 	battle_ground_player_detail_information* info = FindInfoByPlayer( p );
// 	if( info )
// 	{
// 		delete info;
// 		m_mapInformation.erase( p );
// 	}
}