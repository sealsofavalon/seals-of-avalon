#ifndef _SUNYOU_BATTLE_GROUND_BASE_HEAD
#define _SUNYOU_BATTLE_GROUND_BASE_HEAD

class SunyouInstance;

class SunyouBattleGroundBase : public EventableObject
{
public:
	SunyouBattleGroundBase( SunyouInstance* father );
	virtual ~SunyouBattleGroundBase();

	virtual void OnBGStart();
	void OnBGPlayerDie( Player* victim, Object* attacker );
	virtual void OnBGPlayerJoin( Player* p );
	virtual void OnBGPlayerLeave( Player* p );
	void OnBGEndByRace( int WinnerRace, int LoserRace );
	void OnBGPlayerHonorModify( Player* p, int mod );
	void OnBGPlayerDamage( Player* p, uint32 damage );
	void OnBGPlayerHealing( Player* p, uint32 healing );
	void OnBGWin( Player* p );

	void BroadCastInformation( int WinnerRace );

protected:
	bool m_FirstBlood;
	Group* m_groups[3];
	SunyouInstance* m_father;
	std::map<ui32, battle_ground_player_detail_information*> m_mapInformation;

	battle_ground_player_detail_information* FindInfoByPlayer( Player* p )
	{
		std::map<ui32, battle_ground_player_detail_information*>::iterator it = m_mapInformation.find( p->GetLowGUID() );
		if( it != m_mapInformation.end() )
			return it->second;
		else
			return NULL;
	}
};

class SunyouTeamGroupBattleGroundBase : public SunyouBattleGroundBase
{
public:
	SunyouTeamGroupBattleGroundBase(SunyouInstance* father);
	virtual ~SunyouTeamGroupBattleGroundBase();
	virtual void OnBGPlayerJoin( Player* p );
	virtual void OnBGPlayerLeave( Player* p );

	virtual void OnBGStart();
	void OnBGEnd( ui32 teamid );
	void BroadCastNewInformation( int WinTeam );
private:
};
#endif // _SUNYOU_BATTLE_GROUND_BASE_HEAD
