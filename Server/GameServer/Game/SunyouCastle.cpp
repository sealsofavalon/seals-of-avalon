#include "StdAfx.h"
#include "SunyouCastle.h"

void parse_string_and_push_to_vector( const char* str, std::vector<LocationVector*>& v )
{
	char lvstring[5120] = { 0 };
	strcpy( lvstring, str );
	char* start = lvstring;
	char* end = lvstring;
	float value[4] = { 0.f };
	int n = 0;
	while( true )
	{
		if( !*start ) break;
		end = strchr( start, ',' );
		if( end ) *end = 0;
		value[n] = atof( start );

		if( ++n >= 4 )
		{
			v.push_back( new LocationVector( value[0], value[1], value[2], value[3] ) );
			n = 0;
		}
		if( !end ) break;
		start = end + 1;
		if( !*start ) break;
	}
}

void parse_string_and_insert_to_set( const char* str, std::set<uint32>& s )
{
	if( !str ) return;
	char lvstring[5120] = { 0 };
	strcpy( lvstring, str );
	uint32 Counter = 0;
	char * end;
	char * start = lvstring;
	while( true )
	{
		end = strchr(start,',');
		if(!end)break;
		*end=0;
		uint32 value = atol(start);
		s.insert( value );
		start = end +1;
		Counter++;
	}
}


SunyouCastle::SunyouCastle() : m_state( 0 ), m_defence_guild( 0 ), m_boss_guid( 0 ), m_last_event_time( 0 ), m_boss_die_owner_guild( 0 ), boss_die_event( false ), m_count_down_mins( 0xFFFF ), m_is_loading_ok( false )
{

}

void SunyouCastle::Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr )
{
	SunyouInstance::Init( conf, pMapMgr );
	m_castle_config = dbcCastleConfig.LookupEntry( conf.mapid );
	if( !m_castle_config )
	{
		assert( 0 );
		return;
	}
	parse_string_and_push_to_vector( m_castle_config->offense_resurrect_points, m_offense_resurrect_points );
	parse_string_and_push_to_vector( m_castle_config->defence_resurrect_points, m_defence_resurrect_points );
	parse_string_and_push_to_vector( m_castle_config->neutral_resurrect_points, m_neutral_resurrect_points );
	parse_string_and_insert_to_set( m_castle_config->invincible_npcs, m_invincible_npcs );
	parse_string_and_push_to_vector( m_castle_config->kick_out_points, m_kick_out_points );
	parse_string_and_insert_to_set( m_castle_config->start_fight_day_of_week, m_start_fight_day_of_week );

	// for test
	//m_castle_config->fight_duration = 1;
}

void SunyouCastle::Update()
{
	if( !m_is_loading_ok ) return;

	tm* ptm = localtime( &UNIXTIME );
	switch( m_state )
	{
	case CASTLE_STATE_BUILDING:
	case CASTLE_STATE_FREE:
		{
			if( IsFightDayOfWeek( ptm->tm_wday ) )
			{
				int evt_hour = m_castle_config->start_fight_hour - m_castle_config->fight_prepare_time;
				CountDown( CASTLE_STATE_READY_TO_BATTLE, evt_hour, ptm );
				if( ptm->tm_hour == evt_hour )
					OnEnrollEnd();
			}
		}
		break;
	case CASTLE_STATE_READY_TO_BATTLE:
		{
			if( IsFightDayOfWeek( ptm->tm_wday ) )
			{
				int evt_hour = m_castle_config->start_fight_hour;
				CountDown( CASTLE_STATE_BATTLE, evt_hour, ptm );
				if( ptm->tm_hour == evt_hour )
					OnFightStart();
			}
		}
		break;
	case CASTLE_STATE_BATTLE:
		{
			if( IsFightDayOfWeek( ptm->tm_wday ) )
			{
				int evt_hour = m_castle_config->start_fight_hour + m_castle_config->fight_duration;
				CountDown( CASTLE_STATE_FREE, evt_hour, ptm );
				if( ptm->tm_hour == evt_hour )
					OnFightEnd( true );
			}
		}
		break;
	}
	/*

	if( m_last_event_time == 0 ) return;
	switch( m_state )
	{
	case CASTLE_STATE_BUILDING:
	case CASTLE_STATE_FREE:
		{
			if( (uint32)UNIXTIME - m_last_event_time >= 60*5 )
			{
				OnEnrollEnd();
			}
		}
		break;
	case CASTLE_STATE_READY_TO_BATTLE:
		{
			if( (uint32)UNIXTIME - m_last_event_time >= 60*2 )
			{
				OnFightStart();
			}
		}
		break;
	case CASTLE_STATE_BATTLE:
		{
			if( (uint32)UNIXTIME - m_last_event_time >= 60*30 )
			{
				OnFightEnd( true );
			}
		}
		break;
	}
	*/
	if( boss_die_event )
	{
		Guild* p = objmgr.GetGuild( m_defence_guild );
		if (p)
		{
			p->SetCastle(0);
		}
		p = objmgr.GetGuild( m_boss_die_owner_guild );
		if( p )
		{
			p->SetCastle(1);
			if( m_defence_guild == m_boss_die_owner_guild )
			{
				MyLog::log->error("m_defence_guild == m_boss_die_owner_guild");
				m_defence_guild = 0;
			}
			else if( IsEnrollGuild( m_boss_die_owner_guild ) )
				m_defence_guild = m_boss_die_owner_guild;
			else
				m_defence_guild = 0;
		}
		else
		{
			m_defence_guild = 0;
		}
		m_npcs.clear();
		OnFightEnd( false );
		m_boss_die_owner_guild = 0;
		boss_die_event = false;
	}
}

void SunyouCastle::Expire()
{
}

void SunyouCastle::OnPlayerJoin( Player* p )
{
	if( !p->IsValid() || !p->IsInWorld() || p->IsSunyouInstanceJoinLocked() )
		return;

	SunyouInstance::OnPlayerJoin( p );

	if( m_state == CASTLE_STATE_FREE )
	{
		if( IsEnrollGuild( p->GetGuildId() ) )
			SetPlayerFaction( p, CASTLE_FACTION_OFFENSE );
		else
			SetPlayerFaction( p, CASTLE_FACTION_NEUTRAL );
	}
	else
	{
		Guild* pGuild = p->GetGuild();
		if( pGuild )
		{
			if( pGuild->IsAlliance( m_defence_guild ) )
			{
				SetPlayerFaction( p, CASTLE_FACTION_DEFENCE );
			}
			else if( IsEnrollGuild( p->GetGuildId() ) )
			{
				SetPlayerFaction( p, CASTLE_FACTION_OFFENSE );
			}
			else
				SetPlayerFaction( p, CASTLE_FACTION_NEUTRAL );
		}
		else
			SetPlayerFaction( p, CASTLE_FACTION_NEUTRAL );
	}
}

void SunyouCastle::SetPlayerFaction( Player* p, uint32 faction, bool force_transport )
{
	switch( faction )
	{
	case CASTLE_FACTION_DEFENCE:
		{
			p->SetCastleDebuffSpellID( 0 );
			p->SetRandomResurrectLocation( m_defence_resurrect_points );

			// this may cause server crash
			if( force_transport )
				p->SafeTeleport( m_mapmgr->GetMapId(), m_mapmgr->GetInstanceID(), *m_defence_resurrect_points[rand()%m_defence_resurrect_points.size()] );
			m_defence_players.insert( p );
		}
		break;
	case CASTLE_FACTION_OFFENSE:
		{
			p->SetCastleDebuffSpellID( 0 );
			p->SetRandomResurrectLocation( m_offense_resurrect_points );
			if( force_transport )
				p->SafeTeleport( m_mapmgr->GetMapId(), m_mapmgr->GetInstanceID(), *m_offense_resurrect_points[rand()%m_offense_resurrect_points.size()] );
			m_offense_players.insert( p );
		}
		break;
	case CASTLE_FACTION_NEUTRAL:
		{
			p->SetCastleDebuffSpellID( 3 );
			p->SetRandomResurrectLocation( m_neutral_resurrect_points );
			if( force_transport )
				p->SafeTeleport( m_mapmgr->GetMapId(), m_mapmgr->GetInstanceID(), *m_neutral_resurrect_points[rand()%m_neutral_resurrect_points.size()] );
			m_neutral_players.insert( p );
		}
		break;
	default:
		assert( 0 );
		return;
	}
	p->SetFFA( faction );
}

void SunyouCastle::OnPlayerLeave( Player* p )
{
	if( !p->m_sunyou_instance )
		return;
	SunyouInstance::OnPlayerLeave( p );
	p->SetFFA( 0 );
	p->SetCastleDebuffSpellID( 0 );
	m_defence_players.erase( p );
	m_offense_players.erase( p );
	m_neutral_players.erase( p );
}

void SunyouCastle::OnInstanceStartup()
{
	SunyouInstance::OnInstanceStartup();
	OnEnrollStart();
	Load();
	m_is_loading_ok = true;
	m_last_event_time = (uint32)UNIXTIME;
}

bool SunyouCastle::BuyNPC( Player* p, uint32 entry )
{
	if( m_state != CASTLE_STATE_BUILDING )
	{
		p->GetSession()->SendNotification( "城堡不在建造期间，不可以购置守卫" );
		return false;
	}
	if( !p->GetGuild() )
	{
		p->GetSession()->SendNotification( "你不属于任何部族，不可以购置守卫" );
		return false;
	}
	if( !p->GetGuild()->IsAlliance( m_defence_guild ) )
	{
		p->GetSession()->SendNotification( "不是城主，不能购置守卫" );
		return false;
	}
	if( p->GetGuildRank() != 0 )
	{
		p->GetSession()->SendNotification( "不是部族首领，不能购置守卫" );
		return false;
	}

	castle_npc* cn = dbcCastleNPCList.LookupEntry( entry );
	if( cn )
	{
		if( p->GetUInt32Value( PLAYER_FIELD_COINAGE ) < 10000 * cn->gold )
		{
			p->GetSession()->SendNotification( "金币不够，不能购置守卫，需要%d金", cn->gold );
			return false;
		}
		if( !p->GetGuild()->SpendScore( cn->points ) )
		{
			p->GetSession()->SendNotification( "部族积分不够，不能购置守卫，需要%d积分", cn->points );
			return false;
		}

		p->ModCoin(cn->gold * 10000 );
		MyLog::yunyinglog->info( "gold-castle-buynpc player:[%s][%d] spend_gold:[%d] npc_entry:[%d]", p->GetName(), p->GetLowGUID(), cn->gold*10000, entry );
		m_npcs.insert( entry );

		SendNPCState2Player( p );
		return true;
	}

	return false;
}

bool SunyouCastle::Enroll( Player* p )
{
	if( m_state == CASTLE_STATE_READY_TO_BATTLE || m_state == CASTLE_STATE_BATTLE )
	{
		p->GetSession()->SendNotification( "攻城报名已截止" );
		return false;
	}
	if( !p->GetGuild() )
	{
		p->GetSession()->SendNotification( "你不属于任何部族，不可以报名攻城" );
		return false;
	}
	if( p->GetGuild()->IsAlliance( m_defence_guild ) )
	{
		p->GetSession()->SendNotification( "已经是城主，不能报名攻城" );
		return false;
	}
	if( p->GetGuildRank() != 0 )
	{
		p->GetSession()->SendNotification( "不是部族首领，不能报名攻城" );
		return false;
	}
	if( m_enroll_guilds.end() != m_enroll_guilds.find( p->GetGuildId() ) )
	{
		p->GetSession()->SendNotification( "已经报过名了" );
		return false;
	}
	if( !p->GetGuild()->SpendScore( m_castle_config->enroll_cost ) )
	{
		p->GetSession()->SendNotification( "部族积分不够，部族需要拥有至少%d积分才能申请攻城", m_castle_config->enroll_cost );
		return false;
	}
	m_enroll_guilds.insert( p->GetGuildId() );
	p->GetGuild()->AddEnroll( m_mapmgr->GetMapId() );
	p->GetSession()->SendNotification( "报名已接受，花费部族积分:%d", m_castle_config->enroll_cost );

	MSG_S2C::stNotify_Msg msg;
	msg.notify = "部族首领已经报名参加攻打";
	msg.notify += m_mapmgr->GetMapInfo()->name;
	p->GetGuild()->SendPacket( msg );

	for( std::set<Player*>::iterator it = m_neutral_players.begin(); it != m_neutral_players.end(); )
	{
		std::set<Player*>::iterator it2 = it++;
		Player* tmp = *it2;
		if( tmp->GetGuildId() == p->GetGuildId() )
		{
			m_neutral_players.erase( it2 );
			SetPlayerFaction( tmp, CASTLE_FACTION_OFFENSE );
		}
	}
	Save();
	return true;
}

void SunyouCastle::Save()
{
	std::stringstream ss;
	ss << "replace into castles values(" << m_conf.mapid << ", '";

	for( std::set<uint32>::iterator it = m_enroll_guilds.begin(); it != m_enroll_guilds.end(); ++it )
		ss << *it << ",";

	ss << "', " << m_defence_guild << ", " << m_state << ", '";

	for( std::set<uint32>::iterator it = m_npcs.begin(); it != m_npcs.end(); ++it )
		ss << *it << ",";

	ss << "');";

	sWorld.ExecuteSqlToDBServer( ss.str().c_str() );
}

bool SunyouCastle::Load()
{
	smart_ptr<QueryResult> qr = CharacterDatabase.Query( "select * from castles where mapid = %d", m_conf.mapid );
	if( qr )
	{
		Field* f = qr->Fetch();
		parse_string_and_insert_to_set( f[1].GetString(), m_enroll_guilds );
		m_defence_guild = f[2].GetUInt32();
		m_state = f[3].GetUInt32();
		parse_string_and_insert_to_set( f[4].GetString(), m_npcs );

		for( std::set<uint32>::iterator it = m_enroll_guilds.begin(); it != m_enroll_guilds.end(); ++it )
		{
			Guild* p = objmgr.GetGuild( *it );
			if( p )
				p->AddEnroll( m_mapmgr->GetMapId() );
		}
		return true;
	}

	return false;
}

void SunyouCastle::OnEnrollStart()
{
	return;

	for( std::set<uint32>::iterator it = m_enroll_guilds.begin(); it != m_enroll_guilds.end(); ++it )
	{
		Guild* p = objmgr.GetGuild( *it );
		if( p )
			p->RemoveEnroll( m_mapmgr->GetMapId() );
	}
	m_enroll_guilds.clear();
	if( m_defence_guild )
		m_state = CASTLE_STATE_BUILDING;
	else
	{
		m_state = CASTLE_STATE_FREE;
		FillAllCastleGuards();
	}

	char txt[256] = { 0 };
	sprintf( txt, "[%s]已开始接受攻打报名", m_mapmgr->GetMapInfo()->name );
	sWorld.SendWorldText( txt );

	if( m_invincible_npcs_guid.size() == 0 )
		m_mapmgr->SpawnCreatures( this, m_invincible_npcs, m_invincible_npcs_guid, true );
}

void SunyouCastle::OnEnrollEnd()
{
	return;

	m_state = CASTLE_STATE_READY_TO_BATTLE;

	char txt[256] = { 0 };
	sprintf( txt, "报名攻打[%s]已截止，战斗即将开始，请大家做好准备", m_mapmgr->GetMapInfo()->name );
	sWorld.SendWorldText( txt );

	m_last_event_time = (uint32)UNIXTIME;
}

void SunyouCastle::OnFightStart()
{
	m_state = CASTLE_STATE_BATTLE;
	m_mapmgr->DespawnCreatures( m_invincible_npcs_guid );
	m_invincible_npcs_guid.clear();
	m_mapmgr->SpawnCreatures( this, m_npcs, m_npcs_guid );
	m_mapmgr->SpawnCastleBoss( this, m_castle_config->boss_id, m_boss_guid );

	char txt[256] = { 0 };
	sprintf( txt, "[%s]的战斗开始了", m_mapmgr->GetMapInfo()->name );
	sWorld.SendWorldText( txt );

	m_last_event_time = (uint32)UNIXTIME;

	m_conf.expire = m_castle_config->fight_duration * 60 * 60;
	m_starttime = (uint32)UNIXTIME;
}

void SunyouCastle::OnFightEnd( bool is_timeout )
{
	char txt[256] = { 0 };
	m_defence_players.clear();
	m_offense_players.clear();
	m_neutral_players.clear();

	if( is_timeout )
		m_defence_guild = 0;

	if( m_defence_guild )
	{
		Guild* pGuild = objmgr.GetGuild( m_defence_guild );
		if( pGuild )
		{
			if( is_timeout )
				sprintf( txt, "[%s]守住了[%s]！", pGuild->GetGuildName(), m_mapmgr->GetMapInfo()->name );
			else
			{
				m_npcs.clear();
				sprintf( txt, "[%s]占领了[%s]！", pGuild->GetGuildName(), m_mapmgr->GetMapInfo()->name );
			}
			sWorld.SendWorldText( txt );
			std::set<Player*> tmp( m_players );
			for( std::set<Player*>::iterator it = tmp.begin(); it != tmp.end(); ++it )
			{

				Player* p = *it;
				Guild* g = p->GetGuild();
				if( g )
				{
					if( !g->IsAlliance( m_defence_guild ) )
					{
						//SetPlayerFaction( p, CASTLE_FACTION_NEUTRAL );
						KickoutPlayer( p );
					}
					else
					{
						SetPlayerFaction( p, CASTLE_FACTION_DEFENCE, false );
					}
				}
			}
		}
		else
			m_defence_guild = 0;
	}

	if( m_defence_guild == 0 )
	{
		if( is_timeout )
			sprintf( txt, "攻打时间已到，无人占领[%s]，下次再战吧！", m_mapmgr->GetMapInfo()->name );
		else
			sprintf( txt, "中立人士占领了[%s]，城堡恢复无人占领状态，下次再战吧！", m_mapmgr->GetMapInfo()->name );
		sWorld.SendWorldText( txt );

		std::set<Player*> tmp( m_players );
		for( std::set<Player*>::iterator it = tmp.begin(); it != tmp.end(); ++it )
		{
			Player* p = *it;
			//SetPlayerFaction( p, CASTLE_FACTION_NEUTRAL );
			KickoutPlayer( p );
		}
	}
	m_mapmgr->DespawnCreatures( m_npcs_guid );
	if( !is_timeout )
		m_npcs_guid.clear();
	std::set<uint32> tmp;
	tmp.insert( m_boss_guid );
	m_mapmgr->DespawnCreatures( tmp );
	m_boss_guid = 0;
	OnEnrollStart();
	Save();
	m_last_event_time = (uint32)UNIXTIME;
}

void SunyouCastle::KickoutPlayer( Player* p )
{
	if( m_castle_config->kick_out_map_id == m_mapmgr->GetMapId() )
		SetPlayerFaction( p, CASTLE_FACTION_NEUTRAL );//p->SafeTeleport( m_castle_config->kick_out_map_id, m_mapmgr->GetInstanceID(), *m_kick_out_points[rand() % m_kick_out_points.size()] );
	else
		p->SafeTeleport( m_castle_config->kick_out_map_id, 0, *m_kick_out_points[rand() % m_kick_out_points.size()] );
}

void SunyouCastle::OnBossDie( Creature* boss )
{
	std::map<uint32, uint32> damageorder;

	for( std::map<uint32, uint32>::iterator it = boss->m_damage_taken_record.begin(); it != boss->m_damage_taken_record.end(); ++it )
		damageorder[it->second] = it->first;

	if( damageorder.size() )
		m_boss_die_owner_guild = damageorder.rbegin()->second;
	boss_die_event = true;
}

bool SunyouCastle::IsEnrollGuild( uint32 guildid )
{
	return m_enroll_guilds.find( guildid ) != m_enroll_guilds.end();
}

void SunyouCastle::SendNPCState2Player( Player* p )
{
	if( p->GetGuild() && p->GetGuild()->IsAlliance( m_defence_guild ) )
	{
		MSG_S2C::stCastleNPCStateNotify notify;
		notify.map_id = m_mapmgr->GetMapId();
		notify.buy_npcs = m_npcs;
		p->GetSession()->SendPacket( notify );
	}
}

void SunyouCastle::FillAllCastleGuards()
{
	m_npcs.clear();
	StorageContainerIterator<castle_npc>* it = dbcCastleNPCList.MakeIterator();
	while(!it->AtEnd())
	{
		castle_npc* cn = it->Get();
		m_npcs.insert( cn->entry );
		if(!it->Inc())
			break;
	}
	it->Destruct();
}

bool SunyouCastle::IsFightDayOfWeek( uint32 d ) const
{
	return m_start_fight_day_of_week.end() != m_start_fight_day_of_week.find( d );
}

void SunyouCastle::SetFightDayOfWeek( const std::set<uint32>& sd )
{
	m_start_fight_day_of_week = sd;
}

void SunyouCastle::CountDown( uint32 next_state, int event_hour, tm* ptm )
{
	if( ptm->tm_min != m_count_down_mins && event_hour - ptm->tm_hour == 1 && ( ( ptm->tm_min % 5 ) == 0 || ptm->tm_min > 55 ) )
	{
		MSG_S2C::stCastleCountDown ccd;
		ccd.castle_name = m_mapmgr->GetMapInfo()->name;
		ccd.mapid = m_mapmgr->GetMapId();
		ccd.next_state = next_state;
		ccd.count_down_mins = 60 - ptm->tm_min;
		sWorld.SendGlobalMessage( ccd );
		m_count_down_mins = ptm->tm_min;

		MyLog::log->info( "castle:[%s] count down %d minutes, next state:%d", ccd.castle_name.c_str(), (int)ccd.count_down_mins, (int)next_state );
	}
}