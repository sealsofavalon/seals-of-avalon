#ifndef _SUNYOU_CASTLE_HEAD
#define _SUNYOU_CASTLE_HEAD

#include "SunyouInstance.h"

class InstanceQueueMgr;

class SunyouCastle : public SunyouInstance
{
public:
	friend class InstanceQueueMgr;
	SunyouCastle();

	virtual void Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr );
	virtual void Update();
	virtual void Expire();
	virtual void OnPlayerJoin( Player* p );
	virtual void OnPlayerLeave( Player* p );
	virtual void OnInstanceStartup();

	void SetFightDayOfWeek( const std::set<uint32>& sd );
	void SetPlayerFaction( Player* p, uint32 faction, bool force_transport = true );
	bool BuyNPC( Player* p, uint32 entry );
	bool Enroll( Player* p );
	void Save();
	bool Load();
	void OnEnrollStart();
	void OnEnrollEnd();
	void OnFightStart();
	void OnFightEnd( bool is_timeout );
	void KickoutPlayer( Player* p );
	void OnBossDie( Creature* boss );
	bool IsEnrollGuild( uint32 guildid );
	void SendNPCState2Player( Player* p );

	inline const castle_config* GetCastleConfig() const { return m_castle_config; }

protected:
	enum { CASTLE_FACTION_DEFENCE = 11, CASTLE_FACTION_OFFENSE = 12, CASTLE_FACTION_NEUTRAL = 13 };
	enum { CASTLE_STATE_FREE, CASTLE_STATE_BUILDING, CASTLE_STATE_READY_TO_BATTLE, CASTLE_STATE_BATTLE };
	std::set<uint32> m_enroll_guilds;
	uint32 m_defence_guild;

	std::set<Player*> m_offense_players;
	std::set<Player*> m_defence_players;
	std::set<Player*> m_neutral_players;
	uint32 m_state;
	std::set<uint32> m_npcs;
	std::set<uint32> m_npcs_guid;
	std::set<uint32> m_start_fight_day_of_week;
	castle_config* m_castle_config;

	std::vector<LocationVector*> m_offense_resurrect_points;
	std::vector<LocationVector*> m_defence_resurrect_points;
	std::vector<LocationVector*> m_neutral_resurrect_points;
	std::vector<LocationVector*> m_kick_out_points;
	std::set<uint32> m_invincible_npcs;
	std::set<uint32> m_invincible_npcs_guid;
	uint32 m_boss_guid;
	uint32 m_last_event_time;
	uint32 m_boss_die_owner_guild;
	bool boss_die_event;
	uint32 m_count_down_mins;
	bool m_is_loading_ok;

	void FillAllCastleGuards();
	bool IsFightDayOfWeek( uint32 d ) const;
	void CountDown( uint32 next_state, int event_hour, tm* ptm );
};

#endif