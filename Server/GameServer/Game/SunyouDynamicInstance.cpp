#include "StdAfx.h"
#include "SunyouDynamicInstance.h"

SunyouDynamicInstance::SunyouDynamicInstance()
{
}

SunyouDynamicInstance::~SunyouDynamicInstance()
{

}

void SunyouDynamicInstance::OnPlayerJoin( Player* p )
{
	SunyouMonsterGarden::OnPlayerJoin( p );
	if( !p->isAlive() )
		p->ResurrectPlayer();
}

void SunyouDynamicInstance::OnPlayerLeave( Player* p )
{
	SunyouMonsterGarden::OnPlayerLeave( p );
}
