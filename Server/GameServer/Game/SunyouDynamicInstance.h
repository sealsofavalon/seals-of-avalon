#ifndef _SUNYOU_DYNAMIC_INSTANCE_HEAD
#define _SUNYOU_DYNAMIC_INSTANCE_HEAD

#include "SunyouMonsterGarden.h"

class SunyouDynamicInstance : public SunyouMonsterGarden
{
public:
	SunyouDynamicInstance();
	virtual ~SunyouDynamicInstance();

	virtual void OnPlayerJoin( Player* p );
	virtual void OnPlayerLeave( Player* p );
};

#endif