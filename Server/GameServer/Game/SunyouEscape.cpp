#include "StdAfx.h"
#include "SunyouEscape.h"

SunyouEscape::SunyouEscape()
{
	m_isgardenmode = true;
}

SunyouEscape::~SunyouEscape()
{

}

void SunyouEscape::OnInstanceStartup()
{
	SunyouDynamicInstance::OnInstanceStartup();
	StorageContainerIterator<sunyou_instance_periodic_gift>* it = dbcSunyouInstancePeriodicGift.MakeIterator();
	while(!it->AtEnd())
	{
		sunyou_instance_periodic_gift* sipg = it->Get();
		if( sipg->mapid == m_conf.mapid )
			sEventMgr.AddEvent( this, &SunyouEscape::OnGiftEvent, sipg, EVENT_SUNYOU_INSTANCE_PERIODIC_GIFT, sipg->time * 1000, 1, 0 );
		if(!it->Inc())
			break;
	}
	it->Destruct();
}

void SunyouEscape::OnPlayerJoin( Player* p )
{
	if( !p->IsValid() || !p->IsInWorld() || p->IsSunyouInstanceJoinLocked() )
		return;

	SunyouDynamicInstance::OnPlayerJoin( p );
	p->m_escape_mode = true;
}

void SunyouEscape::OnPlayerLeave( Player* p )
{
	if( !p->m_sunyou_instance )
		return;
	SunyouDynamicInstance::OnPlayerLeave( p );
	p->m_escape_mode = false;
}

void SunyouEscape::OnGiftEvent( sunyou_instance_periodic_gift* sipg )
{
	for( std::set<Player*>::iterator it2 = m_players.begin(); it2 != m_players.end(); ++it2 )
	{
		Player* ply = *it2;
		uint32 xp = 0;
		if( sipg->type == 3 )
		{
			xp = (uint32)( (float)ply->lvlinfo->XPToNextLevel * (float)sipg->misc1 / 1000.f );
			ply->GiveGift( sipg->type, xp, sipg->misc2, true, Player::GIFT_CATEGORY_INSTANCE );
		}
		else
			ply->GiveGift( sipg->type, sipg->misc1, sipg->misc2, true, Player::GIFT_CATEGORY_INSTANCE );
	}
}
