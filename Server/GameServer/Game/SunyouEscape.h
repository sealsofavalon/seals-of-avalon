#ifndef _SUNYOU_ESCAPE_HEAD
#define _SUNYOU_ESCAPE_HEAD

#include "SunyouDynamicInstance.h"

class SunyouEscape : public SunyouDynamicInstance
{
public:
	SunyouEscape();
	virtual ~SunyouEscape();

	virtual void OnPlayerJoin( Player* p );
	virtual void OnPlayerLeave( Player* p );
	virtual void OnInstanceStartup();

	void OnGiftEvent( sunyou_instance_periodic_gift* sipg );
};

#endif