#include "StdAfx.h"
#include "SunyouFairground.h"
#include "InstanceQueue.h"

SunyouFairground::SunyouFairground()
{

}

SunyouFairground::~SunyouFairground()
{
	g_instancequeuemgr.RemoveSunyouFairground( this );
}

void SunyouFairground::OnPlayerJoin( Player* p )
{
	if( !p->isAlive() )
	{
		p->SetRandomResurrectLocation( m_conf.random_vrl );
		p->ResurrectPlayer();
	}
	SunyouRaid::OnPlayerJoin( p );
}
