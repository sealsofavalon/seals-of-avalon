#ifndef _SUNYOU_FAIRGROUND_HEAD
#define _SUNYOU_FAIRGROUND_HEAD

#include "SunyouRaid.h"

class SunyouFairground : public SunyouRaid
{
public:
	SunyouFairground();
	virtual ~SunyouFairground();
	
public:
	virtual void OnPlayerJoin( Player* p );
};

#endif	// _SUNYOU_FAIRGROUND_HEAD
