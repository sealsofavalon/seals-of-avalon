#include "StdAfx.h"
#include "SunyouInstance.h"
#include "../Master.h"
#include "SunyouBattleGround.h"
#include "SunyouAdvancedBattleGround.h"
#include "../Master.h"

static std::set<SunyouInstance*> s_valid_sunyouinstances;

SunyouInstance::SunyouInstance()
 : m_mapmgr( NULL ), m_instance( NULL ), m_starttime( 0 ), m_isinitcreatures( false ),
	m_isgardenmode( false ), m_isdynamicproc( false ), m_InstanceStarted( false ), m_player_count( 0 ),
	m_isprivate( false )
{
	s_valid_sunyouinstances.insert( this );
}

SunyouInstance::~SunyouInstance()
{
	for( std::set<Player*>::iterator it = m_players.begin(); it != m_players.end(); ++it )
	{
		Player* p = *it;
		if( p->IsValid() )
			p->m_sunyou_instance = NULL;
	}

	s_valid_sunyouinstances.erase( this );
}

void SunyouInstance::Update()
{

}

void SunyouInstance::Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr )
{
	m_conf = conf;
	m_mapmgr = pMapMgr;
	m_instance = pMapMgr->m_pInstance;
	/*
	if( !g_crosssrvmgr->m_isInstanceSrv )
	{
		sEventMgr.AddEvent( this, &SunyouInstance::OnInstanceStartup, EVENT_SUNYOU_INSTANCE_STARTUP, 9*1000, 1, 0 );
	}
	else if( conf.lua_scripts.size() == 0 )
	{
	*/
	if( conf.lua_scripts.size() == 0 )
		this->OnInstanceStartup();
	//}
}

void SunyouInstance::Expire()
{
	m_mapmgr->TeleportPlayers();
	DeleteThis();
	//sEventMgr.AddEvent( this, &SunyouInstance::DeleteThis, EVENT_SUNYOU_INSTANCE_DELETE_THIS, 10*1000, 1, 0 );
}

void SunyouInstance::DeleteThis()
{
	sInstanceMgr.DeleteSunyouInstance( m_instance, true );
	delete m_mapmgr;
	delete this;
}

SunyouBattleGroundBase* SunyouInstance::GetBGBase()
{
	if( GetCategory() == INSTANCE_CATEGORY_BATTLE_GROUND )
		return static_cast<SunyouBattleGround*>( this )->m_pBGBase;
	else if( GetCategory() == INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND )
		return static_cast<SunyouAdvancedBattleGround*>( this )->m_pBGBase;
	else if( GetCategory() == INSTANCE_CATEGORY_NEW_BATTLE_GROUND )
		return static_cast<SunyouTeamGroupBattleGround*>( this )->m_pBGBase;
	else
		return NULL;
}

void SunyouInstance::TriggerJoin( Player* p )
{
	sEventMgr.AddEvent( p, &Player::OnJoinSunyouInstance, this, EVENT_PLAYER_SUNYOU_INSTANCE_JOIN, 1, 1, 0 );
}

void SunyouInstance::TriggerLeave( Player* p )
{
	OnPlayerLeave( p );
	//sEventMgr.AddEvent( p, &Player::OnLeaveSunyouInstance, this, EVENT_PLAYER_SUNYOU_INSTANCE_LEAVE, 1, 1, 0 );
}

bool SunyouInstance::IsValid()
{
	return s_valid_sunyouinstances.end() != s_valid_sunyouinstances.find( this );
}

bool SunyouInstance::IsFull()
{
	if( m_players.size() >= (uint32)m_conf.maxplayer )
		return true;
	else
		return false;
}

void SunyouInstance::OnPlayerJoin( Player* p )
{
	if( !p->IsValid() || !p->IsInWorld() || p->IsSunyouInstanceJoinLocked() )
		return;

	p->m_HealthBeforeEnterInstance = p->GetUInt32Value( UNIT_FIELD_HEALTH );
	p->m_ManaBeforeEnterInstance = p->GetUInt32Value( UNIT_FIELD_POWER1 );

	p->m_beforeTeleport2Instance = false;
	p->LockSunyouInstanceJoin();
	p->RemoveShapeShift();
	p->DismissMount();

	p->SetInstanceBusy( true );
	p->m_resurrectInstanceID = m_mapmgr->GetInstanceID();
	p->m_resurrectMapId = m_mapmgr->GetMapId();

	p->SetRandomResurrectLocation( m_conf.random_vrl );
	//if( !p->isAlive() )
	//	p->ResurrectPlayer();

	//p->SetUInt32Value( UNIT_FIELD_HEALTH, p->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) );
	//p->SetUInt32Value( UNIT_FIELD_POWER1, p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) );
	m_players.insert( p );
	p->m_sunyou_instance = this;

	MSG_S2C::stPlayerEnterInstance msg;
	if( m_conf.expire > 0 )
		msg.expire = m_conf.expire - ( (uint32)UNIXTIME - m_starttime );
	else
		msg.expire = 0;

	msg.mapid = m_conf.mapid;
	msg.category = m_conf.category;
	if(p->GetSession())
		p->GetSession()->SendPacket( msg );

	StorageContainerIterator<dynamic_instance_configuration>* it = dbcDynamicInstanceConfiguration.MakeIterator();
	while(!it->AtEnd())
	{
		dynamic_instance_configuration* dic = it->Get();

		if( dic->mapid == m_mapmgr->GetMapId() && dic->cls == p->getClass() )
		{
			p->AddExtraSpells( dic->spellid );
			p->AddExtraAuras( dic->aruaid );
			p->SetUInt32Value( UNIT_FIELD_HEALTH, p->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) );
			m_isdynamicproc = true;
			break;
		}

		if(!it->Inc())
			break;
	}
	it->Destruct();
}

void SunyouInstance::OnPlayerLeave( Player* p )
{
	if( !p->m_sunyou_instance )
		return;
	p->ResurrectPlayerOnSite();

	sEventMgr.AddEvent( p, &Player::LeaveSunyouInstanceProc, EVENT_PLAYER_SUNYOU_INSTANCE_LEAVE, 10, 1, 0 );

	MSG_S2C::stPlayerLeaveInstance msg;
	msg.guid = p->GetLowGUID();
	msg.name = p->GetName();
	Broadcast( msg );
	m_players.erase( p );
	p->m_sunyou_instance = NULL;

	if( m_isdynamicproc )
	{
		sEventMgr.AddEvent( p, &Player::RemoveAllExtraSpells, EVENT_SUNYOU_INSTANCE_DYNAMIC_CLEAN, 10, 1, 0 );
		sEventMgr.AddEvent( p, &Player::RemoveAllExtraAuars, EVENT_SUNYOU_INSTANCE_DYNAMIC_CLEAN, 10, 1, 0 );
	}
}

void SunyouInstance::OnInstanceStartup()
{
	if( m_conf.expire )
		sEventMgr.AddEvent( this, &SunyouInstance::Expire, EVENT_SUNYOU_INSTANCE_EXPIRE, m_conf.expire * 1000, 1, 0 );

	m_starttime = (uint32)UNIXTIME;
	m_InstanceStarted = true;

	SunyouBattleGroundBase* pBGBase = GetBGBase();
	if( pBGBase )
		pBGBase->OnBGStart();
}

void SunyouInstance::OnPlayerDie( Player* victim, Object* attacker )
{
}

void SunyouInstance::OnCreatureDie( Creature* c, Object* attacker )
{

}

void SunyouInstance::OnGameObjectTrigger( GameObject* go, Player* who )
{

}

void SunyouInstance::OnUnitEnterCombat( Unit* p )
{

}

void SunyouInstance::OnUnitLeaveCombat( Unit* p )
{

}

void SunyouInstance::OnPlayerResurrect( Player* p )
{

}

void SunyouInstance::OnCreatureResurrect( Creature* p )
{

}

void SunyouInstance::Broadcast( PakHead& msg, Player* pExcept /* = NULL */ )
{
	if(sMaster.m_stopEvent)
		return;
	for( std::set<Player*>::iterator it = m_players.begin(); it != m_players.end(); ++it )
	{
		Player* p = *it;
		if( p->IsValid() )
		{
			WorldSession* pSession = p->GetSession();
			if( p != pExcept && pSession /*&& pSession->m_loggingInPlayer*/)
				pSession->SendPacket( msg );
		}
	}
}
