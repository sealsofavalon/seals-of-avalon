#ifndef _SUNYOU_INSTANCE_HEAD
#define _SUNYOU_INSTANCE_HEAD

class SunyouBattleGroundBase;

class SunyouInstance : public EventableObject
{
	friend class SunyouBattleGroundBase;
	friend class SunyouTeamGroupBattleGroundBase;
public:
	SunyouInstance();
	virtual ~SunyouInstance();

	virtual void Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr );

	virtual void Update();
	virtual void Expire();
	virtual void OnPlayerJoin( Player* p );
	virtual void OnPlayerLeave( Player* p );
	virtual void OnInstanceStartup();
	virtual void OnPlayerDie( Player* victim, Object* attacker );
	virtual void OnCreatureDie( Creature* c, Object* attacker );
	virtual void OnGameObjectTrigger( GameObject* go, Player* who );
	virtual void OnUnitEnterCombat( Unit* p );
	virtual void OnUnitLeaveCombat( Unit* p );
	virtual void OnPlayerResurrect( Player* p );
	virtual void OnCreatureResurrect( Creature* p );
	void Broadcast( PakHead& msg, Player* pExcept = NULL );

	inline bool IsInitCreatures() const { return m_isinitcreatures; }
	inline bool IsGardenMode() const { return m_isgardenmode; }
	inline instance_category_t GetCategory() const { return (instance_category_t)m_conf.category; }
	inline const sunyou_instance_configuration* GetConfig() { return &m_conf; }
	inline bool IsBattleGround() const { return (GetCategory() == INSTANCE_CATEGORY_BATTLE_GROUND || GetCategory() == INSTANCE_CATEGORY_ADVANCED_BATTLE_GROUND || GetCategory() == INSTANCE_CATEGORY_NEW_BATTLE_GROUND); }
	inline uint32 GetPlayerCount() const { return m_players.size(); }
//	inline void IncreasePlayerCount() { ++m_player_count; }
//	inline void DecreasePlayerCount() { --m_player_count; }
	inline MapMgr* GetMapMgr() const { return m_mapmgr; }
	inline bool IsPrivate() const { return m_isprivate; }
	inline void SetPrivate( bool b ) { m_isprivate = b; }

	SunyouBattleGroundBase* GetBGBase();
	void TriggerJoin( Player* p );
	void TriggerLeave( Player* p );
	bool IsValid();
	bool IsFull();
	void DeleteThis();

protected:	
	sunyou_instance_configuration m_conf;
	MapMgr* m_mapmgr;
	Instance* m_instance;
	std::set<Player*> m_players;
	uint32 m_starttime;
	bool m_isinitcreatures;
	bool m_isgardenmode;
	bool m_isdynamicproc;
	bool m_InstanceStarted;
	int m_player_count;
	bool m_isprivate;
};

#endif