#include "StdAfx.h"
#include "SunyouMonsterGarden.h"

SunyouMonsterGarden::SunyouMonsterGarden() : m_spawnindex( 0 )
{
	m_isgardenmode = true;
}

SunyouMonsterGarden::~SunyouMonsterGarden()
{

}

void SunyouMonsterGarden::Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr )
{
	SunyouInstance::Init( conf, pMapMgr );
	pMapMgr->m_UpdateDistance = 30000.f;
}

void SunyouMonsterGarden::Expire()
{
	SunyouInstance::Expire();
}

void SunyouMonsterGarden::OnPlayerJoin( Player* p )
{
	if( !p->IsValid() || !p->IsInWorld() || p->IsSunyouInstanceJoinLocked() )
		return;

	SunyouInstance::OnPlayerJoin( p );
}

void SunyouMonsterGarden::OnPlayerLeave( Player* p )
{
	SunyouInstance::OnPlayerLeave( p );
}

void SunyouMonsterGarden::OnInstanceStartup()
{
	SunyouInstance::OnInstanceStartup();
	if( m_conf.vsi.size() )
	{
		m_spawnindex = 0;
		spawn_info* si = m_conf.vsi.front();
		sEventMgr.AddEvent( this, &SunyouMonsterGarden::SpawnCreatures, EVENT_SUNYOU_INSTANCE_SPAWN_CREATURES, si->interval * 1000, 1, 0 );
	}
}

void SunyouMonsterGarden::SpawnCreatures()
{
	if( m_spawnindex >= m_conf.vsi.size() )
		return;
	spawn_info* si = m_conf.vsi[m_spawnindex];
	m_mapmgr->SpawnCreatures( si->minlevel, si->maxlevel, false, true, m_players );

	if( ++m_spawnindex < m_conf.vsi.size() )
		sEventMgr.AddEvent( this, &SunyouMonsterGarden::SpawnCreatures, EVENT_SUNYOU_INSTANCE_SPAWN_CREATURES, m_conf.vsi[m_spawnindex]->interval * 1000, 1, 0 );
}
