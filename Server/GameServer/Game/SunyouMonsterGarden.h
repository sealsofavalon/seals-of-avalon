#ifndef _SUNYOU_MONSTER_GARDEN_HEAD
#define _SUNYOU_MONSTER_GARDEN_HEAD

#include "SunyouInstance.h"

class SunyouMonsterGarden : public SunyouInstance
{
public:
	SunyouMonsterGarden();
	virtual ~SunyouMonsterGarden();

	virtual void Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr );
	virtual void Expire();
	virtual void OnPlayerJoin( Player* p );
	virtual void OnPlayerLeave( Player* p );
	virtual void OnInstanceStartup();

	void SpawnCreatures();

protected:
	uint32 m_spawnindex;
};

#endif