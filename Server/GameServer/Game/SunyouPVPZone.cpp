#include "StdAfx.h"
#include "SunyouPVPZone.h"

SunyouPVPZone::SunyouPVPZone()
{

}

void SunyouPVPZone::Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr )
{
	SunyouInstance::Init( conf, pMapMgr );
}

void SunyouPVPZone::Expire()
{
}

void SunyouPVPZone::OnPlayerJoin( Player* p )
{
	SunyouInstance::OnPlayerJoin( p );
}

void SunyouPVPZone::OnPlayerLeave( Player* p )
{
	SunyouInstance::OnPlayerLeave( p );
	p->SetFFA( 0 );
}

void SunyouPVPZone::OnInstanceStartup()
{
	SunyouInstance::OnInstanceStartup();
}
