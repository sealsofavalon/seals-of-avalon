#ifndef _SUNYOU_PVP_ZONE_HEAD
#define _SUNYOU_PVP_ZONE_HEAD

#include "SunyouInstance.h"

class SunyouPVPZone : public SunyouInstance
{
public:
	SunyouPVPZone();

	virtual void Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr );
	virtual void Expire();
	virtual void OnPlayerJoin( Player* p );
	virtual void OnPlayerLeave( Player* p );
	virtual void OnInstanceStartup();
};

#endif