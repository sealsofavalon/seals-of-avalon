#include "StdAfx.h"
#include "SunyouRaid.h"
#include "SunyouAdvancedBattleGround.h"
#include "../../../SDBase/Protocol/S2C_Chat.h"
#include "../../../SDBase/Protocol/S2C_Move.h"

SunyouRaid::SunyouRaid() : LUA_COUNT( 0 ), m_lua_units( NULL )
{
}

SunyouRaid::~SunyouRaid()
{
	if( m_lua_units )
		delete[] m_lua_units;
}

void SunyouRaid::Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr )
{
	SunyouInstance::Init( conf, pMapMgr );

	LUA_COUNT = conf.lua_scripts.size();
	if( LUA_COUNT == 0 )
	{
		MyLog::log->error( "SunyouRaid::Init LUA_COUNT == 0!!!" );
		exit( 0 );
		return;
	}

	m_lua_units = new LuaUnit[LUA_COUNT];
	int i = 0;
	for( std::map<uint32, std::string>::const_iterator it = conf.lua_scripts.begin(); it != conf.lua_scripts.end(); ++it )
	{
		m_lua_units[i++].Init( this, it->second.c_str(), it->first );
	}

	this->OnInstanceStartup();
}

void SunyouRaid::Expire()
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].Release();

	SunyouInstance::Expire();
}

void SunyouRaid::OnPlayerJoin( Player* p )
{
	SunyouInstance::OnPlayerJoin( p );

	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnPlayerJoin( p );
}

void SunyouRaid::OnPlayerLeave( Player* p )
{
	SunyouInstance::OnPlayerLeave( p );

	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnPlayerLeave( p );
}

void SunyouRaid::OnInstanceStartup()
{
	SunyouInstance::OnInstanceStartup();

	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnInstanceStartup();
}

void SunyouRaid::Update()
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].Update();
}

void SunyouRaid::SafeTeleportPlayer( Player* p, LocationVector v )
{
	if( m_players.find( p ) != m_players.end() )
	{
		p->SafeTeleport( m_mapmgr->GetMapId(), m_mapmgr->GetInstanceID(), v );
	}
}

/////////////////////////////////////////////////////////////////////////////

void SunyouRaid::OnPlayerDie( Player* p, Object* attacker )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnPlayerDie( p, attacker );
}

void SunyouRaid::OnCreatureDie( Creature* c, Object* attacker )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnCreatureDie( c, attacker );
}

void SunyouRaid::OnGameObjectTrigger( GameObject* go, Player* who )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnGameObjectTrigger( go, who );
}

void SunyouRaid::OnUnitEnterCombat( Unit* p )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnUnitEnterCombat( p );
}

void SunyouRaid::OnUnitLeaveCombat( Unit* p )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnUnitLeaveCombat( p );
}

void SunyouRaid::OnPlayerResurrect( Player* p )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnPlayerResurrect( p );
}

void SunyouRaid::OnCreatureResurrect( Creature* p )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnCreatureResurrect( p );
}

void SunyouRaid::OnNPCGossipTrigger( Unit* npc, int gossip_index, Player* who )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnNPCGossipTrigger( npc, gossip_index, who );
}

void SunyouRaid::OnRaidWipe()
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnRaidWipe();
}

void SunyouRaid::OnUnitCastSpell( int spell_id, Unit* who )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnUnitCastSpell( spell_id, who );
}

void SunyouRaid::OnUnitBeginCastSpell( int spell_id, Unit* who )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnUnitBeginCastSpell( spell_id, who );
}

void SunyouRaid::OnCreatureMoveArrival( Creature* p )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnCreatureMoveArrival( p );
}

void SunyouRaid::OnGameObjectDespawn( GameObject* p )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnGameObjectDespawn( p );
}

void SunyouRaid::OnPlayerLootItem( Player* p, uint32 entry, uint32 amount )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnPlayerLootItem( p, entry, amount );
}

void SunyouRaid::OnUnitHitWithSpell( int spell_id, Unit* attacker, Unit* victim )
{	
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnUnitHitWithSpell( spell_id, attacker, victim );
}

void SunyouRaid::OnDynamicObjectCreated( Object* caster, DynamicObject* dynobj )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnDynamicObjectCreated( caster, dynobj );
}

void SunyouRaid::OnDynamicObjectMoveArrival( DynamicObject* p )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnDynamicObjectMoveArrival( p );
}

void SunyouRaid::OnDynamicObjectDestroyed( DynamicObject* p )
{
	for( int i = 0; i < LUA_COUNT; ++i )
		m_lua_units[i].OnDynamicObjectDestroyed( p );
}

