#ifndef _SUNYOU_RAID_HEAD
#define _SUNYOU_RAID_HEAD

#include "SunyouInstance.h"
#include "LuaUnit.h"

class SunyouRaid : public SunyouInstance
{
public:
	friend class LuaUnit;

	SunyouRaid();
	virtual ~SunyouRaid();

	virtual void Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr );
	virtual void Expire();
	virtual void OnPlayerJoin( Player* p );
	virtual void OnPlayerLeave( Player* p );
	virtual void OnInstanceStartup();
	virtual void Update();

	virtual void OnPlayerDie( Player* p, Object* attacker );
	virtual void OnCreatureDie( Creature* c, Object* attacker );
	virtual void OnGameObjectTrigger( GameObject* go, Player* who );
	virtual void OnUnitEnterCombat( Unit* p );
	virtual void OnUnitLeaveCombat( Unit* p );
	virtual void OnPlayerResurrect( Player* p );
	virtual void OnCreatureResurrect( Creature* p );

	void OnNPCGossipTrigger( Unit* npc, int gossip_index, Player* who );
	void OnRaidWipe();
	void OnUnitCastSpell( int spell_id, Unit* who );
	void OnUnitBeginCastSpell( int spell_id, Unit* who );
	void OnCreatureMoveArrival( Creature* p );
	void OnGameObjectDespawn( GameObject* p );
	void OnPlayerLootItem( Player* p, uint32 entry, uint32 amount );
	void OnUnitHitWithSpell( int spell_id, Unit* attacker, Unit* victim );
	void OnDynamicObjectCreated( Object* caster, DynamicObject* dynobj );
	void OnDynamicObjectMoveArrival( DynamicObject* p );
	void OnDynamicObjectDestroyed( DynamicObject* p );

	//////////////////////////////////////////////////////////////////////////
	void SafeTeleportPlayer( Player* p, LocationVector v );

protected:
	uint32 LUA_COUNT;
	LuaUnit* m_lua_units;
};

#endif

