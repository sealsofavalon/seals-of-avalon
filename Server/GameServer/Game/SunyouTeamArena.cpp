#include "StdAfx.h"
#include "SunyouTeamArena.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "MultiLanguageStringMgr.h"

std::map<uint32, SunyouTeamArena::arena_configuration> SunyouTeamArena::s_confs;

SunyouTeamArena::SunyouTeamArena() : m_end( false ), m_ac( NULL ), m_started( false )
{
	for(uint32 i = 0; i < 2; ++i)
	{
		m_groups[i] = new Group(true);
		m_groups[i]->m_disbandOnNoMembers = false;
	}
}

SunyouTeamArena::~SunyouTeamArena()
{
	for(uint32 i = 0; i < 2; ++i)
	{
		PlayerInfo *inf;
		for(uint32 j = 0; j < m_groups[i]->GetSubGroupCount(); ++j) {
			for(GroupMembersSet::iterator itr = m_groups[i]->GetSubGroup(j)->GetGroupMembersBegin(); itr != m_groups[i]->GetSubGroup(j)->GetGroupMembersEnd();) {
				inf = (*itr);
				++itr;
				m_groups[i]->RemovePlayer(inf);
			}
		}
		delete m_groups[i];
	}
}

void SunyouTeamArena::LoadConfigurations()
{
	QueryResult* qr = WorldDatabase.Query( "select * from sunyou_team_arena" );
	if( qr )
	{
		do
		{
			Field* f = qr->Fetch();
			arena_configuration ac;
			ac.init_point[0] = LocationVector( f[1].GetFloat(), f[2].GetFloat(), f[3].GetFloat(), f[4].GetFloat() );
			ac.init_point[1] = LocationVector( f[5].GetFloat(), f[6].GetFloat(), f[7].GetFloat(), f[8].GetFloat() );

			ac.win_reward_honor = f[9].GetUInt32();

			ac.lose_reward_honor = f[10].GetUInt32();

			ac.win_reward_exp = f[11].GetUInt32();

			ac.lose_reward_exp = f[12].GetUInt32();

			ac.win_reward_yuanbao = f[13].GetUInt32();

			int idx = 14;
			for( int i = 0; i < 5; ++i )
			{
				ac.win_reward_item[i] = f[idx++].GetUInt32();
				ac.win_reward_item_count[i] = f[idx++].GetUInt32();

				ac.lose_reward_item[i] = f[idx++].GetUInt32();
				ac.lose_reward_item_count[i] = f[idx++].GetUInt32();
			}
			
			s_confs[f[0].GetUInt32()] = ac;
		}
		while( qr->NextRow() );

		delete qr;
	}
}

void SunyouTeamArena::Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr )
{
	SunyouInstance::Init( conf, pMapMgr );
	std::map<uint32, arena_configuration>::iterator it = s_confs.find( conf.mapid );
	if( it != s_confs.end() )
	{
		m_ac = &it->second;
	}
	else
		m_ac = NULL;
}

void SunyouTeamArena::Expire()
{
	m_end = true;
	SunyouInstance::Expire();
}

void SunyouTeamArena::Teleport2Initpoints( Player* p )
{
	if( p->IsValid() && p->GetMapMgr() == m_mapmgr )
		p->SafeTeleport( m_mapmgr->GetMapId(), m_mapmgr->GetInstanceID(), m_ac->init_point[p->m_team_arena_idx - 1] );
}

void SunyouTeamArena::OnPlayerJoin( Player* p )
{
	if( !p->IsValid() || !p->IsInWorld() || p->IsSunyouInstanceJoinLocked() )
		return;

	if( p->m_team_arena_idx != 1 && p->m_team_arena_idx != 2 )
		return;

	if( p->GetGroup() )
		p->GetGroup()->RemovePlayer( p->m_playerInfo );

	if( m_vs_type > 0 )
	{
		if( p->GetGroup() == NULL )
		{
			m_groups[p->m_team_arena_idx - 1]->AddMember( p->m_playerInfo );
		}
	}

	p->ResetAllCooldowns();
	p->m_arena_leave_reason_win = false;
	p->ClearRandomResurrectLocation();

	std::vector<LocationVector*> v;
	v.push_back( &m_ac->init_point[p->m_team_arena_idx - 1] );
	p->SetRandomResurrectLocation( v );
	if( !p->isAlive() )
		p->ResurrectPlayerOnSite();

	p->SetUInt32Value( UNIT_FIELD_HEALTH, p->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) );
	p->SetUInt32Value( UNIT_FIELD_POWER1, p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) );

	SunyouInstance::OnPlayerJoin( p );
	p->RemoveAllAuras( false );
	sEventMgr.AddEvent( this, &SunyouTeamArena::Teleport2Initpoints, p, EVENT_SUNYOU_TEAM_ARENA_TELEPORT, 10, 1, 0 );

	p->m_resurrect_lock = true;
	p->SetFFA( 20 + p->m_team_arena_idx );
	p->Root( true );
	p->SetUInt32Value( UNIT_FIELD_HEALTH, p->GetUInt32Value( UNIT_FIELD_MAXHEALTH ) );
	p->SetUInt32Value( UNIT_FIELD_POWER1, p->GetUInt32Value( UNIT_FIELD_MAXPOWER1 ) );
	m_player_names[p->m_team_arena_idx - 1] += "[";
	m_player_names[p->m_team_arena_idx - 1] += p->GetName();
	m_player_names[p->m_team_arena_idx - 1] += "]";



	for( int i = 0; i < 5; ++i )
	{
		if( !m_teams[p->m_team_arena_idx - 1].players[i] )
		{
			m_teams[p->m_team_arena_idx - 1].players[i] = p;
			m_teams[p->m_team_arena_idx - 1].power++;
			break;
		}
	}
}

void SunyouTeamArena::OnPlayerLeave( Player* p )
{
	if( !p->m_sunyou_instance )
		return;

	if( p->GetGroup() == m_groups[p->m_team_arena_idx - 1] )
		p->GetGroup()->RemovePlayer( p->m_playerInfo );

	p->m_escape_mode = false;
	p->m_resurrect_lock = false;
	p->RemoveAllAuras( false );
	p->Unroot( true );

	SunyouInstance::OnPlayerLeave( p );
	if( !m_end )
	{
		int loseteam = p->m_team_arena_idx - 1;
		if( loseteam > 1 || loseteam < 0 )
		{
			assert( 0 );
			return;
		}
		for( int i = 0; i < 5; ++i )
		{
			if( m_teams[loseteam].players[i] == p )
			{
				m_teams[loseteam].players[i] = NULL;
				--m_teams[loseteam].power;
			}
		}

		if( !m_end && m_started )
		{
			if( m_teams[loseteam].power <= 0 )
			{
				if( m_teams[!loseteam].power >= 0 )
				{
					End( !loseteam );
				}
			}
		}
	}
	p->AddArenaResult( m_conf.maxlevel, p->m_arena_leave_reason_win, m_vs_type );
	p->CheckArenaTitles();
}

void SunyouTeamArena::OnInstanceStartup()
{
	SunyouInstance::OnInstanceStartup();
	//sEventMgr.AddEvent( this, &SunyouTeamArena::OnReady2Fight, EVENT_SUNYOU_INSTANCE_ARENA_READY, 20 * 1000, 1, 0 );
	sEventMgr.AddEvent( this, &SunyouTeamArena::OnStartFight, EVENT_SUNYOU_INSTANCE_ARENA_READY, 30 * 1000, 1, 0 );
}

void SunyouTeamArena::OnReady2Fight()
{
	/*
	for( std::set<Player*>::iterator it = m_players.begin(); it != m_players.end(); ++it )
	{
		Player* p = *it;
	}
	*/
}

void SunyouTeamArena::OnStartFight()
{
	m_started = true;
	for( std::set<Player*>::iterator it = m_players.begin(); it != m_players.end(); ++it )
	{
		Player* p = *it;
		p->Unroot( true );
		p->GetSession()->SendNotification( build_language_string( BuildString_ArenaStartFight ) );

		int team = p->m_team_arena_idx - 1;
		if( m_teams[!team].power <= 0 )
		{
			for( std::set<Player*>::iterator it2 = m_players.begin(); it2 != m_players.end(); ++it2 )
			{
				Player* p = *it2;
				p->GetSession()->SendNotification( build_language_string( BuildString_ArenaEndWithoutEnemy ) );
				if( m_ac->win_reward_yuanbao )
				{
					if( m_conf.cost_type == 1 && m_conf.cost_value1 )
					{
						p->AddCardPoints( m_conf.cost_value1, Player::ADD_CARD_NONE );
						MyLog::yunyinglog->info("yuanbao-arena-player[%u][%s] empty return [%u]", p->GetLowGUID(), p->GetName(), m_conf.cost_value1);
					}
				}
			}

			sEventMgr.AddEvent( this, &SunyouTeamArena::Expire, EVENT_SUNYOU_INSTANCE_EXPIRE, 20 * 1000, 1, 0 );
			m_end = true;
			return;
		}
	}
}

void SunyouTeamArena::OnPlayerDie( Player* victim, Object* attacker )
{
	if( m_end )
		return;

	int loseteam = victim->m_team_arena_idx - 1;
	if( loseteam > 1 || loseteam < 0 )
	{
		assert( 0 );
		return;
	}

	if( --m_teams[loseteam].power <= 0 )
	{
		if( m_teams[!loseteam].power >= 0 )
		{
			End( !loseteam );
		}
	}
}

void SunyouTeamArena::OnTeamWin( int idx )
{
	if( !m_ac )
		return;

	for( int i = 0; i < 5; ++i )
	{
		Player* p = m_teams[idx].players[i];
		if( p )
		{
			p->GetSession()->SendNotification( build_language_string( BuildString_ArenaWinnerNotification ) );

			if( m_ac->win_reward_honor )
			{
				p->ModTotalHonor( m_ac->win_reward_honor );
				p->ModHonorCurrency( m_ac->win_reward_honor );
			}

			if( m_ac->win_reward_exp )
				p->GiveXP( m_ac->win_reward_exp, 0, true, GIVE_EXP_TYPE_DYNAMIC_INSTANCE );

			if( m_ac->win_reward_item[m_vs_type] )
				p->GiveGift( 1, m_ac->win_reward_item[m_vs_type], m_ac->win_reward_item_count[m_vs_type], true, Player::GIFT_CATEGORY_TEAM_ARENA );

			if( m_ac->win_reward_yuanbao )
			{
				p->AddCardPoints( m_ac->win_reward_yuanbao );
				MyLog::yunyinglog->info("yuanbao-arena-player[%u][%s] win reward [%u]", p->GetLowGUID(), p->GetName(), m_ac->win_reward_yuanbao);
			}

			p->m_arena_leave_reason_win = true;
		}
	}
}

void SunyouTeamArena::OnTeamLose( int idx )
{
	if( !m_ac )
		return;

	for( int i = 0; i < 5; ++i )
	{
		if( m_teams[idx].players[i] )
		{
			Player* p = m_teams[idx].players[i];
			p->GetSession()->SendNotification( build_language_string( BuildString_ArenaLoserNotification ) );
			
			if( m_ac->lose_reward_honor )
			{
				p->ModTotalHonor( m_ac->lose_reward_honor );
				p->ModHonorCurrency( m_ac->lose_reward_honor );
			}

			if( m_ac->lose_reward_exp )
				p->GiveXP( m_ac->lose_reward_exp, 0, true, GIVE_EXP_TYPE_DYNAMIC_INSTANCE );

			if( m_ac->lose_reward_item[m_vs_type] )
				p->GiveGift( 1, m_ac->lose_reward_item[m_vs_type], m_ac->lose_reward_item_count[m_vs_type], true, Player::GIFT_CATEGORY_TEAM_ARENA );
		}
	}
}

void SunyouTeamArena::End( int win_idx )
{
	OnTeamWin( win_idx );
	OnTeamLose( !win_idx );
	
	MSG_S2C::stNotify_Msg msg;
	msg.notify = build_language_string( BuildString_ArenaEndNotification, m_player_names[win_idx].c_str(), m_player_names[!win_idx].c_str() );
	Broadcast( msg );

	sEventMgr.AddEvent( this, &SunyouTeamArena::Expire, EVENT_SUNYOU_INSTANCE_EXPIRE, 30 * 1000, 1, 0 );
	m_end = true;
}
