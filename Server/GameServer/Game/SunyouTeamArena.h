#ifndef _SUNYOU_TEAM_ARENA_HEAD
#define _SUNYOU_TEAM_ARENA_HEAD

#include "SunyouInstance.h"

class SunyouTeamArena : public SunyouInstance
{
public:
	SunyouTeamArena();
	~SunyouTeamArena();

	virtual void Init( const sunyou_instance_configuration& conf, MapMgr* pMapMgr );
	virtual void Expire();
	virtual void OnPlayerJoin( Player* p );
	virtual void OnPlayerLeave( Player* p );
	virtual void OnInstanceStartup();
	virtual void OnPlayerDie( Player* victim, Object* attacker );

	void End( int win_idx );
	void OnTeamWin( int idx );
	void OnTeamLose( int idx );
	void OnStartFight();
	void OnReady2Fight();
	void Teleport2Initpoints( Player* p );

	struct arena_configuration
	{
		uint32 win_reward_item[5];
		uint32 lose_reward_item[5];

		uint32 win_reward_item_count[5];
		uint32 lose_reward_item_count[5];

		uint32 win_reward_honor;
		uint32 lose_reward_honor;

		uint32 win_reward_exp;
		uint32 lose_reward_exp;

		uint32 win_reward_yuanbao;

		LocationVector init_point[2];
	};

	static void LoadConfigurations();
	static std::map<uint32, arena_configuration> s_confs;

	uint32 m_vs_type;
	arena_configuration* m_ac;

protected:
	struct team_t
	{
		Player* players[5];
		int8 power;
		team_t()
		{
			power = 0;
			for( int i = 0; i < 5; ++i )
				players[i] = NULL;
		}
	};
	team_t m_teams[2];
	bool m_end;
	bool m_started;
	std::string m_player_names[2];
	Group* m_groups[2];
};

#endif
