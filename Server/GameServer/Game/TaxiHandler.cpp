#include "StdAfx.h"
#include "TaxiMgr.h"
#include "../../../SDBase/Protocol/S2C_Move.h"

void WorldSession::HandleTaxiNodeStatusQueryOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MyLog::log->debug( "WORLD: Received CMSG_TAXINODE_STATUS_QUERY" );

	uint64 guid;
	uint32 curloc;
	uint8 field;
	uint32 submask;

	//recv_data >> guid;

	curloc = sTaxiMgr.GetNearestTaxiNode( GetPlayer( )->GetPositionX( ), GetPlayer( )->GetPositionY( ), GetPlayer( )->GetPositionZ( ), GetPlayer( )->GetMapId( ) );

	field = (uint8)((curloc - 1) / 32);
	submask = 1<<((curloc-1)%32);

	MSG_S2C::stTaxi_Node_Stat Msg;
	Msg.guid = guid;

	// Check for known nodes
	if ( (GetPlayer( )->GetTaximask(field) & submask) != submask )
	{   
		Msg.stat = uint8( 0 );	
	}
	else
	{
		Msg.stat = uint8( 1 );
	}	

	SendPacket( Msg );
	MyLog::log->debug( "WORLD: Sent SMSG_TAXINODE_STATUS" );
}


void WorldSession::HandleTaxiQueryAvaibleNodesOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MyLog::log->debug( "WORLD: Received CMSG_TAXIQUERYAVAILABLENODES" );
	uint64 guid;
	//recv_data >> guid;
	Creature *pCreature = _player->GetMapMgr()->GetCreature(GET_LOWGUID_PART(guid));
	if(!pCreature) return;

	SendTaxiList(pCreature);
}

void WorldSession::SendTaxiList(Creature* pCreature)
{
	uint32 curloc;
	uint8 field;
	uint32 TaxiMask[8];
	uint32 submask;
	uint64 guid = pCreature->GetGUID();

	curloc = pCreature->m_TaxiNode;
	if (!curloc)
		return;

	field = (uint8)((curloc - 1) / 32);
	submask = 1<<((curloc-1)%32);

	// Check for known nodes
	if (!(GetPlayer( )->GetTaximask(field) & submask))
	{
		GetPlayer()->SetTaximask(field, (submask | GetPlayer( )->GetTaximask(field)) );

		MSG_S2C::stTaxi_New_Path MsgNewPath;
		SendPacket( MsgNewPath );

		//Send packet
		MSG_S2C::stTaxi_Node_Stat Msg;
		Msg.guid = guid;
		Msg.stat = 1;
		SendPacket( Msg );
	}

	//Set Mask
	memset(TaxiMask, 0, sizeof(uint32)*8);
	sTaxiMgr.GetGlobalTaxiNodeMask(curloc, TaxiMask);
	TaxiMask[field] |= 1 << ((curloc-1)%32);

	//Remove nodes unknown to player
	for(int i = 0; i < 8; i++)
	{
		TaxiMask[i] &= GetPlayer( )->GetTaximask(i);
	}

	MSG_S2C::stTaxi_Show_Nodes Msg;
	Msg.guid = guid;
	Msg.curloc = curloc;
	for(int i = 0; i < 8; i++)
	{
		Msg.vNodes.push_back( TaxiMask[i] );
	}
	SendPacket( Msg );

	MyLog::log->debug( "WORLD: Sent SMSG_SHOWTAXINODES" );
}

void WorldSession::HandleActivateTaxiOpcode( CPacketUsn& packet )
{
	if(!_player->IsInWorld()) return;
	MyLog::log->debug( "WORLD: Received CMSG_ACTIVATETAXI" );

	uint64 guid;
	uint32 sourcenode, destinationnode;
	int32 newmoney;
	uint32 curloc;
	uint8 field;
	uint32 submask;

	//recv_data >> guid >> sourcenode >> destinationnode;

	MSG_S2C::stTaxi_Activate_Reply Msg;
	if(GetPlayer()->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER))
		return;

	TaxiPath* taxipath = sTaxiMgr.GetTaxiPath(sourcenode, destinationnode);
	TaxiNode* taxinode = sTaxiMgr.GetTaxiNode(sourcenode);

	if( !taxinode || !taxipath )
		return;

	curloc = taxinode->id;
	field = (uint8)((curloc - 1) / 32);
	submask = 1<<((curloc-1)%32);

	// Check for known nodes
	if ( (GetPlayer( )->GetTaximask(field) & submask) != submask )
	{   
		Msg.stat = uint32( 1 );
		SendPacket(Msg);
		return;
	}

	// Check for valid node
	if (!taxinode)
	{
		Msg.stat = uint32( 1 );
		SendPacket(Msg);
		return;
	}

	if (!taxipath || !taxipath->GetNodeCount())
	{
		Msg.stat = uint32( 2 );
		SendPacket(Msg);
		return;
	}

	// Check for gold
	newmoney = ((GetPlayer()->GetUInt32Value(PLAYER_FIELD_COINAGE)) - taxipath->GetPrice());
	if(newmoney < 0 )
	{
		Msg.stat = uint32( 3 );
		SendPacket(Msg);
		return;
	}

	// MOUNTDISPLAYID
	// bat: 1566
	// gryph: 1147
	// wyvern: 295
	// hippogryph: 479

	uint32 modelid =0;
	if( _player->GetTeam() )
	{
		if( taxinode->horde_mount == 2224 )
			modelid =295; // In case it's a wyvern
		else
			modelid =1566; // In case it's a bat or a bad id
	}
	else
	{
		if( taxinode->alliance_mount == 3837 )
			modelid =479; // In case it's an hippogryph
		else
			modelid =1147; // In case it's a gryphon or a bad id
	}

	//GetPlayer( )->setDismountCost( newmoney );

	Msg.stat = uint32( 0 );
	SendPacket(Msg);
	MyLog::log->debug( "WORLD: Sent SMSG_ACTIVATETAXIREPLY" );

	// 0x001000 seems to make a mount visible
	// 0x002000 seems to make you sit on the mount, and the mount move with you
	// 0x000004 locks you so you can't move, no msg_move updates are sent to the server
	// 0x000008 seems to enable detailed collision checking

	// check for a summon -> if we do, remove.
	if(_player->GetSummon() != NULL)
	{
		if(_player->GetSummon()->GetUInt32Value(UNIT_CREATED_BY_SPELL) > 0)
			_player->GetSummon()->Dismiss(false);						   // warlock summon -> dismiss
		else
			_player->GetSummon()->Remove(false, true, true);					  // hunter pet -> just remove for later re-call
	}

	_player->taxi_model_id = modelid;
	GetPlayer()->TaxiStart(taxipath, modelid, 0);
	
	//MyLog::log->info("TAXI: Starting taxi trip. Next update in %d msec.", first_node_time);
}

void WorldSession::HandleMultipleActivateTaxiOpcode(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MyLog::log->debug( "WORLD: Received CMSG_ACTIVATETAXI" );

	uint64 guid;
	uint32 moocost;
	uint32 nodecount;
	vector<uint32> pathes;
	int32 newmoney;
	uint32 curloc;
	uint8 field;
	uint32 submask;
	MSG_S2C::stTaxi_Activate_Reply Msg;

	//recvPacket >> guid >> moocost >> nodecount;
	if(nodecount < 2)
		return;

	if(nodecount>10)
	{
		Disconnect();
		return;
	}

// 	for(uint32 i = 0; i < nodecount; ++i)
// 		pathes.push_back( recvPacket.read<uint32>() );

	if(GetPlayer()->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER))
		return;

	// get first trip
	TaxiPath* taxipath = sTaxiMgr.GetTaxiPath(pathes[0], pathes[1]);
	TaxiNode* taxinode = sTaxiMgr.GetTaxiNode(pathes[0]);

	curloc = taxinode->id;
	field = (uint8)((curloc - 1) / 32);
	submask = 1<<((curloc-1)%32);

	// Check for known nodes
	if ( (GetPlayer( )->GetTaximask(field) & submask) != submask )
	{   
		Msg.stat = uint32( 1 );
		SendPacket( Msg );
		return;
	}

	// Check for valid node
	if (!taxinode)
	{
		Msg.stat = uint32( 1 );
		SendPacket( Msg );
		return;
	}

	if (!taxipath || !taxipath->GetNodeCount())
	{
		Msg.stat = uint32( 2 );
		SendPacket( Msg );
		return;
	}

	if (taxipath->GetID() == 766 || taxipath->GetID() == 767 || taxipath->GetID() == 771 || taxipath->GetID() == 772)
	{
		Msg.stat = uint32( 2 );
		SendPacket( Msg );
		return;
	}

	uint32 totalcost = taxipath->GetPrice();
	for(uint32 i = 2; i < nodecount; ++i)
	{
		TaxiPath * np = sTaxiMgr.GetTaxiPath(pathes[i-1], pathes[i]);
		if(!np) return;
		totalcost += np->GetPrice();
	}

	// Check for gold
	newmoney = ((GetPlayer()->GetUInt32Value(PLAYER_FIELD_COINAGE)) - totalcost);
	if(newmoney < 0 )
	{
		Msg.stat = uint32( 3 );
		SendPacket( Msg );
		return;
	}

	// MOUNTDISPLAYID
	// bat: 1566
	// gryph: 1147
	// wyvern: 295
	// hippogryph: 479

	uint32 modelid =0;
	if( _player->GetTeam() )
	{
		if( taxinode->horde_mount == 2224 )
			modelid =295; // In case it's a wyvern
		else
			modelid =1566; // In case it's a bat or a bad id
	}
	else
	{
		if( taxinode->alliance_mount == 3837 )
			modelid =479; // In case it's an hippogryph
		else
			modelid =1147; // In case it's a gryphon or a bad id
	}

	//GetPlayer( )->setDismountCost( newmoney );

	Msg.stat = uint32( 0 );
	// 0 Ok
	// 1 Unspecified Server Taxi Error
	// 2.There is no direct path to that direction
	// 3 Not enough Money
	SendPacket( Msg );
	MyLog::log->debug( "WORLD: Sent SMSG_ACTIVATETAXIREPLY" );

	// 0x001000 seems to make a mount visible
	// 0x002000 seems to make you sit on the mount, and the mount move with you
	// 0x000004 locks you so you can't move, no msg_move updates are sent to the server
	// 0x000008 seems to enable detailed collision checking

	// check for a summon -> if we do, remove.
	if(_player->GetSummon() != NULL)
	{
		if(_player->GetSummon()->GetUInt32Value(UNIT_CREATED_BY_SPELL) > 0)
			_player->GetSummon()->Dismiss(false);						   // warlock summon -> dismiss
		else
			_player->GetSummon()->Remove(false, true, true);					  // hunter pet -> just remove for later re-call
	}

	_player->taxi_model_id = modelid;
	
	// build the rest of the path list
	for(uint32 i = 2; i < nodecount; ++i)
	{
		TaxiPath * np = sTaxiMgr.GetTaxiPath(pathes[i-1], pathes[i]);
		if(!np) return;

/*		if (np->GetID() == 766 || np->GetID() == 767 || np->GetID() == 771 || np->GetID() == 772)
		{
			_player->m_taxiPaths.clear();
			return;
		}
*/
		// add to the list.. :)
		_player->m_taxiPaths.push_back(np);
	}

	// start the first trip :)
	GetPlayer()->TaxiStart(taxipath, modelid, 0);
}
