#include "StdAfx.h"
#include "../../SDBase/Protocol/C2S_Trade.h"
#include "../../SDBase/Protocol/S2C_Trade.h"

void WorldSession::HandleInitiateTrade(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	MSG_C2S::stTradeInit MsgRecv;packet >> MsgRecv;
	MSG_S2C::stTradeStat Msg;
	uint32 TradeStatus = TRADE_STATUS_PROPOSED;

	if( GetGroupIDByGUID(MsgRecv.target_guid) != GetGroupIDByGUID(_player->GetLowGUID()) )
	{
		TradeStatus = TRADE_STATUS_DIFF_SRV;
		Msg.nStat	= TradeStatus;
		SendPacket( Msg );
		return;
	}

	uint64 guid = MsgRecv.target_guid;
	Player * pTarget = _player->GetMapMgr()->GetPlayer((uint32)guid);

	if(pTarget == 0)
	{
		TradeStatus = TRADE_STATUS_PLAYER_NOT_FOUND;
		Msg.nStat	= TradeStatus;
		SendPacket( Msg );
		return;
	}

	if(!isFriendly(_player, pTarget))
	{
		return;
	}
	
	if( pTarget->IsForbidTrade() )
	{
		pTarget->SendForbidTradeMsg( this );
		return;
	}

	if(_player->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT))
	{
		TradeStatus = TRADE_STATUS_FAILED;
		Msg.nStat	= TradeStatus;
		SendPacket( Msg );
		_player->ResetTradeVariables();
		pTarget->ResetTradeVariables();
		return;
	}

	if(pTarget->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT))
	{
		TradeStatus = TRADE_STATUS_FAILED;
		Msg.nStat	= TradeStatus;
		pTarget->GetSession()->SendPacket( Msg );
		_player->ResetTradeVariables();
		pTarget->ResetTradeVariables();
		return;
	}

	// Handle possible error outcomes
	if(pTarget->CalcDistance(_player) > 10.0f)		// This needs to be checked
		TradeStatus = TRADE_STATUS_TOO_FAR_AWAY;
	else if(pTarget->isDead())
		TradeStatus = TRADE_STATUS_DEAD;
	else if(pTarget->mTradeTarget != 0)
		TradeStatus = TRADE_STATUS_ALREADY_TRADING;
	/*
	else if(pTarget->GetTeam() != _player->GetTeam() && GetPermissionCount() == 0)
		TradeStatus = TRADE_STATUS_WRONG_FACTION;
		*/

	Msg.nStat	= TradeStatus;

	if(TradeStatus == TRADE_STATUS_PROPOSED)
	{
		_player->ResetTradeVariables();
		pTarget->ResetTradeVariables();

		pTarget->mTradeTarget = _player->GetLowGUID();
		_player->mTradeTarget = pTarget->GetLowGUID();

		pTarget->mTradeStatus = TradeStatus;
		_player->mTradeStatus = TradeStatus;

		Msg.guid = _player->GetGUID();
		pTarget->m_session->SendPacket(Msg);
	}
	else
		SendPacket( Msg );
	
}

void WorldSession::HandleBeginTrade(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	uint32 TradeStatus = TRADE_STATUS_INITIATED;

	MSG_S2C::stTradeStat Msg;
	Player * plr = _player->GetTradeTarget();
	if(_player->mTradeTarget == 0 || plr == 0)
	{
		TradeStatus = TRADE_STATUS_PLAYER_NOT_FOUND;
		Msg.nStat	= TradeStatus;
		SendPacket( Msg );
		_player->ResetTradeVariables();
		return;
	}
	// We're too far from target now?
	if( _player->CalcDistance( objmgr.GetPlayer(_player->mTradeTarget) ) > 10.0f )
	{
		TradeStatus = TRADE_STATUS_TOO_FAR_AWAY;
		plr->mTradeStatus = TradeStatus;
		_player->mTradeStatus = TradeStatus;

		plr->mTradeTarget = 0;
		_player->mTradeTarget = 0;
	}

	Msg.nStat	= TradeStatus;
	Msg.guid	= _player->GetGUID();
	plr->m_session->SendPacket(Msg);
	Msg.guid	= plr->GetGUID();
	SendPacket(Msg);

	plr->mTradeStatus = TradeStatus;
	_player->mTradeStatus = TradeStatus;
}

void WorldSession::HandleBusyTrade(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	uint32 TradeStatus = TRADE_STATUS_PLAYER_BUSY;

	MSG_C2S::stTrade_Busy MsgRecv;packet >> MsgRecv;
	Player * plr = _player->GetTradeTarget();
	MSG_S2C::stTradeStat Msg;
	if(_player->mTradeTarget == 0 || plr == 0)
	{
		TradeStatus = TRADE_STATUS_PLAYER_NOT_FOUND;
		Msg.nStat = TradeStatus;
		SendPacket( Msg );
		return;
	}

	Msg.nStat	= TradeStatus;
	plr->m_session->SendPacket( Msg );

	plr->mTradeStatus = TradeStatus;
	_player->mTradeStatus = TradeStatus;

	plr->mTradeTarget = 0;
	_player->mTradeTarget = 0;
}

void WorldSession::HandleIgnoreTrade(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	uint32 TradeStatus = TRADE_STATUS_PLAYER_IGNORED;

	Player * plr = _player->GetTradeTarget();
	MSG_S2C::stTradeStat Msg;
	if(_player->mTradeTarget == 0 || plr == 0)
	{
		TradeStatus = TRADE_STATUS_PLAYER_NOT_FOUND;
		Msg.nStat	= TradeStatus;
		SendPacket( Msg );
		return;
	}

	Msg.nStat	= TradeStatus;
	plr->m_session->SendPacket( Msg );

	plr->mTradeStatus = TradeStatus;
	_player->mTradeStatus = TradeStatus;

	plr->mTradeTarget = 0;
	_player->mTradeTarget = 0;
}

void WorldSession::HandleCancelTrade(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	if(_player->mTradeTarget == 0 || _player->mTradeStatus == TRADE_STATUS_COMPLETE)
		return;

	MSG_S2C::stTradeStat Msg;
    uint32 TradeStatus = TRADE_STATUS_CANCELLED;

	Msg.nStat	= TradeStatus;
    SendPacket( Msg );

	Player * plr = _player->GetTradeTarget();
    if(plr)
    {
        if(plr->m_session && plr->m_session->GetSocket())
			plr->m_session->SendPacket( Msg );
	
	    plr->mTradeTarget = 0;
		plr->ResetTradeVariables();
    }
	
	_player->mTradeTarget = 0;
	_player->ResetTradeVariables();
}

void WorldSession::HandleUnacceptTrade(CPacketUsn& packet)
{
	if(!_player->IsInWorld()) return;
	Player * plr = _player->GetTradeTarget();
	_player->ResetTradeVariables();

	if(_player->mTradeTarget == 0 || plr == 0)
		return;

	MSG_S2C::stTradeStat Msg;
	uint32 TradeStatus = TRADE_STATUS_UNACCEPTED;

	Msg.nStat	= TradeStatus;
	SendPacket( Msg );
	plr->m_session->SendPacket( Msg );

	plr->mTradeTarget = 0;
	_player->mTradeTarget = 0;
	plr->ResetTradeVariables();
}

void WorldSession::HandleSetTradeItem(CPacketUsn& packet)
{
	if(_player->mTradeTarget == 0)
		return;

	if(_player->mTradeStatus == TRADE_STATUS_LOCKED)
		return;

	MSG_C2S::stTradeSetItem MsgRecv;packet >> MsgRecv;

	uint8 TradeSlot = MsgRecv.tradeslot;
	uint8 SourceBag = MsgRecv.bag;
	uint8 SourceSlot = MsgRecv.slot;
	Player * pTarget = _player->GetMapMgr()->GetPlayer( _player->mTradeTarget );

	Item * pItem = _player->GetItemInterface()->GetInventoryItem(SourceBag, SourceSlot);
	if( pTarget == NULL || pItem == 0 || TradeSlot >= MAX_TRADE_SLOT || ( TradeSlot < MAX_TRADE_SLOT && (!GetPermissionCount() && pItem->IsSoulbound()) ) )
		return;

	if(pItem->GetProto()->Bonding == ITEM_BIND_NOEXSTRANGE && !GetPermissionCount())
	{
		_player->GetItemInterface()->BuildInventoryChangeError(
			pItem, NULL, IVN_ERR_CANT_TRADE);
		return;
	}

	if(TradeSlot < MAX_TRADE_SLOT && pItem->IsSoulbound() && !GetPermissionCount())
	{
		//sCheatLog.writefromsession(this, "tried to cheat trade a soulbound item");
		//Disconnect();
		return;
	}

	for(uint32 i = 0; i < MAX_TRADE_SLOT; ++i)
	{
		// duping little shits
		if(_player->mTradeItems[i] == pItem || pTarget->mTradeItems[i] == pItem)
		{
			//sCheatLog.writefromsession(this, "tried to dupe an item through trade");
			Disconnect();
			return;
		}
	}

	_player->mTradeItems[TradeSlot] = pItem;
	_player->SendTradeUpdate();
}

void WorldSession::HandleTradeLock(CPacketUsn& packet)
{
	Player * plr = _player->GetTradeTarget();
	if(_player->mTradeTarget == 0 || !plr)
		return;

	MSG_S2C::stTradeStat Msg;
	uint32 TradeStatus = TRADE_STATUS_LOCKED;

	// Tell the other player we're locked.
	Msg.nStat	= TradeStatus;
	plr->m_session->SendPacket( Msg );
	_player->mTradeStatus = TradeStatus;
}

void WorldSession::HandleTradeSwitch(CPacketUsn& packet)
{
	MSG_C2S::stTradeSwitch msg;
	packet >> msg;
	_player->SwitchTrade( !msg.open );
}

void WorldSession::HandleSetTradeGold(CPacketUsn& packet)
{
	if(_player->mTradeTarget == 0)
		return;

	if(_player->mTradeStatus == TRADE_STATUS_LOCKED)
		return;

	MSG_C2S::stTradeSetGold MsgRecv;packet >> MsgRecv;
	uint32 Gold = MsgRecv.nGold;

	if(_player->mTradeGold != Gold)
	{
		_player->mTradeGold = (Gold > _player->GetUInt32Value(PLAYER_FIELD_COINAGE) ? _player->GetUInt32Value(PLAYER_FIELD_COINAGE) : Gold);
		_player->SendTradeUpdate();
	}
}

void WorldSession::HandleClearTradeItem(CPacketUsn& packet)
{
	if(_player->mTradeTarget == 0)
		return;

	MSG_C2S::stTradeClearItem MsgRecv;packet >> MsgRecv;
	uint8 TradeSlot = MsgRecv.tradeslot;
	if(TradeSlot >= MAX_TRADE_SLOT)
		return;

	_player->mTradeItems[TradeSlot] = 0;
	_player->SendTradeUpdate();
}

void WorldSession::HandleAcceptTrade(CPacketUsn& packet)
{
	Player * plr = _player->GetTradeTarget();
	if(_player->mTradeTarget == 0 || !plr)
		return;

	if(_player->mTradeStatus != TRADE_STATUS_LOCKED || !(plr->mTradeStatus == TRADE_STATUS_LOCKED || plr->mTradeStatus == TRADE_STATUS_ACCEPTED))
		return;

	MSG_S2C::stTradeStat Msg;
	uint32 TradeStatus = TRADE_STATUS_ACCEPTED;
	
	// Tell the other player we're green.
	Msg.nStat	= TradeStatus;
	plr->m_session->SendPacket( Msg );
	_player->mTradeStatus = TradeStatus;

	if(plr->mTradeStatus == TRADE_STATUS_ACCEPTED)
	{
		// Ready!
		uint32 ItemCount = 0;
		uint32 TargetItemCount = 0;
		Player * pTarget = plr;

		// Calculate Item Count
		for(uint32 Index = 0; Index < 7; ++Index)
		{
			if(_player->mTradeItems[Index] != 0)	++ItemCount;
			if(pTarget->mTradeItems[Index] != 0)	++TargetItemCount;
		}

		if( (_player->m_ItemInterface->CalculateFreeSlots(NULL) + ItemCount) < TargetItemCount ||
			(pTarget->m_ItemInterface->CalculateFreeSlots(NULL) + TargetItemCount) < ItemCount )
		{
			// Not enough slots on one end.
			TradeStatus = TRADE_STATUS_CANCELLED;
		}
		else
		{
			TradeStatus = TRADE_STATUS_COMPLETE;
			uint64 Guid;
			Item * pItem;
			
			//MyLog::yunyinglog->info("item TradeBegin player1[%u][%s] player2[%u][%s]", _player->GetLowGUID(), _player->GetName(), pTarget->GetLowGUID(), pTarget->GetName());
			// Remove all items from the players inventory
			for(uint32 Index = 0; Index < MAX_TRADE_SLOT; ++Index)
			{
				Guid = _player->mTradeItems[Index] ? _player->mTradeItems[Index]->GetGUID() : 0;
				if(Guid != 0)
				{
					if( _player->mTradeItems[Index]->IsSoulbound() && !GetPermissionCount())
					{
						_player->mTradeItems[Index] = NULL;
					}
					else
					{
						if(GetPermissionCount()>0)
						{
						}
						pItem = _player->m_ItemInterface->SafeRemoveAndRetreiveItemByGuid(Guid, true);
						//MyLog::yunyinglog->info("item-trade-player[%u][%s] remove["I64FMT"][%u] count[%u]", _player->GetLowGUID(), _player->GetName(), _player->mTradeItems[Index]->GetGUID(), _player->mTradeItems[Index]->GetEntry(), _player->mTradeItems[Index]->GetUInt32Value(ITEM_FIELD_STACK_COUNT));
					}
				}

				Guid = pTarget->mTradeItems[Index] ? pTarget->mTradeItems[Index]->GetGUID() : 0;
				if(Guid != 0)
				{
					if( pTarget->mTradeItems[Index]->IsSoulbound() && !pTarget->GetSession()->GetPermissionCount())
					{
						pTarget->mTradeItems[Index] = NULL;
					}
					else
					{
						pTarget->m_ItemInterface->SafeRemoveAndRetreiveItemByGuid(Guid, true);
						//MyLog::yunyinglog->info("item-trade-player[%u][%s] remove["I64FMT"][%u] count[%u]", pTarget->GetLowGUID(), pTarget->GetName(), pTarget->mTradeItems[Index]->GetGUID(), pTarget->mTradeItems[Index]->GetEntry(), pTarget->mTradeItems[Index]->GetUInt32Value(ITEM_FIELD_STACK_COUNT));
					}
				}
			}

			// Dump all items back into the opposite players inventory
			for(uint32 Index = 0; Index < MAX_TRADE_SLOT; ++Index)
			{
				pItem = _player->mTradeItems[Index];
				if(pItem != 0)
				{
					pItem->SetOwner(pTarget);
					if( !pTarget->m_ItemInterface->AddItemToFreeSlot(pItem) )
						delete pItem;
					else
						MyLog::yunyinglog->info("item-trade-player[%u][%s] item["I64FMT"][%u] count[%u] to player[%u][%s]", _player->GetLowGUID(), _player->GetName(), pItem->GetGUID(), pItem->GetEntry(), pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT), pTarget->GetLowGUID(), pTarget->GetName());

				}

				pItem = pTarget->mTradeItems[Index];
				if(pItem != 0)
				{
					pItem->SetOwner(_player);
					if( !_player->m_ItemInterface->AddItemToFreeSlot(pItem) )
						delete pItem;
					else
						MyLog::yunyinglog->info("item-trade-player[%u][%s] item["I64FMT"][%u] count[%u] to player[%u][%s]", pTarget->GetLowGUID(), pTarget->GetName(), pItem->GetGUID(), pItem->GetEntry(), pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT), _player->GetLowGUID(), _player->GetName());

				}
			}

			// Trade Gold
			if(pTarget->mTradeGold)
			{
				_player->ModCoin(pTarget->mTradeGold);
				pTarget->ModCoin(-(int32)pTarget->mTradeGold);
				MyLog::yunyinglog->info("gold-trade-player[%u][%s] get gold[%u] from player[%u][%s]", _player->GetLowGUID(), _player->GetName(), pTarget->mTradeGold, pTarget->GetLowGUID(), pTarget->GetName());
			}

			if(_player->mTradeGold)
			{
				pTarget->ModCoin(_player->mTradeGold);
				_player->ModCoin(-(int32)_player->mTradeGold);
				MyLog::yunyinglog->info("gold-trade-player[%u][%s] get gold[%u] from player[%u][%s]", pTarget->GetLowGUID(), pTarget->GetName(), _player->mTradeGold, _player->GetLowGUID(), _player->GetName());
			}

			//MyLog::yunyinglog->info("item TradeEnd");

			// Close Window
			TradeStatus = TRADE_STATUS_COMPLETE;
			Msg.nStat	= TradeStatus;
			plr->m_session->SendPacket( Msg );
			_player->m_session->SendPacket( Msg );

			_player->mTradeStatus = TRADE_STATUS_COMPLETE;
			plr->mTradeStatus = TRADE_STATUS_COMPLETE;

			// Reset Trade Vars
			_player->ResetTradeVariables();
			pTarget->ResetTradeVariables();
			
			plr->mTradeTarget = 0;
			_player->mTradeTarget = 0;
		}
	}
}
