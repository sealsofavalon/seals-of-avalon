#include "StdAfx.h"
#include "TreasureMgr.h"
#include "../../SDBase/Protocol/S2C_Chat.h"

TreasureMgr* g_treasure_mgr = NULL ;

//加载可以使用的位置的数量
void TreasureMgr::LoadTreasurePos()
{
	QueryResult * qr = WorldDatabase.Query("SELECT * FROM position_storage");
	if (qr)
	{
		do 
		{
			Field* f = qr->Fetch();
			treasureposdata* tdata = new treasureposdata;
			tdata->entry = f[0].GetUInt32();
			tdata->mapid = f[1].GetUInt32();
			tdata->x = f[2].GetFloat();
			tdata->y = f[3].GetFloat();
			tdata->z = f[4].GetFloat();
			tdata->o = f[5].GetFloat();
			tdata->use = false ;

			m_treasurepos.push_back(tdata);

		} while ( qr->NextRow() );
		delete qr ;
	}
}
treasureposdata* TreasureMgr::FindTreasureposdata(ui32 entry)
{
	for (int i = 0 ; i < m_treasurepos.size(); i++)
	{
		if (m_treasurepos[i]->entry == entry)
		{
			return m_treasurepos[i];
		}
	}
	return NULL;
}
TreasureData* TreasureMgr::FindTreasure(ui32 activityid)
{
	std::map<ui32, TreasureData*>::iterator it = m_treasures.find( activityid );
	if( it != m_treasures.end() )
		return it->second;
	else
		return NULL;
}

bool TreasureMgr::FindNextCurDayOfWeek(TreasureData* Data)
{
	int curdayofweekindex =  7;
	bool bfind = false;
	
	for(int i = 0; i < 7; i++ )
	{	
		if( Data->treasure->repeat_dayofweek[i] && is_dayofweek( UNIXTIME, i ) 
			&& in_duration( UNIXTIME, Data->treasure->starthour, Data->treasure->expirehour) && !Data->isbegin)
		{
			if (Data->next_dayofweek == 7)
			{
				bfind = true ;
				curdayofweekindex = i;
				break ;
			}else
			{
				if(i == Data->next_dayofweek)
				{
					bfind = true ;
					curdayofweekindex = i;
					break ;
				}
			}
			
		}
	}
	
	if(curdayofweekindex == 7)
	{
		return false ;
	}

	if(bfind)
	{
		for (int j = curdayofweekindex + 1; j < 7; j++)
		{
			if( Data->treasure->repeat_dayofweek[j])
			{
				Data->next_dayofweek = j;
				return true;
			}
		}

		for (int j = 0; j < curdayofweekindex + 1; j++)
		{
			if( Data->treasure->repeat_dayofweek[j])
			{
				Data->next_dayofweek = j;
				if (j == curdayofweekindex)
				{
					if (UNIXTIME - Data->lastrefurbishtime > 5 * TIME_DAY)
					{
						return true;
					}else
					{
						return false;
					}
				}else
				{
					return true;
				}
			}
		}
	}


	return false ;
}
bool TreasureMgr::Can_begin( TreasureData* Data )
{
	
	if (Data && Data->treasure)
	{
		if( Data->treasure->starttime && Data->treasure->expire )
		{
			if( UNIXTIME >= Data->treasure->starttime && UNIXTIME <= Data->treasure->expire && !Data->isbegin)
			{	
				return FindNextCurDayOfWeek(Data);
			}
		}
	}
	return false ;
}
void TreasureMgr::AddTreasureData(activity_mgr::treasure_event_config_t *treasure)
{
	if (treasure == NULL)
	{
		return ;
	}
	if (treasure->expire <= UNIXTIME) // 过期活动
	{
		return ;
	}
	TreasureData* pkTreasure = FindTreasure(treasure->id);
	if (!pkTreasure)
	{
		TreasureData* pkNewTreasure = new TreasureData;

		pkNewTreasure->treasure = treasure;
		m_treasures.insert(std::map<ui32, TreasureData*>::value_type(treasure->id,  pkNewTreasure));

		// 当前活动可以进行
		if (Can_begin(pkTreasure))
		{
			BeginTreasure(pkNewTreasure);
		}
	}
}

void TreasureMgr::OnUpdateTreasureData(activity_mgr::treasure_event_config_t* treasure) // 外部更新
{
	if (!treasure)
	{
		return ;
	}
	
	TreasureData* pkTreasure = FindTreasure(treasure->id);
	
	if (!pkTreasure) // 
	{
		AddTreasureData(treasure);
		return ;
	}
	if (treasure->expire <= UNIXTIME) // 过期活动
	{
		RemoveTreasureData(treasure->id);
		return ;
	}

	pkTreasure->next_dayofweek = 7;
	pkTreasure->lastrefurbishtime = 0;

	EndTreasure(pkTreasure);
	if (Can_begin(pkTreasure))
	{
		BeginTreasure(pkTreasure);
	}

}
void TreasureMgr::RemoveTreasureData(ui32 activityid)
{
	std::map<ui32, TreasureData*>::iterator it = m_treasures.find( activityid);
	if( it != m_treasures.end() )
	{
		EndTreasure(it->second);
		delete it->second ;
		m_treasures.erase(it);
	}
}

void TreasureMgr::BeginTreasure(TreasureData* data)
{
	if (data && data->treasure)
	{
		data->vGameobj.clear();
		/*if (data->treasure->treasureitemcount == 0 )
		{
			return ;
		}*/
		uint32 PosCount = data->treasure->treasureposend -  data->treasure->treasureposstart;
		uint32 GameOBJCount = data->treasure->treasurecount;

		
		std::vector<treasureposdata*> vTemPos;
		
		for (int i = 0 ; i < m_treasurepos.size(); i++)
		{
			if (!m_treasurepos[i]->use && m_treasurepos[i]->entry >= data->treasure->treasureposstart && m_treasurepos[i]->entry < data->treasure->treasureposend )
			{
				vTemPos.push_back(m_treasurepos[i]);
			}
		}

		if (vTemPos.size() == 0)
		{
			return ;
		}

		if (PosCount > vTemPos.size())
		{
			PosCount = vTemPos.size();
		}
		if (GameOBJCount > PosCount)
		{
			GameOBJCount = PosCount ;
		}
		
		vector<treasureposdata*> PosList;

		for (int i =0; i < PosCount; i++)
		{
			srand(time(NULL));
			uint32 index =  rand() % vTemPos.size();
			PosList.push_back(vTemPos[index]);
			vTemPos.erase(vTemPos.begin() + index);
		}
		
		
		for (int i = 0 ; i < GameOBJCount; i++)
		{
			srand(time(NULL));
			uint32 index =  rand() % PosList.size();
			SwapGameOBject(data, PosList[index]);
			PosList.erase(PosList.begin() + index);
		}
		
		data->refurbishcount = 1 ;
		data->lastrefurbishtime = UNIXTIME ;
		data->isbegin = true;
	}
}
void TreasureMgr::SwapGameOBject(TreasureData* data, treasureposdata* PosData)
{
	if (!data || !PosData)
	{
		return  ;
	}
	MapMgr* pMapMgr = sInstanceMgr.GetMapMgr( PosData->mapid );
	if (pMapMgr)
	{
		GOSpawn * sp = new GOSpawn;
		sp->entry = data->treasure->treasureid;
		sp->x = PosData->x;
		sp->y = PosData->y;
		sp->z = PosData->z;
		sp->o = PosData->o;
		sp->o1 = 0;
		sp->o2 = 0;
		sp->o3 = 0;
		sp->facing = 0;
		sp->flags = 911;
		sp->state = 0;
		sp->faction = 10000;
	
		sp->scale = data->treasure->treasurescale;
		if (sp->scale <= 0.1f)
		{
			sp->scale = 1.0f;
		}

		GameObject* p = pMapMgr->CreateGameObject(data->treasure->treasureid);
		if( p->Load( sp ) )
		{
			p->PushToWorld( pMapMgr );
			p->m_loadedFromDB = true;
			if (data->treasure->lockitemtype == 0)
			{
				p->m_noRespawn = 1; // 原地刷新模式
			}else
			{
				p->m_noRespawn = 0;
			}
			p->range = 10.0f;

			if(p->GetUInt32Value(GAMEOBJECT_TYPE_ID) == GAMEOBJECT_TYPE_TRAP)
			{
				p->invisible = false;
				p->invisibilityFlag = INVIS_FLAG_TRAP;
				p->charges = 1;
				p->checkrate = 1;
				p->m_TriggerByPlayer = 1;
				p->m_AlwaysCanSee = 1;
				sEventMgr.AddEvent(p, &GameObject::TrapSearchTarget, EVENT_GAMEOBJECT_TRAP_SEARCH_TARGET, 100, 0,0);
			}
			p->SetUInt32Value( GAMEOBJECT_STATE, 1 );

			PosData->use = true ;
			
			std::map<ui32, ui64>::iterator it = data->vGameobj.find(PosData->entry);
			if (it != data->vGameobj.end())
			{
				it->second = p->GetGUID();
			}else
			{
				data->vGameobj.insert(std::map<ui32, ui64>::value_type(PosData->entry, p->GetGUID()));
			}

		}
		else
		{
			delete p;
			delete sp;
		}
	}
}
void TreasureMgr::EndTreasure(TreasureData* data)
{
	if (data)
	{
		for( std::map<ui32, ui64>::iterator GIT = data->vGameobj.begin(); GIT != data->vGameobj.end(); ++GIT )
		{
			treasureposdata* pkDate = FindTreasureposdata(GIT->first);
			if (pkDate)
			{
				MapMgr* pMapMgr = sInstanceMgr.GetMapMgr( pkDate->mapid );
				if (pMapMgr)
				{
					pMapMgr->DespawnActivityCreature(GIT->second, false);
				}
				pkDate->use = false ;
			}
		}
		data->vGameobj.clear();
		data->refurbishcount = 0;
		data->isbegin = false;
		
	}
}
void TreasureMgr::UpdateTreasure(TreasureData* data)
{
	if (!in_duration( UNIXTIME, data->treasure->starthour, data->treasure->expirehour))
	{
		if (data->treasure && data->treasure->description[1].length())
		{
			MSG_S2C::stChat_Message Msg;
			sChatHandler.FillMessageData( CHAT_MSG_SYSTEM,data->treasure->description[0].c_str(), 0, &Msg, 0 );
			sWorld.SendGlobalMessage(Msg);
		}
		
		EndTreasure(data);
		return ;
	}
	// lockitemtype = 0 ---  reswapn = 1  so  we need upadat lockitemtype > 0
	if (data->treasure->lockitemtype > 0 && UNIXTIME - data->lastrefurbishtime >= data->treasure->refurbishtime * TIME_MINUTE )
	{
		//treasureitemcount = 0  the object need all swap.
		if (data->refurbishcount >= data->treasure->treasureitemcount && data->treasure->treasureitemcount != 0)
		{
			return ;
		}
		for( std::map<ui32, ui64>::iterator GIT = data->vGameobj.begin(); GIT != data->vGameobj.end(); ++GIT )
		{
			treasureposdata* pkDate = FindTreasureposdata(GIT->first);
			if (pkDate)
			{
				MapMgr* pMapMgr = sInstanceMgr.GetMapMgr( pkDate->mapid );
				if (pMapMgr)
				{
					GameObject* p =pMapMgr->GetGameObject(GIT->second);
					if (!p)
					{
						SwapGameOBject(data, pkDate);
					}
				}
			}
		}
		
		data->lastrefurbishtime = UNIXTIME ;
		data->refurbishcount++ ;
	}
	
}

void TreasureMgr::Update()
{
	//先清除需要删除的列表
	for (int i = 0; i < m_removetreasure.size(); i++)
	{
		RemoveTreasureData(m_removetreasure[i]);
	}
	m_removetreasure.clear();


	for (std::map<ui32, TreasureData*>::iterator it = m_treasures.begin(); it != m_treasures.end(); ++it)
	{
		TreasureData* pkData = it->second;
		if (pkData ) 
		{
			if (pkData->isbegin)
			{
				UpdateTreasure(pkData);
				
			}else
			{
				if (pkData->treasure->starttime <= UNIXTIME)
				{
					if (pkData->treasure->expire > UNIXTIME)
					{
						if (Can_begin(pkData))
						{
							BeginTreasure(pkData);
						}
						
					}else
					{
						m_removetreasure.push_back(pkData->treasure->id);
					}
				}
			}
		}
	}
	
}