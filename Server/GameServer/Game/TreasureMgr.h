#ifndef __TREASURE_MGR_H__
#define __TREASURE_MGR_H__


#include "activity.h"
struct TreasureData
{
	activity_mgr::treasure_event_config_t* treasure;
	std::map<ui32, ui64> vGameobj;
	ui32 lastrefurbishtime; //上次刷新时间
	ui32 refurbishcount; //当前第几次刷新啦
	ui32 isbegin;
	uint8 next_dayofweek;
	TreasureData()
	{
		treasure = NULL;
		lastrefurbishtime = 0 ;
		refurbishcount = 0;
		isbegin = false;
		vGameobj.clear();
		next_dayofweek = 7;
	}
};

struct treasureposdata
{
	ui32 entry;
	ui32 mapid;
	float x,y,z,o; 

	bool use ;
};



class TreasureMgr
{
public:
	// 数据

	bool Can_begin( TreasureData* data );
	bool FindNextCurDayOfWeek(TreasureData* Data);
	TreasureData* FindTreasure(ui32 activityid);
	treasureposdata* FindTreasureposdata(ui32 entry);
	void LoadTreasurePos();
	void AddTreasureData(activity_mgr::treasure_event_config_t* treasure);
	void OnUpdateTreasureData(activity_mgr::treasure_event_config_t* treasure); // 外部更新
	void RemoveTreasureData(ui32 activityid);
public:
	void Update();
protected:

	void SwapGameOBject(TreasureData* data, treasureposdata* PosData);

	void BeginTreasure(TreasureData* data);
	void EndTreasure (TreasureData* data);
	void UpdateTreasure(TreasureData* data);

	void DoRemove();

	
protected:
private:
	std::map<ui32, TreasureData*> m_treasures;
	std::vector<treasureposdata*> m_treasurepos;
	std::vector<ui32> m_removetreasure;
};

extern TreasureMgr* g_treasure_mgr;
#endif