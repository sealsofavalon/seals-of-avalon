#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Spell.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "../../SDBase/Protocol/S2C_Move.h"
#include "../../SDBase/Protocol/S2C_Trade.h"
#include "SunyouInstance.h"
#include "SunyouBattleGroundBase.h"
#include "DynamicObject.h"
#include "SunyouRaid.h"

Unit::Unit()
{
	lastSpellDamage = 0;
	m_fearDamageTaken = 0;
	m_powerRegenerateLock = false;
	m_ForceRoot = false;
	m_fDamage2Health = 0.0f;
	memset(m_spellAuras, 0, sizeof(m_spellAuras));
	memset(MechanicsDispelsAura, 0, sizeof(MechanicsDispelsAura));
	memset(MechanicsDispelsCnt, -1, sizeof(MechanicsDispelsCnt));
	m_attackcnt = 0;
	m_attackTimer = 0;
	m_attackTimer_1 = 0;
	m_duelWield = false;

	m_fearmodifiers = 0;
	m_extraRegenManaPoint = 0;
	m_extraRegenManaPointPCT = 0.0f;

	m_extraRegenHealPoint = 0;
	m_extraRegenHealPCT = 0.f;

	m_extraaddhealthpoint = 0;

	m_state = 0;
	m_special_state = 0;
	m_currentSpell = NULL;
	m_meleespell = 0;
	m_addDmgOnce = 0;
	m_TotemSlots[0] = NULL;
	m_TotemSlots[1] = NULL;
	m_TotemSlots[2] = NULL;
	m_TotemSlots[3] = NULL;
	m_ObjectSlots[0] = 0;
	m_ObjectSlots[1] = 0;
	m_ObjectSlots[2] = 0;
	m_ObjectSlots[3] = 0;
	m_silenced = 0;
	disarmed   = false;

	// Pet
	m_isPet = false;

	//DK:modifiers
	PctRegenModifier = 0;
	for( uint32 x = 0; x < 4; x++ )
	{
		PctPowerRegenModifier[x] = 1;
	}
	m_speedModifier = 0;
	m_slowdown = 0;
	m_mountedspeedModifier=0;
	m_maxSpeed = 0;
	for(uint32 x=0;x<MECHANIC_MAX;x++)
	{
		MechanicsDispels[x]=0;
		MechanicsResistancesPCT[x]=0;
		ModDamageTakenByMechPCT[x]=0;
		MechanicsProbability[x]=0;
	}

	//SM
	SM_CriticalChance=0;
	SM_FDur=0;//flat
	SM_PDur=0;//pct
	SM_FRadius=0;
	SM_FRange=0;
	SM_PCastTime=0;
	SM_FCastTime=0;
	SM_PCriticalDamage=0;
	SM_FDOT=0;
	SM_PDOT=0;
	SM_FEffectBonus=0;
	SM_PEffectBonus=0;
	SM_FDamageBonus=0;
	SM_PDamageBonus=0;
	SM_PSPELL_VALUE[0]=0;
	SM_PSPELL_VALUE[1]=0;
	SM_PSPELL_VALUE[2]=0;
	SM_FSPELL_VALUE[0]=0;
	SM_FSPELL_VALUE[1]=0;
	SM_FSPELL_VALUE[2]=0;
	SM_FHitchance=0;
	SM_PRange=0;//pct
	SM_PRadius=0;
	SM_PAPBonus=0;
	SM_PCost=0;
	SM_FCost=0;
	SM_FAdditionalTargets=0;
	SM_PJumpReduce=0;
	SM_FSpeedMod=0;
	SM_PNonInterrupt=0;
	SM_FPenalty=0;
	SM_PPenalty=0;
	SM_FCooldownTime = 0;
	SM_PCooldownTime = 0;
	SM_FChanceOfSuccess = 0;
	SM_FRezist_dispell = 0;
	SM_PRezist_dispell = 0;
	SM_FThreatReduce = 0;
	SM_PThreatReduce = 0;
	SM_FStackCount = 0;

	m_pacified = 0;
	m_interruptRegen = 0;
	m_resistChance = 0;
	m_powerRegenPCT = 0;
	RAPvModifier=0;
	APvModifier=0;
	stalkedby=0;

	m_extraattacks = 0;
	m_stunned = 0;
	m_sleep = 0;
	m_manashieldamt=0;
	m_rooted = 0;
	m_stormed = 0;
	m_triggerSpell = 0;
	m_triggerDamage = 0;
	m_canMove = 0;
	m_noInterrupt = 0;
	m_modlanguage = -1;

	critterPet = NULL;
	summonPet = NULL;

	m_useAI = false;
	for(uint32 x=0;x<10;x++)
	{
		dispels[x]=0;
		CreatureAttackPowerMod[x] = 0;
		CreatureRangedAttackPowerMod[x] = 0;
	}
	//REMIND:Update these if you make any changes
	CreatureAttackPowerMod[UNIT_TYPE_MISC] = 0;
	CreatureRangedAttackPowerMod[UNIT_TYPE_MISC] = 0;
	CreatureAttackPowerMod[11] = 0;
	CreatureRangedAttackPowerMod[11] = 0;

	m_invisible = false;
	m_invisFlag = INVIS_FLAG_NORMAL;

	for(int i = 0; i < INVIS_FLAG_TOTAL; i++)
	{
		m_invisDetect[i] = 0;
	}

	m_stealthLevel = 0;
	m_stealthDetectBonus = 0;
	m_stealth = 0;
	m_can_stealth = true;

	for(uint32 x=0;x<5;x++)
		BaseStats[x]=0;

	m_H_regenTimer = 2000;
	m_P_regenTimer = 2000;

	//	if(GetTypeId() == TYPEID_PLAYER) //only player for now
	//		CalculateActualArmor();

	m_aiInterface = new AIInterface();
	m_aiInterface->Init(this, AITYPE_AGRO, MOVEMENTTYPE_NONE);

	m_emoteState = 0;
	m_oldEmote = 0;

	BaseDamage[0]=0;
	BaseOffhandDamage[0]=0;
	BaseRangedDamage[0]=0;
	BaseDamage[1]=0;
	BaseOffhandDamage[1]=0;
	BaseRangedDamage[1]=0;

	m_CombatUpdateTimer = 0;
	for(uint32 x=0;x<7;x++)
	{
		SchoolImmunityList[x] = 0;
		BaseResistance[x] = 0;
		HealDoneMod[x] = 0;
		HealDonePctMod[x] = 0;
		HealTakenMod[x] = 0;
		HealTakenPctMod[x] = 0;
		DamageTakenMod[x] = 0;
		DamageDoneModPCT[x]= 0;
		SchoolCastPrevent[x]=0;
		DamageTakenPctMod[x] = 1;
		SpellCritChanceSchool[x] = 0;
		PowerCostMod[x] = 0;
		PowerCostPctMod[x] = 0; // armor penetration & spell penetration
		AttackerCritChanceMod[x]=0;
		CritMeleeDamageTakenPctMod[x]=0;
		CritRangedDamageTakenPctMod[x]=0;
	}
	DamageTakenPctModOnHP35 = 1;
	RangedDamageTaken = 0;

	for(int i = 0; i < 5; i++)
	{
		m_detectRangeGUID[i] = 0;
		m_detectRangeMOD[i] = 0;
	}

	trackStealth = false;
	extradamage = 0;

	m_threatModifyer = 0;
	m_generatedThreatModifyer = 0;
	memset(m_auras, 0, (MAX_AURAS+MAX_PASSIVE_AURAS)*sizeof(Aura*));

	// diminishing return stuff
	memset( m_diminishAuraCount, 0, sizeof( m_diminishAuraCount ) );
	memset( m_diminishCount, 0, sizeof( m_diminishCount ) );
	memset( m_diminishTimer, 0, sizeof( m_diminishTimer ) );
	memset( m_auraStackCount, 0, sizeof( m_auraStackCount ) );
	m_diminishActive = false;
	dynObj = 0;
	pLastSpell = 0;
	m_flyspeedModifier = 0;
	bInvincible = false;
	m_redirectSpellPackets = 0;
	can_parry = false;
	bProcInUse = false;
	spellcritperc = 0;

	polySpell = 0;
	RangedDamageTaken = 0;
	m_procCounter = 0;
	m_extrastriketargets = 0;
	m_damgeShieldsInUse = false;
//	fearSpell = 0;
	m_extraAttackCounter = false;
	CombatStatus.SetUnit(this);
	m_temp_summon=false;
	m_chargeSpellsInUse=false;
	m_spellsbusy=false;
	m_interruptedRegenTime = 0;
	m_hasVampiricEmbrace = m_hasVampiricTouch = 0;
	m_hasBlessOfFreedom = false;
}

Unit::~Unit()
{
	RemoveAllAuras(true);

	if(SM_CriticalChance != 0) delete [] SM_CriticalChance ;
	if(SM_FDur != 0) delete [] SM_FDur ;//flat
	if(SM_PDur != 0) delete [] SM_PDur ;//pct
	if(SM_FRadius != 0) delete [] SM_FRadius ;
	if(SM_FRange != 0) delete [] SM_FRange ;
	if(SM_PCastTime != 0) delete [] SM_PCastTime ;
	if(SM_FCastTime != 0) delete [] SM_FCastTime ;
	if(SM_PCriticalDamage != 0) delete [] SM_PCriticalDamage ;
	if(SM_FDOT != 0) delete [] SM_FDOT ;
	if(SM_PDOT != 0) delete [] SM_PDOT ;
	if(SM_PEffectBonus != 0) delete [] SM_PEffectBonus ;
    if(SM_FEffectBonus != 0) delete [] SM_FEffectBonus ;
	if(SM_FDamageBonus != 0) delete [] SM_FDamageBonus ;
	if(SM_PDamageBonus != 0) delete [] SM_PDamageBonus ;
	if(SM_PSPELL_VALUE[0] != 0) delete [] SM_PSPELL_VALUE[0];
	if(SM_PSPELL_VALUE[1] != 0) delete [] SM_PSPELL_VALUE[1];
	if(SM_PSPELL_VALUE[2] != 0) delete [] SM_PSPELL_VALUE[2];
	if(SM_FSPELL_VALUE[0] != 0) delete [] SM_FSPELL_VALUE[0] ;
	if(SM_FSPELL_VALUE[1] != 0) delete [] SM_FSPELL_VALUE[1] ;
	if(SM_FSPELL_VALUE[2] != 0) delete [] SM_FSPELL_VALUE[2] ;
	if(SM_FHitchance != 0) delete [] SM_FHitchance ;
	if(SM_PRange != 0) delete [] SM_PRange ;//pct
	if(SM_PRadius != 0) delete [] SM_PRadius ;
	if(SM_PAPBonus != 0) delete [] SM_PAPBonus ;
	if(SM_PCost != 0) delete [] SM_PCost ;
	if(SM_FCost != 0) delete [] SM_FCost ;
	if(SM_FAdditionalTargets != 0) delete [] SM_FAdditionalTargets ;
	if(SM_PJumpReduce != 0) delete [] SM_PJumpReduce ;
	if(SM_FSpeedMod != 0) delete [] SM_FSpeedMod ;
	if(SM_PNonInterrupt != 0) delete [] SM_PNonInterrupt ;
	if(SM_FPenalty != 0) delete [] SM_FPenalty ;
	if(SM_PPenalty != 0) delete [] SM_PPenalty ;
	if(SM_FCooldownTime != 0) delete [] SM_FCooldownTime ;
	if(SM_PCooldownTime != 0) delete [] SM_PCooldownTime ;
	if(SM_FChanceOfSuccess != 0) delete [] SM_FChanceOfSuccess ;
	if(SM_FRezist_dispell != 0) delete [] SM_FRezist_dispell ;
	if(SM_PRezist_dispell != 0) delete [] SM_PRezist_dispell ;
	if(SM_FThreatReduce != 0) delete [] SM_FThreatReduce ;
	if(SM_PThreatReduce != 0) delete [] SM_PThreatReduce ;
	if(SM_FStackCount != 0) delete [] SM_FStackCount ;

	if(m_currentSpell)
		m_currentSpell->cancel();

	delete m_aiInterface;

	/*for(int i = 0; i < 4; i++)
		if(m_ObjectSlots[i])
			delete m_ObjectSlots[i];*/
}

void Unit::Update( uint32 p_time )
{
	CombatStatus.Update();
	_UpdateSpells( p_time );

	if(!isDead())
	{
		//-----------------------POWER & HP REGENERATION-----------------
/* Please dont do temp fixes. Better report to me. Thx. Shady */
        if( p_time >= m_H_regenTimer )
		    RegenerateHealth();
	    else
		    m_H_regenTimer -= p_time;

		if( p_time >= m_P_regenTimer )
		{
			RegeneratePower( false );
			m_interruptedRegenTime=0;
		}
		else
		{
			m_P_regenTimer -= p_time;
			if (m_interruptedRegenTime)
			{
				if(p_time>=m_interruptedRegenTime)
					RegeneratePower( true );
				else
					m_interruptedRegenTime -= p_time;
			}
		}


		if(m_aiInterface != NULL && m_useAI)
			m_aiInterface->Update(p_time);

		if(m_diminishActive)
		{
			uint32 count = 0;
			for(uint32 x = 0; x < 16; ++x)
			{
				// diminishing return stuff
				if(m_diminishTimer[x] && !m_diminishAuraCount[x])
				{
					if(p_time >= m_diminishTimer[x])
					{
						// resetting after 15 sec
						m_diminishTimer[x] = 0;
						m_diminishCount[x] = 0;
					}
					else
					{
						// reducing, still.
						m_diminishTimer[x] -= p_time;
						++count;
					}
				}
			}
			if(!count)
				m_diminishActive = false;
		}

/*		//if health changed since last time. Would be perfect if it would work for creatures too :)
		if(m_updateMask.GetBit(UNIT_FIELD_HEALTH))
			EventHealthChangeSinceLastUpdate();*/
	}
}

bool Unit::canReachWithAttack(Unit *pVictim, bool& badfacing)
{
//	float targetreach = pVictim->GetFloatValue(UNIT_FIELD_COMBATREACH);
	float selfreach = m_floatValues[UNIT_FIELD_COMBATREACH];
	float targetradius = pVictim->m_floatValues[UNIT_FIELD_BOUNDINGRADIUS];
	float selfradius = m_floatValues[UNIT_FIELD_BOUNDINGRADIUS];
	float targetscale = pVictim->m_floatValues[OBJECT_FIELD_SCALE_X];
	float selfscale = m_floatValues[OBJECT_FIELD_SCALE_X];

	if( GetMapId() != pVictim->GetMapId() )
		return false;

	float distance = sqrt(GetDistanceSq(pVictim));
	float attackreach = (((targetradius*targetscale) + selfreach) + (((selfradius/**selfradius*/)*selfscale)));

	//formula adjustment for player side.
	if( IsPlayer() )
	{

		//range can not be less than 5 yards - this is normal combat range, SCALE IS NOT SIZE

		// latency compensation!!
		// figure out how much extra distance we need to allow for based on our movespeed and latency.
		/*
 		if( pVictim->IsPlayer() && static_cast< Player* >( pVictim )->m_isMoving )
 		{
 			// this only applies to PvP.
 			uint32 lat = static_cast< Player* >( pVictim )->GetSession() ? static_cast< Player* >( pVictim )->GetSession()->GetLatency() : 0;

 			// if we're over 500 get fucked anyway.. your gonna lag! and this stops cheaters too
 			lat = ( lat > 500 ) ? 500 : lat;

 			// calculate the added distance
 			attackreach += ( m_runSpeed * 0.001f ) * float( lat );
 		}

 		if( static_cast< Player* >( this )->m_isMoving )
 		{
 			// this only applies to PvP.
 			uint32 lat = static_cast< Player* >( this )->GetSession() ? static_cast< Player* >( this )->GetSession()->GetLatency() : 0;

 			// if we're over 500 get fucked anyway.. your gonna lag! and this stops cheaters too
 			lat = ( lat > 500) ? 500 : lat;

 			// calculate the added distance
 			attackreach += ( m_runSpeed * 0.001f ) * float( lat );
 		}
		*/
	}


	if( attackreach < 5 )
		attackreach = 5; //normal units with too small reach.

	
	badfacing = false;

	if( !this->IsPlayer() )
		return ( distance <= attackreach );
	else if( distance <= attackreach )
	{
		if( !this->isInFront( pVictim ) )
			badfacing = true;
		else
			return true;
	}

	return false;
}

void Unit::GiveGroupXP(Unit *pVictim, Player *PlayerInGroup)
{
	if(!PlayerInGroup)
		return;
	if(!pVictim)
		return;
	if(!PlayerInGroup->InGroup())
		return;
	Group *pGroup = PlayerInGroup->GetGroup();
	uint32 xp;
	if(!pGroup)
		return;

	//Get Highest Level Player, Calc Xp and give it to each group member
	Player *pHighLvlPlayer = NULL;
	Player *pGroupGuy = NULL;
	  int active_player_count=0;
	Player *active_player_list[MAX_GROUP_SIZE_RAID];//since group is small we can afford to do this ratehr then recheck again the whole active player set
	int total_level=0;
	float xp_mod = 1.0f;

	//change on 2007 04 22 by Zack
	//we only take into count players that are near us, on same map
	GroupMembersSet::iterator itr;
	for(uint32 i = 0; i < pGroup->GetSubGroupCount(); i++) {
		for(itr = pGroup->GetSubGroup(i)->GetGroupMembersBegin(); itr != pGroup->GetSubGroup(i)->GetGroupMembersEnd(); ++itr)
		{
			pGroupGuy = (*itr)->m_loggedInPlayer;
			if( pGroupGuy &&
				pGroupGuy->isAlive() &&
//				PlayerInGroup->GetInstanceID()==pGroupGuy->GetInstanceID() &&
				pVictim->GetMapMgr() == pGroupGuy->GetMapMgr() &&
				pGroupGuy->GetDistance2dSq(pVictim)<100*100
				)
			{
				active_player_list[active_player_count]=pGroupGuy;
				active_player_count++;
				total_level += pGroupGuy->getLevel();
				if(pHighLvlPlayer)
				{
					if(pGroupGuy->getLevel() > pHighLvlPlayer->getLevel())
						pHighLvlPlayer = pGroupGuy;
				}
				else
					pHighLvlPlayer = pGroupGuy;
			}
		}
	}
	if(active_player_count<1) //killer is always close to the victim. This should never execute
	{
		if(PlayerInGroup == 0)
		{
			PlayerInfo * pleaderinfo = pGroup->GetLeader();
			if(!pleaderinfo->m_loggedInPlayer)
				return;

			PlayerInGroup = pleaderinfo->m_loggedInPlayer;
		}

		xp = CalculateXpToGive(pVictim, PlayerInGroup);
		if( PlayerInGroup->m_sunyou_instance )
			PlayerInGroup->GiveXP(xp, pVictim->GetGUID(), true, GIVE_EXP_TYPE_DYNAMIC_INSTANCE);
		else
			PlayerInGroup->GiveXP(xp, pVictim->GetGUID(), true, GIVE_EXP_TYPE_KILL_MONSTER);
	}
	else
	{
		if( pGroup->GetGroupType() == GROUP_TYPE_PARTY)
		{
			if(active_player_count==2)
				xp_mod=1.2f;
			else if(active_player_count==3)
				xp_mod=1.3f;
			else if(active_player_count==4)
				xp_mod=1.4f;
			else if(active_player_count==5)
				xp_mod=1.5f;
			else xp_mod=1;//in case we have only 2 members ;)
		}

		xp = CalculateXpToGive(pVictim, pHighLvlPlayer, true, total_level, active_player_count);
		//i'm not sure about this formula is correct or not. Maybe some brackets are wrong placed ?
		for(int i=0;i<active_player_count;i++)
		{
			float xp_player = xp;
			xp_player = xp_player * active_player_list[i]->m_ExtraEXPRatio/100;
			//xp = xp * active_player_list[i]->getLevel() / total_level;
			xp_player = xp_player * active_player_list[i]->getLevel() / total_level;
			xp_player = xp_player * xp_mod;

			if( PlayerInGroup->m_sunyou_instance )
				active_player_list[i]->GiveXP( float2int32(xp_player), pVictim->GetGUID(), true, GIVE_EXP_TYPE_DYNAMIC_INSTANCE );
			else
				active_player_list[i]->GiveXP( float2int32(xp_player), pVictim->GetGUID(), true, GIVE_EXP_TYPE_KILL_MONSTER );
		}
	}
}

void Unit::HandleProc( uint32 flag, Unit* victim, SpellEntry* CastingSpell, uint32 dmg, uint32 abs , bool mainhand )
{

	/*

	EVENT_ENTERCOMBAT,
	EVENT_LEAVECOMBAT,
	EVENT_DAMAGETAKEN,
	EVENT_TARGET_CAST_SPELL,
	EVENT_TARGET_PARRY,
	EVENT_TARGET_DODGE,
	EVENT_TARGET_BLOCK,
	EVENT_TARGET_CRI_ATTACK,
	EVENT_TARGET_DEAD,
	EVENT_PARRY,
	EVENT_DODGE,
	EVENT_BLOCK,
	EVENT_CRI_ATTACK,
	EVENT_DEAD,
	EVENT_SUPPORT_DEAD,
	EVENT_FEAR,
	EVENT_UNFEAR,
	EVENT_FOLLOWOWNER,
	EVENT_WANDER,
	EVENT_UNWANDER,
	EVENT_UNITDIED,
	*/

	if (IsCreature())
	{
		AiEvents AiEvent = EVENT_NULL;
		if (flag & PROC_ON_CRIT_ATTACK)
		{
			AiEvent = EVENT_CRI_ATTACK;
		}
		else if (flag & PROC_ON_CRIT_HIT_VICTIM)
		{
			AiEvent = EVENT_TARGET_CRI_ATTACK;
		}
		else if (flag & PROC_ON_DODGE_VICTIM)
		{
			AiEvent = EVENT_TARGET_DODGE;
		}
		else if ( flag & PROC_ON_BLOCK_VICTIM)
		{
			AiEvent = EVENT_TARGET_BLOCK;
		}
		else if (flag & PROC_ON_SPELL_CRIT_HIT_VICTIM)
		{
			AiEvent = EVENT_TARGET_SPELL_CRI_HIT;
		}
		else if (flag & PROC_ON_SPELL_HIT_VICTIM)
		{
			AiEvent = EVENT_TARGET_SPELL_HIT;
		}
		else if (flag & PROC_ON_SPELL_HIT)
		{
			AiEvent = EVENT_SPELL_HIT;
		}
		else if (flag & PROC_ON_SPELL_CRIT_HIT)
		{
			AiEvent = EVENT_SPELL_CRI_HIT;
		}
		else if (flag & PROC_ON_DIE)
		{
			m_aiInterface->HandleEventToAssist(EVENT_SUPPORT_DEAD, victim, 0);
		}
		
		if (AiEvent != EVENT_NULL)
		{
			m_aiInterface->HandleEvent(AiEvent, victim, 0);
		}
		
	}

	++m_procCounter;
	bool can_delete = !bProcInUse; //if this is a nested proc then we should have this set to TRUE by the father proc
	bProcInUse = true; //locking the proc list

	std::list< uint32 > remove;
	std::list< struct ProcTriggerSpell >::iterator itr,itr2;
	for( itr = m_procSpells.begin(); itr != m_procSpells.end(); )  // Proc Trigger Spells for Victim
	{
		itr2 = itr;
		++itr;
		if( itr2->deleted )
		{
			if( can_delete )
				m_procSpells.erase( itr2 );
			continue;
		}

		uint32 origId = itr2->origId;
		if( CastingSpell != NULL )
		{
			//this is to avoid spell proc on spellcast loop. We use dummy that is same for both spells
			//if( CastingSpell->Id == itr2->spellId )
			if( CastingSpell->Id == itr2->origId || CastingSpell->Id == itr2->spellId )
			{
				//printf("WOULD CRASH HERE ON PROC: CastingId: %u, OrigId: %u, SpellId: %u\n", CastingSpell->Id, itr2->origId, itr2->spellId);
				continue;
			}
		}
		SpellEntry* ospinfo = dbcSpell.LookupEntry( origId );//no need to check if exists or not since we were not able to register this trigger if it would not exist :P

		//this requires some specific spell check,not yet implemented
		if( itr2->procFlags & flag )
		{
			uint32 spellId = itr2->spellId;
			if( itr2->procFlags & PROC_ON_CAST_SPECIFIC_SPELL )
			{

				if( CastingSpell == NULL )
					continue;

				//this is wrong, dummy is too common to be based on this, we should use spellgroup or something
				SpellEntry* sp = dbcSpell.LookupEntry( spellId );
				if( sp->dummy != CastingSpell->dummy )
				{
					if( !ospinfo->School )
						continue;
					if( ospinfo->School != CastingSpell->School )
						continue;
					if( CastingSpell->EffectImplicitTargetA[0] == 1 ||
						CastingSpell->EffectImplicitTargetA[1] == 1 ||
						CastingSpell->EffectImplicitTargetA[2] == 1) //Prevents school based procs affecting caster when self buffing
						continue;
				}
				else
					if( sp->dummy == 1 )
						continue;
			}
			// handle "equipped item class/subclass"
			// kinda hacky.
			if( IsPlayer() )
			{
				if( ospinfo && ospinfo->EquippedItemClass && ospinfo->EquippedItemSubClass )
				{
					Item * itm;
					if( itm = static_cast<Player*>(this)->GetItemInterface()->GetInventoryItem( mainhand ? EQUIPMENT_SLOT_MAINHAND : EQUIPMENT_SLOT_OFFHAND ) )
					{
						if( itm->GetProto()->Class != ospinfo->EquippedItemClass || !(( 1 << itm->GetProto()->SubClass ) & ospinfo->EquippedItemSubClass) )
							continue; // not applicable
					}
				}
			}
			uint32 proc_Chance = itr2->procChance;
			SpellEntry* spe  = dbcSpell.LookupEntry( spellId );



			// feral = no procs (need a better way to do this)
			/*if( this->IsPlayer() && static_cast<Player*>(this)->GetShapeShift() )
			{
				if( spe->NameHash == SPELL_HASH_LIGHTNING_SPEED ) // mongoose
					continue;
				else if( spe->NameHash == SPELL_HASH_HOLY_STRENGTH ) //crusader
					continue;
			}*/
			//Custom procchance modifications based on equipped weapon speed.

			//if( this->IsPlayer() && spe != NULL && (
			//	spe->NameHash == SPELL_HASH_MAGTHERIDON_MELEE_TRINKET ||
			//	spe->NameHash == SPELL_HASH_ROMULO_S_POISON ||
			//	spe->NameHash == SPELL_HASH_BLACK_TEMPLE_MELEE_TRINKET || spellId == 16870 ) )
			//{
			//	float ppm = 1.0f;
			//	switch( spe->NameHash )
			//	{
			//		case SPELL_HASH_MAGTHERIDON_MELEE_TRINKET:
			//			ppm = 1.5f;
			//			break; // dragonspine trophy
			//		case SPELL_HASH_ROMULO_S_POISON:
			//			ppm = 1.5f;
			//			break; // romulo's
			//		case SPELL_HASH_BLACK_TEMPLE_MELEE_TRINKET:
			//			ppm = 1.0f;
			//			break; // madness of the betrayer
			//	}
			//	switch( spellId )
			//	{
			//		case 16870:
			//			ppm = 2.0f;
			//			break; //druid: clearcasting
			//	}

			//	Item * mh = static_cast< Player* >( this )->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_MAINHAND );
			//	Item * of = static_cast< Player* >( this )->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_OFFHAND );

			//	if( mh != NULL && of != NULL )
			//	{
			//		float mhs = float( /*mh->GetProto()->Delay*/GetUInt32Value(UNIT_FIELD_BASEATTACKTIME) );
			//		float ohs = float( /*of->GetProto()->Delay*/GetUInt32Value(UNIT_FIELD_BASEATTACKTIME) );
			//		proc_Chance = (ui32)( ( mhs + ohs ) * 0.001f * ppm / 0.6f );
			//	}
			//	else if( mh != NULL )
			//	{
			//		float mhs = float( /*mh->GetProto()->Delay*/GetUInt32Value(UNIT_FIELD_BASEATTACKTIME) );
			//		proc_Chance = float2int32( mhs * 0.001f * ppm / 0.6f );
			//	}
			//	else
			//		proc_Chance = 0;

			//	if( static_cast< Player* >( this )->IsInFeralForm() )
			//	{
			//		if( static_cast< Player* >( this )->GetShapeShift() == FORM_CAT )
			//		{
			//			proc_Chance = (ui32)( ppm / 0.6f );
			//		}
			//		else if( static_cast< Player* >( this )->GetShapeShift() == FORM_BEAR || static_cast< Player* >( this )->GetShapeShift() == FORM_DIREBEAR )
			//		{
			//			proc_Chance = (ui32)( ppm / 0.24f );
			//		}
			//	}
			//}

			if (ospinfo)
				SM_FIValue( SM_FChanceOfSuccess, (int32*)&proc_Chance, ospinfo->SpellGroupType );
			if( spellId && Rand( proc_Chance ) )
			{
				SpellCastTargets targets;
				if( itr2->procFlags & PROC_TARGET_SELF )
					targets.m_unitTarget = GetGUID();
				else
					targets.m_unitTarget = victim->GetGUID();
				/* hmm whats a reasonable value here */
				if( m_procCounter > 40 )
				{
					/* something has proceed over 10 times in a loop :/ dump the spellids to the crashlog, as the crashdump will most likely be useless. */
					// BURLEX FIX ME!
					//OutputCrashLogLine("HandleProc %u SpellId %u (%s) %u", flag, spellId, sSpellStore.LookupString(sSpellStore.LookupEntry(spellId)->Name), m_procCounter);
					return;
				}

				//check if we can trigger due to time limitation
				if( ospinfo && ospinfo->proc_interval )
				{
					uint32 now_in_ms=getMSTime();
					if( itr2->LastTrigger + ospinfo->proc_interval > now_in_ms )
						continue; //we can't trigger it yet.
					itr2->LastTrigger = now_in_ms; // consider it triggered
				}
				//since we did not allow to remove auras like these with interrupt flag we have to remove them manually.
				if( itr2->procFlags & PROC_REMOVEONUSE )
					RemoveAura( origId );
				int dmg_overwrite = dmg;

				//these are player talents. Fuckem they pull the emu speed down
				if( IsPlayer() )
				{
					if (ospinfo) // add by tj

					{
						if( spe && spe->ProcOnNameHash[0] != 0 )
						{
							if( CastingSpell == NULL )
								continue;

							if( CastingSpell->NameHash != spe->ProcOnNameHash[0] &&
								CastingSpell->NameHash != spe->ProcOnNameHash[1] &&
								CastingSpell->NameHash != spe->ProcOnNameHash[2] )
								continue;
						}

						uint32 talentlevel = 0;
					}
				}

				SpellEntry *spellInfo = dbcSpell.LookupEntry(spellId );
				Spell *spell = new Spell(this, spellInfo ,true, NULL);
				spell->forced_basepoints[0] = dmg_overwrite;
				spell->ProcedOnSpell = CastingSpell;
				targets.m_targetMask = spellInfo->Targets;
				targets.m_srcX = GetPositionX();
				targets.m_srcY = GetPositionY();
				targets.m_srcZ = GetPositionZ();
				if (victim)
				{
					targets.m_destX = victim->GetPositionX();
					targets.m_destY = victim->GetPositionY();
					targets.m_destZ = victim->GetPositionZ();
				}
				

				//Spell *spell = new Spell(this,spellInfo,false,0,true,false);
				//if(spellId==974||spellId==32593||spellId==32594) // Earth Shield handler
				//{
				//	spell->pSpellId=itr2->spellId;
				//	spell->SpellEffectDummy(0);
				//	delete spell;
				//	continue;
				//}
				spell->pSpellId=origId;
				spell->prepare(&targets);
			}//not always we have a spell to cast
		}
	}

	m_chargeSpellsInUse=true;
	std::map<uint32,struct SpellCharge>::iterator iter,iter2;
	iter=m_chargeSpells.begin();
	while(iter!= m_chargeSpells.end())
	{
		iter2=iter++;
		if(iter2->second.count)
		{
			if((iter2->second.ProcFlag & flag))
			{
				//Fixes for spells that dont lose charges when dmg is absorbd
				if(iter2->second.ProcFlag==680&&dmg==0) continue;
//				if(CastingSpell)
//				{
//
//					SpellCastTime *sd = dbcSpellCastTime.LookupEntry(CastingSpell->CastingTimeIndex);
//					if(!sd) continue; // this shouldnt happen though :P
//					//if we did not proc these then we should not remove them
//					if( CastingSpell->Id == iter2->second.spellId)
//						continue;
//					switch(iter2->second.spellId)
//					{
//					case 12043:
//						{
//							//Presence of Mind and Nature's Swiftness should only get removed
//							//when a non-instant and bellow 10 sec. Also must be nature :>
////							if(!sd->CastTime||sd->CastTime>10000) continue;
//							if(sd->CastTime==0)
//								continue;
//						}break;
//					case 17116: //Shaman - Nature's Swiftness
//					case 16188:
//						{
////							if( CastingSpell->School!=SCHOOL_NATURE||(!sd->CastTime||sd->CastTime>10000)) continue;
//							if( CastingSpell->School!=SCHOOL_NORMAL||(sd->CastTime==0)) continue;
//						}break;
//					case 16166:
//						{
//							if(!(CastingSpell->School==SCHOOL_NORMAL||CastingSpell->School==SCHOOL_NORMAL||CastingSpell->School==SCHOOL_NORMAL))
//								continue;
//						}break;
//					case 14177: //cold blood will get removed on offensive spell
//						{
//							if(victim==this || isFriendly(this, victim))
//								continue;
//						}break;
//					}
//				}
				if(iter2->second.lastproc!=0)
				{
					if(iter2->second.procdiff>3000)
					{
						//--(iter2->second.count);
						RemoveAura(iter2->second.spellId);
					}
				}
				else
				{
					//--(iter2->second.count);		// done in Aura::Remove
					this->RemoveAura(iter2->second.spellId);
				}
			}
		}
		if(!iter2->second.count)
		{
			m_chargeSpells.erase(iter2);
		}
	}

	for(;!m_chargeSpellRemoveQueue.empty();)
	{
		iter = m_chargeSpells.find(m_chargeSpellRemoveQueue.front());
		if(iter != m_chargeSpells.end())
		{
			if(iter->second.count>1)
				--iter->second.count;
			else
				m_chargeSpells.erase(iter);
		}
		m_chargeSpellRemoveQueue.pop_front();
	}

	m_chargeSpellsInUse=false;
	if(can_delete) //are we the upper level of nested procs ? If yes then we can remove the lock
		bProcInUse = false;
}

//damage shield is a triggered spell by owner to atacker
void Unit::HandleProcDmgShield(uint32 flag, Unit* attacker)
{
	//make sure we do not loop dmg procs
	if(this==attacker || !attacker)
		return;
	if(m_damgeShieldsInUse)
		return;
	m_damgeShieldsInUse = true;
	//charges are already removed in handleproc
	std::list<DamageProc>::iterator i;
	std::list<DamageProc>::iterator i2;
	for(i = m_damageShields.begin();i != m_damageShields.end();)     // Deal Damage to Attacker
	{
		i2 = i++; //we should not proc on proc.. not get here again.. not needed.Better safe then sorry.
		if(	(flag & (*i2).m_flags) )
		{
			if(PROC_MISC & (*i2).m_flags)
			{
				MSG_S2C::stSpell_Damage_Shield Msg;
				Msg.target_guid = this->GetGUID();
				Msg.attacker_guid = attacker->GetGUID();
				Msg.damage		= (*i2).m_damage;
				Msg.school		= (*i2).m_school;
				SendMessageToSet(Msg,true);
				this->DealDamage(attacker,(*i2).m_damage,0,0,(*i2).m_spellId);
			}
			else
			{
				SpellEntry	*ability=dbcSpell.LookupEntry((*i2).m_spellId);
				this->Strike( attacker, RANGED, ability, 0, 0, (*i2).m_damage, true, false, false, false, true );
			}
		}
	}
	m_damgeShieldsInUse = false;
}

/*
void Unit::HandleProcSpellOnSpell(Unit* Victim,uint32 damage,bool critical)
{
}
*/
bool Unit::isCasting()
{
	return (m_currentSpell != NULL);
}

bool Unit::IsInInstance()
{
	MapInfo *pMapinfo = WorldMapInfoStorage.LookupEntry(this->GetMapId());
	if(pMapinfo)
		return (pMapinfo->type != INSTANCE_NULL);

	return false;
}

void Unit::RegenerateHealth()
{
	m_H_regenTimer = 5000;//set next regen time

	if(!isAlive())
		return;

	// player regen
	if(this->IsPlayer())
	{
		// These only NOT in combat
		if(!CombatStatus.IsInCombat())
		{
			static_cast< Player* >( this )->RegenerateHealth(false);
		}
		else
			static_cast< Player* >( this )->RegenerateHealth(true);
	}
	else
	{
		// Only regen health out of combat
		//if(!CombatStatus.IsInCombat())
		//	static_cast<Creature*>(this)->RegenerateHealth();
	}
}

void Unit::RegeneratePower(bool isinterrupted)
{
	if( m_powerRegenerateLock )
		return;

    // This is only 2000 IF the power is not rage
	if (isinterrupted)
		m_interruptedRegenTime =5000;
	else
		m_P_regenTimer = 5000;//set next regen time

	if(!isAlive())
		return;

	// player regen
	if(this->IsPlayer())
	{
		uint32 powertype = GetPowerType();
		switch(powertype)
		{
		case POWER_TYPE_MANA:
			static_cast< Player* >( this )->RegenerateMana(isinterrupted);
			break;
		}
		/*

		There is a problem here for druids.
		Druids when shapeshifted into bear have 2 power with different regen timers
		a) Mana (which regenerates while shapeshifted
		b) Rage

		Mana has a regen timer of 2 seconds
		Rage has a regen timer of 3 seconds

		I think the only viable way of fixing this is to have 2 different timers
		to check each individual power.

		Atm, mana is being regen at 3 seconds while shapeshifted...

		*/


		// druids regen mana when shapeshifted
// 		if(getClass() == CLASS_DRUID && powertype != POWER_TYPE_MANA)
// 			static_cast< Player* >( this )->RegenerateMana(isinterrupted);
	}
	else
	{
		uint32 powertype = GetPowerType();
		switch(powertype)
		{
		case POWER_TYPE_MANA:
			static_cast<Creature*>(this)->RegenerateMana();
			break;
		}
	}
}

void Unit::CalculateResistanceReduction(Unit *pVictim,dealdamage * dmg, SpellEntry* ability)
{
	// sunyou method
	float armor = 0.0f;

	int32 full = dmg[0].full_damage;
	int School = 0;
	if (ability)
	{
		School = ability->School;
		armor = (float)pVictim->GetResistance( ability->School );
	}
	else
	{
		armor = (float)pVictim->GetResistance( 0 );
	}
	
	//uint32 damagereduction = armor / 100;
	//dmg[0].full_damage = std::max<int>( 1, dmg[0].full_damage - damagereduction );
	//dmg[0].full_damage = (float)dmg[0].full_damage *( 1.f - min( 0.75f, ( 0.1f + armor / ( (flot)pVictim->getLevel() * 46.875f * 3.5f ) ) ) );
	//if( dmg[0].full_damage <= 0 )
	//	dmg[0].full_damage = 1;

	//dmg[0].resisted_damage =  full - dmg[0].full_damage;
	//dmg[0].full_damage = full;


	if (!School)
	{
		float fLVC = 90 *  getLevel() + 450;
		float DTR = armor / (armor + fLVC);
		if (DTR>0.75f)
			DTR=0.75f;
		full = full * (1 - DTR);

	}
	else
	{
		float SpellDamageResistance = float(armor) / (float(armor) + 167.f);
		if (SpellDamageResistance > 0.75f)
			SpellDamageResistance = 0.75f;

		full = full * (1 - SpellDamageResistance);
	}
	
	if (full < 1)
	{
		full = 1;
	}
	dmg[0].resisted_damage = dmg[0].full_damage -  full;

	dmg[0].full_damage = dmg[0].full_damage;
	/*
	if( this->IsPlayer() && pVictim->IsCreature() )
	{
		int Role_Level = this->getLevel();
		int Monster_Level = pVictim->getLevel();

		if( Role_Level - Monster_Level + 8 <= 0 )
			dmg[0].full_damage = 0;
		else
			dmg[0].full_damage = (float)dmg[0].full_damage * ( float( Role_Level - Monster_Level + 8 ) / 8.f );
	}
	*/


	//float AverageResistance = 0.0f;
	//float ArmorReduce;

	//if((*dmg).school_type == 0)//physical
	//{
	//	if(this->IsPlayer())
	//		ArmorReduce = PowerCostPctMod[0];
	//	else
	//		ArmorReduce = 0.0f;

	//	if(ArmorReduce >= pVictim->GetResistance(0))		// fully penetrated :O
	//		return;

	//	double Reduction = double(pVictim->GetResistance(0)) / double(pVictim->GetResistance(0)+400+(85*getLevel()));
	//	//dmg reduction formula from xinef
	//	double Reduction = 0;
	//	if(getLevel() < 60) Reduction = double(pVictim->GetResistance(0) - ArmorReduce) / double(pVictim->GetResistance(0)+400+(85*getLevel()));
	//	else if(getLevel() > 59 && getLevel() < 70) Reduction = double(pVictim->GetResistance(0) - ArmorReduce) / double(pVictim->GetResistance(0)-22167.5+(467.5*getLevel()));
	//	
	//	else Reduction = double(pVictim->GetResistance(0) - ArmorReduce) / double(pVictim->GetResistance(0)+10557.5);
	//	if(Reduction > 0.75f) Reduction = 0.75f;
	//	else if(Reduction < 0) Reduction = 0;
	//	if(Reduction) dmg[0].full_damage = (uint32)(dmg[0].full_damage*(1-Reduction));	  // no multiply by 0
	//}
	//else
	//{
	//	// applying resistance to other type of damage
	//	int32 RResist = float2int32( float(pVictim->GetResistance( (*dmg).school_type ) + ((pVictim->getLevel() > getLevel()) ? (pVictim->getLevel() - this->getLevel()) * 5 : 0)) - PowerCostPctMod[(*dmg).school_type] );
	//	if (RResist<0)
	//		RResist = 0;
	//	AverageResistance = (float)(RResist) / (float)(getLevel() * 5) * 0.75f;
	//	  if(AverageResistance > 0.75f)
	//		AverageResistance = 0.75f;

	//	   NOT WOWWIKILIKE but i think it's actual to add some fullresist chance frome resistances
	//	  if (!ability || !(ability->Attributes & ATTRIBUTES_IGNORE_INVULNERABILITY))
	//	  {
	//		  float Resistchance=(float)pVictim->GetResistance( (*dmg).school_type)/(float)pVictim->getLevel();
	//		  Resistchance*=Resistchance;
	//		  if(Rand(Resistchance))
	//			  AverageResistance=1.0f;
	//	  }

	//	if(AverageResistance>0)
	//		(*dmg).resisted_damage = (uint32)(((*dmg).full_damage)*AverageResistance);
	//	else
	//		(*dmg).resisted_damage=0;
	//}

}

uint32 Unit::GetSpellDidHitResult( Unit* pVictim, uint32 weapon_damage_type, SpellEntry* ability )
{
	Item * it = NULL;
	float hitchance          = 0.0f;
	float dodge				 = 0.0f;
	float parry				 = 0.0f;
	float block				 = 0.0f;

	float hitmodifier		 = 0;
	int32 self_skill		 = 0;		
	int32 victim_skill       = 0;
	uint32 SubClassSkill	 = SKILL_UNARMED;

	bool backAttack			 = !pVictim->isInFront( this ); // isInBack is bugged!
	uint32 vskill            = 0;

	//==========================================================================================
	//==============================Victim Skill Base Calculation===============================
	//==========================================================================================
	if( pVictim->IsPlayer() )
	{
		vskill = static_cast< Player* >( pVictim )->_GetSkillLineCurrent( SKILL_DEFENSE );
		if( weapon_damage_type != RANGED && !backAttack )
		{
			//--------------------------------block chance----------------------------------------------
			//block = pVictim->GetFloatValue(PLAYER_BLOCK_PERCENTAGE); //shield check already done in Update chances
			//--------------------------------dodge chance----------------------------------------------
			if( pVictim->m_stunned <= 0 )
			{
				dodge = pVictim->GetFloatValue( PLAYER_DODGE_PERCENTAGE );
			}
			//--------------------------------parry chance----------------------------------------------
			if( pVictim->can_parry && !pVictim->disarmed )
			{
				/*
				if( static_cast< Player* >( pVictim )->HasSpell( 3127 ) || static_cast< Player* >( pVictim )->HasSpell( 18848 ) )
				{
					parry = pVictim->GetFloatValue( PLAYER_PARRY_PERCENTAGE );
				}
				*/
			}
		}
		//victim_skill = float2int32( vskill + static_cast< Player* >( pVictim )->CalcRating( PLAYER_RATING_MODIFIER_DEFENCE ) );
	}
	//--------------------------------mob defensive chances-------------------------------------
	else
	{
		//if( weapon_damage_type != RANGED && !backAttack )
		//{
		//	if (pVictim->IsPlayer())
		//	{
		//		dodge = pVictim->GetFloatValue(dodge);
		//	}

		//	
		//}
		dodge = 5.f;
			 // what is this value?
		//victim_skill = pVictim->getLevel() * 5;
		//if(pVictim->m_objectTypeId == TYPEID_UNIT)
		//{
		//	Creature * c = (Creature*)(pVictim);
		//	if(c&&c->GetCreatureName()&&c->GetCreatureName()->Rank == ELITE_WORLDBOSS)
		//	{
		//		victim_skill = std::max(victim_skill,((int32)this->getLevel()+3)*5); //used max to avoid situation when lowlvl hits boss.
		//	}
		//}
	}
	//==========================================================================================
	//==============================Attacker Skill Base Calculation=============================
	//==========================================================================================
	if(this->IsPlayer())
	{
		self_skill = 0;
		Player* pr = static_cast< Player* >( this );
		hitmodifier = pr->GetHitFromMeleeSpell();

		float RC= 13.f + pr->getLevel() * 0.1f;
		float PhyRC = 10.f + pr->getLevel() * 0.1f;
		float hit = 0.f;
		switch( weapon_damage_type )
		{
		case MELEE:   // melee main hand weapon
			it = disarmed ? NULL : pr->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_MAINHAND );
			hitmodifier += pr->GetFloatValue(PLAYER_RATING_MODIFIER_MELEE_HIT) / PhyRC;
			self_skill = float2int32( pr->GetFloatValue( PLAYER_RATING_MODIFIER_MELEE_MAIN_HAND_SKILL ) );
			break;
		case OFFHAND: // melee offhand weapon (dualwield)
			//it = disarmed ? NULL : pr->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_OFFHAND );
			//hitmodifier += pr->CalcRating( PLAYER_RATING_MODIFIER_MELEE_HIT );
			//self_skill = float2int32( pr->CalcRating( PLAYER_RATING_MODIFIER_MELEE_OFF_HAND_SKILL ) );
			//break;
		case RANGED:  // ranged weapon
			it = disarmed ? NULL : pr->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_MAINHAND );
			hitmodifier +=pr->GetFloatValue(PLAYER_RATING_MODIFIER_RANGED_HIT) / PhyRC;
			self_skill = float2int32( pr->GetFloatValue( PLAYER_RATING_MODIFIER_RANGED_SKILL ) );
			break;
		}


		// erm. some spells don't use ranged weapon skill but are still a ranged spell and use melee stats instead
		// i.e. hammer of wrath
		//if( ability && ability->NameHash == SPELL_HASH_HAMMER_OF_WRATH )
		//{
		//	it = pr->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_MAINHAND );
		//	hitmodifier += pr->CalcRating( PLAYER_RATING_MODIFIER_MELEE_HIT );
		//	self_skill = float2int32( pr->CalcRating( PLAYER_RATING_MODIFIER_MELEE_MAIN_HAND_SKILL ) );
		//}
		/*
		if(it && it->GetProto())
			SubClassSkill = GetSkillByProto(it->GetProto()->Class,it->GetProto()->SubClass);
		else
			SubClassSkill = SKILL_UNARMED;

		if(SubClassSkill==SKILL_FIST_WEAPONS)
			SubClassSkill = SKILL_UNARMED;

		//chances in feral form don't depend on weapon skill
		if(static_cast< Player* >( this )->IsInFeralForm())
		{
			uint8 form = static_cast< Player* >( this )->GetShapeShift();
			if(form == FORM_CAT || form == FORM_BEAR || form == FORM_DIREBEAR)
			{
				SubClassSkill = SKILL_FERAL_COMBAT;
				// Adjust skill for Level * 5 for Feral Combat
				self_skill += pr->getLevel() * 5;
			}
		}


		self_skill += pr->_GetSkillLineCurrent(SubClassSkill);
		*/
	}
	/*
	else
	{
		self_skill = this->getLevel() * 5;
		if(m_objectTypeId == TYPEID_UNIT)
		{
			Creature * c = (Creature*)(this);
			if(c&&c->GetCreatureName()&&c->GetCreatureName()->Rank == ELITE_WORLDBOSS)
				self_skill = std::max(self_skill,((int32)pVictim->getLevel()+3)*5);//used max to avoid situation when lowlvl hits boss.
		}
	}
	*/
	//==========================================================================================
	//==============================Special Chances Base Calculation============================
	//==========================================================================================
	//<THE SHIT> to avoid Linux bug.
	//float diffVcapped = (float)self_skill;
	//if(int32(pVictim->getLevel()*5)>victim_skill)
	//	diffVcapped -=(float)victim_skill;
	//else
	//	diffVcapped -=(float)(pVictim->getLevel()*5);

	//float diffAcapped = (float)victim_skill;
	//if(int32(this->getLevel()*5)>self_skill)
	//	diffAcapped -=(float)self_skill;
	//else
	//	diffAcapped -=(float)(this->getLevel()*5);
	//<SHIT END>

	//--------------------------------by victim state-------------------------------------------
	if(pVictim->IsPlayer()&&pVictim->GetStandState()) //every not standing state is >0
	{
		hitchance = 100.0f;
	}
	//--------------------------------by damage type and by weapon type-------------------------
	if( weapon_damage_type == RANGED )
	{
		dodge=0.0f;
		parry=0.0f;
	}

	if( ability->field114 & SPELL_AVOID_MASK_DODGE )
		dodge = 0;
	//else if(this->IsPlayer())
	//{
	//	/*
	//	it = static_cast< Player* >( this )->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_OFFHAND );
	//	if( it != NULL && it->GetProto()->InventoryType == INVTYPE_WEAPON && !ability )//dualwield to-hit penalty
	//	{
	//		hitmodifier -= 19.0f;
	//	}
	//	else
	//	{
	//		it = static_cast< Player* >( this )->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_MAINHAND );
	//		if( it != NULL && it->GetProto()->InventoryType == INVTYPE_2HWEAPON )//2 handed weapon to-hit penalty
	//			hitmodifier -= 4.0f;
	//	}
	//	*/
	//}

	//--------------------------------by skill difference---------------------------------------
	float vsk = (float)self_skill - (float)victim_skill;
	dodge = std::max( 0.0f, dodge - vsk * 0.04f );
	if( parry )
		parry = std::max( 0.0f, parry - vsk * 0.04f );
	if( block )
		block = std::max( 0.0f, block - vsk * 0.04f );

	if( vsk > 0 )
		hitchance = std::max( hitchance, 95.0f + vsk * 0.02f + hitmodifier );
	else
	{
		if( pVictim->IsPlayer() )
			hitchance = std::max( hitchance, 95.0f + vsk * 0.1f + hitmodifier ); //wowwiki multiplier - 0.04 but i think 0.1 more balanced
		else
			hitchance = std::max( hitchance, 100.0f + vsk * 0.6f + hitmodifier ); //not wowwiki but more balanced
	}

	if( ability && ability->SpellGroupType )
		SM_FFValue( SM_FHitchance, &hitchance, ability->SpellGroupType );
	
	//==========================================================================================
	//==============================One Roll Processing=========================================
	//==========================================================================================
	//--------------------------------cummulative chances generation----------------------------
	int nLevelDif = 0;
	bool pvp =(IsPlayer() && pVictim->IsPlayer());

	float resistchance = 0.f;
	if ( pVictim)
	{
		float hit = 0.f;
		nLevelDif = int(getLevel()) - int(pVictim->getLevel());
		if (nLevelDif + 2 >= 0)
		{
			hit = (0.95f + 0.0125f * nLevelDif);
			if (hit > 1.f)
				hit = 1.f;
		}
		else
		{
			if(pvp)
			{
				hit = 0.95f + 0.07f * (nLevelDif);
			}
			else
			{
				hit = 0.95f + 0.11f * (nLevelDif);
			}
		}
		if (hit < 0.5f)
			hit = 0.5f;
		if (IsCreature())
		{
			Creature* pCreature = (Creature*) this;
			if (pCreature->proto&&pCreature->GetCreatureName()->Rank != ELITE_NORMAL)
			{
				if (hit < 0.8)
				{
					hit = 0.8;
				}
			}
			else
			{
				if (hit < 0.7)
				{
					hit = 0.7;
				}
			}


		}

		resistchance = 100.f - (hit * 100 + (100 - hitchance));
	}



	if( ability->MechanicsType<27)
	{
		if(pVictim)
			resistchance += pVictim->MechanicsResistancesPCT[ability->MechanicsType];
		else
			resistchance += pVictim->MechanicsResistancesPCT[ability->MechanicsType];
	}


	float chances[4];
	chances[0]=std::max(0.0f,resistchance);
	chances[1]=chances[0]+dodge;
	chances[2]=chances[1]+parry;
	//chances[3]=chances[2]+block;


	//--------------------------------roll------------------------------------------------------
	float Roll = RandomFloat(100.0f);
	uint32 r = 0;
	while (r<3&&Roll>chances[r])
	{
		r++;
	}
	uint32 roll_results[4] = { SPELL_DID_HIT_MISS,SPELL_DID_HIT_DODGE,SPELL_DID_HIT_DEFLECT,SPELL_DID_HIT_SUCCESS};
	//uint32 roll_results[5] = { SPELL_DID_HIT_MISS,SPELL_DID_HIT_DODGE,SPELL_DID_HIT_DEFLECT,SPELL_DID_HIT_BLOCK,SPELL_DID_HIT_SUCCESS };
	return roll_results[r];
}

uint32 Unit::Strike(Unit* pVictim, uint32 weapon_damage_type, SpellEntry* ability, int32 add_damage,
					int32 pct_dmg_mod, uint32 exclusive_damage, bool disable_proc, bool skip_hit_check, bool bSchool, bool stato, bool weapon)
{
//==========================================================================================
//==============================Unacceptable Cases Processing===============================
//==========================================================================================
	if(!pVictim->isAlive() || !isAlive()  || IsStunned() || IsPacified() || IsFeared() || IsAsleep())
		return 0;
	if( pVictim->IsPet() ) return 0;

	if( pVictim == this )
	{
		MyLog::log->error( "connot strike self, GUID:%lu", pVictim->GetGUID() );
		return 0;
	}
	if( pVictim->IsPlayer() && !((Player*)pVictim)->m_EnableMeleeAttack )
		return 0;

	if(!ability && !isInFront(pVictim))
		if(IsPlayer())
		{
			MSG_S2C::stAttack_Swing_Bad_Facing Msg;
			static_cast< Player* >( this )->GetSession()->SendPacket( Msg );
			return 0;
		}
//==========================================================================================
//==============================Variables Initialization====================================
//==========================================================================================
	dealdamage dmg			  = {0,0,0};

	Item * it = NULL;

	float hitchance          = 100.0f;
	float dodge				 = 0.0f;
	float parry				 = 0.0f;
	float glanc              = 0.0f;
	float block				 = 0.0f;
	float crit				 = 0.0f;
	float baseCrit			 = 0.0f;
	float crush              = 0.0f;

	uint32 levelattacker = this->GetUInt32Value( UNIT_FIELD_LEVEL );
	uint32 levelvictim = pVictim->GetUInt32Value( UNIT_FIELD_LEVEL );
	//////////////////////////////////////////////////////////////////////////
	// sunyou method
	//////////////////////////////////////////////////////////////////////////

	if( pVictim->IsPlayer() )
	{
		if (!ability)
		{
			dodge = pVictim->GetFloatValue( PLAYER_DODGE_PERCENTAGE );
			//block = pVictim->GetFloatValue(PLAYER_BLOCK_PERCENTAGE);
			parry = pVictim->GetFloatValue(PLAYER_PARRY_PERCENTAGE);
			hitchance += ((Player*)pVictim)->GetHitFromMeleeSpell();
		}

		Item* pItem = ((Player*)pVictim)->GetItemInterface()->GetInventoryItem( EQUIPMENT_SLOT_OFFHAND );
		if( pItem )
			if( pItem->GetProto()->Class == ITEM_CLASS_WEAPON && pItem->GetProto()->SubClass == ITEM_SUBCLASS_WEAPON_SHIELD )
				block = pVictim->GetFloatValue( PLAYER_BLOCK_PERCENTAGE );

		//Crit = pVictim->GetFloatValue(PLAYER_CRIT_PERCENTAGE); 
	}
	else
	{
		if (!ability)
		{
			dodge = 5.f;
		}
		
		//block = 5.f;removed by gui
	}


	if(pVictim->IsPlayer()&&!pVictim->isInFront(this))
	{
		block = 0.f;
		parry = 0.f;
		dodge = 0.f;
	}

	if( this->IsPlayer() )
	{
		hitchance = hitchance + hitchance * static_cast<Player*>( this )->GetHitPCTFromSpell();
		hitchance = std::min(100.0f, hitchance);
		if (!ability || (ability &&ability->School == 0))
		{
			crit = GetFloatValue( PLAYER_CRIT_PERCENTAGE);
		}
		else
		{
			crit = GetFloatValue( PLAYER_SPELL_CRIT_PERCENTAGE1) + GetFloatValue( PLAYER_SPELL_CRIT_PERCENTAGE01 + ability->School);
		}

	}
	else
		crit = 5.f;

	// 等级压制暴击
	if (getLevel() < pVictim->getLevel())
	{
		bool pvp = pVictim->IsPlayer() && this->IsPlayer();
		if(pvp)
		{
			crit -= 5 * (pVictim->getLevel() - getLevel()) * 0.04f;
		}else
		{
			crit -= 5 * (pVictim->getLevel() - getLevel()) * 0.2f;
		}
	}
	else
	{
		crit += 5 * (getLevel() - pVictim->getLevel()) * 0.2f;
	}

	if (crit > 75)
	{
		crit = 75;
	}
	else if (crit < 5)
	{
		crit = 5;
	}

	if( ability && ability->SpellGroupType )
		SM_FFValue( this->SM_CriticalChance, &crit, ability->SpellGroupType );

	bool pvp =(IsPlayer() && pVictim->IsPlayer());
	
	if (!ability/* || (ability&& ability->Spell_Dmg_Type != SPELL_DMG_TYPE_MAGIC)*/)
	{
		int nLevelDif = int(getLevel()) - int(pVictim->getLevel());
		float hit = 0.f;
		if (nLevelDif + 2 >= 0)
		{
			hit = (0.95f + 0.0125f * nLevelDif);
			if (hit > 1.f)
				hit = 1.f;
		}
		else
		{
			if(pvp)
			{
				hit = 0.95f + 0.07f * (nLevelDif);
			}
			else
			{
				hit = 0.95f + 0.11f * (nLevelDif);
			}
		}

		if (hit < 0.50f)
			hit = 0.50f;

		if (IsCreature())
		{
			Creature* pCreature = (Creature*) (this);
			if (pCreature->proto&&pCreature->GetCreatureName()->Rank != ELITE_NORMAL)
			{
				if (hit < 0.8)
				{
					hit = 0.8;
				}
			}
			else
			{
				if (hit < 0.7)
				{
					hit = 0.7;
				}
			}
		}
		hitchance = hit * 100;

	}

	uint32 targetEvent		 = 0;
	uint32 hit_status		 = 0;

	uint32 blocked_damage	 = 0;
	int32  realdamage		 = 0;

	uint32 vstate			 = 1;
	uint32 aproc			 = 0;
	uint32 vproc			 = 0;

	float hitmodifier		 = 0;
	int32 self_skill;
	int32 victim_skill;
	uint32 SubClassSkill	 = SKILL_UNARMED;

	bool backAttack			 = !pVictim->isInFront( this );
	uint32 vskill            = 0;
	bool disable_dR			 = false;
	bool phyattack           = false;

	if(ability)
	{
		dmg.school_type = ability->School;

		if( ability->field114 & SPELL_AVOID_MASK_BLOCK )
			block = 0;
		if( ability->field114 & SPELL_AVOID_MASK_DODGE )
			dodge = 0;
	}
	else
	{
		if (GetTypeId() == TYPEID_UNIT)
			dmg.school_type = static_cast< Creature* >( this )->BaseAttackType;
		else
			dmg.school_type = SCHOOL_NORMAL;
	}
	

//==========================================================================================
//==============================One Roll Processing=========================================
//==========================================================================================
//--------------------------------cummulative chances generation----------------------------
	float chances[7];
	chances[0]=std::max(0.0f,100.0f-hitchance);
	chances[1]=chances[0]+dodge;
	chances[2]=chances[1]+parry;
	chances[3]=chances[2]+glanc;
	chances[4]=chances[3]+block;
	chances[5]=chances[4]+crit;
	chances[6]=chances[5]+crush;

//--------------------------------roll------------------------------------------------------
	float Roll = RandomFloat(100.0f);
	uint32 r = 0;
	while (r<7&&Roll>chances[r])
	{
		r++;
	}

	if(IsPlayer() && ability && ability->Id == 5003)
	{
		if( rand()%100 < GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE1) )
			r = 5;
	}
//--------------------------------postroll processing---------------------------------------
	uint32 abs = 0;

	std::string attackerName, victimName;
	if( IsCreature() )
		attackerName = ((Creature*)this)->creature_info->Name;
	else if( IsPlayer() )
		attackerName = ((Player*)this)->m_playerInfo->name;

	if( pVictim->IsCreature() )
		victimName = ((Creature*)pVictim)->creature_info->Name;
	else if( pVictim->IsPlayer() )
		victimName = ((Player*)pVictim)->m_playerInfo->name;

	switch(r)
	{
//--------------------------------miss------------------------------------------------------
	case 0:
		hit_status |= HITSTATUS_MISS;
		// dirty ai agro fix
		if(pVictim->GetTypeId() == TYPEID_UNIT && pVictim->GetAIInterface()->GetNextTarget() == NULL)
			pVictim->GetAIInterface()->AttackReaction(this, 1, 0);

		//MyLog::log->debug( "attacker:%s victim:%s [miss] [%.2f%%]", attackerName.c_str(), victimName.c_str(), chances[0] );
		break;
//--------------------------------dodge-----------------------------------------------------
	case 1:
		// dirty ai agro fix
		if(pVictim->GetTypeId() == TYPEID_UNIT && pVictim->GetAIInterface()->GetNextTarget() == NULL)
			pVictim->GetAIInterface()->AttackReaction(this, 1, 0);

		CALL_SCRIPT_EVENT(pVictim, OnTargetDodged)(this);
		CALL_SCRIPT_EVENT(this, OnDodged)(this);
		targetEvent = 1;
		vstate = DODGE;
		aproc |= PROC_ON_DODGE_VICTIM;

		//MyLog::log->debug( "attacker:%s victim:%s [dodge] [%.2f%%]", attackerName.c_str(), victimName.c_str(), dodge );
		break;
//--------------------------------parry-----------------------------------------------------
	case 2:
		// dirty ai agro fix
		if(pVictim->GetTypeId() == TYPEID_UNIT && pVictim->GetAIInterface()->GetNextTarget() == NULL)
			pVictim->GetAIInterface()->AttackReaction(this, 1, 0);

		CALL_SCRIPT_EVENT(pVictim, OnTargetParried)(this);
		CALL_SCRIPT_EVENT(this, OnParried)(this);
		targetEvent = 3;
		vstate = PARRY;
		pVictim->Emote(EMOTE_ONESHOT_PARRYUNARMED);			// Animation
		if(pVictim->IsPlayer())
		{
			pVictim->SetFlag( UNIT_FIELD_AURASTATE,AURASTATE_FLAG_PARRY );	//SB@L: Enables spells requiring parry
			if(!sEventMgr.HasEvent( pVictim, EVENT_PARRY_FLAG_EXPIRE ) )
				sEventMgr.AddEvent( pVictim, &Unit::EventAurastateExpire, (uint32)AURASTATE_FLAG_PARRY,EVENT_PARRY_FLAG_EXPIRE, 5000, 1, 0 );
			else
				sEventMgr.ModifyEventTimeLeft( pVictim, EVENT_PARRY_FLAG_EXPIRE, 5000 );
			if( static_cast< Player* >( pVictim )->getClass() == 1 || static_cast< Player* >( pVictim )->getClass() == 4 )//warriors for 'revenge' and rogues for 'riposte'
			{
				pVictim->SetFlag( UNIT_FIELD_AURASTATE,AURASTATE_FLAG_DODGE_BLOCK );	//SB@L: Enables spells requiring dodge
				if(!sEventMgr.HasEvent( pVictim, EVENT_DODGE_BLOCK_FLAG_EXPIRE ) )
					sEventMgr.AddEvent( pVictim, &Unit::EventAurastateExpire, (uint32)AURASTATE_FLAG_DODGE_BLOCK, EVENT_DODGE_BLOCK_FLAG_EXPIRE, 5000, 1, 0 );
				else
					sEventMgr.ModifyEventTimeLeft( pVictim, EVENT_DODGE_BLOCK_FLAG_EXPIRE, 5000 );
			}
		}

		//MyLog::log->debug( "attacker:%s victim:%s [parry] [%.2f%%]", attackerName.c_str(), victimName.c_str(), parry );
		break;
//--------------------------------not miss,dodge or parry-----------------------------------
	default:
		//hit_status |= HITSTATUS_HITANIMATION;//hit animation on victim
		if (ability && pVictim->SchoolImmunityList[ability->School])
		{
			vstate = IMMUNE;
		
		}
		else if(  !ability&&pVictim->SchoolImmunityList[0] )
			vstate = IMMUNE;
		else
		{
//--------------------------------state proc initialization---------------------------------
			vproc |= PROC_ON_ANY_DAMAGE_VICTIM;


			if (ability && ability->Spell_Dmg_Type != SPELL_DMG_TYPE_MAGIC)
			{
				phyattack = true;
			}

			if (!ability)
				phyattack = true;

			if( weapon_damage_type != RANGED )
			{
				aproc |= PROC_ON_MELEE_ATTACK;
				vproc |= PROC_ON_MELEE_ATTACK_VICTIM;
				if(ability && IsPlayer() && getClass()==CLASS_BOW)
				{
					if (ability->Id== 5003 )
						aproc |= PROC_ON_AUTO_SHOT_HIT;
					if (ability->Id == 5003)
						phyattack = true;
				}
			}
			else
			{
				aproc |= PROC_ON_RANGED_ATTACK;
				vproc |= PROC_ON_RANGED_ATTACK_VICTIM;				
			}

			if (phyattack)
			{
				aproc |= PROC_ON_PHYSICAL_ATTACK;
				vproc |= PROC_ON_PHYSICAL_ATTACK_VICTIM;
			}

//--------------------------------base damage calculation-----------------------------------
			if(exclusive_damage)
				dmg.full_damage = exclusive_damage;
			else
			{
				if( weapon_damage_type == MELEE && ability )
					dmg.full_damage = CalculateDamage( this, pVictim, MELEE, ability->SpellGroupType, ability,  false, add_damage, bSchool, stato, weapon  );
				else
					dmg.full_damage = CalculateDamage( this, pVictim, weapon_damage_type, 0, ability, false, add_damage, bSchool, stato, weapon );
			}
			if( weapon_damage_type == RANGED )
			{
				dmg.full_damage += pVictim->RangedDamageTaken;
			}

			if( ability && ability->MechanicsType == MECHANIC_BLEEDING )
				disable_dR = true;

			//float summaryPCTmod = (pVictim->DamageTakenPctMod[dmg.school_type] / 100.0f) + (GetDamageDonePctMod( dmg.school_type ) / 100.0f) + 1;

			if( pct_dmg_mod > 0 )
				dmg.full_damage = float2int32( dmg.full_damage *  ( float( pct_dmg_mod) / 100.0f ) );

			//技能的固定伤害
			//dmg.full_damage += add_damage;

			//a bit dirty fix
			/*if( ability != NULL && ability->NameHash == SPELL_HASH_SHRED )
			{
				summaryPCTmod *= 1 + pVictim->ModDamageTakenByMechPCT[MECHANIC_BLEEDING];
			}*/

			//dmg.full_damage = (dmg.full_damage < 0) ? 0 : float2int32(dmg.full_damage*summaryPCTmod);

			// burlex: fixed this crap properly

			if(dmg.full_damage < 0)
				dmg.full_damage = 1;
//--------------------------------check for special hits------------------------------------
			switch(r)
			{
			case 4:
				{
					vstate = BLOCK;
					aproc |= PROC_ON_BLOCK_VICTIM;
					blocked_damage = dmg.full_damage * 3 / 4;

					//MyLog::log->debug( "attacker:%s victim:%s [block] [%.2f%%]", attackerName.c_str(), victimName.c_str(), block );
				}
				break;
//--------------------------------critical hit----------------------------------------------
			case 5:
				{
					hit_status |= HITSTATUS_CRICTICAL;
					if (ability && ability->Spell_Dmg_Type ==  SPELL_DMG_TYPE_RANGED)
					{
						vproc |= PROC_ON_RANGED_CRIT_ATTACK_VICTIM;
						aproc |= PROC_ON_RANGED_CRIT_ATTACK;
					}
					else
					{
						vproc |= PROC_ON_CRIT_HIT_VICTIM;
						aproc |= PROC_ON_CRIT_ATTACK;
					}
					//dmg.full_damage = (int32)( (float)dmg.full_damage * ( 1.1f + crit / 100.f ) );
					/*if( weapon_damage_type == MELEE && ability )*/

					uint32 typeGroup;
					typeGroup	= 0;
					if (ability)
					{
						typeGroup = ability->SpellGroupType;
					}

					if( !exclusive_damage )
					{
						dmg.full_damage = CalculateDamage( this, pVictim, weapon_damage_type, typeGroup, ability , false, add_damage, bSchool, stato, weapon );
					}

					float critical_bonus = 2.f;
					if( typeGroup )
						SM_PFValue( SM_PCriticalDamage, &critical_bonus, typeGroup );

					dmg.full_damage *= critical_bonus;

					if(this->IsPlayer())
					{
						this->SetFlag(UNIT_FIELD_AURASTATE,AURASTATE_FLAG_CRITICAL);	//SB@L: Enables spells requiring critical strike
						if(!sEventMgr.HasEvent(this,EVENT_CRIT_FLAG_EXPIRE))
							sEventMgr.AddEvent((Unit*)this,&Unit::EventAurastateExpire,(uint32)AURASTATE_FLAG_CRITICAL,EVENT_CRIT_FLAG_EXPIRE,5000,1,0);
						else sEventMgr.ModifyEventTimeLeft(this,EVENT_CRIT_FLAG_EXPIRE,5000);
					}

					CALL_SCRIPT_EVENT(pVictim, OnTargetCritHit)(this, float(dmg.full_damage));
					CALL_SCRIPT_EVENT(this, OnCritHit)(pVictim, float(dmg.full_damage));
				}

				//MyLog::log->debug( "attacker:%s victim:%s [critical strike] [%.2f%%]", attackerName.c_str(), victimName.c_str(), crit );
				break;
//--------------------------------crushing blow---------------------------------------------
			case 6:
				hit_status |= HITSTATUS_CRUSHINGBLOW;
				dmg.full_damage = (dmg.full_damage * 3) >> 1;

				//MyLog::log->debug( "attacker:%s victim:%s strike crushing", attackerName.c_str(), victimName.c_str() );
				break;
//--------------------------------regular hit-----------------------------------------------
			default:
				break;
			}
//==========================================================================================
//==============================Post Roll Damage Processing=================================
//==========================================================================================
//--------------------------absorption------------------------------------------------------
			uint32 dm = dmg.full_damage;
			abs = pVictim->AbsorbDamage(dmg.school_type,(uint32*)&dm);

			if(dmg.full_damage > (int32)blocked_damage)
			{
				uint32 sh = pVictim->ManaShieldAbsorb(dmg.full_damage);
//--------------------------armor reducing--------------------------------------------------
				if(sh)
				{
					dmg.full_damage -= sh;
					if(dmg.full_damage && !disable_dR)
						CalculateResistanceReduction(pVictim,&dmg, ability);
					dmg.full_damage += sh;
					abs+=sh;
				}
				else if(!disable_dR)
					CalculateResistanceReduction(pVictim,&dmg, ability);
			}

			if(abs)
				vproc |= PROC_ON_ABSORB;

		/*	if (dmg.school_type == SCHOOL_NORMAL)
			{
				abs+=dmg.resisted_damage;
				dmg.resisted_damage=0;
			}*/

 			//if( !(pVictim->IsCreature() && static_cast<Creature*>( pVictim )->m_gardenmode) )
 			//{
 			//	Player* plr = this->IsPlayer() ? (Player*)this : NULL;
 			//	if( pVictim->IsPlayer() && plr )
 			//	{
 			//		float delta = 0.f;
 			//		if( plr->getLevel() > pVictim->getLevel() )
 			//			delta = plr->getLevel() - pVictim->getLevel();
 			//		if( delta > 10.f ) delta = 10.f;
 			//		dmg.full_damage = uint32( (float)dmg.full_damage * ( delta + 20.f ) / 20.f );
 			//	}
 			//	else if( pVictim->IsCreature() && plr )
 			//	{
 			//		float delta = 0.f;
 			//		if( plr->getLevel() > pVictim->getLevel() )
 			//			delta = plr->getLevel() - pVictim->getLevel();
 			//		if( delta > 10.f ) delta = 10.f;
 
 			//		dmg.full_damage = uint32( (float)dmg.full_damage * ( delta + 10.f ) / 10.f );
 			//	}
 			//}
			realdamage = dmg.full_damage-abs-dmg.resisted_damage-blocked_damage;
			if(realdamage < 0)
			{
				realdamage = 0;
				//vstate = IMMUNE;
				hit_status |= HITSTATUS_ABSORBED;
			}
		}
		break;
	}

//==========================================================================================
//==============================Post Roll Special Cases Processing==========================
//==========================================================================================
//------------------------------- Special Effects Processing
// Paladin: Blessing of Sacrifice, and Warlock: Soul Link
		if( !pVictim->m_damageSplitTargets.empty() )
		{
			std::list< DamageSplitTarget >::iterator itr;
			Unit * splittarget;
			uint32 splitdamage, tmpsplit;
			for( itr = pVictim->m_damageSplitTargets.begin() ; itr != pVictim->m_damageSplitTargets.end() ; itr ++ )
			{
				// TODO: Separate damage based on school.
				splittarget = pVictim->GetMapMgr() ? pVictim->GetMapMgr()->GetUnit( itr->m_target ) : NULL;
				if( splittarget && dmg.full_damage > 0 )
				{
					// calculate damage
					tmpsplit = itr->m_flatDamageSplit;
					if( tmpsplit > dmg.full_damage )
						tmpsplit = dmg.full_damage; // prevent < 0 damage
					splitdamage = tmpsplit;
					dmg.full_damage -= tmpsplit;
					tmpsplit = itr->m_pctDamageSplit * dmg.full_damage;
					if( tmpsplit > dmg.full_damage )
						tmpsplit = dmg.full_damage;
					splitdamage += tmpsplit;
					dmg.full_damage -= tmpsplit;
					// TODO: pct damage
					if( splitdamage )
					{
						pVictim->DealDamage( splittarget , splitdamage , 0 , 0 , 0 , false );
						// Send damage log
						pVictim->SendSpellNonMeleeDamageLog( pVictim , splittarget , 27148 , splitdamage , SCHOOL_NORMAL , 0 , 0 , true , 0 , 0 , true );
					}
				}
			}
			realdamage = dmg.full_damage;
		}
//--------------------------special states processing---------------------------------------
	if(pVictim->GetTypeId() == TYPEID_UNIT)
	{
		if(pVictim->GetAIInterface() && (pVictim->GetAIInterface()->getAIState()== STATE_EVADE ||
										(pVictim->GetAIInterface()->GetIsSoulLinked() && pVictim->GetAIInterface()->getSoullinkedWith() != this)))
		{
			vstate = EVADE;
			realdamage = 0;
			dmg.full_damage = 0;
			dmg.resisted_damage = 0;
		}
	}
	if(pVictim->GetTypeId() == TYPEID_PLAYER )
	{
		//dmg.resisted_damage = dmg.full_damage; //godmode
		if( static_cast< Player* >(pVictim)->GodModeCheat == true || static_cast< Player* >(pVictim)->IsIceBlocking() )
			vstate = IMMUNE;
	}
//--------------------------dirty fixes-----------------------------------------------------
	//vstate=1-wound,2-dodge,3-parry,4-interrupt,5-block,6-evade,7-immune,8-deflect
	// the above code was remade it for reasons : damage shield needs moslty same flags as handleproc + dual wield should proc too ?
	if( !disable_proc && weapon_damage_type != OFFHAND )
    {
		if (IsCreature())
		{
			AiEvents aievent = EVENT_NULL;
			switch (vstate)
			{
			case DODGE:
				{
					aievent = EVENT_DODGE;
				}break;

			case  PARRY:
				{
					aievent = EVENT_PARRY;
				}
				break;

			case BLOCK:
				{
					aievent = EVENT_BLOCK;
				}
				break;
			}
			if (aievent != EVENT_NULL)
			{
				GetAIInterface()->HandleEvent(aievent, pVictim, 0);
			}
		}

		this->HandleProc(aproc,pVictim, ability,realdamage,abs,(weapon_damage_type == OFFHAND) ? false : true ); //maybe using dmg.resisted_damage is better sometimes but then if using godmode dmg is resisted instead of absorbed....bad
		m_procCounter = 0;

		pVictim->HandleProc(vproc,this, ability,realdamage,abs,(weapon_damage_type == OFFHAND) ? false : true);
		pVictim->m_procCounter = 0;

		if(realdamage > 0)
		{
			pVictim->HandleProcDmgShield(vproc,this);
			HandleProcDmgShield(aproc,pVictim);
		}
	}
//--------------------------spells triggering-----------------------------------------------
	if(realdamage > 0 && ability == 0)
	{
		if( IsPlayer() && static_cast< Player* >( this )->m_onStrikeSpells.size() )
		{
			SpellCastTargets targets;
			targets.m_unitTarget = pVictim->GetGUID();
			targets.m_targetMask = 0x2;
			Spell* cspell;

			// Loop on hit spells, and strike with those.
			for( map< SpellEntry*, pair< uint32, uint32 > >::iterator itr = static_cast< Player* >( this )->m_onStrikeSpells.begin();
				itr != static_cast< Player* >( this )->m_onStrikeSpells.end(); ++itr )
			{
				if( itr->second.first )
				{
					// We have a *periodic* delayed spell.
					uint32 t = getMSTime();
					if( t > itr->second.second )  // Time expired
					{
						// Set new time
						itr->second.second = t + itr->second.first;
					}

					// Cast.
					cspell = new Spell(this, itr->first, true, NULL);
					cspell->prepare(&targets);
				}
				else
				{
					cspell = new Spell(this, itr->first, true, NULL);
					cspell->prepare(&targets);
				}
			}
		}

		if( IsPlayer() && static_cast< Player* >( this )->m_onStrikeSpellDmg.size() )
		{
			map< uint32, OnHitSpell >::iterator it2 = static_cast< Player* >( this )->m_onStrikeSpellDmg.begin();
			map< uint32, OnHitSpell >::iterator itr;
			uint32 min_dmg, max_dmg, range, dmg;
			for(; it2 != static_cast< Player* >( this )->m_onStrikeSpellDmg.end(); )
			{
				itr = it2;
				++it2;

				min_dmg = itr->second.mindmg;
				max_dmg = itr->second.maxdmg;
				range = min_dmg - max_dmg;
				dmg = min_dmg;
				if(range) range += RandomUInt(range);

				SpellNonMeleeDamageLog(pVictim, itr->second.spellid, dmg, true);
			}
		}

		// refresh judgements
		// TODO: find the opcode to refresh the aura or just remove it and re add it
		// rather than fuck with duration
		// DONE: Remove + readded it :P
		for( uint32 x = MAX_POSITIVE_AURAS; x <= MAX_AURAS; x++ )
		{
			if( pVictim->m_auras[x] != NULL && pVictim->m_auras[x]->GetUnitCaster() != NULL && pVictim->m_auras[x]->GetUnitCaster()->GetGUID() == GetGUID() && pVictim->m_auras[x]->GetSpellProto()->buffIndexType == SPELL_TYPE_INDEX_JUDGEMENT )
			{
				Aura * aur = pVictim->m_auras[x];
				SpellEntry * spinfo = aur->GetSpellProto();
				aur->Remove();
				Spell * sp = new Spell( this , spinfo , true , NULL );
				SpellCastTargets tgt;
				tgt.m_unitTarget = pVictim->GetGUID();
				sp->prepare( &tgt );
				/*pVictim->m_auras[x]->SetDuration( 20000 ); // 20 seconds?
				sEventMgr.ModifyEventTimeLeft( pVictim->m_auras[x], EVENT_AURA_REMOVE, 20000 );

				// We have to tell the target that the aura has been refreshed.
				if( pVictim->IsPlayer() )
				{
					WorldPacket data( 5 );
					data.SetOpcode( SMSG_UPDATE_AURA_DURATION );
					data << (uint8)pVictim->m_auras[x]->GetAuraSlot() << 20000;
					static_cast< Player* >( pVictim )->GetSession()->SendPacket( &data );
				}
				*/
				// However, there is also an opcode that tells the caster that the aura has been refreshed.
				// This isn't implemented anywhere else in the source, so I can't work on that part :P
				// (The 'cooldown' meter on the target frame that shows how long the aura has until expired does not get reset)=
				// I would say break; here, but apparently, one paladin can have multiple judgements on the target. No idea if this is blizzlike or not.
			}
		}

	}
	if (ability)
	{
		if (pVictim->SchoolImmunityList[ability->School])
		{
			vstate = IMMUNE;
		}
	}
	else if( pVictim->SchoolImmunityList[0] )
	{
		vstate = IMMUNE;
	}

	realdamage += pVictim->extradamage;
//==========================================================================================
//==============================Data Sending================================================
//==========================================================================================
	MSG_S2C::stAttacker_State_Update Msg;
	//0x4--dualwield,0x10 miss,0x20 absorbed,0x80 crit,0x4000 -glancing,0x8000-crushing
	//only for melee!

	if(vstate == IMMUNE)
	{
		dmg.full_damage = 0;
		realdamage = 0;
		hit_status = HITSTATUS_MISS;
	}

	if( !ability || (IsPlayer() && ability->Id == 5003))
	{
		if( dmg.full_damage > 0 )
		{
			if( dmg.full_damage == (int32)abs )
				hit_status |= HITSTATUS_ABSORBED;
			/*
			else if (dmg.full_damage <= (int32)dmg.resisted_damage)
			{
				hit_status |= HITSTATUS_RESIST;
				dmg.resisted_damage = dmg.full_damage;
			}
			*/
		}

		if( dmg.full_damage < 0 )
			dmg.full_damage = 1;

		if( realdamage < 0 )
			realdamage = 1;

		Msg.hitStats		= hit_status;
		Msg.attacker_guid	= GetNewGUID();
		Msg.victim_guid		=  pVictim->GetNewGUID();

		if( (uint32)realdamage > pVictim->extradamage )
			Msg.realdamage = realdamage - pVictim->extradamage;		 // Realdamage;
		else
			Msg.realdamage = realdamage;
		//data << (uint8)1;				   // Damage type counter / swing type

		Msg.damageschool    = g_spellSchoolConversionTable[dmg.school_type];				  // Damage school
		Msg.fdamage_full	= dmg.full_damage;	 // Damage float
		Msg.ndamage_full	= dmg.full_damage;	// Damage amount
		Msg.damage_abs		= abs;// Damage absorbed
		Msg.resisted_damage	= dmg.resisted_damage;				  // Damage resisted

		Msg.victim_stat		= vstate;			 // new victim state
		//data << (uint32)0x03e8;					// can be 0,1000 or -1
		Msg.blocked_damage	= blocked_damage;	 // Damage amount blocked
		Msg.extra_damage = pVictim->extradamage;
		Msg.damageSeq = pVictim->GetDamageSeq();
		//data << (uint32) 0;
		if (this->IsPet())
		{
			Pet* pPet = (Pet*)(this);
			Player* pPlayer= pPet->GetPetOwner();
			pPlayer->SendMessageToSet(Msg, true);
		}
		else
		{
			SendMessageToSet(Msg, this->IsPlayer());
		}
		
	}
	else
	{

		uint32 id = 0;
		if (ability)
		{
			id = ability->Id;
		}
		if( realdamage > 0 )//FIXME: add log for miss,block etc for ability and ranged
		{
			// here we send "dmg.resisted_damage" for "AbsorbedDamage", "0" for "ResistedDamage", and "false" for "PhysicalDamage" even though "School" is "SCHOOL_NORMAL"   o_O

			SendSpellNonMeleeDamageLog( this, pVictim, id, realdamage, dmg.school_type, abs, dmg.resisted_damage, false, blocked_damage, ( ( hit_status & HITSTATUS_CRICTICAL ) != 0 ), true );

					
		}
		else if( realdamage == 0 )
		{
			if( abs == 0 )
				SendSpellLog( this,pVictim,id, vstate == IMMUNE ? SPELL_LOG_IMMUNE : SPELL_LOG_RESIST );
			else
				SendSpellNonMeleeDamageLog( this, pVictim, id, realdamage, dmg.school_type, abs, dmg.resisted_damage, false, blocked_damage, ( ( hit_status & HITSTATUS_CRICTICAL ) != 0 ), true );
		}
		//FIXME: add log for miss,block etc for ability and ranged
		//example how it works
		//SendSpellLog(this,pVictim,ability->Id,SPELL_LOG_MISS);
	}

	pVictim->extradamage = 0;

//==========================================================================================
//==============================Damage Dealing==============================================
//==========================================================================================

	if(IsPlayer())
	{
		static_cast< Player* >( this )->m_inarmedtimeleft = 20000;
	}
	if(pVictim->IsPlayer())
	{
		static_cast< Player* >( pVictim )->m_inarmedtimeleft = 20000;
	}

	if(this->IsPlayer() && ability)
		static_cast< Player* >( this )->m_casted_amount[dmg.school_type]=(uint32)(realdamage+abs);
	if(realdamage)
	{
		if( ability )
			DealDamage(pVictim, realdamage, 0, targetEvent, ability->Id);
		else
			DealDamage(pVictim, realdamage, 0, targetEvent, 0);
		//pVictim->HandleProcDmgShield(PROC_ON_MELEE_ATTACK_VICTIM,this);
//		HandleProcDmgShield(PROC_ON_MELEE_ATTACK_VICTIM,pVictim);

		if(pVictim->GetCurrentSpell())
			pVictim->GetCurrentSpell()->AddTime(0);
	}
	else
	{
		// have to set attack target here otherwise it wont be set
		// because dealdamage is not called.
		//setAttackTarget(pVictim);

		// modified by gui
		//pVictim->CombatStatus.OnDamageDealt( this );
		if( pVictim->IsCreature() )
		{
			if( ability )
				pVictim->GetAIInterface()->AttackReaction( this, 1, ability->Id );
			else
				pVictim->GetAIInterface()->AttackReaction( this, 1, 0 );
		}
		else
			pVictim->CombatStatus.OnDamageDealt( this );
	}
//==========================================================================================
//==============================Post Damage Dealing Processing==============================
//==========================================================================================
//--------------------------durability processing-------------------------------------------
	if(pVictim->IsPlayer())
	{
		Player* ppVictim = (Player*)pVictim;
		if(ppVictim->GetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID) &&rand()%5==0)
		{
			//ppVictim->RemoveAura(ppVictim->m_MountSpellId);
			//ppVictim->m_MountSpellId = 0;
			//ppVictim->SetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID, 0);
		}
		static_cast< Player* >( pVictim )->GetItemInterface()->ReduceItemDurability();
		if( !this->IsPlayer() )
		{
//			Player *pr = static_cast< Player* >( pVictim );
// 			if( Rand( pr->GetSkillUpChance( SKILL_DEFENSE ) * sWorld.getRate( RATE_SKILLCHANCE ) ) )
// 			{
// 				pr->_AdvanceSkillLine( SKILL_DEFENSE, float2int32( 1.0f * sWorld.getRate(RATE_SKILLRATE)));
// 				pr->UpdateChances();
// 			}
		}
		else
		{
			 static_cast< Player* >( this )->GetItemInterface()->ReduceItemDurability();
		}
	}
	else
	{
		if(this->IsPlayer())//not pvp
		{
			static_cast< Player* >( this )->GetItemInterface()->ReduceItemDurability();
			//Player* pr = static_cast< Player* >( this );
			//if( Rand( pr->GetSkillUpChance( SubClassSkill) * sWorld.getRate( RATE_SKILLCHANCE ) ) )
			{
				//pr->_AdvanceSkillLine( SubClassSkill, float2int32( 1.0f * sWorld.getRate(RATE_SKILLRATE)));
				//pr->UpdateChances();
			}
		}
	}
	RemoveAurasByInterruptFlag(AURA_INTERRUPT_ON_START_ATTACK);
//--------------------------extra strikes processing----------------------------------------
	if(!m_extraAttackCounter)
	{
		int32 extra_attacks = m_extraattacks;
		m_extraAttackCounter = true;
		m_extraattacks = 0;

		while(extra_attacks > 0)
		{
			extra_attacks--;
			Strike( pVictim, weapon_damage_type, NULL, 0, 0, 0, false, false, false, false, true);
		}

		m_extraAttackCounter = false;
	}

	if(m_extrastriketargets)
	{
		int32 m_extra = m_extrastriketargets;
		int32 m_temp = m_extrastriketargets;
		m_extrastriketargets = 0;

		for(set<Object*>::iterator itr = m_objectsInRange.begin(); itr != m_objectsInRange.end() && m_extra; ++itr)
		{
			if(m_extra == 0)
				break;
			if (!(*itr) || (*itr) == pVictim || !(*itr)->IsUnit())
				continue;


			if(CalcDistance(*itr) < 10.0f && isAttackable(this, (*itr)) && (*itr)->isInFront(this) && !((Unit*)(*itr))->IsPacified())
			{
				Strike( static_cast< Unit* >( *itr ), weapon_damage_type, ability, add_damage, pct_dmg_mod, exclusive_damage, false ,false , false, false, true);
				--m_extra;
			}
		}
		m_extrastriketargets = m_temp;
	}

	return dmg.full_damage;
}


void Unit::smsg_AttackStop(Unit* pVictim)
{
	if(!pVictim)
		return;

	m_attackTimer_1 = -1;

	MSG_S2C::stAttack_Stop Msg;
	Msg.attacker_guid	= GetNewGUID();
	Msg.victim_guid		= pVictim->GetNewGUID();
	if(m_objectTypeId==TYPEID_PLAYER)
	{
		static_cast< Player* >( this )->GetSession()->SendPacket( Msg );
	}
	SendMessageToSet(Msg, true );
	// stop swinging, reset pvp timeout

	if( pVictim->IsPlayer() && IsPlayer() )
	{
		pVictim->CombatStatusHandler_ResetPvPTimeout();
		CombatStatusHandler_ResetPvPTimeout();
	}
	else
	{
		if(!IsPlayer() || getClass() == CLASS_ROGUE)
		{
			m_cTimer = getMSTime() + 5000;
			sEventMgr.RemoveEvents(this, EVENT_COMBAT_TIMER);
			sEventMgr.AddEvent(this, &Unit::EventUpdateFlag, EVENT_COMBAT_TIMER, 5000, 1, 0);
			sEventMgr.AddEvent(pVictim, &Unit::EventUpdateFlag, EVENT_COMBAT_TIMER, 5000, 1, 0);
		}
		else
		{
			pVictim->CombatStatus.RemoveAttacker(this, GetGUID());
			CombatStatus.RemoveAttackTarget(pVictim);
		}
	}
}

void Unit::smsg_AttackStop(uint64 victimGuid)
{
	MSG_S2C::stAttack_Stop Msg;
	Msg.attacker_guid	= GetNewGUID();
	Msg.victim_guid		= victimGuid;
	SendMessageToSet(Msg, IsPlayer());
	m_attackTimer_1 = -1;
	//MyLog::log->debug( "WORLD: attacker[%u] victim[%u] Sent smsg_AttackStop, isplayer?[%d]", Msg.attacker_guid, Msg.victim_guid, IsPlayer() );
}

void Unit::smsg_AttackStart(Unit* pVictim)
{

	// Prevent user from ignoring attack speed and stopping and start combat really really fast
	/*if(!isAttackReady())
		setAttackTimer(uint32(0));
	else if(!canReachWithAttack(pVictim))
	{
		setAttackTimer(uint32(500));
		//pThis->GetSession()->OutPacket(SMSG_ATTACKSWING_NOTINRANGE);
	}
	else if(!isInFront(pVictim))
	{
		setAttackTimer(uint32(500));
		//pThis->GetSession()->OutPacket(SMSG_ATTACKSWING_NOTINRANGE);
	}*/

	// Send out ATTACKSTART
	MSG_S2C::stAttack_Start Msg;
	Msg.attacker_guid	= GetGUID();
	Msg.victim_guid		= pVictim->GetGUID();
	Msg.x				= GetPosition().x;
	Msg.y				= GetPosition().y;
	Msg.z				= GetPosition().z;
	Msg.orientation		= GetPosition().o;
	SendMessageToSet(Msg, true);
	//MyLog::log->debug( "WORLD: attacker[%u] victim[%u] Sent smsg_AttackStart", Msg.attacker_guid, Msg.victim_guid );

	// FLAGS changed so other players see attack animation
	//	addUnitFlag(UNIT_FLAG_COMBAT);
	//	setUpdateMaskBit(UNIT_FIELD_FLAGS );
	if(GetTypeId() != TYPEID_PLAYER)
		return;

	Player* pThis = static_cast< Player* >( this );

	if(pThis->cannibalize)
	{
		sEventMgr.RemoveEvents(pThis, EVENT_CANNIBALIZE);
		pThis->SetUInt32Value(UNIT_NPC_EMOTESTATE, 0);
		pThis->cannibalize = false;
	}
}

void Unit::AddAura(Aura *aur)
{
	if( aur->GetSpellProto()->School && SchoolImmunityList[aur->GetSpellProto()->School] )
	{
		delete aur;
		return;
	}

	if (GetUInt32Value( UNIT_FIELD_DEATH_STATE) != ALIVE && !(aur->GetSpellProto()->Flags4 & CAN_PERSIST_AND_CASTED_WHILE_DEAD))
	{
		delete aur;
		return;
	}

	Object* caster = aur->GetCaster();
	if( caster )
	{
		Player* plr = NULL;
		if( caster->IsPet() )
		{
			plr = ((Pet*)caster)->GetPetOwner();
		}
		else if( caster->IsPlayer() )
		{
			plr = (Player*)caster;
		}
		Player* self = NULL;
		if( this->IsPet() )
		{
			self = ((Pet*)caster)->GetPetOwner();
		}
		else if( this->IsPlayer() )
		{
			self = (Player*)this;
		}

		if( plr && self )
		{
			self->CombatStatus.TouchWith( plr, isHostile( plr, self ) );
		}
	}

	int maxStack = aur->GetSpellProto()->maxstack;
	if( caster->IsUnit() )
		SM_FIValue( static_cast<Unit*>( caster )->SM_FStackCount, &maxStack, aur->GetSpellProto()->SpellGroupType );
	//if( aur->GetSpellProto()->procCharges > 0 )
	//	maxStack=aur->GetSpellProto()->procCharges;
	if( IsPlayer() && static_cast< Player* >( this )->stack_cheat )
		maxStack = 999;

	if( maxStack == 0 )
		maxStack = 1;

	SpellEntry * info = aur->GetSpellProto();
	//uint32 flag3 = aur->GetSpellProto()->Flags3;

	AuraCheckResponse acr;
	bool deleteAur = false;
	bool resistAur = false;

	int i = 0;
	int cceffect = -1;
	for (; i < 3; i ++)
	{
		if(deleteAur)
		{
			break;
		}
		Modifier * mod = &aur->m_modList[i];
		if (!mod)
		{
			continue;
		}
		if (mod->m_type == SPELL_AURA_MOD_FEAR || mod->m_type == SPELL_AURA_MOD_STUN ||
			mod->m_type == SPELL_AURA_MOD_ROOT || mod->m_type == SPELL_AURA_MOD_DECREASE_SPEED ||  mod->m_type == SPELL_AURA_MOD_SLEEP)
		{
			if( IsCreature() && static_cast<Creature*>( this )->m_is_castle_boss )
			{
				deleteAur = true;
			}
		}

		switch (mod->m_type )
		{
		case SPELL_AURA_MOD_SLEEP:
			{
				CheckMechanic(info, MECHANIC_ASLEEP, deleteAur, resistAur);

				cceffect = MSG_S2C::stCCResult::_SLEEP;
			}
			break;
		case SPELL_AURA_MOD_TAUNT:
			{

				CheckMechanic(info, MECHANIC_TAUNT, deleteAur, resistAur);

				cceffect = MSG_S2C::stCCResult::_TAUNT;
			}
			break;
		case SPELL_AURA_MOD_SILENCE:
			{
				CheckMechanic(info, MECHANIC_SILENCED, deleteAur, resistAur);
				cceffect = MSG_S2C::stCCResult::_SILENCE;
			}
			break;
		case SPELL_AURA_PERIODIC_DAMAGE:
			{
				if( this->IsIceBlocking() )
					deleteAur = true;
			}
			break;
		case SPELL_AURA_MOD_FEAR:
			{
				Unit* u_caster = aur->GetUnitCaster();

				if( GetTypeId() == TYPEID_UNIT && static_cast<Creature*>(this)->IsTotem() )
				{
					
					deleteAur = true;
				}

				//if( u_caster == NULL ) return;
				// Check Mechanic Immunity
				CheckMechanic(info, MECHANIC_FLEEING, deleteAur, resistAur);
				cceffect = MSG_S2C::stCCResult::_FEAR;
			}
			break;
		case SPELL_AURA_MOD_STUN:
			{
				if( this && !aur->IsPositive() )
				{
					CheckMechanic(info, MECHANIC_STUNNED, deleteAur, resistAur);
				}

				////if( this && !aur->IsPositive() && aur->m_spellProto->NameHash != SPELL_HASH_ICE_BLOCK ) // ice block stuns you, don't want our own spells to ignore stun effects
				////{
				////	bool dispel = false;
				////	if( ( aur->m_spellProto->MechanicsType == MECHANIC_CHARMED &&  MechanicsDispels[MECHANIC_CHARMED] )
				////		|| ( aur->m_spellProto->MechanicsType == MECHANIC_INCAPACIPATED && MechanicsDispels[MECHANIC_INCAPACIPATED] )

				////		|| ( aur->m_spellProto->MechanicsType == MECHANIC_SAPPED && MechanicsDispels[MECHANIC_SAPPED] )
				////		|| ( MechanicsDispels[MECHANIC_STUNNED] )
				////		)

				////	{
				////		dispel = true;

				////	}

				////	if (!dispel&&MechanicsDispelsCnt[MECHANIC_STUNNED])
				////	{
				////		MechanicsDispelsCnt[MECHANIC_STUNNED] --;

				////		dispel = true;
				////	}


				////	if ( dispel || (( aur->m_spellProto->MechanicsType == MECHANIC_CHARMED &&  Rand(MechanicsProbability[MECHANIC_CHARMED]) )
				////		|| ( aur->m_spellProto->MechanicsType == MECHANIC_INCAPACIPATED && Rand( MechanicsProbability[MECHANIC_INCAPACIPATED]) )
				////		|| ( aur->m_spellProto->MechanicsType == MECHANIC_SAPPED && Rand( MechanicsProbability[MECHANIC_SAPPED]) )
				////		|| Rand( MechanicsProbability[MECHANIC_STUNNED] )) )
				////	{
				////		deleteAur = true;
				////		if( !dispel )
				////			resistAur = true;

				////	}
				////	cceffect = MSG_S2C::stCCResult::_STUN;
				////}

				cceffect = MSG_S2C::stCCResult::_STUN;
			}
			break;

		case SPELL_AURA_MODIFY_HITCHANGE_PCT:
			{
				if (!IsPlayer())
				{
					deleteAur = true;
				}
			}
			break;

		case SPELL_AURA_MOD_ROOT:
			{
				CheckMechanic(info, MECHANIC_ROOTED, deleteAur, resistAur);
				
				cceffect = MSG_S2C::stCCResult::_ROOT;
			}
			break;

		case SPELL_AURA_MOD_DECREASE_SPEED:
			{
				CheckMechanic(info, MECHANIC_ENSNARED, deleteAur, resistAur);

				cceffect = MSG_S2C::stCCResult::_SLOW;
			}
			break;
		}
	}
	Unit* pcaster = aur->GetUnitCaster();
	if( deleteAur )
	{
		if( cceffect != -1 )
		{
			MSG_S2C::stCCResult ccmsg;
			ccmsg.result = resistAur ? MSG_S2C::stCCResult::_RESIST : MSG_S2C::stCCResult::_IMMUNE;
			ccmsg.effect = cceffect;
			ccmsg.spellid = aur->GetSpellId();
			ccmsg.casterid = aur->GetCasterGUID();
			ccmsg.victimid = GetGUID();
			if( this->IsPlayer() )
			{
				((Player*)this)->GetSession()->SendPacket( ccmsg );
			}
			if( pcaster && pcaster->IsPlayer() )
			{
				((Player*)pcaster)->GetSession()->SendPacket( ccmsg );
			}
		}
	}
	if( pcaster && pcaster->IsPlayer() && this->IsCreature() )
		this->GetAIInterface()->AttackReaction( pcaster, 1, aur->GetSpellId() );

	if( /*!aur->IsPassive() &&*/ !deleteAur)
	{
		//uint32 aurName = aur->GetSpellProto()->Name;
		//uint32 aurRank = aur->GetSpellProto()->Rank;

		//check if we already have this aura by this caster -> update duration
		// Nasty check for Blood Fury debuff (spell system based on namehashes is bs anyways)
		if( !info->always_apply )
		{
			uint32 f = 0;
			for( uint32 x = 0; x < MAX_AURAS+MAX_PASSIVE_AURAS; x++ )
			{
				if( m_auras[x] )
				{
					//if(	m_auras[x]->GetSpellProto()->Id != aur->GetSpellId() &&
					//	( aur->pSpellId != m_auras[x]->GetSpellProto()->Id ) //if this is a proc spell then it should not remove it's mother : test with combustion later
				///		)
					//if( 1 )
					//{
						// Check for auras by specific type.
						// Check for auras with the same name and a different rank.

						/*
						if(info->buffType > 0 && m_auras[x]->GetSpellProto()->buffType & info->buffType && maxStack == 0)
							deleteAur = HasAurasOfBuffType(info->buffType, aur->m_casterGuid,0);
						else
						{
							acr = AuraCheck(info->NameHash, info->RankNumber, m_auras[x],aur->GetCaster());
							if(acr.Error == AURA_CHECK_RESULT_HIGHER_BUFF_PRESENT)
								deleteAur = true;
							else if(acr.Error == AURA_CHECK_RESULT_LOWER_BUFF_PRESENT)
							{
								// remove the lower aura
								m_auras[x]->Remove();

								// no more checks on bad ptr
								continue;
							}
						}
						*/


					//}

					if( aur->GetSpellProto()->buffType & SPELL_TYPE_MAGE_INTEL || aur->GetSpellProto()->buffType & SPELL_TYPE_FORTITUDE )
					{
						acr = AuraCheck(info->SpellGroupType, info->RankNumber, m_auras[x],aur->GetCaster());
						if(acr.Error == AURA_CHECK_RESULT_HIGHER_BUFF_PRESENT)
							deleteAur = true;
						else if(acr.Error == AURA_CHECK_RESULT_LOWER_BUFF_PRESENT)
						{
							// remove the lower aura
							m_auras[x]->Remove();

							// no more checks on bad ptr
							continue;
						}
					}
					else if( m_auras[x]->GetSpellId() == aur->GetSpellId() ) // not the best formula to test this I know, but it works until we find a solution
					{
						if( /*aur->IsPositive() && */m_auras[x]->m_casterGuid != aur->m_casterGuid )
							continue;

						f++;

						if(maxStack >= 1)
						{
							aur->m_AuraCount =  m_auras[x]->m_AuraCount + 1;
							if (aur->m_AuraCount > maxStack)
							{
								aur->m_AuraCount = maxStack;
							}
							aur->ResetMod();

							//aur->ResetMod();

							sEventMgr.RemoveEvents(m_auras[x]);
							m_auras[x]->Remove();
						}
					}
					else if (aur->GetSpellProto()->SpellGroupType && m_auras[x]->GetSpellId() != aur->GetSpellId() && aur->GetCaster() == m_auras[x]->GetCaster())
					{

						acr = AuraCheck(info->SpellGroupType, info->RankNumber, m_auras[x] ,aur->GetCaster());
						if(acr.Error == AURA_CHECK_RESULT_HIGHER_BUFF_PRESENT)
							deleteAur = true;
						else if(acr.Error == AURA_CHECK_RESULT_LOWER_BUFF_PRESENT)
						{
							// remove the lower aura
							m_auras[x]->Remove();

							// no more checks on bad ptr
							continue;
						}
					}
				}
			}
		}
	}
	//else
	//{
	//	SpellEntry * info = aur->GetSpellProto();
	//	if( !info->always_apply )
	//	{
	//		for(uint32 x=MAX_AURAS;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
	//		{
	//			if( m_auras[x] )
	//			{
	//				if(	m_auras[x]->GetSpellProto()->Id != aur->GetSpellId() &&
	//					( aur->pSpellId != m_auras[x]->GetSpellProto()->Id ) //if this is a proc spell then it should not remove it's mother : test with combustion later
	//					)
	//				{
	//					// Check for auras by specific type.
	//					// Check for auras with the same name and a different rank.


	//					if (aur->GetSpellProto()->SpellGroupType && m_auras[x]->GetSpellProto()->SpellGroupType == aur->GetSpellProto()->SpellGroupType && maxStack == 1
	//						&& aur->GetCaster() == m_auras[x]->GetCaster())
	//					{

	//						acr = AuraCheck(info->SpellGroupType, info->RankNumber, m_auras[x],aur->GetCaster());
	//						if(acr.Error == AURA_CHECK_RESULT_HIGHER_BUFF_PRESENT)
	//							deleteAur = true;
	//						else if(acr.Error == AURA_CHECK_RESULT_LOWER_BUFF_PRESENT)
	//						{
	//							// remove the lower aura
	//							m_auras[x]->Remove();

	//							// no more checks on bad ptr
	//							continue;
	//						}
	//					}

	//					/*
	//					if(info->buffType > 0 && m_auras[x]->GetSpellProto()->buffType & info->buffType && maxStack == 0)
	//						deleteAur = HasAurasOfBuffType(info->buffType, aur->m_casterGuid,0);
	//					else
	//					{
	//						acr = AuraCheck(info->NameHash, info->RankNumber, m_auras[x],aur->GetCaster());
	//						if(acr.Error == AURA_CHECK_RESULT_HIGHER_BUFF_PRESENT)
	//							deleteAur = true;
	//						else if(acr.Error == AURA_CHECK_RESULT_LOWER_BUFF_PRESENT)
	//						{
	//							// remove the lower aura
	//							m_auras[x]->Remove();

	//							// no more checks on bad ptr
	//							continue;
	//						}
	//					}

	//					*/
	//				}
	//			}
	//		}
	//	}

	//	if(deleteAur)
	//	{
	//		sEventMgr.RemoveEvents(aur);
	//		delete aur;
	//		return;
	//	}
	//}

	if(deleteAur)
	{
		sEventMgr.RemoveEvents(aur);
		delete aur;
		return;
	}

	////////////////////////////////////////////////////////
	if(aur->GetSpellProto()->SpellGroupType && m_objectTypeId == TYPEID_PLAYER)
	{
		int32 speedmod=0;
		SM_FIValue(SM_FSpeedMod,&speedmod,aur->GetSpellProto()->SpellGroupType);
		if(speedmod)
		{
			m_speedModifier += speedmod;
			UpdateSpeed();
		}
	}
	////////////////////////////////////////////////////////

	if( aur->m_auraSlot != 0xffffffff )
		m_auras[aur->m_auraSlot] = NULL;

	aur->m_auraSlot=255;
	aur->ApplyModifiers(true);

	if(!aur->IsPassive())
	{
		aur->AddAuraVisual();
		if(aur->m_auraSlot==255)
		{
			//add to invisible slot
			for(uint32 x=MAX_AURAS;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
			{
				if(!m_auras[x])
				{
					m_auras[x]=aur;
					aur->m_auraSlot=x;
					break;
				}
			}
			if(aur->m_auraSlot == 255)
			{
				MyLog::log->error("Aura error in active aura. ");
				// for next loop.. lets kill the fucker
				aur->Remove();
				return;
			}

			// add visual
			AddAuraVisual(aur->GetSpellId(), 1, aur->IsPositive());
		}
		else
		{
			m_auras[aur->m_auraSlot]=aur;
		}
	}
	else
	{
        if((aur->m_spellProto->AttributesEx & 1024))
        {
            aur->AddAuraVisual();
        }
		for(uint32 x=MAX_AURAS;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
		{
			if(!m_auras[x])
			{
				m_auras[x]=aur;
				aur->m_auraSlot=x;
				break;
			}
		}
		if(aur->m_auraSlot==255)
		{
			MyLog::log->error("Aura error in passive aura. removing. SpellId: %u", aur->GetSpellProto()->Id);
			// for next loop.. lets kill the fucker
			aur->Remove();
			return;
		}
	}

	// We add 500ms here to allow for the last tick in DoT spells. This is a dirty hack, but at least it doesn't crash like my other method.
	// - Burlex
	if(aur->GetDuration() > 0 && aur->GetDuration() != -1)
	{
		sEventMgr.AddEvent(aur, &Aura::Remove, EVENT_AURA_REMOVE, aur->GetDuration() + 500, 1,
			EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT | EVENT_FLAG_DELETES_OBJECT);
	}

	aur->RelocateEvents();

	// Reaction from enemy AI
	if(!aur->IsPositive() && aur->IsCombatStateAffecting())	  // Creature
	{
		Unit * pCaster = aur->GetUnitCaster();
		if(pCaster && isAlive())
		{
			pCaster->CombatStatus.OnDamageDealt(this);

			if(m_objectTypeId == TYPEID_UNIT)
				m_aiInterface->AttackReaction(pCaster, 1, aur->GetSpellId());
		}
		/*if(isAlive() && CanAgroHash(aur->m_spellProto->NameHash)) //no threat for hunter's mark
		{
			Unit * pCaster = aur->GetUnitCaster();
			if(!pCaster) return;

			addAttacker(pCaster);

			GetAIInterface()->AttackReaction(pCaster, 1, aur->GetSpellId());
		}*/
	}
}

bool Unit::RemoveAura(Aura *aur)
{
	aur->Remove();
	return true;
}

bool Unit::RemoveAura(uint32 spellId)
{//this can be speed up, if we know passive \pos neg
	for(uint32 x=0;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellId()==spellId)
			{
				m_auras[x]->Remove();
				return true;
			}
		}
	}
	return false;
}


bool Unit::CheckMechanic(SpellEntry* se, MECHANICS en, bool& deleteAur, bool& resistAur)
{
	deleteAur = false;
	resistAur = false;

	bool dispel = false;
	if( MechanicsDispels[en] )
		dispel = true;
	if (MechanicsDispelsCnt[en] > 0&& MechanicsDispelsCnt[en] != -1)
	{
		MechanicsDispelsCnt[en] --;

		if (!MechanicsDispelsCnt[en] &&MechanicsDispelsAura[en])
		{
			RemoveAura(MechanicsDispelsAura[en]);
		}
		dispel = true;
	}
	if( dispel )
	{
		deleteAur = true;
		resistAur = false;
	}
	else if( !(se->field114 & SPELL_AVOID_MASK_RESIST) )
	{
		if( Rand( MechanicsProbability[en] ) )
		{
			resistAur = true;
			deleteAur = true;
			dispel = true;
		}
	}

	return dispel;
}

bool Unit::RemoveAuras(uint32 * SpellIds)
{
	if(!SpellIds || *SpellIds == 0)
		return false;

	uint32 x=0,y;
	bool res = false;
	for(;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
	{
		if(m_auras[x])
		{
			for(y=0;SpellIds[y] != 0;++y)
			{
				if(m_auras[x]->GetSpellId()==SpellIds[y])
				{
					m_auras[x]->Remove();
					res = true;
				}
			}
		}
	}
	return res;
}

bool Unit::RemoveAura(uint32 spellId, uint64 guid)
{
	for(uint32 x=0;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellId()==spellId && m_auras[x]->m_casterGuid == guid)
			{
				m_auras[x]->Remove();
				return true;
			}
		}
	}
	return false;
}

bool Unit::RemoveAuraByNameHash(uint32 namehash)
{
	for(uint32 x=0;x<MAX_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellProto()->NameHash==namehash)
			{
				m_auras[x]->Remove();
				return true;
			}
		}
	}
	return false;
}

bool Unit::RemoveAuraPosByNameHash(uint32 namehash)
{
	for(uint32 x=0;x<MAX_POSITIVE_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellProto()->NameHash==namehash)
			{
				m_auras[x]->Remove();
				return true;
			}
		}
	}
	return false;
}

bool Unit::RemoveAuraNegByNameHash(uint32 namehash)
{
	for(uint32 x=MAX_POSITIVE_AURAS;x<MAX_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellProto()->NameHash==namehash)
			{
				m_auras[x]->Remove();
				return true;
			}
		}
	}
	return false;
}

bool Unit::RemoveAllAuras(uint32 spellId, uint64 guid)
{
	bool res = false;
	for(uint32 x=0;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellId()==spellId)
			{
				if (!guid || m_auras[x]->m_casterGuid == guid)
				{
					m_auras[x]->Remove();
					res = true;
				}
			}
		}
	}
	return res;
}

void Unit::RemoveAllNegAuras()
{
	RemoveNegativeAuras();
}

void Unit::RemoveAllChannelAuras()
{
	for(uint32 x=0;x<MAX_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellProto()->ChannelInterruptFlags != 0)
			{
				Unit* caster = NULL;
				caster = m_auras[x]->GetUnitCaster();
				if(caster&&caster->GetCurrentSpell()&&caster->GetCurrentSpell()->m_spellInfo == m_auras[x]->GetSpellProto()
					&&m_auras[x]->GetSpellProto()->ChannelInterruptFlags&&caster->GetUInt64Value(UNIT_FIELD_CHANNEL_OBJECT) == GetGUID() )
				{
					caster->CancelSpell(NULL);
				}
			}
		}
	}
}

bool Unit::RemoveAllAuraByNameHash(uint32 namehash)
{
	bool res = false;
	for(uint32 x=0;x<MAX_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellProto()->NameHash==namehash)
			{
				m_auras[x]->Remove();
				res=true;
			}
		}
	}
	return res;
}

bool Unit::RemoveAllPosAuraByNameHash(uint32 namehash)
{
	bool res = false;
	for(uint32 x=0;x<MAX_POSITIVE_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellProto()->NameHash==namehash)
			{
				m_auras[x]->Remove();
				res=true;
			}
		}
	}
	return res;
}

bool Unit::RemoveAllNegAuraByNameHash(uint32 namehash)
{
	bool res = false;
	for(uint32 x=MAX_POSITIVE_AURAS;x<MAX_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellProto()->NameHash==namehash)
			{
				m_auras[x]->Remove();
				res=true;
			}
		}
	}
	return res;
}

void Unit::RemoveNegativeAuras()
{
	for(uint32 x=MAX_POSITIVE_AURAS;x<MAX_AURAS;x++)
	{
		if(m_auras[x])
		{
            if(m_auras[x]->GetSpellProto()->Flags4 & CAN_PERSIST_AND_CASTED_WHILE_DEAD)
                continue;
            else
            {
			    m_auras[x]->Remove();
            }
		}
	}
}

void Unit::RemoveAllAuras(bool bALL)
{
	for(uint32 x=0;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(!bALL && IsPlayer())
			{
				if(m_auras[x]->IsRemainWhenDead())
					continue;
			}
			if( !bALL )
			{
				if( m_auras[x]->GetSpellProto()->Attributes & ATTRIBUTES_PASSIVE )
					continue;
			}
			m_auras[x]->Remove();
		}
	}
}

//ex:to remove morph spells
void Unit::RemoveAllAuraType(uint32 auratype)
{
    for(uint32 x=0;x<MAX_AURAS;x++)
    {
		if(m_auras[x])
		{
			SpellEntry *proto=m_auras[x]->GetSpellProto();
			if(proto->EffectApplyAuraName[0]==auratype || proto->EffectApplyAuraName[1]==auratype || proto->EffectApplyAuraName[2]==auratype)
				RemoveAura(m_auras[x]->GetSpellId());//remove all morph auras containig to this spell (like wolf motph also gives speed)
		}
    }
}

bool Unit::SetAurDuration(uint32 spellId,Unit* caster,uint32 duration)
{
	Aura*aur=FindAura(spellId,caster->GetGUID());
	if(!aur)
		return false;
	aur->SetDuration(duration);
	sEventMgr.ModifyEventTimeLeft(aur, EVENT_AURA_REMOVE, duration);

	if(this->IsPlayer())
	{
		MSG_S2C::stAura_Set_Duration Msg;
		Msg.slot = (aur)->GetAuraSlot();
		Msg.duration = duration;
		Msg.count    = aur->m_AuraCount;
		static_cast< Player* >( this )->GetSession()->SendPacket( Msg );
	}

	MSG_S2C::stAura_Set_Single Msg;
	Msg.target_guid = GetNewGUID();
	Msg.slot		= aur->m_visualSlot;
	Msg.spellid		= spellId;
	Msg.duration1	= duration;
	Msg.duration2	= duration;
	Msg.count       = aur->m_AuraCount;
	SendMessageToSet(Msg,true);

	return true;
}

bool Unit::SetAurDuration(uint32 spellId,uint32 duration)
{
	Aura*aur=FindAura(spellId);

	if(!aur)
		return false;


	aur->SetDuration(duration);
	sEventMgr.ModifyEventTimeLeft(aur, EVENT_AURA_REMOVE, duration);

	if(this->IsPlayer())
	{
		MSG_S2C::stAura_Set_Duration Msg;
		Msg.slot = (aur)->GetAuraSlot();
		Msg.duration = duration;
		Msg.count   = aur->m_AuraCount;
		static_cast< Player* >( this )->GetSession()->SendPacket( Msg );
	}
	MSG_S2C::stAura_Set_Single Msg;
	Msg.target_guid = GetNewGUID();
	Msg.slot		= aur->m_visualSlot;
	Msg.count       = aur->m_AuraCount;
	Msg.spellid		= spellId;
	Msg.duration1	= duration;
	Msg.duration2	= duration;
	SendMessageToSet(Msg,true);

	return true;
}


Aura* Unit::FindAuraPosByNameHash(uint32 namehash)
{
	for(uint32 x=0;x<MAX_POSITIVE_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellProto()->NameHash==namehash)
			{
				return m_auras[x];
			}
		}
	}
	return NULL;
}

Aura* Unit::FindAura(uint32 spellId)
{
	for(uint32 x=0;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellId()==spellId)
			{
				return m_auras[x];
			}
		}
	}
	return NULL;
}

Aura* Unit::FindAura(uint32 spellId, uint64 guid)
{
	for(uint32 x=0;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellId() == spellId && m_auras[x]->m_casterGuid == guid)
			{
				return m_auras[x];
			}
		}
	}
	return NULL;
}

void Unit::_UpdateSpells( uint32 time )
{
	/* to avoid deleting the current spell */
	if(m_currentSpell != NULL)
	{
		m_spellsbusy=true;
		m_currentSpell->update(time);
		m_spellsbusy=false;
	}
}

void Unit::castSpell( Spell * pSpell )
{
	// check if we have a spell already casting etc
	if(m_currentSpell && pSpell != m_currentSpell)
	{
//		if(m_spellsbusy)
//		{
			// shouldn't really happen. but due to spell sytem bugs there are some cases where this can happen.
//			sEventMgr.AddEvent(this,&Unit::CancelSpell,m_currentSpell,EVENT_UNK,1,1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
//		}
//		else
			m_currentSpell->cancel();
	}

	m_currentSpell = pSpell;
	pLastSpell = pSpell->m_spellInfo;
}

int32 Unit::GetHealSpellBonus(Unit *pVictim, SpellEntry *spellInfo,int32 base_dmg, bool isdot, float overWriteStato)
{
	if (!pVictim)
	{
		return 0;
	}

	int32 bonus = 0;
	if( IsPlayer() )
	{
		float Res = (float)GetSpellDmgBonus(pVictim,spellInfo,base_dmg, isdot, true, overWriteStato); 
		

		for(uint32 a = 0; a < 6; a++)
			bonus += float2int32( static_cast< Player* >( this )->SpellHealDoneByAttribute[a][spellInfo->School] * static_cast< Player* >( this )->GetUInt32Value( UNIT_FIELD_STAT0 + a) );

		bonus += HealDoneMod[spellInfo->School] + pVictim->HealTakenMod[spellInfo->School];
		bonus = bonus + Res;
	}
	
	int add = ( bonus + base_dmg > 0 ) ? bonus + base_dmg : 0;

	if( !isdot )
		add += float2int32( add * ( pVictim->HealTakenPctMod[spellInfo->School]+ HealDonePctMod[spellInfo->School] / 100.0f));
	return (add -base_dmg);
	
}

int32 Unit::GetSpellDmgBonus(Unit *pVictim, SpellEntry *spellInfo,int32 base_dmg, bool isdot, bool bheal,  float overWriteStato)
{
	float stato_damage = 0;
	if (IsPlayer())
	{
		SpellCastTime *sd = dbcSpellCastTime.LookupEntry(spellInfo->CastingTimeIndex);
		switch (spellInfo->Spell_Dmg_Type)
		{
		case SPELL_DMG_TYPE_MAGIC:
			{
				stato_damage = GetFloatValue(UNIT_FIELD_SPELLDAMAGE);	

				if(spellInfo->SpellGroupType)
				{
					SM_FFValue(SM_FPenalty, &stato_damage, spellInfo->SpellGroupType);
					int sp_bonus_pct=0;
					SM_FIValue(SM_PPenalty, &sp_bonus_pct, spellInfo->SpellGroupType);
					stato_damage += stato_damage * sp_bonus_pct / 100;
				}
			}
			break;
		case  SPELL_DMG_TYPE_MELEE:
			{
				stato_damage = GetAP() / 14 * 3.3f;

			}
			break;
		case SPELL_DMG_TYPE_RANGED:
			{
				stato_damage = GetRAP() / 20 * 2.8f;
			}
		}
		if (spellInfo->Spell_Dmg_Type == SPELL_DMG_TYPE_MAGIC)
		{
			stato_damage = stato_damage/3;
		}
		
		if (sd && sd->CastTime)
		{
			stato_damage *= (float(sd->CastTime) / 1000);
		}

		if (isdot)
		{
			stato_damage = spellInfo->fixed_hotdotcoef * stato_damage;
		}
		else
		{
			stato_damage = spellInfo->fixed_dddhcoef * stato_damage;
		}

		stato_damage *= overWriteStato;
		if (bheal)
		{
			stato_damage *= 1.5f;
			return stato_damage;
		}
	
	}

	int32 plus_damage = 0;
	Unit* caster = this;
	uint32 school = spellInfo->School;

	if( spellInfo->c_is_flags & SPELL_FLAG_IS_NOT_USING_DMG_BONUS )
		return 0;

	if( caster->IsPlayer() )
	{
		for(uint32 a = 0; a < 6; a++)
			plus_damage += float2int32(static_cast< Player* >(caster)->SpellDmgDoneByAttribute[a][school] * float(caster->GetFloatValue(UNIT_FIELD_STAT0 + a)));
	}

	//------------------------------by school---------------------------------------------------
	plus_damage += caster->GetDamageDoneMod(school);
	plus_damage += pVictim->DamageTakenMod[school];
//------------------------------by victim type----------------------------------------------
	if(((Creature*)pVictim)->GetCreatureName() && caster->IsPlayer()&& !pVictim->IsPlayer())
		plus_damage += static_cast< Player* >(caster)->IncreaseDamageByType[((Creature*)pVictim)->GetCreatureName()->Type];
//==========================================================================================
//==============================+Spell Damage Bonus Modifications===========================
//==========================================================================================
//------------------------------by cast duration--------------------------------------------
	float dmgdoneaffectperc = 1.0f;
	if( spellInfo->Dspell_coef_override >= 0 && !isdot )
		plus_damage = float2int32( float( plus_damage ) * spellInfo->Dspell_coef_override );
	else if( spellInfo->OTspell_coef_override >= 0 && isdot )
		plus_damage = float2int32( float( plus_damage ) * spellInfo->OTspell_coef_override );
	else
	{
		/*
		//Bonus to DD part
		if( spellInfo->fixed_dddhcoef >= 0 && !isdot )
			plus_damage = float2int32( float( plus_damage ) * spellInfo->fixed_dddhcoef );
		//Bonus to DoT part
		else if( spellInfo->fixed_hotdotcoef >= 0 && isdot )
		{
			plus_damage = float2int32( float( plus_damage ) * spellInfo->fixed_hotdotcoef );
			if( caster->IsPlayer() )
			{
				int durmod = 0;
				SM_FIValue(caster->SM_FDur, &durmod, spellInfo->SpellGroupType);
				plus_damage += float2int32( float( plus_damage * durmod ) / 15000.0f );
			}
		}
		//In case we dont fit in previous cases do old thing
		else
		{
			plus_damage = float2int32( float( plus_damage ) * spellInfo->casttime_coef );
			float td = float( GetDuration( dbcSpellDuration.LookupEntry( spellInfo->DurationIndex ) ));
			if( spellInfo->NameHash == SPELL_HASH_MOONFIRE || spellInfo->NameHash == SPELL_HASH_IMMOLATE || spellInfo->NameHash == SPELL_HASH_ICE_LANCE || spellInfo->NameHash == SPELL_HASH_PYROBLAST )
				plus_damage = float2int32( float( plus_damage ) * float( 1.0f - ( ( td / 15000.0f ) / ( ( td / 15000.0f ) + dmgdoneaffectperc ) ) ) );
		}
		*/
	}
	plus_damage += stato_damage;

	//------------------------------by downranking----------------------------------------------
	//DOT-DD (Moonfire-Immolate-IceLance-Pyroblast)(Hack Fix)

	//if(spellInfo->baseLevel > 0 && spellInfo->maxLevel > 0)
	//{
	//	float downrank1 = 1.0f;
	//	if(spellInfo->baseLevel < 20)
	//	    downrank1 = 1.0f - (20.0f - float (spellInfo->baseLevel) ) * 0.0375f;
	//	float downrank2 = ( float(spellInfo->maxLevel + 5.0f) / float(static_cast< Player* >(caster)->getLevel()) );
	//	if(downrank2 >= 1 || downrank2 < 0)
	//	        downrank2 = 1.0f;
	//	dmgdoneaffectperc *= downrank1 * downrank2;
	//}
//==========================================================================================
//==============================Bonus Adding To Main Damage=================================
//==========================================================================================
	int32 bonus_damage = float2int32(plus_damage * dmgdoneaffectperc);

	//bonus_damage +=pVictim->DamageTakenMod[school]; Bad copy-past i guess :P
//------------------------------by school----------------------------------------------
	float summaryPCTmod = caster->GetDamageDonePctMod(school); //value is initialized with 1
	summaryPCTmod += pVictim->DamageTakenPctMod[school]-1 ;//value is initialized with 1
	summaryPCTmod += caster->DamageDoneModPCT[school]/ 100.f;	// BURLEX FIXME
	summaryPCTmod += pVictim->ModDamageTakenByMechPCT[spellInfo->MechanicsType];
	int32 res = (int32)((base_dmg+bonus_damage)*summaryPCTmod + bonus_damage); // 1.x*(base_dmg+bonus_damage) == 1.0*base_dmg + 1.0*bonus_damage + 0.x*(base_dmg+bonus_damage) -> we add the returned value to base damage so we do not add it here (function returns bonus only)
return res;
}

void Unit::InterruptSpell()
{
	if(m_currentSpell)
	{
		//ok wtf is this
		//m_currentSpell->SendInterrupted(SPELL_FAILED_INTERRUPTED);
		//m_currentSpell->cancel();
		if(m_spellsbusy)
		{
			// shouldn't really happen. but due to spell sytem bugs there are some cases where this can happen.
			sEventMgr.AddEvent(this,&Unit::CancelSpell,m_currentSpell,EVENT_UNK,1,1,EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
			m_currentSpell=NULL;
		}
		else
			m_currentSpell->cancel();
	}
}

void Unit::DeMorph()
{
	// hope it solves it :)
	uint32 displayid = this->GetUInt32Value(UNIT_FIELD_NATIVEDISPLAYID);
	this->SetUInt32Value(UNIT_FIELD_DISPLAYID, displayid);
}

void Unit::Emote(EmoteType emote)
{
	if(emote == 0)
		return;

	//MSG_S2C::stChat_Emote Msg;
	//Msg.guid = GetGUID();
	//Msg.emote = emote;
	//SendMessageToSet (Msg, true);
}

void Unit::SendChatMessageToPlayer(uint8 type, uint32 lang, const char *msg, Player *plr)
{
	CreatureInfo *ci = (m_objectTypeId == TYPEID_UNIT) ? ((Creature*)this)->creature_info : NULL;

	if(ci == NULL || plr == NULL)
		return;

	MSG_S2C::stChat_Message Msg;
	Msg.type	= type;
	Msg.guid	= GetGUID();
	Msg.creature_name = ci->Name;
	Msg.gm_tag	= 0;
	Msg.txt		= msg;
	Msg.flag	= 0;
	plr->GetSession()->SendPacket(Msg);
}

void Unit::SendChatMessageAlternateEntry(uint32 entry, uint8 type, uint32 lang, const char * msg)
{
	size_t UnitNameLength = 0, MessageLength = 0;
	CreatureInfo *ci;

	ci = CreatureNameStorage.LookupEntry(entry);
	if(!ci)
		return;

	UnitNameLength = strlen((char*)ci->Name) + 1;
	MessageLength = strlen((char*)msg) + 1;

	MSG_S2C::stChat_Message Msg;
	Msg.type	= type;
	Msg.guid	= GetGUID();
	Msg.creature_name = ci->Name;
	Msg.txt		= msg;
	Msg.gm_tag	= 0;
	SendMessageToSet(Msg, true);
}

int Unit::GetHealthPct()
{
	int hp = GetUInt32Value(UNIT_FIELD_HEALTH);
	int mhp = GetUInt32Value(UNIT_FIELD_MAXHEALTH);
	if( mhp == 0 )
		return 0;

	return (int)( hp * 100 / mhp);
}

void Unit::SendChatMessage(uint8 type, uint32 lang, const char *msg)
{
	if( !msg ) return;
	if( msg[0] == 0 ) return;

	size_t UnitNameLength = 0, MessageLength = 0;
	CreatureInfo *ci = (m_objectTypeId == TYPEID_UNIT) ? ((Creature*)this)->creature_info : NULL;

	if(ci == NULL)
		return;

	UnitNameLength = strlen((char*)ci->Name) + 1;
	MessageLength = strlen((char*)msg) + 1;

	MSG_S2C::stChat_Message Msg;
	Msg.type	= type;
	Msg.guid	= GetGUID();
	Msg.creature_name = ci->Name;
	Msg.gm_tag  = 0;
	Msg.txt		= msg;
	SendMessageToSet(Msg, true);
}

void Unit::WipeHateList()
{
	GetAIInterface()->WipeHateList();
}
void Unit::ClearHateList()
{
	GetAIInterface()->ClearHateList();
}

void Unit::WipeTargetList()
{
	GetAIInterface()->WipeTargetList();
}

void Unit::AddInRangeObject(Object* pObj)
{
	if((pObj->GetTypeId() == TYPEID_UNIT) || (pObj->GetTypeId() == TYPEID_PLAYER))
	{
		if( isHostile( this, (Unit*)pObj ) )
			m_oppFactsInRange.insert(pObj);

		// commented - this code won't work anyway due to objects getting added in range before they are created - burlex
		/*if( GetTypeId() == TYPEID_PLAYER )
		{
			if( static_cast< Player* >( this )->InGroup() )
			{
				if( pObj->GetTypeId() == TYPEID_UNIT )
				{
					if(((Creature*)pObj)->Tagged)
					{
						if( static_cast< Player* >( this )->GetGroup()->HasMember( pObj->GetMapMgr()->GetPlayer( (uint32)((Creature*)pObj)->TaggerGuid ) ) )
						{
							uint32 Flags;
							Flags = ((Creature*)pObj)->m_uint32Values[UNIT_DYNAMIC_FLAGS];
							Flags |= U_DYN_FLAG_TAPPED_BY_PLAYER;
							ByteBuffer buf1(500);
							((Creature*)pObj)->BuildFieldUpdatePacket(&buf1, UNIT_DYNAMIC_FLAGS, Flags);
							static_cast< Player* >( this )->PushUpdateData( &buf1, 1 );
						}
					}
				}
			}
		}		*/
	}

	Object::AddInRangeObject(pObj);
}//427

void Unit::OnRemoveInRangeObject(Object* pObj)
{
	m_oppFactsInRange.erase(pObj);

	if(pObj->GetTypeId() == TYPEID_UNIT || pObj->GetTypeId() == TYPEID_PLAYER)
	{
		/*if(m_useAI)*/

		Unit *pUnit = static_cast<Unit*>(pObj);
		GetAIInterface()->CheckTarget(pUnit);

		if(GetUInt64Value(UNIT_FIELD_CHARM) == pObj->GetGUID())
			if(m_currentSpell)
				m_currentSpell->cancel();

        Object::OnRemoveInRangeObject(pObj);
        if(critterPet == pObj)
		{
			critterPet->SafeDelete();
			critterPet = 0;
		}
	}
    else
    {
        Object::OnRemoveInRangeObject(pObj);
    }
}

void Unit::ClearInRangeSet()
{
	Object::ClearInRangeSet();
}

void Unit::SetCurrentSpell(Spell* cSpell)
{
	m_currentSpell = cSpell;
	if( this->IsCreature() )
	{
		Unit* target = GetAIInterface()->GetMostHated();
		GetAIInterface()->SetNextTarget( target );
	}
}

//Events
void Unit::EventAddEmote(EmoteType emote, uint32 time)
{
	m_oldEmote = GetUInt32Value(UNIT_NPC_EMOTESTATE);
	SetUInt32Value(UNIT_NPC_EMOTESTATE,emote);
	sEventMgr.AddEvent(this, &Creature::EmoteExpire, EVENT_UNIT_EMOTE, time, 1,0);
}

void Unit::EmoteExpire()
{
	SetUInt32Value(UNIT_NPC_EMOTESTATE, m_oldEmote);
	sEventMgr.RemoveEvents(this, EVENT_UNIT_EMOTE);
}


float Unit::GetResistance(uint32 type)
{
	return GetFloatValue(UNIT_FIELD_RESISTANCES+type);
}

void Unit::MoveToWaypoint(uint32 wp_id)
{
	if(this->m_useAI && this->GetAIInterface() != NULL)
	{
		AIInterface *ai = GetAIInterface();
		WayPoint *wp = ai->getWayPoint(wp_id);
		if(!wp)
		{
			MyLog::log->info("WARNING: Invalid WP specified in MoveToWaypoint.");
			return;
		}

		ai->m_currentWaypoint = wp_id;
		if(wp->flags!=0)
			ai->m_moveRun = true;
		ai->MoveTo(wp->x, wp->y, wp->z, 0);
	}
}

int32 Unit::GetDamageDoneMod(uint32 school)
{
	if( this->IsPlayer() )
	   return (int32)GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_POS + school ) - (int32)GetFloatValue( PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + school );
	else
	   return static_cast< Creature* >( this )->ModDamageDone[school];
}

float Unit::GetDamageDonePctMod(uint32 school)
{
   if(this->IsPlayer())
	   return m_floatValues[PLAYER_FIELD_MOD_DAMAGE_DONE_PCT+school];
	else
	   return ((Creature*)this)->ModDamageDonePct[school];
}

void Unit::CalcDamage()
{

	float r;
	float delta;
	float mult;
	//float basemp = (float)GetUInt32Value( UNIT_FIELD_BASE_MANA );
	//float basehp = (float)GetUInt32Value( UNIT_FIELD_BASE_HEALTH );
	//float a = ( basehp + 2.f * basemp ) / ( basehp + basemp );

	float ap_bonus = float(GetAP())/14000.0f;

	float bonus = ap_bonus*GetUInt32Value(UNIT_FIELD_BASEATTACKTIME);

	delta = float(((Creature*)this)->ModDamageDone[0]);
	mult = float(((Creature*)this)->ModDamageDonePct[0]);
	r = BaseDamage[0]*mult+delta+bonus;


	//r *= a;
	// give some diversity to pet damage instead of having a 77-78 damage range (as an example)
	SetFloatValue(UNIT_FIELD_MINDAMAGE,r > 0 ? ( IsPet() ? r * 0.9f : r ) : 0 );

	r = BaseDamage[1]*mult+delta+bonus;
	//r *= a;

	SetFloatValue(UNIT_FIELD_MAXDAMAGE, r > 0 ? ( IsPet() ? r * 1.1f : r ) : 0 );

	//	SetFloatValue(UNIT_FIELD_MINRANGEDDAMAGE,BaseRangedDamage[0]*mult+delta);
	//	SetFloatValue(UNIT_FIELD_MAXRANGEDDAMAGE,BaseRangedDamage[1]*mult+delta);


}

//returns absorbed dmg
uint32 Unit::ManaShieldAbsorb(uint32 dmg)
{
	if(!m_manashieldamt)
		return 0;
	//mana shield group->16. the only

	uint32 mana = GetUInt32Value(UNIT_FIELD_POWER1);
	int32 effectbonus = SM_PEffectBonus ? SM_PEffectBonus[16] : 0;

	int32 potential = (mana*50)/((100+effectbonus));
	if(potential>m_manashieldamt)
		potential = m_manashieldamt;

	if((int32)dmg<potential)
		potential = dmg;

	uint32 cost = (potential*(100+effectbonus))/50;

	SetUInt32Value(UNIT_FIELD_POWER1,mana-cost);
	m_manashieldamt -= potential;
	if(!m_manashieldamt)
		RemoveAura(m_manaShieldId);
	return potential;
}

// grep: Remove any AA spells that aren't owned by this player.
//		 Otherwise, if he logs out and in and joins another group,
//		 he'll apply it to them.

void Unit::RemoveAllAreaAuras(bool SelfCast)
{



	for(int i = 0; i < MAX_AURAS+MAX_PASSIVE_AURAS; i ++)
	{
		if(m_auras[i] )
		{
			if(m_auras[i]->m_spellProto->Effect[0] == SPELL_EFFECT_APPLY_AREA_AURA ||
				m_auras[i]->m_spellProto->Effect[1] == SPELL_EFFECT_APPLY_AREA_AURA ||
				m_auras[i]->m_spellProto->Effect[2] == SPELL_EFFECT_APPLY_AREA_AURA)
			{
				bool bRemove = false;

				if (SelfCast)
				{
					if (m_auras[i]->GetCaster() == this)
					{
						bRemove = true;
					}
				}
				else
				{
					if (m_auras[i]->GetCaster() != this)
					{
						bRemove = false;
					}									
				}
				
				if (bRemove)
				{
					if (sEventMgr.HasEvent(m_auras[i], EVENT_AREAAURA_UPDATE))
					{
						sEventMgr.RemoveEvents(m_auras[i], EVENT_AREAAURA_UPDATE);
					}
					m_auras[i]->Remove();
				}
				
				
			}
		
		}
			
	}
	//m_auras[MAX_AURAS+MAX_PASSIVE_AURAS]

	//AuraList::iterator itr,it1;
	//for(itr = m_auras.begin();itr!=m_auras.end();)
	//{
	//	it1 = itr++;
	//	if(((*it1)->m_spellProto->Effect[0] == SPELL_EFFECT_APPLY_AREA_AURA ||
	//		(*it1)->m_spellProto->Effect[1] == SPELL_EFFECT_APPLY_AREA_AURA ||
	//		(*it1)->m_spellProto->Effect[2] == SPELL_EFFECT_APPLY_AREA_AURA) && (*it1)->GetCaster() != this)
	//	{
	//		(*it1)->Remove();
	//		m_auras.erase(it1);
	//	}
	//}
}

uint32 Unit::AbsorbDamage( uint32 School, uint32* dmg )
{
	if( dmg == NULL )
		return 0;

	if( School > 6 )
		return 0;
	uint32 abs = 0;
	SchoolAbsorb::iterator it = AllAbsorb.begin();
	SchoolAbsorb::iterator jt;
	for(; it != AllAbsorb.end();)
	{
		if( (int32)(*dmg) >= (*it)->amt)//remove this absorb
		{
			(*dmg) -= (*it)->amt;
			abs += (*it)->amt;
			jt = it++;

			if( it !=AllAbsorb.end() )
			{
				while( (*it)->spellid == (*jt)->spellid )
				{
					++it;
					if( it == AllAbsorb.end() )
						break;
				}
			}

			this->RemoveAura((*jt)->spellid); //,(*j)->caster);
			if(!*dmg)//absorbed all dmg
				break;
		}
		else //absorb full dmg
		{
			abs += *dmg;
			(*it)->amt -= *dmg;
			*dmg=0;
			break;
		}
	}

	SchoolAbsorb::iterator i, j;

	for( i = Absorbs[School].begin(); i != Absorbs[School].end(); )
	{
		if( (int32)(*dmg) >= (*i)->amt)//remove this absorb
		{
			(*dmg) -= (*i)->amt;
			abs += (*i)->amt;
			j = i++;

			if( i != Absorbs[School].end() )
			{
				while( (*i)->spellid == (*j)->spellid )
				{
					++i;
					if( i == Absorbs[School].end() )
						break;
				}
			}

			this->RemoveAura((*j)->spellid); //,(*j)->caster);
			if(!*dmg)//absorbed all dmg
				break;
		}
		else //absorb full dmg
		{
			abs += *dmg;
			(*i)->amt -= *dmg;
			*dmg=0;
			break;
		}
	}

	return abs;
}

bool Unit::setDetectRangeMod(uint64 guid, int32 amount)
{
	int next_free_slot = -1;
	for(int i = 0; i < 5; i++)
	{
		if(m_detectRangeGUID[i] == 0 && next_free_slot == -1)
		{
			next_free_slot = i;
		}
		if(m_detectRangeGUID[i] == guid)
		{
			m_detectRangeMOD[i] = amount;
			return true;
		}
	}
	if(next_free_slot != -1)
	{
		m_detectRangeGUID[next_free_slot] = guid;
		m_detectRangeMOD[next_free_slot] = amount;
		return true;
	}
	return false;
}

void Unit::unsetDetectRangeMod(uint64 guid)
{
	for(int i = 0; i < 5; i++)
	{
		if(m_detectRangeGUID[i] == guid)
		{
			m_detectRangeGUID[i] = 0;
			m_detectRangeMOD[i] = 0;
		}
	}
}

int32 Unit::getDetectRangeMod(uint64 guid)
{
	for(int i = 0; i < 5; i++)
	{
		if(m_detectRangeGUID[i] == guid)
		{
			return m_detectRangeMOD[i];
		}
	}
	return 0;
}

void Unit::SetStandState(uint8 standstate)
{
	SetByte( UNIT_FIELD_BYTES_1, 0, standstate );
	if( standstate == STANDSTATE_STAND )//standup
		RemoveAurasByInterruptFlag(AURA_INTERRUPT_ON_STAND_UP);

	if( m_objectTypeId == TYPEID_PLAYER )
		static_cast< Player* >( this )->GetSession()->OutPacket( SMSG_STANDSTATE_CHANGE_ACK, 1, &standstate );
}

void Unit::RemoveAurasByInterruptFlag(uint32 flag)
{
	Aura * a;
	for(uint32 x=0;x<MAX_AURAS;x++)
	{
		a = m_auras[x];
		if(a == NULL)
			continue;

		//some spells do not get removed all the time only at specific intervals
		if((a->m_spellProto->AuraInterruptFlags & flag) && !(a->m_spellProto->procFlags & PROC_REMOVEONUSE))
		{
			a->Remove();
			m_auras[x] = NULL;
		}
	}
}

bool Unit::HasAuraVisual(uint32 visualid)
{
	for(uint32 x=0;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
	if(m_auras[x] && m_auras[x]->GetSpellProto()->SpellVisual==visualid)
	{
		return true;
	}

	return false;
}

bool Unit::HasAura(uint32 spellid)
{
	for(uint32 x=0;x<MAX_AURAS+MAX_PASSIVE_AURAS;x++)
		if(m_auras[x] && m_auras[x]->GetSpellId()== spellid)
		{
			return true;
		}

		return false;
}


void Unit::DropAurasOnDeath()
{
	for(uint32 x=0;x<MAX_AURAS;x++)
    {
        if(m_auras[x])
        {
            if(m_auras[x] && m_auras[x]->GetSpellProto()->Flags4 & CAN_PERSIST_AND_CASTED_WHILE_DEAD)
                continue;
			else if(m_auras[x] && m_auras[x]->IsRemainWhenDead())
				continue;
            else
	        {
		        m_auras[x]->Remove();
	        }
        }
    }
}

void Unit::UpdateSpeed()
{
	//if(GetUInt32Value(UNIT_FIELD_MOUNTDISPLAYID) == 0)
	{
		m_runSpeed = m_base_runSpeed*(1.0f + ((float)m_speedModifier)/100.0f);
	}
	//else
	{
	//	m_runSpeed = m_base_runSpeed*(1.0f + ((float)m_mountedspeedModifier)/100.0f);
	//	m_runSpeed += (m_speedModifier<0) ? (m_base_runSpeed*((float)m_speedModifier)/100.0f) : 0;	
	}

	if( IsPlayer() )
	{
		m_backWalkSpeed = 4.5f / 7.f * m_runSpeed;
	}

	m_flySpeed = PLAYER_NORMAL_FLIGHT_SPEED*(1.0f + ((float)m_flyspeedModifier)/100.0f);

	if(IsPlayer())
	{
		if(((Player*)this)->m_changingMaps)
			static_cast< Player* >( this )->resend_speed = true;
		else
		{
			static_cast< Player* >( this )->SetPlayerSpeed(RUN, m_runSpeed);
			static_cast< Player* >( this )->SetPlayerSpeed(FLY, m_flySpeed);
			static_cast< Player* >( this )->SetPlayerSpeed(RUNBACK, m_backWalkSpeed);
		}
	}
}

bool Unit::HasActiveAura(uint32 spellid)
{
	for(uint32 x=0;x<MAX_AURAS;x++)
	if(m_auras[x] && m_auras[x]->GetSpellId()==spellid)
	{
		return true;
	}

	return false;
}

bool Unit::HasActiveAura(uint32 spellid,uint64 guid)
{
	for(uint32 x=0;x<MAX_AURAS;x++)
	if(m_auras[x] && m_auras[x]->GetSpellId()==spellid && m_auras[x]->m_casterGuid==guid)
	{
		return true;
	}

	return false;
}

void Unit::EventSummonPetExpire()
{
	if(summonPet)
	{
		if(summonPet->GetEntry() == 7915)//Goblin Bomb
		{
			SpellEntry *spInfo = dbcSpell.LookupEntry(13259);
			if(!spInfo)
				return;

			Spell*sp=new Spell(summonPet,spInfo,true,NULL);
			SpellCastTargets tgt;
			tgt.m_unitTarget=summonPet->GetGUID();
			sp->prepare(&tgt);
		}
		else
		{
			summonPet->RemoveFromWorld(false, true);
			delete summonPet;
			summonPet = NULL;
		}
	}
	sEventMgr.RemoveEvents(this, EVENT_SUMMON_PET_EXPIRE);
}

void Unit::CastSpell(Unit* Target, SpellEntry* Sp, bool triggered)
{
	if( Sp == NULL )
		return;

	if( m_currentSpell )
	{
		m_currentSpell->cancel();
		m_currentSpell = NULL;
	}

	Spell *newSpell = new Spell(this, Sp, triggered, 0);
	SpellCastTargets targets(0);
	if(Target)
	{
		targets.m_unitTarget |= TARGET_FLAG_UNIT;
		targets.m_unitTarget = Target->GetGUID();
	}
	else
	{
		newSpell->GenerateTargets(&targets);
	}
	newSpell->prepare(&targets);
}

void Unit::CastSpell(Unit* Target, uint32 SpellID, bool triggered)
{
	SpellEntry * ent = dbcSpell.LookupEntry(SpellID);
	if(ent == 0) return;

	CastSpell(Target, ent, triggered);
}

void Unit::CastSpell(uint64 targetGuid, SpellEntry* Sp, bool triggered)
{
	if( Sp == NULL )
		return;

	if( m_currentSpell )
	{
		m_currentSpell->cancel();
		m_currentSpell = NULL;
	}

	SpellCastTargets targets(targetGuid);
	Spell *newSpell = new Spell(this, Sp, triggered, 0);
	newSpell->prepare(&targets);
}

void Unit::CastSpell(uint64 targetGuid, uint32 SpellID, bool triggered)
{
	SpellEntry * ent = dbcSpell.LookupEntry(SpellID);
	if(ent == 0) return;

	CastSpell(targetGuid, ent, triggered);
}

void Unit::CastSpellAoFSource( SpellCastTargets& targets, SpellEntry* Sp, bool triggered)
//void Unit::CastSpellAoFSource(float x,float y,float z,SpellEntry* Sp, bool triggered)
{
	if( Sp == NULL )
		return;
	if( m_currentSpell )
	{
		m_currentSpell->cancel();
		m_currentSpell = NULL;
	}

	/*
	SpellCastTargets targets;
	targets.m_destX = x;
	targets.m_destY = y;
	targets.m_destZ = z;
	targets.m_targetMask=TARGET_FLAG_SOURCE_LOCATION;
	*/
	targets.m_destX = targets.m_srcX;
	targets.m_destY = targets.m_srcY;
	targets.m_destZ = targets.m_srcZ;
	Spell *newSpell = new Spell(this, Sp, triggered, 0);
	newSpell->prepare(&targets);
}

void Unit::CastSpellAoFDest( SpellCastTargets& targets, SpellEntry* Sp, bool triggered)
//void Unit::CastSpellAoFDest(float x,float y,float z,SpellEntry* Sp, bool triggered)
{
	if( Sp == NULL )
		return;

	if( m_currentSpell )
	{
		m_currentSpell->cancel();
		m_currentSpell = NULL;
	}

	/*
	SpellCastTargets targets;
	targets.m_destX = targets.m_;
	targets.m_destY = y;
	targets.m_destZ = z;
	targets.m_targetMask=TARGET_FLAG_DEST_LOCATION;
	*/
	Spell *newSpell = new Spell(this, Sp, triggered, 0);
	newSpell->prepare(&targets);
}

void Unit::PlaySpellVisual(uint64 target, uint32 spellVisual)
{
	MSG_S2C::stSpell_Play_Visual Msg;
	Msg.target_guid		= target;
	Msg.visualid		= spellVisual;
	SendMessageToSet(Msg, true);
}

void Unit::Storm()
{
	m_stormed ++;

	if (m_stormed == 1)
	{
		this->m_special_state |= UNIT_STATE_STORM;
		SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM);

// 		MechanicsDispels[MECHANIC_STUNNED] = 1;
// 		MechanicsDispels[MECHANIC_ENSNARED] = 1;
// 		MechanicsDispels[MECHANIC_ROOTED] = 1;
// 		MechanicsDispels[MECHANIC_FLEEING] = 1;
// 		MechanicsDispels[MECHANIC_CHARMED] = 1;
// 		MechanicsDispels[MECHANIC_FROZEN] = 1;
	}
}

void Unit::Unstorm()
{
	m_stormed --;

	if (m_stormed <= 0)
	{
		this->m_special_state &= ~UNIT_STATE_STORM;
		RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STORM);

// 		MechanicsDispels[MECHANIC_STUNNED] = 0;
// 		MechanicsDispels[MECHANIC_ENSNARED] = 0;
// 		MechanicsDispels[MECHANIC_ROOTED] = 0;
// 		MechanicsDispels[MECHANIC_FLEEING] = 0;
// 		MechanicsDispels[MECHANIC_CHARMED] = 0;
// 		MechanicsDispels[MECHANIC_FROZEN] = 0;
	}
}

void Unit::Root( bool Force )
{
	this->m_special_state |= UNIT_STATE_ROOT;

	if(m_objectTypeId == TYPEID_PLAYER)
	{
		static_cast< Player* >( this )->SetMovement(MOVE_ROOT, 1);
	}
	else
	{
		m_aiInterface->m_canMove = false;
		m_aiInterface->StopMovement(1);
	}

	SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ROOT);

	if( Force )
		m_ForceRoot = true;
}

void Unit::Unroot( bool Force )
{
	this->m_special_state &= ~UNIT_STATE_ROOT;

	if(m_objectTypeId == TYPEID_PLAYER)
	{
		static_cast< Player* >( this )->SetMovement(MOVE_UNROOT, 5);
	}
	else
	{
		m_aiInterface->m_canMove = true;
	}
	RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_ROOT);

	if( Force )
		m_ForceRoot = false;
}

void Unit::RemoveAurasByBuffType(uint32 buff_type, const uint64 &guid, uint32 skip)
{
	uint64 sguid = buff_type >= SPELL_TYPE_BLESSING ? guid : 0;

	for(uint32 x=0;x<MAX_AURAS;x++)
	{
		if(m_auras[x] && m_auras[x]->GetSpellProto()->buffType & buff_type && m_auras[x]->GetSpellId()!=skip)
			if(!sguid || (sguid && m_auras[x]->m_casterGuid == sguid))
				m_auras[x]->Remove();
	}
}

void Unit::RemoveAurasByBuffIndexType(uint32 buff_index_type, const uint64 &guid)
{
	for(uint32 x=0;x<MAX_AURAS;x++)
	{
		if(m_auras[x] && m_auras[x]->GetSpellProto()->buffIndexType == buff_index_type)
			if(!guid || (guid && m_auras[x]->m_casterGuid == guid))
				m_auras[x]->Remove();
	}
}

bool Unit::HasAurasOfBuffType(uint32 buff_type, const uint64 &guid,uint32 skip)
{
	uint64 sguid = (buff_type == SPELL_TYPE_BLESSING || buff_type == SPELL_TYPE_WARRIOR_SHOUT) ? guid : 0;

	for(uint32 x=0;x<MAX_AURAS;x++)
	{
		if(m_auras[x] && m_auras[x]->GetSpellProto()->buffType & buff_type && m_auras[x]->GetSpellId()!=skip)
			if(!sguid || (m_auras[x]->m_casterGuid == sguid))
				return true;
	}

	return false;
}

AuraCheckResponse Unit::AuraCheck(uint32 name_hash, uint32 rank, Object *caster)
{
	AuraCheckResponse resp;

	// no error for now
	resp.Error = AURA_CHECK_RESULT_NONE;
	resp.Misc  = 0;

	// look for spells with same namehash
	for(uint32 x=0;x<MAX_AURAS;x++)
	{
//		if(m_auras[x] && m_auras[x]->GetSpellProto()->NameHash == name_hash && m_auras[x]->GetCaster()==caster)
		if(m_auras[x] && m_auras[x]->GetSpellProto()->NameHash == name_hash)
		{
			// we've got an aura with the same name as the one we're trying to apply
			resp.Misc = m_auras[x]->GetSpellProto()->Id;

			// compare the rank to our applying spell
			if(m_auras[x]->GetSpellProto()->RankNumber > rank)
				resp.Error = AURA_CHECK_RESULT_HIGHER_BUFF_PRESENT;
			else
				resp.Error = AURA_CHECK_RESULT_LOWER_BUFF_PRESENT;

			// we found something, save some loops and exit
			break;
		}
	}

	// return it back to our caller
	return resp;
}

AuraCheckResponse Unit::AuraCheck(uint32 name_hash, uint32 rank, Aura* aur, Object *caster)
{
	AuraCheckResponse resp;

	// no error for now
	resp.Error = AURA_CHECK_RESULT_NONE;
	resp.Misc  = 0;

	// look for spells with same namehash
//	if(aur->GetSpellProto()->NameHash == name_hash && aur->GetCaster()==caster)
	if(aur->GetSpellProto()->SpellGroupType == name_hash)
	{
		// we've got an aura with the same name as the one we're trying to apply
		resp.Misc = aur->GetSpellProto()->Id;

		// compare the rank to our applying spell
		if(aur->GetSpellProto()->RankNumber > rank)
			resp.Error = AURA_CHECK_RESULT_HIGHER_BUFF_PRESENT;
		else
			resp.Error = AURA_CHECK_RESULT_LOWER_BUFF_PRESENT;
	}

	// return it back to our caller
	return resp;
}

void Unit::OnPushToWorld()
{
	for(uint32 x = 0; x < MAX_AURAS+MAX_PASSIVE_AURAS; ++x)
	{
		if(m_auras[x] != 0)
			m_auras[x]->RelocateEvents();
	}
}

void Unit::RemoveFromWorld(bool free_guid)
{
	CombatStatus.OnRemoveFromWorld();
	if(critterPet != 0)
	{
		critterPet->RemoveFromWorld(false, true);
		delete critterPet;
		critterPet = 0;
	}

	if(dynObj != 0)
		dynObj->Remove();

	for(uint32 i = 0; i < 4; ++i)
	{
		if(m_ObjectSlots[i] != 0)
		{
			GameObject * obj = m_mapMgr->GetGameObject(m_ObjectSlots[i]);
			if(obj)
				obj->ExpireAndDelete();

			m_ObjectSlots[i] = 0;
		}
	}

	Object::RemoveFromWorld(free_guid);


	for(uint32 x = 0; x < MAX_AURAS+MAX_PASSIVE_AURAS; ++x)
	{
		if(m_auras[x] != 0)
		{
			if( m_auras[x]->m_deleted )
			{
				m_auras[x] = NULL;
				continue;
			}

			m_auras[x]->RelocateEvents();
		}
	}
	m_aiInterface->WipeReferences();
}

void Unit::RemoveAurasByInterruptFlagButSkip(uint32 flag, uint32 skip)
{
	Aura * a;
	for(uint32 x=0;x<MAX_AURAS;x++)
	{
		a = m_auras[x];
		if(a == 0)
			continue;

		//some spells do not get removed all the time only at specific intervals
		if((a->m_spellProto->AuraInterruptFlags & flag) && (a->m_spellProto->Id != skip) && a->m_spellProto->proc_interval==0)
		{
			//the black sheeps of sociaty
			if(a->m_spellProto->AuraInterruptFlags & AURA_INTERRUPT_ON_CAST_SPELL)
			{
				switch(a->GetSpellProto()->Id)
				{
					//priest - surge of light
					case 33151:
						{
							//our luck. it got trigered on smite..we do not remove it just yet
							if( m_currentSpell && m_currentSpell->m_spellInfo->NameHash == SPELL_HASH_SMITE )
								continue;

							//this spell gets removed only when casting smite
						    SpellEntry *spi = dbcSpell.LookupEntry( skip );
							if( spi && spi->NameHash != SPELL_HASH_SMITE )
								continue;
						}break;
					case 34936:	// Backlash
						{
							if( m_currentSpell && m_currentSpell->m_spellInfo->NameHash == SPELL_HASH_SHADOW_BOLT )
								continue;
							if( m_currentSpell && m_currentSpell->m_spellInfo->NameHash == SPELL_HASH_INCINERATE )
								continue;
							SpellEntry *spi = dbcSpell.LookupEntry( skip );
							if( spi && spi->NameHash != SPELL_HASH_SHADOW_BOLT && spi->NameHash != SPELL_HASH_INCINERATE )
								continue;
						}break;

					case 17941: //Shadow Trance
						{
							if( m_currentSpell && m_currentSpell->m_spellInfo->NameHash == SPELL_HASH_SHADOW_BOLT)
								continue;
							SpellEntry *spi = dbcSpell.LookupEntry( skip );
							if( spi && spi->NameHash != SPELL_HASH_SHADOW_BOLT )
								continue;
						}break;
				}
			}
			a->Remove();
		}
	}
}

int Unit::HasAurasWithNameHash(uint32 name_hash)
{
	for(uint32 x = 0; x < MAX_AURAS; ++x)
	{
		if(m_auras[x] && m_auras[x]->GetSpellProto()->NameHash == name_hash)
			return m_auras[x]->m_spellProto->Id;
	}

	return 0;
}

bool Unit::HasNegativeAuraWithNameHash(uint32 name_hash)
{
	for(uint32 x = MAX_POSITIVE_AURAS; x < MAX_AURAS; ++x)
	{
		if(m_auras[x] && m_auras[x]->GetSpellProto()->NameHash == name_hash)
			return true;
	}

	return false;
}

bool Unit::HasNegativeAura(uint32 spell_id)
{
	for(uint32 x = MAX_POSITIVE_AURAS; x < MAX_AURAS; ++x)
	{
		if(m_auras[x] && m_auras[x]->GetSpellProto()->Id == spell_id)
			return true;
	}

	return false;
}

bool Unit::IsPoisoned()
{
	for(uint32 x = 0; x < MAX_AURAS+MAX_PASSIVE_AURAS; ++x)
	{
		if(m_auras[x] && m_auras[x]->GetSpellProto()->c_is_flags & SPELL_FLAG_IS_POISON)
			return true;
	}

	return false;
}

uint32 Unit::AddAuraVisual(uint32 spellid, uint32 count, bool positive)
{
	int32 free = -1;
	uint32 start = positive ? 0 : MAX_POSITIVE_AURAS;
	uint32 end_  = positive ? MAX_POSITIVE_AURAS : MAX_AURAS;

	for(uint32 x = start; x < end_; ++x)
	{
		if(free == -1 && m_uint32Values[UNIT_FIELD_AURA+x] == 0)
			free = x;

		if(m_uint32Values[UNIT_FIELD_AURA+x] == spellid)
		{
			// Increase count of this aura.
			ModAuraStackCount(x, count);
			continue;
			//return free;
		}
	}

	if(free == -1) return 0xFF;

	uint8 flagslot = (free / 4);
	uint32 value = GetUInt32Value((uint16)(UNIT_FIELD_AURAFLAGS + flagslot));

	/*uint8 aurapos = (free & 7) << 2;
	uint32 setflag = AFLAG_SET;
	if(positive)
		setflag = 0xD;

	uint32 value1 = (uint32)setflag << aurapos;
	value |= value1;*/
	uint8 aurapos = (free%4)*8;
	value &= ~(0xFF<<aurapos);
	if(positive)
		value |= (0x1F<<aurapos);
	else
		value |= (0x09<<aurapos);

	SetUInt32Value((uint16)(UNIT_FIELD_AURAFLAGS + flagslot), value);
	SetUInt32Value(UNIT_FIELD_AURA + free, spellid);
	ModAuraStackCount(free, 1);
	SetAuraSlotLevel(free, positive);

	return free;
}

void Unit::SetAuraSlotLevel(uint32 slot, bool positive)
{
	uint32 index = slot / 4;
	uint32 val = m_uint32Values[UNIT_FIELD_AURALEVELS + index];
	uint32 bit = (slot % 4) * 8;
	val &= ~(0xFF << bit);
	if(positive)
		val |= (0x46 << bit);
	else
		val |= (0x19 << bit);

	SetUInt32Value(UNIT_FIELD_AURALEVELS + index, val);
}

void Unit::RemoveAuraVisual(uint32 spellid, uint32 count, uint32 slot)
{
	for(uint32 x = 0; x < MAX_AURAS; ++x)
	{
		if(m_uint32Values[UNIT_FIELD_AURA+x] == spellid)
		{
			// Decrease count of this aura.
			int test = ModAuraStackCount(x, -(int32)count);
			if(test == 0)
			{
				// Aura has been removed completely.

			}
		}
	}

	uint8 flagslot = (slot/4);
	uint32 value = GetUInt32Value((uint16)(UNIT_FIELD_AURAFLAGS + flagslot));
	/*uint8 aurapos = (x & 7) << 2;
	uint32 value1 = ~( (uint32)0xF << aurapos );
	value &= value1;*/
	uint8 aurapos = (slot%4)*8;
	value &= ~(0xFF<<aurapos);
	SetUInt32Value(UNIT_FIELD_AURAFLAGS + flagslot,value);
	SetUInt32Value(UNIT_FIELD_AURA + slot, 0);
	SetAuraSlotLevel(slot, false);

}

uint32 Unit::ModAuraStackCount(uint32 slot, int32 count)
{
	uint32 index = (slot >> 2);
	uint32 byte  = (slot % 4);
	uint32 val   = m_uint32Values[UNIT_FIELD_AURAAPPLICATIONS+index];

	// shouldn't happen
	uint32 ac;

	if(count < 0 && m_auraStackCount[slot] < abs(count))
	{
		m_auraStackCount[slot] = ac = 0;
	}
	else
	{
		m_auraStackCount[slot] += count;
		ac = (m_auraStackCount[slot] > 0) ? m_auraStackCount[slot] - 1 : 0;
	}

	val &= ~(0xFF << byte * 8);
	val |= (ac << byte * 8);

	SetUInt32Value(UNIT_FIELD_AURAAPPLICATIONS+index, val);
	return m_auraStackCount[slot];
}

void Unit::RemoveAurasOfSchool(uint32 School, bool Positive, bool Immune)
{
	for(uint32 x = 0; x < MAX_AURAS; ++x)
	{
		if(m_auras[x]  && m_auras[x]->GetSpellProto()->School == School && (!m_auras[x]->IsPositive() || Positive) && (!Immune && m_auras[x]->GetSpellProto()->Attributes & ATTRIBUTES_IGNORE_INVULNERABILITY))
			m_auras[x]->Remove();
	}
}

void Unit::EnableFlight()
{
	MSG_S2C::stMove_Set_Fly Msg;
	Msg.guid	= GetNewGUID();

	if(m_objectTypeId != TYPEID_PLAYER || ((Player*)this)->m_changingMaps)
	{
		SendMessageToSet(Msg, true);
		if( IsPlayer() )
		{
			static_cast< Player* >( this )->m_setflycheat = true;
		}
	}
	else
	{
		SendMessageToSet(Msg, true);
		static_cast< Player* >( this )->z_axisposition = 0.0f;
		//static_cast< Player* >( this )->delayedPackets.add( &Msg );
		static_cast< Player* >( this )->GetSession()->SendPacket( Msg );
		static_cast< Player* >( this )->m_setflycheat = true;
	}
}

void Unit::DisableFlight()
{
	MSG_S2C::stMove_Set_UnFly Msg;
	Msg.guid = GetNewGUID();
	if(m_objectTypeId != TYPEID_PLAYER || ((Player*)this)->m_changingMaps)
	{
		SendMessageToSet(Msg, true);

		if( IsPlayer() )
			static_cast< Player* >( this )->m_setflycheat = false;
	}
	else
	{
		SendMessageToSet(Msg, true);
		static_cast< Player* >( this )->z_axisposition = 0.0f;
		//static_cast< Player* >( this )->delayedPackets.add( &Msg );
		static_cast< Player* >( this )->GetSession()->SendPacket( Msg );
		static_cast< Player* >( this )->m_setflycheat = false;
	}
}

bool Unit::IsDazed()
{
	for(uint32 x = 0; x < MAX_AURAS; ++x)
	{
		if(m_auras[x])
		{
			if(m_auras[x]->GetSpellProto()->MechanicsType == MECHANIC_ENSNARED)
			return true;
			for(uint32 y=0;y<3;y++)
			if(m_auras[x]->GetSpellProto()->EffectMechanic[y]==MECHANIC_ENSNARED)
				return true;
		}
	}

	return false;

}

void Unit::UpdateVisibility()
{
	static ByteBuffer buf(2500);
	buf.clear();
	InRangeSet::iterator itr, it3;
	uint32 count;
	bool can_see;
	bool is_visible;
	Player *pl;
	Object * pObj;
	Player * plr;

	if( m_objectTypeId == TYPEID_PLAYER )
	{
		plr = static_cast< Player* >( this );
		for( Object::InRangeSet::iterator itr = m_objectsInRange.begin(); itr != m_objectsInRange.end();)
		{
			pObj = (*itr);
			++itr;

			can_see = plr->CanSee(pObj);
			is_visible = plr->GetVisibility(pObj, &it3);
			if(can_see)
			{
				if(!is_visible)
				{
					buf.clear();
					count = pObj->BuildCreateUpdateBlockForPlayer( &buf, plr );
					plr->PushCreationData(&buf, count);
					plr->AddVisibleObject(pObj);
				}
			}
			else
			{
				if(is_visible)
				{
					pObj->DestroyForPlayer(plr);
					plr->RemoveVisibleObject(it3);
				}
			}

			if( pObj->GetTypeId() == TYPEID_PLAYER )
			{
				pl = static_cast< Player* >( pObj );
				can_see = pl->CanSee( plr );
				is_visible = pl->GetVisibility( plr, &it3 );
				if( can_see )
				{
					if(!is_visible)
					{
						buf.clear();
						count = plr->BuildCreateUpdateBlockForPlayer( &buf, pl );
						pl->PushCreationData(&buf, count);
						pl->AddVisibleObject(plr);
					}
				}
				else
				{
					if(is_visible)
					{
						plr->DestroyForPlayer(pl);
						pl->RemoveVisibleObject(it3);
					}
				}
			}
		}
	}
	else			// For units we can save a lot of work
	{
		for(set<Player*>::iterator it2 = GetInRangePlayerSetBegin(); it2 != GetInRangePlayerSetEnd(); ++it2)
		{
			Player* ply = *it2;
			can_see = ply->CanSee(this);
			is_visible = ply->GetVisibility(this, &itr);
			if(!can_see)
			{
				if(is_visible)
				{
					DestroyForPlayer( ply );
					ply->RemoveVisibleObject(itr);
				}
			}
			else
			{
				if(!is_visible)
				{
					buf.clear();
					count = BuildCreateUpdateBlockForPlayer( &buf, ply );
					ply->PushCreationData(&buf, count);
					ply->AddVisibleObject(this);
				}
			}
		}
	}
}

void Unit::RemoveSoloAura(uint32 type)
{
	switch(type)
	{
		case 1:// Polymorph
		{
			if(!polySpell) return;
			if(HasActiveAura(polySpell))
				RemoveAura(polySpell);
		}break;
/*		case 2:// Sap
		{
			if(!sapSpell) return;
			if(HasActiveAura(sapSpell))
				RemoveAura(sapSpell);
		}break;
		case 3:// Fear (Warlock)
		{
			if(!fearSpell) return;
			if(HasActiveAura(fearSpell))
				RemoveAura(fearSpell);
		}break;*/
		default:
			{
			MyLog::log->debug("Warning: we are trying to remove a soloauratype that has no handle");
			}break;
	}
}

void Unit::EventHealthChangeSinceLastUpdate()
{
	int pct = GetHealthPct();
	if(pct<35)
	{
		uint32 toset=AURASTATE_FLAG_HEALTH35;
		if(pct<20)
			toset |= AURASTATE_FLAG_HEALTH20;
		else
			RemoveFlag(UNIT_FIELD_AURASTATE,AURASTATE_FLAG_HEALTH20);
		SetFlag(UNIT_FIELD_AURASTATE,toset);
	}
	else
		RemoveFlag(UNIT_FIELD_AURASTATE , AURASTATE_FLAG_HEALTH35 | AURASTATE_FLAG_HEALTH20);
}

int32 Unit::GetAP()
{
    int32 baseap = GetUInt32Value(UNIT_FIELD_ATTACK_POWER) + GetUInt32Value(UNIT_FIELD_ATTACK_POWER_MODS);
    float totalap = float(baseap)*(GetFloatValue(UNIT_FIELD_ATTACK_POWER_MULTIPLIER)+1);
	if(totalap>=0.000001)
		return float2int32(totalap);
	return	1;
}

int32 Unit::GetRAP()
{
    int32 baseap = GetUInt32Value(UNIT_FIELD_RANGED_ATTACK_POWER) + GetUInt32Value(UNIT_FIELD_RANGED_ATTACK_POWER_MODS);
    float totalap = float(baseap)*(GetFloatValue(UNIT_FIELD_RANGED_ATTACK_POWER_MULTIPLIER)+1);
	if(totalap>=0)
		return float2int32(totalap);
	return	0;
}

bool Unit::GetSpeedDecrease()
{
	int32 before=m_speedModifier;
	m_speedModifier -= m_slowdown;
	m_slowdown = 0;
	map< uint32, int32 >::iterator itr = speedReductionMap.begin();
	for(; itr != speedReductionMap.end(); ++itr)
		m_slowdown = (int32)min( m_slowdown, itr->second );

	if(m_slowdown<-100)
		m_slowdown = 100; //do not walk backwards !

	m_speedModifier += m_slowdown;
	//save bandwidth :P
	if(m_speedModifier!=before)
		return true;
	return false;
}

void Unit::EventCastSpell(Unit * Target, SpellEntry * Sp)
{
	Spell * pSpell = new Spell(Target, Sp, true, NULL);
	SpellCastTargets targets(Target->GetGUID());
	pSpell->prepare(&targets);
}

void Unit::EventChongzhuang(Unit* Target, SpellEntry * Sp, float x, float y, float z)
{
	//target眩晕

	//self移动位置
	SetPosition(x, y, z, GetOrientation());
	if(IsPlayer())
	{
		sEventMgr.AddEvent((Player*)this,&Player::AttackAfterSpell,Target->GetGUID(),
			Sp, EVENT_SPELL_DAMAGE_HIT, this->GetUInt32Value(UNIT_FIELD_BASEATTACKTIME), 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);
	}
}

uint32 Unit::GetFFA() const
{
	return GetUInt32Value( UNIT_FIELD_FFA );
}

void Unit::SetFFA( uint32 n )
{
	SetUInt32Value(UNIT_FIELD_FFA, n);
}

void Unit::EventApplyAura(uint64 Victim, uint32 SpellID, int32 damage, uint32 i, uint32 duration)
{
	Unit* unitTarget = GetMapMgr()->GetUnit(Victim);
	if(!unitTarget)
		return;


	//check if we already have stronger aura
	Aura *pAura;
	SpellEntry* m_spellInfo = dbcSpell.LookupEntry(SpellID);
	std::map<uint32,Aura*>::iterator itr=unitTarget->tmpAura.find(SpellID);

	int maxi = 0;
	for (int c = 0; c < 3; c ++)
	{
		if (m_spellInfo->Effect[c] == SPELL_EFFECT_APPLY_AURA)
		{
			maxi = c;
		}
		
	}

	//if we do not make a check to see if the aura owner is the same as the caster then we will stack the 2 auras and they will not be visible client sided
	if(itr==unitTarget->tmpAura.end())
	{
		//buf优先级
		{
			if(m_spellInfo->EffectApplyAuraName[i] >= TOTAL_SPELL_AURAS)
			{
				MyLog::log->error("find a auraname which is bigger than %u", TOTAL_SPELL_AURAS);
				return;
			}
			uint32 oldspellid = unitTarget->m_spellAuras[m_spellInfo->EffectApplyAuraName[i]];
			if(oldspellid)
			{
				SpellEntry* pOldSpell = dbcSpell.LookupEntry(oldspellid);
				if(pOldSpell)
				{
					if(pOldSpell->SpellGroupType == m_spellInfo->SpellGroupType && pOldSpell->spellLevel > m_spellInfo->spellLevel)
					{
						return;
					}
				}
			}
		}

		pAura=new Aura(m_spellInfo, duration, this, unitTarget);

		pAura->pSpellId = SpellID; //this is required for triggered spells

		unitTarget->tmpAura[SpellID] = pAura;
		m_spellAuras[m_spellInfo->EffectApplyAuraName[i]] = SpellID;
		m_spellAurasEffectPtr[m_spellInfo->EffectApplyAuraName[i]] = pAura;
	}
	else
	{
		pAura=itr->second;
	}
	pAura->AddMod(m_spellInfo->EffectApplyAuraName[i], m_spellInfo->EffectBasePoints[i],m_spellInfo->EffectMiscValue[i],i);

	Aura* a = pAura;
	if( a )
	{
		if( !a->IsValid() )
		{
			MyLog::log->error( "invalid aura! spell id: %d", m_spellInfo->Id );
			unitTarget->tmpAura.erase(itr);
			return;
		}
		if( a->GetSpellProto()->procCharges>0)
		{

			Aura *aur=NULL;
			for(int i=0;i<a->GetSpellProto()->procCharges-1;i++)
			{
				aur = new Aura(a->GetSpellProto(),a->GetDuration(),a->GetCaster(),a->GetTarget());
				unitTarget->AddAura(aur);
				aur=NULL;
			}
			if(!(a->GetSpellProto()->procFlags & PROC_REMOVEONUSE))
			{
				SpellCharge charge;
				charge.count=a->GetSpellProto()->procCharges;
				charge.spellId=a->GetSpellId();
				charge.ProcFlag=a->GetSpellProto()->procFlags;
				charge.lastproc = 0;
				unitTarget->m_chargeSpells.insert(make_pair(a->GetSpellId(),charge));
			}
		}
		 // the real spell is added last so the modifier is removed last
		if (maxi == i)
		{
			unitTarget->AddAura(a);
			unitTarget->tmpAura.erase(SpellID);			
		}
	}
}

void Unit::SetFacing(float newo)
{
	SetOrientation(newo);

	m_aiInterface->SendMoveToPacket(m_position.x,m_position.y,m_position.z,m_position.o,MOVE_TYPE_POLL,0x100); // MoveFlags = 0x100 (run)
}

//guardians are temporary spawn that will inherit master faction and will folow them. Apart from that they have their own mind
Unit* Unit::create_guardian(uint32 guardian_entry,uint32 duration,float angle, uint32 lvl)
{
	CreatureProto * proto = CreatureProtoStorage.LookupEntry(guardian_entry);
	CreatureInfo * info = CreatureNameStorage.LookupEntry(guardian_entry);
	if(!proto || !info)
	{
		MyLog::log->notice("Warning : Missing summon creature template %u !",guardian_entry);
		return NULL;
	}
	float m_fallowAngle=angle;
	float x = GetPositionX()+(3*(cosf(m_fallowAngle+GetOrientation())));
	float y = GetPositionY()+(3*(sinf(m_fallowAngle+GetOrientation())));
	float z = GetPositionZ();
	Creature * p = GetMapMgr()->CreateCreature(guardian_entry);
	p->SetInstanceID(GetMapMgr()->GetInstanceID());
	p->Load(proto, x, y, z, GetOrientation());

	if (lvl != 0)
	{
		/* MANA */
		p->SetPowerType(POWER_TYPE_MANA);
		p->SetUInt32Value(UNIT_FIELD_MAXPOWER1,p->GetUInt32Value(UNIT_FIELD_MAXPOWER1)+28+10*lvl);
		p->SetUInt32Value(UNIT_FIELD_POWER1,p->GetUInt32Value(UNIT_FIELD_POWER1)+28+10*lvl);
		/* HEALTH */
		p->SetUInt32Value(UNIT_FIELD_MAXHEALTH,p->GetUInt32Value(UNIT_FIELD_MAXHEALTH)+28+30*lvl);
		p->SetUInt32Value(UNIT_FIELD_HEALTH,p->GetUInt32Value(UNIT_FIELD_HEALTH)+28+30*lvl);
		/* LEVEL */
		p->ModUnsigned32Value(UNIT_FIELD_LEVEL, lvl);
	}

	p->SetUInt64Value(UNIT_FIELD_SUMMONEDBY, GetGUID());
    p->SetUInt64Value(UNIT_FIELD_CREATEDBY, GetGUID());
    p->SetZoneId(GetZoneId());
	p->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE,GetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE));
	p->_setFaction();

	p->GetAIInterface()->Init(p,AITYPE_PET,MOVEMENTTYPE_NONE,this);
	p->GetAIInterface()->SetUnitToFollow(this);
	p->GetAIInterface()->SetUnitToFollowAngle(m_fallowAngle);
	p->GetAIInterface()->SetFollowDistance(3.0f);

	p->PushToWorld(GetMapMgr());

	sEventMgr.AddEvent(p, &Creature::SummonExpire, EVENT_SUMMON_EXPIRE, duration, 1,0);

	return p;//lol, will compiler make the pointer conversion ?

}

float Unit::get_chance_to_daze(Unit *target)
{
//	if(GetTypeId()!=TYPEID_UNIT)
//		return 0.0f;
	/*
	float attack_skill = float( getLevel() ) * 5.0f;
	float defense_skill;
	if( target->IsPlayer() )
		defense_skill = float( static_cast< Player* >( target )->_GetSkillLineCurrent( SKILL_DEFENSE, false ) );
	else defense_skill = float( target->getLevel() * 5 );
	if( !defense_skill )
		defense_skill = 1;
	float chance_to_daze = attack_skill * 20 / defense_skill;//if level is equal then we get a 20% chance to daze
	chance_to_daze = chance_to_daze * std::min(target->getLevel() / 30.0f, 1.0f );//for targets below level 30 the chance decreses
	if( chance_to_daze > 40 )
		return 40.0f;
	else
		return chance_to_daze;
		*/
	if( m_mapMgr && m_mapMgr->GetMapInfo()->type > 0 )
		return 0.f;

	int mylevel = getLevel() + 3;
	int targetlevel = target->getLevel();
	if( mylevel >= targetlevel )
		return 20.f + (mylevel - targetlevel) * 3.f;
	else
		return 10.f;
}

void CombatStatusHandler::ClearMyHealers()
{
}

void CombatStatusHandler::TouchWith( Unit* pTarget, bool damage )
{
	if( m_Unit == pTarget )
		return;

	Player* plr = NULL;
	if( m_Unit->IsPlayer() )
		plr  = (Player*)m_Unit;

	Player* plrTarget = NULL;
	if( pTarget->IsPlayer() )
		plrTarget = (Player*)pTarget;

	if( plr && plrTarget )
	{
		if( damage )
		{
			m_PVPLeaveCombatTimer = (uint32)UNIXTIME + 10;
			plrTarget->CombatStatus.m_PVPLeaveCombatTimer = m_PVPLeaveCombatTimer;
			if( !m_lastStatus )
				UpdateFlag( true );

			if( !plrTarget->CombatStatus.m_lastStatus )
				plrTarget->CombatStatus.UpdateFlag( true );
		}
		else
		{
			if( plrTarget->CombatStatus.m_lastStatus || isHostile( plr, plrTarget ) )
			{
				m_PVPLeaveCombatTimer = (uint32)UNIXTIME + 10;
				plrTarget->CombatStatus.m_PVPLeaveCombatTimer = m_PVPLeaveCombatTimer;
				if( !m_lastStatus )
					UpdateFlag( true );

				if( !plrTarget->CombatStatus.m_lastStatus )
					plrTarget->CombatStatus.UpdateFlag( true );
			}
		}
	}
}

void CombatStatusHandler::WeHealed(Unit * pHealTarget, int mount)
{
	// healing entry!
	Player* plr = NULL;
	if( m_Unit->IsPlayer() )
		plr  = (Player*)m_Unit;

	if( plr && plr->m_sunyou_instance && plr->m_sunyou_instance->IsBattleGround() )
	{
		SunyouBattleGroundBase* pBGBase = plr->m_sunyou_instance->GetBGBase();
		if( pBGBase )
		{
			pBGBase->OnBGPlayerHealing( plr, mount );
		}
	}

	TouchWith( pHealTarget, false );
}

void CombatStatusHandler::RemoveHealed(Unit * pHealTarget)
{
	m_healed.erase(pHealTarget->GetLowGUID());
	UpdateFlag();
}

void CombatStatusHandler::Update()
{
	if( m_Unit->GetAIInterface()->m_ForceMoveMode )
		return;

	if( (uint32)UNIXTIME < m_PVPLeaveCombatTimer )
		return;

	if( !m_lastStatus )
	{
		if( !InternalIsInCombat() )
			return;
		else
		{
			UpdateFlag();
			return;
		}
	}

	MapMgr* mgr = m_Unit->GetMapMgr();
	if( !mgr )
		return;

	if( m_Unit->IsCreature() )
	{
		if( m_Unit->GetAIInterface()->GetThreatTargetCount() > 0 )
			return;
	}
	else if( m_Unit->IsPlayer() )
	{
		for( std::set<uint64>::iterator it = _threat_target.begin(); it != _threat_target.end(); ++it )
		{
			uint64 guid = *it;
			Unit* p = mgr->GetUnit( guid );
			if( p && p->IsValid() && p->isAlive() && p->GetAIInterface()->IsInThreatList( m_Unit ) && p->CombatStatus.m_lastStatus )
				return;
		}
	}

	Vanished();
	m_lastStatus = false;
}

void CombatStatusHandler::UpdateFlag( bool force )
{
	bool n_status = false;
	if( !force )
		n_status = InternalIsInCombat();
	else
		n_status = true;

	if(n_status != m_lastStatus)
	{
		m_lastStatus = n_status;
		if(n_status)
		{
			//printf(I64FMT" is now in combat.\n", m_Unit->GetGUID());
			if(m_Unit->IsPlayer())
			{
				((Player*)m_Unit)->m_incombattimeleft = -1;
				((Player*)m_Unit)->m_inarmedtimeleft = 20000;
				if(!m_Unit->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT))
					((Player*)m_Unit)->m_entercombat_time = UNIXTIME;
			}
			SunyouRaid* raid = m_Unit->GetSunyouRaid();
			if( raid && !m_Unit->GetAIInterface()->m_ForceMoveMode )
			{
				raid->OnUnitEnterCombat( m_Unit );
			}

			m_Unit->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);
			if(!m_Unit->hasStateFlag(UF_ATTACKING)) m_Unit->addStateFlag(UF_ATTACKING);

			if(m_Unit->IsPlayer())
			{
				Player* _player = (Player*)m_Unit;
				if(_player->GetTradeTarget() == 0/* || _player->mTradeStatus == TRADE_STATUS_COMPLETE*/)
					return;

				MSG_S2C::stTradeStat Msg;
				uint32 TradeStatus = TRADE_STATUS_CANCELLED;

				Msg.nStat	= TradeStatus;
				_player->GetSession()->SendPacket( Msg );

				Player * plr = _player->GetTradeTarget();
				if(plr)
				{
					if(plr->GetSession() && plr->GetSession()->GetSocket())
						plr->GetSession()->SendPacket( Msg );

					plr->ResetTradeVariables();
				}

				_player->ResetTradeVariables();
			}
		}
		else
		{
			//printf(I64FMT" is no longer in combat.\n", m_Unit->GetGUID());
			/*
			if(m_Unit->IsPlayer())
			{
				((Player*)m_Unit)->m_incombattimeleft = 5000;
				((Player*)m_Unit)->m_inarmedtimeleft = 5000;
			}
			else
				*/
				m_Unit->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_COMBAT);
			if(m_Unit->hasStateFlag(UF_ATTACKING)) m_Unit->clearStateFlag(UF_ATTACKING);

			if( m_Unit->IsInWorld() && m_Unit->GetMapMgr()->m_sunyouinstance )
				m_Unit->GetMapMgr()->m_sunyouinstance->OnUnitLeaveCombat( m_Unit );

			if( m_Unit->IsCreature() )
				m_Unit->RemoveAllNegAuras();
			// remove any of our healers from combat too, if they are able to be.
			//ClearMyHealers();
		}
	}
}

bool CombatStatusHandler::InternalIsInCombat()
{
	if(_threat_target.size() > 0)
		return true;

	if( m_Unit->IsCreature() )
		if( m_Unit->GetAIInterface()->GetThreatTargetCount() > 0 )
			return true;

	return false;
}

void CombatStatusHandler::AddAttackTarget(const uint64& guid)
{
}

void CombatStatusHandler::ClearPrimaryAttackTarget()
{
}

bool CombatStatusHandler::IsAttacking(Unit * pTarget)
{
	// check the target for any of our DoT's.
	for(uint32 i = MAX_POSITIVE_AURAS; i < MAX_AURAS; ++i)
	{
		if(pTarget->m_auras[i] != NULL)
		{
			if(m_Unit->GetGUID() == pTarget->m_auras[i]->m_casterGuid && pTarget->m_auras[i]->IsCombatStateAffecting())
				return true;
		}
	}

	// place any additional checks here
	return false;
}

void CombatStatusHandler::RemoveAttackTarget(Unit * pTarget)
{
}

void CombatStatusHandler::RemoveAttacker(Unit * pAttacker, const uint64& guid)
{
}

void CombatStatusHandler::OnDamageDealt(Unit * pTarget)
{
}

void CombatStatusHandler::AddAttacker(const uint64& guid)
{
}

void CombatStatusHandler::ClearAttackers()
{
}

void CombatStatusHandler::ClearHealers( bool bRemoveArua )
{
}

void CombatStatusHandler::OnRemoveFromWorld()
{
	_threat_target.clear();
}

void CombatStatusHandler::Vanished()
{
	_threat_target.clear();
	m_Unit->GetAIInterface()->m_aiTargets.clear();
	m_Unit->m_fearDamageTaken = 0;

	UpdateFlag();
}

void Unit::CombatStatusHandler_ResetPvPTimeout()
{
}

void Unit::CombatStatusHandler_UpdatePvPTimeout()
{
	CombatStatus.TryToClearAttackTargets();
}

void Unit::Heal(Unit *target, uint32 SpellId, uint32 amount)
{//Static heal
	if(!target || !SpellId || !amount)
		return;

	uint32 ch=target->GetUInt32Value(UNIT_FIELD_HEALTH);
	uint32 mh=target->GetUInt32Value(UNIT_FIELD_MAXHEALTH);
	if(mh!=ch)
	{
		ch += amount;
		if(ch > mh)
		{
			target->SetUInt32Value(UNIT_FIELD_HEALTH, mh);
			amount += mh-ch;
		}
		else
			target->SetUInt32Value(UNIT_FIELD_HEALTH, ch);

		MSG_S2C::stSpell_Heal_On_Player Msg;
		Msg.target_guid	= target->GetNewGUID();
		Msg.caster_guid	= this->GetNewGUID();
		Msg.spell_id	= SpellId;
		Msg.dmg			= amount;
		Msg.critical	= 0;
		this->SendMessageToSet(Msg,true);
	}
}
void Unit::Energize(Unit* target,uint32 SpellId, uint32 amount,uint32 type)
{//Static energize
	if(!target || !SpellId || !amount)
		return;
	uint32 cm=target->GetUInt32Value(UNIT_FIELD_POWER1+type);
	uint32 mm=target->GetUInt32Value(UNIT_FIELD_MAXPOWER1+type);
	if(mm!=cm)
	{
		cm += amount;
		if(cm > mm)
		{
			target->SetUInt32Value(UNIT_FIELD_POWER1+type, mm);
			amount += mm-cm;
		}
		else
			target->SetUInt32Value(UNIT_FIELD_POWER1+type, cm);

		MSG_S2C::stSpell_HealMana_On_Player Msg;
		Msg.caster_guid = this->GetNewGUID();
		Msg.target_guid = target->GetNewGUID();
		Msg.spell_id	= (SpellId);
		Msg.power_type	= 0;
		Msg.dmg			= amount;
		this->SendMessageToSet(Msg,true);
	}
}

void Unit::InheritSMMods(Unit *inherit_from)
{
	if(inherit_from->SM_CriticalChance)
	{
		if(SM_CriticalChance==0)
			SM_CriticalChance = new int32[SPELL_GROUPS];
		memcpy(SM_CriticalChance,inherit_from->SM_CriticalChance,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FDur)
	{
		if(SM_FDur==0)
			SM_FDur = new int32[SPELL_GROUPS];
		memcpy(SM_FDur,inherit_from->SM_FDur,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PDur)
	{
		if(SM_PDur==0)
			SM_PDur = new int32[SPELL_GROUPS];
		memcpy(SM_PDur,inherit_from->SM_PDur,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PRadius)
	{
		if(SM_PRadius==0)
			SM_PRadius = new int32[SPELL_GROUPS];
		memcpy(SM_PRadius,inherit_from->SM_PRadius,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FRadius)
	{
		if(SM_FRadius==0)
			SM_FRadius = new int32[SPELL_GROUPS];
		memcpy(SM_FRadius,inherit_from->SM_FRadius,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FRange)
	{
		if(SM_FRange==0)
			SM_FRange = new int32[SPELL_GROUPS];
		memcpy(SM_FRange,inherit_from->SM_FRange,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PCastTime)
	{
		if(SM_PCastTime==0)
			SM_PCastTime = new int32[SPELL_GROUPS];
		memcpy(SM_PCastTime,inherit_from->SM_PCastTime,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FCastTime)
	{
		if(SM_FCastTime==0)
			SM_FCastTime = new int32[SPELL_GROUPS];
		memcpy(SM_FCastTime,inherit_from->SM_FCastTime,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PCriticalDamage)
	{
		if(SM_PCriticalDamage==0)
			SM_PCriticalDamage = new int32[SPELL_GROUPS];
		memcpy(SM_PCriticalDamage,inherit_from->SM_PCriticalDamage,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FDOT)
	{
		if(SM_FDOT==0)
			SM_FDOT = new int32[SPELL_GROUPS];
		memcpy(SM_FDOT,inherit_from->SM_FDOT,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PDOT)
	{
		if(SM_PDOT==0)
			SM_PDOT = new int32[SPELL_GROUPS];
		memcpy(SM_PDOT,inherit_from->SM_PDOT,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FEffectBonus)
	{
		if(SM_FEffectBonus==0)
			SM_FEffectBonus = new int32[SPELL_GROUPS];
		memcpy(SM_FEffectBonus,inherit_from->SM_FEffectBonus,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PEffectBonus)
	{
		if(SM_PEffectBonus==0)
			SM_PEffectBonus = new int32[SPELL_GROUPS];
		memcpy(SM_PEffectBonus,inherit_from->SM_PEffectBonus,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FDamageBonus)
	{
		if(SM_FDamageBonus==0)
			SM_FDamageBonus = new int32[SPELL_GROUPS];
		memcpy(SM_FDamageBonus,inherit_from->SM_FDamageBonus,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PDamageBonus)
	{
		if(SM_PDamageBonus==0)
			SM_PDamageBonus = new int32[SPELL_GROUPS];
		memcpy(SM_PDamageBonus,inherit_from->SM_PDamageBonus,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PSPELL_VALUE[0])
	{
		if(SM_PSPELL_VALUE[0]==0)
			SM_PSPELL_VALUE[0] = new int32[SPELL_GROUPS];
		memcpy(SM_PSPELL_VALUE[0],inherit_from->SM_PSPELL_VALUE[0],sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PSPELL_VALUE[1])
	{
		if(SM_PSPELL_VALUE[1]==0)
			SM_PSPELL_VALUE[1] = new int32[SPELL_GROUPS];
		memcpy(SM_PSPELL_VALUE[1],inherit_from->SM_PSPELL_VALUE[1],sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PSPELL_VALUE[2])
	{
		if(SM_PSPELL_VALUE[2]==0)
			SM_PSPELL_VALUE[2] = new int32[SPELL_GROUPS];
		memcpy(SM_PSPELL_VALUE[2],inherit_from->SM_PSPELL_VALUE[2],sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FSPELL_VALUE[0])
	{
		if(SM_FSPELL_VALUE[0]==0)
			SM_FSPELL_VALUE[0] = new int32[SPELL_GROUPS];
		memcpy(SM_FSPELL_VALUE[0],inherit_from->SM_FSPELL_VALUE[0],sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FSPELL_VALUE[2])
	{
		if(SM_FSPELL_VALUE[2]==0)
			SM_FSPELL_VALUE[2] = new int32[SPELL_GROUPS];
		memcpy(SM_FSPELL_VALUE[2],inherit_from->SM_FSPELL_VALUE[2],sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FSPELL_VALUE[1])
	{
		if(SM_FSPELL_VALUE[1]==0)
			SM_FSPELL_VALUE[1] = new int32[SPELL_GROUPS];
		memcpy(SM_FSPELL_VALUE[1],inherit_from->SM_FSPELL_VALUE[1],sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FHitchance)
	{
		if(SM_FHitchance==0)
			SM_FHitchance = new int32[SPELL_GROUPS];
		memcpy(SM_FHitchance,inherit_from->SM_FHitchance,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PRange)
	{
		if(SM_PRange==0)
			SM_PRange = new int32[SPELL_GROUPS];
		memcpy(SM_PRange,inherit_from->SM_PRange,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PRadius)
	{
		if(SM_PRadius==0)
			SM_PRadius = new int32[SPELL_GROUPS];
		memcpy(SM_PRadius,inherit_from->SM_PRadius,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PAPBonus)
	{
		if(SM_PAPBonus==0)
			SM_PAPBonus = new int32[SPELL_GROUPS];
		memcpy(SM_PAPBonus,inherit_from->SM_PAPBonus,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PCost)
	{
		if(SM_PCost==0)
			SM_PCost = new int32[SPELL_GROUPS];
		memcpy(SM_PCost,inherit_from->SM_PCost,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FCost)
	{
		if(SM_FCost==0)
			SM_FCost = new int32[SPELL_GROUPS];
		memcpy(SM_FCost,inherit_from->SM_FCost,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FAdditionalTargets)
	{
		if(SM_FAdditionalTargets==0)
			SM_FAdditionalTargets = new int32[SPELL_GROUPS];
		memcpy(SM_FAdditionalTargets,inherit_from->SM_FAdditionalTargets,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PJumpReduce)
	{
		if(SM_PJumpReduce==0)
			SM_PJumpReduce = new int32[SPELL_GROUPS];
		memcpy(SM_PJumpReduce,inherit_from->SM_PJumpReduce,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FSpeedMod)
	{
		if(SM_FSpeedMod==0)
			SM_FSpeedMod = new int32[SPELL_GROUPS];
		memcpy(SM_FSpeedMod,inherit_from->SM_FSpeedMod,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PNonInterrupt)
	{
		if(SM_PNonInterrupt==0)
			SM_PNonInterrupt = new int32[SPELL_GROUPS];
		memcpy(SM_PNonInterrupt,inherit_from->SM_PNonInterrupt,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FPenalty)
	{
		if(SM_FPenalty==0)
			SM_FPenalty = new int32[SPELL_GROUPS];
		memcpy(SM_FPenalty,inherit_from->SM_FPenalty,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FCooldownTime)
	{
		if(SM_FCooldownTime==0)
			SM_FCooldownTime = new int32[SPELL_GROUPS];
		memcpy(SM_FCooldownTime,inherit_from->SM_FCooldownTime,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PCooldownTime)
	{
		if(SM_PCooldownTime==0)
			SM_PCooldownTime = new int32[SPELL_GROUPS];
		memcpy(SM_PCooldownTime,inherit_from->SM_PCooldownTime,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FChanceOfSuccess)
	{
		if(SM_FChanceOfSuccess==0)
			SM_FChanceOfSuccess = new int32[SPELL_GROUPS];
		memcpy(SM_FChanceOfSuccess,inherit_from->SM_FChanceOfSuccess,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FRezist_dispell)
	{
		if(SM_FRezist_dispell==0)
			SM_FRezist_dispell = new int32[SPELL_GROUPS];
		memcpy(SM_FRezist_dispell,inherit_from->SM_FRezist_dispell,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PRezist_dispell)
	{
		if(SM_PRezist_dispell==0)
			SM_PRezist_dispell = new int32[SPELL_GROUPS];
		memcpy(SM_PRezist_dispell,inherit_from->SM_PRezist_dispell,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FThreatReduce)
	{
		if(SM_FThreatReduce==0)
			SM_FThreatReduce = new int32[SPELL_GROUPS];
		memcpy(SM_FThreatReduce,inherit_from->SM_FThreatReduce,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_PThreatReduce)
	{
		if(SM_PThreatReduce==0)
			SM_PThreatReduce = new int32[SPELL_GROUPS];
		memcpy(SM_PThreatReduce,inherit_from->SM_PThreatReduce,sizeof(int)*SPELL_GROUPS);
	}
	if(inherit_from->SM_FStackCount)
	{
		if(SM_FStackCount==0)
			SM_FStackCount = new int32[SPELL_GROUPS];
		memcpy(SM_FStackCount,inherit_from->SM_FStackCount,sizeof(int)*SPELL_GROUPS);
	}
}

void CombatStatusHandler::TryToClearAttackTargets()
{
	return;
	AttackerMap::iterator i, i2;
	Unit * pt;

	for(i = m_attackTargets.begin(); i != m_attackTargets.end();)
	{
		i2 = i++;
		pt = m_Unit->GetMapMgr()->GetUnit(*i2);
		if(pt == NULL)
		{
			m_attackTargets.erase(i2);
			continue;
		}

		RemoveAttackTarget(pt);
		pt->CombatStatus.RemoveAttacker(m_Unit,m_Unit->GetGUID());
	}
}

void CombatStatusHandler::AttackersForgetHate()
{
	return;
	AttackerMap::iterator i, i2;
	Unit * pt;

	for(i = m_attackTargets.begin(); i != m_attackTargets.end();)
	{
		i2 = i++;
		pt = m_Unit->GetMapMgr()->GetUnit(*i2);
		if(pt == NULL)
		{
			m_attackTargets.erase(i2);
			continue;
		}

		if(pt->GetAIInterface())
			pt->GetAIInterface()->RemoveThreatByPtr(m_Unit);
	}
}

void Unit::CancelSpell(Spell * ptr)
{
	

	Spell* pCancel =NULL;

	if(ptr)
		pCancel = ptr;
	else if(m_currentSpell)
		pCancel = m_currentSpell;

	if (pCancel)
	{

		float casttime = 0.f;
		casttime = GetCastTime( dbcSpellCastTime.LookupEntry(pCancel->m_spellInfo->CastingTimeIndex ));
		bool bInterupted = false;
		if(pCancel->m_timer > 0)
		{
			bInterupted = true;
		}
		pCancel->cancel(bInterupted);

		
	}
}

void Unit::EventHealHPWithAbility(uint64 guid, SpellEntry * sp, uint32 damage)
{
	Unit * victim = m_mapMgr ? m_mapMgr->GetUnit(guid) : NULL;
	if(victim)
		Heal( victim, sp->Id, damage/*+GetFloatValue(UNIT_FIELD_SPELLDAMAGE)/2*/ );
}

void Unit::EventHealMPWithAbility(uint64 guid, SpellEntry * sp, uint32 damage)
{
	Unit * victim = m_mapMgr ? m_mapMgr->GetUnit(guid) : NULL;
	if(victim)
		Energize( victim, sp->Id, damage, 0 );
}

//Unit* pVictim, uint32 weapon_damage_type, SpellEntry* ability, int32 add_damage,
//int32 pct_dmg_mod, uint32 exclusive_damage, bool disable_proc, bool skip_hit_check, bool bSchool, bool stato 
void Unit::EventStrikeWithAbility(uint64 guid, SpellEntry * sp,  int32 add_damage, int32 pct_dmg_mod, uint32 exclusive_damage, bool disable_proc,
								  bool skip_hit_check, bool School, bool stato, bool weapon )
{
	Unit * victim = m_mapMgr ? m_mapMgr->GetUnit(guid) : NULL;


	enum WeaponDamageType // this is NOT the same as SPELL_ENTRY_Spell_Dmg_Type, or Spell::GetType(), or SPELL_ENTRY_School !!
	{
		MELEE   = 0,
		OFFHAND = 1,
		RANGED  = 2,
	};

	WeaponDamageType en = MELEE;
	if (sp->Spell_Dmg_Type != SPELL_DMG_TYPE_MELEE)
	{
		en	=  RANGED;

	}

	if(victim)
	{
		Strike( victim, sp->Spell_Dmg_Type , sp, add_damage, pct_dmg_mod, exclusive_damage, disable_proc, skip_hit_check, School, stato, weapon);

	}

}

void Unit::DispelAll(bool positive)
{
	for(uint32 i = 0; i < MAX_AURAS; ++i)
	{
		if(m_auras[i]!=NULL)
			if((m_auras[i]->IsPositive()&&positive)||!m_auras[i]->IsPositive())
				m_auras[i]->Remove();
	}
}

/* bool Unit::RemoveAllAurasByMechanic (renamed from MechanicImmunityMassDispel)
- Removes all auras on this unit that are of a specific mechanic.
- Useful for things like.. Apply Aura: Immune Mechanic, where existing (de)buffs are *always supposed* to be removed.
- I'm not sure if this goes here under unit.
* Arguments:
	- uint32 MechanicType
		*

* Returns;
	- False if no buffs were dispelled, true if more than 0 were dispelled.
*/
bool Unit::RemoveAllAurasByMechanic( uint32 MechanicType , uint32 MaxDispel = -1 , bool HostileOnly = true )
{
	//MyLog::log->info( "Unit::MechanicImmunityMassDispel called, mechanic: %u" , MechanicType );
	uint32 DispelCount = 0;
	for(uint32 x = ( HostileOnly ? MAX_POSITIVE_AURAS : 0 ) ; x < MAX_AURAS ; x++ ) // If HostileOnly = 1, then we use aura slots 40-56 (hostile). Otherwise, we use 0-56 (all)
		{
			if( DispelCount >= MaxDispel && MaxDispel > 0 )
			return true;

			if( m_auras[x] )
			{
					if( MechanicType&& m_auras[x]->GetSpellProto()->MechanicsType == MechanicType ) // Remove all mechanics of type MechanicType (my english goen boom)
					{
						m_auras[x]->Remove(); // EZ-Remove
						DispelCount ++;
					}
					else if( MechanicType == MECHANIC_ENSNARED ) // if got immunity for slow, remove some that are not in the mechanics
					{
						for( int i=0 ; i<3 ; i++ )
						{
							// SNARE + ROOT
							if( m_auras[x]->GetSpellProto()->EffectApplyAuraName[i] == SPELL_AURA_MOD_DECREASE_SPEED || m_auras[x]->GetSpellProto()->EffectApplyAuraName[i] == SPELL_AURA_MOD_ROOT )
							{
								m_auras[x]->Remove();
								break;
							}
						}
					}
			}
		}
	return ( DispelCount == 0 );
}

void Unit::setAttackTimer(int32 time, bool offhand)
{
	if(!time)
		time = offhand ? m_uint32Values[UNIT_FIELD_BASEATTACKTIME_01] : m_uint32Values[UNIT_FIELD_BASEATTACKTIME];


	float ftemp = 0.f;
	float fcastmod = GetFloatValue( UNIT_MOD_CAST_SPEED );
	ftemp = fcastmod;
	fcastmod -= 1.f;
	if (fcastmod > 0)
	{
		time = float2int32( time * (ftemp));
	}
	else
	{
		ftemp = -ftemp + 2.0f;
		time = float2int32( time / (ftemp ));

	}
	time = std::max(1000,time);
	if(time>300000)		// just in case.. shouldn't happen though
		time=offhand ? m_uint32Values[UNIT_FIELD_BASEATTACKTIME_01] : m_uint32Values[UNIT_FIELD_BASEATTACKTIME];

	if(offhand)
		m_attackTimer_1 = getMSTime() + time;
	else
		m_attackTimer = getMSTime() + time;
}

bool Unit::isAttackReady(bool offhand)
{
	if(offhand)
	{
		return getMSTime() >= m_attackTimer_1;
	}
	else
	{
		return getMSTime() >= m_attackTimer;
	}
}

void Unit::ReplaceAIInterface(AIInterface *new_interface)
{
	delete m_aiInterface;	//be carefull when you do this. Might screw unit states !
	m_aiInterface = new_interface;
}

void Unit::EventUpdateFlag()
{
static_cast< Player * >( this )->CombatStatus.UpdateFlag();
}

bool Unit::HasAurasOfNameHashWithCaster(uint32 namehash, Unit * caster)
{
	for(uint32 i = MAX_POSITIVE_AURAS; i < MAX_AURAS; ++i)
		if( m_auras[i] && m_auras[i]->GetSpellProto()->NameHash == namehash && m_auras[i]->GetCasterGUID() == caster->GetGUID() )
			return true;

	return false;
}

bool Unit::IsPlayerAndFlying()
{
	if( IsPlayer() && ((Player*)this)->GetUInt32Value( PLAYER_FIELD_IS_FLYING ) && ((Player*)this)->IsInFlyMap() )
		return true;
	else
		return false;
}
