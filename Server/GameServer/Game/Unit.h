#ifndef __UNIT_H
#define __UNIT_H

class DynamicObject;

typedef struct
{
	uint32 school_type;
	int32 full_damage;
	uint32 resisted_damage;
}dealdamage;

struct ReflectSpellSchool
{
	uint32 spellId;
	int32 school;
	int32 chance;
	int32 require_aura_hash;
};

typedef struct
{
	uint32 spellid;
	uint64 caster;//not yet in use
	int32 amt;
}Absorb;

typedef std::list<Absorb*> SchoolAbsorb;

typedef struct
{
	uint32 spellid;
	uint32 mindmg;
	uint32 maxdmg;
} OnHitSpell;

#define HIGHEST_FACTION = 46
enum Factions {
	FACTION_BLOODSAIL_BUCCANEERS,
	FACTION_BOOTY_BAY,
	FACTION_GELKIS_CLAN_CENTAUR,
	FACTION_MAGRAM_CLAN_CENTAUR,
	FACTION_THORIUM_BROTHERHOOD,
	FACTION_RAVENHOLDT,
	FACTION_SYNDICATE,
	FACTION_GADGETZAN,
	FACTION_WILDHAMMER_CLAN,
	FACTION_RATCHET,
	FACTION_UNK1,
	FACTION_UNK2,
	FACTION_UNK3,
	FACTION_ARGENT_DAWN,
	FACTION_ORGRIMMAR,
	FACTION_DARKSPEAR_TROLLS,
	FACTION_THUNDER_BLUFF,
	FACTION_UNDERCITY,
	FACTION_GNOMEREGAN_EXILES,
	FACTION_STORMWIND,
	FACTION_IRONFORGE,
	FACTION_DARNASSUS,
	FACTION_LEATHERWORKING_DRAGON,
	FACTION_LEATHERWORKING_ELEMENTAL,
	FACTION_LEATHERWORKING_TRIBAL,
	FACTION_ENGINEERING_GNOME,
	FACTION_ENGINEERING_GOBLIN,
	FACTION_WINTERSABER_TRAINERS,
	FACTION_EVERLOOK,
	FACTION_BLACKSMITHING_ARMOR,
	FACTION_BLACKSMITHING_WEAPON,
	FACTION_BLACKSMITHING_AXE,
	FACTION_BLACKSMITHING_SWORD,
	FACTION_BLACKSMITHING_HAMMER,
	FACTION_CAER_DARROW,
	FACTION_TIMBERMAW_FURBOLGS,
	FACTION_CENARION_CIRCLE,
	FACTION_SHATTERSPEAR_TROLLS,
	FACTION_RAVASAUR_TRAINERS,
	FACTION_BATTLEGROUND_NEUTRAL,
	FACTION_STORMPIKE_GUARDS,
	FACTION_FROSTWOLF_CLAN,
	FACTION_HYDRAXIAN_WATERLORDS,
	FACTION_MORO_GAI,
	FACTION_SHEN_DRALAR,
	FACTION_SILVERWING_SENTINELS,
	FACTION_WARSONG_OUTRIDERS
};
typedef enum
{
	TEXTEMOTE_AGREE			= 1,
	TEXTEMOTE_AMAZE			= 2,
	TEXTEMOTE_ANGRY			= 3,
	TEXTEMOTE_APOLOGIZE		= 4,
	TEXTEMOTE_APPLAUD		  = 5,
	TEXTEMOTE_BASHFUL		  = 6,
	TEXTEMOTE_BECKON		   = 7,
	TEXTEMOTE_BEG			  = 8,
	TEXTEMOTE_BITE			 = 9,
	TEXTEMOTE_BLEED			= 10,
	TEXTEMOTE_BLINK			= 11,
	TEXTEMOTE_BLUSH			= 12,
	TEXTEMOTE_BONK			 = 13,
	TEXTEMOTE_BORED			= 14,
	TEXTEMOTE_BOUNCE		   = 15,
	TEXTEMOTE_BRB			  = 16,
	TEXTEMOTE_BOW			  = 17,
	TEXTEMOTE_BURP			 = 18,
	TEXTEMOTE_BYE			  = 19,
	TEXTEMOTE_CACKLE		   = 20,
	TEXTEMOTE_CHEER			= 21,
	TEXTEMOTE_CHICKEN		  = 22,
	TEXTEMOTE_CHUCKLE		  = 23,
	TEXTEMOTE_CLAP			 = 24,
	TEXTEMOTE_CONFUSED		 = 25,
	TEXTEMOTE_CONGRATULATE	 = 26,
	TEXTEMOTE_COUGH			= 27,
	TEXTEMOTE_COWER			= 28,
	TEXTEMOTE_CRACK			= 29,
	TEXTEMOTE_CRINGE		   = 30,
	TEXTEMOTE_CRY			  = 31,
	TEXTEMOTE_CURIOUS		  = 32,
	TEXTEMOTE_CURTSEY		  = 33,
	TEXTEMOTE_DANCE			= 34,
	TEXTEMOTE_DRINK			= 35,
	TEXTEMOTE_DROOL			= 36,
	TEXTEMOTE_EAT			  = 37,
	TEXTEMOTE_EYE			  = 38,
	TEXTEMOTE_FART			 = 39,
	TEXTEMOTE_FIDGET		   = 40,
	TEXTEMOTE_FLEX			 = 41,
	TEXTEMOTE_FROWN			= 42,
	TEXTEMOTE_GASP			 = 43,
	TEXTEMOTE_GAZE			 = 44,
	TEXTEMOTE_GIGGLE		   = 45,
	TEXTEMOTE_GLARE			= 46,
	TEXTEMOTE_GLOAT			= 47,
	TEXTEMOTE_GREET			= 48,
	TEXTEMOTE_GRIN			 = 49,
	TEXTEMOTE_GROAN			= 50,
	TEXTEMOTE_GROVEL		   = 51,
	TEXTEMOTE_GUFFAW		   = 52,
	TEXTEMOTE_HAIL			 = 53,
	TEXTEMOTE_HAPPY			= 54,
	TEXTEMOTE_HELLO			= 55,
	TEXTEMOTE_HUG			  = 56,
	TEXTEMOTE_HUNGRY		   = 57,
	TEXTEMOTE_KISS			 = 58,
	TEXTEMOTE_KNEEL			= 59,
	TEXTEMOTE_LAUGH			= 60,
	TEXTEMOTE_LAYDOWN		  = 61,
	TEXTEMOTE_MASSAGE		  = 62,
	TEXTEMOTE_MOAN			 = 63,
	TEXTEMOTE_MOON			 = 64,
	TEXTEMOTE_MOURN			= 65,
	TEXTEMOTE_NO			   = 66,
	TEXTEMOTE_NOD			  = 67,
	TEXTEMOTE_NOSEPICK		 = 68,
	TEXTEMOTE_PANIC			= 69,
	TEXTEMOTE_PEER			 = 70,
	TEXTEMOTE_PLEAD			= 71,
	TEXTEMOTE_POINT			= 72,
	TEXTEMOTE_POKE			 = 73,
	TEXTEMOTE_PRAY			 = 74,
	TEXTEMOTE_ROAR			 = 75,
	TEXTEMOTE_ROFL			 = 76,
	TEXTEMOTE_RUDE			 = 77,
	TEXTEMOTE_SALUTE		   = 78,
	TEXTEMOTE_SCRATCH		  = 79,
	TEXTEMOTE_SEXY			 = 80,
	TEXTEMOTE_SHAKE			= 81,
	TEXTEMOTE_SHOUT			= 82,
	TEXTEMOTE_SHRUG			= 83,
	TEXTEMOTE_SHY			  = 84,
	TEXTEMOTE_SIGH			 = 85,
	TEXTEMOTE_SIT			  = 86,
	TEXTEMOTE_SLEEP			= 87,
	TEXTEMOTE_SNARL			= 88,
	TEXTEMOTE_SPIT			 = 89,
	TEXTEMOTE_STARE			= 90,
	TEXTEMOTE_SURPRISED		= 91,
	TEXTEMOTE_SURRENDER		= 92,
	TEXTEMOTE_TALK			 = 93,
	TEXTEMOTE_TALKEX		   = 94,
	TEXTEMOTE_TALKQ			= 95,
	TEXTEMOTE_TAP			  = 96,
	TEXTEMOTE_THANK			= 97,
	TEXTEMOTE_THREATEN		 = 98,
	TEXTEMOTE_TIRED			= 99,
	TEXTEMOTE_VICTORY		  = 100,
	TEXTEMOTE_WAVE			 = 101,
	TEXTEMOTE_WELCOME		  = 102,
	TEXTEMOTE_WHINE			= 103,
	TEXTEMOTE_WHISTLE		  = 104,
	TEXTEMOTE_WORK			 = 105,
	TEXTEMOTE_YAWN			 = 106,
	TEXTEMOTE_BOGGLE		   = 107,
	TEXTEMOTE_CALM			 = 108,
	TEXTEMOTE_COLD			 = 109,
	TEXTEMOTE_COMFORT		  = 110,
	TEXTEMOTE_CUDDLE		   = 111,
	TEXTEMOTE_DUCK			 = 112,
	TEXTEMOTE_INSULT		   = 113,
	TEXTEMOTE_INTRODUCE		= 114,
	TEXTEMOTE_JK			   = 115,
	TEXTEMOTE_LICK			 = 116,
	TEXTEMOTE_LISTEN		   = 117,
	TEXTEMOTE_LOST			 = 118,
	TEXTEMOTE_MOCK			 = 119,
	TEXTEMOTE_PONDER		   = 120,
	TEXTEMOTE_POUNCE		   = 121,
	TEXTEMOTE_PRAISE		   = 122,
	TEXTEMOTE_PURR			 = 123,
	TEXTEMOTE_PUZZLE		   = 124,
	TEXTEMOTE_RAISE			= 125,
	TEXTEMOTE_READY			= 126,
	TEXTEMOTE_SHIMMY		   = 127,
	TEXTEMOTE_SHIVER		   = 128,
	TEXTEMOTE_SHOO			 = 129,
	TEXTEMOTE_SLAP			 = 130,
	TEXTEMOTE_SMIRK			= 131,
	TEXTEMOTE_SNIFF			= 132,
	TEXTEMOTE_SNUB			 = 133,
	TEXTEMOTE_SOOTHE		   = 134,
	TEXTEMOTE_STINK			= 135,
	TEXTEMOTE_TAUNT			= 136,
	TEXTEMOTE_TEASE			= 137,
	TEXTEMOTE_THIRSTY		  = 138,
	TEXTEMOTE_VETO			 = 139,
	TEXTEMOTE_SNICKER		  = 140,
	TEXTEMOTE_STAND			= 141,
	TEXTEMOTE_TICKLE		   = 142,
	TEXTEMOTE_VIOLIN		   = 143,
	TEXTEMOTE_SMILE			= 163,
	TEXTEMOTE_RASP			 = 183,
	TEXTEMOTE_PITY			 = 203,
	TEXTEMOTE_GROWL			= 204,
	TEXTEMOTE_BARK			 = 205,
	TEXTEMOTE_SCARED		   = 223,
	TEXTEMOTE_FLOP			 = 224,
	TEXTEMOTE_LOVE			 = 225,
	TEXTEMOTE_MOO			  = 226,
	TEXTEMOTE_COMMEND		  = 243,
	TEXTEMOTE_JOKE			 = 329
} TextEmoteType;

enum StandState
{
	STANDSTATE_STAND			= 0,
	STANDSTATE_SIT			  = 1,
	STANDSTATE_SIT_CHAIR		= 2,
	STANDSTATE_SLEEP			= 3,
	STANDSTATE_SIT_LOW_CHAIR	= 4,
	STANDSTATE_SIT_MEDIUM_CHAIR = 5,
	STANDSTATE_SIT_HIGH_CHAIR   = 6,
	STANDSTATE_DEAD			 = 7,
	STANDSTATE_KNEEL			= 8
};


enum DamageFlags
{
	DAMAGE_FLAG_MELEE   = 1,
// 	DAMAGE_FLAG_HOLY	= 2,
// 	DAMAGE_FLAG_FIRE	= 4,
// 	DAMAGE_FLAG_NATURE  = 8,
// 	DAMAGE_FLAG_FROST   = 16,
// 	DAMAGE_FLAG_SHADOW  = 32,
// 	DAMAGE_FLAG_ARCANE  = 64
};

enum WeaponDamageType // this is NOT the same as SPELL_ENTRY_Spell_Dmg_Type, or Spell::GetType(), or SPELL_ENTRY_School !!
{
	MELEE   = 0,
	OFFHAND = 1,
	RANGED  = 2,
};

enum VisualState
{
	ATTACK = 1,
	DODGE,
	PARRY,
	INTERRUPT,
	BLOCK,
	EVADE,
	IMMUNE,
	DEFLECT
};



enum FIELD_PADDING//Since this field isnt used you can expand it for you needs
{
	PADDING_NONE
};

struct AuraCheckResponse
{
	uint32 Error;
	uint32 Misc;
};

enum AURA_CHECK_RESULT
{
	AURA_CHECK_RESULT_NONE				  = 1,
	AURA_CHECK_RESULT_HIGHER_BUFF_PRESENT   = 2,
	AURA_CHECK_RESULT_LOWER_BUFF_PRESENT	= 3,
};

typedef std::list<struct ProcTriggerSpellOnSpell> ProcTriggerSpellOnSpellList;

/************************************************************************/
/* "In-Combat" Handler                                                  */
/************************************************************************/

class Unit;
class CombatStatusHandler
{
	typedef set<uint64> AttackerMap;
	typedef set<uint32> HealedSet;		// Must Be Players!
	HealedSet m_healers;
	HealedSet m_healed;
	Unit* m_Unit;
	bool m_lastStatus;
	AttackerMap m_attackTargets;
	uint64 m_primaryAttackTarget;
	uint32 m_PVPLeaveCombatTimer;

public:
	CombatStatusHandler() : m_lastStatus(false), m_primaryAttackTarget(0), m_PVPLeaveCombatTimer(0) {}
	AttackerMap m_attackers;

	std::set<uint64> _threat_target;

	void AddAttackTarget(const uint64& guid);						// this means we clicked attack, not actually striked yet, so they shouldnt be in combat.
	void ClearPrimaryAttackTarget();								// means we deselected the unit, stopped attacking it.

	void OnDamageDealt(Unit * pTarget);								// this is what puts the other person in combat.
	void WeHealed(Unit * pHealTarget, int mount);								// called when a player heals another player, regardless of combat state.
	void TouchWith( Unit* pTarget, bool damage );

	void RemoveAttacker(Unit * pAttacker, const uint64& guid);		// this means we stopped attacking them totally. could be because of deagro, etc.
	void RemoveAttackTarget(Unit * pTarget);						// means our DoT expired.

	void UpdateFlag( bool force = false );							// detects if we have changed combat state (in/out), and applies the flag.
	void Update();

	void WeHealed(Unit* p);

	SUNYOU_INLINE bool IsInCombat() { return m_lastStatus; }				// checks if we are in combat or not.

	void OnRemoveFromWorld();										// called when we are removed from world, kills all references to us.

	void Vanished();

	SUNYOU_INLINE const uint64& GetPrimaryAttackTarget() { return m_primaryAttackTarget; }
	SUNYOU_INLINE void SetUnit(Unit * p) { m_Unit = p; }
	void TryToClearAttackTargets();									// for pvp timeout
	void AttackersForgetHate();										// used right now for Feign Death so attackers go home

protected:
	bool InternalIsInCombat();										// called by UpdateFlag, do not call from anything else!
	bool IsAttacking(Unit * pTarget);								// internal function used to determine if we are still attacking target x.
	void AddAttacker(const uint64& guid);							// internal function to add an attacker
	void RemoveHealed(Unit * pHealTarget);							// usually called only by updateflag
	void ClearHealers( bool bRemoveArua);											// this is called on instance change.
	void ClearAttackers();											// means we vanished, or died.
	void ClearMyHealers();
};

//====================================================================
//  Unit
//  Base object for Players and Creatures
//====================================================================
class Creature;
class Aura;
class AIInterface;

class SERVER_DECL Unit : public Object
{
public:
	/************************************************************************/
	/* LUA Stuff                                                            */
	/************************************************************************/
/*	typedef struct { const char *name; int(*mfunc)(lua_State*,Unit*); } RegType;
	static const char className[];
	static RegType methods[];

	// a lua script cannot create a unit.
	Unit(lua_State * L) { ASSERT(false); }*/
	
	map<uint32, int32> m_increasespellcdt;
	float	m_fDamage2Health;

	void CombatStatusHandler_UpdatePvPTimeout();
	void CombatStatusHandler_ResetPvPTimeout();

	virtual ~Unit ( );

	friend class AIInterface;
	friend class Aura;

	virtual void Update( uint32 time );
	virtual void RemoveFromWorld(bool free_guid);
	virtual void OnPushToWorld();

    void setAttackTimer(int32 time, bool offhand);
	bool isAttackReady(bool offhand);

	SUNYOU_INLINE void SetDuelWield(bool enabled)
	{
		m_duelWield = enabled;
	}

	bool __fastcall canReachWithAttack(Unit *pVictim, bool& badfacing);

  //void StrikeWithAbility( Unit* pVictim, Spell* spell, uint32 addspelldmg, uint32 weapon_damage_type );

	/// State flags are server-only flags to help me know when to do stuff, like die, or attack
	SUNYOU_INLINE void addStateFlag(uint32 f) { m_state |= f; };
	SUNYOU_INLINE bool hasStateFlag(uint32 f) { return (m_state & f ? true : false); }
	SUNYOU_INLINE void clearStateFlag(uint32 f) { m_state &= ~f; };

	/// Stats
	SUNYOU_INLINE uint32 getLevel() { return m_uint32Values[ UNIT_FIELD_LEVEL ]; };
	SUNYOU_INLINE uint8 getRace() { return GetByte(UNIT_FIELD_BYTES_0,0); }
	SUNYOU_INLINE uint8 getClass() { return GetByte(UNIT_FIELD_BYTES_0,1); }
	SUNYOU_INLINE void setRace(uint8 race) { SetByte(UNIT_FIELD_BYTES_0,0,race); }
	SUNYOU_INLINE void setClass(uint8 class_) { SetByte(UNIT_FIELD_BYTES_0,1, class_ ); }
	SUNYOU_INLINE uint32 getClassMask() { return 1 << ( getClass()-1 ) ; }
	SUNYOU_INLINE uint32 getRaceMask() { return 1 << ( getRace()-1) ; }
	SUNYOU_INLINE uint8 getGender() { return GetByte(UNIT_FIELD_BYTES_0,2); }
	SUNYOU_INLINE void setGender(uint8 gender) { SetByte(UNIT_FIELD_BYTES_0,2,gender); }
	SUNYOU_INLINE uint8 getStandState() { return ((uint8)m_uint32Values[UNIT_FIELD_BYTES_1]); }

	//// Combat
   // void DealDamage(Unit *pVictim, uint32 damage, uint32 targetEvent, uint32 unitEvent, uint32 spellId = 0);   // to stop from falling, etc
	//void AttackerStateUpdate( Unit* pVictim, uint32 weapon_damage_type ); // weapon_damage_type: 0 = melee, 1 = offhand(dualwield), 2 = ranged
	uint32 GetSpellDidHitResult( Unit* pVictim, uint32 weapon_damage_type, SpellEntry* ability );
	uint32 Strike( Unit* pVictim, uint32 weapon_damage_type, SpellEntry* ability, int32 add_damage,
		int32 pct_dmg_mod, uint32 exclusive_damage, bool disable_proc, bool skip_hit_check, bool bSchool, bool stato , bool weapon);
//	void PeriodicAuraLog(Unit *pVictim, SpellEntry* spellID, uint32 damage, uint32 damageType);
	//void SpellNonMeleeDamageLog(Unit *pVictim, uint32 spellID, uint32 damage);
	uint32 m_procCounter;
	void HandleProc(uint32 flag, Unit* Victim, SpellEntry* CastingSpell,uint32 dmg=-1,uint32 abs=0,bool mainhand=true);
	void HandleProcDmgShield(uint32 flag, Unit* attacker);//almost the same as handleproc :P
//	void HandleProcSpellOnSpell(Unit* Victim,uint32 damage,bool critical);//nasty, some spells proc other spells

	int32 GetAP();
	int32 GetRAP();

	void CastSpell(Unit* Target, uint32 SpellID, bool triggered);
	void CastSpell(Unit* Target, SpellEntry* Sp, bool triggered);
	void CastSpell(uint64 targetGuid, uint32 SpellID, bool triggered);
	void CastSpell(uint64 targetGuid, SpellEntry* Sp, bool triggered);
	//void CastSpellAoFSource(float x,float y,float z,SpellEntry* Sp, bool triggered);
	void CastSpellAoFSource( SpellCastTargets& targets, SpellEntry* Sp, bool triggered);
	//void CastSpellAoFDest(float x,float y,float z,SpellEntry* Sp, bool triggered);
	void CastSpellAoFDest( SpellCastTargets& targets, SpellEntry* Sp, bool triggered);
	void EventCastSpell(Unit * Target, SpellEntry * Sp);
	void EventChongzhuang(Unit* Target, SpellEntry * Sp, float x, float y, float z);
	void EventApplyAura(uint64 Victim, uint32 SpellID, int32 Damage, uint32 i, uint32 duration);

	uint32 GetFFA() const;
	void SetFFA( uint32 n );

	uint16 GetDamageSeq(bool bInc=true){
		if(bInc) m_attackcnt++;
		if(!m_attackcnt)
			m_attackcnt++;
		return m_attackcnt;
	}

	bool isCasting();
	bool IsInInstance();
    void CalculateResistanceReduction(Unit *pVictim,dealdamage *dmg,SpellEntry* ability) ;
	void RegenerateHealth();
	void RegeneratePower(bool isinterrupted);
	SUNYOU_INLINE void setHRegenTimer(uint32 time) {m_H_regenTimer = time; }
	SUNYOU_INLINE void setPRegenTimer(uint32 time) {m_P_regenTimer = time; }
	SUNYOU_INLINE void DelayPowerRegeneration(uint32 time) { m_P_regenTimer = time; if (!m_interruptedRegenTime) m_interruptedRegenTime = 2000; }
	void DeMorph();
	uint32 ManaShieldAbsorb(uint32 dmg);
	void smsg_AttackStart(Unit* pVictim);
	void smsg_AttackStop(Unit* pVictim);
	void smsg_AttackStop(uint64 victimGuid);

	bool IsDazed();
	//this function is used for creatures to get chance to daze for another unit
	float get_chance_to_daze(Unit *target);

	// Stealth
	SUNYOU_INLINE int32 GetStealthLevel() { return m_stealthLevel; }
	SUNYOU_INLINE int32 GetStealthDetectBonus() { return m_stealthDetectBonus; }
	SUNYOU_INLINE void SetStealth(uint32 id) { m_stealth = id; }
	SUNYOU_INLINE bool IsStealth() { return (m_stealth!=0 ? true : false); }
	float detectRange;

	// Invisibility
	bool m_invisible;
	uint8 m_invisFlag;
	int32 m_invisDetect[INVIS_FLAG_TOTAL];

	bool HasAura(uint32 spellid);
	bool HasAuraVisual(uint32 visualid);//not spell id!!!
	bool HasActiveAura(uint32 spelllid);
	bool HasActiveAura(uint32 spelllid,uint64);

	void GiveGroupXP(Unit *pVictim, Player *PlayerInGroup);

	/// Combat / Death Status
	SUNYOU_INLINE bool isAlive() { return GetUInt32Value( UNIT_FIELD_DEATH_STATE ) == ALIVE; }
	SUNYOU_INLINE bool isDead() { return  GetUInt32Value( UNIT_FIELD_DEATH_STATE ) != ALIVE; };
	virtual void setDeathState(DeathState s) {
		SetUInt32Value( UNIT_FIELD_DEATH_STATE, s );
	};
	DeathState getDeathState() { return ALIVE; }
	void OnDamageTaken();

	//! Add Aura to unit
	void AddAura(Aura *aur);
	//! Remove aura from unit
	bool RemoveAura(Aura *aur);
	bool RemoveAura(uint32 spellId);
	bool RemoveAura(uint32 spellId,uint64 guid);
	bool RemoveAuraByNameHash(uint32 namehash);//required to remove weaker instances of a spell
	bool RemoveAuraPosByNameHash(uint32 namehash);//required to remove weaker instances of a spell
	bool RemoveAuraNegByNameHash(uint32 namehash);//required to remove weaker instances of a spell
	bool RemoveAuras(uint32 * SpellIds);
	bool CheckMechanic(SpellEntry* se, MECHANICS en, bool& deleteAur, bool& resistAur);

	void EventRemoveAura(uint32 SpellId)
	{
		RemoveAura(SpellId);
	}

	//! Remove all auras
	void RemoveAllNegAuras();
	void RemoveAllAuras(bool bALL=true);
	void RemoveAllChannelAuras();
	bool RemoveAllAuras(uint32 spellId,uint64 guid); //remove stacked auras but only if they come from the same caster. Shaman purge If GUID = 0 then removes all auras with this spellid
    void RemoveAllAuraType(uint32 auratype);//ex:to remove morph spells
	bool RemoveAllAuraByNameHash(uint32 namehash);//required to remove weaker instances of a spell
	bool RemoveAllPosAuraByNameHash(uint32 namehash);//required to remove weaker instances of a spell
	bool RemoveAllNegAuraByNameHash(uint32 namehash);//required to remove weaker instances of a spell
	bool RemoveAllAurasByMechanic( uint32 MechanicType , uint32 MaxDispel , bool HostileOnly ); // Removes all (de)buffs on unit of a specific mechanic type.

	void RemoveNegativeAuras();
	void RemoveAllAreaAuras(bool SelfCast = true);
	// Temporary remove all auras
	   // Find auras
	Aura *FindAuraPosByNameHash(uint32 namehash);
	Aura* FindAura(uint32 spellId);
	Aura* FindAura(uint32 spellId, uint64 guid);
	bool SetAurDuration(uint32 spellId,Unit* caster,uint32 duration);
	bool SetAurDuration(uint32 spellId,uint32 duration);
	   void DropAurasOnDeath();

	void castSpell(Spell * pSpell);
	void InterruptSpell();

	//caller is the caster
	int32 GetSpellDmgBonus(Unit *pVictim, SpellEntry *spellInfo,int32 base_dmg, bool isdot, bool heal, float overWriteStato = 1.0f);
	int32 GetHealSpellBonus(Unit *pVictim, SpellEntry *spellInfo,int32 base_dmg, bool isdot, float overWriteStato = 1.0f);

	Unit* create_guardian(uint32 guardian_entry,uint32 duration,float angle, uint32 lvl = 0);//guardians are temporary spawn that will inherit master faction and will folow them. Apart from that they have their own mind

	uint32 m_addDmgOnce;
	Creature *m_TotemSlots[4];
	uint32 m_ObjectSlots[4];
	uint32 m_triggerSpell;
	uint32 m_triggerDamage;
	uint32 m_canMove;
	bool m_ForceRoot;

	// Spell Effect Variables
	int32 m_silenced;
	bool m_damgeShieldsInUse;
	std::list<struct DamageProc> m_damageShields;
	std::list<struct ReflectSpellSchool*> m_reflectSpellSchool;
 	std::list<struct DamageSplitTarget> m_damageSplitTargets;

	std::list<struct ProcTriggerSpell> m_procSpells;
//	std::map<uint32,ProcTriggerSpellOnSpellList> m_procSpellonSpell; //index is namehash
	std::map<uint32,struct SpellCharge> m_chargeSpells;
	deque<uint32> m_chargeSpellRemoveQueue;
	bool m_chargeSpellsInUse;
	SUNYOU_INLINE void SetOnMeleeSpell(uint32 spell ) { m_meleespell = spell; }
	SUNYOU_INLINE uint32 GetOnMeleeSpell() { return m_meleespell; }

	// Spell Crit
	float spellcritperc;

	// AIInterface
	uint64 m_firststrikeby;
	AIInterface *GetAIInterface() { return m_aiInterface; }
	void ReplaceAIInterface(AIInterface *new_interface) ;
	void ClearHateList();
	void WipeHateList();
	void WipeTargetList();
	SUNYOU_INLINE void setAItoUse(bool value){m_useAI = value;}
	int32 GetThreatModifyer() { return m_threatModifyer; }
	void ModThreatModifyer(int32 mod) { m_threatModifyer += mod; }
	int32 GetGeneratedThreatModifyer() { return m_generatedThreatModifyer; }
	void ModGeneratedThreatModifyer(int32 mod) { m_generatedThreatModifyer += mod; }

	// DK:Affect
	SUNYOU_INLINE bool IsPacified() { return m_pacified; }
	SUNYOU_INLINE bool IsStunned() { return m_stunned; }
	SUNYOU_INLINE bool IsAsleep() {return m_sleep; }
	SUNYOU_INLINE bool IsFeared() { return bool(m_fearmodifiers > 0); }
	SUNYOU_INLINE uint32 GetResistChanceMod() { return m_resistChance; }
	SUNYOU_INLINE void SetResistChanceMod(uint32 amount) { m_resistChance=amount; }

	SUNYOU_INLINE uint16 HasNoInterrupt() { return m_noInterrupt; }
	bool setDetectRangeMod(uint64 guid, int32 amount);
	void unsetDetectRangeMod(uint64 guid);
	int32 getDetectRangeMod(uint64 guid);
	void Heal(Unit* target,uint32 SpellId, uint32 amount);
	void Energize(Unit* target,uint32 SpellId, uint32 amount, uint32 type);

	Loot loot;
	uint32 SchoolCastPrevent[7];
	int32 GetDamageDoneMod(uint32 school);
	float GetDamageDonePctMod(uint32 school);
	float DamageDoneModPCT[7];
	int32 DamageTakenMod[7];
	float DamageTakenPctMod[7];
	float DamageTakenPctModOnHP35;
	float CritMeleeDamageTakenPctMod[7];
	float CritRangedDamageTakenPctMod[7];
	int32 RangedDamageTaken;
	virtual void CalcDamage();
	float BaseDamage[2];			//如果是玩家,存储的是武器的最大最小攻击力,如果是怪物,存储的是本身的最大最小攻击力
	float BaseOffhandDamage[2];
	float BaseRangedDamage[2];
	SchoolAbsorb Absorbs[7];
	SchoolAbsorb AllAbsorb;
	uint32 AbsorbDamage(uint32 School,uint32 * dmg);//returns amt of absorbed dmg, decreases dmg by absorbed value
	int32 RAPvModifier;
	int32 APvModifier;
	uint64 stalkedby;
	uint32 dispels[10];
	bool trackStealth;
	uint32 MechanicsDispels[MECHANIC_MAX];
	int8 MechanicsDispelsCnt[MECHANIC_MAX];
	uint32 MechanicsDispelsAura[MECHANIC_MAX];
	float MechanicsResistancesPCT[MECHANIC_MAX];
	uint32 MechanicsProbability[MECHANIC_MAX];
	float ModDamageTakenByMechPCT[MECHANIC_MAX];
	uint32 extradamage;
	//int32 RangedDamageTakenPct;

	//SM
	int32 * SM_CriticalChance;//flat
	int32 * SM_FDur;//flat
	int32 * SM_PDur;//pct
	int32 * SM_PRadius;//pct
	int32 * SM_FRadius;//flat
	int32 * SM_PRange;//pct
	int32 * SM_FRange;//flat
	int32 * SM_PCastTime;//pct
	int32 * SM_FCastTime;//flat
	int32 * SM_PCriticalDamage;
	int32 * SM_PDOT;//pct
	int32 * SM_FDOT;//flat
	int32 * SM_FEffectBonus;//flat
	int32 * SM_PEffectBonus;//pct
	int32 * SM_FDamageBonus;//flat
	int32 * SM_PDamageBonus;//pct
	int32 * SM_PSPELL_VALUE[3];//pct
	int32 * SM_FSPELL_VALUE[3];//flat
	int32 * SM_FHitchance;//flat
	int32 * SM_PAPBonus;//pct
	int32 * SM_PCost;
	int32 * SM_FCost;
	int32 * SM_PNonInterrupt;
	int32 * SM_PJumpReduce;
	int32 * SM_FSpeedMod;
	int32 * SM_FAdditionalTargets;
	int32 * SM_FPenalty;//flat
	int32 * SM_PPenalty;//Pct
	int32 * SM_PCooldownTime;
	int32 * SM_FCooldownTime;
	int32 * SM_FChanceOfSuccess;
	int32 * SM_FRezist_dispell;
	int32 * SM_PRezist_dispell;
	int32 * SM_FThreatReduce;
	int32 * SM_PThreatReduce;
	int32 * SM_FStackCount;
	void InheritSMMods(Unit *inherit_from);

	//Events
	void Emote (EmoteType emote);
	void EventAddEmote(EmoteType emote, uint32 time);
	void EmoteExpire();
	SUNYOU_INLINE void setEmoteState(uint8 emote) { m_emoteState = emote; };
	SUNYOU_INLINE uint32 GetOldEmote() { return m_oldEmote; }
	void EventSummonPetExpire();
	void EventAurastateExpire(uint32 aurastateflag){RemoveFlag(UNIT_FIELD_AURASTATE,aurastateflag);} //hmm this looks like so not necesary :S
	void EventHealthChangeSinceLastUpdate();

	void SetStandState (uint8 standstate);

	SUNYOU_INLINE StandState GetStandState()
	{
		uint32 bytes1 = GetUInt32Value (UNIT_FIELD_BYTES_1);
		return StandState (uint8 (bytes1));
	}

	void SendChatMessage(uint8 type, uint32 lang, const char *msg);
	void SendChatMessageToPlayer(uint8 type, uint32 lang, const char *msg, Player *plr);
	void SendChatMessageAlternateEntry(uint32 entry, uint8 type, uint32 lang, const char * msg);
	void RegisterPeriodicChatMessage(uint32 delay, uint32 msgid, std::string message, bool sendnotify);

	int GetHealthPct();
    SUNYOU_INLINE void SetHealthPct(uint32 val) { if (val>0) SetUInt32Value(UNIT_FIELD_HEALTH,float2int32(val*0.01f*GetUInt32Value(UNIT_FIELD_MAXHEALTH))); };
	SUNYOU_INLINE int GetManaPct() { return (int)(GetUInt32Value(UNIT_FIELD_POWER1) * 100 / GetUInt32Value(UNIT_FIELD_MAXPOWER1)); };

	float GetResistance(uint32 type);

	//Pet
	SUNYOU_INLINE void SetIsPet(bool chck) { m_isPet = chck; }

	//In-Range
	virtual void AddInRangeObject(Object* pObj);
	virtual void OnRemoveInRangeObject(Object* pObj);
	void ClearInRangeSet();

	SUNYOU_INLINE Spell * GetCurrentSpell(){return m_currentSpell;}

	void SetCurrentSpell(Spell* cSpell);

	uint32 m_CombatUpdateTimer;

	SUNYOU_INLINE void setcanperry(bool newstatus){can_parry=newstatus;}

	std::map<uint32,Aura*> tmpAura;
	uint32 m_spellAuras[TOTAL_SPELL_AURAS];
	Aura* m_spellAurasEffectPtr[TOTAL_SPELL_AURAS];

	uint32 BaseResistance[7]; //there are resistances for silence, fear, mechanics ....
	float BaseStats[6];
	int32 HealDoneMod[7];
	int32 HealDonePctMod[7];
	int32 HealTakenMod[7];
	float HealTakenPctMod[7];
	uint32 SchoolImmunityList[7];
	float SpellCritChanceSchool[7];
	int32 PowerCostMod[7];
	float PowerCostPctMod[7]; // armor penetration & spell penetration
	int32 AttackerCritChanceMod[7];
	uint32 SpellDelayResist[7];
	int32 CreatureAttackPowerMod[12];
	int32 CreatureRangedAttackPowerMod[12];

	float PctRegenModifier;//1.0 by default
	float PctPowerRegenModifier[4];
	SUNYOU_INLINE uint32 GetPowerType(){ return (GetUInt32Value(UNIT_FIELD_BYTES_0)>> 24);}
	void RemoveSoloAura(uint32 type);

	void RemoveAurasByInterruptFlag(uint32 flag);
	void RemoveAurasByInterruptFlagButSkip(uint32 flag, uint32 skip);
	// Auras Modifiers
	int32 m_pacified;
	int32 m_interruptRegen;
	int32 m_resistChance;
	int32 m_powerRegenPCT;
	int32 m_stunned;
	int32 m_sleep;
	int32 m_extraattacks;
	int32 m_extrastriketargets;
	int32 m_fearmodifiers;

	int32 m_extraRegenHealPoint;
	float m_extraRegenHealPCT;

	int32 m_extraRegenManaPoint;
	float m_extraRegenManaPointPCT;
	//std::set<SpellEntry*> m_onStrikeSpells;

	int32 m_extraaddhealthpoint;

	int32 m_noInterrupt;
	int32 m_rooted;
	int32 m_stormed;
	bool disarmed;
	uint64 m_detectRangeGUID[5];
	int32  m_detectRangeMOD[5];
	// Affect Speed
	int32 m_speedModifier;
	int32 m_slowdown;
	float m_maxSpeed;
	map< uint32, int32 > speedReductionMap;
	bool GetSpeedDecrease();
	int32 m_mountedspeedModifier;
	int32 m_flyspeedModifier;
	bool m_powerRegenerateLock;
	void UpdateSpeed();
	void EnableFlight();
	void DisableFlight();

	// Escort Quests
	//uint32 m_escortquestid;
	//uint32 m_escortupdatetimer;
	//bool bHasEscortQuest;
	//bool bEscortActive;
	//bool bStopAtEndOfWaypoints;
	//bool bReturnOnDie;
	//Player *q_AttachedPlayer;
	//uint16 m_escortStartWP;
	//uint16 m_escortEndWP;
	/*void InitializeEscortQuest(uint32 questid, bool stopatend, bool returnondie);
	void EscortSetStartWP(uint32 wp);
	void EscortSetEndWP(uint32 wp);
	void StartEscortQuest();
	void PauseEscortQuest();
	void EndEscortQuest();*/
	void MoveToWaypoint(uint32 wp_id);
	void PlaySpellVisual(uint64 target, uint32 spellVisual);

	void RemoveStealth()
	{
		if( m_stealth != 0 )
		{
			RemoveAura( m_stealth );
			m_stealth = 0;
		}
	}

	bool m_isPet;
	uint32 m_stealth;
	bool m_can_stealth;

	Aura* m_auras[MAX_AURAS+MAX_PASSIVE_AURAS];

	int32 m_modlanguage;

	Creature *critterPet;
	Creature *summonPet;

	SUNYOU_INLINE uint32 GetCharmTempVal() { return m_charmtemp; }
	SUNYOU_INLINE void SetCharmTempVal(uint32 val) { m_charmtemp = val; }
	set<uint32> m_SpellList;

	SUNYOU_INLINE void DisableAI() { m_useAI = false; }
	SUNYOU_INLINE void EnableAI() { m_useAI = true; }
	inline bool IsIceBlocking() const { return 1 == GetUInt32Value( UNIT_FIELD_ICE_BLOCK ); }

	SUNYOU_INLINE void SetPowerType(uint8 type)
	{
		SetByte(UNIT_FIELD_BYTES_0,3,type);
	}

	SUNYOU_INLINE bool IsSpiritHealer()
	{
		switch(GetEntry())
		{
		case 6491:  // Spirit Healer
		case 13116: // Alliance Spirit Guide
		case 13117: // Horde Spirit Guide
			{
				return true;
			}break;
		}
		return false;
	}

	void Storm();
	void Unstorm();
	void Root( bool Force );
	void Unroot( bool Force );

	void SetFacing(float newo);//only working if creature is idle

	void RemoveAurasByBuffIndexType(uint32 buff_index_type, const uint64 &guid);
	void RemoveAurasByBuffType(uint32 buff_type, const uint64 &guid,uint32 skip);
	bool HasAurasOfBuffType(uint32 buff_type, const uint64 &guid,uint32 skip);
	int	 HasAurasWithNameHash(uint32 name_hash);
	bool HasNegativeAuraWithNameHash(uint32 name_hash); //just to reduce search range in some cases
	bool HasNegativeAura(uint32 spell_id); //just to reduce search range in some cases
	bool IsPoisoned();

	AuraCheckResponse AuraCheck(uint32 name_hash, uint32 rank, Object *caster=NULL);
	AuraCheckResponse AuraCheck(uint32 name_hash, uint32 rank, Aura* aur, Object *caster=NULL);

	uint16 m_diminishCount[23];
	uint8  m_diminishAuraCount[23];
	uint16 m_diminishTimer[23];
	bool   m_diminishActive;

	void SetDiminishTimer(uint32 index)
	{
		m_diminishTimer[index] = 15000;
	}

	DynamicObject * dynObj;

	uint32 AddAuraVisual(uint32 spellid, uint32 count, bool positive);
	void SetAuraSlotLevel(uint32 slot, bool positive);

	void RemoveAuraVisual(uint32 spellid, uint32 count, uint32 slot);
	bool HasVisibleAura(uint32 spellid);

	//! returns: aura stack count
	uint32 ModAuraStackCount(uint32 slot, int32 count);
	uint8 m_auraStackCount[MAX_AURAS];

	void RemoveAurasOfSchool(uint32 School, bool Positive, bool Immune);
	SpellEntry * pLastSpell;
	bool bProcInUse;
	bool bInvincible;
	Player * m_redirectSpellPackets;
	void UpdateVisibility();

	//solo target auras
	uint32 polySpell;
	uint32 m_special_state; //flags for special states (stunned,rooted etc)

//	uint32 fearSpell;
	uint32 m_cTimer;
	void EventUpdateFlag();
	CombatStatusHandler CombatStatus;
	bool m_temp_summon;

	void CancelSpell(Spell * ptr);
	void EventStrikeWithAbility(uint64 guid, SpellEntry * sp,  int32 add_damage, int32 pct_dmg_mod, uint32 exclusive_damage, bool disable_proc, bool skip_hit_check, bool School, bool stato, bool weapon );
	void EventHealHPWithAbility(uint64 guid, SpellEntry * sp, uint32 damage);
	void EventHealMPWithAbility(uint64 guid, SpellEntry * sp, uint32 damage);
	bool m_spellsbusy;
	void DispelAll(bool positive);

	bool HasAurasOfNameHashWithCaster(uint32 namehash, Unit * caster);
	int8 m_hasVampiricTouch;
	int8 m_hasVampiricEmbrace;
	bool m_hasBlessOfFreedom;
	uint32 m_fearDamageTaken;
	bool IsPlayerAndFlying();
	uint32 lastSpellDamage;

protected:
	Unit ();

	uint32 m_meleespell;
	void _UpdateSpells(uint32 time);

	uint32 m_H_regenTimer;
	uint32 m_P_regenTimer;
	uint32 m_interruptedRegenTime; //PowerInterruptedegenTimer.
	uint32 m_state;		 // flags for keeping track of some states
	uint32 m_attackTimer;   // timer for attack
	uint32 m_attackTimer_1;
	bool m_duelWield;
	uint32 m_attackcnt;

	/// Combat
	//DeathState m_deathState;

	// Stealth
	uint32 m_stealthLevel;
	uint32 m_stealthDetectBonus;

	// DK:pet
	//uint32 m_pet_state;
	//uint32 m_pet_action;

	// Spell currently casting
	Spell * m_currentSpell;

	// AI
	AIInterface *m_aiInterface;
	bool m_useAI;
	bool can_parry;//will be enabled by block spell
	int32 m_threatModifyer;
	int32 m_generatedThreatModifyer;

	//	float getDistance( float Position1X, float Position1Y, float Position2X, float Position2Y );

	int32 m_manashieldamt;
	uint32 m_manaShieldId;

	// Quest emote
	uint8 m_emoteState;
	uint32 m_oldEmote;

	uint32 m_charmtemp;

	bool m_extraAttackCounter;
};

struct AreaAura
{
	uint32 auraid;
	Unit* caster;
};

#endif
