#include "stdafx.h"
#include "../../../new_common/Source/utilities/call_back.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "WebInterface.h"
#include "BroadTextMgr.h"
#include "../Master.h"

CWebInterface gWebInterface;
void CWebInterface::Init()
{
	MyLog::log->notice( "Initialized Web Server ..." );
	ctx = mg_start();
	mg_set_option(ctx, "ssl_cert", "ssl_cert.pem");
	mg_set_option(ctx, "aliases", ALIAS_URI "=" ALIAS_DIR);
	mg_set_option(ctx, "ports", "8080,8081s");
	//mg_set_uri_callback(ctx, "/manage", &show_index, (void *) &data);
	//mg_set_auth_callback(ctx, "/*", &on_authorize, NULL);
	mg_set_uri_callback(ctx, "/login", &on_login_page, NULL);
	mg_set_uri_callback(ctx, "/shutdown", &on_shutdown_server, NULL);
	mg_set_uri_callback(ctx, "/playerlist", &on_player_list, NULL);
	mg_set_uri_callback(ctx, "/showmsg", &on_show_msg, NULL);

	mg_set_uri_callback(ctx, "/role", &on_role_main, NULL);
	mg_set_uri_callback(ctx, "/guild", &on_guild_main, NULL);
	mg_set_uri_callback(ctx, "/bulletin", &on_bulletin_main, NULL);
	mg_set_uri_callback(ctx, "/exp", &on_exp_main, NULL);
}

void CWebInterface::Update()
{
	m_cb_mgr.poll();
}

void CWebInterface::Release()
{
	mg_stop(ctx);
}
/*
* This callback function is attached to the "/" and "/abc.html" URIs,
* thus is acting as "index.html" file. It shows a bunch of links
* to other URIs, and allows to change the value of program's
* internal variable. The pointer to that variable is passed to the
* callback function as arg->user_data.
*/
static void
on_show_index(struct mg_connection *conn,
		   const struct mg_request_info *request_info,
		   void *user_data)
{

	char		*value;
	const char	*host;

	mg_printf(conn, "%s",
		"HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n"
		"<html><body><h1>欢迎来到君天下服务器管理页面");
	mg_printf(conn, " v. %s </h1><ul>", "0.1");

	if(user_data)
		mg_printf(conn, "<li>%s<hr>",
		(char*)user_data
		);

	mg_printf(conn, "<li><code>REQUEST_METHOD: %s "
		"REQUEST_URI: \"%s\" QUERY_STRING: \"%s\""
		" REMOTE_ADDR: %lx REMOTE_USER: \"(null)\"</code><hr>",
		request_info->request_method, request_info->uri,
		request_info->query_string ? request_info->query_string : "(null)",
		request_info->remote_ip);

	mg_printf(conn, "%s",
		"<hr><li>"
		"<form method=\"POST\"action=\"/shutdown\">"
		"<input type=\"text\" name=\"arg\"/ default=""15"" size = ""10"">"
		"秒后  "
		"<input type=\"submit\" value=""关闭服务器""><hr></form>");
		//"<a href=\"/shutdown\">关闭服务器</a><hr></form>");
	mg_printf(conn, "%s","<hr><li><a href=\"/playerlist\">在线玩家分布情况</a><hr>");
	
	host = mg_get_header(conn, "Host");
	mg_printf(conn, "<li>'Host' header value: [%s]<hr>",
		host ? host : "NOT SET");

	mg_printf(conn, "<li>Upload file example. "
		"<form method=\"post\" enctype=\"multipart/form-data\" "
		"action=\"/post\"><input type=\"file\" name=\"file\">"
		"<input type=\"submit\"></form>");

	mg_printf(conn, "%s", "</body></html>");
}

static void on_account_main(struct mg_connection *conn,const struct mg_request_info *request_info,void *user_data)
{
	

	char* action = mg_get_var(conn, "action");
	if(!action)
		return;
	char* acct = mg_get_var(conn, "user");
	if(!acct)
		return;
	if(!strcmp(action, "get"))
	{
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_account_status_get, &gWebInterface, conn, string(acct));
	}
}

void CWebInterface::on_account_status_get(mg_connection* conn, string acct)
{
	

	/*WorldSession * pSession = sWorld.FindSession(atoi(acct));
 	if( pSession != NULL )
 	{
 		if(pSession->m_currMsTime > getMSTime())
 			mg_printf(conn, "status:%d : %s", pSession->m_muted, ConvertTimeStampToDataTime(pSession->m_muted));
 		else
 			;
 	}*/
}

static void on_exp_main(struct mg_connection* conn, const struct mg_request_info* ri, void *data)
{
	

	char* action = mg_get_var(conn, "action");
	if(!action)
		mg_printf(conn, "ACTION IS ERROR!");
		mg_printf(conn, "0");
		return;
	if(!strcmp(action, "get"))
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_exp_get, &gWebInterface, conn);
	else if(!strcmp(action, "set"))
	{
		char* start = mg_get_var(conn, "start_time");
		char* end = mg_get_var(conn, "end_time");
		char* ratio = mg_get_var(conn, "ratio");
		if(!start || !end || !ratio)
			return;
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_exp_set, &gWebInterface, conn, string(start), string(end), string(ratio));
	}
}

void CWebInterface::on_exp_get(mg_connection* conn)
{
	
	mg_printf(conn, "ratio:%d,duration:%s", sWorld.m_ExtraExpRatio, ConvertTimeStampToString(sWorld.m_ExtraExpDuration).c_str());
}

void CWebInterface::on_exp_set(mg_connection* conn, string start, string end, string ratio)
{
	
	sWorld.m_ExtraExpRatio = atoi(ratio.c_str());
	sWorld.m_ExtraExpDuration = UNIXTIME + GetTimePeriodFromString(end.c_str());
}

static void on_bulletin_main(struct mg_connection* conn, const struct mg_request_info* ri, void *data)
{
	
	char* action;
	action = mg_get_var(conn, "action");
	if(!action)
		return;
	if(!strcmp(action, "get"))
	{
		
	}
	else if(!strcmp(action, "set"))
	{
		char* id = mg_get_var(conn, "id");
		char* tp = mg_get_var(conn, "type");
		char* notify = mg_get_var(conn, "notify");
		if(!id || !tp || !notify)
			return;
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_bulletin_add, &gWebInterface, conn, string(id), string(tp), string(notify));

		mg_free(id);
		mg_free(tp);
		mg_free(notify);
	}
	mg_free(action);
}

void CWebInterface::on_bulletin_add(mg_connection* conn, string id, string type, string notify)
{
	

	stSystemNotify* pNotify = new stSystemNotify;
	
	pNotify->tips	= strdup(notify.c_str());
	pNotify->id		= atoi(id.c_str());
	pNotify->ntype	= atoi(type.c_str());

	SystemNotifyStorage.SetEntry(pNotify->id, pNotify);
	WorldDatabase.Execute("REPLACE INTO SystemNotify VALUES(%u, %u, % s)", pNotify->id, pNotify->ntype, pNotify->tips);

	MSG_S2C::stSystemNotifyAdd Msg;
	Msg.id = pNotify->id;
	Msg.ntype = pNotify->ntype;
	Msg.tips = pNotify->tips;
	sWorld.SendGlobalMessage(Msg);
}

static void on_guild_main(struct mg_connection* conn, const struct mg_request_info* ri, void *data)
{
	

	char* action,*name,*type,*value;
	action = mg_get_var(conn, "action");
	if(!action)
		return;
	name = mg_get_var(conn, "name");
	if(!name)
		return;
	value = mg_get_var(conn, "value");
	type = mg_get_var(conn, "type");
	if(!strcmp(action, "get"))
	{
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_guild_info, &gWebInterface, conn, string(name));
	}
	else if(!strcmp(action, "set"))
	{
		if(!strcmp(type, "dissolve"))
		{
			gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_guild_set_disband, &gWebInterface, conn, string(name));
		}
		else if(!strcmp(type, "name"))
		{
			if(value)
				gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_guild_set_name, &gWebInterface, conn, string(name), string(value));
		}
		else if(!strcmp(type, "description"))
		{
			if(value)
				gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_guild_set_description, &gWebInterface, conn, string(name), string(value));
		}
	}
	
	if(action)
		mg_free(action);
	if(name)
		mg_free(name);
	if(value)
		mg_free(value);
	if(type)
		mg_free(type);
}

void CWebInterface::on_guild_set_description(mg_connection* conn, string guild_name, string desc)
{
	

	Guild* pGuild = objmgr.GetGuildByGuildName(guild_name);
	if(!pGuild)
	{
		mg_printf(conn, "没有该名字的部族");
		mg_printf(conn, "操作失败:0");
		return;
	}
	pGuild->SetGuildInformation(desc.c_str(), NULL);
	mg_printf(conn, "操作成功:1");
}

void CWebInterface::on_guild_set_name(mg_connection* conn, string guild_name, string new_name)
{
	

	Guild* pGuild = objmgr.GetGuildByGuildName(guild_name);
	if(!pGuild)
	{
		mg_printf(conn, "没有该名字的部族");
		mg_printf(conn, "操作失败:0");
		return;
	}
	if (pGuild->ChangeGuildName(new_name))
	{
		mg_printf(conn, "操作成功:1");
	}else
	{
		mg_printf(conn, "%s该部族已经存在", new_name.c_str());
		mg_printf(conn, "操作失败:0");
	}
	
}

void CWebInterface::on_guild_set_disband(mg_connection* conn, string guild_name)
{
	

	Guild* pGuild = objmgr.GetGuildByGuildName(guild_name);
	if(!pGuild)
	{
		mg_printf(conn, "没有该名字的部族");
		mg_printf(conn, "操作失败:0");
		return;
	}

	pGuild->Disband();
	mg_printf(conn, "操作成功:1");
}

void CWebInterface::on_guild_info(mg_connection* conn, string guild_name)
{
	

	Guild* pGuild = objmgr.GetGuildByGuildName(guild_name);
	if(!pGuild)
	{
		mg_printf(conn, "没有该名字的部族");
		mg_printf(conn, "操作失败:0");
		return;
	}
	mg_printf(conn, "部族:%s,", guild_name.c_str());
	mg_printf(conn, "等级:%d,", pGuild->GetLevel());
	mg_printf(conn, "人数:%d,", pGuild->GetNumMembers());
	mg_printf(conn, "创建时间:%s,", ConvertTimeStampToDataTime(pGuild->GetCreationTime()).c_str());
	mg_printf(conn, "描述:%s,", pGuild->GetGuildInformation());
	PlayerInfo* pInfo = objmgr.GetPlayerInfo(pGuild->GetGuildLeader());
	if(pInfo)
		mg_printf(conn, "会长:%s,", pInfo->name);
	mg_printf(conn, "操作成功:1");
}

static void on_role_main(struct mg_connection* conn, const struct mg_request_info* ri, void *data)
{
	

	char* action,*user, *role,*account,*type,*value;
	action = mg_get_var(conn, "action");
	if(!action)
		return;
	user = mg_get_var(conn, "user");
	role = mg_get_var(conn, "role");
	account = mg_get_var(conn, "account");
	type = mg_get_var(conn, "");
	value = mg_get_var(conn, "value");
	if(!strcmp(action, "get"))
	{
		if(role)
		{
			gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_role_get, &gWebInterface, conn, string(role));
		}
 		else if(account)
 		{
 		
 		}
 		else
 		{
 			mg_printf(conn, "HTTP/1.1 401 OK\r\n"
 				"content-Type: text/html\r\n\r\n");
 		}
	}
	else if(!strcmp(action, "kick"))
	{
		if(!role)return;
		char* reason = mg_get_var(conn, "reason");
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_role_kick, &gWebInterface, conn, string(role), string(reason));
		if(reason)
			mg_free(reason);
	}
	else if(!strcmp(action, "silent"))
	{
		if(!role)
		{
			mg_printf(conn, "role is not allow null");
			mg_printf(conn, "0");
			return;
		}
		char* reason = mg_get_var(conn, "reason");
		if(!reason) 
		{
			mg_printf(conn, "reason is not allow null");
			mg_printf(conn, "0");
			return;
		}
		char* starttime = mg_get_var(conn, "start_time");
		if(!starttime) 
		{
			mg_printf(conn, "starttime is not allow null");
			mg_printf(conn, "0");	
			return;
		}
		char* endtime = mg_get_var(conn, "end_time");
		if(!endtime)
		{
			mg_printf(conn, "endtime is not allow null");
			mg_printf(conn, "0");	
			return;
		}
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_role_silent, &gWebInterface, conn, string(role), string(reason), string(starttime), string(endtime));
		if(reason)mg_free(reason);
		if(starttime)mg_free(starttime);
		if(endtime)mg_free(endtime);
	}
	else if(!strcmp(action, "desilent"))
	{
		if(!role)
			return;
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_role_desilent, &gWebInterface, conn, string(role));
	}
	else if(!strcmp(action, "shift"))
	{
		if(!role)
			return;
		char* mapid,*x,*y,*z;
		mapid = mg_get_var(conn, "mapid");
		x = mg_get_var(conn, "x");
		y = mg_get_var(conn, "y");
		z = mg_get_var(conn, "z");
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_role_shift, &gWebInterface, conn, string(role), string(mapid), string(x), string(y), string(z));
		if(mapid)mg_free(mapid);
		if(x)mg_free(x);
		if(y)mg_free(y);
		if(z)mg_free(z);
	}else if (!strcmp(action, "rename"))
	{
		//http://ip//role?action=rename&role=角色&newname=新的角色名
		if (!role)
			return ;
		char* player_new_name = mg_get_var(conn, "newname");
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_role_updatename, &gWebInterface, conn, string(role), string(player_new_name));
		if (player_new_name)
			mg_free(player_new_name);
	}else if (!strcmp(action, "recover"))
	{
		//http://ip//role?action=recover&role=角色
		if (!role)
			return ;
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_role_recoverrole, &gWebInterface,conn,string(role));
	}else if (!strcmp(action, "close"))
	{
		//http://ip//role?action=close&role=角色&reason=原因&bantime=时间
		if (!role)
			return ;
		char* reason, *bantime ;
		reason = mg_get_var(conn, "reason");
		bantime = mg_get_var(conn, "bantime");
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::on_role_closerole, &gWebInterface,conn,string(role),string(reason),string(bantime));
		if (reason)
			mg_free(reason);
		if (bantime)
			mg_free(bantime);
	}
	

	if(action)
		mg_free(action);
	if(role)
		mg_free(role);
	if (user)
		mg_free(user);
	if(account)
		mg_free(account);
	if(type)
		mg_free(type);
	if(value)
		mg_free(value);
}

void CWebInterface::on_role_shift(mg_connection* conn, string player_name, string mapid, string x, string y, string z)
{
	

	PlayerInfo* pinfo = objmgr.GetPlayerInfoByName(player_name.c_str());
	if (!pinfo)
	{
		mg_printf(conn,"player %s is not found", player_name.c_str());
		mg_printf(conn,"操作失败:0");
		return ;
	}

	if(!pinfo->m_loggedInPlayer)
	{
		mg_printf(conn, "%s Not Online", player_name.c_str());
		mg_printf(conn,"操作失败:0");
		return;
	}

	uint32 nmapid = atoi(mapid.c_str());
	float fx = atof(x.c_str());
	float fy = atof(y.c_str());
	float fz = atof(z.c_str());
	if(fx >= _maxX || fx <= _minX || fy <= _minY || fy >= _maxY)
	{
		mg_printf(conn, "coordinate is error ");
		mg_printf(conn,"操作失败:0");
		return;
	}

	LocationVector vec(fx, fy, fz);
	pinfo->m_loggedInPlayer->SafeTeleport(nmapid, 0, vec);
	mg_printf(conn,"操作成功:1");
}

void CWebInterface::on_role_silent(mg_connection* conn, string player_name, string reason, string start_time, string end_time)
{

	PlayerInfo* pinfo = objmgr.GetPlayerInfoByName(player_name.c_str());
	if (!pinfo)
	{
		mg_printf(conn,"player %s is not found", player_name.c_str());
		mg_printf(conn,"操作失败:0");
		return ;
	}

	if(!pinfo->m_loggedInPlayer)
	{
		mg_printf(conn, "%s Not Online", player_name.c_str());
		mg_printf(conn, "0");
		return;
	}
	
	WorldSession * pSession = pinfo->m_loggedInPlayer->GetSession();
	if( pSession != NULL )
	{
		pSession->m_muted = (uint32)UNIXTIME + GetTimePeriodFromString(end_time.c_str());
		pSession->m_mutereason = reason;
		pSession->SystemMessage("你已经被禁言"/*, tsstr.c_str()*/);
		mg_printf(conn,"操作成功:1");
		return ;
	}
	mg_printf(conn,"操作失败:0");
}

void CWebInterface::on_role_desilent(mg_connection* conn, string player_name)
{
	

	PlayerInfo* pinfo = objmgr.GetPlayerInfoByName(player_name.c_str());
	if (!pinfo)
	{
		mg_printf(conn,"player %s is not found", player_name.c_str());
		mg_printf(conn,"操作失败:0");
		return ;
	}

	if(!pinfo->m_loggedInPlayer)
	{
		mg_printf(conn, "%s Not Online", player_name.c_str());
		mg_printf(conn, "0");
		return;
	}
	WorldSession * pSession = pinfo->m_loggedInPlayer->GetSession();
	if( pSession != NULL )
	{
		pSession->m_muted = 0;
		pSession->SystemMessage("你的禁言解除了.");
		mg_printf(conn,"操作成功:1");
		return ;
	}
	mg_printf(conn,"操作失败:0");
}

void CWebInterface::on_role_kick(mg_connection* conn, string player_name, string reason)
{
	

	if (!player_name.length())
	{
		mg_printf(conn,"name is not allow null");
		mg_printf(conn,"操作失败:0");
		return ;
	}
	PlayerInfo* pinfo = objmgr.GetPlayerInfoByName(player_name.c_str());
	if (!pinfo)
	{
		mg_printf(conn,"player %s is not found", player_name.c_str());
		mg_printf(conn,"操作失败:0");
		return ;
	}
	
	if(pinfo->m_loggedInPlayer)
	{
		pinfo->m_loggedInPlayer->GetSession()->SystemMessage(reason.c_str());
		pinfo->m_loggedInPlayer->Kick(1000);
		mg_printf(conn,"操作成功:1");
		return ;
	}else
	{
		mg_printf(conn, "%s Not Online", player_name.c_str());
		mg_printf(conn, "0");
		return;
	}
}
void CWebInterface::on_role_updatename(mg_connection* conn, string player_name, string player_new_name)
{
	

	PlayerInfo* info = objmgr.GetPlayerInfoByName(player_new_name.c_str());
	if (info)
	{
		mg_printf(conn, "the name %s is be used by other player!", player_new_name.c_str());
		mg_printf(conn,"操作失败:0");
		return ;
	}else
	{
		PlayerInfo* Playerinfo = objmgr.GetPlayerInfoByName(player_name.c_str());
		if (Playerinfo)
		{
			Player * plr = objmgr.GetPlayer(Playerinfo->guid);

			objmgr.RenamePlayerInfo(Playerinfo, player_name.c_str(), player_new_name.c_str());
			free(Playerinfo->name);
			Playerinfo->name = strdup(player_new_name.c_str());

			if(plr != 0)
			{
				plr->SetName(player_name);
				plr->SaveToDB(false);
				mg_printf(conn,"操作成功:1");
			}else
			{

				unsigned long namecrc = crc32((const unsigned char*)player_new_name.c_str(),player_new_name.length());
				if (CharacterDatabase.WaitExecute("UPDATE characters SET name = '%s' , crcname= %u WHERE guid = %u",CharacterDatabase.EscapeString(player_new_name).c_str(), namecrc, (uint32)Playerinfo->guid))
				{
					mg_printf(conn,"操作成功:1");

				}else
				{
					mg_printf(conn,"操作失败:0");
				}
			}
			
			return ;
		}else
		{
			mg_printf(conn, "has not found player:%s", player_name.c_str());
			mg_printf(conn,"操作失败:0");
		}	
	}
	
}
void CWebInterface::on_role_recoverrole(mg_connection* conn, string player_name)
{
	

	PlayerInfo* Playerinfo = objmgr.GetPlayerInfoByName(player_name.c_str());
	
	if (Playerinfo)
	{
		CharacterDatabase.WaitExecute("UPDATE characters SET del = 0 WHERE guid = %u", (uint32)Playerinfo->guid);
		mg_printf(conn,"操作成功:1");
	}else
	{
		mg_printf(conn, "has not found player:%s", player_name.c_str());
		mg_printf(conn,"操作失败:0");
		return ;
	}
	
}
void CWebInterface::on_role_closerole(mg_connection* conn, string player_name,string reason , string ban_time)
{
	

	if (!reason.length() || !ban_time.length())
	{
		mg_printf(conn,"reason or ban_time null");
		mg_printf(conn,"操作失败:0");
		return  ;
	}
	int32 BanTime = GetTimePeriodFromString(ban_time.c_str());
	PlayerInfo* Playerinfo = objmgr.GetPlayerInfoByName(player_name.c_str());
	if (!Playerinfo)
	{
		mg_printf(conn,"player %s is not found", player_name.c_str());
		mg_printf(conn,"操作失败:0");
		return ;
	}
	
	Player * pPlayer = objmgr.GetPlayer(Playerinfo->guid);


	if (BanTime < 1)
	{
		mg_printf(conn,"bantime is not allow  0");
		mg_printf(conn,"操作失败:0");
		return  ;
	}

	if (Playerinfo)
	{
		if (pPlayer)
		{
			pPlayer->GetSession()->SystemMessage("封停账号 '%s', 原因: '%s'.", player_name.c_str(), reason.c_str());
			uint32 uBanTime = BanTime ? BanTime+(uint32)UNIXTIME : 1;
			pPlayer->GetSession()->m_baned = uBanTime;
			pPlayer->GetSession()->m_mutereason = reason;
		
			pPlayer->Kick();
		}else
		{
			string escaped_reason = CharacterDatabase.EscapeString(reason);
			CharacterDatabase.WaitExecute("UPDATE characters SET banned = %u, banReason = '%s' WHERE guid = %u",
				BanTime ? BanTime+(uint32)UNIXTIME : 1, escaped_reason.c_str(), Playerinfo->guid);
		}
		mg_printf(conn,"操作成功:1");

	}
}
void CWebInterface::on_role_get(struct mg_connection* conn, string player_name)
{
	

	PlayerInfo* pinfo = objmgr.GetPlayerInfoByName(player_name.c_str());
	if(!pinfo)
	{
		mg_printf(conn, "%s not found", player_name.c_str());
		mg_printf(conn, "0");
		return;
	}
	mg_printf(conn, "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n");

	std::string race = "人族";
	if (pinfo->race == RACE_YAO)
	{
		race = "妖族";
	}else if (pinfo->race == RACE_WU)
	{
		race = "巫族";
	}
	std::string Class = "";
	mg_printf(conn, "种族:%s, ", race.c_str());
	mg_printf(conn, "职业:%s, ", class_name[pinfo->cl]);
	mg_printf(conn, "在线:%d, ", pinfo->m_loggedInPlayer?1:0);
	mg_printf(conn, "等级:%d, ", pinfo->lastLevel);
	mg_printf(conn, "荣誉:%d, ", pinfo->total_honor);
	mg_printf(conn, "账号ID:%d,", pinfo->acct);

	if(pinfo->m_loggedInPlayer)
	{
		mg_printf(conn, "map:%d,x:%f,y:%f,z:%f, ", pinfo->m_loggedInPlayer->GetMapId(), pinfo->m_loggedInPlayer->GetPositionX(), pinfo->m_loggedInPlayer->GetPositionY(), pinfo->m_loggedInPlayer->GetPositionZ());
	}
	mg_printf(conn,"操作成功:1");
	return;
}

static void on_show_msg(struct mg_connection *conn, const struct mg_request_info *ri, void *data)
{
	

	char* start = mg_get_var(conn, "starttime");
	char* end = mg_get_var(conn, "endtime");
	char* second = mg_get_var(conn, "second");
	char* pmsg = mg_get_var(conn, "msg");
	if (pmsg && start && end && second)
	{
		string msg = pmsg;
		string sec = second ;
		string starttime = start;
		string endtime = end;
		gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::show_msg, &gWebInterface,conn, msg, sec,starttime,endtime);
	}
	if (pmsg)
	{
		mg_free(pmsg);
	}
	if (start)
	{
		mg_free(start);
	}
	if (end)
	{
		mg_free(end);
	}
	if (second)
	{
		mg_free(second);
	}
	
	
}

ui32 CWebInterface::GetUnix_time(const char * str)
{
	uint32 time_to_ban = 0;
	char * p = (char*)str;
	uint32 multiplier;
	string number_temp;
	int y , month, day, h;
	 y = 0;
	 month = 0;
	 day = 0;
	 h = 0;
	 
	uint32 multipliee;
	number_temp.reserve(10);

	while(*p != 0)
	{
		// always starts with a number.
		if(!isdigit(*p))
			break;

		number_temp.clear();
		while(isdigit(*p) && *p != 0)
		{
			number_temp += *p;
			++p;
		}

		// try and find a letter
		if(*p == 0)
			break;

		// check the type
		switch(tolower(*p))
		{
		case 'y':
			 y = atoi(number_temp.c_str());
			break;

		case 'm':
			month = atoi(number_temp.c_str());
			break;

		case 'd':
			day = atoi(number_temp.c_str());
			break;

		case 'h':
			h = atoi(number_temp.c_str());
			break;

		default:
			return -1;
			break;
		}

		++p;
	}

	return make_unix_time(y,month,day,h,0,0);
}
void CWebInterface::show_msg(struct mg_connection *conn, string strMsg, string second,  string start_time, string end_time)
{
	
	uint32 start = GetUnix_time(start_time.c_str());
	uint32 end = GetUnix_time(end_time.c_str());
	ui32 sec = atoi(second.c_str());
	bool success = 	g_broadTextMgr_system->AddBroadText(strMsg, CHAT_MSG_SYSTEM, 0 , start, end, sec * TIME_MINUTE);

	if (success)
	{
		mg_printf(conn,"操作成功:1");
	}else
	{
		mg_printf(conn,"操作失败:0");
	}
}

static void
on_login_page(struct mg_connection *conn,
		   const struct mg_request_info *ri, void *data)
{
	

	char		*name, *pass, uri[100];
	const char	*cookie;

	name = mg_get_var(conn, "name");
	pass = mg_get_var(conn, "pass");
	cookie = mg_get_header(conn, "Cookie");

	/*
	* Here user name and password must be checked against some
	* database - this is step 2 from the algorithm described above.
	* This is an example, so hardcode name and password to be
	* admin/admin, and if this is so, set "allow=yes" cookie and
	* redirect back to the page where we have been redirected to login.
	*/
	if (name != NULL && pass != NULL &&
		strcmp(name, "admin") == 0 && strcmp(pass, "admin") == 0) {
			if (cookie == NULL || sscanf(cookie, "uri=%99s", uri) != 1)
				(void) strcpy(uri, "/");
			on_show_index(conn, ri, (void*)"");
	} else {
		/* Print login page */
		mg_printf(conn, "HTTP/1.1 200 OK\r\n"
			"content-Type: text/html\r\n\r\n"
			"输入验证信息:<br>"
			"<form method=post>"
			"Name:     <input type=text name=name></input><br/>"
			"Password: <input type=password name=pass></input><br/>"
			"<input type=submit value=Login></input>"
			"</form>");
	}

	if (name != NULL)
		mg_free(name);
	if (pass != NULL)
		mg_free(pass);
}

static void
on_authorize(struct mg_connection *conn,
		  const struct mg_request_info *ri, void *data)
{
	

	const char	*cookie, *domain;

	cookie = mg_get_header(conn, "Cookie");

	if (!strcmp(ri->uri, "/login")) {
		/* Always authorize accesses to the login page */
		mg_authorize(conn);
	} else if (cookie != NULL && strstr(cookie, "allow=yes") != NULL) {
		/* Valid cookie is present, authorize */
		mg_authorize(conn);
	} else {
		/* Not authorized. Redirect to the login page */
		mg_printf(conn, "HTTP/1.1 301 Moved Permanently\r\n"
			"Set-Cookie: uri=%s;\r\n"
			"Location: /login\r\n\r\n", ri->uri);
	}
}

void on_shutdown_server(struct mg_connection *conn, const struct mg_request_info *ri, void *data)
{
	

	uint32 shutdowntime = 15;
	char* arg = mg_get_var(conn, "arg");
	if(arg)
		shutdowntime = atol(arg);

	gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::shutdown_server, &gWebInterface, conn, shutdowntime);
}
void CWebInterface::shutdown_server(mg_connection* conn, uint32 shutdowntime)
{
	

	/*
	char msg[500];
	snprintf(msg, 500, "服务器即将在[%d]秒后关闭.", (unsigned int)shutdowntime);

	sWorld.SendWorldText(msg);
	MyLog::gmlog->notice("服务器即将在[%d]秒后关闭", shutdowntime);
	shutdowntime *= 1000;
	sMaster.m_ShutdownTimer = shutdowntime;
	sMaster.m_ShutdownEvent = true;
	sMaster.m_restartEvent = false;
	*/
	sMaster.SetShutdownTimer( shutdowntime );

	mg_printf(conn,"操作成功:1");
}

void on_player_list(struct mg_connection *conn, const struct mg_request_info *ri, void *data)
{
	

	gWebInterface.GetCallBackMgr()->add_cb(&CWebInterface::player_list, &gWebInterface, conn);
}
void CWebInterface::player_list(struct mg_connection *conn)
{
	

	mg_printf(conn, "<html><body><h4>玩家在线情况:</h4><table border="
		"1""><hr>");
	mg_printf(conn, "<tr><th>地图名称</th><th>玩家数量</th><th>怪物数量</th></tr>");
	for( uint32 i = 0; i < NUM_MAPS; ++i )
	{
		MapMgr * mapmgr = NULL;
		if( sInstanceMgr.m_singleMaps[i] )
		{
			mapmgr = sInstanceMgr.m_singleMaps[i];
			mg_printf(conn, "<tr><td>%s</td><td>%u</td><td>%u</td></tr>",
				mapmgr->GetMapInfo()->name, mapmgr->GetPlayerCount(), 0);
		}
		if( sInstanceMgr.m_instances[i] )
		{
			for( InstanceMap::iterator it = sInstanceMgr.m_instances[i]->begin(); it != sInstanceMgr.m_instances[i]->end(); ++it )
			{
				Instance* pInstance = it->second;
				if( pInstance && pInstance->m_mapMgr )
				{
					mapmgr = pInstance->m_mapMgr;
					pInstance->m_mapMgr->Update( getMSTime() );
					mg_printf(conn, "<tr><td>%s</td><td>%u</td><td>%u</td></tr>",
						mapmgr->GetMapInfo()->name, mapmgr->GetPlayerCount(), 0);
				}
			}
		}
	}
	mg_printf(conn, "</table></body></html>");
}
