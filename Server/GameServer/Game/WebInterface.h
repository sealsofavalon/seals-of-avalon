#ifndef _WEBINTERFACE_HEAD
#define _WEBINTERFACE_HEAD

#include "mongoose/mongoose.h"
#define ALIAS_URI "/my_etc"
#define ALIAS_DIR "/etc/"
#define DECLARE_ONREQ(x) static void onx(struct mg_connection *conn,const struct mg_request_info *ri,void *data);

static void on_show_index(struct mg_connection *conn,const struct mg_request_info *request_info,void *user_data);
static void on_login_page(struct mg_connection *conn,const struct mg_request_info *ri, void *data);
static void on_authorize(struct mg_connection *conn, const struct mg_request_info *ri, void *data);
static void on_shutdown_server(struct mg_connection *conn, const struct mg_request_info *ri, void *data);
static void on_player_list(struct mg_connection *conn, const struct mg_request_info *ri, void *data);
static void on_show_msg(struct mg_connection *conn, const struct mg_request_info *ri, void *data);


static void on_account_main(struct mg_connection *conn,const struct mg_request_info *request_info,void *user_data);
static void on_role_main(struct mg_connection* conn, const struct mg_request_info* ri, void *data);
static void on_guild_main(struct mg_connection* conn, const struct mg_request_info* ri, void *data);
static void on_bulletin_main(struct mg_connection* conn, const struct mg_request_info* ri, void *data);
static void on_exp_main(struct mg_connection* conn, const struct mg_request_info* ri, void *data);

//角色列表查询
static void on_role_list(struct mg_connection* conn, const struct mg_request_info* ri, void *data);
//角色信息查询
static void on_player_info(struct mg_connection* conn, const struct mg_request_info* ri, void *data);
//踢人
static void on_kick_player(struct mg_connection* conn, const struct mg_request_info* ri, void *data);
//禁言
static void on_mute_player(struct mg_connection* conn, const struct mg_request_info* ri, void *data);
//取消禁言
static void on_unmute_player(struct mg_connection* conn, const struct mg_request_info* ri, void *data);
//召唤玩家
//static void 

class CWebInterface
{
	struct mg_context	*ctx;
	int			data;
	call_back_mgr m_cb_mgr;
public:
	void Init();
	void Update();
	void Release();
	call_back_mgr* GetCallBackMgr(){ return &m_cb_mgr; }

	ui32 GetUnix_time(const char * str);
	void show_index();
	void shutdown_server(mg_connection* conn,uint32 t);
	void player_list(struct mg_connection *conn);
	void show_msg(struct mg_connection *conn,string strMsg, string second, string start_time, string end_time);

	void on_account_status_get(mg_connection* conn, string acct);
	void on_account_block(mg_connection* conn, string start, string end);
	void on_account_deblock(mg_connection* conn);

	void on_role_get(mg_connection* conn, string player_name);
	void on_role_kick(mg_connection* conn, string player_name, string reason);
	void on_role_silent(mg_connection* conn, string player_name, string reason, string start_time, string end_time);
	void on_role_desilent(mg_connection* conn, string player_name);
	void on_role_shift(mg_connection* conn, string player_name, string mapid, string x, string y, string z);
	void on_role_updatename(mg_connection* conn, string player_name, string player_new_name); 
	void on_role_recoverrole(mg_connection* conn, string player_name);
	void on_role_closerole(mg_connection* conn, string player_name, string reason, string ban_time);

	void on_guild_info(mg_connection* conn, string guild_name);
	void on_guild_set_disband(mg_connection* conn, string guild_name);
	void on_guild_set_name(mg_connection* conn, string guild_name, string new_name);
	void on_guild_set_description(mg_connection* conn, string guild_name, string desc);

	void on_bulletin_add(mg_connection* conn, string id, string type, string notify);

	void on_exp_get(mg_connection* conn);
	void on_exp_set(mg_connection* conn, string start, string end, string ratio);


	void role_list();
	void player_info();
private:
};
extern CWebInterface gWebInterface;

#endif

