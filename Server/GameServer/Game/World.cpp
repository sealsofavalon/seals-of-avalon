#include "StdAfx.h"
#include "../../../SDBase/Protocol/S2C_Chat.h"
#include "ShopMgr.h"
#include "MultiLanguageStringMgr.h"
#include "../Master.h"
#include "WeatherMgr.h"
#include "TaxiMgr.h"
#include "LfgMgr.h"
#include "QuestMgr.h"
#include "../Common/Protocol/GS2DB.h"
#include "../Net/DSSocket.h"
#include "ObjectMgr.h"

//DayWatcherThread* dw = NULL;

float World::m_movementCompressThreshold;
float World::m_movementCompressThresholdCreatures;
uint32 World::m_movementCompressRate;
uint32 World::m_movementCompressInterval;
float World::m_speedHackThreshold;

initialiseSingleton( World );

FILE* fp_undone_sql = NULL;

World::World()
{
	wait_save_sql = false;
	m_ExtraAllExpDuration = 0;
	m_ExtraAllExpRatio = 100;
	m_ExtraExpDuration = 0;
	m_ExtraExpRatio = 100;
	m_playerLimit = 0;
	m_allowMovement = true;
	m_gmTicketSystem = true;

	GmClientChannel = "";

	m_StartTime = 0;
	eventholder = new EventableObjectHolder(-1);
	/*broadtextmgr = new BroadTextMgr;
	if (broadtextmgr)
	{
		broadtextmgr->InitBroadTextMgr();
	}*/
	mazemapcreate = new MazeCreate;
	if (mazemapcreate)
	{
		mazemapcreate->InitMazeMap();
	}

	m_holder = eventholder;
	m_event_Instanceid = eventholder->GetInstanceID();

	mQueueUpdateInterval = 10000;
	PeakSessionCount = 0;
	mInWorldPlayerCount = 0;
	mAcceptedConnections = 0;
	HordePlayers = 0;
	AlliancePlayers = 0;
	gm_skip_attunement = false;
	show_gm_in_who_list = true;
	//allow_gm_friends = true;
	map_unload_time=0;

	m_startLevel=1;
	m_levelCap=200;
	m_genLevelCap=200;
	m_limitedNames=false;
	m_banTable = NULL;
	m_speedHackThreshold = -600.0f;
	m_forceGMTag = false;
	m_showKick = true;
	m_average_leave_time = 1;
	m_last_leave_time = (uint32)UNIXTIME;

	fp_undone_sql = fopen( "undone.sql", "w" );
}

void CleanupRandomNumberGenerators();
void World::LogoutPlayers()
{
	MyLog::log->notice("World : Logging out players...");
	for(SessionMap::iterator i=m_sessions.begin();i!=m_sessions.end();i++)
	{
		(i->second)->LogoutPlayer(true);
	}

	MyLog::log->notice("World", "Deleting sessions...");
	WorldSession * p;

	SessionMap temp( m_sessions );
	for( SessionMap::iterator i = temp.begin(); i != temp.end(); ++i )
	{
		p = i->second;
		DeleteSession(p);
	}
}

bool World::ExecuteSqlToDBServer( QueryBuffer* buf )
{
	if( g_crosssrvmgr->m_isInstanceSrv || wait_save_sql )
	{
		scoped_sql_transaction_proc sstp( &CharacterDatabase );
		for( size_t i = 0; i < buf->queries.size(); ++i )
		{
			char* p = buf->queries[i];
			CharacterDatabase.TransactionExecute( p );
			free( p );
		}
		sstp.success();
		delete buf;
		return true;
	}

	if( sDSSocket.is_connected() )
	{
		MSG_GS2DB::stSave MsgSave;
		for( size_t i = 0; i < buf->queries.size(); ++i )
		{
			char* p = buf->queries[i];
			MsgSave.sqls.push_back( p );
			free( p );
		}
		delete buf;
		sDSSocket.PostSend( MsgSave );
	}
	else
	{
		for( size_t i = 0; i < buf->queries.size(); ++i )
		{
			char* p = buf->queries[i];
			fprintf( fp_undone_sql, "%s\n", p );
			free( p );
		}
		delete buf;
		fflush( fp_undone_sql );
	}

	return true;
}

bool World::ExecuteSqlToDBServer( const char* sql, ... )
{
	static char QueryString[65536];
	va_list vlist;
	va_start(vlist, sql);
	vsnprintf(QueryString, 65536, sql, vlist);
	va_end(vlist);

	if( g_crosssrvmgr->m_isInstanceSrv || wait_save_sql )
	{
		return CharacterDatabase.WaitExecute( QueryString );
	}

	if( sDSSocket.is_connected() )
	{
		MSG_GS2DB::stSave MsgSave;
		MsgSave.sqls.push_back( QueryString );
		sDSSocket.PostSend( MsgSave );
	}
	else
	{
		fprintf( fp_undone_sql, "%s;\n", QueryString );
		fflush( fp_undone_sql );
	}
	return true;
}

World::~World()
{

	MyLog::log->notice("LootMgr : ~LootMgr()");
	delete LootMgr::getSingletonPtr();

	MyLog::log->notice("LfgMgr : ~LfgMgr()");
	delete LfgMgr::getSingletonPtr();

	MyLog::log->notice("ChannelMgr : ~ChannelMgr()");
	delete ChannelMgr::getSingletonPtr();

	MyLog::log->notice("QuestMgr : ~ObjectMgr()");
	delete QuestMgr::getSingletonPtr();

	MyLog::log->notice("WeatherMgr : ~WeatherMgr()");
	delete WeatherMgr::getSingletonPtr();

	MyLog::log->notice("TaxiMgr : ~TaxiMgr()");
	delete TaxiMgr::getSingletonPtr();

	MyLog::log->notice("InstanceMgr : ~InstanceMgr()");
	sInstanceMgr.Shutdown();

	MyLog::log->notice("ObjectMgr : ~ObjectMgr()");
	delete ObjectMgr::getSingletonPtr();
	//MyLog::log->info("Deleting Thread Manager..");
	//delete ThreadMgr::getSingletonPtr();
	MyLog::log->notice("WordFilter : ~WordFilter()");
	delete g_chatFilter;
	delete g_characterNameFilter;

	MyLog::log->notice("Rnd : ~Rnd()");
	CleanupRandomNumberGenerators();

	for( AreaTriggerMap::iterator i = m_AreaTrigger.begin( ); i != m_AreaTrigger.end( ); ++ i )
	{
		delete i->second;
	}

	//eventholder = 0;
	delete eventholder;
	/*if (broadtextmgr)
	{
		broadtextmgr->ShutBroadTextMgr();
		delete broadtextmgr;
	}*/

	if (mazemapcreate)
	{
		delete mazemapcreate;
		mazemapcreate = NULL;
	}
	Storage_Cleanup();
	for(list<SpellEntry*>::iterator itr = dummyspells.begin(); itr != dummyspells.end(); ++itr)
		delete *itr;
}


WorldSession* World::FindSession(uint32 id)
{
	WorldSession * ret = 0;
	SessionMap::const_iterator itr = m_sessions.find(id);

	if(itr != m_sessions.end())
		ret = itr->second;

	return ret;
}

bool World::AddSession(WorldSession* s)
{
	if(!s)
		return false;

	if( sMaster.m_ShutdownEvent )
	{
		MSG_S2C::stMessageBox MsgBox;
		MsgBox.message = build_language_string( BuildString_WorldServerShutdownInSeconds, quick_itoa((uint32)UNIXTIME - sMaster.m_ShutdownTimer / 1000) );
		s->SendPacket(MsgBox);
		return false;
	}

	WorldSession *session = FindSession(s->GetAccountID());
	if( session )
	{
		session->Disconnect();
		MyLog::log->info("WorldSession : player %u is already in game", (uint32)s->_roleID);
		//return false;
	}

	Player* oldplayer = objmgr.GetPlayer((uint32)s->_roleID);

	if( oldplayer )
	{
		oldplayer->GetSession()->Disconnect();
		//return false;
	}
	m_sessions[s->GetAccountID()] = s;
	m_transSessions[s->_SessionID] = s;

	if(s->GetPermissionCount())
		m_gmsessions[s->GetAccountID()] = s;

	if(m_sessions.size() >  PeakSessionCount)
		PeakSessionCount = (uint32)m_sessions.size();
	return true;
}

void World::AddGlobalSession(WorldSession *session)
{
	if(!session)
		return;

	Sessions.insert(session);
}

void World::RemoveGlobalSession(WorldSession *session)
{
	Sessions.erase(session);
}

bool BasicTaskExecutor::run()
{
	/* Set thread priority, this is a bitch for multiplatform :P */
#ifdef WIN32
	switch(priority)
	{
		case BTE_PRIORITY_LOW:
			::SetThreadPriority( ::GetCurrentThread(), THREAD_PRIORITY_LOWEST );
			break;

		case BTW_PRIORITY_HIGH:
			::SetThreadPriority( ::GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL );
			break;

		default:		// BTW_PRIORITY_MED
			::SetThreadPriority( ::GetCurrentThread(), THREAD_PRIORITY_NORMAL );
			break;
	}
#else
	struct sched_param param;
	switch(priority)
	{
	case BTE_PRIORITY_LOW:
		param.sched_priority = 0;
		break;

	case BTW_PRIORITY_HIGH:
		param.sched_priority = 10;
		break;

	default:		// BTW_PRIORITY_MED
		param.sched_priority = 5;
		break;
	}
	pthread_setschedparam(pthread_self(), SCHED_OTHER, &param);
#endif

	// Execute the task in our new context.
	cb->execute();
#ifdef WIN32
	::SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_NORMAL);
#else
	param.sched_priority = 5;
	pthread_setschedparam(pthread_self(), SCHED_OTHER, &param);
#endif

	return true;
}

void Apply112SpellFixes();
void ApplyExtraDataFixes();
void ApplyNormalFixes();

bool World::SetInitialWorldSettings()
{
	Player::InitVisibleUpdateBits();

	CharacterDatabase.WaitExecute("UPDATE characters SET online = 0 WHERE online = 1");
	//CharacterDatabase.WaitExecute("UPDATE characters SET level = 70 WHERE level > 70");
	CharacterDatabase.WaitExecute("UPDATE characters SET banned=0,banReason='' WHERE banned > 100 AND banned < %u", UNIXTIME);

	m_lastTick = UNIXTIME;

	// TODO: clean this
	time_t tiempo;
	char hour[3];
	char minute[3];
	char second[3];
	struct tm *tmPtr;
	tiempo = UNIXTIME;
	tmPtr = localtime(&tiempo);
	strftime( hour, 3, "%H", tmPtr );
	strftime( minute, 3, "%M", tmPtr );
	strftime( second, 3, "%S", tmPtr );
	m_gameTime = (3600*atoi(hour))+(atoi(minute)*60)+(atoi(second)); // server starts at noon

	// Start

	uint32 start_time = getMSTime();

/*
	/* Convert area table ids/flags /
	DBCFile area;

	if( !area.open( "DBC/AreaTable.dbc" ) )
	{
		Log.Error( "World", "Cannot find file ./DBC/AreaTable.dbc" );
		return false;
	}

	uint32 flag_, area_, zone_;
	for(uint32 i = 0; i < area.getRecordCount(); ++i)
	{
		area_ = area.getRecord(i).getUInt(0);
		flag_ = area.getRecord(i).getUInt(3);
		zone_ = area.getRecord(i).getUInt(2);

		mAreaIDToTable[flag_] = AreaStorage.LookupEntry(area_);
		if(mZoneIDToTable.find(zone_) != mZoneIDToTable.end())
		{
			if(mZoneIDToTable[zone_]->AreaFlags != 312 &&
				mAreaIDToTable[flag_]->AreaFlags == 312)
			{
				// over ride.
				mZoneIDToTable[zone_] = mAreaIDToTable[flag_];
			}
		}
		else
		{
			mZoneIDToTable[zone_] = mAreaIDToTable[flag_];
		}
	}
*/
	new ObjectMgr;
	new QuestMgr;
	new LootMgr;
	new LfgMgr;
	new WeatherMgr;
	new TaxiMgr;
	new ChatHandler;

	// grep: this only has to be done once between version updates
	// to re-fill the table.

	/*MyLog::log->info("Filling spell replacements table...");
	FillSpellReplacementsTable();
	MyLog::log->info("");*/
	Storage_FillTaskList();

	/* storage stuff has to be loaded first */

	Storage_LoadAdditionalTables();

	objmgr.LoadPlayerCreateInfo();
	objmgr.LoadPlayersInfo();
	objmgr.LoadCreatureWaypoints();
	objmgr.LoadTrainers();
	objmgr.LoadTotemSpells();
	objmgr.LoadSpellSkills();
	objmgr.LoadSpellOverride();
	objmgr.LoadVendors();
	objmgr.LoadSpellFixes();
	objmgr.LoadSpellProcs();
	objmgr.LoadSpellEffectsOverride();
	objmgr.LoadDefaultPetSpells();
	objmgr.LoadPetSpellCooldowns();
	objmgr.LoadGuildCharters();
	objmgr.LoadGMTickets();
	objmgr.SetHighestGuids();
	objmgr.LoadReputationModifiers();
	objmgr.LoadMonsterSay();
	sWeatherMgr.LoadFromDB();
	objmgr.LoadGroups();
	objmgr.LoadExtraCreatureProtoStuff();
	objmgr.LoadExtraItemStuff();
	sQuestMgr.LoadExtraQuestStuff();
	sQuestMgr.LoadEscortQuestInfo();
	objmgr.LoadArenaTeams();

	// wait for all loading to complete.

	CommandTableStorage::getSingleton().Load();
	MyLog::log->notice("WordFilter", "Loading...");

	g_characterNameFilter = new WordFilter();
	g_chatFilter = new WordFilter();
	g_characterNameFilter->Load("wordfilter_character_names");
	g_chatFilter->Load("wordfilter_chat");

	MyLog::log->notice("WordFilter : Done.");

	MyLog::log->notice("World : Database loaded in %ums.", getMSTime() - start_time);

	// calling this puts all maps into our task list.
	sInstanceMgr.Load();

	// wait for the events to complete.

	// wait for them to exit, now.
	LoadNameGenData();
	Item::LoadWashEnchantments();

	MyLog::log->notice("World : Object size: %u bytes", sizeof(Object));
	MyLog::log->notice("World : Unit size: %u bytes", sizeof(Unit) + sizeof(AIInterface));
	MyLog::log->notice("World : Creature size: %u bytes", sizeof(Creature) + sizeof(AIInterface));
	MyLog::log->notice("World : Player size: %u bytes", sizeof(Player) + sizeof(ItemInterface) + 50000 + 30000 + 1000 + sizeof(AIInterface));
	MyLog::log->notice("World : GameObject size: %u bytes", sizeof(GameObject));

	Apply112SpellFixes();
	ApplyExtraDataFixes();
	ApplyNormalFixes();

// ------------------------------------------------------------------------------------------------

	MyLog::log->notice("World : Starting Transport System...");
	objmgr.LoadTransporters();

	// start mail system
	MailSystem::getSingleton().StartMailSystem();

	MyLog::log->notice("World : Starting Auction System...");
	new AuctionMgr;
	sAuctionMgr.LoadAuctionHouses();

	m_queueUpdateTimer = mQueueUpdateInterval;

	{
		MyLog::log->notice("World : Loading loot in foreground...");
		lootmgr.LoadLoot();
	}

	Channel::LoadConfSettings();

	sShopMgr.LoadShopItems();
	//dw = new DayWatcherThread();
	//ThreadPool.ExecuteTask( dw );

	//ThreadPool.ExecuteTask( new CharacterLoaderThread() );

#ifdef ENABLE_COMPRESSED_MOVEMENT
	MovementCompressor = new CMovementCompressorThread();
	ThreadPool.ExecuteTask( MovementCompressor );
#endif

	// Preload and compile talent and talent tab data to speed up talent inspect

	uint32 talent_max_rank;
	uint32 talent_pos;
	uint32 talent_class;

	StorageContainerIterator<TalentEntry> * itr = dbcTalent.MakeIterator();
	while(!itr->AtEnd())
	{
		TalentEntry *talent_info = itr->Get();
		if( talent_info == NULL )
			continue;

		TalentTabEntry const* tab_info = dbcTalentTab.LookupEntry( talent_info->TalentTree );
		if( tab_info == NULL )
			continue;

		talent_max_rank = 0;
		for( uint32 j = 5; j > 0; --j )
		{
			if( talent_info->RankID[j - 1] )
			{
				talent_max_rank = j;
				break;
			}
		}

		InspectTalentTabBit[( talent_info->Row << 24 ) + ( talent_info->Col << 16 ) + talent_info->TalentID] = talent_max_rank;
		InspectTalentTabSize[talent_info->TalentTree] += talent_max_rank;
		if(!itr->Inc())
			break;
	}
	itr->Destruct();

	StorageContainerIterator<TalentTabEntry> * itrtab = dbcTalentTab.MakeIterator();
	while(!itrtab->AtEnd())
	{
		TalentTabEntry const* tab_info = itrtab->Get();
		if( tab_info == NULL )
			continue;

		talent_pos = 0;

		for( talent_class = 0; talent_class < 12; ++talent_class )
		{
			if( tab_info->ClassMask & ( 1 << talent_class ) )
				break;
		}

		InspectTalentTabPages[talent_class + 1][tab_info->TabPage] = tab_info->TalentTabID;

		for( std::map< uint32, uint32 >::iterator itr = InspectTalentTabBit.begin(); itr != InspectTalentTabBit.end(); ++itr )
		{
			uint32 talent_id = itr->first & 0xFFFF;
			TalentEntry const* talent_info = dbcTalent.LookupEntry( talent_id );
			if( talent_info == NULL )
				continue;

			if( talent_info->TalentTree != tab_info->TalentTabID )
				continue;

			InspectTalentTabPos[talent_id] = talent_pos;
			talent_pos += itr->second;
		}
		if(!itrtab->Inc())
			break;
	}
	itrtab->Destruct();

	sEventMgr.AddEvent(this, &World::CheckForExpiredInstances, EVENT_WORLD_UPDATEAUCTIONS, 120000, 0, 0);
	return true;
}

void World::Update(time_t diff)
{
	if(m_ExtraAllExpDuration < UNIXTIME)
	{
		m_ExtraAllExpRatio = 100;
	}
	if(m_ExtraExpDuration < UNIXTIME)
	{
		m_ExtraExpRatio = 100;
	}
	eventholder->Update((uint32)diff);

	if( !g_crosssrvmgr->m_isInstanceSrv )
		sAuctionMgr.Update();

	sInstanceMgr.Update( getMSTime() );
	_UpdateGameTime();
#ifdef SESSION_CAP
	if( GetSessionCount() >= SESSION_CAP )
		TerminateProcess(GetCurrentProcess(),0);
#endif

	if( !g_crosssrvmgr->m_isInstanceSrv ) 
	{
		static uint32 lastPoll = (uint32)UNIXTIME;
		if( (uint32)UNIXTIME - lastPoll > 60 )
		{
			PollMailboxInsertQueue();
			lastPoll = (uint32)UNIXTIME;
		}
	}
}
bool World::GetMaze(std::map<int, int>& sMap, int& end)
{
	if (mazemapcreate)
	{
		mazemapcreate->GetMaze(sMap, end);
	}
	return false ;
}

//bool World::AddBroadMessage(string text, ui8 ntype, ui8 flag, uint32 start, uint32 end, ui32 second)
//{
//	if (broadtextmgr)
//	{
//		return broadtextmgr->AddBroadText(text,ntype,flag,start,end,second);
//	}
//	return false ;
//}
//bool World::AddActivityMessage(activity_mgr::activity_base_t* p)
//{
//	if (broadtextmgr)
//	{
//		return broadtextmgr->AddActivityText(p);
//	}
//	return false ;
//}
//bool World::RemoveActivityMessage(ui32 evtid)
//{
//	if (broadtextmgr)
//	{
//		return broadtextmgr->RemoveActivityText(evtid);
//	}
//	return false ;
//}
void World::SendGlobalMessage(PakHead& packet, WorldSession *self)
{


	SessionMap::iterator itr;
	for (itr = m_sessions.begin(); itr != m_sessions.end(); itr++)
	{
		if (itr->second->GetPlayer() &&
			itr->second->GetPlayer()->IsInWorld()
			&& itr->second != self)  // dont send to self!
		{
			itr->second->SendPacket(packet);
		}
	}


}
void World::SendMsgToALLGM(PakHead& packet)
{
	SessionMap::iterator itr;
	for(itr = m_gmsessions.begin(); itr != m_gmsessions.end(); itr++)
	{
		if (itr->second->GetPlayer() &&
			itr->second->GetPlayer()->IsInWorld())  // dont send to self!
		{
			itr->second->SendPacket(packet);
		}
	}
}

void World::SendGamemasterMessage(PakHead& packet, WorldSession *self)
{

 	SessionMap::iterator itr;
 	for(itr = m_sessions.begin(); itr != m_sessions.end(); itr++)
 	{
	  if (itr->second->GetPlayer() &&
	  itr->second->GetPlayer()->IsInWorld()
	  && itr->second != self)  // dont send to self!
	  {
 		if(itr->second->CanUseCommand('3'))
 		itr->second->SendPacket(packet);
	  }
 	}

}

void World::SendRaceMessage(PakHead& packet, uint8 raceId)
{

	SessionMap::iterator itr;
	Player * plr;
	for(itr = m_sessions.begin(); itr != m_sessions.end(); itr++)
	{
		plr = itr->second->GetPlayer();
		if(!plr || !plr->IsInWorld())
			continue;

		if(plr->getRace() == raceId)
			itr->second->SendPacket(packet);
	}

}

void World::SendZoneMessage(PakHead& packet, uint32 zoneid, WorldSession *self)
{


	SessionMap::iterator itr;
	for (itr = m_sessions.begin(); itr != m_sessions.end(); itr++)
	{
		if (itr->second->GetPlayer() &&
			itr->second->GetPlayer()->IsInWorld()
			&& itr->second != self)  // dont send to self!
		{
			if (itr->second->GetPlayer()->GetZoneId() == zoneid)
				itr->second->SendPacket(packet);
		}
	}


}

void World::SendGMWorldText(const char* text, WorldSession *self)
{
    uint32 textLen = (uint32)strlen((char*)text) + 1;

	MSG_S2C::stChat_Message Msg;
	Msg.type	= CHAT_MSG_SYSTEM;
	Msg.txt		= text;
	SendGamemasterMessage(Msg, self);
}

void World::SendInstanceMessage(PakHead& packet, uint32 instanceid, WorldSession *self)
{


	SessionMap::iterator itr;
	for (itr = m_sessions.begin(); itr != m_sessions.end(); itr++)
	{
		if (itr->second->GetPlayer() &&
			itr->second->GetPlayer()->IsInWorld()
			&& itr->second != self)  // dont send to self!
		{
			if (itr->second->GetPlayer()->GetInstanceID() == (int32)instanceid)
				itr->second->SendPacket(packet);
		}
	}


}

void World::SendWorldText(const char* text, WorldSession *self)
{
    uint32 textLen = (uint32)strlen((char*)text) + 1;

	MSG_S2C::stChat_Message Msg;
	Msg.type	= CHAT_MSG_SYSTEM;
	Msg.txt		= text;
	SendGlobalMessage(Msg, self);
	if(announce_output)
	MyLog::log->info("> %s", text);
}

void World::SendWorldWideScreenText(const char *text, WorldSession *self)
{
	MSG_S2C::stArea_Trigger_Message Msg;
	Msg.message = text;
	SendGlobalMessage(Msg, self);
	if(announce_output)
	MyLog::log->info("> %s", text);
}

void World::ServerShutdownCountDown( uint32 t )
{
	int secs = (int)t - (int)UNIXTIME;
	if( secs > 0 && secs <= 30 )
	{
		/*
		char szMsg[128] = { 0 };
		sprintf(szMsg, "服务器将在%u秒内关闭", secs);
		MyLog::log->notice(szMsg);
		sWorld.SendWorldText(szMsg);
		*/

		sWorld.SendWorldText( build_language_string( BuildString_WorldServerShutdownInSeconds, quick_itoa(secs) ) );
		MyLog::log->notice( build_language_string( BuildString_WorldServerShutdownInSeconds, quick_itoa(secs) ) );
	}
	else if( secs > 0 )
	{
		if( secs % 60 == 0 )
		{
			/*
			char szMsg[128] = { 0 };
			sprintf(szMsg, "服务器将在%u分钟内关闭", secs / 60);
			MyLog::log->notice(szMsg);
			sWorld.SendWorldText(szMsg);
			*/

			sWorld.SendWorldText( build_language_string( BuildString_WorldServerShutdownInSeconds, quick_itoa(secs / 60) ) );
			MyLog::log->notice( build_language_string( BuildString_WorldServerShutdownInSeconds, quick_itoa(secs / 60) ) );
		}
	}
	else
	{
		sMaster.m_stopEvent = true;
	}
}

void World::UpdateSessions(uint32 diff)
{
	SessionSet::iterator itr;
	WorldSession *session;
	for(itr = Sessions.begin(); itr != Sessions.end(); ++itr)
	{
		session = (*itr);
		if( session->_player )
		{
			if( session->_player->IsValid() )
			{
				session->_player->ProcessPendingUpdates();
				session->ProcessPendingMovements();
				session->ProcessPendingMonstMovements();
			}
			else
			{
				DeleteSession( session );
				break;
			}
		}
	}

	if( m_queuedUsers.size() > 0 && objmgr.GetPlayerCount() < (uint32)sMaster.m_user_limit )
	{
		WorldSession* p = m_queuedUsers.front();
		MSG_S2C::stEnterGameWaitOK msg;
		p->SendPacket( msg );
		p->RealEnterGame();
		m_queuedUsers.pop_front();
	}
	static uint32 last_update = (uint32)UNIXTIME;
	if( (uint32)UNIXTIME - last_update > 10 )
	{
		last_update = (uint32)UNIXTIME;
		MSG_S2C::stEnterGameLine  msg;
		msg.enter_game_number = 0;
		for( std::list<WorldSession*>::iterator it = m_queuedUsers.begin(); it != m_queuedUsers.end(); ++it )
		{
			msg.enter_game_number ++;
			WorldSession* p = *it;
			msg.estimateTime = msg.enter_game_number * m_average_leave_time;
			p->SendPacket( msg );
		}
	}
}

std::string World::GenerateName(uint32 type)
{
	if(_namegendata[type].size() == 0)
		return "ERR";

	uint32 ent = RandomUInt((uint32)_namegendata[type].size()-1);
	return _namegendata[type].at(ent).name;
}

void World::DeleteSession(WorldSession *session, bool bRemoveSet )
{
	SessionMap::iterator it = m_transSessions.find( session->_SessionID );
	if( it != m_transSessions.end() )
	{
		m_transSessions.erase( it );
	}

	it = m_gmsessions.find( session->GetAccountID() );
	if( it != m_gmsessions.end() )
	{
		m_gmsessions.erase( it );
	}

	// remove from big map
	it = m_sessions.find( session->GetAccountId() );
	if( it != m_sessions.end() )
	{
		m_sessions.erase( it );
		m_gmsessions.erase( session->GetAccountID() );

		// delete us
		session->Delete();
		Sessions.erase( session );
	}
	LeaveQueue( session );

	m_average_leave_time = ( m_leave_count * m_average_leave_time + (uint32)UNIXTIME - m_last_leave_time ) / (m_leave_count + 1);
	m_leave_count++;
	m_last_leave_time = (uint32)UNIXTIME;
}

uint32 World::GetNonGmSessionCount()
{
	uint32 total = (uint32)m_sessions.size();

	SessionMap::const_iterator itr = m_sessions.begin();
	for( ; itr != m_sessions.end(); itr++ )
	{
		if( (itr->second)->HasGMPermissions() )
			total--;
	}
	return total;
}


void World::SaveAllPlayers()
{
	if(!(ObjectMgr::getSingletonPtr()))
		return;

	MyLog::log->info("Saving all players to database...");
	uint32 count = 0;
	PlayerStorageMap::const_iterator itr;
		// Servers started and obviously runing. lets save all players.
	uint32 mt;
	for (itr = objmgr._players.begin(); itr != objmgr._players.end(); itr++)
		{
			Player* p = itr->second;
			if(p->GetSession())
			{
				p->EjectFromInstance();
				mt = getMSTime();
				p->RemoveAllExtraAuars();
				p->RemoveAllExtraSpells();
				p->RemoveItemsFromWorld();
				p->SaveToDB(false);
				MyLog::log->info("Saved player `%s` (level %u) in %ums.", p->GetName(), p->GetUInt32Value(UNIT_FIELD_LEVEL), getMSTime() - mt);
				++count;
			}
		}
	MyLog::log->info("Saved %u players.", count);
}

WorldSession* World::FindSessionByTransID(ui32 transid)
{
	// loop sessions, see if we can find him
	SessionMap::iterator itr = m_transSessions.find( transid );
	if( itr != m_transSessions.end() )
		return itr->second;
	else
		return NULL;
}

WorldSession* World::FindSessionByName(const char * Name)//case insensetive
{
	// loop sessions, see if we can find him
	SessionMap::iterator itr = m_sessions.begin();
	for(; itr != m_sessions.end(); ++itr)
	{
	  if(!stricmp(itr->second->GetAccountName().c_str(),Name))
	  {

			return itr->second;
	  }
	}

	return 0;
}

void World::ShutdownClasses()
{
	MyLog::log->notice("AuctionMgr : ~AuctionMgr()");
	delete AuctionMgr::getSingletonPtr();
	MyLog::log->notice("LootMgr : ~LootMgr()");
	delete LootMgr::getSingletonPtr();

	MyLog::log->notice("MailSystem : ~MailSystem()");
	delete MailSystem::getSingletonPtr();
}

void World::GetStats(uint32 * GMCount, float * AverageLatency)
{
	int gm = 0;
	int count = 0;
	int avg = 0;
	PlayerStorageMap::const_iterator itr;
	for (itr = objmgr._players.begin(); itr != objmgr._players.end(); itr++)
	{
		if(itr->second->GetSession())
		{
			count++;
			avg += itr->second->GetSession()->GetLatency();
			if(itr->second->GetSession()->GetPermissionCount())
				gm++;
		}
	}

	*AverageLatency = count ? (float)((float)avg / (float)count) : 0;
	*GMCount = gm;
}

void World::DeleteObject(Object * obj)
{
	delete obj;
}

void World::Rehash(bool load)
{
	if(load)
	{
//		#ifdef WIN32
//		Config.MainConfig.SetSource("ascent-world.conf", true);
//		#else
//		Config.MainConfig.SetSource((char*)CONFDIR "/ascent-world.conf", true);
//		#endif
	}

	if(!ChannelMgr::getSingletonPtr())
		new ChannelMgr;

	if(!MailSystem::getSingletonPtr())
		new MailSystem;

	channelmgr.seperatechannels = Config.MainConfig.GetBoolDefault("Server", "SeperateChatChannels", false);
	MapPath = Config.MainConfig.GetStringDefault("Terrain", "MapPath", "maps");
	vMapPath = Config.MainConfig.GetStringDefault("Terrain", "vMapPath", "vmaps");
	UnloadMapFiles = Config.MainConfig.GetBoolDefault("Terrain", "UnloadMapFiles", true);
	BreathingEnabled = Config.MainConfig.GetBoolDefault("Server", "EnableBreathing", true);
	SendStatsOnJoin = Config.MainConfig.GetBoolDefault("Server", "SendStatsOnJoin", false);
	compression_threshold = Config.MainConfig.GetIntDefault("Server", "CompressionThreshold", 1000);

	// load regeneration rates.
	setRate(RATE_HEALTH,Config.MainConfig.GetFloatDefault("Rates", "Health",1));
	setRate(RATE_POWER1,Config.MainConfig.GetFloatDefault("Rates", "Power1",1));
	setRate(RATE_POWER2,Config.MainConfig.GetFloatDefault("Rates", "Power2",1));
	setRate(RATE_POWER3,Config.MainConfig.GetFloatDefault("Rates", "Power4",1));
	setRate(RATE_DROP0,Config.MainConfig.GetFloatDefault("Rates", "DropGrey",1));
  setRate(RATE_DROP1,Config.MainConfig.GetFloatDefault("Rates", "DropWhite",1));
  setRate(RATE_DROP2,Config.MainConfig.GetFloatDefault("Rates", "DropGreen",1));
  setRate(RATE_DROP3,Config.MainConfig.GetFloatDefault("Rates", "DropBlue",1));
  setRate(RATE_DROP4,Config.MainConfig.GetFloatDefault("Rates", "DropPurple",1));
  setRate(RATE_DROP5,Config.MainConfig.GetFloatDefault("Rates", "DropOrange",1));
  setRate(RATE_DROP6,Config.MainConfig.GetFloatDefault("Rates", "DropArtifact",1));
	setRate(RATE_XP,Config.MainConfig.GetFloatDefault("Rates", "XP",1));
	setRate(RATE_RESTXP,Config.MainConfig.GetFloatDefault("Rates", "RestXP", 1));
	setRate(RATE_QUESTXP,Config.MainConfig.GetFloatDefault("Rates", "QuestXP", 1));
	setIntRate(INTRATE_SAVE, Config.MainConfig.GetIntDefault("Rates", "Save", 60000));
	setRate(RATE_MONEY, Config.MainConfig.GetFloatDefault("Rates", "DropMoney", 1.0f));
	setRate(RATE_QUESTREPUTATION, Config.MainConfig.GetFloatDefault("Rates", "QuestReputation", 1.0f));
	setRate(RATE_KILLREPUTATION, Config.MainConfig.GetFloatDefault("Rates", "KillReputation", 1.0f));
	setRate(RATE_HONOR, Config.MainConfig.GetFloatDefault("Rates", "Honor", 1.0f));
	setRate(RATE_SKILLCHANCE, Config.MainConfig.GetFloatDefault("Rates", "SkillChance", 1.0f));
	setRate(RATE_SKILLRATE, Config.MainConfig.GetFloatDefault("Rates", "SkillRate", 1.0f));
	setIntRate(INTRATE_COMPRESSION, Config.MainConfig.GetIntDefault("Rates", "Compression", 1));
	setIntRate(INTRATE_PVPTIMER, Config.MainConfig.GetIntDefault("Rates", "PvPTimer", 300000));
	setRate(RATE_ARENAPOINTMULTIPLIER2X, Config.MainConfig.GetFloatDefault("Rates", "ArenaMultiplier2x", 1.0f));
	setRate(RATE_ARENAPOINTMULTIPLIER3X, Config.MainConfig.GetFloatDefault("Rates", "ArenaMultiplier3x", 1.0f));
	setRate(RATE_ARENAPOINTMULTIPLIER5X, Config.MainConfig.GetFloatDefault("Rates", "ArenaMultiplier5x", 1.0f));
	SetPlayerLimit(Config.MainConfig.GetIntDefault("Server", "PlayerLimit", 1000));
	SetMotd(Config.MainConfig.GetStringDefault("Server", "Motd", "Default MOTD").c_str());
	mQueueUpdateInterval = Config.MainConfig.GetIntDefault("Server", "QueueUpdateInterval", 5000);
	SetKickAFKPlayerTime(Config.MainConfig.GetIntDefault("Server", "KickAFKPlayers", 0));
	gm_skip_attunement = Config.MainConfig.GetBoolDefault("Server", "SkipAttunementsForGM", true);

#ifdef WIN32
	DWORD current_priority_class = GetPriorityClass( GetCurrentProcess() );
	bool high = Config.MainConfig.GetBoolDefault( "Server", "AdjustPriority", false );

	if( high )
	{
		if( current_priority_class != HIGH_PRIORITY_CLASS )
			SetPriorityClass( GetCurrentProcess(), HIGH_PRIORITY_CLASS );
	}
	else
	{
		if( current_priority_class != NORMAL_PRIORITY_CLASS )
			SetPriorityClass( GetCurrentProcess(), NORMAL_PRIORITY_CLASS );
	}
#endif

	if(!Config.MainConfig.GetString("GMClient", "GmClientChannel", &GmClientChannel))
	{
		GmClientChannel = "";
	}

	m_reqGmForCommands = !Config.MainConfig.GetBoolDefault("Server", "AllowPlayerCommands", false);
	m_lfgForNonLfg = Config.MainConfig.GetBoolDefault("Server", "EnableLFGJoin", false);

	realmtype = Config.MainConfig.GetBoolDefault("Server", "RealmType", false);
	TimeOut= uint32(1000* Config.MainConfig.GetIntDefault("Server", "ConnectionTimeout", 180) );

	uint32 config_flags = 0;
	if(Config.MainConfig.GetBoolDefault("Mail", "DisablePostageCostsForGM", true))
		config_flags |= MAIL_FLAG_NO_COST_FOR_GM;

	if(Config.MainConfig.GetBoolDefault("Mail", "DisablePostageCosts", false))
		config_flags |= MAIL_FLAG_DISABLE_POSTAGE_COSTS;

	if(Config.MainConfig.GetBoolDefault("Mail", "DisablePostageDelayItems", true))
		config_flags |= MAIL_FLAG_DISABLE_HOUR_DELAY_FOR_ITEMS;

	if(Config.MainConfig.GetBoolDefault("Mail", "DisableMessageExpiry", false))
		config_flags |= MAIL_FLAG_NO_EXPIRY;

	if(Config.MainConfig.GetBoolDefault("Mail", "EnableInterfactionMail", true))
		config_flags |= MAIL_FLAG_CAN_SEND_TO_OPPOSITE_FACTION;

	if(Config.MainConfig.GetBoolDefault("Mail", "EnableInterfactionForGM", true))
		config_flags |= MAIL_FLAG_CAN_SEND_TO_OPPOSITE_FACTION_GM;

	sMailSystem.config_flags = config_flags;
	flood_lines = Config.MainConfig.GetIntDefault("FloodProtection", "Lines", 0);
	flood_seconds = Config.MainConfig.GetIntDefault("FloodProtection", "Seconds", 0);
	flood_message = Config.MainConfig.GetBoolDefault("FloodProtection", "SendMessage", false);
	show_gm_in_who_list = Config.MainConfig.GetBoolDefault("Server", "ShowGMInWhoList", true);
	//allow_gm_friends = Config.MainConfig.GetBoolDefault("Server", "AllowGMFriends", false);
	if(!flood_lines || !flood_seconds)
		flood_lines = flood_seconds = 0;

	announce_tag = Config.MainConfig.GetStringDefault("Announce", "Tag", "Staff");
	GMAdminTag = Config.MainConfig.GetBoolDefault("Announce", "GMAdminTag", false);
	NameinAnnounce = Config.MainConfig.GetBoolDefault("Announce", "NameinAnnounce", true);
	NameinWAnnounce = Config.MainConfig.GetBoolDefault("Announce", "NameinWAnnounce", true);
	announce_output = Config.MainConfig.GetBoolDefault("Announce", "ShowInConsole", true);
	ann_namecolor = Config.MainConfig.GetStringDefault("Color", "AnnNameColor", "<color:1f40af20>");
	ann_gmtagcolor = Config.MainConfig.GetStringDefault("Color", "AnnGMTagColor", "<color:ffff6060>");
	ann_tagcolor = Config.MainConfig.GetStringDefault("Color", "AnnTagColor", "<color:ff00ccff>");
	ann_msgcolor = Config.MainConfig.GetStringDefault("Color", "AnnMsgColor", "<color:ffffcc00>");

	map_unload_time=Config.MainConfig.GetIntDefault("Server", "MapUnloadTime", 0);

	antihack_teleport = Config.MainConfig.GetBoolDefault("AntiHack", "Teleport", true);
	antihack_speed = Config.MainConfig.GetBoolDefault("AntiHack", "Speed", true);
	antihack_flight = Config.MainConfig.GetBoolDefault("AntiHack", "Flight", true);
	flyhack_threshold = Config.MainConfig.GetIntDefault("AntiHack", "FlightThreshold", 8);
	no_antihack_on_gm = Config.MainConfig.GetBoolDefault("AntiHack", "DisableOnGM", false);
	m_speedHackThreshold = Config.MainConfig.GetFloatDefault("AntiHack", "SpeedThreshold", -600.0f);
	SpeedhackProtection = antihack_speed;
	m_startLevel = Config.MainConfig.GetIntDefault("Server", "StartingLevel", 1);
	if(m_startLevel > 100) {m_startLevel = 100;} //force the startLevel to not be higher than 100
	m_levelCap = Config.MainConfig.GetIntDefault("Server", "LevelCap", 200);
	m_genLevelCap = Config.MainConfig.GetIntDefault("Server", "GenLevelCap", 200);
	m_limitedNames = Config.MainConfig.GetBoolDefault("Server", "LimitedNames", true);
	m_useAccountData = Config.MainConfig.GetBoolDefault("Server", "UseAccountData", false);
	m_forceGMTag = Config.MainConfig.GetBoolDefault("Server", "ForceGMTag", false);
	m_showKick = Config.MainConfig.GetBoolDefault("Server", "ShowKickMessage", true);

	// ======================================
	m_movementCompressInterval = Config.MainConfig.GetIntDefault("Movement", "FlushInterval", 1000);
	m_movementCompressRate = Config.MainConfig.GetIntDefault("Movement", "CompressRate", 1);

	m_movementCompressThresholdCreatures = Config.MainConfig.GetFloatDefault("Movement", "CompressThresholdCreatures", 15.0f);
	m_movementCompressThresholdCreatures *= m_movementCompressThresholdCreatures;

	m_movementCompressThreshold = Config.MainConfig.GetFloatDefault("Movement", "CompressThreshold", 25.0f);
	m_movementCompressThreshold *= m_movementCompressThreshold;		// square it to avoid sqrt() on checks
	// ======================================

	if( m_banTable != NULL )
		free( m_banTable );

	m_banTable = NULL;
	string s = Config.MainConfig.GetStringDefault( "Server", "BanTable", "" );
	if( !s.empty() )
		m_banTable = strdup( s.c_str() );

	if( load )
		Channel::LoadConfSettings();
}

void World::LoadNameGenData()
{
	_namegendata->clear();
	StorageContainerIterator<NameGenData> * itr = dbcNameGen.MakeIterator();
	NameGenData* it = NULL;
	while(!itr->AtEnd())
	{
		it = itr->Get();

		NameGenData d;
		if(!it->name)
			continue;
		d.name = it->name;
		d.type = it->type;
		_namegendata[d.type].push_back(d);

		if(!itr->Inc())
			break;
	}
	itr->Destruct();
}

void World::LoadAccountDataProc(QueryResultVector& results, uint32 AccountId)
{
	WorldSession * s = FindSession(AccountId);
	if(s == NULL)
		return;

	s->LoadAccountDataProc(results[0].result);
}

void World::CleanupCheaters()
{
	/*uint32 guid;
	string name;
	uint32 cl;
	uint32 level;
	uint32 talentpts;
	char * start, *end;
	Field * f;
	uint32 should_talents;
	uint32 used_talents;
	SpellEntry * sp;

	QueryResult * result = CharacterDatabase.Query("SELECT guid, name, class, level, available_talent_points, spells FROM characters");
	if(result == NULL)
		return;

	do
	{
		f = result->Fetch();
		guid = f[0].GetUInt32();
		name = string(f[1].GetString());
		cl = f[2].GetUInt32();
		level = f[3].GetUInt32();
		talentpts = f[4].GetUInt32();
		start = f[5].GetString();
		should_talents = (level<10 ? 0 : level - 9);
		used_talents -=


		start = (char*)get_next_field.GetString();//buff;
		while(true)
		{
			end = strchr(start,',');
			if(!end)break;
			*end=0;
			sp = dbcSpell.LookupEntry(atol(start));
			start = end +1;

			if(sp->talent_tree)

		}

	} while(result->NextRow());*/

}

void World::CheckForExpiredInstances()
{
	sInstanceMgr.CheckForExpiredInstances();
}

struct insert_playeritem
{
	uint32 ownerguid;
	uint32 entry;
	uint32 wrapped_item_id;
	uint32 wrapped_creator;
	uint32 creator;
	uint32 count;
	uint32 charges;
	uint32 flags;
	uint32 randomprop;
	uint32 randomsuffix;
	uint32 itemtext;
	uint32 durability;
	int32 containerslot;
	int32 slot;
	string enchantments;
};

#define LOAD_THREAD_SLEEP 180

void World::PollMailboxInsertQueue()
{
	QueryResult * result;
	Field * f;
	Item * pItem;
	uint32 itemid;
	uint32 stackcount;

	result = CharacterDatabase.Query("SELECT * FROM mailbox_insert_queue");
	if( result != NULL )
	{
		MyLog::log->notice("MailboxQueue : Sending queued messages....");
		do
		{
			f = result->Fetch();
			itemid = f[6].GetUInt32();
			stackcount = f[7].GetUInt32();

			if( itemid != 0 )
			{
				pItem = objmgr.CreateItem( itemid, NULL );
				if( pItem != NULL )
				{
					pItem->SetUInt32Value( ITEM_FIELD_STACK_COUNT, stackcount );
					pItem->SaveToDB( 0, 0, true, NULL );
				}
			}
			else
				pItem = NULL;

			MyLog::log->notice("MailboxQueue : Sending message to %u (item: %u)...", f[1].GetUInt32(), itemid);
			sMailSystem.SendAutomatedMessage( 0, f[0].GetUInt64(), f[1].GetUInt64(), f[2].GetString(), f[3].GetString(), f[5].GetUInt32(),
				0, pItem ? pItem->GetGUID() : 0, f[4].GetUInt32() );

			if( pItem != NULL )
				delete pItem;

		} while ( result->NextRow() );
		delete result;
		MyLog::log->notice("MailboxQueue : Done.");
		CharacterDatabase.WaitExecute("DELETE FROM mailbox_insert_queue");
	}
}

void World::DisconnectUsersWithAccount(const char * account, WorldSession * m_session)
{
	SessionMap::iterator itr;
	WorldSession * session;

	bool FoundUser = false;
	for(itr = m_sessions.begin(); itr != m_sessions.end();)
	{
		session = itr->second;
		++itr;

		if(!stricmp(account, session->GetAccountNameS()))
		{
			FoundUser = true;
			m_session->SystemMessage("Disconnecting user with account `%s` IP `%s` Player `%s`.", session->GetAccountNameS(),
				session->GetSocket() ? session->GetRemoteIP().c_str() : "noip", session->GetPlayer() ? session->GetPlayer()->GetName() : "noplayer");

			session->Disconnect();
		}
	}

	if ( FoundUser == false )
		m_session->SystemMessage("There is nobody online with account [%s]",account);
}

void World::DisconnectUsersWithAccountId(ui32 acct, WorldSession * m_session)
{
	SessionMap::iterator itr;
	WorldSession * session;

	bool FoundUser = false;
	for(itr = m_sessions.begin(); itr != m_sessions.end();)
	{
		session = itr->second;
		++itr;

		if(session->GetAccountId() == acct)
		{
			FoundUser = true;
			m_session->SystemMessage("Disconnecting user with account `%s` IP `%s` Player `%s`.", session->GetAccountNameS(),
				session->GetSocket() ? session->GetRemoteIP().c_str() : "noip", session->GetPlayer() ? session->GetPlayer()->GetName() : "noplayer");

			session->Disconnect();
		}
	}

	if ( FoundUser == false )
		m_session->SystemMessage("There is nobody online with account [%d]",acct);
}

void World::DisconnectUsersWithIP(const char * ip, WorldSession * m_session)
{
	SessionMap::iterator itr;
	WorldSession * session;

	bool FoundUser = false;
	for(itr = m_sessions.begin(); itr != m_sessions.end();)
	{
		session = itr->second;
		++itr;

		if(!session->GetSocket())
			continue;

		string ip2 = session->GetRemoteIP().c_str();
		if(!stricmp(ip, ip2.c_str()))
		{
			FoundUser = true;
			m_session->SystemMessage("Disconnecting user with account `%s` IP `%s` Player `%s`.", session->GetAccountNameS(),
				ip2.c_str(), session->GetPlayer() ? session->GetPlayer()->GetName() : "noplayer");

			session->Disconnect();
		}
	}
	if ( FoundUser == false )
		m_session->SystemMessage("There is nobody online with ip [%s]",ip);

}

void World::DisconnectUsersWithPlayerName(const char * plr, WorldSession * m_session)
{
	SessionMap::iterator itr;
	WorldSession * session;

	bool FoundUser = false;
	for(itr = m_sessions.begin(); itr != m_sessions.end();)
	{
		session = itr->second;
		++itr;

		if(!session->GetPlayer())
			continue;

		if(!stricmp(plr, session->GetPlayer()->GetName()))
		{
			m_session->SystemMessage("Disconnecting user with account `%s` IP `%s` Player `%s`.", session->GetAccountNameS(),
				session->GetSocket() ? session->GetRemoteIP().c_str() : "noip", session->GetPlayer() ? session->GetPlayer()->GetName() : "noplayer");

			session->Disconnect();
		}
	}
	if (FoundUser == false)
		m_session->SystemMessage("There is no body online with the name [%s]",plr);

}

string World::GetUptimeString()
{
	char str[300];
	time_t pTime = (time_t)UNIXTIME - m_StartTime;
	tm * tmv = gmtime(&pTime);

	snprintf(str, 300, "%u days, %u hours, %u minutes, %u seconds.", tmv->tm_yday, tmv->tm_hour, tmv->tm_min, tmv->tm_sec);
	return string(str);
}

bool World::EnterGameReq( WorldSession* s )
{
	if( s->HasPermissions() || objmgr.GetPlayerCount() < (uint32)sMaster.m_user_limit )
	{
		s->RealEnterGame();
	}
	else if( std::find( m_queuedUsers.begin(), m_queuedUsers.end(), s ) == m_queuedUsers.end() )
	{
		m_queuedUsers.push_back( s );
		MSG_S2C::stEnterGameLine msg;
		msg.estimateTime = m_queuedUsers.size() * m_average_leave_time;
		msg.enter_game_number = m_queuedUsers.size();
		s->SendPacket( msg );
	}
	else
		return false;

	return true;
}

void World::LeaveQueue( WorldSession* s )
{
	m_queuedUsers.remove( s );
}
