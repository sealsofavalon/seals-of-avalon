#ifndef __WORLD_H
#define __WORLD_H

#include "EventableObject.h"
//#include "BroadTextMgr.h"
#include "MazeCreate.h"
#define IS_INSTANCE(a) (((a)>1)&&((a)!=530))

enum Rates
{
	RATE_HEALTH=0,
	RATE_POWER1,	
	RATE_POWER2,	
	RATE_POWER3,	
	RATE_DROP0, // separate rates for each quality level
	RATE_DROP1,
	RATE_DROP2,
	RATE_DROP3,
	RATE_DROP4,
	RATE_DROP5,
	RATE_DROP6,
	RATE_MONEY,
	RATE_XP,
	RATE_RESTXP,
	RATE_QUESTXP,
	RATE_HONOR,
	RATE_QUESTREPUTATION,
	RATE_KILLREPUTATION,
	RATE_SKILLCHANCE,
	RATE_SKILLRATE,
	RATE_ARENAPOINTMULTIPLIER2X,
	RATE_ARENAPOINTMULTIPLIER3X,
	RATE_ARENAPOINTMULTIPLIER5X,
	MAX_RATES
};		

enum IntRates
{
	INTRATE_SAVE=0,
	INTRATE_COMPRESSION,
	INTRATE_PVPTIMER,
	MAX_INTRATES
};



enum CharCreateErrors
{
	SUCCESS,
	FAILURE,
	CANCELLED,
	DISCONNECT_FROM_SERVER,
	FAILED_TO_CONNECT,
	CONNECTED,
	WRONG_CLIENT_VERSION,
	CONNECTING_TO_SERVER,
	NEGOTIATING_SECURITY,
	NEGOTIATING_SECURITY_COMPLETE,
	NEGOTIATING_SECURITY_FAILED,
	AUTHENTICATING,
	AUTHENTICATION_SUCCESSFUL,
	AUTHENTICATION_FAILED,
	LOGIN_UNAVAIBLE,
	SERVER_IS_NOT_VALID,
	SYSTEM_UNAVAIBLE,
	SYSTEM_ERROR,
	BILLING_SYSTEM_ERROR,
	ACCOUNT_BILLING_EXPIRED,
	WRONG_CLIENT_VERSION_2,
	UNKNOWN_ACCOUNT,
	INCORRECT_PASSWORD,
	SESSION_EXPIRED,
	SERVER_SHUTTING_DOWN,
	ALREADY_LOGGED_IN,
	INVALID_LOGIN_SERVER,
	POSITION_IN_QUEUE_0,
	THIS_ACCOUNT_HAS_BEEN_BANNED,
	THIS_CHARACTER_STILL_LOGGED_ON,
	YOUR_WOW_SUBSCRIPTION_IS_EXPIRED,
	THIS_SESSION_HAS_TIMED_OUT,
	THIS_ACCOUNT_TEMP_SUSPENDED,
	ACCOUNT_BLOCKED_BY_PARENTAL_CONTROL,
	RETRIEVING_REALMLIST,
	REALMLIST_RETRIEVED,
	UNABLE_TO_CONNECT_REALMLIST_SERVER,
	INVALID_REALMLIST,
	GAME_SERVER_DOWN,
	CREATING_ACCOUNT,
	ACCOUNT_CREATED,
	ACCOUNT_CREATION_FAIL,
	RETRIEVE_CHAR_LIST,
	CHARLIST_RETRIEVED,
	CHARLIST_ERROR,
	CREATING_CHARACTER,
	CHARACTER_CREATED,
	ERROR_CREATING_CHARACTER,
	CHARACTER_CREATION_FAIL,
	NAME_IS_IN_USE,
	CREATION_OF_RACE_DISABLED,
	ALL_CHARS_ON_PVP_REALM_MUST_AT_SAME_SIDE,
	ALREADY_HAVE_MAXIMUM_CHARACTERS,
	ALREADY_HAVE_MAXIMUM_CHARACTERS_2,
	SERVER_IS_CURRENTLY_QUEUED,
	ONLY_PLAYERS_WHO_HAVE_CHARACTERS_ON_THIS_REALM,
	NEED_EXPANSION_ACCOUNT,
	DELETING_CHARACTER,
	CHARACTER_DELETED,
	CHARACTER_DELETION_FAILED,
	ENTERING_WOW,
	LOGIN_SUCCESFUL,
	WORLD_SERVER_DOWN,
	A_CHARACTER_WITH_THAT_NAME_EXISTS,
	NO_INSTANCE_SERVER_AVAIBLE,
	LOGIN_FAILED,
	LOGIN_FOR_THAT_RACE_DISABLED,
	LOGIN_FOR_THAT_RACE_CLASS_DISABLED,//check
	ENTER_NAME_FOR_CHARACTER,
	NAME_AT_LEAST_TWO_CHARACTER,
	NAME_AT_MOST_12_CHARACTER,
	NAME_CAN_CONTAIN_ONLY_CHAR,
	NAME_CONTAIN_ONLY_ONE_LANG,
	NAME_CONTAIN_PROFANTY,
	NAME_IS_RESERVED,
	YOU_CANNOT_USE_APHOS,
	YOU_CAN_ONLY_HAVE_ONE_APHOS,
	YOU_CANNOT_USE_SAME_LETTER_3_TIMES,
	NO_SPACE_BEFORE_NAME,
	BLANK,
	INVALID_CHARACTER_NAME,
	BLANK_1
	//All further codes give the number in dec.
};

// ServerMessages.dbc
enum ServerMessageType
{
	SERVER_MSG_SHUTDOWN_TIME	  = 1,
	SERVER_MSG_RESTART_TIME	   = 2,
	SERVER_MSG_STRING			 = 3,
	SERVER_MSG_SHUTDOWN_CANCELLED = 4,
	SERVER_MSG_RESTART_CANCELLED  = 5
};

enum WorldMapInfoFlag
{
	WMI_INSTANCE_ENABLED   = 0x1,
	WMI_INSTANCE_WELCOME   = 0x2,
	WMI_INSTANCE_MULTIMODE = 0x4,
	WMI_INSTANCE_XPACK_01  = 0x8, //The Burning Crusade expansion
};

enum AccountFlags
{
	ACCOUNT_FLAG_VIP		 = 0x1,
	ACCOUNT_FLAG_NO_AUTOJOIN = 0x2,
	//ACCOUNT_FLAG_XTEND_INFO  = 0x4,
	ACCOUNT_FLAG_XPACK_01	= 0x8,
};


enum REALM_TYPE
{
    REALM_PVE = 0,
    REALM_PVP = 1,
};


class BasicTaskExecutor : public ThreadBase
{
	CallbackBase * cb;
	uint32 priority;
public:
	BasicTaskExecutor(CallbackBase * Callback, uint32 Priority) : cb(Callback), priority(Priority) {}
	~BasicTaskExecutor() { delete cb; }
	bool run();
};

class Task
{
	CallbackBase * _cb;
public:
	Task(CallbackBase * cb) : _cb(cb), completed(false), in_progress(false) {}
	~Task() { delete _cb; }
	bool completed;
	bool in_progress;
	void execute();
};

struct CharacterLoaderThread : public ThreadBase
{
#ifdef WIN32
	HANDLE hEvent;
#else
	pthread_cond_t cond;
	pthread_mutex_t mutex;
#endif
	bool running;
public:
	CharacterLoaderThread();
	~CharacterLoaderThread();
	void OnShutdown();
	bool run();
};

class TaskList
{
	set<Task*> tasks;
public:
	Task * GetTask();
	void AddTask(Task* task);
	void RemoveTask(Task * task)
	{
		tasks.erase(task);
	}

	void spawn();
	void kill();

	void wait();
	void waitForThreadsToExit();
	uint32 thread_count;
	bool running;

	void incrementThreadCount()
	{
		++thread_count;
	}

	void decrementThreadCount()
	{
		--thread_count;
	}
};

enum BasicTaskExecutorPriorities
{
	BTE_PRIORITY_LOW		= 0,
	BTE_PRIORITY_MED		= 1,
	BTW_PRIORITY_HIGH	   = 2,
};


// Slow for remove in middle, oh well, wont get done much.
typedef set<WorldSession*> SessionSet;

class SERVER_DECL World : public Singleton<World>, public EventableObject
{
public:
	World();
	~World();

	/** Reloads the config and sets all of the setting variables 
	 */
	void Rehash(bool load);

	void CleanupCheaters();
	WorldSession* FindSession(uint32 account);
	WorldSession* FindSessionByName(const char *);
	WorldSession* FindSessionByTransID(ui32 transid);
	bool AddSession(WorldSession *s);

	void AddGlobalSession(WorldSession *session);
	void RemoveGlobalSession(WorldSession *session);
	void DeleteSession(WorldSession *session, bool bRemoveSet = true );

	SUNYOU_INLINE size_t GetSessionCount() const { return m_sessions.size(); }
	uint32 GetNonGmSessionCount();
	void GetStats(uint32 * GMCount, float * AverageLatency);

	SUNYOU_INLINE uint32 GetPlayerLimit() const { return m_playerLimit; }
	void SetPlayerLimit(uint32 limit) { m_playerLimit = limit; }

	SUNYOU_INLINE bool getAllowMovement() const { return m_allowMovement; }
	void SetAllowMovement(bool allow) { m_allowMovement = allow; }
	SUNYOU_INLINE bool getGMTicketStatus() { return m_gmTicketSystem; };
	bool toggleGMTicketStatus()
	{
		m_gmTicketSystem = !m_gmTicketSystem;
		return m_gmTicketSystem;
	};

	SUNYOU_INLINE std::string getGmClientChannel() { return GmClientChannel; }

	void SetMotd(const char *motd) { m_motd = motd; }
	SUNYOU_INLINE const char* GetMotd() const { return m_motd.c_str(); }

	SUNYOU_INLINE time_t GetGameTime() const { return m_gameTime; }

	bool SetInitialWorldSettings();

	void SendWorldText(const char *text, WorldSession *self = 0);
	void SendWorldWideScreenText(const char *text, WorldSession *self = 0);
	void SendGlobalMessage(PakHead& packet, WorldSession *self = 0);
	
	bool GetMaze(std::map<int, int>& sMap, int& end); 

	//bool AddBroadMessage(string text, ui8 ntype, ui8 flag, uint32 start, uint32 end, ui32 second);
	//bool AddActivityMessage(activity_mgr::activity_base_t* p);
	//bool RemoveActivityMessage(ui32 evtid);
	

	void SendZoneMessage(PakHead& packet, uint32 zoneid, WorldSession *self = 0);
	void SendInstanceMessage(PakHead& packet, uint32 instanceid, WorldSession *self = 0);
	void SendRaceMessage(PakHead& packet, uint8 race);
	void SendGamemasterMessage(PakHead& packet, WorldSession *self = 0);
	void SendGMWorldText(const char* text, WorldSession *self = 0);
	void SendMsgToALLGM(PakHead& pak);

	SUNYOU_INLINE void SetStartTime(uint32 val) { m_StartTime = val; }
	SUNYOU_INLINE uint32 GetUptime(void) { return (uint32)UNIXTIME - m_StartTime; }
	SUNYOU_INLINE uint32 GetStartTime(void) { return m_StartTime; }
	std::string GetUptimeString();

	// update the world server every frame
	void Update(time_t diff);
	void CheckForExpiredInstances();

	void ServerShutdownCountDown( uint32 t );
   
	void UpdateSessions(uint32 diff);

	SUNYOU_INLINE void setRate(int index,float value)
	{
		regen_values[index]=value;
	}

	SUNYOU_INLINE float getRate(int index)
	{
		return regen_values[index];
	}
	
	SUNYOU_INLINE uint32 getIntRate(int index)
	{
		return int_rates[index];
	}

	SUNYOU_INLINE void setIntRate(int index, uint32 value)
	{
		int_rates[index] = value;
	}

	// talent inspection lookup tables
	std::map< uint32, uint32 > InspectTalentTabPos;
	std::map< uint32, uint32 > InspectTalentTabSize;
	std::map< uint32, uint32 > InspectTalentTabBit;
	uint32 InspectTalentTabPages[12][3];

	SUNYOU_INLINE uint32 GetTimeOut(){return TimeOut;}

	vector<NameGenData> _namegendata[3];
	void LoadNameGenData();
	std::string GenerateName(uint32 type = 0);

// 	std::map<uint32, AreaTable*> mAreaIDToTable;
// 	std::map<uint32, AreaTable*> mZoneIDToTable;

	std::map<uint32,uint32> TeachingSpellMap;
	uint32 GetTeachingSpell(uint32 NormalSpellId)
	{
		map<uint32,uint32>::iterator i = TeachingSpellMap.find(NormalSpellId);
		if(i!=TeachingSpellMap.end())
			return i->second;
		return 0;
	}

	uint32 mQueueUpdateInterval;
	bool m_useIrc;

	void SaveAllPlayers();

	string MapPath;
	string vMapPath;
	bool UnloadMapFiles;
	bool BreathingEnabled;
	bool SpeedhackProtection;
	uint32 mInWorldPlayerCount;
	uint32 mAcceptedConnections;
	uint32 SocketSendBufSize;
	uint32 SocketRecvBufSize;

	uint32 HordePlayers;
	uint32 AlliancePlayers;
	uint32 PeakSessionCount;
	bool SendStatsOnJoin;
	SessionSet gmList;

	void ShutdownClasses();
	void DeleteObject(Object * obj);

	uint32 compression_threshold;

	void	SetKickAFKPlayerTime(uint32 idletimer){m_KickAFKPlayers=idletimer;}
	uint32	GetKickAFKPlayerTime(){return m_KickAFKPlayers;}

    uint32 GetRealmType() { return realmtype; }

	uint32 flood_lines;
	uint32 flood_seconds;
	bool flood_message;
	bool gm_skip_attunement;

	string announce_tag;
	bool GMAdminTag;
	bool NameinAnnounce;
	bool NameinWAnnounce;
	bool announce_output;

	string ann_namecolor;
	string ann_gmtagcolor;
	string ann_tagcolor;
	string ann_msgcolor;

	bool show_gm_in_who_list;
	//bool allow_gm_friends;
	uint32 map_unload_time;

	bool antihack_teleport;
	bool antihack_speed;
	bool antihack_flight;
	uint32 flyhack_threshold;
	bool no_antihack_on_gm;

	uint32 m_ExtraAllExpRatio;
	uint32 m_ExtraAllExpDuration;
	uint32 m_ExtraExpRatio;
	uint32 m_ExtraExpDuration;

	void LoadAccountDataProc(QueryResultVector& results, uint32 AccountId);

	void PollCharacterInsertQueue(DatabaseConnection * con);
	void PollMailboxInsertQueue();
	void DisconnectUsersWithAccount(const char * account, WorldSession * session);
	void DisconnectUsersWithAccountId(ui32 acct, WorldSession * session);
	void DisconnectUsersWithIP(const char * ip, WorldSession * session);
	void DisconnectUsersWithPlayerName(const char * plr, WorldSession * session);

	void LogoutPlayers();

	bool ExecuteSqlToDBServer( QueryBuffer* buf );
	bool ExecuteSqlToDBServer( const char* sql, ... );

	bool wait_save_sql;

	bool EnterGameReq( WorldSession* s );
	void LeaveQueue( WorldSession* s );

protected:
	// update Stuff, FIXME: use diff
	time_t _UpdateGameTime()
	{
		// Update Server time
		time_t thisTime = UNIXTIME;
		m_gameTime += thisTime - m_lastTick;
		m_lastTick = thisTime;

		return m_gameTime;
	}
	void FillSpellReplacementsTable();

public:
	typedef HM_NAMESPACE::hash_map<uint32, WorldSession*> SessionMap;

	SessionMap m_sessions;
	SessionMap m_gmsessions;
	SessionMap m_transSessions;

private:
	EventableObjectHolder * eventholder;
	//BroadTextMgr* broadtextmgr;
	MazeCreate* mazemapcreate;
	//! Timers
	typedef HM_NAMESPACE::hash_map<uint32, AreaTrigger*> AreaTriggerMap;
	AreaTriggerMap m_AreaTrigger;

protected:
	Mutex SessionsMutex;//FOR GLOBAL !
	SessionSet Sessions;

	float regen_values[MAX_RATES];
	uint32 int_rates[MAX_INTRATES];

	uint32 m_playerLimit;
	bool m_allowMovement;
	bool m_gmTicketSystem;
	std::string m_motd;
   
    uint32 realmtype;

	time_t m_gameTime;
	time_t m_lastTick;
	uint32 TimeOut;

	uint32 m_StartTime;
	uint32 m_queueUpdateTimer;

	uint32	m_KickAFKPlayers;//don't lag the server if you are useless anyway :P
	std::list<WorldSession*> m_queuedUsers;
	uint32 m_average_leave_time;
	uint32 m_last_leave_time;
	uint32 m_leave_count;

public:
	std::string GmClientChannel;
	bool m_reqGmForCommands;
	bool m_lfgForNonLfg;
	list<SpellEntry*> dummyspells;
	uint32 m_levelCap;
	uint32 m_genLevelCap;
	uint32 m_startLevel;
	bool m_limitedNames;
	bool m_useAccountData;
	bool m_forceGMTag;
	bool m_showKick;

	char * m_banTable;

	static float m_movementCompressThreshold;
	static float m_movementCompressThresholdCreatures;
	static uint32 m_movementCompressRate;
	static uint32 m_movementCompressInterval;
	static float m_speedHackThreshold;
};

#define sWorld World::getSingleton()

#endif
