#include "StdAfx.h"

SERVER_DECL InstanceMgr sInstanceMgr;
initialiseSingleton( FormationMgr );

InstanceMgr::InstanceMgr() : m_nMapMgrCount( 0 )
{
	memset(m_maps, 0, sizeof(Map*)* NUM_MAPS);
	memset(m_instances, 0, sizeof(InstanceMap*) * NUM_MAPS);
	memset(m_singleMaps,0, sizeof(MapMgr*) * NUM_MAPS);
}

void InstanceMgr::Load()
{
	new FormationMgr;

	// Create all non-instance type maps.
	QueryResult *result = CharacterDatabase.Query( "SELECT * FROM instance_max_id" );
	if( result )
	{
		m_InstanceHigh = result->Fetch()[0].GetUInt32()+1;
		if( m_InstanceHigh < 1000 )
			m_InstanceHigh = 1001;
		delete result;
	}
	else
		m_InstanceHigh = 1001;

	// load each map we have in the database.
	result = WorldDatabase.Query("SELECT DISTINCT Map FROM creature_spawns");
	if(result)
	{
		do
		{
			if(WorldMapInfoStorage.LookupEntry(result->Fetch()[0].GetUInt32()) == NULL)
				continue;

			if( result->Fetch()[0].GetUInt32() >= NUM_MAPS )
			{
				MyLog::log->warn("InstanceMgr : One or more of your creature_spawns rows specifies an invalid map: %u", result->Fetch()[0].GetUInt32());
				continue;
			}

			//_CreateMap(result->Fetch()[0].GetUInt32());
			_CreateMap(result->Fetch()[0].GetUInt32());
		} while(result->NextRow());
		delete result;
	}

	// create maps for any we don't have yet.
	StorageContainerIterator<MapInfo> * itr = WorldMapInfoStorage.MakeIterator();
	while(!itr->AtEnd())
	{
		if( itr->Get()->mapid >= NUM_MAPS )
		{
			MyLog::log->warn("InstanceMgr : One or more of your worldmap_info rows specifies an invalid map: %u", itr->Get()->mapid);
			assert(0);
		}

		if(m_maps[itr->Get()->mapid] == NULL)
		{
			_CreateMap(itr->Get()->mapid);
		}
		//_CreateMap(itr->Get()->mapid);

		itr->Inc();
	}
	itr->Destruct();

	// load saved instances
	_LoadInstances();
}

InstanceMgr::~InstanceMgr()
{
}

void InstanceMgr::Shutdown()
{
	uint32 i;
	InstanceMap::iterator itr;
	for(i = 0; i < NUM_MAPS; ++i)
	{
		if(m_instances[i] != NULL)
		{
			for(itr = m_instances[i]->begin(); itr != m_instances[i]->end(); ++itr)
			{
				delete itr->second;
			}

			delete m_instances[i];
			m_instances[i]=NULL;
		}

		if(m_singleMaps[i] != NULL)
		{
			MapMgr * ptr = m_singleMaps[i];
			delete ptr;
			m_singleMaps[i]=NULL;
		}

		if(m_maps[i] != NULL)
		{
			delete m_maps[i];
			m_maps[i]=NULL;
		}
	}

	delete FormationMgr::getSingletonPtr();
}

uint32 InstanceMgr::PreTeleport(uint32 mapid, Player * plr, uint32 instanceid)
{
	// preteleport is where all the magic happens :P instance creation, etc.
	MapInfo * inf = WorldMapInfoStorage.LookupEntry(mapid);
	Group * pGroup;
	InstanceMap * instancemap;
	Instance * in;

	if(inf == NULL || mapid>=NUM_MAPS)
		return INSTANCE_ABORT_NOT_FOUND;

	// main continent check.
	if(inf->type == INSTANCE_NULL)
	{
		// this will be useful when clustering comes into play.
		// we can check if the destination world server is online or not and then cancel them before they load.
		return (m_singleMaps[mapid] != NULL) ? INSTANCE_OK : INSTANCE_ABORT_NOT_FOUND;
	}

	// shouldn't happen
	// remove by gui
//	if(inf->type==INSTANCE_PVP)
//		return INSTANCE_ABORT_NOT_FOUND;

	// players without groups cannot enter raid instances (no soloing them:P)
	if(plr->GetGroup() == NULL && (inf->type == INSTANCE_RAID || inf->type == INSTANCE_MULTIMODE) && !plr->triggerpass_cheat)
		return INSTANCE_ABORT_NOT_IN_RAID_GROUP;

	// check that heroic mode is available if the player has requested it.
	if(plr->iInstanceType && inf->type != INSTANCE_MULTIMODE)
		return INSTANCE_ABORT_HEROIC_MODE_NOT_AVAILABLE;

	// if we are here, it means:
	// 1) we're a non-raid instance
	// 2) we're a raid instance, and the person is in a group.
	// so, first we have to check if they have an instance on this map already, if so, allow them to teleport to that.
	// otherwise, we can create them a new one.
	instancemap = m_instances[mapid];

	// set up our pointers (cleaner code is always good)
	pGroup = plr->GetGroup();

	if(instancemap == NULL)
	{
		if(instanceid != 0)
		{
			return INSTANCE_ABORT_NOT_FOUND;
		}

		// gotta create the hashmap.
		m_instances[mapid] = new InstanceMap;
		instancemap = m_instances[mapid];
	}
	else
	{
		InstanceMap::iterator itr;
		if(instanceid != 0)
		{
			itr = instancemap->find(instanceid);
			if(itr != instancemap->end()) {
				return INSTANCE_OK;
			} else {
				return INSTANCE_ABORT_NOT_FOUND;
			}
		}
		else
		{
			// search the instance and see if we have one here.
			for(itr = instancemap->begin(); itr != instancemap->end();)
			{
				in = itr->second;
				++itr;
				if(PlayerOwnsInstance(in, plr))
				{

					// check the player count and in combat status.
					if(in->m_mapMgr)
					{
						if(in->m_mapMgr->IsCombatInProgress())
							return INSTANCE_ABORT_ENCOUNTER;

						if(in->m_mapMgr->GetPlayerCount() >= inf->playerlimit)
							return INSTANCE_ABORT_FULL;
					}

					// found our instance, allow him in.
					return INSTANCE_OK;
				}
			}
		}
	}
	//if( inf->screenid )
	//	return INSTANCE_ABORT_NOT_FOUND;

	// if we're here, it means we need to create a new instance.
	in = new Instance;
	in->m_creation = UNIXTIME;
	in->m_expiration = 0; ///*(inf->type == INSTANCE_NONRAID) ? 0 : */UNIXTIME + inf->cooldown;		// expire time 0 is 10 minutes after last player leaves
	in->m_creatorGuid = pGroup ? 0 : plr->GetLowGUID();		// creator guid is 0 if its owned by a group.
	in->m_creatorGroup = pGroup ? pGroup->GetID() : 0;
	in->m_difficulty = plr->iInstanceType;
	in->m_instanceId = GenerateInstanceID();
	in->m_mapId = mapid;
	in->m_mapMgr = NULL;		// always start off without a map manager, it is created in GetInstance()
	in->m_mapInfo = inf;
	in->m_isBattleground=false;
	in->can_expire = true;
	plr->SetInstanceID(in->m_instanceId);
	m_mapInstances[in->m_instanceId] = in;
	MyLog::log->debug("InstanceMgr : Creating instance for player %u and group %u on map %u. (%u)", in->m_creatorGuid, in->m_creatorGroup, in->m_mapId, in->m_instanceId);

	// save our new instance to the database.
	in->SaveToDB();

	// apply it in the instance map
	instancemap->insert( InstanceMap::value_type( in->m_instanceId, in ) );

	// instance created ok, i guess? return the ok for him to transport.
	return INSTANCE_OK;
}

MapMgr* InstanceMgr::GetMapMgr(uint32 mapId)
{
	if( mapId >= NUM_MAPS)
		return NULL;

	return m_singleMaps[mapId];
}

MapMgr * InstanceMgr::GetInstance(Object* obj)
{
	Player * plr;
	Instance * in;
	InstanceMap::iterator itr;
	InstanceMap * instancemap;
	MapInfo * inf = WorldMapInfoStorage.LookupEntry(obj->GetMapId());

	// we can *never* teleport to maps without a mapinfo.
	if( inf == NULL || obj->GetMapId() >= NUM_MAPS )
		return NULL;

	if( obj->IsPlayer() )
	{
		// players can join instances based on their groups/solo status.
		plr = static_cast< Player* >( obj );

		// single-instance maps never go into the instance set.
		if( inf->type == INSTANCE_NULL )
			return m_singleMaps[obj->GetMapId()];

		instancemap = m_instances[obj->GetMapId()];
		if(instancemap != NULL)
		{
			// check our saved instance id. see if its valid, and if we can join before trying to find one.
			itr = instancemap->find(obj->GetInstanceID());
			if(itr != instancemap->end())
			{
				if(itr->second->m_mapMgr)
				{
					return itr->second->m_mapMgr;
				}
			}
			/*
			if( inf->screenid != 0 )
			{
				return NULL;
			}
			*/

			// iterate over our instances, and see if any of them are owned/joinable by him.
			for(itr = instancemap->begin(); itr != instancemap->end();)
			{
				in = itr->second;
				++itr;
				if(PlayerOwnsInstance(in, plr))
				{
					// this is our instance.
					if(in->m_mapMgr == NULL)
					{
						/*if(plr->m_TeleportState == 1)
						{
							// the player is loading. boot him out to the entry point, we don't want to spam useless instances on startup.
							return NULL;
						}*/

						// create the actual instance.
						in->m_mapMgr = _CreateInstance(in);
						in->m_mapMgr->m_pInstance = in;
						return in->m_mapMgr;
					}
					else
					{
						// instance is already created.
						return in->m_mapMgr;
					}
				}
			}
		}

		// if we're here, it means there are no instances on that map, or none of the instances on that map are joinable
		// by this player.
		return NULL;
	}
	else
	{
		// units are *always* limited to their set instance ids.
		if(inf->type == INSTANCE_NULL)
			return m_singleMaps[obj->GetMapId()];

		instancemap = m_instances[obj->GetMapId()];
		if(instancemap)
		{
			itr = instancemap->find(obj->GetInstanceID());
			if(itr != instancemap->end())
			{
				// we never create instances just for units.
				return itr->second->m_mapMgr;
			}
		}

		// instance is non-existant (shouldn't really happen for units...)
		return NULL;
	}
}

MapMgr* InstanceMgr::GetInstance( uint32 mapid, uint32 instanceid )
{
	Player * plr;
	Instance * in;
	InstanceMap::iterator itr;
	InstanceMap * instancemap;
	MapInfo * inf = WorldMapInfoStorage.LookupEntry( mapid );

	// we can *never* teleport to maps without a mapinfo.
	if( inf == NULL || mapid >= NUM_MAPS )
		return NULL;


	// single-instance maps never go into the instance set.
	if( inf->type == INSTANCE_NULL )
		return m_singleMaps[mapid];

	instancemap = m_instances[mapid];
	if(instancemap != NULL)
	{
		// check our saved instance id. see if its valid, and if we can join before trying to find one.
		itr = instancemap->find(instanceid);
		if(itr != instancemap->end())
		{
			if(itr->second->m_mapMgr)
			{
				return itr->second->m_mapMgr;
			}
		}
	}
	return NULL;
}

MapMgr* InstanceMgr::_CreateInstance(uint32 mapid, uint32 instanceid)
{
	MapInfo * inf = WorldMapInfoStorage.LookupEntry(mapid);
	MapMgr * ret;

	ASSERT(inf && inf->type == INSTANCE_NULL);
	ASSERT(mapid < NUM_MAPS && m_maps[mapid] != NULL);

	MyLog::log->notice("InstanceMgr : Creating continent %s.", m_maps[mapid]->GetName());

	ret = new MapMgr(m_maps[mapid], mapid, instanceid);
	ASSERT(ret);

	// start its thread
	ret->Init();

	// assign pointer
	m_singleMaps[mapid] = ret;
	return ret;
}
void InstanceMgr::Update( uint32 mstime )
{
	for( uint32 i = 0; i < NUM_MAPS; ++i )
	{
		if( m_singleMaps[i] )
		{
			m_singleMaps[i]->Update( mstime );
		}
		if( m_instances[i] )
		{
			for( InstanceMap::iterator it = m_instances[i]->begin(); it != m_instances[i]->end(); )
			{
				InstanceMap::iterator it2 = it++;
				Instance* pInstance = it2->second;
				if( pInstance && pInstance->m_mapMgr )
					pInstance->m_mapMgr->Update( mstime );
			}
		}
	}
}

void InstanceMgr::LogPlayerCnt()
{
	fprintf(MyLog::serverlog, "mapid        mapname          isfb playercnt    playermaxcnt creaturecnt \n");

	for( uint32 i = 0; i < NUM_MAPS; ++i )
	{
		MapMgr * mapmgr = NULL;
		if( m_singleMaps[i] )
		{
			mapmgr = m_singleMaps[i];
			fprintf(MyLog::serverlog, "%5d%20s%5s%10d%10d%10d\n", mapmgr->GetMapId(), mapmgr->GetMapInfo()->name, "No", mapmgr->GetPlayerCount(), mapmgr->GetPlayerMaxCnt(), 0);
		}
		if( m_instances[i] )
		{
			for( InstanceMap::iterator it = m_instances[i]->begin(); it != m_instances[i]->end(); ++it )
			{
				Instance* pInstance = it->second;
				if( pInstance && pInstance->m_mapMgr )
				{
					mapmgr = pInstance->m_mapMgr;
					pInstance->m_mapMgr->Update( getMSTime() );
					fprintf(MyLog::serverlog, "%5d%20s%5s%10d%10d%10d\n", mapmgr->GetMapId(), mapmgr->GetMapInfo()->name, "Yes", mapmgr->GetPlayerCount(), mapmgr->GetPlayerMaxCnt(), 0);
				}
			}
		}
	}

	fflush(MyLog::serverlog);
}

Instance* InstanceMgr::GetInstanceByID( uint32 id )
{
	std::map<uint32, Instance*>::iterator it = m_mapInstances.find( id );
	if( it != m_mapInstances.end() )
		return it->second;
	return NULL;
}

MapMgr * InstanceMgr::_CreateInstance(Instance * in)
{
	MyLog::log->notice("InstanceMgr : Creating saved instance %u (%s)", in->m_instanceId, m_maps[in->m_mapId]->GetName());
	ASSERT(in->m_mapMgr==NULL);

	// we don't have to check for world map info here, since the instance wouldn't have been saved if it didn't have any.
	in->m_mapMgr = new MapMgr(m_maps[in->m_mapId], in->m_mapId, in->m_instanceId);
	in->m_mapMgr->pInstance = in;
	in->m_mapMgr->iInstanceMode = in->m_difficulty;
	in->m_mapMgr->InactiveMoveTime = 60+UNIXTIME;
	in->m_mapMgr->Init();
	return in->m_mapMgr;
}

void InstanceMgr::_CreateMap(uint32 mapid)
{
	if( mapid >= NUM_MAPS )
		return;

	MapInfo * inf;

	inf = WorldMapInfoStorage.LookupEntry(mapid);
	if(inf==NULL)
	{
		MyLog::log->error("InstanceMgr : _CreateMap[%d] not found this mapid in table!", mapid);
		return;
	}
	if(m_maps[mapid]!=NULL)
	{
		MyLog::log->error("InstanceMgr : _CreateMap[%d] this mapid to be created twice!", mapid);
		return;
	}

	m_maps[mapid] = new Map(mapid, inf);
	if(inf->type == INSTANCE_NULL)
	{
		// we're a continent, create the instance.
		_CreateInstance(mapid, GenerateInstanceID());
	}
}

uint32 InstanceMgr::GenerateInstanceID()
{
	uint32 iid;
	iid = m_InstanceHigh++;
	CharacterDatabase.Execute( "update instance_max_id set maxid = %d", iid );
	return iid;
}

void BuildStats(MapMgr * mgr, char * m_file, Instance * inst, MapInfo * inf)
{
	char tmp[200];
	strcpy(tmp, "");
#define pushline strcat(m_file, tmp)

	sprintf(tmp, "	<instance>\n");																												pushline;
	sprintf(tmp, "		<map>%u</map>\n", mgr->GetMapId());																						pushline;
	sprintf(tmp, "		<maptype>%u</maptype>\n", inf->type);																						pushline;
	sprintf(tmp, "		<players>%u</players>\n", mgr->GetPlayerCount());																			pushline;
	sprintf(tmp, "		<maxplayers>%u</maxplayers>\n", inf->playerlimit);																		pushline;

	//<creationtime>
	if (inst)
	{
		tm *ttime = localtime( &inst->m_creation );
		sprintf(tmp, "		<creationtime>%02u:%02u:%02u %02u/%02u/%u</creationtime>\n",ttime->tm_hour, ttime->tm_min, ttime->tm_sec, ttime->tm_mday, ttime->tm_mon, uint32( ttime->tm_year + 1900 ));
		pushline;
	}
	else
	{
		sprintf(tmp, "		<creationtime>N/A</creationtime>\n");
		pushline;
	}

	//<expirytime>
	if (inst && inst->m_expiration)
	{
		tm *ttime = localtime( &inst->m_expiration );
		sprintf(tmp, "		<expirytime>%02u:%02u:%02u %02u/%02u/%u</expirytime>\n",ttime->tm_hour, ttime->tm_min, ttime->tm_sec, ttime->tm_mday, ttime->tm_mon, uint32( ttime->tm_year + 1900 ));
		pushline;
	}
	else
	{
		sprintf(tmp, "		<expirytime>N/A</expirytime>\n");
		pushline;

	}
	//<idletime>
	if (mgr->InactiveMoveTime)
	{
		tm *ttime = localtime( &mgr->InactiveMoveTime );
		sprintf(tmp, "		<idletime>%02u:%02u:%02u %02u/%02u/%u</idletime>\n",ttime->tm_hour, ttime->tm_min, ttime->tm_sec, ttime->tm_mday, ttime->tm_mon, uint32( ttime->tm_year + 1900 ));
		pushline;
	}
	else
	{
		sprintf(tmp, "		<idletime>N/A</idletime>\n");
		pushline;
	}

	sprintf(tmp, "	</instance>\n");																											pushline;
#undef pushline
}

void InstanceMgr::BuildXMLStats(char * m_file)
{
	uint32 i;
	InstanceMap::iterator itr;
	InstanceMap * instancemap;
	Instance * in;

	for(i = 0; i < NUM_MAPS; ++i)
	{
		if(m_singleMaps[i] != NULL)
			BuildStats(m_singleMaps[i], m_file, NULL, m_singleMaps[i]->GetMapInfo());
		else
		{
			instancemap = m_instances[i];
			if(instancemap != NULL)
			{
				for(itr = instancemap->begin(); itr != instancemap->end();)
				{
					in = itr->second;
					++itr;

					if(in->m_mapMgr==NULL)
						continue;

					BuildStats(in->m_mapMgr, m_file, in, in->m_mapInfo);
				}
			}
		}
	}
}

void InstanceMgr::_LoadInstances()
{
	MapInfo * inf;
	Instance * in;
	QueryResult * result;

	// clear any instances that have expired.
	MyLog::log->notice("InstanceMgr : Deleting Expired Instances...");
	CharacterDatabase.WaitExecute("DELETE FROM instances WHERE expiration <= %u", UNIXTIME);

	// load saved instances
	result = CharacterDatabase.Query("SELECT * FROM instances");
	MyLog::log->notice("InstanceMgr : Loading %u saved instances." , result ? result->GetRowCount() : 0);

	if(result)
	{
		do
		{
			inf = WorldMapInfoStorage.LookupEntry(result->Fetch()[1].GetUInt32());
			if(inf == NULL || result->Fetch()[1].GetUInt32() >= NUM_MAPS)
			{
				CharacterDatabase.WaitExecute("DELETE FROM instances WHERE mapid = %u", result->Fetch()[1].GetUInt32());
				continue;
			}

			in = new Instance();
			in->m_mapInfo = inf;
			in->LoadFromDB(result->Fetch());

			m_mapInstances[in->m_instanceId] = in;

			// this assumes that groups are already loaded at this point.
			if(in->m_creatorGroup && objmgr.GetGroupById(in->m_creatorGroup) == NULL)
			{
				delete in;
				continue;
			}

			if(m_instances[in->m_mapId] == NULL)
				m_instances[in->m_mapId] = new InstanceMap;

			m_instances[in->m_mapId]->insert( InstanceMap::value_type( in->m_instanceId, in ) );

		} while(result->NextRow());
		delete result;
	}
}

void Instance::LoadFromDB(Field * fields)
{
	char * p, *q;
	char * m_npcstring = strdup(fields[4].GetString());

	m_instanceId = fields[0].GetUInt32();
	m_mapId = fields[1].GetUInt32();
	m_creation = fields[2].GetUInt32();
	m_expiration = fields[3].GetUInt32();
	m_difficulty = fields[5].GetUInt32();
	m_creatorGroup = fields[6].GetUInt32();
	m_creatorGuid = fields[7].GetUInt32();
	m_mapMgr=NULL;

	// process saved npc's
	q = m_npcstring;
	p = strchr(m_npcstring, ' ');
	while(p)
	{
		*p = 0;
		uint32 val = atol(q);
		if (val)
			m_killedNpcs.insert( val );
		q = p+1;
		p = strchr(q, ' ');
	}

	free(m_npcstring);
}

void InstanceMgr::ResetSavedInstances(Player * plr)
{
	MSG_S2C::stInstance_Reset Msg;
	Instance * in;
	InstanceMap::iterator itr;
	InstanceMap * instancemap;
	uint32 i;

	if(!plr->IsInWorld() || plr->GetMapMgr()->GetMapInfo()->type != INSTANCE_NULL)
		return;

	for(i = 0; i < NUM_MAPS; ++i)
	{
		if(m_instances[i] != NULL)
		{
			instancemap = m_instances[i];
			for(itr = instancemap->begin(); itr != instancemap->end();)
			{
				in = itr->second;
				++itr;

				if( ( in->m_mapInfo->type == INSTANCE_NONRAID && (plr->GetGroup() && plr->GetGroup()->GetID() == in->m_creatorGroup) ) || ( in->m_mapInfo->type == INSTANCE_NONRAID && plr->GetLowGUID() == in->m_creatorGuid ) )
				{
					if(in->m_mapMgr && in->m_mapMgr->HasPlayers())
					{
						plr->GetSession()->SystemMessage("Failed to reset instance %u (%s), due to players still inside.", in->m_instanceId, in->m_mapMgr->GetMapInfo()->name);
						continue;
					}

					// <mapid> has been reset.
					Msg.mapid = uint32(in->m_mapId);
					plr->GetSession()->SendPacket(Msg);

					// destroy the instance
					_DeleteInstance(in, true);
				}
			}
		}
	}
}

void InstanceMgr::OnGroupDestruction(Group * pGroup)
{
	// this means a group has been deleted, so lets clear out all instances that they owned.
	// (instances don't transfer to the group leader, or anything)
	Instance * in;
	InstanceMap::iterator itr;
	InstanceMap * instancemap;
	uint32 i;

	for(i = 0; i < NUM_MAPS; ++i)
	{
		instancemap = m_instances[i];
		if(instancemap)
		{
			for(itr = instancemap->begin(); itr != instancemap->end();)
			{
				in = itr->second;
				++itr;

				if(in->m_creatorGroup && in->m_creatorGroup == pGroup->GetID())
					_DeleteInstance(in, false);
			}
		}
	}
}

void InstanceMgr::DeleteSunyouInstance( Instance* in, bool ForcePlayersOut )
{
	_DeleteInstance( in, ForcePlayersOut );
}

bool InstanceMgr::HasInstance( uint32 mapid, uint32 instanceid )
{
	if( mapid >= NUM_MAPS )
		return false;
	if( m_singleMaps[mapid] ) return true;
	InstanceMap* instancemap = m_instances[mapid];
	if( !instancemap )
		return false;
	return instancemap->end() != instancemap->find( instanceid );
}

MapMgr* InstanceMgr::GetUniqueMapMgr( uint32 mapid, uint32 instanceid )
{
	if( mapid >= NUM_MAPS )
		return NULL;
	if( m_singleMaps[mapid] ) return m_singleMaps[mapid];
	InstanceMap* instancemap = m_instances[mapid];
	if( !instancemap )
		return NULL;
	InstanceMap::iterator it = instancemap->find( instanceid );
	if( it != instancemap->end() )
	{
		return it->second->m_mapMgr;
	}
	else
		return NULL;
}


int InstanceMgr::GetMapMgrCount() const
{
	return m_nMapMgrCount;
}

void InstanceMgr::IncreaseMapMgrCount()
{
	++m_nMapMgrCount;

	MyLog::log->info("Increase map manager, current count:%d", m_nMapMgrCount );
}

void InstanceMgr::DecreaseMapMgrCount()
{
	--m_nMapMgrCount;

	MyLog::log->info("Decrease map manager, current count:%d", m_nMapMgrCount );
}

bool InstanceMgr::_DeleteInstance(Instance * in, bool ForcePlayersOut)
{
	InstanceMap * instancemap;
	InstanceMap::iterator itr;

	if(in->m_mapMgr)
	{
		// "ForcePlayersOut" will teleport the players in this instance to their entry point/hearthstone.
		// otherwise, they will get a 60 second timeout telling them they are not in this instance's group.
		if(in->m_mapMgr->HasPlayers())
		{
			if(ForcePlayersOut)
				in->m_mapMgr->InstanceShutdown();
			else
			{
				in->m_mapMgr->BeginInstanceExpireCountdown();
				in->m_mapMgr->pInstance = NULL;
			}
		}
		else
			in->m_mapMgr->InstanceShutdown();
	}

	// remove the instance from the large map.
	instancemap = m_instances[in->m_mapId];
	if(instancemap)
	{
		itr = instancemap->find(in->m_instanceId);
		if(itr != instancemap->end())
			instancemap->erase(itr);
	}

	// cleanup corpses, database references
	in->DeleteFromDB();
	sInstanceMgr.m_mapInstances.erase( in->m_instanceId );

	// delete the instance pointer.
	delete in;

	return true;
}

void Instance::DeleteFromDB()
{
	// cleanup all the corpses on this map
	sWorld.ExecuteSqlToDBServer("DELETE FROM corpses WHERE instanceid = %u", m_instanceId);

	// delete from the database
	sWorld.ExecuteSqlToDBServer("DELETE FROM instances WHERE id = %u", m_instanceId);
}

Instance::Instance() : can_expire( false )
{
}

void InstanceMgr::CheckForExpiredInstances()
{
	// checking for any expired instances.
	Instance * in;
	InstanceMap::iterator itr;
	InstanceMap * instancemap;
	uint32 i;

	for(i = 0; i < NUM_MAPS; ++i)
	{
		instancemap = m_instances[i];
		if(instancemap)
		{
			for(itr = instancemap->begin(); itr != instancemap->end();)
			{
				in = itr->second;
				++itr;

				// use a "soft" delete here.
				if(in && in->m_mapInfo && in->m_mapInfo->type == INSTANCE_NONRAID && in->can_expire && HasInstanceExpired(in))
				{
					in->m_mapMgr->Release();
					//_DeleteInstance(in, false);
					//delete temp;
				}
			}

		}
	}
}

void InstanceMgr::BuildSavedInstancesForPlayer(Player * plr)
{
	Instance * in;
	InstanceMap::iterator itr;
	InstanceMap * instancemap;
	uint32 i;

	if(!plr->IsInWorld() || plr->GetMapMgr()->GetMapInfo()->type != INSTANCE_NULL)
	{
		for(i = 0; i < NUM_MAPS; ++i)
		{
			if(m_instances[i] != NULL)
			{
				instancemap = m_instances[i];
				for(itr = instancemap->begin(); itr != instancemap->end();)
				{
					in = itr->second;
					++itr;

					if( PlayerOwnsInstance(in, plr) && in->m_mapInfo->type == INSTANCE_NONRAID )
					{

						MSG_S2C::stInstance_Save MsgSave;
						MsgSave.mapid = uint32(in->m_mapId);
						plr->GetSession()->SendPacket(MsgSave);

						MSG_S2C::stInstance_Reset_Active MsgReset;
						MsgReset.reset = 1;
						plr->GetSession()->SendPacket(MsgReset);

						return;
					}
				}
			}
		}
	}

	MSG_S2C::stInstance_Reset_Active MsgReset;
	MsgReset.reset = 0;
	plr->GetSession()->SendPacket(MsgReset);
}

void InstanceMgr::BuildRaidSavedInstancesForPlayer(Player * plr)
{
	MSG_S2C::stRaid_Instance_Info Msg;
	Instance * in;
	InstanceMap::iterator itr;
	InstanceMap * instancemap;
	uint32 i;
	uint32 counter = 0;


	for(i = 0; i < NUM_MAPS; ++i)
	{
		if(m_instances[i] != NULL)
		{
			instancemap = m_instances[i];
			for(itr = instancemap->begin(); itr != instancemap->end();)
			{
				in = itr->second;
				++itr;

				if( in->m_mapInfo->type != INSTANCE_NONRAID && PlayerOwnsInstance(in, plr) )
				{
					MSG_S2C::stRaid_Instance_Info::stMap map;
					map.mapid = in->m_mapId;
					map.expire = uint32(in->m_expiration - UNIXTIME);
					map.instanceid = in->m_instanceId;
					Msg.vMaps.push_back( map );
				}
			}
		}
	}

	plr->GetSession()->SendPacket(Msg);
}

void Instance::SaveToDB()
{
	// don't save non-raid instances.
	if(m_mapInfo->type == INSTANCE_NONRAID || m_isBattleground)
		return;

	std::stringstream ss;
	set<uint32>::iterator itr;

	ss << "REPLACE INTO instances VALUES("
		<< m_instanceId << ","
		<< m_mapId << ","
		<< (uint32)m_creation << ","
		<< (uint32)m_expiration << ",'";

	for(itr = m_killedNpcs.begin(); itr != m_killedNpcs.end(); ++itr)
		ss << (*itr) << " ";

	ss << "',"
		<< m_difficulty << ","
		<< m_creatorGroup << ","
		<< m_creatorGuid << ")";

	sWorld.ExecuteSqlToDBServer(ss.str().c_str());
}

void InstanceMgr::PlayerLeftGroup(Group * pGroup, Player * pPlayer, uint32 lastplayerid )
{
	// does this group own any instances? we have to kick the player out of those instances.
	Instance * in;
	InstanceMap::iterator itr;
	InstanceMap * instancemap;
	MSG_S2C::stRaid_Group_Only Msg;
	uint32 i;

	for(i = 0; i < NUM_MAPS; ++i)
	{
		instancemap = m_instances[i];
		if(instancemap)
		{
			for(itr = instancemap->begin(); itr != instancemap->end();)
			{
				in = itr->second;
				++itr;

				if( in->m_creatorGuid == pPlayer->GetLowGUID() )
				{
					in->m_creatorGuid = lastplayerid;
				}
				if(in->m_creatorGroup && in->m_creatorGroup == pGroup->GetID())
				{
					// better make sure we're actually in that instance.. :P
					if(!pPlayer->raidgrouponlysent && pPlayer->GetInstanceID() == (int32)in->m_instanceId)
					{
						Msg.unk1 = uint32(60000);
						Msg.unk2 = uint32(1);
						pPlayer->GetSession()->SendPacket(Msg);
						pPlayer->raidgrouponlysent=true;

						sEventMgr.AddEvent(pPlayer, &Player::EjectFromInstance, EVENT_PLAYER_EJECT_FROM_INSTANCE, 60000, 1, EVENT_FLAG_DO_NOT_EXECUTE_IN_WORLD_CONTEXT);

						return;
					}
				}
			}
		}
	}
}

void InstanceMgr::PlayerNewGroup( uint32 plyid, uint32 newgroupid )
{
	Instance * in;
	InstanceMap::iterator itr;
	InstanceMap * instancemap;
	MSG_S2C::stRaid_Group_Only Msg;
	uint32 i;

	for(i = 0; i < NUM_MAPS; ++i)
	{
		instancemap = m_instances[i];
		if(instancemap)
		{
			for(itr = instancemap->begin(); itr != instancemap->end();)
			{
				in = itr->second;
				++itr;
				if( in->m_creatorGuid == plyid )
					in->m_creatorGroup = newgroupid;
			}
		}
	}
}

bool InstanceMgr::PlayerOwnsInstance(Instance * pInstance, Player * pPlayer)
{
	// expired?
	/*
	if( pInstance->m_expiration && (UNIXTIME+20) >= pInstance->m_expiration)
	{
		// delete the instance
		_DeleteInstance(pInstance, true);
		return false;
	}
	*/
	/*
	if( pInstance->expire )
	{
		if( !pInstance->m_mapMgr->HasPlayers() )
		{
			_DeleteInstance(pInstance, true);
			delete pInstance->m_mapMgr;
			return false;
		}
	}
	*/

	Group* gp = pPlayer->GetGroup();
	if( gp )
	{
		if( gp->HasMember( pInstance->m_creatorGuid ) )
			return true;
		if( pInstance->m_creatorGroup == gp->GetID() )
			return true;
	}
	else
	{
		if( pPlayer->GetLowGUID() == pInstance->m_creatorGuid )
			return true;
	}

//	if( (pPlayer->GetGroup() && pInstance->m_creatorGroup == pPlayer->GetGroup()->GetID()) || pPlayer->GetLowGUID() == pInstance->m_creatorGuid )
//		return true;

	return false;
}

MapMgr * InstanceMgr::CreateBattlegroundInstance(uint32 mapid, uint32 instanceid /* = 0 */ )
{
	// shouldn't happen
	if( mapid >= NUM_MAPS )
		return NULL;

	if(!m_maps[mapid])
	{
		_CreateMap(mapid);
		if(!m_maps[mapid])
			return NULL;
	}

	if( instanceid == 0 )
		instanceid = GenerateInstanceID();

	MapMgr * ret = new MapMgr(m_maps[mapid],mapid, instanceid);
	Instance * pInstance = new Instance();
	pInstance->m_creation = UNIXTIME;
	pInstance->m_creatorGroup = 0;
	pInstance->m_creatorGuid = 0;
	pInstance->m_difficulty = 0;
	pInstance->m_expiration = 0;
	pInstance->m_instanceId = ret->GetInstanceID();
	pInstance->m_isBattleground = true;
	pInstance->m_mapId = mapid;
	pInstance->m_mapInfo = WorldMapInfoStorage.LookupEntry( mapid );
	pInstance->m_mapMgr = ret;
	ret->m_pInstance = pInstance;
	if( m_instances[mapid] == NULL )
		m_instances[mapid] = new InstanceMap;

	m_instances[mapid]->insert( make_pair( pInstance->m_instanceId, pInstance ) );
	ret->Init();
	return ret;
}

void InstanceMgr::DeleteBattlegroundInstance(uint32 mapid, uint32 instanceid)
{
	InstanceMap::iterator itr = m_instances[mapid]->find( instanceid );
	if( itr == m_instances[mapid]->end() )
	{
		printf("Could not delete battleground instance!\n");
		return;
	}

	m_instances[mapid]->erase( itr );
}

FormationMgr::FormationMgr()
{
	QueryResult * res = WorldDatabase.Query("SELECT * FROM creature_formations");
	if(res)
	{
		Formation *f ;
		do
		{
			f = new Formation;
			f->fol = res->Fetch()[1].GetUInt32();
			f->ang = res->Fetch()[2].GetFloat();
			f->dist = res->Fetch()[3].GetFloat();
			m_formations[res->Fetch()[0].GetUInt32()] = f;
		} while(res->NextRow());
		delete res;
	}
}

FormationMgr::~FormationMgr()
{
	FormationMap::iterator itr;
	for(itr = m_formations.begin(); itr != m_formations.end(); ++itr)
		delete itr->second;
}
