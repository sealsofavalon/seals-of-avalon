#include "StdAfx.h"
#include "../../SDBase/Protocol/S2C_Item.h"
#include "../../SDBase/Protocol/C2S.h"
#include "../../SDBase/Protocol/Opcodes.h"
#include "../../SDBase/Protocol/S2C_Chat.h"
#include "../../SDBase/Protocol/S2C_Move.h"
#include "../../SDBase/Protocol/C2S_Move.h"
#include "../../SDBase/Protocol/S2C_Group.h"
#include "../Common/Protocol/GS2DB.h"
#include "../Common/Protocol/GS2GT.h"
#include "../Net/DSSocket.h"
#include "SunyouRaid.h"
#include "LfgMgr.h"
#include "DynamicObject.h"

#define CLUSTERING

#define WORLDSOCKET_TIMEOUT		 80000000
#define PLAYER_LOGOUT_DELAY (20*1000)		/* 20 seconds should be more than enough to gank ya. */

OpcodeHandler WorldPacketHandlers[MSG_C2S::NUM_MSG_TYPES];

WorldSession::WorldSession(uint32 AccountID, uint32 SessionID, uint64 RoleID, string RoleName, const string& AccountName, CGTSocket* m_GT, string ClientIP) : _player(0), _GTSocket(m_GT),
_accountId(AccountID),_SessionID(SessionID), _roleID(RoleID), _roleName(RoleName), _ClientIP(ClientIP),
_logoutTime(0), permissions(NULL), permissioncount(0), _loggingOut(false), instanceId(0), _accountName( AccountName ),
m_disableMessage( false )
{
	//memset(&m_Movement, 0, sizeof(m_Movement));
	m_fastpingcnt = 0;
	m_currMsTime = getMSTime();
	bDeleted = false;
	m_bIsWLevelSet = false;
	floodLines = 0;
	floodTime = UNIXTIME;
	_updatecount = 0;
	m_moveDelayTime=0;
	m_clientTimeDelay =0;
	m_loggingInPlayer=NULL;
	language=0;
	m_muted = 0;
	m_ChenmiStat = 0;
	_side = -1;
	m_Movement = new MSG_C2S::stMove_OP;
	//m_Movement->move.FallTime = 0;

	for(uint32 x=0;x<8;x++)
		sAccountData[x].data=NULL;

	m_lastPing = (uint32)UNIXTIME;
}

WorldSession::~WorldSession()
{
	delete m_Movement;

	if(HasGMPermissions())
		sWorld.gmList.erase(this);

	if(_player)
	{
		MyLog::log->error("warning: logged out player in worldsession destructor");
		LogoutPlayer(true);
	}

	if(permissions)
		delete [] permissions;

	for(uint32 x=0;x<8;x++)
	{
		if(sAccountData[x].data)
			delete [] sAccountData[x].data;
	}

	if(m_loggingInPlayer)
		m_loggingInPlayer->SetSession(NULL);

}

void WorldSession::SendPacket(PakHead& pak, uint32 delay)
{
	if( _GTSocket && !m_disableMessage && _GTSocket->is_connected())
	{
		pak.nTransID = _SessionID;
		_GTSocket->PostSend(pak, delay);
	}
}

void WorldSession::SendData(const char* pData, ui32 len)
{
	if( _GTSocket && !m_disableMessage && _GTSocket->is_connected())
	{
		*(uint32*)(pData+4) = _SessionID;
		_GTSocket->PostSend(pData, len);
	}
}
void WorldSession::OutPacket(uint16 opcode){}

void WorldSession::HandleMsg(CPacketUsn& pakTool)
{
	if( this != g_activity_mgr->tool_session )
	{
		if(!_player)
		{
			MyLog::log->error( "recv message after player is logout");
			return;
		}
	}
	OpcodeHandler * Handler;
	if( pakTool.GetProNo() >= MSG_C2S::NUM_MSG_TYPES )
	{
		//assert( pakTool.GetProNo() < MSG_C2S::NUM_MSG_TYPES );
		MyLog::log->error( "recv invalid message NO:[%d], user:[%s]", pakTool.GetProNo(), GetAccountNameS() );
		return;
	}

	Handler = &WorldPacketHandlers[pakTool.GetProNo()];
	// Valid Packet :>
	if(Handler->handler == 0 )
	{
		MyLog::log->error("[Session] Received unhandled packet with opcode %s (0x%.4X)",
			LookupName(pakTool.GetProNo(), g_worldOpcodeNames), pakTool.GetProNo());
	}
	else
	{
		try
		{
			(this->*Handler->handler)(pakTool);
		}
		catch( const packet_exception& e )
		{
			MSG_GS2GT::stExitAck Msg;
			Msg.nError = 100 + net_global::BAN_REASON_HACK_PACKET;
			this->SendPacket( Msg );

			MyLog::log->error( "Recv hack packet from account:[%d], kick out and ban this IP for 120 seconds! Exception:[%s]", this->_accountId, e.what() );
		}
#ifdef NDEBUG
		catch( ... )
		{
			MSG_GS2GT::stExitAck Msg;
			Msg.nError = 100 + net_global::BAN_REASON_HACK_PACKET;
			this->SendPacket( Msg );

			MyLog::log->error( "Recv hack packet from account:[%d] and cause server crash, kick out and ban this IP for 120 seconds!", this->_accountId );
		}
#endif
	}
}
int WorldSession::Update(uint32 InstanceID)
{
	m_currMsTime = getMSTime();



	//OpcodeHandler * Handler;

	if(InstanceID != instanceId)
	{
		// We're being updated by the wrong thread.
		// "Remove us!" - 2
		return 2;
	}

	// Socket disconnection.
	if(!_GTSocket)
	{
		// Check if the player is in the process of being moved. We can't delete him
		// if we are.
		if(_player && _player->m_beingPushed)
		{
			// Abort..
			return 0;
		}

		if(!_logoutTime)
			_logoutTime = m_currMsTime + PLAYER_LOGOUT_DELAY;

/*
				if(_player && _player->DuelingWith)
					_player->EndDuel(DUEL_WINNER_RETREAT);

				bGetUniqueIDForLua();
				LogoutPlayer(true);
				// 1 - Delete session completely.
				return 1;*/

	}

	if(InstanceID != instanceId)
	{
		// If we hit this -> means a packet has changed our map.
		return 2;
	}
/*
	if( _logoutTime && (m_currMsTime >= _logoutTime) && instanceId == InstanceID)
	{
		// Check if the player is in the process of being moved. We can't delete him
		// if we are.
		if(_player && _player->m_beingPushed)
		{
			// Abort..
			return 0;
		}

		if( _GTSocket == NULL )
		{
			bGetUniqueIDForLua();
			Disconnect();
			return 1;
		}
		else
			Disconnect();
	}

	if(m_fastpingcnt > 10)
	{
		// ping timeout!
		if(_GTSocket)
			Disconnect();
		// Check if the player is in the process of being moved. We can't delete him
		// if we are.
		if(_player && _player->m_beingPushed)
		{
			// Abort..
			return 0;
		}
	}
*/
	return 0;
}


void WorldSession::LogoutPlayer(bool Save, bool disconnectgt)
{
	if( _loggingOut )
		return;

	_loggingOut = true;

	Player* pPlayer = GetPlayer();

	

	if( _player != NULL )
	{
		if( _player->m_sunyou_instance && _player->m_sunyou_instance->IsValid() )
		{
			if( _player->m_sunyou_instance->GetCategory() == INSTANCE_CATEGORY_TEAM_ARENA
				|| _player->m_sunyou_instance->GetCategory() == INSTANCE_CATEGORY_FAIRGROUND )
			{
				_player->m_escape_mode = true;
				_player->EjectFromInstance();
			}
			else
			{
				_player->m_before_delete = true;
				_player->m_sunyou_instance->OnPlayerLeave( _player );
			}
		}


		pPlayer->RemoveAllChannelAuras();


		std::set<DynamicObject*> sd( _player->m_setDynamicObjs );
		set<DynamicObject*>::iterator itr = sd.begin();
		for(; itr != sd.end(); ++itr)
		{
			DynamicObject* obj = *itr;
			obj->Remove();
		}
		if( _player->GetGroup() )
		{
			SubGroup* sg = _player->GetGroup()->GetSubGroup( _player->GetSubGroup() );
			if(sg)
				sAnimalMgr.OnAniamlRelation( sg );
		}
		sHookInterface.OnLogout( pPlayer );

		if(_player->m_bRecvLoadingOK)
		{
			if( _player->DuelingWith )
				_player->EndDuel( DUEL_WINNER_RETREAT );

			if( _player->m_currentLoot && _player->IsInWorld() )
			{
				Object* obj = _player->GetMapMgr()->_GetObject( _player->m_currentLoot );
				if( obj != NULL )
				{
					switch( obj->GetTypeId() )
					{
					case TYPEID_UNIT:
						static_cast< Creature* >( obj )->loot.looters.erase( _player->GetLowGUID() );
						break;
					case TYPEID_GAMEOBJECT:
						static_cast< GameObject* >( obj )->loot.looters.erase( _player->GetLowGUID() );
						break;
					}
				}
			}

			// part channels
			_player->CleanupChannels();

			if( _player->m_CurrentTransporter != NULL )
				_player->m_CurrentTransporter->RemovePlayer( _player );

			// cancel current spell
			if( _player->m_currentSpell != NULL )
				_player->m_currentSpell->cancel();

			_player->Social_TellFriendsOffline();

			if( _player->GetTeam() == 1 )
			{
				if( sWorld.HordePlayers )
					sWorld.HordePlayers--;
			}
			else
			{
				if( sWorld.AlliancePlayers )
					sWorld.AlliancePlayers--;
			}

			//Duel Cancel on Leave
			if( _player->DuelingWith != NULL )
				_player->EndDuel( DUEL_WINNER_RETREAT );

			//Issue a message telling all guild members that this player signed off
			if( _player->IsInGuild() )
			{
				Guild* pGuild = _player->m_playerInfo->guild;
				if( pGuild != NULL )
					pGuild->LogGuildEvent( GUILD_EVENT_HASGONEOFFLINE, 1, _player->GetName() );
			}

			_player->GetItemInterface()->EmptyBuyBack();

			sLfgMgr.RemovePlayerFromLfgQueues( _player );

			// Save HP/Mana
			_player->load_health = _player->GetUInt32Value( UNIT_FIELD_HEALTH );
			_player->load_mana = _player->GetUInt32Value( UNIT_FIELD_POWER1 );

		}


		objmgr.RemovePlayer( _player );
		_player->ok_to_remove = true;

		if(_player->m_bRecvLoadingOK)
		{
			if( _player->GetSummon() != NULL )
				_player->GetSummon()->Remove( false, true, false );

			//_player->SaveAuras();
			_player->RemoveAllExtraAuars();
			_player->RemoveAllExtraSpells();
			_player->RemoveItemsFromWorld();
				if( Save )
				_player->SaveToDB(false);

			_player->RemoveAllAuras(false);

			uint64 guid;
			guid  = _player->GetUInt64Value(PLAYER_HEAD);
			if( guid )
			{
				Player* player = objmgr.GetPlayer(guid);
				if(player)
				{
					MyLog::log->debug("player[%s] disride for logout", player->GetName());
					player->SetUInt64Value(PLAYER_FOOT, 0);
				}
			}
			else
			{
				guid = _player->GetUInt64Value(PLAYER_FOOT);
				if(guid)
				{
					Player* player = objmgr.GetPlayer(guid);
					if(player)
					{
						MyLog::log->debug("player[%s] disride for logout", player->GetName());
						player->SetUInt64Value(PLAYER_HEAD, 0);
					}
				}
			}


			if( _player->IsInWorld() )
			{
				_player->RemoveFromWorld();
			}
			else
			{
				if( _player->m_playerInfo&&_player->m_playerInfo->m_Group != NULL )
				{
					MSG_S2C::stPartyMemberStat Msg;
					//_player->GetGroup()->UpdateGroupListForPlayer(_player);	
					Msg.mask = GROUP_UPDATE_FLAG_NONE;
					Msg.guid = _player->GetGUID();
					_player->m_playerInfo->m_Group->SendPacketToAllButOne(Msg, _player);

				}
			}

			_player->m_playerInfo->m_loggedInPlayer = NULL;


			if (_player->GetGroup())
			{
				MSG_S2C::stPartyMemberStat Msg;
				//_player->GetGroup()->UpdateGroupListForPlayer(_player);	
				Msg.mask = GROUP_UPDATE_FLAG_NONE;
				Msg.guid = _player->GetGUID();
				_player->GetGroup()->SendPacketToAllButOne(Msg, _player);

				if ((_player->GetGroup()->GetLeader() &&_player->GetGroup()->GetLeader()->guid == Msg.guid) || _player->GetGroup()->GetLeader() ==NULL)
				{
					Player* p = _player->GetGroup()->FindFirstPlayer();
					if (p)
					{
						_player->GetGroup()->SetLeader(p, false); 

					}
				}

			}
				
			// Remove the "player locked" flag, to allow movement on next login
			GetPlayer()->RemoveFlag( UNIT_FIELD_FLAGS, UNIT_FLAG_LOCK_PLAYER );
		}


		// Save Honor Points
		//_player->SaveHonorFields();

		/*
		// Update any dirty account_data fields.
		bool dirty = false;
		if( sWorld.m_useAccountData )
		{
			std::stringstream ss;
			ss << "UPDATE account_data SET ";
			for(uint32 ui=0;ui<8;ui++)
			{
				if(sAccountData[ui].bIsDirty)
				{
					if(dirty)
						ss <<",";
					ss << "uiconfig"<< ui <<"=\"";
					if(sAccountData[ui].data)
					{
						CharacterDatabase.EscapeLongString(sAccountData[ui].data, sAccountData[ui].sz, ss);
						//ss.write(sAccountData[ui].data,sAccountData[ui].sz);
					}
					ss << "\"";
					dirty = true;
					sAccountData[ui].bIsDirty = false;
				}
			}
			if(dirty)
			{
				ss	<<" WHERE acct="<< _accountId <<";";
				CharacterDatabase.ExecuteNA(ss.str().c_str());
			}
		}
		*/

		Player* plr = _player;
		_player = NULL;
		delete plr;
		MyLog::log->debug( "SESSION: Sent SMSG_LOGOUT_COMPLETE Message" );
	}
	_loggingOut = false;

	SetLogoutTimer(0);

	if(disconnectgt)
	{
		MSG_GS2GT::stExitAck Msg;
		Msg.nError = 0;
		this->SendPacket( Msg );
	}
}

void WorldSession::SendBuyFailed(uint64 guid, uint32 itemid, uint8 error)
{
	MSG_S2C::stItem_Buy_Failure Msg;
	Msg.vendorguid	= guid;
	Msg.itementry	= itemid;
	Msg.error		= error;
	SendPacket(Msg);
}

void WorldSession::SendSellItem(uint64 vendorguid, uint64 itemid, uint8 error)
{
	MSG_S2C::stItem_Sell Msg;
	Msg.vendorguid  = vendorguid;
	Msg.itemguid	= itemid;
	Msg.error	= error;
	SendPacket(Msg);
}

void WorldSession::LoadSecurity(std::string securitystring)
{
	std::list<char> tmp;
	
	for(uint32 i = 0; i < securitystring.length(); ++i)
	{
		char c = securitystring.at(i);

		if (c == '1' || c == '2' || c == '3' || c == '4')
		{
			tmp.push_back(c);
			
		}
		c = tolower(c);
		if(c == 'a')
		{
			tmp.push_back('4');
			
		}
	}

	permissions = new char[tmp.size()+1];
	memset(permissions, 0, tmp.size()+1);
	permissioncount = (uint32)tmp.size();
	int k = 0;
	for(std::list<char>::iterator itr = tmp.begin(); itr != tmp.end(); ++itr)
		permissions[k++] = (*itr);

	if(permissions[tmp.size()] != 0)
		permissions[tmp.size()] = 0;

	MyLog::log->debug("Loaded permissions for %u. (%u) : [%s]", this->GetAccountId(), permissioncount, securitystring.c_str());
}

void WorldSession::SetSecurity(std::string securitystring)
{
	delete [] permissions;
	LoadSecurity(securitystring);

	// update db
	sWorld.ExecuteSqlToDBServer("UPDATE accounts SET gm=\"%s\" WHERE acct=%u", CharacterDatabase.EscapeString(string(permissions)).c_str(), _accountId);
}

bool WorldSession::CanUseCommand(char cmdstr)
{
	if(permissioncount == 0)
		return false;
	if(cmdstr == 0)
		return false;
	if (cmdstr != '1' && cmdstr != '2' && cmdstr != '3'&& cmdstr != '4')
	{
		return false ;
	}
	if (permissions[0] - '1' >=  cmdstr - '1')
	{
		return  true ;
	}
	return false ;

	//if(permissions[0] == 'a'/* && cmdstr != 'z'*/)   // all
	//	return true;

	//for(int i = 0; i < permissioncount; ++i)
	//	if(permissions[i] == cmdstr)
	//		return true;

	//return false;
}

void WorldSession::SendNotification(const char *message, ...)
{
	if( !message ) return;
	va_list ap;
	va_start(ap, message);
	char msg1[1024];
	vsnprintf(msg1,1024, message,ap);
	MSG_S2C::stNotify_Msg Msg;
	Msg.notify = msg1;
	SendPacket( Msg );
}

void WorldSession::SendSystemNotifyEnum( uint16 e )
{
	MSG_S2C::stSystemNotifyEnum msg;
	msg.e = e;
	SendPacket( msg );
}

MovementInfo* WorldSession::GetMovementInfo()
{
	return &m_Movement->move;
}
void WorldSession::InitPacketHandlerTable()
{
	// Nullify Everything, default to STATUS_LOGGEDIN
	for(uint32 i = 0; i < NUM_MSG_TYPES; ++i)
	{
		WorldPacketHandlers[i].status = STATUS_LOGGEDIN;
		WorldPacketHandlers[i].handler = 0;
	}

	WorldPacketHandlers[MSG_C2S::CMSG_LOADING_OK].handler							= &WorldSession::HandlePlayerLoadingOKOpcode;
	// Queries
	WorldPacketHandlers[MSG_C2S::QUERY_CORPSE].handler							   = &WorldSession::HandleCorpseQueryOpcode;
	WorldPacketHandlers[MSG_C2S::QUERY_NAME].handler								= &WorldSession::HandleNameQueryOpcode;
	WorldPacketHandlers[MSG_C2S::QUERY_TIME].handler								= &WorldSession::HandleQueryTimeOpcode;
	WorldPacketHandlers[MSG_C2S::QUERY_CREATURE].handler							= &WorldSession::HandleCreatureQueryOpcode;
	WorldPacketHandlers[MSG_C2S::QUERY_GAMEOBJECT].handler						  = &WorldSession::HandleGameObjectQueryOpcode;
	WorldPacketHandlers[MSG_C2S::QUERY_PAGE_TEXT].handler						   = &WorldSession::HandlePageTextQueryOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_NAME_QUERY].handler						   = &WorldSession::HandleItemNameQueryOpcode;

	// Movement
	WorldPacketHandlers[MSG_C2S::MOVE_OP].handler									= &WorldSession::HandleMovementOpcodes;
	WorldPacketHandlers[MSG_C2S::MOVE_ACK].handler									= &WorldSession::HandleAcknowledgementOpcodes;
	WorldPacketHandlers[MSG_C2S::MOVE_WORLDPORT_ACK].handler						 = &WorldSession::HandleMoveWorldportAckOpcode;

	WorldPacketHandlers[MSG_C2S::MOVE_TIME_SKIPPED].handler						 = &WorldSession::HandleMoveTimeSkippedOpcode;
	WorldPacketHandlers[MSG_C2S::MOVE_NOT_ACTIVE_MOVER].handler					 = &WorldSession::HandleMoveNotActiveMoverOpcode;
	WorldPacketHandlers[MSG_C2S::MOVE_SET_ACTIVE_MOVER].handler						  = &WorldSession::HandleSetActiveMoverOpcode;
	// ACK
	WorldPacketHandlers[MSG_C2S::MOVE_TELEPORT_ACK].handler						  = &WorldSession::HandleMoveTeleportAckOpcode;

	WorldPacketHandlers[MSG_C2S::MOUNT_INVITE].handler =								&WorldSession::HandleMountInviteOpcode;
	WorldPacketHandlers[MSG_C2S::MOUNT_ACCEPT].handler =								&WorldSession::HandleMountAcceptOpcode;
	WorldPacketHandlers[MSG_C2S::MOUNT_DECLINE].handler =								&WorldSession::HandleMountDeclineOpcode;
	WorldPacketHandlers[MSG_C2S::MOUNT_DISBAND].handler =								&WorldSession::HandleMountDisbandOpcode;

	// Action Buttons
	WorldPacketHandlers[MSG_C2S::CMSG_SET_ACTION_BUTTON].handler						 = &WorldSession::HandleSetActionButtonOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_REPOP_REQUEST].handler							 = &WorldSession::HandleRepopRequestOpcode;

	// Loot
	WorldPacketHandlers[MSG_C2S::LOOT_AUTOSTORE_ITEM].handler					   = &WorldSession::HandleAutostoreLootItemOpcode;
	WorldPacketHandlers[MSG_C2S::LOOT_MONEY].handler								= &WorldSession::HandleLootMoneyOpcode;
	WorldPacketHandlers[MSG_C2S::LOOT].handler									  = &WorldSession::HandleLootOpcode;
	WorldPacketHandlers[MSG_C2S::LOOT_RELEASE].handler							  = &WorldSession::HandleLootReleaseOpcode;
	WorldPacketHandlers[MSG_C2S::LOOT_ROLL].handler								 = &WorldSession::HandleLootRollOpcode;
	WorldPacketHandlers[MSG_C2S::LOOT_MASTER_GIVE].handler						  = &WorldSession::HandleLootMasterGiveOpcode;

	// Player Interaction
	WorldPacketHandlers[MSG_C2S::MSG_WHO].handler									   = &WorldSession::HandleWhoOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_LOGOUT_REQUEST].handler							= &WorldSession::HandleLogoutRequestOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_PLAYER_LOGOUT].handler							 = &WorldSession::HandlePlayerLogoutOpcode;
	WorldPacketHandlers[MSG_C2S::LOGOUT_REQ].handler							 = &WorldSession::HandlePlayerLogoutOpcode;
	WorldPacketHandlers[MSG_C2S::EXIT_REQ].handler							 = &WorldSession::HandlePlayerExitOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_LOGOUT_CANCEL].handler							 = &WorldSession::HandleLogoutCancelOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_ZONEUPDATE].handler								= &WorldSession::HandleZoneUpdateOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_SET_TARGET_OBSOLETE].handler					   = &WorldSession::HandleSetTargetOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_SET_SELECTION].handler							 = &WorldSession::HandleSetSelectionOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_STANDSTATECHANGE].handler						  = &WorldSession::HandleStandStateChangeOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_DISMOUNT].handler								= &WorldSession::HandleDismountOpcode;

	// Friends
	WorldPacketHandlers[MSG_C2S::FRIEND_LIST].handler							   = &WorldSession::HandleFriendListOpcode;
	WorldPacketHandlers[MSG_C2S::FRIEND_ADD].handler								= &WorldSession::HandleAddFriendOpcode;
	WorldPacketHandlers[MSG_C2S::FRIEND_DEL].handler								= &WorldSession::HandleDelFriendOpcode;
	WorldPacketHandlers[MSG_C2S::FRIEND_ADD_IGNORE].handler								= &WorldSession::HandleAddIgnoreOpcode;
	WorldPacketHandlers[MSG_C2S::FRIEND_DEL_IGNORE].handler								= &WorldSession::HandleDelIgnoreOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_BUG].handler									   = &WorldSession::HandleBugOpcode;
	WorldPacketHandlers[MSG_C2S::FRIEND_SET_NOTE].handler							= &WorldSession::HandleSetFriendNote;

	// Areatrigger
	WorldPacketHandlers[MSG_C2S::AREA_TRIGGER].handler							   = &WorldSession::HandleAreaTriggerOpcode;

	// Account Data
// 	WorldPacketHandlers[MSG_C2S::CMSG_UPDATE_ACCOUNT_DATA].handler					   = &WorldSession::HandleUpdateAccountData;
// 	WorldPacketHandlers[MSG_C2S::CMSG_REQUEST_ACCOUNT_DATA].handler					  = &WorldSession::HandleRequestAccountData;
	WorldPacketHandlers[MSG_C2S::CMSG_SET_WATCHED_FACTION_INDEX].handler				 = &WorldSession::HandleSetWatchedFactionIndexOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_TOGGLE_PVP].handler								= &WorldSession::HandleTogglePVPOpcode;

	// Player Interaction
	WorldPacketHandlers[MSG_C2S::GAMEOBJ_USE].handler							   = &WorldSession::HandleGameObjectUse;
	WorldPacketHandlers[MSG_C2S::CMSG_PLAYED_TIME].handler							   = &WorldSession::HandlePlayedTimeOpcode;
	WorldPacketHandlers[MSG_C2S::SET_SHEATHED].handler							   = &WorldSession::HandleSetSheathedOpcode;
	WorldPacketHandlers[MSG_C2S::CHAT_MESSAGE].handler							   = &WorldSession::HandleMessagechatOpcode;
	WorldPacketHandlers[MSG_C2S::CHAT_TEXT_EMOTE].handler								= &WorldSession::HandleTextEmoteOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_INSPECT].handler								= &WorldSession::HandleInspectOpcode;

	// Channels
	WorldPacketHandlers[MSG_C2S::CHANNEL_JOIN].handler							  = &WorldSession::HandleChannelJoin;
	WorldPacketHandlers[MSG_C2S::CHANNEL_LEAVE].handler							 = &WorldSession::HandleChannelLeave;
	WorldPacketHandlers[MSG_C2S::CHANNEL_LIST].handler							  = &WorldSession::HandleChannelList;
	WorldPacketHandlers[MSG_C2S::CHANNEL_PASSWORD].handler						  = &WorldSession::HandleChannelPassword;
	WorldPacketHandlers[MSG_C2S::CHANNEL_SET_OWNER].handler						 = &WorldSession::HandleChannelSetOwner;
	WorldPacketHandlers[MSG_C2S::CHANNEL_OWNER].handler							 = &WorldSession::HandleChannelOwner;
	WorldPacketHandlers[MSG_C2S::CHANNEL_MODERATOR].handler						 = &WorldSession::HandleChannelModerator;
	WorldPacketHandlers[MSG_C2S::CHANNEL_UNMODERATOR].handler					   = &WorldSession::HandleChannelUnmoderator;
	WorldPacketHandlers[MSG_C2S::CHANNEL_MUTE].handler							  = &WorldSession::HandleChannelMute;
	WorldPacketHandlers[MSG_C2S::CHANNEL_UNMUTE].handler							= &WorldSession::HandleChannelUnmute;
	WorldPacketHandlers[MSG_C2S::CHANNEL_INVITE].handler							= &WorldSession::HandleChannelInvite;
	WorldPacketHandlers[MSG_C2S::CHANNEL_KICK].handler							  = &WorldSession::HandleChannelKick;
	WorldPacketHandlers[MSG_C2S::CHANNEL_BAN].handler							   = &WorldSession::HandleChannelBan;
	WorldPacketHandlers[MSG_C2S::CHANNEL_UNBAN].handler							 = &WorldSession::HandleChannelUnban;
	WorldPacketHandlers[MSG_C2S::CHANNEL_ANNOUNCEMENTS].handler					 = &WorldSession::HandleChannelAnnounce;
	WorldPacketHandlers[MSG_C2S::CHANNEL_MODERATE].handler						  = &WorldSession::HandleChannelModerate;
	WorldPacketHandlers[MSG_C2S::CHANNEL_NUM_MEMBERS_QUERY].handler					= &WorldSession::HandleChannelNumMembersQuery;
	WorldPacketHandlers[MSG_C2S::CHANNEL_GET_ROSTER_INFO].handler					= &WorldSession::HandleChannelRosterQuery;

	// Groups / Raids
	WorldPacketHandlers[MSG_C2S::GROUP_INVITE].handler							  = &WorldSession::HandleGroupInviteOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_CANCEL].handler							  = &WorldSession::HandleGroupCancelOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_ACCEPT].handler							  = &WorldSession::HandleGroupAcceptOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_DECLINE].handler							 = &WorldSession::HandleGroupDeclineOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_UNINVITE_NAME].handler							= &WorldSession::HandleGroupUninviteOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_UNINVITE_GUID].handler					   = &WorldSession::HandleGroupUninviteGuildOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_SET_LEADER].handler						  = &WorldSession::HandleGroupSetLeaderOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_DISBAND].handler							 = &WorldSession::HandleGroupDisbandOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_LOOT_METHOD].handler							   = &WorldSession::HandleLootMethodOpcode;
	WorldPacketHandlers[MSG_C2S::MINIMAP_PING].handler							   = &WorldSession::HandleMinimapPingOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_RAID_CONVERT].handler						= &WorldSession::HandleConvertGroupToRaidOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_CHANGE_SUB_GROUP].handler					= &WorldSession::HandleGroupChangeSubGroup;
	WorldPacketHandlers[MSG_C2S::GROUP_SWEP_MEMBER].handler							= &WorldSession::HandleGroupSwepMember;
	WorldPacketHandlers[MSG_C2S::GROUP_SWEP_MEMBER_NAME].handler                    = &WorldSession::HandleGroupSwepMemberName;
	WorldPacketHandlers[MSG_C2S::GROUP_ASSISTANT_LEADER].handler					= &WorldSession::HandleGroupAssistantLeader;
	WorldPacketHandlers[MSG_C2S::RAID_REQUEST_INFO].handler						    = &WorldSession::HandleRequestRaidInfoOpcode;
	WorldPacketHandlers[MSG_C2S::RAID_READYCHECK].handler						    = &WorldSession::HandleReadyCheckOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_SET_PLAYER_ICON].handler					    = &WorldSession::HandleSetPlayerIconOpcode;
	WorldPacketHandlers[MSG_C2S::PARTY_MEMBER_STAT].handler				            = &WorldSession::HandlePartyMemberStatsOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_PROMOTE].handler								= &WorldSession::HandleGroupPromote;
	WorldPacketHandlers[MSG_C2S::GROUP_APPLYFORJOINING].handler                     = &WorldSession::HandleGroupApplyForJoining;
	WorldPacketHandlers[MSG_C2S::GROUP_APPLYFORJOINING_ACK].handler                 = &WorldSession::HandleGroupApplyForJoiningAckOpcode;
	WorldPacketHandlers[MSG_C2S::GRPOP_LIST_REQ].handler                            = &WorldSession::HandleGroupListReqOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_MODIFY_TITLE_REQ].handler                    = &WorldSession::HandleGroupModifyTitleReqOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_MODIFY_CAN_APPLYFORJOINING].handler          = &WorldSession::HandleGroupModifyCanApplyForJoiningOpcode;
	WorldPacketHandlers[MSG_C2S::GROUP_APPLYFORJOINING_STATE_REQ].handler			= &WorldSession::HandleGroupApplyForJoiningStateOpcode;
	// LFG System
// 	WorldPacketHandlers[MSG_C2S::CMSG_SET_LOOKING_FOR_GROUP_COMMENT].handler				= &WorldSession::HandleSetLookingForGroupComment;
// 	WorldPacketHandlers[MSG_C2S::MSG_LOOKING_FOR_GROUP].handler							= &WorldSession::HandleMsgLookingForGroup;
// 	WorldPacketHandlers[MSG_C2S::CMSG_SET_LOOKING_FOR_GROUP].handler						= &WorldSession::HandleSetLookingForGroup;
// 	WorldPacketHandlers[MSG_C2S::CMSG_SET_LOOKING_FOR_MORE].handler						= &WorldSession::HandleSetLookingForMore;
// 	WorldPacketHandlers[MSG_C2S::CMSG_ENABLE_AUTOJOIN].handler							= &WorldSession::HandleEnableAutoJoin;
// 	WorldPacketHandlers[MSG_C2S::CMSG_DISABLE_AUTOJOIN].handler							= &WorldSession::HandleDisableAutoJoin;
// 	WorldPacketHandlers[MSG_C2S::CMSG_ENABLE_AUTOADD_MEMBERS].handler					= &WorldSession::HandleEnableAutoAddMembers;
// 	WorldPacketHandlers[MSG_C2S::CMSG_DISABLE_AUTOADD_MEMBERS].handler					= &WorldSession::HandleDisableAutoAddMembers;
// 	WorldPacketHandlers[MSG_C2S::CMSG_CLEAR_LOOKING_FOR_GROUP_STATE].handler				= &WorldSession::HandleLfgClear;

	// Taxi / NPC Interaction
	WorldPacketHandlers[MSG_C2S::CMSG_TAXINODE_STATUS_QUERY].handler					 = &WorldSession::HandleTaxiNodeStatusQueryOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_TAXIQUERYAVAILABLENODES].handler				   = &WorldSession::HandleTaxiQueryAvaibleNodesOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_ACTIVATETAXI].handler							  = &WorldSession::HandleActivateTaxiOpcode;
	WorldPacketHandlers[MSG_C2S::NPC_TABARDVENDOR_ACTIVATE].handler					  = &WorldSession::HandleTabardVendorActivateOpcode;
	WorldPacketHandlers[MSG_C2S::NPC_BANKER_ACTIVATE].handler						   = &WorldSession::HandleBankerActivateOpcode;
	WorldPacketHandlers[MSG_C2S::BUY_BANK_SLOT].handler							 = &WorldSession::HandleBuyBankSlotOpcode;
	WorldPacketHandlers[MSG_C2S::NPC_TRAINER_LIST].handler							  = &WorldSession::HandleTrainerListOpcode;
	WorldPacketHandlers[MSG_C2S::NPC_TRAINER_BUY_SPELL].handler						 = &WorldSession::HandleTrainerBuySpellOpcode;
	WorldPacketHandlers[MSG_C2S::NPC_PETITION_SHOWLIST].handler						 = &WorldSession::HandleCharterShowListOpcode;
	WorldPacketHandlers[MSG_C2S::AUCTION_HELLO].handler							  = &WorldSession::HandleAuctionHelloOpcode;
	WorldPacketHandlers[MSG_C2S::NPC_GOSSIP_HELLO].handler							  = &WorldSession::HandleGossipHelloOpcode;
	WorldPacketHandlers[MSG_C2S::NPC_GOSSIP_SELECT_OPTION].handler					  = &WorldSession::HandleGossipSelectOptionOpcode;
	WorldPacketHandlers[MSG_C2S::NPC_SPIRIT_HEALER_ACTIVATE].handler					= &WorldSession::HandleSpiritHealerActivateOpcode;
	WorldPacketHandlers[MSG_C2S::NPC_TEXT_QUERY].handler							= &WorldSession::HandleNpcTextQueryOpcode;
	WorldPacketHandlers[MSG_C2S::NPC_BINDER_ACTIVATE].handler						   = &WorldSession::HandleBinderActivateOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_ACTIVATE_MULTIPLE_TAXI].handler					= &WorldSession::HandleMultipleActivateTaxiOpcode;

	// Item / Vendors
	WorldPacketHandlers[MSG_C2S::ITEM_SHOW_SHIZHUANG].handler					 = &WorldSession::HandleShowShizhuang;
	WorldPacketHandlers[MSG_C2S::ITEM_SWAP_INV].handler							 = &WorldSession::HandleSwapInvItemOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_SWAP].handler								 = &WorldSession::HandleSwapItemOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_DESTROY].handler							   = &WorldSession::HandleDestroyItemOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_AUTOEQUIP].handler							= &WorldSession::HandleAutoEquipItemOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_QUERY_SINGLE].handler						 = &WorldSession::HandleItemQuerySingleOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_SELL].handler								 = &WorldSession::HandleSellItemOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_BUY_IN_SLOT].handler						  = &WorldSession::HandleBuyItemInSlotOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_BUY].handler								  = &WorldSession::HandleBuyItemOpcode;
	WorldPacketHandlers[MSG_C2S::LIST_INVENTORY].handler							= &WorldSession::HandleListInventoryOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_AUTOSTORE_BAG].handler						= &WorldSession::HandleAutoStoreBagItemOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_BUYBACK].handler							  = &WorldSession::HandleBuyBackOpcode;
	//WorldPacketHandlers[MSG_C2S::CMSG_SET_AMMO].handler								  = &WorldSession::HandleAmmoSetOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_SPLIT].handler								= &WorldSession::HandleSplitOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_READ].handler								 = &WorldSession::HandleReadItemOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_REPAIR].handler							   = &WorldSession::HandleRepairItemOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_AUTOBANK].handler							 = &WorldSession::HandleAutoBankItemOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_AUTOSTORE_BANK].handler					   = &WorldSession::HandleAutoStoreBankItemOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_CANCEL_TEMPORARY_ENCHANTMENT].handler			  = &WorldSession::HandleCancelTemporaryEnchantmentOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_SOCKET_GEMS].handler								= &WorldSession::HandleInsertGemOpcode;
	WorldPacketHandlers[MSG_C2S::ITEM_WRAP].handler									= &WorldSession::HandleWrapItemOpcode;

	// Spell System / Talent System
	WorldPacketHandlers[MSG_C2S::ITEM_USE].handler								  = &WorldSession::HandleUseItemOpcode;
	WorldPacketHandlers[MSG_C2S::SPELL_CAST].handler								= &WorldSession::HandleCastSpellOpcode;
	WorldPacketHandlers[MSG_C2S::SPELL_CANCEL_CAST].handler							   = &WorldSession::HandleCancelCastOpcode;
	WorldPacketHandlers[MSG_C2S::SPELL_CANCEL_AURA].handler							   = &WorldSession::HandleCancelAuraOpcode;
	WorldPacketHandlers[MSG_C2S::SPELL_CANCEL_CHANNELLING].handler						= &WorldSession::HandleCancelChannellingOpcode;
	WorldPacketHandlers[MSG_C2S::SPELL_CANCEL_AUTO_REPEAT].handler				  = &WorldSession::HandleCancelAutoRepeatSpellOpcode;
	WorldPacketHandlers[MSG_C2S::SPELL_LEARN_TALENT].handler							  = &WorldSession::HandleLearnTalentOpcode;
	WorldPacketHandlers[MSG_C2S::SPELL_UNLEARN_TALENTS].handler						   = &WorldSession::HandleUnlearnTalents;
	WorldPacketHandlers[MSG_C2S::MSG_TALENT_WIPE_CONFIRM].handler						= &WorldSession::HandleUnlearnTalents;

	// Combat / Duel
	WorldPacketHandlers[MSG_C2S::ATTACK_SWING].handler							   = &WorldSession::HandleAttackSwingOpcode;
	WorldPacketHandlers[MSG_C2S::ATTACK_STOP].handler								= &WorldSession::HandleAttackStopOpcode;
	WorldPacketHandlers[MSG_C2S::DUEL_ACCEPTED].handler							 = &WorldSession::HandleDuelAccepted;
	WorldPacketHandlers[MSG_C2S::DUEL_CANCELLED].handler							= &WorldSession::HandleDuelCancelled;

	// Trade
	WorldPacketHandlers[MSG_C2S::TRADE_INITIATE].handler							= &WorldSession::HandleInitiateTrade;
	WorldPacketHandlers[MSG_C2S::TRADE_BEGIN].handler							   = &WorldSession::HandleBeginTrade;
	WorldPacketHandlers[MSG_C2S::TRADE_BUSY].handler								= &WorldSession::HandleBusyTrade;
	WorldPacketHandlers[MSG_C2S::TRADE_IGNORE].handler							  = &WorldSession::HandleIgnoreTrade;
	WorldPacketHandlers[MSG_C2S::TRADE_ACCEPT].handler							  = &WorldSession::HandleAcceptTrade;
	//WorldPacketHandlers[MSG_C2S::TRADE_UNACCEPT].handler							= &WorldSession::HandleUnacceptTrade;
	WorldPacketHandlers[MSG_C2S::TRADE_CANCEL].handler							  = &WorldSession::HandleCancelTrade;
	WorldPacketHandlers[MSG_C2S::TRADE_SET_ITEM].handler							= &WorldSession::HandleSetTradeItem;
	WorldPacketHandlers[MSG_C2S::TRADE_CLEAR_ITEM].handler						  = &WorldSession::HandleClearTradeItem;
	WorldPacketHandlers[MSG_C2S::TRADE_SET_GOLD].handler							= &WorldSession::HandleSetTradeGold;
	WorldPacketHandlers[MSG_C2S::TRADE_LOCK].handler								= &WorldSession::HandleTradeLock;

	// Quest System
	WorldPacketHandlers[MSG_C2S::QUESTGIVER_STATUS_QUERY].handler				   = &WorldSession::HandleQuestgiverStatusQueryOpcode;
	WorldPacketHandlers[MSG_C2S::QUESTGIVER_HELLO].handler						  = &WorldSession::HandleQuestgiverHelloOpcode;
	WorldPacketHandlers[MSG_C2S::QUESTGIVER_ACCEPT_QUEST].handler				   = &WorldSession::HandleQuestgiverAcceptQuestOpcode;
	WorldPacketHandlers[MSG_C2S::QUESTGIVER_CANCEL].handler						 = &WorldSession::HandleQuestgiverCancelOpcode;
	WorldPacketHandlers[MSG_C2S::QUESTGIVER_CHOOSE_REWARD].handler				  = &WorldSession::HandleQuestgiverChooseRewardOpcode;
	WorldPacketHandlers[MSG_C2S::QUESTGIVER_REQUEST_REWARD].handler				 = &WorldSession::HandleQuestgiverRequestRewardOpcode;
	WorldPacketHandlers[MSG_C2S::QUEST_QUERY].handler							   = &WorldSession::HandleQuestQueryOpcode;
	WorldPacketHandlers[MSG_C2S::QUESTGIVER_QUERY_QUEST].handler					= &WorldSession::HandleQuestGiverQueryQuestOpcode;
	WorldPacketHandlers[MSG_C2S::QUESTGIVER_COMPLETE_QUEST].handler				 = &WorldSession::HandleQuestgiverCompleteQuestOpcode;
	WorldPacketHandlers[MSG_C2S::QUESTLOG_REMOVE_QUEST].handler					 = &WorldSession::HandleQuestlogRemoveQuestOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_RECLAIM_CORPSE].handler							= &WorldSession::HandleCorpseReclaimOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_RESURRECT_RESPONSE].handler						= &WorldSession::HandleResurrectResponseOpcode;
	WorldPacketHandlers[MSG_C2S::RELIVE_REQ].handler									=&WorldSession::HandleReliveReq;
	WorldPacketHandlers[MSG_C2S::QUEST_PUSHTO_PARTY].handler						  = &WorldSession::HandlePushQuestToPartyOpcode;
	WorldPacketHandlers[MSG_C2S::QUEST_PUSH_RESULT].handler						  = &WorldSession::HandleQuestPushResult;
	WorldPacketHandlers[MSG_C2S::CMSG_SERVERTIME].handler						 = &WorldSession::HandleServerTime;

	// Auction System
	WorldPacketHandlers[MSG_C2S::AUCTION_LIST_ITEMS].handler						= &WorldSession::HandleAuctionListItems;
	WorldPacketHandlers[MSG_C2S::AUCTION_LIST_BIDDER_ITEMS].handler				 = &WorldSession::HandleAuctionListBidderItems;
	WorldPacketHandlers[MSG_C2S::AUCTION_SELL_ITEM].handler						 = &WorldSession::HandleAuctionSellItem;
	WorldPacketHandlers[MSG_C2S::AUCTION_LIST_OWNER_ITEMS].handler				  = &WorldSession::HandleAuctionListOwnerItems;
	WorldPacketHandlers[MSG_C2S::AUCTION_PLACE_BID].handler						 = &WorldSession::HandleAuctionPlaceBid;
	WorldPacketHandlers[MSG_C2S::AUCTION_REMOVE_ITEM].handler					   = &WorldSession::HandleCancelAuction;

	// Mail System
	WorldPacketHandlers[MSG_C2S::MAIL_GET_LIST].handler							 = &WorldSession::HandleGetMail;
	WorldPacketHandlers[MSG_C2S::MAIL_TEXT_QUERY].handler						   = &WorldSession::HandleItemTextQuery;
	WorldPacketHandlers[MSG_C2S::MAIL_SEND].handler								 = &WorldSession::HandleSendMail;
	WorldPacketHandlers[MSG_C2S::MAIL_TAKE_MONEY].handler						   = &WorldSession::HandleTakeMoney;
	WorldPacketHandlers[MSG_C2S::MAIL_TAKE_ITEM].handler							= &WorldSession::HandleTakeItem;
	WorldPacketHandlers[MSG_C2S::MAIL_MARK_AS_READ].handler						 = &WorldSession::HandleMarkAsRead;
	WorldPacketHandlers[MSG_C2S::MAIL_RETURN_TO_SENDER].handler					 = &WorldSession::HandleReturnToSender;
	WorldPacketHandlers[MSG_C2S::MAIL_DELETE].handler							   = &WorldSession::HandleMailDelete;
	WorldPacketHandlers[MSG_C2S::MAIL_QUERY_NEXT_TIME].handler					   = &WorldSession::HandleMailTime;
	WorldPacketHandlers[MSG_C2S::MAIL_CREATE_TEXT_ITEM].handler					 = &WorldSession::HandleMailCreateTextItem;

	// Guild Query (called when not logged in sometimes)
	WorldPacketHandlers[MSG_C2S::GUILD_QUERY].handler							   = &WorldSession::HandleGuildQuery;
	WorldPacketHandlers[MSG_C2S::GUILD_QUERY].status								= STATUS_AUTHED;

	// Guild System
	WorldPacketHandlers[MSG_C2S::GUILD_CREATE].handler							  = &WorldSession::HandleCreateGuild;
	WorldPacketHandlers[MSG_C2S::GUILD_INVITE].handler							  = &WorldSession::HandleInviteToGuild;
	WorldPacketHandlers[MSG_C2S::GUILD_ACCEPT].handler							  = &WorldSession::HandleGuildAccept;
	WorldPacketHandlers[MSG_C2S::GUILD_DECLINE].handler							 = &WorldSession::HandleGuildDecline;
	WorldPacketHandlers[MSG_C2S::GUILD_INFO].handler								= &WorldSession::HandleGuildInfo;
	WorldPacketHandlers[MSG_C2S::GUILD_ROSTER].handler							  = &WorldSession::HandleGuildRoster;
	WorldPacketHandlers[MSG_C2S::GUILD_PROMOTE].handler							 = &WorldSession::HandleGuildPromote;
	WorldPacketHandlers[MSG_C2S::GUILD_DEMOTE].handler							  = &WorldSession::HandleGuildDemote;
	WorldPacketHandlers[MSG_C2S::GUILD_SET_MEMBER_RANK].handler						= &WorldSession::HandleGuildSetMemberRank;
	WorldPacketHandlers[MSG_C2S::GUILD_QUERY_GUILD_LIST_REQ].handler				= &WorldSession::HandleQueryGuildListReq;
	WorldPacketHandlers[MSG_C2S::GUILD_DECLARE_WAR].handler							= &WorldSession::HandleGuildDeclareWar;
	WorldPacketHandlers[MSG_C2S::GUILD_ALLY_REQ].handler							= &WorldSession::HandleGuildAllyReq;

	WorldPacketHandlers[MSG_C2S::GUILD_LEAVE].handler							   = &WorldSession::HandleGuildLeave;
	WorldPacketHandlers[MSG_C2S::GUILD_REMOVE].handler							  = &WorldSession::HandleGuildRemove;
	WorldPacketHandlers[MSG_C2S::GUILD_DISBAND].handler							 = &WorldSession::HandleGuildDisband;
	WorldPacketHandlers[MSG_C2S::GUILD_LEADER].handler							  = &WorldSession::HandleGuildLeader;
	WorldPacketHandlers[MSG_C2S::GUILD_MOTD].handler								= &WorldSession::HandleGuildMotd;
	WorldPacketHandlers[MSG_C2S::GUILD_RANK].handler								= &WorldSession::HandleGuildRank;
	WorldPacketHandlers[MSG_C2S::GUILD_ADD_RANK].handler							= &WorldSession::HandleGuildAddRank;
	WorldPacketHandlers[MSG_C2S::GUILD_DEL_RANK].handler							= &WorldSession::HandleGuildDelRank;
	WorldPacketHandlers[MSG_C2S::GUILD_SET_PUBLIC_NOTE].handler					 = &WorldSession::HandleGuildSetPublicNote;
	WorldPacketHandlers[MSG_C2S::GUILD_SET_OFFICER_NOTE].handler					= &WorldSession::HandleGuildSetOfficerNote;
	WorldPacketHandlers[MSG_C2S::GUILD_PETITION_BUY].handler							  = &WorldSession::HandleCharterBuy;
	WorldPacketHandlers[MSG_C2S::GUILD_PETITION_SHOW_SIGNATURES].handler				  = &WorldSession::HandleCharterShowSignatures;
	WorldPacketHandlers[MSG_C2S::GUILD_TURN_IN_PETITION].handler						  = &WorldSession::HandleCharterTurnInCharter;
	WorldPacketHandlers[MSG_C2S::GUILD_PETITION_QUERY].handler							= &WorldSession::HandleCharterQuery;
	WorldPacketHandlers[MSG_C2S::GUILD_OFFER_PETITION].handler							= &WorldSession::HandleCharterOffer;
	WorldPacketHandlers[MSG_C2S::GUILD_PETITION_SIGN].handler							 = &WorldSession::HandleCharterSign;
	WorldPacketHandlers[MSG_C2S::GUILD_PETITION_RENAME].handler							= &WorldSession::HandleCharterRename;
	WorldPacketHandlers[MSG_C2S::GUILD_SAVE_EMBLEM].handler						  = &WorldSession::HandleSaveGuildEmblem;
	WorldPacketHandlers[MSG_C2S::GUILD_SET_INFORMATION].handler					 = &WorldSession::HandleSetGuildInformation;
	WorldPacketHandlers[MSG_C2S::GUILD_LOG].handler								= &WorldSession::HandleGuildLog;
	WorldPacketHandlers[MSG_C2S::GUILD_BANK_OPEN].handler						= &WorldSession::HandleGuildBankOpenVault;
	WorldPacketHandlers[MSG_C2S::GUILD_BANK_PURCHASE_TAB].handler				= &WorldSession::HandleGuildBankBuyTab;
	WorldPacketHandlers[MSG_C2S::GUILD_BANK_GET_AVAILABLE_AMOUNT].handler		= &WorldSession::HandleGuildBankGetAvailableAmount;
	WorldPacketHandlers[MSG_C2S::GUILD_BANK_MODIFY_TAB].handler					= &WorldSession::HandleGuildBankModifyTab;
	WorldPacketHandlers[MSG_C2S::GUILD_BANK_DEPOSIT_ITEM].handler				= &WorldSession::HandleGuildBankDepositItem;
	WorldPacketHandlers[MSG_C2S::GUILD_BANK_WITHDRAW_MONEY].handler				= &WorldSession::HandleGuildBankWithdrawMoney;
	WorldPacketHandlers[MSG_C2S::GUILD_BANK_DEPOSIT_MONEY].handler				= &WorldSession::HandleGuildBankDepositMoney;
	WorldPacketHandlers[MSG_C2S::GUILD_BANK_VIEW_TAB].handler					= &WorldSession::HandleGuildBankViewTab;
	WorldPacketHandlers[MSG_C2S::GUILD_BANK_LOG].handler							= &WorldSession::HandleGuildBankViewLog;
	WorldPacketHandlers[MSG_C2S::GUILD_GET_FULL_PERMISSIONS].handler				= &WorldSession::HandleGuildGetFullPermissions;
	WorldPacketHandlers[MSG_C2S::MSG_REQUEST_GUILD_LEVEL_UP].handler				= &WorldSession::HandleRequestGuildLevelUp;

	// Tutorials
	WorldPacketHandlers[MSG_C2S::TUTORIAL_FLAG].handler							 = &WorldSession::HandleTutorialFlag;
	WorldPacketHandlers[MSG_C2S::TUTORIAL_CLEAR].handler							= &WorldSession::HandleTutorialClear;
	WorldPacketHandlers[MSG_C2S::TUTORIAL_RESET].handler							= &WorldSession::HandleTutorialReset;

	// Pets
	WorldPacketHandlers[MSG_C2S::PET_ACTION].handler								= &WorldSession::HandlePetAction;
	WorldPacketHandlers[MSG_C2S::PET_REQUEST_INFO].handler						  = &WorldSession::HandlePetInfo;
	WorldPacketHandlers[MSG_C2S::QUERY_PET_NAME].handler							= &WorldSession::HandlePetNameQuery;
	WorldPacketHandlers[MSG_C2S::PET_BUY_STABLE_SLOT].handler						   = &WorldSession::HandleBuyStableSlot;
	WorldPacketHandlers[MSG_C2S::PET_STABLE].handler								= &WorldSession::HandleStablePet;
	WorldPacketHandlers[MSG_C2S::PET_UNSTABLE].handler							  = &WorldSession::HandleUnstablePet;
	WorldPacketHandlers[MSG_C2S::PET_STABLE_SWAP].handler							  = &WorldSession::HandleStableSwapPet;
	WorldPacketHandlers[MSG_C2S::PETS_LIST_STABLED].handler						  = &WorldSession::HandleStabledPetList;
	WorldPacketHandlers[MSG_C2S::PET_SET_ACTION].handler							= &WorldSession::HandlePetSetActionOpcode;
	WorldPacketHandlers[MSG_C2S::PET_RENAME].handler								= &WorldSession::HandlePetRename;
	WorldPacketHandlers[MSG_C2S::PET_ABANDON].handler							   = &WorldSession::HandlePetAbandon;
	WorldPacketHandlers[MSG_C2S::PET_UNLEARN].handler								= &WorldSession::HandlePetUnlearn;

	// Battlegrounds
	WorldPacketHandlers[MSG_C2S::BATTLEFIELD_PORT].handler						  = NULL;//&WorldSession::HandleBattlefieldPortOpcode;
	WorldPacketHandlers[MSG_C2S::BATTLEFIELD_STATUS].handler						= NULL;//&WorldSession::HandleBattlefieldStatusOpcode;
	WorldPacketHandlers[MSG_C2S::BATTLEFIELD_LIST].handler						  = NULL;//&WorldSession::HandleBattlefieldListOpcode;
	WorldPacketHandlers[MSG_C2S::BATTLEMASTER_HELLO].handler						= NULL;//&WorldSession::HandleBattleMasterHelloOpcode;
	WorldPacketHandlers[MSG_C2S::ARENA_JOIN].handler								= NULL;//&WorldSession::HandleArenaJoinOpcode;
	WorldPacketHandlers[MSG_C2S::BATTLEMASTER_JOIN].handler						 = NULL;//&WorldSession::HandleBattleMasterJoinOpcode;
	WorldPacketHandlers[MSG_C2S::BATTLEFIELD_LEAVE].handler						 = NULL;//&WorldSession::HandleLeaveBattlefieldOpcode;
	WorldPacketHandlers[MSG_C2S::AREA_SPIRIT_HEALER_QUERY].handler				  = NULL;//&WorldSession::HandleAreaSpiritHealerQueryOpcode;
	WorldPacketHandlers[MSG_C2S::AREA_SPIRIT_HEALER_QUEUE].handler				  = NULL;//&WorldSession::HandleAreaSpiritHealerQueueOpcode;
	WorldPacketHandlers[MSG_C2S::BATTLEGROUND_PLAYER_POSITIONS].handler			  = NULL;//&WorldSession::HandleBattlegroundPlayerPositionsOpcode;
	WorldPacketHandlers[MSG_C2S::PVP_LOG_DATA].handler							   = NULL;//&WorldSession::HandlePVPLogDataOpcode;
	WorldPacketHandlers[MSG_C2S::INSPECT_HONOR_STATS].handler						= NULL;//&WorldSession::HandleInspectHonorStatsOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_SET_ACTIONBAR_TOGGLES].handler					 = &WorldSession::HandleSetActionBarTogglesOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_MOVE_SPLINE_DONE].handler						  = &WorldSession::HandleMoveSplineCompleteOpcode;

	// GM Ticket System
// 	WorldPacketHandlers[MSG_C2S::CMSG_GMTICKET_CREATE].handler						   = &WorldSession::HandleGMTicketCreateOpcode;
// 	WorldPacketHandlers[MSG_C2S::CMSG_GMTICKET_UPDATETEXT].handler					   = &WorldSession::HandleGMTicketUpdateOpcode;
// 	WorldPacketHandlers[MSG_C2S::CMSG_GMTICKET_DELETETICKET].handler					 = &WorldSession::HandleGMTicketDeleteOpcode;
// 	WorldPacketHandlers[MSG_C2S::CMSG_GMTICKET_GETTICKET].handler						= &WorldSession::HandleGMTicketGetTicketOpcode;
// 	WorldPacketHandlers[MSG_C2S::CMSG_GMTICKET_SYSTEMSTATUS].handler					 = &WorldSession::HandleGMTicketSystemStatusOpcode;
// 	WorldPacketHandlers[MSG_C2S::CMSG_GMTICKETSYSTEM_TOGGLE].handler					 = &WorldSession::HandleGMTicketToggleSystemStatusOpcode;
	WorldPacketHandlers[MSG_C2S::SKILL_UNLEARN].handler							 = &WorldSession::HandleUnlearnSkillOpcode;

	// Meeting Stone / Instances
	WorldPacketHandlers[MSG_C2S::CMSG_SUMMON_RESPONSE].handler							= &WorldSession::HandleSummonResponseOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_RESET_INSTANCE].handler							= &WorldSession::HandleResetInstanceOpcode;
	WorldPacketHandlers[MSG_C2S::SPELL_SELF_RES].handler								  = &WorldSession::HandleSelfResurrectOpcode;
	WorldPacketHandlers[MSG_C2S::MSG_RANDOM_ROLL].handler								= &WorldSession::HandleRandomRollOpcode;
	WorldPacketHandlers[MSG_C2S::MSG_DUNGEON_DIFFICULTY].handler                        = &WorldSession::HandleDungeonDifficultyOpcode;

	// Misc
	WorldPacketHandlers[MSG_C2S::ITEM_OPEN].handler								 = &WorldSession::HandleOpenItemOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_COMPLETE_CINEMATIC].handler						= &WorldSession::HandleCompleteCinematic;
	WorldPacketHandlers[MSG_C2S::MOVE_MOUNTSPECIAL_ANIM].handler						 = &WorldSession::HandleMountSpecialAnimOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_TOGGLE_CLOAK].handler							  = &WorldSession::HandleToggleCloakOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_TOGGLE_HELM].handler							   = &WorldSession::HandleToggleHelmOpcode;
	WorldPacketHandlers[MSG_C2S::CMSG_SET_VISIBLE_RANK].handler							= &WorldSession::HandleSetVisibleRankOpcode;
	WorldPacketHandlers[MSG_C2S::CHAT_REPORT_SPAM].handler								= &WorldSession::HandleReportSpamOpcode;

	WorldPacketHandlers[MSG_C2S::SPELL_ADD_DYNAMIC_TARGET_OBSOLETE].handler				= &WorldSession::HandleAddDynamicTargetOpcode;


	// Arenas
	WorldPacketHandlers[MSG_C2S::ARENA_TEAM_QUERY].handler = &WorldSession::HandleArenaTeamQueryOpcode;
	WorldPacketHandlers[MSG_C2S::ARENA_TEAM_ROSTER].handler = &WorldSession::HandleArenaTeamRosterOpcode;
	WorldPacketHandlers[MSG_C2S::ARENA_TEAM_ADD_MEMBER].handler = &WorldSession::HandleArenaTeamAddMemberOpcode;
	WorldPacketHandlers[MSG_C2S::ARENA_TEAM_INVITE_ACCEPT].handler = &WorldSession::HandleArenaTeamInviteAcceptOpcode;
	WorldPacketHandlers[MSG_C2S::ARENA_TEAM_INVITE_DECLINE].handler = &WorldSession::HandleArenaTeamInviteDenyOpcode;
	WorldPacketHandlers[MSG_C2S::ARENA_TEAM_LEAVE].handler = &WorldSession::HandleArenaTeamLeaveOpcode;
	WorldPacketHandlers[MSG_C2S::ARENA_TEAM_REMOVE_PLAYER].handler = &WorldSession::HandleArenaTeamRemoveMemberOpcode;
	WorldPacketHandlers[MSG_C2S::ARENA_TEAM_DISBAND].handler = &WorldSession::HandleArenaTeamDisbandOpcode;
	WorldPacketHandlers[MSG_C2S::ARENA_TEAM_PROMOTE].handler = &WorldSession::HandleArenaTeamPromoteOpcode;
	WorldPacketHandlers[MSG_C2S::INSPECT_ARENA_STATS].handler = NULL;

// #ifdef CLUSTERING
	WorldPacketHandlers[MSG_C2S::CMSG_PING].handler = &WorldSession::HandlePingOpcode;
// #endif

	// cheat/gm commands?
	WorldPacketHandlers[MSG_C2S::MOVE_TELEPORT_CHEAT].handler = &WorldSession::HandleTeleportCheatOpcode;
	WorldPacketHandlers[MSG_C2S::MOVE_TELEPORT_TO_UNIT].handler = &WorldSession::HandleTeleportToUnitOpcode;
	WorldPacketHandlers[MSG_C2S::MOVE_WORLD_TELEPORT].handler = &WorldSession::HandleWorldportOpcode;

	// voicechat
// 	WorldPacketHandlers[MSG_C2S::CMSG_ENABLE_MICROPHONE].handler = &WorldSession::HandleEnableMicrophoneOpcode;
// 	WorldPacketHandlers[MSG_C2S::CMSG_VOICE_CHAT_QUERY].handler = &WorldSession::HandleVoiceChatQueryOpcode;
// 	WorldPacketHandlers[MSG_C2S::CMSG_VOICE_CHAT_QUERY].handler = &WorldSession::HandleChannelVoiceQueryOpcode;
// 	WorldPacketHandlers[MSG_C2S::LOOT_SET_AUTO_PASS].handler = &WorldSession::HandleSetAutoLootPassOpcode;

	WorldPacketHandlers[MSG_C2S::QUESTGIVER_INRANGE_STATUS_QUERY].handler = &WorldSession::HandleInrangeQuestgiverQuery;

	// contribution
	WorldPacketHandlers[MSG_C2S::QUERY_CONTRIBUTION_PLAYER].handler = NULL;//&WorldSession::HandleQueryContributionPlayer;
	WorldPacketHandlers[MSG_C2S::CONTRIBUTION_CONFIRM].handler = NULL;//&WorldSession::HandleContributionConfirm;
	WorldPacketHandlers[MSG_C2S::QUERY_SELF_CONTRIBUTION_HISTORY].handler = NULL;//&WorldSession::HandleQuerySelfContributionHistory;
	WorldPacketHandlers[MSG_C2S::QUERY_CONTRIBUTION_BILLBOARD].handler = NULL;//&WorldSession::HandleQueryContributionBillboard;

	WorldPacketHandlers[MSG_C2S::CMSG_SHOP_BUY].handler					= &WorldSession::HandleShopBuy;
	WorldPacketHandlers[MSG_C2S::CMSG_SHOP_ADDTOCATEGORY].handler		= &WorldSession::handleShopCategory;
	WorldPacketHandlers[MSG_C2S::CMSG_SHOP_GETLIST].handler				= &WorldSession::handleShopGetList;

	WorldPacketHandlers[MSG_C2S::MSG_TRADE_SWITCH].handler			= &WorldSession::HandleTradeSwitch;

	WorldPacketHandlers[MSG_C2S::RESURRECT_BY_OTHER].handler			= &WorldSession::HandleResurrectByOther;

	WorldPacketHandlers[MSG_C2S::MSG_QUEUE_INSTANCE].handler			= &WorldSession::HandleQueueInstance;
	WorldPacketHandlers[MSG_C2S::MSG_LEAVE_QUEUE_INSTANCE].handler			= &WorldSession::HandleLeaveQueueInstance;
	WorldPacketHandlers[MSG_C2S::MSG_ENTER_LEAVE_PVP_ZONE].handler			= &WorldSession::HandleEnterLeavePVPZone;

	WorldPacketHandlers[MSG_C2S::MSG_JINGLIAN_CONTAINER_MOVE_ITEM_REQ].handler = &WorldSession::HandleRefineContainerMoveItemReq;
	WorldPacketHandlers[MSG_C2S::MSG_JINGLIAN_ITEM_REQ].handler = &WorldSession::HandleRefineItemReq;

	WorldPacketHandlers[MSG_C2S::MSG_XIANGQIAN_CONTAINER_MOVE_ITEM_REQ].handler = &WorldSession::HandleRefineContainerMoveItemReq;
	WorldPacketHandlers[MSG_C2S::MSG_XIANGQIAN_ITEM_REQ].handler = &WorldSession::HandleRefineItemReq;
	WorldPacketHandlers[MSG_C2S::CMSG_CHOICE_INSTANCE_DEAD_EXIT].handler = &WorldSession::HandleInstanceDeadChoice;
	WorldPacketHandlers[MSG_C2S::MSG_RECRUIT_REQ].handler = &WorldSession::HandleRecruitReq;
	WorldPacketHandlers[MSG_C2S::MSG_RECRUIT_REPLY].handler = &WorldSession::HandleRecruitReply;

	WorldPacketHandlers[MSG_C2S::MSG_BUY_CASTLE_NPC].handler = &WorldSession::HandleBuyCastleNPC;
	WorldPacketHandlers[MSG_C2S::MSG_QUERY_CASTLE_STATE].handler = &WorldSession::HandleQueryCastleState;
	WorldPacketHandlers[MSG_C2S::MSG_LADDER_REQ].handler = &WorldSession::HandleLadderQuery;

	WorldPacketHandlers[MSG_C2S::MSG_TITLE_LIST].handler = &WorldSession::HandleTitleList;
	WorldPacketHandlers[MSG_C2S::MSG_CHOOSE_TITLE].handler = &WorldSession::HandleChooseTitle;

	WorldPacketHandlers[MSG_C2S::MSG_QUEST_ESCORT_REPLY].handler = &WorldSession::HandleQuestEscortReply;

	WorldPacketHandlers[MSG_C2S::MSG_ACT_TOOL_ACCESS_REQ].handler = &WorldSession::HandleActToolAccessReq;
	WorldPacketHandlers[MSG_C2S::MSG_QUERY_ACTIVITY_LIST].handler = &WorldSession::HandleQueryActivityList;
	WorldPacketHandlers[MSG_C2S::MSG_UPDATE_ACTIVITY_REQ].handler = &WorldSession::HandleUpdateActivityReq;
	WorldPacketHandlers[MSG_C2S::MSG_REMOVE_ACTIVITY_REQ].handler = &WorldSession::HandleRemoveActivityReq;

	WorldPacketHandlers[MSG_C2S::MSG_QUERY_PLAYERS].handler = &WorldSession::HandleQueryPlayers;
	
	WorldPacketHandlers[MSG_C2S::MSG_LEAP_PREPARE].handler = &WorldSession::HandleLeapPrepare;

	WorldPacketHandlers[MSG_C2S::MSG_CREDIT_APPLY_REQ].handler = &WorldSession::HandleCreditApplyReq;

	WorldPacketHandlers[MSG_C2S::MSG_CREDIT_ACTION_REQ].handler = &WorldSession::HandleCreditAction;

	WorldPacketHandlers[MSG_C2S::MSG_QUERY_CREDIT_HISTORY_REQ].handler = &WorldSession::HandleQueryCreditHistoryReq;

	
	WorldPacketHandlers[MSG_C2S::MSG_DONATE_REQ].handler = &WorldSession::HandleDonationDonateReq;
	WorldPacketHandlers[MSG_C2S::MSG_QUERY_DONATION_LADDER].handler = &WorldSession::HandleQueryDonationLadder;

	WorldPacketHandlers[MSG_C2S::SERIAL_CARD_FOR_GIFT].handler = &WorldSession::HandleSerialForGiftUseReq;

	WorldPacketHandlers[MSG_C2S::MSG_UNBIND_ITEM_REQ].handler = &WorldSession::HandleUnbindItem;
	WorldPacketHandlers[MSG_C2S::MSG_APPEAR_ITEM_REQ].handler = &WorldSession::HandleAppearItem;
	WorldPacketHandlers[MSG_C2S::MSG_FALL_SPEED_ON_LAND].handler = &WorldSession::HandleFallSpeedOnLand;

}

//#ifdef CLUSTERING
void WorldSession::HandlePingOpcode(CPacketUsn& packet)
{
// 	uint32 pong;
// 	packet >> pong;

 	MSG_S2C::stPong msg;
 	SendPacket(msg);

	if((uint32)UNIXTIME - m_lastPing < 10)
	{
		m_fastpingcnt+=2;
	}
	else if((uint32)UNIXTIME - m_lastPing < 20)
	{
		m_fastpingcnt++;
	}
	else
	{
		m_fastpingcnt = 0;
	}
	m_lastPing = (uint32)UNIXTIME;

	//m_clientTimeDelay = 0;
}

//#endif

void WorldSession::SystemMessage(const char * format, ...)
{
	char buffer[1024];
	va_list ap;
	va_start(ap,format);
	vsnprintf(buffer,1024,format,ap);
	va_end(ap);

	MSG_S2C::stChat_Message Msg;
	sChatHandler.FillSystemMessageData(buffer, &Msg);
	SendPacket(Msg);
}

void WorldSession::SendChatPacket(MSG_S2C::stChat_Message* Msg, WorldSession * originator)
{
	SendPacket(*Msg);
}

void WorldSession::SendItemPushResult(Item * pItem, bool Created, bool Received, bool SendToSet, bool NewItem, uint8 DestBagSlot, uint32 DestSlot, uint32 AddCount)
{
	MSG_S2C::stItemPushResult data;

	data.recivee_guid = _player->GetGUID();
	data.received = Received;
	data.created = Created;
	data.created = 1;
	data.dstbagslot = DestBagSlot;
	data.dstslot = NewItem ? DestSlot : 0xFFFFFFFF;
	data.entry = pItem->GetEntry();
	data.suffix = pItem->GetItemRandomSuffixFactor();
	data.randomprop = pItem->GetUInt32Value( ITEM_FIELD_RANDOM_PROPERTIES_ID );
	data.count = AddCount;
	data.stackcount = pItem->GetUInt32Value(ITEM_FIELD_STACK_COUNT);

	if(SendToSet)
	{
		if( _player->GetGroup() )
			_player->GetGroup()->SendPacketToAll( data );
		else
			SendPacket( data );
	}
	else
	{
		SendPacket( data );
	}

	SunyouRaid* raid = _player->GetSunyouRaid();
	if( raid )
		raid->OnPlayerLootItem( _player, pItem->GetEntry(), AddCount );

	if( pItem->GetProto()->Flags & ITEM_FLAG_WATCH_OBTAIN && _player->m_playerInfo )
	{
		_player->m_playerInfo->AddItemObtain( pItem->GetEntry(), AddCount );
		_player->CheckArenaTitles( true );
	}
}

void WorldSession::Delete()
{
	delete this;
}

void WorldSession::Disconnect()
{
	LogoutPlayer(true);
	sWorld.DeleteSession( this );
}

Player* WorldSession::RealEnterGame()
{
	Player* plr = new Player((uint32)_roleID);
	ASSERT(plr);
	plr->SetSession( this );
	this->m_loggingInPlayer = plr;
	m_bIsWLevelSet = false;

	if( g_crosssrvmgr->m_isInstanceSrv )
		plr->_db = g_crosssrvmgr->GetDBByGUID( (uint32)_roleID );
	else
	{
		QueryResult* qr = NULL;
		if (g_crosssrvmgr->tempdb)
			qr = g_crosssrvmgr->tempdb->Query( "select guid from characters where guid = %u", (uint32)_roleID );
		if( qr ) {
			sWorld.ExecuteSqlToDBServer( "delete from playeritems where ownerguid = %u", (uint32)_roleID );
			//CharacterDatabase.Execute( "delete from refine_item_container where guid = %u", (uint32)_roleID );
			plr->_db = g_crosssrvmgr->tempdb;
			delete qr;
			plr->m_isReturn2Mother = true;
		}
		else {
			plr->_db = &CharacterDatabase;
		}
	}

	plr->LoadFromDB((uint32)_roleID);
	MyLog::log->debug("WorldSession : Async loading player %u", (uint32)_roleID);
	return plr;
}
