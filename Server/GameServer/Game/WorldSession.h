#ifndef __WORLDSESSION_H
#define __WORLDSESSION_H

#include "../Net/GTSocket.h"
#include "StackBuffer.h"
#include "../../SDBase/Protocol/S2C_Move.h"
#define  WAYPOINTENTRY 300000
struct TrainerSpell;
//#define SESSION_CAP 5

// MovementFlags Contribution by Tenshi
namespace MSG_S2C
{
	struct stQuery_Quest_Response;
	struct stChat_Message;
}
namespace MSG_C2S
{
	struct stMove_OP;
}

struct OpcodeHandler
{
	uint16 status;
	void (WorldSession::*handler)(CPacketUsn& packet);
};

enum SessionStatus
{
	STATUS_AUTHED = 0,
	STATUS_LOGGEDIN
};

struct AccountDataEntry
{
	char * data;
	uint32 sz;
	bool bIsDirty;
};

typedef struct Cords {
	float x,y,z;
}Cords;

#define CHECK_INWORLD_RETURN if(!_player->IsInWorld()) { return; }
#define CHECK_GUID_EXISTS(guidx) if(_player->GetMapMgr()->GetUnit((guidx)) == NULL) { return; }
#define CHECK_PACKET_SIZE(pckp, ssize) if(ssize && pckp.size() < ssize) { Disconnect(); return; }

#define NOTIFICATION_MESSAGE_NO_PERMISSION "You do not have permission to perform that function."
//#define CHECK_PACKET_SIZE(x, y) if(y > 0 && x.size() < y) { _socket->Disconnect(); return; }

void EncodeHex(const char* source, char* dest, uint32 size);
void DecodeHex(const char* source, char* dest, uint32 size);

// extern OpcodeHandler WorldPacketHandlers[MSG_C2S::NUM_MSG_TYPES];
void CapitalizeString(string& arg);

class SERVER_DECL WorldSession
{
	friend class WorldSocket;
public:
	WorldSession(uint32 AccountID, uint32 SessionID, uint64 RoleID, string RoleName, const string& AccountName, CGTSocket* m_GT, string ClientIP);
	~WorldSession();

	CGTSocket* _GTSocket;
	Player * m_loggingInPlayer;
	ui32	_SessionID;
	void SendPacket(PakHead& pak, uint32 delay = 0);
	void SendData(const char* pData, ui32 len);
	//void SendPacket(StackBufferBase * packet);
	void OutPacket(uint16 opcode);

	void Delete();

	void SendChatPacket(MSG_S2C::stChat_Message* Msg, WorldSession * originator);

	uint32 m_currMsTime;
	uint32 m_lastPing;

	uint32 m_fastpingcnt;
	bool m_disableMessage;

	SUNYOU_INLINE uint32 GetAccountId() const { return _accountId; }
	SUNYOU_INLINE Player* GetPlayer() { return _player; }
	SUNYOU_INLINE string GetRemoteIP(){ return "0.0.0.0"; }
	
	/* Acct flags */
	void SetAccountFlags(uint32 flags) { _accountFlags = flags; }
	bool HasFlag(uint32 flag) { return (_accountFlags & flag) != 0; }

	/* GM Permission System */
	void LoadSecurity(std::string securitystring);
	void SetSecurity(std::string securitystring);
	SUNYOU_INLINE char* GetPermissions() { return permissions; }
	SUNYOU_INLINE int GetPermissionCount() { return permissioncount; }
	SUNYOU_INLINE bool HasPermissions() { return (permissioncount > 0) ? true : false; }
	SUNYOU_INLINE bool HasGMPermissions()
	{
		if(!permissioncount)
			return false;
		
		if (strchr(permissions,'4') || strchr(permissions,'3') || strchr(permissions,'2') || strchr(permissions,'1'))
		{
			return true ;
		}
		
		return false ;
		//return (strchr(permissions,'a')!=NULL) ? true : false;
	}
	std::string gmflags;
   
	bool CanUseCommand(char cmdstr);

	SUNYOU_INLINE void SetPlayer(Player *plr) { _player = plr; }
	
	SUNYOU_INLINE void SetAccountData(uint32 index, char* data, bool initial,uint32 sz)
	{
		ASSERT(index < 8);
		if(sAccountData[index].data)
			delete [] sAccountData[index].data;
		sAccountData[index].data = data;
		sAccountData[index].sz = sz;
		if(!initial && !sAccountData[index].bIsDirty)		// Mark as "changed" or "dirty"
			sAccountData[index].bIsDirty = true;
		else if(initial)
			sAccountData[index].bIsDirty = false;
	}
	
	SUNYOU_INLINE AccountDataEntry* GetAccountData(uint32 index)
	{
		ASSERT(index < 8);
		return &sAccountData[index];
	}

	void SetLogoutTimer(uint32 ms)
	{
		if(ms)  _logoutTime = m_currMsTime+ms;
		else	_logoutTime = 0;
	}

	void LogoutPlayer(bool Save, bool disconnectgt=true);
	
	void OutPacket(uint16 opcode, uint16 len, const void* data)
	{
// 		if(_socket && _socket->IsConnected())
// 			_socket->OutPacket(opcode, len, data);
	}

	SUNYOU_INLINE CGTSocket* GetSocket() { return _GTSocket; }
	void SetSocket(CGTSocket* pGT){ _GTSocket = pGT; }
	
	void Disconnect();

	int __fastcall Update(uint32 InstanceID);

	void SendItemPushResult(Item * pItem, bool Created, bool Received, bool SendToSet, bool NewItem, uint8 DestBagSlot, uint32 DestSlot, uint32 AddCount);
	void SendBuyFailed(uint64 guid, uint32 itemid, uint8 error);
	void SendSellItem(uint64 vendorguid, uint64 itemid, uint8 error);
	void SendNotification(const char *message, ...);

	SUNYOU_INLINE void SetInstance(uint32 Instance) { instanceId = Instance; }
	SUNYOU_INLINE uint32 GetLatency() { return _latency; }
	SUNYOU_INLINE string GetAccountName() { return _accountName; }
	SUNYOU_INLINE const char * GetAccountNameS() { return _accountName.c_str(); }

	SUNYOU_INLINE uint32 GetClientBuild() { return client_build; }
	SUNYOU_INLINE void SetClientBuild(uint32 build) { client_build = build; }
	bool bDeleted;
	SUNYOU_INLINE uint32 GetInstance() { return instanceId; }
	void _HandleAreaTriggerOpcode(uint32 id);//real handle
	int32 m_moveDelayTime;
	int32 m_clientTimeDelay;

	void LoadAccountDataProc(QueryResult * result);
	SUNYOU_INLINE bool IsLoggingOut() { return _loggingOut; }

	void SendMailError(uint32 error);

protected:

	/// Login screen opcodes (PlayerHandler.cpp):
	void HandleCharEnumOpcode(CPacketUsn& packet);
	void HandleCharDeleteOpcode(CPacketUsn& packet);
	void HandleCharCreateOpcode(CPacketUsn& packet);
	void HandlePlayerLoginOpcode(CPacketUsn& packet);

	void HandlePlayerLoadingOKOpcode(CPacketUsn& packet);
	/// Authentification and misc opcodes (MiscHandler.cpp):
	void HandlePingOpcode(CPacketUsn& packet);
	void HandleAuthSessionOpcode(CPacketUsn& packet);
	void HandleRepopRequestOpcode(CPacketUsn& packet);
	void HandleAutostoreLootItemOpcode(CPacketUsn& packet);
	void HandleLootMoneyOpcode(CPacketUsn& packet);
	void HandleLootOpcode(CPacketUsn& packet);
	void HandleLootReleaseOpcode(CPacketUsn& packet);
	void HandleLootMasterGiveOpcode(CPacketUsn& packet);
	void HandleLootRollOpcode(CPacketUsn& packet);
	void HandleWhoOpcode(CPacketUsn& packet);
	void HandleLogoutRequestOpcode(CPacketUsn& packet);
	void HandlePlayerLogoutOpcode(CPacketUsn& packet);
	void HandlePlayerExitOpcode(CPacketUsn& packet);
	void HandleLogoutCancelOpcode(CPacketUsn& packet);
	void HandleZoneUpdateOpcode(CPacketUsn& packet);
	void HandleSetTargetOpcode(CPacketUsn& packet);
	void HandleSetSelectionOpcode(CPacketUsn& packet);
	void HandleStandStateChangeOpcode(CPacketUsn& packet);
	void HandleDismountOpcode(CPacketUsn& packet);
	void HandleFriendListOpcode(CPacketUsn& packet);
	void HandleAddFriendOpcode(CPacketUsn& packet);
	void HandleDelFriendOpcode(CPacketUsn& packet);
	void HandleAddIgnoreOpcode(CPacketUsn& packet);
	void HandleDelIgnoreOpcode(CPacketUsn& packet);
	void HandleBugOpcode(CPacketUsn& packet);
	void HandleAreaTriggerOpcode(CPacketUsn& packet);
	void HandleUpdateAccountData(CPacketUsn& packet);
	void HandleRequestAccountData(CPacketUsn& packet);
	void HandleSetActionButtonOpcode(CPacketUsn& packet);
	void HandleSetWatchedFactionIndexOpcode(CPacketUsn& packet);
	void HandleTogglePVPOpcode(CPacketUsn& packet);
	void HandleAmmoSetOpcode(CPacketUsn& packet);
	void HandleGameObjectUse(CPacketUsn& packet);
	//void HandleJoinChannelOpcode(CPacketUsn& packet);
	//void HandleLeaveChannelOpcode(CPacketUsn& packet);
	void HandlePlayedTimeOpcode(CPacketUsn& packet);
	void HandleSetSheathedOpcode(CPacketUsn& packet);
	void HandleCompleteCinematic(CPacketUsn& packet);
	void HandleInspectOpcode( CPacketUsn& packet );

	/// Gm Ticket System in GMTicket.cpp:
	void HandleGMTicketCreateOpcode(CPacketUsn& packet);
	void HandleGMTicketUpdateOpcode(CPacketUsn& packet);
	void HandleGMTicketDeleteOpcode(CPacketUsn& packet);
	void HandleGMTicketGetTicketOpcode(CPacketUsn& packet);
	void HandleGMTicketSystemStatusOpcode(CPacketUsn& packet);
	void HandleGMTicketToggleSystemStatusOpcode(CPacketUsn& packet);

	/// Opcodes implemented in QueryHandler.cpp:
	void HandleNameQueryOpcode(CPacketUsn& packet);
	void HandleQueryTimeOpcode(CPacketUsn& packet);
	void HandleCreatureQueryOpcode(CPacketUsn& packet);
	void HandleGameObjectQueryOpcode(CPacketUsn& packet);
	void HandleItemNameQueryOpcode( CPacketUsn& packet );
	void HandlePageTextQueryOpcode( CPacketUsn& packet );

	/// Opcodes implemented in MovementHandler.cpp
	void HandleMoveOP(MSG_C2S::stMove_OP* moveop);
	void HandleMoveWorldportAckOpcode( CPacketUsn& packet );
	void HandleMovementOpcodes( CPacketUsn& packet );
	void HandleMoveTimeSkippedOpcode( CPacketUsn& packet );
	void HandleMoveNotActiveMoverOpcode( CPacketUsn& packet );
	void HandleSetActiveMoverOpcode( CPacketUsn& packet );
	void HandleMoveTeleportAckOpcode( CPacketUsn& packet );

	void HandleMountInviteOpcode( CPacketUsn& packet );
	void HandleMountAcceptOpcode( CPacketUsn& packet );
	void HandleMountDeclineOpcode( CPacketUsn& packet );
	void HandleMountDisbandOpcode( CPacketUsn& packet );
	void SendMountResult(Player *pPlayer, const std::string& name, uint32 err);

	/// Opcodes implemented in GroupHandler.cpp:
	void HandleGroupInviteOpcode(CPacketUsn& packet);
	void HandleGroupApplyForJoiningAckOpcode(CPacketUsn& packet);
	void HandleGroupModifyTitleReqOpcode(CPacketUsn& packet);
	void HandleGroupModifyCanApplyForJoiningOpcode(CPacketUsn& packet);
	void HandleGroupListReqOpcode(CPacketUsn& packet);
	void HandleGroupCancelOpcode(CPacketUsn& packet);
	void HandleGroupAcceptOpcode(CPacketUsn& packet);
	void HandleGroupDeclineOpcode(CPacketUsn& packet);
	void HandleGroupUninviteOpcode(CPacketUsn& packet);
	void HandleGroupUninviteGuildOpcode(CPacketUsn& packet);
	void HandleGroupSetLeaderOpcode(CPacketUsn& packet);
	void HandleGroupDisbandOpcode(CPacketUsn& packet);
	void HandleGroupSwepMember(CPacketUsn& packet);
	void HandleGroupSwepMemberName(CPacketUsn& packet);
	void HandleLootMethodOpcode(CPacketUsn& packet);
	void HandleMinimapPingOpcode(CPacketUsn& packet);
	void HandleSetPlayerIconOpcode(CPacketUsn& packet);
	void HandleGroupApplyForJoiningStateOpcode(CPacketUsn& packet);
	void SendPartyCommandResult(Player *pPlayer, uint32 p1, const std::string& name, uint32 err);

	// Raid
	void HandleConvertGroupToRaidOpcode(CPacketUsn& packet);
	void HandleGroupChangeSubGroup(CPacketUsn& packet);
	void HandleGroupAssistantLeader(CPacketUsn& packet);
	void HandleRequestRaidInfoOpcode(CPacketUsn& packet);
	void HandleReadyCheckOpcode(CPacketUsn& packet);
	void HandleGroupPromote(CPacketUsn& packet);
	
	// LFG opcodes
	void HandleEnableAutoJoin(CPacketUsn& packet);
	void HandleDisableAutoJoin(CPacketUsn& packet);
	void HandleEnableAutoAddMembers(CPacketUsn& packet);
	void HandleDisableAutoAddMembers(CPacketUsn& packet);
	void HandleSetLookingForGroupComment(CPacketUsn& packet);
	void HandleMsgLookingForGroup(CPacketUsn& packet);
	void HandleSetLookingForGroup(CPacketUsn& packet);
	void HandleSetLookingForMore(CPacketUsn& packet);
	void HandleSetLookingForNone(CPacketUsn& packet);
	void HandleLfgClear(CPacketUsn& packet);
	void HandleLfgInviteAccept(CPacketUsn& packet);
	void HandleLfgInviteDeny(CPacketUsn& packet);

	/// Taxi opcodes (TaxiHandler.cpp)
	void HandleTaxiNodeStatusQueryOpcode(CPacketUsn& packet);
	void HandleTaxiQueryAvaibleNodesOpcode(CPacketUsn& packet);
	void HandleActivateTaxiOpcode(CPacketUsn& packet);
	void HandleMultipleActivateTaxiOpcode(CPacketUsn& packet);

	/// NPC opcodes (NPCHandler.cpp)
	void HandleTabardVendorActivateOpcode(CPacketUsn& packet);
	void HandleBankerActivateOpcode(CPacketUsn& packet);
	void HandleBuyBankSlotOpcode(CPacketUsn& packet); 
	void HandleTrainerListOpcode(CPacketUsn& packet);
	void HandleTrainerBuySpellOpcode(CPacketUsn& packet);
	void HandleCharterShowListOpcode(CPacketUsn& packet);
	void HandleGossipHelloOpcode(CPacketUsn& packet);
	void HandleGossipSelectOptionOpcode(CPacketUsn& packet);
	void HandleSpiritHealerActivateOpcode(CPacketUsn& packet);
	void HandleNpcTextQueryOpcode(CPacketUsn& packet);
	void HandleBinderActivateOpcode(CPacketUsn& packet);

	// Auction House opcodes
	void HandleAuctionHelloOpcode(CPacketUsn& packet);
	void HandleAuctionListItems( CPacketUsn& packet );
	void HandleAuctionListBidderItems( CPacketUsn& packet );
	void HandleAuctionSellItem( CPacketUsn& packet );
	void HandleAuctionListOwnerItems( CPacketUsn& packet );
	void HandleAuctionPlaceBid( CPacketUsn& packet );
	void HandleCancelAuction( CPacketUsn& packet);

	// Mail opcodes
	void HandleGetMail( CPacketUsn& packet );
	void HandleSendMail( CPacketUsn& packet );
	void HandleTakeMoney( CPacketUsn& packet );
	void HandleTakeItem( CPacketUsn& packet );
	void HandleMarkAsRead( CPacketUsn& packet );
	void HandleReturnToSender( CPacketUsn& packet );
	void HandleMailDelete( CPacketUsn& packet );
	void HandleItemTextQuery( CPacketUsn& packet);
	void HandleMailTime(CPacketUsn& packet);
	void HandleMailCreateTextItem(CPacketUsn& packet );

	/// Item opcodes (ItemHandler.cpp)
	void HandleSwapInvItemOpcode(CPacketUsn& packet);
	void HandleSwapItemOpcode(CPacketUsn& packet);
	void HandleDestroyItemOpcode(CPacketUsn& packet);
	void HandleAutoEquipItemOpcode(CPacketUsn& packet);
	void HandleItemQuerySingleOpcode(CPacketUsn& packet);
	void HandleSellItemOpcode(CPacketUsn& packet);
	void HandleBuyItemInSlotOpcode(CPacketUsn& packet);
	void HandleBuyItemOpcode(CPacketUsn& packet);
	void HandleListInventoryOpcode(CPacketUsn& packet);
	void HandleAutoStoreBagItemOpcode(CPacketUsn& packet);
	void HandleBuyBackOpcode(CPacketUsn& packet);
	void HandleSplitOpcode(CPacketUsn& packet);
	void HandleReadItemOpcode(CPacketUsn& packet);
	void HandleRepairItemOpcode(CPacketUsn& packet);
	void HandleAutoBankItemOpcode(CPacketUsn& packet);
	void HandleAutoStoreBankItemOpcode(CPacketUsn& packet);
	void HandleCancelTemporaryEnchantmentOpcode(CPacketUsn& packet);
	void HandleInsertGemOpcode(CPacketUsn& packet);

	/// Combat opcodes (CombatHandler.cpp)
	void HandleAttackSwingOpcode(CPacketUsn& packet);
	void HandleAttackStopOpcode(CPacketUsn& packet);

	/// Spell opcodes (SpellHandler.cpp)
	void HandleUseItemOpcode(CPacketUsn& packet);
	void HandleCastSpellOpcode(CPacketUsn& packet);
	void HandleCancelCastOpcode(CPacketUsn& packet);
	void HandleCancelAuraOpcode(CPacketUsn& packet);
	void HandleCancelChannellingOpcode(CPacketUsn& packet);
	void HandleCancelAutoRepeatSpellOpcode(CPacketUsn& packet);
	void HandleAddDynamicTargetOpcode(CPacketUsn& packet);

	/// Skill opcodes (SkillHandler.spp)
	//void HandleSkillLevelUpOpcode(CPacketUsn& packet);
	void HandleLearnTalentOpcode(CPacketUsn& packet);
	void HandleUnlearnTalents( CPacketUsn& packet );

	/// Quest opcodes (QuestHandler.cpp)
	void HandleQuestgiverStatusQueryOpcode(CPacketUsn& packet);
	void HandleQuestgiverHelloOpcode(CPacketUsn& packet);
	void HandleQuestgiverAcceptQuestOpcode(CPacketUsn& packet);
	void HandleQuestgiverCancelOpcode(CPacketUsn& packet);
	void HandleQuestgiverChooseRewardOpcode(CPacketUsn& packet);
	void HandleQuestgiverRequestRewardOpcode(CPacketUsn& packet);
	void HandleQuestGiverQueryQuestOpcode( CPacketUsn& packet );
	void HandleQuestGiverQueryQuestOpcode( const PakHead& msg );
	void HandleQuestQueryOpcode(CPacketUsn& packet);
	void HandleQuestgiverCompleteQuestOpcode( CPacketUsn& packet );
	void HandleQuestlogRemoveQuestOpcode(CPacketUsn& packet);
	void HandlePushQuestToPartyOpcode(CPacketUsn& packet);
	void HandleQuestPushResult(CPacketUsn& packet);


	/// Chat opcodes (Chat.cpp)
	void HandleMessagechatOpcode(CPacketUsn& packet);
	void HandleTextEmoteOpcode(CPacketUsn& packet);
	void HandleReportSpamOpcode(CPacketUsn& packet);

	/// Corpse opcodes (Corpse.cpp)
	void HandleCorpseReclaimOpcode( CPacketUsn& packet );
	void HandleCorpseQueryOpcode( CPacketUsn& packet );
	void HandleResurrectResponseOpcode(CPacketUsn& packet);

	/// Channel Opcodes (ChannelHandler.cpp)
	void HandleChannelJoin(CPacketUsn& packet);
	void HandleChannelLeave(CPacketUsn& packet);
	void HandleChannelList(CPacketUsn& packet);
	void HandleChannelPassword(CPacketUsn& packet);
	void HandleChannelSetOwner(CPacketUsn& packet);
	void HandleChannelOwner(CPacketUsn& packet);
	void HandleChannelModerator(CPacketUsn& packet);
	void HandleChannelUnmoderator(CPacketUsn& packet);
	void HandleChannelMute(CPacketUsn& packet);
	void HandleChannelUnmute(CPacketUsn& packet);
	void HandleChannelInvite(CPacketUsn& packet);
	void HandleChannelKick(CPacketUsn& packet);
	void HandleChannelBan(CPacketUsn& packet);
	void HandleChannelUnban(CPacketUsn& packet);
	void HandleChannelAnnounce(CPacketUsn& packet);
	void HandleChannelModerate(CPacketUsn& packet);
	void HandleChannelNumMembersQuery(CPacketUsn& packet);
	void HandleChannelRosterQuery(CPacketUsn& packet);

	// Duel
	void HandleDuelAccepted(CPacketUsn& packet);
	void HandleDuelCancelled(CPacketUsn& packet);

	// Trade
	void HandleInitiateTrade(CPacketUsn& packet);
	void HandleBeginTrade(CPacketUsn& packet);
	void HandleBusyTrade(CPacketUsn& packet);
	void HandleIgnoreTrade(CPacketUsn& packet);
	void HandleAcceptTrade(CPacketUsn& packet);
	void HandleUnacceptTrade(CPacketUsn& packet);
	void HandleCancelTrade(CPacketUsn& packet);
	void HandleSetTradeItem(CPacketUsn& packet);
	void HandleClearTradeItem(CPacketUsn& packet);
	void HandleSetTradeGold(CPacketUsn& packet);
	void HandleTradeLock(CPacketUsn& packet);
	void HandleTradeSwitch(CPacketUsn& packet);

	// Guild
	void HandleGuildQuery(CPacketUsn& packet);
	void HandleCreateGuild(CPacketUsn& packet);				   
	void HandleInviteToGuild(CPacketUsn& packet);			  
	void HandleGuildAccept(CPacketUsn& packet);			 
	void HandleGuildDecline(CPacketUsn& packet);
	void HandleGuildInfo(CPacketUsn& packet);
	void HandleGuildRoster(CPacketUsn& packet);
	void HandleGuildPromote(CPacketUsn& packet);
	void HandleGuildDemote(CPacketUsn& packet);
	void HandleGuildSetMemberRank(CPacketUsn& packet);
	void HandleQueryGuildListReq(CPacketUsn& packet);
	void HandleGuildDeclareWar(CPacketUsn& packet);
	void HandleGuildAllyReq(CPacketUsn& packet);
	void HandleGuildLeave(CPacketUsn& packet);
	void HandleGuildRemove(CPacketUsn& packet);
	void HandleGuildDisband(CPacketUsn& packet);
	void HandleGuildLeader(CPacketUsn& packet);
	void HandleGuildMotd(CPacketUsn& packet);
	void HandleGuildRank(CPacketUsn& packet);
	void HandleGuildAddRank(CPacketUsn& packet);
	void HandleGuildDelRank(CPacketUsn& packet);
	void HandleGuildSetPublicNote(CPacketUsn& packet);
	void HandleGuildSetOfficerNote(CPacketUsn& packet);
	void HandleSaveGuildEmblem(CPacketUsn& packet);
	void HandleRequestGuildLevelUp(CPacketUsn& packet);
	void HandleCharterBuy(CPacketUsn& packet);
	void HandleCharterShowSignatures(CPacketUsn& packet);
	void HandleCharterTurnInCharter(CPacketUsn& packet);
	void HandleCharterQuery(CPacketUsn& packet);
	void HandleCharterOffer(CPacketUsn& packet);
	void HandleCharterSign(CPacketUsn& packet);
	void HandleCharterRename(CPacketUsn& packet);
	void HandleSetGuildInformation(CPacketUsn& packet);
	void HandleGuildLog(CPacketUsn& packet);
	void HandleGuildBankViewTab(CPacketUsn& packet);
	void HandleGuildBankViewLog(CPacketUsn& packet);
	void HandleGuildBankOpenVault(CPacketUsn& packet);
	void HandleGuildBankBuyTab(CPacketUsn& packet);
	void HandleGuildBankDepositMoney(CPacketUsn& packet);
	void HandleGuildBankWithdrawMoney(CPacketUsn& packet);
	void HandleGuildBankDepositItem(CPacketUsn& packet);
	void HandleGuildBankWithdrawItem(CPacketUsn& packet);
	void HandleGuildBankGetAvailableAmount(CPacketUsn& packet);
	void HandleGuildBankModifyTab(CPacketUsn& packet);
	void HandleGuildGetFullPermissions(CPacketUsn& packet);

	// Pet
	void HandlePetAction(CPacketUsn& packet);
	void HandlePetInfo(CPacketUsn& packet);
	void HandlePetNameQuery(CPacketUsn& packet);
	void HandleBuyStableSlot(CPacketUsn& packet);
	void HandleStablePet(CPacketUsn& packet);
	void HandleUnstablePet(CPacketUsn& packet);
	void HandleStabledPetList(CPacketUsn& packet);
	void HandleStableSwapPet(CPacketUsn& packet);
	void HandlePetRename(CPacketUsn& packet);
	void HandlePetAbandon(CPacketUsn& packet);
	void HandlePetUnlearn(CPacketUsn& packet);

	// Battleground
	/*
	void HandleBattlefieldPortOpcode(CPacketUsn& packet);
	void HandleBattlefieldStatusOpcode(CPacketUsn& packet);
	void HandleBattleMasterHelloOpcode(CPacketUsn& packet);
	void HandleLeaveBattlefieldOpcode(CPacketUsn& packet);
	void HandleAreaSpiritHealerQueryOpcode(CPacketUsn& packet);
	void HandleAreaSpiritHealerQueueOpcode(CPacketUsn& packet);
	void HandleBattlegroundPlayerPositionsOpcode(CPacketUsn& packet);
	void HandleArenaJoinOpcode(CPacketUsn& packet);
	void HandleBattleMasterJoinOpcode(CPacketUsn& packet);
	void HandleInspectHonorStatsOpcode(CPacketUsn& packet);
	void HandlePVPLogDataOpcode(CPacketUsn& packet);
	void HandleBattlefieldListOpcode(CPacketUsn& packet);
	*/

	void HandleSetActionBarTogglesOpcode(CPacketUsn& packet);
	void HandleMoveSplineCompleteOpcode(CPacketUsn& packet);

	/// Helper functions
	void SetNpcFlagsForTalkToQuest(const uint64& guid, const uint64& targetGuid);

	//Tutorials
	void HandleTutorialFlag ( CPacketUsn& packet );
	void HandleTutorialClear( CPacketUsn& packet );
	void HandleTutorialReset( CPacketUsn& packet );

	//Acknowledgements
	void HandleAcknowledgementOpcodes( CPacketUsn& packet );
	void HandleMountSpecialAnimOpcode(CPacketUsn& packet);	

	void HandleSelfResurrectOpcode(CPacketUsn& packet);
	void HandleUnlearnSkillOpcode(CPacketUsn& packet);
	void HandleRandomRollOpcode(CPacketUsn& packet);
	void HandleOpenItemOpcode(CPacketUsn& packet);
	
	void HandleToggleHelmOpcode(CPacketUsn& packet);
	void HandleToggleCloakOpcode(CPacketUsn& packet);
	void HandleSetVisibleRankOpcode(CPacketUsn& packet);
	void HandlePetSetActionOpcode(CPacketUsn& packet);

	//instances
	void HandleResetInstanceOpcode(CPacketUsn& packet);
    void HandleDungeonDifficultyOpcode(CPacketUsn& packet);

	uint8 TrainerGetSpellStatus(TrainerSpell* pSpell);

	void HandleCharRenameOpcode(CPacketUsn& packet);
	void HandlePartyMemberStatsOpcode(CPacketUsn& packet);
	void HandleSummonResponseOpcode(CPacketUsn& packet);

	void HandleArenaTeamAddMemberOpcode(CPacketUsn& packet);
	void HandleArenaTeamRemoveMemberOpcode(CPacketUsn& packet);
	void HandleArenaTeamInviteAcceptOpcode(CPacketUsn& packet);
	void HandleArenaTeamInviteDenyOpcode(CPacketUsn& packet);
	void HandleArenaTeamLeaveOpcode(CPacketUsn& packet);
	void HandleArenaTeamDisbandOpcode(CPacketUsn& packet);
	void HandleArenaTeamPromoteOpcode(CPacketUsn& packet);
	void HandleArenaTeamQueryOpcode(CPacketUsn& packet);
	void HandleArenaTeamRosterOpcode(CPacketUsn& packet);
	//void HandleInspectArenaStatsOpcode(CPacketUsn& packet);

	void HandleTeleportCheatOpcode(CPacketUsn& packet);
	void HandleTeleportToUnitOpcode(CPacketUsn& packet);
	void HandleWorldportOpcode(CPacketUsn& packet);
	void HandleWrapItemOpcode(CPacketUsn& packet);

	// VOICECHAT
	void HandleEnableMicrophoneOpcode(CPacketUsn& packet);
	void HandleVoiceChatQueryOpcode(CPacketUsn& packet);
	void HandleChannelVoiceQueryOpcode(CPacketUsn& packet);
	void HandleSetAutoLootPassOpcode(CPacketUsn& packet);

	void HandleSetFriendNote(CPacketUsn& packet);
	void HandleInrangeQuestgiverQuery(CPacketUsn& packet);

	// ContributionHandler.cpp
	/*
	void HandleQueryContributionPlayer(CPacketUsn& packet);
	void HandleContributionConfirm(CPacketUsn& packet);
	void HandleQuerySelfContributionHistory(CPacketUsn& packet);
	void HandleQueryContributionBillboard(CPacketUsn& packet);
	*/

	void HandleShopBuy(CPacketUsn& packet);
	void handleShopCategory(CPacketUsn& packet);
	void handleShopGetList(CPacketUsn& packet);

	void HandleReliveReq(CPacketUsn& packet);
	void HandleResurrectByOther(CPacketUsn& packet);

	void HandleQueueInstance(CPacketUsn& packet);
	void HandleLeaveQueueInstance(CPacketUsn& packet);
	void HandleEnterLeavePVPZone(CPacketUsn& packet);

	void HandleRefineContainerMoveItemReq( CPacketUsn& packet );
	void HandleRefineItemReq( CPacketUsn& packet );

	void HandleXiangqianContainerMoveItemReq( CPacketUsn& packet );
	void HandleXiangqianItemReq( CPacketUsn& packet );

	void HandleServerTime( CPacketUsn& packet );

	void HandleRecruitReq(CPacketUsn& packet);
	void HandleRecruitReply(CPacketUsn& packet);

	void HandleInstanceDeadChoice(CPacketUsn& packet);

	void HandleBuyCastleNPC(CPacketUsn& packet);

	void HandleQueryCastleState(CPacketUsn& packet);

	void HandleLadderQuery(CPacketUsn& packet);

	void HandleShowShizhuang(CPacketUsn& packet);

	void HandleTitleList(CPacketUsn& packet);
	void HandleChooseTitle(CPacketUsn& packet);

	void HandleQuestEscortReply( CPacketUsn& packet );

	void HandleActToolAccessReq( CPacketUsn& packet );
	void HandleQueryActivityList( CPacketUsn& packet );
	void HandleUpdateActivityReq( CPacketUsn& packet );
	void HandleRemoveActivityReq( CPacketUsn& packet );
	void HandleQueryPlayers( CPacketUsn& packet );
	void HandleLeapPrepare( CPacketUsn& packet );

	void HandleCreditAction( CPacketUsn& packet );
	void HandleQueryCreditHistoryReq( CPacketUsn& packet );
	void HandleCreditApplyReq( CPacketUsn& packet );

	//donation 
	void HandleDonationDonateReq(CPacketUsn& packet);    //捐助
	void HandleDonationHistroy(CPacketUsn& packet);
	void HandleQueryDonationLadder(CPacketUsn& packet);  //查询排行列表

	// serial for gift 
	void HandleSerialForGiftUseReq(CPacketUsn& packet);


	void HandleUnbindItem(CPacketUsn& packet);
	void HandleAppearItem(CPacketUsn& packet);

	void HandleGroupApplyForJoining(CPacketUsn& packet);
	void HandleFallSpeedOnLand( CPacketUsn& packet );

public:

	void SendInventoryList(Creature* pCreature);
	void SendTrainerList(Creature* pCreature);
	void SendCharterRequest(Creature* pCreature);
	void SendTaxiList(Creature* pCreature);
	void SendInnkeeperBind(Creature* pCreature);
	//void SendBattlegroundList(Creature* pCreature, uint32 mapid);
	void SendBankerList(Creature* pCreature);
	void SendTabardHelp(Creature* pCreature);
	void SendAuctionList(Creature* pCreature);
	void SendSpiritHealerRequest(Creature* pCreature);
	void FullLogin(Player * plr);
	Player* RealEnterGame();

	void SendSystemNotifyEnum( uint16 e );

	float m_wLevel; // Level of water the player is currently in
	bool m_bIsWLevelSet; // Does the m_wLevel variable contain up-to-date information about water level?
	uint64 _roleID;
	Player *_player;

public:
	ui32 GetAccountID(){ return _accountId; }
	MSG_C2S::stMove_OP* m_Movement;
	char *permissions;
	int permissioncount;

	void PushMovement( ByteBuffer* data );
	void PushMonstMovement( const MonsterMovementInfo& info );
	void ProcessPendingMovements();
	void ProcessPendingMonstMovements();

private:
	friend class Player;
		
	/* Preallocated buffers for movement handlers */

	/*uint8 movement_packet[90];*/
	
	uint32 _accountId;
	uint32 _accountFlags;
	string _roleName;
	string _accountName;
	string _ClientIP;
	int8 _side;

	uint32 _logoutTime; // time we received a logout request -- wait 20 seconds, and quit


	AccountDataEntry sAccountData[8];

	uint32 _latency;
	uint32 client_build;
	uint32 instanceId;
	uint8 _updatecount;
	MSG_S2C::stMove_OP m_movmentMessage;
	MSG_S2C::stMove_Monster m_monstMoveMessage;
public:
	bool _loggingOut;
	MovementInfo* GetMovementInfo() ;
	static void InitPacketHandlerTable();
	void HandleMsg(CPacketUsn& pakTool);
	uint32 floodLines;
	time_t floodTime;
	void SystemMessage(const char * format, ...);
	uint32 language;
	void BuildQuestQueryResponse(Quest *qst, MSG_S2C::stQuery_Quest_Response* Msg);
	uint32 m_muted;
	uint32 m_baned;
	string      m_mutereason;
	string      m_banreason;

	bool m_bFangchenmiAccount;//是否是未成年人账号
	ui32	m_LastLogout;
	uint32 m_ChenmiStat;//是否是沉迷状态 0=无,1=3小时以上,2=5小时以上
	uint32	m_ChenmiTime;//沉迷的游戏时间累积
};

typedef std::set<WorldSession*> SessionSet;

#endif
