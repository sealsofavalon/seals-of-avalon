#include "StdAfx.h"
#include "activity.h"
#include "../../../SDBase/Protocol/C2S.h"
#include "../../new_common/Source/compression/zlib/ZlibCompressionStrategy.h"
#ifndef _ACTIVITY_TOOLS
#include "../Master.h"
#endif

static activity_mgr sam;
activity_mgr* g_activity_mgr = &sam;

activity_mgr::activity_mgr()
{
#ifndef _ACTIVITY_TOOLS
	for( int i = 0; i < GIVE_EXP_TYPE_COUNT; ++i )
		exp_modifier[i] = 1.f;
	bless_exp_rate = 1.f;
	bless_exp_interval_minutes = 0;
	last_bless_time = (uint32)UNIXTIME;
	tool_session = NULL;
#endif
	m_listener = NULL;
}

activity_mgr::~activity_mgr()
{
	clear_all();
}

#ifndef _ACTIVITY_TOOLS
void activity_mgr::activity_base_t::duty_change()
{
	on_duty = !on_duty;
	g_activity_mgr->m_listener->on_duty_change( this );
}

void activity_mgr::activity_base_t::duty_update()
{
	g_activity_mgr->m_listener->on_duty_update( this );
}

void activity_mgr::exp_config_t::duty( bool on )
{
	if( on )
	{
		if( !on_duty )
		{
			g_activity_mgr->exp_modifier[type] = rate;
			duty_change();
		}
		else
		{
			duty_update();
		}
	}
	else
	{
		if( on_duty )
		{
			g_activity_mgr->exp_modifier[type] = 1.f;
			duty_change();
		}
	}
}

void activity_mgr::spawn_config_t::duty( bool on )
{
	if( on )
	{
		if( !on_duty )
		{
			MapMgr* mgr = sInstanceMgr.GetMapMgr( mapid );
			if( mgr )
				mgr->SpawnActivityCreature( spawnid, npcid, true, is_creature );
			duty_change();
		}
		else
			duty_update();
	}
	else
	{
		if( on_duty )
		{
			MapMgr* mgr = sInstanceMgr.GetMapMgr( mapid );
			if( mgr )
				mgr->DespawnActivityCreature( npcid, is_creature );
			duty_change();
		}
	}
}

void activity_mgr::bless_config_t::duty( bool on )
{
	if( on )
	{
		if( !on_duty )
		{
			g_activity_mgr->bless_exp_rate = bonus_exp_rate;
			g_activity_mgr->bless_exp_interval_minutes = interval_minutes;
			duty_change();
		}
		else
			duty_update();
	}
	else
	{
		if( on_duty )
		{
			g_activity_mgr->bless_exp_rate = 0.f;
			g_activity_mgr->bless_exp_interval_minutes = 0;
			g_activity_mgr->last_bless_time = (uint32)UNIXTIME;
			duty_change();
		}
	}
}

void activity_mgr::srvshutdown_config_t::duty( bool on )
{
	if( on )
	{
		if( !on_duty )
		{
			sMaster.SetShutdownTimer( seconds );
			duty_change();
		}
		else
			duty_update();
	}
	else
	{
		if( on_duty )
		{
			duty_change();
		}
	}
}

void activity_mgr::donation_event_config_t::duty( bool on )
{
	if( on )
	{
		if( !on_duty )
		{
			//sMaster.SetShutdownTimer( seconds );
			duty_change();
		}
		else
			duty_update();
	}
	else
	{
		if( on_duty )
		{
			duty_change();
		}
	}
}

void activity_mgr::treasure_event_config_t::duty( bool on )
{
	if( on )
	{
		if( !on_duty )
		{
			duty_change();
		}
		else
			duty_update();
	}
	else
	{
		if( on_duty )
		{
			duty_change();
		}
	}
}
void activity_mgr::shop_event_config_t::duty( bool on )
{
	if( on )
	{
		if( !on_duty )
		{
			duty_change();
		}
		else
			duty_update();
	}
	else
	{
		if( on_duty )
		{
			duty_change();
		}
	}
}
void activity_mgr::load()
{
	load_exp_configurations();
	load_spawn_configurations();
	load_bless_configurations();
	load_srvshutdown_configurations();
	load_donation_configurations();
	load_treasure_configurations();
	load_shop_configurations();
}

void activity_mgr::load_exp_configurations()
{
	smart_ptr<QueryResult> qr = RealmDatabase.Query( "select * from activity_exp_configuration" );
	if( !qr )
		return;
	do
	{
		Field* f = qr->Fetch();
		exp_config_t* act = new exp_config_t;
		act->load( f );
		add_activity( act ,true);
	}
	while( qr->NextRow() );
}

void activity_mgr::load_spawn_configurations()
{
	smart_ptr<QueryResult> qr = RealmDatabase.Query( "select * from activity_spawn_configuration" );
	if( !qr )
		return;

	do
	{
		Field* f = qr->Fetch();
		spawn_config_t* act = new spawn_config_t;
		act->load( f );
		add_activity( act , true);
	}
	while( qr->NextRow() );
}

void activity_mgr::load_bless_configurations()
{
	smart_ptr<QueryResult> qr = RealmDatabase.Query( "select * from activity_bless_configuration" );
	if( !qr )
		return;

	do
	{
		Field* f = qr->Fetch();
		bless_config_t* act = new bless_config_t;
		act->load( f );
		add_activity( act , true);
	}
	while( qr->NextRow() );
}

void activity_mgr::load_srvshutdown_configurations()
{
	smart_ptr<QueryResult> qr = RealmDatabase.Query( "select * from activity_srvshutdown_configuration" );
	if( !qr )
		return;

	do
	{
		Field* f = qr->Fetch();
		srvshutdown_config_t* act = new srvshutdown_config_t;
		act->load( f );
		add_activity( act, true );
	}
	while( qr->NextRow() );
}
void activity_mgr::load_shop_configurations()
{
	smart_ptr<QueryResult> qr = RealmDatabase.Query( "select * from activity_shop_configuration" );
	if( !qr )
		return;

	do
	{
		Field* f = qr->Fetch();
		shop_event_config_t* act = new shop_event_config_t;
		act->load( f );
		add_activity( act ,true);
	}
	while( qr->NextRow() );
}
void activity_mgr::load_treasure_configurations()
{
	smart_ptr<QueryResult> qr = RealmDatabase.Query( "select * from activity_treasure_configuration" );
	if( !qr )
		return;

	do
	{
		Field* f = qr->Fetch();
		treasure_event_config_t* act = new treasure_event_config_t;
		act->load( f );
		add_activity( act ,true);
	}
	while( qr->NextRow() );
}
void activity_mgr::load_donation_configurations()
{
	smart_ptr<QueryResult> qr = RealmDatabase.Query( "select * from activity_donation_event_configuration" );
	if( !qr )
		return;

	do
	{
		Field* f = qr->Fetch();
		donation_event_config_t* act = new donation_event_config_t;
		act->load( f );
		add_activity( act , true);
	}
	while( qr->NextRow() );
}

void activity_mgr::process_exp_bonus()
{
	for( activity_map_type::iterator it = m_activities[ACTIVITY_CATEGORY_EXP].begin(); it != m_activities[ACTIVITY_CATEGORY_EXP].end(); ++it )
	{
		exp_config_t* ec = static_cast<exp_config_t*>( it->second );
		ec->duty( ec->is_time2duty( (uint32)UNIXTIME ) );
	}
}

void activity_mgr::process_spawn_npc()
{
	for( activity_map_type::iterator it = m_activities[ACTIVITY_CATEGORY_SPAWN].begin(); it != m_activities[ACTIVITY_CATEGORY_SPAWN].end(); ++it )
	{
		spawn_config_t* sc = static_cast<spawn_config_t*>( it->second );
		sc->duty( sc->is_time2duty( (uint32)UNIXTIME ) );
	}
}

void activity_mgr::process_bless_bonus()
{
	for( activity_map_type::iterator it = m_activities[ACTIVITY_CATEGORY_BLESS].begin(); it != m_activities[ACTIVITY_CATEGORY_BLESS].end(); ++it )
	{
		bless_config_t* bc = static_cast<bless_config_t*>( it->second );
		bc->duty( bc->is_time2duty( (uint32)UNIXTIME ) );
	}
	if( bless_exp_interval_minutes > 0 && (uint32)UNIXTIME - last_bless_time > bless_exp_interval_minutes * 60 )
	{
		objmgr.BlessBonusExp( bless_exp_rate );
		last_bless_time = (uint32)UNIXTIME;
	}
}

void activity_mgr::process_shop()
{
	for( activity_map_type::iterator it = m_activities[ACTIVITY_CATEGORY_SHOP].begin(); it != m_activities[ACTIVITY_CATEGORY_SHOP].end(); ++it )
	{
		bless_config_t* bc = static_cast<bless_config_t*>( it->second );
		bc->duty( bc->is_time2duty( (uint32)UNIXTIME ) );
	}
}
void activity_mgr::process_server_shutdown()
{
	for( activity_map_type::iterator it = m_activities[ACTIVITY_CATEGORY_SRVSHUTDOWN].begin(); it != m_activities[ACTIVITY_CATEGORY_SRVSHUTDOWN].end(); ++it )
	{
		srvshutdown_config_t* sc = static_cast<srvshutdown_config_t*>( it->second );
		sc->duty( sc->is_time2duty( (uint32)UNIXTIME ) );
	}
}
void activity_mgr::process_treasure()
{
	for( activity_map_type::iterator it = m_activities[ACTIVITY_CATEGORY_TREASURE].begin(); it != m_activities[ACTIVITY_CATEGORY_TREASURE].end(); ++it )
	{
		treasure_event_config_t* sc = static_cast<treasure_event_config_t*>( it->second );
		sc->duty( sc->is_time2duty( (uint32)UNIXTIME ) );
	}
}
void activity_mgr::process_donation_event()
{
	for( activity_map_type::iterator it = m_activities[ACTIVITY_CATEGORY_DONATION_EVENT].begin(); it != m_activities[ACTIVITY_CATEGORY_DONATION_EVENT].end(); ++it )
	{
		donation_event_config_t* sc = static_cast<donation_event_config_t*>( it->second );
		sc->duty( sc->is_time2duty( (uint32)UNIXTIME ) );
	}
}

#endif

void activity_mgr::update()
{
#ifndef _ACTIVITY_TOOLS
	process_bless_bonus();
	process_spawn_npc();
	process_exp_bonus();
	process_server_shutdown();
	process_donation_event();
	process_treasure();
	process_shop();
#endif
	m_listener->on_upate();
}

static char buff[MAX_BUF_SIZE];

void activity_mgr::build_packet( CPacketSn& cps )
{
	CPacketSn packet;
	packet << m_totalact.size();
	for( std::map<uint32, activity_base_t*>::iterator it = m_totalact.begin(); it != m_totalact.end(); ++it )
	{
		activity_base_t* act = it->second;
		act->build_packet( packet );
	}
	zip_compress_strategy zcs;
	uint32 len = sizeof( buff );
	if( zcs.compress( buff, &len, packet.GetBuf(), packet.GetSize() ) )
	{
		if( len < 65000 )
		{
			cps.AddBuf( buff, len );
		}
		else
			assert( 0 );
	}
	else
		assert( 0 );
}

void activity_mgr::from_packet( CPacketUsn& cpu )
{
	clear_all();
	
	zip_compress_strategy zcs;

	uint32 len = sizeof( buff );
	if( zcs.uncompress( buff, &len, cpu.m_buff + MESSAGE_HEAD_LEN, cpu.m_size - MESSAGE_HEAD_LEN ) )
	{
		CPacketUsn packet( buff, len );
		uint32 size = 0;
		packet >> size;
		for( uint32 i = 0; i < size; ++i )
		{
			activity_base_t* act = create_act_from_packet( packet );
			if( act )
				add_activity( act );
		}
	}
	else
	{
		assert( 0 );
		return;
	}
}

void activity_mgr::clear_all()
{
	for( std::map<uint32, activity_base_t*>::iterator it = m_totalact.begin(); it != m_totalact.end(); ++it )
		delete it->second;

	m_totalact.clear();
}

void activity_mgr::update_activity( activity_mgr::activity_base_t* act )
{
	//这里需要直接更新
	activity_base_t* p = find_activity( act->id );
	if( p )
	{
		if (p->category != act->category)
		{
			return ;
		}
		p->category = act->category;
		p->id = act->id;
		p->starttime = act->starttime;
		p->expire = act->expire;
		p->starthour = act->starthour;
		p->expirehour = act->expirehour;
		p->repeat_dayofweek[0] = act->repeat_dayofweek[0];
		p->repeat_dayofweek[1] = act->repeat_dayofweek[1];
		p->repeat_dayofweek[2] = act->repeat_dayofweek[2];
		p->repeat_dayofweek[3] = act->repeat_dayofweek[3];
		p->repeat_dayofweek[4] = act->repeat_dayofweek[4];
		p->repeat_dayofweek[5] = act->repeat_dayofweek[5];
		p->repeat_dayofweek[6] = act->repeat_dayofweek[6];
		p->description[0] = act->description[0];
		p->description[1] = act->description[1];
		p->comment = act->comment;
		p->on_duty = act->on_duty;
		p->attach = act->attach;
		p->next_broadcast_interval = act->next_broadcast_interval;

		if (p->category == ACTIVITY_CATEGORY_EXP)
		{
			activity_mgr::exp_config_t* expp1 = static_cast<activity_mgr::exp_config_t*>( p );
			activity_mgr::exp_config_t* expp2 = static_cast<activity_mgr::exp_config_t*>( act );
			expp1->rate = expp2->rate;
			expp1->type = expp2->type;
		}

		if (p->category == ACTIVITY_CATEGORY_BLESS)
		{
			activity_mgr::bless_config_t* bp1 = static_cast<activity_mgr::bless_config_t*>( p );
			activity_mgr::bless_config_t* bp2 = static_cast<activity_mgr::bless_config_t*>( act );

			bp1->bonus_exp_rate = bp2->bonus_exp_rate;
			bp1->interval_minutes = bp2->interval_minutes;
		}

		if (p->category == ACTIVITY_CATEGORY_SPAWN)
		{
			activity_mgr::spawn_config_t * spp1 =  static_cast<activity_mgr::spawn_config_t*>( p );
			activity_mgr::spawn_config_t * spp2 =  static_cast<activity_mgr::spawn_config_t*>( act );

			spp1->spawnid = spp2->spawnid;
			spp1->mapid = spp2->mapid;
			spp1->respawn = spp2->respawn;
			spp1->is_creature = spp2->is_creature;
			spp1->npcid = spp2->npcid;
			
		}

		if (p->category == ACTIVITY_CATEGORY_SRVSHUTDOWN)
		{
			activity_mgr::srvshutdown_config_t * shp1 =  static_cast<activity_mgr::srvshutdown_config_t*>( p );
			activity_mgr::srvshutdown_config_t * shp2 =  static_cast<activity_mgr::srvshutdown_config_t*>( act );
			
			shp1->seconds = shp2->seconds;
		}

		if (p->category == ACTIVITY_CATEGORY_DONATION_EVENT)
		{
			activity_mgr::donation_event_config_t * dap1 =  static_cast<activity_mgr::donation_event_config_t*>( p );
			activity_mgr::donation_event_config_t * dap2 =  static_cast<activity_mgr::donation_event_config_t*>( act );

			dap1->event_id = dap2->event_id;
			dap1->event_photo_count = dap2->event_photo_count;
			dap1->event_title = dap2->event_title;
			
		}
		
		if (p->category == ACTIVITY_CATEGORY_TREASURE)
		{
			activity_mgr::treasure_event_config_t* dap1 = static_cast<activity_mgr::treasure_event_config_t*>( p);
			activity_mgr::treasure_event_config_t* dap2 = static_cast<activity_mgr::treasure_event_config_t*>( act);

			dap1->treasureid = dap2->treasureid;
			dap1->treasurecount = dap2->treasurecount;
			dap1->refurbishtime = dap2->refurbishtime;
			dap1->treasureitemcount = dap2->treasureitemcount;
			dap1->treasureposstart = dap2->treasureposstart;
			dap1->treasureposend = dap2->treasureposend;
			dap1->treasurescale = dap2->treasurescale;
			dap1->lockitemtype = dap2->lockitemtype;
		}
		if (p->category == ACTIVITY_CATEGORY_SHOP)
		{
			activity_mgr::shop_event_config_t* dap1 = static_cast<activity_mgr::shop_event_config_t*>( p);
			activity_mgr::shop_event_config_t* dap2 = static_cast<activity_mgr::shop_event_config_t*>( act);

			dap1->shop_item_category =  dap2->shop_item_category;
			dap1->discount = dap2->discount;
		}
		if (m_listener)
		{
			m_listener->on_upate(p);
		}
	}else
	{
		add_activity(act);	
	}
		//remove_activity( p->id );

	//add_activity( act );
}

void activity_mgr::add_activity( activity_mgr::activity_base_t* act, bool fromdb )
{
	if( !m_listener->on_add_activity( act , fromdb) )
		return;

	m_totalact[act->id] = act;
	m_activities[act->category][act->id] = act;
}

void activity_mgr::remove_activity( activity_mgr::activity_base_t* act )
{
	activity_map_type::iterator it = m_activities[act->category].find( act->id );
	if( it != m_activities[act->category].end() )
	{
		if( m_listener->on_remove_activity( act ) )
		{
			m_activities[act->category].erase( it );
			m_totalact.erase( act->id );
			delete act;
		}
	}
}

void activity_mgr::remove_activity( uint32 id )
{
	std::map<uint32, activity_base_t*>::iterator it = m_totalact.find( id );
	if( it != m_totalact.end() )
	{
		activity_base_t* act = it->second;
		if( m_listener->on_remove_activity( act ) )
		{
			m_activities[act->category].erase( id );
			m_totalact.erase( it );
			delete act;
		}
	}
}

activity_mgr::activity_base_t* activity_mgr::create_act_from_packet( CPacketUsn& cpu )
{
	uint32 category;
	cpu >> category;
	if( category >= ACTIVITY_CATEGORY_MAX )
		return NULL;
	activity_base_t* p = create_act_from_category( (activity_mgr::activity_category)category );
	if( p )
		p->from_packet( cpu );

	return p;
}

activity_mgr::activity_base_t* activity_mgr::create_act_from_category( activity_mgr::activity_category ac )
{
	activity_base_t* p = NULL;
	switch( ac )
	{
	case ACTIVITY_CATEGORY_EXP: p = new exp_config_t; break;
	case ACTIVITY_CATEGORY_SPAWN: p = new spawn_config_t; break;
	case ACTIVITY_CATEGORY_BLESS: p = new bless_config_t; break;
	case ACTIVITY_CATEGORY_SRVSHUTDOWN: p = new srvshutdown_config_t; break;
	case ACTIVITY_CATEGORY_DONATION_EVENT: p = new donation_event_config_t; break;
	case ACTIVITY_CATEGORY_TREASURE: p = new  treasure_event_config_t; break;
	case ACTIVITY_CATEGORY_SHOP: p = new shop_event_config_t ;break ;
	default: return NULL;
	}
	p->category = ac;
	return p;
}

activity_mgr::activity_base_t* activity_mgr::find_activity( uint32 id )
{
	std::map<uint32, activity_base_t*>::iterator it = m_totalact.find( id );
	if( it != m_totalact.end() )
		return it->second;
	else
		return NULL;
}

const char* activity_mgr::get_category_text( activity_mgr::activity_category ac )
{
#define make_enum_txt( x ) case x: return #x; break
	switch( ac )
	{
	make_enum_txt( ACTIVITY_CATEGORY_EXP );
	make_enum_txt( ACTIVITY_CATEGORY_SPAWN );
	make_enum_txt( ACTIVITY_CATEGORY_BLESS );
	make_enum_txt( ACTIVITY_CATEGORY_SRVSHUTDOWN );
	make_enum_txt( ACTIVITY_CATEGORY_DONATION_EVENT );
	make_enum_txt( ACTIVITY_CATEGORY_TREASURE);
	make_enum_txt( ACTIVITY_CATEGORY_SHOP);
	default:
		return "";
	}
#undef make_enum_txt
}

//////////////////////////////////////////////////////////////////////////
namespace MSG_S2C {

CPacketSn& stActivityListAck::Sn(CPacketSn& cps) const {
		PakHead::Sn(cps);
		g_activity_mgr->build_packet( cps );
		return cps;
}

CPacketUsn& stActivityListAck::Usn(CPacketUsn& cpu)    {
		PakHead::Usn(cpu);
		g_activity_mgr->from_packet( cpu );
		return cpu;
}

//-------

CPacketSn& stUpdateActivityAck::Sn(CPacketSn& cps) const {
	PakHead::Sn(cps);
	reinterpret_cast<activity_mgr::activity_base_t*>( act_ptr )->build_packet( cps );
	return cps;
}

CPacketUsn& stUpdateActivityAck::Usn(CPacketUsn& cpu)    {
	PakHead::Usn(cpu);
	act_ptr = (uint32)g_activity_mgr->create_act_from_packet( cpu );
	if( act_ptr )
		g_activity_mgr->update_activity( reinterpret_cast<activity_mgr::activity_base_t*>( act_ptr ) );
	return cpu;
}

//-------

CPacketSn& stRemoveActivityAck::Sn(CPacketSn& cps) const{
	PakHead::Sn(cps); cps << actid; return cps;
}

CPacketUsn& stRemoveActivityAck::Usn(CPacketUsn& cpu)    {
	PakHead::Usn(cpu);cpu >> actid; g_activity_mgr->remove_activity( actid ); return cpu;
}

//-------

CPacketSn& stDutyChangeNotify::Sn(CPacketSn& cps) const {
	PakHead::Sn(cps); cps << actid << on_duty; return cps;
}

CPacketUsn& stDutyChangeNotify::Usn(CPacketUsn& cpu)    {
	PakHead::Usn(cpu);cpu >> actid >> on_duty; 
	g_activity_mgr->m_listener->on_duty_change( g_activity_mgr->find_activity( actid ) );
	return cpu;
}

} // end of namespace MSG_S2C

//////////////////////////////////////////////////////////////////////////

namespace MSG_C2S{

CPacketSn& stUpdateActivityReq::Sn(CPacketSn& cps) const {
	PakHead::Sn(cps);
	reinterpret_cast<activity_mgr::activity_base_t*>( act_ptr )->build_packet( cps );
	return cps;
}
CPacketUsn& stUpdateActivityReq::Usn(CPacketUsn& cpu) {
	PakHead::Usn(cpu);
	activity_mgr::activity_base_t* p = g_activity_mgr->create_act_from_packet( cpu );
	p->on_duty = 0;
	act_ptr = reinterpret_cast<uint32>( p );
	if( act_ptr )
		g_activity_mgr->update_activity( reinterpret_cast<activity_mgr::activity_base_t*>( act_ptr ) );
	return cpu;
}

} // end of namespace MSG_C2S
//////////////////////////////////////////////////////////////////////////