#ifndef _ACTIVITY_HEAD
#define _ACTIVITY_HEAD

#include "../../new_common/Source/utilities/utilities.h"

#ifndef _ACTIVITY_TOOLS
#include "MainServerDefines.h"
#endif

enum
{
	 BROADCAST_0 = 10 * 60,
	 BROADCAST_1 = 30 * 60,
	 BROADCAST_2 =  3600,
	 BROADCAST_3 = 3 * 3600,
	 BROADCAST_4 = 5 *3600,
	 BROADCAST_5 = 7 * 3600,
	 BROADCAST_6 = 12 * 3600,
};
class activity_mgr
{
public:
	struct activity_base_t;

	class activity_listener_base
	{
	public:
		virtual bool on_add_activity( activity_base_t* p, bool fromdb ) = 0;
		virtual bool on_remove_activity( activity_base_t* p ) = 0;
		virtual bool on_upate(activity_base_t* p) = 0;
		virtual void on_duty_change( activity_base_t* p ) = 0;
		virtual void on_upate() = 0;
		virtual void on_duty_update( activity_base_t* p ) = 0;
	};

public:
	activity_mgr();
	virtual ~activity_mgr();

#ifndef _ACTIVITY_TOOLS
	void load();

	float exp_modifier[GIVE_EXP_TYPE_COUNT];

	float bless_exp_rate;
	uint32 bless_exp_interval_minutes;
	uint32 last_bless_time;

	void process_bless_bonus();
	void process_spawn_npc();
	void process_exp_bonus();
	void process_server_shutdown();
	void process_donation_event();
	void process_treasure();
	void process_shop();

	WorldSession* tool_session;

private:
	void load_exp_configurations();
	void load_spawn_configurations();
	void load_bless_configurations();
	void load_srvshutdown_configurations();
	void load_donation_configurations();
	void load_treasure_configurations();
	void load_shop_configurations();

#endif
public:
	enum activity_category
	{
		ACTIVITY_CATEGORY_EXP,
		ACTIVITY_CATEGORY_SPAWN,
		ACTIVITY_CATEGORY_BLESS,
		ACTIVITY_CATEGORY_SRVSHUTDOWN,
		ACTIVITY_CATEGORY_DONATION_EVENT,
		ACTIVITY_CATEGORY_TREASURE ,
		ACTIVITY_CATEGORY_SHOP,
		ACTIVITY_CATEGORY_MAX,
	};

	virtual void update();
	void build_packet( CPacketSn& cps );
	void from_packet( CPacketUsn& cpu );
	void clear_all();
	void update_activity( activity_base_t* act );
	void add_activity( activity_base_t* act, bool fromdb = false );
	void remove_activity( activity_base_t* act );
	void remove_activity( uint32 id );
	activity_base_t* create_act_from_packet( CPacketUsn& cpu );
	activity_base_t* create_act_from_category( activity_category ac );
	activity_base_t* find_activity( uint32 id );
	const char* get_category_text( activity_category ac );
	
	struct activity_base_t
	{
		uint32 category;
		uint32 id;
		uint32 starttime;
		uint32 expire;
		uint8 starthour;
		uint8 expirehour;
		uint8 repeat_dayofweek[7];
		std::string description[2];
		std::string comment;
		uint8 on_duty;
		uint32 attach;
		uint32 next_broadcast_interval;
		void duty_change();
		void duty_update();

		activity_base_t()
			: category( 0 ), id( 0 ), starttime( 0 ), expire( 0 ), starthour( 0 ), expirehour( 0 ), on_duty( 0 ), attach( 0 ), next_broadcast_interval( 600 )
		{
			memset( repeat_dayofweek, 0, sizeof( repeat_dayofweek ) );
		}
		bool is_time2duty( uint32 now )
		{
			if( starttime && expire )
			{
				if( now >= starttime && now <= expire )
					return true;
			}
			for( int i = 0; i < 7; ++i )
				if( repeat_dayofweek[i] && is_dayofweek( now, i ) && in_duration( now, starthour, expirehour ) )
					return true;

			return false;
		}
		virtual void build_packet( CPacketSn& cps )
		{
			cps << category << id << starttime << expire << starthour << expirehour
				<< repeat_dayofweek[0] << repeat_dayofweek[1] << repeat_dayofweek[2]
				<< repeat_dayofweek[3] << repeat_dayofweek[4] << repeat_dayofweek[5] << repeat_dayofweek[6]
				<< description[0] << description[1] << comment << next_broadcast_interval << on_duty ;

		}
		virtual void from_packet( CPacketUsn& cpu )
		{
			cpu >> id >> starttime >> expire >> starthour >> expirehour
				>> repeat_dayofweek[0] >> repeat_dayofweek[1] >> repeat_dayofweek[2]
				>> repeat_dayofweek[3] >> repeat_dayofweek[4] >> repeat_dayofweek[5] >> repeat_dayofweek[6]
				>> description[0] >> description[1] >> comment >> next_broadcast_interval >> on_duty ;
		}
#ifndef _ACTIVITY_TOOLS
		virtual bool save() = 0;
		virtual bool remove() = 0;
		virtual void load( Field* f )
		{
			id = f[0].GetUInt32();
			starttime = f[1].GetUInt32();
			expire = f[2].GetUInt32();
			starthour = f[3].GetUInt32();
			expirehour = f[4].GetUInt32();
			uint32 tmp[7];
			sscanf( f[5].GetString(), "%u %u %u %u %u %u %u", &tmp[0], &tmp[1], &tmp[2], &tmp[3], &tmp[4], &tmp[5], &tmp[6] );
			for( int i = 0; i < 7; ++i )
				repeat_dayofweek[i] = tmp[i];
			description[0] = f[6].GetString();
			description[1] = f[7].GetString();
			comment = f[8].GetString();
			next_broadcast_interval = f[9].GetUInt32();
		}
		virtual void duty( bool on ) = 0;
#endif
	};
#define BASE_INFO_FORMAT "%u, %u, %u, %u, %u, '%u %u %u %u %u %u %u', '%s', '%s', '%s', %u"
#define BASE_INFO_DATA id, starttime, expire, starthour, expirehour, repeat_dayofweek[0], repeat_dayofweek[1], repeat_dayofweek[2], repeat_dayofweek[3], repeat_dayofweek[4], repeat_dayofweek[5], repeat_dayofweek[6], description[0].c_str(), description[1].c_str(), comment.c_str(), next_broadcast_interval
	struct exp_config_t : public activity_base_t
	{
		exp_config_t()
		{
			category = ACTIVITY_CATEGORY_EXP;
		}
		uint32 type;
		float rate;
		virtual void build_packet( CPacketSn& cps )
		{
			activity_base_t::build_packet( cps );
			cps << type << rate;
		}
		virtual void from_packet( CPacketUsn& cpu )
		{
			activity_base_t::from_packet( cpu );
			cpu >> type >> rate;
		}
#ifndef _ACTIVITY_TOOLS
		virtual bool save()
		{
			return RealmDatabase.WaitExecute( 
				"replace into activity_exp_configuration values( "BASE_INFO_FORMAT", %u, %.2f)", BASE_INFO_DATA, type, rate );
		}
		virtual bool remove()
		{
			return RealmDatabase.WaitExecute( "delete from activity_exp_configuration where activityid = %u", id );
		}
		virtual void load( Field* f )
		{
			activity_base_t::load( f );
			type = f[10].GetUInt32();
			rate = f[11].GetFloat();
		}
		virtual void duty( bool on );
#endif
	};
	struct spawn_config_t : public activity_base_t
	{
		spawn_config_t() : spawnid( 0 ), mapid( 0 ), respawn( 0 ), npcid( 0 ), is_creature( 0 )
		{
			category = ACTIVITY_CATEGORY_SPAWN;
		}
		uint32 spawnid;
		uint32 mapid;
		uint32 respawn;
		uint8 is_creature;

		uint32 npcid;
		virtual void build_packet( CPacketSn& cps )
		{
			activity_base_t::build_packet( cps );
			cps << spawnid << mapid << respawn << npcid << is_creature;
		}
		virtual void from_packet( CPacketUsn& cpu )
		{
			activity_base_t::from_packet( cpu );
			cpu >> spawnid >> mapid >> respawn >> npcid >> is_creature;
		}
#ifndef _ACTIVITY_TOOLS
		virtual bool save()
		{
			return RealmDatabase.WaitExecute( "replace into activity_spawn_configuration values("BASE_INFO_FORMAT", %u, %u, %u)", BASE_INFO_DATA, spawnid, mapid, ( (!is_creature << 1) | respawn ) );
		}
		virtual bool remove()
		{
			return RealmDatabase.WaitExecute( "delete from activity_spawn_configuration where activityid = %u", id );
		}
		virtual void load( Field* f )
		{
			activity_base_t::load( f );
			spawnid = f[10].GetUInt32();
			mapid = f[11].GetUInt32();
			uint32 tmp = f[12].GetUInt32();
			is_creature = ( tmp & 2 ) > 0 ? 0 : 1;
			respawn = tmp & 1;
		}
		virtual void duty( bool on );
#endif
	};

	struct bless_config_t : public activity_base_t
	{
		float bonus_exp_rate;
		uint16 interval_minutes;
		bless_config_t()
		{
			category = ACTIVITY_CATEGORY_BLESS;
		}

		virtual void build_packet( CPacketSn& cps )
		{
			activity_base_t::build_packet( cps );
			cps << bonus_exp_rate << interval_minutes;
		}
		virtual void from_packet( CPacketUsn& cpu )
		{
			activity_base_t::from_packet( cpu );
			cpu >> bonus_exp_rate >> interval_minutes;
		}
#ifndef _ACTIVITY_TOOLS
		virtual bool save()
		{
			return RealmDatabase.WaitExecute( "replace into activity_bless_configuration values("BASE_INFO_FORMAT", %.2f, %u )", BASE_INFO_DATA, bonus_exp_rate, interval_minutes );
		}
		virtual bool remove()
		{
			return RealmDatabase.WaitExecute( "delete from activity_spawn_configuration where activityid = %u", id );
		}
		virtual void load( Field* f )
		{
			activity_base_t::load( f );
			bonus_exp_rate = f[10].GetFloat();
			interval_minutes = f[11].GetUInt32();
		}
		virtual void duty( bool on );
#endif
	};

	struct srvshutdown_config_t : public activity_base_t
	{
		uint32 seconds;
		srvshutdown_config_t()
		{
			category = ACTIVITY_CATEGORY_SRVSHUTDOWN;
		}

		virtual void build_packet( CPacketSn& cps )
		{
			activity_base_t::build_packet( cps );
			cps << seconds;
		}
		virtual void from_packet( CPacketUsn& cpu )
		{
			activity_base_t::from_packet( cpu );
			cpu >> seconds;
		}
#ifndef _ACTIVITY_TOOLS
		virtual bool save()
		{
			return RealmDatabase.WaitExecute( "replace into activity_srvshutdown_configuration values("BASE_INFO_FORMAT", %u )", BASE_INFO_DATA, seconds );
		}
		virtual bool remove()
		{
			return RealmDatabase.WaitExecute( "delete from activity_srvshutdown_configuration where activityid = %u", id );
		}
		virtual void load( Field* f )
		{
			activity_base_t::load( f );
			seconds = f[10].GetFloat();
		}
		virtual void duty( bool on );
#endif
	};

	struct donation_event_config_t : public activity_base_t
	{
		uint32 event_id;
		uint32 event_photo_count;
		std::string event_title;
		donation_event_config_t()
		{
			category = ACTIVITY_CATEGORY_DONATION_EVENT;
		}

		virtual void build_packet( CPacketSn& cps )
		{
			activity_base_t::build_packet( cps );
			cps << event_id << event_title << event_photo_count ;	
		}
		virtual void from_packet( CPacketUsn& cpu )
		{
			activity_base_t::from_packet( cpu );
			uint32 p_count = 0;
			cpu >> event_id >> event_title >> event_photo_count;
		}
#ifndef _ACTIVITY_TOOLS
		virtual bool save()
		{
			return RealmDatabase.WaitExecute( "replace into activity_donation_event_configuration values("BASE_INFO_FORMAT", %u, '%s', '%u')", BASE_INFO_DATA,
												event_id, event_title.c_str(), event_photo_count );
		}
		virtual bool remove()
		{
			return RealmDatabase.WaitExecute( "delete from activity_donation_event_configuration where activityid = %u", id );
		}
		virtual void load( Field* f )
		{
			activity_base_t::load( f );
			event_id = f[10].GetUInt32();
			event_title = f[11].GetString();
			event_photo_count = f[12].GetUInt32();
		}
		virtual void duty( bool on );
#endif
	}; 

	struct treasure_event_config_t : public activity_base_t
	{
		ui32 treasureid ;  //物品ID
		ui32 treasurecount; //单次刷新数量
		ui32 refurbishtime; //刷新时间
		ui32 treasureitemcount; //宝箱最多刷新次数
		ui32 treasureposstart; //位置数量
		ui32 treasureposend;
		float treasurescale; //
		ui8 lockitemtype; //锁定刷新数量 true

		treasure_event_config_t()
		{
			category = ACTIVITY_CATEGORY_TREASURE;
		}

		virtual void build_packet( CPacketSn& cps )
		{
			activity_base_t::build_packet( cps );
			cps << treasureid << treasurecount << refurbishtime << treasureposstart << treasureposend <<treasureitemcount << treasurescale << lockitemtype ;	
		}
		virtual void from_packet( CPacketUsn& cpu )
		{
			activity_base_t::from_packet( cpu );
			uint32 p_count = 0;
			cpu >> treasureid >> treasurecount >> refurbishtime >> treasureposstart >> treasureposend >> treasureitemcount >> treasurescale >> lockitemtype;
		}


#ifndef _ACTIVITY_TOOLS
		virtual bool save()
		{
			return RealmDatabase.WaitExecute( "replace into activity_treasure_configuration values("BASE_INFO_FORMAT", %u, '%u', '%u', '%u', '%u', '%u', '%f', '%u')", BASE_INFO_DATA,
				treasureid, treasureposstart, treasureposend, treasurecount,  refurbishtime, treasureitemcount, treasurescale, lockitemtype);
		}
		virtual bool remove()
		{
			return RealmDatabase.WaitExecute( "delete from activity_treasure_configuration where activityid = %u", id );
		}
		virtual void load( Field* f )
		{
			activity_base_t::load( f );
			treasureid = f[10].GetUInt32();
			treasureposstart  = f[11].GetUInt32();
			treasureposend = f[12].GetUInt32();
			treasurecount = f[13].GetUInt32();
			refurbishtime = f[14].GetUInt32();
			treasureitemcount = f[15].GetUInt32();
			treasurescale = f[16].GetFloat();
			lockitemtype = f[17].GetUInt8();
		}
		virtual void duty( bool on );
#endif
	};
	
	struct shop_event_config_t : public activity_base_t
	{
		ui8 shop_item_category ;
		ui32 discount ;
		shop_event_config_t()
		{
			category = ACTIVITY_CATEGORY_SHOP;
		}

		virtual void build_packet( CPacketSn& cps )
		{
			activity_base_t::build_packet( cps );
			cps << shop_item_category << discount  ;	
		}
		virtual void from_packet( CPacketUsn& cpu )
		{
			activity_base_t::from_packet( cpu );
			uint32 p_count = 0;
			cpu >> shop_item_category >> discount ;
		}

#ifndef _ACTIVITY_TOOLS
		virtual bool save()
		{
			return RealmDatabase.WaitExecute( "replace into activity_shop_configuration values("BASE_INFO_FORMAT", %u,  %u)", BASE_INFO_DATA,
				shop_item_category, discount );
		}
		virtual bool remove()
		{
			return RealmDatabase.WaitExecute( "delete from activity_shop_configuration where activityid = %u", id );
		}
		virtual void load( Field* f )
		{
			activity_base_t::load( f );
			shop_item_category = f[10].GetUInt32();
			discount = f[11].GetUInt32();
		}
		virtual void duty( bool on );
#endif
	};
	activity_listener_base* m_listener;

private:
	typedef std::map<uint32, activity_base_t*> activity_map_type;
	activity_map_type m_activities[ACTIVITY_CATEGORY_MAX];
	activity_map_type m_totalact;
};

extern activity_mgr* g_activity_mgr;


#endif // _ACTIVITY_HEAD