#include "StdAfx.h"
#include "activity_srv.h"
#include "../../SDBase/Protocol/C2S.h"
#include "DonationSystem.h"
#include "TreasureMgr.h"
#include "ShopActivity.h"
#include "BroadTextMgr.h"

activity_srv* g_activity_srv = NULL;

activity_srv::activity_srv()
{
	m_listener = new srv_listener;

	g_activity_mgr->m_listener = m_listener;
}

activity_srv::~activity_srv()
{
	delete m_listener;
}

bool activity_srv::srv_listener::on_upate(activity_mgr::activity_base_t* p)
{
	if( p->save() )
	{
		if( g_activity_mgr->tool_session )
		{
			MSG_S2C::stUpdateActivityAck ack;
			ack.SetPtr( p );
			g_activity_mgr->tool_session->SendPacket( ack );
		}
		if( p->category == activity_mgr::ACTIVITY_CATEGORY_DONATION_EVENT )
		{
			activity_mgr::donation_event_config_t* evt = static_cast<activity_mgr::donation_event_config_t*>( p );
			g_donation_system->UpdateDonationEvent( evt->event_id, evt->starttime, evt->expire,evt->event_photo_count);
		}

		if (p->category == activity_mgr::ACTIVITY_CATEGORY_TREASURE)
		{
			activity_mgr::treasure_event_config_t* evt = static_cast<activity_mgr::treasure_event_config_t*>( p);
			g_treasure_mgr->OnUpdateTreasureData(evt);
		}
		if (p->category == activity_mgr::ACTIVITY_CATEGORY_SHOP)
		{
			activity_mgr::shop_event_config_t* evt = static_cast<activity_mgr::shop_event_config_t*>( p);
			//update
			g_shopactivity_system->UpdateShopActivity(evt);
		}
		g_broadTextMgr_system->AddActivityText(p);
		return true;
	}

	return false ;
}
bool activity_srv::srv_listener::on_add_activity( activity_mgr::activity_base_t* p ,bool fromdb )
{
	bool saveed = true;
	if (!fromdb)
	{
		if (!p->save())
			saveed = false;
	}
	if( saveed )
	{
		if( g_activity_mgr->tool_session )
		{
			MSG_S2C::stUpdateActivityAck ack;
			ack.SetPtr( p );
			g_activity_mgr->tool_session->SendPacket( ack );
		}
		if( p->category == activity_mgr::ACTIVITY_CATEGORY_DONATION_EVENT )
		{
			activity_mgr::donation_event_config_t* evt = static_cast<activity_mgr::donation_event_config_t*>( p );
			g_donation_system->AddDonationEvent( evt->event_id, evt->starttime, evt->expire,evt->event_photo_count);
		}
		
		if (p->category == activity_mgr::ACTIVITY_CATEGORY_TREASURE)
		{
			activity_mgr::treasure_event_config_t* evt = static_cast<activity_mgr::treasure_event_config_t*>( p);
			g_treasure_mgr->AddTreasureData(evt);
		}
		if (p->category == activity_mgr::ACTIVITY_CATEGORY_SHOP)
		{
			activity_mgr::shop_event_config_t* evt = static_cast<activity_mgr::shop_event_config_t*>( p);
			//add
			g_shopactivity_system->AddShopActivity(evt, fromdb);
		}
		g_broadTextMgr_system->AddActivityText(p);
		return true;
	}
	else
		return false;
}

bool activity_srv::srv_listener::on_remove_activity( activity_mgr::activity_base_t* p )
{
	if( p->remove() )
	{
		p->duty( false );

		if( g_activity_mgr->tool_session )
		{
			MSG_S2C::stRemoveActivityAck ack;
			ack.actid = p->id;
			g_activity_mgr->tool_session->SendPacket( ack );
		}

		if( p->category == activity_mgr::ACTIVITY_CATEGORY_DONATION_EVENT )
		{
			activity_mgr::donation_event_config_t* evt = static_cast<activity_mgr::donation_event_config_t*>( p );
			g_donation_system->RemoveDonationEvent( evt->event_id );
			
		}
		if (p->category == activity_mgr::ACTIVITY_CATEGORY_TREASURE)
		{
			activity_mgr::treasure_event_config_t* evt = static_cast<activity_mgr::treasure_event_config_t*>( p);
			g_treasure_mgr->RemoveTreasureData(evt->id);
		}

		if (p->category == activity_mgr::ACTIVITY_CATEGORY_SHOP)
		{
			activity_mgr::shop_event_config_t* evt = static_cast<activity_mgr::shop_event_config_t*>( p);
			//remove
			g_shopactivity_system->RemoveShopActivity(evt);
		}
		g_broadTextMgr_system->RemoveActivityText(p->id);
		return true;
	}
	else
		return false;
}

void activity_srv::srv_listener::on_duty_change( activity_mgr::activity_base_t* p )
{
	if( g_activity_mgr->tool_session )
	{
		if( p->on_duty )
			p->attach = UNIXTIME;

		MSG_S2C::stDutyChangeNotify notify;
		notify.actid = p->id;
		notify.on_duty = p->on_duty;
		g_activity_mgr->tool_session->SendPacket( notify );

		//if( p->description[p->on_duty].length() )
		//	sWorld.SendWorldText( p->description[p->on_duty].c_str() );

		g_broadTextMgr_system->AddActivityText(p);

	}
}

void activity_srv::srv_listener::on_upate()
{

}

void activity_srv::srv_listener::on_duty_update( activity_mgr::activity_base_t* p )
{
	return ;
}

void WorldSession::HandleActToolAccessReq( CPacketUsn& packet )
{
	/*
	g_activity_mgr->tool_session = this;
	MSG_S2C::stActToolAccessAck ack;
	ack.pass = 1;
	ack.servertime = UNIXTIME;
	SendPacket( ack );
	*/
}

void WorldSession::HandleQueryActivityList( CPacketUsn& packet )
{
	if( g_activity_mgr->tool_session != this )
		return;

	MSG_S2C::stActivityListAck ack;
	SendPacket( ack );
}

void WorldSession::HandleUpdateActivityReq( CPacketUsn& packet )
{
	if( g_activity_mgr->tool_session != this )
		return;

	MSG_C2S::stUpdateActivityReq req;
	packet >> req;
}

void WorldSession::HandleRemoveActivityReq( CPacketUsn& packet )
{
	if( g_activity_mgr->tool_session != this )
		return;

	MSG_C2S::stRemoveActivityReq req;
	packet >> req;
	g_activity_mgr->remove_activity( req.actid );
}
