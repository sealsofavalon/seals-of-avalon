#ifndef _ACTIVITY_SRV_HEAD
#define _ACTIVITY_SRV_HEAD
#include "activity.h"

class activity_srv
{
public:
	activity_srv();
	~activity_srv();
	class srv_listener : public activity_mgr::activity_listener_base
	{
	public:
		virtual bool on_add_activity( activity_mgr::activity_base_t* p, bool fromdb = false);
		virtual bool on_remove_activity( activity_mgr::activity_base_t* p );
		virtual bool on_upate(activity_mgr::activity_base_t* p);
		virtual void on_duty_change( activity_mgr::activity_base_t* p );
		virtual void on_upate();
		virtual void on_duty_update( activity_mgr::activity_base_t* p );
	};

	srv_listener* m_listener;
};

extern activity_srv* g_activity_srv;

#endif // _ACTIVITY_SRV_HEAD